# OpenTracing Twirp

This library allows for end-to-end tracing via OpenTracing, implemented as a Twirp hook.


## Getting Started

```golang
twirp.ChainHooks(twirptracing.NewHook())
```