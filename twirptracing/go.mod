module code.justin.tv/commerce/twirptracing

go 1.14

require (
	github.com/opentracing/opentracing-go v1.2.0
	github.com/pkg/errors v0.9.1 // indirect
	github.com/twitchtv/twirp v5.12.1+incompatible
)
