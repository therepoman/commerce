package twirptracing

import (
	"context"
	"fmt"
	"strconv"

	"github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/ext"
	"github.com/opentracing/opentracing-go/log"
	"github.com/twitchtv/twirp"
)

// NewHook returns a Twirp hook that enables tracing end-to-end, accepting clients sending trace ids
// and any outgoing twirp call will also attach trace ids.
//
// Example:
//
// twirptracing.NewHook()
//
// This twirp hook expects there to be a opentracing GlobalTracer initialized already.
func NewHook() *twirp.ServerHooks {
	hooks := &twirp.ServerHooks{}

	hooks.RequestReceived = func(ctx context.Context) (context.Context, error) {
		return ctx, nil
	}

	hooks.RequestRouted = func(ctx context.Context) (context.Context, error) {
		tracer := opentracing.GlobalTracer()
		header, okHeader := twirp.HTTPRequestHeaders(ctx)

		method, ok := twirp.MethodName(ctx)
		if !ok {
			return ctx, nil
		}

		operation := method

		if okHeader {
			spanCtx, _ := tracer.Extract(opentracing.HTTPHeaders, opentracing.HTTPHeadersCarrier(header))
			span, ctx := opentracing.StartSpanFromContext(ctx, operation, ext.RPCServerOption(spanCtx))

			if err := tracer.Inject(span.Context(), opentracing.HTTPHeaders, opentracing.HTTPHeadersCarrier(header)); err != nil {
				return ctx, err
			}

			return ctx, nil
		}

		_, ctx = opentracing.StartSpanFromContext(ctx, operation)
		return ctx, nil
	}

	hooks.ResponseSent = func(ctx context.Context) {
		statusCode, haveStatus := twirp.StatusCode(ctx)

		span := opentracing.SpanFromContext(ctx)
		if span == nil {
			return
		}

		defer span.Finish()

		if !haveStatus {
			return
		}

		code, err := strconv.Atoi(statusCode)
		if err != nil {
			return
		}

		span.SetTag("component", "twirp")
		span.SetTag("http.method", "POST")
		span.SetTag("http.status_code", code)
		span.SetTag("span.kind", "server")

		if code >= 400 {
			span.SetTag("error", true)
			span.LogFields(
				log.Error(fmt.Errorf("twirp responded with error code '%s'", statusCode)),
			)
		}
	}

	return hooks
}
