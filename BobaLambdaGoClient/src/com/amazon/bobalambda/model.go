package com_amazon_bobalambda

import (
	__model__ "code.justin.tv/commerce/CoralGoModel/src/coral/model"
	__reflect__ "reflect"
)

func init() {
	var val *bool
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *bool
		if f, ok := from.Interface().(*bool); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to BooleanValue")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *float64
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *float64
		if f, ok := from.Interface().(*float64); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to DoubleValue")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *int32
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *int32
		if f, ok := from.Interface().(*int32); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to DayOfWeek")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *int32
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *int32
		if f, ok := from.Interface().(*int32); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to TimeOfDay")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *int32
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *int32
		if f, ok := from.Interface().(*int32); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to IntValue")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *int64
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *int64
		if f, ok := from.Interface().(*int64); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to LongValue")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to StringValue")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to UserStatus")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to RestrictionType")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to ErrorMessage")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to ModerationStatus")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to ModerationFailReason")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to TaskType")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to BountyStatus")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to PayoutModel")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}

//This exception is when the user cannot claim a bounty because they have
//reached the maximum number of claimed bounties.
type BountyLimitReachedException interface {
	error
	SetMessage(v *string)
	Message() *string
}
type _BountyLimitReachedException struct {
	UnrecoverableException
	Ị_message *string `coral:"message" json:"message"`
}

func (this *_BountyLimitReachedException) Error() string {
	return __model__.ErrorMessage(this)
}
func (this *_BountyLimitReachedException) Message() *string {
	return this.Ị_message
}
func (this *_BountyLimitReachedException) SetMessage(v *string) {
	this.Ị_message = v
}
func NewBountyLimitReachedException() BountyLimitReachedException {
	return &_BountyLimitReachedException{}
}
func init() {
	var val BountyLimitReachedException
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("BobaLambda").Assembly("com.amazon.bobalambda").RegisterShape("BountyLimitReachedException", t, func() interface{} {
		return NewBountyLimitReachedException()
	})
}

type GetBountiesResponse interface {
	SetBounties(v []Bounty)
	Bounties() []Bounty
	SetNextCursor(v *string)
	NextCursor() *string
}
type _GetBountiesResponse struct {
	Ị_bounties   []Bounty `coral:"bounties" json:"bounties"`
	Ị_nextCursor *string  `coral:"nextCursor" json:"nextCursor"`
}

func (this *_GetBountiesResponse) Bounties() []Bounty {
	return this.Ị_bounties
}
func (this *_GetBountiesResponse) SetBounties(v []Bounty) {
	this.Ị_bounties = v
}
func (this *_GetBountiesResponse) NextCursor() *string {
	return this.Ị_nextCursor
}
func (this *_GetBountiesResponse) SetNextCursor(v *string) {
	this.Ị_nextCursor = v
}
func NewGetBountiesResponse() GetBountiesResponse {
	return &_GetBountiesResponse{}
}
func init() {
	var val GetBountiesResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("BobaLambda").Assembly("com.amazon.bobalambda").RegisterShape("GetBountiesResponse", t, func() interface{} {
		return NewGetBountiesResponse()
	})
}

type CheckBountyProgressResponse interface {
	SetBounty(v Bounty)
	Bounty() Bounty
}
type _CheckBountyProgressResponse struct {
	Ị_bounty Bounty `coral:"bounty" json:"bounty"`
}

func (this *_CheckBountyProgressResponse) Bounty() Bounty {
	return this.Ị_bounty
}
func (this *_CheckBountyProgressResponse) SetBounty(v Bounty) {
	this.Ị_bounty = v
}
func NewCheckBountyProgressResponse() CheckBountyProgressResponse {
	return &_CheckBountyProgressResponse{}
}
func init() {
	var val CheckBountyProgressResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("BobaLambda").Assembly("com.amazon.bobalambda").RegisterShape("CheckBountyProgressResponse", t, func() interface{} {
		return NewCheckBountyProgressResponse()
	})
}

type StopBountyRequest interface {
	SetTuid(v *string)
	Tuid() *string
	SetBountyId(v *string)
	BountyId() *string
}
type _StopBountyRequest struct {
	Ị_tuid     *string `coral:"tuid" json:"tuid"`
	Ị_bountyId *string `coral:"bountyId" json:"bountyId"`
}

func (this *_StopBountyRequest) Tuid() *string {
	return this.Ị_tuid
}
func (this *_StopBountyRequest) SetTuid(v *string) {
	this.Ị_tuid = v
}
func (this *_StopBountyRequest) BountyId() *string {
	return this.Ị_bountyId
}
func (this *_StopBountyRequest) SetBountyId(v *string) {
	this.Ị_bountyId = v
}
func NewStopBountyRequest() StopBountyRequest {
	return &_StopBountyRequest{}
}
func init() {
	var val StopBountyRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("BobaLambda").Assembly("com.amazon.bobalambda").RegisterShape("StopBountyRequest", t, func() interface{} {
		return NewStopBountyRequest()
	})
}

type RefreshBroadcasterSettingsRequest interface {
	SetCampaignId(v *string)
	CampaignId() *string
}
type _RefreshBroadcasterSettingsRequest struct {
	Ị_campaignId *string `coral:"campaignId" json:"campaignId"`
}

func (this *_RefreshBroadcasterSettingsRequest) CampaignId() *string {
	return this.Ị_campaignId
}
func (this *_RefreshBroadcasterSettingsRequest) SetCampaignId(v *string) {
	this.Ị_campaignId = v
}
func NewRefreshBroadcasterSettingsRequest() RefreshBroadcasterSettingsRequest {
	return &_RefreshBroadcasterSettingsRequest{}
}
func init() {
	var val RefreshBroadcasterSettingsRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("BobaLambda").Assembly("com.amazon.bobalambda").RegisterShape("RefreshBroadcasterSettingsRequest", t, func() interface{} {
		return NewRefreshBroadcasterSettingsRequest()
	})
}

//This exception is when the user is claiming a bounty that is already
//completed, and the campaign has restricted the number of times it can
//be completed.
type BountyAlreadyCompletedException interface {
	error
	SetMessage(v *string)
	Message() *string
}
type _BountyAlreadyCompletedException struct {
	InvalidParameterException
	UnrecoverableException
	Ị_message *string `coral:"message" json:"message"`
}

func (this *_BountyAlreadyCompletedException) Error() string {
	return __model__.ErrorMessage(this)
}
func (this *_BountyAlreadyCompletedException) Message() *string {
	return this.Ị_message
}
func (this *_BountyAlreadyCompletedException) SetMessage(v *string) {
	this.Ị_message = v
}
func NewBountyAlreadyCompletedException() BountyAlreadyCompletedException {
	return &_BountyAlreadyCompletedException{}
}
func init() {
	var val BountyAlreadyCompletedException
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("BobaLambda").Assembly("com.amazon.bobalambda").RegisterShape("BountyAlreadyCompletedException", t, func() interface{} {
		return NewBountyAlreadyCompletedException()
	})
}

type Settings interface {
	SetOverriddenCcuP20(v *int64)
	OverriddenCcuP20() *int64
	SetOverriddenCcuAvg(v *int64)
	OverriddenCcuAvg() *int64
	SetOverriddenCountry(v *string)
	OverriddenCountry() *string
	SetStatus(v *string)
	Status() *string
	SetUserId(v *string)
	UserId() *string
	SetIsTest(v *bool)
	IsTest() *bool
	SetIsBanned(v *bool)
	IsBanned() *bool
	SetUpdateTimestamps(v []int64)
	UpdateTimestamps() []int64
	SetIsEnabled(v *bool)
	IsEnabled() *bool
	SetViewedBountiesAt(v *int64)
	ViewedBountiesAt() *int64
	SetShowNotification(v *bool)
	ShowNotification() *bool
	SetOverriddenCcuP90(v *int64)
	OverriddenCcuP90() *int64
}
type _Settings struct {
	Ị_overriddenCountry *string `coral:"overriddenCountry" json:"overriddenCountry"`
	Ị_status            *string `coral:"status" json:"status"`
	Ị_userId            *string `coral:"userId" json:"userId"`
	Ị_isTest            *bool   `coral:"isTest" json:"isTest"`
	Ị_isBanned          *bool   `coral:"isBanned" json:"isBanned"`
	Ị_updateTimestamps  []int64 `coral:"updateTimestamps" json:"updateTimestamps"`
	Ị_overriddenCcuP20  *int64  `coral:"overriddenCcuP20" json:"overriddenCcuP20"`
	Ị_overriddenCcuAvg  *int64  `coral:"overriddenCcuAvg" json:"overriddenCcuAvg"`
	Ị_isEnabled         *bool   `coral:"isEnabled" json:"isEnabled"`
	Ị_viewedBountiesAt  *int64  `coral:"viewedBountiesAt" json:"viewedBountiesAt"`
	Ị_showNotification  *bool   `coral:"showNotification" json:"showNotification"`
	Ị_overriddenCcuP90  *int64  `coral:"overriddenCcuP90" json:"overriddenCcuP90"`
}

func (this *_Settings) Status() *string {
	return this.Ị_status
}
func (this *_Settings) SetStatus(v *string) {
	this.Ị_status = v
}
func (this *_Settings) UserId() *string {
	return this.Ị_userId
}
func (this *_Settings) SetUserId(v *string) {
	this.Ị_userId = v
}
func (this *_Settings) IsTest() *bool {
	return this.Ị_isTest
}
func (this *_Settings) SetIsTest(v *bool) {
	this.Ị_isTest = v
}
func (this *_Settings) IsBanned() *bool {
	return this.Ị_isBanned
}
func (this *_Settings) SetIsBanned(v *bool) {
	this.Ị_isBanned = v
}
func (this *_Settings) UpdateTimestamps() []int64 {
	return this.Ị_updateTimestamps
}
func (this *_Settings) SetUpdateTimestamps(v []int64) {
	this.Ị_updateTimestamps = v
}
func (this *_Settings) OverriddenCcuP20() *int64 {
	return this.Ị_overriddenCcuP20
}
func (this *_Settings) SetOverriddenCcuP20(v *int64) {
	this.Ị_overriddenCcuP20 = v
}
func (this *_Settings) OverriddenCcuAvg() *int64 {
	return this.Ị_overriddenCcuAvg
}
func (this *_Settings) SetOverriddenCcuAvg(v *int64) {
	this.Ị_overriddenCcuAvg = v
}
func (this *_Settings) OverriddenCountry() *string {
	return this.Ị_overriddenCountry
}
func (this *_Settings) SetOverriddenCountry(v *string) {
	this.Ị_overriddenCountry = v
}
func (this *_Settings) IsEnabled() *bool {
	return this.Ị_isEnabled
}
func (this *_Settings) SetIsEnabled(v *bool) {
	this.Ị_isEnabled = v
}
func (this *_Settings) ViewedBountiesAt() *int64 {
	return this.Ị_viewedBountiesAt
}
func (this *_Settings) SetViewedBountiesAt(v *int64) {
	this.Ị_viewedBountiesAt = v
}
func (this *_Settings) ShowNotification() *bool {
	return this.Ị_showNotification
}
func (this *_Settings) SetShowNotification(v *bool) {
	this.Ị_showNotification = v
}
func (this *_Settings) OverriddenCcuP90() *int64 {
	return this.Ị_overriddenCcuP90
}
func (this *_Settings) SetOverriddenCcuP90(v *int64) {
	this.Ị_overriddenCcuP90 = v
}
func NewSettings() Settings {
	return &_Settings{}
}
func init() {
	var val Settings
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("BobaLambda").Assembly("com.amazon.bobalambda").RegisterShape("Settings", t, func() interface{} {
		return NewSettings()
	})
}

type EarningsEvent interface {
	SetBountyId(v *string)
	BountyId() *string
	SetId(v *string)
	Id() *string
	SetUserId(v *string)
	UserId() *string
	SetEarnTime(v *int64)
	EarnTime() *int64
	SetEarnAmountCents(v *int64)
	EarnAmountCents() *int64
	SetCampaignId(v *string)
	CampaignId() *string
}
type _EarningsEvent struct {
	Ị_earnTime        *int64  `coral:"earnTime" json:"earnTime"`
	Ị_earnAmountCents *int64  `coral:"earnAmountCents" json:"earnAmountCents"`
	Ị_campaignId      *string `coral:"campaignId" json:"campaignId"`
	Ị_bountyId        *string `coral:"bountyId" json:"bountyId"`
	Ị_id              *string `coral:"id" json:"id"`
	Ị_userId          *string `coral:"userId" json:"userId"`
}

func (this *_EarningsEvent) UserId() *string {
	return this.Ị_userId
}
func (this *_EarningsEvent) SetUserId(v *string) {
	this.Ị_userId = v
}
func (this *_EarningsEvent) EarnTime() *int64 {
	return this.Ị_earnTime
}
func (this *_EarningsEvent) SetEarnTime(v *int64) {
	this.Ị_earnTime = v
}
func (this *_EarningsEvent) EarnAmountCents() *int64 {
	return this.Ị_earnAmountCents
}
func (this *_EarningsEvent) SetEarnAmountCents(v *int64) {
	this.Ị_earnAmountCents = v
}
func (this *_EarningsEvent) CampaignId() *string {
	return this.Ị_campaignId
}
func (this *_EarningsEvent) SetCampaignId(v *string) {
	this.Ị_campaignId = v
}
func (this *_EarningsEvent) BountyId() *string {
	return this.Ị_bountyId
}
func (this *_EarningsEvent) SetBountyId(v *string) {
	this.Ị_bountyId = v
}
func (this *_EarningsEvent) Id() *string {
	return this.Ị_id
}
func (this *_EarningsEvent) SetId(v *string) {
	this.Ị_id = v
}
func NewEarningsEvent() EarningsEvent {
	return &_EarningsEvent{}
}
func init() {
	var val EarningsEvent
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("BobaLambda").Assembly("com.amazon.bobalambda").RegisterShape("EarningsEvent", t, func() interface{} {
		return NewEarningsEvent()
	})
}

//This exception is thrown when trying to start a bounty with banned words in the title.
type StreamTitleBannedWordsException interface {
	error
	SetMessage(v *string)
	Message() *string
}
type _StreamTitleBannedWordsException struct {
	UnrecoverableException
	Ị_message *string `coral:"message" json:"message"`
}

func (this *_StreamTitleBannedWordsException) Error() string {
	return __model__.ErrorMessage(this)
}
func (this *_StreamTitleBannedWordsException) Message() *string {
	return this.Ị_message
}
func (this *_StreamTitleBannedWordsException) SetMessage(v *string) {
	this.Ị_message = v
}
func NewStreamTitleBannedWordsException() StreamTitleBannedWordsException {
	return &_StreamTitleBannedWordsException{}
}
func init() {
	var val StreamTitleBannedWordsException
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("BobaLambda").Assembly("com.amazon.bobalambda").RegisterShape("StreamTitleBannedWordsException", t, func() interface{} {
		return NewStreamTitleBannedWordsException()
	})
}

type ClaimBountyResponse interface {
	SetBounty(v Bounty)
	Bounty() Bounty
}
type _ClaimBountyResponse struct {
	Ị_bounty Bounty `coral:"bounty" json:"bounty"`
}

func (this *_ClaimBountyResponse) Bounty() Bounty {
	return this.Ị_bounty
}
func (this *_ClaimBountyResponse) SetBounty(v Bounty) {
	this.Ị_bounty = v
}
func NewClaimBountyResponse() ClaimBountyResponse {
	return &_ClaimBountyResponse{}
}
func init() {
	var val ClaimBountyResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("BobaLambda").Assembly("com.amazon.bobalambda").RegisterShape("ClaimBountyResponse", t, func() interface{} {
		return NewClaimBountyResponse()
	})
}

type StopBountyResponse interface {
	SetBounty(v Bounty)
	Bounty() Bounty
}
type _StopBountyResponse struct {
	Ị_bounty Bounty `coral:"bounty" json:"bounty"`
}

func (this *_StopBountyResponse) Bounty() Bounty {
	return this.Ị_bounty
}
func (this *_StopBountyResponse) SetBounty(v Bounty) {
	this.Ị_bounty = v
}
func NewStopBountyResponse() StopBountyResponse {
	return &_StopBountyResponse{}
}
func init() {
	var val StopBountyResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("BobaLambda").Assembly("com.amazon.bobalambda").RegisterShape("StopBountyResponse", t, func() interface{} {
		return NewStopBountyResponse()
	})
}

type GetBountiesReportResponse interface {
	SetBountiesReport(v []BountyReport)
	BountiesReport() []BountyReport
}
type _GetBountiesReportResponse struct {
	Ị_bountiesReport []BountyReport `coral:"bountiesReport" json:"bountiesReport"`
}

func (this *_GetBountiesReportResponse) BountiesReport() []BountyReport {
	return this.Ị_bountiesReport
}
func (this *_GetBountiesReportResponse) SetBountiesReport(v []BountyReport) {
	this.Ị_bountiesReport = v
}
func NewGetBountiesReportResponse() GetBountiesReportResponse {
	return &_GetBountiesReportResponse{}
}
func init() {
	var val GetBountiesReportResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("BobaLambda").Assembly("com.amazon.bobalambda").RegisterShape("GetBountiesReportResponse", t, func() interface{} {
		return NewGetBountiesReportResponse()
	})
}

type UpdateBountyProgressRequest interface {
	SetProgress(v *int64)
	Progress() *int64
	SetBountyId(v *string)
	BountyId() *string
}
type _UpdateBountyProgressRequest struct {
	Ị_progress *int64  `coral:"progress" json:"progress"`
	Ị_bountyId *string `coral:"bountyId" json:"bountyId"`
}

func (this *_UpdateBountyProgressRequest) BountyId() *string {
	return this.Ị_bountyId
}
func (this *_UpdateBountyProgressRequest) SetBountyId(v *string) {
	this.Ị_bountyId = v
}
func (this *_UpdateBountyProgressRequest) Progress() *int64 {
	return this.Ị_progress
}
func (this *_UpdateBountyProgressRequest) SetProgress(v *int64) {
	this.Ị_progress = v
}
func NewUpdateBountyProgressRequest() UpdateBountyProgressRequest {
	return &_UpdateBountyProgressRequest{}
}
func init() {
	var val UpdateBountyProgressRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("BobaLambda").Assembly("com.amazon.bobalambda").RegisterShape("UpdateBountyProgressRequest", t, func() interface{} {
		return NewUpdateBountyProgressRequest()
	})
}

type CheckBountyProgressRequest interface {
	SetTuid(v *string)
	Tuid() *string
	SetBountyId(v *string)
	BountyId() *string
}
type _CheckBountyProgressRequest struct {
	Ị_tuid     *string `coral:"tuid" json:"tuid"`
	Ị_bountyId *string `coral:"bountyId" json:"bountyId"`
}

func (this *_CheckBountyProgressRequest) Tuid() *string {
	return this.Ị_tuid
}
func (this *_CheckBountyProgressRequest) SetTuid(v *string) {
	this.Ị_tuid = v
}
func (this *_CheckBountyProgressRequest) BountyId() *string {
	return this.Ị_bountyId
}
func (this *_CheckBountyProgressRequest) SetBountyId(v *string) {
	this.Ị_bountyId = v
}
func NewCheckBountyProgressRequest() CheckBountyProgressRequest {
	return &_CheckBountyProgressRequest{}
}
func init() {
	var val CheckBountyProgressRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("BobaLambda").Assembly("com.amazon.bobalambda").RegisterShape("CheckBountyProgressRequest", t, func() interface{} {
		return NewCheckBountyProgressRequest()
	})
}

type BountyTask interface {
	SetCurrentProgress(v *int64)
	CurrentProgress() *int64
	SetEndTime(v *int64)
	EndTime() *int64
	SetSessionStartTime(v *int64)
	SessionStartTime() *int64
	SetSessionEndTime(v *int64)
	SessionEndTime() *int64
	SetCampaignTaskId(v *string)
	CampaignTaskId() *string
	SetProgressBreakdown(v *string)
	ProgressBreakdown() *string
	SetRestriction(v Restriction)
	Restriction() Restriction
	SetStartTime(v *int64)
	StartTime() *int64
	SetVods(v []string)
	Vods() []string
}
type _BountyTask struct {
	Ị_startTime         *int64      `coral:"startTime" json:"startTime"`
	Ị_vods              []string    `coral:"vods" json:"vods"`
	Ị_campaignTaskId    *string     `coral:"campaignTaskId" json:"campaignTaskId"`
	Ị_progressBreakdown *string     `coral:"progressBreakdown" json:"progressBreakdown"`
	Ị_restriction       Restriction `coral:"restriction" json:"restriction"`
	Ị_sessionEndTime    *int64      `coral:"sessionEndTime" json:"sessionEndTime"`
	Ị_currentProgress   *int64      `coral:"currentProgress" json:"currentProgress"`
	Ị_endTime           *int64      `coral:"endTime" json:"endTime"`
	Ị_sessionStartTime  *int64      `coral:"sessionStartTime" json:"sessionStartTime"`
}

func (this *_BountyTask) SessionStartTime() *int64 {
	return this.Ị_sessionStartTime
}
func (this *_BountyTask) SetSessionStartTime(v *int64) {
	this.Ị_sessionStartTime = v
}
func (this *_BountyTask) SessionEndTime() *int64 {
	return this.Ị_sessionEndTime
}
func (this *_BountyTask) SetSessionEndTime(v *int64) {
	this.Ị_sessionEndTime = v
}
func (this *_BountyTask) CurrentProgress() *int64 {
	return this.Ị_currentProgress
}
func (this *_BountyTask) SetCurrentProgress(v *int64) {
	this.Ị_currentProgress = v
}
func (this *_BountyTask) EndTime() *int64 {
	return this.Ị_endTime
}
func (this *_BountyTask) SetEndTime(v *int64) {
	this.Ị_endTime = v
}
func (this *_BountyTask) Restriction() Restriction {
	return this.Ị_restriction
}
func (this *_BountyTask) SetRestriction(v Restriction) {
	this.Ị_restriction = v
}
func (this *_BountyTask) StartTime() *int64 {
	return this.Ị_startTime
}
func (this *_BountyTask) SetStartTime(v *int64) {
	this.Ị_startTime = v
}
func (this *_BountyTask) Vods() []string {
	return this.Ị_vods
}
func (this *_BountyTask) SetVods(v []string) {
	this.Ị_vods = v
}
func (this *_BountyTask) CampaignTaskId() *string {
	return this.Ị_campaignTaskId
}
func (this *_BountyTask) SetCampaignTaskId(v *string) {
	this.Ị_campaignTaskId = v
}
func (this *_BountyTask) ProgressBreakdown() *string {
	return this.Ị_progressBreakdown
}
func (this *_BountyTask) SetProgressBreakdown(v *string) {
	this.Ị_progressBreakdown = v
}
func NewBountyTask() BountyTask {
	return &_BountyTask{}
}
func init() {
	var val BountyTask
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("BobaLambda").Assembly("com.amazon.bobalambda").RegisterShape("BountyTask", t, func() interface{} {
		return NewBountyTask()
	})
}

type StartBountyRequest interface {
	SetTuid(v *string)
	Tuid() *string
	SetBountyId(v *string)
	BountyId() *string
	SetStreamTitle(v *string)
	StreamTitle() *string
}
type _StartBountyRequest struct {
	Ị_tuid        *string `coral:"tuid" json:"tuid"`
	Ị_bountyId    *string `coral:"bountyId" json:"bountyId"`
	Ị_streamTitle *string `coral:"streamTitle" json:"streamTitle"`
}

func (this *_StartBountyRequest) Tuid() *string {
	return this.Ị_tuid
}
func (this *_StartBountyRequest) SetTuid(v *string) {
	this.Ị_tuid = v
}
func (this *_StartBountyRequest) BountyId() *string {
	return this.Ị_bountyId
}
func (this *_StartBountyRequest) SetBountyId(v *string) {
	this.Ị_bountyId = v
}
func (this *_StartBountyRequest) StreamTitle() *string {
	return this.Ị_streamTitle
}
func (this *_StartBountyRequest) SetStreamTitle(v *string) {
	this.Ị_streamTitle = v
}
func NewStartBountyRequest() StartBountyRequest {
	return &_StartBountyRequest{}
}
func init() {
	var val StartBountyRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("BobaLambda").Assembly("com.amazon.bobalambda").RegisterShape("StartBountyRequest", t, func() interface{} {
		return NewStartBountyRequest()
	})
}

//This exception is thrown on a dependency error.
type DependencyException interface {
	error
	SetMessage(v *string)
	Message() *string
}
type _DependencyException struct {
	RecoverableException
	Ị_message *string `coral:"message" json:"message"`
}

func (this *_DependencyException) Error() string {
	return __model__.ErrorMessage(this)
}
func (this *_DependencyException) Message() *string {
	return this.Ị_message
}
func (this *_DependencyException) SetMessage(v *string) {
	this.Ị_message = v
}
func NewDependencyException() DependencyException {
	return &_DependencyException{}
}
func init() {
	var val DependencyException
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("BobaLambda").Assembly("com.amazon.bobalambda").RegisterShape("DependencyException", t, func() interface{} {
		return NewDependencyException()
	})
}

type CallToActionDefinition interface {
	SetHasCallToAction(v *bool)
	HasCallToAction() *bool
	SetFrequency(v *int64)
	Frequency() *int64
	SetCooldown(v *int64)
	Cooldown() *int64
	SetDuration(v *int64)
	Duration() *int64
	SetCallsToAction(v map[string]CallToAction)
	CallsToAction() map[string]CallToAction
}
type _CallToActionDefinition struct {
	Ị_duration        *int64                  `coral:"duration" json:"duration"`
	Ị_callsToAction   map[string]CallToAction `coral:"callsToAction" json:"callsToAction"`
	Ị_hasCallToAction *bool                   `coral:"hasCallToAction" json:"hasCallToAction"`
	Ị_frequency       *int64                  `coral:"frequency" json:"frequency"`
	Ị_cooldown        *int64                  `coral:"cooldown" json:"cooldown"`
}

func (this *_CallToActionDefinition) Duration() *int64 {
	return this.Ị_duration
}
func (this *_CallToActionDefinition) SetDuration(v *int64) {
	this.Ị_duration = v
}
func (this *_CallToActionDefinition) CallsToAction() map[string]CallToAction {
	return this.Ị_callsToAction
}
func (this *_CallToActionDefinition) SetCallsToAction(v map[string]CallToAction) {
	this.Ị_callsToAction = v
}
func (this *_CallToActionDefinition) HasCallToAction() *bool {
	return this.Ị_hasCallToAction
}
func (this *_CallToActionDefinition) SetHasCallToAction(v *bool) {
	this.Ị_hasCallToAction = v
}
func (this *_CallToActionDefinition) Frequency() *int64 {
	return this.Ị_frequency
}
func (this *_CallToActionDefinition) SetFrequency(v *int64) {
	this.Ị_frequency = v
}
func (this *_CallToActionDefinition) Cooldown() *int64 {
	return this.Ị_cooldown
}
func (this *_CallToActionDefinition) SetCooldown(v *int64) {
	this.Ị_cooldown = v
}
func NewCallToActionDefinition() CallToActionDefinition {
	return &_CallToActionDefinition{}
}
func init() {
	var val CallToActionDefinition
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("BobaLambda").Assembly("com.amazon.bobalambda").RegisterShape("CallToActionDefinition", t, func() interface{} {
		return NewCallToActionDefinition()
	})
}

//This exception is when the user cannot claim a bounty because the
//available quantity for the bounty has been exhausted.
type BountyNotEnoughQuantityException interface {
	error
	SetMessage(v *string)
	Message() *string
}
type _BountyNotEnoughQuantityException struct {
	UnrecoverableException
	Ị_message *string `coral:"message" json:"message"`
}

func (this *_BountyNotEnoughQuantityException) Error() string {
	return __model__.ErrorMessage(this)
}
func (this *_BountyNotEnoughQuantityException) Message() *string {
	return this.Ị_message
}
func (this *_BountyNotEnoughQuantityException) SetMessage(v *string) {
	this.Ị_message = v
}
func NewBountyNotEnoughQuantityException() BountyNotEnoughQuantityException {
	return &_BountyNotEnoughQuantityException{}
}
func init() {
	var val BountyNotEnoughQuantityException
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("BobaLambda").Assembly("com.amazon.bobalambda").RegisterShape("BountyNotEnoughQuantityException", t, func() interface{} {
		return NewBountyNotEnoughQuantityException()
	})
}

type CancelBountyResponse interface {
	SetBounty(v Bounty)
	Bounty() Bounty
}
type _CancelBountyResponse struct {
	Ị_bounty Bounty `coral:"bounty" json:"bounty"`
}

func (this *_CancelBountyResponse) Bounty() Bounty {
	return this.Ị_bounty
}
func (this *_CancelBountyResponse) SetBounty(v Bounty) {
	this.Ị_bounty = v
}
func NewCancelBountyResponse() CancelBountyResponse {
	return &_CancelBountyResponse{}
}
func init() {
	var val CancelBountyResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("BobaLambda").Assembly("com.amazon.bobalambda").RegisterShape("CancelBountyResponse", t, func() interface{} {
		return NewCancelBountyResponse()
	})
}

type Key interface {
	SetCode(v *string)
	Code() *string
	SetProductType(v *string)
	ProductType() *string
	SetRegion(v *string)
	Region() *string
	SetPlatform(v *string)
	Platform() *string
}
type _Key struct {
	Ị_code        *string `coral:"code" json:"code"`
	Ị_productType *string `coral:"productType" json:"productType"`
	Ị_region      *string `coral:"region" json:"region"`
	Ị_platform    *string `coral:"platform" json:"platform"`
}

func (this *_Key) Code() *string {
	return this.Ị_code
}
func (this *_Key) SetCode(v *string) {
	this.Ị_code = v
}
func (this *_Key) ProductType() *string {
	return this.Ị_productType
}
func (this *_Key) SetProductType(v *string) {
	this.Ị_productType = v
}
func (this *_Key) Region() *string {
	return this.Ị_region
}
func (this *_Key) SetRegion(v *string) {
	this.Ị_region = v
}
func (this *_Key) Platform() *string {
	return this.Ị_platform
}
func (this *_Key) SetPlatform(v *string) {
	this.Ị_platform = v
}
func NewKey() Key {
	return &_Key{}
}
func init() {
	var val Key
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("BobaLambda").Assembly("com.amazon.bobalambda").RegisterShape("Key", t, func() interface{} {
		return NewKey()
	})
}

type ReconcileBountyRequest interface {
	SetBountyId(v *string)
	BountyId() *string
	SetShouldReconcileWithoutData(v *bool)
	ShouldReconcileWithoutData() *bool
}
type _ReconcileBountyRequest struct {
	Ị_bountyId                   *string `coral:"bountyId" json:"bountyId"`
	Ị_shouldReconcileWithoutData *bool   `coral:"shouldReconcileWithoutData" json:"shouldReconcileWithoutData"`
}

func (this *_ReconcileBountyRequest) ShouldReconcileWithoutData() *bool {
	return this.Ị_shouldReconcileWithoutData
}
func (this *_ReconcileBountyRequest) SetShouldReconcileWithoutData(v *bool) {
	this.Ị_shouldReconcileWithoutData = v
}
func (this *_ReconcileBountyRequest) BountyId() *string {
	return this.Ị_bountyId
}
func (this *_ReconcileBountyRequest) SetBountyId(v *string) {
	this.Ị_bountyId = v
}
func NewReconcileBountyRequest() ReconcileBountyRequest {
	return &_ReconcileBountyRequest{}
}
func init() {
	var val ReconcileBountyRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("BobaLambda").Assembly("com.amazon.bobalambda").RegisterShape("ReconcileBountyRequest", t, func() interface{} {
		return NewReconcileBountyRequest()
	})
}

type GetBountiesReportRequest interface {
	SetCampaignId(v *string)
	CampaignId() *string
}
type _GetBountiesReportRequest struct {
	Ị_campaignId *string `coral:"campaignId" json:"campaignId"`
}

func (this *_GetBountiesReportRequest) CampaignId() *string {
	return this.Ị_campaignId
}
func (this *_GetBountiesReportRequest) SetCampaignId(v *string) {
	this.Ị_campaignId = v
}
func NewGetBountiesReportRequest() GetBountiesReportRequest {
	return &_GetBountiesReportRequest{}
}
func init() {
	var val GetBountiesReportRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("BobaLambda").Assembly("com.amazon.bobalambda").RegisterShape("GetBountiesReportRequest", t, func() interface{} {
		return NewGetBountiesReportRequest()
	})
}

type PublishSponsoredEventRequest interface {
	SetBountyId(v *string)
	BountyId() *string
	SetPreviousWaitTimestamp(v *string)
	PreviousWaitTimestamp() *string
}
type _PublishSponsoredEventRequest struct {
	Ị_bountyId              *string `coral:"bountyId" json:"bountyId"`
	Ị_previousWaitTimestamp *string `coral:"previousWaitTimestamp" json:"previousWaitTimestamp"`
}

func (this *_PublishSponsoredEventRequest) BountyId() *string {
	return this.Ị_bountyId
}
func (this *_PublishSponsoredEventRequest) SetBountyId(v *string) {
	this.Ị_bountyId = v
}
func (this *_PublishSponsoredEventRequest) PreviousWaitTimestamp() *string {
	return this.Ị_previousWaitTimestamp
}
func (this *_PublishSponsoredEventRequest) SetPreviousWaitTimestamp(v *string) {
	this.Ị_previousWaitTimestamp = v
}
func NewPublishSponsoredEventRequest() PublishSponsoredEventRequest {
	return &_PublishSponsoredEventRequest{}
}
func init() {
	var val PublishSponsoredEventRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("BobaLambda").Assembly("com.amazon.bobalambda").RegisterShape("PublishSponsoredEventRequest", t, func() interface{} {
		return NewPublishSponsoredEventRequest()
	})
}

type GetSettingsResponse interface {
	SetIsEnabled(v *bool)
	IsEnabled() *bool
	SetSettings(v Settings)
	Settings() Settings
}
type _GetSettingsResponse struct {
	Ị_isEnabled *bool    `coral:"isEnabled" json:"isEnabled"`
	Ị_settings  Settings `coral:"settings" json:"settings"`
}

func (this *_GetSettingsResponse) Settings() Settings {
	return this.Ị_settings
}
func (this *_GetSettingsResponse) SetSettings(v Settings) {
	this.Ị_settings = v
}
func (this *_GetSettingsResponse) IsEnabled() *bool {
	return this.Ị_isEnabled
}
func (this *_GetSettingsResponse) SetIsEnabled(v *bool) {
	this.Ị_isEnabled = v
}
func NewGetSettingsResponse() GetSettingsResponse {
	return &_GetSettingsResponse{}
}
func init() {
	var val GetSettingsResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("BobaLambda").Assembly("com.amazon.bobalambda").RegisterShape("GetSettingsResponse", t, func() interface{} {
		return NewGetSettingsResponse()
	})
}

type CallToAction interface {
	SetTitle(v *string)
	Title() *string
	SetUrl(v *string)
	Url() *string
}
type _CallToAction struct {
	Ị_title *string `coral:"title" json:"title"`
	Ị_url   *string `coral:"url" json:"url"`
}

func (this *_CallToAction) Title() *string {
	return this.Ị_title
}
func (this *_CallToAction) SetTitle(v *string) {
	this.Ị_title = v
}
func (this *_CallToAction) Url() *string {
	return this.Ị_url
}
func (this *_CallToAction) SetUrl(v *string) {
	this.Ị_url = v
}
func NewCallToAction() CallToAction {
	return &_CallToAction{}
}
func init() {
	var val CallToAction
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("BobaLambda").Assembly("com.amazon.bobalambda").RegisterShape("CallToAction", t, func() interface{} {
		return NewCallToAction()
	})
}

type GetSettingsRequest interface {
	SetTuid(v *string)
	Tuid() *string
}
type _GetSettingsRequest struct {
	Ị_tuid *string `coral:"tuid" json:"tuid"`
}

func (this *_GetSettingsRequest) Tuid() *string {
	return this.Ị_tuid
}
func (this *_GetSettingsRequest) SetTuid(v *string) {
	this.Ị_tuid = v
}
func NewGetSettingsRequest() GetSettingsRequest {
	return &_GetSettingsRequest{}
}
func init() {
	var val GetSettingsRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("BobaLambda").Assembly("com.amazon.bobalambda").RegisterShape("GetSettingsRequest", t, func() interface{} {
		return NewGetSettingsRequest()
	})
}

//This exception is thrown when the requested object cannot be found.
type ObjectNotFoundException interface {
	error
	SetMessage(v *string)
	Message() *string
}
type _ObjectNotFoundException struct {
	UnrecoverableException
	Ị_message *string `coral:"message" json:"message"`
}

func (this *_ObjectNotFoundException) Error() string {
	return __model__.ErrorMessage(this)
}
func (this *_ObjectNotFoundException) Message() *string {
	return this.Ị_message
}
func (this *_ObjectNotFoundException) SetMessage(v *string) {
	this.Ị_message = v
}
func NewObjectNotFoundException() ObjectNotFoundException {
	return &_ObjectNotFoundException{}
}
func init() {
	var val ObjectNotFoundException
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("BobaLambda").Assembly("com.amazon.bobalambda").RegisterShape("ObjectNotFoundException", t, func() interface{} {
		return NewObjectNotFoundException()
	})
}

//This exception is thrown when the user is not authorized to perform the action.
type UnauthorizedException interface {
	error
	SetMessage(v *string)
	Message() *string
}
type _UnauthorizedException struct {
	UnrecoverableException
	Ị_message *string `coral:"message" json:"message"`
}

func (this *_UnauthorizedException) Error() string {
	return __model__.ErrorMessage(this)
}
func (this *_UnauthorizedException) Message() *string {
	return this.Ị_message
}
func (this *_UnauthorizedException) SetMessage(v *string) {
	this.Ị_message = v
}
func NewUnauthorizedException() UnauthorizedException {
	return &_UnauthorizedException{}
}
func init() {
	var val UnauthorizedException
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("BobaLambda").Assembly("com.amazon.bobalambda").RegisterShape("UnauthorizedException", t, func() interface{} {
		return NewUnauthorizedException()
	})
}

type BountyReport interface {
	SetActualMinutesWatched(v *int64)
	ActualMinutesWatched() *int64
	SetActualUniqueDevices(v *int64)
	ActualUniqueDevices() *int64
	SetActualUniqueDevicesThirtySecondsWatched(v *int64)
	ActualUniqueDevicesThirtySecondsWatched() *int64
	SetActualUniqueDevicesTwoMinutesWatched(v *int64)
	ActualUniqueDevicesTwoMinutesWatched() *int64
	SetBudget(v *int64)
	Budget() *int64
	SetProgressCompletedDate(v *int64)
	ProgressCompletedDate() *int64
	SetCallToActionLinkClicked(v *int64)
	CallToActionLinkClicked() *int64
	SetIsReconciled(v *bool)
	IsReconciled() *bool
	SetStatus(v *string)
	Status() *string
}
type _BountyReport struct {
	Ị_actualUniqueDevicesTwoMinutesWatched    *int64  `coral:"actualUniqueDevicesTwoMinutesWatched" json:"actualUniqueDevicesTwoMinutesWatched"`
	Ị_budget                                  *int64  `coral:"budget" json:"budget"`
	Ị_progressCompletedDate                   *int64  `coral:"progressCompletedDate" json:"progressCompletedDate"`
	Ị_callToActionLinkClicked                 *int64  `coral:"callToActionLinkClicked" json:"callToActionLinkClicked"`
	Ị_actualMinutesWatched                    *int64  `coral:"actualMinutesWatched" json:"actualMinutesWatched"`
	Ị_actualUniqueDevices                     *int64  `coral:"actualUniqueDevices" json:"actualUniqueDevices"`
	Ị_actualUniqueDevicesThirtySecondsWatched *int64  `coral:"actualUniqueDevicesThirtySecondsWatched" json:"actualUniqueDevicesThirtySecondsWatched"`
	Ị_isReconciled                            *bool   `coral:"isReconciled" json:"isReconciled"`
	Ị_status                                  *string `coral:"status" json:"status"`
}

func (this *_BountyReport) IsReconciled() *bool {
	return this.Ị_isReconciled
}
func (this *_BountyReport) SetIsReconciled(v *bool) {
	this.Ị_isReconciled = v
}
func (this *_BountyReport) Status() *string {
	return this.Ị_status
}
func (this *_BountyReport) SetStatus(v *string) {
	this.Ị_status = v
}
func (this *_BountyReport) ActualMinutesWatched() *int64 {
	return this.Ị_actualMinutesWatched
}
func (this *_BountyReport) SetActualMinutesWatched(v *int64) {
	this.Ị_actualMinutesWatched = v
}
func (this *_BountyReport) ActualUniqueDevices() *int64 {
	return this.Ị_actualUniqueDevices
}
func (this *_BountyReport) SetActualUniqueDevices(v *int64) {
	this.Ị_actualUniqueDevices = v
}
func (this *_BountyReport) ActualUniqueDevicesThirtySecondsWatched() *int64 {
	return this.Ị_actualUniqueDevicesThirtySecondsWatched
}
func (this *_BountyReport) SetActualUniqueDevicesThirtySecondsWatched(v *int64) {
	this.Ị_actualUniqueDevicesThirtySecondsWatched = v
}
func (this *_BountyReport) ActualUniqueDevicesTwoMinutesWatched() *int64 {
	return this.Ị_actualUniqueDevicesTwoMinutesWatched
}
func (this *_BountyReport) SetActualUniqueDevicesTwoMinutesWatched(v *int64) {
	this.Ị_actualUniqueDevicesTwoMinutesWatched = v
}
func (this *_BountyReport) Budget() *int64 {
	return this.Ị_budget
}
func (this *_BountyReport) SetBudget(v *int64) {
	this.Ị_budget = v
}
func (this *_BountyReport) ProgressCompletedDate() *int64 {
	return this.Ị_progressCompletedDate
}
func (this *_BountyReport) SetProgressCompletedDate(v *int64) {
	this.Ị_progressCompletedDate = v
}
func (this *_BountyReport) CallToActionLinkClicked() *int64 {
	return this.Ị_callToActionLinkClicked
}
func (this *_BountyReport) SetCallToActionLinkClicked(v *int64) {
	this.Ị_callToActionLinkClicked = v
}
func NewBountyReport() BountyReport {
	return &_BountyReport{}
}
func init() {
	var val BountyReport
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("BobaLambda").Assembly("com.amazon.bobalambda").RegisterShape("BountyReport", t, func() interface{} {
		return NewBountyReport()
	})
}

//This exception is when the user is claiming a bounty that is already
//active (i.e. potentially a duplicate call to claim bounty).
type BountyAlreadyClaimedException interface {
	error
	SetMessage(v *string)
	Message() *string
}
type _BountyAlreadyClaimedException struct {
	UnrecoverableException
	Ị_message *string `coral:"message" json:"message"`
}

func (this *_BountyAlreadyClaimedException) Error() string {
	return __model__.ErrorMessage(this)
}
func (this *_BountyAlreadyClaimedException) Message() *string {
	return this.Ị_message
}
func (this *_BountyAlreadyClaimedException) SetMessage(v *string) {
	this.Ị_message = v
}
func NewBountyAlreadyClaimedException() BountyAlreadyClaimedException {
	return &_BountyAlreadyClaimedException{}
}
func init() {
	var val BountyAlreadyClaimedException
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("BobaLambda").Assembly("com.amazon.bobalambda").RegisterShape("BountyAlreadyClaimedException", t, func() interface{} {
		return NewBountyAlreadyClaimedException()
	})
}

type UpdateBroadcasterSettingsResponse interface {
	SetSettingsList(v []Settings)
	SettingsList() []Settings
}
type _UpdateBroadcasterSettingsResponse struct {
	Ị_settingsList []Settings `coral:"settingsList" json:"settingsList"`
}

func (this *_UpdateBroadcasterSettingsResponse) SettingsList() []Settings {
	return this.Ị_settingsList
}
func (this *_UpdateBroadcasterSettingsResponse) SetSettingsList(v []Settings) {
	this.Ị_settingsList = v
}
func NewUpdateBroadcasterSettingsResponse() UpdateBroadcasterSettingsResponse {
	return &_UpdateBroadcasterSettingsResponse{}
}
func init() {
	var val UpdateBroadcasterSettingsResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("BobaLambda").Assembly("com.amazon.bobalambda").RegisterShape("UpdateBroadcasterSettingsResponse", t, func() interface{} {
		return NewUpdateBroadcasterSettingsResponse()
	})
}

//This exception is thrown when retrying will not help. e.g. bad input.
type UnrecoverableException interface {
	error
	SetMessage(v *string)
	Message() *string
}
type _UnrecoverableException struct {
	Ị_message *string `coral:"message" json:"message"`
}

func (this *_UnrecoverableException) Error() string {
	return __model__.ErrorMessage(this)
}
func (this *_UnrecoverableException) Message() *string {
	return this.Ị_message
}
func (this *_UnrecoverableException) SetMessage(v *string) {
	this.Ị_message = v
}
func NewUnrecoverableException() UnrecoverableException {
	return &_UnrecoverableException{}
}
func init() {
	var val UnrecoverableException
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("BobaLambda").Assembly("com.amazon.bobalambda").RegisterShape("UnrecoverableException", t, func() interface{} {
		return NewUnrecoverableException()
	})
}

//This exception is thrown when trying to cancel a bounty with a title too long.
type StreamTitleTooLongException interface {
	error
	SetMessage(v *string)
	Message() *string
}
type _StreamTitleTooLongException struct {
	UnrecoverableException
	Ị_message *string `coral:"message" json:"message"`
}

func (this *_StreamTitleTooLongException) Error() string {
	return __model__.ErrorMessage(this)
}
func (this *_StreamTitleTooLongException) Message() *string {
	return this.Ị_message
}
func (this *_StreamTitleTooLongException) SetMessage(v *string) {
	this.Ị_message = v
}
func NewStreamTitleTooLongException() StreamTitleTooLongException {
	return &_StreamTitleTooLongException{}
}
func init() {
	var val StreamTitleTooLongException
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("BobaLambda").Assembly("com.amazon.bobalambda").RegisterShape("StreamTitleTooLongException", t, func() interface{} {
		return NewStreamTitleTooLongException()
	})
}

type UpdateBountyProgressResponse interface {
	SetBounty(v Bounty)
	Bounty() Bounty
}
type _UpdateBountyProgressResponse struct {
	Ị_bounty Bounty `coral:"bounty" json:"bounty"`
}

func (this *_UpdateBountyProgressResponse) Bounty() Bounty {
	return this.Ị_bounty
}
func (this *_UpdateBountyProgressResponse) SetBounty(v Bounty) {
	this.Ị_bounty = v
}
func NewUpdateBountyProgressResponse() UpdateBountyProgressResponse {
	return &_UpdateBountyProgressResponse{}
}
func init() {
	var val UpdateBountyProgressResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("BobaLambda").Assembly("com.amazon.bobalambda").RegisterShape("UpdateBountyProgressResponse", t, func() interface{} {
		return NewUpdateBountyProgressResponse()
	})
}

type ReconcileBountyResponse interface {
	SetBounty(v Bounty)
	Bounty() Bounty
}
type _ReconcileBountyResponse struct {
	Ị_bounty Bounty `coral:"bounty" json:"bounty"`
}

func (this *_ReconcileBountyResponse) Bounty() Bounty {
	return this.Ị_bounty
}
func (this *_ReconcileBountyResponse) SetBounty(v Bounty) {
	this.Ị_bounty = v
}
func NewReconcileBountyResponse() ReconcileBountyResponse {
	return &_ReconcileBountyResponse{}
}
func init() {
	var val ReconcileBountyResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("BobaLambda").Assembly("com.amazon.bobalambda").RegisterShape("ReconcileBountyResponse", t, func() interface{} {
		return NewReconcileBountyResponse()
	})
}

type Video interface {
	SetTitle(v *string)
	Title() *string
	SetUrl(v *string)
	Url() *string
}
type _Video struct {
	Ị_title *string `coral:"title" json:"title"`
	Ị_url   *string `coral:"url" json:"url"`
}

func (this *_Video) Url() *string {
	return this.Ị_url
}
func (this *_Video) SetUrl(v *string) {
	this.Ị_url = v
}
func (this *_Video) Title() *string {
	return this.Ị_title
}
func (this *_Video) SetTitle(v *string) {
	this.Ị_title = v
}
func NewVideo() Video {
	return &_Video{}
}
func init() {
	var val Video
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("BobaLambda").Assembly("com.amazon.bobalambda").RegisterShape("Video", t, func() interface{} {
		return NewVideo()
	})
}

type CancelBountyRequest interface {
	SetForce(v *bool)
	Force() *bool
	SetTuid(v *string)
	Tuid() *string
	SetBountyId(v *string)
	BountyId() *string
	SetReason(v *string)
	Reason() *string
}
type _CancelBountyRequest struct {
	Ị_force    *bool   `coral:"force" json:"force"`
	Ị_tuid     *string `coral:"tuid" json:"tuid"`
	Ị_bountyId *string `coral:"bountyId" json:"bountyId"`
	Ị_reason   *string `coral:"reason" json:"reason"`
}

func (this *_CancelBountyRequest) Tuid() *string {
	return this.Ị_tuid
}
func (this *_CancelBountyRequest) SetTuid(v *string) {
	this.Ị_tuid = v
}
func (this *_CancelBountyRequest) BountyId() *string {
	return this.Ị_bountyId
}
func (this *_CancelBountyRequest) SetBountyId(v *string) {
	this.Ị_bountyId = v
}
func (this *_CancelBountyRequest) Reason() *string {
	return this.Ị_reason
}
func (this *_CancelBountyRequest) SetReason(v *string) {
	this.Ị_reason = v
}
func (this *_CancelBountyRequest) Force() *bool {
	return this.Ị_force
}
func (this *_CancelBountyRequest) SetForce(v *bool) {
	this.Ị_force = v
}
func NewCancelBountyRequest() CancelBountyRequest {
	return &_CancelBountyRequest{}
}
func init() {
	var val CancelBountyRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("BobaLambda").Assembly("com.amazon.bobalambda").RegisterShape("CancelBountyRequest", t, func() interface{} {
		return NewCancelBountyRequest()
	})
}

type CampaignTask interface {
	SetRestrictionType(v *string)
	RestrictionType() *string
	SetId(v *string)
	Id() *string
	SetTaskType(v *string)
	TaskType() *string
	SetPayoutModel(v *string)
	PayoutModel() *string
	SetPayoutCentsPerViewer(v *int64)
	PayoutCentsPerViewer() *int64
	SetPayoutRateId(v *string)
	PayoutRateId() *string
	SetBroadcasterPayoutMultiplier(v *float64)
	BroadcasterPayoutMultiplier() *float64
	SetMaxProgress(v *int64)
	MaxProgress() *int64
}
type _CampaignTask struct {
	Ị_payoutCentsPerViewer        *int64   `coral:"payoutCentsPerViewer" json:"payoutCentsPerViewer"`
	Ị_payoutRateId                *string  `coral:"payoutRateId" json:"payoutRateId"`
	Ị_broadcasterPayoutMultiplier *float64 `coral:"broadcasterPayoutMultiplier" json:"broadcasterPayoutMultiplier"`
	Ị_maxProgress                 *int64   `coral:"maxProgress" json:"maxProgress"`
	Ị_restrictionType             *string  `coral:"restrictionType" json:"restrictionType"`
	Ị_id                          *string  `coral:"id" json:"id"`
	Ị_taskType                    *string  `coral:"taskType" json:"taskType"`
	Ị_payoutModel                 *string  `coral:"payoutModel" json:"payoutModel"`
}

func (this *_CampaignTask) Id() *string {
	return this.Ị_id
}
func (this *_CampaignTask) SetId(v *string) {
	this.Ị_id = v
}
func (this *_CampaignTask) TaskType() *string {
	return this.Ị_taskType
}
func (this *_CampaignTask) SetTaskType(v *string) {
	this.Ị_taskType = v
}
func (this *_CampaignTask) PayoutModel() *string {
	return this.Ị_payoutModel
}
func (this *_CampaignTask) SetPayoutModel(v *string) {
	this.Ị_payoutModel = v
}
func (this *_CampaignTask) PayoutCentsPerViewer() *int64 {
	return this.Ị_payoutCentsPerViewer
}
func (this *_CampaignTask) SetPayoutCentsPerViewer(v *int64) {
	this.Ị_payoutCentsPerViewer = v
}
func (this *_CampaignTask) PayoutRateId() *string {
	return this.Ị_payoutRateId
}
func (this *_CampaignTask) SetPayoutRateId(v *string) {
	this.Ị_payoutRateId = v
}
func (this *_CampaignTask) BroadcasterPayoutMultiplier() *float64 {
	return this.Ị_broadcasterPayoutMultiplier
}
func (this *_CampaignTask) SetBroadcasterPayoutMultiplier(v *float64) {
	this.Ị_broadcasterPayoutMultiplier = v
}
func (this *_CampaignTask) MaxProgress() *int64 {
	return this.Ị_maxProgress
}
func (this *_CampaignTask) SetMaxProgress(v *int64) {
	this.Ị_maxProgress = v
}
func (this *_CampaignTask) RestrictionType() *string {
	return this.Ị_restrictionType
}
func (this *_CampaignTask) SetRestrictionType(v *string) {
	this.Ị_restrictionType = v
}
func NewCampaignTask() CampaignTask {
	return &_CampaignTask{}
}
func init() {
	var val CampaignTask
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("BobaLambda").Assembly("com.amazon.bobalambda").RegisterShape("CampaignTask", t, func() interface{} {
		return NewCampaignTask()
	})
}

type GetBountiesRequest interface {
	SetTuid(v *string)
	Tuid() *string
	SetStatus(v *string)
	Status() *string
	SetCampaignId(v *string)
	CampaignId() *string
	SetCursor(v *string)
	Cursor() *string
	SetLimit(v *int64)
	Limit() *int64
}
type _GetBountiesRequest struct {
	Ị_tuid       *string `coral:"tuid" json:"tuid"`
	Ị_status     *string `coral:"status" json:"status"`
	Ị_campaignId *string `coral:"campaignId" json:"campaignId"`
	Ị_cursor     *string `coral:"cursor" json:"cursor"`
	Ị_limit      *int64  `coral:"limit" json:"limit"`
}

func (this *_GetBountiesRequest) Tuid() *string {
	return this.Ị_tuid
}
func (this *_GetBountiesRequest) SetTuid(v *string) {
	this.Ị_tuid = v
}
func (this *_GetBountiesRequest) Status() *string {
	return this.Ị_status
}
func (this *_GetBountiesRequest) SetStatus(v *string) {
	this.Ị_status = v
}
func (this *_GetBountiesRequest) CampaignId() *string {
	return this.Ị_campaignId
}
func (this *_GetBountiesRequest) SetCampaignId(v *string) {
	this.Ị_campaignId = v
}
func (this *_GetBountiesRequest) Cursor() *string {
	return this.Ị_cursor
}
func (this *_GetBountiesRequest) SetCursor(v *string) {
	this.Ị_cursor = v
}
func (this *_GetBountiesRequest) Limit() *int64 {
	return this.Ị_limit
}
func (this *_GetBountiesRequest) SetLimit(v *int64) {
	this.Ị_limit = v
}
func NewGetBountiesRequest() GetBountiesRequest {
	return &_GetBountiesRequest{}
}
func init() {
	var val GetBountiesRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("BobaLambda").Assembly("com.amazon.bobalambda").RegisterShape("GetBountiesRequest", t, func() interface{} {
		return NewGetBountiesRequest()
	})
}

type UpdateBroadcasterSettingsRequest interface {
	SetSettingsList(v []Settings)
	SettingsList() []Settings
}
type _UpdateBroadcasterSettingsRequest struct {
	Ị_settingsList []Settings `coral:"settingsList" json:"settingsList"`
}

func (this *_UpdateBroadcasterSettingsRequest) SettingsList() []Settings {
	return this.Ị_settingsList
}
func (this *_UpdateBroadcasterSettingsRequest) SetSettingsList(v []Settings) {
	this.Ị_settingsList = v
}
func NewUpdateBroadcasterSettingsRequest() UpdateBroadcasterSettingsRequest {
	return &_UpdateBroadcasterSettingsRequest{}
}
func init() {
	var val UpdateBroadcasterSettingsRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("BobaLambda").Assembly("com.amazon.bobalambda").RegisterShape("UpdateBroadcasterSettingsRequest", t, func() interface{} {
		return NewUpdateBroadcasterSettingsRequest()
	})
}

type PublishSponsoredEventResponse interface {
	SetBounty(v Bounty)
	Bounty() Bounty
	SetWaitTimestamp(v *string)
	WaitTimestamp() *string
}
type _PublishSponsoredEventResponse struct {
	Ị_bounty        Bounty  `coral:"bounty" json:"bounty"`
	Ị_waitTimestamp *string `coral:"waitTimestamp" json:"waitTimestamp"`
}

func (this *_PublishSponsoredEventResponse) Bounty() Bounty {
	return this.Ị_bounty
}
func (this *_PublishSponsoredEventResponse) SetBounty(v Bounty) {
	this.Ị_bounty = v
}
func (this *_PublishSponsoredEventResponse) WaitTimestamp() *string {
	return this.Ị_waitTimestamp
}
func (this *_PublishSponsoredEventResponse) SetWaitTimestamp(v *string) {
	this.Ị_waitTimestamp = v
}
func NewPublishSponsoredEventResponse() PublishSponsoredEventResponse {
	return &_PublishSponsoredEventResponse{}
}
func init() {
	var val PublishSponsoredEventResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("BobaLambda").Assembly("com.amazon.bobalambda").RegisterShape("PublishSponsoredEventResponse", t, func() interface{} {
		return NewPublishSponsoredEventResponse()
	})
}

type DisplayInfo interface {
	SetDisplayName(v *string)
	DisplayName() *string
	SetBoxArtURL(v *string)
	BoxArtURL() *string
	SetCoverURL(v *string)
	CoverURL() *string
}
type _DisplayInfo struct {
	Ị_displayName *string `coral:"displayName" json:"displayName"`
	Ị_boxArtURL   *string `coral:"boxArtURL" json:"boxArtURL"`
	Ị_coverURL    *string `coral:"coverURL" json:"coverURL"`
}

func (this *_DisplayInfo) DisplayName() *string {
	return this.Ị_displayName
}
func (this *_DisplayInfo) SetDisplayName(v *string) {
	this.Ị_displayName = v
}
func (this *_DisplayInfo) BoxArtURL() *string {
	return this.Ị_boxArtURL
}
func (this *_DisplayInfo) SetBoxArtURL(v *string) {
	this.Ị_boxArtURL = v
}
func (this *_DisplayInfo) CoverURL() *string {
	return this.Ị_coverURL
}
func (this *_DisplayInfo) SetCoverURL(v *string) {
	this.Ị_coverURL = v
}
func NewDisplayInfo() DisplayInfo {
	return &_DisplayInfo{}
}
func init() {
	var val DisplayInfo
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("BobaLambda").Assembly("com.amazon.bobalambda").RegisterShape("DisplayInfo", t, func() interface{} {
		return NewDisplayInfo()
	})
}

type GetBountyEarningsRequest interface {
	SetStart(v *int64)
	Start() *int64
	SetEnd(v *int64)
	End() *int64
}
type _GetBountyEarningsRequest struct {
	Ị_start *int64 `coral:"start" json:"start"`
	Ị_end   *int64 `coral:"end" json:"end"`
}

func (this *_GetBountyEarningsRequest) Start() *int64 {
	return this.Ị_start
}
func (this *_GetBountyEarningsRequest) SetStart(v *int64) {
	this.Ị_start = v
}
func (this *_GetBountyEarningsRequest) End() *int64 {
	return this.Ị_end
}
func (this *_GetBountyEarningsRequest) SetEnd(v *int64) {
	this.Ị_end = v
}
func NewGetBountyEarningsRequest() GetBountyEarningsRequest {
	return &_GetBountyEarningsRequest{}
}
func init() {
	var val GetBountyEarningsRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("BobaLambda").Assembly("com.amazon.bobalambda").RegisterShape("GetBountyEarningsRequest", t, func() interface{} {
		return NewGetBountyEarningsRequest()
	})
}

type ModerateBountyRequest interface {
	SetBountyId(v *string)
	BountyId() *string
	SetModerationStatus(v *string)
	ModerationStatus() *string
}
type _ModerateBountyRequest struct {
	Ị_bountyId         *string `coral:"bountyId" json:"bountyId"`
	Ị_moderationStatus *string `coral:"moderationStatus" json:"moderationStatus"`
}

func (this *_ModerateBountyRequest) BountyId() *string {
	return this.Ị_bountyId
}
func (this *_ModerateBountyRequest) SetBountyId(v *string) {
	this.Ị_bountyId = v
}
func (this *_ModerateBountyRequest) ModerationStatus() *string {
	return this.Ị_moderationStatus
}
func (this *_ModerateBountyRequest) SetModerationStatus(v *string) {
	this.Ị_moderationStatus = v
}
func NewModerateBountyRequest() ModerateBountyRequest {
	return &_ModerateBountyRequest{}
}
func init() {
	var val ModerateBountyRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("BobaLambda").Assembly("com.amazon.bobalambda").RegisterShape("ModerateBountyRequest", t, func() interface{} {
		return NewModerateBountyRequest()
	})
}

//This exception is thrown when internal service encountering error.
type InternalServerException interface {
	error
	SetMessage(v *string)
	Message() *string
}
type _InternalServerException struct {
	UnrecoverableException
	Ị_message *string `coral:"message" json:"message"`
}

func (this *_InternalServerException) Error() string {
	return __model__.ErrorMessage(this)
}
func (this *_InternalServerException) Message() *string {
	return this.Ị_message
}
func (this *_InternalServerException) SetMessage(v *string) {
	this.Ị_message = v
}
func NewInternalServerException() InternalServerException {
	return &_InternalServerException{}
}
func init() {
	var val InternalServerException
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("BobaLambda").Assembly("com.amazon.bobalambda").RegisterShape("InternalServerException", t, func() interface{} {
		return NewInternalServerException()
	})
}

type GetBountyEarningsResponse interface {
	SetEarningsEvents(v []EarningsEvent)
	EarningsEvents() []EarningsEvent
}
type _GetBountyEarningsResponse struct {
	Ị_earningsEvents []EarningsEvent `coral:"earningsEvents" json:"earningsEvents"`
}

func (this *_GetBountyEarningsResponse) EarningsEvents() []EarningsEvent {
	return this.Ị_earningsEvents
}
func (this *_GetBountyEarningsResponse) SetEarningsEvents(v []EarningsEvent) {
	this.Ị_earningsEvents = v
}
func NewGetBountyEarningsResponse() GetBountyEarningsResponse {
	return &_GetBountyEarningsResponse{}
}
func init() {
	var val GetBountyEarningsResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("BobaLambda").Assembly("com.amazon.bobalambda").RegisterShape("GetBountyEarningsResponse", t, func() interface{} {
		return NewGetBountyEarningsResponse()
	})
}

type RefreshBroadcasterSettingsResponse interface {
	SetSettingsList(v []Settings)
	SettingsList() []Settings
}
type _RefreshBroadcasterSettingsResponse struct {
	Ị_settingsList []Settings `coral:"settingsList" json:"settingsList"`
}

func (this *_RefreshBroadcasterSettingsResponse) SettingsList() []Settings {
	return this.Ị_settingsList
}
func (this *_RefreshBroadcasterSettingsResponse) SetSettingsList(v []Settings) {
	this.Ị_settingsList = v
}
func NewRefreshBroadcasterSettingsResponse() RefreshBroadcasterSettingsResponse {
	return &_RefreshBroadcasterSettingsResponse{}
}
func init() {
	var val RefreshBroadcasterSettingsResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("BobaLambda").Assembly("com.amazon.bobalambda").RegisterShape("RefreshBroadcasterSettingsResponse", t, func() interface{} {
		return NewRefreshBroadcasterSettingsResponse()
	})
}

//This exception is thrown when there was bad input.
type InvalidParameterException interface {
	error
	SetMessage(v *string)
	Message() *string
}
type _InvalidParameterException struct {
	UnrecoverableException
	Ị_message *string `coral:"message" json:"message"`
}

func (this *_InvalidParameterException) Error() string {
	return __model__.ErrorMessage(this)
}
func (this *_InvalidParameterException) Message() *string {
	return this.Ị_message
}
func (this *_InvalidParameterException) SetMessage(v *string) {
	this.Ị_message = v
}
func NewInvalidParameterException() InvalidParameterException {
	return &_InvalidParameterException{}
}
func init() {
	var val InvalidParameterException
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("BobaLambda").Assembly("com.amazon.bobalambda").RegisterShape("InvalidParameterException", t, func() interface{} {
		return NewInvalidParameterException()
	})
}

type Campaign interface {
	SetTargetVarietyBroadcasters(v *bool)
	TargetVarietyBroadcasters() *bool
	SetAllowAllGames(v *bool)
	AllowAllGames() *bool
	SetTitle(v *string)
	Title() *string
	SetStartDate(v *int64)
	StartDate() *int64
	SetEndDate(v *int64)
	EndDate() *int64
	SetLastModified(v *int64)
	LastModified() *int64
	SetWhitelistedUsers(v []string)
	WhitelistedUsers() []string
	SetStreamLengthSeconds(v *int64)
	StreamLengthSeconds() *int64
	SetTargetedGames(v []string)
	TargetedGames() []string
	SetKeyRegions(v map[string][]string)
	KeyRegions() map[string][]string
	SetId(v *string)
	Id() *string
	SetGameId(v *string)
	GameId() *string
	SetTotalPayoutCents(v *int64)
	TotalPayoutCents() *int64
	SetTermsAndConditionsURL(v *string)
	TermsAndConditionsURL() *string
	SetAvailablePlatforms(v []string)
	AvailablePlatforms() []string
	SetTargetAllBroadcasters(v *bool)
	TargetAllBroadcasters() *bool
	SetSponsor(v *string)
	Sponsor() *string
	SetDisplayInfo(v DisplayInfo)
	DisplayInfo() DisplayInfo
	SetSpentBudgetCents(v *int64)
	SpentBudgetCents() *int64
	SetAllowedGames(v []string)
	AllowedGames() []string
	SetPromotionEligible(v *bool)
	PromotionEligible() *bool
	SetDetails(v *string)
	Details() *string
	SetBountyLengthDays(v *int64)
	BountyLengthDays() *int64
	SetTasks(v []CampaignTask)
	Tasks() []CampaignTask
	SetCallToActionDefinition(v CallToActionDefinition)
	CallToActionDefinition() CallToActionDefinition
}
type _Campaign struct {
	Ị_targetVarietyBroadcasters *bool                  `coral:"targetVarietyBroadcasters" json:"targetVarietyBroadcasters"`
	Ị_allowAllGames             *bool                  `coral:"allowAllGames" json:"allowAllGames"`
	Ị_title                     *string                `coral:"title" json:"title"`
	Ị_startDate                 *int64                 `coral:"startDate" json:"startDate"`
	Ị_endDate                   *int64                 `coral:"endDate" json:"endDate"`
	Ị_lastModified              *int64                 `coral:"lastModified" json:"lastModified"`
	Ị_whitelistedUsers          []string               `coral:"whitelistedUsers" json:"whitelistedUsers"`
	Ị_streamLengthSeconds       *int64                 `coral:"streamLengthSeconds" json:"streamLengthSeconds"`
	Ị_targetedGames             []string               `coral:"targetedGames" json:"targetedGames"`
	Ị_keyRegions                map[string][]string    `coral:"keyRegions" json:"keyRegions"`
	Ị_id                        *string                `coral:"id" json:"id"`
	Ị_gameId                    *string                `coral:"gameId" json:"gameId"`
	Ị_totalPayoutCents          *int64                 `coral:"totalPayoutCents" json:"totalPayoutCents"`
	Ị_termsAndConditionsURL     *string                `coral:"termsAndConditionsURL" json:"termsAndConditionsURL"`
	Ị_availablePlatforms        []string               `coral:"availablePlatforms" json:"availablePlatforms"`
	Ị_targetAllBroadcasters     *bool                  `coral:"targetAllBroadcasters" json:"targetAllBroadcasters"`
	Ị_sponsor                   *string                `coral:"sponsor" json:"sponsor"`
	Ị_displayInfo               DisplayInfo            `coral:"displayInfo" json:"displayInfo"`
	Ị_spentBudgetCents          *int64                 `coral:"spentBudgetCents" json:"spentBudgetCents"`
	Ị_allowedGames              []string               `coral:"allowedGames" json:"allowedGames"`
	Ị_promotionEligible         *bool                  `coral:"promotionEligible" json:"promotionEligible"`
	Ị_details                   *string                `coral:"details" json:"details"`
	Ị_bountyLengthDays          *int64                 `coral:"bountyLengthDays" json:"bountyLengthDays"`
	Ị_tasks                     []CampaignTask         `coral:"tasks" json:"tasks"`
	Ị_callToActionDefinition    CallToActionDefinition `coral:"callToActionDefinition" json:"callToActionDefinition"`
}

func (this *_Campaign) DisplayInfo() DisplayInfo {
	return this.Ị_displayInfo
}
func (this *_Campaign) SetDisplayInfo(v DisplayInfo) {
	this.Ị_displayInfo = v
}
func (this *_Campaign) SpentBudgetCents() *int64 {
	return this.Ị_spentBudgetCents
}
func (this *_Campaign) SetSpentBudgetCents(v *int64) {
	this.Ị_spentBudgetCents = v
}
func (this *_Campaign) AllowedGames() []string {
	return this.Ị_allowedGames
}
func (this *_Campaign) SetAllowedGames(v []string) {
	this.Ị_allowedGames = v
}
func (this *_Campaign) PromotionEligible() *bool {
	return this.Ị_promotionEligible
}
func (this *_Campaign) SetPromotionEligible(v *bool) {
	this.Ị_promotionEligible = v
}
func (this *_Campaign) Sponsor() *string {
	return this.Ị_sponsor
}
func (this *_Campaign) SetSponsor(v *string) {
	this.Ị_sponsor = v
}
func (this *_Campaign) BountyLengthDays() *int64 {
	return this.Ị_bountyLengthDays
}
func (this *_Campaign) SetBountyLengthDays(v *int64) {
	this.Ị_bountyLengthDays = v
}
func (this *_Campaign) Tasks() []CampaignTask {
	return this.Ị_tasks
}
func (this *_Campaign) SetTasks(v []CampaignTask) {
	this.Ị_tasks = v
}
func (this *_Campaign) CallToActionDefinition() CallToActionDefinition {
	return this.Ị_callToActionDefinition
}
func (this *_Campaign) SetCallToActionDefinition(v CallToActionDefinition) {
	this.Ị_callToActionDefinition = v
}
func (this *_Campaign) Details() *string {
	return this.Ị_details
}
func (this *_Campaign) SetDetails(v *string) {
	this.Ị_details = v
}
func (this *_Campaign) StartDate() *int64 {
	return this.Ị_startDate
}
func (this *_Campaign) SetStartDate(v *int64) {
	this.Ị_startDate = v
}
func (this *_Campaign) EndDate() *int64 {
	return this.Ị_endDate
}
func (this *_Campaign) SetEndDate(v *int64) {
	this.Ị_endDate = v
}
func (this *_Campaign) LastModified() *int64 {
	return this.Ị_lastModified
}
func (this *_Campaign) SetLastModified(v *int64) {
	this.Ị_lastModified = v
}
func (this *_Campaign) WhitelistedUsers() []string {
	return this.Ị_whitelistedUsers
}
func (this *_Campaign) SetWhitelistedUsers(v []string) {
	this.Ị_whitelistedUsers = v
}
func (this *_Campaign) StreamLengthSeconds() *int64 {
	return this.Ị_streamLengthSeconds
}
func (this *_Campaign) SetStreamLengthSeconds(v *int64) {
	this.Ị_streamLengthSeconds = v
}
func (this *_Campaign) TargetVarietyBroadcasters() *bool {
	return this.Ị_targetVarietyBroadcasters
}
func (this *_Campaign) SetTargetVarietyBroadcasters(v *bool) {
	this.Ị_targetVarietyBroadcasters = v
}
func (this *_Campaign) AllowAllGames() *bool {
	return this.Ị_allowAllGames
}
func (this *_Campaign) SetAllowAllGames(v *bool) {
	this.Ị_allowAllGames = v
}
func (this *_Campaign) Title() *string {
	return this.Ị_title
}
func (this *_Campaign) SetTitle(v *string) {
	this.Ị_title = v
}
func (this *_Campaign) GameId() *string {
	return this.Ị_gameId
}
func (this *_Campaign) SetGameId(v *string) {
	this.Ị_gameId = v
}
func (this *_Campaign) TotalPayoutCents() *int64 {
	return this.Ị_totalPayoutCents
}
func (this *_Campaign) SetTotalPayoutCents(v *int64) {
	this.Ị_totalPayoutCents = v
}
func (this *_Campaign) TermsAndConditionsURL() *string {
	return this.Ị_termsAndConditionsURL
}
func (this *_Campaign) SetTermsAndConditionsURL(v *string) {
	this.Ị_termsAndConditionsURL = v
}
func (this *_Campaign) AvailablePlatforms() []string {
	return this.Ị_availablePlatforms
}
func (this *_Campaign) SetAvailablePlatforms(v []string) {
	this.Ị_availablePlatforms = v
}
func (this *_Campaign) TargetAllBroadcasters() *bool {
	return this.Ị_targetAllBroadcasters
}
func (this *_Campaign) SetTargetAllBroadcasters(v *bool) {
	this.Ị_targetAllBroadcasters = v
}
func (this *_Campaign) TargetedGames() []string {
	return this.Ị_targetedGames
}
func (this *_Campaign) SetTargetedGames(v []string) {
	this.Ị_targetedGames = v
}
func (this *_Campaign) KeyRegions() map[string][]string {
	return this.Ị_keyRegions
}
func (this *_Campaign) SetKeyRegions(v map[string][]string) {
	this.Ị_keyRegions = v
}
func (this *_Campaign) Id() *string {
	return this.Ị_id
}
func (this *_Campaign) SetId(v *string) {
	this.Ị_id = v
}
func NewCampaign() Campaign {
	return &_Campaign{}
}
func init() {
	var val Campaign
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("BobaLambda").Assembly("com.amazon.bobalambda").RegisterShape("Campaign", t, func() interface{} {
		return NewCampaign()
	})
}

type StartBountyResponse interface {
	SetBounty(v Bounty)
	Bounty() Bounty
}
type _StartBountyResponse struct {
	Ị_bounty Bounty `coral:"bounty" json:"bounty"`
}

func (this *_StartBountyResponse) Bounty() Bounty {
	return this.Ị_bounty
}
func (this *_StartBountyResponse) SetBounty(v Bounty) {
	this.Ị_bounty = v
}
func NewStartBountyResponse() StartBountyResponse {
	return &_StartBountyResponse{}
}
func init() {
	var val StartBountyResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("BobaLambda").Assembly("com.amazon.bobalambda").RegisterShape("StartBountyResponse", t, func() interface{} {
		return NewStartBountyResponse()
	})
}

type SponsoredSessionsAuditResponse interface {
	SetBestSession(v *string)
	BestSession() *string
	SetLastEventEndTime(v *int64)
	LastEventEndTime() *int64
}
type _SponsoredSessionsAuditResponse struct {
	Ị_lastEventEndTime *int64  `coral:"lastEventEndTime" json:"lastEventEndTime"`
	Ị_bestSession      *string `coral:"bestSession" json:"bestSession"`
}

func (this *_SponsoredSessionsAuditResponse) LastEventEndTime() *int64 {
	return this.Ị_lastEventEndTime
}
func (this *_SponsoredSessionsAuditResponse) SetLastEventEndTime(v *int64) {
	this.Ị_lastEventEndTime = v
}
func (this *_SponsoredSessionsAuditResponse) BestSession() *string {
	return this.Ị_bestSession
}
func (this *_SponsoredSessionsAuditResponse) SetBestSession(v *string) {
	this.Ị_bestSession = v
}
func NewSponsoredSessionsAuditResponse() SponsoredSessionsAuditResponse {
	return &_SponsoredSessionsAuditResponse{}
}
func init() {
	var val SponsoredSessionsAuditResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("BobaLambda").Assembly("com.amazon.bobalambda").RegisterShape("SponsoredSessionsAuditResponse", t, func() interface{} {
		return NewSponsoredSessionsAuditResponse()
	})
}

type Bounty interface {
	SetIsReconciled(v *bool)
	IsReconciled() *bool
	SetActualUniqueDevices(v *int64)
	ActualUniqueDevices() *int64
	SetActualUniqueDevicesTwoMinutesWatched(v *int64)
	ActualUniqueDevicesTwoMinutesWatched() *int64
	SetModerationFailReason(v *string)
	ModerationFailReason() *string
	SetMaxTwitchMarginRounded(v *int64)
	MaxTwitchMarginRounded() *int64
	SetTwitchMarginRate(v *float64)
	TwitchMarginRate() *float64
	SetActualUniqueDevicesThirtySecondsWatched(v *int64)
	ActualUniqueDevicesThirtySecondsWatched() *int64
	SetActualUniqueDevicesOneMinuteWatched(v *int64)
	ActualUniqueDevicesOneMinuteWatched() *int64
	SetActualMinutesWatched(v *int64)
	ActualMinutesWatched() *int64
	SetSponsoredEventStepFunctionIds(v []string)
	SponsoredEventStepFunctionIds() []string
	SetStatusLastUpdated(v *string)
	StatusLastUpdated() *string
	SetBroadcasterPayoutRate(v *float64)
	BroadcasterPayoutRate() *float64
	SetCampaign(v Campaign)
	Campaign() Campaign
	SetObservedCCU(v *int64)
	ObservedCCU() *int64
	SetStreamTitles(v *string)
	StreamTitles() *string
	SetId(v *string)
	Id() *string
	SetStartDate(v *int64)
	StartDate() *int64
	SetProgressCompletedDate(v *int64)
	ProgressCompletedDate() *int64
	SetCancelReason(v *string)
	CancelReason() *string
	SetActualPayout(v *int64)
	ActualPayout() *int64
	SetActualBudgetConsumedCents(v *int64)
	ActualBudgetConsumedCents() *int64
	SetMinPayoutCCU(v *int64)
	MinPayoutCCU() *int64
	SetTasks(v []BountyTask)
	Tasks() []BountyTask
	SetUserId(v *string)
	UserId() *string
	SetLastModeratedTime(v *int64)
	LastModeratedTime() *int64
	SetIsSponsoredEventsPublishingCompleted(v *bool)
	IsSponsoredEventsPublishingCompleted() *bool
	SetPlatform(v *string)
	Platform() *string
	SetLastSponsoredEventUpdateTime(v *int64)
	LastSponsoredEventUpdateTime() *int64
	SetVideos(v []Video)
	Videos() []Video
	SetCallToAction(v CallToAction)
	CallToAction() CallToAction
	SetEndDate(v *int64)
	EndDate() *int64
	SetMaxPayoutRounded(v *int64)
	MaxPayoutRounded() *int64
	SetEstimatedUniqueDevices(v *int64)
	EstimatedUniqueDevices() *int64
	SetActualTwitchMargin(v *int64)
	ActualTwitchMargin() *int64
	SetMaxPayoutCCU(v *int64)
	MaxPayoutCCU() *int64
	SetChatCallToActionStepFunctionIds(v []string)
	ChatCallToActionStepFunctionIds() []string
	SetStatus(v *string)
	Status() *string
	SetModerationStatus(v *string)
	ModerationStatus() *string
	SetMaxPayout(v *int64)
	MaxPayout() *int64
	SetMaxUniqueDevices(v *int64)
	MaxUniqueDevices() *int64
	SetRegion(v *string)
	Region() *string
	SetKeys(v []Key)
	Keys() []Key
	SetLastUpdated(v *int64)
	LastUpdated() *int64
}
type _Bounty struct {
	Ị_sponsoredEventStepFunctionIds           []string     `coral:"sponsoredEventStepFunctionIds" json:"sponsoredEventStepFunctionIds"`
	Ị_statusLastUpdated                       *string      `coral:"statusLastUpdated" json:"statusLastUpdated"`
	Ị_broadcasterPayoutRate                   *float64     `coral:"broadcasterPayoutRate" json:"broadcasterPayoutRate"`
	Ị_twitchMarginRate                        *float64     `coral:"twitchMarginRate" json:"twitchMarginRate"`
	Ị_actualUniqueDevicesThirtySecondsWatched *int64       `coral:"actualUniqueDevicesThirtySecondsWatched" json:"actualUniqueDevicesThirtySecondsWatched"`
	Ị_actualUniqueDevicesOneMinuteWatched     *int64       `coral:"actualUniqueDevicesOneMinuteWatched" json:"actualUniqueDevicesOneMinuteWatched"`
	Ị_actualMinutesWatched                    *int64       `coral:"actualMinutesWatched" json:"actualMinutesWatched"`
	Ị_id                                      *string      `coral:"id" json:"id"`
	Ị_campaign                                Campaign     `coral:"campaign" json:"campaign"`
	Ị_observedCCU                             *int64       `coral:"observedCCU" json:"observedCCU"`
	Ị_streamTitles                            *string      `coral:"streamTitles" json:"streamTitles"`
	Ị_actualBudgetConsumedCents               *int64       `coral:"actualBudgetConsumedCents" json:"actualBudgetConsumedCents"`
	Ị_minPayoutCCU                            *int64       `coral:"minPayoutCCU" json:"minPayoutCCU"`
	Ị_tasks                                   []BountyTask `coral:"tasks" json:"tasks"`
	Ị_userId                                  *string      `coral:"userId" json:"userId"`
	Ị_startDate                               *int64       `coral:"startDate" json:"startDate"`
	Ị_progressCompletedDate                   *int64       `coral:"progressCompletedDate" json:"progressCompletedDate"`
	Ị_cancelReason                            *string      `coral:"cancelReason" json:"cancelReason"`
	Ị_actualPayout                            *int64       `coral:"actualPayout" json:"actualPayout"`
	Ị_isSponsoredEventsPublishingCompleted    *bool        `coral:"isSponsoredEventsPublishingCompleted" json:"isSponsoredEventsPublishingCompleted"`
	Ị_lastModeratedTime                       *int64       `coral:"lastModeratedTime" json:"lastModeratedTime"`
	Ị_endDate                                 *int64       `coral:"endDate" json:"endDate"`
	Ị_platform                                *string      `coral:"platform" json:"platform"`
	Ị_lastSponsoredEventUpdateTime            *int64       `coral:"lastSponsoredEventUpdateTime" json:"lastSponsoredEventUpdateTime"`
	Ị_videos                                  []Video      `coral:"videos" json:"videos"`
	Ị_callToAction                            CallToAction `coral:"callToAction" json:"callToAction"`
	Ị_chatCallToActionStepFunctionIds         []string     `coral:"chatCallToActionStepFunctionIds" json:"chatCallToActionStepFunctionIds"`
	Ị_status                                  *string      `coral:"status" json:"status"`
	Ị_maxPayoutRounded                        *int64       `coral:"maxPayoutRounded" json:"maxPayoutRounded"`
	Ị_estimatedUniqueDevices                  *int64       `coral:"estimatedUniqueDevices" json:"estimatedUniqueDevices"`
	Ị_actualTwitchMargin                      *int64       `coral:"actualTwitchMargin" json:"actualTwitchMargin"`
	Ị_maxPayoutCCU                            *int64       `coral:"maxPayoutCCU" json:"maxPayoutCCU"`
	Ị_moderationStatus                        *string      `coral:"moderationStatus" json:"moderationStatus"`
	Ị_lastUpdated                             *int64       `coral:"lastUpdated" json:"lastUpdated"`
	Ị_maxPayout                               *int64       `coral:"maxPayout" json:"maxPayout"`
	Ị_maxUniqueDevices                        *int64       `coral:"maxUniqueDevices" json:"maxUniqueDevices"`
	Ị_region                                  *string      `coral:"region" json:"region"`
	Ị_keys                                    []Key        `coral:"keys" json:"keys"`
	Ị_maxTwitchMarginRounded                  *int64       `coral:"maxTwitchMarginRounded" json:"maxTwitchMarginRounded"`
	Ị_isReconciled                            *bool        `coral:"isReconciled" json:"isReconciled"`
	Ị_actualUniqueDevices                     *int64       `coral:"actualUniqueDevices" json:"actualUniqueDevices"`
	Ị_actualUniqueDevicesTwoMinutesWatched    *int64       `coral:"actualUniqueDevicesTwoMinutesWatched" json:"actualUniqueDevicesTwoMinutesWatched"`
	Ị_moderationFailReason                    *string      `coral:"moderationFailReason" json:"moderationFailReason"`
}

func (this *_Bounty) Campaign() Campaign {
	return this.Ị_campaign
}
func (this *_Bounty) SetCampaign(v Campaign) {
	this.Ị_campaign = v
}
func (this *_Bounty) ObservedCCU() *int64 {
	return this.Ị_observedCCU
}
func (this *_Bounty) SetObservedCCU(v *int64) {
	this.Ị_observedCCU = v
}
func (this *_Bounty) StreamTitles() *string {
	return this.Ị_streamTitles
}
func (this *_Bounty) SetStreamTitles(v *string) {
	this.Ị_streamTitles = v
}
func (this *_Bounty) Id() *string {
	return this.Ị_id
}
func (this *_Bounty) SetId(v *string) {
	this.Ị_id = v
}
func (this *_Bounty) StartDate() *int64 {
	return this.Ị_startDate
}
func (this *_Bounty) SetStartDate(v *int64) {
	this.Ị_startDate = v
}
func (this *_Bounty) ProgressCompletedDate() *int64 {
	return this.Ị_progressCompletedDate
}
func (this *_Bounty) SetProgressCompletedDate(v *int64) {
	this.Ị_progressCompletedDate = v
}
func (this *_Bounty) CancelReason() *string {
	return this.Ị_cancelReason
}
func (this *_Bounty) SetCancelReason(v *string) {
	this.Ị_cancelReason = v
}
func (this *_Bounty) ActualPayout() *int64 {
	return this.Ị_actualPayout
}
func (this *_Bounty) SetActualPayout(v *int64) {
	this.Ị_actualPayout = v
}
func (this *_Bounty) ActualBudgetConsumedCents() *int64 {
	return this.Ị_actualBudgetConsumedCents
}
func (this *_Bounty) SetActualBudgetConsumedCents(v *int64) {
	this.Ị_actualBudgetConsumedCents = v
}
func (this *_Bounty) MinPayoutCCU() *int64 {
	return this.Ị_minPayoutCCU
}
func (this *_Bounty) SetMinPayoutCCU(v *int64) {
	this.Ị_minPayoutCCU = v
}
func (this *_Bounty) Tasks() []BountyTask {
	return this.Ị_tasks
}
func (this *_Bounty) SetTasks(v []BountyTask) {
	this.Ị_tasks = v
}
func (this *_Bounty) UserId() *string {
	return this.Ị_userId
}
func (this *_Bounty) SetUserId(v *string) {
	this.Ị_userId = v
}
func (this *_Bounty) LastModeratedTime() *int64 {
	return this.Ị_lastModeratedTime
}
func (this *_Bounty) SetLastModeratedTime(v *int64) {
	this.Ị_lastModeratedTime = v
}
func (this *_Bounty) IsSponsoredEventsPublishingCompleted() *bool {
	return this.Ị_isSponsoredEventsPublishingCompleted
}
func (this *_Bounty) SetIsSponsoredEventsPublishingCompleted(v *bool) {
	this.Ị_isSponsoredEventsPublishingCompleted = v
}
func (this *_Bounty) Platform() *string {
	return this.Ị_platform
}
func (this *_Bounty) SetPlatform(v *string) {
	this.Ị_platform = v
}
func (this *_Bounty) LastSponsoredEventUpdateTime() *int64 {
	return this.Ị_lastSponsoredEventUpdateTime
}
func (this *_Bounty) SetLastSponsoredEventUpdateTime(v *int64) {
	this.Ị_lastSponsoredEventUpdateTime = v
}
func (this *_Bounty) Videos() []Video {
	return this.Ị_videos
}
func (this *_Bounty) SetVideos(v []Video) {
	this.Ị_videos = v
}
func (this *_Bounty) CallToAction() CallToAction {
	return this.Ị_callToAction
}
func (this *_Bounty) SetCallToAction(v CallToAction) {
	this.Ị_callToAction = v
}
func (this *_Bounty) EndDate() *int64 {
	return this.Ị_endDate
}
func (this *_Bounty) SetEndDate(v *int64) {
	this.Ị_endDate = v
}
func (this *_Bounty) MaxPayoutRounded() *int64 {
	return this.Ị_maxPayoutRounded
}
func (this *_Bounty) SetMaxPayoutRounded(v *int64) {
	this.Ị_maxPayoutRounded = v
}
func (this *_Bounty) EstimatedUniqueDevices() *int64 {
	return this.Ị_estimatedUniqueDevices
}
func (this *_Bounty) SetEstimatedUniqueDevices(v *int64) {
	this.Ị_estimatedUniqueDevices = v
}
func (this *_Bounty) ActualTwitchMargin() *int64 {
	return this.Ị_actualTwitchMargin
}
func (this *_Bounty) SetActualTwitchMargin(v *int64) {
	this.Ị_actualTwitchMargin = v
}
func (this *_Bounty) MaxPayoutCCU() *int64 {
	return this.Ị_maxPayoutCCU
}
func (this *_Bounty) SetMaxPayoutCCU(v *int64) {
	this.Ị_maxPayoutCCU = v
}
func (this *_Bounty) ChatCallToActionStepFunctionIds() []string {
	return this.Ị_chatCallToActionStepFunctionIds
}
func (this *_Bounty) SetChatCallToActionStepFunctionIds(v []string) {
	this.Ị_chatCallToActionStepFunctionIds = v
}
func (this *_Bounty) Status() *string {
	return this.Ị_status
}
func (this *_Bounty) SetStatus(v *string) {
	this.Ị_status = v
}
func (this *_Bounty) ModerationStatus() *string {
	return this.Ị_moderationStatus
}
func (this *_Bounty) SetModerationStatus(v *string) {
	this.Ị_moderationStatus = v
}
func (this *_Bounty) MaxPayout() *int64 {
	return this.Ị_maxPayout
}
func (this *_Bounty) SetMaxPayout(v *int64) {
	this.Ị_maxPayout = v
}
func (this *_Bounty) MaxUniqueDevices() *int64 {
	return this.Ị_maxUniqueDevices
}
func (this *_Bounty) SetMaxUniqueDevices(v *int64) {
	this.Ị_maxUniqueDevices = v
}
func (this *_Bounty) Region() *string {
	return this.Ị_region
}
func (this *_Bounty) SetRegion(v *string) {
	this.Ị_region = v
}
func (this *_Bounty) Keys() []Key {
	return this.Ị_keys
}
func (this *_Bounty) SetKeys(v []Key) {
	this.Ị_keys = v
}
func (this *_Bounty) LastUpdated() *int64 {
	return this.Ị_lastUpdated
}
func (this *_Bounty) SetLastUpdated(v *int64) {
	this.Ị_lastUpdated = v
}
func (this *_Bounty) IsReconciled() *bool {
	return this.Ị_isReconciled
}
func (this *_Bounty) SetIsReconciled(v *bool) {
	this.Ị_isReconciled = v
}
func (this *_Bounty) ActualUniqueDevices() *int64 {
	return this.Ị_actualUniqueDevices
}
func (this *_Bounty) SetActualUniqueDevices(v *int64) {
	this.Ị_actualUniqueDevices = v
}
func (this *_Bounty) ActualUniqueDevicesTwoMinutesWatched() *int64 {
	return this.Ị_actualUniqueDevicesTwoMinutesWatched
}
func (this *_Bounty) SetActualUniqueDevicesTwoMinutesWatched(v *int64) {
	this.Ị_actualUniqueDevicesTwoMinutesWatched = v
}
func (this *_Bounty) ModerationFailReason() *string {
	return this.Ị_moderationFailReason
}
func (this *_Bounty) SetModerationFailReason(v *string) {
	this.Ị_moderationFailReason = v
}
func (this *_Bounty) MaxTwitchMarginRounded() *int64 {
	return this.Ị_maxTwitchMarginRounded
}
func (this *_Bounty) SetMaxTwitchMarginRounded(v *int64) {
	this.Ị_maxTwitchMarginRounded = v
}
func (this *_Bounty) TwitchMarginRate() *float64 {
	return this.Ị_twitchMarginRate
}
func (this *_Bounty) SetTwitchMarginRate(v *float64) {
	this.Ị_twitchMarginRate = v
}
func (this *_Bounty) ActualUniqueDevicesThirtySecondsWatched() *int64 {
	return this.Ị_actualUniqueDevicesThirtySecondsWatched
}
func (this *_Bounty) SetActualUniqueDevicesThirtySecondsWatched(v *int64) {
	this.Ị_actualUniqueDevicesThirtySecondsWatched = v
}
func (this *_Bounty) ActualUniqueDevicesOneMinuteWatched() *int64 {
	return this.Ị_actualUniqueDevicesOneMinuteWatched
}
func (this *_Bounty) SetActualUniqueDevicesOneMinuteWatched(v *int64) {
	this.Ị_actualUniqueDevicesOneMinuteWatched = v
}
func (this *_Bounty) ActualMinutesWatched() *int64 {
	return this.Ị_actualMinutesWatched
}
func (this *_Bounty) SetActualMinutesWatched(v *int64) {
	this.Ị_actualMinutesWatched = v
}
func (this *_Bounty) SponsoredEventStepFunctionIds() []string {
	return this.Ị_sponsoredEventStepFunctionIds
}
func (this *_Bounty) SetSponsoredEventStepFunctionIds(v []string) {
	this.Ị_sponsoredEventStepFunctionIds = v
}
func (this *_Bounty) StatusLastUpdated() *string {
	return this.Ị_statusLastUpdated
}
func (this *_Bounty) SetStatusLastUpdated(v *string) {
	this.Ị_statusLastUpdated = v
}
func (this *_Bounty) BroadcasterPayoutRate() *float64 {
	return this.Ị_broadcasterPayoutRate
}
func (this *_Bounty) SetBroadcasterPayoutRate(v *float64) {
	this.Ị_broadcasterPayoutRate = v
}
func NewBounty() Bounty {
	return &_Bounty{}
}
func init() {
	var val Bounty
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("BobaLambda").Assembly("com.amazon.bobalambda").RegisterShape("Bounty", t, func() interface{} {
		return NewBounty()
	})
}

type Restriction interface {
	SetEndTime(v *int32)
	EndTime() *int32
	SetDays(v []int32)
	Days() []int32
	SetStartTime(v *int32)
	StartTime() *int32
}
type _Restriction struct {
	Ị_days      []int32 `coral:"days" json:"days"`
	Ị_startTime *int32  `coral:"startTime" json:"startTime"`
	Ị_endTime   *int32  `coral:"endTime" json:"endTime"`
}

func (this *_Restriction) Days() []int32 {
	return this.Ị_days
}
func (this *_Restriction) SetDays(v []int32) {
	this.Ị_days = v
}
func (this *_Restriction) StartTime() *int32 {
	return this.Ị_startTime
}
func (this *_Restriction) SetStartTime(v *int32) {
	this.Ị_startTime = v
}
func (this *_Restriction) EndTime() *int32 {
	return this.Ị_endTime
}
func (this *_Restriction) SetEndTime(v *int32) {
	this.Ị_endTime = v
}
func NewRestriction() Restriction {
	return &_Restriction{}
}
func init() {
	var val Restriction
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("BobaLambda").Assembly("com.amazon.bobalambda").RegisterShape("Restriction", t, func() interface{} {
		return NewRestriction()
	})
}

type ClaimBountyRequest interface {
	SetTuid(v *string)
	Tuid() *string
	SetBountyId(v *string)
	BountyId() *string
	SetCampaignId(v *string)
	CampaignId() *string
	SetPlatform(v *string)
	Platform() *string
	SetRegion(v *string)
	Region() *string
}
type _ClaimBountyRequest struct {
	Ị_tuid       *string `coral:"tuid" json:"tuid"`
	Ị_bountyId   *string `coral:"bountyId" json:"bountyId"`
	Ị_campaignId *string `coral:"campaignId" json:"campaignId"`
	Ị_platform   *string `coral:"platform" json:"platform"`
	Ị_region     *string `coral:"region" json:"region"`
}

func (this *_ClaimBountyRequest) CampaignId() *string {
	return this.Ị_campaignId
}
func (this *_ClaimBountyRequest) SetCampaignId(v *string) {
	this.Ị_campaignId = v
}
func (this *_ClaimBountyRequest) Platform() *string {
	return this.Ị_platform
}
func (this *_ClaimBountyRequest) SetPlatform(v *string) {
	this.Ị_platform = v
}
func (this *_ClaimBountyRequest) Region() *string {
	return this.Ị_region
}
func (this *_ClaimBountyRequest) SetRegion(v *string) {
	this.Ị_region = v
}
func (this *_ClaimBountyRequest) Tuid() *string {
	return this.Ị_tuid
}
func (this *_ClaimBountyRequest) SetTuid(v *string) {
	this.Ị_tuid = v
}
func (this *_ClaimBountyRequest) BountyId() *string {
	return this.Ị_bountyId
}
func (this *_ClaimBountyRequest) SetBountyId(v *string) {
	this.Ị_bountyId = v
}
func NewClaimBountyRequest() ClaimBountyRequest {
	return &_ClaimBountyRequest{}
}
func init() {
	var val ClaimBountyRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("BobaLambda").Assembly("com.amazon.bobalambda").RegisterShape("ClaimBountyRequest", t, func() interface{} {
		return NewClaimBountyRequest()
	})
}

type SponsoredSessionsAuditRequest interface {
	SetBountyId(v *string)
	BountyId() *string
}
type _SponsoredSessionsAuditRequest struct {
	Ị_bountyId *string `coral:"bountyId" json:"bountyId"`
}

func (this *_SponsoredSessionsAuditRequest) BountyId() *string {
	return this.Ị_bountyId
}
func (this *_SponsoredSessionsAuditRequest) SetBountyId(v *string) {
	this.Ị_bountyId = v
}
func NewSponsoredSessionsAuditRequest() SponsoredSessionsAuditRequest {
	return &_SponsoredSessionsAuditRequest{}
}
func init() {
	var val SponsoredSessionsAuditRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("BobaLambda").Assembly("com.amazon.bobalambda").RegisterShape("SponsoredSessionsAuditRequest", t, func() interface{} {
		return NewSponsoredSessionsAuditRequest()
	})
}

//This exception is thrown when retrying may help. e.g. timeout downstream.
type RecoverableException interface {
	error
	SetMessage(v *string)
	Message() *string
}
type _RecoverableException struct {
	Ị_message *string `coral:"message" json:"message"`
}

func (this *_RecoverableException) Error() string {
	return __model__.ErrorMessage(this)
}
func (this *_RecoverableException) Message() *string {
	return this.Ị_message
}
func (this *_RecoverableException) SetMessage(v *string) {
	this.Ị_message = v
}
func NewRecoverableException() RecoverableException {
	return &_RecoverableException{}
}
func init() {
	var val RecoverableException
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("BobaLambda").Assembly("com.amazon.bobalambda").RegisterShape("RecoverableException", t, func() interface{} {
		return NewRecoverableException()
	})
}

//This exception is thrown when trying to cancel a bounty that isn't live.
type BountyNotLiveException interface {
	error
	SetMessage(v *string)
	Message() *string
}
type _BountyNotLiveException struct {
	UnrecoverableException
	Ị_message *string `coral:"message" json:"message"`
}

func (this *_BountyNotLiveException) Error() string {
	return __model__.ErrorMessage(this)
}
func (this *_BountyNotLiveException) Message() *string {
	return this.Ị_message
}
func (this *_BountyNotLiveException) SetMessage(v *string) {
	this.Ị_message = v
}
func NewBountyNotLiveException() BountyNotLiveException {
	return &_BountyNotLiveException{}
}
func init() {
	var val BountyNotLiveException
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("BobaLambda").Assembly("com.amazon.bobalambda").RegisterShape("BountyNotLiveException", t, func() interface{} {
		return NewBountyNotLiveException()
	})
}

type ModerateBountyResponse interface {
	SetBounty(v Bounty)
	Bounty() Bounty
}
type _ModerateBountyResponse struct {
	Ị_bounty Bounty `coral:"bounty" json:"bounty"`
}

func (this *_ModerateBountyResponse) Bounty() Bounty {
	return this.Ị_bounty
}
func (this *_ModerateBountyResponse) SetBounty(v Bounty) {
	this.Ị_bounty = v
}
func NewModerateBountyResponse() ModerateBountyResponse {
	return &_ModerateBountyResponse{}
}
func init() {
	var val ModerateBountyResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("BobaLambda").Assembly("com.amazon.bobalambda").RegisterShape("ModerateBountyResponse", t, func() interface{} {
		return NewModerateBountyResponse()
	})
}
func init() {
	var val map[string]CallToAction
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to map[string]CallToAction
		if f, ok := from.Interface().(map[string]CallToAction); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to CallToActionMap")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val map[string][]string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to map[string][]string
		if f, ok := from.Interface().(map[string][]string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to StringListMap")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val []Key
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to []Key
		if f, ok := from.Interface().([]Key); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to Keys")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val []EarningsEvent
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to []EarningsEvent
		if f, ok := from.Interface().([]EarningsEvent); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to EarningsEvents")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val []string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to []string
		if f, ok := from.Interface().([]string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to StringList")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val []int64
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to []int64
		if f, ok := from.Interface().([]int64); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to LongList")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val []BountyReport
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to []BountyReport
		if f, ok := from.Interface().([]BountyReport); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to BountiesReport")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val []Settings
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to []Settings
		if f, ok := from.Interface().([]Settings); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to SettingsList")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val []CampaignTask
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to []CampaignTask
		if f, ok := from.Interface().([]CampaignTask); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to CampaignTasks")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val []Bounty
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to []Bounty
		if f, ok := from.Interface().([]Bounty); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to Bounties")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val []Video
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to []Video
		if f, ok := from.Interface().([]Video); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to VideoList")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val []BountyTask
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to []BountyTask
		if f, ok := from.Interface().([]BountyTask); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to BountyTasks")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val []int32
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to []int32
		if f, ok := from.Interface().([]int32); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to Days")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}

//The bounty board service.
type BobaLambda interface { //Checks a bounty's progress and updates its status.
	CheckBountyProgress(input CheckBountyProgressRequest) (CheckBountyProgressResponse, error)                      //Retrieves the list of bounties which match the given arguments.
	GetBounties(input GetBountiesRequest) (GetBountiesResponse, error)                                              //Update bounty progress with bounty ID.
	UpdateBountyProgress(input UpdateBountyProgressRequest) (UpdateBountyProgressResponse, error)                   //Take a campaign and refresh broadcaster settings in the Settings table.
	RefreshBroadcasterSettings(input RefreshBroadcasterSettingsRequest) (RefreshBroadcasterSettingsResponse, error) //Cancels a bounty for a Twitch User for a particular bounty ID.
	CancelBounty(input CancelBountyRequest) (CancelBountyResponse, error)                                           //Takes a list of broadcasters and updates them in the Settings table.
	UpdateBroadcasterSettings(input UpdateBroadcasterSettingsRequest) (UpdateBroadcasterSettingsResponse, error)    //Fetches all the earnings related to Bounty Board in a time frame.
	GetBountyEarnings(input GetBountyEarningsRequest) (GetBountyEarningsResponse, error)                            //Fetches the bounties report for a campaignID in Bounty Board.
	GetBountiesReport(input GetBountiesReportRequest) (GetBountiesReportResponse, error)                            //Update the moderation status of a bounty
	ModerateBounty(input ModerateBountyRequest) (ModerateBountyResponse, error)                                     //Starts a bounty with a given bounty ID for a Twitch User.
	StartBounty(input StartBountyRequest) (StartBountyResponse, error)                                              //Stops a bounty with a given bounty ID for a Twitch User.
	StopBounty(input StopBountyRequest) (StopBountyResponse, error)                                                 //Performs any necessary reconciliation for a bounty (ie. adjusting the
	//consumed budget based on the amount of actual viewers delivered).
	ReconcileBounty(input ReconcileBountyRequest) (ReconcileBountyResponse, error)                      //Take a bountyId and publish bounty sponsored event.
	PublishSponsoredEvent(input PublishSponsoredEventRequest) (PublishSponsoredEventResponse, error)    //Take a bountyId and return bounty sponsored sessions.
	SponsoredSessionsAudit(input SponsoredSessionsAuditRequest) (SponsoredSessionsAuditResponse, error) //Retrieves bounty board settings data that corresponds to a Twitch User ID.
	GetSettings(input GetSettingsRequest) (GetSettingsResponse, error)                                  //Claims a bounty for a Twitch User for a particular campaign ID with a given bounty ID.
	ClaimBounty(input ClaimBountyRequest) (ClaimBountyResponse, error)
}
