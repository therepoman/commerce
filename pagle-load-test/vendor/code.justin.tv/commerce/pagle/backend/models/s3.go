package models

/*
	S3 Input
*/
type ConditionParticipantEffectOutputsS3Input struct {
	ConditionParticipantID            string          `json:"condition_participant_id"`
	ConditionParticipantEffectOutputs []*EffectOutput `json:"condition_participant_effect_outputs"`
}

/*
	S3 Effect Static Config
*/
type EffectStaticConfig struct {
	GiveBits *GiveBitsStaticConfig `json:"give_bits"`
}

type GiveBitsStaticConfig struct {
	ShouldEnforceWhitelist  bool     `json:"should_enforce_whitelist"`
	BenefactorUserWhitelist []string `json:"benefactor_user_whitelist"`
	RecipientUserWhitelist  []string `json:"recipient_user_whitelist"`
}
