package models

import "github.com/aws/aws-sdk-go/service/dynamodb"

/*
	AWS SDK provides similar definitions for Lambda handlers for Dynamo streams,
 	but the way it is structured does not allow for easy unmarshaling. Instead of NewImage/OldImage
	being typed as `map[string]*dynamodb.AttributeValue` which `dynamodbattribute.UnmarshalMap` expects,
  	it is `map[string]events.DynamoDBAttributeValue`
*/
type DynamoEventChange struct {
	NewImage map[string]*dynamodb.AttributeValue `json:"NewImage"`
	OldImage map[string]*dynamodb.AttributeValue `json:"OldImage"`
}

type DynamoEventRecord struct {
	Change    DynamoEventChange `json:"dynamodb"`
	EventName string            `json:"eventName"`
	EventID   string            `json:"eventID"`
}

type DynamoEvent struct {
	Records []DynamoEventRecord `json:"Records"`
}
