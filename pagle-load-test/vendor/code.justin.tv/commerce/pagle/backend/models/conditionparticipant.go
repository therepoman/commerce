package models

import "time"

/*
	ConditionParticipant
*/
type ConditionParticipantProcessingState string
type ConditionParticipantEndState string

const (
	ConditionParticipantProcessingStateCreating                     = ConditionParticipantProcessingState("CREATING")
	ConditionParticipantProcessingStateFailedToCreate               = ConditionParticipantProcessingState("FAILED_TO_CREATE")
	ConditionParticipantProcessingStatePending                      = ConditionParticipantProcessingState("PENDING")
	ConditionParticipantProcessingStateSuccess                      = ConditionParticipantProcessingState("SUCCESS")
	ConditionParticipantProcessingStateError                        = ConditionParticipantProcessingState("ERROR")
	ConditionParticipantProcessingStateFailedToEnqueueForProcessing = ConditionParticipantProcessingState("FAILED_TO_ENQUEUE_FOR_PROCESSING")

	ConditionParticipantEndStatePendingCompletion = ConditionParticipantEndState("PENDING_COMPLETION")
	ConditionParticipantEndStateConditionCanceled = ConditionParticipantEndState("CONDITION_CANCELED")
	ConditionParticipantEndStateConditionExpired  = ConditionParticipantEndState("CONDITION_EXPIRED")
	ConditionParticipantEndStateSatisfied         = ConditionParticipantEndState("SATISFIED")
	ConditionParticipantEndStateExpired           = ConditionParticipantEndState("CONDITION_PARTICIPANT_EXPIRED")
	ConditionParticipantEndStateCanceled          = ConditionParticipantEndState("CONDITION_PARTICIPANT_CANCELED")

	ConditionParticipantMinTTL = int32(60)           // 1 minute
	ConditionParticipantMaxTTL = int32(60 * 60 * 12) // 12 hours
)

type ConditionParticipant struct {
	ConditionParticipantID              string                              `dynamodbav:"condition_participant_id"`
	ConditionID                         string                              `dynamodbav:"condition_id"`
	ConditionOwnerID                    string                              `dynamodbav:"condition_owner_id"`
	ConditionRunID                      string                              `dynamodbav:"condition_run_id"`
	OwnerID                             string                              `dynamodbav:"owner_id"`
	Domain                              string                              `dynamodbav:"domain"`
	DomainOwnerID                       string                              `dynamodbav:"domain-owner_id"`
	CreatedAt                           time.Time                           `dynamodbav:"created_at"`
	UpdatedAt                           time.Time                           `dynamodbav:"updated_at"`
	ConditionParticipantProcessingState ConditionParticipantProcessingState `dynamodbav:"condition_participant_processing_state"`
	ConditionParticipantEndState        ConditionParticipantEndState        `dynamodbav:"condition_participant_end_state"`
	Effect                              Effect                              `dynamodbav:"effect"`
	Error                               string                              `dynamodbav:"error"`
	TTLSeconds                          int32                               `dynamodbav:"ttl_seconds"`

	DomainConditionOwnerIDConditionParticipantOwnerID string `dynamodbav:"domain-condition_owner_id-condition_participant_owner_id"`
}

type PreProcessedConditionParticipant struct {
	ConditionParticipant ConditionParticipant
	EffectOutput         EffectOutput
}
