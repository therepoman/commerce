package models

import "time"

/*
	SFN Input
*/
type TimeoutConditionParticipantSFNInput struct {
	Domain                      string `json:"domain"`
	ConditionParticipantID      string `json:"condition_participant_id"`
	ConditionParticipantOwnerID string `json:"condition_participant_owner_id"`
	TTLSeconds                  int32  `json:"ttl_seconds"`
}

type TimeoutConditionSFNInput struct {
	Domain      string    `json:"domain"`
	ConditionID string    `json:"condition_id"`
	OwnerID     string    `json:"owner_id"`
	TimeoutDate time.Time `json:"timeout_date"`
}

type ConditionAggregatesRetrySFNInput struct {
	ConditionParticipant ConditionParticipant `json:"condition_participant"`
}
