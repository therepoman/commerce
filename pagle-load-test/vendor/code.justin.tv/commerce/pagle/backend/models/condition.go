package models

import "time"

/*
	Condition
*/
type ConditionState string

const (
	ConditionActive   = ConditionState("ACTIVE")
	ConditionCanceled = ConditionState("CANCELED")
	ConditionExpired  = ConditionState("EXPIRED")
	ConditionInactive = ConditionState("INACTIVE")
)

type Condition struct {
	ConditionID                       string           `dynamodbav:"condition_id"`
	OwnerID                           string           `dynamodbav:"owner_id"`
	Domain                            string           `dynamodbav:"domain"`
	DomainOwnerID                     string           `dynamodbav:"domain-owner_id"`
	ConditionRunID                    string           `dynamodbav:"condition_run_id"`
	Name                              string           `dynamodbav:"name"`
	Description                       string           `dynamodbav:"description"`
	DisableWhenSatisfied              bool             `dynamodbav:"disable_when_satisfied"`
	DefineEffectSettingsWhenSatisfied bool             `dynamodbav:"define_effect_settings_when_satisfied"`
	TimeoutAt                         *time.Time       `dynamodbav:"timeout_at"`
	CreatedAt                         time.Time        `dynamodbav:"created_at"`
	UpdatedAt                         time.Time        `dynamodbav:"updated_at"`
	ConditionState                    ConditionState   `dynamodbav:"condition_state"`
	EffectSettings                    []EffectSettings `dynamodbav:"effect_settings"`
	SupportedEffects                  []EffectType     `dynamodbav:"supported_effects"`
}
