package models

import "github.com/aws/aws-sdk-go/service/sqs"

/*
	AWS SDK provides a type for Lambda SQS events, but our utils for unmarshaling use sqs.Message. This type
	allows us to use our utils in the Lambda handlers that receive SQS events.
*/
type SQSEvent struct {
	Records []sqs.Message
}
