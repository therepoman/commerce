package models

/*
	Effect
*/

type EffectType string

const (
	GiveBits = EffectType("GIVE_BITS")
)

// This solution contrasts the proto approach, which uses interfaces to model "oneof" type structures.
// The interface solution does not work with our Dynamo ORM, as it cannot serialize an interface type.
type EffectParams struct {
	GiveBits *EffectParamsGiveBits `dynamodbav:"give_bits" json:"give_bits"`
}

type EffectParamsGiveBits struct {
	PoolableInput EffectInputPoolable `dynamodbav:"poolable_input"  json:"poolable_input"`
}

type EffectOutput struct {
	GiveBits *EffectOutputGiveBits `dynamodbav:"give_bits" json:"give_bits"`
}

type EffectOutputGiveBits struct {
	PoolableOutput EffectOutputPoolable `dynamodbav:"poolable_output"  json:"poolable_output"`
}

type Effect struct {
	EffectType    EffectType   `dynamodbav:"effect_type"    json:"effect_type"`
	Params        EffectParams `dynamodbav:"params"         json:"params"`
	Output        EffectOutput `dynamodbav:"output"         json:"output"`
	TransactionID string       `dynamodbav:"transaction_id" json:"transaction_id"` // Created by the effect tenant
}

type EffectInputPoolable struct {
	Amount     int32  `dynamodbav:"amount"       json:"amount"`
	FromUserID string `dynamodbav:"from_user_id" json:"from_user_id"`
}

type EffectOutputPoolable struct {
	Recipients []PoolRecipientFixedShare `dynamodbav:"recipients" json:"recipients"`
}

/*
	Recipient Share
*/

type PoolRecipientWeightedShare struct {
	ShareWeight int64
	ToUserID    string
}

type PoolRecipientFixedShare struct {
	Amount   int32  `dynamodbav:"amount"     json:"amount"`
	ToUserID string `dynamodbav:"to_user_id" json:"to_user_id"`
}

/*
	Effect Settings
	Defined either
		a) At time of condition creation or
		b) At time of condition satisfaction

	Defining it at the time of condition creation makes subsequent definition unnecessary and invalid
*/

type EffectTypeSettings struct {
	GiveBits *GiveBitsSettings `dynamodbav:"give_bits"`
}

type EffectSettings struct {
	EffectType EffectType          `dynamodbav:"effect_type"`
	Settings   *EffectTypeSettings `dynamodbav:"settings"`
}

type GiveBitsSettings struct {
	PoolRecipients []PoolRecipientWeightedShare
}
