package models

/*
	SNS Messages
*/
type ConditionParticipantProcessingSNSMessage struct {
	ConditionParticipantID       string                       `json:"condition_participant_id"`
	ConditionParticipantEndState ConditionParticipantEndState `json:"condition_participant_end_state"`
	EffectOutput                 *EffectOutput                `json:"effect_output"`
	Domain                       string                       `json:"domain"`
	ConditionParticipantOwnerID  string                       `json:"condition_participant_owner_id"`
}

type ConditionParticipantCleanupSNSMessage struct {
	ConditionParticipantID      string `json:"condition_participant_id"`
	Domain                      string `json:"domain"`
	ConditionParticipantOwnerID string `json:"condition_participant_owner_id"`
}

type ConditionProcessingSNSMessage struct {
	ConditionID    string `json:"condition_id"`
	ConditionRunID string `json:"condition_run_id"`
	OwnerID        string `json:"owner_id"`
	Domain         string `json:"domain"`
}

type ConditionParticipantCompletionSNSMessage struct {
	ConditionParticipantID              string                              `json:"condition_participant_id"`
	ConditionParticipantProcessingState ConditionParticipantProcessingState `json:"condition_participant_processing_state"`
	ConditionID                         string                              `json:"condition_id"`
	ConditionParticipantEndState        ConditionParticipantEndState        `json:"condition_participant_end_state"`
}

type ConditionAggregatesRetryIngestSNSMessage struct {
	ConditionParticipant ConditionParticipant `json:"condition_participant"`
}
