package main

import (
	"bytes"
	"code.justin.tv/commerce/gogogadget/math"
	"code.justin.tv/commerce/gogogadget/random"
	"code.justin.tv/commerce/logrus"
	pagle "code.justin.tv/commerce/pagle/rpc"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"github.com/golang/protobuf/jsonpb"
	vegeta "github.com/tsenart/vegeta/lib"
	"io"
	"io/ioutil"
	"math/rand"
	"net/http"
	"os"
	"path"
	"strings"
	"sync"
	"time"
)

const (
	//pagleEndpoint = "https://payday-private-link.pagle-staging.internal.justin.tv/twirp/code.justin.tv.commerce.pagle.Pagle"
	pagleEndpoint = "http://internal-pagle-874077117.us-west-2.elb.amazonaws.com/twirp/code.justin.tv.commerce.pagle.Pagle"
	dataDir = "data"
	outDir = "out"
	loadTestDomain = "load_test" // All test resources were created with the same domain
	poolRecipient = "116076154" // qa bits partner
)

var (
	allFlag = flag.Bool("all", false, "run all test cases")
	gettersFlag = flag.Bool("getters", false, "run all getter test cases")
	settersFlag = flag.Bool("setters", false, "run all setter test cases")

	// Getters
	getConditionFlag = flag.Bool("getcondition", false, "run GetCondition load test")
	getConditionsFlag = flag.Bool("getconditions", false, "run GetConditions load test")
	batchGetConditionSummaryFlag = flag.Bool("batchgetconditionsummary", false, "run BatchGetConditionSummary load test")
	getConditionParticipantsFlag = flag.Bool("getconditionparticipants", false, "run GetConditionParticipants load test")

	// Setters
	satisfyConditionTestBatchFlag = flag.Bool("satisfyconditiontestbatch", false, "run Satisfy load test - test batch")
	satisfyCondition1Flag = flag.Bool("satisfycondition1", false, "run Satisfy load test - conditions with 1 participant each")
	satisfyCondition100Flag = flag.Bool("satisfycondition100", false, "run Satisfy load test - conditions with 100 participants each")
	satisfyCondition1000Flag = flag.Bool("satisfycondition1000", false, "run Satisfy load test - conditions with 1000 participants each")
	satisfyCondition100000Flag = flag.Bool("satisfycondition100000", false, "run Satisfy load test - conditions with 100000 participants each")
	createConditionParticipantFlag = flag.Bool("createconditionparticipant", false, "run CreateConditionParticipant load test")
)

/**
	MaxHitConstantPacer
 */
type MaxHitPacer struct {
	Pacer vegeta.Pacer
	MaxHits int
}

func (p MaxHitPacer) Pace(elapsed time.Duration, hits uint64) (wait time.Duration, stop bool) {
	if hits >= uint64(p.MaxHits) {
		return time.Millisecond, true
	}

	return p.Pacer.Pace(elapsed, hits)
}

/**
Test Data
 */

// GetCondition
type GetConditionTestItem struct {
	OwnerID string
	ConditionID string
}

// GetConditionParticipants
type GetConditionParticipantsTestItem struct {
	ParticipantOwnerID string
	ConditionOwnerID string
}

// BatchGetConditionSummary
type BatchGetConditionSummaryTestItem struct {
	OwnerID string
	ConditionID string
}

// SatisfyCondition
type SatisfyConditionTestItemsContainer struct {
	IndexOfLastItemUsed int
	Items []SatisfyConditionTestItem
	Mutex sync.Mutex
}

type SatisfyConditionTestItem struct {
	OwnerID string
	ConditionID string
}

// CreateConditionParticipant
type CreateConditionParticipantContainer struct {
	Items []CreateConditionParticipantTestItem
	Conditions []GetConditionTestItem
	Mutex sync.Mutex
}

type CreateConditionParticipantTestItem struct {
	OwnerID string
	Balance int
}

func init() {
	flag.Parse()
	rand.Seed(time.Now().UTC().UnixNano())
}

func main() {
	if *allFlag || *gettersFlag || *getConditionFlag {
		var getConditionTestItems []GetConditionTestItem
		readJSON("GetCondition", &getConditionTestItems)
		testCase_GetCondition(&getConditionTestItems)
	}

	if *allFlag || *gettersFlag ||*getConditionsFlag {
		var getConditionsTestItems []string
		readJSON("GetConditions", &getConditionsTestItems)
		testCase_GetConditions(&getConditionsTestItems)
	}

	if *allFlag || *gettersFlag || *getConditionParticipantsFlag {
		var getConditionParticipantsTestItems []GetConditionParticipantsTestItem
		readJSON("GetConditionParticipants", &getConditionParticipantsTestItems)
		testCase_GetConditionParticipants(&getConditionParticipantsTestItems)
	}

	if *allFlag || *gettersFlag || *batchGetConditionSummaryFlag {
		var batchGetConditionSummaryTestItems []BatchGetConditionSummaryTestItem
		readJSON("BatchGetConditionSummary", &batchGetConditionSummaryTestItems)
		testCase_BatchGetConditionSummary(&batchGetConditionSummaryTestItems)
	}

	if *satisfyConditionTestBatchFlag {
		var satisfyConditionTestBatchContainer SatisfyConditionTestItemsContainer
		name := "SatisfyCondition-TestBatch"
		loadSatisfyConditionData(name, &satisfyConditionTestBatchContainer)
		testCase_SatisfyCondition(name, &satisfyConditionTestBatchContainer)
	}

	if *allFlag || *settersFlag || *satisfyCondition1Flag {
		var satisfyCondition1TestContainer SatisfyConditionTestItemsContainer
		name := "SatisfyCondition-1"
		loadSatisfyConditionData(name, &satisfyCondition1TestContainer)
		testCase_SatisfyCondition(name, &satisfyCondition1TestContainer)
	}

	if *allFlag || *settersFlag || *satisfyCondition100Flag {
		var satisfyCondition100TestContainer SatisfyConditionTestItemsContainer
		name := "SatisfyCondition-100"
		loadSatisfyConditionData(name, &satisfyCondition100TestContainer)
		testCase_SatisfyCondition(name, &satisfyCondition100TestContainer)
	}

	if *allFlag || *settersFlag || *satisfyCondition1000Flag {
		var satisfyCondition1000TestContainer SatisfyConditionTestItemsContainer
		name := "SatisfyCondition-1_000"
		loadSatisfyConditionData(name, &satisfyCondition1000TestContainer)
		testCase_SatisfyCondition(name, &satisfyCondition1000TestContainer)
	}

	if *allFlag || *settersFlag || *satisfyCondition100000Flag {
		var satisfyCondition100000TestContainer SatisfyConditionTestItemsContainer
		name := "SatisfyCondition-100_000"
		loadSatisfyConditionData(name, &satisfyCondition100000TestContainer)
		testCase_SatisfyCondition(name, &satisfyCondition100000TestContainer)
	}

	if *allFlag || *settersFlag || *createConditionParticipantFlag {
		var createConditionParticipantTestItems []CreateConditionParticipantTestItem
		var getConditionTestItems []GetConditionTestItem // Re-using GetCondition test objects
		name := "CreateConditionParticipant"
		readJSON(name, &createConditionParticipantTestItems)
		readJSON("GetCondition", &getConditionTestItems)

		testCase_CreateConditionParticipant(name, &CreateConditionParticipantContainer{
			Mutex: sync.Mutex{},
			Items: createConditionParticipantTestItems,
			Conditions: getConditionTestItems,
		})
	}
}

/**
GetCondition
 */
func createGetConditionTargeter(items *[]GetConditionTestItem) vegeta.Targeter {
	headers := make(http.Header)
	headers.Add("Content-Type", "application/json")
	count := 0
	url := pagleEndpoint + "/GetCondition"

	return func(t *vegeta.Target) (err error) {
		input := getRandomGetConditionInput(items)
		req := &pagle.GetConditionReq{
			OwnerId: input.OwnerID,
			ConditionId: input.ConditionID,
			Domain: loadTestDomain,
		}

		reqBody := bytes.NewBuffer(nil)
		marshaler := &jsonpb.Marshaler{OrigName: true}
		if err = marshaler.Marshal(reqBody, req); err != nil {
			return err
		}

		t.Method = "POST"
		t.URL = url
		t.Header = headers
		t.Body = reqBody.Bytes()
		count++
		return err
	}
}

func getRandomGetConditionInput(itemsP *[]GetConditionTestItem) GetConditionTestItem {
	if itemsP == nil {
		panic(errors.New("received nil items in getRandomGetConditionInput"))
	}

	items := *itemsP

	randomIndex := random.Int(0, len(items) - 1)
	return items[randomIndex]
}

func testCase_GetCondition(items *[]GetConditionTestItem) {
	testDuration := time.Minute * 15
	pacer := vegeta.SinePacer{
		Period:  testDuration,
		Mean:    vegeta.Rate{100, time.Second},
		Amp:     vegeta.Rate{50, time.Second},
		StartAt: vegeta.MeanDown,
	}

	attack(createGetConditionTargeter(items), pacer, testDuration, "GetCondition")
}

/**
GetConditions
*/
func createGetConditionsTargeter(items *[]string) vegeta.Targeter {
	headers := make(http.Header)
	headers.Add("Content-Type", "application/json")
	count := 0
	url := pagleEndpoint + "/GetConditions"

	return func(t *vegeta.Target) (err error) {
		input := getRandomGetConditionsInput(items)
		req := &pagle.GetConditionsReq{
			OwnerId: input,
			ConditionState: pagle.ConditionState_ACTIVE,
			Domain: loadTestDomain,
			Cursor: "",
		}

		reqBody := bytes.NewBuffer(nil)
		marshaler := &jsonpb.Marshaler{OrigName: true}
		if err = marshaler.Marshal(reqBody, req); err != nil {
			return err
		}

		t.Method = "POST"
		t.URL = url
		t.Header = headers
		t.Body = reqBody.Bytes()
		count++
		return err
	}
}

func getRandomGetConditionsInput(itemsP *[]string) string {
	if itemsP == nil {
		panic(errors.New("received nil items in getRandomGetConditionsInput"))
	}

	items := *itemsP

	randomIndex := random.Int(0, len(items) - 1)
	return items[randomIndex]
}

func testCase_GetConditions(items *[]string) {
	testDuration := time.Minute * 30
	pacer := vegeta.SinePacer{
		Period:  testDuration,
		Mean:    vegeta.Rate{1500, time.Second},
		Amp:     vegeta.Rate{500, time.Second},
		StartAt: vegeta.MeanDown,
	}

	attack(createGetConditionsTargeter(items), pacer, testDuration, "GetConditions")
}

/**
GetConditionParticipants
*/
func createGetConditionParticipantsTargeter(items *[]GetConditionParticipantsTestItem) vegeta.Targeter {
	headers := make(http.Header)
	headers.Add("Content-Type", "application/json")
	count := 0
	url := pagleEndpoint + "/GetConditionParticipants"

	return func(t *vegeta.Target) (err error) {
		input := getRandomGetConditionParticipantsInput(items)
		req := &pagle.GetConditionParticipantsReq{
			ConditionParticipantOwnerId: input.ParticipantOwnerID,
			ConditionOwnerId: input.ConditionOwnerID,
			ConditionParticipantEndState: pagle.ConditionParticipantEndState_PENDING_COMPLETION,
			Domain: loadTestDomain,
			Cursor: "",
		}

		reqBody := bytes.NewBuffer(nil)
		marshaler := &jsonpb.Marshaler{OrigName: true}
		if err = marshaler.Marshal(reqBody, req); err != nil {
			return err
		}

		t.Method = "POST"
		t.URL = url
		t.Header = headers
		t.Body = reqBody.Bytes()
		count++
		return err
	}
}

func getRandomGetConditionParticipantsInput(itemsP *[]GetConditionParticipantsTestItem) GetConditionParticipantsTestItem {
	if itemsP == nil {
		panic(errors.New("received nil items in getRandomGetConditionParticipantsInput"))
	}

	items := *itemsP

	randomIndex := random.Int(0, len(items) - 1)
	return items[randomIndex]
}

func testCase_GetConditionParticipants(items *[]GetConditionParticipantsTestItem) {
	testDuration := time.Minute * 30
	pacer := vegeta.SinePacer{
		Period:  testDuration,
		Mean:    vegeta.Rate{1250, time.Second},
		Amp:     vegeta.Rate{500, time.Second},
		StartAt: vegeta.MeanDown,
	}

	attack(createGetConditionParticipantsTargeter(items), pacer, testDuration, "GetConditionParticipants")
}

/**
BatchGetConditionSummary
*/
func batchGetConditionSummaryTargeter(items *[]BatchGetConditionSummaryTestItem) vegeta.Targeter {
	headers := make(http.Header)
	headers.Add("Content-Type", "application/json")
	count := 0
	url := pagleEndpoint + "/BatchGetConditionSummary"

	return func(t *vegeta.Target) (err error) {
		requests := getRandomBatchGetConditionSummaryRequests(items)
		req := &pagle.BatchGetConditionSummaryReq{
			Requests: requests,
		}

		reqBody := bytes.NewBuffer(nil)
		marshaler := &jsonpb.Marshaler{OrigName: true}
		if err = marshaler.Marshal(reqBody, req); err != nil {
			return err
		}

		t.Method = "POST"
		t.URL = url
		t.Header = headers
		t.Body = reqBody.Bytes()
		count++
		return err
	}
}

// Generates a BatchGetConditionSummary request for a random count of conditions {1, 100}
func getRandomBatchGetConditionSummaryRequests(itemsP *[]BatchGetConditionSummaryTestItem) []*pagle.GetConditionSummaryReq {
	const (
		maxBatchReqSize = 100
	)

	if itemsP == nil {
		panic(errors.New("received nil items in getRandomBatchGetConditionSummaryRequests"))
	}

	items := *itemsP

	var requests []*pagle.GetConditionSummaryReq
	upperBound := math.MinInt(maxBatchReqSize, len(items))
	countToTake := random.Int(1, upperBound)

	for _, item := range items {
		if len(requests) >= countToTake {
			return requests
		}

		requests = append(requests, &pagle.GetConditionSummaryReq{
			ConditionId: item.ConditionID,
			OwnerId: item.OwnerID,
			Domain: loadTestDomain,
		})
	}

	return requests
}

func testCase_BatchGetConditionSummary(items *[]BatchGetConditionSummaryTestItem) {
	testDuration := time.Minute * 20
	pacer := vegeta.SinePacer{
		Period:  testDuration,
		Mean:    vegeta.Rate{250, time.Second},
		Amp:     vegeta.Rate{50, time.Second},
		StartAt: vegeta.MeanDown,
	}

	attack(batchGetConditionSummaryTargeter(items), pacer, testDuration, "BatchGetConditionSummary")
}

/**
SatisfyCondition
*/
func satisfyConditionTargeter(container *SatisfyConditionTestItemsContainer) vegeta.Targeter {
	headers := make(http.Header)
	headers.Add("Content-Type", "application/json")
	count := 0
	url := pagleEndpoint + "/SatisfyCondition"

	return func(t *vegeta.Target) (err error) {
		input, err := getNextSatisfyConditionInput(container)

		if err != nil {
			return err
		}

		req := &pagle.SatisfyConditionReq{
			ConditionId: input.ConditionID,
			OwnerId: input.OwnerID,
			Domain: loadTestDomain,
			EffectSettings: []*pagle.EffectSettings{
				{
					EffectType: pagle.EffectType_GIVE_BITS,
					Settings: &pagle.EffectSettings_GiveBitsSettings{
						GiveBitsSettings: &pagle.GiveBitsSettings{
							PoolRecipients: []*pagle.PoolRecipientWeightedShare{
								{
									ToUserId: poolRecipient,
									ShareWeight: 1,
								},
							},
						},
					},
				},
			},
		}

		reqBody := bytes.NewBuffer(nil)
		marshaler := &jsonpb.Marshaler{OrigName: true}
		if err = marshaler.Marshal(reqBody, req); err != nil {
			return err
		}

		t.Method = "POST"
		t.URL = url
		t.Header = headers
		t.Body = reqBody.Bytes()
		count++

		return err
	}
}

func getNextSatisfyConditionInput(container *SatisfyConditionTestItemsContainer) (SatisfyConditionTestItem, error) {
	if container == nil {
		panic(errors.New("received a nil container in getNextSatisfyConditionInput"))
	}

	container.Mutex.Lock()
	defer container.Mutex.Unlock()

	if container.IndexOfLastItemUsed >= len(container.Items) - 1 {
		return SatisfyConditionTestItem{}, errors.New("exhausted satisfy condition test items")
	}

	container.IndexOfLastItemUsed = container.IndexOfLastItemUsed + 1

	return container.Items[container.IndexOfLastItemUsed], nil
}

func testCase_SatisfyCondition(testName string, container *SatisfyConditionTestItemsContainer) {
	if container == nil {
		panic(errors.New("received a nil container in testCase_SatisfyCondition"))
	}

	testDuration := time.Second * 100
	pacer := MaxHitPacer{
		MaxHits: len(container.Items),
		Pacer:	vegeta.ConstantPacer{
			Freq: 100,
			Per: time.Second,
		},
	}

	attack(satisfyConditionTargeter(container), pacer, testDuration, testName)
}

/**
CreateConditionParticipant
*/
func createConditionParticipantTargeter(container *CreateConditionParticipantContainer) vegeta.Targeter {
	headers := make(http.Header)
	headers.Add("Content-Type", "application/json")
	count := 0
	url := pagleEndpoint + "/CreateConditionParticipant"

	return func(t *vegeta.Target) (err error) {
		condition := getRandomCreateConditionParticipantCondition(container)
		input, err := getNextCreateConditionParticipantInput(container)

		if err != nil {
			return err
		}

		req := &pagle.CreateConditionParticipantReq{
			ConditionId: condition.ConditionID,
			ConditionOwnerId: condition.OwnerID,
			Domain: loadTestDomain,
			ConditionParticipantOwnerId: input.OwnerID,
			TtlSeconds: 0,
			Effect: &pagle.Effect{
				EffectType: pagle.EffectType_GIVE_BITS,
				Params: &pagle.Effect_GiveBitsParams{
					GiveBitsParams: &pagle.GiveBitsParams{
						Amount: 1,
						FromUserId: input.OwnerID,
					},
				},
			},
		}

		reqBody := bytes.NewBuffer(nil)
		marshaler := &jsonpb.Marshaler{OrigName: true}
		if err = marshaler.Marshal(reqBody, req); err != nil {
			return err
		}

		t.Method = "POST"
		t.URL = url
		t.Header = headers
		t.Body = reqBody.Bytes()
		count++
		return err
	}
}

func getRandomCreateConditionParticipantCondition(container *CreateConditionParticipantContainer) GetConditionTestItem {
	container.Mutex.Lock()
	defer container.Mutex.Unlock()

	if container == nil {
		panic(errors.New("received a nil container in getRandomCreateConditionParticipantCondition"))
	}

	randomIndex := random.Int(0, len(container.Conditions) - 1)
	return container.Conditions[randomIndex]
}

func getNextCreateConditionParticipantInput(container *CreateConditionParticipantContainer) (CreateConditionParticipantTestItem, error) {
	if container == nil {
		panic(errors.New("received a nil container in getNextCreateConditionParticipantInput"))
	}

	container.Mutex.Lock()
	defer container.Mutex.Unlock()

	const (
		maxAttempts = 1000
	)

	for i := 0; i < maxAttempts; i++ {
		randomIndex := random.Int(0, len(container.Items) - 1)
		item := container.Items[randomIndex]

		if item.Balance > 1 {
			container.Items[randomIndex].Balance = container.Items[randomIndex].Balance - 1
			return container.Items[randomIndex], nil
		}
	}

	return CreateConditionParticipantTestItem{}, errors.New("failed to get a test user with a positive balance")
}

func testCase_CreateConditionParticipant(testName string, container *CreateConditionParticipantContainer) {
	if container == nil {
		panic(errors.New("received a nil container in testCase_CreateConditionParticipant"))
	}

	testDuration := time.Minute * 15
	pacer := vegeta.SinePacer{
		Period:  testDuration,
		Mean:    vegeta.Rate{400, time.Second},
		Amp:     vegeta.Rate{100, time.Second},
		StartAt: vegeta.MeanDown,
	}

	attack(createConditionParticipantTargeter(container), pacer, testDuration, testName)
}

/**
	Utils
 */

func readJSON(sourceFileName string, out interface{}) {
	dir, err := os.Getwd()

	if err != nil {
		panic(err)
	}

	source := path.Join(dir, dataDir, fmt.Sprintf("%s.json", sourceFileName))

	bytes, err := ioutil.ReadFile(source)

	err = json.Unmarshal(bytes, out)

	if err != nil {
		panic(err)
	}
}

func loadSatisfyConditionData(fileName string, out *SatisfyConditionTestItemsContainer) {
	var satisfyConditionRawTestItems map[string]int
	readJSON(fileName, &satisfyConditionRawTestItems)

	var items []SatisfyConditionTestItem

	for key, _ := range satisfyConditionRawTestItems {
		parts := strings.Split(key, "::")

		if len(parts) != 2 {
			panic(errors.New("satisfy condition test data malformed"))
		}

		items = append(items, SatisfyConditionTestItem{
			OwnerID: parts[0],
			ConditionID: parts[1],
		})
	}

	out.Mutex = sync.Mutex{}
	out.IndexOfLastItemUsed = 0
	out.Items = items
}

func printMetrics(title string, metrics vegeta.Metrics) {
	dir, err := os.Getwd()

	if err != nil {
		panic(err)
	}

	dest := path.Join(dir, outDir, fmt.Sprintf("%d_%s_results.txt", time.Now().Unix(), title))

	writers := make([]io.Writer, 0)
	writers = append(writers, os.Stdout)

	file, err := os.Create(dest)
	if err != nil {
		logrus.Info("Error creating results file")
	} else {
		writers = append(writers, file)
	}

	w := io.MultiWriter(writers...)

	_, _ = fmt.Fprintf(w, "\n====== %s Results ======\n", title)

	_, _ = fmt.Fprintf(w, "Start Time: %s \n", metrics.Earliest.String())
	_, _ = fmt.Fprintf(w, "End Time: %s \n", metrics.Latest.String())
	_, _ = fmt.Fprintf(w, "Duration: %s \n", metrics.Duration.String())

	_, _ = fmt.Fprintf(w, "Requests: %d \n", metrics.Requests)
	_, _ = fmt.Fprintf(w, "Success Percentage: %f \n", metrics.Success)
	_, _ = fmt.Fprintf(w, "Request Rate: %f \n", metrics.Rate)
	_, _ = fmt.Fprintf(w, "Succesful Request Throughput: %f \n\n", metrics.Throughput)

	_, _ = fmt.Fprintf(w, "Latency Average: %s \n", metrics.Latencies.Mean)
	_, _ = fmt.Fprintf(w, "Latency p99: %s \n", metrics.Latencies.P99)
	_, _ = fmt.Fprintf(w, "Latency Max: %s \n\n", metrics.Latencies.Max)

	_, _ = fmt.Fprintf(w, "Status Codes: %v \n", metrics.StatusCodes)
	_, _ = fmt.Fprintf(w, "Errors: %v\n", metrics.Errors)
}

func attack(targeter vegeta.Targeter, pacer vegeta.Pacer, dur time.Duration, name string)  {
	logrus.Info("Starting attack on " + name)
	attacker := vegeta.NewAttacker()
	var metrics vegeta.Metrics
	for res := range attacker.Attack(targeter, pacer, dur, name + "-Attack") {
		metrics.Add(res)
	}
	metrics.Close()

	printMetrics(name, metrics)
}