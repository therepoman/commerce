# PILEDRIVER
Ad Hoc Distributed Load Testing

![Zangeif Piledrives!](/images/zangief.gif?raw=true "Zangief Piledrives!")

## Why you would want to use this
* You want to make sure your service is operationally ready.
* The service you want to test is network isolated and you need a way to call it.
* You want to generate so much load that your laptop shouldn't be considered as an option to run it.

## Fetching Piledrive is Easy
I install this via `go get` like so:
```bash
go get code.justin.tv/commerce/piledriver
```

## Things you'll need to know before we start PILEDRIVING
This repository uses [Nebbiolo](https://git-aws.internal.justin.tv/commerce/nebbiolo), which is a plan based load testing framework made by and for the commerce organization. You'll want to read up on how to use their TOML file plans to run load tests with Piledriver.

You will also need to have setup your SSH key on your box. This will allow your local machine to SSH into the instances created by Piledriver to start, stop, set file descriptors, and get results.

## Time to PILEDRIVE your Service!

You will need to run some terraform in the account you wish to load test. This will setup a launch template and an ASG attached to your VPC. You'll want to copy the example TFVars file and replace it with the correct values associated with your VPC.
 
### Terraform terminal commands
```bash
cd terraform
cp piledriver.tfvars.example piledriver.tfvars
# Edit the TF Vars with the appropriate values
terraform init
terraform plan -var-file piledriver.tfvars -out loadtest.plan
terraform apply loadtest.plan
```

You will need to wait for terraform to finish creating your resources. This should not take but only a few minutes.

You will need to edit the file `variables.sh` that exists in the root directory of this repository. This will contain variables that specify the AWS profile, the Teleport Bastion Cluster, and your username that is used to SSH into instances.

If you think you need more file descriptors on your hosts, run the following command. It will increase your hard / soft limits to 80,000.
```bash
make set_file_descriptors
```
 Now you will want to make your load test plan. Create the load test plan file in this directoy. See the [Nebbiolo](https://git-aws.internal.justin.tv/commerce/nebbiolo) repository for example usages.

You are ready to start load testing now! To start, simply run this command:
```bash
make start
```
You should see that it is SCPing your load test plan to all the hosts in your ASG, as well as it starting the command to run the load test on each instance.

If you need to stop the load test from running, simply run:
```bash
make stop
```

When your loadtest is complete, simply run this command to fetch all your results from your ASG:
```bash
make get_results OUTPUT_DIR=loadtest-results
```
where you specify what directory to write the results.

## You're done PILEDRIVING, now CLEAN UP

To remove all your resources you made while running Piledrive, simply run the following commands:

```bash
cd terraform
terraform destroy -var-file piledriver.tfvars
```

It should ask you to confirm that you want to destroy the resources you created, which you will just type `yes` and watch the resources disappear.