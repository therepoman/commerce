#!/bin/bash

# This file exists to define some env vars for running your load test

# The AWS profile you're operating in
AWS_PROFILE=pagle-devo

# The Teleport Bastion you're operating in
TC=twitch-pagle-devo

# Your LDAP username you use to login to EC2 hosts
USERNAME=flomic

RUN_FLAG=createconditionparticipant

OUTPUT_DIR=loadtest-results