#!/bin/bash

. $GOPATH/src/code.justin.tv/commerce/pagle-load-test/variables.sh

loadTestIPs=($(aws ec2 describe-instances --filters "Name=tag:Name,Values=load-testing" --profile ${AWS_PROFILE} | jq -cr .Reservations[].Instances[].PrivateIpAddress))

for i in "${loadTestIPs[@]}"
do
    echo "SCPing to ${i}..."
    TC=$TC scp $GOPATH/src/code.justin.tv/commerce/pagle-load-test/scripts/stop-load-test.sh "${USERNAME}@${i}:."
    TC=$TC scp variables.sh "${USERNAME}@${i}:."
    TC=$TC ssh -t "${USERNAME}@${i}" ./stop-load-test.sh
done