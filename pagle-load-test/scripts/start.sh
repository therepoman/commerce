#!/bin/bash

. $GOPATH/src/code.justin.tv/commerce/pagle-load-test/variables.sh

loadTestIPs=($(aws ec2 describe-instances --filters "Name=tag:Name,Values=load-testing" --profile ${AWS_PROFILE} | jq -cr .Reservations[].Instances[].PrivateIpAddress))

echo "compiling main.go"
GOOS=linux go build -o main

for i in "${loadTestIPs[@]}"
do
    echo "SCPing to ${i}..."
    TC=$TC scp main "${USERNAME}@${i}:."
    TC=$TC scp -r data "${USERNAME}@${i}:."
    TC=$TC scp $GOPATH/src/code.justin.tv/commerce/pagle-load-test/scripts/run-load-test.sh "${USERNAME}@${i}:."
    TC=$TC scp variables.sh "${USERNAME}@${i}:."
    TC=$TC ssh -t "${USERNAME}@${i}" nohup ./run-load-test.sh
done