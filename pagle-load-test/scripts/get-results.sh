#!/bin/bash

. $GOPATH/src/code.justin.tv/commerce/pagle-load-test/variables.sh

outputDir=$OUTPUT_DIR
loadTestIPs=($(aws ec2 describe-instances --filters "Name=tag:Name,Values=load-testing" --profile ${AWS_PROFILE} | jq -cr .Reservations[].Instances[].PrivateIpAddress))

mkdir -p $outputDir

for i in "${loadTestIPs[@]}"
do
    echo "Getting results from to ${i}..."
    TC=$TC scp "${USERNAME}@${i}:nohup.out" "${outputDir}/nohup-${i}.txt"
    TC=$TC scp "${USERNAME}@${i}:test.out" "${outputDir}/${i}.txt"
done