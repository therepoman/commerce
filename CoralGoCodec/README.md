# CoralGoCodec
This package exposes the interfaces used to define Coral [transport codecs and round trippers]
(https://w.amazon.com/index.php/Coral/Protocols#Concrete_Examples).

The *RoundTripper* interface is used for client codec implementations.  It is responsible for translating
a Coral request into the corresponding wire format, sending it on the wire, decoding the response, and returning
the decoded response.

The *Codec* interface is used for defining how to encode a request/response for the wire and how to decode a
request/response from the wire.  It is also responsible for detecting whether or not a request uses its
encoding.

## Update Instructions:

Go to https://w.amazon.com/index.php/Fuel/Store/Development/TwitchGitFarmImporter on the Amazon network for instructions on how to update this client.
