import setuptools
from zoltar.version import VERSION

setuptools.setup(
    name="zoltar",
    version=VERSION,
    author="Commerce ML",
    description="Common utilities for Commerce ML",
    url="https://git-aws.internal.justin.tv/commerce/zoltar",
    packages=["zoltar"],
    include_package_data=True,
    license="Amazon",
    install_requires=[
        "matplotlib",
        "numpy",
        "pandas",
        "boto3",
        "mantra>=1.2.0.dev1",
        "cmake",
        "xgboost>=0.90",
        "sklearn",
    ],
    extras_require={":python_version<'3.7'": ["dataclasses==0.7"]},
)
