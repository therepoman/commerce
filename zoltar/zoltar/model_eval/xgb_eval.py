import typing as t

import pandas as pd
import xgboost


def xgb_feature_importance(
    xgb_booster: xgboost.Booster,
    feature_names: t.List[str],
    importance_types: t.List[str] = ["weight", "gain", "cover"],
    sort_by: str = "gain",
):

    feature_map = {f"f{idx}": feat_name for idx, feat_name in enumerate(feature_names)}
    feature_importances = {}
    for imp_type in importance_types:
        feature_importances[imp_type] = xgb_booster.get_score(importance_type=imp_type)

    imp_df = pd.DataFrame.from_dict(feature_importances)
    imp_df.index = [feature_map[k] for k in imp_df.index]
    return imp_df.sort_values(sort_by, ascending=0)
