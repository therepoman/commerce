import logging

from zoltar.environment import ENV

# TODO configure logger
LOG_LEVEL = logging.INFO if ENV != "prod" else logging.ERROR


def delegate_log(logtxt: str, logger: logging.Logger = None, log_level=logging.INFO):
    if logger:
        logger.log(level=log_level, msg=logtxt)
    else:
        logging.log(level=log_level, msg=logtxt)
