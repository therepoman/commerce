from mantra.plugins.hooks.S3_hook import S3Hook
from moto import mock_s3

from zoltar.io.s3 import StageLogger

TEST_BUCKET = "zoltar-sequence-logger-test"


@mock_s3
def test_old_style():
    S3Hook().init_bucket(TEST_BUCKET)
    stage_logger = StageLogger("test_stage_logs")
    test_meta = {"base_tablename": "test_table", "minday": "2020-01-01"}
    stage_logger.register_completed_log(test_meta, "20200418t011105", TEST_BUCKET)
    assert stage_logger.fetch_latest_log(TEST_BUCKET, "base_tablename") == "test_table"
    assert stage_logger.fetch_latest_log(TEST_BUCKET, "version") == "20200418t011105"


@mock_s3
def test_stateful_logger(monkeypatch):
    monkeypatch.setattr(
        "zoltar.io.s3.StageLogger._get_formatted_ts", lambda x: "20200418t011105"
    )
    expected_run_version = "TEST1_20200418t011105"
    stage_logger = StageLogger(
        "test_stage_logs", base_version="TEST1", s3_bucket=TEST_BUCKET
    )
    test_meta = {"base_tablename": "test_table", "minday": "2020-01-01"}
    stage_logger.register_completed_log(test_meta, None, None)
    assert stage_logger.fetch_latest_log(None, "base_tablename") == "test_table"
    assert stage_logger.fetch_latest_log(None, "version") == expected_run_version
    assert stage_logger.run_version == expected_run_version


@mock_s3
def test_update_log():
    stage_logger = StageLogger(
        "test_stage_logs", base_version="TEST1", s3_bucket=TEST_BUCKET
    )
    test_meta = {"base_tablename": "test_table", "minday": "2020-01-01"}
    stage_logger.register_completed_log(test_meta, None, None)
    stage_logger.update_log({"model_version": "test_model_445"})
    updated_log = stage_logger.fetch_latest_log(None, "ALL")
    assert len(updated_log) == 4
    assert updated_log["model_version"] == "test_model_445"


@mock_s3
def test_gen_id(monkeypatch):
    from zoltar.io.s3 import StageLogger

    monkeypatch.setattr(
        "zoltar.io.s3.StageLogger._get_formatted_ts", lambda x: "20200418t011105"
    )
    stage_logger = StageLogger(
        "test_stage_logs", base_version="TEST1", s3_bucket=TEST_BUCKET
    )
    assert stage_logger.gen_identifier("test_id") == "test_id_20200418t011105"
