from collections import Counter

import pytest

import zoltar.sequences.fingerprints as fp
import zoltar.sequences.tokenizing as tk


@pytest.fixture
def default_fingerprints():
    return [
        """[{"k": "token1", "v": 123}, {"k": "token2", "v": 123}, {"k": "token1", "v": 456}]""",
        """[{"k": "token5", "v": 567}, {"k": "token1", "v": 277}]""",
        """[{"k": "token2", "v": 40}, {"k": "token2", "v": 48}, {"k": "token2", "v": 47}, {"k": "token3", "v": 44}]""",
    ]


@pytest.fixture
def default_raw_fp():
    return """{"start":1584845526245,"time":1584847221609,
    "events":[{"k":"checkout_complete_purchase_click_success","v":1470392},
    {"k":"checkout_purchase_success_screen_shown","v":1470391},
    {"k":"checkout_close","v":1468466},{"k":"mw_79519117","v":1461884},
    {"k":"pv_channel","v":1431918}]}"""


@pytest.fixture
def default_bad_fp():
    return """0a"?"#?akzvi|qa{tirpd}os?*<.akzvi|qazugyvezos.54%7"iy 0a:iys;5x0e(+0%-4\'a}i5sq{gsuh|>
    }0a:iys8*5.-0=x1c=$"5x14.>9&sga\'iyi}{grt,g8s aki08/&?*5x0e\',0/x1c<$14ios=aksui}zf6o*i(sqa3.-2#.09(x0e
    (,<;/4?&x0e?10%08?*>%a}i5sq{gsuh|>}0a:iys8*5.-0=x1c=$"5x14.>9&sga\'iyi}{grt,g8s aki08/&?*5x0e\',
    0/x1c<$14ios=aksui}zf6o*i(sqa29,&%x1c8%749"2?*>%a}i5sq{gsuh|>}0a:iys=3x0e-67>.8"a}i5sq{gsuh|>}0a:iys;
    5x0e(+0%-4\'a}i5sq{gsuh}>}0a:iys8*5.-0=x1c=$"5x14.>9&sga\'iyi}{gru,g8s aki08/&?*5x0e\'"""


def test_twilight_token_stemmer():
    test_cases = [
        dict(input="mw_123", output="mw_"),
        dict(input="vp_abc", output="vp_"),
        dict(input="donotstem_suffix", output="donotstem_suffix"),
    ]

    for case in test_cases:
        assert fp.twilight_token_stemmer(case["input"]) == case["output"]


def test_decompose_raw_fingerprints(default_raw_fp):
    token_times = fp.decompose_twilight_fingerprint(default_raw_fp)
    assert token_times == {
        "tokens": [
            "checkout_complete_purchase_click_success",
            "checkout_purchase_success_screen_shown",
            "checkout_close",
            "mw_",
            "pv_channel",
            "<eos>",
        ],
        "latencies": [1470392, 1470391, 1468466, 1461884, 1431918, 0],
    }


def test_bad_xor_fp_decompose(default_bad_fp):
    token_times = fp.decompose_twilight_fingerprint(default_bad_fp)
    assert token_times == {"tokens": [], "latencies": []}


def test_bad_xor_fp_transform(default_bad_fp):
    sentence = fp.transform_twilight_fingerprint(default_bad_fp)
    assert sentence == ""


def test_empty_sentence():
    assert dict(tk.counter_df([""])) == {"": 1}


def test_decompose_twilight_fingerprint(default_fingerprints):
    token_times = [
        fp.decompose_twilight_fingerprint(raw_fp) for raw_fp in default_fingerprints
    ]
    assert token_times == [
        {
            "tokens": ["token1", "token1", "token2", "<eos>"],
            "latencies": [456, 123, 123, 0],
        },
        {"tokens": ["token5", "token1", "<eos>"], "latencies": [567, 277, 0]},
        {
            "tokens": ["token2", "token2", "token3", "token2", "<eos>"],
            "latencies": [48, 47, 44, 40, 0],
        },
    ]


def test_transform_twilight_fingerprint(default_fingerprints):
    # test transform_fn
    assert (
        fp.transform_twilight_fingerprint(default_fingerprints[1])
        == """token5 token1 zzend"""
    )
    assert (
        fp.transform_twilight_fingerprint(default_fingerprints[1], "other")
        == """token5 token1 other"""
    )
    sentences = [fp.transform_twilight_fingerprint(l) for l in default_fingerprints]
    assert sentences == [
        "token1 token2 token1 zzend",
        "token5 token1 zzend",
        "token2 token2 token2 token3 zzend",
    ]


def test_sequence_tokenizing():
    sentences = [
        "token1 token2 token1 zzend",
        "token5 token1 zzend",
        "token2 token2 token2 token3 zzend",
    ]
    # test counter functions
    df_counter = tk.counter_df(sentences)
    assert df_counter == Counter(
        {"zzend": 3, "token2": 2, "token1": 2, "token5": 1, "token3": 1}
    )
    tf_counter = tk.counter_tf(sentences)
    assert tf_counter == Counter(
        {"token2": 4, "token1": 3, "zzend": 3, "token5": 1, "token3": 1}
    )

    # test Vocabulary
    valid_test_tokens = ["token1", "token2", "token3", "token5", "zzend"]
    test_vocab = tk.Vocabulary(tf_counter)
    assert test_vocab.num_tokens == 5
    assert test_vocab.missing_idx == 5
    for token in valid_test_tokens:
        assert test_vocab.token_to_idx.get(token) >= 0
    for i in range(test_vocab.num_tokens):
        assert test_vocab.idx_to_token.get(i, "missing") in valid_test_tokens + [
            "missing"
        ]

    # test numpy transforms
    vector = tk.counter_to_nparray(test_vocab, tf_counter)

    # example bad vector with a missing token
    for idx, cnt in enumerate(vector):
        assert tf_counter.get(test_vocab.idx_to_token.get(idx)) == cnt.item()

    bad_vector = tk.counter_to_nparray(
        test_vocab,
        Counter(
            {
                "token2": 4,
                "token1": 3,
                "zzend": 3,
                "token5": 1,
                "token3": 1,
                "missing_token": 12,
            }
        ),
    )
    assert bad_vector.all() == vector.all()

    vector_laplace = tk.np_add_laplace_smoothing(2)(vector)
    for idx, cnt in enumerate(vector_laplace):
        assert tf_counter.get(test_vocab.idx_to_token.get(idx)) + 2 == cnt

    vector_norm = tk.np_divide_by_token_value(tf_counter, "token2")(vector)
    for idx, cnt in enumerate(vector_norm):
        assert tf_counter.get(test_vocab.idx_to_token.get(idx)) / 4 == cnt
