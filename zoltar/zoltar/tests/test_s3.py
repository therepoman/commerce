from typing import Any, Dict, List

import boto3
from mantra.plugins.hooks.S3_hook import S3Hook
from moto import mock_s3

from zoltar.io.s3 import s3_copy, s3_copy_prefix


def setup_test(s3_hook: S3Hook, input_args: Dict[str, Any]):
    s3_hook.init_bucket(input_args["src_bucket"])
    s3_hook.init_bucket(input_args["dst_bucket"])
    src_keys = input_args["src_key"]
    if not isinstance(src_keys, list):
        src_keys = [src_keys]
    for src_key in src_keys:
        s3_hook.write_str("1,0.05\n2,0.6\n3,0.7", input_args["src_bucket"], src_key)


def keys_from_prefix(input_args: Dict[str, Any]) -> List[str]:
    s3_resource = boto3.resource("s3")
    test_bucket = s3_resource.Bucket(input_args["dst_bucket"])
    output = [
        obj.key for obj in test_bucket.objects.filter(Prefix=input_args["dst_key"])
    ]
    return output


def test_s3_copy():
    input1 = dict(
        src_bucket="dev-twitch-commerce-testexample",
        src_key="model_output/prediction_result_joined_final.csv",
        dst_bucket="twitch-commerce-science-aws-test-1",
        dst_key="copytest_1.csv",
    )

    input2 = dict(
        src_bucket="twitch-commerce-science-aws-test-1",
        src_key="copytest_1.csv",
        dst_bucket="twitch-commerce-science-aws-test-2",
        dst_key="copytest_2.csv",
    )

    input3 = dict(
        src_bucket="twitch-commerce-science-aws-test-1",
        src_key="copytest_1.csv",
        dst_bucket="twitch-commerce-science-aws-test-2",
        dst_key="copytest_2_v2.csv",
    )

    test_cases = [
        {"name": "to test1 bucket", "input": input1},
        {"name": "from bucket1 to bucket2", "input": input2},
        {"name": "checking key uniqueness", "input": input3},
    ]

    @mock_s3
    def test_one(input_args):
        s3_hook = S3Hook(client=boto3.client("s3", region_name="us-west-2"))
        setup_test(s3_hook, input_args)
        s3_copy(s3_hook=s3_hook, **input_args)
        return keys_from_prefix(input_args)

    for test_case in test_cases:
        test_output = test_one(test_case["input"])
        assert len(test_output) == 1
        assert test_output[0] == test_case["input"]["dst_key"]


@mock_s3
def test_prefix():
    s3_hook = S3Hook(client=boto3.client("s3", region_name="us-west-2"))
    prefix_input = dict(
        src_bucket="twitch-commerce-science-aws-test-1",
        src_prefix="test_prefix",
        src_key=["test_prefix/copytest_1.csv", "test_prefix/copytest_2.csv"],
        dst_bucket="twitch-commerce-science-aws-test-2",
    )
    setup_test(s3_hook, prefix_input)
    src_keys = prefix_input.pop("src_key")
    s3_copy_prefix(**prefix_input)
    prefix_input["dst_key"] = "test_prefix"
    copied_keys = keys_from_prefix(prefix_input)
    assert len(copied_keys) == 2
    assert set(src_keys) == set(copied_keys)
