import pathlib
from datetime import datetime

import pandas as pd
import pytest
from mantra.plugins.hooks.aws_secret_manager import AwsSecretsManagerHook
from mantra.plugins.hooks.postgres_hook import PostgresHook
from mantra.plugins.hooks.tahoe_hook import TahoeHook, TahoeProducerConf
from mantra.utils.file_utils import load_and_format

from zoltar.commerce_config import get_commerce_ml_tap
from zoltar.io.publish_csv_to_tahoe import CSVtoTahoeConfigs

_test_time = None


@pytest.fixture
def get_test_time():
    global _test_time
    if _test_time is None:
        _test_time = datetime.now()
    return _test_time


@pytest.fixture
def commerce_ml_tap():
    return get_commerce_ml_tap()


@pytest.fixture
def default_pg_hook():
    return PostgresHook("localhost", "test_users", "testdb", "5439")


@pytest.fixture
def commerce_tahoe_producer():
    return TahoeHook(
        TahoeProducerConf(
            application_name="commercescienceaws",
            iam_role="arn:aws:iam::331582574546:role/producer-commercescienceaws",
            api_key=AwsSecretsManagerHook("tahoe_producer_key", as_dict=False).secret,
            principal_role="arn:aws:iam::348802193101:root",
        )
    )


@pytest.fixture
def default_integration_sql():
    integration_path = (
        pathlib.Path(__file__).parent.absolute() / "integration" / "default.sql"
    )
    return load_and_format(str(integration_path))[0]


@pytest.fixture
def default_df(get_test_time):
    return pd.DataFrame(
        {
            "myint": [1, 2],
            "mydouble": [3.123, 4.902],
            "myvarchar": ["abc123", "bcd098"],
            "time": get_test_time.strftime("%Y-%m-%d %H:%M:%S"),
        }
    )


@pytest.fixture
def default_coldef(get_test_time):
    return [
        {"name": "myint", "type": "INT"},
        {"name": "mydouble", "type": "DOUBLE"},
        {"name": "myvarchar", "type": "STRING"},
        {"name": "time", "type": "TIMESTAMP"},
    ]


@pytest.fixture
def default_csvtahoe_configs(get_test_time):
    return CSVtoTahoeConfigs(
        s3_bucket="twitch-commerce-science-aws-test-1",
        output_table="zoltar_test0_dev",
        output_identifier_key=f"""{get_test_time.strftime("%Y-%m-%d").replace("-", "")}/{get_test_time.strftime("%Y%m%dT%H%M%S")}""",
    )
