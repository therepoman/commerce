from os import path
from pathlib import Path

import zoltar.config_loaders as lcl


def test_config_loader():
    app_dir = Path(__file__).parent.absolute()
    config_dir = path.join(app_dir, "test_config_loaders.yaml")

    test_cases = [
        {
            "env": "dev",
            "output": {
                "a_path": f"{app_dir}/dev_a",
                "b": {"b1": "default_b1", "b2_s3_bucket": "dev-default_b2"},
            },
        },
        {
            "env": "staging",
            "output": {"a_path": f"{app_dir}/default_a", "b": "staging_b"},
        },
        {
            "env": "integration",
            "output": {
                "a_path": f"{app_dir}/default_a",
                "b": {"b1": "default_b1", "b2_s3_bucket": "integration-default_b2"},
                "c": "integration_c",
            },
        },
        {
            "env": "prod",
            "output": {
                "a_path": f"{app_dir}/default_a",
                "b": {"b1": "default_b1", "b2_s3_bucket": "prod-prod_b2"},
            },
        },
    ]

    for case in test_cases:
        env = case["env"]
        replace_rules = [lcl.rule_abs_path(app_dir), lcl.rule_s3_bucket(env)]
        loader = lcl.LocalConfigLoader(env=env, replace_rules=replace_rules)
        configs = loader.load_environment_configs(config_dir)
        assert configs == case["output"]
