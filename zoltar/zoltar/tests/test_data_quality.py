import datetime

import numpy as np
import pandas as pd

import zoltar.io.data_quality as dq


def apply_tests(fn, test_cases):
    for case in test_cases:
        assert fn(**case["input"]) == case["output"]


def test_construct_batch_filters():
    test_cases = [
        {
            "input": dict(datasize=100, target_rows_per_batch=40),
            "output": [
                "random_index*100 >= 0*40 and random_index*100 < 1*40",
                "random_index*100 >= 1*40 and random_index*100 < 2*40",
                "random_index*100 >= 2*40 and random_index*100 < 3*40",
            ],
        }
    ]
    apply_tests(dq._construct_batch_filters, test_cases)


def test_construct_balanced_sampling_case_filter():
    test_cases = [
        {
            "input": dict(
                group_stats=[["g1", 30], ["g2", 20], ["g3", 10]],
                target_col="label",
                target_rows_per_group=15,
            ),
            "output": (
                "case \n"
                "when label = 'g1' then 0.5\n"
                "when label = 'g2' then 0.75\n"
                "when label = 'g3' then 1.5\n"
                " else null end"
            ),
        }
    ]
    apply_tests(dq._construct_balanced_sampling_case_filter, test_cases)
    pass


def test_extract_group_stats():
    assert dq._extract_group_stats(
        pd.DataFrame({"label": ["g1", "g2", "g3"], "group_weight": [30, 20, 10]})
    ) == [["g1", 30], ["g2", 20], ["g3", 10]]


def test_sql_last_avail():
    assert (
        dq.sql_last_avail("test_table", ["day"])
        == "select max(day) as max_date from test_table where day is not null"
    )


def test_most_recent_for_partition():
    assert (
        dq.sql_most_recent_for_partition("animal_state", "time")
        == "row_number() over(partition by animal_state order by time desc) as recent_day_number"
    )


def test_sql_select_most_recent():
    assert (
        dq.sql_select_most_recent("test_table", ["age", "weight"])
        == "select * from test_table where recent_day_number = 1 and age is not null and weight is not null"
    )


def test_sql_group_counts():
    test_cases = [
        {
            "input": dict(table_handle="test_table", target_col="user_id"),
            "output": "select user_id as label, sum(1) as group_weight from test_table group by 1",
        }
    ]
    apply_tests(dq.sql_group_counts, test_cases)


def test_sql_count():
    assert dq.sql_count("test_table") == "select sum(1) as n from test_table"


def test_last_available(monkeypatch, default_pg_hook):
    """ tests handling of common date types encountered in pandas dataframes"""
    monkeypatch.setattr(
        "mantra.plugins.hooks.postgres_hook.PostgresHook.get_pandas_df",
        lambda _, __: pd.DataFrame([{"max_date": datetime.date(2020, 3, 23)}]),
    )
    assert dq.last_available(
        "test_table", ["day"], default_pg_hook
    ) == datetime.datetime(2020, 3, 23, 0, 0)

    monkeypatch.setattr(
        "mantra.plugins.hooks.postgres_hook.PostgresHook.get_pandas_df",
        lambda _, __: pd.DataFrame([{"max_date": pd.Timestamp("2020-03-24 00:00:00")}]),
    )
    assert dq.last_available(
        "test_table", ["day"], default_pg_hook
    ) == datetime.datetime(2020, 3, 24, 0, 0)

    monkeypatch.setattr(
        "mantra.plugins.hooks.postgres_hook.PostgresHook.get_pandas_df",
        lambda _, __: pd.DataFrame([{"max_date": np.datetime64("2005-02-25T03:30")}]),
    )
    assert dq.last_available(
        "test_table", ["day"], default_pg_hook
    ) == datetime.datetime(2005, 2, 25, 3, 30)


def test_total_rows(monkeypatch, default_pg_hook):
    monkeypatch.setattr(
        "mantra.plugins.hooks.postgres_hook.PostgresHook.get_pandas_df",
        lambda _, __: pd.DataFrame([{"n": 123456}]),
    )
    assert dq.total_rows("test_table", default_pg_hook) == 123456
