import typing as t
from collections import Counter

import numpy as np


class Vocabulary:
    def __init__(self, global_counter: Counter):
        self.num_tokens = len(global_counter)
        self.idx_to_token = dict(
            (idx, token) for idx, token in enumerate(list(global_counter))
        )
        self.token_to_idx = dict(
            (token, idx) for idx, token in enumerate(list(global_counter))
        )
        self.missing_idx = self.num_tokens


# Counter object for word counts
def counter_tf(sentences: t.List[str]) -> Counter:
    corpus = " ".join(sentences)
    return Counter(corpus.split(" "))


# Counter object for document counts
def counter_df(sentences: t.List[str]) -> Counter:
    corpus = " ".join([" ".join(set(s.split(" "))) for s in sentences])
    return Counter(corpus.split(" "))


# Transform counter to np.array
def counter_to_nparray(vocab: Vocabulary, counter: Counter) -> np.array:
    vector = np.zeros(vocab.num_tokens)
    for idx, token in enumerate(list(counter)):
        if 0 <= vocab.token_to_idx.get(token, vocab.missing_idx) < vocab.num_tokens:
            vector[vocab.token_to_idx.get(token, vocab.missing_idx)] += counter.get(
                token
            )
    return vector


# lambda functions for array processing (TODO: may move elsewhere)
def np_divide_by_token_value(
    counter: Counter, token: str
) -> t.Callable[[np.array], np.array]:
    normalize_factor = counter.get(token, 1)
    return lambda v: v / normalize_factor


def np_add_laplace_smoothing(smoothing: int = 1) -> t.Callable[[np.array], np.array]:
    return lambda v: v + smoothing
