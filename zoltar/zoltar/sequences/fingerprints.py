import json
import typing

TOKENS_TO_TRUNCATE = ["mw_", "vp_", "chat_", "zz_"]
DEFAULT_STOPWORD = "zzend"


def twilight_token_stemmer(token: str, prefix_list=None) -> str:
    prefix_list = prefix_list or TOKENS_TO_TRUNCATE
    clean_token = token
    for k in prefix_list:
        if token.startswith(k):
            return k
    return clean_token


def _deser_events(fingerprint: str):
    try:
        fp = json.loads(fingerprint)
    except json.JSONDecodeError:
        return []
    return fp["events"] if "events" in fp else fp


def _transform_valid_fp(events_arr=typing.List[typing.Dict[str, typing.Any]]):

    token_latencies = [
        (twilight_token_stemmer(i["k"]), int(i["v"])) for i in events_arr
    ]
    # sort by descending latency
    clean_tokens, latencies = zip(
        *sorted(token_latencies, key=lambda x: x[1], reverse=True)
    )
    return list(clean_tokens) + ["<eos>"], list(latencies) + [0]


def decompose_twilight_fingerprint(
    fingerprint: str,
) -> typing.Dict[str, typing.List[typing.Any]]:
    events_arr = _deser_events(fingerprint)
    if events_arr:
        tokens, latencies = _transform_valid_fp(events_arr)
    else:
        tokens = []
        latencies = []
    return {"tokens": tokens, "latencies": latencies}


# processes raw request input fingerprint to a sentence -- passed to input_fn


def transform_twilight_fingerprint(
    fingerprint: str,
    stopword: str = DEFAULT_STOPWORD,
    requires_decryption: bool = False,
) -> str:
    events_arr = _deser_events(fingerprint)
    if events_arr:
        sentence = " ".join([twilight_token_stemmer(i["k"]) for i in events_arr])
        return sentence + " " + stopword
    return ""
