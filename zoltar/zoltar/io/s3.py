import logging
import typing
from datetime import datetime

import boto3
from mantra.plugins.hooks.aws_hook import get_aws_session
from mantra.plugins.hooks.S3_hook import S3Hook

from zoltar.logging import delegate_log


def s3_copy(
    src_bucket: str,
    src_key: str,
    dst_bucket: str,
    dst_key: str,
    session: boto3.Session = None,
    logger: logging.Logger = None,
    s3_hook: S3Hook = None,
):
    if not s3_hook:
        if not session:
            session = get_aws_session()

        s3_hook = S3Hook(client=session.client("s3"))
    src_uri = s3_hook.gen_uri(src_bucket, src_key)
    dst_uri = s3_hook.gen_uri(dst_bucket, dst_key)
    delegate_log(f"copying {src_uri} to {dst_uri}", logger)
    s3_hook.copy(src_uri, dst_uri)


def s3_copy_prefix(
    src_bucket: str,
    src_prefix: str,
    dst_bucket: str,
    session: boto3.Session = None,
    logger: logging.Logger = None,
):
    if not session:
        session = get_aws_session()
    s3_hook = S3Hook(client=session.client("s3"))
    src_keys, truncated = s3_hook.get_keys_from_prefix(src_bucket, prefix=src_prefix)
    if truncated:
        delegate_log(
            "more than 1000 objects under prefix, truncated", log_level=logging.WARNING
        )
    for src_key in src_keys:
        s3_copy(
            src_bucket,
            src_key,
            dst_bucket,
            src_key,
            session=session,
            logger=logger,
            s3_hook=s3_hook,
        )


class StageLogger:
    def __init__(
        self,
        stage_logs_prefix: str,
        latest_pointer: str = "latest",
        base_version: typing.Optional[str] = None,
        s3_bucket: typing.Optional[str] = None,
    ):
        self.stage_logs_prefix = stage_logs_prefix
        self.latest_pointer = f"{stage_logs_prefix}/{latest_pointer}.json"
        self._base_version = base_version
        self._s3_bucket = s3_bucket
        self._s3_hook = S3Hook()
        if self._s3_bucket:
            self._s3_hook.init_bucket(self._s3_bucket)
        self._meta: typing.Dict[str, typing.Any] = {}
        self._run_version = None

    @staticmethod
    def _get_formatted_ts():
        return datetime.utcnow().strftime("%Y%m%dT%H%M%S")

    @property
    def run_version(self):
        if self._run_version is None:
            run_ts = self._get_formatted_ts()
            self._run_version = (
                f"{self._base_version}_{run_ts}"
                if self._base_version is not None
                else run_ts
            )
        return self._run_version

    def gen_identifier(self, base_id):
        run_ts = self._get_formatted_ts()
        return f"{base_id}_{run_ts}"

    # TODO: version and s3bucket should be encapsulated, deprecate as params
    # optional positional args for backwards compatability

    def register_completed_log(
        self,
        metadata: typing.Dict[str, typing.Any],
        version: typing.Optional[str],
        s3bucket: typing.Optional[str],
    ) -> None:
        _meta = metadata.copy()
        _meta["version"] = self.run_version if version is None else version
        bucket = self._s3_bucket if s3bucket is None else s3bucket
        self._meta = _meta
        self._persist_meta(bucket)

    def _persist_meta(self, bucket):
        self._s3_hook.write_json(self._meta, bucket, self.latest_pointer)
        # log for versioning
        self._s3_hook.write_json(
            self._meta, bucket, f"{self.stage_logs_prefix}/{self._meta['version']}.json"
        )

    def update_log(self, update_meta: typing.Dict[str, typing.Any]):
        if not self._meta:
            _ = self.fetch_latest_log(None, "ALL")
        self._meta.update(update_meta)
        self._persist_meta(self._s3_bucket)

    def fetch_latest_log(self, s3bucket: typing.Optional[str], key: str) -> typing.Any:
        bucket = self._s3_bucket if s3bucket is None else s3bucket
        latest_meta = self._s3_hook.read_json(bucket, self.latest_pointer)
        self._meta = latest_meta
        if key == "ALL":
            return latest_meta
        return latest_meta[key]
