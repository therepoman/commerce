import datetime
import logging
import math
import typing as t

import pandas as pd
from mantra.plugins.hooks.postgres_hook import PostgresHook


def _extract_datetime(df: pd.DataFrame) -> datetime.datetime:
    return pd.to_datetime(df.values[0])[0].to_pydatetime()


def _extract_bigint(df: pd.DataFrame) -> int:
    return df.values[0][0]


def _extract_group_stats(df: pd.DataFrame):
    return df.values.tolist()


def _construct_batch_filters(
    datasize: int, target_rows_per_batch: int, rand_column: str = "random_index"
) -> t.List[str]:
    """
    Constructs a list of where clauses, one per batch subset of an entire random dataset
    :param dataset: Size of the dataset
    :param target_rows_per_batch: Number of rows per batch
    :param rand_column: column containing random number between 0 and 1
    :return: list of filters (to be used in a where clause)
    """
    nbatches = math.ceil(datasize / target_rows_per_batch)
    logging.info(
        f"datasize = {datasize}. batch_size = {target_rows_per_batch}. {nbatches} batches."
    )
    return [
        f"""{rand_column}*{datasize} >= {b}*{target_rows_per_batch} and {rand_column}*{datasize} < {b+1}*{target_rows_per_batch}"""
        for b in range(nbatches)
    ]


def _construct_balanced_sampling_case_filter(
    group_stats, target_col: str, target_rows_per_group: int
):
    """
    Constructs a case statement for balanced sampling, returning the same number of rows per group value.
    :param group_stats: List of group counts, one for each possible value of the categorical variable
    :param target_col: Column representing the group variable (categorical variable) to balance
    :param target_rows_per_group: Estimated number of rows to return per group
    :return: Case statement for determining whether a rows should to be included in the random sample
"""
    logging.info(group_stats)
    sql_groups = [
        f"""when {target_col} = '{i[0]}' then {target_rows_per_group/i[1]}"""
        for i in group_stats
    ]
    return "case \n" + "\n".join(sql_groups) + "\n else null end"


def _generate_sql_checks(metrics: t.List[str]) -> str:
    return " and ".join([f"{i} is not null" for i in metrics])


def sql_last_avail(
    table_handle: str,
    metrics: t.List[str],
    time_column: str = "day",
    max_date_column: str = "max_date",
) -> str:
    sql_checks = _generate_sql_checks(metrics)
    return f"select max({time_column}) as {max_date_column} from {table_handle} where {sql_checks}"


def sql_select_most_recent(
    table_handle: str, metrics: t.List[str], recency_col="recent_day_number"
) -> str:
    sql_checks = f"{recency_col} = 1 and " + _generate_sql_checks(metrics)
    return f"select * from {table_handle} where {sql_checks}"


def sql_most_recent_for_partition(
    partition_col: str = "user_id",
    time_column: str = "day",
    recency_col: str = "recent_day_number",
) -> str:
    return f"row_number() over(partition by {partition_col} order by {time_column} desc) as {recency_col}"


def sql_group_counts(
    table_handle: str,
    target_col: str,
    label_col: str = "label",
    weight_col="group_weight",
):
    return f"""select {target_col} as {label_col}, sum(1) as {weight_col} from {table_handle} group by 1"""


def sql_count(table_handle: str):
    return f"""select sum(1) as n from {table_handle}"""


def sql_batch_filters(
    table_handle: str,
    target_rows_per_batch: int,
    tap: PostgresHook,
    rand_column: str = "random_index",
):
    datasize = total_rows(table_handle, tap)
    return _construct_batch_filters(datasize, target_rows_per_batch, rand_column)


def sql_balanced_sampling_case_filter(
    table_handle: str, target_col: str, target_rows_per_group: int, tap: PostgresHook
):
    group_stats = _extract_group_stats(
        tap.get_pandas_df(sql_group_counts(table_handle, target_col))
    )
    return _construct_balanced_sampling_case_filter(
        group_stats, target_col, target_rows_per_group
    )


# TODO (Breaking change) rename get_total_rows
def total_rows(table_handle: str, tap: PostgresHook):
    sql_counts = f"""select sum(1) as n from {table_handle}"""
    return _extract_bigint(tap.get_pandas_df(sql_counts))


# TODO (Breaking change) rename get_last_available
def last_available(
    table_handle: str, metrics: t.List[str], tap: PostgresHook, time_column="day"
) -> datetime.datetime:
    return _extract_datetime(
        tap.get_pandas_df(sql_last_avail(table_handle, metrics, time_column))
    )
