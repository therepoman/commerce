import logging
import typing as t
from dataclasses import dataclass
from datetime import datetime

import pandas as pd
from mantra.plugins.hooks.S3_hook import S3Hook
from mantra.plugins.hooks.sts_hook import assume_role_chain
from mantra.plugins.hooks.tahoe_hook import (
    CreateParquetTableFromTSVParams,
    TahoeHook,
    TahoeProducerConf,
)
from pytz import timezone

from zoltar.io.s3 import s3_copy


def get_formatted_ts(tz=timezone("US/Pacific")):
    return datetime.now(tz).strftime("%Y%m%dT%H%M%S")


@dataclass(frozen=True)
class CSVtoTahoeConfigs:
    output_table: str
    output_identifier_key: str
    s3_bucket: str
    s3_key: str = "zoltar/default_publishing.csv"
    column_definitions_s3_key: str = "/zoltar/default_coldef.json"


def publish_csv_to_s3_for_tahoe(
    df: pd.DataFrame, io_configs: CSVtoTahoeConfigs, tahoe_coldef: t.List[t.Dict]
):
    # parse configs
    s3_bucket = io_configs.s3_bucket
    s3_key = io_configs.s3_key
    column_definitions_s3_key = io_configs.column_definitions_s3_key

    # write csv
    logging.info(io_configs)
    s3hook = S3Hook()
    s3hook.write_df_to_csv(
        df,
        s3_bucket,
        s3_key,
        separator="\t",
        replace=True,
        keep_index=False,
        keep_cols=False,
    )

    # also write the column definitions
    s3hook.write_json(tahoe_coldef, s3_bucket, column_definitions_s3_key)


def publish_csv_in_s3_to_tahoe(
    io_configs: CSVtoTahoeConfigs,
    producer_conf: TahoeProducerConf,
    internal_bucket: str,
    env: str,
):
    # internal_bucket, ex = "commercescienceaws-tahoe-export"
    # env, ex = dev, prod,
    logging.info(io_configs)
    iam_role = producer_conf.iam_role
    published_csv_bucket = io_configs.s3_bucket
    published_csv_key = io_configs.s3_key
    coldef_key = io_configs.column_definitions_s3_key
    output_identifier_key = io_configs.output_identifier_key
    table_name = io_configs.output_table

    # role chain
    copy_session = assume_role_chain(iam_role)

    # copy from where it's written to tahoe (internal side)
    tahoe_internal_bucket = internal_bucket
    tahoe_internal_key = f"{env}/{table_name}/{output_identifier_key}"
    s3_copy(
        published_csv_bucket,
        published_csv_key,
        tahoe_internal_bucket,
        tahoe_internal_key,
    )

    # copy from tahoe (internal side) to DI's tahoe bucket for invoking rundag
    tahoe_staging_bucket = "tahoe-input-331582574546"
    tahoe_staging_key = f"{producer_conf.application_name}/{table_name}/{output_identifier_key}/{get_formatted_ts()}"
    s3_copy(
        tahoe_internal_bucket,
        tahoe_internal_key,
        tahoe_staging_bucket,
        tahoe_staging_key,
        session=copy_session,
    )

    # set up and invoke rundag
    s3hook = S3Hook()
    column_definitions = s3hook.read_json(published_csv_bucket, coldef_key)
    table_conf = CreateParquetTableFromTSVParams(
        column_definitions=column_definitions,
        table_name=table_name,
        table_schema=producer_conf.application_name,
        identifier_key=output_identifier_key,
    )

    tahoe_hook = TahoeHook(producer_conf)

    response = tahoe_hook.create_parquet_table_from_tsv(params=table_conf)
    # todo: remove backup when we're confident we can remove it
    # response = backup_tahoe_hook._get_body_redshift_to_parquet_table(producer_conf, table_conf)

    return response
