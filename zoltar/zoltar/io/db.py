import json
import typing as t
from dataclasses import asdict, dataclass

from mantra.plugins.hooks.aws_secret_manager import AwsSecretsManagerHook
from mantra.plugins.hooks.postgres_hook import PostgresHook


@dataclass(frozen=True)
class PGConfig:
    host: t.Optional[str] = None
    user: t.Optional[str] = None
    password: t.Optional[str] = None
    port: t.Optional[str] = None
    dbname: t.Optional[str] = None
    cluster_identifier: t.Optional[str] = None
    sslmode: t.Optional[str] = None
    sslrootcert: t.Optional[str] = None


@dataclass(frozen=True)
class TahoeTapConfigs(PGConfig):
    secrets_manager: t.Optional[str] = None
    port: str = "5439"
    dbname: str = "product"
    cluster_identifier: t.Optional[str] = None

    def __post_init__(self):
        assert any(
            [self.secrets_manager, self.password, self.cluster_identifier]
        ), "No Auth mechanism"


def get_tahoe_tap_iam(conf: TahoeTapConfigs):
    """ Get Tahoe tap using IAM"""
    pg_conf = asdict(conf)
    pg_conf.pop("secrets_manager")
    pg_conf["iam"] = True
    tap = PostgresHook(**pg_conf)
    tap.get_conn()
    return tap


def _remove_ssl(pg_conf):
    # psycopg2 is pedantic about values, remove keys if not using
    if pg_conf["sslmode"] is None:
        pg_conf.pop("sslmode")
        pg_conf.pop("sslrootcert")
    return pg_conf


def get_tahoe_tap_pw(conf: TahoeTapConfigs, pw_key: str = None):
    """ Get Tahoe tap using password optionally fetched from SecretsManager
        using pw_key """
    secret = AwsSecretsManagerHook(
        TahoeTapConfigs.secrets_manager, as_dict=False
    ).secret
    if conf.password is None and conf.host is None:
        pg_conf = json.loads(secret)
    else:
        pg_conf = _get_pw_from_secret(conf, secret, pw_key)
    return _get_db_hook_from_conf(TahoeTapConfigs(**pg_conf))


def _get_pw_from_secret(
    conf: TahoeTapConfigs, secret: str, pw_key: t.Optional[str]
) -> t.Dict[str, t.Any]:
    pg_conf = asdict(conf)
    try:
        secrets_dict = json.loads(secret)
    except json.JSONDecodeError:
        # plain str
        pass
    else:
        secret = secrets_dict[pw_key]
    pg_conf["password"] = secret
    return pg_conf


def get_rds_hook(pg_conf: PGConfig):
    return _get_db_hook_from_conf(pg_conf)


def _get_db_hook_from_conf(pg_conf: t.Union[PGConfig, TahoeTapConfigs]) -> PostgresHook:
    pg_hook = PostgresHook(**asdict(pg_conf))
    pg_hook.get_conn()
    return pg_hook
