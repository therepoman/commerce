from functools import reduce

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd


class TablePlotter:
    def __init__(self, tap, table_name, intro_sql=""):
        self.tap = tap
        self.table_name = table_name
        self.intro_sql = intro_sql

    def get_limits(self, feature_list):
        sql_intermediates = [
            f"min({f}) as min_{f}, max({f}) as max_{f}" for f in feature_list
        ]
        sql_limits = (
            "\n select "
            + reduce(lambda x, y: x + ",\n" + y, sql_intermediates)
            + f"\n from {self.table_name};"
        )
        limits = self.tap.get_pandas_df(self.intro_sql + sql_limits)
        return limits

    @staticmethod
    def plot_me(df, xvar, yvar, v, extent):
        fig = plt.figure(figsize=(8, 8))
        ax = fig.add_subplot(111)
        arr = pd.pivot(df, index=yvar, columns=xvar, values=v).fillna(0)
        ax.matshow(arr, origin="lower", extent=extent)
        ax.set_aspect("auto")
        plt.show()
        return arr

    @staticmethod
    def parse_query_limits(limits, feature, nbins=10.0):
        pmax = limits[f"max_{feature}"][0]
        pmin = limits[f"min_{feature}"][0]
        bins = np.arange(pmin, pmax + (pmax - pmin) / nbins, (pmax - pmin) / nbins)
        query = f"floor( ({feature}-{pmin})/{pmax-pmin}*{nbins})"
        return (query, pmin, pmax, bins)

    def build_2dhist_plot(self, xvar, yvar, limits, nbins=10):
        (x_query, x_min, x_max, x_bins) = self.parse_query_limits(limits, xvar, nbins)
        (y_query, y_min, y_max, y_bins) = self.parse_query_limits(limits, yvar, nbins)
        sql_2dhist = (
            f"select\n"
            f"        {x_query} as {xvar}_bin,\n"
            f"        {y_query} as {yvar}_bin,\n"
            f"        sum(1) as n\n"
            f"        from {self.table_name}\n"
            f"        where \n"
            f"        {xvar} is not null and {yvar} is not null\n"
            f"        group by 1,2 order by 1,2;"
        )

        df = self.tap.get_pandas_df(self.intro_sql + sql_2dhist)
        arr = self.plot_me(
            df, f"{xvar}_bin", f"{yvar}_bin", "n", extent=[x_min, x_max, y_min, y_max]
        )
        return arr
