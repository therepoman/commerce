import typing as t
from dataclasses import dataclass
from os import path

from mantra.utils.file_utils import read_yaml
from mantra.utils.utils import merge_deep_dictionaries


@dataclass()
class ReplaceRule:
    conditional: t.Callable[[str], bool]
    replacement: t.Callable[[str], str]


def rule_abs_path(app_dir):
    return ReplaceRule(
        conditional=lambda value: value.endswith("path"),
        replacement=lambda value: path.join(app_dir, value),
    )


def rule_s3_bucket(env):
    return ReplaceRule(
        conditional=lambda value: value.endswith("s3_bucket"),
        replacement=lambda value: f"{env}-{value}",
    )


class LocalConfigLoader:
    def __init__(self, env, replace_rules: t.List[ReplaceRule]):
        self.env = env
        self.replace_rules = replace_rules

    def map_keys(self, config):
        """ Recursively finds and replaces dictionary keys based on replace_rules
        where keys define both key suffix and path type, while values represent
        strings appended to path."""
        for key, value in config.copy().items():
            # todo: dict is mutable, can probably do this more functionally
            if isinstance(value, dict):
                self.map_keys(value)
            for rule in self.replace_rules:
                if rule.conditional(key):
                    config[key] = rule.replacement(value)
        return config

    def load_environment_configs(self, configs_path):
        configs = read_yaml(configs_path)
        default_configs = configs["default"]
        overrides = configs[self.env]
        return self.merge_config(default_configs, overrides)

    def merge_config(self, default_config, override):
        override = override or {}
        return self.map_keys(merge_deep_dictionaries(default_config, override))
