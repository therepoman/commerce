""" string related utils"""


def under2dash(input_str: str) -> str:
    return input_str.replace("_", "-")
