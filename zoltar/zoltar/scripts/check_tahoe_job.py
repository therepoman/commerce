import argparse

import boto3

from zoltar.commerce_config import get_commerce_tahoe_producer

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-j", "--job")
    args = parser.parse_args()

    if args.job:
        boto3.setup_default_session(profile_name="twitch-commerce-science-aws")
        print(get_commerce_tahoe_producer().check_tahoe_job(args.job))
