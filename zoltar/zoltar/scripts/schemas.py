#!/usr/bin/env python3

import argparse

from mantra.plugins.hooks.aws_secret_manager import AwsSecretsManagerHook
from mantra.plugins.hooks.postgres_hook import PostgresHook


def tahoe_tap(tap_secrets: str = "redshift-commerce-ml") -> PostgresHook:
    secrets_manager = AwsSecretsManagerHook(tap_secrets)
    tap_config = dict(
        (i, secrets_manager.get(i))
        for i in ["host", "user", "dbname", "port", "password"]
    )
    return PostgresHook(**tap_config)


def _sql_create_schema(schema: str, sudo_name: str = "twitcher") -> str:
    return f"""create schema if not exists {schema} authorization {sudo_name};
     grant all on schema {schema} to public;
     alter default privileges in schema {schema} grant all on tables to public;
     """


def _sql_drop_schema(schema: str) -> str:
    return f"""drop schema if exists {schema};"""


def _sql_schemas_owned_by_us() -> str:
    return "select * from pg_namespace where nspowner=100"


def _sql_schema_permissions_by_user(schema: str, username: str) -> str:
    if schema and username:
        where_clause = f"nspname = {schema} and usename = {username}"
    elif schema:
        where_clause = f"nspname = {schema}"
    elif username:
        where_clause = f"usename = {username}"
    else:
        where_clause = "true"

    return f"""SELECT
        u.usename,
        s.nspname,
        has_schema_privilege(u.usename,s.nspname,'create') AS user_has_select_permission,
        has_schema_privilege(u.usename,s.nspname,'usage') AS user_has_usage_permission
    FROM
        pg_user u
    CROSS JOIN
        ({_sql_schemas_owned_by_us()}) s
    where {where_clause}
    """


if __name__ == "__main__":
    tap = tahoe_tap()
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-l", "--list", dest="listall", action="store_true", default=False
    )
    parser.add_argument("-d", "--drop")
    parser.add_argument("-c", "--create")
    args = parser.parse_args()

    if args.listall:
        print(tap.get_pandas_df(_sql_schemas_owned_by_us()))
    if args.drop:
        print("Are you sure?")
        cmd = input()
        assert cmd in ["yes", "y"]
        tap.run(_sql_drop_schema(args.drop))
    if args.create:
        tap.run(_sql_create_schema(args.create))
