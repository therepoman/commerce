#!/usr/bin/env python3

import argparse

import pandas as pd
from mantra.plugins.hooks.aws_secret_manager import AwsSecretsManagerHook
from mantra.plugins.hooks.postgres_hook import PostgresHook


def tahoe_tap(tap_secrets: str = "redshift-commerce-ml") -> PostgresHook:
    secrets_manager = AwsSecretsManagerHook(tap_secrets)
    tap_config = dict(
        (i, secrets_manager.get(i))
        for i in ["host", "user", "dbname", "port", "password"]
    )
    return PostgresHook(**tap_config)


def get_tables_in_schema(tap: PostgresHook, schema: str, table_prefix="") -> pd.Series:
    if table_prefix == "":
        print(f"All tables in {schema}!")

    sql = f"""
          SET search_path TO '{schema}';
          SELECT DISTINCT tablename
          FROM pg_table_def
          WHERE schemaname in ('{schema}') and left(tablename,{len(table_prefix)})='{table_prefix}';
          """
    return tap.get_pandas_df(sql)["tablename"]


def _sql_drop(schema: str, table: str) -> str:
    return f"drop table {schema}.{table};\n"


if __name__ == "__main__":
    tap = tahoe_tap()
    parser = argparse.ArgumentParser()
    parser.add_argument("-s", "--schema")
    parser.add_argument("-p", "--prefix", default="")
    parser.add_argument("-d", "--drop", dest="drop", action="store_true", default=False)
    args = parser.parse_args()

    if args.schema:
        tables_to_drop = get_tables_in_schema(tap, args.schema, args.prefix)
        for t in tables_to_drop:
            print(t)

        if args.drop:
            print("Are you sure?")
            cmd = input()
            assert cmd in ["yes", "y"]
            drop_queries = [_sql_drop(args.schema, t) for t in tables_to_drop]
            tap.run(drop_queries)
