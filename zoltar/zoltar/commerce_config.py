from mantra.plugins.hooks.aws_secret_manager import AwsSecretsManagerHook
from mantra.plugins.hooks.tahoe_hook import TahoeHook, TahoeProducerConf

from zoltar.io.db import TahoeTapConfigs, get_tahoe_tap_iam

TAHOE_EXPORT_S3_BUCKET = "commercescienceaws-tahoe-export"
REDSHIFT_S3_IAM = "arn:aws:iam::348802193101:role/RedshiftS3"


def get_commerce_ml_tap():
    return get_tahoe_tap_iam(
        TahoeTapConfigs(
            secrets_manager="cpds-tahoe",
            # host='10.0.108.69',
            host="localhost",
            user="twitcher",
            cluster_identifier="commerce-ml",
        )
    )


def get_commerce_tahoe_producer():
    return TahoeHook(
        TahoeProducerConf(
            application_name="commercescienceaws",
            iam_role="arn:aws:iam::331582574546:role/producer-commercescienceaws",
            api_key=AwsSecretsManagerHook("tahoe_producer_key", as_dict=False).secret,
            principal_role="arn:aws:iam::348802193101:root",
        )
    )
