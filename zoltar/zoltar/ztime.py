""" date and time related utils"""
from datetime import datetime, timedelta

import pytz

# TODO: merge these with StageLogger ??


def today_minus_delta(delta=timedelta(days=1), tz=pytz.timezone("US/Pacific")):
    today = datetime.now(tz=tz)
    if delta:
        return today - delta


def now_pst():
    return datetime.now(tz=pytz.timezone("US/Pacific"))


def today_pst_ts():
    return now_pst().strftime("%Y-%m-%d")


def get_run_version(base_version):
    run_path = datetime.utcnow().strftime("%Y%m%dT%H%M%S")
    return f"{base_version}_{run_path}"


def output_table_vers(run_version, s3hook, bucket, base_key):
    latest_vers = {"version": run_version}
    # pointer for consumers
    s3hook.write_json(latest_vers, bucket, f"{base_key}/staged_tables/latest.json")
    # log of all staged tables for debugging
    s3hook.write_json(
        latest_vers, bucket, f"{base_key}/staged_tables/{run_version}.json"
    )


def get_data_vers(s3hook, bucket, base_key, version="latest"):
    return s3hook.read_json(bucket, f"{base_key}/staged_tables/{version}.json")[
        "version"
    ]
