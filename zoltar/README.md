# zoltar
Making Commerce BIG with ML


![Zoltar](https://media4.s-nbcnews.com/i/newscms/2018_17/1334783/big-movie-today-180425-tease_34f86a248916dea0d26263e9fbf1e357.jpg)

If you're making a breaking change, add a new namespace instead.

pip3 install zoltar --ignore-installed --no-cache-dir

artifactory: https://packages.internal.justin.tv/artifactory/webapp/#/search/quick/eyJzZWFyY2giOiJxdWljayIsInF1ZXJ5Ijoiem9sdGFyIn0=

jenkins: https://jenkins.internal.justin.tv/job/commerce/job/zoltar/job/master/
