## Get Bits Enabled Channel IDs
This script is used to get the list of all the channels with Bits enabled into a csv file.
To run this script, make sure you have the correct Payday AWS credentials at `~/.aws/credentials` such that:
```sh
[twitch-bits-aws]
aws_access_key_id = {id}
aws_secret_access_key = {key}
```

Once you have the credentials, simply run `go run main.go` in this directory. Available arguments are below:
```sh
-region, -r: region (e.g. "us-west-2")
-environment, -e: environment (e.g. "prod", "staging")
-out, -o: name of the output csv file (e.g. "tuids.csv")

``` 

As such, an example of getting prod data would be:
```sh
go run main.go -e=prod -o=output.csv
```

Note that environment defaults to `staging`, output file to `tuids.csv`, region to `us-west-2` unless specified.