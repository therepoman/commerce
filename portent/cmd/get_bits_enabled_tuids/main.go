package main

import (
	"encoding/csv"
	"fmt"
	"os"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/dynamo"
	"github.com/codegangsta/cli"
)

func main() {
	app := cli.NewApp()
	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:  "region, r",
			Usage: "Payday region (default to us-west-2)",
			Value: "us-west-2",
		},
		cli.StringFlag{
			Name:  "environment, e",
			Usage: "Specifies the environment (either 'staging' or 'prod')",
			Value: "staging",
		},
		cli.StringFlag{
			Name:  "out, o",
			Usage: "The name of the output csv file (default to tuids.csv)",
			Value: "tuids.csv",
		},
	}
	app.Version = "v0.0.1"
	app.Action = run
	app.Run(os.Args)
}

func run(ctx *cli.Context) {
	log.Info(fmt.Sprintf("Region: %s", ctx.String("region")))
	log.Info(fmt.Sprintf("Environment: %s", ctx.String("environment")))
	log.Info(fmt.Sprintf("Output file: %s", ctx.String("out")))
	tablePrefix := "test"
	if ctx.String("environment") == "prod" {
		tablePrefix = "prod"
	}

	dynamoClient := dynamo.NewClient(&dynamo.DynamoClientConfig{
		AwsRegion:   ctx.String("region"),
		TablePrefix: tablePrefix,
	})

	channelDao := dynamo.NewChannelDao(dynamoClient)
	log.Info("Calling Payday dynamo...")
	channels, err := channelDao.GetAll()
	if err != nil {
		log.WithError(err).Panic("Error calling GetAll()")
	}
	log.Info("Finished dynamo call. Writing to CSV...")

	outputFile, err := os.Create(ctx.String("out"))
	if err != nil {
		log.WithError(err).Panic("Error creating output csv file")
	}
	defer outputFile.Close()

	csvWriter := csv.NewWriter(outputFile)
	defer csvWriter.Flush()

	for _, channel := range channels {
		if channel == nil {
			continue
		}

		if channel.OptedOut != nil && *channel.OptedOut {
			continue
		}

		err := csvWriter.Write([]string{string(channel.Id)})
		if err != nil {
			log.WithError(err).Panic(fmt.Sprintf("Error writing channel id (%s) to csv", string(channel.Id)))
		}
	}
	log.Info("Finished! Bye")
}
