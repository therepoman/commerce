package main

import (
	"bytes"
	"context"
	"encoding/csv"
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/portent/analyzer"
	"code.justin.tv/commerce/portent/models"
	"code.justin.tv/common/spade-client-go/spade"
	"code.justin.tv/foundation/twitchclient"
	gds "code.justin.tv/gds/gds/panels/pms/client"
	"github.com/codegangsta/cli"
	"golang.org/x/time/rate"
)

const (
	PMS_ENDPOINT_STAGING = "https://panels-manager-staging.twitch.tv"
	PMS_ENDPOINT_PROD    = "https://panels-manager.prod.us-west2.justin.tv"
)

var CSVHeaders = []string{
	"User ID",
	"Panel ID",
	"Donation Flag",
	"Source",
	"Field",
	"Context",
	"URLs",
	"Score",
}

var CSVLinkHeaders = []string{
	"User ID",
	"Panel ID",
	"Title",
	"Image",
	"Link",
	"Description",
	"Links",
}

func main() {
	app := cli.NewApp()
	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:  "environment, e",
			Usage: "Specifies the environment (either 'staging' or 'prod')",
			Value: "staging",
		},
		cli.StringFlag{
			Name:  "in, i",
			Usage: "The name of the input csv file (default to tuids.csv)",
			Value: "tuids.csv",
		},
		cli.StringFlag{
			Name:  "out, o",
			Usage: "The name of the output csv file (default to results.csv)",
			Value: "results.csv",
		},
		cli.StringFlag{
			Name:  "urls, u",
			Usage: "The name of the urls output csv file (default to urls.csv)",
			Value: "urls.csv",
		},
		cli.IntFlag{
			Name:  "tps, t",
			Usage: "The TPS at which you call GDS for the given tuids (default to 10)",
			Value: 10,
		},
		cli.BoolFlag{
			Name:  "spade, s",
			Usage: "Boolean indicating if the data should be sent to spade or not.",
		},
	}
	app.Version = "v0.0.1"
	app.Action = run
	app.Run(os.Args)
}

func run(ctx *cli.Context) {
	shouldTrackSpade := ctx.Bool("spade")
	log.Info(fmt.Sprintf("Environment: %s", ctx.String("environment")))
	log.Info(fmt.Sprintf("Input file: %s", ctx.String("in")))
	log.Info(fmt.Sprintf("Output file: %s", ctx.String("out")))
	log.Info(fmt.Sprintf("TPS: %d", ctx.Uint("tps")))
	log.Info(fmt.Sprintf("Spade: %t", shouldTrackSpade))
	pmsHost := PMS_ENDPOINT_STAGING
	if ctx.String("environment") == "prod" {
		pmsHost = PMS_ENDPOINT_PROD
	}

	file, err := ioutil.ReadFile(ctx.String("in"))
	if err != nil {
		log.WithError(err).Panic(fmt.Sprintf("Error reading file: %s", ctx.String("in")))
	}

	r := csv.NewReader(bytes.NewReader(file))
	input, err := r.ReadAll()
	tuids := make([]string, 0)
	for _, column := range input {
		if len(column) < 1 {
			continue
		}
		tuids = append(tuids, column[0])
	}

	outputFile, err := os.Create(ctx.String("out"))
	if err != nil {
		log.WithError(err).Panic("Error creating output csv file")
	}
	defer outputFile.Close()

	csvWriter := csv.NewWriter(outputFile)
	defer csvWriter.Flush()

	linksFile, err := os.Create("links.csv")
	if err != nil {
		log.WithError(err).Panic("Error creating links csv file")
	}
	defer linksFile.Close()

	csvLinksWriter := csv.NewWriter(linksFile)
	defer csvLinksWriter.Flush()

	config := twitchclient.ClientConf{
		Host: pmsHost,
	}
	pmsClient, err := gds.NewClient(config)
	if err != nil {
		log.WithError(err).Panic("Error initializing PMS client")
	}

	spadeClient, err := spade.NewClient()
	if err != nil || spadeClient == nil {
		log.WithError(err).Panic("Failed to create Spade client")
	}

	log.Info("Calling PMS...")
	csvWriter.Write(CSVHeaders)
	csvLinksWriter.Write(CSVLinkHeaders)
	limiter := rate.NewLimiter(rate.Limit(float64(ctx.Int("tps"))), ctx.Int("tps"))
	start := time.Now()
	for i := 0; i < len(tuids); {
		if !limiter.Allow() {
			continue
		}
		acceptsDonations := false
		tuid := tuids[i]
		channelPanels, err := pmsClient.GetChannelPanels(context.Background(), tuid, false, false, nil)
		if err != nil {
			log.WithError(err).Error(fmt.Sprintf("Error calling GetChannelPanels for tuid: %s", tuid))
			continue
		}

		for _, panel := range channelPanels.Panels {
			// we are only interested in "just" panel, not things like extensions
			if panel.Kind != "default" {
				continue
			}

			analysis := analyzer.Analyze(panel)
			allLinks := analyzer.AnalyzeLinks(panel)

			for _, field := range analysis.Fields {
				acceptsDonations = true
				entry := []string{
					strconv.Itoa(panel.UserID),
					strconv.Itoa(panel.ID),
					"true",
					"Panels",
					field.Field.String(),
					field.Context,
					strings.Join(field.Urls, "\n"),
					strconv.Itoa(analysis.Score),
				}
				err := csvWriter.Write(entry)

				if err != nil {
					log.WithError(err).Error(fmt.Sprintf("Error writing channel panel info (tuid: %s) to csv", strconv.Itoa(panel.UserID)))
				}

				if !shouldTrackSpade {
					continue
				}
				err = spadeClient.TrackEvent(
					context.Background(),
					models.SPADE_DONATION_LINK_EVENT_TYPE,
					models.DonationEvent{
						ChannelID:      tuid,
						DonationFlag:   true,
						SourceType:     models.PANEL,
						SourceID:       panel.ID,
						Field:          field.Field,
						ContextDetails: field.Context,
					},
				)

				if err != nil {
					log.WithError(err).Error(fmt.Sprintf("Error sending channel panel spade event for (tuid: %s)", tuid))
				}
			}

			if len(allLinks) > 0 {
				linkEntry := []string{
					strconv.Itoa(panel.UserID),
					strconv.Itoa(panel.ID),
					panel.Data.Title,
					panel.Data.Image,
					panel.Data.Link,
					panel.Data.Description,
					strings.Join(allLinks, "\n"),
				}
				err := csvLinksWriter.Write(linkEntry)
				if err != nil {
					log.WithError(err).Error(fmt.Sprintf("Error writing channel panel info (tuid: %s) to links csv", strconv.Itoa(panel.UserID)))
				}
			}

		}

		if !acceptsDonations {
			entry := []string{
				tuid,
				"",
				"false",
				"Panels",
				"",
				"",
				"",
				"0",
			}
			err := csvWriter.Write(entry)

			if err != nil {
				log.WithError(err).Error(fmt.Sprintf("Error writing no dono user info for (tuid: %s) to csv", tuid))
			}

			if shouldTrackSpade {
				err = spadeClient.TrackEvent(
					context.Background(),
					models.SPADE_DONATION_LINK_EVENT_TYPE,
					models.DonationEvent{
						ChannelID:    tuid,
						DonationFlag: false,
						SourceType:   models.PANEL,
					},
				)

				if err != nil {
					log.WithError(err).Error(fmt.Sprintf("Error sending no dono spade event for (tuid: %s)", tuid))
				}
			}

		}
		i++
	}
	elapsed := time.Since(start) / time.Millisecond
	log.Info(fmt.Sprintf("Execution time: %d ms", elapsed))
	log.Info("Finished! Bye")
}
