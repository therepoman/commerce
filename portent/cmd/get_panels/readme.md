## Get Panels info of given tuids
This script is used to get panel information of given tuids in a csv file.

To run this script, navigate to this directory and simply run `go run main.go` with available arguments below:
```sh
-environment, -e: environment (e.g. "prod", "staging")
-in, -i: name of the input csv file (e.g. "tuids.csv")
-out, -o: name of the output csv file (e.g. "panels.csv")
-tps, -t: the TPS at which the script calls GDS
-spade, -s: add this argument to send Spade tracking events. If not included, no events will be tracked.
```

As such, an example of getting prod panel information at 20 TPS would be:
```sh
go run main.go -e=prod -i=input.csv -o=output.csv -t=20
```

Note that environment defaults to `staging`, output file to `panels.csv`, input file to `tuids.csv`, TPS to 10, unless specified.
Also, the input file is expected to have one tuid per line.
e.g.) `input.csv`
```sh
122345
234567
```