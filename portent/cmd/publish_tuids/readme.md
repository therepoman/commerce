## Publish set of TUIDs to specified env SNS Topic
This script is used to publish a set of TUIDs from a CSV file into a given env SNS Topic.

To run this script, navigate to this directory and simply run `go run main.go` with available arguments below:
```sh
-environment, -e: environment (e.g. "prod", "staging")
-in, -i: name of the input csv file (e.g. "tuids.csv")
```

As such, an example of publishing tuids to the given SNS topic would be:
```sh
go run main.go -e=prod -i=input.csv
```

Note that environment defaults to `staging` and input file to `tuids.csv` unless specified.
Also, the input file is expected to have one tuid per line.
e.g.) `input.csv`
```sh
122345
234567
```

### Credential Setup
This file will read from your `~/.aws/credentials` file for connecting to the SNS Topic.

If publishing to staging, it will use the `portent-devo` profile in your credentials file.

If publishing to production, it will use the `portent-prod` profile in your credentials file.