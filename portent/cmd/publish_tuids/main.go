package main

import (
	"bytes"
	"encoding/csv"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/portent/models"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sns"
	"github.com/codegangsta/cli"
)

var stagingTopicARN = "arn:aws:sns:us-west-2:898907546878:events-dev"
var prodTopicARN = "arn:aws:sns:us-west-2:272653928523:events-prod"
var stagingProfile = "portent-devo"
var prodProfile = "portent-prod"

func main() {
	app := cli.NewApp()
	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:  "region, r",
			Usage: "Payday region (default to us-west-2)",
			Value: "us-west-2",
		},
		cli.StringFlag{
			Name:  "environment, e",
			Usage: "Specifies the environment (either 'staging' or 'prod')",
			Value: "staging",
		},
		cli.StringFlag{
			Name:  "in, i",
			Usage: "The name of the input csv file (default to tuids.csv)",
			Value: "tuids.csv",
		},
	}
	app.Version = "v0.0.1"
	app.Action = run
	app.Run(os.Args)
}

func run(ctx *cli.Context) {
	log.Info(fmt.Sprintf("Environment: %s", ctx.String("environment")))
	log.Info(fmt.Sprintf("Input file: %s", ctx.String("in")))
	env := ctx.String("environment")
	topicARN := stagingTopicARN
	profile := stagingProfile
	if env == "prod" {
		topicARN = prodTopicARN
		profile = prodProfile
	}

	log.Info("Creating Session.")
	sess := session.Must(session.NewSessionWithOptions(session.Options{
		Config:  aws.Config{Region: aws.String(ctx.String("region"))},
		Profile: profile,
	}))
	svc := sns.New(sess)
	file, err := ioutil.ReadFile(ctx.String("in"))

	if err != nil {
		log.WithError(err).Panic(fmt.Sprintf("Error reading file: %s", ctx.String("in")))
	}
	r := csv.NewReader(bytes.NewReader(file))
	input, err := r.ReadAll()
	log.Info(fmt.Sprintf("Publishing %d messages to topic %s", len(input), topicARN))
	failedCount := 0
	for _, column := range input {
		if len(column) < 1 {
			continue
		}
		tuid := column[0]
		message, err := json.Marshal(&models.PanelUpdateSNSMessage{ChannelID: tuid})
		if err != nil {
			log.WithError(err).Error(fmt.Sprintf("Error marshalling message: %s", tuid))
			failedCount++
			continue
		}
		messageString := string(message)

		_, err = svc.Publish(&sns.PublishInput{
			Message:  &messageString,
			TopicArn: &topicARN,
		})

		if err != nil {
			log.WithError(err).Error(fmt.Sprintf("Error publishing message: %s", tuid))
			failedCount++
		}
	}
	if failedCount > 0 {
		log.Error(fmt.Sprintf("Failed to publish %d messages. Check previous error for specific TUIDs.", failedCount))
	}
	log.Info("Finished! Bye")
}
