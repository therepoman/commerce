package errors

import "fmt"

type IllegalPoolSize struct {
	name string
	size int
}

func NewIllegalPoolSize(name string, size int) error {
	return &IllegalPoolSize{name, size}
}

func (i *IllegalPoolSize) Error() string {
	return fmt.Sprintf("Unable to resize pool %v: illegal size %v specified", i.name, i.size)
}
