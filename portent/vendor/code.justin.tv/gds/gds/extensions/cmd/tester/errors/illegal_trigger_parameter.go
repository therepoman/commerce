package errors

import "fmt"

type IllegalTriggerParameter struct {
	name  string
	param string
	value interface{}
}

func NewIllegalTriggerParameter(name, param string, value interface{}) error {
	return &IllegalTriggerParameter{name, param, value}
}

func (t *IllegalTriggerParameter) Error() string {
	return fmt.Sprintf(
		"Trigger %v: %v is not a legal value for \"%v\"",
		t.name,
		t.value,
		t.param,
	)
}
