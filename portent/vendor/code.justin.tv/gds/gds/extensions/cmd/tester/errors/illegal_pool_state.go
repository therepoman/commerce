package errors

import "fmt"

type IllegalPoolState struct {
	name string
}

func NewIllegalPoolState(name string) error {
	return &IllegalPoolState{name}
}

func (i *IllegalPoolState) Error() string {
	return fmt.Sprintf("Pool %v has been closed", i.name)
}

// ErrClosed        = errors.New("This queue has been closed")
// ErrOverflow      = errors.New("This queue is full")
