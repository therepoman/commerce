package documents

import "encoding/json"

// PanelKind represents the type of panel we're looking at; must be
// drawn from { "default", "teespring", or "extension" }
type PanelKind string

// Ensure that panels are of the supported types when marshaling to json
func (kind *PanelKind) MarshalJSON() ([]byte, error) {
	switch string(*kind) {
	case "default", "teespring", "extension":
		return json.Marshal(string(*kind))
	}
	return nil, &json.UnsupportedValueError{}
}

// Ensure that panels are of the supported types when reading from json
func (kind *PanelKind) UnmarshalJSON(b []byte) error {
	var s string
	err := json.Unmarshal(b, &s)
	if err != nil {
		return err
	}
	switch s {
	case "default", "teespring", "extension":
		*kind = PanelKind(s)
		return nil
	}
	return &json.UnsupportedValueError{}
}

// CreatePanelParams is the input to the CreatePanel client method
type CreatePanelParams struct {
	UserID string     `json:"user_id"`
	Kind   PanelKind  `json:"kind"`
	Data   *PanelData `json:"data"`
}

// UpdatePanelParams is the input to the UpdatePanel client method
type UpdatePanelParams struct {
	Data *PanelData `json:"data"`
}

// UpdatePanelParams is the input to the UpdatePanel client method
type UploadPanelImageParams struct {
	UserID string
	Width  uint32
	Height uint32
	Left   uint32 `json:"cropX"`
	Top    uint32 `json:"cropY"`
}
