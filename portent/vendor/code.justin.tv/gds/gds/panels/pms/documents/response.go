package documents

import "code.justin.tv/web/upload-service/rpc/uploader"

// PanelData is the union of all three types of panels' data.
// Although the backend service doesn't validate this at all,
// we can't decode the parameters in Visage unless it's an
// actual Go structure.
type PanelData struct {
	Title       string `json:"title,omitempty"`
	Image       string `json:"image,omitempty"`
	Link        string `json:"link,omitempty"`
	Description string `json:"description"`
	Slot        string `json:"slot,omitempty"`
}

// PanelDescription describes a single panel
type PanelDescription struct {
	ID              int        `json:"id"`
	UserID          int        `json:"user_id"`
	DisplayOrder    int        `json:"display_order"`
	Kind            PanelKind  `json:"kind"`
	Data            *PanelData `json:"data"`
	HTMLDescription string     `json:"html_description"`
}

// PanelDocument represents a single panel
type PanelDocument struct {
	Panel *PanelDescription `json:"panel"`
}

// PanelsDocument represents a collection of panels
type PanelsDocument struct {
	Panels []*PanelDescription `json:"panels"`
}

// ImageUploadDocument represents the returned value from the upload service
type ImageUploadDocument struct {
	UploadResponse uploader.UploadResponse `json:"upload_response"`
}
