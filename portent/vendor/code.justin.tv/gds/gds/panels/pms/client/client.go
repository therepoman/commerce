package panels

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"math/rand"
	"net/http"
	"strings"
	"time"

	"code.justin.tv/foundation/twitchclient"
	"code.justin.tv/gds/gds/panels/pms/documents"
)

const (
	defaultStatSampleRate = 0.1
	defaultTimingXactName = "panels"
)

// Client expressing actions that can be taken by the panels service
type Client interface {
	HealthCheck(ctx context.Context, reqOpts *twitchclient.ReqOpts) error
	GetPanel(ctx context.Context, panelID string, reqOpts *twitchclient.ReqOpts) (*documents.PanelDocument, error)
	GetPanels(ctx context.Context, panelIDs []string, reqOpts *twitchclient.ReqOpts) (*documents.PanelsDocument, error)
	GetChannelPanels(ctx context.Context, userID string, includeExtensions, kickCache bool, reqOpts *twitchclient.ReqOpts) (*documents.PanelsDocument, error)
	GetPanelImageURL(ctx context.Context, userID string, width, height, top, left uint32, reqOpts *twitchclient.ReqOpts) (*documents.ImageUploadDocument, error)
	CreatePanel(ctx context.Context, params *documents.CreatePanelParams, reqOpts *twitchclient.ReqOpts) (*documents.PanelDocument, error)
	UpdatePanel(ctx context.Context, panelID string, params *documents.UpdatePanelParams, reqOpts *twitchclient.ReqOpts) (*documents.PanelDocument, error)
	DeletePanel(ctx context.Context, panelID string, reqOpts *twitchclient.ReqOpts) (*documents.PanelDocument, error)
	DeletePanelInternal(ctx context.Context, panelID string, reqOpts *twitchclient.ReqOpts) (*documents.PanelDocument, error)
	OrderPanels(ctx context.Context, panelIDs []string, reqOpts *twitchclient.ReqOpts) (*documents.PanelsDocument, error)
}

type clientImpl struct {
	http twitchclient.Client
}

// NewClient creates an http client to call the validator endpoints
func NewClient(conf twitchclient.ClientConf) (Client, error) {
	if conf.TimingXactName == "" {
		conf.TimingXactName = defaultTimingXactName
	}

	twitchClient, err := twitchclient.NewClient(conf)
	if err != nil {
		return nil, err
	}

	return &clientImpl{
		http: twitchClient,
	}, nil
}

func (cli *clientImpl) HealthCheck(ctx context.Context, reqOpts *twitchclient.ReqOpts) error {
	req, err := cli.http.NewRequest("GET", "/debug/running", nil)
	if err != nil {
		return err
	}
	_, err = cli.http.Do(ctx, req, twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{}))
	return err
}

func (cli *clientImpl) GetPanel(ctx context.Context, panelID string, reqOpts *twitchclient.ReqOpts) (*documents.PanelDocument, error) {
	req, err := cli.http.NewRequest("GET", "/v1/panels/"+panelID, nil)
	if err != nil {
		return nil, err
	}
	var out documents.PanelDocument
	err = cli.execute(ctx, req, "get_panel", &out, reqOpts)
	return &out, err
}

func (cli *clientImpl) GetPanels(ctx context.Context, panelIDs []string, reqOpts *twitchclient.ReqOpts) (*documents.PanelsDocument, error) {
	req, err := cli.http.NewRequest("GET", "/v1/panels?ids="+strings.Join(panelIDs, ","), nil)
	if err != nil {
		return nil, err
	}
	var out documents.PanelsDocument
	err = cli.execute(ctx, req, "get_panels", &out, reqOpts)
	return &out, err
}

func (cli *clientImpl) GetPanelImageURL(ctx context.Context, userID string, width, height, top, left uint32, reqOpts *twitchclient.ReqOpts) (*documents.ImageUploadDocument, error) {
	bodyBytes, err := json.Marshal(documents.UploadPanelImageParams{
		UserID: userID,
		Width:  width,
		Height: height,
		Top:    top,
		Left:   left,
	})
	if err != nil {
		return nil, err
	}

	req, err := cli.http.NewRequest("POST", "/v1/panels/image/upload", bytes.NewReader(bodyBytes))
	if err != nil {
		return nil, err
	}

	var out documents.ImageUploadDocument
	err = cli.execute(ctx, req, "upload_panel_image", &out, reqOpts)
	return &out, err
}

func (cli *clientImpl) GetChannelPanels(ctx context.Context, userID string, includeExtensions, kickCache bool, reqOpts *twitchclient.ReqOpts) (*documents.PanelsDocument, error) {
	path := "/v1/users/" + userID + "/panels"
	if includeExtensions {
		path += "?include_extensions=1"

		if kickCache {
			random := rand.New(rand.NewSource(time.Now().UnixNano()))
			path += fmt.Sprintf("&kick_cache=%d", random.Int31())
		}
	} else if kickCache {
		random := rand.New(rand.NewSource(time.Now().UnixNano()))
		path += fmt.Sprintf("?kick_cache=%d", random.Int31())
	}

	req, err := cli.http.NewRequest("GET", path, nil)
	if err != nil {
		return nil, err
	}
	var out documents.PanelsDocument
	err = cli.execute(ctx, req, "get_channel_panels", &out, reqOpts)
	return &out, err
}

func (cli *clientImpl) CreatePanel(ctx context.Context, params *documents.CreatePanelParams, reqOpts *twitchclient.ReqOpts) (*documents.PanelDocument, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return nil, err
	}

	req, err := cli.http.NewRequest("POST", "/v1/panels", bytes.NewReader(bodyBytes))
	if err != nil {
		return nil, err
	}
	var out documents.PanelDocument
	err = cli.execute(ctx, req, "create_panel", &out, reqOpts)
	return &out, err
}

func (cli *clientImpl) UpdatePanel(ctx context.Context, panelID string, params *documents.UpdatePanelParams, reqOpts *twitchclient.ReqOpts) (*documents.PanelDocument, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return nil, err
	}

	req, err := cli.http.NewRequest("PUT", "/v1/panels/"+panelID, bytes.NewReader(bodyBytes))
	if err != nil {
		return nil, err
	}
	var out documents.PanelDocument
	err = cli.execute(ctx, req, "update_panel", &out, reqOpts)
	return &out, err
}

func (cli *clientImpl) DeletePanel(ctx context.Context, panelID string, reqOpts *twitchclient.ReqOpts) (*documents.PanelDocument, error) {
	req, err := cli.http.NewRequest("DELETE", "/v1/panels/"+panelID, nil)
	if err != nil {
		return nil, err
	}

	var out documents.PanelDocument
	err = cli.execute(ctx, req, "delete_panel", &out, reqOpts)
	return &out, err
}

func (cli *clientImpl) DeletePanelInternal(ctx context.Context, panelID string, reqOpts *twitchclient.ReqOpts) (*documents.PanelDocument, error) {
	req, err := cli.http.NewRequest("DELETE", "/internal/panels/"+panelID, nil)
	if err != nil {
		return nil, err
	}

	var out documents.PanelDocument
	err = cli.execute(ctx, req, "delete_panel_internal", &out, reqOpts)
	return &out, err
}

func (cli *clientImpl) OrderPanels(ctx context.Context, panelIDs []string, reqOpts *twitchclient.ReqOpts) (*documents.PanelsDocument, error) {
	req, err := cli.http.NewRequest("PUT", "/v1/panels/"+strings.Join(panelIDs, ",")+"/order", nil)
	if err != nil {
		return nil, err
	}

	var out documents.PanelsDocument
	err = cli.execute(ctx, req, "order_panels", &out, reqOpts)
	if err != nil {
		return nil, err
	}

	return &out, nil
}

func (cli *clientImpl) execute(ctx context.Context, req *http.Request, statName string, out interface{}, reqOpts *twitchclient.ReqOpts) error {
	opts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.panels." + statName,
		StatSampleRate: defaultStatSampleRate,
	})

	_, err := cli.http.DoJSON(ctx, out, req, opts)
	return err
}
