package hystrix

import afex "github.com/afex/hystrix-go/hystrix"

const (
	//Commands
	UsersCommand                         = "get_user"
	GetRecentEventCommand                = "get_recent_event"
	GetTopEventCommand                   = "get_top_event"
	UpdateRecentEventCommand             = "update_recent_event"
	UpdateTransactionsCommand            = "update_transactions"
	GetTransactionsCommand               = "get_transactions"
	GetHashTagsCommand                   = "get_tags"
	GetBalanceCommand                    = "get_balance"
	GetChannelCommand                    = "get_channel"
	GetChannelRedisCommand               = "get_channel_redis"
	UpdateHashTagsCommand                = "update_tags"
	GetHighestBadgeCommand               = "get_highest_badge"
	GetADGPricesCommand                  = "get_prices_adg"
	GetPaymentServiceUserPricesCommand   = "get_user_prices_payment_service"
	GetFloPricesCommand                  = "get_prices_flo"
	GetPaymentServicePricesCommand       = "get_prices_payment_service"
	GetRecentByTransactionId             = "get_recent_by_transaction_id"
	GetImageCommand                      = "get_image"
	GetPartnerPrefixCommand              = "get_partner_prefix"
	GetImageByAssetIdCommand             = "get_image_by_asset_id"
	GetChannelByImageSetIdCommand        = "get_channel_by_image_set"
	UpdateImageCommand                   = "update_image"
	GetTaxInfo                           = "get_tax_info"
	GetUserPayoutType                    = "get_user_payout_type"
	GetPartnerCommand                    = "get_partner"
	GetPayoutEntityForChannelCommand     = "get_payout_entity_for_channel"
	GetRewardCommand                     = "get_reward"
	GetRewardByFulfillmentAccountCommand = "get_reward_by_fulfillment_account"
	UpdateRewardCommand                  = "update_reward"
	GetBucketedLeaderboardCounts         = "get_bucketed_leaderboard_counts"
	GetLeaderboardCommand                = "get_leaderboard"
	PublishEventCommand                  = "publish_event"
	ModerateEntryCommand                 = "moderate_entry"
	GetLeaderboardBadgeHoldersCommand    = "get_leaderboard_badge_holders"
	GetNumberOfPubSubListenersCommand    = "pub_sub_listeners"

	// Extension Transactions DAO
	GetExtensionTransactionCommand = "get_extension_transaction_command"

	// OWL Commands
	OWLGetClientCommand = "owl_get_client"

	// Zuma Commands
	ZumaListModCommand        = "zuma_list_mods"
	ZumaGetModCommand         = "zuma_get_mod"
	ZumaEnforceMessageCommand = "zuma_enforce_message"

	// FABS Commands
	FABSUseBitsOnExtensionCommand    = "fabs:use_bits_on_extension"
	FABSGiveBitsToBroadcasterCommand = "fabs:give_bits_to_broadcaster"
	FABSGetBitsBalanceCommand        = "fabs:get_bits_balance"
	FABSGetTransactionsCommand       = "fabs:get_transactions"
	FABSAddBitsCommand               = "fabs:add_bits"
	FABSAdminAddBitsCommand          = "fabs:admin_add_bits"
	FABSAdminRemoveBitsCommand       = "fabs:admin_remove_bits"

	// TAILS Commands
	TAILSGetPayoutIdentityCommand         = "tails:get_payout_identity"
	TAILSGetLinkedTwitchAccountsCommand   = "tails:get_linked_twitch_accounts"
	TAILSGetTwitchUserNameCommand         = "tails:get_twitch_user_name"
	TAILSGetTwitchUserInfoCommand         = "tails:get_twitch_user_info"
	TAILSGetLinkedAmazonDirectedIdCommand = "tails:get_linked_amazon_directed_id"
	TAILSGetLinkedAmazonAccountCommand    = "tails:get_linked_amazon_account"
	TAILSUnlinkTwitchAccountCommand       = "tails:unlink_twitch_account"
	TAILSLinkTwitchAccountCommand         = "tails:link_twitch_account"

	// ADGDisco Commands
	ADGDiscoGetProductListDetails = "adgdisco:get_product_list_details"

	// Rooms Commands
	RoomsSendToRoom = "rooms:send_to_room"

	// Prometheus Commands
	PrometheusPublishEvent = "prometheus:publish_event"
	PrometheusSearch       = "prometheus:search"

	//Timeouts
	TestTimeout = 30000

	// Concurrency
	DefaultMaxConcurrent = 100
)

func init() {
	configureTimeout(UsersCommand, afex.DefaultTimeout)
	configureTimeout(GetRecentEventCommand, afex.DefaultTimeout)
	configureTimeout(GetTopEventCommand, afex.DefaultTimeout)
	configureTimeout(UpdateRecentEventCommand, 2000)
	configureTimeout(UpdateTransactionsCommand, afex.DefaultTimeout)
	configureTimeout(GetTransactionsCommand, afex.DefaultTimeout)
	configureTimeout(GetHashTagsCommand, afex.DefaultTimeout)
	configureTimeout(GetBalanceCommand, 8000)
	configure(GetChannelCommand, 3000, 600)
	configureTimeout(GetChannelRedisCommand, afex.DefaultTimeout)
	configureTimeout(UpdateHashTagsCommand, afex.DefaultTimeout)
	configureTimeout(GetHighestBadgeCommand, 2000)
	configureTimeout(GetADGPricesCommand, 4000)
	configureTimeout(GetPaymentServicePricesCommand, 4000)
	configureTimeout(GetRecentByTransactionId, afex.DefaultTimeout)
	configureTimeout(GetImageCommand, afex.DefaultTimeout)
	configureTimeout(GetImageByAssetIdCommand, afex.DefaultTimeout)
	configureTimeout(UpdateImageCommand, afex.DefaultTimeout)
	configureTimeout(GetTaxInfo, afex.DefaultTimeout)
	configureTimeout(GetPartnerPrefixCommand, afex.DefaultTimeout)
	configureTimeout(GetChannelByImageSetIdCommand, afex.DefaultTimeout)
	configureTimeout(GetUserPayoutType, 5000)
	configureTimeout(GetPartnerCommand, 5000)
	configureTimeout(GetPayoutEntityForChannelCommand, afex.DefaultTimeout)
	configureTimeout(GetRewardCommand, afex.DefaultTimeout)
	configureTimeout(UpdateRewardCommand, afex.DefaultTimeout)
	configureTimeout(PublishEventCommand, 3000)
	configureTimeout(GetLeaderboardCommand, afex.DefaultTimeout)
	configureTimeout(ModerateEntryCommand, afex.DefaultTimeout)
	configureTimeout(GetNumberOfPubSubListenersCommand, 500)

	// Extension Transactions DAO Timeouts
	configureTimeout(GetExtensionTransactionCommand, afex.DefaultTimeout)

	// OWL Timeouts
	configureTimeout(OWLGetClientCommand, afex.DefaultTimeout)

	// Zuma Timeouts
	configureTimeout(ZumaListModCommand, afex.DefaultTimeout)
	configureTimeout(ZumaGetModCommand, afex.DefaultTimeout)
	configureTimeout(ZumaEnforceMessageCommand, afex.DefaultTimeout)

	// FABS Timeouts
	configureTimeout(FABSGiveBitsToBroadcasterCommand, 3000)
	configureTimeout(FABSGetBitsBalanceCommand, 2000)
	configureTimeout(FABSGetTransactionsCommand, 2000)
	configureTimeout(FABSAddBitsCommand, 3000)
	configureTimeout(FABSAdminAddBitsCommand, 3000)
	configureTimeout(FABSAdminRemoveBitsCommand, 3000)

	// TAILS Timeouts
	configureTimeout(TAILSGetPayoutIdentityCommand, 2000)
	configureTimeout(TAILSGetLinkedTwitchAccountsCommand, 2000)
	configureTimeout(TAILSGetTwitchUserNameCommand, 2000)
	configureTimeout(TAILSGetTwitchUserInfoCommand, 2000)
	configureTimeout(TAILSGetLinkedAmazonDirectedIdCommand, 2000)
	configureTimeout(TAILSGetLinkedAmazonAccountCommand, 2000)
	configureTimeout(TAILSUnlinkTwitchAccountCommand, 4000)
	configureTimeout(TAILSLinkTwitchAccountCommand, 4000)

	// ADGDisco Timeouts
	configureTimeout(ADGDiscoGetProductListDetails, 2000)

	// Rooms Timeouts
	configureTimeout(RoomsSendToRoom, 2000)
}

func configureTimeout(cmd string, timeout int) {
	configure(cmd, timeout, DefaultMaxConcurrent)
}

func configure(cmd string, timeout int, maxConcurrent int) {
	afex.ConfigureCommand(cmd, afex.CommandConfig{
		Timeout:               timeout,
		MaxConcurrentRequests: maxConcurrent,
		ErrorPercentThreshold: afex.DefaultErrorPercentThreshold,
	})
}

func ConfigureForTests() {
	configureTimeout(UsersCommand, TestTimeout)
	configureTimeout(GetRecentEventCommand, TestTimeout)
	configureTimeout(GetTopEventCommand, TestTimeout)
	configureTimeout(UpdateRecentEventCommand, TestTimeout)
	configureTimeout(UpdateTransactionsCommand, TestTimeout)
	configureTimeout(GetTransactionsCommand, TestTimeout)
	configureTimeout(GetHashTagsCommand, TestTimeout)
	configureTimeout(GetBalanceCommand, TestTimeout)
	configureTimeout(GetChannelCommand, TestTimeout)
	configureTimeout(GetChannelRedisCommand, TestTimeout)
	configureTimeout(UpdateHashTagsCommand, TestTimeout)
	configureTimeout(GetHighestBadgeCommand, TestTimeout)
	configureTimeout(GetADGPricesCommand, TestTimeout)
	configureTimeout(GetPaymentServicePricesCommand, TestTimeout)
	configureTimeout(GetRecentByTransactionId, TestTimeout)
	configureTimeout(GetImageCommand, TestTimeout)
	configureTimeout(GetImageByAssetIdCommand, TestTimeout)
	configureTimeout(UpdateImageCommand, TestTimeout)
	configureTimeout(GetTaxInfo, TestTimeout)
	configureTimeout(GetPartnerPrefixCommand, TestTimeout)
	configureTimeout(GetChannelByImageSetIdCommand, TestTimeout)
	configureTimeout(GetUserPayoutType, TestTimeout)
	configureTimeout(GetPartnerCommand, TestTimeout)
	configureTimeout(GetPayoutEntityForChannelCommand, TestTimeout)
	configureTimeout(GetRewardCommand, TestTimeout)
	configureTimeout(UpdateRewardCommand, TestTimeout)
	configureTimeout(PublishEventCommand, TestTimeout)
	configureTimeout(GetLeaderboardCommand, TestTimeout)
	configureTimeout(ModerateEntryCommand, TestTimeout)

	// Extension Transactions DAO Timeouts
	configureTimeout(GetExtensionTransactionCommand, TestTimeout)

	// OWL Timeouts
	configureTimeout(OWLGetClientCommand, TestTimeout)

	// Zuma Timeouts
	configureTimeout(ZumaListModCommand, TestTimeout)
	configureTimeout(ZumaGetModCommand, TestTimeout)
	configureTimeout(ZumaEnforceMessageCommand, TestTimeout)

	// FABS Timeouts
	configureTimeout(FABSGiveBitsToBroadcasterCommand, TestTimeout)
	configureTimeout(FABSGetBitsBalanceCommand, TestTimeout)
	configureTimeout(FABSGetTransactionsCommand, TestTimeout)
	configureTimeout(FABSAddBitsCommand, TestTimeout)
	configureTimeout(FABSAdminAddBitsCommand, TestTimeout)
	configureTimeout(FABSAdminRemoveBitsCommand, TestTimeout)

	// TAILS Timeouts
	configureTimeout(TAILSGetPayoutIdentityCommand, TestTimeout)
	configureTimeout(TAILSGetLinkedTwitchAccountsCommand, TestTimeout)
	configureTimeout(TAILSGetTwitchUserNameCommand, TestTimeout)
	configureTimeout(TAILSGetTwitchUserInfoCommand, TestTimeout)
	configureTimeout(TAILSGetLinkedAmazonDirectedIdCommand, TestTimeout)
	configureTimeout(TAILSGetLinkedAmazonAccountCommand, TestTimeout)
	configureTimeout(TAILSUnlinkTwitchAccountCommand, TestTimeout)
	configureTimeout(TAILSLinkTwitchAccountCommand, TestTimeout)

	// ADGDisco Timeouts
	configureTimeout(ADGDiscoGetProductListDetails, TestTimeout)

	// Rooms Timeouts
	configureTimeout(RoomsSendToRoom, TestTimeout)
}
