package file

import (
	"archive/zip"
	"bytes"
	"io/ioutil"
	"os"
	"path/filepath"

	"code.justin.tv/commerce/payday/errors"
)

func ZipFile(fileName string, fileContents []byte) ([]byte, error) {
	var zipByteBuffer bytes.Buffer
	zipWriter := zip.NewWriter(&zipByteBuffer)
	internalFileWriter, err := zipWriter.Create(fileName)

	if err != nil {
		return nil, err
	}

	_, err = internalFileWriter.Write(fileContents)
	if err != nil {
		return nil, err
	}

	err = zipWriter.Close()
	if err != nil {
		return nil, err
	}

	return zipByteBuffer.Bytes(), nil
}

func NewZipReader(zippedFile []byte) (*zip.Reader, error) {
	reader := bytes.NewReader(zippedFile)
	zipReader, err := zip.NewReader(reader, int64(len(zippedFile)))
	if err != nil {
		return nil, err
	}
	return zipReader, nil
}

func ZipFileAtPath(pathToFile string) ([]byte, error) {
	contents, err := ioutil.ReadFile(pathToFile)
	if err != nil {
		return nil, err
	}

	fileName := filepath.Base(pathToFile)
	return ZipFile(fileName, contents)
}

func SearchFileInPaths(fileName string, filePaths []string) (string, error) {
	for _, filePathPrefix := range filePaths {
		filePath := filePathPrefix + fileName
		_, err := os.Stat(filePath)
		if err != nil && !os.IsNotExist(err) {
			return "", err
		} else if err == nil {
			return filePath, nil
		}
	}
	return "", errors.Newf("File %s not found in any searched path: %v", fileName, filePaths)
}
