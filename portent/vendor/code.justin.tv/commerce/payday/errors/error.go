package errors

import (
	"fmt"

	goerrors "github.com/go-errors/errors"
)

type Error struct {
	err *goerrors.Error
	msg string
}

type CausedError struct {
	err   *goerrors.Error
	msg   string
	cause error
}

func (e *CausedError) Error() string {
	if e.cause == nil {
		return fmt.Sprintf("%s \n%s", e.msg, e.err.ErrorStack())
	} else {
		return fmt.Sprintf("%s \n%s \nCaused By: %v", e.msg, e.err.ErrorStack(), e.cause)
	}
}

func (e *Error) Error() string {
	return fmt.Sprintf("%s \n%s", e.msg, e.err.ErrorStack())
}

func New(error string) *Error {
	return &Error{
		msg: error,
		err: goerrors.Wrap(error, 1),
	}
}

func Newf(f string, a ...interface{}) *Error {
	txt := fmt.Sprintf(f, a...)

	return &Error{
		err: goerrors.Wrap(txt, 1),
		msg: txt,
	}
}

func Note(cause error, msg string) *CausedError {
	return &CausedError{
		err:   goerrors.Wrap(msg, 1),
		msg:   msg,
		cause: cause,
	}
}

func Notef(cause error, f string, a ...interface{}) *CausedError {
	txt := fmt.Sprintf(f, a...)

	return &CausedError{
		err:   goerrors.Wrap(txt, 1),
		msg:   txt,
		cause: cause,
	}
}

func (e *Error) Message() string {
	return e.msg
}

func (e *CausedError) Message() string {
	return e.msg
}

func (e *CausedError) Cause() error {
	return e.cause
}
