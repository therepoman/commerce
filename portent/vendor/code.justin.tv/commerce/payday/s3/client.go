package s3

import (
	"bytes"
	"strings"

	"io/ioutil"
	"time"

	"code.justin.tv/commerce/payday/errors"
	util "code.justin.tv/commerce/payday/utils/close"
	"code.justin.tv/commerce/payday/utils/pointers"
	stringUtils "code.justin.tv/commerce/payday/utils/strings"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/client"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
)

const (
	defaultRegion = "us-west-2"
)
const defaultEndpoint = "s3-us-west-2.amazonaws.com"
const bucketAlreadyOwnedErrorPrefix = "BucketAlreadyOwnedByYou"

type S3Client struct {
	s3 *s3.S3
}

func NewDefaultConfig() *aws.Config {
	return &aws.Config{
		Region:           aws.String(defaultRegion),
		Endpoint:         aws.String(defaultEndpoint),
		S3ForcePathStyle: aws.Bool(true),
	}
}

func New(provider client.ConfigProvider, config *aws.Config) IS3Client {
	return &S3Client{
		s3: s3.New(provider, config),
	}
}

func NewFromConfig(s3Config *aws.Config) IS3Client {
	return New(session.New(), s3Config)
}

func NewFromDefaultConfig() IS3Client {
	return NewFromConfig(NewDefaultConfig())
}

func (c *S3Client) CreateBucket(bucketName string) error {
	_, err := c.s3.CreateBucket(&s3.CreateBucketInput{
		Bucket: aws.String(bucketName),
	})
	return err
}

func (c *S3Client) EnsureBucketExists(bucketName string) error {
	err := c.CreateBucket(bucketName)
	if err != nil && !strings.HasPrefix(err.Error(), bucketAlreadyOwnedErrorPrefix) {
		return err
	}
	return nil
}

func (c *S3Client) UploadFileToBucket(bucketName string, fileKey string, file []byte) error {
	_, err := c.s3.PutObject(&s3.PutObjectInput{
		Bucket: aws.String(bucketName),
		Key:    aws.String(fileKey),
		Body:   bytes.NewReader(file),
	})
	return err
}

func (c *S3Client) FileExists(bucket string, key string) (bool, error) {
	input := &s3.ListObjectsInput{
		Bucket: aws.String(bucket),
		Prefix: aws.String(key),
	}
	out, err := c.s3.ListObjects(input)
	if err != nil {
		return false, err
	}
	return len(out.Contents) > 0, nil
}

func (c *S3Client) BucketExists(targetBucket string) (bool, error) {
	out, err := c.s3.ListBuckets(&s3.ListBucketsInput{})
	if err != nil {
		return false, err
	}
	for _, bucket := range out.Buckets {
		if *bucket.Name == targetBucket {
			return true, nil
		}
	}
	return false, nil
}

func (c *S3Client) ListFileNames(bucket string, prefix string) ([]string, error) {
	input := &s3.ListObjectsInput{
		Bucket: aws.String(bucket),
		Prefix: aws.String(prefix),
	}
	out, err := c.s3.ListObjects(input)
	if err != nil {
		return nil, err
	}

	allFiles := make([]string, 0)
	for _, obj := range out.Contents {
		allFiles = append(allFiles, *obj.Key)
	}
	return allFiles, nil
}

func (c *S3Client) PaginatedListFileNames(bucket string, prefix string, continuationKey *string) ([]string, *string, error) {
	input := &s3.ListObjectsInput{
		Bucket: aws.String(bucket),
		Prefix: aws.String(prefix),
	}

	if stringUtils.NotBlankP(continuationKey) {
		input.Marker = aws.String(*continuationKey)
	}

	out, err := c.s3.ListObjects(input)
	if err != nil {
		return nil, nil, err
	}

	allFiles := make([]string, 0)
	for _, obj := range out.Contents {
		allFiles = append(allFiles, *obj.Key)
	}

	var nextContinuationKey *string
	if stringUtils.NotBlankP(out.NextMarker) {
		nextContinuationKey = pointers.StringP(*out.NextMarker)
	}
	return allFiles, nextContinuationKey, nil
}

func (c *S3Client) WaitForNFiles(bucket string, prefix string, numFiles int, timeout time.Duration) ([]string, error) {
	// Creates a goroutine that writes to a channel once we have the expected result (or an error)
	ch := make(chan error, 1)
	var result []string
	go func() {
		for {
			fileNames, err := c.ListFileNames(bucket, prefix)
			if err != nil {
				ch <- err
				return
			}
			if len(fileNames) >= numFiles {
				ch <- nil
				result = fileNames
				return
			}
		}
	}()

	// Selects the first channel to respond
	select {
	case err := <-ch:
		return result, err
	case <-time.After(timeout):
		return result, errors.Newf("Timed out waiting for %d files", numFiles)
	}
}

func (c *S3Client) LoadFile(bucket string, fileName string) ([]byte, error) {
	input := &s3.GetObjectInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(fileName),
	}
	out, err := c.s3.GetObject(input)
	if err != nil {
		return nil, err
	}

	defer util.Close(out.Body)

	bytes, err := ioutil.ReadAll(out.Body)
	if err != nil {
		return nil, err
	}

	return bytes, nil
}

func (c *S3Client) DeleteFile(bucket string, key string) error {
	input := &s3.DeleteObjectInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(key),
	}
	_, err := c.s3.DeleteObject(input)
	return err
}

func (c *S3Client) DeleteFiles(bucket string, keys ...string) error {
	objectIdentifiers := make([]*s3.ObjectIdentifier, len(keys))
	for i, key := range keys {
		objectIdentifiers[i] = &s3.ObjectIdentifier{
			Key: aws.String(key),
		}
	}

	input := &s3.DeleteObjectsInput{
		Bucket: aws.String(bucket),
		Delete: &s3.Delete{
			Objects: objectIdentifiers,
		},
	}
	_, err := c.s3.DeleteObjects(input)
	return err
}
