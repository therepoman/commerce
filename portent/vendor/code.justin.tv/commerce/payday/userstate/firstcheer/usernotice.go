package firstcheer

import (
	"strconv"
	"time"

	"code.justin.tv/commerce/payday/clients/tmi"
	"code.justin.tv/commerce/payday/metrics"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/models/api"
	"code.justin.tv/commerce/payday/userstate"
)

const (
	FirstCheerSystemMessageID = "firstcheer"
)

type UserNotice interface {
	SendMessage(request api.CreateEventRequest) (sent bool, err error)
}

type userNoticer struct {
	TmiClient       tmi.ITMIClient        `inject:""`
	UserStateGetter userstate.Getter      `inject:""`
	MetricLogger    metrics.IMetricLogger `inject:""`
	Bouncer         userstate.Bouncer     `inject:""`
}

func NewUserNotice() UserNotice {
	return &userNoticer{}
}

//Sends user notice to TMI only if this bits event is the first cheer of the user
func (un *userNoticer) SendMessage(req api.CreateEventRequest) (sent bool, err error) {
	if !un.Bouncer.IsUserNoticeEnabled(req.ChannelId) {
		return
	}

	userState, err := un.UserStateGetter.Get(req.UserId)
	if err != nil || userState == api.UserStateCheered {
		return
	}

	channelId, err := strconv.ParseInt(req.ChannelId, 10, 64)
	if err != nil {
		return
	}

	userId, err := strconv.ParseInt(req.UserId, 10, 64)
	if err != nil {
		return
	}

	tmiCallStartTime := time.Now()
	err = un.TmiClient.SendUserNotice(models.UserNoticeParams{
		SenderUserID:      userId,
		TargetChannelID:   channelId,
		Body:              req.Message,
		MsgId:             FirstCheerSystemMessageID,
		DefaultSystemBody: "bits first cheer notification",
	})
	go un.MetricLogger.LogDurationMetric("GiveBitsStep_CallTMIUserNotice", time.Since(tmiCallStartTime))

	sent = err == nil
	return
}
