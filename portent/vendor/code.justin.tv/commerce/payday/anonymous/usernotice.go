package anonymous

import (
	"strconv"

	"code.justin.tv/commerce/payday/clients/tmi"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/models/api"
)

const (
	AnonymousCheerSystemMessageID = "anoncheer"
)

type UserNotice interface {
	SendMessage(request api.CreateEventRequest) (err error)
}

type userNoticer struct {
	TmiClient tmi.ITMIClient `inject:""`
}

func NewUserNotice() UserNotice {
	return &userNoticer{}
}

func (un *userNoticer) SendMessage(req api.CreateEventRequest) error {
	channelId, err := strconv.ParseInt(req.ChannelId, 10, 64)
	if err != nil {
		return err
	}

	err = un.TmiClient.SendUserNotice(models.UserNoticeParams{
		SenderUserID:      tmi.OFFICIAL_TWITCH_USER_ID,
		TargetChannelID:   channelId,
		Body:              req.Message,
		MsgId:             AnonymousCheerSystemMessageID,
		MsgParams:         []models.UserNoticeMsgParam{},
		DefaultSystemBody: "Kappa", // This needs to be populated with something, even though we don't use it
	})

	return err
}
