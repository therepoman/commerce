package audit

import (
	"encoding/json"
	"fmt"
	"sort"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/s3"
)

type AuditClient struct {
	s3Client          s3.IS3Client
	dynamoTablePrefix string
}

type SComponent struct {
	S string `json:"S"`
}

type NComponent struct {
	N string `json:"N"`
}

type BoolComponent struct {
	Bool bool `json:"BOOL"`
}

type UserKeys struct {
	Id SComponent `json:"id"`
}

type ChannelKeys struct {
	Id SComponent `json:"id"`
}

type UserImage struct {
	Id          SComponent    `json:"id"`
	Banned      BoolComponent `json:"banned"`
	BannedBy    SComponent    `json:"bannedBy"`
	Annotation  SComponent    `json:"annotation"`
	LastUpdated NComponent    `json:"lastUpdated"`
}

type ChannelImage struct {
	Id          SComponent    `json:"id"`
	OptedOut    BoolComponent `json:"optedOut"`
	Annotation  SComponent    `json:"annotation"`
	LastUpdated NComponent    `json:"lastUpdated"`
}

type UserRecord struct {
	Keys     UserKeys   `json:"keys"`
	OldImage *UserImage `json:"OldImage"`
	NewImage *UserImage `json:"NewImage"`
}

type ChannelRecord struct {
	Keys     ChannelKeys   `json:"keys"`
	OldImage *ChannelImage `json:"OldImage"`
	NewImage *ChannelImage `json:"NewImage"`
}

type AuditedTableName string

const (
	auditRecordBucket   = "payday-dynamo-audit-records"
	UsersDynamoTable    = AuditedTableName("users")
	ChannelsDynamoTable = AuditedTableName("channels")
)

func NewAuditClient(s3Client s3.IS3Client, dynamoTablePrefix string) *AuditClient {
	return &AuditClient{
		s3Client:          s3Client,
		dynamoTablePrefix: dynamoTablePrefix,
	}
}

func (c *AuditClient) ListFileNames(table AuditedTableName, userId string) ([]string, error) {
	switch table {
	case UsersDynamoTable:
		return c.ListUserAuditRecordFileNames(userId)
	case ChannelsDynamoTable:
		return c.ListChannelAuditRecordFileNames(userId)
	default:
		msg := fmt.Sprintf("Unsupported audit table %s", table)
		log.Error(msg)
		return nil, errors.New(msg)
	}
}

func (c *AuditClient) ListUserAuditRecordFileNames(userId string) ([]string, error) {
	return c.s3Client.ListFileNames(auditRecordBucket, c.UserPath(userId))
}

func (c *AuditClient) WaitForNUserAuditRecordFileNames(userId string, numFiles int, timeout time.Duration) ([]string, error) {
	return c.s3Client.WaitForNFiles(auditRecordBucket, c.UserPath(userId), numFiles, timeout)
}

func (c *AuditClient) ListChannelAuditRecordFileNames(channelId string) ([]string, error) {
	return c.s3Client.ListFileNames(auditRecordBucket, c.ChannelPath(channelId))
}

func (c *AuditClient) WaitForNChannelAuditRecordFileNames(channelId string, numFiles int, timeout time.Duration) ([]string, error) {
	return c.s3Client.WaitForNFiles(auditRecordBucket, c.ChannelPath(channelId), numFiles, timeout)
}

func (c *AuditClient) UserPath(userId string) string {
	return c.dynamoTablePrefix + "_" + string(UsersDynamoTable) + "/" + userId
}

func (c *AuditClient) ChannelPath(channelId string) string {
	return c.dynamoTablePrefix + "_" + string(ChannelsDynamoTable) + "/" + channelId
}

func (c *AuditClient) LoadAuditRecordJson(table AuditedTableName, fileName string) ([]byte, error) {
	switch table {
	case UsersDynamoTable:
		return c.LoadUserAuditRecordJson(fileName)
	case ChannelsDynamoTable:
		return c.LoadChannelAuditRecordJson(fileName)
	default:
		msg := fmt.Sprintf("Unsupported audit table %s", table)
		log.Error(msg)
		return nil, errors.New(msg)
	}
}

func (c *AuditClient) LoadUserAuditRecordJson(fileName string) ([]byte, error) {
	return c.s3Client.LoadFile(auditRecordBucket, fileName)
}

func (c *AuditClient) LoadChannelAuditRecordJson(fileName string) ([]byte, error) {
	return c.s3Client.LoadFile(auditRecordBucket, fileName)
}

func (c *AuditClient) LoadUserAuditRecord(fileName string) (*UserRecord, error) {
	jsonResponse, err := c.LoadUserAuditRecordJson(fileName)
	if err != nil {
		return nil, err
	}

	var record UserRecord
	err = json.Unmarshal(jsonResponse, &record)
	if err != nil {
		return nil, err
	}
	return &record, nil
}

func (c *AuditClient) LoadChannelAuditRecord(fileName string) (*ChannelRecord, error) {
	jsonResponse, err := c.LoadChannelAuditRecordJson(fileName)
	if err != nil {
		return nil, err
	}

	var record ChannelRecord
	err = json.Unmarshal(jsonResponse, &record)
	if err != nil {
		return nil, err
	}

	return &record, nil
}

func SortFileNamesBySequenceNumber(paths []string) {
	sort.Strings(paths)
}
