package dynamo

import (
	"strconv"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/errors"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

const (
	RevenueRecordUserId    = "TargetTwitchUserId"
	RevenueRecordDate      = "TimeOfEvent"
	RevenueRecordBitCount  = "num_of_bits"
	RevenueRecordCentCount = "partner_payout_cents"
)

type Bits int64
type Cents int64

type RevenueTable struct {
}

type RevenueRecord struct {
	Channel   ChannelId
	Date      time.Time
	BitCount  Bits
	CentCount Cents
}

type RevenueDao struct {
	BaseDao
}

type IRevenueDao interface {
	GetWithinRange(id ChannelId, start time.Time, end time.Time) ([]*RevenueRecord, error)
}

func (u *RevenueTable) GetBaseTableName() BaseTableName {
	return "raincatcher-dashboard-hours"
}

func (u *RevenueTable) GetNodeJsRecordKeySelector() NodeJsRecordKeySelector {
	return "record.dynamodb.Keys.TargetTwitchUserId.S"
}

func (u *RevenueTable) ConvertScanOutputToRecords(so *dynamodb.ScanOutput) ([]DynamoScanQueryTableRecord, error) {
	records := make([]DynamoScanQueryTableRecord, *so.Count)

	for i, attributes := range so.Items {
		toAdd, err := u.convertAttributeMapToRecord(attributes)
		if err != nil {
			return nil, err
		}
		records[i] = toAdd
	}
	return records, nil
}

func (u *RevenueTable) ConvertQueryOutputToRecords(qo *dynamodb.QueryOutput) ([]DynamoScanQueryTableRecord, error) {
	records := make([]DynamoScanQueryTableRecord, *qo.Count)

	for i, attributes := range qo.Items {
		toAdd, err := u.convertAttributeMapToRecord(attributes)
		if err != nil {
			return nil, err
		}
		records[i] = toAdd
	}
	return records, nil
}

func (u *RevenueTable) convertAttributeMapToRecord(attributeMap map[string]*dynamodb.AttributeValue) (*RevenueRecord, error) {
	date, err := TimeFromAttributesInSeconds(attributeMap, RevenueRecordDate)
	if err != nil {
		return nil, err
	}

	bits, err := Int64FromAttributes(attributeMap, RevenueRecordBitCount)
	if err != nil {
		return nil, err
	}

	cents, err := Int64FromAttributes(attributeMap, RevenueRecordCentCount)
	if err != nil {
		return nil, err
	}

	return &RevenueRecord{
		Channel:   ChannelId(StringFromAttributes(attributeMap, RevenueRecordUserId)),
		Date:      date,
		BitCount:  Bits(bits),
		CentCount: Cents(cents),
	}, nil
}

func NewRevenueDao(client *DynamoClient) IRevenueDao {
	dao := &RevenueDao{}
	dao.client = client
	dao.table = &UsersTable{}
	return dao
}

func (u *RevenueRecord) GetScanQueryTable() DynamoScanQueryTable {
	return &RevenueTable{}
}

func (dao *RevenueDao) GetWithinRange(id ChannelId, start time.Time, end time.Time) ([]*RevenueRecord, error) {
	filter := getQueryFilter(id, start, end)

	result, _, err := dao.client.QueryTable(&RevenueTable{}, filter)
	if err != nil {
		return nil, err
	}

	// Need to type assert the slice of records
	revenueResult := make([]*RevenueRecord, len(result))
	for i, record := range result {
		var isRevenue bool
		revenueResult[i], isRevenue = record.(*RevenueRecord)
		if !isRevenue {
			msg := "Encountered an unexpected type while converting dynamo result to revenue record"
			log.Error(msg)
			return revenueResult, errors.New(msg)
		}
	}

	return revenueResult, nil
}

func getQueryFilter(id ChannelId, start time.Time, end time.Time) QueryFilter {
	filter := QueryFilter{
		Names: map[string]*string{
			"#ID":   aws.String("TargetTwitchUserId"),
			"#TIME": aws.String("TimeOfEvent"),
		},
		Values: map[string]*dynamodb.AttributeValue{
			":id":    &dynamodb.AttributeValue{S: aws.String(string(id))},
			":start": &dynamodb.AttributeValue{N: aws.String(strconv.FormatInt(start.Unix(), 10))},
			":end":   &dynamodb.AttributeValue{N: aws.String(strconv.FormatInt(end.Unix(), 10))},
		},
		KeyConditionExpression: aws.String("#ID = :id AND #TIME BETWEEN :start AND :end"),
	}
	return filter
}
