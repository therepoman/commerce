package dynamo

import (
	"time"

	"github.com/aws/aws-sdk-go/service/dynamodb"
)

type BaseTableName string
type FullTableName string
type NodeJsRecordKeySelector string
type ReadCapacity int64
type WriteCapacity int64

type BaseDynamoTable interface {
	GetBaseTableName() BaseTableName
	GetNodeJsRecordKeySelector() NodeJsRecordKeySelector
}

type DynamoTable interface {
	BaseDynamoTable
	NewCreateTableInput(name FullTableName, read ReadCapacity, write WriteCapacity) *dynamodb.CreateTableInput
	ConvertAttributeMapToRecord(attributeMap map[string]*dynamodb.AttributeValue) (DynamoTableRecord, error)
}

type DynamoTableRecord interface {
	GetTable() DynamoTable
	NewAttributeMap() map[string]*dynamodb.AttributeValue
	NewItemKey() map[string]*dynamodb.AttributeValue
	GetTimestamp() time.Time
	UpdateWithCurrentTimestamp()
	ApplyTimestamp(timestamp time.Time)
	Equals(other DynamoTableRecord) bool
	EqualsWithExpectedLastUpdated(other DynamoTableRecord, expectedLastUpdatedTime time.Time, acceptableTimeDelta time.Duration) bool
	NewHashKeyEqualsExpression() string
	NewHashKeyExpressionAttributeValues() map[string]*dynamodb.AttributeValue
}

type DynamoScanQueryTable interface {
	BaseDynamoTable
	ConvertScanOutputToRecords(so *dynamodb.ScanOutput) ([]DynamoScanQueryTableRecord, error)
	ConvertQueryOutputToRecords(qo *dynamodb.QueryOutput) ([]DynamoScanQueryTableRecord, error)
}

type DynamoScanQueryTableRecord interface {
	GetScanQueryTable() DynamoScanQueryTable
}
