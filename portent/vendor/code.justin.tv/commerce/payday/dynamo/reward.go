package dynamo

import (
	"time"

	"code.justin.tv/commerce/payday/errors"
	cmd "code.justin.tv/commerce/payday/hystrix"
	timeUtils "code.justin.tv/commerce/payday/utils/time"
	"github.com/afex/hystrix-go/hystrix"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

const (
	maxRewardsPerUser = 200
)

type RewardsTable struct {
}

type Reward struct {
	// Dynamo HASH
	// User receiving reward
	UserID string

	// Dynamo RANGE
	// Unique identifier for reward
	RewardID string

	// Event / group the reward is tied to
	// EX: HGC2017
	Domain string

	// Type of milestone reached resulting in reward
	// EX: global
	MilestoneType string

	// Threshold of milestone reached resulting in reward
	// EX: 10000
	MilestoneThreshold int64

	// Type of reward being fulfilled
	// EX: [emote / badge / blizzard]
	RewardType string

	// Current status of the reward in the fulfillment process
	// EX: [pending / fulfilled]
	FulfillmentStatus string

	// ID of the fulfilment request in the fulfillment platform
	// EX: 123
	FulfillmentID string

	// Identifier of the reward on third party platform / Twitch
	// EX: BZ00123 / Emote_Kappa
	FulfillmentSKU string

	// Identifier of the account receiving the reward on third party platform
	// EX: myBattleNetAccount
	FulfillmentAccount string

	LastUpdated time.Time
}

type RewardDAO struct {
	BaseDao
}

type IRewardDAO interface {
	Update(reward *Reward) error
	Get(userID string, rewardID string) (*Reward, error)
	Delete(userID string, rewardID string) error
	DeleteTable() error
	SetupTable() error
	GetClientConfig() *DynamoClientConfig
	GetLatestStreamArn() (string, error)
	GetAllByUserID(userID string) ([]*Reward, error)
	UpdateIfNotExists(reward *Reward) error
	GetAllByFulfillmentAccountID(userID string) ([]*Reward, error)
}

func (r *RewardsTable) GetBaseTableName() BaseTableName {
	return "rewards"
}

func (r *RewardsTable) GetNodeJsRecordKeySelector() NodeJsRecordKeySelector {
	return "record.dynamodb.Keys.userId.S + '_' + record.dynamodb.Keys.rewardId.S"
}

func (r *RewardsTable) NewCreateTableInput(name FullTableName, read ReadCapacity, write WriteCapacity) *dynamodb.CreateTableInput {
	return &dynamodb.CreateTableInput{
		TableName: aws.String(string(name)),
		ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
			ReadCapacityUnits:  aws.Int64(int64(read)),
			WriteCapacityUnits: aws.Int64(int64(write)),
		},
		AttributeDefinitions: []*dynamodb.AttributeDefinition{
			{
				AttributeName: aws.String("userId"),
				AttributeType: aws.String("S"),
			},
			{
				AttributeName: aws.String("rewardId"),
				AttributeType: aws.String("S"),
			},
			{
				AttributeName: aws.String("fulfillmentAccount"),
				AttributeType: aws.String("S"),
			},
		},
		KeySchema: []*dynamodb.KeySchemaElement{
			{
				AttributeName: aws.String("userId"),
				KeyType:       aws.String("HASH"),
			},
			{
				AttributeName: aws.String("rewardId"),
				KeyType:       aws.String("RANGE"),
			},
		},
		StreamSpecification: &dynamodb.StreamSpecification{
			StreamEnabled:  aws.Bool(true),
			StreamViewType: aws.String(dynamodb.StreamViewTypeNewAndOldImages),
		},
		GlobalSecondaryIndexes: []*dynamodb.GlobalSecondaryIndex{
			{
				IndexName: aws.String("fulfillment-account-index"),
				KeySchema: []*dynamodb.KeySchemaElement{
					{
						AttributeName: aws.String("fulfillmentAccount"),
						KeyType:       aws.String("HASH"),
					},
					{
						AttributeName: aws.String("rewardId"),
						KeyType:       aws.String("RANGE"),
					},
				},
				Projection: &dynamodb.Projection{
					ProjectionType: aws.String("ALL"),
				},
				ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
					ReadCapacityUnits:  aws.Int64(int64(read)),
					WriteCapacityUnits: aws.Int64(int64(write)),
				},
			},
		},
	}
}

func (r *RewardsTable) ConvertAttributeMapToRecord(attributeMap map[string]*dynamodb.AttributeValue) (DynamoTableRecord, error) {
	milestoneThreshold, err := Int64FromAttributes(attributeMap, "milestoneThreshold")
	if err != nil {
		return nil, err
	}

	lastUpdated, err := TimeFromAttributes(attributeMap, "lastUpdated")
	if err != nil {
		return nil, err
	}

	return &Reward{
		UserID:             StringFromAttributes(attributeMap, "userId"),
		RewardID:           StringFromAttributes(attributeMap, "rewardId"),
		Domain:             StringFromAttributes(attributeMap, "domain"),
		MilestoneType:      StringFromAttributes(attributeMap, "milestoneType"),
		MilestoneThreshold: milestoneThreshold,
		RewardType:         StringFromAttributes(attributeMap, "rewardType"),
		FulfillmentStatus:  StringFromAttributes(attributeMap, "fulfillmentStatus"),
		FulfillmentID:      StringFromAttributes(attributeMap, "fulfillmentId"),
		FulfillmentSKU:     StringFromAttributes(attributeMap, "fulfillmentSku"),
		FulfillmentAccount: StringFromAttributes(attributeMap, "fulfillmentAccount"),
		LastUpdated:        lastUpdated,
	}, nil
}

func (r *Reward) GetTable() DynamoTable {
	return &RewardsTable{}
}

func (r *Reward) NewAttributeMap() map[string]*dynamodb.AttributeValue {
	itemMap := make(map[string]*dynamodb.AttributeValue)

	PopulateAttributesString(itemMap, "userId", r.UserID)
	PopulateAttributesString(itemMap, "rewardId", r.RewardID)
	PopulateAttributesString(itemMap, "domain", r.Domain)
	PopulateAttributesString(itemMap, "milestoneType", r.MilestoneType)
	PopulateAttributesInt64(itemMap, "milestoneThreshold", r.MilestoneThreshold)
	PopulateAttributesString(itemMap, "rewardType", r.RewardType)
	PopulateAttributesString(itemMap, "fulfillmentStatus", r.FulfillmentStatus)
	PopulateAttributesString(itemMap, "fulfillmentId", r.FulfillmentID)
	PopulateAttributesString(itemMap, "fulfillmentSku", r.FulfillmentSKU)
	PopulateAttributesString(itemMap, "fulfillmentAccount", r.FulfillmentAccount)
	PopulateAttributesTime(itemMap, "lastUpdated", r.LastUpdated)

	return itemMap
}

func (r *Reward) NewItemKey() map[string]*dynamodb.AttributeValue {
	keyMap := make(map[string]*dynamodb.AttributeValue)
	keyMap["userId"] = &dynamodb.AttributeValue{S: aws.String(r.UserID)}
	keyMap["rewardId"] = &dynamodb.AttributeValue{S: aws.String(r.RewardID)}
	return keyMap
}

func (r *Reward) NewHashKeyEqualsExpression() string {
	return "userId = :userId"
}

func (r *Reward) NewHashKeyExpressionAttributeValues() map[string]*dynamodb.AttributeValue {
	attributeMap := make(map[string]*dynamodb.AttributeValue)
	PopulateAttributesString(attributeMap, ":userId", r.UserID)
	return attributeMap
}

func (r *Reward) UpdateWithCurrentTimestamp() {
	r.LastUpdated = time.Now()
}

func (r *Reward) GetTimestamp() time.Time {
	return r.LastUpdated
}

func (r *Reward) ApplyTimestamp(timestamp time.Time) {
	r.LastUpdated = timestamp
}

func (r *Reward) Equals(other DynamoTableRecord) bool {
	otherReward, isReward := other.(*Reward)
	if !isReward {
		return false
	}

	return r.UserID == otherReward.UserID &&
		r.RewardID == otherReward.RewardID &&
		r.Domain == otherReward.Domain &&
		r.MilestoneType == otherReward.MilestoneType &&
		r.MilestoneThreshold == otherReward.MilestoneThreshold &&
		r.RewardType == otherReward.RewardType &&
		r.FulfillmentStatus == otherReward.FulfillmentStatus &&
		r.FulfillmentID == otherReward.FulfillmentID &&
		r.FulfillmentSKU == otherReward.FulfillmentSKU &&
		r.FulfillmentAccount == otherReward.FulfillmentAccount
}

func (r *Reward) EqualsWithExpectedLastUpdated(other DynamoTableRecord, lastUpdatedTime time.Time, lastUpdatedDelta time.Duration) bool {
	return r.Equals(other) && timeUtils.IsTimeInForwardDelta(other.GetTimestamp(), lastUpdatedTime, lastUpdatedDelta)
}

func NewRewardDao(client *DynamoClient) IRewardDAO {
	dao := &RewardDAO{BaseDao{
		client: client,
		table:  &RewardsTable{},
	}}
	return dao
}

func (dao *RewardDAO) Update(reward *Reward) error {
	fn := func() error {
		return dao.client.UpdateItem(reward)
	}
	return hystrix.Do(cmd.UpdateRewardCommand, fn, nil)
}

func (dao *RewardDAO) UpdateIfNotExists(reward *Reward) error {
	conditionalUpdate := &ConditionalExpression{
		Expected: map[string]*dynamodb.ExpectedAttributeValue{
			"userId": {
				Exists: aws.Bool(false),
			},
			"rewardId": {
				Exists: aws.Bool(false),
			},
		},
	}

	var conditionalfailure awserr.Error
	fn := func() error {
		err := dao.client.UpdateItemConditional(reward, conditionalUpdate)
		if err != nil {
			if ae, ok := err.(awserr.Error); ok && ae.Code() == "ConditionalCheckFailedException" {
				conditionalfailure = ae
				return nil
			}
		}
		return err
	}
	err := hystrix.Do(cmd.UpdateRewardCommand, fn, nil)
	if conditionalfailure != nil {
		return conditionalfailure
	}

	return err
}

func (dao *RewardDAO) Get(userID string, rewardID string) (*Reward, error) {
	rewardChan := make(chan *Reward, 1)
	fn := func() error {
		reward, err := dao.get(userID, rewardID)
		if err != nil {
			return err
		}
		rewardChan <- reward
		return nil
	}

	err := hystrix.Do(cmd.GetRewardCommand, fn, nil)
	if err != nil {
		return nil, err
	}

	reward := <-rewardChan
	return reward, nil
}

func (dao *RewardDAO) get(userID string, rewardID string) (*Reward, error) {
	var rewardResult *Reward

	rewardQuery := &Reward{
		UserID:   userID,
		RewardID: rewardID,
	}

	result, err := dao.client.GetItemWithConsistentRead(rewardQuery)
	if err != nil {
		return rewardResult, err
	}

	// Result will be nil if no item is found in dynamo
	if result == nil {
		return nil, nil
	}

	rewardResult, isReward := result.(*Reward)
	if !isReward {
		return rewardResult, errors.New("Encountered an unexpected type while converting dynamo result to reward")
	}

	return rewardResult, nil
}

func (dao *RewardDAO) Delete(userID string, rewardID string) error {
	fn := func() error {
		deletionRecord := &Reward{
			UserID:   userID,
			RewardID: rewardID,
		}
		return dao.client.DeleteItem(deletionRecord)
	}
	return hystrix.Do(cmd.UpdateRewardCommand, fn, nil)
}

func (dao *RewardDAO) GetAllByUserID(userID string) ([]*Reward, error) {
	rewardsChan := make(chan []*Reward, 1)
	fn := func() error {
		rewards, err := dao.getAllByUserID(userID)
		if err != nil {
			return err
		}
		rewardsChan <- rewards
		return nil
	}

	err := hystrix.Do(cmd.GetRewardCommand, fn, nil)
	if err != nil {
		return nil, err
	}

	rewards := <-rewardsChan
	return rewards, nil
}

func (dao *RewardDAO) getAllByUserID(userID string) ([]*Reward, error) {
	rewardsQuery := &Reward{
		UserID: userID,
	}

	results, err := dao.client.QueryByHashKeyWithLimitAndConsistentRead(rewardsQuery, maxRewardsPerUser, true)
	if err != nil {
		return nil, nil
	}

	rewardResults := make([]*Reward, len(results))
	for i, result := range results {
		rewardResult, isReward := result.(*Reward)
		if !isReward {
			return nil, errors.New("Encountered an unexpected type while converting dynamo result to reward")
		}
		rewardResults[i] = rewardResult
	}

	return rewardResults, nil
}

func (dao *RewardDAO) GetAllByFulfillmentAccountID(fulfillmentAccountId string) ([]*Reward, error) {
	rewardsChan := make(chan []*Reward, 1)
	fn := func() error {
		rewards, err := dao.getAllByFulfillmentAccountID(fulfillmentAccountId)
		if err != nil {
			return err
		}
		rewardsChan <- rewards
		return nil
	}

	err := hystrix.Do(cmd.GetRewardByFulfillmentAccountCommand, fn, nil)
	if err != nil {
		return nil, err
	}

	rewards := <-rewardsChan
	return rewards, nil
}

func (dao *RewardDAO) getAllByFulfillmentAccountID(fulfillmentAccountId string) ([]*Reward, error) {

	table := &RewardsTable{}
	queryFilter := QueryFilter{
		Names: map[string]*string{
			"#ID": aws.String("fulfillmentAccount"),
		},
		Values: map[string]*dynamodb.AttributeValue{
			":id": &dynamodb.AttributeValue{S: aws.String(string(fulfillmentAccountId))},
		},
		KeyConditionExpression: aws.String("#ID = :id"),
		Descending:             false,
		IndexName:              aws.String("fulfillment-account-index"),
	}

	result, _, err := dao.client.QueryTable(table, queryFilter)
	if err != nil {
		return nil, err
	}

	rewardResults := make([]*Reward, len(result))
	for i, result := range result {
		rewardResult, isReward := result.(*Reward)
		if !isReward {
			return nil, errors.New("Encountered an unexpected type while converting dynamo result to reward")
		}
		rewardResults[i] = rewardResult
	}

	return rewardResults, nil
}

func (u *RewardsTable) ConvertScanOutputToRecords(so *dynamodb.ScanOutput) ([]DynamoScanQueryTableRecord, error) {
	records := make([]DynamoScanQueryTableRecord, *so.Count)

	for i, attributes := range so.Items {
		recordToAdd, err := u.ConvertAttributeMapToRecord(attributes)
		if err != nil {
			return nil, err
		}

		scanQueryRecord, err := recordToScanQueryRecord(recordToAdd)
		if err != nil {
			return nil, err
		}

		records[i] = scanQueryRecord
	}
	return records, nil
}

func (u *RewardsTable) ConvertQueryOutputToRecords(qo *dynamodb.QueryOutput) ([]DynamoScanQueryTableRecord, error) {
	records := make([]DynamoScanQueryTableRecord, *qo.Count)

	for i, attributes := range qo.Items {
		recordToAdd, err := u.ConvertAttributeMapToRecord(attributes)
		if err != nil {
			return nil, err
		}

		scanQueryRecord, err := recordToScanQueryRecord(recordToAdd)
		if err != nil {
			return nil, err
		}

		records[i] = scanQueryRecord
	}
	return records, nil
}

func (Reward) GetScanQueryTable() DynamoScanQueryTable {
	return &RewardsTable{}
}
