package analyzer

import (
	"bufio"
	"errors"
	"io"
	"os"
	"regexp"
	"sort"
	"strings"

	log "code.justin.tv/commerce/logrus"
)

var dataSourceFilePaths = []string{
	"donation_url_regex",
	"donation_urls/donation_url_regex",
	"data/donation_urls/donation_url_regex",
	"../data/donation_urls/donation_url_regex",
	"../../data/donation_urls/donation_url_regex",
}

var domainDataSourceFilePaths = []string{
	"domain_url_regex",
	"donation_urls/domain_url_regex",
	"data/donation_urls/domain_url_regex",
	"../data/donation_urls/domain_url_regex",
	"../../data/donation_urls/domain_url_regex",
}

var r *regexp.Regexp
var domainR *regexp.Regexp

func init() {
	filePath, err := searchFileInPaths(dataSourceFilePaths)
	if err != nil {
		log.WithError(err).Panic("Failed to locate data source file")
	}

	file, err := os.Open(filePath)
	if err != nil {
		log.WithError(err).Panic("Failed to open Data file")
	}
	defer file.Close()

	lines := make([]string, 0)
	reader := bufio.NewReader(file)
	for {
		regex, _, err := reader.ReadLine()
		if err != nil {
			if err == io.EOF {
				break
			}
			log.WithError(err).Panic("Failed to read a line from Data file")
		}
		if len(regex) == 0 {
			continue
		}

		lines = append(lines, string(regex))
	}

	r = regexp.MustCompile(strings.Join(lines, "|"))

	// Create domain regex
	filePath, err = searchFileInPaths(domainDataSourceFilePaths)
	if err != nil {
		log.WithError(err).Panic("Failed to locate domain data source file")
	}

	file, err = os.Open(filePath)
	if err != nil {
		log.WithError(err).Panic("Failed to open Domain Data file")
	}
	defer file.Close()

	lines = make([]string, 0)
	reader = bufio.NewReader(file)
	for {
		regex, _, err := reader.ReadLine()
		if err != nil {
			if err == io.EOF {
				break
			}
			log.WithError(err).Panic("Failed to read a line from Domain Data file")
		}
		if len(regex) == 0 {
			continue
		}

		lines = append(lines, string(regex))
	}

	domainR = regexp.MustCompile(strings.Join(lines, "|"))
}

func MentionsDonation(target string) bool {
	return r.MatchString(target)
}

func FindDonationLinks(target string) []string {
	return r.FindAllString(target, -1)
}

func FindUniqueDomains(target string) []string {
	UniqueDomains := []string{}
	domainMap := make(map[string]bool)
	domains := domainR.FindAllString(target, -1)
	for _, domain := range domains {
		if _, value := domainMap[domain]; !value {
			domainMap[domain] = true
			UniqueDomains = append(UniqueDomains, domain)
		}
	}
	sort.Strings(UniqueDomains)
	return UniqueDomains
}

func HasLink(target string) []string {
	l := regexp.MustCompile(`(http|https):\/\/([\w\-_]+(?:(?:\.[\w\-_]+)+))([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?`)
	return l.FindAllString(target, -1)
}

func searchFileInPaths(filePaths []string) (string, error) {
	for _, filePath := range filePaths {
		_, err := os.Stat(filePath)
		if err != nil && !os.IsNotExist(err) {
			return "", err
		} else if err == nil {
			return filePath, nil
		}
	}
	return "", errors.New("file %s not found in any searched path")
}
