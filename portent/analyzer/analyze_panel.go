package analyzer

import (
	"strings"

	"code.justin.tv/commerce/portent/models"
	panels "code.justin.tv/gds/gds/panels/pms/documents"
)

type match struct {
	Field   models.Field
	Context string
	Urls    []string
	Domains []string
}

type analysis struct {
	Score  int
	Fields []match
}

func Analyze(panel *panels.PanelDescription) analysis {
	// `score` dictates how many donation links an user has on the panels
	score := 0
	fields := make([]match, 0)

	// Panel Title
	titles := FindDonationLinks(panel.Data.Title)
	if titles != nil {
		fields = append(fields, match{
			Field:   models.TITLE,
			Context: panel.Data.Title,
			Urls:    titles,
			Domains: FindUniqueDomains(strings.Join(titles, "")),
		})
		score += len(titles)
	}
	// Panel Link
	links := MentionsDonation(panel.Data.Link)
	if links {
		fields = append(fields, match{
			Field:   models.LINK,
			Context: panel.Data.Link,
			Urls:    []string{panel.Data.Link},
			Domains: FindUniqueDomains(panel.Data.Link),
		})
		score++
	}
	// Panel Description
	descriptions := FindDonationLinks(panel.Data.Description)
	if descriptions != nil {
		fields = append(fields, match{
			Field:   models.DESCRIPTION,
			Context: panel.Data.Description,
			Urls:    descriptions,
			Domains: FindUniqueDomains(strings.Join(descriptions, "")),
		})
		score += len(descriptions)
	}
	return analysis{Score: score, Fields: fields}
}

func AnalyzeLinks(panel *panels.PanelDescription) []string {
	Urls := make([]string, 0)

	// Panel Title
	titles := HasLink(panel.Data.Title)
	if titles != nil {
		Urls = append(Urls, titles...)
	}
	// Panel Link
	links := HasLink(panel.Data.Link)
	if links != nil {
		Urls = append(Urls, links...)
	}
	// Panel Description
	descriptions := HasLink(panel.Data.Description)
	if descriptions != nil {
		Urls = append(Urls, descriptions...)
	}
	return Urls
}
