# Portent
:cop: Portent - Service for monitoring and tracking donation links on panels.

## Checkout development packages
```sh
go get code.justin.tv/commerce/portent
```

## Dep
Go Dep is the definitive go dependency management tool.
 To pull new dependencies into the vendor directory, make use of the dependency in the code and then run:
```sh
make dep
```

To add a new tool, edit the `tool.json` file to add the new tool and the version. Then, run:
```sh
retool sync
```
This will sync the `/tools` folder to match all the tools and versions specified in the `tools.json` file

![alt text](https://cdn-image.pf.dena.com/b0bf411b1e59ff824059847b9384dcc0134c5eba/1/89ce614f-6c3c-3821-8bc7-7d5fedb0d03a.png "Portent of Doom")
