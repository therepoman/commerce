module "portent" {
  source             = "../modules/portent"
  account_name       = "twitch-portent-aws-prod"
  owner              = "twitch-portent-aws-prod@amazon.com"
  aws_profile        = "portent-prod"
  env                = "prod"
  account_number     = "272653928523"
  pms-aws-account-id = "523543649671"
}

terraform {
  backend "s3" {
    bucket  = "portent-prod-tcs"
    key     = "tfstate/commerce/portent/terraform/production"
    region  = "us-west-2"
    profile = "portent-prod"
  }
}
