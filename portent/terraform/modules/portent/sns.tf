resource "aws_sns_topic" "event-topic" {
  name = "events-${var.env}"
}

resource "aws_sns_topic_policy" "default" {
  arn    = "${aws_sns_topic.event-topic.arn}"
  policy = "${data.aws_iam_policy_document.sns-topic-policy.json}"
}

data "aws_iam_policy_document" "sns-topic-policy" {
  policy_id = "__default_policy_ID"

  statement {
    actions = ["SNS:Publish"]
    effect = "Allow"
    principals {
      identifiers = [
        "arn:aws:iam::021561903526:root",
        "arn:aws:iam::${var.pms-aws-account-id}:root"
      ]
      type = "AWS"
    }
    resources = [
      "${aws_sns_topic.event-topic.arn}",
    ]
    sid = "__default_statement_ID"
  }
}