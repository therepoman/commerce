resource "aws_iam_role" "LambdaExecutionRole" {
  name = "LambdaExecutionRole"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "lambda_role_policy_attachment" {
  role = "${aws_iam_role.LambdaExecutionRole.name}"
  policy_arn = "arn:aws:iam::aws:policy/AdministratorAccess"
}

resource "aws_lambda_event_source_mapping" "lambda_source_mapping" {
  event_source_arn = "${aws_sqs_queue.bits_enabled_tuids_queue.arn}"
  function_name    = "arn:aws:lambda:us-west-2:${var.account_number}:function:${var.lambda-name}"
  enabled          = true
  batch_size       = 10
}
