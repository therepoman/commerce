resource "aws_sqs_queue" "bits_enabled_tuids_queue_dlq" {
  name = "bits-enabled-tuids-queue-dlq-${var.env}"
}

resource "aws_sqs_queue" "bits_enabled_tuids_queue" {
  name           = "bits-enabled-tuids-queue-${var.env}"
  redrive_policy = "{\"deadLetterTargetArn\":\"${aws_sqs_queue.bits_enabled_tuids_queue_dlq.arn}\",\"maxReceiveCount\":5}"
}

resource "aws_sns_topic_subscription" "bits_enabled_tuids_sqs_target" {
  topic_arn = "${aws_sns_topic.event-topic.arn}"
  protocol  = "sqs"
  endpoint  = "${aws_sqs_queue.bits_enabled_tuids_queue.arn}"
}

resource "aws_sqs_queue_policy" "bits_enabled_tuids_queue_policy" {
  queue_url = "${aws_sqs_queue.bits_enabled_tuids_queue.id}"

  policy = <<EOF
  {
    "Version": "2012-10-17",
    "Id": "bits_enabled_tuids_queue_policy",
    "Statement":[
      {
        "Sid": "AllowTopicsFromOtherTwitchAWSAccountsToSendMessages",
        "Effect": "Allow",
        "Principal": "*",
        "Action": "SQS:SendMessage",
        "Resource": "${aws_sqs_queue.bits_enabled_tuids_queue.arn}",
        "Condition": {
          "ArnEquals": {
            "aws:SourceArn": "${aws_sns_topic.event-topic.arn}"
          }
        }
      }
    ]
  }
  EOF
}
