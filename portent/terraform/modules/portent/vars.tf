# AWS general Config
variable "aws_profile" {
  description = "The AWS Named Profile to use when running terraform and creating AWS resources"
}
# Add this in after CIDR block is setup
# variable "private_cidr_blocks" {
#   description = "The private subnet cidr blocks for this stage"
#   type = "list"
# }

variable "aws_region" {
  description = "AWS region to operate in."
  default     = "us-west-2"
}

variable "service" { 
  description = "Name of the service being built. Should probably leave this as the default"
  default     = "commerce/portent"
}

variable "service_name" {
  description = "Name of the service being built, without the 'commerce/' part."
  default     = "portent"
}

variable "env" {
  description = "Whether it's production, dev, etc"
}

variable "account_name" {
  description = "Name of the AWS account"
}

variable "account_number" {
  description = "AWS account number"
}

variable "owner" {
  description = "Owner of the account"
}

# Add these in after VPC is setup
# # VPC Config
# variable "vpc_id" {
#   description = "Id of the systems-created VPC"
# }

# variable "private_subnets" {
#   description = "Comma-separated list of private subnets"
# }

# variable "sg_id" {
#   description = "Id of the security group"
# }

# SNS Config
variable "payday-aws-account-id" {
  description = "The aws account used by payday"
  default     = "021561903526"
}

variable "pms-aws-account-id" {
  description = "The aws account used by GDS/PMS"
}

variable "lambda-name" {
  description = "The name of the Lambda function"
  default     = "PanelHandler"
}