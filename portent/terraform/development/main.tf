module "portent" {
  source             = "../modules/portent"
  account_name       = "twitch-portent-aws-devo"
  owner              = "twitch-portent-aws-devo@amazon.com"
  aws_profile        = "portent-devo"
  env                = "dev"
  account_number     = "898907546878"
  pms-aws-account-id = "510557735000"
  # vpc_id              = "vpc-11635a76"
  # private_subnets     = "subnet-ca2c4083,subnet-c4f6bca3,subnet-aec438f5"
  # private_cidr_blocks = ["10.201.180.0/24", "10.201.181.0/24", "10.201.182.0/24"]
  # sg_id               = "sg-2060fc5b"
}

terraform {
  backend "s3" {
    bucket  = "portent-devo-tcs"
    key     = "tfstate/commerce/portent/terraform/development"
    region  = "us-west-2"
    profile = "portent-devo"
  }
}
