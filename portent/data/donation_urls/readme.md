## Regex collection of donation URLs
The URL regex's in `donation_url_regex` are the authoritative source of truth as to what is considered a donation link
on channel panels by the Lambda. When a new URL is to be added, just add a new regex entry that matches the desired pattern.

### Notes
- Streamlabs:
  - `https://streamlabs.com/slobs/d/{userid}` is not a donation link
  - `https://streamlabs.com/{username}/#/merch` is not a donation link

- Possible candidates:
  - `https://www.tipeeestream.com/{username}/donation`
  - `https://www.twitchalerts.com/donate/{username}`
  - `http://www.donationalerts.ru/r/{username}`