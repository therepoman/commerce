package models

type SourceType string
type Field string

const (
	SPADE_DONATION_LINK_EVENT_TYPE = "channel_donation_scan_info"
)

const (
	PANEL SourceType = "PANEL"
)

const (
	TITLE       Field = "TITLE"
	LINK        Field = "LINK"
	DESCRIPTION Field = "DESCRIPTION"
)

type DonationEvent struct {
	ChannelID      string     `json:"channel_id"`
	DonationFlag   bool       `json:"donation_flag"`
	SourceType     SourceType `json:"source_type,omitempty"`
	SourceID       int        `json:"source_id,omitempty"`
	Field          Field      `json:"field,omitempty"`
	ContextDetails string     `json:"context_details,omitempty"`
	Domains        string     `json:"domains,omitempty"`
}

func (e Field) String() string {
	switch e {
	case TITLE:
		return "TITLE"
	case LINK:
		return "LINK"
	case DESCRIPTION:
		return "DESCRIPTION"
	default:
		return ""
	}
}

func (e SourceType) String() string {
	switch e {
	case PANEL:
		return "PANEL"
	default:
		return ""
	}
}
