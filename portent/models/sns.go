package models

import "time"

type Message struct {
	Type              string                      `json:"Type"`
	MessageId         string                      `json:"MessageId"`
	Token             string                      `json:"Token,optional"`
	TopicArn          string                      `json:"TopicArn"`
	Subject           string                      `json:"Subject,optional"`
	Message           string                      `json:"Message"`
	SubscribeURL      string                      `json:"SubscribeURL,optional"`
	Timestamp         time.Time                   `json:"Timestamp"`
	SignatureVersion  string                      `json:"SignatureVersion"`
	Signature         string                      `json:"Signature"`
	SigningCertURL    string                      `json:"SigningCertURL"`
	UnsubscribeURL    string                      `json:"UnsubscribeURL,optional"`
	MessageAttributes map[string]MessageAttribute `json:"MessageAttributes,optional"`
}

type MessageAttribute struct {
	Type  string `json:"Type"`
	Value string `json:"Value"`
}

type PanelUpdateSNSMessage struct {
	ChannelID string `json:"channel_id"`
}
