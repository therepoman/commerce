package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net/url"
	"os"
	"strings"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/portent/analyzer"
	"code.justin.tv/commerce/portent/models"
	"code.justin.tv/common/spade-client-go/spade"
	"code.justin.tv/foundation/twitchclient"
	gds "code.justin.tv/gds/gds/panels/pms/client"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

var pmsClient gds.Client
var spadeClient spade.Client
var env string

const (
	ENVIRONMENT_VARIABLE_ENV = "ENV"
	PMS_HOST_STAGING         = "https://panels-manager-staging.twitch.tv"
	PMS_HOST_PROD            = "https://panels-manager.prod.us-west2.justin.tv"
)

func init() {
	pmsHost := PMS_HOST_STAGING
	env = os.Getenv(ENVIRONMENT_VARIABLE_ENV)
	if env == "prod" {
		pmsHost = PMS_HOST_PROD
	}

	config := twitchclient.ClientConf{
		Host: pmsHost,
	}
	var err error
	pmsClient, err = gds.NewClient(config)
	if err != nil {
		log.WithError(err).Panic("Error initializing PMS client")
	}

	spadeClient, err = spade.NewClient(spade.InitBaseURL(url.URL{Scheme: "https", Host: "spade.twitch.tv", Path: "/"}))
	if err != nil || spadeClient == nil {
		log.WithError(err).Panic("Failed to create Spade client")
	}
}

func Handler(ctx context.Context, event events.SQSEvent) error {
	start := time.Now()
	channelIDs := make([]string, 0)

	for _, msg := range event.Records {
		var snsMsg models.Message
		err := json.Unmarshal([]byte(msg.Body), &snsMsg)
		if err != nil {
			log.WithError(err).WithField("body", msg.Body).
				Error("Error unmarshaling sqs message to sns model")
			return nil
		}

		var channel models.PanelUpdateSNSMessage
		err = json.Unmarshal([]byte(snsMsg.Message), &channel)
		if err != nil {
			log.WithError(err).WithField("body", snsMsg.Message).
				Error("Error unmarshaling sns message to panel model")
			return nil
		}

		channelIDs = append(channelIDs, channel.ChannelID)
	}

	for _, channelID := range channelIDs {
		channelPanels, err := pmsClient.GetChannelPanels(ctx, channelID, false, false, nil)
		if err != nil {
			log.WithError(err).Error(fmt.Sprintf("Error calling GetChannelPanels for channel: %s", channelID))
			return err
		}

		acceptsDonations := false
		for _, panel := range channelPanels.Panels {
			analysis := analyzer.Analyze(panel)

			for _, field := range analysis.Fields {
				acceptsDonations = true
				// Send the event data through spade in Production, just log it in lower environments
				if env == "prod" {
					// Only call Spade in production
					err = spadeClient.TrackEvent(
						ctx,
						models.SPADE_DONATION_LINK_EVENT_TYPE,
						models.DonationEvent{
							ChannelID:      channelID,
							DonationFlag:   true,
							SourceType:     models.PANEL,
							SourceID:       panel.ID,
							Field:          field.Field,
							ContextDetails: field.Context,
							Domains:        strings.Join(field.Domains, ", "),
						},
					)

					if err != nil {
						log.WithError(err).Error(fmt.Sprintf("Error sending channel panel spade event for (tuid: %s)", channelID))
						return err
					}
				} else {
					data, _ := json.Marshal(models.DonationEvent{
						ChannelID:      channelID,
						DonationFlag:   true,
						SourceType:     models.PANEL,
						SourceID:       panel.ID,
						Field:          field.Field,
						ContextDetails: field.Context,
						Domains:        strings.Join(field.Domains, ", "),
					})
					log.Info("Spade Event Data ", string(data))
				}
			}
		}

		if !acceptsDonations {
			// Send the event data through spade in Production, just log it in lower environments
			if env == "prod" {
				err = spadeClient.TrackEvent(
					ctx,
					models.SPADE_DONATION_LINK_EVENT_TYPE,
					models.DonationEvent{
						ChannelID:    channelID,
						DonationFlag: false,
						SourceType:   models.PANEL,
					},
				)

				if err != nil {
					log.WithError(err).Error(fmt.Sprintf("Error sending no dono spade event for (tuid: %s)", channelID))
					return err
				}
			} else {
				data, _ := json.Marshal(models.DonationEvent{
					ChannelID:    channelID,
					DonationFlag: false,
					SourceType:   models.PANEL,
				})
				log.Info("Spade Event Data ", string(data))
			}

		}

		fmt.Print(fmt.Sprintf("processed channelID: %s", channelID))
	}

	elapsed := time.Since(start)
	remainder := time.Second - elapsed
	if remainder > 0 {
		// Subtracting 100ms to keep the threshold just below 1s for billing reasons,
		// since lambdas bill in 100ms batches
		time.Sleep(time.Duration(int(remainder - 100*time.Millisecond)))
	}

	return nil
}

func main() {
	lambda.Start(Handler)
}
