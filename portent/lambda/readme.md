## Lambda - analyzes user panels
Lambda function is the thing that actually executes the live panel analysis.

When you need to make a change in Lambda, make the change in `handler.go` first and then follow the next steps:

(staging)
```
$ cd portent/lambda
$ chmod +x deploy.sh  # you should have to do this only once for all
$ ./deploy.sh
```
(prod)
```
$ cd portent/lambda
$ chmod +x build-zip.sh # you should have to do this only once for all
$ ./deploy.sh prod
```
This builds your code into an executable, compresses to a zip, and then deploys it.