#!/bin/bash

GOOS=linux go build -o main
zip handler.zip main ../data/donation_urls/donation_url_regex ../data/donation_urls/domain_url_regex

accountId="898907546878"
sqsSuffix="dev"
environment="staging"
export AWS_ACCESS_KEY_ID=$(aws configure get aws_access_key_id --profile portent-devo)
export AWS_SECRET_ACCESS_KEY=$(aws configure get aws_secret_access_key --profile portent-devo)

if [ "$1" == "prod" ]; then
    accountId="272653928523"
    sqsSuffix="prod"
    environment="prod"
    export AWS_ACCESS_KEY_ID=$(aws configure get aws_access_key_id --profile portent-prod)
    export AWS_SECRET_ACCESS_KEY=$(aws configure get aws_secret_access_key --profile portent-prod)
fi;

echo "Lambda Role: arn:aws:iam::$accountId:role/LambdaExecutionRole"
echo "SQS queue: arn:aws:sqs:us-west-2:$accountId:bits-enabled-tuids-queue-$sqsSuffix"

currentHandler=$(aws lambda get-function --function-name PanelHandler)

if [ -z "$currentHandler" ]
then
    echo "Function: 'PanelHandler' not found. Creating a new one"

    aws lambda create-function \
      --region us-west-2 \
      --function-name PanelHandler \
      --memory 128 \
      --role "arn:aws:iam::$accountId:role/LambdaExecutionRole" \
      --environment Variables="{ENV=$environment}" \
      --runtime go1.x \
      --zip-file fileb://handler.zip \
      --handler main
else
    echo "Function: existing 'PanelHandler' found. Updating the current one"

    aws lambda update-function-code \
      --function-name PanelHandler \
      --zip-file fileb://handler.zip
fi

aws lambda put-function-concurrency \
  --function-name PanelHandler \
  --reserved-concurrent-executions 3

rm main handler.zip