package materialocal_test

import (
	"testing"

	"code.justin.tv/amzn/MateriaTwirp"
	"code.justin.tv/commerce/Materia/internal/testsuite"
	"code.justin.tv/commerce/Materia/materialocal"
)

func TestMateriaLocal(t *testing.T) {
	testsuite.Twirp(t, func() MateriaTwirp.Materia {
		return materialocal.New()
	})
}
