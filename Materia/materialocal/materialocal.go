package materialocal

import (
	telemetry "code.justin.tv/amzn/TwitchTelemetry"
	"code.justin.tv/commerce/Materia/internal/local"
	"code.justin.tv/commerce/Materia/internal/server"
)

// New returns a local implementation of the Materia twirp interface to be used for testing.
func New() *server.MateriaServer {
	return &server.MateriaServer{
		EntitlementStore: &local.EntitlementStore{},
		SampleReporter:   telemetry.SampleReporter{},
	}
}
