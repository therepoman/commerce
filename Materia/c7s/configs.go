package c7s

// Config is the core configuration struct for the service
type Config struct {
	BasePrice int64 `c7:"basePrice"`
	Port      int64 `c7:"port"`
}
