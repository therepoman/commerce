package internal_test

import (
	"reflect"
	"testing"
	"time"

	"code.justin.tv/commerce/Materia/internal"
)

func TestEntitlement_Active(t *testing.T) {
	type fields struct {
		OwnerID  string
		Domain   string
		ItemID   string
		OriginID string
		StartsAt time.Time
		EndsAt   internal.InfTime
	}
	tests := []struct {
		name   string
		fields fields
		want   bool
	}{
		{
			name: "When entitlement EndDate is after now and StartDate is before now expect entitlement to not be active",
			fields: fields{
				OwnerID:  "some_owner",
				Domain:   "some_domain",
				ItemID:   "some_item",
				OriginID: "some_origin",
				StartsAt: time.Now().Add(-2 * time.Hour),
				EndsAt: internal.InfTime{
					Time: time.Now().Add(-1 * time.Hour),
				},
			},
			want: false,
		},
		{
			name: "When entitlement EndDate is before now and StartDate is after now expect entitlement to not be active",
			fields: fields{
				OwnerID:  "some_owner",
				Domain:   "some_domain",
				ItemID:   "some_item",
				OriginID: "some_origin",
				StartsAt: time.Now().Add(1 * time.Hour),
				EndsAt: internal.InfTime{
					Time: time.Now().Add(2 * time.Hour),
				},
			},
			want: false,
		},
		{
			name: "When entitlement EndDate is before now and StartDate is before now expect entitlement to be active",
			fields: fields{
				OwnerID:  "some_owner",
				Domain:   "some_domain",
				ItemID:   "some_item",
				OriginID: "some_origin",
				StartsAt: time.Now().Add(-1 * time.Hour),
				EndsAt: internal.InfTime{
					Time: time.Now().Add(1 * time.Hour),
				},
			},
			want: true,
		},
		{
			name: "When entitlement EndDate is unlimited and StartDate is before now expect entitlement to be active",
			fields: fields{
				OwnerID:  "some_owner",
				Domain:   "some_domain",
				ItemID:   "some_item",
				OriginID: "some_origin",
				StartsAt: time.Now().Add(-1 * time.Hour),
				EndsAt: internal.InfTime{
					IsInfinite: true,
				},
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			e := internal.Entitlement{
				OwnerID:  tt.fields.OwnerID,
				Domain:   tt.fields.Domain,
				ItemID:   tt.fields.ItemID,
				OriginID: tt.fields.OriginID,
				StartsAt: tt.fields.StartsAt,
				EndsAt:   tt.fields.EndsAt,
			}
			if got := e.Active(); got != tt.want {
				t.Errorf("Entitlement.Active() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFilterExpiredEntitlements(t *testing.T) {
	now := time.Now()

	type args struct {
		entitlements []internal.Entitlement
	}
	tests := []struct {
		name string
		args args
		want []internal.Entitlement
	}{
		{
			name: "When some entitlements are expired and some are active want to return the active entitlements only",
			args: args{
				entitlements: []internal.Entitlement{
					{
						OwnerID:  "some_owner",
						Domain:   "some_domain",
						ItemID:   "some_item",
						OriginID: "some_origin",
						StartsAt: now.Add(-2 * time.Hour),
						EndsAt: internal.InfTime{
							Time:       now.Add(-1 * time.Hour),
							IsInfinite: false,
						},
					},
					{
						OwnerID:  "some_owner",
						Domain:   "some_domain",
						ItemID:   "some_item2",
						OriginID: "some_origin",
						StartsAt: now.Add(1 * time.Hour),
						EndsAt: internal.InfTime{
							Time:       now.Add(2 * time.Hour),
							IsInfinite: false,
						},
					},
					{
						OwnerID:  "some_owner",
						Domain:   "some_domain",
						ItemID:   "some_item3",
						OriginID: "some_origin",
						StartsAt: now.Add(-1 * time.Hour),
						EndsAt: internal.InfTime{
							Time:       now.Add(1 * time.Hour),
							IsInfinite: false,
						},
					},
					{
						OwnerID:  "some_owner",
						Domain:   "some_domain",
						ItemID:   "some_item4",
						OriginID: "some_origin",
						StartsAt: now.Add(-1 * time.Hour),
						EndsAt: internal.InfTime{
							Time:       time.Time{},
							IsInfinite: true,
						},
					},
				},
			},
			want: []internal.Entitlement{
				{
					OwnerID:  "some_owner",
					Domain:   "some_domain",
					ItemID:   "some_item3",
					OriginID: "some_origin",
					StartsAt: now.Add(-1 * time.Hour),
					EndsAt: internal.InfTime{
						Time:       now.Add(1 * time.Hour),
						IsInfinite: false,
					},
				},
				{
					OwnerID:  "some_owner",
					Domain:   "some_domain",
					ItemID:   "some_item4",
					OriginID: "some_origin",
					StartsAt: now.Add(-1 * time.Hour),
					EndsAt: internal.InfTime{
						Time:       time.Time{},
						IsInfinite: true,
					},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := internal.FilterExpiredEntitlements(tt.args.entitlements); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("FilterExpiredEntitlements() = %v, want %v", got, tt.want)
			}
		})
	}
}
