package server

import (
	"testing"

	"code.justin.tv/amzn/MateriaTwirp"
	telemetry "code.justin.tv/amzn/TwitchTelemetry"
	"code.justin.tv/commerce/Materia/internal/testsuite"
)

func TestServer(t *testing.T) {
	// Uncomment below when timed twirp entitlements are added
	testsuite.Twirp(t, func() MateriaTwirp.Materia {
		s, err := NewServer(nil, telemetry.SampleReporter{}, nil)
		if err != nil {
			t.Fatalf("Error creating server: %v", err)
		}
		return s
	})
}
