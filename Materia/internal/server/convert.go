package server

import (
	"errors"

	serviceapi "code.justin.tv/amzn/MateriaTwirp"
	"code.justin.tv/commerce/Materia/internal"

	"github.com/golang/protobuf/ptypes"
)

func toTwirpEntitlements(entitlements []internal.Entitlement) ([]*serviceapi.Entitlement, error) {
	twirpEntitlements := make([]*serviceapi.Entitlement, 0, len(entitlements))
	for _, entitlement := range entitlements {
		startsAt, err := ptypes.TimestampProto(entitlement.StartsAt)
		if err != nil {
			return nil, err
		}

		endsAt, err := toTwirpInfiniteTime(entitlement.EndsAt)
		if err != nil {
			return nil, err
		}

		twirpEntitlements = append(twirpEntitlements, &serviceapi.Entitlement{
			OwnerId:  entitlement.OwnerID,
			Domain:   entitlement.Domain,
			ItemId:   entitlement.ItemID,
			OriginId: entitlement.OriginID,
			StartsAt: startsAt,
			EndsAt:   endsAt,
		})
	}
	return twirpEntitlements, nil
}

func toAppEntitlements(entitlements []*serviceapi.Entitlement) ([]internal.Entitlement, error) {
	appEntitlements := make([]internal.Entitlement, 0, len(entitlements))
	for _, entitlement := range entitlements {
		if entitlement == nil {
			return nil, errors.New("found nil Entitlement")
		}

		startsAt, err := ptypes.Timestamp(entitlement.StartsAt)
		if err != nil {
			return nil, err
		}

		endsAt, err := toAppInfTime(entitlement.EndsAt)
		if err != nil {
			return nil, err
		}

		appEntitlements = append(appEntitlements, internal.Entitlement{
			OwnerID:  entitlement.OwnerId,
			Domain:   entitlement.Domain,
			ItemID:   entitlement.ItemId,
			OriginID: entitlement.OriginId,
			StartsAt: startsAt,
			EndsAt:   endsAt,
		})
	}
	return appEntitlements, nil
}

func toTwirpInfiniteTime(infTime internal.InfTime) (*serviceapi.InfiniteTime, error) {
	if infTime.IsInfinite {
		return &serviceapi.InfiniteTime{IsInfinite: true}, nil
	}

	t, err := ptypes.TimestampProto(infTime.Time)
	if err != nil {
		return nil, err
	}

	return &serviceapi.InfiniteTime{Time: t}, nil
}

func toAppInfTime(infiniteTime *serviceapi.InfiniteTime) (internal.InfTime, error) {
	if infiniteTime == nil {
		return internal.InfTime{}, errors.New("found nil InfiniteTime")
	}

	if infiniteTime.IsInfinite {
		return internal.InfTime{IsInfinite: true}, nil
	}

	t, err := ptypes.Timestamp(infiniteTime.Time)
	if err != nil {
		return internal.InfTime{}, err
	}

	return internal.InfTime{Time: t}, nil
}
