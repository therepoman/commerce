package server

// The implementation of a Twirp server
// See https://twitchtv.github.io/twirp/docs/example.html#implement-the-server
// We generate your Twirp server interfaces in MateriaTwirp
// by taking a dependency on MateriaSchema

import (
	"context"
	"os"

	logging "code.justin.tv/amzn/TwitchLogging"
	telemetry "code.justin.tv/amzn/TwitchTelemetry"

	serviceapi "code.justin.tv/amzn/MateriaTwirp"
	"code.justin.tv/video/metricmiddleware-beta/awsmetric"
	"code.justin.tv/video/metricmiddleware-beta/operation"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/twitchtv/twirp"

	"code.justin.tv/commerce/Materia/internal"
	"code.justin.tv/commerce/Materia/internal/local"
)

// MateriaServer An implementation of the MateriaServer Twirp service.
type MateriaServer struct {
	SampleReporter   telemetry.SampleReporter
	Logger           logging.Logger
	EntitlementStore internal.EntitlementStore
}

var isColdStart = true

// NewServer creates a new MateriaServer. Expects to be executed
// inside an AWS ECS execution environment.
func NewServer(opStarter *operation.Starter, sampleReporter telemetry.SampleReporter, logger logging.Logger) (*MateriaServer, error) {
	// Report a cold start on every new server
	if isColdStart {
		sampleReporter.Report("ServerColdStart", 1, telemetry.UnitCount)
		isColdStart = false
	}

	// Dynamo DB setup
	sess, err := session.NewSession(&aws.Config{
		// Current region is pre-defined in the execution environment
		Region: aws.String(os.Getenv("AWS_DEFAULT_REGION")),
	})

	if err != nil {
		return nil, err
	}

	awsMetricClient := &awsmetric.Client{Starter: opStarter}
	sess = awsMetricClient.AddToSession(sess)

	localEntitlementStore := &local.EntitlementStore{}
	localEntitlementStore.CreateEntitlements(context.Background(), []internal.Entitlement{
		{OwnerID: "29386024", Domain: "emotes", ItemID: "chunkThink_BW", OriginID: "cyrus :)"},
		{OwnerID: "89614178", Domain: "emotes", ItemID: "chunkThink_BW", OriginID: "cyrus :)"},
	})

	return &MateriaServer{SampleReporter: sampleReporter, Logger: logger, EntitlementStore: localEntitlementStore}, nil
}

// GetEntitlements returns a list of entitlements for a given owner and domain.
func (s *MateriaServer) GetEntitlements(ctx context.Context, request *serviceapi.GetEntitlementsRequest) (*serviceapi.GetEntitlementsResponse, error) {
	entitlements, err := s.EntitlementStore.GetEntitlements(ctx, request.OwnerId, request.Domain)
	if err != nil {
		return nil, twirp.InternalErrorWith(err)
	}

	twirpEntitlements, err := toTwirpEntitlements(entitlements)
	if err != nil {
		return nil, twirp.InternalErrorWith(err)
	}

	return &serviceapi.GetEntitlementsResponse{
		Entitlements: twirpEntitlements,
	}, nil
}

// CreateEntitlements persists a given list of entitlements.
func (s *MateriaServer) CreateEntitlements(ctx context.Context, request *serviceapi.CreateEntitlementsRequest) (*serviceapi.CreateEntitlementsResponse, error) {
	appEntitlements, err := toAppEntitlements(request.Entitlements)
	if err != nil {
		return nil, twirp.InternalErrorWith(err)
	}

	err = s.EntitlementStore.CreateEntitlements(ctx, appEntitlements)
	if err != nil {
		return nil, twirp.InternalErrorWith(err)
	}
	return &serviceapi.CreateEntitlementsResponse{}, nil
}
