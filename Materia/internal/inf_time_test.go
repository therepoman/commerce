package internal_test

import (
	"testing"
	"time"

	"code.justin.tv/commerce/Materia/internal"
)

func TestInfTime_Before(t *testing.T) {
	type fields struct {
		Time       time.Time
		IsInfinite bool
	}
	type args struct {
		t time.Time
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name:   "When infinite time Before() should return false",
			fields: fields{IsInfinite: true},
			args:   args{t: time.Now()},
			want:   false,
		},
		{
			name:   "When InfTime before passed in time Before() should return true",
			fields: fields{Time: time.Now()},
			args:   args{t: time.Now().Add(time.Hour)},
			want:   true,
		},
		{
			name:   "When InfTime after passed in time Before() should return false",
			fields: fields{Time: time.Now()},
			args:   args{t: time.Now().Add(-1 * time.Hour)},
			want:   false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			i := internal.InfTime{
				Time:       tt.fields.Time,
				IsInfinite: tt.fields.IsInfinite,
			}
			if got := i.Before(tt.args.t); got != tt.want {
				t.Errorf("InfTime.Before() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestInfTime_After(t *testing.T) {
	type fields struct {
		Time       time.Time
		IsInfinite bool
	}
	type args struct {
		t time.Time
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name:   "When infinite time After() should return true",
			fields: fields{IsInfinite: true},
			args:   args{t: time.Now()},
			want:   true,
		},
		{
			name:   "When InfTime before passed in time After() should return false",
			fields: fields{Time: time.Now()},
			args:   args{t: time.Now().Add(time.Hour)},
			want:   false,
		},
		{
			name:   "When InfTime after passed in time After() should return true",
			fields: fields{Time: time.Now()},
			args:   args{t: time.Now().Add(-1 * time.Hour)},
			want:   true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			i := internal.InfTime{
				Time:       tt.fields.Time,
				IsInfinite: tt.fields.IsInfinite,
			}
			if got := i.After(tt.args.t); got != tt.want {
				t.Errorf("InfTime.After() = %v, want %v", got, tt.want)
			}
		})
	}
}
