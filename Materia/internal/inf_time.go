package internal

import (
	"time"
)

// InfTime represents a timestamp that is either a specific time or an infinite date in the future.
// Should be initialized with either Time set or IsInfinite set, but not both:
// - InfTime{Time: time.Now()}
// - InfTime{IsInfinite: true}
type InfTime struct {
	Time       time.Time
	IsInfinite bool
}

// Before returns true if the InfTime is before the passed in time.
func (i InfTime) Before(t time.Time) bool {
	if i.IsInfinite {
		return false
	}
	return i.Time.Before(t)
}

// After returns true if the InfTime is after the passed in time.
func (i InfTime) After(t time.Time) bool {
	if i.IsInfinite {
		return true
	}
	return i.Time.After(t)
}
