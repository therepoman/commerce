package internal

import (
	"context"
	"time"
)

// EntitlementStore is an interface with CRUD operations for entitlements. This interface can be
// implemented by Dynamo, a local in memory store, or any other means.
type EntitlementStore interface {
	// GetEntitlements returns active entitlements for a given user and domain.
	GetEntitlements(ctx context.Context, ownerID, domain string) ([]Entitlement, error)
	// CreateEntitlements creates a given list of entitlements in the store.
	CreateEntitlements(ctx context.Context, entitlements []Entitlement) error
}

// Entitlement represents a relation between a Twitch user (owner) and an item. If this
// relation exists, then an owner is entitled to said item. An entitlement can be time based
// with a start date and an end date. If an entitlement is not within a given start/end date, then
// the entitlement is not active.
type Entitlement struct {
	OwnerID  string
	Domain   string
	ItemID   string
	OriginID string

	StartsAt time.Time
	EndsAt   InfTime
}

// Active determines if the entitlement is active given its StartsAt and EndsAt fields.
func (e Entitlement) Active() bool {
	now := time.Now()
	if e.StartsAt.After(now) || e.EndsAt.Before(now) {
		return false
	}
	return true
}

// FilterExpiredEntitlements returns a new slice with all expired entitelments removed from the
// passed in slice of entitlements.
func FilterExpiredEntitlements(entitlements []Entitlement) []Entitlement {
	filteredEntitltments := make([]Entitlement, 0, len(entitlements))
	for _, entitlement := range entitlements {
		if entitlement.Active() {
			filteredEntitltments = append(filteredEntitltments, entitlement)
		}
	}
	return filteredEntitltments
}
