package local

import (
	"testing"

	"code.justin.tv/commerce/Materia/internal"
	"code.justin.tv/commerce/Materia/internal/testsuite"
)

func TestEntitlementStore(t *testing.T) {
	testsuite.EntitlementStore(t, func() internal.EntitlementStore {
		return &EntitlementStore{}
	})
}
