package local

import (
	"context"
	"sync"

	"code.justin.tv/commerce/Materia/internal"
)

type EntitlementStore struct {
	store []internal.Entitlement
	mu    sync.RWMutex
}

func (es *EntitlementStore) GetEntitlements(ctx context.Context, ownerID, domain string) ([]internal.Entitlement, error) {
	es.mu.RLock()
	defer es.mu.RUnlock()

	filteredEntitlementsByUserDomain := []internal.Entitlement{}
	for _, entitlement := range es.store {
		if entitlement.OwnerID == ownerID && entitlement.Domain == domain {
			filteredEntitlementsByUserDomain = append(filteredEntitlementsByUserDomain, entitlement)
		}
	}
	return internal.FilterExpiredEntitlements(filteredEntitlementsByUserDomain), nil
}

func (es *EntitlementStore) CreateEntitlements(ctx context.Context, entitlements []internal.Entitlement) error {
	es.mu.Lock()
	defer es.mu.Unlock()

	es.store = append(es.store, entitlements...)
	return nil
}
