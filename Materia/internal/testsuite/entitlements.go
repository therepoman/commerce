package testsuite

import (
	"context"
	"fmt"
	"reflect"
	"testing"
	"time"

	"code.justin.tv/commerce/Materia/internal"
)

func EntitlementStore(t *testing.T, makeEntitlementStore func() internal.EntitlementStore) {
	futureDate := time.Now().Add(1 * time.Hour)
	pastDate := time.Now().Add(-1 * time.Hour)

	type args struct {
		ownerID string
		domain  string
	}
	tests := []struct {
		name   string
		expect string

		entitlementsToCreate []internal.Entitlement
		args                 args

		want    []internal.Entitlement
		wantErr bool
	}{
		{
			name:   "When no entitlements exist",
			expect: "no entitlements to be returned",

			entitlementsToCreate: []internal.Entitlement{},
			args:                 args{ownerID: "some_owner", domain: "some_domain"},

			want:    []internal.Entitlement{},
			wantErr: false,
		},
		{
			name:   "When entitlements are created",
			expect: "entitlements with the given ownerID and domain should be returned",

			entitlementsToCreate: []internal.Entitlement{
				{OwnerID: "owner1", Domain: "domain1", OriginID: "origin1", ItemID: "item1", StartsAt: pastDate, EndsAt: internal.InfTime{Time: futureDate}},
				{OwnerID: "owner1", Domain: "domain1", OriginID: "origin2", ItemID: "item2", StartsAt: pastDate, EndsAt: internal.InfTime{Time: futureDate}},
				{OwnerID: "owner1", Domain: "domain2", OriginID: "origin1", ItemID: "item1", StartsAt: pastDate, EndsAt: internal.InfTime{Time: futureDate}},
				{OwnerID: "owner2", Domain: "domain1", OriginID: "origin1", ItemID: "item1", StartsAt: pastDate, EndsAt: internal.InfTime{Time: futureDate}},
				{OwnerID: "owner2", Domain: "domain2", OriginID: "origin1", ItemID: "item1", StartsAt: pastDate, EndsAt: internal.InfTime{Time: futureDate}},
			},
			args: args{ownerID: "owner1", domain: "domain1"},

			want: []internal.Entitlement{
				{OwnerID: "owner1", Domain: "domain1", OriginID: "origin1", ItemID: "item1", StartsAt: pastDate, EndsAt: internal.InfTime{Time: futureDate}},
				{OwnerID: "owner1", Domain: "domain1", OriginID: "origin2", ItemID: "item2", StartsAt: pastDate, EndsAt: internal.InfTime{Time: futureDate}},
			},
			wantErr: false,
		},
		{
			name:   "When entitlements are created",
			expect: "entitlements with a different ownerID and domain should not be returned",

			entitlementsToCreate: []internal.Entitlement{
				{OwnerID: "owner1", Domain: "domain1", OriginID: "origin1", ItemID: "item1", StartsAt: pastDate, EndsAt: internal.InfTime{Time: futureDate}},
				{OwnerID: "owner1", Domain: "domain1", OriginID: "origin2", ItemID: "item2", StartsAt: pastDate, EndsAt: internal.InfTime{Time: futureDate}},
				{OwnerID: "owner1", Domain: "domain2", OriginID: "origin1", ItemID: "item1", StartsAt: pastDate, EndsAt: internal.InfTime{Time: futureDate}},
				{OwnerID: "owner2", Domain: "domain1", OriginID: "origin1", ItemID: "item1", StartsAt: pastDate, EndsAt: internal.InfTime{Time: futureDate}},
				{OwnerID: "owner2", Domain: "domain2", OriginID: "origin1", ItemID: "item1", StartsAt: pastDate, EndsAt: internal.InfTime{Time: futureDate}},
			},
			args: args{ownerID: "owner3", domain: "domain1"},

			want:    []internal.Entitlement{},
			wantErr: false,
		},
		{
			name:   "When entitlements are created, but only some are active",
			expect: "only active entitlements with the given ownerID and domain should be returned",

			entitlementsToCreate: []internal.Entitlement{
				{OwnerID: "owner1", Domain: "domain1", OriginID: "origin1", ItemID: "item1", StartsAt: pastDate, EndsAt: internal.InfTime{Time: futureDate}},
				{OwnerID: "owner1", Domain: "domain1", OriginID: "origin2", ItemID: "item2", StartsAt: pastDate, EndsAt: internal.InfTime{Time: pastDate}},
				{OwnerID: "owner1", Domain: "domain2", OriginID: "origin1", ItemID: "item1", StartsAt: pastDate, EndsAt: internal.InfTime{Time: futureDate}},
				{OwnerID: "owner2", Domain: "domain1", OriginID: "origin1", ItemID: "item1", StartsAt: pastDate, EndsAt: internal.InfTime{Time: futureDate}},
				{OwnerID: "owner2", Domain: "domain2", OriginID: "origin1", ItemID: "item1", StartsAt: pastDate, EndsAt: internal.InfTime{Time: futureDate}},
			},
			args: args{ownerID: "owner1", domain: "domain1"},

			want: []internal.Entitlement{
				{OwnerID: "owner1", Domain: "domain1", OriginID: "origin1", ItemID: "item1", StartsAt: pastDate, EndsAt: internal.InfTime{Time: futureDate}},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(fmt.Sprintf("%s expect %s", tt.name, tt.expect), func(t *testing.T) {
			ctx := context.Background()
			entitlementStore := makeEntitlementStore()
			if len(tt.entitlementsToCreate) > 0 {
				err := entitlementStore.CreateEntitlements(ctx, tt.entitlementsToCreate)
				if err != nil {
					t.Fatalf("Error creating entitlements %+v: %v", tt.entitlementsToCreate, err)
				}
			}

			got, err := entitlementStore.GetEntitlements(ctx, tt.args.ownerID, tt.args.domain)
			if (err != nil) != tt.wantErr {
				t.Fatalf("EntitlementDB.GetEntitlements() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Fatalf("EntitlementDB.GetEntitlements() = %+v, want %+v", got, tt.want)
			}
		})
	}
}
