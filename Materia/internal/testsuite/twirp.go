package testsuite

import (
	"context"
	"fmt"
	"reflect"
	"testing"
	"time"

	"code.justin.tv/amzn/MateriaTwirp"

	"github.com/golang/protobuf/ptypes"
)

func assertTwirpEntitlements(t *testing.T, actual, expected []*MateriaTwirp.Entitlement) {
	t.Helper()

	if len(actual) != len(expected) {
		t.Fatalf("Expected %v entitlements, but got %v entitlements instead", len(expected), len(actual))
	}

	for i := range actual {
		if !reflect.DeepEqual(*actual[i], *expected[i]) {
			t.Fatalf("Expected %+v, but got %+v instead", *expected[i], *actual[i])
		}
	}
}

func Twirp(t *testing.T, makeServer func() MateriaTwirp.Materia) {
	testTwirpGetEntitlements(t, makeServer)
}

func testTwirpGetEntitlements(t *testing.T, makeServer func() MateriaTwirp.Materia) {
	dateBeforeNow, err := ptypes.TimestampProto(time.Now().Add(-1 * time.Hour))
	if err != nil {
		t.Fatal(err)
	}

	dateAfterNow, err := ptypes.TimestampProto(time.Now().Add(1 * time.Hour))
	if err != nil {
		t.Fatal(err)
	}

	tests := []struct {
		name   string
		expect string

		existingEntitlements []*MateriaTwirp.Entitlement
		request              *MateriaTwirp.GetEntitlementsRequest

		want    *MateriaTwirp.GetEntitlementsResponse
		wantErr bool
	}{
		{
			name:   "When no entitlements exist",
			expect: "no entitlements to be returned",

			existingEntitlements: []*MateriaTwirp.Entitlement{},
			request: &MateriaTwirp.GetEntitlementsRequest{
				OwnerId: "some_owner",
				Domain:  "some_domain",
			},

			want: &MateriaTwirp.GetEntitlementsResponse{
				Entitlements: nil,
			},
			wantErr: false,
		},
		{
			name:   "When entitlements are created",
			expect: "entitlements with the given ownerID and domain should be returned",

			existingEntitlements: []*MateriaTwirp.Entitlement{
				{OwnerId: "owner1", Domain: "domain1", OriginId: "origin1", ItemId: "item1", StartsAt: dateBeforeNow, EndsAt: &MateriaTwirp.InfiniteTime{IsInfinite: true}},
				{OwnerId: "owner1", Domain: "domain1", OriginId: "origin2", ItemId: "item2", StartsAt: dateBeforeNow, EndsAt: &MateriaTwirp.InfiniteTime{IsInfinite: true}},
				{OwnerId: "owner1", Domain: "domain2", OriginId: "origin1", ItemId: "item1", StartsAt: dateBeforeNow, EndsAt: &MateriaTwirp.InfiniteTime{IsInfinite: true}},
				{OwnerId: "owner2", Domain: "domain1", OriginId: "origin1", ItemId: "item1", StartsAt: dateBeforeNow, EndsAt: &MateriaTwirp.InfiniteTime{IsInfinite: true}},
				{OwnerId: "owner2", Domain: "domain2", OriginId: "origin1", ItemId: "item1", StartsAt: dateBeforeNow, EndsAt: &MateriaTwirp.InfiniteTime{IsInfinite: true}},
			},
			request: &MateriaTwirp.GetEntitlementsRequest{
				OwnerId: "owner1",
				Domain:  "domain1",
			},

			want: &MateriaTwirp.GetEntitlementsResponse{
				Entitlements: []*MateriaTwirp.Entitlement{
					{OwnerId: "owner1", Domain: "domain1", OriginId: "origin1", ItemId: "item1", StartsAt: dateBeforeNow, EndsAt: &MateriaTwirp.InfiniteTime{IsInfinite: true}},
					{OwnerId: "owner1", Domain: "domain1", OriginId: "origin2", ItemId: "item2", StartsAt: dateBeforeNow, EndsAt: &MateriaTwirp.InfiniteTime{IsInfinite: true}},
				},
			},
			wantErr: false,
		},
		{
			name:   "When entitlements are created",
			expect: "entitlements with a different ownerID and domain should not be returned",

			existingEntitlements: []*MateriaTwirp.Entitlement{
				{OwnerId: "owner1", Domain: "domain1", OriginId: "origin1", ItemId: "item1", StartsAt: dateBeforeNow, EndsAt: &MateriaTwirp.InfiniteTime{IsInfinite: true}},
				{OwnerId: "owner1", Domain: "domain1", OriginId: "origin2", ItemId: "item2", StartsAt: dateBeforeNow, EndsAt: &MateriaTwirp.InfiniteTime{IsInfinite: true}},
				{OwnerId: "owner1", Domain: "domain2", OriginId: "origin1", ItemId: "item1", StartsAt: dateBeforeNow, EndsAt: &MateriaTwirp.InfiniteTime{IsInfinite: true}},
				{OwnerId: "owner2", Domain: "domain1", OriginId: "origin1", ItemId: "item1", StartsAt: dateBeforeNow, EndsAt: &MateriaTwirp.InfiniteTime{IsInfinite: true}},
				{OwnerId: "owner2", Domain: "domain2", OriginId: "origin1", ItemId: "item1", StartsAt: dateBeforeNow, EndsAt: &MateriaTwirp.InfiniteTime{IsInfinite: true}},
			},
			request: &MateriaTwirp.GetEntitlementsRequest{
				OwnerId: "owner3",
				Domain:  "domain1",
			},

			want: &MateriaTwirp.GetEntitlementsResponse{
				Entitlements: []*MateriaTwirp.Entitlement{},
			},
			wantErr: false,
		},
		{
			name:   "When entitlements are created, but only some are active",
			expect: "only active entitlements with the given ownerID and domain should be returned",

			existingEntitlements: []*MateriaTwirp.Entitlement{
				{OwnerId: "owner1", Domain: "domain1", OriginId: "origin1", ItemId: "item1", StartsAt: dateBeforeNow, EndsAt: &MateriaTwirp.InfiniteTime{Time: dateAfterNow}},
				{OwnerId: "owner1", Domain: "domain1", OriginId: "origin2", ItemId: "item2", StartsAt: dateBeforeNow, EndsAt: &MateriaTwirp.InfiniteTime{Time: dateBeforeNow}},
				{OwnerId: "owner1", Domain: "domain2", OriginId: "origin1", ItemId: "item1", StartsAt: dateBeforeNow, EndsAt: &MateriaTwirp.InfiniteTime{Time: dateBeforeNow}},
				{OwnerId: "owner2", Domain: "domain1", OriginId: "origin1", ItemId: "item1", StartsAt: dateBeforeNow, EndsAt: &MateriaTwirp.InfiniteTime{Time: dateBeforeNow}},
				{OwnerId: "owner2", Domain: "domain2", OriginId: "origin1", ItemId: "item1", StartsAt: dateBeforeNow, EndsAt: &MateriaTwirp.InfiniteTime{Time: dateBeforeNow}},
			},
			request: &MateriaTwirp.GetEntitlementsRequest{
				OwnerId: "owner1",
				Domain:  "domain1",
			},

			want: &MateriaTwirp.GetEntitlementsResponse{
				Entitlements: []*MateriaTwirp.Entitlement{
					{OwnerId: "owner1", Domain: "domain1", OriginId: "origin1", ItemId: "item1", StartsAt: dateBeforeNow, EndsAt: &MateriaTwirp.InfiniteTime{Time: dateAfterNow}},
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(fmt.Sprintf("%s expect %s", tt.name, tt.expect), func(t *testing.T) {
			server := makeServer()
			ctx := context.Background()

			if len(tt.existingEntitlements) > 0 {
				_, err := server.CreateEntitlements(ctx, &MateriaTwirp.CreateEntitlementsRequest{
					Entitlements: tt.existingEntitlements,
				})
				if err != nil {
					t.Fatal(err)
				}
			}

			got, err := server.GetEntitlements(ctx, tt.request)
			if (err != nil) != tt.wantErr {
				t.Errorf("MateriaServer.GetEntitlements() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			assertTwirpEntitlements(t, got.Entitlements, tt.want.Entitlements)
		})
	}
}
