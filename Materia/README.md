# Materia

Materia is the Twitch Entitlements Service, which keeps track of what benefits a user on Twitch has.
Currently Materia is used to store emoticon entitlements from Channel Points, such as individual
emotes and  emote modifications obtained by using Community Points.

## Integrating with Materia
If you would like to integrate with Materia, please first reach out to the Subscriptions team in the
\#subs slack channel. Materia is a Twirp service that provides RPCs to entitle users to benefits,
get a user's entitlements, and delete a user's entitlements -- all detailed in our
[proto file](https://code.amazon.com/packages/MateriaSchema/blobs/mainline/--/model/code.justin.tv/amzn/MateriaTwirp/twirp.proto).

More information to come on integrating if you are a Twitch service vs a Fulton service.

---

# Developing on Materia

## Useful Links
* [Metrics Dashboard](https://w.amazon.com/bin/view/Twitch/Fulton/Materia/Dashboard)
* [Deployment Pipeline](https://pipelines.amazon.com/pipelines/Materia)
* [VPC Infrastructure Pipeline](https://pipelines.amazon.com/pipelines/MateriaVpcInfrastructure)
* Repositories
  * [Materia](https://code.amazon.com/packages/Materia)
  * [MateriaSchema](https://code.amazon.com/packages/MateriaSchema)
  * [MateriaClientConfig](https://code.amazon.com/packages/MateriaClientConfig)
  * [MateriaTwirp](https://code.amazon.com/packages/MateriaTwirp)
* [Fulton Docs](https://docs.fulton.twitch.a2z.com/docs/intro.html)

## First Time Setup
### Setup Brazil Workspace

Run the following commands to create your Brazil workspace:
```
export NAME=Materia
mkdir -p ~/workspaces
cd ~/workspaces
brazil workspace create --name "${NAME}" && \
cd "${NAME}" && \
brazil workspace use -p "${NAME}" -p "${NAME}"Twirp -p "${NAME}"Schema -p "${NAME}"ClientConfig -vs "${NAME}"/development && \
brazil setup platform-support
```

This command will create your Materia workspace under `~/workspaces/Materia`. In this directory you will
find four folders under the `src/` directory:
* **Materia**: Contains all our application code.
* **MateriaSchema**: Contains our proto file.
* **MateriaTwirp**: Does not contain any source code, but will build the twirp files from the proto
file at runtime.
* **MateriaClientConfig**: Contains default settings for consumers to import for our twirp client.

### Setup IDE
See [the Fulton docs for setting up your IDE](https://docs.fulton.twitch.a2z.com/docs/ide_support.html?search=ide).
If you use VSCode you will want to [enable modules support](https://github.com/Microsoft/vscode-go/wiki/Go-modules-support-in-Visual-Studio-Code).

### Testing Changes
I am still figuring out the best way to test changes. The Fulton docs have [a few suggested ways](https://docs.fulton.twitch.a2z.com/docs/getting_started_existing_project.html#5-testing)
including using RDE or SAM to run tests against real infrastructure.

It is also possible to run locally, although advised to use the tests above to test your changes. To run locally,
run the command `AWS_PROFILE=twitch-materia-beta-us-west-2 HOSTNAME=$USER AWS_DEFAULT_REGION=us-west-2 go run cmd/Materia/main.go`.

### Code Reviews using CRUX
Instead of using Github, Fulton uses CRUX for code reviews. CRUX integrates with Brazil much better
since it can create a Code Review containing multiple packages (eg. both Materia and MateriaSchema).

After making and commiting your changes to a branch, run the following to create a CRUX Code Review:
```
cr --all
```

Follow the link provided by the command, add a description and a title, then press the `Publish review`
button in the top left corner. Upon making new changes, commit them to your branch and run `cr --all`
again to add them to the CRUX CR, follow the link, select the new revision, and press the button to
publish a new revision.

### The Deployment Pipeline
Upon merging to mainline, [the deployment pipeline](https://pipelines.amazon.com/pipelines/Materia)
will automatically build, test, and deploy your new changes. Currently, only staging (beta) is setup,
but production is coming soon.

---
# Old README below

# What?

This package comes with an example ECS service. ECS task setup is managed via CloudFormation.

The CloudFormation is in `configuration/cloudFormation`. You set these parameters in your LPT package.

## Parameters you can set:
*  CPU: Defaults to `256`. The number of `cpu` units used by the task. You should [read the Fargate docs](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-resource-ecs-taskdefinition.html#cfn-ecs-taskdefinition-cpu) on this parameter as it controls how much CPU and Memory you can set.
   For the impatient:
   ```
      256 (.25 vCPU) - Available memory values: 0.5GB, 1GB, 2GB

      512 (.5 vCPU) - Available memory values: 1GB, 2GB, 3GB, 4GB

      1024 (1 vCPU) - Available memory values: 2GB, 3GB, 4GB, 5GB, 6GB, 7GB, 8GB

      2048 (2 vCPU) - Available memory values: Between 4GB and 16GB in 1GB increments

      4096 (4 vCPU) - Available memory values: Between 8GB and 30GB in 1GB increments

    ```
*  Memory: Defaults to: `512` in MB See above for available settings.
*  ServiceContainerPort: Defaults to: `8080`. The port your container will listen on. We auto-wire an NLB and ALB in front of your service; these terminate on the container at this port.
*  ServiceDesiredCount: Defaults to: `1`.  The desired number of containers.
*  CanaryServiceDesiredCount: Defaults to: `1`. The desired number of containers for your canary cluster. We recommend keeping this at `1` for most cases, but you can adjust it to fit your needs.
*  Substage: Defaults to: `'primary'`. The substage of your container. We recommend keeping this as `'primary'`, see the Fulton Docs for more info on this.

## Parameters not to mess with:

All of these parameters are set via LPT so you probably don't want to modify these:

*  Stage: Defaults to: `'beta'`. The logical stage of your container; you probably don't want to override this as we set it in LPT based on the stage of your Pipeline
*  UseBatsKeyParam: Defaults to `false`. Don't mess with this as it will lead to issues getting your S3 bucket for deployment purposes.
*  ImageTag: Pipelines will compute this for you as it replicates build artifacts - the Docker image to deploy to ECS
*  ImageDigest: Pipelines will compute this for you as it replicates build artifacts - the SHA of the Docker image.
*  DeploymentBucketImportName: We compute this for you, this is where you set which bucket to pull your deployment from.
*  PipelinesControlledEcrRespository: We fill this for you.

## Changing LPT parameters

You edit CloudFormation stack parameters in your service's LPT package, in the `stack_params_for_options` method. Run `brazil-build synthesize-pipeline`, and kick off a build in your ECS pipeline to see the new values take effect.

This package uses [CfnBuild](https://code.amazon.com/packages/CfnBuild) to build the final CloudFormation templates it uses. See the README in [CfnBuild](https://code.amazon.com/packages/CfnBuild) for details. 

This package comes with some other goodies out of the box, we have [SAMToolkit](https://w.amazon.com/index.php/SAMToolkit) built in, as well as [Rapid Dev Environment](https://w.amazon.com/index.php/RDE).

# The Example ECS service

The example ECS service gets and updates pricing data in a DynamoDB table.

# SAMToolkit

SAMToolkit is when you want to deploy a local ECS cluster to your own AWS Account.
In short, it packages (in a similar way to BATS) your ECS setup, and deploys (including your CloudFormation) to whatever account you configure it for. SAMToolkit supports many other features as well, which are covered [in the wiki](https://w.amazon.com/index.php/SAMToolkit).


See [the fulton docs](https://docs.fulton.twitch.a2z.com/docs/getting_started_existing_project.html#sam) for more information.

# RDE (Local Integration Tests)

[RDE](https://w.amazon.com/index.php/RDE) is a tool that emulates a full development environment on your local machine (including [laptops with full credential access](https://w.amazon.com/index.php/RDE/How_do_I#Use_AWS_credentials_on_my_Mac)). It's based on Docker.
We set up a base `definition.yaml` file that sets up the RDE workflow. You can add more to this file to run more custom workflows against your code.

Again see [the fulton docs] (https://docs.fulton.twitch.a2z.com/docs/getting_started_existing_project.html#rde) for the most up-to-date information.

You can debug your container locally by following [this guide](https://w.amazon.com/index.php/RDE/Tutorial/Container_Applications).

To setup credentials use the following [guide](https://w.amazon.com/index.php/RDE/How_do_I#Use_AWS_credentials_on_my_Mac).

# General workflow

For testing with containers, here's our current recommendation:

1. Unit tests. Run good old fashioned unit tests against your code.
2. RDE. Use `make test_rde_personal` to call the local endpoint to test your service.
3. SAMToolkit. Deploy your container + Associated cloud formation to your own personal testing account, make sure it's all happy.
4. CR and Push. Run integration tests in your pipeline for your function.


# Mac development

If you are developing on a Mac, you will need to enable [platform support](https://w.amazon.com/index.php/BrazilCLI%202.0/PlatformSupport). Otherwise, and among other issues, commands like `brazil-build release` or `brazil-build test` will silently skip running the unit tests while reporting `BUILD SUCCEEDED`.

