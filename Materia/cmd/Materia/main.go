package main

// The implementation of a Twirp server for Lambda
// See https://twitchtv.github.io/twirp/docs/example.html#implement-the-server
// We generate your Twirp server interfaces in MateriaTwirp
// by taking a dependency on MateriaSchema

import (
	"fmt"
	"net/http"

	serviceapi "code.justin.tv/amzn/MateriaTwirp"
	logmiddleware "code.justin.tv/amzn/TwitchLoggingMiddleware"
	poller "code.justin.tv/amzn/TwitchTelemetryPollingCollector"
	"code.justin.tv/commerce/Materia/c7s"
	"code.justin.tv/commerce/Materia/internal/server"
	"golang.a2z.com/FultonGoHttp/handlers"
	"golang.a2z.com/FultonGoLangBootstrap/bootstrap"
	"golang.a2z.com/FultonGoLangBootstrap/fultonecs"
	simplebootstrap "golang.a2z.com/FultonGoLangSimpleBootstrap/bootstrap"

	agent "code.justin.tv/amzn/TwitchTelemetrySystemMetricsCollector"
	"code.justin.tv/video/metricmiddleware-beta/twirpmetric"
	"context"
	"github.com/twitchtv/twirp"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"
)

func main() {
	// Spin up a Fulton bootstrap for ECS (via FultonGoLangBootstrap)
	bootstrapper := &fultonecs.ECSBootstrapper{
		ServiceName:     "Materia",
		ConfigSetLoader: bootstrap.DefaultConfigSetLoader,
		MetricsPlatform: simplebootstrap.CloudWatch,
	}
	bs, err := bootstrapper.Bootstrap()
	if err != nil {
		panic(fmt.Sprintf("Error bootstrapping fulton: %s", err))
	}
	// Load the C7 server-side config
	config := c7s.Config{}
	err = bs.C7.FillWithNamespace("Materia", &config)
	if err != nil {
		panic(fmt.Sprintf("Error loading config: %s", err))
	}
	// Initialize a middleware for metrics and logging - you get metrics for RPCs to other services baked in
	// Along with select AWS services

	metricsMiddleware := &twirpmetric.Server{Starter: bs.OperationStarter}

	errorLoggingMiddleware := logmiddleware.Server{
		Logger: bs.Logger,

		// Status codes that are just noise and don't need to be logged can be filtered here.
		// Suppressing the logging won't affect metrics.
		// SuppressedErrorCodes: []twirp.ErrorCode{twirp.Unauthenticated, twirp.BadRoute},
	}
	// ...and prepare a way for Twirp's server side to use it
	twirpMiddleware := twirp.ChainHooks(
		metricsMiddleware.ServerHooks(),
		errorLoggingMiddleware.ServerHooks(),
	)
	// Spin up a Server interface
	// This is the same as where the public Twirp tutorial "mounts" the server
	// https://twitchtv.github.io/twirp/docs/example.html#mount-and-run-the-server
	srv, err := server.NewServer(
		bs.OperationStarter,
		bs.SampleReporter,
		bs.Logger)

	if err != nil {
		panic("Failed to create twirp server")
	}

	twirpHandler := serviceapi.NewMateriaServer(srv, twirpMiddleware)
	// We spin up a Mux to serve a custom health check on `/ping`
	// Which we use on the ALBs to check service health.

	mux := http.NewServeMux()
	mux.Handle(serviceapi.MateriaPathPrefix, twirpHandler)

	pingPath := "/ping"
	mux.HandleFunc(pingPath, func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte("OK"))
		if err != nil {
			bs.Logger.Log("Unable to write 'OK' to response for a " + pingPath + " request")
		}
	})
	// You probably want to make sure you're not taking an unlimited-sized Twirp RPC
	maxRequestSize := int64(10 << 20) // 10 MiB
	handler := handlers.LimitRequestSizes(mux, maxRequestSize)
	// We specify the port from the C7 file
	location := fmt.Sprintf(":%d", config.Port)
	server := &http.Server{
		Addr:    location,
		Handler: handler,
	}
	bs.Logger.Log("Server listening on "+location,
		"ProcessAddressLaunchID", bs.ProcessIdentifier.LaunchID,
		"ProcessAddressMachine", bs.ProcessIdentifier.Machine,
		"ProcessAddressVersion", bs.ProcessIdentifier.Version,
		"ServiceTupleStage", bs.ProcessIdentifier.Stage,
		"ServiceTupleSubstage", bs.ProcessIdentifier.Substage,
		"ServiceTupleService", bs.ProcessIdentifier.Service,
		"ServiceTupleRegion", bs.ProcessIdentifier.Region)

	// Start up the Go service stat poller (for metrics like GC)
	goStatsPoller := poller.NewGoStatsPollingCollector(10*time.Second, &bs.SampleReporter.SampleBuilder, bs.SampleReporter.SampleObserver, bs.Logger)
	goStatsPoller.Start()

	// Create a poller for system level metrics
	// Please see https://code.amazon.com/packages/TwitchTelemetrySystemMetricsCollector/trees/mainline for additional
	// agents and info
	systemMetricsConfig := agent.SystemMetricsCollectorConfig{
		Collectors: []agent.SystemMetricsCollector{agent.CPUCollector},
		PerCPU:     false,
	}
	systemMetricsPoller := agent.NewSystemMetricsPollingCollector(systemMetricsConfig, 10*time.Second, bs.ProcessIdentifier, bs.SampleReporter.SampleObserver, bs.Logger)
	systemMetricsPoller.Start()
	// Implement a cleaner shutdown
	shutdownChannel := make(chan bool)
	go func() {
		shutdownSignalChannel := make(chan os.Signal, 1)
		signal.Notify(shutdownSignalChannel, syscall.SIGINT, syscall.SIGTERM)
		sig := <-shutdownSignalChannel
		bs.Logger.Log("Received shutdown request", "Signal", sig)

		// Shut down the server
		err := server.Shutdown(context.Background())

		// End system metrics polling and metrics in general (ensure we flush metrics)
		systemMetricsPoller.Stop()
		goStatsPoller.Stop()
		bs.SampleReporter.SampleObserver.Stop()

		if err != nil {
			log.Fatal("Failed to shut down server: ", err)
		}
		close(shutdownChannel)
	}()

	// And finally launch the server
	err = server.ListenAndServe()
	if err != nil && err != http.ErrServerClosed {
		log.Fatal(err)
	}
	<-shutdownChannel
	bs.Logger.Log("Server shut down")
}
