package integrationtest

// See https://docs.fulton.twitch.a2z.com/docs/integration_tests.html for more information on how integration tests work
import (
	"os"
	"testing"

	"code.justin.tv/amzn/C7-go/c7"
	"code.justin.tv/amzn/C7-go/resolvers"
	"code.justin.tv/amzn/TwirpGoLangClientAutowire/c7autowire"
	"code.justin.tv/amzn/TwirpGoLangClientAutowire/configs"
	"code.justin.tv/commerce/Materia/internal/local"
	"code.justin.tv/commerce/Materia/internal/server"
	"github.com/stretchr/testify/assert"

	serviceapi "code.justin.tv/amzn/MateriaTwirp"
)

const (
	// functionName function to test
	testVegetable = "integration test vegetable"
)

var (
	region string
	stage  string
)

// TestMain test setup.
func TestMain(m *testing.M) {
	stage = "beta"
	if os.Getenv("LOGICAL_STAGE_NAME") != "" {
		stage = os.Getenv("LOGICAL_STAGE_NAME")
	}

	retCode := m.Run()

	os.Exit(retCode)
}

func getMateria(t *testing.T) serviceapi.Materia {
	t.Helper()

	// Remove this (and uncomment below) when dynamo is added. Cannot use real int tests with local
	// ntitlement store since they will hit different ecs contaners with different stores.
	return &server.MateriaServer{
		EntitlementStore: &local.EntitlementStore{},
	}

	// if os.Getenv("HYDRA") == "" {
	// 	return &server.MateriaServer{
	// 		EntitlementStore: &local.EntitlementStore{},
	// 	}
	// }

	// return protoClient(t)
}

func jsonClient(t *testing.T) serviceapi.Materia {
	t.Helper()
	lc := autowiredClient(t)

	// Get the twirp client
	return serviceapi.NewMateriaJSONClient(getClientEndpoint(t), lc)
}

func protoClient(t *testing.T) serviceapi.Materia {
	t.Helper()
	lc := autowiredClient(t)
	return serviceapi.NewMateriaProtobufClient(getClientEndpoint(t), lc)
}

func autowiredClient(t *testing.T) serviceapi.HTTPClient {
	t.Helper()

	twirpAutowire := c7autowire.NewC7TwirpAutowire(loadC7(t))

	client, err := twirpAutowire.ClientForService("MateriaClient")
	assert.NoError(t, err)
	return client
}

func loadC7(t *testing.T) *c7.C7 {
	t.Helper()

	configFile := "all.c7"
	if os.Getenv("FULTON_CONFIG") != "" {
		configFile = os.Getenv("FULTON_CONFIG")
	}

	set, err := resolvers.ResolveFile(configFile)
	assert.NoError(t, err)

	return c7.NewC7(*set, region, stage)
}

func getClientEndpoint(t *testing.T) string {
	t.Helper()
	config := configs.TwirpClientConfig{}
	err := loadC7(t).FillWithNamespace("MateriaClient", &config)
	assert.NoError(t, err)
	return config.Endpoint
}

func TestIntegrationGetEntitlements(t *testing.T) {
	// Uncomment below when adding timed twirp entitlements
	// testsuite.Twirp(t, func() serviceapi.Materia {
	// 	return getMateria(t)
	// })
}
