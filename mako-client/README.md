# mako-client
---

## THIS IS DEPRECATED.
### Please use [commerce/mako/twirp](http://git-aws.internal.justin.tv/commerce/mako) instead.

---
This package contains the Twirp client for the Mako service.

Further Reading:
 - Mako: https://git-aws.internal.justin.tv/commerce/mako
 - Twirp: https://git-aws.internal.justin.tv/common/twirp
 - Protobuf: https://developers.google.com/protocol-buffers/

## Usage
1. Pull down this package with `git clone` or `go get`
2. Make changes to the mako-service.proto
3. Run `make` to generate server and client go code
4. PR and merge your changes
5. Go to the mako repo and run `make upgrade` to pull in the latest version of this package. You may need to clear the glide cache with `glide cc` to correctly update.
