package chitin

import (
	"fmt"
	"time"
)

type eventKind int

const (
	serverRequestBodyClose eventKind = iota
	serverWriteHeader
	serverReadEOF
	serverRequestBadHeader
	serverResponseBadHeader
)

type multiEvent struct {
	kind     eventKind
	duration time.Duration
	status   int
	bytes    int64
	header   string
}

var _ event = multiEvent{}

func (ev multiEvent) Logfmt() string {
	switch ev.kind {
	default:
		return ""
	case serverRequestBodyClose:
		return fmt.Sprintf("body=close duration=%s", micros(ev.duration))
	case serverWriteHeader:
		return fmt.Sprintf("write-header status=%d duration=%s",
			ev.status, micros(ev.duration))
	case serverReadEOF:
		return fmt.Sprintf("body=EOF duration=%s bytes=%d", micros(ev.duration), ev.bytes)
	case serverRequestBadHeader:
		return fmt.Sprintf("reserved-header=%q", ev.header)
	case serverResponseBadHeader:
		return fmt.Sprintf("reserved-header-out=%q", ev.header)
	}
}

func micros(d time.Duration) string {
	return fmt.Sprintf("%dµs", d/time.Microsecond)
}
