module code.justin.tv/commerce/safedax

go 1.15

require (
	github.com/antlr/antlr4 v0.0.0-20200915201312-e73f72be7355 // indirect
	github.com/aws/aws-dax-go v1.2.1
	github.com/aws/aws-sdk-go v1.35.1
	github.com/cep21/circuit/v3 v3.1.1
	github.com/gofrs/uuid v3.3.0+incompatible // indirect
	github.com/stretchr/testify v1.3.0
)
