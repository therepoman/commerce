# Safedax
Safedax provides an abstraction over DAX and Dynamo clients to help prevent a DAX outage from entirely bringing down a 
service's data layer. The Safedax provider includes a [circuit](github.com/cep21/circuit) that wraps DAX calls, allowing 
graceful fallbacks to the direct Dynamo client.
 
## How it works
The `safedax.Provider` type stores your Dynamo and optional DAX client. To interact with your database, you invoke one of
the provider's methods: `safedax.Provider.UseDynamo`, `safedax.Provider.UseDAX`, `safedax.Provider.PreferDAX`, and pass
a function that will receive an abstract Dynamo client as a parameter.

### Creating a provider
```go
package example
import (
    "code.justin.tv/commerce/safedax"
    "github.com/aws/aws-dax-go/dax"
    "github.com/aws/aws-sdk-go/aws/session"
    "github.com/aws/aws-sdk-go/service/dynamodb"
)

func Example() {
    dynamoClient := dynamodb.New(session.Must(session.NewSession()))

    // A Dynamo client is always required.
    provider := safedax.NewProvider(dynamoClient)
    
    // Add a DAX client, which is optional (but why do you need this library if you aren't using DAX?)
    daxClient, _ := dax.New(dax.DefaultConfig())
    // Chaining methods produce clones of the provider
    provider = provider.WithDAXClient(daxClient)
}
```

### Using a provider to call Dynamo
Any interaction with DAX/Dynamo should be wrapped by one of the method's providers. The methods each follow the same scheme,
but differ in how they expose DAX/Dynamo clients to the caller. The caller passes a function that accepts a context value
and an abstract `dynamodbiface.DynamoDBAPI` value, using these values to perform any desired Dynamo actions.

#### `(*Provider).PreferDAX`
`(*Provider).PreferDAX` first attempts to hand the DAX client to the caller. DAX actions are wrapped in a circuit, and if 
the circuit opens, any calls to `PreferDAX` will immediately receive the Dynamo client instead. DAX actions that return an
error will also be retried using a fallback to Dynamo.

```go
package example

import (
    "context"

    "code.justin.tv/commerce/safedax"
    "github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
)

var provider *safedax.Provider

// initialization elided

func Example() {
    var output *dynamodb.GetItemOutput
    err := provider.PreferDAX(context.Background(), func(ctx context.Context, ddb dynamodbiface.DynamoDBAPI) {
        var ddbErr error
        output, ddbErr = ddb.GetItemWithContext(ctx, &dynamodb.GetItemInput{
        	// fields omitted
    	})
        return ddbErr
    })
}
```

#### `(*Provider).UseDAX`
`(*Provider).UseDAX` is identical to PreferDAX, but will only ever expose the DAX client to the caller. If the circuit is open, nothing will happen.

#### `(*Provider).UseDynamo`
`(*Provider).UseDynamo` is identical to PreferDAX, but will only ever expose the direct DynamoDB client to the caller.

### Request and error hooks
Providers can be passed a set of hooks, which will be called at certain points during the lifecycle of a single call to one of `PreferDAX`, `UseDAX`, or `UseDynamo`.
These hooks can be used to log errors, emit metrics, or whatever else you want.
```go
p = p.WithHooks(safedax.Hooks{
    BeforeRequest: func(context.Context) {},
    AfterRequest: func(context.Context) {},
    OnError: func(context.Context, error) {},

    BeforeDAXRequest: func(context.Context) {},
    AfterDAXRequest: func(context.Context) {},
    OnDAXError: func(context.Context, error) {},

    BeforeDynamoRequest: func(context.Context) {},
    AfterDynamoRequest: func(context.Context) {},
    OnDynamoError: func(context.Context, error) {},
})
```
Hooks are called in these orders: (Note that circuit operations around DAX are not illustrated here)
```
PreferDAX: 
    BeforeRequest -> 
    BeforeDAXRequest ->
    <DAX call occurs> ->
    AfterRequest -> 
    AfterDAXRequest ->
        if err == nil: -> done
        if err != nil: ->
            OnError -> 
            OnDAXError ->
            BeforeRequest ->
            BeforeDynamoRequest ->
            <Dynamo call occurs> ->
            AfterRequest ->
            AfterDynamoRequest ->
                if err == nil: -> done
                if err != nil: ->
                    OnError ->
                    OnDynamoError -> done
```
```
UseDAX: 
    BeforeRequest ->
    BeforeDAXRequest ->
    <DAX call occurs> ->
    AfterRequest ->
    AfterDAXRequest ->
        if err == nil: -> done
        if err != nil: ->
            OnError ->
            OnDAXError -> done
```
```
UseDynamo: 
    BeforeRequest ->
    BeforeDynamoRequest ->
    <Dynamo call occurs> ->
    AfterRequest ->
    AfterDynamoRequest ->
        if err == nil -> done
        if err != nil: ->
            OnError ->
            OnDynamoError -> done
```

### Using a custom circuit
Providers created using `NewProvider` come with a default circuit that accords to Hystrix logic. If you want to customize
this behavior more deeply, you can call `(*Provider).WithCircuit` to attach your own circuit:
```go
package example
import (
    "code.justin.tv/commerce/safedax"
    "github.com/aws/aws-sdk-go/service/dynamodb"
    "github.com/cep21/circuit/v3"
)

func Example() {
    var ddb *dynamodb.DynamoDB // initialization elided
    
    myCircuit := circuit.NewCircuitFromConfig("custom-circuit", circuit.Config{
        /* customize your circuit here */
    })

    provider := safedax.NewProvider(ddb).WithCircuit(myCircuit)
}
```