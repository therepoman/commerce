package safedax

import (
	"context"
	"errors"
	"time"

	"github.com/aws/aws-dax-go/dax"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/request"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"github.com/cep21/circuit/v3"
	"github.com/cep21/circuit/v3/closers/hystrix"
)

var (
	NilExecutorError    = errors.New("executor function cannot be nil")
	NoDynamoClientError = errors.New("no Dynamo client is configured")
	NoDAXClientError    = errors.New("no DAX client is configured")
)

type NoFallbackError struct {
	Err error
}

func (n NoFallbackError) Error() string {
	return n.Err.Error()
}

// implements circuit.BadRequest
func (n NoFallbackError) BadRequest() bool {
	return true
}

type ExecutorFunc func(context.Context, dynamodbiface.DynamoDBAPI) error

type Provider struct {
	dynamo  *dynamodb.DynamoDB
	dax     *dax.Dax
	circuit *circuit.Circuit
	hooks   safeHooks
}

func NewProvider(dynamo *dynamodb.DynamoDB) *Provider {
	if dynamo == nil {
		panic(NoDynamoClientError)
	}

	return &Provider{dynamo: dynamo, circuit: defaultCircuit()}
}

func (p *Provider) WithDAXClient(dax *dax.Dax) *Provider {
	tmp := *p
	tmp.dax = dax
	return &tmp
}

func (p *Provider) WithCircuit(circuit *circuit.Circuit) *Provider {
	tmp := *p
	tmp.circuit = circuit
	return &tmp
}

type Hooks struct {
	BeforeRequest func(context.Context)
	AfterRequest  func(context.Context)
	OnError       func(context.Context, error)

	BeforeDAXRequest func(context.Context)
	AfterDAXRequest  func(context.Context)
	OnDAXError       func(context.Context, error)

	BeforeDynamoRequest func(context.Context)
	AfterDynamoRequest  func(context.Context)
	OnDynamoError       func(context.Context, error)
}

func (h Hooks) asSafe() safeHooks {
	return safeHooks{
		BeforeRequest:       safeRequestHook{h.BeforeRequest},
		AfterRequest:        safeRequestHook{h.AfterRequest},
		OnError:             safeErrorHook{h.OnError},
		BeforeDAXRequest:    safeRequestHook{h.BeforeDAXRequest},
		AfterDAXRequest:     safeRequestHook{h.AfterDAXRequest},
		OnDAXError:          safeErrorHook{h.OnDAXError},
		BeforeDynamoRequest: safeRequestHook{h.BeforeDynamoRequest},
		AfterDynamoRequest:  safeRequestHook{h.AfterDynamoRequest},
		OnDynamoError:       safeErrorHook{h.OnDynamoError},
	}
}

type safeRequestHook struct{ fn func(context.Context) }

func (s safeRequestHook) call(ctx context.Context) {
	if s.fn != nil {
		s.fn(ctx)
	}
}

type safeErrorHook struct{ fn func(context.Context, error) }

func (s safeErrorHook) call(ctx context.Context, err error) {
	if s.fn != nil {
		s.fn(ctx, err)
	}
}

type safeHooks struct {
	BeforeRequest, AfterRequest, BeforeDAXRequest,
	AfterDAXRequest, BeforeDynamoRequest, AfterDynamoRequest safeRequestHook

	OnError, OnDAXError, OnDynamoError safeErrorHook
}

func (p *Provider) WithHooks(hooks Hooks) *Provider {
	tmp := *p
	tmp.hooks = hooks.asSafe()
	return &tmp
}

func (p *Provider) PreferDAX(ctx context.Context, fn ExecutorFunc) error {
	if fn == nil {
		return NilExecutorError
	}

	if p.dax == nil {
		return p.UseDynamo(ctx, fn)
	}

	return p.circuit.Execute(ctx, p.daxExecutor(fn), func(ctx context.Context, lastErr error) error {
		// If AWS doesn't want to retry the DAX error, short-circuit the fallback.
		// We don't want to hit Dynamo if Dynamo was already pushing back on DAX,
		// or if it's a bad request.
		//    As a special case to AWS's retry condition, we only short-circuit
		// for a context timeout if it was a parent context timeout rather than
		// a context timeout within the DAX executor (which has its own aggressive
		// timeout on top of any parent timeout). This is detected by 1) checking
		// if the awsErr is a CanceledError; 2) checking if the parent context
		// (which is in scope here) is canceled. If 1) is true but 2) is not,
		// we can retry with Dynamo.
		if !request.IsErrorRetryable(lastErr) {
			if awsErr, ok := lastErr.(awserr.Error); (ok && awsErr.Code() == request.CanceledErrorCode && ctx.Err() != nil) ||
				(ok && awsErr.Code() != request.CanceledErrorCode) ||
				!ok {
				return lastErr
			}
		}

		return p.UseDynamo(ctx, fn)
	})
}

func (p *Provider) UseDAX(ctx context.Context, fn ExecutorFunc) error {
	if fn == nil {
		return NilExecutorError
	}

	if p.dax == nil {
		return NoDAXClientError
	}

	return p.circuit.Execute(ctx, p.daxExecutor(fn), nil)
}

func (p *Provider) daxExecutor(fn ExecutorFunc) func(ctx context.Context) error {
	return func(ctx context.Context) error {
		p.hooks.BeforeRequest.call(ctx)
		p.hooks.BeforeDAXRequest.call(ctx)

		err := fn(ctx, p.dax)

		p.hooks.AfterRequest.call(ctx)
		p.hooks.AfterDAXRequest.call(ctx)

		if err != nil {
			p.hooks.OnError.call(ctx, err)
			p.hooks.OnDAXError.call(ctx, err)
		}

		return err
	}
}

func (p *Provider) UseDynamo(ctx context.Context, fn ExecutorFunc) error {
	if fn == nil {
		return NilExecutorError
	}
	if p.dynamo == nil {
		return NoDynamoClientError
	}

	p.hooks.BeforeRequest.call(ctx)
	p.hooks.BeforeDynamoRequest.call(ctx)

	err := fn(ctx, p.dynamo)

	p.hooks.AfterRequest.call(ctx)
	p.hooks.AfterDynamoRequest.call(ctx)

	if err != nil {
		p.hooks.OnError.call(ctx, err)
		p.hooks.OnDynamoError.call(ctx, err)
	}

	return err
}

func defaultCircuit() *circuit.Circuit {
	hystrixConfig := hystrix.Factory{}
	c := hystrixConfig.Configure("safedax.Provider")
	c.Execution = circuit.ExecutionConfig{
		// Disable max concurrent requests – allow the service to throttle us as necessary.
		MaxConcurrentRequests: -1,
		// 100ms timeout for DAX (pretty generous)
		Timeout: 100 * time.Millisecond,
	}
	c.Fallback = circuit.FallbackConfig{MaxConcurrentRequests: -1}
	return circuit.NewCircuitFromConfig("safedax.Provider", c)
}
