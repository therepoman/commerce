package safedax_test

import (
	"context"
	"errors"
	"testing"

	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/request"

	"code.justin.tv/commerce/safedax"

	"github.com/aws/aws-dax-go/dax"
	"github.com/aws/aws-sdk-go/awstesting/mock"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"github.com/cep21/circuit/v3"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestProviderCreation(t *testing.T) {
	t.Run("NewProvider with empty DDB client", func(t *testing.T) {
		assert.Panics(t, func() {
			_ = safedax.NewProvider(nil)
		})
	})

	t.Run("NewProvider with populated DDB client", func(t *testing.T) {
		p := safedax.NewProvider(dynamodb.New(mock.Session))
		assert.NotNil(t, p)
	})

	t.Run("NewProvider.WithDAXClient", func(t *testing.T) {
		p := safedax.NewProvider(dynamodb.New(mock.Session))
		assert.NotNil(t, p)

		p = p.WithDAXClient(&dax.Dax{})

		assert.NotNil(t, p)
	})

	t.Run("NewProvider.WithCircuit", func(t *testing.T) {
		p := safedax.NewProvider(dynamodb.New(mock.Session))
		assert.NotNil(t, p)

		c := circuit.NewCircuitFromConfig("test", circuit.Config{})

		p = p.WithCircuit(c)
		assert.NotNil(t, p)
	})
}

func TestUseDynamo(t *testing.T) {
	t.Run("nil client", func(t *testing.T) {
		p := &safedax.Provider{}

		err := p.UseDynamo(context.Background(), func(ctx context.Context, dbapi dynamodbiface.DynamoDBAPI) error {
			return nil
		})

		assert.Error(t, err)
	})

	t.Run("nil executor", func(t *testing.T) {
		ddb := dynamodb.New(mock.Session)
		p := safedax.NewProvider(ddb)

		err := p.UseDynamo(context.Background(), nil)
		assert.Error(t, err)
	})

	t.Run("no error from executor", func(*testing.T) {
		ddb := dynamodb.New(mock.Session)
		p := safedax.NewProvider(ddb)

		err := p.UseDynamo(context.Background(), func(ctx context.Context, dbapi dynamodbiface.DynamoDBAPI) error {
			assert.IsType(t, ddb, dbapi)
			return nil
		})
		assert.NoError(t, err)
	})

	t.Run("error from executor", func(*testing.T) {
		ddb := dynamodb.New(mock.Session)
		p := safedax.NewProvider(ddb)

		err := p.UseDynamo(context.Background(), func(ctx context.Context, dbapi dynamodbiface.DynamoDBAPI) error {
			assert.IsType(t, ddb, dbapi)
			return errors.New(":'(")
		})
		assert.Error(t, err)
	})
}

func TestUseDAX(t *testing.T) {
	setup := func(t *testing.T) *safedax.Provider {
		p := safedax.NewProvider(dynamodb.New(mock.Session))
		return p
	}

	t.Run("nil client", func(t *testing.T) {
		p := setup(t)

		err := p.UseDAX(context.Background(), func(ctx context.Context, dbapi dynamodbiface.DynamoDBAPI) error {
			return nil
		})

		assert.Error(t, err)
		assert.EqualError(t, err, safedax.NoDAXClientError.Error())
	})

	t.Run("nil executor", func(t *testing.T) {
		p := setup(t).WithDAXClient(&dax.Dax{})

		err := p.UseDAX(context.Background(), nil)

		assert.Error(t, err)
		assert.EqualError(t, err, safedax.NilExecutorError.Error())
	})

	t.Run("no error from executor", func(t *testing.T) {
		p := setup(t).WithDAXClient(&dax.Dax{})

		err := p.UseDAX(context.Background(), func(ctx context.Context, dbapi dynamodbiface.DynamoDBAPI) error {
			assert.IsType(t, &dax.Dax{}, dbapi)
			return nil
		})

		assert.NoError(t, err)
	})

	t.Run("error from executor", func(t *testing.T) {
		p := setup(t).WithDAXClient(&dax.Dax{})

		err := p.UseDAX(context.Background(), func(ctx context.Context, dbapi dynamodbiface.DynamoDBAPI) error {
			assert.IsType(t, &dax.Dax{}, dbapi)
			return errors.New(":'(")
		})

		assert.Error(t, err)
	})
}

func TestPreferDAX(t *testing.T) {
	setup := func(t *testing.T) *safedax.Provider {
		ddb := dynamodb.New(mock.Session)
		daxc := &dax.Dax{}
		p := safedax.NewProvider(ddb)
		return p.WithDAXClient(daxc)
	}

	t.Run("circuit closed, receives DAX client", func(t *testing.T) {
		p := setup(t)

		c := circuit.NewCircuitFromConfig("testing", circuit.Config{})

		p = p.WithCircuit(c)

		c.CloseCircuit()

		err := p.PreferDAX(context.Background(), func(ctx context.Context, dbapi dynamodbiface.DynamoDBAPI) error {
			assert.IsType(t, &dax.Dax{}, dbapi)
			return nil
		})

		assert.NoError(t, err)
	})

	t.Run("circuit open, receives Dynamo client", func(t *testing.T) {
		p := setup(t)

		c := circuit.NewCircuitFromConfig("testing", circuit.Config{})

		p = p.WithCircuit(c)

		c.OpenCircuit()

		err := p.PreferDAX(context.Background(), func(ctx context.Context, dbapi dynamodbiface.DynamoDBAPI) error {
			assert.IsType(t, &dynamodb.DynamoDB{}, dbapi)
			return nil
		})

		assert.NoError(t, err)
	})

	t.Run("failure on DAX call, falls back to Dynamo", func(t *testing.T) {
		p := setup(t)

		c := circuit.NewCircuitFromConfig("testing", circuit.Config{})

		p = p.WithCircuit(c)

		var calls int

		err := p.PreferDAX(context.Background(), func(ctx context.Context, dbapi dynamodbiface.DynamoDBAPI) error {
			calls++
			if calls == 1 {
				assert.IsType(t, &dax.Dax{}, dbapi)
				return errors.New("your dax asplode again :(")
			}

			assert.IsType(t, &dynamodb.DynamoDB{}, dbapi)
			return nil
		})

		assert.Equal(t, 2, calls)
		assert.NoError(t, err)
	})

	t.Run("NoFallbackError from DAX does not fall back to Dynamo", func(t *testing.T) {
		p := setup(t)

		c := circuit.NewCircuitFromConfig("testing", circuit.Config{})

		p = p.WithCircuit(c)

		var calls int

		err := p.PreferDAX(context.Background(), func(ctx context.Context, dbapi dynamodbiface.DynamoDBAPI) error {
			calls++
			if calls == 1 {
				assert.IsType(t, &dax.Dax{}, dbapi)
				return safedax.NoFallbackError{errors.New("your dax asplode again :(")}
			}

			assert.IsType(t, &dynamodb.DynamoDB{}, dbapi)
			return nil
		})

		assert.Equal(t, 1, calls)
		assert.Error(t, err)
		assert.EqualError(t, err, "your dax asplode again :(")
	})

	t.Run("ProvisionedThroughputExceededException from DAX does not fall back to Dynamo", func(t *testing.T) {
		p := setup(t)

		c := circuit.NewCircuitFromConfig("testing", circuit.Config{})

		p = p.WithCircuit(c)

		var calls int

		err := p.PreferDAX(context.Background(), func(ctx context.Context, dbapi dynamodbiface.DynamoDBAPI) error {
			calls++
			if calls == 1 {
				assert.IsType(t, &dax.Dax{}, dbapi)
				return &dynamodb.ProvisionedThroughputExceededException{}
			}

			return nil
		})

		assert.Equal(t, 1, calls)
		assert.Error(t, err)
		assert.IsType(t, &dynamodb.ProvisionedThroughputExceededException{}, err)
	})

	t.Run("a canceled parent context does not fall back to Dynamo", func(t *testing.T) {
		p := setup(t)

		c := circuit.NewCircuitFromConfig("testing", circuit.Config{})

		p = p.WithCircuit(c)

		var calls int

		ctx, cancel := context.WithCancel(context.Background())

		err := p.PreferDAX(ctx, func(ctx context.Context, dbapi dynamodbiface.DynamoDBAPI) error {
			calls++
			if calls == 1 {
				assert.IsType(t, &dax.Dax{}, dbapi)
				cancel()
				return awserr.New(request.CanceledErrorCode, "", ctx.Err())
			}

			return nil
		})

		assert.Equal(t, 1, calls)
		assert.Error(t, err)
	})

	t.Run("a canceled execution context in DAX call falls back to Dynamo", func(t *testing.T) {
		p := setup(t)

		c := circuit.NewCircuitFromConfig("testing", circuit.Config{})

		p = p.WithCircuit(c)

		var calls int

		err := p.PreferDAX(context.Background(), func(ctx context.Context, dbapi dynamodbiface.DynamoDBAPI) error {
			calls++
			if calls == 1 {
				assert.IsType(t, &dax.Dax{}, dbapi)
				return awserr.New(request.CanceledErrorCode, "", nil)
			}

			return nil
		})

		assert.Equal(t, 2, calls)
		assert.NoError(t, err)
	})
}

type mockHook struct {
	calls int
}

func (m *mockHook) requestHook(_ context.Context) {
	m.calls++
}

func (m *mockHook) errorHook(_ context.Context, _ error) {
	m.calls++
}

// readability helpers
var called = mockHook{calls: 1}
var calledTwice = mockHook{calls: 2}
var notCalled = mockHook{calls: 0}

type mockHooks struct {
	BeforeRequest, AfterRequest, OnError,
	BeforeDAXRequest, AfterDAXRequest, OnDAXError,
	BeforeDynamoRequest, AfterDynamoRequest, OnDynamoError mockHook
}

func (m *mockHooks) asHooks() safedax.Hooks {
	return safedax.Hooks{
		BeforeRequest:       m.BeforeRequest.requestHook,
		AfterRequest:        m.AfterRequest.requestHook,
		OnError:             m.OnError.errorHook,
		BeforeDAXRequest:    m.BeforeDAXRequest.requestHook,
		AfterDAXRequest:     m.AfterDAXRequest.requestHook,
		OnDAXError:          m.OnDAXError.errorHook,
		BeforeDynamoRequest: m.BeforeDynamoRequest.requestHook,
		AfterDynamoRequest:  m.AfterDynamoRequest.requestHook,
		OnDynamoError:       m.OnDynamoError.errorHook,
	}
}

func TestHooks(t *testing.T) {
	setup := func() *safedax.Provider {
		ddb := dynamodb.New(mock.Session)
		daxc := &dax.Dax{}
		p := safedax.NewProvider(ddb)
		return p.WithDAXClient(daxc)
	}

	type providerMethod func(context.Context, safedax.ExecutorFunc) error

	cases := []struct {
		description    string
		methodSelector func(p *safedax.Provider) providerMethod
		executor       func(context.Context, dynamodbiface.DynamoDBAPI) error
		want           mockHooks
	}{
		{
			description: "DAX only flow, no error",
			methodSelector: func(p *safedax.Provider) providerMethod {
				return p.UseDAX
			},
			executor: func(ctx context.Context, dbapi dynamodbiface.DynamoDBAPI) error {
				return nil
			},
			want: mockHooks{
				BeforeRequest:       called,
				AfterRequest:        called,
				OnError:             notCalled,
				BeforeDAXRequest:    called,
				AfterDAXRequest:     called,
				OnDAXError:          notCalled,
				BeforeDynamoRequest: notCalled,
				AfterDynamoRequest:  notCalled,
				OnDynamoError:       notCalled,
			},
		},
		{
			description: "DAX only flow with error",
			methodSelector: func(p *safedax.Provider) providerMethod {
				return p.UseDAX
			},
			executor: func(ctx context.Context, dbapi dynamodbiface.DynamoDBAPI) error {
				return errors.New("asplode :(")
			},
			want: mockHooks{
				BeforeRequest:       called,
				AfterRequest:        called,
				OnError:             called,
				BeforeDAXRequest:    called,
				AfterDAXRequest:     called,
				OnDAXError:          called,
				BeforeDynamoRequest: notCalled,
				AfterDynamoRequest:  notCalled,
				OnDynamoError:       notCalled,
			},
		},
		{
			description: "Dynamo only flow, no error",
			methodSelector: func(p *safedax.Provider) providerMethod {
				return p.UseDynamo
			},
			executor: func(ctx context.Context, dbapi dynamodbiface.DynamoDBAPI) error {
				return nil
			},
			want: mockHooks{
				BeforeRequest:       called,
				AfterRequest:        called,
				OnError:             notCalled,
				BeforeDAXRequest:    notCalled,
				AfterDAXRequest:     notCalled,
				OnDAXError:          notCalled,
				BeforeDynamoRequest: called,
				AfterDynamoRequest:  called,
				OnDynamoError:       notCalled,
			},
		},
		{
			description: "Dynamo only flow with error",
			methodSelector: func(p *safedax.Provider) providerMethod {
				return p.UseDynamo
			},
			executor: func(ctx context.Context, dbapi dynamodbiface.DynamoDBAPI) error {
				return errors.New("asplode :(")
			},
			want: mockHooks{
				BeforeRequest:       called,
				AfterRequest:        called,
				OnError:             called,
				BeforeDAXRequest:    notCalled,
				AfterDAXRequest:     notCalled,
				OnDAXError:          notCalled,
				BeforeDynamoRequest: called,
				AfterDynamoRequest:  called,
				OnDynamoError:       called,
			},
		},
		{
			description: "Fallback flow with no DAX error, no Dynamo error",
			methodSelector: func(p *safedax.Provider) providerMethod {
				return p.PreferDAX
			},
			executor: func(ctx context.Context, dbapi dynamodbiface.DynamoDBAPI) error {
				return nil
			},
			want: mockHooks{
				BeforeRequest:       called,
				AfterRequest:        called,
				OnError:             notCalled,
				BeforeDAXRequest:    called,
				AfterDAXRequest:     called,
				OnDAXError:          notCalled,
				BeforeDynamoRequest: notCalled,
				AfterDynamoRequest:  notCalled,
				OnDynamoError:       notCalled,
			},
		},
		{
			description: "Fallback flow with DAX error, no Dynamo error",
			methodSelector: func(p *safedax.Provider) providerMethod {
				return p.PreferDAX
			},
			executor: func(ctx context.Context, dbapi dynamodbiface.DynamoDBAPI) error {
				if _, ok := dbapi.(*dax.Dax); ok {
					return errors.New("bad dax >:(")
				}
				return nil
			},
			want: mockHooks{
				BeforeRequest:       calledTwice,
				AfterRequest:        calledTwice,
				OnError:             called,
				BeforeDAXRequest:    called,
				AfterDAXRequest:     called,
				OnDAXError:          called,
				BeforeDynamoRequest: called,
				AfterDynamoRequest:  called,
				OnDynamoError:       notCalled,
			},
		},
		{
			description: "Fallback flow with DAX error and Dynamo error",
			methodSelector: func(p *safedax.Provider) providerMethod {
				return p.PreferDAX
			},
			executor: func(ctx context.Context, dbapi dynamodbiface.DynamoDBAPI) error {
				return errors.New("your system is down")
			},
			want: mockHooks{
				BeforeRequest:       calledTwice,
				AfterRequest:        calledTwice,
				OnError:             calledTwice,
				BeforeDAXRequest:    called,
				AfterDAXRequest:     called,
				OnDAXError:          called,
				BeforeDynamoRequest: called,
				AfterDynamoRequest:  called,
				OnDynamoError:       called,
			},
		},
	}

	for _, tt := range cases {
		t.Run(tt.description, func(t *testing.T) {
			var hooks mockHooks
			p := setup().WithHooks(hooks.asHooks())

			require.NotNil(t, tt.methodSelector)
			method := tt.methodSelector(p)

			_ = method(context.Background(), tt.executor)

			assert.Equal(t, tt.want, hooks)
		})
	}
}
