package model

import (
	__client__ "code.justin.tv/commerce/CoralGoClient/src/coral/client"
	__codec__ "code.justin.tv/commerce/CoralGoCodec/src/coral/codec"
	__dialer__ "code.justin.tv/commerce/CoralGoClient/src/coral/dialer"
)

//Provides discovery functionality for Amazon Digital Goods, including search and product details.
type ADGDiscoveryServiceClient struct {
	C __client__.Client
}

//Creates a new ADGDiscoveryServiceClient
func NewADGDiscoveryServiceClient(dialer __dialer__.Dialer, codec __codec__.RoundTripper) (service *ADGDiscoveryServiceClient) {
	return &ADGDiscoveryServiceClient{__client__.NewClient("com.amazon.adg.discovery.model", "ADGDiscoveryService", dialer, codec)}
}

//Retrieves multiple paged responses of all the products for the specified domains.
//productsInDomainRequest - Requests to specify the domain and paging parameters that products should be fetched from.
//traversal - The traversal to use to find the domains.  See traversal for more details.
//productFilter - The ProductFilter to be applied to the returned objects.  Products that do not pass this filter will not be returned.
func (this *ADGDiscoveryServiceClient) GetProductsInDomains(input GetProductsInDomainsRequest) (GetProductsInDomainsResponse, error) {
	var output GetProductsInDomainsResponse
	err := this.C.Call("com.amazon.adg.discovery.model", "getProductsInDomains", input, &output)
	return output, err
}

//Retrieves the product details for a given product, including store specific information
//(e.g. app details for apps, Skill command words for skills, etc).
func (this *ADGDiscoveryServiceClient) GetProductListDetails(input GetProductListDetailsRequest) (GetProductListDetailsResponse, error) {
	var output GetProductListDetailsResponse
	err := this.C.Call("com.amazon.adg.discovery.model", "getProductListDetails", input, &output)
	return output, err
}

//Retrieves the reviews for a given product.
func (this *ADGDiscoveryServiceClient) GetProductReviews(input GetProductReviewsRequest) (GetProductReviewsResponse, error) {
	var output GetProductReviewsResponse
	err := this.C.Call("com.amazon.adg.discovery.model", "getProductReviews", input, &output)
	return output, err
}
func (this *ADGDiscoveryServiceClient) ValidateDocket(input ValidateDocketRequest) (ValidateDocketResponse, error) {
	var output ValidateDocketResponse
	err := this.C.Call("com.amazon.adg.discovery.model", "validateDocket", input, &output)
	return output, err
}

//Get the list of categories given either a root node id or product line. Given either input, it will retrieve the
//id, name, description and itemTypeKeyword of all the immediate children. If both a id and product line are provided,
//the id will be preferred.
func (this *ADGDiscoveryServiceClient) GetCategories(input GetCategoriesRequest) (GetCategoriesResponse, error) {
	var output GetCategoriesResponse
	err := this.C.Call("com.amazon.adg.discovery.model", "getCategories", input, &output)
	return output, err
}

//Provides a list of recommended widgets
func (this *ADGDiscoveryServiceClient) GetRecommendedWidgets(input GetRecommendedWidgetsRequest) (GetRecommendedWidgetsResponse, error) {
	var output GetRecommendedWidgetsResponse
	err := this.C.Call("com.amazon.adg.discovery.model", "getRecommendedWidgets", input, &output)
	return output, err
}

//Search a given browse node for matching products.
func (this *ADGDiscoveryServiceClient) Search(input SearchRequest) (SearchResponse, error) {
	var output SearchResponse
	err := this.C.Call("com.amazon.adg.discovery.model", "search", input, &output)
	return output, err
}

//Retrieves the product details for a given product, including store specific information
//(e.g. app details for apps, Skill command words for skills, etc).
func (this *ADGDiscoveryServiceClient) GetProductDetails(input GetProductDetailsRequest) (GetProductDetailsResponse, error) {
	var output GetProductDetailsResponse
	err := this.C.Call("com.amazon.adg.discovery.model", "getProductDetails", input, &output)
	return output, err
}

//Expands on one widget, providing more products than are returned in the getRecommendedWidgets call
func (this *ADGDiscoveryServiceClient) GetRecommendationsForWidget(input GetRecommendationsForWidgetRequest) (GetRecommendationsForWidgetResponse, error) {
	var output GetRecommendationsForWidgetResponse
	err := this.C.Call("com.amazon.adg.discovery.model", "getRecommendationsForWidget", input, &output)
	return output, err
}

//Creates a penName for a given customer
func (this *ADGDiscoveryServiceClient) CreateReviewPenName(input CreateReviewPenNameRequest) (CreateReviewPenNameResponse, error) {
	var output CreateReviewPenNameResponse
	err := this.C.Call("com.amazon.adg.discovery.model", "createReviewPenName", input, &output)
	return output, err
}

//Create a product review
func (this *ADGDiscoveryServiceClient) CreateProductReview(input CreateProductReviewRequest) (CreateProductReviewResponse, error) {
	var output CreateProductReviewResponse
	err := this.C.Call("com.amazon.adg.discovery.model", "createProductReview", input, &output)
	return output, err
}

//Allows a customer to provide feedback on a review.
func (this *ADGDiscoveryServiceClient) CastVote(input CastVoteRequest) (CastVoteResponse, error) {
	var output CastVoteResponse
	err := this.C.Call("com.amazon.adg.discovery.model", "castVote", input, &output)
	return output, err
}
