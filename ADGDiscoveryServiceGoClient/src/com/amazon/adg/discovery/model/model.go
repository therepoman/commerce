package model

import (
	model "code.justin.tv/commerce/ADGDiscoveryServiceGoClient/src/com/amazon/adg/common/model"
	__model__ "code.justin.tv/commerce/CoralGoModel/src/coral/model"
	__big__ "math/big"
	__reflect__ "reflect"
	__time__ "time"
)

func init() {
	var val *bool
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *bool
		if f, ok := from.Interface().(*bool); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to Boolean")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *bool
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *bool
		if f, ok := from.Interface().(*bool); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to BooleanObject")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *__time__.Time
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *__time__.Time
		if f, ok := from.Interface().(*__time__.Time); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to Timestamp")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *float64
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *float64
		if f, ok := from.Interface().(*float64); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to Double")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *float64
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *float64
		if f, ok := from.Interface().(*float64); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to DoubleObject")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *int32
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *int32
		if f, ok := from.Interface().(*int32); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to Integer")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *int64
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *int64
		if f, ok := from.Interface().(*int64); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to Long")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to CategoryIdType")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to LocalizedStringType")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to ReviewEligibilityStatus")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to DeviceDescriptorId")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to DeviceType")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to String")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to SearchSortOrder")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}

//An object that contains the description of a widget.
//value: the default string representation to use
//type: tells us what the value represents (an LMS string id, a literal string, or an Asin)
//substitutes: an optional list of regionalized variants that should be used instead of the 'value' under certain conditions
type LocalizedString interface {
	__type()
	SetSubstitutes(v []LocalizedStringSubst)
	Substitutes() []LocalizedStringSubst
	SetValue(v *string)
	Value() *string
	SetType(v *string)
	Type() *string
}
type _LocalizedString struct {
	Ị_value       *string                `coral:"value" json:"value"`
	Ị_type        *string                `coral:"type" json:"type"`
	Ị_substitutes []LocalizedStringSubst `coral:"substitutes" json:"substitutes"`
}

func (this *_LocalizedString) Value() *string {
	return this.Ị_value
}
func (this *_LocalizedString) SetValue(v *string) {
	this.Ị_value = v
}
func (this *_LocalizedString) Type() *string {
	return this.Ị_type
}
func (this *_LocalizedString) SetType(v *string) {
	this.Ị_type = v
}
func (this *_LocalizedString) Substitutes() []LocalizedStringSubst {
	return this.Ị_substitutes
}
func (this *_LocalizedString) SetSubstitutes(v []LocalizedStringSubst) {
	this.Ị_substitutes = v
}
func (this *_LocalizedString) __type() {
}
func NewLocalizedString() LocalizedString {
	return &_LocalizedString{}
}
func init() {
	var val LocalizedString
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.discovery.model").RegisterShape("LocalizedString", t, func() interface{} {
		return NewLocalizedString()
	})
}

type ProductReview interface {
	__type()
	SetAuthor(v ReviewAuthor)
	Author() ReviewAuthor
	SetTitle(v *string)
	Title() *string
	SetVotes(v ReviewVotes)
	Votes() ReviewVotes
	SetTags(v map[string]*string)
	Tags() map[string]*string
	SetMarketplaceId(v *string)
	MarketplaceId() *string
	SetOverallRating(v *float64)
	OverallRating() *float64
	SetStatus(v *string)
	Status() *string
	SetVersion(v *int64)
	Version() *int64
	SetSubmissionDateMs(v *int64)
	SubmissionDateMs() *int64
	SetText(v *string)
	Text() *string
	SetWrittenInLanguageCode(v *string)
	WrittenInLanguageCode() *string
	SetIsSyndicatedReview(v *bool)
	IsSyndicatedReview() *bool
	SetReviewId(v *string)
	ReviewId() *string
	SetRatings(v []ReviewRating)
	Ratings() []ReviewRating
	SetIsGreenlighted(v *bool)
	IsGreenlighted() *bool
	SetVerifiedPurchase(v *bool)
	VerifiedPurchase() *bool
}
type _ProductReview struct {
	Ị_marketplaceId         *string            `coral:"marketplaceId" json:"marketplaceId"`
	Ị_overallRating         *float64           `coral:"overallRating" json:"overallRating"`
	Ị_status                *string            `coral:"status" json:"status"`
	Ị_version               *int64             `coral:"version" json:"version"`
	Ị_votes                 ReviewVotes        `coral:"votes" json:"votes"`
	Ị_tags                  map[string]*string `coral:"tags" json:"tags"`
	Ị_submissionDateMs      *int64             `coral:"submissionDateMs" json:"submissionDateMs"`
	Ị_text                  *string            `coral:"text" json:"text"`
	Ị_writtenInLanguageCode *string            `coral:"writtenInLanguageCode" json:"writtenInLanguageCode"`
	Ị_isSyndicatedReview    *bool              `coral:"isSyndicatedReview" json:"isSyndicatedReview"`
	Ị_reviewId              *string            `coral:"reviewId" json:"reviewId"`
	Ị_ratings               []ReviewRating     `coral:"ratings" json:"ratings"`
	Ị_isGreenlighted        *bool              `coral:"isGreenlighted" json:"isGreenlighted"`
	Ị_verifiedPurchase      *bool              `coral:"verifiedPurchase" json:"verifiedPurchase"`
	Ị_author                ReviewAuthor       `coral:"author" json:"author"`
	Ị_title                 *string            `coral:"title" json:"title"`
}

func (this *_ProductReview) Version() *int64 {
	return this.Ị_version
}
func (this *_ProductReview) SetVersion(v *int64) {
	this.Ị_version = v
}
func (this *_ProductReview) Votes() ReviewVotes {
	return this.Ị_votes
}
func (this *_ProductReview) SetVotes(v ReviewVotes) {
	this.Ị_votes = v
}
func (this *_ProductReview) Tags() map[string]*string {
	return this.Ị_tags
}
func (this *_ProductReview) SetTags(v map[string]*string) {
	this.Ị_tags = v
}
func (this *_ProductReview) MarketplaceId() *string {
	return this.Ị_marketplaceId
}
func (this *_ProductReview) SetMarketplaceId(v *string) {
	this.Ị_marketplaceId = v
}
func (this *_ProductReview) OverallRating() *float64 {
	return this.Ị_overallRating
}
func (this *_ProductReview) SetOverallRating(v *float64) {
	this.Ị_overallRating = v
}
func (this *_ProductReview) Status() *string {
	return this.Ị_status
}
func (this *_ProductReview) SetStatus(v *string) {
	this.Ị_status = v
}
func (this *_ProductReview) IsSyndicatedReview() *bool {
	return this.Ị_isSyndicatedReview
}
func (this *_ProductReview) SetIsSyndicatedReview(v *bool) {
	this.Ị_isSyndicatedReview = v
}
func (this *_ProductReview) SubmissionDateMs() *int64 {
	return this.Ị_submissionDateMs
}
func (this *_ProductReview) SetSubmissionDateMs(v *int64) {
	this.Ị_submissionDateMs = v
}
func (this *_ProductReview) Text() *string {
	return this.Ị_text
}
func (this *_ProductReview) SetText(v *string) {
	this.Ị_text = v
}
func (this *_ProductReview) WrittenInLanguageCode() *string {
	return this.Ị_writtenInLanguageCode
}
func (this *_ProductReview) SetWrittenInLanguageCode(v *string) {
	this.Ị_writtenInLanguageCode = v
}
func (this *_ProductReview) VerifiedPurchase() *bool {
	return this.Ị_verifiedPurchase
}
func (this *_ProductReview) SetVerifiedPurchase(v *bool) {
	this.Ị_verifiedPurchase = v
}
func (this *_ProductReview) ReviewId() *string {
	return this.Ị_reviewId
}
func (this *_ProductReview) SetReviewId(v *string) {
	this.Ị_reviewId = v
}
func (this *_ProductReview) Ratings() []ReviewRating {
	return this.Ị_ratings
}
func (this *_ProductReview) SetRatings(v []ReviewRating) {
	this.Ị_ratings = v
}
func (this *_ProductReview) IsGreenlighted() *bool {
	return this.Ị_isGreenlighted
}
func (this *_ProductReview) SetIsGreenlighted(v *bool) {
	this.Ị_isGreenlighted = v
}
func (this *_ProductReview) Author() ReviewAuthor {
	return this.Ị_author
}
func (this *_ProductReview) SetAuthor(v ReviewAuthor) {
	this.Ị_author = v
}
func (this *_ProductReview) Title() *string {
	return this.Ị_title
}
func (this *_ProductReview) SetTitle(v *string) {
	this.Ị_title = v
}
func (this *_ProductReview) __type() {
}
func NewProductReview() ProductReview {
	return &_ProductReview{}
}
func init() {
	var val ProductReview
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.discovery.model").RegisterShape("ProductReview", t, func() interface{} {
		return NewProductReview()
	})
}

type GetProductListDetailsRequest interface {
	__type()
	SetClient(v model.ClientInfo)
	Client() model.ClientInfo
	SetCustomer(v model.AmazonCustomerInfo)
	Customer() model.AmazonCustomerInfo
	SetPreferredLocale(v model.CustomerLocalePrefs)
	PreferredLocale() model.CustomerLocalePrefs
	SetLanguage(v *string)
	Language() *string
	SetProducts(v []model.Product)
	Products() []model.Product
	SetDocketId(v *string)
	DocketId() *string
}
type _GetProductListDetailsRequest struct {
	Ị_customer        model.AmazonCustomerInfo  `coral:"customer" json:"customer"`
	Ị_preferredLocale model.CustomerLocalePrefs `coral:"preferredLocale" json:"preferredLocale"`
	Ị_language        *string                   `coral:"language" json:"language"`
	Ị_client          model.ClientInfo          `coral:"client" json:"client"`
	Ị_products        []model.Product           `coral:"products" json:"products"`
	Ị_docketId        *string                   `coral:"docketId" json:"docketId"`
}

func (this *_GetProductListDetailsRequest) Language() *string {
	return this.Ị_language
}
func (this *_GetProductListDetailsRequest) SetLanguage(v *string) {
	this.Ị_language = v
}
func (this *_GetProductListDetailsRequest) Client() model.ClientInfo {
	return this.Ị_client
}
func (this *_GetProductListDetailsRequest) SetClient(v model.ClientInfo) {
	this.Ị_client = v
}
func (this *_GetProductListDetailsRequest) Customer() model.AmazonCustomerInfo {
	return this.Ị_customer
}
func (this *_GetProductListDetailsRequest) SetCustomer(v model.AmazonCustomerInfo) {
	this.Ị_customer = v
}
func (this *_GetProductListDetailsRequest) PreferredLocale() model.CustomerLocalePrefs {
	return this.Ị_preferredLocale
}
func (this *_GetProductListDetailsRequest) SetPreferredLocale(v model.CustomerLocalePrefs) {
	this.Ị_preferredLocale = v
}
func (this *_GetProductListDetailsRequest) Products() []model.Product {
	return this.Ị_products
}
func (this *_GetProductListDetailsRequest) SetProducts(v []model.Product) {
	this.Ị_products = v
}
func (this *_GetProductListDetailsRequest) DocketId() *string {
	return this.Ị_docketId
}
func (this *_GetProductListDetailsRequest) SetDocketId(v *string) {
	this.Ị_docketId = v
}
func (this *_GetProductListDetailsRequest) __type() {
}
func NewGetProductListDetailsRequest() GetProductListDetailsRequest {
	return &_GetProductListDetailsRequest{}
}
func init() {
	var val GetProductListDetailsRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.discovery.model").RegisterShape("GetProductListDetailsRequest", t, func() interface{} {
		return NewGetProductListDetailsRequest()
	})
}

//Retrieves multiple paged responses of all the products for the specified domains.
//docketId - the docket used to hydrate the request.
//productsInDomainRequest - Requests to specify the domain and paging parameters that products should be fetched from.
//traversal - The traversal to use to find the domains.  See traversal for more details.
//productFilter - The ProductFilter to be applied to the returned objects.  Products that do not pass this filter will not be returned.
type GetProductsInDomainsRequest interface {
	__type()
	SetClient(v model.ClientInfo)
	Client() model.ClientInfo
	SetCustomer(v model.AmazonCustomerInfo)
	Customer() model.AmazonCustomerInfo
	SetPreferredLocale(v model.CustomerLocalePrefs)
	PreferredLocale() model.CustomerLocalePrefs
	SetLanguage(v *string)
	Language() *string
	SetDocketId(v *string)
	DocketId() *string
	SetProductsInDomainRequests(v []ProductsInDomainRequest)
	ProductsInDomainRequests() []ProductsInDomainRequest
	SetTraversal(v *string)
	Traversal() *string
	SetProductFilter(v model.ProductFilter)
	ProductFilter() model.ProductFilter
}
type _GetProductsInDomainsRequest struct {
	Ị_client                   model.ClientInfo          `coral:"client" json:"client"`
	Ị_customer                 model.AmazonCustomerInfo  `coral:"customer" json:"customer"`
	Ị_preferredLocale          model.CustomerLocalePrefs `coral:"preferredLocale" json:"preferredLocale"`
	Ị_language                 *string                   `coral:"language" json:"language"`
	Ị_docketId                 *string                   `coral:"docketId" json:"docketId"`
	Ị_productsInDomainRequests []ProductsInDomainRequest `coral:"productsInDomainRequests" json:"productsInDomainRequests"`
	Ị_traversal                *string                   `coral:"traversal" json:"traversal"`
	Ị_productFilter            model.ProductFilter       `coral:"productFilter" json:"productFilter"`
}

func (this *_GetProductsInDomainsRequest) Customer() model.AmazonCustomerInfo {
	return this.Ị_customer
}
func (this *_GetProductsInDomainsRequest) SetCustomer(v model.AmazonCustomerInfo) {
	this.Ị_customer = v
}
func (this *_GetProductsInDomainsRequest) PreferredLocale() model.CustomerLocalePrefs {
	return this.Ị_preferredLocale
}
func (this *_GetProductsInDomainsRequest) SetPreferredLocale(v model.CustomerLocalePrefs) {
	this.Ị_preferredLocale = v
}
func (this *_GetProductsInDomainsRequest) Language() *string {
	return this.Ị_language
}
func (this *_GetProductsInDomainsRequest) SetLanguage(v *string) {
	this.Ị_language = v
}
func (this *_GetProductsInDomainsRequest) Client() model.ClientInfo {
	return this.Ị_client
}
func (this *_GetProductsInDomainsRequest) SetClient(v model.ClientInfo) {
	this.Ị_client = v
}
func (this *_GetProductsInDomainsRequest) Traversal() *string {
	return this.Ị_traversal
}
func (this *_GetProductsInDomainsRequest) SetTraversal(v *string) {
	this.Ị_traversal = v
}
func (this *_GetProductsInDomainsRequest) ProductFilter() model.ProductFilter {
	return this.Ị_productFilter
}
func (this *_GetProductsInDomainsRequest) SetProductFilter(v model.ProductFilter) {
	this.Ị_productFilter = v
}
func (this *_GetProductsInDomainsRequest) DocketId() *string {
	return this.Ị_docketId
}
func (this *_GetProductsInDomainsRequest) SetDocketId(v *string) {
	this.Ị_docketId = v
}
func (this *_GetProductsInDomainsRequest) ProductsInDomainRequests() []ProductsInDomainRequest {
	return this.Ị_productsInDomainRequests
}
func (this *_GetProductsInDomainsRequest) SetProductsInDomainRequests(v []ProductsInDomainRequest) {
	this.Ị_productsInDomainRequests = v
}
func (this *_GetProductsInDomainsRequest) __type() {
}
func NewGetProductsInDomainsRequest() GetProductsInDomainsRequest {
	return &_GetProductsInDomainsRequest{}
}
func init() {
	var val GetProductsInDomainsRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.discovery.model").RegisterShape("GetProductsInDomainsRequest", t, func() interface{} {
		return NewGetProductsInDomainsRequest()
	})
}

type ServiceException interface {
	__type()
	error
	SetMessage(v *string)
	Message() *string
}
type _ServiceException struct {
	Ị_message *string `coral:"message" json:"message"`
}

func (this *_ServiceException) Error() string {
	return __model__.ErrorMessage(this)
}
func (this *_ServiceException) Message() *string {
	return this.Ị_message
}
func (this *_ServiceException) SetMessage(v *string) {
	this.Ị_message = v
}
func (this *_ServiceException) __type() {
}
func NewServiceException() ServiceException {
	return &_ServiceException{}
}
func init() {
	var val ServiceException
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.discovery.model").RegisterShape("ServiceException", t, func() interface{} {
		return NewServiceException()
	})
}

//A search refinement value is a specific value that can be applied for a refinement.
//Ex: a price refinement might have a $0.99-$1.99 search refinement value
type SearchRefinementValue interface {
	__type()
	SetDisplayName(v *string)
	DisplayName() *string
	SetName(v *string)
	Name() *string
	SetValue(v *string)
	Value() *string
}
type _SearchRefinementValue struct {
	Ị_displayName *string `coral:"displayName" json:"displayName"`
	Ị_name        *string `coral:"name" json:"name"`
	Ị_value       *string `coral:"value" json:"value"`
}

func (this *_SearchRefinementValue) Name() *string {
	return this.Ị_name
}
func (this *_SearchRefinementValue) SetName(v *string) {
	this.Ị_name = v
}
func (this *_SearchRefinementValue) Value() *string {
	return this.Ị_value
}
func (this *_SearchRefinementValue) SetValue(v *string) {
	this.Ị_value = v
}
func (this *_SearchRefinementValue) DisplayName() *string {
	return this.Ị_displayName
}
func (this *_SearchRefinementValue) SetDisplayName(v *string) {
	this.Ị_displayName = v
}
func (this *_SearchRefinementValue) __type() {
}
func NewSearchRefinementValue() SearchRefinementValue {
	return &_SearchRefinementValue{}
}
func init() {
	var val SearchRefinementValue
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.discovery.model").RegisterShape("SearchRefinementValue", t, func() interface{} {
		return NewSearchRefinementValue()
	})
}

type SearchInfo interface {
	__type()
	SetQueryId(v *string)
	QueryId() *string
	SetSuggestion(v *string)
	Suggestion() *string
	SetSearchAlias(v *__big__.Rat)
	SearchAlias() *__big__.Rat
}
type _SearchInfo struct {
	Ị_queryId     *string      `coral:"queryId" json:"queryId"`
	Ị_suggestion  *string      `coral:"suggestion" json:"suggestion"`
	Ị_searchAlias *__big__.Rat `coral:"searchAlias" json:"searchAlias"`
}

func (this *_SearchInfo) QueryId() *string {
	return this.Ị_queryId
}
func (this *_SearchInfo) SetQueryId(v *string) {
	this.Ị_queryId = v
}
func (this *_SearchInfo) Suggestion() *string {
	return this.Ị_suggestion
}
func (this *_SearchInfo) SetSuggestion(v *string) {
	this.Ị_suggestion = v
}
func (this *_SearchInfo) SearchAlias() *__big__.Rat {
	return this.Ị_searchAlias
}
func (this *_SearchInfo) SetSearchAlias(v *__big__.Rat) {
	this.Ị_searchAlias = v
}
func (this *_SearchInfo) __type() {
}
func NewSearchInfo() SearchInfo {
	return &_SearchInfo{}
}
func init() {
	var val SearchInfo
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.discovery.model").RegisterShape("SearchInfo", t, func() interface{} {
		return NewSearchInfo()
	})
}

type SearchQueryMetadata interface {
	__type()
	SetRefinementValues(v []SearchRefinementValue)
	RefinementValues() []SearchRefinementValue
	SetRefMarker(v *string)
	RefMarker() *string
	SetSprefix(v *string)
	Sprefix() *string
	SetRestrictions(v []model.KeyValue)
	Restrictions() []model.KeyValue
	SetBrowseId(v *int64)
	BrowseId() *int64
	SetSort(v *string)
	Sort() *string
}
type _SearchQueryMetadata struct {
	Ị_sprefix          *string                 `coral:"sprefix" json:"sprefix"`
	Ị_restrictions     []model.KeyValue        `coral:"restrictions" json:"restrictions"`
	Ị_browseId         *int64                  `coral:"browseId" json:"browseId"`
	Ị_sort             *string                 `coral:"sort" json:"sort"`
	Ị_refinementValues []SearchRefinementValue `coral:"refinementValues" json:"refinementValues"`
	Ị_refMarker        *string                 `coral:"refMarker" json:"refMarker"`
}

func (this *_SearchQueryMetadata) BrowseId() *int64 {
	return this.Ị_browseId
}
func (this *_SearchQueryMetadata) SetBrowseId(v *int64) {
	this.Ị_browseId = v
}
func (this *_SearchQueryMetadata) Sort() *string {
	return this.Ị_sort
}
func (this *_SearchQueryMetadata) SetSort(v *string) {
	this.Ị_sort = v
}
func (this *_SearchQueryMetadata) RefinementValues() []SearchRefinementValue {
	return this.Ị_refinementValues
}
func (this *_SearchQueryMetadata) SetRefinementValues(v []SearchRefinementValue) {
	this.Ị_refinementValues = v
}
func (this *_SearchQueryMetadata) RefMarker() *string {
	return this.Ị_refMarker
}
func (this *_SearchQueryMetadata) SetRefMarker(v *string) {
	this.Ị_refMarker = v
}
func (this *_SearchQueryMetadata) Sprefix() *string {
	return this.Ị_sprefix
}
func (this *_SearchQueryMetadata) SetSprefix(v *string) {
	this.Ị_sprefix = v
}
func (this *_SearchQueryMetadata) Restrictions() []model.KeyValue {
	return this.Ị_restrictions
}
func (this *_SearchQueryMetadata) SetRestrictions(v []model.KeyValue) {
	this.Ị_restrictions = v
}
func (this *_SearchQueryMetadata) __type() {
}
func NewSearchQueryMetadata() SearchQueryMetadata {
	return &_SearchQueryMetadata{}
}
func init() {
	var val SearchQueryMetadata
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.discovery.model").RegisterShape("SearchQueryMetadata", t, func() interface{} {
		return NewSearchQueryMetadata()
	})
}

type SearchResult interface {
	__type()
	SetProduct(v model.Product)
	Product() model.Product
	SetRelevance(v *int64)
	Relevance() *int64
}
type _SearchResult struct {
	Ị_relevance *int64        `coral:"relevance" json:"relevance"`
	Ị_product   model.Product `coral:"product" json:"product"`
}

func (this *_SearchResult) Relevance() *int64 {
	return this.Ị_relevance
}
func (this *_SearchResult) SetRelevance(v *int64) {
	this.Ị_relevance = v
}
func (this *_SearchResult) Product() model.Product {
	return this.Ị_product
}
func (this *_SearchResult) SetProduct(v model.Product) {
	this.Ị_product = v
}
func (this *_SearchResult) __type() {
}
func NewSearchResult() SearchResult {
	return &_SearchResult{}
}
func init() {
	var val SearchResult
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.discovery.model").RegisterShape("SearchResult", t, func() interface{} {
		return NewSearchResult()
	})
}

type ReviewVotes interface {
	__type()
	SetHelpfulVotes(v *int32)
	HelpfulVotes() *int32
	SetTotalVotes(v *int32)
	TotalVotes() *int32
}
type _ReviewVotes struct {
	Ị_helpfulVotes *int32 `coral:"helpfulVotes" json:"helpfulVotes"`
	Ị_totalVotes   *int32 `coral:"totalVotes" json:"totalVotes"`
}

func (this *_ReviewVotes) HelpfulVotes() *int32 {
	return this.Ị_helpfulVotes
}
func (this *_ReviewVotes) SetHelpfulVotes(v *int32) {
	this.Ị_helpfulVotes = v
}
func (this *_ReviewVotes) TotalVotes() *int32 {
	return this.Ị_totalVotes
}
func (this *_ReviewVotes) SetTotalVotes(v *int32) {
	this.Ị_totalVotes = v
}
func (this *_ReviewVotes) __type() {
}
func NewReviewVotes() ReviewVotes {
	return &_ReviewVotes{}
}
func init() {
	var val ReviewVotes
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.discovery.model").RegisterShape("ReviewVotes", t, func() interface{} {
		return NewReviewVotes()
	})
}

type SearchRequest interface {
	__type()
	SetClient(v model.ClientInfo)
	Client() model.ClientInfo
	SetCustomer(v model.AmazonCustomerInfo)
	Customer() model.AmazonCustomerInfo
	SetPreferredLocale(v model.CustomerLocalePrefs)
	PreferredLocale() model.CustomerLocalePrefs
	SetLanguage(v *string)
	Language() *string
	SetDeviceInfo(v DeviceInfo)
	DeviceInfo() DeviceInfo
	SetSearchText(v *string)
	SearchText() *string
	SetDocketId(v *string)
	DocketId() *string
	SetIncludeRefinements(v *bool)
	IncludeRefinements() *bool
	SetPagedRequest(v model.PagedRequestParameters)
	PagedRequest() model.PagedRequestParameters
	SetRemoteSearchParameters(v SearchQueryMetadata)
	RemoteSearchParameters() SearchQueryMetadata
}
type _SearchRequest struct {
	Ị_client                 model.ClientInfo             `coral:"client" json:"client"`
	Ị_customer               model.AmazonCustomerInfo     `coral:"customer" json:"customer"`
	Ị_preferredLocale        model.CustomerLocalePrefs    `coral:"preferredLocale" json:"preferredLocale"`
	Ị_language               *string                      `coral:"language" json:"language"`
	Ị_deviceInfo             DeviceInfo                   `coral:"deviceInfo" json:"deviceInfo"`
	Ị_searchText             *string                      `coral:"searchText" json:"searchText"`
	Ị_docketId               *string                      `coral:"docketId" json:"docketId"`
	Ị_includeRefinements     *bool                        `coral:"includeRefinements" json:"includeRefinements"`
	Ị_pagedRequest           model.PagedRequestParameters `coral:"pagedRequest" json:"pagedRequest"`
	Ị_remoteSearchParameters SearchQueryMetadata          `coral:"remoteSearchParameters" json:"remoteSearchParameters"`
}

func (this *_SearchRequest) Language() *string {
	return this.Ị_language
}
func (this *_SearchRequest) SetLanguage(v *string) {
	this.Ị_language = v
}
func (this *_SearchRequest) Client() model.ClientInfo {
	return this.Ị_client
}
func (this *_SearchRequest) SetClient(v model.ClientInfo) {
	this.Ị_client = v
}
func (this *_SearchRequest) Customer() model.AmazonCustomerInfo {
	return this.Ị_customer
}
func (this *_SearchRequest) SetCustomer(v model.AmazonCustomerInfo) {
	this.Ị_customer = v
}
func (this *_SearchRequest) PreferredLocale() model.CustomerLocalePrefs {
	return this.Ị_preferredLocale
}
func (this *_SearchRequest) SetPreferredLocale(v model.CustomerLocalePrefs) {
	this.Ị_preferredLocale = v
}
func (this *_SearchRequest) DocketId() *string {
	return this.Ị_docketId
}
func (this *_SearchRequest) SetDocketId(v *string) {
	this.Ị_docketId = v
}
func (this *_SearchRequest) IncludeRefinements() *bool {
	return this.Ị_includeRefinements
}
func (this *_SearchRequest) SetIncludeRefinements(v *bool) {
	this.Ị_includeRefinements = v
}
func (this *_SearchRequest) PagedRequest() model.PagedRequestParameters {
	return this.Ị_pagedRequest
}
func (this *_SearchRequest) SetPagedRequest(v model.PagedRequestParameters) {
	this.Ị_pagedRequest = v
}
func (this *_SearchRequest) RemoteSearchParameters() SearchQueryMetadata {
	return this.Ị_remoteSearchParameters
}
func (this *_SearchRequest) SetRemoteSearchParameters(v SearchQueryMetadata) {
	this.Ị_remoteSearchParameters = v
}
func (this *_SearchRequest) DeviceInfo() DeviceInfo {
	return this.Ị_deviceInfo
}
func (this *_SearchRequest) SetDeviceInfo(v DeviceInfo) {
	this.Ị_deviceInfo = v
}
func (this *_SearchRequest) SearchText() *string {
	return this.Ị_searchText
}
func (this *_SearchRequest) SetSearchText(v *string) {
	this.Ị_searchText = v
}
func (this *_SearchRequest) __type() {
}
func NewSearchRequest() SearchRequest {
	return &_SearchRequest{}
}
func init() {
	var val SearchRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.discovery.model").RegisterShape("SearchRequest", t, func() interface{} {
		return NewSearchRequest()
	})
}

type GetProductListDetailsResponse interface {
	__type()
	SetProducts(v []model.Product)
	Products() []model.Product
}
type _GetProductListDetailsResponse struct {
	Ị_products []model.Product `coral:"products" json:"products"`
}

func (this *_GetProductListDetailsResponse) Products() []model.Product {
	return this.Ị_products
}
func (this *_GetProductListDetailsResponse) SetProducts(v []model.Product) {
	this.Ị_products = v
}
func (this *_GetProductListDetailsResponse) __type() {
}
func NewGetProductListDetailsResponse() GetProductListDetailsResponse {
	return &_GetProductListDetailsResponse{}
}
func init() {
	var val GetProductListDetailsResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.discovery.model").RegisterShape("GetProductListDetailsResponse", t, func() interface{} {
		return NewGetProductListDetailsResponse()
	})
}

type ValidateDocketRequest interface {
	__type()
	SetDocketId(v *string)
	DocketId() *string
}
type _ValidateDocketRequest struct {
	Ị_docketId *string `coral:"docketId" json:"docketId"`
}

func (this *_ValidateDocketRequest) DocketId() *string {
	return this.Ị_docketId
}
func (this *_ValidateDocketRequest) SetDocketId(v *string) {
	this.Ị_docketId = v
}
func (this *_ValidateDocketRequest) __type() {
}
func NewValidateDocketRequest() ValidateDocketRequest {
	return &_ValidateDocketRequest{}
}
func init() {
	var val ValidateDocketRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.discovery.model").RegisterShape("ValidateDocketRequest", t, func() interface{} {
		return NewValidateDocketRequest()
	})
}

//This exception is thrown on another service or resource this service depends on has an error
type DependencyException interface {
	__type()
	error
	SetMessage(v *string)
	Message() *string
}
type _DependencyException struct {
	ServiceException
	Ị_message *string `coral:"message" json:"message"`
}

func (this *_DependencyException) Error() string {
	return __model__.ErrorMessage(this)
}
func (this *_DependencyException) Message() *string {
	return this.Ị_message
}
func (this *_DependencyException) SetMessage(v *string) {
	this.Ị_message = v
}
func (this *_DependencyException) __type() {
}
func NewDependencyException() DependencyException {
	return &_DependencyException{}
}
func init() {
	var val DependencyException
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.discovery.model").RegisterShape("DependencyException", t, func() interface{} {
		return NewDependencyException()
	})
}

//A search refinement defines a category which the results can be refined on. Ex: price
type SearchRefinement interface {
	__type()
	SetDisplayName(v *string)
	DisplayName() *string
	SetId(v *string)
	Id() *string
	SetValues(v []SearchRefinementValue)
	Values() []SearchRefinementValue
}
type _SearchRefinement struct {
	Ị_displayName *string                 `coral:"displayName" json:"displayName"`
	Ị_id          *string                 `coral:"id" json:"id"`
	Ị_values      []SearchRefinementValue `coral:"values" json:"values"`
}

func (this *_SearchRefinement) Id() *string {
	return this.Ị_id
}
func (this *_SearchRefinement) SetId(v *string) {
	this.Ị_id = v
}
func (this *_SearchRefinement) Values() []SearchRefinementValue {
	return this.Ị_values
}
func (this *_SearchRefinement) SetValues(v []SearchRefinementValue) {
	this.Ị_values = v
}
func (this *_SearchRefinement) DisplayName() *string {
	return this.Ị_displayName
}
func (this *_SearchRefinement) SetDisplayName(v *string) {
	this.Ị_displayName = v
}
func (this *_SearchRefinement) __type() {
}
func NewSearchRefinement() SearchRefinement {
	return &_SearchRefinement{}
}
func init() {
	var val SearchRefinement
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.discovery.model").RegisterShape("SearchRefinement", t, func() interface{} {
		return NewSearchRefinement()
	})
}

type GetProductReviewsRequest interface {
	__type()
	SetClient(v model.ClientInfo)
	Client() model.ClientInfo
	SetCustomer(v model.AmazonCustomerInfo)
	Customer() model.AmazonCustomerInfo
	SetPreferredLocale(v model.CustomerLocalePrefs)
	PreferredLocale() model.CustomerLocalePrefs
	SetLanguage(v *string)
	Language() *string
	SetIncludeSyndicatedReviews(v *bool)
	IncludeSyndicatedReviews() *bool
	SetPagedRequest(v model.PagedRequestParameters)
	PagedRequest() model.PagedRequestParameters
	SetProduct(v model.Product)
	Product() model.Product
	SetRelatedAttributes(v map[string]*string)
	RelatedAttributes() map[string]*string
	SetSortBy(v *string)
	SortBy() *string
}
type _GetProductReviewsRequest struct {
	Ị_client                   model.ClientInfo             `coral:"client" json:"client"`
	Ị_customer                 model.AmazonCustomerInfo     `coral:"customer" json:"customer"`
	Ị_preferredLocale          model.CustomerLocalePrefs    `coral:"preferredLocale" json:"preferredLocale"`
	Ị_language                 *string                      `coral:"language" json:"language"`
	Ị_includeSyndicatedReviews *bool                        `coral:"includeSyndicatedReviews" json:"includeSyndicatedReviews"`
	Ị_pagedRequest             model.PagedRequestParameters `coral:"pagedRequest" json:"pagedRequest"`
	Ị_product                  model.Product                `coral:"product" json:"product"`
	Ị_relatedAttributes        map[string]*string           `coral:"relatedAttributes" json:"relatedAttributes"`
	Ị_sortBy                   *string                      `coral:"sortBy" json:"sortBy"`
}

func (this *_GetProductReviewsRequest) Customer() model.AmazonCustomerInfo {
	return this.Ị_customer
}
func (this *_GetProductReviewsRequest) SetCustomer(v model.AmazonCustomerInfo) {
	this.Ị_customer = v
}
func (this *_GetProductReviewsRequest) PreferredLocale() model.CustomerLocalePrefs {
	return this.Ị_preferredLocale
}
func (this *_GetProductReviewsRequest) SetPreferredLocale(v model.CustomerLocalePrefs) {
	this.Ị_preferredLocale = v
}
func (this *_GetProductReviewsRequest) Language() *string {
	return this.Ị_language
}
func (this *_GetProductReviewsRequest) SetLanguage(v *string) {
	this.Ị_language = v
}
func (this *_GetProductReviewsRequest) Client() model.ClientInfo {
	return this.Ị_client
}
func (this *_GetProductReviewsRequest) SetClient(v model.ClientInfo) {
	this.Ị_client = v
}
func (this *_GetProductReviewsRequest) Product() model.Product {
	return this.Ị_product
}
func (this *_GetProductReviewsRequest) SetProduct(v model.Product) {
	this.Ị_product = v
}
func (this *_GetProductReviewsRequest) RelatedAttributes() map[string]*string {
	return this.Ị_relatedAttributes
}
func (this *_GetProductReviewsRequest) SetRelatedAttributes(v map[string]*string) {
	this.Ị_relatedAttributes = v
}
func (this *_GetProductReviewsRequest) SortBy() *string {
	return this.Ị_sortBy
}
func (this *_GetProductReviewsRequest) SetSortBy(v *string) {
	this.Ị_sortBy = v
}
func (this *_GetProductReviewsRequest) IncludeSyndicatedReviews() *bool {
	return this.Ị_includeSyndicatedReviews
}
func (this *_GetProductReviewsRequest) SetIncludeSyndicatedReviews(v *bool) {
	this.Ị_includeSyndicatedReviews = v
}
func (this *_GetProductReviewsRequest) PagedRequest() model.PagedRequestParameters {
	return this.Ị_pagedRequest
}
func (this *_GetProductReviewsRequest) SetPagedRequest(v model.PagedRequestParameters) {
	this.Ị_pagedRequest = v
}
func (this *_GetProductReviewsRequest) __type() {
}
func NewGetProductReviewsRequest() GetProductReviewsRequest {
	return &_GetProductReviewsRequest{}
}
func init() {
	var val GetProductReviewsRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.discovery.model").RegisterShape("GetProductReviewsRequest", t, func() interface{} {
		return NewGetProductReviewsRequest()
	})
}

type GetProductReviewsResponse interface {
	__type()
	SetSyndicatedReviewSummary(v ReviewSummary)
	SyndicatedReviewSummary() ReviewSummary
	SetReviews(v []ProductReview)
	Reviews() []ProductReview
	SetReviewSummary(v ReviewSummary)
	ReviewSummary() ReviewSummary
	SetSyndicatedReviews(v []ProductReview)
	SyndicatedReviews() []ProductReview
}
type _GetProductReviewsResponse struct {
	Ị_reviewSummary           ReviewSummary   `coral:"reviewSummary" json:"reviewSummary"`
	Ị_syndicatedReviews       []ProductReview `coral:"syndicatedReviews" json:"syndicatedReviews"`
	Ị_syndicatedReviewSummary ReviewSummary   `coral:"syndicatedReviewSummary" json:"syndicatedReviewSummary"`
	Ị_reviews                 []ProductReview `coral:"reviews" json:"reviews"`
}

func (this *_GetProductReviewsResponse) SyndicatedReviewSummary() ReviewSummary {
	return this.Ị_syndicatedReviewSummary
}
func (this *_GetProductReviewsResponse) SetSyndicatedReviewSummary(v ReviewSummary) {
	this.Ị_syndicatedReviewSummary = v
}
func (this *_GetProductReviewsResponse) Reviews() []ProductReview {
	return this.Ị_reviews
}
func (this *_GetProductReviewsResponse) SetReviews(v []ProductReview) {
	this.Ị_reviews = v
}
func (this *_GetProductReviewsResponse) ReviewSummary() ReviewSummary {
	return this.Ị_reviewSummary
}
func (this *_GetProductReviewsResponse) SetReviewSummary(v ReviewSummary) {
	this.Ị_reviewSummary = v
}
func (this *_GetProductReviewsResponse) SyndicatedReviews() []ProductReview {
	return this.Ị_syndicatedReviews
}
func (this *_GetProductReviewsResponse) SetSyndicatedReviews(v []ProductReview) {
	this.Ị_syndicatedReviews = v
}
func (this *_GetProductReviewsResponse) __type() {
}
func NewGetProductReviewsResponse() GetProductReviewsResponse {
	return &_GetProductReviewsResponse{}
}
func init() {
	var val GetProductReviewsResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.discovery.model").RegisterShape("GetProductReviewsResponse", t, func() interface{} {
		return NewGetProductReviewsResponse()
	})
}

type CreateProductReviewResponse interface {
	__type()
	SetReviewId(v *string)
	ReviewId() *string
}
type _CreateProductReviewResponse struct {
	Ị_reviewId *string `coral:"reviewId" json:"reviewId"`
}

func (this *_CreateProductReviewResponse) ReviewId() *string {
	return this.Ị_reviewId
}
func (this *_CreateProductReviewResponse) SetReviewId(v *string) {
	this.Ị_reviewId = v
}
func (this *_CreateProductReviewResponse) __type() {
}
func NewCreateProductReviewResponse() CreateProductReviewResponse {
	return &_CreateProductReviewResponse{}
}
func init() {
	var val CreateProductReviewResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.discovery.model").RegisterShape("CreateProductReviewResponse", t, func() interface{} {
		return NewCreateProductReviewResponse()
	})
}

//Exception for any hard errors caused by internal failures.
type InternalServiceException interface {
	__type()
	error
	SetMessage(v *string)
	Message() *string
}
type _InternalServiceException struct {
	ServiceException
	Ị_message *string `coral:"message" json:"message"`
}

func (this *_InternalServiceException) Error() string {
	return __model__.ErrorMessage(this)
}
func (this *_InternalServiceException) Message() *string {
	return this.Ị_message
}
func (this *_InternalServiceException) SetMessage(v *string) {
	this.Ị_message = v
}
func (this *_InternalServiceException) __type() {
}
func NewInternalServiceException() InternalServiceException {
	return &_InternalServiceException{}
}
func init() {
	var val InternalServiceException
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.discovery.model").RegisterShape("InternalServiceException", t, func() interface{} {
		return NewInternalServiceException()
	})
}

//Sample inputs as follows:
//voteDomain: "Reviews",
//voteDimension: "Helpful" || "Inappropriate",
//voteEntityName: "Review",
//voteInstanceId: "REVIEW_ID",
//voteValue: 1 || -1
//voteEntityName is defined officially here https://w.amazon.com/index.php/Community_Entity_ID
type CastVoteRequest interface {
	__type()
	SetPreferredLocale(v model.CustomerLocalePrefs)
	PreferredLocale() model.CustomerLocalePrefs
	SetLanguage(v *string)
	Language() *string
	SetClient(v model.ClientInfo)
	Client() model.ClientInfo
	SetCustomer(v model.AmazonCustomerInfo)
	Customer() model.AmazonCustomerInfo
	SetVoteInstanceId(v *string)
	VoteInstanceId() *string
	SetVoteValue(v *int32)
	VoteValue() *int32
	SetVoteDomain(v *string)
	VoteDomain() *string
	SetVoteDimension(v *string)
	VoteDimension() *string
	SetVoteEntityName(v *string)
	VoteEntityName() *string
}
type _CastVoteRequest struct {
	Ị_customer        model.AmazonCustomerInfo  `coral:"customer" json:"customer"`
	Ị_preferredLocale model.CustomerLocalePrefs `coral:"preferredLocale" json:"preferredLocale"`
	Ị_language        *string                   `coral:"language" json:"language"`
	Ị_client          model.ClientInfo          `coral:"client" json:"client"`
	Ị_voteDomain      *string                   `coral:"voteDomain" json:"voteDomain"`
	Ị_voteDimension   *string                   `coral:"voteDimension" json:"voteDimension"`
	Ị_voteEntityName  *string                   `coral:"voteEntityName" json:"voteEntityName"`
	Ị_voteInstanceId  *string                   `coral:"voteInstanceId" json:"voteInstanceId"`
	Ị_voteValue       *int32                    `coral:"voteValue" json:"voteValue"`
}

func (this *_CastVoteRequest) PreferredLocale() model.CustomerLocalePrefs {
	return this.Ị_preferredLocale
}
func (this *_CastVoteRequest) SetPreferredLocale(v model.CustomerLocalePrefs) {
	this.Ị_preferredLocale = v
}
func (this *_CastVoteRequest) Language() *string {
	return this.Ị_language
}
func (this *_CastVoteRequest) SetLanguage(v *string) {
	this.Ị_language = v
}
func (this *_CastVoteRequest) Client() model.ClientInfo {
	return this.Ị_client
}
func (this *_CastVoteRequest) SetClient(v model.ClientInfo) {
	this.Ị_client = v
}
func (this *_CastVoteRequest) Customer() model.AmazonCustomerInfo {
	return this.Ị_customer
}
func (this *_CastVoteRequest) SetCustomer(v model.AmazonCustomerInfo) {
	this.Ị_customer = v
}
func (this *_CastVoteRequest) VoteValue() *int32 {
	return this.Ị_voteValue
}
func (this *_CastVoteRequest) SetVoteValue(v *int32) {
	this.Ị_voteValue = v
}
func (this *_CastVoteRequest) VoteDomain() *string {
	return this.Ị_voteDomain
}
func (this *_CastVoteRequest) SetVoteDomain(v *string) {
	this.Ị_voteDomain = v
}
func (this *_CastVoteRequest) VoteDimension() *string {
	return this.Ị_voteDimension
}
func (this *_CastVoteRequest) SetVoteDimension(v *string) {
	this.Ị_voteDimension = v
}
func (this *_CastVoteRequest) VoteEntityName() *string {
	return this.Ị_voteEntityName
}
func (this *_CastVoteRequest) SetVoteEntityName(v *string) {
	this.Ị_voteEntityName = v
}
func (this *_CastVoteRequest) VoteInstanceId() *string {
	return this.Ị_voteInstanceId
}
func (this *_CastVoteRequest) SetVoteInstanceId(v *string) {
	this.Ị_voteInstanceId = v
}
func (this *_CastVoteRequest) __type() {
}
func NewCastVoteRequest() CastVoteRequest {
	return &_CastVoteRequest{}
}
func init() {
	var val CastVoteRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.discovery.model").RegisterShape("CastVoteRequest", t, func() interface{} {
		return NewCastVoteRequest()
	})
}

type SearchResultMetadata interface {
	__type()
	SetRefinements(v []SearchRefinement)
	Refinements() []SearchRefinement
	SetSearchInfo(v SearchInfo)
	SearchInfo() SearchInfo
}
type _SearchResultMetadata struct {
	Ị_refinements []SearchRefinement `coral:"refinements" json:"refinements"`
	Ị_searchInfo  SearchInfo         `coral:"searchInfo" json:"searchInfo"`
}

func (this *_SearchResultMetadata) Refinements() []SearchRefinement {
	return this.Ị_refinements
}
func (this *_SearchResultMetadata) SetRefinements(v []SearchRefinement) {
	this.Ị_refinements = v
}
func (this *_SearchResultMetadata) SearchInfo() SearchInfo {
	return this.Ị_searchInfo
}
func (this *_SearchResultMetadata) SetSearchInfo(v SearchInfo) {
	this.Ị_searchInfo = v
}
func (this *_SearchResultMetadata) __type() {
}
func NewSearchResultMetadata() SearchResultMetadata {
	return &_SearchResultMetadata{}
}
func init() {
	var val SearchResultMetadata
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.discovery.model").RegisterShape("SearchResultMetadata", t, func() interface{} {
		return NewSearchResultMetadata()
	})
}

type SearchResponse interface {
	__type()
	SetSearchResults(v []SearchResult)
	SearchResults() []SearchResult
	SetPagedResponse(v model.PagedResponseParameters)
	PagedResponse() model.PagedResponseParameters
	SetRemoteSearchMetadata(v SearchResultMetadata)
	RemoteSearchMetadata() SearchResultMetadata
}
type _SearchResponse struct {
	Ị_searchResults        []SearchResult                `coral:"searchResults" json:"searchResults"`
	Ị_pagedResponse        model.PagedResponseParameters `coral:"pagedResponse" json:"pagedResponse"`
	Ị_remoteSearchMetadata SearchResultMetadata          `coral:"remoteSearchMetadata" json:"remoteSearchMetadata"`
}

func (this *_SearchResponse) SearchResults() []SearchResult {
	return this.Ị_searchResults
}
func (this *_SearchResponse) SetSearchResults(v []SearchResult) {
	this.Ị_searchResults = v
}
func (this *_SearchResponse) PagedResponse() model.PagedResponseParameters {
	return this.Ị_pagedResponse
}
func (this *_SearchResponse) SetPagedResponse(v model.PagedResponseParameters) {
	this.Ị_pagedResponse = v
}
func (this *_SearchResponse) RemoteSearchMetadata() SearchResultMetadata {
	return this.Ị_remoteSearchMetadata
}
func (this *_SearchResponse) SetRemoteSearchMetadata(v SearchResultMetadata) {
	this.Ị_remoteSearchMetadata = v
}
func (this *_SearchResponse) __type() {
}
func NewSearchResponse() SearchResponse {
	return &_SearchResponse{}
}
func init() {
	var val SearchResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.discovery.model").RegisterShape("SearchResponse", t, func() interface{} {
		return NewSearchResponse()
	})
}

type GetProductDetailsResponse interface {
	__type()
	SetProduct(v model.Product)
	Product() model.Product
}
type _GetProductDetailsResponse struct {
	Ị_product model.Product `coral:"product" json:"product"`
}

func (this *_GetProductDetailsResponse) Product() model.Product {
	return this.Ị_product
}
func (this *_GetProductDetailsResponse) SetProduct(v model.Product) {
	this.Ị_product = v
}
func (this *_GetProductDetailsResponse) __type() {
}
func NewGetProductDetailsResponse() GetProductDetailsResponse {
	return &_GetProductDetailsResponse{}
}
func init() {
	var val GetProductDetailsResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.discovery.model").RegisterShape("GetProductDetailsResponse", t, func() interface{} {
		return NewGetProductDetailsResponse()
	})
}

//Contains a productsInDomainResponses that matched the given domains, traversal, and filter.
//See GetProductsInDomainResponse for more details.
type GetProductsInDomainsResponse interface {
	__type()
	SetProductsInDomainResponses(v []ProductsInDomainResponse)
	ProductsInDomainResponses() []ProductsInDomainResponse
}
type _GetProductsInDomainsResponse struct {
	Ị_productsInDomainResponses []ProductsInDomainResponse `coral:"productsInDomainResponses" json:"productsInDomainResponses"`
}

func (this *_GetProductsInDomainsResponse) ProductsInDomainResponses() []ProductsInDomainResponse {
	return this.Ị_productsInDomainResponses
}
func (this *_GetProductsInDomainsResponse) SetProductsInDomainResponses(v []ProductsInDomainResponse) {
	this.Ị_productsInDomainResponses = v
}
func (this *_GetProductsInDomainsResponse) __type() {
}
func NewGetProductsInDomainsResponse() GetProductsInDomainsResponse {
	return &_GetProductsInDomainsResponse{}
}
func init() {
	var val GetProductsInDomainsResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.discovery.model").RegisterShape("GetProductsInDomainsResponse", t, func() interface{} {
		return NewGetProductsInDomainsResponse()
	})
}

type CreateProductReviewRequest interface {
	__type()
	SetClient(v model.ClientInfo)
	Client() model.ClientInfo
	SetCustomer(v model.AmazonCustomerInfo)
	Customer() model.AmazonCustomerInfo
	SetPreferredLocale(v model.CustomerLocalePrefs)
	PreferredLocale() model.CustomerLocalePrefs
	SetLanguage(v *string)
	Language() *string
	SetWrittenInLanguageCode(v *string)
	WrittenInLanguageCode() *string
	SetNewReview(v ProductReview)
	NewReview() ProductReview
	SetAsin(v *string)
	Asin() *string
	SetAsinVersion(v *string)
	AsinVersion() *string
	SetRelatedAttributes(v map[string]*string)
	RelatedAttributes() map[string]*string
}
type _CreateProductReviewRequest struct {
	Ị_client                model.ClientInfo          `coral:"client" json:"client"`
	Ị_customer              model.AmazonCustomerInfo  `coral:"customer" json:"customer"`
	Ị_preferredLocale       model.CustomerLocalePrefs `coral:"preferredLocale" json:"preferredLocale"`
	Ị_language              *string                   `coral:"language" json:"language"`
	Ị_newReview             ProductReview             `coral:"newReview" json:"newReview"`
	Ị_asin                  *string                   `coral:"asin" json:"asin"`
	Ị_asinVersion           *string                   `coral:"asinVersion" json:"asinVersion"`
	Ị_relatedAttributes     map[string]*string        `coral:"relatedAttributes" json:"relatedAttributes"`
	Ị_writtenInLanguageCode *string                   `coral:"writtenInLanguageCode" json:"writtenInLanguageCode"`
}

func (this *_CreateProductReviewRequest) Client() model.ClientInfo {
	return this.Ị_client
}
func (this *_CreateProductReviewRequest) SetClient(v model.ClientInfo) {
	this.Ị_client = v
}
func (this *_CreateProductReviewRequest) Customer() model.AmazonCustomerInfo {
	return this.Ị_customer
}
func (this *_CreateProductReviewRequest) SetCustomer(v model.AmazonCustomerInfo) {
	this.Ị_customer = v
}
func (this *_CreateProductReviewRequest) PreferredLocale() model.CustomerLocalePrefs {
	return this.Ị_preferredLocale
}
func (this *_CreateProductReviewRequest) SetPreferredLocale(v model.CustomerLocalePrefs) {
	this.Ị_preferredLocale = v
}
func (this *_CreateProductReviewRequest) Language() *string {
	return this.Ị_language
}
func (this *_CreateProductReviewRequest) SetLanguage(v *string) {
	this.Ị_language = v
}
func (this *_CreateProductReviewRequest) Asin() *string {
	return this.Ị_asin
}
func (this *_CreateProductReviewRequest) SetAsin(v *string) {
	this.Ị_asin = v
}
func (this *_CreateProductReviewRequest) AsinVersion() *string {
	return this.Ị_asinVersion
}
func (this *_CreateProductReviewRequest) SetAsinVersion(v *string) {
	this.Ị_asinVersion = v
}
func (this *_CreateProductReviewRequest) RelatedAttributes() map[string]*string {
	return this.Ị_relatedAttributes
}
func (this *_CreateProductReviewRequest) SetRelatedAttributes(v map[string]*string) {
	this.Ị_relatedAttributes = v
}
func (this *_CreateProductReviewRequest) WrittenInLanguageCode() *string {
	return this.Ị_writtenInLanguageCode
}
func (this *_CreateProductReviewRequest) SetWrittenInLanguageCode(v *string) {
	this.Ị_writtenInLanguageCode = v
}
func (this *_CreateProductReviewRequest) NewReview() ProductReview {
	return this.Ị_newReview
}
func (this *_CreateProductReviewRequest) SetNewReview(v ProductReview) {
	this.Ị_newReview = v
}
func (this *_CreateProductReviewRequest) __type() {
}
func NewCreateProductReviewRequest() CreateProductReviewRequest {
	return &_CreateProductReviewRequest{}
}
func init() {
	var val CreateProductReviewRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.discovery.model").RegisterShape("CreateProductReviewRequest", t, func() interface{} {
		return NewCreateProductReviewRequest()
	})
}

type RecommendedCategory interface {
	__type()
	SetId(v *string)
	Id() *string
	SetType(v *string)
	Type() *string
	SetName(v LocalizedString)
	Name() LocalizedString
}
type _RecommendedCategory struct {
	Ị_id   *string         `coral:"id" json:"id"`
	Ị_type *string         `coral:"type" json:"type"`
	Ị_name LocalizedString `coral:"name" json:"name"`
}

func (this *_RecommendedCategory) Type() *string {
	return this.Ị_type
}
func (this *_RecommendedCategory) SetType(v *string) {
	this.Ị_type = v
}
func (this *_RecommendedCategory) Name() LocalizedString {
	return this.Ị_name
}
func (this *_RecommendedCategory) SetName(v LocalizedString) {
	this.Ị_name = v
}
func (this *_RecommendedCategory) Id() *string {
	return this.Ị_id
}
func (this *_RecommendedCategory) SetId(v *string) {
	this.Ị_id = v
}
func (this *_RecommendedCategory) __type() {
}
func NewRecommendedCategory() RecommendedCategory {
	return &_RecommendedCategory{}
}
func init() {
	var val RecommendedCategory
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.discovery.model").RegisterShape("RecommendedCategory", t, func() interface{} {
		return NewRecommendedCategory()
	})
}

type ReviewRating interface {
	__type()
	SetDimensionId(v *int64)
	DimensionId() *int64
	SetDimensionName(v *string)
	DimensionName() *string
	SetReviewId(v *string)
	ReviewId() *string
	SetValue(v *string)
	Value() *string
}
type _ReviewRating struct {
	Ị_dimensionId   *int64  `coral:"dimensionId" json:"dimensionId"`
	Ị_dimensionName *string `coral:"dimensionName" json:"dimensionName"`
	Ị_reviewId      *string `coral:"reviewId" json:"reviewId"`
	Ị_value         *string `coral:"value" json:"value"`
}

func (this *_ReviewRating) DimensionId() *int64 {
	return this.Ị_dimensionId
}
func (this *_ReviewRating) SetDimensionId(v *int64) {
	this.Ị_dimensionId = v
}
func (this *_ReviewRating) DimensionName() *string {
	return this.Ị_dimensionName
}
func (this *_ReviewRating) SetDimensionName(v *string) {
	this.Ị_dimensionName = v
}
func (this *_ReviewRating) ReviewId() *string {
	return this.Ị_reviewId
}
func (this *_ReviewRating) SetReviewId(v *string) {
	this.Ị_reviewId = v
}
func (this *_ReviewRating) Value() *string {
	return this.Ị_value
}
func (this *_ReviewRating) SetValue(v *string) {
	this.Ị_value = v
}
func (this *_ReviewRating) __type() {
}
func NewReviewRating() ReviewRating {
	return &_ReviewRating{}
}
func init() {
	var val ReviewRating
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.discovery.model").RegisterShape("ReviewRating", t, func() interface{} {
		return NewReviewRating()
	})
}

//Contains the source of the domain request and the paging parameters in the request.
//The source is a product object.  The product's primary domain will be used as the domain input.
//The source's primary domain, vendor, or product identifiers (id, sku+vendor, or asin) must be populated to be a valid request.
//The pagingRequestParameters takes in a page size and a cursor for the paging request.
type ProductsInDomainRequest interface {
	__type()
	SetSource(v model.Product)
	Source() model.Product
	SetPagingRequestParameters(v model.CursorPagingRequestParameters)
	PagingRequestParameters() model.CursorPagingRequestParameters
}
type _ProductsInDomainRequest struct {
	Ị_source                  model.Product                       `coral:"source" json:"source"`
	Ị_pagingRequestParameters model.CursorPagingRequestParameters `coral:"pagingRequestParameters" json:"pagingRequestParameters"`
}

func (this *_ProductsInDomainRequest) Source() model.Product {
	return this.Ị_source
}
func (this *_ProductsInDomainRequest) SetSource(v model.Product) {
	this.Ị_source = v
}
func (this *_ProductsInDomainRequest) PagingRequestParameters() model.CursorPagingRequestParameters {
	return this.Ị_pagingRequestParameters
}
func (this *_ProductsInDomainRequest) SetPagingRequestParameters(v model.CursorPagingRequestParameters) {
	this.Ị_pagingRequestParameters = v
}
func (this *_ProductsInDomainRequest) __type() {
}
func NewProductsInDomainRequest() ProductsInDomainRequest {
	return &_ProductsInDomainRequest{}
}
func init() {
	var val ProductsInDomainRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.discovery.model").RegisterShape("ProductsInDomainRequest", t, func() interface{} {
		return NewProductsInDomainRequest()
	})
}

//Contains a list of products that matched the given domain, traversal, and filter.
//source - the domain provided in the request.  If it was just product information, it will be populated with domain and vendor information.
//products - the paged reponse of products in the result after applying the filter.
//pagingResponseParameters - parameters to specify cursor and page size.
type ProductsInDomainResponse interface {
	__type()
	SetSource(v model.Product)
	Source() model.Product
	SetProducts(v []model.Product)
	Products() []model.Product
	SetPagingResponseParameters(v model.CursorPagingResponseParameters)
	PagingResponseParameters() model.CursorPagingResponseParameters
}
type _ProductsInDomainResponse struct {
	Ị_products                 []model.Product                      `coral:"products" json:"products"`
	Ị_pagingResponseParameters model.CursorPagingResponseParameters `coral:"pagingResponseParameters" json:"pagingResponseParameters"`
	Ị_source                   model.Product                        `coral:"source" json:"source"`
}

func (this *_ProductsInDomainResponse) Source() model.Product {
	return this.Ị_source
}
func (this *_ProductsInDomainResponse) SetSource(v model.Product) {
	this.Ị_source = v
}
func (this *_ProductsInDomainResponse) Products() []model.Product {
	return this.Ị_products
}
func (this *_ProductsInDomainResponse) SetProducts(v []model.Product) {
	this.Ị_products = v
}
func (this *_ProductsInDomainResponse) PagingResponseParameters() model.CursorPagingResponseParameters {
	return this.Ị_pagingResponseParameters
}
func (this *_ProductsInDomainResponse) SetPagingResponseParameters(v model.CursorPagingResponseParameters) {
	this.Ị_pagingResponseParameters = v
}
func (this *_ProductsInDomainResponse) __type() {
}
func NewProductsInDomainResponse() ProductsInDomainResponse {
	return &_ProductsInDomainResponse{}
}
func init() {
	var val ProductsInDomainResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.discovery.model").RegisterShape("ProductsInDomainResponse", t, func() interface{} {
		return NewProductsInDomainResponse()
	})
}

//docketId: the docket to use for hydration
//widgetId: the widget we're looking to expand upon
//purchasedProduct: optional field that allows P13N to arrange results based on a given product
//cursor: an indicator of where to start within the widget when returning products
//pageSize: how many products to return in one shot
//contextMetadata: optional map of additional arguments
type GetRecommendationsForWidgetRequest interface {
	__type()
	SetClient(v model.ClientInfo)
	Client() model.ClientInfo
	SetCustomer(v model.AmazonCustomerInfo)
	Customer() model.AmazonCustomerInfo
	SetPreferredLocale(v model.CustomerLocalePrefs)
	PreferredLocale() model.CustomerLocalePrefs
	SetLanguage(v *string)
	Language() *string
	SetPurchasedProduct(v model.Product)
	PurchasedProduct() model.Product
	SetCursor(v *string)
	Cursor() *string
	SetPageSize(v *int32)
	PageSize() *int32
	SetContextMetadata(v map[string]*string)
	ContextMetadata() map[string]*string
	SetDocketId(v *string)
	DocketId() *string
	SetWidgetId(v *string)
	WidgetId() *string
}
type _GetRecommendationsForWidgetRequest struct {
	Ị_language         *string                   `coral:"language" json:"language"`
	Ị_client           model.ClientInfo          `coral:"client" json:"client"`
	Ị_customer         model.AmazonCustomerInfo  `coral:"customer" json:"customer"`
	Ị_preferredLocale  model.CustomerLocalePrefs `coral:"preferredLocale" json:"preferredLocale"`
	Ị_widgetId         *string                   `coral:"widgetId" json:"widgetId"`
	Ị_purchasedProduct model.Product             `coral:"purchasedProduct" json:"purchasedProduct"`
	Ị_cursor           *string                   `coral:"cursor" json:"cursor"`
	Ị_pageSize         *int32                    `coral:"pageSize" json:"pageSize"`
	Ị_contextMetadata  map[string]*string        `coral:"contextMetadata" json:"contextMetadata"`
	Ị_docketId         *string                   `coral:"docketId" json:"docketId"`
}

func (this *_GetRecommendationsForWidgetRequest) Customer() model.AmazonCustomerInfo {
	return this.Ị_customer
}
func (this *_GetRecommendationsForWidgetRequest) SetCustomer(v model.AmazonCustomerInfo) {
	this.Ị_customer = v
}
func (this *_GetRecommendationsForWidgetRequest) PreferredLocale() model.CustomerLocalePrefs {
	return this.Ị_preferredLocale
}
func (this *_GetRecommendationsForWidgetRequest) SetPreferredLocale(v model.CustomerLocalePrefs) {
	this.Ị_preferredLocale = v
}
func (this *_GetRecommendationsForWidgetRequest) Language() *string {
	return this.Ị_language
}
func (this *_GetRecommendationsForWidgetRequest) SetLanguage(v *string) {
	this.Ị_language = v
}
func (this *_GetRecommendationsForWidgetRequest) Client() model.ClientInfo {
	return this.Ị_client
}
func (this *_GetRecommendationsForWidgetRequest) SetClient(v model.ClientInfo) {
	this.Ị_client = v
}
func (this *_GetRecommendationsForWidgetRequest) ContextMetadata() map[string]*string {
	return this.Ị_contextMetadata
}
func (this *_GetRecommendationsForWidgetRequest) SetContextMetadata(v map[string]*string) {
	this.Ị_contextMetadata = v
}
func (this *_GetRecommendationsForWidgetRequest) DocketId() *string {
	return this.Ị_docketId
}
func (this *_GetRecommendationsForWidgetRequest) SetDocketId(v *string) {
	this.Ị_docketId = v
}
func (this *_GetRecommendationsForWidgetRequest) WidgetId() *string {
	return this.Ị_widgetId
}
func (this *_GetRecommendationsForWidgetRequest) SetWidgetId(v *string) {
	this.Ị_widgetId = v
}
func (this *_GetRecommendationsForWidgetRequest) PurchasedProduct() model.Product {
	return this.Ị_purchasedProduct
}
func (this *_GetRecommendationsForWidgetRequest) SetPurchasedProduct(v model.Product) {
	this.Ị_purchasedProduct = v
}
func (this *_GetRecommendationsForWidgetRequest) Cursor() *string {
	return this.Ị_cursor
}
func (this *_GetRecommendationsForWidgetRequest) SetCursor(v *string) {
	this.Ị_cursor = v
}
func (this *_GetRecommendationsForWidgetRequest) PageSize() *int32 {
	return this.Ị_pageSize
}
func (this *_GetRecommendationsForWidgetRequest) SetPageSize(v *int32) {
	this.Ị_pageSize = v
}
func (this *_GetRecommendationsForWidgetRequest) __type() {
}
func NewGetRecommendationsForWidgetRequest() GetRecommendationsForWidgetRequest {
	return &_GetRecommendationsForWidgetRequest{}
}
func init() {
	var val GetRecommendationsForWidgetRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.discovery.model").RegisterShape("GetRecommendationsForWidgetRequest", t, func() interface{} {
		return NewGetRecommendationsForWidgetRequest()
	})
}

type GetCategoriesRequest interface {
	__type()
	SetCustomer(v model.AmazonCustomerInfo)
	Customer() model.AmazonCustomerInfo
	SetPreferredLocale(v model.CustomerLocalePrefs)
	PreferredLocale() model.CustomerLocalePrefs
	SetLanguage(v *string)
	Language() *string
	SetClient(v model.ClientInfo)
	Client() model.ClientInfo
	SetFilters(v []*string)
	Filters() []*string
	SetId(v *int64)
	Id() *int64
	SetProductLine(v *string)
	ProductLine() *string
}
type _GetCategoriesRequest struct {
	Ị_client          model.ClientInfo          `coral:"client" json:"client"`
	Ị_customer        model.AmazonCustomerInfo  `coral:"customer" json:"customer"`
	Ị_preferredLocale model.CustomerLocalePrefs `coral:"preferredLocale" json:"preferredLocale"`
	Ị_language        *string                   `coral:"language" json:"language"`
	Ị_id              *int64                    `coral:"id" json:"id"`
	Ị_productLine     *string                   `coral:"productLine" json:"productLine"`
	Ị_filters         []*string                 `coral:"filters" json:"filters"`
}

func (this *_GetCategoriesRequest) Client() model.ClientInfo {
	return this.Ị_client
}
func (this *_GetCategoriesRequest) SetClient(v model.ClientInfo) {
	this.Ị_client = v
}
func (this *_GetCategoriesRequest) Customer() model.AmazonCustomerInfo {
	return this.Ị_customer
}
func (this *_GetCategoriesRequest) SetCustomer(v model.AmazonCustomerInfo) {
	this.Ị_customer = v
}
func (this *_GetCategoriesRequest) PreferredLocale() model.CustomerLocalePrefs {
	return this.Ị_preferredLocale
}
func (this *_GetCategoriesRequest) SetPreferredLocale(v model.CustomerLocalePrefs) {
	this.Ị_preferredLocale = v
}
func (this *_GetCategoriesRequest) Language() *string {
	return this.Ị_language
}
func (this *_GetCategoriesRequest) SetLanguage(v *string) {
	this.Ị_language = v
}
func (this *_GetCategoriesRequest) Id() *int64 {
	return this.Ị_id
}
func (this *_GetCategoriesRequest) SetId(v *int64) {
	this.Ị_id = v
}
func (this *_GetCategoriesRequest) ProductLine() *string {
	return this.Ị_productLine
}
func (this *_GetCategoriesRequest) SetProductLine(v *string) {
	this.Ị_productLine = v
}
func (this *_GetCategoriesRequest) Filters() []*string {
	return this.Ị_filters
}
func (this *_GetCategoriesRequest) SetFilters(v []*string) {
	this.Ị_filters = v
}
func (this *_GetCategoriesRequest) __type() {
}
func NewGetCategoriesRequest() GetCategoriesRequest {
	return &_GetCategoriesRequest{}
}
func init() {
	var val GetCategoriesRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.discovery.model").RegisterShape("GetCategoriesRequest", t, func() interface{} {
		return NewGetCategoriesRequest()
	})
}

type GetCategoriesResponse interface {
	__type()
	SetCategories(v []CategoryInfo)
	Categories() []CategoryInfo
}
type _GetCategoriesResponse struct {
	Ị_categories []CategoryInfo `coral:"categories" json:"categories"`
}

func (this *_GetCategoriesResponse) Categories() []CategoryInfo {
	return this.Ị_categories
}
func (this *_GetCategoriesResponse) SetCategories(v []CategoryInfo) {
	this.Ị_categories = v
}
func (this *_GetCategoriesResponse) __type() {
}
func NewGetCategoriesResponse() GetCategoriesResponse {
	return &_GetCategoriesResponse{}
}
func init() {
	var val GetCategoriesResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.discovery.model").RegisterShape("GetCategoriesResponse", t, func() interface{} {
		return NewGetCategoriesResponse()
	})
}

type CategoryInfo interface {
	__type()
	SetId(v *int64)
	Id() *int64
	SetName(v *string)
	Name() *string
	SetDescription(v *string)
	Description() *string
	SetItemTypeKeyword(v *string)
	ItemTypeKeyword() *string
}
type _CategoryInfo struct {
	Ị_itemTypeKeyword *string `coral:"itemTypeKeyword" json:"itemTypeKeyword"`
	Ị_id              *int64  `coral:"id" json:"id"`
	Ị_name            *string `coral:"name" json:"name"`
	Ị_description     *string `coral:"description" json:"description"`
}

func (this *_CategoryInfo) Id() *int64 {
	return this.Ị_id
}
func (this *_CategoryInfo) SetId(v *int64) {
	this.Ị_id = v
}
func (this *_CategoryInfo) Name() *string {
	return this.Ị_name
}
func (this *_CategoryInfo) SetName(v *string) {
	this.Ị_name = v
}
func (this *_CategoryInfo) Description() *string {
	return this.Ị_description
}
func (this *_CategoryInfo) SetDescription(v *string) {
	this.Ị_description = v
}
func (this *_CategoryInfo) ItemTypeKeyword() *string {
	return this.Ị_itemTypeKeyword
}
func (this *_CategoryInfo) SetItemTypeKeyword(v *string) {
	this.Ị_itemTypeKeyword = v
}
func (this *_CategoryInfo) __type() {
}
func NewCategoryInfo() CategoryInfo {
	return &_CategoryInfo{}
}
func init() {
	var val CategoryInfo
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.discovery.model").RegisterShape("CategoryInfo", t, func() interface{} {
		return NewCategoryInfo()
	})
}

//The client has submitted a request that is invalid, either with bad parameters, missing parameters,
//or unrecognized parameters.
type InvalidRequestException interface {
	__type()
	error
	SetMessage(v *string)
	Message() *string
}
type _InvalidRequestException struct {
	Ị_message *string `coral:"message" json:"message"`
}

func (this *_InvalidRequestException) Error() string {
	return __model__.ErrorMessage(this)
}
func (this *_InvalidRequestException) Message() *string {
	return this.Ị_message
}
func (this *_InvalidRequestException) SetMessage(v *string) {
	this.Ị_message = v
}
func (this *_InvalidRequestException) __type() {
}
func NewInvalidRequestException() InvalidRequestException {
	return &_InvalidRequestException{}
}
func init() {
	var val InvalidRequestException
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.discovery.model").RegisterShape("InvalidRequestException", t, func() interface{} {
		return NewInvalidRequestException()
	})
}

type ReviewSummary interface {
	__type()
	SetAverageRating(v *float64)
	AverageRating() *float64
	SetOneStars(v *int32)
	OneStars() *int32
	SetTwoStars(v *int32)
	TwoStars() *int32
	SetThreeStars(v *int32)
	ThreeStars() *int32
	SetFourStars(v *int32)
	FourStars() *int32
	SetFiveStars(v *int32)
	FiveStars() *int32
	SetTotalReviews(v *int32)
	TotalReviews() *int32
}
type _ReviewSummary struct {
	Ị_oneStars      *int32   `coral:"oneStars" json:"oneStars"`
	Ị_twoStars      *int32   `coral:"twoStars" json:"twoStars"`
	Ị_threeStars    *int32   `coral:"threeStars" json:"threeStars"`
	Ị_fourStars     *int32   `coral:"fourStars" json:"fourStars"`
	Ị_fiveStars     *int32   `coral:"fiveStars" json:"fiveStars"`
	Ị_totalReviews  *int32   `coral:"totalReviews" json:"totalReviews"`
	Ị_averageRating *float64 `coral:"averageRating" json:"averageRating"`
}

func (this *_ReviewSummary) TotalReviews() *int32 {
	return this.Ị_totalReviews
}
func (this *_ReviewSummary) SetTotalReviews(v *int32) {
	this.Ị_totalReviews = v
}
func (this *_ReviewSummary) AverageRating() *float64 {
	return this.Ị_averageRating
}
func (this *_ReviewSummary) SetAverageRating(v *float64) {
	this.Ị_averageRating = v
}
func (this *_ReviewSummary) OneStars() *int32 {
	return this.Ị_oneStars
}
func (this *_ReviewSummary) SetOneStars(v *int32) {
	this.Ị_oneStars = v
}
func (this *_ReviewSummary) TwoStars() *int32 {
	return this.Ị_twoStars
}
func (this *_ReviewSummary) SetTwoStars(v *int32) {
	this.Ị_twoStars = v
}
func (this *_ReviewSummary) ThreeStars() *int32 {
	return this.Ị_threeStars
}
func (this *_ReviewSummary) SetThreeStars(v *int32) {
	this.Ị_threeStars = v
}
func (this *_ReviewSummary) FourStars() *int32 {
	return this.Ị_fourStars
}
func (this *_ReviewSummary) SetFourStars(v *int32) {
	this.Ị_fourStars = v
}
func (this *_ReviewSummary) FiveStars() *int32 {
	return this.Ị_fiveStars
}
func (this *_ReviewSummary) SetFiveStars(v *int32) {
	this.Ị_fiveStars = v
}
func (this *_ReviewSummary) __type() {
}
func NewReviewSummary() ReviewSummary {
	return &_ReviewSummary{}
}
func init() {
	var val ReviewSummary
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.discovery.model").RegisterShape("ReviewSummary", t, func() interface{} {
		return NewReviewSummary()
	})
}

type ReviewAuthor interface {
	__type()
	SetLocation(v *string)
	Location() *string
	SetAlias(v *string)
	Alias() *string
	SetCustomerId(v *string)
	CustomerId() *string
	SetDisplayName(v *string)
	DisplayName() *string
	SetIsUnderage(v *bool)
	IsUnderage() *bool
	SetName(v *string)
	Name() *string
	SetOriginatingMarketplaceId(v *string)
	OriginatingMarketplaceId() *string
	SetRawBadgeSummary(v *string)
	RawBadgeSummary() *string
	SetBadgeSummary(v *string)
	BadgeSummary() *string
}
type _ReviewAuthor struct {
	Ị_originatingMarketplaceId *string `coral:"originatingMarketplaceId" json:"originatingMarketplaceId"`
	Ị_rawBadgeSummary          *string `coral:"rawBadgeSummary" json:"rawBadgeSummary"`
	Ị_badgeSummary             *string `coral:"badgeSummary" json:"badgeSummary"`
	Ị_customerId               *string `coral:"customerId" json:"customerId"`
	Ị_displayName              *string `coral:"displayName" json:"displayName"`
	Ị_isUnderage               *bool   `coral:"isUnderage" json:"isUnderage"`
	Ị_name                     *string `coral:"name" json:"name"`
	Ị_alias                    *string `coral:"alias" json:"alias"`
	Ị_location                 *string `coral:"location" json:"location"`
}

func (this *_ReviewAuthor) Alias() *string {
	return this.Ị_alias
}
func (this *_ReviewAuthor) SetAlias(v *string) {
	this.Ị_alias = v
}
func (this *_ReviewAuthor) Location() *string {
	return this.Ị_location
}
func (this *_ReviewAuthor) SetLocation(v *string) {
	this.Ị_location = v
}
func (this *_ReviewAuthor) DisplayName() *string {
	return this.Ị_displayName
}
func (this *_ReviewAuthor) SetDisplayName(v *string) {
	this.Ị_displayName = v
}
func (this *_ReviewAuthor) IsUnderage() *bool {
	return this.Ị_isUnderage
}
func (this *_ReviewAuthor) SetIsUnderage(v *bool) {
	this.Ị_isUnderage = v
}
func (this *_ReviewAuthor) Name() *string {
	return this.Ị_name
}
func (this *_ReviewAuthor) SetName(v *string) {
	this.Ị_name = v
}
func (this *_ReviewAuthor) OriginatingMarketplaceId() *string {
	return this.Ị_originatingMarketplaceId
}
func (this *_ReviewAuthor) SetOriginatingMarketplaceId(v *string) {
	this.Ị_originatingMarketplaceId = v
}
func (this *_ReviewAuthor) RawBadgeSummary() *string {
	return this.Ị_rawBadgeSummary
}
func (this *_ReviewAuthor) SetRawBadgeSummary(v *string) {
	this.Ị_rawBadgeSummary = v
}
func (this *_ReviewAuthor) BadgeSummary() *string {
	return this.Ị_badgeSummary
}
func (this *_ReviewAuthor) SetBadgeSummary(v *string) {
	this.Ị_badgeSummary = v
}
func (this *_ReviewAuthor) CustomerId() *string {
	return this.Ị_customerId
}
func (this *_ReviewAuthor) SetCustomerId(v *string) {
	this.Ị_customerId = v
}
func (this *_ReviewAuthor) __type() {
}
func NewReviewAuthor() ReviewAuthor {
	return &_ReviewAuthor{}
}
func init() {
	var val ReviewAuthor
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.discovery.model").RegisterShape("ReviewAuthor", t, func() interface{} {
		return NewReviewAuthor()
	})
}

type CreateReviewPenNameRequest interface {
	__type()
	SetClient(v model.ClientInfo)
	Client() model.ClientInfo
	SetCustomer(v model.AmazonCustomerInfo)
	Customer() model.AmazonCustomerInfo
	SetPreferredLocale(v model.CustomerLocalePrefs)
	PreferredLocale() model.CustomerLocalePrefs
	SetLanguage(v *string)
	Language() *string
	SetPenName(v *string)
	PenName() *string
}
type _CreateReviewPenNameRequest struct {
	Ị_client          model.ClientInfo          `coral:"client" json:"client"`
	Ị_customer        model.AmazonCustomerInfo  `coral:"customer" json:"customer"`
	Ị_preferredLocale model.CustomerLocalePrefs `coral:"preferredLocale" json:"preferredLocale"`
	Ị_language        *string                   `coral:"language" json:"language"`
	Ị_penName         *string                   `coral:"penName" json:"penName"`
}

func (this *_CreateReviewPenNameRequest) Customer() model.AmazonCustomerInfo {
	return this.Ị_customer
}
func (this *_CreateReviewPenNameRequest) SetCustomer(v model.AmazonCustomerInfo) {
	this.Ị_customer = v
}
func (this *_CreateReviewPenNameRequest) PreferredLocale() model.CustomerLocalePrefs {
	return this.Ị_preferredLocale
}
func (this *_CreateReviewPenNameRequest) SetPreferredLocale(v model.CustomerLocalePrefs) {
	this.Ị_preferredLocale = v
}
func (this *_CreateReviewPenNameRequest) Language() *string {
	return this.Ị_language
}
func (this *_CreateReviewPenNameRequest) SetLanguage(v *string) {
	this.Ị_language = v
}
func (this *_CreateReviewPenNameRequest) Client() model.ClientInfo {
	return this.Ị_client
}
func (this *_CreateReviewPenNameRequest) SetClient(v model.ClientInfo) {
	this.Ị_client = v
}
func (this *_CreateReviewPenNameRequest) PenName() *string {
	return this.Ị_penName
}
func (this *_CreateReviewPenNameRequest) SetPenName(v *string) {
	this.Ị_penName = v
}
func (this *_CreateReviewPenNameRequest) __type() {
}
func NewCreateReviewPenNameRequest() CreateReviewPenNameRequest {
	return &_CreateReviewPenNameRequest{}
}
func init() {
	var val CreateReviewPenNameRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.discovery.model").RegisterShape("CreateReviewPenNameRequest", t, func() interface{} {
		return NewCreateReviewPenNameRequest()
	})
}

type CreateReviewPenNameResponse interface {
	__type()
	SetStatus(v *string)
	Status() *string
}
type _CreateReviewPenNameResponse struct {
	Ị_status *string `coral:"status" json:"status"`
}

func (this *_CreateReviewPenNameResponse) Status() *string {
	return this.Ị_status
}
func (this *_CreateReviewPenNameResponse) SetStatus(v *string) {
	this.Ị_status = v
}
func (this *_CreateReviewPenNameResponse) __type() {
}
func NewCreateReviewPenNameResponse() CreateReviewPenNameResponse {
	return &_CreateReviewPenNameResponse{}
}
func init() {
	var val CreateReviewPenNameResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.discovery.model").RegisterShape("CreateReviewPenNameResponse", t, func() interface{} {
		return NewCreateReviewPenNameResponse()
	})
}

//docketId: the docket to use for product hydration
//minRecsPerWidget: the minimum number of recommendations needed to populate a widget
//maxRecsPerWidget: the most products we should retrieve for a single widget
//widgetGroupId: an identifier that tells P13N what recommendation strategies/types of products should be returned
//widgetStartIndex: which widget to start at, within the widgetGroupId
//widgetEndIndex: which widget to end on
//purchasedProduct: optional field which allows for providing recommendations based on a specific product
//contextMetadata: an optional map of additional arguments
type GetRecommendedWidgetsRequest interface {
	__type()
	SetClient(v model.ClientInfo)
	Client() model.ClientInfo
	SetCustomer(v model.AmazonCustomerInfo)
	Customer() model.AmazonCustomerInfo
	SetPreferredLocale(v model.CustomerLocalePrefs)
	PreferredLocale() model.CustomerLocalePrefs
	SetLanguage(v *string)
	Language() *string
	SetContextMetadata(v map[string]*string)
	ContextMetadata() map[string]*string
	SetDocketId(v *string)
	DocketId() *string
	SetMinRecsPerWidget(v *int32)
	MinRecsPerWidget() *int32
	SetMaxRecsPerWidget(v *int32)
	MaxRecsPerWidget() *int32
	SetWidgetGroupId(v *string)
	WidgetGroupId() *string
	SetWidgetStartIndex(v *int32)
	WidgetStartIndex() *int32
	SetWidgetEndIndex(v *int32)
	WidgetEndIndex() *int32
	SetPurchasedProduct(v model.Product)
	PurchasedProduct() model.Product
}
type _GetRecommendedWidgetsRequest struct {
	Ị_customer         model.AmazonCustomerInfo  `coral:"customer" json:"customer"`
	Ị_preferredLocale  model.CustomerLocalePrefs `coral:"preferredLocale" json:"preferredLocale"`
	Ị_language         *string                   `coral:"language" json:"language"`
	Ị_client           model.ClientInfo          `coral:"client" json:"client"`
	Ị_widgetEndIndex   *int32                    `coral:"widgetEndIndex" json:"widgetEndIndex"`
	Ị_purchasedProduct model.Product             `coral:"purchasedProduct" json:"purchasedProduct"`
	Ị_contextMetadata  map[string]*string        `coral:"contextMetadata" json:"contextMetadata"`
	Ị_docketId         *string                   `coral:"docketId" json:"docketId"`
	Ị_minRecsPerWidget *int32                    `coral:"minRecsPerWidget" json:"minRecsPerWidget"`
	Ị_maxRecsPerWidget *int32                    `coral:"maxRecsPerWidget" json:"maxRecsPerWidget"`
	Ị_widgetGroupId    *string                   `coral:"widgetGroupId" json:"widgetGroupId"`
	Ị_widgetStartIndex *int32                    `coral:"widgetStartIndex" json:"widgetStartIndex"`
}

func (this *_GetRecommendedWidgetsRequest) Language() *string {
	return this.Ị_language
}
func (this *_GetRecommendedWidgetsRequest) SetLanguage(v *string) {
	this.Ị_language = v
}
func (this *_GetRecommendedWidgetsRequest) Client() model.ClientInfo {
	return this.Ị_client
}
func (this *_GetRecommendedWidgetsRequest) SetClient(v model.ClientInfo) {
	this.Ị_client = v
}
func (this *_GetRecommendedWidgetsRequest) Customer() model.AmazonCustomerInfo {
	return this.Ị_customer
}
func (this *_GetRecommendedWidgetsRequest) SetCustomer(v model.AmazonCustomerInfo) {
	this.Ị_customer = v
}
func (this *_GetRecommendedWidgetsRequest) PreferredLocale() model.CustomerLocalePrefs {
	return this.Ị_preferredLocale
}
func (this *_GetRecommendedWidgetsRequest) SetPreferredLocale(v model.CustomerLocalePrefs) {
	this.Ị_preferredLocale = v
}
func (this *_GetRecommendedWidgetsRequest) PurchasedProduct() model.Product {
	return this.Ị_purchasedProduct
}
func (this *_GetRecommendedWidgetsRequest) SetPurchasedProduct(v model.Product) {
	this.Ị_purchasedProduct = v
}
func (this *_GetRecommendedWidgetsRequest) ContextMetadata() map[string]*string {
	return this.Ị_contextMetadata
}
func (this *_GetRecommendedWidgetsRequest) SetContextMetadata(v map[string]*string) {
	this.Ị_contextMetadata = v
}
func (this *_GetRecommendedWidgetsRequest) DocketId() *string {
	return this.Ị_docketId
}
func (this *_GetRecommendedWidgetsRequest) SetDocketId(v *string) {
	this.Ị_docketId = v
}
func (this *_GetRecommendedWidgetsRequest) MinRecsPerWidget() *int32 {
	return this.Ị_minRecsPerWidget
}
func (this *_GetRecommendedWidgetsRequest) SetMinRecsPerWidget(v *int32) {
	this.Ị_minRecsPerWidget = v
}
func (this *_GetRecommendedWidgetsRequest) MaxRecsPerWidget() *int32 {
	return this.Ị_maxRecsPerWidget
}
func (this *_GetRecommendedWidgetsRequest) SetMaxRecsPerWidget(v *int32) {
	this.Ị_maxRecsPerWidget = v
}
func (this *_GetRecommendedWidgetsRequest) WidgetGroupId() *string {
	return this.Ị_widgetGroupId
}
func (this *_GetRecommendedWidgetsRequest) SetWidgetGroupId(v *string) {
	this.Ị_widgetGroupId = v
}
func (this *_GetRecommendedWidgetsRequest) WidgetStartIndex() *int32 {
	return this.Ị_widgetStartIndex
}
func (this *_GetRecommendedWidgetsRequest) SetWidgetStartIndex(v *int32) {
	this.Ị_widgetStartIndex = v
}
func (this *_GetRecommendedWidgetsRequest) WidgetEndIndex() *int32 {
	return this.Ị_widgetEndIndex
}
func (this *_GetRecommendedWidgetsRequest) SetWidgetEndIndex(v *int32) {
	this.Ị_widgetEndIndex = v
}
func (this *_GetRecommendedWidgetsRequest) __type() {
}
func NewGetRecommendedWidgetsRequest() GetRecommendedWidgetsRequest {
	return &_GetRecommendedWidgetsRequest{}
}
func init() {
	var val GetRecommendedWidgetsRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.discovery.model").RegisterShape("GetRecommendedWidgetsRequest", t, func() interface{} {
		return NewGetRecommendedWidgetsRequest()
	})
}

//Returns a list of products corresponding to the widgetId passed in, along with a cursor to indicate where we left off
type GetRecommendationsForWidgetResponse interface {
	__type()
	SetCursor(v *string)
	Cursor() *string
	SetRecommendations(v []model.Product)
	Recommendations() []model.Product
}
type _GetRecommendationsForWidgetResponse struct {
	Ị_cursor          *string         `coral:"cursor" json:"cursor"`
	Ị_recommendations []model.Product `coral:"recommendations" json:"recommendations"`
}

func (this *_GetRecommendationsForWidgetResponse) Cursor() *string {
	return this.Ị_cursor
}
func (this *_GetRecommendationsForWidgetResponse) SetCursor(v *string) {
	this.Ị_cursor = v
}
func (this *_GetRecommendationsForWidgetResponse) Recommendations() []model.Product {
	return this.Ị_recommendations
}
func (this *_GetRecommendationsForWidgetResponse) SetRecommendations(v []model.Product) {
	this.Ị_recommendations = v
}
func (this *_GetRecommendationsForWidgetResponse) __type() {
}
func NewGetRecommendationsForWidgetResponse() GetRecommendationsForWidgetResponse {
	return &_GetRecommendationsForWidgetResponse{}
}
func init() {
	var val GetRecommendationsForWidgetResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.discovery.model").RegisterShape("GetRecommendationsForWidgetResponse", t, func() interface{} {
		return NewGetRecommendationsForWidgetResponse()
	})
}

type RecommendedWidget interface {
	__type()
	SetRelevantCategory(v RecommendedCategory)
	RelevantCategory() RecommendedCategory
	SetMetadataMap(v map[string]*string)
	MetadataMap() map[string]*string
	SetId(v *string)
	Id() *string
	SetReftag(v *string)
	Reftag() *string
	SetTitle(v LocalizedString)
	Title() LocalizedString
	SetSubtitle(v LocalizedString)
	Subtitle() LocalizedString
	SetRecommendations(v []model.Product)
	Recommendations() []model.Product
}
type _RecommendedWidget struct {
	Ị_relevantCategory RecommendedCategory `coral:"relevantCategory" json:"relevantCategory"`
	Ị_metadataMap      map[string]*string  `coral:"metadataMap" json:"metadataMap"`
	Ị_id               *string             `coral:"id" json:"id"`
	Ị_reftag           *string             `coral:"reftag" json:"reftag"`
	Ị_title            LocalizedString     `coral:"title" json:"title"`
	Ị_subtitle         LocalizedString     `coral:"subtitle" json:"subtitle"`
	Ị_recommendations  []model.Product     `coral:"recommendations" json:"recommendations"`
}

func (this *_RecommendedWidget) Id() *string {
	return this.Ị_id
}
func (this *_RecommendedWidget) SetId(v *string) {
	this.Ị_id = v
}
func (this *_RecommendedWidget) Reftag() *string {
	return this.Ị_reftag
}
func (this *_RecommendedWidget) SetReftag(v *string) {
	this.Ị_reftag = v
}
func (this *_RecommendedWidget) Title() LocalizedString {
	return this.Ị_title
}
func (this *_RecommendedWidget) SetTitle(v LocalizedString) {
	this.Ị_title = v
}
func (this *_RecommendedWidget) Subtitle() LocalizedString {
	return this.Ị_subtitle
}
func (this *_RecommendedWidget) SetSubtitle(v LocalizedString) {
	this.Ị_subtitle = v
}
func (this *_RecommendedWidget) Recommendations() []model.Product {
	return this.Ị_recommendations
}
func (this *_RecommendedWidget) SetRecommendations(v []model.Product) {
	this.Ị_recommendations = v
}
func (this *_RecommendedWidget) RelevantCategory() RecommendedCategory {
	return this.Ị_relevantCategory
}
func (this *_RecommendedWidget) SetRelevantCategory(v RecommendedCategory) {
	this.Ị_relevantCategory = v
}
func (this *_RecommendedWidget) MetadataMap() map[string]*string {
	return this.Ị_metadataMap
}
func (this *_RecommendedWidget) SetMetadataMap(v map[string]*string) {
	this.Ị_metadataMap = v
}
func (this *_RecommendedWidget) __type() {
}
func NewRecommendedWidget() RecommendedWidget {
	return &_RecommendedWidget{}
}
func init() {
	var val RecommendedWidget
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.discovery.model").RegisterShape("RecommendedWidget", t, func() interface{} {
		return NewRecommendedWidget()
	})
}

type DeviceInfo interface {
	__type()
	SetBanjoCapabilityVersion(v *string)
	BanjoCapabilityVersion() *string
	SetOsVersion(v *string)
	OsVersion() *string
	SetKiwiCompatibleVersion(v *string)
	KiwiCompatibleVersion() *string
	SetRef(v *string)
	Ref() *string
	SetDeviceDensityClassification(v *string)
	DeviceDensityClassification() *string
	SetBuild_product(v *string)
	Build_product() *string
	SetPackageName(v *string)
	PackageName() *string
	SetDeviceDescriptorId(v *string)
	DeviceDescriptorId() *string
	SetAmznSdkVersion(v *string)
	AmznSdkVersion() *string
	SetBuild_fingerprint(v *string)
	Build_fingerprint() *string
	SetSimOperator(v *string)
	SimOperator() *string
	SetCarrier(v *string)
	Carrier() *string
	SetDeviceType(v *string)
	DeviceType() *string
	SetDeviceScreenLayout(v *string)
	DeviceScreenLayout() *string
	SetSerial(v *string)
	Serial() *string
	SetPhoneType(v *string)
	PhoneType() *string
	SetManufacturer(v *string)
	Manufacturer() *string
	SetModel(v *string)
	Model() *string
}
type _DeviceInfo struct {
	Ị_build_product               *string `coral:"build_product" json:"build_product"`
	Ị_packageName                 *string `coral:"packageName" json:"packageName"`
	Ị_banjoCapabilityVersion      *string `coral:"banjoCapabilityVersion" json:"banjoCapabilityVersion"`
	Ị_osVersion                   *string `coral:"osVersion" json:"osVersion"`
	Ị_kiwiCompatibleVersion       *string `coral:"kiwiCompatibleVersion" json:"kiwiCompatibleVersion"`
	Ị_ref                         *string `coral:"ref" json:"ref"`
	Ị_deviceDensityClassification *string `coral:"deviceDensityClassification" json:"deviceDensityClassification"`
	Ị_deviceDescriptorId          *string `coral:"deviceDescriptorId" json:"deviceDescriptorId"`
	Ị_amznSdkVersion              *string `coral:"amznSdkVersion" json:"amznSdkVersion"`
	Ị_build_fingerprint           *string `coral:"build_fingerprint" json:"build_fingerprint"`
	Ị_simOperator                 *string `coral:"simOperator" json:"simOperator"`
	Ị_phoneType                   *string `coral:"phoneType" json:"phoneType"`
	Ị_carrier                     *string `coral:"carrier" json:"carrier"`
	Ị_deviceType                  *string `coral:"deviceType" json:"deviceType"`
	Ị_deviceScreenLayout          *string `coral:"deviceScreenLayout" json:"deviceScreenLayout"`
	Ị_serial                      *string `coral:"serial" json:"serial"`
	Ị_manufacturer                *string `coral:"manufacturer" json:"manufacturer"`
	Ị_model                       *string `coral:"model" json:"model"`
}

func (this *_DeviceInfo) Carrier() *string {
	return this.Ị_carrier
}
func (this *_DeviceInfo) SetCarrier(v *string) {
	this.Ị_carrier = v
}
func (this *_DeviceInfo) DeviceType() *string {
	return this.Ị_deviceType
}
func (this *_DeviceInfo) SetDeviceType(v *string) {
	this.Ị_deviceType = v
}
func (this *_DeviceInfo) DeviceScreenLayout() *string {
	return this.Ị_deviceScreenLayout
}
func (this *_DeviceInfo) SetDeviceScreenLayout(v *string) {
	this.Ị_deviceScreenLayout = v
}
func (this *_DeviceInfo) Serial() *string {
	return this.Ị_serial
}
func (this *_DeviceInfo) SetSerial(v *string) {
	this.Ị_serial = v
}
func (this *_DeviceInfo) PhoneType() *string {
	return this.Ị_phoneType
}
func (this *_DeviceInfo) SetPhoneType(v *string) {
	this.Ị_phoneType = v
}
func (this *_DeviceInfo) Manufacturer() *string {
	return this.Ị_manufacturer
}
func (this *_DeviceInfo) SetManufacturer(v *string) {
	this.Ị_manufacturer = v
}
func (this *_DeviceInfo) Model() *string {
	return this.Ị_model
}
func (this *_DeviceInfo) SetModel(v *string) {
	this.Ị_model = v
}
func (this *_DeviceInfo) OsVersion() *string {
	return this.Ị_osVersion
}
func (this *_DeviceInfo) SetOsVersion(v *string) {
	this.Ị_osVersion = v
}
func (this *_DeviceInfo) KiwiCompatibleVersion() *string {
	return this.Ị_kiwiCompatibleVersion
}
func (this *_DeviceInfo) SetKiwiCompatibleVersion(v *string) {
	this.Ị_kiwiCompatibleVersion = v
}
func (this *_DeviceInfo) Ref() *string {
	return this.Ị_ref
}
func (this *_DeviceInfo) SetRef(v *string) {
	this.Ị_ref = v
}
func (this *_DeviceInfo) DeviceDensityClassification() *string {
	return this.Ị_deviceDensityClassification
}
func (this *_DeviceInfo) SetDeviceDensityClassification(v *string) {
	this.Ị_deviceDensityClassification = v
}
func (this *_DeviceInfo) Build_product() *string {
	return this.Ị_build_product
}
func (this *_DeviceInfo) SetBuild_product(v *string) {
	this.Ị_build_product = v
}
func (this *_DeviceInfo) PackageName() *string {
	return this.Ị_packageName
}
func (this *_DeviceInfo) SetPackageName(v *string) {
	this.Ị_packageName = v
}
func (this *_DeviceInfo) BanjoCapabilityVersion() *string {
	return this.Ị_banjoCapabilityVersion
}
func (this *_DeviceInfo) SetBanjoCapabilityVersion(v *string) {
	this.Ị_banjoCapabilityVersion = v
}
func (this *_DeviceInfo) DeviceDescriptorId() *string {
	return this.Ị_deviceDescriptorId
}
func (this *_DeviceInfo) SetDeviceDescriptorId(v *string) {
	this.Ị_deviceDescriptorId = v
}
func (this *_DeviceInfo) AmznSdkVersion() *string {
	return this.Ị_amznSdkVersion
}
func (this *_DeviceInfo) SetAmznSdkVersion(v *string) {
	this.Ị_amznSdkVersion = v
}
func (this *_DeviceInfo) Build_fingerprint() *string {
	return this.Ị_build_fingerprint
}
func (this *_DeviceInfo) SetBuild_fingerprint(v *string) {
	this.Ị_build_fingerprint = v
}
func (this *_DeviceInfo) SimOperator() *string {
	return this.Ị_simOperator
}
func (this *_DeviceInfo) SetSimOperator(v *string) {
	this.Ị_simOperator = v
}
func (this *_DeviceInfo) __type() {
}
func NewDeviceInfo() DeviceInfo {
	return &_DeviceInfo{}
}
func init() {
	var val DeviceInfo
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.discovery.model").RegisterShape("DeviceInfo", t, func() interface{} {
		return NewDeviceInfo()
	})
}

type GetProductDetailsRequest interface {
	__type()
	SetClient(v model.ClientInfo)
	Client() model.ClientInfo
	SetCustomer(v model.AmazonCustomerInfo)
	Customer() model.AmazonCustomerInfo
	SetPreferredLocale(v model.CustomerLocalePrefs)
	PreferredLocale() model.CustomerLocalePrefs
	SetLanguage(v *string)
	Language() *string
	SetProduct(v model.Product)
	Product() model.Product
	SetDocketId(v *string)
	DocketId() *string
}
type _GetProductDetailsRequest struct {
	Ị_customer        model.AmazonCustomerInfo  `coral:"customer" json:"customer"`
	Ị_preferredLocale model.CustomerLocalePrefs `coral:"preferredLocale" json:"preferredLocale"`
	Ị_language        *string                   `coral:"language" json:"language"`
	Ị_client          model.ClientInfo          `coral:"client" json:"client"`
	Ị_product         model.Product             `coral:"product" json:"product"`
	Ị_docketId        *string                   `coral:"docketId" json:"docketId"`
}

func (this *_GetProductDetailsRequest) PreferredLocale() model.CustomerLocalePrefs {
	return this.Ị_preferredLocale
}
func (this *_GetProductDetailsRequest) SetPreferredLocale(v model.CustomerLocalePrefs) {
	this.Ị_preferredLocale = v
}
func (this *_GetProductDetailsRequest) Language() *string {
	return this.Ị_language
}
func (this *_GetProductDetailsRequest) SetLanguage(v *string) {
	this.Ị_language = v
}
func (this *_GetProductDetailsRequest) Client() model.ClientInfo {
	return this.Ị_client
}
func (this *_GetProductDetailsRequest) SetClient(v model.ClientInfo) {
	this.Ị_client = v
}
func (this *_GetProductDetailsRequest) Customer() model.AmazonCustomerInfo {
	return this.Ị_customer
}
func (this *_GetProductDetailsRequest) SetCustomer(v model.AmazonCustomerInfo) {
	this.Ị_customer = v
}
func (this *_GetProductDetailsRequest) Product() model.Product {
	return this.Ị_product
}
func (this *_GetProductDetailsRequest) SetProduct(v model.Product) {
	this.Ị_product = v
}
func (this *_GetProductDetailsRequest) DocketId() *string {
	return this.Ị_docketId
}
func (this *_GetProductDetailsRequest) SetDocketId(v *string) {
	this.Ị_docketId = v
}
func (this *_GetProductDetailsRequest) __type() {
}
func NewGetProductDetailsRequest() GetProductDetailsRequest {
	return &_GetProductDetailsRequest{}
}
func init() {
	var val GetProductDetailsRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.discovery.model").RegisterShape("GetProductDetailsRequest", t, func() interface{} {
		return NewGetProductDetailsRequest()
	})
}

type CastVoteResponse interface {
	__type()
}
type _CastVoteResponse struct {
}

func (this *_CastVoteResponse) __type() {
}
func NewCastVoteResponse() CastVoteResponse {
	return &_CastVoteResponse{}
}
func init() {
	var val CastVoteResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.discovery.model").RegisterShape("CastVoteResponse", t, func() interface{} {
		return NewCastVoteResponse()
	})
}

//Returns a list of widgets, each of which contains metadata about itself along with a list of products
type GetRecommendedWidgetsResponse interface {
	__type()
	SetRecommendedWidgets(v []RecommendedWidget)
	RecommendedWidgets() []RecommendedWidget
}
type _GetRecommendedWidgetsResponse struct {
	Ị_recommendedWidgets []RecommendedWidget `coral:"recommendedWidgets" json:"recommendedWidgets"`
}

func (this *_GetRecommendedWidgetsResponse) RecommendedWidgets() []RecommendedWidget {
	return this.Ị_recommendedWidgets
}
func (this *_GetRecommendedWidgetsResponse) SetRecommendedWidgets(v []RecommendedWidget) {
	this.Ị_recommendedWidgets = v
}
func (this *_GetRecommendedWidgetsResponse) __type() {
}
func NewGetRecommendedWidgetsResponse() GetRecommendedWidgetsResponse {
	return &_GetRecommendedWidgetsResponse{}
}
func init() {
	var val GetRecommendedWidgetsResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.discovery.model").RegisterShape("GetRecommendedWidgetsResponse", t, func() interface{} {
		return NewGetRecommendedWidgetsResponse()
	})
}

type ValidateDocketResponse interface {
	__type()
	SetProduct(v model.Product)
	Product() model.Product
}
type _ValidateDocketResponse struct {
	Ị_product model.Product `coral:"product" json:"product"`
}

func (this *_ValidateDocketResponse) Product() model.Product {
	return this.Ị_product
}
func (this *_ValidateDocketResponse) SetProduct(v model.Product) {
	this.Ị_product = v
}
func (this *_ValidateDocketResponse) __type() {
}
func NewValidateDocketResponse() ValidateDocketResponse {
	return &_ValidateDocketResponse{}
}
func init() {
	var val ValidateDocketResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.discovery.model").RegisterShape("ValidateDocketResponse", t, func() interface{} {
		return NewValidateDocketResponse()
	})
}

//An object that contains a substituion string for a LocalizedString object, used for Recommendations-based titles and subtitles
type LocalizedStringSubst interface {
	__type()
	SetKey(v *string)
	Key() *string
	SetValue(v *string)
	Value() *string
}
type _LocalizedStringSubst struct {
	Ị_key   *string `coral:"key" json:"key"`
	Ị_value *string `coral:"value" json:"value"`
}

func (this *_LocalizedStringSubst) Key() *string {
	return this.Ị_key
}
func (this *_LocalizedStringSubst) SetKey(v *string) {
	this.Ị_key = v
}
func (this *_LocalizedStringSubst) Value() *string {
	return this.Ị_value
}
func (this *_LocalizedStringSubst) SetValue(v *string) {
	this.Ị_value = v
}
func (this *_LocalizedStringSubst) __type() {
}
func NewLocalizedStringSubst() LocalizedStringSubst {
	return &_LocalizedStringSubst{}
}
func init() {
	var val LocalizedStringSubst
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.discovery.model").RegisterShape("LocalizedStringSubst", t, func() interface{} {
		return NewLocalizedStringSubst()
	})
}

type ReviewResponseSummary interface {
	__type()
	SetReviewSummary(v ReviewSummary)
	ReviewSummary() ReviewSummary
	SetSyndicatedReviewSummary(v ReviewSummary)
	SyndicatedReviewSummary() ReviewSummary
	SetReviews(v []ProductReview)
	Reviews() []ProductReview
	SetSyndicatedReviews(v []ProductReview)
	SyndicatedReviews() []ProductReview
}
type _ReviewResponseSummary struct {
	Ị_reviewSummary           ReviewSummary   `coral:"reviewSummary" json:"reviewSummary"`
	Ị_syndicatedReviewSummary ReviewSummary   `coral:"syndicatedReviewSummary" json:"syndicatedReviewSummary"`
	Ị_reviews                 []ProductReview `coral:"reviews" json:"reviews"`
	Ị_syndicatedReviews       []ProductReview `coral:"syndicatedReviews" json:"syndicatedReviews"`
}

func (this *_ReviewResponseSummary) ReviewSummary() ReviewSummary {
	return this.Ị_reviewSummary
}
func (this *_ReviewResponseSummary) SetReviewSummary(v ReviewSummary) {
	this.Ị_reviewSummary = v
}
func (this *_ReviewResponseSummary) SyndicatedReviewSummary() ReviewSummary {
	return this.Ị_syndicatedReviewSummary
}
func (this *_ReviewResponseSummary) SetSyndicatedReviewSummary(v ReviewSummary) {
	this.Ị_syndicatedReviewSummary = v
}
func (this *_ReviewResponseSummary) Reviews() []ProductReview {
	return this.Ị_reviews
}
func (this *_ReviewResponseSummary) SetReviews(v []ProductReview) {
	this.Ị_reviews = v
}
func (this *_ReviewResponseSummary) SyndicatedReviews() []ProductReview {
	return this.Ị_syndicatedReviews
}
func (this *_ReviewResponseSummary) SetSyndicatedReviews(v []ProductReview) {
	this.Ị_syndicatedReviews = v
}
func (this *_ReviewResponseSummary) __type() {
}
func NewReviewResponseSummary() ReviewResponseSummary {
	return &_ReviewResponseSummary{}
}
func init() {
	var val ReviewResponseSummary
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.adg.discovery.model").RegisterShape("ReviewResponseSummary", t, func() interface{} {
		return NewReviewResponseSummary()
	})
}
func init() {
	var val map[string]*string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to map[string]*string
		if f, ok := from.Interface().(map[string]*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to StringMap")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val []SearchRefinement
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to []SearchRefinement
		if f, ok := from.Interface().([]SearchRefinement); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to SearchRefinementList")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val []SearchResult
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to []SearchResult
		if f, ok := from.Interface().([]SearchResult); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to SearchResultsList")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val []CategoryInfo
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to []CategoryInfo
		if f, ok := from.Interface().([]CategoryInfo); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to CategoryInfoList")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val []RecommendedWidget
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to []RecommendedWidget
		if f, ok := from.Interface().([]RecommendedWidget); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to RecommendedWidgetsList")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val []ReviewRating
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to []ReviewRating
		if f, ok := from.Interface().([]ReviewRating); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to ReviewRatingsList")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val []ProductReview
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to []ProductReview
		if f, ok := from.Interface().([]ProductReview); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to ProductReviewList")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val []*string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to []*string
		if f, ok := from.Interface().([]*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to StringList")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val []SearchRefinementValue
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to []SearchRefinementValue
		if f, ok := from.Interface().([]SearchRefinementValue); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to SearchRefinementValueList")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val []LocalizedStringSubst
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to []LocalizedStringSubst
		if f, ok := from.Interface().([]LocalizedStringSubst); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to LocalizedStringSubstList")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val []ProductsInDomainRequest
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to []ProductsInDomainRequest
		if f, ok := from.Interface().([]ProductsInDomainRequest); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to ProductsInDomainRequestList")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val []ProductsInDomainResponse
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to []ProductsInDomainResponse
		if f, ok := from.Interface().([]ProductsInDomainResponse); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to ProductsInDomainResponseList")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val []*int64
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to []*int64
		if f, ok := from.Interface().([]*int64); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to LongList")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val []model.Product
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to []model.Product
		if f, ok := from.Interface().([]model.Product); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to ProductList")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}

//Provides discovery functionality for Amazon Digital Goods, including search and product details.
type ADGDiscoveryService interface { //Retrieves multiple paged responses of all the products for the specified domains.
	//productsInDomainRequest - Requests to specify the domain and paging parameters that products should be fetched from.
	//traversal - The traversal to use to find the domains.  See traversal for more details.
	//productFilter - The ProductFilter to be applied to the returned objects.  Products that do not pass this filter will not be returned.
	GetProductsInDomains(input GetProductsInDomainsRequest) (GetProductsInDomainsResponse, error) //Retrieves the product details for a given product, including store specific information
	//(e.g. app details for apps, Skill command words for skills, etc).
	GetProductListDetails(input GetProductListDetailsRequest) (GetProductListDetailsResponse, error) //Retrieves the reviews for a given product.
	GetProductReviews(input GetProductReviewsRequest) (GetProductReviewsResponse, error)
	ValidateDocket(input ValidateDocketRequest) (ValidateDocketResponse, error) //Get the list of categories given either a root node id or product line. Given either input, it will retrieve the
	//id, name, description and itemTypeKeyword of all the immediate children. If both a id and product line are provided,
	//the id will be preferred.
	GetCategories(input GetCategoriesRequest) (GetCategoriesResponse, error)                         //Provides a list of recommended widgets
	GetRecommendedWidgets(input GetRecommendedWidgetsRequest) (GetRecommendedWidgetsResponse, error) //Search a given browse node for matching products.
	Search(input SearchRequest) (SearchResponse, error)                                              //Retrieves the product details for a given product, including store specific information
	//(e.g. app details for apps, Skill command words for skills, etc).
	GetProductDetails(input GetProductDetailsRequest) (GetProductDetailsResponse, error)                               //Expands on one widget, providing more products than are returned in the getRecommendedWidgets call
	GetRecommendationsForWidget(input GetRecommendationsForWidgetRequest) (GetRecommendationsForWidgetResponse, error) //Creates a penName for a given customer
	CreateReviewPenName(input CreateReviewPenNameRequest) (CreateReviewPenNameResponse, error)                         //Create a product review
	CreateProductReview(input CreateProductReviewRequest) (CreateProductReviewResponse, error)                         //Allows a customer to provide feedback on a review.
	CastVote(input CastVoteRequest) (CastVoteResponse, error)
}
