
    job {
	name "common-chitin"
	using 'TEMPLATE-autobuild'
	scm {
            git {
		remote {
			github 'common/chitin', 'ssh', 'git-aws.internal.justin.tv'
			credentials 'git-aws-read-key'
		}
		clean true
	    }
	}
	steps {
shell 'manta -proxy -f _build/mantas/manta.go1.11.13.linux-amd64.json'
shell 'manta -proxy -f _build/mantas/manta.go1.12.17.linux-amd64.json'
shell 'manta -proxy -f _build/mantas/manta.go1.13.8.linux-amd64.json'
shell 'manta -proxy -f _build/mantas/manta.go1.14.linux-amd64.json'
	}
    }

    job {
	name 'common-chitin-deploy'
	using 'TEMPLATE-deploy'
	steps {
	}
    }
    