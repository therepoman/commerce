module code.justin.tv/common/chitin

go 1.14

require (
	code.justin.tv/common/golibs v0.0.0-20160822174935-4bc817203296
	code.justin.tv/release/trace v0.0.0-20170825220123-03f03e697a47
	github.com/golang/protobuf v1.3.2
	github.com/kr/logfmt v0.0.0-20140226030751-b84e30acd515
	github.com/youtube/vitess v2.0.0+incompatible // indirect
	github.com/zenazn/goji v0.9.1-0.20160823030549-4d7077956293
	golang.org/x/net v0.0.0-20190311183353-d8887717615a
	google.golang.org/grpc v1.27.1
)
