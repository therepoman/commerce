# Changelog

## v0.9.0 -- 2016-12-07

- First versioned release! (D4714)

## 2016-11-18

- Adapt to API change in gRPC v1.0.3 (D4606)

## 2016-11-16

- Support Go 1.7's use of context in http.Request values (D4176)

## 2016-11-06

- Break dependency between chitin and gRPC metadata package (D4513)

## 2016-10-26

- protoc_gen command sets import path for google.api.annotations (D4430)

## 2016-09-19

- Allow access to Trace ID and Span from Context (D4190)

## 2016-09-07

- Drop support for Go 1.4 (D4156)

## 2016-07-20

- Use ptypes package for well-known protobuf types (D3804)

## 2016-06-30

- Fix goroutine leak when RoundTrip encounters an error

## 2016-06-14

- Remove vendor symlink to ease adoption of Go-1.5 vendoring

## 2016-06-01

- Allow multiple copies of the package to be linked into a binary

## 2016-04-21

- Update to new gRPC API signatures

## 2016-03-25

- Update dependencies

## 2016-03-03

- Run tests against several versions of Go (1.4, 1.5, 1.6)

## 2016-01-25

- Include fully-qualified gRPC method name in Trace events
- Fix crash bug with Go 1.6's new http.CloseNotifier implementation

## 2016-01-07

- Add generated code for well-known protobuf types "any", "empty", and "timestamp"

## 2015-12-15

- Fix bug in process-wide opt-in API which prevented its use in some cases
- Include SQL user name and db name in Trace events

## 2015-11-20

- Add script to invoke protoc and to add chitin instrumentation to the generated Go code
- Use updated proto3 definitions for Trace message format

## 2015-11-10

- Add pprof instrumentation for leaked HTTP and gRPC client requests

## 2015-11-09

- Emit more Trace events for HTTP client requests for diagnosing slow dials and slow body fetches

## 2015-10-22

- Update to new gRPC API signatures

## 2015-09-24

- Update to new gRPC API signatures

## 2015-09-15

- Support the OPTIONS method for HTTP

## 2015-09-01

- Allow instrumentation of gRPC APIs

## 2015-08-20

- Allow users to enable Trace events for a whole process, rather than per HTTP handler

## 2015-05-14

- Update clog prefix to match standard ASCII txid format
- Include normalized SQL query in trace events

## 2015-05-08

- Fix goroutine leak in CloseNotifier implementation

## 2015-04-20

- Allow users to emit basic trace events for uncommon protocols

## 2015-04-17

- Trace db queries

## 2015-04-16

- Work around golang.org/issue/10474 in http request cancellation

## 2015-04-15

- Allow users to create new trace spans
- Users can upgrade their existing http.RoundTripper

## 2015-04-09

- Outbound http requests are now canceled when their context expires, even while reading the response Body
- Include additional information in the emitted trace events

## 2015-02-20

- Batch trace events to amortize syscall overhead

## 2015-02-13

- Report the import path of the current command as the trace service name

## 2015-02-11

- Optionally emit protobuf-formatted trace events over UDP

## 2015-02-02

- Do not emit logs unless the user passes a SetLogger option, this time with tests

## 2015-01-28

- Do not emit logs unless the user passes a SetLogger option
- Bubble up original panic in HTTP server so package net/http will log it

## 2015-01-25

- Test case for Trace header passing

## 2015-01-20

- HTTP client correctly passes response headers

## 2015-01-16

- HTTP client and server allow PATCH method

## 2014-12-19

- (*net/http.Request).Body.Close() in an HTTP server doesn't recurse infinitely

## 2014-12-17

- Breaking change to make chitin.Context's correct usage more obvious
