module code.justin.tv/hygienic/metrics

go 1.12

require (
	github.com/benbjohnson/clock v0.0.0-20161215174838-7dc76406b6d3
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/stretchr/testify v1.3.0
)
