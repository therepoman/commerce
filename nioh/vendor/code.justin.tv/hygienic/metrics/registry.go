package metrics

import (
	"sync"
)

// Registry implements the BaseRegistry interface in a sane way that you probably don't want to modify
type Registry struct {
	TimeSeriesSet
	MetricCollectorSet
	AggregationConstructor AggregationConstructor
}

var _ BaseRegistry = &Registry{}
var _ AggregationSource = &Registry{}

// TimeSeriesSet allows the unique creation of time series by their identifier.  It is thread safe.
type TimeSeriesSet struct {
	lookup map[string]*TimeSeries
	mu     sync.RWMutex
}

func (s *TimeSeriesSet) get(tsid TimeSeriesIdentifier) *TimeSeries {
	s.mu.RLock()
	defer s.mu.RUnlock()
	return s.lookup[tsid.UID()]
}

// TimeSeries returns an existing time series for a tsid, or creates a new one if one does not exist.  It is thread safe
func (s *TimeSeriesSet) TimeSeries(tsid TimeSeriesIdentifier, metadataConstructor MetadataConstructor) *TimeSeries {
	if ts := s.get(tsid); ts != nil {
		return ts
	}

	s.mu.Lock()
	defer s.mu.Unlock()
	if ts, exists := s.lookup[tsid.UID()]; exists {
		return ts
	}

	if s.lookup == nil {
		s.lookup = make(map[string]*TimeSeries)
	}
	ret := &TimeSeries{
		Tsi: uniqueCopy(tsid),
	}
	if metadataConstructor != nil {
		ret.Tsm = metadataConstructor(tsid, nil)
	}
	s.lookup[tsid.UID()] = ret
	return ret
}

// MetricCollectorSet allows associating time series to unique metric collectors
type MetricCollectorSet struct {
	collectors map[*TimeSeries]MetricCollector
	mu         sync.RWMutex
}

// GetOrSet will set the collector of a time series, if one does not exist.  It then returns the set collector, or
// the collector that was already there.
func (t *MetricCollectorSet) GetOrSet(ts *TimeSeries, mc MetricCollectorConstructor) MetricCollector {
	t.mu.Lock()
	defer t.mu.Unlock()
	if mc, exists := t.collectors[ts]; exists {
		return mc
	}
	ret := mc(ts)
	if t.collectors == nil {
		t.collectors = make(map[*TimeSeries]MetricCollector)
	}
	t.collectors[ts] = ret
	return ret
}

// FlushMetrics aggregates all time series in this set using their set collector and returns the result.
func (t *MetricCollectorSet) FlushMetrics() []TimeSeriesAggregation {
	t.mu.RLock()
	defer t.mu.RUnlock()
	batch := make([]TimeSeriesAggregation, 0, len(t.collectors))
	for k, v := range t.collectors {
		for _, m := range v.CollectMetrics() {
			batch = append(batch, TimeSeriesAggregation{
				TS:          k,
				Aggregation: m,
			})
		}
	}
	return batch
}

func (t *Registry) aggregationConstructor(ts *TimeSeries) MetricCollector {
	if t.AggregationConstructor == nil {
		return &nopAggregator{}
	}
	return t.AggregationConstructor(ts)
}

// Observer creates an observer for a time series
func (t *Registry) Observer(ts *TimeSeries) Observer {
	mc := t.GetOrSet(ts, t.aggregationConstructor)
	asAg, ok := mc.(Observer)
	if !ok {
		// It's something else ... strange
		return nil
	}
	return asAg
}
