module code.justin.tv/hygienic/metricscactusstatsd

go 1.12

require (
	code.justin.tv/amzn/TwitchLogging v0.0.0-20190731182733-d8aae132db1f // indirect
	code.justin.tv/amzn/TwitchProcessIdentifier v0.0.0-20191004180637-dc817f563e55
	code.justin.tv/amzn/TwitchTelemetry v0.0.0-20191018223415-c7305aaf8766
	code.justin.tv/amzn/TwitchTelemetryCloudWatchMetricsSender v0.0.0-20190822201853-9acf2b6ccaa1
	code.justin.tv/amzn/TwitchTelemetryMetricsMiddleware v0.0.0-20190731182748-cb62e9370b7f
	code.justin.tv/hygienic/metrics v0.2.2
	code.justin.tv/video/metrics-middleware/v2 v2.0.0
	github.com/cactus/go-statsd-client v3.1.1+incompatible
	github.com/smartystreets/goconvey v1.6.4 // indirect
	github.com/stretchr/testify v1.3.0
	github.com/twitchtv/twirp v5.9.0+incompatible
)
