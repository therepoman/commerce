package metricscactusstatsd

import (
	"errors"
	"strings"
	"time"

	"code.justin.tv/hygienic/metrics"
	"code.justin.tv/hygienic/metrics/metricsext"
	"github.com/cactus/go-statsd-client/statsd"
	// General imports for example
	"github.com/twitchtv/twirp"

	// TwitchTelemetry imports
	"code.justin.tv/amzn/TwitchProcessIdentifier"
	"code.justin.tv/amzn/TwitchTelemetry"
	cw "code.justin.tv/amzn/TwitchTelemetryCloudWatchMetricsSender"

	// Middleware Related
	metricsmiddleware "code.justin.tv/amzn/TwitchTelemetryMetricsMiddleware"
	"code.justin.tv/video/metrics-middleware/v2/operation"
	"code.justin.tv/video/metrics-middleware/v2/twirpmetric"
)

type ExtendedSubStatter interface {
	statsd.SubStatter
	TaggingSubStatter
}

type TaggingSubStatter interface {
	IncD(key string, dimensions map[string]string, v int64)
	GaugeD(key string, dimensions map[string]string, v int64)
	TimingDurationD(key string, dimensions map[string]string, v time.Duration)
	NewDimensionalSubStatter(dimensions map[string]string) TaggingSubStatter
}

// Statsd does statsd metrics to a metric registry
// Namespacing picked from https://github.com/statsd/statsd/blob/master/docs/namespacing.md
type Statsd struct {
	Registry   metrics.BaseRegistry
	prefix     string
	dimensions map[string]string
}

func (s *Statsd) Inc(key string, v int64, _ float32) error {
	metricsext.Counter(s.Registry, "counters."+s.metricName(key), nil).Observe(float64(v))
	return nil
}

func (s *Statsd) Dec(key string, v int64, _ float32) error {
	// Cloudwatch is dumb and doing this will cause you to loose your p99 metrics
	metricsext.Counter(s.Registry, "counters."+s.metricName(key), nil).Observe(float64(-v))
	return nil
}

func (s *Statsd) Gauge(key string, v int64, _ float32) error {
	metricsext.Gauge(s.Registry, "gauges."+s.metricName(key), nil).Observe(float64(v))
	return nil
}

func (s *Statsd) GaugeDelta(key string, v int64, _ float32) error {
	return errors.New("gauge delta is unimplemented")
}

func (s *Statsd) Timing(key string, v int64, f float32) error {
	return s.TimingDuration(key, time.Duration(v*time.Millisecond.Nanoseconds()), f)
}

func (s *Statsd) TimingDuration(key string, v time.Duration, _ float32) error {
	metricsext.Duration(s.Registry, "timers."+s.metricName(key), nil).Observe(v)
	return nil
}

func (s *Statsd) IncD(key string, dimensions map[string]string, v int64) {
	metricsext.Counter(s.Registry, key, s.metricDimensions(dimensions, "counter")).Observe(float64(v))
}

func (s *Statsd) GaugeD(key string, dimensions map[string]string, v int64) {
	metricsext.Gauge(s.Registry, key, s.metricDimensions(dimensions, "gauge")).Observe(float64(v))
}

func (s *Statsd) TimingDurationD(key string, dimensions map[string]string, v time.Duration) {
	metricsext.Duration(s.Registry, s.metricName(key), s.metricDimensions(dimensions, "timer")).Observe(v)
}

func (s *Statsd) Set(string, string, float32) error {
	return errors.New("set is unimplemented")
}

func (s *Statsd) SetInt(string, int64, float32) error {
	return errors.New("set int is unimplemented")
}

func (s *Statsd) Raw(string, string, float32) error {
	return errors.New("raw is unimplemented")
}

func (s *Statsd) SetSamplerFunc(statsd.SamplerFunc) {
	// Not worth implementing
}

func (s *Statsd) metricName(key string) string {
	if len(s.prefix) <= 0 {
		return key
	}
	return s.prefix + "." + key
}

func (s *Statsd) metricDimensions(dimensions map[string]string, metricType string) map[string]string {
	newDimensions := make(map[string]string, len(s.dimensions)+len(dimensions)+1)
	for k, v := range s.dimensions {
		newDimensions[k] = v
	}
	for k, v := range dimensions {
		newDimensions[k] = v
	}
	newDimensions["MetricType"] = metricType
	return newDimensions
}

func (s *Statsd) NewSubStatter(key string) statsd.SubStatter {
	key = strings.Trim(key, ".")
	if len(key) <= 0 {
		return s
	}
	if s.prefix != "" {
		key = strings.Trim(s.prefix, ".") + "." + key
	}
	return &Statsd{
		Registry:   s.Registry,
		prefix:     key,
		dimensions: s.dimensions,
	}
}

func (s *Statsd) NewDimensionalSubStatter(dimensions map[string]string) TaggingSubStatter {
	newDimensions := make(map[string]string, len(s.dimensions)+len(dimensions))
	for k, v := range s.dimensions {
		newDimensions[k] = v
	}
	for k, v := range dimensions {
		newDimensions[k] = v
	}
	return &Statsd{
		Registry:   s.Registry,
		prefix:     s.prefix,
		dimensions: newDimensions,
	}
}

var _ statsd.SubStatter = &Statsd{}


type TelemetryStatsdShim interface {
	ExtendedSubStatter
	ServerHooks() *twirp.ServerHooks
}

// TelemetryStatsdShim handles dual sending to statsd as well as Telemetry
// Namespacing picked from https://github.com/statsd/statsd/blob/master/docs/namespacing.md
type telemetryStatsdShim struct {
	Statsd
	metricsMiddleware  twirpmetric.Server
	sampleReporter telemetry.SampleReporter
}

func (t *telemetryStatsdShim) Inc(key string, v int64, _ float32) error {
	t.sampleReporter.Report("counters."+t.metricName(key), float64(v), "Count")
	return nil
}

func (t *telemetryStatsdShim) Dec(key string, v int64, _ float32) error {
	t.sampleReporter.Report("counters."+t.metricName(key), float64(-v), "Count")
	return nil
}

func (t *telemetryStatsdShim) Gauge(key string, v int64, _ float32) error {
	t.sampleReporter.Report("gauges."+t.metricName(key), float64(v), "Count")
	return nil
}

func (t *telemetryStatsdShim) GaugeDelta(key string, v int64, _ float32) error {
	return errors.New("gauge delta is unimplemented")
}

func (t *telemetryStatsdShim) Timing(key string, v int64, f float32) error {
	t.TimingDuration("timers."+t.metricName(key), time.Duration(v*time.Millisecond.Nanoseconds()), f)
	return nil
}

func (t *telemetryStatsdShim) TimingDuration(key string, v time.Duration, _ float32) error {
	t.sampleReporter.ReportDurationSample("timers."+t.metricName(key), v)
	return nil
}

func (t *telemetryStatsdShim) IncD(key string, dimensions map[string]string, v int64) {
	newDimensions := t.metricDimensions(dimensions, "counter")
	reporter := t.sampleReporter
	reporter.Dimensions = newDimensions
	reporter.Report(key, float64(v), "Count")
}

func (t *telemetryStatsdShim) GaugeD(key string, dimensions map[string]string, v int64) {
	newDimensions := t.metricDimensions(dimensions, "gauge")
	reporter := t.sampleReporter
	reporter.Dimensions = newDimensions
	reporter.Report(key, float64(v), "Count")
}

func (t *telemetryStatsdShim) TimingDurationD(key string, dimensions map[string]string, v time.Duration) {
	newDimensions := t.metricDimensions(dimensions, "timer")
	reporter := t.sampleReporter
	reporter.Dimensions = newDimensions
	reporter.ReportDurationSample(key, v)
}

func (t *telemetryStatsdShim) NewSubStatter(key string) statsd.SubStatter {
	key = strings.Trim(key, ".")
	if len(key) <= 0 {
		return t
	}
	if t.prefix != "" {
		key = strings.Trim(t.prefix, ".") + "." + key
	}
	return &telemetryStatsdShim{
		Statsd{
			Registry:   t.Registry,
			prefix:     key,
			dimensions: t.dimensions,

		},
		t.metricsMiddleware,
		t.sampleReporter,
	}
}

func (t *telemetryStatsdShim) NewDimensionalSubStatter(dimensions map[string]string) TaggingSubStatter {
	// just copy pasta here, as type assertion from t.Statsd.NewSubStatter response could potentially fail, leading to swallowed errors
	newDimensions := make(map[string]string, len(t.dimensions)+len(dimensions))
	for k, v := range t.dimensions {
		newDimensions[k] = v
	}
	for k, v := range dimensions {
		newDimensions[k] = v
	}

	sampleReporter := t.sampleReporter
	sampleReporter.Dimensions = newDimensions

	return &telemetryStatsdShim{
		Statsd{
			Registry:   t.Registry,
			prefix:     t.prefix,
			dimensions: newDimensions,

		},
		t.metricsMiddleware,
		sampleReporter,
	}
}

func (t *telemetryStatsdShim) ServerHooks() *twirp.ServerHooks {
	return t.metricsMiddleware.ServerHooks()
}

func NewTelemetryStatsdShim(registry metrics.BaseRegistry, prefix string, dimensions map[string]string, tPid identifier.ProcessIdentifier) TelemetryStatsdShim {
	sender := cw.NewUnbuffered(&tPid, nil)
	sampleObserver := telemetry.NewBufferedAggregator(30 * time.Second, 100000, time.Minute, sender, nil)
	sampleReporter := telemetry.SampleReporter{
		SampleBuilder:  telemetry.SampleBuilder{ProcessIdentifier: tPid},
		SampleObserver: sampleObserver,
	}
	sampleReporter.Dimensions = dimensions

	metricsOpMonitor := &metricsmiddleware.OperationMonitor{
		SampleReporter: sampleReporter,
		AutoFlush:      false,
	}
	opStarter := &operation.Starter{OpMonitors: []operation.OpMonitor{metricsOpMonitor}}
	metricsMiddleware := twirpmetric.Server{Starter: opStarter}

	return &telemetryStatsdShim{
		Statsd{
			Registry:   registry,
			prefix:     prefix,
			dimensions: dimensions,

		},
		metricsMiddleware,
		sampleReporter,
	}
}

var _ ExtendedSubStatter = &telemetryStatsdShim{}

type TelemetryStatterShim interface {
	TelemetryStatsdShim
	SetPrefix(string)
	Close() error
}

type telemetryStatterShim struct {
	telemetryStatsdShim
}

func (t *telemetryStatterShim) SetPrefix(prefix string) {
	t.telemetryStatsdShim.prefix = prefix
}

func (t *telemetryStatterShim) Close() error {
	return nil
}

func NewTelemetryStatterShim(registry metrics.BaseRegistry, prefix string, dimensions map[string]string, tPid identifier.ProcessIdentifier) TelemetryStatterShim {
	embeddedShim := NewTelemetryStatsdShim(registry, prefix, dimensions, tPid)
	return &telemetryStatterShim {
		*embeddedShim.(*telemetryStatsdShim),
	}

}

var _ TelemetryStatterShim = &telemetryStatterShim{}

