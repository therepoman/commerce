package zuma

import (
	"bytes"
	"context"
	"encoding/json"

	"code.justin.tv/chat/zuma/app/api"
	"code.justin.tv/foundation/twitchclient"
)

func (c *client) CreateContainer(ctx context.Context, params api.CreateContainerRequest, reqOpts *twitchclient.ReqOpts) (api.CreateContainerResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.CreateContainerResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/maap/containers/create", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.CreateContainerResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.maap.create_container",
		StatSampleRate: defaultStatSampleRate,
	})

	var decoded api.CreateContainerResponse
	if _, err := c.DoJSON(ctx, &decoded, req, combinedReqOpts); err != nil {
		return api.CreateContainerResponse{}, err
	}
	return decoded, nil
}
