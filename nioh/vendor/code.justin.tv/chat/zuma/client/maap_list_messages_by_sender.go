package zuma

import (
	"bytes"
	"context"
	"encoding/json"

	"code.justin.tv/chat/zuma/app/api"
	"code.justin.tv/foundation/twitchclient"
)

func (c *client) ListMessagesBySender(ctx context.Context, params api.ListMessagesBySenderRequest, reqOpts *twitchclient.ReqOpts) (api.ListMessagesBySenderResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.ListMessagesBySenderResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/maap/messages/list_by_sender", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.ListMessagesBySenderResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.maap.list_messages_by_sender",
		StatSampleRate: defaultStatSampleRate,
	})

	var decoded api.ListMessagesBySenderResponse
	if _, err := c.DoJSON(ctx, &decoded, req, combinedReqOpts); err != nil {
		return api.ListMessagesBySenderResponse{}, err
	}
	return decoded, nil
}
