package zuma

import (
	"bytes"
	"context"
	"encoding/json"

	"code.justin.tv/chat/zuma/app/api"
	"code.justin.tv/foundation/twitchclient"
)

func (c *client) UpdateContainerView(ctx context.Context, params api.UpdateContainerViewRequest, reqOpts *twitchclient.ReqOpts) (api.UpdateContainerViewResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.UpdateContainerViewResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/maap/container_views/update", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.UpdateContainerViewResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.maap.update_container_view",
		StatSampleRate: defaultStatSampleRate,
	})

	var decoded api.UpdateContainerViewResponse
	if _, err := c.DoJSON(ctx, &decoded, req, combinedReqOpts); err != nil {
		return api.UpdateContainerViewResponse{}, err
	}
	return decoded, nil
}
