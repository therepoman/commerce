package zuma

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"

	"code.justin.tv/chat/zuma/app/api"
	"code.justin.tv/foundation/twitchclient"
)

const (
	defaultStatSampleRate = 0.1
	defaultTimingXactName = "zuma"
)

// Client is the interface for the Zuma go client
type Client interface {
	GetMod(ctx context.Context, channelID, userID string, reqOpts *twitchclient.ReqOpts) (api.GetModResponse, error)
	// ListMods is deprecated. ListModsV2 shoudld be used instead
	ListMods(ctx context.Context, channelID string, reqOpts *twitchclient.ReqOpts) (api.ListModsResponse, error)
	ListModsV2(ctx context.Context, channelID, cursor string, limit int, reqOpts *twitchclient.ReqOpts) (api.ListModsV2Response, error)
	AddMod(ctx context.Context, channelID, targetUserID, requestingUserID string, reqOpts *twitchclient.ReqOpts) (api.AddModResponse, error)
	RemoveMod(ctx context.Context, channelID, targetUserID, requestingUserID string, reqOpts *twitchclient.ReqOpts) (api.RemoveModResponse, error)

	GetUserChatProperties(ctx context.Context, params api.UserChatPropertiesRequest, reqOpts *twitchclient.ReqOpts) (api.UserChatPropertiesResponse, error)
	ModifyUserChatProperties(ctx context.Context, params api.ModifyUserChatPropertiesRequest, reqOpts *twitchclient.ReqOpts) error

	GetVIP(ctx context.Context, channelID, userID string, reqOpts *twitchclient.ReqOpts) (api.GetVIPResponse, error)
	ListVIPs(ctx context.Context, channelID, cursor string, limit int, reqOpts *twitchclient.ReqOpts) (api.ListVIPsResponse, error)
	AddVIP(ctx context.Context, channelID, targetUserID, requestingUserID string, reqOpts *twitchclient.ReqOpts) (api.AddVIPResponse, error)
	RemoveVIP(ctx context.Context, channelID, targetUserID, requestingUserID string, reqOpts *twitchclient.ReqOpts) (api.RemoveVIPResponse, error)

	ListGameFollows(ctx context.Context, params api.ListGameFollowsRequest, reqOpts *twitchclient.ReqOpts) (api.ListGameFollowsResponse, error)
	ListGameFollowsLive(ctx context.Context, params api.ListGameFollowsLiveRequest, reqOpts *twitchclient.ReqOpts) (api.ListGameFollowsLiveResponse, error)
	CountGameFollows(ctx context.Context, params api.CountGameFollowsRequest, reqOpts *twitchclient.ReqOpts) (api.CountGameFollowsResponse, error)
	GetGameFollow(ctx context.Context, params api.GameFollowRequest, reqOpts *twitchclient.ReqOpts) (api.GameFollowResponse, error)
	FollowGame(ctx context.Context, params api.GameFollowRequest, reqOpts *twitchclient.ReqOpts) (api.GameFollowResponse, error)
	UnfollowGame(ctx context.Context, params api.GameFollowRequest, reqOpts *twitchclient.ReqOpts) (api.GameFollowResponse, error)
	CountGameFollowers(ctx context.Context, params api.CountGameFollowersRequest, reqOpts *twitchclient.ReqOpts) (api.CountGameFollowersResponse, error)

	CreateCommunity(ctx context.Context, params api.CreateCommunityRequest, reqOpts *twitchclient.ReqOpts) (api.CreateCommunityResponse, error)
	GetCommunityIDByName(ctx context.Context, params api.GetCommunityByNameRequest, reqOpts *twitchclient.ReqOpts) (api.GetCommunityByNameResponse, error)
	BulkGetCommunityIDByGameName(ctx context.Context, params api.BulkGetCommunitiesByGameNameRequest, reqOpts *twitchclient.ReqOpts) (api.BulkGetCommunitiesByGameNameResponse, error)
	GetCommunitySettings(ctx context.Context, params api.GetCommunitySettingsRequest, reqOpts *twitchclient.ReqOpts) (api.GetCommunitySettingsResponse, error)
	InternalGetCommunitySettings(ctx context.Context, params api.GetCommunitySettingsRequest, reqOpts *twitchclient.ReqOpts) (api.GetCommunitySettingsResponse, error)
	SetCommunitySettings(ctx context.Context, params api.SetCommunitySettingsRequest, reqOpts *twitchclient.ReqOpts) error
	BulkGetCommunitySettings(ctx context.Context, params api.BulkGetCommunitySettingsRequest, reqOpts *twitchclient.ReqOpts) (api.BulkGetCommunitySettingsResponse, error)
	BulkGetCommunitySettingsByName(ctx context.Context, params api.BulkGetCommunitySettingsByNameRequest, reqOpts *twitchclient.ReqOpts) (api.BulkGetCommunitySettingsResponse, error)

	TopCommunities(ctx context.Context, params api.TopCommunitiesRequest, reqOpts *twitchclient.ReqOpts) (api.TopCommunitiesResponse, error)

	ReportCommunity(ctx context.Context, params api.ReportCommunityRequest, reqOpts *twitchclient.ReqOpts) error
	TOSBanCommunity(ctx context.Context, params api.TOSBanCommunityRequest, reqOpts *twitchclient.ReqOpts) error

	CreateCommunityImageUploadURL(ctx context.Context, params api.CreateCommunityImageUploadURLRequest, reqOpts *twitchclient.ReqOpts) (api.CreateCommunityImageUploadURLResponse, error)
	UploadCommunityImage(ctx context.Context, params api.UploadCommunityImageRequest, reqOpts *twitchclient.ReqOpts) error
	RemoveCommunityImage(ctx context.Context, params api.RemoveCommunityImageRequest, reqOpts *twitchclient.ReqOpts) error

	GetCommunityMod(ctx context.Context, params api.GetCommunityModRequest, reqOpts *twitchclient.ReqOpts) (api.GetCommunityModResponse, error)
	ListCommunityMods(ctx context.Context, params api.ListCommunityModsRequest, reqOpts *twitchclient.ReqOpts) (api.ListCommunityModsResponse, error)
	AddCommunityMod(ctx context.Context, params api.AddCommunityModRequest, reqOpts *twitchclient.ReqOpts) error
	RemoveCommunityMod(ctx context.Context, params api.RemoveCommunityModRequest, reqOpts *twitchclient.ReqOpts) error

	AddCommunityBan(ctx context.Context, params api.AddCommunityBanRequest, reqOpts *twitchclient.ReqOpts) error
	GetCommunityBan(ctx context.Context, params api.GetCommunityBanRequest, reqOpts *twitchclient.ReqOpts) (api.GetCommunityBanResponse, error)
	BulkGetCommunityBans(ctx context.Context, params api.BulkGetCommunityBansRequest, reqOpts *twitchclient.ReqOpts) (api.BulkGetCommunityBansResponse, error)
	ListCommunityBans(ctx context.Context, params api.ListCommunityBansRequest, reqOpts *twitchclient.ReqOpts) (api.ListCommunityBansResponse, error)
	RemoveCommunityBan(ctx context.Context, params api.RemoveCommunityBanRequest, reqOpts *twitchclient.ReqOpts) error

	AddCommunityTimeout(ctx context.Context, params api.AddCommunityTimeoutRequest, reqOpts *twitchclient.ReqOpts) error
	GetCommunityTimeout(ctx context.Context, params api.GetCommunityTimeoutRequest, reqOpts *twitchclient.ReqOpts) (api.GetCommunityTimeoutResponse, error)
	BulkGetCommunityTimeouts(ctx context.Context, params api.BulkGetCommunityTimeoutsRequest, reqOpts *twitchclient.ReqOpts) (api.BulkGetCommunityTimeoutsResponse, error)
	ListCommunityTimeouts(ctx context.Context, params api.ListCommunityTimeoutsRequest, reqOpts *twitchclient.ReqOpts) (api.ListCommunityTimeoutsResponse, error)
	RemoveCommunityTimeout(ctx context.Context, params api.RemoveCommunityTimeoutRequest, reqOpts *twitchclient.ReqOpts) error
	GetCommunityModerationLogs(ctx context.Context, params api.GetCommunityModerationActionsRequest, reqOpts *twitchclient.ReqOpts) (api.GetCommunityModerationActionsResponse, error)

	GetCommunityPermissions(ctx context.Context, params api.GetCommunityPermissionsRequest, reqOpts *twitchclient.ReqOpts) (api.GetCommunityPermissionsResponse, error)
	InternalGetCommunityPermissions(ctx context.Context, params api.GetCommunityPermissionsRequest, reqOpts *twitchclient.ReqOpts) (api.GetCommunityPermissionsResponse, error)

	GetChannelCommunity(ctx context.Context, params api.GetChannelCommunityRequest, reqOpts *twitchclient.ReqOpts) (api.GetChannelCommunityResponse, error)
	SetChannelCommunity(ctx context.Context, params api.SetChannelCommunityRequest, reqOpts *twitchclient.ReqOpts) error
	SetChannelCommunities(ctx context.Context, params api.SetChannelCommunitiesRequest, reqOpts *twitchclient.ReqOpts) error
	UnsetChannelCommunity(ctx context.Context, params api.UnsetChannelCommunityRequest, reqOpts *twitchclient.ReqOpts) error
	ReportChannelCommunity(ctx context.Context, params api.ReportChannelCommunityRequest, reqOpts *twitchclient.ReqOpts) error

	BulkGetChannelCommunities(ctx context.Context, params api.BulkGetChannelCommunitiesRequest, reqOpts *twitchclient.ReqOpts) (api.BulkGetChannelCommunitiesResponse, error)
	BulkGetChannelCommunitiesByIDs(ctx context.Context, params api.BulkGetChannelCommunitiesByIDsRequest, reqOpts *twitchclient.ReqOpts) (api.BulkGetChannelCommunitiesByIDsResponse, error)

	ListUserBlocks(ctx context.Context, params api.ListUserBlocksParams, reqOpts *twitchclient.ReqOpts) (api.ListUserBlocksResponse, error)
	AddUserBlock(ctx context.Context, params api.AddUserBlockParams, reqOpts *twitchclient.ReqOpts) (api.AddUserBlockResponse, error)
	RemoveUserBlock(ctx context.Context, params api.RemoveUserBlockParams, reqOpts *twitchclient.ReqOpts) error
	IsBlocked(ctx context.Context, params api.IsBlockedParams, opts *twitchclient.ReqOpts) (api.IsBlockedResponse, error)
	ListUserBlockers(ctx context.Context, params api.ListUserBlockersParams, opts *twitchclient.ReqOpts) (api.ListUserBlockersResponse, error)

	GetUserRoles(ctx context.Context, params api.UserRoleRequest, reqOpts *twitchclient.ReqOpts) (api.UserRoleResponse, error)

	GetCommunityFollower(ctx context.Context, params api.GetCommunityFollowerRequest, reqOpts *twitchclient.ReqOpts) (api.GetCommunityFollowerResponse, error)
	ListCommunityFollowers(ctx context.Context, params api.ListCommunityFollowersRequest, reqOpts *twitchclient.ReqOpts) (api.ListCommunityFollowersResponse, error)
	CountCommunityFollowers(ctx context.Context, params api.CountCommunityFollowersRequest, reqOpts *twitchclient.ReqOpts) (api.CountCommunityFollowersResponse, error)

	AddUserFollowedCommunity(ctx context.Context, params api.AddUserFollowedCommunityRequest, reqOpts *twitchclient.ReqOpts) error
	RemoveUserFollowedCommunity(ctx context.Context, params api.RemoveUserFollowedCommunityRequest, reqOpts *twitchclient.ReqOpts) error
	ListUserFollowedCommunities(ctx context.Context, params api.ListUserFollowedCommunitiesRequest, reqOpts *twitchclient.ReqOpts) (api.ListUserFollowedCommunitiesResponse, error)
	TopUserFollowedCommunities(ctx context.Context, params api.TopUserFollowedCommunitiesRequest, reqOpts *twitchclient.ReqOpts) (api.TopUserFollowedCommunitiesResponse, error)

	CreateGameCommunity(ctx context.Context, params api.CreateGameCommunityRequest, reqOpts *twitchclient.ReqOpts) error
	GetGameBannerImages(ctx context.Context, params api.GetGameBannerImagesRequest, reqOpts *twitchclient.ReqOpts) (api.GetGameBannerImagesResponse, error)

	CheckEmoticonsAccess(ctx context.Context, params api.CheckEmoticonsAccessRequest, reqOpts *twitchclient.ReqOpts) (api.CheckEmoticonsAccessResponse, error)

	// Messaging as a Platform (MaaP)

	// Deprecated: Use ExtractMessage()
	ParseMessage(ctx context.Context, params api.ParseMessageRequest, reqOpts *twitchclient.ReqOpts) (api.ParseMessageResponse, error)
	// Deprecated: Replaced with EnforceMessage
	ExtractMessage(ctx context.Context, params api.ExtractMessageRequest, reqOpts *twitchclient.ReqOpts) (api.ExtractMessageResponse, error)
	// EnforceMessage provides the same functionality as ExtractMessage but with better default behavior, a simpler api and other improvements
	EnforceMessage(ctx context.Context, params api.EnforceMessageRequest, reqOpts *twitchclient.ReqOpts) (api.EnforceMessageResponse, error)
	ListMessages(ctx context.Context, params api.ListMessagesRequest, reqOpts *twitchclient.ReqOpts) (api.ListMessagesResponse, error)
	ListMessagesBySender(ctx context.Context, params api.ListMessagesBySenderRequest, reqOpts *twitchclient.ReqOpts) (api.ListMessagesBySenderResponse, error)
	GetMessage(ctx context.Context, params api.GetMessageRequest, reqOpts *twitchclient.ReqOpts) (api.GetMessageResponse, error)
	CreateMessage(ctx context.Context, params api.CreateMessageRequest, reqOpts *twitchclient.ReqOpts) (api.CreateMessageResponse, error)
	EditMessage(ctx context.Context, params api.EditMessageRequest, reqOpts *twitchclient.ReqOpts) (api.EditMessageResponse, error)
	DeleteMessage(ctx context.Context, params api.DeleteMessageRequest, reqOpts *twitchclient.ReqOpts) (api.DeleteMessageResponse, error)
	ApproveMessage(ctx context.Context, params api.ApproveMessageRequest, reqOpts *twitchclient.ReqOpts) (api.ApproveMessageResponse, error)
	RejectMessage(ctx context.Context, params api.RejectMessageRequest, reqOpts *twitchclient.ReqOpts) (api.RejectMessageResponse, error)
	BulkUpdateMessages(ctx context.Context, params api.BulkUpdateMessagesRequest, reqOpts *twitchclient.ReqOpts) (api.BulkUpdateMessagesResponse, error)

	ListContainersByOwner(ctx context.Context, params api.ListContainersByOwnerRequest, reqOpts *twitchclient.ReqOpts) (api.ListContainersByOwnerResponse, error)
	GetContainer(ctx context.Context, params api.GetContainerRequest, reqOpts *twitchclient.ReqOpts) (api.GetContainerResponse, error)
	GetContainerByUniqueName(ctx context.Context, params api.GetContainerByUniqueNameRequest, reqOpts *twitchclient.ReqOpts) (api.GetContainerByUniqueNameResponse, error)
	CreateContainer(ctx context.Context, params api.CreateContainerRequest, reqOpts *twitchclient.ReqOpts) (api.CreateContainerResponse, error)
	UpdateContainer(ctx context.Context, params api.UpdateContainerRequest, reqOpts *twitchclient.ReqOpts) (api.UpdateContainerResponse, error)
	DeleteContainer(ctx context.Context, params api.DeleteContainerRequest, reqOpts *twitchclient.ReqOpts) (api.DeleteContainerResponse, error)

	ListContainerViewsByUser(ctx context.Context, params api.ListContainerViewsByUserRequest, reqOpts *twitchclient.ReqOpts) (api.ListContainerViewsByUserResponse, error)
	GetContainerView(ctx context.Context, params api.GetContainerViewRequest, reqOpts *twitchclient.ReqOpts) (api.GetContainerViewResponse, error)
	BulkGetContainerViews(ctx context.Context, params api.BulkGetContainerViewsRequest, reqOpts *twitchclient.ReqOpts) (api.BulkGetContainerViewsResponse, error)
	// UpdateContainerView updates or create a container view for a user + container.
	UpdateContainerView(ctx context.Context, params api.UpdateContainerViewRequest, reqOpts *twitchclient.ReqOpts) (api.UpdateContainerViewResponse, error)

	// Browse
	GetBrowseTop(ctx context.Context, params api.GetTopBrowseParams, reqOpts *twitchclient.ReqOpts) (api.GetTopBrowseResponse, error)

	// Rituals
	ListRitualTokensByChannel(ctx context.Context, params api.ListRitualTokensByChannelRequest, reqOpts *twitchclient.ReqOpts) (api.ListRitualTokensByChannelResponse, error)
	RequestEligibleRitualToken(ctx context.Context, params api.RequestEligibleRitualTokenRequest, reqOpts *twitchclient.ReqOpts) (api.RequestEligibleRitualTokenResponse, error)
	UpdateRitualToken(ctx context.Context, params api.UpdateRitualTokenRequest, reqOpts *twitchclient.ReqOpts) (api.UpdateRitualTokenResponse, error)

	// Entities
	AutoModCheckUsername(ctx context.Context, params api.AutoModCheckUsernameRequest, reqOpts *twitchclient.ReqOpts) (api.AutomodCheckUsernameResponse, error)
	ChatRoomProperties(ctx context.Context, params api.ChatRoomPropertiesRequest, reqOpts *twitchclient.ReqOpts) (api.ChatRoomPropertiesResponse, error)
}

type client struct {
	twitchclient.Client
}

// NewClient creates a new Zuma go client
func NewClient(conf twitchclient.ClientConf) (Client, error) {
	if conf.TimingXactName == "" {
		conf.TimingXactName = defaultTimingXactName
	}
	twitchClient, err := twitchclient.NewClient(conf)
	return &client{twitchClient}, err
}

func (c *client) GetMod(ctx context.Context, channelID, userID string, reqOpts *twitchclient.ReqOpts) (api.GetModResponse, error) {
	bodyBytes, err := json.Marshal(api.GetModRequest{
		ChannelID: channelID,
		UserID:    userID,
	})
	if err != nil {
		return api.GetModResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/mods/get", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.GetModResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.mods.get",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return api.GetModResponse{}, err
	}
	defer close(resp.Body)

	if resp.StatusCode >= 400 {
		return api.GetModResponse{}, c.errorFromFailedRequest(resp)
	}

	data := api.GetModResponse{}
	err = json.NewDecoder(resp.Body).Decode(&data)
	if err != nil {
		return api.GetModResponse{}, fmt.Errorf("can't read response from zuma: %s", err)
	}

	return data, nil
}

func (c *client) ListMods(ctx context.Context, channelID string, reqOpts *twitchclient.ReqOpts) (api.ListModsResponse, error) {
	bodyBytes, err := json.Marshal(api.ListModsRequest{
		ChannelID: channelID,
	})
	if err != nil {
		return api.ListModsResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/mods/list", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.ListModsResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.mods.list",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return api.ListModsResponse{}, err
	}
	defer close(resp.Body)

	if resp.StatusCode >= 400 {
		return api.ListModsResponse{}, c.errorFromFailedRequest(resp)
	}

	data := api.ListModsResponse{}
	err = json.NewDecoder(resp.Body).Decode(&data)
	if err != nil {
		return api.ListModsResponse{}, fmt.Errorf("can't read response from zuma: %s", err)
	}

	return data, nil
}

func (c *client) ListModsV2(ctx context.Context, channelID, cursor string, limit int, reqOpts *twitchclient.ReqOpts) (api.ListModsV2Response, error) {
	bodyBytes, err := json.Marshal(api.ListModsV2Request{
		ChannelID: channelID,
		Cursor:    cursor,
		Limit:     limit,
	})
	if err != nil {
		return api.ListModsV2Response{}, err
	}

	req, err := c.NewRequest("POST", "/v2/mods/list", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.ListModsV2Response{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.mods.list_v2",
		StatSampleRate: defaultStatSampleRate,
	})

	decoded := api.ListModsV2Response{}
	if _, err := c.DoJSON(ctx, &decoded, req, combinedReqOpts); err != nil {
		return api.ListModsV2Response{}, err
	}

	return decoded, nil
}

func (c *client) AddMod(ctx context.Context, channelID, targetUserID, requestingUserID string, reqOpts *twitchclient.ReqOpts) (api.AddModResponse, error) {
	bodyBytes, err := json.Marshal(api.AddModRequest{
		ChannelID:        channelID,
		TargetUserID:     targetUserID,
		RequestingUserID: requestingUserID,
	})
	if err != nil {
		return api.AddModResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/mods/add", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.AddModResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.mods.add",
		StatSampleRate: defaultStatSampleRate,
	})
	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return api.AddModResponse{}, err
	}
	defer close(resp.Body)

	var decoded api.AddModResponse
	err = decodeErrorCodeResponse(ctx, resp, &decoded)
	if err != nil {
		return api.AddModResponse{}, err
	}

	return decoded, nil
}

func (c *client) RemoveMod(ctx context.Context, channelID, targetUserID, requestingUserID string, reqOpts *twitchclient.ReqOpts) (api.RemoveModResponse, error) {
	bodyBytes, err := json.Marshal(api.AddModRequest{
		ChannelID:        channelID,
		TargetUserID:     targetUserID,
		RequestingUserID: requestingUserID,
	})
	if err != nil {
		return api.RemoveModResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/mods/remove", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.RemoveModResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.mods.remove",
		StatSampleRate: defaultStatSampleRate,
	})
	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return api.RemoveModResponse{}, err
	}
	defer close(resp.Body)

	var decoded api.RemoveModResponse
	err = decodeErrorCodeResponse(ctx, resp, &decoded)
	if err != nil {
		return api.RemoveModResponse{}, err
	}

	return decoded, nil
}

func (c *client) GetVIP(ctx context.Context, channelID, userID string, reqOpts *twitchclient.ReqOpts) (api.GetVIPResponse, error) {
	bodyBytes, err := json.Marshal(api.GetModRequest{
		ChannelID: channelID,
		UserID:    userID,
	})
	if err != nil {
		return api.GetVIPResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/vips/get", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.GetVIPResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.vips.get",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return api.GetVIPResponse{}, err
	}
	defer close(resp.Body)

	if resp.StatusCode >= 400 {
		return api.GetVIPResponse{}, c.errorFromFailedRequest(resp)
	}

	data := api.GetVIPResponse{}
	err = json.NewDecoder(resp.Body).Decode(&data)
	if err != nil {
		return api.GetVIPResponse{}, fmt.Errorf("can't read response from zuma: %s", err)
	}

	return data, nil
}

func (c *client) ListVIPs(ctx context.Context, channelID, cursor string, limit int, reqOpts *twitchclient.ReqOpts) (api.ListVIPsResponse, error) {
	bodyBytes, err := json.Marshal(api.ListVIPsRequest{
		ChannelID: channelID,
		Cursor:    cursor,
		Limit:     limit,
	})
	if err != nil {
		return api.ListVIPsResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/vips/list", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.ListVIPsResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.vips.list",
		StatSampleRate: defaultStatSampleRate,
	})

	decoded := api.ListVIPsResponse{}
	if _, err := c.DoJSON(ctx, &decoded, req, combinedReqOpts); err != nil {
		return api.ListVIPsResponse{}, err
	}

	return decoded, nil
}

func (c *client) AddVIP(ctx context.Context, channelID, targetUserID, requestingUserID string, reqOpts *twitchclient.ReqOpts) (api.AddVIPResponse, error) {
	bodyBytes, err := json.Marshal(api.AddModRequest{
		ChannelID:        channelID,
		TargetUserID:     targetUserID,
		RequestingUserID: requestingUserID,
	})
	if err != nil {
		return api.AddVIPResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/vips/add", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.AddVIPResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.vips.add",
		StatSampleRate: defaultStatSampleRate,
	})
	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return api.AddVIPResponse{}, err
	}
	defer close(resp.Body)

	var decoded api.AddVIPResponse
	err = decodeErrorCodeResponse(ctx, resp, &decoded)
	if err != nil {
		return api.AddVIPResponse{}, err
	}

	return decoded, nil
}

func (c *client) RemoveVIP(ctx context.Context, channelID, targetUserID, requestingUserID string, reqOpts *twitchclient.ReqOpts) (api.RemoveVIPResponse, error) {
	bodyBytes, err := json.Marshal(api.AddModRequest{
		ChannelID:        channelID,
		TargetUserID:     targetUserID,
		RequestingUserID: requestingUserID,
	})
	if err != nil {
		return api.RemoveVIPResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/vips/remove", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.RemoveVIPResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.vips.remove",
		StatSampleRate: defaultStatSampleRate,
	})
	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return api.RemoveVIPResponse{}, err
	}
	defer close(resp.Body)

	var decoded api.RemoveVIPResponse
	err = decodeErrorCodeResponse(ctx, resp, &decoded)
	if err != nil {
		return api.RemoveVIPResponse{}, err
	}

	return decoded, nil
}

func (c *client) ListGameFollows(ctx context.Context, params api.ListGameFollowsRequest, reqOpts *twitchclient.ReqOpts) (api.ListGameFollowsResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.ListGameFollowsResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/game_follows/list", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.ListGameFollowsResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.game_follows.list",
		StatSampleRate: defaultStatSampleRate,
	})

	var decoded api.ListGameFollowsResponse
	if _, err := c.DoJSON(ctx, &decoded, req, combinedReqOpts); err != nil {
		return api.ListGameFollowsResponse{}, err
	}

	return decoded, nil
}

func (c *client) ListGameFollowsLive(ctx context.Context, params api.ListGameFollowsLiveRequest, reqOpts *twitchclient.ReqOpts) (api.ListGameFollowsLiveResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.ListGameFollowsLiveResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/game_follows/live", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.ListGameFollowsLiveResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.game_follows.live",
		StatSampleRate: defaultStatSampleRate,
	})

	var decoded api.ListGameFollowsLiveResponse
	if _, err := c.DoJSON(ctx, &decoded, req, combinedReqOpts); err != nil {
		return api.ListGameFollowsLiveResponse{}, err
	}

	return decoded, nil
}

func (c *client) CountGameFollows(ctx context.Context, params api.CountGameFollowsRequest, reqOpts *twitchclient.ReqOpts) (api.CountGameFollowsResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.CountGameFollowsResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/game_follows/count", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.CountGameFollowsResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.game_follows.count",
		StatSampleRate: defaultStatSampleRate,
	})

	var decoded api.CountGameFollowsResponse
	if _, err := c.DoJSON(ctx, &decoded, req, combinedReqOpts); err != nil {
		return api.CountGameFollowsResponse{}, err
	}

	return decoded, nil
}

func (c *client) GetGameFollow(ctx context.Context, params api.GameFollowRequest, reqOpts *twitchclient.ReqOpts) (api.GameFollowResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.GameFollowResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/game_follows/get", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.GameFollowResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.game_follows.get",
		StatSampleRate: defaultStatSampleRate,
	})

	var decoded api.GameFollowResponse
	if _, err := c.DoJSON(ctx, &decoded, req, combinedReqOpts); err != nil {
		return api.GameFollowResponse{}, err
	}

	return decoded, nil
}

func (c *client) FollowGame(ctx context.Context, params api.GameFollowRequest, reqOpts *twitchclient.ReqOpts) (api.GameFollowResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.GameFollowResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/game_follows/create", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.GameFollowResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.game_follows.create",
		StatSampleRate: defaultStatSampleRate,
	})

	var decoded api.GameFollowResponse
	if _, err := c.DoJSON(ctx, &decoded, req, combinedReqOpts); err != nil {
		return api.GameFollowResponse{}, err
	}

	return decoded, nil
}

func (c *client) UnfollowGame(ctx context.Context, params api.GameFollowRequest, reqOpts *twitchclient.ReqOpts) (api.GameFollowResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.GameFollowResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/game_follows/delete", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.GameFollowResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.game_follows.delete",
		StatSampleRate: defaultStatSampleRate,
	})

	var decoded api.GameFollowResponse
	if _, err := c.DoJSON(ctx, &decoded, req, combinedReqOpts); err != nil {
		return api.GameFollowResponse{}, err
	}

	return decoded, nil
}

func (c *client) CountGameFollowers(ctx context.Context, params api.CountGameFollowersRequest, reqOpts *twitchclient.ReqOpts) (api.CountGameFollowersResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.CountGameFollowersResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/game_followers/count", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.CountGameFollowersResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.game_followers.count",
		StatSampleRate: defaultStatSampleRate,
	})

	var decoded api.CountGameFollowersResponse
	if _, err := c.DoJSON(ctx, &decoded, req, combinedReqOpts); err != nil {
		return api.CountGameFollowersResponse{}, err
	}

	return decoded, nil
}

func (c *client) CreateCommunity(ctx context.Context, params api.CreateCommunityRequest, reqOpts *twitchclient.ReqOpts) (api.CreateCommunityResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.CreateCommunityResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/communities/create", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.CreateCommunityResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.communities.create",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return api.CreateCommunityResponse{}, err
	}
	defer close(resp.Body)

	if resp.StatusCode >= 400 {
		return api.CreateCommunityResponse{}, c.errorFromFailedRequest(resp)
	}

	data := api.CreateCommunityResponse{}
	err = json.NewDecoder(resp.Body).Decode(&data)
	if err != nil {
		return api.CreateCommunityResponse{}, fmt.Errorf("can't read response from zuma: %s", err)
	}

	return data, nil
}

func (c *client) GetCommunityIDByName(ctx context.Context, params api.GetCommunityByNameRequest, reqOpts *twitchclient.ReqOpts) (api.GetCommunityByNameResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.GetCommunityByNameResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/communities/get_by_name", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.GetCommunityByNameResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.communities.get_by_name",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return api.GetCommunityByNameResponse{}, err
	}
	defer close(resp.Body)

	if resp.StatusCode >= 400 {
		return api.GetCommunityByNameResponse{}, c.errorFromFailedRequest(resp)
	}

	data := api.GetCommunityByNameResponse{}
	err = json.NewDecoder(resp.Body).Decode(&data)
	if err != nil {
		return api.GetCommunityByNameResponse{}, fmt.Errorf("can't read response from zuma: %s", err)
	}

	return data, nil
}

func (c *client) BulkGetCommunityIDByGameName(ctx context.Context, params api.BulkGetCommunitiesByGameNameRequest, reqOpts *twitchclient.ReqOpts) (api.BulkGetCommunitiesByGameNameResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.BulkGetCommunitiesByGameNameResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/communities/bulk_get_by_game_name", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.BulkGetCommunitiesByGameNameResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.communities.bulk_get_by_game_name",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return api.BulkGetCommunitiesByGameNameResponse{}, err
	}
	defer close(resp.Body)

	if resp.StatusCode >= 400 {
		return api.BulkGetCommunitiesByGameNameResponse{}, c.errorFromFailedRequest(resp)
	}

	data := api.BulkGetCommunitiesByGameNameResponse{}
	err = json.NewDecoder(resp.Body).Decode(&data)
	if err != nil {
		return api.BulkGetCommunitiesByGameNameResponse{}, fmt.Errorf("can't read response from zuma: %s", err)
	}

	return data, nil

}

func (c *client) ReportCommunity(ctx context.Context, params api.ReportCommunityRequest, reqOpts *twitchclient.ReqOpts) error {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return err
	}

	req, err := c.NewRequest("POST", "/v1/communities/report", bytes.NewReader(bodyBytes))
	if err != nil {
		return err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.communities.report",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}
	defer close(resp.Body)

	if resp.StatusCode >= 400 {
		return c.errorFromFailedRequest(resp)
	}

	return nil
}

func (c *client) TOSBanCommunity(ctx context.Context, params api.TOSBanCommunityRequest, reqOpts *twitchclient.ReqOpts) error {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return err
	}

	req, err := c.NewRequest("POST", "/v1/communities/tos_ban", bytes.NewReader(bodyBytes))
	if err != nil {
		return err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.communities.tos_ban",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}
	defer close(resp.Body)

	if resp.StatusCode >= 400 {
		return c.errorFromFailedRequest(resp)
	}

	return nil
}

func (c *client) GetCommunitySettings(ctx context.Context, params api.GetCommunitySettingsRequest, reqOpts *twitchclient.ReqOpts) (api.GetCommunitySettingsResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.GetCommunitySettingsResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/communities/settings/get", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.GetCommunitySettingsResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.communities.get_settings",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return api.GetCommunitySettingsResponse{}, err
	}
	defer close(resp.Body)

	if resp.StatusCode >= 400 {
		return api.GetCommunitySettingsResponse{}, c.errorFromFailedRequest(resp)
	}

	data := api.GetCommunitySettingsResponse{}
	err = json.NewDecoder(resp.Body).Decode(&data)
	if err != nil {
		return api.GetCommunitySettingsResponse{}, fmt.Errorf("can't read response from zuma: %s", err)
	}

	return data, nil
}

func (c *client) InternalGetCommunitySettings(ctx context.Context, params api.GetCommunitySettingsRequest, reqOpts *twitchclient.ReqOpts) (api.GetCommunitySettingsResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.GetCommunitySettingsResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/communities/settings/internal_get", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.GetCommunitySettingsResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.communities.internal_get_settings",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return api.GetCommunitySettingsResponse{}, err
	}
	defer close(resp.Body)

	if resp.StatusCode >= 400 {
		return api.GetCommunitySettingsResponse{}, c.errorFromFailedRequest(resp)
	}

	data := api.GetCommunitySettingsResponse{}
	err = json.NewDecoder(resp.Body).Decode(&data)
	if err != nil {
		return api.GetCommunitySettingsResponse{}, fmt.Errorf("can't read response from zuma: %s", err)
	}

	return data, nil
}

func (c *client) SetCommunitySettings(ctx context.Context, params api.SetCommunitySettingsRequest, reqOpts *twitchclient.ReqOpts) error {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return err
	}

	req, err := c.NewRequest("POST", "/v1/communities/settings/set", bytes.NewReader(bodyBytes))
	if err != nil {
		return err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.communities.set_settings",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}
	defer close(resp.Body)

	if resp.StatusCode >= 400 {
		return c.errorFromFailedRequest(resp)
	}

	return nil
}

func (c *client) TopCommunities(ctx context.Context, params api.TopCommunitiesRequest, reqOpts *twitchclient.ReqOpts) (api.TopCommunitiesResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.TopCommunitiesResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/communities/top", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.TopCommunitiesResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.communities.top",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return api.TopCommunitiesResponse{}, err
	}
	defer close(resp.Body)

	if resp.StatusCode >= 400 {
		return api.TopCommunitiesResponse{}, c.errorFromFailedRequest(resp)
	}

	data := api.TopCommunitiesResponse{}
	err = json.NewDecoder(resp.Body).Decode(&data)
	if err != nil {
		return api.TopCommunitiesResponse{}, fmt.Errorf("can't read response from zuma: %s", err)
	}

	return data, nil
}

func (c *client) CreateCommunityImageUploadURL(ctx context.Context, params api.CreateCommunityImageUploadURLRequest, reqOpts *twitchclient.ReqOpts) (api.CreateCommunityImageUploadURLResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.CreateCommunityImageUploadURLResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/communities/images/upload/url", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.CreateCommunityImageUploadURLResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.communities.create_upload_url",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return api.CreateCommunityImageUploadURLResponse{}, err
	}
	defer close(resp.Body)

	if resp.StatusCode >= 400 {
		return api.CreateCommunityImageUploadURLResponse{}, c.errorFromFailedRequest(resp)
	}

	data := api.CreateCommunityImageUploadURLResponse{}
	err = json.NewDecoder(resp.Body).Decode(&data)
	if err != nil {
		return api.CreateCommunityImageUploadURLResponse{}, fmt.Errorf("can't read response from zuma: %s", err)
	}

	return data, nil
}

func (c *client) UploadCommunityImage(ctx context.Context, params api.UploadCommunityImageRequest, reqOpts *twitchclient.ReqOpts) error {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return err
	}

	req, err := c.NewRequest("POST", "/v1/communities/images/upload", bytes.NewReader(bodyBytes))
	if err != nil {
		return err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.communities.upload_image",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}
	defer close(resp.Body)

	if resp.StatusCode >= 400 {
		return c.errorFromFailedRequest(resp)
	}

	return nil
}

func (c *client) RemoveCommunityImage(ctx context.Context, params api.RemoveCommunityImageRequest, reqOpts *twitchclient.ReqOpts) error {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return err
	}

	req, err := c.NewRequest("POST", "/v1/communities/images/remove", bytes.NewReader(bodyBytes))
	if err != nil {
		return err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.communities.remove_image",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}
	defer close(resp.Body)

	if resp.StatusCode >= 400 {
		return c.errorFromFailedRequest(resp)
	}

	return nil
}

func (c *client) GetCommunityMod(ctx context.Context, params api.GetCommunityModRequest, reqOpts *twitchclient.ReqOpts) (api.GetCommunityModResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.GetCommunityModResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/communities/mods/get", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.GetCommunityModResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.communities.get_mod",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return api.GetCommunityModResponse{}, err
	}
	defer close(resp.Body)

	if resp.StatusCode >= 400 {
		return api.GetCommunityModResponse{}, c.errorFromFailedRequest(resp)
	}

	data := api.GetCommunityModResponse{}
	err = json.NewDecoder(resp.Body).Decode(&data)
	if err != nil {
		return api.GetCommunityModResponse{}, fmt.Errorf("can't read response from zuma: %s", err)
	}

	return data, nil
}

func (c *client) ListCommunityMods(ctx context.Context, params api.ListCommunityModsRequest, reqOpts *twitchclient.ReqOpts) (api.ListCommunityModsResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.ListCommunityModsResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/communities/mods/list", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.ListCommunityModsResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.communities.list_mods",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return api.ListCommunityModsResponse{}, err
	}
	defer close(resp.Body)

	if resp.StatusCode >= 400 {
		return api.ListCommunityModsResponse{}, c.errorFromFailedRequest(resp)
	}

	data := api.ListCommunityModsResponse{}
	err = json.NewDecoder(resp.Body).Decode(&data)
	if err != nil {
		return api.ListCommunityModsResponse{}, fmt.Errorf("can't read response from zuma: %s", err)
	}

	return data, nil
}

func (c *client) AddCommunityMod(ctx context.Context, params api.AddCommunityModRequest, reqOpts *twitchclient.ReqOpts) error {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return err
	}

	req, err := c.NewRequest("POST", "/v1/communities/mods/add", bytes.NewReader(bodyBytes))
	if err != nil {
		return err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.communities.add_mod",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}
	defer close(resp.Body)

	if resp.StatusCode >= 400 {
		return c.errorFromFailedRequest(resp)
	}

	return nil
}

func (c *client) RemoveCommunityMod(ctx context.Context, params api.RemoveCommunityModRequest, reqOpts *twitchclient.ReqOpts) error {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return err
	}

	req, err := c.NewRequest("POST", "/v1/communities/mods/remove", bytes.NewReader(bodyBytes))
	if err != nil {
		return err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.communities.remove_mod",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}
	defer close(resp.Body)

	if resp.StatusCode >= 400 {
		return c.errorFromFailedRequest(resp)
	}

	return nil
}

func (c *client) GetCommunityBan(ctx context.Context, params api.GetCommunityBanRequest, reqOpts *twitchclient.ReqOpts) (api.GetCommunityBanResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.GetCommunityBanResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/communities/bans/get", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.GetCommunityBanResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.communities.get_ban",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return api.GetCommunityBanResponse{}, err
	}
	defer close(resp.Body)

	if resp.StatusCode >= 400 {
		return api.GetCommunityBanResponse{}, c.errorFromFailedRequest(resp)
	}

	data := api.GetCommunityBanResponse{}
	err = json.NewDecoder(resp.Body).Decode(&data)
	if err != nil {
		return api.GetCommunityBanResponse{}, fmt.Errorf("can't read response from zuma: %s", err)
	}

	return data, nil
}

func (c *client) BulkGetCommunityBans(ctx context.Context, params api.BulkGetCommunityBansRequest, reqOpts *twitchclient.ReqOpts) (api.BulkGetCommunityBansResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.BulkGetCommunityBansResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/communities/bans/bulk_get", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.BulkGetCommunityBansResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.communities.bulk_get_bans",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return api.BulkGetCommunityBansResponse{}, err
	}
	defer close(resp.Body)

	if resp.StatusCode >= 400 {
		return api.BulkGetCommunityBansResponse{}, c.errorFromFailedRequest(resp)
	}

	data := api.BulkGetCommunityBansResponse{}
	err = json.NewDecoder(resp.Body).Decode(&data)
	if err != nil {
		return api.BulkGetCommunityBansResponse{}, fmt.Errorf("can't read response from zuma: %s", err)
	}

	return data, nil
}

func (c *client) ListCommunityBans(ctx context.Context, params api.ListCommunityBansRequest, reqOpts *twitchclient.ReqOpts) (api.ListCommunityBansResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.ListCommunityBansResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/communities/bans/list", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.ListCommunityBansResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.communities.list_bans",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return api.ListCommunityBansResponse{}, err
	}
	defer close(resp.Body)

	if resp.StatusCode >= 400 {
		return api.ListCommunityBansResponse{}, c.errorFromFailedRequest(resp)
	}

	data := api.ListCommunityBansResponse{}
	err = json.NewDecoder(resp.Body).Decode(&data)
	if err != nil {
		return api.ListCommunityBansResponse{}, fmt.Errorf("can't read response from zuma: %s", err)
	}

	return data, nil
}

func (c *client) AddCommunityBan(ctx context.Context, params api.AddCommunityBanRequest, reqOpts *twitchclient.ReqOpts) error {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return err
	}

	req, err := c.NewRequest("POST", "/v1/communities/bans/add", bytes.NewReader(bodyBytes))
	if err != nil {
		return err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.communities.add_ban",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}
	defer close(resp.Body)

	if resp.StatusCode >= 400 {
		return c.errorFromFailedRequest(resp)
	}

	return nil
}

func (c *client) RemoveCommunityBan(ctx context.Context, params api.RemoveCommunityBanRequest, reqOpts *twitchclient.ReqOpts) error {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return err
	}

	req, err := c.NewRequest("POST", "/v1/communities/bans/remove", bytes.NewReader(bodyBytes))
	if err != nil {
		return err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.communities.remove_ban",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}
	defer close(resp.Body)

	if resp.StatusCode >= 400 {
		return c.errorFromFailedRequest(resp)
	}

	return nil
}

func (c *client) GetCommunityTimeout(ctx context.Context, params api.GetCommunityTimeoutRequest, reqOpts *twitchclient.ReqOpts) (api.GetCommunityTimeoutResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.GetCommunityTimeoutResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/communities/timeouts/get", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.GetCommunityTimeoutResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.communities.get_timeout",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return api.GetCommunityTimeoutResponse{}, err
	}
	defer close(resp.Body)

	if resp.StatusCode >= 400 {
		return api.GetCommunityTimeoutResponse{}, c.errorFromFailedRequest(resp)
	}

	data := api.GetCommunityTimeoutResponse{}
	err = json.NewDecoder(resp.Body).Decode(&data)
	if err != nil {
		return api.GetCommunityTimeoutResponse{}, fmt.Errorf("can't read response from zuma: %s", err)
	}

	return data, nil
}

func (c *client) BulkGetCommunityTimeouts(ctx context.Context, params api.BulkGetCommunityTimeoutsRequest, reqOpts *twitchclient.ReqOpts) (api.BulkGetCommunityTimeoutsResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.BulkGetCommunityTimeoutsResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/communities/timeouts/bulk_get", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.BulkGetCommunityTimeoutsResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.communities.bulk_get_timeouts",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return api.BulkGetCommunityTimeoutsResponse{}, err
	}
	defer close(resp.Body)

	if resp.StatusCode >= 400 {
		return api.BulkGetCommunityTimeoutsResponse{}, c.errorFromFailedRequest(resp)
	}

	data := api.BulkGetCommunityTimeoutsResponse{}
	err = json.NewDecoder(resp.Body).Decode(&data)
	if err != nil {
		return api.BulkGetCommunityTimeoutsResponse{}, fmt.Errorf("can't read response from zuma: %s", err)
	}

	return data, nil
}

func (c *client) ListCommunityTimeouts(ctx context.Context, params api.ListCommunityTimeoutsRequest, reqOpts *twitchclient.ReqOpts) (api.ListCommunityTimeoutsResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.ListCommunityTimeoutsResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/communities/timeouts/list", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.ListCommunityTimeoutsResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.communities.list_timeouts",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return api.ListCommunityTimeoutsResponse{}, err
	}
	defer close(resp.Body)

	if resp.StatusCode >= 400 {
		return api.ListCommunityTimeoutsResponse{}, c.errorFromFailedRequest(resp)
	}

	data := api.ListCommunityTimeoutsResponse{}
	err = json.NewDecoder(resp.Body).Decode(&data)
	if err != nil {
		return api.ListCommunityTimeoutsResponse{}, fmt.Errorf("can't read response from zuma: %s", err)
	}

	return data, nil
}

func (c *client) AddCommunityTimeout(ctx context.Context, params api.AddCommunityTimeoutRequest, reqOpts *twitchclient.ReqOpts) error {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return err
	}

	req, err := c.NewRequest("POST", "/v1/communities/timeouts/add", bytes.NewReader(bodyBytes))
	if err != nil {
		return err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.communities.add_timeout",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}
	defer close(resp.Body)

	if resp.StatusCode >= 400 {
		return c.errorFromFailedRequest(resp)
	}

	return nil
}

func (c *client) RemoveCommunityTimeout(ctx context.Context, params api.RemoveCommunityTimeoutRequest, reqOpts *twitchclient.ReqOpts) error {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return err
	}

	req, err := c.NewRequest("POST", "/v1/communities/timeouts/remove", bytes.NewReader(bodyBytes))
	if err != nil {
		return err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.communities.remove_timeout",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}
	defer close(resp.Body)

	if resp.StatusCode >= 400 {
		return c.errorFromFailedRequest(resp)
	}

	return nil
}

func (c *client) GetCommunityModerationLogs(ctx context.Context, params api.GetCommunityModerationActionsRequest, reqOpts *twitchclient.ReqOpts) (api.GetCommunityModerationActionsResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.GetCommunityModerationActionsResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/communities/moderation_logs/list", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.GetCommunityModerationActionsResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.communities.get_mod_actions",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return api.GetCommunityModerationActionsResponse{}, err
	}
	defer close(resp.Body)

	if resp.StatusCode >= 400 {
		return api.GetCommunityModerationActionsResponse{}, c.errorFromFailedRequest(resp)
	}

	data := api.GetCommunityModerationActionsResponse{}
	err = json.NewDecoder(resp.Body).Decode(&data)
	if err != nil {
		return api.GetCommunityModerationActionsResponse{}, fmt.Errorf("can't read response from zuma: %s", err)
	}

	return data, nil
}

func (c *client) InternalGetCommunityPermissions(ctx context.Context, params api.GetCommunityPermissionsRequest, reqOpts *twitchclient.ReqOpts) (api.GetCommunityPermissionsResponse, error) {
	return c.getCommunityPermissions(ctx, true, params, reqOpts)
}

func (c *client) GetCommunityPermissions(ctx context.Context, params api.GetCommunityPermissionsRequest, reqOpts *twitchclient.ReqOpts) (api.GetCommunityPermissionsResponse, error) {
	return c.getCommunityPermissions(ctx, false, params, reqOpts)
}

func (c *client) getCommunityPermissions(ctx context.Context, internal bool, params api.GetCommunityPermissionsRequest, reqOpts *twitchclient.ReqOpts) (api.GetCommunityPermissionsResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.GetCommunityPermissionsResponse{}, err
	}

	path := "/v1/communities/permissions"
	if internal {
		path = "/v1/communities/internal_permissions"
	}

	req, err := c.NewRequest("POST", path, bytes.NewReader(bodyBytes))
	if err != nil {
		return api.GetCommunityPermissionsResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.communities.get_permissions",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return api.GetCommunityPermissionsResponse{}, err
	}
	defer close(resp.Body)

	if resp.StatusCode >= 400 {
		return api.GetCommunityPermissionsResponse{}, c.errorFromFailedRequest(resp)
	}

	data := api.GetCommunityPermissionsResponse{}
	err = json.NewDecoder(resp.Body).Decode(&data)
	if err != nil {
		return api.GetCommunityPermissionsResponse{}, fmt.Errorf("can't read response from zuma: %s", err)
	}

	return data, nil
}

func (c *client) GetChannelCommunity(ctx context.Context, params api.GetChannelCommunityRequest, reqOpts *twitchclient.ReqOpts) (api.GetChannelCommunityResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.GetChannelCommunityResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/channels/communities/get", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.GetChannelCommunityResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.channels.get_community",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return api.GetChannelCommunityResponse{}, err
	}
	defer close(resp.Body)

	if resp.StatusCode >= 400 {
		return api.GetChannelCommunityResponse{}, c.errorFromFailedRequest(resp)
	}

	data := api.GetChannelCommunityResponse{}
	err = json.NewDecoder(resp.Body).Decode(&data)
	if err != nil {
		return api.GetChannelCommunityResponse{}, fmt.Errorf("can't read response from zuma: %s", err)
	}

	return data, nil
}

func (c *client) SetChannelCommunity(ctx context.Context, params api.SetChannelCommunityRequest, reqOpts *twitchclient.ReqOpts) error {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return err
	}

	req, err := c.NewRequest("POST", "/v1/channels/communities/set", bytes.NewReader(bodyBytes))
	if err != nil {
		return err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.channels.set_community",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}
	defer close(resp.Body)

	if resp.StatusCode >= 400 {
		return c.errorFromFailedRequest(resp)
	}

	return nil
}

func (c *client) SetChannelCommunities(ctx context.Context, params api.SetChannelCommunitiesRequest, reqOpts *twitchclient.ReqOpts) error {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return err
	}

	req, err := c.NewRequest("POST", "/v1/channels/communities/set_multiple", bytes.NewReader(bodyBytes))
	if err != nil {
		return err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.channels.set_communities",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}
	defer close(resp.Body)

	if resp.StatusCode >= 400 {
		return c.errorFromFailedRequest(resp)
	}

	return nil
}

func (c *client) UnsetChannelCommunity(ctx context.Context, params api.UnsetChannelCommunityRequest, reqOpts *twitchclient.ReqOpts) error {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return err
	}

	req, err := c.NewRequest("POST", "/v1/channels/communities/unset", bytes.NewReader(bodyBytes))
	if err != nil {
		return err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.channels.unset_community",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}
	defer close(resp.Body)

	if resp.StatusCode >= 400 {
		return c.errorFromFailedRequest(resp)
	}

	return nil
}

func (c *client) ReportChannelCommunity(ctx context.Context, params api.ReportChannelCommunityRequest, reqOpts *twitchclient.ReqOpts) error {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return err
	}

	req, err := c.NewRequest("POST", "/v1/channels/communities/report", bytes.NewReader(bodyBytes))
	if err != nil {
		return err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.channels.report_community",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}
	defer close(resp.Body)

	if resp.StatusCode >= 400 {
		return c.errorFromFailedRequest(resp)
	}

	return nil
}

func (c *client) BulkGetChannelCommunities(ctx context.Context, params api.BulkGetChannelCommunitiesRequest, reqOpts *twitchclient.ReqOpts) (api.BulkGetChannelCommunitiesResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.BulkGetChannelCommunitiesResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/channels/communities/bulk_get_by_login", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.BulkGetChannelCommunitiesResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.channels.bulk_get_communities",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return api.BulkGetChannelCommunitiesResponse{}, err
	}
	defer close(resp.Body)

	if resp.StatusCode >= 400 {
		return api.BulkGetChannelCommunitiesResponse{}, c.errorFromFailedRequest(resp)
	}

	data := api.BulkGetChannelCommunitiesResponse{}
	err = json.NewDecoder(resp.Body).Decode(&data)
	if err != nil {
		return api.BulkGetChannelCommunitiesResponse{}, fmt.Errorf("can't read response from zuma: %s", err)
	}

	return data, nil
}

func (c *client) BulkGetChannelCommunitiesByIDs(ctx context.Context, params api.BulkGetChannelCommunitiesByIDsRequest, reqOpts *twitchclient.ReqOpts) (api.BulkGetChannelCommunitiesByIDsResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.BulkGetChannelCommunitiesByIDsResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/channels/communities/bulk_get_by_ids", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.BulkGetChannelCommunitiesByIDsResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.channels.bulk_get_communities_by_ids",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return api.BulkGetChannelCommunitiesByIDsResponse{}, err
	}
	defer close(resp.Body)

	if resp.StatusCode >= 400 {
		return api.BulkGetChannelCommunitiesByIDsResponse{}, c.errorFromFailedRequest(resp)
	}

	data := api.BulkGetChannelCommunitiesByIDsResponse{}
	err = json.NewDecoder(resp.Body).Decode(&data)
	if err != nil {
		return api.BulkGetChannelCommunitiesByIDsResponse{}, fmt.Errorf("can't read response from zuma: %s", err)
	}

	return data, nil
}

func (c *client) ListUserBlocks(ctx context.Context, params api.ListUserBlocksParams, reqOpts *twitchclient.ReqOpts) (api.ListUserBlocksResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.ListUserBlocksResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/users/blocks/get", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.ListUserBlocksResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.users.get_blocks",
		StatSampleRate: defaultStatSampleRate,
	})

	var decoded api.ListUserBlocksResponse
	if _, err := c.DoJSON(ctx, &decoded, req, combinedReqOpts); err != nil {
		return api.ListUserBlocksResponse{}, err
	}
	return decoded, nil
}

func (c *client) ListUserBlockers(ctx context.Context, params api.ListUserBlockersParams, reqOpts *twitchclient.ReqOpts) (api.ListUserBlockersResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.ListUserBlockersResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/users/blocks/blockers", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.ListUserBlockersResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.users.list_blockers",
		StatSampleRate: defaultStatSampleRate,
	})

	var decoded api.ListUserBlockersResponse
	if _, err := c.DoJSON(ctx, &decoded, req, combinedReqOpts); err != nil {
		return api.ListUserBlockersResponse{}, err
	}
	return decoded, nil
}

func (c *client) IsBlocked(ctx context.Context, params api.IsBlockedParams, reqOpts *twitchclient.ReqOpts) (api.IsBlockedResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.IsBlockedResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/users/blocks/is_blocked", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.IsBlockedResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.users.get_blocks",
		StatSampleRate: defaultStatSampleRate,
	})

	var decoded api.IsBlockedResponse
	if _, err := c.DoJSON(ctx, &decoded, req, combinedReqOpts); err != nil {
		return api.IsBlockedResponse{}, err
	}
	return decoded, nil
}

func (c *client) AddUserBlock(ctx context.Context, params api.AddUserBlockParams, reqOpts *twitchclient.ReqOpts) (api.AddUserBlockResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.AddUserBlockResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/users/blocks/add", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.AddUserBlockResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.users.add_block",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return api.AddUserBlockResponse{}, err
	}
	defer close(resp.Body)

	// 422 is a valid, expected response and should be handled
	if resp.StatusCode >= 400 && resp.StatusCode != http.StatusUnprocessableEntity {
		return api.AddUserBlockResponse{}, c.errorFromFailedRequest(resp)
	}

	data := api.AddUserBlockResponse{}
	err = json.NewDecoder(resp.Body).Decode(&data)
	if err != nil {
		return api.AddUserBlockResponse{}, err
	}

	return data, nil
}

func (c *client) RemoveUserBlock(ctx context.Context, params api.RemoveUserBlockParams, reqOpts *twitchclient.ReqOpts) error {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return err
	}

	req, err := c.NewRequest("POST", "/v1/users/blocks/remove", bytes.NewReader(bodyBytes))
	if err != nil {
		return err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.users.remove_block",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}
	defer close(resp.Body)

	if resp.StatusCode >= 400 {
		return c.errorFromFailedRequest(resp)
	}

	return nil
}

func (c *client) GetUserRoles(ctx context.Context, params api.UserRoleRequest, reqOpts *twitchclient.ReqOpts) (api.UserRoleResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.UserRoleResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/users/roles/get", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.UserRoleResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.users.get_roles",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return api.UserRoleResponse{}, err
	}
	defer close(resp.Body)

	if resp.StatusCode >= 400 {
		return api.UserRoleResponse{}, c.errorFromFailedRequest(resp)
	}

	var data api.UserRoleResponse

	if err = json.NewDecoder(resp.Body).Decode(&data); err != nil {
		return api.UserRoleResponse{}, err
	}

	return data, nil
}

func (c *client) ParseMessage(ctx context.Context, params api.ParseMessageRequest, reqOpts *twitchclient.ReqOpts) (api.ParseMessageResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.ParseMessageResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/messages/parse", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.ParseMessageResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.messages.parse",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return api.ParseMessageResponse{}, err
	}
	defer close(resp.Body)

	switch resp.StatusCode {
	case http.StatusForbidden, http.StatusOK:
		data := api.ParseMessageResponse{}
		err = json.NewDecoder(resp.Body).Decode(&data)
		if err != nil {
			return api.ParseMessageResponse{}, fmt.Errorf("can't read response from zuma: %s", err)
		}

		return data, nil
	default:
		return api.ParseMessageResponse{}, c.errorFromFailedRequest(resp)
	}
}

func (c *client) GetCommunityFollower(ctx context.Context, params api.GetCommunityFollowerRequest, reqOpts *twitchclient.ReqOpts) (api.GetCommunityFollowerResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.GetCommunityFollowerResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/communities/followers/get", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.GetCommunityFollowerResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.communities.get_follower",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return api.GetCommunityFollowerResponse{}, err
	}
	defer close(resp.Body)

	switch resp.StatusCode {
	case http.StatusForbidden, http.StatusOK:
		data := api.GetCommunityFollowerResponse{}
		err = json.NewDecoder(resp.Body).Decode(&data)
		if err != nil {
			return api.GetCommunityFollowerResponse{}, fmt.Errorf("can't read response from zuma: %s", err)
		}

		return data, nil
	default:
		return api.GetCommunityFollowerResponse{}, c.errorFromFailedRequest(resp)
	}
}

func (c *client) ListCommunityFollowers(ctx context.Context, params api.ListCommunityFollowersRequest, reqOpts *twitchclient.ReqOpts) (api.ListCommunityFollowersResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.ListCommunityFollowersResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/communities/followers/list", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.ListCommunityFollowersResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.communities.list_followers",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return api.ListCommunityFollowersResponse{}, err
	}
	defer close(resp.Body)

	switch resp.StatusCode {
	case http.StatusForbidden, http.StatusOK:
		data := api.ListCommunityFollowersResponse{}
		err = json.NewDecoder(resp.Body).Decode(&data)
		if err != nil {
			return api.ListCommunityFollowersResponse{}, fmt.Errorf("can't read response from zuma: %s", err)
		}

		return data, nil
	default:
		return api.ListCommunityFollowersResponse{}, c.errorFromFailedRequest(resp)
	}
}

func (c *client) CountCommunityFollowers(ctx context.Context, params api.CountCommunityFollowersRequest, reqOpts *twitchclient.ReqOpts) (api.CountCommunityFollowersResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.CountCommunityFollowersResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/communities/followers/count", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.CountCommunityFollowersResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.communities.count_followers",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return api.CountCommunityFollowersResponse{}, err
	}
	defer close(resp.Body)

	switch resp.StatusCode {
	case http.StatusForbidden, http.StatusOK:
		data := api.CountCommunityFollowersResponse{}
		err = json.NewDecoder(resp.Body).Decode(&data)
		if err != nil {
			return api.CountCommunityFollowersResponse{}, fmt.Errorf("can't read response from zuma: %s", err)
		}

		return data, nil
	default:
		return api.CountCommunityFollowersResponse{}, c.errorFromFailedRequest(resp)
	}
}

func (c *client) AddUserFollowedCommunity(ctx context.Context, params api.AddUserFollowedCommunityRequest, reqOpts *twitchclient.ReqOpts) error {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return err
	}

	req, err := c.NewRequest("POST", "/v1/users/follows/communities/add", bytes.NewReader(bodyBytes))
	if err != nil {
		return err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.users.add_followed_community",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}
	defer close(resp.Body)

	if resp.StatusCode >= 400 {
		return c.errorFromFailedRequest(resp)
	}

	return nil
}

func (c *client) RemoveUserFollowedCommunity(ctx context.Context, params api.RemoveUserFollowedCommunityRequest, reqOpts *twitchclient.ReqOpts) error {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return err
	}

	req, err := c.NewRequest("POST", "/v1/users/follows/communities/remove", bytes.NewReader(bodyBytes))
	if err != nil {
		return err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.users.remove_followed_community",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}
	defer close(resp.Body)

	if resp.StatusCode >= 400 {
		return c.errorFromFailedRequest(resp)
	}

	return nil
}

func (c *client) ListUserFollowedCommunities(ctx context.Context, params api.ListUserFollowedCommunitiesRequest, reqOpts *twitchclient.ReqOpts) (api.ListUserFollowedCommunitiesResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.ListUserFollowedCommunitiesResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/users/follows/communities/list", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.ListUserFollowedCommunitiesResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.users.list_followed_communities",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return api.ListUserFollowedCommunitiesResponse{}, err
	}
	defer close(resp.Body)

	switch resp.StatusCode {
	case http.StatusForbidden, http.StatusOK:
		data := api.ListUserFollowedCommunitiesResponse{}
		err = json.NewDecoder(resp.Body).Decode(&data)
		if err != nil {
			return api.ListUserFollowedCommunitiesResponse{}, fmt.Errorf("can't read response from zuma: %s", err)
		}

		return data, nil
	default:
		return api.ListUserFollowedCommunitiesResponse{}, c.errorFromFailedRequest(resp)
	}
}

func (c *client) TopUserFollowedCommunities(ctx context.Context, params api.TopUserFollowedCommunitiesRequest, reqOpts *twitchclient.ReqOpts) (api.TopUserFollowedCommunitiesResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.TopUserFollowedCommunitiesResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/users/follows/communities/top", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.TopUserFollowedCommunitiesResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.users.top_followed_communities",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return api.TopUserFollowedCommunitiesResponse{}, err
	}
	defer close(resp.Body)

	switch resp.StatusCode {
	case http.StatusForbidden, http.StatusOK:
		data := api.TopUserFollowedCommunitiesResponse{}
		err = json.NewDecoder(resp.Body).Decode(&data)
		if err != nil {
			return api.TopUserFollowedCommunitiesResponse{}, fmt.Errorf("can't read response from zuma: %s", err)
		}

		return data, nil
	default:
		return api.TopUserFollowedCommunitiesResponse{}, c.errorFromFailedRequest(resp)
	}
}

func (c *client) GetBrowseTop(ctx context.Context, params api.GetTopBrowseParams, reqOpts *twitchclient.ReqOpts) (api.GetTopBrowseResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.GetTopBrowseResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/browse/top", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.GetTopBrowseResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.browse.get_top",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return api.GetTopBrowseResponse{}, err
	}
	defer close(resp.Body)

	switch resp.StatusCode {
	case http.StatusOK:
		data := api.GetTopBrowseResponse{}
		err = json.NewDecoder(resp.Body).Decode(&data)
		if err != nil {
			return api.GetTopBrowseResponse{}, fmt.Errorf("can't read response from zuma: %s", err)
		}

		return data, nil
	default:
		return api.GetTopBrowseResponse{}, c.errorFromFailedRequest(resp)
	}
}

func (c *client) CreateGameCommunity(ctx context.Context, params api.CreateGameCommunityRequest, reqOpts *twitchclient.ReqOpts) error {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return err
	}

	req, err := c.NewRequest("POST", "/v1/games/communities/create", bytes.NewReader(bodyBytes))
	if err != nil {
		return err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.games.create_community",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}
	defer close(resp.Body)

	if resp.StatusCode >= 400 {
		return c.errorFromFailedRequest(resp)
	}

	return nil
}

func (c *client) GetGameBannerImages(ctx context.Context, params api.GetGameBannerImagesRequest, reqOpts *twitchclient.ReqOpts) (api.GetGameBannerImagesResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.GetGameBannerImagesResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/games/banner_images/bulk_get", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.GetGameBannerImagesResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.games.bulk_get_banner_images",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return api.GetGameBannerImagesResponse{}, err
	}
	defer close(resp.Body)

	if resp.StatusCode >= 400 {
		return api.GetGameBannerImagesResponse{}, c.errorFromFailedRequest(resp)
	}

	data := api.GetGameBannerImagesResponse{}
	err = json.NewDecoder(resp.Body).Decode(&data)
	if err != nil {
		return api.GetGameBannerImagesResponse{}, fmt.Errorf("can't read response from zuma: %s", err)
	}

	return data, nil
}

func (c *client) BulkGetCommunitySettings(ctx context.Context, params api.BulkGetCommunitySettingsRequest, reqOpts *twitchclient.ReqOpts) (api.BulkGetCommunitySettingsResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.BulkGetCommunitySettingsResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/communities/settings/bulk_get", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.BulkGetCommunitySettingsResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.communities.bulk_get_settings",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return api.BulkGetCommunitySettingsResponse{}, err
	}
	defer close(resp.Body)

	if resp.StatusCode >= 400 {
		return api.BulkGetCommunitySettingsResponse{}, c.errorFromFailedRequest(resp)
	}

	data := api.BulkGetCommunitySettingsResponse{}
	err = json.NewDecoder(resp.Body).Decode(&data)
	if err != nil {
		return api.BulkGetCommunitySettingsResponse{}, fmt.Errorf("can't read response from zuma: %s", err)
	}

	return data, nil
}

func (c *client) BulkGetCommunitySettingsByName(ctx context.Context, params api.BulkGetCommunitySettingsByNameRequest, reqOpts *twitchclient.ReqOpts) (api.BulkGetCommunitySettingsResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.BulkGetCommunitySettingsResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/communities/settings/bulk_get_by_name", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.BulkGetCommunitySettingsResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.communities.bulk_get_settings_by_name",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return api.BulkGetCommunitySettingsResponse{}, err
	}
	defer close(resp.Body)

	if resp.StatusCode >= 400 {
		return api.BulkGetCommunitySettingsResponse{}, c.errorFromFailedRequest(resp)
	}

	data := api.BulkGetCommunitySettingsResponse{}
	err = json.NewDecoder(resp.Body).Decode(&data)
	if err != nil {
		return api.BulkGetCommunitySettingsResponse{}, fmt.Errorf("can't read response from zuma: %s", err)
	}

	return data, nil
}

// handleFailedRequest parses the detailed ErrorResponse from Zuma, if one is
// provided
func (c *client) errorFromFailedRequest(resp *http.Response) error {
	var errResp api.ErrorResponse
	err := json.NewDecoder(resp.Body).Decode(&errResp)
	if err != nil {
		// could not decode the Zuma response
		return &api.ErrorResponse{
			Err:    fmt.Sprintf("failed zuma request. StatusCode=%v Unable to read response body (%v)", resp.StatusCode, err),
			Status: resp.StatusCode,
		}
	}
	return &errResp
}

// decodeErrorCodeResponse unmarshals a JSON response into v. This function returns an error
// if the status code is >= 400, unless an error_code field is present in the response.
func decodeErrorCodeResponse(ctx context.Context, resp *http.Response, v interface{}) error {
	body, err := ioutil.ReadAll(resp.Body)
	close(resp.Body)
	if err != nil {
		return err
	}

	// Reassign the body since we can only read the ReadCloser once.
	resp.Body = ioutil.NopCloser(bytes.NewBuffer(body))
	defer close(resp.Body)

	err = json.Unmarshal(body, &v)
	if err != nil {
		return twitchclient.HandleFailedResponse(resp)
	}

	if resp.StatusCode >= 400 {
		var errorCodeResponse api.ErrorCodeResponse
		err = json.Unmarshal(body, &errorCodeResponse)
		if err != nil {
			return err
		}

		if errorCodeResponse.ErrorCode != nil {
			return nil
		}

		return twitchclient.HandleFailedResponse(resp)
	}

	return nil
}

func close(closer io.Closer) {
	if err := closer.Close(); err != nil {
		log.Printf("error closing response body: %v", err)
	}
}
