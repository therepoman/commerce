package zuma

import (
	"bytes"
	"context"
	"encoding/json"

	"code.justin.tv/chat/zuma/app/api"
	"code.justin.tv/foundation/twitchclient"
)

func (c *client) ListContainerViewsByUser(ctx context.Context, params api.ListContainerViewsByUserRequest, reqOpts *twitchclient.ReqOpts) (api.ListContainerViewsByUserResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.ListContainerViewsByUserResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/maap/container_views/list_by_user", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.ListContainerViewsByUserResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.maap.list_container_views_by_user",
		StatSampleRate: defaultStatSampleRate,
	})

	var decoded api.ListContainerViewsByUserResponse
	if _, err := c.DoJSON(ctx, &decoded, req, combinedReqOpts); err != nil {
		return api.ListContainerViewsByUserResponse{}, err
	}
	return decoded, nil
}
