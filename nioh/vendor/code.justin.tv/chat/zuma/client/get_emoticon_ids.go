package zuma

import (
	"bytes"
	"context"
	"encoding/json"

	"code.justin.tv/chat/zuma/app/api"
	"code.justin.tv/foundation/twitchclient"
)

func (c *client) GetEmoticonIDs(ctx context.Context, params api.GetEmoticonIDsRequest, reqOpts *twitchclient.ReqOpts) (api.GetEmoticonIDsResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.GetEmoticonIDsResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/users/emoticons/get_ids", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.GetEmoticonIDsResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.get_emoticon_ids",
		StatSampleRate: defaultStatSampleRate,
	})

	var decoded api.GetEmoticonIDsResponse
	if _, err := c.DoJSON(ctx, &decoded, req, combinedReqOpts); err != nil {
		return api.GetEmoticonIDsResponse{}, err
	}
	return decoded, nil
}
