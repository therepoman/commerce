package zuma

import (
	"bytes"
	"context"
	"encoding/json"

	"code.justin.tv/chat/zuma/app/api"
	"code.justin.tv/foundation/twitchclient"
)

func (c *client) UpdateContainer(ctx context.Context, params api.UpdateContainerRequest, reqOpts *twitchclient.ReqOpts) (api.UpdateContainerResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.UpdateContainerResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/maap/containers/update", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.UpdateContainerResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.maap.update_container",
		StatSampleRate: defaultStatSampleRate,
	})

	var decoded api.UpdateContainerResponse
	if _, err := c.DoJSON(ctx, &decoded, req, combinedReqOpts); err != nil {
		return api.UpdateContainerResponse{}, err
	}
	return decoded, nil
}
