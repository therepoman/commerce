package zuma

import (
	"bytes"
	"context"
	"encoding/json"

	"code.justin.tv/chat/zuma/app/api"
	"code.justin.tv/foundation/twitchclient"
)

func (c *client) DeleteMessage(ctx context.Context, params api.DeleteMessageRequest, reqOpts *twitchclient.ReqOpts) (api.DeleteMessageResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.DeleteMessageResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/maap/messages/delete", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.DeleteMessageResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.maap.delete_message",
		StatSampleRate: defaultStatSampleRate,
	})

	var decoded api.DeleteMessageResponse
	if _, err := c.DoJSON(ctx, &decoded, req, combinedReqOpts); err != nil {
		return api.DeleteMessageResponse{}, err
	}
	return decoded, nil
}
