package client

import (
	"bytes"
	"encoding/json"
	"log"
	"strconv"
	"time"

	"code.justin.tv/foundation/twitchclient"
	"golang.org/x/net/context"
)

const (
	defaultStatSampleRate = 0.1
	defaultTimingXactName = "pubsub"
	pubTimeout            = 1 * time.Second
)

type clientImpl struct {
	twitchclient.Client
}

func NewPubClient(conf twitchclient.ClientConf) (PubClient, error) {
	if conf.Host == "" {
		log.Printf("Using Noop Pub Client")
		return &noopClientImpl{}, nil
	}

	if conf.TimingXactName == "" {
		conf.TimingXactName = defaultTimingXactName
	}

	twitchClient, err := twitchclient.NewClient(conf)
	return &clientImpl{twitchClient}, err
}

// PubClient is the interface that all pubsub clients should support.
type PubClient interface {
	Publish(context.Context, []string, string, *twitchclient.ReqOpts) error
}

func (p *clientImpl) Publish(ctx context.Context, topics []string, message string, opts *twitchclient.ReqOpts) error {
	if len(topics) == 0 {
		return nil
	}

	ctx, cancelFunc := context.WithTimeout(ctx, pubTimeout)
	defer cancelFunc()

	data, err := generateMessage(topics, message)
	if err != nil {
		return err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(opts, twitchclient.ReqOpts{
		StatName:       "service.pubsub.publish",
		StatSampleRate: defaultStatSampleRate,
	})

	err = p.postMessage(ctx, data, combinedReqOpts)
	return err
}

type pubMessage struct {
	Topics  []string `json:"topics"`
	Message string   `json:"message"`
}

// GenerateMessage creates the message that will be posted to the pubsub broker.
func generateMessage(topics []string, data string) (pubMessage, error) {
	ret := pubMessage{Topics: topics, Message: data}
	return ret, nil
}

// PostMessage sends a messsage to the pubsub broker.
func (p *clientImpl) postMessage(ctx context.Context, data pubMessage, reqOpts twitchclient.ReqOpts) error {
	dataJSON, err := json.Marshal(data)
	if err != nil {
		return err
	}

	req, err := p.NewRequest("POST", "/v1/message", bytes.NewReader(dataJSON))
	if err != nil {
		return err
	}
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Content-Length", strconv.Itoa(len(dataJSON)))

	resp, err := p.Do(ctx, req, reqOpts)
	if err != nil {
		return err
	}
	defer func() {
		if cerr := resp.Body.Close(); err == nil {
			err = cerr
		}
	}()

	if resp.StatusCode >= 400 {
		return &twitchclient.Error{StatusCode: resp.StatusCode, Message: "pubsub publish failed"}
	}

	return nil
}

type noopClientImpl struct{}

func (p *noopClientImpl) Publish(ctx context.Context, topics []string, message string, opts *twitchclient.ReqOpts) error {
	log.Printf("Noop Publish - topics: %v, message: %s", topics, message)
	return nil
}
