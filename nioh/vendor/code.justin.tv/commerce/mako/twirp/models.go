package mako_client

type EmoteUploadSNSCallbackData struct {
	Size        string `json:"size,omitempty"`
	ImageHeight uint32 `json:"image_height,omitempty"`
	ImageWidth  uint32 `json:"image_width,omitempty"`
}

// Deprecated: this is used by the previous uploading method. Optional Callback data for clients to pass in and receive back from Upload Service via SNS
type EmoticonUploadSNSCallbackData struct {
	Code        string `json:"code,omitempty"`
	GroupId     string `json:"group_id,omitempty"`
	ImageHeight int32  `json:"image_height,omitempty"`
	ImageWidth  int32  `json:"image_width,omitempty"`
}
