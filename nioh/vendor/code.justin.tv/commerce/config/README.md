# File based Configuration

## How to use
In your repository, create json files in line with your separate environments.

For example, if your development, staging, and production boxes are labeled as `dev`, `staging`, `prod` within their respective environment variables, create
`config/dev.json`, `config/staging.json`, `config/prod.json` and set your configs.

If your boxes are instead labelled different, such as `devo`, `gamma`, `canary`, etc, create files labeled the same respectively.

**Note:** On boxes without environment variables set such as your local host, this will default to `dev.json`

```
// config/dev.json

{
    "SomeKey": "SomeValue"
}

```


```
import (
    "log"

    "code.justin.tv/commerce/config"
)

// This struct can live anywhere as this library is agnostic to the struct being used
type configuration struct {
    SomeKey string `valid:"required"`
}

func main() {
    environment := config.GetEnvironment()

    options := config.Options{
        OrganizationName: "<GITHUB ORG NAME>",
        ServiceName: "<GITHUB REPO NAME>",
        Environment: environment,
    }

    cfg := configuration{}
    if err := config.LoadConfigForEnvironment(&cfg, options); err != nil {
        log.Fatal(err)
    }

    log.Printf("SomeKey: %s", cfg.SomeKey) // -> "SomeKey: SomeValue"
}

```