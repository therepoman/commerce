Trace FAQ
===

How do I log in to https://trace.internal.justin.tv/?
---

The current HTML reports require authentication via **clean LDAP**. Please
use that username and password when prompted.

My login was rejected—what's going on?
---

Additionally, access to Trace HTML reports requires that your user is in an
LDAP group corresponding to a team with a _need to know_. The lists are
[enumerated in puppet](https://git-aws.internal.justin.tv/systems/puppet/blob/master/modules/twitch_trace_report/templates/nginx/twitch_trace_report.conf.erb);
membership in any of those groups will grant you access.

You can get a list of LDAP groups your user is in by running the `id -a`
command on a puppetized server. Running `id -a $user` will tell you about
someone else's group membership.

If your team has access but your LDAP user doesn't have the right membership,
you can ask for access in #systems on Slack.

If your team has a specific LDAP group that isn't listed in Puppet, you can
ask in #trace on Slack for it to be added to the whitelist. The LDAP group
should be specific to your team—the "infra", "employees", and "dev" groups are
too broad to be granted access.

Can you do something to improve that?
---

Yes, the plan is to remove the potentially-sensitive data from the reports so
they can be shared more freely.

You can help by
[contributing an up-to-date list of your service's routes](https://git-aws.internal.justin.tv/release/trace/blob/master/CONTRIBUTING.md#contributing-routes)
so HTTP calls to your system are identifiable without the URI path.
