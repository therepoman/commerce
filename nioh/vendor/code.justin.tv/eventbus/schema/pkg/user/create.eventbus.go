// NOTE this is a generated file! do not edit!

package user

import (
	"context"

	"github.com/golang/protobuf/proto"

	eventbus "code.justin.tv/eventbus/client"
)

const (
	CreateEventType = "UserCreate"
)

type Create = UserCreate

type UserCreateHandler func(context.Context, *eventbus.Header, *UserCreate) error

func (h UserCreateHandler) Handler() eventbus.Handler {
	return func(ctx context.Context, message eventbus.RawMessage) error {
		dst := &UserCreate{}
		err := proto.Unmarshal(message.Payload, dst)
		if err != nil {
			return err
		}
		return h(ctx, message.Header, dst)
	}
}

func RegisterUserCreateHandler(mux *eventbus.Mux, f UserCreateHandler) {
	mux.RegisterHandler(CreateEventType, f.Handler())
}

func RegisterCreateHandler(mux *eventbus.Mux, f UserCreateHandler) {
	RegisterUserCreateHandler(mux, f)
}

func (*UserCreate) EventBusName() string {
	return CreateEventType
}
