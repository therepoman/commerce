// NOTE this is a generated file! do not edit!

package user

import (
	"context"

	"github.com/golang/protobuf/proto"

	eventbus "code.justin.tv/eventbus/client"
)

const (
	UpdateEventType = "UserUpdate"
)

type Update = UserUpdate

type UserUpdateHandler func(context.Context, *eventbus.Header, *UserUpdate) error

func (h UserUpdateHandler) Handler() eventbus.Handler {
	return func(ctx context.Context, message eventbus.RawMessage) error {
		dst := &UserUpdate{}
		err := proto.Unmarshal(message.Payload, dst)
		if err != nil {
			return err
		}
		return h(ctx, message.Header, dst)
	}
}

func RegisterUserUpdateHandler(mux *eventbus.Mux, f UserUpdateHandler) {
	mux.RegisterHandler(UpdateEventType, f.Handler())
}

func RegisterUpdateHandler(mux *eventbus.Mux, f UserUpdateHandler) {
	RegisterUserUpdateHandler(mux, f)
}

func (*UserUpdate) EventBusName() string {
	return UpdateEventType
}
