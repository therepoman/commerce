// Code generated by protoc-gen-go. DO NOT EDIT.
// source: user/update.proto

package user

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"
import change "code.justin.tv/eventbus/schema/pkg/eventbus/change"

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion2 // please upgrade the proto package

// UserUpdate is triggered on various user/profile change actions.
//
// Other than the 'user_id' field which is always included, all the
// other fields are optional and not necessarily included in all events.
// Clients should anticipate this depending on user actions and not rely
// on getting the entire range of values.
//
// Various PII fields are currently omitted, though that they have changed
// is signaled using the 'Change' type. It is designed in such a way that
// we may be able to upgrade them to encrypted fields in the future.
type UserUpdate struct {
	UserId string `protobuf:"bytes,1,opt,name=user_id,json=userId,proto3" json:"user_id,omitempty"`
	// The alphanumeric login name of the user.
	Login *change.StringChange `protobuf:"bytes,2,opt,name=login,proto3" json:"login,omitempty"`
	// Display Name is the stylized version of the login name (may contain localized characters).
	DisplayName *change.StringChange `protobuf:"bytes,3,opt,name=display_name,json=displayName,proto3" json:"display_name,omitempty"`
	// Birthday change can be signaled, data currently omitted for PII.
	Birthday *change.Change `protobuf:"bytes,4,opt,name=birthday,proto3" json:"birthday,omitempty"`
	// Email address change can be signaled, data currently omitted for PII.
	Email *change.Change `protobuf:"bytes,5,opt,name=email,proto3" json:"email,omitempty"`
	// Phone number change can be signaled, data currently omitted for PII.
	PhoneNumber *change.Change `protobuf:"bytes,11,opt,name=phone_number,json=phoneNumber,proto3" json:"phone_number,omitempty"`
	// Language is an ISO-639-1 two-letter language code e.g. 'en'
	Language *change.StringChange `protobuf:"bytes,6,opt,name=language,proto3" json:"language,omitempty"`
	// The user's own set description. May be multi-line.
	Description *change.StringChange `protobuf:"bytes,7,opt,name=description,proto3" json:"description,omitempty"`
	// Each image change key is a size code e.g 320x400.
	ProfileImage         map[string]*ImageChange `protobuf:"bytes,8,rep,name=profile_image,json=profileImage,proto3" json:"profile_image,omitempty" protobuf_key:"bytes,1,opt,name=key,proto3" protobuf_val:"bytes,2,opt,name=value,proto3"`
	ProfileBanner        map[string]*ImageChange `protobuf:"bytes,9,rep,name=profile_banner,json=profileBanner,proto3" json:"profile_banner,omitempty" protobuf_key:"bytes,1,opt,name=key,proto3" protobuf_val:"bytes,2,opt,name=value,proto3"`
	ChannelOfflineImage  map[string]*ImageChange `protobuf:"bytes,10,rep,name=channel_offline_image,json=channelOfflineImage,proto3" json:"channel_offline_image,omitempty" protobuf_key:"bytes,1,opt,name=key,proto3" protobuf_val:"bytes,2,opt,name=value,proto3"`
	XXX_NoUnkeyedLiteral struct{}                `json:"-"`
	XXX_unrecognized     []byte                  `json:"-"`
	XXX_sizecache        int32                   `json:"-"`
}

func (m *UserUpdate) Reset()         { *m = UserUpdate{} }
func (m *UserUpdate) String() string { return proto.CompactTextString(m) }
func (*UserUpdate) ProtoMessage()    {}
func (*UserUpdate) Descriptor() ([]byte, []int) {
	return fileDescriptor_update_28faacaa26da7500, []int{0}
}
func (m *UserUpdate) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_UserUpdate.Unmarshal(m, b)
}
func (m *UserUpdate) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_UserUpdate.Marshal(b, m, deterministic)
}
func (dst *UserUpdate) XXX_Merge(src proto.Message) {
	xxx_messageInfo_UserUpdate.Merge(dst, src)
}
func (m *UserUpdate) XXX_Size() int {
	return xxx_messageInfo_UserUpdate.Size(m)
}
func (m *UserUpdate) XXX_DiscardUnknown() {
	xxx_messageInfo_UserUpdate.DiscardUnknown(m)
}

var xxx_messageInfo_UserUpdate proto.InternalMessageInfo

func (m *UserUpdate) GetUserId() string {
	if m != nil {
		return m.UserId
	}
	return ""
}

func (m *UserUpdate) GetLogin() *change.StringChange {
	if m != nil {
		return m.Login
	}
	return nil
}

func (m *UserUpdate) GetDisplayName() *change.StringChange {
	if m != nil {
		return m.DisplayName
	}
	return nil
}

func (m *UserUpdate) GetBirthday() *change.Change {
	if m != nil {
		return m.Birthday
	}
	return nil
}

func (m *UserUpdate) GetEmail() *change.Change {
	if m != nil {
		return m.Email
	}
	return nil
}

func (m *UserUpdate) GetPhoneNumber() *change.Change {
	if m != nil {
		return m.PhoneNumber
	}
	return nil
}

func (m *UserUpdate) GetLanguage() *change.StringChange {
	if m != nil {
		return m.Language
	}
	return nil
}

func (m *UserUpdate) GetDescription() *change.StringChange {
	if m != nil {
		return m.Description
	}
	return nil
}

func (m *UserUpdate) GetProfileImage() map[string]*ImageChange {
	if m != nil {
		return m.ProfileImage
	}
	return nil
}

func (m *UserUpdate) GetProfileBanner() map[string]*ImageChange {
	if m != nil {
		return m.ProfileBanner
	}
	return nil
}

func (m *UserUpdate) GetChannelOfflineImage() map[string]*ImageChange {
	if m != nil {
		return m.ChannelOfflineImage
	}
	return nil
}

// Image denotes one of the user's image choices.
type Image struct {
	Height               uint32   `protobuf:"varint,1,opt,name=height,proto3" json:"height,omitempty"`
	Width                uint32   `protobuf:"varint,2,opt,name=width,proto3" json:"width,omitempty"`
	Url                  string   `protobuf:"bytes,3,opt,name=url,proto3" json:"url,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *Image) Reset()         { *m = Image{} }
func (m *Image) String() string { return proto.CompactTextString(m) }
func (*Image) ProtoMessage()    {}
func (*Image) Descriptor() ([]byte, []int) {
	return fileDescriptor_update_28faacaa26da7500, []int{1}
}
func (m *Image) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Image.Unmarshal(m, b)
}
func (m *Image) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Image.Marshal(b, m, deterministic)
}
func (dst *Image) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Image.Merge(dst, src)
}
func (m *Image) XXX_Size() int {
	return xxx_messageInfo_Image.Size(m)
}
func (m *Image) XXX_DiscardUnknown() {
	xxx_messageInfo_Image.DiscardUnknown(m)
}

var xxx_messageInfo_Image proto.InternalMessageInfo

func (m *Image) GetHeight() uint32 {
	if m != nil {
		return m.Height
	}
	return 0
}

func (m *Image) GetWidth() uint32 {
	if m != nil {
		return m.Width
	}
	return 0
}

func (m *Image) GetUrl() string {
	if m != nil {
		return m.Url
	}
	return ""
}

// ImageChange represents a changed image and follows the 'change.proto' scheme.
type ImageChange struct {
	Updated              bool     `protobuf:"varint,1,opt,name=updated,proto3" json:"updated,omitempty"`
	Value                *Image   `protobuf:"bytes,2,opt,name=value,proto3" json:"value,omitempty"`
	HasOldValue          bool     `protobuf:"varint,3,opt,name=has_old_value,json=hasOldValue,proto3" json:"has_old_value,omitempty"`
	OldValue             *Image   `protobuf:"bytes,4,opt,name=old_value,json=oldValue,proto3" json:"old_value,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *ImageChange) Reset()         { *m = ImageChange{} }
func (m *ImageChange) String() string { return proto.CompactTextString(m) }
func (*ImageChange) ProtoMessage()    {}
func (*ImageChange) Descriptor() ([]byte, []int) {
	return fileDescriptor_update_28faacaa26da7500, []int{2}
}
func (m *ImageChange) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_ImageChange.Unmarshal(m, b)
}
func (m *ImageChange) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_ImageChange.Marshal(b, m, deterministic)
}
func (dst *ImageChange) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ImageChange.Merge(dst, src)
}
func (m *ImageChange) XXX_Size() int {
	return xxx_messageInfo_ImageChange.Size(m)
}
func (m *ImageChange) XXX_DiscardUnknown() {
	xxx_messageInfo_ImageChange.DiscardUnknown(m)
}

var xxx_messageInfo_ImageChange proto.InternalMessageInfo

func (m *ImageChange) GetUpdated() bool {
	if m != nil {
		return m.Updated
	}
	return false
}

func (m *ImageChange) GetValue() *Image {
	if m != nil {
		return m.Value
	}
	return nil
}

func (m *ImageChange) GetHasOldValue() bool {
	if m != nil {
		return m.HasOldValue
	}
	return false
}

func (m *ImageChange) GetOldValue() *Image {
	if m != nil {
		return m.OldValue
	}
	return nil
}

func init() {
	proto.RegisterType((*UserUpdate)(nil), "user.UserUpdate")
	proto.RegisterMapType((map[string]*ImageChange)(nil), "user.UserUpdate.ChannelOfflineImageEntry")
	proto.RegisterMapType((map[string]*ImageChange)(nil), "user.UserUpdate.ProfileBannerEntry")
	proto.RegisterMapType((map[string]*ImageChange)(nil), "user.UserUpdate.ProfileImageEntry")
	proto.RegisterType((*Image)(nil), "user.Image")
	proto.RegisterType((*ImageChange)(nil), "user.ImageChange")
}

func init() { proto.RegisterFile("user/update.proto", fileDescriptor_update_28faacaa26da7500) }

var fileDescriptor_update_28faacaa26da7500 = []byte{
	// 522 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xa4, 0x94, 0xd1, 0x6a, 0xdb, 0x30,
	0x14, 0x86, 0x49, 0x53, 0x3b, 0xce, 0x71, 0x32, 0x16, 0x6d, 0x5d, 0x45, 0x60, 0x90, 0x79, 0x17,
	0xcb, 0x2e, 0xe6, 0x42, 0x73, 0xb3, 0xf5, 0x66, 0xa3, 0x65, 0x94, 0xee, 0xa2, 0x1d, 0x2e, 0x1d,
	0x6c, 0x30, 0x8c, 0x1c, 0x2b, 0xb6, 0x98, 0x2c, 0x1b, 0xd9, 0xee, 0xc8, 0x9b, 0xec, 0x31, 0xf6,
	0x88, 0x43, 0x92, 0xdd, 0x64, 0x98, 0x96, 0x40, 0xef, 0x7c, 0xa4, 0xef, 0xff, 0xf5, 0x1f, 0x1f,
	0x5b, 0x30, 0xa9, 0x4b, 0x2a, 0x8f, 0xea, 0x22, 0x26, 0x15, 0xf5, 0x0b, 0x99, 0x57, 0x39, 0xda,
	0x57, 0x4b, 0xd3, 0x03, 0x7a, 0x4b, 0x45, 0x15, 0xd5, 0xe5, 0xd1, 0x32, 0x25, 0x22, 0x69, 0x36,
	0xbd, 0xbf, 0x03, 0x80, 0x9b, 0x92, 0xca, 0x1b, 0xad, 0x40, 0x87, 0x30, 0x50, 0x74, 0xc8, 0x62,
	0xdc, 0x9b, 0xf5, 0xe6, 0xc3, 0xc0, 0x56, 0xe5, 0x45, 0x8c, 0x16, 0x60, 0xf1, 0x3c, 0x61, 0x02,
	0xef, 0xcd, 0x7a, 0x73, 0xf7, 0xf8, 0xa5, 0xdf, 0xda, 0xf9, 0x8d, 0xdd, 0x75, 0x25, 0x99, 0x48,
	0xce, 0x74, 0x11, 0x18, 0x16, 0x7d, 0x82, 0x51, 0xcc, 0xca, 0x82, 0x93, 0x75, 0x28, 0x48, 0x46,
	0x71, 0x7f, 0x17, 0xad, 0xdb, 0x48, 0x2e, 0x49, 0x46, 0xd1, 0x02, 0x9c, 0x88, 0xc9, 0x2a, 0x8d,
	0xc9, 0x1a, 0xef, 0x6b, 0xf5, 0x61, 0x47, 0xdd, 0xe8, 0xee, 0x40, 0xf4, 0x0e, 0x2c, 0x9a, 0x11,
	0xc6, 0xb1, 0xf5, 0xb0, 0xc2, 0x50, 0xe8, 0x04, 0x46, 0x45, 0x9a, 0x0b, 0x1a, 0x8a, 0x3a, 0x8b,
	0xa8, 0xc4, 0xee, 0xc3, 0x2a, 0x57, 0xc3, 0x97, 0x9a, 0x45, 0x1f, 0xc0, 0xe1, 0x44, 0x24, 0x35,
	0x49, 0x28, 0xb6, 0x77, 0xe9, 0xee, 0x0e, 0x47, 0x1f, 0xc1, 0x8d, 0x69, 0xb9, 0x94, 0xac, 0xa8,
	0x58, 0x2e, 0xf0, 0x60, 0xb7, 0x77, 0xb3, 0x51, 0xa0, 0x73, 0x18, 0x17, 0x32, 0x5f, 0x31, 0x4e,
	0x43, 0x96, 0xa9, 0x00, 0xce, 0xac, 0x3f, 0x77, 0x8f, 0x3d, 0x5f, 0x8d, 0xcc, 0xdf, 0x0c, 0xd5,
	0xff, 0x6a, 0xa8, 0x0b, 0x05, 0x7d, 0x16, 0x95, 0x5c, 0x07, 0xa3, 0x62, 0x6b, 0x09, 0x7d, 0x81,
	0x27, 0xad, 0x51, 0x44, 0x84, 0xa0, 0x12, 0x0f, 0xb5, 0xd3, 0xeb, 0xfb, 0x9c, 0x4e, 0x35, 0x65,
	0xac, 0xda, 0x0c, 0x66, 0x0d, 0xfd, 0x84, 0x03, 0x15, 0x5c, 0x50, 0x1e, 0xe6, 0xab, 0x15, 0x67,
	0xa2, 0x0d, 0x07, 0xda, 0xf2, 0x6d, 0xc7, 0xf2, 0xcc, 0xd0, 0x57, 0x06, 0xde, 0xca, 0xf8, 0x6c,
	0xd9, 0xdd, 0x99, 0x06, 0x30, 0xe9, 0x74, 0x83, 0x9e, 0x42, 0xff, 0x17, 0x5d, 0x37, 0x1f, 0xac,
	0x7a, 0x44, 0x6f, 0xc0, 0xba, 0x25, 0xbc, 0xa6, 0xcd, 0xd7, 0x3a, 0x31, 0xa7, 0x6a, 0x49, 0x3b,
	0x7b, 0xbd, 0x7f, 0xb2, 0xf7, 0xbe, 0x37, 0xbd, 0x06, 0xd4, 0xed, 0xeb, 0xb1, 0xa6, 0xdf, 0x01,
	0xdf, 0xd7, 0xd9, 0x23, 0xad, 0xbd, 0x73, 0xb0, 0xcc, 0xdc, 0x5e, 0x80, 0x9d, 0x52, 0x96, 0xa4,
	0x95, 0xb6, 0x1a, 0x07, 0x4d, 0x85, 0x9e, 0x83, 0xf5, 0x9b, 0xc5, 0x55, 0xaa, 0xdd, 0xc6, 0x81,
	0x29, 0xd4, 0xa9, 0xb5, 0xe4, 0xfa, 0x1f, 0x1c, 0x06, 0xea, 0xd1, 0xfb, 0xd3, 0x03, 0x77, 0xeb,
	0x0c, 0x84, 0x61, 0x60, 0x2e, 0x0e, 0xf3, 0xf3, 0x3b, 0x41, 0x5b, 0xa2, 0x57, 0xff, 0xe7, 0x73,
	0xb7, 0xf2, 0x35, 0xc9, 0x90, 0x07, 0xe3, 0x94, 0x94, 0x61, 0xce, 0xe3, 0xd0, 0xa0, 0x7d, 0x6d,
	0xe1, 0xa6, 0xa4, 0xbc, 0xe2, 0xf1, 0x37, 0xcd, 0xcc, 0x61, 0xb8, 0xd9, 0xdf, 0xef, 0x5a, 0x39,
	0x79, 0x43, 0x9e, 0xda, 0x3f, 0xf4, 0xad, 0x15, 0xd9, 0xfa, 0x96, 0x5a, 0xfc, 0x0b, 0x00, 0x00,
	0xff, 0xff, 0x3b, 0x7a, 0x90, 0xfe, 0xd7, 0x04, 0x00, 0x00,
}
