package fhevent

import "github.com/aws/aws-sdk-go/service/firehose"

// Batch builds batches to send to firehose.
type Batch struct {
	batch       []*firehose.Record
	sizeInBytes int
}

// Add adds a record to the batch
func (fb *Batch) Add(item []byte) {
	fb.sizeInBytes += len(item)
	fb.batch = append(fb.batch, &firehose.Record{
		Data: item,
	})
}

// SizeInBytes returns the size of the batch in bytes
func (fb *Batch) SizeInBytes() int {
	return fb.sizeInBytes
}

// Len returns the length of the batch
func (fb *Batch) Len() int {
	return len(fb.batch)
}

// Batch returns the batch
func (fb *Batch) Batch() []*firehose.Record {
	return fb.batch
}

// Empty returns true if there are no items in the batch
func (fb *Batch) Empty() bool {
	return fb.sizeInBytes == 0
}

// Clear the batch
func (fb *Batch) Clear() {
	fb.batch = fb.batch[:0]
	fb.sizeInBytes = 0
}
