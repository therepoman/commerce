// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.25.0
// 	protoc        v3.13.0
// source: code.justin.tv/amzn/PDMSLambdaTwirp/get_deleted_user_data.proto

package PDMSLambdaTwirp

import (
	proto "github.com/golang/protobuf/proto"
	timestamp "github.com/golang/protobuf/ptypes/timestamp"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

// This is a compile-time assertion that a sufficiently up-to-date version
// of the legacy proto package is being used.
const _ = proto.ProtoPackageIsVersion4

// This gets the data of a deleted user. It should ONLY be invoked by parties who have a demonstrated need for it. S2S will guard this.
type GetDeletedUserDataRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	UserId string `protobuf:"bytes,1,opt,name=user_id,json=userId,proto3" json:"user_id,omitempty"`
}

func (x *GetDeletedUserDataRequest) Reset() {
	*x = GetDeletedUserDataRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_code_justin_tv_amzn_PDMSLambdaTwirp_get_deleted_user_data_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetDeletedUserDataRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetDeletedUserDataRequest) ProtoMessage() {}

func (x *GetDeletedUserDataRequest) ProtoReflect() protoreflect.Message {
	mi := &file_code_justin_tv_amzn_PDMSLambdaTwirp_get_deleted_user_data_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetDeletedUserDataRequest.ProtoReflect.Descriptor instead.
func (*GetDeletedUserDataRequest) Descriptor() ([]byte, []int) {
	return file_code_justin_tv_amzn_PDMSLambdaTwirp_get_deleted_user_data_proto_rawDescGZIP(), []int{0}
}

func (x *GetDeletedUserDataRequest) GetUserId() string {
	if x != nil {
		return x.UserId
	}
	return ""
}

type GetDeletedUserDataPayload struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	UserId                       string               `protobuf:"bytes,1,opt,name=user_id,json=userId,proto3" json:"user_id,omitempty"`
	Login                        string               `protobuf:"bytes,2,opt,name=login,proto3" json:"login,omitempty"`
	Email                        string               `protobuf:"bytes,3,opt,name=email,proto3" json:"email,omitempty"` // optional
	Created                      *timestamp.Timestamp `protobuf:"bytes,4,opt,name=created,proto3" json:"created,omitempty"`
	AssociatedExtensionClientIds []string             `protobuf:"bytes,5,rep,name=associated_extension_client_ids,json=associatedExtensionClientIds,proto3" json:"associated_extension_client_ids,omitempty"`
}

func (x *GetDeletedUserDataPayload) Reset() {
	*x = GetDeletedUserDataPayload{}
	if protoimpl.UnsafeEnabled {
		mi := &file_code_justin_tv_amzn_PDMSLambdaTwirp_get_deleted_user_data_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetDeletedUserDataPayload) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetDeletedUserDataPayload) ProtoMessage() {}

func (x *GetDeletedUserDataPayload) ProtoReflect() protoreflect.Message {
	mi := &file_code_justin_tv_amzn_PDMSLambdaTwirp_get_deleted_user_data_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetDeletedUserDataPayload.ProtoReflect.Descriptor instead.
func (*GetDeletedUserDataPayload) Descriptor() ([]byte, []int) {
	return file_code_justin_tv_amzn_PDMSLambdaTwirp_get_deleted_user_data_proto_rawDescGZIP(), []int{1}
}

func (x *GetDeletedUserDataPayload) GetUserId() string {
	if x != nil {
		return x.UserId
	}
	return ""
}

func (x *GetDeletedUserDataPayload) GetLogin() string {
	if x != nil {
		return x.Login
	}
	return ""
}

func (x *GetDeletedUserDataPayload) GetEmail() string {
	if x != nil {
		return x.Email
	}
	return ""
}

func (x *GetDeletedUserDataPayload) GetCreated() *timestamp.Timestamp {
	if x != nil {
		return x.Created
	}
	return nil
}

func (x *GetDeletedUserDataPayload) GetAssociatedExtensionClientIds() []string {
	if x != nil {
		return x.AssociatedExtensionClientIds
	}
	return nil
}

var File_code_justin_tv_amzn_PDMSLambdaTwirp_get_deleted_user_data_proto protoreflect.FileDescriptor

var file_code_justin_tv_amzn_PDMSLambdaTwirp_get_deleted_user_data_proto_rawDesc = []byte{
	0x0a, 0x3f, 0x63, 0x6f, 0x64, 0x65, 0x2e, 0x6a, 0x75, 0x73, 0x74, 0x69, 0x6e, 0x2e, 0x74, 0x76,
	0x2f, 0x61, 0x6d, 0x7a, 0x6e, 0x2f, 0x50, 0x44, 0x4d, 0x53, 0x4c, 0x61, 0x6d, 0x62, 0x64, 0x61,
	0x54, 0x77, 0x69, 0x72, 0x70, 0x2f, 0x67, 0x65, 0x74, 0x5f, 0x64, 0x65, 0x6c, 0x65, 0x74, 0x65,
	0x64, 0x5f, 0x75, 0x73, 0x65, 0x72, 0x5f, 0x64, 0x61, 0x74, 0x61, 0x2e, 0x70, 0x72, 0x6f, 0x74,
	0x6f, 0x12, 0x21, 0x74, 0x77, 0x69, 0x74, 0x63, 0x68, 0x2e, 0x66, 0x75, 0x6c, 0x74, 0x6f, 0x6e,
	0x2e, 0x70, 0x72, 0x69, 0x76, 0x61, 0x63, 0x79, 0x2e, 0x70, 0x64, 0x6d, 0x73, 0x73, 0x65, 0x72,
	0x76, 0x69, 0x63, 0x65, 0x1a, 0x1f, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2f, 0x70, 0x72, 0x6f,
	0x74, 0x6f, 0x62, 0x75, 0x66, 0x2f, 0x74, 0x69, 0x6d, 0x65, 0x73, 0x74, 0x61, 0x6d, 0x70, 0x2e,
	0x70, 0x72, 0x6f, 0x74, 0x6f, 0x22, 0x34, 0x0a, 0x19, 0x47, 0x65, 0x74, 0x44, 0x65, 0x6c, 0x65,
	0x74, 0x65, 0x64, 0x55, 0x73, 0x65, 0x72, 0x44, 0x61, 0x74, 0x61, 0x52, 0x65, 0x71, 0x75, 0x65,
	0x73, 0x74, 0x12, 0x17, 0x0a, 0x07, 0x75, 0x73, 0x65, 0x72, 0x5f, 0x69, 0x64, 0x18, 0x01, 0x20,
	0x01, 0x28, 0x09, 0x52, 0x06, 0x75, 0x73, 0x65, 0x72, 0x49, 0x64, 0x22, 0xdd, 0x01, 0x0a, 0x19,
	0x47, 0x65, 0x74, 0x44, 0x65, 0x6c, 0x65, 0x74, 0x65, 0x64, 0x55, 0x73, 0x65, 0x72, 0x44, 0x61,
	0x74, 0x61, 0x50, 0x61, 0x79, 0x6c, 0x6f, 0x61, 0x64, 0x12, 0x17, 0x0a, 0x07, 0x75, 0x73, 0x65,
	0x72, 0x5f, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x06, 0x75, 0x73, 0x65, 0x72,
	0x49, 0x64, 0x12, 0x14, 0x0a, 0x05, 0x6c, 0x6f, 0x67, 0x69, 0x6e, 0x18, 0x02, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x05, 0x6c, 0x6f, 0x67, 0x69, 0x6e, 0x12, 0x14, 0x0a, 0x05, 0x65, 0x6d, 0x61, 0x69,
	0x6c, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x05, 0x65, 0x6d, 0x61, 0x69, 0x6c, 0x12, 0x34,
	0x0a, 0x07, 0x63, 0x72, 0x65, 0x61, 0x74, 0x65, 0x64, 0x18, 0x04, 0x20, 0x01, 0x28, 0x0b, 0x32,
	0x1a, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75,
	0x66, 0x2e, 0x54, 0x69, 0x6d, 0x65, 0x73, 0x74, 0x61, 0x6d, 0x70, 0x52, 0x07, 0x63, 0x72, 0x65,
	0x61, 0x74, 0x65, 0x64, 0x12, 0x45, 0x0a, 0x1f, 0x61, 0x73, 0x73, 0x6f, 0x63, 0x69, 0x61, 0x74,
	0x65, 0x64, 0x5f, 0x65, 0x78, 0x74, 0x65, 0x6e, 0x73, 0x69, 0x6f, 0x6e, 0x5f, 0x63, 0x6c, 0x69,
	0x65, 0x6e, 0x74, 0x5f, 0x69, 0x64, 0x73, 0x18, 0x05, 0x20, 0x03, 0x28, 0x09, 0x52, 0x1c, 0x61,
	0x73, 0x73, 0x6f, 0x63, 0x69, 0x61, 0x74, 0x65, 0x64, 0x45, 0x78, 0x74, 0x65, 0x6e, 0x73, 0x69,
	0x6f, 0x6e, 0x43, 0x6c, 0x69, 0x65, 0x6e, 0x74, 0x49, 0x64, 0x73, 0x42, 0x25, 0x5a, 0x23, 0x63,
	0x6f, 0x64, 0x65, 0x2e, 0x6a, 0x75, 0x73, 0x74, 0x69, 0x6e, 0x2e, 0x74, 0x76, 0x2f, 0x61, 0x6d,
	0x7a, 0x6e, 0x2f, 0x50, 0x44, 0x4d, 0x53, 0x4c, 0x61, 0x6d, 0x62, 0x64, 0x61, 0x54, 0x77, 0x69,
	0x72, 0x70, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_code_justin_tv_amzn_PDMSLambdaTwirp_get_deleted_user_data_proto_rawDescOnce sync.Once
	file_code_justin_tv_amzn_PDMSLambdaTwirp_get_deleted_user_data_proto_rawDescData = file_code_justin_tv_amzn_PDMSLambdaTwirp_get_deleted_user_data_proto_rawDesc
)

func file_code_justin_tv_amzn_PDMSLambdaTwirp_get_deleted_user_data_proto_rawDescGZIP() []byte {
	file_code_justin_tv_amzn_PDMSLambdaTwirp_get_deleted_user_data_proto_rawDescOnce.Do(func() {
		file_code_justin_tv_amzn_PDMSLambdaTwirp_get_deleted_user_data_proto_rawDescData = protoimpl.X.CompressGZIP(file_code_justin_tv_amzn_PDMSLambdaTwirp_get_deleted_user_data_proto_rawDescData)
	})
	return file_code_justin_tv_amzn_PDMSLambdaTwirp_get_deleted_user_data_proto_rawDescData
}

var file_code_justin_tv_amzn_PDMSLambdaTwirp_get_deleted_user_data_proto_msgTypes = make([]protoimpl.MessageInfo, 2)
var file_code_justin_tv_amzn_PDMSLambdaTwirp_get_deleted_user_data_proto_goTypes = []interface{}{
	(*GetDeletedUserDataRequest)(nil), // 0: twitch.fulton.privacy.pdmsservice.GetDeletedUserDataRequest
	(*GetDeletedUserDataPayload)(nil), // 1: twitch.fulton.privacy.pdmsservice.GetDeletedUserDataPayload
	(*timestamp.Timestamp)(nil),       // 2: google.protobuf.Timestamp
}
var file_code_justin_tv_amzn_PDMSLambdaTwirp_get_deleted_user_data_proto_depIdxs = []int32{
	2, // 0: twitch.fulton.privacy.pdmsservice.GetDeletedUserDataPayload.created:type_name -> google.protobuf.Timestamp
	1, // [1:1] is the sub-list for method output_type
	1, // [1:1] is the sub-list for method input_type
	1, // [1:1] is the sub-list for extension type_name
	1, // [1:1] is the sub-list for extension extendee
	0, // [0:1] is the sub-list for field type_name
}

func init() { file_code_justin_tv_amzn_PDMSLambdaTwirp_get_deleted_user_data_proto_init() }
func file_code_justin_tv_amzn_PDMSLambdaTwirp_get_deleted_user_data_proto_init() {
	if File_code_justin_tv_amzn_PDMSLambdaTwirp_get_deleted_user_data_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_code_justin_tv_amzn_PDMSLambdaTwirp_get_deleted_user_data_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetDeletedUserDataRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_code_justin_tv_amzn_PDMSLambdaTwirp_get_deleted_user_data_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetDeletedUserDataPayload); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_code_justin_tv_amzn_PDMSLambdaTwirp_get_deleted_user_data_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   2,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_code_justin_tv_amzn_PDMSLambdaTwirp_get_deleted_user_data_proto_goTypes,
		DependencyIndexes: file_code_justin_tv_amzn_PDMSLambdaTwirp_get_deleted_user_data_proto_depIdxs,
		MessageInfos:      file_code_justin_tv_amzn_PDMSLambdaTwirp_get_deleted_user_data_proto_msgTypes,
	}.Build()
	File_code_justin_tv_amzn_PDMSLambdaTwirp_get_deleted_user_data_proto = out.File
	file_code_justin_tv_amzn_PDMSLambdaTwirp_get_deleted_user_data_proto_rawDesc = nil
	file_code_justin_tv_amzn_PDMSLambdaTwirp_get_deleted_user_data_proto_goTypes = nil
	file_code_justin_tv_amzn_PDMSLambdaTwirp_get_deleted_user_data_proto_depIdxs = nil
}
