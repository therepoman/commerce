package manager

func newTestDynamoSecret() *dynamoSecret {
	return &dynamoSecret{
		Tombstone:      false,
		Name:           "secretName",
		UpdatedAt:      742521600,
		Value:          "UmFuZG9tVmFsdWU=",
		Key:            "UmFuZG9tS2V5",
		KeyARN:         "Random ARN",
		DoNotBroadcast: false,
		Class:          3,
	}
}

func newTestSecret() *Secret {
	return &Secret{
		Name:      "secretName",
		Plaintext: []byte("plaintext"),
		UpdatedAt: 1,
		KeyARN:    "Random ARN",
		Class:     ClassDefault,
	}
}
