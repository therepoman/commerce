package permissions

const (
	//
	// Resources
	//

	Extension = "extension"
	Game      = "game"
	Company   = "company"
	User      = "user"

	//
	// Permissions
	//

	// Company
	EditCompanyInfo       = "editCompanyInfo"
	ShowCompanyMembers    = "showCompanyMembers"
	AddUser               = "addUser"
	AddUserBillingManager = "addUser::Billing_Manager"
	RemoveUser            = "removeUser"
	AssignPermissions     = "assignPermissions"

	// Users

	// Can see payment onboarding status of another user
	ViewPaymentOnboarding = "viewPaymentOnboarding"

	// Drops
	ViewDrops   = "drops::view"
	CreateDrops = "drops::create"
	ManageDrops = "drops::manage"
	DeleteDrops = "drops::delete"

	// Insights
	InsightsOnePagerView = "insights-1pager::view"

	// Games
	AddGames          = "gamesAdd"
	GameAnalyticsView = "games::analytics::view"
	GameBoxArtEdit    = "games::boxart::edit"

	// Extensions
	CreateExtensions      = "extensions::create"
	ManageExtensions      = "extensions::manage"
	MonetizeExtensions    = "extensions::monetize"
	ViewExtensionInsights = "extensions::insights"
	SetBillingManager     = "extensions::setBillingManager" // can assign extension billing managers
)
