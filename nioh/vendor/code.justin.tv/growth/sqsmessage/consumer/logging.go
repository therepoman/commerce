package consumer

import "log"

// LogContext contains queue specific values
type LogContext struct {
	QueueName string
}

// ErrorLogger is the interface for logging errors
type ErrorLogger interface {
	Log(ctx LogContext, err error)
}

// ErrorLoggerFunc is a convenience adapter type
type ErrorLoggerFunc func(LogContext, error)

func (l ErrorLoggerFunc) Log(ctx LogContext, err error) {
	l(ctx, err)
}

// DefaultErrorLogger is an ErrorLogger that prints to stderr
var DefaultErrorLogger ErrorLogger = ErrorLoggerFunc(func(ctx LogContext, err error) {
	log.Printf("sqsmessage [ERROR] [QueueName=%s]: %v", ctx.QueueName, err)
})
