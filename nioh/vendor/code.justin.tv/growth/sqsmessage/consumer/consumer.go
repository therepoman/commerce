package consumer

import (
	"context"
	"errors"
	"fmt"
	"strconv"
	"sync"
	"sync/atomic"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/aws/aws-sdk-go/service/sqs/sqsiface"
	"github.com/cactus/go-statsd-client/statsd"
	"golang.org/x/sync/semaphore"

	"code.justin.tv/hygienic/metricscactusstatsd"
)

var (
	allAttributeNames = []*string{aws.String("All")}
)

// Handler is a generic interface for processing SQS messages.
type Handler interface {
	Process(ctx context.Context, w ResultWriter, message *sqs.Message)
}

// HandlerFunc is an adapter to use a function as a Handler interface
type HandlerFunc func(context.Context, ResultWriter, *sqs.Message)

func (h HandlerFunc) Process(ctx context.Context, w ResultWriter, message *sqs.Message) {
	h(ctx, w, message)
}

// ResultWriter is used by consumers to handle SQS messages.
type ResultWriter interface {
	// Fail marks the message as failed. It will be reprocessed when the visibility timeout elapses.
	// This is useful if there is a transient error.
	Fail(err error)

	// Changes the message visibility. The duration should be divisible by time.Second and be
	// between 0 to 43200 seconds.
	ChangeMessageVisibility(dur time.Duration)
}

// resultWriter is an internal implementation of ResultWriter
type resultWriter struct {
	errorHandler             func(error)
	messageVisibilityChanger func(time.Duration)

	failed  bool
	changed bool
}

func (w *resultWriter) Fail(err error) {
	if w.failed {
		return
	}

	w.failed = true
	w.errorHandler(err)
}

func (w *resultWriter) ChangeMessageVisibility(dur time.Duration) {
	w.messageVisibilityChanger(dur)
}

// Consumer polls for and processes SQS messages. It has 'NumPollers' polling goroutines , and
// 'NumMessageProcessors' message processing goroutines. The default values are set conservatively,
// and it may be desirable to increase based on hardware capabilities and other considerations.
//
// Each poller receives 'MaxNumberOfMessages' messages from SQS ReceiveMessage. For each message, the poller
// sends each message to a buffered channel (capacity is determined by 'QueueCapacity'), and each message
// processor is receiving from the buffered channel. Since the poller is sending to a buffered channel, if the
// channel is at full capacity, the poller will block until the buffered channel has free capacity.

// Message processors will queue the SQS message for deletion if successfully processed.
//
// There is a single goroutine for batching up to 10 messages to delete. The batch of messages are deleted on a separate
// goroutine.
type Consumer struct {
	initialized bool
	started     bool

	// sqs related
	sqs                      sqsiface.SQSAPI
	accountID                string
	queueName                string
	queueURL                 string
	maxNumberOfMessages      int64
	numPollers               int
	numMessageProcessors     int
	keepMessageHidden        bool
	maxVisibilityTimeout     time.Duration
	defaultVisibilityTimeout time.Duration
	waitTimeSeconds          int64

	pendingQueue chan *sqs.Message

	deleteQueue       chan *sqs.Message
	deleteSemaphore   *semaphore.Weighted
	maxPendingDeletes int

	// message handler
	handler Handler

	// shutting down
	messageProcessorWg sync.WaitGroup
	deleteBatcherWg    sync.WaitGroup
	pollerWg           sync.WaitGroup
	wait               chan struct{}
	shutdown           chan struct{}
	shutdownTimeout    time.Duration
	shutdownOnce       sync.Once

	errorLogger ErrorLogger

	stats        statsd.StatSender
	taggingStats metricscactusstatsd.TaggingSubStatter

	// consumer metrics
	pollersActive           int32
	messageProcessorsActive int32
	deletesPending          int32
}

// ConsumerConfig is the configuration for a consumer
type ConsumerConfig struct {
	//
	// Required configuration
	//

	// SQS specifies the SQS service client. This is required because the client encapsulates
	// authentication, region, and other custom configurations - Required
	SQS sqsiface.SQSAPI

	// Handler is the SQS message handler - Required
	Handler Handler

	// QueueName is the SQS queue name - Required
	QueueName string

	//
	// Optional configuration
	//

	// NumPollers is the number of pollers to spawn. Defaults to 2.
	NumPollers int

	// NumMessageProcessors is the number of message processors to spawn. Defaults to 5.
	NumMessageProcessors int

	// QueueCapacity is the number of SQS messages to buffer. Defaults to 30.
	QueueCapacity int

	// ShutdownTimeout is the max amount of time to wait for messages to finish processing. If this timeout elapses
	// then Wait() will return, else Wait() will return when there are no more in-flight messages being processed.
	// Defaults to 3 minutes.
	ShutdownTimeout time.Duration

	// AccountID is the AWS account that created the queue. If unset, defaults
	// to the AWS account of the caller. This is useful for consuming from other
	// AWS accounts without using STS.
	AccountID string

	// MaxNumberOfMessages is the max number of messages SQS ReceiveMessage will return. Defaults to 1.
	// Must be from 1 to 10.
	MaxNumberOfMessages int

	// WaitTimeSeconds is the amount of time to wait for a message when SQS ReceiveMessage is called. Long-polling
	// is always enabled.
	// Defaults to 20. Must be from 1 to 20.
	WaitTimeSeconds int64

	// KeepMessageHidden will continuously extend a message periodically, up to the
	// MaxVisibilityTimeout. Defaults to false. If set to true, MaxVisibilityTimeout
	// must be set to a duration greater than 0.
	KeepMessageHidden bool

	// MaxVisibilityTimeout is the max duration a task can be hidden in the queue
	// before it is made visible to other SQS consumers. This duration should be
	// set to the longest time this message should take to process.
	MaxVisibilityTimeout time.Duration

	// ErrorLogger logs errors. Defaults to outputting to stderr
	ErrorLogger ErrorLogger

	// Stats is the statsd client. Defaults to nil (no stat emitting).
	Stats statsd.StatSender

	// TaggingStats is the tagging stats client. Defaults to nil (no stat emitting).
	TaggingStats metricscactusstatsd.TaggingSubStatter
}

// fillDefaults sets default field values
func (c *ConsumerConfig) fillDefaults() {
	if c.MaxNumberOfMessages == 0 {
		c.MaxNumberOfMessages = 1
	}

	if c.QueueCapacity == 0 {
		c.QueueCapacity = 30
	}

	if c.NumPollers == 0 {
		c.NumPollers = 2
	}

	if c.NumMessageProcessors == 0 {
		c.NumMessageProcessors = 5
	}

	if c.WaitTimeSeconds == 0 {
		c.WaitTimeSeconds = 20
	}

	if c.ShutdownTimeout == time.Duration(0) {
		c.ShutdownTimeout = 3 * time.Minute
	}
}

// validate checks fields
func (c *ConsumerConfig) validate() error {
	if c.SQS == nil {
		return errors.New("SQS cannot be nil")
	}

	if c.Handler == nil {
		return errors.New("handler cannot be nil")
	}

	if c.QueueName == "" {
		return errors.New("QueueName cannot be blank")
	}

	if c.MaxNumberOfMessages < 1 || c.MaxNumberOfMessages > 10 {
		return fmt.Errorf("MaxNumberOfMessages must be from 1 to 10: received %v", c.MaxNumberOfMessages)
	}

	if c.WaitTimeSeconds < 1 || c.WaitTimeSeconds > 20 {
		return fmt.Errorf("WaitTimeSeconds must be from 1 to 20: received %v", c.WaitTimeSeconds)
	}

	if c.QueueCapacity < 0 {
		return fmt.Errorf("QueueCapacity cannot be less than 0: received %v", c.QueueCapacity)
	}

	if c.NumPollers < 1 {
		return fmt.Errorf("NumPollers cannot be less than 1: received: %v", c.NumPollers)
	}

	if c.NumMessageProcessors < 1 {
		return fmt.Errorf("NumMessageProcessors cannot be less than 1: received: %v", c.NumPollers)
	}

	if c.KeepMessageHidden && c.MaxVisibilityTimeout == time.Duration(0) {
		return fmt.Errorf("MaxVisibilityTimeout should be greater than 0 when KeepMessageHidden is true: received: %v", c.MaxVisibilityTimeout)
	}

	return nil
}

// NewConsumer returns a new SQS message consumer
func NewConsumer(conf ConsumerConfig) (*Consumer, error) {
	conf.fillDefaults()

	if err := conf.validate(); err != nil {
		return nil, err
	}

	errorLogger := DefaultErrorLogger
	if conf.ErrorLogger != nil {
		errorLogger = conf.ErrorLogger
	}

	maxPendingDeletes := calcMaxPendingDeletes(conf.NumMessageProcessors)

	return &Consumer{
		handler:              conf.Handler,
		queueName:            conf.QueueName,
		sqs:                  conf.SQS,
		accountID:            conf.AccountID,
		numPollers:           conf.NumPollers,
		numMessageProcessors: conf.NumMessageProcessors,
		maxNumberOfMessages:  int64(conf.MaxNumberOfMessages),
		pendingQueue:         make(chan *sqs.Message, conf.QueueCapacity),
		deleteQueue:          make(chan *sqs.Message, 128*conf.NumMessageProcessors),
		deleteSemaphore:      semaphore.NewWeighted(int64(maxPendingDeletes)),
		maxPendingDeletes:    maxPendingDeletes,
		wait:                 make(chan struct{}),
		shutdown:             make(chan struct{}),
		shutdownTimeout:      conf.ShutdownTimeout,
		errorLogger:          errorLogger,
		keepMessageHidden:    conf.KeepMessageHidden,
		maxVisibilityTimeout: conf.MaxVisibilityTimeout,
		waitTimeSeconds:      conf.WaitTimeSeconds,
		stats:                conf.Stats,
		taggingStats:         conf.TaggingStats,
	}, nil
}

func (c *Consumer) QueueName() string {
	return c.queueName
}

func (c *Consumer) QueueURL() string {
	return c.queueURL
}

// Initialize sets up consumer state by retrieving queue remote state.
func (c *Consumer) Initialize() error {
	if c.initialized {
		return fmt.Errorf("consumer already initialized")
	}

	// if there is no configured accountID, ensure that QueueOwnerAWSAccountId is set to nil, instead
	// of a blank string
	var accountID *string
	if c.accountID != "" {
		accountID = &c.accountID
	}

	out, err := c.sqs.GetQueueUrl(&sqs.GetQueueUrlInput{
		QueueName:              &c.queueName,
		QueueOwnerAWSAccountId: accountID,
	})

	if err != nil {
		return fmt.Errorf("failed to retrieve queue URL: %v", err)
	}

	c.queueURL = *out.QueueUrl

	if c.keepMessageHidden {
		queueAttrsOut, err := c.sqs.GetQueueAttributes(&sqs.GetQueueAttributesInput{
			QueueUrl:       &c.queueURL,
			AttributeNames: allAttributeNames,
		})

		if err != nil {
			return fmt.Errorf("failed to retrieve queue attributes: %v", err)
		}

		defaultVisibilityTimeout, err := checkQueueAttrs(c.maxVisibilityTimeout, queueAttrsOut)
		if err != nil {
			return err
		}

		c.defaultVisibilityTimeout = defaultVisibilityTimeout
	}

	c.initialized = true

	return nil
}

// Start starts polling SQS for new messages.
func (c *Consumer) Start() error {
	if !c.initialized {
		return fmt.Errorf("consumer is not initialized")
	} else if c.started {
		return fmt.Errorf("consumer already started")
	}

	c.started = true

	c.pollerWg.Add(c.numPollers)
	for i := 0; i < c.numPollers; i++ {
		go c.poller()
	}

	c.messageProcessorWg.Add(c.numMessageProcessors)
	for i := 0; i < c.numMessageProcessors; i++ {
		go c.messageProcessor()
	}

	c.deleteBatcherWg.Add(1)
	go c.deleter()

	return nil
}

func calcMaxPendingDeletes(numMessageProcessors int) int {
	// scale the max pending deletes to a function of the number of message processors in the worst case. The deleter will
	// batch up to 10 messages when there is enough throughput in a window of 100ms.
	maxPendingDeletes := numMessageProcessors * 2
	// at minimum have at least 1 deleter
	if maxPendingDeletes < 1 {
		maxPendingDeletes = 1
	}

	return maxPendingDeletes
}

// checkQueueAttrs validates config values against the SQS configuration. This happens if `KeepMessageHidden` is true.
func checkQueueAttrs(maxVisibilityTimeout time.Duration, queueAttrsOut *sqs.GetQueueAttributesOutput) (time.Duration, error) {
	attrs := queueAttrsOut.Attributes

	messageRetentionPeriodSeconds, err := strconv.Atoi(aws.StringValue(attrs["MessageRetentionPeriod"]))
	if err != nil {
		return 0, fmt.Errorf("failed to get queue attribute 'MessageRetentionPeriod': %v", err)
	}

	messageRetentionPeriod := time.Duration(messageRetentionPeriodSeconds) * time.Second
	if maxVisibilityTimeout >= messageRetentionPeriod {
		return 0, fmt.Errorf("MaxVisibilityTimeout (%v) should be less than MessageRetentionPeriod (%v)",
			maxVisibilityTimeout, messageRetentionPeriod)
	}

	visibilityTimeoutSeconds, err := strconv.Atoi(aws.StringValue(attrs["VisibilityTimeout"]))
	if err != nil {
		return 0, fmt.Errorf("failed to get queue attribute 'VisibilityTimeout': %v", err)
	}

	// store the queue's visibility timeout for the first visibility timeout change
	defaultVisibilityTimeout := time.Duration(visibilityTimeoutSeconds) * time.Second

	if maxVisibilityTimeout < defaultVisibilityTimeout {
		return 0, fmt.Errorf("MaxVisibilityTimeout (%v) should be greater than VisibilityTimeout (%v)",
			maxVisibilityTimeout, defaultVisibilityTimeout)
	}

	if defaultVisibilityTimeout == 0 {
		return 0, fmt.Errorf("queue configured VisibilityTimeout (%v) should be greater than 0s", defaultVisibilityTimeout)
	}

	return defaultVisibilityTimeout, nil
}

func (c *Consumer) handleError(err error) {
	if err == nil {
		return
	}

	if c.errorLogger != nil {
		c.errorLogger.Log(LogContext{QueueName: c.queueName}, err)
	}
}

// poller continuously long polls
func (c *Consumer) poller() {
	atomic.AddInt32(&c.pollersActive, 1)
	defer func() {
		c.pollerWg.Done()
		atomic.AddInt32(&c.pollersActive, -1)
	}()

	for {
		select {
		case <-c.shutdown:
			return
		default:
		}

		c.poll()
	}
}

func (c *Consumer) poll() {
	out, err := c.receiveMessage()

	if err != nil {
		c.handleError(err)
		return
	}

	// this is intentionally a timing to get `median` and `upper`
	c.emitPollNumReceived(int64(len(out.Messages)))

	for _, message := range out.Messages {
		c.emitMessageQueueMetrics(message.Attributes)
	}

	for _, message := range out.Messages {
		c.pendingQueue <- message
	}
}

func (c *Consumer) emitMessageQueueMetrics(attrs map[string]*string) {
	if len(attrs) == 0 {
		return
	}

	// these are both epoch times in milliseconds
	sentTimestamp, sterr := strconv.ParseInt(aws.StringValue(attrs["SentTimestamp"]), 10, 0)
	receivedTimestamp, rterr := strconv.ParseInt(aws.StringValue(attrs["ApproximateFirstReceiveTimestamp"]), 10, 0)

	if sterr == nil && rterr == nil {
		c.emitMessageInQueue(receivedTimestamp - sentTimestamp)
	}

	receiveCount, rcerr := strconv.ParseInt(aws.StringValue(attrs["ApproximateReceiveCount"]), 10, 0)
	if rcerr == nil {
		// this is intentionally a timing to get `median` and `upper`
		c.emitMessageReceiveCount(receiveCount)
	}
}

func (c *Consumer) emitMessageProcessMetric(attrs map[string]*string) {
	if len(attrs) == 0 {
		return
	}

	// this is epoch time in milliseconds
	sentTimestamp, sterr := strconv.ParseInt(aws.StringValue(attrs["SentTimestamp"]), 10, 0)
	if sterr != nil {
		return
	}

	sent := time.Unix(0, sentTimestamp*int64(time.Millisecond))
	// the time since sending the message and processing the message
	c.emitMessageLatency(time.Since(sent))
}

func (c *Consumer) receiveMessage() (*sqs.ReceiveMessageOutput, error) {
	start := time.Now()
	defer func() {
		c.emitPollReceiveMessage(time.Since(start))
	}()

	// protect against the poller goroutine hanging on the ReceiveMessage call due
	// to unknown network issues. the long-poll is configured to be at most 20 seconds so it's unexpected to get a
	// context cancelled error.
	ctx, cancel := context.WithTimeout(context.Background(), 45*time.Second)
	defer cancel() // when `shutdown` is closed, cancelling this context causes the long-poll to terminate

	return c.sqs.ReceiveMessageWithContext(ctx, &sqs.ReceiveMessageInput{
		QueueUrl:              &c.queueURL,
		MaxNumberOfMessages:   &c.maxNumberOfMessages,
		AttributeNames:        allAttributeNames,
		MessageAttributeNames: allAttributeNames,
		WaitTimeSeconds:       &c.waitTimeSeconds,
	})
}

func (c *Consumer) messageProcessor() {
	atomic.AddInt32(&c.messageProcessorsActive, 1)
	defer c.messageProcessorWg.Done()
	defer atomic.AddInt32(&c.messageProcessorsActive, -1)

	for message := range c.pendingQueue {
		c.processMessage(message)
	}
}

func (c *Consumer) processMessage(message *sqs.Message) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	w := resultWriter{errorHandler: c.handleError}

	messageVisibilityChanger := func(dur time.Duration) {
		w.changed = true

		seconds := int64(dur / time.Second)

		err := c.changeMessageVisibility(ctx, &sqs.ChangeMessageVisibilityInput{
			ReceiptHandle:     message.ReceiptHandle,
			QueueUrl:          &c.queueURL,
			VisibilityTimeout: aws.Int64(seconds),
		})

		if err != nil {
			c.handleError(err)
		}
	}

	w.messageVisibilityChanger = messageVisibilityChanger

	c.handleMessage(ctx, message, &w)
	c.emitMessageProcessMetric(message.Attributes)

	if w.failed || w.changed {
		return
	}

	c.queueDelete(message)
}

func (c *Consumer) handleMessage(ctx context.Context, message *sqs.Message, w *resultWriter) {
	start := time.Now()
	defer func() {
		if w.failed {
			c.emitHandlerFail(time.Since(start))
		} else {
			c.emitHandlerSuccess(time.Since(start))
		}

	}()

	defer func() {
		if r := recover(); r != nil {
			err := fmt.Errorf("handler panic: %v", r)
			w.Fail(err)
		}
	}()

	if c.keepMessageHidden {
		go c.extendVisibilityTimeout(ctx, message.ReceiptHandle)
	}

	c.handler.Process(ctx, w, message)
}

const (
	// defaultVisibilityExtension is the duration to extend the visibility
	// timeout of a message that is currently being processed.
	defaultVisibilityExtension = 1 * time.Minute

	// visibilityExtensionBuffer is the time before a visibility timeout expires
	// to extend the visibility timeout.
	visibilityExtensionBuffer = 10 * time.Second
)

func (c *Consumer) extendVisibilityTimeout(ctx context.Context, receiptHandle *string) {
	maxVisibilityExceeded := time.NewTimer(c.maxVisibilityTimeout)
	defer maxVisibilityExceeded.Stop()

	input := &sqs.ChangeMessageVisibilityInput{
		ReceiptHandle:     receiptHandle,
		QueueUrl:          &c.queueURL,
		VisibilityTimeout: aws.Int64(int64(defaultVisibilityExtension.Seconds())),
	}

	// This timer could be negative, which will cause it to fire immediately
	almostExpired := time.NewTimer(c.defaultVisibilityTimeout - visibilityExtensionBuffer)
	defer almostExpired.Stop()

	select {
	case <-ctx.Done():
		// Handler completed: no need to keep task hidden.
		return
	case <-almostExpired.C:
	}

	err := c.changeMessageVisibility(ctx, input)
	if err != nil {
		c.handleError(err)
		return
	}

	ticker := time.NewTicker(defaultVisibilityExtension - visibilityExtensionBuffer)
	defer ticker.Stop()

	for {
		select {
		case <-ctx.Done():
			// Handler completed: no need to keep task hidden.
			return
		case <-maxVisibilityExceeded.C:
			// Max visibility timeout duration reached: let task become visible to other workers.
			return
		case <-ticker.C:
			err := c.changeMessageVisibility(ctx, input)
			// One possibility for a 400 error is InvalidParameterValueError. This is possible if 2+ workers are processing
			// the same message and one worker deletes the message.
			if err != nil {
				c.handleError(err)
			}
		}
	}
}

func (c *Consumer) changeMessageVisibility(parentCtx context.Context, input *sqs.ChangeMessageVisibilityInput) error {
	start := time.Now()

	ctx, cancel := context.WithTimeout(parentCtx, 15*time.Second)
	defer cancel()
	_, err := c.sqs.ChangeMessageVisibilityWithContext(ctx, input)

	if err != nil {
		c.emitChangeMessageVisibilityFail(time.Since(start))
	} else {
		c.emitChangeMessageVisibilitySuccess(time.Since(start))
	}

	return err
}

const (
	deleteBatchSize = 10
)

func (c *Consumer) deleter() {
	defer c.deleteBatcherWg.Done()

	// When this loop exits, deleteQueue's length is 0.
	for {
		messagesToDelete := make([]*sqs.Message, 1, deleteBatchSize)
		message, ok := <-c.deleteQueue
		if !ok {
			return
		}

		messagesToDelete[0] = message

		// After time expires or deleteBatchSize, whichever comes first, then delete the messages.
		timer := time.NewTimer(100 * time.Millisecond)
	outerLoop:
		for {
			select {
			case anotherMessage, ok := <-c.deleteQueue:
				if !ok {
					break outerLoop
				}

				messagesToDelete = append(messagesToDelete, anotherMessage)

				if len(messagesToDelete) == deleteBatchSize {
					break outerLoop
				}
			case <-timer.C:
				break outerLoop
			}
		}
		timer.Stop()

		c.asyncDeleteMessageBatch(messagesToDelete)
	}
}

func (c *Consumer) asyncDeleteMessageBatch(messages []*sqs.Message) {
	ctx := context.Background()

	l := len(messages)

	c.emitMessageDeleteBatchSize(int64(l))

	if err := c.deleteSemaphore.Acquire(ctx, 1); err != nil {
		c.handleError(fmt.Errorf("acquiring semaphore: %v", err))
		return
	}

	go func() {
		atomic.AddInt32(&c.deletesPending, int32(l))
		defer func() {
			atomic.AddInt32(&c.deletesPending, -1*int32(l))
			c.deleteSemaphore.Release(1)
		}()

		err := c.deleteMessageBatch(messages)
		if err != nil {
			c.handleError(err)
		}
	}()
}

func (c *Consumer) deleteMessageBatch(messages []*sqs.Message) error {
	if len(messages) == 0 {
		return errors.New("empty messages")
	}

	if len(messages) > deleteBatchSize {
		return errors.New("too many messages")
	}

	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	entries := make([]*sqs.DeleteMessageBatchRequestEntry, len(messages))
	for i, message := range messages {
		entries[i] = &sqs.DeleteMessageBatchRequestEntry{
			Id:            aws.String(fmt.Sprintf("%d", i)),
			ReceiptHandle: message.ReceiptHandle,
		}
	}

	start := time.Now()
	var success bool
	defer func() {
		if success {
			c.emitMessageDeleteBatchSuccess(time.Since(start))
		} else {
			c.emitMessageDeleteBatchFail(time.Since(start))
		}
	}()

	out, err := c.sqs.DeleteMessageBatchWithContext(ctx, &sqs.DeleteMessageBatchInput{
		Entries:  entries,
		QueueUrl: &c.queueURL,
	})

	if err != nil {
		success = false
		return err
	}

	if len(out.Failed) > 0 {
		err = c.retryDeleteMessageBatch(ctx, entries, out.Failed)
		if err != nil {
			success = false
			return err
		}
	}

	success = true
	return nil
}

func (c *Consumer) retryDeleteMessageBatch(ctx context.Context, entries []*sqs.DeleteMessageBatchRequestEntry, failedEntries []*sqs.BatchResultErrorEntry) error {
	// construct the entries to retry
	retryEntries := make([]*sqs.DeleteMessageBatchRequestEntry, len(failedEntries))

	for i, entry := range failedEntries {
		if entry.Id == nil {
			return fmt.Errorf("batch result entry is malformed, no Id: %v", entry)
		}

		index, err := strconv.Atoi(aws.StringValue(entry.Id))
		if err != nil {
			return fmt.Errorf("batch result entry ID is not a number: %v", entry.Id)
		}

		if index < 0 || index >= len(entries) {
			return fmt.Errorf("batch result entry ID is out of range: %d [%d,%d)", index, 0, len(entries))
		}

		retryEntries[i] = entries[index]
	}

	out, err := c.sqs.DeleteMessageBatchWithContext(ctx, &sqs.DeleteMessageBatchInput{
		Entries:  retryEntries,
		QueueUrl: &c.queueURL,
	})

	if err != nil {
		return err
	}

	if len(out.Failed) > 0 {
		return fmt.Errorf("messages failed to delete: %v", out.Failed)
	}

	return nil
}

func (c *Consumer) queueDelete(message *sqs.Message) {
	c.deleteQueue <- message
}

// Shutdown signals the pollers and message processors to exit. This is blocking.
// It is safe to run this on a goroutine and call Wait() to block on shutdown; this is
// useful if there are multiple consumers that should be shutdown in parallel.
func (c *Consumer) Shutdown() {
	c.shutdownOnce.Do(c.gracefulShutdown)
}

func (c *Consumer) gracefulShutdown() {
	done := make(chan struct{}) // closed when pollers and message processors exit

	go func() {
		close(c.shutdown) // starts the process of shutting down; this signals pollers to eventually exit
		c.pollerWg.Wait()

		// all pollers exited so this is safe to close because nothing else
		// will be sending to this channel; closing causes messages processors
		// to eventually exit.
		close(c.pendingQueue)

		c.messageProcessorWg.Wait()

		// all message processors exited so this is safe to close because nothing
		// else will be sending to this channel; closing causes deleters to
		// eventually exit.
		close(c.deleteQueue)
		c.deleteBatcherWg.Wait()

		// wait for all pending deletes to finish by acquiring all the weight of the sempahore.
		ctx := context.Background()
		if err := c.deleteSemaphore.Acquire(ctx, int64(c.maxPendingDeletes)); err != nil {
			c.handleError(fmt.Errorf("acquiring semaphore: %v", err))
		}

		close(done)
	}()

	timer := time.NewTimer(c.shutdownTimeout)
	defer timer.Stop()

	// wait for all pollers and message processors to exit, or for the shutdown timeout to elapse, which
	// ever happens first
	select {
	case <-timer.C:
	case <-done:
	}

	// report any internal goroutines still active
	pollersActive := atomic.LoadInt32(&c.pollersActive)
	if pollersActive > 0 {
		c.handleError(fmt.Errorf("shutdown with %d pollers still active", pollersActive))
	}

	messageProcessorsActive := atomic.LoadInt32(&c.messageProcessorsActive)
	if messageProcessorsActive > 0 {
		c.handleError(fmt.Errorf("shutdown with %d message processors still active; %d messages are buffered",
			messageProcessorsActive, len(c.pendingQueue)))
	}

	deletesPending := atomic.LoadInt32(&c.deletesPending)
	if deletesPending > 0 {
		c.handleError(fmt.Errorf("shutdown with %d message deletes still pending", deletesPending))
	}

	close(c.wait) // unblock any callers blocking on Wait()
}

// Wait blocks until all the pollers stop and messages processors exit. A shutdown
// should be initiated by calling Shutdown().
func (c *Consumer) Wait() {
	<-c.wait
}

// Metrics
func (c *Consumer) emitTiming(stat string, val int64) {
	if c.stats != nil {
		c.stats.Timing(c.statName(stat), val, 1.0)
	}
}

func (c *Consumer) emitTimingDuration(stat string, dur time.Duration) {
	if c.stats != nil {
		c.stats.TimingDuration(c.statName(stat), dur, 1.0)
	}
}

func (c *Consumer) incD(stat string, operation string, val int64) {
	if c.taggingStats != nil {
		c.taggingStats.IncD(stat, c.statTags(operation), val)
	}
}

func (c *Consumer) timingDurationD(stat string, operation string, dur time.Duration) {
	if c.taggingStats != nil {
		c.taggingStats.TimingDurationD(stat, c.statTags(operation), dur)
	}
}

func (c *Consumer) statName(stat string) string {
	return "service." + c.queueName + ".consumer." + stat
}

func (c *Consumer) emitPollNumReceived(val int64) {
	// this is intentionally a timing to get `median` and `upper`
	c.emitTiming("poll.num_received", val)
	c.incD("num_received", "poll", val)
}

func (c *Consumer) emitPollReceiveMessage(val time.Duration) {
	c.emitTimingDuration("poll.receive_message", val)
	c.timingDurationD("receive", "poll", val)
}

func (c *Consumer) emitHandlerFail(val time.Duration) {
	c.emitTimingDuration("handler.fail", val)
	c.timingDurationD("fail", "message", val)
}

func (c *Consumer) emitHandlerSuccess(val time.Duration) {
	c.emitTimingDuration("handler.success", val)
	c.timingDurationD("success", "message", val)
}

func (c *Consumer) emitMessageInQueue(val int64) {
	c.emitTiming("message.in_queue", val)
	c.timingDurationD("in_queue", "message", time.Duration(val)*time.Millisecond)

}

func (c *Consumer) emitMessageReceiveCount(val int64) {
	// this is intentionally a timing to get `median` and `upper`
	c.emitTiming("message.receive_count", val)
	c.incD("receive_count", "message", val)
}

func (c *Consumer) emitMessageLatency(val time.Duration) {
	c.emitTimingDuration("message.latency", val)
	c.timingDurationD("latency", "message", val)
}

func (c *Consumer) emitChangeMessageVisibilityFail(val time.Duration) {
	c.emitTimingDuration("change_message_visibility.fail", val)
	c.timingDurationD("fail", "change_message_visibility", val)
}

func (c *Consumer) emitChangeMessageVisibilitySuccess(val time.Duration) {
	c.emitTimingDuration("change_message_visibility.success", val)
	c.timingDurationD("success", "change_message_visibility", val)
}

func (c *Consumer) emitMessageDeleteBatchSize(val int64) {
	c.emitTiming("message.delete_batch_size", val)
	c.incD("size", "delete_batch", val)
}

func (c *Consumer) emitMessageDeleteBatchSuccess(val time.Duration) {
	c.emitTimingDuration("message.delete_batch.success", val)
	c.timingDurationD("success", "delete_batch", val)

}

func (c *Consumer) emitMessageDeleteBatchFail(val time.Duration) {
	c.emitTimingDuration("message.delete_batch.fail", val)
	c.timingDurationD("fail", "delete_batch", val)
}

func (c *Consumer) statTags(operation string) map[string]string {
	return map[string]string{"Queue": c.queueName, "Operation": operation}
}
