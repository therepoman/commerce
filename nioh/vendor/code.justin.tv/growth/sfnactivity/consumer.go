package sfnactivity

import (
	"context"
	"encoding/json"
	"fmt"
	"sync"
	"sync/atomic"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/sfn"
	"github.com/aws/aws-sdk-go/service/sfn/sfniface"
	uuid "github.com/satori/go.uuid"
)

const (
	// runtimeErrorCode is the default error code sent in SendTaskFailure for an error returned
	// by a Handler
	runtimeErrorCode = "sfnactivity.RuntimeError"
)

const (
	defaultNumPollers      = 5
	defaultShutdownTimeout = 3 * time.Minute
	defaultHeartbeatPeriod = 5 * time.Second
	requestTimeout         = 15 * time.Second
)

// Consumer polls for and performs AWS Step Function activity tasks. It has 'NumPollers' polling goroutines.
// Tasks are performed on the same polling goroutine, so there may be a max of 'NumPollers" goroutines performing
// in parallel.
//
// Caveat: SFN may try to schedule tasks to disconnected workers still in the 60 second window. As a best practice
// configure retries, timeouts, and heartbeats on your tasks.
type Consumer struct {
	sfn               sfniface.SFNAPI
	activityARN       string
	numPollers        int
	handler           Handler
	workerName        string
	disableHeartbeats bool
	errorLogger       ErrorLogger
	heartbeatPeriod   time.Duration
	wait              chan struct{}
	shutdown          chan struct{}
	pollersActive     int32
	pollerWg          sync.WaitGroup
	shutdownOnce      sync.Once
	shutdownTimeout   time.Duration
}

type TaskError struct {
	ErrorCode string
	Cause     string
}

func (e TaskError) Error() string {
	return fmt.Sprintf("sfnactivity: task error errorCode=%q cause=%q", e.ErrorCode, e.Cause)
}

var _ error = TaskError{}

func NewTaskError(errorCode string, cause string) error {
	return TaskError{
		ErrorCode: errorCode,
		Cause:     cause,
	}
}

type Handler interface {
	// Do does the actual task. If the returned error is non-nil, then SendTaskFailure is requested, otherwise
	// SendTaskSuccess is requested.
	//
	// If interface{} is nil, it will be marshaled into a JSON null
	//
	// If the error is of type TaskError, then the custom ErrorCode and Cause
	// are sent in SendTaskFailure. This is useful if you want to catch specific errors
	// in your state machine, otherwise the ErrorCode is "sfnactivity.RuntimeError", and
	// the cause is the error string.
	Do(ctx context.Context, input []byte) (interface{}, error)
}

// HandlerFunc is an adapter to use a function as a Handler interface
type HandlerFunc func(ctx context.Context, input []byte) (interface{}, error)

func (f HandlerFunc) Do(ctx context.Context, input []byte) (interface{}, error) {
	return f(ctx, input)
}

// NewConsumer returns a new SFN activity consumer
func NewConsumer(conf Config) (*Consumer, error) {
	conf.fillDefaults()

	if err := conf.validate(); err != nil {
		return nil, err
	}

	return &Consumer{
		sfn:               conf.SFN,
		activityARN:       conf.ActivityARN,
		numPollers:        conf.NumPollers,
		handler:           conf.Handler,
		workerName:        conf.WorkerName,
		heartbeatPeriod:   conf.HeartbeatPeriod,
		disableHeartbeats: conf.DisableHeartbeats,
		errorLogger:       conf.ErrorLogger,
		wait:              make(chan struct{}),
		shutdown:          make(chan struct{}),
		shutdownTimeout:   conf.ShutdownTimeout,
	}, nil
}

// Note: The taskToken is needed for heartbeating, sending task success, and sending task failure

// Config is the configuration for a consumer
type Config struct {
	// Required - SFN specifies the SFN service client. This is required because the client encapsulates
	// authentication, region, and other custom configurations.
	SFN sfniface.SFNAPI

	// Required - The ARN of the activity to poll tasks from.
	ActivityARN string

	// Required - The activity handler.
	Handler Handler

	// ShutdownTimeout is the max amount of time to wait for tasks to finish performing. If this timeout elapses
	// then Wait() will return, else Wait() will return when there are no more in-flight tasks being processed.
	// Defaults to 3 minutes.
	ShutdownTimeout time.Duration

	// Optional - How often to update SFN that the task is still running.
	// Defaults to 5 seconds.
	HeartbeatPeriod time.Duration

	// Optional - NumPollers is the number of pollers to spawn.
	// Defaults to 5.
	NumPollers int

	// Optional - WorkerName is used to identify the name of the worker
	// that a task is assigned to.
	// Defaults to a random UUID
	WorkerName string

	// Optional - ErrorLogger logs errors
	// Defaults to outputting to stderr.
	ErrorLogger ErrorLogger

	// Optional - Disables heartbeating
	// Defaults to false
	DisableHeartbeats bool
}

func (c *Config) validate() error {
	if c.SFN == nil {
		return ValidationError{"SFN", "cannot be nil"}
	}

	if c.Handler == nil {
		return ValidationError{"Handler", "cannot be nil"}
	}

	if c.ActivityARN == "" {
		return ValidationError{"ActivityARN", "cannot be blank"}
	}

	if c.NumPollers <= 0 {
		return ValidationError{"NumPollers", "must be greater than 0"}
	}

	if c.WorkerName == "" {
		return ValidationError{"WorkerName", "cannot be blank"}
	}

	if len(c.WorkerName) > 80 {
		return ValidationError{"WorkerName", "length cannot be greater than 80"}
	}

	if c.ShutdownTimeout <= 0 {
		return ValidationError{"ShutdownTimeout", "must be greater than 0"}
	}

	if c.HeartbeatPeriod <= 0 {
		return ValidationError{"HeartbeatPeriod", "must be greater than 0"}
	}

	return nil
}

func (c *Config) fillDefaults() {
	if c.NumPollers == 0 {
		c.NumPollers = defaultNumPollers
	}

	if c.WorkerName == "" {
		c.WorkerName = uuid.NewV4().String()
	}

	if c.ShutdownTimeout == 0 {
		c.ShutdownTimeout = defaultShutdownTimeout
	}

	if c.HeartbeatPeriod == 0 {
		c.HeartbeatPeriod = defaultHeartbeatPeriod
	}

	if c.ErrorLogger == nil {
		c.ErrorLogger = DefaultErrorLogger
	}
}

func (c *Consumer) WorkerName() string {
	return c.workerName
}

func (c *Consumer) ActivityARN() string {
	return c.activityARN
}

// Validate does a runtime check to see if the activity exists. This is useful if
// you have many consumers and want to check if they will all start.
func (c *Consumer) Validate() error {
	ctx, cancel := context.WithTimeout(context.Background(), requestTimeout)
	defer cancel()

	_, err := c.sfn.DescribeActivityWithContext(ctx, &sfn.DescribeActivityInput{
		ActivityArn: &c.activityARN,
	})

	if err != nil {
		return RequestError{
			"DescribeActivity",
			fmt.Sprintf("unexpected error describing activity ARN %q. The activity must exist and the machine "+
				"must have IAM permissions to describe it.", c.activityARN),
			err,
		}
	}

	return nil
}

// Start starts polling SFN for new activity tasks
func (c *Consumer) Start() {
	c.pollerWg.Add(c.numPollers)
	for i := 0; i < c.numPollers; i++ {
		go c.crank()
	}
}

func (c *Consumer) Wait() {
	<-c.wait
}

func (c *Consumer) Shutdown() {
	c.shutdownOnce.Do(c.gracefulShutdown)
}

func (c *Consumer) crank() {
	atomic.AddInt32(&c.pollersActive, 1)
	defer c.pollerWg.Done()
	defer atomic.AddInt32(&c.pollersActive, -1)

	for {
		select {
		case <-c.shutdown:
			return
		default:
		}

		c.poll()
	}
}

// poll will long-poll for an activity task. If a task is received, the task handler
// is invoked, and either SendTaskSuccess or SendTaskFailure is requested.
func (c *Consumer) poll() {
	getActivityTaskOut, err := c.getActivityTask()

	if err != nil {
		c.logError(RequestError{"GetActivityTask", "unexpected error", err})
		return
	}

	// long-poll returned no task
	taskToken := getActivityTaskOut.TaskToken
	if aws.StringValue(taskToken) == "" {
		return
	}

	handlerOut, err := c.doHandler(getActivityTaskOut)

	if err != nil {
		c.logError(err)

		serr := c.sendTaskFailure(err, taskToken)
		if serr != nil {
			c.logError(serr)
		}

		return
	}

	err = c.sendTaskSuccess(handlerOut, taskToken)
	if err != nil {
		c.logError(err)
	}
}

// doHandler has named return params so the potential recovered error is returned.
func (c *Consumer) doHandler(getActivityTaskOut *sfn.GetActivityTaskOutput) (handlerOut interface{}, err error) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	if !c.disableHeartbeats {
		go c.crankHeartbeats(ctx, getActivityTaskOut.TaskToken)
	}

	defer func() {
		if r := recover(); r != nil {
			err = fmt.Errorf("handler panic: %v", r)
		}
	}()

	input := []byte(aws.StringValue(getActivityTaskOut.Input))
	handlerOut, err = c.handler.Do(ctx, input)

	return
}

func (c *Consumer) crankHeartbeats(parentCtx context.Context, taskToken *string) {
	ticker := time.NewTicker(c.heartbeatPeriod)
	defer ticker.Stop()
	for {
		select {
		case <-parentCtx.Done():
			return
		case <-ticker.C:
			go c.sendTaskHeartbeat(parentCtx, taskToken)
		}
	}
}

func (c *Consumer) sendTaskHeartbeat(parentCtx context.Context, taskToken *string) {
	ctx, cancel := context.WithTimeout(context.Background(), requestTimeout)
	defer cancel()

	_, err := c.sfn.SendTaskHeartbeatWithContext(ctx, &sfn.SendTaskHeartbeatInput{
		TaskToken: taskToken,
	})

	if err != nil {
		select {
		case <-parentCtx.Done():
			// if the task has just completed, it's possible that a
			// SendTaskHeartbeat happens right after SendTaskSuccess; the SFN service
			// returns a "TaskTimedOut" error code (???). Checking the parent
			// context here for completion should handle squashing this error.
			return
		default:
			c.logError(RequestError{"SendTaskHeartbeat", "unexpected error", err})
		}
	}
}

func (c *Consumer) sendTaskSuccess(output interface{}, taskToken *string) error {
	ctx, cancel := context.WithTimeout(context.Background(), requestTimeout)
	defer cancel()

	outputBytes, err := json.Marshal(output)
	if err != nil {
		return SerializationError{"failed to JSON marshal SendTaskSuccess output", err}
	}

	_, err = c.sfn.SendTaskSuccessWithContext(ctx, &sfn.SendTaskSuccessInput{
		Output:    aws.String(string(outputBytes)),
		TaskToken: taskToken,
	})

	if err != nil {
		return RequestError{"SendTaskSuccess", "unexpected error", err}
	}

	return nil
}

func (c *Consumer) sendTaskFailure(handlerError error, taskToken *string) error {
	ctx, cancel := context.WithTimeout(context.Background(), requestTimeout)
	defer cancel()

	var errorCode, cause string
	taskError, ok := handlerError.(TaskError)

	if ok {
		errorCode = taskError.ErrorCode
		cause = taskError.Cause
	} else {
		errorCode = runtimeErrorCode
		cause = handlerError.Error()
	}

	_, err := c.sfn.SendTaskFailureWithContext(ctx, &sfn.SendTaskFailureInput{
		Error:     &errorCode,
		Cause:     &cause,
		TaskToken: taskToken,
	})

	if err != nil {
		return RequestError{"SendTaskFailure", "unexpected error", err}
	}

	return nil
}

type getActivityTaskResult struct {
	out *sfn.GetActivityTaskOutput
	err error
}

// From SFN documentation: http://docs.aws.amazon.com/step-functions/latest/apireference/API_GetActivityTask.html
//
// "The maximum time the service holds on to the request before responding is 60 seconds. If no task is available
//  within 60 seconds, the poll will return a taskToken with a null string."
func (c *Consumer) getActivityTask() (*sfn.GetActivityTaskOutput, error) {
	// protect against the poller goroutine hanging on the GetActivityTask call due
	// to unknown network issues.
	ctx, cancel := context.WithTimeout(context.Background(), 80*time.Second) // 1m + buffer
	defer cancel()

	result := make(chan getActivityTaskResult, 1)
	go func() {
		defer close(result)

		out, err := c.sfn.GetActivityTaskWithContext(ctx, &sfn.GetActivityTaskInput{
			ActivityArn: &c.activityARN,
			WorkerName:  &c.workerName,
		})

		result <- getActivityTaskResult{
			out: out,
			err: err,
		}
	}()

	select {
	case <-c.shutdown:
		// Caveat: Even though on shutdown we cancel the request, the connection from
		// SFN's end still thinks it could be open within a 60 second window, so SFN
		// may send to a disconnected worker.
		return &sfn.GetActivityTaskOutput{}, nil
	case r, ok := <-result:
		if !ok {
			return &sfn.GetActivityTaskOutput{}, nil
		}

		return r.out, r.err
	}
}

func (c *Consumer) logError(err error) {
	c.errorLogger.Log(LogContext{ActivityARN: c.activityARN}, err)
}

func (c *Consumer) gracefulShutdown() {
	done := make(chan struct{})
	go func() {
		// starts the process of shutting down; this signals the pollers
		// to eventually exit
		close(c.shutdown)
		c.pollerWg.Wait()
		close(done)
	}()

	timer := time.NewTimer(c.shutdownTimeout)
	defer timer.Stop()

	// wait for all pollers to exit, or for the shutdown timeout to elapse, whichever
	// happens first
	select {
	case <-timer.C:
	case <-done:
	}

	// report any internal goroutines still active
	pollersActive := atomic.LoadInt32(&c.pollersActive)
	if pollersActive > 0 {
		c.logError(fmt.Errorf("shutdown with %d pollers still active", pollersActive))
	}

	close(c.wait)
}
