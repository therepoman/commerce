# sfnactivity

sfnactivity is a Go library for performing [AWS StepFunction activities](https://docs.aws.amazon.com/step-functions/latest/dg/concepts-activities.html)

## Consumer

# Usage

  ```go
  package main

  import (
    "log"
    "os"
    "os/signal"

    "code.justin.tv/growth/sfnactivity"

    "github.com/aws/aws-sdk-go/aws"
    "github.com/aws/aws-sdk-go/aws/endpoints"
    "github.com/aws/aws-sdk-go/aws/session"
    "github.com/aws/aws-sdk-go/service/sfn"
    "golang.org/x/net/context"
  )

  // This demo sets up a SFN activity task consumer for "activityARN". The handler will succeed the task with
  // an empty JSON object.

  // State machine example
  /*
  {
    "Comment": "Executes a single activity task",
    "StartAt": "Test",
    "States": {
      "Test": {
        "Type": "Task",
        "TimeoutSeconds": 120,
        "HeartbeatSeconds": 10,
        "Retry": [
          {
            "ErrorEquals": ["States.ALL"],
            "MaxAttempts": 5
          }
        ],
        "Resource": "arn:aws:states:us-west-2:330176225010:activity:sfnactivitytesting",
        "End": true
      }
    }
  }
  */

  const (
    activityARN = "arn:aws:states:us-west-2:330176225010:activity:sfnactivitytesting"
  )

  type TaskHandler struct{}

  func (p *TaskHandler) Do(ctx context.Context, input []byte) (interface{}, error) {
    log.Println("received task with input", string(input))
    return map[string]string{}, nil

    // Alternative case for returning an error
    //
    // return nil, sfnactivity.TaskError{
    //   ErrorCode: "SomeCustomError",
    //   Cause:     "Some cause",
    // }
  }

  func main() {
    svc := sfn.New(session.Must(session.NewSession(&aws.Config{
      Region: aws.String(endpoints.UsWest2RegionID),
    })))

    // Setup the sfnactivity task consumer
    consumer, err := sfnactivity.NewConsumer(sfnactivity.Config{
      SFN:         svc,
      ActivityARN: activityARN,
      Handler:     &TaskHandler{},
    })

    if err != nil {
      log.Fatalf("failed to create sfnactivity consumer: %v", err)
    }

    if err := consumer.Validate(); err != nil {
      log.Fatalf("failed to validate sfnactivity consumer: %v", err)
    }

    consumer.Start()

    log.Printf("sfnactivity consumer started WorkerName=%s. You can now go into the console and execute the test state machine", consumer.WorkerName())

    interruptCh := make(chan os.Signal, 1)
    signal.Notify(interruptCh, os.Interrupt)
    <-interruptCh

    go consumer.Shutdown()

    log.Println("waiting for consumer to to gracefully exit")
    consumer.Wait()
    log.Println("done")
  }
  ```

## IAM permissions

Your application will need IAM permissions for performing step function tasks.

  ```
  states:GetActivityTask
  states:SendTaskFailure
  states:SendTaskHeartbeat
  states:SendTaskSuccess
  ```

## Examples

See the `examples/` directory for more advanced usages.

# Development

Install vendored dependencies

```
$ make deps
```

Run tests
```
$ make test
```
