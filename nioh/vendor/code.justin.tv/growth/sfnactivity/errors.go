package sfnactivity

import (
	"fmt"
)

// ValidationError is returned on bad inputs to NewConsumer.
type ValidationError struct {
	Field   string
	Message string
}

func (e ValidationError) Error() string {
	return fmt.Sprintf("sfnactivity: validation error: %s: %s", e.Field, e.Message)
}

type RequestError struct {
	Operation string
	Message   string
	OrigError error
}

func (e RequestError) Error() string {
	msg := fmt.Sprintf("sfnactivity: %s error: %s", e.Operation, e.Message)

	if e.OrigError != nil {
		msg = fmt.Sprintf("%s\ncaused by: %s", msg, e.OrigError.Error())
	}

	return msg
}

// SerializationError represents any marshaling errors
type SerializationError struct {
	Message   string
	OrigError error
}

func (e SerializationError) Error() string {
	msg := fmt.Sprintf("sfnactivity: serialization error: %s", e.Message)

	if e.OrigError != nil {
		msg = fmt.Sprintf("%s\ncaused by: %s", msg, e.OrigError.Error())
	}

	return msg
}
