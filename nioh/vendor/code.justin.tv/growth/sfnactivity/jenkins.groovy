job {
  name 'growth-sfnactivity'
  using 'TEMPLATE-autobuild'
  scm {
    git {
      remote {
        github 'growth/sfnactivity', 'ssh', 'git-aws.internal.justin.tv'
        credentials 'git-aws-read-key'
      }
      clean true
    }
  }

  steps {
    shell 'rm -rf .manta/'
    shell 'manta -v'
  }
}
