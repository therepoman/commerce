// +build tools

package main

import (
	_ "github.com/alecthomas/gometalinter"
	_ "github.com/axw/gocov/gocov"
	_ "github.com/golang/protobuf/proto"
	_ "github.com/golang/protobuf/protoc-gen-go"
	_ "github.com/stretchr/testify"
	_ "github.com/t-yuki/gocov-xml"
	_ "github.com/twitchtv/twirp/protoc-gen-twirp"
	_ "github.com/vektra/mockery"
)
