package config

import (
	"os"

	"code.justin.tv/commerce/config"
)

const (
	localDevEnv    = "dev"
	DevelopmentEnv = "development"
	BetaEnv        = "beta"
	CanaryEnv      = "canary"
	ProductionEnv  = "production"
)

type Configuration struct {
	AutoprofS3BucketName string `validate:"nonzero"`

	AWSRegion string `validate:"nonzero"`

	// Dynamo and DAX
	DAXClusterEndpoint string `validate:"nonzero"`

	// Cloudwatch
	CloudwatchBufferSize               int     `validate:"nonzero"`
	CloudwatchBatchSize                int     `validate:"nonzero"`
	CloudwatchFlushIntervalSeconds     int     `validate:"nonzero"`
	CloudwatchFlushCheckDelayMS        int     `validate:"nonzero"`
	CloudwatchEmergencyFlushPercentage float64 `validate:"nonzero"`
	CloudwatchRegion                   string  `validate:"nonzero"`

	StatsURL string `validate:"nonzero"`

	SpadeHost string `validate:"nonzero"`

	// Cache Settings
	InMemoryCacheExpirationMinutes    int `validate:"nonzero"`
	InMemoryCachePurgeDurationMinutes int `validate:"nonzero"`

	// Pubsub Settings
	PubsubBrokerHost                string `validate:"nonzero"`
	PubsubBrokerTimeoutMilliseconds int    `validate:"nonzero"`

	// Hystrix Timings
	SpadeMaxConcurrentRequests    int `validate:"nonzero"`
	SpadeTimeoutTrackMilliseconds int `validate:"nonzero"`

	// Users Service
	UsersServiceHost         string `validate:"nonzero"`
	UsersTimeoutMilliseconds int    `validate:"nonzero"`

	// Upload Service
	UploadServiceHost                string `validation:"nonzero"`
	UploadServiceTimeoutMilliseconds int    `validation:"nonzero"`
	UploadServiceCallbackSNSTopicARN string `validation:"nonzero"`
	UploadServiceS3BucketName        string `validation:"nonzero"`
	ContentAttributeImageURLPrefix   string `validation:"nonzero"`

	// Video Channel Properties
	ChannelPropertiesHost                string `validate:"nonzero"`
	ChannelPropertiesTimeoutMilliseconds int    `validate:"nonzero"`
	ChannelPropertiesAPIKey              string `validate:"nonzero"`

	// Liveline Service
	LivelineServiceHost                string `validate:"nonzero"`
	LivelineServiceTimeoutMilliseconds int    `validate:"nonzero"`

	// Siosa Ads Service
	SiosaHost                string `validate:"nonzero"`
	SiosaTimeoutMilliseconds int    `validate:"nonzero"`

	// Subscription Service
	SubscriptionsServiceHost                string `validate:"nonzero"`
	SubscriptionsServiceTimeoutMilliseconds int    `validate:"nonzero"`

	// Pepper Service
	PepperServiceHost                string `validate:"nonzero"`
	PepperServiceTimeoutMilliseconds int    `validate:"nonzero"`

	// Zuma Service
	ZumaServiceHost                string `validate:"nonzero"`
	ZumaServiceTimeoutMilliseconds int    `validate:"nonzero"`

	// RBAC service
	RbacServiceHost                string `validate:"nonzero"`
	RbacServiceTimeoutMilliseconds int    `validate:"nonzero"`

	// Voyager service
	VoyagerServiceHost                string `validate:"nonzero"`
	VoyagerServiceTimeoutMilliseconds int    `validate:"nonzero"`

	// VodAPI service
	VodAPIServiceHost                string `validate:"nonzero"`
	VodAPIServiceTimeoutMilliseconds int    `validate:"nonzero"`

	// API Gating Configuration
	UserGates            map[string]UserGateFlags
	BlockGetChanletsFlag bool // Always return empty result for GetChanlets (kill switch)

	PreviewOrigins    []string
	PreviewOriginsMap map[string]bool

	AutotagSNSTopicArn string `validate:"nonzero"`

	StreamEventsSQSName string `validate:"nonzero"`

	OfflineCleanupStateMachineArn string `validate:"nonzero"`
	OfflineCleanupActivityArn     string `validate:"nonzero"`

	// S2S Configuration
	S2SCallerName string

	// Kinesis Stream Name for Vinyl
	VinylKinesisStream string `validate:"nonzero"`
}

type UserGateFlags struct {
	Enabled bool

	// Override Lists
	// These lists can be used to force allow or deny any arbitrary users.
	OverrideAllowlist []string
	OverrideDenylist  []string
	OverrideMap       map[string]bool // the two lists above will be parsed in to this map

	// Staff Override
	StaffEnabled bool // Staff override if gating is enabled

	// Allowlist
	// This is to restrict normal users from using the feature. It should be deprecated (or removed) after the feature is public.
	AllowlistEnabled bool
	Allowlist        []string
	AllowlistMap     map[string]bool // the list is parsed into this map for O(1) look up.

	// Risk evaluation - based on low risk evaluation data from Pepper
	LowRiskGatingEnabled bool
}

type BoolFlag struct {
	Flag bool
}

type RolloutPercent struct {
	Percent int64
}

// ParseAllowlist converts a UserGateFlags allowlist and override lists into maps
func (c *Configuration) ParseAllowlist() {
	parseUsergateListsToMaps(&c.UserGates)
}

// ParsePreviewOrigin converts PreviewOrigin list to a map
func (c *Configuration) ParsePreviewOrigin() {
	c.PreviewOriginsMap = make(map[string]bool)
	for _, origin := range c.PreviewOrigins {
		c.PreviewOriginsMap[origin] = true
	}
}

// GetStatsURL returns the stats host url from the environment if it exists. If not, it returns the url in the config file.
func (c *Configuration) GetStatsURL() string {
	statsHostURL := os.Getenv("STATSD_HOST_PORT")
	if statsHostURL == "" {
		return c.StatsURL
	}
	return statsHostURL
}

// GetCanonicalEnv returns an environment that the resources being used are generated for
func GetCanonicalEnv() string {
	env := config.GetEnvironment()
	if env == CanaryEnv {
		return ProductionEnv
	}
	if env == localDevEnv {
		return DevelopmentEnv
	}
	return env
}

// IsLocalEnv returns whether the current environment is local
func IsLocalEnv() bool {
	return config.GetEnvironment() == localDevEnv
}

func parseUsergateListsToMaps(userGatesPointer *map[string]UserGateFlags) {
	userGates := *userGatesPointer
	for gateName, flags := range userGates {
		// Allowlist
		allowlistMap := make(map[string]bool, len(flags.Allowlist))
		for _, v := range flags.Allowlist {
			allowlistMap[v] = true
		}
		flags.AllowlistMap = allowlistMap

		// Override Lists
		overrideMap := make(map[string]bool, len(flags.OverrideAllowlist)+len(flags.OverrideDenylist))
		for _, v := range flags.OverrideAllowlist {
			overrideMap[v] = true
		}
		for _, v := range flags.OverrideDenylist {
			overrideMap[v] = false
		}
		flags.OverrideMap = overrideMap

		userGates[gateName] = flags // Ensure map is written into config
	}
}
