package config

import (
	"context"
	"fmt"
	"io/ioutil"
	"os"
	"time"

	"code.justin.tv/commerce/dynamicconfig"

	"github.com/BurntSushi/toml"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/pkg/errors"
	"code.justin.tv/commerce/logrus"
)

const (
	awsRegion   = "us-west-2"
	s3Endpoint  = "s3-us-west-2.amazonaws.com"
	s3BucketFmt = "twitch-nioh-%s-dynamic-configs"

	filePathUserGates     = "usergates.toml"
	boolFlagRBACCalls     = "RBACUserQueryDisabled"
	rolloutPercentsVodAPI = "VodAPI"
)

var (
	pollInterval = 60 * time.Second
	pollTimeout  = 5 * time.Second
)

// IDynamicConfiguration is an interface that returns dynamically updated config data
type IDynamicConfiguration interface {
	GetUserGates() (map[string]UserGateFlags, error)
	GetAllAccessPassModeratorsForChannel(channelID string) (map[string]bool, error)
	IsRBACUserQueryDisabled() (bool, error)
	VodAPIRolloutPercent() (int64, error)
	Shutdown()
}

// DynamicConfiguration implements the interface by using the s3 sources
type DynamicConfiguration struct {
	dynamicCfg    *dynamicconfig.DynamicConfig
	poller        *dynamicconfig.Poller
	env           string
	metricsLogger *DynamicConfigurationLogger
}

// DynamicConfigurationLogger logs and reports error metrics for DynamicConfiguration
type DynamicConfigurationLogger struct {
	statter statsd.Statter
}

// UserGatesRoot is the root struct that contains the content of the TOML based config file
type UserGatesRoot struct {
	UserGates map[string]UserGateFlags

	// Special hardcoded list of moderators for All Access Pass channels
	AllAccessPassModerators map[string]map[string]bool

	// EnableRBACDefaultResponse is a bool flag to control if nioh needs to call RBAC to get org info.
	BoolFlags map[string]BoolFlag

	// List of traffic rollout percentages
	RolloutPercents map[string]RolloutPercent
}

// NewDynamicConfiguration returns an instance of DynamicConfiguration that polls for fresh data at a regular interval
func NewDynamicConfiguration(env string, statter statsd.Statter) (IDynamicConfiguration, error) {
	ctx := context.Background()
	source, err := dynamicconfig.NewS3MultiFileSource(awsRegion, s3Endpoint, fmt.Sprintf(s3BucketFmt, env), []string{filePathUserGates})
	if err != nil {
		return nil, errors.Wrap(err, "failed to initialize DynamicConfiguration with s3 source")
	}

	logger := &DynamicConfigurationLogger{statter}

	dc, err := dynamicconfig.NewDynamicConfig(ctx, source, map[string]dynamicconfig.Transform{
		filePathUserGates: logger.parseUsergatesTOMLWithLogging,
	})

	poller := &dynamicconfig.Poller{
		Reloader: dc,
		Interval: pollInterval,
		Timeout:  pollTimeout,
		OnError:  logger.onPollError,
	}

	dynamicConfiguration := &DynamicConfiguration{
		dynamicCfg:    dc,
		poller:        poller,
		env:           env,
		metricsLogger: logger,
	}

	go poller.Poll()

	// Check the initial value (since error does not seem to surface if transform fails)
	if _, err := dynamicConfiguration.GetUserGates(); err != nil {
		dynamicConfiguration.Shutdown()
		return nil, errors.Wrap(err, "failed to fetch the initial config data")
	}

	return dynamicConfiguration, nil
}

// NewDynamicConfigurationFromLocalFile returns an instance of DynamicConfiguration based on a local file. This does not poll for new updates.
func NewDynamicConfigurationFromLocalFile(env string, statter statsd.Statter) (IDynamicConfiguration, error) {
	userGatesPath := os.Getenv("GOPATH") + "/src/code.justin.tv/commerce/nioh/config/" + env + "/" + filePathUserGates
	if _, err := os.Stat(userGatesPath); !os.IsNotExist(err) {
		// Load from local file
		userGatesData, err := ioutil.ReadFile(userGatesPath)
		if err != nil {
			return nil, err
		}

		parsedUserGatesData, err := ParseUsergatesTOML(filePathUserGates, userGatesData)
		if err != nil {
			return nil, err
		}

		// Set local source with parsed data
		source := dynamicconfig.NewLocalSource()
		source.Set(map[string]interface{}{
			filePathUserGates: parsedUserGatesData,
		}, nil)
		logger := &DynamicConfigurationLogger{statter}

		// Make dynamic config without poller
		dc, err := dynamicconfig.NewDynamicConfig(context.Background(), source, nil)
		if err != nil {
			return nil, err
		}
		dynamicConfiguration := &DynamicConfiguration{
			dynamicCfg:    dc,
			env:           env,
			metricsLogger: logger,
		}
		return dynamicConfiguration, nil
	}

	return nil, errors.New("local dynamic config file does not exist at " + userGatesPath)
}

// Shutdown stops the polling
func (dc *DynamicConfiguration) Shutdown() {
	dc.poller.Stop()
}

// GetUserGates returns the newest data polled for UserGates
func (dc *DynamicConfiguration) GetUserGates() (map[string]UserGateFlags, error) {
	if value, ok := dc.dynamicCfg.Get(filePathUserGates).(UserGatesRoot); ok {
		return value.UserGates, nil
	}

	return nil, errors.New("failed to parse user gate data")
}

// GetAllAccessPassModeratorsForChannel returns the newest data polled for UserGates
func (dc *DynamicConfiguration) GetAllAccessPassModeratorsForChannel(channelID string) (map[string]bool, error) {
	if value, ok := dc.dynamicCfg.Get(filePathUserGates).(UserGatesRoot); ok {
		return value.AllAccessPassModerators[channelID], nil
	}

	return nil, errors.New("failed to parse moderator data")
}

// IsRBACUserQueryDisabled return the bool flag value on whether we should disable user queries in RBAC.
// If the result is true, it means niho won't call RBAC to get org membership info and use a default result as response instead.
// Otherwise nioh will call RBAC.
func (dc *DynamicConfiguration) IsRBACUserQueryDisabled() (bool, error) {
	if value, ok := dc.dynamicCfg.Get(filePathUserGates).(UserGatesRoot); ok {
		return value.BoolFlags[boolFlagRBACCalls].Flag, nil
	}

	// by default enable rbac calls
	return true, errors.Errorf("failed to parse %s flag", boolFlagRBACCalls)
}

func (dc *DynamicConfiguration) VodAPIRolloutPercent() (int64, error) {
	if value, ok := dc.dynamicCfg.Get(filePathUserGates).(UserGatesRoot); ok {
		return value.RolloutPercents[rolloutPercentsVodAPI].Percent, nil
	}

	return 0, errors.Errorf("failed to parse %s flag", rolloutPercentsVodAPI)
}

func (dl *DynamicConfigurationLogger) onPollError(err error) {
	logrus.WithError(err).Error("DynamicConfiguration encountered an error while polling for new data.")
	if err := dl.statter.Inc("dynamic_config.poll_error", 1, 1.0); err != nil {
		logrus.Debugf("Failed to send an increment metric %+v", err)
	}
}

func (dl *DynamicConfigurationLogger) parseUsergatesTOMLWithLogging(key string, value interface{}) (interface{}, error) {
	result, err := ParseUsergatesTOML(key, value)
	if err != nil {
		// Transform error is never exposed to the consumer.
		// Since it runs on each poll, treat this error as a poll error.
		dl.onPollError(err)
	}
	return result, err
}

// ParseUsergatesTOML is a Transform func used to parse the raw data to the struct
func ParseUsergatesTOML(key string, value interface{}) (interface{}, error) {
	blob, ok := value.([]byte)
	if !ok {
		return nil, errors.New("failed to get the bytes as the value in an s3 toml file at " + key)
	}
	usergatesRoot := UserGatesRoot{}

	if err := toml.Unmarshal(blob, &usergatesRoot); err != nil {
		return nil, errors.Wrap(err, "failed to unmarshal toml on data change event")
	}

	parseUsergateListsToMaps(&usergatesRoot.UserGates)

	return usergatesRoot, nil
}
