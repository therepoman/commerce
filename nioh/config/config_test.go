package config

import (
	"os"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestConfig(t *testing.T) {
	Convey("ParseAllowlist", t, func() {
		Convey("allowlist map is populated", func() {
			gateName := "GateName"
			userID := "123"
			cfg := Configuration{
				UserGates: map[string]UserGateFlags{
					gateName: {
						Allowlist: []string{userID},
					},
				},
			}
			cfg.ParseAllowlist()
			So(cfg.UserGates[gateName].AllowlistMap[userID], ShouldBeTrue)
		})
	})

	Convey("GetStatsURL", t, func() {
		defaultStatsURL := "defaultStatsURL"
		cfg := Configuration{
			StatsURL: defaultStatsURL,
		}
		Convey("defaults to configuration", func() {
			So(cfg.GetStatsURL(), ShouldEqual, defaultStatsURL)
		})
		Convey("uses STATSD_HOST_PORT", func() {
			statsURL := "localhost:0"
			os.Setenv("STATSD_HOST_PORT", statsURL)
			So(cfg.GetStatsURL(), ShouldEqual, statsURL)
		})
	})
}
