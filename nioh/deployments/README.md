# CloudFormation

## Overview

Nioh uses CloudFormation for most of its infrastructure resources. `./deployments` directory contains, along with other non-CF configuration files, template files containing groups of such resources.

In order to create/edit resources, you need to either create a new yaml file (a new stack) or edit any of the existing files.

You can execute the change by using the aws cli (make sure you have the right credentials already stored in `~/.aws/credentials`).

Update Example:
```
aws cloudformation update-stack --stack-name nioh-global \
		--template-body file://./deployments/global/global.yaml \
		--parameters file://./deployments/global/global-params.json \
		--capabilities CAPABILITY_NAMED_IAM --profile twitch-nioh-aws
```

Create Example:
```
aws cloudformation create-stack --stack-name nioh-global \
		--template-body file://./deployments/global/global.yaml \
		--parameters file://./deployments/global/global-params.json \
		--capabilities CAPABILITY_NAMED_IAM --profile twitch-nioh-aws
```

If the template file contains `Transforms`, you need to use `deploy` instead, which has slightly different parameters.
```
 aws cloudformation deploy --template-file dist/lambda/template.yml \
	  --capabilities CAPABILITY_IAM --stack-name nioh-tools-lambda \
		--profile twitch-nioh-aws \
		--parameter-overrides AppName=nioh
```

For prod stacks, we highly recommend using `create-change-set` instead of `update-stack` so that we can previous the diffs before execution.

Example:

```
aws cloudformation create-change-set --change-set-name prod-global --stack-name nioh-global \
		--template-body file://./deployments/global/global.yaml \
		--parameters file://./deployments/global/global-params.json \
		--capabilities CAPABILITY_NAMED_IAM --profile twitch-nioh-aws
```

This command will run a basic lint on the template, then creates a change set on the stack. The ID will be return as a response. Go to AWS-console to preview/execute the stack update, or you can use the CLI tools.

Example:
```
aws cloudformation describe-change-set \
    --change-set-name my-change-set \
    --stack-name my-stack

aws cloudformation execute-change-set \
    --change-set-name my-change-set \
    --stack-name my-stack
```

## Directories

*global* - contains IAM and VPC related resources. There is usually no need to touch these.
```make update_global_stack```

*infra* - contains alarms and Application level resources such as Dynamo and DAX.
```make update_alarms_stack```
```make update_prod_infra```

*private-links* - contains Private Links resources. For each downstream service, we need to add an entry there.

*dns* - contains Route 53 Hosted Zone and various entries (github, statsd, etc...).
