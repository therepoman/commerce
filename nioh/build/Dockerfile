ARG go_version=1.13
FROM golang:$go_version as build
RUN apt-get update && \
  apt-get install -y openjdk-11-jdk && \
  apt-get clean

WORKDIR $GOPATH/src/code.justin.tv/commerce/nioh
# Copy requirements to make the tools, so we can cache the tool-building step
# on subsequent runs.
# Copy over everything else required for the build.
COPY . .
RUN make build_linux
RUN go test -c -o /integration-tester -mod vendor -tags=integration -count=1 -v -race ./test/internal_integration_tests

FROM alpine
RUN apk add --no-cache ca-certificates
COPY --from=build /integration-tester /integration-tester
COPY --from=build /go/src/code.justin.tv/commerce/nioh/nioh /nioh
COPY --from=build /go/src/code.justin.tv/commerce/nioh/config /config

ENTRYPOINT ["/nioh"]
EXPOSE 8000
