# Nioh
Original technical design document can be found [here](https://docs.google.com/document/d/1eb0drNPX9ZLABL4ec-INa7LEBwSipDXL6ZldQpotkKM/edit#heading=h.7yq80nf76nmv).
## Responsibilities
Nioh houses the business logic for two primary functions:
1. **Paywalling**. Nioh houses access control policies that are specifically related to commerce (as opposed to security policies, geoblocking policies, etc.). Nioh works with *restrictions* that are applied to *resources*, forbidding users access to the resource unless they meet requirements stated in the restriction. Nioh's restrictions underlie Overwatch League's Command Center functionality, as well as the upcoming Sub-Only Live site-wide feature.
2. **Multiview/sub-streams**.  Nioh allows the creation and management of *sub-streams* (known internally as *chanlets*), which are user accounts created specifically to act as auxiliary streams related to an *owner channel*. Sub-streams implement Overwatch League's Command Center functionality.

## Paywalling details
Sub-Only Live early tech design document can be found [here](https://docs.google.com/document/d/1VVbosGxZDPi70maiw7raWHLvXGLPbjMthwNpOAfoKec/edit#heading=h.upkqlmf7ltx0).

Five Minute Preview tech design document can be found [here](https://docs.google.com/document/d/1Whl7qURuHeur88ack8_awQhqcw1xZhee2hPomSlbLpw/edit#heading=h.upkqlmf7ltx0).

### The restriction model
See the [Protobuf models](../rpc/nioh/models.proto) for annotated schemas for restriction and exemption messages. The Protobuf schema is the source of truth for message shape and includes detailed documentation comments on elements of the messages.

Restrictions within Nioh are per-resource, and describe both _if_ the resource is restricted and _how_ the resource is restricted. A restriction is never materialized for an unrestricted resource; it's the _existence_ of a restriction for a given resource that indicates whether that resource is restricted, _not_ any data within the restriction block itself.

Example request/response using restrictions (pseudo-HTTP):
```
POST /twirp/nioh.API/GetRestrictionsByResources

{
	"resources": [
		{
			"id": "167136666",
			"type": "LIVE"
		},
		{
			"id": "250540387",
			"type": "LIVE"
		}
	]
}
```
```
200 OK

{
  "restrictions": [
    {
      "id": "167136666",
      "type": "LIVE"
    },
    {
      "id": "250540387",
      "type": "LIVE",
      "restriction": {
        "id": "channel:250540387",
        "restriction_type": "SUB_ONLY_LIVE",
        "exemptions": [
          // elided
        ]
      }
    }
  ]
}
```
In this example, channel with ID 167136666 is *unrestricted*, indicated by the absence/emptiness of the `"restriction"` field in its corresponding block in the response. Channel with ID 250540387 *is* restricted, indicated by its populated `"restriction"` field, containing details about the restriction.

#### A note on the UNKNOWN restriction type
In Nioh's model, there is no restriction type that indicates "unrestricted". Every variant of the RestrictionType enum describes a type of restriction _that exists and restricts a resource_. Restrictions of type `UNKNOWN` indicate the presence of a restriction (and therefore a restricted resource), but whose restriction type is unknown to the client. `UNKNOWN` restrictions should be treated as unhandleable: there is no business logic appropriate for an `UNKNOWN` restriction. `UNKNOWN` restrictions should _never_ be expected, as they indicate a variant of RestrictionType that was unparseable by client code.

### Previews
A restriction with an exemption of `exemption_type`=`PREVIEW` offers a consumable five-minute preview to logged-in (not anonymous) users. A call to GetUserAuthorization on behalf of a user can consume the preview, if available, by setting `consume_preview`=`true` in the request. Once a preview is consumed for a given restriction and user, that user has access to the restricted resource for five minutes, after which their access expires and the PREVIEW exemption becomes inert until an 8-hour reset period elapses. A preview exemption can exist alongside exemptions of other types, which take priority over the preview exemption and remain active during the preview's reset period.

## Glossary
**chanlet**: The internal name for *sub-streams*, from *channel* + *-let* (diminutive suffix).

**exemption**: An element within a restriction that describes a mechanism by which a user can gain access to the resource that the restriction applies to. Exemptions are the only way to describe means of accessing a restricted resource; a restriction with no exemptions will never grant any user access to its corresponding resource.

**resource**: An abstract entity which users on Twitch access. Examples of resources are channels (implicitly, channels' livestreams) and individual VODs.

**restriction**: A policy applied to a *resource* that restricts user access to that resource. Restrictions are owned by Nioh and live within its data store.

**sub-stream**: A programmatically-created Twitch account that exists to provide a stream key and stream that are subordinate to another channel, described as the sub-stream's *owner*.