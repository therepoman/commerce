#!/bin/bash

if [ $# -eq 0 ]
  then
    echo "Requires an AWS account ID as an argument"
    exit
fi

ACCOUNT_ID=$1

# Log into ECR registry
$(aws ecr get-login --region us-west-2 --no-include-email)
# Build target image to a docker repo. Will be used for testing in a separate step.
docker build -f build/Dockerfile --target build -t ${ACCOUNT_ID}.dkr.ecr.us-west-2.amazonaws.com/nioh:build-${GIT_COMMIT} .
# Build image artifact
docker build -f build/Dockerfile -t ${ACCOUNT_ID}.dkr.ecr.us-west-2.amazonaws.com/nioh:${GIT_COMMIT} .
# Tag build as :latest
docker tag ${ACCOUNT_ID}.dkr.ecr.us-west-2.amazonaws.com/nioh:${GIT_COMMIT} ${ACCOUNT_ID}.dkr.ecr.us-west-2.amazonaws.com/nioh:latest
# Push the image artifact to the docker repository
docker push ${ACCOUNT_ID}.dkr.ecr.us-west-2.amazonaws.com/nioh:${GIT_COMMIT}
# Push the build image to the docker repository
docker push ${ACCOUNT_ID}.dkr.ecr.us-west-2.amazonaws.com/nioh:build-${GIT_COMMIT}