#!/bin/bash

if [ $# -lt 3 ]
  then
    echo "Requires [AWS account ID], [environment], [dockerTag] as arguments"
    exit
fi

ACCOUNT_ID=$1
ENVIRONMENT=$2
DOCKER_TAG=$3
CLUSTER=$ENVIRONMENT

if [ $CLUSTER = 'canary' ]
  then
    CLUSTER='production'
fi

docker run -e AWS_SECRET_ACCESS_KEY -e AWS_ACCESS_KEY_ID \
  docker.internal.justin.tv/edge/ecs-deploy \
  -t 600s \
  -r us-west-2 \
  -c nioh-${CLUSTER} \
  -n nioh-${ENVIRONMENT}-ec2 \
  -i ${ACCOUNT_ID}.dkr.ecr.us-west-2.amazonaws.com/nioh:${DOCKER_TAG}

# Notify grafana of deployment
echo "deploys.commerce.nioh.${ENVIRONMENT}:1|c" | nc -w 1 -u statsd.internal.justin.tv 8125