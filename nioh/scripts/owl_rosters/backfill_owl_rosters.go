package main

/*
 * This script populates the OWL roster data based on the input csv file.
 * It creates team, role and player content attributes and makes the team <-> players and player <-> roles associations
 * It also uploads icons for teams and roles. Icon file name are expected to be ub the form of "{attribute key}_{attribute value joined by underscore and lowercased}.png"
 *    For example, for the team Los Angeles Gladiators, the icon file name is expected to be "team_los_angeles_gladiators.png"
 *                 for the role DPS, the icon file name is expected to be "role_dps.png"
 *
 * The parameters that are expected to run the script:
 *    channelID: which channel to create the content attributes for
 *    inputFile: path to the csv file, expecting the first line to be the header, and in the format of "team, player, role"
 *    imageDir: path to the icon directory
 *    useProd: optional, default to false. Talks to prod Nioh and upload service if set to true, talks to staging services otherwise.
 *
 * If running the script with a local Socks5 proxy, prepend the command with `HTTP_PROXY=socks5://proxy_address:port`,
 *    which should direct service calls to the local socks5 proxy
 *    For example, `HTTP_PROXY=socks5://localhost:8080 go run backfill_owl_rosters.go -useProd=true -channelID=167136666 -file=OWL2018_roster.csv -imageDir=./icons`
 *
 * Currently the script uploads icons one at the time. Given the low number of icons we have right now this is simpler and more manageable.
 *    We should revisit this if the number of icons increase significantly.
 */

import (
	"bufio"
	"context"
	"encoding/csv"
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strings"
	"sync"
	"time"

	"code.justin.tv/commerce/nioh/rpc/nioh"
	"code.justin.tv/foundation/twitchclient"
	"code.justin.tv/web/upload-service/rpc/uploader"
	"github.com/pkg/errors"
)

const (
	niohHostProd = "https://nioh-production.internal.justin.tv"
	niohHostBeta = "https://nioh-beta.internal.justin.tv"

	uploadStaging = "https://staging-web-upload-service.staging.us-west2.twitch.tv"
	uploadProd    = "https://prod-web-upload-service.prod.us-west2.twitch.tv"

	teamAttributeKey  = "team"
	teamAttributeName = "Team"

	playerAttributeKey  = "player"
	playerAttributeName = "Player"

	roleAttributeKey  = "role"
	roleAttributeName = "Role"
)

var channelID *string
var inputFile *string
var niohHost string
var uploadHost string
var imageDir *string

// Entry represent a single line entry in the CSV input file
type Entry struct {
	Team   string
	Player string
	Role   string
}

type Executor struct {
	client     nioh.API
	uploader   uploader.Uploader
	httpClient *http.Client
	teamsMap   map[string]*nioh.ContentAttribute
	rolesMap   map[string]*nioh.ContentAttribute
	playersMap map[string]*nioh.ContentAttribute
	entries    []*Entry
}

func main() {
	channelID = flag.String("channelID", "", "the channel of which the content attributes will be created under")
	inputFile = flag.String("file", "", "path to the CSV file that contains the roster data")
	useProd := flag.Bool("useProd", false, "Boolean flag to indicate whether the production fleet should be targetted")
	imageDir = flag.String("imageDir", "", "Path to the directory where the content attribute icons are stored")

	flag.Parse()

	if *channelID == "" {
		log.Println("channelID id must be specified")
		return
	}

	if *inputFile == "" {
		log.Println("file must be specified to load roster data")
		return
	}

	if *imageDir == "" {
		log.Println("imageDir must be specified to load team and role icons")
		return
	}

	log.Printf("Use Prod = %t", *useProd)
	log.Printf("Input file: %s", *inputFile)
	log.Printf("Image Directory: %s", *imageDir)

	if *useProd {
		niohHost = niohHostProd
		uploadHost = uploadProd
	} else {
		niohHost = niohHostBeta
		uploadHost = uploadStaging
	}

	client := nioh.NewAPIProtobufClient(niohHost, twitchclient.NewHTTPClient(twitchclient.ClientConf{
		Host: niohHost,
		Transport: twitchclient.TransportConf{
			MaxIdleConnsPerHost: 10,
		},
	}))

	uploader := uploader.NewUploaderProtobufClient(uploadHost, twitchclient.NewHTTPClient(twitchclient.ClientConf{
		Host: uploadHost,
		Transport: twitchclient.TransportConf{
			MaxIdleConnsPerHost: 10,
		},
	}))

	httpClient := &http.Client{
		Timeout: time.Duration(5 * time.Second),
	}

	executor := &Executor{
		client:     client,
		uploader:   uploader,
		httpClient: httpClient,
		teamsMap:   make(map[string]*nioh.ContentAttribute),
		rolesMap:   make(map[string]*nioh.ContentAttribute),
		playersMap: make(map[string]*nioh.ContentAttribute),
		entries:    []*Entry{},
	}

	executor.Run(*inputFile)
}

func (e *Executor) Run(inputFile string) {
	log.Println("Executing script...")

	file, err := os.Open(inputFile)
	if err != nil {
		log.Fatalf("Could not open input file %s", err.Error())
		return
	}

	// initialize csv reader and skip the header line
	reader := csv.NewReader(bufio.NewReader(file))
	reader.Read()
	for {
		line, err := reader.Read()
		if err == io.EOF {
			break
		} else if err != nil {
			log.Fatalf("Could not read from input file %s", err.Error())
			continue
		}

		log.Printf("Reading %s from input file\n", line)
		entry := &Entry{
			Team:   strings.TrimSpace(line[0]),
			Player: strings.TrimSpace(line[1]),
			Role:   strings.TrimSpace(line[2]),
		}
		e.entries = append(e.entries, entry)

		_, ok := e.teamsMap[entry.Team]
		if !ok {
			e.teamsMap[entry.Team] = &nioh.ContentAttribute{
				AttributeKey:  teamAttributeKey,
				AttributeName: teamAttributeName,
				Value:         entry.Team,
			}
		}

		_, ok = e.rolesMap[entry.Role]
		if !ok {
			e.rolesMap[entry.Role] = &nioh.ContentAttribute{
				AttributeKey:  roleAttributeKey,
				AttributeName: roleAttributeName,
				Value:         entry.Role,
			}
		}

		_, ok = e.playersMap[entry.Player]
		if !ok {
			e.playersMap[entry.Player] = &nioh.ContentAttribute{
				AttributeKey:  playerAttributeKey,
				AttributeName: playerAttributeName,
				Value:         entry.Player,
			}
		} else {
			// TODO: handle duplicate player names
		}
	}

	log.Printf("Creating all %d teams...", len(e.teamsMap))
	err = e.createTeams()
	if err != nil {
		log.Printf("Error creating teams, %s", err.Error())
		log.Println("Rolling back previously created data and exiting...")
		e.revertCreatedData(e.teamsMap)
		os.Exit(0)
	}

	log.Printf("Creating all %d role...", len(e.rolesMap))
	err = e.createRoles()
	if err != nil {
		log.Printf("Error creating roles, %s", err.Error())
		log.Println("Rolling back previously created data and exiting...")
		e.revertCreatedData(e.teamsMap)
		e.revertCreatedData(e.rolesMap)
		os.Exit(0)
	}

	log.Printf("Creating all %d players...", len(e.playersMap))
	err = e.createPlayers()
	if err != nil {
		log.Printf("Error creating players, %s", err.Error())
		log.Println("Rolling back previously created data and exiting...")
		e.revertCreatedData(e.teamsMap)
		e.revertCreatedData(e.rolesMap)
		e.revertCreatedData(e.playersMap)
		os.Exit(0)
	}

	log.Println("Done!")
	return
}

func (e *Executor) createContentAttributes(client nioh.API, attributes []*nioh.ContentAttribute) ([]*nioh.ContentAttribute, []*nioh.ContentAttribute, error) {
	requestQueue := make([]*nioh.ContentAttribute, 0)
	created := make([]*nioh.ContentAttribute, 0)
	failed := make([]*nioh.ContentAttribute, 0)

	for i, attribute := range attributes {
		requestQueue = append(requestQueue, attribute)

		if len(requestQueue) >= 30 || i == len(attributes)-1 {
			req := &nioh.CreateContentAttributesRequest{
				ChannelId:         *channelID,
				ContentAttributes: requestQueue,
			}

			fmt.Printf("Creating %d content attributes...\n", len(requestQueue))
			resp, err := client.CreateContentAttributes(context.Background(), req)
			if err != nil {
				return nil, nil, err
			}

			created = append(created, resp.SucceededCreates...)
			failed = append(failed, resp.FailedCreates...)

			requestQueue = make([]*nioh.ContentAttribute, 0)

			time.Sleep(1 * time.Second)
		}
	}

	return created, failed, nil
}

func (e *Executor) createTeams() error {
	return e.createAllInMap(e.teamsMap, true)
}

func (e *Executor) createRoles() error {
	return e.createAllInMap(e.rolesMap, true)
}

func (e *Executor) createAllInMap(inputMap map[string]*nioh.ContentAttribute, uploadIcon bool) error {
	if len(inputMap) == 0 {
		return nil
	}

	attributes := make([]*nioh.ContentAttribute, 0)

	for _, attribute := range inputMap {
		if uploadIcon {
			imageURL, err := e.uploadIcon(getImgFilename(attribute.AttributeKey, attribute.Value))
			if err != nil {
				return errors.Wrapf(err, "Failed to upload image icon for %s", attribute.Value)
			}
			attribute.ImageUrl = imageURL
		}
		attributes = append(attributes, attribute)
	}

	succeeded, failed, err := e.createContentAttributes(e.client, attributes)
	if err != nil {
		return err
	}
	if len(failed) > 0 {
		return errors.New("Failed to create some content attributes")
	}

	for _, created := range succeeded {
		// override previous content attribute with newly created one
		inputMap[created.Value] = created
	}

	log.Printf("################ Created %d content attributes ################", len(succeeded))

	return nil
}

func (e *Executor) uploadIcon(filename string) (string, error) {
	uploadConfig, err := e.client.CreateContentAttributeImageUploadConfig(
		context.Background(),
		&nioh.CreateContentAttributeImageUploadConfigRequest{
			ChannelId: *channelID,
		})
	if err != nil {
		return "", err
	}

	log.Printf("Uploading image %s", uploadConfig.UploadId)
	err = e.uploadImage(uploadConfig.UploadUrl, filename)
	if err != nil {
		log.Printf("Error uploading image %s", err.Error())
		return "", err
	}

	// TODO: upload all icons in parallel with a fixed number of workers
	var wg sync.WaitGroup
	var waitError error
	wg.Add(1)

	go func() {
		ticker := time.NewTicker(1 * time.Second)
		defer ticker.Stop()
		defer wg.Done()
		for {
			resp, err := e.uploader.Status(context.Background(), &uploader.StatusRequest{
				UploadId: uploadConfig.UploadId,
			})

			if err != nil {
				waitError = err
				return
			}

			if resp.Status == uploader.Status_COMPLETE {
				return
			}

			// > status_COMPLETE indicates failures
			if resp.Status > uploader.Status_COMPLETE {
				waitError = errors.Errorf("Upload status indicates failed image upload: upload ID %s, error %s", uploadConfig.UploadId, resp.Status)
				return
			}

			<-ticker.C
		}
	}()

	log.Println("Waiting on upload service to finish process the uploaded image")
	wg.Wait()
	if waitError != nil {
		return "", waitError
	}
	log.Println("Image uploaded")

	return uploadConfig.ImageUrl, nil
}

func (e *Executor) uploadImage(url string, filename string) error {
	imageFile, err := os.Open(filename)
	if err != nil {
		imageFile, err = os.Open(fmt.Sprintf("%s/%s", *imageDir, "owl.png"))
		if err != nil {
			return err
		}
	}

	var contentLength int64
	fileInfo, err := imageFile.Stat()
	if err != nil {
		return err
	}
	contentLength = fileInfo.Size()

	req, err := http.NewRequest(http.MethodPut, url, imageFile)
	if err != nil {
		return err
	}

	req.Header.Add("Content-Type", "image/png")
	req.ContentLength = contentLength

	resp, err := e.httpClient.Do(req)
	if err != nil {
		return err
	}

	if resp.StatusCode < 200 || resp.StatusCode > 299 {
		return errors.Errorf("Unexpected status code while uploading image file\n image=\"%v\"\n", filename)
	}

	return nil
}

func (e *Executor) createPlayers() error {
	players := make([]*nioh.ContentAttribute, 0, len(e.entries))
	for _, entry := range e.entries {
		team, ok := e.teamsMap[entry.Team]
		if !ok {
			// TODO: handle missing team key here (should never happen)
			return errors.Errorf("Unrecognized team found: %s", entry.Team)
		}

		role, ok := e.rolesMap[entry.Role]
		if !ok {
			// TODO: handle missing role key here (should never happen)
			return errors.Errorf("Unrecognized role found: %s", entry.Role)
		}

		players = append(players, &nioh.ContentAttribute{
			AttributeKey:       playerAttributeKey,
			AttributeName:      playerAttributeName,
			Value:              entry.Player,
			ParentAttributeKey: teamAttributeKey,
			ParentAttributeId:  team.Id,
			ChildAttributeIds:  []string{role.Id},
		})
	}

	succeeded, failed, err := e.createContentAttributes(e.client, players)
	if len(failed) > 0 {
		return errors.Errorf("%d player content attribute were rejected upon creation, %v+", len(failed), failed)
	}

	log.Printf("Created %d players", len(succeeded))
	return err
}

func (e *Executor) revertCreatedData(inputMap map[string]*nioh.ContentAttribute) error {
	var attributesToDelete []*nioh.ContentAttribute
	for _, attribute := range inputMap {
		if attribute.Id != "" {
			attributesToDelete = append(attributesToDelete, attribute)
		}
	}

	return e.deleteContentAttributes(attributesToDelete)
}

func (e *Executor) deleteContentAttributes(attributes []*nioh.ContentAttribute) error {
	if len(attributes) == 0 {
		return nil
	}

	req := &nioh.DeleteContentAttributesRequest{
		ChannelId:         *channelID,
		ContentAttributes: attributes,
	}

	_, err := e.client.DeleteContentAttributes(context.Background(), req)
	return err
}

func getImgFilename(key string, value string) string {
	return fmt.Sprintf("%s/%s_%s.png", *imageDir, strings.ToLower(key), strings.Join(strings.Split(strings.ToLower(value), " "), "_"))
}
