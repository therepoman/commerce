package main

/*
 * This script deletes the roster info (teams, players and roles) content attributes from the given channel.

 * The parameters that are expected to run the script:
 *    channelID: which channel to create the content attributes for
 *    useProd: optional, default to false. Talks to prod Nioh and upload service if set to true, talks to staging services otherwise.
 *
 * If running the script with a local Socks5 proxy, prepend the command with `HTTP_PROXY=socks5://proxy_address:port`,
 *    which should direct service calls to the local socks5 proxy
 *    For example, `HTTP_PROXY=socks5://localhost:8080 go run delete_owl_rosters.go -useProd=true -channelID=167136666`
 */

import (
	"context"
	"flag"
	"log"
	"time"

	"code.justin.tv/commerce/nioh/rpc/nioh"
	"code.justin.tv/foundation/twitchclient"
)

const (
	niohHostProd = "https://nioh-production.internal.justin.tv"
	niohHostBeta = "https://nioh-beta.internal.justin.tv"

	uploadStaging = "https://staging-web-upload-service.staging.us-west2.twitch.tv"
	uploadProd    = "https://prod-web-upload-service.prod.us-west2.twitch.tv"

	teamAttributeKey   = "team"
	playerAttributeKey = "player"
	roleAttributeKey   = "role"
)

var channelID *string
var niohHost string

func main() {
	channelID = flag.String("channelID", "", "the channel of which the content attributes will be created under")
	useProd := flag.Bool("useProd", false, "Boolean flag to indicate whether the production fleet should be targetted")

	flag.Parse()

	if *channelID == "" {
		log.Println("channelID id must be specified")
		return
	}

	log.Printf("Use Prod = %t", *useProd)

	if *useProd {
		niohHost = niohHostProd
	} else {
		niohHost = niohHostBeta
	}

	client := nioh.NewAPIProtobufClient(niohHost, twitchclient.NewHTTPClient(twitchclient.ClientConf{
		Host: niohHost,
		Transport: twitchclient.TransportConf{
			MaxIdleConnsPerHost: 10,
		},
	}))

	resp, err := client.GetContentAttributesForChannel(context.Background(), &nioh.GetContentAttributesForChannelRequest{
		ChannelId: *channelID,
	})

	if err != nil {
		log.Printf("Could not retrieve content attributes for channel %s, %v+", channelID, err)
	}

	var deleteList []*nioh.ContentAttribute
	for i, attribute := range resp.ContentAttributes {
		if attribute.AttributeKey == teamAttributeKey || attribute.AttributeKey == playerAttributeKey || attribute.AttributeKey == roleAttributeKey {
			deleteList = append(deleteList, attribute)
		}

		if len(deleteList) >= 30 || i == len(resp.ContentAttributes)-1 {
			_, err = client.DeleteContentAttributes(context.Background(), &nioh.DeleteContentAttributesRequest{
				ChannelId:         *channelID,
				ContentAttributes: deleteList,
			})
			if err != nil {
				log.Printf("Error deleting content attributes %v+", err)
			}

			deleteList = make([]*nioh.ContentAttribute, 0)
			time.Sleep(1 * time.Second)
		}
	}

	return
}
