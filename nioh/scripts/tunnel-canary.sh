#!/bin/bash

set -e
CANARY_TASK=$(aws --profile twitch-nioh-aws ecs list-tasks --cluster nioh-production --service-name nioh-canary --desired-status RUNNING | jq -r '.taskArns[0]')
CANARY_IP=$(aws --profile twitch-nioh-aws ecs describe-tasks --cluster nioh-production --tasks $CANARY_TASK | jq -r '.tasks[0].containers[0].networkInterfaces[0].privateIpv4Address')

echo "================================================"
echo "You can now access canary through localhost:8080"
echo "================================================"

TC=teleport-remote-twitch-nioh-prod ssh -L 8080:${CANARY_IP}:8000 10.204.74.100