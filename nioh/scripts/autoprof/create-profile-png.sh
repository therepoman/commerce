#!/bin/bash
# Make sure to install graphviz and pprof before running
# If you have a lot of logs, it can potentially crash your computers. :kappa:

for D in `find ./autoprof_logs -type d`
do
  if [ -f $D/pprof/profile ]; then
   go tool pprof -png -output=$D/profile.png $D/pprof/profile
   open $D/profile.png
  fi
done