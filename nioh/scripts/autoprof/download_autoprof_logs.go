package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
	"os/exec"
	"strings"
	"time"

	"code.justin.tv/commerce/nioh/pkg/s3"

	"github.com/aws/aws-sdk-go/aws/session"
)

const (
	bucket = "twitch-nioh-production-autoprof"
	prefix = "pprof/code.justin.tv%2Fcommerce%2Fnioh%2Fcmd%2Fnioh"
	outDir = "autoprof_logs"
)

var (
	maxLogCount int = 20 // Downloading will stop after reaching this amount (since there are A LOT)
)

// Downloader is a file downloader for s3
type Downloader struct {
	S3Client s3.IClient
}

// Usage: go run scripts/autoprof_logs/download_autoprof_logs.go -start=2019-03-22T19:00:00.000Z -end=2019-03-22T19:30:00.000Z
// Default for start is 1 hour ago. Default for end is now.
// If start and end are not specificed, it fetches data during the last 1 hour.

func main() {
	startDateF := flag.String("start", "", "Lower bound on the time window for fetching logs - e.g. 2019-03-22T19:00:00.000Z. Default: 1 hour ago.")
	endDateF := flag.String("end", "", "Upper bound on the time window for fetching logs - e.g. 2019-03-22T19:30:00.000Z. Default: Now.")
	maxLogCountF := flag.Int("max", maxLogCount, "Max number of logs fetched. Default: 20")
	flag.Parse()

	startDateStr := *startDateF
	endDateStr := *endDateF
	maxLogCount = *maxLogCountF

	startDate, err := time.Parse(time.RFC3339, startDateStr)
	if err != nil {
		startDate = time.Now().Add(-1 * time.Hour)
		fmt.Printf("Start date is not set. Defaulting to 24 hours ago: %+v\n", startDate)
	}

	endDate, err := time.Parse(time.RFC3339, endDateStr)
	if err != nil {
		endDate = time.Now()
		fmt.Printf("End date is not set. Defaulting to now: %+v\n", endDate)
	}

	fmt.Printf("Download logs created between %+v and %+v?\n", startDate, endDate)
	if !confirm() {
		os.Exit(0)
	}

	// Create dir
	if err = createDirIfNotExist(outDir); err != nil {
		fmt.Printf("Encountered an error while creating directory: %+v\n", err)
		os.Exit(1)
	}

	// Downloader
	downloader, err := newDownloader()
	if err != nil {
		fmt.Printf("Encountered an error while initializing downloader: %+v\n", err)
		os.Exit(1)
	}

	// Download logs and save paths to downloaded loags
	paths, err := downloader.downloadLogs(startDate, endDate)
	if err != nil {
		fmt.Printf("Encountered an error while downloading logs: %+v\n", err)
		os.Exit(1)
	}

	fmt.Println("Successfully downloaded autoprof logs. Unzipping bundles...")

	// Unzip
	if err = unzipAll(paths); err != nil {
		fmt.Printf("Encountered an error while unzipping: %+v\n", err)
		os.Exit(1)
	}

	fmt.Println("Successfully unzipped all downloaded log bundles.")

	os.Exit(0)
}

func confirm() bool {
	reader := bufio.NewReader(os.Stdin)
	for {
		fmt.Print("y / [n] :")
		answer, _ := reader.ReadString('\n')
		answer = strings.TrimSpace(answer)
		if answer == "y" || answer == "Y" {
			return true
		} else if answer == "n" || answer == "N" || answer == "" {
			return false
		} else {
			fmt.Println("Please enter y or n")
			continue
		}
	}
}

func newDownloader() (*Downloader, error) {
	awsProfile := "twitch-nioh-aws"
	sess := session.Must(session.NewSessionWithOptions(session.Options{
		Profile: awsProfile,
		Config:  *s3.NewDefaultConfig(),
	}))
	s3Client := s3.New(sess, s3.NewDefaultConfig())

	if exists, err := s3Client.BucketExists(bucket); !exists || err != nil {
		if err != nil {
			return nil, fmt.Errorf("encountered an error while checking s3 bucket: %+v", err)
		}
		return nil, fmt.Errorf("bucket %s does not exist", bucket)
	}

	return &Downloader{
		S3Client: s3Client,
	}, nil
}

func (d *Downloader) downloadLogs(start time.Time, end time.Time) ([]string, error) {
	var marker string
	var downloadCount int
	var done bool
	downloadedFilePaths := []string{}

	for {
		objs, err := d.S3Client.ListObjects(bucket, prefix, marker)
		if err != nil {
			return downloadedFilePaths, err
		}

		if len(objs) == 0 {
			done = true
		}

		for _, obj := range objs {
			if obj.LastModified.After(start) && obj.LastModified.Before(end) {
				key := *obj.Key
				keySlice := strings.Split(key, "/")
				filePath := fmt.Sprintf("./%s/%s", outDir, keySlice[len(keySlice)-1])

				fmt.Printf("Downloading file to %s\n", filePath)
				file, err := os.Create(filePath)
				if err != nil {
					return downloadedFilePaths, err
				}

				defer file.Close()
				downloadCount = downloadCount + 1

				if err = d.S3Client.WriteObjectToFile(file, bucket, key); err != nil {
					return downloadedFilePaths, err
				}
				downloadedFilePaths = append(downloadedFilePaths, filePath)

				if downloadCount >= maxLogCount {
					fmt.Println("Enough")
					done = true
					break
				}
			}
			marker = *obj.Key
		}

		if done {
			break
		}
	}

	return downloadedFilePaths, nil
}

func createDirIfNotExist(dir string) error {
	if _, err := os.Stat(dir); os.IsNotExist(err) {
		err = os.MkdirAll(dir, 0766)
		if err != nil {
			return err
		}
	}

	return nil
}

func unzipAll(paths []string) error {
	for _, path := range paths {
		fmt.Printf("Unzipping %s\n", path)
		if err := createDirIfNotExist(path); err != nil {
			return err
		}

		// Rename to .zip file before decompressing (so the dir name and the file name can differ)
		renamedPath := path + ".zip"
		if err := os.Rename(path, renamedPath); err != nil {
			return err
		}

		cmd := exec.Command("unzip", "-o", "-d", path, renamedPath)
		if err := cmd.Run(); err != nil {
			return err
		}
	}

	return nil
}
