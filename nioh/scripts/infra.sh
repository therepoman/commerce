#!/bin/bash

if [ $# -eq 0 ]
  then
    echo "Requires an environment name as an argument (beta or production)"
    exit
fi

if [ $1 != "beta" ] && [ $1 != "production" ]
  then
    echo "Invalid environment. Requires 'beta' or 'production'"
    exit
fi

ENV_NAME=$1

# TODO: This will fail for the jenkins-ci user, as it doesn't have the right permissions to run it.
aws cloudformation deploy --stack-name ecs-nioh-${ENV_NAME} --template-file ./deployments/infra/ecs-app.yaml