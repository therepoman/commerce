#!/bin/bash

if [ $(grep HttpOnly_midway-auth.amazon.com ~/.midway/cookie | cut -d $'\t' -f 5) -lt $( date  +%s ) ]
    then
        echo "Sign into Midway"
        mwinit --aea
    else
        echo "Current Midway token is still valid"
fi