package main

import (
	"bufio"
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"strings"

	"code.justin.tv/commerce/nioh/config"
	"code.justin.tv/commerce/nioh/internal/server"
	"code.justin.tv/commerce/nioh/pkg/s3"

	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/sergi/go-diff/diffmatchpatch"
)

const (
	configFilePathFmt = "%s/src/code.justin.tv/commerce/nioh/config/%s/%s"
)

var environments = map[string]bool{
	config.DevelopmentEnv: true,
	config.BetaEnv:        true,
	config.ProductionEnv:  true,
}

// Config Specific Settings
var (
	configTypes = map[string]bool{
		"usergates": true,
	}
	configValidationFuncs = map[string]func(key string, value interface{}) error{
		"usergates": validateUserGates,
	}
	configFilenames = map[string]string{
		"usergates": "usergates.toml",
	}
)

// Uploader is a file uploader for s3
type Uploader struct {
	S3Client   s3.IClient
	FileName   string
	BucketName string
}

func main() {
	env := flag.String("env", "", "Environment you want to upload dynamic configuration for (Required)")
	cType := flag.String("type", "", "Dynamic config type you want to upload (Required)")
	flag.Parse()

	environment := *env
	configType := *cType

	if environment == "" || configType == "" {
		fmt.Println("Usage: upload_dynamic_config -env=development -type=usergates")
		flag.PrintDefaults()
		os.Exit(1)
	}

	if !environments[environment] {
		fmt.Println("Invalid environment. Available environments: development, beta, produnction")
		os.Exit(1)
	}

	if !configTypes[configType] {
		fmt.Println("Invalid type. Available types: usergates")
		os.Exit(1)
	}

	fmt.Printf("Loading configuration files for environment: %s \n", environment)

	// Load the config file
	fileName := configFilenames[configType]
	filePath := fmt.Sprintf(configFilePathFmt, os.Getenv("GOPATH"), environment, fileName)
	data, err := ioutil.ReadFile(filePath)
	if err != nil {
		fmt.Printf("Error reading configuration file: %+v", err)
		os.Exit(1)
	}

	// Validate
	validationFunc, ok := configValidationFuncs[configType]
	if ok {
		err := validationFunc(configType, data)
		if err != nil {
			fmt.Printf("Error validating config: %s", err)
			os.Exit(1)
		}
		fmt.Println("Validation Success")
	} else {
		fmt.Println("No defined validation function. Skipping validation.")
	}

	// Moving on to upload
	uploader, exists, err := newUploader(environment, fileName)
	if err != nil {
		fmt.Printf("Error creating S3 client: %s", err)
		os.Exit(1)
	}

	if exists {
		s3File, err := uploader.fetchFromS3()
		if err != nil {
			fmt.Printf("Error fetching file from S3 %s/%s: %s", uploader.BucketName, uploader.FileName, err)
			os.Exit(1)
		}

		// Show File diff between S3 and local version
		diffString := getFileDiff(s3File, data)

		if diffString == "" {
			fmt.Println("No changes found, exiting...")
			os.Exit(0)
		}
		fmt.Println(diffString)
	}

	shouldUpload := confirm()
	if !shouldUpload {
		os.Exit(0)
	}

	fileLocation, err := uploader.uploadToS3(environment, data)
	if err != nil {
		fmt.Printf("Error uploading to S3 %s/%s: %s", uploader.BucketName, uploader.FileName, err)
		os.Exit(1)
	}

	fmt.Printf("Successfully uploaded configuration to %s.", fileLocation)
	os.Exit(0)
}

func confirm() bool {
	reader := bufio.NewReader(os.Stdin)
	for {
		fmt.Print("Upload? y / [n] :")
		answer, _ := reader.ReadString('\n')
		answer = strings.TrimSpace(answer)
		if answer == "y" || answer == "Y" {
			return true
		} else if answer == "n" || answer == "N" || answer == "" {
			return false
		} else {
			fmt.Println("Please enter y or n")
			continue
		}
	}
}

func newUploader(env string, fileName string) (*Uploader, bool, error) {
	awsProfileTemplate := "twitch-nioh-%s"
	awsProfile := fmt.Sprintf(awsProfileTemplate, "dev")

	if env == config.ProductionEnv {
		awsProfile = fmt.Sprintf(awsProfileTemplate, "aws")
	}

	bucketName := fmt.Sprintf("twitch-nioh-%s-dynamic-configs", env)

	// Set up S3 client with proper AWS_PROFILE for the given environment
	sess := session.Must(session.NewSessionWithOptions(session.Options{
		Profile: awsProfile,
	}))
	s3Client := s3.New(sess, s3.NewDefaultConfig())

	if exists, err := s3Client.BucketExists(bucketName); !exists || err != nil {
		if err != nil {
			return nil, false, fmt.Errorf("encountered an error while checking s3 bucket: %+v", err)
		}
		return nil, false, fmt.Errorf("bucket %s does not exist", bucketName)
	}

	exists, err := s3Client.FileExists(bucketName, fileName)
	if err != nil {
		return nil, false, fmt.Errorf("could not check if file exists: %s / %s", bucketName, fileName)
	}

	return &Uploader{
		S3Client:   s3Client,
		FileName:   fileName,
		BucketName: bucketName,
	}, exists, nil
}

func (u *Uploader) fetchFromS3() ([]byte, error) {
	return u.S3Client.LoadFile(u.BucketName, u.FileName)
}

func (u *Uploader) uploadToS3(env string, file []byte) (string, error) {
	if err := u.S3Client.UploadFileToBucket(u.BucketName, u.FileName, file); err != nil {
		return "", err
	}
	return fmt.Sprintf("%s/%s", u.BucketName, u.FileName), nil
}

func getFileDiff(fileA []byte, fileB []byte) string {
	differ := diffmatchpatch.New()
	diffs := differ.DiffMain(string(fileA), string(fileB), false)
	return differ.DiffPrettyText(diffs)
}

func validateUserGates(key string, value interface{}) error {
	parsed, err := config.ParseUsergatesTOML(key, value)
	if err != nil {
		return err
	}
	usergates, ok := parsed.(config.UserGatesRoot)
	if !ok {
		return fmt.Errorf("usergate is malformed")
	}

	if !server.CheckAllScopesPresent(usergates.UserGates) {
		return fmt.Errorf("PermissibleScopes in server not matching dynamic config")
	}

	return nil
}
