SHELL := /bin/bash

GIT_COMMIT := $(shell git log -1 --pretty='%h')
GIT_BRANCH := $(shell git rev-parse --abbrev-ref HEAD)

TESTDIRS := ./cmd/... ./internal/... ./pkg/...
LINTDIRS := ./cmd ./internal ./pkg
LINTEXCLUDEDIRS := mocks|server
LINTDIRS := $(shell find $(LINTDIRS) -type d | grep -v -E "$(LINTEXCLUDEDIRS)")

.PHONY: build test

mock:
	# Remove old mocks
	rm -f mocks/nioh/*.go
	rm -f internal/auth/mocks/*.go
	rm -f internal/backend/mocks/*.go
	rm -f internal/clients/users/mocks/*.go
	rm -f internal/clients/subs/mocks/*.go
	rm -f internal/clients/voyager/mocks/*.go
	rm -f internal/models/mocks/*.go
	rm -f internal/clients/spade/mocks/*.go
	rm -f pkg/dynamo/dao/*.go
	rm -f pkg/errorlogger/mocks/*.go
	rm -f pkg/loaders/mocks/*.go
	rm -f config/mocks/*.go
	rm -f internal/clients/rbac/mocks/*.go
	# Generate mocks for tests
	mockery --note @generated --name API --output mocks/nioh --dir rpc/nioh
	mockery --note @generated --name Handler --output internal/auth/mocks --dir internal/auth
	mockery --note @generated --name IUsersClient --output internal/clients/users/mocks --dir internal/clients/users
	mockery --note @generated --name Client --output internal/clients/subs/mocks --dir internal/clients/subs
	mockery --note @generated --name IPubsubClient --output internal/clients/pubsub/mocks --dir internal/clients/pubsub
	mockery --note @generated --name PubClient --dir ./vendor/code.justin.tv//chat/pubsub-go-pubclient/client --output ./internal/clients/pubsub/mocks/
	mockery --note @generated --name IChannelProperties --output internal/clients/video/mocks --dir internal/clients/video
	mockery --note @generated --name ISiosaClient --output internal/clients/siosa/mocks --dir internal/clients/siosa
	mockery --note @generated --name ILivelineClient --output internal/clients/liveline/mocks --dir internal/clients/liveline
	mockery --note @generated --name Subscriptions --dir ./vendor/code.justin.tv/revenue/subscriptions/twirp --output ./internal/clients/subs/mocks/
	mockery --note @generated --name IClock --output internal/models/mocks --dir internal/models
	mockery --note @generated --name Client --output internal/clients/spade/mocks --dir ./vendor/code.justin.tv/common/spade-client-go/spade
	mockery --note @generated --name RBAC --dir ./vendor/code.justin.tv/devrel/devsite-rbac/rpc/rbacrpc --output internal/clients/rbac/mocks/vendormocks
	mockery --note @generated --name Client --dir internal/clients/rbac --output internal/clients/rbac/mocks
	mockery --note @generated --name Backend --output internal/backend/mocks --dir internal/backend # Make sure this is after all other internal/** mocks
	mockery --note @generated --name IExemptionChecker --output pkg/exemptions/mocks --dir pkg/exemptions
	mockery --note @generated --name ITestTableDAO --output pkg/dynamodb/dao/mocks --dir pkg/dynamodb/dao
	mockery --note @generated --name IChanletDAO --output pkg/dynamodb/dao/mocks --dir pkg/dynamodb/dao
	mockery --note @generated --name IChannelRelationDAO --output pkg/dynamodb/dao/mocks --dir pkg/dynamodb/dao
	mockery --note @generated --name IContentAttributeDAO --output pkg/dynamodb/dao/mocks --dir pkg/dynamodb/dao
	mockery --note @generated --name IRestrictionDAO --output pkg/dynamodb/dao/mocks --dir pkg/dynamodb/dao
	mockery --note @generated --name IPreviewDAO --output pkg/dynamodb/dao/mocks --dir pkg/dynamodb/dao
	mockery --note @generated --name ErrorLogger --output pkg/errorlogger/mocks --dir pkg/errorlogger
	mockery --note @generated --name IUserProductSubsLoader --output pkg/loaders/mocks --dir pkg/loaders
	mockery --note @generated --name IUserLoader --output pkg/loaders/mocks --dir pkg/loaders
	mockery --note @generated --name IDynamicConfiguration --output config/mocks --dir config
	mockery --note @generated --name Client --output internal/clients/zuma/mocks/vendormocks --dir ./vendor/code.justin.tv/chat/zuma/client
	mockery --note @generated --name Client --output internal/clients/vodapi/mocks --dir internal/clients/vodapi
	mockery --note @generated --name StarfruitTwitchChannelProps --dir ./vendor/code.justin.tv/amzn/StarfruitTwitchChannelPropsTwirp --output ./mocks
	mockery --note @generated --name Liveline --dir ./vendor/code.justin.tv/discovery/liveline/proto/liveline --output ./internal/clients/liveline/mocks
	mockery --note @generated --name Client --output internal/clients/voyager/mocks --dir internal/clients/voyager
	mockery --note @generated --name TwitchVoyager --dir ./vendor/code.justin.tv/amzn/TwitchVoyagerTwirp --output ./internal/clients/voyager/mocks/
	mockery --note @generated --name PDMSService --dir ./vendor/code.justin.tv/amzn/PDMSLambdaTwirp --output ./mocks
	mockery --note @generated --name SFNAPI --dir ./vendor/github.com/aws/aws-sdk-go/service/sfn/sfniface --output ./mocks
	# Generate any mocks declared with //go:generate
	go generate -run="mockery" ./...

quality:
	GO111MODULE=off go get github.com/alecthomas/gometalinter
	gometalinter --disable-all --vendor --tests --deadline=3m --enable=gofmt --enable=vet $(LINTDIRS)

dev: midway
	# Run nioh locally.
	env ENVIRONMENT=dev AWS_REGION=us-west-2 AWS_SDK_LOAD_CONFIG=1 AWS_PROFILE=twitch-nioh-dev BYPASS_DAX=true STATSD_HOST_PORT=localhost:0 go run cmd/nioh/main.go

dev-tunnel: tunnel midway
	# Run nioh locally through socks5 proxy.
	env ENVIRONMENT=dev SOCKS5_ADDR=localhost:8100 AWS_REGION=us-west-2 AWS_SDK_LOAD_CONFIG=1 AWS_PROFILE=twitch-nioh-dev BYPASS_DAX=true STATSD_HOST_PORT=localhost:0 go run cmd/nioh/main.go

tunnel:
	# Set up dynamic tunnel through teleport bastion and run in the background. Should run before dev-tunnel to allow socks proxy.
	# regular bastion box - 10.204.70.49 (for those who have ldap issue like Charles)
	TC=teleport-remote-twitch-nioh-dev ssh -fNT -D 8100 jumpbox

kill-tunnel:
	# Kill the ssh process for the tunnel above.
	kill $$(lsof -t -i:8100)

build:
	# Builds nioh.
	go build -mod vendor -tags "netgo" -ldflags "-X code.justin.tv/commerce/nioh/internal/server.BuildTime=$(shell date -u '+%Y-%m-%dT%H:%M:%S%z') -X code.justin.tv/commerce/nioh/internal/server.Version=${GIT_COMMIT}" code.justin.tv/commerce/nioh/cmd/nioh

build_linux:
	# Builds nioh for linux.
	env CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -mod vendor -tags "netgo" -ldflags "-X code.justin.tv/commerce/nioh/internal/server.BuildTime=$(shell date -u '+%Y-%m-%dT%H:%M:%S%z') -X code.justin.tv/commerce/nioh/internal/server.Version=${GIT_COMMIT}" code.justin.tv/commerce/nioh/cmd/nioh

# If the dynamodb_local folder does not exist, download and unpack
dynamodb_local:
	wget -q https://s3-us-west-2.amazonaws.com/dynamodb-local/dynamodb_local_latest.tar.gz
	mkdir -p dynamodb_local
	tar -C dynamodb_local -xzf dynamodb_local_latest.tar.gz

# Stop all running dynamo local instances and wait for them to shut down
dynamo_cleanup_%:
	pkill -f "^(\/usr\/bin\/)?java.*DynamoDBLocal.jar" ; true
	sleep 1

# Start a dynamo local in-memory instance
dynamo_start: dynamodb_local dynamo_cleanup_1
	java -Djava.library.path=dynamodb_local/DynamoDBLocal_lib -jar dynamodb_local/DynamoDBLocal.jar -port 9001 -inMemory &

test_setup: dynamo_start midway

test: test_setup
	# Runs unit tests.
	go test -mod vendor -race $(TESTDIRS)

test_local_code_coverage: test_setup
	# Generating code coverage information for local coverage % validation.
	rm -f coverage-all.out
	$(foreach dir,$(TESTDIRS),\
		touch coverage.out;\
		go test -mod vendor -timeout 10s -coverprofile=coverage.out -covermode=count $(dir);\
		tail -n +2 coverage.out >> coverage-all.out;\
		rm -f coverage.out;)
	./scripts/coverage_percent

generate:
	go generate -x ./rpc/nioh

quality_checks: quality build test test_local_code_coverage

# validating cloudformation templates can only run locally
local_checks: quality_checks validate_templates

test_generate_codecov:
	# Generating code coverage information to be used with CodeCov.
	gocov test -race $(TESTDIRS) | gocov-xml > coverage.xml

test_integration_dev: midway
	# Integration tests against dev RPCs intended to run locally for development.
	env ENVIRONMENT=dev AWS_PROFILE=twitch-nioh-dev AWS_SDK_LOAD_CONFIG=1 go test -mod vendor -tags=integration -count=1 -v -race ./test/internal_integration_tests

test_integration_beta:
	# Integration tests against beta RPCs.
	env ENVIRONMENT=beta AWS_PROFILE=twitch-nioh-dev go test -mod vendor -tags=integration -count=1 -v -race ./test/internal_integration_tests

test_integration_production:
	# Integration tests against production RPCs.
	env ENVIRONMENT=production go test -mod vendor -tags=integration -count=1 -v -race ./test/internal_integration_tests

test_public_integration_beta:
	# Integration tests against beta API gateway (Visage).
	env ENVIRONMENT=beta go test -mod vendor -count=1 -v -race ./test/external_integration_tests

test_public_integration_production:
	# Integration tests against production API gateway (Visage).
	env ENVIRONMENT=production go test -mod vendor -count=1 -v -race ./test/external_integration_tests

# The pipeline is defunct as the EB app is gone.
# We might need to delete this stack later when we confirm we don't need all the steps this pipeline runs.
# This command itself is left as is in case we want to modify (trim down) the pipeline in the future.
update_pipeline_stack:
	# Update the main CodePipeline definition.
	aws cloudformation update-stack --stack-name nioh-pipeline \
		--template-body file://$(GOPATH)/src/code.justin.tv/commerce/nioh/deployments/pipelines/production.yaml \
		--parameters file://$(GOPATH)/src/code.justin.tv/commerce/nioh/deployments/pipelines/production-params.json \
		--capabilities CAPABILITY_NAMED_IAM --profile twitch-nioh-aws

update_global_stack:
	# Update the global properties / resources CloudFormation stack.
	aws cloudformation update-stack --stack-name nioh-global \
		--template-body file://$(GOPATH)/src/code.justin.tv/commerce/nioh/deployments/global/global.yaml \
		--parameters file://$(GOPATH)/src/code.justin.tv/commerce/nioh/deployments/global/global-params.json \
		--capabilities CAPABILITY_NAMED_IAM --profile twitch-nioh-aws

# Use this in order to create a change set for global.yaml. You should preview and apply on aws console.
create_global_change_set:
	# Update the global properties / resources CloudFormation stack.
	aws cloudformation create-change-set --change-set-name nioh-global-cs --stack-name nioh-global \
		--template-body file://$(GOPATH)/src/code.justin.tv/commerce/nioh/deployments/global/global.yaml \
		--parameters file://$(GOPATH)/src/code.justin.tv/commerce/nioh/deployments/global/global-params.json \
		--capabilities CAPABILITY_NAMED_IAM --profile twitch-nioh-aws

update_global_stack_dev:
	# Update the global properties / resources CloudFormation dev stack.
	aws cloudformation update-stack --stack-name nioh-global \
		--template-body file://$(GOPATH)/src/code.justin.tv/commerce/nioh/deployments/global/global-dev.yaml \
		--parameters file://$(GOPATH)/src/code.justin.tv/commerce/nioh/deployments/global/global-dev-params.json \
		--capabilities CAPABILITY_NAMED_IAM --profile twitch-nioh-dev

update_alarms_stack:
	# alarms template file exceeded allowed size, so we must upload to s3 first
	aws s3 cp deployments/infra/alarms.yaml s3://twitch-nioh-aws/alarms.yaml --profile twitch-nioh-aws
	aws cloudformation update-stack --stack-name nioh-alarms \
		--template-url https://s3-us-west-2.amazonaws.com/twitch-nioh-aws/alarms.yaml \
		--parameters file://$(GOPATH)/src/code.justin.tv/commerce/nioh/deployments/infra/alarms-production.json \
		--capabilities CAPABILITY_NAMED_IAM --profile twitch-nioh-aws
	# When updating with cfncarnval, run the following command:
	# $(brazil-path run.runtimefarm)/bin/cfncarnaval --region us-west-2 --stackName nioh-alarms --materialSet com.amazon.credentials.isengard.483600360996.user/CloudFormationCarnaval -namePrefix Twitch.Nioh -owner ${YOUR_AMAZON_LOGIN} -cti Twitch/Rewards/Nioh -impact 2

update_alarms_stack_canary:
	# alarms template file exceeded allowed size, so we must upload to s3 first
	aws s3 cp deployments/infra/alarms.yaml s3://twitch-nioh-aws/alarms.yaml --profile twitch-nioh-aws
	aws cloudformation update-stack --stack-name nioh-alarms-canary \
		--template-url https://s3-us-west-2.amazonaws.com/twitch-nioh-aws/alarms.yaml \
		--parameters file://$(GOPATH)/src/code.justin.tv/commerce/nioh/deployments/infra/alarms-canary.json \
		--capabilities CAPABILITY_NAMED_IAM --profile twitch-nioh-aws
	# When updating with cfncarnval, run the following command:
	# $(brazil-path run.runtimefarm)/bin/cfncarnaval --region us-west-2 --stackName nioh-alarms-canary --materialSet com.amazon.credentials.isengard.483600360996.user/CloudFormationCarnaval -namePrefix Twitch.Nioh -owner ${YOUR_AMAZON_LOGIN} -cti Twitch/Rewards/Nioh -impact 2

update_dns_stack_dev: midway
	AWS_PROFILE=twitch-nioh-dev AWS_SDK_LOAD_CONFIG=1 aws cloudformation update-stack --stack-name nioh-dns \
							--template-body file://./deployments/dns/dns.yaml \
							--parameters file://./deployments/dns/dns-dev.json \
							--capabilities CAPABILITY_NAMED_IAM --profile twitch-nioh-dev

# Creates a change set for Route 53 records on prod. Preview and execute manually (from CLI or AWS console)
create_change_set_dns_stack: midway
	AWS_PROFILE=twitch-nioh-aws AWS_SDK_LOAD_CONFIG=1 aws cloudformation create-change-set --change-set-name prod-dns --stack-name nioh-dns \
							--template-body file://./deployments/dns/dns.yaml \
							--parameters file://./deployments/dns/dns-prod.json \
							--capabilities CAPABILITY_NAMED_IAM --profile twitch-nioh-aws

update_stepfn_stack_dev: midway
	AWS_PROFILE=twitch-nioh-dev AWS_SDK_LOAD_CONFIG=1 aws cloudformation update-stack --stack-name nioh-stepfn-dev \
							--template-body file://./deployments/infra/stepfn.yaml \
							--parameters ParameterKey=Environment,ParameterValue=development \
							--capabilities CAPABILITY_NAMED_IAM --profile twitch-nioh-dev

update_stepfn_stack_beta: midway
	AWS_PROFILE=twitch-nioh-dev AWS_SDK_LOAD_CONFIG=1 aws cloudformation update-stack --stack-name nioh-stepfn-beta \
							--template-body file://./deployments/infra/stepfn.yaml \
							--parameters ParameterKey=Environment,ParameterValue=beta \
							--capabilities CAPABILITY_NAMED_IAM --profile twitch-nioh-dev

update_stepfn_stack_prod: midway
	AWS_PROFILE=twitch-nioh-aws AWS_SDK_LOAD_CONFIG=1 aws cloudformation update-stack --stack-name nioh-stepfn-prod \
							--template-body file://./deployments/infra/stepfn.yaml \
							--parameters ParameterKey=Environment,ParameterValue=production \
							--capabilities CAPABILITY_NAMED_IAM --profile twitch-nioh-aws

update_dev_infra: update_dev_resources update_dev_ecs

update_dev_resources: midway
	AWS_PROFILE=twitch-nioh-dev AWS_SDK_LOAD_CONFIG=1 aws cloudformation update-stack --stack-name nioh-development-master \
							--template-body file://./deployments/infra/resources.yaml \
							--parameters ParameterKey=Environment,ParameterValue=development ParameterKey=AppName,ParameterValue=nioh \
							--capabilities CAPABILITY_NAMED_IAM --profile twitch-nioh-dev

update_dev_ecs: midway
	AWS_PROFILE=twitch-nioh-dev AWS_SDK_LOAD_CONFIG=1 aws cloudformation update-stack --stack-name app-nioh-development-master \
							--template-body file://./deployments/infra/ecs-app.yaml \
							--parameters ParameterKey=Environment,ParameterValue=development ParameterKey=EnvironmentName,ParameterValue=nioh-development ParameterKey=AppName,ParameterValue=nioh \
							--capabilities CAPABILITY_NAMED_IAM --profile twitch-nioh-dev

update_beta_infra: update_beta_resources update_beta_lambdas update_beta_ecs

update_beta_resources: midway
	AWS_PROFILE=twitch-nioh-dev AWS_SDK_LOAD_CONFIG=1 aws cloudformation update-stack --stack-name nioh-beta \
							--template-body file://./deployments/infra/resources.yaml \
							--parameters ParameterKey=Environment,ParameterValue=beta ParameterKey=AppName,ParameterValue=nioh \
							--capabilities CAPABILITY_NAMED_IAM --profile twitch-nioh-dev

update_beta_ecs: midway
	AWS_PROFILE=twitch-nioh-dev AWS_SDK_LOAD_CONFIG=1 aws cloudformation update-stack --stack-name ecs-nioh-beta \
							--template-body file://./deployments/infra/ecs-app.yaml \
							--parameters ParameterKey=Environment,ParameterValue=beta ParameterKey=EnvironmentName,ParameterValue=nioh-beta ParameterKey=AppName,ParameterValue=nioh \
							--capabilities CAPABILITY_NAMED_IAM --profile twitch-nioh-dev

# For Prod resources, we are only creating a change set without actual execution.
# One must preview the changes in console before applying them.
update_prod_infra: create_change_set_prod_resources update_prod_lambdas create_change_set_prod_ecs

create_change_set_prod_resources: midway
	AWS_PROFILE=twitch-nioh-aws AWS_SDK_LOAD_CONFIG=1 aws cloudformation create-change-set --stack-name nioh-production-resources --change-set-name prod-resources \
							--template-body file://./deployments/infra/resources.yaml \
							--parameters ParameterKey=Environment,ParameterValue=production ParameterKey=AppName,ParameterValue=nioh \
							--capabilities CAPABILITY_NAMED_IAM --profile twitch-nioh-aws

# Only run if ecs infrastructure has to be updated without going through pipeline
create_change_set_prod_ecs: midway
	AWS_PROFILE=twitch-nioh-aws AWS_SDK_LOAD_CONFIG=1 aws cloudformation create-change-set --stack-name ecs-nioh-production --change-set-name prod-ecs \
							--template-body file://./deployments/infra/ecs-app.yaml \
							--parameters ParameterKey=Environment,ParameterValue=production ParameterKey=EnvironmentName,ParameterValue=nioh-production ParameterKey=AppName,ParameterValue=nioh \
							--capabilities CAPABILITY_NAMED_IAM --profile twitch-nioh-aws

update_privatelink_beta:
	AWS_PROFILE=twitch-nioh-dev AWS_SDK_LOAD_CONFIG=1 aws s3 mb s3://commerce-nioh-beta.us-west-2.service-artifacts --profile twitch-nioh-dev || true
	AWS_PROFILE=twitch-nioh-dev AWS_SDK_LOAD_CONFIG=1 aws cloudformation package --template-file deployments/private-links/upstream-endpoints.yaml \
		--s3-bucket commerce-nioh-beta.us-west-2.service-artifacts \
		--s3-prefix privatelink \
		--profile twitch-nioh-dev \
		--output-template-file deployments/private-links/upstream-endpoints-transformed.yaml
	AWS_PROFILE=twitch-nioh-dev AWS_SDK_LOAD_CONFIG=1 aws cloudformation deploy --stack-name nioh-beta-privatelink \
		--template-file deployments/private-links/upstream-endpoints-transformed.yaml \
		--profile twitch-nioh-dev \
		--parameter-overrides $$(jq -r '.[] | [.ParameterKey, .ParameterValue] | join("=")' deployments/private-links/upstream-endpoints-dev-params.json) \
		--capabilities CAPABILITY_NAMED_IAM
	rm deployments/private-links/upstream-endpoints-transformed.yaml

update_privatelink_prod:
	AWS_PROFILE=twitch-nioh-aws AWS_SDK_LOAD_CONFIG=1 aws s3 mb s3://commerce-nioh-production.us-west-2.service-artifacts --profile twitch-nioh-aws || true
	AWS_PROFILE=twitch-nioh-aws AWS_SDK_LOAD_CONFIG=1 aws cloudformation package --template-file deployments/private-links/upstream-endpoints.yaml \
		--s3-bucket commerce-nioh-production.us-west-2.service-artifacts \
		--s3-prefix privatelink \
		--profile twitch-nioh-aws \
		--output-template-file deployments/private-links/upstream-endpoints-transformed.yaml
	AWS_PROFILE=twitch-nioh-aws AWS_SDK_LOAD_CONFIG=1 aws cloudformation deploy --stack-name nioh-production-privatelink \
		--template-file deployments/private-links/upstream-endpoints-transformed.yaml \
		--profile twitch-nioh-aws \
		--parameter-overrides $$(jq -r '.[] | [.ParameterKey, .ParameterValue] | join("=")' deployments/private-links/upstream-endpoints-params.json) \
		--capabilities CAPABILITY_NAMED_IAM
	rm deployments/private-links/upstream-endpoints-transformed.yaml

tools-lambda-deploy:
	# Package tools lambda - zips the binary and uploads to s3, prepares another template file for the `deploy` command
	# We are now using the lambdas built from a different repo. Copy the binaries to dist/lambda then use this command
	aws cloudformation package --template-file tools/lambda/tools-lambda.yaml \
		--s3-bucket twitch-nioh-aws \
		--s3-prefix tools-lambda \
		--region us-west-2 \
		--output-template-file dist/lambda/template.yml \
		--profile twitch-nioh-aws
	# Deploys tools lambda to AWS account
	aws cloudformation deploy --template-file dist/lambda/template.yml \
		--capabilities CAPABILITY_IAM --stack-name nioh-tools-lambda \
		--profile twitch-nioh-aws \
		--parameter-overrides AppName=nioh
	rm dist/lambda/template.yaml

validate_templates:
	echo "Checking AWS CloudFormation templates..."
	aws cloudformation validate-template --template-body file://deployments/global/global.yaml 1>/dev/null
	aws cloudformation validate-template --template-body file://deployments/global/global-dev.yaml 1>/dev/null
	aws cloudformation validate-template --template-body file://deployments/global/twitch-vpc.yaml 1>/dev/null
	aws cloudformation validate-template --template-body file://deployments/infra/resources.yaml 1>/dev/null
	aws cloudformation validate-template --template-body file://deployments/infra/ecs-app.yaml 1>/dev/null
	aws cloudformation validate-template --template-body file://deployments/pipelines/production.yaml 1>/dev/null
	aws cloudformation validate-template --template-body file://deployments/private-links/upstream-endpoints.yaml 1>/dev/null
	aws cloudformation validate-template --template-body file://tools/lambda/tools-lambda.yaml 1>/dev/null
	# aws cloudformation validate-template --template-body file://deployments/infra/alarms.yaml 1>/dev/null # File exceeds 51200 bytes. Requires uploading to S3 to correctly validate
	echo "AWS CloudFormation validation passed."

push_dynamic_configs_dev: midway
	AWS_PROFILE=twitch-nioh-dev AWS_SDK_LOAD_CONFIG=1 go run scripts/upload_dynamic_config/upload_dynamic_config.go -env=development -type=usergates

push_dynamic_configs_beta: midway
	AWS_PROFILE=twitch-nioh-dev AWS_SDK_LOAD_CONFIG=1 go run scripts/upload_dynamic_config/upload_dynamic_config.go -env=beta -type=usergates

push_dynamic_configs_prod: midway
	AWS_PROFILE=twitch-nioh-aws AWS_SDK_LOAD_CONFIG=1 go run scripts/upload_dynamic_config/upload_dynamic_config.go -env=production -type=usergates

# Download 8 logs from last hour
# It will automatically open the png analysis of one of the files. User is expected to further interact with profile sources directly with "go tool pprof"
# Make sure you installed graphviz: brew install graphviz
analyze_autoprof_logs:
	rm -rf ./autoprof_logs
	go run scripts/autoprof/download_autoprof_logs.go -max=8
	rm -rf ./autoprof_logs/*.zip
	./scripts/autoprof/create-profile-png.sh

tunnel_canary:
	./scripts/tunnel-canary.sh

midway:
	./scripts/midway_init.sh

define execute_on_func_directories
	for i in cmd/lambda/* ; do \
		if [ -d "$$i" ]; then \
			(eval $(1)); \
		fi \
	done
endef

# Build each lambda function
build_lambdas:
	$(call execute_on_func_directories, 'cd "$$i" && GOOS=linux GOARCH=amd64 go build -mod vendor -o main *.go && zip main.zip ./main')

update_beta_lambdas: midway build_lambdas
	AWS_PROFILE=twitch-nioh-dev sam deploy --config-file ../../config/beta/samconfig.toml -t ./deployments/infra/lambdas.yaml

update_prod_lambdas: midway build_lambdas
	AWS_PROFILE=twitch-nioh-aws sam deploy --config-file ../../config/production/samconfig.toml -t ./deployments/infra/lambdas.yaml
