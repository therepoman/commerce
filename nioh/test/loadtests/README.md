# nioh load testing

## Tool candidates

We are currently experimenting with different load test tools. The issue we have for Nioh that we didn't have for Fortuna and Mako is that our service is running in a VPC that is not reachable from the Twitch internal network. Our potential tools/strategies to get around this issue include:

- TPSGenerator with PublicEC2 - https://w.amazon.com/index.php/TPSGenerator/Running/PublicEC2
- Jmeter - https://jmeter.apache.org/ (via ec2 proxy we setup in our VPC)
- Vegeta running inside of our EC2 instances -  https://github.com/tsenart/vegeta
- nebbiolo - In-house tool built on top of Vegeta https://git-aws.internal.justin.tv/commerce/nebbiolo

For the TPSGenerator, it states the following

```
If you're trying to load test an internal endpoint, then running on Public EC2 is NOT for you. The public EC2 support is intended for testing publicly visible endpoints...
```

which makes this unfit for our needs. Even if we can somehow use an EC2 instance within the VPC, it looks like we need to request allowlisting via SIM ticket, and there are other Amazon specific tools involved to get this completely setup.

Vegeta is good for simple tests, but it's not as robust as other tools. Especially, it's hard to randomize requests making the load test even more non-organic. Our Fortuna load tests have been just making the same exact requests with the same payload for a constant rate. This might not be ideal.

jmeter was the initial choice and had been useful for various load tests. However, we found that it wasn't ideal for hitting high TPS tests (order of 10^5 TPS was near impossible) probably due to its overhead.

nebbiolo, which was built in early 2019 is the current default load test tool for Nioh.

## [Nebbiolo](https://git-aws.internal.justin.tv/commerce/nebbiolo)

Nebbiolo currently requires a single TOML file that defines the target, request variation data, and load configurations. We save the TOML files in `test/loadtests/nebbiolo` directory. Use previous files as examples. You can see full documentation [here](https://git-aws.internal.justin.tv/commerce/nebbiolo).

Here's an example of running a loadtest from a toml file.

```
nebbiolo -plan=test/loadtests/nebbiolo/GetPermissions.toml -proxy=socks5://localhost:8080
```

### Running Nebbiolo on EC2

Sometimes you just need more juice... Set up an EC2 instance if one doesn't exist. SSH into the box, `scp` a Nebbiolo binary, and fire from low orbit.

Script to create EC2 instance
```bash
AWS_PROFILE=twitch-nioh-aws aws ec2 run-instances \
  --image-id ami-01bbe152bf19d0289 \
  --instance-type c4.8xlarge --key-name nioh \
  --security-group-ids sg-0acd8df3f70bfb5d5 \
  --subnet-id subnet-0589501c945f1b905 \
  --tag-specifications 'ResourceType=instance,Tags=[{Key=Name,Value=nebbiolo-load-test-server}]' \
  --count 1 \
  --user-data file://./test/loadtests/nebbiolo/setup_ec2_nebbiolo_client
```

## [jmeter](https://jmeter.apache.org/)

jmeter had been used before nebbiolo was created. It can still prove useful for its robust tooling and ability to generate good looking reports. The caveat is that we found that jmeter cannot handle high TPS tests (one machine cannot handle more than 1k TPS). To install it on your laptop, use homebrew to keep your sanity.

```
brew install jmeter
```

To circumvent the VPC issue, create a socks proxy using `ssh -D 8080 {ip-to-your-ec-instance}` first.

Run jmeter GUI with proxy to create and run test plans
```
JVM_ARGS='-DsocksProxyHost=localhost -DsocksProxyPort=8080' jmeter
```

*Technically you can edit the xml file directly. In some cases, this is the faster way to achieve your goal.*

Run jmeter non-GUI for executing a test plan that is already created.
```
JVM_ARGS='-DsocksProxyHost=localhost -DsocksProxyPort=8080' jmeter -n -t <path-to-test-plan-file> -Jduration=<duration in seconds> -Jload=<desired tps>
```

For example, in order to execute `GetChanlets` load test for 10 minutes with 20TPS load, run
```
JVM_ARGS='-DsocksProxyHost=localhost -DsocksProxyPort=8080' jmeter -n -t test/loadtests/jmeter/GetChanlets.jmx -Jduration=600 -Jload=20
```

which should generate outputs like
```
Starting the test @ Mon Oct 08 14:39:20 PDT 2018 (1539034760298)
Waiting for possible Shutdown/StopTestNow/Heapdump message on port 4445
summary +    175 in 00:00:09 =   19.4/s Avg:    46 Min:    36 Max:   180 Err:     0 (0.00%) Active: 27 Started: 27 Finished: 0
summary +    672 in 00:00:30 =   22.4/s Avg:    43 Min:    36 Max:   100 Err:     0 (0.00%) Active: 100 Started: 100 Finished: 0
summary =    847 in 00:00:39 =   21.7/s Avg:    44 Min:    36 Max:   180 Err:     0 (0.00%)
summary +    600 in 00:00:30 =   20.0/s Avg:    50 Min:    35 Max:   142 Err:     0 (0.00%) Active: 100 Started: 100 Finished: 0
summary =   1447 in 00:01:09 =   21.0/s Avg:    46 Min:    35 Max:   180 Err:     0 (0.00%)
summary +    600 in 00:00:30 =   20.0/s Avg:    47 Min:    37 Max:   113 Err:     0 (0.00%) Active: 100 Started: 100 Finished: 0
summary =   2047 in 00:01:39 =   20.7/s Avg:    46 Min:    35 Max:   180 Err:     0 (0.00%)
summary +    600 in 00:00:30 =   20.0/s Avg:    47 Min:    36 Max:   130 Err:     0 (0.00%) Active: 100 Started: 100 Finished: 0
summary =   2647 in 00:02:09 =   20.5/s Avg:    46 Min:    35 Max:   180 Err:     0 (0.00%)
.
.
.
```

The easiest way to create a new test plan is to making a copy of the existing one, then tweaking values from there.

### Configuring Throughput/Duration

jmeter does not use the term TPS or RPS. A semi-constant throughput is maintained via the Constant Throughput Timer node. Using a variable, we can pass the amount via the command line (which is how it's setup currently). jmeter does not start testing with the configured throughput value immediately. It starts creating the number of threads (users) predetermined in the Thread Group node for the duration defined in the Ramp Up Period (if Ramp Up Period is 30, it creates the configured amount of threads at 30 second mark starting from creating just one thread at 0 second mark). This allows us to "soften up" the traffic unlike the blocky traffic resulted with a tool like Vegeta. Also, the wait time between each sample is randomized (so the TPS isn't really "constant", but it fluctuates around that value), which also makes the traffic look a bit more organic. The actual duration of the test is also configured in Thread Group node, but it can also be passed from the command line as a variable, which is how it is setup now.

### Randomizing Requests

jmeter supports a pre-processor, which can be a script that generates a variable for each request. JSR223 PreProcessor supports Javascript, which is usually the language to use. You can also use BeanShell, which is Java. The key to randomizing a variable is to generate a random string in the pre-processor, then exporting it using the `put` method.

```javascript
vars.put("channel_id", generateRandomChannelID())
```

Then this exported variable is available in the Request body via the accessor `${channel_id}`.

### Reporting

Reporting in jmeter is done via attached listeners when using the GUI. It's not recommended to use the GUI when running the load test. When you are running the test using the headless version, basic stats will be output to stdout. You can choose to output to a file using the option `-e -o <dashboard_folder>`.

### Testing the test plans

When you are in GUI, you should test whether your test plan is doing what it should. You can create (or enable if it's just disabled) View Results Tree node. After a test is run, you can view request/reponse for each individual sample. If you confirm the request/response to be generally correct, save and run the test in a headless state.

### Distributed Testing

JMeter supports remote testing, which means we can leverage multiple hosts to execute a distributed testing.

#### Server Setup

Because of the VPC being not reachable from the Twitch backbone, the simplest solution is to simply use EC2 boxes in the same VPC as Nioh. I spun up a couple of Ubuntu boxes and was able to run the test remotely. Jmeter installation and starting the jmeter server is automated with user data (stored as a file in this repo at test/loadtests/ec2). Host instances are already created for you to use. Simply start the instances if they are stopped. You can also launch another ec2 instance by running a command like this:

```bash
AWS_PROFILE=twitch-nioh-aws aws ec2 run-instances \
  --image-id ami-061392db613a6357b \
  --instance-type c4.8xlarge --key-name nioh \
  --security-group-ids sg-0acd8df3f70bfb5d5 \
  --subnet-id subnet-0589501c945f1b905 \
  --tag-specifications 'ResourceType=instance,Tags=[{Key=Name,Value=jmeter-load-test-server}]' \
  --count 1 \
  --user-data file://./test/loadtests/ec2/server_ec2_user_data
```

This will spin up the instance and launch the server (see the server_ec2_user_data file for exact commands being executed). Even though at this point, you can start testing, I still recommend that you SSH into all the running instances because not everything is visible from the client. Also, in case something goes wrong, in order to stop the test, the best way is to stop it is from the running servers, not from the client (using the `bin/shutdown.sh <port>` command). Or if you are panicking heavily because you are causing a downstream service a sev2, stopping/terminating the test server instances is not a bad idea.

When the server isn't starting on its own (which might be the case if you are just starting the server from a stopped state), you can SSH into the server than start the server by issuing the following commands.

```bash
sudo su
cd ~/jmeter/apache-jmeter-5.0
bin/jmeter-server -Jserver_port=55501 \
  -Jserver.rmi.localport=55511 \
  -Jserver.rmi.ssl.disable=true > ~/jmeter_stdout &
```

The server outputs logs to a file at `/root/jmeter_stdout`. If you want to access this file, ssh in to the box, and run `sudo cat /root/jmeter_stdout`. You can skip `> ~/jmeter_stdout &` part of the command if you rather run the server in non-daemon mode.

#### Client Setup

On the client side, we currently ssh into a separate client instance. (Some attempts to use my laptop as a client were made via proxy. However, outputs look like they are succeeding, but no traffic is actually made to Nioh for some reason. It is less problematic to just ssh into an ec2 and run the test from there.) 

The client instance is not much different than the server instances. It is currently a t2.medium instance, and it has jmeter and git installed. You can simply ssh into the box, pull repo for the test template, then run the test.

*Note: we are using teleport-remote to ssh into the client box. It is created using the user-data contained in the `test/loadtests/ec2/client_ec2_user_data` file. If your public key is not there, you won't be able to ssh into it. Simply follow [cloud-init](https://wiki.twitch.com/display/SEC/Instance+cloud-init+Configuration+for+Bastion+Support) instructions to add yours there. Make sure to never expose your private key (id_rsa) and only paste the public key (id_rsa.pub). If your private key is checked into the repo, you will want to regenerate your key-pair.*

```bash
TC=teleport-remote-twitch-nioh-prod ssh 10.204.74.73
sudo su
cd /root/nioh
git pull
```

Then we can run the headless load test by using the command:

```bash
cd ~/nioh
~/jmeter/apache-jmeter-5.0/bin/jmeter -n -r -t test/loadtests/jmeter/GetChanlets.jmx \
  -Jremote_hosts=10.204.74.198:55501,10.204.74.120:55501 -Jserver.rmi.ssl.disable=true -Jclient.rmi.localport=55512
```

Note that the script parameters such as `-Jduration=600` and `-Jload=200` do not work. The test template file itself must define the load for distributed load tests. I recommend creating a separate template that has multiple targets and real load amount. Duration is defined in the thread group node (direct child of the test plan node), and the TPS defined in the Constant Throughput Timer node (`Target throughput`).

If you want to try running the remote test from your laptop, the command to run is:

```bash
JVM_ARGS='-DsocksProxyHost=localhost -DsocksProxyPort=8080' jmeter -n -r -t test/loadtests/jmeter/GetChanlets.jmx -Jduration=600 -Jload=200 \
  -Jremote_hosts=10.204.74.198:55501 -Jserver.rmi.ssl.disable=true -Jclient.rmi.localport=55512
```

and as noted above, it does not work. It produces outputs like `Remote engines have been started`, but the remote server sends no traffic. You are welcome to diagnose and fix this.

### Notes on throughput

*I found that Constant Throughput Timer does not work very well at very high TPS (500TPS+ per server). I'm not sure if it's due to the wrong configuration on the thread number and ramp up time. Feel free to tweak these values to achieve your target load. It seems like the latency of the response itself contributes to the throughput calculation, so there is no straight-forward way to stabilize the throughput. On the bright side, the traffic looks more organic because the threads simulate users, and users aren't allowed to make simultaneous requests (throughput will therefore drop if latency becomes higher).*

**Important: The load you are defining in the test case is PER SERVER. If you are running 8 test servers and you are targeting 4000TPS, the test plan file should be configured to hit 500TPS, and each server will execute this same configuration, and thus hitting the total throughput of 4000TPS.**

*If you are lazy and do not want to keep committing test plan changes and pull from the client box in order to have your test plan updated, you can just use VIM to copy and paste your test plan file. If you are not familiar with VIM, the commands I use are:*

1. `[[` to move the cursor to the beginning of the file.
2. `dG` to delete the entire file.
3. `i` to go into the insert mode.
4. `cmb+v` to paste the content in your clip board (copy from your laptop beforehand)
5. `esc` to exit out of the insert mode.
6. `:wq` to save and quit.

## I just want to run a test. Tell me what to do.

If you want to run a low TPS (less than 1k) on a single API target, run
```bash
ssh -D 8080 {ip-to-your-ec-instance}
JVM_ARGS='-DsocksProxyHost=localhost -DsocksProxyPort=8080' jmeter -n -t test/loadtests/jmeter/YOUR_API_TEST.jmx -Jduration=600 -Jload=100
```

If you want to run a high TPS or/and have multiple API targets, it is critical that you actually read all the nuances documented above. Then do the following:

1. Create such test case in test/loadtests/jmeter with load specified in the file. Commit to a branch.
2. Go to AWS console (AWS -> EC2 -> Instances) and start `load-test-server-n` instances if they are in stopped status. If there are terminated or you need more instances, start them using the aws cli command. This is not that straight forward so read above details.
3. SSH into the client and run the test.
```bash
TC=teleport-remote-twitch-nioh-prod ssh 10.204.74.73
sudo su
cd ~/nioh
git checkout <branch-name>
git pull
~/jmeter/apache-jmeter-5.0/bin/jmeter -n -r -t test/loadtests/jmeter/YOUR_TEST.jmx \
  -Jremote_hosts=10.204.74.198:55501,10.204.74.120:55501 -Jserver.rmi.ssl.disable=true -Jclient.rmi.localport=55512
```