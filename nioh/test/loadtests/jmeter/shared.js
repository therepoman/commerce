// TODO: It would be nice if this function can be shared across all test plans.
// Might be possible using this method? https://github.com/lifecube/JMeter-Load-Javascript-Libraries-For-Future-Usage
function generateRandomChannelID(numPossible) {
  var text = "123456";
  numPossible = numPossible || 5000;
  return text + Math.floor(Math.random() * numPossible);
}