// +build integration

package integration

import (
	"context"
	"testing"
	"time"

	"code.justin.tv/commerce/nioh/internal/clients/zuma"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/nioh/internal/models"
	"code.justin.tv/commerce/nioh/pkg/loaders"
	"code.justin.tv/commerce/nioh/rpc/nioh"
	"github.com/golang/protobuf/ptypes"
	. "github.com/smartystreets/goconvey/convey"
)

func TestUserAuthorizationAPI(t *testing.T) {
	Convey("Test User Authorization APIs", t, func() {
		ctx := loaders.New(testDeps.niohClients).Attach(context.Background())
		past := time.Date(2018, time.February, 7, 9, 0, 0, 0, time.UTC)
		future := time.Date(3019, time.February, 7, 9, 0, 0, 0, time.UTC)

		Convey("Test GetUserAuthorization", func() {
			Convey("for VOD", func() {
				testUserID := "203316457" // camhux

				req := &nioh.GetUserAuthorizationRequest{
					UserId: testUserID,
					Resource: &nioh.GetUserAuthorizationRequest_Vod{
						Vod: &nioh.VOD{
							ChannelId: "186243532",
							VideoId:   testVodID,
						},
					},
				}

				expected := &nioh.GetUserAuthorizationResponse{
					UserId:               testUserID,
					UserIsAuthorized:     true,
					ResourceIsRestricted: true,
					Resource: &nioh.GetUserAuthorizationResponse_Vod{
						Vod: &nioh.VOD{
							ChannelId: "186243532",
							VideoId:   testVodID,
						},
					},
				}

				Convey("with invalid arguments", func() {
					Convey("empty productName in reason data", func() {
						req.GetVod().HedwigReasonData = "{\"productName\": \"\"}"
					})

					_, err := testDeps.niohServer.GetUserAuthorization(ctx, req)
					So(err, ShouldNotBeNil)
				})

				// hedwig is completely offboarded and integration tests are removed

				Convey("without hedwig data", func() {
					Convey("without restriction", func() {
						expected.ResourceIsRestricted = false
						res, err := testDeps.niohServer.GetUserAuthorization(ctx, req)
						So(err, ShouldBeNil)
						So(UserAuthRespResembles(res, expected), ShouldBeTrue)
					})

					if !isProd {
						Convey("with product restriction", func() {
							restriction := &models.RestrictionV2{
								ResourceKey:     "vod:" + testVodID,
								RestrictionType: models.SubOnlyLiveRestrictionType,
								Exemptions: []*models.Exemption{
									{
										ExemptionType:   models.ProductExemptionType,
										ActiveStartDate: past,
										ActiveEndDate:   future,
										Keys:            []string{testProductID},
									},
								},
							}

							err := testDeps.daos.RestrictionDAO.SetRestriction(ctx, restriction)
							defer testDeps.daos.RestrictionDAO.DeleteRestriction(ctx, "vod:"+testVodID)

							expected.UserIsAuthorized = false
							expected.ResourceIsRestricted = true
							expected.RestrictionType = models.RestrictionTypeToProto(restriction.RestrictionType)
							res, err := testDeps.niohServer.GetUserAuthorization(ctx, req)
							So(err, ShouldBeNil)
							So(UserAuthRespResembles(res, expected), ShouldBeTrue)
						})

						Convey("with restriction but user is exempt", func() {
							restriction := &models.RestrictionV2{
								ResourceKey:     "vod:" + testVodID,
								RestrictionType: models.SubOnlyLiveRestrictionType,
								Exemptions: []*models.Exemption{
									{
										ExemptionType:   models.AllExemptionType,
										ActiveStartDate: past,
										ActiveEndDate:   future,
									},
								},
							}

							err := testDeps.daos.RestrictionDAO.SetRestriction(ctx, restriction)
							defer testDeps.daos.RestrictionDAO.DeleteRestriction(ctx, "vod:"+testVodID)

							expected.UserIsAuthorized = true
							expected.ResourceIsRestricted = true
							expected.RestrictionType = models.RestrictionTypeToProto(restriction.RestrictionType)
							res, err := testDeps.niohServer.GetUserAuthorization(ctx, req)
							appliedExemptions, err := models.ExemptionsToProto(restriction.Exemptions)
							So(err, ShouldBeNil)
							appliedExemptions[0].ExemptionKeys = []string{}
							expected.AppliedExemptions = appliedExemptions

							So(err, ShouldBeNil)
							So(UserAuthRespResembles(res, expected), ShouldBeTrue)
						})
					}
				})
			})

			Convey("for Channel", func() {
				testChannel := &nioh.Channel{Id: testChannelID}
				req := &nioh.GetUserAuthorizationRequest{
					UserId: testUserID,
					Resource: &nioh.GetUserAuthorizationRequest_Channel{
						Channel: testChannel,
					},
				}

				expected := &nioh.GetUserAuthorizationResponse{
					UserId:               testUserID,
					UserIsAuthorized:     true,
					ResourceIsRestricted: false,
					Resource: &nioh.GetUserAuthorizationResponse_Channel{
						Channel: testChannel,
					},
					AppliedExemptions: []*nioh.Exemption{},
				}

				Convey("invalid arguments", func() {
					Convey("without channel id", func() {
						testChannel.Id = ""
					})

					_, err := testDeps.niohServer.GetUserAuthorization(ctx, req)
					So(err, ShouldNotBeNil)
				})

				Convey("without restriction", func() {
					res, err := testDeps.niohServer.GetUserAuthorization(ctx, req)
					So(err, ShouldBeNil)
					So(UserAuthRespResembles(res, expected), ShouldBeTrue)
				})

				if !isProd {
					Convey("with product restriction", func() {
						// Create an exempt all restriction
						restriction := &models.RestrictionV2{
							ResourceKey:     "channel:" + testChannelID,
							RestrictionType: models.SubOnlyLiveRestrictionType,
							Exemptions: []*models.Exemption{
								{
									ExemptionType:   models.AllExemptionType,
									ActiveStartDate: past,
									ActiveEndDate:   future,
								},
							},
						}

						err := testDeps.daos.RestrictionDAO.SetRestriction(ctx, restriction)
						defer testDeps.daos.RestrictionDAO.DeleteRestriction(ctx, "channel:"+testChannelID)

						So(err, ShouldBeNil)

						expected.ResourceIsRestricted = true
						expected.RestrictionType = models.RestrictionTypeToProto(restriction.RestrictionType)
						appliedExemptions, err := models.ExemptionsToProto(restriction.Exemptions)
						So(err, ShouldBeNil)
						appliedExemptions[0].ExemptionKeys = []string{}
						expected.AppliedExemptions = appliedExemptions

						res, err := testDeps.niohServer.GetUserAuthorization(ctx, req)
						So(err, ShouldBeNil)
						So(UserAuthRespResembles(res, expected), ShouldBeTrue)
					})

					Convey("with product sub restriction", func() {
						// Create a product sub restriction
						restriction := &models.RestrictionV2{
							ResourceKey:     "channel:" + testChannelID,
							RestrictionType: models.SubOnlyLiveRestrictionType,
							Exemptions: []*models.Exemption{
								{
									ExemptionType:   models.ProductExemptionType,
									ActiveStartDate: past,
									ActiveEndDate:   future,
									Keys:            []string{testProductID},
								},
							},
						}

						err := testDeps.daos.RestrictionDAO.SetRestriction(ctx, restriction)
						defer testDeps.daos.RestrictionDAO.DeleteRestriction(ctx, "channel:"+testChannelID)

						So(err, ShouldBeNil)

						expected.ResourceIsRestricted = true
						expected.RestrictionType = models.RestrictionTypeToProto(restriction.RestrictionType)

						Convey("and user does not have product", func() {
							// Assume user doesn't already have the product
							expected.UserIsAuthorized = false
						})

						/* Skipped due to problematic behaviors of sub dependency -- seems that it doesn't create the test subscription correctly.
						Convey("and user has product", func() {
							// Create a test subs
							_, err := niohClients.Subscriptions.CreateTestSubscription(context.Background(), testUserID, testProductID)
							defer niohClients.Subscriptions.CancelTestSubscription(context.Background(), testUserID, testProductID)

							So(err, ShouldBeNil)
							expected.UserIsAuthorized = true
						})
						*/

						Convey("and user is anonymous", func() {
							req.UserId = ""
							expected.UserId = ""
							expected.UserIsAuthorized = false
						})

						authRes, authErr := testDeps.niohServer.GetUserAuthorization(ctx, req)
						So(authErr, ShouldBeNil)
						So(UserAuthRespResembles(authRes, expected), ShouldBeTrue)
					})

					Convey("with product sub restriction and free preview", func() {
						// Create a product sub restriction with preview
						restriction := &models.RestrictionV2{
							ResourceKey:     "channel:" + testChannelID,
							RestrictionType: models.SubOnlyLiveRestrictionType,
							Exemptions: []*models.Exemption{
								{
									ExemptionType:   models.PreviewExemptionType,
									ActiveStartDate: past,
									ActiveEndDate:   future,
									Keys:            []string{},
								},
							},
						}

						req.ConsumePreview = true

						err := testDeps.daos.RestrictionDAO.SetRestriction(ctx, restriction)
						defer testDeps.daos.RestrictionDAO.DeleteRestriction(ctx, "channel:"+testChannelID)

						So(err, ShouldBeNil)

						expected.ResourceIsRestricted = true
						expected.RestrictionType = models.RestrictionTypeToProto(restriction.RestrictionType)

						Convey("and user is given a preview", func() {
							testTime := time.Now().Add(models.PreviewDuration)
							testTimeFuzzyMatchRange := 500 * time.Millisecond
							upperExpectedTime := testTime.Add(testTimeFuzzyMatchRange)
							lowerExpectedTime := testTime.Add(-testTimeFuzzyMatchRange)

							authRes, authErr := testDeps.niohServer.GetUserAuthorization(ctx, req)
							firstAccessExpiration := authRes.AccessExpiration
							accessExpiration, _ := ptypes.Timestamp(firstAccessExpiration)

							timeline := []time.Time{
								lowerExpectedTime,
								accessExpiration,
								upperExpectedTime,
							}

							So(authErr, ShouldBeNil)
							So(authRes.UserIsAuthorized, ShouldBeTrue)
							So(authRes.AccessExpiration, ShouldNotBeNil)
							So(timeline, ShouldBeChronological)

							// Here, we're waiting a few seconds and running through the business logic
							// of consuming the preview a second time to ensure the consistency of the
							// accessExpiration. If the values don't match, then the initial write failed,
							// or there's another business logic flaw.
							logrus.Debug("Sleeping 3 seconds to try preview again...")
							time.Sleep(3 * time.Second)

							authRes, authErr = testDeps.niohServer.GetUserAuthorization(ctx, req)

							So(authErr, ShouldBeNil)
							So(authRes.UserIsAuthorized, ShouldBeTrue)
							// Now that offline and unknown status channels vend unrecorded previews,
							// this check would require an always on channel
							So(authRes.AccessExpiration, ShouldNotBeNil)

							// This should vend a new free preview token that is unique and different to the previous token due to the origin
							Convey("with origin provided in the request", func() {
								testTime := time.Now().Add(models.PreviewDuration)
								testTimeFuzzyMatchRange := 500 * time.Millisecond
								upperExpectedTime := testTime.Add(testTimeFuzzyMatchRange)
								lowerExpectedTime := testTime.Add(-testTimeFuzzyMatchRange)

								req.Origin = "android:autoplay"
								authRes, authErr := testDeps.niohServer.GetUserAuthorization(ctx, req)
								firstAccessExpiration := authRes.AccessExpiration
								accessExpiration, _ := ptypes.Timestamp(firstAccessExpiration)

								timeline := []time.Time{
									lowerExpectedTime,
									accessExpiration,
									upperExpectedTime,
								}

								So(authErr, ShouldBeNil)
								So(authRes.UserIsAuthorized, ShouldBeTrue)
								So(authRes.AccessExpiration, ShouldNotBeNil)
								So(timeline, ShouldBeChronological)

								logrus.Debug("Sleeping 3 seconds to try preview again...")
								time.Sleep(3 * time.Second)

								authRes, authErr = testDeps.niohServer.GetUserAuthorization(ctx, req)

								So(authErr, ShouldBeNil)
								So(authRes.UserIsAuthorized, ShouldBeTrue)
								So(authRes.AccessExpiration, ShouldNotBeNil)
							})
						})

						Convey("and user is anonymous", func() {
							req.UserId = ""
							expected.UserId = ""
							expected.UserIsAuthorized = false

							authRes, authErr := testDeps.niohServer.GetUserAuthorization(ctx, req)

							So(authErr, ShouldBeNil)
							So(UserAuthRespResembles(authRes, expected), ShouldBeTrue)
						})

						testDeps.daos.PreviewDAO.DeletePreview(ctx, testUserID+":channel:"+testChannelID)
					})

					Convey("with VIP-exempt restriction", func() {
						testAffiliateChannelID := "436929429" // ccm_test

						req.Resource = &nioh.GetUserAuthorizationRequest_Channel{
							Channel: &nioh.Channel{Id: testAffiliateChannelID},
						}

						expected.Resource = &nioh.GetUserAuthorizationResponse_Channel{
							Channel: &nioh.Channel{Id: testAffiliateChannelID},
						}

						zumaIntegrationClient, ok := zuma.ConvertToIntegrationClient(testDeps.niohClients.Zuma)
						So(ok, ShouldBeTrue)

						restriction := &models.RestrictionV2{
							ResourceKey:     "channel:" + testAffiliateChannelID,
							RestrictionType: models.SubOnlyLiveRestrictionType,
							Exemptions: []*models.Exemption{
								{
									ExemptionType:   models.ChannelVIPExemptionType,
									ActiveStartDate: past,
									ActiveEndDate:   future,
									Keys:            []string{testAffiliateChannelID},
								},
							},
						}

						err := testDeps.daos.RestrictionDAO.SetRestriction(ctx, restriction)
						defer testDeps.daos.RestrictionDAO.DeleteRestriction(ctx, restriction.ResourceKey)

						expected.ResourceIsRestricted = true
						expected.RestrictionType = nioh.Restriction_SUB_ONLY_LIVE

						So(err, ShouldBeNil)

						Convey("and user is VIP", func() {
							err := zumaIntegrationClient.AddVIP(ctx, testAffiliateChannelID, testUserID)
							defer zumaIntegrationClient.RemoveVIP(ctx, testAffiliateChannelID, testUserID)
							So(err, ShouldBeNil)

							expected.UserIsAuthorized = true
							appliedExemptions, _ := models.ExemptionsToProto(restriction.Exemptions)
							expected.AppliedExemptions = appliedExemptions

							authRes, authErr := testDeps.niohServer.GetUserAuthorization(ctx, req)
							So(authErr, ShouldBeNil)
							So(UserAuthRespResembles(authRes, expected), ShouldBeTrue)
						})

						Convey("and user is not VIP", func() {
							expected.UserIsAuthorized = false
							expected.AppliedExemptions = []*nioh.Exemption{}

							authRes, authErr := testDeps.niohServer.GetUserAuthorization(ctx, req)
							So(authErr, ShouldBeNil)
							So(UserAuthRespResembles(authRes, expected), ShouldBeTrue)
						})

						Convey("and user is anonymous", func() {
							req.UserId = ""
							expected.UserId = ""

							expected.UserIsAuthorized = false
							expected.AppliedExemptions = []*nioh.Exemption{}

							authRes, authErr := testDeps.niohServer.GetUserAuthorization(ctx, req)
							So(authErr, ShouldBeNil)
							So(UserAuthRespResembles(authRes, expected), ShouldBeTrue)
						})
					})

					Convey("with moderator-exempt restriction", func() {
						testAffiliateChannelID := "436929429" // ccm_test

						req.Resource = &nioh.GetUserAuthorizationRequest_Channel{
							Channel: &nioh.Channel{Id: testAffiliateChannelID},
						}

						expected.Resource = &nioh.GetUserAuthorizationResponse_Channel{
							Channel: &nioh.Channel{Id: testAffiliateChannelID},
						}

						zumaIntegrationClient, ok := zuma.ConvertToIntegrationClient(testDeps.niohClients.Zuma)
						So(ok, ShouldBeTrue)

						restriction := &models.RestrictionV2{
							ResourceKey:     "channel:" + testAffiliateChannelID,
							RestrictionType: models.SubOnlyLiveRestrictionType,
							Exemptions: []*models.Exemption{
								{
									ExemptionType:   models.ChannelModeratorExemptionType,
									ActiveStartDate: past,
									ActiveEndDate:   future,
									Keys:            []string{testAffiliateChannelID},
								},
							},
						}

						err := testDeps.daos.RestrictionDAO.SetRestriction(ctx, restriction)
						defer testDeps.daos.RestrictionDAO.DeleteRestriction(ctx, restriction.ResourceKey)

						expected.ResourceIsRestricted = true
						expected.RestrictionType = nioh.Restriction_SUB_ONLY_LIVE

						So(err, ShouldBeNil)

						Convey("and user is moderator", func() {
							err := zumaIntegrationClient.AddMod(ctx, testAffiliateChannelID, testUserID)
							defer zumaIntegrationClient.RemoveMod(ctx, testAffiliateChannelID, testUserID)
							So(err, ShouldBeNil)

							expected.UserIsAuthorized = true
							appliedExemptions, _ := models.ExemptionsToProto(restriction.Exemptions)
							expected.AppliedExemptions = appliedExemptions

							authRes, authErr := testDeps.niohServer.GetUserAuthorization(ctx, req)
							So(authErr, ShouldBeNil)
							So(UserAuthRespResembles(authRes, expected), ShouldBeTrue)
						})

						Convey("and user is not moderator", func() {
							expected.UserIsAuthorized = false
							expected.AppliedExemptions = []*nioh.Exemption{}

							authRes, authErr := testDeps.niohServer.GetUserAuthorization(ctx, req)
							So(authErr, ShouldBeNil)
							So(UserAuthRespResembles(authRes, expected), ShouldBeTrue)
						})

						Convey("and user is anonymous", func() {
							req.UserId = ""
							expected.UserId = ""
							expected.UserIsAuthorized = false
							expected.AppliedExemptions = []*nioh.Exemption{}

							authRes, authErr := testDeps.niohServer.GetUserAuthorization(ctx, req)
							So(authErr, ShouldBeNil)
							So(UserAuthRespResembles(authRes, expected), ShouldBeTrue)
						})
					})

					Convey("with all access pass restriction", func() {
						// Create a channel sub restriction
						restriction := &models.RestrictionV2{
							ResourceKey:     "channel:" + testChannelID,
							RestrictionType: models.AllAccessPassRestrictionType,
							Exemptions: []*models.Exemption{
								{
									ExemptionType:   models.ProductExemptionType,
									ActiveStartDate: past,
									ActiveEndDate:   future,
									Keys:            []string{testProductID},
								},
							},
						}

						err := testDeps.daos.RestrictionDAO.SetRestriction(ctx, restriction)
						defer testDeps.daos.RestrictionDAO.DeleteRestriction(ctx, "channel:"+testChannelID)

						So(err, ShouldBeNil)

						var authRes *nioh.GetUserAuthorizationResponse
						var authErr error
						expected.ResourceIsRestricted = true
						expected.RestrictionType = models.RestrictionTypeToProto(restriction.RestrictionType)

						Convey("and user is not subscribed to the channel", func() {
							// Assume user is not already subscribed to the channel
							expected.UserIsAuthorized = false

							authRes, authErr = testDeps.niohServer.GetUserAuthorization(ctx, req)
						})

						Convey("and user is anonymous", func() {
							req.UserId = ""
							expected.UserId = ""
							expected.UserIsAuthorized = false

							authRes, authErr = testDeps.niohServer.GetUserAuthorization(ctx, req)
						})

						So(authErr, ShouldBeNil)
						So(UserAuthRespResembles(authRes, expected), ShouldBeTrue)
					})
				}
			})
		})

		Convey("Test GetUserAuthorizationsByResources", func() {
			testChannel1 := &nioh.Channel{Id: "186243532"}
			testChannel2 := &nioh.Channel{Id: "235068075"}
			testVod := &nioh.VOD{VideoId: "8484848484"}
			req := &nioh.GetUserAuthorizationsByResourcesRequest{
				UserId:   testUserID,
				Channels: []*nioh.Channel{testChannel1, testChannel2},
				Vods:     []*nioh.VOD{testVod},
			}
			expected := &nioh.GetUserAuthorizationsByResourcesResponse{
				UserId: testUserID,
				ChannelAuthorizations: []*nioh.UserAuthorizationForResource{
					{
						UserId:               testUserID,
						UserIsAuthorized:     true,
						ResourceIsRestricted: false,
						Resource: &nioh.UserAuthorizationForResource_Channel{
							Channel: testChannel1,
						},
						AppliedExemptions: []*nioh.Exemption{},
					},
					{
						UserId:               testUserID,
						UserIsAuthorized:     true,
						ResourceIsRestricted: false,
						Resource: &nioh.UserAuthorizationForResource_Channel{
							Channel: testChannel2,
						},
						AppliedExemptions: []*nioh.Exemption{},
					},
				},
				VodAuthorizations: []*nioh.UserAuthorizationForResource{
					{
						UserId:               testUserID,
						UserIsAuthorized:     true,
						ResourceIsRestricted: false,
						Resource: &nioh.UserAuthorizationForResource_Vod{
							Vod: testVod,
						},
						AppliedExemptions: []*nioh.Exemption{},
					},
				},
			}

			Convey("without restrictions", func() {
				Convey("with empty channel list", func() {
					req.Channels = []*nioh.Channel{}
					req.Vods = []*nioh.VOD{}
					expected.ChannelAuthorizations = []*nioh.UserAuthorizationForResource{}
					expected.VodAuthorizations = []*nioh.UserAuthorizationForResource{}
				})

				Convey("with channels that are not restricted", func() {})

				res, err := testDeps.niohServer.GetUserAuthorizationsByResources(ctx, req)
				So(err, ShouldBeNil)
				So(UserAuthByResRespResembles(res, expected), ShouldBeTrue)
			})

			if !isProd {
				Convey("with restrictions", func() {
					resourceKey1 := "channel:186243532"
					resourceKey2 := "channel:235068075"
					resourceKey3 := "vod:8484848484"
					restriction1 := &models.RestrictionV2{
						ResourceKey:     resourceKey1,
						RestrictionType: models.SubOnlyLiveRestrictionType,
					}
					restriction2 := &models.RestrictionV2{
						ResourceKey:     resourceKey2,
						RestrictionType: models.SubOnlyLiveRestrictionType,
					}
					restriction3 := &models.RestrictionV2{
						ResourceKey:     resourceKey3,
						RestrictionType: models.SubOnlyLiveRestrictionType,
					}
					expected.ChannelAuthorizations[0].ResourceIsRestricted = true
					expected.ChannelAuthorizations[1].ResourceIsRestricted = true
					expected.VodAuthorizations[0].ResourceIsRestricted = true
					expected.ChannelAuthorizations[0].RestrictionType = models.RestrictionTypeToProto(restriction1.RestrictionType)
					expected.ChannelAuthorizations[1].RestrictionType = models.RestrictionTypeToProto(restriction2.RestrictionType)
					expected.VodAuthorizations[0].RestrictionType = models.RestrictionTypeToProto(restriction3.RestrictionType)

					var resCheck func(res *nioh.GetUserAuthorizationsByResourcesResponse)

					Convey("with no exemption", func() {
						expected.ChannelAuthorizations[0].UserIsAuthorized = false
						expected.ChannelAuthorizations[1].UserIsAuthorized = false
						expected.VodAuthorizations[0].UserIsAuthorized = false

						resCheck = func(res *nioh.GetUserAuthorizationsByResourcesResponse) {
							So(UserAuthByResRespResembles(res, expected), ShouldBeTrue)
						}
					})

					Convey("with ALL exemption", func() {
						exempts := []*models.Exemption{
							{
								ExemptionType:   models.AllExemptionType,
								ActiveStartDate: time.Date(2019, time.January, 1, 0, 0, 0, 0, time.UTC),
								ActiveEndDate:   time.Date(3021, time.January, 1, 0, 0, 0, 0, time.UTC),
								Keys:            []string{},
								Actions:         []*models.ExemptionAction{},
							},
						}
						restriction1.Exemptions = exempts
						restriction2.Exemptions = exempts
						restriction3.Exemptions = exempts

						expected.ChannelAuthorizations[0].UserIsAuthorized = true
						expected.ChannelAuthorizations[1].UserIsAuthorized = true
						expected.VodAuthorizations[0].UserIsAuthorized = true
						protoExempts, err := models.ExemptionsToProto(exempts)
						So(err, ShouldBeNil)
						expected.ChannelAuthorizations[0].AppliedExemptions = protoExempts
						expected.ChannelAuthorizations[1].AppliedExemptions = protoExempts
						expected.VodAuthorizations[0].AppliedExemptions = protoExempts

						resCheck = func(res *nioh.GetUserAuthorizationsByResourcesResponse) {
							So(UserAuthByResRespResembles(res, expected), ShouldBeTrue)
						}
					})

					Convey("with PRODUCT exemption", func() {
						exempts := []*models.Exemption{
							{
								ExemptionType:   models.ProductExemptionType,
								ActiveStartDate: time.Date(2019, time.January, 1, 0, 0, 0, 0, time.UTC),
								ActiveEndDate:   time.Date(3021, time.January, 1, 0, 0, 0, 0, time.UTC),
								Keys:            []string{"186243532"},
								Actions:         []*models.ExemptionAction{},
							},
						}
						restriction1.Exemptions = exempts
						restriction2.Exemptions = exempts
						restriction3.Exemptions = exempts
						resCheck = func(res *nioh.GetUserAuthorizationsByResourcesResponse) {
							So(res.ChannelAuthorizations[0].ResourceIsRestricted, ShouldBeTrue)
							So(res.ChannelAuthorizations[1].ResourceIsRestricted, ShouldBeTrue)
							So(res.VodAuthorizations[0].ResourceIsRestricted, ShouldBeTrue)
						}
					})

					err := testDeps.daos.RestrictionDAO.SetRestriction(ctx, restriction1)
					defer testDeps.daos.RestrictionDAO.DeleteRestriction(ctx, resourceKey1)
					So(err, ShouldBeNil)
					err = testDeps.daos.RestrictionDAO.SetRestriction(ctx, restriction2)
					defer testDeps.daos.RestrictionDAO.DeleteRestriction(ctx, resourceKey2)
					So(err, ShouldBeNil)
					err = testDeps.daos.RestrictionDAO.SetRestriction(ctx, restriction3)
					defer testDeps.daos.RestrictionDAO.DeleteRestriction(ctx, resourceKey3)
					So(err, ShouldBeNil)

					res, err := testDeps.niohServer.GetUserAuthorizationsByResources(ctx, req)
					So(err, ShouldBeNil)
					resCheck(res)
				})
			}
		})
	})
}

func UserAuthRespResembles(actual *nioh.GetUserAuthorizationResponse, expected *nioh.GetUserAuthorizationResponse) bool {
	return actual.UserIsAuthorized == expected.UserIsAuthorized &&
		actual.UserId == expected.UserId &&
		actual.ResourceIsRestricted == expected.ResourceIsRestricted &&
		len(actual.AppliedExemptions) == len(expected.AppliedExemptions) &&
		actual.RestrictionType == expected.RestrictionType
}

func ResourceAuthResembles(actual *nioh.UserAuthorizationForResource, expected *nioh.UserAuthorizationForResource) bool {
	return actual.ResourceIsRestricted == expected.ResourceIsRestricted &&
		actual.UserIsAuthorized == expected.UserIsAuthorized &&
		actual.RestrictionType == expected.RestrictionType
}

func UserAuthByResRespResembles(actual *nioh.GetUserAuthorizationsByResourcesResponse, expected *nioh.GetUserAuthorizationsByResourcesResponse) bool {
	if len(actual.ChannelAuthorizations) != len(expected.ChannelAuthorizations) {
		return false
	}
	for index, channelAuth := range actual.ChannelAuthorizations {
		expectedChannelAuth := expected.ChannelAuthorizations[index]
		if !ResourceAuthResembles(channelAuth, expectedChannelAuth) {
			return false
		}
	}
	if len(actual.VodAuthorizations) != len(expected.VodAuthorizations) {
		return false
	}
	for index, vodAuth := range actual.VodAuthorizations {
		expectedVodAuth := expected.VodAuthorizations[index]
		if !ResourceAuthResembles(vodAuth, expectedVodAuth) {
			return false
		}
	}
	return true
}
