// +build integration

package integration

import (
	"context"
	"fmt"
	"testing"
	"time"

	"code.justin.tv/commerce/nioh/internal/models"
	"code.justin.tv/commerce/nioh/internal/testdata"
	"code.justin.tv/commerce/nioh/pkg/helpers"
	"code.justin.tv/commerce/nioh/rpc/nioh"
	. "github.com/smartystreets/goconvey/convey"
)

func TestContentAttributes(t *testing.T) {
	if !isProd {
		Convey("Test Content Attributes APIs", t, func() {
			channelID := "666777888"
			attributes := testdata.ProtoContentAttributes()
			for _, attribute := range attributes {
				attribute.OwnerChannelId = ""
				attribute.Id = ""
			}

			// clean up artifacts (if any) from previous (possibly failed) test runs
			So(cleanUpBeforeTests(channelID), ShouldBeNil)

			Convey("CreateContentAttributes", func() {
				req := &nioh.CreateContentAttributesRequest{
					ChannelId:         channelID,
					ContentAttributes: attributes,
				}

				Convey("with valid content attributes", func() {
					requestedAt := time.Now()
					res, err := testDeps.niohServer.CreateContentAttributes(context.Background(), req)
					defer cleanUpCreatedContentAttributes(channelID, res.SucceededCreates)

					So(err, ShouldBeNil)
					So(res, ShouldNotBeNil)
					So(res.SucceededCreates, ShouldHaveLength, len(attributes))
					So(res.FailedCreates, ShouldHaveLength, 0)

					for i, created := range res.SucceededCreates {
						So(created.Id, ShouldNotBeEmpty)
						So(created.OwnerChannelId, ShouldNotBeEmpty)

						So(created.AttributeKey, ShouldEqual, attributes[i].AttributeKey)
						So(created.AttributeName, ShouldEqual, attributes[i].AttributeName)
						So(created.Value, ShouldEqual, attributes[i].Value)
						So(created.ValueShortname, ShouldEqual, attributes[i].ValueShortname)
						So(created.ParentAttributeKey, ShouldEqual, attributes[i].ParentAttributeKey)
						So(created.ParentAttributeId, ShouldEqual, attributes[i].ParentAttributeId)
						So(created.ChildAttributeIds, ShouldEqual, attributes[i].ChildAttributeIds)
						So(created.ImageUrl, ShouldEqual, attributes[i].ImageUrl)

						So(created.CreatedAt.Seconds, ShouldBeGreaterThanOrEqualTo, requestedAt.Second())
						So(created.UpdatedAt.Seconds, ShouldBeGreaterThanOrEqualTo, requestedAt.Second())
					}
				})

				Convey("with invalid content attributes", func() {
					So(len(attributes), ShouldBeGreaterThanOrEqualTo, 5)
					attributes[0].Id = "123"             // new content attribute should not have an ID before it is created server-side
					attributes[1].OwnerChannelId = "555" // same as above
					attributes[2].AttributeKey = ""      // key must not be empty
					attributes[3].AttributeName = ""     // name must not be empty
					attributes[4].Value = ""             // value must not be empty

					res, err := testDeps.niohServer.CreateContentAttributes(context.Background(), req)
					defer cleanUpCreatedContentAttributes(channelID, res.SucceededCreates)

					So(err, ShouldBeNil)
					So(res, ShouldNotBeNil)
					So(res.SucceededCreates, ShouldHaveLength, len(attributes)-5)
					So(res.FailedCreates, ShouldHaveLength, 5)
				})

				Convey("with invalid requests", func() {
					Convey("empty Channel ID", func() {
						req.ChannelId = ""
					})

					Convey("no content attribute", func() {
						req.ContentAttributes = []*nioh.ContentAttribute{}
					})

					_, err := testDeps.niohServer.CreateContentAttributes(context.Background(), req)
					So(err, ShouldNotBeNil)
				})
			})

			Convey("GetContentAttributesForChannel", func() {
				createReq := &nioh.CreateContentAttributesRequest{
					ChannelId:         channelID,
					ContentAttributes: attributes,
				}
				createRes, err := testDeps.niohServer.CreateContentAttributes(context.Background(), createReq)
				So(err, ShouldBeNil)
				defer cleanUpCreatedContentAttributes(channelID, createRes.SucceededCreates)

				req := &nioh.GetContentAttributesForChannelRequest{
					ChannelId: channelID,
				}

				Convey("with valid requests", func() {
					res, err := testDeps.niohServer.GetContentAttributesForChannel(context.Background(), req)
					So(err, ShouldBeNil)
					So(res, ShouldNotBeNil)
					So(res.ContentAttributes, ShouldHaveLength, len(attributes))
				})

				Convey("with invalid requests", func() {
					Convey("empty Channel ID", func() {
						req.ChannelId = ""
						_, err := testDeps.niohServer.GetContentAttributesForChannel(context.Background(), req)
						So(err, ShouldNotBeNil)
					})
				})
			})

			Convey("DeleteContentAttributes", func() {
				createReq := &nioh.CreateContentAttributesRequest{
					ChannelId:         channelID,
					ContentAttributes: attributes,
				}
				createRes, err := testDeps.niohServer.CreateContentAttributes(context.Background(), createReq)
				So(err, ShouldBeNil)
				defer cleanUpCreatedContentAttributes(channelID, createRes.SucceededCreates)

				req := &nioh.DeleteContentAttributesRequest{
					ChannelId:         channelID,
					ContentAttributes: createRes.SucceededCreates,
				}

				Convey("with valid request", func() {
					res, err := testDeps.niohServer.DeleteContentAttributes(context.Background(), req)
					So(err, ShouldBeNil)
					So(res, ShouldNotBeNil)
					So(res.SucceededDeletes, ShouldHaveLength, len(attributes))
					So(res.FailedDeletes, ShouldHaveLength, 0)

					getReq := &nioh.GetContentAttributesForChannelRequest{
						ChannelId: channelID,
					}
					getRes, err := testDeps.niohServer.GetContentAttributesForChannel(context.Background(), getReq)
					So(err, ShouldBeNil)
					So(getRes.ContentAttributes, ShouldHaveLength, 0)
				})

				Convey("with invalid requests", func() {
					Convey("empty Channel ID", func() {
						req.ChannelId = ""
					})

					Convey("no content attribute", func() {
						req.ContentAttributes = []*nioh.ContentAttribute{}
					})

					_, err := testDeps.niohServer.DeleteContentAttributes(context.Background(), req)
					So(err, ShouldNotBeNil)
				})
			})

			Convey("UpdateContentAttributes", func() {
				createReq := &nioh.CreateContentAttributesRequest{
					ChannelId:         channelID,
					ContentAttributes: attributes,
				}
				createRes, err := testDeps.niohServer.CreateContentAttributes(context.Background(), createReq)
				So(err, ShouldBeNil)
				defer cleanUpCreatedContentAttributes(channelID, createRes.SucceededCreates)

				Convey("with valid request", func() {
					So(len(createRes.SucceededCreates), ShouldBeGreaterThanOrEqualTo, 5)
					createdAttributes := createRes.SucceededCreates
					createdAttributes[0].Value = "new value"
					createdAttributes[1].ValueShortname = "new shortname"
					createdAttributes[2].ParentAttributeKey = "new parent key"
					createdAttributes[2].ParentAttributeId = "999000999"
					createdAttributes[2].ChildAttributeIds = []string{"1"}
					createdAttributes[3].ImageUrl = fmt.Sprintf("%s/image", testdata.ContentAttributeImageURLPrefix)

					req := &nioh.UpdateContentAttributesRequest{
						ChannelId:         channelID,
						ContentAttributes: createdAttributes,
					}
					res, err := testDeps.niohServer.UpdateContentAttributes(context.Background(), req)

					So(err, ShouldBeNil)
					So(res, ShouldNotBeNil)
					So(res.SucceededUpdates, ShouldHaveLength, len(createdAttributes))

					So(res.SucceededUpdates[0].Value, ShouldEqual, "new value")
					So(res.SucceededUpdates[1].ValueShortname, ShouldEqual, "new shortname")
					So(res.SucceededUpdates[2].ParentAttributeKey, ShouldEqual, "new parent key")
					So(res.SucceededUpdates[2].ParentAttributeId, ShouldEqual, "999000999")
					So(res.SucceededUpdates[2].ChildAttributeIds, ShouldResemble, []string{"1"})
					So(res.SucceededUpdates[3].ImageUrl, ShouldEqual, fmt.Sprintf("%s/image", testdata.ContentAttributeImageURLPrefix))
					So(res.FailedUpdates, ShouldHaveLength, 0)
				})

				Convey("with invalid requests", func() {
					req := &nioh.UpdateContentAttributesRequest{
						ChannelId:         channelID,
						ContentAttributes: createRes.SucceededCreates,
					}

					Convey("empty channel ID", func() {
						req.ChannelId = ""
					})

					Convey("no content attributes", func() {
						req.ContentAttributes = []*nioh.ContentAttribute{}
					})

					_, err := testDeps.niohServer.UpdateContentAttributes(context.Background(), req)
					So(err, ShouldNotBeNil)
				})
			})
		})

		SkipConvey("Test Content Attribute Image Upload APIs", t, func() {
			channelID := "777888999"

			Convey("with invalid request", func() {
				req := &nioh.CreateContentAttributeImageUploadConfigRequest{
					ChannelId: "",
				}

				_, err := testDeps.niohServer.CreateContentAttributeImageUploadConfig(context.Background(), req)
				So(err, ShouldNotBeNil)
			})

			Convey("with valid request", func() {
				req := &nioh.CreateContentAttributeImageUploadConfigRequest{
					ChannelId: channelID,
				}

				resp, err := testDeps.niohServer.CreateContentAttributeImageUploadConfig(context.Background(), req)
				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
				So(resp.ChannelId, ShouldEqual, channelID)
				So(resp.UploadUrl, ShouldNotBeEmpty)
				So(resp.UploadId, ShouldNotBeEmpty)
				So(resp.ImageUrl, ShouldNotBeEmpty)

				// TODO: probably worth uploading an image to the upload URL and verify that the image is accessible.
			})
		})

		Convey("Test Computed Content Attribtues", t, func() {
			team := testdata.ProtoContentAttribute("", helpers.TeamKey, "LA Gladiators", "GLA")
			player := testdata.ProtoContentAttribute("", helpers.PlayerKey, "Surefour", "")
			role := testdata.ProtoContentAttribute("", helpers.RoleKey, "DPS", "")

			// clear IDs on new content attributes
			team.OwnerChannelId = ""
			player.OwnerChannelId = ""
			role.OwnerChannelId = ""

			expectedNumOfContentAttributes := 6 // team, player, role + 3 computed content attributes

			createReq := &nioh.CreateContentAttributesRequest{
				ChannelId:         testChannelID,
				ContentAttributes: []*nioh.ContentAttribute{team, player, role},
			}

			// create content attributes that will be used for computed content attributes
			creatResp, err := testDeps.niohServer.CreateContentAttributes(context.Background(), createReq)
			So(err, ShouldBeNil)
			createdAttributes := creatResp.SucceededCreates
			So(createdAttributes, ShouldHaveLength, 3)
			defer cleanUpCreatedContentAttributes(testChannelID, createdAttributes)

			// create a new test chanlet
			createChanletReq := &nioh.CreateChanletRequest{
				ChannelId: testChannelID,
			}
			createChanletResp, err := testDeps.niohServer.CreateChanlet(context.Background(), createChanletReq)
			So(err, ShouldBeNil)

			// Make chanlets enabled so the API returns chanlet data.
			_, err = testDeps.niohServer.UpdateOwnerChanletAttributes(context.Background(), &nioh.UpdateOwnerChanletAttributesRequest{
				UserId:         testChannelID,
				OwnerChanletId: testChannelID,
				Attributes: &nioh.OwnerChanletAttributes{
					ChanletsEnabled: true,
				},
			})
			So(err, ShouldBeNil)

			defer testDeps.daos.ChanletDAO.DeleteChanlet(context.Background(), createChanletResp.Chanlet.OwnerChannelId, createChanletResp.Chanlet.ChanletId)
			createdChanlet := createChanletResp.Chanlet

			// put content attributes on the test chanlet
			updateChanletReq := &nioh.UpdateChanletContentAttributesRequest{
				OwnerChannelId: testChannelID,
				ChanletId:      createdChanlet.ChanletId,
				ContentAttributeIds: []string{
					createdAttributes[0].Id,
					createdAttributes[1].Id,
					createdAttributes[2].Id,
				},
			}
			updateChanletResp, err := testDeps.niohServer.UpdateChanletContentAttributes(context.Background(), updateChanletReq)
			So(err, ShouldBeNil)
			updatedChanlet := updateChanletResp.Chanlet
			So(updatedChanlet.ContentAttributes, ShouldHaveLength, expectedNumOfContentAttributes)

			// check that the chanlet response includes computed content attributes after the update
			displayTitle := getAttributeByKey(updatedChanlet.ContentAttributes, helpers.DisplayTitleKey)
			So(displayTitle, ShouldNotBeNil)
			So(displayTitle.Value, ShouldEqual, "Surefour")

			icon := getAttributeByKey(updatedChanlet.ContentAttributes, helpers.IconKey)
			So(icon, ShouldNotBeNil)
			So(icon.ImageUrl, ShouldEqual, icon.ImageUrl)

			secondaryIcon := getAttributeByKey(updatedChanlet.ContentAttributes, helpers.SecondaryIconKey)
			So(secondaryIcon, ShouldNotBeNil)
			So(secondaryIcon.ImageUrl, ShouldEqual, role.ImageUrl)

			// check that GetChanlets API also returns the computed content attributes
			getChanletsReq := &nioh.GetChanletsRequest{
				UserId:    testChannelID,
				ChannelId: testChannelID,
			}
			getChanletsResp, err := testDeps.niohServer.GetChanlets(context.Background(), getChanletsReq)
			So(err, ShouldBeNil)

			var testChanlet *nioh.Chanlet
			for _, chanlet := range getChanletsResp.Chanlets {
				if chanlet.ChanletId == createdChanlet.ChanletId {
					testChanlet = chanlet
					break
				}
			}
			So(testChanlet, ShouldNotBeNil)
			So(testChanlet.ContentAttributes, ShouldHaveLength, expectedNumOfContentAttributes)
		})
	}
}

func cleanUpBeforeTests(channelID string) error {
	attributes, err := testDeps.daos.ContentAttributeDAO.GetContentAttributesForChannel(context.Background(), channelID)
	if err != nil {
		return err
	}
	if len(attributes) == 0 {
		return nil
	}
	return cleanUpCreatedContentAttributes(channelID, models.ContentAttributesToProtos(context.Background(), attributes))
}

func cleanUpCreatedContentAttributes(channelID string, attributes []*nioh.ContentAttribute) error {
	ids := make([]string, 0, len(attributes))
	for _, attribute := range attributes {
		ids = append(ids, attribute.Id)
	}
	_, err := testDeps.daos.ContentAttributeDAO.DeleteContentAttributesByIDs(context.Background(), channelID, ids)
	return err
}

func getAttributeByKey(attributes []*nioh.ContentAttribute, key string) *nioh.ContentAttribute {
	for _, attribute := range attributes {
		if attribute.AttributeKey == key {
			return attribute
		}
	}

	return nil
}
