// +build integration

package integration

import (
	"context"
	"testing"

	"code.justin.tv/commerce/nioh/internal/models"
	"code.justin.tv/commerce/nioh/internal/testdata"
	"code.justin.tv/commerce/nioh/rpc/nioh"
	. "github.com/smartystreets/goconvey/convey"
)

func TestChanlet(t *testing.T) {
	Convey("Test Chanlet APIs", t, func() {
		ctx := context.Background()

		Convey("Test CreateChanlets", func() {
			Convey("with invalid request", func() {
				req := &nioh.CreateChanletRequest{
					ChannelId: "",
				}
				res, err := testDeps.niohServer.CreateChanlet(context.Background(), req)
				So(res, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})

			// This test generates garbage data. Best not run in production.
			if !isProd {
				Convey("with valid request", func() {
					req := &nioh.CreateChanletRequest{
						ChannelId: testChannelID,
					}
					res, err := testDeps.niohServer.CreateChanlet(context.Background(), req)
					So(err, ShouldBeNil)
					So(res, ShouldNotBeNil)
					So(res.Chanlet.OwnerChannelId, ShouldEqual, testChannelID)

					createdChanlet := res.Chanlet

					// Make chanlets enabled so the API returns chanlet data.
					_, err = testDeps.niohServer.UpdateOwnerChanletAttributes(context.Background(), &nioh.UpdateOwnerChanletAttributesRequest{
						UserId:         testChannelID,
						OwnerChanletId: testChannelID,
						Attributes: &nioh.OwnerChanletAttributes{
							ChanletsEnabled: true,
						},
					})
					So(err, ShouldBeNil)

					// Clean up by deleting the created chanlet
					defer testDeps.daos.ChanletDAO.DeleteChanlet(ctx, createdChanlet.OwnerChannelId, createdChanlet.ChanletId)

					Convey("Test GetChanlets with valid request", func() {
						req := &nioh.GetChanletsRequest{
							UserId:    testChannelID,
							ChannelId: testChannelID,
						}
						res, err := testDeps.niohServer.GetChanlets(context.Background(), req)
						So(err, ShouldBeNil)
						So(res, ShouldNotBeNil)
						So(len(res.Chanlets), ShouldEqual, 2)
						So(res.Chanlets[0].OwnerChannelId, ShouldEqual, testChannelID)
						So(res.Chanlets[0].ChanletId, ShouldEqual, testChannelID)
						So(res.Chanlets[0].IsPublicChannel, ShouldBeTrue)
						So(res.Chanlets[1].OwnerChannelId, ShouldEqual, testChannelID)
						So(res.Chanlets[1].ChanletId, ShouldEqual, createdChanlet.ChanletId)
						So(res.Chanlets[1].IsPublicChannel, ShouldBeFalse)
					})

					Convey("Test IsHiddenChanlet with valid request", func() {
						req := &nioh.IsHiddenChanletRequest{
							ChannelId: createdChanlet.ChanletId,
						}
						res, err := testDeps.niohServer.IsHiddenChanlet(context.Background(), req)
						So(err, ShouldBeNil)
						So(res, ShouldNotBeNil)
						So(res.IsHiddenChanlet, ShouldBeTrue)
						So(res.OwnerChannelId, ShouldEqual, testChannelID)
						So(res.ChannelId, ShouldEqual, createdChanlet.ChanletId)
					})

					Convey("Test BulkIsHiddenChanlet with valid request", func() {
						req := &nioh.BulkIsHiddenChanletRequest{
							ChannelIds: []string{createdChanlet.ChanletId},
						}
						res, err := testDeps.niohServer.BulkIsHiddenChanlet(context.Background(), req)
						So(err, ShouldBeNil)
						So(res, ShouldNotBeNil)
						So(len(res.Results), ShouldEqual, 1)
						result := res.Results[0]
						So(result.IsHiddenChanlet, ShouldBeTrue)
						So(result.OwnerChannelId, ShouldEqual, testChannelID)
						So(result.ChannelId, ShouldEqual, createdChanlet.ChanletId)
					})

					Convey("Test ArchiveChanlet with valid request", func() {
						req := &nioh.ArchiveChanletRequest{
							ChanletId: createdChanlet.ChanletId,
							UserId:    createdChanlet.OwnerChannelId,
						}
						res, err := testDeps.niohServer.ArchiveChanlet(context.Background(), req)
						So(err, ShouldBeNil)
						So(res, ShouldNotBeNil)

						chanlet, err := testDeps.daos.ChanletDAO.GetChanlet(ctx, createdChanlet.OwnerChannelId, createdChanlet.ChanletId)
						So(err, ShouldBeNil)
						So(chanlet.ArchivedDate.IsZero(), ShouldBeFalse)
					})
				})
			}
		})

		Convey("Test GetChanlets with invalid request", func() {
			req := &nioh.GetChanletsRequest{
				ChannelId: "",
			}
			res, err := testDeps.niohServer.GetChanlets(context.Background(), req)
			So(res, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("Test IsHiddenChanlet with invalid request", func() {
			req := &nioh.IsHiddenChanletRequest{
				ChannelId: "",
			}
			res, err := testDeps.niohServer.IsHiddenChanlet(context.Background(), req)
			So(res, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("Test BulkIsHiddenChanlet with invalid request", func() {
			req := &nioh.BulkIsHiddenChanletRequest{
				ChannelIds: []string{},
			}
			res, err := testDeps.niohServer.BulkIsHiddenChanlet(context.Background(), req)
			So(res, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("Test ArchiveChanlet with invalid request", func() {
			req := &nioh.ArchiveChanletRequest{
				ChanletId: "",
			}
			res, err := testDeps.niohServer.ArchiveChanlet(context.Background(), req)
			So(res, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("Test UpdateChanletContentAttributes", func() {
			Convey("with empty owner channel ID", func() {
				req := &nioh.UpdateChanletContentAttributesRequest{
					OwnerChannelId:      "",
					ChanletId:           "123",
					ContentAttributeIds: []string{},
				}
				_, err := testDeps.niohServer.UpdateChanletContentAttributes(context.Background(), req)
				So(err, ShouldNotBeNil)
			})

			Convey("with empty chanlet ID", func() {
				req := &nioh.UpdateChanletContentAttributesRequest{
					OwnerChannelId:      "123",
					ChanletId:           "",
					ContentAttributeIds: []string{},
				}
				_, err := testDeps.niohServer.UpdateChanletContentAttributes(context.Background(), req)
				So(err, ShouldNotBeNil)
			})

			if !isProd {
				Convey("with valid request", func() {
					req := &nioh.CreateChanletRequest{
						ChannelId: testChannelID,
					}
					res, err := testDeps.niohServer.CreateChanlet(context.Background(), req)
					So(err, ShouldBeNil)
					So(res, ShouldNotBeNil)

					createdChanlet := res.Chanlet

					// Clean up by deleting the created chanlet
					defer testDeps.daos.ChanletDAO.DeleteChanlet(ctx, createdChanlet.OwnerChannelId, createdChanlet.ChanletId)

					// construct new content attributes
					attributes := testdata.ProtoContentAttributes()
					// clear IDs
					for _, attribute := range attributes {
						attribute.OwnerChannelId = ""
						attribute.Id = ""
					}

					// create new content attributes
					attributesReq := &nioh.CreateContentAttributesRequest{
						ChannelId:         createdChanlet.OwnerChannelId,
						ContentAttributes: attributes,
					}
					attributesRes, _ := testDeps.niohServer.CreateContentAttributes(context.Background(), attributesReq)
					createdAttributes := attributesRes.SucceededCreates

					// clean up content attributes after test
					deleteAttributeReq := &nioh.DeleteContentAttributesRequest{
						ChannelId:         createdChanlet.OwnerChannelId,
						ContentAttributes: createdAttributes,
					}
					defer testDeps.niohServer.DeleteContentAttributes(context.Background(), deleteAttributeReq)

					Convey("add new content attributes to chanlet", func() {
						updateReq := &nioh.UpdateChanletContentAttributesRequest{
							OwnerChannelId:      createdChanlet.OwnerChannelId,
							ChanletId:           createdChanlet.ChanletId,
							ContentAttributeIds: []string{createdAttributes[0].Id, createdAttributes[1].Id},
						}
						updateRes, err := testDeps.niohServer.UpdateChanletContentAttributes(context.Background(), updateReq)
						So(err, ShouldBeNil)
						So(updateRes, ShouldNotBeNil)
						So(updateRes.Chanlet.ContentAttributes, ShouldHaveLength, 2)
						So(updateRes.Chanlet.ContentAttributes[0].Id, ShouldEqual, createdAttributes[0].Id)
						So(updateRes.Chanlet.ContentAttributes[1].Id, ShouldEqual, createdAttributes[1].Id)
					})

					Convey("update content attributes on chanlet", func() {
						updateReq := &nioh.UpdateChanletContentAttributesRequest{
							OwnerChannelId:      createdChanlet.OwnerChannelId,
							ChanletId:           createdChanlet.ChanletId,
							ContentAttributeIds: []string{createdAttributes[0].Id, createdAttributes[2].Id},
						}
						updateRes, err := testDeps.niohServer.UpdateChanletContentAttributes(context.Background(), updateReq)
						So(err, ShouldBeNil)
						So(updateRes, ShouldNotBeNil)
						So(updateRes.Chanlet.ContentAttributes, ShouldHaveLength, 2)
						So(updateRes.Chanlet.ContentAttributes[0].Id, ShouldEqual, createdAttributes[0].Id)
						So(updateRes.Chanlet.ContentAttributes[1].Id, ShouldEqual, createdAttributes[2].Id)
					})

					Convey("remove content attributes on chanlet", func() {
						updateReq := &nioh.UpdateChanletContentAttributesRequest{
							OwnerChannelId:      createdChanlet.OwnerChannelId,
							ChanletId:           createdChanlet.ChanletId,
							ContentAttributeIds: []string{},
						}
						updateRes, err := testDeps.niohServer.UpdateChanletContentAttributes(context.Background(), updateReq)
						So(err, ShouldBeNil)
						So(updateRes, ShouldNotBeNil)
						So(updateRes.Chanlet.ContentAttributes, ShouldHaveLength, 0)
					})

					Convey("prevent duplicated content attributes from attaching", func() {
						updateReq := &nioh.UpdateChanletContentAttributesRequest{
							OwnerChannelId:      createdChanlet.OwnerChannelId,
							ChanletId:           createdChanlet.ChanletId,
							ContentAttributeIds: []string{createdAttributes[0].Id, createdAttributes[0].Id, createdAttributes[1].Id},
						}
						updateRes, err := testDeps.niohServer.UpdateChanletContentAttributes(context.Background(), updateReq)
						So(err, ShouldBeNil)
						So(updateRes, ShouldNotBeNil)
						So(updateRes.Chanlet.ContentAttributes, ShouldHaveLength, 2)
					})
				})
			}
		})

		Convey("Test GetChanletStreamKey", func() {
			Convey("with invalid request", func() {
				req := &nioh.GetChanletStreamKeyRequest{
					OwnerChannelId: "",
				}
				res, err := testDeps.niohServer.GetChanletStreamKey(context.Background(), req)
				So(res, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})

			Convey("with non-existent chanlet", func() {
				req := &nioh.GetChanletStreamKeyRequest{
					OwnerChannelId: testChannelID,
					ChanletId:      testChannelID,
				}
				testDeps.daos.ChanletDAO.DeleteChanlet(ctx, testChannelID, testChannelID)

				res, err := testDeps.niohServer.GetChanletStreamKey(context.Background(), req)
				So(res, ShouldNotBeNil)
				So(res.StreamKey, ShouldBeEmpty)
				So(err, ShouldBeNil)
			})

			// This test generates garbage data. Best not run in production.
			if !isProd {
				// Video service in beta is extremely unreliable. Skipping this test for now.
				SkipConvey("with valid request", func() {
					req := &nioh.GetChanletStreamKeyRequest{
						OwnerChannelId: testChannelID,
						ChanletId:      testChannelID,
					}

					err := testDeps.daos.ChanletDAO.AddChanlet(ctx, &models.Chanlet{
						OwnerChannelID: testChannelID,
						ChanletID:      testChannelID,
					})
					So(err, ShouldBeNil)

					// Clean up by deleting the created chanlet
					defer testDeps.daos.ChanletDAO.DeleteChanlet(ctx, testChannelID, testChannelID)

					expectedStreamKey, err := testDeps.niohClients.ChannelProperties.GetStreamKey(ctx, testChannelID)
					So(err, ShouldBeNil)

					res, err := testDeps.niohServer.GetChanletStreamKey(context.Background(), req)

					So(err, ShouldBeNil)
					So(res, ShouldNotBeNil)
					So(res.StreamKey, ShouldEqual, expectedStreamKey)

					Convey("when chanlet is public and is NOT the owner chanlet", func() {
						req := &nioh.GetChanletStreamKeyRequest{
							OwnerChannelId: testChannelID,
							ChanletId:      testUserID,
						}

						err := testDeps.daos.ChanletDAO.AddChanlet(ctx, &models.Chanlet{
							OwnerChannelID:  testChannelID,
							ChanletID:       testUserID,
							IsPublicChannel: true,
						})
						So(err, ShouldBeNil)

						// Clean up by deleting the created chanlet
						defer testDeps.daos.ChanletDAO.DeleteChanlet(ctx, testChannelID, testUserID)

						res, err := testDeps.niohServer.GetChanletStreamKey(context.Background(), req)
						So(err, ShouldBeNil)
						So(res, ShouldNotBeNil)
						So(res.StreamKey, ShouldBeEmpty)
					})
				})
			}
		})
	})
}
