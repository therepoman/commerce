// +build integration

package integration

import (
	"context"
	"testing"

	"code.justin.tv/commerce/nioh/internal/models"
	"code.justin.tv/commerce/nioh/rpc/nioh"

	. "github.com/smartystreets/goconvey/convey"
)

func TestChannelRelation(t *testing.T) {
	Convey("Test Channel Relation APIs", t, func() {
		Convey("Test CreateChannelRelation", func() {
			Convey("with invalid request", func() {
				req := &nioh.CreateChannelRelationRequest{}
				res, err := testDeps.niohServer.CreateChannelRelation(context.Background(), req)
				So(res, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})

			// This test generates garbage data. Best not run in production.
			if !isProd {
				Convey("with valid request", func() {
					req := &nioh.CreateChannelRelationRequest{
						ChannelId:        testBorrowerChannelID,
						RelatedChannelId: testChannelID,
					}

					// Clean up by deleting the created chanlet
					defer testDeps.daos.ChannelRelationDAO.DeleteChannelRelation(context.Background(), testBorrowerChannelID, testChannelID)

					res, err := testDeps.niohServer.CreateChannelRelation(context.Background(), req)
					So(err, ShouldBeNil)
					So(res, ShouldNotBeNil)
					So(res.ChannelRelation.ChannelId, ShouldEqual, testBorrowerChannelID)
					So(res.ChannelRelation.RelatedChannelId, ShouldEqual, testChannelID)
					So(res.ChannelRelation.Relation, ShouldEqual, string(models.RelationBorrower))

					// Make chanlets enabled so the API returns chanlet data.
					_, err = testDeps.niohServer.UpdateOwnerChanletAttributes(context.Background(), &nioh.UpdateOwnerChanletAttributesRequest{
						UserId:         testBorrowerChannelID,
						OwnerChanletId: testBorrowerChannelID,
						Attributes: &nioh.OwnerChanletAttributes{
							ChanletsEnabled: true,
						},
					})
					So(err, ShouldBeNil)

					// An owner chanlet should be created for the borrower channel
					chanletsRes, err := testDeps.niohServer.GetChanlets(context.Background(), &nioh.GetChanletsRequest{
						UserId:    testBorrowerChannelID,
						ChannelId: testBorrowerChannelID,
					})
					So(chanletsRes.Chanlets[0].OwnerChannelId, ShouldEqual, testBorrowerChannelID)
					So(chanletsRes.Chanlets[0].ChanletId, ShouldEqual, testBorrowerChannelID)

					Convey("Test GetChannelRelations", func() {
						req := &nioh.GetChannelRelationsRequest{}

						Convey("with channel id", func() {
							req.Id = &nioh.GetChannelRelationsRequest_ChannelId{ChannelId: testBorrowerChannelID}
						})

						Convey("with related channel id", func() {
							req.Id = &nioh.GetChannelRelationsRequest_RelatedChannelId{RelatedChannelId: testChannelID}
						})

						res, err := testDeps.niohServer.GetChannelRelations(context.Background(), req)
						So(err, ShouldBeNil)
						So(res, ShouldNotBeNil)
						So(len(res.ChannelRelations), ShouldEqual, 1)
						result := res.ChannelRelations[0]
						So(result.ChannelId, ShouldEqual, testBorrowerChannelID)
						So(result.RelatedChannelId, ShouldEqual, testChannelID)
						So(result.Relation, ShouldEqual, string(models.RelationBorrower))
					})

					Convey("Test DeleteChannelRelation", func() {
						req := &nioh.DeleteChannelRelationRequest{
							ChannelId:        testBorrowerChannelID,
							RelatedChannelId: testChannelID,
						}

						_, err := testDeps.niohServer.DeleteChannelRelation(context.Background(), req)
						So(err, ShouldBeNil)
						result, err := testDeps.daos.ChannelRelationDAO.GetChannelRelationsByChannelID(context.Background(), testBorrowerChannelID)
						So(err, ShouldBeNil)
						So(result, ShouldBeEmpty)
					})
				})
			}
		})

		Convey("Test DeleteChannelRelation with invalid request", func() {
			req := &nioh.DeleteChannelRelationRequest{}
			res, err := testDeps.niohServer.DeleteChannelRelation(context.Background(), req)
			So(res, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("Test GetChannelRelations with invalid request", func() {
			req := &nioh.GetChannelRelationsRequest{}
			res, err := testDeps.niohServer.GetChannelRelations(context.Background(), req)
			So(res, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})
	})
}
