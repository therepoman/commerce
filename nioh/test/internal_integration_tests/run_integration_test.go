// +build integration

package integration

import (
	"context"
	"fmt"
	"os"
	"testing"
	"time"

	"github.com/aws/aws-sdk-go/service/sqs"

	"code.justin.tv/commerce/nioh/internal/orch/autotag"

	"code.justin.tv/commerce/config"
	log "code.justin.tv/commerce/logrus"
	niohConfig "code.justin.tv/commerce/nioh/config"
	"code.justin.tv/commerce/nioh/internal/backend"
	"code.justin.tv/commerce/nioh/internal/clients"
	"code.justin.tv/commerce/nioh/internal/models"
	"code.justin.tv/commerce/nioh/internal/server"
	"code.justin.tv/commerce/nioh/internal/testdata"
	"code.justin.tv/commerce/nioh/pkg/dynamodb/dao"
	"code.justin.tv/commerce/nioh/pkg/errorlogger"
	"code.justin.tv/commerce/nioh/pkg/exemptions"
	"code.justin.tv/commerce/nioh/pkg/helpers"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/cactus/go-statsd-client/statsd"
)

const (
	hystrixMetricsSampleRate = 1.0

	namespace    = "nioh"
	organization = "commerce"

	// File paths overrides for file-based configurations
	localFilePath = "/src/code.justin.tv/%s/%s/config/%s.json"

	// Test Data - DO NOT CHANGE THESE. We delete data based on these test IDs as they are regarded as "garbage"
	testUserID            = "234092104" // testfortuna
	testChannelID         = "235068075" // testfortuna2
	testProductID         = "523455"
	testBorrowerChannelID = "235068073"
	testVodID             = "1234567"
)

var (
	isProd bool
)

type testDependencyBundle struct {
	config       *niohConfig.Configuration
	niohServer   *server.Server
	daos         *dao.DAOs
	niohClients  *clients.Clients
	niohBackend  backend.Backend
	clock        models.IClock
	stats        statsd.Statter
	awsSession   *session.Session
	autotagger   autotag.Autotagger
	snsPublisher *testSNSPublisher
}

var testDeps testDependencyBundle = testDependencyBundle{}

func TestMain(m *testing.M) {
	env := config.GetEnvironment()
	canonicalEnv := niohConfig.GetCanonicalEnv()
	isProd = env == niohConfig.ProductionEnv || env == niohConfig.CanaryEnv

	log.SetLevel(log.DebugLevel)
	log.SetFormatter(&log.TextFormatter{FullTimestamp: true, TimestampFormat: "02/Jan/2006:15:04:05 -0700"})

	log.Infof("Running Nioh Integration Test in environment: %s", env)

	cfg := niohConfig.Configuration{}

	configName := env
	if env == "dev" {
		configName = niohConfig.DevelopmentEnv
	}

	localPathOverride := os.Getenv("GOPATH") + fmt.Sprintf(localFilePath, organization, namespace, configName)

	var err error

	if err = config.LoadConfigForEnvironment(&cfg, &config.Options{
		RepoOrganization:  organization,
		RepoService:       namespace,
		Environment:       env,
		LocalPathOverride: localPathOverride,
	}); err != nil {
		log.Fatal(err)
	}
	cfg.ParseAllowlist()

	// Set ContentAttributeImageURLPrefix manually for integration tests
	cfg.ContentAttributeImageURLPrefix = testdata.ContentAttributeImageURLPrefix

	testDeps.config = &cfg

	// Use stubbed Statter
	statter := &statsd.NoopClient{}

	testDeps.stats = statter

	cm := helpers.MakeCircuitManager(statter)

	dynamicCfg, err := niohConfig.NewDynamicConfiguration(canonicalEnv, statter)
	if err != nil {
		log.WithError(err).Fatal("DynamicConfiguration failed to initialize during deployment")
	}
	defer dynamicCfg.Shutdown()

	awsSession := session.Must(session.NewSession(aws.NewConfig().WithRegion(cfg.AWSRegion)))
	niohClients, err := clients.SetupClients(&cfg, statter, cm, dynamicCfg, awsSession)
	if err != nil {
		log.Fatal(err)
	}

	testDeps.niohClients = niohClients

	errorLogger := configureErrorLogger(statter)

	daos, err := dao.SetupDAOs(cfg, cm, false, statter)
	if err != nil {
		log.Fatal(err)
	}

	testDeps.daos = daos

	clock := &models.RealClock{}

	testDeps.clock = clock

	exemptionChecker := &exemptions.ExemptionChecker{
		Clients: niohClients,
		Clock:   clock,
	}

	testDeps.awsSession = awsSession

	testDeps.snsPublisher = &testSNSPublisher{}

	autotagger := autotag.NewAutotagger(&autotag.AutotaggerInput{
		Config:       testDeps.config,
		SNSPublisher: testDeps.snsPublisher,
		Clock:        testDeps.clock,
		Stats:        testDeps.stats,
	})
	testDeps.autotagger = autotagger

	niohBackend := backend.NewBackend(&backend.NewBackendInput{
		Config:           testDeps.config,
		Clients:          testDeps.niohClients,
		Stats:            testDeps.stats,
		Errorlogger:      errorLogger,
		DAOs:             testDeps.daos,
		ExemptionChecker: exemptionChecker,
		Clock:            testDeps.clock,
		Autotagger:       autotagger,
	})

	testDeps.niohBackend = niohBackend

	serverConfig := server.ServerConfig{
		Backend:       testDeps.niohBackend,
		Clients:       testDeps.niohClients,
		Config:        testDeps.config,
		DynamicConfig: dynamicCfg,
		Errorlogger:   errorLogger,
		Stats:         testDeps.stats,
		S2SName:       fmt.Sprintf("nioh-%s", canonicalEnv),
		UseDAX:        false,
	}

	niohServer, err := server.NewServer(serverConfig)
	if err != nil {
		log.Fatalf("Failed to initialize the server: %v", err)
	}

	testDeps.niohServer = niohServer

	// Run Tests on server
	testSuiteResult := m.Run()
	if !isProd { // We don't even generate garbage db on prod
		cleanUpGarbageDB()
	}
	log.Info("Integration test complete.")

	os.Exit(testSuiteResult)
}

func configureErrorLogger(statter statsd.Statter) errorlogger.ErrorLogger {
	env := config.GetEnvironment()
	return errorlogger.NewErrorLogger("", env, statter)
}

func cleanUpGarbageDB() {
	// Test Chanlet Data
	testChanlets, err := testDeps.daos.ChanletDAO.GetChanlets(context.Background(), testChannelID)
	if err != nil {
		log.Error(err)
	} else {
		for _, chanlet := range testChanlets {
			testDeps.daos.ChanletDAO.DeleteChanlet(context.Background(), chanlet.OwnerChannelID, chanlet.ChanletID)
		}
	}

	// Test Subs Data
	err = testDeps.niohClients.Subscriptions.CancelTestSubscription(context.Background(), testUserID, testProductID)
	if err != nil {
		log.Error(err)
	}
}

func setupTestStreamEventsSQS(s *session.Session) (queueName string, queueUrl string, teardown func(), err error) {
	sqsClient := sqs.New(s)

	queueName = fmt.Sprintf("__integration-stream-events-%d", time.Now().Unix())

	out, err := sqsClient.CreateQueue(&sqs.CreateQueueInput{
		Attributes: nil,
		QueueName:  &queueName,
	})
	if err != nil {
		return
	}

	teardown = func() {
		_, err := sqsClient.DeleteQueue(&sqs.DeleteQueueInput{
			QueueUrl: out.QueueUrl,
		})
		if err != nil {
			log.WithField("QueueURL", *out.QueueUrl).WithError(err).Error("Failed to clean up integration stream event SQS queue")
		}
	}

	queueUrl = *out.QueueUrl

	return
}
