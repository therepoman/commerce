// +build integration

package integration

import (
	"context"
	"testing"

	"code.justin.tv/commerce/nioh/internal/models"
	"code.justin.tv/commerce/nioh/internal/testdata"
	"code.justin.tv/commerce/nioh/rpc/nioh"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/twitchtv/twirp"
)

func TestGetRestrictionsByResources(t *testing.T) {
	Convey("Test ResourcesRestriction APIs", t, func() {
		Convey("Test GetRestrictionsByResources", func() {
			req := &nioh.GetRestrictionsByResourcesRequest{
				Resources: []*nioh.RestrictionResource{
					{
						Id:   testChannelID,
						Type: nioh.ResourceType_LIVE,
					},
					{
						Id:   testVodID,
						Type: nioh.ResourceType_VIDEO,
					},
				},
			}

			Convey("with invalid request", func() {
				req.Resources[0].Id = ""
				res, err := testDeps.niohServer.GetRestrictionsByResources(context.Background(), req)
				So(res, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})

			// This test generates garbage data. Best not run in production.
			if !isProd {
				restriction := testdata.Restriction(req.Resources[0].Id)
				restriction.Exemptions = testdata.ExemptionsStaffProduct()
				channel := models.Channel{ID: req.Resources[0].Id}

				Convey("with valid request", func() {
					testDeps.daos.RestrictionDAO.SetRestriction(context.Background(), restriction)
					defer testDeps.daos.RestrictionDAO.DeleteRestriction(context.Background(), channel.GetResourceKey())

					res, err := testDeps.niohServer.GetRestrictionsByResources(context.Background(), req)
					So(err, ShouldBeNil)
					So(res, ShouldNotBeNil)
					So(len(res.Restrictions), ShouldEqual, 2)
					So(res.Restrictions[0].Restriction.RestrictionType, ShouldEqual, nioh.Restriction_SUB_ONLY_LIVE)
					So(res.Restrictions[1].Restriction, ShouldBeNil)
				})
			}
		})

		Convey("Test SetResourceRestriction", func() {
			req := &nioh.SetResourceRestrictionRequest{
				UserId:  testChannelID,
				OwnerId: testChannelID,
				Resource: &nioh.RestrictionResource{
					Id: "",
				},
				Restriction: &nioh.Restriction{
					Id:              "test-restriction",
					RestrictionType: nioh.Restriction_SUB_ONLY_LIVE,
				},
			}

			orgMemberAccessOnlyReq := &nioh.SetResourceRestrictionRequest{
				UserId:  testChannelID,
				OwnerId: testChannelID,
				Resource: &nioh.RestrictionResource{
					Id: "",
				},
				Restriction: &nioh.Restriction{
					Id:              "test-restriction-org-member_only",
					RestrictionType: nioh.Restriction_ORGANIZATION_ACCESS_ONLY,
				},
			}

			Convey("with VOD resource", func() {
				req.Resource = &nioh.RestrictionResource{
					Type: nioh.ResourceType_VIDEO,
					Id:   "test-video-id",
				}

				orgMemberAccessOnlyReq.Resource = &nioh.RestrictionResource{
					Type: nioh.ResourceType_VIDEO,
					Id:   "test-video-id",
				}

				Convey("with invalid request", func() {
					req.UserId = ""
					res, err := testDeps.niohServer.SetResourceRestriction(context.Background(), req)
					So(res, ShouldBeNil)
					So(err, ShouldNotBeNil)
				})

				if !isProd {
					Convey("with valid sub-only live request", func() {
						res, err := testDeps.niohServer.SetResourceRestriction(context.Background(), req)

						vod := models.VOD{ID: req.Resource.Id, OwnerID: testChannelID}
						defer testDeps.daos.RestrictionDAO.DeleteRestriction(context.Background(), vod.GetResourceKey())

						So(err, ShouldNotBeNil) // TODO: Test Channel has no products associated with it, which fails setting the restriction. Swap when a ticket product is associated.
						So(res, ShouldBeNil)
						// So(res.Restriction.Exemptions, ShouldHaveLength, 3) // Should have staff, site-admin, and product exemptions.
					})

					Convey("with valid organization_access_only request", func() {
						res, err := testDeps.niohServer.SetResourceRestriction(context.Background(), orgMemberAccessOnlyReq)

						vod := models.VOD{ID: req.Resource.Id, OwnerID: testChannelID}
						defer testDeps.daos.RestrictionDAO.DeleteRestriction(context.Background(), vod.GetResourceKey())

						So(err, ShouldNotBeNil) // TODO: Test Channel has no products associated with it, which fails setting the restriction. Swap when a ticket product is associated.
						So(res, ShouldBeNil)
						So(err.Error(), ShouldNotEqual, twirp.InvalidArgumentError("UserID", "user cannot create/update restriction for a given resource").Error()) // TODO: Test Channel has no products associated with it, which fails setting the restriction. Swap when a ticket product is associated.
						So(err.Error(), ShouldNotEqual, twirp.InvalidArgumentError(orgMemberAccessOnlyReq.GetUserId(), "UserId is not authorized").Error())
					})

					Convey("with valid organization_access_only request, requester is in SetResourceRestrictionAdmin allow list", func() {
						reqWithRequesterAsAdmin := &nioh.SetResourceRestrictionRequest{
							UserId:   "249677451",
							OwnerId:  testChannelID,
							Resource: orgMemberAccessOnlyReq.Resource,
							Restriction: &nioh.Restriction{
								Id:              "test-restriction-org-member_only",
								RestrictionType: nioh.Restriction_ORGANIZATION_ACCESS_ONLY,
							},
						}
						res, err := testDeps.niohServer.SetResourceRestriction(context.Background(), reqWithRequesterAsAdmin)

						vod := models.VOD{ID: req.Resource.Id, OwnerID: testChannelID}
						defer testDeps.daos.RestrictionDAO.DeleteRestriction(context.Background(), vod.GetResourceKey())

						So(err, ShouldNotBeNil) // TODO: Test Channel has no products associated with it, which fails setting the restriction. Swap when a ticket product is associated.
						So(err.Error(), ShouldNotEqual, twirp.InvalidArgumentError(reqWithRequesterAsAdmin.GetUserId(), "UserId is not authorized").Error())
						So(res, ShouldBeNil)
					})

					Convey("with valid organization_access_only request, requester is not in SetResourceRestrictionAdmin allow list", func() {
						reqWithRequesterAsNonAdmin := &nioh.SetResourceRestrictionRequest{
							UserId:   "438125537",
							OwnerId:  testChannelID,
							Resource: orgMemberAccessOnlyReq.Resource,
							Restriction: &nioh.Restriction{
								Id:              "test-restriction-org-member_only",
								RestrictionType: nioh.Restriction_ORGANIZATION_ACCESS_ONLY,
							},
						}
						res, err := testDeps.niohServer.SetResourceRestriction(context.Background(), reqWithRequesterAsNonAdmin)

						So(err.Error(), ShouldEqual, twirp.InvalidArgumentError("UserID", "user cannot create/update restriction for a given resource").Error()) // TODO: Test Channel has no products associated with it, which fails setting the restriction. Swap when a ticket product is associated.
						So(res, ShouldBeNil)
					})
				}

			})

			Convey("with Channel resource", func() {
				req.Resource = &nioh.RestrictionResource{
					Type: nioh.ResourceType_LIVE,
					Id:   testChannelID,
				}

				orgMemberAccessOnlyReq.Resource = &nioh.RestrictionResource{
					Type: nioh.ResourceType_LIVE,
					Id:   testChannelID,
				}

				Convey("with invalid request", func() {
					req.UserId = ""
					res, err := testDeps.niohServer.SetResourceRestriction(context.Background(), req)
					So(res, ShouldBeNil)
					So(err, ShouldNotBeNil)
				})

				if !isProd {
					Convey("with valid sub-only live request", func() {
						res, err := testDeps.niohServer.SetResourceRestriction(context.Background(), req)

						channel := models.Channel{ID: req.Resource.Id}
						defer testDeps.daos.RestrictionDAO.DeleteRestriction(context.Background(), channel.GetResourceKey())

						So(err, ShouldNotBeNil) // TODO: Test Channel has no products associated with it, which fails setting the restriction. Swap when a ticket product is associated.
						So(res, ShouldBeNil)
						// So(res.Restriction.Exemptions, ShouldHaveLength, 3) // Should have staff, site-admin, and product exemptions.
					})

					Convey("with valid organization_member_only request", func() {
						res, err := testDeps.niohServer.SetResourceRestriction(context.Background(), orgMemberAccessOnlyReq)

						channel := models.Channel{ID: req.Resource.Id}
						defer testDeps.daos.RestrictionDAO.DeleteRestriction(context.Background(), channel.GetResourceKey())

						So(err.Error(), ShouldNotEqual, twirp.InvalidArgumentError("UserID", "user cannot create/update restriction for a given resource").Error()) // TODO: Test Channel has no products associated with it, which fails setting the restriction. Swap when a ticket product is associated.
						So(err.Error(), ShouldNotEqual, twirp.InvalidArgumentError(orgMemberAccessOnlyReq.GetUserId(), "UserId is not authorized").Error())
						So(err, ShouldNotBeNil) // TODO: Test Channel has no products associated with it, which fails setting the restriction. Swap when a ticket product is associated.
						So(res, ShouldBeNil)
						// So(res.Restriction.Exemptions, ShouldHaveLength, 3) // Should have staff, site-admin, and product exemptions.
					})

					Convey("with valid organization_access_only request, requester is in SetResourceRestrictionAdmin allow list", func() {
						reqWithRequesterAsAdmin := &nioh.SetResourceRestrictionRequest{
							UserId:  "249677451",
							OwnerId: testChannelID,
							Resource: &nioh.RestrictionResource{
								Type: nioh.ResourceType_LIVE,
								Id:   testChannelID,
							},
							Restriction: &nioh.Restriction{
								Id:              "test-restriction-org-member_only",
								RestrictionType: nioh.Restriction_ORGANIZATION_ACCESS_ONLY,
							},
						}
						res, err := testDeps.niohServer.SetResourceRestriction(context.Background(), reqWithRequesterAsAdmin)

						channel := models.Channel{ID: req.Resource.Id}
						defer testDeps.daos.RestrictionDAO.DeleteRestriction(context.Background(), channel.GetResourceKey())

						So(err, ShouldNotBeNil) // TODO: Test Channel has no products associated with it, which fails setting the restriction. Swap when a ticket product is associated.
						So(err.Error(), ShouldNotEqual, twirp.InvalidArgumentError("UserID", "UserId is not authorized").Error())
						So(res, ShouldBeNil)
					})

					Convey("with valid organization_access_only request, requester is not in SetResourceRestrictionAdmin allow list", func() {
						reqWithRequesterAsNonAdmin := &nioh.SetResourceRestrictionRequest{
							UserId:  "438125537",
							OwnerId: testChannelID,
							Resource: &nioh.RestrictionResource{
								Type: nioh.ResourceType_LIVE,
								Id:   testChannelID,
							},
							Restriction: &nioh.Restriction{
								Id:              "test-restriction-org-member_only",
								RestrictionType: nioh.Restriction_ORGANIZATION_ACCESS_ONLY,
							},
						}
						res, err := testDeps.niohServer.SetResourceRestriction(context.Background(), reqWithRequesterAsNonAdmin)

						So(err.Error(), ShouldEqual, twirp.InvalidArgumentError("UserID", "user cannot create/update restriction for a given resource").Error()) // TODO: Test Channel has no products associated with it, which fails setting the restriction. Swap when a ticket product is associated.
						So(res, ShouldBeNil)
					})
				}

			})
		})
	})
}
