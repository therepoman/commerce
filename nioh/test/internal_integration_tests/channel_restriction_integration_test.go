// +build integration

package integration

import (
	"context"
	"testing"

	"code.justin.tv/commerce/nioh/internal/models"
	"code.justin.tv/commerce/nioh/internal/testdata"
	"code.justin.tv/commerce/nioh/rpc/nioh"

	. "github.com/smartystreets/goconvey/convey"
)

func TestChannelRestriction(t *testing.T) {
	Convey("Test ChannelRestriction APIs", t, func() {
		Convey("Test GetChannelRestriction(s)", func() {
			req := &nioh.GetChannelRestrictionRequest{
				ChannelId: testChannelID,
			}

			Convey("with invalid request", func() {
				req.ChannelId = ""
				res, err := testDeps.niohServer.GetChannelRestriction(context.Background(), req)
				So(res, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})

			// This test generates garbage data. Best not run in production.
			if !isProd {
				restriction := testdata.Restriction(req.ChannelId)
				restriction.Exemptions = testdata.ExemptionsStaffProduct()
				channel := models.Channel{ID: req.ChannelId}

				Convey("with valid request", func() {
					testDeps.daos.RestrictionDAO.SetRestriction(context.Background(), restriction)
					defer testDeps.daos.RestrictionDAO.DeleteRestriction(context.Background(), channel.GetResourceKey())
					res, err := testDeps.niohServer.GetChannelRestriction(context.Background(), req)

					So(err, ShouldBeNil)
					So(res, ShouldNotBeNil)
					So(res.Restriction.RestrictionType, ShouldEqual, nioh.Restriction_SUB_ONLY_LIVE)

					Convey("batch call", func() {
						testChannelID2 := "12345678"
						batchReq := &nioh.GetChannelRestrictionsRequest{
							ChannelIds: []string{testChannelID, testChannelID2},
						}

						res, err := testDeps.niohServer.GetChannelRestrictions(context.Background(), batchReq)
						So(err, ShouldBeNil)
						So(res, ShouldNotBeNil)
						So(len(res.ChannelRestrictions), ShouldEqual, 2)
						So(res.ChannelRestrictions[0].Restriction.RestrictionType, ShouldEqual, nioh.Restriction_SUB_ONLY_LIVE)
						So(res.ChannelRestrictions[1].Restriction, ShouldBeNil)
					})
				})
			}
		})
	})
}
