// +build integration

package integration

import (
	"context"
	"fmt"
	"sync"
	"testing"
	"time"

	"code.justin.tv/commerce/nioh/internal/models"
	"code.justin.tv/commerce/nioh/internal/testdata"

	"code.justin.tv/commerce/logrus"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/request"
	"github.com/aws/aws-sdk-go/service/sfn"
	"github.com/aws/aws-sdk-go/service/sns"
	"github.com/aws/aws-sdk-go/service/sqs"

	"code.justin.tv/commerce/nioh/internal/orch/streamevent"
)

type sfnExecutorWithCleanup struct {
	sfn           *sfn.SFN
	executionArns []string
	lock          sync.Mutex
}

func (s *sfnExecutorWithCleanup) StartExecutionWithContext(ctx aws.Context, input *sfn.StartExecutionInput, opts ...request.Option) (*sfn.StartExecutionOutput, error) {
	out, err := s.sfn.StartExecutionWithContext(ctx, input, opts...)
	if err != nil {
		return out, err
	}
	s.lock.Lock()
	s.executionArns = append(s.executionArns, *out.ExecutionArn)
	s.lock.Unlock()
	return out, err
}

func (s *sfnExecutorWithCleanup) cleanup() {
	var failedArns []string
	cause := "cleaning up after integration test :)"

	s.lock.Lock()
	defer s.lock.Unlock()

	for _, arn := range s.executionArns {
		_, err := s.sfn.StopExecution(&sfn.StopExecutionInput{
			Cause:        &cause,
			ExecutionArn: &arn,
		})
		if err != nil {
			failedArns = append(failedArns, arn)
		}
	}

	s.executionArns = nil

	logrus.WithField("failedArns", failedArns).Info("Cleaned up sfn executions in integration test. Some may have failed, please see field `failedArns`.")
}

func (s *sfnExecutorWithCleanup) countExecutions() int {
	s.lock.Lock()
	defer s.lock.Unlock()
	return len(s.executionArns)
}

// testSNSPublisher is a simple fake SNS publisher that only keeps track of count of publishing
type testSNSPublisher struct {
	count int
	lock  sync.Mutex
}

func (t *testSNSPublisher) PublishWithContext(ctx aws.Context, input *sns.PublishInput, opts ...request.Option) (*sns.PublishOutput, error) {
	t.lock.Lock()
	t.count++
	t.lock.Unlock()
	return nil, nil
}

func (t *testSNSPublisher) reset() {
	t.lock.Lock()
	t.count = 0
	t.lock.Unlock()
}

func (t *testSNSPublisher) countPublishes() int {
	t.lock.Lock()
	defer t.lock.Unlock()
	return t.count
}

func TestStreamEventsIntegration(t *testing.T) {
	if isProd {
		return
	}

	queueName, queueUrl, teardownSQS, err := setupTestStreamEventsSQS(testDeps.awsSession)
	if err != nil {
		t.Fatalf("failed to setup test stream events SQS: %v", err)
	}
	defer teardownSQS()

	// make copy of config and overwrite with integration queue URL
	config := *testDeps.config
	config.StreamEventsSQSName = queueName

	sfnClient := sfn.New(testDeps.awsSession)
	testSfnExecutor := &sfnExecutorWithCleanup{
		sfn: sfnClient,
	}
	defer testSfnExecutor.cleanup()

	offlineCleanupScheduler := streamevent.NewOfflineCleanupScheduler(&streamevent.OfflineCleanupSchedulerInput{
		Config:      testDeps.config,
		SFNExecutor: testSfnExecutor,
		Clock:       testDeps.clock,
		Backend:     testDeps.niohBackend,
		Clients:     testDeps.niohClients,
	})

	onlineAutotagHandler := streamevent.NewOnlineAutotagHandler(testDeps.autotagger, testDeps.niohBackend)

	streamEventProcessor, err := streamevent.NewProcessor(
		&config, sqs.New(testDeps.awsSession), testDeps.stats, []streamevent.Handler{offlineCleanupScheduler, onlineAutotagHandler})
	if err != nil {
		t.Fatalf("failed to setup stream event processor in integration test: %v", err)
	}

	err = streamEventProcessor.Start(context.Background())
	if err != nil {
		t.Fatalf("failed to start stream event processor in integration test: %v", err)
	}
	defer func() {
		ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
		defer cancel()
		streamEventProcessor.Shutdown(ctx)
	}()

	sqsClient := sqs.New(testDeps.awsSession)

	t.Run("stream ready event with restriction triggers autotag", func(t *testing.T) {
		testDeps.snsPublisher.reset()

		body := `{"event":"ready","channel_id":235068075}`

		restriction := testdata.Restriction("235068075")
		restriction.Exemptions = testdata.ExemptionsStaffProduct()
		channel := models.Channel{ID: "235068075"}
		testDeps.daos.RestrictionDAO.SetRestriction(context.Background(), restriction)
		defer testDeps.daos.RestrictionDAO.DeleteRestriction(context.Background(), channel.GetResourceKey())

		_, err := sqsClient.SendMessage(&sqs.SendMessageInput{
			MessageBody: &body,
			QueueUrl:    &queueUrl,
		})
		if err != nil {
			t.Fatalf("failed to send integration SQS stream event: %v", err)
		}

		// Wait for message to process
		<-time.After(1 * time.Second)

		if testDeps.snsPublisher.countPublishes() != 1 {
			t.Error("sns publisher did not publish as expected")
		}
	})

	t.Run("stream ready event without restriction does not trigger autotag", func(t *testing.T) {
		testDeps.snsPublisher.reset()

		body := `{"event":"ready","channel_id":235068075}`
		channel := models.Channel{ID: "235068075"}
		testDeps.daos.RestrictionDAO.DeleteRestriction(context.Background(), channel.GetResourceKey())

		_, err := sqsClient.SendMessage(&sqs.SendMessageInput{
			MessageBody: &body,
			QueueUrl:    &queueUrl,
		})
		if err != nil {
			t.Fatalf("failed to send integration SQS stream event: %v", err)
		}

		// Wait for message to process
		<-time.After(1 * time.Second)

		if testDeps.snsPublisher.countPublishes() != 0 {
			t.Error("sns publisher published but was not expected to")
		}
	})

	t.Run("stream ready event does not trigger an execution", func(t *testing.T) {
		defer testSfnExecutor.cleanup()

		body := `{"event":"ready","channel_id":235068075}`
		_, err := sqsClient.SendMessage(&sqs.SendMessageInput{
			MessageBody: &body,
			QueueUrl:    &queueUrl,
		})
		if err != nil {
			t.Fatalf("failed to send integration SQS stream event: %v", err)
		}

		// Wait for message to process
		<-time.After(1 * time.Second)

		if testSfnExecutor.countExecutions() != 0 {
			t.Errorf("expected no sfn executions, observed %d", len(testSfnExecutor.executionArns))
		}
	})

	t.Run("stream down event for unrestricted channel does not trigger an execution", func(t *testing.T) {
		defer testSfnExecutor.cleanup()

		body := `{"event":"down","channel_id":235068075}`
		_, err := sqsClient.SendMessage(&sqs.SendMessageInput{
			MessageBody: &body,
			QueueUrl:    &queueUrl,
		})
		if err != nil {
			t.Fatalf("failed to send integration SQS stream event: %v", err)
		}

		// Wait for message to process
		<-time.After(1 * time.Second)

		if testSfnExecutor.countExecutions() != 0 {
			t.Errorf("expected no sfn executions, observed %d", len(testSfnExecutor.executionArns))
		}
	})

	t.Run("stream down event for restricted channel triggers an execution", func(t *testing.T) {
		defer testSfnExecutor.cleanup()

		err := testDeps.daos.RestrictionDAO.SetRestriction(context.Background(), testdata.Restriction(testChannelID))
		if err != nil {
			t.Fatalf("failed to setup restriction: %v", err)
		}
		defer func() {
			err := testDeps.daos.RestrictionDAO.DeleteRestriction(context.Background(), fmt.Sprintf("channel:%s", testChannelID))
			if err != nil {
				t.Logf("failed to clean up restriction: %v", err)
			}
		}()

		body := `{"event":"down","channel_id":235068075}`
		_, err = sqsClient.SendMessage(&sqs.SendMessageInput{
			MessageBody: &body,
			QueueUrl:    &queueUrl,
		})
		if err != nil {
			t.Fatalf("failed to send integration SQS stream event: %v", err)
		}

		// Wait for message to process
		<-time.After(1 * time.Second)

		if testSfnExecutor.countExecutions() != 1 {
			t.Errorf("expected 1 sfn execution, observed %d", len(testSfnExecutor.executionArns))
		}
	})
}
