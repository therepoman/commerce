// +build integration

package integration

import (
	"context"
	"testing"

	"code.justin.tv/commerce/nioh/rpc/nioh"
	. "github.com/smartystreets/goconvey/convey"
)

func TestPermission(t *testing.T) {
	Convey("Test Permissions API", t, func() {
		ctx := context.Background()

		Convey("Test GetPermissions", func() {
			req := &nioh.GetPermissionsRequest{
				ChannelId: testChannelID,
				UserId:    testChannelID,
			}

			Convey("with invalid request", func() {
				req.ChannelId = ""
				res, err := testDeps.niohServer.GetPermissions(ctx, req)
				So(res, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})

			Convey("with user not being the channel owner", func() {
				req.UserId = testUserID
				res, err := testDeps.niohServer.GetPermissions(ctx, req)
				So(err, ShouldBeNil)
				So(res, ShouldNotBeNil)
				So(res.Permissions, ShouldBeEmpty)
			})

			if !isProd {
				// The tests below use dynamic config files in s3.
				// Some entries in beta/dev are set up specifically for these tests.
				// You can find these files in the repo at config/beta(development)/usergates.toml

				Convey("with user who has permissions", func() {
					res, err := testDeps.niohServer.GetPermissions(ctx, req)
					So(err, ShouldBeNil)
					So(res, ShouldNotBeNil)
					So(res.UserId, ShouldEqual, testChannelID)
					So(res.ChannelId, ShouldEqual, testChannelID)
					So(len(res.Permissions), ShouldEqual, 2)
					So(res.Permissions[0], ShouldEqual, nioh.GetPermissionsResponse_MULTIVIEW_ADMIN)
					So(res.Permissions[1], ShouldEqual, nioh.GetPermissionsResponse_SET_CHANNEL_RESTRICTION)
				})

				Convey("with user who does not have permissions", func() {
					req.ChannelId = "testUserID" // Invalid test user id should have nothing in Pepper
					req.UserId = "testUserID"
					res, err := testDeps.niohServer.GetPermissions(ctx, req)
					So(err, ShouldBeNil)
					So(res, ShouldNotBeNil)
					So(res.Permissions, ShouldBeEmpty)
				})
			}
		})
	})
}
