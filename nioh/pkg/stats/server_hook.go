package stats

import (
	"context"
	"fmt"
	"regexp"
	"time"

	"code.justin.tv/commerce/nioh/config"

	log "code.justin.tv/commerce/logrus"
	"github.com/twitchtv/twirp"
	twirp_statsd "github.com/twitchtv/twirp/hooks/statsd"
)

// Define APIs that need sampling here.
var statsdSamplingRateMap = map[string]float32{
	"production-GetUserAuthorization":             0.05, // Expected load is around 4k-8k
	"production-GetUserAuthorizationsByResources": 0.04, // Expected load is 6k-12k
	"production-GetRestrictionsByResources":       0.05, // Expected load is 3k-6k
}

var reqStartTimestampKey = new(int) // Store a private address to an integer to be used as a context key. Value has no significance.

const (
	statsTwirpTotalRequests  string = "twirp.total.requests"
	statsTwirpTotalResponses string = "twirp.total.responses"
	statsTwirpAllMethods     string = "twirp.all_methods.response"

	defaultStatsdSamplingRate float32 = 1.0
)

// NewStatsdServerHooks creates a twirp server hook that sends stats data to statsd.
// Custom version of this source: https://github.com/twitchtv/twirp/blob/master/hooks/statsd/statsd.go
func NewStatsdServerHooks(stats twirp_statsd.Statter) *twirp.ServerHooks {
	hooks := &twirp.ServerHooks{}
	// RequestReceived: inc twirp.total.req_recv
	hooks.RequestReceived = func(ctx context.Context) (context.Context, error) {
		ctx = markReqStart(ctx)
		sendInc(stats, statsTwirpTotalRequests, 1, 1.0)
		return ctx, nil
	}

	// RequestRouted: inc twirp.<method>.req_recv
	hooks.RequestRouted = func(ctx context.Context) (context.Context, error) {
		method, ok := twirp.MethodName(ctx)
		if !ok {
			return ctx, nil
		}
		sampleRate := getSampleRate(method)
		sendInc(stats, "twirp."+sanitize(method)+".requests", 1, sampleRate)
		return ctx, nil
	}

	hooks.ResponseSent = func(ctx context.Context) {
		var (
			start  time.Time
			method string
			status string

			haveStart  bool
			haveMethod bool
			haveStatus bool
		)

		start, haveStart = getReqStart(ctx)
		method, haveMethod = twirp.MethodName(ctx)
		status, haveStatus = twirp.StatusCode(ctx)

		method = sanitize(method)
		status = sanitize(status)

		sampleRate := getSampleRate(method)

		sendInc(stats, statsTwirpTotalResponses, 1, sampleRate)

		if haveMethod {
			sendInc(stats, "twirp."+method+".responses", 1, sampleRate)
		}
		if haveStatus {
			sendInc(stats, "twirp.status_codes.total."+status, 1, sampleRate)
		}
		if haveMethod && haveStatus {
			sendInc(stats, "twirp.status_codes."+method+"."+status, 1, sampleRate)
		}

		if haveStart {
			dur := time.Since(start)
			sendTimeDuration(stats, statsTwirpAllMethods, dur, sampleRate)

			if haveMethod {
				sendTimeDuration(stats, "twirp."+method+".response", dur, sampleRate)
			}
			if haveStatus {
				sendTimeDuration(stats, "twirp.status_codes.all_methods."+status, dur, sampleRate)
			}
			if haveMethod && haveStatus {
				sendTimeDuration(stats, "twirp.status_codes."+method+"."+status, dur, sampleRate)
			}
		}
	}
	return hooks
}

func markReqStart(ctx context.Context) context.Context {
	return context.WithValue(ctx, reqStartTimestampKey, time.Now())
}

func getReqStart(ctx context.Context) (time.Time, bool) {
	t, ok := ctx.Value(reqStartTimestampKey).(time.Time)
	return t, ok
}

func sanitize(s string) string {
	pattern := regexp.MustCompile("[^0-9A-Za-z]")
	return pattern.ReplaceAllString(s, "_")
}

func getSampleRate(method string) float32 {
	environment := config.GetCanonicalEnv()
	key := fmt.Sprintf("%s-%s", environment, method)
	if rate, ok := statsdSamplingRateMap[key]; ok {
		return rate
	}
	return defaultStatsdSamplingRate
}

func sendInc(stats twirp_statsd.Statter, stat string, count int64, sampleRate float32) {
	if err := stats.Inc(stat, count, sampleRate); err != nil {
		log.Debugf("Failed to send a increment metric %+v", err)
	}
}

func sendTimeDuration(stats twirp_statsd.Statter, stat string, duration time.Duration, sampleRate float32) {
	if err := stats.TimingDuration(stat, duration, sampleRate); err != nil {
		log.Debugf("Failed to send a time duration metric %+v", err)
	}
}
