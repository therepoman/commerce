package stats

import (
	"context"
	"testing"

	"code.justin.tv/commerce/nioh/mocks"
	. "github.com/smartystreets/goconvey/convey"
)

func TestSanitize(t *testing.T) {
	type test struct {
		in   string
		want string
	}
	cases := []test{
		{"", ""},
		{"SomeString", "SomeString"},
		{"with whitespace", "with_whitespace"},
		{"with.punctuation", "with_punctuation"},
		{"with世界unicode", "with__unicode"},
		{"with123numbers", "with123numbers"},
	}
	for _, c := range cases {
		have := sanitize(c.in)
		if have != c.want {
			t.Errorf("in=%q  have=%q  want=%q", c.in, have, c.want)
		}
	}
}

func TestServerHook(t *testing.T) {
	statter := new(mocks.Statter)
	hooks := NewStatsdServerHooks(statter)
	ctx := context.Background()

	Convey("Test hook", t, func() {
		Convey("hook executions", func() {
			statter.On("Inc", "twirp.total.responses", int64(1), float32(1.0)).Return(nil)
			statter.On("Inc", "twirp.total.requests", int64(1), float32(1.0)).Return(nil)

			hooks.RequestReceived(ctx)
			hooks.ResponseSent(ctx)
			statter.AssertNumberOfCalls(t, "Inc", 2)
		})
	})
}
