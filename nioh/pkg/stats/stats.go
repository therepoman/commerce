package stats

import (
	"time"

	"github.com/cactus/go-statsd-client/statsd"

	niohConfig "code.justin.tv/commerce/nioh/config"
	"code.justin.tv/commerce/splatter"
)

const (
	namespace          = "nioh"
	primaryEnvironment = "primary"

	twitchTelemetryFlush     = 30 * time.Second // Flush metrics every 30 seconds for TwitchTelemetry
	twitchTelemetryMinBuffer = 100000           // Define a min buffer size to ensure there is enough room for metrics while flushing
)

// SetupStats returns an initialized statter for Nioh
func SetupStats(cfg niohConfig.Configuration, env string) (statsd.Statter, error) {
	// With statsd service deprecated, just return the twitch telemetry shim statter.
	// Always return nil for error for now. We should look into migrating to the non-shim version when it becomes a thing.
	return setupTwitchTelemetryStatter(cfg, env), nil
}

// ConfigureHystrixMetricCollector registers and initializes statsd collector for hystrix
func ConfigureHystrixMetricCollector(cfg niohConfig.Configuration) {
	// TODO: find a way to output to CloudWatch.
}

func setupTwitchTelemetryStatter(cfg niohConfig.Configuration, env string) statsd.Statter {
	// Define a buffer as at least twitchTelemetryMinBuffer
	bufferSize := cfg.CloudwatchBufferSize
	if bufferSize < twitchTelemetryMinBuffer {
		bufferSize = twitchTelemetryMinBuffer
	}

	// Update the Stage and Substage depending on whether we're using a Canary
	telemetryStage := niohConfig.GetCanonicalEnv() // production
	telemetrySubstage := env                       // canary | primary
	if env == niohConfig.ProductionEnv {
		telemetrySubstage = primaryEnvironment
	}

	// Construct the TwitchTelemetry Config
	twitchTelemetryConfig := &splatter.BufferedTelemetryConfig{
		FlushPeriod:       twitchTelemetryFlush,
		BufferSize:        bufferSize,
		AggregationPeriod: time.Minute, // Place all metrics into 1 minute buckets
		ServiceName:       namespace,
		AWSRegion:         cfg.CloudwatchRegion,
		Stage:             telemetryStage,
		Substage:          telemetrySubstage,
		Prefix:            "", // No need for a prefix since service, stage, and substage are all dimensions
	}

	// Construct the buffered CloudWatch TwitchTelemetry sender, no need to filter any stats
	return splatter.NewBufferedTelemetryCloudWatchStatter(twitchTelemetryConfig, map[string]bool{})
}
