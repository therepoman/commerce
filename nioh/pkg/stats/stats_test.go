package stats_test

import (
	"testing"

	"code.justin.tv/commerce/nioh/internal/testdata"
	"code.justin.tv/commerce/nioh/pkg/stats"
	. "github.com/smartystreets/goconvey/convey"
)

func TestStats(t *testing.T) {
	// Most of this is testing that the basic setup calls in stats aren't invalid
	// or leading to immediate/fatal crashes.

	cfg := testdata.Config()

	Convey("TestSetupStats", t, func() {
		statter, err := stats.SetupStats(cfg, "canary")
		So(err, ShouldBeNil)
		So(statter, ShouldNotBeNil)
	})

	Convey("ConfigureHystrixMetricCollector", t, func() {
		stats.ConfigureHystrixMetricCollector(cfg)
	})
}
