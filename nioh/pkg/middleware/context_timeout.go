package middleware

import (
	"context"
	"net/http"
	"time"
)

const (
	defaultContextTimeout = 500 * time.Millisecond
)

var (
	contextTimeoutByURL = map[string]time.Duration{
		"/twirp/nioh.API/GetChannelRestriction":            250 * time.Millisecond, // This is just a DB look up. Should be fast.
		"/twirp/nioh.API/CreateContentAttributes":          2 * time.Second,
		"/twirp/nioh.API/DeleteContentAttributes":          2 * time.Second,
		"/twirp/nioh.API/UpdateContentAttributes":          2 * time.Second,
		"/twirp/nioh.API/UpdateChanletContentAttributes":   2 * time.Second,
		"/twirp/nioh.API/GetUserAuthorizationsByResources": 2 * time.Second,
		"/twirp/nioh.API/GetChanlets":                      2 * time.Second,
		"/twirp/nioh.API/GetRestrictionsByResources":       2 * time.Second,
	}
)

// ContextTimeout sets a context timeout on all requests
func ContextTimeout(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		// We can have a different timeout for each URL falling back to a default.
		contextTimeout, ok := contextTimeoutByURL[r.URL.String()]
		if !ok {
			contextTimeout = defaultContextTimeout
		}
		ctx, cancel := context.WithTimeout(r.Context(), contextTimeout)
		defer cancel()
		r = r.WithContext(ctx)
		h.ServeHTTP(w, r)
	})
}
