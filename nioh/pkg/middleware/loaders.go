package middleware

import (
	"net/http"

	"code.justin.tv/commerce/nioh/internal/clients"
	"code.justin.tv/commerce/nioh/pkg/loaders"
)

// HandlerLoaders ...
type HandlerLoaders struct {
	loaders loaders.Loaders
}

// NewHandlerLoaders ...
func NewHandlerLoaders(clients *clients.Clients) HandlerLoaders {
	return HandlerLoaders{
		loaders: loaders.New(clients),
	}
}

// Attach connects the current loaders collection to the request's context
func (h *HandlerLoaders) Attach(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := h.loaders.Attach(r.Context())
		r = r.WithContext(ctx)
		next.ServeHTTP(w, r)
	})
}
