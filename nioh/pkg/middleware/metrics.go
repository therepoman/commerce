package middleware

import (
	"net/http"

	"github.com/cactus/go-statsd-client/statsd"

	"github.com/felixge/httpsnoop"
)

// HandlerMetrics is responsible for logging service metrics.
// This middleware is intended to supplment twirp metrics, which will not show
// up until an occurence of a metric. Metrics logged here will be logged on
// every request, making them easier to alarm on.
type HandlerMetrics struct {
	stats statsd.Statter
}

// NewHandlerMetrics instantiates a new HandlerMetrics
func NewHandlerMetrics(stats statsd.Statter) *HandlerMetrics {
	return &HandlerMetrics{
		stats: stats,
	}
}

// Metrics logs latency metrics around all API handlers
func (m *HandlerMetrics) Metrics(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		snoopMetrics := httpsnoop.CaptureMetrics(next, w, r)

		if m.stats != nil {
			var errorValue int64
			if snoopMetrics.Code >= 500 {
				errorValue = 1
			}
			m.stats.Inc("Nioh.Errors", errorValue, 1.0)
		}
	})
}
