package errorlogger

import (
	"errors"
	"fmt"
	"log"
	"net/http"

	"github.com/heroku/rollbar"
)

// RequestError is for HTTP request errors.
func (e *Impl) RequestError(r *http.Request, err error) {
	rollbar.RequestError(rollbar.ERR, r, err)
}

// RequestPanic is for critical HTTP request errors.
func (e *Impl) RequestPanic(r *http.Request, p interface{}) {
	err := fmt.Errorf(fmt.Sprintf("panic: %v", p))
	rollbar.RequestError(rollbar.CRIT, r, err)
}

// LogFatalWithExtras provides the same functionality as LogFatal, but passes
// additional information in the map to rollbar
// Approximately Sev-1
func (e *Impl) LogFatalWithExtras(errorMsg string, errorStatName string, extras map[string]interface{}) {
	e.sendStatAndRollbarLogWithExtras(rollbar.CRIT, errorMsg, errorStatName, extras)
	log.Fatalf("FATAL: %s WITH EXTRAS: %v\n", errorMsg, extras)
}

// LogErrorWithExtras provides the same functionality as LogError, but passes
// additional information in the map to rollbar
// Approximately Sev-2
func (e *Impl) LogErrorWithExtras(errorMsg string, errorStatName string, extras map[string]interface{}) {
	e.sendStatAndRollbarLogWithExtras(rollbar.ERR, errorMsg, errorStatName, extras)
	log.Printf("ERROR: %s WITH EXTRAS: %v\n", errorMsg, extras)
}

// LogWarnWithExtras provides the same functionality as LogWarn, but passes
// additional information in the map to rollbar
// Approximately Sev-3 or Sev-4
func (e *Impl) LogWarnWithExtras(errorMsg string, errorStatName string, extras map[string]interface{}) {
	e.sendStatAndRollbarLogWithExtras(rollbar.WARN, errorMsg, errorStatName, extras)
	log.Printf("WARN: %s WITH EXTRAS: %v\n", errorMsg, extras)
}

// LogInfoWithExtras provides the same functionality as LogInfo, but passes
// additional information in the map to rollbar
// No severity mapping
func (e *Impl) LogInfoWithExtras(errorMsg string, errorStatName string, extras map[string]interface{}) {
	e.sendStatAndRollbarLogWithExtras(rollbar.INFO, errorMsg, errorStatName, extras)
	log.Printf("INFO: %s WITH EXTRAS: %v\n", errorMsg, extras)
}

// LogDebugWithExtras provides the same functionality as LogDebug, but passes
// additional information in the map to rollbar
// No severity mapping
func (e *Impl) LogDebugWithExtras(errorMsg string, errorStatName string, extras map[string]interface{}) {
	e.sendStatAndRollbarLogWithExtras(rollbar.DEBUG, errorMsg, errorStatName, extras)
	log.Printf("DEBUG: %s WITH EXTRAS: %v\n", errorMsg, extras)
}

func (e *Impl) sendStatAndRollbarLogWithExtras(level string, errorMsg string, errorStatName string, extras map[string]interface{}) {
	e.Stats.Inc(errorStatName, 1, 1.0)
	rollbarErrorFunction(level, errors.New(errorMsg), extras)
}
