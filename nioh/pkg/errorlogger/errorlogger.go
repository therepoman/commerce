package errorlogger

import (
	"net/http"

	"github.com/cactus/go-statsd-client/statsd"
	"github.com/heroku/rollbar"

	"code.justin.tv/common/golibs/bininfo"
)

var rollbarErrorFunction = rollbar.ErrorWithExtras

// ErrorLogger a set of error loggers to wrap calls to rollbar, statsd, etc...
type ErrorLogger interface {
	RequestError(*http.Request, error)
	RequestPanic(*http.Request, interface{})

	LogFatalWithExtras(errorMsg string, errorStatName string, extras map[string]interface{})
	LogErrorWithExtras(errorMsg string, errorStatName string, extras map[string]interface{})
	LogWarnWithExtras(errorMsg string, errorStatName string, extras map[string]interface{})
	LogInfoWithExtras(errorMsg string, errorStatName string, extras map[string]interface{})
	LogDebugWithExtras(errorMsg string, errorStatName string, extras map[string]interface{})
}

// Impl is an implementation of ErrorLogger
type Impl struct {
	Stats statsd.Statter
}

// NewErrorLogger returns a new ErrorLogger.
func NewErrorLogger(token, env string, stats statsd.Statter) ErrorLogger {
	if token != "" {
		rollbar.SetToken(token)
	}
	rollbar.SetEnvironment(env)
	rollbar.SetCodeVersion(bininfo.Revision())
	return &Impl{Stats: stats}
}
