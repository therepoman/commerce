// Code generated by mockery 2.9.0. DO NOT EDIT.

// @generated

package mocks

import (
	http "net/http"

	mock "github.com/stretchr/testify/mock"
)

// ErrorLogger is an autogenerated mock type for the ErrorLogger type
type ErrorLogger struct {
	mock.Mock
}

// LogDebugWithExtras provides a mock function with given fields: errorMsg, errorStatName, extras
func (_m *ErrorLogger) LogDebugWithExtras(errorMsg string, errorStatName string, extras map[string]interface{}) {
	_m.Called(errorMsg, errorStatName, extras)
}

// LogErrorWithExtras provides a mock function with given fields: errorMsg, errorStatName, extras
func (_m *ErrorLogger) LogErrorWithExtras(errorMsg string, errorStatName string, extras map[string]interface{}) {
	_m.Called(errorMsg, errorStatName, extras)
}

// LogFatalWithExtras provides a mock function with given fields: errorMsg, errorStatName, extras
func (_m *ErrorLogger) LogFatalWithExtras(errorMsg string, errorStatName string, extras map[string]interface{}) {
	_m.Called(errorMsg, errorStatName, extras)
}

// LogInfoWithExtras provides a mock function with given fields: errorMsg, errorStatName, extras
func (_m *ErrorLogger) LogInfoWithExtras(errorMsg string, errorStatName string, extras map[string]interface{}) {
	_m.Called(errorMsg, errorStatName, extras)
}

// LogWarnWithExtras provides a mock function with given fields: errorMsg, errorStatName, extras
func (_m *ErrorLogger) LogWarnWithExtras(errorMsg string, errorStatName string, extras map[string]interface{}) {
	_m.Called(errorMsg, errorStatName, extras)
}

// RequestError provides a mock function with given fields: _a0, _a1
func (_m *ErrorLogger) RequestError(_a0 *http.Request, _a1 error) {
	_m.Called(_a0, _a1)
}

// RequestPanic provides a mock function with given fields: _a0, _a1
func (_m *ErrorLogger) RequestPanic(_a0 *http.Request, _a1 interface{}) {
	_m.Called(_a0, _a1)
}
