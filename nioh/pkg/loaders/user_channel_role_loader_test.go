package loaders

import (
	"context"
	"testing"

	"github.com/pkg/errors"
	"github.com/stretchr/testify/mock"

	"github.com/stretchr/testify/assert"

	"code.justin.tv/commerce/nioh/internal/clients"
	"code.justin.tv/commerce/nioh/internal/clients/zuma"
	"code.justin.tv/commerce/nioh/internal/clients/zuma/mocks"
)

func TestUserChannelRoleLoader(t *testing.T) {
	testQuery := zuma.UserRoleQuery{
		ChannelID: "123",
		UserID:    "456",
	}

	testCases := []struct {
		description      string
		response         zuma.UserRole
		wantVIP, wantMod bool
		wantErr          bool
	}{
		{
			description: "user has no roles",
		},
		{
			description: "user has `broadcaster` role",
			response:    "broadcaster",
		},
		{
			description: "user has `vip` role",
			response:    zuma.UserRoleVIP,
			wantVIP:     true,
		},
		{
			description: "user has `mod` role",
			response:    zuma.UserRoleModerator,
			wantMod:     true,
		},
		{
			description: "with error from Zuma",
			wantErr:     true,
		},
	}
	for _, tt := range testCases {
		t.Run(tt.description, func(t *testing.T) {
			mockZuma := new(mocks.Client)

			if tt.wantErr {
				mockZuma.On("GetUserRole", mock.Anything, testQuery).Return(tt.response, errors.New("ohno"))
			} else {
				mockZuma.On("GetUserRole", mock.Anything, testQuery).Return(tt.response, nil)
			}

			loaders := New(&clients.Clients{Zuma: mockZuma})
			ctx := loaders.Attach(context.Background())

			gotVIP, vipErr := IsUserChannelVIP(ctx, testQuery)
			gotMod, modErr := IsUserChannelModerator(ctx, testQuery)

			assert.Equal(t, tt.wantVIP, gotVIP)
			assert.Equal(t, tt.wantMod, gotMod)
			assert.Equal(t, tt.wantErr, vipErr != nil || modErr != nil)

			// should only get one call due to loader cache
			mockZuma.AssertNumberOfCalls(t, "GetUserRole", 1)
		})
	}
}
