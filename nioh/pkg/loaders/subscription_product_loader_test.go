package loaders

import (
	"context"
	"testing"

	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"

	"code.justin.tv/commerce/nioh/internal/clients"
	substwirp "code.justin.tv/revenue/subscriptions/twirp"
)

func TestSubscriptionProductLoader(t *testing.T) {
	Convey("Test TestSubscriptionProductLoader", t, func() {
		testProduct := &substwirp.Product{
			Id:        "123",
			ShortName: "testProductName",
		}
		clientImpl, mockClients := clients.InitMockClients()
		ctx := New(clientImpl).Attach(context.Background())

		Convey("test GetProductsByID", func() {
			Convey("error", func() {
				Convey("with subs error", func() {
					mockClients.Subscriptions.On("GetProductsByIDs", ctx, []string{testProduct.Id}).Return(nil, errors.New("error"))
				})

				Convey("without result", func() {
					mockClients.Subscriptions.On("GetProductsByIDs", ctx, []string{testProduct.Id}).Return(map[string]*substwirp.Product{}, nil)
				})

				results, err := GetProductsByIDs(ctx, []string{testProduct.Id})
				So(err, ShouldNotBeNil)
				So(results, ShouldBeEmpty)
			})

			Convey("success", func() {
				mockClients.Subscriptions.On("GetProductsByIDs", ctx, []string{testProduct.Id}).Return(map[string]*substwirp.Product{testProduct.Id: testProduct}, nil)

				results, err := GetProductsByIDs(ctx, []string{testProduct.Id})
				So(err, ShouldBeNil)
				So(results[testProduct.Id], ShouldEqual, testProduct)

				results, err = GetProductsByIDs(ctx, []string{testProduct.Id}) // Call twice in a row
				So(err, ShouldBeNil)
				So(results[testProduct.Id], ShouldEqual, testProduct)

				mockClients.Subscriptions.AssertNumberOfCalls(t, "GetProductsByIDs", 1) // Should be called only once
			})
		})
	})
}
