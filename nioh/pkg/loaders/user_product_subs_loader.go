package loaders

import (
	"context"
	"github.com/graph-gophers/dataloader"

	"code.justin.tv/commerce/logrus"

	voyagerTwirp "code.justin.tv/amzn/TwitchVoyagerTwirp"
	"code.justin.tv/commerce/nioh/internal/clients/voyager"
	"code.justin.tv/commerce/nioh/pkg/helpers"
)

// UserProductSubsLoaderResultType informs the status of the product sub for each product ID passed
type UserProductSubsLoaderResultType int8

const (
	// UserProductSubsLoaderResultTypeError means something went wrong and the product sub status is not resolved properly
	UserProductSubsLoaderResultTypeError UserProductSubsLoaderResultType = 1
	// UserProductSubsLoaderResultTypeSubbed means the user is subbed to this product
	UserProductSubsLoaderResultTypeSubbed UserProductSubsLoaderResultType = 2
	// UserProductSubsLoaderResultTypeNotSubbed means the user is not subbed to this product
	UserProductSubsLoaderResultTypeNotSubbed UserProductSubsLoaderResultType = 3
)

// IUserProductSubsLoader interface provides a way to load users product subscription data.
type IUserProductSubsLoader interface {
	GetSubsForProducts(ctx context.Context, productIDs []string) (results map[string]UserProductSubsLoaderResultType)
}

// UserProductSubsLoader implements IUserProductSubsLoader
type UserProductSubsLoader struct {
	client voyager.Client
}

// Batch ...
func (l *UserProductSubsLoader) Batch() dataloader.BatchFunc {
	return func(ctx context.Context, keys dataloader.Keys) []*dataloader.Result {
		userID, ok := helpers.UserIDFromContext(ctx)
		if !ok || userID == "" {
			return nil
		}

		var results []*dataloader.Result
		productIDs := keys.Keys()
		subscriptions, err := l.client.GetUserProductSubscriptions(ctx, userID, keys.Keys())
		if err != nil {
			logrus.WithField("productIDs", keys.Keys()).WithError(err).Error("Dataloader failed to load product subscriptions from voyager service")
			// All the product IDs are not resolved if the subs call failed.
			for range productIDs {
				results = append(results, &dataloader.Result{
					Error: err,
				})
			}
		} else {
			// Build the map
			productIDMap := map[string]*voyagerTwirp.Subscription{}
			for _, productID := range productIDs {
				productIDMap[productID] = nil
			}

			// Update the status map based on the result
			for _, subscription := range subscriptions {
				productIDMap[subscription.ProductId] = subscription
			}

			// Append to dataloader results
			for _, productID := range productIDs {
				results = append(results, &dataloader.Result{
					Data: productIDMap[productID],
				})
			}
		}
		return results
	}
}

// GetSubsForProducts ...
func GetSubsForProducts(ctx context.Context, productIDs []string) (results map[string]UserProductSubsLoaderResultType) {
	subStatuses, errs := loadMany(ctx, UserProductSubscriptionLoaderKey, productIDs)

	results = map[string]UserProductSubsLoaderResultType{}
	// dataloader either returns a slice of errors that matches the length of keys, or nil slice if there is no error at all.
	// Check for errs != nil first before accessing it.
	if len(subStatuses) != len(productIDs) || (errs != nil && len(errs) != len(productIDs)) {
		logrus.Warn("Dataloader returned unexpected results. Returning results as errors")
		for _, err := range errs {
			logrus.WithError(err).Error("failed to load user subscription for product")
		}
		for _, productID := range productIDs {
			results[productID] = UserProductSubsLoaderResultTypeError
		}
		return
	}
	for idx, productID := range productIDs {
		if errs != nil && errs[idx] != nil {
			results[productID] = UserProductSubsLoaderResultTypeError
		} else if subStatuses[idx].(*voyagerTwirp.Subscription) != nil {
			results[productID] = UserProductSubsLoaderResultTypeSubbed
		} else {
			results[productID] = UserProductSubsLoaderResultTypeNotSubbed
		}
	}
	return
}
