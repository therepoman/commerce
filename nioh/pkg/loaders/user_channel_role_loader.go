package loaders

import (
	"context"
	"fmt"
	"sync"

	"github.com/pkg/errors"

	"github.com/graph-gophers/dataloader"

	"code.justin.tv/commerce/nioh/internal/clients/zuma"
)

// IsUserChannelVIP uses the dataloader to answer whether the user specified in the query has the VIP role for the
// channel specified in the query.
func IsUserChannelVIP(ctx context.Context, query zuma.UserRoleQuery) (bool, error) {
	res, err := doUserChannelRoleQuery(ctx, userChannelRoleKey(query))
	return res == zuma.UserRoleVIP, err
}

// IsUserChannelModerator uses the dataloader to answer whether the user specified in the query has the channel moderator
// role for the channel specified in the query.
func IsUserChannelModerator(ctx context.Context, query zuma.UserRoleQuery) (bool, error) {
	res, err := doUserChannelRoleQuery(ctx, userChannelRoleKey(query))
	return res == zuma.UserRoleModerator, err
}

func doUserChannelRoleQuery(ctx context.Context, key userChannelRoleKey) (zuma.UserRole, error) {
	loader, err := Extract(ctx, UserChannelRoleLoaderKey)
	if err != nil {
		return "", err
	}

	thunk := loader.Load(ctx, key)

	data, err := thunk()
	if err != nil {
		return "", err
	}

	val, ok := data.(zuma.UserRole)
	if !ok {
		return "", errors.Errorf("unexpected return type from UserChannelRoleLoader: %v", data)
	}

	return val, nil
}

// userChannelRoleKey implements dataloader.Key
type userChannelRoleKey zuma.UserRoleQuery

func (u userChannelRoleKey) String() string {
	return fmt.Sprintf("channel:%s;user:%s", u.ChannelID, u.UserID)
}

func (u userChannelRoleKey) Raw() interface{} {
	return u
}

type userChannelRoleLoader struct {
	client zuma.Client
}

func (l *userChannelRoleLoader) Batch() dataloader.BatchFunc {
	return func(ctx context.Context, keys dataloader.Keys) []*dataloader.Result {
		results := make([]*dataloader.Result, len(keys))
		var wg sync.WaitGroup
		wg.Add(len(keys))

		for i, key := range keys {
			results[i] = new(dataloader.Result)

			go func(key dataloader.Key, result *dataloader.Result) {
				defer wg.Done()

				raw, ok := key.Raw().(userChannelRoleKey)
				if !ok {
					result.Error = errors.Errorf("UserChannelRoleLoader key was malformed or incorrect type: %+v", key)
					return
				}

				role, err := l.client.GetUserRole(ctx, zuma.UserRoleQuery{
					ChannelID: raw.ChannelID,
					UserID:    raw.UserID,
				})

				result.Data = role
				result.Error = err
			}(key, results[i])
		}

		wg.Wait()

		return results
	}
}
