package loaders

import (
	"context"
	"errors"
	"fmt"

	"code.justin.tv/commerce/nioh/internal/clients"

	"github.com/graph-gophers/dataloader"
)

type key string

// Theses keys are used get data loaders from the request context.
const (
	SubscriptionProductLoaderKey     = key("SubscriptionProductLoaderKey")
	UserProductSubscriptionLoaderKey = key("UserProductSubscriptionLoaderKey")
	UserLoaderKey                    = key("UserLoaderKey")
	UserChannelRoleLoaderKey         = key("UserChannelRoleLoader")
)

// Loader returns a dataloader batch function.
type Loader interface {
	Batch() dataloader.BatchFunc
}

type loaderWrapper struct {
	Loader
	maxBatchSize int
}

// Loaders ...
type Loaders map[key]Loader

// New initializes a mapping of context keys to attacher functions.
// The clients are injected into the attacher functions so that the dataloader
// batch functions can use them.
func New(clients *clients.Clients) Loaders {
	return Loaders{
		SubscriptionProductLoaderKey:     &SubscriptionProductLoader{client: clients.Subscriptions},
		UserProductSubscriptionLoaderKey: &UserProductSubsLoader{client: clients.TwitchVoyager},
		UserLoaderKey:                    &UserLoader{client: clients.UsersClient},
		UserChannelRoleLoaderKey:         &userChannelRoleLoader{client: clients.Zuma},
	}
}

// Attach batched data loaders to the request context by their respective keys.
func (ll Loaders) Attach(ctx context.Context) context.Context {
	for key, attacher := range ll {
		var loader dataloader.Interface
		if wrapper, ok := attacher.(*loaderWrapper); ok {
			loader = dataloader.NewBatchedLoader(
				wrapper.Batch(),
				dataloader.WithBatchCapacity(wrapper.maxBatchSize),
			)
		} else {
			loader = dataloader.NewBatchedLoader(attacher.Batch())
		}
		ctx = context.WithValue(ctx, key, safeLoader{loader})
	}
	return ctx
}

// Extract a dataloader instance from the request context.
func Extract(ctx context.Context, k key) (dataloader.Interface, error) {
	loader, found := ctx.Value(k).(dataloader.Interface)
	if !found {
		return nil, fmt.Errorf("unable to find %q loader on the request context", k)
	}
	return loader, nil
}

// LoaderExists returns if the given loader key exists in the context
func LoaderExists(ctx context.Context, k key) bool {
	_, found := ctx.Value(k).(dataloader.Interface)
	return found
}

func loadData(ctx context.Context, id key, key string) (interface{}, error) {
	if key == "" {
		return nil, errors.New("no key specified")
	}

	loader, err := Extract(ctx, id)
	if err != nil {
		return nil, err
	}

	loadFn := loader.Load(ctx, dataloader.StringKey(key))
	data, err := loadFn()
	if err != nil {
		return nil, err
	}

	return data, nil
}

func loadMany(ctx context.Context, id key, keys []string) ([]interface{}, []error) {
	if len(keys) == 0 {
		return nil, []error{errors.New("no keys to load")}
	}

	loader, err := Extract(ctx, id)
	if err != nil {
		return nil, []error{err}
	}

	loadFn := loader.LoadMany(ctx, dataloader.NewKeysFromStrings(keys))
	data, errs := loadFn()
	return data, errs
}

// fakeLoader is used to create fake dataloader.Interface instances for use in `Mock` functions.
func fakeLoader(m map[string]interface{}) dataloader.Interface {
	ldr := dataloader.NewBatchedLoader(func(ctx context.Context, keys dataloader.Keys) []*dataloader.Result {
		results := make([]*dataloader.Result, len(keys))
		for i, k := range keys {
			result := &dataloader.Result{}
			results[i] = result

			data, ok := m[k.String()]
			if !ok {
				continue
			}

			if err, ok := data.(error); ok {
				result.Error = err
			} else {
				result.Data = data
			}
		}
		return results
	})
	return safeLoader{ldr}
}

// MockLoader is a helper used to create mock versions of dataloaders that return the provided data.
// vv is expected to be an even-length array where each even index is a string-castable identifier for the data,
// and the following (odd) index is the data with that identifier.
func MockLoader(ctx context.Context, k key, vv ...interface{}) (context.Context, error) {
	if len(vv)%2 != 0 {
		return ctx, fmt.Errorf("expected an even number of arguments: %v", vv)
	}

	m := map[string]interface{}{}

	for i, v := range vv {
		if i%2 != 0 {
			continue
		}

		key, ok := v.(string)
		if !ok {
			return ctx, fmt.Errorf("Wrong Type. Expected: %s, Actual: %s", key, v)
		}

		m[key] = vv[i+1]
	}

	ldr := fakeLoader(m)

	ctx = context.WithValue(ctx, k, ldr)

	return ctx, nil
}

// safeLoader wraps dataloaders to prevent mocks comparison testing from reading the loader fields
type safeLoader struct {
	dataloader.Interface
}

func (s safeLoader) String() string {
	return "dataloader.Interface"
}
