package loaders

import (
	"context"

	"github.com/graph-gophers/dataloader"
	multierror "github.com/hashicorp/go-multierror"
	"github.com/pkg/errors"

	"code.justin.tv/commerce/nioh/internal/clients/subs"
	substwirp "code.justin.tv/revenue/subscriptions/twirp"
)

// ISubscriptionProductLoader provides user staff/admin status
type ISubscriptionProductLoader interface {
	GetProductByID(ctx context.Context, productID string) (*substwirp.Product, error)
}

// SubscriptionProductLoader implements the interface using the dataloader via users service client
type SubscriptionProductLoader struct {
	client subs.Client
}

// Batch ...
func (l *SubscriptionProductLoader) Batch() dataloader.BatchFunc {
	return func(ctx context.Context, keys dataloader.Keys) []*dataloader.Result {
		productIDs := keys.Keys()
		m := make([]*dataloader.Result, len(keys))

		productMap := l.client.GetProductsByIDs(ctx, productIDs)

		for idx, productID := range productIDs {
			result := &dataloader.Result{}

			if product, ok := productMap[productID]; ok {
				result.Data = product
			} else {
				result.Error = errors.New("no product for given productID")
			}

			m[idx] = result
		}
		return m
	}
}

// GetProductsByIDs returns sub product for the given id
func GetProductsByIDs(ctx context.Context, productIDs []string) (map[string]*substwirp.Product, error) {
	products, errs := loadMany(ctx, SubscriptionProductLoaderKey, productIDs)

	results := map[string]*substwirp.Product{}

	for _, p := range products {
		if p != nil {
			product := p.(*substwirp.Product)
			results[product.Id] = product
		}
	}

	multierr := &multierror.Error{Errors: errs}
	return results, multierr.ErrorOrNil()
}
