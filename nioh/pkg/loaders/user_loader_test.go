package loaders

import (
	"context"
	"testing"

	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"

	"code.justin.tv/commerce/nioh/internal/clients"
	usersModels "code.justin.tv/web/users-service/models"
)

func TestUserLoader(t *testing.T) {
	Convey("Test UserLoader", t, func() {
		testUserID := "123"
		clientImpl, mockClients := clients.InitMockClients()
		ctx := New(clientImpl).Attach(context.Background())
		userProps := &usersModels.Properties{
			ID: testUserID,
		}

		Convey("test IsUserStaff", func() {
			Convey("with error", func() {
				mockClients.UsersClient.On("GetUserByID", mock.Anything, testUserID).Return(nil, errors.New("error"))
				_, err := IsUserStaff(ctx, testUserID)
				So(err, ShouldNotBeNil)
			})

			Convey("without error", func() {
				var expected bool

				Convey("with non staff user", func() {
					expected = false
				})

				Convey("with staff user", func() {
					expected = true
				})

				userProps.Admin = &expected
				mockClients.UsersClient.On("GetUserByID", mock.Anything, testUserID).Return(userProps, nil)

				result, err := IsUserStaff(ctx, testUserID)
				So(err, ShouldBeNil)
				So(result, ShouldEqual, expected)
			})
		})

		Convey("test IsUserSiteAdmin", func() {
			Convey("with error", func() {
				mockClients.UsersClient.On("GetUserByID", mock.Anything, testUserID).Return(nil, errors.New("error"))
				_, err := IsUserSiteAdmin(ctx, testUserID)
				So(err, ShouldNotBeNil)
			})

			Convey("without error", func() {
				var expected bool

				Convey("with non staff user", func() {
					expected = false
				})

				Convey("with staff user", func() {
					expected = true
				})

				userProps.Subadmin = &expected
				mockClients.UsersClient.On("GetUserByID", mock.Anything, testUserID).Return(userProps, nil)

				result, err := IsUserSiteAdmin(ctx, testUserID)
				So(err, ShouldBeNil)
				So(result, ShouldEqual, expected)
			})
		})
	})
}
