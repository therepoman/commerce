package loaders

import (
	"context"
	"testing"

	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"

	voyager "code.justin.tv/amzn/TwitchVoyagerTwirp"
	"code.justin.tv/commerce/nioh/internal/clients"
	"code.justin.tv/commerce/nioh/pkg/helpers"
)

func TestUserProductSubsLoader(t *testing.T) {
	Convey("Test TestUserProductSubsLoader", t, func() {
		testUserID := "123"
		testProductID1 := "456"
		testProductID2 := "789"
		clientImpl, mockClients := clients.InitMockClients()
		ctx := New(clientImpl).Attach(context.Background())
		ctx = helpers.ContextWithUser(ctx, testUserID)

		Convey("test GetSubsForProducts", func() {
			var productIDs []string
			var expected map[string]UserProductSubsLoaderResultType

			Convey("with empty product ID list", func() {
				expected = map[string]UserProductSubsLoaderResultType{}
			})

			Convey("with product IDs to resolve", func() {
				productIDs = []string{testProductID1, testProductID2}
				subscriptions := []*voyager.Subscription{{
					ProductId: testProductID1,
				}}
				mockClients.TwitchVoyager.On("GetUserProductSubscriptions", ctx, testUserID, mock.Anything).Return(subscriptions, nil)
				expected = map[string]UserProductSubsLoaderResultType{
					testProductID1: UserProductSubsLoaderResultTypeSubbed,
					testProductID2: UserProductSubsLoaderResultTypeNotSubbed,
				}
			})

			Convey("with subs client error", func() {
				productIDs = []string{testProductID1, testProductID2}
				mockClients.TwitchVoyager.On("GetUserProductSubscriptions", ctx, testUserID, mock.Anything).Return(nil, errors.New("subs error"))
				expected = map[string]UserProductSubsLoaderResultType{
					testProductID1: UserProductSubsLoaderResultTypeError,
					testProductID2: UserProductSubsLoaderResultTypeError,
				}
			})

			So(GetSubsForProducts(ctx, productIDs), ShouldResemble, expected)
		})
	})
}
