package loaders

import (
	"context"
	"sync"

	"github.com/graph-gophers/dataloader"
	"github.com/pkg/errors"

	"code.justin.tv/commerce/nioh/internal/clients/users"
	usersModels "code.justin.tv/web/users-service/models"
)

// IUserLoader provides user staff/admin status
type IUserLoader interface {
	IsUserStaff(ctx context.Context, userID string) (bool, error)
	IsUserSiteAdmin(ctx context.Context, userID string) (bool, error)
}

// UserLoader implements the interface using the dataloader via users service client
type UserLoader struct {
	client users.IUsersClient
}

// Batch ...
func (l *UserLoader) Batch() dataloader.BatchFunc {
	return func(ctx context.Context, keys dataloader.Keys) []*dataloader.Result {
		userIDs := keys.Keys()
		results := make([]*dataloader.Result, len(keys))
		var wg sync.WaitGroup
		var mutex sync.Mutex
		wg.Add(len(userIDs))

		for idx, userID := range userIDs {
			go func(ctx context.Context, userID string, idx int) {
				defer wg.Done()
				result := &dataloader.Result{}
				resp, err := l.client.GetUserByID(ctx, userID)
				if err != nil {
					result.Error = err
				} else {
					result.Data = resp
				}
				mutex.Lock()
				results[idx] = result
				mutex.Unlock()
			}(ctx, userID, idx)
		}

		wg.Wait()

		return results
	}
}

// IsUserStaff returns whether the user is staff
func IsUserStaff(ctx context.Context, userID string) (bool, error) {
	user, err := getUserByID(ctx, userID)
	if err != nil {
		return false, err
	}
	if user.Admin == nil {
		return false, nil
	}
	return *user.Admin, nil
}

// IsUserSiteAdmin returns whether the user is site admin
func IsUserSiteAdmin(ctx context.Context, userID string) (bool, error) {
	user, err := getUserByID(ctx, userID)
	if err != nil {
		return false, err
	}
	if user.Subadmin == nil {
		return false, nil
	}
	return *user.Subadmin, nil
}

func getUserByID(ctx context.Context, userID string) (*usersModels.Properties, error) {
	resp, err := loadData(ctx, UserLoaderKey, userID)
	if err != nil {
		return nil, err
	}

	user, ok := resp.(*usersModels.Properties)
	if !ok {
		return nil, errors.New("loader encountered unexpected data")
	}

	return user, nil
}
