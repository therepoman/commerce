package exemptions

import (
	"context"
	"testing"
	"time"

	"code.justin.tv/commerce/nioh/internal/clients/zuma"
	"code.justin.tv/commerce/nioh/internal/testdata"

	"github.com/pkg/errors"
	"github.com/stretchr/testify/assert"

	"github.com/stretchr/testify/mock"

	"code.justin.tv/commerce/nioh/pkg/loaders"

	voyager "code.justin.tv/amzn/TwitchVoyagerTwirp"
	"code.justin.tv/commerce/nioh/internal/clients"
	"code.justin.tv/commerce/nioh/internal/models"
	"code.justin.tv/commerce/nioh/internal/models/mocks"
	sMocks "code.justin.tv/commerce/nioh/mocks"
	userModels "code.justin.tv/web/users-service/models"
)

func TestExemptionChecker(t *testing.T) {
	exemptionSlice := func(es ...*models.Exemption) []*models.Exemption {
		return append([]*models.Exemption{}, es...)
	}

	testUserID := "test-user-id"
	now := time.Date(2019, time.January, 18, 2, 42, 0, 0, time.UTC)

	activeStartDate := time.Date(2019, time.January, 1, 0, 0, 0, 0, time.UTC)
	activeEndDate := time.Date(2019, time.March, 10, 0, 0, 0, 0, time.UTC)

	inactiveEndDate := time.Date(2019, time.January, 5, 0, 0, 0, 0, time.UTC)

	unknownExemption := &models.Exemption{
		ExemptionType:   models.UnknownExemptionType,
		ActiveStartDate: activeStartDate,
		ActiveEndDate:   activeEndDate,
		Keys:            []string{},
	}

	allExemption := &models.Exemption{
		ExemptionType:   models.AllExemptionType,
		ActiveStartDate: activeStartDate,
		ActiveEndDate:   activeEndDate,
		Keys:            []string{},
	}

	staffExemption := &models.Exemption{
		ExemptionType:   models.StaffExemptionType,
		ActiveStartDate: activeStartDate,
		ActiveEndDate:   activeEndDate,
		Keys:            []string{},
	}

	siteAdminExemption := &models.Exemption{
		ExemptionType:   models.SiteAdminExemptionType,
		ActiveStartDate: activeStartDate,
		ActiveEndDate:   activeEndDate,
		Keys:            []string{},
	}

	testProductID := "test-product-id"
	productExemption := &models.Exemption{
		ExemptionType:   models.ProductExemptionType,
		ActiveStartDate: activeStartDate,
		ActiveEndDate:   activeEndDate,
		Keys:            []string{testProductID},
	}

	testChannelID := "test-channel-id"

	channelVIPExemption := &models.Exemption{
		ExemptionType:   models.ChannelVIPExemptionType,
		ActiveStartDate: activeStartDate,
		ActiveEndDate:   activeEndDate,
		Keys:            []string{testChannelID},
	}
	multipleChannelVIPExemption := &models.Exemption{
		ExemptionType:   models.ChannelVIPExemptionType,
		ActiveStartDate: activeStartDate,
		ActiveEndDate:   activeEndDate,
		Keys:            []string{"1", "2", "3"},
	}

	channelModeratorExemption := &models.Exemption{
		ExemptionType:   models.ChannelModeratorExemptionType,
		ActiveStartDate: activeStartDate,
		ActiveEndDate:   activeEndDate,
		Keys:            []string{testChannelID},
	}
	multipleChannelModeratorExemption := &models.Exemption{
		ExemptionType:   models.ChannelModeratorExemptionType,
		ActiveStartDate: activeStartDate,
		ActiveEndDate:   activeEndDate,
		Keys:            []string{"1", "2", "3"},
	}

	testCompanyID := "test_company_id"
	organizationMemberExemption := &models.Exemption{
		ExemptionType:   models.OrganizationMemberExemptionType,
		ActiveStartDate: activeStartDate,
		ActiveEndDate:   activeEndDate,
		Keys:            []string{testCompanyID},
	}
	testValidationMap := make(map[string]bool)
	testValidationMap[testCompanyID] = true

	inactiveExemption := &models.Exemption{
		ExemptionType:   models.AllExemptionType,
		ActiveStartDate: activeStartDate,
		ActiveEndDate:   inactiveEndDate,
		Keys:            []string{"test-product-id"},
	}

	boolTrue := true
	boolFalse := false

	testCases := []struct {
		description string

		userID     string
		exemptions []*models.Exemption

		expectedUserExemptionStatus UserExemptionStatus
		wantErr                     bool

		setupMocks func(mocks *clients.MockClients)
	}{
		{
			description: "unknown exemption",
			userID:      testUserID,
			exemptions:  exemptionSlice(unknownExemption),
			expectedUserExemptionStatus: UserExemptionStatus{
				UserID:               testUserID,
				ApplicableExemptions: nil,
			},
			wantErr:    false,
			setupMocks: nil,
		},
		{
			description: "all exemption with known user",
			userID:      testUserID,
			exemptions:  exemptionSlice(allExemption),
			expectedUserExemptionStatus: UserExemptionStatus{
				UserID:               testUserID,
				ApplicableExemptions: testdata.ApplyExemptions(exemptionSlice(allExemption)),
			},
			wantErr:    false,
			setupMocks: nil,
		},
		{
			description: "all exemption with anonymous user",
			userID:      "",
			exemptions:  exemptionSlice(allExemption),
			expectedUserExemptionStatus: UserExemptionStatus{
				UserID:               "",
				ApplicableExemptions: testdata.ApplyExemptions(exemptionSlice(allExemption)),
			},
			wantErr:    false,
			setupMocks: nil,
		},
		{
			description: "staff exemption with staff user",
			userID:      testUserID,
			exemptions:  exemptionSlice(staffExemption),
			expectedUserExemptionStatus: UserExemptionStatus{
				UserID:               testUserID,
				ApplicableExemptions: testdata.ApplyExemptions(exemptionSlice(staffExemption)),
			},
			setupMocks: func(mocks *clients.MockClients) {
				mocks.UsersClient.On("GetUserByID", mock.Anything, mock.Anything).
					Return(&userModels.Properties{Admin: &boolTrue}, nil)
			},
		},
		{
			description: "staff exemption with nonstaff user",
			userID:      testUserID,
			exemptions:  exemptionSlice(staffExemption),
			expectedUserExemptionStatus: UserExemptionStatus{
				UserID:               testUserID,
				ApplicableExemptions: nil,
			},
			setupMocks: func(mocks *clients.MockClients) {
				mocks.UsersClient.On("GetUserByID", mock.Anything, mock.Anything).
					Return(&userModels.Properties{Admin: &boolFalse}, nil)
			},
		},
		{
			description: "staff exemption with anonymous user",
			userID:      "",
			exemptions:  exemptionSlice(staffExemption),
			expectedUserExemptionStatus: UserExemptionStatus{
				UserID:               "",
				ApplicableExemptions: nil,
			},
		},
		{
			description: "staff exemption with error",
			userID:      testUserID,
			exemptions:  exemptionSlice(staffExemption),
			expectedUserExemptionStatus: UserExemptionStatus{
				UserID:               testUserID,
				ApplicableExemptions: nil,
			},
			setupMocks: func(mocks *clients.MockClients) {
				mocks.UsersClient.On("GetUserByID", mock.Anything, mock.Anything).
					Return(nil, errors.New("your users client asplode"))
			},
			wantErr: false,
		},
		{
			description: "site admin exemption with admin user",
			userID:      testUserID,
			exemptions:  exemptionSlice(siteAdminExemption),
			expectedUserExemptionStatus: UserExemptionStatus{
				UserID:               testUserID,
				ApplicableExemptions: testdata.ApplyExemptions(exemptionSlice(siteAdminExemption)),
			},
			setupMocks: func(mocks *clients.MockClients) {
				mocks.UsersClient.On("GetUserByID", mock.Anything, mock.Anything).
					Return(&userModels.Properties{Subadmin: &boolTrue}, nil)
			},
		},
		{
			description: "site admin exemption with nonadmin user",
			userID:      testUserID,
			exemptions:  exemptionSlice(siteAdminExemption),
			expectedUserExemptionStatus: UserExemptionStatus{
				UserID:               testUserID,
				ApplicableExemptions: nil,
			},
			setupMocks: func(mocks *clients.MockClients) {
				mocks.UsersClient.On("GetUserByID", mock.Anything, mock.Anything).
					Return(&userModels.Properties{Subadmin: &boolFalse}, nil)
			},
		},
		{
			description: "site admin exemption with anonymous user",
			userID:      "",
			exemptions:  exemptionSlice(siteAdminExemption),
			expectedUserExemptionStatus: UserExemptionStatus{
				UserID:               "",
				ApplicableExemptions: nil,
			},
		},

		{
			description: "site admin exemption with error",
			userID:      testUserID,
			exemptions:  exemptionSlice(siteAdminExemption),
			expectedUserExemptionStatus: UserExemptionStatus{
				UserID:               testUserID,
				ApplicableExemptions: nil,
			},
			setupMocks: func(mocks *clients.MockClients) {
				mocks.UsersClient.On("GetUserByID", mock.Anything, mock.Anything).
					Return(nil, errors.New("your users client asplode"))
			},
			wantErr: false,
		},
		{
			description: "product exemption with entitled user",
			userID:      testUserID,
			exemptions:  exemptionSlice(productExemption),
			expectedUserExemptionStatus: UserExemptionStatus{
				UserID:               testUserID,
				ApplicableExemptions: testdata.ApplyExemptionsWithMatchedKey(exemptionSlice(productExemption), testProductID),
			},
			setupMocks: func(mocks *clients.MockClients) {
				mocks.TwitchVoyager.On("GetUserProductSubscriptions", mock.Anything, mock.Anything, mock.Anything).
					Return([]*voyager.Subscription{{ProductId: testProductID}}, nil)
			},
		},
		{
			description: "product exemption with unentitled user",
			userID:      testUserID,
			exemptions:  exemptionSlice(productExemption),
			expectedUserExemptionStatus: UserExemptionStatus{
				UserID:               testUserID,
				ApplicableExemptions: nil,
			},
			setupMocks: func(mocks *clients.MockClients) {
				mocks.TwitchVoyager.On("GetUserProductSubscriptions", mock.Anything, mock.Anything, mock.Anything).
					Return(nil, nil)
			},
		},
		{
			description: "product exemption with anonymous",
			userID:      "",
			exemptions:  exemptionSlice(productExemption),
			expectedUserExemptionStatus: UserExemptionStatus{
				UserID:               "",
				ApplicableExemptions: nil,
			},
		},
		{
			description: "product exemption with subs error",
			userID:      testUserID,
			exemptions:  exemptionSlice(productExemption),
			expectedUserExemptionStatus: UserExemptionStatus{
				UserID:               testUserID,
				ApplicableExemptions: nil,
			},
			setupMocks: func(mocks *clients.MockClients) {
				mocks.TwitchVoyager.On("GetUserProductSubscriptions", mock.Anything, mock.Anything, mock.Anything).
					Return(nil, errors.New("bad sub"))
			},
			wantErr: true,
		},
		{
			description: "inactive exemption with known user",
			userID:      testUserID,
			exemptions:  exemptionSlice(inactiveExemption),
			expectedUserExemptionStatus: UserExemptionStatus{
				UserID:               testUserID,
				ApplicableExemptions: nil,
			},
		},
		{
			description: "mix of exemptions including inactive",
			userID:      testUserID,
			exemptions:  exemptionSlice(inactiveExemption, staffExemption, siteAdminExemption, productExemption),
			expectedUserExemptionStatus: UserExemptionStatus{
				UserID:               testUserID,
				ApplicableExemptions: testdata.ApplyExemptionsWithMatchedKey(exemptionSlice(staffExemption, productExemption), ""),
			},
			setupMocks: func(mocks *clients.MockClients) {
				mocks.UsersClient.On("GetUserByID", mock.Anything, mock.Anything).Return(&userModels.Properties{Admin: &boolTrue, Subadmin: &boolFalse}, nil)
				mocks.TwitchVoyager.On("GetUserProductSubscriptions", mock.Anything, mock.Anything, mock.Anything).
					Return([]*voyager.Subscription{{ProductId: testProductID}}, nil)
			},
		},
		{
			description: "channel VIP exemption with anonymous",
			userID:      "",
			exemptions:  exemptionSlice(channelVIPExemption),
			expectedUserExemptionStatus: UserExemptionStatus{
				UserID:               "",
				ApplicableExemptions: nil,
			},
		},
		{
			description: "channel VIP exemption with VIP user",
			userID:      testUserID,
			exemptions:  exemptionSlice(channelVIPExemption),
			expectedUserExemptionStatus: UserExemptionStatus{
				UserID:               testUserID,
				ApplicableExemptions: testdata.ApplyExemptionsWithMatchedKey(exemptionSlice(channelVIPExemption), testChannelID),
			},
			setupMocks: func(mocks *clients.MockClients) {
				mocks.Zuma.On("GetUserRole", mock.Anything, mock.Anything).Return(zuma.UserRoleVIP, nil)
			},
		},
		{
			description: "channel VIP exemption with non-VIP user",
			userID:      testUserID,
			exemptions:  exemptionSlice(channelVIPExemption),
			expectedUserExemptionStatus: UserExemptionStatus{
				UserID:               testUserID,
				ApplicableExemptions: nil,
			},
			setupMocks: func(mocks *clients.MockClients) {
				mocks.Zuma.On("GetUserRole", mock.Anything, mock.Anything).Return(zuma.UserRole(""), nil)
			},
		},
		{
			description: "channel VIP exemption with Zuma error",
			userID:      testUserID,
			exemptions:  exemptionSlice(channelVIPExemption),
			expectedUserExemptionStatus: UserExemptionStatus{
				UserID:               testUserID,
				ApplicableExemptions: nil,
			},
			setupMocks: func(mocks *clients.MockClients) {
				mocks.Zuma.On("GetUserRole", mock.Anything, mock.Anything).Return(zuma.UserRole(""), errors.New("zuma explode"))
			},
		},
		{
			description: "channel VIP exemption behavior with multiple keys",
			userID:      testUserID,
			exemptions:  exemptionSlice(multipleChannelVIPExemption),
			expectedUserExemptionStatus: UserExemptionStatus{
				UserID:               testUserID,
				ApplicableExemptions: testdata.ApplyExemptionsWithMatchedKey(exemptionSlice(multipleChannelVIPExemption), "3"),
			},
			setupMocks: func(mocks *clients.MockClients) {
				mocks.Zuma.On("GetUserRole", mock.Anything, zuma.UserRoleQuery{
					ChannelID: "1",
					UserID:    testUserID,
				}).Return(zuma.UserRoleModerator, nil)
				mocks.Zuma.On("GetUserRole", mock.Anything, zuma.UserRoleQuery{
					ChannelID: "2",
					UserID:    testUserID,
				}).Return(zuma.UserRole(""), errors.New("zuma explode"))
				mocks.Zuma.On("GetUserRole", mock.Anything, zuma.UserRoleQuery{
					ChannelID: "3",
					UserID:    testUserID,
				}).Return(zuma.UserRoleVIP, nil)
			},
		},
		{
			description: "channel moderator exemption with anonymous",
			userID:      "",
			exemptions:  exemptionSlice(channelModeratorExemption),
			expectedUserExemptionStatus: UserExemptionStatus{
				UserID:               "",
				ApplicableExemptions: nil,
			},
		},
		{
			description: "channel moderator exemption with modded user",
			userID:      testUserID,
			exemptions:  exemptionSlice(channelModeratorExemption),
			expectedUserExemptionStatus: UserExemptionStatus{
				UserID:               testUserID,
				ApplicableExemptions: testdata.ApplyExemptionsWithMatchedKey(exemptionSlice(channelModeratorExemption), testChannelID),
			},
			setupMocks: func(mocks *clients.MockClients) {
				mocks.Zuma.On("GetUserRole", mock.Anything, mock.Anything).Return(zuma.UserRoleModerator, nil)
			},
		},
		{
			description: "channel moderator exemption with non-modded user",
			userID:      testUserID,
			exemptions:  exemptionSlice(channelModeratorExemption),
			expectedUserExemptionStatus: UserExemptionStatus{
				UserID:               testUserID,
				ApplicableExemptions: nil,
			},
			setupMocks: func(mocks *clients.MockClients) {
				mocks.Zuma.On("GetUserRole", mock.Anything, mock.Anything).Return(zuma.UserRole(""), nil)
			},
		},
		{
			description: "channel moderator exemption with Zuma error",
			userID:      testUserID,
			exemptions:  exemptionSlice(channelModeratorExemption),
			expectedUserExemptionStatus: UserExemptionStatus{
				UserID:               testUserID,
				ApplicableExemptions: nil,
			},
			setupMocks: func(mocks *clients.MockClients) {
				mocks.Zuma.On("GetUserRole", mock.Anything, mock.Anything).Return(zuma.UserRole(""), errors.New("zuma explode"))
			},
		},
		{
			description: "channel moderator exemption behavior with multiple keys",
			userID:      testUserID,
			exemptions:  exemptionSlice(multipleChannelModeratorExemption),
			expectedUserExemptionStatus: UserExemptionStatus{
				UserID:               testUserID,
				ApplicableExemptions: testdata.ApplyExemptionsWithMatchedKey(exemptionSlice(multipleChannelModeratorExemption), "3"),
			},
			setupMocks: func(mocks *clients.MockClients) {
				mocks.Zuma.On("GetUserRole", mock.Anything, zuma.UserRoleQuery{
					ChannelID: "1",
					UserID:    testUserID,
				}).Return(zuma.UserRoleVIP, nil)
				mocks.Zuma.On("GetUserRole", mock.Anything, zuma.UserRoleQuery{
					ChannelID: "2",
					UserID:    testUserID,
				}).Return(zuma.UserRole(""), errors.New("zuma explode"))
				mocks.Zuma.On("GetUserRole", mock.Anything, zuma.UserRoleQuery{
					ChannelID: "3",
					UserID:    testUserID,
				}).Return(zuma.UserRoleModerator, nil)
			},
		},
		{
			description: "organization member exemption behavior with valid membership",
			userID:      testUserID,
			exemptions:  exemptionSlice(organizationMemberExemption),
			expectedUserExemptionStatus: UserExemptionStatus{
				UserID:               testUserID,
				ApplicableExemptions: testdata.ApplyExemptionsWithMatchedKey(exemptionSlice(organizationMemberExemption), testCompanyID),
			},
			setupMocks: func(mocks *clients.MockClients) {
				mocks.Rbac.On(
					"ValidateUserMembershipInCompanies",
					mock.Anything,
					testUserID,
					[]string{testCompanyID},
				).Return(testValidationMap, nil)
			},
		},
		{
			description: "organization member exemption behavior with invalid membership",
			userID:      testUserID,
			exemptions:  exemptionSlice(organizationMemberExemption),
			expectedUserExemptionStatus: UserExemptionStatus{
				UserID:               testUserID,
				ApplicableExemptions: nil,
			},
			setupMocks: func(mocks *clients.MockClients) {
				mocks.Rbac.On(
					"ValidateUserMembershipInCompanies",
					mock.Anything,
					testUserID,
					[]string{testCompanyID},
				).Return(make(map[string]bool), nil)
			},
		},
		{
			description: "organization member exemption doesn't apply to anonymous user",
			userID:      "",
			exemptions:  exemptionSlice(),
			expectedUserExemptionStatus: UserExemptionStatus{
				UserID:               "",
				ApplicableExemptions: nil,
			},
		},
	}

	for _, tt := range testCases {
		t.Run(tt.description, func(t *testing.T) {
			clientImpl, mockClients := clients.InitMockClients()
			attacher := loaders.New(clientImpl)
			ctx := attacher.Attach(context.Background())

			if tt.setupMocks != nil {
				tt.setupMocks(mockClients)
			}

			mockClock := new(mocks.IClock)
			mockClock.On("Now").Return(now)
			mockStatter := new(sMocks.Statter)
			mockStatter.On("Inc", mock.Anything, mock.Anything, mock.Anything).Return(nil)

			exemptionChecker := &ExemptionChecker{
				Clients: clientImpl,
				Clock:   mockClock,
				Stats:   mockStatter,
			}

			actualUserExemptionStatus, err := exemptionChecker.GetUserExemptionStatus(ctx, tt.userID, tt.exemptions)
			assert.Equal(t, tt.wantErr, err != nil)
			assert.Equal(t, tt.expectedUserExemptionStatus, actualUserExemptionStatus)
		})
	}
}
