package exemptions

import (
	"context"
	"fmt"
	"sync"

	"code.justin.tv/commerce/nioh/internal/clients"
	"code.justin.tv/commerce/nioh/internal/clients/zuma"
	"code.justin.tv/commerce/nioh/internal/models"
	"code.justin.tv/commerce/nioh/pkg/helpers"
	"code.justin.tv/commerce/nioh/pkg/loaders"

	"github.com/cactus/go-statsd-client/statsd"
	"github.com/hashicorp/go-multierror"
	"github.com/pkg/errors"

	log "code.justin.tv/commerce/logrus"
)

// IExemptionChecker provides a mechanism for checking a user's eligibility under a set of exemptions.
type IExemptionChecker interface {
	GetUserExemptionStatus(ctx context.Context, userID string, exemptions []*models.Exemption) (UserExemptionStatus, error)
}

// ExemptionChecker is used for various channel restriction exemption checking purposes
type ExemptionChecker struct {
	Stats   statsd.Statter
	Clients *clients.Clients
	Clock   models.IClock
}

var _ IExemptionChecker = new(ExemptionChecker)

func (e *ExemptionChecker) isUserExemptViaProductSubscription(ctx context.Context, exemption *models.Exemption) (bool, string, error) {
	productIDs := exemption.Keys
	if len(productIDs) <= 0 {
		return true, "", errors.New("restriction with product subscription exemption must specify at least one product ID")
	}

	subsStatuses := loaders.GetSubsForProducts(ctx, productIDs)

	var err error
	for _, productID := range productIDs {
		if subsStatuses[productID] == loaders.UserProductSubsLoaderResultTypeSubbed {
			return true, productID, nil
		} else if subsStatuses[productID] == loaders.UserProductSubsLoaderResultTypeError {
			err = errors.New("loader failed to resolve product ID for user subs status")
		}
	}

	return false, "", err
}

// UserExemptionStatus associates a user to a list of exemptions that the user was eligible for at the time of a
// GetUserExemptionStatus call.
// TODO: Possibly attach the entire models.RestrictionV2 here for context (or perhaps just the ResourceKey) to disambiguate the data
type UserExemptionStatus struct {
	UserID               string
	ApplicableExemptions []*models.AppliedExemption
}

// GetUserExemptionStatus checks a user's status under each of the passed exemptions and returns a UserExemptionStatus
// representing the set of exemptions that the user is eligible under at the time of the call.
func (e *ExemptionChecker) GetUserExemptionStatus(ctx context.Context, userID string, exemptions []*models.Exemption) (UserExemptionStatus, error) {
	out := UserExemptionStatus{UserID: userID}
	ctx = helpers.ContextWithUser(ctx, userID)

	log.Debugf("Checking %v exemptions", len(exemptions))

	wg := &sync.WaitGroup{}
	mutex := &sync.Mutex{}
	errs := make([]error, len(exemptions))
	applicableExemptions := make([]*models.AppliedExemption, len(exemptions))

	for idx, exemption := range exemptions {
		wg.Add(1)
		go func(exemption *models.Exemption, idx int) {
			defer wg.Done()
			log.Debugf("Checking %v exemption", exemption.ExemptionType)
			exempt, matchedKey, err := e.checkExemption(ctx, userID, exemption)
			if err != nil {
				mutex.Lock()
				errs[idx] = err
				mutex.Unlock()
				return
			}

			if exempt {
				log.Debugf("User is exempt for %v", exemption.ExemptionType)
				mutex.Lock()
				applicableExemptions[idx] = &models.AppliedExemption{
					Exemption:  exemption,
					MatchedKey: matchedKey,
				}
				mutex.Unlock()
			}
		}(exemption, idx)
	}

	wg.Wait()

	var multierr *multierror.Error
	// Keeps found errors in order
	for _, err := range errs {
		if err != nil {
			multierr = multierror.Append(multierr, err)
		}
	}
	// Keeps applied exemptions in order
	for _, exemption := range applicableExemptions {
		if exemption != nil {
			out.ApplicableExemptions = append(out.ApplicableExemptions, exemption)
		}
	}
	return out, multierr.ErrorOrNil()
}

func (e *ExemptionChecker) checkExemption(ctx context.Context, userID string, exemption *models.Exemption) (bool, string, error) {
	isExempt, matchedKey, err := e.checkExemptionResult(ctx, userID, exemption)
	if err != nil {
		e.incExemptionFailure(exemption.ExemptionType)
	}
	return isExempt, matchedKey, err
}

func (e *ExemptionChecker) checkExemptionResult(ctx context.Context, userID string, exemption *models.Exemption) (bool, string, error) {
	if !exemption.IsActive(e.Clock.Now()) {
		return false, "", nil
	}

	if canAccess, shortcircuit := e.shortcircuitAnonymous(userID, exemption.ExemptionType); shortcircuit {
		return canAccess, "", nil
	}

	switch exemption.ExemptionType {
	case models.UnknownExemptionType:
		return false, "", nil

	case models.AllExemptionType:
		return true, "", nil

	case models.StaffExemptionType:
		isStaff, err := loaders.IsUserStaff(ctx, userID)
		if err != nil {
			e.incExemptionFailure(exemption.ExemptionType)
			return false, "", nil
		}

		return isStaff, "", nil

	case models.SiteAdminExemptionType:
		isSiteAdmin, err := loaders.IsUserSiteAdmin(ctx, userID)
		if err != nil {
			e.incExemptionFailure(exemption.ExemptionType)
			return false, "", nil
		}

		return isSiteAdmin, "", nil

	case models.ProductExemptionType:
		return e.isUserExemptViaProductSubscription(ctx, exemption)

	case models.ChannelVIPExemptionType:
		return e.isUserExemptViaChannelRole(ctx, userID, exemption, loaders.IsUserChannelVIP)

	case models.ChannelModeratorExemptionType:
		return e.isUserExemptViaChannelRole(ctx, userID, exemption, loaders.IsUserChannelModerator)

	case models.OrganizationMemberExemptionType:
		return e.isUserExemptViaMembershipChecking(ctx, userID, exemption)

	default:
		return false, "", nil
	}
}

type userRoleQueryFunc func(context.Context, zuma.UserRoleQuery) (bool, error)

func (e *ExemptionChecker) isUserExemptViaChannelRole(
	ctx context.Context,
	userID string,
	exemption *models.Exemption,
	queryFunc userRoleQueryFunc,
) (bool, string, error) {
	var hasRole bool
	var err error
	var matchedKey string

	for _, channelID := range exemption.Keys {
		hasRole, err = queryFunc(ctx, zuma.UserRoleQuery{
			ChannelID: channelID,
			UserID:    userID,
		})

		if err != nil {
			log.WithError(err).WithFields(log.Fields{
				"channelID": channelID,
				"userID":    userID,
				"queryFunc": queryFunc,
			}).Error("unexpected error while checking if user has channel role in checkExemption")
		}

		if hasRole {
			matchedKey = channelID
			break
		}
	}

	return hasRole, matchedKey, nil
}

func (e *ExemptionChecker) shortcircuitAnonymous(userID string, exemptionType models.ExemptionType) (canAccess bool, shortcircuit bool) {
	switch exemptionType {
	case models.StaffExemptionType,
		models.SiteAdminExemptionType,
		models.ProductExemptionType,
		models.ChannelVIPExemptionType,
		models.ChannelModeratorExemptionType,
		models.OrganizationMemberExemptionType:
		return false, userID == ""
	default:
		return false, false
	}
}

func (e *ExemptionChecker) incExemptionFailure(exemptionType models.ExemptionType) {
	go func() {
		statName := fmt.Sprintf("backend.exemption-checker.error.%s", exemptionType.String())
		e.Stats.Inc(statName, 1, 1.0)
	}()
}

func (e *ExemptionChecker) isUserExemptViaMembershipChecking(
	ctx context.Context,
	viewerID string,
	exemption *models.Exemption,
) (bool, string, error) {
	if viewerID == "" {
		return false, "", nil
	}

	validationMap, err := e.Clients.Rbac.ValidateUserMembershipInCompanies(ctx, viewerID, exemption.Keys)
	if err != nil || validationMap == nil {
		return false, "", err
	}

	for companyID, isValid := range validationMap {
		if isValid {
			return true, companyID, nil
		}
	}

	return false, "", nil
}
