package helpers

import (
	"testing"

	"code.justin.tv/commerce/nioh/internal/models"
	"code.justin.tv/commerce/nioh/internal/testdata"
	. "github.com/smartystreets/goconvey/convey"
)

func TestContentAttributeHelpers(t *testing.T) {
	player1 := testdata.ContentAttribute("p1", PlayerKey, "player 1", "p1")
	player2 := testdata.ContentAttribute("p2", PlayerKey, "player 2", "p2")
	player3 := testdata.ContentAttribute("p3", PlayerKey, "player 3", "p3")

	team1 := testdata.ContentAttribute("t1", TeamKey, "team 1", "t1")

	streamType1 := testdata.ContentAttribute("st1", StreamTypeKey, "stream type 1", "st1")
	streamType2 := testdata.ContentAttribute("st2", StreamTypeKey, "stream type 1", "st1")

	fallbackTitle := testdata.ContentAttribute("ft1", TitleKey, "fallback title", "ft")

	role1 := testdata.ContentAttribute("r1", RoleKey, "role 1", "r1")
	role2 := testdata.ContentAttribute("r2", RoleKey, "role 2", "r2")

	chanlet := &models.Chanlet{
		OwnerChannelID:    testdata.Channel1ID,
		ChanletID:         testdata.Channel2ID,
		ContentAttributes: []*models.ContentAttribute{},
	}

	Convey("Test GetComputedContentAttributes", t, func() {

		Convey("with invalid input", func() {
			var actual []*models.ContentAttribute
			Convey("nil chanlet", func() {
				actual = GetComputedContentAttributes(nil)
			})

			Convey("no content attributes", func() {
				chanlet := &models.Chanlet{}
				actual = GetComputedContentAttributes(chanlet)
			})

			So(actual, ShouldBeEmpty)
		})

		Convey("display title", func() {
			Convey("when there are title elements", func() {
				Convey("should use player names", func() {
					Convey("with one player", func() {
						chanlet.ContentAttributes = []*models.ContentAttribute{player1}
						computedAttributes := GetComputedContentAttributes(chanlet)

						So(len(computedAttributes), ShouldBeGreaterThanOrEqualTo, 1)
						displayTitle := getAttributeByKey(computedAttributes, DisplayTitleKey)
						So(displayTitle, ShouldNotBeNil)
						So(displayTitle.Value, ShouldContainSubstring, player1.Value)
					})

					Convey("with multiple players", func() {
						chanlet.ContentAttributes = []*models.ContentAttribute{player1, player2, player3}
						computedAttributes := GetComputedContentAttributes(chanlet)

						So(len(computedAttributes), ShouldBeGreaterThanOrEqualTo, 1)
						displayTitle := getAttributeByKey(computedAttributes, DisplayTitleKey)
						So(displayTitle, ShouldNotBeNil)
						So(displayTitle.Value, ShouldContainSubstring, player1.Value)
						So(displayTitle.Value, ShouldContainSubstring, player2.Value)
						So(displayTitle.Value, ShouldContainSubstring, player3.Value)

					})

					Convey("should ignore fallback title", func() {
						chanlet.ContentAttributes = []*models.ContentAttribute{player1, fallbackTitle}
						computedAttributes := GetComputedContentAttributes(chanlet)

						So(len(computedAttributes), ShouldBeGreaterThanOrEqualTo, 1)
						displayTitle := getAttributeByKey(computedAttributes, DisplayTitleKey)
						So(displayTitle, ShouldNotBeNil)
						So(displayTitle.Value, ShouldContainSubstring, player1.Value)
						So(displayTitle.Value, ShouldNotContainSubstring, fallbackTitle.Value)
					})
				})

				Convey("should use stream types", func() {
					Convey("with one stream type", func() {
						chanlet.ContentAttributes = []*models.ContentAttribute{streamType1}
						computedAttributes := GetComputedContentAttributes(chanlet)

						So(len(computedAttributes), ShouldBeGreaterThan, 0)
						displayTitle := getAttributeByKey(computedAttributes, DisplayTitleKey)
						So(displayTitle, ShouldNotBeNil)
						So(displayTitle.Value, ShouldContainSubstring, streamType1.ValueShortname)
					})

					Convey("with multiple stream types", func() {
						chanlet.ContentAttributes = []*models.ContentAttribute{streamType1, streamType2}
						computedAttributes := GetComputedContentAttributes(chanlet)

						So(len(computedAttributes), ShouldBeGreaterThan, 0)
						displayTitle := getAttributeByKey(computedAttributes, DisplayTitleKey)
						So(displayTitle, ShouldNotBeNil)
						So(displayTitle.Value, ShouldContainSubstring, streamType1.ValueShortname)
						So(displayTitle.Value, ShouldContainSubstring, streamType2.ValueShortname)
					})

					Convey("should ignore fallback title", func() {
						chanlet.ContentAttributes = []*models.ContentAttribute{streamType1, fallbackTitle}
						computedAttributes := GetComputedContentAttributes(chanlet)

						So(len(computedAttributes), ShouldBeGreaterThan, 0)
						displayTitle := getAttributeByKey(computedAttributes, DisplayTitleKey)
						So(displayTitle, ShouldNotBeNil)
						So(displayTitle.Value, ShouldContainSubstring, streamType1.ValueShortname)
						So(displayTitle.Value, ShouldNotContainSubstring, fallbackTitle.Value)
					})
				})

				Convey("when there are no title elements", func() {
					Convey("with a fallback title", func() {
						chanlet.ContentAttributes = []*models.ContentAttribute{fallbackTitle}
						computedAttributes := GetComputedContentAttributes(chanlet)

						So(len(computedAttributes), ShouldBeGreaterThanOrEqualTo, 1)
						fallbackTitle := getAttributeByKey(computedAttributes, DisplayTitleKey)
						So(fallbackTitle, ShouldNotBeNil)
						So(fallbackTitle.Value, ShouldContainSubstring, fallbackTitle.Value)
					})

					Convey("with no fallback title", func() {
						chanlet.ContentAttributes = []*models.ContentAttribute{}
						computedAttributes := GetComputedContentAttributes(chanlet)

						So(getAttributeByKey(computedAttributes, TitleKey), ShouldBeNil)
					})
				})

			})
		})

		Convey("icon", func() {
			Convey("when there are icon elements", func() {
				player1.ImageURL = "http://player/1"
				Convey("should use a player icon if it is available", func() {
					chanlet.ContentAttributes = []*models.ContentAttribute{player1}
					computedAttributes := GetComputedContentAttributes(chanlet)
					So(len(computedAttributes), ShouldBeGreaterThan, 0)

					icon := getAttributeByKey(computedAttributes, IconKey)
					So(icon, ShouldNotBeNil)
					So(icon.ImageURL, ShouldEqual, player1.ImageURL)
				})

				Convey("should use a team icon if it is available", func() {
					chanlet.ContentAttributes = []*models.ContentAttribute{player1, team1}
					computedAttributes := GetComputedContentAttributes(chanlet)
					So(len(computedAttributes), ShouldBeGreaterThan, 0)

					icon := getAttributeByKey(computedAttributes, IconKey)
					So(icon, ShouldNotBeNil)
					So(icon.ImageURL, ShouldEqual, team1.ImageURL)
				})

				Convey("should use a stream type icon if it is available", func() {
					chanlet.ContentAttributes = []*models.ContentAttribute{player1, streamType1, team1}
					computedAttributes := GetComputedContentAttributes(chanlet)
					So(len(computedAttributes), ShouldBeGreaterThan, 0)

					icon := getAttributeByKey(computedAttributes, IconKey)
					So(icon, ShouldNotBeNil)
					So(icon.ImageURL, ShouldEqual, streamType1.ImageURL)
				})
			})

			Convey("when there are no icon elements", func() {
				chanlet.ContentAttributes = []*models.ContentAttribute{}
				computedAttributes := GetComputedContentAttributes(chanlet)

				So(getAttributeByKey(computedAttributes, IconKey), ShouldBeNil)
			})
		})

		Convey("secondary icons", func() {
			Convey("when there is secondary icon elements", func() {
				chanlet.ContentAttributes = []*models.ContentAttribute{role1}
				computedAttributes := GetComputedContentAttributes(chanlet)

				So(len(computedAttributes), ShouldBeGreaterThanOrEqualTo, 1)
				secondaryIcon := getAttributeByKey(computedAttributes, SecondaryIconKey)
				So(secondaryIcon, ShouldNotBeNil)
				So(secondaryIcon.ImageURL, ShouldEqual, role1.ImageURL)
			})

			Convey("when there are multiple secondary icon elements", func() {
				chanlet.ContentAttributes = []*models.ContentAttribute{role1, role2}
				computedAttributes := GetComputedContentAttributes(chanlet)

				So(len(computedAttributes), ShouldBeGreaterThanOrEqualTo, 2)
			})

			Convey("when there are no secondary icon elements", func() {
				chanlet.ContentAttributes = []*models.ContentAttribute{}
				computedAttributes := GetComputedContentAttributes(chanlet)

				So(getAttributeByKey(computedAttributes, SecondaryIconKey), ShouldBeNil)
			})
		})
	})
}

func getAttributeByKey(attributes []*models.ContentAttribute, key string) *models.ContentAttribute {
	for _, attribute := range attributes {
		if attribute.AttributeKey == key {
			return attribute
		}
	}

	return nil
}
