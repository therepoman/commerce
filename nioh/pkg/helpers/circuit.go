package helpers

import (
	"time"

	"github.com/cactus/go-statsd-client/statsd"
	"github.com/cep21/circuit"
	"github.com/cep21/circuit/closers/hystrix"
	"github.com/cep21/circuit/metrics/statsdmetrics"
)

// CircuitInput is used as an input for MakeCircuit. All fields are expected to be non-zero value.
type CircuitInput struct {
	Name                  string
	Timeout               time.Duration
	SleepWindow           time.Duration
	VolumeThreshold       int64
	ErrorPercentThreshold int64
	MaxConcurrentRequest  int64
	Statter               statsd.Statter
}

// MakeCircuitManager returns a CircuitManager with the statsd metrics collector
func MakeCircuitManager(statter statsd.Statter) *circuit.Manager {
	// For test convenience.
	if statter == nil {
		return &circuit.Manager{}
	}

	commandFactory := statsdmetrics.CommandFactory{
		StatSender: statter,
	}

	return &circuit.Manager{
		DefaultCircuitProperties: []circuit.CommandPropertiesConstructor{commandFactory.CommandProperties},
	}
}

// MakeCircuit creates a circuit based on the input and returns the pointer to it
func MakeCircuit(manager *circuit.Manager, input *CircuitInput) *circuit.Circuit {
	config := circuit.Config{
		Metrics: circuit.MetricsCollectors{},
		General: circuit.GeneralConfig{
			OpenToClosedFactory: hystrix.CloserFactory(hystrix.ConfigureCloser{
				SleepWindow: input.SleepWindow,
			}),
			ClosedToOpenFactory: hystrix.OpenerFactory(hystrix.ConfigureOpener{
				RequestVolumeThreshold:   input.VolumeThreshold,
				ErrorThresholdPercentage: input.ErrorPercentThreshold,
			}),
		},
		Execution: circuit.ExecutionConfig{
			Timeout:               input.Timeout,
			MaxConcurrentRequests: input.MaxConcurrentRequest,
		},
		Fallback: circuit.FallbackConfig{
			MaxConcurrentRequests: input.MaxConcurrentRequest,
		},
	}

	return manager.MustCreateCircuit(input.Name, config)
}
