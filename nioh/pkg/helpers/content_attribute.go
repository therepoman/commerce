package helpers

import (
	"strings"
	"time"

	"code.justin.tv/commerce/nioh/internal/models"
	uuid "github.com/satori/go.uuid"
)

// !!!!!! WARNING !!!!!!
// Computed content attributes are read and used to display chanlet information.
// Those keys/names should match the keys used in Twilight and other clients.
// If updated, make sure to also update references in Twilight and notify other client teams.
// For Twilight changes, look for references in pages/dashboard/multiview-page and pages/multiview-stream components.
const (
	DisplayTitleKey  = "displayTitle"
	DisplayTitleName = "Display Title"

	TitleKey  = "title"
	TitleName = "Title"

	IconKey  = "icon"
	IconName = "Icon"

	SecondaryIconKey  = "secondaryIcon"
	SecondaryIconName = "Secondary Icon"

	StreamTypeKey = "streamType"
	TeamKey       = "team"

	PlayerKey = "player"
	RoleKey   = "role"

	TitleElementsSeparator = " / "
)

// GetComputedContentAttributes returns computed content attributes based on the
// existing content attributes attached to a chanlet, if any.
func GetComputedContentAttributes(chanlet *models.Chanlet) []*models.ContentAttribute {
	if chanlet == nil || len(chanlet.ContentAttributes) == 0 {
		return []*models.ContentAttribute{}
	}

	attributesByKey := make(map[string][]*models.ContentAttribute)

	for _, attribute := range chanlet.ContentAttributes {
		if _, ok := attributesByKey[attribute.AttributeKey]; !ok {
			attributesByKey[attribute.AttributeKey] = make([]*models.ContentAttribute, 0)
		}
		attributesByKey[attribute.AttributeKey] = append(attributesByKey[attribute.AttributeKey], attribute)
	}

	computedContentAttributes := make([]*models.ContentAttribute, 0)

	// compute display title content attribute
	title := computeDisplayTitle(chanlet, attributesByKey)
	if title != nil {
		computedContentAttributes = append(computedContentAttributes, title)
	}

	// compute icon content attribute
	icon := computeIcon(attributesByKey)
	if icon != nil {
		computedContentAttributes = append(computedContentAttributes, icon)
	}

	// compute secondary icon content attribute
	secondaryIcons := computeSecondaryIcons(attributesByKey)
	if secondaryIcons != nil {
		computedContentAttributes = append(computedContentAttributes, secondaryIcons...)
	}

	return computedContentAttributes
}

func computeDisplayTitle(chanlet *models.Chanlet, attributesByKey map[string][]*models.ContentAttribute) *models.ContentAttribute {
	var titleElements []string

	// include all players
	if players, ok := attributesByKey[PlayerKey]; ok {
		for _, player := range players {
			if player.Value != "" {
				titleElements = append(titleElements, player.Value)
			}
		}
	}

	// include all stream types
	if streamTypes, ok := attributesByKey[StreamTypeKey]; ok {
		for _, streamType := range streamTypes {
			if streamType.ValueShortname != "" {
				titleElements = append(titleElements, streamType.ValueShortname)
			}
		}
	}

	// use the fallback title field set by the broadcaster if no title elements are found
	if len(titleElements) == 0 {
		if title, ok := attributesByKey[TitleKey]; ok {
			if len(title) > 0 && title[0].Value != "" {
				titleElements = append(titleElements, title[0].Value)
			}
		}
	}

	if len(titleElements) > 0 {
		return &models.ContentAttribute{
			OwnerChannelID: chanlet.OwnerChannelID,
			ID:             uuid.NewV4().String(),
			AttributeKey:   DisplayTitleKey,
			AttributeName:  DisplayTitleName,
			Value:          strings.Join(titleElements, TitleElementsSeparator),
			CreatedAt:      time.Now(),
			UpdatedAt:      time.Now(),
		}
	}
	return nil
}

func computeIcon(attributesByKey map[string][]*models.ContentAttribute) *models.ContentAttribute {
	attributeKeysByPriorityDesc := []string{StreamTypeKey, TeamKey, PlayerKey}

	for _, key := range attributeKeysByPriorityDesc {
		if attributes, ok := attributesByKey[key]; ok {
			for _, attribute := range attributes {
				if attribute.ImageURL != "" {
					return &models.ContentAttribute{
						OwnerChannelID: attribute.OwnerChannelID,
						ID:             uuid.NewV4().String(),
						AttributeKey:   IconKey,
						AttributeName:  IconName,
						Value:          attribute.Value,
						ValueShortname: attribute.ValueShortname,
						CreatedAt:      time.Now(),
						UpdatedAt:      time.Now(),
						ImageURL:       attribute.ImageURL,
					}
				}
			}
		}
	}
	return nil
}

func computeSecondaryIcons(attributesByKey map[string][]*models.ContentAttribute) []*models.ContentAttribute {
	secondaryIcons := make([]*models.ContentAttribute, 0)
	if roles, ok := attributesByKey[RoleKey]; ok {
		for _, role := range roles {
			if role.ImageURL != "" {
				secondaryIcons = append(secondaryIcons, &models.ContentAttribute{
					OwnerChannelID: role.OwnerChannelID,
					ID:             uuid.NewV4().String(),
					AttributeKey:   SecondaryIconKey,
					AttributeName:  SecondaryIconName,
					Value:          role.Value,
					ValueShortname: role.ValueShortname,
					CreatedAt:      time.Now(),
					UpdatedAt:      time.Now(),
					ImageURL:       role.ImageURL,
				})
			}
		}
	}
	return secondaryIcons
}
