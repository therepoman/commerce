package helpers

import (
	"context"
)

type contextKey string

var (
	accessInfoContextKey = contextKey("access-info")
	channelIDContextKey  = contextKey("channel-id")
	userIDContextKey     = contextKey("user-id")
)

// AccessInfo stores the relevant access control information for a user
type AccessInfo struct {
	IsStaff       bool
	IsAllowlisted bool
	IsLowRisk     bool
	CanAccess     bool
}

// ChannelIDFromContext returns the channel ID from a context variable
func ChannelIDFromContext(ctx context.Context) (string, bool) {
	userID, ok := ctx.Value(channelIDContextKey).(string)
	return userID, ok
}

// ContextWithChannelID returns a context with channel ID stored in it
func ContextWithChannelID(ctx context.Context, channelID string) context.Context {
	return context.WithValue(ctx, channelIDContextKey, channelID)
}

// UserIDFromContext returns the user ID from a context variable
func UserIDFromContext(ctx context.Context) (string, bool) {
	userID, ok := ctx.Value(userIDContextKey).(string)
	return userID, ok
}

// ContextWithUser returns a context with user ID stored in it
func ContextWithUser(ctx context.Context, userID string) context.Context {
	return context.WithValue(ctx, userIDContextKey, userID)
}

// AccessInfoFromContext returns the Access Info from a context variable
func AccessInfoFromContext(ctx context.Context) AccessInfo {
	return ctx.Value(accessInfoContextKey).(AccessInfo)
}

// ContextWithAccessInfo returns a context with Access Info stored in it
func ContextWithAccessInfo(ctx context.Context, accessInfo AccessInfo) context.Context {
	return context.WithValue(ctx, accessInfoContextKey, accessInfo)
}

// AccessAllowed returns whether or not a user has access based on the context
func AccessAllowed(ctx context.Context) bool {
	return AccessInfoFromContext(ctx).CanAccess
}
