package dynamodb

import (
	"net/http"
	"time"

	"code.justin.tv/commerce/nioh/config"
	"github.com/aws/aws-dax-go/dax"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	db "github.com/guregu/dynamo"
)

const (
	localEnvironmentPrefix       = "dev"
	developmentEnvironmentPrefix = "development"

	tableNamePrefixSeperator = "-"
)

// New Creates a new client that interacts with either DynamoDB or DAX
func New(cfg config.Configuration, useDax bool) (*db.DB, error) {
	if useDax {
		return newDax(cfg)
	}

	sess, err := session.NewSession(&aws.Config{
		Region: aws.String(cfg.AWSRegion),
		HTTPClient: &http.Client{
			Transport: &http.Transport{
				MaxConnsPerHost:     1000,
				MaxIdleConnsPerHost: 1000,
				MaxIdleConns:        1000,
			},
		},
	})
	if err != nil {
		return nil, err
	}
	return db.New(sess), nil
}

func newDax(cfg config.Configuration) (*db.DB, error) {
	daxConfig := dax.DefaultConfig()
	daxConfig.HostPorts = []string{cfg.DAXClusterEndpoint}
	daxConfig.Region = cfg.AWSRegion
	daxConfig.ReadRetries = 1 // default is 2
	daxConfig.WriteRetries = 1
	daxConfig.RequestTimeout = time.Second * 1 // default is 1 minute
	daxConfig.MaxPendingConnectionsPerHost = 100
	apiIface, err := dax.New(daxConfig)

	if err != nil {
		return nil, err
	}
	return db.NewFromIface(apiIface), nil
}

// FormatTableName is a helper method to get the DDB table name
func FormatTableName(prefix string, name string) string {
	// Local environment defaults to "dev"
	if prefix == localEnvironmentPrefix {
		prefix = developmentEnvironmentPrefix
	}
	return prefix + tableNamePrefixSeperator + name
}
