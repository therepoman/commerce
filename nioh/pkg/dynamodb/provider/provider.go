package provider

import (
	"context"
	"net/http"

	"github.com/guregu/dynamo"
)

// Provider allows for ahead-of-time registration of different DynamoDB clients and dynamic retrieval of those clients
// based on values stored within a request context. This is useful for allowing conditional usage of DAX, emergency
// bypass, and dynamic fallback to direct Dynamo reads.
type Provider interface {
	RegisterClient(clientKey, *dynamo.DB)
	GetClient(context.Context) *dynamo.DB
	RegisterFallbackClient(*dynamo.DB)
	GetFallbackClient(context.Context) *dynamo.DB
}

type providerImpl struct {
	fallbackClient *dynamo.DB
	clients        map[clientKey]*dynamo.DB
}

var _ Provider = (*providerImpl)(nil)

// New allocates and returns a Provider implementation ready for use.
func New() Provider {
	return &providerImpl{
		clients: make(map[clientKey]*dynamo.DB),
	}
}

func (p *providerImpl) RegisterFallbackClient(client *dynamo.DB) {
	if client == nil {
		panic("registered nil client as fallback")
	}

	p.fallbackClient = client
}

func (p *providerImpl) GetFallbackClient(context.Context) *dynamo.DB {
	return p.fallbackClient
}

func (p *providerImpl) RegisterClient(key clientKey, client *dynamo.DB) {
	if client == nil {
		panic("cannot register nil client")
	}

	p.clients[key] = client
}

func (p *providerImpl) GetClient(ctx context.Context) *dynamo.DB {
	if isFallbackEngaged(ctx) {
		return p.fallbackClient
	}

	desiredClientKey, keyOk := ctx.Value(clientContextKey).(clientKey)

	var desiredClient *dynamo.DB
	var clientExists bool
	if keyOk {
		desiredClient, clientExists = p.clients[desiredClientKey]
	}

	if !(keyOk && clientExists) {
		return p.fallbackClient
	}

	return desiredClient
}

// DynamoProviderMiddleware returns a function can be used as request middleware on a request path.
// Parameter `useDax` determines whether requests are marked to prefer the DAX client. When false,
// request contexts will be marked to prefer the direct DynamoDB client.
// TODO: make this able to read `useDax` equivalent from dynamicconfig
func DynamoProviderMiddleware(useDax bool) func(http.Handler) http.Handler {
	return func(h http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			ctx := r.Context()

			// Add support for dynamic fallback setting on context.
			// This requires an ahead-of-time allocation of a value within the context.
			ctx = WithDynamicFallback(ctx)

			desiredClientType := DynamoKey
			if useDax {
				desiredClientType = DaxKey
			}

			ctx = SetClientOnContext(ctx, desiredClientType)
			r = r.WithContext(ctx)

			h.ServeHTTP(w, r)
		})
	}
}
