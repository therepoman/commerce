package provider

import (
	"context"
	"net/http"
	"testing"

	"github.com/guregu/dynamo"
	"github.com/stretchr/testify/assert"
)

func TestProviderNew(t *testing.T) {
	p := New()
	assert.NotNil(t, p)
}

func TestProviderFallbackClient(t *testing.T) {
	p := New()
	db := new(dynamo.DB)

	p.RegisterFallbackClient(db)

	assert.Equal(t, db, p.GetFallbackClient(context.Background()), "registered fallback client should return")
}

func TestProviderFallbackClientPanic(t *testing.T) {
	p := New()

	assert.Panics(t, func() {
		p.RegisterFallbackClient((*dynamo.DB)(nil))
	}, "registering a nil fallback client should panic")
}

func TestProviderGetClients(t *testing.T) {
	p := New()

	ddb := new(dynamo.DB)
	dax := new(dynamo.DB)
	fallback := new(dynamo.DB)

	p.RegisterClient(DynamoKey, ddb)
	p.RegisterClient(DaxKey, dax)
	p.RegisterFallbackClient(fallback)

	blankCtx := context.Background()
	ddbCtx := context.WithValue(blankCtx, clientContextKey, DynamoKey)
	daxCtx := context.WithValue(blankCtx, clientContextKey, DaxKey)
	unknownCtx := context.WithValue(blankCtx, clientContextKey, clientKey{"unknown"})

	assert.Equal(t, fallback, p.GetClient(blankCtx))
	assert.Equal(t, ddb, p.GetClient(ddbCtx))
	assert.Equal(t, dax, p.GetClient(daxCtx))
	assert.Equal(t, fallback, p.GetClient(unknownCtx))
}

func TestProviderClientPanic(t *testing.T) {
	p := New()

	assert.Panics(t, func() {
		p.RegisterClient(DynamoKey, (*dynamo.DB)(nil))
	}, "registering a nil client should panic")
}

type stubHandler func(r *http.Request)

func (d stubHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	d(r)
}

func TestDynamoProviderMiddleware(t *testing.T) {
	testCases := []struct {
		description string
		useDax      bool
		expected    clientKey
	}{
		{
			description: "with useDax=true",
			useDax:      true,
			expected:    DaxKey,
		},
		{
			description: "with useDax=false",
			useDax:      false,
			expected:    DynamoKey,
		},
	}

	for _, tt := range testCases {
		t.Run(tt.description, func(t *testing.T) {
			mw := DynamoProviderMiddleware(tt.useDax)
			dummy := stubHandler(func(r *http.Request) {
				assert.Equal(t, tt.expected, r.Context().Value(clientContextKey))
			})

			composed := mw(dummy)
			composed.ServeHTTP(nil, &http.Request{})
		})
	}
}
