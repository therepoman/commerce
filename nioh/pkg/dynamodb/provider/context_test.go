package provider

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestWithDynamicFallback(t *testing.T) {
	rootCtx := context.Background()

	fCtx := WithDynamicFallback(rootCtx)

	assert.IsType(t, new(fallbackStatus), fCtx.Value(fallbackContextKey))

	assert.False(t, isFallbackEngaged(fCtx))
}

func TestEngageFallback(t *testing.T) {
	rootCtx := context.Background()

	assert.False(t, isFallbackEngaged(rootCtx))

	fCtx := WithDynamicFallback(rootCtx)

	EngageFallback(fCtx)
	assert.True(t, isFallbackEngaged(fCtx))
}

func TestDisengageFallback(t *testing.T) {
	rootCtx := context.Background()

	fCtx := WithDynamicFallback(rootCtx)
	EngageFallback(fCtx)

	DisengageFallback(fCtx)
	assert.False(t, isFallbackEngaged(fCtx))
}

func TestIsFallbackEngagedWithNil(t *testing.T) {
	ctx := context.WithValue(context.Background(), fallbackContextKey, (*fallbackStatus)(nil))
	assert.False(t, isFallbackEngaged(ctx))
}
