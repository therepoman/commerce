package provider

import (
	"context"
)

// SetClientOnContext returns a derived context with the passed client key set as that context's preferred client
// in interactions with provider methods.
func SetClientOnContext(ctx context.Context, client clientKey) context.Context {
	return context.WithValue(ctx, clientContextKey, client)
}

type clientContextKeyType string

const clientContextKey clientContextKeyType = "DynamoDB Provider Key"

type clientKey struct {
	raw string
}

var (
	// DaxKey attaches to a request context under the clientContextKey to indicate that Dynamo actions
	// taken while handling that request should prefer to use the DAX client if available.
	DaxKey = clientKey{"dax"}
	// DynamoKey attaches to a request context under the clientContextKey to indicate that Dynamo actions
	// taken while handling that request should prefer to use the direct DynamoDB client if available.
	DynamoKey = clientKey{"dynamo"}
)

type fallbackContextKeyType string

const fallbackContextKey fallbackContextKeyType = "DynamoDB Provider Fallback Status"

type fallbackStatus bool

// WithDynamicFallback returns a copy of ctx that supports a dynamic fallback setting for provider consumers of this
// context. Note that changing the fallback setting is an *interior mutation* of the context and does not create a copy.
//
// `EngageFallback` and `DisengageFallback` should be used to manipulate the dynamic fallback setting.
func WithDynamicFallback(ctx context.Context) context.Context {
	return context.WithValue(ctx, fallbackContextKey, new(fallbackStatus))
}

// EngageFallback attempts to mutate ctx to enable the dynamic fallback setting for provider consumers of this context.
// If ctx is not part of a context tree rooted in a call to WithDynamicFallback, EngageFallback has no effect.
// After EngageFallback is called on a given context, any provider consumers that take that context will use the provider's
// fallback client (instead of whatever client marked as the preference on that context) until DisengageFallback is
// called on that same context.
//
// The return value of EngageFallback indicates whether the fallback was successfully engaged.
func EngageFallback(ctx context.Context) (ok bool) {
	return setFallback(ctx, true)
}

// DisengageFallback attempts to mutate ctx to disable the dynamic fallback setting for provider consumers of this context.
// If ctx is not part of a context tree rooted in a call to WithDynamicFallback, DisengageFallback has no effect.
// After DisengageFallback is called, any provider consumers that take that context will use the client marked as the
// preference for that context (if any).
//
// The return value of DisengageFallback indicates whether the fallback was successfully disengaged.
func DisengageFallback(ctx context.Context) (ok bool) {
	return setFallback(ctx, false)
}

func setFallback(ctx context.Context, value bool) (ok bool) {
	f, ok := ctx.Value(fallbackContextKey).(*fallbackStatus)
	if !ok || f == nil {
		return false
	}

	*f = fallbackStatus(value)
	return ok
}

func isFallbackEngaged(ctx context.Context) bool {
	f, ok := ctx.Value(fallbackContextKey).(*fallbackStatus)
	if !ok || f == nil {
		return false
	}

	return bool(*f)
}
