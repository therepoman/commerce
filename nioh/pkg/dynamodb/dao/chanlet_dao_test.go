package dao

import (
	"context"
	"testing"

	"code.justin.tv/commerce/nioh/pkg/dynamodb/provider"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/nioh/internal/models"
	"code.justin.tv/commerce/nioh/internal/testdata"
	test "code.justin.tv/commerce/nioh/pkg/dynamodb/dao/test"
	"github.com/cactus/go-statsd-client/statsd"
	. "github.com/smartystreets/goconvey/convey"
)

func (d ChanletDAO) CreateTable() error {
	client := d.Provider.GetClient(context.TODO())
	if test.DoesTableExist(d.tableName, client) {
		return nil
	}

	err := client.CreateTable(d.tableName, models.Chanlet{}).
		Provision(test.TestProvisionedReadUnits, test.TestProvisionedWriteUnits).
		Run()

	if err != nil {
		log.WithError(err).Error("Table creation error")
	}

	return err
}

func (d ChanletDAO) DeleteTable() error {
	return d.table(context.TODO()).DeleteTable().Run()
}

func TestChanletTable(t *testing.T) {
	ctx := context.Background()
	dynamoContext := test.DynamoTestContext{}
	db, _ := test.CreateTestClient()
	dbProvider := provider.New()
	dbProvider.RegisterFallbackClient(db)
	statter, _ := statsd.NewNoopClient()
	dao := NewChanletDAO(dbProvider, TestCircuitManager(), statter)
	dynamoContext.RegisterDao(dao)
	defer dynamoContext.Cleanup()

	ownerChannelID := "123"
	chanletID := "456"

	Convey("TestAddChanlet", t, func() {
		input := &models.Chanlet{
			OwnerChannelID: ownerChannelID,
			ChanletID:      chanletID,
		}
		err := dao.AddChanlet(ctx, input)
		So(err, ShouldBeNil)

		expected := &models.Chanlet{
			OwnerChannelID: ownerChannelID,
			ChanletID:      chanletID,
		}

		Convey("GetChanlets", func() {
			result, err := dao.GetChanlets(ctx, ownerChannelID)
			So(err, ShouldBeNil)
			So(len(result), ShouldEqual, 1)
			So(result[0], ShouldResemble, expected)
		})

		Convey("GetChanlet", func() {
			result, err := dao.GetChanlet(ctx, ownerChannelID, chanletID)
			So(err, ShouldBeNil)
			So(result, ShouldResemble, expected)
		})

		Convey("GetChanletByID", func() {
			result, err := dao.GetChanletByID(ctx, chanletID)
			So(err, ShouldBeNil)
			So(result, ShouldResemble, expected)
		})

		Convey("GetChanletCount", func() {
			result, err := dao.GetChanletCount(ctx, ownerChannelID)
			So(err, ShouldBeNil)
			So(result, ShouldEqual, 1)
		})

		Convey("ListAllChanlets", func() {
			result, cursor, err := dao.ListAllChanlets(ctx, "", 10)
			So(err, ShouldBeNil)
			So(len(result), ShouldEqual, 1)
			So(result[0], ShouldResemble, expected)
			So(cursor, ShouldBeEmpty)
		})

		Convey("ArchiveChanlet", func() {
			err := dao.ArchiveChanlet(ctx, ownerChannelID, chanletID)
			So(err, ShouldBeNil)

			results, err := dao.GetChanlets(ctx, ownerChannelID)
			So(err, ShouldBeNil)
			So(results, ShouldBeEmpty)

			result, err := dao.GetChanletByID(ctx, chanletID)
			So(err, ShouldBeNil)
			So(result.ArchivedDate, ShouldNotBeNil)
		})

		Convey("DeleteChanlet", func() {
			err := dao.DeleteChanlet(ctx, ownerChannelID, chanletID)
			So(err, ShouldBeNil)

			results, err := dao.GetChanlets(ctx, ownerChannelID)
			So(err, ShouldBeNil)
			So(results, ShouldBeEmpty)

			result, err := dao.GetChanletByID(ctx, chanletID)
			So(err, ShouldBeNil)
			So(result, ShouldBeNil)
		})
	})

	Convey("UpdateChanletContentAttributes", t, func() {
		Convey("with empty owner channel ID", func() {
			_, err := dao.UpdateChanletContentAttributes(ctx, "", "123", []string{})
			So(err, ShouldNotBeNil)
		})

		Convey("with empty chanlet ID", func() {
			_, err := dao.UpdateChanletContentAttributes(ctx, "123", "", []string{})
			So(err, ShouldNotBeNil)
		})

		Convey("with non-existent chanlet", func() {
			_, err := dao.UpdateChanletContentAttributes(ctx, "123", "456", []string{})
			So(err, ShouldNotBeNil)
		})

		Convey("with new content attributes", func() {
			input := &models.Chanlet{
				OwnerChannelID: ownerChannelID,
				ChanletID:      chanletID,
			}
			dao.AddChanlet(ctx, input)

			result, err := dao.UpdateChanletContentAttributes(ctx, input.OwnerChannelID, input.ChanletID, testdata.ContentAttributeIDs())
			So(err, ShouldBeNil)
			So(result.ContentAttributeIDs, ShouldResemble, testdata.ContentAttributeIDs())
		})
	})
}
