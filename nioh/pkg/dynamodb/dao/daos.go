package dao

import (
	"context"
	"fmt"
	"time"

	niohConfig "code.justin.tv/commerce/nioh/config"
	"code.justin.tv/commerce/nioh/pkg/dynamodb"
	dbprovider "code.justin.tv/commerce/nioh/pkg/dynamodb/provider"

	log "code.justin.tv/commerce/logrus"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/cep21/circuit"
	db "github.com/guregu/dynamo"
	"github.com/pkg/errors"
)

// DAOs is a collection of DAOs
type DAOs struct {
	TestTableDAO        ITestTableDAO
	ChanletDAO          IChanletDAO
	ChannelRelationDAO  IChannelRelationDAO
	ContentAttributeDAO IContentAttributeDAO
	RestrictionDAO      IRestrictionDAO
	PreviewDAO          IPreviewDAO
}

// SetupDAOs returns DAOs with configuration
func SetupDAOs(cfg niohConfig.Configuration, cm *circuit.Manager, useDax bool, statter statsd.Statter) (*DAOs, error) {
	dbProvider := dbprovider.New()

	dynamoClient, err := dynamodb.New(cfg, false)
	if err != nil {
		return nil, errors.Wrap(err, "Failed to create DynamoDB client")
	}

	dbProvider.RegisterFallbackClient(dynamoClient)
	dbProvider.RegisterClient(dbprovider.DynamoKey, dynamoClient)

	if useDax {
		daxClient, err := dynamodb.New(cfg, true)
		if err != nil {
			return nil, errors.Wrap(err, "Failed to create DAX client")
		}

		dbProvider.RegisterClient(dbprovider.DaxKey, daxClient)
	}

	testDAO := NewTestTableDAO(dbProvider, cm, statter)
	chanletDAO := NewChanletDAO(dbProvider, cm, statter)
	channelRelationDAO := NewChannelRelationDAO(dbProvider, cm, statter)
	contentAttributeDAO := NewContentAttributeDAO(dbProvider, cm, statter)
	restrictionDAO := NewRestrictionDAO(dbProvider, cm, statter)
	previewDAO := NewPreviewDAO(dbProvider, cm, statter)

	return &DAOs{
		TestTableDAO:        testDAO,
		ChanletDAO:          chanletDAO,
		ChannelRelationDAO:  channelRelationDAO,
		ContentAttributeDAO: contentAttributeDAO,
		RestrictionDAO:      restrictionDAO,
		PreviewDAO:          previewDAO,
	}, nil
}

func filterNoItemError(err error) error {
	if err == db.ErrNotFound {
		return nil
	}
	return err
}

// internalDAO is an internal struct with necessary resources to execute read/writes with fallbacks
type internalDAO struct {
	*tableGetter
	readCircuit         *circuit.Circuit
	fallbackReadCircuit *circuit.Circuit
	writeCircuit        *circuit.Circuit
	statter             statsd.Statter
	statsDimension      string
}

type tableExecutionFunc func(ctx context.Context, table db.Table) error

const (
	daoFallbackStatsFailFmt    = "daos.%s.fallback.fail"
	daoFallbackStatsSuccessFmt = "daos.%s.fallback.success"
)

func (dao *internalDAO) executeReadWithFallback(ctx context.Context, execution tableExecutionFunc) error {
	return dao.executeWithFallback(ctx, dao.readCircuit, dao.statsDimension+".read", execution)
}

func (dao *internalDAO) executeWithFallback(ctx context.Context, circ *circuit.Circuit, dimension string, execution tableExecutionFunc) error {
	err := circ.Execute(ctx, func(ctx context.Context) error {
		start := time.Now()
		err := filterNoItemError(execution(ctx, dao.table(ctx)))
		t := time.Now()

		suffix := ".timing.success"
		if err != nil {
			suffix = ".timing.fail"
		}
		dao.measureTiming(dimension+suffix, t.Sub(start))

		return err
	}, func(ctx context.Context, err error) error {
		fallbackErr := dao.executeFallback(ctx, execution)
		if fallbackErr != nil {
			dao.incrementStat(fmt.Sprintf(daoFallbackStatsFailFmt, dimension), 1)
		} else {
			dao.incrementStat(fmt.Sprintf(daoFallbackStatsSuccessFmt, dimension), 1)
		}
		return fallbackErr
	})

	return err
}

func (dao *internalDAO) executeFallback(ctx context.Context, execution tableExecutionFunc) error {
	if dao.fallbackReadCircuit != nil {
		return dao.fallbackReadCircuit.Execute(ctx, func(ctx context.Context) error {
			return filterNoItemError(execution(ctx, dao.fallbackTable(ctx)))
		}, nil)
	}

	return filterNoItemError(execution(ctx, dao.fallbackTable(ctx)))
}

func (dao *internalDAO) incrementStat(name string, num int64) {
	go func() {
		err := dao.statter.Inc(name, num, 1.0)
		if err != nil {
			log.Debug("dao failed to post stat increment")
		}
	}()
}

func (dao *internalDAO) measureTiming(name string, delta time.Duration) {
	go func() {
		err := dao.statter.Timing(name, delta.Milliseconds(), 1.0)
		if err != nil {
			log.Debug("dao failed to post timing measurement")
		}
	}()
}

// tableGetter provides a client to access the table. The client can either be DAX or DynamoDB.
type tableGetter struct {
	dbprovider.Provider
	tableName string
	memo      map[*db.DB]db.Table
}

func newTableGetter(tableName string, dbProvider dbprovider.Provider) *tableGetter {
	return &tableGetter{
		tableName: tableName,
		Provider:  dbProvider,
		memo:      make(map[*db.DB]db.Table),
	}
}

func (t *tableGetter) table(ctx context.Context) db.Table {
	client := t.GetClient(ctx)
	return client.Table(t.tableName)
}

func (t *tableGetter) fallbackTable(ctx context.Context) db.Table {
	client := t.GetFallbackClient(ctx)
	return client.Table(t.tableName)
}
