package dao

import (
	"context"

	"code.justin.tv/commerce/nioh/pkg/dynamodb/provider"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/cep21/circuit"
	db "github.com/guregu/dynamo"
)

const (
	testAttributeUserID = "userID"
	testAttributeMSG    = "msg"
)

var testCircuitManager = &circuit.Manager{}

func TestCircuitManager() *circuit.Manager {
	return testCircuitManager
}

// TestData is the Dynamo record
type TestData struct {
	UserID string `dynamo:"userID,hash"` // Hash key
	Msg    string `dynamo:"msg"`
}

// TestTableDAO a random test table
type TestTableDAO struct {
	internalDAO
}

// ITestTableDAO is the interface for the test table
type ITestTableDAO interface {
	GetTestItem(ctx context.Context, userID string) (*TestData, error)
	AddTestItem(ctx context.Context, userID string, msg string) (*TestData, error)
}

// NewTestTableDAO creates a new DAO interface
func NewTestTableDAO(dbProvider provider.Provider, cm *circuit.Manager, statter statsd.Statter) *TestTableDAO {
	return &TestTableDAO{
		internalDAO{
			tableGetter:         newTableGetter("test-table", dbProvider),
			readCircuit:         makeCircuit(cm, testReadCircuit),
			fallbackReadCircuit: makeCircuit(cm, fallbackTestReadCircuit),
			writeCircuit:        makeCircuit(cm, testWriteCircuit),
			statter:             statter,
			statsDimension:      "test",
		},
	}
}

// GetTestItem fetches a row by userID
func (dao TestTableDAO) GetTestItem(ctx context.Context, userID string) (*TestData, error) {
	var result TestData

	err := dao.executeReadWithFallback(ctx, func(ctx context.Context, table db.Table) error {
		return table.Get(testAttributeUserID, userID).One(&result)
	})

	if err != nil {
		return nil, err
	}
	return &result, nil
}

// AddTestItem adds a message by userID
func (dao TestTableDAO) AddTestItem(ctx context.Context, userID string, msg string) (*TestData, error) {
	var result TestData
	err := dao.writeCircuit.Execute(ctx, func(ctx context.Context) error {
		return dao.table(ctx).Update(testAttributeUserID, userID).Set(testAttributeMSG, msg).Value(&result)
	}, nil)

	if err != nil {
		return nil, err
	}
	return &result, nil
}
