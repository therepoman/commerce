package dao

import (
	"testing"

	"code.justin.tv/commerce/nioh/pkg/dynamodb/provider"
	"github.com/stretchr/testify/assert"
)

func TestNewTableGetter(t *testing.T) {
	tableName := "test-table"
	dbProvider := provider.New()

	tg := newTableGetter(tableName, dbProvider)

	assert.Equal(t, tableName, tg.tableName)
	assert.Equal(t, dbProvider, tg.Provider)
	assert.NotNil(t, tg.memo)
}
