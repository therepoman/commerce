package dao

import (
	"context"
	"testing"
	"time"

	"code.justin.tv/commerce/nioh/pkg/dynamodb/provider"

	. "github.com/smartystreets/goconvey/convey"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/nioh/internal/models"
	dao_test "code.justin.tv/commerce/nioh/pkg/dynamodb/dao/test"
	"github.com/cactus/go-statsd-client/statsd"
)

func (dao *PreviewDAO) CreateTable() error {
	client := dao.GetClient(context.TODO())
	if dao_test.DoesTableExist(dao.tableName, client) {
		return nil
	}

	err := client.CreateTable(dao.tableName, models.Preview{}).
		Provision(dao_test.TestProvisionedReadUnits, dao_test.TestProvisionedWriteUnits).
		Run()

	if err != nil {
		logrus.WithError(err).Error("failed to create test table")
	}

	return err
}

func (dao *PreviewDAO) DeleteTable() error {
	return dao.table(context.TODO()).DeleteTable().Run()
}

func TestPreviewDAO(t *testing.T) {
	ctx := context.Background()
	dynamoCtx := dao_test.DynamoTestContext{}
	db, _ := dao_test.CreateTestClient()
	dbProvider := provider.New()
	dbProvider.RegisterFallbackClient(db)
	statter, _ := statsd.NewNoopClient()
	dao := NewPreviewDAO(dbProvider, TestCircuitManager(), statter)
	dynamoCtx.RegisterDao(dao)
	defer dynamoCtx.Cleanup()

	testPreviewKey := "test-preview-key"

	testPreview := &models.Preview{
		PreviewKey:       testPreviewKey,
		ResourceKey:      "herp",
		UserID:           "derp",
		CreatedAt:        time.Date(2018, time.February, 26, 17, 0, 0, 0, time.UTC),
		ContentExpiresAt: time.Date(2018, time.February, 26, 17, 5, 0, 0, time.UTC),
		ResetAt:          time.Date(2018, time.February, 27, 17, 0, 0, 0, time.UTC),
	}

	Convey("Test PreviewDAO", t, func() {
		Convey("test DeletePreview when Preview does not exist", func() {
			err := dao.DeletePreview(ctx, testPreviewKey)
			So(err, ShouldBeNil)
		})

		Convey("test GetPreview when Preview does not exist", func() {
			Preview, err := dao.GetPreview(ctx, testPreviewKey)
			So(err, ShouldBeNil)
			So(Preview, ShouldBeNil)
		})

		Convey("when a Preview exists", func() {
			err := dao.SetPreview(ctx, testPreview)
			So(err, ShouldBeNil)

			Convey("test GetPreview when Preview exists", func() {
				Preview, err := dao.GetPreview(ctx, testPreviewKey)
				So(err, ShouldBeNil)
				So(Preview, ShouldResemble, testPreview)
			})

			Convey("test SetPreview when Preview already exists", func() {
				err := dao.SetPreview(ctx, testPreview)
				So(err, ShouldBeNil)
			})

			Convey("test DeletePreview", func() {
				err := dao.DeletePreview(ctx, testPreviewKey)
				So(err, ShouldBeNil)

				Preview, err := dao.GetPreview(ctx, testPreviewKey)
				So(err, ShouldBeNil)
				So(Preview, ShouldBeNil)
			})

			// cleanup
			dao.DeletePreview(ctx, testPreviewKey)
		})
	})
}
