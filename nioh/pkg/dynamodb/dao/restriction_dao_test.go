package dao

import (
	"context"
	"testing"
	"time"

	"code.justin.tv/commerce/nioh/pkg/dynamodb/provider"
	"code.justin.tv/commerce/nioh/pkg/helpers"

	. "github.com/smartystreets/goconvey/convey"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/nioh/internal/models"
	dao_test "code.justin.tv/commerce/nioh/pkg/dynamodb/dao/test"
	"github.com/cactus/go-statsd-client/statsd"
)

func (dao *RestrictionDAO) CreateTable() error {
	client := dao.GetClient(context.TODO())
	if dao_test.DoesTableExist(dao.tableName, client) {
		return nil
	}

	err := client.CreateTable(dao.tableName, models.RestrictionV2{}).
		Provision(dao_test.TestProvisionedReadUnits, dao_test.TestProvisionedWriteUnits).
		Run()

	if err != nil {
		logrus.WithError(err).Error("failed to create test table")
	}

	return err
}

func (dao *RestrictionDAO) DeleteTable() error {
	return dao.table(context.TODO()).DeleteTable().Run()
}

func TestRestrictionDAO(t *testing.T) {
	ctx := context.Background()
	dynamoCtx := dao_test.DynamoTestContext{}
	db, _ := dao_test.CreateTestClient()
	dbProvider := provider.New()
	dbProvider.RegisterFallbackClient(db)
	statter, _ := statsd.NewNoopClient()
	circuitConfigurationMap = map[circuitName]helpers.CircuitInput{} // Test Dynamo is too slow for the circuit timeout. Reset to default.
	dao := NewRestrictionDAO(dbProvider, TestCircuitManager(), statter)
	dynamoCtx.RegisterDao(dao)
	defer dynamoCtx.Cleanup()

	testResourceKey := "test-resource-key"

	testRestriction := &models.RestrictionV2{
		ResourceKey:     testResourceKey,
		RestrictionType: models.SubOnlyLiveRestrictionType,
		Exemptions: []*models.Exemption{
			{
				ExemptionType:   models.StaffExemptionType,
				ActiveStartDate: time.Date(2018, time.February, 26, 17, 0, 0, 0, time.UTC),
				ActiveEndDate:   time.Date(2019, time.December, 31, 12, 30, 0, 0, time.UTC),
				Keys:            []string{},
				Actions:         []*models.ExemptionAction{},
			},
			{
				ExemptionType:   models.ProductExemptionType,
				ActiveStartDate: time.Date(2018, time.February, 26, 17, 0, 0, 0, time.UTC),
				ActiveEndDate:   time.Date(2019, time.January, 31, 2, 0, 0, 0, time.UTC),
				Keys:            []string{"689376"},
				Actions:         []*models.ExemptionAction{},
			},
		},
		Options:   []models.RestrictionOption{},
		Geoblocks: []models.Geoblock{},
	}

	Convey("Test RestrictionDAO", t, func() {
		Convey("test DeleteRestriction when restriction does not exist", func() {
			err := dao.DeleteRestriction(ctx, testResourceKey)
			So(err, ShouldBeNil)
		})

		Convey("test GetRestriction when restriction does not exist", func() {
			restriction, err := dao.GetRestriction(ctx, testResourceKey)
			So(err, ShouldBeNil)
			So(restriction, ShouldBeNil)
		})

		Convey("test GetRestrictions when restriction does not exist", func() {
			restrictions, err := dao.GetRestrictions(ctx, []string{testResourceKey})
			So(err, ShouldBeNil)
			So(restrictions, ShouldBeNil)
		})

		Convey("when a restriction exists", func() {
			err := dao.SetRestriction(ctx, testRestriction)
			So(err, ShouldBeNil)

			Convey("test GetRestriction when restriction exists", func() {
				restriction, err := dao.GetRestriction(ctx, testResourceKey)
				So(err, ShouldBeNil)
				So(restriction, ShouldResemble, testRestriction)
			})

			Convey("test GetRestrictions when one restriction exists", func() {
				restrictions, err := dao.GetRestrictions(ctx, []string{testResourceKey, "somekey"})
				So(err, ShouldBeNil)
				So(len(restrictions), ShouldEqual, 1)
				So(restrictions[0].ResourceKey, ShouldEqual, testResourceKey)
			})

			Convey("test SetRestriction when restriction already exists", func() {
				err := dao.SetRestriction(ctx, testRestriction)
				So(err, ShouldBeNil)
			})

			Convey("test DeleteRestriction", func() {
				err := dao.DeleteRestriction(ctx, testResourceKey)
				So(err, ShouldBeNil)

				restriction, err := dao.GetRestriction(ctx, testResourceKey)
				So(err, ShouldBeNil)
				So(restriction, ShouldBeNil)
			})

			// cleanup
			dao.DeleteRestriction(ctx, testResourceKey)
		})
	})
}
