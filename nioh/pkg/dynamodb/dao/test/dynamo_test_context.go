package dao_test

type (
	// DynamoDao interface used for tests for create and delete tables
	DynamoDao interface {
		DeleteTable() error
		CreateTable() error
	}

	// DynamoTestContext custom DAO context
	DynamoTestContext struct {
		Daos []DynamoDao
	}
)

// RegisterDao registers a DAO to the dynamo context
func (context *DynamoTestContext) RegisterDao(dao DynamoDao) error {
	err := dao.CreateTable()
	if err != nil {
		return err
	}

	context.Daos = append(context.Daos, dao)
	return nil
}

// Cleanup deletes the DAOs in the dynamo context
func (context *DynamoTestContext) Cleanup() error {
	for _, dao := range context.Daos {
		err := dao.DeleteTable()
		if err != nil {
			return err
		}
	}

	return nil
}

// Reset deletes and readds DAOs to the dynamo context
func (context *DynamoTestContext) Reset() error {
	for _, dao := range context.Daos {
		err := dao.DeleteTable()
		if err != nil {
			return err
		}
		err = dao.CreateTable()
		if err != nil {
			return err
		}
	}

	return nil
}
