package dao_test

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	db "github.com/guregu/dynamo"
)

const (
	// TestDynamoEndpoint is the local Dynamo host
	TestDynamoEndpoint string = "http://localhost:9001"
	// TestDynamoRegion is the local Dynamo region
	TestDynamoRegion string = "us-east-1"
	// TestProvisionedReadUnits is the provisioned read units for test DynamoDB
	TestProvisionedReadUnits int64 = 10
	// TestProvisionedWriteUnits is the provisioned write units for test DynamoDB
	TestProvisionedWriteUnits int64 = 5
)

// CreateTestClient creates a Dynamo client for the testing environment
func CreateTestClient() (*db.DB, error) {
	sess, err := session.NewSession(&aws.Config{
		Region:   aws.String(TestDynamoRegion),
		Endpoint: aws.String(TestDynamoEndpoint),
	})
	if err != nil {
		return nil, err
	}
	return db.New(sess), nil
}

// DoesTableExist checks if a Dynamo table currently exists within our local environment
func DoesTableExist(tableName string, client *db.DB) bool {
	tables, err := client.ListTables().All()
	if err != nil {
		panic(err)
	}

	for _, preexistingTableName := range tables {
		if preexistingTableName == tableName {
			return true
		}
	}

	return false
}
