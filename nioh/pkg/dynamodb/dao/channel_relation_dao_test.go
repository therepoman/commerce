package dao

import (
	"context"
	"testing"

	"code.justin.tv/commerce/nioh/pkg/dynamodb/provider"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/nioh/internal/models"
	test "code.justin.tv/commerce/nioh/pkg/dynamodb/dao/test"
	"github.com/cactus/go-statsd-client/statsd"
	. "github.com/smartystreets/goconvey/convey"
)

func (d ChannelRelationDAO) CreateTable() error {
	client := d.GetClient(context.TODO())
	if test.DoesTableExist(d.tableName, client) {
		return nil
	}

	err := client.CreateTable(d.tableName, models.ChannelRelation{}).
		Provision(test.TestProvisionedReadUnits, test.TestProvisionedWriteUnits).
		Run()

	if err != nil {
		log.WithError(err).Error("Table creation error")
	}

	return err
}

func (d ChannelRelationDAO) DeleteTable() error {
	return d.table(context.TODO()).DeleteTable().Run()
}

func TestChannelRelationDAO(t *testing.T) {
	ctx := context.Background()
	dynamoContext := test.DynamoTestContext{}
	db, _ := test.CreateTestClient()
	dbProvider := provider.New()
	dbProvider.RegisterFallbackClient(db)
	statter, _ := statsd.NewNoopClient()
	dao := NewChannelRelationDAO(dbProvider, TestCircuitManager(), statter)
	dynamoContext.RegisterDao(dao)
	defer dynamoContext.Cleanup()

	channelID := "123"
	relatedChannelID := "456"
	Convey("TestAddChannelRelation", t, func() {
		input := &models.ChannelRelation{
			ChannelID:        channelID,
			RelatedChannelID: relatedChannelID,
			Relation:         models.RelationParent,
		}
		err := dao.AddChannelRelation(ctx, input)
		So(err, ShouldBeNil)

		incorrectInput := &models.ChannelRelation{
			ChannelID:        channelID,
			RelatedChannelID: relatedChannelID,
			Relation:         "i'm groot",
		}
		err = dao.AddChannelRelation(ctx, incorrectInput)
		So(err, ShouldNotBeNil)

		incorrectInput2 := &models.ChannelRelation{
			ChannelID:        channelID,
			RelatedChannelID: channelID,
			Relation:         models.RelationBorrower,
		}

		err = dao.AddChannelRelation(ctx, incorrectInput2)
		So(err, ShouldNotBeNil)

		Convey("GetChannelRelationByChannelID", func() {
			result, err := dao.GetChannelRelationsByChannelID(ctx, channelID)
			So(err, ShouldBeNil)
			So(result[0].ChannelID, ShouldEqual, channelID)
			So(result[0].RelatedChannelID, ShouldEqual, relatedChannelID)
			So(result[0].Relation, ShouldEqual, models.RelationParent)
		})

		Convey("GetChannelRelationByRelatedChannelID", func() {
			result, err := dao.GetChannelRelationsByRelatedChannelID(ctx, relatedChannelID)
			So(err, ShouldBeNil)
			So(result[0].ChannelID, ShouldEqual, channelID)
			So(result[0].RelatedChannelID, ShouldEqual, relatedChannelID)
			So(result[0].Relation, ShouldEqual, models.RelationParent)
		})

		Convey("DeleteChannelRelation", func() {
			err := dao.DeleteChannelRelation(ctx, channelID, relatedChannelID)
			So(err, ShouldBeNil)
			result, err := dao.GetChannelRelationsByChannelID(ctx, channelID)
			So(err, ShouldBeNil)
			So(result, ShouldBeEmpty)
		})
	})
}
