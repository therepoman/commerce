package dao

import (
	"context"
	"strings"
	"sync"

	"code.justin.tv/commerce/nioh/config"

	"code.justin.tv/commerce/nioh/pkg/dynamodb/provider"

	"code.justin.tv/commerce/nioh/internal/models"
	"code.justin.tv/commerce/nioh/pkg/dynamodb"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/cep21/circuit"
	db "github.com/guregu/dynamo"
	"github.com/hashicorp/go-multierror"
	"github.com/pkg/errors"
)

const (
	contentAttributesTableName     = "content-attributes"
	contentAttributesTableHashKey  = "ownerChannelID"
	contentAttributesTableRangeKey = "contentAttributeID"

	placeholder = "      " // use a placeholder for empty fields, to enforce that the fields are written to DynamoDB
)

// ContentAttributeDAO is the accessor to the content attributes table
type ContentAttributeDAO struct {
	internalDAO
}

// IContentAttributeDAO is the interface for the content attribute table
type IContentAttributeDAO interface {
	CreateContentAttributes(ctx context.Context, attrs []*models.ContentAttribute) (int, error)
	GetContentAttributesForChannel(ctx context.Context, ownerChannelID string) ([]*models.ContentAttribute, error)
	GetContentAttributesByIDs(ctx context.Context, ownerChannelID string, contentAttributeIDs []string) ([]*models.ContentAttribute, error)
	DeleteContentAttributesByIDs(ctx context.Context, ownerChannelID string, contentAttributeIDs []string) (int, error)
	UpdateContentAttributes(ctx context.Context, attrs []*models.ContentAttribute) (int, error)
}

// NewContentAttributeDAO creates a new DAO
func NewContentAttributeDAO(dbProvider provider.Provider, cm *circuit.Manager, statter statsd.Statter) *ContentAttributeDAO {
	return &ContentAttributeDAO{
		internalDAO{
			tableGetter: newTableGetter(
				dynamodb.FormatTableName(config.GetCanonicalEnv(), contentAttributesTableName),
				dbProvider),
			readCircuit:         makeCircuit(cm, contentAttributeReadCircuit),
			fallbackReadCircuit: makeCircuit(cm, fallbackContentAttributeReadCircuit),
			writeCircuit:        makeCircuit(cm, contentAttributeWriteCircuit),
			statter:             statter,
			statsDimension:      "content-attribute",
		},
	}
}

// CreateContentAttributes batch write new content attributes
// returns number of content attributes written
func (dao *ContentAttributeDAO) CreateContentAttributes(ctx context.Context, contentAttributes []*models.ContentAttribute) (int, error) {
	if len(contentAttributes) == 0 {
		return 0, nil
	}

	batchWrite := dao.table(ctx).Batch().Write()
	for _, contentAttribute := range contentAttributes {
		if contentAttribute.OwnerChannelID == "" {
			return 0, errors.Errorf("OwnerChannelID (hash key) cannot be empty %v+", contentAttribute)
		}

		if contentAttribute.ID == "" {
			return 0, errors.Errorf("ID (range key) cannot be empty %v+", contentAttribute)
		}

		batchWrite.Put(serialize(contentAttribute))
	}

	var numWritten int
	err := dao.writeCircuit.Execute(ctx, func(ctx context.Context) error {
		var writeErr error
		numWritten, writeErr = batchWrite.RunWithContext(ctx)
		return writeErr
	}, nil)

	if err != nil {
		return numWritten, errors.Wrap(err, "failed to write content attributes in batch")
	}

	return numWritten, nil
}

// GetContentAttributesForChannel retrieves all content attributes created for the given owner channel
func (dao *ContentAttributeDAO) GetContentAttributesForChannel(ctx context.Context, ownerChannelID string) ([]*models.ContentAttribute, error) {
	if ownerChannelID == "" {
		return nil, errors.New("ownerChannelID is required")
	}

	var attributes []*models.ContentAttribute
	err := dao.executeReadWithFallback(ctx, func(ctx context.Context, table db.Table) error {
		return table.Get(contentAttributesTableHashKey, ownerChannelID).AllWithContext(ctx, &attributes)
	})

	if err != nil {
		return nil, errors.Wrapf(
			err, "failed to retrieve all content attributes for the owner channel %s", ownerChannelID)
	}

	return deserializeAll(attributes), nil
}

// GetContentAttributesByIDs retrieves specific content attributes,
// based on the provided owner channel ID and content attribute IDs
func (dao *ContentAttributeDAO) GetContentAttributesByIDs(ctx context.Context, ownerChannelID string, contentAttributeIDs []string) ([]*models.ContentAttribute, error) {
	if ownerChannelID == "" {
		return nil, errors.New("ownerChannelID is required")
	}

	if len(contentAttributeIDs) == 0 {
		return []*models.ContentAttribute{}, nil
	}

	var attributes []*models.ContentAttribute
	var keys []db.Keyed
	for _, id := range contentAttributeIDs {
		if id == "" {
			return nil, errors.New("content attribute ID is required")
		}
		keys = append(keys, db.Keys{ownerChannelID, id})
	}

	err := dao.executeReadWithFallback(ctx, func(ctx context.Context, table db.Table) error {
		return table.Batch(contentAttributesTableHashKey, contentAttributesTableRangeKey).Get(keys...).AllWithContext(ctx, &attributes)
	})

	if err != nil && err != db.ErrNotFound {
		return nil, errors.Wrapf(
			err, "failed to batch retrieve content attributes for owner channel %s", ownerChannelID)
	}

	return deserializeAll(attributes), nil
}

// DeleteContentAttributesByIDs batch deletes specific content attributes
func (dao *ContentAttributeDAO) DeleteContentAttributesByIDs(ctx context.Context, ownerChannelID string, contentAttributeIDs []string) (int, error) {
	if ownerChannelID == "" {
		return 0, errors.New("ownerChannelID is required")
	}

	if len(contentAttributeIDs) == 0 {
		return 0, nil
	}

	var keys []db.Keyed
	var numDeleted int
	for _, id := range contentAttributeIDs {
		if id == "" {
			return 0, errors.New("content attribute ID is required")
		}
		keys = append(keys, db.Keys{ownerChannelID, id})
	}

	err := dao.writeCircuit.Execute(ctx, func(ctx context.Context) error {
		var deleteError error
		numDeleted, deleteError = dao.table(ctx).Batch(contentAttributesTableHashKey, contentAttributesTableRangeKey).
			Write().Delete(keys...).RunWithContext(ctx)
		return deleteError
	}, nil)

	if err != nil && err != db.ErrNotFound {
		return numDeleted, errors.Wrapf(
			err, "failed to batch delete content attributes for owner channel %s", ownerChannelID)
	}

	return numDeleted, nil
}

// UpdateContentAttributes batch update content attributes
// Returns succeeded and failed updates
func (dao *ContentAttributeDAO) UpdateContentAttributes(ctx context.Context, contentAttributes []*models.ContentAttribute) (int, error) {
	if len(contentAttributes) == 0 {
		return 0, nil
	}

	wg := &sync.WaitGroup{}
	mutex := &sync.Mutex{}

	var multierr *multierror.Error
	numUpdated := 0
	for _, attribute := range contentAttributes {
		wg.Add(1)

		go func(attribute *models.ContentAttribute) {
			defer wg.Done()
			var result models.ContentAttribute
			serialized := serialize(attribute)

			err := dao.writeCircuit.Execute(ctx, func(ctx context.Context) error {
				return dao.table(ctx).Update(contentAttributesTableHashKey, serialized.OwnerChannelID).
					Range(contentAttributesTableRangeKey, serialized.ID).
					Set("attributeName", serialized.AttributeName).
					Set("value", serialized.Value).
					Set("valueShortname", serialized.ValueShortname).
					Set("parentAttributeKey", serialized.ParentAttributeKey).
					Set("parentAttributeID", serialized.ParentAttributeID).
					Set("childAttributeIDs", serialized.ChildAttributeIDs).
					Set("imageURL", serialized.ImageURL).
					Set("updatedAt", serialized.UpdatedAt).
					ValueWithContext(ctx, &result)
			}, nil)

			mutex.Lock()
			if err != nil {
				multierr = multierror.Append(multierr, err)
			} else {
				numUpdated++
			}
			mutex.Unlock()
		}(attribute)
	}

	wg.Wait()
	return numUpdated, multierr.ErrorOrNil()
}

func serialize(attribute *models.ContentAttribute) *models.ContentAttribute {
	return &models.ContentAttribute{
		OwnerChannelID:     attribute.OwnerChannelID,
		ID:                 attribute.ID,
		AttributeKey:       attribute.AttributeKey,
		AttributeName:      attribute.AttributeName,
		Value:              attribute.Value,
		ValueShortname:     placeholderIfEmpty(attribute.ValueShortname),
		ParentAttributeID:  placeholderIfEmpty(attribute.ParentAttributeID),
		ParentAttributeKey: placeholderIfEmpty(attribute.ParentAttributeKey),
		ImageURL:           placeholderIfEmpty(attribute.ImageURL),
		ChildAttributeIDs:  attribute.ChildAttributeIDs,
		CreatedAt:          attribute.CreatedAt,
		UpdatedAt:          attribute.UpdatedAt,
	}
}

func deserializeAll(attributes []*models.ContentAttribute) []*models.ContentAttribute {
	deserialized := make([]*models.ContentAttribute, 0, len(attributes))
	for _, attribute := range attributes {
		deserialized = append(deserialized, deserialize(attribute))
	}
	return deserialized
}

func deserialize(attribute *models.ContentAttribute) *models.ContentAttribute {
	return &models.ContentAttribute{
		OwnerChannelID:     attribute.OwnerChannelID,
		ID:                 attribute.ID,
		AttributeKey:       attribute.AttributeKey,
		AttributeName:      attribute.AttributeName,
		Value:              attribute.Value,
		ValueShortname:     removePlaceholder(attribute.ValueShortname),
		ParentAttributeID:  removePlaceholder(attribute.ParentAttributeID),
		ParentAttributeKey: removePlaceholder(attribute.ParentAttributeKey),
		ImageURL:           removePlaceholder(attribute.ImageURL),
		ChildAttributeIDs:  attribute.ChildAttributeIDs,
		CreatedAt:          attribute.CreatedAt,
		UpdatedAt:          attribute.UpdatedAt,
	}
}

func placeholderIfEmpty(s string) string {
	if s == "" {
		return placeholder
	}
	return s
}

func removePlaceholder(s string) string {
	return strings.Replace(s, placeholder, "", 1)
}
