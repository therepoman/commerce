package dao

import (
	"context"
	"testing"

	"code.justin.tv/commerce/nioh/pkg/dynamodb/provider"

	log "code.justin.tv/commerce/logrus"
	test "code.justin.tv/commerce/nioh/pkg/dynamodb/dao/test"
	"github.com/cactus/go-statsd-client/statsd"
	. "github.com/smartystreets/goconvey/convey"
)

func (d TestTableDAO) CreateTable() error {
	client := d.GetClient(context.TODO())
	if test.DoesTableExist(d.table(context.TODO()).Name(), client) {
		return nil
	}

	err := client.CreateTable(d.tableName, TestData{}).
		Provision(test.TestProvisionedReadUnits, test.TestProvisionedWriteUnits).
		Run()

	if err != nil {
		log.WithError(err).Error("Table creation error")
	}

	return err
}

func (d TestTableDAO) DeleteTable() error {
	return d.table(context.TODO()).DeleteTable().Run()
}

func TestTestTable(t *testing.T) {
	ctx := context.Background()
	dynamoContext := test.DynamoTestContext{}
	db, _ := test.CreateTestClient()
	dbProvider := provider.New()
	dbProvider.RegisterFallbackClient(db)
	statter, _ := statsd.NewNoopClient()
	dao := NewTestTableDAO(dbProvider, TestCircuitManager(), statter)
	dynamoContext.RegisterDao(dao)
	defer dynamoContext.Cleanup()

	userID := "123"
	Convey("AddTestItem", t, func() {
		result, err := dao.AddTestItem(ctx, userID, "hello world")
		So(err, ShouldBeNil)
		So(result.Msg, ShouldEqual, "hello world")

		Convey("GetTestItem", func() {
			result, err := dao.GetTestItem(ctx, userID)
			So(err, ShouldBeNil)
			So(result.Msg, ShouldEqual, "hello world")
		})
	})
}
