package dao

import (
	"context"
	"sort"
	"testing"

	"code.justin.tv/commerce/nioh/pkg/dynamodb/provider"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/nioh/internal/models"
	"code.justin.tv/commerce/nioh/internal/testdata"
	test "code.justin.tv/commerce/nioh/pkg/dynamodb/dao/test"
	"github.com/cactus/go-statsd-client/statsd"
	. "github.com/smartystreets/goconvey/convey"
)

func (dao *ContentAttributeDAO) CreateTable() error {
	client := dao.GetClient(context.TODO())
	if test.DoesTableExist(dao.tableName, client) {
		return nil
	}

	err := client.CreateTable(dao.tableName, models.ContentAttribute{}).
		Provision(test.TestProvisionedReadUnits, test.TestProvisionedWriteUnits).
		Run()

	if err != nil {
		log.WithError(err).Error("Table creation error")
	}

	return err
}

func (dao *ContentAttributeDAO) DeleteTable() error {
	return dao.table(context.TODO()).DeleteTable().Run()
}

func TestContentAttributeDAO(t *testing.T) {
	ctx := context.Background()
	dynamoContext := test.DynamoTestContext{}
	db, _ := test.CreateTestClient()
	dbProvider := provider.New()
	dbProvider.RegisterFallbackClient(db)
	statter, _ := statsd.NewNoopClient()
	dao := NewContentAttributeDAO(dbProvider, TestCircuitManager(), statter)
	dynamoContext.RegisterDao(dao)
	defer dynamoContext.Cleanup()

	Convey("Test ContentAttribute DAO", t, func() {

		createInput := testdata.ContentAttributes()

		Convey("CreateContentAttributes", func() {
			Convey("with no item", func() {
				numCreated, err := dao.CreateContentAttributes(ctx, []*models.ContentAttribute{})
				So(err, ShouldBeNil)
				So(numCreated, ShouldEqual, 0)
			})

			Convey("with a single item", func() {
				numCreated, err := dao.CreateContentAttributes(ctx, createInput[0:1])
				defer dynamoContext.Reset()

				So(err, ShouldBeNil)
				So(numCreated, ShouldEqual, 1)
			})

			Convey("with multiple items", func() {
				numCreated, err := dao.CreateContentAttributes(ctx, createInput)
				defer dynamoContext.Reset()

				So(err, ShouldBeNil)
				So(numCreated, ShouldEqual, len(createInput))
			})

			Convey("with empty hash key", func() {
				createInput[0].OwnerChannelID = ""
				_, err := dao.CreateContentAttributes(ctx, createInput)
				So(err, ShouldNotBeNil)
			})

			Convey("with empty range key", func() {
				createInput[0].ID = ""
				_, err := dao.CreateContentAttributes(ctx, createInput)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("GetContentAttributesForChannel", func() {
			_, err := dao.CreateContentAttributes(ctx, createInput)
			So(err, ShouldBeNil)
			defer dynamoContext.Reset()

			Convey("with empty channelID", func() {
				_, err := dao.GetContentAttributesForChannel(ctx, "")
				So(err, ShouldNotBeNil)
			})

			Convey("retrieving multiple items", func() {
				retrieved, err := dao.GetContentAttributesForChannel(ctx, createInput[0].OwnerChannelID)
				So(err, ShouldBeNil)
				So(len(retrieved), ShouldEqual, len(createInput))

				sort.Slice(retrieved, func(i, j int) bool {
					return retrieved[i].ID < retrieved[j].ID
				})
				So(retrieved, ShouldResemble, createInput)
			})

			Convey("retrieving non-existent items", func() {
				attributes, err := dao.GetContentAttributesForChannel(ctx, "thischanneldoesnotexist")
				So(err, ShouldBeNil)
				So(len(attributes), ShouldEqual, 0)
			})
		})

		Convey("BatchGetContentAttributesForChannel", func() {
			_, err := dao.CreateContentAttributes(ctx, createInput)
			So(err, ShouldBeNil)
			defer dynamoContext.Reset()

			Convey("with empty channelID", func() {
				_, err := dao.GetContentAttributesByIDs(ctx, "", []string{})
				So(err, ShouldNotBeNil)
			})

			Convey("retrieving multiple items", func() {
				retrieveIDs := []string{
					createInput[0].ID,
					createInput[1].ID,
					createInput[2].ID,
				}

				retrieved, err := dao.GetContentAttributesByIDs(ctx, createInput[0].OwnerChannelID, retrieveIDs)
				So(err, ShouldBeNil)
				So(len(retrieved), ShouldEqual, len(retrieveIDs))

				sort.Slice(retrieved, func(i, j int) bool {
					return retrieved[i].ID < retrieved[j].ID
				})
				So(createInput[0:3], ShouldResemble, retrieved)

			})

			Convey("retrieving non-existent items", func() {
				retrieveIDs := []string{
					"maybeplayhearthstone",
					"moreblizzheroes",
				}

				retrieved, err := dao.GetContentAttributesByIDs(ctx, createInput[0].OwnerChannelID, retrieveIDs)
				So(err, ShouldBeNil)
				So(len(retrieved), ShouldEqual, 0)
			})

			Convey("with empty content attribute IDs", func() {
				retrieveIDs := []string{
					createInput[0].ID,
					"",
					createInput[2].ID,
				}

				_, err := dao.GetContentAttributesByIDs(ctx, createInput[0].OwnerChannelID, retrieveIDs)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("DeleteContentAttributesByID", func() {

			Convey("with empty owner channel ID", func() {
				_, err := dao.DeleteContentAttributesByIDs(ctx, "", []string{})
				So(err, ShouldNotBeNil)
			})

			Convey("with empty list of IDs", func() {
				numDeleted, err := dao.DeleteContentAttributesByIDs(ctx, testdata.Channel1ID, []string{})
				So(err, ShouldBeNil)
				So(numDeleted, ShouldEqual, 0)
			})

			Convey("with non-existent IDs", func() {
				numDeleted, err := dao.DeleteContentAttributesByIDs(ctx, testdata.Channel1ID, []string{"1", "2", "3"})
				So(err, ShouldBeNil)
				So(numDeleted, ShouldEqual, 3)
			})

			Convey("with existing IDs", func() {
				dao.CreateContentAttributes(ctx, testdata.ContentAttributes())
				defer dynamoContext.Reset()

				numDeleted, err := dao.DeleteContentAttributesByIDs(ctx, testdata.Channel1ID, testdata.ContentAttributeIDs())
				So(err, ShouldBeNil)
				So(numDeleted, ShouldEqual, len(testdata.ContentAttributeIDs()))
			})

			Convey("with empty IDs", func() {
				attributes := testdata.ContentAttributes()
				dao.CreateContentAttributes(ctx, attributes)
				defer dynamoContext.Reset()

				_, err := dao.DeleteContentAttributesByIDs(ctx, testdata.Channel1ID, []string{
					attributes[0].ID,
					"",
					attributes[2].ID,
				})
				So(err, ShouldNotBeNil)
			})
		})

		Convey("UpdateContentAttributes", func() {
			updateInput := testdata.ContentAttributes()

			Convey("with no item", func() {
				numUpdated, err := dao.UpdateContentAttributes(ctx, []*models.ContentAttribute{})
				So(err, ShouldBeNil)
				So(numUpdated, ShouldEqual, 0)
			})

			Convey("with a single item", func() {
				numUpdated, err := dao.UpdateContentAttributes(ctx, updateInput[0:1])
				defer dynamoContext.Reset()

				So(err, ShouldBeNil)
				So(numUpdated, ShouldEqual, 1)
			})

			Convey("with multiple items", func() {
				numUpdated, err := dao.CreateContentAttributes(ctx, updateInput)
				defer dynamoContext.Reset()

				So(err, ShouldBeNil)
				So(numUpdated, ShouldEqual, len(updateInput))
			})

			Convey("with empty hash key", func() {
				updateInput[0].OwnerChannelID = ""
				_, err := dao.CreateContentAttributes(ctx, updateInput)
				So(err, ShouldNotBeNil)
			})

			Convey("with empty range key", func() {
				updateInput[0].ID = ""
				_, err := dao.CreateContentAttributes(ctx, updateInput)
				So(err, ShouldNotBeNil)
			})
		})
	})
}
