package dao

import (
	"context"

	"code.justin.tv/commerce/nioh/config"

	"code.justin.tv/commerce/nioh/pkg/dynamodb/provider"

	"code.justin.tv/commerce/nioh/internal/models"
	"code.justin.tv/commerce/nioh/pkg/dynamodb"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/cep21/circuit"
	db "github.com/guregu/dynamo"
	"github.com/pkg/errors"
)

const (
	previewsTableName = "previews"
	previewsTableKey  = "previewKey"
)

// PreviewDAO implements operations on the previews table.
type PreviewDAO struct {
	internalDAO
}

// IPreviewDAO provides operations on the previews table.
type IPreviewDAO interface {
	GetPreview(ctx context.Context, previewKey string) (*models.Preview, error)
	SetPreview(ctx context.Context, preview *models.Preview) error
	DeletePreview(ctx context.Context, previewKey string) error
}

var _ IPreviewDAO = &PreviewDAO{}

// NewPreviewDAO instantiates a PreviewDAO and returns a pointer to it.
func NewPreviewDAO(dbProvider provider.Provider, cm *circuit.Manager, statter statsd.Statter) *PreviewDAO {
	return &PreviewDAO{
		internalDAO{
			tableGetter: newTableGetter(
				dynamodb.FormatTableName(config.GetCanonicalEnv(), previewsTableName),
				dbProvider,
			),
			readCircuit:         makeCircuit(cm, previewReadCircuit),
			fallbackReadCircuit: makeCircuit(cm, fallbackPreviewReadCircuit),
			writeCircuit:        makeCircuit(cm, previewWriteCircuit),
			statter:             statter,
			statsDimension:      "preview",
		},
	}
}

// GetPreview looks up a preview for the passed review key and returns it if it exists.
// If no preview exists for the passed resource key, a nil *models.Preview is returned, and no error is returned.
func (dao *PreviewDAO) GetPreview(ctx context.Context, previewKey string) (*models.Preview, error) {
	var result *models.Preview

	err := dao.executeReadWithFallback(ctx, func(ctx context.Context, table db.Table) error {
		return table.Get(previewsTableKey, previewKey).OneWithContext(ctx, &result)
	})

	if err != nil {
		return nil, errors.Wrapf(err, "unexpected error while getting preview in PreviewDAO. previewKey=%s", previewKey)
	}

	return result, nil
}

// SetPreview inserts a preview into the preview table if no preview with the same preview key already exists.
// If a preview already exists with the same preview key, the preview is replaced with the given input.
func (dao *PreviewDAO) SetPreview(ctx context.Context, preview *models.Preview) error {
	err := dao.writeCircuit.Execute(ctx, func(ctx context.Context) error {
		return dao.table(ctx).Put(preview).RunWithContext(ctx)
	}, nil)
	if err != nil {
		return errors.Wrapf(err, "unexpected error while inserting preview in PreviewDAO. previewKey=%s", preview.PreviewKey)
	}

	return nil
}

// DeletePreview deletes the preview for the passed resource key if it exists.
// No error is returned if no preview exists for the passed resource key.
func (dao *PreviewDAO) DeletePreview(ctx context.Context, previewKey string) error {
	// Note that Dynamo deletes are idempotent and will not return an error if the item does not exist.
	err := dao.writeCircuit.Execute(ctx, func(ctx context.Context) error {
		return dao.table(ctx).Delete(previewsTableKey, previewKey).RunWithContext(ctx)
	}, nil)
	if err != nil {
		return errors.Wrapf(err, "unexpected error while deleting preview in PreviewDAO. previewKey=%s", previewKey)
	}

	return nil
}
