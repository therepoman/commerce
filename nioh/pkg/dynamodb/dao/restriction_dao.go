package dao

import (
	"context"

	"code.justin.tv/commerce/nioh/config"
	"code.justin.tv/commerce/nioh/internal/models"
	"code.justin.tv/commerce/nioh/pkg/dynamodb"
	"code.justin.tv/commerce/nioh/pkg/dynamodb/provider"

	"github.com/cactus/go-statsd-client/statsd"
	"github.com/cep21/circuit"
	db "github.com/guregu/dynamo"
	"github.com/pkg/errors"
)

const (
	restrictionTableName = "restrictions"
)

// RestrictionDAO implements operations on the restrictions table.
type RestrictionDAO struct {
	internalDAO
}

// IRestrictionDAO provides operations on the restrictions table.
type IRestrictionDAO interface {
	GetRestriction(ctx context.Context, resourceKey string) (*models.RestrictionV2, error)
	SetRestriction(ctx context.Context, restriction *models.RestrictionV2) error
	DeleteRestriction(ctx context.Context, resourceKey string) error
}

var _ IRestrictionDAO = (*RestrictionDAO)(nil)

// NewRestrictionDAO instantiates a RestrictionDAO and returns a pointer to it.
func NewRestrictionDAO(provider provider.Provider, cm *circuit.Manager, statter statsd.Statter) *RestrictionDAO {
	return &RestrictionDAO{
		internalDAO{
			tableGetter: newTableGetter(
				dynamodb.FormatTableName(config.GetCanonicalEnv(), restrictionTableName),
				provider,
			),
			readCircuit:    makeCircuit(cm, restrictionReadCircuit),
			writeCircuit:   makeCircuit(cm, restrictionWriteCircuit),
			statter:        statter,
			statsDimension: "restriction",
		},
	}
}

// GetRestriction looks up a restriction for the passed resource key and returns it if it exists.
// If no restriction exists for the passed resource key, a nil *models.RestrictionV2 is returned, and no error is returned.
func (dao *RestrictionDAO) GetRestriction(ctx context.Context, resourceKey string) (*models.RestrictionV2, error) {
	var result *models.RestrictionV2

	err := dao.executeReadWithFallback(ctx, func(ctx context.Context, table db.Table) error {
		return table.Get("ResourceKey", resourceKey).OneWithContext(ctx, &result)
	})

	if err != nil {
		return nil, errors.Wrapf(err, "unexpected error while getting restriction in RestrictionDAO. resourceKey=%s", resourceKey)
	}

	return result, nil
}

// GetRestrictions looks up multiple restrictions using Batch Get for the passed resource keys.
// Note: It seems that an error is returned even if some of the keys are resolved.
//       dynamo library does not give a granular error report and simply halts the batch iterator if one key chokes.
func (dao *RestrictionDAO) GetRestrictions(ctx context.Context, resourceKeys []string) ([]*models.RestrictionV2, error) {
	var results []*models.RestrictionV2
	keys := make([]db.Keyed, len(resourceKeys))
	for i, rKey := range resourceKeys {
		keys[i] = db.Keys{rKey, nil}
	}
	err := dao.readCircuit.Execute(ctx, func(ctx context.Context) error {
		return filterNoItemError(dao.table(ctx).Batch("ResourceKey").Get(keys...).AllWithContext(ctx, &results))
	}, nil)

	return results, err
}

// SetRestriction inserts a restriction into the restriction table if no restriction with the same resource key already exists.
// If a restriction already exists with the same resource key, the restriction is replaced with the given input.
func (dao *RestrictionDAO) SetRestriction(ctx context.Context, restriction *models.RestrictionV2) error {
	err := dao.writeCircuit.Execute(ctx, func(ctx context.Context) error {
		return dao.table(ctx).Put(restriction).RunWithContext(ctx)
	}, nil)

	if err != nil {
		return errors.Wrapf(err, "unexpected error while inserting restriction in RestrictionDAO. resourceKey=%s", restriction.ResourceKey)
	}

	return nil
}

// DeleteRestriction deletes the restriction for the passed resource key if it exists.
// No error is returned if no restriction exists for the passed resource key.
func (dao *RestrictionDAO) DeleteRestriction(ctx context.Context, resourceKey string) error {
	// Note that Dynamo deletes are idempotent and will not return an error if the item does not exist.
	err := dao.writeCircuit.Execute(ctx, func(ctx context.Context) error {
		return dao.table(ctx).Delete("ResourceKey", resourceKey).RunWithContext(ctx)
	}, nil)

	if err != nil {
		return errors.Wrapf(err, "unexpected error while deleting restriction in RestrictionDAO. resourceKey=%s", resourceKey)
	}

	return nil
}
