package dao

import (
	"context"

	"code.justin.tv/commerce/nioh/config"

	"code.justin.tv/commerce/nioh/pkg/dynamodb/provider"

	"code.justin.tv/commerce/nioh/internal/models"
	"code.justin.tv/commerce/nioh/pkg/dynamodb"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/cep21/circuit"
	db "github.com/guregu/dynamo"
	"github.com/pkg/errors"
)

const (
	channelRelationsTableName        string = "channel-relations"
	channelRelationsChannelID        string = "channelID"
	channelRelationsRelatedChannelID string = "relatedChannelID"

	channelRelationsRelatedChannelIDIndex string = "relatedChannelID-Index"
)

// ChannelRelationDAO is the channel hierarchy DAO
type ChannelRelationDAO struct {
	internalDAO
}

// IChannelRelationDAO is the interface for the channel hierarchy table
type IChannelRelationDAO interface {
	GetChannelRelationsByChannelID(ctx context.Context, channelID string) ([]*models.ChannelRelation, error)
	GetChannelRelationsByRelatedChannelID(ctx context.Context, relatedChannelID string) ([]*models.ChannelRelation, error)
	AddChannelRelation(ctx context.Context, input *models.ChannelRelation) error
	DeleteChannelRelation(ctx context.Context, channelID, relatedChannelID string) error
}

// NewChannelRelationDAO creates a new DAO interface
func NewChannelRelationDAO(dbProvider provider.Provider, cm *circuit.Manager, statter statsd.Statter) *ChannelRelationDAO {
	return &ChannelRelationDAO{
		internalDAO{
			tableGetter:         newTableGetter(dynamodb.FormatTableName(config.GetCanonicalEnv(), channelRelationsTableName), dbProvider),
			readCircuit:         makeCircuit(cm, channelRelationReadCircuit),
			fallbackReadCircuit: makeCircuit(cm, fallbackChannelRelationReadCircuit),
			writeCircuit:        makeCircuit(cm, channelRelationWriteCircuit),
			statter:             statter,
			statsDimension:      "channel-relation",
		},
	}
}

// GetChannelRelationsByChannelID fetches rows by channelID
func (dao *ChannelRelationDAO) GetChannelRelationsByChannelID(ctx context.Context, channelID string) ([]*models.ChannelRelation, error) {
	var result []*models.ChannelRelation
	err := dao.executeReadWithFallback(ctx, func(ctx context.Context, table db.Table) error {
		return table.Get(channelRelationsChannelID, channelID).AllWithContext(ctx, &result)
	})

	if err != nil {
		return nil, err
	}
	return result, nil
}

// GetChannelRelationsByRelatedChannelID fetches rows by relatedChannelID
func (dao *ChannelRelationDAO) GetChannelRelationsByRelatedChannelID(ctx context.Context, relatedChannelID string) ([]*models.ChannelRelation, error) {
	var result []*models.ChannelRelation

	err := dao.executeReadWithFallback(ctx, func(ctx context.Context, table db.Table) error {
		return table.Get(channelRelationsRelatedChannelID, relatedChannelID).Index(channelRelationsRelatedChannelIDIndex).AllWithContext(ctx, &result)
	})

	if err != nil {
		return nil, err
	}
	return result, nil
}

// AddChannelRelation adds a row of hierarchy data
func (dao *ChannelRelationDAO) AddChannelRelation(ctx context.Context, input *models.ChannelRelation) error {
	if input.Relation != models.RelationParent && input.Relation != models.RelationBorrower {
		return errors.New("Relation can only be either PARENT or BORROWER")
	}

	if input.RelatedChannelID == input.ChannelID {
		return errors.New("RelatedChannelID cannot be equal to ChannelID")
	}

	err := dao.writeCircuit.Execute(ctx, func(ctx context.Context) error {
		return dao.table(ctx).Put(input).RunWithContext(ctx)
	}, nil)

	return err
}

// DeleteChannelRelation deletes a channel relation row
func (dao *ChannelRelationDAO) DeleteChannelRelation(ctx context.Context, channelID, relatedChannelID string) error {
	err := dao.writeCircuit.Execute(ctx, func(ctx context.Context) error {
		return dao.table(ctx).Delete(channelRelationsChannelID, channelID).Range(channelRelationsRelatedChannelID, relatedChannelID).RunWithContext(ctx)
	}, nil)

	if err != nil {
		return errors.Wrap(err, "failed to delete channel relation from DynamoDB table")
	}

	return nil
}
