package dao

import (
	"context"
	"fmt"
	"strings"
	"time"

	"code.justin.tv/commerce/nioh/config"
	"code.justin.tv/commerce/nioh/internal/models"
	"code.justin.tv/commerce/nioh/pkg/dynamodb"
	"code.justin.tv/commerce/nioh/pkg/dynamodb/provider"
	db "github.com/guregu/dynamo"

	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/cep21/circuit"
	"github.com/pkg/errors"
)

const (
	chanletsTableName      string = "chanlets"
	chanletsOwnerChannelID string = "ownerChannelID"
	chanletsChanletID      string = "chanletID"
	chanletsArchivedDate   string = "archivedDate"

	chanletsChanletIDIndex string = "chanletID-Index"
)

var timeNil = time.Time{}

// ChanletDAO a chanlet table
type ChanletDAO struct {
	internalDAO
}

// IChanletDAO is the interface for the chanlet table
type IChanletDAO interface {
	GetChanlets(ctx context.Context, ownerChannelID string) ([]*models.Chanlet, error)
	ListAllChanlets(ctx context.Context, cursor string, limit int64) ([]*models.Chanlet, string, error)
	GetChanlet(ctx context.Context, ownerChannelID string, chanletID string) (*models.Chanlet, error)
	GetChanletCount(ctx context.Context, ownerChannelID string) (int64, error)
	GetChanletByID(ctx context.Context, chanletID string) (*models.Chanlet, error)
	AddChanlet(ctx context.Context, input *models.Chanlet) error
	DeleteChanlet(ctx context.Context, ownerChannelID string, chanletID string) error
	UpdateChanletContentAttributes(ctx context.Context, ownerChannelID string, chanletID string, attributeIDs []string) (*models.Chanlet, error)
	ArchiveChanlet(ctx context.Context, ownerChannelID string, chanletID string) error
}

// NewChanletDAO creates a new DAO interface
func NewChanletDAO(dbProvider provider.Provider, cm *circuit.Manager, statter statsd.Statter) *ChanletDAO {
	return &ChanletDAO{
		internalDAO{
			tableGetter: newTableGetter(
				dynamodb.FormatTableName(config.GetCanonicalEnv(), chanletsTableName),
				dbProvider,
			),
			readCircuit:         makeCircuit(cm, chanletReadCircuit),
			fallbackReadCircuit: makeCircuit(cm, fallbackChanletReadCircuit),
			writeCircuit:        makeCircuit(cm, chanletWriteCircuit),
			statter:             statter,
			statsDimension:      "chanlet",
		},
	}
}

// GetChanlets fetches rows by ownerChannelID
func (dao *ChanletDAO) GetChanlets(ctx context.Context, ownerChannelID string) ([]*models.Chanlet, error) {
	var result []*models.Chanlet

	err := dao.executeReadWithFallback(ctx, func(ctx context.Context, table db.Table) error {
		return dao.getChanletsQuery(ctx, table, ownerChannelID).AllWithContext(ctx, &result)
	})

	if err != nil {
		return nil, errors.Wrap(err, "failed to get chanlets from DynamoDB table")
	}
	return result, nil
}

// ListAllChanlets returns a paginated list of all chanlets with scan operation along with the next cursor
func (dao *ChanletDAO) ListAllChanlets(ctx context.Context, cursor string, limit int64) ([]*models.Chanlet, string, error) {
	var result []*models.Chanlet
	var newPagingKey db.PagingKey
	currentPagingKey, err := parseStringToChanletPagingKey(cursor)

	if err != nil {
		return nil, "", errors.Wrap(err, "failed to parse cursor into a paging key")
	}

	err = dao.executeReadWithFallback(ctx, func(ctx context.Context, table db.Table) error {
		// include StartFrom
		newPagingKey, err = table.Scan().StartFrom(currentPagingKey).Limit(limit).AllWithLastEvaluatedKeyContext(ctx, &result)
		return err
	})

	if err != nil {
		return nil, "", errors.Wrap(err, "failed to get chanlets from DynamoDB table")
	}

	nextCursor, err := parseChanletPagingKeyToString(newPagingKey)

	if err != nil {
		return nil, "", errors.Wrap(err, "unexpected paging key format from DynamoDB table")
	}

	return result, nextCursor, nil
}

func parseChanletPagingKeyToString(pagingKey db.PagingKey) (string, error) {
	out := make(map[string]string)
	err := dynamodbattribute.UnmarshalMap(pagingKey, &out)

	if err != nil {
		return "", errors.Wrap(err, "failed to unmarshal paging key to string")
	}

	ownerChannelID := out["ownerChannelID"]
	chanletID := out["chanletID"]

	if ownerChannelID == "" && chanletID == "" {
		return "", nil
	}

	return fmt.Sprintf("%v-%v", ownerChannelID, chanletID), nil
}

func parseStringToChanletPagingKey(pagingKeyStr string) (db.PagingKey, error) {
	if pagingKeyStr == "" {
		return nil, nil
	}

	inputParts := strings.Split(pagingKeyStr, "-")
	if len(inputParts) != 2 {
		return nil, errors.New("wrong cursor paging key format. expects ownerChannelID-chanletID")
	}

	in := make(map[string]string)
	in["ownerChannelID"] = inputParts[0]
	in["chanletID"] = inputParts[1]

	marshalledDynamoDBAttributeMap, err := dynamodbattribute.MarshalMap(in)

	if err != nil {
		return nil, errors.New("failed to marshal cursor string into paging key")
	}

	return marshalledDynamoDBAttributeMap, nil
}

// GetChanlet fetches a specific chanlet row by ownerChannelID and chanletID
// Archived chanlets can be fetched with this
func (dao *ChanletDAO) GetChanlet(ctx context.Context, ownerChannelID string, chanletID string) (*models.Chanlet, error) {
	var result *models.Chanlet

	err := dao.executeReadWithFallback(ctx, func(ctx context.Context, table db.Table) error {
		return table.Get(chanletsOwnerChannelID, ownerChannelID).Range(chanletsChanletID, db.Equal, chanletID).OneWithContext(ctx, &result)
	})

	if err != nil {
		return nil, err
	}
	return result, nil
}

// GetChanletCount fetches the number of chanlets with the given ownerChannelID
func (dao *ChanletDAO) GetChanletCount(ctx context.Context, ownerChannelID string) (int64, error) {
	var count int64

	err := dao.executeReadWithFallback(ctx, func(ctx context.Context, table db.Table) error {
		var queryErr error
		count, queryErr = dao.getChanletsQuery(ctx, table, ownerChannelID).CountWithContext(ctx)
		return queryErr
	})
	return count, err
}

// GetChanletByID fetches a chanlet by the given ID using the global secondary index query. If multiple chanlets exist, returns the first one found
// Archived chanlets can be fetched with this
func (dao *ChanletDAO) GetChanletByID(ctx context.Context, chanletID string) (*models.Chanlet, error) {
	var result []*models.Chanlet

	err := dao.executeReadWithFallback(ctx, func(ctx context.Context, table db.Table) error {
		return table.Get(chanletsChanletID, chanletID).Index(chanletsChanletIDIndex).AllWithContext(ctx, &result)
	})

	if err != nil {
		return nil, err
	}

	if len(result) == 0 {
		return nil, nil
	}

	return result[0], nil
}

// AddChanlet adds a chanlet (via PUT)
func (dao *ChanletDAO) AddChanlet(ctx context.Context, input *models.Chanlet) error {
	err := dao.writeCircuit.Execute(ctx, func(ctx context.Context) error {
		return dao.table(ctx).Put(input).RunWithContext(ctx)
	}, nil)

	if err != nil {
		return errors.Wrap(err, "failed to add a chanlet to DynamoDB table")
	}
	return nil
}

// DeleteChanlet deletes a chanlet
func (dao *ChanletDAO) DeleteChanlet(ctx context.Context, ownerChannelID string, chanletID string) error {
	err := dao.writeCircuit.Execute(ctx, func(ctx context.Context) error {
		return dao.table(ctx).Delete(chanletsOwnerChannelID, ownerChannelID).Range(chanletsChanletID, chanletID).RunWithContext(ctx)
	}, nil)

	if err != nil {
		return errors.Wrap(err, "failed to delete a chanlet from DynamoDB table")
	}
	return nil
}

// ArchiveChanlet archives a chanlet
func (dao *ChanletDAO) ArchiveChanlet(ctx context.Context, ownerChannelID string, chanletID string) error {
	err := dao.writeCircuit.Execute(ctx, func(ctx context.Context) error {
		return dao.table(ctx).Update(chanletsOwnerChannelID, ownerChannelID).
			Range(chanletsChanletID, chanletID).
			Set(chanletsArchivedDate, time.Now()).RunWithContext(ctx)
	}, nil)

	if err != nil {
		return errors.Wrap(err, "failed to archive a chanlet from DynamoDB table")
	}

	return nil
}

// UpdateChanletContentAttributes updates the list of content attributes associated with the chanlet
func (dao *ChanletDAO) UpdateChanletContentAttributes(ctx context.Context, ownerChannelID string, chanletID string, attributeIDs []string) (*models.Chanlet, error) {
	if ownerChannelID == "" {
		return nil, errors.New("ownerChannelID is required")
	}
	if chanletID == "" {
		return nil, errors.New("chanletID is required")
	}

	var result models.Chanlet

	err := dao.writeCircuit.Execute(ctx, func(ctx context.Context) error {
		return dao.table(ctx).Update(chanletsOwnerChannelID, ownerChannelID).
			Range(chanletsChanletID, chanletID).
			Set("contentAttributeIDs", attributeIDs).
			If("attribute_exists(ownerChannelID)").
			ValueWithContext(ctx, &result)
	}, nil)

	if err != nil {
		return nil, errors.Wrap(err, "failed to update content attributes for chanlet in DynamoDB table")
	}

	return &result, nil
}

func (dao *ChanletDAO) getChanletsQuery(ctx context.Context, table db.Table, ownerChannelID string) *db.Query {
	// Filter out archived chanlets
	return table.Get(chanletsOwnerChannelID, ownerChannelID).Filter("attribute_not_exists($) OR $ = ?", chanletsArchivedDate, chanletsArchivedDate, timeNil)
}
