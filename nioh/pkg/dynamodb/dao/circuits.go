package dao

import (
	"time"

	"github.com/cep21/circuit"

	"code.justin.tv/commerce/nioh/pkg/helpers"
)

type circuitName string

const (
	testReadCircuit              circuitName = "dao-test-read"
	testWriteCircuit             circuitName = "dao-test-write"
	chanletReadCircuit           circuitName = "dao-chanlet-read"
	chanletWriteCircuit          circuitName = "dao-chanlet-write"
	channelRelationReadCircuit   circuitName = "dao-channel-relation-read"
	channelRelationWriteCircuit  circuitName = "dao-channel-relation-write"
	contentAttributeReadCircuit  circuitName = "dao-content-attribute-read"
	contentAttributeWriteCircuit circuitName = "dao-content-attribute-write"
	restrictionReadCircuit       circuitName = "dao-restriction-read"
	restrictionWriteCircuit      circuitName = "dao-restriction-write"
	previewReadCircuit           circuitName = "dao-preview-read"
	previewWriteCircuit          circuitName = "dao-preview-write"

	// Circuits used around fallback reads (DynamoDB client)
	fallbackTestReadCircuit             circuitName = "dao-fallback-test-read"
	fallbackChanletReadCircuit          circuitName = "dao-fallback-chanlet-read"
	fallbackChannelRelationReadCircuit  circuitName = "dao-fallback-channel-relation-read"
	fallbackContentAttributeReadCircuit circuitName = "dao-fallback-content-attribute-read"
	fallbackRestrictionReadCircuit      circuitName = "dao-fallback-restriction-read"
	fallbackPreviewReadCircuit          circuitName = "dao-fallback-preview-read"
)

// defaultCircuitConfiguration is the configuration used in the circuit if there is no table specific config found.
var defaultCircuitConfiguration = helpers.CircuitInput{
	Timeout:               400 * time.Millisecond,  // Should be less than 1 second so context has time to retry with fallback
	SleepWindow:           5000 * time.Millisecond, // https://github.com/Netflix/Hystrix/wiki/Configuration#circuitBreaker.sleepWindowInMilliseconds
	VolumeThreshold:       300,
	ErrorPercentThreshold: 50,
	MaxConcurrentRequest:  7500,
}

// circuitConfigurationMap defines circuit configuration for each table's r/w circuit
var circuitConfigurationMap = map[circuitName]helpers.CircuitInput{
	restrictionReadCircuit: {
		Timeout:               50 * time.Millisecond, // GetRestriction has a tighter schedule
		SleepWindow:           1000 * time.Millisecond,
		VolumeThreshold:       1000,
		ErrorPercentThreshold: 100,
		MaxConcurrentRequest:  30000,
	},
	fallbackRestrictionReadCircuit: {
		Timeout:               200 * time.Millisecond, // GetRestriction has a tighter schedule
		SleepWindow:           5000 * time.Millisecond,
		VolumeThreshold:       400,
		ErrorPercentThreshold: 50,
		MaxConcurrentRequest:  7500,
	},
	previewWriteCircuit: {
		Timeout:               1000 * time.Millisecond, // preview write is asynchronous and more disastrous when failed, and thus we afford to spend more time
		SleepWindow:           5000 * time.Millisecond,
		VolumeThreshold:       400,
		ErrorPercentThreshold: 50,
		MaxConcurrentRequest:  5000,
	},
}

func makeCircuit(cm *circuit.Manager, name circuitName) *circuit.Circuit {
	input, ok := circuitConfigurationMap[name]
	if !ok {
		input = defaultCircuitConfiguration
	}
	input.Name = string(name)
	return helpers.MakeCircuit(cm, &input)
}
