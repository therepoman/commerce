package utils

import (
	"testing"
	"time"

	. "github.com/smartystreets/goconvey/convey"
)

func TestNow(t *testing.T) {
	Convey("StubTimeNow", t, func() {
		Now()
		StubTimeNow()
		So(Now(), ShouldResemble, time.Date(2017, 12, 12, 12, 12, 0, 0, time.UTC))
	})
}
