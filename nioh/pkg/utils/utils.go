package utils

import (
	"time"
)

// Now is the interface to time.Now to allow for stubbing
var Now func() time.Time

func init() {
	Now = func() time.Time {
		return time.Now().UTC()
	}
}

// StubTimeNow is a shorthand for stubbing out the time.Now
func StubTimeNow() time.Time {
	Now = func() time.Time {
		return time.Date(2017, 12, 12, 12, 12, 0, 0, time.UTC)
	}
	return Now()
}
