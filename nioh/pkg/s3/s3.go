package s3

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"strings"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/client"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"github.com/pkg/errors"
)

const (
	defaultRegion                 = "us-west-2"
	defaultEndpoint               = "s3-us-west-2.amazonaws.com"
	bucketAlreadyOwnedErrorPrefix = "BucketAlreadyOwnedByYou"
)

// IClient is an s3 client
type IClient interface {
	CreateBucket(bucketName string) error
	EnsureBucketExists(bucketName string) error
	UploadFileToBucket(bucketName string, fileKey string, file []byte) error
	FileExists(bucket string, key string) (bool, error)
	BucketExists(targetBucket string) (bool, error)
	ListFileNames(bucket string, prefix string) ([]string, error)
	ListObjects(bucket string, prefix string, marker string) ([]*s3.Object, error)
	WriteObjectToFile(file io.WriterAt, bucket string, key string) error
	LoadFile(bucket string, fileName string) ([]byte, error)
	DeleteFile(bucket string, key string) error
	DeleteFiles(bucket string, keys ...string) error
}

// Client implements IClient
type Client struct {
	s3         *s3.S3
	downloader *s3manager.Downloader
}

// NewDefaultConfig returns default config
func NewDefaultConfig() *aws.Config {
	return &aws.Config{
		Region:           aws.String(defaultRegion),
		Endpoint:         aws.String(defaultEndpoint),
		S3ForcePathStyle: aws.Bool(true),
	}
}

// New returns new Client
func New(provider client.ConfigProvider, config *aws.Config) IClient {
	return &Client{
		s3:         s3.New(provider, config),
		downloader: s3manager.NewDownloader(provider),
	}
}

// NewFromConfig returns new Client from config
func NewFromConfig(s3Config *aws.Config) (IClient, error) {
	ss, err := session.NewSession()
	if err != nil {
		return nil, err
	}
	return New(ss, s3Config), nil
}

// NewFromDefaultConfig returns new Client from default config
func NewFromDefaultConfig() (IClient, error) {
	return NewFromConfig(NewDefaultConfig())
}

// CreateBucket creates a new bucket
func (c *Client) CreateBucket(bucketName string) error {
	_, err := c.s3.CreateBucket(&s3.CreateBucketInput{
		Bucket: aws.String(bucketName),
	})
	return err
}

// EnsureBucketExists ensures bucket
func (c *Client) EnsureBucketExists(bucketName string) error {
	err := c.CreateBucket(bucketName)
	if err != nil && !strings.HasPrefix(err.Error(), bucketAlreadyOwnedErrorPrefix) {
		return err
	}
	return nil
}

// UploadFileToBucket uploads file
func (c *Client) UploadFileToBucket(bucketName string, fileKey string, file []byte) error {
	_, err := c.s3.PutObject(&s3.PutObjectInput{
		Bucket: aws.String(bucketName),
		Key:    aws.String(fileKey),
		Body:   bytes.NewReader(file),
	})
	return err
}

// FileExists returns whether file exists
func (c *Client) FileExists(bucket string, key string) (bool, error) {
	input := &s3.ListObjectsInput{
		Bucket: aws.String(bucket),
		Prefix: aws.String(key),
	}
	out, err := c.s3.ListObjects(input)
	if err != nil {
		return false, err
	}
	return len(out.Contents) > 0, nil
}

// BucketExists returns whether bucket exists
func (c *Client) BucketExists(targetBucket string) (bool, error) {
	out, err := c.s3.ListBuckets(&s3.ListBucketsInput{})
	if err != nil {
		return false, err
	}
	for _, bucket := range out.Buckets {
		if *bucket.Name == targetBucket {
			return true, nil
		}
	}
	return false, nil
}

// ListFileNames lists files
func (c *Client) ListFileNames(bucket string, prefix string) ([]string, error) {
	contents, err := c.ListObjects(bucket, prefix, "")
	if err != nil {
		return nil, err
	}

	allFiles := make([]string, 0)
	for _, obj := range contents {
		allFiles = append(allFiles, *obj.Key)
	}
	return allFiles, nil
}

// ListObjects lists objects
func (c *Client) ListObjects(bucket string, prefix string, marker string) ([]*s3.Object, error) {
	input := &s3.ListObjectsInput{
		Bucket: aws.String(bucket),
		Prefix: aws.String(prefix),
	}

	if marker != "" {
		input.Marker = aws.String(marker)
	}

	out, err := c.s3.ListObjects(input)
	if err != nil {
		return nil, err
	}

	return out.Contents, nil
}

// WriteObjectToFile downloads content and writes to the io.WriterAt interface
func (c *Client) WriteObjectToFile(file io.WriterAt, bucket string, key string) error {
	_, err := c.downloader.Download(file,
		&s3.GetObjectInput{
			Bucket: aws.String(bucket),
			Key:    aws.String(key),
		})

	return err
}

// WaitForNFiles waits for files
func (c *Client) WaitForNFiles(bucket string, prefix string, numFiles int, timeout time.Duration) ([]string, error) {
	// Creates a goroutine that writes to a channel once we have the expected result (or an error)
	ch := make(chan error, 1)
	var result []string
	go func() {
		for {
			fileNames, err := c.ListFileNames(bucket, prefix)
			if err != nil {
				ch <- err
				return
			}
			if len(fileNames) >= numFiles {
				result = fileNames
				ch <- nil
				return
			}
		}
	}()

	// Selects the first channel to respond
	select {
	case err := <-ch:
		return result, err
	case <-time.After(timeout):
		return result, errors.WithStack(fmt.Errorf("Timed out waiting for %d files", numFiles))
	}
}

// LoadFile loads file
func (c *Client) LoadFile(bucket string, fileName string) ([]byte, error) {
	input := &s3.GetObjectInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(fileName),
	}
	out, err := c.s3.GetObject(input)
	if err != nil {
		return nil, err
	}

	defer func() {
		err := out.Body.Close()
		if err != nil {
			fmt.Printf("Error while closing the file %+v\n", err)
		}
	}()
	bytes, err := ioutil.ReadAll(out.Body)
	if err != nil {
		return nil, err
	}

	return bytes, nil
}

// DeleteFile deletes file
func (c *Client) DeleteFile(bucket string, key string) error {
	input := &s3.DeleteObjectInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(key),
	}
	_, err := c.s3.DeleteObject(input)
	return err
}

// DeleteFiles deletes files
func (c *Client) DeleteFiles(bucket string, keys ...string) error {
	objectIdentifiers := make([]*s3.ObjectIdentifier, len(keys))
	for i, key := range keys {
		objectIdentifiers[i] = &s3.ObjectIdentifier{
			Key: aws.String(key),
		}
	}

	input := &s3.DeleteObjectsInput{
		Bucket: aws.String(bucket),
		Delete: &s3.Delete{
			Objects: objectIdentifiers,
		},
	}
	_, err := c.s3.DeleteObjects(input)
	return err
}
