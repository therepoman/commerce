package clientid

import (
	"context"
	"net/http"

	"code.justin.tv/foundation/twitchclient"
)

// AttacherMiddleware is an HTTP request middleware that reads and attachs the `Twitch-Client-Id` header value to the
// request context if it's present, allowing downstream functions to read that data from the context.
func AttacherMiddleware(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		clientID := r.Header.Get(twitchclient.TwitchClientIDHeader)

		r = r.WithContext(context.WithValue(r.Context(), ClientIDKey, clientID))

		h.ServeHTTP(w, r)
	})
}
