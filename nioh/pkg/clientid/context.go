package clientid

import "context"

type clientIDKeyType string

// ClientIDKey is the context key used for storing and retrieving the `Twitch-Client-Id` header value in a request context.
var ClientIDKey clientIDKeyType = "Client-ID"

// GetClientID reads and returns the ClientIDKey value from the passed context. The boolean return value indicates whether
// the value was set on the context.
func GetClientID(ctx context.Context) (string, bool) {
	v := ctx.Value(ClientIDKey)
	s, ok := v.(string)
	return s, ok
}
