# nioh

Paywalled Multiview Service

To learn about how Nioh works, see the contents of the [`docs/`](./docs) folder as well as our [service wiki page](https://wiki.twitch.com/pages/viewpage.action?pageId=193142599).
For On-calls, please reference the [Runbook](https://docs.google.com/document/d/1uD2zu28VkVaZVA8TEXG0RDUs2mV5ozW_-ZpRKasNVoU)

<a href="https://docs.google.com/document/d/1eb0drNPX9ZLABL4ec-INa7LEBwSipDXL6ZldQpotkKM/edit">
    <img src="https://upload.wikimedia.org/wikipedia/en/thumb/f/fd/Nioh_cover_art.jpg/220px-Nioh_cover_art.jpg" width="220" height="243" />
</a>

| Environment   | AWS Account     | DNS Alias                       |
| ------------- | --------------- |---------------------------------|
| development   | twitch-nioh-dev | development.nioh.twitch.a2z.com |
| beta          | twitch-nioh-dev | beta.nioh.twitch.a2z.com        |
| canary        | twitch-nioh-aws | See **Networking** section below|
| production    | twitch-nioh-aws | production.nioh.twitch.a2z.com  |

**Note**: Nioh has Twitch DNS entries, but the resolved canonical names are not reachable from within the Twitch backbone. They are there for VPC peered services that use Twitch DNS.

## Setting up your workspace

### Go
 - Install Go:

```
$ brew install go
```

 - Create a Go workspace folder in your home directory. Ex:

```
$ mkdir /Users/<username>/GoWorkspace
```

 - Set your environment variable for GOPATH (in your .bashrc, .zshrc, or etc.)

```
$ export GOPATH=/Users/<username>/GoWorkspace
```

 - Append GOPATH/bin to PATH environment variable

```
$ export PATH=$PATH:$GOPATH/bin
```

 - Clone down Nioh source within the new commerce folder. Make sure you added your ssh key to Github (https://git.xarth.tv/settings/keys)

```
$ git clone git@git.xarth.tv:commerce/nioh.git $GOPATH/src/code.justin.tv/commerce/nioh
```

 - Directory structure will now look like this:

```
$ /Users/<username>/GoWorkspace/src/code.justin.tv/commerce/nioh
```

 ### AWS
Running Nioh locally requires programmatic access to AWS resources in the appropriate AWS account. [Ensure you have the AWS CLI installed](https://docs.aws.amazon.com/cli/latest/userguide/installing.html)

1. In [Isengard](https://isengard.amazon.com), confirm you have access to the Admin console for the Nioh account you need credentials for (either `twitch-nioh-aws` for production or `twitch-nioh-dev` for development).
2. Install [`isengard_credentials`](https://git.xarth.tv/twitch/isengard_credentials)
3. Add these entries into your `~/.aws/credentials` file:
    ```
    [twitch-nioh-dev]
    region=us-west-2
    credential_process=isengard_credentials --account-id 239530023759 --role Admin

    [twitch-nioh-aws]
    region=us-west-2
    credential_process=isengard_credentials --account-id 483600360996 --role Admin
    ```

## Developing in Nioh
### Make sure you are running go1.10 or above

### Running the service locally
- To run the service locally:

```
$ make dev
```

- Most downstream dependencies require access to the Twitch VPN. To test the service locally without logging into the Twitch VPN, you can set up an ssh proxy:

```bash
# Terminal window 1
$ make tunnel

...

# Terminal window 2
$ make dev-tunnel
```

**Note**: Make sure you have the right AWS credentials profile (**twitch-nioh-dev**) before running this command. These will be the stored in the `~/.aws/credentials` file. Your twitch-nioh-dev credentials should either be default or you can specify the credentials as such:

```
$ AWS_PROFILE=twitch-nioh-dev go run cmd/nioh/main.go
```

If you need to customize the TCP ports that Nioh binds its server and debug listener to, use the environment variables `BIND_ADDR` and `DEBUG_ADDR`:
```
$ env BIND_ADDR=:8001 DEBUG_ADDR=:6001 make dev
```
(Note the TCP address syntax: the hostname is elided, but the `:` character before the port is required.)

- To perform all the quality and static analysis prior to PR:

```
$ make quality
```

### Testing
#### Unit Testing
```
$ make test
```

#### Integration Testing

Integration tests against dev RPCs intended to run locally for development.
```
$ make test_integration_dev
```

Integration tests against beta RPCs.
```
$ make test_integration_beta
```

Integration tests against production RPCs.
```
$ make test_integration_production
```

Integration tests against beta API gateway (Visage).
```
$ make test_public_integration_beta
```

Integration tests against production API gateway (Visage).
```
$ make test_public_integration_production
```


### Updating Dependencies
Nioh's dependencies are managed with **gomodules**.

#### Installing Dependencies
Run `go mod vendor` to ensure `vendor/` is in the correct state for your configuration.

```
$ go mod vendor
$ go mod tidy
```

You might run into an issue running `go mod vendor` after running `go mod tidy`, getting something like:

```
code.justin.tv/commerce/nioh/cmd/nioh imports
	github.com/cactus/go-statsd-client/statsd: ambiguous import: found package github.com/cactus/go-statsd-client/statsd in multiple modules:
	github.com/cactus/go-statsd-client v3.1.1+incompatible (/Users/$USER/go/pkg/mod/github.com/cactus/go-statsd-client@v3.1.1+incompatible/statsd)
	github.com/cactus/go-statsd-client/statsd v0.0.0-20190922113730-52b467de415c (/Users/$USER/go/pkg/mod/github.com/cactus/go-statsd-client/statsd@v0.0.0-20190922113730-52b467de415c
```

You can fix this by getting a specific version of this client:

```
$ go get github.com/cactus/go-statsd-client@d238121835fbe8fafea2c3113efc
```

#### Further Reading
- [dep Github](https://github.com/golang/dep)
- [dep FAQ](https://github.com/golang/dep/blob/master/docs/FAQ.md)

### Mocks
Nioh uses mockery to generate mocks for testing. You can generate mock objects with this command.

```
$ make mock
```
This will delete and recreate mocks in the following paths (take a look at Makefile for details). If you add mockable objects in different paths, you may consider updating this command.
```
internal/auth/
internal/backend/
internal/datastore/
internal/tracker/
pkg/dynamodb/
pkg/errorlogger/
pkg/spade/
```

### Load Tests

We have a separate [README](test/loadtests/README.md) for load tests.

### CloudFormation

Nioh's infrastructures are created and edited with CloudFormation. The template files can be found in `deployments/infra/resources.yaml` and `deployments/infra/ecs-app.yaml`. These templates are intended for each environment, and to be run as a part of the deployment pipeline. ~~It is unnecessary to execute changes manually, but when a need arises,~~ **Infrastructure changes will need to be ran manually for now:** it can be done with the command `make update_{env}_infra` where `{env}` is `dev`, `beta`, or `prod`.

For `prod`, the execution won't run immediately. Instead, it will create a changeset. You will need to preview the change before applying it. This can be done in AWS Console or using the CLI commands (arn is returned in the json when you run the make command above). Note that the preview does not contain details modifications. It just lists resources with high level descriptions on what are being changed. This is still useful against accidental resource deletion (which have happened before).

```
AWS_PROFILE=twitch-nioh-aws aws cloudformation describe-change-set --change-set-name <arn>
AWS_PROFILE=twitch-nioh-aws aws cloudformation execute-change-set --change-set-name <arn>
```

For more information on CloudFormation, read [CloudFormation AWS Doc](https://docs.aws.amazon.com/cloudformation/index.html#lang/en_us).

### Upload Image with Upload Service
Nioh provides an endpoint (`CreateContentAttributeImageUploadConfig`) to request new image upload URL for `Content Attributes`. This endpoint replies with the necessary information for clients to upload new images via the [Upload Service](https://git.xarth.tv/web/upload-service).

Response from this endpoint will include the following information:
```
UploadID    // generated by the Upload Service, can be used to lookup upload status and progress.

UploadURL   // the URL that the new image should be uploaded to

ImageURL    // the URL to access the image once uploaded
```

Clients can listen to Pubsub topics from the Upload Service to track the progress of uploads. The Upload Service also provides a `Status` endpoint to lookup the status.
```
Staging:
pubsubtest.upload.<upload_id>

Prod:
upload.<upload_id>
```
Upload service will put the image asset in our S3 buckets (bucket configurations can be found in the cloudformation file or configuration JSON files) once the upload is complete.


## Deployment

### Development
Jenkins is used for all deployments. You can test your changes on the `development` ECS application stack after pushing your branch. To do so, go to the [Development Jenkins job](https://jenkins-og.xarth.tv/job/commerce-nioh-deploy-development/) and click **Build Now**. When the pipeline asks for a git version, choose the git sha from your branch and deploy.
**Note**: Development environment is currently broken due to a previous infra migration. If you must test manually, use local or beta.

### Beta
Although Beta deployment is a part of the main production pipeline, you can manually run integration tests and deploy to Beta when the need arises. To do so, go to the [Beta Jenkins job](https://jenkins-og.xarth.tv/job/commerce-nioh-deploy-beta/) and click **Build Now** similar to the Development deployment above. Note that a merge will override this deployment.

### Production Pipeline

We are using Jenkins for our CI/CD. The production pipeline consists of various stages from obtaining the source from Github to running tests, building and pushing containers to ECR, and finally shipping to the live environment(s). The pipeline is maintained in the root `Jenkinsfile`, as well as the `deployments/{environment}/Jenkinsfile` files for directly deploying to an environment. When an update is made to a `Jenkinsfile`, the changes will be pulled in automatically within the next branch commit. You can view the build pipeline here:

https://jenkins-og.xarth.tv/job/commerce/job/nioh/job/master

The master pipeline will deploy your change to prod, and there is no action for you to take. Messages will appear in the [#nioh](https://app.slack.com/client/T0266V6GF/C01DFDSPK43) slack channel with updates.

If you need to manually deploy a change (i.e. during an emergency), you can quickly select a commit sha from here and deploy it to prod:

https://jenkins-og.xarth.tv/job/commerce-nioh-deploy-production/

From here, start a new build by pressing **Build Now**, and then select the canary sha version from the newly started pipeline job:

![Production Build Job](./docs/img/prod-build-job.jpg)

### Rolling back
In the case of a failed or bad deployment, you can rollback to a previous application version through the [Canary Jenkins job](https://jenkins-og.xarth.tv/job/commerce-nioh-deploy-canary/) and [Production Jenkins job](https://jenkins-og.xarth.tv/job/commerce-nioh-deploy-production/). Simply click **Build Now** on the left menu and choose the desired build sha when the pipeline prompts for a version.

#### Code Freeze

When we need to place a code freeze so that no new code is delivered to any of the production environments (canary and prod), click **Disable this project** in the Jenkins Pipeline job configuration:

https://jenkins-og.xarth.tv/job/commerce/job/nioh/job/master/configure

![Code Freeze](./docs/img/code-freeze.jpg)

## Networking

Nioh's VPCs are not connected to the Twitch backbone. In order to reach the service from within your local machine, you will need to use [Teleport Remote](https://wiki.twitch.com/display/SEC/Teleport+Remote). The tunneling instance is already setup with the `infra` LDAP group.

Further reading:
- [Teleport-Bastion](https://wiki.twitch.com/display/SEC/Teleport+Bastion)
- [Bastion-LDAP-Host](https://git.xarth.tv/twitch/bastion_ldap_host)
- [cloud-init](https://wiki.twitch.com/display/SEC/Supported+Instance+Configuration+for+Teleport+Bastion+Support)

To ssh into a host:
```
TC=teleport-remote-twitch-nioh-dev ssh jumpbox
```

This is cool, but you probably want to make Twirp service calls to a deployed environment. You can do this using socks5 proxy.

```
TC=teleport-remote-twitch-nioh-dev ssh -D 8080 jumpbox
```

To curl,
```
curl --socks5 localhost:8080 nioh-service-development.internal.justin.tv
```
(replace the host url to your target in dev VPC)

To use in Insomnia

- In preferences check Enable Proxy
- Under HTTP Proxy: `socks5h://localhost:8080`

**What about Postman?** Use Insomnia.

**I want to be able to hit Nioh Production from a locally running service**

You can with a bit of extra work. Instead of using the dynamic forwarding with `ssh -D` you can use `ssh -L` which lets you specify the endpoint. Using this, it also removes the need to have the proxy via direct tunneling given you are hitting the specified port on your localhost.

```
TC=teleport-remote-twitch-nioh-prod ssh -L 8080:nioh-service-production.internal.justin.tv:443 jumpbox
```

While tunneling, simply hitting `localhost:8080` will tunnel through your proxy host and eventually resolve to `nioh-service-production.internal.justin.tv:443`.

```
curl --insecure https://localhost:8080
```

This will give you an OK response, which means this is the address you want to have as Nioh endpoint in the configuration of your locally running service.

**I want to be able to hit Nioh Canary from a locally running service**

To hit a Canary task, simply run `make tunnel_canary`. An ssh tunnel will be made directly to a Canary container task within the nioh-canary ECS service.

```bash
make tunnel_canary
...
# Hit canary on port 8080
curl localhost:8080
> OK
```

### Private Links (VPC Endpoint Service)

When an upstream service wants to create a private link to Nioh, you'll need to add their AWS account id to the `AllowedPrincipals` list in the `upstream-endpoints(-dev)-params.json` file. To deploy the private link change, run the following commands for either beta or production:

```bash
make update_privatelink_beta
make update_privatelink_prod
```

Our private link Cloudformation template is based on https://git.xarth.tv/achou/privatelink. A detailed look into how the private link infrastructure is setup can be found here: https://aws.amazon.com/blogs/networking-and-content-delivery/using-static-ip-addresses-for-application-load-balancers/
