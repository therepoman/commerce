module code.justin.tv/commerce/nioh

go 1.15

replace (
	code.justin.tv/safety/gateway => code.justin.tv/safety/gateway v1.10.8-0.20210614180359-9d3ef522edda
	gopkg.in/DATA-DOG/go-sqlmock.v1 => github.com/DATA-DOG/go-sqlmock v1.5.0
)

require (
	code.cloudfoundry.org/go-diodes v0.0.0-20190809170250-f77fb823c7ee // indirect
	code.justin.tv/amzn/PDMSLambdaTwirp v0.0.0-20210624231252-1f5b3db2c132
	code.justin.tv/amzn/SiosaTwirp v0.0.0-20201016185246-6e37cbd6052d
	code.justin.tv/amzn/StarfruitTwitchChannelPropsTwirp v0.0.0-20210330171614-4bf4714378c2
	code.justin.tv/amzn/TwirpGoLangAWSTransports v0.0.0-20200311200501-30801aea0958
	code.justin.tv/amzn/TwitchVoyagerTwirp v0.0.0-20210624231730-f3a81435ad24
	code.justin.tv/chat/pubsub-go-pubclient v1.0.0
	code.justin.tv/chat/zuma v1.3.9
	code.justin.tv/commerce/config v0.0.0-20180202175027-9870c66b280a
	code.justin.tv/commerce/dynamicconfig v1.1.3
	code.justin.tv/commerce/logrus v1.1.2
	code.justin.tv/commerce/mako v1.1.9 // indirect
	code.justin.tv/commerce/rollrus v0.0.0-20181016000234-3d66897470b9
	code.justin.tv/commerce/splatter v1.5.6
	code.justin.tv/common/chitin v0.16.0
	code.justin.tv/common/goauthorization v1.0.0
	code.justin.tv/common/golibs v1.1.0
	code.justin.tv/common/gometrics v0.0.0-20171010235942-7491402d56a5
	code.justin.tv/common/spade-client-go v2.1.1+incompatible
	code.justin.tv/common/twirp v4.9.0+incompatible
	code.justin.tv/devrel/devsite-rbac v0.0.0-20200129215941-d920e3d3ce9c
	code.justin.tv/discovery/liveline v0.0.0-20200514184419-010e5740014b
	code.justin.tv/eventbus/client v1.6.0
	code.justin.tv/eventbus/schema v0.0.0-20200130195606-faa7d7edd5aa
	code.justin.tv/foundation/twitchclient v4.11.0+incompatible
	code.justin.tv/growth/sfnactivity v1.0.2
	code.justin.tv/growth/sqsmessage v4.1.2+incompatible
	code.justin.tv/revenue/pepper v0.0.0-20190429221722-0bf81c322d79
	code.justin.tv/revenue/subscriptions v0.0.0-20200612194741-d764c0be5fe2
	code.justin.tv/safety/canthey v0.0.0-20210602163732-284721dcfc91 // indirect
	code.justin.tv/sse/malachai v0.5.9
	code.justin.tv/video/autoprof v1.1.2
	code.justin.tv/vod/vodapi v1.1.10-0.20201105223905-948c608ba215
	code.justin.tv/web/upload-service v3.1.1-0.20180831213518-13f017f525af+incompatible
	code.justin.tv/web/users-service v2.12.2+incompatible
	github.com/BurntSushi/toml v0.3.1
	github.com/afex/hystrix-go v0.0.0-20180502004556-fa1af6a1f4f5
	github.com/alecthomas/gometalinter v3.0.0+incompatible
	github.com/alecthomas/units v0.0.0-20210208195552-ff826a37aa15 // indirect
	github.com/aws/aws-dax-go v1.2.7
	github.com/aws/aws-lambda-go v1.19.1
	github.com/aws/aws-sdk-go v1.38.7
	github.com/axw/gocov v1.0.0
	github.com/cactus/go-statsd-client/statsd v0.0.0-20200423205355-cb0885a1018c
	github.com/cep21/circuit v3.0.0+incompatible
	github.com/cloudfoundry/go-diodes v0.0.0-20190809170250-f77fb823c7ee // indirect
	github.com/dgraph-io/ristretto v0.0.4-0.20201126003255-67fef616c676
	github.com/felixge/httpsnoop v1.0.0
	github.com/gofrs/uuid v3.3.0+incompatible
	github.com/golang/protobuf v1.5.2
	github.com/google/shlex v0.0.0-20191202100458-e7afc7fbc510 // indirect
	github.com/graph-gophers/dataloader v5.0.0+incompatible
	github.com/guregu/dynamo v1.1.1
	github.com/hashicorp/go-multierror v1.1.0
	github.com/heroku/rollbar v0.6.0
	github.com/nicksnyder/go-i18n v1.10.1 // indirect
	github.com/patrickmn/go-cache v2.1.0+incompatible
	github.com/pelletier/go-toml v1.9.0 // indirect
	github.com/pkg/errors v0.9.1
	github.com/satori/go.uuid v1.2.0
	github.com/sergi/go-diff v1.0.0
	github.com/sirupsen/logrus v1.8.1
	github.com/smartystreets/goconvey v1.6.4
	github.com/stretchr/testify v1.6.1
	github.com/t-yuki/gocov-xml v0.0.0-20131029153837-1cdaab087002
	github.com/twitchscience/kinsumer v0.0.0-20201111182439-cd685b6b5f68
	github.com/twitchtv/twirp v8.1.0+incompatible
	github.com/vektra/mockery v0.0.0-20181123154057-e78b021dcbb5
	goji.io v2.0.2+incompatible
	golang.org/x/net v0.0.0-20210331212208-0fccb6fa2b5c
	google.golang.org/protobuf v1.26.0
	gopkg.in/alecthomas/kingpin.v3-unstable v3.0.0-20191105091915-95d230a53780 // indirect
)
