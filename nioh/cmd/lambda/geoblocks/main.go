package main

import (
	"log"
	"os"

	"github.com/aws/aws-lambda-go/lambda"
)

func main() {
	env := os.Getenv("ENVIRONMENT")
	niohHost := os.Getenv("NIOH_HOST")

	log.Printf("starting lambda in %s environment with nioh host %s\n", env, niohHost)

	handler, err := NewHandler(niohHost)
	if err != nil {
		log.Fatal(err)
	}

	lambda.Start(handler.HandleRequest)
}
