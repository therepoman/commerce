package main

import (
	"context"
	"errors"
	"testing"

	mocks "code.justin.tv/commerce/nioh/mocks/nioh"
	"code.justin.tv/commerce/nioh/rpc/nioh"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func TestProcessEvent(t *testing.T) {
	tests := []struct {
		scenario  string
		setup     func() Handler
		geoblock  nioh.GeoblockRequest
		shouldErr bool
	}{
		{
			scenario: "when the call to nioh fails for a create geoblock request",
			setup: func() Handler {
				mockNioh := new(mocks.API)

				mockNioh.On("SetResourceRestriction", mock.Anything, mock.Anything).Return(nil, errors.New("error"))

				return Handler{
					nioh: mockNioh,
				}
			},
			geoblock: nioh.GeoblockRequest{
				Action:         ActionBlock,
				ContentId:      "contentID",
				ContentOwnerId: "contentOwnerID",
				ContentType:    ContentTypeBroadcast,
				CountryCodes:   []string{"DE"},
				RequesterId:    "requesterID",
			},
			shouldErr: true,
		},
		{
			scenario: "when the call to nioh fails for a delete geoblock request",
			setup: func() Handler {
				mockNioh := new(mocks.API)

				mockNioh.On("DeleteResourceRestriction", mock.Anything, mock.Anything).Return(nil, errors.New("error"))

				return Handler{
					nioh: mockNioh,
				}
			},
			geoblock: nioh.GeoblockRequest{
				Action:         ActionAllow,
				ContentId:      "contentID",
				ContentOwnerId: "contentOwnerID",
				ContentType:    ContentTypeBroadcast,
				CountryCodes:   []string{"DE"},
				RequesterId:    "requesterID",
			},
			shouldErr: true,
		},
		{
			scenario: "when all calls succeed for a request",
			setup: func() Handler {
				mockNioh := new(mocks.API)

				mockNioh.On("SetResourceRestriction", mock.Anything, mock.Anything).Return(&nioh.MutateResourceRestrictionResponse{}, nil)

				return Handler{
					nioh: mockNioh,
				}
			},
			geoblock: nioh.GeoblockRequest{
				Action:         ActionBlock,
				ContentId:      "contentID",
				ContentOwnerId: "contentOwnerID",
				ContentType:    ContentTypeBroadcast,
				CountryCodes:   []string{"DE"},
				RequesterId:    "requesterID",
			},
			shouldErr: false,
		},
	}

	for _, test := range tests {
		test := test

		t.Run(test.scenario, func(t *testing.T) {
			t.Parallel()

			h := test.setup()
			err := h.processEvent(context.Background(), test.geoblock)
			if test.shouldErr {
				assert.NotNil(t, err)
			} else {
				assert.Nil(t, err)
			}
		})
	}
}

func TestValidateRequest(t *testing.T) {
	tests := []struct {
		scenario       string
		request        nioh.GeoblockRequest
		isValidRequest bool
	}{
		{
			scenario: "when an invalid action is passed in",
			request: nioh.GeoblockRequest{
				Action:         "asdf",
				CountryCodes:   []string{"DE"},
				ContentId:      "contentID",
				ContentOwnerId: "ownerID",
				ContentType:    ContentTypeBroadcast,
				RequesterId:    "requesterID",
			},
			isValidRequest: false,
		},
		{
			scenario: "when no country codes are passed in for a block action",
			request: nioh.GeoblockRequest{
				Action:         ActionBlock,
				CountryCodes:   []string{},
				ContentId:      "contentID",
				ContentOwnerId: "contentOwnerID",
				ContentType:    ContentTypeBroadcast,
				RequesterId:    "requesterID",
			},
			isValidRequest: false,
		},
		{
			scenario: "when content_owner_id is empty",
			request: nioh.GeoblockRequest{
				Action:         ActionAllow,
				CountryCodes:   []string{"DE"},
				ContentId:      "contentID",
				ContentOwnerId: "",
				ContentType:    ContentTypeBroadcast,
				RequesterId:    "requesterID",
			},
			isValidRequest: false,
		},
		{
			scenario: "when content_id is empty",
			request: nioh.GeoblockRequest{
				Action:         ActionAllow,
				CountryCodes:   []string{"DE"},
				ContentId:      "",
				ContentOwnerId: "contentOwnerID",
				ContentType:    ContentTypeBroadcast,
				RequesterId:    "requesterID",
			},
			isValidRequest: false,
		},
		{
			scenario: "when an invalid content_type is passed in",
			request: nioh.GeoblockRequest{
				Action:         ActionAllow,
				CountryCodes:   []string{"DE"},
				ContentId:      "contentID",
				ContentOwnerId: "contentOwnerID",
				ContentType:    "asdf",
				RequesterId:    "requesterID",
			},
			isValidRequest: false,
		},
		{
			scenario: "when requester_id is empty",
			request: nioh.GeoblockRequest{
				Action:         ActionAllow,
				CountryCodes:   []string{"DE"},
				ContentId:      "contentID",
				ContentOwnerId: "contentOwnerID",
				ContentType:    ContentTypeBroadcast,
				RequesterId:    "",
			},
			isValidRequest: false,
		},
		{
			scenario: "when input is valid",
			request: nioh.GeoblockRequest{
				Action:         ActionAllow,
				CountryCodes:   []string{"DE"},
				ContentId:      "contentID",
				ContentOwnerId: "contentOwnerID",
				ContentType:    ContentTypeBroadcast,
				RequesterId:    "requesterID",
			},
			isValidRequest: true,
		},
	}

	for _, test := range tests {
		test := test

		t.Run(test.scenario, func(t *testing.T) {
			t.Parallel()

			h, err := NewHandler("nioh")
			assert.Nil(t, err)

			err = h.validateRequest(test.request)

			if test.isValidRequest {
				assert.Nil(t, err)
			} else {
				assert.NotNil(t, err)
			}
		})
	}
}
