package main

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"

	"code.justin.tv/commerce/nioh/rpc/nioh"
	"github.com/aws/aws-lambda-go/events"
)

// Handler encapsulates the data needed to handle a geoblock request
type Handler struct {
	nioh nioh.API
}

const (
	// ActionAllow represents a request to allow content in a location
	ActionAllow = "allow"

	// ActionBlock represents a request to block content from a location
	ActionBlock = "block"

	// ContentTypeBroadcast represents a broadcast
	ContentTypeBroadcast = "broadcast"

	// HighlightBroadcastID is the broadcast ID that _all_ highlights share.
	// We special case this broadcast ID and explicitly do not block this
	// broadcast ID because it would make all highlights blocked in that geo
	HighlightBroadcastID = "1"
)

// NewHandler instantiates a new handler
func NewHandler(niohHost string) (*Handler, error) {
	if niohHost == "" {
		log.Fatal("NIOH_HOST must be set")
	}

	return &Handler{
		nioh: nioh.NewAPIProtobufClient(niohHost, &http.Client{}),
	}, nil
}

// HandleRequest reads geoblock messages from an SQS queue and processes them appropriately
func (h *Handler) HandleRequest(ctx context.Context, event events.SQSEvent) error {
	for _, record := range event.Records {
		var g nioh.GeoblockRequest
		if err := h.unmarshalSNSMessage(record.Body, &g); err != nil {
			log.Printf("error unmarshalling SNS message: %s", err.Error())
			return nil
		}

		log.Printf("processing message: %+v\n", g)

		if err := h.validateRequest(g); err != nil {
			log.Printf("received invalid geoblock request: %s", err.Error())
			return nil
		}

		if err := h.processEvent(ctx, g); err != nil {
			log.Printf("error processing geoblocking request: %s", err.Error())
			return err
		}

		log.Printf("successfully processed message")
	}

	return nil
}

func (h *Handler) processEvent(ctx context.Context, g nioh.GeoblockRequest) error {
	var err error

	switch g.Action {
	case ActionAllow:
		err = h.deleteGeoblock(ctx, g)
	default:
		err = h.createGeoblock(ctx, g)
	}

	return err
}

func (h *Handler) createGeoblock(ctx context.Context, g nioh.GeoblockRequest) error {
	req := &nioh.SetResourceRestrictionRequest{
		UserId:  g.RequesterId,
		OwnerId: g.ContentOwnerId,
		Resource: &nioh.RestrictionResource{
			Id:   g.ContentId,
			Type: nioh.ResourceType_BROADCAST,
		},
		Restriction: &nioh.Restriction{
			RestrictionType: nioh.Restriction_GEOBLOCK,
			Geoblocks: []*nioh.Geoblock{
				{Operator: nioh.Geoblock_BLOCK, CountryCodes: g.CountryCodes},
			},
		},
	}

	_, err := h.nioh.SetResourceRestriction(ctx, req)
	return err
}

func (h *Handler) deleteGeoblock(ctx context.Context, g nioh.GeoblockRequest) error {
	req := &nioh.DeleteResourceRestrictionRequest{
		UserId:  g.RequesterId,
		OwnerId: g.ContentOwnerId,
		Resource: &nioh.RestrictionResource{
			Id:   g.ContentId,
			Type: nioh.ResourceType_BROADCAST,
		},
	}

	_, err := h.nioh.DeleteResourceRestriction(ctx, req)
	return err
}

func (h *Handler) validateRequest(g nioh.GeoblockRequest) error {
	if g.Action != ActionAllow && g.Action != ActionBlock {
		return fmt.Errorf("action must be %s or %s, received %s", ActionAllow, ActionBlock, g.Action)
	}

	if g.Action == ActionBlock && len(g.CountryCodes) == 0 {
		return errors.New("country_codes must not be empty in a block request")
	}

	if g.ContentId == "" {
		return errors.New("content_id must not be empty")
	}

	if g.ContentOwnerId == "" {
		return errors.New("content_owner_id must not be empty")
	}

	if g.ContentType != ContentTypeBroadcast {
		return fmt.Errorf("content_type must be %s, received %s", ContentTypeBroadcast, g.ContentType)
	}

	if g.ContentType == ContentTypeBroadcast && g.ContentId == HighlightBroadcastID {
		return fmt.Errorf("cannot geoblock broadcast ID %s as it would block all highlights", HighlightBroadcastID)
	}

	if g.RequesterId == "" {
		return errors.New("requester_id must not be empty")
	}

	return nil
}

func (h *Handler) unmarshalSNSMessage(message string, v interface{}) error {
	if message == "" {
		return errors.New("no message provided")
	}

	var entity events.SNSEntity
	if err := json.Unmarshal([]byte(message), &entity); err != nil {
		return err
	}

	return json.Unmarshal([]byte(entity.Message), v)
}
