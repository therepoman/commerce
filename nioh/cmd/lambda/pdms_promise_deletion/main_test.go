package main

import (
	"context"
	"errors"
	"testing"

	pdmstwirp "code.justin.tv/amzn/PDMSLambdaTwirp"
	mocks "code.justin.tv/commerce/nioh/mocks"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func TestHandler(t *testing.T) {
	tests := []struct {
		scenario  string
		setup     func() handler
		shouldErr bool
	}{
		{
			scenario: "when the call to pdms fails",
			setup: func() handler {
				mockPDMS := new(mocks.PDMSService)

				mockPDMS.On("PromiseDeletion", mock.Anything, mock.Anything).Return(nil, errors.New("error"))

				return handler{
					pdms:      mockPDMS,
					serviceID: "419",
				}
			},
			shouldErr: true,
		},
		{
			scenario: "when all calls succeed for a request",
			setup: func() handler {
				mockPDMS := new(mocks.PDMSService)

				mockPDMS.On("PromiseDeletion", mock.Anything, mock.Anything).Return(&pdmstwirp.PromiseDeletionPayload{}, nil)

				return handler{
					pdms:      mockPDMS,
					serviceID: "419",
				}
			},
			shouldErr: false,
		},
	}

	for _, test := range tests {
		test := test

		t.Run(test.scenario, func(t *testing.T) {
			t.Parallel()

			h := test.setup()
			res, err := h.Handler(context.Background(), &request{"test-user-id"})
			if test.shouldErr {
				assert.NotNil(t, err)
			} else {
				assert.Nil(t, err)
				assert.Equal(t, "test-user-id", res.UserID)
			}
		})
	}
}
