package main

import (
	"context"
	"errors"
	"net/http"
	"os"
	"time"

	pdmstwirp "code.justin.tv/amzn/PDMSLambdaTwirp"
	twirplambdatransport "code.justin.tv/amzn/TwirpGoLangAWSTransports/lambda"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-lambda-go/lambdacontext"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials/stscreds"
	"github.com/aws/aws-sdk-go/aws/endpoints"
	"github.com/aws/aws-sdk-go/aws/session"
	lambdaCli "github.com/aws/aws-sdk-go/service/lambda"
	"github.com/golang/protobuf/ptypes"
	"github.com/sirupsen/logrus"
)

const ThirtyDays = time.Duration(30) * time.Hour * 24

type handler struct {
	pdms      pdmstwirp.PDMSService
	serviceID string
}

type request struct {
	UserID string `json:"user_id"`
}

type response struct {
	UserID string `json:"user_id"`
}

func (h *handler) Handler(ctx context.Context, req *request) (*response, error) {
	if req.UserID == "" {
		return nil, errors.New("invalid request: no user_id supplied")
	}

	logrus := logrus.WithFields(logrus.Fields{
		"FunctionName":    lambdacontext.FunctionName,
		"FunctionVersion": lambdacontext.FunctionVersion,
		"UserID":          req.UserID,
	})

	_, err := h.pdms.ReportDeletion(ctx, &pdmstwirp.ReportDeletionRequest{
		UserId:    req.UserID,
		ServiceId: h.serviceID,
		Timestamp: ptypes.TimestampNow(),
	})
	if err != nil {
		logrus.WithError(err).Error("error calling PDMS ReportDeletion")
		return nil, err
	}

	return &response{
		UserID: req.UserID,
	}, nil
}

func main() {
	pdmsHost := os.Getenv("PDMS_HOST")
	if pdmsHost == "" {
		logrus.Fatal("PDMS_HOST must be set")
	}

	pdmsCallerRole := os.Getenv("PDMS_CALLER_ROLE")
	if pdmsCallerRole == "" {
		logrus.Fatal("PDMS_CALLER_ROLE must be set")
	}

	serviceID := os.Getenv("SERVICE_ID")
	if pdmsCallerRole == "" {
		logrus.Fatal("SERVICE_ID must be set")
	}
	// Add timestamp to log events in Common Log Format Date
	logrus.SetFormatter(&logrus.TextFormatter{FullTimestamp: true, TimestampFormat: "02/Jan/2006:15:04:05 -0700"})

	handler := handler{
		pdms:      newPDMSClient(pdmsHost, pdmsCallerRole),
		serviceID: serviceID,
	}

	lambda.Start(handler.Handler)
}

func newPDMSClient(pdmsHost string, pdmsCallerRole string) pdmstwirp.PDMSService {
	// Client creation outlined here: https://wiki.twitch.com/pages/viewpage.action?spaceKey=SEC&title=PDMS-+Delete+pipeline+Service+Onboarding
	sess := session.Must(session.NewSessionWithOptions(session.Options{
		Config: aws.Config{
			Region:              aws.String("us-west-2"),
			STSRegionalEndpoint: endpoints.RegionalSTSEndpoint,
			// We want a long timeout for PDMS, as Lambda based services can take a while to warm up on cold start.
			HTTPClient: &http.Client{Timeout: 10 * time.Second},
		},
	}))
	creds := stscreds.NewCredentials(sess, pdmsCallerRole)
	pdmsLambdaCli := lambdaCli.New(sess, &aws.Config{Credentials: creds})
	pdmsTransport := twirplambdatransport.NewClient(pdmsLambdaCli, pdmsHost)
	return pdmstwirp.NewPDMSServiceProtobufClient("", pdmsTransport)
}
