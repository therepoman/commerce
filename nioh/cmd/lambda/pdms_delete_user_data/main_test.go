package main

import (
	"context"
	"errors"
	"testing"

	mocks "code.justin.tv/commerce/nioh/mocks/nioh"
	"code.justin.tv/commerce/nioh/rpc/nioh"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func TestHandler(t *testing.T) {
	tests := []struct {
		scenario  string
		setup     func() handler
		shouldErr bool
	}{
		{
			scenario: "when the call to nioh fails",
			setup: func() handler {
				mockNioh := new(mocks.API)

				mockNioh.On("PDMSDeleteResourceRestriction", mock.Anything, mock.Anything).Return(nil, errors.New("error"))

				return handler{
					nioh: mockNioh,
				}
			},
			shouldErr: true,
		},
		{
			scenario: "when all calls succeed for a request",
			setup: func() handler {
				mockNioh := new(mocks.API)

				mockNioh.On("PDMSDeleteResourceRestriction", mock.Anything, mock.Anything).Return(&nioh.MutateResourceRestrictionResponse{}, nil)

				return handler{
					nioh: mockNioh,
				}
			},
			shouldErr: false,
		},
	}

	for _, test := range tests {
		test := test

		t.Run(test.scenario, func(t *testing.T) {
			t.Parallel()

			h := test.setup()
			res, err := h.Handler(context.Background(), &request{"test-user-id"})
			if test.shouldErr {
				assert.NotNil(t, err)
			} else {
				assert.Nil(t, err)
				assert.Equal(t, "test-user-id", res.UserID)
			}
		})
	}
}
