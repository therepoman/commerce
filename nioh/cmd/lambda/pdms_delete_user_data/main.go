package main

import (
	"context"
	"errors"
	"net/http"
	"os"

	"code.justin.tv/commerce/nioh/rpc/nioh"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-lambda-go/lambdacontext"
	"github.com/sirupsen/logrus"
)

type handler struct {
	nioh nioh.API
}

type request struct {
	UserID string `json:"user_id"`
}

type response struct {
	UserID string `json:"user_id"`
}

func (h *handler) Handler(ctx context.Context, req *request) (*response, error) {
	if req.UserID == "" {
		return nil, errors.New("invalid request: no user_id supplied")
	}

	logrus := logrus.WithFields(logrus.Fields{
		"FunctionName":    lambdacontext.FunctionName,
		"FunctionVersion": lambdacontext.FunctionVersion,
		"UserID":          req.UserID,
	})

	_, err := h.nioh.PDMSDeleteResourceRestriction(ctx, &nioh.DeleteResourceRestrictionRequest{
		UserId:  req.UserID,
		OwnerId: req.UserID,
		Resource: &nioh.RestrictionResource{
			Id:   req.UserID,
			Type: nioh.ResourceType_LIVE,
		},
	})
	if err != nil {
		logrus.WithError(err).Error("error calling Nioh DeleteResourceRestriction")
		return nil, err
	}

	return &response{req.UserID}, nil
}

func main() {
	niohHost := os.Getenv("NIOH_HOST")
	if niohHost == "" {
		logrus.Fatal("NIOH_HOST must be set")
	}
	// Add timestamp to log events in Common Log Format Date
	logrus.SetFormatter(&logrus.TextFormatter{FullTimestamp: true, TimestampFormat: "02/Jan/2006:15:04:05 -0700"})

	handler := handler{
		nioh: nioh.NewAPIProtobufClient(niohHost, &http.Client{}),
	}

	lambda.Start(handler.Handler)
}
