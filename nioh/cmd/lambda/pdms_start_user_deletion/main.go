package main

import (
	"context"
	"encoding/json"
	"fmt"
	"os"

	eventbus "code.justin.tv/eventbus/client"
	"code.justin.tv/eventbus/client/subscriber/lambdafunc"
	"code.justin.tv/eventbus/schema/pkg/user"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sfn"
	"github.com/aws/aws-sdk-go/service/sfn/sfniface"
	"github.com/gofrs/uuid"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

type handler struct {
	sfnClient sfniface.SFNAPI
	sfnARN    string
}

type sfnPayload struct {
	UserID string `json:"user_id"`
}

func (h *handler) Handler(ctx context.Context, header *eventbus.Header, event *user.Destroy) error {
	if event == nil {
		return errors.New("userdestroy: nil eventbus event")
	}

	userID := event.GetUserId()
	if userID == "" {
		return errors.New("userdestroy: userID cannot be empty")
	}

	jsonPayload := sfnPayload{userID}
	uuid, err := uuid.DefaultGenerator.NewV4()
	if err != nil {
		return errors.Wrap(err, "failed to generate new uuid")
	}

	inputBytes, err := json.Marshal(jsonPayload)
	if err != nil {
		return errors.Wrapf(err, "Failed to marshall state machine execution input. stateMachineARN: %s executionInput: %+v", h.sfnARN, jsonPayload)
	}

	executionName := fmt.Sprintf("%s-%s", userID, uuid.String())
	_, err = h.sfnClient.StartExecutionWithContext(ctx, &sfn.StartExecutionInput{
		Input:           aws.String(string(inputBytes)),
		Name:            aws.String(executionName),
		StateMachineArn: aws.String(h.sfnARN),
	})
	if err != nil {
		logrus.WithError(err).Error("failed to run execution")
		return err
	}

	return nil
}

func main() {
	sfnARN := os.Getenv("SFN_ARN")
	if sfnARN == "" {
		logrus.Fatal("SFN_ARN must be set")
	}
	// Add timestamp to log events in Common Log Format Date
	logrus.SetFormatter(&logrus.TextFormatter{FullTimestamp: true, TimestampFormat: "02/Jan/2006:15:04:05 -0700"})

	sess, err := session.NewSession(aws.NewConfig().WithRegion("us-west-2"))
	if err != nil {
		logrus.Fatalf("failed to create aws session")
		return
	}

	sfnClient := sfn.New(sess)
	h := handler{
		sfnClient: sfnClient,
		sfnARN:    sfnARN,
	}

	mux := eventbus.NewMux()
	user.RegisterDestroyHandler(mux, h.Handler)
	lambda.Start(lambdafunc.NewSQS(mux.Dispatcher()))
}
