package main

import (
	"context"
	"testing"

	"code.justin.tv/commerce/nioh/mocks"
	"code.justin.tv/eventbus/schema/pkg/user"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func TestHandler(t *testing.T) {
	tests := []struct {
		scenario  string
		setup     func() handler
		input     *user.Destroy
		shouldErr bool
	}{
		{
			scenario: "when the handler is invoked with an invalid input",
			setup: func() handler {
				mockSFN := new(mocks.SFNAPI)
				return handler{
					sfnClient: mockSFN,
				}
			},
			input:     nil,
			shouldErr: true,
		},
		{
			scenario: "when the event has no user ID",
			setup: func() handler {
				mockSFN := new(mocks.SFNAPI)
				return handler{
					sfnClient: mockSFN,
				}
			},
			input: &user.Destroy{
				UserId: "",
			},
			shouldErr: true,
		},
		{
			scenario: "when the input is valid",
			setup: func() handler {
				mockSFN := new(mocks.SFNAPI)
				mockSFN.On("StartExecutionWithContext", mock.Anything, mock.Anything).Return(nil, nil)
				return handler{
					sfnClient: mockSFN,
				}
			},
			input: &user.Destroy{
				UserId: "test-user",
			},
			shouldErr: false,
		},
	}

	for _, test := range tests {
		test := test

		t.Run(test.scenario, func(t *testing.T) {
			t.Parallel()

			h := test.setup()
			err := h.Handler(context.Background(), nil, test.input)
			if test.shouldErr {
				assert.NotNil(t, err)
			} else {
				assert.Nil(t, err)
			}
		})
	}
}
