package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"runtime"
	"time"

	"sync"

	"code.justin.tv/commerce/config"
	log "code.justin.tv/commerce/logrus"
	niohConfig "code.justin.tv/commerce/nioh/config"
	"code.justin.tv/commerce/nioh/internal/backend"
	"code.justin.tv/commerce/nioh/internal/clients"
	"code.justin.tv/commerce/nioh/internal/models"
	"code.justin.tv/commerce/nioh/internal/orch/activity"
	"code.justin.tv/commerce/nioh/internal/orch/autotag"
	kinesismanager "code.justin.tv/commerce/nioh/internal/orch/kinesis"
	"code.justin.tv/commerce/nioh/internal/orch/streamevent"
	"code.justin.tv/commerce/nioh/internal/server"
	"code.justin.tv/commerce/nioh/pkg/dynamodb/dao"
	"code.justin.tv/commerce/nioh/pkg/errorlogger"
	"code.justin.tv/commerce/nioh/pkg/exemptions"
	"code.justin.tv/commerce/nioh/pkg/helpers"
	"code.justin.tv/commerce/nioh/pkg/stats"
	"code.justin.tv/commerce/rollrus"
	"code.justin.tv/common/gometrics"
	"code.justin.tv/video/autoprof/profs3"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/kinesis"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/secretsmanager"
	"github.com/aws/aws-sdk-go/service/sfn"
	"github.com/aws/aws-sdk-go/service/sfn/sfniface"
	"github.com/aws/aws-sdk-go/service/sns"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/aws/aws-sdk-go/service/sqs/sqsiface"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/gofrs/uuid"
	"github.com/pkg/errors"
	"github.com/twitchscience/kinsumer"
	kinsumerstatsd "github.com/twitchscience/kinsumer/statsd"
)

const (
	// this file contains config options deployed by sandstorm
	namespace    = "nioh"
	organization = "commerce"

	// File paths overrides for file-based configurations
	localFilePath  = "/src/code.justin.tv/%s/%s/config/%s.json"
	globalFilePath = "/config/%s.json"
)

var (
	ballast      []byte
	ballastBytes = map[string]int{
		niohConfig.ProductionEnv:  3 << 30, // 3GB/8GB from c5.xlarge
		niohConfig.BetaEnv:        3 << 29, // 1.5GB/4GB from c5.large
		niohConfig.DevelopmentEnv: 0,
	}
)

func main() {
	env := config.GetEnvironment()
	canonicalEnv := niohConfig.GetCanonicalEnv()
	useDAX := os.Getenv("BYPASS_DAX") != "true"

	// Log
	{
		logLevel := log.DebugLevel
		if env == niohConfig.ProductionEnv || env == niohConfig.CanaryEnv {
			logLevel = log.InfoLevel
		}

		log.SetLevel(logLevel)
		log.SetFormatter(&log.TextFormatter{FullTimestamp: true, TimestampFormat: "02/Jan/2006:15:04:05 -0700"})

		log.Infof("Running Nioh in environment: %s", env)
		log.Infof("Nioh built with Go version %s", runtime.Version())
	}

	// Block Profiling
	{
		if env == niohConfig.CanaryEnv {
			runtime.SetBlockProfileRate(1)
		}
	}

	// Memory Ballast
	{
		log.Info("Allocating memory ballast")
		// Intentionally initialized and not used, forces a large allocation on to the heap
		ballast = makeBallast(canonicalEnv)
		log.Info("Finished allocating memory ballast")
	}

	// Config
	cfg := niohConfig.Configuration{}
	{
		// Setting local path to development.json as the default would be dev.json
		localPathOverride := os.Getenv("GOPATH") + fmt.Sprintf(localFilePath, organization, namespace, canonicalEnv)
		globalPathOverride := fmt.Sprintf(globalFilePath, env)
		if err := config.LoadConfigForEnvironment(&cfg, &config.Options{
			RepoOrganization:   organization,
			RepoService:        namespace,
			Environment:        env,
			LocalPathOverride:  localPathOverride,
			GlobalPathOverride: globalPathOverride,
		}); err != nil {
			log.Fatal(err)
		}
		cfg.ParseAllowlist()
		cfg.ParsePreviewOrigin()
	}

	// Stats / Metrics
	statter, err := stats.SetupStats(cfg, env)
	if err != nil {
		log.Fatalf("Failed to initialize stats: %s", err)
	}

	gometrics.Monitor(statter, 5*time.Second)

	// Circuit Manager
	circuitManager := helpers.MakeCircuitManager(statter)

	// Dynamic Config
	var dynamicCfg niohConfig.IDynamicConfiguration
	{
		if niohConfig.IsLocalEnv() {
			// Try to load from a local source
			dynamicCfg, err = niohConfig.NewDynamicConfigurationFromLocalFile(canonicalEnv, statter)
			if err != nil {
				log.WithError(err).Warn("Failed to load local dynamic config in a dev environment. Falling back to s3.")
			} else {
				log.Info("Dynamic config is loaded from a local file. No polling will be done. Restart server if you make changes to the dynamic config file.")
			}
		}

		if dynamicCfg == nil {
			dynamicCfg, err = niohConfig.NewDynamicConfiguration(canonicalEnv, statter)
			if err != nil {
				log.WithError(err).Fatal("DynamicConfiguration failed to initialize during deployment")
			}
			defer dynamicCfg.Shutdown()
		}
	}

	// Clients
	awsSession := session.Must(session.NewSession(aws.NewConfig().WithRegion(cfg.AWSRegion)))
	clientInstances, err := clients.SetupClients(&cfg, statter, circuitManager, dynamicCfg, awsSession)
	if err != nil {
		log.Fatal(err)
	}

	// Setup Autoprof
	setupAutoprof(cfg)

	// Secrets
	secrets, err := configureSecretsManager()
	if err != nil {
		log.Fatal(err)
	}
	rollbarToken, err := fetchRollbarToken(env, secrets)
	if err != nil {
		log.Fatal(err)
	}

	// Rollbar
	err = setupRollbar(env, rollbarToken)
	if err != nil {
		log.Fatal(err)
	}

	errorLogger := configureErrorLogger(rollbarToken, statter)

	stats.ConfigureHystrixMetricCollector(cfg)

	daos, err := dao.SetupDAOs(cfg, circuitManager, useDAX, statter)
	if err != nil {
		log.Fatal(err)
	}

	clock := &models.RealClock{}

	exemptionChecker := &exemptions.ExemptionChecker{
		Clients: clientInstances,
		Clock:   clock,
		Stats:   statter,
	}

	var snsPublisher clients.SNSPublisher
	var sqsClient sqsiface.SQSAPI
	var sfnClient sfniface.SFNAPI
	{
		snsPublisher = sns.New(awsSession)
		sqsClient = sqs.New(awsSession)
		sfnClient = sfn.New(awsSession)
	}

	// Set up autotagger for transmitting autotag SNS messages to Graffiti when necessary.
	autotagger := autotag.NewAutotagger(&autotag.AutotaggerInput{
		Config:         &cfg,
		SNSPublisher:   snsPublisher,
		Clock:          clock,
		Stats:          statter,
		CircuitManager: circuitManager,
	})

	// Set up backend instance responsible for business logic.
	backendInstance := backend.NewBackend(&backend.NewBackendInput{
		Config:           &cfg,
		Clients:          clientInstances,
		Stats:            statter,
		Errorlogger:      errorLogger,
		DAOs:             daos,
		Clock:            clock,
		ExemptionChecker: exemptionChecker,
		Autotagger:       autotagger,
	})

	// Set up offline cleanup activity worker which handles Step Function task states for performing scheduled cleanups.
	offlineCleanupActivityWorker, err := activity.NewOfflineCleanupActivityWorker(&cfg, sfnClient, backendInstance, clientInstances)
	if err != nil {
		log.Fatal(err)
	}
	offlineCleanupActivityWorker.Start(context.TODO())

	// Set up offline cleanup scheduler which consumes stream events and schedules restriction cleanup when necessary.
	offlineCleanupScheduler := streamevent.NewOfflineCleanupScheduler(&streamevent.OfflineCleanupSchedulerInput{
		Config:      &cfg,
		SFNExecutor: sfnClient,
		Clock:       clock,
		Backend:     backendInstance,
		Clients:     clientInstances,
	})

	// Set up online autotag handler that updates the tag again after Graffiti deletes it after its TTL
	onlineAutotagHandler := streamevent.NewOnlineAutotagHandler(autotagger, backendInstance)

	// Set up stream event processor which reads and delegates SQS messages representing stream lifecycle events.
	streamEventProcessor, err := streamevent.NewProcessor(&cfg, sqsClient, statter, []streamevent.Handler{offlineCleanupScheduler, onlineAutotagHandler})
	if err != nil {
		log.Fatal(err)
	}
	if err := streamEventProcessor.Start(context.TODO()); err != nil {
		log.Fatal(err)
	}

	// Set up and start Kinesis consumer for Vinyl VOD creation events.
	kinsumerClient, err := setupVinylKinsumerService(cfg, statter)
	if err != nil {
		log.Fatal(err)
	}

	var wg sync.WaitGroup
	err = kinsumerClient.Run()
	if err != nil {
		log.Fatalf("kinsumer.Kinsumer.Run() returned error: %v", err)
	}

	defer func() {
		log.Info("Waiting for the consumer routines to finish")
		wg.Wait()
		log.Info("Consumer routines have finished")
	}()
	defer func() {
		log.Info("Stopping kinsumer client")
		kinsumerClient.Stop()
	}()

	records := make(chan []byte, 1)
	wg.Add(2)

	go func() { defer wg.Done(); kinesismanager.ConsumeRecords(kinsumerClient, records) }()
	go func() { defer wg.Done(); kinesismanager.ProcessRecords(backendInstance, records) }()

	// Init HTTP Server
	var niohServer *server.Server
	{
		serverConfig := server.ServerConfig{
			Backend:       backendInstance,
			Clients:       clientInstances,
			Config:        &cfg,
			DynamicConfig: dynamicCfg,
			Errorlogger:   errorLogger,
			Stats:         statter,
			S2SName:       fmt.Sprintf("%s-%s", namespace, canonicalEnv),
			UseDAX:        useDAX,
		}
		niohServer, err = server.NewServer(serverConfig)
		if err != nil {
			log.Fatalf("Failed to initialize the server: %v", err)
		}
	}

	go func() {
		err = niohServer.ListenAndServe()
		if err != nil && err != http.ErrServerClosed {
			log.Fatal(err)
		}
	}()

	// Graceful shutdown
	interruptChannel := make(chan os.Signal, 1)
	signal.Notify(interruptChannel, os.Interrupt)
	<-interruptChannel

	log.Info("Received SIGINT, starting teardown...")

	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	if err := offlineCleanupActivityWorker.Shutdown(ctx); err != nil {
		log.Fatalf("Failed to gracefully shut down offline cleanup activity worker: %v", err)
	}

	if err := streamEventProcessor.Shutdown(ctx); err != nil {
		log.Fatalf("Failed to gracefully shut down stream event processor: %v", err)
	}

	if err := niohServer.Shutdown(ctx); err != nil {
		log.Fatalf("Failed to gracefully shutdown server: %v", err)
	}

	log.Info("Shutdown complete")
}

// Creates a large heap allocation - See https://blog.twitch.tv/go-memory-ballast-how-i-learnt-to-stop-worrying-and-love-the-heap-26c2462549a2
func makeBallast(environment string) []byte {
	return make([]byte, ballastBytes[environment])
}

func fetchRollbarToken(env string, secrets *secretsmanager.SecretsManager) (string, error) {
	output, err := secrets.GetSecretValue(&secretsmanager.GetSecretValueInput{
		SecretId: aws.String("nioh-rollbar-token"),
	})
	if err != nil {
		return "", err
	}
	rollbarToken := *output.SecretString

	// extract token string and setup rollbar hook
	var secretJSONMap map[string]string
	err = json.Unmarshal([]byte(rollbarToken), &secretJSONMap)
	if err != nil {
		return "", err
	}

	return secretJSONMap["nioh-rollbar-token"], nil
}

func setupRollbar(env string, rollbarTokenString string) error {
	if env == niohConfig.DevelopmentEnv {
		return nil
	}

	// Setup rollbar hook
	rollrus.SetupLogging(rollbarTokenString, env)
	log.StandardLogger().SetNoLock()
	log.SetLevel(log.WarnLevel)
	return nil
}

func configureSecretsManager() (*secretsmanager.SecretsManager, error) {
	// This is a bad solution, but previous `config.Resolve("aws-region")` was failing.
	sess, err := session.NewSession(&aws.Config{
		Region: aws.String("us-west-2"),
	})
	if err != nil {
		return nil, err
	}

	return secretsmanager.New(sess), nil
}

func configureErrorLogger(token string, statter statsd.Statter) errorlogger.ErrorLogger {
	env := config.GetEnvironment()
	return errorlogger.NewErrorLogger(token, env, statter)
}

func setupAutoprof(cfg niohConfig.Configuration) {
	sess, err := session.NewSession(aws.NewConfig().WithRegion(cfg.AWSRegion))
	if err != nil {
		log.WithError(err).Error("failed to create new AWS session while bootstrapping autoprof")
		return
	}

	collector := profs3.Collector{
		S3:       s3.New(sess),
		S3Bucket: cfg.AutoprofS3BucketName,
		OnError: func(err error) error {
			log.WithError(err).Error("error from autoprof S3 collector")
			return nil
		},
	}

	go func() {
		if err := collector.Run(context.Background()); err != nil {
			log.WithError(err).Error("failed to run the autoprof collector")
		}
	}()
}

func setupVinylKinsumerService(cfg niohConfig.Configuration, stats statsd.Statter) (*kinsumer.Kinsumer, error) {
	applicationName := fmt.Sprintf("%s-vinyl", niohConfig.GetCanonicalEnv())

	// Kinsumer needs a way to differentiate between running clients, generally you want to use information
	// about the machine it is running on like ip. We'll use a UUID
	clientName, err := uuid.NewV4()
	if err != nil {
		return nil, errors.Wrap(err, "generating uuid for client name")
	}

	conf := kinsumer.NewConfig().WithStats(kinsumerstatsd.NewWithStatter(stats))
	return kinsumer.NewWithInterfaces(newKinesisSession(cfg), newDynamoDBSession(cfg), cfg.VinylKinesisStream, applicationName, clientName.String(), conf)
}

// New Dynamo session just for Kinesis consumer to use (does not rely on DAX or our DAOs)
func newDynamoDBSession(cfg niohConfig.Configuration) *dynamodb.DynamoDB {
	sess := session.Must(session.NewSession(aws.NewConfig().WithRegion(cfg.AWSRegion), &aws.Config{
		HTTPClient: httpClient(1000),
	}))

	return dynamodb.New(sess)
}

func newKinesisSession(cfg niohConfig.Configuration) *kinesis.Kinesis {
	kinesisSession := session.Must(session.NewSession(aws.NewConfig().WithRegion(cfg.AWSRegion), &aws.Config{
		HTTPClient: httpClient(1000),
	}))

	return kinesis.New(kinesisSession)
}

func httpClient(maxIdleConnsPerHost int) *http.Client {
	return &http.Client{
		Transport: &http.Transport{
			Proxy:                 http.ProxyFromEnvironment,
			MaxIdleConnsPerHost:   maxIdleConnsPerHost,
			IdleConnTimeout:       90 * time.Second,
			TLSHandshakeTimeout:   10 * time.Second,
			ExpectContinueTimeout: 1 * time.Second,
		},
	}
}
