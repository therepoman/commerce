package main

import (
	"testing"

	"code.justin.tv/commerce/nioh/config"
	configMocks "code.justin.tv/commerce/nioh/config/mocks"
	"code.justin.tv/commerce/nioh/internal/backend"
	"code.justin.tv/commerce/nioh/internal/clients"
	"code.justin.tv/commerce/nioh/internal/server"
	"code.justin.tv/commerce/nioh/pkg/dynamodb/dao"
	"code.justin.tv/commerce/splatter"

	"github.com/cactus/go-statsd-client/statsd"
	. "github.com/smartystreets/goconvey/convey"
)

func TestCmdMain(t *testing.T) {
	// Most of this is testing that the basic setup calls in main aren't invalid
	// or leading to immediate/fatal crashes.
	Convey("Testing main", t, func() {
		statter, _ := splatter.NewCompositeStatter([]statsd.Statter{})

		Convey("when configuring error logger", func() {
			_ = configureErrorLogger("", statter)
		})
		Convey("when configuring secrets manager", func() {
			_, err := configureSecretsManager()
			So(err, ShouldBeNil)
		})
		Convey("when configuring backend", func() {
			logger := configureErrorLogger("", statter)
			clients := &clients.Clients{}
			daos := &dao.DAOs{}
			testBackend := backend.NewBackend(&backend.NewBackendInput{
				Clients:     clients,
				Stats:       statter,
				Errorlogger: logger,
				DAOs:        daos,
			})
			So(testBackend, ShouldNotBeNil)
		})

		Convey("when configuring server", func() {
			logger := configureErrorLogger("", statter)

			clients := &clients.Clients{}
			daos := &dao.DAOs{}
			cfg := &config.Configuration{}
			dynamicCfg := new(configMocks.IDynamicConfiguration)
			testBackend := backend.NewBackend(&backend.NewBackendInput{
				Clients:     clients,
				Stats:       statter,
				Errorlogger: logger,
				DAOs:        daos,
			})
			serverCfg := server.ServerConfig{
				Backend:       testBackend,
				Clients:       clients,
				Config:        cfg,
				DynamicConfig: dynamicCfg,
				Errorlogger:   logger,
				Stats:         statter,
			}
			server, err := server.NewServer(serverCfg)
			So(err, ShouldBeNil)
			So(server, ShouldNotBeNil)
		})
	})
}
