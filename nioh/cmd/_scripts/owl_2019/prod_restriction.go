package main

import (
	"time"

	"code.justin.tv/commerce/nioh/internal/models"
)

var pacificTimeZone, tzErr = time.LoadLocation("America/Los_Angeles")

func init() {
	if tzErr != nil {
		panic(tzErr)
	}
}

var prodAdminExemption = &models.Exemption{
	ExemptionType:   models.SiteAdminExemptionType,
	ActiveStartDate: time.Date(2019, time.February, 14, 18, 0, 0, 0, time.UTC),
	ActiveEndDate:   time.Date(2020, time.February, 14, 18, 0, 0, 0, time.UTC),
	Keys:            nil,
}

var prodPassExemption = &models.Exemption{
	ExemptionType:   models.ProductExemptionType,
	ActiveStartDate: time.Date(2019, time.February, 14, 18, 0, 0, 0, time.UTC),
	ActiveEndDate:   time.Date(2020, time.February, 14, 18, 0, 0, 0, time.UTC),
	Keys:            []string{"1189177"},
}

// See https://jira.twitch.com/browse/REWARD-2195 <- outdated
// See https://jira.twitch.com/browse/REWARD-2772
var freeTrialSchedule = []*models.Exemption{
	{
		ExemptionType:   models.AllExemptionType,
		ActiveStartDate: time.Date(2019, time.September, 5, 10, 0, 0, 0, pacificTimeZone),
		ActiveEndDate:   time.Date(2019, time.September, 7, 10, 0, 0, 0, pacificTimeZone),
		Keys:            nil,
	},
	{
		ExemptionType:   models.AllExemptionType,
		ActiveStartDate: time.Date(2019, time.September, 12, 10, 0, 0, 0, pacificTimeZone),
		ActiveEndDate:   time.Date(2019, time.September, 16, 10, 0, 0, 0, pacificTimeZone),
		Keys:            nil,
	},
}

func getProdRestrictionForChannel(channelID string) *models.RestrictionV2 {
	var exemptions = []*models.Exemption{
		prodPassExemption,
		prodAdminExemption,
	}

	exemptions = append(exemptions, freeTrialSchedule...)

	return &models.RestrictionV2{
		ResourceKey:     models.Channel{ID: channelID}.GetResourceKey(),
		RestrictionType: "ALL_ACCESS_PASS",
		Exemptions:      exemptions,
	}
}
