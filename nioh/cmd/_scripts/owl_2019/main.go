package main

import (
	"context"
	"flag"
	"fmt"

	"code.justin.tv/commerce/nioh/config"
	"code.justin.tv/commerce/nioh/pkg/dynamodb"
	"code.justin.tv/commerce/nioh/pkg/dynamodb/dao"
	dbprovider "code.justin.tv/commerce/nioh/pkg/dynamodb/provider"
	"code.justin.tv/commerce/nioh/pkg/helpers"
	"github.com/cactus/go-statsd-client/statsd"
	gdb "github.com/guregu/dynamo"
)

// overwatchleague
var owlProdChanletIDs = []string{
	"409525958",
	"409526420",
	"409526428",
	"409526433",
	"409526440",
	"409526454",
	"409526458",
	"409526460",
	"409526463",
	"409526692",
	"409526698",
	"409526705",
	"409526708",
	"409526714",
	"409526721",
	"409526725",
	"409526738",
	"409526746",
	"409526750",
	"409528197",
	"409528201",
	"409528206",
	"409528212",
	"409528217",
	"409528220",
}

var dry = flag.Bool("dry", false, "print restrictions instead of applying them")

// USAGE Copy-Pasta:
// mwinit --aea
// env ENVIRONMENT=production AWS_REGION=us-west-2 AWS_SDK_LOAD_CONFIG=1 AWS_PROFILE=twitch-nioh-aws go run ./cmd/_scripts/owl_2019/main.go ./cmd/_scripts/owl_2019/prod_restriction.go [-dry]
func main() {
	flag.Parse()

	db, err := dynamodb.New(config.Configuration{
		AWSRegion: "us-west-2",
	}, false)
	if err != nil {
		panic("couldn't make dynamo client")
	}

	statter, _ := statsd.NewNoopClient()
	circuitManager := helpers.MakeCircuitManager(statter)
	provider := registerClient(db)
	restrictionsDAO := dao.NewRestrictionDAO(*provider, circuitManager, statter)

	for _, chanletID := range owlProdChanletIDs {
		r := getProdRestrictionForChannel(chanletID)

		if *dry {
			fmt.Printf("Would put restriction:\n")
			fmt.Printf("%+v\n", r)
			fmt.Printf("  with exemptions:\n")
			for _, ex := range r.Exemptions {
				fmt.Printf("  %+v\n", ex)
			}
			fmt.Println()
		} else {
			err := restrictionsDAO.SetRestriction(context.Background(), r)
			if err != nil {
				fmt.Println("ERROR: Failed to set restriction for chanletID=" + chanletID)
				fmt.Println(err)
			} else {
				fmt.Println("SUCCESS: Set restriction for chanletID=" + chanletID)
			}
		}

	}
}

func registerClient(client *gdb.DB) *dbprovider.Provider {
	provider := dbprovider.New()
	provider.RegisterFallbackClient(client)
	provider.RegisterClient(dbprovider.DynamoKey, client)
	return &provider
}
