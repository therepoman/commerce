package main

import (
	"fmt"

	"github.com/guregu/dynamo"
)

type hedwigDAO struct {
	table  dynamo.Table
	client *dynamo.DB
}

type hedwigData struct {
	ResourceID     string `dynamo:",hash"`
	HandlerData    handlerData
	HandlerSubType string
	ResourceType   string
	Type           string
	UpdatedAt      string
}

type handlerData struct {
	ProductName     string `dynamo:"productName"`
	RestrictionType string `dynamo:"restrictionType"`
	Title           string `dynamo:"title"`
}

func newHedwigDAO(client *dynamo.DB, tableName string) *hedwigDAO {
	return &hedwigDAO{
		client: client,
		table:  client.Table(tableName),
	}
}

func (d *hedwigDAO) GetRestrictions() ([]hedwigData, error) {
	var result []hedwigData
	var cc dynamo.ConsumedCapacity
	err := d.table.Scan().Consistent(true).ConsumedCapacity(&cc).All(&result)
	if err != nil {
		return nil, err
	}

	fmt.Printf("found %d items", len(result))

	return result, nil
}
