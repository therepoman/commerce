package main

import (
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"os"

	"code.justin.tv/commerce/nioh/config"
	"code.justin.tv/commerce/nioh/internal/models"
	"code.justin.tv/commerce/nioh/pkg/dynamodb"
	"code.justin.tv/commerce/nioh/pkg/dynamodb/dao"
	dbprovider "code.justin.tv/commerce/nioh/pkg/dynamodb/provider"
	"code.justin.tv/commerce/nioh/pkg/helpers"
	"github.com/cactus/go-statsd-client/statsd"
	db "github.com/guregu/dynamo"
)

const (
	prodEnv = "production"
	devEnv  = "dev"

	devProfile  = "twitch-nioh-dev"
	prodProfile = "twitch-nioh-aws"

	owl2018TPID = "689376"
	owl2019TPID = "1189177"

	owl2018Title = "All-Access Pass 2018"
	owl2019Title = "All-Access Pass 2019"
)

var (
	input = flag.String("file", "", "input file of a json encoded data dump of Hedwig restriction data.")
	pull  = flag.Bool("pull", false, "pull data from Hedwig")
	dry   = flag.Bool("dry", false, "print restrictions instead of applying them")
	env   = flag.String("env", "", "environment to run in")
)

// USAGE: go run ./cmd/_scripts/hedwig_migration/main.go ./cmd/_scripts/hedwig_migration/hedwig_dao.go -file hedwig.json [-dry] [-pull]
func main() {
	flag.Parse()

	// Just fetch latest Hedwig data with -pull flag
	if *pull {
		os.Setenv("AWS_PROFILE", "twitch-vodentitle-aws")
		fetchHedwigData()
		os.Exit(0)
	}

	if *input == "" {
		fmt.Println("You must provide a path containing Hedwig restriction data with the -file flag")
		os.Exit(1)
	}

	var awsProfile string
	switch *env {
	case prodEnv:
		awsProfile = prodProfile
	case devEnv:
		awsProfile = devProfile
	default:
		fmt.Println("You must provide an environment")
		os.Exit(1)
	}

	os.Setenv("AWS_PROFILE", awsProfile)
	os.Setenv("ENVIRONMENT", *env)

	dbClient, err := newDynamoClient()
	if err != nil {
		panic("couldn't make dynamo client")
	}

	fmt.Printf("Opening input file at %s...\n", *input)
	inFile, err := os.Open(*input)
	if err != nil {
		fmt.Printf("failed to open input file: %v\n", err)
		os.Exit(1)
	}
	defer inFile.Close()

	var data []*hedwigData
	bytes, err := ioutil.ReadAll(inFile)
	if err != nil {
		fmt.Printf("failed to open input file: %v", err)
	}

	json.Unmarshal(bytes, &data)

	fmt.Printf("Found %d entires\n", len(data))

	statter, _ := statsd.NewNoopClient()
	circuitManager := helpers.MakeCircuitManager(statter)
	dbprovider := registerClient(dbClient)

	restrictionDAO := dao.NewRestrictionDAO(*dbprovider, circuitManager, statter)
	clock := &models.RealClock{}

	// Convert Hedwig data into V2 Restriction and upload
	written := 0
	for _, restriction := range data {
		resourceKey := models.VOD{ID: restriction.ResourceID}.GetResourceKey()

		var key string
		var title string
		if restriction.HandlerData.ProductName == "owlallaccess2018" {
			key = owl2018TPID
			title = owl2018Title
		} else if restriction.HandlerData.ProductName == "owlallaccess2019" {
			key = owl2019TPID
			title = owl2019Title
		} else {
			fmt.Printf("unexpected product type: %s, skipping...\n", restriction.HandlerData.ProductName)
			continue
		}

		restrictionV2 := &models.RestrictionV2{
			ResourceKey:     resourceKey,
			RestrictionType: models.AllAccessPassRestrictionType,
			Exemptions: []*models.Exemption{
				{
					ExemptionType:   models.ProductExemptionType,
					ActiveStartDate: clock.Now(),
					ActiveEndDate:   models.IndefiniteEndDate,
					Keys: []string{
						key,
					},
					Actions: []*models.ExemptionAction{
						{
							Name:  restriction.HandlerData.ProductName,
							Title: title,
						},
					},
				},
			},
		}

		// Dry run, do not write to dynamo
		if *dry {
			fmt.Println(prettyPrint(restrictionV2))
			continue
		}

		if err := restrictionDAO.SetRestriction(context.Background(), restrictionV2); err != nil {
			fmt.Printf("error writing item %s to dao: %v", prettyPrint(restrictionV2), err)
		}
		written++
	}

	fmt.Printf("wrote %d to dynamo\n", written)

	os.Exit(0)
}

func fetchHedwigData() {
	dbClient, err := newDynamoClient()
	if err != nil {
		panic("couldn't make dynamo client")
	}

	var tableName string
	switch *env {
	case prodEnv:
		tableName = "hedwig-production"
	case devEnv:
		tableName = "hedwig-staging"
	default:
		fmt.Println("You must provide an environment")
		os.Exit(1)
	}

	hedwigDAO := newHedwigDAO(dbClient, tableName)
	data, err := hedwigDAO.GetRestrictions()
	if err != nil {
		fmt.Printf("failed to get data from %s: %v\n", tableName, err)
		os.Exit(1)
	}

	file, err := json.MarshalIndent(data, "", "  ")
	if err != nil {
		fmt.Printf("failed to marshal hedwig data: %v\n", err)
	}

	err = ioutil.WriteFile("./cmd/_scripts/hedwig_migration/hedwig.json", file, 0644)
	if err != nil {
		fmt.Printf("failed to write to file: %v", err)
	}
}

func newDynamoClient() (*db.DB, error) {
	return dynamodb.New(config.Configuration{
		AWSRegion: "us-west-2",
	}, false)
}

func registerClient(client *db.DB) *dbprovider.Provider {
	provider := dbprovider.New()
	provider.RegisterFallbackClient(client)
	provider.RegisterClient(dbprovider.DynamoKey, client)
	return &provider
}

// Helper to print structs to console in a human readable way
func prettyPrint(i interface{}) string {
	s, _ := json.MarshalIndent(i, "", "  ")
	return string(s)
}
