package server

import (
	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/nioh/internal/backend"
	"code.justin.tv/commerce/nioh/internal/models"
	"code.justin.tv/commerce/nioh/rpc/nioh"
	"code.justin.tv/common/chitin"
	"github.com/twitchtv/twirp"
	"golang.org/x/net/context"
)

// CreateContentAttributes creates new content attributes
func (s *Server) CreateContentAttributes(ctx context.Context, req *nioh.CreateContentAttributesRequest) (*nioh.CreateContentAttributesResponse, error) {
	log := logrus.WithField("RequestID", chitin.GetTraceID(ctx))

	if req.ChannelId == "" {
		return nil, twirp.RequiredArgumentError("ChannelID")
	}
	if len(req.ContentAttributes) == 0 {
		return nil, twirp.InvalidArgumentError("ContentAttributes", "requires at least 1 content attribute to create")
	}

	results, err := s.backend.CreateContentAttributes(
		ctx,
		&backend.ContentAttributesBulkParams{
			ChannelID:         req.ChannelId,
			ContentAttributes: models.ContentAttributesFromProtos(ctx, req.ContentAttributes),
		})

	if err != nil {
		log.WithError(err).Error("CreateContentAttributes encountered failure while creating content attributes")
		return nil, twirp.InternalError("Failed to create new content attributes")
	}

	return &nioh.CreateContentAttributesResponse{
		SucceededCreates: models.ContentAttributesToProtos(ctx, results.Success),
		FailedCreates:    models.ContentAttributesToProtos(ctx, results.Fail),
	}, nil
}

// DeleteContentAttributes deletes content attributes
func (s *Server) DeleteContentAttributes(ctx context.Context, req *nioh.DeleteContentAttributesRequest) (*nioh.DeleteContentAttributesResponse, error) {
	log := logrus.WithField("RequestID", chitin.GetTraceID(ctx))

	if req.ChannelId == "" {
		return nil, twirp.RequiredArgumentError("ChannelID")
	}
	if len(req.ContentAttributes) == 0 {
		return nil, twirp.InvalidArgumentError("ContentAttributes", "requires at least 1 content attribute to delete")
	}

	results, err := s.backend.DeleteContentAttributes(
		ctx,
		&backend.ContentAttributesBulkParams{
			ChannelID:         req.ChannelId,
			ContentAttributes: models.ContentAttributesFromProtos(ctx, req.ContentAttributes),
		})
	if err != nil {
		log.WithError(err).Error("DeleteContentAttributes encountered failure while deleting content attributes")
		return nil, twirp.InternalError("Failed to delete content attributes")
	}

	return &nioh.DeleteContentAttributesResponse{
		SucceededDeletes: models.ContentAttributesToProtos(ctx, results.Success),
		FailedDeletes:    models.ContentAttributesToProtos(ctx, results.Fail),
	}, nil
}

func idsToModelContentAttributes(ids []string) []*models.ContentAttribute {
	contentAttributes := make([]*models.ContentAttribute, 0, len(ids))
	for _, id := range ids {
		contentAttributes = append(contentAttributes, &models.ContentAttribute{ID: id})
	}

	return contentAttributes
}

// GetContentAttributesForChannel retrieves content attributes for a given channel
func (s *Server) GetContentAttributesForChannel(ctx context.Context, req *nioh.GetContentAttributesForChannelRequest) (*nioh.GetContentAttributesForChannelResponse, error) {
	log := logrus.WithField("RequestID", chitin.GetTraceID(ctx))

	if req.ChannelId == "" {
		return nil, twirp.RequiredArgumentError("ChannelID")
	}

	contentAttributes, err := s.backend.GetContentAttributesForChannel(ctx, req.ChannelId)
	if err != nil {
		log.WithError(err).Error("GetContentAttributesForChannel encountered failure while retrieving content attributes")
		return nil, twirp.InternalError("Failed to retrieve content attributes")
	}

	return &nioh.GetContentAttributesForChannelResponse{
		ContentAttributes: models.ContentAttributesToProtos(ctx, contentAttributes),
	}, nil
}

// UpdateContentAttributes updates existing content attributes with new values
func (s *Server) UpdateContentAttributes(ctx context.Context, req *nioh.UpdateContentAttributesRequest) (*nioh.UpdateContentAttributesResponse, error) {
	log := logrus.WithField("RequestID", chitin.GetTraceID(ctx))

	if req.ChannelId == "" {
		return nil, twirp.RequiredArgumentError("ChannelID")
	}
	if len(req.ContentAttributes) == 0 {
		return nil, twirp.InvalidArgumentError("ContentAttributes", "requires at least 1 content attribute to update")
	}

	results, err := s.backend.UpdateContentAttributes(
		ctx,
		&backend.ContentAttributesBulkParams{
			ChannelID:         req.ChannelId,
			ContentAttributes: models.ContentAttributesFromProtos(ctx, req.ContentAttributes),
		})
	if err != nil {
		log.WithError(err).Error("UpdateContentAttributes encountered failure while updating content attributes")
		return nil, twirp.InternalError("Failed to update content attributes")
	}

	return &nioh.UpdateContentAttributesResponse{
		SucceededUpdates: models.ContentAttributesToProtos(ctx, results.Success),
		FailedUpdates:    models.ContentAttributesToProtos(ctx, results.Fail),
	}, nil
}

// CreateContentAttributeImageUploadConfig generates new configurations for a new image upload
func (s *Server) CreateContentAttributeImageUploadConfig(ctx context.Context, req *nioh.CreateContentAttributeImageUploadConfigRequest) (*nioh.CreateContentAttributeImageUploadConfigResponse, error) {
	if req.ChannelId == "" {
		return nil, twirp.RequiredArgumentError("ChannelID")
	}

	uploadConfig, err := s.backend.CreateContentAttributeImageUploadConfig(ctx, req.ChannelId)
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"RequestID": chitin.GetTraceID(ctx),
			"ChannelID": req.ChannelId,
		}).WithError(err).Error("Failed to get an upload image URL for content attribute image")
		return nil, twirp.InternalError("Failed to request new upload config")
	}

	return &nioh.CreateContentAttributeImageUploadConfigResponse{
		ChannelId: req.ChannelId,
		UploadId:  uploadConfig.UploadID,
		UploadUrl: uploadConfig.UploadURL,
		ImageUrl:  uploadConfig.ImageURL,
	}, nil
}

// GetContentAttributesForChanlet is used to get content attributes for a chanlet.
func (s *Server) GetContentAttributesForChanlet(ctx context.Context, req *nioh.GetContentAttributesForChanletRequest) (*nioh.GetContentAttributesForChanletResponse, error) {
	log := logrus.WithField("RequestID", chitin.GetTraceID(ctx))

	if req.ChanletId == "" {
		return nil, twirp.RequiredArgumentError("chanlet_id")
	}

	attrs, err := s.backend.GetContentAttributesForChanlet(ctx, req.ChanletId)

	if err == backend.ErrChanletNotFound {
		return nil, twirp.NotFoundError("no such chanlet")
	} else if err != nil {
		log.WithError(err).Error("failed to get content attributes for chanlet from backend")
		return nil, twirp.InternalError("failed to get chanlet's content attributes")
	}

	return &nioh.GetContentAttributesForChanletResponse{
		ChanletId:         req.ChanletId,
		ContentAttributes: models.ContentAttributesToProtos(ctx, attrs),
	}, nil
}
