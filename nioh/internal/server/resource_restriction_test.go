package server

import (
	"context"
	"fmt"
	"strconv"
	"strings"
	"testing"
	"time"

	"code.justin.tv/commerce/nioh/pkg/clientid"

	"code.justin.tv/commerce/nioh/config"
	"code.justin.tv/commerce/nioh/internal/models"
	"code.justin.tv/commerce/nioh/internal/testdata"
	"code.justin.tv/commerce/nioh/pkg/helpers"
	"code.justin.tv/commerce/nioh/rpc/nioh"
	"code.justin.tv/discovery/liveline/proto/liveline"
	"github.com/golang/protobuf/ptypes"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestResourceRestrictionsAPI(t *testing.T) {
	Convey("Test Resource Restrictions API", t, func() {
		Convey("Test Get Restrictions", func() {
			id := "146851449"
			id2 := "27697171"

			cfg := &config.Configuration{}
			server, mockClients := initTestableServer(cfg)
			expectedError := "unexpected error while handling getRestrictionByResource"

			Convey("Test BROADCAST restrictions", func() {
				ctx := context.Background()
				req := &nioh.GetRestrictionsByResourcesRequest{
					Resources: []*nioh.RestrictionResource{
						{
							Id:   id,
							Type: nioh.ResourceType_BROADCAST,
						},
					},
				}

				Convey("with missing Id", func() {
					req.Resources[0].Id = ""
					_, err := server.GetRestrictionsByResources(ctx, req)
					So(err, ShouldNotBeNil)
				})

				Convey("with missing Type", func() {
					req.Resources[0].Type = nioh.ResourceType_UNKNOWN
					_, err := server.GetRestrictionsByResources(ctx, req)
					So(err, ShouldNotBeNil)
				})

				Convey("with valid input", func() {
					Convey("when the backend errors", func() {
						expected := &nioh.GetRestrictionsByResourcesResponse{
							Restrictions: []*nioh.RestrictionResourceResponse{
								{
									Id:          id,
									Type:        nioh.ResourceType_BROADCAST,
									Restriction: nil,
									Error:       expectedError,
								},
							},
						}
						mockClients.backend.On("GetRestriction", mock.Anything, models.Broadcast{ID: id}, mock.Anything).Return(nil, errors.New("test error"))

						res, err := server.GetRestrictionsByResources(ctx, req)
						So(err, ShouldBeNil)
						So(res, ShouldResemble, expected)
					})
					Convey("when all calls succeed with a valid geoblock", func() {
						startDate := time.Date(2019, time.January, 1, 0, 0, 0, 0, time.UTC)
						endDate := time.Date(2021, time.January, 1, 0, 0, 0, 0, time.UTC)
						startDateProto, _ := ptypes.TimestampProto(startDate)
						endDateProto, _ := ptypes.TimestampProto(endDate)
						countryCodes := []string{"DE"}

						expected := &nioh.GetRestrictionsByResourcesResponse{
							Restrictions: []*nioh.RestrictionResourceResponse{
								{
									Id:   id,
									Type: nioh.ResourceType_BROADCAST,
									Restriction: &nioh.Restriction{
										Id:              "broadcast:" + id,
										RestrictionType: nioh.Restriction_GEOBLOCK,
										Exemptions: []*nioh.Exemption{
											{
												ExemptionKeys:   countryCodes,
												ExemptionType:   nioh.Exemption_NOT_IN_COUNTRIES,
												ActiveStartDate: startDateProto,
												ActiveEndDate:   endDateProto,
											},
										},
										Geoblocks: []*nioh.Geoblock{
											{
												Operator:     nioh.Geoblock_BLOCK,
												CountryCodes: countryCodes,
											},
										},
									},
									Error: "",
								},
							},
						}

						mockRestriction := &models.RestrictionV2{
							ResourceKey:     "broadcast:" + id,
							RestrictionType: models.GeoblockRestrictionType,
							Exemptions: []*models.Exemption{
								{
									ExemptionType:   models.NotInCountriesExemptionType,
									ActiveStartDate: startDate,
									ActiveEndDate:   endDate,
									Keys:            countryCodes,
									Actions:         []*models.ExemptionAction{},
								},
							},
							Geoblocks: []models.Geoblock{
								{
									Operator:     models.BlockOperator,
									CountryCodes: countryCodes,
								},
							},
						}

						mockClients.backend.On("GetRestriction", mock.Anything, models.Broadcast{ID: id}, mock.Anything).Return(mockRestriction, nil)

						res, err := server.GetRestrictionsByResources(ctx, req)
						So(err, ShouldBeNil)
						So(res, ShouldResemble, expected)
					})
				})
			})

			Convey("Test LIVE restrictions", func() {
				ctx := context.Background()
				req := &nioh.GetRestrictionsByResourcesRequest{
					Resources: []*nioh.RestrictionResource{
						{
							Id:   id,
							Type: nioh.ResourceType_LIVE,
						},
					},
				}

				Convey("with missing Id", func() {
					req.Resources[0].Id = ""
					_, err := server.GetRestrictionsByResources(ctx, req)
					So(err, ShouldNotBeNil)
				})

				Convey("with missing Type", func() {
					req.Resources[0].Type = nioh.ResourceType_UNKNOWN
					_, err := server.GetRestrictionsByResources(ctx, req)
					So(err, ShouldNotBeNil)
				})

				Convey("Test API", func() {
					testRestriction := testdata.Restriction(id)
					expected := &nioh.GetRestrictionsByResourcesResponse{
						Restrictions: []*nioh.RestrictionResourceResponse{
							{
								Id:          id,
								Type:        nioh.ResourceType_LIVE,
								Restriction: nil,
								Error:       "",
							},
						},
					}

					Convey("with no restriction", func() {
						mockClients.backend.On("GetRestriction", mock.Anything, models.Channel{ID: id}, mock.Anything).Return(nil, nil)
					})

					Convey("with one error", func() {
						mockClients.backend.On("GetRestriction", mock.Anything, models.Channel{ID: id}, mock.Anything).Return(nil, errors.New("test error"))
						expected.Restrictions[0].Error = expectedError
					})

					Convey("with one success", func() {
						mockClients.backend.On("GetRestriction", mock.Anything, models.Channel{ID: id}, mock.Anything).Return(testRestriction, nil)
						protoRes, err := models.RestrictionToProto(testRestriction)
						So(err, ShouldBeNil)
						expected.Restrictions[0].Restriction = protoRes
					})

					res, err := server.GetRestrictionsByResources(ctx, req)
					So(err, ShouldBeNil)
					So(res, ShouldResemble, expected)
				})
			})

			Convey("Test restrictions", func() {
				id := "146851449"
				cfg := &config.Configuration{}
				server, mockClients := initTestableServer(cfg)

				Convey("Test VOD restrictions", func() {
					ctx := context.Background()
					req := &nioh.GetRestrictionsByResourcesRequest{
						Resources: []*nioh.RestrictionResource{
							{
								Id:   id,
								Type: nioh.ResourceType_VIDEO,
							},
						},
						IsPlayback: true,
					}

					Convey("with missing Id", func() {
						req.Resources[0].Id = ""
						_, err := server.GetRestrictionsByResources(ctx, req)
						fmt.Println(err)
						So(err, ShouldNotBeNil)
					})

					Convey("with missing Type", func() {
						req.Resources[0].Type = nioh.ResourceType_UNKNOWN
						_, err := server.GetRestrictionsByResources(ctx, req)
						So(err, ShouldNotBeNil)
					})

					Convey("Test API", func() {
						testRestriction := testdata.Restriction(id)
						expected := &nioh.GetRestrictionsByResourcesResponse{
							Restrictions: []*nioh.RestrictionResourceResponse{
								{
									Id:          id,
									Type:        nioh.ResourceType_VIDEO,
									Restriction: nil,
									Error:       "",
								},
							},
						}

						Convey("with no restriction", func() {
							mockClients.backend.On("GetRestriction", mock.Anything, models.VOD{ID: id}, mock.Anything).Return(nil, nil)
						})

						Convey("with one error", func() {
							mockClients.backend.On("GetRestriction", mock.Anything, models.VOD{ID: id}, mock.Anything).Return(nil, errors.New("test error"))
							expected.Restrictions[0].Error = expectedError
						})

						Convey("with one success", func() {
							mockClients.backend.On("GetRestriction", mock.Anything, models.VOD{ID: id}, mock.Anything).Return(testRestriction, nil)
							protoRes, err := models.RestrictionToProto(testRestriction)
							So(err, ShouldBeNil)
							expected.Restrictions[0].Restriction = protoRes
						})

						res, err := server.GetRestrictionsByResources(ctx, req)
						So(err, ShouldBeNil)
						So(res, ShouldResemble, expected)
					})
				})
			})

			Convey("Test MIXED restrictions", func() {
				cfg := &config.Configuration{}
				server, mockClients := initTestableServer(cfg)

				Convey("Test LIVE and VIDEO restrictions", func() {
					ctx := context.Background()
					req := &nioh.GetRestrictionsByResourcesRequest{
						Resources: []*nioh.RestrictionResource{
							{
								Id:   id,
								Type: nioh.ResourceType_LIVE,
							},
							{
								Id:   id2,
								Type: nioh.ResourceType_VIDEO,
							},
						},
						IsPlayback: true,
					}

					Convey("with a missing Id", func() {
						req.Resources[0].Id = ""
						_, err := server.GetRestrictionsByResources(ctx, req)
						fmt.Println(err)
						So(err, ShouldNotBeNil)
					})

					Convey("with a missing Type", func() {
						req.Resources[0].Type = nioh.ResourceType_UNKNOWN
						_, err := server.GetRestrictionsByResources(ctx, req)
						So(err, ShouldNotBeNil)
					})

					Convey("Test API", func() {
						testRestriction1 := testdata.Restriction(id)
						testRestriction2 := testdata.VODRestriction(id2)
						expected := &nioh.GetRestrictionsByResourcesResponse{
							Restrictions: []*nioh.RestrictionResourceResponse{
								{
									Id:          id,
									Type:        nioh.ResourceType_LIVE,
									Restriction: nil,
									Error:       "",
								},
								{
									Id:          id2,
									Type:        nioh.ResourceType_VIDEO,
									Restriction: nil,
									Error:       "",
								},
							},
						}

						Convey("with no restrictions", func() {
							mockClients.backend.On("GetRestriction", mock.Anything, models.Channel{ID: id}, mock.Anything).Return(nil, nil)
							mockClients.backend.On("GetRestriction", mock.Anything, models.VOD{ID: id2}, mock.Anything).Return(nil, nil)
						})

						Convey("with one error", func() {
							mockClients.backend.On("GetRestriction", mock.Anything, models.Channel{ID: id}, mock.Anything).Return(nil, errors.New("test error"))
							mockClients.backend.On("GetRestriction", mock.Anything, models.VOD{ID: id2}, mock.Anything).Return(testRestriction2, nil)
							protoRes, err := models.RestrictionToProto(testRestriction2)
							So(err, ShouldBeNil)
							expected.Restrictions[0].Error = expectedError
							expected.Restrictions[1].Restriction = protoRes
						})

						Convey("with both error", func() {
							mockClients.backend.On("GetRestriction", mock.Anything, models.Channel{ID: id}, mock.Anything).Return(nil, errors.New("test error"))
							mockClients.backend.On("GetRestriction", mock.Anything, models.VOD{ID: id2}, mock.Anything).Return(nil, errors.New("test error"))
							expected.Restrictions[0].Error = expectedError
							expected.Restrictions[1].Error = expectedError
						})

						Convey("with both success", func() {
							mockClients.backend.On("GetRestriction", mock.Anything, models.Channel{ID: id}, mock.Anything).Return(testRestriction1, nil)
							mockClients.backend.On("GetRestriction", mock.Anything, models.VOD{ID: id2}, mock.Anything).Return(testRestriction2, nil)
							protoRes1, err := models.RestrictionToProto(testRestriction1)
							So(err, ShouldBeNil)
							protoRes2, err := models.RestrictionToProto(testRestriction2)
							So(err, ShouldBeNil)
							expected.Restrictions[0].Restriction = protoRes1
							expected.Restrictions[1].Restriction = protoRes2
						})

						res, err := server.GetRestrictionsByResources(ctx, req)
						So(err, ShouldBeNil)
						So(res, ShouldResemble, expected)
					})
				})
			})
		})

		Convey("Test Restriction Mutations", func() {
			testChannelID := "146851449"
			testChannelIDInt, err := strconv.Atoi(testChannelID)
			if err != nil {
				panic(err)
			}

			spadeSendChan := make(chan struct{}, 1)
			// Spade event calls are async, so call this func to wait (or timeout)
			awaitSpadeSendOrTimeout := func() {
				select {
				case <-spadeSendChan:
				case <-time.After(1 * time.Millisecond):
				}
			}

			cfg := &config.Configuration{}
			server, mockClients := initTestableServer(cfg)

			mockClients.dynamicConfig.On("GetUserGates").Return(mockUserGates(), nil)
			testRestriction := testdata.Restriction(testChannelID)

			mockClients.clients.Spade.On("TrackEvent", mock.Anything, mock.Anything, mock.Anything).
				Run(func(mock.Arguments) { spadeSendChan <- struct{}{} }).
				Return(nil)

			Convey("Test SetRestrictionResource", func() {
				ctx := helpers.ContextWithUser(context.Background(), testChannelID)
				ctx = helpers.ContextWithAccessInfo(ctx, helpers.AccessInfo{
					CanAccess: true,
				})
				testClientID := "test-client-id"
				ctx = context.WithValue(ctx, clientid.ClientIDKey, testClientID)

				req := &nioh.SetResourceRestrictionRequest{
					UserId:  testChannelID,
					OwnerId: testChannelID,
					Resource: &nioh.RestrictionResource{
						Type: nioh.ResourceType_LIVE,
						Id:   "test-resource",
					},
					Restriction: &nioh.Restriction{
						Id: "test-restriction",
					},
				}

				Convey("with invalid request", func() {
					Convey("with missing userID", func() {
						req.UserId = ""
						_, err := server.SetResourceRestriction(ctx, req)
						So(err, ShouldNotBeNil)
					})

					Convey("with missing ownerID", func() {
						req.OwnerId = ""
						_, err := server.SetResourceRestriction(ctx, req)
						So(err, ShouldNotBeNil)
					})

					Convey("with missing resourceID", func() {
						req.Resource.Id = ""
						_, err := server.SetResourceRestriction(ctx, req)
						So(err, ShouldNotBeNil)
					})

					Convey("with incorrectly populated exemptions", func() {
						req.Restriction.Exemptions = []*nioh.Exemption{{}}
						_, err := server.SetResourceRestriction(ctx, req)
						So(err, ShouldNotBeNil)
					})

					Convey("with an incompatible option", func() {
						req.Restriction.RestrictionType = nioh.Restriction_ALL_ACCESS_PASS
						req.Restriction.Options = []nioh.Option{nioh.Option_ALLOW_CHANNEL_VIP}
						_, err := server.SetResourceRestriction(ctx, req)
						So(err, ShouldNotBeNil)
					})

					Convey("with too many tier options", func() {
						req.Restriction.RestrictionType = nioh.Restriction_SUB_ONLY_LIVE
						req.Restriction.Options = []nioh.Option{nioh.Option_ALLOW_TIER_3_ONLY, nioh.Option_ALLOW_TIER_2_AND_3_ONLY}
						_, err := server.SetResourceRestriction(ctx, req)
						So(err, ShouldNotBeNil)
					})

					Convey("with unauthorized userID", func() {
						mockClients.backend.On("GetChanletByID", mock.Anything, mock.Anything).Return(&models.Chanlet{OwnerChannelID: req.UserId, ChanletID: req.Resource.Id}, nil)

						req.UserId = "bad-guy"
						_, err := server.SetResourceRestriction(ctx, req)
						So(err, ShouldNotBeNil)
					})

					awaitSpadeSendOrTimeout()
					mockClients.clients.Spade.AssertNotCalled(t, "TrackEvent")
				})

				Convey("with Broadcast restriction", func() {
					broadcastID := "broadcastID"

					req.Resource = &nioh.RestrictionResource{
						Type: nioh.ResourceType_BROADCAST,
						Id:   broadcastID,
					}

					Convey("with no geoblocks", func() {
						req.Restriction.RestrictionType = nioh.Restriction_GEOBLOCK
						req.Restriction.Geoblocks = nil
						_, err := server.SetResourceRestriction(ctx, req)
						So(err, ShouldNotBeNil)
					})

					Convey("with geoblocks but no country codes", func() {
						req.Restriction.RestrictionType = nioh.Restriction_GEOBLOCK
						req.Restriction.Geoblocks = []*nioh.Geoblock{
							{Operator: nioh.Geoblock_ALLOW},
						}
						_, err := server.SetResourceRestriction(ctx, req)
						So(err, ShouldNotBeNil)
					})

					Convey("with valid geoblocks", func() {
						req.Restriction.RestrictionType = nioh.Restriction_GEOBLOCK
						req.Restriction.Geoblocks = []*nioh.Geoblock{
							{Operator: nioh.Geoblock_BLOCK, CountryCodes: []string{"DE"}},
						}

						mockClients.backend.On("SetRestriction", mock.Anything, mock.Anything, mock.Anything).Return(&models.RestrictionV2{}, nil)
						_, err := server.SetResourceRestriction(ctx, req)
						So(err, ShouldBeNil)
					})
				})

				Convey("with VOD restriction", func() {
					videoID := "videoID"

					req.Resource = &nioh.RestrictionResource{
						Type: nioh.ResourceType_VIDEO,
						Id:   videoID,
					}

					// VOD restrictions check the user ID and the owner ID
					Convey("with unauthorized ownerID", func() {
						mockClients.backend.On("GetChanletByID", mock.Anything, mock.Anything).Return(nil, nil)
						mockClients.dynamicConfig.On("GetAllAccessPassModeratorsForChannel", mock.Anything).Return(nil, nil)
						req.OwnerId = "bystander"
						_, err := server.SetResourceRestriction(ctx, req)
						So(err, ShouldNotBeNil)
					})

					Convey("does not check live status", func() {
						mockClients.backend.On("SetRestriction", mock.Anything, models.VOD{ID: req.Resource.Id, OwnerID: req.OwnerId}, mock.Anything).Return(nil, nil)
						_, err := server.SetResourceRestriction(ctx, req)
						mockClients.clients.Liveline.AssertNotCalled(t, "GetStreamsByChannelIDs")
						So(err, ShouldBeNil)
					})

					Convey("with no restriction", func() {
						mockClients.backend.On("SetRestriction", mock.Anything, models.VOD{ID: req.Resource.Id, OwnerID: req.OwnerId}, mock.Anything).Return(nil, nil)

						res, err := server.SetResourceRestriction(ctx, req)
						So(err, ShouldBeNil)
						So(res, ShouldResemble, &nioh.MutateResourceRestrictionResponse{
							UserId:      req.UserId,
							OwnerId:     req.OwnerId,
							ResourceId:  req.Resource.Id,
							Type:        req.Resource.Type,
							Restriction: nil,
						})
					})

					Convey("with geoblocks", func() {
						req.Restriction.RestrictionType = nioh.Restriction_GEOBLOCK

						_, err := server.SetResourceRestriction(ctx, req)
						So(err, ShouldNotBeNil)
					})

					Convey("with sub-only restriction", func() {
						mockClients.backend.On("SetRestriction", mock.Anything, models.VOD{ID: req.Resource.Id, OwnerID: req.OwnerId}, models.RestrictionV2{RestrictionType: models.SubOnlyLiveRestrictionType}).
							Return(testRestriction, nil)

						req.Restriction.RestrictionType = nioh.Restriction_SUB_ONLY_LIVE
						res, err := server.SetResourceRestriction(ctx, req)
						So(err, ShouldBeNil)
						So(res, ShouldResemble, &nioh.MutateResourceRestrictionResponse{
							UserId:      req.UserId,
							OwnerId:     req.OwnerId,
							ResourceId:  req.Resource.Id,
							Type:        req.Resource.Type,
							Restriction: testdata.ProtoRestriction(testChannelID),
						})
					})

					Convey("with non-sub-only restriction", func() {
						testRestriction.RestrictionType = models.UnknownRestrictionType
						expectedProtoRestriction := testdata.ProtoRestriction(testChannelID)
						expectedProtoRestriction.RestrictionType = nioh.Restriction_UNKNOWN

						mockClients.backend.On("SetRestriction", mock.Anything, models.VOD{ID: req.Resource.Id, OwnerID: req.OwnerId}, models.RestrictionV2{RestrictionType: models.UnknownRestrictionType}).
							Return(testRestriction, nil)

						req.Restriction.RestrictionType = nioh.Restriction_UNKNOWN
						res, err := server.SetResourceRestriction(ctx, req)
						So(err, ShouldBeNil)
						So(res, ShouldResemble, &nioh.MutateResourceRestrictionResponse{
							UserId:      req.UserId,
							OwnerId:     req.OwnerId,
							ResourceId:  req.Resource.Id,
							Type:        req.Resource.Type,
							Restriction: expectedProtoRestriction,
						})
					})

					Convey("with backend error", func() {
						mockClients.backend.On("SetRestriction", mock.Anything, models.VOD{ID: req.Resource.Id, OwnerID: req.OwnerId}, mock.Anything).
							Return(nil, errors.New("your backend asplode"))

						_, err := server.SetResourceRestriction(ctx, req)
						So(err, ShouldNotBeNil)
					})

					awaitSpadeSendOrTimeout()
					mockClients.clients.Spade.AssertNotCalled(t, "TrackEvent")
				})

				Convey("with Channel restriction", func() {
					req.Resource = &nioh.RestrictionResource{
						Type: nioh.ResourceType_LIVE,
						Id:   testChannelID,
					}
					mockClients.backend.On("GetRestriction", mock.Anything, models.Channel{ID: req.Resource.Id}, mock.Anything).Return(nil, nil)

					Convey("with unauthorized ownerID", func() {
						req.Resource.Id = "bystander"
						// Channel restrictions check the resource ID and the user ID for ownership
						mockClients.backend.On("GetChanletByID", mock.Anything, mock.Anything).Return(&models.Chanlet{OwnerChannelID: req.Resource.Id}, nil)
						_, err := server.SetResourceRestriction(ctx, req)
						So(err, ShouldNotBeNil)
					})

					// TODO: unskip when liveline check is back in
					SkipConvey("with stream live", func() {
						mockClients.clients.Liveline.On("GetStreamsByChannelIDs", mock.Anything, []string{req.Resource.Id}).
							Return(&liveline.StreamsResponse{Streams: []*liveline.StreamResponse{{ChannelId: "123"}}}, nil)

						_, err := server.SetResourceRestriction(ctx, req)
						So(err, ShouldNotBeNil)
					})

					// TODO: unskip when jax check is back in
					SkipConvey("with stream error", func() {
						mockClients.clients.Liveline.On("GetStreamsByChannelIDs", mock.Anything, []string{req.Resource.Id}).Return(nil, errors.New("what's jax"))

						_, err := server.SetResourceRestriction(ctx, req)
						So(err, ShouldNotBeNil)
					})

					Convey("with geoblocks", func() {
						req.Restriction.RestrictionType = nioh.Restriction_GEOBLOCK

						_, err := server.SetResourceRestriction(ctx, req)
						So(err, ShouldNotBeNil)
					})

					Convey("with no restriction", func() {
						mockClients.clients.Liveline.On("GetStreamsByChannelIDs", mock.Anything, []string{req.Resource.Id}).Return(&liveline.StreamsResponse{Streams: []*liveline.StreamResponse{}}, nil)
						mockClients.backend.On("SetRestriction", mock.Anything, models.Channel{ID: req.Resource.Id}, mock.Anything).
							Return(nil, nil)

						res, err := server.SetResourceRestriction(ctx, req)
						So(err, ShouldBeNil)
						So(res, ShouldResemble, &nioh.MutateResourceRestrictionResponse{
							UserId:      req.UserId,
							OwnerId:     req.OwnerId,
							ResourceId:  req.Resource.Id,
							Type:        req.Resource.Type,
							Restriction: nil,
						})
					})

					Convey("with sub-only restriction", func() {
						mockClients.clients.Liveline.On("GetStreamsByChannelIDs", mock.Anything, []string{req.Resource.Id}).Return(&liveline.StreamsResponse{Streams: []*liveline.StreamResponse{}}, nil)
						testRestriction.Options = []models.RestrictionOption{models.AllowTier2And3Only, models.AllowChannelVIPOption}
						mockClients.backend.On("SetRestriction", mock.Anything, models.Channel{ID: req.Resource.Id}, models.RestrictionV2{RestrictionType: models.SubOnlyLiveRestrictionType, Options: testRestriction.Options}).
							Return(testRestriction, nil)

						req.Restriction.RestrictionType = nioh.Restriction_SUB_ONLY_LIVE
						req.Restriction.Options = []nioh.Option{nioh.Option_ALLOW_TIER_2_AND_3_ONLY, nioh.Option_ALLOW_CHANNEL_VIP}
						res, err := server.SetResourceRestriction(ctx, req)
						expectedRestriction := testdata.ProtoRestriction(testChannelID)
						expectedRestriction.Options = req.Restriction.Options

						So(err, ShouldBeNil)
						So(res, ShouldResemble, &nioh.MutateResourceRestrictionResponse{
							UserId:      req.UserId,
							OwnerId:     req.OwnerId,
							ResourceId:  req.Resource.Id,
							Type:        req.Resource.Type,
							Restriction: expectedRestriction,
						})

						awaitSpadeSendOrTimeout()
						mockClients.clients.Spade.AssertCalled(t, "TrackEvent", mock.Anything,
							"channel_restriction_status",
							map[string]interface{}{
								"channel_id":        testChannelIDInt,
								"is_restricted":     true,
								"toggle_method":     models.ToggleMethodAPI,
								"restriction_type":  strings.ToLower(models.SubOnlyLiveRestrictionType.String()),
								"include_mods":      false,
								"include_vips":      true,
								"client_id":         testClientID,
								"include_subs_1000": false,
								"include_subs_2000": true,
								"include_subs_3000": true,
							},
						)
					})

					Convey("with non-sub-only restriction, doesn't send spade event", func() {
						mockClients.clients.Liveline.On("GetStreamsByChannelIDs", mock.Anything, []string{req.Resource.Id}).Return(&liveline.StreamsResponse{Streams: []*liveline.StreamResponse{}}, nil)

						testRestriction.RestrictionType = models.UnknownRestrictionType
						expectedProtoRestriction := testdata.ProtoRestriction(testChannelID)
						expectedProtoRestriction.RestrictionType = nioh.Restriction_UNKNOWN

						mockClients.backend.On("SetRestriction", mock.Anything, models.Channel{ID: req.Resource.Id}, models.RestrictionV2{RestrictionType: models.UnknownRestrictionType}).
							Return(testRestriction, nil)

						req.Restriction.RestrictionType = nioh.Restriction_UNKNOWN
						res, err := server.SetResourceRestriction(ctx, req)
						So(err, ShouldBeNil)
						So(res, ShouldResemble, &nioh.MutateResourceRestrictionResponse{
							UserId:      req.UserId,
							OwnerId:     req.OwnerId,
							ResourceId:  req.Resource.Id,
							Type:        req.Resource.Type,
							Restriction: expectedProtoRestriction,
						})

						awaitSpadeSendOrTimeout()
						mockClients.clients.Spade.AssertNotCalled(t, "TrackEvent")
					})

					Convey("with backend error", func() {
						mockClients.clients.Liveline.On("GetStreamsByChannelIDs", mock.Anything, []string{req.Resource.Id}).Return(&liveline.StreamsResponse{Streams: []*liveline.StreamResponse{}}, nil)
						mockClients.backend.On("SetRestriction", mock.Anything, models.Channel{ID: req.Resource.Id}, mock.Anything).
							Return(nil, errors.New("your backend asplode"))

						_, err := server.SetResourceRestriction(ctx, req)
						So(err, ShouldNotBeNil)

						awaitSpadeSendOrTimeout()
						mockClients.clients.Spade.AssertNotCalled(t, "TrackEvent")
					})

					Convey("with existing ALL_ACCESS_PASS restriction", func() {
						req.Resource.Id = "999"
						req.UserId = "999"
						mockClients.backend.On("GetRestriction", mock.Anything, models.Channel{ID: req.Resource.Id}, mock.Anything).Return(&models.RestrictionV2{
							ResourceKey:     "channel:999",
							RestrictionType: models.AllAccessPassRestrictionType,
						}, nil)
						_, err := server.SetResourceRestriction(ctx, req)
						So(err, ShouldNotBeNil)
					})
				})
			})

			Convey("Test DeleteRestrictionResource", func() {
				ctx := context.Background()
				testClientID := "test-client-id"
				ctx = context.WithValue(ctx, clientid.ClientIDKey, testClientID)
				req := &nioh.DeleteResourceRestrictionRequest{
					UserId:  testChannelID,
					OwnerId: testChannelID,
					Resource: &nioh.RestrictionResource{
						Id: "",
					},
				}

				Convey("with missing userID", func() {
					req.UserId = ""
					_, err := server.DeleteResourceRestriction(ctx, req)
					So(err, ShouldNotBeNil)

					_, err = server.PDMSDeleteResourceRestriction(ctx, req)
					So(err, ShouldNotBeNil)

					awaitSpadeSendOrTimeout()
					mockClients.clients.Spade.AssertNotCalled(t, "TrackEvent")
				})

				Convey("with missing ownerID", func() {
					req.OwnerId = ""
					_, err := server.DeleteResourceRestriction(ctx, req)
					So(err, ShouldNotBeNil)

					_, err = server.PDMSDeleteResourceRestriction(ctx, req)
					So(err, ShouldNotBeNil)

					awaitSpadeSendOrTimeout()
					mockClients.clients.Spade.AssertNotCalled(t, "TrackEvent")

				})

				Convey("with missing resourceID", func() {
					req.Resource.Id = ""
					_, err := server.DeleteResourceRestriction(ctx, req)
					So(err, ShouldNotBeNil)

					_, err = server.PDMSDeleteResourceRestriction(ctx, req)
					So(err, ShouldNotBeNil)

					awaitSpadeSendOrTimeout()
					mockClients.clients.Spade.AssertNotCalled(t, "TrackEvent")
				})

				Convey("with unauthorized userID", func() {
					mockClients.backend.On("GetChanletByID", mock.Anything, mock.Anything).Return(&models.Chanlet{OwnerChannelID: req.UserId, ChanletID: req.Resource.Id}, nil)

					req.UserId = "bad-guy"
					_, err := server.DeleteResourceRestriction(ctx, req)
					So(err, ShouldNotBeNil)

					awaitSpadeSendOrTimeout()
					mockClients.clients.Spade.AssertNotCalled(t, "TrackEvent")
				})

				Convey("with VOD restriction", func() {
					videoID := "videoID"

					req.Resource = &nioh.RestrictionResource{
						Type: nioh.ResourceType_VIDEO,
						Id:   videoID,
					}

					// VOD restrictions check the user ID and the owner ID
					Convey("with unauthorized ownerID", func() {
						mockClients.backend.On("GetChanletByID", mock.Anything, mock.Anything).Return(nil, nil)
						mockClients.dynamicConfig.On("GetAllAccessPassModeratorsForChannel", mock.Anything).Return(nil, nil)
						req.OwnerId = "bystander"
						_, err := server.DeleteResourceRestriction(ctx, req)
						So(err, ShouldNotBeNil)
					})

					Convey("does not check live status", func() {
						mockClients.backend.On("DeleteRestriction", mock.Anything, models.VOD{ID: req.Resource.Id, OwnerID: req.OwnerId}).Return(nil, nil)
						_, err := server.DeleteResourceRestriction(ctx, req)
						mockClients.clients.Liveline.AssertNotCalled(t, "GetStreamsByChannelIDs")
						So(err, ShouldBeNil)
					})

					Convey("with no restriction", func() {
						mockClients.backend.On("DeleteRestriction", mock.Anything, models.VOD{ID: req.Resource.Id, OwnerID: req.OwnerId}).Return(nil, nil)

						res, err := server.DeleteResourceRestriction(ctx, req)
						So(err, ShouldBeNil)
						So(res, ShouldResemble, &nioh.MutateResourceRestrictionResponse{
							UserId:      req.UserId,
							OwnerId:     req.OwnerId,
							ResourceId:  req.Resource.Id,
							Type:        req.Resource.Type,
							Restriction: nil,
						})
					})

					Convey("with sub-only restriction", func() {
						mockClients.backend.On("DeleteRestriction", mock.Anything, models.VOD{ID: req.Resource.Id, OwnerID: req.OwnerId}).Return(testRestriction, nil)

						res, err := server.DeleteResourceRestriction(ctx, req)
						So(err, ShouldBeNil)
						So(res, ShouldResemble, &nioh.MutateResourceRestrictionResponse{
							UserId:      req.UserId,
							OwnerId:     req.OwnerId,
							ResourceId:  req.Resource.Id,
							Type:        req.Resource.Type,
							Restriction: testdata.ProtoRestriction(testChannelID),
						})
					})

					Convey("with all-access-pass restriction for moderator", func() {
						req.OwnerId = "moderator"
						mockClients.backend.On("DeleteRestriction", mock.Anything, models.VOD{ID: req.Resource.Id, OwnerID: req.OwnerId}).Return(testRestriction, nil)
						mockClients.backend.On("GetChanletByID", mock.Anything, mock.Anything).Return(nil, nil)
						mockClients.dynamicConfig.On("GetAllAccessPassModeratorsForChannel", req.OwnerId).Return(map[string]bool{req.UserId: true}, nil)

						res, err := server.DeleteResourceRestriction(ctx, req)
						So(err, ShouldBeNil)
						So(res, ShouldResemble, &nioh.MutateResourceRestrictionResponse{
							UserId:      req.UserId,
							OwnerId:     req.OwnerId,
							ResourceId:  req.Resource.Id,
							Type:        req.Resource.Type,
							Restriction: testdata.ProtoRestriction(testChannelID),
						})
					})

					Convey("with non-sub-only restriction", func() {
						testRestriction.RestrictionType = models.UnknownRestrictionType
						expectedProtoRestriction := testdata.ProtoRestriction(testChannelID)
						expectedProtoRestriction.RestrictionType = nioh.Restriction_UNKNOWN

						mockClients.backend.On("DeleteRestriction", mock.Anything, models.VOD{ID: req.Resource.Id, OwnerID: req.OwnerId}).Return(testRestriction, nil)

						res, err := server.DeleteResourceRestriction(ctx, req)
						So(err, ShouldBeNil)
						So(res, ShouldResemble, &nioh.MutateResourceRestrictionResponse{
							UserId:      req.UserId,
							OwnerId:     req.OwnerId,
							ResourceId:  req.Resource.Id,
							Type:        req.Resource.Type,
							Restriction: expectedProtoRestriction,
						})
					})

					Convey("with backend error", func() {
						mockClients.backend.On("DeleteRestriction", mock.Anything, models.VOD{ID: req.Resource.Id, OwnerID: req.OwnerId}, mock.Anything).
							Return(nil, errors.New("your backend asplode"))

						_, err := server.DeleteResourceRestriction(ctx, req)
						So(err, ShouldNotBeNil)
					})

					awaitSpadeSendOrTimeout()
					mockClients.clients.Spade.AssertNotCalled(t, "TrackEvent")
				})

				Convey("With Broadcast restriction", func() {
					broadcastID := "broadcastID"
					req.Resource = &nioh.RestrictionResource{
						Type: nioh.ResourceType_BROADCAST,
						Id:   broadcastID,
					}

					Convey("with existing restriction", func() {
						startDate := time.Date(2019, time.January, 1, 0, 0, 0, 0, time.UTC)
						endDate := time.Date(2021, time.January, 1, 0, 0, 0, 0, time.UTC)
						countryCodes := []string{"DE"}

						mockClients.backend.On("DeleteRestriction", mock.Anything, models.Broadcast{ID: req.Resource.Id}).Return(&models.RestrictionV2{
							ResourceKey:     "broadcast:" + broadcastID,
							RestrictionType: models.GeoblockRestrictionType,
							Exemptions: []*models.Exemption{
								{
									ExemptionType:   models.NotInCountriesExemptionType,
									ActiveStartDate: startDate,
									ActiveEndDate:   endDate,
									Keys:            countryCodes,
								},
							},
							Geoblocks: []models.Geoblock{
								{
									Operator:     models.BlockOperator,
									CountryCodes: countryCodes,
								},
							},
						}, nil)

						res, err := server.DeleteResourceRestriction(ctx, req)
						So(err, ShouldBeNil)
						So(res.Restriction, ShouldNotBeNil)
					})
				})

				Convey("With Channel Restriction", func() {
					req.Resource = &nioh.RestrictionResource{
						Type: nioh.ResourceType_LIVE,
						Id:   testChannelID,
					}

					// TODO: unskip when jax check is back in
					SkipConvey("with stream live", func() {
						mockClients.clients.Liveline.On("GetStreamsByChannelIDs", mock.Anything, []string{req.Resource.Id}).
							Return(&liveline.StreamsResponse{Streams: []*liveline.StreamResponse{{ChannelId: "123"}}}, nil)

						_, err := server.DeleteResourceRestriction(ctx, req)
						So(err, ShouldNotBeNil)
					})

					Convey("with existing restriction", func() {
						mockClients.clients.Liveline.On("GetStreamsByChannelIDs", mock.Anything, []string{req.Resource.Id}).Return(&liveline.StreamsResponse{Streams: []*liveline.StreamResponse{}}, nil)
						mockClients.backend.On("DeleteRestriction", mock.Anything, models.Channel{ID: req.Resource.Id}).Return(testdata.Restriction(testChannelID), nil)

						res, err := server.DeleteResourceRestriction(ctx, req)
						So(err, ShouldBeNil)
						So(res.Restriction, ShouldNotBeNil)

						res, err = server.PDMSDeleteResourceRestriction(ctx, req)
						So(err, ShouldBeNil)
						So(res.Restriction, ShouldNotBeNil)

						awaitSpadeSendOrTimeout()
						mockClients.clients.Spade.AssertCalled(t, "TrackEvent", mock.Anything,
							"channel_restriction_status",
							map[string]interface{}{
								"channel_id":        testChannelIDInt,
								"is_restricted":     false,
								"toggle_method":     models.ToggleMethodAPI,
								"restriction_type":  strings.ToLower(models.SubOnlyLiveRestrictionType.String()),
								"include_mods":      false,
								"include_vips":      false,
								"client_id":         testClientID,
								"include_subs_1000": false,
								"include_subs_2000": false,
								"include_subs_3000": false,
							},
						)
					})

					Convey("with existing non-SOL restriction, doesn't send Spade event", func() {
						mockClients.clients.Liveline.On("GetStreamsByChannelIDs", mock.Anything, []string{req.Resource.Id}).Return(&liveline.StreamsResponse{Streams: []*liveline.StreamResponse{}}, nil)
						testRestriction := testdata.Restriction(testChannelID)
						testRestriction.RestrictionType = models.UnknownRestrictionType

						mockClients.backend.On("DeleteRestriction", mock.Anything, models.Channel{ID: req.Resource.Id}).Return(testRestriction, nil)

						res, err := server.DeleteResourceRestriction(ctx, req)
						So(err, ShouldBeNil)
						So(res.Restriction, ShouldNotBeNil)

						awaitSpadeSendOrTimeout()
						mockClients.clients.Spade.AssertNotCalled(t, "TrackEvent")
					})

					Convey("with no restriction", func() {
						mockClients.clients.Liveline.On("GetStreamsByChannelIDs", mock.Anything, []string{req.Resource.Id}).Return(&liveline.StreamsResponse{Streams: []*liveline.StreamResponse{}}, nil)
						mockClients.backend.On("DeleteRestriction", mock.Anything, models.Channel{ID: req.Resource.Id}).Return(nil, nil)

						res, err := server.DeleteResourceRestriction(ctx, req)
						So(err, ShouldBeNil)
						So(res.Restriction, ShouldBeNil)

						res, err = server.PDMSDeleteResourceRestriction(ctx, req)
						So(err, ShouldBeNil)
						So(res.Restriction, ShouldBeNil)

						awaitSpadeSendOrTimeout()
						mockClients.clients.Spade.AssertNotCalled(t, "TrackEvent")
					})

					Convey("with backend error", func() {
						mockClients.clients.Liveline.On("GetStreamsByChannelIDs", mock.Anything, []string{req.Resource.Id}).Return(&liveline.StreamsResponse{Streams: []*liveline.StreamResponse{}}, nil)
						mockClients.backend.On("DeleteRestriction", mock.Anything, models.Channel{ID: req.Resource.Id}).
							Return(nil, errors.New("your backend asplode"))

						_, err := server.DeleteResourceRestriction(ctx, req)
						So(err, ShouldNotBeNil)

						_, err = server.PDMSDeleteResourceRestriction(ctx, req)
						So(err, ShouldNotBeNil)

						awaitSpadeSendOrTimeout()
						mockClients.clients.Spade.AssertNotCalled(t, "TrackEvent")
					})
				})
			})
		})
	})
}
