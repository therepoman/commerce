package server

import (
	"context"
	"fmt"
	"strconv"
	"sync"

	"code.justin.tv/commerce/nioh/pkg/clientid"
	"code.justin.tv/commerce/nioh/pkg/helpers"

	"code.justin.tv/commerce/logrus"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/nioh/internal/clients/rbac"
	"code.justin.tv/commerce/nioh/internal/models"
	"code.justin.tv/commerce/nioh/rpc/nioh"
	"code.justin.tv/common/chitin"
	"code.justin.tv/common/twirp"
)

const (
	getRestrictionsByResourcesCacheControlFmt = "public, max-age=%d"

	// Cache duration to be inserted for Cache-Control header for single resource GetRestrictionsByResources response (in seconds)
	singleRestrictionCacheDuration = 5
)

type mutateRestrictionRequest interface {
	GetUserId() string
	GetOwnerId() string
	GetResource() *nioh.RestrictionResource
	GetRestriction() *nioh.Restriction
}

type deleteMutationWrapper struct {
	*nioh.DeleteResourceRestrictionRequest
}

func (d deleteMutationWrapper) GetRestriction() *nioh.Restriction {
	return nil
}

// GetRestrictionsByResources returns V2 restrictions for the passed resource ID and type
func (s *Server) GetRestrictionsByResources(ctx context.Context, req *nioh.GetRestrictionsByResourcesRequest) (*nioh.GetRestrictionsByResourcesResponse, error) {
	log := logrus.WithField("RequestID", chitin.GetTraceID(ctx))
	log.WithField("request", *req).Debug("Handling GetRestrictionsByResources")

	resp := &nioh.GetRestrictionsByResourcesResponse{
		Restrictions: make([]*nioh.RestrictionResourceResponse, len(req.Resources)),
	}

	wg := &sync.WaitGroup{}

	for idx, resource := range req.Resources {
		if resource.GetId() == "" {
			return nil, twirp.InvalidArgumentError("Resources", "Unrecognized object sent as resource, an object is missing a resource ID")
		}

		if resource.GetType() == nioh.ResourceType_UNKNOWN {
			return nil, twirp.InvalidArgumentError("Resources", "Unrecognized object sent as resource, an object is missing a resource Type")
		}

		wg.Add(1)
		go func(resource *nioh.RestrictionResource, idx int) {
			defer wg.Done()

			resp.Restrictions[idx] = s.getRestrictionByResource(ctx, resource, req.IsPlayback)
		}(resource, idx)
	}
	wg.Wait()

	// We want GQL to cache only a singular gets because request with multiple resources will have too many key permutations
	// There are plenty of use cases where a GQL query only involes a single resource
	if len(req.Resources) == 1 {
		// Make upstream cache this response
		setResponseHeader(ctx, cacheControlKey, fmt.Sprintf(getRestrictionsByResourcesCacheControlFmt, singleRestrictionCacheDuration))
	}

	return resp, nil
}

func (s *Server) getRestrictionByResource(ctx context.Context, resource *nioh.RestrictionResource, isPlayback bool) *nioh.RestrictionResourceResponse {
	restrictableResource := models.RestrictableResourceFromProto(resource, "")
	if restrictableResource == nil {
		return &nioh.RestrictionResourceResponse{Error: "unrecognized object sent as resource type"}
	}

	response := models.RestrictionResourceToProto(resource)

	restriction, err := s.backend.GetRestriction(ctx, restrictableResource, isPlayback)
	if err != nil {
		response.Error = "unexpected error while handling getRestrictionByResource"
		log.WithError(err).Error(response.Error)
	} else if restriction != nil {
		protoRestriction, err := models.RestrictionToProto(restriction)
		if err != nil {
			response.Error = "restriction is malformed"
			log.WithError(err).Error(response.Error)
		} else {
			response.Restriction = protoRestriction
		}
	}

	return response
}

// SetResourceRestriction sets a V2 restriction for a given resource
func (s *Server) SetResourceRestriction(ctx context.Context, req *nioh.SetResourceRestrictionRequest) (*nioh.MutateResourceRestrictionResponse, error) {
	if err := s.validateRequest(ctx, req); err != nil {
		return nil, err
	}

	resource := req.GetResource()
	log := logrus.WithFields(logrus.Fields{
		"RequestID":    chitin.GetTraceID(ctx),
		"UserID":       req.GetUserId(),
		"OwnerID":      req.GetOwnerId(),
		"ResourceType": resource.GetType(),
		"ResourceID":   resource.GetId(),
	})
	log.WithField("Request", *req).Debug("Handling SetResourceRestriction")

	if err := s.authorizeRequest(ctx, req, setResourceRestrictionGateKey); err != nil {
		return nil, err
	}

	// If resource is a channel, check for live status
	if req.GetResource().GetType() == nioh.ResourceType_LIVE {
		isLive, err := s.isChannelLive(ctx, resource.GetId())
		if err != nil {
			log.WithError(err).Error("SetResourceRestriction encountered an error while retrieving stream status")
			return nil, twirp.InternalError("Failed to detect stream status")
		}

		if isLive {
			return nil, twirp.InvalidArgumentError(req.GetResource().GetId(), "Channel restriction cannot be changed while it is live")
		}
	}

	response := &nioh.MutateResourceRestrictionResponse{
		UserId:     req.GetUserId(),
		OwnerId:    req.GetOwnerId(),
		ResourceId: resource.GetId(),
		Type:       resource.GetType(),
	}

	// TODO: remove after OWL is done
	// We are checking the existing channel restriction, and if the existing restriction is ALL_ACCESS_PASS
	// we return an error saying this is forbidden. This prevents an accidental override of script generated restrictions.
	if resource.GetType() == nioh.ResourceType_LIVE {
		existingRestriction, err := s.backend.GetRestriction(ctx, models.Channel{
			ID: resource.GetId(),
		}, false)
		if err != nil {
			log.WithError(err).Error("SetRestriction encountered an error while looking up existing restriction")
			return nil, twirp.InternalError("Failed to create or update channel restriction")
		} else if existingRestriction != nil && existingRestriction.RestrictionType == models.AllAccessPassRestrictionType {
			return nil, twirp.NewError(twirp.PermissionDenied, "You cannot override an ALL_ACCESS_PASS restriction with API")
		}
	}

	// TODO: cleanup, yikes
	restriction, err := s.backend.SetRestriction(
		ctx,
		models.RestrictableResourceFromProto(resource, req.GetOwnerId()),
		models.RestrictionFromProto(req.GetRestriction()),
	)
	if err != nil {
		if _, ok := err.(rbac.OrganizationNotFoundError); ok {
			log.WithError(err).Error("SetResourceRestriction encountered an error while retrieving organization of the channel owner")
			return nil, twirp.NotFoundError(err.Error())
		}

		log.WithError(err).Error("SetRestriction encountered an error while creating or updating Restriction")
		return nil, twirp.InternalError("Failed to create or update channel restriction")
	}

	if restriction == nil {
		return response, nil
	}

	protoRestriction, err := models.RestrictionToProto(restriction)
	if err != nil {
		msg := "set resource restriction is malformed"
		log.WithError(err).Error(msg)
		return nil, twirp.InternalError(msg)
	}

	response.Restriction = protoRestriction

	// Send Spade event if it's a channel restriction type
	if resource.GetType() == nioh.ResourceType_LIVE {
		// Send Spade event if SOL restriction.
		go s.trackSOLRestrictionResourceEvent(ctx, setRestrictionSpade, response)
	}

	return response, nil
}

// DeleteResourceRestriction deletes and returns a V2 restriction for the passed in resource. If no restriction exists, nil is returned.
func (s *Server) DeleteResourceRestriction(ctx context.Context, req *nioh.DeleteResourceRestrictionRequest) (*nioh.MutateResourceRestrictionResponse, error) {
	if err := s.validateRequest(ctx, deleteMutationWrapper{req}); err != nil {
		return nil, err
	}

	resource := req.GetResource()
	log := logrus.WithFields(logrus.Fields{
		"RequestID":    chitin.GetTraceID(ctx),
		"UserID":       req.GetUserId(),
		"OwnerID":      req.GetOwnerId(),
		"ResourceType": resource.GetType(),
		"ResourceID":   resource.GetId(),
	})
	log.WithField("Request", *req).Info("Handling DeleteResourceRestriction")

	if err := s.authorizeRequest(ctx, deleteMutationWrapper{req}, setResourceRestrictionGateKey); err != nil {
		log.WithError(err).Error("DeleteResourceRestriction encountered an error while authorizing request")
		return nil, err
	}

	// If resource is a channel, check for live status
	if resource.GetType() == nioh.ResourceType_LIVE {
		isLive, err := s.isChannelLive(ctx, resource.GetId())
		if err != nil {
			log.WithError(err).Error("DeleteResourceRestriction encountered an error while retrieving stream status")
			return nil, twirp.InternalError("Failed to detect stream status")
		}

		if isLive {
			return nil, twirp.InvalidArgumentError(resource.GetId(), "Channel restriction cannot be changed while it is live")
		}
	}

	response, err := s.deleteRestriction(ctx, req)
	if err != nil {
		return nil, err
	}

	// Send Spade event if it's a channel restriction type
	if resource.GetType() == nioh.ResourceType_LIVE {
		// Send Spade event if SOL restriction.
		go s.trackSOLRestrictionResourceEvent(ctx, deleteRestrictionSpade, response)
	}

	return response, nil
}

// PDMSDeleteResourceRestriction deletes a resources restriction for LIVE types bypassing all authorization checks for privacy compliance
func (s *Server) PDMSDeleteResourceRestriction(ctx context.Context, req *nioh.DeleteResourceRestrictionRequest) (*nioh.MutateResourceRestrictionResponse, error) {
	if err := s.validateRequest(ctx, deleteMutationWrapper{req}); err != nil {
		return nil, err
	}

	resource := req.GetResource()
	log := logrus.WithFields(logrus.Fields{
		"RequestID":    chitin.GetTraceID(ctx),
		"UserID":       req.GetUserId(),
		"OwnerID":      req.GetOwnerId(),
		"ResourceType": resource.GetType(),
		"ResourceID":   resource.GetId(),
	})
	log.WithField("Request", req).Info("Handling DeleteResourceRestriction")

	// We only delete restrictions that are keyed by channel ID
	if resource.GetType() != nioh.ResourceType_LIVE {
		return nil, twirp.InvalidArgumentError("ResourceType", "must be LIVE for PDMS deletions")
	}

	// Skip spade event for SOL
	return s.deleteRestriction(ctx, req)
}

func (s *Server) deleteRestriction(ctx context.Context, req *nioh.DeleteResourceRestrictionRequest) (*nioh.MutateResourceRestrictionResponse, error) {
	resource := req.GetResource()
	response := &nioh.MutateResourceRestrictionResponse{
		UserId:     req.GetUserId(),
		OwnerId:    req.GetOwnerId(),
		ResourceId: resource.GetId(),
		Type:       resource.GetType(),
	}

	restriction, err := s.backend.DeleteRestriction(ctx, models.RestrictableResourceFromProto(resource, req.GetOwnerId()))
	if err != nil {
		log.WithError(err).Error("DeleteResourceRestriction encountered error while deleting Restriction on backend")
		return nil, twirp.InternalError("Failed to delete resource restriction")
	}

	// No restriction to delete. Return nil.
	if restriction == nil {
		return response, nil
	}

	protoRestriction, err := models.RestrictionToProto(restriction)
	if err != nil {
		msg := "delete resource restriction is malformed"
		log.WithError(err).Error(msg)
		return nil, twirp.InternalError(msg)
	}

	response.Restriction = protoRestriction

	return response, nil
}

// Helper to validate the mutation APIs
func (s *Server) validateRequest(ctx context.Context, req mutateRestrictionRequest) error {
	if req.GetUserId() == "" {
		return twirp.RequiredArgumentError("UserId")
	}

	if req.GetOwnerId() == "" {
		return twirp.RequiredArgumentError("OwnerId")
	}

	if req.GetResource().GetId() == "" {
		return twirp.RequiredArgumentError("ResourceId")
	}

	restriction := req.GetRestriction()
	if restriction != nil {
		if len(restriction.Exemptions) > 0 {
			// Sending any exemptions in a mutation request is invalid
			return twirp.InvalidArgumentError("Exemptions", "cannot be specified in a restriction mutation")
		}
		var incompatibleOpts []nioh.Option
		var tierOptCount int
		for _, opt := range restriction.GetOptions() {
			internalOpt := models.RestrictionOptionFromProto(opt)
			if !internalOpt.ValidFor(models.RestrictionTypeFromProto(restriction.RestrictionType)) {
				incompatibleOpts = append(incompatibleOpts, opt)
			}
			if internalOpt.IsTierOption() {
				tierOptCount++
			}
		}
		if len(incompatibleOpts) > 0 {
			var optNames []string
			for _, opt := range incompatibleOpts {
				optNames = append(optNames, opt.String())
			}
			return twirp.InvalidArgumentError("Options", fmt.Sprintf("%v not allowed for restriction type %s", optNames, restriction.RestrictionType.String()))
		}
		if tierOptCount > 1 {
			return twirp.InvalidArgumentError("Options", "there can only be one tier related option")
		}

		// Check that geoblocks were passed in if this is a geoblock restriction
		if restriction.GetRestrictionType() == nioh.Restriction_GEOBLOCK {
			if req.GetResource().GetType() != nioh.ResourceType_BROADCAST {
				return twirp.InvalidArgumentError("ResourceType", "must be BROADCAST for a geoblock")
			}

			if restriction.GetGeoblocks() == nil || len(restriction.GetGeoblocks()) == 0 {
				return twirp.InvalidArgumentError("Geoblocks", "cannot be empty when the restriction is a geoblock")
			}

			for _, g := range restriction.GetGeoblocks() {
				if len(g.GetCountryCodes()) == 0 {
					return twirp.InvalidArgumentError("Geoblocks", "cannot contain empty country codes")
				}
			}
		}
	}

	// Check for proper ownership based on resource type
	resourceType := req.GetResource().GetType()
	if resourceType == nioh.ResourceType_VIDEO && !(s.isUserChannelOwnerOrAAPMod(ctx, req.GetUserId(), req.GetOwnerId())) {
		// VOD resource, verify owner ID with userID
		return twirp.InvalidArgumentError("UserID", "user cannot create/update restriction for a given resource")
	} else if resourceType == nioh.ResourceType_UNKNOWN {
		// Unknown resource, return bad argument error
		return twirp.RequiredArgumentError("Type")
	}

	return nil
}

func (s *Server) authorizeRequest(ctx context.Context, req mutateRestrictionRequest, fallbackKey userGateKey) error {
	if s.canSkipOwnerIDChecking(ctx, req.GetUserId()) {
		if ctx = s.setContextWithAccessControl(ctx, setResourceRestrictionAdminGateKey, req.GetUserId()); helpers.AccessAllowed(ctx) {
			return nil
		}
	}

	ctx = s.setContextWithAccessControl(ctx, fallbackKey, req.GetUserId())

	if req.GetResource().GetType() == nioh.ResourceType_LIVE && !s.isUserChannelOwner(ctx, req.GetUserId(), req.GetResource().GetId()) {
		return twirp.InvalidArgumentError("UserID", "user cannot create/update restriction for a given resource")
	}

	if !helpers.AccessAllowed(ctx) {
		return twirp.InvalidArgumentError(req.GetUserId(), "UserId is not authorized")
	}

	return nil
}

func (s *Server) canSkipOwnerIDChecking(ctx context.Context, userID string) bool {
	userGate, ok := s.getUserGates()[string(setResourceRestrictionAdminGateKey)]
	if !ok {
		logrus.WithField("gateName", string(setResourceRestrictionAdminGateKey)).Warn("No gating info found in the dynamic config for the gate name")
		return false
	}

	if !userGate.Enabled {
		return false
	}

	return userGate.AllowlistMap[userID]
}

type restrictionAction int

const (
	deleteRestrictionSpade restrictionAction = iota
	setRestrictionSpade
)

func (s *Server) trackSOLRestrictionResourceEvent(ctx context.Context, action restrictionAction, response interface {
	GetOwnerId() string
	GetRestriction() *nioh.Restriction
}) {
	// Bail out if this isn't a populated SOL restriction.
	if response.GetRestriction() == nil || response.GetRestriction().RestrictionType != nioh.Restriction_SUB_ONLY_LIVE {
		return
	}

	clientID, _ := clientid.GetClientID(ctx)

	channelIDInt, err := strconv.Atoi(response.GetOwnerId())
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"channelID": response.GetOwnerId(),
		}).Warn("could not convert channel ID to integer for Spade event in trackSOLRestrictionResourceEvent")
		return
	}

	e := models.ChannelRestrictionStatusEvent{
		ChannelID:       channelIDInt,
		IsRestricted:    action == setRestrictionSpade,
		ToggleMethod:    models.ToggleMethodAPI,
		RestrictionType: models.RestrictionType(response.GetRestriction().GetRestrictionType().String()),
		ClientID:        clientID,
	}

	for _, option := range response.GetRestriction().Options {
		if option == nioh.Option_ALLOW_CHANNEL_MODERATOR {
			e.IncludeMods = true
		}

		if option == nioh.Option_ALLOW_CHANNEL_VIP {
			e.IncludeVIPs = true
		}

		if option == nioh.Option_ALLOW_ALL_TIERS {
			e.IncludeSubs1000, e.IncludeSubs2000, e.IncludeSubs3000 = true, true, true
		}

		if option == nioh.Option_ALLOW_TIER_2_AND_3_ONLY {
			e.IncludeSubs2000, e.IncludeSubs3000 = true, true
		}

		if option == nioh.Option_ALLOW_TIER_3_ONLY {
			e.IncludeSubs3000 = true
		}
	}

	// context.TODO because there is already an HTTP client timeout on this client – decide whether we want redundancy
	// in a context timeout too.
	err = s.clients.Spade.TrackEvent(context.TODO(), e.EventName(), e.Params())
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"error":  err,
			"event":  e.EventName(),
			"params": e.Params(),
		}).Error("failed to send Spade event")
	}
}
