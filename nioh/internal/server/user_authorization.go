package server

import (
	"context"
	"fmt"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/nioh/internal/backend"
	"code.justin.tv/commerce/nioh/internal/models"
	"code.justin.tv/commerce/nioh/rpc/nioh"
	"code.justin.tv/common/chitin"
	"github.com/pkg/errors"
	"github.com/twitchtv/twirp"
)

const (
	getUserAuthorizationsByResourcesCacheControlFmt = "public, max-age=%d"

	getUserAuthorizationsByResourcesCacheDuration = 5 // Cache duration to be inserted for Cache-Control header for GetUserAuthorizationsByResources (in seconds)
)

// GetUserAuthorization accepts either a VOD or Channel access request and returns the response containing the authorization result
func (s *Server) GetUserAuthorization(ctx context.Context, request *nioh.GetUserAuthorizationRequest) (*nioh.GetUserAuthorizationResponse, error) {
	log := logrus.WithField("RequestID", chitin.GetTraceID(ctx)).WithField("UserID", request.GetUserId())

	response := &nioh.GetUserAuthorizationResponse{}
	log.WithField("request", *request).Debug("Handling GetUserAuthorization")

	if request.Resource == nil {
		return response, twirp.RequiredArgumentError("Resource (oneof {Channel,VOD})")
	}

	// If Origin is not a valid origin, sanitize origin to default
	if _, ok := s.config.PreviewOriginsMap[request.GetOrigin()]; !ok && request.GetOrigin() != "" {
		log.WithField("origin", request.GetOrigin()).Debug("Invalid Origin for GetUserAuthorization, resetting...")
		request.Origin = ""
	}

	switch request.Resource.(type) {
	case *nioh.GetUserAuthorizationRequest_Channel:
		return s.handleUserChannelAuthorization(ctx, request)

	case *nioh.GetUserAuthorizationRequest_Vod:
		return s.handleUserVODAuthorization(ctx, request)

	default:
		return response, twirp.InvalidArgumentError("Resource", "Unrecognized object sent as Resource")
	}
}

// GetUserAuthorizationsByResources accepts lists of Channels and returns whether and why a user is authorized
// Note: Order is retained
func (s *Server) GetUserAuthorizationsByResources(ctx context.Context, request *nioh.GetUserAuthorizationsByResourcesRequest) (*nioh.GetUserAuthorizationsByResourcesResponse, error) {
	log := logrus.WithField("RequestID", chitin.GetTraceID(ctx)).WithField("UserID", request.GetUserId())

	response := &nioh.GetUserAuthorizationsByResourcesResponse{UserId: request.UserId}
	log.WithField("request", *request).Debug("Handling GetUserAuthorizationsByResources")

	channelResults, err := s.handleUserChannelAuthorizations(ctx, request.UserId, request.Channels)
	if err != nil {
		errMsg := "Failed to get user authorizations for channels"
		log.WithError(err).Error(errMsg)
		return nil, twirp.InternalError(errMsg)
	}
	setResponseHeader(ctx, cacheControlKey, fmt.Sprintf(getUserAuthorizationsByResourcesCacheControlFmt, getUserAuthorizationsByResourcesCacheDuration))
	response.ChannelAuthorizations = channelResults

	// TODO: Parallelize?
	// (there is no known case where the upstream actually makes a request that contains both vods and channels - so not much to be gained by parallelizing)
	vodResults, err := s.handleUserVODAuthorizations(ctx, request.UserId, request.Vods)
	if err != nil {
		errMsg := "Failed to get user authorizations for VODs"
		log.WithError(err).Error(errMsg)
		return nil, twirp.InternalError(errMsg)
	}
	response.VodAuthorizations = vodResults

	return response, nil
}

func (s *Server) ListAuthorizedUsers(ctx context.Context, request *nioh.ListAuthorizedUsersRequest) (*nioh.ListAuthorizedUsersResponse, error) {
	return nil, twirp.NewError(twirp.Unimplemented, "ListAuthorizedUsers is no longer available, please use ListAuthorizedUsersV2")
}

func (s *Server) ListAuthorizedUsersV2(ctx context.Context, request *nioh.ListAuthorizedUsersRequestV2) (*nioh.ListAuthorizedUsersResponseV2, error) {
	log := logrus.WithField("RequestID", chitin.GetTraceID(ctx))
	log.WithField("request", *request).Debug("Handling ListAuthorizedUsersV2")

	if request.Resource == nil {
		return nil, twirp.RequiredArgumentError("Resource")
	}

	switch request.Resource.(type) {
	case *nioh.ListAuthorizedUsersRequestV2_Channel:
		return s.handleAuthorizedUsersForChannelV2(ctx, request)
	case *nioh.ListAuthorizedUsersRequestV2_Vod:
		return nil, twirp.NewError(twirp.Unimplemented, "ListAuthorizedUsers for VOD resource is not implemented yet")
	default:
		return nil, twirp.InvalidArgumentError("Resource", "Unrecognized object sent as Resource")
	}
}

func (s *Server) handleUserVODAuthorization(ctx context.Context, request *nioh.GetUserAuthorizationRequest) (*nioh.GetUserAuthorizationResponse, error) {
	log := logrus.WithField("RequestID", chitin.GetTraceID(ctx)).WithField("UserID", request.GetUserId())

	authorizationRequest, err := models.VODAuthorizationRequestFromProto(request)
	if err != nil {
		log.WithField("Request", *request).Error("Failed to convert proto VOD request into VODAuthorizationRequest")

		if badReasonErr, ok := errors.Cause(err).(models.BadReasonDataError); ok {
			return nil, twirp.InvalidArgumentError("HedwigReasonData", badReasonErr.Error())
		}

		// This should be unreachable as long as the only errors returned from VODAuthorizationRequestFromProto are unmarshalling errors
		return nil, twirp.InternalError("Failed to parse GetUserAuthorizationRequest as VOD request.")
	}

	if authorizationRequest.Reason != nil && authorizationRequest.Reason.ProductName == "" {
		return nil, twirp.RequiredArgumentError("VOD.HedwigReason.ProductName")
	}

	authorization, err := s.backend.GetUserVODAuthorization(ctx, authorizationRequest)
	if err != nil {
		if _, ok := err.(backend.ProductNotFoundError); ok {
			log.WithField("Product Shortname", authorizationRequest.Reason.ProductName).Info("Product not found with shortname")
			return nil, twirp.NotFoundError(err.Error())
		}

		log.WithError(err).Error("Failed to GetUserVODAuthorization")
		return getEmergencyAllowUserAuthorizationResponse(ctx, request), twirp.InternalError("Failed to GetUserVODAuthorization")
	}

	protoResponse, err := models.VODAuthorizationToProto(authorization)
	if err != nil {
		return nil, twirp.InternalError("Failed to parse result data to response")
	}
	// This is kind of ugly – reattach the JSON reason data since it's lost in the conversions to/from
	// the internal models, but may be useful in the proto response.
	protoResponse.GetVod().HedwigReasonData = request.GetVod().HedwigReasonData

	return protoResponse, nil
}

func (s *Server) handleUserVODAuthorizations(ctx context.Context, userID string, vods []*nioh.VOD) ([]*nioh.UserAuthorizationForResource, error) {
	log := logrus.WithField("RequestID", chitin.GetTraceID(ctx))
	vodReq := &models.VODAuthorizationsRequest{
		UserID:   userID,
		VideoIDs: make([]string, len(vods)),
	}

	for i, vod := range vods {
		vodReq.VideoIDs[i] = vod.VideoId
	}

	results, err := s.backend.GetUserVODAuthorizations(ctx, vodReq)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get user vod authorizations from backend")
	}

	convertedResults := make([]*nioh.UserAuthorizationForResource, len(results))
	for i, vodAuth := range results {
		if vodAuth.Error != nil {
			log.WithError(vodAuth.Error).Error("failed to get individual vod authorization from handleUserVODAuthorizations")
		}

		proto, err := models.VODAuthorizationToUserAuthorizationForResourceProto(vodAuth)
		if err != nil {
			return nil, errors.Wrap(err, "failed to convert results to proto in handleUserVODAuthorizations")
		}

		convertedResults[i] = proto
	}
	return convertedResults, nil
}

func (s *Server) handleUserChannelAuthorization(ctx context.Context, request *nioh.GetUserAuthorizationRequest) (*nioh.GetUserAuthorizationResponse, error) {
	log := logrus.WithField("RequestID", chitin.GetTraceID(ctx)).WithField("UserID", request.GetUserId())

	channel := request.GetChannel()
	if channel == nil {
		return nil, twirp.RequiredArgumentError("Channel")
	}

	if channel.Id == "" {
		return nil, twirp.RequiredArgumentError("Channel.Id")
	}

	channelAuthorization, err := s.backend.GetUserChannelAuthorization(ctx, &models.ChannelAuthorizationRequest{
		UserID:         request.GetUserId(),
		ChannelID:      channel.GetId(),
		ConsumePreview: request.GetConsumePreview(),
		Origin:         request.GetOrigin(),
	})
	if err != nil {
		msg := "failed to get user channel authorization"
		log.WithError(errors.WithStack(err)).Error(msg)
		return getEmergencyAllowUserAuthorizationResponse(ctx, request), twirp.InternalError(msg)
	}

	return models.ChannelAuthorizationToGetUserAuthorizationForResourceProto(channelAuthorization)
}

func (s *Server) handleUserChannelAuthorizations(ctx context.Context, userID string, channels []*nioh.Channel) ([]*nioh.UserAuthorizationForResource, error) {
	log := logrus.WithField("RequestID", chitin.GetTraceID(ctx))
	channelReq := &models.ChannelAuthorizationsRequest{
		UserID:     userID,
		ChannelIDs: make([]string, 0, len(channels)),
	}

	for _, channel := range channels {
		channelReq.ChannelIDs = append(channelReq.ChannelIDs, channel.Id)
	}

	results, err := s.backend.GetUserChannelAuthorizations(ctx, channelReq)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get user channel authorizations from backend")
	}

	convertedResults := make([]*nioh.UserAuthorizationForResource, 0, len(results))

	for _, channelAuth := range results {
		if channelAuth.Error != nil {
			log.WithError(channelAuth.Error).Error("failed to get individual channel authorization from handleUserChannelAuthorizations")
		}

		proto, err := models.ChannelAuthorizationToUserAuthorizationForResourceProto(channelAuth)
		if err != nil {
			return nil, errors.Wrap(err, "failed to convert results to proto in handleUserChannelAuthorizations")
		}
		convertedResults = append(convertedResults, proto)
	}

	return convertedResults, nil
}

func (s *Server) handleAuthorizedUsersForChannelV2(ctx context.Context, request *nioh.ListAuthorizedUsersRequestV2) (*nioh.ListAuthorizedUsersResponseV2, error) {
	channel := request.GetChannel()

	if channel.Id == "" {
		return nil, twirp.RequiredArgumentError("Channel.Id")
	}

	userIDs, isRestricted, nextCursor, err := s.backend.GetAuthorizedUsersForChannelV2(ctx, channel.Id, request.Limit, request.Cursor)
	if err != nil {
		errMsg := "failed to get authorized users"
		logrus.WithField("Request", *request).WithError(err).Error(errMsg)
		return nil, twirp.InternalError(errMsg)
	}

	response := &nioh.ListAuthorizedUsersResponseV2{
		UserIds:              userIDs,
		ResourceIsRestricted: isRestricted,
		Resource: &nioh.ListAuthorizedUsersResponseV2_Channel{
			Channel: channel,
		},
	}

	if nextCursor != "" {
		response.NextCursor = nextCursor
	}

	return response, nil
}

func getEmergencyAllowUserAuthorizationResponse(ctx context.Context, request *nioh.GetUserAuthorizationRequest) *nioh.GetUserAuthorizationResponse {
	log := logrus.WithField("RequestID", chitin.GetTraceID(ctx)).WithField("UserID", request.GetUserId())

	res := &nioh.GetUserAuthorizationResponse{
		UserId:               request.UserId,
		UserIsAuthorized:     true,
		ResourceIsRestricted: false,
	}

	switch resource := request.Resource.(type) {
	case *nioh.GetUserAuthorizationRequest_Channel:
		res.Resource = &nioh.GetUserAuthorizationResponse_Channel{
			Channel: resource.Channel,
		}
	case *nioh.GetUserAuthorizationRequest_Vod:
		res.Resource = &nioh.GetUserAuthorizationResponse_Vod{
			Vod: resource.Vod,
		}
	default:
		log.Error("Unknown type in Resource field of request while calling `getEmergencyAllowAccess`")
	}

	return res
}
