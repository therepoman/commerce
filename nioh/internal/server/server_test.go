package server

import (
	e "errors"
	"net/http"
	"testing"

	"github.com/cactus/go-statsd-client/statsd"
	. "github.com/smartystreets/goconvey/convey"

	"context"

	"code.justin.tv/commerce/nioh/config"
	configMocks "code.justin.tv/commerce/nioh/config/mocks"
	"code.justin.tv/commerce/nioh/internal/backend"
	backendMock "code.justin.tv/commerce/nioh/internal/backend/mocks"
	"code.justin.tv/commerce/nioh/internal/clients"
	"code.justin.tv/commerce/nioh/internal/errors"
	"code.justin.tv/commerce/nioh/internal/models"
	errorloggerMock "code.justin.tv/commerce/nioh/pkg/errorlogger/mocks"
	"code.justin.tv/discovery/liveline/proto/liveline"
	"github.com/stretchr/testify/mock"
)

type serverMockClients interface {
	AssertExpectations(t *testing.T)
}

type serverMockClientsImpl struct {
	clients       *clients.MockClients
	backend       *backendMock.Backend
	dynamicConfig *configMocks.IDynamicConfiguration
	errorlogger   *errorloggerMock.ErrorLogger
}

func mockUserGates() map[string]config.UserGateFlags {
	return map[string]config.UserGateFlags{
		string(multiviewAdminGateKey):         {Enabled: false},
		string(setResourceRestrictionGateKey): {Enabled: false},
	}
}

func initTestableServer(cfg *config.Configuration) (*Server, *serverMockClientsImpl) {
	backend := &backendMock.Backend{}
	clientsImpl, clientsMocks := clients.InitMockClients()
	errorlogger := &errorloggerMock.ErrorLogger{}
	stats, _ := statsd.NewNoopClient()
	dynamicCfg := new(configMocks.IDynamicConfiguration)
	serverConfig := ServerConfig{
		Backend:       backend,
		Clients:       clientsImpl,
		Config:        cfg,
		DynamicConfig: dynamicCfg,
		Errorlogger:   errorlogger,
		Stats:         stats,
		S2SName:       "",
	}
	server, _ := NewServer(serverConfig)

	mocks := &serverMockClientsImpl{
		backend:       backend,
		clients:       clientsMocks,
		dynamicConfig: dynamicCfg,
		errorlogger:   errorlogger,
	}
	return server, mocks
}

func TestGetStatusCode(t *testing.T) {
	cfg := &config.Configuration{}
	Convey("getStatusCode", t, func() {
		s, _ := initTestableServer(cfg)

		Convey("handles nioh error", func() {
			err := &errors.GenericInternal{}
			So(s.getStatusCode(err), ShouldEqual, http.StatusInternalServerError)
		})

		Convey("handles generic error", func() {
			err := e.New("random 500")
			So(s.getStatusCode(err), ShouldEqual, http.StatusInternalServerError)
		})
	})
}

func TestInternalHelpers(t *testing.T) {
	Convey("BuildAccessInfo", t, func() {
		gateKey := multiviewAdminGateKey
		gateName := string(gateKey)
		defaultUserGates := map[string]config.UserGateFlags{
			gateName: {Enabled: true, AllowlistEnabled: true},
		}
		cfg := &config.Configuration{
			UserGates: defaultUserGates,
		}
		var dynamicConfigError error
		var usergates map[string]config.UserGateFlags
		var s *Server
		var clientMocks *serverMockClientsImpl

		// Expected output for each test case
		var expectedAccess bool

		Convey("with no UserGateFlags", func() {
			usergates = map[string]config.UserGateFlags{}
			s, clientMocks = initTestableServer(cfg)
			expectedAccess = false
		})

		Convey("with UserGateFlags", func() {
			// Pre-conditions for each test case
			var gateEnabled bool
			var staffEnabled bool
			var allowlistEnabled bool
			var allowlisted bool
			var lowRiskEnabled bool
			var overrideAllow bool
			var overrideDeny bool

			Convey("with UserGateFlags disabled and user not allowlisted", func() {
				s, clientMocks = initTestableServer(cfg)
				expectedAccess = true
			})

			Convey("with UserGateFlags enabled", func() {
				gateEnabled = true

				Convey("with user allowlisted", func() {
					allowlistEnabled = true
					allowlisted = true
					s, clientMocks = initTestableServer(cfg)
					clientMocks.clients.UsersClient.On("IsUserStaff", mock.Anything, mock.Anything).Return(false, nil)
					expectedAccess = true
				})

				Convey("with user not allowlisted and staff enabled", func() {
					allowlistEnabled = true
					staffEnabled = true

					s, clientMocks = initTestableServer(cfg)
					Convey("with user not as staff", func() {
						clientMocks.clients.UsersClient.On("IsUserStaff", mock.Anything, mock.Anything).Return(false, nil)
						expectedAccess = false
					})

					Convey("with user as staff", func() {
						clientMocks.clients.UsersClient.On("IsUserStaff", mock.Anything, mock.Anything).Return(true, nil)
						expectedAccess = true
					})
				})

				Convey("with user not allowlisted and staff disabled", func() {
					allowlistEnabled = true
					staffEnabled = false

					s, clientMocks = initTestableServer(cfg)
					Convey("with user not as staff", func() {
						clientMocks.clients.UsersClient.On("IsUserStaff", mock.Anything, mock.Anything).Return(false, nil)
					})

					Convey("with user as staff", func() {
						clientMocks.clients.UsersClient.On("IsUserStaff", mock.Anything, mock.Anything).Return(true, nil)
					})

					expectedAccess = false
				})

				Convey("with user not allowlisted but overridden", func() {
					allowlistEnabled = true
					overrideAllow = true
					expectedAccess = true
					s, clientMocks = initTestableServer(cfg)
				})

				Convey("with user allowlisted but overridden", func() {
					allowlistEnabled = true
					overrideDeny = true
					expectedAccess = false
					s, clientMocks = initTestableServer(cfg)
				})

				Convey("with low risk gating", func() {
					lowRiskEnabled = true

					s, clientMocks = initTestableServer(cfg)
					Convey("with user low risk", func() {
						clientMocks.clients.Pepper.On("IsUserLowRisk", mock.Anything, mock.Anything).Return(true, nil)
						expectedAccess = true
					})

					Convey("with user not low risk", func() {
						clientMocks.clients.Pepper.On("IsUserLowRisk", mock.Anything, mock.Anything).Return(false, nil)
					})

					Convey("with pepper error", func() {
						clientMocks.clients.Pepper.On("IsUserLowRisk", mock.Anything, mock.Anything).Return(false, e.New("iron man abandons pepper"))
						expectedAccess = false // pepper fails closed
					})
				})
			})

			gate := config.UserGateFlags{Enabled: gateEnabled, StaffEnabled: staffEnabled, AllowlistEnabled: allowlistEnabled, LowRiskGatingEnabled: lowRiskEnabled}
			if allowlisted {
				gate.AllowlistMap = map[string]bool{"123": true}
			}
			if overrideAllow {
				gate.OverrideMap = map[string]bool{"123": true}
			}
			if overrideDeny {
				gate.OverrideMap = map[string]bool{"123": false}
			}
			usergates = map[string]config.UserGateFlags{gateName: gate}
		})

		Convey("with dynamic config failure", func() {
			dynamicConfigError = e.New("an error")
			s, clientMocks = initTestableServer(cfg)
			clientMocks.clients.UsersClient.On("IsUserStaff", mock.Anything, mock.Anything).Return(false, nil)
			expectedAccess = false // gating is enabled in static config, which it should fallback to
		})

		clientMocks.dynamicConfig.On("GetUserGates").Return(usergates, dynamicConfigError)
		accessInfo := s.BuildAccessInfo(context.Background(), gateKey, "123")
		So(accessInfo, ShouldNotBeNil)
		So(accessInfo.CanAccess, ShouldEqual, expectedAccess)
	})

	Convey("isUserChannelOwner", t, func() {
		server, mockClients := initTestableServer(&config.Configuration{})
		ctx := context.Background()
		testUserID := "id"
		testChannelID := "not-user"

		Convey("returning true", func() {
			Convey("with matching user and channel id", func() {
				testChannelID = testUserID
			})

			Convey("with user owning chanlet", func() {
				mockClients.backend.On("GetChanletByID", mock.Anything, testChannelID).Return(&models.Chanlet{OwnerChannelID: testUserID}, nil)
			})

			val := server.isUserChannelOwner(ctx, testUserID, testChannelID)
			So(val, ShouldBeTrue)
		})

		Convey("returning false", func() {
			Convey("with user not owning chanlet", func() {
				mockClients.backend.On("GetChanletByID", mock.Anything, testChannelID).Return(&models.Chanlet{OwnerChannelID: testChannelID}, nil)
			})

			Convey("with non-existent chanlet", func() {
				mockClients.backend.On("GetChanletByID", mock.Anything, testChannelID).Return(nil, nil)
			})

			Convey("with chanlet not found error", func() {
				mockClients.backend.On("GetChanletByID", mock.Anything, testChannelID).Return(nil, backend.ErrChanletNotFound)
			})

			Convey("with backend error", func() {
				mockClients.backend.On("GetChanletByID", mock.Anything, testChannelID).Return(nil, e.New("oh no"))
			})

			val := server.isUserChannelOwner(ctx, testUserID, testChannelID)
			So(val, ShouldBeFalse)
		})
	})

	Convey("isUserChannelAAPMod", t, func() {
		server, mockClients := initTestableServer(&config.Configuration{})
		ctx := context.Background()
		testUserID := "id"
		testChannelID := "not-user"

		Convey("returning true", func() {
			Convey("with matching user and channel id", func() {
				testChannelID = testUserID
			})

			Convey("with user in channel mod list", func() {
				mockClients.dynamicConfig.On("GetAllAccessPassModeratorsForChannel", testChannelID).Return(map[string]bool{testUserID: true}, nil)
			})

			val := server.isUserChannelAAPMod(ctx, testUserID, testChannelID)
			So(val, ShouldBeTrue)
		})

		Convey("returning false", func() {
			Convey("with user not in channel mod list", func() {
				mockClients.dynamicConfig.On("GetAllAccessPassModeratorsForChannel", testChannelID).Return(map[string]bool{}, nil)
			})

			Convey("with backend error", func() {
				mockClients.dynamicConfig.On("GetAllAccessPassModeratorsForChannel", testChannelID).Return(nil, e.New("oh no"))
			})

			val := server.isUserChannelAAPMod(ctx, testUserID, testChannelID)
			So(val, ShouldBeFalse)
		})
	})

	SkipConvey("isChannelLive", t, func() {
		server, mockClients := initTestableServer(&config.Configuration{})
		ctx := context.Background()
		channelID := "1234"

		Convey("when not live", func() {
			mockClients.clients.Liveline.On("GetStreamsByChannelIDs", mock.Anything, []string{channelID}).Return(&liveline.StreamsResponse{Streams: []*liveline.StreamResponse{}}, nil)
			isLive, err := server.isChannelLive(ctx, channelID)
			So(isLive, ShouldBeFalse)
			So(err, ShouldBeNil)
		})

		Convey("when live", func() {
			mockClients.clients.Liveline.On("GetStreamsByChannelIDs", mock.Anything, []string{channelID}).
				Return(&liveline.StreamsResponse{Streams: []*liveline.StreamResponse{{ChannelId: "123"}}}, nil)
			isLive, err := server.isChannelLive(ctx, channelID)
			So(isLive, ShouldBeTrue)
			So(err, ShouldBeNil)
		})

		Convey("with liveline error", func() {
			mockClients.clients.Liveline.On("GetStreamsByChannelIDs", mock.Anything, []string{channelID}).Return(nil, e.New("what's liveline"))

			isLive, err := server.isChannelLive(ctx, channelID)
			So(isLive, ShouldBeFalse)
			So(err, ShouldNotBeNil)
		})
	})
}

func (s *serverMockClientsImpl) AssertExpectations(t *testing.T) {
	s.backend.AssertExpectations(t)
	s.errorlogger.AssertExpectations(t)
}
