package server

import (
	"context"
	"testing"

	"code.justin.tv/commerce/nioh/internal/models"
	"code.justin.tv/commerce/nioh/internal/testdata"
	"code.justin.tv/commerce/nioh/rpc/nioh"

	"code.justin.tv/commerce/nioh/config"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestChannelRestrictionV2API(t *testing.T) {
	Convey("Test ChannelRestriction V2 API", t, func() {
		testChannelID := "146851449"

		cfg := &config.Configuration{}
		server, mockClients := initTestableServer(cfg)
		mockClients.dynamicConfig.On("GetUserGates").Return(mockUserGates(), nil)
		testRestriction := testdata.Restriction(testChannelID)

		Convey("Test GetChannelRestriction", func() {
			ctx := context.Background()
			req := &nioh.GetChannelRestrictionRequest{
				ChannelId: testChannelID,
			}

			Convey("with missing ChannelId", func() {
				req.ChannelId = ""
				_, err := server.GetChannelRestriction(ctx, req)
				So(err, ShouldNotBeNil)
			})

			Convey("with no restriction", func() {
				mockClients.backend.On("GetRestriction", mock.Anything, models.Channel{ID: req.ChannelId}, mock.Anything).
					Return(nil, nil)

				res, err := server.GetChannelRestriction(ctx, req)
				So(err, ShouldBeNil)
				So(res, ShouldResemble, &nioh.GetChannelRestrictionResponse{
					ChannelId:   req.ChannelId,
					Restriction: nil,
				})
			})

			Convey("with restriction", func() {
				mockClients.backend.On("GetRestriction", mock.Anything, models.Channel{ID: req.ChannelId}, mock.Anything).
					Return(testRestriction, nil)

				res, err := server.GetChannelRestriction(ctx, req)
				So(err, ShouldBeNil)
				So(res, ShouldResemble, &nioh.GetChannelRestrictionResponse{
					ChannelId:   req.ChannelId,
					Restriction: testdata.ProtoRestriction(testChannelID),
				})
			})

			Convey("with backend error", func() {
				mockClients.backend.On("GetRestriction", mock.Anything, models.Channel{ID: req.ChannelId}, mock.Anything).
					Return(nil, errors.New("your backend asplode"))

				_, err := server.GetChannelRestriction(ctx, req)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("Test GetChannelRestrictions", func() {
			ctx := context.Background()
			testChannelID1 := "146851449"
			testChannelID2 := "234234232"
			testRestriction1 := testdata.Restriction(testChannelID1)
			testRestriction2 := testdata.Restriction(testChannelID2)
			expected := &nioh.GetChannelRestrictionsResponse{
				ChannelRestrictions: []*nioh.ChannelRestrictionResponse{
					{
						ChannelId: testChannelID1,
					},
					{
						ChannelId: testChannelID2,
					},
				},
			}
			backendError := errors.New("your backend asplode")
			expectedBackendErrorStr := "twirp error internal: unexpected error while handling GetChannelRestriction"

			Convey("with no restrictions", func() {
				mockClients.backend.On("GetRestriction", mock.Anything, models.Channel{ID: testChannelID1}, mock.Anything).Return(nil, nil)
				mockClients.backend.On("GetRestriction", mock.Anything, models.Channel{ID: testChannelID2}, mock.Anything).Return(nil, nil)
			})

			Convey("with one error", func() {
				mockClients.backend.On("GetRestriction", mock.Anything, models.Channel{ID: testChannelID1}, mock.Anything).Return(nil, backendError)
				mockClients.backend.On("GetRestriction", mock.Anything, models.Channel{ID: testChannelID2}, mock.Anything).Return(testRestriction2, nil)
				expected.ChannelRestrictions[0].Error = expectedBackendErrorStr
				protoRes2, err := models.RestrictionToProto(testRestriction2)
				So(err, ShouldBeNil)
				expected.ChannelRestrictions[1].Restriction = protoRes2
			})

			Convey("with both errors", func() {
				mockClients.backend.On("GetRestriction", mock.Anything, models.Channel{ID: testChannelID1}, mock.Anything).Return(nil, backendError)
				mockClients.backend.On("GetRestriction", mock.Anything, models.Channel{ID: testChannelID2}, mock.Anything).Return(nil, backendError)
				expected.ChannelRestrictions[0].Error = expectedBackendErrorStr
				expected.ChannelRestrictions[1].Error = expectedBackendErrorStr
			})

			Convey("with both successes", func() {
				mockClients.backend.On("GetRestriction", mock.Anything, models.Channel{ID: testChannelID1}, mock.Anything).Return(testRestriction1, nil)
				mockClients.backend.On("GetRestriction", mock.Anything, models.Channel{ID: testChannelID2}, mock.Anything).Return(testRestriction2, nil)
				protoRes1, err := models.RestrictionToProto(testRestriction1)
				So(err, ShouldBeNil)
				protoRes2, err := models.RestrictionToProto(testRestriction2)
				So(err, ShouldBeNil)
				expected.ChannelRestrictions[0].Restriction = protoRes1
				expected.ChannelRestrictions[1].Restriction = protoRes2
			})

			req := &nioh.GetChannelRestrictionsRequest{
				ChannelIds: []string{testChannelID1, testChannelID2},
			}

			res, err := server.GetChannelRestrictions(ctx, req)
			So(err, ShouldBeNil)
			So(res, ShouldResemble, expected)
		})
	})
}
