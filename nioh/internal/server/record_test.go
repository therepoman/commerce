package server

import (
	"context"
	"testing"

	"github.com/pkg/errors"
	"github.com/twitchtv/twirp"

	"code.justin.tv/commerce/nioh/config"
	"code.justin.tv/commerce/nioh/internal/backend/mocks"
	"code.justin.tv/commerce/nioh/rpc/nioh"

	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestRecordAPI(t *testing.T) {
	Convey("Test Record API", t, func() {
		mockBackend := new(mocks.Backend)
		mockBackend.On("GetRecord", mock.Anything, mock.Anything).Return(&nioh.Record{}, nil)

		Convey("Test GetRecord", func() {
			testCases := []struct {
				description     string
				request         *nioh.GetRecordRequest
				backendErr      error
				expectedErrType twirp.Error
			}{
				{
					description: "happy path",
					request: &nioh.GetRecordRequest{
						ChannelId: "1234",
						RecordId:  "4321",
					},
					backendErr:      nil,
					expectedErrType: nil,
				},
				{
					description: "with backend error",
					request: &nioh.GetRecordRequest{
						ChannelId: "1234",
						RecordId:  "4321",
					},
					backendErr:      errors.New("your backend asplode"),
					expectedErrType: twirp.InternalError(""),
				},
				{
					description: "with missing ChannelId",
					request: &nioh.GetRecordRequest{
						ChannelId: "",
						RecordId:  "4321",
					},
					backendErr:      nil,
					expectedErrType: twirp.RequiredArgumentError(""),
				},
				{
					description: "with missing RecordId",
					request: &nioh.GetRecordRequest{
						ChannelId: "1234",
						RecordId:  "",
					},
					backendErr:      nil,
					expectedErrType: twirp.RequiredArgumentError(""),
				},
			}

			for _, tt := range testCases {
				server, serverMocks := initTestableServer(&config.Configuration{})
				serverMocks.backend.On("GetRecord", mock.Anything, mock.Anything).Return(&nioh.Record{}, tt.backendErr)

				Convey(tt.description, func() {
					_, err := server.GetRecord(context.Background(), tt.request)
					So(err, ShouldHaveSameTypeAs, tt.expectedErrType)
				})
			}
		})
	})
}
