package server

import (
	"context"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/nioh/internal/models"
	"code.justin.tv/commerce/nioh/rpc/nioh"
	"code.justin.tv/common/chitin"
	"code.justin.tv/common/twirp"
)

// GetOwnerChanletAttributes is used to get owner chanlet attributes.
func (s *Server) GetOwnerChanletAttributes(ctx context.Context, req *nioh.GetOwnerChanletAttributesRequest) (*nioh.GetOwnerChanletAttributesResponse, error) {
	log := logrus.WithField("RequestID", chitin.GetTraceID(ctx))

	if req.OwnerChanletId == "" {
		return nil, twirp.RequiredArgumentError("owner_chanlet_id")
	}

	if req.UserId == "" {
		return nil, twirp.RequiredArgumentError("user_id")
	}

	chanlet, err := s.backend.GetChanletByID(ctx, req.OwnerChanletId)
	if err != nil {
		log.WithError(err).Error("GetOwnerChanletAttributes failed to get chanlet from backend.")
		return nil, twirp.InternalError("unable to retrieve owner chanlet")
	}

	if chanlet == nil || !chanlet.IsOwnerChanlet() {
		return nil, twirp.NotFoundError("no such owner chanlet")
	}

	return &nioh.GetOwnerChanletAttributesResponse{
		Attributes: models.OwnerChanletAttributesToProto(chanlet.OwnerChanletAttributes),
	}, nil
}

// UpdateOwnerChanletAttributes is used to update owner chanlet attributes.
func (s *Server) UpdateOwnerChanletAttributes(ctx context.Context, req *nioh.UpdateOwnerChanletAttributesRequest) (*nioh.UpdateOwnerChanletAttributesResponse, error) {
	log := logrus.WithField("RequestID", chitin.GetTraceID(ctx))

	if req.OwnerChanletId == "" {
		return nil, twirp.RequiredArgumentError("owner_chanlet_id")
	}

	if req.UserId == "" {
		return nil, twirp.RequiredArgumentError("user_id")
	}

	if req.Attributes == nil {
		return nil, twirp.RequiredArgumentError("attributes")
	}

	if req.UserId != req.OwnerChanletId {
		return nil, twirp.NewError(twirp.PermissionDenied, "you cannot update owner configurations as a non-owner")
	}

	chanlet, err := s.backend.GetChanletByID(ctx, req.OwnerChanletId)
	if err != nil {
		log.WithError(err).Error("UpdateOwnerChanletAttributes failed to get chanlet from backend.")
		return nil, twirp.InternalError("unable to retrieve owner chanlet")
	}

	if chanlet == nil || !chanlet.IsOwnerChanlet() {
		return nil, twirp.NotFoundError("no such owner chanlet")
	}

	chanlet.OwnerChanletAttributes = models.OwnerChanletAttributesFromProto(req.GetAttributes())

	err = s.backend.AddOrReplaceChanlet(ctx, chanlet)
	if err != nil {
		log.WithError(err).Error("UpdateOwnerChanletAttributes failed to get chanlet from backend.")
		return nil, twirp.InternalError("unable to retrieve owner chanlet")
	}

	return &nioh.UpdateOwnerChanletAttributesResponse{
		Attributes: req.GetAttributes(),
	}, nil
}
