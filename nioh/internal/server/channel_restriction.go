package server

import (
	"context"
	"sync"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/nioh/internal/models"
	"code.justin.tv/commerce/nioh/rpc/nioh"
	"code.justin.tv/common/chitin"
	"code.justin.tv/common/twirp"
)

// DEPRECATED: Use GetRestrictionsByResources
func (s *Server) GetChannelRestriction(ctx context.Context, req *nioh.GetChannelRestrictionRequest) (*nioh.GetChannelRestrictionResponse, error) {
	if req.GetChannelId() == "" {
		return nil, twirp.RequiredArgumentError("ChannelId")
	}

	response := &nioh.GetChannelRestrictionResponse{
		ChannelId: req.GetChannelId(),
	}

	log := logrus.WithFields(logrus.Fields{
		"RequestID": chitin.GetTraceID(ctx),
		"ChannelID": req.ChannelId,
	})

	restriction, err := s.backend.GetRestriction(ctx, models.Channel{ID: req.GetChannelId()}, false)
	if err != nil {
		msg := "unexpected error while handling GetChannelRestriction"
		log.WithError(err).Error(msg)
		return nil, twirp.InternalError(msg)
	}

	if restriction == nil {
		return response, nil
	}

	protoRestriction, err := models.RestrictionToProto(restriction)
	if err != nil {
		msg := "stored restriction is malformed"
		log.WithError(err).Error(msg)
		return nil, twirp.InternalError(msg)
	}

	response.Restriction = protoRestriction

	return response, nil
}

// DEPRECATED: Use GetRestrictionsByResources
// GetChannelRestrictions returns V2 restrictions for the passed channel ID
func (s *Server) GetChannelRestrictions(ctx context.Context, req *nioh.GetChannelRestrictionsRequest) (*nioh.GetChannelRestrictionsResponse, error) {
	log := logrus.WithField("RequestID", chitin.GetTraceID(ctx))
	log.WithField("request", *req).Debug("Handling GetChannelRestrictions")

	resp := &nioh.GetChannelRestrictionsResponse{
		ChannelRestrictions: make([]*nioh.ChannelRestrictionResponse, 0, len(req.ChannelIds)),
	}
	restrictionsByChannelID := make(map[string]*nioh.ChannelRestrictionResponse)

	wg := &sync.WaitGroup{}
	mutex := &sync.Mutex{}

	// TODO: GetRestrictions DAO method is added, but we are using the fanned out individual GetItem for error granularity.
	//       Research if there is a better way than this. I think this is okay. DynamoDB read capacities are based on byte usage, not # of requests.
	for _, channelID := range req.ChannelIds {
		wg.Add(1)
		go func(channelID string) {
			defer wg.Done()
			restriction := &nioh.ChannelRestrictionResponse{
				ChannelId: channelID,
			}
			restrictionFromRPC, err := s.GetChannelRestriction(ctx, &nioh.GetChannelRestrictionRequest{
				ChannelId: channelID,
			})
			if err != nil {
				log.WithError(err).Error("failed to get channel restriction in GetChannelRestrictions")
				restriction.Error = err.Error()
			} else {
				restriction.Restriction = restrictionFromRPC.Restriction
			}
			mutex.Lock()
			restrictionsByChannelID[channelID] = restriction
			mutex.Unlock()
		}(channelID)
	}
	wg.Wait()

	// We retain the order by looping through the passed channel IDs and appending the result stored in the map.
	for _, channelID := range req.ChannelIds {
		restriction, ok := restrictionsByChannelID[channelID]
		if !ok {
			log.WithField("channel_id", channelID).Warn("GetChannelRestrictions did not resolve the passed channel ID")
			restriction = &nioh.ChannelRestrictionResponse{ChannelId: channelID, Error: "internal error; channel not resolved"}
		}
		resp.ChannelRestrictions = append(resp.ChannelRestrictions, restriction)
	}

	return resp, nil
}
