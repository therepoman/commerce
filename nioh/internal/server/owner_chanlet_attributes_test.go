package server

import (
	"context"
	"testing"

	"code.justin.tv/commerce/nioh/config"
	"code.justin.tv/commerce/nioh/internal/models"
	"code.justin.tv/commerce/nioh/pkg/helpers"
	"code.justin.tv/commerce/nioh/rpc/nioh"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestOwnerChanletAttributesAPIs(t *testing.T) {
	Convey("Test API", t, func() {
		server, mockClients := initTestableServer(&config.Configuration{})
		channelID := "123"
		chanlet := &models.Chanlet{
			OwnerChannelID: channelID,
			ChanletID:      channelID,
		}

		ctx := helpers.ContextWithUser(context.Background(), channelID)

		Convey("Test GetOwnerChanletAttributes", func() {
			req := &nioh.GetOwnerChanletAttributesRequest{
				UserId:         channelID,
				OwnerChanletId: channelID,
			}

			Convey("with missing param", func() {
				Convey("userId", func() {
					req.UserId = ""
				})

				Convey("ownerChanletId", func() {
					req.OwnerChanletId = ""
				})

				res, err := server.GetOwnerChanletAttributes(ctx, req)
				So(err, ShouldNotBeNil)
				So(res, ShouldBeNil)
			})

			Convey("with backend error", func() {
				mockClients.backend.On("GetChanletByID", mock.Anything, channelID).Return(nil, errors.New("your backend asplode"))
				res, err := server.GetOwnerChanletAttributes(ctx, req)
				So(err, ShouldNotBeNil)
				So(res, ShouldBeNil)
			})

			Convey("success path", func() {
				expected := &nioh.GetOwnerChanletAttributesResponse{
					Attributes: &nioh.OwnerChanletAttributes{},
				}
				Convey("with no defined attributes", func() {
					mockClients.backend.On("GetChanletByID", mock.Anything, channelID).Return(chanlet, nil)
				})

				Convey("with attributes in db", func() {
					chanlet.OwnerChanletAttributes = &models.OwnerChanletAttributes{
						ChanletsEnabled: true,
					}
					mockClients.backend.On("GetChanletByID", mock.Anything, channelID).Return(chanlet, nil)
					expected.Attributes.ChanletsEnabled = true
				})

				res, err := server.GetOwnerChanletAttributes(ctx, req)
				So(err, ShouldBeNil)
				So(res, ShouldResemble, expected)
			})
		})

		Convey("Test UpdateOwnerChanletAttributes", func() {
			req := &nioh.UpdateOwnerChanletAttributesRequest{
				UserId:         channelID,
				OwnerChanletId: channelID,
				Attributes:     &nioh.OwnerChanletAttributes{},
			}

			Convey("with missing param", func() {
				Convey("userId", func() {
					req.UserId = ""
				})

				Convey("ownerChanletId", func() {
					req.OwnerChanletId = ""
				})

				Convey("attributes", func() {
					req.Attributes = nil
				})

				res, err := server.UpdateOwnerChanletAttributes(ctx, req)
				So(err, ShouldNotBeNil)
				So(res, ShouldBeNil)
			})

			Convey("with non-owner", func() {
				req.UserId = "999999"
				res, err := server.UpdateOwnerChanletAttributes(ctx, req)
				So(err, ShouldNotBeNil)
				So(res, ShouldBeNil)
			})

			Convey("with backend error", func() {
				mockClients.backend.On("GetChanletByID", mock.Anything, channelID).Return(nil, errors.New("your backend asplode"))
				res, err := server.UpdateOwnerChanletAttributes(ctx, req)
				So(err, ShouldNotBeNil)
				So(res, ShouldBeNil)
			})

			Convey("success path", func() {
				req.Attributes.ChanletsEnabled = true
				expected := &nioh.UpdateOwnerChanletAttributesResponse{
					Attributes: &nioh.OwnerChanletAttributes{
						ChanletsEnabled: true,
					},
				}
				mockClients.backend.On("GetChanletByID", mock.Anything, channelID).Return(chanlet, nil)
				mockClients.backend.On("AddOrReplaceChanlet", mock.Anything, mock.Anything).Return(nil)

				res, err := server.UpdateOwnerChanletAttributes(ctx, req)
				So(err, ShouldBeNil)
				So(res, ShouldResemble, expected)
			})
		})
	})
}
