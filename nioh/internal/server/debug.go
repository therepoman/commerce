package server

import (
	"context"
	"encoding/json"
	"net/http"

	"code.justin.tv/commerce/nioh/internal/models"
	"code.justin.tv/commerce/nioh/rpc/nioh"

	log "code.justin.tv/commerce/logrus"
)

// TestConnections writes a message about connection status to the http response.
func (s *Server) TestConnections(w http.ResponseWriter, r *http.Request) {
	testResult := s.backend.TestConnections(context.Background())

	err := writeAsJSON(w, testResult)
	if err != nil {
		log.Error(err)
	}
}

func writeAsJSON(w http.ResponseWriter, responseStruct interface{}) error {
	w.Header().Set("Content-Type", "application/json")
	return json.NewEncoder(w).Encode(responseStruct)
}

func (s *Server) ResetPreview(ctx context.Context, request *nioh.ResetPreviewRequest) (*nioh.ResetPreviewResponse, error) {
	preview, err := s.backend.DeletePreview(ctx, request.UserId, models.Channel{
		ID: request.ChannelId,
	})
	return &nioh.ResetPreviewResponse{
		UserId:    request.UserId,
		ChannelId: request.ChannelId,
		WasReset:  preview != nil,
	}, err
}
