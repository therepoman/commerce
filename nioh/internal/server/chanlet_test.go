package server

import (
	"context"
	"testing"

	"code.justin.tv/commerce/nioh/config"
	"code.justin.tv/commerce/nioh/internal/backend"
	"code.justin.tv/commerce/nioh/internal/models"
	"code.justin.tv/commerce/nioh/internal/testdata"
	"code.justin.tv/commerce/nioh/pkg/helpers"
	"code.justin.tv/commerce/nioh/rpc/nioh"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestChanletAPI(t *testing.T) {
	Convey("Test API", t, func() {
		server, mockClients := initTestableServer(&config.Configuration{})
		mockClients.dynamicConfig.On("GetUserGates").Return(mockUserGates(), nil)
		channelID := "123"
		chanletID := "456"
		chanlet := &models.Chanlet{
			OwnerChannelID:      channelID,
			ChanletID:           chanletID,
			ContentAttributeIDs: testdata.ContentAttributeIDs(),
			ContentAttributes:   testdata.ContentAttributes(),
		}
		ownerChanlet := &models.Chanlet{
			OwnerChannelID: channelID,
			ChanletID:      channelID,
			OwnerChanletAttributes: &models.OwnerChanletAttributes{
				ChanletsEnabled: true,
			},
		}

		ctx := helpers.ContextWithUser(context.Background(), channelID)
		ctx = helpers.ContextWithAccessInfo(ctx, helpers.AccessInfo{
			CanAccess: true,
		})

		Convey("Test GetChanlets", func() {
			req := &nioh.GetChanletsRequest{
				ChannelId: channelID,
			}

			Convey("with success", func() {
				mockClients.backend.On("GetChanlets", mock.Anything, channelID, mock.Anything).Return([]*models.Chanlet{ownerChanlet, chanlet}, nil)
				res, err := server.GetChanlets(ctx, req)
				So(err, ShouldBeNil)
				So(res, ShouldResemble, &nioh.GetChanletsResponse{
					Chanlets: []*nioh.Chanlet{
						{
							OwnerChannelId:    channelID,
							ChanletId:         channelID,
							ContentAttributes: []*nioh.ContentAttribute{},
						},
						{
							OwnerChannelId:    channelID,
							ChanletId:         chanletID,
							ContentAttributes: testdata.ProtoContentAttributes(),
						},
					},
				})
			})

			Convey("with missing channelID", func() {
				req.ChannelId = ""
				res, err := server.GetChanlets(ctx, req)
				So(err, ShouldNotBeNil)
				So(res, ShouldBeNil)
			})

			Convey("with backend failure", func() {
				mockClients.backend.On("GetChanlets", mock.Anything, channelID, mock.Anything).Return(nil, errors.New("test"))
				res, err := server.GetChanlets(ctx, req)
				So(err, ShouldNotBeNil)
				So(res, ShouldBeNil)
			})

			Convey("with blocking flag", func() {
				server.config.BlockGetChanletsFlag = true
				res, err := server.GetChanlets(ctx, req)
				So(err, ShouldBeNil)
				So(res, ShouldResemble, &nioh.GetChanletsResponse{})
			})

			Convey("with chanlets disabled", func() {
				// Send with an owner chanlet with chanlets disabled
				ownerChanlet.OwnerChanletAttributes.ChanletsEnabled = false
				mockClients.backend.On("GetChanlets", mock.Anything, channelID, mock.Anything).Return([]*models.Chanlet{ownerChanlet, chanlet}, nil)

				Convey("and requester is not the owner and bypass flag is on", func() {
					req.BypassDisableFlag = true
					res, err := server.GetChanlets(ctx, req)
					So(err, ShouldBeNil)
					So(res, ShouldResemble, &nioh.GetChanletsResponse{
						Chanlets: []*nioh.Chanlet{},
					})
				})

				Convey("and requester is the owner but bypass flag is off", func() {
					req.UserId = channelID
					res, err := server.GetChanlets(ctx, req)
					So(err, ShouldBeNil)
					So(res, ShouldResemble, &nioh.GetChanletsResponse{
						Chanlets: []*nioh.Chanlet{},
					})
				})

				Convey("and requester is the owner", func() {
					req.BypassDisableFlag = true
					req.UserId = channelID
					res, err := server.GetChanlets(ctx, req)
					So(err, ShouldBeNil)
					So(res, ShouldResemble, &nioh.GetChanletsResponse{
						Chanlets: []*nioh.Chanlet{
							{
								OwnerChannelId:    channelID,
								ChanletId:         channelID,
								ContentAttributes: []*nioh.ContentAttribute{},
							},
							{
								OwnerChannelId:    channelID,
								ChanletId:         chanletID,
								ContentAttributes: testdata.ProtoContentAttributes(),
							},
						},
					})
				})
			})
		})

		Convey("Test IsHiddenChanlet", func() {
			req := &nioh.IsHiddenChanletRequest{
				ChannelId: channelID,
			}

			Convey("with result chanlet", func() {
				mockClients.backend.On("GetChanletByID", mock.Anything, channelID).Return(chanlet, nil)
				res, err := server.IsHiddenChanlet(ctx, req)
				So(err, ShouldBeNil)
				So(res, ShouldResemble, &nioh.IsHiddenChanletResponse{
					IsHiddenChanlet: true,
					OwnerChannelId:  chanlet.OwnerChannelID,
					ChannelId:       channelID,
				})
			})

			Convey("with self chanlet result", func() {
				chanlet.IsPublicChannel = true
				mockClients.backend.On("GetChanletByID", mock.Anything, channelID).Return(chanlet, nil)
				res, err := server.IsHiddenChanlet(ctx, req)
				So(err, ShouldBeNil)
				So(res, ShouldResemble, &nioh.IsHiddenChanletResponse{
					ChannelId: channelID,
				})
			})

			Convey("with missing channelID", func() {
				req.ChannelId = ""
				res, err := server.IsHiddenChanlet(ctx, req)
				So(err, ShouldNotBeNil)
				So(res, ShouldBeNil)
			})

			Convey("with backend failure", func() {
				mockClients.backend.On("GetChanletByID", mock.Anything, channelID).Return(nil, errors.New("test"))
				res, err := server.IsHiddenChanlet(ctx, req)
				So(err, ShouldBeNil)
				So(res, ShouldResemble, &nioh.IsHiddenChanletResponse{
					ChannelId: channelID,
				})
			})
		})

		Convey("Test ListAllChanlets", func() {
			req := &nioh.ListAllChanletsRequest{
				Cursor: "",
			}
			chanlets := []*models.Chanlet{
				{
					ChanletID:      "1",
					OwnerChannelID: "2",
				},
			}

			Convey("with backend error", func() {
				mockClients.backend.On("ListAllChanlets", mock.Anything, "").Return(nil, "", errors.New("test"))
				res, err := server.ListAllChanlets(ctx, req)
				So(err, ShouldNotBeNil)
				So(res, ShouldBeNil)
			})

			Convey("with backend success", func() {
				mockClients.backend.On("ListAllChanlets", mock.Anything, "").Return(chanlets, "", nil)
				res, err := server.ListAllChanlets(ctx, req)
				So(err, ShouldBeNil)
				So(res, ShouldResemble, &nioh.ListAllChanletsResponse{
					Chanlets: models.ChanletsToProtos(ctx, chanlets),
					Cursor:   "",
				})
			})
		})

		Convey("Test BulkIsHiddenChanlet", func() {
			req := &nioh.BulkIsHiddenChanletRequest{
				ChannelIds: []string{channelID},
			}

			Convey("with result chanlet", func() {
				mockClients.backend.On("GetChanletByID", mock.Anything, channelID).Return(chanlet, nil)
				res, err := server.BulkIsHiddenChanlet(ctx, req)
				So(err, ShouldBeNil)
				So(res, ShouldResemble, &nioh.BulkIsHiddenChanletResponse{
					Results: []*nioh.IsHiddenChanletResponse{
						{
							IsHiddenChanlet: true,
							OwnerChannelId:  chanlet.OwnerChannelID,
							ChannelId:       channelID,
						},
					},
				})
			})

			Convey("with self chanlet result", func() {
				chanlet.IsPublicChannel = true
				mockClients.backend.On("GetChanletByID", mock.Anything, channelID).Return(chanlet, nil)
				res, err := server.BulkIsHiddenChanlet(ctx, req)
				So(err, ShouldBeNil)
				So(res, ShouldResemble, &nioh.BulkIsHiddenChanletResponse{
					Results: []*nioh.IsHiddenChanletResponse{
						{
							ChannelId: channelID,
						},
					},
				})
			})

			Convey("with nil channelIDs", func() {
				req.ChannelIds = nil
				res, err := server.BulkIsHiddenChanlet(ctx, req)
				So(err, ShouldNotBeNil)
				So(res, ShouldBeNil)
			})

			Convey("with empty channelIDs", func() {
				req.ChannelIds = []string{}
				res, err := server.BulkIsHiddenChanlet(ctx, req)
				So(err, ShouldNotBeNil)
				So(res, ShouldBeNil)
			})

			Convey("with at least one empty channelID", func() {
				req.ChannelIds = []string{""}
				res, err := server.BulkIsHiddenChanlet(ctx, req)
				So(err, ShouldNotBeNil)
				So(res, ShouldBeNil)
			})

			Convey("with backend failure", func() {
				mockClients.backend.On("GetChanletByID", mock.Anything, channelID).Return(nil, errors.New("test"))
				res, err := server.BulkIsHiddenChanlet(ctx, req)
				So(err, ShouldBeNil)
				So(res, ShouldResemble, &nioh.BulkIsHiddenChanletResponse{
					Results: []*nioh.IsHiddenChanletResponse{
						{
							ChannelId: channelID,
						},
					},
				})
			})
		})

		Convey("Test CreateChanlet", func() {
			req := &nioh.CreateChanletRequest{
				ChannelId: channelID,
			}

			Convey("with success", func() {
				mockClients.backend.On("CreateChanlet", mock.Anything, channelID).Return(chanlet, nil)
				res, err := server.CreateChanlet(ctx, req)
				So(err, ShouldBeNil)
				So(res, ShouldResemble, &nioh.CreateChanletResponse{
					Chanlet: &nioh.Chanlet{
						OwnerChannelId:    channelID,
						ChanletId:         chanletID,
						ContentAttributes: testdata.ProtoContentAttributes(),
					},
				})
			})

			Convey("with missing channelID", func() {
				req.ChannelId = ""
				_, err := server.CreateChanlet(ctx, req)
				So(err, ShouldNotBeNil)
			})

			Convey("with backend failure", func() {
				mockClients.backend.On("CreateChanlet", mock.Anything, channelID).Return(nil, errors.New("test"))
				res, err := server.CreateChanlet(ctx, req)
				So(err, ShouldNotBeNil)
				So(res, ShouldBeNil)
			})
		})

		Convey("Test CreateOwnerChanlet", func() {
			req := &nioh.CreateChanletRequest{
				ChannelId: channelID,
			}

			Convey("with success", func() {
				mockClients.backend.On("CreateOwnerChanlet", mock.Anything, channelID).Return(nil)
				res, err := server.CreateOwnerChanlet(ctx, req)
				So(err, ShouldBeNil)
				So(res, ShouldResemble, &nioh.CreateChanletResponse{
					Chanlet: &nioh.Chanlet{
						OwnerChannelId: channelID,
						ChanletId:      channelID,
					},
				})
			})

			Convey("with missing channelID", func() {
				req.ChannelId = ""
				_, err := server.CreateOwnerChanlet(ctx, req)
				So(err, ShouldNotBeNil)
			})

			Convey("with backend failure", func() {
				mockClients.backend.On("CreateOwnerChanlet", mock.Anything, channelID).Return(errors.New("test"))
				res, err := server.CreateOwnerChanlet(ctx, req)
				So(err, ShouldNotBeNil)
				So(res, ShouldBeNil)
			})
		})

		Convey("Test UpdateChanletContentAttributes", func() {
			attributeIDs := testdata.ContentAttributeIDs()

			req := &nioh.UpdateChanletContentAttributesRequest{
				OwnerChannelId:      channelID,
				ChanletId:           chanletID,
				ContentAttributeIds: attributeIDs,
			}

			output := &backend.UpdateChanletOutput{
				Chanlet: chanlet,
			}

			Convey("with valid request", func() {
				mockClients.backend.On("UpdateChanletContentAttributes", mock.Anything, mock.Anything).Return(output, nil)
				res, err := server.UpdateChanletContentAttributes(context.Background(), req)
				So(err, ShouldBeNil)
				So(res, ShouldNotBeNil)
				So(res.Chanlet.OwnerChannelId, ShouldEqual, chanlet.OwnerChannelID)
				So(res.Chanlet.ChanletId, ShouldEqual, chanlet.ChanletID)
				So(len(res.Chanlet.ContentAttributes), ShouldEqual, len(chanlet.ContentAttributes))
				for i := 0; i < len(res.Chanlet.ContentAttributes); i++ {
					So(res.Chanlet.ContentAttributes[i].Id, ShouldEqual, chanlet.ContentAttributes[i].ID)
				}
			})

			Convey("with empty ownerChannelID", func() {
				req.OwnerChannelId = ""
				_, err := server.UpdateChanletContentAttributes(context.Background(), req)
				So(err, ShouldNotBeNil)
			})

			Convey("with empty chanletID", func() {
				req.ChanletId = ""
				_, err := server.UpdateChanletContentAttributes(context.Background(), req)
				So(err, ShouldNotBeNil)
			})

			Convey("with backend failure", func() {
				mockClients.backend.On("UpdateChanletContentAttributes", mock.Anything, mock.Anything).Return(nil, errors.New("can't update"))
				_, err := server.UpdateChanletContentAttributes(context.Background(), req)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("Test GetChanletStreamKey", func() {
			streamKey := "abcdefgh"
			req := &nioh.GetChanletStreamKeyRequest{
				OwnerChannelId: channelID,
				ChanletId:      chanletID,
			}

			Convey("with success", func() {
				mockClients.backend.On("GetChanletStreamKey", mock.Anything, mock.Anything, mock.Anything).Return(streamKey, nil)
				res, err := server.GetChanletStreamKey(ctx, req)
				So(err, ShouldBeNil)
				So(res, ShouldResemble, &nioh.GetChanletStreamKeyResponse{
					StreamKey: streamKey,
				})
			})

			Convey("with missing ownerChannelID", func() {
				req.OwnerChannelId = ""
				res, err := server.GetChanletStreamKey(ctx, req)
				So(err, ShouldNotBeNil)
				So(res, ShouldBeNil)
			})

			Convey("with missing chanletID", func() {
				req.ChanletId = ""
				res, err := server.GetChanletStreamKey(ctx, req)
				So(err, ShouldNotBeNil)
				So(res, ShouldBeNil)
			})

			Convey("with non-existent chanlet", func() {
				mockClients.backend.On("GetChanletStreamKey", mock.Anything, mock.Anything, mock.Anything).Return("", backend.ErrChanletNotFound)
				res, err := server.GetChanletStreamKey(ctx, req)
				So(err, ShouldBeNil)
				So(res, ShouldResemble, &nioh.GetChanletStreamKeyResponse{
					StreamKey: "",
				})
			})

			Convey("with backend failure", func() {
				mockClients.backend.On("GetChanletStreamKey", mock.Anything, mock.Anything, mock.Anything).Return("", errors.New("test"))
				res, err := server.GetChanletStreamKey(ctx, req)
				So(err, ShouldNotBeNil)
				So(res, ShouldBeNil)
			})
		})

		Convey("Test ArchiveChanlet", func() {
			req := &nioh.ArchiveChanletRequest{
				ChanletId: chanletID,
				UserId:    channelID,
			}
			mockClients.backend.On("GetChanletByID", mock.Anything, mock.Anything).Return(&models.Chanlet{
				OwnerChannelID: channelID,
				ChanletID:      chanletID,
			}, nil)

			Convey("with success", func() {
				mockClients.backend.On("ArchiveChanlet", mock.Anything, mock.Anything).Return(chanlet, nil)
				res, err := server.ArchiveChanlet(ctx, req)
				So(err, ShouldBeNil)
				So(res, ShouldNotBeNil)
			})

			Convey("with non-existent chanlet", func() {
				mockClients.backend.On("ArchiveChanlet", mock.Anything, mock.Anything).Return(nil, backend.ErrChanletNotFound)
				res, err := server.ArchiveChanlet(ctx, req)
				So(err, ShouldNotBeNil)
				So(res, ShouldBeNil)
			})

			Convey("with archived chanlet", func() {
				mockClients.backend.On("ArchiveChanlet", mock.Anything, mock.Anything).Return(nil, backend.ErrChanletAlreadyArchived)
				res, err := server.ArchiveChanlet(ctx, req)
				So(err, ShouldNotBeNil)
				So(res, ShouldBeNil)
			})

			Convey("with backend error", func() {
				mockClients.backend.On("ArchiveChanlet", mock.Anything, mock.Anything).Return(nil, errors.New("not disclosed"))
				res, err := server.ArchiveChanlet(ctx, req)
				So(err, ShouldNotBeNil)
				So(res, ShouldBeNil)
			})

			Convey("with user id not matching owner id", func() {
				req.UserId = "101010101010"
				res, err := server.ArchiveChanlet(ctx, req)
				So(err, ShouldNotBeNil)
				So(res, ShouldBeNil)
			})
		})
	})
}
