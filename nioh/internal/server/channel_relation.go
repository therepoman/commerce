package server

import (
	"golang.org/x/net/context"

	"code.justin.tv/commerce/logrus"

	"code.justin.tv/commerce/nioh/internal/models"
	"code.justin.tv/commerce/nioh/rpc/nioh"
	"code.justin.tv/common/chitin"
	"code.justin.tv/common/twirp"
)

// CreateChannelRelation creates a channel relation.
func (s *Server) CreateChannelRelation(ctx context.Context, req *nioh.CreateChannelRelationRequest) (*nioh.CreateChannelRelationResponse, error) {
	log := logrus.WithField("RequestID", chitin.GetTraceID(ctx))

	if req.ChannelId == "" {
		return nil, twirp.RequiredArgumentError("channel_id")
	}

	if req.RelatedChannelId == "" {
		return nil, twirp.RequiredArgumentError("related_channel_id")
	}

	relation, err := s.backend.CreateChannelRelation(ctx, req.ChannelId, req.RelatedChannelId, models.RelationBorrower)

	if err != nil {
		log.WithError(err).Error("CreateChannelRelation encountered error while creating a channel relation row")
		return nil, twirp.InternalError("Failed to create a channel relation")
	}

	return &nioh.CreateChannelRelationResponse{
		ChannelRelation: models.ChannelRelationToProto(relation),
	}, nil
}

// DeleteChannelRelation deletes a channel relation.
func (s *Server) DeleteChannelRelation(ctx context.Context, req *nioh.DeleteChannelRelationRequest) (*nioh.DeleteChannelRelationResponse, error) {
	log := logrus.WithField("RequestID", chitin.GetTraceID(ctx))
	if req.ChannelId == "" {
		return nil, twirp.RequiredArgumentError("channel_id")
	}

	if req.RelatedChannelId == "" {
		return nil, twirp.RequiredArgumentError("related_channel_id")
	}

	err := s.backend.DeleteChannelRelation(ctx, req.ChannelId, req.RelatedChannelId)

	if err != nil {
		log.WithError(err).Error("DeleteChannelRelation encountered error while deleting a channel relation row")
		return nil, twirp.InternalError("Failed to delete a channel relation")
	}

	return &nioh.DeleteChannelRelationResponse{
		ChannelRelation: &nioh.ChannelRelation{
			ChannelId:        req.ChannelId,
			RelatedChannelId: req.RelatedChannelId,
		},
	}, nil
}

// GetChannelRelations Gets channel relations based on either channel id or related channel id.
func (s *Server) GetChannelRelations(ctx context.Context, req *nioh.GetChannelRelationsRequest) (*nioh.GetChannelRelationsResponse, error) {
	log := logrus.WithField("RequestID", chitin.GetTraceID(ctx))
	if req.GetChannelId() == "" && req.GetRelatedChannelId() == "" {
		return nil, twirp.RequiredArgumentError("channel_id or related_channel_id")
	}

	var relations []*models.ChannelRelation
	var err error

	switch req.Id.(type) {
	case *nioh.GetChannelRelationsRequest_ChannelId:
		relations, err = s.backend.GetChannelRelationsByChannelID(ctx, req.GetChannelId())
	case *nioh.GetChannelRelationsRequest_RelatedChannelId:
		relations, err = s.backend.GetChannelRelationsByRelatedChannelID(ctx, req.GetRelatedChannelId())
	default:
		return nil, twirp.InvalidArgumentError("id", "Unrecognized object sent as id")
	}

	if err != nil {
		log.WithError(err).Error("GetChannelRelations encountered error while getting channel relation rows")
		return nil, twirp.InternalError("Failed to get channel relations")
	}

	return &nioh.GetChannelRelationsResponse{
		ChannelRelations: models.ChannelRelationsToProtos(relations),
	}, nil
}
