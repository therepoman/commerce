package server

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/nioh/config"
	"code.justin.tv/commerce/nioh/internal/backend"
	"code.justin.tv/commerce/nioh/internal/models"
	"code.justin.tv/commerce/nioh/internal/testdata"
	"code.justin.tv/commerce/nioh/rpc/nioh"
	. "github.com/smartystreets/goconvey/convey"
	. "github.com/stretchr/testify/mock"
)

const (
	CreateContentAttributes = "CreateContentAttributes"
	DeleteContentAttributes = "DeleteContentAttributes"
	UpdateContentAttributes = "UpdateContentAttributes"

	GetContentAttributesForChannel          = "GetContentAttributesForChannel"
	CreateContentAttributeImageUploadConfig = "CreateContentAttributeImageUploadConfig"

	ChannelID = "123456789"
)

func TestContentAttributesAPIs(t *testing.T) {

	Convey("Test APIs", t, func() {
		server, mocks := initTestableServer(&config.Configuration{})

		contentAttributes := testdata.ContentAttributes()
		contentAttributesSuccess := contentAttributes[0:3]
		contentAttributesFail := contentAttributes[3:5]

		protoContentAttributes := models.ContentAttributesToProtos(context.Background(), testdata.ContentAttributes())
		protoContentAttributesSuccess := protoContentAttributes[0:3]
		protoContentAttributesFail := protoContentAttributes[3:5]

		Convey("CreateContentAttributes", func() {
			Convey("with invalid request", func() {
				var req *nioh.CreateContentAttributesRequest
				Convey("no channel ID", func() {
					req = &nioh.CreateContentAttributesRequest{
						ChannelId:         "",
						ContentAttributes: []*nioh.ContentAttribute{},
					}
				})

				Convey("no content attribute", func() {
					req = &nioh.CreateContentAttributesRequest{
						ChannelId:         ChannelID,
						ContentAttributes: []*nioh.ContentAttribute{},
					}
				})

				_, err := server.CreateContentAttributes(context.Background(), req)
				So(err, ShouldNotBeNil)
				mocks.backend.AssertNotCalled(t, CreateContentAttributes)
			})

			Convey("with valid request", func() {
				req := &nioh.CreateContentAttributesRequest{
					ChannelId:         ChannelID,
					ContentAttributes: protoContentAttributes,
				}

				results := &backend.ContentAttributesBulkResults{
					Success: contentAttributesSuccess,
					Fail:    contentAttributesFail,
				}

				mocks.backend.On(CreateContentAttributes, Anything, Anything).Return(results, nil)

				resp, err := server.CreateContentAttributes(context.Background(), req)
				So(err, ShouldBeNil)
				So(resp.SucceededCreates, ShouldResemble, protoContentAttributesSuccess)
				So(resp.FailedCreates, ShouldResemble, protoContentAttributesFail)
				mocks.backend.AssertCalled(
					t,
					CreateContentAttributes,
					context.Background(),
					&backend.ContentAttributesBulkParams{
						ChannelID:         req.ChannelId,
						ContentAttributes: contentAttributes,
					})
			})

			Convey("with backend error", func() {
				req := &nioh.CreateContentAttributesRequest{
					ChannelId:         ChannelID,
					ContentAttributes: protoContentAttributes,
				}

				mocks.backend.On(CreateContentAttributes, Anything, Anything).Return(
					nil, errors.New("failed to create new content attributes"))

				_, err := server.CreateContentAttributes(context.Background(), req)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("DeleteContentAttributes", func() {
			Convey("with invalid request", func() {
				var req *nioh.DeleteContentAttributesRequest
				Convey("no channel ID", func() {
					req = &nioh.DeleteContentAttributesRequest{
						ChannelId:         "",
						ContentAttributes: protoContentAttributes,
					}
				})

				Convey("no content attribute", func() {
					req = &nioh.DeleteContentAttributesRequest{
						ChannelId:         ChannelID,
						ContentAttributes: []*nioh.ContentAttribute{},
					}
				})
				_, err := server.DeleteContentAttributes(context.Background(), req)
				So(err, ShouldNotBeNil)
				mocks.backend.AssertNotCalled(t, DeleteContentAttributes)
			})

			Convey("with valid request", func() {
				req := &nioh.DeleteContentAttributesRequest{
					ChannelId:         ChannelID,
					ContentAttributes: protoContentAttributes[0:2],
				}

				results := &backend.ContentAttributesBulkResults{
					Success: contentAttributesSuccess,
					Fail:    contentAttributesFail,
				}

				mocks.backend.On(DeleteContentAttributes, Anything, Anything).Return(results, nil)

				resp, err := server.DeleteContentAttributes(context.Background(), req)
				So(err, ShouldBeNil)
				So(resp.SucceededDeletes, ShouldResemble, protoContentAttributesSuccess)
				So(resp.FailedDeletes, ShouldResemble, protoContentAttributesFail)
				mocks.backend.AssertNumberOfCalls(t, DeleteContentAttributes, 1)
				mocks.backend.AssertCalled(
					t,
					DeleteContentAttributes,
					context.Background(),
					&backend.ContentAttributesBulkParams{
						ChannelID:         req.ChannelId,
						ContentAttributes: contentAttributes[0:2],
					})
			})

			Convey("with backend error", func() {
				req := &nioh.DeleteContentAttributesRequest{
					ChannelId:         ChannelID,
					ContentAttributes: protoContentAttributes[0:2],
				}

				mocks.backend.On(DeleteContentAttributes, Anything, Anything).Return(
					nil, errors.New("failed to delete content attributes"))

				_, err := server.DeleteContentAttributes(context.Background(), req)
				So(err, ShouldNotBeNil)
			})

		})

		Convey("GetContentAttributesForChannel", func() {
			Convey("with invalid request", func() {
				req := &nioh.GetContentAttributesForChannelRequest{
					ChannelId: "",
				}

				_, err := server.GetContentAttributesForChannel(context.Background(), req)
				So(err, ShouldNotBeNil)
			})

			Convey("with valid request", func() {
				req := &nioh.GetContentAttributesForChannelRequest{
					ChannelId: ChannelID,
				}

				mocks.backend.On(GetContentAttributesForChannel, Anything, Anything).Return(
					contentAttributes, nil)

				resp, err := server.GetContentAttributesForChannel(context.Background(), req)
				So(err, ShouldBeNil)
				So(resp.ContentAttributes, ShouldResemble, protoContentAttributes)
				mocks.backend.AssertCalled(
					t,
					GetContentAttributesForChannel,
					context.Background(),
					req.ChannelId)
			})

			Convey("with backend error", func() {
				req := &nioh.GetContentAttributesForChannelRequest{
					ChannelId: ChannelID,
				}

				mocks.backend.On(GetContentAttributesForChannel, Anything, Anything).Return(
					nil, errors.New("Failed to retrieve content attributes for channel"))

				_, err := server.GetContentAttributesForChannel(context.Background(), req)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("UpdateContentAttributes", func() {
			Convey("with invalid request", func() {
				var req *nioh.UpdateContentAttributesRequest
				Convey("empty channel ID", func() {
					req = &nioh.UpdateContentAttributesRequest{
						ChannelId:         "",
						ContentAttributes: []*nioh.ContentAttribute{},
					}
				})

				Convey("no content attributes", func() {
					req = &nioh.UpdateContentAttributesRequest{
						ChannelId:         ChannelID,
						ContentAttributes: []*nioh.ContentAttribute{},
					}
				})

				_, err := server.UpdateContentAttributes(context.Background(), req)
				So(err, ShouldNotBeNil)
				mocks.backend.AssertNotCalled(t, UpdateContentAttributes)
			})

			Convey("with valid request", func() {
				req := &nioh.UpdateContentAttributesRequest{
					ChannelId:         ChannelID,
					ContentAttributes: protoContentAttributes,
				}

				results := &backend.ContentAttributesBulkResults{
					Success: contentAttributesSuccess,
					Fail:    contentAttributesFail,
				}

				mocks.backend.On(UpdateContentAttributes, Anything, Anything).Return(results, nil)

				resp, err := server.UpdateContentAttributes(context.Background(), req)
				So(err, ShouldBeNil)
				So(resp.SucceededUpdates, ShouldResemble, protoContentAttributesSuccess)
				So(resp.FailedUpdates, ShouldResemble, protoContentAttributesFail)
				mocks.backend.AssertCalled(
					t,
					UpdateContentAttributes,
					context.Background(),
					&backend.ContentAttributesBulkParams{
						ChannelID:         req.ChannelId,
						ContentAttributes: contentAttributes,
					})
			})

			Convey("with backend error", func() {
				req := &nioh.UpdateContentAttributesRequest{
					ChannelId:         ChannelID,
					ContentAttributes: protoContentAttributes,
				}

				mocks.backend.On(UpdateContentAttributes, Anything, Anything).Return(
					nil, errors.New("failed to update content attributes"))

				_, err := server.UpdateContentAttributes(context.Background(), req)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("CreateContentAttributeImageUploadConfig", func() {
			req := &nioh.CreateContentAttributeImageUploadConfigRequest{
				ChannelId: ChannelID,
			}

			config := &models.ContentAttributeImageUploadConfig{
				ChannelID: ChannelID,
				ImageURL:  "http://image.url",
				UploadURL: "http//upload.url",
				UploadID:  "12345",
			}

			Convey("with invalid request", func() {
				req.ChannelId = ""
				_, err := server.CreateContentAttributeImageUploadConfig(context.Background(), req)
				So(err, ShouldNotBeNil)
			})

			Convey("with valid request", func() {
				mocks.backend.On(CreateContentAttributeImageUploadConfig, Anything, Anything).Return(config, nil)

				resp, err := server.CreateContentAttributeImageUploadConfig(context.Background(), req)
				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
				So(resp.ChannelId, ShouldEqual, config.ChannelID)
				So(resp.UploadUrl, ShouldEqual, config.UploadURL)
				So(resp.UploadId, ShouldEqual, config.UploadID)
				So(resp.ImageUrl, ShouldEqual, config.ImageURL)
			})

			Convey("with backend failure", func() {
				mocks.backend.On(CreateContentAttributeImageUploadConfig, Anything, Anything).Return(nil, errors.New("backend failure"))

				_, err := server.CreateContentAttributeImageUploadConfig(context.Background(), req)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("GetContentAttributesForChanlet", func() {
			req := &nioh.GetContentAttributesForChanletRequest{
				ChanletId: ChannelID,
			}

			Convey("with invalid request", func() {
				req.ChanletId = ""
				_, err := server.GetContentAttributesForChanlet(context.Background(), req)
				So(err, ShouldNotBeNil)
			})

			Convey("with valid request", func() {
				mocks.backend.On("GetContentAttributesForChanlet", Anything, ChannelID).Return(testdata.ContentAttributes(), nil)
				result, err := server.GetContentAttributesForChanlet(context.Background(), req)
				So(err, ShouldBeNil)
				So(result, ShouldResemble, &nioh.GetContentAttributesForChanletResponse{
					ChanletId:         ChannelID,
					ContentAttributes: models.ContentAttributesToProtos(context.Background(), testdata.ContentAttributes()),
				})
			})

			Convey("with backend fail", func() {
				mocks.backend.On("GetContentAttributesForChanlet", Anything, ChannelID).Return(nil, errors.New("backend asplode"))
				_, err := server.GetContentAttributesForChanlet(context.Background(), req)
				So(err, ShouldNotBeNil)
			})
		})
	})
}
