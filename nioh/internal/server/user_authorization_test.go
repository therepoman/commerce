package server

import (
	"context"
	"testing"

	"github.com/pkg/errors"

	"code.justin.tv/commerce/nioh/config"
	"code.justin.tv/commerce/nioh/internal/backend"
	"github.com/twitchtv/twirp"

	backend_mocks "code.justin.tv/commerce/nioh/internal/backend/mocks"
	"code.justin.tv/commerce/nioh/internal/models"
	"code.justin.tv/commerce/nioh/internal/testdata"
	"code.justin.tv/commerce/nioh/rpc/nioh"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func generateVODAuthorization(req *nioh.GetUserAuthorizationRequest, userIsAuthorized, resourceIsRestricted bool) (*models.VODAuthorization, error) {
	r, err := models.VODAuthorizationRequestFromProto(req)
	return &models.VODAuthorization{
		Request: r,
		ResourceAuthorization: models.ResourceAuthorization{
			UserIsAuthorized:     userIsAuthorized,
			ResourceIsRestricted: resourceIsRestricted,
		},
	}, err
}

func generateChannelAuthorization(req *nioh.GetUserAuthorizationRequest, exemptions []*models.Exemption, userIsAuthorized, resourceIsRestricted bool) *models.ChannelAuthorization {
	return &models.ChannelAuthorization{
		Request: &models.ChannelAuthorizationRequest{
			UserID:         req.UserId,
			ChannelID:      req.GetChannel().Id,
			ConsumePreview: req.ConsumePreview,
			Origin:         req.Origin,
		},
		ResourceAuthorization: models.ResourceAuthorization{
			UserIsAuthorized:     userIsAuthorized,
			ResourceIsRestricted: resourceIsRestricted,
			AppliedExemptions:    testdata.ApplyExemptions(exemptions),
		},
	}
}

func TestGetUserAuthorization(t *testing.T) {
	Convey("GetUserAuthorization for VOD resource", t, func() {
		testUserID := "123454321"
		testVOD := &nioh.VOD{
			ChannelId:        "cool-channel-id",
			VideoId:          "great-video-id",
			HedwigReasonData: "{\"productName\":\"walrusfest2019\"}",
		}

		testCases := []struct {
			description          string
			configureMockBackend func(*backend_mocks.Backend, *nioh.GetUserAuthorizationRequest)
			request              *nioh.GetUserAuthorizationRequest
			want                 *nioh.GetUserAuthorizationResponse
			wantErr              bool
			expectedErrType      error
		}{
			{
				description: "requesting VOD resource for which user is authorized",
				configureMockBackend: func(b *backend_mocks.Backend, req *nioh.GetUserAuthorizationRequest) {
					auth, err := generateVODAuthorization(req, true, true)
					b.On("GetUserVODAuthorization", mock.Anything, mock.Anything).Return(auth, err)
				},
				request: &nioh.GetUserAuthorizationRequest{
					UserId: testUserID,
					Resource: &nioh.GetUserAuthorizationRequest_Vod{
						Vod: testVOD,
					},
				},
				want: &nioh.GetUserAuthorizationResponse{
					UserId:               testUserID,
					UserIsAuthorized:     true,
					ResourceIsRestricted: true,
					Resource: &nioh.GetUserAuthorizationResponse_Vod{
						Vod: testVOD,
					},
				},
				wantErr: false,
			},
			{
				description: "requesting VOD resource for which user is unauthorized",
				configureMockBackend: func(b *backend_mocks.Backend, req *nioh.GetUserAuthorizationRequest) {
					auth, err := generateVODAuthorization(req, false, true)
					b.On("GetUserVODAuthorization", mock.Anything, mock.Anything).Return(auth, err)
				},
				request: &nioh.GetUserAuthorizationRequest{
					UserId: testUserID,
					Resource: &nioh.GetUserAuthorizationRequest_Vod{
						Vod: testVOD,
					},
				},
				want: &nioh.GetUserAuthorizationResponse{
					UserId:               testUserID,
					UserIsAuthorized:     false,
					ResourceIsRestricted: true,
					Resource: &nioh.GetUserAuthorizationResponse_Vod{
						Vod: testVOD,
					},
				},
				wantErr: false,
			},
			{
				description: "when product with shortname is not found",
				configureMockBackend: func(b *backend_mocks.Backend, req *nioh.GetUserAuthorizationRequest) {
					b.On("GetUserVODAuthorization", mock.Anything, mock.Anything).
						Return(nil, backend.NewProductNotFoundError("F"))
				},
				request: &nioh.GetUserAuthorizationRequest{
					UserId: testUserID,
					Resource: &nioh.GetUserAuthorizationRequest_Vod{
						Vod: testVOD,
					},
				},
				want:            nil,
				wantErr:         true,
				expectedErrType: twirp.NotFoundError(""),
			},
			{
				description: "when backend returns an unknown error",
				configureMockBackend: func(b *backend_mocks.Backend, req *nioh.GetUserAuthorizationRequest) {
					b.On("GetUserVODAuthorization", mock.Anything, mock.Anything).
						Return(nil, errors.New("F"))
				},
				request: &nioh.GetUserAuthorizationRequest{
					UserId: testUserID,
					Resource: &nioh.GetUserAuthorizationRequest_Vod{
						Vod: testVOD,
					},
				},
				want: &nioh.GetUserAuthorizationResponse{
					UserId:               testUserID,
					UserIsAuthorized:     true,
					ResourceIsRestricted: false,
					Resource: &nioh.GetUserAuthorizationResponse_Vod{
						Vod: testVOD,
					},
				},
				wantErr:         true,
				expectedErrType: twirp.InternalError(""),
			},
			{
				description: "when HedwigReasonData is empty",
				configureMockBackend: func(b *backend_mocks.Backend, req *nioh.GetUserAuthorizationRequest) {
					auth, err := generateVODAuthorization(req, false, true)
					b.On("GetUserVODAuthorization", mock.Anything, mock.Anything).Return(auth, err)
				},
				request: &nioh.GetUserAuthorizationRequest{
					UserId: testUserID,
					Resource: &nioh.GetUserAuthorizationRequest_Vod{
						Vod: testVOD,
					},
				},
				want: &nioh.GetUserAuthorizationResponse{
					UserId:               testUserID,
					UserIsAuthorized:     false,
					ResourceIsRestricted: true,
					Resource: &nioh.GetUserAuthorizationResponse_Vod{
						Vod: testVOD,
					},
				},
				wantErr:         false,
				expectedErrType: twirp.InvalidArgumentError("", ""),
			},
			{
				description:          "when HedwigReasonData is malformed",
				configureMockBackend: func(b *backend_mocks.Backend, req *nioh.GetUserAuthorizationRequest) {},
				request: &nioh.GetUserAuthorizationRequest{
					UserId: testUserID,
					Resource: &nioh.GetUserAuthorizationRequest_Vod{
						Vod: &nioh.VOD{
							ChannelId:        "123",
							VideoId:          "321",
							HedwigReasonData: "{fasdf}",
						},
					},
				},
				want:            nil,
				wantErr:         true,
				expectedErrType: twirp.InvalidArgumentError("", ""),
			},
			{
				description:          "when HedwigReasonData has empty ProductName",
				configureMockBackend: func(b *backend_mocks.Backend, req *nioh.GetUserAuthorizationRequest) {},
				request: &nioh.GetUserAuthorizationRequest{
					UserId: testUserID,
					Resource: &nioh.GetUserAuthorizationRequest_Vod{
						Vod: &nioh.VOD{
							ChannelId:        "123",
							VideoId:          "321",
							HedwigReasonData: "{\"productName\": \"\"}",
						},
					},
				},
				want:            nil,
				wantErr:         true,
				expectedErrType: twirp.RequiredArgumentError(""),
			},
			{
				description: "when anonymous user tries to access restricted VOD",
				configureMockBackend: func(b *backend_mocks.Backend, req *nioh.GetUserAuthorizationRequest) {
					auth, err := generateVODAuthorization(req, false, true)
					b.On("GetUserVODAuthorization", mock.Anything, mock.Anything).Return(auth, err)
				},
				request: &nioh.GetUserAuthorizationRequest{
					Resource: &nioh.GetUserAuthorizationRequest_Vod{
						Vod: testVOD,
					},
				},
				want: &nioh.GetUserAuthorizationResponse{
					UserIsAuthorized:     false,
					ResourceIsRestricted: true,
					Resource: &nioh.GetUserAuthorizationResponse_Vod{
						Vod: testVOD,
					},
				},
			},
		}

		for _, tt := range testCases {
			Convey(tt.description, func() {
				server, serverMocks := initTestableServer(&config.Configuration{})
				tt.configureMockBackend(serverMocks.backend, tt.request)
				got, err := server.GetUserAuthorization(context.Background(), tt.request)
				So(got, ShouldResemble, tt.want)

				gotErr := err != nil

				So(gotErr, ShouldResemble, tt.wantErr)

				if gotErr {
					So(err, ShouldHaveSameTypeAs, tt.expectedErrType)
				}

				// If we wanted and got a Twirp error, assert they have the same Code
				gotTwirpErr, gotOk := err.(twirp.Error)
				wantTwirpErr, wantOk := tt.expectedErrType.(twirp.Error)

				if gotOk && wantOk {
					So(gotTwirpErr.Code(), ShouldEqual, wantTwirpErr.Code())
				}
			})
		}
	})

	Convey("GetUserAuthorization for Channel resource", t, func() {
		server, serverMocks := initTestableServer(&config.Configuration{})
		testUserID := "12345533"
		testChannelID := "35533"
		testChannel := &nioh.Channel{Id: testChannelID}
		req := &nioh.GetUserAuthorizationRequest{
			UserId: testUserID,
			Resource: &nioh.GetUserAuthorizationRequest_Channel{
				Channel: testChannel,
			},
		}
		expected := &nioh.GetUserAuthorizationResponse{
			UserId:               testUserID,
			UserIsAuthorized:     true,
			ResourceIsRestricted: false,
			Resource: &nioh.GetUserAuthorizationResponse_Channel{
				Channel: testChannel,
			},
			AppliedExemptions: []*nioh.Exemption{},
		}

		testBackendRequest := &models.ChannelAuthorizationRequest{
			UserID:    testUserID,
			ChannelID: testChannelID,
		}

		Convey("with anonymous user", func() {
			req.UserId = ""
			expected.UserId = ""
			testBackendRequest.UserID = ""
			testUserAuthorization := generateChannelAuthorization(req, []*models.Exemption{}, false, false)

			m := serverMocks.backend.On("GetUserChannelAuthorization", mock.Anything, testBackendRequest)

			Convey("with no restriction", func() {
				testUserAuthorization.UserIsAuthorized = true
				m.Return(testUserAuthorization, nil)
			})

			Convey("with restriction and no access", func() {
				testUserAuthorization.ResourceIsRestricted = true
				m.Return(testUserAuthorization, nil)

				expected.UserIsAuthorized = false
				expected.ResourceIsRestricted = true
			})

			res, err := server.GetUserAuthorization(context.Background(), req)
			So(err, ShouldBeNil)
			So(res, ShouldResemble, expected)
		})

		Convey("with logged in user", func() {
			testUserAuthorization := generateChannelAuthorization(req, []*models.Exemption{}, false, false)

			Convey("with no restriction", func() {
				testUserAuthorization.UserIsAuthorized = true
				serverMocks.backend.On("GetUserChannelAuthorization", mock.Anything, testBackendRequest).Return(testUserAuthorization, nil)
				res, err := server.GetUserAuthorization(context.Background(), req)
				So(res, ShouldResemble, expected)
				So(err, ShouldBeNil)
			})

			Convey("with restriction", func() {
				testUserAuthorization.ResourceIsRestricted = true
				expected.ResourceIsRestricted = true

				Convey("and no user access", func() {
					serverMocks.backend.On("GetUserChannelAuthorization", mock.Anything, testBackendRequest).Return(testUserAuthorization, nil)
					expected.UserIsAuthorized = false

					res, err := server.GetUserAuthorization(context.Background(), req)
					So(res, ShouldResemble, expected)
					So(err, ShouldBeNil)
				})

				Convey("and user access", func() {
					testUserAuthorization.UserIsAuthorized = true
					serverMocks.backend.On("GetUserChannelAuthorization", mock.Anything, testBackendRequest).Return(testUserAuthorization, nil)
					res, err := server.GetUserAuthorization(context.Background(), req)
					So(res, ShouldResemble, expected)
					So(err, ShouldBeNil)
				})
			})
		})

		Convey("with a missing channel argument", func() {
			req.Resource = &nioh.GetUserAuthorizationRequest_Channel{}
			res, err := server.GetUserAuthorization(context.Background(), req)
			So(res, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("with a missing channel id argument", func() {
			testChannel.Id = ""
			res, err := server.GetUserAuthorization(context.Background(), req)
			So(res, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("with backend error", func() {
			serverMocks.backend.On("GetUserChannelAuthorization", mock.Anything, testBackendRequest).Return(nil, errors.New("your backend asplode"))
			res, err := server.GetUserAuthorization(context.Background(), req)
			expected.AppliedExemptions = nil
			So(res, ShouldResemble, expected)
			So(err, ShouldNotBeNil)
		})
	})

	Convey("GetUserAuthorizationsByResources", t, func() {
		server, serverMocks := initTestableServer(&config.Configuration{})
		ctx := context.Background()
		testUserID := "123"
		testChannelID1 := "555"
		testChannelID2 := "666"
		testChannel1 := &nioh.Channel{Id: testChannelID1}
		testChannel2 := &nioh.Channel{Id: testChannelID2}
		testVideoID1 := "777"
		testVideoID2 := "888"
		testVideo1 := &nioh.VOD{VideoId: testVideoID1}
		testVideo2 := &nioh.VOD{VideoId: testVideoID2}

		req := &nioh.GetUserAuthorizationsByResourcesRequest{
			UserId:   testUserID,
			Channels: []*nioh.Channel{{Id: testChannelID1}, {Id: testChannelID2}},
		}

		Convey("returns error", func() {
			Convey("with backend error", func() {
				serverMocks.backend.On("GetUserChannelAuthorizations", mock.Anything, mock.Anything).Return(nil, errors.New("err"))
			})

			res, err := server.GetUserAuthorizationsByResources(ctx, req)
			So(res, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("returns results without error", func() {
			channelAuths := []*models.ChannelAuthorization{
				generateChannelAuthorization(
					&nioh.GetUserAuthorizationRequest{UserId: testUserID, Resource: &nioh.GetUserAuthorizationRequest_Channel{Channel: testChannel1}},
					testdata.ExemptionsStaffProduct(), true, true,
				),
				generateChannelAuthorization(
					&nioh.GetUserAuthorizationRequest{UserId: testUserID, Resource: &nioh.GetUserAuthorizationRequest_Channel{Channel: testChannel2}},
					nil, true, false,
				),
			}

			vodAuth1, err := generateVODAuthorization(
				&nioh.GetUserAuthorizationRequest{UserId: testUserID, Resource: &nioh.GetUserAuthorizationRequest_Vod{Vod: testVideo1}},
				true, true,
			)
			So(err, ShouldBeNil)

			vodAuth2, err := generateVODAuthorization(
				&nioh.GetUserAuthorizationRequest{UserId: testUserID, Resource: &nioh.GetUserAuthorizationRequest_Vod{Vod: testVideo2}},
				true, false,
			)
			So(err, ShouldBeNil)

			vodAuths := []*models.VODAuthorization{vodAuth1, vodAuth2}

			Convey("without user id", func() {
				req.UserId = ""
				testUserID = ""
				channelAuths[0].UserIsAuthorized = false // anonymous user cannot be authorized with actual restriction
			})

			Convey("with user id", func() {})

			serverMocks.backend.On("GetUserChannelAuthorizations", mock.Anything, mock.Anything).Return(channelAuths, nil)
			serverMocks.backend.On("GetUserVODAuthorizations", mock.Anything, mock.Anything).Return(vodAuths, nil)

			res, err := server.GetUserAuthorizationsByResources(ctx, req)
			So(res, ShouldNotBeNil)
			So(err, ShouldBeNil)
			channelProto, err := models.ChannelAuthorizationsToUserAuthorizationForResourceProto(channelAuths)
			So(err, ShouldBeNil)
			authProto, err := models.VODAuthorizationsToUserAuthorizationsForResourcesProto(vodAuths)
			So(err, ShouldBeNil)
			So(res.UserId, ShouldEqual, testUserID)
			So(len(res.ChannelAuthorizations), ShouldEqual, len(channelProto))
			for i := 0; i < len(res.ChannelAuthorizations); i++ {
				So(res.ChannelAuthorizations[i].ResourceIsRestricted, ShouldEqual, channelProto[i].ResourceIsRestricted)
			}
			So(len(res.VodAuthorizations), ShouldEqual, len(authProto))
			for i := 0; i < len(res.VodAuthorizations); i++ {
				So(res.VodAuthorizations[i].ResourceIsRestricted, ShouldEqual, authProto[i].ResourceIsRestricted)
			}
		})
	})

	Convey("ListAuthorizedUsersV2", t, func() {
		server, serverMocks := initTestableServer(&config.Configuration{})
		ctx := context.Background()

		authorizedIDs := []string{"123", "456"}

		Convey("with missing resource argument", func() {
			req := &nioh.ListAuthorizedUsersRequestV2{}
			res, err := server.ListAuthorizedUsersV2(ctx, req)
			So(res, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("with missing channel id argument", func() {
			req := &nioh.ListAuthorizedUsersRequestV2{
				Resource: &nioh.ListAuthorizedUsersRequestV2_Channel{
					Channel: &nioh.Channel{},
				},
			}
			res, err := server.ListAuthorizedUsersV2(ctx, req)
			So(res, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("with a valid request", func() {
			testChannelID := "35533"
			testChannel := &nioh.Channel{Id: testChannelID}
			req := &nioh.ListAuthorizedUsersRequestV2{
				Resource: &nioh.ListAuthorizedUsersRequestV2_Channel{
					Channel: testChannel,
				},
				Limit:  20,
				Cursor: "",
			}
			nextCursor := ""
			nextRealCursor := "qwerty"

			Convey("with error on GetAuthorizedUsersForChannelV2", func() {
				serverMocks.backend.On("GetAuthorizedUsersForChannelV2", mock.Anything, testChannelID, mock.Anything, mock.Anything).Return([]string{}, false, nextCursor, errors.New("nah"))
				res, err := server.ListAuthorizedUsersV2(ctx, req)
				So(res, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})

			Convey("with result from GetAuthorizedUsersForChannelV2", func() {
				serverMocks.backend.On("GetAuthorizedUsersForChannelV2", mock.Anything, testChannelID, mock.Anything, mock.Anything).Return(authorizedIDs, true, nextCursor, nil)
				res, err := server.ListAuthorizedUsersV2(ctx, req)
				So(res, ShouldNotBeNil)
				So(err, ShouldBeNil)
				So(res.UserIds, ShouldResemble, authorizedIDs)
				So(res.ResourceIsRestricted, ShouldBeTrue)
				So(res.Resource, ShouldResemble, &nioh.ListAuthorizedUsersResponseV2_Channel{
					Channel: testChannel,
				})
			})

			Convey("with result from GetAuthorizedUsersForChannelV2 that includes a cursor", func() {
				serverMocks.backend.On("GetAuthorizedUsersForChannelV2", mock.Anything, testChannelID, mock.Anything, mock.Anything).Return(authorizedIDs, true, nextRealCursor, nil)
				res, err := server.ListAuthorizedUsersV2(ctx, req)
				So(res, ShouldNotBeNil)
				So(err, ShouldBeNil)
				So(res.UserIds, ShouldResemble, authorizedIDs)
				So(res.ResourceIsRestricted, ShouldBeTrue)
				So(res.Resource, ShouldResemble, &nioh.ListAuthorizedUsersResponseV2_Channel{
					Channel: testChannel,
				})
				So(res.NextCursor, ShouldResemble, "qwerty")
			})
		})
	})
}
