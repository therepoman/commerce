package server

import (
	"context"
	"testing"

	"code.justin.tv/commerce/nioh/config"
	"code.justin.tv/commerce/nioh/rpc/nioh"
	. "github.com/smartystreets/goconvey/convey"
)

func TestPermissionsAPI(t *testing.T) {
	Convey("Test Permissions API", t, func() {
		server, mockClients := initTestableServer(&config.Configuration{})
		channelID := "123"
		userID := "123"
		ctx := context.Background()

		Convey("Test GetPermissions", func() {
			req := &nioh.GetPermissionsRequest{
				ChannelId: channelID,
				UserId:    userID,
			}

			Convey("with missing arg", func() {
				Convey("user_id", func() {
					req.UserId = ""
				})

				Convey("channel_id", func() {
					req.ChannelId = ""
				})

				res, err := server.GetPermissions(ctx, req)
				So(err, ShouldNotBeNil)
				So(res, ShouldBeNil)
			})

			Convey("with good request", func() {
				expected := &nioh.GetPermissionsResponse{
					ChannelId: channelID,
					UserId:    userID,
				}

				Convey("with user who does not own channel", func() {
					mockClients.dynamicConfig.On("GetAllAccessPassModeratorsForChannel", req.ChannelId).Return(nil, nil)
					req.UserId = "12444"
					expected.UserId = req.UserId
					expected.Permissions = []nioh.GetPermissionsResponse_PermissionScope{}
				})

				Convey("with no allowlist", func() {
					mockClients.dynamicConfig.On("GetAllAccessPassModeratorsForChannel", req.ChannelId).Return(nil, nil)
					mockClients.dynamicConfig.On("GetUserGates").Return(map[string]config.UserGateFlags{}, nil)
					expected.Permissions = []nioh.GetPermissionsResponse_PermissionScope{}
				})

				Convey("with allowlist", func() {
					mockClients.dynamicConfig.On("GetAllAccessPassModeratorsForChannel", req.ChannelId).Return(nil, nil)
					mockClients.dynamicConfig.On("GetUserGates").Return(map[string]config.UserGateFlags{
						string(multiviewAdminGateKey): {
							Enabled:          true,
							StaffEnabled:     false,
							AllowlistEnabled: true,
							AllowlistMap: map[string]bool{
								userID: true,
							},
						},
					}, nil)
					expected.Permissions = []nioh.GetPermissionsResponse_PermissionScope{
						nioh.GetPermissionsResponse_MULTIVIEW_ADMIN,
					}
				})

				Convey("with user who is an AAP moderator ", func() {
					req.UserId = "12444"
					mockClients.dynamicConfig.On("GetAllAccessPassModeratorsForChannel", req.ChannelId).Return(map[string]bool{req.UserId: true}, nil)
					mockClients.dynamicConfig.On("GetUserGates").Return(map[string]config.UserGateFlags{
						string(setResourceRestrictionGateKey): {
							Enabled: true,
						},
					}, nil)
					expected.UserId = req.UserId
					expected.Permissions = []nioh.GetPermissionsResponse_PermissionScope{
						nioh.GetPermissionsResponse_SET_CHANNEL_RESTRICTION,
					}
				})

				res, err := server.GetPermissions(ctx, req)
				So(err, ShouldBeNil)
				So(res, ShouldResemble, expected)
			})
		})
	})
}
