package server

import (
	"context"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/nioh/config"
	"code.justin.tv/commerce/nioh/rpc/nioh"
	"code.justin.tv/common/chitin"
	"code.justin.tv/common/twirp"
)

// permissibleScopeProtoMap is used as the list of feature permissions to be checked.
// It is defined as a map for easy conversion to proto representation.
// Make sure permissibleScopes has all keys in this map.
var permissibleScopeProtoMap = map[userGateKey]nioh.GetPermissionsResponse_PermissionScope{
	multiviewAdminGateKey:         nioh.GetPermissionsResponse_MULTIVIEW_ADMIN,
	setResourceRestrictionGateKey: nioh.GetPermissionsResponse_SET_CHANNEL_RESTRICTION,
}
var permissibleScopes = []userGateKey{
	multiviewAdminGateKey,
	setResourceRestrictionGateKey,
}

// GetPermissions is used to get feature scoped permissions for a user on a given channel.
func (s *Server) GetPermissions(ctx context.Context, req *nioh.GetPermissionsRequest) (*nioh.GetPermissionsResponse, error) {
	log := logrus.WithField("RequestID", chitin.GetTraceID(ctx))
	log.WithField("request", *req).Debug("Handling GetPermissions")

	if req.ChannelId == "" {
		return nil, twirp.RequiredArgumentError("channel_id")
	}

	if req.UserId == "" {
		return nil, twirp.RequiredArgumentError("user_id")
	}

	permissions := []nioh.GetPermissionsResponse_PermissionScope{}
	for _, permissibleScope := range permissibleScopes {
		if proto, ok := permissibleScopeProtoMap[permissibleScope]; ok {
			if s.isScopePermittedForUserOnChannel(ctx, permissibleScope, req.UserId, req.ChannelId) {
				permissions = append(permissions, proto)
			}
		}
	}

	return &nioh.GetPermissionsResponse{
		ChannelId:   req.ChannelId,
		UserId:      req.UserId,
		Permissions: permissions,
	}, nil
}

// isScopePermittedForUserOnChannel returns whether the user is permitted to the feature scope on the given channel
func (s *Server) isScopePermittedForUserOnChannel(ctx context.Context, gateName userGateKey, userID, channelID string) bool {
	// Nioh currently does not permit anyone to edit anyone else's resources.
	// The only exception is for AllAccessPassModerators.
	if userID != channelID {
		userIDMap, err := s.dynamicConfig.GetAllAccessPassModeratorsForChannel(channelID)
		if !userIDMap[userID] {
			if err != nil {
				log := logrus.WithField("RequestID", chitin.GetTraceID(ctx))
				log.WithError(err).Error("getting all access pass moderators map")
			}
			return false
		}
	}

	accessInfo := s.BuildAccessInfo(ctx, gateName, userID)
	return accessInfo.CanAccess
}

// CheckAllScopesPresent checks if the given user gate map has all the keys mapping from defined permissible scopes
func CheckAllScopesPresent(usergates map[string]config.UserGateFlags) bool {
	for _, p := range permissibleScopes {
		if _, ok := usergates[string(p)]; !ok {
			return false
		}
	}
	return true
}
