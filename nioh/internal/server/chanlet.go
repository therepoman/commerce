package server

import (
	"context"
	"fmt"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/nioh/internal/backend"
	"code.justin.tv/commerce/nioh/internal/models"
	"code.justin.tv/commerce/nioh/pkg/helpers"
	"code.justin.tv/commerce/nioh/rpc/nioh"
	"code.justin.tv/common/chitin"
	"code.justin.tv/common/twirp"
)

const (
	getChanletsCacheControlFmt = "public, max-age=%d"

	emptyChannelCacheDuration = 3600 // Cache duration to be inserted for Cache-Control header for empty GetChanlets response (in seconds)
)

// GetChanlets is used to get chanlets.
func (s *Server) GetChanlets(ctx context.Context, req *nioh.GetChanletsRequest) (*nioh.GetChanletsResponse, error) {
	log := logrus.WithField("RequestID", chitin.GetTraceID(ctx))

	if req.ChannelId == "" {
		return nil, twirp.RequiredArgumentError("channel_id")
	}

	ctx = helpers.ContextWithChannelID(ctx, req.ChannelId)

	if s.config.BlockGetChanletsFlag {
		log.Warn("Returning empty response from GetChanlets API due to blocking flag")
		return &nioh.GetChanletsResponse{}, nil
	}

	chanlets, err := s.backend.GetChanlets(ctx, req.ChannelId, backend.GetChanletsOptions{
		Sort: backend.ChanletSort(req.Sort),
	})
	if err != nil {
		log.WithError(err).Error("GetChanlets encountered error while getting chanlets via backend")
		return nil, twirp.InternalError("Failed to get chanlets")
	}

	if len(chanlets) == 0 {
		// Make upstream cache this response
		setResponseHeader(ctx, cacheControlKey, fmt.Sprintf(getChanletsCacheControlFmt, emptyChannelCacheDuration))
	}

	if !s.shouldShowChanlets(ctx, chanlets, req.UserId, req.BypassDisableFlag) {
		chanlets = []*models.Chanlet{}
	}

	return &nioh.GetChanletsResponse{
		Chanlets: models.ChanletsToProtos(ctx, chanlets),
	}, nil
}

// shouldShowChanlets returns true if the owner enabled the chanlets or the owner is looking at their own chanlets
func (s *Server) shouldShowChanlets(ctx context.Context, chanlets []*models.Chanlet, requesterUserID string, bypass bool) bool {
	var ownerChanlet *models.Chanlet
	for _, chanlet := range chanlets {
		if chanlet.IsOwnerChanlet() {
			ownerChanlet = chanlet
			break
		}
	}

	if ownerChanlet != nil {
		ownerBypass := requesterUserID == ownerChanlet.ChanletID && bypass
		return ownerBypass || (ownerChanlet.OwnerChanletAttributes != nil && ownerChanlet.OwnerChanletAttributes.ChanletsEnabled)
	}

	return false
}

// ListAllChanlets returns a list of all chanlets paginated. This is intended to be service-to-service only.
func (s *Server) ListAllChanlets(ctx context.Context, req *nioh.ListAllChanletsRequest) (*nioh.ListAllChanletsResponse, error) {
	log := logrus.WithField("RequestID", chitin.GetTraceID(ctx))

	chanlets, nextCursor, err := s.backend.ListAllChanlets(ctx, req.Cursor)

	if err != nil {
		log.WithError(err).Error("ListAllChanlets encountered error while retrieving chanlets")
		return nil, twirp.InternalError("failed to retrieve chanlets")
	}

	return &nioh.ListAllChanletsResponse{
		Chanlets: models.ChanletsToProtos(ctx, chanlets),
		Cursor:   nextCursor,
	}, nil
}

// CreateChanlet is used to create a chanlet on a given channel.
func (s *Server) CreateChanlet(ctx context.Context, req *nioh.CreateChanletRequest) (*nioh.CreateChanletResponse, error) {
	log := logrus.WithField("RequestID", chitin.GetTraceID(ctx))

	if req.ChannelId == "" {
		return nil, twirp.RequiredArgumentError("channel_id")
	}

	// Simple API gating
	ctx = s.setContextWithAccessControl(ctx, multiviewAdminGateKey, req.ChannelId)
	if !helpers.AccessAllowed(ctx) {
		return nil, twirp.InvalidArgumentError(req.ChannelId, "channel_id is not authorized")
	}

	chanlet, err := s.backend.CreateChanlet(ctx, req.ChannelId)
	if err != nil {
		log.WithError(err).Error("CreateChanlet encountered error while creating a chanlet via backend")
		return nil, twirp.InternalError("Failed to create chanlet")
	}

	return &nioh.CreateChanletResponse{
		Chanlet: models.ChanletToProto(ctx, chanlet),
	}, nil
}

// CreateOwnerChanlet is used to create an owner chanlet on a given channel.
func (s *Server) CreateOwnerChanlet(ctx context.Context, req *nioh.CreateChanletRequest) (*nioh.CreateChanletResponse, error) {
	log := logrus.WithField("RequestID", chitin.GetTraceID(ctx))

	if req.ChannelId == "" {
		return nil, twirp.RequiredArgumentError("channel_id")
	}

	// Simple API gating
	ctx = s.setContextWithAccessControl(ctx, multiviewAdminGateKey, req.ChannelId)
	if !helpers.AccessAllowed(ctx) {
		return nil, twirp.InvalidArgumentError(req.ChannelId, "channel_id is not authorized")
	}

	err := s.backend.CreateOwnerChanlet(ctx, req.ChannelId)
	if err != nil {
		log.WithError(err).Error("CreateOwnerChanlet encountered error while creating a chanlet via backend")
		return nil, twirp.InternalError("Failed to create owner chanlet")
	}

	return &nioh.CreateChanletResponse{
		Chanlet: &nioh.Chanlet{
			OwnerChannelId: req.ChannelId,
			ChanletId:      req.ChannelId,
		},
	}, nil
}

// UpdateChanletContentAttributes updates the list of content attributes a chanlet is tagged with
func (s *Server) UpdateChanletContentAttributes(ctx context.Context, req *nioh.UpdateChanletContentAttributesRequest) (*nioh.UpdateChanletContentAttributesResponse, error) {
	log := logrus.WithField("RequestID", chitin.GetTraceID(ctx))

	if req.OwnerChannelId == "" {
		return nil, twirp.RequiredArgumentError("owner_channel_id")
	}
	if req.ChanletId == "" {
		return nil, twirp.RequiredArgumentError("chanlet_id")
	}

	output, err := s.backend.UpdateChanletContentAttributes(
		ctx,
		&backend.UpdateChanletInput{
			OwnerChannelID:      req.OwnerChannelId,
			ChanletID:           req.ChanletId,
			ContentAttributeIDs: req.ContentAttributeIds,
		})

	if err != nil {
		log.WithError(err).Error("UpdateChanletContentAttributes encountered error while updating a chanlet's content attributes via backend")
		return nil, twirp.InternalError("Failed to update content attribute IDs for chanlet")
	}

	return &nioh.UpdateChanletContentAttributesResponse{
		Chanlet: models.ChanletToProto(ctx, output.Chanlet),
	}, nil
}

// GetChanletStreamKey gets the stream key for a specific chanlet
func (s *Server) GetChanletStreamKey(ctx context.Context, req *nioh.GetChanletStreamKeyRequest) (*nioh.GetChanletStreamKeyResponse, error) {
	log := logrus.WithField("RequestID", chitin.GetTraceID(ctx))

	if req.OwnerChannelId == "" {
		return nil, twirp.RequiredArgumentError("owner_channel_id")
	} else if req.ChanletId == "" {
		return nil, twirp.RequiredArgumentError("chanlet_id")
	}

	// Simple API gating
	ctx = s.setContextWithAccessControl(ctx, multiviewAdminGateKey, req.OwnerChannelId)
	if !helpers.AccessAllowed(ctx) {
		return nil, twirp.InvalidArgumentError(req.OwnerChannelId, "owner_channel_id is not authorized")
	}

	streamKey, err := s.backend.GetChanletStreamKey(ctx, req.OwnerChannelId, req.ChanletId)
	if err == backend.ErrChanletNotFound {
		log.WithFields(logrus.Fields{
			"chanletID":      req.ChanletId,
			"ownerChanletID": req.OwnerChannelId,
		}).Info("GetChanletStreamKey API received non-existent chanlet input")
		return &nioh.GetChanletStreamKeyResponse{
			StreamKey: "",
		}, nil
	} else if err != nil {
		log.WithError(err).Error("GetChanletStreamKey encountered an error while getting chanlet stream key via backend")
		return nil, twirp.InternalError("Failed to get chanlet stream key")
	}

	return &nioh.GetChanletStreamKeyResponse{
		StreamKey: streamKey,
	}, nil
}

// IsHiddenChanlet returns whether a channel is a chanlet with a parent channel
func (s *Server) IsHiddenChanlet(ctx context.Context, req *nioh.IsHiddenChanletRequest) (*nioh.IsHiddenChanletResponse, error) {
	log := logrus.WithField("RequestID", chitin.GetTraceID(ctx))

	if req.ChannelId == "" {
		return nil, twirp.RequiredArgumentError("channel_id")
	}

	chanlet, err := s.backend.GetChanletByID(ctx, req.ChannelId)
	res := &nioh.IsHiddenChanletResponse{
		ChannelId: req.ChannelId,
	}

	if err != nil {
		log.WithError(err).Error("IsHiddenChanlet failed to get chanlet from backend. Returning default response - false")
		return res, nil
	}

	if chanlet != nil && !chanlet.IsPublicChannel {
		res.IsHiddenChanlet = true
		res.OwnerChannelId = chanlet.OwnerChannelID
	}

	return res, nil
}

// BulkIsHiddenChanlet returns whether the request channels are hidden chanlets (bulk version of IsHiddenChanlet)
func (s *Server) BulkIsHiddenChanlet(ctx context.Context, req *nioh.BulkIsHiddenChanletRequest) (*nioh.BulkIsHiddenChanletResponse, error) {
	log := logrus.WithField("RequestID", chitin.GetTraceID(ctx))

	if req.ChannelIds == nil {
		return nil, twirp.RequiredArgumentError("channel_ids")
	} else if len(req.ChannelIds) <= 0 {
		return nil, twirp.InvalidArgumentError("channel_ids", "must have at least one non-empty value")
	}

	res := &nioh.BulkIsHiddenChanletResponse{}

	for _, channelID := range req.ChannelIds {
		if channelID == "" {
			return nil, twirp.InvalidArgumentError("channel_ids", "cannot have empty values")
		}

		chanlet, err := s.backend.GetChanletByID(ctx, channelID)
		currentRes := &nioh.IsHiddenChanletResponse{
			ChannelId: channelID,
		}

		if err != nil {
			log.WithError(err).Errorf("BulkIsHiddenChanlet failed to get chanlet from backend. Returning default response for channel %s - false", channelID)
			res.Results = append(res.Results, currentRes)
			continue
		}

		if chanlet != nil && !chanlet.IsPublicChannel {
			currentRes.IsHiddenChanlet = true
			currentRes.OwnerChannelId = chanlet.OwnerChannelID
		}

		res.Results = append(res.Results, currentRes)
	}

	return res, nil
}

// ArchiveChanlet archives a chanlet
func (s *Server) ArchiveChanlet(ctx context.Context, req *nioh.ArchiveChanletRequest) (*nioh.ArchiveChanletResponse, error) {
	log := logrus.WithField("RequestID", chitin.GetTraceID(ctx))

	if req.ChanletId == "" {
		return nil, twirp.RequiredArgumentError("chanlet_id")
	} else if req.UserId == "" {
		return nil, twirp.RequiredArgumentError("user_id")
	} else if !s.isUserChannelOwner(ctx, req.UserId, req.ChanletId) {
		return nil, twirp.InvalidArgumentError("user_id", "this user cannot archive this chanlet")
	}

	chanlet, err := s.backend.ArchiveChanlet(ctx, req.ChanletId)

	if err == backend.ErrChanletNotFound {
		return nil, twirp.NotFoundError("no such chanlet")
	} else if err == backend.ErrChanletAlreadyArchived {
		return nil, twirp.InvalidArgumentError("chanlet_id", "this chanlet is already archived")
	} else if err != nil {
		log.WithError(err).Errorf("ArchiveChanlet failed to archive a chanlet - %s", req.ChanletId)
		return nil, twirp.InternalError("Failed to archive a chanlet")
	}

	return &nioh.ArchiveChanletResponse{
		Chanlet: models.ChanletToProto(ctx, chanlet),
	}, nil
}
