package server

import (
	"context"
	"testing"

	"code.justin.tv/commerce/nioh/config"
	"code.justin.tv/commerce/nioh/internal/models"
	"code.justin.tv/commerce/nioh/rpc/nioh"

	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"

	"github.com/pkg/errors"
)

func TestChannelRelationAPI(t *testing.T) {
	Convey("Test API", t, func() {
		server, mockClients := initTestableServer(&config.Configuration{})
		channelID := "123"
		relatedChannelID := "456"
		relation := &models.ChannelRelation{
			ChannelID:        channelID,
			RelatedChannelID: relatedChannelID,
			Relation:         models.RelationBorrower,
		}

		Convey("Test CreateChannelRelation", func() {
			req := &nioh.CreateChannelRelationRequest{
				ChannelId:        channelID,
				RelatedChannelId: relatedChannelID,
			}

			Convey("with missing channelID", func() {
				req.ChannelId = ""
				res, err := server.CreateChannelRelation(context.Background(), req)
				So(err, ShouldNotBeNil)
				So(res, ShouldBeNil)
			})

			Convey("with missing relatedChannelID", func() {
				req.RelatedChannelId = ""
				res, err := server.CreateChannelRelation(context.Background(), req)
				So(err, ShouldNotBeNil)
				So(res, ShouldBeNil)
			})

			Convey("with success", func() {
				mockClients.backend.On("CreateChannelRelation", mock.Anything, channelID, relatedChannelID, models.RelationBorrower).Return(relation, nil)
				res, err := server.CreateChannelRelation(context.Background(), req)
				expectedResponse := &nioh.CreateChannelRelationResponse{
					ChannelRelation: models.ChannelRelationToProto(relation),
				}
				So(err, ShouldBeNil)
				So(res, ShouldResemble, expectedResponse)
			})

			Convey("with backend error", func() {
				mockClients.backend.On("CreateChannelRelation", mock.Anything, channelID, relatedChannelID, models.RelationBorrower).Return(nil, errors.New("test"))
				res, err := server.CreateChannelRelation(context.Background(), req)
				So(err, ShouldNotBeNil)
				So(res, ShouldBeNil)
			})
		})

		Convey("Test DeleteChannelRelation", func() {
			req := &nioh.DeleteChannelRelationRequest{
				ChannelId:        channelID,
				RelatedChannelId: relatedChannelID,
			}

			Convey("with missing channelID", func() {
				req.ChannelId = ""
				res, err := server.DeleteChannelRelation(context.Background(), req)
				So(err, ShouldNotBeNil)
				So(res, ShouldBeNil)
			})

			Convey("with missing relatedChannelID", func() {
				req.RelatedChannelId = ""
				res, err := server.DeleteChannelRelation(context.Background(), req)
				So(err, ShouldNotBeNil)
				So(res, ShouldBeNil)
			})

			Convey("with success", func() {
				mockClients.backend.On("DeleteChannelRelation", mock.Anything, channelID, relatedChannelID).Return(nil)
				res, err := server.DeleteChannelRelation(context.Background(), req)
				relation.Relation = ""
				expectedResponse := &nioh.DeleteChannelRelationResponse{
					ChannelRelation: models.ChannelRelationToProto(relation),
				}
				So(err, ShouldBeNil)
				So(res, ShouldResemble, expectedResponse)
			})

			Convey("with backend error", func() {
				mockClients.backend.On("DeleteChannelRelation", mock.Anything, channelID, relatedChannelID).Return(errors.New("test"))
				res, err := server.DeleteChannelRelation(context.Background(), req)
				So(err, ShouldNotBeNil)
				So(res, ShouldBeNil)
			})
		})

		Convey("GetChannelRelations", func() {
			req := &nioh.GetChannelRelationsRequest{}

			Convey("with missing id", func() {
				res, err := server.GetChannelRelations(context.Background(), req)
				So(err, ShouldNotBeNil)
				So(res, ShouldBeNil)
			})

			Convey("with valid", func() {
				Convey("channel id", func() {
					mockClients.backend.On("GetChannelRelationsByChannelID", mock.Anything, channelID).Return([]*models.ChannelRelation{relation}, nil)
					req.Id = &nioh.GetChannelRelationsRequest_ChannelId{ChannelId: channelID}
				})

				Convey("related_channel id", func() {
					mockClients.backend.On("GetChannelRelationsByRelatedChannelID", mock.Anything, relatedChannelID).Return([]*models.ChannelRelation{relation}, nil)
					req.Id = &nioh.GetChannelRelationsRequest_RelatedChannelId{RelatedChannelId: relatedChannelID}
				})

				res, err := server.GetChannelRelations(context.Background(), req)
				expectedResponse := &nioh.GetChannelRelationsResponse{
					ChannelRelations: models.ChannelRelationsToProtos([]*models.ChannelRelation{relation}),
				}
				So(err, ShouldBeNil)
				So(res, ShouldResemble, expectedResponse)
			})

			Convey("backend error", func() {
				mockClients.backend.On("GetChannelRelationsByChannelID", mock.Anything, channelID).Return(nil, errors.New("test"))
				req.Id = &nioh.GetChannelRelationsRequest_ChannelId{ChannelId: channelID}
				res, err := server.GetChannelRelations(context.Background(), req)
				So(err, ShouldNotBeNil)
				So(res, ShouldBeNil)
			})
		})
	})
}
