package server

import (
	"code.justin.tv/commerce/logrus"
	"github.com/twitchtv/twirp"

	"code.justin.tv/common/chitin"

	"golang.org/x/net/context"

	"code.justin.tv/commerce/nioh/rpc/nioh"
)

// GetRecord is used to get a record.
func (s *Server) GetRecord(ctx context.Context, r *nioh.GetRecordRequest) (*nioh.Record, error) {
	log := logrus.WithField("RequestID", chitin.GetTraceID(ctx))

	if r.ChannelId == "" {
		return nil, twirp.RequiredArgumentError("ChannelId")
	}
	if r.RecordId == "" {
		return nil, twirp.RequiredArgumentError("RecordId")
	}

	record, err := s.backend.GetRecord(ctx, r)
	if err != nil {
		return nil, twirp.InternalError("Error fetching data from table")
	}

	log.Info("GetRecord")
	return record, nil
}
