package server

import (
	"context"
	"fmt"
	"net/http"
	"os"

	"code.justin.tv/commerce/nioh/pkg/clientid"

	"code.justin.tv/commerce/nioh/config"
	"code.justin.tv/commerce/nioh/internal/backend"
	"code.justin.tv/commerce/nioh/internal/clients"
	internalErrors "code.justin.tv/commerce/nioh/internal/errors"
	"code.justin.tv/commerce/nioh/pkg/dynamodb/provider"
	"code.justin.tv/commerce/nioh/pkg/errorlogger"
	"code.justin.tv/commerce/nioh/pkg/helpers"
	"code.justin.tv/commerce/nioh/pkg/middleware"
	"code.justin.tv/commerce/nioh/rpc/nioh"
	"code.justin.tv/common/chitin"
	"code.justin.tv/sse/malachai/pkg/events"
	"code.justin.tv/sse/malachai/pkg/s2s/callee"

	log "code.justin.tv/commerce/logrus"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/pkg/errors"
	"github.com/twitchtv/twirp"
	statsd_hook "github.com/twitchtv/twirp/hooks/statsd"
	"goji.io"
	"goji.io/pat"
)

var (
	// BuildTime is set to the current date when the app is built.
	BuildTime = "not provided"
	// Version is set to the current git SHA when the app is built.
	Version = "not provided"
)

const (
	defaultServerPort = ":8000"
	defaultDebugPort  = ":6000"

	statsDependencyErrorFmt = "dependency.pepper.%s.error"

	cacheControlKey = "Cache-Control"
)

var (
	serverPort = defaultServerPort
	debugPort  = defaultDebugPort
)

func init() {
	if port, ok := os.LookupEnv("BIND_ADDR"); ok {
		serverPort = port
	}
	if port, ok := os.LookupEnv("DEBUG_ADDR"); ok {
		debugPort = port
	}
}

type contextKey string

const (
	contextResponseWriterKey contextKey = "__ctx_response_writer"
)

// userGateKey is a string key used to access the user gate configuration in dynamic config
type userGateKey string

const (
	// WARNING: When values below are updated, please update the keys in dynamic config as well
	// Because dynamic configs are uploaded at deploy time, we have to have both old and new keys while deployment is pending.

	// multiviewAdminGateKey is a user gate key to access chanlet mutation user gate config.
	// MultiviewAdmin permission allows you to make changes to chanlets on your channel
	multiviewAdminGateKey userGateKey = "MultiviewAdmin"

	// setResourceRestrictionGateKey is a user gate key to access restriction mutation user gate config
	// SetResourceRestriction permission allows you to make changes to restrictions on your channel
	setResourceRestrictionGateKey userGateKey = "SetResourceRestriction"

	// setResourceRestrictionAdminGateKey is a user gate key for setResourceRestriction.
	// The difference is, userIDs in the allowlist for this key can bypass the requirement ownerID == userID
	setResourceRestrictionAdminGateKey userGateKey = "SetResourceRestrictionAdmin"
)

// Server exposes HTTP endpoints.
type Server struct {
	HTTP          *http.Server
	backend       backend.Backend
	clients       *clients.Clients
	config        *config.Configuration
	dynamicConfig config.IDynamicConfiguration
	errorlogger   errorlogger.ErrorLogger
	stats         statsd.Statter
}

type ServerConfig struct {
	Backend       backend.Backend
	Clients       *clients.Clients
	Config        *config.Configuration
	DynamicConfig config.IDynamicConfiguration
	Errorlogger   errorlogger.ErrorLogger
	Stats         statsd.Statter
	S2SName       string
	UseDAX        bool
}

// NewServer allocates and returns a new Server.
func NewServer(serverConfig ServerConfig) (*Server, error) {
	mux := goji.NewMux()

	// We are bypassing s2s for tests because the runner has to assume certain role in order for s2s to work
	if serverConfig.S2SName != "" {
		calleeClient, err := setupS2SCallee(serverConfig.S2SName, serverConfig.Config.AWSRegion)
		if err != nil {
			return nil, errors.Wrap(err, "an error occurred while setting up S2S callee")
		}
		err = calleeClient.Start()
		if err != nil {
			return nil, errors.Wrap(err, "an error occurred while statring S2S callee client")
		}
	}

	handlerMetrics := middleware.NewHandlerMetrics(serverConfig.Stats)
	handlerLoaders := middleware.NewHandlerLoaders(serverConfig.Clients)
	mux.Use(middleware.PanicLogger)
	mux.Use(handlerMetrics.Metrics)
	mux.Use(handlerLoaders.Attach)
	mux.Use(middleware.ContextTimeout)
	mux.Use(provider.DynamoProviderMiddleware(serverConfig.UseDAX))
	mux.Use(clientid.AttacherMiddleware)

	if err := chitin.ExperimentalTraceProcessOptIn(); err != nil {
		serverConfig.Errorlogger.LogFatalWithExtras("Failed to set trace", "service.errors.chitin", map[string]interface{}{
			"error": err.Error(),
		})
	}

	handler := chitin.Handler(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		r = r.WithContext(context.WithValue(chitin.Context(w, r), contextResponseWriterKey, w))
		mux.ServeHTTP(w, r)
	}))

	server := &http.Server{
		Addr:    serverPort,
		Handler: handler,
	}

	s := &Server{
		server,
		serverConfig.Backend,
		serverConfig.Clients,
		serverConfig.Config,
		serverConfig.DynamicConfig,
		serverConfig.Errorlogger,
		serverConfig.Stats,
	}

	// Validate passed dynamic config
	if config.GetCanonicalEnv() != config.DevelopmentEnv {
		usergates, err := s.dynamicConfig.GetUserGates()
		if err != nil {
			return nil, err
		}
		if !CheckAllScopesPresent(usergates) {
			return nil, errors.New("PermissibleScopes in server not matching dynamic config")
		}
	}

	// Statsd hooks
	var basicHook *twirp.ServerHooks
	if serverConfig.Stats != nil {
		basicHook = statsd_hook.NewStatsdServerHooks(serverConfig.Stats)
	}
	hook := twirp.ChainHooks(basicHook)

	// Register Twirp RPC endpoints.
	twirpHandler := nioh.NewAPIServer(s, hook)
	mux.HandleFunc(pat.Get("/"), s.everythingIsFine)
	mux.HandleFunc(pat.Get("/health"), s.everythingIsFine)
	mux.Handle(pat.Post(nioh.APIPathPrefix+"*"), twirpHandler)

	// Debug APIs
	twirpDebugHandler := nioh.NewDebugAPIServer(s, hook)
	mux.Handle(pat.Post(nioh.DebugAPIPathPrefix+"*"), twirpDebugHandler)
	mux.HandleFunc(pat.Get("/debug/TestConnections"), s.TestConnections)

	return s, nil
}

// ListenAndServe starts the server on its default port
func (s *Server) ListenAndServe() error {
	go func() {
		log.Infof("debug listening on %s", debugPort)
		log.Info(http.ListenAndServe(debugPort, nil))
	}()

	log.Infof("application listening on %s", serverPort)
	return s.HTTP.ListenAndServe()
}

// Shutdown stops the server from running
func (s *Server) Shutdown(ctx context.Context) error {
	return s.HTTP.Shutdown(ctx)
}

// A helper that sets the context with User information and access controls
func (s *Server) setContextWithAccessControl(ctx context.Context, gateName userGateKey, userID string) context.Context {
	ctx = helpers.ContextWithUser(ctx, userID)
	accessInfo := s.BuildAccessInfo(ctx, gateName, userID)

	return helpers.ContextWithAccessInfo(ctx, accessInfo)
}

// A helper that determines if the given user is owns the given channel
func (s *Server) isUserChannelOwner(ctx context.Context, userID, channelID string) bool {
	if userID == channelID {
		return true
	}
	chanlet, err := s.backend.GetChanletByID(ctx, channelID)
	if err != nil {
		if err != backend.ErrChanletNotFound {
			log.WithField("channelID", channelID).WithError(err).Error("Could not retrieve chanlet for given channelID")
		}
		return false
	}
	if chanlet == nil {
		return false
	}

	return chanlet.OwnerChannelID == userID
}

// A helper that determines if the given user is a mod for the given channel. Hardcoded list for now.
func (s *Server) isUserChannelAAPMod(ctx context.Context, userID, channelID string) bool {
	if userID == channelID {
		return true
	}
	moderators, err := s.dynamicConfig.GetAllAccessPassModeratorsForChannel(channelID)
	if err != nil {
		log.WithField("channelID", channelID).WithError(err).Error("Could not get moderators list for given channelID")
		return false
	}
	if mod, ok := moderators[userID]; ok {
		return mod
	}

	return false
}

// A helper that determines if the given user is a mod or owner of the given channel
func (s *Server) isUserChannelOwnerOrAAPMod(ctx context.Context, userID, channelID string) bool {
	return s.isUserChannelOwner(ctx, userID, channelID) || s.isUserChannelAAPMod(ctx, userID, channelID)
}

// A helper to determine if the given channel is streaming based on JAX
func (s *Server) isChannelLive(ctx context.Context, channelID string) (bool, error) {
	// Jax is unreliable to use right now. Bypass this check for now.
	// JIRA: https://jira.twitch.com/browse/REWARD-2385
	return false, nil
}

func (s *Server) getStatusCode(err error) int {
	// nioh wrapped errors.
	if err, ok := err.(internalErrors.WithStatusCode); ok {
		return err.StatusCode()
	}

	return http.StatusInternalServerError
}

func (s *Server) getUserGates() map[string]config.UserGateFlags {
	// Attempt to get from dynamic config
	usergates, err := s.dynamicConfig.GetUserGates()
	if err != nil {
		log.WithError(err).Error("Could not get UserGates from dynamic config")
	}

	return usergates
}

func (s *Server) everythingIsFine(w http.ResponseWriter, r *http.Request) {
	_, err := w.Write([]byte("OK"))
	if err != nil {
		log.Fatal(err)
	}
}

func (s *Server) BuildAccessInfo(ctx context.Context, gateName userGateKey, userID string) helpers.AccessInfo {
	userGate, ok := s.getUserGates()[string(gateName)]
	if !ok {
		log.WithField("gateName", string(gateName)).Warn("No gating info found in the dynamic config for the gate name")
		return helpers.AccessInfo{}
	}

	if !userGate.Enabled {
		return helpers.AccessInfo{CanAccess: true}
	}

	// Check overrides
	override, ok := userGate.OverrideMap[userID]
	if ok {
		return helpers.AccessInfo{CanAccess: override}
	}

	// Staff gating bypasses other gating (if you are staff, you don't need to be anything else to have access)
	if userGate.StaffEnabled {
		isStaff, err := s.clients.UsersClient.IsUserStaff(ctx, userID)
		if err != nil {
			log.WithError(err).Error("failed to check staff status. skipping staff check in allowlist")
		} else if isStaff {
			return helpers.AccessInfo{CanAccess: true, IsStaff: true}
		}
	}

	// Test for each gating
	accessInfo := helpers.AccessInfo{}
	accessInfo.IsAllowlisted = userGate.AllowlistMap[userID]

	// No access if allowlist is enabled but the user is not allowlisted
	if userGate.AllowlistEnabled && !accessInfo.IsAllowlisted {
		return accessInfo
	}

	// No access if low risk gating is enabled, and the user is not low risk
	if userGate.LowRiskGatingEnabled {
		isLowRisk, err := s.clients.Pepper.IsUserLowRisk(ctx, userID)
		if !isLowRisk {
			if err != nil {
				s.statsInc(fmt.Sprintf(statsDependencyErrorFmt, "GetUserRiskEvaluation"), 1)
				log.WithError(err).WithField("userID", userID).Error("failed to get low risk evaluation from Pepper")
			}
			return accessInfo // Fail closed
		}
	}

	accessInfo.CanAccess = true
	return accessInfo
}

func (s *Server) statsInc(name string, amount int64) {
	go func() {
		err := s.stats.Inc(name, amount, 1.0)
		if err != nil {
			log.WithError(err).Debug("failed to send stats from server")
		}
	}()
}

func setResponseHeader(ctx context.Context, key string, value string) {
	responseWriter, ok := ctx.Value(contextResponseWriterKey).(http.ResponseWriter)
	if ok {
		responseWriter.Header().Set(key, value)
	}
}

func setupS2SCallee(serviceName, region string) (*callee.Client, error) {
	logger := log.StandardLogger() // pass the global logger instance used across the code base

	if config.IsLocalEnv() {
		// Malachi spams pretty hard. For local dev, silence all the debug logs.
		logger = log.New()
		logger.SetLevel(log.InfoLevel)
	}

	eventsWriterClient, err := events.NewEventLogger(events.Config{
		BufferSizeLimit: 0, // 0 is unlimited, adjust as necessary
	}, logger)

	if err != nil {
		return nil, err
	}

	AWSSTSRegionEndpoint := fmt.Sprintf("https://sts.%s.amazonaws.com", region) // region specific endpoint as the global endpoint is deprecated: https://docs.aws.amazon.com/general/latest/gr/rande.html#sts_region

	calleeClient := &callee.Client{
		Logger:             logger,
		ServiceName:        serviceName,
		EventsWriterClient: eventsWriterClient,
		Config: &callee.Config{
			DisableStatsClient: config.GetCanonicalEnv() == config.DevelopmentEnv, // Callee has hardcoded stats url of statsd.internal.justin.tv:8125. Override for development env
			AWSConfigBase: &aws.Config{
				Endpoint: &AWSSTSRegionEndpoint,
				Region:   &region,
			},
		},
	}

	return calleeClient, nil
}
