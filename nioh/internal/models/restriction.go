package models

import (
	"time"

	"code.justin.tv/commerce/nioh/rpc/nioh"
)

// RestrictionV2 is a refinement of the ChannelRestriction type, representing the intent (in RestrictionType)
// and exemption mechanisms (in Exemptions) of a restriction.
type RestrictionV2 struct {
	ResourceKey     string `dynamo:",hash"`
	RestrictionType RestrictionType
	Exemptions      []*Exemption
	Options         []RestrictionOption
	Geoblocks       []Geoblock
}

// RestrictionFromProto converts a proto representation of a restriction to an internal representation of a restriction
func RestrictionFromProto(r *nioh.Restriction) RestrictionV2 {
	var out RestrictionV2

	// Note that we do not read the Exemptions field because
	// the Nioh API does not allow users to pass in Exemptions
	out.RestrictionType = RestrictionType(r.GetRestrictionType().String())
	out.Options = RestrictionOptionsFromProto(r.GetOptions())
	out.Geoblocks = RestrictionGeoblocksFromProto(r.GetGeoblocks()...)

	return out
}

// RestrictionType describes the intent of the restriction: What Twitch.tv feature does the restriction support?
type RestrictionType string

// String returns the RestrictionType converted to a string.
func (r RestrictionType) String() string {
	return string(r)
}

// These consts mirror the variants of the RestrictionType enum in rpc/nioh/models.proto.
const (
	// UnknownRestrictionType represents any unknown or malformed restriction type (mirroring protobuf enum behavior)
	UnknownRestrictionType RestrictionType = "UNKNOWN"
	// SubOnlyLiveRestrictionType represents a restriction relating to the Sub-Only Live site feature
	SubOnlyLiveRestrictionType RestrictionType = "SUB_ONLY_LIVE"
	// AllAccessPassRestrictionType represents a restriction relating to the All-Access Pass site feature
	AllAccessPassRestrictionType RestrictionType = "ALL_ACCESS_PASS"
	// OrganizationAccessOnlyType represents a restriction relating to the organization-member-only feature.
	OrganizationAccessOnlyType RestrictionType = "ORGANIZATION_ACCESS_ONLY"
	// GeoblockRestrictionType respresents a restriction relating to a geoblock
	GeoblockRestrictionType RestrictionType = "GEOBLOCK"
)

const (
	// OWLAllAccessPassChannelID is the exact channel id for the main all-access pass channel
	OWLAllAccessPassChannelID = "137512364"
)

// Exemption is a refinement of the ChannelRestrictionExemptionType and ChannelRestrictionExemptionInfo types.
// It exists subordinate to a restriction and describes a mechanism for gaining exemption from a restriction.
// i.e. a "STAFF" exemption type means that the exemption applies to any staff user, allowing them access.
type Exemption struct {
	ExemptionType   ExemptionType
	ActiveStartDate time.Time
	ActiveEndDate   time.Time
	Keys            []string
	Actions         []*ExemptionAction
}

// AppliedExemption wraps Exemption with application data
type AppliedExemption struct {
	*Exemption
	MatchedKey string
}

// IsActive indicates whether the passed time.Time falls within the range of active start date and active end date
// attached to the exemption.
func (e Exemption) IsActive(now time.Time) bool {
	return (e.ActiveStartDate.Before(now) || e.ActiveStartDate.Equal(now)) &&
		(e.ActiveEndDate.After(now) || e.ActiveEndDate.Equal(now))
}

// ExemptionType describes the mechanism of an exemption: What criterion allows a user to qualify for this exemption
// and thereby bypass its parent restriction?
type ExemptionType string

// String returns the ExemptionType converted to a string.
func (e ExemptionType) String() string {
	return string(e)
}

// These consts mirror the variants of the ExemptionType enum in rpc/nioh/models.proto.
const (
	// UnknownExemptionType represents any unknown or malformed exemption type (mirroring protobuf enum behavior)
	UnknownExemptionType ExemptionType = "UNKNOWN"
	// AllExemptionType represents an exemption that grants access to all users (such as a free trial)
	AllExemptionType ExemptionType = "ALL"
	// StaffExemptionType represents an exemption that grants access to all staff users
	StaffExemptionType ExemptionType = "STAFF"
	// SiteAdminExemptionType represents an exemption that grants access to all site admins
	SiteAdminExemptionType ExemptionType = "SITE_ADMIN"
	// ProductExemptionType represents an exemption that grants access to all users entitled to one or more products
	ProductExemptionType ExemptionType = "PRODUCT"
	// PreviewExemptionType represents an exemption that grants access to users based on their use
	// of the preview window stored in the Previews table
	PreviewExemptionType ExemptionType = "PREVIEW"
	// ChannelVIPExemptionType represents an exemption that grants access to users holding the channel VIP role.
	ChannelVIPExemptionType ExemptionType = "CHANNEL_VIP"
	// ChannelModeratorExemptionType represents an exemption that grants access to users holding the channel moderator role.
	ChannelModeratorExemptionType ExemptionType = "CHANNEL_MODERATOR"
	// OrganizationMemberExemptionType represents an exemption that grants access to users that are in the same organization as the channel owner.
	OrganizationMemberExemptionType ExemptionType = "ORGANIZATION_MEMBER"
	// NotInCountriesExemptionType represents an exemption that grants access to users that are not in the listed countries.
	NotInCountriesExemptionType ExemptionType = "NOT_IN_COUNTRIES"
	// InCountriesExemptionType represents an exemption that grants access to users that are in the listed countries.
	InCountriesExemptionType ExemptionType = "IN_COUNTRIES"
)

// ExemptionAction defines the metadata needed to take action on an exemption
type ExemptionAction struct {
	Name  string
	Title string
}

// RestrictionOption represents a modulation of a restriction's access policy passed by a client at time of creation.
type RestrictionOption string

// These consts mirror the variants of the RestrictionOption enum in rpc/nioh/restriction_option.proto.
const (
	UnknownOption               RestrictionOption = "UNKNOWN"
	AllowChannelVIPOption       RestrictionOption = "ALLOW_CHANNEL_VIP"
	AllowChannelModeratorOption RestrictionOption = "ALLOW_CHANNEL_MODERATOR"
	AllowTier3Only              RestrictionOption = "ALLOW_TIER_3_ONLY"
	AllowTier2And3Only          RestrictionOption = "ALLOW_TIER_2_AND_3_ONLY"
	AllowAllTiers               RestrictionOption = "ALLOW_ALL_TIERS"
)

// ValidFor returns a boolean answering whether the option is valid for the passed restriction type.
func (o RestrictionOption) ValidFor(restrictionType RestrictionType) bool {
	switch o {
	case UnknownOption:
		return false
	case AllowChannelVIPOption, AllowChannelModeratorOption, AllowTier3Only, AllowTier2And3Only, AllowAllTiers:
		return restrictionType == SubOnlyLiveRestrictionType
	default:
		return false
	}
}

// IsTierOption simply returns bool representing whether the option is for product tier.
// Only one such option should be set per restriction.
func (o RestrictionOption) IsTierOption() bool {
	switch o {
	case AllowTier3Only, AllowTier2And3Only, AllowAllTiers:
		return true
	default:
		return false
	}
}

// ToProto converts the RestrictionOption to the corresponding RPC representation.
func (o RestrictionOption) ToProto() nioh.Option {
	return nioh.Option(nioh.Option_value[string(o)])
}

// RestrictionOptionFromProto converts an RPC representation to the corresponding RestrictionOption.
func RestrictionOptionFromProto(proto nioh.Option) RestrictionOption {
	return RestrictionOption(proto.String())
}

// RestrictionOptionsToProto converts a list of RestrictionOption to a list of the corresponding RPC representations.
func RestrictionOptionsToProto(opts []RestrictionOption) []nioh.Option {
	var out []nioh.Option
	for _, o := range opts {
		out = append(out, o.ToProto())
	}
	return out
}

// RestrictionOptionsFromProto converts a list of RPC representations to a list of RestrictionOption.
func RestrictionOptionsFromProto(opts []nioh.Option) []RestrictionOption {
	var out []RestrictionOption
	for _, o := range opts {
		out = append(out, RestrictionOptionFromProto(o))
	}
	return out
}

// Geoblock describes how to geoblock content on Twitch
type Geoblock struct {
	Operator     Operator
	CountryCodes []string
}

// Operator represents whether content should be allowed or blocked
type Operator string

// String returns the Operator converted to a string
func (o Operator) String() string {
	return string(o)
}

// These consts mirror the variants of the Operator enum in rpc/nioh/models.proto
const (
	// UnknownOperator represents any unknown or malformed Operator type
	UnknownOperator Operator = "UNKNOWN"
	// AllowOperator represents allowing content
	AllowOperator Operator = "ALLOW"
	// BlockOperator represents blocking content
	BlockOperator Operator = "BLOCK"
)

// ToProto converts an internal representation of a geoblock to a proto representation
func (g Geoblock) ToProto() nioh.Geoblock {
	return nioh.Geoblock{Operator: nioh.Geoblock_Operator(nioh.Geoblock_Operator_value[g.Operator.String()]), CountryCodes: g.CountryCodes}
}

// RestrictionGeoblocksToProto converts a list of internally
// represented geoblocks to a list of proto geoblocks
func RestrictionGeoblocksToProto(geoblocks ...Geoblock) []*nioh.Geoblock {
	var out []*nioh.Geoblock
	for _, g := range geoblocks {
		val := g.ToProto()
		out = append(out, &val)
	}
	return out
}

// RestrictionGeoblocksFromProto converts a list of proto
// geoblocks to a list of internally represented geoblocks
func RestrictionGeoblocksFromProto(geoblocks ...*nioh.Geoblock) []Geoblock {
	var out []Geoblock
	for _, g := range geoblocks {
		out = append(out, Geoblock{
			Operator:     Operator(g.GetOperator().String()),
			CountryCodes: g.GetCountryCodes(),
		})
	}
	return out
}
