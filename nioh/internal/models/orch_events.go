package models

// RestrictionUpdateEvent represents an event message requesting a restriction state update of a restrictable resource.
// This message is used to notify Graffiti of the correct autotag state for the resource.
type RestrictionUpdateEvent struct {
	Resource    RestrictableResource
	Restriction *RestrictionV2
}

// ShouldUpdateAutotag implements the business logic that decides whether the RestrictionUpdateEvent should be projected
// as a PremiumVideoTagEvent, causing a corresponding update in the resource's tags.
func (r RestrictionUpdateEvent) ShouldUpdateAutotag() bool {
	return r.Resource.GetType() == ResourceTypeChannel &&
		(r.Restriction == nil ||
			r.Restriction.RestrictionType == SubOnlyLiveRestrictionType)
}

// GetFlattenedRestrictionType maps the `Restriction` field to a *RestrictionType. A nil restriction maps
// to a nil *RestrictionType, while a populated restriction maps to a *RestrictionType containing the restriction's
// RestrictionType.
func (r RestrictionUpdateEvent) GetFlattenedRestrictionType() *RestrictionType {
	if r.Restriction != nil {
		return &r.Restriction.RestrictionType
	}
	return nil
}

// PremiumVideoTagEvent maps to a message that is sent on SNS when an autotag should be added or removed from a channel
// due to a premium video feature (such as becoming restricted by a SUB_ONLY_LIVE restriction)
type PremiumVideoTagEvent struct {
	ChannelID          string  `json:"channel_id"`
	RestrictionType    *string `json:"restriction_type"`
	Timestamp          string  `json:"timestamp"` // RFC-3339
	RestrictionMinTier uint8   `json:"restriction_min_tier"`
}
