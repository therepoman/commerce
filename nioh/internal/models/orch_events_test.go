package models

import (
	"reflect"
	"testing"
)

func TestRestrictionUpdateEvent_shouldUpdateAutotag(t *testing.T) {
	const testChannelID = "test-channel"

	type fields struct {
		Resource    RestrictableResource
		Restriction *RestrictionV2
	}
	tests := []struct {
		name   string
		fields fields
		want   bool
	}{
		{
			name: "restriction is nil – should update (to remove)",
			fields: fields{
				Resource: Channel{
					ID: testChannelID,
				},
				Restriction: nil,
			},
			want: true,
		},
		{
			name: "restriction is present and SOL – should update (to add)",
			fields: fields{
				Resource: Channel{
					ID: testChannelID,
				},
				Restriction: &RestrictionV2{RestrictionType: SubOnlyLiveRestrictionType},
			},
			want: true,
		},
		{
			name: "restriction is present and not SOL – should not update",
			fields: fields{
				Resource: Channel{
					ID: testChannelID,
				},
				Restriction: &RestrictionV2{RestrictionType: AllAccessPassRestrictionType},
			},
			want: false,
		},
		{
			name: "restriction is present and SOL, but resource is not channel - should not update",
			fields: fields{
				Resource: VOD{
					ID:      "test-vod",
					OwnerID: "test-channel",
				},
				Restriction: &RestrictionV2{RestrictionType: AllAccessPassRestrictionType},
			},
			want: false,
		},
		{
			name: "restriction is nil, but resource is not channel - should not update",
			fields: fields{
				Resource: VOD{
					ID:      "test-vod",
					OwnerID: "test-channel",
				},
				Restriction: nil,
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := RestrictionUpdateEvent{
				Resource:    tt.fields.Resource,
				Restriction: tt.fields.Restriction,
			}
			if got := r.ShouldUpdateAutotag(); got != tt.want {
				t.Errorf("RestrictionUpdateEvent.ShouldUpdateAutotag() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRestrictionUpdateEvent_getFlattenedRestrictionType(t *testing.T) {
	// can't take address of constants; make shadow vars
	aapRestrictionType := AllAccessPassRestrictionType
	solRestrictionType := SubOnlyLiveRestrictionType

	type fields struct {
		Resource    RestrictableResource
		Restriction *RestrictionV2
	}
	tests := []struct {
		name   string
		fields fields
		want   *RestrictionType
	}{
		{
			name: "returns pointer to restriction type when restriction != nil",
			fields: fields{
				Restriction: &RestrictionV2{
					RestrictionType: AllAccessPassRestrictionType,
				},
			},
			want: &aapRestrictionType,
		},
		{
			name: "returns pointer to restriction type when restriction != nil",
			fields: fields{
				Restriction: &RestrictionV2{
					RestrictionType: SubOnlyLiveRestrictionType,
				},
			},
			want: &solRestrictionType,
		},
		{
			name: "returns nil when restriction == nil",
			fields: fields{
				Restriction: nil,
			},
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := RestrictionUpdateEvent{
				Resource:    tt.fields.Resource,
				Restriction: tt.fields.Restriction,
			}
			if got := r.GetFlattenedRestrictionType(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("RestrictionUpdateEvent.GetFlattenedRestrictionType() = %v, want %v", got, tt.want)
			}
		})
	}
}
