package models

import (
	"reflect"
	"testing"

	"code.justin.tv/commerce/nioh/rpc/nioh"
)

func TestRestrictionOption_ValidFor(t *testing.T) {
	tests := []struct {
		name            string
		opt             RestrictionOption
		restrictionType RestrictionType
		want            bool
	}{
		{
			name:            "unknown option, unknown restriction type",
			opt:             UnknownOption,
			restrictionType: UnknownRestrictionType,
			want:            false,
		},
		{
			name:            "malformed option, unknown restriction type",
			opt:             "( ͡° ͜ʖ ͡°)",
			restrictionType: UnknownRestrictionType,
			want:            false,
		},
		{
			name:            "unknown option, AAP restriction type",
			opt:             UnknownOption,
			restrictionType: AllAccessPassRestrictionType,
			want:            false,
		},
		{
			name:            "unknown option, SOL restriction type",
			opt:             UnknownOption,
			restrictionType: SubOnlyLiveRestrictionType,
			want:            false,
		},
		{
			name:            "channel moderator option, AAP restriction type",
			opt:             AllowChannelModeratorOption,
			restrictionType: AllAccessPassRestrictionType,
			want:            false,
		},
		{
			name:            "channel VIP option, AAP restriction type",
			opt:             AllowChannelVIPOption,
			restrictionType: AllAccessPassRestrictionType,
			want:            false,
		},
		{
			name:            "channel moderator option, SOL restriction type",
			opt:             AllowChannelModeratorOption,
			restrictionType: SubOnlyLiveRestrictionType,
			want:            true,
		},
		{
			name:            "channel VIP option, SOL restriction type",
			opt:             AllowChannelVIPOption,
			restrictionType: SubOnlyLiveRestrictionType,
			want:            true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.opt.ValidFor(tt.restrictionType); got != tt.want {
				t.Errorf("RestrictionOption.ValidFor() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRestrictionOption_ToProto(t *testing.T) {
	tests := []struct {
		name string
		o    RestrictionOption
		want nioh.Option
	}{
		{
			name: "malformed RestrictionOption",
			o:    "( ͡° ͜ʖ ͡°)",
			want: nioh.Option_UNKNOWN,
		},
		{
			name: "UNKNOWN RestrictionOption",
			o:    UnknownOption,
			want: nioh.Option_UNKNOWN,
		},
		{
			name: "channel VIP RestrictionOption",
			o:    AllowChannelVIPOption,
			want: nioh.Option_ALLOW_CHANNEL_VIP,
		},
		{
			name: "channel moderator RestrictionOption",
			o:    AllowChannelModeratorOption,
			want: nioh.Option_ALLOW_CHANNEL_MODERATOR,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.o.ToProto(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("RestrictionOption.ToProto() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRestrictionOptionFromProto(t *testing.T) {
	tests := []struct {
		name  string
		proto nioh.Option
		want  RestrictionOption
	}{
		{
			name:  "UNKNOWN nioh.Option",
			proto: nioh.Option_UNKNOWN,
			want:  UnknownOption,
		},
		{
			name:  "channel VIP nioh.Option",
			proto: nioh.Option_ALLOW_CHANNEL_VIP,
			want:  AllowChannelVIPOption,
		},
		{
			name:  "channel moderator nioh.Option",
			proto: nioh.Option_ALLOW_CHANNEL_MODERATOR,
			want:  AllowChannelModeratorOption,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := RestrictionOptionFromProto(tt.proto); got != tt.want {
				t.Errorf("RestrictionOptionFromProto() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestOptionsToProto(t *testing.T) {
	tests := []struct {
		name string
		opts []RestrictionOption
		want []nioh.Option
	}{
		{
			name: "all happy",
			opts: []RestrictionOption{AllowChannelModeratorOption, AllowChannelVIPOption},
			want: []nioh.Option{nioh.Option_ALLOW_CHANNEL_MODERATOR, nioh.Option_ALLOW_CHANNEL_VIP},
		},
		{
			name: "with unknowns",
			opts: []RestrictionOption{"ʕ•ᴥ•ʔ", AllowChannelVIPOption, "(▀̿Ĺ̯▀̿ ̿)", "( ͡° ͜ʖ ͡°)", AllowChannelModeratorOption},
			want: []nioh.Option{nioh.Option_UNKNOWN, nioh.Option_ALLOW_CHANNEL_VIP, nioh.Option_UNKNOWN, nioh.Option_UNKNOWN, nioh.Option_ALLOW_CHANNEL_MODERATOR},
		},
		{
			name: "nil",
			opts: nil,
			want: nil,
		},
		{
			name: "empty",
			opts: []RestrictionOption{},
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := RestrictionOptionsToProto(tt.opts); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("RestrictionOptionsToProto() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRestrictionOptionsFromProto(t *testing.T) {
	tests := []struct {
		name string
		opts []nioh.Option
		want []RestrictionOption
	}{
		{
			name: "all happy",
			opts: []nioh.Option{nioh.Option_ALLOW_CHANNEL_MODERATOR, nioh.Option_ALLOW_CHANNEL_VIP},
			want: []RestrictionOption{AllowChannelModeratorOption, AllowChannelVIPOption},
		},
		{
			name: "with unknowns",
			opts: []nioh.Option{nioh.Option_UNKNOWN, nioh.Option_ALLOW_CHANNEL_VIP, nioh.Option_UNKNOWN, nioh.Option_UNKNOWN, nioh.Option_ALLOW_CHANNEL_MODERATOR},
			want: []RestrictionOption{UnknownOption, AllowChannelVIPOption, UnknownOption, UnknownOption, AllowChannelModeratorOption},
		},
		{
			name: "nil",
			opts: nil,
			want: nil,
		},
		{
			name: "empty",
			opts: []nioh.Option{},
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := RestrictionOptionsFromProto(tt.opts); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("RestrictionOptionsFromProto() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRestrictionGeoblocksToProto(t *testing.T) {
	tests := []struct {
		name      string
		geoblocks []Geoblock
		want      []*nioh.Geoblock
	}{
		{
			name: "non-empty",
			geoblocks: []Geoblock{
				{Operator: BlockOperator, CountryCodes: []string{"AB", "CD"}},
				{Operator: AllowOperator, CountryCodes: []string{"EF", "GH"}},
			},
			want: []*nioh.Geoblock{
				{
					Operator:     nioh.Geoblock_BLOCK,
					CountryCodes: []string{"AB", "CD"},
				},
				{
					Operator:     nioh.Geoblock_ALLOW,
					CountryCodes: []string{"EF", "GH"},
				},
			},
		},
		{
			name:      "empty",
			geoblocks: nil,
			want:      nil,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := RestrictionGeoblocksToProto(tt.geoblocks...); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("RestrictionToProto() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRestrictionGeoblocksFromProto(t *testing.T) {
	tests := []struct {
		name      string
		geoblocks []*nioh.Geoblock
		want      []Geoblock
	}{
		{
			name: "non-empty",
			geoblocks: []*nioh.Geoblock{
				{
					Operator:     nioh.Geoblock_BLOCK,
					CountryCodes: []string{"AB", "CD"},
				},
				{
					Operator:     nioh.Geoblock_ALLOW,
					CountryCodes: []string{"EF", "GH"},
				},
			},
			want: []Geoblock{
				{Operator: BlockOperator, CountryCodes: []string{"AB", "CD"}},
				{Operator: AllowOperator, CountryCodes: []string{"EF", "GH"}},
			},
		},
		{
			name:      "empty",
			geoblocks: nil,
			want:      nil,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := RestrictionGeoblocksFromProto(tt.geoblocks...); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("RestrictionGeoblocksFromProto() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRestrictionFromProto(t *testing.T) {
	tests := []struct {
		name        string
		restriction *nioh.Restriction
		want        RestrictionV2
	}{
		{
			name: "happy case",
			restriction: &nioh.Restriction{
				RestrictionType: nioh.Restriction_GEOBLOCK,
				Options:         []nioh.Option{nioh.Option_UNKNOWN},
				Geoblocks: []*nioh.Geoblock{
					{Operator: nioh.Geoblock_BLOCK, CountryCodes: []string{"AB"}},
				},
			},
			want: RestrictionV2{
				RestrictionType: GeoblockRestrictionType,
				Options:         []RestrictionOption{UnknownOption},
				Geoblocks: []Geoblock{
					{Operator: BlockOperator, CountryCodes: []string{"AB"}},
				},
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := RestrictionFromProto(tt.restriction); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("RestrictionFromProto() = %v, want %v", got, tt.want)
			}
		})
	}
}
