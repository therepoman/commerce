package models

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/nioh/rpc/nioh"
	"code.justin.tv/common/chitin"
	"github.com/golang/protobuf/ptypes"
	"github.com/golang/protobuf/ptypes/timestamp"
	"github.com/pkg/errors"
)

// RecordToProto converts an internal record to a Twirp/Protobuf record.
func RecordToProto(record *Record) *nioh.Record {
	return &nioh.Record{
		ChannelId: record.ChannelID,
		RecordId:  record.ID,
		Data:      record.Data,
	}
}

// ContentAttributesToProtos converts internal ContentAttributes to Twirp/Protobuf ContentAttributes
func ContentAttributesToProtos(ctx context.Context, models []*ContentAttribute) []*nioh.ContentAttribute {
	protos := make([]*nioh.ContentAttribute, 0, len(models))
	for _, model := range models {
		protos = append(protos, ContentAttributeToProto(ctx, model))
	}

	return protos
}

// ContentAttributeToProto converts an internal ContentAttribute to a Twirp/Protobuf ContentAttribute
func ContentAttributeToProto(ctx context.Context, model *ContentAttribute) *nioh.ContentAttribute {
	// those timestamps are only used on the broadcaster dashboard,
	// not failing here so that in the case of a bug on time conversion, we can still serve back content attributes (just with inaccurate timestamps)
	var createdAt, updatedAt *timestamp.Timestamp
	createdAt, err := ptypes.TimestampProto(model.CreatedAt)
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"RequestID": chitin.GetTraceID(ctx),
			"createdAt": model.CreatedAt,
		}).WithError(err).Error("failed to convert createdAt timestamp from model content attribute")
	}
	updatedAt, err = ptypes.TimestampProto(model.UpdatedAt)
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"RequestID": chitin.GetTraceID(ctx),
			"updatedAt": model.UpdatedAt,
		}).WithError(err).Error("failed to convert updatedAt timestamp from model content attribute")
	}
	return &nioh.ContentAttribute{
		OwnerChannelId:     model.OwnerChannelID,
		Id:                 model.ID,
		AttributeKey:       model.AttributeKey,
		AttributeName:      model.AttributeName,
		Value:              model.Value,
		ValueShortname:     model.ValueShortname,
		ParentAttributeId:  model.ParentAttributeID,
		ParentAttributeKey: model.ParentAttributeKey,
		ChildAttributeIds:  model.ChildAttributeIDs,
		ImageUrl:           model.ImageURL,
		CreatedAt:          createdAt,
		UpdatedAt:          updatedAt,
	}
}

// ContentAttributesFromProtos converts Twirp/Protobuf ContentAttributes to internal ContentAttributes
func ContentAttributesFromProtos(ctx context.Context, protos []*nioh.ContentAttribute) []*ContentAttribute {
	models := make([]*ContentAttribute, 0, len(protos))
	for _, proto := range protos {
		models = append(models, ContentAttributeFromProto(ctx, proto))
	}

	return models
}

// ContentAttributeFromProto converts a Twirp/Protobuf ContentAttribute to an internal ContentAttribute
func ContentAttributeFromProto(ctx context.Context, proto *nioh.ContentAttribute) *ContentAttribute {
	// logging and not failing the conversion here since the fields are overridden internally by Nioh
	// both the creation and the update API use their internal clocks and do not rely on the provided timestamps on the proto
	createdAt, err := TimeFromProtoTimestamp(proto.CreatedAt)
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"RequestID": chitin.GetTraceID(ctx),
			"createdAt": proto.CreatedAt,
		}).WithError(err).Error("failed to convert createdAt timestamp from model content attribute")
	}
	updatedAt, err := TimeFromProtoTimestamp(proto.UpdatedAt)
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"RequestID": chitin.GetTraceID(ctx),
			"updatedAt": proto.UpdatedAt,
		}).WithError(err).Error("failed to convert updatedAt timestamp from model content attribute")
	}

	return &ContentAttribute{
		OwnerChannelID:     proto.OwnerChannelId,
		ID:                 proto.Id,
		AttributeKey:       proto.AttributeKey,
		AttributeName:      proto.AttributeName,
		Value:              proto.Value,
		ValueShortname:     proto.ValueShortname,
		ParentAttributeID:  proto.ParentAttributeId,
		ParentAttributeKey: proto.ParentAttributeKey,
		ChildAttributeIDs:  proto.ChildAttributeIds,
		ImageURL:           proto.ImageUrl,
		CreatedAt:          createdAt,
		UpdatedAt:          updatedAt,
	}
}

// VODRestrictionReasonFromJSON parses the contents of a string as JSON whose structure matches the conventional
// "Hedwig reason data" format, an important piece of data for determining a user's VOD access eligibility.
func VODRestrictionReasonFromJSON(reasonData string) (*VODRestrictionReason, error) {
	out := &VODRestrictionReason{}
	err := json.Unmarshal([]byte(reasonData), out)

	return out, err
}

// BadReasonDataError is a type whose instances signify that the HedwigReasonData in a GetUserAuthorization request
// targeting a VOD resource was malformed or missing
type BadReasonDataError struct {
	UnmarshalError error
}

func (b BadReasonDataError) Error() string {
	return fmt.Sprintf("HedwigReasonData could not be deserialized as JSON: %v", b.UnmarshalError)
}

// VODAuthorizationRequestFromProto converts an API request into an internal representation of a VOD request.
func VODAuthorizationRequestFromProto(proto *nioh.GetUserAuthorizationRequest) (*VODAuthorizationRequest, error) {
	vod := proto.GetVod()
	if vod == nil {
		return nil, errors.New("VODAuthorizationRequestFromProto called with proto request that does not contain VOD resource")
	}

	// TODO: `ConsumePreview` field seems like it should be passed in here as well, but for now we don't want to provide VODs with previews.
	out := &VODAuthorizationRequest{
		UserID:    proto.GetUserId(),
		VideoID:   vod.GetVideoId(),
		ChannelID: vod.GetChannelId(),
		Origin:    proto.GetOrigin(),
	}

	hedwigReasonData := vod.GetHedwigReasonData()

	if hedwigReasonData != "" {
		reason, err := VODRestrictionReasonFromJSON(hedwigReasonData)
		if err != nil {
			return out, BadReasonDataError{UnmarshalError: err}
		}

		out.Reason = reason
	}

	return out, nil
}

// VODAuthorizationToProto converts an internal VODAuthorization representation to an API response.
func VODAuthorizationToProto(authorization *VODAuthorization) (*nioh.GetUserAuthorizationResponse, error) {
	proto := &nioh.GetUserAuthorizationResponse{
		UserId:               authorization.Request.UserID,
		UserIsAuthorized:     authorization.UserIsAuthorized,
		ResourceIsRestricted: authorization.ResourceIsRestricted,
		Resource: &nioh.GetUserAuthorizationResponse_Vod{
			Vod: &nioh.VOD{
				ChannelId: authorization.Request.ChannelID,
				VideoId:   authorization.Request.VideoID,
			},
		},
		RestrictionType: RestrictionTypeToProto(authorization.RestrictionType),
	}

	if len(authorization.AppliedExemptions) > 0 {
		exemptionProtos, err := AppliedExemptionsToProto(authorization.AppliedExemptions)
		if err != nil {
			return nil, errors.Wrap(err, "failed to convert exemptions to proto in VODAuthorizationToProto")
		}
		proto.AppliedExemptions = exemptionProtos
	}

	return proto, nil
}

// ChanletToProto converts an internal Chanlet to Twirp/Protobuf Chanlet
func ChanletToProto(ctx context.Context, model *Chanlet) *nioh.Chanlet {
	return &nioh.Chanlet{
		OwnerChannelId:    model.OwnerChannelID,
		ChanletId:         model.ChanletID,
		ContentAttributes: ContentAttributesToProtos(ctx, model.ContentAttributes),
		IsPublicChannel:   model.IsPublicChannel,
	}
}

// ChanletsToProtos converts internal Chanlets to Twirp/Protobuf Chanlets
func ChanletsToProtos(ctx context.Context, models []*Chanlet) []*nioh.Chanlet {
	protos := make([]*nioh.Chanlet, 0, len(models))
	for _, model := range models {
		protos = append(protos, ChanletToProto(ctx, model))
	}

	return protos
}

// ChannelRelationToProto converts an internal ChannelRelation to Twirp/Protobuf ChannelRelation
func ChannelRelationToProto(model *ChannelRelation) *nioh.ChannelRelation {
	return &nioh.ChannelRelation{
		ChannelId:        model.ChannelID,
		RelatedChannelId: model.RelatedChannelID,
		Relation:         string(model.Relation),
	}
}

// ChannelRelationsToProtos converts an list of internal ChannelRelation to a list of Twirp/Protobuf ChannelRelation
func ChannelRelationsToProtos(models []*ChannelRelation) []*nioh.ChannelRelation {
	protos := make([]*nioh.ChannelRelation, 0, len(models))
	for _, model := range models {
		proto := ChannelRelationToProto(model)
		protos = append(protos, proto)
	}

	return protos
}

// TimeFromProtoTimestamp converts a google.protobuf.Timestamp to time.Time
func TimeFromProtoTimestamp(proto *timestamp.Timestamp) (time.Time, error) {
	var time time.Time
	var err error

	if proto != nil {
		// Convert google.protobuf.Timestamp type to time.Time type
		time, err = ptypes.Timestamp(proto)
		if err != nil {
			return time, errors.Wrap(err, "Timestamp conversion failure")
		}
	}

	return time, nil
}

// RestrictionToProto converts an internal representation of a RestrictionV2 into an RPC-friendly representation.
func RestrictionToProto(restriction *RestrictionV2) (*nioh.Restriction, error) {
	out := new(nioh.Restriction)

	// TODO: maybe out.Id becomes out.ResourceKey
	out.Id = restriction.ResourceKey

	out.RestrictionType = RestrictionTypeToProto(restriction.RestrictionType)

	exemptions, err := ExemptionsToProto(restriction.Exemptions)
	if err != nil {
		return nil, err
	}

	out.Exemptions = exemptions
	out.Options = RestrictionOptionsToProto(restriction.Options)
	out.Geoblocks = RestrictionGeoblocksToProto(restriction.Geoblocks...)

	return out, nil
}

// ExemptionToProto converts an internal representation of Exemption into an RPC-friendly representation.
func ExemptionToProto(exemption *Exemption) (*nioh.Exemption, error) {
	startDate, err := ptypes.TimestampProto(exemption.ActiveStartDate)
	if err != nil {
		return nil, err
	}
	endDate, err := ptypes.TimestampProto(exemption.ActiveEndDate)
	if err != nil {
		return nil, err
	}

	proto := &nioh.Exemption{
		ExemptionType:   ExemptionTypeToProto(exemption.ExemptionType),
		ActiveStartDate: startDate,
		ActiveEndDate:   endDate,
		ExemptionKeys:   exemption.Keys,
		Actions:         ExemptionActionsToProto(exemption.Actions),
	}

	return proto, nil
}

// AppliedExemptionToProto converts an internal representation of AppliedExemption into an RPC-friendly representation.
func AppliedExemptionToProto(appliedExemption *AppliedExemption) (*nioh.Exemption, error) {
	return ExemptionToProto(appliedExemption.Exemption)
}

// ExemptionsToProto converts a list of internal representations of Exemptions into an RPC-friendly representation.
func ExemptionsToProto(exemptions []*Exemption) ([]*nioh.Exemption, error) {
	out := make([]*nioh.Exemption, 0, len(exemptions))

	for _, exemption := range exemptions {
		proto, err := ExemptionToProto(exemption)
		if err != nil {
			return nil, err
		}

		out = append(out, proto)
	}

	return out, nil
}

// AppliedExemptionsToProto converts a slice of backend AppliedExemption to proto
func AppliedExemptionsToProto(exemptions []*AppliedExemption) ([]*nioh.Exemption, error) {
	out := make([]*nioh.Exemption, 0, len(exemptions))

	for _, exemption := range exemptions {
		proto, err := AppliedExemptionToProto(exemption)
		if err != nil {
			return nil, err
		}

		out = append(out, proto)
	}

	return out, nil
}

// RestrictionTypeToProto maps an internal RestrictionType onto the strictly-equivalent RPC RestrictionType.
func RestrictionTypeToProto(restrictionType RestrictionType) nioh.Restriction_RestrictionType {
	return nioh.Restriction_RestrictionType(nioh.Restriction_RestrictionType_value[string(restrictionType)])
}

// RestrictionTypeFromProto maps an RPC RestrictionType to the equivalent internal model.
func RestrictionTypeFromProto(proto nioh.Restriction_RestrictionType) RestrictionType {
	return RestrictionType(proto.String())
}

// ExemptionTypeToProto maps an internal ExemptionType onto the strictly-equivalent RPC ExemptionType.
func ExemptionTypeToProto(exemptionType ExemptionType) nioh.Exemption_ExemptionType {
	return nioh.Exemption_ExemptionType(nioh.Exemption_ExemptionType_value[string(exemptionType)])
}

// ExemptionActionsToProto maps an internal ExemptionAction onto the strictly-equivalent RPC ExemptionAction.
func ExemptionActionsToProto(exemptionActions []*ExemptionAction) []*nioh.Exemption_Action {
	if len(exemptionActions) == 0 {
		return nil
	}
	out := make([]*nioh.Exemption_Action, 0, len(exemptionActions))

	for _, action := range exemptionActions {
		proto := &nioh.Exemption_Action{
			Name:  action.Name,
			Title: action.Title,
		}
		out = append(out, proto)
	}
	return out
}

// ChannelAuthorizationToGetUserAuthorizationForResourceProto converts to a ChannelAuthorization to GetUserAuthorizationResponse RPC representation
func ChannelAuthorizationToGetUserAuthorizationForResourceProto(channelAuth *ChannelAuthorization) (*nioh.GetUserAuthorizationResponse, error) {
	proto := &nioh.GetUserAuthorizationResponse{}
	proto.UserId = channelAuth.Request.UserID
	proto.UserIsAuthorized = channelAuth.UserIsAuthorized
	proto.ResourceIsRestricted = channelAuth.ResourceIsRestricted
	proto.RestrictionType = RestrictionTypeToProto(channelAuth.RestrictionType)
	proto.Resource = &nioh.GetUserAuthorizationResponse_Channel{
		Channel: &nioh.Channel{Id: channelAuth.Request.ChannelID},
	}

	zeroTime := time.Time{}
	accessExpiration := zeroTime
	exemptionProtos := make([]*nioh.Exemption, 0, len(channelAuth.AppliedExemptions))
	for _, exemption := range channelAuth.AppliedExemptions {
		protoExemption, err := AppliedExemptionToProto(exemption)
		if err != nil {
			return nil, err
		}

		exemptionProtos = append(exemptionProtos, protoExemption)

		// Preview exemptions should set access expirations
		if exemption.ExemptionType == PreviewExemptionType &&
			exemption.ActiveEndDate.After(accessExpiration) &&
			exemption.ActiveEndDate.Before(IndefiniteEndDate) {
			accessExpiration = exemption.ActiveEndDate
		}
	}
	proto.AppliedExemptions = exemptionProtos

	// Add AccessExpiration if set to a definite time
	if accessExpiration.After(zeroTime) {
		accessExpirationProto, err := ptypes.TimestampProto(accessExpiration)
		if err != nil {
			return nil, err
		}
		proto.AccessExpiration = accessExpirationProto
	}

	return proto, nil
}

// ChannelAuthorizationsToUserAuthorizationForResourceProto converts a list of ChannelAuthorization to
// a list of UserAuthorizationForResource RPC representation
func ChannelAuthorizationsToUserAuthorizationForResourceProto(channelAuths []*ChannelAuthorization) ([]*nioh.UserAuthorizationForResource, error) {
	out := make([]*nioh.UserAuthorizationForResource, 0, len(channelAuths))

	for _, channelAuth := range channelAuths {
		proto, err := ChannelAuthorizationToUserAuthorizationForResourceProto(channelAuth)
		if err != nil {
			return nil, errors.WithStack(err)
		}
		out = append(out, proto)
	}

	return out, nil
}

// ChannelAuthorizationToUserAuthorizationForResourceProto converts a ChannelAuthorization to UserAuthorizationForResource RPC representation
func ChannelAuthorizationToUserAuthorizationForResourceProto(channelAuth *ChannelAuthorization) (*nioh.UserAuthorizationForResource, error) {
	proto := &nioh.UserAuthorizationForResource{}
	proto.UserId = channelAuth.Request.UserID
	proto.UserIsAuthorized = channelAuth.UserIsAuthorized
	proto.ResourceIsRestricted = channelAuth.ResourceIsRestricted
	proto.RestrictionType = RestrictionTypeToProto(channelAuth.RestrictionType)
	proto.Resource = &nioh.UserAuthorizationForResource_Channel{
		Channel: &nioh.Channel{Id: channelAuth.Request.ChannelID},
	}
	if channelAuth.Error != nil {
		proto.Error = channelAuth.Error.Error()
	}
	exemptionProtos, err := AppliedExemptionsToProto(channelAuth.AppliedExemptions)
	if err != nil {
		return nil, errors.Wrap(err, "failed to convert exemptions to proto in ChannelAuthorizationsToUserAuthorizationForResourceProto")
	}
	proto.AppliedExemptions = exemptionProtos
	return proto, nil
}

// VODAuthorizationsToUserAuthorizationsForResourcesProto coverts a list of VODAuthorization to a list of UserAuthorizationForResource RPC representation
func VODAuthorizationsToUserAuthorizationsForResourcesProto(vodAuths []*VODAuthorization) ([]*nioh.UserAuthorizationForResource, error) {
	out := make([]*nioh.UserAuthorizationForResource, 0, len(vodAuths))

	for _, vodAuth := range vodAuths {
		proto, err := VODAuthorizationToUserAuthorizationForResourceProto(vodAuth)
		if err != nil {
			return nil, errors.WithStack(err)
		}
		out = append(out, proto)
	}

	return out, nil
}

// VODAuthorizationToUserAuthorizationForResourceProto converts a VODAuthorization to UserAuthorizationForResource RPC representation
func VODAuthorizationToUserAuthorizationForResourceProto(vodAuth *VODAuthorization) (*nioh.UserAuthorizationForResource, error) {
	proto := &nioh.UserAuthorizationForResource{
		UserId:               vodAuth.Request.UserID,
		UserIsAuthorized:     vodAuth.UserIsAuthorized,
		ResourceIsRestricted: vodAuth.ResourceIsRestricted,
		RestrictionType:      RestrictionTypeToProto(vodAuth.RestrictionType),
		Resource: &nioh.UserAuthorizationForResource_Vod{
			Vod: &nioh.VOD{VideoId: vodAuth.Request.VideoID},
		},
	}

	if vodAuth.Error != nil {
		proto.Error = vodAuth.Error.Error()
	}

	exemptionProtos, err := AppliedExemptionsToProto(vodAuth.AppliedExemptions)
	if err != nil {
		return nil, errors.Wrap(err, "failed to convert exemptions to proto in VODAuthorizationToUserAuthorizationForResourceProto")
	}
	proto.AppliedExemptions = exemptionProtos
	return proto, nil
}

// RestrictionResourceToProto converts a restriction resource model into an RPC-friendly representation based on resource type.
func RestrictionResourceToProto(resource *nioh.RestrictionResource) *nioh.RestrictionResourceResponse {
	return &nioh.RestrictionResourceResponse{
		Id:   resource.GetId(),
		Type: resource.GetType(),
	}
}

// OwnerChanletAttributesToProto converts an OwnerChanletAttributes to an RPC-friendly representation.
func OwnerChanletAttributesToProto(attrs *OwnerChanletAttributes) *nioh.OwnerChanletAttributes {
	if attrs != nil {
		return &nioh.OwnerChanletAttributes{
			ChanletsEnabled: attrs.ChanletsEnabled,
		}
	}

	// nil reference converts to the default configs (all set to zero values)
	return &nioh.OwnerChanletAttributes{}
}

// OwnerChanletAttributesFromProto converts an RPC proto to OwnerChanletAttributes
func OwnerChanletAttributesFromProto(attrs *nioh.OwnerChanletAttributes) *OwnerChanletAttributes {
	return &OwnerChanletAttributes{
		ChanletsEnabled: attrs.ChanletsEnabled,
	}
}
