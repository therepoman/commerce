package models

import (
	"fmt"
	"strings"

	"code.justin.tv/commerce/nioh/rpc/nioh"
)

// RestrictableResource is a representation of some Nioh-external resource whose access policy is stored and implemented by Nioh.
type RestrictableResource interface {
	// GetResourceKey should return a key of the format `<resource-type>:<resource-id>` that is globally unique at Twitch.
	GetResourceKey() string
	// GetType should return resource type
	GetType() ResourceType
	// GetIdentifier should return the scoped identifier string that uniquely identifies this resource within its type
	GetIdentifier() string
}

// RestrictableResourceFromProto converts a proto representation of a restricable resource to an internal model
func RestrictableResourceFromProto(resource *nioh.RestrictionResource, ownerID string) RestrictableResource {
	switch resource.GetType() {
	case nioh.ResourceType_LIVE:
		return Channel{ID: resource.GetId()}
	case nioh.ResourceType_VIDEO:
		return VOD{ID: resource.GetId(), OwnerID: ownerID}
	case nioh.ResourceType_BROADCAST:
		return Broadcast{ID: resource.GetId()}
	default:
		return nil
	}
}

// RestrictableResourceWithRequester extends the Resource interface with information by caller requesting origins.
type RestrictableResourceWithRequester interface {
	RestrictableResource

	GetRequesterOrigin() string

	// GetOrigin should return the scoped string that should identify where a resource is requested from
	GetResourceKeyWithRequester() string
}

// ResourceType represents a restictable resource type
type ResourceType string

const (
	// ResourceTypeChannel is a channel
	ResourceTypeChannel = "channel"
	// ResourceTypeVOD is a vod
	ResourceTypeVOD = "vod"
	// ResourceTypeBroadcast is a broadcast
	ResourceTypeBroadcast = "broadcast"
)

// Channel represents a Twitch channel. ID is the Twitch User ID for the channel.
type Channel struct {
	ID string

	// Optional field used by free preview only
	RequesterOrigin string
}

var _ RestrictableResource = Channel{}
var _ RestrictableResourceWithRequester = Channel{}

// GetResourceKey returns a channel-scoped resource key of the format "channel:<channel-id>".
func (r Channel) GetResourceKey() string {
	return fmt.Sprintf("channel:%s", r.ID)
}

// GetType returns resource type
func (r Channel) GetType() ResourceType {
	return ResourceTypeChannel
}

// GetIdentifier returns the channel's ID
func (r Channel) GetIdentifier() string {
	return r.ID
}

// GetRequesterOrigin returns the requester origin for the channel, if there is one.
func (r Channel) GetRequesterOrigin() string {
	return r.RequesterOrigin
}

// GetResourceKeyWithRequester returns a channel-scoped key with origin of an authorization request
func (r Channel) GetResourceKeyWithRequester() string {
	var key strings.Builder
	fmt.Fprintf(&key, "channel:%s", r.ID)

	// Concat origin if it exists
	if r.RequesterOrigin != "" {
		fmt.Fprintf(&key, ":%s", r.RequesterOrigin)
	}

	return key.String()
}

// VOD represents a Twitch VOD. ID is the VOD ID.
type VOD struct {
	ID      string
	OwnerID string

	// Optional field used by free preview only
	RequesterOrigin string
}

var _ RestrictableResource = VOD{}
var _ RestrictableResourceWithRequester = VOD{}

// GetResourceKey returns a video-scoped resource key of the format "vod:<vod-id>".
func (r VOD) GetResourceKey() string {
	return fmt.Sprintf("vod:%s", r.ID)
}

// GetType returns resource type
func (r VOD) GetType() ResourceType {
	return ResourceTypeVOD
}

// GetIdentifier returns the VOD ID (*not* the owning channel ID)
func (r VOD) GetIdentifier() string {
	return r.ID
}

// GetRequesterOrigin returns the requester origin for the VOD, if there is one.
func (r VOD) GetRequesterOrigin() string {
	return r.RequesterOrigin
}

// GetResourceKeyWithRequester returns a video-scoped key with origin of an authorization request
func (r VOD) GetResourceKeyWithRequester() string {
	var key strings.Builder
	fmt.Fprintf(&key, "vod:%s", r.ID)

	// Concat origin if it exists
	if r.RequesterOrigin != "" {
		fmt.Fprintf(&key, ":%s", r.RequesterOrigin)
	}

	return key.String()
}

// Broadcast represents a Twitch broadcast. ID is the ID of the original broadcast. This type is used to restrict all content derived from a live stream as they all share the same broadcast ID
type Broadcast struct {
	ID string

	// Optional field used by free preview only
	RequesterOrigin string
}

var _ RestrictableResource = Broadcast{}
var _ RestrictableResourceWithRequester = Broadcast{}

// GetResourceKey returns a broadcast-scoped resource key of the format "broadcast:<broadcast-id>".
func (r Broadcast) GetResourceKey() string {
	return fmt.Sprintf("broadcast:%s", r.ID)
}

// GetType returns resource type
func (r Broadcast) GetType() ResourceType {
	return ResourceTypeBroadcast
}

// GetIdentifier returns the broadcast's ID
func (r Broadcast) GetIdentifier() string {
	return r.ID
}

// GetRequesterOrigin returns the requester origin for the broadcast, if there is one.
func (r Broadcast) GetRequesterOrigin() string {
	return r.RequesterOrigin
}

// GetResourceKeyWithRequester returns a broadcast-scoped key with origin of an authorization request
func (r Broadcast) GetResourceKeyWithRequester() string {
	var key strings.Builder
	fmt.Fprintf(&key, "broadcast:%s", r.ID)

	// Concat origin if it exists
	if r.RequesterOrigin != "" {
		fmt.Fprintf(&key, ":%s", r.RequesterOrigin)
	}

	return key.String()
}
