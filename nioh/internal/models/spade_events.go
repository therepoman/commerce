package models

import (
	"strings"
	"time"

	"code.justin.tv/common/spade-client-go/spade"
)

// SpadeEvent represents some value that can generate the correct data to be sent as a Spade event.
type SpadeEvent interface {
	// EventName() should return the string that uniquely identifies this Spade event in Tahoe.
	EventName() string
	// Params() should return the event's parameters.
	Params() map[string]interface{}
}

// SpadeEvents is a slice of SpadeEvent. Useful for spade.TrackEvents
type SpadeEvents []SpadeEvent

// ToNativeSpadeEvents converts SpadeEvents to native []spade.Event for use with spade.TrackEvents
func (s SpadeEvents) ToNativeSpadeEvents() []spade.Event {
	nativeSpadeEvents := make([]spade.Event, len(s))
	for index, nativeSpadeEvent := range s {
		nativeSpadeEvents[index] = spade.Event{
			Name:       nativeSpadeEvent.EventName(),
			Properties: nativeSpadeEvent.Params(),
		}
	}
	return nativeSpadeEvents
}

type channelRestrictionToggleMethod string

const (
	// ToggleMethodAPI indicates that a channel restriction was manipulated by an API call to Nioh.
	ToggleMethodAPI channelRestrictionToggleMethod = "api"
	// ToggleMethodSystemOfflineCleanup indicates that a channel restriction was removed due to Nioh's offline restriction cleanup functionality.
	ToggleMethodSystemOfflineCleanup channelRestrictionToggleMethod = "system_offline_cleanup"
)

// ChannelRestrictionStatusEvent represents a change in a channel's restriction state. It describes whether the change
// originated from an API call or from a system event (such as offline cleanup). If the change originated from the API,
// ClientID contains the `Twitch-Client-Id` header value (if any) that was attached to the API request.
type ChannelRestrictionStatusEvent struct {
	ChannelID       int // yuck! but channel IDs as numbers is what's standard within our tracking data, unfortunately.
	IsRestricted    bool
	IncludeMods     bool
	IncludeVIPs     bool
	IncludeSubs1000 bool // Subs1000 = Subscription Tier 1 and so on
	IncludeSubs2000 bool
	IncludeSubs3000 bool
	ToggleMethod    channelRestrictionToggleMethod
	RestrictionType RestrictionType
	ClientID        string
}

// EventName implements SpadeEvent
func (c ChannelRestrictionStatusEvent) EventName() string {
	return "channel_restriction_status"
}

// Params implements SpadeEvent
func (c ChannelRestrictionStatusEvent) Params() map[string]interface{} {
	return map[string]interface{}{
		"channel_id":        c.ChannelID,
		"is_restricted":     c.IsRestricted,
		"toggle_method":     c.ToggleMethod,
		"restriction_type":  strings.ToLower(c.RestrictionType.String()),
		"include_mods":      c.IncludeMods,
		"include_vips":      c.IncludeVIPs,
		"client_id":         c.ClientID,
		"include_subs_1000": c.IncludeSubs1000,
		"include_subs_2000": c.IncludeSubs2000,
		"include_subs_3000": c.IncludeSubs3000,
	}
}

// UserAuthorizationEvent represents spade events that are reported every time a
// viewer is granted an exemption to a restricted stream
type UserAuthorizationEvent struct {
	ExemptionType          ExemptionType
	StartTime              time.Time
	ExemptionLengthSeconds int
	EndTime                time.Time
	ChannelID              string
	VideoID                string
	UserID                 string
	ResetTime              time.Time
	MatchedKey             string
}

var _ SpadeEvent = UserAuthorizationEvent{}

// EventName implements SpadeEvent
func (e UserAuthorizationEvent) EventName() string {
	return "viewer_exemptions_granted"
}

// Params implements SpadeEvent
func (e UserAuthorizationEvent) Params() map[string]interface{} {
	eventJSON := map[string]interface{}{
		"exemption_type":   strings.ToLower(e.ExemptionType.String()),
		"start_time":       e.StartTime,
		"exemption_length": e.ExemptionLengthSeconds,
		"end_time":         e.EndTime,
		"channel_id":       e.ChannelID,
		"video_id":         e.VideoID,
		"user_id":          e.UserID,
		"reset_time":       e.ResetTime,
	}
	if e.MatchedKey != "" {
		eventJSON["matched_key"] = e.MatchedKey
	}

	return eventJSON
}
