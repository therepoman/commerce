package models_test

import (
	"context"
	"testing"

	"code.justin.tv/commerce/nioh/internal/models"
	"code.justin.tv/commerce/nioh/internal/testdata"
	"code.justin.tv/commerce/nioh/rpc/nioh"
	. "github.com/smartystreets/goconvey/convey"
)

func TestConverters(t *testing.T) {
	Convey("Test Converters", t, func() {
		Convey("ContentAttributesToProtos", func() {
			So(models.ContentAttributesToProtos(context.Background(), testdata.ContentAttributes()),
				ShouldResemble,
				testdata.ProtoContentAttributes())
		})

		Convey("ContentAttributeToProto", func() {
			So(models.ContentAttributeToProto(context.Background(), testdata.ContentAttribute("1", "team", "Team ABC", "ABC")),
				ShouldResemble,
				testdata.ProtoContentAttribute("1", "team", "Team ABC", "ABC"))
		})

		Convey("ContentAttributesFromProtos", func() {
			So(models.ContentAttributesFromProtos(context.Background(), testdata.ProtoContentAttributes()),
				ShouldResemble,
				testdata.ContentAttributes())
		})

		Convey("ContentAttributeFromProto", func() {
			So(models.ContentAttributeFromProto(context.Background(), testdata.ProtoContentAttribute("1", "team", "Team XYZ", "XYZ")),
				ShouldResemble,
				testdata.ContentAttribute("1", "team", "Team XYZ", "XYZ"))
		})

		Convey("RestrictionToProto", func() {
			channelID := "146851449"
			actual, err := models.RestrictionToProto(testdata.Restriction(channelID))
			So(err, ShouldBeNil)
			So(actual, ShouldResemble, testdata.ProtoRestriction(channelID))
		})

		Convey("RestrictionResourceToProto", func() {
			So(models.RestrictionResourceToProto(testdata.ProtoRestrictionResource("1", nioh.ResourceType_LIVE)),
				ShouldResemble,
				testdata.ResourceRestriction("1", nioh.ResourceType_LIVE))
		})

		Convey("ChannelAuthorizationToGetUserAuthorizationForResourceProto", func() {
			Convey("returns access_expiration for previews", func() {
				testAuth := testdata.ChannelAuthorizationWithPreviewExemption()
				authProto, err := models.ChannelAuthorizationToGetUserAuthorizationForResourceProto(&testAuth)
				So(err, ShouldBeNil)
				So(authProto, ShouldResemble, testdata.GetUserAuthorizationForResourceProto())
			})

			Convey("does not return access_expiration for other exemptions", func() {
				testAuth := testdata.ChannelAuthorizationWithAllExemption()
				authProto, err := models.ChannelAuthorizationToGetUserAuthorizationForResourceProto(&testAuth)
				So(err, ShouldBeNil)
				So(authProto, ShouldResemble, testdata.GetUserAuthorizationForResourceProtoAll())
			})
		})
	})
}
