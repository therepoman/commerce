package models

import (
	"time"
)

// Record is a single record - corresponds to the record object stored in
// DynamoDB.
type Record struct {
	ID        string    `dynamo:"ID" json:"id"`
	ChannelID string    `dynamo:"ChannelID" json:"channel_id"`
	Data      string    `dynamo:"Data" json:"data"`
	UpdatedAt time.Time `dynamo:"UpdatedAt" json:"updated_at"`
}

// Equals returns true if the record equals the one supplied.
func (i *Record) Equals(other *Record) bool {
	if other == nil {
		return false
	}

	return i.ID == other.ID && i.ChannelID == other.ChannelID && i.Data == other.Data && i.UpdatedAt == other.UpdatedAt
}

// RecordCreate is used to create a new record - gets translated into a Record
// object.
type RecordCreate struct {
	ChannelID string     `json:"channel_id"`
	Data      string     `json:"data"`
	UpdatedAt *time.Time `json:"updated_at"`
}

// RecordUpdate is used to update an existing record - gets translated into a
// Record object.
type RecordUpdate struct {
	ID        string     `json:"id"`
	ChannelID string     `json:"channel_id"`
	Data      *string    `json:"data"`
	UpdatedAt *time.Time `json:"updated_at"`
}

// RecordsResult is the result of requesting multiple records.
type RecordsResult struct {
	Records           []*Record `json:"records"`
	NextCursor        *string   `json:"-"`
	EncodedNextCursor *string   `json:"next_cursor,omitempty"`
}

// ContentAttribute represent one of all the possible values for an attribute
type ContentAttribute struct {
	OwnerChannelID string `dynamo:"ownerChannelID,hash"`      // Hash key
	ID             string `dynamo:"contentAttributeID,range"` // Sort key

	AttributeKey   string `dynamo:"attributeKey"`
	AttributeName  string `dynamo:"attributeName"`
	Value          string `dynamo:"value"`
	ValueShortname string `dynamo:"valueShortname"`

	ParentAttributeID  string `dynamo:"parentAttributeID"`
	ParentAttributeKey string `dynamo:"parentAttributeKey"`

	ImageURL string `dynamo:"imageURL"`

	ChildAttributeIDs []string `dynamo:"childAttributeIDs,set"`

	CreatedAt time.Time `dynamo:"createdAt"`
	UpdatedAt time.Time `dynamo:"updatedAt"`
}

// ContentAttributeImageUploadConfig has the upload configuration for content attribute images
type ContentAttributeImageUploadConfig struct {
	ChannelID string
	UploadID  string
	UploadURL string
	ImageURL  string
}

// Chanlet is the Dynamo record representing a child channel
type Chanlet struct {
	OwnerChannelID  string `dynamo:"ownerChannelID,hash"`                          // Hash key
	ChanletID       string `dynamo:"chanletID,range" index:"chanletID-Index,hash"` // Sort Key
	IsPublicChannel bool   `dynamo:"isPublicChannel"`                              // Whether this chanlet is also a channel connected to an actual user

	ContentAttributeIDs []string            `dynamo:"contentAttributeIDs,set"`
	ContentAttributes   []*ContentAttribute `dynamo:"-"` // not saved to Dynamo

	ArchivedDate time.Time `dynamo:"archivedDate"` // If this is non-zero, the chanlet is archived

	OwnerChanletAttributes *OwnerChanletAttributes `dynamo:"ownerChanletAttributes"` // Data applicable only for OwnerChanlets
}

// OwnerChanletAttributes stores attributes specific to owner chanlets (ownerChanletID == chanletID)
type OwnerChanletAttributes struct {
	ChanletsEnabled bool `dynamo:"chanletsEnabled"` // Whether the command center feature is turned on.
}

// IsOwnerChanlet returns whether the chanlet is an owner chanlet (OwnerChannelID and ChanletID are the same)
func (c *Chanlet) IsOwnerChanlet() bool {
	return c.OwnerChannelID == c.ChanletID
}

// Relation is a string alias for relation attribute in the table
type Relation string

const (
	// RelationParent is used when the RelatedChannelID is for a child channel.
	RelationParent Relation = "PARENT"
	// RelationBorrower is used when RelatedChannelID is a sibling channel that has Chanlets that are shared with this Channel.
	RelationBorrower Relation = "BORROWER"
)

// ChannelRelation is the Dynamo record
type ChannelRelation struct {
	ChannelID        string   `dynamo:"channelID,hash"`                                             // Hash key
	RelatedChannelID string   `dynamo:"relatedChannelID,range" index:"relatedChannelID-Index,hash"` // Sort key
	Relation         Relation `dynamo:"relation"`                                                   // Describes what the relation is for the Channel to the RelatedChannel: PARENT | BORROWER
}

// VODRestrictionReason is the parsed form of the conventional "Hedwig reason data" format, indicating the name of a
// product that a user must own in order to qualify to access some VOD. RestrictionType and Title are currently unused in Nioh.
type VODRestrictionReason struct {
	ProductName     string `json:"productName"`
	RestrictionType string `json:"restrictionType"`
	Title           string `json:"title"`
}

// ResourceAuthorization represents the common resource actions of checking a user's access eligibility
type ResourceAuthorization struct {
	// UserIsAuthorized indicates whether a user should be allowed to view the resource.
	UserIsAuthorized bool
	// ResourceIsRestricted indicates whether the VOD specified in Request is restricted at all.
	ResourceIsRestricted bool
	// RestrictionType indicates the restriction type if the resource has a restriction.
	RestrictionType RestrictionType
	// AppliedExemptions indicates which exemptions were applied to the resource/user pair.
	AppliedExemptions []*AppliedExemption
	// Any error encountered during resolving this resource's authorization.
	Error error
}

// VODAuthorizationRequest is an internal representation of a request to check a user's VOD access eligibility.
// Backward compatible with old hedwig path
type VODAuthorizationRequest struct {
	UserID         string
	CountryCode    string
	VideoID        string
	ChannelID      string
	ConsumePreview bool
	Reason         *VODRestrictionReason // Deprecated - if provided, the old hedwig path will be taken

	// An optional parameter used by free preview
	Origin string
}

// VODAuthorizationsRequest is an internal representation of a batch request to check a user's VOD access eligibility.
// NOT backward compatible with old hedwig path
type VODAuthorizationsRequest struct {
	UserID         string
	VideoIDs       []string
	ConsumePreview bool
}

// VODAuthorization represents the completed backend action of checking a user's VOD access eligibility.
type VODAuthorization struct {
	ResourceAuthorization
	// Request is the original request that prompted the backend to check the user's eligibility.
	Request *VODAuthorizationRequest
}

// ChannelAuthorizationRequest is an internal representation of a request to check a user's access to a channel resource.
type ChannelAuthorizationRequest struct {
	UserID         string
	CountryCode    string
	ChannelID      string
	ConsumePreview bool

	// An optional parameter used by free preview
	Origin string
}

// ChannelAuthorizationsRequest is an internal representation of a request to check a user's access to multiple channel resources.
type ChannelAuthorizationsRequest struct {
	UserID         string
	ChannelIDs     []string
	ConsumePreview bool
}

// ChannelAuthorization represents the completed backend action of checking a user's channel access.
type ChannelAuthorization struct {
	ResourceAuthorization
	// Request is the original request that prompted the backend to check the user's eligibility.
	Request *ChannelAuthorizationRequest
}

// IClock provides an interface for time-related functions
type IClock interface {
	Now() time.Time
}

// RealClock implements the IClock interface using real clocks
type RealClock struct{}

// Now returns the current time
func (c *RealClock) Now() time.Time {
	return time.Now()
}

const (
	// PreviewDuration indicates how long a preview grants access to a user from the moment the user consumes the preview.
	PreviewDuration = 5*time.Minute + 30*time.Second

	// ShortPreviewDuration is a special case PreviewDuration for how long a preview grants access to a user.
	ShortPreviewDuration = 30 * time.Second

	// PreviewCooldown indicates how long a user must wait after consuming a preview before being able to consume another one.
	PreviewCooldown = 8 * time.Hour
)

// Preview is the Dynamo record for a preview record
type Preview struct {
	PreviewKey       string    `dynamo:"previewKey,hash"`
	ResourceKey      string    `dynamo:"resourceKey"`
	UserID           string    `dynamo:"userID"`
	CreatedAt        time.Time `dynamo:"createdAt"`
	ContentExpiresAt time.Time `dynamo:"contentExpiresAt"`
	ResetAt          time.Time `dynamo:"resetAt"`
}

// IndefiniteEndDate is a Timestamp in the far future
var IndefiniteEndDate = time.Date(3000, time.January, 1, 0, 0, 0, 0, time.UTC)

// GetAuthorizedUsersForChannelCursor is cursor for GetAuthorizedUsersForChannel API sub call
type GetAuthorizedUsersForChannelCursor struct {
	Cursor     string
	IsComplete bool
}

// ServiceCursorKey Consts for GetAuthorizedUsersForChannelV2 sub calls
type ServiceCursorKey string

// ServiceCursorKey Consts for GetAuthorizedUsersForChannelV2 sub calls
const (
	VIPServiceCursor ServiceCursorKey = "VIP"
	ModServiceCursor ServiceCursorKey = "MOD"
	SubServiceCursor ServiceCursorKey = "SUB"
	OrgServiceCursor ServiceCursorKey = "ORG"
)

// GetAuthorizedUsersForChannelServiceOrder is for deterministic call order for GetAuthorizedUsersForChannelV2
var GetAuthorizedUsersForChannelServiceOrder = []ServiceCursorKey{
	VIPServiceCursor,
	ModServiceCursor,
	SubServiceCursor,
	OrgServiceCursor,
}
