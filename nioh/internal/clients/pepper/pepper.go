package pepper

import (
	"context"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/nioh/internal/clients/wrapper"
	"code.justin.tv/foundation/twitchclient"
	pepperTwirp "code.justin.tv/revenue/pepper/rpc"
	"code.justin.tv/sse/malachai/pkg/s2s/caller"
	"github.com/pkg/errors"
)

const (
	// ServiceName is the name of the service for stats display
	ServiceName           = "pepper"
	getUserRiskEvaluation = "GetUserRiskEvaluation"
)

// IPepperClient is the jax service client interface
//go:generate mockery --note @generated --name IPepperClient --output ./mocks
type IPepperClient interface {
	IsUserLowRisk(ctx context.Context, userID string) (bool, error)
}

// clientImpl is the implementation for pepper service client
type clientImpl struct {
	client pepperTwirp.Pepper
}

// NewPepperClient returns a new pepper client
func NewPepperClient(cfg twitchclient.ClientConf, s2sCallerName string) (IPepperClient, error) {
	s2sTransport, err := caller.S2STwirpTransport(
		s2sCallerName,
		nil,
		twitchclient.NewHTTPClient(cfg),
		logrus.New(),
	)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create S2S transport for Pepper client")
	}
	client := pepperTwirp.NewPepperProtobufClient(cfg.Host, s2sTransport)
	return &clientImpl{client}, nil
}

// IsUserLowRisk returns whether the use is low risk or not
func (c *clientImpl) IsUserLowRisk(ctx context.Context, userID string) (bool, error) {
	ctx = wrapper.WrapRequestContext(ctx, ServiceName, getUserRiskEvaluation)
	req := &pepperTwirp.GetUserRiskEvaluationRequest{
		UserId:         userID,
		EvaluationType: pepperTwirp.EvaluationType_LOW_RISK,
	}

	resp, err := c.client.GetUserRiskEvaluation(ctx, req)

	if err != nil {
		return false, errors.Wrap(err, "failed to get risk evaluation from Pepper")
	}

	return resp.Result == pepperTwirp.EvaluationResult_IS_LOW_RISK, nil
}
