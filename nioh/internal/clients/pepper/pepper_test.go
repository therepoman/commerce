package pepper

import (
	"context"
	"testing"

	"code.justin.tv/commerce/nioh/internal/clients/pepper/mocks"
	pepperTwirp "code.justin.tv/revenue/pepper/rpc"
	"github.com/pkg/errors"
	"github.com/stretchr/testify/mock"
)

func TestPepperClient(t *testing.T) {

	// Test conditions
	type input struct {
		pepperReturnsError bool
		pepperReturnsValue pepperTwirp.EvaluationResult
	}

	// Expected output
	type output struct {
		errorPresent bool
		value        bool
	}

	// Test cases
	tests := []struct {
		name   string
		input  input
		output output
	}{
		{
			name:   "pepper service error",
			input:  input{true, pepperTwirp.EvaluationResult_IS_LOW_RISK},
			output: output{true, false},
		},
		{
			name:   "pepper returns low risk ",
			input:  input{false, pepperTwirp.EvaluationResult_IS_LOW_RISK},
			output: output{false, true},
		},
		{
			name:   "pepper returns not low risk",
			input:  input{true, pepperTwirp.EvaluationResult_IS_NOT_LOW_RISK},
			output: output{false, false},
		},
	}

	// Test execution loop
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mockPepper := new(mocks.Pepper)
			if tt.input.pepperReturnsError {
				mockPepper.On("GetUserRiskEvaluation", mock.Anything, mock.Anything).Return(nil, errors.New("pepper asplode"))
			} else {
				mockPepper.On("GetUserRiskEvaluation", mock.Anything, mock.Anything).Return(&pepperTwirp.GetUserRiskEvaluationResponse{Result: tt.input.pepperReturnsValue}, nil)
			}

			client := &clientImpl{mockPepper}
			resp, err := client.IsUserLowRisk(context.Background(), "1")

			if tt.output.errorPresent && err == nil {
				t.Error("error is unexpectedly nil")
			}
			if tt.output.value != resp {
				t.Errorf("expected output is %v but got %v", tt.output.value, resp)
			}
		})
	}
}
