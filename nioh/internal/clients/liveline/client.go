package liveline

import (
	"context"
	"net/http"

	"code.justin.tv/commerce/nioh/internal/clients/wrapper"
	liveline "code.justin.tv/discovery/liveline/proto/liveline"
	"code.justin.tv/web/users-service/client"
	"github.com/pkg/errors"
)

const (
	// ServiceName is the name of the service for stats display
	ServiceName               = "liveline"
	getStreamByChannelIdsName = "GetStreamsByChannelIDs"
)

// ILivelineClient is the liveline service client interface
type ILivelineClient interface {
	GetStreamsByChannelIDs(ctx context.Context, ids []string) (*liveline.StreamsResponse, error)
}

// Client is the implementation for liveline service client
type Client struct {
	liveline liveline.Liveline
}

// NewLivelineClient returns a new LivelineClient
func NewLivelineClient(host string, httpClient *http.Client) ILivelineClient {
	client := liveline.NewLivelineProtobufClient(host, httpClient)
	return &Client{client}
}

// GetStreamsByChannelIDs returns stream information for the given channels
func (l *Client) GetStreamsByChannelIDs(ctx context.Context, ids []string) (*liveline.StreamsResponse, error) {
	ctx = wrapper.WrapRequestContext(ctx, ServiceName, getStreamByChannelIdsName)

	request := &liveline.StreamsByChannelIDsRequest{
		ChannelIds: ids,
	}

	resp, err := l.liveline.GetStreamsByChannelIDs(ctx, request)
	// Pass unwrapped error if not found so downstream can check with client.IsUserNotFound
	if client.IsUserNotFound(err) {
		return nil, err
	}
	if err != nil {
		return nil, errors.Wrap(err, "call to liveline.GetStreamsByChannelIDs failed")
	}

	return resp, nil
}
