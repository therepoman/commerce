package liveline

import (
	"context"
	"testing"

	"code.justin.tv/commerce/nioh/internal/clients/liveline/mocks"
	livelineTwirp "code.justin.tv/discovery/liveline/proto/liveline"
	"github.com/pkg/errors"
	"github.com/stretchr/testify/mock"
)

func TestLivelineClient(t *testing.T) {

	// Test conditions
	type input struct {
		livelineReturnsError bool
		livelineReturnsValue *livelineTwirp.StreamsResponse
	}

	// Expected output
	type output struct {
		errorPresent    bool
		responsePresent bool
	}

	// Test cases
	tests := []struct {
		name   string
		input  input
		output output
	}{
		{
			name:   "liveline service error",
			input:  input{true, nil},
			output: output{true, false},
		},
		{
			name:   "liveline returns stream response ",
			input:  input{false, &livelineTwirp.StreamsResponse{}},
			output: output{false, true},
		},
	}

	// Test execution loop
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mockLiveline := new(mocks.Liveline)
			if tt.input.livelineReturnsError {
				mockLiveline.On("GetStreamsByChannelIDs", mock.Anything, mock.Anything).Return(nil, errors.New("liveline asplode"))
			} else {
				mockLiveline.On("GetStreamsByChannelIDs", mock.Anything, mock.Anything).Return(tt.input.livelineReturnsValue, nil)
			}

			client := &Client{mockLiveline}
			// GetStreamsByChannelIDs returns all streams regardless of restriction type or lack thereof.
			resp, err := client.GetStreamsByChannelIDs(context.Background(), []string{"1"})

			if tt.output.errorPresent && err == nil {
				t.Error("error is unexpectedly nil")
			}
			if tt.output.responsePresent && resp == nil {
				t.Errorf("expected response to be not nil but was nil")
			}
		})
	}
}
