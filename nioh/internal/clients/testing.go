package clients

import (
	"testing"

	liveline_mocks "code.justin.tv/commerce/nioh/internal/clients/liveline/mocks"
	pepper_mocks "code.justin.tv/commerce/nioh/internal/clients/pepper/mocks"
	pubsub_mocks "code.justin.tv/commerce/nioh/internal/clients/pubsub/mocks"
	rbac_mocks "code.justin.tv/commerce/nioh/internal/clients/rbac/mocks"
	siosa_mocks "code.justin.tv/commerce/nioh/internal/clients/siosa/mocks"
	spade_mocks "code.justin.tv/commerce/nioh/internal/clients/spade/mocks"
	subs_mocks "code.justin.tv/commerce/nioh/internal/clients/subs/mocks"
	upload_mocks "code.justin.tv/commerce/nioh/internal/clients/upload/mocks"
	users_mocks "code.justin.tv/commerce/nioh/internal/clients/users/mocks"
	video_mocks "code.justin.tv/commerce/nioh/internal/clients/video/mocks"
	vodapi_mocks "code.justin.tv/commerce/nioh/internal/clients/vodapi/mocks"
	voyager_mocks "code.justin.tv/commerce/nioh/internal/clients/voyager/mocks"
	zuma_mocks "code.justin.tv/commerce/nioh/internal/clients/zuma/mocks"
)

// MockClients exposes the concrete mocks of a test Clients instance so that they can be configured for testing calls
// and asserted against.
type MockClients struct {
	Subscriptions     *subs_mocks.Client
	TwitchVoyager     *voyager_mocks.Client
	UsersClient       *users_mocks.IUsersClient
	Spade             *spade_mocks.Client
	ChannelProperties *video_mocks.IChannelProperties
	SiosaClient       *siosa_mocks.ISiosaClient
	Pubsub            *pubsub_mocks.IPubsubClient
	Liveline          *liveline_mocks.ILivelineClient
	UploadService     *upload_mocks.IUploadServiceClient
	Pepper            *pepper_mocks.IPepperClient
	Zuma              *zuma_mocks.Client
	Rbac              *rbac_mocks.Client
	VodAPI            *vodapi_mocks.Client
}

// InitMockClients returns an instance of *Clients backed by mock dependencies, as well as a *MockClients instance
// that exposes the concrete mocks underlying the *Clients instance.
func InitMockClients() (*Clients, *MockClients) {
	mockSubs := new(subs_mocks.Client)
	mockVoyager := new(voyager_mocks.Client)
	mockSiosa := new(siosa_mocks.ISiosaClient)
	mockUsers := new(users_mocks.IUsersClient)
	mockChannelProperties := new(video_mocks.IChannelProperties)
	mockSpade := new(spade_mocks.Client)
	mockPubsub := new(pubsub_mocks.IPubsubClient)
	mockLiveline := new(liveline_mocks.ILivelineClient)
	mockUploadService := new(upload_mocks.IUploadServiceClient)
	mockPepper := new(pepper_mocks.IPepperClient)
	mockZuma := new(zuma_mocks.Client)
	mockRbac := new(rbac_mocks.Client)
	mockVodAPI := new(vodapi_mocks.Client)
	// TODO: what to do about Cache?

	return &Clients{
			ChannelProperties: mockChannelProperties,
			Pubsub:            mockPubsub,
			SiosaClient:       mockSiosa,
			Subscriptions:     mockSubs,
			TwitchVoyager:     mockVoyager,
			Spade:             mockSpade,
			UploadService:     mockUploadService,
			UsersClient:       mockUsers,
			Liveline:          mockLiveline,
			Pepper:            mockPepper,
			Zuma:              mockZuma,
			Rbac:              mockRbac,
			VodAPI:            mockVodAPI,
		}, &MockClients{
			ChannelProperties: mockChannelProperties,
			Pubsub:            mockPubsub,
			SiosaClient:       mockSiosa,
			Subscriptions:     mockSubs,
			TwitchVoyager:     mockVoyager,
			Spade:             mockSpade,
			UploadService:     mockUploadService,
			UsersClient:       mockUsers,
			Liveline:          mockLiveline,
			Pepper:            mockPepper,
			Zuma:              mockZuma,
			Rbac:              mockRbac,
			VodAPI:            mockVodAPI,
		}
}

// AssertExpectations delegates AssertExpectations to the constituent mock instances in MockClients.
func (m *MockClients) AssertExpectations(t *testing.T) {
	m.Subscriptions.AssertExpectations(t)
	m.TwitchVoyager.AssertExpectations(t)
	m.UsersClient.AssertExpectations(t)
	m.Spade.AssertExpectations(t)
	m.SiosaClient.AssertExpectations(t)
	m.ChannelProperties.AssertExpectations(t)
	m.Liveline.AssertExpectations(t)
	m.UploadService.AssertExpectations(t)
	m.Pubsub.AssertExpectations(t)
	m.Zuma.AssertExpectations(t)
	m.Rbac.AssertExpectations(t)
	m.VodAPI.AssertExpectations(t)
}
