package users

import (
	"context"
	"fmt"
	"math"
	"strconv"
	"time"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/nioh/internal/clients/wrapper"
	"code.justin.tv/foundation/twitchclient"
	"code.justin.tv/web/users-service/client"
	channelsClient "code.justin.tv/web/users-service/client/channels"
	"code.justin.tv/web/users-service/client/usersclient_internal"
	usersModels "code.justin.tv/web/users-service/models"
	"github.com/pkg/errors"
)

const (
	// ServiceName is the name of the service for stats display
	ServiceName = "users"

	getUserAPIName      = "GetUser"
	createUserAPIName   = "CreateUser"
	patchChannelAPIName = "PatchChannel"
)

// IUsersClient is the users service client interface
type IUsersClient interface {
	GetUserByID(ctx context.Context, userID string) (*usersModels.Properties, error)
	IsUserStaff(ctx context.Context, userID string) (bool, error)
	IsUserSiteAdmin(ctx context.Context, userID string) (bool, error)
	CreateHiddenUser(ctx context.Context, login string) (*usersModels.Properties, error)
	UpdateStreamTitle(ctx context.Context, channelID string, title string) error
}

// Client is the implementation for users service client
type Client struct {
	InternalClient usersclient_internal.InternalClient
	ChannelsClient channelsClient.Client
}

// NewUsersClient returns a new UsersClient
func NewUsersClient(cfg twitchclient.ClientConf) (IUsersClient, error) {
	client, err := usersclient_internal.NewClient(cfg)
	if err != nil {
		return nil, errors.Wrap(err, "Failed to initialize internal Users client")
	}

	channelsClient, err := channelsClient.NewClient(cfg)
	if err != nil {
		return nil, errors.Wrap(err, "Failed to initialize Users channels client")
	}

	return &Client{client, channelsClient}, nil
}

// GetUserByID returns a user given ID
func (u *Client) GetUserByID(ctx context.Context, userID string) (*usersModels.Properties, error) {
	ctx = wrapper.WrapRequestContext(ctx, ServiceName, getUserAPIName)

	resp, err := u.InternalClient.GetUserByID(ctx, userID, nil)
	// Pass unwrapped error if not found so downstream can check with client.IsUserNotFound
	if client.IsUserNotFound(err) {
		return nil, err
	}
	if err != nil {
		return nil, errors.Wrap(err, "Call to UsersService.GetUserByID failed")
	}

	return resp, nil
}

// IsUserStaff returns boolean representing whether the given user is staff or not
func (u *Client) IsUserStaff(ctx context.Context, userID string) (bool, error) {
	userProperties, err := u.GetUserByID(ctx, userID)
	if err != nil {
		return false, err
	}

	if userProperties.Admin == nil {
		return false, nil
	}

	return *userProperties.Admin, nil
}

// IsUserSiteAdmin returns boolean representing whether the given user is a site admin or not
func (u *Client) IsUserSiteAdmin(ctx context.Context, userID string) (bool, error) {
	userProperties, err := u.GetUserByID(ctx, userID)
	if err != nil {
		return false, err
	}

	if userProperties.Subadmin == nil {
		return false, nil
	}

	return *userProperties.Subadmin, nil
}

// CreateHiddenUser creates a new user and sets DirectoryHidden to true
func (u *Client) CreateHiddenUser(ctx context.Context, login string) (*usersModels.Properties, error) {
	ctx = wrapper.WrapRequestContext(ctx, ServiceName, createUserAPIName)

	userModel := usersModels.CreateUserProperties{
		Login: login,
		Email: fmt.Sprintf("%s@twitch.tv", login), // Fake Email
		Birthday: usersModels.Birthday{ // Fake BDAY
			Day:   1,
			Month: 1,
			Year:  1980,
		},
		SkipNameValidation: true,
	}

	res, err := u.InternalClient.CreateUser(ctx, &userModel, nil)

	if err != nil {
		return nil, errors.Wrap(err, "users service client failed to create a user")
	}

	idInt, err := strconv.ParseUint(res.ID, 10, 64)
	if err != nil {
		return nil, errors.Wrap(err, "users service returned an unexpected user ID")
	}

	// Sever context tree to avoid canceling when handler returns
	go u.setDirectoryHidden(context.TODO(), idInt)

	return res, nil
}

// UpdateStreamTitle overides the title field for a given channel
func (u *Client) UpdateStreamTitle(ctx context.Context, channelID string, title string) error {
	if channelID == "" {
		return errors.New("channelID is required")
	}

	ctx = wrapper.WrapRequestContext(ctx, ServiceName, patchChannelAPIName)

	idInt, err := strconv.ParseUint(channelID, 10, 64)
	if err != nil {
		return errors.Wrap(err, "cannot parse provided channel ID as a int")
	}

	prop := usersModels.UpdateChannelProperties{
		ID:     idInt,
		Status: &title, // status is the stream title
	}

	err = u.ChannelsClient.Set(ctx, prop, nil)
	if err != nil {
		return errors.Wrap(err, "failed to update channel title in user services")
	}

	return nil
}

func (u *Client) setDirectoryHidden(ctx context.Context, userID uint64) {
	ctxDirectoryHidden := wrapper.WrapRequestContext(ctx, ServiceName, patchChannelAPIName)
	trueValue := true

	channelPropertiesModel := usersModels.UpdateChannelProperties{
		ID:              userID,
		DirectoryHidden: &trueValue,
	}

	maxAttempt := 4
	for i := 0; i < maxAttempt; i++ {
		delay := 250 * math.Pow(2, float64(i)) // x1, x2, x4, x8
		time.Sleep(time.Duration(delay) * time.Millisecond)
		err := u.ChannelsClient.Set(ctxDirectoryHidden, channelPropertiesModel, nil)
		// If the "No properties found" error is returned, wait a bit to allow the user creation info to propagate and try again
		if IsNoPropertiesFound(err) {
			if i == maxAttempt+1 {
				logrus.WithFields(logrus.Fields{
					"userID": userID,
				}).Error("All retry attempts are exhausted. Failed to set a chanlet as hidden.")
			} else {
				logrus.WithFields(logrus.Fields{
					"attempt_i": i,
					"userID":    userID,
				}).Warn("No channel properties were found while creating chanlet. Retrying update...")
			}
		} else {
			if err != nil {
				// Is it okay to leave the created user in an orphaned state? Users Service does not allow deletion of users without Admin credentials.
				logrus.WithError(err).Error("users service client failed to set user as hidden")
			}
			break
		}
	}
}
