package users_test

import (
	goctx "context" // "context" collides with an unexported type in "convey"
	"testing"
	"time"

	"code.justin.tv/commerce/nioh/internal/clients/users"
	internalUsersMocks "code.justin.tv/commerce/nioh/mocks/users"
	usersModels "code.justin.tv/web/users-service/models"

	"code.justin.tv/web/users-service/models"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestUsersClient(t *testing.T) {
	Convey("Test UsersClient", t, func() {
		ctx := goctx.Background()
		mockInternalClient := new(internalUsersMocks.InternalClient)
		mockChannelClient := new(internalUsersMocks.Client)
		usersClient := users.Client{
			InternalClient: mockInternalClient,
			ChannelsClient: mockChannelClient,
		}

		Convey("Test GetUserByID", func() {
			Convey("happy case", func() {
				mockInternalClient.On("GetUserByID", mock.Anything, mock.Anything, mock.Anything).
					Return(&models.Properties{}, nil)

				_, err := usersClient.GetUserByID(ctx, "123")
				So(err, ShouldBeNil)
			})

			Convey("with error from InternalClient", func() {
				mockInternalClient.On("GetUserByID", mock.Anything, mock.Anything, mock.Anything).
					Return(nil, errors.New("test"))

				_, err := usersClient.GetUserByID(ctx, "123")
				So(err, ShouldNotBeNil)
			})
		})

		Convey("Test IsUserStaff", func() {
			Convey("happy case", func() {
				trueBool := true
				mockInternalClient.On("GetUserByID", mock.Anything, mock.Anything, mock.Anything).
					Return(&models.Properties{
						Admin: &trueBool,
					}, nil)

				isStaff, err := usersClient.IsUserStaff(ctx, "123")
				So(err, ShouldBeNil)
				So(isStaff, ShouldBeTrue)
			})

			Convey("with nil field from InternalClient", func() {
				mockInternalClient.On("GetUserByID", mock.Anything, mock.Anything, mock.Anything).
					Return(&models.Properties{}, nil)

				isStaff, err := usersClient.IsUserStaff(ctx, "123")
				So(err, ShouldBeNil)
				So(isStaff, ShouldBeFalse)
			})

			Convey("with error from InternalClient", func() {
				mockInternalClient.On("GetUserByID", mock.Anything, mock.Anything, mock.Anything).
					Return(nil, errors.New("test"))

				isStaff, err := usersClient.IsUserStaff(ctx, "123")
				So(err, ShouldNotBeNil)
				So(isStaff, ShouldBeFalse)
			})
		})

		Convey("Test IsUserSiteAdmin", func() {
			Convey("happy case", func() {
				trueBool := true
				mockInternalClient.On("GetUserByID", mock.Anything, mock.Anything, mock.Anything).
					Return(&models.Properties{
						Subadmin: &trueBool,
					}, nil)

				isSiteAdmin, err := usersClient.IsUserSiteAdmin(ctx, "123")
				So(err, ShouldBeNil)
				So(isSiteAdmin, ShouldBeTrue)
			})

			Convey("with nil field from InternalClient", func() {
				mockInternalClient.On("GetUserByID", mock.Anything, mock.Anything, mock.Anything).
					Return(&models.Properties{}, nil)

				isSiteAdmin, err := usersClient.IsUserSiteAdmin(ctx, "123")
				So(err, ShouldBeNil)
				So(isSiteAdmin, ShouldBeFalse)
			})

			Convey("with error from InternalClient", func() {
				mockInternalClient.On("GetUserByID", mock.Anything, mock.Anything, mock.Anything).
					Return(nil, errors.New("test"))

				isSiteAdmin, err := usersClient.IsUserSiteAdmin(ctx, "123")
				So(err, ShouldNotBeNil)
				So(isSiteAdmin, ShouldBeFalse)
			})
		})

		Convey("Test CreateHiddenUser", func() {
			Convey("success", func() {
				mockInternalClient.On("CreateUser", mock.Anything, mock.Anything, mock.Anything).Return(&models.Properties{
					ID: "1234",
				}, nil)
				mockChannelClient.On("Set", mock.Anything, mock.Anything, mock.Anything).Return(nil)
				res, err := usersClient.CreateHiddenUser(ctx, "helloworld")
				So(res, ShouldNotBeNil)
				So(err, ShouldBeNil)
			})

			Convey("users service error", func() {
				mockInternalClient.On("CreateUser", mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("test"))
				res, err := usersClient.CreateHiddenUser(ctx, "helloworld")
				So(res, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})

			Convey("users channels service 'no properties found' error twice", func() {
				mockInternalClient.On("CreateUser", mock.Anything, mock.Anything, mock.Anything).Return(&models.Properties{
					ID: "1234",
				}, nil)
				mockChannelClient.On("Set", mock.Anything, mock.Anything, mock.Anything).Return(&users.NoPropertiesFoundError{}).Twice()
				mockChannelClient.On("Set", mock.Anything, mock.Anything, mock.Anything).Return(nil)
				res, err := usersClient.CreateHiddenUser(ctx, "helloworld")
				So(res, ShouldNotBeNil)
				So(err, ShouldBeNil)
				time.Sleep(2000 * time.Millisecond) // wait for go routine to finish
				mockChannelClient.AssertNumberOfCalls(t, "Set", 3)
			})

			Convey("users channels service 'no properties found' error perpetually", func() {
				mockInternalClient.On("CreateUser", mock.Anything, mock.Anything, mock.Anything).Return(&models.Properties{
					ID: "1234",
				}, nil)
				mockChannelClient.On("Set", mock.Anything, mock.Anything, mock.Anything).Return(&users.NoPropertiesFoundError{})
				res, err := usersClient.CreateHiddenUser(ctx, "helloworld")
				So(res, ShouldNotBeNil)
				So(err, ShouldBeNil)
				time.Sleep(4000 * time.Millisecond)                // wait for go routine to finish
				mockChannelClient.AssertNumberOfCalls(t, "Set", 4) // total attempts
			})

			Convey("users channels service set channel properties error", func() {
				mockInternalClient.On("CreateUser", mock.Anything, mock.Anything, mock.Anything).Return(&models.Properties{
					ID: "1234",
				}, nil)
				mockChannelClient.On("Set", mock.Anything, mock.Anything, mock.Anything).Return(errors.New("unknown error")).Once()
				res, err := usersClient.CreateHiddenUser(ctx, "helloworld")
				So(res, ShouldNotBeNil)
				So(err, ShouldBeNil)
				time.Sleep(500 * time.Millisecond) // wait for go routine to finish
				mockChannelClient.AssertNumberOfCalls(t, "Set", 1)
			})
		})

		Convey("Test UpdateStreamTitle", func() {
			channelID := "123"
			channelIDInt := 123
			title := "new title"

			Convey("with empty channel ID", func() {
				err := usersClient.UpdateStreamTitle(ctx, "", title)
				So(err, ShouldNotBeNil)
			})

			Convey("with valid request", func() {
				mockChannelClient.On("Set", mock.Anything, mock.Anything, mock.Anything).Return(nil)
				err := usersClient.UpdateStreamTitle(ctx, channelID, title)
				So(err, ShouldBeNil)
				arg := mockChannelClient.Calls[0].Arguments[1].(usersModels.UpdateChannelProperties)
				So(arg.ID, ShouldEqual, channelIDInt)
				So(arg.Status, ShouldNotBeNil)
				So(*arg.Status, ShouldEqual, title)
			})

			Convey("with users channel service error", func() {
				mockChannelClient.On("Set", mock.Anything, mock.Anything, mock.Anything).Return(errors.New("you are not groot"))
				err := usersClient.UpdateStreamTitle(ctx, channelID, title)
				So(err, ShouldNotBeNil)
			})
		})
	})
}
