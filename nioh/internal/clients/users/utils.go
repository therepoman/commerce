package users

// NoPropertiesFoundError is an error for when the channels client returns the "No properties found" error when attempting to update user channel properties
// Obtained from: https://git-aws.internal.justin.tv/web/users-service/blob/master/backend/util/utils.go
type NoPropertiesFoundError struct{}

func (e *NoPropertiesFoundError) Error() string {
	return "No properties found for this user identifier"
}

// IsNoPropertiesFound returns whether the specified error is the channels client "No properties found" error
func IsNoPropertiesFound(err error) bool {
	return err != nil && err.Error() == (&NoPropertiesFoundError{}).Error()
}
