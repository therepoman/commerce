package rbac

import (
	"context"
	"testing"

	configMocks "code.justin.tv/commerce/nioh/config/mocks"
	mocks "code.justin.tv/commerce/nioh/internal/clients/rbac/mocks/vendormocks"
	"code.justin.tv/devrel/devsite-rbac/rpc/rbacrpc"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/twitchtv/twirp"
)

const (
	testCompanyID = "test_company_id"
	testUserID    = "test_user_id"
)

var testCompany = &rbacrpc.Company{
	Id:                testCompanyID,
	CompanyName:       "test_company_name",
	Url:               "testURL",
	Type:              1,
	VhsContractSigned: false,
	CampaignsEnabled:  false,
	Identifier:        "test_company",
	CurseCompanyId:    3,
	CreatedAt:         "2018-04-10 23:58:21.387",
}
var testMembership = &rbacrpc.CompanyMembership{
	Company:             testCompany,
	Role:                "Shadow_Account",
	SelfCanLeave:        false,
	SelfCanAddMember:    false,
	SelfCanRemoveMember: false,
	JoinedAt:            "2018-04-10 23:58:21.387",
}
var testCtx = context.Background()

func setupTestClient(disableRBACUserQuery bool) (Client, *mocks.RBAC) {
	mockRBAC := &mocks.RBAC{}
	mockDynamicCfg := &configMocks.IDynamicConfiguration{}
	mockDynamicCfg.On("IsRBACUserQueryDisabled").Return(disableRBACUserQuery, nil)
	return &clientImpl{
		rbac:          mockRBAC,
		dynamicConfig: mockDynamicCfg,
	}, mockRBAC
}

func TestClient_ValidateUserMembershipInCompanies(t *testing.T) {
	testClient, mockRBAC := setupTestClient(false)
	mockRBAC.On("ValidateByTwitchID", mock.Anything, mock.Anything).Return(&rbacrpc.ValidateResponse{
		Valid: true,
	}, nil)

	validationMap, err := testClient.ValidateUserMembershipInCompanies(testCtx, testUserID, []string{testCompanyID})
	assert.Nil(t, err)
	assert.NotEmpty(t, validationMap)
	assert.True(t, validationMap[testCompanyID])
}

func TestClient_ValidateUserMembershipInCompanies_Error(t *testing.T) {
	testClient, mockRBAC := setupTestClient(false)
	mockRBAC.On("ValidateByTwitchID", mock.Anything, mock.Anything).Return(nil, twirp.NewError(twirp.Internal, "unknown error"))

	validationMap, err := testClient.ValidateUserMembershipInCompanies(testCtx, testUserID, []string{testCompanyID})
	assert.NotNil(t, err)
	assert.Empty(t, validationMap)
}

func TestClient_ValidateUserMembershipInCompanies_DisabledRBACUserQuery(t *testing.T) {
	testClient, mockRBAC := setupTestClient(true)
	validationMap, err := testClient.ValidateUserMembershipInCompanies(testCtx, testUserID, []string{testCompanyID})
	assert.Nil(t, err)
	assert.Empty(t, validationMap)
	mockRBAC.AssertNotCalled(t, "ValidateByTwitchID")
}

func TestClient_ListCompanyIDsForUser(t *testing.T) {
	testClient, mockRBAC := setupTestClient(false)
	mockRBAC.On("ListCompanyMemberships", mock.Anything, mock.Anything).Return(&rbacrpc.ListCompanyMembershipsResponse{
		Memberships: []*rbacrpc.CompanyMembership{testMembership},
	}, nil)

	companyIDs, err := testClient.ListCompanyIDsForUser(testCtx, testUserID)
	assert.Nil(t, err)
	assert.NotEmpty(t, companyIDs)
	assert.Equal(t, []string{testCompanyID}, companyIDs)
}

func TestClient_ListCompanyIDsForUser_EmptyCompany(t *testing.T) {
	testClient, mockRBAC := setupTestClient(false)
	mockRBAC.On("ListCompanyMemberships", mock.Anything, mock.Anything).Return(&rbacrpc.ListCompanyMembershipsResponse{
		Memberships: []*rbacrpc.CompanyMembership{},
	}, nil)

	companyIDs, err := testClient.ListCompanyIDsForUser(testCtx, testUserID)
	assert.NotNil(t, err)
	assert.IsType(t, OrganizationNotFoundError{}, err)
	assert.Empty(t, companyIDs)
}

func TestClient_ListCompanyIDsForUser_Error(t *testing.T) {
	testClient, mockRBAC := setupTestClient(false)
	mockRBAC.On("ListCompanyMemberships", mock.Anything, mock.Anything).Return(nil, twirp.NewError(twirp.Internal, "unknown error"))

	companyIDs, err := testClient.ListCompanyIDsForUser(testCtx, testUserID)
	assert.NotNil(t, err)
	assert.EqualError(t, err, "twirp error internal: unknown error")
	assert.Empty(t, companyIDs)
}

func TestClient_ListUsersByCompanyID_Error(t *testing.T) {
	testClient, mockRBAC := setupTestClient(false)
	mockRBAC.On("GetUsersByCompanyId", mock.Anything, mock.Anything).Return(nil, twirp.NewError(twirp.Internal, "unknown error"))

	users, nextOffset, isComplete, err := testClient.ListUsersByCompanyID(testCtx, testUserID, testCompanyID, "0", int32(0))
	assert.NotNil(t, err)
	assert.EqualError(t, err, "twirp error internal: unknown error")
	assert.Empty(t, users)
	assert.Equal(t, "", nextOffset)
	assert.False(t, isComplete)
}

func TestClient_ListUsersByCompanyID(t *testing.T) {
	testClient, mockRBAC := setupTestClient(false)
	testCompanyID := "5fdbf7a7-6249-44d3-98d8-27800169df69"
	testTwitchID := "21561321"
	mockRBAC.On("GetUsersByCompanyId", mock.Anything, mock.Anything).Return(
		&rbacrpc.GetUsersByCompanyIdResponse{
			Id:     testCompanyID,
			Total:  1,
			Offset: 0,
			Memberships: []*rbacrpc.Membership{
				{
					TwitchId:   testTwitchID,
					CompanyId:  testCompanyID,
					Role:       "Owner",
					DevTitle:   "Developer",
					CreatedAt:  "2018-05-24 13:39:31.36",
					ModifiedAt: "2018-08-24 16:23:18.30",
					LastName:   "test_last_name",
					FirstName:  "test_first_name",
					DevEmail:   "testemail@test.com",
				},
			},
		}, nil,
	)
	users, nextOffset, isComplete, err := testClient.ListUsersByCompanyID(testCtx, testUserID, testCompanyID, "0", int32(0))
	assert.Nil(t, err)
	assert.True(t, isComplete)
	assert.Equal(t, "1", nextOffset)
	assert.Equal(t, 1, len(users))
	assert.Equal(t, testTwitchID, users[0])
}

func TestClient_ListUsersByCompanyID_EmptyResponse(t *testing.T) {
	testClient, mockRBAC := setupTestClient(false)
	testCompanyID := "5fdbf7a7-6249-44d3-98d8-27800169df69"
	mockRBAC.On("GetUsersByCompanyId", mock.Anything, mock.Anything).Return(
		&rbacrpc.GetUsersByCompanyIdResponse{
			Id:          testCompanyID,
			Total:       0,
			Offset:      0,
			Memberships: []*rbacrpc.Membership{},
		}, nil,
	)
	users, nextOffset, isComplete, err := testClient.ListUsersByCompanyID(testCtx, testUserID, testCompanyID, "0", int32(0))
	assert.Nil(t, err)
	assert.True(t, isComplete)
	assert.Equal(t, "0", nextOffset)
	assert.Empty(t, users)
}

func TestClient_ListUsersByCompanyID_DisabledRBACUserQuery(t *testing.T) {
	testClient, mockRBAC := setupTestClient(true)
	testCompanyID := "5fdbf7a7-6249-44d3-98d8-27800169df69"
	users, nextOffset, isComplete, err := testClient.ListUsersByCompanyID(testCtx, testUserID, testCompanyID, "0", int32(0))
	assert.Nil(t, err)
	assert.True(t, isComplete)
	assert.Equal(t, "", nextOffset)
	assert.Equal(t, []string{testUserID}, users)
	mockRBAC.AssertNotCalled(t, "GetUsersByCompanyId")
}
