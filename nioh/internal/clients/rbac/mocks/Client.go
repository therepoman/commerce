// Code generated by mockery 2.9.0. DO NOT EDIT.

// @generated

package mocks

import (
	context "context"

	mock "github.com/stretchr/testify/mock"
)

// Client is an autogenerated mock type for the Client type
type Client struct {
	mock.Mock
}

// ListCompanyIDsForUser provides a mock function with given fields: ctx, userID
func (_m *Client) ListCompanyIDsForUser(ctx context.Context, userID string) ([]string, error) {
	ret := _m.Called(ctx, userID)

	var r0 []string
	if rf, ok := ret.Get(0).(func(context.Context, string) []string); ok {
		r0 = rf(ctx, userID)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]string)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string) error); ok {
		r1 = rf(ctx, userID)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// ListUsersByCompanyID provides a mock function with given fields: ctx, channelID, companyID, offset, limit
func (_m *Client) ListUsersByCompanyID(ctx context.Context, channelID string, companyID string, offset string, limit int32) ([]string, string, bool, error) {
	ret := _m.Called(ctx, channelID, companyID, offset, limit)

	var r0 []string
	if rf, ok := ret.Get(0).(func(context.Context, string, string, string, int32) []string); ok {
		r0 = rf(ctx, channelID, companyID, offset, limit)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]string)
		}
	}

	var r1 string
	if rf, ok := ret.Get(1).(func(context.Context, string, string, string, int32) string); ok {
		r1 = rf(ctx, channelID, companyID, offset, limit)
	} else {
		r1 = ret.Get(1).(string)
	}

	var r2 bool
	if rf, ok := ret.Get(2).(func(context.Context, string, string, string, int32) bool); ok {
		r2 = rf(ctx, channelID, companyID, offset, limit)
	} else {
		r2 = ret.Get(2).(bool)
	}

	var r3 error
	if rf, ok := ret.Get(3).(func(context.Context, string, string, string, int32) error); ok {
		r3 = rf(ctx, channelID, companyID, offset, limit)
	} else {
		r3 = ret.Error(3)
	}

	return r0, r1, r2, r3
}

// ValidateUserMembershipInCompanies provides a mock function with given fields: ctx, userID, companyIDs
func (_m *Client) ValidateUserMembershipInCompanies(ctx context.Context, userID string, companyIDs []string) (map[string]bool, error) {
	ret := _m.Called(ctx, userID, companyIDs)

	var r0 map[string]bool
	if rf, ok := ret.Get(0).(func(context.Context, string, []string) map[string]bool); ok {
		r0 = rf(ctx, userID, companyIDs)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(map[string]bool)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string, []string) error); ok {
		r1 = rf(ctx, userID, companyIDs)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}
