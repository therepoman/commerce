package rbac

import (
	"fmt"
)

// OrganizationNotFoundError is a type implementing `error` that signals to a caller that the owner
// of the channel doesn't join in any organization.
type OrganizationNotFoundError struct {
	message string
}

func (o OrganizationNotFoundError) Error() string {
	return o.message
}

// NewOrganizationNotFoundError returns a new error interface implemented by OrganizationNotFoundError type.
func NewOrganizationNotFoundError(channelID string) error {
	return OrganizationNotFoundError{
		message: fmt.Sprintf("The owner of the channel %s is not found in any organization.", channelID),
	}
}
