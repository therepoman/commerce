package rbac

import (
	"context"
	"strconv"
	"sync"

	niohConfig "code.justin.tv/commerce/nioh/config"
	rbacPermissions "code.justin.tv/devrel/devsite-rbac/models/permissions"
	"code.justin.tv/devrel/devsite-rbac/rpc/rbacrpc"
	"code.justin.tv/foundation/twitchclient"

	"code.justin.tv/commerce/logrus"
	multierror "github.com/hashicorp/go-multierror"
	"github.com/pkg/errors"
)

const (
	// ServiceName is the dimension of the rbac service appearing in stats/metrics
	ServiceName = "rbac"

	defaultQueryLimit uint64 = 1000
)

// Client is RBAC client wrapper
type Client interface {
	ValidateUserMembershipInCompanies(ctx context.Context, userID string, companyIDs []string) (map[string]bool, error)
	ListCompanyIDsForUser(ctx context.Context, userID string) ([]string, error)
	ListUsersByCompanyID(ctx context.Context, channelID string, companyID string, offset string, limit int32) ([]string, string, bool, error)
}

type clientImpl struct {
	rbac          rbacrpc.RBAC
	dynamicConfig niohConfig.IDynamicConfiguration
}

// NewClient initializes a new RBAC client
func NewClient(host string, dynamicCfg niohConfig.IDynamicConfiguration, conf twitchclient.ClientConf) Client {
	value, err := dynamicCfg.IsRBACUserQueryDisabled()
	logrus.Infof("Dynamic config: %v, %v\n", value, err)
	return &clientImpl{
		rbac:          rbacrpc.NewRBACProtobufClient(host, twitchclient.NewHTTPClient(conf)),
		dynamicConfig: dynamicCfg,
	}
}

func (c *clientImpl) ValidateUserMembershipInCompanies(ctx context.Context, userID string, companyIDs []string) (map[string]bool, error) {
	if disabled, err := c.dynamicConfig.IsRBACUserQueryDisabled(); err != nil || disabled {
		return nil, err
	}

	wg := &sync.WaitGroup{}
	mutex := &sync.Mutex{}
	errs := make(map[string]error, len(companyIDs))
	validationMap := make(map[string]bool, len(companyIDs))

	isUserInCompany := func(userID string, companyID string) {
		defer wg.Done()
		resp, err := c.rbac.ValidateByTwitchID(ctx, &rbacrpc.ValidateQuery{
			UserId:       userID,
			ResourceId:   companyID,
			ResourceType: rbacPermissions.Company,
			Permission:   rbacPermissions.ShowCompanyMembers,
		})
		if err != nil {
			mutex.Lock()
			errs[companyID] = err
			mutex.Unlock()
			return
		}

		if resp != nil {
			mutex.Lock()
			validationMap[companyID] = resp.Valid
			mutex.Unlock()
		}
	}

	for _, companyID := range companyIDs {
		wg.Add(1)
		go isUserInCompany(userID, companyID)
	}
	wg.Wait()

	var multierr *multierror.Error
	for companyID, err := range errs {
		if err != nil {
			multierr = multierror.Append(multierr, errors.Wrap(err, "failed to get memberships for Company: "+companyID))
		}
	}

	if multierr != nil {
		return nil, multierr.ErrorOrNil()
	}

	return validationMap, nil
}

func (c *clientImpl) ListCompanyIDsForUser(ctx context.Context, userID string) ([]string, error) {
	// this is not user query so this call will not be gated by dynamic config
	resp, err := c.rbac.ListCompanyMemberships(ctx, &rbacrpc.ListCompanyMembershipsRequest{
		TwitchId: userID,
	})
	if err != nil {
		return nil, err
	}

	if resp == nil || len(resp.Memberships) == 0 {
		return nil, NewOrganizationNotFoundError(userID)
	}

	var companyIDs = make([]string, len(resp.Memberships))
	for idx, membership := range resp.Memberships {
		companyIDs[idx] = membership.Company.Id
	}

	return companyIDs, nil
}

// ListUsersByCompanyID returns company user IDs, next offset to use, isComplete, and error
func (c *clientImpl) ListUsersByCompanyID(ctx context.Context, channelID string, companyID string, offset string, limit int32) ([]string, string, bool, error) {
	if disabled, err := c.dynamicConfig.IsRBACUserQueryDisabled(); err != nil || disabled {
		return []string{channelID}, "", true, nil
	}

	offsetUint64, err := strconv.ParseUint(offset, 10, 64)
	if err != nil {
		return nil, "", false, err
	}
	var appliedLimit = uint64(limit)
	if appliedLimit == 0 {
		appliedLimit = defaultQueryLimit
	}

	resp, err := c.rbac.GetUsersByCompanyId(ctx, &rbacrpc.GetUsersByCompanyIdRequest{
		Id:     companyID,
		Limit:  appliedLimit,
		Offset: offsetUint64,
		SortBy: "created_at",
	})
	if err != nil {
		return nil, "", false, err
	}

	if resp == nil || len(resp.Memberships) == 0 {
		return nil, offset, true, nil
	}

	var userIDs = make([]string, len(resp.Memberships))
	for idx, membership := range resp.Memberships {
		userIDs[idx] = membership.TwitchId
	}
	nextOffset := offsetUint64 + uint64(len(resp.Memberships))
	isComplete := uint64(len(resp.Memberships)) < appliedLimit

	return userIDs, strconv.FormatUint(nextOffset, 10), isComplete, nil
}
