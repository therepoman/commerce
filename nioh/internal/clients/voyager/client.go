package voyager

import (
	"context"
	"net/http"

	"code.justin.tv/commerce/nioh/internal/clients/wrapper"

	voyager "code.justin.tv/amzn/TwitchVoyagerTwirp"
)

// Client wraps a voyager.TwitchVoyager client, providing versions of its methods.
type Client interface {
	GetUserProductSubscription(ctx context.Context, userID string, productID string) (*voyager.Subscription, error)
	GetUserProductSubscriptions(ctx context.Context, userID string, productIDs []string) ([]*voyager.Subscription, error)
	GetUserChannelSubscription(ctx context.Context, userID string, channelID string) (*voyager.Subscription, error)
	GetSubscribedUsersForChannel(ctx context.Context, channelID string, limit int32, cursor string) (map[string]string, string, error)
}

type clientImpl struct {
	voyagerSubs voyager.TwitchVoyager
}

const (
	// ServiceName is dimension of the voyager service appearing in stats/metrics
	ServiceName = "twitchVoyager"

	getUserProductSubscriptionsAPIName = "GetUserProductSubscriptions"
	getUserChannelSubscriptionAPIName  = "GetUserChannelSubscription"
	getChannelSubscriptionsAPIName     = "GetChannelSubscriptions"
)

var _ Client = &clientImpl{}

// NewClient instantiates a Client configured with the passed TwitchVoyager service host as `address` and an HTTP timeout.
func NewClient(voyagerAddress string, voyagerHttpClient *http.Client) Client {
	voyagerClient := voyager.NewTwitchVoyagerProtobufClient(voyagerAddress, voyagerHttpClient)

	return &clientImpl{
		voyagerSubs: voyagerClient,
	}
}

func (c *clientImpl) GetUserProductSubscription(ctx context.Context, userID, productID string) (*voyager.Subscription, error) {
	ctx = wrapper.WrapRequestContext(ctx, ServiceName, getUserProductSubscriptionsAPIName)
	resp, err := c.voyagerSubs.GetUserProductSubscriptions(ctx, &voyager.GetUserProductSubscriptionsRequest{
		UserId:     userID,
		ProductIds: []string{productID},
	})
	if err != nil {
		return nil, err
	}

	var subscription *voyager.Subscription
	if len(resp.Subscriptions) > 0 {
		subscription = resp.Subscriptions[0]
	}

	return subscription, nil
}
func (c *clientImpl) GetUserProductSubscriptions(ctx context.Context, userID string, productIDs []string) ([]*voyager.Subscription, error) {
	ctx = wrapper.WrapRequestContext(ctx, ServiceName, getUserProductSubscriptionsAPIName)
	resp, err := c.voyagerSubs.GetUserProductSubscriptions(ctx, &voyager.GetUserProductSubscriptionsRequest{
		UserId:     userID,
		ProductIds: productIDs,
	})
	if err != nil {
		return nil, err
	}

	return resp.Subscriptions, nil
}

func (c *clientImpl) GetUserChannelSubscription(ctx context.Context, userID string, channelID string) (*voyager.Subscription, error) {
	ctx = wrapper.WrapRequestContext(ctx, ServiceName, getUserChannelSubscriptionAPIName)
	resp, err := c.voyagerSubs.GetUserChannelSubscription(ctx, &voyager.GetUserChannelSubscriptionRequest{
		UserId:    userID,
		ChannelId: channelID,
	})
	if err != nil {
		return nil, err
	}

	return resp.Subscription, nil
}

// GetSubscribedUsersForChannel fetches a paginated list of userIDs that represent subscribers to the given channel.
// Note: This maybe outdated when subs service starts to support a query with ticket productIDs instead of channelID.
func (c *clientImpl) GetSubscribedUsersForChannel(ctx context.Context, channelID string, limit int32, cursor string) (map[string]string, string, error) {
	ctx = wrapper.WrapRequestContext(ctx, ServiceName, getChannelSubscriptionsAPIName)
	req := &voyager.GetChannelSubscriptionsRequest{
		ChannelId: channelID,
		Limit:     int64(limit),
		Cursor:    cursor,
	}

	res, err := c.voyagerSubs.GetChannelSubscriptions(ctx, req)

	if err != nil {
		return nil, "", err
	}

	subbedIDs := make(map[string]string, len(res.Subscriptions))
	for _, sub := range res.Subscriptions {
		subbedIDs[sub.UserId] = sub.ProductId
	}

	return subbedIDs, res.Cursor, nil
}
