// Code generated by mockery 2.9.0. DO NOT EDIT.

// @generated

package mocks

import (
	context "context"

	TwitchVoyagerTwirp "code.justin.tv/amzn/TwitchVoyagerTwirp"

	mock "github.com/stretchr/testify/mock"
)

// Client is an autogenerated mock type for the Client type
type Client struct {
	mock.Mock
}

// GetSubscribedUsersForChannel provides a mock function with given fields: ctx, channelID, limit, cursor
func (_m *Client) GetSubscribedUsersForChannel(ctx context.Context, channelID string, limit int32, cursor string) (map[string]string, string, error) {
	ret := _m.Called(ctx, channelID, limit, cursor)

	var r0 map[string]string
	if rf, ok := ret.Get(0).(func(context.Context, string, int32, string) map[string]string); ok {
		r0 = rf(ctx, channelID, limit, cursor)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(map[string]string)
		}
	}

	var r1 string
	if rf, ok := ret.Get(1).(func(context.Context, string, int32, string) string); ok {
		r1 = rf(ctx, channelID, limit, cursor)
	} else {
		r1 = ret.Get(1).(string)
	}

	var r2 error
	if rf, ok := ret.Get(2).(func(context.Context, string, int32, string) error); ok {
		r2 = rf(ctx, channelID, limit, cursor)
	} else {
		r2 = ret.Error(2)
	}

	return r0, r1, r2
}

// GetUserChannelSubscription provides a mock function with given fields: ctx, userID, channelID
func (_m *Client) GetUserChannelSubscription(ctx context.Context, userID string, channelID string) (*TwitchVoyagerTwirp.Subscription, error) {
	ret := _m.Called(ctx, userID, channelID)

	var r0 *TwitchVoyagerTwirp.Subscription
	if rf, ok := ret.Get(0).(func(context.Context, string, string) *TwitchVoyagerTwirp.Subscription); ok {
		r0 = rf(ctx, userID, channelID)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*TwitchVoyagerTwirp.Subscription)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string, string) error); ok {
		r1 = rf(ctx, userID, channelID)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetUserProductSubscription provides a mock function with given fields: ctx, userID, productID
func (_m *Client) GetUserProductSubscription(ctx context.Context, userID string, productID string) (*TwitchVoyagerTwirp.Subscription, error) {
	ret := _m.Called(ctx, userID, productID)

	var r0 *TwitchVoyagerTwirp.Subscription
	if rf, ok := ret.Get(0).(func(context.Context, string, string) *TwitchVoyagerTwirp.Subscription); ok {
		r0 = rf(ctx, userID, productID)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*TwitchVoyagerTwirp.Subscription)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string, string) error); ok {
		r1 = rf(ctx, userID, productID)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetUserProductSubscriptions provides a mock function with given fields: ctx, userID, productIDs
func (_m *Client) GetUserProductSubscriptions(ctx context.Context, userID string, productIDs []string) ([]*TwitchVoyagerTwirp.Subscription, error) {
	ret := _m.Called(ctx, userID, productIDs)

	var r0 []*TwitchVoyagerTwirp.Subscription
	if rf, ok := ret.Get(0).(func(context.Context, string, []string) []*TwitchVoyagerTwirp.Subscription); ok {
		r0 = rf(ctx, userID, productIDs)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]*TwitchVoyagerTwirp.Subscription)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string, []string) error); ok {
		r1 = rf(ctx, userID, productIDs)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}
