package voyager

import (
	"context"
	"testing"

	voyager "code.justin.tv/amzn/TwitchVoyagerTwirp"
	"code.justin.tv/commerce/nioh/internal/clients/voyager/mocks"
	"github.com/pkg/errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func mock_GetUserProductSubscriptions(userID, productID string, subscription *voyager.Subscription, err error) *mocks.TwitchVoyager {
	m := new(mocks.TwitchVoyager)

	response := &voyager.GetUserProductSubscriptionsResponse{}
	if subscription != nil {
		response.Subscriptions = []*voyager.Subscription{subscription}
	}

	m.On("GetUserProductSubscriptions", mock.Anything, &voyager.GetUserProductSubscriptionsRequest{
		UserId:     userID,
		ProductIds: []string{productID},
	}).Return(response, err)

	return m
}

func mockGetUserChannelSubscription(userID, channelID string, subscription *voyager.Subscription, err error) *mocks.TwitchVoyager {
	m := new(mocks.TwitchVoyager)

	response := &voyager.GetUserChannelSubscriptionResponse{}
	if subscription != nil {
		response.Subscription = subscription
	}

	m.On("GetUserChannelSubscription", mock.Anything, &voyager.GetUserChannelSubscriptionRequest{
		UserId:    userID,
		ChannelId: channelID,
	}).Return(response, err)

	return m
}

func mock_GetSubscribedUsersForChannel(cursor string, channelID string, subscriptions []*voyager.Subscription, err error) *mocks.TwitchVoyager {
	m := new(mocks.TwitchVoyager)

	response := &voyager.GetChannelSubscriptionsResponse{}
	if subscriptions != nil || len(subscriptions) == 0 {
		response.Subscriptions = subscriptions
		response.Cursor = cursor
	}

	m.On("GetChannelSubscriptions", mock.Anything, mock.Anything).Return(response, err)

	return m
}

func TestClient_GetUserProductSubscription(t *testing.T) {
	type testmocks struct {
		twitchVoyager *mocks.Client
	}

	testUserID := "my-cool-user-id"
	testProductID := "my-cool-product-id"
	testSubscription := &voyager.Subscription{
		Id:        "test-sub-id",
		UserId:    testUserID,
		ProductId: testProductID,
	}

	testCases := []struct {
		description string
		client      voyager.TwitchVoyager
		want        *voyager.Subscription
		wantErr     bool
	}{
		{
			description: "when subscription is found",
			client:      mock_GetUserProductSubscriptions(testUserID, testProductID, testSubscription, nil),
			want:        testSubscription,
			wantErr:     false,
		},
		{
			description: "when subscription not found",
			client:      mock_GetUserProductSubscriptions(testUserID, testProductID, nil, nil),
			want:        nil,
			wantErr:     false,
		},
		{
			description: "when subs returns an error",
			client:      mock_GetUserProductSubscriptions(testUserID, testProductID, nil, errors.New("kerblammo")),
			want:        nil,
			wantErr:     true,
		},
	}

	for _, tt := range testCases {
		t.Run(tt.description, func(t *testing.T) {
			client := &clientImpl{voyagerSubs: tt.client}
			got, err := client.GetUserProductSubscription(context.Background(), testUserID, testProductID)
			assert.Equal(t, tt.want, got)
			assert.Equal(t, tt.wantErr, err != nil)
		})
	}
}

func TestClient_GetUserProductSubscriptions(t *testing.T) {
	type testmocks struct {
		twitchVoyager *mocks.Client
	}

	testUserID := "my-super-cool-user-id"
	testProductID := "my-super-cool-product-id"
	testSubscription := &voyager.Subscription{
		Id:        "test-sub-id",
		UserId:    testUserID,
		ProductId: testProductID,
	}
	testSubscriptions := []*voyager.Subscription{testSubscription}

	testCases := []struct {
		description string
		client      voyager.TwitchVoyager
		want        []*voyager.Subscription
		wantErr     bool
	}{
		{
			description: "when subscription is found",
			client:      mock_GetUserProductSubscriptions(testUserID, testProductID, testSubscription, nil),
			want:        testSubscriptions,
			wantErr:     false,
		},
		{
			description: "when subscription not found",
			client:      mock_GetUserProductSubscriptions(testUserID, testProductID, nil, nil),
			want:        nil,
			wantErr:     false,
		},
		{
			description: "when subs returns an error",
			client:      mock_GetUserProductSubscriptions(testUserID, testProductID, nil, errors.New("kerblammo")),
			want:        nil,
			wantErr:     true,
		},
	}

	for _, tt := range testCases {
		t.Run(tt.description, func(t *testing.T) {
			client := &clientImpl{voyagerSubs: tt.client}
			got, err := client.GetUserProductSubscriptions(context.Background(), testUserID, []string{testProductID})
			assert.Equal(t, tt.want, got)
			assert.Equal(t, tt.wantErr, err != nil)
		})
	}
}
func TestClient_GetSubscribedUsersForChannel(t *testing.T) {
	type testmocks struct {
		twitchVoyager *mocks.Client
	}

	testSubscriberID1 := "546"
	testSubscriberID2 := "534522"
	testProductID := "1337"
	channelID := "123"
	cursor := "wow!"
	testSubscriptions := []*voyager.Subscription{
		{
			UserId:    testSubscriberID1,
			ProductId: testProductID,
		},
		{
			UserId:    testSubscriberID2,
			ProductId: testProductID,
		},
	}

	testCases := []struct {
		description string
		client      voyager.TwitchVoyager
		want        map[string]string
		wantBoolean bool
		wantErr     bool
	}{
		{
			description: "when subs returns an error",
			client:      mock_GetSubscribedUsersForChannel(cursor, channelID, nil, errors.New("error")),
			want:        nil,
			wantBoolean: false,
			wantErr:     true,
		},
		{
			description: "when subscription returns a response",
			client:      mock_GetSubscribedUsersForChannel(cursor, channelID, testSubscriptions, nil),
			want: map[string]string{
				testSubscriberID1: testProductID,
				testSubscriberID2: testProductID,
			},
			wantBoolean: true,
			wantErr:     false,
		},
	}

	for _, tt := range testCases {
		t.Run(tt.description, func(t *testing.T) {
			client := &clientImpl{voyagerSubs: tt.client}
			got, _, err := client.GetSubscribedUsersForChannel(context.Background(), channelID, 20, "")
			assert.Equal(t, tt.wantBoolean, got != nil)
			assert.Equalf(t, tt.want, got, "error message %s", "formatted")
			assert.Equal(t, tt.wantErr, err != nil)
		})
	}

}

func TestClient_GetUserChannelSubscriptions(t *testing.T) {
	type testmocks struct {
		twitchVoyager *mocks.Client
	}

	testUserID := "my-cool-user-id"
	testChannelID := "my-cool-channel-id"
	testSubscription := &voyager.Subscription{
		Id:        "test-sub-id",
		UserId:    testUserID,
		ChannelId: testChannelID,
	}

	testCases := []struct {
		description string
		client      voyager.TwitchVoyager
		want        *voyager.Subscription
		wantErr     bool
	}{
		{
			description: "when subscription is found",
			client:      mockGetUserChannelSubscription(testUserID, testChannelID, testSubscription, nil),
			want:        testSubscription,
			wantErr:     false,
		},
		{
			description: "when subscription not found",
			client:      mockGetUserChannelSubscription(testUserID, testChannelID, nil, nil),
			want:        nil,
			wantErr:     false,
		},
		{
			description: "when subs returns an error",
			client:      mockGetUserChannelSubscription(testUserID, testChannelID, nil, errors.New("kerblammo")),
			want:        nil,
			wantErr:     true,
		},
	}

	for _, tt := range testCases {
		t.Run(tt.description, func(t *testing.T) {
			client := &clientImpl{voyagerSubs: tt.client}
			got, err := client.GetUserChannelSubscription(context.Background(), testUserID, testChannelID)
			assert.Equal(t, tt.want, got)
			assert.Equal(t, tt.wantErr, err != nil)
		})
	}
}
