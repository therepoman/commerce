package pubsub

import (
	"context"
	"encoding/json"
	"testing"

	"code.justin.tv/commerce/nioh/internal/clients/pubsub/mocks"
	"code.justin.tv/commerce/nioh/internal/models"
	"code.justin.tv/commerce/nioh/internal/testdata"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestPubsubClient(t *testing.T) {
	Convey("Test PubsubClient", t, func() {
		mockPubClient := new(mocks.PubClient)
		pubsubClient := pubsubClient{
			pubClient: mockPubClient,
		}
		Convey("PublishChanletUpdate", func() {
			attributeIDs := testdata.ContentAttributeIDs()
			attributes := testdata.ContentAttributes()
			chanlet := &models.Chanlet{
				OwnerChannelID:      "123",
				ChanletID:           "456",
				ContentAttributeIDs: attributeIDs,
				ContentAttributes:   attributes,
			}
			channelIDs := []string{"123", "223", "323"}

			Convey("when pubClient succeeds", func() {
				mockPubClient.On("Publish", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)
				err := pubsubClient.PublishChanletUpdate(context.Background(), channelIDs, chanlet)
				So(err, ShouldBeNil)

				Convey("should send out parsable message", func() {
					marshalledMsg := mockPubClient.Calls[0].Arguments[2].(string)
					var entity ChanletEntity
					err = json.Unmarshal([]byte(marshalledMsg), &entity)

					So(err, ShouldBeNil)
					So(entity.Type, ShouldEqual, chanletType)
					So(entity.Chanlet.ContentAttributes, ShouldHaveLength, len(attributes))

					for i, actual := range entity.Chanlet.ContentAttributes {
						So(actual.OwnerChannel.ID, ShouldEqual, attributes[i].OwnerChannelID)
						So(actual.ID, ShouldEqual, attributes[i].ID)
						So(actual.Key, ShouldEqual, attributes[i].AttributeKey)
						So(actual.Value, ShouldEqual, attributes[i].Value)
						So(actual.ValueShortName, ShouldEqual, attributes[i].ValueShortname)
						So(actual.ImageURL, ShouldEqual, attributes[i].ImageURL)
						So(actual.CreatedAt, ShouldEqual, attributes[i].CreatedAt)
						So(actual.UpdatedAt, ShouldEqual, attributes[i].UpdatedAt)
					}
				})
			})

			Convey("when pubClient fails", func() {
				mockPubClient.On("Publish", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(errors.New("can't pub, can't sub"))
				err := pubsubClient.PublishChanletUpdate(context.Background(), channelIDs, chanlet)
				So(err, ShouldNotBeNil)
			})

			mockPubClient.AssertNumberOfCalls(t, "Publish", 1)
		})
	})
}
