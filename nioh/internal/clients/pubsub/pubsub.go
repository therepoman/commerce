package pubsub

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	pubClient "code.justin.tv/chat/pubsub-go-pubclient/client"
	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/nioh/internal/clients/wrapper"
	"code.justin.tv/commerce/nioh/internal/models"
	"code.justin.tv/foundation/twitchclient"
	"github.com/pkg/errors"
)

const (
	// ServiceName is dimension of the subscription service appearing in stats/metrics
	ServiceName    = "Pubsub"
	publishAPIName = "Publish"

	chanletUpdateTopic = "multiview-chanlet-update.%s"
	chanletType        = "chanlet"
)

// IPubsubClient is the pubsub client interface
type IPubsubClient interface {
	PublishChanletUpdate(ctx context.Context, channelIDs []string, chanlet *models.Chanlet) error
}

type pubsubClient struct {
	pubClient pubClient.PubClient
}

// ChanletEntity is the pubsub message for chanlet updates
type ChanletEntity struct {
	Type    string  `json:"type"`
	Chanlet Chanlet `json:"chanlet"`
}

// Chanlet is the pubsub message representation of a chanlet
type Chanlet struct {
	ID                string              `json:"id"`
	ContentAttributes []*ContentAttribute `json:"contentAttributes"`
}

// ContentAttribute is the pubsub message representation of a content attribute
type ContentAttribute struct {
	OwnerChannel   Channel   `json:"ownerChannel"`
	ID             string    `json:"id"`
	Key            string    `json:"key"`
	Name           string    `json:"name"`
	Value          string    `json:"value"`
	ValueShortName string    `json:"valueShortName,omitempty"`
	ParentID       string    `json:"parentID,omitempty"`
	ParentKey      string    `json:"parentKey,omitempty"`
	ImageURL       string    `json:"imageURL,omitempty"`
	ChildIDs       []string  `json:"childIDs,omitempty"`
	CreatedAt      time.Time `json:"createdAt"`
	UpdatedAt      time.Time `json:"updatedAt"`
}

// Channel is the pubsub message representation of a channel
type Channel struct {
	ID string `json:"id"`
}

// NewPubsubClient initialize a new client based on provided configurations
func NewPubsubClient(cfg twitchclient.ClientConf) (IPubsubClient, error) {
	client, err := pubClient.NewPubClient(cfg)
	if err != nil {
		return nil, err
	}

	return &pubsubClient{
		pubClient: client,
	}, nil
}

// PublishChanletUpdate publishes chanlet update message to all channels in the provided channelIDs list
func (c *pubsubClient) PublishChanletUpdate(ctx context.Context, channelIDs []string, chanlet *models.Chanlet) error {
	ctx = wrapper.WrapRequestContext(ctx, ServiceName, publishAPIName)

	if len(channelIDs) == 0 {
		return errors.New("at least 1 channel ID is required")
	}
	if chanlet == nil {
		return errors.New("chanlet is required")
	}

	topics := make([]string, 0, len(channelIDs))
	for _, id := range channelIDs {
		topics = append(topics, fmt.Sprintf(chanletUpdateTopic, id))
	}

	marshalled, err := json.Marshal(ChanletEntity{
		Type:    chanletType,
		Chanlet: *chanletToPubsub(chanlet),
	})
	if err != nil {
		return errors.Wrap(err, "failed to marshal pubsub message for chanlet update")
	}

	logrus.WithFields(logrus.Fields{
		"ChannelIDs": channelIDs,
		"Data":       string(marshalled),
	}).Debug("Publishing chanlet updates to pubsub")
	return c.pubClient.Publish(ctx, topics, string(marshalled), nil)
}

func chanletToPubsub(chanlet *models.Chanlet) *Chanlet {
	return &Chanlet{
		ID:                chanlet.ChanletID,
		ContentAttributes: contentAttributesToPubsub(chanlet.ContentAttributes),
	}
}

func contentAttributesToPubsub(attributes []*models.ContentAttribute) []*ContentAttribute {
	results := make([]*ContentAttribute, 0, len(attributes))
	for _, attribute := range attributes {
		results = append(results, contentAttributeToPubsub(attribute))
	}
	return results
}

func contentAttributeToPubsub(attribute *models.ContentAttribute) *ContentAttribute {
	return &ContentAttribute{
		OwnerChannel:   Channel{ID: attribute.OwnerChannelID},
		ID:             attribute.ID,
		Key:            attribute.AttributeKey,
		Name:           attribute.AttributeName,
		Value:          attribute.Value,
		ValueShortName: attribute.ValueShortname,
		ParentKey:      attribute.ParentAttributeKey,
		ParentID:       attribute.ParentAttributeID,
		ChildIDs:       attribute.ChildAttributeIDs,
		ImageURL:       attribute.ImageURL,
		CreatedAt:      attribute.CreatedAt,
		UpdatedAt:      attribute.UpdatedAt,
	}
}
