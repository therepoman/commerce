package zuma

import (
	"context"
	"errors"
	"testing"
	"time"

	"code.justin.tv/foundation/twitchclient"

	"code.justin.tv/chat/zuma/app/api"
	mocks "code.justin.tv/commerce/nioh/internal/clients/zuma/mocks/vendormocks"
	"code.justin.tv/commerce/nioh/internal/testdata"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func TestClient_GetUserRole(t *testing.T) {
	type testmocks struct {
		zuma *mocks.Client
	}

	testQuery := UserRoleQuery{
		ChannelID: testdata.Channel1ID,
		UserID:    testdata.Channel2ID,
	}

	tests := []struct {
		name        string
		apiResponse api.UserRoleResponse
		apiError    error
		want        UserRole
		wantErr     bool
	}{
		{
			name: "user has no role",
			apiResponse: api.UserRoleResponse{
				Role: "",
			},
			want:    UserRole(""),
			wantErr: false,
		},
		{
			name: "user is VIP",
			apiResponse: api.UserRoleResponse{
				Role: api.UserRoleVIP,
			},
			want:    UserRoleVIP,
			wantErr: false,
		},
		{
			name: "user is mod",
			apiResponse: api.UserRoleResponse{
				Role: api.UserRoleModerator,
			},
			want:    UserRoleModerator,
			wantErr: false,
		},
		{
			name:     "error from Zuma",
			apiError: errors.New("your zuma asplode"),
			want:     UserRole(""),
			wantErr:  true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tm := testmocks{new(mocks.Client)}
			tm.zuma.On("GetUserRoles", mock.Anything, api.UserRoleRequest(testQuery), (*twitchclient.ReqOpts)(nil)).
				Return(tt.apiResponse, tt.apiError)

			c := client{tm.zuma}
			got, gotErr := c.GetUserRole(context.TODO(), testQuery)
			assert.Equal(t, tt.wantErr, gotErr != nil)
			assert.Equal(t, tt.want, got)
		})
	}
}

func TestClient_ListMods(t *testing.T) {
	type testmocks struct {
		zuma *mocks.Client
	}

	testTime := time.Now()

	tests := []struct {
		name        string
		apiResponse api.ListModsV2Response
		apiError    error
		wantUsers   []string
		wantCursor  string
		wantErr     bool
	}{
		{
			name: "channel has no mods",
			apiResponse: api.ListModsV2Response{
				ModResults: []api.ModResult{},
			},
			wantUsers:  nil,
			wantCursor: "",
			wantErr:    false,
		},
		{
			name: "some mod retreived",
			apiResponse: api.ListModsV2Response{
				ModResults: []api.ModResult{
					{
						Mod: api.Mod{
							UserID:    "123",
							CreatedAt: &testTime,
						},
						Cursor: "321",
					},
				},
			},
			wantUsers:  []string{"123"},
			wantCursor: "321",
			wantErr:    false,
		},
		{
			name: "last mod retreived",
			apiResponse: api.ListModsV2Response{
				ModResults: []api.ModResult{
					{
						Mod: api.Mod{
							UserID:    "123",
							CreatedAt: &testTime,
						},
						Cursor: "",
					},
				},
			},
			wantUsers:  []string{"123"},
			wantCursor: "",
			wantErr:    false,
		},
		{
			name:       "error from Zuma",
			apiError:   errors.New("zuma r ded"),
			wantUsers:  nil,
			wantCursor: "",
			wantErr:    true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tm := testmocks{new(mocks.Client)}
			tm.zuma.On("ListModsV2", mock.Anything, mock.Anything, mock.Anything, mock.Anything, (*twitchclient.ReqOpts)(nil)).
				Return(tt.apiResponse, tt.apiError)

			c := client{tm.zuma}
			gotUsers, gotCursor, gotErr := c.ListMods(context.TODO(), testdata.Channel1ID, "", 1)
			assert.Equal(t, tt.wantUsers, gotUsers)
			assert.Equal(t, tt.wantCursor, gotCursor)
			assert.Equal(t, tt.wantErr, gotErr != nil)
		})
	}
}

func TestClient_ListVIPs(t *testing.T) {
	type testmocks struct {
		zuma *mocks.Client
	}

	testTime := time.Now()

	tests := []struct {
		name        string
		apiResponse api.ListVIPsResponse
		apiError    error
		wantUsers   []string
		wantCursor  string
		wantErr     bool
	}{
		{
			name: "channel has no VIPs",
			apiResponse: api.ListVIPsResponse{
				VIPs: []api.VIPResult{},
			},
			wantUsers:  []string{},
			wantCursor: "",
			wantErr:    false,
		},
		{
			name: "some VIPs retreived",
			apiResponse: api.ListVIPsResponse{
				VIPs: []api.VIPResult{
					{
						VIP: api.VIP{
							UserID:    "123",
							GrantedAt: &testTime,
						},
						Cursor: "321",
					},
				},
			},
			wantUsers:  []string{"123"},
			wantCursor: "321",
			wantErr:    false,
		},
		{
			name: "last VIPs retreived",
			apiResponse: api.ListVIPsResponse{
				VIPs: []api.VIPResult{
					{
						VIP: api.VIP{
							UserID:    "123",
							GrantedAt: &testTime,
						},
						Cursor: "",
					},
				},
			},
			wantUsers:  []string{"123"},
			wantCursor: "",
			wantErr:    false,
		},
		{
			name:       "error from Zuma",
			apiError:   errors.New("zuma r ded"),
			wantUsers:  []string{},
			wantCursor: "",
			wantErr:    true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tm := testmocks{new(mocks.Client)}
			tm.zuma.On("ListVIPs", mock.Anything, mock.Anything, mock.Anything, mock.Anything, (*twitchclient.ReqOpts)(nil)).
				Return(tt.apiResponse, tt.apiError)

			c := client{tm.zuma}
			gotUsers, gotCursor, gotErr := c.ListVIPs(context.TODO(), testdata.Channel1ID, "", 1)
			assert.Equal(t, tt.wantUsers, gotUsers)
			assert.Equal(t, tt.wantCursor, gotCursor)
			assert.Equal(t, tt.wantErr, gotErr != nil)
		})
	}
}
