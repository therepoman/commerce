// Only allow usage of these mutations in integration builds. Do not remove this build tag. Do not mutate data in production.
// +build integration

package zuma

import "context"

type IntegrationClient interface {
	Client
	AddMod(ctx context.Context, channelID, targetUserID string) error
	RemoveMod(ctx context.Context, channelID, targetUserID string) error
	AddVIP(ctx context.Context, channelID, targetUserID string) error
	RemoveVIP(ctx context.Context, channelID, targetUserID string) error
}

// ConvertToIntegrationClient upcasts a Client to an IntegrationClient.
func ConvertToIntegrationClient(c Client) (IntegrationClient, bool) {
	raw, ok := c.(*client)
	if !ok {
		return nil, false
	}
	return raw, true
}

func (c *client) AddMod(ctx context.Context, channelID, targetUserID string) error {
	_, err := c.Client.AddMod(ctx, channelID, targetUserID, channelID, nil)
	return err
}

func (c *client) RemoveMod(ctx context.Context, channelID, targetUserID string) error {
	_, err := c.Client.RemoveMod(ctx, channelID, targetUserID, channelID, nil)
	return err
}

func (c *client) AddVIP(ctx context.Context, channelID, targetUserID string) error {
	_, err := c.Client.AddVIP(ctx, channelID, targetUserID, channelID, nil)
	return err
}

func (c *client) RemoveVIP(ctx context.Context, channelID, targetUserID string) error {
	_, err := c.Client.RemoveVIP(ctx, channelID, targetUserID, channelID, nil)
	return err
}
