package zuma

import (
	"context"

	"code.justin.tv/commerce/nioh/internal/clients/wrapper"

	"code.justin.tv/chat/zuma/app/api"

	zuma "code.justin.tv/chat/zuma/client"
	"code.justin.tv/foundation/twitchclient"
)

const (
	// ServiceName ...
	ServiceName = "zuma"
)

// UserRoleQuery is the data required to query for a user's role on a specified channel.
type UserRoleQuery api.UserRoleRequest

// UserRole allows for type-safe, Nioh-internal forwarding of the Zuma API constants for these roles.
type UserRole string

const (
	// UserRoleVIP represents the role of channel VIP in responses from GetUserRole.
	UserRoleVIP UserRole = api.UserRoleVIP
	// UserRoleModerator represents the role of channel moderator in responses from GetUserRole.
	UserRoleModerator UserRole = api.UserRoleModerator
)

//go:generate mockery --note @generated --name Client

// Client wraps Zuma APIs to expose useful methods about user roles.
type Client interface {
	GetUserRole(ctx context.Context, query UserRoleQuery) (UserRole, error)
	ListMods(ctx context.Context, channelID, cursor string, limit int) ([]string, string, error)
	ListVIPs(ctx context.Context, channelID, cursor string, limit int) ([]string, string, error)
}

// NewClient returns a Client ready for use.
func NewClient(conf twitchclient.ClientConf) (Client, error) {
	c, err := zuma.NewClient(conf)
	if err != nil {
		return nil, err
	}
	return &client{c}, err
}

type client struct {
	zuma.Client
}

func (c *client) GetUserRole(ctx context.Context, query UserRoleQuery) (UserRole, error) {
	ctx = wrapper.WrapRequestContext(ctx, ServiceName, "GetUserRoles")
	res, err := c.Client.GetUserRoles(ctx, api.UserRoleRequest(query), nil)
	return UserRole(res.Role), err
}

func (c *client) ListMods(ctx context.Context, channelID, cursor string, limit int) ([]string, string, error) {
	ctx = wrapper.WrapRequestContext(ctx, ServiceName, "ListModsV2")
	res, err := c.Client.ListModsV2(ctx, channelID, cursor, limit, nil)
	var users []string
	for _, mod := range res.ModResults {
		users = append(users, mod.Mod.UserID)
	}
	nextCursor := ""
	if len(res.ModResults) > 0 {
		nextCursor = res.ModResults[len(res.ModResults)-1].Cursor
	}
	return users, nextCursor, err
}

func (c *client) ListVIPs(ctx context.Context, channelID, cursor string, limit int) ([]string, string, error) {
	ctx = wrapper.WrapRequestContext(ctx, ServiceName, "ListVIPs")
	res, err := c.Client.ListVIPs(ctx, channelID, cursor, limit, nil)
	users := []string{}
	for _, mod := range res.VIPs {
		users = append(users, mod.VIP.UserID)
	}
	nextCursor := ""
	if len(res.VIPs) > 0 {
		nextCursor = res.VIPs[len(res.VIPs)-1].Cursor
	}
	return users, nextCursor, err
}
