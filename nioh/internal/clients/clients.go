package clients

import (
	"net/http"
	"time"

	"code.justin.tv/commerce/nioh/config"
	niohConfig "code.justin.tv/commerce/nioh/config"
	"code.justin.tv/commerce/nioh/internal/clients/liveline"
	"code.justin.tv/commerce/nioh/internal/clients/pepper"
	"code.justin.tv/commerce/nioh/internal/clients/pubsub"
	"code.justin.tv/commerce/nioh/internal/clients/rbac"
	"code.justin.tv/commerce/nioh/internal/clients/siosa"
	spadeWrapper "code.justin.tv/commerce/nioh/internal/clients/spade"
	"code.justin.tv/commerce/nioh/internal/clients/subs"
	"code.justin.tv/commerce/nioh/internal/clients/upload"
	"code.justin.tv/commerce/nioh/internal/clients/users"
	"code.justin.tv/commerce/nioh/internal/clients/video"
	"code.justin.tv/commerce/nioh/internal/clients/vodapi"
	"code.justin.tv/commerce/nioh/internal/clients/voyager"
	"code.justin.tv/commerce/nioh/internal/clients/wrapper"
	"code.justin.tv/commerce/nioh/internal/clients/zuma"
	"code.justin.tv/common/spade-client-go/spade"
	"code.justin.tv/foundation/twitchclient"

	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/cep21/circuit"
	"github.com/patrickmn/go-cache"
	"github.com/pkg/errors"
)

// Clients exposes methods to use other clients
type Clients struct {
	InMemoryCache     *cache.Cache
	UsersClient       users.IUsersClient
	Spade             spade.Client
	ChannelProperties video.IChannelProperties
	Subscriptions     subs.Client
	TwitchVoyager     voyager.Client
	Pubsub            pubsub.IPubsubClient
	SiosaClient       siosa.ISiosaClient
	Liveline          liveline.ILivelineClient
	UploadService     upload.IUploadServiceClient
	Pepper            pepper.IPepperClient
	Zuma              zuma.Client
	Rbac              rbac.Client
	VodAPI            vodapi.Client
}

// SetupClients returns a new Clients
func SetupClients(cfg *config.Configuration, stats statsd.Statter, cm *circuit.Manager, dynamicCfg niohConfig.IDynamicConfiguration, awsSession *session.Session) (*Clients, error) {
	// In Memory Cache
	inMemoryCache := cache.New(
		time.Minute*time.Duration(cfg.InMemoryCacheExpirationMinutes),
		time.Minute*time.Duration(cfg.InMemoryCachePurgeDurationMinutes),
	)

	// Users
	usersCfg := twitchclient.ClientConf{
		Host:  cfg.UsersServiceHost,
		Stats: stats,
		RoundTripperWrappers: []func(http.RoundTripper) http.RoundTripper{
			wrapper.NewProxyRoundTripWrapper(),
			wrapper.NewCircuitRoundTripWrapper(cm, users.ServiceName, time.Duration(cfg.UsersTimeoutMilliseconds)*time.Millisecond, stats),
			wrapper.NewRetryRoundTripWrapper(stats, users.ServiceName, 1),
		},
	}
	usersClient, err := users.NewUsersClient(usersCfg)
	if err != nil {
		return nil, errors.Wrap(err, "failed to init users client")
	}

	// Video Channel Properties
	channelProperties := video.NewChannelProperties(twitchclient.ClientConf{
		Host:  cfg.ChannelPropertiesHost,
		Stats: stats,
		RoundTripperWrappers: []func(http.RoundTripper) http.RoundTripper{
			wrapper.NewProxyRoundTripWrapper(),
			wrapper.NewCircuitRoundTripWrapper(cm, video.ServiceName, time.Duration(cfg.ChannelPropertiesTimeoutMilliseconds)*time.Millisecond, stats),
			wrapper.NewRetryRoundTripWrapper(stats, video.ServiceName, 1),
		},
	}, cfg.ChannelPropertiesAPIKey, awsSession.Config.Credentials)

	// Spade client
	spadeClient, err := spadeWrapper.NewClient(cfg, stats, twitchclient.NewHTTPClient(twitchclient.ClientConf{
		Host:  cfg.StatsURL,
		Stats: stats,
		RoundTripperWrappers: []func(http.RoundTripper) http.RoundTripper{
			wrapper.NewProxyRoundTripWrapper(),
			wrapper.NewCircuitRoundTripWrapper(cm, spadeWrapper.ServiceName, time.Duration(cfg.SpadeTimeoutTrackMilliseconds)*time.Millisecond, stats),
			wrapper.NewRetryRoundTripWrapper(stats, spadeWrapper.ServiceName, 1),
		},
	}))
	if err != nil {
		return nil, errors.Wrap(err, "failed to init spade client")
	}

	// Pubsub client
	pubsubClient, err := pubsub.NewPubsubClient(twitchclient.ClientConf{
		Host:  cfg.PubsubBrokerHost,
		Stats: stats,
		RoundTripperWrappers: []func(http.RoundTripper) http.RoundTripper{
			wrapper.NewProxyRoundTripWrapper(),
			wrapper.NewCircuitRoundTripWrapper(cm, pubsub.ServiceName, time.Duration(cfg.PubsubBrokerTimeoutMilliseconds)*time.Millisecond, stats),
			wrapper.NewRetryRoundTripWrapper(stats, pubsub.ServiceName, 1),
		},
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to init pubsub client")
	}

	// Subscriptions client
	subsClient := subs.NewClient(cfg.SubscriptionsServiceHost, twitchclient.NewHTTPClient(twitchclient.ClientConf{
		Host:  cfg.SubscriptionsServiceHost,
		Stats: stats,
		RoundTripperWrappers: []func(http.RoundTripper) http.RoundTripper{
			wrapper.NewProxyRoundTripWrapper(),
			wrapper.NewCircuitRoundTripWrapper(cm, subs.ServiceName, time.Duration(cfg.SubscriptionsServiceTimeoutMilliseconds)*time.Millisecond, stats),
			wrapper.NewRetryRoundTripWrapper(stats, subs.ServiceName, 1),
		},
	}))

	// TwitchVoyager client
	voyagerClient := voyager.NewClient(cfg.VoyagerServiceHost, twitchclient.NewHTTPClient(twitchclient.ClientConf{
		Host:  cfg.VoyagerServiceHost,
		Stats: stats,
		RoundTripperWrappers: []func(http.RoundTripper) http.RoundTripper{
			wrapper.NewProxyRoundTripWrapper(),
			wrapper.NewCircuitRoundTripWrapper(cm, voyager.ServiceName, time.Duration(cfg.VoyagerServiceTimeoutMilliseconds)*time.Millisecond, stats),
			wrapper.NewRetryRoundTripWrapper(stats, voyager.ServiceName, 1),
		},
	}))

	// Siosa client
	siosaClient := siosa.NewSiosaClient(twitchclient.ClientConf{
		Host:  cfg.SiosaHost,
		Stats: stats,
		RoundTripperWrappers: []func(http.RoundTripper) http.RoundTripper{
			wrapper.NewProxyRoundTripWrapper(),
			wrapper.NewCircuitRoundTripWrapper(cm, siosa.ServiceName, time.Duration(cfg.SiosaTimeoutMilliseconds)*time.Millisecond, stats),
			wrapper.NewRetryRoundTripWrapper(stats, siosa.ServiceName, 1),
		},
	})

	// Liveline client
	livelineClient := liveline.NewLivelineClient(cfg.LivelineServiceHost, twitchclient.NewHTTPClient(twitchclient.ClientConf{
		Host:  cfg.LivelineServiceHost,
		Stats: stats,
		RoundTripperWrappers: []func(http.RoundTripper) http.RoundTripper{
			wrapper.NewProxyRoundTripWrapper(),
			wrapper.NewCircuitRoundTripWrapper(cm, liveline.ServiceName, time.Duration(cfg.LivelineServiceTimeoutMilliseconds)*time.Millisecond, stats),
			wrapper.NewRetryRoundTripWrapper(stats, liveline.ServiceName, 1),
		},
	}))

	// Upload service client
	uploadService := upload.NewUploadServiceClient(
		cfg.UploadServiceHost,
		twitchclient.NewHTTPClient(twitchclient.ClientConf{
			Host:  cfg.UploadServiceHost,
			Stats: stats,
			RoundTripperWrappers: []func(http.RoundTripper) http.RoundTripper{
				wrapper.NewProxyRoundTripWrapper(),
				wrapper.NewCircuitRoundTripWrapper(cm, upload.ServiceName, time.Duration(cfg.UploadServiceTimeoutMilliseconds)*time.Millisecond, stats),
				wrapper.NewRetryRoundTripWrapper(stats, upload.ServiceName, 1),
			},
		}),
		cfg.UploadServiceCallbackSNSTopicARN,
		cfg.UploadServiceS3BucketName,
		cfg.ContentAttributeImageURLPrefix,
	)

	// Pepper client
	pepperClient, err := pepper.NewPepperClient(twitchclient.ClientConf{
		Host:  cfg.PepperServiceHost,
		Stats: stats,
		RoundTripperWrappers: []func(http.RoundTripper) http.RoundTripper{
			wrapper.NewProxyRoundTripWrapper(),
			wrapper.NewCircuitRoundTripWrapper(cm, pepper.ServiceName, time.Duration(cfg.PepperServiceTimeoutMilliseconds)*time.Millisecond, stats),
			wrapper.NewRetryRoundTripWrapper(stats, pepper.ServiceName, 1),
		},
	}, cfg.S2SCallerName)
	if err != nil {
		return nil, errors.Wrap(err, "failed to init pepper client")
	}

	// Zuma client
	zumaClient, err := zuma.NewClient(twitchclient.ClientConf{
		Host:  cfg.ZumaServiceHost,
		Stats: stats,
		RoundTripperWrappers: []func(http.RoundTripper) http.RoundTripper{
			wrapper.NewProxyRoundTripWrapper(),
			wrapper.NewCircuitRoundTripWrapper(cm, zuma.ServiceName, time.Duration(cfg.ZumaServiceTimeoutMilliseconds)*time.Millisecond, stats),
			wrapper.NewRetryRoundTripWrapper(stats, zuma.ServiceName, 1),
		},
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to initialize Zuma client")
	}

	// RBAC client
	rbacClient := rbac.NewClient(cfg.RbacServiceHost, dynamicCfg, twitchclient.ClientConf{
		Host:  cfg.RbacServiceHost,
		Stats: stats,
		RoundTripperWrappers: []func(http.RoundTripper) http.RoundTripper{
			wrapper.NewProxyRoundTripWrapper(),
			wrapper.NewCircuitRoundTripWrapper(cm, rbac.ServiceName, time.Duration(cfg.RbacServiceTimeoutMilliseconds)*time.Millisecond, stats),
			wrapper.NewRetryRoundTripWrapper(stats, rbac.ServiceName, 1),
		},
	})

	vodAPIClient, err := vodapi.NewClient(cfg.VodAPIServiceHost, twitchclient.NewHTTPClient(twitchclient.ClientConf{
		Host:  cfg.VodAPIServiceHost,
		Stats: stats,
		RoundTripperWrappers: []func(http.RoundTripper) http.RoundTripper{
			wrapper.NewProxyRoundTripWrapper(),
			wrapper.NewCircuitRoundTripWrapper(cm, vodapi.ServiceName, time.Duration(cfg.VodAPIServiceTimeoutMilliseconds)*time.Millisecond, stats),
			wrapper.NewRetryRoundTripWrapper(stats, vodapi.ServiceName, 1),
		},
	}), dynamicCfg)
	if err != nil {
		return nil, errors.Wrap(err, "failed to initialize vodAPI client")
	}

	return &Clients{
		InMemoryCache:     inMemoryCache,
		UsersClient:       usersClient,
		Spade:             spadeClient,
		ChannelProperties: channelProperties,
		Subscriptions:     subsClient,
		TwitchVoyager:     voyagerClient,
		Pubsub:            pubsubClient,
		SiosaClient:       siosaClient,
		Liveline:          livelineClient,
		UploadService:     uploadService,
		Pepper:            pepperClient,
		Zuma:              zumaClient,
		Rbac:              rbacClient,
		VodAPI:            vodAPIClient,
	}, nil
}
