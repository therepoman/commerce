package clients

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/request"
	"github.com/aws/aws-sdk-go/service/sfn"
	"github.com/aws/aws-sdk-go/service/sns"
)

//go:generate mockery --note @generated --name SNSPublisher

// SNSPublisher isolates a single method from "github.com/aws/aws-sdk-go/service/sns/snsiface".SNSAPI
type SNSPublisher interface {
	PublishWithContext(ctx aws.Context, input *sns.PublishInput, opts ...request.Option) (*sns.PublishOutput, error)
}

//go:generate mockery --note @generated --name SFNExecutor

// SFNExecutor isolates a single method from "github.com/aws/aws-sdk-go/service/sfn/sfniface".SFNAPI
type SFNExecutor interface {
	StartExecutionWithContext(ctx aws.Context, input *sfn.StartExecutionInput, opts ...request.Option) (*sfn.StartExecutionOutput, error)
}
