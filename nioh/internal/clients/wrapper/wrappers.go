package wrapper

import (
	"context"
	"fmt"

	log "code.justin.tv/commerce/logrus"
	"github.com/pkg/errors"

	"code.justin.tv/foundation/twitchclient"
)

const (
	metricPattern         = "service.%s.%s"
	defaultStatSampleRate = 1.0
	defaultServiceName    = "unknownservice"
	defaultAPIName        = "unknownapi"
)

// GenerateRequestContext returns a context with metrics name
func GenerateRequestContext(serviceName string, apiName string) context.Context {
	return twitchclient.WithReqOpts(context.Background(), getReqOpts(serviceName, apiName))
}

// WrapRequestContext returns a context wrapped with metrics name
func WrapRequestContext(ctx context.Context, serviceName, apiName string) context.Context {
	return twitchclient.WithReqOpts(ctx, getReqOpts(serviceName, apiName))
}

func getReqOpts(serviceName, apiName string) twitchclient.ReqOpts {
	if serviceName == "" {
		log.WithError(errors.New("Service name must not be empty when generating client request context")).Error("Service name must not be empty when generating client request context")
		serviceName = defaultServiceName
	}

	if apiName == "" {
		log.WithError(errors.New("API name must not be empty when generating client request context")).Error("API name must not be empty when generating client request context")
		apiName = defaultAPIName
	}

	reqOpts := twitchclient.ReqOpts{}
	reqOpts.StatName = fmt.Sprintf(metricPattern, serviceName, apiName)
	reqOpts.StatSampleRate = defaultStatSampleRate
	return reqOpts
}
