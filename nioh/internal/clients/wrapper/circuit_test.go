package wrapper_test

import (
	"bytes"
	"io/ioutil"
	"net/http"
	"testing"
	"time"

	"github.com/pkg/errors"

	"code.justin.tv/commerce/nioh/internal/clients/wrapper"
	"code.justin.tv/commerce/nioh/mocks"
	"code.justin.tv/commerce/nioh/pkg/helpers"
	"github.com/cactus/go-statsd-client/statsd"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

const (
	statusCode200 = 200
	statusCode500 = 500
)

func TestCircuitRoundTripper(t *testing.T) {
	Convey("Test RoundTrip", t, func() {
		defaultReqBodyBytes := []byte("this is the request body in byte array format")
		defaultRespBodyBytes := []byte("this is the response body in byte array format. http 200 ok lul")
		defaultHTTPRequest := generateHTTPRequest(defaultReqBodyBytes)

		mockDownstreamResponse := new(mocks.RoundTripper)

		name := "test"
		cm := helpers.MakeCircuitManager(nil)
		stats, _ := statsd.NewNoopClient()
		circuitRoundTripper := wrapper.NewCircuitRoundTripWrapper(cm, name, 1000*time.Millisecond, stats)
		circuitWrapper, ok := circuitRoundTripper(mockDownstreamResponse).(*wrapper.CircuitRoundTripWrapper)
		So(ok, ShouldBeTrue)

		Convey("Downstream returns 200", func() {
			mockDownstreamResponse.On("RoundTrip", mock.Anything).Return(generateHTTPResponse(statusCode200, defaultRespBodyBytes), nil)

			resp, err := circuitWrapper.RoundTrip(defaultHTTPRequest)
			So(err, ShouldBeNil)
			So(resp, ShouldNotBeNil)

			respBodyBytes, err := ioutil.ReadAll(resp.Body)
			So(err, ShouldBeNil)
			So(resp, ShouldNotBeNil)
			So(resp.StatusCode, ShouldEqual, statusCode200)
			So(bytes.Compare(respBodyBytes, defaultRespBodyBytes), ShouldEqual, 0)
			mockDownstreamResponse.AssertNumberOfCalls(t, "RoundTrip", 1)
		})

		Convey("Downstream returns 500", func() {
			mockDownstreamResponse.On("RoundTrip", mock.Anything).Return(generateHTTPResponse(statusCode500, defaultRespBodyBytes), nil)

			resp, err := circuitWrapper.RoundTrip(defaultHTTPRequest)
			So(err, ShouldBeNil)
			So(resp, ShouldNotBeNil)

			respBodyBytes, err := ioutil.ReadAll(resp.Body)
			So(err, ShouldBeNil)
			So(resp, ShouldNotBeNil)
			So(resp.StatusCode, ShouldEqual, statusCode500)
			So(bytes.Compare(respBodyBytes, defaultRespBodyBytes), ShouldEqual, 0)
			mockDownstreamResponse.AssertNumberOfCalls(t, "RoundTrip", 1)
		})

		Convey("error calling downstream", func() {
			mockDownstreamResponse.On("RoundTrip", mock.Anything).Return(nil, errors.New("connection refused"))

			resp, err := circuitWrapper.RoundTrip(defaultHTTPRequest)
			So(err, ShouldNotBeNil)
			So(resp, ShouldBeNil)

			mockDownstreamResponse.AssertNumberOfCalls(t, "RoundTrip", 1)
		})

		Convey("Downstream returns 500 multiple times - open circuit", func() {
			mockDownstreamResponse.On("RoundTrip", mock.Anything).Return(generateHTTPResponse(statusCode500, defaultRespBodyBytes), nil)

			resp, err := circuitWrapper.RoundTrip(defaultHTTPRequest)
			So(err, ShouldBeNil)
			So(resp, ShouldNotBeNil)

			respBodyBytes, err := ioutil.ReadAll(resp.Body)
			So(err, ShouldBeNil)
			So(resp, ShouldNotBeNil)
			So(resp.StatusCode, ShouldEqual, statusCode500)
			So(bytes.Compare(respBodyBytes, defaultRespBodyBytes), ShouldEqual, 0)
			mockDownstreamResponse.AssertNumberOfCalls(t, "RoundTrip", 1)

			// Fail 100 more times
			for i := 1; i <= 100; i++ {
				circuitWrapper.RoundTrip(defaultHTTPRequest)
			}

			resp, err = circuitWrapper.RoundTrip(defaultHTTPRequest)
			So(err, ShouldNotBeNil)
			So(resp, ShouldBeNil)
			So(len(mockDownstreamResponse.Calls), ShouldBeLessThan, 100)
		})
	})
}

func generateHTTPRequest(bodyBytes []byte) *http.Request {
	reqBody := ioutil.NopCloser(bytes.NewBuffer(bodyBytes))
	req, err := http.NewRequest("GET", "myurl", reqBody)
	So(err, ShouldBeNil)
	return req
}

func generateHTTPResponse(statusCode int, bodyBytes []byte) *http.Response {
	respBody := ioutil.NopCloser(bytes.NewBuffer(bodyBytes))
	resp := &http.Response{
		StatusCode: statusCode,
		Body:       respBody,
	}
	return resp
}
