package wrapper

import (
	"net/http"
	"os"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

type fakeRoundTripper struct{}

func (f *fakeRoundTripper) RoundTrip(req *http.Request) (*http.Response, error) {
	return nil, nil
}

func TestProxyRoundTripper(t *testing.T) {
	Convey("Test Proxy", t, func() {
		Convey("without addr", func() {
			tripFunc := NewProxyRoundTripWrapper()
			_, ok := tripFunc(&fakeRoundTripper{}).(*http.Transport)
			So(ok, ShouldBeFalse)
		})

		Convey("with addr", func() {
			os.Setenv("SOCKS5_ADDR", "localhost:8100")
			tripFunc := NewProxyRoundTripWrapper()
			transport, ok := tripFunc(&fakeRoundTripper{}).(*http.Transport)
			So(ok, ShouldBeTrue)
			So(transport.DialContext, ShouldNotBeNil)
		})
	})
}
