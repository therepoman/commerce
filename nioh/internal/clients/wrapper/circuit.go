package wrapper

import (
	"context"
	"net/http"
	"time"

	"code.justin.tv/commerce/nioh/pkg/helpers"

	log "code.justin.tv/commerce/logrus"

	"github.com/cactus/go-statsd-client/statsd"
	"github.com/cep21/circuit"
	"github.com/pkg/errors"
)

// CircuitRoundTripWrapper is a wrapper around an http round tripper which utilizes
// a circuit to allow timeouts, circuit breakers, connection pool, etc.
type CircuitRoundTripWrapper struct {
	Next    http.RoundTripper
	Name    string
	Circuit *circuit.Circuit
}

var errCircuit5xx = errors.New("circuit encountered an HTTP 5XX")

// NewCircuitRoundTripWrapper returns a new CircuitRoundTripWrapper.
func NewCircuitRoundTripWrapper(cm *circuit.Manager, name string, timeout time.Duration, statter statsd.Statter) func(c http.RoundTripper) http.RoundTripper {
	c := helpers.MakeCircuit(cm, &helpers.CircuitInput{
		Name:                  name,
		Timeout:               timeout,
		SleepWindow:           time.Duration(DefaultSleepWindow) * time.Millisecond,
		VolumeThreshold:       DefaultVolumeThreshold,
		ErrorPercentThreshold: DefaultErrorPercentThreshold,
		MaxConcurrentRequest:  DefaultMaxConcurrentRequest,
		Statter:               statter,
	})

	return func(next http.RoundTripper) http.RoundTripper {
		return &CircuitRoundTripWrapper{
			Next:    next,
			Name:    name,
			Circuit: c,
		}
	}
}

// RoundTrip wraps HTTP client calls. It wraps the downstream call in a Hystrix circuit.
func (c *CircuitRoundTripWrapper) RoundTrip(req *http.Request) (*http.Response, error) {
	var resp *http.Response
	err := c.Circuit.Execute(req.Context(), func(ctx context.Context) error {
		var nextErr error
		resp, nextErr = c.Next.RoundTrip(req)
		if nextErr != nil {
			return errors.Wrap(nextErr, "Error from next round tripper")
		}
		if resp.StatusCode >= http.StatusInternalServerError {
			// 5XX errors count towards circuit errors so that we can break the circuit on them
			// when we see too many
			return errCircuit5xx
		}
		return nil
	}, nil)

	if err != nil {
		if err == errCircuit5xx {
			// Swallowing the circuit error and allowing the 5XX response to propagate out.
			// This allows other handlers and wrappers to handle the status code at their discretion
			log.WithError(err).Errorf("Circuit encountered an HTTP 5XX from service: %s", c.Name)
			return resp, nil
		}
		return nil, errors.Wrap(err, "error from circuit")
	}

	return resp, nil
}
