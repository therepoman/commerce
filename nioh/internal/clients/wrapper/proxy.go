package wrapper

import (
	"context"
	"log"
	"net"
	"net/http"
	"os"

	"code.justin.tv/commerce/nioh/config"
	"golang.org/x/net/proxy"
)

// NewProxyRoundTripWrapper returns a new ProxyRoundTripWrapper.
func NewProxyRoundTripWrapper() func(c http.RoundTripper) http.RoundTripper {
	// Only use in development!
	socksAddr := os.Getenv("SOCKS5_ADDR")
	if socksAddr == "" || config.GetCanonicalEnv() != config.DevelopmentEnv {
		return func(next http.RoundTripper) http.RoundTripper {
			return next
		}
	}

	dialer, err := proxy.SOCKS5("tcp", socksAddr, nil, proxy.Direct)
	if err != nil {
		log.Fatalf("can't connect to the proxy: %v", err)
	}
	return func(next http.RoundTripper) http.RoundTripper {
		return &http.Transport{
			DialContext: func(ctx context.Context, network, addr string) (net.Conn, error) {
				return dialer.Dial(network, addr)
			},
		}
	}
}
