package wrapper_test

import (
	"bytes"
	"io/ioutil"
	"testing"

	"github.com/afex/hystrix-go/hystrix"
	"github.com/pkg/errors"

	"code.justin.tv/commerce/nioh/internal/clients/wrapper"
	"code.justin.tv/commerce/nioh/mocks"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestHystrixRoundTripper(t *testing.T) {
	Convey("Test RoundTrip", t, func() {
		defaultReqBodyBytes := []byte("this is the request body in byte array format")
		defaultRespBodyBytes := []byte("this is the response body in byte array format. http 200 ok lul")
		defaultHTTPRequest := generateHTTPRequest(defaultReqBodyBytes)

		mockDownstreamResponse := new(mocks.RoundTripper)

		name := "test"
		wrapper := wrapper.HystrixRoundTripWrapper{
			Next: mockDownstreamResponse,
			Name: name,
		}

		config := hystrix.CommandConfig{
			Timeout:                1000,
			MaxConcurrentRequests:  100,
			RequestVolumeThreshold: 4,
			SleepWindow:            5000,
			ErrorPercentThreshold:  20,
		}
		hystrix.Flush()
		hystrix.ConfigureCommand(name, config)

		Convey("Downstream returns 200", func() {
			mockDownstreamResponse.On("RoundTrip", mock.Anything).Return(generateHTTPResponse(statusCode200, defaultRespBodyBytes), nil)

			resp, err := wrapper.RoundTrip(defaultHTTPRequest)
			So(err, ShouldBeNil)
			So(resp, ShouldNotBeNil)

			respBodyBytes, err := ioutil.ReadAll(resp.Body)
			So(err, ShouldBeNil)
			So(resp, ShouldNotBeNil)
			So(resp.StatusCode, ShouldEqual, statusCode200)
			So(bytes.Compare(respBodyBytes, defaultRespBodyBytes), ShouldEqual, 0)
			mockDownstreamResponse.AssertNumberOfCalls(t, "RoundTrip", 1)
		})

		Convey("Downstream returns 500", func() {
			mockDownstreamResponse.On("RoundTrip", mock.Anything).Return(generateHTTPResponse(statusCode500, defaultRespBodyBytes), nil)

			resp, err := wrapper.RoundTrip(defaultHTTPRequest)
			So(err, ShouldBeNil)
			So(resp, ShouldNotBeNil)

			respBodyBytes, err := ioutil.ReadAll(resp.Body)
			So(err, ShouldBeNil)
			So(resp, ShouldNotBeNil)
			So(resp.StatusCode, ShouldEqual, statusCode500)
			So(bytes.Compare(respBodyBytes, defaultRespBodyBytes), ShouldEqual, 0)
			mockDownstreamResponse.AssertNumberOfCalls(t, "RoundTrip", 1)
		})

		Convey("error calling downstream", func() {
			mockDownstreamResponse.On("RoundTrip", mock.Anything).Return(nil, errors.New("connection refused"))

			resp, err := wrapper.RoundTrip(defaultHTTPRequest)
			So(err, ShouldNotBeNil)
			So(resp, ShouldBeNil)

			mockDownstreamResponse.AssertNumberOfCalls(t, "RoundTrip", 1)
		})

		Convey("Downstream returns 500 multiple times - open circuit", func() {
			mockDownstreamResponse.On("RoundTrip", mock.Anything).Return(generateHTTPResponse(statusCode500, defaultRespBodyBytes), nil)

			resp, err := wrapper.RoundTrip(defaultHTTPRequest)
			So(err, ShouldBeNil)
			So(resp, ShouldNotBeNil)

			respBodyBytes, err := ioutil.ReadAll(resp.Body)
			So(err, ShouldBeNil)
			So(resp, ShouldNotBeNil)
			So(resp.StatusCode, ShouldEqual, statusCode500)
			So(bytes.Compare(respBodyBytes, defaultRespBodyBytes), ShouldEqual, 0)
			mockDownstreamResponse.AssertNumberOfCalls(t, "RoundTrip", 1)

			// Fail 10 more times
			for i := 1; i <= 10; i++ {
				wrapper.RoundTrip(defaultHTTPRequest)
			}

			resp, err = wrapper.RoundTrip(defaultHTTPRequest)
			So(err, ShouldNotBeNil)
			So(resp, ShouldBeNil)
			So(len(mockDownstreamResponse.Calls), ShouldBeLessThan, 11)
		})
	})
}
