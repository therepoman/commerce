package wrapper

import (
	"context"
	"testing"

	"code.justin.tv/foundation/twitchclient"
	. "github.com/smartystreets/goconvey/convey"
)

func TestGenerateRequestContext(t *testing.T) {
	var ctx context.Context

	Convey("Test GenerateRequestContext", t, func() {
		Convey("with valid input", func() {
			ctx = GenerateRequestContext("bar", "foo")
		})

		Convey("with missing service name", func() {
			ctx = GenerateRequestContext("", "foo")
		})

		Convey("with missing api name", func() {
			ctx = GenerateRequestContext("bar", "")
		})

		Convey("with missing both", func() {
			ctx = GenerateRequestContext("", "")
		})

		So(ctx, ShouldNotBeNil)
	})

	Convey("Test WrapRequestContext", t, func() {
		ctx = context.Background()

		Convey("with valid input", func() {
			ctx = WrapRequestContext(ctx, "bar", "foo")
		})

		Convey("with missing service name", func() {
			ctx = WrapRequestContext(ctx, "", "foo")
		})

		Convey("with missing api name", func() {
			ctx = WrapRequestContext(ctx, "bar", "")
		})

		Convey("with missing both", func() {
			ctx = WrapRequestContext(ctx, "", "")
		})

		So(ctx, ShouldNotBeNil)
	})

	Convey("Test getReqOpts", t, func() {
		Convey("with valid input", func() {
			reqOpts := getReqOpts("bar", "foo")
			So(reqOpts, ShouldResemble, twitchclient.ReqOpts{
				StatName:       "service.bar.foo",
				StatSampleRate: defaultStatSampleRate,
			})
		})

		Convey("with missing service name", func() {
			reqOpts := getReqOpts("", "foo")
			So(reqOpts, ShouldResemble, twitchclient.ReqOpts{
				StatName:       "service.unknownservice.foo",
				StatSampleRate: defaultStatSampleRate,
			})
		})

		Convey("with missing api name", func() {
			reqOpts := getReqOpts("bar", "")
			So(reqOpts, ShouldResemble, twitchclient.ReqOpts{
				StatName:       "service.bar.unknownapi",
				StatSampleRate: defaultStatSampleRate,
			})
		})

		Convey("with missing both", func() {
			reqOpts := getReqOpts("", "")
			So(reqOpts, ShouldResemble, twitchclient.ReqOpts{
				StatName:       "service.unknownservice.unknownapi",
				StatSampleRate: defaultStatSampleRate,
			})
		})
	})
}
