package vodapi

import (
	"context"
	"net/http"
	"strconv"

	"code.justin.tv/commerce/logrus"
	niohConfig "code.justin.tv/commerce/nioh/config"
	"code.justin.tv/commerce/nioh/internal/clients/wrapper"
	"code.justin.tv/vod/vodapi/rpc/vodapi"
	"github.com/dgraph-io/ristretto"
	"github.com/pkg/errors"
	"github.com/twitchtv/twirp"
)

// Client wraps vodapi.VodApi interface client, providing versions of its methods.
type Client interface {
	GetVodBroadcastID(ctx context.Context, vodID string) (string, error)
}

type clientImpl struct {
	cache         ristretto.Cache
	vodAPI        vodapi.VodApi
	dynamicConfig niohConfig.IDynamicConfiguration
}

const (
	// ServiceName is the dimension of the vodapi service used in stats/metrics.
	ServiceName = "vodapi"

	getVodByIDAPIName = "GetVodByID"

	// github.com/dgraph-io/ristretto recommends this be 10x cacheMaxSizeBytes
	cacheCounters = 100000

	// maximum size to allow the cache to grow to. setting to 100MB
	cacheMaxSizeBytes = 100000000

	// value recommended by https://github.com/dgraph-io/ristretto
	cacheBufferItems = 64
)

var _ Client = &clientImpl{}

// NewClient instantiates a new vodapi client for use in nioh
func NewClient(address string, httpClient *http.Client, dynamicCfg niohConfig.IDynamicConfiguration) (Client, error) {
	vodAPIPercent, err := dynamicCfg.VodAPIRolloutPercent()
	if err != nil {
		logrus.Errorf("Error getting VodAPI rollout percent: %v\n", err)
	}

	logrus.Infof("VodAPI rollout percent: %v\n", vodAPIPercent)

	vodAPIClient := vodapi.NewVodApiProtobufClient(address, httpClient)

	cache, err := ristretto.NewCache(&ristretto.Config{
		NumCounters: cacheCounters,
		MaxCost:     cacheMaxSizeBytes,
		BufferItems: cacheBufferItems,
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to initialize vodAPI cache: %v")
	}

	return &clientImpl{
		cache:         *cache,
		vodAPI:        vodAPIClient,
		dynamicConfig: dynamicCfg,
	}, nil
}

// GetVodBroadcastID takes in a vod id and returns the vod's broadcastID
func (c *clientImpl) GetVodBroadcastID(ctx context.Context, vodID string) (string, error) {
	vodAPIRolloutPercent, _ := c.dynamicConfig.VodAPIRolloutPercent()
	vodIDInt, err := strconv.Atoi(vodID)
	if err != nil {
		return "", err
	}

	if int64(vodIDInt%100) < vodAPIRolloutPercent {
		value, found := c.cache.Get(vodID)
		if found {
			return value.(string), nil
		}

		ctx = wrapper.WrapRequestContext(ctx, ServiceName, getVodByIDAPIName)

		resp, err := c.vodAPI.InternalGetVodByID(ctx, &vodapi.InternalGetVodByIDRequest{
			VodId: vodID,
		})
		if err != nil {
			// don't return error if vodapi returns 404
			if twerr, ok := err.(twirp.Error); ok {
				if twerr.Code() == twirp.NotFound {
					c.cache.Set(vodID, "", 1)
					return "", nil
				}
			}

			return "", err
		}

		broadcastID := resp.Vod.BroadcastId
		c.cache.Set(vodID, broadcastID, 1)

		return broadcastID, nil
	}

	return "", nil
}
