package upload

import (
	"context"
	"fmt"
	"net/http"

	"code.justin.tv/commerce/nioh/internal/clients/wrapper"
	"code.justin.tv/commerce/nioh/internal/models"
	"code.justin.tv/web/upload-service/rpc/uploader"
	"github.com/pkg/errors"
)

const (
	// ServiceName is dimension of the upload service appearing in stats/metrics
	ServiceName   = "uploadservice"
	createAPIName = "Create"

	globalReadAccessPermission = "uri=http://acs.amazonaws.com/groups/global/AllUsers"
	defaultPubsubTopic         = "default" // using upload service provided pubsub topic

	fileNameFormat     = "{{UploadID}}"
	s3PathPrefixFormat = "s3://%s/%s/"

	imageURLFormat = "%s/%s/%s" // urlPrefix/channelID/uploadID
)

// IUploadServiceClient is the upload service client interface
type IUploadServiceClient interface {
	CreateContentAttributeImageUploadConfig(ctx context.Context, channelID string) (*models.ContentAttributeImageUploadConfig, error)
}

type uploadServiceClient struct {
	uploader            uploader.Uploader
	callbackSNSTopicARN string
	s3BucketName        string
	imageURLPrefix      string
}

// NewUploadServiceClient sets up a new client to the upload service
func NewUploadServiceClient(host string, httpClient *http.Client, callbackSNSTopicARN string, s3BucketName string, imageURLPrefix string) IUploadServiceClient {
	return &uploadServiceClient{
		uploader:            uploader.NewUploaderProtobufClient(host, httpClient),
		callbackSNSTopicARN: callbackSNSTopicARN,
		s3BucketName:        s3BucketName,
		imageURLPrefix:      imageURLPrefix,
	}
}

func (c *uploadServiceClient) CreateContentAttributeImageUploadConfig(ctx context.Context, channelID string) (*models.ContentAttributeImageUploadConfig, error) {
	if channelID == "" {
		return nil, errors.New("channelID is required")
	}

	ctx = wrapper.WrapRequestContext(ctx, ServiceName, createAPIName)
	resp, err := c.uploader.Create(ctx, &uploader.UploadRequest{
		PreValidation: &uploader.Validation{
			Format: "image", // jpeg, png and gif
			AspectRatioConstraints: []*uploader.Constraint{
				// we really only want to accept square images for content attribute
				// but loosening up the constraints a bit here so that a few pixels off isn't an issue for us
				// TODO: decide on a file size constraint
				{
					Value: 0.98,
					Test:  ">=",
				},
				{
					Value: 1.02,
					Test:  "<=",
				},
			},
		},
		Outputs: []*uploader.Output{
			{
				Name: fileNameFormat,
				Permissions: &uploader.Permissions{
					GrantRead: globalReadAccessPermission,
				},
			},
		},
		Callback: &uploader.Callback{
			SnsTopicArn: c.callbackSNSTopicARN,
			PubsubTopic: defaultPubsubTopic,
		},
		OutputPrefix: c.s3PathPrefix(channelID),
	})

	if err != nil {
		return nil, errors.Wrap(err, "encountered error while making image upload request to upload service")
	}

	return &models.ContentAttributeImageUploadConfig{
		ChannelID: channelID,
		UploadID:  resp.UploadId,
		UploadURL: resp.Url,
		ImageURL:  c.imageURL(channelID, resp),
	}, nil
}

func (c *uploadServiceClient) imageURL(channelID string, resp *uploader.UploadResponse) string {
	return fmt.Sprintf(imageURLFormat, c.imageURLPrefix, channelID, resp.UploadId)
}

func (c *uploadServiceClient) s3PathPrefix(channelID string) string {
	return fmt.Sprintf(s3PathPrefixFormat, c.s3BucketName, channelID)
}
