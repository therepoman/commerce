package upload

import (
	"context"
	"errors"
	"fmt"
	"testing"

	"code.justin.tv/commerce/nioh/internal/clients/upload/mocks"
	"code.justin.tv/web/upload-service/rpc/uploader"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestUploadServiceClient(t *testing.T) {
	s3BucketName := "bucket_name"
	snsTopicARN := "arn:of:my:topic"
	channelID := "11112222"
	uploadURL := "http://upload.url"
	uploadID := "12345"
	imageURLHostPrefix := "http://cdn.url"

	Convey("CreateContentAttributeImageUploadConfig", t, func() {
		mockUploader := new(mocks.Uploader)
		uploadService := uploadServiceClient{
			uploader:            mockUploader,
			s3BucketName:        s3BucketName,
			callbackSNSTopicARN: snsTopicARN,
			imageURLPrefix:      imageURLHostPrefix,
		}

		Convey("with empty channelID", func() {
			_, err := uploadService.CreateContentAttributeImageUploadConfig(context.Background(), "")
			So(err, ShouldNotBeNil)
		})

		Convey("when uploader returns success", func() {
			resp := &uploader.UploadResponse{
				Url:      uploadURL,
				UploadId: uploadID,
			}
			mockUploader.On("Create", mock.Anything, mock.Anything).Return(resp, nil)

			config, err := uploadService.CreateContentAttributeImageUploadConfig(context.Background(), channelID)
			So(err, ShouldBeNil)
			So(config, ShouldNotBeNil)
			So(config.ChannelID, ShouldEqual, channelID)
			So(config.UploadURL, ShouldEqual, uploadURL)
			So(config.UploadID, ShouldEqual, uploadID)
			So(config.ImageURL, ShouldEqual, fmt.Sprintf(imageURLFormat, imageURLHostPrefix, channelID, uploadID))
		})

		Convey("when uploader fails", func() {
			mockUploader.On("Create", mock.Anything, mock.Anything).Return(nil, errors.New("can't upload"))

			_, err := uploadService.CreateContentAttributeImageUploadConfig(context.Background(), channelID)
			So(err, ShouldNotBeNil)
		})
	})
}
