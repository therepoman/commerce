package video

import (
	"context"
	"net/http"
	"strings"

	api "code.justin.tv/amzn/StarfruitTwitchChannelPropsTwirp"
	"code.justin.tv/commerce/nioh/internal/auth"
	"code.justin.tv/commerce/nioh/internal/clients/wrapper"
	"code.justin.tv/foundation/twitchclient"

	"github.com/aws/aws-sdk-go/aws/credentials"
	signer "github.com/aws/aws-sdk-go/aws/signer/v4"
	"github.com/pkg/errors"
	"github.com/twitchtv/twirp"
)

const (
	// ServiceName is the name of the service for stats display
	ServiceName = "video_channel_properties"

	getChannelByIDAPIName = "GetChannelById"
	streamKeyPrefix       = "live"
)

// IChannelProperties is an interface to interact with video channel properties service
type IChannelProperties interface {
	GetStreamKey(ctx context.Context, userID string) (string, error)
}

// ChannelProperties implements  IChannelProperties
type ChannelProperties struct {
	Client api.StarfruitTwitchChannelProps
	APIKey string
}

// NewChannelProperties returns a new ChannelProperties
func NewChannelProperties(clientConfig twitchclient.ClientConf, apiKey string, creds *credentials.Credentials) IChannelProperties {
	baseClient := twitchclient.NewHTTPClient(clientConfig)
	sigv4Client := &auth.SignerClient{
		BaseClient: baseClient,
		Signer:     signer.NewSigner(creds),
		Service:    "execute-api",
		Region:     "us-west-2",
	}

	return &ChannelProperties{
		Client: api.NewStarfruitTwitchChannelPropsProtobufClient(clientConfig.Host, sigv4Client),
		APIKey: apiKey,
	}
}

// GetStreamKey returns a stream key for a user ID
func (c *ChannelProperties) GetStreamKey(ctx context.Context, userID string) (string, error) {
	ctx = wrapper.WrapRequestContext(ctx, ServiceName, getChannelByIDAPIName)
	header := make(http.Header)
	header.Set("x-api-key", c.APIKey)
	ctx, err := twirp.WithHTTPRequestHeaders(ctx, header)
	if err != nil {
		return "", errors.Wrap(err, "failed to set api key header for channel properties")
	}

	req := &api.GetChannelByIdRequest{
		Id: userID,
	}

	channel, err := c.Client.GetChannelById(ctx, req)

	if err != nil {
		return "", errors.Wrap(err, "failed to get stream key from channel properties")
	}

	streamKeyParts := []string{streamKeyPrefix, userID, channel.StreamKey}
	streamKey := strings.Join(streamKeyParts, "_")
	return streamKey, nil
}
