package video_test

import (
	goctx "context" // "context" collides with unexported type in convey
	"testing"

	api "code.justin.tv/amzn/StarfruitTwitchChannelPropsTwirp"
	"code.justin.tv/commerce/nioh/internal/clients/video"
	"code.justin.tv/commerce/nioh/mocks"

	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestChannelProperties(t *testing.T) {
	Convey("Test UsersClient", t, func() {
		mockClient := new(mocks.StarfruitTwitchChannelProps)
		channelProperties := &video.ChannelProperties{
			Client: mockClient,
			APIKey: "fake",
		}

		Convey("Test GetStreamKey", func() {
			Convey("happy case", func() {
				mockClient.On("GetChannelById", mock.Anything, mock.Anything).Return(&api.Channel{
					StreamKey: "hello",
				}, nil)

				result, err := channelProperties.GetStreamKey(goctx.Background(), "123")
				So(err, ShouldBeNil)
				So(result, ShouldEqual, "live_123_hello")
			})

			Convey("with error from APIClient", func() {
				mockClient.On("GetChannelById", mock.Anything, mock.Anything).Return(nil, errors.New("test"))

				_, err := channelProperties.GetStreamKey(goctx.Background(), "123")
				So(err, ShouldNotBeNil)
			})
		})
	})
}
