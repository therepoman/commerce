package siosa

import (
	"context"

	siosaTwirp "code.justin.tv/amzn/SiosaTwirp"
	"code.justin.tv/commerce/nioh/internal/clients/wrapper"
	"code.justin.tv/foundation/twitchclient"
	"github.com/pkg/errors"
)

const (
	// ServiceName is the name of the service for stats display
	ServiceName = "siosa"

	setChannelAdPropertiesAPIName = "SetChannelAdProperties"
)

// ISiosaClient is the siosa ads service client interface
type ISiosaClient interface {
	DisableAdsForChannel(ctx context.Context, channelID string) error
}

// Client is the implementation for siosa ads service client
type Client struct {
	siosaClient siosaTwirp.Siosa
}

// NewSiosaClient returns a new SiosaClient
func NewSiosaClient(cfg twitchclient.ClientConf) ISiosaClient {
	client := twitchclient.NewHTTPClient(cfg)
	protobufClient := siosaTwirp.NewSiosaProtobufClient(cfg.Host, client)
	return &Client{
		siosaClient: protobufClient,
	}
}

// DisableAdsForChannel disables a channel's preroll and postroll ad properties
func (c *Client) DisableAdsForChannel(ctx context.Context, channelID string) error {
	ctx = wrapper.WrapRequestContext(ctx, ServiceName, setChannelAdPropertiesAPIName)
	_, err := c.siosaClient.SetChannelAdProperties(ctx, &siosaTwirp.SetChannelAdPropertiesInput{
		ChannelId: channelID,
		Input: []*siosaTwirp.AdProperty{
			{Property: &siosaTwirp.AdProperty_PrerollsDisabled{PrerollsDisabled: true}},
			{Property: &siosaTwirp.AdProperty_PostrollsDisabled{PostrollsDisabled: true}},
		},
	})

	if err != nil {
		return errors.Wrap(err, "call to siosa.SetChannelAdProperties failed")
	}

	return nil
}
