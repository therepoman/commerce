package siosa

import (
	"context"
	"testing"

	"code.justin.tv/commerce/nioh/internal/clients/siosa/mocks"
	"github.com/pkg/errors"
	"github.com/stretchr/testify/mock"
)

func TestSiosaClient(t *testing.T) {

	// Test conditions
	type input struct {
		siosaReturnsError bool
	}

	// Expected output
	type output struct {
		errorPresent bool
	}

	// Test cases
	tests := []struct {
		name   string
		input  input
		output output
	}{
		{
			name:   "siosa service error",
			input:  input{true},
			output: output{true},
		},
		{
			name:   "siosa succeeds ",
			input:  input{false},
			output: output{false},
		},
	}

	// Test execution loop
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mockSiosa := new(mocks.Siosa)
			if tt.input.siosaReturnsError {
				mockSiosa.On("SetChannelAdProperties", mock.Anything, mock.Anything).Return(nil, errors.New("siosa asplode"))
			} else {
				mockSiosa.On("SetChannelAdProperties", mock.Anything, mock.Anything).Return(nil, nil)
			}

			client := &Client{mockSiosa}
			err := client.DisableAdsForChannel(context.Background(), "1")

			if tt.output.errorPresent && err == nil {
				t.Error("error is unexpectedly nil")
			} else if !tt.output.errorPresent && err != nil {
				t.Error("error is unexpectedly present")
			}
		})
	}
}
