// Code generated by mockery 2.9.0. DO NOT EDIT.

// @generated

package mocks

import (
	context "context"

	request "github.com/aws/aws-sdk-go/aws/request"
	mock "github.com/stretchr/testify/mock"

	sns "github.com/aws/aws-sdk-go/service/sns"
)

// SNSPublisher is an autogenerated mock type for the SNSPublisher type
type SNSPublisher struct {
	mock.Mock
}

// PublishWithContext provides a mock function with given fields: ctx, input, opts
func (_m *SNSPublisher) PublishWithContext(ctx context.Context, input *sns.PublishInput, opts ...request.Option) (*sns.PublishOutput, error) {
	_va := make([]interface{}, len(opts))
	for _i := range opts {
		_va[_i] = opts[_i]
	}
	var _ca []interface{}
	_ca = append(_ca, ctx, input)
	_ca = append(_ca, _va...)
	ret := _m.Called(_ca...)

	var r0 *sns.PublishOutput
	if rf, ok := ret.Get(0).(func(context.Context, *sns.PublishInput, ...request.Option) *sns.PublishOutput); ok {
		r0 = rf(ctx, input, opts...)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*sns.PublishOutput)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, *sns.PublishInput, ...request.Option) error); ok {
		r1 = rf(ctx, input, opts...)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}
