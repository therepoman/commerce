package spade

import (
	"fmt"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/cactus/go-statsd-client/statsd"

	"code.justin.tv/commerce/nioh/config"
	"code.justin.tv/common/spade-client-go/spade"
	"github.com/pkg/errors"
	"golang.org/x/net/context"
)

const (
	// ServiceName is the name of the service for stats display
	ServiceName = "spade"
)

// NewClient allocates a correctly-configured live Spade client using cfg and stats; or, if cfg represents an environment
// in which Spade events should not be sent, allocates a dummy client.
func NewClient(cfg *config.Configuration, stats statsd.Statter, httpClient *http.Client) (spade.Client, error) {
	if !shouldEnableSpade(cfg) {
		return new(NoopClient), nil
	}

	spadeURL, err := url.Parse(cfg.SpadeHost)
	if err != nil {
		return nil, errors.Wrap(err, "failed to parse cfg.SpadeHost when creating Spade client")
	}

	opts := []spade.InitFunc{
		spade.InitBaseURL(*spadeURL),
		spade.InitMaxConcurrency(cfg.SpadeMaxConcurrentRequests),
		spade.InitStatHook(func(name string, httpStatusCode int, d time.Duration) {
			stats.TimingDuration(fmt.Sprintf("service.spade.%s.%d", name, httpStatusCode), d, 1)
		}),
		spade.InitHTTPClient(httpClient),
	}

	return spade.NewClient(opts...)
}

func shouldEnableSpade(cfg *config.Configuration) bool {
	return !strings.HasPrefix(cfg.SpadeHost, "http://0.0.0.0")
}

// NoopClient implements the (code.justin.tv/common/spade-client-go).Client interface with no-op methods.
// It's useful as a dummy client when executing in an environment that should not generate Spade data.
type NoopClient struct{}

// TrackEvent ...
func (*NoopClient) TrackEvent(ctx context.Context, event string, properties interface{}) error {
	return nil
}

// TrackEvents ...
func (*NoopClient) TrackEvents(ctx context.Context, events ...spade.Event) error {
	return nil
}
