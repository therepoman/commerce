package subs

import (
	"context"
	"testing"

	"code.justin.tv/commerce/nioh/internal/clients/subs/mocks"
	substwirp "code.justin.tv/revenue/subscriptions/twirp"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func mockGetProductsByNames(requestedShortname string, product *substwirp.Product, err error) *mocks.Subscriptions {
	m := new(mocks.Subscriptions)

	response := &substwirp.GetProductsByNamesResponse{}
	if product != nil {
		response.Products = []*substwirp.Product{product}
	}

	m.On("GetProductsByNames", mock.Anything, &substwirp.GetProductsByNamesRequest{
		ProductNames: []string{requestedShortname},
	}).Return(response, err)

	return m
}

func mockGetProductsByIDs(requestedProductID string, product *substwirp.Product, err error) *mocks.Subscriptions {
	m := new(mocks.Subscriptions)

	response := &substwirp.GetProductsByIDsResponse{}
	if product != nil {
		response.Products = []*substwirp.Product{product}
	}

	m.On("GetProductsByIDs", mock.Anything, &substwirp.GetProductsByIDsRequest{
		ProductIds: []string{requestedProductID},
	}).Return(response, err)

	return m
}

func TestClient(t *testing.T) {
	Convey("Test Subscriptions client wrapper", t, func() {
		Convey("GetProductByName", func() {
			testShortname := "walrusfest2019"
			testProduct := &substwirp.Product{
				Id:        "1234",
				ShortName: testShortname,
			}

			testCases := []struct {
				description string
				client      substwirp.Subscriptions
				want        *substwirp.Product
				wantErr     bool
			}{
				{
					description: "when product is found",
					client:      mockGetProductsByNames(testShortname, testProduct, nil),
					want:        testProduct,
					wantErr:     false,
				},
				{
					description: "when product is not found",
					client:      mockGetProductsByNames(testShortname, nil, nil),
					want:        nil,
					wantErr:     false,
				},
				{
					description: "when subs returns error",
					client:      mockGetProductsByNames(testShortname, testProduct, errors.New("kaboom")),
					want:        nil,
					wantErr:     true,
				},
			}

			for _, tt := range testCases {
				Convey(tt.description, func() {
					client := &clientImpl{subs: tt.client}
					got, err := client.GetProductByName(context.Background(), testShortname)
					So(got, ShouldResemble, tt.want)
					So(err != nil, ShouldEqual, tt.wantErr)
				})
			}

			Convey("test in-memory caching", func() {
				subsClient := mockGetProductsByNames(testShortname, testProduct, nil)
				client := &clientImpl{
					subs:                     subsClient,
					ticketProductByNameCache: newTicketProductCache(),
					ticketProductByIDCache:   newTicketProductCache(),
				}

				for i := 0; i < 5; i++ {
					product, err := client.GetProductByName(context.Background(), testShortname)
					So(product, ShouldNotBeNil)
					So(err, ShouldBeNil)

					product, err = client.GetProductByID(context.Background(), testProduct.Id)
					So(product, ShouldNotBeNil)
					So(err, ShouldBeNil)
				}

				// first call should populate cache, subsequent calls should hit
				subsClient.AssertNumberOfCalls(t, "GetProductsByNames", 1)
			})
		})

		Convey("GetProductByID", func() {
			testProductID := "1234"
			testProduct := &substwirp.Product{
				Id: testProductID,
			}

			testCases := []struct {
				description string
				client      substwirp.Subscriptions
				want        *substwirp.Product
				wantErr     bool
			}{
				{
					description: "when product is found",
					client:      mockGetProductsByIDs(testProductID, testProduct, nil),
					want:        testProduct,
					wantErr:     false,
				},
				{
					description: "when product is not found",
					client:      mockGetProductsByIDs(testProductID, nil, nil),
					want:        nil,
					wantErr:     false,
				},
				{
					description: "when subs returns error",
					client:      mockGetProductsByIDs(testProductID, testProduct, errors.New("kaboom")),
					want:        nil,
					wantErr:     true,
				},
			}

			for _, tt := range testCases {
				Convey(tt.description, func() {
					client := &clientImpl{subs: tt.client}
					got, err := client.GetProductByID(context.Background(), testProductID)
					So(got, ShouldResemble, tt.want)
					So(err != nil, ShouldEqual, tt.wantErr)
				})
			}
		})

	})
}
