package subs

import (
	"context"
	"net/http"
	"time"

	"github.com/patrickmn/go-cache"

	"code.justin.tv/commerce/logrus"

	"github.com/golang/protobuf/ptypes"

	"code.justin.tv/commerce/nioh/internal/clients/wrapper"
	substwirp "code.justin.tv/revenue/subscriptions/twirp"
)

// Client wraps a substwirp.Subscriptions client, providing versions of its methods.
type Client interface {
	GetProductByName(ctx context.Context, productShortName string) (*substwirp.Product, error)
	GetProductByID(ctx context.Context, productID string) (*substwirp.Product, error)
	GetProductsByIDs(ctx context.Context, productIDs []string) map[string]*substwirp.Product
	GetChannelProducts(ctx context.Context, channelID string) ([]*substwirp.Product, error)

	// For testing only
	CreateTestSubscription(ctx context.Context, userID string, productID string) (*substwirp.Subscription, error)
	CancelTestSubscription(ctx context.Context, userID string, productID string) error
}

type clientImpl struct {
	subs substwirp.Subscriptions
	// ticketProductByNameCache caches the mapping of ticket product shortname -> ticket product
	ticketProductByNameCache *cache.Cache
	// ticketProductByIDCache caches the mapping of ticket product id -> ticket product
	ticketProductByIDCache *cache.Cache
}

const (
	// ServiceName is dimension of the subscription service appearing in stats/metrics
	ServiceName = "subscriptions"

	getProductsByNamesAPIName = "GetProductsByNames"
	getProductsByIDsAPIName   = "GetProductsByIDs"
	createSubscriptionAPIName = "CreateSubscription"
	cancelSubscriptionAPIName = "CancelSubscription"
	getChannelProductsAPIName = "GetChannelProducts"
)

const (
	ticketProductCacheTTL     = 60 * time.Minute
	ticketProductCacheCleanup = 2 * time.Hour
)

var _ Client = &clientImpl{}

// NewClient instantiates a Client configured with the passed subscriptions service host as `address` and an HTTP timeout.
func NewClient(address string, httpClient *http.Client) Client {
	subsClient := substwirp.NewSubscriptionsProtobufClient(address, httpClient)

	return &clientImpl{
		subs:                     subsClient,
		ticketProductByNameCache: newTicketProductCache(),
		ticketProductByIDCache:   newTicketProductCache(),
	}
}

func newTicketProductCache() *cache.Cache {
	return cache.New(ticketProductCacheTTL, ticketProductCacheCleanup)
}

func (c *clientImpl) GetProductByName(ctx context.Context, shortName string) (*substwirp.Product, error) {
	if c.ticketProductByNameCache != nil {
		if product, found := c.ticketProductByNameCache.Get(shortName); found {
			return product.(*substwirp.Product), nil
		}
	}

	ctx = wrapper.WrapRequestContext(ctx, ServiceName, getProductsByNamesAPIName)
	resp, err := c.subs.GetProductsByNames(ctx, &substwirp.GetProductsByNamesRequest{
		ProductNames: []string{shortName},
	})
	if err != nil {
		return nil, err
	}

	var product *substwirp.Product
	if len(resp.Products) > 0 {
		product = resp.Products[0]
	}

	if product != nil {
		if c.ticketProductByNameCache != nil {
			c.ticketProductByNameCache.Set(shortName, product, cache.DefaultExpiration)
		}
		if c.ticketProductByIDCache != nil {
			c.ticketProductByIDCache.Set(product.Id, product, cache.DefaultExpiration)
		}
	}

	return product, nil
}

func (c *clientImpl) GetProductByID(ctx context.Context, productID string) (*substwirp.Product, error) {
	if c.ticketProductByIDCache != nil {
		if product, found := c.ticketProductByIDCache.Get(productID); found {
			return product.(*substwirp.Product), nil
		}
	}

	ctx = wrapper.WrapRequestContext(ctx, ServiceName, getProductsByIDsAPIName)
	resp, err := c.subs.GetProductsByIDs(ctx, &substwirp.GetProductsByIDsRequest{
		ProductIds: []string{productID},
	})
	if err != nil {
		return nil, err
	}

	var product *substwirp.Product
	if len(resp.Products) > 0 {
		product = resp.Products[0]
	}

	if product != nil {
		if c.ticketProductByNameCache != nil {
			c.ticketProductByNameCache.Set(product.ShortName, product, cache.DefaultExpiration)
		}
		if c.ticketProductByIDCache != nil {
			c.ticketProductByIDCache.Set(productID, product, cache.DefaultExpiration)
		}
	}

	return product, nil
}

func (c *clientImpl) GetProductsByIDs(ctx context.Context, productIDs []string) map[string]*substwirp.Product {
	if len(productIDs) == 0 {
		return nil
	}

	uncachedProductIDs := make([]string, 0, len(productIDs))
	products := make(map[string]*substwirp.Product, len(productIDs))

	for _, productID := range productIDs {
		if c.ticketProductByIDCache != nil {
			if product, found := c.ticketProductByIDCache.Get(productID); found {
				products[productID] = product.(*substwirp.Product)
			} else {
				uncachedProductIDs = append(uncachedProductIDs, productID)
			}
		}
	}

	// All products found in cache. Return early.
	if len(uncachedProductIDs) == 0 {
		return products
	}

	ctx = wrapper.WrapRequestContext(ctx, ServiceName, getProductsByIDsAPIName)
	resp, err := c.subs.GetProductsByIDs(ctx, &substwirp.GetProductsByIDsRequest{
		ProductIds: uncachedProductIDs,
	})
	if err != nil {
		logrus.WithError(err).Error("failed to call GetProductsByIDs")
		return products
	}

	for _, product := range resp.Products {
		if c.ticketProductByNameCache != nil {
			c.ticketProductByNameCache.Set(product.ShortName, product, cache.DefaultExpiration)
		}
		if c.ticketProductByIDCache != nil {
			c.ticketProductByIDCache.Set(product.Id, product, cache.DefaultExpiration)
		}
		products[product.Id] = product
	}

	return products
}

// CreateTestSubscription creates a subscription for a user for testing
func (c *clientImpl) CreateTestSubscription(ctx context.Context, userID string, productID string) (*substwirp.Subscription, error) {
	ctx = wrapper.WrapRequestContext(ctx, ServiceName, createSubscriptionAPIName)
	// The subscriptions Redis cache TTL is as long as a sub takes to expire. Keep the BenefitEnd as small as possible to avoid
	// having to invalidate the subs cache in case of desync between the subs DB and cache in staging env.
	pastProto, _ := ptypes.TimestampProto(time.Now().Add(-5 * time.Minute))
	futureProto, _ := ptypes.TimestampProto(time.Now().Add(5 * time.Minute))
	req := &substwirp.CreateSubscriptionRequest{
		OwnerId:          userID,
		ProductId:        productID,
		OriginId:         "nioh-test",
		BenefitStart:     pastProto,
		BenefitEnd:       futureProto,
		SubscriptionType: substwirp.SubscriptionType_SUBSCRIPTION_PAID,
	}

	res, err := c.subs.CreateSubscription(ctx, req)
	if err != nil {
		return nil, err
	}

	return res.Subscription, nil
}

// CancelTestSubscription cancels a subscription for a user. Used for test subscription.
func (c *clientImpl) CancelTestSubscription(ctx context.Context, userID string, productID string) error {
	ctx = wrapper.WrapRequestContext(ctx, ServiceName, cancelSubscriptionAPIName)
	req := &substwirp.CancelSubscriptionRequest{
		OwnerId:   userID,
		ProductId: productID,
		OriginId:  "nioh-test",
	}
	_, err := c.subs.CancelSubscription(ctx, req)
	return err
}

// GetChannelProducts ...
func (c *clientImpl) GetChannelProducts(ctx context.Context, channelID string) ([]*substwirp.Product, error) {
	ctx = wrapper.WrapRequestContext(ctx, ServiceName, getChannelProductsAPIName)
	req := &substwirp.GetChannelProductsRequest{
		ChannelId: channelID,
	}

	res, err := c.subs.GetChannelProducts(ctx, req)

	if err != nil {
		return nil, err
	}

	return res.Products, nil
}
