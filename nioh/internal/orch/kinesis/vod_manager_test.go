package kinesismanager

import (
	"testing"

	"fmt"

	"sync"

	"code.justin.tv/commerce/nioh/internal/backend/mocks"
	"code.justin.tv/commerce/nioh/internal/models"
	"github.com/stretchr/testify/mock"
)

func TestProcessRecords(t *testing.T) {
	type testmocks struct {
		backend *mocks.Backend
	}
	userID := 123
	videoID := 234

	tests := []struct {
		name               string
		message            string
		overrideMocks      func(*testmocks)
		assertExpectations func(t *testing.T, backend *mocks.Backend)
	}{
		{
			name:    "valid record - happy path",
			message: fmt.Sprintf(`{"event_type":"created","vod":{"broadcast_type":"archive","created_at":"2019-05-15T18:45:26Z","id":%d,"owner_id": %d}}`, videoID, userID),
			assertExpectations: func(t *testing.T, backend *mocks.Backend) {
				backend.AssertNumberOfCalls(t, "GetRestriction", 1)
				backend.AssertNumberOfCalls(t, "SetRestriction", 1)
			},
		},
		{
			name:    "valid record - record has no restriction",
			message: fmt.Sprintf(`{"event_type":"created","vod":{"broadcast_type":"archive","created_at":"2019-05-15T18:45:26Z","id":%d,"owner_id": %d}}`, videoID, userID),
			overrideMocks: func(testmocks *testmocks) {
				testmocks.backend = new(mocks.Backend)
				testmocks.backend.On("GetRestriction", mock.Anything, mock.Anything, mock.Anything).Return(nil, nil)
			},
			assertExpectations: func(t *testing.T, backend *mocks.Backend) {
				backend.AssertNumberOfCalls(t, "GetRestriction", 1)
				backend.AssertNotCalled(t, "SetRestriction")
			},
		},
		{
			name:    "invalid record - incorrect event type",
			message: fmt.Sprintf(`{"event_type":"deleted","vod":{"broadcast_type":"archive","created_at":"2019-05-15T18:45:26Z","id":%d,"owner_id": %d}}`, videoID, userID),
			assertExpectations: func(t *testing.T, backend *mocks.Backend) {
				backend.AssertNotCalled(t, "GetRestriction")
				backend.AssertNotCalled(t, "SetRestriction")
			},
		},
		{
			name:    "invalid record - incorrect owner ID",
			message: fmt.Sprintf(`{"event_type":"deleted","vod":{"broadcast_type":"archive","created_at":"2019-05-15T18:45:26Z","id":%d,"owner_id": %d}}`, videoID, 0),
			assertExpectations: func(t *testing.T, backend *mocks.Backend) {
				backend.AssertNotCalled(t, "GetRestriction")
				backend.AssertNotCalled(t, "SetRestriction")
			},
		},
		{
			name:    "invalid record - incorrect broadcast type",
			message: fmt.Sprintf(`{"event_type":"created","vod":{"broadcast_type":"","created_at":"2019-05-15T18:45:26Z","id":%d,"owner_id": %d}}`, videoID, userID),
			assertExpectations: func(t *testing.T, backend *mocks.Backend) {
				backend.AssertNotCalled(t, "GetRestriction")
				backend.AssertNotCalled(t, "SetRestriction")
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mockInstances := testmocks{
				backend: new(mocks.Backend),
			}
			mockInstances.backend.On("GetRestriction", mock.Anything, mock.Anything, mock.Anything).Return(&models.RestrictionV2{
				RestrictionType: models.SubOnlyLiveRestrictionType,
			}, nil)
			mockInstances.backend.On("SetRestriction", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil, nil)

			if tt.overrideMocks != nil {
				tt.overrideMocks(&mockInstances)
			}

			var wg sync.WaitGroup
			wg.Add(1)

			records := make(chan []byte, 1)
			go func() { defer wg.Done(); ProcessRecords(mockInstances.backend, records) }()
			records <- []byte(tt.message)
			close(records)

			wg.Wait()
			tt.assertExpectations(t, mockInstances.backend)
		})
	}
}
