package kinesismanager

// Most of this code had been taken from the kinsumer example found in the Twitch docs
// https://git-aws.internal.justin.tv/twitch/docs/blob/master/datainfra/kinesis/kinsumer_example.go

import (
	"encoding/json"

	"context"
	"strconv"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/nioh/internal/backend"
	"code.justin.tv/commerce/nioh/internal/models"
	"github.com/pkg/errors"
	"github.com/twitchscience/kinsumer"
)

// VinylRecord is the JSON object returned from Kinsumer
type VinylRecord struct {
	EventType string    `json:"event_type"`
	VOD       VODRecord `json:"vod"`
}

// VODRecord is the JSON object for the `VOD` field in VinylRecord
type VODRecord struct {
	ID            int    `json:"id"`
	BroadcastType string `json:"broadcast_type"`
	CreatedAt     string `json:"created_at"`
	OwnerID       int    `json:"owner_id"`
}

// ConsumeRecords pulls messages from a Kinesis stream
func ConsumeRecords(k *kinsumer.Kinsumer, records chan []byte) {
	defer close(records)

	for {
		record, err := k.Next()
		if err != nil {
			log.Errorf("k.Next returned error %v", errors.WithStack(err))
		}
		// The kinsumer has been stopped.
		if record == nil {
			return
		}
		records <- record
	}
}

// ProcessRecords parses the record and validates the data before modifying a restriction
func ProcessRecords(backendInstance backend.Backend, records chan []byte) {
	for record := range records {
		event, err := extractEvent(record)
		if err != nil {
			log.Errorf("Unable to extract event: %v", errors.WithStack(err))
			continue
		}

		if !isValidEvent(event) {
			continue
		}

		// Check to see if the owner channel is currently restricted
		channelModel := models.Channel{ID: strconv.Itoa(event.VOD.OwnerID)}
		restriction, err := backendInstance.GetRestriction(context.Background(), channelModel, false)
		if err != nil {
			log.Errorf("unable to get restriction for vod owner (%d): %v", event.VOD.OwnerID, errors.WithStack(err))
			continue
		}

		// If no restriction exists for the channel or restriction is unknown, skip the VOD
		if restriction == nil || restriction.RestrictionType == models.UnknownRestrictionType {
			continue
		}

		// Set the restriction for the VOD
		vodModel := models.VOD{ID: strconv.Itoa(event.VOD.ID), OwnerID: strconv.Itoa(event.VOD.OwnerID)}
		_, err = backendInstance.SetRestriction(context.Background(), vodModel, *restriction)
		if err != nil {
			log.Errorf("unable to set restriction for vod: %v", errors.WithStack(err))
			continue
		}

		log.Infof("Successfully created restriction for VOD %d", event.VOD.ID)
	}
}

func extractEvent(record []byte) (*VinylRecord, error) {
	var kinesisRecord VinylRecord
	err := json.Unmarshal(record, &kinesisRecord)
	if err != nil {
		return nil, errors.Wrap(errors.WithStack(err), "json Unmarshal error")
	}

	return &kinesisRecord, nil
}

func isValidEvent(event *VinylRecord) bool {
	if event.EventType != "created" {
		return false
	}

	if event.VOD.OwnerID == 0 {
		return false
	}

	if event.VOD.BroadcastType != "archive" {
		return false
	}

	return true
}
