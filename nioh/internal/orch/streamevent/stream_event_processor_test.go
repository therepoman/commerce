package streamevent

import (
	"context"
	"encoding/json"
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	"github.com/aws/aws-sdk-go/service/sqs"
)

type handlerStub struct {
	err        error
	calledWith *StreamEvent
}

func (h *handlerStub) f(ctx context.Context, event *StreamEvent) error {
	h.calledWith = event
	return h.err
}

type resultWriterStub struct {
	failed bool
}

func (r *resultWriterStub) Fail(error) {
	r.failed = true
}
func (r *resultWriterStub) ChangeMessageVisibility(time.Duration) {}

func TestStreamEventProcessor_process(t *testing.T) {
	var goodMsgString = `{"event":"down","channel_id":1234,"channel":"coolstreamer","stream_id":4321,"session-id":"4321"}`
	var goodStreamEvent = new(StreamEvent)

	err := json.Unmarshal([]byte(goodMsgString), goodStreamEvent)
	if err != nil {
		panic(err)
	}

	var badMsgString = `( ͡° ͜ʖ ͡°) ｎｏｔ ｊｓｏｎ`

	tests := []struct {
		name          string
		handlers      []*handlerStub
		message       *sqs.Message
		expectedEvent *StreamEvent
		wantFailure   bool
	}{
		{
			name:          "happy path with no handlers - no failure",
			message:       &sqs.Message{Body: &goodMsgString},
			expectedEvent: goodStreamEvent,
			wantFailure:   false,
		},
		{
			name:          "happy path with one handler - no failure, handler is called",
			message:       &sqs.Message{Body: &goodMsgString},
			handlers:      []*handlerStub{{}},
			expectedEvent: goodStreamEvent,
			wantFailure:   false,
		},
		{
			name:          "happy path with many handlers - no failure, all are called with expected event",
			message:       &sqs.Message{Body: &goodMsgString},
			handlers:      []*handlerStub{{}, {}, {}},
			expectedEvent: goodStreamEvent,
			wantFailure:   false,
		},
		{
			name:          "with malformed message body",
			message:       &sqs.Message{Body: &badMsgString},
			handlers:      []*handlerStub{{}},
			expectedEvent: nil,
			wantFailure:   true,
		},
		{
			name:          "with nil message body",
			message:       &sqs.Message{Body: nil},
			handlers:      []*handlerStub{{}},
			expectedEvent: nil,
			wantFailure:   true,
		},
		{
			name:          "with failure from one handler, with one handler",
			message:       &sqs.Message{Body: &goodMsgString},
			handlers:      []*handlerStub{{err: fmt.Errorf("your handler asplode")}},
			expectedEvent: goodStreamEvent,
			wantFailure:   true,
		},
		{
			name:          "with early failure from one handler, with many handlers - all are called",
			message:       &sqs.Message{Body: &goodMsgString},
			handlers:      []*handlerStub{{err: fmt.Errorf("your handler asplode")}, {}, {}, {}},
			expectedEvent: goodStreamEvent,
			wantFailure:   true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			resultStub := new(resultWriterStub)

			var handlers []Handler
			for _, stub := range tt.handlers {
				handlers = append(handlers, HandleFunc(stub.f))
			}

			s := &Processor{
				handlers: handlers,
			}
			s.process(context.TODO(), resultStub, tt.message)

			assert.Equal(t, tt.wantFailure, resultStub.failed)
			for _, stub := range tt.handlers {
				assert.Equal(t, tt.expectedEvent, stub.calledWith)
			}
		})
	}
}
