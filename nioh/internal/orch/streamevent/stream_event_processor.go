package streamevent

import (
	"context"
	"encoding/json"
	"strconv"

	"github.com/pkg/errors"

	"code.justin.tv/commerce/nioh/config"
	sqsmessage "code.justin.tv/growth/sqsmessage/consumer"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/aws/aws-sdk-go/service/sqs/sqsiface"
	"github.com/cactus/go-statsd-client/statsd"
)

// Handler is the interface for consuming StreamEvents from SQS.
type Handler interface {
	Handle(ctx context.Context, message *StreamEvent) error
}

// HandleFunc is a convenience adapter type for using a function as a Handler.
type HandleFunc func(context.Context, *StreamEvent) error

// Handle implements Handler.
func (s HandleFunc) Handle(ctx context.Context, message *StreamEvent) error {
	return s(ctx, message)
}

// Processor receives stream event messages from SQS and delegates them to its Handlers.
type Processor struct {
	sqsConsumer *sqsmessage.Consumer
	handlers    []Handler
}

// NewProcessor allocates and returns a Processor ready for use. It does not receive and process messages until Start is called.
func NewProcessor(cfg *config.Configuration, sqsClient sqsiface.SQSAPI, statter statsd.Statter, handlers []Handler) (*Processor, error) {
	s := new(Processor)

	consumerConfig := sqsmessage.ConsumerConfig{
		SQS:        sqsClient,
		QueueName:  cfg.StreamEventsSQSName,
		NumPollers: 1,
		Stats:      statter,
		Handler:    sqsmessage.HandlerFunc(s.process),
	}

	consumer, err := sqsmessage.NewConsumer(consumerConfig)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create new SQS consumer")
	}

	err = consumer.Initialize()
	if err != nil {
		return nil, errors.Wrap(err, "failed to initialize SQS consumer")
	}

	s.sqsConsumer = consumer
	s.handlers = handlers

	return s, err
}

// Start causes the Processor to begin receiving and processing messages from SQS.
func (s *Processor) Start(ctx context.Context) error {
	return s.sqsConsumer.Start()
}

// Shutdown causes the Processor to begin gracefully shutting down. It returns an error if the passed context
// is marked done before the shutdown has gracefully completed.
func (s *Processor) Shutdown(ctx context.Context) error {
	go s.sqsConsumer.Shutdown()

	done := make(chan struct{})
	go func() {
		s.sqsConsumer.Wait()
		close(done)
	}()

	select {
	case <-ctx.Done():
		return errors.New("timed out while shutting down")
	case <-done:
		return nil
	}
}

func (s *Processor) process(ctx context.Context, w sqsmessage.ResultWriter, message *sqs.Message) {
	if message.Body == nil {
		w.Fail(errors.New("message body was nil"))
		return
	}

	event := new(StreamEvent)
	err := json.Unmarshal([]byte(*message.Body), event)
	if err != nil {
		w.Fail(errors.Wrap(err, "message body could not be parsed"))
		return
	}

	for _, handler := range s.handlers {
		err := handler.Handle(ctx, event)
		if err != nil {
			w.Fail(errors.Wrap(err, "stream event handler returned with error"))
		}
	}
}

const (
	// streamReadyEvent is the string value for StreamEvent.Event that indicates a stream has become live ("ready" for consumption)
	streamReadyEvent = "ready"
	// streamDownEvent is the string value for StreamEvent.Event that indicates a stream has ended
	streamDownEvent = "down"
)

// NumEncoded is a Go string that unmarshals itself from a JSON number.
// The Ingest notifications encode channel ID and stream ID as JSON numbers,
// despite how they're opaque identifiers. NumEncoded normalizes this during unmarshaling.
type NumEncoded string

// String returns the NumEncoded's value as a `string`.
func (n *NumEncoded) String() string {
	if n == nil {
		return ""
	}
	return string(*n)
}

// UnmarshalJSON decodes bytes representing a JSON number into a NumEncoded.
func (n *NumEncoded) UnmarshalJSON(in []byte) error {
	var asNum int
	err := json.Unmarshal(in, &asNum)
	if err != nil {
		return err
	}
	*n = NumEncoded(strconv.Itoa(asNum))
	return nil
}

// StreamEvent represents the shape of messages received from Ingest indicating stream lifecycle events.
type StreamEvent struct {
	// Event is either "ready" or "down"
	Event     string     `json:"event"`
	ChannelID NumEncoded `json:"channel_id"`
	Channel   string     `json:"channel"`
	StreamID  NumEncoded `json:"stream_id"`
	SessionID string     `json:"session_id"`
}
