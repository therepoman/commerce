package streamevent

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"code.justin.tv/commerce/nioh/pkg/loaders"

	"code.justin.tv/commerce/nioh/internal/clients"

	"code.justin.tv/commerce/nioh/config"

	"code.justin.tv/commerce/logrus"

	"github.com/aws/aws-sdk-go/service/sfn"

	"code.justin.tv/commerce/nioh/internal/backend"
	"code.justin.tv/commerce/nioh/internal/models"
	"code.justin.tv/commerce/nioh/internal/orch/autotag"
)

// OfflineCleanupScheduler is a Handler that, when appropriate, starts an execution of the offline cleanup state machine,
// effectively scheduling a later check to clean up a channel's restriction.
type OfflineCleanupScheduler struct {
	executor                      clients.SFNExecutor
	clock                         models.IClock
	backend                       backend.Backend
	offlineCleanupStateMachineArn string
	loaders                       loaders.Loaders
}

// OfflineCleanupSchedulerInput gathers the dependencies of an OfflineCleanupScheduler.
type OfflineCleanupSchedulerInput struct {
	Config      *config.Configuration
	SFNExecutor clients.SFNExecutor
	Clock       models.IClock
	Backend     backend.Backend
	Clients     *clients.Clients
}

// NewOfflineCleanupScheduler allocates and returns an OfflineCleanupScheduler ready for use.
func NewOfflineCleanupScheduler(input *OfflineCleanupSchedulerInput) *OfflineCleanupScheduler {
	return &OfflineCleanupScheduler{
		executor:                      input.SFNExecutor,
		clock:                         input.Clock,
		backend:                       input.Backend,
		offlineCleanupStateMachineArn: input.Config.OfflineCleanupStateMachineArn,
		loaders:                       loaders.New(input.Clients),
	}
}

// Handle implements Handler.
func (o *OfflineCleanupScheduler) Handle(ctx context.Context, event *StreamEvent) error {
	if event.Event != streamDownEvent {
		return nil
	}

	ctx = o.loaders.Attach(ctx)

	channelID := event.ChannelID.String()

	restriction, err := o.backend.GetRestriction(ctx, models.Channel{ID: channelID}, false)
	if err != nil {
		return err
	}
	if restriction == nil {
		return nil
	}

	if restriction.RestrictionType != models.SubOnlyLiveRestrictionType {
		return nil
	}

	chanlet, err := o.backend.GetChanletByID(ctx, channelID)
	if err != nil {
		return err
	}

	if chanlet != nil && chanlet.OwnerChannelID != chanlet.ChanletID {
		// don't clean up for a substream, since it's more likely that it's intended to always be restricted
		return nil
	}

	return o.scheduleRestrictionCleanup(ctx, channelID, restriction)
}

func (o *OfflineCleanupScheduler) scheduleRestrictionCleanup(ctx context.Context, channelID string, restriction *models.RestrictionV2) error {
	now := o.clock.Now()

	data := offlineCleanupExecutionData{
		ChannelID:       channelID,
		RestrictionType: restriction.RestrictionType,
		Timestamp:       now,
	}

	dataBytes, err := json.Marshal(data)
	if err != nil {
		return err
	}
	dataString := string(dataBytes)

	name := fmt.Sprintf("%s-%d", channelID, now.UnixNano())

	sfnInput := sfn.StartExecutionInput{
		Input:           &dataString,
		Name:            &name,
		StateMachineArn: &o.offlineCleanupStateMachineArn,
	}

	out, err := o.executor.StartExecutionWithContext(ctx, &sfnInput)
	if err != nil {
		return err
	}

	logrus.WithFields(logrus.Fields{
		"ChannelID":    channelID,
		"Restriction":  restriction,
		"ExecutionArn": *out.ExecutionArn,
	}).Info("scheduled offline cleanup")

	return nil
}

type offlineCleanupExecutionData struct {
	ChannelID       string
	RestrictionType models.RestrictionType
	Timestamp       time.Time
}

// OnlineAutotagHandler is a Handler that pushes an appropriate tag event to Graffiti's SNS on stream online events.
// Its job is to push an auto tagging event only when the stream is restricted and becoming online.
// This addresses the edge case where a long duration stream can lose the Subs only tag due to Graffiti TTL.
type OnlineAutotagHandler struct {
	autotagger autotag.Autotagger
	backend    backend.Backend
}

// NewOnlineAutotagHandler returns a new instance of OnlineAutotagHandler
func NewOnlineAutotagHandler(autotagger autotag.Autotagger, be backend.Backend) *OnlineAutotagHandler {
	return &OnlineAutotagHandler{
		autotagger: autotagger,
		backend:    be,
	}
}

// Handle implements Handler.
func (o *OnlineAutotagHandler) Handle(ctx context.Context, event *StreamEvent) error {
	if event.Event != streamReadyEvent {
		return nil
	}

	channelID := event.ChannelID.String()

	restriction, err := o.backend.GetRestriction(ctx, models.Channel{ID: channelID}, false)
	if err != nil {
		return err
	}

	return o.updateAutotag(ctx, restriction, channelID)
}

// updateAutotag takes restriction and channel ID, then calls the underlying UpdateAutotag only if appropriate.
func (o *OnlineAutotagHandler) updateAutotag(ctx context.Context, restriction *models.RestrictionV2, channelID string) error {
	// This handler only addresses the case where the stream up event is SubOnlyLive restricted.
	if restriction == nil || restriction.RestrictionType != models.SubOnlyLiveRestrictionType {
		return nil
	}

	resource := &models.Channel{ID: channelID}
	updateEvent := models.RestrictionUpdateEvent{
		Resource:    resource,
		Restriction: restriction,
	}

	return o.autotagger.UpdateAutotag(ctx, updateEvent)
}
