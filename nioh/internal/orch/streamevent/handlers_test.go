package streamevent

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"testing"
	"time"

	"code.justin.tv/commerce/nioh/internal/clients"

	"github.com/aws/aws-sdk-go/service/sfn"

	"code.justin.tv/commerce/nioh/config"
	"github.com/stretchr/testify/assert"

	"github.com/stretchr/testify/mock"

	clientMocks "code.justin.tv/commerce/nioh/internal/clients/mocks"
	"code.justin.tv/commerce/nioh/internal/models"

	backendMocks "code.justin.tv/commerce/nioh/internal/backend/mocks"
	modelsMocks "code.justin.tv/commerce/nioh/internal/models/mocks"
	autotagMocks "code.justin.tv/commerce/nioh/internal/orch/autotag/mocks"
)

func TestOfflineCleanupScheduler_Handle(t *testing.T) {
	type testmocks struct {
		backend *backendMocks.Backend
		clock   *modelsMocks.IClock
		sfn     *clientMocks.SFNExecutor
	}

	testTime := time.Date(2016, time.January, 1, 0, 0, 0, 0, time.UTC)

	readyEvent := StreamEvent{
		Event:     streamReadyEvent,
		ChannelID: "test-channel-id",
		Channel:   "test-channel",
		StreamID:  "test-stream",
		SessionID: "test-session",
	}

	downEvent := readyEvent
	downEvent.Event = streamDownEvent

	tests := []struct {
		name          string
		overrideMocks func(*testmocks)
		event         *StreamEvent
		restriction   *models.RestrictionV2
		chanlet       *models.Chanlet
		wantExecution bool
		wantErr       bool
	}{
		{
			name:          `stream event is "ready" - does not execute`,
			event:         &readyEvent,
			restriction:   &models.RestrictionV2{RestrictionType: models.SubOnlyLiveRestrictionType},
			wantExecution: false,
			wantErr:       false,
		},
		{
			name:          `stream event is "down" with no restriction - does not execute`,
			event:         &downEvent,
			restriction:   nil,
			wantExecution: false,
			wantErr:       false,
		},
		{
			name:          `stream event is "down" with non-SOL restriction - does not execute`,
			event:         &downEvent,
			restriction:   &models.RestrictionV2{RestrictionType: models.AllAccessPassRestrictionType},
			wantExecution: false,
			wantErr:       false,
		},
		{
			name:          `stream event is "down" with SOL restriction - executes`,
			event:         &downEvent,
			restriction:   &models.RestrictionV2{RestrictionType: models.SubOnlyLiveRestrictionType},
			wantExecution: true,
			wantErr:       false,
		},
		{
			name:  "with error from GetRestriction - does not execute",
			event: &downEvent,
			overrideMocks: func(t *testmocks) {
				t.backend = new(backendMocks.Backend)
				t.backend.On("GetRestriction", mock.Anything, mock.Anything, mock.Anything).
					Return(nil, errors.New("your backend asplode"))
			},
			wantExecution: false,
			wantErr:       true,
		},
		{
			name:        "with error from StartExecution - returns error",
			event:       &downEvent,
			restriction: &models.RestrictionV2{RestrictionType: models.SubOnlyLiveRestrictionType},
			overrideMocks: func(t *testmocks) {
				t.sfn = new(clientMocks.SFNExecutor)
				t.sfn.On("StartExecutionWithContext", mock.Anything, mock.Anything).
					Return(nil, errors.New("your managed service asplode"))
			},
			wantExecution: true,
			wantErr:       true,
		},
		{
			name:        `stream event is "down" with SOL restriction for substream chanlet - does not execute`,
			event:       &downEvent,
			restriction: &models.RestrictionV2{RestrictionType: models.SubOnlyLiveRestrictionType},
			chanlet: &models.Chanlet{
				OwnerChannelID: "parent ID",
				ChanletID:      "substream ID",
			},
			wantExecution: false,
			wantErr:       false,
		},
		{
			name:        `stream event is "down" with SOL restriction for main chanlet - executes`,
			event:       &downEvent,
			restriction: &models.RestrictionV2{RestrictionType: models.SubOnlyLiveRestrictionType},
			chanlet: &models.Chanlet{
				OwnerChannelID: "parent ID",
				ChanletID:      "parent ID",
			},
			wantExecution: true,
			wantErr:       false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mockInstances := testmocks{
				backend: new(backendMocks.Backend),
				clock:   new(modelsMocks.IClock),
				sfn:     new(clientMocks.SFNExecutor),
			}
			mockInstances.backend.On("GetRestriction", mock.Anything, mock.Anything, mock.Anything).Return(tt.restriction, nil)
			mockInstances.backend.On("GetChanletByID", mock.Anything, mock.Anything).Return(tt.chanlet, nil)
			mockInstances.clock.On("Now").Return(testTime)
			mockInstances.sfn.On("StartExecutionWithContext", mock.Anything, mock.Anything).Return(&sfn.StartExecutionOutput{
				ExecutionArn: new(string),
			}, nil)

			if tt.overrideMocks != nil {
				tt.overrideMocks(&mockInstances)
			}

			arn := "test-arn"

			handler := NewOfflineCleanupScheduler(&OfflineCleanupSchedulerInput{
				Config:      &config.Configuration{OfflineCleanupStateMachineArn: arn},
				SFNExecutor: mockInstances.sfn,
				Clock:       mockInstances.clock,
				Backend:     mockInstances.backend,
				Clients:     &clients.Clients{},
			})

			err := handler.Handle(context.TODO(), tt.event)
			assert.Equal(t, tt.wantErr, err != nil, "err != nil (%v) should equal tt.wantErr (%v)", err != nil, tt.wantErr)

			if tt.wantExecution {
				expectedInput := &sfn.StartExecutionInput{}
				expectedInput.SetStateMachineArn(arn)
				expectedInput.SetName(fmt.Sprintf("%s-%d", tt.event.ChannelID.String(), testTime.UnixNano()))

				expectedData := offlineCleanupExecutionData{
					ChannelID:       tt.event.ChannelID.String(),
					RestrictionType: tt.restriction.RestrictionType,
					Timestamp:       testTime,
				}
				bodyBytes, err := json.Marshal(expectedData)
				if err != nil {
					t.Fatal(err)
				}

				expectedInput.SetInput(string(bodyBytes))

				mockInstances.sfn.AssertCalled(t, "StartExecutionWithContext", mock.Anything, expectedInput)
			} else {
				mockInstances.sfn.AssertNotCalled(t, "StartExecutionWithContext")
			}
		})
	}
}

func TestOnlineAutotagHandler_Handle(t *testing.T) {
	type testmocks struct {
		backend    *backendMocks.Backend
		autotagger *autotagMocks.Autotagger
	}

	readyEvent := StreamEvent{
		Event:     streamReadyEvent,
		ChannelID: "123123123",
		Channel:   "channel",
		StreamID:  "123456",
		SessionID: "session-id",
	}

	downEvent := readyEvent
	downEvent.Event = streamDownEvent

	tests := []struct {
		name          string
		event         *StreamEvent
		restriction   *models.RestrictionV2
		backendError  bool
		wantExecution bool
		wantErr       bool
	}{
		{
			name:          `stream event is "ready" with restriction - execute`,
			event:         &readyEvent,
			restriction:   &models.RestrictionV2{RestrictionType: models.SubOnlyLiveRestrictionType},
			wantExecution: true,
		},
		{
			name:  `stream event is "ready" with no restriction - does not execute`,
			event: &readyEvent,
		},
		{
			name:        `stream event is "down" with restriction - does not execute`,
			event:       &downEvent,
			restriction: &models.RestrictionV2{RestrictionType: models.AllAccessPassRestrictionType},
		},
		{
			name:  `stream event is "down" with no restriction - does not execute`,
			event: &downEvent,
		},
		{
			name:         "with error from GetRestriction - does not execute",
			event:        &readyEvent,
			backendError: true,
			wantErr:      true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mockInstances := testmocks{
				backend:    new(backendMocks.Backend),
				autotagger: new(autotagMocks.Autotagger),
			}
			if tt.backendError {
				mockInstances.backend.On("GetRestriction", mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("your backend asplode"))
			} else {
				mockInstances.backend.On("GetRestriction", mock.Anything, mock.Anything, mock.Anything).Return(tt.restriction, nil)
			}

			mockInstances.autotagger.On("UpdateAutotag", mock.Anything, mock.Anything).Return(nil)

			handler := NewOnlineAutotagHandler(mockInstances.autotagger, mockInstances.backend)

			err := handler.Handle(context.TODO(), tt.event)
			assert.Equal(t, tt.wantErr, err != nil, "err != nil (%v) should equal tt.wantErr (%v)", err != nil, tt.wantErr)

			if tt.wantExecution {
				expectedChange := models.RestrictionUpdateEvent{
					Resource:    &models.Channel{ID: tt.event.ChannelID.String()},
					Restriction: tt.restriction,
				}
				mockInstances.autotagger.AssertCalled(t, "UpdateAutotag", mock.Anything, expectedChange)
			} else {
				mockInstances.autotagger.AssertNotCalled(t, "UpdateAutotag")
			}
		})
	}
}
