package autotag

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"code.justin.tv/commerce/nioh/config"
	"code.justin.tv/commerce/nioh/internal/clients"
	"code.justin.tv/commerce/nioh/internal/models"
	"code.justin.tv/commerce/nioh/pkg/helpers"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/sns"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/cep21/circuit"
	"github.com/pkg/errors"
)

//go:generate mockery --note @generated --name Autotagger

// Autotagger exposes functionality related to autotagging streams based on restriction changes.
type Autotagger interface {
	UpdateAutotag(ctx context.Context, change models.RestrictionUpdateEvent) error
}

// AutotaggerInput gathers the dependencies for a new Autotagger.
type AutotaggerInput struct {
	Config         *config.Configuration
	Clock          models.IClock
	SNSPublisher   clients.SNSPublisher
	Stats          statsd.Statter
	CircuitManager *circuit.Manager
}

// NewAutotagger allocates and returns an Autotagger ready for use.
func NewAutotagger(input *AutotaggerInput) Autotagger {
	var circuit *circuit.Circuit
	if input.CircuitManager != nil {
		circuit = helpers.MakeCircuit(input.CircuitManager, &helpers.CircuitInput{
			Name:    "autotagger-sns",
			Timeout: 400 * time.Millisecond,
			Statter: input.Stats,
		})
	}
	return &autotaggerImpl{
		config:  input.Config,
		clock:   input.Clock,
		sns:     input.SNSPublisher,
		stats:   input.Stats,
		circuit: circuit,
	}
}

type autotaggerImpl struct {
	config  *config.Configuration
	clock   models.IClock
	sns     clients.SNSPublisher
	stats   statsd.Statter
	circuit *circuit.Circuit
}

func (a *autotaggerImpl) UpdateAutotag(ctx context.Context, change models.RestrictionUpdateEvent) error {
	if !change.ShouldUpdateAutotag() {
		return nil
	}

	defer func(start time.Time) {
		if a.stats == nil {
			return
		}
		_ = a.stats.TimingDuration("orch.UpdateAutotag.duration", time.Since(start), 1.0)
	}(a.clock.Now())

	event := models.PremiumVideoTagEvent{
		ChannelID:       change.Resource.GetIdentifier(),
		Timestamp:       a.clock.Now().Format(time.RFC3339),
		RestrictionType: (*string)(change.GetFlattenedRestrictionType()),
	}

	// Set the restriction tier if set
	if change.Restriction != nil && change.Restriction.RestrictionType == models.SubOnlyLiveRestrictionType {
		for _, option := range change.Restriction.Options {
			// It is assumed that only one tier related option is present in the Options slice (validation is done on Set)
			switch option {
			case models.AllowAllTiers:
				event.RestrictionMinTier = 1
			case models.AllowTier2And3Only:
				event.RestrictionMinTier = 2
			case models.AllowTier3Only:
				event.RestrictionMinTier = 3
			}
		}
	}

	body, err := toJSONString(event)
	if err != nil {
		return errors.Wrap(err, "failed to marshal SNS message body to JSON")
	}

	snsInput := (&sns.PublishInput{}).
		SetTopicArn(a.config.AutotagSNSTopicArn).
		SetMessage(body).
		SetMessageAttributes(tagEventMsgAttrs)

	return a.circuit.Execute(ctx, func(i context.Context) error {
		_, err = a.sns.PublishWithContext(ctx, snsInput)
		return err
	}, nil)
}

func toJSONString(data interface{}) (string, error) {
	b, err := json.Marshal(data)
	if err != nil {
		return "", err
	}
	return string(b), nil
}

// tagEventVersionAttr is sent as a message attribute when transmitting PremiumVideoTagEvent SNS messages.
// It should be incremented through the natural numbers when any change is made to the PremiumVideoTagEvent schema.
const tagEventVersionAttr = "2"

// tagEventTypeAttr is sent as a message attribute when transmitting PremiumVideoTagEvent SNS messages.
// Dependent services may read this attribute to discriminate these messages from other types, so changing this value
// should be treated as a breaking change.
const tagEventTypeAttr = "premium-video-tag"

var tagEventMsgAttrs = map[string]*sns.MessageAttributeValue{
	"type": {
		DataType:    aws.String("String"),
		StringValue: aws.String(tagEventTypeAttr),
	},
	"version": {
		DataType:    aws.String("Number"),
		StringValue: aws.String(tagEventVersionAttr),
	},
}

func init() {
	for _, attr := range tagEventMsgAttrs {
		if err := attr.Validate(); err != nil {
			panic(fmt.Sprintf("premium video tag event message attributes failed validation. err: %+v", err))
		}
	}
}
