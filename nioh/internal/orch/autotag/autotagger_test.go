package autotag

import (
	"context"
	"errors"
	"testing"
	"time"

	"code.justin.tv/commerce/nioh/config"
	clientmocks "code.justin.tv/commerce/nioh/internal/clients/mocks"
	"code.justin.tv/commerce/nioh/internal/models"
	"code.justin.tv/commerce/nioh/internal/models/mocks"
	"github.com/aws/aws-sdk-go/service/sns"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func TestAutotagger_UpdateAutotag(t *testing.T) {
	type testmocks struct {
		clock *mocks.IClock
		sns   *clientmocks.SNSPublisher
	}

	channelID := "1234"
	solType := models.SubOnlyLiveRestrictionType

	testTime := time.Date(2016, time.January, 1, 0, 0, 0, 0, time.UTC)
	testTimestamp := time.Date(2016, time.January, 1, 0, 0, 0, 0, time.UTC).Format(time.RFC3339)

	tests := []struct {
		name             string
		overrideMocks    func(*testmocks)
		change           models.RestrictionUpdateEvent
		wantErr          bool
		wantPublish      bool
		expectedTagEvent *models.PremiumVideoTagEvent
	}{
		{
			name: "happy path with channel resource and SOL restriction - should publish",
			change: models.RestrictionUpdateEvent{
				Resource:    models.Channel{ID: channelID},
				Restriction: &models.RestrictionV2{RestrictionType: models.SubOnlyLiveRestrictionType},
			},
			wantErr:     false,
			wantPublish: true,
			expectedTagEvent: &models.PremiumVideoTagEvent{
				ChannelID:       channelID,
				RestrictionType: (*string)(&solType),
				Timestamp:       testTimestamp,
			},
		},
		{
			name: "happy path with channel resource and no restriction - should publish",
			change: models.RestrictionUpdateEvent{
				Resource:    models.Channel{ID: channelID},
				Restriction: nil,
			},
			wantErr:     false,
			wantPublish: true,
			expectedTagEvent: &models.PremiumVideoTagEvent{
				ChannelID:       channelID,
				RestrictionType: (*string)(nil),
				Timestamp:       testTimestamp,
			},
		},
		{
			name: "happy path with channel resource and tiered SOL restriction - should publish",
			change: models.RestrictionUpdateEvent{
				Resource: models.Channel{ID: channelID},
				Restriction: &models.RestrictionV2{
					RestrictionType: models.SubOnlyLiveRestrictionType,
					Options:         []models.RestrictionOption{models.AllowTier3Only},
				},
			},
			wantErr:     false,
			wantPublish: true,
			expectedTagEvent: &models.PremiumVideoTagEvent{
				ChannelID:          channelID,
				RestrictionType:    (*string)(&solType),
				Timestamp:          testTimestamp,
				RestrictionMinTier: 3,
			},
		},
		{
			name: "happy path with channel resource and non-SOL restriction - should not publish",
			change: models.RestrictionUpdateEvent{
				Resource:    models.Channel{ID: channelID},
				Restriction: &models.RestrictionV2{RestrictionType: models.AllAccessPassRestrictionType},
			},
			wantErr:     false,
			wantPublish: false,
		},
		{
			name: "happy path with VOD resource and no restriction - should not publish",
			change: models.RestrictionUpdateEvent{
				Resource: models.VOD{
					ID:      "asdf",
					OwnerID: channelID,
				},
				Restriction: nil,
			},
			wantErr:     false,
			wantPublish: false,
		},
		{
			name: "happy path with VOD resource and restriction - should not publish",
			change: models.RestrictionUpdateEvent{
				Resource: models.VOD{
					ID:      "asdf",
					OwnerID: channelID,
				},
				Restriction: &models.RestrictionV2{RestrictionType: models.SubOnlyLiveRestrictionType},
			},
			wantErr:     false,
			wantPublish: false,
		},
		{
			name: "with error on publish call",
			overrideMocks: func(t *testmocks) {
				t.sns = new(clientmocks.SNSPublisher)
				t.sns.On("PublishWithContext", mock.Anything, mock.Anything).Return(nil, errors.New("your sns asplode"))
			},
			change: models.RestrictionUpdateEvent{
				Resource:    models.Channel{ID: channelID},
				Restriction: &models.RestrictionV2{RestrictionType: models.SubOnlyLiveRestrictionType},
			},
			wantErr:     true,
			wantPublish: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ctx := context.Background()
			config := config.Configuration{
				AutotagSNSTopicArn: "test-arn",
			}

			mockInstances := testmocks{
				clock: new(mocks.IClock),
				sns:   new(clientmocks.SNSPublisher),
			}
			mockInstances.clock.On("Now").Return(testTime)
			mockInstances.sns.On("PublishWithContext", mock.Anything, mock.Anything).Return(nil, nil)

			if tt.overrideMocks != nil {
				tt.overrideMocks(&mockInstances)
			}

			autotagger := NewAutotagger(&AutotaggerInput{
				Config:       &config,
				Clock:        mockInstances.clock,
				SNSPublisher: mockInstances.sns,
			})

			err := autotagger.UpdateAutotag(ctx, tt.change)
			assert.Equal(t, tt.wantErr, err != nil)

			if tt.wantPublish {
				expectedPublishInput := &sns.PublishInput{}
				expectedPublishInput.SetTopicArn("test-arn")
				expectedPublishInput.SetMessageAttributes(tagEventMsgAttrs)

				body, err := toJSONString(tt.expectedTagEvent)
				if err != nil {
					t.Fatal(err)
				}

				expectedPublishInput.SetMessage(body)

				mockInstances.sns.AssertCalled(t, "PublishWithContext", ctx, expectedPublishInput)
			} else {
				mockInstances.sns.AssertNotCalled(t, "PublishWithContext")
			}
		})
	}
}
