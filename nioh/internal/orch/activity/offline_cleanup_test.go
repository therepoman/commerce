package activity

import (
	"context"
	"encoding/json"
	"errors"
	"strconv"
	"testing"
	"time"

	"code.justin.tv/commerce/nioh/internal/testdata"

	"github.com/stretchr/testify/assert"

	"code.justin.tv/discovery/liveline/proto/liveline"

	backendmocks "code.justin.tv/commerce/nioh/internal/backend/mocks"
	livelinemocks "code.justin.tv/commerce/nioh/internal/clients/liveline/mocks"
	spademocks "code.justin.tv/commerce/nioh/internal/clients/spade/mocks"
	"code.justin.tv/commerce/nioh/internal/models"

	"github.com/stretchr/testify/mock"
)

func TestOfflineCleanupActivityWorker_handle(t *testing.T) {
	type testmocks struct {
		backendInstance *backendmocks.Backend
		liveline        *livelinemocks.ILivelineClient
		spade           *spademocks.Client
	}

	testTime := time.Date(2016, time.January, 1, 0, 0, 0, 0, time.UTC)
	solRestriction := &models.RestrictionV2{RestrictionType: models.SubOnlyLiveRestrictionType}
	aapRestriction := &models.RestrictionV2{RestrictionType: models.AllAccessPassRestrictionType}

	channelIDStr, err := strconv.Atoi(testdata.Channel1ID)
	if err != nil {
		panic(err)
	}

	cleanupSpadeEvent := models.ChannelRestrictionStatusEvent{
		ChannelID:       channelIDStr,
		IsRestricted:    false,
		ToggleMethod:    models.ToggleMethodSystemOfflineCleanup,
		RestrictionType: models.SubOnlyLiveRestrictionType,
	}

	tests := []struct {
		name               string
		overrideMocks      func(*testmocks)
		input              cleanupRestrictionInput
		restriction        *models.RestrictionV2
		expectedOutput     cleanupRestrictionOutput
		expectedSpadeEvent models.SpadeEvent
		wantErr            bool
	}{
		{
			name: "when stream is still online with SOL restriction – restriction not cleaned up",
			overrideMocks: func(testmocks *testmocks) {
				testmocks.liveline = new(livelinemocks.ILivelineClient)
				testmocks.liveline.On("GetStreamsByChannelIDs", mock.Anything, mock.Anything).
					Return(&liveline.StreamsResponse{Streams: []*liveline.StreamResponse{{ChannelId: "123"}}}, nil)
			},
			input: cleanupRestrictionInput{
				ChannelID:       testdata.Channel1ID,
				RestrictionType: string(models.SubOnlyLiveRestrictionType),
				Timestamp:       testTime,
			},
			restriction: solRestriction,
			expectedOutput: cleanupRestrictionOutput{
				RestrictionWasRemoved: false,
				Reason:                reasonNotOffline,
			},
			wantErr: false,
		},
		{
			name: "when stream is still online with no restriction – restriction not cleaned up",
			overrideMocks: func(testmocks *testmocks) {
				testmocks.liveline = new(livelinemocks.ILivelineClient)
				testmocks.liveline.On("GetStreamsByChannelIDs", mock.Anything, mock.Anything).
					Return(&liveline.StreamsResponse{Streams: []*liveline.StreamResponse{{ChannelId: "123"}}}, nil)
			},
			input: cleanupRestrictionInput{
				ChannelID:       testdata.Channel1ID,
				RestrictionType: string(models.SubOnlyLiveRestrictionType),
				Timestamp:       testTime,
			},
			expectedOutput: cleanupRestrictionOutput{
				RestrictionWasRemoved: false,
				Reason:                reasonNotOffline,
			},
			wantErr: false,
		},
		{
			name: "when stream is offline with no restriction – restriction not cleaned up",
			input: cleanupRestrictionInput{
				ChannelID:       testdata.Channel1ID,
				RestrictionType: string(models.SubOnlyLiveRestrictionType),
				Timestamp:       testTime,
			},
			expectedOutput: cleanupRestrictionOutput{
				RestrictionWasRemoved: false,
				Reason:                reasonNotRestricted,
			},
			wantErr: false,
		},
		{
			name: "when stream is offline with SOL restriction – restriction cleaned up",
			input: cleanupRestrictionInput{
				ChannelID:       testdata.Channel1ID,
				RestrictionType: string(models.SubOnlyLiveRestrictionType),
				Timestamp:       testTime,
			},
			restriction: solRestriction,
			expectedOutput: cleanupRestrictionOutput{
				RestrictionWasRemoved: true,
			},
			expectedSpadeEvent: cleanupSpadeEvent,
			wantErr:            false,
		},
		{
			name: "when stream is offline with different restriction type – restriction not cleaned up",
			input: cleanupRestrictionInput{
				ChannelID:       testdata.Channel1ID,
				RestrictionType: string(models.SubOnlyLiveRestrictionType),
				Timestamp:       testTime,
			},
			restriction: aapRestriction,
			expectedOutput: cleanupRestrictionOutput{
				RestrictionWasRemoved: false,
				Reason:                reasonRestrictionMismatch,
			},
			wantErr: false,
		},
		{
			name: "when input is missing channel ID - error is returned",
			input: cleanupRestrictionInput{
				ChannelID:       "",
				RestrictionType: string(models.SubOnlyLiveRestrictionType),
				Timestamp:       testTime,
			},
			restriction: solRestriction,
			wantErr:     true,
		},
		{
			name: "when input is missing restriction type - error is returned",
			input: cleanupRestrictionInput{
				ChannelID:       testdata.Channel1ID,
				RestrictionType: "",
				Timestamp:       testTime,
			},
			restriction: solRestriction,
			wantErr:     true,
		},
		{
			name: "when GetRestriction returns error – error is returned",
			overrideMocks: func(testmocks *testmocks) {
				testmocks.backendInstance = new(backendmocks.Backend)
				testmocks.backendInstance.On("GetRestriction", mock.Anything, mock.Anything, mock.Anything).
					Return(nil, errors.New("your backend asplode"))
			},
			input: cleanupRestrictionInput{
				ChannelID:       testdata.Channel1ID,
				RestrictionType: string(models.SubOnlyLiveRestrictionType),
				Timestamp:       testTime,
			},
			restriction: solRestriction,
			wantErr:     true,
		},
		{
			name: "when Liveline returns error – error is returned",
			overrideMocks: func(testmocks *testmocks) {
				testmocks.liveline = new(livelinemocks.ILivelineClient)
				testmocks.liveline.On("GetStreamsByChannelIDs", mock.Anything, mock.Anything).
					Return(nil, errors.New("liveline asplode"))
			},
			input: cleanupRestrictionInput{
				ChannelID:       testdata.Channel1ID,
				RestrictionType: string(models.SubOnlyLiveRestrictionType),
				Timestamp:       testTime,
			},
			restriction: solRestriction,
			wantErr:     true,
		},
		{
			name: "when DeleteRestriction returns error – error is returned",
			overrideMocks: func(testmocks *testmocks) {
				testmocks.backendInstance = new(backendmocks.Backend)
				testmocks.backendInstance.On("GetRestriction", mock.Anything, mock.Anything, mock.Anything).
					Return(solRestriction, nil)
				testmocks.backendInstance.On("DeleteRestriction", mock.Anything, mock.Anything).
					Return(nil, errors.New("your backend asplode"))
			},
			input: cleanupRestrictionInput{
				ChannelID:       testdata.Channel1ID,
				RestrictionType: string(models.SubOnlyLiveRestrictionType),
				Timestamp:       testTime,
			},
			restriction: solRestriction,
			wantErr:     true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mockInstances := testmocks{
				backendInstance: new(backendmocks.Backend),
				liveline:        new(livelinemocks.ILivelineClient),
				spade:           new(spademocks.Client),
			}

			mockInstances.backendInstance.On("GetRestriction", mock.Anything, mock.Anything, mock.Anything).
				Return(tt.restriction, nil)
			mockInstances.backendInstance.On("DeleteRestriction", mock.Anything, mock.Anything).
				Return(tt.restriction, nil)
			mockInstances.liveline.On("GetStreamsByChannelIDs", mock.Anything, mock.Anything).
				Return(&liveline.StreamsResponse{Streams: []*liveline.StreamResponse{}}, nil)
			mockInstances.spade.On("TrackEvent", mock.Anything, mock.Anything, mock.Anything).
				Return(nil)

			if tt.overrideMocks != nil {
				tt.overrideMocks(&mockInstances)
			}

			worker := &OfflineCleanupActivityWorker{
				backendInstance: mockInstances.backendInstance,
				livelineClient:  mockInstances.liveline,
				spadeClient:     mockInstances.spade,
			}

			inputBytes, err := json.Marshal(tt.input)
			if err != nil {
				t.Fatalf("could not marshal test input to JSON: %v", err)
			}

			outIface, err := worker.handle(context.TODO(), inputBytes)
			assert.Equal(t, tt.wantErr, err != nil, "tt.wantErr (%v) should equal err != nil (%v) ", tt.wantErr, err != nil)

			if tt.expectedSpadeEvent != nil {
				mockInstances.spade.AssertCalled(t, "TrackEvent", context.TODO(),
					tt.expectedSpadeEvent.EventName(), tt.expectedSpadeEvent.Params())
			}

			if !tt.wantErr {
				out, ok := outIface.(cleanupRestrictionOutput)
				if !ok {
					t.Fatalf("worker returned unexpected type. expected cleanupRestrictionOutput")
				}
				assert.Equal(t, tt.expectedOutput, out)
			}
		})
	}
}
