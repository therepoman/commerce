package activity

import (
	"context"
	"encoding/json"
	"strconv"
	"time"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/common/spade-client-go/spade"

	"code.justin.tv/commerce/nioh/internal/clients"

	"code.justin.tv/commerce/nioh/internal/models"

	"github.com/pkg/errors"

	"code.justin.tv/commerce/nioh/internal/clients/liveline"

	"code.justin.tv/commerce/nioh/config"
	"code.justin.tv/commerce/nioh/internal/backend"
	"code.justin.tv/growth/sfnactivity"
	"github.com/aws/aws-sdk-go/service/sfn/sfniface"
)

// OfflineCleanupActivityWorker handles the scheduled offline cleanup activity from Step Functions
type OfflineCleanupActivityWorker struct {
	consumer        *sfnactivity.Consumer
	backendInstance backend.Backend
	livelineClient  liveline.ILivelineClient
	spadeClient     spade.Client
}

// NewOfflineCleanupActivityWorker allocates and returns a new OfflineCleanupActivityWorker, ready for use.
func NewOfflineCleanupActivityWorker(
	config *config.Configuration,
	sfn sfniface.SFNAPI,
	backendInstance backend.Backend,
	clients *clients.Clients,
) (*OfflineCleanupActivityWorker, error) {
	o := &OfflineCleanupActivityWorker{
		backendInstance: backendInstance,
		livelineClient:  clients.Liveline,
		spadeClient:     clients.Spade,
	}

	cfg := sfnactivity.Config{
		SFN:         sfn,
		ActivityARN: config.OfflineCleanupActivityArn,
		Handler:     sfnactivity.HandlerFunc(o.handle),
	}

	consumer, err := sfnactivity.NewConsumer(cfg)
	if err != nil {
		return nil, err
	}
	o.consumer = consumer

	return o, nil
}

// Start causes the worker to begin polling and handling the offline cleanup activity from Step Functions.
func (o *OfflineCleanupActivityWorker) Start(ctx context.Context) {
	o.consumer.Start()
}

// Shutdown causes the worker to begin gracefully shutting down. If the context is canceled before the shutdown
// gracefully completes, an error is returned.
func (o *OfflineCleanupActivityWorker) Shutdown(ctx context.Context) error {
	go o.consumer.Shutdown()

	done := make(chan struct{})
	go func() {
		o.consumer.Wait()
		close(done)
	}()

	select {
	case <-ctx.Done():
		return errors.New("timed out while shutting")
	case <-done:
		return nil
	}
}

type cleanupRestrictionInput struct {
	ChannelID       string
	RestrictionType string
	Timestamp       time.Time
}

func (c cleanupRestrictionInput) validate() error {
	if c.ChannelID == "" {
		return errors.New("ChannelID should not be empty")
	}
	if c.RestrictionType == "" {
		return errors.New("RestrictionType should not be empty")
	}
	return nil
}

type cleanupRestrictionOutput struct {
	RestrictionWasRemoved bool
	Reason                string
}

const (
	reasonNotOffline          = "channel was not offline"
	reasonNotRestricted       = "channel was not restricted"
	reasonRestrictionMismatch = "restriction type changed since cleanup was scheduled"
)

func (o *OfflineCleanupActivityWorker) handle(ctx context.Context, input []byte) (interface{}, error) {
	data := cleanupRestrictionInput{}
	err := json.Unmarshal(input, &data)
	if err != nil {
		return nil, errors.Wrapf(err, "could not unmarshal input from JSON: %v", string(input))
	}
	if err := data.validate(); err != nil {
		return nil, errors.Wrap(err, "input is invalid")
	}

	// check if offline
	isOffline, err := o.isChannelOffline(ctx, data.ChannelID)
	if err != nil {
		return nil, err
	}
	if !isOffline {
		return cleanupRestrictionOutput{
			RestrictionWasRemoved: false,
			Reason:                reasonNotOffline,
		}, nil
	}

	// check if restricted
	restriction, err := o.backendInstance.GetRestriction(ctx, models.Channel{ID: data.ChannelID}, false)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get restriction")
	}
	if restriction == nil {
		return cleanupRestrictionOutput{
			RestrictionWasRemoved: false,
			Reason:                reasonNotRestricted,
		}, nil
	}

	if restriction.RestrictionType != models.RestrictionType(data.RestrictionType) {
		return cleanupRestrictionOutput{
			RestrictionWasRemoved: false,
			Reason:                reasonRestrictionMismatch,
		}, nil
	}

	// delete restricted if both yes
	restriction, err = o.backendInstance.DeleteRestriction(ctx, models.Channel{ID: data.ChannelID})
	if err != nil {
		return nil, errors.Wrap(err, "failed to delete restriction")
	}

	// track deletion event in Spade
	o.trackCleanupInSpade(ctx, data.ChannelID, restriction)

	return cleanupRestrictionOutput{
		RestrictionWasRemoved: true,
	}, nil
}

func (o *OfflineCleanupActivityWorker) isChannelOffline(ctx context.Context, channelID string) (bool, error) {
	streamsResponse, err := o.livelineClient.GetStreamsByChannelIDs(ctx, []string{channelID})
	if err != nil {
		return false, errors.Wrap(err, "error calling Liveline.GetStreamsByChannelIDs")
	}

	return len(streamsResponse.Streams) == 0, nil
}

func (o *OfflineCleanupActivityWorker) trackCleanupInSpade(ctx context.Context, channelID string, restriction *models.RestrictionV2) {
	var restrictionType models.RestrictionType
	// restriction should never be nil, but it possibly could be, due to racing writes in Dynamo.
	if restriction != nil {
		restrictionType = restriction.RestrictionType
	}

	channelIDInt, err := strconv.Atoi(channelID)
	if err != nil {
		logrus.WithField("channelID", channelID).WithError(err).
			Warn("could not convert channel ID to integer for Spade event in OfflineCleanupActivityWorker")
		return
	}

	e := models.ChannelRestrictionStatusEvent{
		ChannelID:       channelIDInt,
		IsRestricted:    false,
		ToggleMethod:    models.ToggleMethodSystemOfflineCleanup,
		RestrictionType: restrictionType,
		ClientID:        "", // client ID is inapplicable to this toggle method
	}

	for _, option := range restriction.Options {
		if option == models.AllowChannelModeratorOption {
			e.IncludeMods = true
		}

		if option == models.AllowChannelVIPOption {
			e.IncludeVIPs = true
		}
	}

	err = o.spadeClient.TrackEvent(ctx, e.EventName(), e.Params())
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"error":  err,
			"event":  e.EventName(),
			"params": e.Params(),
		}).Error("failed to send Spade event")
	}
}
