package testdata

const (
	// Channel1ID a test channel ID
	Channel1ID string = "8675309"
	// Channel2ID a test channel ID
	Channel2ID string = "86753099"
)

const (
	// Record1ID a test record ID
	Record1ID string = "1012341"
	// Record2ID a test record ID
	Record2ID string = "1012342"
	// Record3ID a test record ID
	Record3ID string = "1012343"
)

const (
	// Data1 is test data
	Data1 string = "{\"Am_I_data\": true, \"num\": 1}"
	// Data2 is test data
	Data2 string = "{\"Am_I_data\": true, \"num\": 2}"
	// Data3 is test data
	Data3 string = "{\"Am_I_data\": true, \"num\": 3}"
)

const (
	// UpdatedData1 is updated test data
	UpdatedData1 string = "{\"Am_I_data\": true, \"num\": 1, \"updated\": true}"
)

const (
	// ContentAttributeImageURLPrefix is used for tests in place of the content attribute URL prefix that is read out of config files
	ContentAttributeImageURLPrefix = "https://static-cdn.jtvnw.net/content-attribute-assets"
)
