package testdata

import (
	"fmt"
	"time"

	"code.justin.tv/commerce/nioh/config"
	"code.justin.tv/commerce/nioh/internal/models"
	"code.justin.tv/commerce/nioh/rpc/nioh"
	"github.com/golang/protobuf/ptypes"
)

const (
	// TestProductID can be used for resource exemptions as a product key
	TestProductID = "1189177"
)

// Config returns a test configuration
func Config() config.Configuration {
	return config.Configuration{
		SpadeHost:                          "localhost",
		CloudwatchRegion:                   "aws-west-2",
		CloudwatchBatchSize:                1,
		CloudwatchBufferSize:               1,
		CloudwatchEmergencyFlushPercentage: 1.0,
		CloudwatchFlushIntervalSeconds:     3600,
		CloudwatchFlushCheckDelayMS:        1000,
	}
}

// ContentAttribute returns a test content attribute
func ContentAttribute(id string, key string, value string, valueShortname string) *models.ContentAttribute {
	return &models.ContentAttribute{
		OwnerChannelID:     Channel1ID,
		ID:                 id,
		AttributeKey:       key,
		AttributeName:      key,
		Value:              value,
		ValueShortname:     valueShortname,
		ParentAttributeID:  "00001",
		ParentAttributeKey: "country",
		ImageURL:           fmt.Sprintf("%s/%s", ContentAttributeImageURLPrefix, id),
		CreatedAt:          time.Time{},
		UpdatedAt:          time.Time{},
	}
}

// ContentAttributes returns a slice of 5 test content attributes
func ContentAttributes() []*models.ContentAttribute {
	return []*models.ContentAttribute{
		ContentAttribute("1", "owl_team", "Los Angleles Gladiators", "GLA"),
		ContentAttribute("2", "owl_team", "Dallas Fuel", "DAL"),
		ContentAttribute("3", "owl_team", "Boston Uprising", "BOS"),
		ContentAttribute("4", "owl_team", "Shanghai Dragons", "SHD"),
		ContentAttribute("5", "owl_team", "London Spitfire", "LDN"),
	}
}

// ContentAttributeIDs returns the IDs of test content attributes
func ContentAttributeIDs() []string {
	return []string{
		"1", "2", "3", "4", "5",
	}
}

// ProtoContentAttribute returns a test content attribute
func ProtoContentAttribute(id string, key string, value string, valueShortname string) *nioh.ContentAttribute {
	timestamp, _ := ptypes.TimestampProto(time.Time{})
	return &nioh.ContentAttribute{
		OwnerChannelId:     Channel1ID,
		Id:                 id,
		AttributeKey:       key,
		AttributeName:      key,
		Value:              value,
		ValueShortname:     valueShortname,
		ParentAttributeId:  "00001",
		ParentAttributeKey: "country",
		ImageUrl:           fmt.Sprintf("%s/%s", ContentAttributeImageURLPrefix, id),
		CreatedAt:          timestamp,
		UpdatedAt:          timestamp,
	}
}

// ProtoContentAttributes returns a slice of test content attributes
func ProtoContentAttributes() []*nioh.ContentAttribute {
	return []*nioh.ContentAttribute{
		ProtoContentAttribute("1", "owl_team", "Los Angleles Gladiators", "GLA"),
		ProtoContentAttribute("2", "owl_team", "Dallas Fuel", "DAL"),
		ProtoContentAttribute("3", "owl_team", "Boston Uprising", "BOS"),
		ProtoContentAttribute("4", "owl_team", "Shanghai Dragons", "SHD"),
		ProtoContentAttribute("5", "owl_team", "London Spitfire", "LDN"),
	}
}

// ResourceRestriction returns a test resource restriction
func ResourceRestriction(id string, resourceType nioh.ResourceType) *nioh.RestrictionResourceResponse {
	return &nioh.RestrictionResourceResponse{
		Id:   id,
		Type: resourceType,
	}
}

// ProtoRestrictionResource returns the response of a test restriction resource request
func ProtoRestrictionResource(id string, resourceType nioh.ResourceType) *nioh.RestrictionResource {
	return &nioh.RestrictionResource{
		Id:   id,
		Type: resourceType,
	}
}

// ChannelRelationChannel1BorrowsFromChannel2 returns a channel relation
// representing channel 1 as a borrower to channel 2
func ChannelRelationChannel1BorrowsFromChannel2() *models.ChannelRelation {
	return &models.ChannelRelation{
		ChannelID:        Channel1ID,
		Relation:         models.RelationBorrower,
		RelatedChannelID: Channel2ID,
	}
}

// Restriction ...
func Restriction(channelID string) *models.RestrictionV2 {
	return &models.RestrictionV2{
		ResourceKey:     "channel:" + channelID,
		RestrictionType: models.SubOnlyLiveRestrictionType,
		Exemptions:      ExemptionsStaffProduct(),
	}
}

// RestrictionWithModAndVIP ...
func RestrictionWithModAndVIP(channelID string) *models.RestrictionV2 {
	exemptions := ExemptionsStaffProduct()
	exemptions = append(exemptions, ExemptionsVIP()...)
	exemptions = append(exemptions, ExemptionsMod()...)
	return &models.RestrictionV2{
		ResourceKey:     "channel:" + channelID,
		RestrictionType: models.SubOnlyLiveRestrictionType,
		Exemptions:      exemptions,
	}
}

// Preview ...
func Preview(userID string, channelID string, now time.Time) *models.Preview {
	return &models.Preview{
		PreviewKey:       userID + ":" + channelID,
		ResourceKey:      channelID,
		UserID:           userID,
		CreatedAt:        now,
		ContentExpiresAt: now.Add(models.PreviewDuration),
		ResetAt:          now.Add(models.PreviewCooldown),
	}
}

// PreviewWithOrigin ...
func PreviewWithOrigin(userID string, channelID string, origin string, now time.Time) *models.Preview {
	return &models.Preview{
		PreviewKey:       userID + ":" + channelID + ":" + origin,
		ResourceKey:      channelID,
		UserID:           userID,
		CreatedAt:        now,
		ContentExpiresAt: now.Add(models.PreviewDuration),
		ResetAt:          now.Add(models.PreviewCooldown),
	}
}

// VODRestriction ...
func VODRestriction(vodID string) *models.RestrictionV2 {
	return &models.RestrictionV2{
		ResourceKey:     "vod:" + vodID,
		RestrictionType: models.SubOnlyLiveRestrictionType,
		Exemptions:      ExemptionsStaffProduct(),
	}
}

// ExemptionsStaffProduct ...
func ExemptionsStaffProduct() []*models.Exemption {
	return []*models.Exemption{
		{
			ExemptionType:   models.StaffExemptionType,
			ActiveStartDate: time.Date(2019, time.January, 1, 0, 0, 0, 0, time.UTC),
			ActiveEndDate:   time.Date(2021, time.January, 1, 0, 0, 0, 0, time.UTC),
			Keys:            []string{},
			Actions:         []*models.ExemptionAction{},
		},
		{
			ExemptionType:   models.ProductExemptionType,
			ActiveStartDate: time.Date(2019, time.January, 1, 0, 0, 0, 0, time.UTC),
			ActiveEndDate:   time.Date(2021, time.January, 1, 0, 0, 0, 0, time.UTC),
			Keys:            []string{TestProductID},
			Actions:         []*models.ExemptionAction{},
		},
	}
}

// ExemptionsVIP ...
func ExemptionsVIP() []*models.Exemption {
	return []*models.Exemption{
		{
			ExemptionType:   models.ChannelVIPExemptionType,
			ActiveStartDate: time.Date(2019, time.January, 1, 0, 0, 0, 0, time.UTC),
			ActiveEndDate:   time.Date(2021, time.January, 1, 0, 0, 0, 0, time.UTC),
			Keys:            []string{},
		},
	}
}

// ExemptionsMod ...
func ExemptionsMod() []*models.Exemption {
	return []*models.Exemption{
		{
			ExemptionType:   models.ChannelModeratorExemptionType,
			ActiveStartDate: time.Date(2019, time.January, 1, 0, 0, 0, 0, time.UTC),
			ActiveEndDate:   time.Date(2021, time.January, 1, 0, 0, 0, 0, time.UTC),
			Keys:            []string{},
		},
	}
}

// ExemptionsPreview ...
func ExemptionsPreview(now time.Time) []*models.Exemption {
	return []*models.Exemption{
		{
			ExemptionType:   models.PreviewExemptionType,
			ActiveStartDate: now,
			ActiveEndDate:   now.Add(models.PreviewDuration),
			Keys:            []string{},
		},
	}
}

// ExemptionsCustomPreview ...
func ExemptionsCustomPreview(now time.Time, duration time.Duration) []*models.Exemption {
	return []*models.Exemption{
		{
			ExemptionType:   models.PreviewExemptionType,
			ActiveStartDate: now,
			ActiveEndDate:   now.Add(duration),
			Keys:            []string{},
		},
	}
}

// ApplyExemptions make AppliedExemptions out of Exemptions (without giving a matched key value)
func ApplyExemptions(exemptions []*models.Exemption) []*models.AppliedExemption {
	appliedExemptions := make([]*models.AppliedExemption, len(exemptions))
	for i, exemption := range exemptions {
		appliedExemptions[i] = &models.AppliedExemption{
			Exemption: exemption,
		}
	}
	return appliedExemptions
}

// ApplyExemptionsWithMatchedKey make AppliedExemptions out of Exemptions with a matched key
// If matchedKey argument is zero value, it will always pick the first value from the Keys ("" if no Keys)
func ApplyExemptionsWithMatchedKey(exemptions []*models.Exemption, matchedKey string) []*models.AppliedExemption {
	appliedExemptions := make([]*models.AppliedExemption, len(exemptions))
	for i, exemption := range exemptions {
		mKey := matchedKey
		if matchedKey == "" && len(exemption.Keys) > 0 {
			mKey = exemption.Keys[0]
		}

		appliedExemptions[i] = &models.AppliedExemption{
			Exemption:  exemption,
			MatchedKey: mKey,
		}
	}
	return appliedExemptions
}

// ExemptionsAll ...
func ExemptionsAll() []*models.Exemption {
	return []*models.Exemption{
		{
			ExemptionType:   models.AllExemptionType,
			ActiveStartDate: time.Date(2019, time.January, 1, 0, 0, 0, 0, time.UTC),
			ActiveEndDate:   time.Date(2021, time.January, 1, 0, 0, 0, 0, time.UTC),
			Keys:            []string{},
		},
	}
}

// ProtoRestriction ...
func ProtoRestriction(channelID string) *nioh.Restriction {
	return &nioh.Restriction{
		Id:              "channel:" + channelID,
		RestrictionType: nioh.Restriction_SUB_ONLY_LIVE,
		Exemptions:      ProtoExemptions(),
	}
}

// ProtoExemptions ...
func ProtoExemptions() []*nioh.Exemption {
	startTs, _ := ptypes.TimestampProto(time.Date(2019, time.January, 1, 0, 0, 0, 0, time.UTC))
	endTs, _ := ptypes.TimestampProto(time.Date(2021, time.January, 1, 0, 0, 0, 0, time.UTC))

	return []*nioh.Exemption{
		{
			ExemptionType:   nioh.Exemption_STAFF,
			ActiveStartDate: startTs,
			ActiveEndDate:   endTs,
			ExemptionKeys:   []string{},
		},
		{
			ExemptionType:   nioh.Exemption_PRODUCT,
			ActiveStartDate: startTs,
			ActiveEndDate:   endTs,
			ExemptionKeys:   []string{TestProductID},
		},
	}
}

// ChannelAuthorizationWithPreviewExemption ...
func ChannelAuthorizationWithPreviewExemption() models.ChannelAuthorization {
	return models.ChannelAuthorization{
		Request: &models.ChannelAuthorizationRequest{
			UserID:         "123",
			ChannelID:      "456",
			ConsumePreview: true,
		},
		ResourceAuthorization: models.ResourceAuthorization{
			UserIsAuthorized:     true,
			ResourceIsRestricted: true,
			RestrictionType:      models.SubOnlyLiveRestrictionType,
			AppliedExemptions:    ApplyExemptions(ExemptionsPreview(time.Date(2021, time.January, 1, 0, 0, 0, 0, time.UTC))),
			Error:                nil,
		},
	}
}

// ChannelAuthorizationWithAllExemption ...
func ChannelAuthorizationWithAllExemption() models.ChannelAuthorization {
	return models.ChannelAuthorization{
		Request: &models.ChannelAuthorizationRequest{
			UserID:         "123",
			ChannelID:      "456",
			ConsumePreview: true,
		},
		ResourceAuthorization: models.ResourceAuthorization{
			UserIsAuthorized:     true,
			ResourceIsRestricted: true,
			RestrictionType:      models.SubOnlyLiveRestrictionType,
			AppliedExemptions:    ApplyExemptions(ExemptionsAll()),
			Error:                nil,
		},
	}
}

// GetUserAuthorizationForResourceProto ...
func GetUserAuthorizationForResourceProto() *nioh.GetUserAuthorizationResponse {
	startTime := time.Date(2021, time.January, 1, 0, 0, 0, 0, time.UTC)
	startTs, _ := ptypes.TimestampProto(startTime)
	endTs, _ := ptypes.TimestampProto(startTime.Add(models.PreviewDuration))

	return &nioh.GetUserAuthorizationResponse{
		UserId:               "123",
		UserIsAuthorized:     true,
		ResourceIsRestricted: true,
		Resource: &nioh.GetUserAuthorizationResponse_Channel{
			Channel: &nioh.Channel{Id: "456"},
		},
		AppliedExemptions: []*nioh.Exemption{
			{
				ExemptionType:   nioh.Exemption_PREVIEW,
				ActiveStartDate: startTs,
				ActiveEndDate:   endTs,
				ExemptionKeys:   []string{},
			},
		},
		RestrictionType:  nioh.Restriction_SUB_ONLY_LIVE,
		AccessExpiration: endTs,
	}
}

// GetUserAuthorizationForResourceProtoAll ...
func GetUserAuthorizationForResourceProtoAll() *nioh.GetUserAuthorizationResponse {
	startTs, _ := ptypes.TimestampProto(time.Date(2019, time.January, 1, 0, 0, 0, 0, time.UTC))
	endTs, _ := ptypes.TimestampProto(time.Date(2021, time.January, 1, 0, 0, 0, 0, time.UTC))

	return &nioh.GetUserAuthorizationResponse{
		UserId:               "123",
		UserIsAuthorized:     true,
		ResourceIsRestricted: true,
		Resource: &nioh.GetUserAuthorizationResponse_Channel{
			Channel: &nioh.Channel{Id: "456"},
		},
		AppliedExemptions: []*nioh.Exemption{
			{
				ExemptionType:   nioh.Exemption_ALL,
				ActiveStartDate: startTs,
				ActiveEndDate:   endTs,
				ExemptionKeys:   []string{},
			},
		},
		RestrictionType: nioh.Restriction_SUB_ONLY_LIVE,
	}
}

// RestrictionWithOrgAccessOnly ...
func RestrictionWithOrgAccessOnly(channelID string, companyID string) *models.RestrictionV2 {
	return &models.RestrictionV2{
		ResourceKey:     "channel:" + channelID,
		RestrictionType: models.OrganizationAccessOnlyType,
		Exemptions: []*models.Exemption{
			{
				ExemptionType:   models.OrganizationMemberExemptionType,
				Keys:            []string{companyID},
				ActiveStartDate: time.Date(2019, time.January, 1, 0, 0, 0, 0, time.UTC),
				ActiveEndDate:   time.Date(3000, time.January, 1, 0, 0, 0, 0, time.UTC),
			},
		},
	}
}
