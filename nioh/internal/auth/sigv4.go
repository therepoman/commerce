package auth

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	signer "github.com/aws/aws-sdk-go/aws/signer/v4"
)

type HTTPClient interface {
	Do(req *http.Request) (*http.Response, error)
}

type SignerClient struct {
	BaseClient HTTPClient
	Signer     *signer.Signer
	Service    string
	Region     string
}

func (s *SignerClient) Do(req *http.Request) (*http.Response, error) {
	b, err := ioutil.ReadAll(req.Body)
	if err != nil {
		return nil, fmt.Errorf("failed to request read body for sigv4 signing: %s", err)
	}

	_, err = s.Signer.Sign(req, bytes.NewReader(b), s.Service, s.Region, time.Now())
	if err != nil {
		return nil, fmt.Errorf("failed to sigv4 sign the request: %s", err)
	}
	return s.BaseClient.Do(req)
}

// NewBaseSigV4HTTPClient returns a base http client for use with a sigv4 client (without an OpStarter, because client
// metrics should be manually reported to account for errors from intermediary (API Gateway)
func NewBaseSigV4HTTPClient(timeout time.Duration) *http.Client {
	return &http.Client{
		Timeout: timeout,
		// Strip off default redirect handler
		// https://github.com/twitchtv/twirp/blob/v5.9.0/protoc-gen-twirp/generator.go#L664
		CheckRedirect: func(_ *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
	}
}
