package auth

import (
	e "errors"
	"fmt"
	"net/http"

	"golang.org/x/net/context"

	"code.justin.tv/commerce/nioh/internal/errors"
	"code.justin.tv/commerce/nioh/pkg/helpers"
	"code.justin.tv/common/goauthorization"
)

const (
	capabilityNameManageRecord = "manage_record"
	algorithm                  = "ES256"
	audience                   = "code.justin.tv/commerce/nioh"
	issuer                     = "code.justin.tv/web/cartman"
)

type jwtHandler struct {
	decoder goauthorization.Decoder
}

// NewJWTHandler creates a new JWT handler.
func NewJWTHandler(key string) (Handler, error) {
	decoder, err := goauthorization.NewDecoder(
		algorithm,
		audience,
		issuer,
		[]byte(key),
	)
	if err != nil {
		return nil, err
	}

	return &jwtHandler{
		decoder: decoder,
	}, nil
}

// AuthMiddleware runs on every request.
func (j *jwtHandler) AuthMiddleware(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		token, err := j.decoder.ParseToken(r)
		if err != nil {
			h.ServeHTTP(w, r) // no auth if token failed to parse
		}

		err = j.decoder.Validate(token, goauthorization.CapabilityClaims{
			capabilityNameManageRecord: goauthorization.CapabilityClaim{
				"channel_id": token.GetSubject(),
			},
		})
		if err != nil {
			h.ServeHTTP(w, r) // no auth if token failed to validate
		}

		ctx := helpers.ContextWithUser(r.Context(), token.GetSubject())
		h.ServeHTTP(w, r.WithContext(ctx))
	})
}

// AuthorizeRecordOperation checks if record operations are allowed.
func (j *jwtHandler) AuthorizeRecordOperation(ctx context.Context, channelID string) error {
	validatedChannelID, ok := helpers.UserIDFromContext(ctx)
	if !ok || validatedChannelID != channelID {
		msg := fmt.Sprintf("User %s is not authorized for channel %s", validatedChannelID, channelID)
		return &errors.Unauthorized{Err: e.New(msg)}
	}

	return nil
}
