package auth

import (
	"net/http"

	"golang.org/x/net/context"
)

// Handler is the interface to authorize consumer actions.
type Handler interface {
	AuthMiddleware(http.Handler) http.Handler
	AuthorizeRecordOperation(ctx context.Context, channelID string) error
}
