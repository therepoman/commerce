package auth

import (
	e "errors"
	"net/http"

	"golang.org/x/net/context"

	"code.justin.tv/commerce/nioh/internal/errors"
)

type fakeHandler struct {
	allow bool
}

// NewFakeHandler returns a handler that either allows all calls or denies them
// (controlled by allow flag).
func NewFakeHandler(allow bool) Handler {
	return &fakeHandler{allow: allow}
}

func (f *fakeHandler) AuthMiddleware(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		h.ServeHTTP(w, r)
	})
}

func (f *fakeHandler) AuthorizeRecordOperation(ctx context.Context, channelID string) error {
	if f.allow {
		return nil
	}
	return &errors.Unauthorized{Err: e.New("Auth error")}
}
