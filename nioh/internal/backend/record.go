package backend

import (
	"context"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/nioh/rpc/nioh"
	"code.justin.tv/common/chitin"
	"github.com/pkg/errors"
)

// GetRecord is used to get a record.
func (b *backendImpl) GetRecord(ctx context.Context, r *nioh.GetRecordRequest) (*nioh.Record, error) {
	log := logrus.WithField("RequestID", chitin.GetTraceID(ctx))

	result, err := b.daos.TestTableDAO.GetTestItem(ctx, r.ChannelId)
	if err != nil {
		log.WithError(errors.WithStack(err)).Errorf("Failed to fetch item for %v", r.ChannelId)
		return nil, errors.Wrap(err, "Failed to fetch record")
	}
	record := &nioh.Record{
		ChannelId: result.UserID,
		Data:      result.Msg,
	}

	return record, nil
}
