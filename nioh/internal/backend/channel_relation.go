package backend

import (
	"context"

	"code.justin.tv/commerce/nioh/internal/models"
	"github.com/pkg/errors"
)

// CreateChannelRelation is used to create a channel relation
func (b *backendImpl) CreateChannelRelation(ctx context.Context, channelID string, relatedChannelID string, relationType models.Relation) (*models.ChannelRelation, error) {
	// If the relation type is BORROWER, make sure to create an owner chanlet for the borrower channel (if it does not exist already)
	// to substitute the owner chanlet of the lender channel when getting the borrower's chanlets (see GetChanlets)
	if relationType == models.RelationBorrower {
		err := b.CreateOwnerChanlet(ctx, channelID)
		if err != nil {
			return nil, errors.Wrapf(err, "failed to create owner chanlet while creating borrower relation for channel %s", channelID)
		}
	}

	input := &models.ChannelRelation{
		ChannelID:        channelID,
		RelatedChannelID: relatedChannelID,
		Relation:         relationType,
	}

	// Create DB entry
	err := b.daos.ChannelRelationDAO.AddChannelRelation(ctx, input)
	if err != nil {
		return nil, errors.Wrapf(err, "failed to create channel relation database entry for channel %s", input.ChannelID)
	}

	return input, nil
}

// DeleteChannelRelation deletes a channel relation
func (b *backendImpl) DeleteChannelRelation(ctx context.Context, channelID string, relatedChannelID string) error {
	err := b.daos.ChannelRelationDAO.DeleteChannelRelation(ctx, channelID, relatedChannelID)
	if err != nil {
		return errors.Wrapf(err, "failed to delete a channel relation database entry for channel %s", channelID)
	}

	return nil
}

// GetChannelRelationsByChannelID queries for channel relations based on channel id
func (b *backendImpl) GetChannelRelationsByChannelID(ctx context.Context, channelID string) ([]*models.ChannelRelation, error) {
	relations, err := b.daos.ChannelRelationDAO.GetChannelRelationsByChannelID(ctx, channelID)

	if err != nil {
		return nil, errors.Wrapf(err, "failed to get channel relations for channel id %s", channelID)
	}

	return relations, nil
}

// GetChannelRelationsByRelatedChannelID queries for channel relations based on related channel id
func (b *backendImpl) GetChannelRelationsByRelatedChannelID(ctx context.Context, relatedChannelID string) ([]*models.ChannelRelation, error) {
	relations, err := b.daos.ChannelRelationDAO.GetChannelRelationsByRelatedChannelID(ctx, relatedChannelID)

	if err != nil {
		return nil, errors.Wrapf(err, "failed to get channel relations for related channel id %s", relatedChannelID)
	}

	return relations, nil
}
