package backend

import (
	"context"
	"testing"
	"time"

	"code.justin.tv/commerce/nioh/internal/models"
	"code.justin.tv/commerce/nioh/internal/testdata"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	. "github.com/stretchr/testify/mock"
)

const (
	createContentAttributes                 = "CreateContentAttributes"
	getContentAttributesForChannel          = "GetContentAttributesForChannel"
	getChannelRelationsByChannelID          = "GetChannelRelationsByChannelID"
	deleteContentAttributesByIDs            = "DeleteContentAttributesByIDs"
	updateContentAttributes                 = "UpdateContentAttributes"
	createContentAttributeImageUploadConfig = "CreateContentAttributeImageUploadConfig"
)

func TestContentAttributeBackend(t *testing.T) {
	testTime := time.Date(2018, time.July, 1, 0, 0, 0, 0, time.UTC)
	testUpdateTime := time.Date(2018, time.August, 1, 0, 0, 0, 0, time.UTC)

	Convey("Test Content Attribute Backend", t, func() {
		backend, mockBackendClients := initTestableBackend()

		Convey("CreateContentAttributes", func() {
			mockBackendClients.clock.On("Now").Return(testTime)

			input := &ContentAttributesBulkParams{
				ChannelID:         testdata.Channel1ID,
				ContentAttributes: testdata.ContentAttributes(),
			}
			// clear content attribute IDs
			for _, attribute := range input.ContentAttributes {
				attribute.OwnerChannelID = ""
				attribute.ID = ""
			}

			Convey("with nil params", func() {
				_, err := backend.CreateContentAttributes(context.Background(), nil)
				So(err, ShouldNotBeNil)
			})

			Convey("with empty channel ID", func() {
				input.ChannelID = ""
				_, err := backend.CreateContentAttributes(context.Background(), input)
				So(err, ShouldNotBeNil)
			})

			Convey("with no item", func() {
				input.ContentAttributes = []*models.ContentAttribute{}
				results, err := backend.CreateContentAttributes(context.Background(), input)

				So(err, ShouldBeNil)
				So(len(results.Success), ShouldEqual, 0)
				mockBackendClients.contentAttributeDAO.AssertNotCalled(t, createContentAttributes)
			})

			Convey("with multiple items", func() {
				mockBackendClients.contentAttributeDAO.On(
					createContentAttributes, Anything, input.ContentAttributes).Return(len(input.ContentAttributes), nil)

				results, err := backend.CreateContentAttributes(context.Background(), input)

				So(err, ShouldBeNil)
				So(len(results.Success), ShouldEqual, len(input.ContentAttributes))
				So(len(results.Fail), ShouldEqual, 0)

				for _, attribute := range results.Success {
					So(attribute.CreatedAt, ShouldResemble, testTime)
					So(attribute.UpdatedAt, ShouldResemble, testTime)
				}
			})

			Convey("with invalid content attributes", func() {
				backend.config.ContentAttributeImageURLPrefix = testdata.ContentAttributeImageURLPrefix

				mockBackendClients.contentAttributeDAO.On(
					createContentAttributes, Anything, Anything).Return(4, nil)

				Convey("with ID'd content attributes", func() {
					// the first content attribute is skipped as it has an ID
					input.ContentAttributes[0].ID = "12345"
				})

				Convey("with empty attribute key", func() {
					input.ContentAttributes[0].AttributeKey = ""
				})

				Convey("with empty attribute name", func() {
					input.ContentAttributes[0].AttributeName = ""
				})

				Convey("with empty value", func() {
					input.ContentAttributes[0].Value = ""
				})

				Convey("with disallowed image url domain", func() {
					input.ContentAttributes[0].ImageURL = "http://nonono.com/images/1"
				})

				results, err := backend.CreateContentAttributes(context.Background(), input)
				So(err, ShouldBeNil)
				So(len(results.Success), ShouldEqual, 4)
				So(results.Fail[0], ShouldResemble, input.ContentAttributes[0])
			})

			Convey("with content attribute DAO failure", func() {
				Convey("when no new records are written", func() {
					mockBackendClients.contentAttributeDAO.On(
						createContentAttributes, Anything, input.ContentAttributes).Return(0, errors.New("DAO failed"))

					_, err := backend.CreateContentAttributes(context.Background(), input)
					So(err, ShouldNotBeNil)
				})

				Convey("when some new records are written", func() {
					mockBackendClients.contentAttributeDAO.On(
						createContentAttributes, Anything, input.ContentAttributes).Return(2, errors.New("DAO failure"))

					_, err := backend.CreateContentAttributes(context.Background(), input)
					So(err, ShouldNotBeNil)
				})
			})
		})

		Convey("GetContentAttributesForChannel", func() {
			Convey("with empty channelID", func() {
				_, err := backend.GetContentAttributesForChannel(context.Background(), "")

				So(err, ShouldNotBeNil)
				mockBackendClients.contentAttributeDAO.AssertNotCalled(t, getContentAttributesForChannel)
			})

			Convey("with valid channelID", func() {
				Convey("with success", func() {
					mockBackendClients.contentAttributeDAO.On(
						getContentAttributesForChannel, Anything, testdata.Channel1ID).Return(testdata.ContentAttributes(), nil)
					ctx := context.Background()
					contentAttributes, err :=
						backend.GetContentAttributesForChannel(ctx, testdata.Channel1ID)

					So(err, ShouldBeNil)
					So(contentAttributes, ShouldResemble, testdata.ContentAttributes())
					mockBackendClients.contentAttributeDAO.AssertCalled(
						t, getContentAttributesForChannel, ctx, testdata.Channel1ID)
				})

				Convey("when content attributes DAO fails", func() {
					mockBackendClients.contentAttributeDAO.On(
						getContentAttributesForChannel, Anything, testdata.Channel1ID).Return(nil, errors.New("DAO failure"))

					_, err := backend.GetContentAttributesForChannel(context.Background(), testdata.Channel1ID)
					So(err, ShouldNotBeNil)
				})
			})
		})

		Convey("DeleteContentAttributes", func() {
			input := &ContentAttributesBulkParams{
				ChannelID:         testdata.Channel1ID,
				ContentAttributes: testdata.ContentAttributes(),
			}

			Convey("with nil params", func() {
				_, err := backend.DeleteContentAttributes(context.Background(), nil)
				So(err, ShouldNotBeNil)
			})

			Convey("with empty channel ID", func() {
				input.ChannelID = ""
				_, err := backend.DeleteContentAttributes(context.Background(), input)
				So(err, ShouldNotBeNil)
			})

			Convey("with no items to delete", func() {
				input.ContentAttributes = []*models.ContentAttribute{}
				res, err := backend.DeleteContentAttributes(context.Background(), input)
				So(err, ShouldBeNil)
				So(len(res.Success), ShouldEqual, 0)
				So(len(res.Fail), ShouldEqual, 0)
			})

			Convey("with multiple items", func() {
				mockBackendClients.contentAttributeDAO.On(
					deleteContentAttributesByIDs, Anything, Anything, Anything).Return(len(input.ContentAttributes), nil)

				res, err := backend.DeleteContentAttributes(context.Background(), input)
				So(err, ShouldBeNil)
				So(len(res.Success), ShouldEqual, len(input.ContentAttributes))
				So(len(res.Fail), ShouldEqual, 0)
			})

			Convey("with invalid content attributes", func() {
				mockBackendClients.contentAttributeDAO.On(
					deleteContentAttributesByIDs, Anything, Anything, Anything).Return(len(input.ContentAttributes)-1, nil)

				Convey("that has empty content attribute ID", func() {
					input.ContentAttributes[1].ID = ""
					results, err := backend.DeleteContentAttributes(context.Background(), input)
					So(err, ShouldBeNil)
					So(len(results.Success), ShouldEqual, len(input.ContentAttributes)-1)
					So(results.Fail[0], ShouldResemble, input.ContentAttributes[1])
				})

				Convey("that belongs to a different owner channel", func() {
					input.ContentAttributes[1].OwnerChannelID = testdata.Channel2ID
					results, err := backend.DeleteContentAttributes(context.Background(), input)
					So(err, ShouldBeNil)
					So(len(results.Success), ShouldEqual, len(input.ContentAttributes)-1)
					So(results.Fail[0], ShouldResemble, input.ContentAttributes[1])
				})
			})

			Convey("with content attribute DAO failures", func() {
				mockBackendClients.contentAttributeDAO.On(
					deleteContentAttributesByIDs, Anything, Anything, Anything).Return(0, errors.New("DAO failed"))

				_, err := backend.DeleteContentAttributes(context.Background(), input)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("UpdateContentAttributes", func() {
			input := &ContentAttributesBulkParams{
				ChannelID:         testdata.Channel1ID,
				ContentAttributes: testdata.ContentAttributes(),
			}

			mockBackendClients.clock.On("Now").Return(testUpdateTime)
			mockBackendClients.channelRelationDAO.On(getChannelRelationsByChannelID, Anything, Anything).Return([]*models.ChannelRelation{}, nil)

			Convey("with nil params", func() {
				_, err := backend.UpdateContentAttributes(context.Background(), nil)
				So(err, ShouldNotBeNil)
			})

			Convey("with no channel ID", func() {
				input.ChannelID = ""
				_, err := backend.UpdateContentAttributes(context.Background(), input)
				So(err, ShouldNotBeNil)
			})

			Convey("with no item to update", func() {
				input.ContentAttributes = []*models.ContentAttribute{}
				results, err := backend.UpdateContentAttributes(context.Background(), input)

				So(err, ShouldBeNil)
				So(results.Success, ShouldBeEmpty)
				So(results.Fail, ShouldBeEmpty)
				mockBackendClients.contentAttributeDAO.AssertNotCalled(t, updateContentAttributes)
			})

			Convey("with multiple items to update", func() {
				mockBackendClients.contentAttributeDAO.On(updateContentAttributes, Anything, Anything, Anything, Anything).Return(len(input.ContentAttributes), nil)
				results, err := backend.UpdateContentAttributes(context.Background(), input)

				So(err, ShouldBeNil)
				So(results.Success, ShouldResemble, input.ContentAttributes)
				So(results.Fail, ShouldBeEmpty)

				for _, attribute := range results.Success {
					So(attribute.UpdatedAt, ShouldEqual, testUpdateTime)
				}
			})

			Convey("with content attributes not owned by the given channel", func() {
				input.ChannelID = "blah"
				results, err := backend.UpdateContentAttributes(context.Background(), input)

				So(err, ShouldBeNil)
				So(results.Success, ShouldBeEmpty)
				So(results.Fail, ShouldResemble, input.ContentAttributes)
			})

			Convey("with invalid content attributes", func() {
				mockBackendClients.contentAttributeDAO.On(updateContentAttributes, Anything, Anything).Return(4, nil)

				Convey("that has no ID", func() {
					input.ContentAttributes[0].ID = ""
					results, err := backend.UpdateContentAttributes(context.Background(), input)
					So(err, ShouldBeNil)
					So(len(results.Success), ShouldEqual, 4)
					So(results.Fail[0], ShouldResemble, input.ContentAttributes[0])
				})

				Convey("that has empty attribute key", func() {
					input.ContentAttributes[0].AttributeKey = ""
					results, err := backend.UpdateContentAttributes(context.Background(), input)
					So(err, ShouldBeNil)
					So(len(results.Success), ShouldEqual, 4)
					So(results.Fail[0], ShouldResemble, input.ContentAttributes[0])
				})

				Convey("that has empty attribute name", func() {
					input.ContentAttributes[0].AttributeName = ""
					results, err := backend.UpdateContentAttributes(context.Background(), input)
					So(err, ShouldBeNil)
					So(len(results.Success), ShouldEqual, 4)
					So(results.Fail[0], ShouldResemble, input.ContentAttributes[0])
				})

				Convey("that has empty value", func() {
					input.ContentAttributes[0].Value = ""
					results, err := backend.UpdateContentAttributes(context.Background(), input)
					So(err, ShouldBeNil)
					So(len(results.Success), ShouldEqual, 4)
					So(results.Fail[0], ShouldResemble, input.ContentAttributes[0])
				})
			})

			Convey("with content attribute DAO failure", func() {
				Convey("when no records are updated", func() {
					mockBackendClients.contentAttributeDAO.On(updateContentAttributes, Anything, input.ContentAttributes).Return(0, errors.New("DAO failed"))
					_, err := backend.UpdateContentAttributes(context.Background(), input)
					So(err, ShouldNotBeNil)
				})

				Convey("when some records are updated", func() {
					mockBackendClients.contentAttributeDAO.On(updateContentAttributes, Anything, input.ContentAttributes).Return(2, errors.New("DAO failed"))
					_, err := backend.UpdateContentAttributes(context.Background(), input)
					So(err, ShouldNotBeNil)
				})
			})
		})

		Convey("CreateContentAttributeImageUploadConfig", func() {
			Convey("with empty channel ID", func() {
				_, err := backend.CreateContentAttributeImageUploadConfig(context.Background(), "")
				So(err, ShouldNotBeNil)
			})

			Convey("when upload service client returns successfully", func() {
				config := &models.ContentAttributeImageUploadConfig{
					ChannelID: testdata.Channel1ID,
					ImageURL:  "http://image.url",
					UploadURL: "http//upload.url",
					UploadID:  "12345",
				}

				mockBackendClients.clients.UploadService.On(createContentAttributeImageUploadConfig, Anything, testdata.Channel1ID).Return(config, nil)
				result, err := backend.CreateContentAttributeImageUploadConfig(context.Background(), testdata.Channel1ID)
				So(err, ShouldBeNil)
				So(result, ShouldNotBeNil)
				So(result, ShouldResemble, config)
			})

			Convey("when upload service client returns with failure", func() {
				mockBackendClients.clients.UploadService.On(createContentAttributeImageUploadConfig, Anything, testdata.Channel1ID).Return(nil, errors.New("no upload"))
				_, err := backend.CreateContentAttributeImageUploadConfig(context.Background(), testdata.Channel1ID)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("GetContentAttributesForChanlet", func() {
			Convey("with dao failure", func() {
				mockBackendClients.chanletDAO.On("GetChanletByID", Anything, testdata.Channel1ID).Return(nil, errors.New("dao asplode"))
				result, err := backend.GetContentAttributesForChanlet(context.Background(), testdata.Channel1ID)
				So(result, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})

			Convey("with no chanlet found", func() {
				mockBackendClients.chanletDAO.On("GetChanletByID", Anything, testdata.Channel1ID).Return(nil, nil)
				result, err := backend.GetContentAttributesForChanlet(context.Background(), testdata.Channel1ID)
				So(result, ShouldBeNil)
				So(err, ShouldEqual, ErrChanletNotFound)
			})

			Convey("with success", func() {
				contentAttributes := testdata.ContentAttributes()
				contentAttributeIDs := testdata.ContentAttributeIDs()
				chanlet := &models.Chanlet{
					ChanletID:           testdata.Channel1ID,
					OwnerChannelID:      testdata.Channel2ID,
					ContentAttributeIDs: contentAttributeIDs,
				}
				mockBackendClients.chanletDAO.On("GetChanletByID", Anything, testdata.Channel1ID).Return(chanlet, nil)
				mockBackendClients.contentAttributeDAO.On("GetContentAttributesByIDs", Anything, testdata.Channel2ID, Anything).Return(contentAttributes, nil)
				result, err := backend.GetContentAttributesForChanlet(context.Background(), testdata.Channel1ID)
				So(err, ShouldBeNil)
				So(result, ShouldResemble, contentAttributes)
			})
		})
	})
}
