package backend

import (
	"context"
	"testing"

	"code.justin.tv/commerce/nioh/pkg/dynamodb/dao"
	"code.justin.tv/commerce/nioh/rpc/nioh"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestRecordAPI(t *testing.T) {
	Convey("Test API", t, func() {
		mockBackend, mockBackendClients := initTestableBackend()

		Convey("Test GetRecord", func() {
			req := &nioh.GetRecordRequest{
				ChannelId: "123",
			}

			Convey("with success", func() {
				mockBackendClients.testTableDAO.On("GetTestItem", mock.Anything, mock.Anything).Return(&dao.TestData{Msg: "testdata"}, nil)
				res, err := mockBackend.GetRecord(context.Background(), req)
				So(err, ShouldBeNil)
				So(res.Data, ShouldEqual, "testdata")
			})

			Convey("with failure", func() {
				mockBackendClients.testTableDAO.On("GetTestItem", mock.Anything, mock.Anything).Return(nil, errors.New("test error"))
				res, err := mockBackend.GetRecord(context.Background(), req)
				So(err, ShouldNotBeNil)
				So(res, ShouldBeNil)
			})
		})
	})
}
