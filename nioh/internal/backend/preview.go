package backend

import (
	"context"
	"time"

	"code.justin.tv/commerce/nioh/pkg/dynamodb/provider"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/nioh/internal/models"
	"github.com/pkg/errors"
)

// GetPreview returns the preview record from Dynamo
func (b *backendImpl) GetPreview(ctx context.Context, userID string, resource models.RestrictableResourceWithRequester) (*models.Preview, error) {
	previewKey := b.constructPreviewKey(userID, resource.GetResourceKeyWithRequester())
	previewRecord, err := b.daos.PreviewDAO.GetPreview(ctx, previewKey)

	if err != nil {
		return nil, errors.Wrap(err, "unexpected error from PreviewDAO.GetPreview while handling GetPreview")
	}

	return previewRecord, nil
}

// SetPreview sets a preview record to Dynamo
func (b *backendImpl) SetPreview(ctx context.Context, userID string, resource models.RestrictableResourceWithRequester) (*models.Preview, error) {
	if userID == "" || resource.GetIdentifier() == "" {
		return nil, errors.Errorf("SetPreview requires both a userID and a valid resource - userID: %v, resource: %v", userID, resource)
	}

	previewKey := b.constructPreviewKey(userID, resource.GetResourceKeyWithRequester())

	now := b.clock.Now()

	previewDuration := models.PreviewDuration

	// If a origin exists (and is unique) use a special duration instead of the default preview duration
	if resource.GetResourceKeyWithRequester() != resource.GetResourceKey() {
		previewDuration = models.ShortPreviewDuration
	}

	preview := &models.Preview{
		PreviewKey:       previewKey,
		ResourceKey:      resource.GetResourceKeyWithRequester(),
		UserID:           userID,
		CreatedAt:        now,
		ContentExpiresAt: now.Add(previewDuration),
		ResetAt:          now.Add(models.PreviewCooldown),
	}

	go func() {
		newCtx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
		defer cancel()

		newCtx = provider.WithDynamicFallback(newCtx)
		// Prefer DAX for this write. Works as expected in environments without DAX, which will automatically fall back.
		newCtx = provider.SetClientOnContext(newCtx, provider.DaxKey)

		shouldRecordPreview := true

		// Channels that are in offline, or unknown states will vend a preview without recording it.
		// This is primarily to address Player UI's behavior of asking for an Nauth token even
		// for offline streams
		if resource.GetType() == models.ResourceTypeChannel {
			streamsResponse, err := b.clients.Liveline.GetStreamsByChannelIDs(context.Background(), []string{resource.GetIdentifier()})
			if err != nil {
				logrus.WithError(err).WithField("PreviewRecord", preview).Error("Liveline failure in SetPreview. Vending unrecorded free preview.")
				return
			}

			if len(streamsResponse.Streams) == 0 {
				shouldRecordPreview = false
				logrus.Debug("Stream offline. Vending unrecorded free preview")
			}
		}

		if shouldRecordPreview {
			if err := b.daos.PreviewDAO.SetPreview(newCtx, preview); err != nil {
				logrus.WithField("PreviewRecord", preview).Error(errors.Wrapf(err, "failed to create preview database entry for preview %s", preview.PreviewKey))
			}
		}

	}()

	return preview, nil
}

// DeletePreview deletes and returns preview record for the passed resource. If no preview exists, nil is returned.
func (b *backendImpl) DeletePreview(ctx context.Context, userID string, resource models.RestrictableResourceWithRequester) (*models.Preview, error) {
	preview, err := b.GetPreview(ctx, userID, resource)
	if err != nil {
		return nil, errors.Wrap(err, "unexpected error from PreviewDAO.GetPreview while handling DeletePreview")
	}
	if preview == nil {
		return nil, nil
	}
	if err := b.daos.PreviewDAO.DeletePreview(ctx, preview.PreviewKey); err != nil {
		return nil, errors.Wrap(err, "unexpected error from PreviewDAO.DeletePreview while handling DeletePreview")
	}
	return preview, nil
}

// ConsumePreview returns a preview record if a user is eligible, and nil if no preview is currently available
func (b *backendImpl) ConsumePreview(ctx context.Context, userID string, resource models.RestrictableResourceWithRequester) (*models.Preview, error) {
	if userID == "" || resource.GetIdentifier() == "" {
		logrus.Debugf("ConsumePreview requires both a userID and a valid resource - userID: %v, resource: %v", userID, resource)
		return nil, nil
	}

	// intervene to resolve a chanlet resource to an owning channel for coordinated preview records
	resolvedResource := resource
	if resource.GetType() == models.ResourceTypeChannel {
		resolvedChannelID, err := b.resolveOwnerChannelID(ctx, resource.GetIdentifier())
		if err != nil {
			return nil, err
		}

		resolvedResource = models.Channel{
			ID:              resolvedChannelID,
			RequesterOrigin: resource.GetRequesterOrigin(),
		}
	}

	previewRecord, err := b.GetPreview(ctx, userID, resolvedResource)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get preview from GetPreview backend")
	}

	now := b.clock.Now()

	if previewRecord != nil {
		if previewRecord.ResetAt.Before(now) {
			logrus.Debug("Preview record needs to be reset")
			// The user has an old expired preview and we need to log a new one
			previewRecord, err = b.SetPreview(ctx, userID, resolvedResource)
			if err != nil {
				return previewRecord, errors.Wrap(err, "failed to set user resource preview from SetPreview backend")
			}
		} else if previewRecord.ContentExpiresAt.After(now) {
			logrus.Debug("Preview record is active")
			// The user has an active preview that is still running
			return previewRecord, nil
		} else {
			logrus.Debug("Preview record is expired")
			// The user has an active preview that has run out
			return nil, nil
		}
	} else {
		logrus.Debug("Preview record does not exist")
		// The user has never previewed this resource
		previewRecord, err = b.SetPreview(ctx, userID, resolvedResource)
		if err != nil {
			return previewRecord, errors.Wrap(err, "failed to set user resource preview from SetPreview backend")
		}
	}
	logrus.WithField("PreviewRecord", previewRecord).Debug("Preview record created")

	return previewRecord, nil
}

func (b *backendImpl) constructPreviewKey(userID string, resourceKey string) string {
	return userID + ":" + resourceKey
}
