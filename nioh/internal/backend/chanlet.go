package backend

import (
	"context"
	"fmt"
	"sort"
	"time"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/nioh/internal/models"
	"code.justin.tv/commerce/nioh/pkg/helpers"
	"code.justin.tv/common/chitin"
	"github.com/pkg/errors"
)

var niohBirthDate = time.Date(2018, 9, 24, 18, 0, 0, 0, time.UTC)

const (
	maxChanletCount int64 = 30

	chanletScanLimit int64 = 200
)

var (
	// ErrChanletNotFound indicates that chanlet wasn't found
	ErrChanletNotFound = errors.New("no such chanlet")
	// ErrChanletAlreadyArchived indicates that chanlet is already archived
	ErrChanletAlreadyArchived = errors.New("chanlet is already archived")
)

// UpdateChanletInput has all the params to update a chanlet information
type UpdateChanletInput struct {
	OwnerChannelID      string
	ChanletID           string
	ContentAttributeIDs []string
}

// UpdateChanletOutput has the updated results of a chanlet
type UpdateChanletOutput struct {
	Chanlet *models.Chanlet
}

// GetChanletsOptions specifies how the list of chanlets returned by GetChanlets should be modulated (such as by sorting,
// pinning, or filtering)
type GetChanletsOptions struct {
	Sort ChanletSort
}

// ChanletSort indicates what sorting (if any) should be done to the chanlets list
type ChanletSort int

const (
	// None indicates that chanlets should be returned in the order returned by Dynamo
	None ChanletSort = 0
	// ViewerCount indicates that the chanlets should be sorted by viewer count (descending)
	ViewerCount ChanletSort = 1
)

// Implement pinning chanlets to front of chanlet list by implementing sort.Interface
type chanletPinner struct {
	chanlets     []*models.Chanlet
	pinTargetIDs []string
}

func (c *chanletPinner) Len() int { return len(c.chanlets) }
func (c *chanletPinner) Less(i, j int) bool {
	var iTargetMatch, jTargetMatch bool
	var iTargetIx, jTargetIx int

	for targetIx, target := range c.pinTargetIDs {
		if c.chanlets[i].ChanletID == target {
			iTargetMatch = true
			iTargetIx = targetIx
		}
		if c.chanlets[j].ChanletID == target {
			jTargetMatch = true
			jTargetIx = targetIx
		}
	}

	if iTargetMatch && jTargetMatch {
		// Stabilize target sorts by order in specified target IDs
		return iTargetIx < jTargetIx
	}

	return iTargetMatch
}

func (c *chanletPinner) Swap(i, j int) {
	c.chanlets[i], c.chanlets[j] = c.chanlets[j], c.chanlets[i]
}

func pinChanlets(chanlets []*models.Chanlet, pinTargetIDs []string) {
	sort.Stable(&chanletPinner{
		chanlets:     chanlets,
		pinTargetIDs: pinTargetIDs,
	})
}

// GetChanlets is used to get chanlets for a channel.
func (b *backendImpl) GetChanlets(ctx context.Context, channelID string, options GetChanletsOptions) ([]*models.Chanlet, error) {
	chanlets := []*models.Chanlet{}

	// Retrieve chanlets this channel owns
	ownedChanlets, err := b.getChanletsFromOwnerChannel(ctx, channelID)
	if err != nil {
		return nil, errors.Wrap(err, "failed to retrieve owned chanlets")
	}

	// Multiview-enable channel should at least have its OwnerChanlet, if the result is empty, bail early.
	if len(ownedChanlets) == 0 {
		return ownedChanlets, nil
	}

	// Hydrate requester's chanlets with content attributes
	err = b.hydrateChanletsWithContentAttributes(ctx, channelID, ownedChanlets)
	if err != nil {
		return nil, errors.Wrap(err, "failed to populate content attributes for requester channel's chanlets")
	}

	chanlets = append(chanlets, ownedChanlets...)

	// See if the channel is a borrower
	channelRelations, err := b.daos.ChannelRelationDAO.GetChannelRelationsByChannelID(ctx, channelID)
	if err != nil {
		return nil, errors.Wrap(err, "failed to retrieve channel relation records")
	}

	// TODO: we don't really have a use case where this loop runs more than once.
	// When we do, it would be a good idea to implement batch Get in DAO and use that instead.
	for _, cr := range channelRelations {
		if cr.Relation == models.RelationBorrower {
			// NOTE: same as above, we are assuming a single owner for now
			// as in, a channel does not borrow chanlets from multiple owners
			ownerChannelID := cr.RelatedChannelID
			borrowedChanlets, err := b.getChanletsFromOwnerChannel(ctx, ownerChannelID)
			if err != nil {
				return nil, errors.Wrap(err, "failed to retrieve borrowed chanlets")
			}

			// Make sure to NOT include the owner channel's chanlet in the response
			var borrowedChanletsWithoutOwner []*models.Chanlet
			for _, borrowedChanlet := range borrowedChanlets {
				if borrowedChanlet.ChanletID != ownerChannelID {
					borrowedChanletsWithoutOwner = append(borrowedChanletsWithoutOwner, borrowedChanlet)
				}
			}

			// Hydrate borrowed chanlets with content attributes
			err = b.hydrateChanletsWithContentAttributes(ctx, ownerChannelID, borrowedChanletsWithoutOwner)
			if err != nil {
				return nil, errors.Wrap(err, "failed to populate content attributes for borrowed chanlets")
			}

			chanlets = append(chanlets, borrowedChanletsWithoutOwner...)
		}
	}

	switch options.Sort {
	case None: /* do nothing */
	case ViewerCount:
		chanlets, err = b.orderChanletsByDescViewCount(ctx, chanlets)
	default: /* do nothing */
	}

	pinChanlets(chanlets, []string{channelID})

	return chanlets, err
}

// ListAllChanlets returns a paginated list of all chanlets using the cursor
func (b *backendImpl) ListAllChanlets(ctx context.Context, cursor string) ([]*models.Chanlet, string, error) {
	chanlets, nextCursor, err := b.daos.ChanletDAO.ListAllChanlets(ctx, cursor, chanletScanLimit)

	if err != nil {
		return nil, "", errors.Wrap(err, "failed to get chanlets from dao")
	}

	return chanlets, nextCursor, nil
}

// order the streams by viewcount in decreasing order. Does not return an error if getting viewcount fails
func (b *backendImpl) orderChanletsByDescViewCount(ctx context.Context, chanlets []*models.Chanlet) ([]*models.Chanlet, error) {
	chanletIDs := make([]string, 0, len(chanlets))
	idTochanletMapping := make(map[string]*models.Chanlet)

	for _, chanlet := range chanlets {
		chanletIDs = append(chanletIDs, chanlet.ChanletID)
		idTochanletMapping[chanlet.ChanletID] = chanlet
	}

	streamsResponse, err := b.clients.Liveline.GetStreamsByChannelIDs(ctx, chanletIDs)
	if err != nil {
		logrus.WithError(err).Error("failed to get stream counts for chanlets")
	}

	// Map from view count to chanlet info
	viewMapping := make(map[int][]*models.Chanlet)
	if streamsResponse != nil && len(streamsResponse.Streams) > 0 {
		for _, stream := range streamsResponse.Streams {
			var viewCount int
			if stream.ViewcountData != nil {
				viewCount = int(stream.ViewcountData.Viewcount)
			}
			channelID := stream.ChannelId

			chanlet, ok := idTochanletMapping[channelID]
			if !ok {
				logrus.Error("Liveline returned channel not in list")
			} else {
				//Remove mapping to check for offline streams later
				delete(idTochanletMapping, channelID)
				viewMapping[viewCount] = append(viewMapping[viewCount], chanlet)
			}
		}
	}

	// Chanlets left in the map are offline (view count 0), append in id-order to maintain offline order
	idKeys := make([]string, 0, len(idTochanletMapping))
	for k := range idTochanletMapping {
		idKeys = append(idKeys, k)
	}
	sort.Strings(idKeys)

	for _, chanletID := range idKeys {
		val := idTochanletMapping[chanletID]
		viewMapping[0] = append(viewMapping[0], val)
	}

	// Sort viewership, descending order
	keys := make([]int, 0, len(viewMapping))
	for k := range viewMapping {
		keys = append(keys, k)
	}
	sort.Sort(sort.Reverse(sort.IntSlice(keys)))

	sortedChanlets := []*models.Chanlet{}
	for _, index := range keys {
		chanlets, ok := viewMapping[index]
		if !ok {
			logrus.Error("Attempted to get chanlets with a viewcount that does not exist")
		} else {
			sortedChanlets = append(sortedChanlets, chanlets...)
		}
	}

	return sortedChanlets, nil
}

// CreateChanlet is used to create a new chanlet for a given channel
func (b *backendImpl) CreateChanlet(ctx context.Context, ownerID string) (*models.Chanlet, error) {
	// First, create owner chanlet if it does not exist yet
	err := b.CreateOwnerChanlet(ctx, ownerID)
	if err != nil {
		return nil, errors.Wrapf(err, "failed to create owner chanlet while creating chanlet for owner channel %s", ownerID)
	}

	chanletCount, err := b.daos.ChanletDAO.GetChanletCount(ctx, ownerID)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get current chanlet count")
	}
	if chanletCount >= maxChanletCount {
		return nil, fmt.Errorf("reached max chanlet count of %d", maxChanletCount)
	}

	// Create A User
	generatedLogin := generateUniqueLogin()
	newChanlet, err := b.clients.UsersClient.CreateHiddenUser(ctx, generatedLogin)
	if err != nil {
		return nil, errors.Wrapf(err, "failed to create chanlet user with login %s", generatedLogin)
	}

	// Create DB entry
	chanlet := &models.Chanlet{
		OwnerChannelID:  ownerID,
		ChanletID:       newChanlet.ID,
		IsPublicChannel: false,
	}
	err = b.daos.ChanletDAO.AddChanlet(ctx, chanlet)
	if err != nil {
		return nil, errors.Wrapf(err, "failed to create chanlet database row with login %s", generatedLogin)
	}

	// Disable ads properties for chanlet. Does not return error if a call fails.
	go func() {
		// context.TODO because there is already an HTTP client timeout on this client – decide whether we want redundancy
		// in a context timeout too.
		if err := b.clients.SiosaClient.DisableAdsForChannel(context.TODO(), chanlet.ChanletID); err != nil {
			logrus.WithFields(logrus.Fields{
				"OwnerID":   ownerID,
				"ChanletID": chanlet.ChanletID,
			}).WithError(err).Error("could not disable ads for channel")
		}
	}()

	return chanlet, nil
}

// AddOrReplaceChanlet takes a chanlet and attempts to save it to the DB (will replace if one with the same ID already exists)
func (b *backendImpl) AddOrReplaceChanlet(ctx context.Context, chanlet *models.Chanlet) error {
	return b.daos.ChanletDAO.AddChanlet(ctx, chanlet)
}

// UpdateChanletContentAttributes updates the list of content attributes a chanlet is tagged with
func (b *backendImpl) UpdateChanletContentAttributes(ctx context.Context, input *UpdateChanletInput) (*UpdateChanletOutput, error) {
	if input == nil {
		return nil, errors.New("input is required")
	}
	if input.OwnerChannelID == "" {
		return nil, errors.New("ownerChannelID is required")
	}
	if input.ChanletID == "" {
		return nil, errors.New("chanletID is required")
	}

	var chanlet *models.Chanlet
	var err error

	// update content attributes for this chanlet
	dedupedIDs := b.dedupeContentAttributeIDs(input.ContentAttributeIDs)
	chanlet, err = b.daos.ChanletDAO.UpdateChanletContentAttributes(ctx, input.OwnerChannelID, input.ChanletID, dedupedIDs)
	if err != nil {
		return nil, errors.Wrapf(err, "failed to update content attributes for chanlet (ownerChannelID %s, chanletID %s)", input.OwnerChannelID, input.ChanletID)
	}

	err = b.hydrateChanletsWithContentAttributes(ctx, input.OwnerChannelID, []*models.Chanlet{chanlet})
	if err != nil {
		return nil, errors.Wrap(err, "failed to populate content attributes for chanlets")
	}

	// publish chanlet update to pubsub
	go func(chanlet *models.Chanlet) {
		ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
		defer cancel()

		log := logrus.WithFields(logrus.Fields{
			"RequestID":      chitin.GetTraceID(ctx),
			"OwnerChannelID": chanlet.OwnerChannelID,
			"ChanletID":      chanlet.ChanletID,
		})
		log.Info("Sending pubsub messages for chanlet content attributes update")

		err := b.PublishChanletUpdate(ctx, chanlet)
		if err != nil {
			log.WithError(err).Error("Failed to send out pubsub message for chanlet update")
		}
	}(chanlet)

	return &UpdateChanletOutput{
		Chanlet: chanlet,
	}, nil
}

func (b *backendImpl) PublishChanletUpdate(ctx context.Context, chanlet *models.Chanlet) error {
	channelIDs := []string{chanlet.OwnerChannelID}
	if chanlet.OwnerChannelID != chanlet.ChanletID {
		// chanlet is a chanlet and not the parent itself
		// find all borrowers who borrow chanlets from this chanlet's owner
		relations, err := b.GetChannelRelationsByRelatedChannelID(ctx, chanlet.OwnerChannelID)
		if err != nil {
			logrus.WithFields(logrus.Fields{
				"OwnerChannelID": chanlet.OwnerChannelID,
				"ChanletID":      chanlet.ChanletID,
			}).WithError(err).Errorf("Failed to find borrowers of chanlet (ownerChannelID %s chanletID %s), sending pubsub update to only the owner", chanlet.OwnerChannelID, chanlet.ChanletID)
			relations = []*models.ChannelRelation{}
		}

		// updates should be sent out to chanlet owner and each of the borrowers
		for _, relation := range relations {
			if relation.Relation == models.RelationBorrower {
				channelIDs = append(channelIDs, relation.ChannelID)
			}
		}
	}

	return b.clients.Pubsub.PublishChanletUpdate(ctx, channelIDs, chanlet)
}

// GetChanletStreamKey gets the stream key for a specific chanlet
func (b *backendImpl) GetChanletStreamKey(ctx context.Context, ownerChannelID string, chanletID string) (string, error) {
	// First, check if the given input is actually a chanlet in Nioh so that we don't get the stream key for any channel
	chanlet, err := b.daos.ChanletDAO.GetChanlet(ctx, ownerChannelID, chanletID)
	if err != nil {
		return "", errors.Wrapf(err, "failed to check chanlet existence for ownerChannelID %s and chanletID %s", ownerChannelID, chanletID)
	} else if chanlet == nil {
		return "", ErrChanletNotFound
	}

	// Second, check if the chanlet is a public channel and is not the owner chanlet so that we don't get the stream key for a channel that's
	// not primarily a chanlet owned by the owner channel
	if chanlet.IsPublicChannel && !chanlet.IsOwnerChanlet() {
		return "", nil
	}

	// Get the chanlet stream key through ChannelProperties
	streamKey, err := b.clients.ChannelProperties.GetStreamKey(ctx, chanletID)
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"ownerChannelID": ownerChannelID,
			"chanletID":      chanletID,
		}).WithError(err).Error("failed to get chanlet stream key for chanletID")
		return "", nil
	}

	return streamKey, nil
}

func (b *backendImpl) GetChanletByID(ctx context.Context, chanletID string) (*models.Chanlet, error) {
	chanlet, err := b.daos.ChanletDAO.GetChanletByID(ctx, chanletID)
	if err != nil {
		return nil, errors.Wrap(err, "failed to fetch chanlet from db")
	}

	return chanlet, nil
}

// note: resolveOwnerChannelID assumes only a single level of ownership. make it recursive if we ever have more nesting.
func (b *backendImpl) resolveOwnerChannelID(ctx context.Context, queryChannelID string) (string, error) {
	chanlet, err := b.daos.ChanletDAO.GetChanletByID(ctx, queryChannelID)
	if err != nil {
		return queryChannelID, errors.Wrapf(err, "failed to look up chanlet for channelID=%s", queryChannelID)
	}

	if chanlet != nil {
		return chanlet.OwnerChannelID, nil
	}

	return queryChannelID, nil
}

func (b *backendImpl) getChanletsFromOwnerChannel(ctx context.Context, channelID string) ([]*models.Chanlet, error) {
	return b.daos.ChanletDAO.GetChanlets(ctx, channelID)
}

func (b *backendImpl) hydrateChanletsWithContentAttributes(ctx context.Context, ownerChannelID string, chanlets []*models.Chanlet) error {
	if len(chanlets) == 0 {
		return nil
	}

	// collect all content attribute IDs from all chanlets
	// using a map to remove duplicates
	idMap := make(map[string]bool)
	for _, chanlet := range chanlets {
		for _, id := range chanlet.ContentAttributeIDs {
			idMap[id] = true
		}
	}
	if len(idMap) == 0 {
		return nil
	}
	// coverting from map to a slice
	ids := make([]string, 0, len(idMap))
	for id := range idMap {
		ids = append(ids, id)
	}

	// retrieve all content attributes, store in a map
	attributes, err := b.daos.ContentAttributeDAO.GetContentAttributesByIDs(ctx, ownerChannelID, ids)
	if err != nil {
		return errors.Wrap(err, "failed to retrieve content attributes")
	}
	attributesByID := make(map[string]*models.ContentAttribute)
	for _, attribute := range attributes {
		attributesByID[attribute.ID] = attribute
	}

	// hydrate chanlets with content attributes
	// removing content attribute IDs that are not found in the datastore (they have likely been deleted)
	for _, chanlet := range chanlets {
		foundIDs := []string{}
		for _, id := range chanlet.ContentAttributeIDs {
			if attributesByID[id] == nil {
				continue
			}
			chanlet.ContentAttributes = append(chanlet.ContentAttributes, attributesByID[id])
			foundIDs = append(foundIDs, id)
		}

		chanlet.ContentAttributeIDs = foundIDs

		// add computed content attributes (currently just for OWL S2)
		// TODO this feature should probably be gated depending on the channel
		computedContentAttributes := helpers.GetComputedContentAttributes(chanlet)
		for _, attribute := range computedContentAttributes {
			chanlet.ContentAttributeIDs = append(chanlet.ContentAttributeIDs, attribute.ID)
			chanlet.ContentAttributes = append(chanlet.ContentAttributes, attribute)
		}
	}
	return nil
}

func (b *backendImpl) dedupeContentAttributeIDs(ids []string) []string {
	var dedupedIDs []string
	seen := make(map[string]bool)
	for _, id := range ids {
		if _, ok := seen[id]; !ok {
			seen[id] = true
			dedupedIDs = append(dedupedIDs, id)
		}
	}

	return dedupedIDs
}

// CreateOwnerChanlet creates a chanlet that has its chanletID the same as it ownerChannelID (hence the name ownerChanlet)
func (b *backendImpl) CreateOwnerChanlet(ctx context.Context, ownerChannelID string) error {
	// Make sure to not overwrite an existing ownerChanlet entry if it exists
	chanlet, err := b.daos.ChanletDAO.GetChanlet(ctx, ownerChannelID, ownerChannelID)
	if err != nil {
		return errors.Wrapf(err, "failed to check chanlet database row existence for owner channel %s", ownerChannelID)
	} else if chanlet == nil {
		ownerChanlet := &models.Chanlet{
			OwnerChannelID:  ownerChannelID,
			ChanletID:       ownerChannelID,
			IsPublicChannel: true, // owner channel is a public channel connected to an actual user
			OwnerChanletAttributes: &models.OwnerChanletAttributes{
				ChanletsEnabled: false, // by default, the command center is disabled. The user has to manually enable it first.
			},
		}
		err = b.daos.ChanletDAO.AddChanlet(ctx, ownerChanlet)
		if err != nil {
			return errors.Wrapf(err, "failed to create chanlet database row for owner channel %s", ownerChannelID)
		}
	}
	return nil
}

// ArchiveChanlet marks a chanlet as archived. Archived Chanlets will not surface through queries like GetChanlets
func (b *backendImpl) ArchiveChanlet(ctx context.Context, chanletID string) (*models.Chanlet, error) {
	chanlet, err := b.daos.ChanletDAO.GetChanletByID(ctx, chanletID)
	if err != nil {
		return nil, errors.Wrap(err, "failed to fetch chanlet while archiving")
	} else if chanlet == nil {
		return nil, ErrChanletNotFound
	} else if !chanlet.ArchivedDate.IsZero() {
		return nil, ErrChanletAlreadyArchived
	}

	err = b.daos.ChanletDAO.ArchiveChanlet(ctx, chanlet.OwnerChannelID, chanlet.ChanletID)

	if err != nil {
		return nil, errors.Wrap(err, "failed to archive chanlet in dao")
	}

	return chanlet, nil
}

func generateUniqueLogin() string {
	timeSinceNioh := int64(time.Since(niohBirthDate) / 1e3)            // microseconds since birth of nioh
	return fmt.Sprintf("telnach_%s", hashInt64ToString(timeSinceNioh)) // formerly "chanlet_{timeSinceNioh}". "telnach" is a made up word and anagram of "chanlet."
}

func hashInt64ToString(number int64) string {
	// This converts a number to a unique string not containing any number in the output.
	// The purpose of this is to generate a string that doesn't look like any sort of recognizable data (e.g. phone number, zip code)
	hex := fmt.Sprintf("%X", number) // Convert to hex
	var constructed string
	for _, c := range hex {
		if c < 58 {
			// convert remaining number characters to a lower case alphabet
			constructed += string(c + 49)
		} else {
			constructed += string(c)
		}
	}
	return constructed
}
