package backend

import (
	"testing"

	"code.justin.tv/commerce/nioh/internal/orch/autotag/mocks"

	"code.justin.tv/commerce/nioh/config"
	"code.justin.tv/commerce/nioh/internal/clients"
	modelMocks "code.justin.tv/commerce/nioh/internal/models/mocks"
	"code.justin.tv/commerce/nioh/pkg/dynamodb/dao"
	daoMocks "code.justin.tv/commerce/nioh/pkg/dynamodb/dao/mocks"
	errorloggerMock "code.justin.tv/commerce/nioh/pkg/errorlogger/mocks"
	exemptionsMocks "code.justin.tv/commerce/nioh/pkg/exemptions/mocks"
	"github.com/cactus/go-statsd-client/statsd"
	. "github.com/smartystreets/goconvey/convey"
)

type backendMockClients interface {
	AssertExpectations(t *testing.T)
}

type backendMockClientsImpl struct {
	config              *config.Configuration
	clients             *clients.MockClients
	errorlogger         *errorloggerMock.ErrorLogger
	testTableDAO        *daoMocks.ITestTableDAO
	chanletDAO          *daoMocks.IChanletDAO
	channelRelationDAO  *daoMocks.IChannelRelationDAO
	contentAttributeDAO *daoMocks.IContentAttributeDAO
	restrictionDAO      *daoMocks.IRestrictionDAO
	previewDAO          *daoMocks.IPreviewDAO
	clock               *modelMocks.IClock
	exemptionChecker    *exemptionsMocks.IExemptionChecker
	autotagger          *mocks.Autotagger
}

func initTestableBackend() (*backendImpl, *backendMockClientsImpl) {
	stats, err := statsd.NewNoopClient()
	So(err, ShouldBeNil)
	errorlogger := &errorloggerMock.ErrorLogger{}
	clientsImpl, clientsMocks := clients.InitMockClients()

	cfg := &config.Configuration{
		PreviewOriginsMap: map[string]bool{
			"ios:autoplay": true,
		},
	}
	mockRecordDAO := new(daoMocks.ITestTableDAO)
	mockChanletDAO := new(daoMocks.IChanletDAO)
	mockChannelRelationDAO := new(daoMocks.IChannelRelationDAO)
	mockContentAttributeDAO := new(daoMocks.IContentAttributeDAO)
	mockRestrictionDAO := new(daoMocks.IRestrictionDAO)
	mockPreviewDAO := new(daoMocks.IPreviewDAO)
	mockDAOs := &dao.DAOs{
		TestTableDAO:        mockRecordDAO,
		ChanletDAO:          mockChanletDAO,
		ChannelRelationDAO:  mockChannelRelationDAO,
		ContentAttributeDAO: mockContentAttributeDAO,
		RestrictionDAO:      mockRestrictionDAO,
		PreviewDAO:          mockPreviewDAO,
	}
	mockClock := new(modelMocks.IClock)
	mockExemptionChecker := new(exemptionsMocks.IExemptionChecker)
	mockAutotagger := new(mocks.Autotagger)

	return &backendImpl{
			config:           cfg,
			errorlogger:      errorlogger,
			stats:            stats,
			clients:          clientsImpl,
			daos:             mockDAOs,
			clock:            mockClock,
			exemptionChecker: mockExemptionChecker,
			autotagger:       mockAutotagger,
		}, &backendMockClientsImpl{
			config:              cfg,
			clients:             clientsMocks,
			errorlogger:         errorlogger,
			testTableDAO:        mockRecordDAO,
			chanletDAO:          mockChanletDAO,
			channelRelationDAO:  mockChannelRelationDAO,
			contentAttributeDAO: mockContentAttributeDAO,
			restrictionDAO:      mockRestrictionDAO,
			previewDAO:          mockPreviewDAO,
			clock:               mockClock,
			exemptionChecker:    mockExemptionChecker,
			autotagger:          mockAutotagger,
		}
}

func (b *backendMockClientsImpl) AssertExpectations(t *testing.T) {
	b.errorlogger.AssertExpectations(t)
	b.clients.AssertExpectations(t)
	b.testTableDAO.AssertExpectations(t)
	b.chanletDAO.AssertExpectations(t)
	b.channelRelationDAO.AssertExpectations(t)
	b.contentAttributeDAO.AssertExpectations(t)
	b.restrictionDAO.AssertExpectations(t)
	b.previewDAO.AssertExpectations(t)
	b.exemptionChecker.AssertExpectations(t)
	b.autotagger.AssertExpectations(t)
}
