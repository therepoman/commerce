package backend

import (
	"context"
	"math/rand"
	"strconv"
	"testing"
	"time"

	"code.justin.tv/commerce/nioh/internal/models"
	"code.justin.tv/commerce/nioh/internal/testdata"
	"code.justin.tv/commerce/nioh/pkg/helpers"
	"code.justin.tv/discovery/liveline/proto/liveline"
	usersModels "code.justin.tv/web/users-service/models"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestChanletBackend(t *testing.T) {
	Convey("Test Backend", t, func() {
		mockBackend, mockBackendClients := initTestableBackend()
		channelID := "123"
		chanletID := "456"
		usersClientResponse := &usersModels.Properties{
			ID: chanletID,
		}

		ctx := helpers.ContextWithUser(context.Background(), channelID)
		ctx = helpers.ContextWithChannelID(ctx, channelID)

		Convey("Test CreateChanlets", func() {
			Convey("with success", func() {
				mockBackendClients.chanletDAO.On("AddChanlet", mock.Anything, mock.Anything).Return(nil)
				mockBackendClients.clients.UsersClient.On("CreateHiddenUser", mock.Anything, mock.Anything).Return(usersClientResponse, nil)
				mockBackendClients.clients.SiosaClient.On("DisableAdsForChannel", mock.Anything, mock.Anything).Return(nil)
				mockBackendClients.chanletDAO.On("GetChanletCount", mock.Anything, mock.Anything).Return(int64(0), nil)

				Convey("with existing owner chanlet", func() {
					mockBackendClients.chanletDAO.On("GetChanlet", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(&models.Chanlet{}, nil)
					res, err := mockBackend.CreateChanlet(ctx, channelID)
					So(err, ShouldBeNil)
					So(res.ChanletID, ShouldEqual, chanletID)
					So(res.IsPublicChannel, ShouldBeFalse)
					So(res.ContentAttributeIDs, ShouldBeEmpty)
					So(res.ContentAttributes, ShouldBeEmpty)
					// Only the new chanlet was created, not the owner chanlet
					mockBackendClients.chanletDAO.AssertNumberOfCalls(t, "AddChanlet", 1)
				})

				Convey("without existing owner chanlet", func() {
					mockBackendClients.chanletDAO.On("GetChanlet", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil, nil)
					res, err := mockBackend.CreateChanlet(ctx, channelID)
					So(err, ShouldBeNil)
					So(res.ChanletID, ShouldEqual, chanletID)
					So(res.IsPublicChannel, ShouldBeFalse)
					So(res.ContentAttributeIDs, ShouldBeEmpty)
					So(res.ContentAttributes, ShouldBeEmpty)
					// Owner chanlet was also created
					mockBackendClients.chanletDAO.AssertNumberOfCalls(t, "AddChanlet", 2)
				})
			})

			Convey("with DAO failure", func() {
				Convey("with AddChanlet DAO failure", func() {
					mockBackendClients.chanletDAO.On("GetChanlet", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(&models.Chanlet{}, nil)
					mockBackendClients.chanletDAO.On("AddChanlet", mock.Anything, mock.Anything).Return(errors.New("test"))
					mockBackendClients.chanletDAO.On("GetChanletCount", mock.Anything, mock.Anything).Return(int64(0), nil)
					mockBackendClients.clients.UsersClient.On("CreateHiddenUser", mock.Anything, mock.Anything).Return(usersClientResponse, nil)
					res, err := mockBackend.CreateChanlet(ctx, channelID)
					So(err, ShouldNotBeNil)
					So(res, ShouldBeNil)
				})

				Convey("with owner chanlet GetChanlet DAO failure", func() {
					mockBackendClients.chanletDAO.On("GetChanlet", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("test"))
					res, err := mockBackend.CreateChanlet(ctx, channelID)
					So(err, ShouldNotBeNil)
					So(res, ShouldBeNil)
				})

				Convey("with owner chanlet GetChanletCount DAO failure", func() {
					mockBackendClients.chanletDAO.On("GetChanlet", mock.Anything, mock.Anything, mock.Anything).Return(&models.Chanlet{}, nil)
					mockBackendClients.chanletDAO.On("AddChanlet", mock.Anything, mock.Anything).Return(nil)
					mockBackendClients.chanletDAO.On("GetChanletCount", mock.Anything, mock.Anything).Return(int64(0), errors.New("test"))
					mockBackendClients.clients.UsersClient.On("CreateHiddenUser", mock.Anything, mock.Anything).Return(usersClientResponse, nil)
					res, err := mockBackend.CreateChanlet(ctx, channelID)
					So(err, ShouldNotBeNil)
					So(res, ShouldBeNil)
				})
			})

			Convey("with users client failure", func() {
				mockBackendClients.chanletDAO.On("GetChanlet", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(&models.Chanlet{}, nil)
				mockBackendClients.chanletDAO.On("AddChanlet", mock.Anything, mock.Anything).Return(nil)
				mockBackendClients.chanletDAO.On("GetChanletCount", mock.Anything, mock.Anything).Return(int64(0), nil)
				mockBackendClients.clients.UsersClient.On("CreateHiddenUser", mock.Anything, mock.Anything).Return(nil, errors.New("test"))
				mockBackendClients.clients.SiosaClient.On("DisableAdsForChannel", mock.Anything, mock.Anything).Return(nil)
				res, err := mockBackend.CreateChanlet(ctx, channelID)
				So(err, ShouldNotBeNil)
				So(res, ShouldBeNil)
			})

			Convey("with siosa client failure still succeeds", func() {
				mockBackendClients.chanletDAO.On("GetChanlet", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(&models.Chanlet{}, nil)
				mockBackendClients.chanletDAO.On("AddChanlet", mock.Anything, mock.Anything).Return(nil)
				mockBackendClients.chanletDAO.On("GetChanletCount", mock.Anything, mock.Anything).Return(int64(0), nil)
				mockBackendClients.clients.UsersClient.On("CreateHiddenUser", mock.Anything, mock.Anything).Return(usersClientResponse, nil)
				mockBackendClients.clients.SiosaClient.On("DisableAdsForChannel", mock.Anything, mock.Anything).Return(errors.New("test"))
				res, err := mockBackend.CreateChanlet(ctx, channelID)
				So(err, ShouldBeNil)
				So(res.ChanletID, ShouldEqual, chanletID)
				So(res.ContentAttributeIDs, ShouldBeEmpty)
				So(res.ContentAttributes, ShouldBeEmpty)
			})

			Convey("with max chanlet creation count", func() {
				mockBackendClients.chanletDAO.On("GetChanlet", mock.Anything, mock.Anything, mock.Anything).Return(&models.Chanlet{}, nil)
				mockBackendClients.chanletDAO.On("AddChanlet", mock.Anything, mock.Anything).Return(nil)
				mockBackendClients.chanletDAO.On("GetChanletCount", mock.Anything, mock.Anything).Return(maxChanletCount, nil)
				res, err := mockBackend.CreateChanlet(ctx, channelID)
				So(err, ShouldNotBeNil)
				So(res, ShouldBeNil)
			})
		})

		Convey("Test CreateOwnerChanlet", func() {
			Convey("with success", func() {
				mockBackendClients.chanletDAO.On("AddChanlet", mock.Anything, mock.Anything).Return(nil)
				mockBackendClients.chanletDAO.On("GetChanletCount", mock.Anything, mock.Anything).Return(int64(0), nil)

				Convey("with existing owner chanlet", func() {
					mockBackendClients.chanletDAO.On("GetChanlet", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(&models.Chanlet{}, nil)
					err := mockBackend.CreateOwnerChanlet(ctx, channelID)
					So(err, ShouldBeNil)
					mockBackendClients.chanletDAO.AssertNotCalled(t, "AddChanlet")
				})

				Convey("without existing owner chanlet", func() {
					mockBackendClients.chanletDAO.On("GetChanlet", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil, nil)
					err := mockBackend.CreateOwnerChanlet(ctx, channelID)
					So(err, ShouldBeNil)
					mockBackendClients.chanletDAO.AssertNumberOfCalls(t, "AddChanlet", 1)
				})
			})

			Convey("with DAO failure", func() {
				Convey("with owner chanlet GetChanletCount DAO failure", func() {
					mockBackendClients.chanletDAO.On("GetChanlet", mock.Anything, mock.Anything, mock.Anything).Return(&models.Chanlet{}, errors.New("test"))
					err := mockBackend.CreateOwnerChanlet(ctx, channelID)
					So(err, ShouldNotBeNil)
				})
			})
		})

		Convey("Test GetChanlets", func() {
			relatedChannelID := "789"
			chanletID2 := "159"
			relations := []*models.ChannelRelation{
				{
					ChannelID:        channelID,
					RelatedChannelID: relatedChannelID,
					Relation:         models.RelationBorrower,
				},
			}
			borrowedChanlet := &models.Chanlet{
				OwnerChannelID: relatedChannelID,
				ChanletID:      chanletID,
			}

			ownedChanlet := &models.Chanlet{
				OwnerChannelID: channelID,
				ChanletID:      chanletID2,
			}

			Convey("with borrowed chanlet", func() {
				expectedChanlets := []*models.Chanlet{ownedChanlet, borrowedChanlet}

				mockBackendClients.channelRelationDAO.On("GetChannelRelationsByChannelID", mock.Anything, channelID).Return(relations, nil)
				mockBackendClients.chanletDAO.On("GetChanlets", mock.Anything, relatedChannelID).Return([]*models.Chanlet{borrowedChanlet}, nil)
				mockBackendClients.chanletDAO.On("GetChanlets", mock.Anything, channelID).Return([]*models.Chanlet{ownedChanlet}, nil)
				res, err := mockBackend.GetChanlets(ctx, channelID, GetChanletsOptions{Sort: None})
				So(err, ShouldBeNil)
				So(res, ShouldResemble, expectedChanlets)
			})

			Convey("with owned chanlet", func() {
				expectedChanlets := []*models.Chanlet{ownedChanlet}

				mockBackendClients.channelRelationDAO.On("GetChannelRelationsByChannelID", mock.Anything, channelID).Return([]*models.ChannelRelation{}, nil)
				mockBackendClients.chanletDAO.On("GetChanlets", mock.Anything, channelID).Return(expectedChanlets, nil)
				res, err := mockBackend.GetChanlets(ctx, channelID, GetChanletsOptions{Sort: None})
				So(err, ShouldBeNil)
				So(res, ShouldResemble, expectedChanlets)
			})

			Convey("with no owned chanlets at all", func() {
				mockBackendClients.chanletDAO.On("GetChanlets", mock.Anything, channelID).Return([]*models.Chanlet{}, nil)
				res, err := mockBackend.GetChanlets(ctx, channelID, GetChanletsOptions{Sort: None})
				So(err, ShouldBeNil)
				So(res, ShouldBeEmpty)
				mockBackendClients.channelRelationDAO.AssertNotCalled(t, "GetChannelRelationsByChannelID")
			})

			Convey("with both owned chanlet and borrowed chanlet", func() {
				expectedChanlets := []*models.Chanlet{ownedChanlet, borrowedChanlet}

				mockBackendClients.channelRelationDAO.On("GetChannelRelationsByChannelID", mock.Anything, channelID).Return(relations, nil)
				mockBackendClients.chanletDAO.On("GetChanlets", mock.Anything, relatedChannelID).Return([]*models.Chanlet{borrowedChanlet}, nil)
				mockBackendClients.chanletDAO.On("GetChanlets", mock.Anything, channelID).Return([]*models.Chanlet{ownedChanlet}, nil)
				res, err := mockBackend.GetChanlets(ctx, channelID, GetChanletsOptions{Sort: None})
				So(err, ShouldBeNil)
				So(res, ShouldResemble, expectedChanlets)
			})

			Convey("with content attributes associated", func() {
				expectedContentAttributes := testdata.ContentAttributes()

				Convey("when the current channel owns the content attributes", func() {
					ownedChanlet.ContentAttributeIDs = testdata.ContentAttributeIDs()
					expectedChanlets := []*models.Chanlet{ownedChanlet}

					mockBackendClients.channelRelationDAO.On("GetChannelRelationsByChannelID", mock.Anything, channelID).Return([]*models.ChannelRelation{}, nil)
					mockBackendClients.chanletDAO.On("GetChanlets", mock.Anything, channelID).Return(expectedChanlets, nil)
					mockBackendClients.contentAttributeDAO.On("GetContentAttributesByIDs", mock.Anything, channelID, mock.Anything).Return(expectedContentAttributes, nil)
					res, err := mockBackend.GetChanlets(ctx, channelID, GetChanletsOptions{Sort: None})
					So(err, ShouldBeNil)
					So(res, ShouldHaveLength, 1)
					So(res[0].ContentAttributeIDs, ShouldResemble, ownedChanlet.ContentAttributeIDs)
					So(res[0].ContentAttributes, ShouldResemble, expectedContentAttributes)
				})

				Convey("when the current channel borrows the content attributes", func() {
					borrowedChanlet.ContentAttributeIDs = testdata.ContentAttributeIDs()

					mockBackendClients.channelRelationDAO.On("GetChannelRelationsByChannelID", mock.Anything, channelID).Return(relations, nil)
					mockBackendClients.chanletDAO.On("GetChanlets", mock.Anything, relatedChannelID).Return([]*models.Chanlet{borrowedChanlet}, nil)
					mockBackendClients.chanletDAO.On("GetChanlets", mock.Anything, channelID).Return([]*models.Chanlet{ownedChanlet}, nil)
					mockBackendClients.contentAttributeDAO.On("GetContentAttributesByIDs", mock.Anything, relatedChannelID, mock.Anything).Return(expectedContentAttributes, nil)

					res, err := mockBackend.GetChanlets(ctx, channelID, GetChanletsOptions{Sort: None})
					So(err, ShouldBeNil)
					So(res, ShouldHaveLength, 2)
					So(res[1].ContentAttributeIDs, ShouldResemble, borrowedChanlet.ContentAttributeIDs)
					So(res[1].ContentAttributes, ShouldResemble, expectedContentAttributes)
				})

				Convey("when the current channel both owns and borrows the content attributes", func() {
					ownedChanlet.ContentAttributeIDs = testdata.ContentAttributeIDs()
					expectedOwnedChanlets := []*models.Chanlet{ownedChanlet}
					borrowedChanlet.ContentAttributeIDs = testdata.ContentAttributeIDs()
					expectedBorrowedChanlets := []*models.Chanlet{borrowedChanlet}

					mockBackendClients.channelRelationDAO.On("GetChannelRelationsByChannelID", mock.Anything, channelID).Return(relations, nil)
					mockBackendClients.chanletDAO.On("GetChanlets", mock.Anything, relatedChannelID).Return(expectedBorrowedChanlets, nil)
					mockBackendClients.chanletDAO.On("GetChanlets", mock.Anything, channelID).Return(expectedOwnedChanlets, nil)
					mockBackendClients.contentAttributeDAO.On("GetContentAttributesByIDs", mock.Anything, relatedChannelID, mock.Anything).Return(expectedContentAttributes, nil)
					mockBackendClients.contentAttributeDAO.On("GetContentAttributesByIDs", mock.Anything, channelID, mock.Anything).Return(expectedContentAttributes, nil)

					res, err := mockBackend.GetChanlets(ctx, channelID, GetChanletsOptions{Sort: None})
					So(err, ShouldBeNil)
					So(res, ShouldHaveLength, 2)
					So(res[0].ContentAttributeIDs, ShouldResemble, borrowedChanlet.ContentAttributeIDs)
					So(res[0].ContentAttributes, ShouldResemble, expectedContentAttributes)
					So(res[1].ContentAttributeIDs, ShouldResemble, ownedChanlet.ContentAttributeIDs)
					So(res[1].ContentAttributes, ShouldResemble, expectedContentAttributes)
				})

				Convey("that are non-existent/deleted", func() {
					ownedChanlet.ContentAttributeIDs = testdata.ContentAttributeIDs()
					expectedChanlets := []*models.Chanlet{ownedChanlet}

					mockBackendClients.channelRelationDAO.On("GetChannelRelationsByChannelID", mock.Anything, channelID).Return([]*models.ChannelRelation{}, nil)
					mockBackendClients.chanletDAO.On("GetChanlets", mock.Anything, channelID).Return(expectedChanlets, nil)
					mockBackendClients.contentAttributeDAO.On("GetContentAttributesByIDs", mock.Anything, channelID, mock.Anything).Return(expectedContentAttributes[0:3], nil)

					res, err := mockBackend.GetChanlets(ctx, channelID, GetChanletsOptions{Sort: None})
					So(err, ShouldBeNil)
					So(res, ShouldHaveLength, 1)
					So(res[0].ContentAttributeIDs, ShouldHaveLength, 3)
					So(res[0].ContentAttributes, ShouldHaveLength, 3)
				})
			})

			Convey("with database error", func() {
				mockBackendClients.chanletDAO.On("GetChanlets", mock.Anything, channelID).Return(nil, errors.New("test"))
				res, err := mockBackend.GetChanlets(ctx, channelID, GetChanletsOptions{Sort: None})
				So(err, ShouldNotBeNil)
				So(res, ShouldBeNil)
			})

			Convey("with ordered by viewership and group by attributes", func() {
				Convey("with all live ", func() {
					expectedChanlets := []*models.Chanlet{borrowedChanlet, ownedChanlet}

					mockBackendClients.channelRelationDAO.On("GetChannelRelationsByChannelID", mock.Anything, channelID).Return(relations, nil)
					mockBackendClients.chanletDAO.On("GetChanlets", mock.Anything, relatedChannelID).Return([]*models.Chanlet{borrowedChanlet}, nil)
					mockBackendClients.chanletDAO.On("GetChanlets", mock.Anything, channelID).Return([]*models.Chanlet{ownedChanlet}, nil)
					mockBackendClients.clients.Liveline.On("GetStreamsByChannelIDs", mock.Anything, mock.Anything).Return(&liveline.StreamsResponse{
						Streams: []*liveline.StreamResponse{
							{ChannelId: chanletID, ViewcountData: &liveline.ViewcountData{Viewcount: 3}},
							{ChannelId: chanletID2, ViewcountData: &liveline.ViewcountData{Viewcount: 2}},
						},
					}, nil)

					res, err := mockBackend.GetChanlets(ctx, channelID, GetChanletsOptions{Sort: ViewerCount})
					So(err, ShouldBeNil)
					So(res, ShouldResemble, expectedChanlets)
				})

				Convey("with all offline ", func() {
					expectedChanlets := []*models.Chanlet{ownedChanlet, borrowedChanlet}

					mockBackendClients.channelRelationDAO.On("GetChannelRelationsByChannelID", mock.Anything, channelID).Return(relations, nil)
					mockBackendClients.chanletDAO.On("GetChanlets", mock.Anything, relatedChannelID).Return([]*models.Chanlet{borrowedChanlet}, nil)
					mockBackendClients.chanletDAO.On("GetChanlets", mock.Anything, channelID).Return([]*models.Chanlet{ownedChanlet}, nil)
					mockBackendClients.clients.Liveline.On("GetStreamsByChannelIDs", mock.Anything, mock.Anything).Return(&liveline.StreamsResponse{
						Streams: []*liveline.StreamResponse{},
					}, nil)

					res, err := mockBackend.GetChanlets(ctx, channelID, GetChanletsOptions{Sort: ViewerCount})
					So(err, ShouldBeNil)
					So(res, ShouldResemble, expectedChanlets)
				})

				Convey("with online and offline", func() {
					expectedChanlets := []*models.Chanlet{borrowedChanlet, ownedChanlet}

					mockBackendClients.channelRelationDAO.On("GetChannelRelationsByChannelID", mock.Anything, channelID).Return(relations, nil)
					mockBackendClients.chanletDAO.On("GetChanlets", mock.Anything, relatedChannelID).Return([]*models.Chanlet{borrowedChanlet}, nil)
					mockBackendClients.chanletDAO.On("GetChanlets", mock.Anything, channelID).Return([]*models.Chanlet{ownedChanlet}, nil)
					mockBackendClients.clients.Liveline.On("GetStreamsByChannelIDs", mock.Anything, mock.Anything).Return(&liveline.StreamsResponse{
						Streams: []*liveline.StreamResponse{
							{ChannelId: chanletID, ViewcountData: &liveline.ViewcountData{Viewcount: 2}},
						},
					}, nil)

					res, err := mockBackend.GetChanlets(ctx, channelID, GetChanletsOptions{Sort: ViewerCount})
					So(err, ShouldBeNil)
					So(res, ShouldResemble, expectedChanlets)
				})

				Convey("with liveline error", func() {
					expectedChanlets := []*models.Chanlet{ownedChanlet, borrowedChanlet}
					mockBackendClients.channelRelationDAO.On("GetChannelRelationsByChannelID", mock.Anything, channelID).Return(relations, nil)
					mockBackendClients.chanletDAO.On("GetChanlets", mock.Anything, relatedChannelID).Return([]*models.Chanlet{borrowedChanlet}, nil)
					mockBackendClients.chanletDAO.On("GetChanlets", mock.Anything, channelID).Return([]*models.Chanlet{ownedChanlet}, nil)
					mockBackendClients.clients.Liveline.On("GetStreamsByChannelIDs", mock.Anything, mock.Anything).Return(nil, errors.New("oops"))

					res, err := mockBackend.GetChanlets(ctx, channelID, GetChanletsOptions{Sort: ViewerCount})
					So(err, ShouldBeNil)
					So(res, ShouldResemble, expectedChanlets)
				})
			})

			Convey("test pinning of parent (owner/borrower) channel in chanlet list", func() {
				ownerChannelID := "21"
				borrowerChannelID := "22"
				relations := []*models.ChannelRelation{
					{
						ChannelID:        borrowerChannelID,
						RelatedChannelID: ownerChannelID,
						Relation:         models.RelationBorrower,
					},
				}

				// make a list of 15 chanlets and randomize their orders
				// Golang sorts differently depending on whether the size of the collection exceeds 12.
				chanletCount := 15
				randomizedChanlets := make([]*models.Chanlet, chanletCount)
				for i, id := range rand.Perm(chanletCount) {
					randomizedChanlets[i] = &models.Chanlet{OwnerChannelID: ownerChannelID, ChanletID: strconv.Itoa(id)}
				}

				// insert the channel's main chanlet at the end
				mainChanlet := &models.Chanlet{OwnerChannelID: ownerChannelID, ChanletID: ownerChannelID}
				disorderedChanlets := make([]*models.Chanlet, chanletCount)
				copy(disorderedChanlets, randomizedChanlets)
				disorderedChanlets = append(disorderedChanlets, mainChanlet)

				// expect the main chanlet to be the first
				expectedChanlets := append([]*models.Chanlet{mainChanlet}, randomizedChanlets...)

				Convey("with owner channel as channelID", func() {
					mockBackendClients.channelRelationDAO.On("GetChannelRelationsByChannelID", mock.Anything, ownerChannelID).Return(nil, nil)
					mockBackendClients.chanletDAO.On("GetChanlets", mock.Anything, ownerChannelID).Return(disorderedChanlets, nil)

					actualChanlets, err := mockBackend.GetChanlets(context.Background(), ownerChannelID, GetChanletsOptions{Sort: None})
					So(err, ShouldBeNil)

					So(actualChanlets, ShouldResemble, expectedChanlets)
					So(actualChanlets[0].OwnerChannelID, ShouldEqual, ownerChannelID)
					So(actualChanlets[0].ChanletID, ShouldEqual, ownerChannelID)
				})

				Convey("with borrower channel as channelID", func() {
					expectedChanlets[0].OwnerChannelID = borrowerChannelID
					expectedChanlets[0].ChanletID = borrowerChannelID

					mockBackendClients.channelRelationDAO.On("GetChannelRelationsByChannelID", mock.Anything, borrowerChannelID).Return(relations, nil)
					mockBackendClients.chanletDAO.On("GetChanlets", mock.Anything, ownerChannelID).Return(randomizedChanlets, nil)
					mockBackendClients.chanletDAO.On("GetChanlets", mock.Anything, borrowerChannelID).Return([]*models.Chanlet{mainChanlet}, nil)

					actualChanlets, err := mockBackend.GetChanlets(context.Background(), borrowerChannelID, GetChanletsOptions{Sort: None})
					So(err, ShouldBeNil)

					So(actualChanlets, ShouldResemble, expectedChanlets)
					So(actualChanlets[0].OwnerChannelID, ShouldEqual, borrowerChannelID)
					So(actualChanlets[0].ChanletID, ShouldEqual, borrowerChannelID)
				})
			})
		})

		Convey("Test GetChanletByID", func() {
			chanlet := &models.Chanlet{
				OwnerChannelID: channelID,
				ChanletID:      chanletID,
			}

			Convey("with database error", func() {
				mockBackendClients.chanletDAO.On("GetChanletByID", mock.Anything, chanletID).Return(nil, errors.New("test"))
				res, err := mockBackend.GetChanletByID(ctx, chanletID)
				So(err, ShouldNotBeNil)
				So(res, ShouldBeNil)
			})

			Convey("with no result", func() {
				mockBackendClients.chanletDAO.On("GetChanletByID", mock.Anything, chanletID).Return(nil, nil)
				res, err := mockBackend.GetChanletByID(ctx, chanletID)
				So(err, ShouldBeNil)
				So(res, ShouldBeNil)
			})

			Convey("with result from database", func() {
				mockBackendClients.chanletDAO.On("GetChanletByID", mock.Anything, chanletID).Return(chanlet, nil)
				res, err := mockBackend.GetChanletByID(ctx, chanletID)
				So(err, ShouldBeNil)
				So(res, ShouldNotBeNil)
			})
		})

		Convey("Test ListAllChanlets", func() {
			chanlets := []*models.Chanlet{{
				OwnerChannelID: channelID,
				ChanletID:      chanletID,
			}}

			Convey("with database error", func() {
				mockBackendClients.chanletDAO.On("ListAllChanlets", mock.Anything, "", mock.Anything).Return(nil, "", errors.New("test"))
				res, _, err := mockBackend.ListAllChanlets(ctx, "")
				So(err, ShouldNotBeNil)
				So(res, ShouldBeNil)
			})

			Convey("with result from database with no further result", func() {
				mockBackendClients.chanletDAO.On("ListAllChanlets", mock.Anything, "", mock.Anything).Return(chanlets, "", nil)
				res, cursor, err := mockBackend.ListAllChanlets(ctx, "")
				So(err, ShouldBeNil)
				So(res, ShouldNotBeNil)
				So(cursor, ShouldBeEmpty)
			})

			Convey("with result from database with further result", func() {
				mockBackendClients.chanletDAO.On("ListAllChanlets", mock.Anything, "", mock.Anything).Return(chanlets, "pagingKey", nil)
				res, cursor, err := mockBackend.ListAllChanlets(ctx, "")
				So(err, ShouldBeNil)
				So(res, ShouldNotBeNil)
				So(cursor, ShouldEqual, "pagingKey")
			})
		})

		Convey("Test UpdateChanletContentAttributes", func() {
			attributeIDs := testdata.ContentAttributeIDs()
			attributes := testdata.ContentAttributes()

			input := &UpdateChanletInput{
				OwnerChannelID:      channelID,
				ChanletID:           chanletID,
				ContentAttributeIDs: attributeIDs,
			}

			updatedChanlet := &models.Chanlet{
				OwnerChannelID:      channelID,
				ChanletID:           chanletID,
				ContentAttributeIDs: attributeIDs,
			}

			borrowerID1 := "55555"
			borrowerID2 := "66666"
			relations := []*models.ChannelRelation{
				{
					ChannelID:        borrowerID1,
					RelatedChannelID: channelID,
					Relation:         models.RelationBorrower,
				},
				{
					ChannelID:        borrowerID2,
					RelatedChannelID: channelID,
					Relation:         models.RelationBorrower,
				},
			}

			Convey("with empty owner channel ID", func() {
				input.OwnerChannelID = ""
				_, err := mockBackend.UpdateChanletContentAttributes(context.Background(), input)
				So(err, ShouldNotBeNil)
				mockBackendClients.chanletDAO.AssertNotCalled(t, "UpdateChanletContentAttributes")
			})

			Convey("with empty chanlet ID", func() {
				input.ChanletID = ""
				_, err := mockBackend.UpdateChanletContentAttributes(context.Background(), input)
				So(err, ShouldNotBeNil)
				mockBackendClients.chanletDAO.AssertNotCalled(t, "UpdateChanletContentAttributes")
			})

			Convey("with valid input", func() {
				mockBackendClients.chanletDAO.On("UpdateChanletContentAttributes", mock.Anything, channelID, chanletID, mock.Anything).Return(updatedChanlet, nil)
				mockBackendClients.contentAttributeDAO.On("GetContentAttributesByIDs", mock.Anything, channelID, mock.Anything).Return(testdata.ContentAttributes(), nil)
				mockBackendClients.channelRelationDAO.On("GetChannelRelationsByRelatedChannelID", mock.Anything, channelID).Return(relations, nil)
				mockBackendClients.clients.Pubsub.On("PublishChanletUpdate", mock.Anything, mock.Anything, updatedChanlet).Return(nil)

				Convey("with new content attributes", func() {
					result, err := mockBackend.UpdateChanletContentAttributes(context.Background(), input)

					So(err, ShouldBeNil)
					So(result, ShouldNotBeNil)
					So(result.Chanlet.ContentAttributeIDs, ShouldResemble, attributeIDs)
					So(result.Chanlet.ContentAttributes, ShouldResemble, attributes)
				})

				Convey("with nil content attributes", func() {
					input.ContentAttributeIDs = nil
					result, err := mockBackend.UpdateChanletContentAttributes(context.Background(), input)

					So(err, ShouldBeNil)
					So(result, ShouldNotBeNil)
					So(result.Chanlet, ShouldNotBeNil)
					mockBackendClients.contentAttributeDAO.AssertNotCalled(t, "UpdateChanletContentAttributes")
				})

				Convey("with duplicated content attributes", func() {
					input.ContentAttributeIDs = []string{"1", "1", "2", "3", "1", "2"}
					result, err := mockBackend.UpdateChanletContentAttributes(context.Background(), input)

					So(err, ShouldBeNil)
					So(result, ShouldNotBeNil)
					idsSaved := mockBackendClients.chanletDAO.Calls[0].Arguments[3].([]string)
					// Should be only saving "1", "2" and "3" as IDs to the DB
					So(idsSaved, ShouldHaveLength, 3)
				})
			})

			Convey("with dependency failures", func() {
				attributeIDs := testdata.ContentAttributeIDs()
				Convey("when chanlet DAO fails", func() {
					mockBackendClients.chanletDAO.On("UpdateChanletContentAttributes", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("uh oh"))
					_, err := mockBackend.UpdateChanletContentAttributes(context.Background(), input)
					So(err, ShouldNotBeNil)
				})

				Convey("when content attributes DAO fails", func() {
					updatedChanlet := &models.Chanlet{
						OwnerChannelID:      channelID,
						ChanletID:           chanletID,
						ContentAttributeIDs: attributeIDs,
					}
					mockBackendClients.chanletDAO.On("UpdateChanletContentAttributes", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(updatedChanlet, nil)
					mockBackendClients.contentAttributeDAO.On("GetContentAttributesByIDs", mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("nah"))
					_, err := mockBackend.UpdateChanletContentAttributes(context.Background(), input)
					So(err, ShouldNotBeNil)
				})

				Convey("when users services client fails", func() {
					mockBackendClients.chanletDAO.On("UpdateChanletContentAttributes", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(updatedChanlet, nil)
					mockBackendClients.contentAttributeDAO.On("GetContentAttributesByIDs", mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("nah"))
					mockBackendClients.channelRelationDAO.On("GetChannelRelationsByRelatedChannelID", mock.Anything, channelID).Return(relations, nil)
					_, err := mockBackend.UpdateChanletContentAttributes(context.Background(), input)
					So(err, ShouldNotBeNil)
				})

				Convey("when channel relation DAO fails", func() {
					mockBackendClients.chanletDAO.On("UpdateChanletContentAttributes", mock.Anything, channelID, chanletID, mock.Anything).Return(updatedChanlet, nil)
					mockBackendClients.contentAttributeDAO.On("GetContentAttributesByIDs", mock.Anything, channelID, mock.Anything).Return(testdata.ContentAttributes(), nil)
					mockBackendClients.channelRelationDAO.On("GetChannelRelationsByRelatedChannelID", mock.Anything, channelID).Return(nil, errors.New("nah"))
					mockBackendClients.clients.Pubsub.On("PublishChanletUpdate", mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("nah"))

					result, err := mockBackend.UpdateChanletContentAttributes(context.Background(), input)
					So(err, ShouldBeNil) // fail to find borrower channels should not fail the call
					So(result, ShouldNotBeNil)
				})

				Convey("when pubsub client fails", func() {
					mockBackendClients.chanletDAO.On("UpdateChanletContentAttributes", mock.Anything, channelID, chanletID, mock.Anything).Return(updatedChanlet, nil)
					mockBackendClients.contentAttributeDAO.On("GetContentAttributesByIDs", mock.Anything, channelID, mock.Anything).Return(testdata.ContentAttributes(), nil)
					mockBackendClients.channelRelationDAO.On("GetChannelRelationsByRelatedChannelID", mock.Anything, channelID).Return(relations, nil)
					mockBackendClients.clients.Pubsub.On("PublishChanletUpdate", mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("nah"))

					result, err := mockBackend.UpdateChanletContentAttributes(context.Background(), input)
					So(err, ShouldBeNil) // pubsub failure does not fail the call
					So(result, ShouldNotBeNil)
				})
			})
		})

		Convey("Test PublishChanletUpdate", func() {
			chanlet := &models.Chanlet{
				OwnerChannelID:      channelID,
				ChanletID:           chanletID,
				ContentAttributeIDs: []string{"1", "2", "3"},
			}
			borrowerID1 := "55555"
			borrowerID2 := "66666"
			relations := []*models.ChannelRelation{
				{
					ChannelID:        borrowerID1,
					RelatedChannelID: channelID,
					Relation:         models.RelationBorrower,
				},
				{
					ChannelID:        borrowerID2,
					RelatedChannelID: channelID,
					Relation:         models.RelationBorrower,
				},
			}

			Convey("with valid input", func() {
				mockBackendClients.channelRelationDAO.On("GetChannelRelationsByRelatedChannelID", mock.Anything, channelID).Return(relations, nil)
				mockBackendClients.clients.Pubsub.On("PublishChanletUpdate", mock.Anything, mock.Anything, mock.Anything).Return(nil)

				err := mockBackend.PublishChanletUpdate(context.Background(), chanlet)

				So(err, ShouldBeNil)
				mockBackendClients.clients.Pubsub.AssertNumberOfCalls(t, "PublishChanletUpdate", 1)
				channelIDs := mockBackendClients.clients.Pubsub.Calls[0].Arguments[1].([]string)
				// should publish to owner and all borrowers
				So(channelIDs, ShouldResemble, []string{channelID, borrowerID1, borrowerID2})
			})

			Convey("with updates to the main channel", func() {
				chanlet.ChanletID = channelID
				mockBackendClients.clients.Pubsub.On("PublishChanletUpdate", mock.Anything, mock.Anything, mock.Anything).Return(nil)

				err := mockBackend.PublishChanletUpdate(context.Background(), chanlet)

				So(err, ShouldBeNil)
				channelIDs := mockBackendClients.clients.Pubsub.Calls[0].Arguments[1].([]string)
				// should publish to owner only
				So(channelIDs, ShouldResemble, []string{channelID})
			})

			Convey("with dependency failures", func() {
				Convey("when channel relations DAO fails", func() {
					mockBackendClients.channelRelationDAO.On("GetChannelRelationsByRelatedChannelID", mock.Anything, channelID).Return(nil, errors.New("test"))
					mockBackendClients.clients.Pubsub.On("PublishChanletUpdate", mock.Anything, mock.Anything, mock.Anything).Return(nil)

					err := mockBackend.PublishChanletUpdate(context.Background(), chanlet)
					So(err, ShouldBeNil)
					mockBackendClients.clients.Pubsub.AssertNumberOfCalls(t, "PublishChanletUpdate", 1)
					channelIDs := mockBackendClients.clients.Pubsub.Calls[0].Arguments[1].([]string)
					So(channelIDs, ShouldResemble, []string{channelID})
				})

				Convey("when pubsub client fails", func() {
					mockBackendClients.channelRelationDAO.On("GetChannelRelationsByRelatedChannelID", mock.Anything, channelID).Return(relations, nil)
					mockBackendClients.clients.Pubsub.On("PublishChanletUpdate", mock.Anything, mock.Anything, mock.Anything).Return(errors.New("test"))
					err := mockBackend.PublishChanletUpdate(context.Background(), chanlet)
					So(err, ShouldNotBeNil)
				})
			})
		})

		Convey("Test GetChanletStreamKey", func() {
			streamKey := "abcdefgh"
			Convey("with success", func() {
				mockBackendClients.chanletDAO.On("GetChanlet", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(&models.Chanlet{}, nil)
				mockBackendClients.clients.ChannelProperties.On("GetStreamKey", mock.Anything, mock.Anything).Return(streamKey, nil)
				res, err := mockBackend.GetChanletStreamKey(context.Background(), channelID, chanletID)
				So(err, ShouldBeNil)
				So(res, ShouldEqual, streamKey)
			})

			Convey("with non-existent chanlet", func() {
				mockBackendClients.chanletDAO.On("GetChanlet", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil, nil)
				res, err := mockBackend.GetChanletStreamKey(context.Background(), channelID, chanletID)
				So(err, ShouldNotBeNil)
				So(err, ShouldEqual, ErrChanletNotFound)
				So(res, ShouldBeEmpty)
			})

			Convey("with chanlet that is a public channel", func() {
				Convey("when chanlet is the owner chanlet", func() {
					mockBackendClients.chanletDAO.On("GetChanlet", mock.Anything, mock.Anything, mock.Anything).Return(&models.Chanlet{
						OwnerChannelID:  channelID,
						ChanletID:       channelID,
						IsPublicChannel: true,
					}, nil)
					mockBackendClients.clients.ChannelProperties.On("GetStreamKey", mock.Anything, mock.Anything).Return(streamKey, nil)
					res, err := mockBackend.GetChanletStreamKey(context.Background(), channelID, channelID)
					So(err, ShouldBeNil)
					So(res, ShouldEqual, streamKey)
				})

				Convey("when chanlet is NOT the owner chanlet", func() {
					mockBackendClients.chanletDAO.On("GetChanlet", mock.Anything, mock.Anything, mock.Anything).Return(&models.Chanlet{
						OwnerChannelID:  channelID,
						ChanletID:       chanletID,
						IsPublicChannel: true,
					}, nil)
					res, err := mockBackend.GetChanletStreamKey(context.Background(), channelID, chanletID)
					So(err, ShouldBeNil)
					So(res, ShouldBeEmpty)
				})
			})

			Convey("with GetChanlet DAO failure", func() {
				mockBackendClients.chanletDAO.On("GetChanlet", mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("nope"))
				res, err := mockBackend.GetChanletStreamKey(context.Background(), channelID, chanletID)
				So(err, ShouldNotBeNil)
				So(res, ShouldBeEmpty)
			})

			Convey("with ChannelProperties client failure", func() {
				mockBackendClients.chanletDAO.On("GetChanlet", mock.Anything, mock.Anything, mock.Anything).Return(&models.Chanlet{}, nil)
				mockBackendClients.clients.ChannelProperties.On("GetStreamKey", mock.Anything, mock.Anything).Return("", errors.New("nope"))
				res, err := mockBackend.GetChanletStreamKey(context.Background(), channelID, chanletID)
				So(err, ShouldBeNil)
				So(res, ShouldBeEmpty)
			})
		})

		Convey("Test ArchiveChanlet", func() {
			Convey("with non-existent chanlet", func() {
				mockBackendClients.chanletDAO.On("GetChanletByID", mock.Anything, mock.Anything).Return(nil, nil)
				res, err := mockBackend.ArchiveChanlet(context.Background(), channelID)
				So(err, ShouldNotBeNil)
				So(err, ShouldEqual, ErrChanletNotFound)
				So(res, ShouldBeNil)
			})

			Convey("with archived chanlet", func() {
				mockBackendClients.chanletDAO.On("GetChanletByID", mock.Anything, mock.Anything).Return(&models.Chanlet{ArchivedDate: time.Now()}, nil)
				res, err := mockBackend.ArchiveChanlet(context.Background(), channelID)
				So(err, ShouldNotBeNil)
				So(err, ShouldEqual, ErrChanletAlreadyArchived)
				So(res, ShouldBeNil)
			})

			Convey("with dao failure", func() {
				mockBackendClients.chanletDAO.On("GetChanletByID", mock.Anything, mock.Anything).Return(&models.Chanlet{}, nil)
				mockBackendClients.chanletDAO.On("ArchiveChanlet", mock.Anything, mock.Anything, mock.Anything).Return(errors.New("not disclosed"))
				res, err := mockBackend.ArchiveChanlet(context.Background(), channelID)
				So(err, ShouldNotBeNil)
				So(res, ShouldBeNil)
			})

			Convey("with success", func() {
				mockBackendClients.chanletDAO.On("GetChanletByID", mock.Anything, mock.Anything).Return(&models.Chanlet{OwnerChannelID: channelID, ChanletID: chanletID}, nil)
				mockBackendClients.chanletDAO.On("ArchiveChanlet", mock.Anything, channelID, chanletID).Return(nil)
				res, err := mockBackend.ArchiveChanlet(context.Background(), channelID)
				So(err, ShouldBeNil)
				So(res, ShouldNotBeNil)
			})
		})
	})
}
