package backend

import (
	"context"
	"fmt"
	"strings"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/nioh/internal/models"
	"code.justin.tv/common/chitin"
	"github.com/pkg/errors"
	uuid "github.com/satori/go.uuid"
)

// ContentAttributesBulkParams encompass all parameters for bulk operations on ContentAttributes
type ContentAttributesBulkParams struct {
	ChannelID         string
	ContentAttributes []*models.ContentAttribute
}

// ContentAttributesBulkResults encompass results from bulk operations on ContentAttributes
type ContentAttributesBulkResults struct {
	Success []*models.ContentAttribute
	Fail    []*models.ContentAttribute
}

// CreateContentAttributes creates new content attributes
// The provided content attributes will be created with a unique contentAttributeID
func (b *backendImpl) CreateContentAttributes(ctx context.Context, params *ContentAttributesBulkParams) (*ContentAttributesBulkResults, error) {
	log := logrus.WithField("RequestID", chitin.GetTraceID(ctx))

	if params == nil {
		return nil, errors.New("params is required")
	}
	if params.ChannelID == "" {
		return nil, errors.New("params.ChannelID is required")
	}
	if len(params.ContentAttributes) == 0 {
		return &ContentAttributesBulkResults{}, nil
	}

	results := &ContentAttributesBulkResults{
		Success: []*models.ContentAttribute{},
		Fail:    []*models.ContentAttribute{},
	}

	var createInput []*models.ContentAttribute
	now := b.clock.Now()
	for _, attribute := range params.ContentAttributes {
		if err := b.validateNewContentAttribute(attribute); err != nil {
			log.WithFields(logrus.Fields{
				"failed_attribute": fmt.Sprintf("%+v", attribute),
			}).WithError(err).Info("validation failed for new content attribute")
			results.Fail = append(results.Fail, attribute)
			continue
		}

		// assign owner channel ID and unique content attribute ID
		attribute.OwnerChannelID = params.ChannelID
		attribute.ID = uuid.NewV4().String()
		// update timestamps
		attribute.CreatedAt = now
		attribute.UpdatedAt = now
		createInput = append(createInput, attribute)
	}

	// create new content attributes in DB
	numCreated, err := b.daos.ContentAttributeDAO.CreateContentAttributes(ctx, createInput)
	if err != nil {
		if numCreated > 0 {
			// TODO: try load all content attributes from createInput to see which ones succeeded.
			log.WithError(err).Errorf(
				"encountered an error while creating new content attributes, (%d) requested, (%d) written",
				len(createInput), numCreated)
		}

		return nil, errors.Wrapf(err, "failed to create new content attributes for owner channel %s", params.ChannelID)
	}

	results.Success = createInput
	return results, nil
}

func (b *backendImpl) validateNewContentAttribute(contentAttribute *models.ContentAttribute) error {
	if contentAttribute.ID != "" {
		// content attribute IDs are UUIDs generated here when they are created,
		return errors.New("new content attribute should not have an assigned ID")
	}
	if contentAttribute.OwnerChannelID != "" {
		// owner channel is assigned using the channel ID field, which is attached and verified in the API layer
		return errors.New("new content attribute should not have owner channel assigned")
	}
	if contentAttribute.AttributeKey == "" {
		return errors.New("content attribute key is required")
	}
	if contentAttribute.AttributeName == "" {
		return errors.New("content attribute name is required")
	}
	if contentAttribute.Value == "" {
		return errors.New("content attribute value is required")
	}
	if contentAttribute.ImageURL != "" && !strings.HasPrefix(contentAttribute.ImageURL, b.config.ContentAttributeImageURLPrefix) {
		return errors.New("provided image URL prefix is not allowed")
	}
	return nil
}

// DeleteContentAttributes deletes content attributes
func (b *backendImpl) DeleteContentAttributes(ctx context.Context, params *ContentAttributesBulkParams) (*ContentAttributesBulkResults, error) {
	if params == nil {
		return nil, errors.New("params is required")
	}
	if params.ChannelID == "" {
		return nil, errors.New("params.ChannelID is required")
	}
	if len(params.ContentAttributes) == 0 {
		return &ContentAttributesBulkResults{}, nil
	}

	log := logrus.WithFields(logrus.Fields{
		"RequestID": chitin.GetTraceID(ctx),
		"ChannelID": params.ChannelID,
	})

	results := &ContentAttributesBulkResults{
		Success: []*models.ContentAttribute{},
		Fail:    []*models.ContentAttribute{},
	}

	var ids []string
	var processed []*models.ContentAttribute
	for _, attribute := range params.ContentAttributes {
		if attribute.ID == "" {
			log.WithField("Attribute", attribute).Error("content attribute ID is required for deletion")
			results.Fail = append(results.Fail, attribute)
			continue
		}
		if attribute.OwnerChannelID != params.ChannelID {
			log.WithFields(logrus.Fields{
				"ChannelID":               params.ChannelID,
				"AttributeOwnerChannelID": attribute.OwnerChannelID,
				"Attribute":               attribute,
			}).Error("cannot delete an attribute that does not belong to the requester's owner channel")
			results.Fail = append(results.Fail, attribute)
			continue
		}

		ids = append(ids, attribute.ID)
		processed = append(processed, attribute)
	}

	numDeleted, err := b.daos.ContentAttributeDAO.DeleteContentAttributesByIDs(ctx, params.ChannelID, ids)
	if err != nil {
		if numDeleted > 0 {
			// TODO: try load all content attributes from createInput to see which ones still exist
			log.WithError(err).Errorf(
				"encountered an error while deleting content attributes, (%d) requested, (%d) written",
				len(ids), numDeleted)
		}

		return nil, errors.Wrapf(err, "failed to delete content attributes for owner channel %s", params.ChannelID)
	}

	results.Success = processed
	return results, nil
}

// GetContentAttributesForChannel retrieves content attributes for a given channel (does not include borrowed content attributes from channel relations)
func (b *backendImpl) GetContentAttributesForChannel(ctx context.Context, channelID string) ([]*models.ContentAttribute, error) {
	if channelID == "" {
		return nil, errors.New("channelID is required")
	}

	contentAttributes, err := b.daos.ContentAttributeDAO.GetContentAttributesForChannel(ctx, channelID)
	if err != nil {
		return nil, errors.Wrapf(
			err, "failed to retrieve content attributes. Owner channel ID (%s)",
			channelID)
	}

	return contentAttributes, nil
}

// UpdateContentAttributes updates existing content attributes
func (b *backendImpl) UpdateContentAttributes(ctx context.Context, params *ContentAttributesBulkParams) (*ContentAttributesBulkResults, error) {
	if params == nil {
		return nil, errors.New("params is required")
	}
	if params.ChannelID == "" {
		return nil, errors.New("params.ChannelID is required")
	}
	if len(params.ContentAttributes) == 0 {
		return &ContentAttributesBulkResults{}, nil
	}

	log := logrus.WithFields(logrus.Fields{
		"RequestID": chitin.GetTraceID(ctx),
		"ChannelID": params.ChannelID,
	})

	results := &ContentAttributesBulkResults{
		Success: []*models.ContentAttribute{},
		Fail:    []*models.ContentAttribute{},
	}

	var updateInput []*models.ContentAttribute
	now := b.clock.Now()
	for _, attribute := range params.ContentAttributes {
		if err := b.validateContentAttribute(attribute); err != nil {
			log.WithError(err).Errorf("validation failed for content attribute, skipping this one, %v+", attribute)
			results.Fail = append(results.Fail, attribute)
			continue
		}
		if attribute.OwnerChannelID != params.ChannelID {
			log.WithFields(logrus.Fields{
				"AttributeOwnerChannelID": attribute.OwnerChannelID,
				"Attribute":               attribute,
			}).Error("cannot update an attribute that does not belong to the requester's channel")
			results.Fail = append(results.Fail, attribute)
			continue
		}
		// update timestamps
		attribute.UpdatedAt = now
		updateInput = append(updateInput, attribute)
	}

	if len(updateInput) == 0 {
		return results, nil
	}

	// updates content attributes in DB
	numUpdated, err := b.daos.ContentAttributeDAO.UpdateContentAttributes(ctx, updateInput)
	if err != nil {
		if numUpdated > 0 {
			// TODO: try load all content attributes from updateInput to see which ones succeeded.
			log.WithError(err).Errorf(
				"encountered an error while updating content attributes, (%d) requested, (%d) written",
				len(updateInput), numUpdated)
		}

		return nil, errors.Wrapf(err, "failed to update content attributes for owner channel %s", params.ChannelID)
	}

	results.Success = updateInput
	return results, nil

}

func (b *backendImpl) validateContentAttribute(contentAttribute *models.ContentAttribute) error {
	if contentAttribute.ID == "" {
		return errors.New("content attribute ID is required")
	}
	if contentAttribute.OwnerChannelID == "" {
		return errors.New("owner channel ID is required")
	}
	if contentAttribute.AttributeKey == "" {
		return errors.New("content attribute key is required")
	}
	if contentAttribute.AttributeName == "" {
		return errors.New("content attribute name is required")
	}
	if contentAttribute.Value == "" {
		return errors.New("content attribute value is required")
	}
	if contentAttribute.ImageURL != "" && !strings.HasPrefix(contentAttribute.ImageURL, b.config.ContentAttributeImageURLPrefix) {
		return errors.New("provided content attribute image URL is not allowed")
	}
	return nil
}

// CreateContentAttributeImageUploadConfig requests for new image upload configurations from the upload service
func (b *backendImpl) CreateContentAttributeImageUploadConfig(ctx context.Context, channelID string) (*models.ContentAttributeImageUploadConfig, error) {
	if channelID == "" {
		return nil, errors.New("Channel ID is required")
	}

	uploadConfig, err := b.clients.UploadService.CreateContentAttributeImageUploadConfig(ctx, channelID)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get new image upload configurations for content attribute")
	}

	return uploadConfig, nil
}

// GetContentAttributesForChanlet requests for content attributes owned by passed channel and associated with the passed chanlet
func (b *backendImpl) GetContentAttributesForChanlet(ctx context.Context, chanletID string) ([]*models.ContentAttribute, error) {
	chanlet, err := b.GetChanletByID(ctx, chanletID)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get chanlet")
	}
	if chanlet == nil {
		return nil, ErrChanletNotFound
	}

	err = b.hydrateChanletsWithContentAttributes(ctx, chanlet.OwnerChannelID, []*models.Chanlet{chanlet})
	if err != nil {
		return nil, errors.Wrap(err, "failed to populate content attributes for chanlets")
	}

	return chanlet.ContentAttributes, nil
}
