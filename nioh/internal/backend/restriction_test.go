package backend

import (
	"context"
	"testing"
	"time"

	substwirp "code.justin.tv/revenue/subscriptions/twirp"
	"code.justin.tv/vod/vodapi/rpc/vodapi"

	"code.justin.tv/commerce/nioh/internal/models"
	"code.justin.tv/commerce/nioh/internal/testdata"
	"code.justin.tv/commerce/nioh/pkg/loaders"
	"code.justin.tv/common/twirp"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestRestrictionV2Backend(t *testing.T) {
	Convey("Test GetRestriction", t, func() {
		testChannelID := "146851449"
		testRestriction := testdata.Restriction(testChannelID)
		testRestrictableResource := models.Channel{
			ID: testChannelID,
		}

		backend, mockClients := initTestableBackend()
		ctx := loaders.New(backend.clients).Attach(context.Background())

		Convey("with no restriction", func() {
			mockClients.restrictionDAO.On("GetRestriction", mock.Anything, testRestriction.ResourceKey).
				Return(nil, nil)

			res, err := backend.GetRestriction(ctx, testRestrictableResource, false)
			So(res, ShouldBeNil)
			So(err, ShouldBeNil)
		})

		Convey("with restriction", func() {
			mockClients.restrictionDAO.On("GetRestriction", mock.Anything, testRestriction.ResourceKey).
				Return(testRestriction, nil)

			// Make copy of testRestriction
			expectedRestriction := testdata.Restriction(testChannelID)

			Convey("when exemptions are active", func() {
				mockClients.clock.On("Now").Return(time.Date(2019, time.March, 1, 0, 0, 0, 0, time.UTC))
			})

			Convey("when exemptions are inactive", func() {
				mockClients.clock.On("Now").Return(time.Date(1066, time.March, 1, 0, 0, 0, 0, time.UTC))
				expectedRestriction.Exemptions = nil
			})

			Convey("mixed active/inactive exemptions", func() {
				mockClients.clock.On("Now").Return(time.Date(2019, time.March, 1, 0, 0, 0, 0, time.UTC))
				testRestriction.Exemptions[0].ActiveEndDate = time.Time{}
				expectedRestriction.Exemptions = testRestriction.Exemptions[1:]
			})

			res, err := backend.GetRestriction(ctx, testRestrictableResource, false)
			So(res, ShouldResemble, expectedRestriction)
			So(err, ShouldBeNil)
		})

		Convey("vod with broadcast restriction", func() {
			// return a test vod with a broadcast id
			testVod := &vodapi.Vod{
				BroadcastId: "broadcastID",
				Id:          "vodID",
			}
			testRestriction := testdata.VODRestriction(testVod.Id)
			testRestrictableResource := models.VOD{
				ID: testVod.Id,
			}
			testProduct := &substwirp.Product{
				Id:          testdata.TestProductID,
				ShortName:   "testProductName",
				DisplayName: "A title",
			}

			expectedRestriction := testdata.VODRestriction(testVod.Id)

			mockClients.clock.On("Now").Return(time.Date(2019, time.March, 1, 0, 0, 0, 0, time.UTC))
			mockClients.clients.VodAPI.On("GetVodBroadcastID", mock.Anything, mock.Anything).Return(testVod.BroadcastId, nil)
			mockClients.restrictionDAO.On("GetRestriction", mock.Anything, mock.Anything).Return(testRestriction, nil)

			Convey("with product exemption key", func() {
				mockClients.clients.Subscriptions.On("GetProductsByIDs", ctx, []string{testProduct.Id}).Return(map[string]*substwirp.Product{testProduct.Id: testProduct}, nil)
				expectedRestriction.Exemptions[1].Actions = []*models.ExemptionAction{
					{
						Name:  testProduct.ShortName,
						Title: testProduct.DisplayName,
					},
				}
			})
			res, err := backend.GetRestriction(ctx, testRestrictableResource, true)
			So(res, ShouldResemble, expectedRestriction)
			So(err, ShouldBeNil)
		})

		Convey("vod with vod restriction", func() {
			testVODID := "146851450"
			testRestriction := testdata.VODRestriction(testVODID)
			testRestrictableResource := models.VOD{
				ID: testVODID,
			}
			testProduct := &substwirp.Product{
				Id:          testdata.TestProductID,
				ShortName:   "testProductName",
				DisplayName: "A title",
			}

			expectedRestriction := testdata.VODRestriction(testVODID)

			mockClients.clock.On("Now").Return(time.Date(2019, time.March, 1, 0, 0, 0, 0, time.UTC))
			mockClients.clients.VodAPI.On("GetVodBroadcastID", mock.Anything, mock.Anything).Return("", nil)
			mockClients.restrictionDAO.On("GetRestriction", mock.Anything, mock.Anything).Return(testRestriction, nil)

			Convey("with product exemption key", func() {
				mockClients.clients.Subscriptions.On("GetProductsByIDs", ctx, []string{testProduct.Id}).Return(map[string]*substwirp.Product{testProduct.Id: testProduct}, nil)
				expectedRestriction.Exemptions[1].Actions = []*models.ExemptionAction{
					{
						Name:  testProduct.ShortName,
						Title: testProduct.DisplayName,
					},
				}
			})
			res, err := backend.GetRestriction(ctx, testRestrictableResource, false)
			So(res, ShouldResemble, expectedRestriction)
			So(err, ShouldBeNil)
		})

		Convey("with DAO failure", func() {
			mockClients.restrictionDAO.On("GetRestriction", mock.Anything, testRestriction.ResourceKey).
				Return(nil, errors.New("your DAO asplode"))

			res, err := backend.GetRestriction(ctx, testRestrictableResource, false)
			So(res, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})
	})

	Convey("Test SetRestriction", t, func() {
		channelID := "146851449"
		testTime := time.Date(2018, time.October, 1, 0, 0, 0, 0, time.UTC)
		testRestrictableResource := models.Channel{
			ID: channelID,
		}

		backend, mockClients := initTestableBackend()
		mockClients.clock.On("Now").Return(testTime)
		asyncAutotagCalled := make(chan struct{})
		asyncAutotagWait := func() {
			select {
			case <-asyncAutotagCalled:
			case <-time.After(5 * time.Millisecond):
			}
		}
		mockClients.autotagger.On("UpdateAutotag", mock.Anything, mock.Anything, mock.Anything).
			Run(func(mock.Arguments) {
				close(asyncAutotagCalled)
			}).
			Return(nil)

		Convey("with no restriction", func() {
			Convey("returns error", func() {
				res, err := backend.SetRestriction(context.Background(), testRestrictableResource, models.RestrictionV2{RestrictionType: models.UnknownRestrictionType})
				So(res, ShouldBeNil)
				So(err, ShouldNotBeNil)

				Convey("and does not update autotag", func() {
					asyncAutotagWait()
					mockClients.autotagger.AssertNotCalled(t, "UpdateAutotag")
				})
			})
		})

		Convey("with restriction", func() {
			ctx := context.Background()

			Convey("assigns product exemption key for sub-only-live", func() {
				products := []*substwirp.Product{{Id: testdata.TestProductID, Tier: "2000"}}
				mockClients.clients.Subscriptions.On("GetChannelProducts", mock.Anything, channelID).Return(products, nil)
				mockClients.restrictionDAO.On("SetRestriction", mock.Anything, mock.Anything).Return(nil)
				mockClients.chanletDAO.On("GetChanletByID", mock.Anything, mock.Anything).Return(nil, nil)

				var options []models.RestrictionOption

				expectedRestriction := &models.RestrictionV2{
					ResourceKey:     "channel:" + channelID,
					RestrictionType: models.SubOnlyLiveRestrictionType,
					Exemptions: []*models.Exemption{
						backend.newActiveExemption(models.StaffExemptionType, []string{}),
						backend.newActiveExemption(models.SiteAdminExemptionType, []string{}),
						backend.newActiveExemption(models.ProductExemptionType, []string{testdata.TestProductID}),
						backend.newActiveExemption(models.PreviewExemptionType, []string{}),
					},
				}

				Convey("and supports options", func() {
					Convey("allow channel moderator option", func() {
						options = append(options, models.AllowChannelModeratorOption)
						expectedRestriction.Exemptions = append(expectedRestriction.Exemptions,
							backend.newActiveExemption(models.ChannelModeratorExemptionType, []string{channelID}))
					})
					Convey("allow channel VIP option", func() {
						options = append(options, models.AllowChannelVIPOption)
						expectedRestriction.Exemptions = append(expectedRestriction.Exemptions,
							backend.newActiveExemption(models.ChannelVIPExemptionType, []string{channelID}))
					})
					Convey("combination of moderator/VIP options", func() {
						options = append(options, models.AllowChannelModeratorOption, models.AllowChannelVIPOption)
						expectedRestriction.Exemptions = append(expectedRestriction.Exemptions,
							backend.newActiveExemption(models.ChannelVIPExemptionType, []string{channelID}),
							backend.newActiveExemption(models.ChannelModeratorExemptionType, []string{channelID}))
					})
					Convey("allow minimum tier 3 option", func() {
						options = append(options, models.AllowTier3Only)
						// product returned from the mock should be filtered out since it's tier 2
						expectedRestriction.Exemptions[2] = backend.newActiveExemption(models.ProductExemptionType, []string{})
					})
					Convey("allow minimum tier 2 option", func() {
						options = append(options, models.AllowTier2And3Only)
					})

					expectedRestriction.Options = options
				})

				res, err := backend.SetRestriction(ctx, testRestrictableResource, models.RestrictionV2{RestrictionType: models.SubOnlyLiveRestrictionType, Options: options})
				So(res, ShouldResemble, expectedRestriction)
				So(err, ShouldBeNil)

				Convey("and updates autotag", func() {
					asyncAutotagWait()
					mockClients.autotagger.AssertCalled(t, "UpdateAutotag", context.Background(), models.RestrictionUpdateEvent{
						Resource:    testRestrictableResource,
						Restriction: expectedRestriction,
					})
				})
			})

			Convey("correctly handles geoblock broadcast restrictions", func() {
				broadcastID := "broadcastID"
				countryCodes := []string{"DE"}
				resource := models.Broadcast{ID: broadcastID}
				restriction := models.RestrictionV2{
					ResourceKey:     "broadcast:" + broadcastID,
					RestrictionType: models.GeoblockRestrictionType,
					Geoblocks: []models.Geoblock{
						{Operator: models.BlockOperator, CountryCodes: countryCodes},
					},
				}

				expected := &models.RestrictionV2{
					ResourceKey:     "broadcast:" + broadcastID,
					RestrictionType: models.GeoblockRestrictionType,
					Exemptions: []*models.Exemption{
						backend.newActiveExemption(models.NotInCountriesExemptionType, countryCodes),
					},
					Geoblocks: []models.Geoblock{
						{Operator: models.BlockOperator, CountryCodes: countryCodes},
					},
				}

				mockClients.restrictionDAO.On("SetRestriction", mock.Anything, mock.Anything).Return(nil)

				resp, err := backend.SetRestriction(ctx, resource, restriction)
				So(err, ShouldBeNil)
				So(resp, ShouldResemble, expected)
			})

			Convey("correctly handles setting a sub-only restriction on a chanlet by using parent's product IDs", func() {
				ownerChannelID := "408653032"

				products := []*substwirp.Product{{Id: testdata.TestProductID, Tier: "2000"}}
				mockClients.clients.Subscriptions.On("GetChannelProducts", mock.Anything, ownerChannelID).Return(products, nil)
				mockClients.restrictionDAO.On("SetRestriction", mock.Anything, mock.Anything).Return(nil)
				mockClients.chanletDAO.On("GetChanletByID", mock.Anything, channelID).Return(&models.Chanlet{
					OwnerChannelID: ownerChannelID,
					ChanletID:      channelID,
				}, nil)

				expectedRestriction := &models.RestrictionV2{
					ResourceKey:     "channel:" + channelID,
					RestrictionType: models.SubOnlyLiveRestrictionType,
					Exemptions: []*models.Exemption{
						backend.newActiveExemption(models.StaffExemptionType, []string{}),
						backend.newActiveExemption(models.SiteAdminExemptionType, []string{}),
						backend.newActiveExemption(models.ProductExemptionType, []string{testdata.TestProductID}),
						backend.newActiveExemption(models.PreviewExemptionType, []string{}),
						backend.newActiveExemption(models.ChannelVIPExemptionType, []string{ownerChannelID}),
						backend.newActiveExemption(models.ChannelModeratorExemptionType, []string{ownerChannelID}),
					},
					Options: []models.RestrictionOption{
						models.AllowChannelModeratorOption,
						models.AllowChannelVIPOption,
					},
				}

				restriction := models.RestrictionV2{
					RestrictionType: models.SubOnlyLiveRestrictionType,
					Options:         []models.RestrictionOption{models.AllowChannelModeratorOption, models.AllowChannelVIPOption},
				}

				res, err := backend.SetRestriction(ctx, testRestrictableResource, restriction)
				So(err, ShouldBeNil)
				So(res, ShouldResemble, expectedRestriction)
			})

			Convey("assigns the all-access product exemption key", func() {
				products := []*substwirp.Product{{Id: testdata.TestProductID}}
				mockClients.clients.Subscriptions.On("GetChannelProducts", mock.Anything, models.OWLAllAccessPassChannelID).Return(products, nil)
				mockClients.restrictionDAO.On("SetRestriction", mock.Anything, mock.Anything).Return(nil)
				mockClients.chanletDAO.On("GetChanletByID", mock.Anything, mock.Anything).Return(nil, nil)

				expectedRestriction := &models.RestrictionV2{
					ResourceKey:     "channel:" + channelID,
					RestrictionType: models.AllAccessPassRestrictionType,
					Exemptions: []*models.Exemption{
						backend.newActiveExemption(models.ProductExemptionType, []string{testdata.TestProductID}),
					},
				}

				res, err := backend.SetRestriction(ctx, testRestrictableResource, models.RestrictionV2{RestrictionType: models.AllAccessPassRestrictionType})
				So(res, ShouldResemble, expectedRestriction)
				So(err, ShouldBeNil)

				Convey("and does not update autotag", func() {
					asyncAutotagWait()
					mockClients.autotagger.AssertNotCalled(t, "UpdateAutotag")
				})
			})

			Convey("with DAO failure", func() {
				products := []*substwirp.Product{{Id: testdata.TestProductID}}
				mockClients.clients.Subscriptions.On("GetChannelProducts", mock.Anything, mock.Anything).Return(products, nil)
				mockClients.restrictionDAO.On("SetRestriction", mock.Anything, mock.Anything).
					Return(errors.New("your DAO asplode"))
				mockClients.chanletDAO.On("GetChanletByID", mock.Anything, mock.Anything).Return(nil, nil)

				res, err := backend.SetRestriction(ctx, testRestrictableResource, models.RestrictionV2{RestrictionType: models.SubOnlyLiveRestrictionType})
				So(res, ShouldBeNil)
				So(err, ShouldNotBeNil)

				Convey("and does not update autotag", func() {
					asyncAutotagWait()
					mockClients.autotagger.AssertNotCalled(t, "UpdateAutotag")
				})
			})

			Convey("with subscriptions failure", func() {
				mockClients.clients.Subscriptions.On("GetChannelProducts", mock.Anything, mock.Anything).Return(nil, errors.New("bad products"))
				mockClients.restrictionDAO.On("SetRestriction", mock.Anything, mock.Anything).Return(nil)
				mockClients.chanletDAO.On("GetChanletByID", mock.Anything, mock.Anything).Return(nil, nil)

				res, err := backend.SetRestriction(ctx, testRestrictableResource, models.RestrictionV2{RestrictionType: models.SubOnlyLiveRestrictionType})
				So(res, ShouldBeNil)
				So(err, ShouldNotBeNil)

				Convey("and does not update autotag", func() {
					asyncAutotagWait()
					mockClients.autotagger.AssertNotCalled(t, "UpdateAutotag")
				})
			})

			Convey("with empty products list", func() {
				mockClients.clients.Subscriptions.On("GetChannelProducts", mock.Anything, mock.Anything).Return(nil, nil)
				mockClients.restrictionDAO.On("SetRestriction", mock.Anything, mock.Anything).Return(nil)
				mockClients.chanletDAO.On("GetChanletByID", mock.Anything, mock.Anything).Return(nil, nil)

				res, err := backend.SetRestriction(ctx, testRestrictableResource, models.RestrictionV2{RestrictionType: models.SubOnlyLiveRestrictionType})
				So(res, ShouldBeNil)
				So(err, ShouldNotBeNil)

				Convey("and does not update autotag", func() {
					asyncAutotagWait()
					mockClients.autotagger.AssertNotCalled(t, "UpdateAutotag")
				})
			})

			Convey("when type is ORGANIZATION_ACCESS_ONLY", func() {
				testChannel := models.Channel{
					ID: "test_channel_id",
				}
				testCompanyID := "test_company_id"
				mockClients.restrictionDAO.On("SetRestriction", mock.Anything, mock.Anything).Return(nil)

				Convey("succeed", func() {
					mockClients.clients.Rbac.On("ListCompanyIDsForUser", mock.Anything, testChannel.ID).Return([]string{testCompanyID}, nil)
					res, err := backend.SetRestriction(ctx, testChannel, models.RestrictionV2{RestrictionType: models.OrganizationAccessOnlyType})
					expectedRestriction := &models.RestrictionV2{
						ResourceKey:     "channel:" + testChannel.ID,
						RestrictionType: models.OrganizationAccessOnlyType,
						Exemptions: []*models.Exemption{
							backend.newActiveExemption(models.OrganizationMemberExemptionType, []string{testCompanyID}),
						},
					}
					asyncAutotagWait()
					So(res, ShouldNotBeNil)
					So(err, ShouldBeNil)
					So(res.ResourceKey, ShouldEqual, expectedRestriction.ResourceKey)
					So(res.RestrictionType, ShouldEqual, expectedRestriction.RestrictionType)
					So(len(res.Exemptions), ShouldEqual, 1)
					So(res.Exemptions[0], ShouldResemble, expectedRestriction.Exemptions[0])
				})

				Convey("RBAC returns error", func() {
					mockClients.clients.Rbac.On("ListCompanyIDsForUser", mock.Anything, mock.Anything).Return(nil, twirp.NewError(twirp.Internal, "unknown error."))
					res, err := backend.SetRestriction(ctx, testChannel, models.RestrictionV2{RestrictionType: models.OrganizationAccessOnlyType})
					asyncAutotagWait()
					So(res, ShouldBeNil)
					So(err, ShouldNotBeNil)
					So(err.Error(), ShouldEqual, "twirp error internal: unknown error.")
				})
			})
		})
	})

	Convey("Test DeleteRestriction", t, func() {
		channelID := "146851449"
		testTime := time.Date(2018, time.October, 1, 0, 0, 0, 0, time.UTC)
		testRestrictableResource := models.Channel{
			ID: channelID,
		}

		backend, mockClients := initTestableBackend()
		mockClients.clock.On("Now").Return(testTime)
		asyncAutotagCalled := make(chan struct{})
		asyncAutotagWait := func() {
			select {
			case <-asyncAutotagCalled:
			case <-time.After(5 * time.Millisecond):
			}
		}
		mockClients.autotagger.On("UpdateAutotag", mock.Anything, mock.Anything, mock.Anything).
			Run(func(mock.Arguments) {
				close(asyncAutotagCalled)
			}).
			Return(nil)

		ctx := context.Background()

		Convey("returns deleted restriction", func() {
			restriction := testdata.Restriction(channelID)
			mockClients.restrictionDAO.On("GetRestriction", mock.Anything, mock.Anything, mock.Anything).Return(restriction, nil)
			mockClients.restrictionDAO.On("DeleteRestriction", mock.Anything, mock.Anything).Return(nil)

			res, err := backend.DeleteRestriction(ctx, testRestrictableResource)
			So(res, ShouldResemble, restriction)
			So(err, ShouldBeNil)

			Convey("and updates autotag", func() {
				asyncAutotagWait()
				mockClients.autotagger.AssertCalled(t, "UpdateAutotag", context.Background(), models.RestrictionUpdateEvent{
					Resource:    testRestrictableResource,
					Restriction: nil,
				})
			})
		})

		Convey("returns nil without a restriction", func() {
			mockClients.restrictionDAO.On("GetRestriction", mock.Anything, mock.Anything, mock.Anything).Return(nil, nil)
			mockClients.restrictionDAO.On("DeleteRestriction", mock.Anything, mock.Anything).Return(nil)

			res, err := backend.DeleteRestriction(ctx, testRestrictableResource)
			So(res, ShouldBeNil)
			So(err, ShouldBeNil)

			Convey("and does not update autotag", func() {
				asyncAutotagWait()
				mockClients.autotagger.AssertNotCalled(t, "UpdateAutotag")
			})
		})

		Convey("with Get DAO failure", func() {
			mockClients.restrictionDAO.On("GetRestriction", mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("your DAO asplode"))

			res, err := backend.DeleteRestriction(ctx, testRestrictableResource)
			So(res, ShouldBeNil)
			So(err, ShouldNotBeNil)
			Convey("and does not update autotag", func() {
				asyncAutotagWait()
				mockClients.autotagger.AssertNotCalled(t, "UpdateAutotag")
			})
		})

		Convey("with Delete DAO failure", func() {
			mockClients.restrictionDAO.On("GetRestriction", mock.Anything, mock.Anything, mock.Anything).Return(testdata.Restriction(channelID), nil)
			mockClients.restrictionDAO.On("DeleteRestriction", mock.Anything, mock.Anything).Return(errors.New("your DAO asplode"))

			res, err := backend.DeleteRestriction(ctx, testRestrictableResource)
			So(res, ShouldBeNil)
			So(err, ShouldNotBeNil)
			Convey("and does not update autotag", func() {
				asyncAutotagWait()
				mockClients.autotagger.AssertNotCalled(t, "UpdateAutotag")
			})
		})
	})
}
