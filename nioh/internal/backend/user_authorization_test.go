package backend

import (
	"context"
	"testing"
	"time"

	"github.com/pkg/errors"

	"github.com/stretchr/testify/mock"

	voyager "code.justin.tv/amzn/TwitchVoyagerTwirp"
	"code.justin.tv/commerce/nioh/internal/testdata"
	"code.justin.tv/commerce/nioh/pkg/exemptions"
	"code.justin.tv/discovery/liveline/proto/liveline"
	substwirp "code.justin.tv/revenue/subscriptions/twirp"
	"code.justin.tv/vod/vodapi/rpc/vodapi"

	subs_mocks "code.justin.tv/commerce/nioh/internal/clients/subs/mocks"
	voyager_mocks "code.justin.tv/commerce/nioh/internal/clients/voyager/mocks"
	"code.justin.tv/commerce/nioh/internal/models"

	. "github.com/smartystreets/goconvey/convey"
)

func TestUserAuthorization(t *testing.T) {
	Convey("GetUserVODAuthorization", t, func() {
		backend, backendMocks := initTestableBackend()
		testRequest := &models.VODAuthorizationRequest{
			UserID:    "test-user-id",
			VideoID:   "test-video-id",
			ChannelID: "test-channel-id",
			Reason: &models.VODRestrictionReason{
				ProductName:     "walrusfest2019",
				RestrictionType: "sub_only",
				Title:           "Walrusfest 2019 VIP Pass",
			},
		}

		// No hedwig data is required for the new path
		newTestRequest := &models.VODAuthorizationRequest{
			UserID:  "test-user-id",
			VideoID: "test-video-id",
		}

		// Copy testRequest, overwrite a single field
		anonymousRequest := new(models.VODAuthorizationRequest)
		*anonymousRequest = *testRequest
		anonymousRequest.UserID = ""

		testVod := &vodapi.Vod{
			BroadcastId: "test-broadcast-id",
			Id:          "test-video-id",
		}
		testProduct := &substwirp.Product{
			Id: "walrusfest2019-product-id",
		}
		testSubscription := &voyager.Subscription{
			Id:        "walrusfest2019-subscription-id",
			UserId:    testRequest.UserID,
			ProductId: testProduct.Id,
		}
		testRestriction := testdata.VODRestriction(newTestRequest.VideoID)

		backendMocks.clock.On("Now").Return(time.Date(2019, time.June, 1, 0, 0, 0, 0, time.UTC))
		backendMocks.clients.Spade.On("TrackEvents", mock.Anything, mock.Anything, mock.Anything).Return(nil)

		testCases := []struct {
			description       string
			configureMockSubs func(*voyager_mocks.Client, *subs_mocks.Client)
			request           *models.VODAuthorizationRequest
			vod               *vodapi.Vod
			want              *models.VODAuthorization
			wantErr           bool
			restriction       *models.RestrictionV2
			hasExemptions     bool
		}{
			{
				description: "when user is authorized with hedwig data",
				configureMockSubs: func(c *voyager_mocks.Client, subsC *subs_mocks.Client) {
					subsC.On("GetProductByName", mock.Anything, testRequest.Reason.ProductName).
						Return(testProduct, nil)
					c.On("GetUserProductSubscription", mock.Anything, testRequest.UserID, testProduct.Id).
						Return(testSubscription, nil)
				},
				request: testRequest,
				vod:     testVod,
				want: &models.VODAuthorization{
					Request: testRequest,
					ResourceAuthorization: models.ResourceAuthorization{
						UserIsAuthorized:     true,
						ResourceIsRestricted: true,
					},
				},
				wantErr: false,
			},
			{
				description: "when user is unauthorized with hedwig data",
				configureMockSubs: func(c *voyager_mocks.Client, subsC *subs_mocks.Client) {
					subsC.On("GetProductByName", mock.Anything, testRequest.Reason.ProductName).
						Return(testProduct, nil)
					c.On("GetUserProductSubscription", mock.Anything, testRequest.UserID, testProduct.Id).
						Return(nil, nil)
				},
				request: testRequest,
				vod:     testVod,
				want: &models.VODAuthorization{
					Request: testRequest,
					ResourceAuthorization: models.ResourceAuthorization{
						UserIsAuthorized:     false,
						ResourceIsRestricted: true,
					},
				},
				wantErr: false,
			},
			{
				description:       "when user is authorized with restriction",
				configureMockSubs: func(c *voyager_mocks.Client, subsC *subs_mocks.Client) {},
				request:           newTestRequest,
				vod:               testVod,
				want: &models.VODAuthorization{
					Request: newTestRequest,
					ResourceAuthorization: models.ResourceAuthorization{
						UserIsAuthorized:     true,
						ResourceIsRestricted: true,
						RestrictionType:      testRestriction.RestrictionType,
						AppliedExemptions:    testdata.ApplyExemptions(testRestriction.Exemptions),
					},
				},
				wantErr:       false,
				restriction:   testRestriction,
				hasExemptions: true,
			},
			{
				description:       "when user is unauthorized with restriction",
				configureMockSubs: func(c *voyager_mocks.Client, subsC *subs_mocks.Client) {},
				request:           newTestRequest,
				vod:               testVod,
				want: &models.VODAuthorization{
					Request: newTestRequest,
					ResourceAuthorization: models.ResourceAuthorization{
						UserIsAuthorized:     false,
						ResourceIsRestricted: true,
						RestrictionType:      testRestriction.RestrictionType,
					},
				},
				restriction: testRestriction,
				wantErr:     false,
			},
			{
				description:       "when user is authorized without restriction",
				configureMockSubs: func(c *voyager_mocks.Client, subsC *subs_mocks.Client) {},
				request:           newTestRequest,
				vod:               testVod,
				want: &models.VODAuthorization{
					Request: newTestRequest,
					ResourceAuthorization: models.ResourceAuthorization{
						UserIsAuthorized:     true,
						ResourceIsRestricted: false,
					},
				},
				wantErr: false,
			},
			{
				description:       "when user is anonymous",
				configureMockSubs: func(c *voyager_mocks.Client, subsC *subs_mocks.Client) {},
				request:           anonymousRequest,
				vod:               testVod,
				want: &models.VODAuthorization{
					Request: anonymousRequest,
					ResourceAuthorization: models.ResourceAuthorization{
						UserIsAuthorized:     false,
						ResourceIsRestricted: true,
					},
				},
			},
			{
				description: "when no product is found with shortname",
				configureMockSubs: func(c *voyager_mocks.Client, subsC *subs_mocks.Client) {
					subsC.On("GetProductByName", mock.Anything, testRequest.Reason.ProductName).
						Return(nil, nil)
				},
				request: testRequest,
				vod:     testVod,
				want: &models.VODAuthorization{
					Request: testRequest,
					ResourceAuthorization: models.ResourceAuthorization{
						UserIsAuthorized:     false,
						ResourceIsRestricted: true,
					},
				},
				wantErr: true,
			},
			{
				description: "with an error calling Subscriptions.GetProductByName",
				configureMockSubs: func(c *voyager_mocks.Client, subsC *subs_mocks.Client) {
					subsC.On("GetProductByName", mock.Anything, testRequest.Reason.ProductName).
						Return(nil, errors.New("asplode"))
				},
				request: testRequest,
				vod:     testVod,
				want: &models.VODAuthorization{
					Request: testRequest,
					ResourceAuthorization: models.ResourceAuthorization{
						UserIsAuthorized:     false,
						ResourceIsRestricted: true,
					},
				},
				wantErr: true,
			},
			{
				description: "with an error calling Subscriptions.GetUserProductSubscription",
				configureMockSubs: func(c *voyager_mocks.Client, subsC *subs_mocks.Client) {
					subsC.On("GetProductByName", mock.Anything, testRequest.Reason.ProductName).
						Return(testProduct, nil)
					c.On("GetUserProductSubscription", mock.Anything, testRequest.UserID, testProduct.Id).
						Return(nil, errors.New("uh oh"))
				},
				request: testRequest,
				vod:     testVod,
				want: &models.VODAuthorization{
					Request: testRequest,
					ResourceAuthorization: models.ResourceAuthorization{
						UserIsAuthorized:     false,
						ResourceIsRestricted: true,
					},
				},
				wantErr: true,
			},
		}

		for _, tt := range testCases {
			Convey(tt.description, func() {
				tt.configureMockSubs(backendMocks.clients.TwitchVoyager, backendMocks.clients.Subscriptions)
				backendMocks.restrictionDAO.On("GetRestriction", mock.Anything, mock.Anything).Return(tt.restriction, nil)
				backendMocks.clients.VodAPI.On("GetVodBroadcastID", mock.Anything, mock.Anything).Return(tt.vod.BroadcastId, nil)
				var appliedExemptions []*models.AppliedExemption
				if tt.restriction != nil && tt.hasExemptions {
					appliedExemptions = testdata.ApplyExemptions(tt.restriction.Exemptions)
				}
				backendMocks.exemptionChecker.On("GetUserExemptionStatus", mock.Anything, tt.request.UserID, mock.Anything).
					Return(exemptions.UserExemptionStatus{
						UserID:               tt.request.UserID,
						ApplicableExemptions: appliedExemptions,
					}, nil)

				got, err := backend.GetUserVODAuthorization(context.Background(), tt.request)
				So(got, ShouldResemble, tt.want)
				So(err != nil, ShouldEqual, tt.wantErr)
			})
		}
	})

	Convey("Test GetAuthorizedUsersForChannelV2", t, func() {
		backend, backendMocks := initTestableBackend()
		testChannel := "146851449"
		testRestriction := testdata.RestrictionWithModAndVIP(testChannel) // Exemptions: staff / product

		backendMocks.clock.On("Now").Return(time.Date(2019, time.June, 1, 0, 0, 0, 0, time.UTC))
		backendMocks.clients.Subscriptions.On("GetProductByID", mock.Anything, mock.Anything).Return(nil, nil)

		firstCursor := "eyJNT0QiOnsiQ3Vyc29yIjoiIiwiSXNDb21wbGV0ZSI6ZmFsc2V9LCJTVUIiOnsiQ3Vyc29yIjoiIiwiSXNDb21wbGV0ZSI6ZmFsc2V9LCJWSVAiOnsiQ3Vyc29yIjoiIiwiSXNDb21wbGV0ZSI6dHJ1ZX19"
		secondCursor := "eyJNT0QiOnsiQ3Vyc29yIjoiIiwiSXNDb21wbGV0ZSI6dHJ1ZX0sIlNVQiI6eyJDdXJzb3IiOiIiLCJJc0NvbXBsZXRlIjpmYWxzZX0sIlZJUCI6eyJDdXJzb3IiOiIiLCJJc0NvbXBsZXRlIjp0cnVlfX0="

		Convey("with DAO error", func() {
			backendMocks.restrictionDAO.On("GetRestriction", mock.Anything, testRestriction.ResourceKey).Return(nil, errors.New("error"))
			ids, restricted, nextCursor, err := backend.GetAuthorizedUsersForChannelV2(context.Background(), testChannel, 20, "")
			So(ids, ShouldEqual, nil)
			So(nextCursor, ShouldEqual, "")
			So(restricted, ShouldBeFalse)
			So(err, ShouldNotBeNil)
		})

		Convey("with no restriction", func() {
			backendMocks.restrictionDAO.On("GetRestriction", mock.Anything, testRestriction.ResourceKey).Return(nil, nil)
			ids, restricted, nextCursor, err := backend.GetAuthorizedUsersForChannelV2(context.Background(), testChannel, 20, "")
			So(ids, ShouldEqual, nil)
			So(nextCursor, ShouldEqual, "")
			So(restricted, ShouldBeFalse)
			So(err, ShouldBeNil)
		})

		Convey("with restriction with ALL exemption", func() {
			testRestriction.Exemptions = testdata.ExemptionsAll()
			backendMocks.restrictionDAO.On("GetRestriction", mock.Anything, testRestriction.ResourceKey).Return(testRestriction, nil)
			ids, restricted, nextCursor, err := backend.GetAuthorizedUsersForChannelV2(context.Background(), testChannel, 20, "")
			So(ids, ShouldEqual, nil)
			So(nextCursor, ShouldEqual, "")
			So(restricted, ShouldBeFalse)
			So(err, ShouldBeNil)
		})

		Convey("with product restriction", func() {
			backendMocks.restrictionDAO.On("GetRestriction", mock.Anything, testRestriction.ResourceKey).Return(testRestriction, nil)

			Convey("with VIPs error", func() {
				backendMocks.clients.Zuma.On("ListVIPs", mock.Anything, testChannel, mock.Anything, mock.Anything).Return([]string{}, "", errors.New("error"))
				ids, restricted, nextCursor, err := backend.GetAuthorizedUsersForChannelV2(context.Background(), testChannel, 20, "")
				So(ids, ShouldEqual, nil)
				So(nextCursor, ShouldEqual, "")
				So(restricted, ShouldBeTrue)
				So(err, ShouldNotBeNil)
			})

			Convey("with VIPs result", func() {
				authedIDs := []string{"4444", "666"}
				backendMocks.clients.Zuma.On("ListVIPs", mock.Anything, testChannel, mock.Anything, mock.Anything).Return(authedIDs, "", nil)
				ids, restricted, nextCursor, err := backend.GetAuthorizedUsersForChannelV2(context.Background(), testChannel, 20, "")
				So(ids, ShouldResemble, authedIDs)
				So(nextCursor, ShouldEqual, firstCursor)
				So(restricted, ShouldBeTrue)
				So(err, ShouldBeNil)
			})

			Convey("with mods error", func() {
				backendMocks.clients.Zuma.On("ListMods", mock.Anything, testChannel, mock.Anything, mock.Anything).Return([]string{}, "", errors.New("error"))
				ids, restricted, nextCursor, err := backend.GetAuthorizedUsersForChannelV2(context.Background(), testChannel, 20, firstCursor)
				So(ids, ShouldEqual, nil)
				So(nextCursor, ShouldEqual, "")
				So(restricted, ShouldBeTrue)
				So(err, ShouldNotBeNil)
			})

			Convey("with mods result", func() {
				authedIDs := []string{"4444", "666"}
				backendMocks.clients.Zuma.On("ListMods", mock.Anything, testChannel, mock.Anything, mock.Anything).Return(authedIDs, "", nil)
				ids, restricted, nextCursor, err := backend.GetAuthorizedUsersForChannelV2(context.Background(), testChannel, 20, firstCursor)
				So(ids, ShouldResemble, authedIDs)
				So(nextCursor, ShouldEqual, secondCursor)
				So(restricted, ShouldBeTrue)
				So(err, ShouldBeNil)
			})

			Convey("with subs error", func() {
				backendMocks.clients.TwitchVoyager.On("GetSubscribedUsersForChannel", mock.Anything, testChannel, int32(20), mock.Anything).Return(nil, "", errors.New("error"))
				ids, restricted, nextCursor, err := backend.GetAuthorizedUsersForChannelV2(context.Background(), testChannel, 20, secondCursor)
				So(ids, ShouldEqual, nil)
				So(nextCursor, ShouldEqual, "")
				So(restricted, ShouldBeTrue)
				So(err, ShouldNotBeNil)
			})

			Convey("with subs result", func() {
				authedIDMap := map[string]string{
					"4444": testdata.TestProductID,
					"666":  "345", // this user does not have the right product, and should be filtered out
				}
				backendMocks.clients.TwitchVoyager.On("GetSubscribedUsersForChannel", mock.Anything, testChannel, int32(20), mock.Anything).Return(authedIDMap, "", nil)
				ids, restricted, nextCursor, err := backend.GetAuthorizedUsersForChannelV2(context.Background(), testChannel, 20, secondCursor)
				So(ids, ShouldResemble, []string{"4444"})
				So(nextCursor, ShouldEqual, "")
				So(restricted, ShouldBeTrue)
				So(err, ShouldBeNil)
			})
		})

		Convey("with restriction with ORGANIZATION_MEMBER_ONLY", func() {
			testCompanyID := "5fdbf7a7-6249-44d3-98d8-27800169df69"
			testChannel := "146851449"
			testCompanyMemberIDs := []string{"1246124", "3546136", "12546661"}
			testRestriction := testdata.RestrictionWithOrgAccessOnly(testChannel, testCompanyID)
			backendMocks.restrictionDAO.On("GetRestriction", mock.Anything, testRestriction.ResourceKey).Return(testRestriction, nil)
			backendMocks.clients.Rbac.On("ListUsersByCompanyID", mock.Anything, testChannel, testCompanyID, "0", int32(100)).Return(testCompanyMemberIDs, "", true, nil)
			ids, restricted, nextCursor, err := backend.GetAuthorizedUsersForChannelV2(context.Background(), testChannel, int32(100), "")
			So(ids, ShouldResemble, testCompanyMemberIDs)
			So(nextCursor, ShouldEqual, "")
			So(restricted, ShouldBeTrue)
			So(err, ShouldBeNil)
		})
	})

	Convey("Test GetUserChannelAuthorizations", t, func() {
		backend, backendMocks := initTestableBackend()
		testUser := "1"
		testChannels := []string{"146851449", "146851450", "146851451"}
		testRestriction1 := testdata.Restriction(testChannels[0])
		testRestriction2 := testdata.Restriction(testChannels[1])
		testRestriction3 := testdata.Restriction(testChannels[2])

		req := &models.ChannelAuthorizationsRequest{
			UserID:     testUser,
			ChannelIDs: []string{testChannels[0], testChannels[1], testChannels[2]},
		}

		backendMocks.clock.On("Now").Return(time.Date(2019, time.June, 1, 0, 0, 0, 0, time.UTC))
		backendMocks.clients.Subscriptions.On("GetProductByID", mock.Anything, mock.Anything).Return(nil, nil)
		backendMocks.clients.Spade.On("TrackEvents", mock.Anything, mock.Anything, mock.Anything).Return(nil)

		Convey("with error: ", func() {
			err := errors.New("eh")

			Convey("DAO error", func() {
				backendMocks.restrictionDAO.On("GetRestriction", mock.Anything, testRestriction1.ResourceKey).Return(nil, err)
				backendMocks.restrictionDAO.On("GetRestriction", mock.Anything, testRestriction2.ResourceKey).Return(nil, err)
				backendMocks.restrictionDAO.On("GetRestriction", mock.Anything, testRestriction3.ResourceKey).Return(nil, err)
			})

			Convey("exemption checker error", func() {
				backendMocks.restrictionDAO.On("GetRestriction", mock.Anything, testRestriction1.ResourceKey).Return(testRestriction1, nil)
				backendMocks.restrictionDAO.On("GetRestriction", mock.Anything, testRestriction2.ResourceKey).Return(testRestriction2, nil)
				backendMocks.restrictionDAO.On("GetRestriction", mock.Anything, testRestriction3.ResourceKey).Return(testRestriction3, nil)
				backendMocks.exemptionChecker.On("GetUserExemptionStatus", mock.Anything, testUser, mock.Anything).
					Return(exemptions.UserExemptionStatus{UserID: testUser}, err)
			})

			results, err := backend.GetUserChannelAuthorizations(context.Background(), req)

			So(err, ShouldBeNil)
			So(results, ShouldNotBeNil)
			So(len(results), ShouldEqual, 3)
			So(results[0].Error, ShouldNotBeNil)
			So(results[1].Error, ShouldNotBeNil)
		})

		Convey("with no restrictions", func() {
			backendMocks.restrictionDAO.On("GetRestriction", mock.Anything, testRestriction1.ResourceKey).Return(nil, nil)
			backendMocks.restrictionDAO.On("GetRestriction", mock.Anything, testRestriction2.ResourceKey).Return(nil, nil)
			backendMocks.restrictionDAO.On("GetRestriction", mock.Anything, testRestriction3.ResourceKey).Return(nil, nil)

			Convey("with no user id", func() {
				testUser = ""
				req.UserID = ""
			})

			Convey("with user id", func() {})

			results, err := backend.GetUserChannelAuthorizations(context.Background(), req)

			So(err, ShouldBeNil)
			So(results, ShouldNotBeNil)
			So(len(results), ShouldEqual, 3)
			for _, result := range results {
				So(result.Error, ShouldBeNil)
				So(result.AppliedExemptions, ShouldBeNil)
				So(result.UserIsAuthorized, ShouldBeTrue)
				So(result.ResourceIsRestricted, ShouldBeFalse)
			}
		})

		Convey("with resolved restrictions", func() {
			backendMocks.restrictionDAO.On("GetRestriction", mock.Anything, testRestriction1.ResourceKey).Return(testRestriction1, nil)
			backendMocks.restrictionDAO.On("GetRestriction", mock.Anything, testRestriction2.ResourceKey).Return(testRestriction2, nil)
			backendMocks.restrictionDAO.On("GetRestriction", mock.Anything, testRestriction3.ResourceKey).Return(testRestriction3, nil)
			var expectedAppliedExemptions []*models.AppliedExemption
			expectedUserAuth := false
			expectedRestrictionType := testRestriction1.RestrictionType

			Convey("and applied exemptions returns results with order retained", func() {
				backendMocks.exemptionChecker.On("GetUserExemptionStatus", mock.Anything, testUser, mock.Anything).
					Return(exemptions.UserExemptionStatus{
						UserID:               testUser,
						ApplicableExemptions: testdata.ApplyExemptions(testRestriction1.Exemptions),
					}, nil)

				expectedAppliedExemptions = testdata.ApplyExemptions(testRestriction1.Exemptions)
				expectedUserAuth = true
			})

			Convey("and no applied exemptions returns results with order retained", func() {
				backendMocks.exemptionChecker.On("GetUserExemptionStatus", mock.Anything, testUser, mock.Anything).
					Return(exemptions.UserExemptionStatus{
						UserID:               testUser,
						ApplicableExemptions: nil,
					}, nil)
			})

			results, err := backend.GetUserChannelAuthorizations(context.Background(), req)

			So(err, ShouldBeNil)
			So(results, ShouldNotBeNil)
			So(len(results), ShouldEqual, 3)
			for idx, result := range results {
				So(result.Error, ShouldBeNil)
				So(result.Request.ChannelID, ShouldEqual, testChannels[idx])
				So(result.AppliedExemptions, ShouldResemble, expectedAppliedExemptions)
				So(result.UserIsAuthorized, ShouldEqual, expectedUserAuth)
				So(result.ResourceIsRestricted, ShouldBeTrue)
				So(result.RestrictionType, ShouldEqual, expectedRestrictionType)
			}
		})
	})

	Convey("Test GetUserVODAuthorizations", t, func() {
		backend, backendMocks := initTestableBackend()
		testUser := "1"
		testVODs := []string{"146851449", "146851450", "146851451"}
		testRestriction1 := testdata.VODRestriction(testVODs[0])
		testRestriction2 := testdata.VODRestriction(testVODs[1])
		testRestriction3 := testdata.VODRestriction(testVODs[2])

		req := &models.VODAuthorizationsRequest{
			UserID:   testUser,
			VideoIDs: []string{testVODs[0], testVODs[1], testVODs[2]},
		}

		backendMocks.clock.On("Now").Return(time.Date(2019, time.June, 1, 0, 0, 0, 0, time.UTC))
		backendMocks.clients.Subscriptions.On("GetProductByID", mock.Anything, mock.Anything).Return(nil, nil)
		backendMocks.clients.Spade.On("TrackEvents", mock.Anything, mock.Anything, mock.Anything).Return(nil)
		backendMocks.clients.VodAPI.On("GetVodBroadcastID", mock.Anything, mock.Anything).Return("", nil)

		Convey("with error: ", func() {
			err := errors.New("eh")

			Convey("DAO error", func() {
				backendMocks.restrictionDAO.On("GetRestriction", mock.Anything, testRestriction1.ResourceKey).Return(nil, err)
				backendMocks.restrictionDAO.On("GetRestriction", mock.Anything, testRestriction2.ResourceKey).Return(nil, err)
				backendMocks.restrictionDAO.On("GetRestriction", mock.Anything, testRestriction3.ResourceKey).Return(nil, err)
			})

			Convey("exemption checker error", func() {
				backendMocks.restrictionDAO.On("GetRestriction", mock.Anything, testRestriction1.ResourceKey).Return(testRestriction1, nil)
				backendMocks.restrictionDAO.On("GetRestriction", mock.Anything, testRestriction2.ResourceKey).Return(testRestriction2, nil)
				backendMocks.restrictionDAO.On("GetRestriction", mock.Anything, testRestriction3.ResourceKey).Return(testRestriction3, nil)
				backendMocks.exemptionChecker.On("GetUserExemptionStatus", mock.Anything, testUser, mock.Anything).
					Return(exemptions.UserExemptionStatus{UserID: testUser}, err)
			})

			results, err := backend.GetUserVODAuthorizations(context.Background(), req)

			So(err, ShouldBeNil)
			So(results, ShouldNotBeNil)
			So(len(results), ShouldEqual, 3)
			So(results[0].Error, ShouldNotBeNil)
			So(results[1].Error, ShouldNotBeNil)
		})

		Convey("with no restrictions", func() {
			backendMocks.restrictionDAO.On("GetRestriction", mock.Anything, testRestriction1.ResourceKey).Return(nil, nil)
			backendMocks.restrictionDAO.On("GetRestriction", mock.Anything, testRestriction2.ResourceKey).Return(nil, nil)
			backendMocks.restrictionDAO.On("GetRestriction", mock.Anything, testRestriction3.ResourceKey).Return(nil, nil)

			Convey("with no user id", func() {
				testUser = ""
				req.UserID = ""
			})

			Convey("with user id", func() {})

			results, err := backend.GetUserVODAuthorizations(context.Background(), req)

			So(err, ShouldBeNil)
			So(results, ShouldNotBeNil)
			So(len(results), ShouldEqual, 3)
			for _, result := range results {
				So(result.Error, ShouldBeNil)
				So(result.AppliedExemptions, ShouldBeNil)
				So(result.UserIsAuthorized, ShouldBeTrue)
				So(result.ResourceIsRestricted, ShouldBeFalse)
			}
		})

		Convey("with resolved restrictions", func() {
			backendMocks.restrictionDAO.On("GetRestriction", mock.Anything, testRestriction1.ResourceKey).Return(testRestriction1, nil)
			backendMocks.restrictionDAO.On("GetRestriction", mock.Anything, testRestriction2.ResourceKey).Return(testRestriction2, nil)
			backendMocks.restrictionDAO.On("GetRestriction", mock.Anything, testRestriction3.ResourceKey).Return(testRestriction3, nil)
			var expectedAppliedExemptions []*models.AppliedExemption
			expectedUserAuth := false
			expectedRestrictionType := testRestriction1.RestrictionType

			Convey("and applied exemptions returns results with order retained", func() {
				backendMocks.exemptionChecker.On("GetUserExemptionStatus", mock.Anything, testUser, mock.Anything).
					Return(exemptions.UserExemptionStatus{
						UserID:               testUser,
						ApplicableExemptions: testdata.ApplyExemptions(testRestriction1.Exemptions),
					}, nil)

				expectedAppliedExemptions = testdata.ApplyExemptions(testRestriction1.Exemptions)
				expectedUserAuth = true
			})

			Convey("and no applied exemptions returns results with order retained", func() {
				backendMocks.exemptionChecker.On("GetUserExemptionStatus", mock.Anything, testUser, mock.Anything).
					Return(exemptions.UserExemptionStatus{
						UserID:               testUser,
						ApplicableExemptions: nil,
					}, nil)
			})

			results, err := backend.GetUserVODAuthorizations(context.Background(), req)

			So(err, ShouldBeNil)
			So(results, ShouldNotBeNil)
			So(len(results), ShouldEqual, 3)
			for idx, result := range results {
				So(result.Error, ShouldBeNil)
				So(result.Request.VideoID, ShouldEqual, testVODs[idx])
				So(result.AppliedExemptions, ShouldResemble, expectedAppliedExemptions)
				So(result.UserIsAuthorized, ShouldEqual, expectedUserAuth)
				So(result.ResourceIsRestricted, ShouldBeTrue)
				So(result.RestrictionType, ShouldEqual, expectedRestrictionType)
			}
		})
	})
}

func TestGetUserChannelAuthorization(t *testing.T) {
	Convey("Test GetUserChannelAuthorization", t, func() {
		ctx := context.Background()

		testUserID := "test-user-id"
		testChannelID := "test-channel-id"

		testRequest := &models.ChannelAuthorizationRequest{
			UserID:    testUserID,
			ChannelID: testChannelID,
		}

		testChannelResource := models.Channel{ID: testRequest.ChannelID}

		testRestriction := &models.RestrictionV2{
			ResourceKey:     testChannelResource.GetResourceKey(),
			RestrictionType: "SUB_ONLY_LIVE",
			Exemptions:      testdata.ExemptionsStaffProduct(),
		}

		testUserExemptionStatus := exemptions.UserExemptionStatus{
			UserID: testRequest.UserID,
			ApplicableExemptions: []*models.AppliedExemption{
				{
					Exemption: &models.Exemption{},
				},
			},
		} // return one applied exemption to mark as authorized

		backend, mockClients := initTestableBackend()

		testTime := time.Date(2019, time.June, 1, 0, 0, 0, 0, time.UTC)
		mockClients.clock.On("Now").Return(testTime)
		mockClients.clients.Subscriptions.On("GetProductByID", mock.Anything, mock.Anything).Return(nil, nil)

		mockClients.clients.Spade.On("TrackEvents", mock.Anything, mock.Anything, mock.Anything).Return(nil)

		expectedChannelAuthorization := &models.ChannelAuthorization{
			Request: testRequest,
			ResourceAuthorization: models.ResourceAuthorization{
				UserIsAuthorized:     false,
				ResourceIsRestricted: false,
			},
		}

		Convey("with populated v2 restriction", func() {
			mockClients.restrictionDAO.On("GetRestriction", mock.Anything, testChannelResource.GetResourceKey()).
				Return(testRestriction, nil)

			expectedChannelAuthorization.RestrictionType = testRestriction.RestrictionType
			expectedChannelAuthorization.ResourceIsRestricted = true

			getExemptionMock := mockClients.exemptionChecker.On("GetUserExemptionStatus", mock.Anything, testRequest.UserID, mock.Anything)

			Convey("with exempt user", func() {
				getExemptionMock.Return(testUserExemptionStatus, nil)
				expectedChannelAuthorization.UserIsAuthorized = true
				expectedChannelAuthorization.AppliedExemptions = testUserExemptionStatus.ApplicableExemptions
			})

			Convey("with unexempt user", func() {
				testUserExemptionStatus.ApplicableExemptions = nil
				getExemptionMock.Return(testUserExemptionStatus, nil)
				expectedChannelAuthorization.UserIsAuthorized = false
			})

			Convey("with unexempt user who owns channel", func() {
				testRequest.UserID = testRequest.ChannelID
				testUserExemptionStatus.ApplicableExemptions = nil
				mockClients.exemptionChecker.On("GetUserExemptionStatus", mock.Anything, testRequest.UserID, mock.Anything).
					Return(testUserExemptionStatus, nil)
				expectedChannelAuthorization.UserIsAuthorized = true
			})

			authorization, err := backend.GetUserChannelAuthorization(ctx, testRequest)
			So(err, ShouldBeNil)
			So(authorization, ShouldResemble, expectedChannelAuthorization)
		})

		Convey("with error from exemption checker", func() {
			mockClients.restrictionDAO.On("GetRestriction", mock.Anything, testChannelResource.GetResourceKey()).
				Return(testRestriction, nil)

			mockClients.exemptionChecker.On("GetUserExemptionStatus", mock.Anything, mock.Anything, mock.Anything).
				Return(testUserExemptionStatus, errors.New("your exemptions asplode"))

			_, err := backend.GetUserChannelAuthorization(ctx, testRequest)
			So(err, ShouldNotBeNil)
		})

		Convey("with no restrictions in v2 table", func() {
			mockClients.restrictionDAO.On("GetRestriction", mock.Anything, testChannelResource.GetResourceKey()).
				Return(nil, nil)

			expectedChannelAuthorization.UserIsAuthorized = true
			expectedChannelAuthorization.ResourceIsRestricted = false

			authorization, err := backend.GetUserChannelAuthorization(ctx, testRequest)
			So(err, ShouldBeNil)
			So(authorization, ShouldResemble, expectedChannelAuthorization)
		})

		Convey("with preview consumption", func() {
			testPreviewRequest := &models.ChannelAuthorizationRequest{
				UserID:         testUserID,
				ChannelID:      testChannelID,
				ConsumePreview: true,
			}

			testRestriction := &models.RestrictionV2{
				ResourceKey:     testChannelResource.GetResourceKey(),
				RestrictionType: "SUB_ONLY_LIVE",
				Exemptions:      testdata.ExemptionsPreview(testTime),
			}

			previewOrigin := "ios:autoplay"

			mockClients.restrictionDAO.On("GetRestriction", mock.Anything, mock.Anything).
				Return(testRestriction, nil)

			mockGetUserExemptionStatus := mockClients.exemptionChecker.On("GetUserExemptionStatus", mock.Anything, testPreviewRequest.UserID, mock.Anything, mock.Anything)

			mockClients.chanletDAO.On("GetChanletByID", mock.Anything, testChannelID).Return(&models.Chanlet{OwnerChannelID: testChannelID}, nil)

			mockSetPreviewCall := mockClients.previewDAO.On("SetPreview", mock.Anything, mock.Anything)
			mockGetPreviewCall := mockClients.previewDAO.On("GetPreview", mock.Anything, mock.Anything)
			mockLivelineCall := mockClients.clients.Liveline.On("GetStreamsByChannelIDs", mock.Anything, mock.Anything)

			testUserExemptionStatus.ApplicableExemptions = []*models.AppliedExemption{}

			expectedChannelAuthorization.ResourceIsRestricted = true
			expectedChannelAuthorization.UserIsAuthorized = true
			expectedChannelAuthorization.Request = testPreviewRequest
			expectedChannelAuthorization.RestrictionType = testRestriction.RestrictionType
			expectedChannelAuthorization.AppliedExemptions = testdata.ApplyExemptions(testdata.ExemptionsPreview(testTime))

			Convey("channel is online", func() {
				mockLivelineCall.Return(&liveline.StreamsResponse{
					Streams: []*liveline.StreamResponse{
						{ChannelId: testChannelID},
					},
				}, nil)

				Convey("user meets other exemption type and doesn't need a preview", func() {
					mockClients.previewDAO.AssertNotCalled(t, "GetPreview")
					testUserExemptionStatus.ApplicableExemptions = testdata.ApplyExemptions(testdata.ExemptionsAll())

					expectedChannelAuthorization.AppliedExemptions = testUserExemptionStatus.ApplicableExemptions

					mockGetUserExemptionStatus.Return(testUserExemptionStatus, nil)

					authorization, err := backend.GetUserChannelAuthorization(ctx, testPreviewRequest)
					So(err, ShouldBeNil)
					So(authorization, ShouldResemble, expectedChannelAuthorization)
				})

				mockGetUserExemptionStatus.Return(testUserExemptionStatus, nil)

				Convey("user has no preview record", func() {
					mockSetPreviewCall.Return(nil)
					mockGetPreviewCall.Return(nil, nil)
					authorization, err := backend.GetUserChannelAuthorization(ctx, testPreviewRequest)
					So(err, ShouldBeNil)
					So(authorization, ShouldResemble, expectedChannelAuthorization)

					Convey("with origin", func() {
						mockSetPreviewCall.Return(nil)
						mockGetPreviewCall.Return(nil, nil)
						req := testPreviewRequest
						req.Origin = previewOrigin
						authorization, err := backend.GetUserChannelAuthorization(ctx, req)
						So(err, ShouldBeNil)
						expectedChannelAuthorization.Request.Origin = previewOrigin
						expectedChannelAuthorization.AppliedExemptions = testdata.ApplyExemptions(testdata.ExemptionsCustomPreview(testTime, time.Second*30))
						So(authorization, ShouldResemble, expectedChannelAuthorization)
					})
				})

				Convey("user has old preview record that needs to be reset", func() {
					mockSetPreviewCall.Return(nil)
					testOldPreview := testdata.Preview(testUserID, testChannelID, testTime.Add(time.Hour*-50))
					mockGetPreviewCall.Return(testOldPreview, nil)
					authorization, err := backend.GetUserChannelAuthorization(ctx, testPreviewRequest)
					So(err, ShouldBeNil)
					So(authorization, ShouldResemble, expectedChannelAuthorization)

					Convey("with origin", func() {
						req := testPreviewRequest
						req.Origin = previewOrigin
						authorization, err := backend.GetUserChannelAuthorization(ctx, req)
						So(err, ShouldBeNil)
						expectedChannelAuthorization.Request.Origin = previewOrigin
						expectedChannelAuthorization.AppliedExemptions = testdata.ApplyExemptions(testdata.ExemptionsCustomPreview(testTime, time.Second*30))
						So(authorization, ShouldResemble, expectedChannelAuthorization)
					})

					Convey("with invalid origin", func() {
						req := testPreviewRequest
						req.Origin = "big:yikes"
						authorization, err := backend.GetUserChannelAuthorization(ctx, req)
						So(err, ShouldBeNil)
						So(authorization, ShouldResemble, expectedChannelAuthorization)
					})
				})

				Convey("user has active preview record", func() {
					mockSetPreviewCall.Return(nil)
					timeShift := time.Minute * -3
					testActivePreview := testdata.Preview(testUserID, testChannelID, testTime.Add(timeShift))
					expectedChannelAuthorization.AppliedExemptions[0].Exemption.ActiveStartDate =
						expectedChannelAuthorization.AppliedExemptions[0].Exemption.ActiveStartDate.Add(timeShift)
					expectedChannelAuthorization.AppliedExemptions[0].Exemption.ActiveEndDate =
						expectedChannelAuthorization.AppliedExemptions[0].Exemption.ActiveEndDate.Add(timeShift)
					mockGetPreviewCall.Return(testActivePreview, nil)
					authorization, err := backend.GetUserChannelAuthorization(ctx, testPreviewRequest)
					So(err, ShouldBeNil)
					So(authorization, ShouldResemble, expectedChannelAuthorization)

					Convey("with origin", func() {
						req := testPreviewRequest
						req.Origin = previewOrigin
						authorization, err := backend.GetUserChannelAuthorization(ctx, req)
						So(err, ShouldBeNil)
						expectedChannelAuthorization.Request.Origin = previewOrigin
						So(authorization, ShouldResemble, expectedChannelAuthorization)
					})

					Convey("with invalid origin", func() {
						req := testPreviewRequest
						req.Origin = "big:yikes"
						authorization, err := backend.GetUserChannelAuthorization(ctx, req)
						So(err, ShouldBeNil)
						So(authorization, ShouldResemble, expectedChannelAuthorization)
					})
				})

				// TODO: Fix mocking of time.Now
				SkipConvey("user has active preview record that has expired content time", func() {
					mockSetPreviewCall.Return(nil)
					testExpiredPreview := testdata.Preview(testUserID, testChannelID, testTime.Add(time.Minute*-8))
					mockGetPreviewCall.Return(testExpiredPreview, nil)
					expectedChannelAuthorization.UserIsAuthorized = false
					expectedChannelAuthorization.AppliedExemptions = []*models.AppliedExemption{}

					authorization, err := backend.GetUserChannelAuthorization(ctx, testPreviewRequest)
					So(err, ShouldBeNil)
					So(authorization, ShouldResemble, expectedChannelAuthorization)

					Convey("with origin", func() {
						req := testPreviewRequest
						req.Origin = previewOrigin
						authorization, err := backend.GetUserChannelAuthorization(ctx, req)
						expectedChannelAuthorization.Request.Origin = previewOrigin
						So(err, ShouldBeNil)
						So(authorization, ShouldResemble, expectedChannelAuthorization)
					})

					Convey("with invalid origin", func() {
						req := testPreviewRequest
						req.Origin = "big:yikes"
						authorization, err := backend.GetUserChannelAuthorization(ctx, req)
						So(err, ShouldBeNil)
						So(authorization, ShouldResemble, expectedChannelAuthorization)
					})
				})

				Convey("with failed SetPreview DAO", func() {
					mockSetPreviewCall.Return(errors.New("asplode"))
					mockGetPreviewCall.Return(nil, nil)
					authorization, err := backend.GetUserChannelAuthorization(ctx, testPreviewRequest)
					So(err, ShouldBeNil)
					So(authorization, ShouldResemble, expectedChannelAuthorization)
				})
			})

			Convey("channel is offline", func() {
				mockLivelineCall.Return(&liveline.StreamsResponse{
					Streams: []*liveline.StreamResponse{},
				}, nil)
				mockGetPreviewCall.Return(nil, nil)
				mockGetUserExemptionStatus.Return(testUserExemptionStatus, nil)

				authorization, err := backend.GetUserChannelAuthorization(ctx, testPreviewRequest)
				So(err, ShouldBeNil)
				So(authorization, ShouldResemble, expectedChannelAuthorization)
			})
		})
	})
}
