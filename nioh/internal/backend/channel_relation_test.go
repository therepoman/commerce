package backend

import (
	"context"
	"testing"

	"code.justin.tv/commerce/nioh/internal/models"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestChannelRelationBackend(t *testing.T) {
	Convey("Test Backend", t, func() {
		mockBackend, mockBackendClients := initTestableBackend()
		channelID := "123"
		relatedChannelID := "123"

		channelRelation := &models.ChannelRelation{
			ChannelID:        channelID,
			RelatedChannelID: relatedChannelID,
			Relation:         models.RelationBorrower,
		}
		channelRelations := []*models.ChannelRelation{channelRelation}

		Convey("Test CreateChannelRelation", func() {
			Convey("with success", func() {
				Convey("with existing owner chanlet for borrower channel", func() {
					mockBackendClients.chanletDAO.On("GetChanlet", mock.Anything, mock.Anything, mock.Anything).Return(&models.Chanlet{}, nil)
					mockBackendClients.channelRelationDAO.On("AddChannelRelation", mock.Anything, mock.Anything).Return(nil)
					res, err := mockBackend.CreateChannelRelation(context.Background(), channelID, relatedChannelID, models.RelationBorrower)
					So(err, ShouldBeNil)
					So(res, ShouldResemble, channelRelation)
					// Owner chanlet was NOT created for borrower channel
					mockBackendClients.chanletDAO.AssertNotCalled(t, "AddChanlet")
				})

				Convey("without existing owner chanlet for borrower channel", func() {
					mockBackendClients.chanletDAO.On("GetChanlet", mock.Anything, mock.Anything, mock.Anything).Return(nil, nil)
					mockBackendClients.chanletDAO.On("AddChanlet", mock.Anything, mock.Anything).Return(nil)
					mockBackendClients.channelRelationDAO.On("AddChannelRelation", mock.Anything, mock.Anything).Return(nil)
					res, err := mockBackend.CreateChannelRelation(context.Background(), channelID, relatedChannelID, models.RelationBorrower)
					So(err, ShouldBeNil)
					So(res, ShouldResemble, channelRelation)
					// Owner chanlet was created for borrower channel
					mockBackendClients.chanletDAO.AssertNumberOfCalls(t, "AddChanlet", 1)
				})
			})

			Convey("with DAO failure", func() {
				Convey("with AddChannelRelation DAO failure", func() {
					mockBackendClients.chanletDAO.On("GetChanlet", mock.Anything, mock.Anything, mock.Anything).Return(&models.Chanlet{}, nil)
					mockBackendClients.channelRelationDAO.On("AddChannelRelation", mock.Anything, mock.Anything).Return(errors.New("test"))
					res, err := mockBackend.CreateChannelRelation(context.Background(), channelID, relatedChannelID, models.RelationBorrower)
					So(err, ShouldNotBeNil)
					So(res, ShouldBeNil)
				})

				Convey("with GetChanlet DAO failure", func() {
					mockBackendClients.chanletDAO.On("GetChanlet", mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("test"))
					res, err := mockBackend.CreateChannelRelation(context.Background(), channelID, relatedChannelID, models.RelationBorrower)
					So(err, ShouldNotBeNil)
					So(res, ShouldBeNil)
				})

				Convey("with AddChanlet DAO failure", func() {
					mockBackendClients.chanletDAO.On("GetChanlet", mock.Anything, mock.Anything, mock.Anything).Return(nil, nil)
					mockBackendClients.chanletDAO.On("AddChanlet", mock.Anything, mock.Anything).Return(errors.New("test"))
					res, err := mockBackend.CreateChannelRelation(context.Background(), channelID, relatedChannelID, models.RelationBorrower)
					So(err, ShouldNotBeNil)
					So(res, ShouldBeNil)
				})
			})
		})

		Convey("Test DeleteChannelRelation", func() {
			Convey("with success", func() {
				mockBackendClients.channelRelationDAO.On("DeleteChannelRelation", mock.Anything, channelID, relatedChannelID).Return(nil)
				err := mockBackend.DeleteChannelRelation(context.Background(), channelID, relatedChannelID)
				So(err, ShouldBeNil)
			})

			Convey("with DAO failure", func() {
				mockBackendClients.channelRelationDAO.On("DeleteChannelRelation", mock.Anything, channelID, relatedChannelID).Return(errors.New("test"))
				err := mockBackend.DeleteChannelRelation(context.Background(), channelID, relatedChannelID)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("Test GetChannelRelationsByChannelID", func() {
			Convey("with success", func() {
				mockBackendClients.channelRelationDAO.On("GetChannelRelationsByChannelID", mock.Anything, channelID).Return(channelRelations, nil)
				res, err := mockBackend.GetChannelRelationsByChannelID(context.Background(), channelID)
				So(err, ShouldBeNil)
				So(res, ShouldResemble, channelRelations)
			})

			Convey("with DAO failure", func() {
				mockBackendClients.channelRelationDAO.On("GetChannelRelationsByChannelID", mock.Anything, channelID).Return(nil, errors.New("test"))
				res, err := mockBackend.GetChannelRelationsByChannelID(context.Background(), channelID)
				So(err, ShouldNotBeNil)
				So(res, ShouldBeNil)
			})
		})

		Convey("Test GetChannelRelationsByRelatedChannelID", func() {
			Convey("with success", func() {
				mockBackendClients.channelRelationDAO.On("GetChannelRelationsByRelatedChannelID", mock.Anything, relatedChannelID).Return(channelRelations, nil)
				res, err := mockBackend.GetChannelRelationsByRelatedChannelID(context.Background(), relatedChannelID)
				So(err, ShouldBeNil)
				So(res, ShouldResemble, channelRelations)
			})

			Convey("with DAO failure", func() {
				mockBackendClients.channelRelationDAO.On("GetChannelRelationsByRelatedChannelID", mock.Anything, relatedChannelID).Return(nil, errors.New("test"))
				res, err := mockBackend.GetChannelRelationsByRelatedChannelID(context.Background(), relatedChannelID)
				So(err, ShouldNotBeNil)
				So(res, ShouldBeNil)
			})
		})
	})
}
