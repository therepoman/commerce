package backend

import (
	"context"
	"fmt"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/nioh/internal/models"
	"code.justin.tv/commerce/nioh/pkg/loaders"
	"code.justin.tv/common/chitin"
	"github.com/pkg/errors"
)

const (
	statsRestrictionBroadcast = "backend.GetRestriction.broadcast"
	statsRestrictionChannel   = "backend.GetRestriction.channel"
	statsRestrictionVOD       = "backend.GetRestriction.vod"
)

// GetRestriction returns a single restriction for a resource
// even if there are multiple restrictions. Restrictions are prioritized:
//   1) legal restrictions (currently only geoblocks for netzdg)
//   2) twitch product restrictions (sub only content)
func (b *backendImpl) GetRestriction(ctx context.Context, resource models.RestrictableResource, isPlayback bool) (*models.RestrictionV2, error) {
	var err error
	var restriction *models.RestrictionV2

	b.incResourceTypeStat(resource.GetType())

	// if the request asked to check for geoblocks, we need to determine
	// if the resource being requested was a VOD, and if so, get the vod's
	// broadcast ID to see if there are restrictions against it
	if isPlayback && resource.GetType() == models.ResourceTypeVOD {
		broadcastID, err := b.clients.VodAPI.GetVodBroadcastID(ctx, resource.GetIdentifier())
		if err != nil {
			return nil, errors.Wrap(err, "error calling vodAPI")
		}

		if broadcastID != "" {
			broadcast := models.Broadcast{ID: broadcastID}
			restriction, err = b.daos.RestrictionDAO.GetRestriction(ctx, broadcast.GetResourceKey())
			if err != nil {
				return nil, errors.Wrap(err, "error getting from restrictionDAO")
			}
		}
	}

	// if restriction is nil, that means the resource type wasn't a vod or it was a vod and we didn't
	// find a restriction for the vod's broadcast, so we lookup restrictions keyed under the vod id
	if restriction == nil {
		restriction, err = b.daos.RestrictionDAO.GetRestriction(ctx, resource.GetResourceKey())
		if err != nil {
			return nil, errors.Wrap(err, "unexpected error from RestrictionDAO.GetRestriction while handling GetRestriction")
		}
	}

	// Skip if the loader does not exist
	if loaders.LoaderExists(ctx, loaders.SubscriptionProductLoaderKey) {
		b.hydrateRestrictionExemptionActions(ctx, resource, restriction)
	}

	b.filterToActiveExemptions(restriction)

	return restriction, nil
}

func (b *backendImpl) hydrateRestrictionExemptionActions(ctx context.Context, resource models.RestrictableResource, restriction *models.RestrictionV2) {
	if restriction == nil {
		return
	}
	for _, exemption := range restriction.Exemptions {
		var actions []*models.ExemptionAction
		switch exemption.ExemptionType {
		case models.ProductExemptionType:
			if resource.GetType() == models.ResourceTypeVOD {
				actions = b.getActionsForProductExemption(ctx, restriction, exemption)
			}
		default:
			continue
		}

		if actions != nil {
			exemption.Actions = actions
		}
	}
}

func (b *backendImpl) filterToActiveExemptions(restriction *models.RestrictionV2) {
	if restriction == nil {
		return
	}

	var activeExemptions []*models.Exemption

	now := b.clock.Now()
	for _, exemption := range restriction.Exemptions {
		if exemption.IsActive(now) {
			activeExemptions = append(activeExemptions, exemption)
		}
	}

	restriction.Exemptions = activeExemptions
}

// SetRestriction sets a V2 restriction for the passed resource
func (b *backendImpl) SetRestriction(ctx context.Context, resource models.RestrictableResource, restriction models.RestrictionV2) (*models.RestrictionV2, error) {
	var resourceID string

	switch resource := resource.(type) {
	case models.Channel, models.Broadcast:
		resourceID = resource.GetIdentifier()
	case models.VOD:
		resourceID = resource.OwnerID
	default:
		return nil, errors.New("unexpected resource type")
	}

	exemptions, err := b.exemptionsForRestriction(ctx, resourceID, restriction)
	if err != nil {
		return nil, err
	}

	restriction.ResourceKey = resource.GetResourceKey()
	restriction.Exemptions = exemptions

	if err := b.daos.RestrictionDAO.SetRestriction(ctx, &restriction); err != nil {
		return nil, errors.Wrapf(err, "failed to create restriction database entry for resource %s", restriction.ResourceKey)
	}

	// Sever context tree to prevent canceling autotag send when request handler returns
	go b.updateAutotag(context.Background(), resource, &restriction)

	return &restriction, nil
}

// DeleteRestriction  deletes and returns a V2 restriction for the passed resource. If no restriction exists, nil is returned.
func (b *backendImpl) DeleteRestriction(ctx context.Context, resource models.RestrictableResource) (*models.RestrictionV2, error) {
	restriction, err := b.daos.RestrictionDAO.GetRestriction(ctx, resource.GetResourceKey())
	if err != nil {
		return nil, errors.Wrap(err, "unexpected error from RestrictionDAO.GetRestriction while handling DeleteRestriction")
	}
	if restriction == nil {
		return nil, nil
	}
	if err := b.daos.RestrictionDAO.DeleteRestriction(ctx, resource.GetResourceKey()); err != nil {
		return nil, errors.Wrap(err, "unexpected error from RestrictionDAO.DeleteRestriction while handling DeleteRestriction")
	}

	// Sever context tree to prevent canceling autotag send when request handler returns
	go b.updateAutotag(context.Background(), resource, nil)

	return restriction, nil
}

// Returns exemptions list based on restrictionType
func (b *backendImpl) exemptionsForRestriction(ctx context.Context, resourceID string, restriction models.RestrictionV2) ([]*models.Exemption, error) {
	switch restriction.RestrictionType {
	case models.AllAccessPassRestrictionType:
		return b.exemptionsForAllAccessPass(ctx)
	case models.SubOnlyLiveRestrictionType:
		return b.exemptionsForSubOnlyLive(ctx, resourceID, restriction.Options)
	case models.OrganizationAccessOnlyType:
		return b.exemptionsForOrganizationAccessOnly(ctx, resourceID)
	case models.GeoblockRestrictionType:
		return b.exemptionsForGeoblocks(ctx, restriction.Geoblocks), nil
	default:
		return nil, fmt.Errorf("no exemptions for restrictionType: %s", restriction.RestrictionType)
	}
}

// Returns exemptions for the ALL_ACCESS_PASS restriction
func (b *backendImpl) exemptionsForAllAccessPass(ctx context.Context) ([]*models.Exemption, error) {
	allAccessPassExemption, err := b.exemptionForChannelProducts(ctx, &exemptionForChannelProductsInput{
		channelID: models.OWLAllAccessPassChannelID,
	})
	if err != nil {
		return nil, err
	}
	return []*models.Exemption{allAccessPassExemption}, nil
}

// Returns exemptions for the SUB_ONLY_LIVE restriction
func (b *backendImpl) exemptionsForSubOnlyLive(ctx context.Context, channelID string, options []models.RestrictionOption) ([]*models.Exemption, error) {
	optionsInclude := func(target models.RestrictionOption) bool {
		for _, o := range options {
			if o == target {
				return true
			}
		}
		return false
	}

	// Add staff exemption by default
	staffExemption := b.newActiveExemption(models.StaffExemptionType, []string{})

	// Add site-admin exemption by default
	siteAdminExemption := b.newActiveExemption(models.SiteAdminExemptionType, []string{})

	// Product exemption
	var minimumTier uint8

	if optionsInclude(models.AllowTier3Only) {
		minimumTier = 3
	} else if optionsInclude(models.AllowTier2And3Only) {
		minimumTier = 2
	}

	subscribableChannelID, err := b.resolveOwnerChannelID(ctx, channelID)
	if err != nil {
		return nil, err
	}

	productExemption, err := b.exemptionForChannelProducts(ctx, &exemptionForChannelProductsInput{
		channelID:   subscribableChannelID,
		minimumTier: minimumTier,
	})
	if err != nil {
		return nil, err
	}

	// Add preview exemption by default
	previewExemption := b.newActiveExemption(models.PreviewExemptionType, []string{})

	// Add supported optional exemptions
	var optionalExemptions []*models.Exemption
	if optionsInclude(models.AllowChannelVIPOption) {
		optionalExemptions = append(optionalExemptions, b.newActiveExemption(models.ChannelVIPExemptionType, []string{subscribableChannelID}))
	}
	if optionsInclude(models.AllowChannelModeratorOption) {
		optionalExemptions = append(optionalExemptions, b.newActiveExemption(models.ChannelModeratorExemptionType, []string{subscribableChannelID}))
	}

	return append([]*models.Exemption{staffExemption, siteAdminExemption, productExemption, previewExemption}, optionalExemptions...), nil
}

type exemptionForChannelProductsInput struct {
	channelID   string
	minimumTier uint8
}

// Returns a PRODUCT exemption for the given channel's products
func (b *backendImpl) exemptionForChannelProducts(ctx context.Context, input *exemptionForChannelProductsInput) (*models.Exemption, error) {
	products, err := b.clients.Subscriptions.GetChannelProducts(ctx, input.channelID)
	if err != nil {
		return nil, errors.Wrap(err, "unexpected error from Subscriptions.GetChannelProducts")
	}
	if len(products) == 0 {
		return nil, errors.New("no products exist for channel")
	}

	productKeys := make([]string, 0, len(products))
	for _, product := range products {
		// The return values are not enums, but they are documented (1000, 2000, or 3000)
		// https://git-aws.internal.justin.tv/revenue/subscriptions/blob/master/twirp/subscriptions.proto#L1464
		if product.Tier == "1000" && input.minimumTier > 1 {
			continue
		} else if product.Tier == "2000" && input.minimumTier > 2 {
			continue
		}
		productKeys = append(productKeys, product.Id)
	}
	return b.newActiveExemption(models.ProductExemptionType, productKeys), nil
}

// Returns a PREVIEW exemption for the given Preview
func (b *backendImpl) exemptionForPreview(ctx context.Context, preview *models.Preview) *models.Exemption {

	return &models.Exemption{
		ExemptionType:   models.PreviewExemptionType,
		ActiveStartDate: preview.CreatedAt,
		ActiveEndDate:   preview.ContentExpiresAt,
		Keys:            []string{},
	}
}

// Helper that returns a new exemption starting now
func (b *backendImpl) newActiveExemption(exemptionType models.ExemptionType, keys []string) *models.Exemption {
	return &models.Exemption{
		ExemptionType:   exemptionType,
		ActiveStartDate: b.clock.Now(),
		ActiveEndDate:   models.IndefiniteEndDate,
		Keys:            keys,
	}
}

// Helper to get actions for a Product exemption
func (b *backendImpl) getActionsForProductExemption(ctx context.Context, restriction *models.RestrictionV2, exemption *models.Exemption) []*models.ExemptionAction {
	if len(exemption.Keys) == 0 {
		return nil
	}

	log := logrus.WithFields(logrus.Fields{
		"RequestID":   chitin.GetTraceID(ctx),
		"ResourceKey": restriction.ResourceKey,
	})
	actions := make([]*models.ExemptionAction, 0, len(exemption.Keys))
	products, err := loaders.GetProductsByIDs(ctx, exemption.Keys)
	if err != nil {
		log.WithField("keys", exemption.Keys).WithError(err).Error("error loading products by ids")
		return nil
	}
	for _, key := range exemption.Keys {
		product, ok := products[key]
		if !ok || product == nil {
			msg := "could not find product with given key"
			log.WithField("productKey", key).WithError(errors.New(msg)).Error(msg)
			continue
		}
		action := &models.ExemptionAction{
			Name:  product.ShortName,
			Title: product.DisplayName,
		}
		actions = append(actions, action)
	}
	return actions
}

func (b *backendImpl) updateAutotag(ctx context.Context, resource models.RestrictableResource, restriction *models.RestrictionV2) {
	err := b.autotagger.UpdateAutotag(ctx, models.RestrictionUpdateEvent{
		Resource:    resource,
		Restriction: restriction,
	})
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"ResourceKey": resource.GetResourceKey(),
			"Restriction": restriction,
		}).WithError(err).Error("failed to send autotag update")
	}
}

func (b *backendImpl) exemptionsForOrganizationAccessOnly(ctx context.Context, channelID string) ([]*models.Exemption, error) {
	// currently the ememption only supports organization members
	companyIDs, err := b.clients.Rbac.ListCompanyIDsForUser(ctx, channelID)
	if err != nil {
		return nil, err
	}

	return []*models.Exemption{b.newActiveExemption(models.OrganizationMemberExemptionType, companyIDs)}, nil
}

// Returns exemptions for the GEOBLOCK restriction
func (b *backendImpl) exemptionsForGeoblocks(ctx context.Context, geoblocks []models.Geoblock) []*models.Exemption {
	exemptions := make([]*models.Exemption, len(geoblocks))

	for i, g := range geoblocks {
		o := models.NotInCountriesExemptionType
		if g.Operator == models.AllowOperator {
			o = models.InCountriesExemptionType
		}

		exemptions[i] = b.newActiveExemption(o, g.CountryCodes)
	}

	return exemptions
}

func (b *backendImpl) incResourceTypeStat(rt models.ResourceType) {
	switch rt {
	case models.ResourceTypeChannel:
		b.incStats(statsRestrictionChannel, 1)
	case models.ResourceTypeVOD:
		b.incStats(statsRestrictionVOD, 1)
	case models.ResourceTypeBroadcast:
		b.incStats(statsRestrictionBroadcast, 1)
	}
}
