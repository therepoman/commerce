package backend

import (
	"context"
	"testing"
	"time"

	"code.justin.tv/commerce/nioh/internal/models"
	"code.justin.tv/commerce/nioh/internal/testdata"
	"code.justin.tv/discovery/liveline/proto/liveline"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestPreviewBackend(t *testing.T) {
	testUserID := "1234"
	testChannelID := "146851449"
	testResource := &models.Channel{ID: testChannelID}
	testTime := time.Date(2018, time.October, 1, 0, 0, 0, 0, time.UTC)
	testPreview := testdata.Preview(testUserID, testResource.GetResourceKey(), testTime)

	Convey("Test Preview Backend", t, func() {
		backend, mockClients := initTestableBackend()
		mockLivelineCall := mockClients.clients.Liveline.On("GetStreamsByChannelIDs", mock.Anything, mock.Anything)

		mockClients.clock.On("Now").Return(testTime)

		Convey("Test GetPreview", func() {
			Convey("with no preview", func() {
				mockClients.previewDAO.On("GetPreview", mock.Anything, mock.Anything).Return(nil, nil)

				res, err := backend.GetPreview(context.Background(), testUserID, testResource)
				So(res, ShouldBeNil)
				So(err, ShouldBeNil)
			})

			Convey("with preview", func() {
				mockClients.previewDAO.On("GetPreview", mock.Anything, mock.Anything).Return(testPreview, nil)

				res, err := backend.GetPreview(context.Background(), testUserID, testResource)
				So(res, ShouldResemble, testPreview)
				So(err, ShouldBeNil)
			})

			Convey("with DAO failure", func() {
				mockClients.previewDAO.On("GetPreview", mock.Anything, mock.Anything).Return(nil, errors.New("your DAO asplode"))

				res, err := backend.GetPreview(context.Background(), testUserID, testResource)
				So(res, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("Test SetPreview", func() {
			Convey("channel is online", func() {
				mockLivelineCall.Return(&liveline.StreamsResponse{
					Streams: []*liveline.StreamResponse{
						{ChannelId: testChannelID},
					},
				}, nil)

				Convey("returns preview record", func() {
					mockClients.previewDAO.On("SetPreview", mock.Anything, mock.Anything).Return(nil)

					res, err := backend.SetPreview(context.Background(), testUserID, testResource)
					So(res, ShouldResemble, testPreview)
					So(err, ShouldBeNil)
				})

				Convey("returns error without userID", func() {
					mockClients.previewDAO.On("SetPreview", mock.Anything, mock.Anything).Return(nil)

					res, err := backend.SetPreview(context.Background(), "", testResource)
					So(res, ShouldBeNil)
					So(err, ShouldNotBeNil)
				})

				Convey("returns error with nil Resource", func() {
					mockClients.previewDAO.On("SetPreview", mock.Anything, mock.Anything).Return(nil)

					res, err := backend.SetPreview(context.Background(), testUserID, models.Channel{})
					So(res, ShouldBeNil)
					So(err, ShouldNotBeNil)
				})

				Convey("handles DAO error", func() {
					mockClients.previewDAO.On("SetPreview", mock.Anything, mock.Anything).Return(errors.New("your DAO asplode"))

					res, err := backend.SetPreview(context.Background(), testUserID, testResource)
					So(res, ShouldNotBeNil)
					So(err, ShouldBeNil)
				})
			})
		})

		Convey("Test DeletePreview", func() {
			Convey("returns deleted preview", func() {
				mockClients.previewDAO.On("GetPreview", mock.Anything, mock.Anything).Return(testPreview, nil)
				mockClients.previewDAO.On("DeletePreview", mock.Anything, mock.Anything).Return(nil)

				res, err := backend.DeletePreview(context.Background(), testUserID, testResource)
				So(res, ShouldResemble, testPreview)
				So(err, ShouldBeNil)
			})

			Convey("returns nil without a preview record", func() {
				mockClients.previewDAO.On("GetPreview", mock.Anything, mock.Anything).Return(nil, nil)
				mockClients.previewDAO.On("DeletePreview", mock.Anything, mock.Anything).Return(nil)

				res, err := backend.DeletePreview(context.Background(), testUserID, testResource)
				So(res, ShouldBeNil)
				So(err, ShouldBeNil)
			})

			Convey("with Get DAO failure", func() {
				mockClients.previewDAO.On("GetPreview", mock.Anything, mock.Anything).Return(nil, errors.New("your DAO asplode"))

				res, err := backend.DeletePreview(context.Background(), testUserID, testResource)
				So(res, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})

			Convey("with Delete DAO failure", func() {
				mockClients.previewDAO.On("GetPreview", mock.Anything, mock.Anything).Return(testPreview, nil)
				mockClients.previewDAO.On("DeletePreview", mock.Anything, mock.Anything).Return(errors.New("your DAO asplode"))

				res, err := backend.DeletePreview(context.Background(), testUserID, testResource)
				So(res, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("Test ConsumePreview", func() {
			mockClients.previewDAO.On("SetPreview", mock.Anything, mock.Anything).Return(nil)

			testOldPreview := testdata.Preview(testUserID, testResource.GetResourceKey(), testTime.Add(time.Hour*-50))
			testActivePreview := testdata.Preview(testUserID, testResource.GetResourceKey(), testTime.Add(time.Minute*-3))
			testExpiredPreview := testdata.Preview(testUserID, testResource.GetResourceKey(), testTime.Add(time.Minute*-8))

			mockClients.chanletDAO.On("GetChanletByID", mock.Anything, mock.Anything).Return(&models.Chanlet{OwnerChannelID: testChannelID}, nil)

			Convey("channel is online", func() {
				mockLivelineCall.Return(&liveline.StreamsResponse{
					Streams: []*liveline.StreamResponse{
						{ChannelId: testChannelID},
					},
				}, nil)

				Convey("returns new preview record if user doesn't have one", func() {
					mockClients.previewDAO.On("GetPreview", mock.Anything, mock.Anything).Return(nil, nil)

					res, err := backend.ConsumePreview(context.Background(), testUserID, testResource)
					So(res, ShouldResemble, testPreview)
					So(err, ShouldBeNil)
				})

				Convey("returns active preview record if content has not expired", func() {
					mockClients.previewDAO.On("GetPreview", mock.Anything, mock.Anything).Return(testActivePreview, nil)

					res, err := backend.ConsumePreview(context.Background(), testUserID, testResource)
					So(res, ShouldResemble, testActivePreview)
					So(err, ShouldBeNil)
				})

				Convey("returns nil if content has expired", func() {
					mockClients.previewDAO.On("GetPreview", mock.Anything, mock.Anything).Return(testExpiredPreview, nil)

					res, err := backend.ConsumePreview(context.Background(), testUserID, testResource)
					So(res, ShouldBeNil)
					So(err, ShouldBeNil)
				})

				Convey("returns new preview record if user has an old one", func() {
					mockClients.previewDAO.On("GetPreview", mock.Anything, mock.Anything).Return(testOldPreview, nil)

					res, err := backend.ConsumePreview(context.Background(), testUserID, testResource)
					So(res, ShouldResemble, testPreview)
					So(err, ShouldBeNil)
				})

				Convey("returns nil for logged out user", func() {
					res, err := backend.ConsumePreview(context.Background(), "", testResource)
					So(res, ShouldBeNil)
					So(err, ShouldBeNil)
				})

				Convey("returns nil for no resource", func() {
					mockClients.previewDAO.On("GetPreview", mock.Anything, mock.Anything).Return(nil, nil)

					res, err := backend.ConsumePreview(context.Background(), testUserID, models.Channel{})
					So(res, ShouldBeNil)
					So(err, ShouldBeNil)
				})

				Convey("with Get DAO failure", func() {
					mockClients.previewDAO.On("GetPreview", mock.Anything, mock.Anything).Return(nil, errors.New("your DAO asplode"))

					res, err := backend.DeletePreview(context.Background(), testUserID, testResource)
					So(res, ShouldBeNil)
					So(err, ShouldNotBeNil)
				})

				Convey("channel is a chanlet", func() {
					restrictedChanlet1 := &models.Channel{ID: "some-restricted-chanlet-id"}
					restrictedChanlet2 := &models.Channel{ID: "some-other-restricted-chanlet-id"}

					Convey("returns active preview record if content has not expired", func() {
						mockClients.previewDAO.On("GetPreview", mock.Anything, mock.Anything).Return(testActivePreview, nil)

						res, err := backend.ConsumePreview(context.Background(), testUserID, restrictedChanlet1)
						So(res, ShouldResemble, testActivePreview)
						So(err, ShouldBeNil)

						res, err = backend.ConsumePreview(context.Background(), testUserID, restrictedChanlet2)
						So(res, ShouldResemble, testActivePreview)
						So(err, ShouldBeNil)
					})
				})
			})

			Convey("channel is offline", func() {
				mockLivelineCall.Return(&liveline.StreamsResponse{
					Streams: []*liveline.StreamResponse{},
				}, nil)

				Convey("returns new preview record if user doesn't have one", func() {
					mockClients.previewDAO.On("GetPreview", mock.Anything, mock.Anything).Return(nil, nil)

					res, err := backend.ConsumePreview(context.Background(), testUserID, testResource)
					So(res, ShouldResemble, testPreview)
					So(err, ShouldBeNil)
				})

				Convey("returns active preview record if content has not expired", func() {
					mockClients.previewDAO.On("GetPreview", mock.Anything, mock.Anything).Return(testActivePreview, nil)

					res, err := backend.ConsumePreview(context.Background(), testUserID, testResource)
					So(res, ShouldResemble, testActivePreview)
					So(err, ShouldBeNil)
				})

				Convey("returns nil if content has expired", func() {
					mockClients.previewDAO.On("GetPreview", mock.Anything, mock.Anything).Return(testExpiredPreview, nil)

					res, err := backend.ConsumePreview(context.Background(), testUserID, testResource)
					So(res, ShouldBeNil)
					So(err, ShouldBeNil)
				})

				Convey("returns new preview record if user has an old one", func() {
					mockClients.previewDAO.On("GetPreview", mock.Anything, mock.Anything).Return(testOldPreview, nil)

					res, err := backend.ConsumePreview(context.Background(), testUserID, testResource)
					So(res, ShouldResemble, testPreview)
					So(err, ShouldBeNil)
				})

				Convey("returns nil for logged out user", func() {
					res, err := backend.ConsumePreview(context.Background(), "", testResource)
					So(res, ShouldBeNil)
					So(err, ShouldBeNil)
				})

				Convey("returns nil for no resource", func() {
					mockClients.previewDAO.On("GetPreview", mock.Anything, mock.Anything).Return(nil, nil)

					res, err := backend.ConsumePreview(context.Background(), testUserID, models.Channel{})
					So(res, ShouldBeNil)
					So(err, ShouldBeNil)
				})

				Convey("with Get DAO failure", func() {
					mockClients.previewDAO.On("GetPreview", mock.Anything, mock.Anything).Return(nil, errors.New("your DAO asplode"))

					res, err := backend.DeletePreview(context.Background(), testUserID, testResource)
					So(res, ShouldBeNil)
					So(err, ShouldNotBeNil)
				})

				mockClients.previewDAO.AssertNotCalled(t, "SetPreview")
			})
		})
	})
}
