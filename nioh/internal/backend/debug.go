package backend

import (
	"context"
	"fmt"
)

// TestConnectionsResult records the result of each connection's test action as a string.
type TestConnectionsResult struct {
	UsersService      string `json:"users-service"`
	ChannelProperties string `json:"channel-properties"`
}

func (b *backendImpl) TestConnections(ctx context.Context) *TestConnectionsResult {
	res := &TestConnectionsResult{}
	testUserID := "215590603" // cthedork

	// Users-Service Connection Test
	user, err := b.clients.UsersClient.GetUserByID(ctx, testUserID)
	if err != nil {
		res.UsersService = fmt.Sprintf("Users-Service test call failed: %v.", err)
	} else {
		res.UsersService = fmt.Sprintf("Users-Service test call successful: user is %s.", *user.Login)
	}

	// Video Channel Properties Connection Test
	key, err := b.clients.ChannelProperties.GetStreamKey(ctx, testUserID)
	if err != nil {
		res.ChannelProperties = fmt.Sprintf("Channel Propertes test call failed: %v.", err)
	} else {
		res.ChannelProperties = fmt.Sprintf("Channel Properties test call successful: key is %s.", key)
	}

	return res
}
