package backend

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"sync"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/nioh/internal/models"
	"code.justin.tv/common/chitin"
	"github.com/pkg/errors"
)

const (
	statsAuthChannel                       = "backend.GetUserAuthorization.channel"
	statsAuthChannelRestrictedAuthorized   = "backend.GetUserAuthorization.channel.restricted.authorized"
	statsAuthChannelRestrictedUnauthorized = "backend.GetUserAuthorization.channel.restricted.unauthorized"
	statsAuthChannelUnrestricted           = "backend.GetUserAuthorization.channel.unrestricted"

	statsAuthVOD                       = "backend.GetUserAuthorization.vod"
	statsAuthVODRestrictedAuthorized   = "backend.GetUserAuthorization.vod.restricted.authorized"
	statsAuthVODRestrictedUnauthorized = "backend.GetUserAuthorization.vod.restricted.unauthorized"
	statsAuthVODUnrestricted           = "backend.GetUserAuthorization.vod.unrestricted"
)

// ProductNotFoundError is a type implementing `error` that signals to a caller that the product named in
// `VODAuthorizationRequest.Reason.ProductName was not found to exist.
type ProductNotFoundError struct {
	message string
}

func (p ProductNotFoundError) Error() string {
	return p.message
}

// NewProductNotFoundError creates a ProductNotFoundError instance.
func NewProductNotFoundError(shortname string) ProductNotFoundError {
	return ProductNotFoundError{
		message: fmt.Sprintf("No product found for shortname: %s\n", shortname),
	}
}

func (b *backendImpl) GetUserChannelAuthorization(ctx context.Context, request *models.ChannelAuthorizationRequest) (*models.ChannelAuthorization, error) {
	log := logrus.WithField("RequestID", chitin.GetTraceID(ctx)).WithFields(logrus.Fields{
		"ChannelID":      request.ChannelID,
		"UserID":         request.UserID,
		"ConsumePreview": request.ConsumePreview,
		"Origin":         request.Origin,
	})

	log.Debug("backend serving RestrictionsV2 GetUserChannelAuthorization call")

	if request.ChannelID == "" {
		msg := "ChannelID cannot be empty when calling GetUserChannelAuthorization"
		return nil, errors.New(msg)
	}

	// Additional check for origin
	if _, ok := b.config.PreviewOriginsMap[request.Origin]; !ok && request.Origin != "" {
		request.Origin = ""
	}

	b.incStats(statsAuthChannel, 1)

	restriction, err := b.GetRestriction(ctx, models.Channel{ID: request.ChannelID}, false)
	if err != nil {
		return nil, err
	}

	out := b.getChannelAuthorizationFromRestriction(ctx, request, restriction)
	if out.Error != nil {
		return nil, out.Error
	}

	return out, nil
}

func (b *backendImpl) GetUserVODAuthorization(ctx context.Context, request *models.VODAuthorizationRequest) (*models.VODAuthorization, error) {
	var out *models.VODAuthorization

	b.incStats(statsAuthVOD, 1)

	// Old Hedwig path
	// Remove after migration
	if request.Reason != nil && request.Reason.ProductName != "" {
		out = &models.VODAuthorization{
			ResourceAuthorization: models.ResourceAuthorization{},
			Request:               request,
		}
		// Assume user is unauthorized until proven otherwise.
		out.UserIsAuthorized = false

		// Currently, the intent is that Nioh only handles VOD authorization for restricted VODs.
		// An unrestricted VOD request would never make it this far. Thus, `ResourceIsRestricted` is categorically true.
		// If this design ever changes (i.e., Nioh absorbs Hedwig's responsibility and becomes the direct target of
		// `isRestricted` RPC calls), then this field should become dynamic.
		out.ResourceIsRestricted = true

		// Short-circuit for anonymous users. They can't ever currently be authorized to a restricted VOD.
		// TODO: if/when Nioh becomes the authority for whether a VOD is restricted, this should be revisited.
		if request.UserID == "" {
			return out, nil
		}

		product, err := b.clients.Subscriptions.GetProductByName(ctx, request.Reason.ProductName)
		if err != nil {
			return out, errors.Wrap(err, "failed to look up ticket product by name via Subscriptions")
		}
		if product == nil {
			return out, NewProductNotFoundError(request.Reason.ProductName)
		}

		sub, err := b.clients.TwitchVoyager.GetUserProductSubscription(ctx, request.UserID, product.Id)
		if err != nil {
			return out, errors.Wrap(err, "failed to look up user product subscription via Voyager service")
		}

		out.UserIsAuthorized = sub != nil
	} else {
		restriction, err := b.GetRestriction(ctx, models.VOD{ID: request.VideoID}, false)
		if err != nil {
			return nil, err
		}
		out = b.getVODAuthorizationFromRestriction(ctx, request, restriction)
	}

	return out, nil
}

// GetUserChannelAuthorizations returns a list of authorizations for a user for multiple channel resources.
func (b *backendImpl) GetUserChannelAuthorizations(ctx context.Context, request *models.ChannelAuthorizationsRequest) ([]*models.ChannelAuthorization, error) {
	results := make([]*models.ChannelAuthorization, len(request.ChannelIDs))

	// Preload subs statuses
	restrictionMap := b.loadChannelRestrictions(ctx, request.ChannelIDs)

	b.incStats(statsAuthChannel, int64(len(request.ChannelIDs)))

	var wg sync.WaitGroup
	var mutex sync.Mutex
	// Generate results for each channel
	// This has to be done in parallel in order for the loader to collect keys and resolve in collected batches
	for idx, channelID := range request.ChannelIDs {
		wg.Add(1)
		go func(channelID string, idx int) {
			defer wg.Done()

			request := &models.ChannelAuthorizationRequest{
				UserID:    request.UserID,
				ChannelID: channelID,
			}

			restriction, ok := restrictionMap[channelID]
			var out *models.ChannelAuthorization

			if !ok {
				out = &models.ChannelAuthorization{
					ResourceAuthorization: models.ResourceAuthorization{
						UserIsAuthorized: true,
						Error:            errors.New("failed to load restriction for channel"),
					},
					Request: request,
				}
			} else {
				out = b.getChannelAuthorizationFromRestriction(ctx, request, restriction)
			}

			mutex.Lock()
			results[idx] = out
			mutex.Unlock()
		}(channelID, idx)
	}

	wg.Wait()

	return results, nil
}

func (b *backendImpl) GetUserVODAuthorizations(ctx context.Context, request *models.VODAuthorizationsRequest) ([]*models.VODAuthorization, error) {
	results := make([]*models.VODAuthorization, len(request.VideoIDs))

	// Preload sub statuses TODO: Get Channel IDs
	restrictionMap := b.loadVODRestrictions(ctx, request.VideoIDs)
	b.incStats(statsAuthVOD, int64(len(request.VideoIDs)))

	var (
		wg    sync.WaitGroup
		mutex sync.Mutex
	)

	// Generate results for each VOD
	// This has to be done in parallel in order for the loader to collect keys and resolve in collected batches
	for i, videoID := range request.VideoIDs {
		wg.Add(1)
		go func(videoID string, idx int) {
			defer wg.Done()

			request := &models.VODAuthorizationRequest{
				UserID:  request.UserID,
				VideoID: videoID,
			}

			restriction, ok := restrictionMap[videoID]
			var out *models.VODAuthorization

			if !ok {
				out = &models.VODAuthorization{
					ResourceAuthorization: models.ResourceAuthorization{},
					Request:               request,
				}
				out.UserIsAuthorized = true
				out.Error = errors.New("failed to load restriction for channel")
			} else {
				out = b.getVODAuthorizationFromRestriction(ctx, request, restriction)
			}

			mutex.Lock()
			results[idx] = out
			mutex.Unlock()
		}(videoID, i)
	}

	wg.Wait()

	return results, nil
}

// GetAuthorizedUsersForChannelV2 returns a paginated list of authorized users for a given channel.
// If the channel is not restricted, the list is empty.
// The second returned value denotes whether restriction is detected or not.
// The third returned value is the cursor to be used in the next paging request. A nil value indicates
// all data has been retreived
// For PRODUCT type exemption, due to the limitation on Subs side, we check for channel sub instead
// of matching against product ids.
func (b *backendImpl) GetAuthorizedUsersForChannelV2(ctx context.Context, channelID string, limit int32, cursor string) ([]string, bool, string, error) {
	var data []string

	restriction, err := b.GetRestriction(ctx, models.Channel{ID: channelID}, false)
	if err != nil {
		return nil, false, "", errors.Wrapf(err, "failed to get restriction on channel %v", channelID)
	}

	var workingCursor map[models.ServiceCursorKey]models.GetAuthorizedUsersForChannelCursor

	// cursor is zero value (no nil in protobuf), init new cursor, else validate/hydrate cursor
	if cursor == "" {
		workingCursor, err = b.initGetAuthorizedUsersForChannelCursor(ctx, restriction)
	} else {
		workingCursor, err = b.decodeGetAuthorizedUsersForChannelCursor(ctx, cursor)
	}

	if err != nil {
		return nil, false, "", err
	}

	// early return if unrestricted
	if workingCursor == nil {
		return nil, false, "", nil
	}

	// range through cursor keys
CursorLoop:
	for _, callKey := range models.GetAuthorizedUsersForChannelServiceOrder {
		cursorValue, ok := workingCursor[callKey]

		// If the cursor key isn't found in the cursor data,
		// it means the restriction didn't require that particular service call.
		if !ok || cursorValue.IsComplete {
			continue
		}

		switch callKey {
		case models.VIPServiceCursor:
			cursorInfo := workingCursor[callKey]
			users, nextCursor, caseErr := b.clients.Zuma.ListVIPs(ctx, channelID, cursorInfo.Cursor, int(limit))
			if caseErr != nil {
				return nil, true, "", errors.Wrap(caseErr, "failed to get VIPs for a channel from Zuma client")
			}

			data = users

			// update cursor value
			workingCursor[callKey] = getAuthorizedUsersCursor(nextCursor)
			break CursorLoop
		case models.ModServiceCursor:
			cursorInfo := workingCursor[callKey]
			users, nextCursor, caseErr := b.clients.Zuma.ListMods(ctx, channelID, cursorInfo.Cursor, int(limit))
			if caseErr != nil {
				return nil, true, "", errors.Wrap(caseErr, "failed to get Mods for a channel from Zuma client")
			}

			data = users

			// update cursor value
			workingCursor[callKey] = getAuthorizedUsersCursor(nextCursor)
			break CursorLoop
		case models.SubServiceCursor:
			cursorInfo := workingCursor[callKey]
			productData, newCursor, err := b.getAuthorizedUsersForProductWithCursor(ctx, getAuthorizedUsersForProductWithCursorInput{
				restriction: restriction,
				channelID:   channelID,
				limit:       limit,
				prevCursor:  &cursorInfo,
			})
			if err != nil {
				return nil, true, "", err
			}
			data = productData
			workingCursor[callKey] = *newCursor
			break CursorLoop
		case models.OrgServiceCursor:
			cursorInfo := workingCursor[callKey]
			if len(restriction.Exemptions) == 0 || len(restriction.Exemptions[0].Keys) == 0 {
				// currently org only access restriction type only has one exemption for a channel
				// if no exemption found, only the channel owner is authorized
				return []string{channelID}, true, "", nil
			}
			companyID := restriction.Exemptions[0].Keys[0]
			users, nextCursor, isComplete, err := b.clients.Rbac.ListUsersByCompanyID(ctx, channelID, companyID, cursorInfo.Cursor, limit)
			if err != nil {
				return nil, true, "", err
			}
			workingCursor[callKey] = models.GetAuthorizedUsersForChannelCursor{
				Cursor:     nextCursor,
				IsComplete: isComplete,
			}
			data = users
			break CursorLoop
		}
	}

	nextCursor, err := b.encodeGetAuthorizedUsersForChannelCursor(ctx, workingCursor)
	if err != nil {
		return nil, true, "", err
	}

	// return data and nextCursor
	return data, true, nextCursor, nil
}

type getAuthorizedUsersForProductWithCursorInput struct {
	restriction *models.RestrictionV2
	channelID   string
	limit       int32
	prevCursor  *models.GetAuthorizedUsersForChannelCursor
}

func (b *backendImpl) getAuthorizedUsersForProductWithCursor(ctx context.Context, input getAuthorizedUsersForProductWithCursorInput) ([]string, *models.GetAuthorizedUsersForChannelCursor, error) {
	// do service call
	subscriberMap, newCursor, err := b.clients.TwitchVoyager.GetSubscribedUsersForChannel(ctx, input.channelID, input.limit, input.prevCursor.Cursor)
	if err != nil {
		return nil, nil, errors.Wrap(err, "failed to get subscriptions for a channel from Subscription client")
	}

	// determine if additional requests are needed
	isComplete := len(subscriberMap) < int(input.limit)

	// Filter data for tier
	data := make([]string, 0, len(subscriberMap))
	for _, exemption := range input.restriction.Exemptions {
		if exemption.ExemptionType == models.ProductExemptionType {
			productIDs := exemption.Keys
			for userID, subbedProductID := range subscriberMap {

				// This goes against the CS 101 instinct, but in Go, array search is faster when N is only a few. In our case N is 3 max.
				for _, productID := range productIDs {
					if subbedProductID == productID {
						data = append(data, userID)
					}
				}
			}
		}
	}
	return data, &models.GetAuthorizedUsersForChannelCursor{
		Cursor:     newCursor,
		IsComplete: isComplete,
	}, nil
}

func (b *backendImpl) encodeGetAuthorizedUsersForChannelCursor(ctx context.Context, cursor map[models.ServiceCursorKey]models.GetAuthorizedUsersForChannelCursor) (string, error) {
	// Check for completeness and return nil if all done
	allCallsCompleted := true
	for _, cursorData := range cursor {
		if !cursorData.IsComplete {
			allCallsCompleted = false
			break
		}
	}
	if allCallsCompleted {
		return "", nil
	}

	encodedCursorBytes, err := json.Marshal(cursor)
	if err != nil {
		return "", err
	}
	encodedCursor := base64.StdEncoding.EncodeToString(encodedCursorBytes)

	return encodedCursor, nil
}

func (b *backendImpl) decodeGetAuthorizedUsersForChannelCursor(ctx context.Context, cursor string) (map[models.ServiceCursorKey]models.GetAuthorizedUsersForChannelCursor, error) {
	cursorBytes, err := base64.StdEncoding.DecodeString(cursor)
	if err != nil {
		return nil, err
	}
	unmarshalledCursor := map[models.ServiceCursorKey]models.GetAuthorizedUsersForChannelCursor{}
	err = json.Unmarshal(cursorBytes, &unmarshalledCursor)
	if err != nil {
		return nil, err
	}

	return unmarshalledCursor, nil
}

func (b *backendImpl) initGetAuthorizedUsersForChannelCursor(ctx context.Context, restriction *models.RestrictionV2) (map[models.ServiceCursorKey]models.GetAuthorizedUsersForChannelCursor, error) {
	cursor := map[models.ServiceCursorKey]models.GetAuthorizedUsersForChannelCursor{}

	if restriction == nil {
		return nil, nil
	}

	// Detect supported exemptions
	for _, exemption := range restriction.Exemptions {
		switch exemption.ExemptionType {
		case models.AllExemptionType:
			return nil, nil
		case models.ProductExemptionType:
			initialOffset := ""
			cursor[models.SubServiceCursor] = models.GetAuthorizedUsersForChannelCursor{Cursor: initialOffset, IsComplete: false}
		case models.ChannelVIPExemptionType:
			initialCursor := ""
			cursor[models.VIPServiceCursor] = models.GetAuthorizedUsersForChannelCursor{Cursor: initialCursor, IsComplete: false}
		case models.ChannelModeratorExemptionType:
			initialCursor := ""
			cursor[models.ModServiceCursor] = models.GetAuthorizedUsersForChannelCursor{Cursor: initialCursor, IsComplete: false}
		case models.OrganizationMemberExemptionType:
			initialOffset := "0"
			cursor[models.OrgServiceCursor] = models.GetAuthorizedUsersForChannelCursor{Cursor: initialOffset, IsComplete: false}
		}
	}

	return cursor, nil
}

func (b *backendImpl) getChannelAuthorizationFromRestriction(ctx context.Context, request *models.ChannelAuthorizationRequest, restriction *models.RestrictionV2) *models.ChannelAuthorization {
	resourceAuthorization := b.getAuthorizationFromRestriction(ctx, &getAuthorizationFromRestrictionParams{
		userID:         request.UserID,
		resource:       getRestrictableResourceWithRequester(request.ChannelID, models.ResourceTypeChannel, request.Origin),
		restriction:    restriction,
		consumePreview: request.ConsumePreview,
		origin:         request.Origin,
	})

	// User is always authorized to their own channel regardless of exemption status.
	// TODO: model this as an actual exemption type: "SELF".
	resourceAuthorization.UserIsAuthorized = resourceAuthorization.UserIsAuthorized || request.ChannelID == request.UserID

	out := &models.ChannelAuthorization{
		ResourceAuthorization: resourceAuthorization,
		Request:               request,
	}
	if !out.ResourceIsRestricted {
		b.incStats(statsAuthChannelUnrestricted, 1)
	} else if out.UserIsAuthorized {
		b.incStats(statsAuthChannelRestrictedAuthorized, 1)
	} else {
		b.incStats(statsAuthChannelRestrictedUnauthorized, 1)
	}

	return out
}

func (b *backendImpl) getVODAuthorizationFromRestriction(ctx context.Context, request *models.VODAuthorizationRequest, restriction *models.RestrictionV2) *models.VODAuthorization {
	resourceAuthorization := b.getAuthorizationFromRestriction(ctx, &getAuthorizationFromRestrictionParams{
		userID:         request.UserID,
		resource:       getRestrictableResourceWithRequester(request.VideoID, models.ResourceTypeVOD, request.Origin),
		restriction:    restriction,
		consumePreview: request.ConsumePreview,
	})

	out := &models.VODAuthorization{
		ResourceAuthorization: resourceAuthorization,
		Request:               request,
	}

	if !out.ResourceIsRestricted {
		b.incStats(statsAuthVODUnrestricted, 1)
	} else if out.UserIsAuthorized {
		b.incStats(statsAuthVODRestrictedAuthorized, 1)
	} else {
		b.incStats(statsAuthVODRestrictedUnauthorized, 1)
	}

	return out
}

type getAuthorizationFromRestrictionParams struct {
	userID         string
	resource       models.RestrictableResourceWithRequester
	restriction    *models.RestrictionV2
	consumePreview bool
	origin         string
}

// getAuthorizationFromRestriction returns the authorization result given the params that include user/resource/restriction data
func (b *backendImpl) getAuthorizationFromRestriction(ctx context.Context, params *getAuthorizationFromRestrictionParams) models.ResourceAuthorization {
	out := models.ResourceAuthorization{
		UserIsAuthorized: true,
	}

	if params.restriction == nil {
		// Resource is unrestricted; user is always authorized.
		// These field writes are redundant with initialization of the struct but are retained to protect against bad refactors.
		out.UserIsAuthorized = true
		out.ResourceIsRestricted = false

		return out
	}

	out.ResourceIsRestricted = true
	out.RestrictionType = params.restriction.RestrictionType

	userExemptionStatus, err := b.exemptionChecker.GetUserExemptionStatus(ctx, params.userID, params.restriction.Exemptions)
	if err != nil {
		out.Error = errors.Wrap(err, "unexpected error from GetUserExemptionStatus in GetUserChannelAuthorization")
		return out
	}

	b.sendSpadeEventsForAppliedExemptions(userExemptionStatus.ApplicableExemptions, params.resource, params.userID)

	// If user is currently ineligible and previews are available for this restriction and we've been
	// told to consume it, consume the preview and conditionally add it to the ApplicableExemptions
	eligibleForPreview := false
	for _, exemption := range params.restriction.Exemptions {
		if exemption.ExemptionType == models.PreviewExemptionType {
			eligibleForPreview = true
			break
		}
	}

	if len(userExemptionStatus.ApplicableExemptions) == 0 &&
		params.consumePreview &&
		eligibleForPreview {
		previewRecord, err := b.ConsumePreview(ctx, params.userID, params.resource)
		if err != nil {
			out.Error = err
		}
		if previewRecord != nil {
			userExemptionStatus.ApplicableExemptions = append(userExemptionStatus.ApplicableExemptions, &models.AppliedExemption{
				Exemption: b.exemptionForPreview(ctx, previewRecord),
			})
			b.sendSpadeEventForPreviewConsumption(previewRecord, params.resource)
		}
	}

	out.AppliedExemptions = userExemptionStatus.ApplicableExemptions

	// The presence of any applicable exemption indicates that the user is exempt from the restriction and therefore authorized.
	out.UserIsAuthorized = len(userExemptionStatus.ApplicableExemptions) > 0

	return out
}

// loadChannelRestrictions loads restrictions for multiple channels then pre-load necessary data from the dataloader
// The returned map can have a nil value if the restriction is not found. No key in the map means there has been an error retrieving it from DAO.
func (b *backendImpl) loadChannelRestrictions(ctx context.Context, channelIDs []string) map[string]*models.RestrictionV2 {
	return b.loadRestrictionsByResources(ctx, channelIDs, models.ResourceTypeChannel)
}

func (b *backendImpl) loadVODRestrictions(ctx context.Context, videoIDs []string) map[string]*models.RestrictionV2 {
	return b.loadRestrictionsByResources(ctx, videoIDs, models.ResourceTypeVOD)
}

func (b *backendImpl) loadRestrictionsByResources(ctx context.Context, resourceIDs []string, resourceType models.ResourceType) map[string]*models.RestrictionV2 {
	log := logrus.WithField("RequestID", chitin.GetTraceID(ctx))

	restrictionsByResourceIDs := map[string]*models.RestrictionV2{} // Storage for loaded restrictions

	var wg sync.WaitGroup
	var mutex sync.Mutex
	for _, resourceID := range resourceIDs {
		wg.Add(1)
		go func(resourceID string) {
			defer wg.Done()
			resource := getRestrictableResource(resourceID, resourceType)
			restriction, err := b.GetRestriction(ctx, resource, false)
			if err != nil {
				log.WithError(err).Warn("unexpected error while getting restriction data in backend")
			} else {
				mutex.Lock()
				restrictionsByResourceIDs[resourceID] = restriction
				mutex.Unlock()
			}
		}(resourceID)

	}
	wg.Wait()

	return restrictionsByResourceIDs
}

// Spade Event helpers

// sendSpadeEventForPreviewConsumption sends a spade event for a preview consumption
func (b *backendImpl) sendSpadeEventForPreviewConsumption(previewRecord *models.Preview, resource models.RestrictableResource) {
	spadeEvent := models.UserAuthorizationEvent{
		ExemptionType:          models.PreviewExemptionType,
		StartTime:              previewRecord.CreatedAt,
		ExemptionLengthSeconds: int(previewRecord.CreatedAt.Sub(previewRecord.ContentExpiresAt).Seconds()),
		EndTime:                previewRecord.ContentExpiresAt,
		UserID:                 previewRecord.UserID,
		ResetTime:              previewRecord.ResetAt,
	}
	if resource.GetType() == models.ResourceTypeVOD {
		spadeEvent.VideoID = resource.GetIdentifier()
	} else {
		spadeEvent.ChannelID = resource.GetIdentifier()
	}
	b.sendUserAuthoriazationSpadeEvents(models.SpadeEvents{&spadeEvent})
}

// sendSpadeEventsForAppliedExemptions sends spade events for a list of exemptions that are applied to the user
func (b *backendImpl) sendSpadeEventsForAppliedExemptions(appliedExemptions []*models.AppliedExemption, resource models.RestrictableResource, userID string) {
	if len(appliedExemptions) == 0 {
		return
	}

	spadeEvents := models.SpadeEvents{}
	for _, appliedExemption := range appliedExemptions {
		spadeEvent := models.UserAuthorizationEvent{
			ExemptionType: appliedExemption.ExemptionType,
			StartTime:     appliedExemption.ActiveStartDate,
			EndTime:       appliedExemption.ActiveEndDate,
			UserID:        userID,
			MatchedKey:    appliedExemption.MatchedKey,
		}
		if resource.GetType() == models.ResourceTypeVOD {
			spadeEvent.VideoID = resource.GetIdentifier()
		} else {
			spadeEvent.ChannelID = resource.GetIdentifier()
		}
		spadeEvents = append(spadeEvents, spadeEvent)
	}
	b.sendUserAuthoriazationSpadeEvents(spadeEvents)
}

func (b *backendImpl) sendUserAuthoriazationSpadeEvents(spadeEvents models.SpadeEvents) {
	// Spade tracking
	if len(spadeEvents) > 0 {
		go func() {
			err := b.clients.Spade.TrackEvents(context.TODO(), spadeEvents.ToNativeSpadeEvents()...)
			if err != nil {
				logrus.WithFields(logrus.Fields{
					"error": err,
				}).Error("failed to send Spade event for getChannelAuthorizationFromRestriction")
			}
		}()
	}
}

func getRestrictableResource(resourceID string, resourceType models.ResourceType) models.RestrictableResource {
	if resourceType == models.ResourceTypeVOD {
		return &models.VOD{ID: resourceID}
	}
	return &models.Channel{ID: resourceID}
}

// Helper to build a common GetAuthorizedUsersForChannelCursor struct
func getAuthorizedUsersCursor(nextCursor string) models.GetAuthorizedUsersForChannelCursor {
	isComplete := nextCursor == ""
	return models.GetAuthorizedUsersForChannelCursor{
		Cursor:     nextCursor,
		IsComplete: isComplete,
	}
}

// Helper create a resource with an requester origin. This should be used when dealing with free previews
func getRestrictableResourceWithRequester(resourceID string, resourceType models.ResourceType, origin string) models.RestrictableResourceWithRequester {
	// Origin may be empty and is an acceptable usecase
	if resourceType == models.ResourceTypeVOD {
		return &models.VOD{ID: resourceID, RequesterOrigin: origin}
	}
	return &models.Channel{ID: resourceID, RequesterOrigin: origin}
}
