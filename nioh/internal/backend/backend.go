package backend

import (
	"context"

	"code.justin.tv/commerce/nioh/internal/orch/autotag"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/nioh/config"
	"code.justin.tv/commerce/nioh/internal/clients"
	"code.justin.tv/commerce/nioh/internal/models"
	"code.justin.tv/commerce/nioh/pkg/dynamodb/dao"
	"code.justin.tv/commerce/nioh/pkg/errorlogger"
	"code.justin.tv/commerce/nioh/pkg/exemptions"
	"code.justin.tv/commerce/nioh/rpc/nioh"
	"github.com/cactus/go-statsd-client/statsd"
)

// Backend represents all actions in this layer.
type Backend interface {
	GetRecord(ctx context.Context, r *nioh.GetRecordRequest) (*nioh.Record, error)
	TestConnections(ctx context.Context) *TestConnectionsResult

	CreateContentAttributes(context.Context, *ContentAttributesBulkParams) (*ContentAttributesBulkResults, error)
	GetContentAttributesForChannel(context.Context, string) ([]*models.ContentAttribute, error)
	DeleteContentAttributes(context.Context, *ContentAttributesBulkParams) (*ContentAttributesBulkResults, error)
	UpdateContentAttributes(context.Context, *ContentAttributesBulkParams) (*ContentAttributesBulkResults, error)
	CreateContentAttributeImageUploadConfig(ctx context.Context, channelID string) (*models.ContentAttributeImageUploadConfig, error)
	GetContentAttributesForChanlet(ctx context.Context, chanletID string) ([]*models.ContentAttribute, error)

	GetUserVODAuthorization(context.Context, *models.VODAuthorizationRequest) (*models.VODAuthorization, error)
	GetUserVODAuthorizations(context.Context, *models.VODAuthorizationsRequest) ([]*models.VODAuthorization, error)
	GetUserChannelAuthorization(ctx context.Context, request *models.ChannelAuthorizationRequest) (*models.ChannelAuthorization, error)
	GetUserChannelAuthorizations(ctx context.Context, request *models.ChannelAuthorizationsRequest) ([]*models.ChannelAuthorization, error)
	GetAuthorizedUsersForChannelV2(ctx context.Context, channelID string, limit int32, cursor string) ([]string, bool, string, error)

	GetChanlets(ctx context.Context, channelID string, options GetChanletsOptions) ([]*models.Chanlet, error)
	ListAllChanlets(ctx context.Context, cursor string) ([]*models.Chanlet, string, error)
	CreateChanlet(ctx context.Context, ownerID string) (*models.Chanlet, error)
	AddOrReplaceChanlet(ctx context.Context, chanlet *models.Chanlet) error
	CreateOwnerChanlet(ctx context.Context, ownerChannelID string) error
	UpdateChanletContentAttributes(ctx context.Context, input *UpdateChanletInput) (*UpdateChanletOutput, error)
	GetChanletStreamKey(ctx context.Context, ownerChannelID string, chanletID string) (string, error)
	GetChanletByID(ctx context.Context, chanletID string) (*models.Chanlet, error)
	ArchiveChanlet(ctx context.Context, chanletID string) (*models.Chanlet, error)

	GetRestriction(ctx context.Context, resource models.RestrictableResource, isPlayback bool) (*models.RestrictionV2, error)
	SetRestriction(ctx context.Context, resource models.RestrictableResource, restriction models.RestrictionV2) (*models.RestrictionV2, error)
	DeleteRestriction(ctx context.Context, resource models.RestrictableResource) (*models.RestrictionV2, error)

	CreateChannelRelation(ctx context.Context, channelID string, relatedChannelID string, relationType models.Relation) (*models.ChannelRelation, error)
	DeleteChannelRelation(ctx context.Context, channelID string, relatedChannelID string) error
	GetChannelRelationsByChannelID(ctx context.Context, channelID string) ([]*models.ChannelRelation, error)
	GetChannelRelationsByRelatedChannelID(ctx context.Context, relatedChannelID string) ([]*models.ChannelRelation, error)

	GetPreview(ctx context.Context, userID string, resource models.RestrictableResourceWithRequester) (*models.Preview, error)
	SetPreview(ctx context.Context, userID string, resource models.RestrictableResourceWithRequester) (*models.Preview, error)
	DeletePreview(ctx context.Context, userID string, resource models.RestrictableResourceWithRequester) (*models.Preview, error)
	ConsumePreview(ctx context.Context, userID string, resource models.RestrictableResourceWithRequester) (*models.Preview, error)
}

type backendImpl struct {
	config           *config.Configuration
	errorlogger      errorlogger.ErrorLogger
	stats            statsd.Statter
	clients          *clients.Clients
	daos             *dao.DAOs
	clock            models.IClock
	exemptionChecker exemptions.IExemptionChecker
	autotagger       autotag.Autotagger
}

var _ Backend = &backendImpl{}

// NewBackendInput gathers the backend's dependencies to allow instantiation when passed to NewBackend.
type NewBackendInput struct {
	Config           *config.Configuration
	Clients          *clients.Clients
	Stats            statsd.Statter
	Errorlogger      errorlogger.ErrorLogger
	DAOs             *dao.DAOs
	Clock            models.IClock
	ExemptionChecker exemptions.IExemptionChecker
	Autotagger       autotag.Autotagger
}

// NewBackend creates a new backend instance
func NewBackend(input *NewBackendInput) Backend {
	backend := &backendImpl{
		config:           input.Config,
		errorlogger:      input.Errorlogger,
		stats:            input.Stats,
		clients:          input.Clients,
		daos:             input.DAOs,
		clock:            input.Clock,
		exemptionChecker: input.ExemptionChecker,
		autotagger:       input.Autotagger,
	}

	return backend
}

func (b *backendImpl) incStats(name string, amount int64) {
	if err := b.stats.Inc(name, amount, 1.0); err != nil {
		logrus.Debug("failed to send backend metrics to statsd")
	}
}
