package errors

import (
	"fmt"
	"net/http"
)

// WithStatusCode an interface for errors returned by the API.
type WithStatusCode interface {
	StatusCode() int
	Error() string
}

// WithBody is an interface for errors that have a custom JSON body.
type WithBody interface {
	Body() map[string]interface{}
}

// Unauthorized is returned when a user is not authorized to perform an action.
type Unauthorized struct {
	Err error
}

// Error returns the string representation of an Unauthorized.
func (e *Unauthorized) Error() string {
	return fmt.Sprintf("You are not authorized: %s", e.Err.Error())
}

// StatusCode returns the http status code this error maps to.
func (e *Unauthorized) StatusCode() int {
	return http.StatusUnauthorized
}

// InvalidJWT is returned when the JWT can't be parsed.
type InvalidJWT struct {
	Message string
}

// Error returns the message describing the parse error.
func (e *InvalidJWT) Error() string {
	return fmt.Sprintf("Invalid JWT: %s", e.Message)
}

// StatusCode returns the http status code this error maps to.
func (e *InvalidJWT) StatusCode() int {
	return http.StatusInternalServerError
}

// GenericInternal is a generic, internal error.
type GenericInternal struct{}

// Error returns the string representation of a GenericInternal.
func (e *GenericInternal) Error() string {
	return fmt.Sprintf("Generic Internal Error")
}

// StatusCode returns the http status code this error maps to.
func (e *GenericInternal) StatusCode() int {
	return http.StatusInternalServerError
}

// RecordIDNotFound is returned when a record cannot be found.
type RecordIDNotFound struct {
	RecordID string
}

// Error returns the string representation of an RecordIDNotFound.
func (e *RecordIDNotFound) Error() string {
	return fmt.Sprintf("Record not found")
}

// StatusCode returns the http status code this error maps to.
func (e *RecordIDNotFound) StatusCode() int {
	return http.StatusNotFound
}

// CursorEncode is returned when we fail to encode a cursor into json
type CursorEncode struct {
	Err error
}

// Error returns the invalid cursor supplied
func (e *CursorEncode) Error() string {
	return fmt.Sprintf("Unable to encode collection cursor: %s", e.Err.Error())
}

// StatusCode returns the http status code this error maps to
func (e *CursorEncode) StatusCode() int {
	return http.StatusInternalServerError
}

// CursorDecode is returned when we fail to decode a cursor from json
type CursorDecode struct {
	Err error
}

// Error returns the invalid cursor supplied
func (e *CursorDecode) Error() string {
	return fmt.Sprintf("Unable to decode collection cursor: %s", e.Err.Error())
}

// StatusCode returns the http status code this error maps to
func (e *CursorDecode) StatusCode() int {
	return http.StatusInternalServerError
}

// UserConcurrentModification when there was an error updating collections due to concurrent modification
type UserConcurrentModification struct {
	Err error
}

// Error returns the unauthorized error message
func (e *UserConcurrentModification) Error() string {
	return fmt.Sprintf("Unable to update collection: %s", e.Err.Error())
}

// StatusCode returns the http status code this error maps to
func (e *UserConcurrentModification) StatusCode() int {
	return http.StatusConflict
}

// BackendConnection when the ddb connection is not working
type BackendConnection struct {
	Err error
	Ddb string
}

// Error returns the error message
func (e *BackendConnection) Error() string {
	return fmt.Sprintf("Error in ddb: %s. Error: %s", e.Ddb, e.Err.Error())
}

// StatusCode returns the http status code this error maps to
func (e *BackendConnection) StatusCode() int {
	return http.StatusInternalServerError
}
