package errors

import (
	"fmt"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

// TestErrors is pretty simple. It goes through and constructs each error and
// calls its methods to make sure nothing will crash in operation.
// Why it doesn't use something fancier: https://xkcd.com/1205/
func TestErrors(t *testing.T) {
	Convey("when constructing and calling errors", t, func() {

		func() {
			err := RecordIDNotFound{RecordID: "123"}
			_ = err.StatusCode()
			_ = err.Error()
		}()

		func() {
			err := Unauthorized{Err: fmt.Errorf("auth error")}
			_ = err.StatusCode()
			_ = err.Error()
		}()

		func() {
			err := InvalidJWT{Message: "123"}
			_ = err.StatusCode()
			_ = err.Error()
		}()

		func() {
			err := GenericInternal{}
			_ = err.StatusCode()
			_ = err.Error()
		}()

		func() {
			err := CursorEncode{Err: fmt.Errorf("error")}
			_ = err.StatusCode()
			_ = err.Error()
		}()

		func() {
			err := CursorDecode{Err: fmt.Errorf("error")}
			_ = err.StatusCode()
			_ = err.Error()
		}()

		func() {
			err := UserConcurrentModification{Err: fmt.Errorf("error")}
			_ = err.StatusCode()
			_ = err.Error()
		}()

		func() {
			err := BackendConnection{Err: fmt.Errorf("error"), Ddb: "ddb"}
			_ = err.StatusCode()
			_ = err.Error()
		}()
	})
}
