package main

import (
	"context"
	"flag"
	"fmt"
	"os"

	niohConfig "code.justin.tv/commerce/nioh/config"
	"code.justin.tv/commerce/nioh/internal/models"
	"code.justin.tv/commerce/nioh/mocks"
	"code.justin.tv/commerce/nioh/pkg/dynamodb/dao"
	"code.justin.tv/commerce/nioh/pkg/helpers"
	"github.com/stretchr/testify/mock"
)

// Build: GOOS=linux go build -o set-restriction tools/set-restriction/main.go
// Upload: TC=teleport-remote-twitch-nioh-prod scp ./set-restriction 10.204.74.4:set-restriction
// SSH into proxy and make sure to have .aws/credentials file with aws credentials
// Usage: ENVIRONMENT=production ./set-restriction -channelID=186243532 -on=true -productID=1189177
// This just sets a restriction in prod with given channelID/productID through DAX so cache is updated.
// Also, this bypasses having to take stream offline / having to be partner / etc
func main() {
	channelIDF := flag.String("channelID", "", "Channel ID you want your restriction on/off on")
	onF := flag.Bool("on", true, "On or off, default: on")
	productIDF := flag.String("productID", "1000000", "Product ID in your product exemption")
	flag.Parse()

	channelID := *channelIDF
	on := *onF
	productID := *productIDF

	if channelID == "" {
		fmt.Println("channelID is required")
		os.Exit(1)
	}

	statter := new(mocks.Statter)
	statter.On("Inc", mock.Anything, mock.Anything, mock.Anything).Return(nil)
	statter.On("TimingDuration", mock.Anything, mock.Anything, mock.Anything).Return(nil)
	cfg := niohConfig.Configuration{
		AWSRegion:          "us-west-2",
		DAXClusterEndpoint: "production-dax-v3.7vw9rj.clustercfg.dax.usw2.cache.amazonaws.com:8111", // change if you want different env
	}
	clock := &models.RealClock{}

	cm := helpers.MakeCircuitManager(statter)
	daos, err := dao.SetupDAOs(cfg, cm, true, statter)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	resource := &models.Channel{ID: channelID}
	ctx := context.Background()
	if on {
		exemptions := []*models.Exemption{
			{
				ExemptionType:   models.ProductExemptionType,
				ActiveStartDate: clock.Now(),
				ActiveEndDate:   models.IndefiniteEndDate,
				Keys:            []string{productID},
			},
		}
		restriction := &models.RestrictionV2{
			ResourceKey:     resource.GetResourceKey(),
			RestrictionType: models.SubOnlyLiveRestrictionType,
			Exemptions:      exemptions,
		}
		if err := daos.RestrictionDAO.SetRestriction(ctx, restriction); err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
	} else {
		restriction, err := daos.RestrictionDAO.GetRestriction(ctx, resource.GetResourceKey())
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
		if restriction == nil {
			fmt.Println("there wasn't any restriction to remove.")
			os.Exit(0)
		}
		if err := daos.RestrictionDAO.DeleteRestriction(ctx, resource.GetResourceKey()); err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
	}

	fmt.Println("Done")
	os.Exit(0)
}
