package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
)

type UsersServiceChannel struct {
	DirectoryHidden bool `json:"directory_hidden"`
	ID              int  `json:"id"`
}

type UsersServiceJSON struct {
	Results []*UsersServiceChannel `json:"results"`
}

const usersServiceURL = "http://users-service.prod.us-west2.twitch.tv/channels"

// This little tool invokes a couple of Users Service calls to check and set DirectoryHidden for a given channel
// This is a stand-alone tool and does not require any twitch or 3rd party dependency. However, it should be run inside of EC2 that can access Users Service VPN.
// Run me inside an EC2 by copying the compiled binary to the nioh proxy ec2 instance. See below for commands.

// Build: GOOS=linux go build -o set-directory-hidden tools/set-directory-hidden/main.go
// Upload: TC=teleport-remote-twitch-nioh-prod scp ./set-directory-hidden 10.204.74.4:set-directory-hidden
// Usage: ./set-directory-hidden chanlet_1234235345
func main() {
	argsWithoutProg := os.Args[1:]

	if len(argsWithoutProg) == 0 {
		fmt.Println("Channel name argument missing.")
		os.Exit(1)
	}

	req, _ := http.NewRequest("GET", usersServiceURL+"?name="+argsWithoutProg[0], nil)

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	defer res.Body.Close()

	body, _ := ioutil.ReadAll(res.Body)

	var jsonRes UsersServiceJSON
	err = json.Unmarshal(body, &jsonRes)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	if len(jsonRes.Results) == 0 {
		fmt.Println("No result for that channel name")
		os.Exit(1)
	}

	channel := jsonRes.Results[0]

	if channel.DirectoryHidden {
		fmt.Println("Already hidden")
		os.Exit(1)
	}

	channel.DirectoryHidden = true
	payload, _ := json.Marshal(channel)
	patchReq, _ := http.NewRequest("PATCH", usersServiceURL+"?name="+argsWithoutProg[0], bytes.NewReader(payload))

	patchRes, err := http.DefaultClient.Do(patchReq)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	defer patchRes.Body.Close()

	fmt.Println("Done")
	os.Exit(0)
}
