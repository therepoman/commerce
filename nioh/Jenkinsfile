import java.time.format.DateTimeFormatter
import java.time.LocalDateTime
import java.time.ZoneOffset
import groovy.json.JsonOutput

// Definition for the https://jenkins-og.xarth.tv/job/commerce/job/nioh/ job

AWS_REGION = "us-west-2"
MASTER_BRANCH = "master"
DEV_CREDENTIALS_ID = "twitch-nioh-dev"
PROD_CREDENTIALS_ID = "twitch-nioh-aws"
DEV_ACCOUNT_ID = "239530023759"
PROD_ACCOUNT_ID = "483600360996"

def gitShortCommitSha() {
  return sh(script: "git rev-parse --short HEAD", returnStdout: true).trim()
}

def gitShortCommitMsg() {
  return sh(script: "git log --pretty=oneline --abbrev-commit -1 --no-decorate", returnStdout: true).trim()
}

// Returns the formatted commit author name and email.
def commitAuthorName(sha) {
  return sh(script: "git log -1 --format='%aN (%ae)' ${sha}", returnStdout: true).trim()
}

// Returns a GitHub Enterprise link to the commit in question.
def commitLink(sha) {
  return "https://git.xarth.tv/commerce/nioh/commit/${sha}"
}

// Returns the title of the commit.
def commitTitle(sha) {
  return sh(script: "git log -1 --format='%s' ${sha}", returnStdout: true).trim()
}

// Formats a deploy failure message.
def failMessage(sha) {
  return "Pipeline failed to deploy commit `${sha}` to Nioh."
}

// Formats a deploy success message.
def successMessage(sha, env) {
  return "Pipeline deployed commit `${sha}` to Nioh *${env}*."
}

// Formats a rollback message.
def rollbackMessage(sha, env) {
  return "Rolling back to `${sha}` to Nioh *${env}*."
}

// Formats a rollback complete message.
def rollbackCompleteMessage(sha, env) {
  return "Rollback deployed commit `${sha}` to Nioh *${env}*."
}

// Returns the docker tag of nioh running in canary. Unclear how this works during another deployment.
def niohInCanary() {
  withAWS(credentials: PROD_CREDENTIALS_ID, region: AWS_REGION) {
    return sh(script: "aws ecs describe-task-definition --task-definition \$(aws ecs describe-services --cluster nioh-production --services nioh-canary-ec2 | jq -r '.services[0].taskDefinition') | jq -r '.taskDefinition.containerDefinitions[] | select (.name | contains(\"nioh\")) | .image ' | sed 's/.*://'", returnStdout: true).trim()
  }
}

def runInTestBuild(accountID, command) {
  return sh("docker run ${accountID}.dkr.ecr.us-west-2.amazonaws.com/nioh:build-${GIT_COMMIT} ${command}")
}

def runIntegrationTest(accountID, env) {
  return sh("""
    docker run \
      -e AWS_ACCESS_KEY=${AWS_ACCESS_KEY_ID} \
      -e AWS_SECRET_KEY=${AWS_SECRET_ACCESS_KEY} \
      docker.pkgs.xarth.tv/subs/buddy:0.1.1 \
        -task.name="nioh-tester-${GIT_COMMIT}" \
        -task.role="arn:aws:iam::${accountID}:role/niohInstanceProfile" \
        -task.image="${accountID}.dkr.ecr.us-west-2.amazonaws.com/nioh:build-${GIT_COMMIT}" \
        -task.entrypoint="/integration-tester -test.v -test.failfast -test.parallel 1 -test.timeout 10m" \
        -task.envvars='[ENVIRONMENT=${env},ECS=true,TESTING=true]' \
        -task.cpu=2048 \
        -task.memory=2048;

    docker run \
      -e AWS_ACCESS_KEY=${AWS_ACCESS_KEY_ID} \
      -e AWS_SECRET_KEY=${AWS_SECRET_ACCESS_KEY} \
      docker.pkgs.xarth.tv/subs/ecs-task-runner:latest \
        -task="nioh-tester-${GIT_COMMIT}" \
        -cluster=nioh-${env} \
        -region=us-west-2 \
        -group="nioh-tester-${GIT_COMMIT}";
  """)
}

def checkAlarmsForEnvironment(env) {
  return sh("aws cloudwatch describe-alarms --alarm-name-prefix ${env}-nioh --state-value ALARM | jq -e '.MetricAlarms | length == 0'")
}

// Sends a Slack message to the deployment channel.
def message(payload) {
  try {
    slackSend(
      attachments: JsonOutput.toJson(payload),
      channel: '#nioh',
      tokenCredentialId: 'samus-slack-api-token'
    )
  } catch (Exception e) {
    println e
  }
}

properties([
  pipelineTriggers([
    [$class: "tv.justin.jenkins.twitchgithub.GitHubPushTrigger"]
  ])
])


pipeline {
  agent any
  options {
    disableConcurrentBuilds()
    timeout(time: 1, unit: 'HOURS')
    ansiColor('xterm')
    timestamps()
  }

  stages {
    stage('Build') {
      steps {
        withAWS(credentials: DEV_CREDENTIALS_ID, region: AWS_REGION) {
          sh("./scripts/build_docker.sh ${DEV_ACCOUNT_ID}")
        }
        withAWS(credentials: PROD_CREDENTIALS_ID, region: AWS_REGION) {
          // Only push prod artifacts in master
          script {
            if( env.BRANCH_NAME == MASTER_BRANCH ){
              sh("./scripts/build_docker.sh ${PROD_ACCOUNT_ID}")
            }
          }
        }
      }
    }
    stage('Test:') {
      parallel {
        stage(':Unit') {
          steps {
            withAWS(credentials: DEV_CREDENTIALS_ID, region: AWS_REGION) {
              runInTestBuild(DEV_ACCOUNT_ID, "make test")
            }
          }
        }
        stage(':Lint') {
          steps {
            withAWS(credentials: PROD_CREDENTIALS_ID, region: AWS_REGION) {
              runInTestBuild(DEV_ACCOUNT_ID, "make quality")
            }
          }
        }
      }
    }
    stage('Beta Integration Test') {
      when {
        branch MASTER_BRANCH
      }
      steps {
        withAWS(credentials: DEV_CREDENTIALS_ID, region: AWS_REGION) {
          retry (3) {
            runIntegrationTest(DEV_ACCOUNT_ID, "beta")
          }
        }
      }
    }
    stage('Deploy to Beta') {
      when {
        branch MASTER_BRANCH
      }
      steps {
        withAWS(credentials: DEV_CREDENTIALS_ID, region: AWS_REGION) {
          sh("./scripts/deploy.sh ${DEV_ACCOUNT_ID} beta ${GIT_COMMIT}")
        }
      }
    }
    stage('Canary Integration Test') {
      when {
        branch MASTER_BRANCH
      }
      steps {
        withAWS(credentials: PROD_CREDENTIALS_ID, region: AWS_REGION) {
          retry (3) {
            runIntegrationTest(PROD_ACCOUNT_ID, "production")
          }
        }
      }
    }
    stage('Deploy to Canary') {
      when {
        branch MASTER_BRANCH
      }
      steps {
        script {
          niohCanary = niohInCanary()
        }
        withAWS(credentials: PROD_CREDENTIALS_ID, region: AWS_REGION) {
          sh("./scripts/deploy.sh ${PROD_ACCOUNT_ID} canary ${GIT_COMMIT}")
        }
      }
      post {
        success {
          script {
            def commitSHA = gitShortCommitSha()
            message([[
              color: 'good',
              author_name: commitAuthorName(commitSHA),
              title: commitTitle(commitSHA),
              title_link: commitLink(commitSHA),
              text: successMessage(commitSHA, "canary"),
              actions: [
                [
                  type: 'button',
                  text: 'Dashboard',
                  url: 'https://grafana.internal.justin.tv/dashboard/db/nioh'
                ]
              ]
            ]])
          }
        }
      }
    }
    stage('Bake') {
      when {
        branch MASTER_BRANCH
      }
      steps {
        sleep(time:15, unit:"MINUTES")
      }
    }
    stage('Health Check') {
      when {
        branch MASTER_BRANCH
      }
      steps {
        withAWS(credentials: PROD_CREDENTIALS_ID, region: AWS_REGION) {
          checkAlarmsForEnvironment("canary")
        }
      }
      post {
        failure {
          script {
            message([[
              author_name: commitAuthorName(niohCanary),
              title: commitTitle(niohCanary),
              title_link: commitLink(niohCanary),
              text: rollbackMessage(niohCanary, "canary"),
              actions: [
                [
                  type: 'button',
                  text: 'Jenkins Job',
                  url: 'https://jenkins-og.xarth.tv/job/commerce/job/nioh/'
                ]
              ]
            ]])
          }
          withAWS(credentials: PROD_CREDENTIALS_ID, region: AWS_REGION) {
            sh("./scripts/deploy.sh ${PROD_ACCOUNT_ID} canary ${niohCanary}")
          }
          script {
            message([[
              color: 'good',
              author_name: commitAuthorName(niohCanary),
              title: commitTitle(niohCanary),
              title_link: commitLink(niohCanary),
              text: rollbackCompleteMessage(niohCanary, "canary"),
              actions: [
                [
                  type: 'button',
                  text: 'Dashboard',
                  url: 'https://grafana.internal.justin.tv/dashboard/db/nioh'
                ]
              ]
            ]])
          }
        }
      }
    }
    stage('Deploy to Prod') {
      when {
        branch MASTER_BRANCH
      }
      steps {
        withAWS(credentials: PROD_CREDENTIALS_ID, region: AWS_REGION) {
          sh("./scripts/deploy.sh ${PROD_ACCOUNT_ID} production ${GIT_COMMIT}")
        }
      }
      post {
        success {
          script {
            def commitSHA = gitShortCommitSha()
            message([[
              color: 'good',
              author_name: commitAuthorName(commitSHA),
              title: commitTitle(commitSHA),
              title_link: commitLink(commitSHA),
              text: successMessage(commitSHA, "production"),
              actions: [
                [
                  type: 'button',
                  text: 'Dashboard',
                  url: 'https://grafana.internal.justin.tv/dashboard/db/nioh'
                ]
              ]
            ]])
          }
        }
      }
    }
  }

  post {
    failure {
      script {
        if( env.BRANCH_NAME == MASTER_BRANCH ){
          def commitSHA = gitShortCommitSha()
          message([[
            color: 'danger',
            author_name: commitAuthorName(commitSHA),
            title: commitTitle(commitSHA),
            title_link: commitLink(commitSHA),
            text: failMessage(commitSHA),
            actions: [[
              type: 'button',
              text: 'Build logs',
              url: "${env.BUILD_URL}console"
            ]]
          ]])
        }
      }
    }
  }
}
