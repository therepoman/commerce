package tails

import (
	__model__ "code.justin.tv/commerce/CoralGoModel/src/coral/model"
	__reflect__ "reflect"
)

func init() {
	var val *bool
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *bool
		if f, ok := from.Interface().(*bool); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to IsAlive")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *bool
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *bool
		if f, ok := from.Interface().(*bool); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to Bool")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *int32
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *int32
		if f, ok := from.Interface().(*int32); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to Integer")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to TwitchUserID")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to AmazonCustomerId")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to AmazonAccountPool")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to AccountLinkChangeType")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to String")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to TwitchUserName")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to OAuthCode")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to OAuthState")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to DirectedId")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to ErrorMessage")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to RequiredString")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}

type AmazonAccount interface {
	__type()
	SetAmazonId(v *string)
	AmazonId() *string
	SetAmazonAccountPool(v *string)
	AmazonAccountPool() *string
}
type _AmazonAccount struct {
	Ị_AmazonId          *string `coral:"AmazonId"`
	Ị_AmazonAccountPool *string `coral:"AmazonAccountPool"`
}

func (this *_AmazonAccount) AmazonId() *string {
	return this.Ị_AmazonId
}
func (this *_AmazonAccount) SetAmazonId(v *string) {
	this.Ị_AmazonId = v
}
func (this *_AmazonAccount) AmazonAccountPool() *string {
	return this.Ị_AmazonAccountPool
}
func (this *_AmazonAccount) SetAmazonAccountPool(v *string) {
	this.Ị_AmazonAccountPool = v
}
func (this *_AmazonAccount) __type() {
}
func NewAmazonAccount() AmazonAccount {
	return &_AmazonAccount{}
}
func init() {
	var val AmazonAccount
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("tv.justin.tails").RegisterShape("AmazonAccount", t, func() interface{} {
		return NewAmazonAccount()
	})
}

//The root of all TAILS exceptions.
type TAILSException interface {
	__type()
	error
	SetMessage(v *string)
	Message() *string
	SetContext(v *string)
	Context() *string
}
type _TAILSException struct {
	Ị_Message *string `coral:"Message"`
	Ị_Context *string `coral:"Context"`
}

func (this *_TAILSException) Error() string {
	return __model__.ErrorMessage(this)
}
func (this *_TAILSException) Message() *string {
	return this.Ị_Message
}
func (this *_TAILSException) SetMessage(v *string) {
	this.Ị_Message = v
}
func (this *_TAILSException) Context() *string {
	return this.Ị_Context
}
func (this *_TAILSException) SetContext(v *string) {
	this.Ị_Context = v
}
func (this *_TAILSException) __type() {
}
func NewTAILSException() TAILSException {
	return &_TAILSException{}
}
func init() {
	var val TAILSException
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("tv.justin.tails").RegisterShape("TAILSException", t, func() interface{} {
		return NewTAILSException()
	})
}

type GetPayoutIdentityResponse interface {
	__type()
	SetSupportedBitsCountry(v *bool)
	SupportedBitsCountry() *bool
	SetHasServicesRecord(v *bool)
	HasServicesRecord() *bool
}
type _GetPayoutIdentityResponse struct {
	Ị_HasServicesRecord    *bool `coral:"HasServicesRecord"`
	Ị_SupportedBitsCountry *bool `coral:"SupportedBitsCountry"`
}

func (this *_GetPayoutIdentityResponse) SupportedBitsCountry() *bool {
	return this.Ị_SupportedBitsCountry
}
func (this *_GetPayoutIdentityResponse) SetSupportedBitsCountry(v *bool) {
	this.Ị_SupportedBitsCountry = v
}
func (this *_GetPayoutIdentityResponse) HasServicesRecord() *bool {
	return this.Ị_HasServicesRecord
}
func (this *_GetPayoutIdentityResponse) SetHasServicesRecord(v *bool) {
	this.Ị_HasServicesRecord = v
}
func (this *_GetPayoutIdentityResponse) __type() {
}
func NewGetPayoutIdentityResponse() GetPayoutIdentityResponse {
	return &_GetPayoutIdentityResponse{}
}
func init() {
	var val GetPayoutIdentityResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("tv.justin.tails").RegisterShape("GetPayoutIdentityResponse", t, func() interface{} {
		return NewGetPayoutIdentityResponse()
	})
}

//This exception is thrown when concurrent link modifications are made for the same item
type ConcurrentModificationException interface {
	__type()
	error
	SetMessage(v *string)
	Message() *string
	SetContext(v *string)
	Context() *string
}
type _ConcurrentModificationException struct {
	RecoverableException
	TAILSException
	Ị_Message *string `coral:"Message"`
	Ị_Context *string `coral:"Context"`
}

func (this *_ConcurrentModificationException) Error() string {
	return __model__.ErrorMessage(this)
}
func (this *_ConcurrentModificationException) Message() *string {
	return this.Ị_Message
}
func (this *_ConcurrentModificationException) SetMessage(v *string) {
	this.Ị_Message = v
}
func (this *_ConcurrentModificationException) Context() *string {
	return this.Ị_Context
}
func (this *_ConcurrentModificationException) SetContext(v *string) {
	this.Ị_Context = v
}
func (this *_ConcurrentModificationException) __type() {
}
func NewConcurrentModificationException() ConcurrentModificationException {
	return &_ConcurrentModificationException{}
}
func init() {
	var val ConcurrentModificationException
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("tv.justin.tails").RegisterShape("ConcurrentModificationException", t, func() interface{} {
		return NewConcurrentModificationException()
	})
}

//This exception is thrown when concurrent link or unlink modifications are made for the same item
type ALSConcurrentModificationException interface {
	__type()
	error
	SetMessage(v *string)
	Message() *string
	SetContext(v *string)
	Context() *string
}
type _ALSConcurrentModificationException struct {
	UnrecoverableException
	TAILSException
	Ị_Context *string `coral:"Context"`
	Ị_Message *string `coral:"Message"`
}

func (this *_ALSConcurrentModificationException) Error() string {
	return __model__.ErrorMessage(this)
}
func (this *_ALSConcurrentModificationException) Message() *string {
	return this.Ị_Message
}
func (this *_ALSConcurrentModificationException) SetMessage(v *string) {
	this.Ị_Message = v
}
func (this *_ALSConcurrentModificationException) Context() *string {
	return this.Ị_Context
}
func (this *_ALSConcurrentModificationException) SetContext(v *string) {
	this.Ị_Context = v
}
func (this *_ALSConcurrentModificationException) __type() {
}
func NewALSConcurrentModificationException() ALSConcurrentModificationException {
	return &_ALSConcurrentModificationException{}
}
func init() {
	var val ALSConcurrentModificationException
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("tv.justin.tails").RegisterShape("ALSConcurrentModificationException", t, func() interface{} {
		return NewALSConcurrentModificationException()
	})
}

type GetLinkedAmazonAccountRequest interface {
	__type()
	SetTwitchUserId(v *string)
	TwitchUserId() *string
}
type _GetLinkedAmazonAccountRequest struct {
	Ị_TwitchUserId *string `coral:"TwitchUserId"`
}

func (this *_GetLinkedAmazonAccountRequest) TwitchUserId() *string {
	return this.Ị_TwitchUserId
}
func (this *_GetLinkedAmazonAccountRequest) SetTwitchUserId(v *string) {
	this.Ị_TwitchUserId = v
}
func (this *_GetLinkedAmazonAccountRequest) __type() {
}
func NewGetLinkedAmazonAccountRequest() GetLinkedAmazonAccountRequest {
	return &_GetLinkedAmazonAccountRequest{}
}
func init() {
	var val GetLinkedAmazonAccountRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("tv.justin.tails").RegisterShape("GetLinkedAmazonAccountRequest", t, func() interface{} {
		return NewGetLinkedAmazonAccountRequest()
	})
}

type GetTwitchUserInfoResponse interface {
	__type()
	SetTwitchAccount(v TwitchAccount)
	TwitchAccount() TwitchAccount
}
type _GetTwitchUserInfoResponse struct {
	Ị_TwitchAccount TwitchAccount `coral:"TwitchAccount"`
}

func (this *_GetTwitchUserInfoResponse) TwitchAccount() TwitchAccount {
	return this.Ị_TwitchAccount
}
func (this *_GetTwitchUserInfoResponse) SetTwitchAccount(v TwitchAccount) {
	this.Ị_TwitchAccount = v
}
func (this *_GetTwitchUserInfoResponse) __type() {
}
func NewGetTwitchUserInfoResponse() GetTwitchUserInfoResponse {
	return &_GetTwitchUserInfoResponse{}
}
func init() {
	var val GetTwitchUserInfoResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("tv.justin.tails").RegisterShape("GetTwitchUserInfoResponse", t, func() interface{} {
		return NewGetTwitchUserInfoResponse()
	})
}

type TwitchAccount interface {
	__type()
	SetTwitchUserID(v *string)
	TwitchUserID() *string
	SetTwitchUserName(v *string)
	TwitchUserName() *string
	SetTwitchUserAvatar(v *string)
	TwitchUserAvatar() *string
}
type _TwitchAccount struct {
	Ị_TwitchUserID     *string `coral:"TwitchUserID"`
	Ị_TwitchUserName   *string `coral:"TwitchUserName"`
	Ị_TwitchUserAvatar *string `coral:"TwitchUserAvatar"`
}

func (this *_TwitchAccount) TwitchUserName() *string {
	return this.Ị_TwitchUserName
}
func (this *_TwitchAccount) SetTwitchUserName(v *string) {
	this.Ị_TwitchUserName = v
}
func (this *_TwitchAccount) TwitchUserAvatar() *string {
	return this.Ị_TwitchUserAvatar
}
func (this *_TwitchAccount) SetTwitchUserAvatar(v *string) {
	this.Ị_TwitchUserAvatar = v
}
func (this *_TwitchAccount) TwitchUserID() *string {
	return this.Ị_TwitchUserID
}
func (this *_TwitchAccount) SetTwitchUserID(v *string) {
	this.Ị_TwitchUserID = v
}
func (this *_TwitchAccount) __type() {
}
func NewTwitchAccount() TwitchAccount {
	return &_TwitchAccount{}
}
func init() {
	var val TwitchAccount
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("tv.justin.tails").RegisterShape("TwitchAccount", t, func() interface{} {
		return NewTwitchAccount()
	})
}

//This exception is thrown when an amazon account already has the maximum number of allowable links
type TooManyTwitchAccountsLinkedException interface {
	__type()
	error
	SetMessage(v *string)
	Message() *string
	SetContext(v *string)
	Context() *string
	SetLinkedAccountLimit(v *int32)
	LinkedAccountLimit() *int32
}
type _TooManyTwitchAccountsLinkedException struct {
	UnrecoverableException
	TAILSException
	Ị_Message            *string `coral:"Message"`
	Ị_Context            *string `coral:"Context"`
	Ị_LinkedAccountLimit *int32  `coral:"LinkedAccountLimit"`
}

func (this *_TooManyTwitchAccountsLinkedException) Error() string {
	return __model__.ErrorMessage(this)
}
func (this *_TooManyTwitchAccountsLinkedException) Message() *string {
	return this.Ị_Message
}
func (this *_TooManyTwitchAccountsLinkedException) SetMessage(v *string) {
	this.Ị_Message = v
}
func (this *_TooManyTwitchAccountsLinkedException) Context() *string {
	return this.Ị_Context
}
func (this *_TooManyTwitchAccountsLinkedException) SetContext(v *string) {
	this.Ị_Context = v
}
func (this *_TooManyTwitchAccountsLinkedException) LinkedAccountLimit() *int32 {
	return this.Ị_LinkedAccountLimit
}
func (this *_TooManyTwitchAccountsLinkedException) SetLinkedAccountLimit(v *int32) {
	this.Ị_LinkedAccountLimit = v
}
func (this *_TooManyTwitchAccountsLinkedException) __type() {
}
func NewTooManyTwitchAccountsLinkedException() TooManyTwitchAccountsLinkedException {
	return &_TooManyTwitchAccountsLinkedException{}
}
func init() {
	var val TooManyTwitchAccountsLinkedException
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("tv.justin.tails").RegisterShape("TooManyTwitchAccountsLinkedException", t, func() interface{} {
		return NewTooManyTwitchAccountsLinkedException()
	})
}

type LinkAccountsResponse interface {
	__type()
	SetSuccess(v *bool)
	Success() *bool
}
type _LinkAccountsResponse struct {
	Ị_Success *bool `coral:"Success"`
}

func (this *_LinkAccountsResponse) Success() *bool {
	return this.Ị_Success
}
func (this *_LinkAccountsResponse) SetSuccess(v *bool) {
	this.Ị_Success = v
}
func (this *_LinkAccountsResponse) __type() {
}
func NewLinkAccountsResponse() LinkAccountsResponse {
	return &_LinkAccountsResponse{}
}
func init() {
	var val LinkAccountsResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("tv.justin.tails").RegisterShape("LinkAccountsResponse", t, func() interface{} {
		return NewLinkAccountsResponse()
	})
}

type GetLinkedAmazonAccountResponse interface {
	__type()
	SetHasLinkedAccount(v *bool)
	HasLinkedAccount() *bool
	SetPublicAmazonAccount(v PublicAmazonAccount)
	PublicAmazonAccount() PublicAmazonAccount
}
type _GetLinkedAmazonAccountResponse struct {
	Ị_HasLinkedAccount    *bool               `coral:"HasLinkedAccount"`
	Ị_PublicAmazonAccount PublicAmazonAccount `coral:"PublicAmazonAccount"`
}

func (this *_GetLinkedAmazonAccountResponse) HasLinkedAccount() *bool {
	return this.Ị_HasLinkedAccount
}
func (this *_GetLinkedAmazonAccountResponse) SetHasLinkedAccount(v *bool) {
	this.Ị_HasLinkedAccount = v
}
func (this *_GetLinkedAmazonAccountResponse) PublicAmazonAccount() PublicAmazonAccount {
	return this.Ị_PublicAmazonAccount
}
func (this *_GetLinkedAmazonAccountResponse) SetPublicAmazonAccount(v PublicAmazonAccount) {
	this.Ị_PublicAmazonAccount = v
}
func (this *_GetLinkedAmazonAccountResponse) __type() {
}
func NewGetLinkedAmazonAccountResponse() GetLinkedAmazonAccountResponse {
	return &_GetLinkedAmazonAccountResponse{}
}
func init() {
	var val GetLinkedAmazonAccountResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("tv.justin.tails").RegisterShape("GetLinkedAmazonAccountResponse", t, func() interface{} {
		return NewGetLinkedAmazonAccountResponse()
	})
}

type GetTwitchUserNameResponse interface {
	__type()
	SetTwitchUserName(v *string)
	TwitchUserName() *string
	SetTwitchUserAvatar(v *string)
	TwitchUserAvatar() *string
}
type _GetTwitchUserNameResponse struct {
	Ị_TwitchUserName   *string `coral:"TwitchUserName"`
	Ị_TwitchUserAvatar *string `coral:"TwitchUserAvatar"`
}

func (this *_GetTwitchUserNameResponse) TwitchUserName() *string {
	return this.Ị_TwitchUserName
}
func (this *_GetTwitchUserNameResponse) SetTwitchUserName(v *string) {
	this.Ị_TwitchUserName = v
}
func (this *_GetTwitchUserNameResponse) TwitchUserAvatar() *string {
	return this.Ị_TwitchUserAvatar
}
func (this *_GetTwitchUserNameResponse) SetTwitchUserAvatar(v *string) {
	this.Ị_TwitchUserAvatar = v
}
func (this *_GetTwitchUserNameResponse) __type() {
}
func NewGetTwitchUserNameResponse() GetTwitchUserNameResponse {
	return &_GetTwitchUserNameResponse{}
}
func init() {
	var val GetTwitchUserNameResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("tv.justin.tails").RegisterShape("GetTwitchUserNameResponse", t, func() interface{} {
		return NewGetTwitchUserNameResponse()
	})
}

type GetLinkedTwitchAccountsResponse interface {
	__type()
	SetTwitchAccounts(v []TwitchAccount)
	TwitchAccounts() []TwitchAccount
}
type _GetLinkedTwitchAccountsResponse struct {
	Ị_TwitchAccounts []TwitchAccount `coral:"TwitchAccounts"`
}

func (this *_GetLinkedTwitchAccountsResponse) TwitchAccounts() []TwitchAccount {
	return this.Ị_TwitchAccounts
}
func (this *_GetLinkedTwitchAccountsResponse) SetTwitchAccounts(v []TwitchAccount) {
	this.Ị_TwitchAccounts = v
}
func (this *_GetLinkedTwitchAccountsResponse) __type() {
}
func NewGetLinkedTwitchAccountsResponse() GetLinkedTwitchAccountsResponse {
	return &_GetLinkedTwitchAccountsResponse{}
}
func init() {
	var val GetLinkedTwitchAccountsResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("tv.justin.tails").RegisterShape("GetLinkedTwitchAccountsResponse", t, func() interface{} {
		return NewGetLinkedTwitchAccountsResponse()
	})
}

type PublicAmazonAccount interface {
	__type()
	SetAmazonName(v *string)
	AmazonName() *string
	SetAmazonAccountPool(v *string)
	AmazonAccountPool() *string
	SetAmazonEmailAddress(v *string)
	AmazonEmailAddress() *string
	SetCountryOfResidence(v *string)
	CountryOfResidence() *string
	SetPreferredMarketplace(v *string)
	PreferredMarketplace() *string
	SetAmazonDirectedId(v *string)
	AmazonDirectedId() *string
}
type _PublicAmazonAccount struct {
	Ị_AmazonAccountPool    *string `coral:"AmazonAccountPool"`
	Ị_AmazonEmailAddress   *string `coral:"AmazonEmailAddress"`
	Ị_CountryOfResidence   *string `coral:"CountryOfResidence"`
	Ị_PreferredMarketplace *string `coral:"PreferredMarketplace"`
	Ị_AmazonDirectedId     *string `coral:"AmazonDirectedId"`
	Ị_AmazonName           *string `coral:"AmazonName"`
}

func (this *_PublicAmazonAccount) AmazonEmailAddress() *string {
	return this.Ị_AmazonEmailAddress
}
func (this *_PublicAmazonAccount) SetAmazonEmailAddress(v *string) {
	this.Ị_AmazonEmailAddress = v
}
func (this *_PublicAmazonAccount) CountryOfResidence() *string {
	return this.Ị_CountryOfResidence
}
func (this *_PublicAmazonAccount) SetCountryOfResidence(v *string) {
	this.Ị_CountryOfResidence = v
}
func (this *_PublicAmazonAccount) PreferredMarketplace() *string {
	return this.Ị_PreferredMarketplace
}
func (this *_PublicAmazonAccount) SetPreferredMarketplace(v *string) {
	this.Ị_PreferredMarketplace = v
}
func (this *_PublicAmazonAccount) AmazonDirectedId() *string {
	return this.Ị_AmazonDirectedId
}
func (this *_PublicAmazonAccount) SetAmazonDirectedId(v *string) {
	this.Ị_AmazonDirectedId = v
}
func (this *_PublicAmazonAccount) AmazonName() *string {
	return this.Ị_AmazonName
}
func (this *_PublicAmazonAccount) SetAmazonName(v *string) {
	this.Ị_AmazonName = v
}
func (this *_PublicAmazonAccount) AmazonAccountPool() *string {
	return this.Ị_AmazonAccountPool
}
func (this *_PublicAmazonAccount) SetAmazonAccountPool(v *string) {
	this.Ị_AmazonAccountPool = v
}
func (this *_PublicAmazonAccount) __type() {
}
func NewPublicAmazonAccount() PublicAmazonAccount {
	return &_PublicAmazonAccount{}
}
func init() {
	var val PublicAmazonAccount
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("tv.justin.tails").RegisterShape("PublicAmazonAccount", t, func() interface{} {
		return NewPublicAmazonAccount()
	})
}

//This exception is thrown when retrying may help. e.g. timeout downstream.
type RecoverableException interface {
	__type()
	error
	SetMessage(v *string)
	Message() *string
	SetContext(v *string)
	Context() *string
}
type _RecoverableException struct {
	TAILSException
	Ị_Message *string `coral:"Message"`
	Ị_Context *string `coral:"Context"`
}

func (this *_RecoverableException) Error() string {
	return __model__.ErrorMessage(this)
}
func (this *_RecoverableException) Message() *string {
	return this.Ị_Message
}
func (this *_RecoverableException) SetMessage(v *string) {
	this.Ị_Message = v
}
func (this *_RecoverableException) Context() *string {
	return this.Ị_Context
}
func (this *_RecoverableException) SetContext(v *string) {
	this.Ị_Context = v
}
func (this *_RecoverableException) __type() {
}
func NewRecoverableException() RecoverableException {
	return &_RecoverableException{}
}
func init() {
	var val RecoverableException
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("tv.justin.tails").RegisterShape("RecoverableException", t, func() interface{} {
		return NewRecoverableException()
	})
}

//This exception is thrown when trying to link an already linked Twitch account to another amazon account
type AccountAlreadyLinkedException interface {
	__type()
	error
	SetMessage(v *string)
	Message() *string
	SetContext(v *string)
	Context() *string
}
type _AccountAlreadyLinkedException struct {
	UnrecoverableException
	TAILSException
	Ị_Message *string `coral:"Message"`
	Ị_Context *string `coral:"Context"`
}

func (this *_AccountAlreadyLinkedException) Error() string {
	return __model__.ErrorMessage(this)
}
func (this *_AccountAlreadyLinkedException) Message() *string {
	return this.Ị_Message
}
func (this *_AccountAlreadyLinkedException) SetMessage(v *string) {
	this.Ị_Message = v
}
func (this *_AccountAlreadyLinkedException) Context() *string {
	return this.Ị_Context
}
func (this *_AccountAlreadyLinkedException) SetContext(v *string) {
	this.Ị_Context = v
}
func (this *_AccountAlreadyLinkedException) __type() {
}
func NewAccountAlreadyLinkedException() AccountAlreadyLinkedException {
	return &_AccountAlreadyLinkedException{}
}
func init() {
	var val AccountAlreadyLinkedException
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("tv.justin.tails").RegisterShape("AccountAlreadyLinkedException", t, func() interface{} {
		return NewAccountAlreadyLinkedException()
	})
}

//Thrown when the payout entity has multiple CORs in TIMS
type InconsistentTIMSDataException interface {
	__type()
	error
	SetMessage(v *string)
	Message() *string
	SetContext(v *string)
	Context() *string
}
type _InconsistentTIMSDataException struct {
	UnrecoverableException
	TAILSException
	Ị_Message *string `coral:"Message"`
	Ị_Context *string `coral:"Context"`
}

func (this *_InconsistentTIMSDataException) Error() string {
	return __model__.ErrorMessage(this)
}
func (this *_InconsistentTIMSDataException) Message() *string {
	return this.Ị_Message
}
func (this *_InconsistentTIMSDataException) SetMessage(v *string) {
	this.Ị_Message = v
}
func (this *_InconsistentTIMSDataException) Context() *string {
	return this.Ị_Context
}
func (this *_InconsistentTIMSDataException) SetContext(v *string) {
	this.Ị_Context = v
}
func (this *_InconsistentTIMSDataException) __type() {
}
func NewInconsistentTIMSDataException() InconsistentTIMSDataException {
	return &_InconsistentTIMSDataException{}
}
func init() {
	var val InconsistentTIMSDataException
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("tv.justin.tails").RegisterShape("InconsistentTIMSDataException", t, func() interface{} {
		return NewInconsistentTIMSDataException()
	})
}

type LinkAccountsRequest interface {
	__type()
	SetWithoutSendingEmail(v *bool)
	WithoutSendingEmail() *bool
	SetWithoutSendingSNSNotification(v *bool)
	WithoutSendingSNSNotification() *bool
	SetTestCall(v *bool)
	TestCall() *bool
	SetAmazonAccount(v AmazonAccount)
	AmazonAccount() AmazonAccount
	SetTwitchUserID(v *string)
	TwitchUserID() *string
}
type _LinkAccountsRequest struct {
	Ị_TestCall                      *bool         `coral:"TestCall"`
	Ị_AmazonAccount                 AmazonAccount `coral:"AmazonAccount"`
	Ị_TwitchUserID                  *string       `coral:"TwitchUserID"`
	Ị_WithoutSendingEmail           *bool         `coral:"WithoutSendingEmail"`
	Ị_WithoutSendingSNSNotification *bool         `coral:"WithoutSendingSNSNotification"`
}

func (this *_LinkAccountsRequest) WithoutSendingEmail() *bool {
	return this.Ị_WithoutSendingEmail
}
func (this *_LinkAccountsRequest) SetWithoutSendingEmail(v *bool) {
	this.Ị_WithoutSendingEmail = v
}
func (this *_LinkAccountsRequest) WithoutSendingSNSNotification() *bool {
	return this.Ị_WithoutSendingSNSNotification
}
func (this *_LinkAccountsRequest) SetWithoutSendingSNSNotification(v *bool) {
	this.Ị_WithoutSendingSNSNotification = v
}
func (this *_LinkAccountsRequest) TestCall() *bool {
	return this.Ị_TestCall
}
func (this *_LinkAccountsRequest) SetTestCall(v *bool) {
	this.Ị_TestCall = v
}
func (this *_LinkAccountsRequest) AmazonAccount() AmazonAccount {
	return this.Ị_AmazonAccount
}
func (this *_LinkAccountsRequest) SetAmazonAccount(v AmazonAccount) {
	this.Ị_AmazonAccount = v
}
func (this *_LinkAccountsRequest) TwitchUserID() *string {
	return this.Ị_TwitchUserID
}
func (this *_LinkAccountsRequest) SetTwitchUserID(v *string) {
	this.Ị_TwitchUserID = v
}
func (this *_LinkAccountsRequest) __type() {
}
func NewLinkAccountsRequest() LinkAccountsRequest {
	return &_LinkAccountsRequest{}
}
func init() {
	var val LinkAccountsRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("tv.justin.tails").RegisterShape("LinkAccountsRequest", t, func() interface{} {
		return NewLinkAccountsRequest()
	})
}

type GetLinkedAmazonDirectedIdRequest interface {
	__type()
	SetTwitchUserId(v *string)
	TwitchUserId() *string
}
type _GetLinkedAmazonDirectedIdRequest struct {
	Ị_TwitchUserId *string `coral:"TwitchUserId"`
}

func (this *_GetLinkedAmazonDirectedIdRequest) TwitchUserId() *string {
	return this.Ị_TwitchUserId
}
func (this *_GetLinkedAmazonDirectedIdRequest) SetTwitchUserId(v *string) {
	this.Ị_TwitchUserId = v
}
func (this *_GetLinkedAmazonDirectedIdRequest) __type() {
}
func NewGetLinkedAmazonDirectedIdRequest() GetLinkedAmazonDirectedIdRequest {
	return &_GetLinkedAmazonDirectedIdRequest{}
}
func init() {
	var val GetLinkedAmazonDirectedIdRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("tv.justin.tails").RegisterShape("GetLinkedAmazonDirectedIdRequest", t, func() interface{} {
		return NewGetLinkedAmazonDirectedIdRequest()
	})
}

type GetPayoutIdentityRequest interface {
	__type()
	SetPayoutEntityId(v *string)
	PayoutEntityId() *string
}
type _GetPayoutIdentityRequest struct {
	Ị_PayoutEntityId *string `coral:"PayoutEntityId"`
}

func (this *_GetPayoutIdentityRequest) PayoutEntityId() *string {
	return this.Ị_PayoutEntityId
}
func (this *_GetPayoutIdentityRequest) SetPayoutEntityId(v *string) {
	this.Ị_PayoutEntityId = v
}
func (this *_GetPayoutIdentityRequest) __type() {
}
func NewGetPayoutIdentityRequest() GetPayoutIdentityRequest {
	return &_GetPayoutIdentityRequest{}
}
func init() {
	var val GetPayoutIdentityRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("tv.justin.tails").RegisterShape("GetPayoutIdentityRequest", t, func() interface{} {
		return NewGetPayoutIdentityRequest()
	})
}

type GetLinkedAmazonDirectedIdResponse interface {
	__type()
	SetHasLinkedAccount(v *bool)
	HasLinkedAccount() *bool
	SetAmazonDirectedId(v *string)
	AmazonDirectedId() *string
}
type _GetLinkedAmazonDirectedIdResponse struct {
	Ị_HasLinkedAccount *bool   `coral:"HasLinkedAccount"`
	Ị_AmazonDirectedId *string `coral:"AmazonDirectedId"`
}

func (this *_GetLinkedAmazonDirectedIdResponse) AmazonDirectedId() *string {
	return this.Ị_AmazonDirectedId
}
func (this *_GetLinkedAmazonDirectedIdResponse) SetAmazonDirectedId(v *string) {
	this.Ị_AmazonDirectedId = v
}
func (this *_GetLinkedAmazonDirectedIdResponse) HasLinkedAccount() *bool {
	return this.Ị_HasLinkedAccount
}
func (this *_GetLinkedAmazonDirectedIdResponse) SetHasLinkedAccount(v *bool) {
	this.Ị_HasLinkedAccount = v
}
func (this *_GetLinkedAmazonDirectedIdResponse) __type() {
}
func NewGetLinkedAmazonDirectedIdResponse() GetLinkedAmazonDirectedIdResponse {
	return &_GetLinkedAmazonDirectedIdResponse{}
}
func init() {
	var val GetLinkedAmazonDirectedIdResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("tv.justin.tails").RegisterShape("GetLinkedAmazonDirectedIdResponse", t, func() interface{} {
		return NewGetLinkedAmazonDirectedIdResponse()
	})
}

//This exception is thrown when there was bad input.
type InvalidParameterException interface {
	__type()
	error
	SetMessage(v *string)
	Message() *string
	SetContext(v *string)
	Context() *string
}
type _InvalidParameterException struct {
	UnrecoverableException
	TAILSException
	Ị_Message *string `coral:"Message"`
	Ị_Context *string `coral:"Context"`
}

func (this *_InvalidParameterException) Error() string {
	return __model__.ErrorMessage(this)
}
func (this *_InvalidParameterException) Message() *string {
	return this.Ị_Message
}
func (this *_InvalidParameterException) SetMessage(v *string) {
	this.Ị_Message = v
}
func (this *_InvalidParameterException) Context() *string {
	return this.Ị_Context
}
func (this *_InvalidParameterException) SetContext(v *string) {
	this.Ị_Context = v
}
func (this *_InvalidParameterException) __type() {
}
func NewInvalidParameterException() InvalidParameterException {
	return &_InvalidParameterException{}
}
func init() {
	var val InvalidParameterException
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("tv.justin.tails").RegisterShape("InvalidParameterException", t, func() interface{} {
		return NewInvalidParameterException()
	})
}

type UnlinkTwitchAccountRequest interface {
	__type()
	SetTwitchUserID(v *string)
	TwitchUserID() *string
	SetWithoutSendingEmail(v *bool)
	WithoutSendingEmail() *bool
	SetWithoutSendingSNSNotification(v *bool)
	WithoutSendingSNSNotification() *bool
	SetTestCall(v *bool)
	TestCall() *bool
}
type _UnlinkTwitchAccountRequest struct {
	Ị_WithoutSendingEmail           *bool   `coral:"WithoutSendingEmail"`
	Ị_WithoutSendingSNSNotification *bool   `coral:"WithoutSendingSNSNotification"`
	Ị_TestCall                      *bool   `coral:"TestCall"`
	Ị_TwitchUserID                  *string `coral:"TwitchUserID"`
}

func (this *_UnlinkTwitchAccountRequest) TwitchUserID() *string {
	return this.Ị_TwitchUserID
}
func (this *_UnlinkTwitchAccountRequest) SetTwitchUserID(v *string) {
	this.Ị_TwitchUserID = v
}
func (this *_UnlinkTwitchAccountRequest) WithoutSendingEmail() *bool {
	return this.Ị_WithoutSendingEmail
}
func (this *_UnlinkTwitchAccountRequest) SetWithoutSendingEmail(v *bool) {
	this.Ị_WithoutSendingEmail = v
}
func (this *_UnlinkTwitchAccountRequest) WithoutSendingSNSNotification() *bool {
	return this.Ị_WithoutSendingSNSNotification
}
func (this *_UnlinkTwitchAccountRequest) SetWithoutSendingSNSNotification(v *bool) {
	this.Ị_WithoutSendingSNSNotification = v
}
func (this *_UnlinkTwitchAccountRequest) TestCall() *bool {
	return this.Ị_TestCall
}
func (this *_UnlinkTwitchAccountRequest) SetTestCall(v *bool) {
	this.Ị_TestCall = v
}
func (this *_UnlinkTwitchAccountRequest) __type() {
}
func NewUnlinkTwitchAccountRequest() UnlinkTwitchAccountRequest {
	return &_UnlinkTwitchAccountRequest{}
}
func init() {
	var val UnlinkTwitchAccountRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("tv.justin.tails").RegisterShape("UnlinkTwitchAccountRequest", t, func() interface{} {
		return NewUnlinkTwitchAccountRequest()
	})
}

type GetTwitchUserNameRequest interface {
	__type()
	SetTwitchUserId(v *string)
	TwitchUserId() *string
}
type _GetTwitchUserNameRequest struct {
	Ị_TwitchUserId *string `coral:"TwitchUserId"`
}

func (this *_GetTwitchUserNameRequest) TwitchUserId() *string {
	return this.Ị_TwitchUserId
}
func (this *_GetTwitchUserNameRequest) SetTwitchUserId(v *string) {
	this.Ị_TwitchUserId = v
}
func (this *_GetTwitchUserNameRequest) __type() {
}
func NewGetTwitchUserNameRequest() GetTwitchUserNameRequest {
	return &_GetTwitchUserNameRequest{}
}
func init() {
	var val GetTwitchUserNameRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("tv.justin.tails").RegisterShape("GetTwitchUserNameRequest", t, func() interface{} {
		return NewGetTwitchUserNameRequest()
	})
}

//A notification that is sent whenever the status of an account link changes.
//
//
//Type - The type of change that occurred.
//
//AmazonAccount - The amazon account that the change occurred on.
//
//TwitchAccounts - All the twitch accounts currently attached to the amazon account.
//
//ActionedTwitchAccount - The twitch account that the action occurred on. I.E. The account that was linked/unlinked.
type AccountLinkChangedNotification interface {
	__type()
	SetActionedTwitchAccount(v TwitchAccount)
	ActionedTwitchAccount() TwitchAccount
	SetType(v *string)
	Type() *string
	SetAmazonAccount(v AmazonAccount)
	AmazonAccount() AmazonAccount
	SetTwitchAccounts(v []TwitchAccount)
	TwitchAccounts() []TwitchAccount
}
type _AccountLinkChangedNotification struct {
	Ị_Type                  *string         `coral:"Type"`
	Ị_AmazonAccount         AmazonAccount   `coral:"AmazonAccount"`
	Ị_TwitchAccounts        []TwitchAccount `coral:"TwitchAccounts"`
	Ị_ActionedTwitchAccount TwitchAccount   `coral:"ActionedTwitchAccount"`
}

func (this *_AccountLinkChangedNotification) TwitchAccounts() []TwitchAccount {
	return this.Ị_TwitchAccounts
}
func (this *_AccountLinkChangedNotification) SetTwitchAccounts(v []TwitchAccount) {
	this.Ị_TwitchAccounts = v
}
func (this *_AccountLinkChangedNotification) ActionedTwitchAccount() TwitchAccount {
	return this.Ị_ActionedTwitchAccount
}
func (this *_AccountLinkChangedNotification) SetActionedTwitchAccount(v TwitchAccount) {
	this.Ị_ActionedTwitchAccount = v
}
func (this *_AccountLinkChangedNotification) Type() *string {
	return this.Ị_Type
}
func (this *_AccountLinkChangedNotification) SetType(v *string) {
	this.Ị_Type = v
}
func (this *_AccountLinkChangedNotification) AmazonAccount() AmazonAccount {
	return this.Ị_AmazonAccount
}
func (this *_AccountLinkChangedNotification) SetAmazonAccount(v AmazonAccount) {
	this.Ị_AmazonAccount = v
}
func (this *_AccountLinkChangedNotification) __type() {
}
func NewAccountLinkChangedNotification() AccountLinkChangedNotification {
	return &_AccountLinkChangedNotification{}
}
func init() {
	var val AccountLinkChangedNotification
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("tv.justin.tails").RegisterShape("AccountLinkChangedNotification", t, func() interface{} {
		return NewAccountLinkChangedNotification()
	})
}

type UnlinkTwitchAccountResponse interface {
	__type()
	SetSuccess(v *bool)
	Success() *bool
}
type _UnlinkTwitchAccountResponse struct {
	Ị_Success *bool `coral:"Success"`
}

func (this *_UnlinkTwitchAccountResponse) Success() *bool {
	return this.Ị_Success
}
func (this *_UnlinkTwitchAccountResponse) SetSuccess(v *bool) {
	this.Ị_Success = v
}
func (this *_UnlinkTwitchAccountResponse) __type() {
}
func NewUnlinkTwitchAccountResponse() UnlinkTwitchAccountResponse {
	return &_UnlinkTwitchAccountResponse{}
}
func init() {
	var val UnlinkTwitchAccountResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("tv.justin.tails").RegisterShape("UnlinkTwitchAccountResponse", t, func() interface{} {
		return NewUnlinkTwitchAccountResponse()
	})
}

//This exception is thrown when retrying will not help. e.g. bad input.
type UnrecoverableException interface {
	__type()
	error
	SetMessage(v *string)
	Message() *string
	SetContext(v *string)
	Context() *string
}
type _UnrecoverableException struct {
	TAILSException
	Ị_Message *string `coral:"Message"`
	Ị_Context *string `coral:"Context"`
}

func (this *_UnrecoverableException) Error() string {
	return __model__.ErrorMessage(this)
}
func (this *_UnrecoverableException) Message() *string {
	return this.Ị_Message
}
func (this *_UnrecoverableException) SetMessage(v *string) {
	this.Ị_Message = v
}
func (this *_UnrecoverableException) Context() *string {
	return this.Ị_Context
}
func (this *_UnrecoverableException) SetContext(v *string) {
	this.Ị_Context = v
}
func (this *_UnrecoverableException) __type() {
}
func NewUnrecoverableException() UnrecoverableException {
	return &_UnrecoverableException{}
}
func init() {
	var val UnrecoverableException
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("tv.justin.tails").RegisterShape("UnrecoverableException", t, func() interface{} {
		return NewUnrecoverableException()
	})
}

//This exception is thrown when the Twitch account is linked to multiple Amazon accounts
type MultipleAmazonAccountsLinkedException interface {
	__type()
	error
	SetMessage(v *string)
	Message() *string
	SetContext(v *string)
	Context() *string
}
type _MultipleAmazonAccountsLinkedException struct {
	UnrecoverableException
	TAILSException
	Ị_Context *string `coral:"Context"`
	Ị_Message *string `coral:"Message"`
}

func (this *_MultipleAmazonAccountsLinkedException) Error() string {
	return __model__.ErrorMessage(this)
}
func (this *_MultipleAmazonAccountsLinkedException) Message() *string {
	return this.Ị_Message
}
func (this *_MultipleAmazonAccountsLinkedException) SetMessage(v *string) {
	this.Ị_Message = v
}
func (this *_MultipleAmazonAccountsLinkedException) Context() *string {
	return this.Ị_Context
}
func (this *_MultipleAmazonAccountsLinkedException) SetContext(v *string) {
	this.Ị_Context = v
}
func (this *_MultipleAmazonAccountsLinkedException) __type() {
}
func NewMultipleAmazonAccountsLinkedException() MultipleAmazonAccountsLinkedException {
	return &_MultipleAmazonAccountsLinkedException{}
}
func init() {
	var val MultipleAmazonAccountsLinkedException
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("tv.justin.tails").RegisterShape("MultipleAmazonAccountsLinkedException", t, func() interface{} {
		return NewMultipleAmazonAccountsLinkedException()
	})
}

//This exception is thrown when there is no customer info found from the Amazon address service
type NoCustomerInfoFoundException interface {
	__type()
	error
	SetMessage(v *string)
	Message() *string
	SetContext(v *string)
	Context() *string
}
type _NoCustomerInfoFoundException struct {
	UnrecoverableException
	TAILSException
	Ị_Context *string `coral:"Context"`
	Ị_Message *string `coral:"Message"`
}

func (this *_NoCustomerInfoFoundException) Error() string {
	return __model__.ErrorMessage(this)
}
func (this *_NoCustomerInfoFoundException) Message() *string {
	return this.Ị_Message
}
func (this *_NoCustomerInfoFoundException) SetMessage(v *string) {
	this.Ị_Message = v
}
func (this *_NoCustomerInfoFoundException) Context() *string {
	return this.Ị_Context
}
func (this *_NoCustomerInfoFoundException) SetContext(v *string) {
	this.Ị_Context = v
}
func (this *_NoCustomerInfoFoundException) __type() {
}
func NewNoCustomerInfoFoundException() NoCustomerInfoFoundException {
	return &_NoCustomerInfoFoundException{}
}
func init() {
	var val NoCustomerInfoFoundException
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("tv.justin.tails").RegisterShape("NoCustomerInfoFoundException", t, func() interface{} {
		return NewNoCustomerInfoFoundException()
	})
}

type GetTwitchUserInfoRequest interface {
	__type()
	SetOAuthCodeAndState(v OAuthCodeAndState)
	OAuthCodeAndState() OAuthCodeAndState
	SetOAuthToken(v *string)
	OAuthToken() *string
}
type _GetTwitchUserInfoRequest struct {
	Ị_OAuthCodeAndState OAuthCodeAndState `coral:"OAuthCodeAndState"`
	Ị_OAuthToken        *string           `coral:"OAuthToken"`
}

func (this *_GetTwitchUserInfoRequest) OAuthCodeAndState() OAuthCodeAndState {
	return this.Ị_OAuthCodeAndState
}
func (this *_GetTwitchUserInfoRequest) SetOAuthCodeAndState(v OAuthCodeAndState) {
	this.Ị_OAuthCodeAndState = v
}
func (this *_GetTwitchUserInfoRequest) OAuthToken() *string {
	return this.Ị_OAuthToken
}
func (this *_GetTwitchUserInfoRequest) SetOAuthToken(v *string) {
	this.Ị_OAuthToken = v
}
func (this *_GetTwitchUserInfoRequest) __type() {
}
func NewGetTwitchUserInfoRequest() GetTwitchUserInfoRequest {
	return &_GetTwitchUserInfoRequest{}
}
func init() {
	var val GetTwitchUserInfoRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("tv.justin.tails").RegisterShape("GetTwitchUserInfoRequest", t, func() interface{} {
		return NewGetTwitchUserInfoRequest()
	})
}

type GetLinkedTwitchAccountsRequest interface {
	__type()
	SetAmazonAccount(v AmazonAccount)
	AmazonAccount() AmazonAccount
}
type _GetLinkedTwitchAccountsRequest struct {
	Ị_AmazonAccount AmazonAccount `coral:"AmazonAccount"`
}

func (this *_GetLinkedTwitchAccountsRequest) AmazonAccount() AmazonAccount {
	return this.Ị_AmazonAccount
}
func (this *_GetLinkedTwitchAccountsRequest) SetAmazonAccount(v AmazonAccount) {
	this.Ị_AmazonAccount = v
}
func (this *_GetLinkedTwitchAccountsRequest) __type() {
}
func NewGetLinkedTwitchAccountsRequest() GetLinkedTwitchAccountsRequest {
	return &_GetLinkedTwitchAccountsRequest{}
}
func init() {
	var val GetLinkedTwitchAccountsRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("tv.justin.tails").RegisterShape("GetLinkedTwitchAccountsRequest", t, func() interface{} {
		return NewGetLinkedTwitchAccountsRequest()
	})
}

type OAuthCodeAndState interface {
	__type()
	SetOAuthCode(v *string)
	OAuthCode() *string
	SetOAuthState(v *string)
	OAuthState() *string
}
type _OAuthCodeAndState struct {
	Ị_OAuthState *string `coral:"OAuthState"`
	Ị_OAuthCode  *string `coral:"OAuthCode"`
}

func (this *_OAuthCodeAndState) OAuthCode() *string {
	return this.Ị_OAuthCode
}
func (this *_OAuthCodeAndState) SetOAuthCode(v *string) {
	this.Ị_OAuthCode = v
}
func (this *_OAuthCodeAndState) OAuthState() *string {
	return this.Ị_OAuthState
}
func (this *_OAuthCodeAndState) SetOAuthState(v *string) {
	this.Ị_OAuthState = v
}
func (this *_OAuthCodeAndState) __type() {
}
func NewOAuthCodeAndState() OAuthCodeAndState {
	return &_OAuthCodeAndState{}
}
func init() {
	var val OAuthCodeAndState
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("tv.justin.tails").RegisterShape("OAuthCodeAndState", t, func() interface{} {
		return NewOAuthCodeAndState()
	})
}

//This exception is thrown on a database (or other dependency) error
type DependencyException interface {
	__type()
	error
	SetContext(v *string)
	Context() *string
	SetMessage(v *string)
	Message() *string
}
type _DependencyException struct {
	UnrecoverableException
	TAILSException
	Ị_Message *string `coral:"Message"`
	Ị_Context *string `coral:"Context"`
}

func (this *_DependencyException) Error() string {
	return __model__.ErrorMessage(this)
}
func (this *_DependencyException) Message() *string {
	return this.Ị_Message
}
func (this *_DependencyException) SetMessage(v *string) {
	this.Ị_Message = v
}
func (this *_DependencyException) Context() *string {
	return this.Ị_Context
}
func (this *_DependencyException) SetContext(v *string) {
	this.Ị_Context = v
}
func (this *_DependencyException) __type() {
}
func NewDependencyException() DependencyException {
	return &_DependencyException{}
}
func init() {
	var val DependencyException
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("tv.justin.tails").RegisterShape("DependencyException", t, func() interface{} {
		return NewDependencyException()
	})
}
func init() {
	var val []TwitchAccount
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to []TwitchAccount
		if f, ok := from.Interface().([]TwitchAccount); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to TwitchAccounts")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}

//A service that manages links between Amazon and Twitch Accounts.
type TwitchAmazonIdentityLinkingService interface { //Link a twitch account to an amazon account.
	LinkAccounts(input LinkAccountsRequest) (LinkAccountsResponse, error)                                        //Unlink a twitch account from its linked amazon account.
	UnlinkTwitchAccount(input UnlinkTwitchAccountRequest) (UnlinkTwitchAccountResponse, error)                   //Get linked Amazon Account from TwitchUserID.
	GetLinkedAmazonAccount(input GetLinkedAmazonAccountRequest) (GetLinkedAmazonAccountResponse, error)          //Get linked Amazon DirectedId from TwitchUserID.
	GetLinkedAmazonDirectedId(input GetLinkedAmazonDirectedIdRequest) (GetLinkedAmazonDirectedIdResponse, error) //Retrieves the Twitch login name and user ID for a user, given the OAuth code and state
	GetTwitchUserInfo(input GetTwitchUserInfoRequest) (GetTwitchUserInfoResponse, error)                         //Retrieves the Twitch user name given the Twitch user ID
	GetTwitchUserName(input GetTwitchUserNameRequest) (GetTwitchUserNameResponse, error)                         //Get twitch linked accounts from amazon account
	GetLinkedTwitchAccounts(input GetLinkedTwitchAccountsRequest) (GetLinkedTwitchAccountsResponse, error)       //Used to lookup select TIMS information for a partners account
	GetPayoutIdentity(input GetPayoutIdentityRequest) (GetPayoutIdentityResponse, error)
}
