package model

import (
	"reflect"
	"testing"
)

const (
	testAssemblyName    = "foo"
	testOpName          = "baz"
	testShapeName       = "bar"
	testShapeNameInput  = "barInput"
	testShapeNameOutput = "barOutput"
	testShapeNameErr    = "barErr"
)

func init() {
	// Register our test shapes
	var i testShape
	typeOf := reflect.TypeOf(&i)
	asm := LookupAssembly(testAssemblyName)
	asm.RegisterShape(testShapeName, typeOf, func() interface{} {
		return testShape{}
	})
	asm.RegisterShape(testShapeNameInput, typeOf, func() interface{} {
		return testShape{}
	})
	asm.RegisterShape(testShapeNameOutput, typeOf, func() interface{} {
		return testShape{}
	})
	asm.RegisterShape(testShapeNameErr, typeOf, func() interface{} {
		return testShape{}
	})
}

func TestRegisterShape(t *testing.T) {
	asm := LookupAssembly(testAssemblyName)
	if shape, err := asm.Shape("unregistered"); shape != nil || err == nil {
		t.Error("Expected nil shape and an err but found", shape, err)
	}

	var i testShape
	typeOf := reflect.TypeOf(i)
	if shape, err := GetShapeFromType(typeOf); shape == nil || err != nil {
		t.Error("GetShapeFromType - Expected shape and nil err but found", shape, err)
	}
	typeOf = reflect.TypeOf(asm)
	if shape, err := GetShapeFromType(typeOf); shape != nil || err == nil {
		t.Error("GetShapeFromType - Expected nil shape and an err but found", shape, err)
	}
	if shape, err := GetShapeFromFQN(testAssemblyName, testShapeName); shape == nil || err != nil {
		t.Error("GetShapeFromFQN - Expected shape and nil err but found", shape, err)
	}
	if shape, err := GetShapeFromFQN("", testAssemblyName+"#"+testShapeName); shape == nil || err != nil {
		t.Error("GetShapeFromFQN with # - Expected shape and nil err but found", shape, err)
	}
	if shape, err := GetShapeFromFQN("", testShapeName+"#"+testAssemblyName); shape != nil || err == nil {
		t.Error("GetShapeFromFQN unregistered - Expected nil shape and an err but found", shape, err)
	}
}

func TestShape(t *testing.T) {
	asm := LookupAssembly(testAssemblyName)
	var shape Shape
	var err error
	if shape, err = asm.Shape(testShapeName); shape == nil || err != nil {
		t.Fatal("Expected shape and nil err but found", shape, err)
	}

	if sAsm := shape.Assembly(); sAsm != asm {
		t.Error("Expected Assembly", asm, "but found", sAsm)
	}
	if name := shape.Name(); name != testShapeName {
		t.Error("Expected name", testShapeName, "but found", name)
	}
	if fqn := shape.FullyQualifiedName(); fqn != testAssemblyName+"#"+testShapeName {
		t.Error("Expected FQN", testAssemblyName+"#"+testShapeName, "but found", fqn)
	}
	var ts testShape
	expectedType := reflect.TypeOf(ts)
	if typ := shape.Type(); typ != expectedType {
		t.Error("Expected type", expectedType, "but found", typ)
	}

	r := shape.New()
	if _, ok := r.(testShape); !ok {
		t.Errorf(`New("foo","bar") = %t, expected %t`, r, testShape{})
	}
}

func TestRegisterOp(t *testing.T) {
	asm := LookupAssembly(testAssemblyName)

	registered, err := asm.Shape(testShapeName)
	if err != nil {
		t.Fatalf("%s", err)
	}
	unregistered := &shape{name: "notRegistered"}

	tests := []struct {
		name           string
		opName         string
		input          Shape
		output         Shape
		errs           []Shape
		isValid        bool
		nameRegistered bool
	}{
		{"no name", "", nil, nil, nil, false, false},
		{"registered input", "reg in", registered, nil, nil, true, true},
		{"registered output", "reg out", nil, registered, nil, true, true},
		{"registered errs", "reg errs", nil, nil, []Shape{registered}, true, true},
		{"duplicate", "reg in", registered, nil, nil, false, true},
		{"unregistered input", "unreg in", unregistered, registered, []Shape{registered}, false, false},
		{"unregistered output", "unreg out", registered, unregistered, []Shape{registered}, false, false},
		{"unregistered errs", "unreg errs", registered, registered, []Shape{unregistered}, false, false},
	}

	for _, test := range tests {
		if err := asm.RegisterOp(test.opName, test.input, test.output, test.errs); test.isValid != (err == nil) {
			t.Error(test.name, "- Expected err == nil:", test.isValid, "but got err", err)
		} else if test.nameRegistered {
			// Verify that we can access the operation we just registered.
			if op, err := asm.Op(test.opName); op == nil || err != nil {
				t.Error(test.name, "- Expected an op and nil err but found", op, err)
			}
		} else if !test.nameRegistered {
			// Verify that we the operation was not registered.
			if op, err := asm.Op(test.opName); op != nil || err == nil {
				t.Error(test.name, "- Expected nil op and an err but found", op, err)
			}
		}
	}
}

func TestOp(t *testing.T) {
	asm := LookupAssembly(testAssemblyName)
	inShape, outShape, errsShape := getOpTestShapes(asm)
	if inShape == nil || outShape == nil || errsShape == nil {
		t.Fatal("Not all test shapes found:", inShape, outShape, errsShape)
	}
	asm.RegisterOp(testOpName, inShape, outShape, errsShape)

	var op Op
	var err error
	if op, err = asm.Op(testOpName); op == nil || err != nil {
		t.Fatal("Expected op and nil err but found", op, err)
	}

	if sAsm := op.Assembly(); sAsm != asm {
		t.Error("Expected Assembly", asm, "but found", sAsm)
	}
	if name := op.Name(); name != testOpName {
		t.Error("Expected name", testOpName, "but found", name)
	}
	if input := op.Input(); input != inShape {
		t.Error("Expected input", inShape, "but found", input)
	}
	if output := op.Output(); output != outShape {
		t.Error("Expected output", outShape, "but found", output)
	}
	if errs := op.Errors(); !reflect.DeepEqual(errs, errsShape) {
		t.Error("Expected errs", errsShape, "but found", errs)
	}
}

func getOpTestShapes(asm Assembly) (input, output Shape, errs []Shape) {
	input, _ = asm.Shape(testShapeNameInput)
	output, _ = asm.Shape(testShapeNameOutput)
	errShape, _ := asm.Shape(testShapeNameErr)
	if errShape != nil {
		errs = []Shape{errShape}
	}
	return
}

type testShape struct {
}
