# Compare two Broadcaster's Followers
We want to be able to suggest other streamers that you might like... 
one way of doing this is to compare the followers of two channels
Write some code that can tell us how many followers two channels have in common.

##API for followers:
https://dev.twitch.tv/docs/v5/reference/channels/#get-channel-followers

##Request is available:
https://www.npmjs.com/package/request

##Example Request:
'giggle_fairies', 'thekittenhugs'