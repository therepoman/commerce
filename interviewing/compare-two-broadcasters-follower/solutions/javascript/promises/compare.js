let request = require('request-promise-native');

// This solution shows how a promises solution in JS would work.
// This does not include paging. It uses Promise.all() to sync after the promises finish

getCommonFollowers('giggle_fairies', 'thekittenhugs');

function getFollowers(channel, callback) {
    let options = {
        url: `https://api.twitch.tv/kraken/channels/${channel}/follows`,
        json:true,
        headers: {
            'Client-ID':'uo6dggojyb8d6soh92zknwmi5ej1q2'
        },
        qs: {
            limit: 100
        }
    };
    return request(options)
        .then((body) => {
            return { follows: body.follows }
        });
}


function getCommonFollowers(channelA, channelB) {
    Promise.all([
        getFollowers(channelA),
        getFollowers(channelB)
    ]).then(([{follows: channelAFollows}, {follows: channelBFollows}]) => {
        let channelATable = {};
        for(let i = 0; i < channelAFollows.length; i++) {
            channelATable[channelAFollows[i].user.display_name] = true;
        }
        let commons = channelBFollows.filter((follow)=> channelATable[follow.user.display_name]);
        console.log(commons.map((follow) => follow.user.display_name));

    });

}