let request = require('request');

// This example shows callbacks result w/o paging.
// It uses a sync / finalize function + closures to ensure the async operations are finished.

getCommonFollowers('giggle_fairies', 'thekittenhugs');

function getFollowers(channel, callback) {
    let options = {
        url: `https://api.twitch.tv/kraken/channels/${channel}/follows`,
        json:true,
        headers: {
            'Client-ID':'uo6dggojyb8d6soh92zknwmi5ej1q2'
        },
        qs: {
            limit: 100
        }
    };
    return request(options, callback);
}


function getCommonFollowers(channelA, channelB) {
    let returnedA = false, returnedB = false;
    let channelAFollows, channelBFollows;

    let finalize = () => {
        if (!returnedA || !returnedB) return;

        let channelATable = {};
        for(let i = 0; i < channelAFollows.length; i++) {
            channelATable[channelAFollows[i].user.display_name] = true;
        }
        let commons = channelBFollows.filter((follow)=> channelATable[follow.user.display_name]);
        console.log(commons.map((follow) => follow.user.display_name));
    };
    getFollowers(channelA, (err, response, body) => {
        channelAFollows = body.follows;
        returnedA = true;
        finalize();
    });
    getFollowers(channelB, (err, response, body) => {
        channelBFollows = body.follows;
        returnedB = true;
        finalize();
    });

}