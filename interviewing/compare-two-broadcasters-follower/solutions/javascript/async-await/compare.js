let request = require('request-promise-native');

// This solution shows how an async / await solution in JS would work.
// It'll page 50 times to get a sizable result, approximately 1% of both of these streamers.
// If anyone can write this in an interview, we should hire them on the spot.

getCommonFollowers('bacon_donut', 'sevadus');

async function getFollowers(channel, cursor) {
    let options = {
        url: `https://api.twitch.tv/kraken/channels/${channel}/follows`,
        json:true,
        headers: {
            'Client-ID':'uo6dggojyb8d6soh92zknwmi5ej1q2'
        },
        qs: {
            cursor,
            limit: 100
        }
    };
    return request(options)
        .then((body) => {
            return {cursor: body._cursor, follows: body.follows, total: body._total}
        });
}

async function getAllFollowers(channel){
    var cursor, allFollows = [], maxRequests = 50;
    for (let i=0; i < maxRequests; i++){
        try {
            let result = await getFollowers(channel, cursor);
            if(result.follows.length) {
                allFollows = allFollows.concat(result.follows);
                cursor = result.cursor;
                console.log(allFollows.length + " / " + result.total);
            } else {
                break;
            }
        } catch(e) {
            // Our API sometimes returns 500... by not updating cursor, we make the same request.
            i--;
        }
    }
    return allFollows;
}

// This is abstracted out to intentionally NOT use async/await, so that both threads can be run in parallel.
async function getTheFollowers(channelA, channelB) {
    return Promise.all([
        getAllFollowers(channelA),
        getAllFollowers(channelB)
    ]);
}

async function getCommonFollowers(channelA, channelB) {
    let [ channelAFollows, channelBFollows ] = await getTheFollowers(channelA, channelB);
    let channelATable = {};
    for(let i = 0; i < channelAFollows.length; i++) {
        channelATable[channelAFollows[i].user.display_name] = true;
    }
    let commons = channelBFollows.filter((follow)=> channelATable[follow.user.display_name]);
    console.log(commons.map((follow) => follow.user.display_name));

}


