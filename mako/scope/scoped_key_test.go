package scope

import (
	"testing"

	mk "code.justin.tv/commerce/mako/twirp"
	. "github.com/smartystreets/goconvey/convey"
)

func TestScopedKeys(t *testing.T) {
	Convey("Test scoped keys", t, func() {
		Convey("Test marshalling", func() {
			Convey("with valid inputs", func() {
				scopedKey := mk.ScopedKey{
					Scope: "/scope/",
					Key:   "key",
				}
				marshalledScopedKey, err := Marshall(scopedKey)
				So(err, ShouldBeNil)
				So(marshalledScopedKey, ShouldEqual, "/scope/key")
			})
			Convey("with more valid inputs", func() {
				scopedKey := mk.ScopedKey{
					Scope: "scope/",
					Key:   "key",
				}
				marshalledScopedKey, err := Marshall(scopedKey)
				So(err, ShouldBeNil)
				So(marshalledScopedKey, ShouldEqual, "scope/key")
			})
			Convey("with invalid key", func() {
				scopedKey := mk.ScopedKey{
					Scope: "scope/",
					Key:   "/key",
				}
				_, err := Marshall(scopedKey)
				So(err, ShouldNotBeNil)
			})
			Convey("with another invalid key", func() {
				scopedKey := mk.ScopedKey{
					Scope: "scope/",
					Key:   "key/",
				}
				_, err := Marshall(scopedKey)
				So(err, ShouldNotBeNil)
			})
			Convey("with invalid scope", func() {
				scopedKey := mk.ScopedKey{
					Scope: "scope",
					Key:   "key",
				}
				_, err := Marshall(scopedKey)
				So(err, ShouldNotBeNil)
			})
			Convey("with empty key", func() {
				scopedKey := mk.ScopedKey{
					Scope: "/scope/",
					Key:   "",
				}
				marshalledScopedKey, err := Marshall(scopedKey)
				So(err, ShouldBeNil)
				So(marshalledScopedKey, ShouldEqual, "/scope/")
			})
			Convey("with empty scope", func() {
				scopedKey := mk.ScopedKey{
					Scope: "",
					Key:   "key",
				}
				_, err := Marshall(scopedKey)
				So(err, ShouldNotBeNil)
			})
			Convey("with empty both", func() {
				scopedKey := mk.ScopedKey{
					Scope: "",
					Key:   "",
				}
				_, err := Marshall(scopedKey)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("Test unmarshalling", func() {
			Convey("with simple input", func() {
				scopedKey, err := Unmarshall("/scope/key")
				So(err, ShouldBeNil)
				So(scopedKey.Scope, ShouldEqual, "/scope/")
				So(scopedKey.Key, ShouldEqual, "key")
			})
			Convey("with nested input", func() {
				scopedKey, err := Unmarshall("/scope/first/second/key")
				So(err, ShouldBeNil)
				So(scopedKey.Scope, ShouldEqual, "/scope/first/second/")
				So(scopedKey.Key, ShouldEqual, "key")
			})
			Convey("with many separators", func() {
				scopedKey, err := Unmarshall("/scope/////key")
				So(err, ShouldBeNil)
				So(scopedKey.Scope, ShouldEqual, "/scope/////")
				So(scopedKey.Key, ShouldEqual, "key")
			})
			Convey("with no separators", func() {
				_, err := Unmarshall("foobar")
				So(err, ShouldNotBeNil)
			})
			Convey("with empty string", func() {
				_, err := Unmarshall("")
				So(err, ShouldNotBeNil)
			})
			Convey("with 1 separator separators", func() {
				scopedKey, err := Unmarshall("foo/bar")
				So(err, ShouldBeNil)
				So(scopedKey.Scope, ShouldEqual, "foo/")
				So(scopedKey.Key, ShouldEqual, "bar")
			})
			Convey("with empty key", func() {
				scopedKey, err := Unmarshall("foo/")
				So(err, ShouldBeNil)
				So(scopedKey.Scope, ShouldEqual, "foo/")
				So(scopedKey.Key, ShouldEqual, "")
			})
			Convey("with empty scope", func() {
				scopedKey, err := Unmarshall("/bar")
				So(err, ShouldBeNil)
				So(scopedKey.Scope, ShouldEqual, "/")
				So(scopedKey.Key, ShouldEqual, "bar")
			})
			Convey("with both empty", func() {
				scopedKey, err := Unmarshall("/")
				So(err, ShouldBeNil)
				So(scopedKey.Scope, ShouldEqual, "/")
				So(scopedKey.Key, ShouldEqual, "")
			})
			Convey("with only separators", func() {
				scopedKey, err := Unmarshall("////")
				So(err, ShouldBeNil)
				So(scopedKey.Scope, ShouldEqual, "////")
				So(scopedKey.Key, ShouldEqual, "")
			})
		})
	})
}
