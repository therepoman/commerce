package scope

import "fmt"

const (
	CommercePurchase = "/commerce-purchase/" // Scope for Twitch Commerce purchase events. Purchases happen on the RespawnStoreWebsite
)

var validEventScopes = map[string]bool{
	CommercePurchase: true,
}

func ValidateEventScope(input string) error {
	if validEventScopes[input] {
		return nil
	}
	return fmt.Errorf("Invalid event scope: %s", input)
}
