package scope

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestEventScopes(t *testing.T) {
	Convey("Test validateEventScope", t, func() {
		Convey("When valid scope received", func() {
			err := ValidateEventScope("/commerce-purchase/")
			So(err, ShouldBeNil)
		})
		Convey("When invalid scope received", func() {
			err := ValidateEventScope("commerce-purchase")
			So(err, ShouldNotBeNil)
		})
		Convey("When another invalid scope received", func() {
			err := ValidateEventScope("alkfjasklfj")
			So(err, ShouldNotBeNil)
		})
		Convey("When empty string received", func() {
			err := ValidateEventScope("")
			So(err, ShouldNotBeNil)
		})
	})
}
