package scope

import (
	"fmt"
	"strings"
)

const (
	BadgeSets                           = "/badge_sets/"                              // Root scope for badge set entitlements. Badges are displayed on Twitch.tv in chat
	BadgeSetsCommerce                   = "/badge_sets/commerce/"                     // Scope for commerce-based badge entitlements. Sent by TFS.
	BadgeSetsCommerceCrate              = "/badge_sets/commerce/crate/"               // Scope for badges coming from crates. Sent by TFS.
	BadgeSetsCommerceCampaignDefault    = "/badge_sets/commerce/campaign/"            // Scope for badges coming from Fortuna Campaigns.
	EmoteSets                           = "/emote_sets/"                              // Root scope for emote set entitlements. Emotes are used on Twitch.tv in chat
	EmoteSetsCommerce                   = "/emote_sets/commerce/"                     // Scope for commerce-based emote entitlements. Sent by TFS.
	EmoteSetsCommerceCampaignDefault    = "/emote_sets/commerce/campaign/"            // Scope for emotes coming from Fortuna Campaigns,
	EmoteSetsCommerceCrate              = "/emote_sets/commerce/crate/"               // Scope for emotes coming from crates. Sent by TFS.
	EmoteSetsCommerceCrateHalloween2017 = "/emote_sets/commerce/crate/halloween2017/" // Scope for emotes coming from crates for Halloween 2017 event
	EmoteSetsCommerceCrateHoliday2017   = "/emote_sets/commerce/crate/holiday2017/"   // Scope for emotes coming from Holiday 2017 event
	EmoteSetsClips                      = "/emote_sets/clips/"                        // Scope for clips emote entitlements.
	EmoteSetsBitsEvents                 = "/emote_sets/bits/event/"                   // Scope for cheering related emote entitlements.
)

var validEntitlementScopes = map[string]bool{
	BadgeSets:                           true,
	BadgeSetsCommerce:                   true,
	BadgeSetsCommerceCrate:              true,
	EmoteSets:                           true,
	EmoteSetsCommerce:                   true,
	EmoteSetsCommerceCrate:              true,
	EmoteSetsCommerceCrateHalloween2017: true,
	EmoteSetsCommerceCrateHoliday2017:   true,
	EmoteSetsClips:                      true,
	EmoteSetsBitsEvents:                 true,
}

// ValidateEntitlementScope returns an error if the input scope is invalid, returns nil otherwise
func ValidateEntitlementScope(input string) error {
	if validEntitlementScopes[input] {
		return nil
	}
	if strings.HasPrefix(input, EmoteSetsCommerceCampaignDefault) || strings.HasPrefix(input, BadgeSetsCommerceCampaignDefault) {
		return nil
	}
	return fmt.Errorf("Invalid entitlement scope: %s", input)
}
