package scope

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestEntitlementScopes(t *testing.T) {
	Convey("Test validateEntitlementScope", t, func() {
		Convey("When valid scope received", func() {
			err := ValidateEntitlementScope(EmoteSets)
			So(err, ShouldBeNil)
		})
		Convey("When invalid scope received", func() {
			err := ValidateEntitlementScope("emote_sets")
			So(err, ShouldNotBeNil)
		})
		Convey("When another invalid scope received", func() {
			err := ValidateEntitlementScope("alkfjasklfj")
			So(err, ShouldNotBeNil)
		})
		Convey("When empty string received", func() {
			err := ValidateEntitlementScope("")
			So(err, ShouldNotBeNil)
		})
		Convey("When scope begins with EmoteSetsCommerceCampaignDefault", func() {
			err := ValidateEntitlementScope(EmoteSetsCommerceCampaignDefault + "anything")
			So(err, ShouldBeNil)
		})
		Convey("When scope begins with BadgeSetsCommerceCampaignDefault", func() {
			err := ValidateEntitlementScope(BadgeSetsCommerceCampaignDefault + "anything")
			So(err, ShouldBeNil)
		})
	})
}
