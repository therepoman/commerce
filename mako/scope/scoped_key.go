package scope

import (
	mk "code.justin.tv/commerce/mako/twirp"

	"fmt"
	"strings"
)

const separator = "/"

// Convert the ScopedKey struct to string format
// Key must not contain the separator character "/"
// Scope must have separator character "/" as last character in string. e.g. "myscope/"
// Scope may optionally contain multiple separator characters. e.g. "/my/scope/"
func Marshall(scopedKey mk.ScopedKey) (string, error) {
	if strings.Contains(scopedKey.Key, separator) {
		return "", fmt.Errorf("Key must not contain separator character: %s, %s", separator, scopedKey.Key)
	}

	if len(scopedKey.Scope) == 0 {
		return "", fmt.Errorf("Scope must contain atleast one separator character: %s, %s", separator, scopedKey.Scope)
	}

	lastChar := scopedKey.Scope[len(scopedKey.Scope)-1:]
	if lastChar != separator {
		return "", fmt.Errorf("Scope must contain atleast one separator character: %s, %s", separator, scopedKey.Scope)
	}

	inputs := []string{
		scopedKey.Scope,
		scopedKey.Key,
	}
	return strings.Join(inputs, ""), nil
}

// Convert string to ScopedKey format
func Unmarshall(input string) (mk.ScopedKey, error) {
	index := strings.LastIndex(input, separator)

	if index == -1 {
		return mk.ScopedKey{}, fmt.Errorf("Invalid format detected for string: %s", input)
	}

	scope := input[:index+1]
	key := input[index+1:]

	return mk.ScopedKey{
		Scope: scope,
		Key:   key,
	}, nil
}
