package localdb

import (
	"context"
	"errors"

	mako "code.justin.tv/commerce/mako/internal"
)

type EmoteModifierDB struct {
	emoteModifierGroups []mako.EmoteModifierGroup
}

func NewEmoteModifierDB() *EmoteModifierDB {
	return &EmoteModifierDB{
		emoteModifierGroups: make([]mako.EmoteModifierGroup, 0),
	}
}

func (db *EmoteModifierDB) CreateGroups(ctx context.Context, groups ...mako.EmoteModifierGroup) ([]mako.EmoteModifierGroup, error) {
	for _, group := range groups {
		found := false

		for _, emg := range db.emoteModifierGroups {
			if group.ID == emg.ID {
				found = true
			}
		}

		if !found {
			db.emoteModifierGroups = append(db.emoteModifierGroups, group)
		}
	}

	return nil, nil
}

func (db *EmoteModifierDB) UpdateGroups(ctx context.Context, groups ...mako.EmoteModifierGroup) ([]mako.EmoteModifierGroup, error) {
	updated := make([]mako.EmoteModifierGroup, 0)

	for _, group := range groups {
		for i, emg := range db.emoteModifierGroups {
			if group.ID == emg.ID {
				db.emoteModifierGroups[i].OwnerID = group.OwnerID
				db.emoteModifierGroups[i].Modifiers = group.Modifiers
				db.emoteModifierGroups[i].SourceEmoteGroupIDs = group.SourceEmoteGroupIDs

				updated = append(updated, db.emoteModifierGroups[i])
			}
		}
	}

	return updated, nil
}

func (db *EmoteModifierDB) ReadByIDs(ctx context.Context, ids ...string) ([]mako.EmoteModifierGroup, error) {
	results := make([]mako.EmoteModifierGroup, 0)

	for _, id := range ids {
		for _, emg := range db.emoteModifierGroups {
			if id == emg.ID {
				results = append(results, emg)
			}
		}
	}

	return results, nil
}

func (db *EmoteModifierDB) ReadByOwnerIDs(ctx context.Context, ownerIDs ...string) ([]mako.EmoteModifierGroup, error) {
	results := make([]mako.EmoteModifierGroup, 0)

	for _, ownerID := range ownerIDs {
		for _, emg := range db.emoteModifierGroups {
			if ownerID == emg.OwnerID {
				results = append(results, emg)
			}
		}
	}

	return results, nil
}

func (db *EmoteModifierDB) DeleteByIDs(ctx context.Context, ids ...string) error {
	results := make([]mako.EmoteModifierGroup, 0)

	for _, id := range ids {
		for _, emg := range db.emoteModifierGroups {
			if id != emg.ID {
				results = append(results, emg)
			}
		}
	}

	db.emoteModifierGroups = results
	return nil
}

// Caching related functionality is not supported when running localdb tests
func (db *EmoteModifierDB) ReadEntitledGroupsForUser(ctx context.Context, userID string) ([]mako.EmoteModifierGroup, error) {
	return nil, errors.New("not supported")
}

func (db *EmoteModifierDB) WriteEntitledGroupsForUser(ctx context.Context, userID string, groups ...mako.EmoteModifierGroup) error {
	return errors.New("not supported")
}

func (db *EmoteModifierDB) InvalidateEntitledGroupsCacheForUser(userID string) error {
	return errors.New("not supported")
}

func MakeEmoteModifierDB(ctx context.Context) (mako.EmoteModifiersDB, error) {
	return NewEmoteModifierDB(), nil
}
