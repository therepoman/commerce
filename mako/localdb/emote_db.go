package localdb

import (
	"context"
	"errors"

	mako "code.justin.tv/commerce/mako/internal"
)

type emoteID string

type EmoteDB struct {
	emotes map[emoteID]mako.Emote
}

func NewEmoteDB() *EmoteDB {
	return &EmoteDB{
		emotes: make(map[emoteID]mako.Emote, 0),
	}
}

func (db *EmoteDB) ReadByIDs(ctx context.Context, ids ...string) ([]mako.Emote, error) {
	emotes := make([]mako.Emote, 0)

	for _, id := range ids {
		emote, ok := db.emotes[emoteID(id)]
		if ok {
			emotes = append(emotes, emote)
		}
	}

	return emotes, nil
}

func (db *EmoteDB) ReadByCodes(ctx context.Context, codes ...string) ([]mako.Emote, error) {
	emotes := make([]mako.Emote, 0)

	for _, code := range codes {
		for _, emote := range db.emotes {
			if code == emote.Code {
				emotes = append(emotes, emote)
			}
		}
	}

	return emotes, nil
}

func (db *EmoteDB) ReadByGroupIDs(ctx context.Context, ids ...string) ([]mako.Emote, error) {
	var emotes []mako.Emote
	for _, id := range ids {
		e, err := db.Query(ctx, mako.EmoteQuery{
			GroupID: id,
		})

		if err != nil {
			return nil, err
		}

		emotes = append(emotes, e...)
	}

	return emotes, nil
}

func (db *EmoteDB) WriteEmotes(ctx context.Context, emotes ...mako.Emote) ([]mako.Emote, error) {
	for _, emote := range emotes {
		db.emotes[emoteID(emote.ID)] = emote
	}

	return emotes, nil
}

func (db *EmoteDB) DeleteEmote(ctx context.Context, id string) (mako.Emote, error) {
	emote := db.emotes[emoteID(id)]

	delete(db.emotes, emoteID(id))

	return emote, nil
}

func (db *EmoteDB) UpdateCode(ctx context.Context, id, code, suffix string) (mako.Emote, error) {
	modifiedEmote, ok := db.emotes[emoteID(id)]
	if !ok {
		return mako.Emote{}, errors.New("Not found")
	}
	modifiedEmote.Code = code
	modifiedEmote.Suffix = suffix
	db.emotes[emoteID(id)] = modifiedEmote

	return modifiedEmote, nil
}

func (db *EmoteDB) UpdatePrefix(ctx context.Context, id, code, prefix string) (mako.Emote, error) {
	modifiedEmote, ok := db.emotes[emoteID(id)]
	if !ok {
		return mako.Emote{}, errors.New("Not found")
	}
	modifiedEmote.Code = code
	modifiedEmote.Prefix = prefix
	db.emotes[emoteID(id)] = modifiedEmote

	return modifiedEmote, nil
}

func (db *EmoteDB) UpdateState(ctx context.Context, id string, state mako.EmoteState) (mako.Emote, error) {
	modifiedEmote, ok := db.emotes[emoteID(id)]
	if !ok {
		return mako.Emote{}, errors.New("Not found")
	}
	modifiedEmote.State = state
	db.emotes[emoteID(id)] = modifiedEmote

	return modifiedEmote, nil
}

func (db *EmoteDB) UpdateEmote(ctx context.Context, id string, params mako.EmoteUpdate) (mako.Emote, error) {
	modifiedEmote, ok := db.emotes[emoteID(id)]
	if !ok {
		return mako.Emote{}, errors.New("Not found")
	}

	numUpdates := 0

	if params.GroupID != nil {
		modifiedEmote.GroupID = *params.GroupID
		numUpdates++
	}
	if params.State != nil {
		modifiedEmote.State = *params.State
		numUpdates++
	}
	if params.Order != nil {
		modifiedEmote.Order = *params.Order
		numUpdates++
	}
	if params.EmotesGroup != nil {
		modifiedEmote.EmotesGroup = *params.EmotesGroup
		numUpdates++
	}

	if numUpdates < 1 {
		return mako.Emote{}, errors.New("cannot update emote due to unspecified update parameters")
	}

	db.emotes[emoteID(id)] = modifiedEmote

	return modifiedEmote, nil
}

func (db *EmoteDB) UpdateEmotes(ctx context.Context, emoteUpdatesByID map[string]mako.EmoteUpdate) ([]mako.Emote, error) {
	var emotesToExpire []mako.Emote
	var ids []string
	for id := range emoteUpdatesByID {
		ids = append(ids, id)
	}

	existingEmotes, err := db.ReadByIDs(ctx, ids...)
	if err != nil {
		return nil, err
	}

	emotesToExpire = append(emotesToExpire, existingEmotes...)

	for id, emoteUpdate := range emoteUpdatesByID {
		emote, err := db.UpdateEmote(ctx, id, emoteUpdate)
		if err != nil {
			return nil, err
		}
		emotesToExpire = append(emotesToExpire, emote)
	}

	return emotesToExpire, nil
}

func (db *EmoteDB) Query(ctx context.Context, query mako.EmoteQuery) ([]mako.Emote, error) {
	emotes := make([]mako.Emote, 0)
	for _, emote := range db.emotes {
		if query.Code != "" && emote.Code != query.Code {
			continue
		} else if query.OwnerID != "" && emote.OwnerID != query.OwnerID {
			continue
		} else if query.GroupID != "" && emote.GroupID != query.GroupID {
			continue
		}

		if query.Domain != "" && emote.Domain != query.Domain {
			continue
		}

		if query.EmoteGroup != "" && emote.EmotesGroup != query.EmoteGroup {
			continue
		}

		if query.AssetType != "" && emote.AssetType != query.AssetType {
			continue
		}

		if len(query.States) != 0 {
			found := false
			for _, state := range query.States {
				if state == emote.State {
					found = true
				}
			}

			if !found {
				continue
			}
		}

		emotes = append(emotes, emote)
	}

	return emotes, nil
}

func (db *EmoteDB) Close() error {
	return nil
}

func MakeEmoteDB(ctx context.Context, emotes ...mako.Emote) (mako.EmoteDB, func(), error) {
	db := NewEmoteDB()

	if _, err := db.WriteEmotes(ctx, emotes...); err != nil {
		return nil, nil, err
	}

	return db, func() {}, nil
}
