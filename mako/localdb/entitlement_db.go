package localdb

import (
	"context"

	mako "code.justin.tv/commerce/mako/internal"
)

type EntitlementDB struct {
	entitlements []mako.Entitlement
}

func NewEntitlementDB() *EntitlementDB {
	return &EntitlementDB{
		entitlements: make([]mako.Entitlement, 0),
	}
}

func (db *EntitlementDB) WriteEntitlements(ctx context.Context, entitlements ...mako.Entitlement) error {
	var found bool
	for i, dbEntitlement := range db.entitlements {
		for _, entitlement := range entitlements {
			if dbEntitlement.ID == entitlement.ID {
				db.entitlements[i] = entitlement
				found = true
			}
		}
	}

	if !found {
		db.entitlements = append(db.entitlements, entitlements...)
	}

	return nil
}

func (db *EntitlementDB) ReadByIDs(ctx context.Context, ownerID string, ids ...string) ([]mako.Entitlement, error) {
	entitlements := make([]mako.Entitlement, 0)

	for _, entitlement := range db.entitlements {
		if entitlement.OwnerID != ownerID {
			continue
		}

		var found bool
		for _, id := range ids {
			if entitlement.ID == id {
				found = true
			}
		}

		if !found {
			continue
		}

		entitlements = append(entitlements, entitlement)
	}

	return entitlements, nil
}

func (db *EntitlementDB) ReadByOwnerID(ctx context.Context, ownerID string, channelID string) ([]mako.Entitlement, error) {
	entitlements := make([]mako.Entitlement, 0)

	for _, entitlement := range db.entitlements {
		if entitlement.OwnerID != ownerID {
			continue
		}

		// TODO: in the future handle entitlement scope as a field like `entitlement.scope == ChannelOnly` rather than using this hack
		if entitlement.Source == mako.FollowerSource && channelID != entitlement.ChannelID {
			continue
		}

		entitlements = append(entitlements, entitlement)
	}

	return entitlements, nil
}

func MakeEntitlementDB(ctx context.Context, entitlements ...mako.Entitlement) (mako.EntitlementDB, func(), error) {
	db := NewEntitlementDB()

	if err := db.WriteEntitlements(ctx, entitlements...); err != nil {
		return nil, nil, err
	}

	return db, func() {}, nil
}
