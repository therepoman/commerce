package localdb

import (
	"context"

	mako "code.justin.tv/commerce/mako/internal"
)

type EmoteGroupDB struct {
	byID      map[string]mako.EmoteGroup
	byOwnerID map[string][]mako.EmoteGroup
}

func NewEmoteGroupDB() *EmoteGroupDB {
	return &EmoteGroupDB{
		byID:      make(map[string]mako.EmoteGroup, 0),
		byOwnerID: make(map[string][]mako.EmoteGroup, 0),
	}
}

func (e *EmoteGroupDB) CreateEmoteGroup(ctx context.Context, group mako.EmoteGroup) (emoteGroup mako.EmoteGroup, conditionalCheckFailed bool, err error) {
	_, alreadyExists := e.byID[group.GroupID]
	if alreadyExists {
		return emoteGroup, true, nil
	}

	e.byID[group.GroupID] = group
	e.byOwnerID[group.OwnerID] = append(e.byOwnerID[group.OwnerID], group)
	return group, false, nil
}

func (e *EmoteGroupDB) GetEmoteGroups(ctx context.Context, ids ...string) ([]mako.EmoteGroup, error) {
	groups := make([]mako.EmoteGroup, 0, len(ids))
	for _, id := range ids {
		group, ok := e.byID[id]
		if ok {
			if group.GroupType == "" {
				group.GroupType = mako.EmoteGroupStatic
			}
			groups = append(groups, group)
		}
	}
	return groups, nil
}

func (e *EmoteGroupDB) GetFollowerEmoteGroup(ctx context.Context, ownerID string) (*mako.EmoteGroup, error) {
	groups, ok := e.byOwnerID[ownerID]
	if !ok {
		return nil, nil
	}
	for _, group := range groups {
		if group.Origin == mako.EmoteGroupOriginFollower {
			return &group, nil
		}
	}
	return nil, nil
}

func (e *EmoteGroupDB) DeleteEmoteGroup(ctx context.Context, id string) error {
	group, ok := e.byID[id]
	if ok {
		delete(e.byOwnerID, group.OwnerID)
	}

	delete(e.byID, id)
	return nil
}

func MakeEmoteGroupDB(ctx context.Context, emoteGroups ...mako.EmoteGroup) (mako.EmoteGroupDB, func(), error) {
	db := NewEmoteGroupDB()

	for _, emoteGroup := range emoteGroups {
		if _, _, err := db.CreateEmoteGroup(ctx, emoteGroup); err != nil {
			return nil, nil, err
		}
	}

	return db, func() {}, nil
}
