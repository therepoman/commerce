package localdb

import (
	"testing"

	"code.justin.tv/commerce/mako/tests"
)

func TestLocalEntitlementDB(t *testing.T) {
	tests.TestEntitlementDB(t, MakeEntitlementDB)
}
