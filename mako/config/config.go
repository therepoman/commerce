package config

import (
	"encoding/json"
	"fmt"
	"os"

	validator "gopkg.in/validator.v2"

	mk "code.justin.tv/commerce/mako/twirp"
)

// Configuration defines the config values
type Configuration struct {
	Preload                        bool
	WaitForBloomFilterOnDeploy     bool
	AWSRegion                      string   `validate:"nonzero"`
	BadgeServiceMaxIdleConnections int      `validate:"nonzero"`
	BadgeServiceEndpoint           string   `validate:"nonzero"`
	EnvironmentPrefix              string   `validate:"nonzero"`
	RedisHosts                     []string `validate:"nonzero"`
	RedisNodeConnectionPoolSize    int      `validate:"nonzero"`
	RedisPoolTimeoutSeconds        int      `validate:"nonzero"`
	RedisDialTimeoutSeconds        int      `validate:"nonzero"`
	RedisReadTimeoutSeconds        int      `validate:"nonzero"`
	RedisWriteTimeoutSeconds       int      `validate:"nonzero"`
	RedisMaxRetryCount             int      `validate:"nonzero"`

	// EmoteDBCacheRedisHost is to separate out the new redis features in Mako.
	EmoteDBCacheRedisHost string `validate:"nonzero"`

	RailsClientID                string `validate:"nonzero"`
	RailsGetEmotesBatchSize      int    `validate:"min=0"`
	EmoteDBDynamoTable           string `validate:"nonzero"`
	EmoteGroupDBDynamoTable      string `validate:"nonzero"`
	EmoteModifierGroupsTable     string `validate:"nonzero"`
	EmoteStandingOverridesTable  string `validate:"nonzero"`
	EmoteRedisConnectionPoolSize int    `validate:"nonzero"`
	EmoteRedisMinIdleConnections int    `validate:"nonzero"`

	EmoteModifiersRedisConnectionPoolSize int `validate:"nonzero"`
	EmoteModifiersRedisMinIdleConnections int `validate:"nonzero"`

	FollowingEntitlementsDBRedisConnectionPoolSize int `validate:"nonzero"`
	FollowingEntitlementsDBRedisMinIdleConnections int `validate:"nonzero"`

	CloudwatchBufferSize               int                             `validate:"nonzero"`
	CloudwatchBatchSize                int                             `validate:"nonzero"`
	CloudwatchFlushIntervalSeconds     int                             `validate:"nonzero"`
	CloudwatchFlushCheckDelayMs        int                             `validate:"nonzero"`
	CloudwatchEmergencyFlushPercentage float64                         `validate:"nonzero"`
	CloudwatchRegion                   string                          `validate:"nonzero"`
	SandstormRoleArn                   string                          `validate:"nonzero"`
	SandstormEnvironment               string                          `validate:"nonzero"`
	SalesforceClientID                 string                          `validate:"nonzero"`
	SalesforceClientSecret             string                          `validate:"nonzero"`
	SalesforceUsername                 string                          `validate:"nonzero"`
	SalesforcePassword                 string                          `validate:"nonzero"`
	MakoEventsSNS                      string                          `validate:"nonzero"`
	PrefixUpdatesSNS                   string                          `validate:"nonzero"`
	EventEmoteCollections              map[string]EventEmoteCollection `validate:"nonzero"`
	PubsubHost                         string                          `validate:"nonzero"`
	RollbarToken                       string                          `validate:"nonzero"`
	UploadServiceURL                   string                          `validate:"nonzero"`
	UploadCallbackSNSTopic             string                          `validate:"nonzero"`
	UploadS3Host                       string                          `validate:"nonzero"`
	UploadS3BucketName                 string                          `validate:"nonzero"`
	AnimatedEmoteFramesS3BucketName    string                          `validate:"nonzero"`
	SubscriptionsHost                  string                          `validate:"nonzero"`
	PaydayHost                         string                          `validate:"nonzero"`
	PaintHost                          string                          `validate:"nonzero"`
	MateriaHost                        string                          `validate:"nonzero"`
	FollowingServiceHost               string                          `validate:"nonzero"`
	UserServiceHost                    string                          `validate:"nonzero"`
	DartReceiverHost                   string                          `validate:"nonzero"`
	ClearedGlobals                     []*mk.Emote
	CommunityPointsTicketProductID     string          `validate:"nonzero"`
	CommunityPointsEmoteSetID          string          `validate:"nonzero"`
	CommunityPointsOwnerID             string          `validate:"nonzero"`
	SpecialEventOwnerIDs               map[string]bool `validate:"nonzero"`
	InferenceServiceEndpoint           string          `validate:"nonzero"`
	RipleyServiceEndpoint              string          `validate:"nonzero"`
	PepperServiceEndpoint              string          `validate:"nonzero"`
	SalesforceOauthHost                string          `validate:"nonzero"`
	MateriaDenylist                    map[string]bool `validate:"nonzero"`
	PrefixEditabilityOverrideList      map[string]bool `validate:"nonzero"`
	ChemtrailLambdaARN                 string          `validate:"nonzero"`

	AutoprofS3BucketName            string `validate:"nonzero"`
	ExperimentOverridesS3BucketName string `validate:"nonzero"`

	// Hystrix Timeout Duration
	PubsubTimeoutMilliseconds                                   int `validate:"nonzero"`
	DynamoDBTimeoutMilliseconds                                 int `validate:"nonzero"`
	MateriaTimeoutMilliseconds                                  int `validate:"nonzero"`
	FollowingServiceTimeoutMilliseconds                         int `validate:"nonzero"`
	InferenceAcceptableNamesTimeoutMilliseconds                 int `validate:"nonzero"`
	InferenceClassifyEmoteTimeoutMilliseconds                   int `validate:"nonzero"`
	PepperTimeoutMilliseconds                                   int `validate:"nonzero"`
	SubscriptionsGetChannelEmoteLimitsTimeoutMilliseconds       int `validate:"nonzero"`
	SubscriptionsGetChannelProductsTimeoutMilliseconds          int `validate:"nonzero"`
	PaydayGetBitsTierEmoteGroupsTimeoutMilliseconds             int `validate:"nonzero"`
	EmoteDBDynamoGroupHystrixTimeoutMilliseconds                int `validate:"nonzero"`
	EmoteDBDynamoCodeHystrixTimeoutMilliseconds                 int `validate:"nonzero"`
	EmoteDBRedisGetHystrixTimeoutMilliseconds                   int `validate:"nonzero"`
	EmoteDBRedisCodeGetHystrixTimeoutMilliseconds               int `validate:"nonzero"`
	EmoteModifierDBDynamoGetHystrixTimeoutMilliseconds          int `validate:"nonzero"`
	EmoteModifierDBRedisGetHystrixTimeoutMilliseconds           int `validate:"nonzero"`
	EmoteModifierDBRedisOwnerHystrixTimeoutMilliseconds         int `validate:"nonzero"`
	FollowingEntitlementsDBRedisIDHystrixTimeoutMilliseconds    int `validate:"nonzero"`
	FollowingEntitlementsDBRedisOwnerHystrixTimeoutMilliseconds int `validate:"nonzero"`
	EntitlementDBRedisGetHystrixTimeoutMilliseconds             int `validate:"nonzero"`
	EntitlementDBMateriaGetHystrixTimeoutMilliseconds           int `validate:"nonzero"`
	EntitlementDBLegacyGetHystrixTimeoutMilliseconds            int `validate:"nonzero"`
	SalesforceInGoodStandingHystrixTimeoutMilliseconds          int `validate:"nonzero"`
	RipleyGetPayoutTypeHystrixTimeoutMilliseconds               int `validate:"nonzero"`
	ChemtrailGetEmoteUsageHystrixTimeoutMilliseconds            int `validate:"nonzero"`

	ConfigS3Endpoint                    string `validate:"nonzero"`
	ConfigS3Bucket                      string `validate:"nonzero"`
	CampaignEmoteOwnersConfigS3Filename string `validate:"nonzero"`
	KnownOrVerifiedBotsS3Filename       string `validate:"nonzero"`

	// Default Emote Images
	DefaultEmoteImagesDataS3Endpoint string `validate:"nonzero"`
	DefaultEmoteImagesDataS3Bucket   string `validate:"nonzero"`
	DefaultEmoteImagesDataS3Filename string `validate:"nonzero"`
	DefaultImageLibraryS3Bucket      string `validate:"nonzero"`

	// S2S
	S2SServiceName        string
	S2SEnabled            bool
	S2SPassthroughEnabled bool

	// SQS
	EventsSQSWorkerCount        int    `validate:"nonzero"`
	EventsSQSQueueName          string `validate:"nonzero"`
	PrefixUpdatesSQSWorkerCount int    `validate:"nonzero"`
	PrefixUpdatesSQSQueueName   string `validate:"nonzero"`
	UserDestroySQSQueueURL      string `validate:"nonzero"`
	DeleteS3ImagesQueueURL      string `validate:"nonzero"`

	// SFN
	UserDestroySFNARN                 string `validate:"nonzero"`
	AnimatedEmoteFrameSplittingSFNARN string `validate:"nonzero"`
}

// EventEmoteCollection is for defining different emote emote events
type EventEmoteCollection struct {
	EmoteSets     map[string]bool `validate:"nonzero"`
	BonusEmoteSet string          `validate:"nonzero"`
}

type ClearedGlobals struct {
	Emoticons []*mk.Emote
}

const (
	localConfigFilePath  = "/src/code.justin.tv/commerce/mako/config/%s.json"
	globalConfigFilePath = "/etc/mako/config/%s.json"

	unspecifiedEnvironment = ""
	devEnvironment         = "dev"
)

var localClearedGlobalsFilePath = fmt.Sprintf(localConfigFilePath, "cleared_globals")
var globalClearedGlobalsFilePath = fmt.Sprintf(globalConfigFilePath, "cleared_globals")

// GetEnvironment returns environment type
func GetEnvironment() string {
	env := os.Getenv("ENVIRONMENT")
	if env == unspecifiedEnvironment {
		return devEnvironment
	}
	return env
}

// LoadConfigForEnvironment pulls the Config data from the JSON config file
func LoadConfigForEnvironment(environment string) (*Configuration, error) {
	configFilePath, err := getConfigFilePath(environment)
	if err != nil {
		return nil, err
	}

	clearedGlobalsFilePath, err := getClearedGlobalsFilePath()
	if err != nil {
		return nil, err
	}
	return loadConfig(configFilePath, clearedGlobalsFilePath)
}

func getConfigFilePath(environment string) (string, error) {
	localFname := os.Getenv("GOPATH") + fmt.Sprintf(localConfigFilePath, environment)
	if _, err := os.Stat(localFname); !os.IsNotExist(err) {
		return localFname, nil
	}
	globalFname := fmt.Sprintf(globalConfigFilePath, environment)
	if _, err := os.Stat(globalFname); os.IsNotExist(err) {
		return "", err
	}
	return globalFname, nil
}

func getClearedGlobalsFilePath() (string, error) {
	localFname := os.Getenv("GOPATH") + localClearedGlobalsFilePath
	if _, err := os.Stat(localFname); !os.IsNotExist(err) {
		return localFname, nil
	}
	globalFname := globalClearedGlobalsFilePath
	if _, err := os.Stat(globalFname); os.IsNotExist(err) {
		return "", err
	}
	return globalFname, nil
}

func loadConfig(filepath, clearedGlobalsFilePath string) (*Configuration, error) {
	file, err := os.Open(filepath)
	if err != nil {
		return nil, fmt.Errorf("Failed to open config file: %v", err)
	}

	decoder := json.NewDecoder(file)
	configuration := &Configuration{}
	if err := decoder.Decode(configuration); err != nil {
		return nil, fmt.Errorf("Failed to parse configuration file: %v", err)
	}

	if err := validator.Validate(configuration); err != nil {
		return nil, fmt.Errorf("Failed to validate configuration file: %v", err)
	}

	clearedGlobalsFile, err := os.Open(clearedGlobalsFilePath)
	if err != nil {
		return nil, fmt.Errorf("Failed to open cleared globals file: %v", err)
	}

	decoder = json.NewDecoder(clearedGlobalsFile)
	clearedGlobals := &ClearedGlobals{}
	if err := decoder.Decode(clearedGlobals); err != nil {
		return nil, fmt.Errorf("Failed to parse cleared globals file: %v", err)
	}

	configuration.ClearedGlobals = clearedGlobals.Emoticons
	return configuration, nil
}
