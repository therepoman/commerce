package config

import (
	"bufio"
	"context"
	"encoding/json"
	"fmt"
	"hash/fnv"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/ssm"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/cenkalti/backoff"
	"github.com/pkg/errors"

	"code.justin.tv/commerce/dynamicconfig"
	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/mako/integration/user_utils"
	mako_dashboard "code.justin.tv/commerce/mako/twirp/dashboard"
)

type Dynamic struct {
	dynamicConfig         *dynamicconfig.DynamicConfig
	statter               statsd.Statter
	statsPrefix           string
	freemotesChannelsMap  map[string]bool
	freemotesMarketingMap map[string]bool
}

type DynamicS3Configs struct {
	CampaignEmoteOwnersDynamicS3Config   *Dynamic
	KnownOrVerifiedBotsDynamicS3Config   *Dynamic
	DefaultEmoteImageDataDynamicS3Config *Dynamic
}

const (
	SmilieFixKey                         = "smilie-fix"
	ThirdPartyEmoteEntitlementsAllowlist = "third-party-emote-entitlements-allowlist"
	GoldenKappaAlwaysOnKey               = "golden-kappa-always-on"
	FreemotesExperimentKey               = "freemotes-experiment"
	DefaultEmoteImagesDataKey            = "default-emote-images"
	EnableEmoteUpdatePubsubKey           = "enable-emote-update-pubsub"
	LibraryLimitsIncreasedKey            = "library-limits-increased"

	// Metrics
	startupSuccessMetricFormat = "dynconfig.%s.startup.success"
	startupFailureMetricFormat = "dynconfig.%s.startup.failure"
	startupLatencyMetricFormat = "dynconfig.%s.startup.latency"
	reloadSuccessMetricFormat  = "dynconfig.%s.refresh.success"
	reloadFailureMetricFormat  = "dynconfig.%s.refresh.failure"
	reloadLatencyMetricFormat  = "dynconfig.%s.refresh.latency"

	localTextFilePath  = "/src/code.justin.tv/commerce/mako/config/%s.txt"
	globalTextFilePath = "/etc/mako/config/%s.txt"

	maxReloadRetries = 3
)

var (
	configKeys = []string{
		SmilieFixKey,
		ThirdPartyEmoteEntitlementsAllowlist,
		GoldenKappaAlwaysOnKey,
		FreemotesExperimentKey,
		EnableEmoteUpdatePubsubKey,
		LibraryLimitsIncreasedKey,
	}
	localFreemotesExperimentFilePath  = fmt.Sprintf(localTextFilePath, "channel_list_freemotes_5_5")
	globalFreemotesExperimentFilePath = fmt.Sprintf(globalTextFilePath, "channel_list_freemotes_5_5")
	localFreemotesMarketingFilePath   = fmt.Sprintf(localTextFilePath, "addendum_userids_freemotes")
	globalFreemotesMarketingFilePath  = fmt.Sprintf(globalTextFilePath, "addendum_userids_freemotes")
)

type CampaignEmoteOwners interface {
	// CampaignEmoteOwner determines what (if any) the owner channel for the emote is
	CampaignEmoteOwner(emoteID string) string
}

type DefaultEmoteImagesDataConfigClient interface {
	GetDefaultEmoteImagesData() ([]*mako_dashboard.DefaultEmoteImage, error)
}

type KnownOrVerifiedBotsConfigClient interface {
	IsKnownOrVerifiedBot(userID string) bool
}

type DynamicConfigClient interface {
	SmilieFix(userID string) bool

	IsInFreemotesExperiment(userID string) bool

	// GetThirdPartyEmoteEntitlementsAllowlistMap returns a map containing all 3p client IDs that are allowed to entitle emotes
	// key: 3p client ID
	// value: list of all emote IDs the client from `key` is allowed to entitle
	GetThirdPartyEmoteEntitlementsAllowlistMap() map[string][]string

	// GetGoldenKappaAlwaysOnEnabled determines if the dial-up for always returning golden kappa is enabled
	GetGoldenKappaAlwaysOnEnabled(userID string) bool

	// EnableEmoteUpdatePubsub determines the rollout percentage for sending pubsub events when an emote is updated in dynamo
	EnableEmoteUpdatePubsub(channelID string) bool

	LibraryLimitIncreaseLaunched(userID string) bool
}

type smilieFixValue struct {
	AllowlistedUsers []string `json:"allowlisted_users"`
	RolloutPercent   int      `json:"rollout_percent"`
}

type rolloutConfig struct {
	AllowlistedUsers []string `json:"allowlisted_users"`
	RolloutPercent   int      `json:"rollout_percent"`
}

type freemotesExperimentValue struct {
	AllowlistedUsers []string `json:"allowlisted_users"`
	RolloutPercent   int      `json:"rollout_percent"`
}

type EnableEmoteUpdatePubsubRolloutConfig struct {
	AllowlistedUsers []string `json:"allowlisted_users"`
	RolloutPercent   int      `json:"rollout_percent"`
}

type LibraryLimitsIncreased struct {
	AllowlistedUsers []string `json:"allowlisted_users"`
	IsLaunched       bool     `json:"launched"`
}

func NewDynamic(region string, statter statsd.Statter) (*Dynamic, error) {
	// Timeout for initialization of the dynamic configuration
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*30)
	defer cancel()

	statsPrefix := "ssm"
	awsConfig := &aws.Config{Region: aws.String(region)}
	sess, err := session.NewSession(awsConfig)
	if err != nil {
		incrementStatter(statter, fmt.Sprintf(startupFailureMetricFormat, statsPrefix))
		return nil, errors.Wrap(err, "creating aws session")
	}

	// Create ssm client and source
	ssmClient := ssm.New(sess)
	ssmSource := dynamicconfig.NewSSMSource(configKeys, ssmClient)

	loadStart := time.Now()

	// Create dynamic config
	dynamicConfig, err := dynamicconfig.NewDynamicConfig(ctx, ssmSource, map[string]dynamicconfig.Transform{})
	if err != nil {
		// This error is recoverable because NewDynamicConfig still returns a working dynamic config object, so we don't return the error.
		// We passed in a valid source, so the config will be re-fetched on the next call to Reload.
		incrementStatter(statter, fmt.Sprintf(startupFailureMetricFormat, statsPrefix))
		logrus.WithError(err).Error("initializing dynamic config values -- using empty default")
	} else {
		incrementStatter(statter, fmt.Sprintf(startupSuccessMetricFormat, statsPrefix))
	}
	statter.TimingDuration(fmt.Sprintf(startupLatencyMetricFormat, statsPrefix), time.Since(loadStart), 1.0)
	logrus.Info("initialized dynamic config values")

	// Loading Freemotes Experiment Lists into Memory
	experimentListPath, err := getFreemotesExperimentFilePath(localFreemotesExperimentFilePath, globalFreemotesExperimentFilePath)
	if err != nil {
		return nil, err
	}

	freemotesExperimentMap, err := createFreemotesMapFromFilename(experimentListPath)
	if err != nil {
		return nil, err
	}

	marketingListPath, err := getFreemotesExperimentFilePath(localFreemotesMarketingFilePath, globalFreemotesMarketingFilePath)
	if err != nil {
		return nil, err
	}

	freemotesMarketingMap, err := createFreemotesMapFromFilename(marketingListPath)
	if err != nil {
		return nil, err
	}

	return &Dynamic{
		dynamicConfig:         dynamicConfig,
		statter:               statter,
		statsPrefix:           statsPrefix,
		freemotesChannelsMap:  freemotesExperimentMap,
		freemotesMarketingMap: freemotesMarketingMap,
	}, nil
}

func NewS3Dynamic(region string, endpoint string, bucket string, filename string, statter statsd.Statter) (*Dynamic, error) {
	// 10 second timeout for initialization of the dynamic configuration
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()

	statsPrefix := fmt.Sprintf("s3.%s", filename)
	// NewS3JSONSource has built-in retries, but we still see this fail every so often.
	// We retry this here too since an error here is non-recoverable.
	var s3JSONSource *dynamicconfig.S3JSONSource
	if err := backoff.Retry(func() error {
		var err error
		s3JSONSource, err = dynamicconfig.NewS3JSONSource(region, endpoint, bucket, filename)
		return err
	}, backoff.WithMaxRetries(backoff.WithContext(newBackoff(), ctx), 2)); err != nil {
		incrementStatter(statter, fmt.Sprintf(startupFailureMetricFormat, statsPrefix))
		logrus.WithError(err).Error("initializing s3 dynamic config source")
		return nil, err
	}

	loadStart := time.Now()

	dynamicConfig, err := dynamicconfig.NewDynamicConfig(ctx, s3JSONSource, nil)
	if err != nil {
		// This error is recoverable because NewDynamicConfig still returns a working dynamic config object, so we don't return the error.
		// We passed in a valid source, so the config will be re-fetched on the next call to Reload.
		incrementStatter(statter, fmt.Sprintf(startupFailureMetricFormat, statsPrefix))
		logrus.WithError(err).Error("initializing dynamic config values -- using empty default")
	} else {
		incrementStatter(statter, fmt.Sprintf(startupSuccessMetricFormat, statsPrefix))
	}
	statter.TimingDuration(fmt.Sprintf(startupLatencyMetricFormat, statsPrefix), time.Since(loadStart), 1.0)
	logrus.Info("initialized dynamic config values")

	return &Dynamic{
		dynamicConfig: dynamicConfig,
		statter:       statter,
		statsPrefix:   statsPrefix,
	}, nil
}

// Reload wraps the dynamicConfig.Reload() method and emits metrics on success/failure
func (d *Dynamic) Reload(ctx context.Context) error {
	if d == nil || d.dynamicConfig == nil {
		msg := "failed to reload dynamic config because dynamic wasn't initialized properly"
		err := errors.New(msg)
		incrementStatter(d.statter, fmt.Sprintf(reloadFailureMetricFormat, d.statsPrefix))
		logrus.WithError(err).Error()
		return err
	}

	start := time.Now()

	err := backoff.Retry(func() error {
		reloadCtx, cancel := context.WithTimeout(ctx, 2*time.Second)
		defer cancel()
		return d.dynamicConfig.Reload(reloadCtx)
	}, backoff.WithMaxRetries(backoff.WithContext(newBackoff(), ctx), maxReloadRetries))

	if err != nil {
		incrementStatter(d.statter, fmt.Sprintf(reloadFailureMetricFormat, d.statsPrefix))
		logrus.WithError(err).Error("failed to reload dynamic config")
	} else {
		incrementStatter(d.statter, fmt.Sprintf(reloadSuccessMetricFormat, d.statsPrefix))
	}

	d.statter.TimingDuration(fmt.Sprintf(reloadLatencyMetricFormat, d.statsPrefix), time.Since(start), 1.0)

	return err
}

func (d *Dynamic) CampaignEmoteOwner(emoteID string) string {
	if d == nil || d.dynamicConfig == nil {
		return ""
	}

	if emoteMap := d.dynamicConfig.Get(emoteID); emoteMap != nil {
		if channel, ok := emoteMap.(string); ok {
			return channel
		} else {
			return ""
		}
	}

	return ""
}

// FreemotesExperiment returns whether or not you are in the experiment group + rollout percentage for Follower emotes.
func (d *Dynamic) IsInFreemotesExperiment(userID string) bool {
	// Allow regular integration test users to bypass the experiment flag
	if strings.HasPrefix(userID, user_utils.IntegrationTestUserPrefix) {
		return true
	}

	if d == nil || d.dynamicConfig == nil {
		return false
	}

	strValue := d.dynamicConfig.Get(FreemotesExperimentKey)
	if strValue == nil {
		return false
	}

	value, ok := strValue.(string)
	if !ok {
		return false
	}

	var v freemotesExperimentValue
	if err := json.Unmarshal([]byte(value), &v); err != nil {
		logrus.WithError(err).Error("failed to unmarshal dynamic config FreemotesExperiment")
		return false
	}

	for _, allowlistedUserID := range v.AllowlistedUsers {
		if allowlistedUserID == userID {
			return true
		}
	}

	h := fnv.New32a()
	if _, err := h.Write([]byte(userID)); err != nil {
		return false
	}

	// Experiment & Marketing channels should be put against the percentage rollout
	if _, ok := d.freemotesChannelsMap[userID]; !ok {
		if _, ok := d.freemotesMarketingMap[userID]; !ok {
			return false
		}
	}

	return h.Sum32()%uint32(100) < uint32(v.RolloutPercent)
}

func (d *Dynamic) IsKnownOrVerifiedBot(userID string) bool {
	isKnownOrVerifiedBot := false

	// KnownOrVerifiedBots is a map in the S3 JSON from userID to bool (for quick access, the bool doesn't mean anything)
	// that indicates that a specific user is a bot which should be considered as automatically receiving entitlements.
	if knownOrVerifiedBots := d.dynamicConfig.Get("KnownOrVerifiedBots"); knownOrVerifiedBots != nil {
		if knownOrVerifiedBotsMap, ok := knownOrVerifiedBots.(map[string]interface{}); ok {
			_, isKnownOrVerifiedBot = knownOrVerifiedBotsMap[userID]
		}
	}

	return isKnownOrVerifiedBot
}

func (d *Dynamic) SmilieFix(userID string) bool {
	if d == nil || d.dynamicConfig == nil {
		return false
	}

	strValue := d.dynamicConfig.Get(SmilieFixKey)
	if strValue == nil {
		return false
	}

	value, ok := strValue.(string)
	if !ok {
		return false
	}

	var v smilieFixValue
	if err := json.Unmarshal([]byte(value), &v); err != nil {
		logrus.WithError(err).Error("failed to unmarshal dynamic config SmilieFix")
		return false
	}

	for _, allowlistedUserID := range v.AllowlistedUsers {
		if allowlistedUserID == userID {
			return true
		}
	}

	h := fnv.New32a()
	if _, err := h.Write([]byte(userID)); err != nil {
		return false
	}

	return h.Sum32()%uint32(100) < uint32(v.RolloutPercent)
}

func (d *Dynamic) GetThirdPartyEmoteEntitlementsAllowlistMap() map[string][]string {
	if d == nil || d.dynamicConfig == nil {
		return map[string][]string{}
	}

	dynamicConfigResult := d.dynamicConfig.Get(ThirdPartyEmoteEntitlementsAllowlist)
	if dynamicConfigResult == nil {
		return map[string][]string{}
	}

	dynamicConfigResultString, ok := dynamicConfigResult.(string)
	if !ok {
		return map[string][]string{}
	}

	var allowlistMap map[string][]string
	if err := json.Unmarshal([]byte(dynamicConfigResultString), &allowlistMap); err != nil {
		logrus.WithError(err).Error("failed to unmarshal dynamic config for GetThirdPartyEmoteEntitlementsAllowlistMap")
		return map[string][]string{}
	}

	return allowlistMap
}

func (d *Dynamic) GetGoldenKappaAlwaysOnEnabled(userID string) bool {
	if d == nil || d.dynamicConfig == nil {
		return false
	}

	dynamicConfigResult := d.dynamicConfig.Get(GoldenKappaAlwaysOnKey)
	if dynamicConfigResult == nil {
		return false
	}

	dynamicConfigResultString, ok := dynamicConfigResult.(string)
	if !ok {
		return false
	}

	var rolloutConfig rolloutConfig
	if err := json.Unmarshal([]byte(dynamicConfigResultString), &rolloutConfig); err != nil {
		logrus.WithError(err).Error("failed to unmarshal dynamic config for GetGoldenKappaAlwaysOnEnabled")
		return false
	}

	for _, allowlistedUserID := range rolloutConfig.AllowlistedUsers {
		if allowlistedUserID == userID {
			return true
		}
	}

	userIdInt, err := strconv.Atoi(userID)
	if err != nil {
		return false
	}

	return userIdInt%100 < rolloutConfig.RolloutPercent
}

func (d *Dynamic) EnableEmoteUpdatePubsub(channelID string) bool {
	if d == nil || d.dynamicConfig == nil {
		return false
	}

	dynamicConfigResult := d.dynamicConfig.Get(EnableEmoteUpdatePubsubKey)

	if dynamicConfigResult == nil {
		return false
	}

	dynamicConfigResultString, ok := dynamicConfigResult.(string)
	if !ok {
		return false
	}

	var rolloutConfig EnableEmoteUpdatePubsubRolloutConfig
	if err := json.Unmarshal([]byte(dynamicConfigResultString), &rolloutConfig); err != nil {
		logrus.WithError(err).Error("failed to unmarshal dynamic config for EnableEmoteUpdatePubsub")
		return false
	}

	for _, allowlistedChannelID := range rolloutConfig.AllowlistedUsers {
		if allowlistedChannelID == channelID {
			return true
		}
	}

	h := fnv.New32a()
	if _, err := h.Write([]byte(channelID)); err != nil {
		return false
	}

	return h.Sum32()%uint32(100) < uint32(rolloutConfig.RolloutPercent)
}

func (d *Dynamic) GetDefaultEmoteImagesData() ([]*mako_dashboard.DefaultEmoteImage, error) {
	data := d.dynamicConfig.Get(DefaultEmoteImagesDataKey)
	if data == nil {
		return nil, errors.New(fmt.Sprintf("no data returned from %s", DefaultEmoteImagesDataKey))
	}
	return generateDefaultImageFromInterface(data)
}

func (d *Dynamic) LibraryLimitIncreaseLaunched(userID string) bool {
	if d == nil || d.dynamicConfig == nil {
		return false
	}

	dynamicConfigResult := d.dynamicConfig.Get(LibraryLimitsIncreasedKey)
	if dynamicConfigResult == nil {
		return false
	}

	dynamicConfigResultString, ok := dynamicConfigResult.(string)
	if !ok {
		return false
	}

	var rolloutConfig LibraryLimitsIncreased
	if err := json.Unmarshal([]byte(dynamicConfigResultString), &rolloutConfig); err != nil {
		logrus.WithError(err).Error("failed to unmarshal dynamic config for InAnimatedEmotesForAffiliatesRollout")
		return false
	}

	if rolloutConfig.IsLaunched {
		return true
	}

	for _, allowlistedUserID := range rolloutConfig.AllowlistedUsers {
		if allowlistedUserID == userID {
			return true
		}
	}

	return false
}

func generateDefaultImageFromInterface(data interface{}) ([]*mako_dashboard.DefaultEmoteImage, error) {
	values, ok := data.([]interface{})
	if !ok {
		return nil, errors.New(fmt.Sprintf("unexpected data format from %s", DefaultEmoteImagesDataKey))
	}
	var defaultEmoteImages []*mako_dashboard.DefaultEmoteImage
	for _, value := range values {
		valueMap, ok := value.(map[string]interface{})
		if !ok {
			return nil, errors.New(fmt.Sprintf("unexpected data format from default emote %+v", value))
		}

		tagsInterface := valueMap["tags"].([]interface{})
		tags := make([]string, len(tagsInterface))
		for i, v := range tagsInterface {
			tags[i] = fmt.Sprint(v)
		}

		id, ok := valueMap["id"].(string)
		if !ok {
			return nil, errors.New(fmt.Sprintf("unexpected id format from default emote %+v", value))
		}

		name, ok := valueMap["name"].(string)
		if !ok {
			return nil, errors.New(fmt.Sprintf("unexpected name format from default emote %+v", value))
		}

		assetTypeString, ok := valueMap["asset_type"].(string)
		if !ok {
			return nil, errors.New(fmt.Sprintf("unexpected asset type format from default emote%+v", value))
		}

		assetType := mako_dashboard.EmoteAssetType_static
		if assetTypeString == mako_dashboard.EmoteAssetType_animated.String() {
			assetType = mako_dashboard.EmoteAssetType_animated
		}

		image := &mako_dashboard.DefaultEmoteImage{
			Id:        id,
			Name:      name,
			AssetType: assetType,
			Tags:      tags,
		}
		defaultEmoteImages = append(defaultEmoteImages, image)
	}

	return defaultEmoteImages, nil
}

func incrementStatter(statter statsd.Statter, stat string) {
	statErr := statter.Inc(stat, 1, 1.0)
	if statErr != nil {
		logrus.WithField("stat", stat).WithError(statErr).Error("Failed to increment statter")
	}
}

func newBackoff() backoff.BackOff {
	exponential := backoff.NewExponentialBackOff()
	exponential.InitialInterval = 250 * time.Millisecond
	exponential.MaxElapsedTime = 10 * time.Second
	exponential.Multiplier = 2
	return exponential
}

func createFreemotesMapFromFilename(filePath string) (map[string]bool, error) {
	freemotesListFile, err := os.Open(filePath)
	if err != nil {
		return nil, fmt.Errorf("Failed to open Freemotes Experiment List file: %v", err)
	}
	defer freemotesListFile.Close()

	freemotesExperimentMap := make(map[string]bool)
	scanner := bufio.NewScanner(freemotesListFile)
	for scanner.Scan() {
		freemotesExperimentMap[scanner.Text()] = true
	}

	if err := scanner.Err(); err != nil {
		return nil, fmt.Errorf("Failed to scan the Freemotes Experiment file: %v", err)
	}

	return freemotesExperimentMap, nil
}

func getFreemotesExperimentFilePath(localFilePath string, globalFilePath string) (string, error) {
	localFname := os.Getenv("GOPATH") + localFilePath
	if _, err := os.Stat(localFname); !os.IsNotExist(err) {
		return localFname, nil
	}
	globalFname := globalFilePath
	if _, err := os.Stat(globalFname); os.IsNotExist(err) {
		return "", err
	}
	return globalFname, nil
}

type dynamicFake struct{}

func NewDynamicConfigFake() DynamicConfigClient {
	return &dynamicFake{}
}

func (df *dynamicFake) SmilieFix(userID string) bool {
	return false
}

func (df *dynamicFake) IsInFreemotesExperiment(userID string) bool {
	return false
}

func (df *dynamicFake) GetThirdPartyEmoteEntitlementsAllowlistMap() map[string][]string {
	return map[string][]string{}
}

func (df *dynamicFake) GetGoldenKappaAlwaysOnEnabled(userID string) bool {
	return false
}

func (df *dynamicFake) EnableEmoteUpdatePubsub(channelID string) bool {
	return false
}

func (df *dynamicFake) LibraryLimitIncreaseLaunched(userID string) bool {
	return false
}
