package hystrix

import (
	"code.justin.tv/commerce/mako/clients/chemtrail"
	"code.justin.tv/commerce/mako/clients/emotesinference"
	"code.justin.tv/commerce/mako/clients/emoticons"
	makoEmoticons "code.justin.tv/commerce/mako/clients/emoticons"
	"code.justin.tv/commerce/mako/clients/following"
	"code.justin.tv/commerce/mako/clients/materia"
	"code.justin.tv/commerce/mako/clients/names"
	"code.justin.tv/commerce/mako/clients/payday"
	"code.justin.tv/commerce/mako/clients/pdms"
	"code.justin.tv/commerce/mako/clients/pepper"
	"code.justin.tv/commerce/mako/clients/ripley"
	"code.justin.tv/commerce/mako/clients/salesforce"
	"code.justin.tv/commerce/mako/clients/subscriptions"
	"code.justin.tv/commerce/mako/config"
	"code.justin.tv/commerce/mako/dynamo"
	"code.justin.tv/commerce/mako/redis"
	"github.com/afex/hystrix-go/hystrix"
)

var TestTimeoutMilliseconds = 60000 // 60 seconds for test timeouts

func ConfigureHystrixCommands(cfg *config.Configuration) {
	hystrix.Configure(map[string]hystrix.CommandConfig{
		// Circuit for DynamoDB Entitlements GET
		dynamo.HystrixNameEntitlementsGet: {
			MaxConcurrentRequests:  600,
			RequestVolumeThreshold: 40,
			Timeout:                cfg.DynamoDBTimeoutMilliseconds,
			ErrorPercentThreshold:  50,
		},
		makoEmoticons.HystrixNameMateriaEntitlementsGet: {
			MaxConcurrentRequests:  600,
			RequestVolumeThreshold: 40,
			Timeout:                cfg.MateriaTimeoutMilliseconds,
			ErrorPercentThreshold:  50,
		},
		following.HystrixNameFollowingGetFollow: {
			MaxConcurrentRequests:  600,
			RequestVolumeThreshold: 40,
			Timeout:                cfg.FollowingServiceTimeoutMilliseconds,
		},
		dynamo.HystrixNameDynamoEmotesGroupGet: {
			MaxConcurrentRequests: 10000,
			Timeout:               cfg.EmoteDBDynamoGroupHystrixTimeoutMilliseconds,
		},
		dynamo.HystrixNameDynamoEmotesCodeGet: {
			MaxConcurrentRequests: 1000,
			Timeout:               cfg.EmoteDBDynamoCodeHystrixTimeoutMilliseconds,
		},
		dynamo.HystrixNameDynamoEmoteModifierGroupsGet: {
			MaxConcurrentRequests: 1000,
			Timeout:               cfg.EmoteModifierDBDynamoGetHystrixTimeoutMilliseconds,
		},
		redis.EmoteModifiersDBOwnerHystrix: {
			MaxConcurrentRequests: 1000,
			Timeout:               cfg.EmoteModifierDBRedisOwnerHystrixTimeoutMilliseconds,
		},
		redis.EmoteModifiersDBUserHystrix: {
			MaxConcurrentRequests: 1000,
			Timeout:               cfg.EmoteModifierDBRedisGetHystrixTimeoutMilliseconds,
		},
		redis.EmoteDBHystrix: {
			MaxConcurrentRequests:  1000,
			Timeout:                cfg.EmoteDBRedisGetHystrixTimeoutMilliseconds,
			RequestVolumeThreshold: 200,
		},
		redis.EmoteDBCodeHystrix: {
			MaxConcurrentRequests: 1000,
			Timeout:               cfg.EmoteDBRedisCodeGetHystrixTimeoutMilliseconds,
		},
		redis.EmoteDBCodeSetHystrix: {
			MaxConcurrentRequests: 1000,
			Timeout:               cfg.EmoteDBRedisCodeGetHystrixTimeoutMilliseconds,
		},
		redis.FollowingEntitlementsDBIDHystrix: {
			MaxConcurrentRequests: 1000,
			Timeout:               cfg.FollowingEntitlementsDBRedisIDHystrixTimeoutMilliseconds,
		},
		redis.FollowingEntitlementsDBOwnerHystrix: {
			MaxConcurrentRequests: 1000,
			Timeout:               cfg.FollowingEntitlementsDBRedisOwnerHystrixTimeoutMilliseconds,
		},
		materia.HystrixNameMateriaEntitlementsGet: {
			MaxConcurrentRequests:  1000,
			Timeout:                cfg.EntitlementDBMateriaGetHystrixTimeoutMilliseconds,
			RequestVolumeThreshold: 40,
			ErrorPercentThreshold:  50,
		},
		emoticons.LegacyEntitlementHystrix: {
			MaxConcurrentRequests: 1000,
			Timeout:               cfg.EntitlementDBLegacyGetHystrixTimeoutMilliseconds,
		},
		emotesinference.HystrixNameEmotesInferenceEmoteClassify: {
			MaxConcurrentRequests: 100,
			Timeout:               cfg.InferenceClassifyEmoteTimeoutMilliseconds,
		},
		names.HystrixNameNamesAcceptableNamesGet: {
			MaxConcurrentRequests: 100,
			Timeout:               cfg.InferenceAcceptableNamesTimeoutMilliseconds,
		},
		pepper.HystrixNamePepperRiskEvaluationGet: {
			MaxConcurrentRequests: 200,
			Timeout:               cfg.PepperTimeoutMilliseconds,
		},
		pdms.HystrixNamePDMSReportDeletion: {
			MaxConcurrentRequests: 200,
			Timeout:               pdms.TimeoutMilliseconds,
		},
		subscriptions.HystrixNameSubscriptionsChannelEmoteLimitsGet: {
			MaxConcurrentRequests: 200,
			Timeout:               cfg.SubscriptionsGetChannelEmoteLimitsTimeoutMilliseconds,
		},
		subscriptions.HystrixNameSubscriptionsChannelProductsGet: {
			MaxConcurrentRequests: 200,
			Timeout:               cfg.SubscriptionsGetChannelProductsTimeoutMilliseconds,
		},
		payday.HystrixNamePaydayBitsTierEmoteGroupsGet: {
			MaxConcurrentRequests: 200,
			Timeout:               cfg.PaydayGetBitsTierEmoteGroupsTimeoutMilliseconds,
		},
		salesforce.SalesforceGoodStandingHystrix: {
			MaxConcurrentRequests: 200,
			Timeout:               cfg.SalesforceInGoodStandingHystrixTimeoutMilliseconds,
		},
		ripley.HystrixNameRipleyPayoutGet: {
			MaxConcurrentRequests: 200,
			Timeout:               cfg.RipleyGetPayoutTypeHystrixTimeoutMilliseconds,
		},
		chemtrail.HystrixNameChemtrailEmoteUsageGet: {
			MaxConcurrentRequests: 100,
			Timeout:               cfg.ChemtrailGetEmoteUsageHystrixTimeoutMilliseconds,
		},
	})
}

func ConfigureHystrixCommandsForTest() {
	configureTimeout(ripley.HystrixNameRipleyPayoutGet, TestTimeoutMilliseconds)
	configureTimeout(dynamo.HystrixNameEntitlementsGet, TestTimeoutMilliseconds)
	configureTimeout(makoEmoticons.HystrixNameMateriaEntitlementsGet, TestTimeoutMilliseconds)
	configureForHighFailureTolerance(following.HystrixNameFollowingGetFollow, TestTimeoutMilliseconds)
	configureTimeout(dynamo.HystrixNameDynamoEmotesGroupGet, TestTimeoutMilliseconds)
	configureTimeout(dynamo.HystrixNameDynamoEmotesCodeGet, TestTimeoutMilliseconds)
	configureTimeout(dynamo.HystrixNameDynamoEmoteModifierGroupsGet, TestTimeoutMilliseconds)
	configureTimeout(redis.EmoteModifiersDBOwnerHystrix, TestTimeoutMilliseconds)
	configureTimeout(redis.EmoteModifiersDBUserHystrix, TestTimeoutMilliseconds)
	configureTimeout(redis.EmoteDBHystrix, TestTimeoutMilliseconds)
	configureTimeout(redis.EmoteDBCodeHystrix, TestTimeoutMilliseconds)
	configureTimeout(redis.EmoteDBCodeSetHystrix, TestTimeoutMilliseconds)
	configureTimeout(materia.HystrixNameMateriaEntitlementsGet, TestTimeoutMilliseconds)
	configureTimeout(emoticons.LegacyEntitlementHystrix, TestTimeoutMilliseconds)
	configureTimeout(emotesinference.HystrixNameEmotesInferenceEmoteClassify, TestTimeoutMilliseconds)
	configureTimeout(names.HystrixNameNamesAcceptableNamesGet, TestTimeoutMilliseconds)
	configureTimeout(pepper.HystrixNamePepperRiskEvaluationGet, TestTimeoutMilliseconds)
	configureTimeout(pdms.HystrixNamePDMSReportDeletion, TestTimeoutMilliseconds)
	configureTimeout(subscriptions.HystrixNameSubscriptionsChannelEmoteLimitsGet, TestTimeoutMilliseconds)
	configureTimeout(salesforce.SalesforceGoodStandingHystrix, TestTimeoutMilliseconds)
	configureTimeout(ripley.HystrixNameRipleyPayoutGet, TestTimeoutMilliseconds)
	configureTimeout(redis.FollowingEntitlementsDBIDHystrix, TestTimeoutMilliseconds)
	configureTimeout(redis.FollowingEntitlementsDBOwnerHystrix, TestTimeoutMilliseconds)
	configureTimeout(chemtrail.HystrixNameChemtrailEmoteUsageGet, TestTimeoutMilliseconds)
}

func configureTimeout(cmd string, timeout int) {
	configure(cmd, timeout)
}

func configure(cmd string, timeout int) {
	hystrix.ConfigureCommand(cmd, hystrix.CommandConfig{
		Timeout: timeout,
		// We explicitly do not want to use hystrix max concurrency see: https://twitch.slack.com/archives/G5LTHELHL/p1616186139083300
		// Set this high enough so that max concurrency is never utilized, but low enough so that we are not at risk of going OOM here:
		//      https://github.com/afex/hystrix-go/blob/master/hystrix/pool.go#L17
		MaxConcurrentRequests: 1000,
		ErrorPercentThreshold: hystrix.DefaultErrorPercentThreshold,
	})
}

// Used for when you want to be able to test error cases of a hystrix wrapped call without tripping the circuit in your unit tests
func configureForHighFailureTolerance(cmd string, timeout int) {
	hystrix.ConfigureCommand(cmd, hystrix.CommandConfig{
		Timeout:               timeout,
		MaxConcurrentRequests: 1000,
		ErrorPercentThreshold: 100,
	})
}
