package hystrix

import (
	metricCollector "github.com/afex/hystrix-go/hystrix/metric_collector"
	"github.com/cactus/go-statsd-client/statsd"
)

type HystrixMetricsCollector struct {
	stats statsd.Statter
}

func NewHystrixMetricsCollector(stats statsd.Statter) *HystrixMetricsCollector {
	return &HystrixMetricsCollector{stats: stats}
}

type HystrixCommandMetricsCollector struct {
	name string

	attemptsMetric          string
	errorsMetric            string
	successesMetric         string
	failuresMetric          string
	rejectsMetric           string
	shortCircuitsMetric     string
	timeoutsMetric          string
	fallbackSuccessesMetric string
	fallbackFailuresMetric  string
	contextCancelledMetric  string
	contextDeadlineMetric   string
	concurrencyInUseMetric  string
	totalDurationMetric     string
	runDurationMetric       string

	stats statsd.Statter
}

func (h *HystrixMetricsCollector) NewHystrixCommandMetricsCollector(name string) metricCollector.MetricCollector {
	statsPrefix := "hystrix." + name

	return &HystrixCommandMetricsCollector{
		name:                    name,
		attemptsMetric:          statsPrefix + ".attempts",
		errorsMetric:            statsPrefix + ".errors",
		failuresMetric:          statsPrefix + ".failures",
		rejectsMetric:           statsPrefix + ".rejects",
		shortCircuitsMetric:     statsPrefix + ".shortCircuits",
		timeoutsMetric:          statsPrefix + ".timeouts",
		fallbackSuccessesMetric: statsPrefix + ".fallbackSuccesses",
		fallbackFailuresMetric:  statsPrefix + ".fallbackFailures",
		contextCancelledMetric:  statsPrefix + ".contextCancelled",
		contextDeadlineMetric:   statsPrefix + ".contextDeadlineExceeded",
		concurrencyInUseMetric:  statsPrefix + ".concurrencyInUse",
		totalDurationMetric:     statsPrefix + ".totalDuration",
		runDurationMetric:       statsPrefix + ".runDuration",
		stats:                   h.stats,
	}
}

func (d *HystrixCommandMetricsCollector) Update(r metricCollector.MetricResult) {
	go func() {
		d.stats.Inc(d.attemptsMetric, int64(r.Attempts), 1.0)
		d.stats.Inc(d.errorsMetric, int64(r.Errors), 1.0)
		d.stats.Inc(d.successesMetric, int64(r.Successes), 1.0)
		d.stats.Inc(d.failuresMetric, int64(r.Failures), 1.0)
		d.stats.Inc(d.rejectsMetric, int64(r.Rejects), 1.0)
		d.stats.Inc(d.shortCircuitsMetric, int64(r.ShortCircuits), 1.0)
		d.stats.Inc(d.timeoutsMetric, int64(r.Timeouts), 1.0)
		d.stats.Inc(d.fallbackSuccessesMetric, int64(r.FallbackSuccesses), 1.0)
		d.stats.Inc(d.fallbackFailuresMetric, int64(r.FallbackFailures), 1.0)
		d.stats.Inc(d.contextCancelledMetric, int64(r.ContextCanceled), 1.0)
		d.stats.Inc(d.contextDeadlineMetric, int64(r.ContextDeadlineExceeded), 1.0)
		d.stats.Inc(d.concurrencyInUseMetric, int64(r.ConcurrencyInUse), 1.0)

		d.stats.TimingDuration(d.totalDurationMetric, r.TotalDuration, 1.0)
		d.stats.TimingDuration(d.runDurationMetric, r.RunDuration, 1.0)
	}()
}

// It's a noop because it's all set remotely
func (d *HystrixCommandMetricsCollector) Reset() {}
