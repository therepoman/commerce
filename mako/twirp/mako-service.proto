syntax = "proto3";

package code.justin.tv.commerce.mako;
option go_package = "mako-client";

import "google/protobuf/timestamp.proto";

service Mako {

    // Deprecated, use CreateEmoteEntitlements instead.  Get Fuel Entitlements for a user.
    rpc GetEntitlements(GetEntitlementsRequest) returns (GetEntitlementsResponse){
        option deprecated = true;
    };

    // Deprecated, use CreateEmoteEntitlements instead. Set Fuel Entitlement for a user.
    rpc SetEntitlement(SetEntitlementRequest) returns (SetEntitlementResponse){
        option deprecated = true;
    };

    // Deprecated, we have substitute for this currently. Delete Fuel Entitlements for a user.
    rpc DeleteEntitlements(DeleteEntitlementsRequest) returns (DeleteEntitlementsResponse){
        option deprecated = true;
    };

    // Get the emote set details for a given user.
    rpc GetEmoteSetDetails(GetEmoteSetDetailsRequest) returns (GetEmoteSetDetailsResponse);

    // Get the relevant emotes for this user in this channel.
    rpc GetUserChannelEmotes(GetUserChannelEmotesRequest) returns (GetUserChannelEmotesResponse);

    // Get the emoticon group details for the given emoticon groups
    rpc GetEmoticonsByGroups(GetEmoticonsByGroupsRequest) returns (GetEmoticonsByGroupsResponse);

    // Get the emoticon details for the given emoticon ids
    rpc GetEmoticonsByEmoticonIds(GetEmoticonsByEmoticonIdsRequest) returns (GetEmoticonsByEmoticonIdsResponse);

    // Get all emotes for the given channel id - due to the query behavior and filters available, this API is not cached
    rpc GetAllEmotesByOwnerId(GetAllEmotesByOwnerIdRequest) returns (GetAllEmotesByOwnerIdResponse);

    // Get all emotes including emote modifications for the given owner id
    rpc GetAllEmotesWithModificationsByOwnerID(GetAllEmotesWithModificationsByOwnerIDRequest) returns (GetAllEmotesWithModificationsByOwnerIDResponse);

    // Get all emotes that a user is entitled to given a set of emote codes.
    rpc GetEmotesByCodes(GetEmotesByCodesRequest) returns (GetEmotesByCodesResponse);

    // Get active emote codes for a given user.
    rpc GetActiveEmoteCodes(GetActiveEmoteCodesRequest) returns (GetActiveEmoteCodesResponse) {
        option deprecated = true;
    };

    // Get emoticon image/upload id and url, deprecated, please migrate to GetEmoteUploadConfig in Dashboard API
    rpc GetEmoticonUploadConfiguration(GetEmoticonUploadConfigurationRequest) returns (GetEmoticonUploadConfigurationResponse) {
        option deprecated = true;
    };

    // Get emote entitlements from Materia.
    rpc GetEmoteEntitlements(GetEmoteEntitlementsRequest) returns (GetEmoteEntitlementsResponse);

    // Entitle single emotes.
    rpc CreateEmoteEntitlements(CreateEmoteEntitlementsRequest) returns (CreateEmoteEntitlementsResponse);

    // Entitle single emotes. This API is called by the Helix API of the same name at helix/ent/emotes/entitlements
    rpc Create3PEmoteEntitlements(Create3PEmoteEntitlementsRequest) returns (Create3PEmoteEntitlementsResponse);

    // Entitle emote groups (emote slots, groups of emotes, etc).
    rpc CreateEmoteGroupEntitlements(CreateEmoteGroupEntitlementsRequest) returns (CreateEmoteGroupEntitlementsResponse);

    // Delete entitlements
    rpc DeleteEmoteGroupEntitlements(DeleteEmoteGroupEntitlementsRequest) returns (DeleteEmoteGroupEntitlementsResponse);

    // Deprecated, use CreateEmote. Create emoticon given 3 image IDs and emoticon info.
    rpc CreateEmoticon(CreateEmoticonRequest) returns (CreateEmoticonReponse){
        option deprecated = true;
    };

    // Delete emoticon given id, code, and option to delete assets images from S3. Images are used for VOD Chat. Set true if ABSOLUTELY want to nuke emoticon.
    rpc DeleteEmoticon(DeleteEmoticonRequest) returns (DeleteEmoticonResponse);

    // Deactivate emoticon given id
    rpc DeactivateEmoticon(DeactivateEmoticonRequest) returns (DeactivateEmoticonResponse);

    // Activate emoticon given id
    rpc ActivateEmoticon(ActivateEmoticonRequest) returns (ActivateEmoticonResponse);

    // Update the specified emote's code suffix
    rpc UpdateEmoteCodeSuffix(UpdateEmoteCodeSuffixRequest) returns (UpdateEmoteCodeSuffixResponse);

    // Get whether a creator can upload emotes automatically or whether they must go through the pending emote queue
    rpc GetUserEmoteStanding(GetUserEmoteStandingRequest) returns (GetUserEmoteStandingResponse);

    // Set a values for each of several users' emote standings that override their true evaluated emote standings
    rpc SetUserEmoteStandingOverrides(SetUserEmoteStandingOverridesRequest) returns (SetUserEmoteStandingOverridesResponse);

    // Get the selected smilies set and emoticon info.
    rpc GetSelectedSmilies(GetSelectedSmiliesRequest) returns (GetSelectedSmiliesResponse);

    // Get all of the smilies sets and emoticon info.
    rpc GetAllSmilies(GetAllSmiliesRequest) returns (GetAllSmiliesResponse);

    //Set the selected smilies set and emoticon info.
    rpc SetSmilies(SetSmiliesRequest) returns (SetSmiliesResponse);

    // Emote Modifiers Endpoints:
    //
    // These are used to manage the content of the emote_modifier_groups table in Mako. This table is where
    // broadcaster's settings about what modifiers they have enabled on their channel are stored.
    // There may be more than one Emote Modifier Group per channel.
    rpc CreateEmoteModifierGroups(CreateEmoteModifierGroupsRequest) returns (CreateEmoteModifierGroupsResponse);
    rpc UpdateEmoteModifierGroups(UpdateEmoteModifierGroupsRequest) returns (UpdateEmoteModifierGroupsResponse);
    rpc GetEmoteModifierGroups(GetEmoteModifierGroupsRequest) returns (GetEmoteModifierGroupsResponse);
    rpc GetEmoteModifierGroupsByOwnerId(GetEmoteModifierGroupsByOwnerIdRequest) returns (GetEmoteModifierGroupsByOwnerIdResponse);
    rpc DeleteEmoteModifierGroups(DeleteEmoteModifierGroupsRequest) returns (DeleteEmoteModifierGroupsResponse);

    // (Emote/Cheermote) Prefix Endpoints

    // Set a prefix for a channel.
    rpc SetPrefix(SetPrefixRequest) returns (SetPrefixResponse);

    // Get a prefix for a channel
    rpc GetPrefix(GetPrefixRequest) returns (GetPrefixResponse);

    // Get prefixes by state
    rpc GetPrefixesByState(GetPrefixesByStateRequest) returns (GetPrefixesByStateResponse);

    // Generates a recommended prefix for the channel based on a very simple set of rules
    rpc GetRecommendedPrefix(GetRecommendedPrefixRequest) returns (GetRecommendedPrefixResponse);

    // Submit a user initiated prefix change. Checks if the user is allowed to change their prefix
    rpc SubmitPrefix(SubmitPrefixRequest) returns (SubmitPrefixResponse);

    // Pending emotes

    // GetPendingEmoticonByID returns a particular pending emoticon for use in the aegis GQL loader
    rpc GetPendingEmoticonsByIDs (GetPendingEmoticonsByIDsRequest) returns (GetPendingEmoticonsByIDsResponse);

    // GetPendingEmoticons returns an array of pending emoticons for the moderation team's approval flow.
    rpc GetPendingEmoticons (GetPendingEmoticonsRequest) returns (GetPendingEmoticonsResponse);

    // ReviewPendingEmoticons accepts an array of approve or reject actions and either approves or rejects each emoticon
    rpc ReviewPendingEmoticons (ReviewPendingEmoticonsRequest) returns (ReviewPendingEmoticonsResponse);

    // DeferPendingEmoticons marks a list of pending emoticons' review states as "deferred".
    rpc DeferPendingEmoticons (DeferPendingEmoticonsRequest) returns (DeferPendingEmoticonsResponse) ;

    // CreatePendingEmoticon creates a pending emoticon.
    rpc CreatePendingEmoticon (CreatePendingEmoticonRequest) returns (CreatePendingEmoticonResponse);

    // DeletePendingEmoticons deletes a list of pending emoticons.
    rpc DeletePendingEmoticons (DeletePendingEmoticonsRequest) returns (DeletePendingEmoticonsResponse);

    // CreateEmoteGroup creates an emote group and returns its ID.
    rpc CreateEmoteGroup (CreateEmoteGroupRequest) returns (CreateEmoteGroupResponse);

    // Legacy Emoticon Endpoints

    // Returns S3 URLs to the emoticon all payloads for the legacy kraken endpoints.
    rpc GetLegacyEmoticonAllURLs(GetLegacyEmoticonAllURLsRequest) returns (GetLegacyEmoticonAllURLsResponse);

    // Represents the kraken/chat/:channel_name/emoticons API which return channel and global emoticons
    // based upon channel_name.
    rpc GetLegacyChannelEmoticons(GetLegacyChannelEmoticonsRequest) returns (GetLegacyChannelEmoticonsResponse);

    // Given a list of emote codes, BatchValidateCodeUniqueness returns a map indicating whether the codes are unique
    // to all existing emotes of the given emote filter.
    rpc BatchValidateCodeUniqueness(BatchValidateCodeUniquenessRequest) returns (BatchValidateCodeUniquenessResponse);

    // Given a list of emote codes and their suffixes, BatchValidateCodeAcceptability returns whether they are acceptable.
    // Acceptable here means that they are not considered offensive and do not contain banned words in the suffix.
    rpc BatchValidateEmoteCodeAcceptability(BatchValidateEmoteCodeAcceptabilityRequest) returns (BatchValidateEmoteCodeAcceptabilityResponse);

    // GetAvailableFollowerEmotes returns the associated follower emotes for a given channel_id.
    rpc GetAvailableFollowerEmotes(GetAvailableFollowerEmotesRequest) returns (GetAvailableFollowerEmotesResponse);
}

// Request/Response Types

message GetEmotesByCodesRequest {
    string owner_id = 1;
    repeated string codes = 2;
    string channel_id = 3; // Optional, to include channel-only emotes.
}

message GetEmotesByCodesResponse {
    repeated Emoticon emoticons = 1;
}

message GetEntitlementsRequest {
    string user_id = 1 [deprecated=true]; // Required
    ScopedKey entitlement_scoped_key = 2 [deprecated=true]; // Optional
    bool is_staff = 3 [deprecated=true]; // Optional. True if the passed-in user is a Twitch Staff account, false otherwise.
}

message GetSelectedSmiliesRequest {
    string user_id = 1; // Required
}

message GetSelectedSmiliesResponse {
    SmiliesSetId smilies_set_id = 1;
}

message GetAllSmiliesRequest {
}

message GetAllSmiliesResponse {
    repeated SmiliesSet smiliesSets = 1;
}

message SetSmiliesRequest {
    string user_id = 1; // Required
    SmiliesSetId smilies_set_id = 2; // Required
}

message SetSmiliesResponse {
    SmiliesSetId smilies_set_id = 1;
}

message GetEntitlementsResponse {
    repeated Entitlement entitlements = 1 [deprecated=true];
}

message SetEntitlementRequest {
    Entitlement entitlement = 1 [deprecated=true];
}

message SetEntitlementResponse {
    Entitlement entitlement = 1 [deprecated=true];
}

message DeleteEntitlementsRequest {
    repeated Entitlement entitlements = 1 [deprecated=true];
}

message DeleteEntitlementsResponse {
    repeated Entitlement entitlements = 1 [deprecated=true];
}

message GetEmoteSetDetailsRequest {
    string user_id = 1; // Required
    // Temporary field for the Octane project that is used to verify that entitlements are correctly
    // returned via Materia instead of SiteDB.
    // TODO: https://jira.twitch.com/browse/SUBS-4860
    bool skip_sitedb = 2;
    // Optional Field to be used when requesting follower emotes.
    string channel_id = 3; 
}

message GetEmoteSetDetailsResponse {
    repeated EmoteSet emote_sets = 1;
}

message GetUserChannelEmotesRequest {
    string user_id = 1; // Required
    string requesting_user_id = 2; // Required
    string channel_id = 3; // Required
}

message GetUserChannelEmotesResponse {
    repeated EmoteSet emote_sets = 1;
}

message GetEmoticonsByGroupsRequest {
    repeated string emoticon_group_keys = 1;
    EmoteFilter filter = 2;
}

message GetEmoticonsByGroupsResponse {
    repeated EmoticonGroup groups = 1;
}

message GetEmoticonsByEmoticonIdsRequest {
    repeated string emoticon_ids = 1;
}

message GetEmoticonsByEmoticonIdsResponse {
    repeated Emoticon emoticons = 1;
}

message EmoteFilter {
    Domain domain = 2;
    repeated EmoteState states = 3;
}

message GetAllEmotesByOwnerIdRequest {
    string owner_id = 1;
    EmoteFilter filter = 2;
    bool enforce_owner_restriction = 3;
    string requesting_user_id = 4;
}

message GetAllEmotesByOwnerIdResponse {
    repeated Emoticon emoticons = 1;
}

message GetAllEmotesWithModificationsByOwnerIDRequest {
    string owner_id = 1;
}

message GetAllEmotesWithModificationsByOwnerIDResponse {
    repeated EmoteModificationGroup emote_groups = 1;
}

message GetActiveEmoteCodesRequest {
    string user_id = 1 [deprecated=true];
    Domain domain = 2 [deprecated=true];
}

message GetActiveEmoteCodesResponse {
    map<string, EmoteDetails> codes = 1;
    Domain domain = 2;
}

message GetEmoticonUploadConfigurationRequest {
    string user_id = 1;
    int32 image_width = 2;
    int32 image_height = 3;
    string code = 4;
    string sns_callback_topic = 5;
    string group_id = 6;
}

message GetEmoticonUploadConfigurationResponse {
    string upload_id = 1;
    string upload_url = 2;
    string image_id = 3;
    string image_url = 4;
}

message CreateEmoticonRequest {
    string user_id = 1 [deprecated=true];
    string code = 2 [deprecated=true];
    string code_suffix = 3 [deprecated=true];
    string state = 4 [deprecated=true];
    string group_id = 5 [deprecated=true];
    string image28_id = 6 [deprecated=true];
    string image56_id = 7 [deprecated=true];
    string image112_id = 8 [deprecated=true];
    string domain = 9 [deprecated=true];
    // TODO: There is an inverse relationship between @ryanresi's happiness and how long we let emote_group
    // live alongside (the in no way related) group_id. We should change emote_group to be something like emote_type.
    EmoteGroup emote_group = 10 [deprecated=true];
    // Set this to true if you want Mako to determine initial emote state, based on the logic below:
    // (Partner OR Affiliate) AND inGoodStanding -> Active
    // Otherwise -> Pending and added to the moderation queue
    bool enforce_moderation_workflow = 11 [deprecated=true];
}

message CreateEmoticonReponse {
    string id = 1;
    string code = 2;
    string group_id = 3;
    string state = 4;
}

message DeleteEmoticonRequest {
    string id = 1;
    string code = 2 [deprecated = true];
    bool deleteImages = 3;
    EmoteGroup emote_group = 4 [deprecated = true];
    bool requires_permitted_user = 5;
    string user_id = 6;
}

message DeleteEmoticonResponse {
}

message DeactivateEmoticonRequest {
    string id = 1;
}

message DeactivateEmoticonResponse {}

message ActivateEmoticonRequest {
    string id = 1;
}

message ActivateEmoticonResponse {}

message UpdateEmoteCodeSuffixRequest {
    string id = 1;
    string code_suffix = 2;
}

message UpdateEmoteCodeSuffixResponse {}

message GetEmoteEntitlementsRequest {
    // REQUIRED: owner_id is the Twitch user id who owns the entitlement.
    string owner_id = 1;
    // OPTIONAL: item_id is the id of the emote or emote_set the user is entitled to.
    string item_id = 2;
    // OPTIONAL: origin_id represents how the entitlement was created, for example through a purchase order id.
    string origin_id = 4;
    // OPTIONAL: group represents the type of emote group (i.e. Materia's source)
    EmoteGroup group = 5;
    // OPTIONAL: for including channel-only emotes
    string channel_id = 6;
}

message GetEmoteEntitlementsResponse {
    repeated EmoteEntitlement entitlements = 1;
}

message CreateEmoteEntitlementsRequest {
    repeated EmoteEntitlement entitlements = 1;
}

message CreateEmoteEntitlementsResponse {}

message Create3PEmoteEntitlementsRequest {
    // REQUIRED: client_id is the ID representing the 3rd Party client calling this API via Helix.
    string client_id = 1;
    // REQUIRED: entitlements is the list of emote entitlements to be created. Maximum size: 25
    repeated EmoteEntitlement3P entitlements = 2;
}

message Create3PEmoteEntitlementsResponse {}

message CreateEmoteGroupEntitlementsRequest {
    repeated EmoteGroupEntitlement entitlements = 1;
}

message CreateEmoteGroupEntitlementsResponse {}

message DeleteEmoteGroupEntitlementsRequest {
    repeated EmoteGroupEntitlement entitlements = 1;
}

message DeleteEmoteGroupEntitlementsResponse {}

message BatchValidateCodeUniquenessRequest {
    repeated string emote_codes = 1;
    EmoteFilter filter = 2;
}

message BatchValidateCodeUniquenessResponse {
    map<string, bool> is_unique = 1;
}

message BatchValidateEmoteCodeAcceptabilityRequest {
    repeated ValidateEmoteCodeAcceptabilityInput requests = 1;
    // user_id is not required. It may be used for serving different behavior for different users.
    string user_id = 2;
}

message ValidateEmoteCodeAcceptabilityInput {
    string emote_code = 1;
    string emote_code_suffix = 2;
}

message BatchValidateEmoteCodeAcceptabilityResponse {
    repeated ValidateEmoteCodeAcceptabilityResult results = 1;
}

message ValidateEmoteCodeAcceptabilityResult {
    string emote_code = 1;
    string emote_code_suffix = 2;
    bool is_acceptable = 3;
}

message GetUserEmoteStandingRequest {
    string user_id = 1;
    string requesting_user_id = 2;
}

message GetUserEmoteStandingResponse {
    bool is_in_emote_good_standing = 1; //true if user can bypass the pending queue
}

message SetUserEmoteStandingOverridesRequest {
    repeated UserEmoteStandingOverride overrides = 1;
}

message SetUserEmoteStandingOverridesResponse {
    repeated UserEmoteStandingOverride failed_overrides = 1;
}

// Data Types

// Lookup identifier used across Mako. The scope and key are used together
// to identify various resources such as events and entitlements.
message ScopedKey {
    string scope = 1 [deprecated=true]; // e.g. "/my/scope/"
    string key = 2 [deprecated=true]; // e.g. "mykey"
}

// An Entitlement represents a user "being entitled to" something.
// For example in plain english, "user 123 is entitled to emote set 456".
message Entitlement {
    string user_id = 1 [deprecated=true];
    ScopedKey scoped_key = 2 [deprecated=true];
    string origin = 3 [deprecated=true];
}

// Deprecated.  Use EmoticonGroup for future APIs
// An Emote Set contains a list of emotes tied to that emote set
message EmoteSet {
    string id = 1;
    repeated Emote emotes = 2;
    string channel_id = 3;
}

// An Emoticon Group contains a list of emoticons tied to that emoticon set
message EmoticonGroup {
    string id = 1;
    repeated Emoticon emoticons = 2;
    int64 order = 3;
}

// A Smilies Set contains a set id along with the emoticon group contained in it
message SmiliesSet {
    SmiliesSetId smilies_set_id = 1;
    repeated Emoticon emoticons = 2;
}

enum SmiliesSetId {
    NONE = 0; // Indicates missing argument
    ROBOTS = 1; // default smilies set
    PURPLE = 2;
    MONKEYS = 3;
}

// Deprecated.  Use Emoticon for future APIs
// An emote has an identifier and a regex pattern used in clients
message Emote {
    string id = 1;
    string pattern = 2;
    repeated Modification modifiers = 3;
    string group_id = 4;
    EmoteAssetType asset_type = 5;
    EmoteGroup type = 6;
    int64 order = 7;
    google.protobuf.Timestamp created_at = 8;
    AnimatedEmoteTemplate animated_emote_template = 9;
}

// An emoticon has an identifier and a code used in clients
message Emoticon {
    string id = 1;
    string code = 2;
    string group_id = 3;
    string owner_id = 4;
    string product_id = 5;
    EmoteState state = 6;
    Domain domain = 7;
    string suffix = 8;
    EmoteGroup type = 9;
    EmoteAssetType asset_type = 10;
    int64 order = 11;
    google.protobuf.Timestamp created_at = 12;
    AnimatedEmoteTemplate animated_emote_template = 13;
}

// An emote detail contains an emote identifier and associated group ID
message EmoteDetails {
    string id = 1;
    string group_id = 2;
}

message SetPrefixRequest {
    string channel_id = 1;
    string prefix = 2;
    PrefixState state = 3;
}

message SetPrefixResponse {

}

message GetPrefixRequest {
    string channel_id = 1;
    bool should_check_editability = 2;
}

message GetPrefixResponse {
    string prefix = 1;
    PrefixState state = 2;
    bool is_editable = 3;
}

message GetPrefixesByStateRequest {
    PrefixState state = 1;
    int32 limit = 2;
}

message GetPrefixesByStateResponse {
    repeated PrefixEntry prefix_entries = 1;
}

message PrefixEntry {
    string channel_id = 1;
    string prefix = 2;
    PrefixState state = 3;
}

enum PrefixState {
    PREFIX_UNKNOWN = 0;
    PREFIX_UNSET = 1;
    PREFIX_ACTIVE = 2;
    PREFIX_REJECTED = 3;
    PREFIX_PENDING = 4;
}

message SubmitPrefixRequest {
    string channel_id = 1;
    string prefix = 2;
}

message SubmitPrefixResponse {}

message GetRecommendedPrefixRequest {
    string channel_id = 1;
}

message GetRecommendedPrefixResponse {
    string prefix = 1;
}

message UserEmoteStandingOverride {
    string user_id = 1;
    EmoteStandingOverride override = 2;
}

enum EmoteStandingOverride {
    NotInEmoteGoodStanding = 0;
    InEmoteGoodStanding = 1;
}

// NOTE: This object is separate and distinct from the Emote Modifier Groups in the emote_modifier_groups table in Dynamo.
// This object powers the GetAllEmotesWithModifiers API that chat uses to power the CoPo modifiers display.
// The name of this unfortunately collides with the name of the new emote_modifier_groups table :cclamrip:
message EmoteModificationGroup {
    Modification modification = 1;
    repeated EmoteWithModified emotes = 2;
}

message EmoteWithModified{
    Emoticon base_emote = 1;
    ModifiedEmote modified_emote = 2;
}

message ModifiedEmote {
    string id = 1;
    string code = 2;
}

enum Modification {
    ModificationNone = 0;
    BlackWhite = 1;
    HorizontalFlip = 2;
    Sunglasses = 3;
    Thinking = 4;
    Squished = 5;
    Pumpkin = 6;
    Reindeer = 7;
    Santa = 8;
    RedEnvelope = 9;
    LunarRat = 10;
    HeartEyes = 11;
    KissImprint = 12;
    HeartBroken = 13;
    Beard = 14;
    LuckyHat = 15;
    CloverEyes = 16;
    WomensDay = 17;
    RosieBandana = 18;
    BunnyEars = 19;
    EasterEggs = 20;
    EasterBasket = 21;
    FlowerCrown = 22;
    BubbleTea = 23;
    Sombrero = 24;
    Unicorn = 25;
    PrideFan = 26;
    FeatherBoa = 27;
    Overlay = 28;
    Maracas = 29;
    Guitar = 30;
    WitchHat = 31;
    Pumpkin2 = 32;
    Snowflake = 33;
    Snowman = 34;
}

// Emote Modifier Groups
// ---------------------
// NOTE: This object represents the Emote Modifier Groups in the emote_modifier_groups table in Dynamo.
// The table is used to hold the source of truth for what emote modifications a broacaster has enabled on their channel.
message EmoteModifierGroup {
    // ID must be omitted for Create requests, and must be included for Update requests
    string id = 1;
    string owner_id = 2;
    repeated Modification modifiers = 3;
    repeated string source_emote_group_ids = 4;
}

message CreateEmoteModifierGroupsRequest {
    repeated EmoteModifierGroup groups = 1;
}

message CreateEmoteModifierGroupsResponse {
    repeated EmoteModifierGroup groups = 1;
}

message UpdateEmoteModifierGroupsRequest {
    repeated EmoteModifierGroup groups = 1;
}

message UpdateEmoteModifierGroupsResponse {
    repeated EmoteModifierGroup groups = 1;
}

message GetEmoteModifierGroupsRequest {
    repeated string modifier_group_ids = 1;
}

message GetEmoteModifierGroupsResponse {
    repeated EmoteModifierGroup groups = 1;
}

message GetEmoteModifierGroupsByOwnerIdRequest {
    string owner_id = 1;
}

message GetEmoteModifierGroupsByOwnerIdResponse {
    repeated EmoteModifierGroup groups = 1;
}

message DeleteEmoteModifierGroupsRequest {
    repeated string modifier_group_ids = 1;
}

message DeleteEmoteModifierGroupsResponse {
}

message EmoteEntitlement {
    string owner_id = 1;
    string emote_id = 2;
    string origin_id = 3;
    EmoteGroup group = 4;
    google.protobuf.Timestamp start = 5;
    InfiniteTime end = 6;
    string item_owner_id = 7; // OPTIONAL
    string override_group_id = 8;
}

message EmoteEntitlement3P {
    // REQUIRED: user_id is the ID of the user who is being entitled the emote.
    string user_id = 1;
    // REQUIRED: emote_id is the ID of the emote that will be entitled.
    string emote_id = 2;
}

message EmoteGroupEntitlement {
    // owner_id represents who is entitled to the emote group.
    string owner_id = 1;
    // item_id represents who owns the emote group. It's required to be passed in by the requester
    // because the group may exist but have no emotes yet.
    string item_id = 2;
    // group_id represents the emote group to entitle.
    string group_id = 3;
    // origin_id represents how the entitlement was created, for example through a purchase order id.
    string origin_id = 4;
    // group represents the type of emote group.
    EmoteGroup group = 5;
    google.protobuf.Timestamp start = 6;
    InfiniteTime end = 7;
}

// Legacy Emoticon Data Types
message GetLegacyEmoticonAllURLsRequest {}

message GetLegacyEmoticonAllURLsResponse {
    string emoticons_url = 1;
    string emoticon_images_url = 2;
    string emoticon_images_all_url = 3;
}

message GetLegacyChannelEmoticonsRequest {
    string channel_id = 1;
    string channel_name = 2;
}

message GetLegacyChannelEmoticonsResponse {
    Links _links = 1;
    repeated LegacyChannelEmoticonsEmoticon emoticons = 2;
}

message LegacyChannelEmoticonsEmoticon {
    string regex = 1;
    string state = 2;
    bool subscriber_only = 3;
    string url = 4;
    int32 height = 5;
    int32 width = 6;
}

message Links {
    string self = 1;
}

enum Domain {
    emotes = 0;
    stickers = 1;
}

enum EmoteState {
    active = 0;
    inactive = 1;
    moderated = 2;
    pending = 3;
    archived = 4;
    unknown = 5;
    pending_archived = 6;
}

enum EmoteGroup {
    None = 0;
    ChannelPoints = 1;
    BitsBadgeTierEmotes = 2;
    Subscriptions = 3;
    Rewards = 4;
    HypeTrain = 5;
    Prime = 6;
    Turbo = 7;
    Smilies = 8;
    Globals = 9;
    OWL2019 = 10;
    TwoFactor = 11;
    LimitedTime = 12;
    MegaCommerce = 13;
    Archive = 14;
    Follower = 15;
}

enum EmoteAssetType {
    static = 0;
    animated = 1;
}

// InfiniteTime is a timestamp that can be either a specific time or an infinite date in the future.
// If is_infinite is true, then time will be nil and should not be used.
message InfiniteTime {
    // The time of the event, if is_infinite is false. WILL BE NIL IF is_infinite IS TRUE.
    google.protobuf.Timestamp time = 1;


    // If is_infinite is true then the date is an infinite time in the future.
    // is_infinite is used to represent entitlements that last forever.
    bool is_infinite = 2;
}

// Group Management
// =========
message CreateEmoteGroupRequest {
    // From which product this group creation request originates (Subscriptions, Bits Tiers, etc.)
    // Not all products are supported.
    EmoteGroup origin = 1;
    // The type of emotes contained in this group (specially different then EmoteAssetType as we can forsee a world where
    // there are different group types vs emote types).
    EmoteGroupType group_type = 2;
    // For being able to set and look up which channel owns a given emote group. Only used with Follower Emotes for now
    string owner_id = 3;
}

enum EmoteGroupType {
    static_group = 0;
    animated_group = 1;
}

message CreateEmoteGroupResponse {
    string group_id = 1;
}

// Pending Emoticons
// =========
message GetPendingEmoticonsByIDsRequest {
    repeated string emoticon_ids = 1;
}

message GetPendingEmoticonsByIDsResponse {
    repeated PendingEmoticon emoticons = 1;
}

// GetPendingEmoticonsRequest
message GetPendingEmoticonsRequest {
    EmoticonFilter filter = 1;
    int32 count = 2; // Defaults to 25 - must be > 0
    EmoticonBucket bucket = 3;
    google.protobuf.Timestamp after_timestamp = 4;
    PendingEmoticonReviewStateFilter review_state_filter = 5;
    PendingEmoticonAssetTypeFilter asset_type_filter = 6;
}

// GetPendingEmoticonsResponse
message GetPendingEmoticonsResponse {
    repeated PendingEmoticon emoticons = 1;
    int32 total_count = 2;
}

message ReviewPendingEmoticonsRequest {
    repeated ReviewPendingEmoticonRequest reviews = 1;
    string admin_id = 2; // The LDAP ID of the admin who took this action
}

message ReviewPendingEmoticonRequest {
    string id = 1;
    bool approve = 2;
    string decline_reason = 3; // One of a set of well-known decline reasons
    string decline_explanation = 4; // A more descriptive explanation written by the admin
    bool should_skip_notification = 5; // If the user should not be emailed about the review result.
}

message ReviewPendingEmoticonsResponse {
    repeated ReviewPendingEmoticonResponse results = 1;
}

message ReviewPendingEmoticonResponse {
    string id = 1;
    bool succeeded = 2;
    string new_status = 3;
}

message DeferPendingEmoticonsRequest {
    repeated string emoticon_ids = 1;
    string admin_id = 2; // The LDAP ID of the admin who took this action
}

message DeferPendingEmoticonsResponse {
    repeated DeferPendingEmoticonsResult results = 1;
}

message DeferPendingEmoticonsResult {
    string emoticon_id = 1;
    bool succeeded = 2;
}

message CreatePendingEmoticonRequest {
    string id = 1;
    string prefix = 2;
    string code = 3;
    string owner_id = 4;
    AccountType account_type = 5;
    EmoteAssetType asset_type = 6;
    AnimatedEmoteTemplate animated_emote_template = 7;
}

message CreatePendingEmoticonResponse {
    PendingEmoticon pending_emoticon = 1;
}

message DeletePendingEmoticonsRequest {
    repeated string emoticon_ids = 1;
}

message DeletePendingEmoticonsResponse {
}

message EmoticonBucket {
    int32 requested = 1;
    int32 bucket_count = 2;
}

enum EmoticonFilter {
    EmoticonFilter_All = 0;
    EmoticonFilter_Partner = 1;
    EmoticonFilter_Affiliate = 2;
}

enum PendingEmoticonAssetTypeFilter {
    PendingEmoticonAssetTypeFilter_All = 0;
    PendingEmoticonAssetTypeFilter_Static = 1;
    PendingEmoticonAssetTypeFilter_Animated = 2;
}

enum PendingEmoticonReviewStateFilter {
    // Filter to get both deferred and non-deferred pending emoticons
    PendingEmoticonReviewStateFilter_All = 0;
    // Filter to get non-deferred pending emoticons
    PendingEmoticonReviewStateFilter_None = 1;
    // Filter to get deferred pending emoticons
    PendingEmoticonReviewStateFilter_Deferred = 2;
}

// PendingEmoticon contains all the data necessary for Moderation to view and judge an emoticon
message PendingEmoticon {
    string id = 1;
    string prefix = 2;
    string code = 3;
    string owner_id = 4;
    google.protobuf.Timestamp upload_time = 5;
    AccountType account_type = 6;
    EmoteAssetType asset_type = 7;
    AnimatedEmoteTemplate animated_emote_template = 8;
}

enum AccountType {
    Partner = 0;
    Affiliate = 1;
}

enum AnimatedEmoteTemplate {
  NO_TEMPLATE = 0;
  SHAKE = 1;
  ROLL = 2;
  SPIN = 3;
  RAVE = 4;
  SLIDEIN = 5;
  SLIDEOUT = 6;
}

message GetAvailableFollowerEmotesRequest {
  string channel_id = 1;
}

message GetAvailableFollowerEmotesResponse {
  repeated Emoticon emoticons = 1;
}
