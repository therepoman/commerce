package stats

import (
	"context"
	"testing"

	stats_mock "code.justin.tv/commerce/mako/mocks/github.com/cactus/go-statsd-client/statsd"
	. "github.com/smartystreets/goconvey/convey"
)

func TestServerHook(t *testing.T) {
	statter := new(stats_mock.Statter)
	hooks := NewStatsdServerHooks(statter)
	ctx := context.Background()

	Convey("Test hook", t, func() {
		Convey("hook executions", func() {
			statter.On("Inc", "twirp.total.responses", int64(1), float32(1.0)).Return(nil)
			statter.On("Inc", "twirp.total.requests", int64(1), float32(1.0)).Return(nil)

			hooks.RequestReceived(ctx)
			hooks.ResponseSent(ctx)
			statter.AssertNumberOfCalls(t, "Inc", 2)
		})
	})
}
