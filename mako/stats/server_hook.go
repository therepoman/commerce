package stats

import (
	"context"
	"fmt"
	"time"

	"code.justin.tv/commerce/mako/config"
	"github.com/twitchtv/twirp"
	twirp_statsd "github.com/twitchtv/twirp/hooks/statsd"
)

var statsdSamplingRateMap = map[string]float32{
	"prod-GetEmoteSetDetails":                     0.1, // 10k+ TPS
	"prod-GetAllEmotesWithModificationsByOwnerID": 0.1, // 2k+ TPS
	"prod-GetActiveEmoteCodes":                    0.1, // 2k+ TPS
	"prod-GetEmoticonsByGroups":                   0.1, // 1k+ TPS
	"prod-GetEmoticonsByEmoticonIds":              0.1, // 1k+ TPS
}

var reqStartTimestampKey = new(int) // Store a private address to an integer to be used as a context key. Value has no significance.

const (
	statsTwirpTotalRequests  string = "twirp.total.requests"
	statsTwirpTotalResponses string = "twirp.total.responses"

	defaultStatsdSamplingRate float32 = 1.0
)

// NewStatsdServerHooks creates a twirp server hook that sends stats data to statsd.
// Custom version of this source: https://git.xarth.tv/common/twirp/blob/develop/hooks/statsd/statsd.go
func NewStatsdServerHooks(stats twirp_statsd.Statter) *twirp.ServerHooks {
	hooks := &twirp.ServerHooks{}
	// RequestReceived: inc twirp.total.req_recv
	hooks.RequestReceived = func(ctx context.Context) (context.Context, error) {
		ctx = markReqStart(ctx)
		stats.Inc(statsTwirpTotalRequests, 1, 1.0)
		return ctx, nil
	}

	// RequestRouted: inc twirp.<method>.req_recv
	hooks.RequestRouted = func(ctx context.Context) (context.Context, error) {
		method, ok := twirp.MethodName(ctx)
		if !ok {
			return ctx, nil
		}
		sampleRate := getSampleRate(method)
		stats.Inc("twirp."+method+".requests", 1, sampleRate)
		return ctx, nil
	}

	hooks.ResponsePrepared = func(ctx context.Context) context.Context {
		start, haveStart := getReqStart(ctx)
		method, haveMethod := twirp.MethodName(ctx)

		if haveStart {
			dur := time.Since(start)

			if haveMethod {
				sampleRate := getSampleRate(method)
				stats.TimingDuration("twirp."+method+".prepared.response", dur, sampleRate)
			}
		}

		return ctx
	}

	hooks.ResponseSent = func(ctx context.Context) {
		var (
			start  time.Time
			method string
			status string

			haveStart  bool
			haveMethod bool
			haveStatus bool
		)

		start, haveStart = getReqStart(ctx)
		method, haveMethod = twirp.MethodName(ctx)
		status, haveStatus = twirp.StatusCode(ctx)

		sampleRate := getSampleRate(method)

		stats.Inc(statsTwirpTotalResponses, 1, sampleRate)

		if haveMethod {
			stats.Inc("twirp."+method+".responses", 1, sampleRate)
		}
		if haveStatus {
			stats.Inc("twirp.status_codes.total."+status, 1, sampleRate)
		}
		if haveMethod && haveStatus {
			stats.Inc("twirp.status_codes."+method+"."+status, 1, sampleRate)
		}

		if haveStart {
			dur := time.Since(start)
			stats.TimingDuration("twirp.all_methods.response", dur, sampleRate)

			if haveMethod {
				stats.TimingDuration("twirp."+method+".response", dur, sampleRate)
			}
			if haveStatus {
				stats.TimingDuration("twirp.status_codes.all_methods."+status, dur, sampleRate)
			}
			if haveMethod && haveStatus {
				stats.TimingDuration("twirp.status_codes."+method+"."+status+".latencies", dur, sampleRate)
			}
		}
	}
	return hooks
}

func markReqStart(ctx context.Context) context.Context {
	return context.WithValue(ctx, reqStartTimestampKey, time.Now())
}

func getReqStart(ctx context.Context) (time.Time, bool) {
	t, ok := ctx.Value(reqStartTimestampKey).(time.Time)
	return t, ok
}

func getSampleRate(method string) float32 {
	environment := config.GetEnvironment()
	key := fmt.Sprintf("%s-%s", environment, method)
	if rate, ok := statsdSamplingRateMap[key]; ok {
		return rate
	}
	return defaultStatsdSamplingRate
}
