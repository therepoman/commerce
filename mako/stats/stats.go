package stats

import (
	"fmt"
	"time"

	"code.justin.tv/commerce/mako/config"
	"code.justin.tv/commerce/splatter"
	"github.com/cactus/go-statsd-client/statsd"
)

const (
	canaryStage              = "canary"
	prodStage                = "prod"
	primarySubstage          = "primary"
	twitchTelemetryFlush     = 30 * time.Second // Flush metrics every 30 seconds for TwitchTelemetry
	twitchTelemetryMinBuffer = 100000           // Define a min buffer size to ensure there is enough room for metrics while flushing
)

const makoNamespace = "mako"

// Set up stats objects for use throughout this service. Returns the built statter that should be used everywhere.
func SetupStats(deploymentStage string, makoConfig *config.Configuration) statsd.Statter {
	// Create the TwitchTelemetry statter
	return setupTwitchTelemetryStatter(makoNamespace, deploymentStage, makoConfig)
}

// SetupStatsWithMinBufferSize creates a statter using the min TwitchTelemetry buffer size. This should be used for lambdas.
func SetupStatsWithMinBufferSize(deploymentStage string, cloudwatchRegion string) statsd.Statter {
	return setupTwitchTelemetryStatter(makoNamespace, deploymentStage, &config.Configuration{
		CloudwatchBufferSize: twitchTelemetryMinBuffer,
		CloudwatchRegion:     cloudwatchRegion,
	})
}

func setupTwitchTelemetryStatter(namespace string, deploymentStage string, makoConfig *config.Configuration) statsd.Statter {
	// Define a buffer as at least twitchTelemetryMinBuffer
	bufferSize := makoConfig.CloudwatchBufferSize
	if bufferSize < twitchTelemetryMinBuffer {
		bufferSize = twitchTelemetryMinBuffer
	}

	// Update the Stage and Substage depending on whether we're using a Canary
	telemetryStage := deploymentStage
	telemetrySubstage := primarySubstage
	if telemetryStage == canaryStage {
		telemetryStage = prodStage
		telemetrySubstage = canaryStage
	}

	// Construct the TwitchTelemetry Config
	twitchTelemetryConfig := &splatter.BufferedTelemetryConfig{
		FlushPeriod:       twitchTelemetryFlush,
		BufferSize:        bufferSize,
		AggregationPeriod: time.Minute, // Place all metrics into 1 minute buckets
		ServiceName:       namespace,
		AWSRegion:         makoConfig.CloudwatchRegion,
		Stage:             telemetryStage,
		Substage:          telemetrySubstage,
		Prefix:            "", // No need for a prefix since service, stage, and substage are all dimensions
	}

	// We do not want to send these frequent metrics that are not used in CW in anyway.
	filteredMetrics := map[string]bool{
		statsTwirpTotalRequests:  true,
		statsTwirpTotalResponses: true,
	}

	// Construct the buffered CloudWatch TwitchTelemetry sender
	return splatter.NewBufferedTelemetryCloudWatchStatter(twitchTelemetryConfig, filteredMetrics)
}

func getStatsPrefix(deploymentStage string) string {
	return fmt.Sprintf("mako.%s", deploymentStage)
}
