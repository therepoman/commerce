package api

import (
	"context"

	mk "code.justin.tv/commerce/mako/twirp"
	"github.com/twitchtv/twirp"
)

type ActiveEmoteCodesAPI struct{}

// Deprecated: please migrate to GetEmotesByCodes API
func (aec *ActiveEmoteCodesAPI) GetActiveEmoteCodes(ctx context.Context, r *mk.GetActiveEmoteCodesRequest) (*mk.GetActiveEmoteCodesResponse, error) {
	return nil, twirp.NewError(twirp.Unimplemented, "Method no longer supported or implemented")
}
