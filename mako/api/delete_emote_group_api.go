package api

import (
	"context"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/mako/clients"
	"github.com/twitchtv/twirp"
)

type DeleteEmoteGroupAPI struct {
	Clients clients.Clients
}

func (de *DeleteEmoteGroupAPI) DeleteEmoteGroup(ctx context.Context, groupID string) error {
	ctx, cancel := context.WithTimeout(ctx, time.Second*5)
	defer cancel()

	log.Infof("Delete Emote Group: %s", groupID)

	if groupID == "" {
		return twirp.RequiredArgumentError("group_id must not be empty")
	}

	err := de.Clients.EmoteGroupDB.DeleteEmoteGroup(ctx, groupID)
	if err != nil {
		log.WithFields(log.Fields{
			"request": groupID,
		}).WithError(err).Error("Error deleting emote group")
		return twirp.InternalError("Error deleting emote group").
			WithMeta("request", groupID).
			WithMeta("error", err.Error())
	}

	return nil
}
