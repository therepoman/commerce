package api

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/mako/clients"
	productemotegroups_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/productemotegroups"
	clients_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/web/users-service/client/usersclient_internal"
	mkd "code.justin.tv/commerce/mako/twirp/dashboard"
	"code.justin.tv/web/users-service/models"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestUserProductEmoteGroupsAPI(t *testing.T) {
	Convey("Test UserProductEmoteGroups API", t, func() {

		mockUserServiceClient := new(clients_mock.InternalClient)
		mockProductEmoteGroups := new(productemotegroups_mock.ProductEmoteGroups)

		api := UserProductEmoteGroupsAPI{
			Clients: clients.Clients{
				UserServiceClient:        mockUserServiceClient,
				ProductEmoteGroupsClient: mockProductEmoteGroups,
			},
		}

		ctx := context.Background()
		userID := "someUser"
		followerGroupID := "follower-group-id"
		subsTier1StaticGroupID := "subs-tier1-static-group-id"
		subsTier1AnimatedGroupID := "subs-tier1-animated-group-id"
		subsTier2GroupID := "subs-tier2-group-id"
		bitsTier1KGroupID := "bits-tier1k-group-id"
		bitsTier5KGroupID := "bits-tier5k-group-id"

		Convey("with empty requesting_user_id provided", func() {
			req := &mkd.GetUserProductEmoteGroupsRequest{
				RequestingUserId: "",
			}

			resp, err := api.GetUserProductEmoteGroups(ctx, req)
			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("with empty owner_id provided", func() {
			req := &mkd.GetUserProductEmoteGroupsRequest{
				RequestingUserId: userID,
				OwnerId:          "",
			}

			resp, err := api.GetUserProductEmoteGroups(ctx, req)
			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("with requesting_user_id not equal to user_id and user service client error", func() {
			mockUserServiceClient.On("GetUserByID", mock.Anything, userID, mock.Anything).Return(nil, errors.New("dummy error"))

			req := &mkd.GetUserProductEmoteGroupsRequest{
				RequestingUserId: userID,
				OwnerId:          "someOtherUser",
			}

			resp, err := api.GetUserProductEmoteGroups(ctx, req)
			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("with requesting_user_id not equal to user_id and requesting user admin is nil", func() {
			mockUserServiceClient.On("GetUserByID", mock.Anything, userID, mock.Anything).Return(&models.Properties{
				ID:    userID,
				Admin: nil,
			}, nil)

			req := &mkd.GetUserProductEmoteGroupsRequest{
				RequestingUserId: userID,
				OwnerId:          "someOtherUser",
			}

			resp, err := api.GetUserProductEmoteGroups(ctx, req)
			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("with requesting_user_id not equal to user_id and requesting user is not admin", func() {
			mockUserServiceClient.On("GetUserByID", mock.Anything, userID, mock.Anything).Return(&models.Properties{
				ID:    userID,
				Admin: pointers.BoolP(false),
			}, nil)

			req := &mkd.GetUserProductEmoteGroupsRequest{
				RequestingUserId: userID,
				OwnerId:          "someOtherUser",
			}

			resp, err := api.GetUserProductEmoteGroups(ctx, req)
			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("with no emote groups", func() {
			mockProductEmoteGroups.On("GetProductEmoteGroups", mock.Anything, mock.Anything).Return([]*mkd.EmoteGroup{}, nil)

			req := &mkd.GetUserProductEmoteGroupsRequest{
				RequestingUserId: userID,
				OwnerId:          userID,
			}

			resp, err := api.GetUserProductEmoteGroups(ctx, req)
			So(err, ShouldBeNil)
			So(resp, ShouldResemble, &mkd.GetUserProductEmoteGroupsResponse{
				EmoteGroups: []*mkd.EmoteGroup{},
			})
		})

		Convey("with emote groups ", func() {
			mockProductEmoteGroups.On("GetProductEmoteGroups", mock.Anything, mock.Anything).Return([]*mkd.EmoteGroup{{
				Id:          followerGroupID,
				GroupType:   mkd.EmoteGroupType_static_group,
				ProductType: mkd.EmoteGroupProductType_FollowerTier,
			},
				{
					Id:          subsTier1StaticGroupID,
					GroupType:   mkd.EmoteGroupType_static_group,
					ProductType: mkd.EmoteGroupProductType_SubscriptionTier1,
				},
				{
					Id:          subsTier1AnimatedGroupID,
					GroupType:   mkd.EmoteGroupType_animated_group,
					ProductType: mkd.EmoteGroupProductType_SubscriptionTier1,
				},
				{
					Id:          subsTier2GroupID,
					GroupType:   mkd.EmoteGroupType_static_group,
					ProductType: mkd.EmoteGroupProductType_SubscriptionTier2,
				},
				{
					Id:          bitsTier1KGroupID,
					GroupType:   mkd.EmoteGroupType_static_group,
					ProductType: mkd.EmoteGroupProductType_BitsTier1000,
				},
				{
					Id:          bitsTier5KGroupID,
					GroupType:   mkd.EmoteGroupType_static_group,
					ProductType: mkd.EmoteGroupProductType_BitsTier5000,
				}}, nil)
			req := &mkd.GetUserProductEmoteGroupsRequest{
				RequestingUserId: userID,
				OwnerId:          userID,
			}

			resp, err := api.GetUserProductEmoteGroups(ctx, req)
			So(err, ShouldBeNil)
			So(resp, ShouldNotBeNil)
			So(resp.EmoteGroups, ShouldHaveLength, 6)
		})
	})
}
