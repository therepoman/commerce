package api

import (
	"context"
	"time"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/mako/clients"
	"code.justin.tv/commerce/mako/config"
	mako "code.justin.tv/commerce/mako/internal"
	mk "code.justin.tv/commerce/mako/twirp"
	"github.com/twitchtv/twirp"
	"github.com/twitchtv/twirp/hooks/statsd"
	"google.golang.org/protobuf/types/known/timestamppb"
)

type GetUserChannelEmotesAPI struct {
	Clients       clients.Clients
	Stats         statsd.Statter
	DynamicConfig config.DynamicConfigClient
	Config        *config.Configuration
}

func (api *GetUserChannelEmotesAPI) GetUserChannelEmotes(ctx context.Context, r *mk.GetUserChannelEmotesRequest) (*mk.GetUserChannelEmotesResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, 500*time.Millisecond)
	defer cancel()

	err := validateGetUserChannelEmotesRequest(r, api.Config)
	if err != nil {
		return nil, err
	}

	emotes, err := mako.GetUserChannelEmotes(ctx, mako.GetUserChannelEmotesParams{
		EmoteDB:                   api.Clients.EmoteDB,
		EntitlementDB:             api.Clients.MultiSourceEntitlementDB,
		EmoteModifiersDB:          api.Clients.EmoteModifiersDB,
		Dynamic:                   api.DynamicConfig,
		SpecialEventOwnerIDs:      api.Config.SpecialEventOwnerIDs,
		CommunityPointsOwnerID:    api.Config.CommunityPointsOwnerID,
		CommunityPointsEmoteSetID: api.Config.CommunityPointsEmoteSetID,
		UserID:                    r.GetUserId(),
		ChannelID:                 r.GetChannelId(),
	})

	if err != nil {
		msg := "failed to get user channel emotes"
		logrus.WithError(err).WithFields(logrus.Fields{"channelID": r.GetChannelId(), "userID": r.GetUserId()}).Error(msg)
		return nil, twirp.InternalError(msg)
	}

	emoteSets := consolidateForEmoteSets(emotes)

	return &mk.GetUserChannelEmotesResponse{EmoteSets: emoteSets}, nil
}

func validateGetUserChannelEmotesRequest(req *mk.GetUserChannelEmotesRequest, cfg *config.Configuration) error {
	if req.GetUserId() == "" {
		return mk.NewClientError(
			twirp.RequiredArgumentError("user_id"),
			mk.ErrCodeMissingRequiredArgument)
	}

	if req.GetRequestingUserId() == "" {
		return mk.NewClientError(
			twirp.RequiredArgumentError("requesting_user_id"),
			mk.ErrCodeMissingRequiredArgument)
	}

	if req.GetChannelId() == "" {
		return mk.NewClientError(
			twirp.RequiredArgumentError("channel_id"),
			mk.ErrCodeMissingRequiredArgument)
	}

	if req.GetRequestingUserId() != req.GetUserId() {
		return mk.NewClientError(
			twirp.NewError(twirp.PermissionDenied, "user not permitted to get this user's emotes in this channel"),
			mk.ErrCodeUserNotPermitted)
	}

	// TODO: consult with PM/UX on a paginated emote picker, and use that rather than short circuiting on known problem users
	// For users like Nightbot, StreamElements, Slin, etc there are so many admin and gifted subs that trying to load their entitlements
	// or return all their emotes would likely timeout or exceed reasonable response size limits. For this reason, we prevent them from
	// seeing any emotes when they open their emote picker as a way to fail fast without downstream impacts.
	if cfg.MateriaDenylist[req.GetUserId()] {
		return mk.NewClientError(
			twirp.NewError(twirp.FailedPrecondition, "Too many emotes to be able to return safely via this endpoint"),
			mk.ErrCodeResponseTooLarge)
	}

	return nil
}

func consolidateForEmoteSets(emotes []mako.Emote) []*mk.EmoteSet {
	emoteSetsMap := make(map[string]map[string][]*mk.Emote)

	for _, emote := range emotes {
		if emoteSetsMap[emote.OwnerID] == nil {
			emoteSetsMap[emote.OwnerID] = make(map[string][]*mk.Emote)
		}

		// Convert string modifiers ("TK") to twirp modifications (mk.Modification_Thinking)
		modifiers := mako.ModifierCodesToModifiers(emote.Modifiers...)

		emoteSetsMap[emote.OwnerID][emote.GroupID] = append(emoteSetsMap[emote.OwnerID][emote.GroupID], &mk.Emote{
			Id:                    emote.ID,
			Pattern:               emote.Code,
			Modifiers:             modifiers,
			GroupId:               emote.GroupID,
			AssetType:             mako.EmoteAssetTypeAsTwirp(emote.AssetType),
			Type:                  mako.GetEmoteType(emote),
			Order:                 emote.Order,
			CreatedAt:             timestamppb.New(emote.CreatedAt),
			AnimatedEmoteTemplate: mk.AnimatedEmoteTemplate(mk.AnimatedEmoteTemplate_value[string(emote.AnimationTemplate)]),
		})
	}

	emoteSets := make([]*mk.EmoteSet, 0)
	for channelID, emoteGroupMap := range emoteSetsMap {
		for emoteGroupID, emotesInSet := range emoteGroupMap {
			mako.SortTwirpEmotesByOrderAscendingAndCreatedAtDescending(emotesInSet)

			emoteSets = append(emoteSets, &mk.EmoteSet{
				Id:        emoteGroupID,
				Emotes:    emotesInSet,
				ChannelId: channelID,
			})
		}
	}

	return emoteSets
}
