package api

import (
	"context"
	"fmt"
	"time"

	"code.justin.tv/commerce/gogogadget/strings"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/mako/clients"
	"code.justin.tv/commerce/mako/config"
	mako "code.justin.tv/commerce/mako/internal"
	mk "code.justin.tv/commerce/mako/twirp"
	"github.com/twitchtv/twirp"
)

const (
	originIdThirdPartyPrefix = "amzn.twitch.3p.%s"
	// All "Unlocked" Rewards-type emotes are currently owned by the qa_tw_partner channel
	channelIdQaTwitchPartner = "139075904"
)

type Create3PEmoteEntitlementsAPI struct {
	Clients       clients.Clients
	DynamicConfig config.DynamicConfigClient
}

func (c3pe *Create3PEmoteEntitlementsAPI) Create3PEmoteEntitlements(ctx context.Context, r *mk.Create3PEmoteEntitlementsRequest) (*mk.Create3PEmoteEntitlementsResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, 3*time.Second)
	defer cancel()

	if r == nil {
		return nil, twirp.RequiredArgumentError("request")
	}

	log.Infof("Create3PEmoteEntitlements: %s", r.String())

	allowlistMap := c3pe.DynamicConfig.GetThirdPartyEmoteEntitlementsAllowlistMap()

	err := validateRequest(r, allowlistMap)
	if err != nil {
		return nil, err
	}

	var entitlementsToWrite []mako.Entitlement
	now := time.Now()
	// 12:00 AM on 12-23-2020 PST
	entitlementEndTime := time.Date(2020, time.Month(12), 23, 8, 0, 0, 0, time.UTC)

	for _, entitlement := range r.GetEntitlements() {
		entitlementsToWrite = append(entitlementsToWrite, mako.Entitlement{
			ID:        entitlement.GetEmoteId(),
			OwnerID:   entitlement.GetUserId(),
			Source:    mako.RewardsSource,
			Type:      mako.EmoteEntitlement,
			OriginID:  fmt.Sprintf(originIdThirdPartyPrefix, r.GetClientId()),
			ChannelID: channelIdQaTwitchPartner,
			Start:     now,
			End:       &entitlementEndTime,
		})
	}

	if err := c3pe.Clients.EntitlementDB.WriteEntitlements(ctx, entitlementsToWrite...); err != nil {
		msg := "failed to create 3P emote entitlement"
		log.WithFields(log.Fields{
			"client_id":    r.GetClientId(),
			"entitlements": r.GetEntitlements(),
		}).WithError(err).Error(msg)
		return nil, twirp.InternalError(msg)
	}

	return &mk.Create3PEmoteEntitlementsResponse{}, nil
}

func validateRequest(request *mk.Create3PEmoteEntitlementsRequest, allowlistMap map[string][]string) error {
	clientID := request.GetClientId()
	if strings.Blank(clientID) {
		return twirp.RequiredArgumentError("client_id")
	}

	emoteIdAllowlist, ok := allowlistMap[clientID]
	if !ok {
		msg := fmt.Sprintf("client id %s not authorized to call Create3PEmoteEntitlements", clientID)
		return twirp.NewError(twirp.PermissionDenied, msg)
	}

	if len(request.GetEntitlements()) == 0 {
		return twirp.RequiredArgumentError("entitlements")
	}

	for _, entitlement := range request.GetEntitlements() {
		if entitlement == nil {
			return twirp.InvalidArgumentError("entitlement", "cannot be empty")
		}

		userID := entitlement.GetUserId()
		emoteID := entitlement.GetEmoteId()

		if strings.Blank(userID) {
			return twirp.RequiredArgumentError("user_id")
		}

		if strings.Blank(emoteID) {
			return twirp.RequiredArgumentError("emote_id")
		}

		if !strings.NewSetFromArray(emoteIdAllowlist).Contains(emoteID) {
			msg := fmt.Sprintf("client id %s not authorized to entitle emote id %s", clientID, emoteID)
			return twirp.NewError(twirp.PermissionDenied, msg)
		}
	}

	return nil
}
