package api

import (
	"context"
	"fmt"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/mako/clients/imageuploader"
	"code.justin.tv/commerce/mako/config"
	mako "code.justin.tv/commerce/mako/internal"
	mk "code.justin.tv/commerce/mako/twirp"
	mkd "code.justin.tv/commerce/mako/twirp/dashboard"
	"code.justin.tv/web/upload-service/rpc/uploader"
	"github.com/twitchtv/twirp"
)

type GetEmoteUploadConfigAPI interface {
	GetEmoteUploadConfig(ctx context.Context, r *mkd.GetEmoteUploadConfigRequest) (*mkd.GetEmoteUploadConfigResponse, error)
}

type getEmoteUploadConfigAPI struct {
	Config        *config.Configuration
	Uploader      imageuploader.IImageUploader
	MetadataCache mako.EmoteUploadMetadataCache
}

func NewGetEmoteUploadConfigAPI(config *config.Configuration, uploader imageuploader.IImageUploader, redisClient mako.EmoteUploadMetadataCache) GetEmoteUploadConfigAPI {
	return &getEmoteUploadConfigAPI{
		Config:        config,
		Uploader:      uploader,
		MetadataCache: redisClient,
	}
}

func (ue *getEmoteUploadConfigAPI) GetEmoteUploadConfig(ctx context.Context, r *mkd.GetEmoteUploadConfigRequest) (*mkd.GetEmoteUploadConfigResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()

	log.Infof("Get Emote Upload Config: %s", r.String())

	err := validateGetEmoteUploadConfigRequest(r)
	if err != nil {
		return nil, err
	}

	userID := r.GetUserId()
	isAnimated := r.GetAssetType() == mkd.EmoteAssetType_animated
	generateStaticVersionOfAnimatedAssets := r.GetGenerateStaticVersionOfAnimatedAssets()

	var uploadParamsList []imageuploader.EmoteUploadParams
	if r.ResizePlan == mkd.ResizePlan_auto_resize {
		uploadParamsList = append(uploadParamsList, imageuploader.EmoteUploadParams{
			UserID:                               userID,
			Size:                                 imageuploader.StringifySize(mkd.EmoteImageSize_size_original),
			IsAnimated:                           isAnimated,
			GenerateStaticVersionOfAnimatedAsset: generateStaticVersionOfAnimatedAssets,
		})

		// If static assets are not being automatically generated from animated assets, then they need to be uploaded separately
		if isAnimated && !generateStaticVersionOfAnimatedAssets {
			uploadParamsList = append(uploadParamsList, imageuploader.EmoteUploadParams{
				UserID:     userID,
				Size:       imageuploader.StringifySize(mkd.EmoteImageSize_size_original),
				IsAnimated: false,
			})
		}
	} else {
		for _, size := range r.GetSizes() {
			uploadParamsList = append(uploadParamsList, imageuploader.EmoteUploadParams{
				UserID:                               userID,
				Size:                                 imageuploader.StringifySize(size),
				IsAnimated:                           isAnimated,
				GenerateStaticVersionOfAnimatedAsset: generateStaticVersionOfAnimatedAssets,
			})

			// If static assets are not being automatically generated from animated assets, then they need to be uploaded separately
			if isAnimated && !generateStaticVersionOfAnimatedAssets {
				uploadParamsList = append(uploadParamsList, imageuploader.EmoteUploadParams{
					UserID:     userID,
					Size:       imageuploader.StringifySize(size),
					IsAnimated: false,
				})
			}
		}
	}

	var uploadConfigs []*mkd.EmoteUploadConfig
	uploadMetadataCached := false
	for _, uploadParams := range uploadParamsList {
		var response *uploader.UploadResponse
		response, err = ue.Uploader.CreateEmoteUploadRequest(ctx, uploadParams)
		if err != nil {
			msg := "Error getting emote upload config"
			log.WithFields(log.Fields{
				"request":      r.String(),
				"uploadParams": uploadParams,
			}).WithError(err).Error(msg)
			return nil, twirp.InternalError(msg).
				WithMeta("request", r.String()).
				WithMeta("error", err.Error())
		}

		assetType := mkd.EmoteAssetType_static
		if uploadParams.IsAnimated {
			assetType = mkd.EmoteAssetType_animated
		}

		if uploadParams.Size == imageuploader.SizeOriginalString {
			uploadConfig := &mkd.EmoteUploadConfig{
				Id:        response.GetUploadId(),
				Url:       response.GetUrl(),
				Size:      mkd.EmoteImageSize_size_original,
				AssetType: assetType,
				Images: []*mkd.EmoteImage{
					ue.generateEmoteImageBlock(r.GetUserId(), response.GetUploadId(), mkd.EmoteImageSize_size_1x, assetType),
					ue.generateEmoteImageBlock(r.GetUserId(), response.GetUploadId(), mkd.EmoteImageSize_size_2x, assetType),
					ue.generateEmoteImageBlock(r.GetUserId(), response.GetUploadId(), mkd.EmoteImageSize_size_4x, assetType),
				},
			}

			// If static assets are being generated from animated assets, include the static images as well
			if uploadParams.IsAnimated && uploadParams.GenerateStaticVersionOfAnimatedAsset {
				uploadConfig.Images = append(uploadConfig.Images,
					ue.generateEmoteImageBlock(r.GetUserId(), response.GetUploadId(), mkd.EmoteImageSize_size_1x, mkd.EmoteAssetType_static),
					ue.generateEmoteImageBlock(r.GetUserId(), response.GetUploadId(), mkd.EmoteImageSize_size_2x, mkd.EmoteAssetType_static),
					ue.generateEmoteImageBlock(r.GetUserId(), response.GetUploadId(), mkd.EmoteImageSize_size_4x, mkd.EmoteAssetType_static),
				)
			}

			uploadConfigs = append(uploadConfigs, uploadConfig)

			// Check if metadata was already cached. This can occur for animated emotes whose static assets are not auto-generated.
			if !uploadMetadataCached {
				uploadID4x := predict4xEmoteIDFromUploadID(r.GetUserId(), response.GetUploadId())
				ue.MetadataCache.StoreUploadMethod(ctx, uploadID4x, mako.EmoteUploadMethodSimple)
				uploadMetadataCached = true
			}

		} else {
			uploadConfig := &mkd.EmoteUploadConfig{
				Id:        response.GetUploadId(),
				Url:       response.GetUrl(),
				Size:      imageuploader.StringSizeToTwirp(uploadParams.Size),
				AssetType: assetType,
				Images: []*mkd.EmoteImage{
					ue.generateEmoteImageBlock(r.GetUserId(), response.GetUploadId(), imageuploader.StringSizeToTwirp(uploadParams.Size), assetType),
				},
			}

			// If static assets are being generated from animated assets, include the static images as well
			if uploadParams.IsAnimated && uploadParams.GenerateStaticVersionOfAnimatedAsset {
				uploadConfig.Images = append(uploadConfig.Images,
					ue.generateEmoteImageBlock(r.GetUserId(), response.GetUploadId(), imageuploader.StringSizeToTwirp(uploadParams.Size), mkd.EmoteAssetType_static),
				)
			}

			uploadConfigs = append(uploadConfigs, uploadConfig)

			if uploadParams.Size == imageuploader.Size4xString && !uploadMetadataCached {
				uploadID4x := predict4xEmoteIDFromUploadID(r.GetUserId(), response.GetUploadId())
				ue.MetadataCache.StoreUploadMethod(ctx, uploadID4x, mako.EmoteUploadMethodAdvanced)
				uploadMetadataCached = true
			}
		}
	}

	return &mkd.GetEmoteUploadConfigResponse{UploadConfigs: uploadConfigs}, nil
}

func predict4xEmoteIDFromUploadID(userID, uploadID string) string {
	// userid_4x_uploadID as defined in the imageuploader package
	return fmt.Sprintf(imageuploader.ImageIDStaticFormat, userID, imageuploader.Size4xString, uploadID)
}

func (ue *getEmoteUploadConfigAPI) generateEmoteImageBlock(userID, uploadID string, size mkd.EmoteImageSize, assetType mkd.EmoteAssetType) *mkd.EmoteImage {
	format := imageuploader.ImageIDStaticFormat
	if assetType == mkd.EmoteAssetType_animated {
		format = imageuploader.ImageIDAnimatedFormat
	}

	imageID := fmt.Sprintf(format, userID, imageuploader.StringifySize(size), uploadID)
	return &mkd.EmoteImage{
		Id:        imageID,
		Url:       fmt.Sprintf(imageuploader.ImageS3URLFormat, ue.Config.UploadS3Host, ue.Config.UploadS3BucketName, imageID),
		Size:      size,
		AssetType: assetType,
	}
}

func validateGetEmoteUploadConfigRequest(req *mkd.GetEmoteUploadConfigRequest) error {
	if req == nil {
		return mk.NewClientError(
			twirp.RequiredArgumentError("request"),
			mk.ErrCodeMissingRequiredArgument)
	}

	if req.GetUserId() == "" {
		return mk.NewClientError(
			twirp.RequiredArgumentError("user_id"),
			mk.ErrCodeMissingRequiredArgument)
	}

	if req.GetResizePlan() == mkd.ResizePlan_no_resize {
		if len(req.GetSizes()) == 0 || len(req.GetSizes()) > 3 {
			return mk.NewClientError(
				twirp.InvalidArgumentError("sizes", "supports between 1-3 values"),
				mk.ErrCodeInvalidQuantityOfSizes)
		}

		var seen1x, seen2x, seen4x bool
		for i, size := range req.GetSizes() {
			switch size {
			case mkd.EmoteImageSize_size_original:
				return mk.NewClientError(
					twirp.InvalidArgumentError(fmt.Sprintf("sizes[%d]", i), "cannot request to upload an original resolution image while also requesting that no resizing be performed"),
					mk.ErrCodeInvalidSize)
			case mkd.EmoteImageSize_size_1x:
				if seen1x {
					return mk.NewClientError(
						twirp.InvalidArgumentError("sizes", "cannot contain a duplicate size"),
						mk.ErrCodeDuplicateSize)
				}
				seen1x = true
			case mkd.EmoteImageSize_size_2x:
				if seen2x {
					return mk.NewClientError(
						twirp.InvalidArgumentError("sizes", "cannot contain a duplicate sized"),
						mk.ErrCodeDuplicateSize)
				}
				seen2x = true
			case mkd.EmoteImageSize_size_4x:
				if seen4x {
					return mk.NewClientError(
						twirp.InvalidArgumentError("sizes", "cannot contain a duplicate size"),
						mk.ErrCodeDuplicateSize)
				}
				seen4x = true
			default:
				return mk.NewClientError(
					twirp.InvalidArgumentError(fmt.Sprintf("sizes[%d]", i), "is of an unknown size"),
					mk.ErrCodeInvalidSize)
			}
		}
	} else if req.GetResizePlan() == mkd.ResizePlan_auto_resize {
		if len(req.GetSizes()) > 1 {
			return mk.NewClientError(
				twirp.InvalidArgumentError("sizes", "sizes only supports a single value of size_original for auto_resize plan (which itself is optional)"),
				mk.ErrCodeInvalidQuantityOfSizes)
		} else if len(req.GetSizes()) == 1 && req.GetSizes()[0] != mkd.EmoteImageSize_size_original {
			return mk.NewClientError(
				twirp.InvalidArgumentError("sizes", "can only contain original size when auto_resize plan is specified"),
				mk.ErrCodeInvalidSize)
		}
	} else {
		return mk.NewClientError(
			twirp.InvalidArgumentError("resize_plan", "is not of a supported type"),
			mk.ErrCodeInvalidResizePlan)
	}

	if req.GetGenerateStaticVersionOfAnimatedAssets() && req.GetAssetType() != mkd.EmoteAssetType_animated {
		return mk.NewClientError(
			twirp.InvalidArgumentError("generate_static_version_of_animated_assets", "cannot be true when asset_type is not animated"),
			mk.ErrCodeInvalidStaticAssetGeneration)
	}

	return nil
}
