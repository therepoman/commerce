package api

import (
	"context"
	"sort"
	"strconv"
	"time"

	"github.com/twitchtv/twirp"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/mako/clients"
	"code.justin.tv/commerce/mako/config"
	mako "code.justin.tv/commerce/mako/internal"
	mk "code.justin.tv/commerce/mako/twirp"
)

// EmoteSetDetailsAPI includes the API for getting all emotes given emote sets
type EmoteSetDetailsAPI struct {
	Clients        clients.Clients
	CampaignConfig config.CampaignEmoteOwners
	Config         *config.Configuration
}

// GetEmoteSetDetails Get all entitled emoticon sets for a user
func (emoteSetDetailsAPI *EmoteSetDetailsAPI) GetEmoteSetDetails(ctx context.Context, r *mk.GetEmoteSetDetailsRequest) (*mk.GetEmoteSetDetailsResponse, error) {
	internalCtx, cancel := context.WithTimeout(ctx, 600*time.Millisecond)
	defer cancel()

	defer func(start time.Time) {
		dur := time.Since(start)
		if dur > 200*time.Millisecond {
			logrus.WithField("userID", r.GetUserId()).Infof("Slow GetEmoteSetDetails request, duration: %s", dur.String())
		}
	}(time.Now())

	userID := r.GetUserId()
	channelID := r.GetChannelId()

	allEmoticons, err := emoteSetDetailsAPI.Clients.EmoticonsAggregator.GetAllUserEmoticonsByGroup(internalCtx, userID, channelID)
	if err != nil {
		return &mk.GetEmoteSetDetailsResponse{
			EmoteSets: []*mk.EmoteSet{},
		}, twirp.InternalErrorWith(err)
	}

	// emoteGroupIndex is used to remove duplicate emote sets from different sources.
	emoteGroupIndex := make(map[string]struct{})

	materiaEntitlements := []*mk.EmoteSet{}
	// Prefer emotes that come from Materia as there's additional deduping logic in the EmoticonsAggregator that
	// always picks Materia emotes if there's any dups so this needs to stay consistent here.
	for key, emoteSet := range allEmoticons.Materia {
		if _, ok := emoteGroupIndex[key]; !ok {
			channelIDOverride := ""
			emoteSet := mk.EmoteSet{Id: key, Emotes: emoteSet.Emotes, ChannelId: emoteSet.ChannelId}

			if emoteSetDetailsAPI.Config != nil {
				channelIDOverride = emoteSetDetailsAPI.Config.CommunityPointsOwnerID

				if emoteSetDetailsAPI.Config.CommunityPointsEmoteSetID == key {
					emoteSet.ChannelId = channelIDOverride
				} else if emoteSetDetailsAPI.Config.SpecialEventOwnerIDs[key] {
					// Set channel to groupID for special events (HypeTrain, MegaCommerce...)
					emoteSet.ChannelId = key
				}
			}

			materiaEntitlements = append(materiaEntitlements, &emoteSet)
			emoteGroupIndex[key] = struct{}{}
		}
	}

	// Convert EntitlementsDB emoticons to twirp response
	entitlementsDBEntitlements := []*mk.EmoteSet{}
	for key, emotes := range allEmoticons.EntitlementsDB {
		if _, ok := emoteGroupIndex[key]; !ok {
			entitlementsDBEntitlements = append(entitlementsDBEntitlements, &mk.EmoteSet{Id: key, Emotes: emotes})
			emoteGroupIndex[key] = struct{}{}
		}
	}

	// Add channelID to dynamoDB entitlements
	for i, emoteSet := range entitlementsDBEntitlements {
		if channelID := emoteSetDetailsAPI.CampaignConfig.CampaignEmoteOwner(emoteSet.Emotes[0].Id); channelID != "" {
			entitlementsDBEntitlements[i].ChannelId = channelID
		}
	}

	// Convert follower entitlements to twirp response
	followerEntitlements := []*mk.EmoteSet{}
	if len(allEmoticons.FollowerEmotes) != 0 {
		emoticons := make([]*mk.Emote, 0, len(allEmoticons.FollowerEmotes))
		for _, emote := range allEmoticons.FollowerEmotes {
			twirpEmote := emote.ToLegacyTwirpEmote()
			emoticons = append(emoticons, &twirpEmote)
		}
		followerEntitlements = append(followerEntitlements, &mk.EmoteSet{Id: allEmoticons.FollowerEmotes[0].GroupID, Emotes: emoticons, ChannelId: channelID})
	}

	// Combine entitlementsDB and siteDB emote sets
	emoteSets := make([]*mk.EmoteSet, 0, len(entitlementsDBEntitlements)+len(materiaEntitlements)+len(followerEntitlements))

	emoteSets = append(emoteSets, entitlementsDBEntitlements...)
	emoteSets = append(emoteSets, materiaEntitlements...)
	emoteSets = append(emoteSets, followerEntitlements...)

	// Joins modified emote data to their respective emote sets
	if !emoteSetDetailsAPI.Config.MateriaDenylist[userID] {
		emoteSets = emoteSetDetailsAPI.filterModifiedEmotes(internalCtx, emoteSets, userID)
	}

	// Remove duplicates
	emoteSets = removeDuplicateEmotes(emoteSets)

	return &mk.GetEmoteSetDetailsResponse{EmoteSets: emoteSets}, nil
}

// removeDuplicateEmotes Takes care of the case where a prime user would have both the monkeys/glitches returned
// along with the robots causing mapping issues on the front-end
func removeDuplicateEmotes(emoteSets []*mk.EmoteSet) []*mk.EmoteSet {
	// Remove duplicate patterns by taking the one with the higher id
	// First iteration pulls out all the patterns with the highest ids
	desiredEmotes := make(map[string]int)
	for _, emoteSet := range emoteSets {
		for _, emote := range emoteSet.Emotes {
			var emoteID, err = strconv.Atoi(emote.Id)
			if err != nil {
				continue
			}
			if val, ok := desiredEmotes[emote.Pattern]; ok {
				// Default smilies have smaller ids
				if val < emoteID {
					desiredEmotes[emote.Pattern] = emoteID
				}
			} else {
				desiredEmotes[emote.Pattern] = emoteID
			}
		}
	}

	// Second iteration re-creates the objects with only the patterns with the highest ids
	var cleanEmoteSets = []*mk.EmoteSet{}
	for _, emoteSet := range emoteSets {
		var cleanEmoteSet = &mk.EmoteSet{Id: emoteSet.Id, ChannelId: emoteSet.ChannelId}
		var cleanEmotes = []*mk.Emote{}
		for _, emote := range emoteSet.Emotes {
			var emoteID, err = strconv.Atoi(emote.Id)
			if err != nil {
				cleanEmotes = append(cleanEmotes, emote)
				continue
			}
			if desiredEmotes[emote.Pattern] == emoteID {
				cleanEmotes = append(cleanEmotes, emote)
			}
		}
		if len(cleanEmotes) == 0 {
			continue
		}
		cleanEmoteSet.Emotes = cleanEmotes
		cleanEmoteSets = append(cleanEmoteSets, cleanEmoteSet)
	}
	return cleanEmoteSets
}

// Joins modified emote data to their respective emote sets.
func (api *EmoteSetDetailsAPI) filterModifiedEmotes(ctx context.Context, emoteSets []*mk.EmoteSet, userID string) []*mk.EmoteSet {
	emoteGroupIndex := make(map[string]struct{})
	emoteModGroupOwnerIDMap := make(map[string]struct{})
	emoteModGroupOwnerIDs := []string{}

	for _, emoteSet := range emoteSets {
		// Do not read emote modifier groups for special event (MegaCommerce) based channels.
		if api.Config.SpecialEventOwnerIDs[emoteSet.ChannelId] {
			continue
		}
		emoteModGroupOwnerIDMap[emoteSet.ChannelId] = struct{}{}
		emoteGroupIndex[emoteSet.Id] = struct{}{}
	}

	for ownerID := range emoteModGroupOwnerIDMap {
		if ownerID == "" {
			continue
		}
		emoteModGroupOwnerIDs = append(emoteModGroupOwnerIDs, ownerID)
	}
	// If there are no emote mod group owners to lookup, no filtering is required.
	if len(emoteModGroupOwnerIDs) == 0 {
		return emoteSets
	}

	// Attempt to look up the cached emote modifier group result for the requesting user.
	// If no cache value exists, lookup all modifier groups by ownerIDs, and write it to the cache.
	groups, err := api.Clients.EmoteModifiersDB.ReadEntitledGroupsForUser(ctx, userID)
	if err != nil || groups == nil {
		if err != nil {
			logrus.WithField("userID", userID).WithError(err).Error("could not retrieve entitled emote modifier groups for given user, falling back on fetching by owner IDs")
		}

		groupsByOwnerID, err := api.Clients.EmoteModifiersDB.ReadByOwnerIDs(ctx, emoteModGroupOwnerIDs...)
		if err != nil {
			logrus.WithField("emoteModGroupOwnerIDs", emoteModGroupOwnerIDs).WithError(err).Error("could not retrieve emote modifier groups for given ownerIDs")
		} else {
			groups = make([]mako.EmoteModifierGroup, len(groupsByOwnerID))
			copy(groups, groupsByOwnerID)
			go func() {
				ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
				defer cancel()
				if err := api.Clients.EmoteModifiersDB.WriteEntitledGroupsForUser(ctx, userID, groupsByOwnerID...); err != nil {
					logrus.WithField("userID", userID).WithError(err).Error("could not write emote modifier groups for given user")
				}
			}()
		}
	}

	modifiersByChannelID := map[string][]string{}
	modifiableEmoteSets := map[string]bool{}
	// Sorting groups by the number of SourceEmoteGroupIDs, so t2 groups will come before t3 groups.
	sort.Slice(groups, func(a, b int) bool {
		return len(groups[b].SourceEmoteGroupIDs) > len(groups[a].SourceEmoteGroupIDs)
	})
	for _, group := range groups {
		userEntitled := true
		for _, sourceEmoteGroup := range group.SourceEmoteGroupIDs {
			modifiableEmoteSets[sourceEmoteGroup] = true
			if _, ok := emoteGroupIndex[sourceEmoteGroup]; !ok {
				userEntitled = false
			}
		}
		if userEntitled {
			modifiersByChannelID[group.OwnerID] = append(modifiersByChannelID[group.OwnerID], group.Modifiers...)
		}
	}

	for _, emoteSet := range emoteSets {
		modifiers := modifiersByChannelID[emoteSet.ChannelId]
		if modifiableEmoteSets[emoteSet.Id] && len(modifiers) > 0 {
			for _, emote := range emoteSet.Emotes {
				if emoteCanBeModified(emote) {
					emote.Modifiers = mako.ModifierCodesToModifiers(modifiers...)
				}
			}
		}
	}

	return emoteSets
}

func emoteCanBeModified(emote *mk.Emote) bool {
	return emote.AssetType != mk.EmoteAssetType_animated
}
