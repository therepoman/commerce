package api

import (
	"context"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/mako/clients"
	mako "code.justin.tv/commerce/mako/internal"
	mk "code.justin.tv/commerce/mako/twirp"
	"github.com/pkg/errors"
	"github.com/twitchtv/twirp"
)

// EmoticonsByOwnerAPI includes the API for getting all emotes by owner id
type EmoticonsByOwnerAPI struct {
	Clients clients.Clients
	EmoteDB mako.EmoteDB
}

// GetAllEmotesByOwnerId gets all emotes for the given owner, filtered by the given domain
func (api *EmoticonsByOwnerAPI) GetAllEmotesByOwnerId(ctx context.Context, r *mk.GetAllEmotesByOwnerIdRequest) (*mk.GetAllEmotesByOwnerIdResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, time.Second)
	defer cancel()

	ownerId := r.GetOwnerId()

	if ownerId == "" {
		return nil, twirp.InvalidArgumentError("owner_id", "cannot be empty")
	}

	if r.Filter == nil {
		return nil, twirp.InvalidArgumentError("filter", "filter is empty")
	}

	// Callers may request that Mako verifies that the authed user should be allowed to make this request on behalf of the owner
	// Current validation rules allow only the user themself or members of twitch staff to make this request.
	if r.EnforceOwnerRestriction {
		if r.RequestingUserId == "" {
			return nil, twirp.InvalidArgumentError("requesting_user_id", "cannot be empty when owner restrictions are enforced")
		}

		permitted, err := api.isUserPermitted(ctx, ownerId, r.RequestingUserId)
		if err != nil {
			return nil, twirp.InternalErrorWith(err)
		}

		if !permitted {
			return nil, twirp.NewError(twirp.PermissionDenied, "requesting user not allowed to view all of this owner's emotes")
		}
	}

	domain, exists := mk.Domain_name[int32(r.Filter.Domain)]
	if !exists {
		return nil, twirp.InvalidArgumentError("domain", "is invalid")
	}

	var states []mako.EmoteState

	for _, mkState := range r.Filter.States {
		state, exists := mk.EmoteState_name[int32(mkState)]
		if !exists {
			return nil, twirp.InvalidArgumentError("state", "is invalid")
		}

		states = append(states, mako.EmoteState(state))
	}

	dynamoEmotes, err := api.EmoteDB.Query(ctx, mako.EmoteQuery{
		OwnerID: ownerId,
		Domain:  mako.Domain(domain),
		States:  states,
	})
	if err != nil {
		log.WithError(err)
		return nil, twirp.InternalErrorWith(err)
	}

	var emoticons []*mk.Emoticon
	for _, emote := range dynamoEmotes {
		emoticon := emote.ToTwirp()

		// This ProductId override is to preserve previous behavior in case it was expected for this API. This is technically incorrect but this
		// field is deprecated anyway so I'd rather keep the behavior the same as it previously worked rather than fix it.
		emoticon.ProductId = emote.GroupID

		emoticons = append(emoticons, &emoticon)
	}

	return &mk.GetAllEmotesByOwnerIdResponse{
		Emoticons: emoticons,
	}, nil
}

func (api *EmoticonsByOwnerAPI) GetAllEmotesWithModificationsByOwnerID(ctx context.Context, r *mk.GetAllEmotesWithModificationsByOwnerIDRequest) (*mk.GetAllEmotesWithModificationsByOwnerIDResponse, error) {
	// matching our historical metrics and GQL timeout
	ctx, cancel := context.WithTimeout(ctx, time.Second)
	defer cancel()

	ownerId := r.GetOwnerId()

	if ownerId == "" {
		return nil, twirp.InvalidArgumentError("owner_id", "cannot be empty")
	}

	emotes, err := api.getProductEmotes(ctx, ownerId)
	if err != nil {
		log.WithError(err).Error("Error channel emoticons")
		return nil, twirp.InternalError("Error getting channel emoticons").
			WithMeta("request", r.String())
	}

	emoteGroups := make([]*mk.EmoteModificationGroup, 0, len(mk.Modification_name))
	for mod := range mk.Modification_name {
		modification := mk.Modification(mod)
		emotesWithModified := make([]*mk.EmoteWithModified, 0, len(emotes))
		for _, emote := range emotes {
			if modification == mk.Modification_ModificationNone {
				emotesWithModified = append(emotesWithModified, &mk.EmoteWithModified{
					BaseEmote:     emote,
					ModifiedEmote: nil,
				})
				continue
			}

			modifiedEmote := mk.ModifiedEmote{
				Id:   emote.Id + mako.GetModifierSuffix(modification),
				Code: emote.Code + mako.GetModifierSuffix(modification),
			}

			emotesWithModified = append(emotesWithModified, &mk.EmoteWithModified{
				BaseEmote:     emote,
				ModifiedEmote: &modifiedEmote,
			})
		}

		emoteGroups = append(emoteGroups, &mk.EmoteModificationGroup{
			Modification: modification,
			Emotes:       emotesWithModified,
		})
	}

	return &mk.GetAllEmotesWithModificationsByOwnerIDResponse{
		EmoteGroups: emoteGroups,
	}, nil
}

// The user is permitted if the requestingUserID is requesting their own emotes or if the requestingUserID is a member of twitch staff
func (api *EmoticonsByOwnerAPI) isUserPermitted(ctx context.Context, ownerID, requestingUserID string) (bool, error) {
	if ownerID == requestingUserID {
		return true, nil
	}

	requestingUser, err := api.Clients.UserServiceClient.GetUserByID(ctx, requestingUserID, nil)
	if err != nil {
		msg := "error looking up requesting user while validating AllEmotesByOwnerID request"
		log.WithError(err).WithField("requestingUserID", requestingUserID).Error(msg)
		return false, errors.Wrap(err, msg)
	}

	if requestingUser.Admin != nil && *requestingUser.Admin {
		return true, nil
	}

	return false, nil
}

func (api *EmoticonsByOwnerAPI) getProductEmotes(ctx context.Context, ownerID string) ([]*mk.Emoticon, error) {
	products, err := api.Clients.SubscriptionsClient.GetChannelProducts(ctx, ownerID)
	if err != nil {
		return nil, err
	}

	// short circuit if no products
	if len(products.Products) <= 0 {
		return []*mk.Emoticon{}, nil
	}

	groupIds := make([]string, len(products.Products), len(products.Products))
	productIdsByGroupId := make(map[string]string, len(products.Products))
	for i, product := range products.Products {
		groupId := product.EmoticonSetId
		groupIds[i] = groupId
		productIdsByGroupId[groupId] = product.Id
	}

	emotes, err := api.Clients.EmoteDB.ReadByGroupIDs(ctx, groupIds...)
	if err != nil {
		return nil, err
	}

	var twirpEmotes []*mk.Emoticon
	for _, emote := range emotes {
		if emote.State != mako.Active {
			continue
		}

		tempEmote := emote.ToTwirp()
		tempEmote.ProductId = productIdsByGroupId[tempEmote.GroupId]
		twirpEmotes = append(twirpEmotes, &tempEmote)
	}

	return twirpEmotes, nil
}
