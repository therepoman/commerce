package api

import (
	"context"
	"testing"

	. "github.com/smartystreets/goconvey/convey"

	mkd "code.justin.tv/commerce/mako/twirp/dashboard"
)

func TestUserRolloutStatusAPI(t *testing.T) {
	Convey("Test GetUserEmoteLimits API", t, func() {
		userRolloutStatusAPI := UserRolloutStatusAPI{}

		ctx := context.Background()
		userID := "someUser"

		Convey("with empty user_id provided", func() {
			req := &mkd.GetUserRolloutStatusRequest{
				UserId: "",
			}

			resp, err := userRolloutStatusAPI.GetUserRolloutStatus(ctx, req)
			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("with success", func() {
			req := &mkd.GetUserRolloutStatusRequest{
				UserId: userID,
			}

			resp, err := userRolloutStatusAPI.GetUserRolloutStatus(ctx, req)
			So(resp, ShouldNotBeNil)
			So(resp.IsDigitalAssetManagerM2Enabled, ShouldBeTrue)
			So(err, ShouldBeNil)
		})
	})
}
