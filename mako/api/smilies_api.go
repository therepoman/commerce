package api

import (
	"context"
	"sort"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/mako/clients"
	mako "code.justin.tv/commerce/mako/internal"
	mk "code.justin.tv/commerce/mako/twirp"
	"github.com/twitchtv/twirp"
)

// SmiliesAPI includes the APIs for all turbo smilie related functions.
type SmiliesAPI struct {
	Clients clients.Clients
}

func (s *SmiliesAPI) GetSelectedSmilies(ctx context.Context, r *mk.GetSelectedSmiliesRequest) (*mk.GetSelectedSmiliesResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, time.Second)
	defer cancel()

	if r.UserId == "" {
		return nil, mk.NewClientError(twirp.RequiredArgumentError("user_id"), mk.ErrCodeMissingRequiredArgument)
	}

	smilie, err := mako.ReadUserSmilies(ctx, s.Clients.EntitlementDB, r.UserId)
	if err != nil {
		log.WithError(err).Error("Error getting materia selected smilies")
		return nil, twirp.InternalError("Error getting selected smilies").
			WithMeta("request", r.String()).
			WithMeta("error", err.Error())
	}

	return &mk.GetSelectedSmiliesResponse{
		SmiliesSetId: makeSmilie(smilie),
	}, nil
}

func (s *SmiliesAPI) GetAllSmilies(ctx context.Context, r *mk.GetAllSmiliesRequest) (*mk.GetAllSmiliesResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, time.Second)
	defer cancel()

	smiliesSetsMap := mako.GetAllSmilies()

	var smiliesSets []*mk.SmiliesSet
	for groupID, emotes := range smiliesSetsMap {
		var emoticons []*mk.Emoticon
		for _, emote := range emotes {
			emoticon := emote.ToTwirp()
			emoticons = append(emoticons, &emoticon)
		}

		smiliesSets = append(smiliesSets, &mk.SmiliesSet{
			SmiliesSetId: makeSmilie(groupID),
			Emoticons:    emoticons,
		})

		// Sort the smilies sets by groupID ascending, since we converted from map to slice and golang doesn't
		// guarantee preservation of key/value map orders
		sort.Slice(smiliesSets, func(i, j int) bool {
			return smiliesSets[i].SmiliesSetId < smiliesSets[j].SmiliesSetId
		})
	}

	return &mk.GetAllSmiliesResponse{
		SmiliesSets: smiliesSets,
	}, nil
}

func (s *SmiliesAPI) SetSmilies(ctx context.Context, r *mk.SetSmiliesRequest) (*mk.SetSmiliesResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, time.Second)
	defer cancel()

	if r.UserId == "" {
		return nil, mk.NewClientError(twirp.RequiredArgumentError("user_id"), mk.ErrCodeMissingRequiredArgument)
	}

	if r.SmiliesSetId == mk.SmiliesSetId_NONE {
		return nil, mk.NewClientError(twirp.RequiredArgumentError("smilies_set_id"), mk.ErrCodeMissingRequiredArgument)
	}

	var smilie mako.Smilie
	switch r.SmiliesSetId {
	case mk.SmiliesSetId_MONKEYS:
		smilie = mako.MonkeysSmilieGroupID
	case mk.SmiliesSetId_PURPLE:
		smilie = mako.PurpleSmilieGroupID
	default:
		smilie = mako.RobotsSmilieGroupID
	}

	if err := mako.UpdateUserSmilies(ctx, s.Clients.EntitlementDB, r.UserId, smilie); err != nil {
		log.WithError(err).Error("Error setting mako smilies")

		return nil, twirp.InternalError("Error setting mako smilies").
			WithMeta("request", r.String()).
			WithMeta("error", err.Error())
	}

	return &mk.SetSmiliesResponse{
		SmiliesSetId: makeSmilie(smilie),
	}, nil
}

func makeSmilie(val mako.Smilie) mk.SmiliesSetId {
	switch val {
	case mako.MonkeysSmilieGroupID:
		return mk.SmiliesSetId_MONKEYS
	case mako.PurpleSmilieGroupID:
		return mk.SmiliesSetId_PURPLE
	}

	return mk.SmiliesSetId_ROBOTS
}
