package api

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/mako/clients"
	mako "code.justin.tv/commerce/mako/internal"
	"code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/internal_mock"
	mk "code.justin.tv/commerce/mako/twirp"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestEmoteGroupsAPI_CreateEmoteGroup(t *testing.T) {
	Convey("Test CreateEmoteGroup API", t, func() {
		mockEmoteGroupDB := new(internal_mock.EmoteGroupDB)

		groupsAPI := CreateEmoteGroupAPI{
			Clients: clients.Clients{
				EmoteGroupDB: mockEmoteGroupDB,
			},
		}

		Convey("when the request object is nil, we should return an error", func() {
			resp, err := groupsAPI.CreateEmoteGroup(context.Background(), nil)

			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("when the request origin is not supported, we should return an error", func() {
			resp, err := groupsAPI.CreateEmoteGroup(context.Background(), &mk.CreateEmoteGroupRequest{
				Origin: mk.EmoteGroup_Globals,
			})

			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("when the groups client returns an error, we should return an error", func() {
			mockEmoteGroupDB.On("CreateEmoteGroup", mock.Anything, mock.Anything).Return(mako.EmoteGroup{}, false, errors.New("ERROR"))

			resp, err := groupsAPI.CreateEmoteGroup(context.Background(), &mk.CreateEmoteGroupRequest{
				Origin: mk.EmoteGroup_BitsBadgeTierEmotes,
			})

			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("when the groups client returns a groupID, we should return it", func() {
			mockEmoteGroup := mako.EmoteGroup{
				GroupID: "very-unique-123",
				Origin:  mako.ToMakoGroupOrigin(mk.EmoteGroup_BitsBadgeTierEmotes),
			}
			mockEmoteGroupDB.On("CreateEmoteGroup", mock.Anything, mock.Anything).Return(mockEmoteGroup, false, nil)

			resp, err := groupsAPI.CreateEmoteGroup(context.Background(), &mk.CreateEmoteGroupRequest{
				Origin: mk.EmoteGroup_BitsBadgeTierEmotes,
			})

			So(err, ShouldBeNil)
			So(resp, ShouldResemble, &mk.CreateEmoteGroupResponse{
				GroupId: "very-unique-123",
			})
		})

		Convey("when group is static", func() {
			mockEmoteGroup := mako.EmoteGroup{
				GroupID:   "very-unique-123",
				Origin:    mako.ToMakoGroupOrigin(mk.EmoteGroup_BitsBadgeTierEmotes),
				GroupType: mako.ToMakoGroupAssetType(mk.EmoteGroupType_static_group),
			}
			mockEmoteGroupDB.On("CreateEmoteGroup", mock.Anything, mock.Anything).Return(mockEmoteGroup, false, nil)

			resp, err := groupsAPI.CreateEmoteGroup(context.Background(), &mk.CreateEmoteGroupRequest{
				Origin:    mk.EmoteGroup_BitsBadgeTierEmotes,
				GroupType: mk.EmoteGroupType_static_group,
			})

			So(err, ShouldBeNil)
			So(resp, ShouldResemble, &mk.CreateEmoteGroupResponse{
				GroupId: "very-unique-123",
			})
		})

		Convey("when group is animated", func() {
			mockEmoteGroup := mako.EmoteGroup{
				GroupID:   "very-unique-123",
				Origin:    mako.ToMakoGroupOrigin(mk.EmoteGroup_BitsBadgeTierEmotes),
				GroupType: mako.ToMakoGroupAssetType(mk.EmoteGroupType_animated_group),
			}
			mockEmoteGroupDB.On("CreateEmoteGroup", mock.Anything, mock.Anything).Return(mockEmoteGroup, false, nil)

			resp, err := groupsAPI.CreateEmoteGroup(context.Background(), &mk.CreateEmoteGroupRequest{
				Origin:    mk.EmoteGroup_BitsBadgeTierEmotes,
				GroupType: mk.EmoteGroupType_animated_group,
			})

			So(err, ShouldBeNil)
			So(resp, ShouldResemble, &mk.CreateEmoteGroupResponse{
				GroupId: "very-unique-123",
			})
		})
	})
}
