package api

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/mako/clients"
	"code.justin.tv/commerce/mako/clients/emotelimits"
	emotelimits_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/emotelimits"
	clients_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/web/users-service/client/usersclient_internal"
	mkd "code.justin.tv/commerce/mako/twirp/dashboard"
	"code.justin.tv/web/users-service/models"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestUserEmoteLimitsAPI(t *testing.T) {
	Convey("Test GetUserEmoteLimits API", t, func() {
		mockEmoteLimitsManager := new(emotelimits_mock.EmoteLimitsManager)
		mockUserServiceClient := new(clients_mock.InternalClient)

		userEmoteLimitsAPI := UserEmoteLimitsAPI{
			Clients: clients.Clients{
				EmoteLimitsManager: mockEmoteLimitsManager,
				UserServiceClient:  mockUserServiceClient,
			},
		}

		ctx := context.Background()
		userID := "someUser"

		Convey("with empty requesting_user_id provided", func() {
			req := &mkd.GetUserEmoteLimitsRequest{
				RequestingUserId: "",
			}

			resp, err := userEmoteLimitsAPI.GetUserEmoteLimits(ctx, req)
			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("with no user_id provided", func() {
			req := &mkd.GetUserEmoteLimitsRequest{
				RequestingUserId: userID,
				UserId:           "",
			}

			resp, err := userEmoteLimitsAPI.GetUserEmoteLimits(ctx, req)
			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("with requesting_user_id not equal to user_id and user service client error", func() {
			mockUserServiceClient.On("GetUserByID", mock.Anything, userID, mock.Anything).Return(nil, errors.New("dummy error"))

			req := &mkd.GetUserEmoteLimitsRequest{
				RequestingUserId: userID,
				UserId:           "someOtherUser",
			}

			resp, err := userEmoteLimitsAPI.GetUserEmoteLimits(ctx, req)
			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("with requesting_user_id not equal to user_id and requesting user admin is nil", func() {
			mockUserServiceClient.On("GetUserByID", mock.Anything, userID, mock.Anything).Return(&models.Properties{
				ID:    userID,
				Admin: nil,
			}, nil)

			req := &mkd.GetUserEmoteLimitsRequest{
				RequestingUserId: userID,
				UserId:           "someOtherUser",
			}

			resp, err := userEmoteLimitsAPI.GetUserEmoteLimits(ctx, req)
			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("with requesting_user_id not equal to user_id and requesting user is not admin", func() {
			mockUserServiceClient.On("GetUserByID", mock.Anything, userID, mock.Anything).Return(&models.Properties{
				ID:    userID,
				Admin: pointers.BoolP(false),
			}, nil)

			req := &mkd.GetUserEmoteLimitsRequest{
				RequestingUserId: userID,
				UserId:           "someOtherUser",
			}

			resp, err := userEmoteLimitsAPI.GetUserEmoteLimits(ctx, req)
			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("with error from emote limits manager", func() {
			mockEmoteLimitsManager.On("GetUserEmoteLimits", mock.Anything, userID).Return(emotelimits.UserEmoteLimits{}, errors.New("dummy error"))

			req := &mkd.GetUserEmoteLimitsRequest{
				RequestingUserId: userID,
				UserId:           userID,
			}

			resp, err := userEmoteLimitsAPI.GetUserEmoteLimits(ctx, req)
			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("with success from emote limits manager", func() {
			mockEmoteLimitsManager.On("GetUserEmoteLimits", mock.Anything, userID).Return(emotelimits.UserEmoteLimits{
				OwnedStaticEmoteLimit:   10,
				OwnedAnimatedEmoteLimit: 5,
			}, nil)

			req := &mkd.GetUserEmoteLimitsRequest{
				RequestingUserId: userID,
				UserId:           userID,
			}

			resp, err := userEmoteLimitsAPI.GetUserEmoteLimits(ctx, req)
			So(resp, ShouldResemble, &mkd.GetUserEmoteLimitsResponse{
				OwnedStaticEmoteLimit:   10,
				OwnedAnimatedEmoteLimit: 5,
			})
			So(err, ShouldBeNil)
		})
	})
}
