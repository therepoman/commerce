package api

import (
	"context"
	"testing"
	"time"

	"code.justin.tv/commerce/mako/clients"
	mako "code.justin.tv/commerce/mako/internal"
	"code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/internal_mock"
	mako_dashboard "code.justin.tv/commerce/mako/twirp/dashboard"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestGetEmotesByGroups(t *testing.T) {
	Convey("Test Emoticons By Group API", t, func() {
		mockEmoteDB := new(internal_mock.EmoteDB)
		mockEmoteGroupDB := new(internal_mock.EmoteGroupDB)

		getEmotesByGroupsAPI := GetEmotesByGroupsAPI{
			Clients: clients.Clients{
				EmoteDBUncached: mockEmoteDB,
				EmoteGroupDB:    mockEmoteGroupDB,
			},
		}

		Convey("Test GetEmoticonsByGroups API", func() {
			Convey("Given a group Id slice that contains empty strings, filter these out before calling emote db", func() {
				testGroupId := "500000001"
				testIdsWithEmptyStrings := []string{testGroupId, "", ""}
				mockEmoteDB.On("ReadByGroupIDs", mock.Anything, testGroupId).Return(nil, nil)
				mockEmoteGroupDB.On("GetEmoteGroups", mock.Anything, testGroupId).Return(nil, nil)

				request := &mako_dashboard.GetEmotesByGroupsRequest{EmoteGroupIds: testIdsWithEmptyStrings}
				_, err := getEmotesByGroupsAPI.GetEmotesByGroups(context.Background(), request)

				So(err, ShouldBeNil)
				mockEmoteDB.AssertCalled(t, "ReadByGroupIDs", mock.Anything, testGroupId)
				mockEmoteGroupDB.AssertCalled(t, "GetEmoteGroups", mock.Anything, testGroupId)
			})

			Convey("Given a group Id slice that contains duplicates, filter these out before calling emote db", func() {
				testGroupId := "500000001"
				testIdsWithDuplicates := []string{testGroupId, testGroupId}
				mockEmoteDB.On("ReadByGroupIDs", mock.Anything, testGroupId).Return(nil, nil)
				mockEmoteGroupDB.On("GetEmoteGroups", mock.Anything, testGroupId).Return(nil, nil)

				request := &mako_dashboard.GetEmotesByGroupsRequest{EmoteGroupIds: testIdsWithDuplicates}
				_, err := getEmotesByGroupsAPI.GetEmotesByGroups(context.Background(), request)

				So(err, ShouldBeNil)
				mockEmoteDB.AssertCalled(t, "ReadByGroupIDs", mock.Anything, testGroupId)
				mockEmoteGroupDB.AssertCalled(t, "GetEmoteGroups", mock.Anything, testGroupId)
			})

			Convey("Given emote db returns error", func() {
				groupID := "500000001"
				mockEmoteDB.On("ReadByGroupIDs", mock.Anything, mock.Anything).Return(nil, errors.New("dummy emotedb error"))
				mockEmoteGroupDB.On("GetEmoteGroups", mock.Anything, mock.Anything).Return(nil, nil)

				Convey("then return internal error", func() {
					request := &mako_dashboard.GetEmotesByGroupsRequest{EmoteGroupIds: []string{groupID}}
					resp, err := getEmotesByGroupsAPI.GetEmotesByGroups(context.Background(), request)

					So(resp, ShouldBeNil)
					So(err, ShouldNotBeNil)
					So(err.Error(), ShouldContainSubstring, "twirp error internal")

					mockEmoteGroupDB.AssertNotCalled(t, "GetEmoteGroups")
				})
			})

			Convey("Given emote group db returns error", func() {
				groupID := "500000001"
				mockEmoteDB.On("ReadByGroupIDs", mock.Anything, mock.Anything).Return(nil, nil)
				mockEmoteGroupDB.On("GetEmoteGroups", mock.Anything, mock.Anything).Return(nil, errors.New("dummy emotedb error"))

				Convey("then return internal error", func() {
					request := &mako_dashboard.GetEmotesByGroupsRequest{EmoteGroupIds: []string{groupID}}
					resp, err := getEmotesByGroupsAPI.GetEmotesByGroups(context.Background(), request)

					So(resp, ShouldBeNil)
					So(err, ShouldNotBeNil)
					So(err.Error(), ShouldContainSubstring, "twirp error internal")
				})
			})

			Convey("Given successful emote group fetch from emotedb with all active emotes", func() {
				queriedIds := []string{"500000001", "500000002"}
				testEmote1 := mako.Emote{
					ID:          "emotesv2_3e22a0ab6add4a4c9bbb43897d196cdc",
					OwnerID:     "267893713",
					GroupID:     queriedIds[0],
					Prefix:      "wolf",
					Suffix:      "HOWL",
					Code:        "wolfHOWL",
					State:       "active",
					Domain:      "emotes",
					EmotesGroup: "BitsBadgeTierEmotes",
					Order:       0,
					CreatedAt:   time.Date(2016, 1, 18, 12, 0, 0, 0, time.UTC),
				}
				testEmote2 := mako.Emote{
					ID:          "emotesv2_3e22a0ab6add4a4c9bbb43897d196cdd",
					OwnerID:     "267893713",
					GroupID:     queriedIds[1],
					Prefix:      "wolf",
					Suffix:      "CUB",
					Code:        "wolfCUB",
					State:       "active",
					Domain:      "emotes",
					EmotesGroup: "BitsBadgeTierEmotes",
					Order:       0,
					CreatedAt:   time.Date(2018, 3, 14, 1, 59, 26, 0, time.UTC),
				}
				mockEmoteDB.On("ReadByGroupIDs", mock.Anything, queriedIds[0], queriedIds[1]).Return([]mako.Emote{
					testEmote1,
					testEmote2,
				}, nil)

				testGroup1 := mako.EmoteGroup{
					GroupID:   queriedIds[0],
					CreatedAt: time.Now(),
					GroupType: mako.EmoteGroupAnimated,
				}

				testGroup2 := mako.EmoteGroup{
					GroupID:   queriedIds[1],
					CreatedAt: time.Now(),
					GroupType: mako.EmoteGroupStatic,
				}

				mockEmoteGroupDB.On("GetEmoteGroups", mock.Anything, queriedIds[0], queriedIds[1]).Return([]mako.EmoteGroup{
					testGroup1,
					testGroup2,
				}, nil)

				Convey("Then return those emote groups", func() {
					request := &mako_dashboard.GetEmotesByGroupsRequest{EmoteGroupIds: queriedIds}
					resp, err := getEmotesByGroupsAPI.GetEmotesByGroups(context.Background(), request)

					So(err, ShouldBeNil)
					So(resp, ShouldNotBeNil)
					So(len(resp.Groups), ShouldEqual, 2)

					actualGroup1 := resp.Groups[0]
					actualGroup2 := resp.Groups[1]
					if resp.Groups[0].Id != queriedIds[0] {
						actualGroup2 = resp.Groups[0]
						actualGroup1 = resp.Groups[1]
					}

					actualGroup1Emotes := actualGroup1.GetEmotes()
					actualGroup2Emotes := actualGroup2.GetEmotes()

					So(len(actualGroup1Emotes), ShouldEqual, 1)
					So(len(actualGroup2Emotes), ShouldEqual, 1)

					So(actualGroup1.GetId(), ShouldEqual, testGroup1.GroupID)
					So(actualGroup1.GetGroupType(), ShouldEqual, mako_dashboard.EmoteGroupType_animated_group)

					So(actualGroup1Emotes[0].GetId(), ShouldEqual, testEmote1.ID)
					So(actualGroup1Emotes[0].GetGroupId(), ShouldEqual, testEmote1.GroupID)
					So(actualGroup1Emotes[0].GetCode(), ShouldEqual, testEmote1.Code)
					So(actualGroup1Emotes[0].GetCreatedAt().Seconds, ShouldEqual, testEmote1.CreatedAt.Unix())

					So(actualGroup2.GetId(), ShouldEqual, testGroup2.GroupID)
					So(actualGroup2.GetGroupType(), ShouldEqual, mako_dashboard.EmoteGroupType_static_group)

					So(actualGroup2Emotes[0].GetId(), ShouldEqual, testEmote2.ID)
					So(actualGroup2Emotes[0].GetGroupId(), ShouldEqual, testEmote2.GroupID)
					So(actualGroup2Emotes[0].GetCode(), ShouldEqual, testEmote2.Code)
					So(actualGroup2Emotes[0].GetCreatedAt().Seconds, ShouldEqual, testEmote2.CreatedAt.Unix())
				})
			})

			Convey("Given successful emote group fetch from emotedb with a mix of active and non-active emotes", func() {
				queriedIds := []string{"500000001", "500000002", "500000003"}
				testEmote1 := mako.Emote{
					ID:          "emotesv2_3e22a0ab6add4a4c9bbb43897d196cdd",
					OwnerID:     "267893713",
					GroupID:     queriedIds[1],
					Prefix:      "wolf",
					Suffix:      "CUB",
					Code:        "wolfCUB",
					State:       "inactive",
					Domain:      "emotes",
					EmotesGroup: "BitsBadgeTierEmotes",
					Order:       0,
				}
				testEmote4 := mako.Emote{
					ID:          "emotesv2_3e22a0ab6add4a4c9bbb43897d196cdd",
					OwnerID:     "267893713",
					GroupID:     queriedIds[1],
					Prefix:      "wolf",
					Suffix:      "CUB",
					Code:        "wolfCUB",
					State:       "active",
					Domain:      "emotes",
					EmotesGroup: "BitsBadgeTierEmotes",
					Order:       0,
				}
				testEmote2 := mako.Emote{
					ID:          "emotesv2_3e22a0ab6add4a4c9bbb43897d196eee",
					OwnerID:     "267893713",
					GroupID:     queriedIds[0],
					Prefix:      "wolf",
					Suffix:      "PACK",
					Code:        "wolfPACK",
					State:       "active",
					Domain:      "emotes",
					EmotesGroup: "BitsBadgeTierEmotes",
					Order:       1,
				}
				testEmote3 := mako.Emote{
					ID:          "88",
					OwnerID:     "0",
					GroupID:     queriedIds[0],
					Prefix:      "",
					Suffix:      "PogChamp",
					Code:        "PogChamp",
					State:       "active",
					Domain:      "emotes",
					EmotesGroup: "Global",
					Order:       0,
				}
				mockEmoteDB.On("ReadByGroupIDs", mock.Anything, queriedIds[0], queriedIds[1], queriedIds[2]).Return([]mako.Emote{
					testEmote1,
					testEmote4,
					testEmote2,
					testEmote3,
				}, nil)

				testGroup1 := mako.EmoteGroup{
					GroupID:   queriedIds[0],
					CreatedAt: time.Now(),
					GroupType: mako.EmoteGroupAnimated,
				}

				testGroup2 := mako.EmoteGroup{
					GroupID:   queriedIds[1],
					CreatedAt: time.Now(),
					GroupType: mako.EmoteGroupStatic,
				}

				mockEmoteGroupDB.On("GetEmoteGroups", mock.Anything, queriedIds[0], queriedIds[1], queriedIds[2]).Return([]mako.EmoteGroup{
					testGroup1,
					testGroup2,
				}, nil)

				Convey("Given a request with no filter, then return only active emotes, sorted by order ascending", func() {
					request := &mako_dashboard.GetEmotesByGroupsRequest{EmoteGroupIds: queriedIds}
					resp, err := getEmotesByGroupsAPI.GetEmotesByGroups(context.Background(), request)

					So(err, ShouldBeNil)
					So(resp, ShouldNotBeNil)
					So(len(resp.Groups), ShouldEqual, 3)

					var animatedGroup *mako_dashboard.EmoteGroup
					var staticGroup *mako_dashboard.EmoteGroup
					var emptyStaticGroup *mako_dashboard.EmoteGroup

					for _, group := range resp.Groups {
						if group.GroupType == mako_dashboard.EmoteGroupType_animated_group {
							animatedGroup = group
						} else if group.GroupType == mako_dashboard.EmoteGroupType_static_group && len(group.Emotes) > 0 {
							staticGroup = group
						} else {
							emptyStaticGroup = group
						}
					}

					So(animatedGroup, ShouldNotBeNil)
					So(staticGroup, ShouldNotBeNil)
					So(emptyStaticGroup, ShouldNotBeNil)

					actualGroup := animatedGroup.GetEmotes()
					So(len(actualGroup), ShouldEqual, 2)

					So(actualGroup[1].GetId(), ShouldEqual, testEmote2.ID)
					So(actualGroup[1].GetGroupId(), ShouldEqual, testEmote2.GroupID)
					So(actualGroup[1].GetCode(), ShouldEqual, testEmote2.Code)

					So(actualGroup[0].GetId(), ShouldEqual, testEmote3.ID)
					So(actualGroup[0].GetGroupId(), ShouldEqual, testEmote3.GroupID)
					So(actualGroup[0].GetCode(), ShouldEqual, testEmote3.Code)
				})
			})
		})
	})
}
