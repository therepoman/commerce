package api

import (
	"context"
	"errors"
	"testing"
	"time"

	"code.justin.tv/commerce/mako/clients"
	mako "code.justin.tv/commerce/mako/internal"
	"code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/internal_mock"
	mk "code.justin.tv/commerce/mako/twirp"
	"github.com/segmentio/ksuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
)

func makeEmoteModifierGroupsAPI(db mako.EmoteModifiersDB) EmoteModifierGroupsAPIs {
	return EmoteModifierGroupsAPIs{
		Clients: clients.Clients{
			EmoteModifiersDB: db,
		},
	}
}

func makeEmoteModifierGroup() *mk.EmoteModifierGroup {
	return &mk.EmoteModifierGroup{
		Id:                  ksuid.New().String(),
		OwnerId:             ksuid.New().String(),
		Modifiers:           []mk.Modification{},
		SourceEmoteGroupIds: []string{ksuid.New().String()},
	}
}

func makeEmoteModifierGroupFn(f func(e *mk.EmoteModifierGroup)) *mk.EmoteModifierGroup {
	emg := makeEmoteModifierGroup()
	f(emg)
	return emg
}

type emoteModGroupTest struct {
	scenario string
	fn       func(ctx context.Context, t *testing.T)
}

func runEmoteModGroupTests(t *testing.T, tests ...emoteModGroupTest) {
	for _, test := range tests {
		test := test
		t.Run(test.scenario, func(t *testing.T) {
			t.Parallel()
			ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
			defer cancel()
			test.fn(ctx, t)
		})
	}
}

func TestEmoteModifierGroupsAPIs_CreateEmoteModifierGroups(t *testing.T) {
	tests := []emoteModGroupTest{
		{
			scenario: "GIVEN 0 groups WHEN create emote mod groups THEN return error",
			fn: func(ctx context.Context, t *testing.T) {
				// GIVEN: request with 0 groups
				req := &mk.CreateEmoteModifierGroupsRequest{}
				mockDB := new(internal_mock.EmoteModifiersDB)
				api := makeEmoteModifierGroupsAPI(mockDB)

				// WHEN
				resp, err := api.CreateEmoteModifierGroups(ctx, req)

				// THEN: empty response returned.
				require.Error(t, err)
				assert.Contains(t, err.Error(), "groups is required")
				assert.Nil(t, resp)
			},
		},
		{
			scenario: "GIVEN group with ID already set WHEN create emote mod groups THEN return error",
			fn: func(ctx context.Context, t *testing.T) {
				// GIVEN:
				req := &mk.CreateEmoteModifierGroupsRequest{
					Groups: []*mk.EmoteModifierGroup{
						makeEmoteModifierGroupFn(func(e *mk.EmoteModifierGroup) {
							e.Modifiers = []mk.Modification{mk.Modification_HorizontalFlip}
						}),
					},
				}
				mockDB := new(internal_mock.EmoteModifiersDB)
				api := makeEmoteModifierGroupsAPI(mockDB)

				// WHEN
				resp, err := api.CreateEmoteModifierGroups(ctx, req)

				// THEN:
				require.Error(t, err)
				assert.Contains(t, err.Error(), "id must not be set")
				assert.Nil(t, resp)
			},
		},
		{
			scenario: "GIVEN group with no OwnerID WHEN create emote mod groups THEN return error",
			fn: func(ctx context.Context, t *testing.T) {
				// GIVEN:
				req := &mk.CreateEmoteModifierGroupsRequest{
					Groups: []*mk.EmoteModifierGroup{
						makeEmoteModifierGroupFn(func(e *mk.EmoteModifierGroup) {
							e.Id = ""
							e.OwnerId = ""
							e.Modifiers = []mk.Modification{mk.Modification_Thinking}
						}),
					},
				}
				mockDB := new(internal_mock.EmoteModifiersDB)
				api := makeEmoteModifierGroupsAPI(mockDB)

				// WHEN
				resp, err := api.CreateEmoteModifierGroups(ctx, req)

				// THEN:
				require.Error(t, err)
				assert.Contains(t, err.Error(), "owner_id is required")
				assert.Nil(t, resp)
			},
		},
		{
			scenario: "GIVEN some other error WHEN create emote mod groups THEN return error",
			fn: func(ctx context.Context, t *testing.T) {
				// GIVEN:
				req := &mk.CreateEmoteModifierGroupsRequest{
					Groups: []*mk.EmoteModifierGroup{
						makeEmoteModifierGroupFn(func(e *mk.EmoteModifierGroup) {
							e.Id = ""
							e.Modifiers = []mk.Modification{mk.Modification_Thinking}
						}),
					},
				}
				mockDB := new(internal_mock.EmoteModifiersDB)
				mockDB.On("CreateGroups", mock.Anything, mock.Anything).Return(nil, errors.New("mock error"))
				api := makeEmoteModifierGroupsAPI(mockDB)

				// WHEN
				resp, err := api.CreateEmoteModifierGroups(ctx, req)

				// THEN:
				require.Error(t, err)
				assert.Contains(t, err.Error(), "failed to create emote modifier groups")
				assert.Nil(t, resp)
			},
		},
		{
			scenario: "GIVEN success WHEN create emote mod groups THEN return newly created groups",
			fn: func(ctx context.Context, t *testing.T) {
				// GIVEN:
				req := &mk.CreateEmoteModifierGroupsRequest{
					Groups: []*mk.EmoteModifierGroup{
						makeEmoteModifierGroupFn(func(e *mk.EmoteModifierGroup) {
							e.Id = ""
							e.Modifiers = []mk.Modification{mk.Modification_Thinking}
						}),
					},
				}
				mockDB := new(internal_mock.EmoteModifiersDB)
				mockDB.On("CreateGroups", mock.Anything, mock.Anything).Return([]mako.EmoteModifierGroup{
					{
						ID: ksuid.New().String(),
					},
				}, nil)
				api := makeEmoteModifierGroupsAPI(mockDB)

				// WHEN
				resp, err := api.CreateEmoteModifierGroups(ctx, req)

				// THEN:
				assert.NotEmpty(t, resp)
				assert.NoError(t, err)
			},
		},
	}

	runEmoteModGroupTests(t, tests...)
}

func TestEmoteModifierGroupsAPIs_UpdateEmoteModifierGroups(t *testing.T) {
	tests := []emoteModGroupTest{
		{
			scenario: "GIVEN 0 groups WHEN update emote mod groups THEN return error",
			fn: func(ctx context.Context, t *testing.T) {
				// GIVEN: request with 0 groups
				req := &mk.UpdateEmoteModifierGroupsRequest{}
				mockDB := new(internal_mock.EmoteModifiersDB)
				api := makeEmoteModifierGroupsAPI(mockDB)

				// WHEN
				resp, err := api.UpdateEmoteModifierGroups(ctx, req)

				// THEN: empty response returned.
				require.Error(t, err)
				assert.Contains(t, err.Error(), "groups is required")
				assert.Nil(t, resp)
			},
		},
		{
			scenario: "GIVEN group without ID specified WHEN update emote mod groups THEN return error",
			fn: func(ctx context.Context, t *testing.T) {
				// GIVEN: request with 0 groups
				req := &mk.UpdateEmoteModifierGroupsRequest{
					Groups: []*mk.EmoteModifierGroup{
						makeEmoteModifierGroupFn(func(e *mk.EmoteModifierGroup) {
							e.Id = ""
							e.Modifiers = []mk.Modification{mk.Modification_HorizontalFlip}
						}),
					},
				}
				mockDB := new(internal_mock.EmoteModifiersDB)
				api := makeEmoteModifierGroupsAPI(mockDB)

				// WHEN
				resp, err := api.UpdateEmoteModifierGroups(ctx, req)

				// THEN: empty response returned.
				require.Error(t, err)
				assert.Contains(t, err.Error(), "id is required")
				assert.Nil(t, resp)
			},
		},
		{
			scenario: "GIVEN non-permanent emote modifiers WHEN update emote mod groups THEN return error",
			fn: func(ctx context.Context, t *testing.T) {
				// GIVEN: non-permanent emote modifiers
				req := &mk.UpdateEmoteModifierGroupsRequest{
					Groups: []*mk.EmoteModifierGroup{
						makeEmoteModifierGroupFn(func(e *mk.EmoteModifierGroup) {
							e.Modifiers = []mk.Modification{mk.Modification_Reindeer}
						}),
					},
				}
				mockDB := new(internal_mock.EmoteModifiersDB)
				api := makeEmoteModifierGroupsAPI(mockDB)

				// WHEN
				resp, err := api.UpdateEmoteModifierGroups(ctx, req)

				// THEN: empty response returned.
				require.Error(t, err)
				assert.Contains(t, err.Error(), "must be permanent modifiers")
				assert.Nil(t, resp)
			},
		},
		{
			scenario: "GIVEN some other error WHEN update emote mod groups THEN return error",
			fn: func(ctx context.Context, t *testing.T) {
				// GIVEN: request with 0 groups
				req := &mk.UpdateEmoteModifierGroupsRequest{
					Groups: []*mk.EmoteModifierGroup{
						makeEmoteModifierGroupFn(func(e *mk.EmoteModifierGroup) {
							e.Modifiers = []mk.Modification{mk.Modification_HorizontalFlip}
						}),
					},
				}
				mockDB := new(internal_mock.EmoteModifiersDB)
				mockDB.On("UpdateGroups", mock.Anything, mock.Anything).Return(nil, errors.New("mock error"))
				api := makeEmoteModifierGroupsAPI(mockDB)

				// WHEN
				resp, err := api.UpdateEmoteModifierGroups(ctx, req)

				// THEN: empty response returned.
				require.Error(t, err)
				assert.Contains(t, err.Error(), "failed to update emote modifier groups")
				assert.Nil(t, resp)
			},
		},
		{
			scenario: "GIVEN success WHEN update emote mod groups THEN return updated emote mod groups",
			fn: func(ctx context.Context, t *testing.T) {
				// GIVEN: request with 0 groups
				req := &mk.UpdateEmoteModifierGroupsRequest{
					Groups: []*mk.EmoteModifierGroup{
						makeEmoteModifierGroupFn(func(e *mk.EmoteModifierGroup) {
							e.Modifiers = []mk.Modification{mk.Modification_HorizontalFlip}
						}),
					},
				}
				mockDB := new(internal_mock.EmoteModifiersDB)
				mockDB.On("UpdateGroups", mock.Anything, mock.Anything).Return([]mako.EmoteModifierGroup{
					{
						ID: ksuid.New().String(),
					},
				}, nil)
				api := makeEmoteModifierGroupsAPI(mockDB)

				// WHEN
				resp, err := api.UpdateEmoteModifierGroups(ctx, req)

				// THEN:
				assert.NoError(t, err)
				assert.NotEmpty(t, resp)
			},
		},
	}

	runEmoteModGroupTests(t, tests...)
}

func TestEmoteModifierGroupsAPIs_GetEmoteModifierGroups(t *testing.T) {
	tests := []emoteModGroupTest{
		{
			scenario: "GIVEN 0 ids in list WHEN get emote mod groups THEN return error",
			fn: func(ctx context.Context, t *testing.T) {
				// GIVEN: request with 0 groups
				req := &mk.GetEmoteModifierGroupsRequest{}
				mockDB := new(internal_mock.EmoteModifiersDB)
				api := makeEmoteModifierGroupsAPI(mockDB)

				// WHEN
				resp, err := api.GetEmoteModifierGroups(ctx, req)

				// THEN: error response returned.
				assert.Empty(t, resp)
				require.Error(t, err)
				assert.Contains(t, err.Error(), "modifier_group_ids is required")
			},
		},
		{
			scenario: "GIVEN error WHEN get emote mod groups THEN return error",
			fn: func(ctx context.Context, t *testing.T) {
				// GIVEN: request with 0 groups
				req := &mk.GetEmoteModifierGroupsRequest{
					ModifierGroupIds: []string{
						ksuid.New().String(),
					},
				}
				mockDB := new(internal_mock.EmoteModifiersDB)
				mockDB.On("ReadByIDs", mock.Anything, mock.Anything).Return(nil, errors.New("mock error"))
				api := makeEmoteModifierGroupsAPI(mockDB)

				// WHEN
				resp, err := api.GetEmoteModifierGroups(ctx, req)

				// THEN
				assert.Empty(t, resp)
				assert.Error(t, err)
				assert.Contains(t, err.Error(), "failed to get emote modifier groups by id")
			},
		},
		{
			scenario: "GIVEN 0 results WHEN get emote mod groups THEN return no error",
			fn: func(ctx context.Context, t *testing.T) {
				// GIVEN: 0 results
				req := &mk.GetEmoteModifierGroupsRequest{
					ModifierGroupIds: []string{
						ksuid.New().String(),
					},
				}
				mockDB := new(internal_mock.EmoteModifiersDB)
				mockDB.On("ReadByIDs", mock.Anything, mock.Anything).Return([]mako.EmoteModifierGroup{}, nil)
				api := makeEmoteModifierGroupsAPI(mockDB)

				// WHEN
				resp, err := api.GetEmoteModifierGroups(ctx, req)

				// THEN
				assert.Empty(t, resp.Groups)
				assert.NoError(t, err)
			},
		},
		{
			scenario: "GIVEN success WHEN get emote mod groups THEN return the groups",
			fn: func(ctx context.Context, t *testing.T) {
				// GIVEN: request with 0 groups
				req := &mk.GetEmoteModifierGroupsRequest{
					ModifierGroupIds: []string{
						ksuid.New().String(),
					},
				}
				mockDB := new(internal_mock.EmoteModifiersDB)
				mockDB.On("ReadByIDs", mock.Anything, mock.Anything).Return([]mako.EmoteModifierGroup{
					{
						ID: ksuid.New().String(),
					},
				}, nil)
				api := makeEmoteModifierGroupsAPI(mockDB)

				// WHEN
				resp, err := api.GetEmoteModifierGroups(ctx, req)

				// THEN
				assert.NotEmpty(t, resp)
				assert.NoError(t, err)
			},
		},
	}

	runEmoteModGroupTests(t, tests...)
}

func TestEmoteModifierGroupsAPIs_GetEmoteModifierGroupsByOwnerId(t *testing.T) {
	tests := []emoteModGroupTest{
		{
			scenario: "GIVEN 0 ids in list WHEN get emote mod groups THEN return error",
			fn: func(ctx context.Context, t *testing.T) {
				// GIVEN: request with 0 groups
				req := &mk.GetEmoteModifierGroupsByOwnerIdRequest{}
				mockDB := new(internal_mock.EmoteModifiersDB)
				api := makeEmoteModifierGroupsAPI(mockDB)

				// WHEN
				resp, err := api.GetEmoteModifierGroupsByOwnerId(ctx, req)

				// THEN: error response returned.
				assert.Empty(t, resp)
				require.Error(t, err)
				assert.Contains(t, err.Error(), "owner_id is required")
			},
		},
		{
			scenario: "GIVEN error WHEN get emote mod groups THEN return error",
			fn: func(ctx context.Context, t *testing.T) {
				// GIVEN: request with 0 groups
				req := &mk.GetEmoteModifierGroupsByOwnerIdRequest{
					OwnerId: ksuid.New().String(),
				}
				mockDB := new(internal_mock.EmoteModifiersDB)
				mockDB.On("ReadByOwnerIDs", mock.Anything, mock.Anything).Return(nil, errors.New("mock error"))
				api := makeEmoteModifierGroupsAPI(mockDB)

				// WHEN
				resp, err := api.GetEmoteModifierGroupsByOwnerId(ctx, req)

				// THEN
				assert.Empty(t, resp)
				assert.Error(t, err)
				assert.Contains(t, err.Error(), "failed to get emote modifier groups by owner id")
			},
		},
		{
			scenario: "GIVEN 0 results WHEN get emote mod groups THEN don't return an error",
			fn: func(ctx context.Context, t *testing.T) {
				// GIVEN: 0 results
				req := &mk.GetEmoteModifierGroupsByOwnerIdRequest{
					OwnerId: ksuid.New().String(),
				}
				mockDB := new(internal_mock.EmoteModifiersDB)
				mockDB.On("ReadByOwnerIDs", mock.Anything, mock.Anything).Return([]mako.EmoteModifierGroup{}, nil)
				api := makeEmoteModifierGroupsAPI(mockDB)

				// WHEN
				resp, err := api.GetEmoteModifierGroupsByOwnerId(ctx, req)

				// THEN
				assert.Empty(t, resp.Groups)
				assert.NoError(t, err)
			},
		},
		{
			scenario: "GIVEN success WHEN get emote mod groups THEN return the groups",
			fn: func(ctx context.Context, t *testing.T) {
				// GIVEN: request with 0 groups
				req := &mk.GetEmoteModifierGroupsByOwnerIdRequest{
					OwnerId: ksuid.New().String(),
				}
				mockDB := new(internal_mock.EmoteModifiersDB)
				mockDB.On("ReadByOwnerIDs", mock.Anything, mock.Anything).Return([]mako.EmoteModifierGroup{
					{
						ID: ksuid.New().String(),
					},
				}, nil)
				api := makeEmoteModifierGroupsAPI(mockDB)

				// WHEN
				resp, err := api.GetEmoteModifierGroupsByOwnerId(ctx, req)

				// THEN
				assert.NotEmpty(t, resp)
				assert.NoError(t, err)
			},
		},
	}

	runEmoteModGroupTests(t, tests...)
}
func TestEmoteModifierGroupsAPIs_DeleteEmoteModifierGroups(t *testing.T) {
	tests := []emoteModGroupTest{
		{
			scenario: "GIVEN 0 ids in list WHEN delete emote mod groups THEN return error",
			fn: func(ctx context.Context, t *testing.T) {
				// GIVEN: request with 0 groups
				req := &mk.DeleteEmoteModifierGroupsRequest{
					ModifierGroupIds: []string{},
				}
				mockDB := new(internal_mock.EmoteModifiersDB)
				api := makeEmoteModifierGroupsAPI(mockDB)

				// WHEN
				resp, err := api.DeleteEmoteModifierGroups(ctx, req)

				// THEN: error response returned.
				assert.Empty(t, resp)
				require.Error(t, err)
				assert.Contains(t, err.Error(), "modifier_group_ids is required")
			},
		},
		{
			scenario: "GIVEN error WHEN get emote mod groups THEN return error",
			fn: func(ctx context.Context, t *testing.T) {
				// GIVEN
				req := &mk.DeleteEmoteModifierGroupsRequest{
					ModifierGroupIds: []string{
						ksuid.New().String(),
					},
				}

				mockDB := new(internal_mock.EmoteModifiersDB)
				mockDB.On("DeleteByIDs", mock.Anything, mock.Anything).Return(errors.New("mock error"))
				api := makeEmoteModifierGroupsAPI(mockDB)

				// WHEN
				resp, err := api.DeleteEmoteModifierGroups(ctx, req)

				// THEN
				assert.Empty(t, resp)
				assert.Error(t, err)
				assert.Contains(t, err.Error(), "failed to delete emote modifier groups")
			},
		},
		{
			scenario: "GIVEN succuess WHEN get emote mod groups THEN return success",
			fn: func(ctx context.Context, t *testing.T) {
				// GIVEN
				req := &mk.DeleteEmoteModifierGroupsRequest{
					ModifierGroupIds: []string{
						ksuid.New().String(),
					},
				}
				mockDB := new(internal_mock.EmoteModifiersDB)
				mockDB.On("DeleteByIDs", mock.Anything, mock.Anything).Return(nil)
				api := makeEmoteModifierGroupsAPI(mockDB)

				// WHEN
				resp, err := api.DeleteEmoteModifierGroups(ctx, req)

				// THEN
				assert.Empty(t, resp)
				assert.NoError(t, err)
			},
		},
	}

	runEmoteModGroupTests(t, tests...)
}
