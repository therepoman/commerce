package api

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/mako/clients"
	"code.justin.tv/commerce/mako/clients/emoticonsmanager"
	emoticonsmanager_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/emoticonsmanager"
	mk "code.justin.tv/commerce/mako/twirp"

	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
	"github.com/twitchtv/twirp"
)

func TestDeleteEmoticonAPI(t *testing.T) {
	Convey("Test Delete Emoticon API", t, func() {
		mockEmoticonsManager := new(emoticonsmanager_mock.IEmoticonsManager)
		deleteEmoticonAPI := &DeleteEmoticonAPI{
			Clients: clients.Clients{
				EmoticonsManager: mockEmoticonsManager,
			},
		}

		Convey("On Success", func() {
			req := &mk.DeleteEmoticonRequest{
				Id: "123",
			}
			mockEmoticonsManager.On("Delete", mock.Anything, mock.Anything, mock.Anything).Return(nil)

			_, err := deleteEmoticonAPI.DeleteEmoticon(context.Background(), req)
			So(err, ShouldBeNil)
			mockEmoticonsManager.AssertExpectations(t)
		})

		Convey("validation on ID error", func() {
			req := &mk.DeleteEmoticonRequest{
				Id: "",
			}
			_, err := deleteEmoticonAPI.DeleteEmoticon(context.Background(), req)
			So(err, ShouldNotBeNil)
			twerr, ok := err.(twirp.Error)
			So(ok, ShouldBeTrue)
			So(twerr.Code(), ShouldEqual, twirp.InvalidArgument)
			mockEmoticonsManager.AssertExpectations(t)
		})

		Convey("when emoticon manager returns user permission error", func() {
			req := &mk.DeleteEmoticonRequest{
				Id: "123",
			}
			mockEmoticonsManager.On("Delete", mock.Anything, mock.Anything, mock.Anything).Return(emoticonsmanager.UserNotPermitted)

			_, err := deleteEmoticonAPI.DeleteEmoticon(context.Background(), req)
			So(err, ShouldNotBeNil)
			twerr, ok := err.(twirp.Error)
			So(ok, ShouldBeTrue)
			So(twerr.Code(), ShouldEqual, twirp.PermissionDenied)
			mockEmoticonsManager.AssertExpectations(t)
		})

		Convey("when emoticon manager returns emote does not exist", func() {
			req := &mk.DeleteEmoticonRequest{
				Id: "123",
			}
			mockEmoticonsManager.On("Delete", mock.Anything, mock.Anything, mock.Anything).Return(emoticonsmanager.EmoteDoesNotExist)

			_, err := deleteEmoticonAPI.DeleteEmoticon(context.Background(), req)
			So(err, ShouldNotBeNil)
			twerr, ok := err.(twirp.Error)
			So(ok, ShouldBeTrue)
			So(twerr.Code(), ShouldEqual, twirp.NotFound)
			mockEmoticonsManager.AssertExpectations(t)
		})

		Convey("when emoticon manager returns internal error", func() {
			req := &mk.DeleteEmoticonRequest{
				Id: "123",
			}
			mockEmoticonsManager.On("Delete", mock.Anything, mock.Anything, mock.Anything).Return(errors.New("test error"))

			_, err := deleteEmoticonAPI.DeleteEmoticon(context.Background(), req)
			So(err, ShouldNotBeNil)
			twerr, ok := err.(twirp.Error)
			So(ok, ShouldBeTrue)
			So(twerr.Code(), ShouldEqual, twirp.Internal)
			mockEmoticonsManager.AssertExpectations(t)
		})
	})
}
