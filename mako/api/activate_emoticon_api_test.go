package api

import (
	"context"
	"testing"

	"code.justin.tv/commerce/mako/clients"
	emoticonsmanager_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/emoticonsmanager"
	mk "code.justin.tv/commerce/mako/twirp"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestAactivateEmoticonAPI(t *testing.T) {
	Convey("Test ActivateEmoticonAPI", t, func() {
		mockEmoticon := new(emoticonsmanager_mock.IEmoticonsManager)
		activateEmoticonAPI := &ActivateEmoticonAPI{
			Clients: clients.Clients{
				EmoticonsManager: mockEmoticon,
			},
		}

		Convey("Test ActivateEmoticon", func() {
			Convey("With empty id", func() {
				req := &mk.ActivateEmoticonRequest{
					Id: "",
				}

				_, err := activateEmoticonAPI.ActivateEmoticon(context.Background(), req)
				So(err, ShouldNotBeNil)
			})

			Convey("With error on emoticon deactivate", func() {
				req := &mk.ActivateEmoticonRequest{
					Id: "1234",
				}
				mockEmoticon.On("Activate", mock.Anything, mock.Anything).Return(errors.New("test"))

				_, err := activateEmoticonAPI.ActivateEmoticon(context.Background(), req)
				So(err, ShouldNotBeNil)
			})
			Convey("With Success", func() {
				req := &mk.ActivateEmoticonRequest{
					Id: "1234",
				}
				mockEmoticon.On("Activate", mock.Anything, mock.Anything).Return(nil)

				_, err := activateEmoticonAPI.ActivateEmoticon(context.Background(), req)
				So(err, ShouldBeNil)
			})
		})
	})
}
