package api

import (
	"context"
	"errors"
	"testing"
	"time"

	config_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/config"

	"code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/internal_mock"

	"code.justin.tv/commerce/mako/clients"
	"code.justin.tv/commerce/mako/clients/emoticonsmanager"
	mako "code.justin.tv/commerce/mako/internal"
	emoticonsmanager_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/emoticonsmanager"
	paint_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/paint"
	stats_mock "code.justin.tv/commerce/mako/mocks/github.com/cactus/go-statsd-client/statsd"
	mkd "code.justin.tv/commerce/mako/twirp/dashboard"
	"code.justin.tv/subs/paint/paintrpc"
	"github.com/golang/protobuf/proto"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func getCreateEmoteRequest() *mkd.CreateEmoteRequest {
	return &mkd.CreateEmoteRequest{
		UserId:     "123",
		Code:       "mothebHey",
		CodeSuffix: "Hey",
		State:      mkd.EmoteState_unknown,
		GroupIds:   []string{"567567"},
		ImageAssets: []*mkd.EmoteImageAsset{{
			ImageIds: &mkd.EmoteImageIds{
				Image1XId: "9d928ab5-1482-4be8-90cb-11357ab3c5c2",
				Image2XId: "9d928ab5-1482-4be8-90cb-11357ab3c5c3",
				Image4XId: "9d928ab5-1482-4be8-90cb-11357ab3c5c4",
			},
			AssetType: mkd.EmoteAssetType_static,
		}},
		AssetType:                 mkd.EmoteAssetType_static,
		Origin:                    mkd.EmoteOrigin_Subscriptions,
		EnforceModerationWorkflow: true,
	}
}

func getCreateFollowerEmoteRequest() *mkd.CreateFollowerEmoteRequest {
	return &mkd.CreateFollowerEmoteRequest{
		UserId:     "123",
		CodeSuffix: "Hey",
		State:      mkd.EmoteState_unknown,
		ImageAssets: []*mkd.EmoteImageAsset{{
			ImageIds: &mkd.EmoteImageIds{
				Image1XId: "9d928ab5-1482-4be8-90cb-11357ab3c5c2",
				Image2XId: "9d928ab5-1482-4be8-90cb-11357ab3c5c3",
				Image4XId: "9d928ab5-1482-4be8-90cb-11357ab3c5c4",
			},
			AssetType: mkd.EmoteAssetType_static,
		}},
		AssetType:                 mkd.EmoteAssetType_static,
		EnforceModerationWorkflow: true,
	}
}

func TestCreateEmoteAPI(t *testing.T) {
	Convey("Test CreateEmoteAPI", t, func() {
		mockEmoticon := new(emoticonsmanager_mock.IEmoticonsManager)
		mockPaint := new(paint_mock.PaintClient)
		mockStats := new(stats_mock.Statter)
		mockDynamicConfig := new(config_mock.DynamicConfigClient)
		mockEmoteGroupDB := new(internal_mock.EmoteGroupDB)
		createEmoteAPI := &CreateEmoteAPI{
			Clients: clients.Clients{
				EmoticonsManager: mockEmoticon,
				PaintClient:      mockPaint,
				EmoteGroupDB:     mockEmoteGroupDB,
			},
			DynamicConfig: mockDynamicConfig,
			Stats:         mockStats,
		}

		mockStats.On("TimingDuration", mock.Anything, mock.Anything, mock.Anything).Return(nil)

		Convey("Test CreateEmote", func() {
			Convey("With empty user_id", func() {
				req := &mkd.CreateEmoteRequest{
					UserId: "",
				}
				mockPaint.On("ModifyEmote", mock.Anything, mock.Anything).Return(mock.Anything, nil)

				_, err := createEmoteAPI.CreateEmote(context.Background(), req)
				So(err, ShouldNotBeNil)
			})

			Convey("With empty code_suffix", func() {
				req := getCreateEmoteRequest()
				req.CodeSuffix = ""

				_, err := createEmoteAPI.CreateEmote(context.Background(), req)
				So(err, ShouldNotBeNil)
			})

			Convey("With empty group_ids", func() {
				req := getCreateEmoteRequest()
				req.GroupIds = []string{}

				_, err := createEmoteAPI.CreateEmote(context.Background(), req)
				So(err, ShouldNotBeNil)
			})

			Convey("With group_ids that have an empty groupID string", func() {
				req := getCreateEmoteRequest()
				req.GroupIds = []string{""}

				_, err := createEmoteAPI.CreateEmote(context.Background(), req)
				So(err, ShouldNotBeNil)
			})

			Convey("With non-empty group_ids and Archive origin", func() {
				req := getCreateEmoteRequest()
				req.Origin = mkd.EmoteOrigin_Archive

				_, err := createEmoteAPI.CreateEmote(context.Background(), req)
				So(err, ShouldNotBeNil)
			})

			Convey("With empty image_assets", func() {
				req := getCreateEmoteRequest()
				req.ImageAssets = []*mkd.EmoteImageAsset{}

				_, err := createEmoteAPI.CreateEmote(context.Background(), req)
				So(err, ShouldNotBeNil)
			})

			Convey("With too many image_assets for static", func() {
				req := getCreateEmoteRequest()
				req.ImageAssets = append(req.ImageAssets, &mkd.EmoteImageAsset{
					ImageIds: &mkd.EmoteImageIds{
						Image1XId: "9d928ab5-1482-4be8-90cb-11357ab3c5c2",
						Image2XId: "9d928ab5-1482-4be8-90cb-11357ab3c5c3",
						Image4XId: "9d928ab5-1482-4be8-90cb-11357ab3c5c4",
					},
					AssetType: mkd.EmoteAssetType_static,
				})
				req.AssetType = mkd.EmoteAssetType_static

				_, err := createEmoteAPI.CreateEmote(context.Background(), req)
				So(err, ShouldNotBeNil)
			})

			Convey("With too few image_assets for animated", func() {
				req := getCreateEmoteRequest()
				req.AssetType = mkd.EmoteAssetType_animated

				_, err := createEmoteAPI.CreateEmote(context.Background(), req)
				So(err, ShouldNotBeNil)
			})

			Convey("With too many image_assets for animated", func() {
				req := getCreateEmoteRequest()
				req.ImageAssets = append(req.ImageAssets, &mkd.EmoteImageAsset{
					ImageIds: &mkd.EmoteImageIds{
						Image1XId: "9d928ab5-1482-4be8-90cb-11357ab3c5c2",
						Image2XId: "9d928ab5-1482-4be8-90cb-11357ab3c5c3",
						Image4XId: "9d928ab5-1482-4be8-90cb-11357ab3c5c4",
					},
					AssetType: mkd.EmoteAssetType_static,
				}, &mkd.EmoteImageAsset{
					ImageIds: &mkd.EmoteImageIds{
						Image1XId: "9d928ab5-1482-4be8-90cb-11357ab3c5c2",
						Image2XId: "9d928ab5-1482-4be8-90cb-11357ab3c5c3",
						Image4XId: "9d928ab5-1482-4be8-90cb-11357ab3c5c4",
					},
					AssetType: mkd.EmoteAssetType_animated,
				})
				req.AssetType = mkd.EmoteAssetType_static

				_, err := createEmoteAPI.CreateEmote(context.Background(), req)
				So(err, ShouldNotBeNil)
			})

			Convey("With no static asset for a static emote", func() {
				req := getCreateEmoteRequest()
				req.ImageAssets = []*mkd.EmoteImageAsset{{
					ImageIds: &mkd.EmoteImageIds{
						Image1XId: "9d928ab5-1482-4be8-90cb-11357ab3c5c2",
						Image2XId: "9d928ab5-1482-4be8-90cb-11357ab3c5c3",
						Image4XId: "9d928ab5-1482-4be8-90cb-11357ab3c5c4",
					},
					AssetType: mkd.EmoteAssetType_animated,
				}}
				req.AssetType = mkd.EmoteAssetType_static

				_, err := createEmoteAPI.CreateEmote(context.Background(), req)
				So(err, ShouldNotBeNil)
			})

			Convey("With no static asset for an animated emote", func() {
				req := getCreateEmoteRequest()
				req.ImageAssets = []*mkd.EmoteImageAsset{{
					ImageIds: &mkd.EmoteImageIds{
						Image1XId: "9d928ab5-1482-4be8-90cb-11357ab3c5c2",
						Image2XId: "9d928ab5-1482-4be8-90cb-11357ab3c5c3",
						Image4XId: "9d928ab5-1482-4be8-90cb-11357ab3c5c4",
					},
					AssetType: mkd.EmoteAssetType_animated,
				}, {
					ImageIds: &mkd.EmoteImageIds{
						Image1XId: "9d928ab5-1482-4be8-90cb-11357ab3c5c2",
						Image2XId: "9d928ab5-1482-4be8-90cb-11357ab3c5c3",
						Image4XId: "9d928ab5-1482-4be8-90cb-11357ab3c5c4",
					},
					AssetType: mkd.EmoteAssetType_animated,
				}}
				req.AssetType = mkd.EmoteAssetType_animated

				_, err := createEmoteAPI.CreateEmote(context.Background(), req)
				So(err, ShouldNotBeNil)
			})

			Convey("With no animated asset for an animated emote", func() {
				req := getCreateEmoteRequest()
				req.ImageAssets = []*mkd.EmoteImageAsset{{
					ImageIds: &mkd.EmoteImageIds{
						Image1XId: "9d928ab5-1482-4be8-90cb-11357ab3c5c2",
						Image2XId: "9d928ab5-1482-4be8-90cb-11357ab3c5c3",
						Image4XId: "9d928ab5-1482-4be8-90cb-11357ab3c5c4",
					},
					AssetType: mkd.EmoteAssetType_static,
				}, {
					ImageIds: &mkd.EmoteImageIds{
						Image1XId: "9d928ab5-1482-4be8-90cb-11357ab3c5c2",
						Image2XId: "9d928ab5-1482-4be8-90cb-11357ab3c5c3",
						Image4XId: "9d928ab5-1482-4be8-90cb-11357ab3c5c4",
					},
					AssetType: mkd.EmoteAssetType_static,
				}}
				req.AssetType = mkd.EmoteAssetType_animated

				_, err := createEmoteAPI.CreateEmote(context.Background(), req)
				So(err, ShouldNotBeNil)
			})

			Convey("With no image ids in image_asset", func() {
				req := getCreateEmoteRequest()
				req.ImageAssets = []*mkd.EmoteImageAsset{{
					ImageIds: &mkd.EmoteImageIds{
						Image1XId: "",
						Image2XId: "",
						Image4XId: "",
					},
					AssetType: mkd.EmoteAssetType_static,
				}}

				_, err := createEmoteAPI.CreateEmote(context.Background(), req)
				So(err, ShouldNotBeNil)
			})

			Convey("With unknown emote state and emote approval check false", func() {
				req := getCreateEmoteRequest()
				req.State = mkd.EmoteState_unknown
				req.EnforceModerationWorkflow = false

				_, err := createEmoteAPI.CreateEmote(context.Background(), req)

				So(err, ShouldNotBeNil)
				So(err.Error(), ShouldContainSubstring, "cannot use unknown state unless enforce_moderation_flow is true")
			})

			Convey("With archived emote state and non-Archive origin", func() {
				req := getCreateEmoteRequest()
				req.State = mkd.EmoteState_archived

				_, err := createEmoteAPI.CreateEmote(context.Background(), req)

				So(err, ShouldNotBeNil)
				So(err.Error(), ShouldContainSubstring, "cannot use archived or pending_archived state unless origin is Archive")
			})

			Convey("With pending_archived emote state and non-Archive origin", func() {
				req := getCreateEmoteRequest()
				req.State = mkd.EmoteState_archived

				_, err := createEmoteAPI.CreateEmote(context.Background(), req)

				So(err, ShouldNotBeNil)
				So(err.Error(), ShouldContainSubstring, "cannot use archived or pending_archived state unless origin is Archive")
			})

			Convey("With error on emoticon manager create", func() {
				req := getCreateEmoteRequest()
				mockEmoticon.On("Create", mock.Anything, mock.Anything).Return(mako.Emote{}, errors.New("test"))

				_, err := createEmoteAPI.CreateEmote(context.Background(), req)
				So(err, ShouldNotBeNil)
			})

			Convey(" with an animation template and a static emote", func() {
				req := getCreateEmoteRequest()
				req.AnimatedEmoteTemplate = mkd.AnimatedEmoteTemplate_SLIDEIN

				_, err := createEmoteAPI.CreateEmote(context.Background(), req)

				So(err, ShouldNotBeNil)
				So(err.Error(), ShouldContainSubstring, "only animated emotes can use animation templates")
			})

			Convey(" with an animated type and a default image library", func() {
				req := getCreateEmoteRequest()
				req.AssetType = mkd.EmoteAssetType_animated
				req.ImageSource = mkd.ImageSource_DefaultLibraryImage

				_, err := createEmoteAPI.CreateEmote(context.Background(), req)

				So(err, ShouldNotBeNil)
				So(err.Error(), ShouldContainSubstring, "default library emotes cannot be used to create animated emotes")
			})

			Convey("Given emoticons manager returns successfully", func() {
				req := getCreateEmoteRequest()
				res := mako.Emote{
					ID:          "789",
					OwnerID:     req.UserId,
					GroupID:     req.GroupIds[0],
					Prefix:      mako.ExtractEmotePrefix(req.Code, req.CodeSuffix),
					Suffix:      req.CodeSuffix,
					Code:        req.Code,
					State:       mako.Pending,
					Domain:      mako.EmotesDomain,
					CreatedAt:   time.Now(),
					UpdatedAt:   time.Now(),
					Order:       0,
					EmotesGroup: "None",
					AssetType:   mako.StaticAssetType,
				}
				mockEmoticon.On("Create", mock.Anything, mock.Anything).Return(res, nil)

				Convey("Given paint client returns successfully then return no error", func() {
					mockPaint.On("ModifyEmote", mock.Anything, mock.Anything).Return(&paintrpc.ModifyEmoteResponse{}, nil)

					result, err := createEmoteAPI.CreateEmote(context.Background(), req)
					So(err, ShouldBeNil)
					So(result, ShouldNotBeNil)
					So(result.Emote, ShouldNotBeNil)

					twirpRes := res.ToDashboardTwirp()
					So(proto.Equal(result.Emote, &twirpRes), ShouldBeTrue)
				})

				Convey("Given a request to create a Bits Badge Tier Emote then don't call paint client to modify the emote", func() {
					req.Origin = mkd.EmoteOrigin_BitsBadgeTierEmotes

					result, err := createEmoteAPI.CreateEmote(context.Background(), req)
					So(err, ShouldBeNil)
					So(result, ShouldNotBeNil)
					mockPaint.AssertNotCalled(t, "ModifyEmote", mock.Anything, mock.Anything)
				})

				Convey("Given archive related input then pass a true ShouldBeArchived to the emoticons manager", func() {
					req.Origin = mkd.EmoteOrigin_Archive
					req.GroupIds = nil

					result, err := createEmoteAPI.CreateEmote(context.Background(), req)
					So(err, ShouldBeNil)
					So(result, ShouldNotBeNil)

					createParams := mockEmoticon.Calls[0].Arguments.Get(1).(*emoticonsmanager.CreateParams)
					So(createParams.ShouldBeArchived, ShouldBeTrue)
				})
			})

			Convey("Given emoticons manager returns successfully with an animated emote", func() {
				req := getCreateEmoteRequest()
				req.AnimatedEmoteTemplate = mkd.AnimatedEmoteTemplate_ROLL
				req.AssetType = mkd.EmoteAssetType_animated
				req.ImageAssets = append(req.ImageAssets, &mkd.EmoteImageAsset{
					ImageIds: &mkd.EmoteImageIds{
						Image1XId: "9d928ab5-1482-4be8-90cb-11357ab3c5c2",
						Image2XId: "9d928ab5-1482-4be8-90cb-11357ab3c5c3",
						Image4XId: "9d928ab5-1482-4be8-90cb-11357ab3c5c4",
					},
					AssetType: mkd.EmoteAssetType_animated,
				})

				res := mako.Emote{
					ID:                "789",
					OwnerID:           req.UserId,
					GroupID:           req.GroupIds[0],
					Prefix:            mako.ExtractEmotePrefix(req.Code, req.CodeSuffix),
					Suffix:            req.CodeSuffix,
					Code:              req.Code,
					State:             mako.Pending,
					Domain:            mako.EmotesDomain,
					CreatedAt:         time.Now(),
					UpdatedAt:         time.Now(),
					Order:             0,
					EmotesGroup:       "None",
					AssetType:         mako.AnimatedAssetType,
					AnimationTemplate: mako.Roll,
				}
				mockEmoticon.On("Create", mock.Anything, mock.Anything).Return(res, nil)

				Convey("Given paint client returns successfully then return no error", func() {
					mockPaint.On("ModifyEmote", mock.Anything, mock.Anything).Return(&paintrpc.ModifyEmoteResponse{}, nil)

					result, err := createEmoteAPI.CreateEmote(context.Background(), req)
					So(err, ShouldBeNil)
					So(result, ShouldNotBeNil)
					So(result.Emote, ShouldNotBeNil)

					twirpRes := res.ToDashboardTwirp()
					So(proto.Equal(result.Emote, &twirpRes), ShouldBeTrue)
				})

				Convey("Given archive related input then pass a true ShouldBeArchived to the emoticons manager", func() {
					req.Origin = mkd.EmoteOrigin_Archive
					req.GroupIds = nil

					result, err := createEmoteAPI.CreateEmote(context.Background(), req)
					So(err, ShouldBeNil)
					So(result, ShouldNotBeNil)

					createParams := mockEmoticon.Calls[0].Arguments.Get(1).(*emoticonsmanager.CreateParams)
					So(createParams.ShouldBeArchived, ShouldBeTrue)
				})
			})

			Convey("Given emoticons manager errors", func() {
				Convey("When the code is not unique", func() {
					req := getCreateEmoteRequest()
					mockEmoticon.On("Create", mock.Anything, mock.Anything).Return(mako.Emote{}, emoticonsmanager.EmoticonCodeNotUnique)
					result, err := createEmoteAPI.CreateEmote(context.Background(), req)
					So(result, ShouldBeNil)
					So(err, ShouldNotBeNil)
					So(err.Error(), ShouldContainSubstring, "must be unique")
				})

				Convey("When the code is not acceptable", func() {
					req := getCreateEmoteRequest()
					mockEmoticon.On("Create", mock.Anything, mock.Anything).Return(mako.Emote{}, emoticonsmanager.EmoticonCodeUnacceptable)
					result, err := createEmoteAPI.CreateEmote(context.Background(), req)
					So(result, ShouldBeNil)
					So(err, ShouldNotBeNil)
					So(err.Error(), ShouldContainSubstring, "is unacceptable")
				})

				Convey("When the suffix is invalid", func() {
					req := getCreateEmoteRequest()
					mockEmoticon.On("Create", mock.Anything, mock.Anything).Return(mako.Emote{}, emoticonsmanager.InvalidCodeSuffixFormat)
					result, err := createEmoteAPI.CreateEmote(context.Background(), req)
					So(result, ShouldBeNil)
					So(err, ShouldNotBeNil)
					So(err.Error(), ShouldContainSubstring, "invalid format")
				})

				Convey("When the user owned emote limit is reached", func() {
					req := getCreateEmoteRequest()
					mockEmoticon.On("Create", mock.Anything, mock.Anything).Return(mako.Emote{}, emoticonsmanager.UserOwnedEmoteLimitReached)
					result, err := createEmoteAPI.CreateEmote(context.Background(), req)
					So(result, ShouldBeNil)
					So(err, ShouldNotBeNil)
					So(err.Error(), ShouldContainSubstring, "user cannot own any more emotes of this type")
				})

				Convey("When the image is invalid", func() {
					req := getCreateEmoteRequest()
					mockEmoticon.On("Create", mock.Anything, mock.Anything).Return(mako.Emote{}, emoticonsmanager.InvalidImageUpload)
					result, err := createEmoteAPI.CreateEmote(context.Background(), req)
					So(result, ShouldBeNil)
					So(err, ShouldNotBeNil)
					So(err.Error(), ShouldContainSubstring, "invalid image uploaded")
				})
				Convey("When the 28 image_id is not found", func() {
					req := getCreateEmoteRequest()
					mockEmoticon.On("Create", mock.Anything, mock.Anything).Return(mako.Emote{}, emoticonsmanager.Image1XIDNotFound)
					result, err := createEmoteAPI.CreateEmote(context.Background(), req)
					So(result, ShouldBeNil)
					So(err, ShouldNotBeNil)
					So(err.Error(), ShouldContainSubstring, "not found")
				})
				Convey("When the 56 image_id is not found", func() {
					req := getCreateEmoteRequest()
					mockEmoticon.On("Create", mock.Anything, mock.Anything).Return(mako.Emote{}, emoticonsmanager.Image2XIDNotFound)
					result, err := createEmoteAPI.CreateEmote(context.Background(), req)
					So(result, ShouldBeNil)
					So(err, ShouldNotBeNil)
					So(err.Error(), ShouldContainSubstring, "not found")
				})
				Convey("When the 112 image_id is not found", func() {
					req := getCreateEmoteRequest()
					mockEmoticon.On("Create", mock.Anything, mock.Anything).Return(mako.Emote{}, emoticonsmanager.Image4XIDNotFound)
					result, err := createEmoteAPI.CreateEmote(context.Background(), req)
					So(result, ShouldBeNil)
					So(err, ShouldNotBeNil)
					So(err.Error(), ShouldContainSubstring, "not found")
				})
			})
		})

		Convey("Test CreateFollowerEmote", func() {
			Convey("Is in freemotes experiment", func() {
				mockDynamicConfig.On("IsInFreemotesExperiment", userID).Return(true)

				Convey("when no user_id is provided", func() {
					result, err := createEmoteAPI.CreateFollowerEmote(context.Background(), &mkd.CreateFollowerEmoteRequest{})
					So(result, ShouldBeNil)
					So(err, ShouldNotBeNil)
				})

				Convey("with a properly formed request", func() {
					req := getCreateFollowerEmoteRequest()

					Convey("When the follower emote group for this channel is not found due to error", func() {
						mockEmoteGroupDB.On("GetFollowerEmoteGroup", mock.Anything, userID).Return(nil, errors.New("test error"))

						result, err := createEmoteAPI.CreateFollowerEmote(context.Background(), req)
						So(result, ShouldBeNil)
						So(err, ShouldNotBeNil)
					})

					Convey("When there is no follower emote group for this channel", func() {
						mockEmoteGroupDB.On("GetFollowerEmoteGroup", mock.Anything, userID).Return(nil, nil)

						result, err := createEmoteAPI.CreateFollowerEmote(context.Background(), req)
						So(result, ShouldBeNil)
						So(err, ShouldNotBeNil)
					})

					Convey("When the follower emote group for this channel is found", func() {
						groupID := "123456"
						mockEmoteGroupDB.On("GetFollowerEmoteGroup", mock.Anything, userID).Return(&mako.EmoteGroup{
							GroupID:   groupID,
							OwnerID:   userID,
							Origin:    mako.EmoteGroupOriginFollower,
							GroupType: mako.EmoteGroupStatic,
						}, nil)

						res := mako.Emote{
							ID:          "789",
							OwnerID:     req.UserId,
							GroupID:     groupID,
							Suffix:      req.CodeSuffix,
							State:       mako.Active,
							Domain:      mako.EmotesDomain,
							CreatedAt:   time.Now(),
							UpdatedAt:   time.Now(),
							Order:       0,
							EmotesGroup: mkd.EmoteOrigin_Follower.String(),
							AssetType:   mako.StaticAssetType,
						}
						mockEmoticon.On("Create", mock.Anything, mock.Anything).Return(res, nil)
						mockPaint.On("ModifyEmote", mock.Anything, mock.Anything).Return(&paintrpc.ModifyEmoteResponse{}, nil)
						result, err := createEmoteAPI.CreateFollowerEmote(context.Background(), req)
						So(result, ShouldNotBeNil)
						So(err, ShouldBeNil)
					})
				})
			})

			Convey("Is not in freemotes experiment", func() {
				groupID := "123456"
				mockEmoteGroupDB.On("GetFollowerEmoteGroup", mock.Anything, userID).Return(&mako.EmoteGroup{
					GroupID:   groupID,
					OwnerID:   userID,
					Origin:    mako.EmoteGroupOriginFollower,
					GroupType: mako.EmoteGroupStatic,
				}, nil)

				mockDynamicConfig.On("IsInFreemotesExperiment", userID).Return(false)

				req := getCreateFollowerEmoteRequest()
				result, err := createEmoteAPI.CreateFollowerEmote(context.Background(), req)
				So(result, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})
		})
	})
}
