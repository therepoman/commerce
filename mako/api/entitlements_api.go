package api

import (
	"context"

	mk "code.justin.tv/commerce/mako/twirp"
	"github.com/twitchtv/twirp"
)

// These API's are deprecated.
type EntitlementsAPI struct{}

func (entitlementsAPI *EntitlementsAPI) GetEntitlements(ctx context.Context, r *mk.GetEntitlementsRequest) (*mk.GetEntitlementsResponse, error) {
	return nil, twirp.NewError(twirp.Unimplemented, "Method no longer supported or implemented")
}

func (entitlementsAPI *EntitlementsAPI) SetEntitlement(ctx context.Context, r *mk.SetEntitlementRequest) (*mk.SetEntitlementResponse, error) {
	return nil, twirp.NewError(twirp.Unimplemented, "Method no longer supported or implemented")
}

func (entitlementsAPI *EntitlementsAPI) DeleteEntitlements(ctx context.Context, r *mk.DeleteEntitlementsRequest) (*mk.DeleteEntitlementsResponse, error) {
	return nil, twirp.NewError(twirp.Unimplemented, "Method no longer supported or implemented")
}
