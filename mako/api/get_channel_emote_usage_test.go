package api

import (
	"context"
	"errors"
	"testing"
	"time"

	chemtrailTwirp "code.justin.tv/ce-analytics/chemtrail/rpc/chemtrail"
	"code.justin.tv/commerce/mako/clients"
	mako "code.justin.tv/commerce/mako/internal"
	"code.justin.tv/commerce/mako/localdb"
	chemtrail_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/chemtrail"
	productemotegroups_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/productemotegroups"
	clients_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/web/users-service/client/usersclient_internal"
	"code.justin.tv/web/users-service/models"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
	"google.golang.org/protobuf/types/known/timestamppb"

	mkd "code.justin.tv/commerce/mako/twirp/dashboard"
)

func TestGetChannelEmoteUsage(t *testing.T) {
	emoteDBEmotes := []mako.Emote{
		{
			ID:      "777",
			OwnerID: "666",
			GroupID: "123",
			State:   mako.Active,
			Suffix:  "Hype",
		}, {
			ID:      "888",
			OwnerID: "666",
			GroupID: "123",
			State:   mako.Active,
			Suffix:  "Pog",
		}, {
			ID:      "1000",
			OwnerID: "001",
			GroupID: "123",
			State:   mako.Active,
			Suffix:  "Sip",
		}, {
			ID:      "1200",
			OwnerID: "666",
			GroupID: "123",
			State:   mako.Pending,
			Suffix:  "Laugh",
		}, {
			ID:      "1300",
			OwnerID: "666",
			GroupID: "125",
			State:   mako.Active,
			Suffix:  "Heart",
		}, {
			ID:      "1400",
			OwnerID: "666",
			GroupID: "223",
			State:   mako.Active,
			Suffix:  "Boo",
		}, {
			ID:        "1500",
			OwnerID:   "666",
			GroupID:   "124",
			State:     mako.Active,
			Suffix:    "Shake",
			AssetType: mako.AnimatedAssetType,
		},
	}

	ctx := context.Background()
	fakeEmoteDB, _, _ := localdb.MakeEmoteDB(ctx, emoteDBEmotes...)
	mockUsersService := new(clients_mock.InternalClient)
	mockChemtrailClient := new(chemtrail_mock.ChemtrailClient)
	mockProductEmoteGroups := new(productemotegroups_mock.ProductEmoteGroups)

	getChannelEmoteUsageAPI := GetChannelEmoteUsageAPI{
		Clients: clients.Clients{
			EmoteDB:                  fakeEmoteDB,
			UserServiceClient:        mockUsersService,
			ChemtrailClient:          mockChemtrailClient,
			ProductEmoteGroupsClient: mockProductEmoteGroups,
		},
	}

	emoteGroupResponse := []*mkd.EmoteGroup{{
		Id:          "123",
		GroupType:   mkd.EmoteGroupType_static_group,
		ProductType: mkd.EmoteGroupProductType_SubscriptionTier1,
	}, {
		Id:          "124",
		GroupType:   mkd.EmoteGroupType_animated_group,
		ProductType: mkd.EmoteGroupProductType_SubscriptionTier1,
	}, {
		Id:          "125",
		GroupType:   mkd.EmoteGroupType_static_group,
		ProductType: mkd.EmoteGroupProductType_SubscriptionTier2,
	}, {
		Id:          "126",
		GroupType:   mkd.EmoteGroupType_static_group,
		ProductType: mkd.EmoteGroupProductType_SubscriptionTier3,
	}, {
		Id:          "223",
		GroupType:   mkd.EmoteGroupType_static_group,
		ProductType: mkd.EmoteGroupProductType_FollowerTier,
	}, {
		Id:          "323",
		GroupType:   mkd.EmoteGroupType_static_group,
		ProductType: mkd.EmoteGroupProductType_BitsTier1000,
	}}

	chemtrailResp := &chemtrailTwirp.GetEmoteUsageResponse{
		EmoteUsage: []*chemtrailTwirp.EmoteUsage{
			{
				EmoteId:      "777",
				EmoteGroupId: "123",
				TotalUsage:   9001,
				UniqueUsers:  800,
			}, {
				EmoteId:      "1000", // EmoteGroupID collisions. Different owner
				EmoteGroupId: "123",
				TotalUsage:   8888,
				UniqueUsers:  100,
			}, {
				EmoteId:      "1300",
				EmoteGroupId: "125",
				TotalUsage:   120,
				UniqueUsers:  90,
			}, {
				EmoteId:      "1400",
				EmoteGroupId: "223",
				TotalUsage:   120,
				UniqueUsers:  90,
			}, {
				EmoteId:      "888",
				EmoteGroupId: "123",
				TotalUsage:   100,
				UniqueUsers:  1,
			}, {
				EmoteId:      "1200", // Pending State
				EmoteGroupId: "123",
				TotalUsage:   97,
				UniqueUsers:  97,
			}, {
				EmoteId:      "999", // Deleted emote
				EmoteGroupId: "123",
				TotalUsage:   25,
				UniqueUsers:  10,
			},
		},
	}

	chemtrailStaticT1SubResp := &chemtrailTwirp.GetEmoteUsageResponse{
		EmoteUsage: []*chemtrailTwirp.EmoteUsage{
			{
				EmoteId:      "777",
				EmoteGroupId: "123",
				TotalUsage:   9001,
				UniqueUsers:  800,
			}, {
				EmoteId:      "888",
				EmoteGroupId: "123",
				TotalUsage:   100,
				UniqueUsers:  1,
			}, {
				EmoteId:      "999", // Deleted emote
				EmoteGroupId: "123",
				TotalUsage:   25,
				UniqueUsers:  10,
			}, {
				EmoteId:      "1000", // EmoteGroupID collisions. Different owner
				EmoteGroupId: "123",
				TotalUsage:   8888,
				UniqueUsers:  100,
			}, {
				EmoteId:      "1200", // Pending State
				EmoteGroupId: "123",
				TotalUsage:   97,
				UniqueUsers:  97,
			},
		},
	}

	Convey("Test GetChannelEmoteUsage API", t, func() {
		req := &mkd.GetChannelEmoteUsageRequest{}

		Convey("Missing channelId", func() {
			resp, err := getChannelEmoteUsageAPI.GetChannelEmoteUsage(context.Background(), req)
			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})
		Convey("With ChannelId", func() {
			req.ChannelId = "666"

			Convey("Missing requestingUserId", func() {
				resp, err := getChannelEmoteUsageAPI.GetChannelEmoteUsage(context.Background(), req)
				So(resp, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})

			Convey("RequestingUserId does not match ChannelId", func() {
				req.RequestingUserId = "777"

				Convey("UsersService returns an error", func() {
					mockUsersService.On("GetUserByID", mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("userservice error")).Once()
					resp, err := getChannelEmoteUsageAPI.GetChannelEmoteUsage(context.Background(), req)
					So(resp, ShouldBeNil)
					So(err, ShouldNotBeNil)
				})

				Convey("Requesting userId is not staff", func() {
					isAdmin := false
					mockUsersService.On("GetUserByID", mock.Anything, mock.Anything, mock.Anything).Return(&models.Properties{
						Admin: &isAdmin,
					}, nil).Once()

					resp, err := getChannelEmoteUsageAPI.GetChannelEmoteUsage(context.Background(), req)
					So(resp, ShouldBeNil)
					So(err, ShouldNotBeNil)

				})
				Convey("Requesting userId is staff", func() {
					mockChemtrailClient.On("GetEmoteUsage", mock.Anything, mock.Anything).Return(chemtrailResp, nil).Once()
					isAdmin := true
					mockUsersService.On("GetUserByID", mock.Anything, mock.Anything, mock.Anything).Return(&models.Properties{
						Admin: &isAdmin,
					}, nil)
					mockProductEmoteGroups.On("GetProductEmoteGroups", mock.Anything, mock.Anything).Return(emoteGroupResponse, nil).Once()

					resp, err := getChannelEmoteUsageAPI.GetChannelEmoteUsage(context.Background(), req)
					So(resp, ShouldNotBeNil)
					So(err, ShouldBeNil)
				})
			})

			Convey("When user is authorized", func() {
				req.ChannelId = "666"
				req.RequestingUserId = "666"
				req.StartTime = timestamppb.New(time.Now().AddDate(0, -1, 0))
				req.EndTime = timestamppb.New(time.Now())
				req.UsageType = mkd.EmoteUsageType_All
				req.SortBy = mkd.EmoteUsageSortBy_TotalUsageDescending
				req.EmoteGroupAssetTypes = []mkd.EmoteGroupType{mkd.EmoteGroupType_static_group}
				req.EmoteGroupProductTypes = []mkd.EmoteGroupProductType{mkd.EmoteGroupProductType_SubscriptionTier1}

				isAdmin := false
				mockUsersService.On("GetUserByID", mock.Anything, mock.Anything, mock.Anything).Return(&models.Properties{
					Admin: &isAdmin,
				}, nil)

				Convey("GetProductEmoteGroups returns an error", func() {
					mockProductEmoteGroups.On("GetProductEmoteGroups", mock.Anything, mock.Anything).Return(nil, errors.New("emotegroupproducts error")).Once()
					resp, err := getChannelEmoteUsageAPI.GetChannelEmoteUsage(context.Background(), req)

					So(resp, ShouldBeNil)
					So(err, ShouldNotBeNil)
				})

				Convey("GetProductEmoteGroups returns success", func() {
					mockProductEmoteGroups.On("GetProductEmoteGroups", mock.Anything, mock.Anything).Return(emoteGroupResponse, nil)

					Convey("GetProductEmoteGroups returns but no groups match filter", func() {
						req.EmoteGroupAssetTypes = []mkd.EmoteGroupType{mkd.EmoteGroupType_animated_group}
						req.EmoteGroupProductTypes = []mkd.EmoteGroupProductType{mkd.EmoteGroupProductType_BitsTier1000}
						resp, err := getChannelEmoteUsageAPI.GetChannelEmoteUsage(context.Background(), req)
						So(err, ShouldBeNil)
						So(resp.EmoteUsage, ShouldHaveLength, 0)
					})

					Convey("Chemtrail returns error", func() {
						mockChemtrailClient.On("GetEmoteUsage", mock.Anything, mock.Anything).Return(nil, errors.New("chemtrail error")).Once()
						resp, err := getChannelEmoteUsageAPI.GetChannelEmoteUsage(context.Background(), req)

						So(resp, ShouldBeNil)
						So(err, ShouldNotBeNil)
					})

					Convey("Maintains the order from chemtrail and returns the correct emote group for each usage", func() {
						mockChemtrailClient.On("GetEmoteUsage", mock.Anything, mock.Anything).Return(chemtrailResp, nil).Once()
						req.EmoteGroupAssetTypes = []mkd.EmoteGroupType{mkd.EmoteGroupType_static_group}
						req.EmoteGroupProductTypes = []mkd.EmoteGroupProductType{
							mkd.EmoteGroupProductType_SubscriptionTier1,
							mkd.EmoteGroupProductType_SubscriptionTier2,
							mkd.EmoteGroupProductType_SubscriptionTier3,
							mkd.EmoteGroupProductType_FollowerTier,
						}
						resp, err := getChannelEmoteUsageAPI.GetChannelEmoteUsage(context.Background(), req)
						So(err, ShouldBeNil)
						So(resp, ShouldNotBeNil)

						expectedEmoteIdOrder := []string{"777", "1300", "1400", "888"}
						expectedGroups := []*mkd.EmoteGroup{
							emoteGroupResponse[0],
							emoteGroupResponse[2],
							emoteGroupResponse[4],
							emoteGroupResponse[0],
						}

						for i := 0; i < len(resp.EmoteUsage); i++ {
							So(resp.EmoteUsage[i].Emote.Id, ShouldEqual, expectedEmoteIdOrder[i])
							So(resp.EmoteUsage[i].EmoteGroup.Id, ShouldEqual, expectedGroups[i].Id)
							So(resp.EmoteUsage[i].EmoteGroup.GroupType, ShouldEqual, expectedGroups[i].GroupType)
							So(resp.EmoteUsage[i].EmoteGroup.ProductType, ShouldEqual, expectedGroups[i].ProductType)
						}
					})

					Convey("Chemtrail returns static tier 1 sub usage data", func() {
						mockChemtrailClient.On("GetEmoteUsage", mock.Anything, mock.Anything).Return(chemtrailStaticT1SubResp, nil).Once()

						Convey("Filters out deleted emotes", func() {
							req.EmoteGroupAssetTypes = []mkd.EmoteGroupType{mkd.EmoteGroupType_static_group}
							req.EmoteGroupProductTypes = []mkd.EmoteGroupProductType{mkd.EmoteGroupProductType_SubscriptionTier1}
							resp, err := getChannelEmoteUsageAPI.GetChannelEmoteUsage(context.Background(), req)
							So(err, ShouldBeNil)
							So(resp, ShouldNotBeNil)

							So(resp.EmoteUsage, ShouldHaveLength, 2)
							for _, usage := range resp.EmoteUsage {
								So(usage.Emote.Id, ShouldNotEqual, "999")
							}
						})
						Convey("Filters out emotes with a different owner", func() {
							req.EmoteGroupAssetTypes = []mkd.EmoteGroupType{mkd.EmoteGroupType_static_group}
							req.EmoteGroupProductTypes = []mkd.EmoteGroupProductType{mkd.EmoteGroupProductType_SubscriptionTier1}
							resp, err := getChannelEmoteUsageAPI.GetChannelEmoteUsage(context.Background(), req)
							So(err, ShouldBeNil)
							So(resp, ShouldNotBeNil)

							So(resp.EmoteUsage, ShouldHaveLength, 2)
							for _, usage := range resp.EmoteUsage {
								So(usage.Emote.Id, ShouldNotEqual, "1000")
							}
						})
						Convey("Filters out emotes that are not active or archived", func() {
							req.EmoteGroupAssetTypes = []mkd.EmoteGroupType{mkd.EmoteGroupType_static_group}
							req.EmoteGroupProductTypes = []mkd.EmoteGroupProductType{mkd.EmoteGroupProductType_SubscriptionTier1}
							resp, err := getChannelEmoteUsageAPI.GetChannelEmoteUsage(context.Background(), req)
							So(err, ShouldBeNil)
							So(resp, ShouldNotBeNil)

							So(resp.EmoteUsage, ShouldHaveLength, 2)
							for _, usage := range resp.EmoteUsage {
								So(usage.Emote.Id, ShouldNotEqual, "1200")
							}
						})
					})

					Convey("Test assetType and productType filters", func() {
						mockChemtrailClient.On("GetEmoteUsage", mock.Anything, mock.Anything).Return(chemtrailResp, nil)

						Convey("Does not filter by any emote groups when no asset and product type filters are provided", func() {
							req.EmoteGroupAssetTypes = nil
							req.EmoteGroupProductTypes = nil
							_, err := getChannelEmoteUsageAPI.GetChannelEmoteUsage(context.Background(), req)
							So(err, ShouldBeNil)
							So(mockChemtrailClient.Calls[len(mockChemtrailClient.Calls)-1].Arguments[1], ShouldResemble, &chemtrailTwirp.GetEmoteUsageRequest{
								UserId:      req.ChannelId,
								StartTime:   req.StartTime,
								EndTime:     req.EndTime,
								UsageType:   chemtrailTwirp.EmoteUsageType_EMOTE_USAGE_TYPE_ALL,
								EmoteSortBy: chemtrailTwirp.EmoteUsageSortBy_EMOTE_USAGE_SORT_BY_TOTAL_USAGE,
								Sort:        chemtrailTwirp.SortOrder_DESC,
								Limit:       chemtrailUsageRequestLimit,
							})
						})

						Convey("Filters emote groups by assetType", func() {
							req.EmoteGroupAssetTypes = []mkd.EmoteGroupType{mkd.EmoteGroupType_animated_group}
							req.EmoteGroupProductTypes = []mkd.EmoteGroupProductType{mkd.EmoteGroupProductType_SubscriptionTier1}
							_, err := getChannelEmoteUsageAPI.GetChannelEmoteUsage(context.Background(), req)
							So(err, ShouldBeNil)
							So(mockChemtrailClient.Calls[len(mockChemtrailClient.Calls)-1].Arguments[1], ShouldResemble, &chemtrailTwirp.GetEmoteUsageRequest{
								UserId:        req.ChannelId,
								StartTime:     req.StartTime,
								EndTime:       req.EndTime,
								UsageType:     chemtrailTwirp.EmoteUsageType_EMOTE_USAGE_TYPE_ALL,
								EmoteSortBy:   chemtrailTwirp.EmoteUsageSortBy_EMOTE_USAGE_SORT_BY_TOTAL_USAGE,
								Sort:          chemtrailTwirp.SortOrder_DESC,
								EmoteGroupIds: []string{"124"},
								Limit:         chemtrailUsageRequestLimit,
							})
						})

						Convey("Does not filter emote groups by assetType when no assetType is provided", func() {
							req.EmoteGroupAssetTypes = nil
							req.EmoteGroupProductTypes = []mkd.EmoteGroupProductType{mkd.EmoteGroupProductType_SubscriptionTier1}
							resp, err := getChannelEmoteUsageAPI.GetChannelEmoteUsage(context.Background(), req)
							So(err, ShouldBeNil)
							So(resp, ShouldNotBeNil)
							So(mockChemtrailClient.Calls[len(mockChemtrailClient.Calls)-1].Arguments[1], ShouldResemble, &chemtrailTwirp.GetEmoteUsageRequest{
								UserId:        req.ChannelId,
								StartTime:     req.StartTime,
								EndTime:       req.EndTime,
								UsageType:     chemtrailTwirp.EmoteUsageType_EMOTE_USAGE_TYPE_ALL,
								EmoteSortBy:   chemtrailTwirp.EmoteUsageSortBy_EMOTE_USAGE_SORT_BY_TOTAL_USAGE,
								Sort:          chemtrailTwirp.SortOrder_DESC,
								EmoteGroupIds: []string{"123", "124"},
								Limit:         chemtrailUsageRequestLimit,
							})
						})

						Convey("Filters emote groups by product type", func() {
							req.EmoteGroupAssetTypes = []mkd.EmoteGroupType{mkd.EmoteGroupType_static_group}
							req.EmoteGroupProductTypes = []mkd.EmoteGroupProductType{mkd.EmoteGroupProductType_FollowerTier}
							resp, err := getChannelEmoteUsageAPI.GetChannelEmoteUsage(context.Background(), req)
							So(err, ShouldBeNil)
							So(resp, ShouldNotBeNil)
							So(mockChemtrailClient.Calls[len(mockChemtrailClient.Calls)-1].Arguments[1], ShouldResemble, &chemtrailTwirp.GetEmoteUsageRequest{
								UserId:        req.ChannelId,
								StartTime:     req.StartTime,
								EndTime:       req.EndTime,
								UsageType:     chemtrailTwirp.EmoteUsageType_EMOTE_USAGE_TYPE_ALL,
								EmoteSortBy:   chemtrailTwirp.EmoteUsageSortBy_EMOTE_USAGE_SORT_BY_TOTAL_USAGE,
								Sort:          chemtrailTwirp.SortOrder_DESC,
								EmoteGroupIds: []string{"223"},
								Limit:         chemtrailUsageRequestLimit,
							})
						})

						Convey("Does not filter emote groups by product type if no product type is provided", func() {
							req.EmoteGroupAssetTypes = []mkd.EmoteGroupType{mkd.EmoteGroupType_static_group}
							req.EmoteGroupProductTypes = nil
							resp, err := getChannelEmoteUsageAPI.GetChannelEmoteUsage(context.Background(), req)
							So(err, ShouldBeNil)
							So(resp, ShouldNotBeNil)
							So(mockChemtrailClient.Calls[len(mockChemtrailClient.Calls)-1].Arguments[1], ShouldResemble, &chemtrailTwirp.GetEmoteUsageRequest{
								UserId:        req.ChannelId,
								StartTime:     req.StartTime,
								EndTime:       req.EndTime,
								UsageType:     chemtrailTwirp.EmoteUsageType_EMOTE_USAGE_TYPE_ALL,
								EmoteSortBy:   chemtrailTwirp.EmoteUsageSortBy_EMOTE_USAGE_SORT_BY_TOTAL_USAGE,
								Sort:          chemtrailTwirp.SortOrder_DESC,
								EmoteGroupIds: []string{"123", "125", "126", "223", "323"},
								Limit:         chemtrailUsageRequestLimit,
							})
						})
					})
				})
			})
		})
	})
}
