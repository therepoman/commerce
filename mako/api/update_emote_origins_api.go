package api

import (
	"context"
	"time"

	"code.justin.tv/commerce/mako/clients/emoticonsmanager"

	log "code.justin.tv/commerce/logrus"
	mk "code.justin.tv/commerce/mako/twirp"
	mkd "code.justin.tv/commerce/mako/twirp/dashboard"
	"github.com/twitchtv/twirp"
)

const maxAllowedNumberOfOriginUpdates = 100

type UpdateEmoteOriginsAPI struct {
	EmoticonsManager emoticonsmanager.IEmoticonsManager
}

func (ueoa *UpdateEmoteOriginsAPI) UpdateEmoteOrigins(ctx context.Context, r *mkd.UpdateEmoteOriginsRequest) (*mkd.UpdateEmoteOriginsResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, time.Second*5)
	defer cancel()

	log.Infof("Update Emote Origins: %s", r.String())

	params, err := validateAndParseEmoteOriginsRequest(r)
	if err != nil {
		return nil, err
	}

	updatedEmotes, err := ueoa.EmoticonsManager.UpdateEmoteOrigins(ctx, *params)
	if err != nil {
		convertedErr := convertEmoteOriginError(err)
		if convertedErr != nil {
			return nil, convertedErr
		}
		log.WithError(err).Error("Error updating emote origins")
		return nil, twirp.InternalError("Error updating emote origins").
			WithMeta("request", r.String()).
			WithMeta("error", err.Error())
	}

	updatedTwirpEmotes := make([]*mkd.Emote, 0, len(updatedEmotes))
	for _, emote := range updatedEmotes {
		twirpEmote := emote.ToDashboardTwirp()
		updatedTwirpEmotes = append(updatedTwirpEmotes, &twirpEmote)
	}

	return &mkd.UpdateEmoteOriginsResponse{
		Emotes: updatedTwirpEmotes,
	}, nil
}

func validateAndParseEmoteOriginsRequest(req *mkd.UpdateEmoteOriginsRequest) (*emoticonsmanager.UpdateOriginsParams, error) {
	twirpOrigins := req.GetOriginUpdates()

	if len(twirpOrigins) == 0 {
		return nil, mk.NewClientError(
			twirp.RequiredArgumentError("origin updates"),
			mk.ErrCodeMissingRequiredArgument)
	}

	if len(twirpOrigins) > maxAllowedNumberOfOriginUpdates {
		return nil, mk.NewClientError(
			twirp.InvalidArgumentError("emote_origins", "too many origin updates provided"), mk.ErrCodeTooManyOriginUpdates)
	}

	newOrigins := make(map[string]mkd.EmoteOrigin)
	for _, originUpdate := range twirpOrigins {
		if originUpdate.EmoteId == "" {
			return nil, mk.NewClientError(
				twirp.RequiredArgumentError("emote_origin.emote_id"),
				mk.ErrCodeEmptyEmoteID)
		}

		newOrigins[originUpdate.GetEmoteId()] = originUpdate.GetOrigin()
	}

	if len(newOrigins) < len(twirpOrigins) {
		// At least one emoteID was duplicated
		return nil, mk.NewClientError(twirp.InvalidArgumentError("emote_origins", "emote given more than one origin update"),
			mk.ErrCodeOriginsNotUnique)
	}

	params := emoticonsmanager.UpdateOriginsParams{
		NewOrigins: newOrigins,
	}

	return &params, nil
}

func convertEmoteOriginError(originError error) error {
	if originError == emoticonsmanager.EmoteDoesNotExist {
		return mk.NewClientError(
			twirp.InvalidArgumentError("emote_origins", "one or more emotes could not be found"),
			mk.ErrEmoteDoesNotExist)
	}

	return nil
}
