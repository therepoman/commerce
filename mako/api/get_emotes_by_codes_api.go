package api

import (
	"context"
	"time"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/mako/clients"
	"code.justin.tv/commerce/mako/config"
	mako "code.justin.tv/commerce/mako/internal"
	mk "code.justin.tv/commerce/mako/twirp"
	"github.com/pkg/errors"
	"github.com/twitchtv/twirp"
	"github.com/twitchtv/twirp/hooks/statsd"
)

type GetEmotesByCodesAPI struct {
	Clients       clients.Clients
	Stats         statsd.Statter
	DynamicConfig config.DynamicConfigClient
}

func (api *GetEmotesByCodesAPI) GetEmotesByCodes(ctx context.Context, r *mk.GetEmotesByCodesRequest) (*mk.GetEmotesByCodesResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, 500*time.Millisecond)
	defer cancel()

	defer func(start time.Time) {
		dur := time.Since(start)
		if dur > 150*time.Millisecond {
			logrus.WithFields(logrus.Fields{
				"userID":    r.GetOwnerId(),
				"codes":     r.GetCodes(),
				"channelID": r.GetChannelId(),
			}).Infof("Slow GetEmotesByCodes request, duration: %s", dur.String())
		}
	}(time.Now())

	if len(r.GetCodes()) == 0 {
		return &mk.GetEmotesByCodesResponse{}, nil
	}

	start := time.Now()

	emotes, err := mako.GetEmotesByCodes(ctx, mako.GetEmotesByCodesParams{
		EmoteDB:          api.Clients.EmoteDB,
		EntitlementDB:    api.Clients.MultiSourceEntitlementDB,
		EmoteModifiersDB: api.Clients.EmoteModifiersDB,
		Dynamic:          api.DynamicConfig,
		UserID:           r.GetOwnerId(),
		ChannelID:        r.GetChannelId(),
		Codes:            r.GetCodes(),
	})
	if err != nil {
		logrus.WithError(err).Errorf("failed to get emotes by codes: (# of codes=%v) %v", len(r.GetCodes()), r.GetCodes())
		return nil, twirp.InternalErrorWith(errors.Wrap(err, "failed to get emotes by codes"))
	}

	twirpEmoticons := make([]*mk.Emoticon, 0, len(emotes))
	for _, emote := range emotes {
		twirpEmote := emote.ToTwirp()
		twirpEmoticons = append(twirpEmoticons, &twirpEmote)
	}

	api.Stats.TimingDuration("test.GetEmotesByCodes.latencies", time.Since(start), 1.0)

	return &mk.GetEmotesByCodesResponse{
		Emoticons: twirpEmoticons,
	}, nil
}
