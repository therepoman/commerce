package api

import (
	"context"
	"testing"

	"code.justin.tv/commerce/mako/clients"
	mako "code.justin.tv/commerce/mako/internal"
	"code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/internal_mock"
	mk "code.justin.tv/commerce/mako/twirp"
	"github.com/pkg/errors"
	"github.com/segmentio/ksuid"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestSetUserEmoteStandingOverrideAPI(t *testing.T) {
	Convey("Test GetUserEmoteStanding API", t, func() {
		mockEmoteStandingOverridesDB := new(internal_mock.EmoteStandingOverridesDB)

		setUserEmoteStandingOverrideAPI := SetUserEmoteStandingOverrideAPI{
			Clients: clients.Clients{
				EmoteStandingOverridesDB: mockEmoteStandingOverridesDB,
			},
		}

		ctx := context.Background()

		Convey("with no overrides provided", func() {
			req := mk.SetUserEmoteStandingOverridesRequest{Overrides: []*mk.UserEmoteStandingOverride{}}

			resp, err := setUserEmoteStandingOverrideAPI.SetUserEmoteStandingOverrides(ctx, &req)
			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("with no user_id provided on the override", func() {
			req := mk.SetUserEmoteStandingOverridesRequest{
				Overrides: []*mk.UserEmoteStandingOverride{
					{
						UserId: "",
					},
				},
			}

			resp, err := setUserEmoteStandingOverrideAPI.SetUserEmoteStandingOverrides(ctx, &req)
			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("with no override provided on the user's override", func() {
			req := mk.SetUserEmoteStandingOverridesRequest{
				Overrides: []*mk.UserEmoteStandingOverride{
					{
						UserId:   userID,
						Override: mk.EmoteStandingOverride(100000),
					},
				},
			}

			resp, err := setUserEmoteStandingOverrideAPI.SetUserEmoteStandingOverrides(ctx, &req)
			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("with too many overrides in the request", func() {
			override1 := mk.UserEmoteStandingOverride{
				UserId:   ksuid.New().String(),
				Override: mk.EmoteStandingOverride_InEmoteGoodStanding,
			}

			req := mk.SetUserEmoteStandingOverridesRequest{
				Overrides: []*mk.UserEmoteStandingOverride{
					&override1,
					&override1,
					&override1,
					&override1,
					&override1,
					&override1,
					&override1,
					&override1,
					&override1,
					&override1,
					&override1,
					&override1,
					&override1,
					&override1,
					&override1,
					&override1,
					&override1,
					&override1,
					&override1,
					&override1,
					&override1,
					&override1,
					&override1,
					&override1,
					&override1,
					&override1,
				},
			}

			resp, err := setUserEmoteStandingOverrideAPI.SetUserEmoteStandingOverrides(ctx, &req)
			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("if the dynamoDB returns an error", func() {
			req := mk.SetUserEmoteStandingOverridesRequest{
				Overrides: []*mk.UserEmoteStandingOverride{
					{
						UserId:   ksuid.New().String(),
						Override: mk.EmoteStandingOverride_InEmoteGoodStanding,
					},
				},
			}

			mockEmoteStandingOverridesDB.On("UpdateOverrides", mock.Anything, mock.Anything).Return(nil, errors.New("test error"))

			resp, err := setUserEmoteStandingOverrideAPI.SetUserEmoteStandingOverrides(ctx, &req)
			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("if the dynamoDB only processes some overrides", func() {
			unprocessedUserID := ksuid.New().String()
			override1 := mk.UserEmoteStandingOverride{
				UserId:   ksuid.New().String(),
				Override: mk.EmoteStandingOverride_InEmoteGoodStanding,
			}
			override2 := mk.UserEmoteStandingOverride{
				UserId:   ksuid.New().String(),
				Override: mk.EmoteStandingOverride_NotInEmoteGoodStanding,
			}
			override3 := mk.UserEmoteStandingOverride{
				UserId:   ksuid.New().String(),
				Override: mk.EmoteStandingOverride_InEmoteGoodStanding,
			}
			override4 := mk.UserEmoteStandingOverride{
				UserId:   unprocessedUserID,
				Override: mk.EmoteStandingOverride_NotInEmoteGoodStanding,
			}
			override5 := mk.UserEmoteStandingOverride{
				UserId:   ksuid.New().String(),
				Override: mk.EmoteStandingOverride_InEmoteGoodStanding,
			}

			req := mk.SetUserEmoteStandingOverridesRequest{
				Overrides: []*mk.UserEmoteStandingOverride{
					&override1,
					&override2,
					&override3,
					&override4,
					&override5,
				},
			}

			mockEmoteStandingOverridesDB.On("UpdateOverrides",
				mock.Anything,
				mock.Anything,
				mock.Anything,
				mock.Anything,
				mock.Anything,
				mock.Anything).
				Return([]mako.UserEmoteStandingOverride{
					{
						UserID:   unprocessedUserID,
						Override: mako.NotInEmoteGoodStanding,
					},
				}, nil)

			resp, err := setUserEmoteStandingOverrideAPI.SetUserEmoteStandingOverrides(ctx, &req)
			So(err, ShouldBeNil)
			So(resp, ShouldNotBeNil)
			So(resp.GetFailedOverrides(), ShouldNotBeNil)
			So(len(resp.GetFailedOverrides()), ShouldEqual, 1)
			So(resp.GetFailedOverrides()[0], ShouldNotBeNil)
			So(resp.GetFailedOverrides()[0].GetUserId(), ShouldEqual, unprocessedUserID)
		})

		Convey("if the dynamoDB processes all overrides", func() {
			override1 := mk.UserEmoteStandingOverride{
				UserId:   ksuid.New().String(),
				Override: mk.EmoteStandingOverride_InEmoteGoodStanding,
			}
			override2 := mk.UserEmoteStandingOverride{
				UserId:   ksuid.New().String(),
				Override: mk.EmoteStandingOverride_NotInEmoteGoodStanding,
			}
			override3 := mk.UserEmoteStandingOverride{
				UserId:   ksuid.New().String(),
				Override: mk.EmoteStandingOverride_InEmoteGoodStanding,
			}
			override4 := mk.UserEmoteStandingOverride{
				UserId:   ksuid.New().String(),
				Override: mk.EmoteStandingOverride_NotInEmoteGoodStanding,
			}
			override5 := mk.UserEmoteStandingOverride{
				UserId:   ksuid.New().String(),
				Override: mk.EmoteStandingOverride_InEmoteGoodStanding,
			}

			req := mk.SetUserEmoteStandingOverridesRequest{
				Overrides: []*mk.UserEmoteStandingOverride{
					&override1,
					&override2,
					&override3,
					&override4,
					&override5,
				},
			}

			mockEmoteStandingOverridesDB.On("UpdateOverrides",
				mock.Anything,
				mock.Anything,
				mock.Anything,
				mock.Anything,
				mock.Anything,
				mock.Anything).
				Return(nil, nil)

			resp, err := setUserEmoteStandingOverrideAPI.SetUserEmoteStandingOverrides(ctx, &req)
			So(err, ShouldBeNil)
			So(resp, ShouldNotBeNil)
			So(len(resp.GetFailedOverrides()), ShouldBeZeroValue)
		})
	})
}
