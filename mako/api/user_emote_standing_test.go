package api

import (
	"context"
	"testing"

	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/mako/clients"
	"code.justin.tv/commerce/mako/clients/ripley"
	autoapproval_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/autoapproval"
	clients_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/web/users-service/client/usersclient_internal"
	mk "code.justin.tv/commerce/mako/twirp"
	"code.justin.tv/web/users-service/models"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestUserEmoteStandingAPI(t *testing.T) {
	Convey("Test GetUserEmoteStanding API", t, func() {
		mockAutoApprovalClient := new(autoapproval_mock.EligibilityFetcher)
		mockUserServiceClient := new(clients_mock.InternalClient)

		userEmoteStandingAPI := UserEmoteStandingAPI{
			Clients: clients.Clients{
				AutoApprovalEligibilityFetcher: mockAutoApprovalClient,
				UserServiceClient:              mockUserServiceClient,
			},
		}

		ctx := context.Background()

		Convey("with no userID provided", func() {
			req := mk.GetUserEmoteStandingRequest{UserId: ""}

			resp, err := userEmoteStandingAPI.GetUserEmoteStanding(ctx, &req)
			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("with no requestingUserID provided", func() {
			req := mk.GetUserEmoteStandingRequest{UserId: "12345", RequestingUserId: ""}

			resp, err := userEmoteStandingAPI.GetUserEmoteStanding(ctx, &req)
			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("with a partner in good standing", func() {
			userID := "12345"
			mockAutoApprovalClient.On("IsUserEligibleForEmoteUploadAutoApproval", mock.Anything, userID).Return(true, ripley.TraditionalPartnerType, nil)

			req := mk.GetUserEmoteStandingRequest{
				UserId:           userID,
				RequestingUserId: userID,
			}

			resp, err := userEmoteStandingAPI.GetUserEmoteStanding(ctx, &req)
			So(resp, ShouldNotBeNil)
			So(resp.IsInEmoteGoodStanding, ShouldBeTrue)
			So(err, ShouldBeNil)
		})

		Convey("with a partner not in good standing", func() {
			userID := "23456"
			mockAutoApprovalClient.On("IsUserEligibleForEmoteUploadAutoApproval", mock.Anything, userID).Return(false, ripley.TraditionalPartnerType, nil)

			req := mk.GetUserEmoteStandingRequest{
				UserId:           userID,
				RequestingUserId: userID,
			}

			resp, err := userEmoteStandingAPI.GetUserEmoteStanding(ctx, &req)
			So(resp, ShouldNotBeNil)
			So(resp.IsInEmoteGoodStanding, ShouldBeFalse)
			So(err, ShouldBeNil)
		})

		Convey("with an affiliate in good standing", func() {
			userID := "34567"
			mockAutoApprovalClient.On("IsUserEligibleForEmoteUploadAutoApproval", mock.Anything, userID).Return(true, ripley.AffiliatePartnerType, nil)

			req := mk.GetUserEmoteStandingRequest{
				UserId:           userID,
				RequestingUserId: userID,
			}

			resp, err := userEmoteStandingAPI.GetUserEmoteStanding(ctx, &req)
			So(resp, ShouldNotBeNil)
			So(resp.IsInEmoteGoodStanding, ShouldBeTrue)
			So(err, ShouldBeNil)
		})

		Convey("with an affiliate not in good standing", func() {
			userID := "45678"
			mockAutoApprovalClient.On("IsUserEligibleForEmoteUploadAutoApproval", mock.Anything, userID).Return(false, ripley.AffiliatePartnerType, nil)

			req := mk.GetUserEmoteStandingRequest{
				UserId:           userID,
				RequestingUserId: userID,
			}

			resp, err := userEmoteStandingAPI.GetUserEmoteStanding(ctx, &req)
			So(resp, ShouldNotBeNil)
			So(resp.IsInEmoteGoodStanding, ShouldBeFalse)
			So(err, ShouldBeNil)
		})

		Convey("with an error looking up standing status", func() {
			userID := "56789"
			mockAutoApprovalClient.On("IsUserEligibleForEmoteUploadAutoApproval", mock.Anything, userID).Return(false, ripley.UnknownPartnerType, errors.New("test error"))

			req := mk.GetUserEmoteStandingRequest{
				UserId:           userID,
				RequestingUserId: userID,
			}

			resp, err := userEmoteStandingAPI.GetUserEmoteStanding(ctx, &req)
			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("with a staff member looking up another user", func() {
			userID := "45678"
			requestingUserID := "56789"
			mockUserServiceClient.On("GetUserByID", mock.Anything, requestingUserID, mock.Anything).Return(&models.Properties{
				Login: pointers.StringP("thereallittlemistershadowgamer"),
				ID:    requestingUserID,
				Admin: pointers.BoolP(true),
			}, nil)
			mockAutoApprovalClient.On("IsUserEligibleForEmoteUploadAutoApproval", mock.Anything, userID).Return(false, ripley.AffiliatePartnerType, nil)

			req := mk.GetUserEmoteStandingRequest{
				UserId:           userID,
				RequestingUserId: requestingUserID,
			}

			resp, err := userEmoteStandingAPI.GetUserEmoteStanding(ctx, &req)
			So(resp, ShouldNotBeNil)
			So(resp.IsInEmoteGoodStanding, ShouldBeFalse)
			So(err, ShouldBeNil)
		})

		Convey("with a non-staff member looking up another user", func() {
			userID := "45678"
			requestingUserID := "56789"
			mockUserServiceClient.On("GetUserByID", mock.Anything, requestingUserID, mock.Anything).Return(&models.Properties{
				Login: pointers.StringP("anyunrealbigmisseslightwatcher"),
				ID:    requestingUserID,
				Admin: pointers.BoolP(false),
			}, nil)
			mockAutoApprovalClient.On("IsUserEligibleForEmoteUploadAutoApproval", mock.Anything, userID).Return(false, ripley.AffiliatePartnerType, nil)

			req := mk.GetUserEmoteStandingRequest{
				UserId:           userID,
				RequestingUserId: requestingUserID,
			}

			resp, err := userEmoteStandingAPI.GetUserEmoteStanding(ctx, &req)
			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("with an error looking up whether the requesting user is staff", func() {
			userID := "45678"
			requestingUserID := "56789"
			mockUserServiceClient.On("GetUserByID", mock.Anything, requestingUserID, mock.Anything).Return(nil, errors.New("test error"))
			mockAutoApprovalClient.On("IsUserEligibleForEmoteUploadAutoApproval", mock.Anything, userID).Return(false, ripley.AffiliatePartnerType, nil)

			req := mk.GetUserEmoteStandingRequest{
				UserId:           userID,
				RequestingUserId: requestingUserID,
			}

			resp, err := userEmoteStandingAPI.GetUserEmoteStanding(ctx, &req)
			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})
	})
}
