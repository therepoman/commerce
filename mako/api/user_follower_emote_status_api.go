package api

import (
	"context"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/mako/config"
	"code.justin.tv/common/twirp"

	"code.justin.tv/commerce/mako/clients"
	mkd "code.justin.tv/commerce/mako/twirp/dashboard"
)

type GetUserFollowerEmoteStatusAPI struct {
	Clients       clients.Clients
	DynamicConfig config.DynamicConfigClient
}

func (ufes *GetUserFollowerEmoteStatusAPI) GetUserFollowerEmoteStatus(ctx context.Context, r *mkd.GetUserFollowerEmoteStatusRequest) (*mkd.GetUserFollowerEmoteStatusResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, 1500*time.Millisecond) //1.5 second to match GQL timeout
	defer cancel()

	userID := r.GetUserId()
	if userID == "" {
		return nil, twirp.RequiredArgumentError("user_id")
	}

	inExperiment := ufes.DynamicConfig.IsInFreemotesExperiment(userID)

	if !inExperiment {
		return &mkd.GetUserFollowerEmoteStatusResponse{
			Status: mkd.FollowerEmoteDashboardStatus_NotAllowed,
		}, nil
	}

	isInEmoteGoodStanding, _, err := ufes.Clients.AutoApprovalEligibilityFetcher.IsUserEligibleForEmoteUploadAutoApproval(ctx, userID)
	if err != nil {
		msg := "error fetching emote auto-approval eligibility"
		log.WithError(err).WithField("userID", userID).Error(msg)
		return nil, twirp.InternalError(msg)
	}

	if !isInEmoteGoodStanding {
		return &mkd.GetUserFollowerEmoteStatusResponse{
			Status: mkd.FollowerEmoteDashboardStatus_NoUpload,
		}, nil
	}

	return &mkd.GetUserFollowerEmoteStatusResponse{
		Status: mkd.FollowerEmoteDashboardStatus_Allowed,
	}, nil
}
