package api

import (
	"context"
	"fmt"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/mako/clients"
	mako "code.justin.tv/commerce/mako/internal"
	mk "code.justin.tv/commerce/mako/twirp"
	"github.com/segmentio/ksuid"
	"github.com/twitchtv/twirp"
)

const (
	emoteModGroupIDPrefix = "emote-mod-group.%v"
)

type EmoteModifierGroupsAPIs struct {
	Clients clients.Clients
}

func (api *EmoteModifierGroupsAPIs) CreateEmoteModifierGroups(ctx context.Context, req *mk.CreateEmoteModifierGroupsRequest) (*mk.CreateEmoteModifierGroupsResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, 1*time.Second)
	defer cancel()

	if len(req.Groups) == 0 {
		return nil, twirp.RequiredArgumentError("groups")
	}

	groups := make([]mako.EmoteModifierGroup, 0, len(req.Groups))
	for _, g := range req.Groups {
		if g.Id != "" {
			return nil, twirp.InvalidArgumentError("id", "must not be set for create")
		}
		if g.OwnerId == "" {
			return nil, twirp.RequiredArgumentError("owner_id")
		}
		// TODO: validate that incoming modifiers are not seasonal. See: https://jira.twitch.com/browse/SUBS-5459
		group := mako.TwirpToEmoteModifierGroup(g)
		// we need to generate an ID when creating a group.
		group.ID = fmt.Sprintf(emoteModGroupIDPrefix, ksuid.New().String())
		groups = append(groups, group)
	}

	created, err := api.Clients.EmoteModifiersDB.CreateGroups(ctx, groups...)
	if err != nil {
		msg := "failed to create emote modifier groups"
		log.WithField("emote_modifier_groups", req.String()).WithError(err).Error(msg)
		return nil, twirp.InternalError(msg)
	}

	return &mk.CreateEmoteModifierGroupsResponse{
		Groups: mako.EmoteModifierGroupsToTwirp(created...),
	}, nil
}

func (api *EmoteModifierGroupsAPIs) UpdateEmoteModifierGroups(ctx context.Context, req *mk.UpdateEmoteModifierGroupsRequest) (*mk.UpdateEmoteModifierGroupsResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, 1*time.Second)
	defer cancel()

	if len(req.Groups) == 0 {
		return nil, twirp.RequiredArgumentError("groups")
	}

	groups := make([]mako.EmoteModifierGroup, 0, len(req.Groups))
	for _, g := range req.Groups {
		if g.Id == "" {
			return nil, twirp.RequiredArgumentError("id")
		}

		for _, m := range g.Modifiers {
			if !mako.IsPermanentModifier(m) {
				return nil, twirp.InvalidArgumentError("modifiers", "must be permanent modifiers")
			}
		}

		group := mako.TwirpToEmoteModifierGroup(g)
		groups = append(groups, group)
	}

	updated, err := api.Clients.EmoteModifiersDB.UpdateGroups(ctx, groups...)
	if err != nil {
		msg := "failed to update emote modifier groups"
		log.WithField("emote_modifier_groups", req.String()).WithError(err).Error(msg)
		return nil, twirp.InternalError(msg)
	}

	return &mk.UpdateEmoteModifierGroupsResponse{
		Groups: mako.EmoteModifierGroupsToTwirp(updated...),
	}, nil
}

func (api *EmoteModifierGroupsAPIs) GetEmoteModifierGroups(ctx context.Context, req *mk.GetEmoteModifierGroupsRequest) (*mk.GetEmoteModifierGroupsResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, 1*time.Second)
	defer cancel()

	if len(req.ModifierGroupIds) == 0 {
		return nil, twirp.RequiredArgumentError("modifier_group_ids")
	}

	groups, err := api.Clients.EmoteModifiersDB.ReadByIDs(ctx, req.ModifierGroupIds...)
	if err != nil {
		msg := "failed to get emote modifier groups by id"
		log.WithField("modifier_group_ids", req.GetModifierGroupIds()).WithError(err).Error(msg)
		return nil, twirp.InternalError(msg)
	}

	return &mk.GetEmoteModifierGroupsResponse{
		Groups: mako.EmoteModifierGroupsToTwirp(groups...),
	}, nil
}

func (api *EmoteModifierGroupsAPIs) GetEmoteModifierGroupsByOwnerId(ctx context.Context, req *mk.GetEmoteModifierGroupsByOwnerIdRequest) (*mk.GetEmoteModifierGroupsByOwnerIdResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, 1*time.Second)
	defer cancel()

	if req.OwnerId == "" {
		return nil, twirp.RequiredArgumentError("owner_id")
	}

	groups, err := api.Clients.EmoteModifiersDB.ReadByOwnerIDs(ctx, req.OwnerId)
	if err != nil {
		msg := "failed to get emote modifier groups by owner id"
		log.WithField("owner_id", req.GetOwnerId()).WithError(err).Error(msg)
		return nil, twirp.InternalError(msg)
	}

	return &mk.GetEmoteModifierGroupsByOwnerIdResponse{
		Groups: mako.EmoteModifierGroupsToTwirp(groups...),
	}, nil
}

func (api *EmoteModifierGroupsAPIs) DeleteEmoteModifierGroups(ctx context.Context, req *mk.DeleteEmoteModifierGroupsRequest) (*mk.DeleteEmoteModifierGroupsResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, 1*time.Second)
	defer cancel()

	if len(req.ModifierGroupIds) == 0 {
		return nil, twirp.RequiredArgumentError("modifier_group_ids")
	}

	err := api.Clients.EmoteModifiersDB.DeleteByIDs(ctx, req.ModifierGroupIds...)
	if err != nil {
		msg := "failed to delete emote modifier groups"
		log.WithField("modifier_group_ids", req.GetModifierGroupIds()).WithError(err).Error(msg)
		return nil, twirp.InternalError(msg)
	}

	return &mk.DeleteEmoteModifierGroupsResponse{}, nil
}
