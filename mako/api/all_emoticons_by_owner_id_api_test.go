package api

import (
	"context"
	"testing"

	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/mako/clients"
	mako "code.justin.tv/commerce/mako/internal"
	subscriptions_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/subscriptions"
	internal_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/internal_mock"
	clients_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/web/users-service/client/usersclient_internal"
	mk "code.justin.tv/commerce/mako/twirp"
	substwirp "code.justin.tv/revenue/subscriptions/twirp"
	"code.justin.tv/web/users-service/models"
	"github.com/golang/protobuf/proto"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
	"github.com/twitchtv/twirp"
)

func TestGetAllEmotesByOwnerId(t *testing.T) {
	Convey("Given EmoticonsByOwnerAPI", t, func() {
		mockEmoteDB := new(internal_mock.EmoteDB)
		mockUserServiceClient := new(clients_mock.InternalClient)
		testChannelID := "pagliacci001"

		emoticonsByOwnerId := EmoticonsByOwnerAPI{
			Clients: clients.Clients{
				UserServiceClient: mockUserServiceClient,
			},
			EmoteDB: mockEmoteDB,
		}

		Convey("when owner id is empty", func() {
			req := mk.GetAllEmotesByOwnerIdRequest{}

			Convey("it should return an error", func() {
				resp, err := emoticonsByOwnerId.GetAllEmotesByOwnerId(context.Background(), &req)

				So(resp, ShouldBeNil)
				So(err, ShouldNotBeNil)
				tErr, isTErr := err.(twirp.Error)
				So(isTErr, ShouldBeTrue)
				So(tErr.Code(), ShouldEqual, twirp.InvalidArgument)
			})
		})

		Convey("when domain is invalid", func() {
			req := mk.GetAllEmotesByOwnerIdRequest{
				OwnerId: testChannelID,
				Filter: &mk.EmoteFilter{
					Domain: 100,
				},
			}

			Convey("it should return an error", func() {
				resp, err := emoticonsByOwnerId.GetAllEmotesByOwnerId(context.Background(), &req)

				So(resp, ShouldBeNil)
				So(err, ShouldNotBeNil)
				tErr, isTErr := err.(twirp.Error)
				So(isTErr, ShouldBeTrue)
				So(tErr.Code(), ShouldEqual, twirp.InvalidArgument)
			})
		})

		Convey("when state is invalid", func() {
			req := mk.GetAllEmotesByOwnerIdRequest{
				OwnerId: testChannelID,
				Filter: &mk.EmoteFilter{
					Domain: mk.Domain_emotes,
					States: []mk.EmoteState{
						100,
					},
				},
			}

			Convey("it should return an error", func() {
				resp, err := emoticonsByOwnerId.GetAllEmotesByOwnerId(context.Background(), &req)

				So(resp, ShouldBeNil)
				So(err, ShouldNotBeNil)
				tErr, isTErr := err.(twirp.Error)
				So(isTErr, ShouldBeTrue)
				So(tErr.Code(), ShouldEqual, twirp.InvalidArgument)
			})
		})

		Convey("when auth is required but the requesting user is missing", func() {
			req := mk.GetAllEmotesByOwnerIdRequest{
				OwnerId: testChannelID,
				Filter: &mk.EmoteFilter{
					Domain: mk.Domain_emotes,
					States: []mk.EmoteState{
						100,
					},
				},
				EnforceOwnerRestriction: true,
			}

			Convey("it should return an error", func() {
				resp, err := emoticonsByOwnerId.GetAllEmotesByOwnerId(context.Background(), &req)

				So(resp, ShouldBeNil)
				So(err, ShouldNotBeNil)
				tErr, isTErr := err.(twirp.Error)
				So(isTErr, ShouldBeTrue)
				So(tErr.Code(), ShouldEqual, twirp.InvalidArgument)
			})
		})

		Convey("when the user is not permitted", func() {
			requestingUserID := "snooping_user"
			req := mk.GetAllEmotesByOwnerIdRequest{
				OwnerId: testChannelID,
				Filter: &mk.EmoteFilter{
					Domain: mk.Domain_emotes,
					States: []mk.EmoteState{
						100,
					},
				},
				EnforceOwnerRestriction: true,
				RequestingUserId:        requestingUserID,
			}
			mockUserServiceClient.On("GetUserByID", mock.Anything, requestingUserID, mock.Anything).Return(&models.Properties{
				Login: pointers.StringP("the_twitch_spy"),
				ID:    requestingUserID,
				Admin: pointers.BoolP(false),
			}, nil)

			Convey("it should return an error", func() {
				resp, err := emoticonsByOwnerId.GetAllEmotesByOwnerId(context.Background(), &req)

				So(resp, ShouldBeNil)
				So(err, ShouldNotBeNil)
				tErr, isTErr := err.(twirp.Error)
				So(isTErr, ShouldBeTrue)
				So(tErr.Code(), ShouldEqual, twirp.PermissionDenied)
			})
		})

		Convey("when request is valid", func() {
			req := mk.GetAllEmotesByOwnerIdRequest{
				OwnerId: testChannelID,
				Filter: &mk.EmoteFilter{
					Domain: mk.Domain_emotes,
					States: []mk.EmoteState{
						mk.EmoteState_active,
					},
				},
			}

			Convey("when EmotesDAO errors", func() {
				mockEmoteDB.On("Query", mock.Anything, mock.Anything).Return([]mako.Emote{}, errors.New("you are pagliaccied"))

				Convey("it should return an error", func() {
					resp, err := emoticonsByOwnerId.GetAllEmotesByOwnerId(context.Background(), &req)

					So(resp, ShouldBeNil)
					So(err, ShouldNotBeNil)
					tErr, isTErr := err.(twirp.Error)
					So(isTErr, ShouldBeTrue)
					So(tErr.Code(), ShouldEqual, twirp.Internal)
				})
			})

			Convey("When EmotesDAO doesn't error", func() {
				mockEmoteDB.On("Query", mock.Anything, mock.Anything).Return([]mako.Emote{
					{
						ID:      "123457",
						GroupID: "emote-tier-1",
						Code:    "pagPepperoni",
						Domain:  "emotes",
						State:   "active",
					},
					{
						ID:      "123458",
						GroupID: "emote-tier-1",
						Code:    "pagPineapple",
						Domain:  "emotes",
						State:   "active",
					},
				}, nil)

				Convey("it should return the emotes", func() {
					resp, err := emoticonsByOwnerId.GetAllEmotesByOwnerId(context.Background(), &req)

					So(err, ShouldBeNil)
					So(resp, ShouldNotBeNil)
					So(len(resp.Emoticons), ShouldEqual, 2)
				})
			})
		})

		Convey("when auth is required and the user is permitted", func() {
			mockEmoteDB.On("Query", mock.Anything, mock.Anything).Return([]mako.Emote{
				{
					ID:      "123457",
					GroupID: "emote-tier-1",
					Code:    "pagPepperoni",
					Domain:  "emotes",
					State:   "active",
				},
				{
					ID:      "123458",
					GroupID: "emote-tier-1",
					Code:    "pagPineapple",
					Domain:  "emotes",
					State:   "active",
				},
			}, nil)

			Convey("because the user is staff", func() {
				requestingUserID := "paragon_of_twitch_values"
				req := mk.GetAllEmotesByOwnerIdRequest{
					OwnerId: testChannelID,
					Filter: &mk.EmoteFilter{
						Domain: mk.Domain_emotes,
						States: []mk.EmoteState{
							mk.EmoteState_active,
						},
					},
					EnforceOwnerRestriction: true,
					RequestingUserId:        requestingUserID,
				}
				mockUserServiceClient.On("GetUserByID", mock.Anything, requestingUserID, mock.Anything).Return(&models.Properties{
					Login: pointers.StringP("glitch"),
					ID:    requestingUserID,
					Admin: pointers.BoolP(true),
				}, nil)

				Convey("it should return the emotes", func() {
					resp, err := emoticonsByOwnerId.GetAllEmotesByOwnerId(context.Background(), &req)

					So(err, ShouldBeNil)
					So(resp, ShouldNotBeNil)
					So(len(resp.Emoticons), ShouldEqual, 2)
				})
			})

			Convey("because the user is requesting their own emotes", func() {
				req := mk.GetAllEmotesByOwnerIdRequest{
					OwnerId: testChannelID,
					Filter: &mk.EmoteFilter{
						Domain: mk.Domain_emotes,
						States: []mk.EmoteState{
							mk.EmoteState_active,
						},
					},
					EnforceOwnerRestriction: true,
					RequestingUserId:        testChannelID,
				}

				Convey("it should return the emotes", func() {
					resp, err := emoticonsByOwnerId.GetAllEmotesByOwnerId(context.Background(), &req)

					So(err, ShouldBeNil)
					So(resp, ShouldNotBeNil)
					So(len(resp.Emoticons), ShouldEqual, 2)
				})
			})

		})

	})

	Convey("Given GetAllEmotesWithModificationsByOwnerID", t, func() {
		mockEmoteDB := new(internal_mock.EmoteDB)
		mockSubscriptionsClient := new(subscriptions_mock.ClientWrapper)

		emoticonsByOwnerId := EmoticonsByOwnerAPI{
			Clients: clients.Clients{
				SubscriptionsClient: mockSubscriptionsClient,
				EmoteDB:             mockEmoteDB,
			},
		}

		Convey("given owner id is empty", func() {
			req := mk.GetAllEmotesWithModificationsByOwnerIDRequest{}

			Convey("return an invalid argument error", func() {
				resp, err := emoticonsByOwnerId.GetAllEmotesWithModificationsByOwnerID(context.Background(), &req)

				So(resp, ShouldBeNil)
				So(err, ShouldNotBeNil)
				tErr, isTErr := err.(twirp.Error)
				So(isTErr, ShouldBeTrue)
				So(tErr.Code(), ShouldEqual, twirp.InvalidArgument)
			})
		})

		Convey("given a request with owner id populated", func() {
			req := mk.GetAllEmotesWithModificationsByOwnerIDRequest{
				OwnerId: "1234567890",
			}

			Convey("given subs returns an error", func() {
				mockSubscriptionsClient.On("GetChannelProducts", mock.Anything, mock.Anything).Return(nil, errors.New("dummy subs error"))

				Convey("return an internal error", func() {
					resp, err := emoticonsByOwnerId.GetAllEmotesWithModificationsByOwnerID(context.Background(), &req)

					So(resp, ShouldBeNil)
					So(err, ShouldNotBeNil)
					tErr, isTErr := err.(twirp.Error)
					So(isTErr, ShouldBeTrue)
					So(tErr.Code(), ShouldEqual, twirp.Internal)
				})
			})

			Convey("given subs returns no products", func() {
				mockSubscriptionsClient.On("GetChannelProducts", mock.Anything, req.OwnerId).Return(&substwirp.GetChannelProductsResponse{}, nil)

				Convey("return an empty list of emotes for each modification", func() {
					resp, err := emoticonsByOwnerId.GetAllEmotesWithModificationsByOwnerID(context.Background(), &req)

					So(err, ShouldBeNil)

					for _, modGroup := range resp.EmoteGroups {
						So(modGroup.Emotes, ShouldBeEmpty)
					}
				})
			})

			Convey("given subs returns products successfully", func() {
				testGroupId1 := "test-group-id-1"
				testGroupId2 := "test-group-id-2"
				testProductId1 := "test-product-id-1"
				testProductId2 := "test-product-id-2"

				mockSubscriptionsClient.On("GetChannelProducts", mock.Anything, req.OwnerId).Return(&substwirp.GetChannelProductsResponse{
					Products: []*substwirp.Product{
						{
							Id:            testProductId1,
							EmoticonSetId: testGroupId1,
						},
						{
							Id:            testProductId2,
							EmoticonSetId: testGroupId2,
						},
					},
				}, nil)

				Convey("given emote db returns an error", func() {
					mockEmoteDB.On("ReadByGroupIDs", mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("dummy emotedb error"))

					Convey("return an internal error", func() {
						resp, err := emoticonsByOwnerId.GetAllEmotesWithModificationsByOwnerID(context.Background(), &req)

						So(resp, ShouldBeNil)
						So(err, ShouldNotBeNil)
						tErr, isTErr := err.(twirp.Error)
						So(isTErr, ShouldBeTrue)
						So(tErr.Code(), ShouldEqual, twirp.Internal)
					})
				})

				Convey("given emote db returns one emote for each set returned from subs", func() {
					testEmote1 := mako.Emote{
						ID:          "1",
						OwnerID:     "267893713",
						GroupID:     testGroupId1,
						Prefix:      "wolf",
						Suffix:      "HOWL",
						Code:        "wolfHOWL",
						State:       "active",
						Domain:      "emotes",
						EmotesGroup: mk.EmoteGroup_Subscriptions.String(),
					}
					testEmote2 := mako.Emote{
						ID:          "2",
						OwnerID:     "267893713",
						GroupID:     testGroupId2,
						Prefix:      "wolf",
						Suffix:      "CUB",
						Code:        "wolfCUB",
						State:       "active",
						Domain:      "emotes",
						EmotesGroup: mk.EmoteGroup_Subscriptions.String(),
					}
					testEmote1Twirp := testEmote1.ToTwirp()
					testEmote1Twirp.ProductId = testProductId1
					testEmote2Twirp := testEmote2.ToTwirp()
					testEmote2Twirp.ProductId = testProductId2
					mockEmoteDB.On("ReadByGroupIDs", mock.Anything, testGroupId1, testGroupId2).Return([]mako.Emote{testEmote1, testEmote2}, nil)

					Convey("return a list of those emotes with all modifications and product ids populated from the subs products", func() {
						resp, err := emoticonsByOwnerId.GetAllEmotesWithModificationsByOwnerID(context.Background(), &req)

						So(resp, ShouldNotBeNil)
						So(err, ShouldBeNil)

						So(len(resp.EmoteGroups), ShouldEqual, len(mk.Modification_name))

						for _, modGroup := range resp.EmoteGroups {
							if modGroup.Modification == mk.Modification_ModificationNone {
								continue
							}

							So(len(modGroup.Emotes), ShouldEqual, 2)

							if modGroup.Emotes[0].BaseEmote.Id == testEmote1.ID {
								So(proto.Equal(modGroup.Emotes[0].BaseEmote, &testEmote1Twirp), ShouldBeTrue)
								So(modGroup.Emotes[0].ModifiedEmote, ShouldNotBeNil)

								So(modGroup.Emotes[0].ModifiedEmote.Id, ShouldEqual, testEmote1.ID+mako.GetModifierSuffix(modGroup.Modification))
								So(modGroup.Emotes[0].ModifiedEmote.Code, ShouldEqual, testEmote1.Code+mako.GetModifierSuffix(modGroup.Modification))

								So(proto.Equal(modGroup.Emotes[1].BaseEmote, &testEmote2Twirp), ShouldBeTrue)
								So(modGroup.Emotes[1].ModifiedEmote.Id, ShouldEqual, testEmote2.ID+mako.GetModifierSuffix(modGroup.Modification))
								So(modGroup.Emotes[1].ModifiedEmote.Code, ShouldEqual, testEmote2.Code+mako.GetModifierSuffix(modGroup.Modification))
							} else {
								So(proto.Equal(modGroup.Emotes[1].BaseEmote, &testEmote1Twirp), ShouldBeTrue)
								So(modGroup.Emotes[1].ModifiedEmote.Id, ShouldEqual, testEmote1.ID+mako.GetModifierSuffix(modGroup.Modification))
								So(modGroup.Emotes[1].ModifiedEmote.Code, ShouldEqual, testEmote1.Code+mako.GetModifierSuffix(modGroup.Modification))

								So(proto.Equal(modGroup.Emotes[0].BaseEmote, &testEmote2Twirp), ShouldBeTrue)
								So(modGroup.Emotes[0].ModifiedEmote.Id, ShouldEqual, testEmote2.ID+mako.GetModifierSuffix(modGroup.Modification))
								So(modGroup.Emotes[0].ModifiedEmote.Code, ShouldEqual, testEmote2.Code+mako.GetModifierSuffix(modGroup.Modification))
							}
						}
					})
				})

				Convey("given emote db returns a mix of active and non-active emotes", func() {
					testEmote1 := mako.Emote{
						ID:          "1",
						OwnerID:     "267893713",
						GroupID:     testGroupId1,
						Prefix:      "wolf",
						Suffix:      "HOWL",
						Code:        "wolfHOWL",
						State:       mako.Active,
						Domain:      "emotes",
						EmotesGroup: mk.EmoteGroup_Subscriptions.String(),
					}
					testEmote2 := mako.Emote{
						ID:          "2",
						OwnerID:     "267893713",
						GroupID:     testGroupId2,
						Prefix:      "wolf",
						Suffix:      "CUB",
						Code:        "wolfCUB",
						State:       mako.Pending,
						Domain:      "emotes",
						EmotesGroup: mk.EmoteGroup_Subscriptions.String(),
					}
					testEmote3 := mako.Emote{
						ID:          "3",
						OwnerID:     "267893713",
						GroupID:     testGroupId2,
						Prefix:      "wolf",
						Suffix:      "ALPHA",
						Code:        "wolfALPHA",
						State:       mako.Inactive,
						Domain:      "emotes",
						EmotesGroup: mk.EmoteGroup_Subscriptions.String(),
					}
					testEmote1Twirp := testEmote1.ToTwirp()
					testEmote1Twirp.ProductId = testProductId1
					mockEmoteDB.On("ReadByGroupIDs", mock.Anything, testGroupId1, testGroupId2).Return([]mako.Emote{testEmote1, testEmote2, testEmote3}, nil)

					Convey("return a list of only active emotes with all modifications and product ids populated from the subs products", func() {
						resp, err := emoticonsByOwnerId.GetAllEmotesWithModificationsByOwnerID(context.Background(), &req)

						So(resp, ShouldNotBeNil)
						So(err, ShouldBeNil)

						So(len(resp.EmoteGroups), ShouldEqual, len(mk.Modification_name))

						for _, modGroup := range resp.EmoteGroups {
							if modGroup.Modification == mk.Modification_ModificationNone {
								continue
							}

							So(len(modGroup.Emotes), ShouldEqual, 1)

							So(proto.Equal(modGroup.Emotes[0].BaseEmote, &testEmote1Twirp), ShouldBeTrue)
							So(modGroup.Emotes[0].ModifiedEmote, ShouldNotBeNil)

							So(modGroup.Emotes[0].ModifiedEmote.Id, ShouldEqual, testEmote1.ID+mako.GetModifierSuffix(modGroup.Modification))
							So(modGroup.Emotes[0].ModifiedEmote.Code, ShouldEqual, testEmote1.Code+mako.GetModifierSuffix(modGroup.Modification))
						}
					})
				})
			})
		})
	})
}
