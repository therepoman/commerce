package api

import (
	"context"
	"testing"

	"github.com/pkg/errors"

	config_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/config"
	mako_dashboard "code.justin.tv/commerce/mako/twirp/dashboard"

	"github.com/stretchr/testify/assert"
)

func TestDefaultEmoteImagesAPI_GetDefaultEmoteImages(t *testing.T) {
	t.Run("GIVEN a set of default emote images stored in dynamic config WHEN GetDefaultEmoteImages is called THEN expect default emote images response", func(t *testing.T) {
		//GIVEN
		mockDefaultConfig := new(config_mock.DefaultEmoteImagesDataConfigClient)
		getDefaultEmoteImagesAPI := &DefaultEmoteImagesAPI{
			DefaultEmoteImagesDataDynamicConfig: mockDefaultConfig,
		}
		//should expect exactly one call to GetDefaultEmoteImagesData
		expectedResponse := &mako_dashboard.GetDefaultEmoteImagesResponse{
			DefaultEmoteImages: []*mako_dashboard.DefaultEmoteImage{},
		}
		mockDefaultConfig.On("GetDefaultEmoteImagesData").Return(expectedResponse.DefaultEmoteImages, nil).Once()

		//WHEN
		resp, err := getDefaultEmoteImagesAPI.GetDefaultEmoteImages(context.TODO(), &mako_dashboard.GetDefaultEmoteImagesRequest{})

		//THEN
		assert.Nil(t, err)
		assert.Equal(t, expectedResponse, resp)
	})

	t.Run("GIVEN a set of default emote images stored in dynamic config WHEN GetDefaultEmoteImages returns an error THEN expect an error", func(t *testing.T) {
		//GIVEN
		mockDefaultConfig := new(config_mock.DefaultEmoteImagesDataConfigClient)
		getDefaultEmoteImagesAPI := &DefaultEmoteImagesAPI{
			DefaultEmoteImagesDataDynamicConfig: mockDefaultConfig,
		}
		//should expect exactly one call to GetDefaultEmoteImagesData
		expectedResponse := &mako_dashboard.GetDefaultEmoteImagesResponse{
			DefaultEmoteImages: []*mako_dashboard.DefaultEmoteImage{},
		}
		mockDefaultConfig.On("GetDefaultEmoteImagesData").Return(nil, errors.New("test error")).Once()

		//WHEN
		resp, err := getDefaultEmoteImagesAPI.GetDefaultEmoteImages(context.TODO(), &mako_dashboard.GetDefaultEmoteImagesRequest{})

		//THEN
		assert.NotNil(t, err)
		assert.NotEqual(t, expectedResponse, resp)
	})
}
