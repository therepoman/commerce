package api

import (
	"context"
	"testing"

	"code.justin.tv/commerce/mako/clients"
	mako "code.justin.tv/commerce/mako/internal"
	"code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/internal_mock"
	stats_mock "code.justin.tv/commerce/mako/mocks/github.com/cactus/go-statsd-client/statsd"
	mk "code.justin.tv/commerce/mako/twirp"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestEmoticonsByGroupAPI(t *testing.T) {
	Convey("Test Emoticons By Group API", t, func() {
		mockEmoteDB := new(internal_mock.EmoteDB)
		mockStats := new(stats_mock.Statter)

		testGlobalEmoteID := "sampleGlobal"
		testClearedGlobals := []*mk.Emoticon{{Id: testGlobalEmoteID}}
		fakeClearedGlobals := mako.MakeClearedGlobalsForTesting(testClearedGlobals)

		emoteByGroupsAPI := EmoticonsByGroupsAPI{
			Clients: clients.Clients{
				EmoteDB:             mockEmoteDB,
				ClearedGlobalsCache: fakeClearedGlobals,
			},
			Stats: mockStats,
		}

		mockStats.On("TimingDuration", mock.Anything, mock.Anything, mock.Anything).Return(nil)

		Convey("Test GetEmoticonsByGroups API", func() {
			Convey("Given a group Id slice that contains the cleared globals id, don't call emote db", func() {
				testIDs := []string{clearedGlobalsGroup}

				request := &mk.GetEmoticonsByGroupsRequest{EmoticonGroupKeys: testIDs}
				resp, err := emoteByGroupsAPI.GetEmoticonsByGroups(context.Background(), request)

				So(err, ShouldBeNil)
				So(len(resp.Groups), ShouldEqual, 1)
				So(resp.Groups[0].Id, ShouldEqual, clearedGlobalsGroup)
				So(resp.Groups[0].Emoticons[0].Id, ShouldEqual, testGlobalEmoteID)
				mockEmoteDB.AssertNotCalled(t, "ReadByGroupIDs", mock.Anything, mock.Anything)
			})

			Convey("Given a group Id slice that contains empty strings, filter these out before calling emote db", func() {
				testGroupId := "500000001"
				testIdsWithEmptyStrings := []string{testGroupId, "", ""}
				mockEmoteDB.On("ReadByGroupIDs", mock.Anything, testGroupId).Return(nil, nil)

				request := &mk.GetEmoticonsByGroupsRequest{EmoticonGroupKeys: testIdsWithEmptyStrings}
				_, err := emoteByGroupsAPI.GetEmoticonsByGroups(context.Background(), request)

				So(err, ShouldBeNil)
				mockEmoteDB.AssertCalled(t, "ReadByGroupIDs", mock.Anything, testGroupId)
			})

			Convey("Given a group Id slice that contains duplicates, filter these out before calling emote db", func() {
				testGroupId := "500000001"
				testIdsWithDuplicates := []string{testGroupId, testGroupId}
				mockEmoteDB.On("ReadByGroupIDs", mock.Anything, testGroupId).Return(nil, nil)

				request := &mk.GetEmoticonsByGroupsRequest{EmoticonGroupKeys: testIdsWithDuplicates}
				_, err := emoteByGroupsAPI.GetEmoticonsByGroups(context.Background(), request)

				So(err, ShouldBeNil)
				mockEmoteDB.AssertCalled(t, "ReadByGroupIDs", mock.Anything, testGroupId)
			})

			Convey("Given emote db returns error", func() {
				groupID := "500000001"
				mockEmoteDB.On("ReadByGroupIDs", mock.Anything, mock.Anything).Return(nil, errors.New("dummy emotedb error"))

				Convey("then return internal error", func() {
					request := &mk.GetEmoticonsByGroupsRequest{EmoticonGroupKeys: []string{groupID}}
					resp, err := emoteByGroupsAPI.GetEmoticonsByGroups(context.Background(), request)

					So(resp, ShouldBeNil)
					So(err, ShouldNotBeNil)
					So(err.Error(), ShouldContainSubstring, "twirp error internal")
				})
			})

			Convey("Given successful emote group fetch from emotedb with all active emotes", func() {
				queriedIds := []string{"500000001", "500000002"}
				testEmote1 := mako.Emote{
					ID:          "emotesv2_3e22a0ab6add4a4c9bbb43897d196cdc",
					OwnerID:     "267893713",
					GroupID:     queriedIds[0],
					Prefix:      "wolf",
					Suffix:      "HOWL",
					Code:        "wolfHOWL",
					State:       "active",
					Domain:      "emotes",
					EmotesGroup: "BitsBadgeTierEmotes",
					Order:       0,
				}
				testEmote2 := mako.Emote{
					ID:          "emotesv2_3e22a0ab6add4a4c9bbb43897d196cdd",
					OwnerID:     "267893713",
					GroupID:     queriedIds[1],
					Prefix:      "wolf",
					Suffix:      "CUB",
					Code:        "wolfCUB",
					State:       "active",
					Domain:      "emotes",
					EmotesGroup: "BitsBadgeTierEmotes",
					Order:       0,
				}
				mockEmoteDB.On("ReadByGroupIDs", mock.Anything, queriedIds[0], queriedIds[1]).Return([]mako.Emote{
					testEmote1,
					testEmote2,
				}, nil)

				Convey("Then return those emote groups with emotes ordered by order", func() {
					request := &mk.GetEmoticonsByGroupsRequest{EmoticonGroupKeys: queriedIds}
					resp, err := emoteByGroupsAPI.GetEmoticonsByGroups(context.Background(), request)

					So(err, ShouldBeNil)
					So(resp, ShouldNotBeNil)
					So(len(resp.Groups), ShouldEqual, 2)
					actualGroup1 := resp.Groups[0].GetEmoticons()
					actualGroup2 := resp.Groups[1].GetEmoticons()
					if resp.Groups[0].Id != queriedIds[0] {
						actualGroup1 = resp.Groups[1].GetEmoticons()
						actualGroup2 = resp.Groups[0].GetEmoticons()
					}
					So(len(actualGroup1), ShouldEqual, 1)
					So(len(actualGroup2), ShouldEqual, 1)
					So(actualGroup1[0].GetId(), ShouldEqual, testEmote1.ID)
					So(actualGroup1[0].GetGroupId(), ShouldEqual, testEmote1.GroupID)
					So(actualGroup1[0].GetCode(), ShouldEqual, testEmote1.Code)

					So(actualGroup2[0].GetId(), ShouldEqual, testEmote2.ID)
					So(actualGroup2[0].GetGroupId(), ShouldEqual, testEmote2.GroupID)
					So(actualGroup2[0].GetCode(), ShouldEqual, testEmote2.Code)
				})
			})

			Convey("Given successful emote group fetch from emotedb with a mix of active and non-active emotes", func() {
				queriedIds := []string{"500000001", "500000002"}
				testEmote1 := mako.Emote{
					ID:          "emotesv2_3e22a0ab6add4a4c9bbb43897d196cdc",
					OwnerID:     "267893713",
					GroupID:     queriedIds[0],
					Prefix:      "wolf",
					Suffix:      "HOWL",
					Code:        "wolfHOWL",
					State:       "active",
					Domain:      "emotes",
					EmotesGroup: "BitsBadgeTierEmotes",
					Order:       2,
				}
				testEmote2 := mako.Emote{
					ID:          "emotesv2_3e22a0ab6add4a4c9bbb43897d196cdd",
					OwnerID:     "267893713",
					GroupID:     queriedIds[1],
					Prefix:      "wolf",
					Suffix:      "CUB",
					Code:        "wolfCUB",
					State:       "inactive",
					Domain:      "emotes",
					EmotesGroup: "BitsBadgeTierEmotes",
					Order:       0,
				}
				testEmote3 := mako.Emote{
					ID:          "emotesv2_3e22a0ab6add4a4c9bbb43897d196eee",
					OwnerID:     "267893713",
					GroupID:     queriedIds[0],
					Prefix:      "wolf",
					Suffix:      "PACK",
					Code:        "wolfPACK",
					State:       "active",
					Domain:      "emotes",
					EmotesGroup: "BitsBadgeTierEmotes",
					Order:       1,
				}
				testEmote4 := mako.Emote{
					ID:          "88",
					OwnerID:     "0",
					GroupID:     queriedIds[0],
					Prefix:      "",
					Suffix:      "PogChamp",
					Code:        "PogChamp",
					State:       "active",
					Domain:      "emotes",
					EmotesGroup: "Global",
					Order:       0,
				}
				mockEmoteDB.On("ReadByGroupIDs", mock.Anything, queriedIds[0], queriedIds[1]).Return([]mako.Emote{
					testEmote3,
					testEmote1,
					testEmote4,
					testEmote2,
				}, nil)

				Convey("Then return only active emotes, sorted by order ascending", func() {
					request := &mk.GetEmoticonsByGroupsRequest{EmoticonGroupKeys: queriedIds}
					resp, err := emoteByGroupsAPI.GetEmoticonsByGroups(context.Background(), request)

					So(err, ShouldBeNil)
					So(resp, ShouldNotBeNil)
					So(len(resp.Groups), ShouldEqual, 1)
					actualGroup := resp.Groups[0].GetEmoticons()
					So(len(actualGroup), ShouldEqual, 3)

					So(actualGroup[0].GetId(), ShouldEqual, testEmote4.ID)
					So(actualGroup[0].GetGroupId(), ShouldEqual, testEmote4.GroupID)
					So(actualGroup[0].GetCode(), ShouldEqual, testEmote4.Code)

					So(actualGroup[1].GetId(), ShouldEqual, testEmote3.ID)
					So(actualGroup[1].GetGroupId(), ShouldEqual, testEmote3.GroupID)
					So(actualGroup[1].GetCode(), ShouldEqual, testEmote3.Code)

					So(actualGroup[2].GetId(), ShouldEqual, testEmote1.ID)
					So(actualGroup[2].GetGroupId(), ShouldEqual, testEmote1.GroupID)
					So(actualGroup[2].GetCode(), ShouldEqual, testEmote1.Code)
				})
			})
		})
	})
}
