package api

import (
	"context"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/mako/clients"
	mk "code.justin.tv/commerce/mako/twirp"
	"github.com/twitchtv/twirp"
)

// EmoticonsByEmoticonIdsAPI includes the API for getting emotes by group id
type EmoticonsByEmoticonIdsAPI struct {
	Clients clients.Clients
}

// GetEmoticonsByEmoticonIds returns all emotes for the given emote id
func (emoticonsByEmoticonIdsAPI *EmoticonsByEmoticonIdsAPI) GetEmoticonsByEmoticonIds(ctx context.Context, r *mk.GetEmoticonsByEmoticonIdsRequest) (*mk.GetEmoticonsByEmoticonIdsResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, time.Second)
	defer cancel()

	emoticons, err := emoticonsByEmoticonIdsAPI.Clients.EmoticonsAggregator.GetEmoticonsByEmoteIDs(ctx, r.EmoticonIds)
	if err != nil {
		msg := "Error reading emotes by emote ids"
		log.WithError(err).WithField("emote_ids", r.EmoticonIds).Error(msg)
		return nil, twirp.InternalError(msg)
	}

	var result []*mk.Emoticon
	for _, emote := range emoticons {
		emoticon := emote.ToTwirp()
		result = append(result, &emoticon)
	}

	return &mk.GetEmoticonsByEmoticonIdsResponse{
		Emoticons: result,
	}, nil
}
