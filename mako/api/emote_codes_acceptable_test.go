package api

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/mako/clients"
	"code.justin.tv/commerce/mako/clients/emotecodeacceptability"
	emotecodeacceptability_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/emotecodeacceptability"
	mako_client "code.justin.tv/commerce/mako/twirp"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestBatchValidateEmoteCodeAcceptabilityAPI(t *testing.T) {
	Convey("Test Batch Validate Emote Code Acceptability API", t, func() {
		mockEmoteCodeAcceptabilityValidator := new(emotecodeacceptability_mock.AcceptabilityValidator)
		api := EmoteCodeAcceptableAPI{
			Clients: clients.Clients{
				EmoteCodeAcceptabilityValidator: mockEmoteCodeAcceptabilityValidator,
			},
		}

		testContext := context.Background()
		emoteCode := "testEmote"
		emoteCodeSuffix := "Emote"
		emoteCode2 := "testEmote2"
		emoteCodeSuffix2 := "Emote2"

		Convey("When there are no requests in the request body, then error", func() {
			resp, err := api.BatchValidateEmoteCodeAcceptability(testContext, &mako_client.BatchValidateEmoteCodeAcceptabilityRequest{
				Requests: nil,
			})

			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
			So(err.Error(), ShouldContainSubstring, "requests must contain at least one set of inputs")
		})

		Convey("When a request has an empty emote code, then error", func() {
			resp, err := api.BatchValidateEmoteCodeAcceptability(testContext, &mako_client.BatchValidateEmoteCodeAcceptabilityRequest{
				Requests: []*mako_client.ValidateEmoteCodeAcceptabilityInput{
					{
						EmoteCode: "",
					},
				},
			})

			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
			So(err.Error(), ShouldContainSubstring, "emote_code is required")
		})

		Convey("When a request has an empty emote code suffix, then error", func() {
			resp, err := api.BatchValidateEmoteCodeAcceptability(testContext, &mako_client.BatchValidateEmoteCodeAcceptabilityRequest{
				Requests: []*mako_client.ValidateEmoteCodeAcceptabilityInput{
					{
						EmoteCode:       emoteCode,
						EmoteCodeSuffix: "",
					},
				},
			})

			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
			So(err.Error(), ShouldContainSubstring, "emote_code_suffix is required")
		})

		Convey("When a request has an emote code suffix that does not match the full emote code, then error", func() {
			resp, err := api.BatchValidateEmoteCodeAcceptability(testContext, &mako_client.BatchValidateEmoteCodeAcceptabilityRequest{
				Requests: []*mako_client.ValidateEmoteCodeAcceptabilityInput{
					{
						EmoteCode:       emoteCode,
						EmoteCodeSuffix: "CompletelyDifferentString",
					},
				},
			})

			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
			So(err.Error(), ShouldContainSubstring, "emote_code_suffix must be present at the end of emote_code")
		})

		Convey("When the acceptability validator errors, then error", func() {
			mockEmoteCodeAcceptabilityValidator.On("AreEmoteCodesAcceptable", mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("validator error"))

			resp, err := api.BatchValidateEmoteCodeAcceptability(testContext, &mako_client.BatchValidateEmoteCodeAcceptabilityRequest{
				Requests: []*mako_client.ValidateEmoteCodeAcceptabilityInput{
					{
						EmoteCode:       emoteCode,
						EmoteCodeSuffix: emoteCodeSuffix,
					},
				},
			})

			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
			So(err.Error(), ShouldContainSubstring, "Error validating emote code acceptability")
		})

		Convey("When the acceptability validator succeeds, then return a proper response", func() {
			mockEmoteCodeAcceptabilityValidator.On("AreEmoteCodesAcceptable", mock.Anything, mock.Anything, mock.Anything).Return([]emotecodeacceptability.EmoteCodeAcceptabilityResult{
				{
					EmoteCode:       emoteCode,
					EmoteCodeSuffix: emoteCodeSuffix,
					IsAcceptable:    true,
				},
				{
					EmoteCode:       emoteCode2,
					EmoteCodeSuffix: emoteCodeSuffix2,
					IsAcceptable:    false,
				},
			}, nil)

			resp, err := api.BatchValidateEmoteCodeAcceptability(testContext, &mako_client.BatchValidateEmoteCodeAcceptabilityRequest{
				Requests: []*mako_client.ValidateEmoteCodeAcceptabilityInput{
					{
						EmoteCode:       emoteCode,
						EmoteCodeSuffix: emoteCodeSuffix,
					},
					{
						EmoteCode:       emoteCode2,
						EmoteCodeSuffix: emoteCodeSuffix2,
					},
				},
			})

			So(err, ShouldBeNil)
			So(resp, ShouldResemble, &mako_client.BatchValidateEmoteCodeAcceptabilityResponse{
				Results: []*mako_client.ValidateEmoteCodeAcceptabilityResult{
					{
						EmoteCode:       emoteCode,
						EmoteCodeSuffix: emoteCodeSuffix,
						IsAcceptable:    true,
					},
					{
						EmoteCode:       emoteCode2,
						EmoteCodeSuffix: emoteCodeSuffix2,
						IsAcceptable:    false,
					},
				},
			})
		})
	})
}
