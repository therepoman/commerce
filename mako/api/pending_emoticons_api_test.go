package api

import (
	"context"
	"testing"
	"time"

	"code.justin.tv/commerce/mako/clients"
	"code.justin.tv/commerce/mako/clients/pendingemoticons/models"
	"code.justin.tv/commerce/mako/dynamo"
	mako "code.justin.tv/commerce/mako/internal"
	pending_emoticons_client_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/pendingemoticons"
	mk "code.justin.tv/commerce/mako/twirp"
	"github.com/golang/protobuf/ptypes/timestamp"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestPendingEmoticonsAPI(t *testing.T) {
	Convey("Test PendingEmoticons API", t, func() {
		mockPendingEmoticonsClient := new(pending_emoticons_client_mock.IPendingEmoticonsClient)

		pendingEmoticonsAPI := PendingEmoticonsAPI{
			Clients: clients.Clients{
				PendingEmoticonsClient: mockPendingEmoticonsClient,
			},
		}

		Convey("Test GetPendingEmoticonsByIDs API", func() {
			Convey("when PendingEmoticonsClient.GetPendingEmoticonsByIDs returns an error, we should return an error", func() {
				req := &mk.GetPendingEmoticonsByIDsRequest{
					EmoticonIds: []string{"1", "2"},
				}

				mockPendingEmoticonsClient.On("GetPendingEmoticonsByIDs", mock.Anything, req.EmoticonIds).Return(nil, errors.New("ERROR"))

				resp, err := pendingEmoticonsAPI.GetPendingEmoticonsByIDs(context.Background(), req)

				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})

			Convey("when PendingEmoticonsClient.GetPendingEmoticonsByIDs does not return an error, we should return the response", func() {
				req := &mk.GetPendingEmoticonsByIDsRequest{
					EmoticonIds: []string{"1", "2"},
				}

				mockPendingEmotes := []*dynamo.PendingEmote{
					{
						EmoteId:   "123",
						UpdatedAt: time.Now(),
						Type:      dynamo.Partner,
					},
				}

				mockPendingEmoticonsClient.On("GetPendingEmoticonsByIDs", mock.Anything, req.EmoticonIds).Return(mockPendingEmotes, nil)

				resp, err := pendingEmoticonsAPI.GetPendingEmoticonsByIDs(context.Background(), req)

				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
			})
		})

		Convey("Test DeletePendingEmoticons API", func() {
			Convey("when PendingEmoticonsClient.DeletePendingEmoticons returns an error, we should return an error", func() {
				req := &mk.DeletePendingEmoticonsRequest{
					EmoticonIds: []string{"1", "2"},
				}

				mockPendingEmoticonsClient.On("DeletePendingEmoticons", mock.Anything, req.EmoticonIds).Return(errors.New("ERROR"))

				resp, err := pendingEmoticonsAPI.DeletePendingEmoticons(context.Background(), req)

				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})

			Convey("when PendingEmoticonsClient.DeletePendingEmoticons does not return an error, we should return the response", func() {
				req := &mk.DeletePendingEmoticonsRequest{
					EmoticonIds: []string{"1", "2"},
				}

				mockPendingEmoticonsClient.On("DeletePendingEmoticons", mock.Anything, req.EmoticonIds).Return(nil)

				resp, err := pendingEmoticonsAPI.DeletePendingEmoticons(context.Background(), req)

				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
			})
		})

		Convey("Test GetPendingEmoticons API", func() {
			Convey("when the request.Count is < 1, we should error", func() {
				resp, err := pendingEmoticonsAPI.GetPendingEmoticons(context.Background(), &mk.GetPendingEmoticonsRequest{
					Count: -1,
				})

				So(resp, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})

			Convey("when request.Bucket.BucketCount < 1, we should error", func() {
				resp, err := pendingEmoticonsAPI.GetPendingEmoticons(context.Background(), &mk.GetPendingEmoticonsRequest{
					Count:          1,
					AfterTimestamp: &timestamp.Timestamp{},
					Bucket: &mk.EmoticonBucket{
						Requested:   0,
						BucketCount: -1,
					},
				})

				So(resp, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})

			Convey("when request.Bucket.Requested >= request.Bucket.BucketCount, we should error", func() {
				resp, err := pendingEmoticonsAPI.GetPendingEmoticons(context.Background(), &mk.GetPendingEmoticonsRequest{
					Count:          1,
					AfterTimestamp: &timestamp.Timestamp{},
					Bucket: &mk.EmoticonBucket{
						Requested:   2,
						BucketCount: 1,
					},
				})

				So(resp, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})

			Convey("when PendingEmoticonsClient.GetPendingEmoticons returns an error, we should return an error", func() {
				req := &mk.GetPendingEmoticonsRequest{
					Count:          1,
					AfterTimestamp: &timestamp.Timestamp{},
					Bucket: &mk.EmoticonBucket{
						Requested:   1,
						BucketCount: 2,
					},
				}

				mockPendingEmoticonsClient.On("GetPendingEmoticons", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("ERROR"))

				resp, err := pendingEmoticonsAPI.GetPendingEmoticons(context.Background(), req)

				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})

			Convey("when PendingEmoticonsClient.GetPendingEmoticons does not return an error, we should return the response", func() {
				req := &mk.GetPendingEmoticonsRequest{
					Count:          1,
					AfterTimestamp: &timestamp.Timestamp{},
					Bucket: &mk.EmoticonBucket{
						Requested:   1,
						BucketCount: 2,
					},
				}

				mockPendingEmotes := []*dynamo.PendingEmote{
					{
						EmoteId:   "123",
						UpdatedAt: time.Now(),
						Type:      dynamo.Partner,
					},
				}

				mockPendingEmoticonsClient.On("GetPendingEmoticons", mock.Anything, int(req.Count), models.PendingEmoticonFilterAll, models.PendingEmoticonReviewStateFilterAll, models.PendingEmoteAssetTypeFilterAll, mock.Anything, 2, 1).Return(mockPendingEmotes, nil)

				resp, err := pendingEmoticonsAPI.GetPendingEmoticons(context.Background(), req)

				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
			})
		})

		Convey("Test ReviewPendingEmoticons API", func() {
			Convey("when PendingEmoticonsClient.ReviewPendingEmoticons returns an error, we should return an error", func() {
				req := &mk.ReviewPendingEmoticonsRequest{
					Reviews: []*mk.ReviewPendingEmoticonRequest{},
					AdminId: "123",
				}

				mockPendingEmoticonsClient.On("ReviewPendingEmoticons", mock.Anything, []models.EmoticonReview{}, "123").Return(nil, errors.New("ERROR"))

				resp, err := pendingEmoticonsAPI.ReviewPendingEmoticons(context.Background(), req)

				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})

			Convey("when PendingEmoticonsClient.ReviewPendingEmoticons does not return an error, we should return the response", func() {
				req := &mk.ReviewPendingEmoticonsRequest{
					Reviews: []*mk.ReviewPendingEmoticonRequest{
						{
							Id:      "123",
							Approve: true,
						},
						{
							Id:                 "456",
							Approve:            false,
							DeclineReason:      "Bad",
							DeclineExplanation: "GG",
						},
						{
							Id:                     "789",
							Approve:                false,
							DeclineReason:          "No Email Pls",
							DeclineExplanation:     "NoEmails",
							ShouldSkipNotification: true,
						},
					},
					AdminId: "123",
				}

				expectedEmoticonReview := []models.EmoticonReview{
					{
						EmoticonID:             "123",
						Approve:                true,
						ShouldSkipNotification: false,
					},
					{
						EmoticonID:             "456",
						Approve:                false,
						RejectReason:           "Bad",
						RejectDescription:      "GG",
						ShouldSkipNotification: false,
					},
					{
						EmoticonID:             "789",
						Approve:                false,
						RejectReason:           "No Email Pls",
						RejectDescription:      "NoEmails",
						ShouldSkipNotification: true,
					},
				}

				mockReviewedPendingEmoticons := []models.ReviewEmoticonResponse{
					{
						EmoticonID: "123",
						Succeeded:  true,
						NewState:   mako.Active,
					},
					{
						EmoticonID: "456",
						Succeeded:  true,
						NewState:   mako.Inactive,
					},
					{
						EmoticonID: "789",
						Succeeded:  true,
						NewState:   mako.Inactive,
					},
				}

				mockPendingEmoticonsClient.On("ReviewPendingEmoticons", mock.Anything, expectedEmoticonReview, "123").Return(mockReviewedPendingEmoticons, nil)

				resp, err := pendingEmoticonsAPI.ReviewPendingEmoticons(context.Background(), req)

				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
			})
		})

		Convey("Test DeferPendingEmoticons API", func() {
			Convey("when an empty EmoticonIds list is provided, we should return an error", func() {
				req := &mk.DeferPendingEmoticonsRequest{
					EmoticonIds: []string{},
					AdminId:     "123",
				}

				resp, err := pendingEmoticonsAPI.DeferPendingEmoticons(context.Background(), req)

				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})

			Convey("when PendingEmoticonsClient.DeferPendingEmoticons returns an error, we should return an error", func() {
				req := &mk.DeferPendingEmoticonsRequest{
					EmoticonIds: []string{"123", "456"},
					AdminId:     "123",
				}

				mockPendingEmoticonsClient.On("DeferPendingEmoticons", mock.Anything, []string{"123", "456"}, "123").Return(nil, errors.New("ERROR"))

				resp, err := pendingEmoticonsAPI.DeferPendingEmoticons(context.Background(), req)

				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})

			Convey("when PendingEmoticonsClient.DeferPendingEmoticons does not return an error, we should return the response", func() {
				req := &mk.DeferPendingEmoticonsRequest{
					EmoticonIds: []string{"123", "456"},
					AdminId:     "123",
				}

				expectedDeferredEmoticonIDs := []string{
					"123",
					"456",
				}

				mockDeferredPendingEmoticons := []*models.DeferPendingEmoticonResponse{
					{
						EmoticonID: "123",
						Succeeded:  true,
					},
					{
						EmoticonID: "456",
						Succeeded:  true,
					},
				}

				mockPendingEmoticonsClient.On("DeferPendingEmoticons", mock.Anything, expectedDeferredEmoticonIDs, "123").Return(mockDeferredPendingEmoticons, nil)

				resp, err := pendingEmoticonsAPI.DeferPendingEmoticons(context.Background(), req)

				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
			})
		})

		Convey("Test CreatePendingEmoticon API", func() {
			Convey("when PendingEmoticonsClient.CreatePendingEmoticon returns an error, we should return an error", func() {
				req := &mk.CreatePendingEmoticonRequest{
					Id:          "123",
					Prefix:      "michael",
					Code:        "Zoomer",
					OwnerId:     "abc",
					AccountType: mk.AccountType_Affiliate,
				}

				mockPendingEmoticonsClient.On("CreatePendingEmoticon", mock.Anything, req.Id, req.Prefix, req.Code, req.OwnerId, dynamo.Affiliate, dynamo.PendingEmoteReviewStateNone, mako.StaticAssetType, mock.Anything).Return(dynamo.PendingEmote{}, errors.New("ERROR"))

				resp, err := pendingEmoticonsAPI.CreatePendingEmoticon(context.Background(), req)

				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})

			Convey("when PendingEmoticonsClient.CreatePendingEmoticon (Affiliate) does not return an error, we should return the response", func() {
				req := &mk.CreatePendingEmoticonRequest{
					Id:          "123",
					Prefix:      "michael",
					Code:        "Zoomer",
					OwnerId:     "abc",
					AccountType: mk.AccountType_Affiliate,
				}

				mockPendingEmoticon := dynamo.PendingEmote{
					EmoteId:   "123",
					OwnerId:   "abc",
					Code:      "Zoomer",
					Prefix:    "michael",
					UpdatedAt: time.Now(),
					Type:      dynamo.Affiliate,
				}

				mockPendingEmoticonsClient.On("CreatePendingEmoticon", mock.Anything, req.Id, req.Prefix, req.Code, req.OwnerId, dynamo.Affiliate, dynamo.PendingEmoteReviewStateNone, mako.StaticAssetType, mako.NoTemplate).Return(mockPendingEmoticon, nil)

				resp, err := pendingEmoticonsAPI.CreatePendingEmoticon(context.Background(), req)

				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
			})

			Convey("when PendingEmoticonsClient.CreatePendingEmoticon (Partner) does not return an error, we should return the response", func() {
				req := &mk.CreatePendingEmoticonRequest{
					Id:                    "123",
					Prefix:                "michael",
					Code:                  "Zoomer",
					OwnerId:               "abc",
					AccountType:           mk.AccountType_Partner,
					AnimatedEmoteTemplate: mk.AnimatedEmoteTemplate_SLIDEOUT,
				}

				mockPendingEmoticon := dynamo.PendingEmote{
					EmoteId:           "123",
					OwnerId:           "abc",
					Code:              "Zoomer",
					Prefix:            "michael",
					UpdatedAt:         time.Now(),
					Type:              dynamo.Partner,
					AnimationTemplate: string(mako.SlideOut),
				}

				mockPendingEmoticonsClient.On("CreatePendingEmoticon", mock.Anything, req.Id, req.Prefix, req.Code, req.OwnerId, dynamo.Partner, dynamo.PendingEmoteReviewStateNone, mako.StaticAssetType, mako.SlideOut).Return(mockPendingEmoticon, nil)

				resp, err := pendingEmoticonsAPI.CreatePendingEmoticon(context.Background(), req)

				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
			})
		})
	})
}
