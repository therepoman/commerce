package api

import (
	"context"
	"errors"
	"testing"

	mako "code.justin.tv/commerce/mako/internal"
	emoticonsmanager_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/emoticonsmanager"
	mkd "code.justin.tv/commerce/mako/twirp/dashboard"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestEmoteOrdersAPI(t *testing.T) {
	Convey("Test EmoteOrdersAPI", t, func() {
		mockEmoticon := new(emoticonsmanager_mock.IEmoticonsManager)
		emoteOrdersAPI := &EmoteOrdersAPI{
			EmoticonsManager: mockEmoticon,
		}

		Convey("Test UpdateEmoteOrders", func() {
			Convey("With empty user_id", func() {
				req := &mkd.UpdateEmoteOrdersRequest{
					RequestingUserId: "",
				}

				_, err := emoteOrdersAPI.UpdateEmoteOrders(context.Background(), req)
				So(err, ShouldNotBeNil)

			})

			Convey("With empty orders", func() {
				req := &mkd.UpdateEmoteOrdersRequest{
					RequestingUserId: "someUser",
				}

				_, err := emoteOrdersAPI.UpdateEmoteOrders(context.Background(), req)
				So(err, ShouldNotBeNil)
			})

			Convey("With orders from multiple groups", func() {
				req := &mkd.UpdateEmoteOrdersRequest{
					RequestingUserId: "someUser",
					EmoteOrders: []*mkd.EmoteOrder{
						{EmoteId: "emote1", GroupId: "group1", Order: 0},
						{EmoteId: "emote2", GroupId: "group2", Order: 1},
					},
				}

				_, err := emoteOrdersAPI.UpdateEmoteOrders(context.Background(), req)
				So(err, ShouldNotBeNil)
			})

			Convey("With ambiguous orders for an emote", func() {
				req := &mkd.UpdateEmoteOrdersRequest{
					RequestingUserId: "someUser",
					EmoteOrders: []*mkd.EmoteOrder{
						{EmoteId: "emote1", GroupId: "group1", Order: 0},
						{EmoteId: "emote1", GroupId: "group1", Order: 1},
					},
				}

				_, err := emoteOrdersAPI.UpdateEmoteOrders(context.Background(), req)
				So(err, ShouldNotBeNil)
			})

			Convey("With a blank emoteID", func() {
				req := &mkd.UpdateEmoteOrdersRequest{
					RequestingUserId: "someUser",
					EmoteOrders: []*mkd.EmoteOrder{
						{EmoteId: "", GroupId: "group1", Order: 0},
						{EmoteId: "emote2", GroupId: "group1", Order: 1},
					},
				}

				_, err := emoteOrdersAPI.UpdateEmoteOrders(context.Background(), req)
				So(err, ShouldNotBeNil)
			})

			Convey("With a blank groupID", func() {
				req := &mkd.UpdateEmoteOrdersRequest{
					RequestingUserId: "someUser",
					EmoteOrders: []*mkd.EmoteOrder{
						{EmoteId: "emote1", GroupId: "", Order: 0},
						{EmoteId: "emote2", GroupId: "group1", Order: 1},
					},
				}

				_, err := emoteOrdersAPI.UpdateEmoteOrders(context.Background(), req)
				So(err, ShouldNotBeNil)
			})

			Convey("With valid input", func() {
				userID := "someUser"
				groupID := "someGroup"
				requestedOrder := []*mkd.EmoteOrder{
					{EmoteId: "emote1", GroupId: groupID, Order: 0},
					{EmoteId: "emote2", GroupId: groupID, Order: 1},
				}

				req := &mkd.UpdateEmoteOrdersRequest{
					RequestingUserId: userID,
					EmoteOrders:      requestedOrder,
				}

				Convey("when emoticonmanager fails", func() {
					mockEmoticon.On("UpdateOrders", mock.Anything, mock.Anything).Return(nil, errors.New("some_error"))

					resp, err := emoteOrdersAPI.UpdateEmoteOrders(context.Background(), req)
					So(err, ShouldNotBeNil)
					So(resp, ShouldBeNil)
				})

				Convey("when emoticonmanager succeeds", func() {
					mockEmoticon.On("UpdateOrders", mock.Anything, mock.Anything).Return([]mako.Emote{
						{ID: "emote1", GroupID: groupID, Order: 0},
						{ID: "emote2", GroupID: groupID, Order: 1},
						{ID: "emote3", GroupID: groupID, Order: 2},
					}, nil)

					expectedEmotes := []*mkd.Emote{
						{Id: "emote1", GroupId: groupID, Order: 0},
						{Id: "emote2", GroupId: groupID, Order: 1},
						{Id: "emote3", GroupId: groupID, Order: 2},
					}

					resp, err := emoteOrdersAPI.UpdateEmoteOrders(context.Background(), req)
					So(err, ShouldBeNil)
					So(resp, ShouldNotBeNil)
					So(len(resp.Emotes), ShouldEqual, 3)
					for i, emote := range resp.Emotes {
						So(emote.Id, ShouldEqual, expectedEmotes[i].Id)
						So(emote.GroupId, ShouldEqual, expectedEmotes[i].GroupId)
						So(emote.Order, ShouldEqual, expectedEmotes[i].Order)
					}
				})
			})
		})
	})
}
