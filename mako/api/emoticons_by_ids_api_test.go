package api

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/mako/clients"
	mako "code.justin.tv/commerce/mako/internal"
	emoticons_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/emoticons"
	mk "code.justin.tv/commerce/mako/twirp"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func TestEmoticonsByEmoticonIdsAPI(t *testing.T) {
	Convey("Test Emoticons By Emoticon Ids API", t, func() {
		mockEmoticonsAggregator := new(emoticons_mock.EmoticonAggregator)

		emoticonsByEmoticonIdsAPI := EmoticonsByEmoticonIdsAPI{
			Clients: clients.Clients{
				EmoticonsAggregator: mockEmoticonsAggregator,
			},
		}

		Convey("Test GetEmoticonsByEmoticonIds API", func() {
			Convey("Given aggregator returns error", func() {
				emoteID := "3e22a0ab-6add-4a4c-9bbb-43897d196cdc"
				mockEmoticonsAggregator.On("GetEmoticonsByEmoteIDs", mock.Anything, mock.Anything).Return(nil, errors.New("dummy aggregator error"))

				Convey("then return generic internal error", func() {
					request := &mk.GetEmoticonsByEmoticonIdsRequest{EmoticonIds: []string{emoteID}}
					resp, err := emoticonsByEmoticonIdsAPI.GetEmoticonsByEmoticonIds(context.Background(), request)

					So(resp, ShouldBeNil)
					So(err, ShouldNotBeNil)
					So(err.Error(), ShouldContainSubstring, "twirp error internal")
				})
			})

			Convey("Given successful emote fetch from aggregator", func() {
				queriedIds := []string{"3e22a0ab-6add-4a4c-9bbb-43897d196cdc", "3e22a0ab-6add-4a4c-9bbb-43897d196cdd"}
				testEmote1 := mako.Emote{
					ID:      queriedIds[0],
					OwnerID: "267893713",
					GroupID: "BitsBadgeTier-267893713-1000",
					Code:    "wolfHOWL",
					State:   mako.Active,
					Domain:  mako.EmotesDomain,
				}
				testEmote2 := mako.Emote{
					ID:      queriedIds[1],
					OwnerID: "267893713",
					GroupID: "BitsBadgeTier-267893713-5000",
					Code:    "wolfCUB",
					State:   mako.Active,
					Domain:  mako.EmotesDomain,
				}
				mockEmoticonsAggregator.On("GetEmoticonsByEmoteIDs", mock.Anything, queriedIds).Return([]mako.Emote{testEmote1, testEmote2}, nil)

				Convey("Then return those emotes", func() {
					request := &mk.GetEmoticonsByEmoticonIdsRequest{EmoticonIds: queriedIds}
					resp, err := emoticonsByEmoticonIdsAPI.GetEmoticonsByEmoticonIds(context.Background(), request)

					So(err, ShouldBeNil)
					So(resp, ShouldNotBeNil)
					So(len(resp.Emoticons), ShouldEqual, 2)

					if resp.Emoticons[0].Id == testEmote1.ID {
						So(resp.Emoticons[0].Id, ShouldEqual, testEmote1.ID)
						So(resp.Emoticons[0].GroupId, ShouldEqual, testEmote1.GroupID)
						So(resp.Emoticons[0].Code, ShouldEqual, testEmote1.Code)

						So(resp.Emoticons[1].Id, ShouldEqual, testEmote2.ID)
						So(resp.Emoticons[1].GroupId, ShouldEqual, testEmote2.GroupID)
						So(resp.Emoticons[1].Code, ShouldEqual, testEmote2.Code)
					} else if resp.Emoticons[0].Id == testEmote2.ID {
						So(resp.Emoticons[1].Id, ShouldEqual, testEmote1.ID)
						So(resp.Emoticons[1].GroupId, ShouldEqual, testEmote1.GroupID)
						So(resp.Emoticons[1].Code, ShouldEqual, testEmote1.Code)

						So(resp.Emoticons[0].Id, ShouldEqual, testEmote2.ID)
						So(resp.Emoticons[0].GroupId, ShouldEqual, testEmote2.GroupID)
						So(resp.Emoticons[0].Code, ShouldEqual, testEmote2.Code)
					} else {
						assert.Fail(t, "response from API does not match expected emote")
					}
				})
			})
		})
	})
}
