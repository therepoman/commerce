package api

import (
	"context"
	"time"

	"code.justin.tv/ce-analytics/chemtrail/rpc/chemtrail"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/mako/clients"
	mako "code.justin.tv/commerce/mako/internal"
	"github.com/twitchtv/twirp"

	mkd "code.justin.tv/commerce/mako/twirp/dashboard"
)

type GetChannelEmoteUsageAPI struct {
	Clients clients.Clients
}

const maxEmoteUsageCount = 100
const chemtrailUsageRequestLimit = 200

func (api *GetChannelEmoteUsageAPI) GetChannelEmoteUsage(ctx context.Context, r *mkd.GetChannelEmoteUsageRequest) (*mkd.GetChannelEmoteUsageResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, 3*time.Second)
	defer cancel()

	if r.ChannelId == "" {
		return nil, twirp.InvalidArgumentError("channel_id", "cannot be empty")
	} else if r.RequestingUserId == "" {
		return nil, twirp.InvalidArgumentError("requesting_user_id", "cannot be empty")
	}

	permitted, err := api.isUserPermitted(ctx, r.ChannelId, r.RequestingUserId)
	if err != nil {
		return nil, err
	}

	if !permitted {
		return nil, twirp.NewError(twirp.PermissionDenied, "requesting user is not allowed to view this channel's emote usage")
	}

	// Get all the channel's product emote groups
	productGroups, err := api.Clients.ProductEmoteGroupsClient.GetProductEmoteGroups(ctx, r.ChannelId)
	if err != nil {
		msg := "error looking up emote group product info for channel"
		log.WithError(err).WithField("channel_id", r.ChannelId).Error(err)
		return nil, twirp.InternalError(msg)
	}

	var groupIds []string

	if len(r.EmoteGroupAssetTypes) != 0 || len(r.EmoteGroupProductTypes) != 0 {
		productTypeFilter := make(map[mkd.EmoteGroupProductType]struct{})
		assetTypeFilter := make(map[mkd.EmoteGroupType]struct{})

		// Build asset type filter
		if len(r.EmoteGroupAssetTypes) == 0 {
			assetTypeFilter[mkd.EmoteGroupType_animated_group] = struct{}{}
			assetTypeFilter[mkd.EmoteGroupType_static_group] = struct{}{}
		} else {
			for _, assetType := range r.EmoteGroupAssetTypes {
				assetTypeFilter[assetType] = struct{}{}
			}
		}

		// Build product type filter
		allProducts := false
		if len(r.EmoteGroupProductTypes) == 0 {
			// If we are passed no product types treat it as all product types
			allProducts = true
		} else {
			for _, productType := range r.EmoteGroupProductTypes {
				productTypeFilter[productType] = struct{}{}
			}
		}

		// Apply filters to groups
		for _, group := range productGroups {
			_, matchedAssetType := assetTypeFilter[group.GroupType]
			_, matchedProductType := productTypeFilter[group.ProductType]
			if matchedAssetType && (allProducts || matchedProductType) {
				groupIds = append(groupIds, group.Id)
			}
		}

		// If groups don't exist for the specified filters, then return nothing.
		if len(groupIds) == 0 {
			return &mkd.GetChannelEmoteUsageResponse{
				EmoteUsage: []*mkd.EmoteUsage{},
			}, nil
		}
	}

	sortBy, sort := toChemtrailSortByAndSortOrder(r.SortBy)
	usageType := toChemtrailUsageType(r.UsageType)

	emoteUsageResp, err := api.Clients.ChemtrailClient.GetEmoteUsage(ctx, &chemtrail.GetEmoteUsageRequest{
		UserId:        r.ChannelId,
		StartTime:     r.StartTime,
		EndTime:       r.EndTime,
		UsageType:     usageType,
		EmoteSortBy:   sortBy,
		Sort:          sort,
		EmoteGroupIds: groupIds,
		Limit:         chemtrailUsageRequestLimit,
	})
	if err != nil {
		msg := "error calling chemtrail for emote usage"
		log.WithError(err).WithFields(log.Fields{
			"user_id":    r.ChannelId,
			"start_time": r.StartTime,
			"end_time":   r.EndTime,
			"usage_type": usageType,
			"sort_by":    sortBy,
			"sort_order": sort,
			"group_ids":  groupIds,
			"limit":      chemtrailUsageRequestLimit,
		}).Error(msg)
		return nil, twirp.InternalError(msg)
	}

	emoteUsage := emoteUsageResp.EmoteUsage
	var filteredUsage []*mkd.EmoteUsage

	if len(emoteUsage) == 0 {
		return &mkd.GetChannelEmoteUsageResponse{
			EmoteUsage: filteredUsage,
		}, nil
	}

	// Emote usage may have more than one usage per emoteId since the emote could have
	// been in another group. We dedupe the emoteIds for fetching their current state.
	uniqueEmoteIdsMap := make(map[string]struct{})
	uniqueEmoteIds := make([]string, 0)
	for _, u := range emoteUsage {
		if _, ok := uniqueEmoteIdsMap[u.EmoteId]; !ok {
			uniqueEmoteIds = append(uniqueEmoteIds, u.EmoteId)
			uniqueEmoteIdsMap[u.EmoteId] = struct{}{}
		}
	}

	// Get all emotes from the database
	emotes, err := api.Clients.EmoteDB.ReadByIDs(ctx, uniqueEmoteIds...)
	if err != nil {
		msg := "Error reading emotes by ids in GetChannelEmoteUsage"
		log.WithError(err).WithField("emote_ids", uniqueEmoteIds).Error(err)
		return nil, twirp.InternalError(msg)
	}

	// Build a map of the emotes for quick access
	emoteIdToEmote := make(map[string]mako.Emote)
	for _, emote := range emotes {
		emoteIdToEmote[emote.ID] = emote
	}

	// Build a map of the product emote groups for quick access
	productGroupsMap := make(map[string]*mkd.EmoteGroup)
	for _, group := range productGroups {
		productGroupsMap[group.Id] = group
	}

	// Filter out emotes that should not be returned and build the response
	for _, usage := range emoteUsage {
		emote := emoteIdToEmote[usage.EmoteId]
		if emote.OwnerID == r.ChannelId && (emote.State == mako.Active || emote.State == mako.Archived) {
			usageGroup := productGroupsMap[usage.EmoteGroupId]
			dashboardEmote := emote.ToDashboardTwirp()
			filteredUsage = append(filteredUsage, &mkd.EmoteUsage{
				Emote:       &dashboardEmote,
				EmoteGroup:  usageGroup,
				TotalUsage:  usage.TotalUsage,
				UniqueUsers: usage.UniqueUsers,
			})
		}
	}

	if len(filteredUsage) > maxEmoteUsageCount {
		filteredUsage = filteredUsage[:maxEmoteUsageCount]
	}

	return &mkd.GetChannelEmoteUsageResponse{
		EmoteUsage: filteredUsage,
	}, nil
}

func (api *GetChannelEmoteUsageAPI) isUserPermitted(ctx context.Context, channelID, requestingUserID string) (bool, error) {
	if channelID == requestingUserID {
		return true, nil
	}

	requestingUser, err := api.Clients.UserServiceClient.GetUserByID(ctx, requestingUserID, nil)
	if err != nil {
		msg := "error looking up requesting user while validating GetChannelEmoteUsage request"
		log.WithError(err).WithField("requestingUserID", requestingUserID).Error(msg)
		return false, twirp.InternalError(msg)
	}

	if requestingUser.Admin != nil && *requestingUser.Admin {
		return true, nil
	}

	return false, nil
}

func toChemtrailUsageType(usage mkd.EmoteUsageType) chemtrail.EmoteUsageType {
	switch usage {
	case mkd.EmoteUsageType_OutsideChannel:
		return chemtrail.EmoteUsageType_EMOTE_USAGE_TYPE_OUTSIDE_CHANNEL
	case mkd.EmoteUsageType_WithinChannel:
		return chemtrail.EmoteUsageType_EMOTE_USAGE_TYPE_WITHIN_CHANNEL
	default:
		return chemtrail.EmoteUsageType_EMOTE_USAGE_TYPE_ALL
	}
}

func toChemtrailSortByAndSortOrder(sort mkd.EmoteUsageSortBy) (chemtrail.EmoteUsageSortBy, chemtrail.SortOrder) {
	switch sort {
	case mkd.EmoteUsageSortBy_TotalUsageAscending:
		return chemtrail.EmoteUsageSortBy_EMOTE_USAGE_SORT_BY_TOTAL_USAGE, chemtrail.SortOrder_ASC
	case mkd.EmoteUsageSortBy_TotalUsageDescending:
		return chemtrail.EmoteUsageSortBy_EMOTE_USAGE_SORT_BY_TOTAL_USAGE, chemtrail.SortOrder_DESC
	case mkd.EmoteUsageSortBy_UniqueUsersAscending:
		return chemtrail.EmoteUsageSortBy_EMOTE_USAGE_SORT_BY_UNIQUE_USERS, chemtrail.SortOrder_ASC
	case mkd.EmoteUsageSortBy_UniqueUsersDescending:
		return chemtrail.EmoteUsageSortBy_EMOTE_USAGE_SORT_BY_UNIQUE_USERS, chemtrail.SortOrder_DESC
	default:
		return chemtrail.EmoteUsageSortBy_EMOTE_USAGE_SORT_BY_TOTAL_USAGE, chemtrail.SortOrder_DESC
	}
}
