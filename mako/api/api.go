package api

import (
	"net/http"
	"net/http/pprof"
	_ "net/http/pprof"

	"code.justin.tv/commerce/mako/config"
	"code.justin.tv/commerce/mako/middleware"
	makoStats "code.justin.tv/commerce/mako/stats"
	mk "code.justin.tv/commerce/mako/twirp"
	mako_dashboard "code.justin.tv/commerce/mako/twirp/dashboard"
	"github.com/cactus/go-statsd-client/statsd"
)

type MakoServer struct {
	EntitlementsAPI
	HealthAPI
	EmoteSetDetailsAPI
	EmoticonsByGroupsAPI
	EmoticonsByEmoticonIdsAPI
	ActiveEmoteCodesAPI
	GetEmoticonUploadConfigurationAPI
	CreateEmoticonAPI
	DeleteEmoticonAPI
	DeactivateEmoticonAPI
	DefaultEmoteImagesAPI
	ActivateEmoticonAPI
	UpdateEmoteCodeSuffixAPI
	UserEmoteStandingAPI
	SetUserEmoteStandingOverrideAPI
	SmiliesAPI
	LegacyEmoticonsAPI
	PrefixesAPI
	EmoticonsByOwnerAPI
	EmoteEntitlementsAPI
	CreateEmoteEntitlementsAPI
	Create3PEmoteEntitlementsAPI
	CreateEmoteGroupEntitlementsAPI
	DeleteEmoteGroupEntitlementsAPI
	PendingEmoticonsAPI
	EmoteCodeUniqueAPI
	EmoteCodeAcceptableAPI
	CreateEmoteGroupAPI
	GetEmotesByCodesAPI
	EmoteModifierGroupsAPIs
	GetAvailableFollowerEmotesAPI
	GetUserChannelEmotesAPI

	// Dashboard APIs
	GetEmotesByGroupsAPI
	GetEmoteUploadConfigAPI
	GetUserFollowerEmoteStatusAPI
	CreateEmoteAPI
	EmoteOrdersAPI
	EmotesByIDsAPI
	EmoteGroupAssignAPI
	UpdateEmoteOriginsAPI
	UserProductEmoteGroupsAPI
	UserEmoteLimitsAPI
	UserRolloutStatusAPI
	GetChannelEmoteUsageAPI

	// Not-exposed-via-twirp APIs (used only for integration test cleanup and developer scripts)
	DeleteEmoteGroupAPI
}

// NewServer sets up new server
func (makoServer *MakoServer) NewServer(stats statsd.StatSender, cfg *config.Configuration) http.Handler {
	twirpStatsdHook := makoStats.NewStatsdServerHooks(stats)
	twirpHandler := mk.NewMakoServer(makoServer, twirpStatsdHook)
	twirpDashboardHandler := mako_dashboard.NewMakoServer(makoServer, twirpStatsdHook)
	mux := http.NewServeMux()

	mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		_, _ = w.Write([]byte("Hello Mako!"))
	})

	mux.HandleFunc("/health", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		_, _ = w.Write([]byte("OK!"))
	})

	mux.Handle(mk.MakoPathPrefix, twirpHandler)
	mux.Handle(mako_dashboard.MakoPathPrefix, twirpDashboardHandler)

	mux.HandleFunc("/debug/pprof/", pprof.Index)
	mux.HandleFunc("/debug/pprof/cmdline", pprof.Cmdline)
	mux.HandleFunc("/debug/pprof/profile", pprof.Profile)
	mux.HandleFunc("/debug/pprof/symbol", pprof.Symbol)
	mux.HandleFunc("/debug/pprof/trace", pprof.Trace)

	handler := middleware.ClientIDMiddleware(mux)
	handler = middleware.XForwardedForMiddleware(handler)
	handler = middleware.AuthenticatedUserID(handler)
	handler = middleware.PanicLogger(handler)

	return handler
}
