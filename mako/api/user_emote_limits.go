package api

import (
	"context"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/mako/clients"
	mk "code.justin.tv/commerce/mako/twirp"
	mkd "code.justin.tv/commerce/mako/twirp/dashboard"
	"github.com/twitchtv/twirp"
)

type UserEmoteLimitsAPI struct {
	Clients clients.Clients
}

func (u *UserEmoteLimitsAPI) GetUserEmoteLimits(ctx context.Context, r *mkd.GetUserEmoteLimitsRequest) (*mkd.GetUserEmoteLimitsResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, 1250*time.Millisecond)
	defer cancel()

	err := validateGetUserEmoteLimitsRequest(r)
	if err != nil {
		return nil, err
	}

	requestingUserID := r.GetRequestingUserId()
	userID := r.GetUserId()

	logFields := log.Fields{
		"userID":           userID,
		"requestingUserID": requestingUserID,
	}

	// Allow a staff member to check another user's emote limits
	if userID != requestingUserID {
		requestingUser, err := u.Clients.UserServiceClient.GetUserByID(ctx, requestingUserID, nil)
		if err != nil {
			msg := "error getting user for staff check to get user emote limits"
			log.WithError(err).WithFields(logFields).Error(msg)
			return nil, twirp.InternalError(msg)
		}

		if requestingUser.Admin == nil || !*requestingUser.Admin {
			return nil, twirp.NewError(twirp.PermissionDenied, "requesting user not allowed to view user's emote limits")
		}
	}

	userEmoteLimits, err := u.Clients.EmoteLimitsManager.GetUserEmoteLimits(ctx, userID)
	if err != nil {
		msg := "error getting user emote limits"
		log.WithError(err).WithFields(logFields).Error(msg)
		return nil, twirp.InternalError(msg)
	}

	return &mkd.GetUserEmoteLimitsResponse{
		OwnedStaticEmoteLimit:   userEmoteLimits.OwnedStaticEmoteLimit,
		OwnedAnimatedEmoteLimit: userEmoteLimits.OwnedAnimatedEmoteLimit,
	}, nil
}

func validateGetUserEmoteLimitsRequest(r *mkd.GetUserEmoteLimitsRequest) error {
	// Validate required arguments
	if r.GetRequestingUserId() == "" {
		return mk.NewClientError(
			twirp.RequiredArgumentError("requesting_user_id"),
			mk.ErrCodeMissingRequiredArgument)
	} else if r.GetUserId() == "" {
		return mk.NewClientError(
			twirp.RequiredArgumentError("user_id"),
			mk.ErrCodeMissingRequiredArgument)
	}

	return nil
}
