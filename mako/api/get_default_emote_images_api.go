package api

import (
	"context"

	"github.com/twitchtv/twirp"

	"code.justin.tv/commerce/mako/config"
	mkd "code.justin.tv/commerce/mako/twirp/dashboard"
)

type DefaultEmoteImagesAPI struct {
	DefaultEmoteImagesDataDynamicConfig config.DefaultEmoteImagesDataConfigClient
}

func (d *DefaultEmoteImagesAPI) GetDefaultEmoteImages(ctx context.Context, r *mkd.GetDefaultEmoteImagesRequest) (*mkd.GetDefaultEmoteImagesResponse, error) {
	resp, err := d.DefaultEmoteImagesDataDynamicConfig.GetDefaultEmoteImagesData()
	if err != nil {
		return nil, twirp.InternalErrorWith(err)
	}
	return &mkd.GetDefaultEmoteImagesResponse{
		DefaultEmoteImages: resp,
	}, nil
}
