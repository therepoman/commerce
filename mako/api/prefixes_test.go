package api

import (
	"context"
	"errors"
	"testing"
	"time"

	receiver "code.justin.tv/amzn/TwitchDartReceiverTwirp"
	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/mako/clients"
	"code.justin.tv/commerce/mako/config"
	"code.justin.tv/commerce/mako/dynamo"
	receiver_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/amzn/TwitchDartReceiverTwirp"
	ripley_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/ripley"
	salesforce_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/salesforce"
	sns_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/sns"
	dynamo_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/dynamo"
	mako_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/prefixes"
	clients_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/web/users-service/client/usersclient_internal"
	stats_mock "code.justin.tv/commerce/mako/mocks/github.com/cactus/go-statsd-client/statsd"
	"code.justin.tv/commerce/mako/prefixes"
	mk "code.justin.tv/commerce/mako/twirp"
	riptwirp "code.justin.tv/revenue/ripley/rpc"
	"code.justin.tv/web/users-service/models"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
	"github.com/twitchtv/twirp"
)

const (
	EMOTE_UPDATE_PREFIX = "EMOTE_PREFIX_UPDATES!"
)

func TestGetByOwnerId_PrefixAPI(t *testing.T) {
	ctx := context.Background()
	ctx = context.WithValue(ctx, "client-id", "test client id")
	ctx = context.WithValue(ctx, "id", "test device id")

	Convey("Test Prefixes API", t, func() {
		mockPrefixesDao := new(dynamo_mock.IPrefixDao)
		mockRipleyClient := new(ripley_mock.ClientWrapper)
		mockSalesforceClient := new(salesforce_mock.APIClient)
		mockUserServiceClient := new(clients_mock.InternalClient)
		cfg := &config.Configuration{}
		api := NewPrefixesAPI(mockPrefixesDao, cfg, &clients.Clients{RipleyClient: mockRipleyClient, SalesforceClient: mockSalesforceClient, UserServiceClient: mockUserServiceClient}, nil)
		mockSalesforceClient.On("InGoodStanding", mock.Anything, mock.Anything, mock.Anything).Return(pointers.BoolP(true), nil)
		mockRipleyClient.On("GetPayoutType", mock.Anything, "265716088").Return(&riptwirp.PayoutType{
			ChannelId:   "265716088",
			IsAffiliate: false,
			IsPartner:   true,
		}, nil)

		Convey("With a nil request", func() {
			var req *mk.GetPrefixRequest

			resp, err := api.GetPrefix(ctx, req)
			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
			So(err.(twirp.Error).Code(), ShouldEqual, twirp.InvalidArgument)
			So(err.(twirp.Error).Meta("argument"), ShouldEqual, "request")
			So(err.(twirp.Error).Msg(), ShouldEqual, "request cannot be nil")

			Convey("With an empty request", func() {
				req = &mk.GetPrefixRequest{}

				resp, err := api.GetPrefix(ctx, req)
				So(resp, ShouldBeNil)
				So(err, ShouldNotBeNil)
				So(err.(twirp.Error).Code(), ShouldEqual, twirp.InvalidArgument)
				So(err.(twirp.Error).Meta("argument"), ShouldEqual, "channelId")
				So(err.(twirp.Error).Msg(), ShouldEqual, "channelId cannot be empty")

				Convey("With only channel id populated in the request", func() {
					req.ChannelId = "265716088"

					Convey("When the DAO returns an error", func() {
						animeBearError := errors.New("ANIME BEAR OVERFLOW")
						mockPrefixesDao.On("GetByOwnerId", mock.Anything, mock.Anything).Return(nil, animeBearError)

						resp, err := api.GetPrefix(ctx, req)
						So(resp, ShouldBeNil)
						So(err.(twirp.Error).Code(), ShouldEqual, twirp.Internal)
						So(err.(twirp.Error).Msg(), ShouldEqual, "Internal failure getting prefix for channel")
					})

					Convey("when the DAO return a-ok", func() {
						dynamoResp := dynamo.Prefix{
							Prefix: "bear",
							State:  dynamo.PrefixPending,
						}
						mockPrefixesDao.On("GetByOwnerId", mock.Anything, mock.Anything).Return(&dynamoResp, nil)

						resp, err := api.GetPrefix(ctx, req)
						So(err, ShouldBeNil)
						So(resp.Prefix, ShouldEqual, "bear")
						So(resp.State, ShouldEqual, mk.PrefixState_PREFIX_PENDING)
					})

					Convey("when getting editable status fails", func() {
						dynamoResp := dynamo.Prefix{
							Prefix: "bear",
							State:  dynamo.PrefixPending,
						}
						req.ChannelId = "777"
						req.ShouldCheckEditability = true
						mockPrefixesDao.On("GetByOwnerId", mock.Anything, mock.Anything).Return(&dynamoResp, nil)
						mockRipleyClient.On("GetPayoutType", mock.Anything, "777").Return(nil, errors.New("payout type error")).Once()
						resp, err := api.GetPrefix(ctx, req)
						So(err, ShouldBeNil)
						So(resp.Prefix, ShouldEqual, "bear")
						So(resp.State, ShouldEqual, mk.PrefixState_PREFIX_PENDING)
						So(resp.IsEditable, ShouldBeFalse)
					})

					Convey("when the shouldCheckEditability flag is false", func() {
						dynamoResp := dynamo.Prefix{
							Prefix: "bear",
							State:  dynamo.PrefixActive,
						}
						req.ChannelId = "888"
						req.ShouldCheckEditability = false
						mockPrefixesDao.On("GetByOwnerId", mock.Anything, mock.Anything).Return(&dynamoResp, nil)
						mockRipleyClient.On("GetPayoutType", mock.Anything, "888").Return(&riptwirp.PayoutType{
							ChannelId:   "888",
							IsAffiliate: false,
							IsPartner:   true,
						}, nil).Once()
						resp, err := api.GetPrefix(ctx, req)
						So(resp, ShouldNotBeNil)
						So(err, ShouldBeNil)
						So(resp.IsEditable, ShouldEqual, false)
					})
				})
			})
		})
	})
}

func TestGetPrefixesByState_PrefixAPI(t *testing.T) {
	ctx := context.Background()

	Convey("Test Prefixes API", t, func() {
		mockPrefixesDao := new(dynamo_mock.IPrefixDao)
		mockStats := new(stats_mock.Statter)
		cfg := &config.Configuration{}
		api := NewPrefixesAPI(mockPrefixesDao, cfg, &clients.Clients{}, prefixes.NewPrefixRecommender(mockPrefixesDao, mockStats, &clients.Clients{}))

		Convey("With a nil request", func() {
			var req *mk.GetPrefixesByStateRequest

			resp, err := api.GetPrefixesByState(ctx, req)
			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
			So(err.(twirp.Error).Code(), ShouldEqual, twirp.InvalidArgument)
			So(err.(twirp.Error).Meta("argument"), ShouldEqual, "request")
			So(err.(twirp.Error).Msg(), ShouldEqual, "request cannot be nil")
		})

		Convey("With an empty request", func() {
			req := &mk.GetPrefixesByStateRequest{}

			resp, err := api.GetPrefixesByState(ctx, req)
			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
			So(err.(twirp.Error).Code(), ShouldEqual, twirp.InvalidArgument)
			So(err.(twirp.Error).Meta("argument"), ShouldEqual, "state")
			So(err.(twirp.Error).Msg(), ShouldEqual, "state cannot be empty")
		})

		Convey("With state populated in request", func() {
			req := &mk.GetPrefixesByStateRequest{
				State: mk.PrefixState_PREFIX_PENDING,
			}

			Convey("When the DAO returns an error", func() {
				mockPrefixesDao.On("GetByState", mock.Anything, mock.Anything, defaultPrefixesResponseLimit).Return(nil, errors.New("test"))

				resp, err := api.GetPrefixesByState(ctx, req)
				So(resp, ShouldBeNil)
				So(err.(twirp.Error).Code(), ShouldEqual, twirp.Internal)
				So(err.(twirp.Error).Msg(), ShouldEqual, "Internal failure getting prefixes by state")
			})

			Convey("when the DAO return a healthy response", func() {
				mockResp := []*dynamo.Prefix{
					{
						Prefix: "first",
						State:  dynamo.PrefixPending,
					},
					{
						Prefix: "second",
						State:  dynamo.PrefixPending,
					},
				}
				mockPrefixesDao.On("GetByState", mock.Anything, mock.Anything, defaultPrefixesResponseLimit).Return(mockResp, nil)

				resp, err := api.GetPrefixesByState(ctx, req)
				So(err, ShouldBeNil)
				So(len(resp.PrefixEntries), ShouldEqual, 2)
				So(resp.PrefixEntries[0].Prefix, ShouldEqual, "first")
				So(resp.PrefixEntries[0].State, ShouldEqual, mk.PrefixState_PREFIX_PENDING)
				So(resp.PrefixEntries[1].Prefix, ShouldEqual, "second")
				So(resp.PrefixEntries[1].State, ShouldEqual, mk.PrefixState_PREFIX_PENDING)
			})
		})

		Convey("With Limit 3", func() {
			limit := 3
			req := &mk.GetPrefixesByStateRequest{
				State: mk.PrefixState_PREFIX_PENDING,
				Limit: int32(limit),
			}

			numReturned := 3
			mockResp := generatePrefixes(numReturned)
			mockPrefixesDao.On("GetByState", mock.Anything, mock.Anything, limit).Return(mockResp, nil)

			resp, err := api.GetPrefixesByState(ctx, req)
			So(err, ShouldBeNil)
			So(len(resp.PrefixEntries), ShouldEqual, numReturned)
			So(resp.PrefixEntries[0].Prefix, ShouldEqual, "generated")
			So(resp.PrefixEntries[0].State, ShouldEqual, mk.PrefixState_PREFIX_PENDING)
			So(resp.PrefixEntries[1].Prefix, ShouldEqual, "generated")
			So(resp.PrefixEntries[1].State, ShouldEqual, mk.PrefixState_PREFIX_PENDING)
		})

		Convey("With Limit -7, use default limit", func() {
			limit := -7
			req := &mk.GetPrefixesByStateRequest{
				State: mk.PrefixState_PREFIX_PENDING,
				Limit: int32(limit),
			}

			numReturned := 5
			mockResp := generatePrefixes(numReturned)
			mockPrefixesDao.On("GetByState", mock.Anything, mock.Anything, defaultPrefixesResponseLimit).Return(mockResp, nil)

			resp, err := api.GetPrefixesByState(ctx, req)
			So(err, ShouldBeNil)
			So(len(resp.PrefixEntries), ShouldEqual, numReturned)
			So(resp.PrefixEntries[0].Prefix, ShouldEqual, "generated")
			So(resp.PrefixEntries[0].State, ShouldEqual, mk.PrefixState_PREFIX_PENDING)
			So(resp.PrefixEntries[1].Prefix, ShouldEqual, "generated")
			So(resp.PrefixEntries[1].State, ShouldEqual, mk.PrefixState_PREFIX_PENDING)
		})
	})
}

func generatePrefixes(count int) []*dynamo.Prefix {
	resp := []*dynamo.Prefix{}
	for i := 0; i < count; i++ {
		resp = append(resp, &dynamo.Prefix{
			Prefix: "generated",
			State:  dynamo.PrefixPending,
		})
	}
	return resp
}

func TestSet_PrefixAPI(t *testing.T) {
	ctx := context.Background()

	Convey("Test Prefixes API", t, func() {
		mockPrefixesDao := new(dynamo_mock.IPrefixDao)
		mockSnsClient := new(sns_mock.ISNSClient)
		mockDartReceiverClient := new(receiver_mock.Receiver)
		cfg := &config.Configuration{
			PrefixUpdatesSNS: EMOTE_UPDATE_PREFIX,
		}

		clients := clients.Clients{
			SnsClient:          mockSnsClient,
			DartReceiverClient: mockDartReceiverClient,
		}
		api := NewPrefixesAPI(mockPrefixesDao, cfg, &clients, nil)

		Convey("With a nil request", func() {
			var req *mk.SetPrefixRequest

			resp, err := api.SetPrefix(ctx, req)
			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
			So(err.(twirp.Error).Code(), ShouldEqual, twirp.InvalidArgument)
			So(err.(twirp.Error).Meta("argument"), ShouldEqual, "request")
			So(err.(twirp.Error).Msg(), ShouldEqual, "request cannot be nil")

			Convey("With an empty request", func() {
				req = &mk.SetPrefixRequest{}

				resp, err := api.SetPrefix(ctx, req)
				So(resp, ShouldBeNil)
				So(err, ShouldNotBeNil)
				So(err.(twirp.Error).Code(), ShouldEqual, twirp.InvalidArgument)
				So(err.(twirp.Error).Meta("argument"), ShouldEqual, "channelId")
				So(err.(twirp.Error).Msg(), ShouldEqual, "channelId cannot be empty")

				Convey("With only channel id populated in the request", func() {
					req.ChannelId = "265716088"

					resp, err := api.SetPrefix(ctx, req)
					So(resp, ShouldBeNil)
					So(err, ShouldNotBeNil)
					So(err.(twirp.Error).Code(), ShouldEqual, twirp.InvalidArgument)
					So(err.(twirp.Error).Meta("argument"), ShouldEqual, "prefix")
					So(err.(twirp.Error).Msg(), ShouldEqual, "prefix cannot be empty")

					Convey("With prefix populated in the request", func() {
						req.Prefix = "qabonta"

						resp, err := api.SetPrefix(ctx, req)
						So(resp, ShouldBeNil)
						So(err, ShouldNotBeNil)
						So(err.(twirp.Error).Code(), ShouldEqual, twirp.InvalidArgument)
						So(err.(twirp.Error).Meta("argument"), ShouldEqual, "state")
						So(err.(twirp.Error).Msg(), ShouldEqual, "state cannot be empty")

						Convey("With state populated in the request", func() {
							req.State = mk.PrefixState_PREFIX_ACTIVE
							mockPrefixesDao.On("Get", mock.Anything, mock.Anything).Return(nil, nil)

							Convey("When the Prefix DAO returns an internal error", func() {
								animeBearError := errors.New("ANIME BEAR INVASION")
								mockPrefixesDao.On("Put", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil, animeBearError)

								resp, err := api.SetPrefix(ctx, req)
								So(resp, ShouldBeNil)
								So(err.(twirp.Error).Code(), ShouldEqual, twirp.Internal)
								So(err.(twirp.Error).Msg(), ShouldEqual, "Internal failure updating prefix for channel")
							})

							Convey("When the Prefix DAO returns error for prefix already taken", func() {
								mockPrefixesDao.On("Put", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil, dynamo.PrefixAlreadyTaken)

								resp, err := api.SetPrefix(ctx, req)
								So(resp, ShouldBeNil)
								So(err.(twirp.Error).Code(), ShouldEqual, twirp.InvalidArgument)
								So(err.(twirp.Error).Msg(), ShouldEqual, "prefix already taken")
							})

							Convey("When the Prefix DAO returns ok", func() {
								mockPrefixesDao.On("Put", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil, nil)

								Convey("When the SNS topic is emitted is confronted with failure", func() {
									animeBearError := errors.New("ANIME BEAR CONFUSION")
									mockSnsClient.On("PostToTopic", mock.Anything, EMOTE_UPDATE_PREFIX, mock.Anything).Return(animeBearError)

									resp, err := api.SetPrefix(ctx, req)
									So(resp, ShouldBeNil)
									So(err.(twirp.Error).Code(), ShouldEqual, twirp.Internal)
									So(err.(twirp.Error).Msg(), ShouldEqual, "Internal failure updating emotes with prefix change")
								})

								Convey("When the SNS topic is emitted with success!", func() {
									mockSnsClient.On("PostToTopic", mock.Anything, EMOTE_UPDATE_PREFIX, mock.Anything).Return(nil)

									Convey("When the email should not be sent because state is pending", func() {
										req.State = mk.PrefixState_PREFIX_PENDING

										resp, err := api.SetPrefix(ctx, req)

										So(resp, ShouldNotBeNil)
										So(err, ShouldBeNil)
										mockDartReceiverClient.AssertExpectations(t)
									})

									Convey("When the Dart returns a failure", func() {
										mockDartReceiverClient.On("PublishNotification", mock.Anything, mock.Anything).Return(nil, errors.New("test"))

										resp, err := api.SetPrefix(ctx, req)

										So(resp, ShouldBeNil)
										So(err.(twirp.Error).Code(), ShouldEqual, twirp.Internal)
										So(err.(twirp.Error).Msg(), ShouldEqual, "Internal failure publishing email for prefix change")
										mockDartReceiverClient.AssertExpectations(t)
									})

									Convey("When the Dart returns a success", func() {
										mockDartReceiverClient.On("PublishNotification", mock.Anything, mock.Anything).Return(&receiver.PublishNotificationResponse{}, nil)

										resp, err := api.SetPrefix(ctx, req)

										So(resp, ShouldNotBeNil)
										So(err, ShouldBeNil)
										mockDartReceiverClient.AssertExpectations(t)
									})
								})
							})
						})
					})
				})
			})
		})
		Convey("GIVEN a prefix that is too short WHEN SetPrefix is called THEN expect an invalid argument error", func() {
			req := &mk.SetPrefixRequest{
				ChannelId: "1234",
				Prefix:    "no",
				State:     mk.PrefixState_PREFIX_ACTIVE,
			}

			resp, err := api.SetPrefix(ctx, req)
			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
			So(err.(twirp.Error).Code(), ShouldEqual, twirp.InvalidArgument)
			So(err.(twirp.Error).Msg(), ShouldEqual, "prefix must be between 3 and 10 characters")
		})

		Convey("GIVEN a prefix that is too long WHEN SetPrefix is called THEN expect an invalid argument error", func() {
			req := &mk.SetPrefixRequest{
				ChannelId: "1234",
				Prefix:    "nononononononononono",
				State:     mk.PrefixState_PREFIX_ACTIVE,
			}

			resp, err := api.SetPrefix(ctx, req)
			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
			So(err.(twirp.Error).Code(), ShouldEqual, twirp.InvalidArgument)
			So(err.(twirp.Error).Msg(), ShouldEqual, "prefix must be between 3 and 10 characters")
		})

		Convey("GIVEN a prefix that begins with a capital letter WHEN SetPrefix is called THEN expect an invalid argument error", func() {
			req := &mk.SetPrefixRequest{
				ChannelId: "1234",
				Prefix:    "Nonono",
				State:     mk.PrefixState_PREFIX_ACTIVE,
			}

			resp, err := api.SetPrefix(ctx, req)
			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
			So(err.(twirp.Error).Code(), ShouldEqual, twirp.InvalidArgument)
			So(err.(twirp.Error).Msg(), ShouldEqual, "prefix must start with a letter and only contain letters or numbers")
		})

		Convey("GIVEN a prefix that contains a capital letter WHEN SetPrefix is called THEN expect an invalid argument error", func() {
			req := &mk.SetPrefixRequest{
				ChannelId: "1234",
				Prefix:    "noNono",
				State:     mk.PrefixState_PREFIX_ACTIVE,
			}

			resp, err := api.SetPrefix(ctx, req)
			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
			So(err.(twirp.Error).Code(), ShouldEqual, twirp.InvalidArgument)
			So(err.(twirp.Error).Msg(), ShouldEqual, "prefix must start with a letter and only contain letters or numbers")
		})

		Convey("GIVEN a prefix that starts with a number WHEN SetPrefix is called THEN expect an invalid argument error", func() {
			req := &mk.SetPrefixRequest{
				ChannelId: "1234",
				Prefix:    "0noNono",
				State:     mk.PrefixState_PREFIX_ACTIVE,
			}

			resp, err := api.SetPrefix(ctx, req)
			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
			So(err.(twirp.Error).Code(), ShouldEqual, twirp.InvalidArgument)
			So(err.(twirp.Error).Msg(), ShouldEqual, "prefix must start with a letter and only contain letters or numbers")
		})

		Convey("GIVEN a prefix that starts with a non-alphanumeric character WHEN SetPrefix is called THEN expect an invalid argument error", func() {
			req := &mk.SetPrefixRequest{
				ChannelId: "1234",
				Prefix:    " nonono",
				State:     mk.PrefixState_PREFIX_ACTIVE,
			}

			resp, err := api.SetPrefix(ctx, req)
			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
			So(err.(twirp.Error).Code(), ShouldEqual, twirp.InvalidArgument)
			So(err.(twirp.Error).Msg(), ShouldEqual, "prefix must start with a letter and only contain letters or numbers")
		})
	})
}

func TestSubmitPrefix_PrefixAPI(t *testing.T) {
	ctx := context.Background()

	Convey("Test Prefixes API", t, func() {
		mockPrefixesDao := new(dynamo_mock.IPrefixDao)
		mockSnsClient := new(sns_mock.ISNSClient)
		mockRipleyClient := new(ripley_mock.ClientWrapper)
		mockSalesforceClient := new(salesforce_mock.APIClient)
		mockUserServiceClient := new(clients_mock.InternalClient)
		mockRecommender := new(mako_mock.PrefixRecommender)
		mockRecommender.On("GetRecommendedEmoticonPrefix", mock.Anything, mock.Anything).Return("thereal", nil)

		cfg := &config.Configuration{
			PrefixUpdatesSNS: EMOTE_UPDATE_PREFIX,
		}

		api := NewPrefixesAPI(mockPrefixesDao, cfg, &clients.Clients{SnsClient: mockSnsClient, RipleyClient: mockRipleyClient, SalesforceClient: mockSalesforceClient, UserServiceClient: mockUserServiceClient}, mockRecommender)

		Convey("With a nil request", func() {
			var req *mk.SubmitPrefixRequest

			resp, err := api.SubmitPrefix(ctx, req)
			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
			So(err.(twirp.Error).Code(), ShouldEqual, twirp.InvalidArgument)
			So(err.(twirp.Error).Meta("argument"), ShouldEqual, "request")
			So(err.(twirp.Error).Msg(), ShouldEqual, "request cannot be nil")

			Convey("With an empty request", func() {
				req = &mk.SubmitPrefixRequest{}
				resp, err := api.SubmitPrefix(ctx, req)
				So(resp, ShouldBeNil)
				So(err, ShouldNotBeNil)
				So(err.(twirp.Error).Code(), ShouldEqual, twirp.InvalidArgument)
				So(err.(twirp.Error).Meta("argument"), ShouldEqual, "channelId")
				So(err.(twirp.Error).Msg(), ShouldEqual, "channelId cannot be empty")

				Convey("With only channelId in the request", func() {
					req = &mk.SubmitPrefixRequest{
						ChannelId: "666",
					}
					resp, err := api.SubmitPrefix(ctx, req)
					So(resp, ShouldBeNil)
					So(err, ShouldNotBeNil)
					So(err.(twirp.Error).Code(), ShouldEqual, twirp.InvalidArgument)
					So(err.(twirp.Error).Meta("argument"), ShouldEqual, "prefix")
					So(err.(twirp.Error).Msg(), ShouldEqual, "prefix cannot be empty")
					Convey("With valid request with unique prefix", func() {
						req = &mk.SubmitPrefixRequest{
							ChannelId: "666",
							Prefix:    "thereal",
						}
						mockPrefixesDao.On("Get", mock.Anything, mock.Anything).Return(nil, nil)
						Convey("When prefixMetadata errors", func() {
							Convey("When ripley errors", func() {
								mockRipleyClient.On("GetPayoutType", mock.Anything, "666").Return(nil, errors.New("ripley error")).Once()
								resp, err := api.SubmitPrefix(ctx, req)
								So(resp, ShouldBeNil)
								So(err, ShouldNotBeNil)
								So(err.(twirp.Error).Code(), ShouldEqual, twirp.Internal)
							})
							Convey("When user is not partner or affiliate", func() {
								mockRipleyClient.On("GetPayoutType", mock.Anything, "666").Return(&riptwirp.PayoutType{
									ChannelId:   "666",
									IsAffiliate: false,
									IsPartner:   false,
								}, nil).Once()
								resp, err := api.SubmitPrefix(ctx, req)
								So(resp, ShouldBeNil)
								So(err, ShouldNotBeNil)
								errorCode, parseErr := getClientError(err)
								So(parseErr, ShouldBeNil)
								So(errorCode, ShouldEqual, mk.ErrCodeInvalidUser)
							})
							Convey("When user is a partner and fail to get good standing status", func() {
								mockRipleyClient.On("GetPayoutType", mock.Anything, "666").Return(&riptwirp.PayoutType{
									ChannelId:   "666",
									IsAffiliate: false,
									IsPartner:   true,
								}, nil).Once()
								mockSalesforceClient.On("InGoodStanding", mock.Anything, mock.Anything).Return(pointers.BoolP(false), errors.New("salesforce error"))
								resp, err := api.SubmitPrefix(ctx, req)
								So(resp, ShouldBeNil)
								So(err, ShouldNotBeNil)
								So(err.(twirp.Error).Code(), ShouldEqual, twirp.Internal)
								So(err.(twirp.Error).Msg(), ShouldEqual, "Internal error submitting prefix")
							})

							Convey("When getting existing prefix fails", func() {
								mockRipleyClient.On("GetPayoutType", mock.Anything, "666").Return(&riptwirp.PayoutType{
									ChannelId:   "666",
									IsAffiliate: false,
									IsPartner:   true,
								}, nil).Once()

								mockSalesforceClient.On("InGoodStanding", mock.Anything, mock.Anything).Return(pointers.BoolP(true), nil)
								mockPrefixesDao.On("GetByOwnerId", mock.Anything, mock.Anything).Return(nil, errors.New("error getting existing prefix"))
								resp, err := api.SubmitPrefix(ctx, req)
								So(resp, ShouldBeNil)
								So(err, ShouldNotBeNil)
								So(err.(twirp.Error).Code(), ShouldEqual, twirp.Internal)
							})
							Convey("When prefix does not exist", func() {
								mockRipleyClient.On("GetPayoutType", mock.Anything, "666").Return(&riptwirp.PayoutType{
									ChannelId:   "666",
									IsAffiliate: false,
									IsPartner:   true,
								}, nil).Once()
								mockPrefixesDao.On("Put", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(&dynamo.Prefix{
									OwnerId: "666",
									Prefix:  "thereal",
									State:   "active",
								}, nil).Once()
								mockSnsClient.On("PostToTopic", mock.Anything, mock.Anything, mock.Anything).Return(nil)

								mockSalesforceClient.On("InGoodStanding", mock.Anything, mock.Anything).Return(pointers.BoolP(true), nil)
								mockPrefixesDao.On("GetByOwnerId", mock.Anything, mock.Anything).Return(nil, nil)
								resp, err := api.SubmitPrefix(ctx, req)
								So(err, ShouldBeNil)
								So(resp, ShouldNotBeNil)
								mockPrefixesDao.AssertCalled(t, "Put", mock.Anything, mock.Anything, mock.Anything, dynamo.PrefixActive)
							})

							Convey("When existing prefix exists", func() {
								mockRipleyClient.On("GetPayoutType", mock.Anything, "666").Return(&riptwirp.PayoutType{
									ChannelId:   "666",
									IsAffiliate: false,
									IsPartner:   true,
								}, nil).Once()
								mockPrefixesDao.On("Put", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(&dynamo.Prefix{
									OwnerId: "666",
									Prefix:  "thereal",
									State:   "pending",
								}, nil).Once()
								mockSnsClient.On("PostToTopic", mock.Anything, mock.Anything, mock.Anything).Return(nil)
								Convey("When existing prefix state is set to rejected", func() {
									mockPrefixesDao.On("GetByOwnerId", mock.Anything, mock.Anything).Return(&dynamo.Prefix{
										OwnerId:     "666",
										Prefix:      "thereal",
										State:       "rejected",
										LastUpdated: time.Now(),
									}, nil).Once()

									mockSalesforceClient.On("InGoodStanding", mock.Anything, mock.Anything).Return(pointers.BoolP(false), nil).Once()
									resp, err := api.SubmitPrefix(ctx, req)
									So(resp, ShouldNotBeNil)
									So(err, ShouldBeNil)
									mockPrefixesDao.AssertCalled(t, "Put", mock.Anything, mock.Anything, mock.Anything, dynamo.PrefixPending)
								})
								Convey("When existing prefix state is set to unset", func() {
									Convey("When existing prefix state is unset and in good standing", func() {
										mockSalesforceClient.On("InGoodStanding", mock.Anything, mock.Anything).Return(pointers.BoolP(true), nil).Once()
										mockPrefixesDao.On("GetByOwnerId", mock.Anything, mock.Anything).Return(&dynamo.Prefix{
											OwnerId:     "666",
											Prefix:      "thereal",
											State:       "unset",
											LastUpdated: time.Now(),
										}, nil).Once()
										resp, err := api.SubmitPrefix(ctx, req)
										So(resp, ShouldNotBeNil)
										So(err, ShouldBeNil)
										mockPrefixesDao.AssertCalled(t, "Put", mock.Anything, mock.Anything, mock.Anything, dynamo.PrefixActive)
									})
									Convey("When existing prefix state is unset and NOT in good standing", func() {
										mockSalesforceClient.On("InGoodStanding", mock.Anything, mock.Anything).Return(pointers.BoolP(false), nil).Once()
										mockPrefixesDao.On("GetByOwnerId", mock.Anything, mock.Anything).Return(&dynamo.Prefix{
											OwnerId:     "666",
											Prefix:      "thereal",
											State:       "unset",
											LastUpdated: time.Now(),
										}, nil).Once()
										resp, err := api.SubmitPrefix(ctx, req)
										So(resp, ShouldNotBeNil)
										So(err, ShouldBeNil)
										mockPrefixesDao.AssertCalled(t, "Put", mock.Anything, mock.Anything, mock.Anything, dynamo.PrefixPending)
									})
								})
								Convey("When existing prefix state is set to active", func() {
									Convey("When user is partner", func() {
										mockRipleyClient.On("GetPayoutType", mock.Anything, "666").Return(&riptwirp.PayoutType{
											ChannelId:   "666",
											IsAffiliate: false,
											IsPartner:   true,
										}, nil).Once()
										Convey("When not in good standing", func() {
											mockPrefixesDao.On("GetByOwnerId", mock.Anything, mock.Anything).Return(&dynamo.Prefix{
												OwnerId:     "666",
												Prefix:      "thereal",
												State:       "active",
												LastUpdated: time.Now(),
											}, nil).Once()
											mockSalesforceClient.On("InGoodStanding", mock.Anything, mock.Anything).Return(pointers.BoolP(false), nil).Once()
											resp, err := api.SubmitPrefix(ctx, req)
											So(resp, ShouldBeNil)
											errorCode, parseErr := getClientError(err)
											So(parseErr, ShouldBeNil)
											So(errorCode, ShouldEqual, mk.ErrCodeNotInGoodStanding)
										})

										Convey("When last updated was less than 60 days ago", func() {
											mockPrefixesDao.On("GetByOwnerId", mock.Anything, mock.Anything).Return(&dynamo.Prefix{
												OwnerId:     "666",
												Prefix:      "thereal",
												State:       "active",
												LastUpdated: time.Now(),
											}, nil).Once()
											mockSalesforceClient.On("InGoodStanding", mock.Anything, mock.Anything).Return(pointers.BoolP(true), nil).Once()
											resp, err := api.SubmitPrefix(ctx, req)
											So(resp, ShouldBeNil)
											errorCode, parseErr := getClientError(err)
											So(parseErr, ShouldBeNil)
											So(errorCode, ShouldEqual, mk.ErrCodePrefixUpdateTooSoon)
										})

										Convey("When last updated was less than 60 days ago but user is on override list", func() {
											mockPrefixesDao.On("GetByOwnerId", mock.Anything, mock.Anything).Return(&dynamo.Prefix{
												OwnerId:     "666",
												Prefix:      "thereal",
												State:       "active",
												LastUpdated: time.Now(),
											}, nil).Once()
											mockSalesforceClient.On("InGoodStanding", mock.Anything, mock.Anything).Return(pointers.BoolP(true), nil).Once()
											cfg.PrefixEditabilityOverrideList = map[string]bool{"666": true}
											resp, err := api.SubmitPrefix(ctx, req)
											So(resp, ShouldNotBeNil)
											So(err, ShouldBeNil)
										})

										Convey("When last update was more than 60 days ago", func() {
											mockSalesforceClient.On("InGoodStanding", mock.Anything, mock.Anything).Return(pointers.BoolP(true), nil).Once()
											mockPrefixesDao.On("GetByOwnerId", mock.Anything, mock.Anything).Return(&dynamo.Prefix{
												OwnerId:     "666",
												Prefix:      "thereal",
												State:       "active",
												LastUpdated: time.Now().AddDate(0, 0, -100),
											}, nil).Once()
											resp, err := api.SubmitPrefix(ctx, req)
											So(resp, ShouldNotBeNil)
											So(err, ShouldBeNil)
										})

									})
									Convey("When user is an affiliate", func() {
										req = &mk.SubmitPrefixRequest{
											ChannelId: "777",
											Prefix:    "thereal",
										}

										Convey("When existing prefix is active and user is affiliate and userservice returns error", func() {
											mockRipleyClient.On("GetPayoutType", mock.Anything, "777").Return(&riptwirp.PayoutType{
												ChannelId:   "777",
												IsAffiliate: true,
												IsPartner:   false,
											}, nil).Once()
											mockPrefixesDao.On("GetByOwnerId", mock.Anything, mock.Anything).Return(&dynamo.Prefix{
												OwnerId:     "777",
												Prefix:      "thereal",
												State:       "active",
												LastUpdated: time.Now(),
											}, nil).Once()
											mockUserServiceClient.On("GetUserByID", mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("userservice err")).Once()
											resp, err := api.SubmitPrefix(ctx, req)
											So(resp, ShouldBeNil)
											So(err.(twirp.Error).Code(), ShouldEqual, twirp.Internal)
											So(err.(twirp.Error).Msg(), ShouldEqual, "Internal error submitting prefix")
										})
										Convey("When existing prefix is active and user is affiliate and user has not ever changed their name", func() {
											mockRipleyClient.On("GetPayoutType", mock.Anything, "777").Return(&riptwirp.PayoutType{
												ChannelId:   "777",
												IsAffiliate: true,
												IsPartner:   false,
											}, nil).Once()
											mockPrefixesDao.On("GetByOwnerId", mock.Anything, mock.Anything).Return(&dynamo.Prefix{
												OwnerId:     "777",
												Prefix:      "thereal",
												State:       "active",
												LastUpdated: time.Now(),
											}, nil).Once()
											mockUserServiceClient.On("GetUserByID", mock.Anything, mock.Anything, mock.Anything).Return(&models.Properties{
												Login:               pointers.StringP("thereallittlemistershadowgamer"),
												LastLoginChangeDate: nil,
												CreatedOn:           pointers.TimeP(time.Now()),
											}, nil).Once()
											resp, err := api.SubmitPrefix(ctx, req)
											So(resp, ShouldBeNil)
											So(err, ShouldNotBeNil)
											errorCode, parseErr := getClientError(err)
											So(parseErr, ShouldBeNil)
											So(errorCode, ShouldEqual, mk.ErrCodeAffiliatePrefixUpdate)
										})
										Convey("When existing prefix is active and user is affiliate and user has not changed their name since last prefix update", func() {
											mockRipleyClient.On("GetPayoutType", mock.Anything, "777").Return(&riptwirp.PayoutType{
												ChannelId:   "777",
												IsAffiliate: true,
												IsPartner:   false,
											}, nil).Once()
											mockPrefixesDao.On("GetByOwnerId", mock.Anything, mock.Anything).Return(&dynamo.Prefix{
												OwnerId:     "777",
												Prefix:      "thereal",
												State:       "active",
												LastUpdated: time.Now(),
											}, nil).Once()
											mockUserServiceClient.On("GetUserByID", mock.Anything, mock.Anything, mock.Anything).Return(&models.Properties{
												Login:               pointers.StringP("thereallittlemistershadowgamer"),
												LastLoginChangeDate: pointers.TimeP(time.Now().AddDate(0, 0, -10)),
												CreatedOn:           pointers.TimeP(time.Now().AddDate(0, 0, -20)),
											}, nil).Once()
											resp, err := api.SubmitPrefix(ctx, req)

											So(resp, ShouldBeNil)
											So(err, ShouldNotBeNil)
											errorCode, parseErr := getClientError(err)
											So(parseErr, ShouldBeNil)
											So(errorCode, ShouldEqual, mk.ErrCodeAffiliatePrefixUpdate)
										})

										Convey("When existing prefix is active and user is affiliate and user has not changed their name since last prefix update but on override list", func() {
											mockRipleyClient.On("GetPayoutType", mock.Anything, "777").Return(&riptwirp.PayoutType{
												ChannelId:   "777",
												IsAffiliate: true,
												IsPartner:   false,
											}, nil).Once()
											mockPrefixesDao.On("GetByOwnerId", mock.Anything, mock.Anything).Return(&dynamo.Prefix{
												OwnerId:     "777",
												Prefix:      "thereal",
												State:       "active",
												LastUpdated: time.Now(),
											}, nil).Once()
											cfg.PrefixEditabilityOverrideList = map[string]bool{"777": true}
											resp, err := api.SubmitPrefix(ctx, req)

											So(resp, ShouldNotBeNil)
											So(err, ShouldBeNil)
										})

										Convey("When existing prefix is active and user is affiliate and user has not updated their prefix since their last name change", func() {
											mockRipleyClient.On("GetPayoutType", mock.Anything, "777").Return(&riptwirp.PayoutType{
												ChannelId:   "777",
												IsAffiliate: true,
												IsPartner:   false,
											}, nil).Once()
											mockPrefixesDao.On("GetByOwnerId", mock.Anything, mock.Anything).Return(&dynamo.Prefix{
												OwnerId:     "777",
												Prefix:      "thereal",
												State:       "active",
												LastUpdated: time.Now().AddDate(0, 0, -150),
											}, nil).Once()
											mockUserServiceClient.On("GetUserByID", mock.Anything, "777", mock.Anything).Return(&models.Properties{
												Login:               pointers.StringP("thereallittlemistershadowgamer"),
												LastLoginChangeDate: pointers.TimeP(time.Now().AddDate(0, 0, -100)),
												CreatedOn:           pointers.TimeP(time.Now().AddDate(0, 0, -200)),
											}, nil).Once()
											resp, err := api.SubmitPrefix(ctx, req)

											So(resp, ShouldNotBeNil)
											So(err, ShouldBeNil)
										})
									})
								})

								Convey("When existing prefix state is unknown", func() {
									mockSalesforceClient.On("InGoodStanding", mock.Anything, mock.Anything).Return(pointers.BoolP(true), nil).Once()
									mockPrefixesDao.On("GetByOwnerId", mock.Anything, mock.Anything).Return(&dynamo.Prefix{
										OwnerId:     "666",
										Prefix:      "thereal",
										State:       "unknown",
										LastUpdated: time.Now(),
									}, nil).Once()
									resp, err := api.SubmitPrefix(ctx, req)
									So(resp, ShouldBeNil)
									So(err.(twirp.Error).Code(), ShouldEqual, twirp.Internal)
									So(err.(twirp.Error).Msg(), ShouldEqual, "Internal error submitting prefix")
								})
							})
						})
						Convey("When posting to sns errors", func() {
							mockRipleyClient.On("GetPayoutType", mock.Anything, "666").Return(&riptwirp.PayoutType{
								ChannelId:   "666",
								IsAffiliate: false,
								IsPartner:   true,
							}, nil).Once()
							mockSalesforceClient.On("InGoodStanding", mock.Anything, mock.Anything).Return(pointers.BoolP(true), nil).Once()
							mockSnsClient.On("PostToTopic", mock.Anything, mock.Anything, mock.Anything).Return(errors.New("sns error"))
							mockPrefixesDao.On("GetByOwnerId", mock.Anything, mock.Anything).Return(&dynamo.Prefix{
								OwnerId:     "666",
								Prefix:      "thereal",
								State:       "active",
								LastUpdated: time.Now().AddDate(0, 0, -150),
							}, nil).Once()
							mockPrefixesDao.On("Put", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(&dynamo.Prefix{
								OwnerId: "666",
								Prefix:  "thereal",
								State:   "active",
							}, nil).Once()
							resp, err := api.SubmitPrefix(ctx, req)
							So(resp, ShouldBeNil)
							So(err.(twirp.Error).Code(), ShouldEqual, twirp.Internal)
							So(err.(twirp.Error).Msg(), ShouldEqual, "Internal failure updating emotes with prefix change")
						})
						Convey("When success", func() {
							mockRipleyClient.On("GetPayoutType", mock.Anything, "666").Return(&riptwirp.PayoutType{
								ChannelId:   "666",
								IsAffiliate: false,
								IsPartner:   true,
							}, nil).Once()
							mockSalesforceClient.On("InGoodStanding", mock.Anything, mock.Anything).Return(pointers.BoolP(true), nil).Once()
							mockSnsClient.On("PostToTopic", mock.Anything, mock.Anything, mock.Anything).Return(nil)
							mockPrefixesDao.On("GetByOwnerId", mock.Anything, mock.Anything).Return(&dynamo.Prefix{
								OwnerId:     "666",
								Prefix:      "thereal",
								State:       "active",
								LastUpdated: time.Now().AddDate(0, 0, -150),
							}, nil).Once()
							mockPrefixesDao.On("Put", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(&dynamo.Prefix{
								OwnerId: "666",
								Prefix:  "thereal",
								State:   "active",
							}, nil).Once()
							resp, err := api.SubmitPrefix(ctx, req)
							So(resp, ShouldNotBeNil)
							So(err, ShouldBeNil)
						})
					})
					Convey("With valid request with non-unique prefix", func() {
						req = &mk.SubmitPrefixRequest{
							ChannelId: "666",
							Prefix:    "thereal",
						}
						mockPrefixesDao.On("Get", mock.Anything, mock.Anything).Return(&dynamo.Prefix{
							OwnerId:     req.ChannelId,
							Prefix:      req.Prefix,
							State:       "active",
							LastUpdated: time.Time{},
						}, nil)
						Convey("When submitted prefix is owned by the requesting channel", func() {
							mockRipleyClient.On("GetPayoutType", mock.Anything, "666").Return(&riptwirp.PayoutType{
								ChannelId:   "666",
								IsAffiliate: false,
								IsPartner:   true,
							}, nil).Once()
							mockSalesforceClient.On("InGoodStanding", mock.Anything, mock.Anything).Return(pointers.BoolP(true), nil).Once()
							mockSnsClient.On("PostToTopic", mock.Anything, mock.Anything, mock.Anything).Return(nil)
							mockPrefixesDao.On("GetByOwnerId", mock.Anything, mock.Anything).Return(&dynamo.Prefix{
								OwnerId:     "666",
								Prefix:      "thereal",
								State:       "active",
								LastUpdated: time.Now().AddDate(0, 0, -150),
							}, nil).Once()
							mockPrefixesDao.On("Put", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(&dynamo.Prefix{
								OwnerId: "666",
								Prefix:  "thereal",
								State:   "active",
							}, nil).Once()
							resp, err := api.SubmitPrefix(ctx, req)
							So(resp, ShouldNotBeNil)
							So(err, ShouldBeNil)
						})
						Convey("When submitted prefix is already owned by a different channel", func() {
							mockRipleyClient.On("GetPayoutType", mock.Anything, "777").Return(&riptwirp.PayoutType{
								ChannelId:   "777",
								IsAffiliate: false,
								IsPartner:   true,
							}, nil).Once()
							mockSalesforceClient.On("InGoodStanding", mock.Anything, mock.Anything).Return(pointers.BoolP(true), nil).Once()
							mockSnsClient.On("PostToTopic", mock.Anything, mock.Anything, mock.Anything).Return(nil)
							mockPrefixesDao.On("GetByOwnerId", mock.Anything, mock.Anything).Return(&dynamo.Prefix{
								OwnerId:     "777",
								Prefix:      "thereal",
								State:       "active",
								LastUpdated: time.Now().AddDate(0, 0, -150),
							}, nil).Once()
							mockPrefixesDao.On("Put", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(&dynamo.Prefix{
								OwnerId: "777",
								Prefix:  "thereal",
								State:   "active",
							}, nil).Once()
							resp, err := api.SubmitPrefix(ctx, &mk.SubmitPrefixRequest{
								ChannelId: "777",
								Prefix:    "thereal",
							})
							So(resp, ShouldBeNil)
							So(err, ShouldNotBeNil)
							So(err.(twirp.Error).Code(), ShouldEqual, twirp.InvalidArgument)
							So(err.(twirp.Error).Meta("argument"), ShouldEqual, "prefix")
							So(err.(twirp.Error).Msg(), ShouldEqual, "prefix already exists")
						})
					})
				})
			})
		})
	})
}

func getClientError(err error) (string, error) {
	var clientError mk.ClientError
	parseErr := mk.ParseClientError(err, &clientError)
	if parseErr != nil {
		return "", parseErr
	}
	return clientError.ErrorCode, nil
}
