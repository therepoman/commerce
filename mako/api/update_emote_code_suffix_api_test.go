package api

import (
	"context"
	"testing"

	"code.justin.tv/commerce/mako/clients"
	emoticonsmanager_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/emoticonsmanager"
	mk "code.justin.tv/commerce/mako/twirp"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestUpdateEmoteCodeSuffixAPI(t *testing.T) {
	Convey("Test UpdateEmoteCodeSuffixAPI", t, func() {
		mockEmoticon := new(emoticonsmanager_mock.IEmoticonsManager)
		updateEmoticonCodeAPI := &UpdateEmoteCodeSuffixAPI{
			Clients: clients.Clients{
				EmoticonsManager: mockEmoticon,
			},
		}

		Convey("With empty id", func() {
			req := &mk.UpdateEmoteCodeSuffixRequest{
				Id:         "",
				CodeSuffix: "",
			}
			_, err := updateEmoticonCodeAPI.UpdateEmoteCodeSuffix(context.Background(), req)
			So(err, ShouldNotBeNil)
		})

		Convey("With empty codesuffix", func() {
			req := &mk.UpdateEmoteCodeSuffixRequest{
				Id:         "1234",
				CodeSuffix: "",
			}
			_, err := updateEmoticonCodeAPI.UpdateEmoteCodeSuffix(context.Background(), req)
			So(err, ShouldNotBeNil)
		})
		Convey("With success", func() {
			req := &mk.UpdateEmoteCodeSuffixRequest{
				Id:         "1234",
				CodeSuffix: "1234",
			}
			mockEmoticon.On("UpdateEmoteCodeSuffix", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)
			_, err := updateEmoticonCodeAPI.UpdateEmoteCodeSuffix(context.Background(), req)
			So(err, ShouldBeNil)
		})
	})
}
