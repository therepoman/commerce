package api

import (
	"context"
	"testing"

	"code.justin.tv/commerce/mako/clients"
	"code.justin.tv/commerce/mako/clients/emoticonsmanager"
	mako "code.justin.tv/commerce/mako/internal"
	emoticonsmanager_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/emoticonsmanager"
	paint_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/paint"
	stats_mock "code.justin.tv/commerce/mako/mocks/github.com/cactus/go-statsd-client/statsd"
	mk "code.justin.tv/commerce/mako/twirp"
	"code.justin.tv/subs/paint/paintrpc"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
	"github.com/twitchtv/twirp"
)

func getEmoticonRequest() *mk.CreateEmoticonRequest {
	return &mk.CreateEmoticonRequest{
		UserId:     "123",
		Code:       "chuckFoo",
		CodeSuffix: "Foo",
		State:      "active",
		GroupId:    "567567",
		Image28Id:  "9d928ab5-1482-4be8-90cb-11357ab3c5c2",
		Image56Id:  "9d928ab5-1482-4be8-90cb-11357ab3c5c3",
		Image112Id: "9d928ab5-1482-4be8-90cb-11357ab3c5c4",
		Domain:     "emotes",
	}
}

func TestCreateEmoticonAPI(t *testing.T) {
	Convey("Test CreateEmoticonAPI", t, func() {
		mockEmoticon := new(emoticonsmanager_mock.IEmoticonsManager)
		mockPaint := new(paint_mock.PaintClient)
		mockStats := new(stats_mock.Statter)
		createEmoticonAPI := &CreateEmoticonAPI{
			Clients: clients.Clients{
				EmoticonsManager: mockEmoticon,
				PaintClient:      mockPaint,
			},
			Stats: mockStats,
		}

		mockStats.On("TimingDuration", mock.Anything, mock.Anything, mock.Anything).Return(nil)

		Convey("Test CreateEmoticon", func() {
			Convey("With empty userId", func() {
				req := &mk.CreateEmoticonRequest{
					UserId: "",
				}

				mockPaint.On("ModifyEmote", mock.Anything, mock.Anything).Return(mock.Anything, nil)

				_, err := createEmoticonAPI.CreateEmoticon(context.Background(), req)
				So(err, ShouldNotBeNil)
			})

			Convey("With empty emote state and emote approval check false", func() {
				req := getEmoticonRequest()
				req.State = ""
				req.EnforceModerationWorkflow = false

				_, err := createEmoticonAPI.CreateEmoticon(context.Background(), req)

				So(err, ShouldNotBeNil)
				So(err.Error(), ShouldContainSubstring, "state is required")
			})

			Convey("With error on emoticon create", func() {
				req := getEmoticonRequest()
				mockEmoticon.On("Create", mock.Anything, mock.Anything).Return(mako.Emote{}, errors.New("test"))

				_, err := createEmoticonAPI.CreateEmoticon(context.Background(), req)
				So(err, ShouldNotBeNil)
			})

			Convey("Invalid Image Upload error on emoticon create", func() {
				req := getEmoticonRequest()
				mockEmoticon.On("Create", mock.Anything, mock.Anything).Return(mako.Emote{},
					emoticonsmanager.InvalidImageUpload)

				_, err := createEmoticonAPI.CreateEmoticon(context.Background(), req)
				expectedErr := mk.NewClientError(
					twirp.InvalidArgumentError("image", "invalid image uploaded"),
					mk.ErrCodeInvalidImageUpload)

				So(err.Error(), ShouldEqual, expectedErr.Error())
			})

			Convey("Given emoticons manager returns successfully", func() {
				req := getEmoticonRequest()
				res := mako.Emote{
					ID:      "789",
					Code:    req.Code,
					GroupID: req.GroupId,
					State:   mako.Active,
				}
				mockEmoticon.On("Create", mock.Anything, mock.Anything).Return(res, nil)

				Convey("Given paint client returns successfully then return no error", func() {
					mockPaint.On("ModifyEmote", mock.Anything, mock.Anything).Return(&paintrpc.ModifyEmoteResponse{}, nil)

					result, err := createEmoticonAPI.CreateEmoticon(context.Background(), req)
					So(err, ShouldBeNil)
					So(result, ShouldNotBeNil)
				})

				Convey("Given a request to create a Bits Badge Tier Emote then don't call paint client to modify the emote", func() {
					req.EmoteGroup = mk.EmoteGroup_BitsBadgeTierEmotes

					result, err := createEmoticonAPI.CreateEmoticon(context.Background(), req)
					So(err, ShouldBeNil)
					So(result, ShouldNotBeNil)
					mockPaint.AssertNotCalled(t, "ModifyEmote", mock.Anything, mock.Anything)
				})
			})
		})
	})
}
