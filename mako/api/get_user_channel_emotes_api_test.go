package api

import (
	"context"
	"testing"

	"code.justin.tv/commerce/mako/clients"
	"code.justin.tv/commerce/mako/config"
	"code.justin.tv/commerce/mako/localdb"
	config_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/config"
	stats_mock "code.justin.tv/commerce/mako/mocks/github.com/cactus/go-statsd-client/statsd"
	mk "code.justin.tv/commerce/mako/twirp"
	. "github.com/smartystreets/goconvey/convey"
)

// See mako's root/tests/test_user_channel_emotes for tests of the underlying internal calls.
func TestGetUserChannelEmotesAPI(t *testing.T) {
	mockStats := new(stats_mock.Statter)
	mockDynamicConfig := new(config_mock.DynamicConfigClient)
	mockConfg := config.Configuration{}

	getUserChannelEmotesAPI := GetUserChannelEmotesAPI{
		Clients: clients.Clients{
			// These are unused for this test, they're just here to make sure we don't panic during runtime
			EntitlementDB:    localdb.NewEntitlementDB(),
			EmoteDB:          localdb.NewEmoteDB(),
			EmoteModifiersDB: localdb.NewEmoteModifierDB(),
		},
		Stats:         mockStats,
		DynamicConfig: mockDynamicConfig,
		Config:        &mockConfg,
	}

	Convey("Test GetUserChannelEmotes", t, func() {
		Convey("when no userID is provided", func() {
			req := mk.GetUserChannelEmotesRequest{}

			resp, err := getUserChannelEmotesAPI.GetUserChannelEmotes(context.Background(), &req)

			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("when no requestingUserID is provided", func() {
			req := mk.GetUserChannelEmotesRequest{
				UserId: "12345",
			}

			resp, err := getUserChannelEmotesAPI.GetUserChannelEmotes(context.Background(), &req)

			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("when no channelID is provided", func() {
			req := mk.GetUserChannelEmotesRequest{
				UserId:           "12345",
				RequestingUserId: "12345",
			}

			resp, err := getUserChannelEmotesAPI.GetUserChannelEmotes(context.Background(), &req)

			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("when requestingUserID does not equal userID", func() {
			req := mk.GetUserChannelEmotesRequest{
				UserId:           "12345",
				RequestingUserId: "23456",
				ChannelId:        "34567",
			}

			resp, err := getUserChannelEmotesAPI.GetUserChannelEmotes(context.Background(), &req)

			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})
	})
}
