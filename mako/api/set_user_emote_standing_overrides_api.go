package api

import (
	"context"
	"fmt"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/mako/clients"
	mako "code.justin.tv/commerce/mako/internal"
	mk "code.justin.tv/commerce/mako/twirp"
	"github.com/twitchtv/twirp"
)

const (
	maxAllowedNumberOfOverrides = 25
)

type SetUserEmoteStandingOverrideAPI struct {
	Clients clients.Clients
}

func (sueso *SetUserEmoteStandingOverrideAPI) SetUserEmoteStandingOverrides(ctx context.Context, r *mk.SetUserEmoteStandingOverridesRequest) (*mk.SetUserEmoteStandingOverridesResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, time.Second)
	defer cancel()

	err := validateSetUserEmoteStandingOverrideRequest(r)
	if err != nil {
		return nil, err
	}

	overrides, err := toMako(r.GetOverrides())
	if err != nil {
		return nil, twirp.InvalidArgumentError("overrides", "invalid emote standing override included in request")
	}

	unprocessedOverrides, err := sueso.Clients.EmoteStandingOverridesDB.UpdateOverrides(ctx, overrides...)
	if err != nil {
		msg := "Unable to update emote standing overrides"
		log.WithError(err).WithField("overrides", overrides).Error(msg)
		return nil, twirp.InternalError(msg).
			WithMeta("request", r.String()).
			WithMeta("error", err.Error())
	}

	if len(unprocessedOverrides) == 0 {
		return &mk.SetUserEmoteStandingOverridesResponse{}, nil
	}

	twirpOverrides, err := toTwirp(unprocessedOverrides)
	if err != nil {
		return nil, twirp.InternalError("unprocessedOverrides contained invalid emote standing override")
	}

	return &mk.SetUserEmoteStandingOverridesResponse{
		FailedOverrides: twirpOverrides,
	}, nil
}

func validateSetUserEmoteStandingOverrideRequest(r *mk.SetUserEmoteStandingOverridesRequest) error {
	overrides := r.GetOverrides()
	if len(overrides) == 0 {
		return mk.NewClientError(
			twirp.InvalidArgumentError("overrides", "must include at least one override"),
			mk.ErrCodeMissingRequiredArgument)
	}

	// Batch item update in dynamo only supports 25 by default, so until we separate the list into a bunch of batches
	// of 25 each, we'll just hardcode this max
	if len(overrides) > maxAllowedNumberOfOverrides {
		return mk.NewClientError(
			twirp.InvalidArgumentError("overrides", fmt.Sprintf("must be no more than %d overrides", maxAllowedNumberOfOverrides)),
			mk.ErrCodeMissingRequiredArgument)
	}

	for _, override := range overrides {
		if override.GetUserId() == "" {
			return mk.NewClientError(
				twirp.RequiredArgumentError("user_id"),
				mk.ErrCodeMissingRequiredArgument)
		}

		if override.GetOverride().String() == "" {
			return mk.NewClientError(
				twirp.RequiredArgumentError("override"),
				mk.ErrCodeMissingRequiredArgument)
		}

		if _, ok := mk.EmoteStandingOverride_value[override.Override.String()]; !ok {
			return mk.NewClientError(
				twirp.InvalidArgumentError("override", "must be a valid override enum"),
				mk.ErrCodeMissingRequiredArgument)
		}
	}

	return nil
}

func toTwirp(overrides []mako.UserEmoteStandingOverride) ([]*mk.UserEmoteStandingOverride, error) {
	var emoteStandingOverrideList = make([]*mk.UserEmoteStandingOverride, 0, len(overrides))
	for _, emoteStandingOverride := range overrides {
		twirpEmoteStandingOverride, err := emoteStandingOverride.ToTwirp()
		if err != nil {
			return nil, err
		}
		emoteStandingOverrideList = append(emoteStandingOverrideList, &twirpEmoteStandingOverride)
	}
	return emoteStandingOverrideList, nil
}

func toMako(overrides []*mk.UserEmoteStandingOverride) ([]mako.UserEmoteStandingOverride, error) {
	var emoteStandingOverrideList = make([]mako.UserEmoteStandingOverride, 0, len(overrides))
	for _, twirpEmoteStandingOverride := range overrides {
		if twirpEmoteStandingOverride == nil {
			continue
		}
		makoEmoteStandingOverride, err := mako.ToMako(*twirpEmoteStandingOverride)
		if err != nil {
			return nil, err
		}
		emoteStandingOverrideList = append(emoteStandingOverrideList, makoEmoteStandingOverride)
	}

	return emoteStandingOverrideList, nil
}
