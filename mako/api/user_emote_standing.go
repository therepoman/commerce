package api

import (
	"context"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/mako/clients"
	mk "code.justin.tv/commerce/mako/twirp"
	"github.com/twitchtv/twirp"
)

type UserEmoteStandingAPI struct {
	Clients clients.Clients
}

func (ues *UserEmoteStandingAPI) GetUserEmoteStanding(ctx context.Context, r *mk.GetUserEmoteStandingRequest) (*mk.GetUserEmoteStandingResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, 1500*time.Millisecond) //1.5 second to match GQL timeout
	defer cancel()

	userID := r.GetUserId()
	if userID == "" {
		return nil, twirp.RequiredArgumentError("user_id")
	}

	requestingUserID := r.GetRequestingUserId()
	if requestingUserID == "" {
		return nil, twirp.RequiredArgumentError("requesting_user_id")
	}

	// Allow a staff member to check another user's emote standing
	if userID != requestingUserID {
		requestingUser, err := ues.Clients.UserServiceClient.GetUserByID(ctx, requestingUserID, nil)
		if err != nil {
			msg := "error looking up user"
			log.WithError(err).WithField("userID", userID).WithField("requestingUserID", requestingUserID).Error(msg)
			return nil, twirp.InternalError(msg)
		}

		if requestingUser.Admin == nil || !*requestingUser.Admin {
			return nil, twirp.NewError(twirp.PermissionDenied, "requesting user not allowed to view user's emote standing")
		}
	}

	isInEmoteGoodStanding, _, err := ues.Clients.AutoApprovalEligibilityFetcher.IsUserEligibleForEmoteUploadAutoApproval(ctx, userID)
	if err != nil {
		msg := "error fetching emote auto-approval eligibility"
		log.WithError(err).WithField("userID", userID).WithField("requestingUserID", requestingUserID).Error(msg)
		return nil, twirp.InternalError(msg)
	}

	return &mk.GetUserEmoteStandingResponse{
		IsInEmoteGoodStanding: isInEmoteGoodStanding,
	}, nil
}
