package api

import (
	"context"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/mako/clients"
	mk "code.justin.tv/commerce/mako/twirp"
	"github.com/twitchtv/twirp"
)

type DeactivateEmoticonAPI struct {
	Clients clients.Clients
}

func (de *DeactivateEmoticonAPI) DeactivateEmoticon(ctx context.Context, r *mk.DeactivateEmoticonRequest) (*mk.DeactivateEmoticonResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, time.Second*5)
	defer cancel()

	log.Infof("Deactivate Emoticon: %s", r.String())

	if r.GetId() == "" {
		return nil, twirp.RequiredArgumentError("id must not be nil")
	}

	err := de.Clients.EmoticonsManager.Deactivate(ctx, r.GetId())
	if err != nil {
		log.WithFields(log.Fields{
			"request": r.String(),
		}).WithError(err).Error("Error deactivating emoticon")
		return nil, twirp.InternalError("Error deactivating emoticon").
			WithMeta("request", r.String()).
			WithMeta("error", err.Error())
	}

	return &mk.DeactivateEmoticonResponse{}, nil
}
