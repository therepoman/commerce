package api

import (
	"context"
	"time"

	materiatwirp "code.justin.tv/amzn/MateriaTwirp"
	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/mako/clients"
	mk "code.justin.tv/commerce/mako/twirp"
	"github.com/twitchtv/twirp"
)

type DeleteEmoteGroupEntitlementsAPI struct {
	Clients clients.Clients
}

func (ce *DeleteEmoteGroupEntitlementsAPI) DeleteEmoteGroupEntitlements(ctx context.Context, r *mk.DeleteEmoteGroupEntitlementsRequest) (*mk.DeleteEmoteGroupEntitlementsResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, 3*time.Second)
	defer cancel()

	keys := make([]*materiatwirp.EntitlementKey, len(r.Entitlements))

	for i, entitlement := range r.Entitlements {
		if err := validateGroupEntitlement(entitlement); err != nil {
			return nil, err
		}

		var source materiatwirp.Source
		switch entitlement.Group {
		case mk.EmoteGroup_BitsBadgeTierEmotes:
			source = materiatwirp.Source_BITS
		case mk.EmoteGroup_Subscriptions:
			source = materiatwirp.Source_SUBS
		case mk.EmoteGroup_HypeTrain:
			source = materiatwirp.Source_HYPE_TRAIN
		case mk.EmoteGroup_MegaCommerce:
			source = materiatwirp.Source_MEGA_COMMERCE
		case mk.EmoteGroup_Rewards:
			source = materiatwirp.Source_REWARDS
		default:
			logrus.Errorf("the group '%s' is not supported by the CreateEmoteGroupEntitlements API", entitlement.Group.String())
			return nil, twirp.InvalidArgumentError("group", "The group specified is not supported for this API")
		}

		keys[i] = &materiatwirp.EntitlementKey{
			OwnerId:  entitlement.OwnerId,
			Domain:   materiatwirp.Domain_EMOTES,
			Source:   source,
			ItemId:   entitlement.GroupId,
			OriginId: entitlement.OriginId,
		}
	}

	if _, err := ce.Clients.MateriaClient.DeleteEntitlements(ctx, &materiatwirp.DeleteEntitlementsRequest{
		EntitlementKeys: keys,
	}); err != nil {
		logrus.WithError(err).Error("failed to delete materia entitlements")
		return nil, twirp.InternalErrorWith(err)
	}

	return &mk.DeleteEmoteGroupEntitlementsResponse{}, nil
}
