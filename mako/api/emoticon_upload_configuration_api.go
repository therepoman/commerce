package api

import (
	"context"

	mk "code.justin.tv/commerce/mako/twirp"
	"github.com/twitchtv/twirp"
)

// These API's are deprecated.
type GetEmoticonUploadConfigurationAPI struct{}

// Deprecated: please migrate to GetEmoteUploadConfig in Dashboard API UploadEmoticon requests an upload ID and upload s3 URL for an emoticon image
func (ue *GetEmoticonUploadConfigurationAPI) GetEmoticonUploadConfiguration(ctx context.Context, r *mk.GetEmoticonUploadConfigurationRequest) (*mk.GetEmoticonUploadConfigurationResponse, error) {
	return nil, twirp.NewError(twirp.Unimplemented, "Method no longer supported or implemented")
}
