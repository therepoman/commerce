package api

import (
	"context"
	"testing"
	"time"

	materiatwirp "code.justin.tv/amzn/MateriaTwirp"
	"code.justin.tv/commerce/mako/clients"
	"code.justin.tv/commerce/mako/config"
	materia_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/materia"
	mk "code.justin.tv/commerce/mako/twirp"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
	"google.golang.org/protobuf/types/known/timestamppb"
)

func TestEmoteEntitlements(t *testing.T) {
	Convey("Given EmoteEntitlementsAPI", t, func() {
		mockMateria := new(materia_mock.MateriaClient)
		emoteEntitlements := EmoteEntitlementsAPI{
			Clients: clients.Clients{
				MateriaClient: mockMateria,
			},
			Config: &config.Configuration{
				CommunityPointsEmoteSetID: "300000028",
			},
		}

		ctx := context.Background()
		startTime := timestamppb.New(time.Now().Add(-1 * time.Minute))
		endTime := timestamppb.New(time.Now().Add(5 * time.Minute))

		hypeTrainEntitlement := materiatwirp.Entitlement{
			Domain:      materiatwirp.Domain_EMOTES,
			Source:      materiatwirp.Source_HYPE_TRAIN,
			ItemId:      "1",
			ItemOwnerId: "123456789",
			OwnerId:     "5678",
			OriginId:    "test_mako1",
			StartsAt:    startTime,
			EndsAt: &materiatwirp.InfiniteTime{
				Time:       endTime,
				IsInfinite: false,
			},
		}

		hypeTrainEntitlement2 := materiatwirp.Entitlement{
			Domain:      materiatwirp.Domain_EMOTES,
			Source:      materiatwirp.Source_HYPE_TRAIN,
			ItemId:      "2",
			ItemOwnerId: "123456789",
			OwnerId:     "5678",
			OriginId:    "test_mako2",
			StartsAt:    startTime,
			EndsAt: &materiatwirp.InfiniteTime{
				Time:       endTime,
				IsInfinite: false,
			},
		}

		subEntitlement := materiatwirp.Entitlement{
			Domain:      materiatwirp.Domain_EMOTES,
			Source:      materiatwirp.Source_SUBS,
			ItemId:      "3",
			ItemOwnerId: "123456789",
			OwnerId:     "5678",
			OriginId:    "test_mako3",
			StartsAt:    startTime,
			EndsAt: &materiatwirp.InfiniteTime{
				Time:       endTime,
				IsInfinite: false,
			},
		}

		subExpiredEntitlement := materiatwirp.Entitlement{
			Domain:      materiatwirp.Domain_EMOTES,
			Source:      materiatwirp.Source_SUBS,
			ItemId:      "3",
			ItemOwnerId: "123456789",
			OwnerId:     "5678",
			OriginId:    "test_mako3",
			StartsAt:    startTime,
			EndsAt: &materiatwirp.InfiniteTime{
				Time:       startTime,
				IsInfinite: false,
			},
		}

		Convey("Test GetEmoteEntitlements", func() {
			Convey("With empty OwnerID", func() {
				req := &mk.GetEmoteEntitlementsRequest{
					OwnerId: "",
				}

				resp, err := emoteEntitlements.GetEmoteEntitlements(ctx, req)
				So(resp, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})

			Convey("with valid ownerID", func() {
				materiaEntitlements := &materiatwirp.GetEntitlementsResponse{
					Entitlements: []*materiatwirp.Entitlement{&hypeTrainEntitlement, &hypeTrainEntitlement2, &subEntitlement, &subExpiredEntitlement},
				}

				req := &mk.GetEmoteEntitlementsRequest{
					OwnerId: "5678",
				}

				mockMateria.On("GetEntitlements", mock.Anything, mock.Anything).Return(materiaEntitlements, nil)
				resp, err := emoteEntitlements.GetEmoteEntitlements(ctx, req)
				So(resp, ShouldNotBeNil)
				So(len(resp.Entitlements), ShouldEqual, 3)
				So(err, ShouldBeNil)
			})

			Convey("with valid ownerID and itemID", func() {
				materiaEntitlements := &materiatwirp.GetEntitlementsResponse{
					Entitlements: []*materiatwirp.Entitlement{&hypeTrainEntitlement},
				}

				req := &mk.GetEmoteEntitlementsRequest{
					OwnerId:  "5678",
					OriginId: "test_mako3",
				}

				mockMateria.On("GetEntitlements", mock.Anything, mock.Anything).Return(materiaEntitlements, nil)
				resp, err := emoteEntitlements.GetEmoteEntitlements(ctx, req)
				So(resp, ShouldNotBeNil)
				So(len(resp.Entitlements), ShouldEqual, 1)
				So(resp.Entitlements[0].Group, ShouldEqual, mk.EmoteGroup_HypeTrain)
				So(err, ShouldBeNil)
			})

			Convey("with valid ownerID and originID", func() {
				materiaEntitlements := &materiatwirp.GetEntitlementsResponse{
					Entitlements: []*materiatwirp.Entitlement{&hypeTrainEntitlement},
				}

				req := &mk.GetEmoteEntitlementsRequest{
					OwnerId: "5678",
					ItemId:  "1",
				}

				mockMateria.On("GetEntitlements", mock.Anything, mock.Anything).Return(materiaEntitlements, nil)
				resp, err := emoteEntitlements.GetEmoteEntitlements(ctx, req)
				So(resp, ShouldNotBeNil)
				So(len(resp.Entitlements), ShouldEqual, 1)
				So(resp.Entitlements[0].OriginId, ShouldEqual, hypeTrainEntitlement.OriginId)

				So(err, ShouldBeNil)
			})

			Convey("with valid ownerID and group", func() {
				materiaEntitlements := &materiatwirp.GetEntitlementsResponse{
					Entitlements: []*materiatwirp.Entitlement{&hypeTrainEntitlement, &hypeTrainEntitlement2},
				}

				req := &mk.GetEmoteEntitlementsRequest{
					OwnerId: "5678",
					Group:   mk.EmoteGroup_HypeTrain,
				}

				mockMateria.On("GetEntitlements", mock.Anything, mock.Anything).Return(materiaEntitlements, nil)
				resp, err := emoteEntitlements.GetEmoteEntitlements(ctx, req)
				So(resp, ShouldNotBeNil)
				So(len(resp.Entitlements), ShouldEqual, 2)
				So(resp.Entitlements[0].Group, ShouldEqual, mk.EmoteGroup_HypeTrain)

				So(err, ShouldBeNil)
			})
		})
	})
}
