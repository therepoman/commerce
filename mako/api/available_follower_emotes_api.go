package api

import (
	"context"
	"time"

	"code.justin.tv/commerce/mako/config"

	mako "code.justin.tv/commerce/mako/internal"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/mako/clients"
	mk "code.justin.tv/commerce/mako/twirp"
	"github.com/twitchtv/twirp"
)

type GetAvailableFollowerEmotesAPI struct {
	Clients       clients.Clients
	DynamicConfig config.DynamicConfigClient
}

func (afe *GetAvailableFollowerEmotesAPI) GetAvailableFollowerEmotes(ctx context.Context, r *mk.GetAvailableFollowerEmotesRequest) (*mk.GetAvailableFollowerEmotesResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, time.Second)
	defer cancel()

	// Make sure user is in Follower Emotes Experiment before returning available Follower Emotes
	if !afe.DynamicConfig.IsInFreemotesExperiment(r.GetChannelId()) {
		return &mk.GetAvailableFollowerEmotesResponse{}, nil
	}

	group, err := afe.Clients.EmoteGroupDB.GetFollowerEmoteGroup(ctx, r.ChannelId)
	if err != nil {
		msg := "Couldn't get follower emote group for channel"
		log.WithError(err).WithField("channel_id", r.ChannelId).Error(msg)
		return nil, twirp.InternalError(msg)
	}

	if group == nil {
		return &mk.GetAvailableFollowerEmotesResponse{}, nil
	}

	emotes, err := afe.Clients.EmoteDB.ReadByGroupIDs(ctx, group.GroupID)
	if err != nil {
		msg := "Couldn't read follower emotes for channel"
		log.WithError(err).WithField("group_id", group.GroupID).Error(msg)
		return nil, twirp.InternalError(msg)
	}

	emoticons := make([]*mk.Emoticon, 0, len(emotes))
	for _, emote := range emotes {
		twirpEmote := emote.ToTwirp()
		emoticons = append(emoticons, &twirpEmote)
	}

	mako.SortTwirpEmoticonsByOrderAscendingAndCreatedAtDescending(emoticons)
	return &mk.GetAvailableFollowerEmotesResponse{
		Emoticons: emoticons,
	}, nil
}
