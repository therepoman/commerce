package api

import (
	"context"
	"time"

	"code.justin.tv/commerce/mako/clients/paint"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/mako/clients"
	"code.justin.tv/commerce/mako/clients/emoticonsmanager"
	mako "code.justin.tv/commerce/mako/internal"
	mk "code.justin.tv/commerce/mako/twirp"
	mkd "code.justin.tv/commerce/mako/twirp/dashboard"
	"code.justin.tv/subs/paint/paintrpc"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/twitchtv/twirp"
)

type CreateEmoticonAPI struct {
	Clients clients.Clients
	Stats   statsd.Statter
}

func (ce *CreateEmoticonAPI) CreateEmoticon(ctx context.Context, r *mk.CreateEmoticonRequest) (*mk.CreateEmoticonReponse, error) {
	ctx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()

	log.Infof("Create Emoticon: %s", r.String())
	err := validateCreateEmoticonRequest(r)
	if err != nil {
		return nil, err
	}

	newEmote, err := ce.Clients.EmoticonsManager.Create(ctx, convertCreateEmoticonRequestToClient(r))
	if err != nil {
		validateErr := validateEmoticonCreateResponse(err)
		if validateErr != nil {
			return nil, validateErr
		}

		log.WithFields(log.Fields{
			"request": r.String(),
		}).WithError(err).Error("Error creating emoticon")
		return nil, twirp.InternalError("Error creating emoticon").
			WithMeta("request", r.String()).
			WithMeta("error", err.Error())
	}

	// We do not want to modify Bits Badge Tier Emotes
	if r.EmoteGroup != mk.EmoteGroup_BitsBadgeTierEmotes {
		go func() {
			start := time.Now()
			if _, err := ce.Clients.PaintClient.ModifyEmote(context.Background(), &paintrpc.ModifyEmoteRequest{
				EmoteId:       newEmote.ID,
				Modifications: paint.DefaultModifications,
			}); err != nil {
				// We only want to log the failure. The CreateEmoticon call should still return successfully.
				log.WithFields(log.Fields{
					"emote_id": newEmote.ID,
				}).WithError(err).Error("failed to modify emote")
			}
			ce.Stats.TimingDuration("create_emoticon.modify", time.Since(start), 1.0)
		}()
	}

	return convertMakoEmoteToCreateEmoticonResponse(newEmote), nil
}

func convertCreateEmoticonRequestToClient(req *mk.CreateEmoticonRequest) *emoticonsmanager.CreateParams {
	return &emoticonsmanager.CreateParams{
		UserID:      req.GetUserId(),
		CodeSuffix:  req.GetCodeSuffix(),
		State:       convertLegacyTwirpStateToClientState(req.GetState()),
		GroupID:     req.GetGroupId(),
		Image1xID:   req.GetImage28Id(),
		Image2xID:   req.GetImage56Id(),
		Image4xID:   req.GetImage112Id(),
		Domain:      mako.ParseDomain(req.GetDomain()),
		EmoteOrigin: convertLegacyEmoteGroupToEmoteOrigin(req.GetEmoteGroup()),
		// If a code is provided, and the suffix is the same as the code, that means that someone is trying to upload
		// a global emote via admin panel. In all other cases, suffix and code don't match, so we should prepend this
		// channel's prefix.
		ShouldIgnorePrefix:        req.GetCode() == req.GetCodeSuffix(),
		IsAnimated:                false,
		EnforceModerationWorkflow: req.GetEnforceModerationWorkflow(),
	}
}

func convertLegacyEmoteGroupToEmoteOrigin(group mk.EmoteGroup) mkd.EmoteOrigin {
	switch group {
	case mk.EmoteGroup_ChannelPoints:
		return mkd.EmoteOrigin_ChannelPoints
	case mk.EmoteGroup_BitsBadgeTierEmotes:
		return mkd.EmoteOrigin_BitsBadgeTierEmotes
	case mk.EmoteGroup_Subscriptions:
		return mkd.EmoteOrigin_Subscriptions
	case mk.EmoteGroup_Rewards:
		return mkd.EmoteOrigin_Rewards
	case mk.EmoteGroup_HypeTrain:
		return mkd.EmoteOrigin_HypeTrain
	case mk.EmoteGroup_MegaCommerce:
		return mkd.EmoteOrigin_MegaCommerce
	case mk.EmoteGroup_Prime:
		return mkd.EmoteOrigin_Prime
	case mk.EmoteGroup_Turbo:
		return mkd.EmoteOrigin_Turbo
	case mk.EmoteGroup_Smilies:
		return mkd.EmoteOrigin_Smilies
	case mk.EmoteGroup_Globals:
		return mkd.EmoteOrigin_Globals
	case mk.EmoteGroup_OWL2019:
		return mkd.EmoteOrigin_OWL2019
	case mk.EmoteGroup_TwoFactor:
		return mkd.EmoteOrigin_TwoFactor
	case mk.EmoteGroup_LimitedTime:
		return mkd.EmoteOrigin_LimitedTime
	default:
		return mkd.EmoteOrigin_None
	}
}

func convertLegacyTwirpStateToClientState(s string) mako.EmoteState {
	switch s {
	case "active":
		return mako.Active
	case "archived":
		return mako.Archived
	case "inactive":
		return mako.Inactive
	case "moderated":
		return mako.Moderated
	case "pending":
		return mako.Pending
	case "pending_archived":
		return mako.PendingArchived
	case "unknown":
		fallthrough
	default:
		return mako.Unknown
	}
}

func convertMakoEmoteToCreateEmoticonResponse(emote mako.Emote) *mk.CreateEmoticonReponse {
	return &mk.CreateEmoticonReponse{
		Id:      emote.ID,
		Code:    emote.Code,
		GroupId: emote.GroupID,
		State:   string(emote.State),
	}
}

func validateCreateEmoticonRequest(req *mk.CreateEmoticonRequest) error {
	if req.GetUserId() == "" {
		return mk.NewClientError(
			twirp.RequiredArgumentError("user_id"),
			mk.ErrCodeMissingRequiredArgument)
	}

	if req.GetCode() == "" {
		return mk.NewClientError(
			twirp.RequiredArgumentError("code"),
			mk.ErrCodeMissingRequiredArgument)
	}

	if req.GetCodeSuffix() == "" {
		return mk.NewClientError(
			twirp.RequiredArgumentError("code_suffix"),
			mk.ErrCodeMissingRequiredArgument)
	}

	if req.GetState() == "" && !req.GetEnforceModerationWorkflow() {
		return mk.NewClientError(
			twirp.RequiredArgumentError("state"),
			mk.ErrCodeMissingRequiredArgument)
	}

	if req.GetGroupId() == "" {
		return mk.NewClientError(
			twirp.RequiredArgumentError("group_id"),
			mk.ErrCodeMissingRequiredArgument)
	}

	if req.GetImage28Id() == "" {
		return mk.NewClientError(
			twirp.RequiredArgumentError("image28_id"),
			mk.ErrCodeMissingRequiredArgument)
	}

	if req.GetImage56Id() == "" {
		return mk.NewClientError(
			twirp.RequiredArgumentError("image56_id"),
			mk.ErrCodeMissingRequiredArgument)
	}

	if req.GetImage112Id() == "" {
		return mk.NewClientError(
			twirp.RequiredArgumentError("image112_id"),
			mk.ErrCodeMissingRequiredArgument)
	}

	return nil
}

func validateEmoticonCreateResponse(createError error) error {
	if createError == emoticonsmanager.EmoticonCodeNotUnique {
		return mk.NewClientError(
			twirp.InvalidArgumentError("code", "must be unique"),
			mk.ErrCodeCodeNotUnique)
	}

	if createError == emoticonsmanager.EmoticonCodeUnacceptable {
		return mk.NewClientError(
			twirp.InvalidArgumentError("code", "is unacceptable"),
			mk.ErrCodeCodeUnacceptable)
	}

	if createError == emoticonsmanager.InvalidCodeSuffixFormat {
		return mk.NewClientError(
			twirp.InvalidArgumentError("code_suffix", "invalid format"),
			mk.ErrCodeInvalidSuffixFormat)
	}

	if createError == emoticonsmanager.InvalidImageUpload {
		return mk.NewClientError(
			twirp.InvalidArgumentError("image", "invalid image uploaded"),
			mk.ErrCodeInvalidImageUpload)
	}

	if createError == emoticonsmanager.Image1XIDNotFound {
		return mk.NewClientError(
			twirp.InvalidArgumentError("image1x_id", "not found"),
			mk.ErrCodeImage1XIDNotFound)
	}

	if createError == emoticonsmanager.Image2XIDNotFound {
		return mk.NewClientError(
			twirp.InvalidArgumentError("image2x_id", "not found"),
			mk.ErrCodeImage2XIDNotFound)
	}

	if createError == emoticonsmanager.Image4XIDNotFound {
		return mk.NewClientError(
			twirp.InvalidArgumentError("image4x_id", "not found"),
			mk.ErrCodeImage4XIDNotFound)
	}

	return nil
}
