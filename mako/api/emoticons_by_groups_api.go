package api

import (
	"context"
	"fmt"
	"time"

	"code.justin.tv/commerce/gogogadget/strings"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/mako/clients"
	mako "code.justin.tv/commerce/mako/internal"
	mk "code.justin.tv/commerce/mako/twirp"
	"github.com/twitchtv/twirp"
	"github.com/twitchtv/twirp/hooks/statsd"
)

const (
	statsFormat         = "GetEmoticonsByGroups.%s"
	clearedGlobalsGroup = "CLEARED_GLOBALS"
)

// EmoticonsByGroupsAPI includes the API for getting emotes by group id
type EmoticonsByGroupsAPI struct {
	Clients clients.Clients
	Stats   statsd.Statter
}

// GetEmoticonsByGroups returns all emotes for the given emote groups
func (emoticonByGroupsAPI *EmoticonsByGroupsAPI) GetEmoticonsByGroups(ctx context.Context, r *mk.GetEmoticonsByGroupsRequest) (*mk.GetEmoticonsByGroupsResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, 400*time.Millisecond)
	defer cancel()

	emoteGroupIDs := strings.Dedupe(r.GetEmoticonGroupKeys())
	if len(emoteGroupIDs) != len(r.GetEmoticonGroupKeys()) {
		log.WithFields(log.Fields{
			"group_ids": r.GetEmoticonGroupKeys(),
		}).Warn("given set of group ids in GetEmoticonsByGroups contains duplicates")
	}

	emoteGroupIDs = strings.RemoveEmptyStrings(emoteGroupIDs)

	// Cache-Control header, used by Visage's cache mechanism
	err := twirp.SetHTTPResponseHeader(ctx, "Cache-Control", "public, max-age=300")
	if err != nil {
		msg := "Error setting HTTP response header"
		log.WithError(err).Error(msg)
		return nil, twirp.InternalError(msg)
	}

	// Detect the magic Cleared Globals ID used by Helix
	// Helix always requests the cleared globals on their own and never as part of a larger request
	// No other callers of Mako query for cleared globals.
	if len(emoteGroupIDs) == 1 && emoteGroupIDs[0] == clearedGlobalsGroup {
		return &mk.GetEmoticonsByGroupsResponse{
			Groups: emoticonByGroupsAPI.getClearedGlobals(),
		}, nil
	}

	start := time.Now()
	emotes, err := emoticonByGroupsAPI.Clients.EmoteDB.ReadByGroupIDs(ctx, emoteGroupIDs...)
	emoticonByGroupsAPI.Stats.TimingDuration(fmt.Sprintf(statsFormat, "EmoteDBDialUpReader.ReadByGroupIDs"), time.Since(start), 1.0)
	if err != nil {
		msg := "Error reading emotes by groups in GetEmoticonsByGroups"
		log.WithError(err).WithField("group_ids", emoteGroupIDs).Error(msg)
		return nil, twirp.InternalError(msg)
	}

	emoticonsByGroup := make(map[string][]*mk.Emoticon)
	filteredEmotes := mako.ApplyEmoteFilter(mako.MakeEmoteFilterFromTwirp(r.GetFilter()), emotes)
	for _, emote := range filteredEmotes {
		twirpEmote := emote.ToTwirp()
		emoticonsByGroup[emote.GroupID] = append(emoticonsByGroup[emote.GroupID], &twirpEmote)
	}

	var emoticonGroups []*mk.EmoticonGroup
	for groupID, emotes := range emoticonsByGroup {
		mako.SortTwirpEmoticonsByOrderAscendingAndCreatedAtDescending(emotes)

		emoticonGroups = append(emoticonGroups, &mk.EmoticonGroup{
			Id:        groupID,
			Emoticons: emotes,
			Order:     mako.GetEmoteGroupOrder(groupID),
		})
	}

	return &mk.GetEmoticonsByGroupsResponse{
		Groups: emoticonGroups,
	}, nil
}

// This API is a special case of the GetEmoticonsByGroups request that is only used by Helix for 3rd party APIs.
// IMHO this really should be its own API but for the short term we have to maintain its existing functionality as a weird "special group".
// This API returns a subset of the global emotes, only those for which Twitch has full legal rights.
func (emoticonByGroupsAPI *EmoticonsByGroupsAPI) getClearedGlobals() []*mk.EmoticonGroup {
	clearedGlobals := emoticonByGroupsAPI.Clients.ClearedGlobalsCache.GetClearedGlobals()

	return []*mk.EmoticonGroup{
		{
			Id:        clearedGlobalsGroup,
			Emoticons: clearedGlobals[clearedGlobalsGroup],
		},
	}
}
