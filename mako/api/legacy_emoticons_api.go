package api

import (
	"context"
	"fmt"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/mako/clients"
	"code.justin.tv/commerce/mako/config"
	mako "code.justin.tv/commerce/mako/internal"
	mk "code.justin.tv/commerce/mako/twirp"
	"github.com/twitchtv/twirp"
	"github.com/twitchtv/twirp/hooks/statsd"
)

const (
	channelEmoticonLink = "https://api.twitch.tv/kraken/chat/%s/emoticons"
	EmoticonUrl         = "https://static-cdn.jtvnw.net/emoticons/v1/%s/1.0"

	globalsEmoteGroupId = "0"

	legacyEmoticonS3URL          = "https://s3-us-west-2.amazonaws.com"
	legacyEmoticonsAllS3Folder   = "emoticonsall"
	legacyEmoticonsS3Key         = "emoticons"
	legacyEmoticonImagesS3Key    = "emoticon_images"
	legacyEmoticonImagesAllS3Key = "emoticon_images_all"
)

type LegacyEmoticonsAPI struct {
	Clients clients.Clients
	Stats   statsd.Statter
	Config  *config.Configuration
}

func (legacyEmoticonsAPI *LegacyEmoticonsAPI) GetLegacyEmoticonAllURLs(ctx context.Context, r *mk.GetLegacyEmoticonAllURLsRequest) (*mk.GetLegacyEmoticonAllURLsResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, time.Second)
	defer cancel()
	emoticonS3URLBase := fmt.Sprintf("%s/%s/%s", legacyEmoticonS3URL, legacyEmoticonsAPI.Config.UploadS3BucketName, legacyEmoticonsAllS3Folder)

	LegacyEmoticonsURL := fmt.Sprintf("%s/%s", emoticonS3URLBase, legacyEmoticonsS3Key)
	LegacyEmoticonImagesURL := fmt.Sprintf("%s/%s", emoticonS3URLBase, legacyEmoticonImagesS3Key)
	LegacyEmoticonImagesAllURL := fmt.Sprintf("%s/%s", emoticonS3URLBase, legacyEmoticonImagesAllS3Key)

	return &mk.GetLegacyEmoticonAllURLsResponse{
		EmoticonsUrl:         LegacyEmoticonsURL,
		EmoticonImagesUrl:    LegacyEmoticonImagesURL,
		EmoticonImagesAllUrl: LegacyEmoticonImagesAllURL,
	}, nil
}

func (legacyEmoticonsAPI *LegacyEmoticonsAPI) GetLegacyChannelEmoticons(ctx context.Context, r *mk.GetLegacyChannelEmoticonsRequest) (*mk.GetLegacyChannelEmoticonsResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, time.Second)
	defer cancel()

	err := twirp.SetHTTPResponseHeader(ctx, "Cache-Control", "public, max-age=600")
	if err != nil {
		log.WithError(err).Error("Error setting HTTP response header")
		return nil, twirp.InternalErrorWith(err)
	}

	if r.GetChannelId() == "" {
		return nil, twirp.InvalidArgumentError("channel_id", "must not be empty")
	}

	if r.GetChannelName() == "" {
		return nil, twirp.InvalidArgumentError("channel_name", "must not be empty")
	}

	emotes, err := legacyEmoticonsAPI.getEmotes(ctx, r.GetChannelId())
	if err != nil {
		log.WithError(err).Error("Error getting legacy channel emoticons")
		return nil, twirp.InternalError("Error getting legacy channel emoticons").
			WithMeta("request", r.String())
	}

	twirpEmotes := toTwirpEmotes(emotes)

	return &mk.GetLegacyChannelEmoticonsResponse{
		XLinks: &mk.Links{
			Self: fmt.Sprintf(channelEmoticonLink, r.GetChannelName()),
		},
		Emoticons: twirpEmotes,
	}, nil
}

func (legacyEmoticonsAPI *LegacyEmoticonsAPI) getEmotes(ctx context.Context, channelID string) ([]mako.Emote, error) {
	products, err := legacyEmoticonsAPI.Clients.SubscriptionsClient.GetChannelProducts(ctx, channelID)
	if err != nil {
		return nil, err
	}

	var groupIds []string

	if products != nil {
		for _, product := range products.Products {
			if product != nil {
				groupIds = append(groupIds, product.EmoticonSetId)
			}
		}
	}

	// also fetch globals
	groupIds = append(groupIds, globalsEmoteGroupId)

	emotes, err := legacyEmoticonsAPI.Clients.EmoteDB.ReadByGroupIDs(ctx, groupIds...)
	if err != nil {
		return nil, err
	}

	var filteredEmotes []mako.Emote
	for _, emote := range emotes {
		if emote.State != mako.Active {
			continue
		}

		filteredEmotes = append(filteredEmotes, emote)
	}

	return filteredEmotes, nil
}

func toTwirpEmotes(emotes []mako.Emote) []*mk.LegacyChannelEmoticonsEmoticon {
	var twirpEmotes []*mk.LegacyChannelEmoticonsEmoticon
	for _, emote := range emotes {
		subOnly := emote.GroupID != globalsEmoteGroupId

		twirpEmotes = append(twirpEmotes, &mk.LegacyChannelEmoticonsEmoticon{
			Regex:          emote.Code,
			State:          "active",
			SubscriberOnly: subOnly,
			Url:            fmt.Sprintf(EmoticonUrl, emote.ID),
			Height:         28,
			Width:          28,
		})
	}

	return twirpEmotes
}
