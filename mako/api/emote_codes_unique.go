package api

import (
	"context"
	"time"

	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/mako/clients"
	mako "code.justin.tv/commerce/mako/internal"
	mk "code.justin.tv/commerce/mako/twirp"
	"github.com/twitchtv/twirp"
)

// EmoteCodeUniqueAPI includes the APIs to get establish given emote code uniqueness
type EmoteCodeUniqueAPI struct {
	Clients clients.Clients
}

func (ecu *EmoteCodeUniqueAPI) BatchValidateCodeUniqueness(ctx context.Context, r *mk.BatchValidateCodeUniquenessRequest) (*mk.BatchValidateCodeUniquenessResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, time.Second*5)
	defer cancel()

	err := validateBatchValidateCodeUniquenessRequest(r)
	if err != nil {
		return nil, err
	}

	result := map[string]bool{}
	var states []mako.EmoteState
	for _, state := range r.GetFilter().GetStates() {
		states = append(states, mako.EmoteState(state.String()))
	}

	domain := r.GetFilter().GetDomain().String()

	emotes, err := ecu.Clients.EmoteDB.ReadByCodes(ctx, r.GetEmoteCodes()...)
	if err != nil {
		return nil, twirp.InternalError(err.Error())
	}

	for _, emote := range emotes {
		if emote.Domain != mako.Domain(domain) {
			continue
		}

		var found bool
		for _, state := range states {
			if state == mako.EmoteState(emote.State) {
				found = true
				break
			}
		}

		if !found {
			continue
		}

		result[emote.Code] = false
	}

	// Fill in remainder mappings i.e. if the code hasn't been found yet, then it is unique
	for _, code := range r.GetEmoteCodes() {
		if _, ok := result[code]; !ok {
			result[code] = true
		}
	}

	return &mk.BatchValidateCodeUniquenessResponse{
		IsUnique: result,
	}, nil
}

func validateBatchValidateCodeUniquenessRequest(req *mk.BatchValidateCodeUniquenessRequest) error {
	if req.GetFilter() == nil {
		return twirp.InvalidArgumentError("filter", "is empty")
	}

	_, exists := mk.Domain_name[int32(req.GetFilter().GetDomain())]
	if !exists {
		return twirp.InvalidArgumentError("domain", "is invalid")
	}

	for _, mkState := range req.GetFilter().GetStates() {
		_, exists := mk.EmoteState_name[int32(mkState)]
		if !exists {
			return twirp.InvalidArgumentError("state", "is invalid")
		}
	}

	for _, code := range req.GetEmoteCodes() {
		if strings.Blank(code) {
			return twirp.InvalidArgumentError("emote_code", "can't be blank")
		}
	}

	return nil
}
