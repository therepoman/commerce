package api

import (
	"context"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/mako/clients"
	mk "code.justin.tv/commerce/mako/twirp"
	"github.com/twitchtv/twirp"
)

type ActivateEmoticonAPI struct {
	Clients clients.Clients
}

func (ae *ActivateEmoticonAPI) ActivateEmoticon(ctx context.Context, r *mk.ActivateEmoticonRequest) (*mk.ActivateEmoticonResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, time.Second*5)
	defer cancel()

	log.Infof("Activate Emoticon: %s", r.String())

	if r.GetId() == "" {
		return nil, twirp.RequiredArgumentError("id must not be nil")
	}

	err := ae.Clients.EmoticonsManager.Activate(ctx, r.GetId())
	if err != nil {
		log.WithFields(log.Fields{
			"request": r.String(),
		}).WithError(err).Error("Error activating emoticon")
		return nil, twirp.InternalError("Error activating emoticon").
			WithMeta("request", r.String()).
			WithMeta("error", err.Error())
	}

	return &mk.ActivateEmoticonResponse{}, nil
}
