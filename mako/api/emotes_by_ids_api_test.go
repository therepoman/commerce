package api

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/mako/clients"
	mako "code.justin.tv/commerce/mako/internal"
	"code.justin.tv/commerce/mako/localdb"
	internal_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/internal_mock"
	mkd "code.justin.tv/commerce/mako/twirp/dashboard"
	"github.com/golang/protobuf/proto"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestGetEmotesByIDsAPI(t *testing.T) {
	Convey("Test GetEmotesByIDs API", t, func() {
		Convey("With empty input array", func() {
			mockEmoteDB := new(internal_mock.EmoteDB)

			emotesByIDsAPI := EmotesByIDsAPI{
				Clients: clients.Clients{
					EmoteDBUncached: mockEmoteDB,
				},
			}

			request := &mkd.GetEmotesByIDsRequest{EmoteIds: []string{}}
			resp, err := emotesByIDsAPI.GetEmotesByIDs(context.Background(), request)

			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
			So(err.Error(), ShouldContainSubstring, "emote_ids is required")
			So(len(mockEmoteDB.Calls), ShouldEqual, 0)
		})

		Convey("With invalid input IDs", func() {
			mockEmoteDB := new(internal_mock.EmoteDB)

			emotesByIDsAPI := EmotesByIDsAPI{
				Clients: clients.Clients{
					EmoteDBUncached: mockEmoteDB,
				},
			}

			request := &mkd.GetEmotesByIDsRequest{EmoteIds: []string{"", ""}}
			resp, err := emotesByIDsAPI.GetEmotesByIDs(context.Background(), request)

			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
			So(err.Error(), ShouldContainSubstring, "emote_ids is required")
			So(len(mockEmoteDB.Calls), ShouldEqual, 0)
		})

		Convey("Given emoteDB returns error", func() {
			mockEmoteDB := new(internal_mock.EmoteDB)

			emotesByIDsAPI := EmotesByIDsAPI{
				Clients: clients.Clients{
					EmoteDBUncached: mockEmoteDB,
				},
			}

			emoteID := "3e22a0ab-6add-4a4c-9bbb-43897d196cdc"
			mockEmoteDB.On("ReadByIDs", mock.Anything, mock.Anything).Return(nil, errors.New("dummy aggregator error"))

			Convey("then return generic internal error", func() {
				request := &mkd.GetEmotesByIDsRequest{EmoteIds: []string{emoteID}}
				resp, err := emotesByIDsAPI.GetEmotesByIDs(context.Background(), request)

				So(resp, ShouldBeNil)
				So(err, ShouldNotBeNil)
				So(err.Error(), ShouldContainSubstring, "twirp error internal")
			})
		})

		Convey("Given successful emote fetch from emoteDB", func() {
			fakeEmoteDB := localdb.NewEmoteDB()

			emotesByIDsAPI := EmotesByIDsAPI{
				Clients: clients.Clients{
					EmoteDBUncached: fakeEmoteDB,
				},
			}

			testEmote1 := mako.Emote{
				ID:      "3e22a0ab-6add-4a4c-9bbb-43897d196cdc",
				OwnerID: "267893713",
				GroupID: "BitsBadgeTier-267893713-1000",
				Code:    "wolfHOWL",
				State:   mako.Active,
				Domain:  mako.EmotesDomain,
			}
			testEmote2 := mako.Emote{
				ID:      "3e22a0ab-6add-4a4c-9bbb-43897d196cdd",
				OwnerID: "267893713",
				GroupID: "BitsBadgeTier-267893713-5000",
				Code:    "wolfCUB",
				State:   mako.Active,
				Domain:  mako.EmotesDomain,
			}

			_, err := fakeEmoteDB.WriteEmotes(context.Background(), testEmote1, testEmote2)
			if err != nil {
				So(err, ShouldBeNil)
			}

			Convey("Doesn't return anything when emotes not found", func() {
				request := &mkd.GetEmotesByIDsRequest{EmoteIds: []string{"not_a_real_id"}}
				resp, err := emotesByIDsAPI.GetEmotesByIDs(context.Background(), request)

				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
				So(len(resp.Emotes), ShouldEqual, 0)
			})

			Convey("Returns the correct emote", func() {
				request := &mkd.GetEmotesByIDsRequest{EmoteIds: []string{testEmote1.ID}}
				resp, err := emotesByIDsAPI.GetEmotesByIDs(context.Background(), request)

				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
				So(len(resp.Emotes), ShouldEqual, 1)
				twirpTestEmote1 := testEmote1.ToDashboardTwirp()
				So(proto.Equal(resp.Emotes[0], &twirpTestEmote1), ShouldBeTrue)
			})

			Convey("Returns the correct number of emotes", func() {
				requestIDs := []string{testEmote1.ID, testEmote2.ID}
				request := &mkd.GetEmotesByIDsRequest{EmoteIds: requestIDs}
				resp, err := emotesByIDsAPI.GetEmotesByIDs(context.Background(), request)

				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
				So(len(resp.Emotes), ShouldEqual, 2)
				respLookup := make(map[string]*mkd.Emote, 2)
				for _, emote := range resp.Emotes {
					respLookup[emote.Id] = emote
				}

				twirpTestEmote1 := testEmote1.ToDashboardTwirp()
				So(proto.Equal(respLookup[testEmote1.ID], &twirpTestEmote1), ShouldBeTrue)
				twirpTestEmote2 := testEmote2.ToDashboardTwirp()
				So(proto.Equal(respLookup[testEmote2.ID], &twirpTestEmote2), ShouldBeTrue)
			})
		})
	})
}
