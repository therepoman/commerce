package api

import (
	"context"
	"time"

	"code.justin.tv/commerce/mako/clients/emotelimits"
	"code.justin.tv/commerce/mako/config"
	mako "code.justin.tv/commerce/mako/internal"

	log "code.justin.tv/commerce/logrus"

	mk "code.justin.tv/commerce/mako/twirp"
	"github.com/twitchtv/twirp"

	"code.justin.tv/commerce/mako/clients/emoticonsmanager"
	mkd "code.justin.tv/commerce/mako/twirp/dashboard"
)

var supportedOriginsForEmoteGroupAssignment = map[mkd.EmoteOrigin]bool{
	mkd.EmoteOrigin_BitsBadgeTierEmotes: true,
	mkd.EmoteOrigin_Subscriptions:       true,
	mkd.EmoteOrigin_Follower:            true,
}

// EmoteGroupAssignAPIs includes the APIs for assigning an emote to a group or removing it from one
type EmoteGroupAssignAPI struct {
	EmoticonsManager emoticonsmanager.IEmoticonsManager
	EmoteGroupDB     mako.EmoteGroupDB
	EmoteDB          mako.EmoteDB
	DynamicConfig    config.DynamicConfigClient
}

// AssignEmoteToGroup assigns an emote to a particular group (and removes it from its old group if it already had one)
// If the emote is already in this group, nothing happens.
func (ega *EmoteGroupAssignAPI) AssignEmoteToGroup(ctx context.Context, r *mkd.AssignEmoteToGroupRequest) (*mkd.AssignEmoteToGroupResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, 2*time.Second)
	defer cancel()

	err := validateAssignEmoteToGroupRequest(r)
	if err != nil {
		return nil, err
	}

	emote, err := ega.EmoticonsManager.AssignToGroup(ctx, r.GetRequestingUserId(), r.GetEmoteId(), r.GetGroupId(), r.GetOrigin())
	if err != nil {
		logFields := log.Fields{
			"userID":  r.GetRequestingUserId(),
			"emoteID": r.GetEmoteId(),
			"groupID": r.GetGroupId(),
			"origin":  r.GetOrigin(),
		}

		assignErr := convertAssignError(err)
		if assignErr != nil {
			return nil, assignErr
		}

		msg := "Error assigning emote to group"
		log.WithError(err).WithFields(logFields).Error(msg)
		return nil, twirp.InternalError(msg)
	}

	twirpEmote := emote.ToDashboardTwirp()
	return &mkd.AssignEmoteToGroupResponse{
		Emote: &twirpEmote,
	}, nil
}

func validateAssignEmoteToGroupRequest(r *mkd.AssignEmoteToGroupRequest) error {
	// Validate required arguments
	if r.GetRequestingUserId() == "" {
		return mk.NewClientError(
			twirp.RequiredArgumentError("requesting_user_id"),
			mk.ErrCodeMissingRequiredArgument)
	} else if r.GetEmoteId() == "" {
		return mk.NewClientError(
			twirp.RequiredArgumentError("emote_id"),
			mk.ErrCodeEmptyEmoteID)
	} else if r.GetGroupId() == "" {
		return mk.NewClientError(
			twirp.RequiredArgumentError("group_id"),
			mk.ErrCodeEmptyGroupID)
	}

	// Validate origin
	if isOriginSupported := supportedOriginsForEmoteGroupAssignment[r.GetOrigin()]; !isOriginSupported {
		return mk.NewClientError(
			twirp.InvalidArgumentError("origin", "only supports BitsBadgeTierEmotes and Subscriptions"),
			mk.ErrCodeUnsupportedEmoteOrigin)
	}

	return nil
}

func convertAssignError(err error) error {
	if err == emoticonsmanager.UserNotPermitted {
		return mk.NewClientError(
			twirp.NewError(twirp.PermissionDenied, "user is not permitted to modify this emote"),
			mk.ErrCodeUserNotPermitted)
	}

	if err == emoticonsmanager.EmoteDoesNotExist {
		return mk.NewClientError(
			twirp.NotFoundError("emote with the requested ID could not be found"),
			mk.ErrEmoteDoesNotExist)
	}

	if err == emoticonsmanager.InvalidEmoteState {
		return mk.NewClientError(
			twirp.NewError(twirp.FailedPrecondition, "emote state is not valid for group assignment"),
			mk.ErrCodeInvalidState)
	}

	if err == emoticonsmanager.EmoticonCodeNotUnique {
		return mk.NewClientError(
			twirp.NewError(twirp.FailedPrecondition, "emote does not have a unique code among active and pending emotes"),
			mk.ErrCodeCodeNotUnique)
	}

	if err == emoticonsmanager.EmoteAssetTypeDoesNotMatchGroupType {
		return mk.NewClientError(
			twirp.NewError(twirp.FailedPrecondition, "emote asset type does not match group type"),
			mk.ErrCodeInvalidAssetType)
	}

	return nil
}

func (ega *EmoteGroupAssignAPI) AssignEmoteToFollowerGroup(ctx context.Context, r *mkd.AssignEmoteToFollowerGroupRequest) (*mkd.AssignEmoteToFollowerGroupResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, 2*time.Second)
	defer cancel()

	err := validateAssignEmoteToFollowerGroupRequest(r)
	if err != nil {
		return nil, err
	}

	// Get whether this user is allowed to upload (or assign) new Follower Emotes
	isAllowed := ega.DynamicConfig.IsInFreemotesExperiment(r.GetChannelId())
	if !isAllowed {
		return nil, twirp.NewError(twirp.PermissionDenied, "This channel is not allowed to assign emotes to follower emote slots")
	}

	// Get this channel's Follower Emote group's groupID
	group, err := ega.EmoteGroupDB.GetFollowerEmoteGroup(ctx, r.GetChannelId())
	if err != nil {
		msg := "Error reading emote group while assigning Follower Emote to group"
		log.WithError(err).WithField("channelID", r.GetChannelId()).Error(msg)
		return nil, twirp.InternalError(msg)
	}

	if group == nil {
		msg := "Expected to find follower groupID for channel but found none"
		log.WithError(err).WithField("channelID", r.GetChannelId()).Error(msg)
		return nil, twirp.InternalError(msg)
	}

	groupID := group.GroupID

	// Get the follower emotes associated with this follower groupID if there are any
	emotesByGroupID, err := ega.EmoteDB.ReadByGroupIDs(ctx, groupID)
	if err != nil {
		logFields := log.Fields{
			"channelID": r.GetChannelId(),
			"userID":    r.GetRequestingUserId(),
			"groupID":   groupID,
		}

		msg := "Error reading emotes by groupID while assigning emote to follower group"
		log.WithError(err).WithFields(logFields).Error(msg)
		return nil, twirp.InternalError(msg)
	}

	// Prevent additional assignments if all Follower Emote slots for this channel are already filled
	if int32(len(emotesByGroupID)) >= emotelimits.FollowerEmoteLimit {
		return nil, twirp.NewError(twirp.FailedPrecondition, "This channel is not allowed to assign additional emotes to Follower Emote slots")
	}

	// At this point, everything has been validated, so assign the emote as a Follower Emote to the relevant Follower Emote group.
	emote, err := ega.EmoticonsManager.AssignToGroup(ctx, r.GetChannelId(), r.GetEmoteId(), groupID, mkd.EmoteOrigin_Follower)
	if err != nil {
		logFields := log.Fields{
			"channelID":        r.GetChannelId(),
			"requestingUserID": r.GetRequestingUserId(),
			"emoteID":          r.GetEmoteId(),
			"groupID":          groupID,
		}

		assignErr := convertAssignError(err)
		if assignErr != nil {
			return nil, assignErr
		}

		msg := "Error assigning emote to group"
		log.WithError(err).WithFields(logFields).Error(msg)
		return nil, twirp.InternalError(msg)
	}

	twirpEmote := emote.ToDashboardTwirp()
	return &mkd.AssignEmoteToFollowerGroupResponse{
		Emote: &twirpEmote,
	}, nil
}

func validateAssignEmoteToFollowerGroupRequest(r *mkd.AssignEmoteToFollowerGroupRequest) error {
	// Validate required arguments
	if r.GetChannelId() == "" {
		return mk.NewClientError(
			twirp.RequiredArgumentError("channel_id"),
			mk.ErrCodeMissingRequiredArgument)
	} else if r.GetRequestingUserId() == "" {
		return mk.NewClientError(
			twirp.RequiredArgumentError("requesting_user_id"),
			mk.ErrCodeMissingRequiredArgument)
	} else if r.GetEmoteId() == "" {
		return mk.NewClientError(
			twirp.RequiredArgumentError("emote_id"),
			mk.ErrCodeEmptyEmoteID)
	}

	if r.GetChannelId() != r.GetRequestingUserId() {
		return mk.NewClientError(
			twirp.NewError(twirp.PermissionDenied, "channel_id and requesting_user_id must be the same"),
			mk.ErrCodeUserNotPermitted)
	}

	return nil
}

// RemoveEmoteFromGroup removes an emote from its assigned group. If the emote already has no group, nothing happens.
func (ega *EmoteGroupAssignAPI) RemoveEmoteFromGroup(ctx context.Context, r *mkd.RemoveEmoteFromGroupRequest) (*mkd.RemoveEmoteFromGroupResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, time.Second)
	defer cancel()

	err := validateRemoveEmoteFromGroupRequest(r)
	if err != nil {
		return nil, err
	}

	emote, err := ega.EmoticonsManager.RemoveFromGroup(ctx, r.GetRequestingUserId(), r.GetEmoteId())
	if err != nil {
		logFields := log.Fields{
			"userID":  r.GetRequestingUserId(),
			"emoteID": r.GetEmoteId(),
		}

		removeErr := convertRemoveError(err)
		if removeErr != nil {
			return nil, removeErr
		}

		msg := "Error removing emote from group"
		log.WithError(err).WithFields(logFields).Error(msg)
		return nil, twirp.InternalError(msg)
	}

	twirpEmote := emote.ToDashboardTwirp()
	return &mkd.RemoveEmoteFromGroupResponse{
		Emote: &twirpEmote,
	}, nil
}

func validateRemoveEmoteFromGroupRequest(r *mkd.RemoveEmoteFromGroupRequest) error {
	// Validate required arguments
	if r.GetRequestingUserId() == "" {
		return mk.NewClientError(
			twirp.RequiredArgumentError("requesting_user_id"),
			mk.ErrCodeMissingRequiredArgument)
	} else if r.GetEmoteId() == "" {
		return mk.NewClientError(
			twirp.RequiredArgumentError("emote_id"),
			mk.ErrCodeEmptyEmoteID)
	}

	return nil
}

func convertRemoveError(err error) error {
	if err == emoticonsmanager.UserNotPermitted {
		return mk.NewClientError(
			twirp.NewError(twirp.PermissionDenied, "user is not permitted to modify this emote"),
			mk.ErrCodeUserNotPermitted)
	}

	if err == emoticonsmanager.EmoteDoesNotExist {
		return mk.NewClientError(
			twirp.NotFoundError("emote with the requested ID could not be found"),
			mk.ErrEmoteDoesNotExist)
	}

	return nil
}
