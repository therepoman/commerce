package api

import (
	"context"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/mako/clients"
	mk "code.justin.tv/commerce/mako/twirp"
	mkd "code.justin.tv/commerce/mako/twirp/dashboard"
	"github.com/twitchtv/twirp"
)

type UserProductEmoteGroupsAPI struct {
	Clients clients.Clients
}

func (u *UserProductEmoteGroupsAPI) GetUserProductEmoteGroups(ctx context.Context, r *mkd.GetUserProductEmoteGroupsRequest) (*mkd.GetUserProductEmoteGroupsResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, 1*time.Second)
	defer cancel()

	err := validateGetUserProductEmoteGroupsRequest(r)
	if err != nil {
		return nil, err
	}

	requestingUserID := r.GetRequestingUserId()
	ownerID := r.GetOwnerId()

	// Allow a staff member to get another user's emote groups
	if ownerID != requestingUserID {
		requestingUser, err := u.Clients.UserServiceClient.GetUserByID(ctx, requestingUserID, nil)
		if err != nil {
			msg := "error getting user for staff check to get user emote groups with product info"
			log.WithError(err).WithFields(log.Fields{
				"ownerID":          ownerID,
				"requestingUserID": requestingUserID,
			}).Error(msg)
			return nil, twirp.InternalError(msg)
		}

		if requestingUser.Admin == nil || !*requestingUser.Admin {
			return nil, twirp.NewError(twirp.PermissionDenied, "requesting user not allowed to view user's emote groups with product info")
		}
	}

	emoteGroups, err := u.Clients.ProductEmoteGroupsClient.GetProductEmoteGroups(ctx, ownerID)

	if err != nil {
		msg := "error getting emote group product types"
		log.WithError(err).WithFields(log.Fields{
			"ownerID":          ownerID,
			"requestingUserID": requestingUserID,
		}).Error(msg)
		return nil, twirp.InternalError(msg)
	}

	return &mkd.GetUserProductEmoteGroupsResponse{
		EmoteGroups: emoteGroups,
	}, nil
}

func validateGetUserProductEmoteGroupsRequest(r *mkd.GetUserProductEmoteGroupsRequest) error {
	// Validate required arguments
	if r.GetRequestingUserId() == "" {
		return mk.NewClientError(
			twirp.RequiredArgumentError("requesting_user_id"),
			mk.ErrCodeMissingRequiredArgument)
	} else if r.GetOwnerId() == "" {
		return mk.NewClientError(
			twirp.RequiredArgumentError("owner_id"),
			mk.ErrCodeMissingRequiredArgument)
	}

	return nil
}
