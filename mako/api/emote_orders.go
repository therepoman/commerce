package api

import (
	"context"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/mako/clients/emoticonsmanager"
	mako "code.justin.tv/commerce/mako/internal"
	mk "code.justin.tv/commerce/mako/twirp"
	dash "code.justin.tv/commerce/mako/twirp/dashboard"
	"github.com/twitchtv/twirp"
)

// EmoteOrdersAPI includes the API for modifying emote orders within a group
type EmoteOrdersAPI struct {
	EmoticonsManager emoticonsmanager.IEmoticonsManager
}

// UpdateEmoteOrders Get all entitled emoticon sets for a user
func (eo *EmoteOrdersAPI) UpdateEmoteOrders(ctx context.Context, r *dash.UpdateEmoteOrdersRequest) (*dash.UpdateEmoteOrdersResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()

	uoParams, err := validateAndParseEmoteOrderRequest(r)
	if err != nil {
		return nil, err
	}

	emotes, err := eo.EmoticonsManager.UpdateOrders(ctx, *uoParams)
	if err != nil {
		convertedErr := convertEmoteOrderError(err)
		if convertedErr != nil {
			return nil, convertedErr
		}
		log.WithError(err).WithFields(log.Fields{"userID": uoParams.UserID, "orderUpdates": uoParams.NewOrders}).Errorf("Failed to update emote orders")
		return nil, twirp.InternalErrorWith(err)
	}

	return emotesToOrdersTwirpResponse(emotes), nil
}

func emotesToOrdersTwirpResponse(emotes []mako.Emote) *dash.UpdateEmoteOrdersResponse {
	twirpEmotes := make([]*dash.Emote, len(emotes))
	for i, emote := range emotes {
		twirpEmote := emote.ToDashboardTwirp()
		twirpEmotes[i] = &twirpEmote
	}
	return &dash.UpdateEmoteOrdersResponse{Emotes: twirpEmotes}
}

func convertEmoteOrderError(orderError error) error {
	if orderError == emoticonsmanager.UserNotPermitted {
		return mk.NewClientError(
			twirp.NewError(twirp.PermissionDenied, "user is not permitted to modify selected emotes"),
			mk.ErrCodeInvalidUser)
	}

	if orderError == emoticonsmanager.GroupNotFound {
		return mk.NewClientError(
			twirp.InvalidArgumentError("emote_orders", "emote group contained zero emotes"),
			mk.ErrCodeGroupNotFound)
	}

	if orderError == emoticonsmanager.EmoteDoesNotExist {
		return mk.NewClientError(
			twirp.InvalidArgumentError("emote_orders", "one or more emotes could not be found"),
			mk.ErrEmoteDoesNotExist)
	}

	if orderError == emoticonsmanager.InvalidEmoteOrders {
		return mk.NewClientError(
			twirp.InvalidArgumentError("emote_orders", "the orders provided were not unique per group"),
			mk.ErrCodeOrdersNotUnique)
	}

	return nil
}

func validateAndParseEmoteOrderRequest(req *dash.UpdateEmoteOrdersRequest) (*emoticonsmanager.UpdateOrdersParams, error) {
	userID := req.GetRequestingUserId()
	if userID == "" {
		return nil, mk.NewClientError(
			twirp.RequiredArgumentError("requesting_user_id"),
			mk.ErrCodeMissingRequiredArgument)
	}

	twirpOrders := req.GetEmoteOrders()

	if len(twirpOrders) < 1 {
		return nil, mk.NewClientError(
			twirp.RequiredArgumentError("emote_orders"),
			mk.ErrCodeMissingRequiredArgument)
	}

	// We are choosing to support only one groupID at a time for this API right now
	groupID := twirpOrders[0].GroupId
	if groupID == "" {
		return nil, mk.NewClientError(
			twirp.RequiredArgumentError("emote_orders.group_id"),
			mk.ErrCodeEmptyGroupID)
	}

	orderUpdates := make(map[string]int64)

	for _, order := range twirpOrders {
		if order.GroupId != groupID {
			return nil, mk.NewClientError(
				twirp.InvalidArgumentError("emote_orders", "too many distinct groups"),
				mk.ErrCodeTooManyGroups)
		}
		if order.EmoteId == "" {
			return nil, mk.NewClientError(
				twirp.RequiredArgumentError("emote_orders.emote_id"),
				mk.ErrCodeEmptyEmoteID)
		}
		orderUpdates[order.EmoteId] = int64(order.Order)
	}

	if len(orderUpdates) < len(twirpOrders) {
		// At least one emoteID was duplicated
		return nil, mk.NewClientError(twirp.InvalidArgumentError("emote_orders", "emote given more than one order"),
			mk.ErrCodeOrdersNotUnique)
	}

	return &emoticonsmanager.UpdateOrdersParams{
		UserID:    userID,
		GroupID:   groupID,
		NewOrders: orderUpdates,
	}, nil
}
