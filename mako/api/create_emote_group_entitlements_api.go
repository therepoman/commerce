package api

import (
	"context"
	"time"

	"code.justin.tv/commerce/logrus"
	mako "code.justin.tv/commerce/mako/internal"
	"github.com/pkg/errors"

	"code.justin.tv/commerce/mako/clients"
	mk "code.justin.tv/commerce/mako/twirp"
	"github.com/twitchtv/twirp"
)

type CreateEmoteGroupEntitlementsAPI struct {
	Clients clients.Clients
}

func (ce *CreateEmoteGroupEntitlementsAPI) CreateEmoteGroupEntitlements(ctx context.Context, r *mk.CreateEmoteGroupEntitlementsRequest) (*mk.CreateEmoteGroupEntitlementsResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, 3*time.Second)
	defer cancel()

	entitlements := make([]mako.Entitlement, len(r.Entitlements))

	for i, entitlement := range r.Entitlements {
		err := validateGroupEntitlement(entitlement)

		if err != nil {
			return nil, err
		}

		var source mako.EntitlementSource
		switch entitlement.Group {
		case mk.EmoteGroup_BitsBadgeTierEmotes:
			source = mako.BitsBadgeTierSource
		case mk.EmoteGroup_Subscriptions:
			source = mako.SubscriptionsSource
		case mk.EmoteGroup_Rewards, mk.EmoteGroup_TwoFactor:
			// TODO (DIGI-220): Remove this after legacy entitlementsDB migration
			source = mako.RewardsSource
		default:
			logrus.Errorf("the group '%s' is not supported by the CreateEmoteGroupEntitlements API", entitlement.Group.String())
			return nil, twirp.InvalidArgumentError("group", "The group specified is not supported for this API")
		}

		var start time.Time
		if entitlement.Start != nil {
			start = time.Unix(entitlement.Start.Seconds, 0)
		}

		var end *time.Time
		if entitlement.End != nil && !entitlement.End.IsInfinite {
			if entitlement.End.Time != nil {
				t := time.Unix(entitlement.End.Time.Seconds, 0)
				end = &t
			}
		}

		entitlements[i] = mako.Entitlement{
			OwnerID:  entitlement.OwnerId,
			Source:   source,
			OriginID: entitlement.OriginId,
			// ItemOwnerId is typically a channel_id and represents the owner of the emote group.
			ChannelID: entitlement.ItemId,
			ID:        entitlement.GroupId,
			Start:     start,
			End:       end,
			Type:      mako.EmoteGroupEntitlement,
		}
	}

	if err := ce.Clients.EntitlementDB.WriteEntitlements(ctx, entitlements...); err != nil {
		logrus.WithError(err).Error("failed to create group entitlement")
		return nil, twirp.InternalErrorWith(errors.Wrap(err, "failed to create group entitlements"))
	}

	ownerIDsToCacheInvalidate := map[string]bool{}
	for _, entitlement := range r.Entitlements {
		if ownerIDsToCacheInvalidate[entitlement.OwnerId] {
			continue
		}
		ownerIDsToCacheInvalidate[entitlement.OwnerId] = true
	}
	go func() {
		for ownerID := range ownerIDsToCacheInvalidate {
			if err := ce.Clients.EmoteModifiersDB.InvalidateEntitledGroupsCacheForUser(ownerID); err != nil {
				logrus.WithError(err).Error("failed to invalidate emote mod group entitlement redis cache")
			}
		}
	}()

	return &mk.CreateEmoteGroupEntitlementsResponse{}, nil
}

func validateGroupEntitlement(entitlement *mk.EmoteGroupEntitlement) error {
	if entitlement.OwnerId == "" {
		return twirp.InvalidArgumentError("OwnerId", "Required")
	}

	if entitlement.OriginId == "" {
		return twirp.InvalidArgumentError("OriginId", "Required")
	}

	if entitlement.GroupId == mako.RobotsSmilieGroupID.ToString() ||
		entitlement.GroupId == mako.PurpleSmilieGroupID.ToString() ||
		entitlement.GroupId == mako.MonkeysSmilieGroupID.ToString() {
		return twirp.InvalidArgumentError("GroupId", "Must use SetSmilies to entitle smilies emotes")
	}

	if entitlement.ItemId == "" {
		return twirp.InvalidArgumentError("ItemId", "Required")
	}

	if entitlement.Start == nil {
		return twirp.InvalidArgumentError("Start", "Required")
	}

	if !entitlement.End.IsInfinite && entitlement.End.Time == nil {
		return twirp.InvalidArgumentError("End", "End.IsInfinite must be true or End.Time must be set")
	}

	return nil
}
