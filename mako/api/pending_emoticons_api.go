package api

import (
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/mako/clients"
	"code.justin.tv/commerce/mako/clients/pendingemoticons/models"
	"code.justin.tv/commerce/mako/dynamo"
	mako "code.justin.tv/commerce/mako/internal"
	mk "code.justin.tv/commerce/mako/twirp"
	"github.com/pkg/errors"
	"github.com/twitchtv/twirp"
	"golang.org/x/net/context"
	"google.golang.org/protobuf/types/known/timestamppb"
)

// PendingEmoticonsAPI implements all twirp endpoints for the pending emoticons flow
type PendingEmoticonsAPI struct {
	Clients clients.Clients
}

func (p *PendingEmoticonsAPI) GetPendingEmoticonsByIDs(ctx context.Context, req *mk.GetPendingEmoticonsByIDsRequest) (*mk.GetPendingEmoticonsByIDsResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, time.Second)
	defer cancel()

	emotes, err := p.Clients.PendingEmoticonsClient.GetPendingEmoticonsByIDs(ctx, req.EmoticonIds)
	if err != nil {
		return nil, twirp.InternalErrorWith(err).WithMeta("request", req.String())
	}

	twirpEmoticons, err := pendingEmoticonsToTwirpPendingEmoticons(emotes)
	if err != nil {
		return nil, twirp.InternalErrorWith(err).WithMeta("request", req.String())
	}

	return &mk.GetPendingEmoticonsByIDsResponse{Emoticons: twirpEmoticons}, nil
}

func (p *PendingEmoticonsAPI) GetPendingEmoticons(ctx context.Context, req *mk.GetPendingEmoticonsRequest) (*mk.GetPendingEmoticonsResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, time.Second*5)
	defer cancel()

	if req.Count == 0 {
		// Use default value
		req.Count = 25
	}

	if req.Count < 1 {
		return nil, twirp.InvalidArgumentError("count", "must be more than 0").WithMeta("request", req.String())
	}

	afterTime := time.Time{}
	if req.AfterTimestamp != nil {
		afterTime = req.AfterTimestamp.AsTime()
	}

	// Use default bucket values if Bucket is not provided
	bucketCount := 1
	bucketNumber := 0
	if req.Bucket != nil {
		bucketCount = int(req.Bucket.BucketCount)
		bucketNumber = int(req.Bucket.Requested)
	}
	if bucketCount < 1 {
		return nil, twirp.InvalidArgumentError("bucketCount", "must be more than 0").WithMeta("request", req.String())
	}
	if bucketNumber >= bucketCount {
		return nil, twirp.InvalidArgumentError("bucketNumber", "must be less than bucketCount").WithMeta("request", req.String())
	}

	pendingEmoticons, err := p.Clients.PendingEmoticonsClient.GetPendingEmoticons(
		ctx,
		int(req.Count),
		twirpFilterToEmoticonsFilter(req.Filter),
		twirpReviewStateFilterToReviewStateFilter(req.ReviewStateFilter),
		twirpAssetTypeFilterToAssetTypeFilter(req.AssetTypeFilter),
		afterTime.UTC(),
		bucketCount,
		bucketNumber,
	)

	if err != nil {
		return nil, twirp.InternalErrorWith(err).WithMeta("request", req.String())
	}

	twirpEmoticons, err := pendingEmoticonsToTwirpPendingEmoticons(pendingEmoticons)
	if err != nil {
		return nil, twirp.InternalErrorWith(err).WithMeta("request", req.String())
	}

	return &mk.GetPendingEmoticonsResponse{
		Emoticons: twirpEmoticons,
	}, nil
}

func (p *PendingEmoticonsAPI) ReviewPendingEmoticons(ctx context.Context, req *mk.ReviewPendingEmoticonsRequest) (*mk.ReviewPendingEmoticonsResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, time.Second*5)
	defer cancel()

	log.WithFields(log.Fields{
		"ReviewPendingEmoticonsRequest": req.String(),
	}).Info("Review Pending Emoticons")

	reviews := make([]models.EmoticonReview, len(req.Reviews))
	for i, twirpReview := range req.Reviews {
		reviews[i] = twirpReviewToEmoticonsReview(twirpReview)
	}

	results, err := p.Clients.PendingEmoticonsClient.ReviewPendingEmoticons(ctx, reviews, req.AdminId)

	if err != nil {
		return nil, twirp.InternalErrorWith(err).WithMeta("request", req.String())
	}

	return &mk.ReviewPendingEmoticonsResponse{Results: reviewEmoticonsResultsToTwirpResults(results)}, nil
}

func (p *PendingEmoticonsAPI) DeferPendingEmoticons(ctx context.Context, req *mk.DeferPendingEmoticonsRequest) (*mk.DeferPendingEmoticonsResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, time.Second*5)
	defer cancel()

	log.WithFields(log.Fields{
		"DeferPendingEmoticonsRequest": req.String(),
	}).Info("Defer Pending Emoticons")

	if req == nil {
		return nil, twirp.InvalidArgumentError("request", "must not be nil")
	} else if len(req.EmoticonIds) == 0 {
		return nil, twirp.InvalidArgumentError("EmoticonIds", "must not be empty")
	}

	results, err := p.Clients.PendingEmoticonsClient.DeferPendingEmoticons(ctx, req.EmoticonIds, req.AdminId)
	if err != nil {
		return nil, twirp.InternalErrorWith(err).WithMeta("request", req.String())
	}

	return &mk.DeferPendingEmoticonsResponse{Results: deferPendingEmoticonsResultsToTwirpResults(results)}, nil
}

func (p *PendingEmoticonsAPI) CreatePendingEmoticon(ctx context.Context, req *mk.CreatePendingEmoticonRequest) (*mk.CreatePendingEmoticonResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, time.Second*5)
	defer cancel()

	accountType, err := twirpAccountTypeToAccountType(req.AccountType)

	if err != nil {
		return nil, twirp.InternalErrorWith(err).WithMeta("request", req.String())
	}

	// All pending emotes should default to no review state
	pendingEmoteReviewState := dynamo.PendingEmoteReviewStateNone

	assetType := twirpAssetTypeToAssetType(req.AssetType)
	animationTemplate := twirpAnimationTemplateToAnimationTemplate(req.AnimatedEmoteTemplate)

	pendingEmoticon, err := p.Clients.PendingEmoticonsClient.CreatePendingEmoticon(ctx, req.Id, req.Prefix, req.Code, req.OwnerId, accountType, pendingEmoteReviewState, assetType, animationTemplate)

	if err != nil {
		return nil, twirp.InternalErrorWith(err).WithMeta("request", req.String())
	}

	twirpPendingEmoticon, err := pendingEmoticonToTwirpPendingEmoticon(pendingEmoticon)

	if err != nil {
		return nil, twirp.InternalErrorWith(err).WithMeta("request", req.String())
	}

	return &mk.CreatePendingEmoticonResponse{
		PendingEmoticon: twirpPendingEmoticon,
	}, nil
}

func (p *PendingEmoticonsAPI) DeletePendingEmoticons(ctx context.Context, req *mk.DeletePendingEmoticonsRequest) (*mk.DeletePendingEmoticonsResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, time.Second*5)
	defer cancel()

	err := p.Clients.PendingEmoticonsClient.DeletePendingEmoticons(ctx, req.EmoticonIds)

	if err != nil {
		return nil, twirp.InternalErrorWith(err).WithMeta("request", req.String())
	}

	return &mk.DeletePendingEmoticonsResponse{}, nil
}

func twirpFilterToEmoticonsFilter(filter mk.EmoticonFilter) models.PendingEmoticonFilter {
	switch filter {
	case mk.EmoticonFilter_EmoticonFilter_Partner:
		return models.PendingEmoticonFilterPartner
	case mk.EmoticonFilter_EmoticonFilter_Affiliate:
		return models.PendingEmoticonFilterAffiliate
	default:
		return models.PendingEmoticonFilterAll
	}
}

func twirpReviewStateFilterToReviewStateFilter(filter mk.PendingEmoticonReviewStateFilter) models.PendingEmoticonReviewStateFilter {
	switch filter {
	case mk.PendingEmoticonReviewStateFilter_PendingEmoticonReviewStateFilter_None:
		return models.PendingEmoticonReviewStateFilterNone
	case mk.PendingEmoticonReviewStateFilter_PendingEmoticonReviewStateFilter_Deferred:
		return models.PendingEmoticonReviewStateFilterDeferred
	default:
		return models.PendingEmoticonReviewStateFilterAll
	}
}

func twirpAccountTypeToAccountType(accountType mk.AccountType) (dynamo.AccountType, error) {
	switch accountType {
	case mk.AccountType_Affiliate:
		return dynamo.Affiliate, nil
	case mk.AccountType_Partner:
		return dynamo.Partner, nil
	default:
		return "", errors.New("Unknown account type")
	}
}

func twirpAssetTypeFilterToAssetTypeFilter(assetType mk.PendingEmoticonAssetTypeFilter) models.PendingEmoteAssetTypeFilter {
	switch assetType {
	case mk.PendingEmoticonAssetTypeFilter_PendingEmoticonAssetTypeFilter_Static:
		return models.PendingEmoteAssetTypeFilterStatic
	case mk.PendingEmoticonAssetTypeFilter_PendingEmoticonAssetTypeFilter_Animated:
		return models.PendingEmoteAssetTypeFilterAnimated
	default:
		return models.PendingEmoteAssetTypeFilterAll
	}
}

func accountTypeToTwirpAccountType(accountType dynamo.AccountType) (mk.AccountType, error) {
	switch accountType {
	case dynamo.Affiliate:
		return mk.AccountType_Affiliate, nil
	case dynamo.Partner:
		return mk.AccountType_Partner, nil
	default:
		return mk.AccountType(-1), errors.New("Unknown account type")
	}
}

func twirpReviewToEmoticonsReview(review *mk.ReviewPendingEmoticonRequest) models.EmoticonReview {
	return models.EmoticonReview{
		EmoticonID:             review.Id,
		Approve:                review.Approve,
		RejectReason:           review.DeclineReason,
		RejectDescription:      review.DeclineExplanation,
		ShouldSkipNotification: review.ShouldSkipNotification,
	}
}

func reviewEmoticonsResultsToTwirpResults(results []models.ReviewEmoticonResponse) []*mk.ReviewPendingEmoticonResponse {
	ret := make([]*mk.ReviewPendingEmoticonResponse, len(results))
	for i, result := range results {
		ret[i] = reviewEmoticonsResultToTwirpResult(result)
	}
	return ret
}

func reviewEmoticonsResultToTwirpResult(result models.ReviewEmoticonResponse) *mk.ReviewPendingEmoticonResponse {
	return &mk.ReviewPendingEmoticonResponse{
		Id:        result.EmoticonID,
		Succeeded: result.Succeeded,
		NewStatus: string(result.NewState),
	}
}

func deferPendingEmoticonsResultsToTwirpResults(results []*models.DeferPendingEmoticonResponse) []*mk.DeferPendingEmoticonsResult {
	ret := make([]*mk.DeferPendingEmoticonsResult, len(results))
	for i, result := range results {
		ret[i] = &mk.DeferPendingEmoticonsResult{
			EmoticonId: result.EmoticonID,
			Succeeded:  result.Succeeded,
		}
	}
	return ret
}

func pendingEmoticonToTwirpPendingEmoticon(emoticon dynamo.PendingEmote) (*mk.PendingEmoticon, error) {
	accountType, err := accountTypeToTwirpAccountType(emoticon.Type)

	if err != nil {
		return nil, err
	}

	return &mk.PendingEmoticon{
		Id:                    emoticon.EmoteId,
		Prefix:                emoticon.Prefix,
		Code:                  emoticon.Code,
		OwnerId:               emoticon.OwnerId,
		UploadTime:            timestamppb.New(emoticon.UpdatedAt),
		AccountType:           accountType,
		AssetType:             assetTypeToTwirpAssetType(emoticon.AssetType),
		AnimatedEmoteTemplate: animationTemplateToTwirpAnimationType(emoticon.AnimationTemplate),
	}, nil
}

func assetTypeToTwirpAssetType(assetType string) mk.EmoteAssetType {
	result := mk.EmoteAssetType_static

	if assetType == string(mako.AnimatedAssetType) {
		result = mk.EmoteAssetType_animated
	}

	return result
}

func twirpAssetTypeToAssetType(assetType mk.EmoteAssetType) mako.EmoteAssetType {
	result := mako.StaticAssetType

	if assetType == mk.EmoteAssetType_animated {
		result = mako.AnimatedAssetType
	}

	return result
}

func twirpAnimationTemplateToAnimationTemplate(animationTemplate mk.AnimatedEmoteTemplate) mako.AnimatedEmoteTemplate {
	switch animationTemplate {
	case mk.AnimatedEmoteTemplate_NO_TEMPLATE:
		return mako.NoTemplate
	case mk.AnimatedEmoteTemplate_SHAKE:
		return mako.Shake
	case mk.AnimatedEmoteTemplate_ROLL:
		return mako.Roll
	case mk.AnimatedEmoteTemplate_SPIN:
		return mako.Spin
	case mk.AnimatedEmoteTemplate_RAVE:
		return mako.Rave
	case mk.AnimatedEmoteTemplate_SLIDEIN:
		return mako.SlideIn
	case mk.AnimatedEmoteTemplate_SLIDEOUT:
		return mako.SlideOut
	default:
		return mako.NoTemplate
	}
}

func animationTemplateToTwirpAnimationType(template string) mk.AnimatedEmoteTemplate {
	switch template {
	case string(mako.NoTemplate):
		return mk.AnimatedEmoteTemplate_NO_TEMPLATE
	case string(mako.Shake):
		return mk.AnimatedEmoteTemplate_SHAKE
	case string(mako.Roll):
		return mk.AnimatedEmoteTemplate_ROLL
	case string(mako.Spin):
		return mk.AnimatedEmoteTemplate_SPIN
	case string(mako.Rave):
		return mk.AnimatedEmoteTemplate_RAVE
	case string(mako.SlideIn):
		return mk.AnimatedEmoteTemplate_SLIDEIN
	case string(mako.SlideOut):
		return mk.AnimatedEmoteTemplate_SLIDEOUT
	default:
		return mk.AnimatedEmoteTemplate_NO_TEMPLATE
	}
}

func pendingEmoticonsToTwirpPendingEmoticons(emoticons []*dynamo.PendingEmote) ([]*mk.PendingEmoticon, error) {
	ret := make([]*mk.PendingEmoticon, len(emoticons))
	for i, emoticon := range emoticons {
		if emoticon == nil {
			continue
		}

		twirpPendingEmoticon, err := pendingEmoticonToTwirpPendingEmoticon(*emoticon)

		if err != nil {
			return nil, err
		}

		ret[i] = twirpPendingEmoticon
	}

	return ret, nil
}
