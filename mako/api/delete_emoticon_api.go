package api

import (
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/mako/clients"
	"code.justin.tv/commerce/mako/clients/emoticonsmanager"
	mk "code.justin.tv/commerce/mako/twirp"
	"github.com/twitchtv/twirp"
	"golang.org/x/net/context"
)

type DeleteEmoticonAPI struct {
	Clients clients.Clients
}

func (de *DeleteEmoticonAPI) DeleteEmoticon(ctx context.Context, r *mk.DeleteEmoticonRequest) (*mk.DeleteEmoticonResponse, error) {
	// This API has a wildly different latency depending on whether or not the DeleteImages flag is provided.
	// If DeleteImages is provided we leave the context timeout unbounded.
	if !r.DeleteImages {
		var cancel context.CancelFunc
		ctx, cancel = context.WithTimeout(ctx, 1*time.Second)
		defer cancel()
	}

	log.Infof("Delete Emote: %s", r.String())
	err := validateDeleteEmoticonRequest(r)
	if err != nil {
		return nil, err
	}

	params := convertRequestToDeleteParams(r)

	err = de.Clients.EmoticonsManager.Delete(ctx, params)
	if err != nil {
		log.WithFields(log.Fields{
			"request": r.String(),
		}).WithError(err).Error("Error deleting emote")

		twirpErr := convertErrorToTwirp(err)
		return nil, twirpErr
	}
	return &mk.DeleteEmoticonResponse{}, nil
}

func validateDeleteEmoticonRequest(r *mk.DeleteEmoticonRequest) error {
	if r.GetId() == "" {
		return twirp.RequiredArgumentError("id")
	}
	return nil
}

func convertRequestToDeleteParams(r *mk.DeleteEmoticonRequest) emoticonsmanager.DeleteParams {
	return emoticonsmanager.DeleteParams{
		EmoticonID:            r.GetId(),
		DeleteS3Images:        r.GetDeleteImages(),
		RequiresPermittedUser: r.GetRequiresPermittedUser(),
		UserID:                r.GetUserId(),
	}
}

func convertErrorToTwirp(deleteError error) error {
	if deleteError == emoticonsmanager.UserNotPermitted {
		return mk.NewClientError(
			twirp.NewError(twirp.PermissionDenied, "user not permitted to modify this emote"),
			mk.ErrCodeUserNotPermitted)
	}

	if deleteError == emoticonsmanager.EmoteDoesNotExist {
		return mk.NewClientError(
			twirp.NotFoundError("no emote found with the requested id"),
			mk.ErrEmoteDoesNotExist)
	}

	return twirp.InternalError("Error deleting emote")
}
