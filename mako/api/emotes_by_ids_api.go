package api

import (
	"context"
	"time"

	"code.justin.tv/commerce/gogogadget/strings"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/mako/clients"
	mkd "code.justin.tv/commerce/mako/twirp/dashboard"
	"github.com/twitchtv/twirp"
	"github.com/twitchtv/twirp/hooks/statsd"
)

// EmotesByIDsAPI includes the API for getting emotes by ID
type EmotesByIDsAPI struct {
	Clients clients.Clients
	Stats   statsd.Statter
}

// GetEmotesByIDs returns all emotes for the given emote ids while skipping all caches (for uses on the dashboard or admin tooling)
func (emotesByIDsAPI *EmotesByIDsAPI) GetEmotesByIDs(ctx context.Context, r *mkd.GetEmotesByIDsRequest) (*mkd.GetEmotesByIDsResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, time.Second)
	defer cancel()

	// Filter out obviously invalid IDs (this currently only means empty string)
	prunedIDs := strings.RemoveEmptyStrings(r.EmoteIds)

	if len(prunedIDs) < 1 {
		return nil, twirp.RequiredArgumentError("emote_ids")
	}

	emotes, err := emotesByIDsAPI.Clients.EmoteDBUncached.ReadByIDs(ctx, prunedIDs...)
	if err != nil {
		msg := "Error reading emotes by emote ids in Dashboard GetEmotesByIDs"
		log.WithError(err).WithField("emote_ids", prunedIDs).Error(msg)
		return nil, twirp.InternalError(msg)
	}

	var result []*mkd.Emote
	for _, emote := range emotes {
		mkdEmote := emote.ToDashboardTwirp()
		result = append(result, &mkdEmote)
	}

	return &mkd.GetEmotesByIDsResponse{
		Emotes: result,
	}, nil
}
