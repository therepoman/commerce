package api

import (
	"context"

	"github.com/twitchtv/twirp"

	mkd "code.justin.tv/commerce/mako/twirp/dashboard"
)

type UserRolloutStatusAPI struct {
}

func (u *UserRolloutStatusAPI) GetUserRolloutStatus(ctx context.Context, r *mkd.GetUserRolloutStatusRequest) (*mkd.GetUserRolloutStatusResponse, error) {
	userID := r.GetUserId()
	if userID == "" {
		return nil, twirp.RequiredArgumentError("user_id")
	}

	return &mkd.GetUserRolloutStatusResponse{
		IsDigitalAssetManagerM2Enabled: true,
	}, nil
}
