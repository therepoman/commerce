package api

import (
	"context"
	"testing"

	"code.justin.tv/commerce/mako/clients"
	"code.justin.tv/commerce/mako/internal"
	internal_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/internal_mock"
	mako_client "code.justin.tv/commerce/mako/twirp"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestBatchValidateCodeUniquenessAPI(t *testing.T) {
	Convey("Test Batch Validate Code Uniqueness API", t, func() {
		mockEmoteDB := new(internal_mock.EmoteDB)
		emoteCodeUnique := EmoteCodeUniqueAPI{
			Clients: clients.Clients{
				EmoteDB: mockEmoteDB,
			},
		}

		testContext := context.Background()
		emoteCode := "wolfPUP"
		emoteCode2 := "wolfALPHA"

		Convey("Test AreTheseEmoteCodesUnique", func() {
			Convey("Given request is missing filter, return error", func() {
				resp, err := emoteCodeUnique.BatchValidateCodeUniqueness(testContext, &mako_client.BatchValidateCodeUniquenessRequest{
					EmoteCodes: []string{emoteCode},
				})

				So(resp, ShouldBeNil)
				So(err, ShouldNotBeNil)
				So(err.Error(), ShouldContainSubstring, "twirp error invalid_argument: filter is empty")
			})

			Convey("Given request filter contains invalid domain, return error", func() {
				resp, err := emoteCodeUnique.BatchValidateCodeUniqueness(testContext, &mako_client.BatchValidateCodeUniquenessRequest{
					EmoteCodes: []string{emoteCode},
					Filter: &mako_client.EmoteFilter{
						Domain: 999999,
					},
				})

				So(resp, ShouldBeNil)
				So(err, ShouldNotBeNil)
				So(err.Error(), ShouldContainSubstring, "twirp error invalid_argument: domain is invalid")
			})

			Convey("Given request filter contains invalid state, return error", func() {
				resp, err := emoteCodeUnique.BatchValidateCodeUniqueness(testContext, &mako_client.BatchValidateCodeUniquenessRequest{
					EmoteCodes: []string{emoteCode},
					Filter: &mako_client.EmoteFilter{
						Domain: 0,
						States: []mako_client.EmoteState{999999},
					},
				})

				So(resp, ShouldBeNil)
				So(err, ShouldNotBeNil)
				So(err.Error(), ShouldContainSubstring, "twirp error invalid_argument: state is invalid")
			})

			Convey("Given request contains blank emote codes, return error", func() {
				resp, err := emoteCodeUnique.BatchValidateCodeUniqueness(testContext, &mako_client.BatchValidateCodeUniquenessRequest{
					EmoteCodes: []string{""},
					Filter: &mako_client.EmoteFilter{
						Domain: 0,
						States: []mako_client.EmoteState{0},
					},
				})

				So(resp, ShouldBeNil)
				So(err, ShouldNotBeNil)
				So(err.Error(), ShouldContainSubstring, "twirp error invalid_argument: emote_code can't be blank")
			})

			Convey("Given valid request with active emote filter", func() {
				req := mako_client.BatchValidateCodeUniquenessRequest{
					EmoteCodes: []string{emoteCode},
					Filter: &mako_client.EmoteFilter{
						Domain: 0,
						States: []mako_client.EmoteState{0},
					},
				}

				Convey("Given emotes db returns error, then return error", func() {
					mockEmoteDB.On("ReadByCodes", mock.Anything, mock.Anything).Return(nil, errors.New("dummy dynamo error"))

					resp, err := emoteCodeUnique.BatchValidateCodeUniqueness(testContext, &req)

					So(resp, ShouldBeNil)
					So(err, ShouldNotBeNil)
					So(err.Error(), ShouldContainSubstring, "dummy dynamo error")
				})

				Convey("Given emote exists in db and is active, then return not unique", func() {
					mockEmoteDB.On("ReadByCodes", mock.Anything, emoteCode).Return([]mako.Emote{
						{
							Code:   emoteCode,
							Domain: mako.EmotesDomain,
							State:  mako.Active,
						},
					}, nil)

					resp, err := emoteCodeUnique.BatchValidateCodeUniqueness(testContext, &req)

					So(resp, ShouldNotBeNil)
					So(err, ShouldBeNil)
					So(resp.IsUnique[emoteCode], ShouldBeFalse)
				})

				Convey("Given emote exists in db and is inactive, then return unique", func() {
					mockEmoteDB.On("ReadByCodes", mock.Anything, emoteCode).Return([]mako.Emote{
						{
							Code:   emoteCode,
							Domain: mako.EmotesDomain,
							State:  mako.Inactive,
						},
					}, nil)

					resp, err := emoteCodeUnique.BatchValidateCodeUniqueness(testContext, &req)

					So(resp, ShouldNotBeNil)
					So(err, ShouldBeNil)
					So(resp.IsUnique[emoteCode], ShouldBeTrue)
				})

				Convey("Given emote does not exist in db, then return unique", func() {
					mockEmoteDB.On("ReadByCodes", mock.Anything, emoteCode).Return(nil, nil)

					resp, err := emoteCodeUnique.BatchValidateCodeUniqueness(testContext, &req)

					So(resp, ShouldNotBeNil)
					So(err, ShouldBeNil)
					So(resp.IsUnique[emoteCode], ShouldBeTrue)
				})
			})

			Convey("Given valid request for multiple emote codes", func() {
				req := mako_client.BatchValidateCodeUniquenessRequest{
					EmoteCodes: []string{emoteCode, emoteCode2},
					Filter: &mako_client.EmoteFilter{
						Domain: 0,
						States: []mako_client.EmoteState{0},
					},
				}

				Convey("Given both emote codes currently exist, return not unique for both", func() {
					mockEmoteDB.On("ReadByCodes", mock.Anything, mock.Anything, mock.Anything).Return([]mako.Emote{
						{
							Code:   emoteCode,
							Domain: mako.EmotesDomain,
							State:  mako.Active,
						},
						{
							Code:   emoteCode2,
							Domain: mako.EmotesDomain,
							State:  mako.Active,
						},
					}, nil)

					resp, err := emoteCodeUnique.BatchValidateCodeUniqueness(testContext, &req)

					So(resp, ShouldNotBeNil)
					So(err, ShouldBeNil)
					So(resp.IsUnique[emoteCode], ShouldBeFalse)
					So(resp.IsUnique[emoteCode2], ShouldBeFalse)
				})

				Convey("Given both emote codes currently exist, but only one is active, return unique for inactive emote and not unique for active emote", func() {
					mockEmoteDB.On("ReadByCodes", mock.Anything, mock.Anything, mock.Anything).Return([]mako.Emote{
						{
							Code:   emoteCode,
							Domain: mako.EmotesDomain,
							State:  mako.Active,
						},
						{
							Code:   emoteCode2,
							Domain: mako.EmotesDomain,
							State:  mako.Inactive,
						},
					}, nil)

					resp, err := emoteCodeUnique.BatchValidateCodeUniqueness(testContext, &req)

					So(resp, ShouldNotBeNil)
					So(err, ShouldBeNil)
					So(resp.IsUnique[emoteCode], ShouldBeFalse)
					So(resp.IsUnique[emoteCode2], ShouldBeTrue)
				})

				Convey("Given both emote codes currently do not exist, return unique for both", func() {
					mockEmoteDB.On("ReadByCodes", mock.Anything, mock.Anything, mock.Anything).Return(nil, nil)

					resp, err := emoteCodeUnique.BatchValidateCodeUniqueness(testContext, &req)

					So(resp, ShouldNotBeNil)
					So(err, ShouldBeNil)
					So(resp.IsUnique[emoteCode], ShouldBeTrue)
					So(resp.IsUnique[emoteCode2], ShouldBeTrue)
				})
			})
		})
	})
}
