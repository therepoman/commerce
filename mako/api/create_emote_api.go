package api

import (
	"context"
	"fmt"
	"time"

	"code.justin.tv/commerce/mako/config"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/mako/clients"
	em "code.justin.tv/commerce/mako/clients/emoticonsmanager"
	"code.justin.tv/commerce/mako/clients/paint"
	mako "code.justin.tv/commerce/mako/internal"
	mk "code.justin.tv/commerce/mako/twirp"
	mkd "code.justin.tv/commerce/mako/twirp/dashboard"
	"code.justin.tv/subs/paint/paintrpc"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/twitchtv/twirp"
)

type CreateEmoteAPI struct {
	Clients       clients.Clients
	DynamicConfig config.DynamicConfigClient
	Stats         statsd.Statter
}

func (ce *CreateEmoteAPI) CreateEmote(ctx context.Context, r *mkd.CreateEmoteRequest) (*mkd.CreateEmoteResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()

	log.Infof("Create Emote: %s", r.String())

	err := validateCreateEmoteRequest(r)

	if err != nil {
		log.WithField("request", r.String()).WithError(err).Error("Invalid CreateEmote Request")
		return nil, err
	}

	// TODO: remove this check once the Follower Emotes feature is GA fully launched, (since good standing is already checked below)
	// Validate follower emote is in the follower emotes experiment
	if r.GetOrigin() == mkd.EmoteOrigin_Follower {
		isInExperiment := ce.DynamicConfig.IsInFreemotesExperiment(r.GetUserId())
		if !isInExperiment {
			return nil, twirp.NewError(twirp.PermissionDenied, "you are not allowed to create follower emotes")
		}
	}

	isAnimated := false
	var image1xID,
		image2xID,
		image4xID,
		image1xAnimatedID,
		image2xAnimatedID,
		image4xAnimatedID string

	if r.GetAssetType() == mkd.EmoteAssetType_static {
		asset := r.GetImageAssets()[0]
		image1xID = asset.ImageIds.Image1XId
		image2xID = asset.ImageIds.Image2XId
		image4xID = asset.ImageIds.Image4XId

	} else if r.GetAssetType() == mkd.EmoteAssetType_animated {
		isAnimated = true

		for _, asset := range r.GetImageAssets() {
			if asset.AssetType == mkd.EmoteAssetType_animated {
				image1xAnimatedID = asset.ImageIds.Image1XId
				image2xAnimatedID = asset.ImageIds.Image2XId
				image4xAnimatedID = asset.ImageIds.Image4XId
			} else if asset.AssetType == mkd.EmoteAssetType_static {
				image1xID = asset.ImageIds.Image1XId
				image2xID = asset.ImageIds.Image2XId
				image4xID = asset.ImageIds.Image4XId
			}
		}
	} else {
		// How did you get here? You're not supposed to be here
		return nil, mk.NewClientError(
			twirp.InvalidArgumentError("asset_type", "invalid asset type"),
			mk.ErrCodeInvalidAssetType)
	}

	groupID := ""
	if len(r.GetGroupIds()) > 0 {
		groupID = r.GetGroupIds()[0]
	}

	newEmote, err := ce.Clients.EmoticonsManager.Create(ctx, &em.CreateParams{
		UserID:             r.GetUserId(),
		CodeSuffix:         r.GetCodeSuffix(),
		State:              convertTwirpStateToClientState(r.GetState()),
		GroupID:            groupID,
		Image1xID:          image1xID,
		Image2xID:          image2xID,
		Image4xID:          image4xID,
		Image1xAnimatedID:  image1xAnimatedID,
		Image2xAnimatedID:  image2xAnimatedID,
		Image4xAnimatedID:  image4xAnimatedID,
		Domain:             mako.EmotesDomain,
		EmoteOrigin:        r.GetOrigin(),
		IsAnimated:         isAnimated,
		ShouldIgnorePrefix: r.GetShouldIgnorePrefix(),
		// An empty groupID and an origin of "Archive" indicate that this emote is destined to be archived
		ShouldBeArchived:          groupID == "" && r.GetOrigin() == mkd.EmoteOrigin_Archive,
		AnimatedEmoteTemplate:     convertTwirpAnimationTemplateToClientTemplate(r.AnimatedEmoteTemplate),
		EnforceModerationWorkflow: r.GetEnforceModerationWorkflow(),
		ImageSource:               convertImageSourceToClientImageSource(r.GetImageSource()),
	})

	if err != nil {
		createError := convertError(err)
		if createError != nil {
			log.WithField("request", r.String()).WithError(err).Error("Post validation 4xx error for CreateEmote")
			return nil, createError
		}

		log.WithField("request", r.String()).WithError(err).Error("Error creating emote")
		return nil, twirp.InternalError("Error creating emote").
			WithMeta("request", r.String()).
			WithMeta("error", err.Error())
	}

	if mako.ShouldModify(r.Origin, r.AssetType) {
		go func() {
			start := time.Now()
			if _, err := ce.Clients.PaintClient.ModifyEmote(context.Background(), &paintrpc.ModifyEmoteRequest{
				EmoteId:       newEmote.ID,
				Modifications: paint.DefaultModifications,
			}); err != nil {
				// We only want to log the failure. The CreateEmote call should still return successfully.
				log.WithFields(log.Fields{
					"emote_id": newEmote.ID,
				}).WithError(err).Error("failed to modify emote")
			}
			ce.Stats.TimingDuration("create_emoticon.modify", time.Since(start), 1.0)
		}()
	}

	twirpEmote := newEmote.ToDashboardTwirp()

	var twirpEmoteGroupIDs []string
	if twirpEmote.GroupId != "" {
		twirpEmoteGroupIDs = append(twirpEmoteGroupIDs, twirpEmote.GroupId)
	}

	return &mkd.CreateEmoteResponse{
		Id:        twirpEmote.Id,
		Code:      twirpEmote.Code,
		GroupIds:  twirpEmoteGroupIDs,
		State:     twirpEmote.State,
		CreatedAt: twirpEmote.CreatedAt,
		Emote:     &twirpEmote,
	}, nil
}

func (ce *CreateEmoteAPI) CreateFollowerEmote(ctx context.Context, r *mkd.CreateFollowerEmoteRequest) (*mkd.CreateFollowerEmoteResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()

	if r.GetUserId() == "" {
		return nil, twirp.RequiredArgumentError("user_id")
	}

	groupID, err := ce.Clients.EmoteGroupDB.GetFollowerEmoteGroup(ctx, r.GetUserId())
	if groupID == nil || err != nil {
		return nil, twirp.NewError(twirp.PermissionDenied, "you are not allowed to create follower emotes")
	}

	resp, err := ce.CreateEmote(ctx, &mkd.CreateEmoteRequest{
		UserId:                    r.GetUserId(),
		CodeSuffix:                r.GetCodeSuffix(),
		GroupIds:                  []string{groupID.GroupID},
		ImageAssets:               r.GetImageAssets(),
		AssetType:                 r.GetAssetType(),
		Origin:                    mkd.EmoteOrigin_Follower,
		State:                     r.GetState(),
		EnforceModerationWorkflow: r.GetEnforceModerationWorkflow(),
		// Follower Emotes do not support animation yet
		AnimatedEmoteTemplate: 0,
		// Follower Emotes are channel-specific emotes, so prefix should never be ignored
		ShouldIgnorePrefix: false,
		ImageSource:        r.GetImageSource(),
	})
	if err != nil {
		return nil, err
	}

	return &mkd.CreateFollowerEmoteResponse{Emote: resp.GetEmote()}, nil
}

func convertTwirpStateToClientState(s mkd.EmoteState) mako.EmoteState {
	switch s {
	case mkd.EmoteState_active:
		return mako.Active
	case mkd.EmoteState_archived:
		return mako.Archived
	case mkd.EmoteState_inactive:
		return mako.Inactive
	case mkd.EmoteState_moderated:
		return mako.Moderated
	case mkd.EmoteState_pending:
		return mako.Pending
	case mkd.EmoteState_pending_archived:
		return mako.PendingArchived
	case mkd.EmoteState_unknown:
		fallthrough
	default:
		return mako.Unknown
	}
}

func convertTwirpAnimationTemplateToClientTemplate(s mkd.AnimatedEmoteTemplate) mako.AnimatedEmoteTemplate {
	switch s {
	case mkd.AnimatedEmoteTemplate_NO_TEMPLATE:
		return mako.NoTemplate
	case mkd.AnimatedEmoteTemplate_SHAKE:
		return mako.Shake
	case mkd.AnimatedEmoteTemplate_ROLL:
		return mako.Roll
	case mkd.AnimatedEmoteTemplate_SPIN:
		return mako.Spin
	case mkd.AnimatedEmoteTemplate_RAVE:
		return mako.Rave
	case mkd.AnimatedEmoteTemplate_SLIDEIN:
		return mako.SlideIn
	case mkd.AnimatedEmoteTemplate_SLIDEOUT:
		return mako.SlideOut
	default:
		return mako.NoTemplate
	}
}

func convertImageSourceToClientImageSource(s mkd.ImageSource) mako.ImageSource {
	switch s {
	case mkd.ImageSource_DefaultLibraryImage:
		return mako.DefaultLibraryImage
	default:
		return mako.UserImage
	}
}

func validateCreateEmoteRequest(req *mkd.CreateEmoteRequest) error {
	if req.GetUserId() == "" {
		return mk.NewClientError(
			twirp.RequiredArgumentError("user_id"),
			mk.ErrCodeMissingRequiredArgument)
	}

	if req.GetCodeSuffix() == "" {
		return mk.NewClientError(
			twirp.RequiredArgumentError("code_suffix"),
			mk.ErrCodeMissingRequiredArgument)
	}

	if req.GetOrigin() == mkd.EmoteOrigin_Archive {
		if len(req.GetGroupIds()) > 0 {
			return mk.NewClientError(
				twirp.InvalidArgumentError("group_ids", "must be empty when creating an emote with Archive origin"),
				mk.ErrCodeGroupsNotEmpty)
		}
	} else {
		if len(req.GetGroupIds()) < 1 {
			return mk.NewClientError(
				twirp.RequiredArgumentError("group_ids"),
				mk.ErrCodeMissingRequiredArgument)
		}

		for _, groupID := range req.GetGroupIds() {
			if groupID == "" {
				return mk.NewClientError(
					twirp.InvalidArgumentError("group_ids", "cannot have an empty groupId"),
					mk.ErrCodeEmptyGroupID)
			}
		}
	}

	if req.GetAssetType() == mkd.EmoteAssetType_animated && req.GetImageSource() == mkd.ImageSource_DefaultLibraryImage {
		// There is nothing stopping us specially from using default animated emotes. But we will not have any in the
		// our launch and should do additional work to make sure all our metrics/logic work for animated emotes. If someone
		// has gotten here, its not through our normal flows and should be looked at.
		return mk.NewClientError(
			twirp.InvalidArgumentError("asset_type", "default library emotes cannot be used to create animated emotes"),
			mk.ErrCodeInvalidAssetType)
	}

	// Only animated emotes can use animation templates
	if req.GetAssetType() == mkd.EmoteAssetType_static && req.GetAnimatedEmoteTemplate() != mkd.AnimatedEmoteTemplate_NO_TEMPLATE {
		return mk.NewClientError(
			twirp.InvalidArgumentError("animated_emote_template", "only animated emotes can use animation templates"),
			mk.ErrCodeInvalidAssetType)
	}

	if len(req.GetImageAssets()) < 1 {
		return mk.NewClientError(
			twirp.RequiredArgumentError("image_assets"),
			mk.ErrCodeImageAssetsNotFound)
	}

	if len(req.GetImageAssets()) > 1 && req.GetAssetType() == mkd.EmoteAssetType_static {
		return mk.NewClientError(
			twirp.InvalidArgumentError("image_assets", "static emotes should have only one asset"),
			mk.ErrCodeTooManyImageAssets)
	}

	if len(req.GetImageAssets()) > 2 && req.GetAssetType() == mkd.EmoteAssetType_animated {
		return mk.NewClientError(
			twirp.InvalidArgumentError("image_assets", "animated emotes should have only two assets"),
			mk.ErrCodeTooManyImageAssets)
	}

	if len(req.GetImageAssets()) < 2 && req.GetAssetType() == mkd.EmoteAssetType_animated {
		return mk.NewClientError(
			twirp.InvalidArgumentError("image_assets", "animated emotes should have two assets"),
			mk.ErrCodeNotEnoughImageAssets)
	}

	if req.GetAssetType() == mkd.EmoteAssetType_static && req.GetImageAssets()[0].AssetType != mkd.EmoteAssetType_static {
		return mk.NewClientError(
			twirp.InvalidArgumentError("image_assets.asset_type", "static emotes require a static asset"),
			mk.ErrMissingStaticAsset)
	}

	if req.GetAssetType() == mkd.EmoteAssetType_animated {
		hasStatic := false
		hasAnimated := false
		for _, asset := range req.GetImageAssets() {
			if asset.AssetType == mkd.EmoteAssetType_static {
				hasStatic = true
			} else if asset.AssetType == mkd.EmoteAssetType_animated {
				hasAnimated = true
			}
		}
		if !hasStatic {
			return mk.NewClientError(
				twirp.InvalidArgumentError("image_assets.asset_type", "animated emotes require a static asset"),
				mk.ErrMissingStaticAsset)
		}

		if !hasAnimated {
			return mk.NewClientError(
				twirp.InvalidArgumentError("image_assets.asset_type", "animated emotes require an animated asset"),
				mk.ErrMissingAnimatedAsset)
		}
	}

	// validate that each image asset has non-empty ids
	for idx, asset := range req.GetImageAssets() {
		if asset.ImageIds.Image1XId == "" {
			return mk.NewClientError(
				twirp.RequiredArgumentError(fmt.Sprintf("image_assets[%d].image_ids.image1x_id", idx)),
				mk.ErrCodeImage1XIDNotFound)
		}

		if asset.ImageIds.Image2XId == "" {
			return mk.NewClientError(
				twirp.RequiredArgumentError(fmt.Sprintf("image_assets[%d].image_ids.image2x_id", idx)),
				mk.ErrCodeImage2XIDNotFound)
		}

		if asset.ImageIds.Image4XId == "" {
			return mk.NewClientError(
				twirp.RequiredArgumentError(fmt.Sprintf("image_assets[%d].image_ids.image4x_id", idx)),
				mk.ErrCodeImage4XIDNotFound)
		}
	}

	if req.GetState() == mkd.EmoteState_unknown && !req.GetEnforceModerationWorkflow() {
		return mk.NewClientError(
			twirp.InvalidArgumentError("state", "cannot use unknown state unless enforce_moderation_flow is true"),
			mk.ErrCodeInvalidState)
	} else if (req.GetState() == mkd.EmoteState_archived || req.GetState() == mkd.EmoteState_pending_archived) && req.GetOrigin() != mkd.EmoteOrigin_Archive {
		return mk.NewClientError(
			twirp.InvalidArgumentError("state", "cannot use archived or pending_archived state unless origin is Archive"),
			mk.ErrCodeInvalidState)
	}

	return nil
}

func convertError(createError error) error {
	if createError == em.EmoticonCodeNotUnique {
		return mk.NewClientError(
			twirp.InvalidArgumentError("code", "must be unique"),
			mk.ErrCodeCodeNotUnique)
	}

	if createError == em.EmoticonCodeUnacceptable {
		return mk.NewClientError(
			twirp.InvalidArgumentError("code", "is unacceptable"),
			mk.ErrCodeCodeUnacceptable)
	}

	if createError == em.InvalidCodeSuffixFormat {
		return mk.NewClientError(
			twirp.InvalidArgumentError("code_suffix", "invalid format"),
			mk.ErrCodeInvalidSuffixFormat)
	}

	if createError == em.UserOwnedEmoteLimitReached {
		return mk.NewClientError(
			twirp.NewError(twirp.FailedPrecondition, "user cannot own any more emotes of this type"),
			mk.ErrCodeUserOwnedEmoteLimitReached)
	}

	if createError == em.InvalidImageUpload {
		return mk.NewClientError(
			twirp.InvalidArgumentError("image", "invalid image uploaded"),
			mk.ErrCodeInvalidImageUpload)
	}

	if createError == em.Image1XIDNotFound {
		return mk.NewClientError(
			twirp.InvalidArgumentError("image1x_id", "not found"),
			mk.ErrCodeImage1XIDNotFound)
	}

	if createError == em.Image2XIDNotFound {
		return mk.NewClientError(
			twirp.InvalidArgumentError("image2x_id", "not found"),
			mk.ErrCodeImage2XIDNotFound)
	}

	if createError == em.Image4XIDNotFound {
		return mk.NewClientError(
			twirp.InvalidArgumentError("image4x_id", "not found"),
			mk.ErrCodeImage4XIDNotFound)
	}

	if createError == em.UserNotPermitted {
		return mk.NewClientError(
			twirp.NewError(twirp.PermissionDenied, "user is not permitted to perform this action"),
			mk.ErrCodeUserNotPermitted)
	}

	return nil
}
