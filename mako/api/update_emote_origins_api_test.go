package api

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/mako/clients/emoticonsmanager"

	mako "code.justin.tv/commerce/mako/internal"

	"github.com/stretchr/testify/mock"

	emoticonsmanager_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/emoticonsmanager"
	mkd "code.justin.tv/commerce/mako/twirp/dashboard"
	. "github.com/smartystreets/goconvey/convey"
)

func TestUpdateEmoteOriginsAPI(t *testing.T) {
	Convey("Test EmoteOrdersAPI", t, func() {
		mockEmoticon := new(emoticonsmanager_mock.IEmoticonsManager)
		updateEmoteOriginsAPI := &UpdateEmoteOriginsAPI{
			EmoticonsManager: mockEmoticon,
		}

		Convey("Test UpdateEmoteOrigins", func() {
			Convey("With empty origins", func() {
				req := &mkd.UpdateEmoteOriginsRequest{
					OriginUpdates: nil,
				}

				_, err := updateEmoteOriginsAPI.UpdateEmoteOrigins(context.Background(), req)
				So(err, ShouldNotBeNil)
			})

			Convey("With ambiguous origins for an emote", func() {
				req := &mkd.UpdateEmoteOriginsRequest{
					OriginUpdates: []*mkd.OriginUpdate{
						{EmoteId: "emote1", Origin: mkd.EmoteOrigin_MegaCommerce},
						{EmoteId: "emote1", Origin: mkd.EmoteOrigin_Rewards},
					},
				}

				_, err := updateEmoteOriginsAPI.UpdateEmoteOrigins(context.Background(), req)
				So(err, ShouldNotBeNil)
			})

			Convey("With a blank emoteID", func() {
				req := &mkd.UpdateEmoteOriginsRequest{
					OriginUpdates: []*mkd.OriginUpdate{
						{EmoteId: "", Origin: mkd.EmoteOrigin_MegaCommerce},
					},
				}

				_, err := updateEmoteOriginsAPI.UpdateEmoteOrigins(context.Background(), req)
				So(err, ShouldNotBeNil)
			})

			Convey("With valid input", func() {
				requestedOrigin := []*mkd.OriginUpdate{
					{EmoteId: "emote1", Origin: mkd.EmoteOrigin_MegaCommerce},
				}

				req := &mkd.UpdateEmoteOriginsRequest{
					OriginUpdates: requestedOrigin,
				}

				Convey("when emoticonmanager fails", func() {
					mockEmoticon.On("UpdateEmoteOrigins", mock.Anything, mock.Anything).Return(nil, errors.New("some_error"))

					resp, err := updateEmoteOriginsAPI.UpdateEmoteOrigins(context.Background(), req)
					So(err, ShouldNotBeNil)
					So(resp, ShouldBeNil)
				})

				Convey("when the emote doesn't exist", func() {
					mockEmoticon.On("UpdateEmoteOrigins", mock.Anything, mock.Anything).Return(nil, emoticonsmanager.EmoteDoesNotExist)

					resp, err := updateEmoteOriginsAPI.UpdateEmoteOrigins(context.Background(), req)
					So(err, ShouldNotBeNil)
					So(err.Error(), ShouldContainSubstring, "one or more emotes could not be found")
					So(resp, ShouldBeNil)
				})

				Convey("when emoticonmanager succeeds", func() {
					mockEmoticon.On("UpdateEmoteOrigins", mock.Anything, mock.Anything).Return([]mako.Emote{
						{ID: "emote1", EmotesGroup: mkd.EmoteOrigin_MegaCommerce.String()},
						{ID: "emote2", EmotesGroup: mkd.EmoteOrigin_Subscriptions.String()},
						{ID: "emote3", EmotesGroup: mkd.EmoteOrigin_HypeTrain.String()},
					}, nil)

					expectedEmotes := []*mkd.Emote{
						{Id: "emote1", Origin: mkd.EmoteOrigin_MegaCommerce},
						{Id: "emote2", Origin: mkd.EmoteOrigin_Subscriptions},
						{Id: "emote3", Origin: mkd.EmoteOrigin_HypeTrain},
					}

					resp, err := updateEmoteOriginsAPI.UpdateEmoteOrigins(context.Background(), req)
					So(err, ShouldBeNil)
					So(resp, ShouldNotBeNil)
					So(len(resp.Emotes), ShouldEqual, 3)
					for i, emote := range resp.Emotes {
						So(emote.Id, ShouldEqual, expectedEmotes[i].Id)
						So(emote.Origin, ShouldEqual, expectedEmotes[i].Origin)
					}
				})
			})
		})
	})
}
