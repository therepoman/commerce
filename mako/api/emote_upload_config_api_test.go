package api

import (
	"context"
	"fmt"
	"testing"

	"code.justin.tv/commerce/mako/clients/imageuploader"
	"code.justin.tv/commerce/mako/config"
	mako "code.justin.tv/commerce/mako/internal"
	imageuploader_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/imageuploader"
	internal_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/internal_mock"
	mk "code.justin.tv/commerce/mako/twirp/dashboard"
	"code.justin.tv/web/upload-service/rpc/uploader"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
	"github.com/twitchtv/twirp"
)

func TestGetEmoteUploadConfigAPI_GetEmoteUploadConfig(t *testing.T) {
	Convey("Given an GetEmoticonUploadConfigurationAPI", t, func() {
		mockImageUploader := new(imageuploader_mock.IImageUploader)
		mockMetadataCache := new(internal_mock.EmoteUploadMetadataCache)
		uploadEmoticonAPI := getEmoteUploadConfigAPI{
			Config: &config.Configuration{
				UploadS3Host:       "upload_s3",
				UploadS3BucketName: "upload_s3_bucket_name",
			},
			Uploader:      mockImageUploader,
			MetadataCache: mockMetadataCache,
		}

		ctx := context.Background()
		req := mk.GetEmoteUploadConfigRequest{}

		Convey("when the request is populated and is auto_resize", func() {
			req.UserId = "46024993"
			req.ResizePlan = mk.ResizePlan_auto_resize

			Convey("when the uploader returns an error", func() {
				animeBearErr := errors.New("WHEN WILL BONTA STOP WITH THE ANIME BEARS...PLS")
				mockImageUploader.On("CreateEmoteUploadRequest", mock.Anything, mock.Anything).Return(nil, animeBearErr)

				resp, err := uploadEmoticonAPI.GetEmoteUploadConfig(ctx, &req)
				So(resp, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})

			Convey("when the uploader works", func() {
				Convey("when the emote is static", func() {
					uploadID := "upload-super-cool-edition"
					uploadURL := "https://s3.mako-aws.amazon.com/super-cool-files-go-here"
					expectedCacheKey := fmt.Sprintf("%s_4x_%s", req.UserId, uploadID)

					mockImageUploader.On("CreateEmoteUploadRequest", mock.Anything, mock.Anything).Return(&uploader.UploadResponse{
						UploadId: uploadID,
						Url:      uploadURL,
					}, nil)
					mockMetadataCache.On("StoreUploadMethod", mock.Anything, mock.Anything, mock.Anything).Return(nil)

					resp, err := uploadEmoticonAPI.GetEmoteUploadConfig(ctx, &req)
					So(err, ShouldBeNil)
					So(resp, ShouldNotBeNil)

					So(resp.GetUploadConfigs(), ShouldHaveLength, 1)
					So(resp.GetUploadConfigs()[0].GetId(), ShouldEqual, uploadID)
					So(resp.GetUploadConfigs()[0].GetUrl(), ShouldEqual, uploadURL)
					So(resp.GetUploadConfigs()[0].GetSize(), ShouldEqual, mk.EmoteImageSize_size_original)
					So(resp.GetUploadConfigs()[0].GetAssetType(), ShouldEqual, mk.EmoteAssetType_static)

					So(resp.GetUploadConfigs()[0].GetImages(), ShouldHaveLength, 3)
					found1x, found2x, found4x := checkImagesAndReturnPresence(resp.GetUploadConfigs()[0].GetImages(), req.UserId, uploadID, mk.EmoteAssetType_static)
					So(found1x, ShouldBeTrue)
					So(found2x, ShouldBeTrue)
					So(found4x, ShouldBeTrue)

					mockMetadataCache.AssertCalled(t, "StoreUploadMethod", mock.Anything, expectedCacheKey, mako.EmoteUploadMethodSimple)
					mockMetadataCache.AssertNumberOfCalls(t, "StoreUploadMethod", 1)
				})

				Convey("when the emote is animated and the static assets are NOT to be generated", func() {
					uploadID := "upload-super-cool-edition"
					uploadURL := "https://s3.mako-aws.amazon.com/super-cool-files-go-here"
					expectedCacheKey := fmt.Sprintf("%s_4x_%s", req.UserId, uploadID)

					mockImageUploader.On("CreateEmoteUploadRequest", mock.Anything, mock.Anything).Return(&uploader.UploadResponse{
						UploadId: uploadID,
						Url:      uploadURL,
					}, nil)
					mockMetadataCache.On("StoreUploadMethod", mock.Anything, mock.Anything, mock.Anything).Return(nil)

					req.AssetType = mk.EmoteAssetType_animated

					resp, err := uploadEmoticonAPI.GetEmoteUploadConfig(ctx, &req)
					So(err, ShouldBeNil)
					So(resp, ShouldNotBeNil)

					So(resp.GetUploadConfigs(), ShouldHaveLength, 2) // one for the animated asset, one for the corresponding static asset

					So(resp.GetUploadConfigs()[0].GetId(), ShouldEqual, uploadID)
					So(resp.GetUploadConfigs()[0].GetUrl(), ShouldEqual, uploadURL)
					So(resp.GetUploadConfigs()[0].GetSize(), ShouldEqual, mk.EmoteImageSize_size_original)
					So(resp.GetUploadConfigs()[0].GetAssetType(), ShouldEqual, mk.EmoteAssetType_animated)

					So(resp.GetUploadConfigs()[1].GetId(), ShouldEqual, uploadID)
					So(resp.GetUploadConfigs()[1].GetUrl(), ShouldEqual, uploadURL)
					So(resp.GetUploadConfigs()[1].GetSize(), ShouldEqual, mk.EmoteImageSize_size_original)
					So(resp.GetUploadConfigs()[1].GetAssetType(), ShouldEqual, mk.EmoteAssetType_static)

					So(resp.GetUploadConfigs()[0].GetImages(), ShouldHaveLength, 3)
					So(resp.GetUploadConfigs()[1].GetImages(), ShouldHaveLength, 3)

					foundAnimated1x, foundAnimated2x, foundAnimated4x := checkImagesAndReturnPresence(resp.GetUploadConfigs()[0].GetImages(), req.UserId, uploadID, mk.EmoteAssetType_animated)
					So(foundAnimated1x, ShouldBeTrue)
					So(foundAnimated2x, ShouldBeTrue)
					So(foundAnimated4x, ShouldBeTrue)

					foundStatic1x, foundStatic2x, foundStatic4x := checkImagesAndReturnPresence(resp.GetUploadConfigs()[1].GetImages(), req.UserId, uploadID, mk.EmoteAssetType_static)
					So(foundStatic1x, ShouldBeTrue)
					So(foundStatic2x, ShouldBeTrue)
					So(foundStatic4x, ShouldBeTrue)

					mockMetadataCache.AssertCalled(t, "StoreUploadMethod", mock.Anything, expectedCacheKey, mako.EmoteUploadMethodSimple)
					mockMetadataCache.AssertNumberOfCalls(t, "StoreUploadMethod", 1)
				})

				Convey("when the emote is animated and the static assets are to be generated", func() {
					uploadID := "upload-super-cool-edition"
					uploadURL := "https://s3.mako-aws.amazon.com/super-cool-files-go-here"
					expectedCacheKey := fmt.Sprintf("%s_4x_%s", req.UserId, uploadID)

					mockImageUploader.On("CreateEmoteUploadRequest", mock.Anything, mock.Anything).Return(&uploader.UploadResponse{
						UploadId: uploadID,
						Url:      uploadURL,
					}, nil)
					mockMetadataCache.On("StoreUploadMethod", mock.Anything, mock.Anything, mock.Anything).Return(nil)

					req.AssetType = mk.EmoteAssetType_animated
					req.GenerateStaticVersionOfAnimatedAssets = true

					resp, err := uploadEmoticonAPI.GetEmoteUploadConfig(ctx, &req)
					So(err, ShouldBeNil)
					So(resp, ShouldNotBeNil)

					So(resp.GetUploadConfigs(), ShouldHaveLength, 1) // the static assets are being generated as part of the same upload config

					So(resp.GetUploadConfigs()[0].GetId(), ShouldEqual, uploadID)
					So(resp.GetUploadConfigs()[0].GetUrl(), ShouldEqual, uploadURL)
					So(resp.GetUploadConfigs()[0].GetSize(), ShouldEqual, mk.EmoteImageSize_size_original)
					So(resp.GetUploadConfigs()[0].GetAssetType(), ShouldEqual, mk.EmoteAssetType_animated)

					So(resp.GetUploadConfigs()[0].GetImages(), ShouldHaveLength, 6)

					foundAnimated1x, foundAnimated2x, foundAnimated4x := checkImagesAndReturnPresence(resp.GetUploadConfigs()[0].GetImages(), req.UserId, uploadID, mk.EmoteAssetType_animated)
					So(foundAnimated1x, ShouldBeTrue)
					So(foundAnimated2x, ShouldBeTrue)
					So(foundAnimated4x, ShouldBeTrue)

					foundStatic1x, foundStatic2x, foundStatic4x := checkImagesAndReturnPresence(resp.GetUploadConfigs()[0].GetImages(), req.UserId, uploadID, mk.EmoteAssetType_static)
					So(foundStatic1x, ShouldBeTrue)
					So(foundStatic2x, ShouldBeTrue)
					So(foundStatic4x, ShouldBeTrue)

					mockMetadataCache.AssertCalled(t, "StoreUploadMethod", mock.Anything, expectedCacheKey, mako.EmoteUploadMethodSimple)
					mockMetadataCache.AssertNumberOfCalls(t, "StoreUploadMethod", 1)
				})
			})
		})

		Convey("when the request is populated with a single size and is no_resize", func() {
			req.UserId = "46024993"
			req.ResizePlan = mk.ResizePlan_no_resize
			req.Sizes = []mk.EmoteImageSize{mk.EmoteImageSize_size_2x}

			Convey("when the uploader works", func() {
				Convey("when the emote is static", func() {
					uploadID := "upload-ok-edition"
					uploadURL := "https://s3.mako-aws.amazon.com/super-ok-files-go-here"

					mockImageUploader.On("CreateEmoteUploadRequest", mock.Anything, mock.Anything).Return(&uploader.UploadResponse{
						UploadId: uploadID,
						Url:      uploadURL,
					}, nil)

					resp, err := uploadEmoticonAPI.GetEmoteUploadConfig(ctx, &req)
					So(err, ShouldBeNil)
					So(resp, ShouldNotBeNil)

					So(resp.GetUploadConfigs(), ShouldHaveLength, 1)
					So(resp.GetUploadConfigs()[0].GetId(), ShouldEqual, uploadID)
					So(resp.GetUploadConfigs()[0].GetUrl(), ShouldEqual, uploadURL)
					So(resp.GetUploadConfigs()[0].GetSize(), ShouldEqual, mk.EmoteImageSize_size_2x)
					So(resp.GetUploadConfigs()[0].GetAssetType(), ShouldEqual, mk.EmoteAssetType_static)

					So(resp.GetUploadConfigs()[0].GetImages(), ShouldHaveLength, 1)
					found1x, found2x, found4x := checkImagesAndReturnPresence(resp.GetUploadConfigs()[0].GetImages(), req.UserId, uploadID, mk.EmoteAssetType_static)
					So(found1x, ShouldBeFalse)
					So(found2x, ShouldBeTrue)
					So(found4x, ShouldBeFalse)
				})

				Convey("when the emote is animated and the static assets are NOT to be generated", func() {
					uploadID := "upload-ok-edition"
					uploadURL := "https://s3.mako-aws.amazon.com/super-ok-files-go-here"

					mockImageUploader.On("CreateEmoteUploadRequest", mock.Anything, mock.Anything).Return(&uploader.UploadResponse{
						UploadId: uploadID,
						Url:      uploadURL,
					}, nil)

					req.AssetType = mk.EmoteAssetType_animated

					resp, err := uploadEmoticonAPI.GetEmoteUploadConfig(ctx, &req)
					So(err, ShouldBeNil)
					So(resp, ShouldNotBeNil)

					So(resp.GetUploadConfigs(), ShouldHaveLength, 2) // one for the animated asset, one for the corresponding static asset

					So(resp.GetUploadConfigs()[0].GetId(), ShouldEqual, uploadID)
					So(resp.GetUploadConfigs()[0].GetUrl(), ShouldEqual, uploadURL)
					So(resp.GetUploadConfigs()[0].GetSize(), ShouldEqual, mk.EmoteImageSize_size_2x)
					So(resp.GetUploadConfigs()[0].GetAssetType(), ShouldEqual, mk.EmoteAssetType_animated)

					So(resp.GetUploadConfigs()[1].GetId(), ShouldEqual, uploadID)
					So(resp.GetUploadConfigs()[1].GetUrl(), ShouldEqual, uploadURL)
					So(resp.GetUploadConfigs()[1].GetSize(), ShouldEqual, mk.EmoteImageSize_size_2x)
					So(resp.GetUploadConfigs()[1].GetAssetType(), ShouldEqual, mk.EmoteAssetType_static)

					So(resp.GetUploadConfigs()[0].GetImages(), ShouldHaveLength, 1)
					So(resp.GetUploadConfigs()[1].GetImages(), ShouldHaveLength, 1)

					foundAnimated1x, foundAnimated2x, foundAnimated4x := checkImagesAndReturnPresence(resp.GetUploadConfigs()[0].GetImages(), req.UserId, uploadID, mk.EmoteAssetType_animated)
					So(foundAnimated1x, ShouldBeFalse)
					So(foundAnimated2x, ShouldBeTrue)
					So(foundAnimated4x, ShouldBeFalse)

					foundStatic1x, foundStatic2x, foundStatic4x := checkImagesAndReturnPresence(resp.GetUploadConfigs()[1].GetImages(), req.UserId, uploadID, mk.EmoteAssetType_static)
					So(foundStatic1x, ShouldBeFalse)
					So(foundStatic2x, ShouldBeTrue)
					So(foundStatic4x, ShouldBeFalse)
				})

				Convey("when the emote is animated and the static assets are to be generated", func() {
					uploadID := "upload-ok-edition"
					uploadURL := "https://s3.mako-aws.amazon.com/super-ok-files-go-here"

					mockImageUploader.On("CreateEmoteUploadRequest", mock.Anything, mock.Anything).Return(&uploader.UploadResponse{
						UploadId: uploadID,
						Url:      uploadURL,
					}, nil)

					req.AssetType = mk.EmoteAssetType_animated
					req.GenerateStaticVersionOfAnimatedAssets = true

					resp, err := uploadEmoticonAPI.GetEmoteUploadConfig(ctx, &req)
					So(err, ShouldBeNil)
					So(resp, ShouldNotBeNil)

					So(resp.GetUploadConfigs(), ShouldHaveLength, 1) // the static assets are being generated as part of the same upload config

					So(resp.GetUploadConfigs()[0].GetId(), ShouldEqual, uploadID)
					So(resp.GetUploadConfigs()[0].GetUrl(), ShouldEqual, uploadURL)
					So(resp.GetUploadConfigs()[0].GetSize(), ShouldEqual, mk.EmoteImageSize_size_2x)
					So(resp.GetUploadConfigs()[0].GetAssetType(), ShouldEqual, mk.EmoteAssetType_animated)

					So(resp.GetUploadConfigs()[0].GetImages(), ShouldHaveLength, 2)

					foundAnimated1x, foundAnimated2x, foundAnimated4x := checkImagesAndReturnPresence(resp.GetUploadConfigs()[0].GetImages(), req.UserId, uploadID, mk.EmoteAssetType_animated)
					So(foundAnimated1x, ShouldBeFalse)
					So(foundAnimated2x, ShouldBeTrue)
					So(foundAnimated4x, ShouldBeFalse)

					foundStatic1x, foundStatic2x, foundStatic4x := checkImagesAndReturnPresence(resp.GetUploadConfigs()[0].GetImages(), req.UserId, uploadID, mk.EmoteAssetType_static)
					So(foundStatic1x, ShouldBeFalse)
					So(foundStatic2x, ShouldBeTrue)
					So(foundStatic4x, ShouldBeFalse)
				})
			})
		})

		Convey("when the request is populated with all sizes and is no_resize", func() {
			req.UserId = "46024993"
			req.ResizePlan = mk.ResizePlan_no_resize
			req.Sizes = []mk.EmoteImageSize{mk.EmoteImageSize_size_2x, mk.EmoteImageSize_size_4x, mk.EmoteImageSize_size_1x}

			Convey("when the uploader works", func() {
				Convey("when the emote is static", func() {
					uploadID := "upload-amazing-edition"
					uploadURL := "https://s3.mako-aws.amazon.com/amazing-files-go-here"
					expectedCacheKey := fmt.Sprintf("%s_4x_%s", req.UserId, uploadID)

					mockImageUploader.On("CreateEmoteUploadRequest", mock.Anything, mock.Anything).Return(&uploader.UploadResponse{
						UploadId: uploadID,
						Url:      uploadURL,
					}, nil)
					mockMetadataCache.On("StoreUploadMethod", mock.Anything, mock.Anything, mock.Anything).Return(nil)

					resp, err := uploadEmoticonAPI.GetEmoteUploadConfig(ctx, &req)
					So(err, ShouldBeNil)
					So(resp, ShouldNotBeNil)

					So(resp.GetUploadConfigs(), ShouldHaveLength, 3)

					for i, uploadConfig := range resp.GetUploadConfigs() {
						// normally don't validate order, but it should be a safe assumption for now.
						var expectedSize mk.EmoteImageSize
						switch i {
						case 0:
							expectedSize = mk.EmoteImageSize_size_2x
						case 1:
							expectedSize = mk.EmoteImageSize_size_4x
						case 2:
							expectedSize = mk.EmoteImageSize_size_1x
						}

						checkEmoteSize(uploadConfig, expectedSize, uploadID, uploadURL, req.UserId, mk.EmoteAssetType_static)
					}

					mockMetadataCache.AssertCalled(t, "StoreUploadMethod", mock.Anything, expectedCacheKey, mako.EmoteUploadMethodAdvanced)
					mockMetadataCache.AssertNumberOfCalls(t, "StoreUploadMethod", 1)
				})

				Convey("when the emote is animated and the static assets are NOT to be generated", func() {
					uploadID := "upload-amazing-edition"
					uploadURL := "https://s3.mako-aws.amazon.com/amazing-files-go-here"
					expectedCacheKey := fmt.Sprintf("%s_4x_%s", req.UserId, uploadID)

					mockImageUploader.On("CreateEmoteUploadRequest", mock.Anything, mock.Anything).Return(&uploader.UploadResponse{
						UploadId: uploadID,
						Url:      uploadURL,
					}, nil)
					mockMetadataCache.On("StoreUploadMethod", mock.Anything, mock.Anything, mock.Anything).Return(nil)

					req.AssetType = mk.EmoteAssetType_animated

					resp, err := uploadEmoticonAPI.GetEmoteUploadConfig(ctx, &req)
					So(err, ShouldBeNil)
					So(resp, ShouldNotBeNil)

					So(resp.GetUploadConfigs(), ShouldHaveLength, 6) // two for each requested size (one animated, one static)

					for i, uploadConfig := range resp.GetUploadConfigs() {
						// normally don't validate order, but it should be a safe assumption for now.
						var expectedSize mk.EmoteImageSize
						var expectedAssetType mk.EmoteAssetType
						switch i {
						case 0:
							expectedSize = mk.EmoteImageSize_size_2x
							expectedAssetType = mk.EmoteAssetType_animated
						case 1:
							expectedSize = mk.EmoteImageSize_size_2x
							expectedAssetType = mk.EmoteAssetType_static
						case 2:
							expectedSize = mk.EmoteImageSize_size_4x
							expectedAssetType = mk.EmoteAssetType_animated
						case 3:
							expectedSize = mk.EmoteImageSize_size_4x
							expectedAssetType = mk.EmoteAssetType_static
						case 4:
							expectedSize = mk.EmoteImageSize_size_1x
							expectedAssetType = mk.EmoteAssetType_animated
						case 5:
							expectedSize = mk.EmoteImageSize_size_1x
							expectedAssetType = mk.EmoteAssetType_static
						}

						checkEmoteSize(uploadConfig, expectedSize, uploadID, uploadURL, req.UserId, expectedAssetType)
					}

					mockMetadataCache.AssertCalled(t, "StoreUploadMethod", mock.Anything, expectedCacheKey, mako.EmoteUploadMethodAdvanced)
					mockMetadataCache.AssertNumberOfCalls(t, "StoreUploadMethod", 1)
				})

				Convey("when the emote is animated and the static assets are to be generated", func() {
					uploadID := "upload-amazing-edition"
					uploadURL := "https://s3.mako-aws.amazon.com/amazing-files-go-here"
					expectedCacheKey := fmt.Sprintf("%s_4x_%s", req.UserId, uploadID)

					mockImageUploader.On("CreateEmoteUploadRequest", mock.Anything, mock.Anything).Return(&uploader.UploadResponse{
						UploadId: uploadID,
						Url:      uploadURL,
					}, nil)
					mockMetadataCache.On("StoreUploadMethod", mock.Anything, mock.Anything, mock.Anything).Return(nil)

					req.AssetType = mk.EmoteAssetType_animated
					req.GenerateStaticVersionOfAnimatedAssets = true

					resp, err := uploadEmoticonAPI.GetEmoteUploadConfig(ctx, &req)
					So(err, ShouldBeNil)
					So(resp, ShouldNotBeNil)

					So(resp.GetUploadConfigs(), ShouldHaveLength, 3) // the static assets are being generated as part of the same upload config

					for i, uploadConfig := range resp.GetUploadConfigs() {
						// normally don't validate order, but it should be a safe assumption for now.
						var expectedSize mk.EmoteImageSize
						switch i {
						case 0:
							expectedSize = mk.EmoteImageSize_size_2x
						case 1:
							expectedSize = mk.EmoteImageSize_size_4x
						case 2:
							expectedSize = mk.EmoteImageSize_size_1x
						}

						So(uploadConfig.GetId(), ShouldEqual, uploadID)
						So(uploadConfig.GetUrl(), ShouldEqual, uploadURL)
						So(uploadConfig.GetSize(), ShouldEqual, expectedSize)
						So(uploadConfig.GetAssetType(), ShouldEqual, mk.EmoteAssetType_animated)

						So(uploadConfig.GetImages(), ShouldHaveLength, 2) // one animated image, one static image

						foundAnimated1x, foundAnimated2x, foundAnimated4x := checkImagesAndReturnPresence(uploadConfig.GetImages(), req.UserId, uploadID, mk.EmoteAssetType_animated)
						switch expectedSize {
						case mk.EmoteImageSize_size_1x:
							So(foundAnimated1x, ShouldBeTrue)
							So(foundAnimated2x, ShouldBeFalse)
							So(foundAnimated4x, ShouldBeFalse)
						case mk.EmoteImageSize_size_2x:
							So(foundAnimated1x, ShouldBeFalse)
							So(foundAnimated2x, ShouldBeTrue)
							So(foundAnimated4x, ShouldBeFalse)
						case mk.EmoteImageSize_size_4x:
							So(foundAnimated1x, ShouldBeFalse)
							So(foundAnimated2x, ShouldBeFalse)
							So(foundAnimated4x, ShouldBeTrue)
						}

						foundStatic1x, foundStatic2x, foundStatic4x := checkImagesAndReturnPresence(uploadConfig.GetImages(), req.UserId, uploadID, mk.EmoteAssetType_static)
						switch expectedSize {
						case mk.EmoteImageSize_size_1x:
							So(foundStatic1x, ShouldBeTrue)
							So(foundStatic2x, ShouldBeFalse)
							So(foundStatic4x, ShouldBeFalse)
						case mk.EmoteImageSize_size_2x:
							So(foundStatic1x, ShouldBeFalse)
							So(foundStatic2x, ShouldBeTrue)
							So(foundStatic4x, ShouldBeFalse)
						case mk.EmoteImageSize_size_4x:
							So(foundStatic1x, ShouldBeFalse)
							So(foundStatic2x, ShouldBeFalse)
							So(foundStatic4x, ShouldBeTrue)
						}
					}

					mockMetadataCache.AssertCalled(t, "StoreUploadMethod", mock.Anything, expectedCacheKey, mako.EmoteUploadMethodAdvanced)
					mockMetadataCache.AssertNumberOfCalls(t, "StoreUploadMethod", 1)
				})
			})
		})
	})
}

func checkEmoteSize(uploadConfig *mk.EmoteUploadConfig, expectedSize mk.EmoteImageSize, uploadID, uploadURL, userID string, expectedAssetType mk.EmoteAssetType) {
	So(uploadConfig.GetId(), ShouldEqual, uploadID)
	So(uploadConfig.GetUrl(), ShouldEqual, uploadURL)
	So(uploadConfig.GetSize(), ShouldEqual, expectedSize)
	So(uploadConfig.GetAssetType(), ShouldEqual, expectedAssetType)

	So(uploadConfig.GetImages(), ShouldHaveLength, 1)
	found1x, found2x, found4x := checkImagesAndReturnPresence(uploadConfig.GetImages(), userID, uploadID, expectedAssetType)

	switch expectedSize {
	case mk.EmoteImageSize_size_1x:
		So(found1x, ShouldBeTrue)
		So(found2x, ShouldBeFalse)
		So(found4x, ShouldBeFalse)
	case mk.EmoteImageSize_size_2x:
		So(found1x, ShouldBeFalse)
		So(found2x, ShouldBeTrue)
		So(found4x, ShouldBeFalse)
	case mk.EmoteImageSize_size_4x:
		So(found1x, ShouldBeFalse)
		So(found2x, ShouldBeFalse)
		So(found4x, ShouldBeTrue)
	}
}

// Checks which sizes are present of a particular asset type and checks that the image data is valid
func checkImagesAndReturnPresence(images []*mk.EmoteImage, userID, uploadID string, assetType mk.EmoteAssetType) (bool, bool, bool) {
	format := imageuploader.ImageIDStaticFormat
	if assetType == mk.EmoteAssetType_animated {
		format = imageuploader.ImageIDAnimatedFormat
	}

	var found1x, found2x, found4x bool
	for _, image := range images {
		if image.AssetType != assetType {
			// We're checking specific asset types, so skip over the irrelevant ones
			continue
		}

		imageID := fmt.Sprintf(format, userID, imageuploader.StringifySize(image.Size), uploadID)
		imageURL := fmt.Sprintf(imageuploader.ImageS3URLFormat, "upload_s3", "upload_s3_bucket_name", imageID)
		So(imageID, ShouldEqual, image.GetId())
		So(imageURL, ShouldEqual, image.GetUrl())

		switch image.Size {
		case mk.EmoteImageSize_size_1x:
			found1x = true
		case mk.EmoteImageSize_size_2x:
			found2x = true
		case mk.EmoteImageSize_size_4x:
			found4x = true
		}
	}
	return found1x, found2x, found4x
}

func TestGetEmoteUploadConfigAPI_validateGetEmoteUploadConfigRequest(t *testing.T) {
	Convey("with nothing", t, func() {
		Convey("when the request is nil", func() {
			var req *mk.GetEmoteUploadConfigRequest

			err := validateTwirpError(validateGetEmoteUploadConfigRequest(req), twirp.InvalidArgument)
			So(err.Msg(), ShouldEqual, "request is required")
			So(err.Meta("argument"), ShouldEqual, "request")
		})

		Convey("when the request has an empty user_id", func() {
			req := &mk.GetEmoteUploadConfigRequest{}

			err := validateTwirpError(validateGetEmoteUploadConfigRequest(req), twirp.InvalidArgument)
			So(err.Msg(), ShouldEqual, "user_id is required")
			So(err.Meta("argument"), ShouldEqual, "user_id")
		})

		Convey("when the request is no-resize", func() {
			req := &mk.GetEmoteUploadConfigRequest{
				UserId:     "265716088",
				ResizePlan: mk.ResizePlan_no_resize,
			}

			err := validateTwirpError(validateGetEmoteUploadConfigRequest(req), twirp.InvalidArgument)
			So(err.Msg(), ShouldEqual, "sizes supports between 1-3 values")
			So(err.Meta("argument"), ShouldEqual, "sizes")

			Convey("when the request has a 1x size", func() {
				req.Sizes = []mk.EmoteImageSize{mk.EmoteImageSize_size_1x}
				So(validateGetEmoteUploadConfigRequest(req), ShouldBeNil)

				Convey("when the request has a duplicate 1x size", func() {
					req.Sizes = append(req.Sizes, mk.EmoteImageSize_size_1x)
					err := validateTwirpError(validateGetEmoteUploadConfigRequest(req), twirp.InvalidArgument)
					So(err.Msg(), ShouldEqual, "sizes cannot contain a duplicate size")
					So(err.Meta("argument"), ShouldEqual, "sizes")
				})

				Convey("when the request has a 2x size too", func() {
					req.Sizes = append(req.Sizes, mk.EmoteImageSize_size_2x)
					So(validateGetEmoteUploadConfigRequest(req), ShouldBeNil)

					Convey("when the request has a 4x size too", func() {
						req.Sizes = append(req.Sizes, mk.EmoteImageSize_size_4x)
						So(validateGetEmoteUploadConfigRequest(req), ShouldBeNil)

						Convey("when the request has a bonus 4x size too", func() {
							req.Sizes = append(req.Sizes, mk.EmoteImageSize_size_4x)
							err := validateTwirpError(validateGetEmoteUploadConfigRequest(req), twirp.InvalidArgument)
							So(err.Msg(), ShouldEqual, "sizes supports between 1-3 values")
							So(err.Meta("argument"), ShouldEqual, "sizes")
						})
					})
				})
			})

			Convey("when the request has an original size", func() {
				req.Sizes = []mk.EmoteImageSize{mk.EmoteImageSize_size_original}
				err := validateTwirpError(validateGetEmoteUploadConfigRequest(req), twirp.InvalidArgument)
				So(err.Msg(), ShouldEqual, "sizes[0] cannot request to upload an original resolution image while also requesting that no resizing be performed")
				So(err.Meta("argument"), ShouldEqual, "sizes[0]")
			})
		})

		Convey("when the request is auto-resize", func() {
			req := &mk.GetEmoteUploadConfigRequest{
				UserId:     "265716088",
				ResizePlan: mk.ResizePlan_auto_resize,
			}

			So(validateGetEmoteUploadConfigRequest(req), ShouldBeNil)

			Convey("when the request has original size in the sizes list", func() {
				req.Sizes = []mk.EmoteImageSize{mk.EmoteImageSize_size_original}
				So(validateGetEmoteUploadConfigRequest(req), ShouldBeNil)

				Convey("when the request has an additional value", func() {
					req.Sizes = append(req.Sizes, mk.EmoteImageSize_size_1x)
					err := validateTwirpError(validateGetEmoteUploadConfigRequest(req), twirp.InvalidArgument)
					So(err.Msg(), ShouldEqual, "sizes sizes only supports a single value of size_original for auto_resize plan (which itself is optional)")
					So(err.Meta("argument"), ShouldEqual, "sizes")
				})
			})

			Convey("when the request has a fixed size in the sizes list", func() {
				req.Sizes = []mk.EmoteImageSize{mk.EmoteImageSize_size_2x}
				err := validateTwirpError(validateGetEmoteUploadConfigRequest(req), twirp.InvalidArgument)
				So(err.Msg(), ShouldEqual, "sizes can only contain original size when auto_resize plan is specified")
				So(err.Meta("argument"), ShouldEqual, "sizes")
			})
		})

		Convey("when the request is for an animated asset", func() {
			req := &mk.GetEmoteUploadConfigRequest{
				UserId:     "265716088",
				ResizePlan: mk.ResizePlan_auto_resize,
				AssetType:  mk.EmoteAssetType_animated,
			}

			So(validateGetEmoteUploadConfigRequest(req), ShouldBeNil)

			Convey("when static asset generation is enabled", func() {
				req.GenerateStaticVersionOfAnimatedAssets = true

				So(validateGetEmoteUploadConfigRequest(req), ShouldBeNil)
			})
		})

		Convey("when the request is for a static asset", func() {
			req := &mk.GetEmoteUploadConfigRequest{
				UserId:     "265716088",
				ResizePlan: mk.ResizePlan_auto_resize,
				AssetType:  mk.EmoteAssetType_static,
			}

			So(validateGetEmoteUploadConfigRequest(req), ShouldBeNil)

			Convey("when static asset generation is enabled", func() {
				req.GenerateStaticVersionOfAnimatedAssets = true

				err := validateTwirpError(validateGetEmoteUploadConfigRequest(req), twirp.InvalidArgument)
				So(err.Msg(), ShouldEqual, "generate_static_version_of_animated_assets cannot be true when asset_type is not animated")
				So(err.Meta("argument"), ShouldEqual, "generate_static_version_of_animated_assets")
			})
		})
	})
}

func validateTwirpError(err error, errorCode twirp.ErrorCode) twirp.Error {
	So(err, ShouldNotBeNil)
	twirpErr, ok := err.(twirp.Error)
	So(ok, ShouldBeTrue)
	So(twirpErr.Code(), ShouldEqual, errorCode)
	return twirpErr
}
