package api

import (
	"context"
	"time"

	"code.justin.tv/commerce/logrus"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/mako/clients"
	"code.justin.tv/commerce/mako/clients/pubsub"
	"code.justin.tv/commerce/mako/config"
	mako "code.justin.tv/commerce/mako/internal"
	mk "code.justin.tv/commerce/mako/twirp"
	"github.com/pkg/errors"
	"github.com/twitchtv/twirp"
)

type CreateEmoteEntitlementsAPI struct {
	Clients clients.Clients
	Config  *config.Configuration
}

func (ce *CreateEmoteEntitlementsAPI) CreateEmoteEntitlements(ctx context.Context, r *mk.CreateEmoteEntitlementsRequest) (*mk.CreateEmoteEntitlementsResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, 4*time.Second)
	defer cancel()

	entitlements := make([]mako.Entitlement, len(r.Entitlements))

	// introduceLag is used to add latency before sending the response back to compensate for the eventual consistency
	// of the EntitlementDB layer where read-after-writes are stale and always n-1. Materia experiences ~400ms of GSI replication
	// latency and thus the lag introduced needs to be slightly greater than that.
	introduceLag := false
	entitledUsers := make(map[string]bool)

	for i, entitlement := range r.Entitlements {
		err := validateEntitlement(entitlement)

		if err != nil {
			return nil, err
		}

		var group = entitlement.OverrideGroupId
		var source mako.EntitlementSource

		switch entitlement.Group {
		case mk.EmoteGroup_ChannelPoints:
			source = mako.ChannelPointsSource
			group = ce.Config.CommunityPointsEmoteSetID
			introduceLag = true
		case mk.EmoteGroup_HypeTrain:
			source = mako.HypeTrainSource
		case mk.EmoteGroup_MegaCommerce:
			source = mako.MegaCommerceSource
		case mk.EmoteGroup_Rewards:
			source = mako.RewardsSource
		default:
			logrus.Errorf("the group '%s' is not supported by the CreateEmoteEntitlements API", entitlement.Group.String())
			return nil, twirp.InvalidArgumentError("group", "The group specified is not supported for this API")
		}

		ownerID := entitlement.ItemOwnerId
		if ownerID == "" {
			emotes, err := ce.Clients.EmoteDB.ReadByIDs(ctx, entitlement.EmoteId)
			if err != nil {
				return nil, twirp.InternalErrorWith(errors.Wrap(err, "failed to find emote by id"))
			}

			if len(emotes) == 0 {
				return nil, twirp.InternalErrorWith(errors.New("Could not find emotes for given emote id"))
			}

			ownerID = emotes[0].OwnerID
		}

		var start time.Time
		if entitlement.Start != nil {
			start = time.Unix(entitlement.Start.Seconds, 0)
		}

		var end *time.Time
		if entitlement.End != nil && !entitlement.End.IsInfinite {
			if entitlement.End.Time != nil {
				t := time.Unix(entitlement.End.Time.Seconds, 0)
				end = &t
			}
		}

		entitlements[i] = mako.Entitlement{
			OwnerID:   entitlement.OwnerId,
			Source:    source,
			OriginID:  entitlement.OriginId,
			ID:        entitlement.EmoteId,
			ChannelID: ownerID,
			Start:     start,
			End:       end,
			Type:      mako.EmoteEntitlement,
			Metadata: mako.EntitlementMetadata{
				GroupID: group,
			},
		}

		// Add user to map
		entitledUsers[entitlement.OwnerId] = true
	}

	if err := ce.Clients.EntitlementDB.WriteEntitlements(ctx, entitlements...); err != nil {
		logrus.WithError(err).Error("failed to create emote entitlement")
		return nil, twirp.InternalErrorWith(errors.Wrap(err, "failed to create emote entitlements"))
	}

	// we're going to wait async and then send the events out to refresh to buy Materia's GSI's to update.
	go func() {
		time.Sleep(3 * time.Second)
		// refresh emote picker for entitled users
		for userID, _ := range entitledUsers {
			psEvent := pubsub.UserSubscribeEvent{
				UserID:    userID,
				ChannelID: "0", // we do not have a channel so will default to 0
			}

			if err := ce.Clients.Pubsub.UserSubscription(context.Background(), psEvent, nil); err != nil {
				log.WithField("entitled user", userID).
					WithError(err).
					Error("Could not send user-subscribe-event pubsub message")
			}
		}
	}()

	if introduceLag {
		select {
		case <-time.After(600 * time.Millisecond):
		case <-ctx.Done():
		}
	}

	return &mk.CreateEmoteEntitlementsResponse{}, nil
}

func validateEntitlement(entitlement *mk.EmoteEntitlement) error {
	if entitlement.OwnerId == "" {
		return twirp.InvalidArgumentError("OwnerId", "Required")
	}

	if entitlement.EmoteId == "" {
		return twirp.InvalidArgumentError("EmoteId", "Required")
	}

	if entitlement.OriginId == "" {
		return twirp.InvalidArgumentError("OriginId", "Required")
	}

	if entitlement.Start == nil {
		return twirp.InvalidArgumentError("Start", "Required")
	}

	if !entitlement.End.IsInfinite && entitlement.End.Time == nil {
		return twirp.InvalidArgumentError("End", "End.IsInfinite must be true or End.Time must be set")
	}

	return nil
}
