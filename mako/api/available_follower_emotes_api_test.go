package api

import (
	"context"
	"testing"

	config_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/config"

	"code.justin.tv/commerce/mako/clients"
	mako "code.justin.tv/commerce/mako/internal"
	internal_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/internal_mock"
	mk "code.justin.tv/commerce/mako/twirp"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestGetAvailableFollowerEmotesAPI(t *testing.T) {
	Convey("Test GetUserFollowerEmoteStatus API", t, func() {
		mockEmoteGroupDB := new(internal_mock.EmoteGroupDB)
		mockEmoteDB := new(internal_mock.EmoteDB)
		mockDynamicConfigClient := new(config_mock.DynamicConfigClient)
		getAvailableFollowerEmotesAPI := GetAvailableFollowerEmotesAPI{
			Clients: clients.Clients{
				EmoteGroupDB: mockEmoteGroupDB,
				EmoteDB:      mockEmoteDB,
			},
			DynamicConfig: mockDynamicConfigClient,
		}

		ctx := context.Background()
		channelID := "channel_id"
		groupID := "group_ID"

		req := &mk.GetAvailableFollowerEmotesRequest{
			ChannelId: channelID,
		}

		Convey("Not in Follower Emotes Beta", func() {
			mockDynamicConfigClient.On("IsInFreemotesExperiment", channelID).Return(false)

			resp, err := getAvailableFollowerEmotesAPI.GetAvailableFollowerEmotes(ctx, req)
			So(resp, ShouldResemble, &mk.GetAvailableFollowerEmotesResponse{})
			So(err, ShouldBeNil)
		})

		Convey("In Follower Emotes Beta", func() {
			mockDynamicConfigClient.On("IsInFreemotesExperiment", channelID).Return(true)

			Convey("Error retrieving follower emote group for channel", func() {
				mockEmoteGroupDB.On("GetFollowerEmoteGroup", mock.Anything, channelID).Return(nil, errors.New("ERROR"))

				resp, err := getAvailableFollowerEmotesAPI.GetAvailableFollowerEmotes(ctx, req)
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})

			Convey("Error retrieving follower emotes for group", func() {
				req := &mk.GetAvailableFollowerEmotesRequest{
					ChannelId: channelID,
				}

				mockEmoteGroupDB.On("GetFollowerEmoteGroup", mock.Anything, channelID).Return(&mako.EmoteGroup{
					GroupID: groupID,
					OwnerID: channelID,
				}, nil)

				mockEmoteDB.On("ReadByGroupIDs", mock.Anything, groupID).Return(nil, errors.New("ERROR"))

				resp, err := getAvailableFollowerEmotesAPI.GetAvailableFollowerEmotes(ctx, req)
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})

			Convey("didn't find any emotes", func() {
				req := &mk.GetAvailableFollowerEmotesRequest{
					ChannelId: channelID,
				}

				mockEmoteGroupDB.On("GetFollowerEmoteGroup", mock.Anything, channelID).Return(&mako.EmoteGroup{
					GroupID: groupID,
					OwnerID: channelID,
				}, nil)

				mockEmoteDB.On("ReadByGroupIDs", mock.Anything, groupID).Return([]mako.Emote{}, nil)

				resp, err := getAvailableFollowerEmotesAPI.GetAvailableFollowerEmotes(ctx, req)
				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
				So(len(resp.GetEmoticons()), ShouldEqual, 0)
			})

			Convey("success", func() {
				req := &mk.GetAvailableFollowerEmotesRequest{
					ChannelId: channelID,
				}

				mockEmoteGroupDB.On("GetFollowerEmoteGroup", mock.Anything, channelID).Return(&mako.EmoteGroup{
					GroupID: groupID,
					OwnerID: channelID,
				}, nil)

				mockEmoteDB.On("ReadByGroupIDs", mock.Anything, groupID).Return([]mako.Emote{
					{
						ID:    "emote1",
						Order: 1,
					},
					{
						ID:    "emote2",
						Order: 0,
					},
				}, nil)

				resp, err := getAvailableFollowerEmotesAPI.GetAvailableFollowerEmotes(ctx, req)
				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
				So(len(resp.GetEmoticons()), ShouldEqual, 2)
				So(resp.GetEmoticons()[0].Order, ShouldEqual, 0)
				So(resp.GetEmoticons()[0].Id, ShouldEqual, "emote2")
				So(resp.GetEmoticons()[1].Order, ShouldEqual, 1)
				So(resp.GetEmoticons()[1].Id, ShouldEqual, "emote1")
			})
		})
	})
}
