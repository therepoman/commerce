package api

import (
	"net/http"

	log "code.justin.tv/commerce/logrus"
)

// HealthAPI encompasses health-check related APIs
type HealthAPI struct{}

// EverythingIsFine returns a health check and default response
func (health *HealthAPI) RunHealthCheck(w http.ResponseWriter, r *http.Request) {
	_, err := w.Write([]byte("OK"))
	if err != nil {
		log.Error("OK response failed")
	}
}
