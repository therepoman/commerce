package api

import (
	"context"
	"testing"
	"time"

	materiatwirp "code.justin.tv/amzn/MateriaTwirp"
	"code.justin.tv/commerce/mako/clients"
	materia_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/materia"
	mk "code.justin.tv/commerce/mako/twirp"
	"github.com/golang/protobuf/ptypes/timestamp"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestDeleteEmoteGroupEntitlements(t *testing.T) {
	Convey("Given DeleteEmoteGroupEntitlementsAPI", t, func() {
		mockMateria := new(materia_mock.MateriaClient)
		api := DeleteEmoteGroupEntitlementsAPI{
			Clients: clients.Clients{
				MateriaClient: mockMateria,
			},
		}

		Convey("Test DeleteEmoteGroupEntitlement", func() {
			Convey("With empty OwnerId", func() {
				req := &mk.DeleteEmoteGroupEntitlementsRequest{
					Entitlements: []*mk.EmoteGroupEntitlement{
						{
							OriginId: "1",
							GroupId:  "2",
							ItemId:   "5",
							End: &mk.InfiniteTime{
								IsInfinite: true,
							},
						},
					},
				}
				_, err := api.DeleteEmoteGroupEntitlements(context.Background(), req)
				So(err, ShouldNotBeNil)
			})

			Convey("With empty GroupId", func() {
				req := &mk.DeleteEmoteGroupEntitlementsRequest{
					Entitlements: []*mk.EmoteGroupEntitlement{
						{
							OriginId: "1",
							OwnerId:  "2",
							ItemId:   "5",
							End: &mk.InfiniteTime{
								IsInfinite: true,
							},
						},
					},
				}
				_, err := api.DeleteEmoteGroupEntitlements(context.Background(), req)
				So(err, ShouldNotBeNil)
			})

			Convey("With empty OriginId", func() {
				req := &mk.DeleteEmoteGroupEntitlementsRequest{
					Entitlements: []*mk.EmoteGroupEntitlement{
						{
							GroupId: "2",
							ItemId:  "5",
							OwnerId: "1",
							End: &mk.InfiniteTime{
								IsInfinite: true,
							},
						},
					},
				}
				_, err := api.DeleteEmoteGroupEntitlements(context.Background(), req)
				So(err, ShouldNotBeNil)
			})

			Convey("With empty End", func() {
				req := &mk.DeleteEmoteGroupEntitlementsRequest{
					Entitlements: []*mk.EmoteGroupEntitlement{
						{
							OriginId: "1",
							GroupId:  "2",
						},
					},
				}
				_, err := api.DeleteEmoteGroupEntitlements(context.Background(), req)
				So(err, ShouldNotBeNil)
			})

			Convey("Deletes materia entitlements", func() {
				req := &mk.DeleteEmoteGroupEntitlementsRequest{
					Entitlements: []*mk.EmoteGroupEntitlement{
						{
							OwnerId:  "ownerid1",
							OriginId: "1",
							GroupId:  "emotegroup1",
							Group:    mk.EmoteGroup_Subscriptions,
							ItemId:   "channelid1",
							Start: &timestamp.Timestamp{
								Seconds: time.Now().Unix(),
							},
							End: &mk.InfiniteTime{
								IsInfinite: true,
							},
						},
					},
				}

				mockMateria.On("DeleteEntitlements", mock.Anything, mock.Anything).Return(&materiatwirp.DeleteEntitlementsResponse{}, nil)

				_, err := api.DeleteEmoteGroupEntitlements(context.Background(), req)
				So(err, ShouldBeNil)
			})
		})
	})
}
