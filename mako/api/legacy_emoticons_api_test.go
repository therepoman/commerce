package api

import (
	"context"
	"testing"

	"code.justin.tv/commerce/mako/clients"
	mako "code.justin.tv/commerce/mako/internal"
	subscriptions_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/subscriptions"
	"code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/internal_mock"
	mk "code.justin.tv/commerce/mako/twirp"
	substwirp "code.justin.tv/revenue/subscriptions/twirp"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestLegacyEmoticonsAPI(t *testing.T) {
	Convey("Test Legacy Emoticons API", t, func() {
		mockSubscriptionsClient := new(subscriptions_mock.ClientWrapper)
		mockEmoteDB := new(internal_mock.EmoteDB)

		legacyEmoticonsAPI := LegacyEmoticonsAPI{
			Clients: clients.Clients{
				EmoteDB:             mockEmoteDB,
				SubscriptionsClient: mockSubscriptionsClient,
			},
		}

		testChannelId := "232889822"
		testEmoteSetId := "17133"
		globalSetId := "0"
		testEmoteCode := "TestCode"
		testGlobalEmoteCode := "TestGlobal"

		Convey("Test GetLegacyChannelEmoticons", func() {

			Convey("given request is missing channel id, then return twirp invalid argument error", func() {
				request := &mk.GetLegacyChannelEmoticonsRequest{
					ChannelName: "test_channel",
				}

				resp, err := legacyEmoticonsAPI.GetLegacyChannelEmoticons(context.Background(), request)

				So(resp, ShouldBeNil)
				So(err, ShouldNotBeNil)
				So(err.Error(), ShouldContainSubstring, "channel_id")
				So(err.Error(), ShouldContainSubstring, "must not be empty")
			})

			Convey("given request is missing channel name, then return twirp invalid argument error", func() {
				request := &mk.GetLegacyChannelEmoticonsRequest{
					ChannelId: testChannelId,
				}

				resp, err := legacyEmoticonsAPI.GetLegacyChannelEmoticons(context.Background(), request)

				So(resp, ShouldBeNil)
				So(err, ShouldNotBeNil)
				So(err.Error(), ShouldContainSubstring, "channel_name")
				So(err.Error(), ShouldContainSubstring, "must not be empty")
			})

			Convey("given subs service returns an error, then return generic internal twirp error", func() {
				mockSubscriptionsClient.On("GetChannelProducts", mock.Anything, testChannelId).Return(
					&substwirp.GetChannelProductsResponse{Products: nil}, errors.New("dummy subs error"))

				request := &mk.GetLegacyChannelEmoticonsRequest{
					ChannelId:   testChannelId,
					ChannelName: "test_channel",
				}

				resp, err := legacyEmoticonsAPI.GetLegacyChannelEmoticons(context.Background(), request)
				So(resp, ShouldBeNil)
				So(err, ShouldNotBeNil)
				So(err.Error(), ShouldContainSubstring, "Error getting legacy channel emoticons")
				So(err.Error(), ShouldNotContainSubstring, "dummy subs error")
			})

			Convey("given subs service returns success with products", func() {
				mockSubscriptionsClient.On("GetChannelProducts", mock.Anything, testChannelId).Return(&substwirp.GetChannelProductsResponse{
					Products: []*substwirp.Product{
						{
							EmoticonSetId: testEmoteSetId,
						},
					},
				}, nil)

				Convey("given emote db returns error, then return generic internal twirp error", func() {
					mockEmoteDB.On("ReadByGroupIDs", mock.Anything, testEmoteSetId, globalSetId).Return(nil, errors.New("dummy emotedb error"))

					request := &mk.GetLegacyChannelEmoticonsRequest{
						ChannelId:   testChannelId,
						ChannelName: "test_channel",
					}

					resp, err := legacyEmoticonsAPI.GetLegacyChannelEmoticons(context.Background(), request)
					So(resp, ShouldBeNil)
					So(err, ShouldNotBeNil)
					So(err.Error(), ShouldContainSubstring, "Error getting legacy channel emoticons")
					So(err.Error(), ShouldNotContainSubstring, "dummy emotedb error")
				})

				Convey("given emote db returns success with channel emotes for the subs product and globals, then return those emotes", func() {
					mockEmoteDB.On("ReadByGroupIDs", mock.Anything, testEmoteSetId, globalSetId).Return([]mako.Emote{
						{
							ID:      "1",
							Code:    testEmoteCode,
							GroupID: testEmoteSetId,
							State:   mako.Active,
						},
						{
							ID:      "2",
							Code:    testGlobalEmoteCode,
							GroupID: globalSetId,
							State:   mako.Active,
						},
					}, nil)

					request := &mk.GetLegacyChannelEmoticonsRequest{
						ChannelId:   testChannelId,
						ChannelName: "test_channel",
					}

					response, err := legacyEmoticonsAPI.GetLegacyChannelEmoticons(context.Background(), request)
					So(err, ShouldBeNil)
					So(len(response.Emoticons), ShouldEqual, 2)

					if response.Emoticons[0].Regex == testEmoteCode {
						So(response.Emoticons[0].Regex, ShouldEqual, testEmoteCode)
						So(response.Emoticons[0].SubscriberOnly, ShouldBeTrue)

						So(response.Emoticons[1].Regex, ShouldEqual, testGlobalEmoteCode)
						So(response.Emoticons[1].SubscriberOnly, ShouldBeFalse)
					} else {
						So(response.Emoticons[1].Regex, ShouldEqual, testEmoteCode)
						So(response.Emoticons[1].SubscriberOnly, ShouldBeTrue)

						So(response.Emoticons[0].Regex, ShouldEqual, testGlobalEmoteCode)
						So(response.Emoticons[0].SubscriberOnly, ShouldBeFalse)
					}
				})

				Convey("given emote db returns success with a mix of active and non-active emotes, return only active emotes", func() {
					mockEmoteDB.On("ReadByGroupIDs", mock.Anything, testEmoteSetId, globalSetId).Return([]mako.Emote{
						{
							ID:      "1",
							Code:    testEmoteCode,
							GroupID: testEmoteSetId,
							State:   mako.Active,
						},
						{
							ID:      "2",
							Code:    testGlobalEmoteCode,
							GroupID: globalSetId,
							State:   mako.Active,
						},
						{
							ID:      "3",
							Code:    "notAnActiveEmoteCode",
							GroupID: testEmoteSetId,
							State:   mako.Pending,
						},
						{
							ID:      "4",
							Code:    "notAnActiveGlobalCode",
							GroupID: globalSetId,
							State:   mako.Inactive,
						},
					}, nil)

					request := &mk.GetLegacyChannelEmoticonsRequest{
						ChannelId:   testChannelId,
						ChannelName: "test_channel",
					}

					response, err := legacyEmoticonsAPI.GetLegacyChannelEmoticons(context.Background(), request)
					So(err, ShouldBeNil)
					So(len(response.Emoticons), ShouldEqual, 2)

					if response.Emoticons[0].Regex == testEmoteCode {
						So(response.Emoticons[0].Regex, ShouldEqual, testEmoteCode)
						So(response.Emoticons[0].SubscriberOnly, ShouldBeTrue)

						So(response.Emoticons[1].Regex, ShouldEqual, testGlobalEmoteCode)
						So(response.Emoticons[1].SubscriberOnly, ShouldBeFalse)
					} else {
						So(response.Emoticons[1].Regex, ShouldEqual, testEmoteCode)
						So(response.Emoticons[1].SubscriberOnly, ShouldBeTrue)

						So(response.Emoticons[0].Regex, ShouldEqual, testGlobalEmoteCode)
						So(response.Emoticons[0].SubscriberOnly, ShouldBeFalse)
					}
				})
			})

			Convey("given subs service returns success with no products, then emote db is queried for only globals and only globals are returned", func() {
				mockSubscriptionsClient.On("GetChannelProducts", mock.Anything, testChannelId).Return(&substwirp.GetChannelProductsResponse{
					Products: []*substwirp.Product{},
				}, nil)

				mockEmoteDB.On("ReadByGroupIDs", mock.Anything, globalSetId).Return([]mako.Emote{{ID: "2", Code: testGlobalEmoteCode, GroupID: globalSetId, State: mako.Active}}, nil)

				request := &mk.GetLegacyChannelEmoticonsRequest{
					ChannelId:   testChannelId,
					ChannelName: "test_channel",
				}

				response, err := legacyEmoticonsAPI.GetLegacyChannelEmoticons(context.Background(), request)
				So(err, ShouldBeNil)
				So(len(response.Emoticons), ShouldEqual, 1)
				So(response.Emoticons[0].Regex, ShouldEqual, testGlobalEmoteCode)
				So(response.Emoticons[0].SubscriberOnly, ShouldBeFalse)
			})
		})
	})
}
