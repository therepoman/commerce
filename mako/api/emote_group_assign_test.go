package api

import (
	"context"
	"errors"
	"testing"

	config_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/config"
	"code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/internal_mock"

	mako "code.justin.tv/commerce/mako/internal"
	emoticonsmanager_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/emoticonsmanager"
	mkd "code.justin.tv/commerce/mako/twirp/dashboard"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestEmoteGroupAssignAPI(t *testing.T) {
	Convey("Test EmoteGroupAssignAPI", t, func() {
		mockEmoticonsManager := new(emoticonsmanager_mock.IEmoticonsManager)
		emoteGroupAssignAPI := &EmoteGroupAssignAPI{
			EmoticonsManager: mockEmoticonsManager,
		}

		userID := "someUser"
		emoteID := "emotesv2_1234abcd"
		groupID := "someGroup"

		Convey("Test AssignEmoteToGroup", func() {
			Convey("With empty requesting_user_id", func() {
				req := &mkd.AssignEmoteToGroupRequest{
					RequestingUserId: "",
				}

				res, err := emoteGroupAssignAPI.AssignEmoteToGroup(context.Background(), req)
				So(err, ShouldNotBeNil)
				So(res, ShouldBeNil)
			})

			Convey("With empty emote_id", func() {
				req := &mkd.AssignEmoteToGroupRequest{
					RequestingUserId: userID,
					EmoteId:          "",
				}

				res, err := emoteGroupAssignAPI.AssignEmoteToGroup(context.Background(), req)
				So(err, ShouldNotBeNil)
				So(res, ShouldBeNil)
			})

			Convey("With empty group_id", func() {
				req := &mkd.AssignEmoteToGroupRequest{
					RequestingUserId: userID,
					EmoteId:          emoteID,
					GroupId:          "",
				}

				res, err := emoteGroupAssignAPI.AssignEmoteToGroup(context.Background(), req)
				So(err, ShouldNotBeNil)
				So(res, ShouldBeNil)
			})

			Convey("With unsupported origin", func() {
				req := &mkd.AssignEmoteToGroupRequest{
					RequestingUserId: userID,
					EmoteId:          emoteID,
					GroupId:          groupID,
					Origin:           mkd.EmoteOrigin_Globals,
				}

				res, err := emoteGroupAssignAPI.AssignEmoteToGroup(context.Background(), req)
				So(err, ShouldNotBeNil)
				So(res, ShouldBeNil)
			})

			Convey("With a valid request", func() {
				req := &mkd.AssignEmoteToGroupRequest{
					RequestingUserId: userID,
					EmoteId:          emoteID,
					GroupId:          groupID,
					Origin:           mkd.EmoteOrigin_Subscriptions,
				}

				Convey("When the EmoticonsManager fails", func() {
					mockEmoticonsManager.On("AssignToGroup", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(mako.Emote{}, errors.New("some_error"))

					res, err := emoteGroupAssignAPI.AssignEmoteToGroup(context.Background(), req)
					So(err, ShouldNotBeNil)
					So(res, ShouldBeNil)
				})

				Convey("When the EmoticonsManager succeeds", func() {
					mockEmoticonsManager.On("AssignToGroup", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(
						mako.Emote{
							ID:          emoteID,
							OwnerID:     userID,
							GroupID:     groupID,
							State:       mako.Active,
							EmotesGroup: req.Origin.String(),
						}, nil)

					res, err := emoteGroupAssignAPI.AssignEmoteToGroup(context.Background(), req)
					So(err, ShouldBeNil)
					So(res, ShouldNotBeNil)
					So(res.Emote, ShouldNotBeNil)
					So(res.Emote.Id, ShouldEqual, emoteID)
					So(res.Emote.OwnerId, ShouldEqual, userID)
					So(res.Emote.State, ShouldEqual, mkd.EmoteState_active)
					So(res.Emote.GroupId, ShouldEqual, groupID)
				})
			})
		})

		Convey("Test AssignEmoteToFollowerGroup", func() {
			mockEmoticonsManager := new(emoticonsmanager_mock.IEmoticonsManager)
			mockDynamicConfig := new(config_mock.DynamicConfigClient)
			mockEmoteGroupDB := new(internal_mock.EmoteGroupDB)
			mockEmoteDB := new(internal_mock.EmoteDB)
			emoteGroupAssignAPI := &EmoteGroupAssignAPI{
				EmoticonsManager: mockEmoticonsManager,
				EmoteGroupDB:     mockEmoteGroupDB,
				EmoteDB:          mockEmoteDB,
				DynamicConfig:    mockDynamicConfig,
			}

			userID := "someUser"
			emoteID := "emotesv2_1234abcd"

			Convey("With empty channel_id", func() {
				req := &mkd.AssignEmoteToFollowerGroupRequest{
					ChannelId: "",
				}

				res, err := emoteGroupAssignAPI.AssignEmoteToFollowerGroup(context.Background(), req)
				So(err, ShouldNotBeNil)
				So(res, ShouldBeNil)
			})

			Convey("With empty requesting_user_id", func() {
				req := &mkd.AssignEmoteToFollowerGroupRequest{
					ChannelId:        userID,
					RequestingUserId: "",
				}

				res, err := emoteGroupAssignAPI.AssignEmoteToFollowerGroup(context.Background(), req)
				So(err, ShouldNotBeNil)
				So(res, ShouldBeNil)
			})

			Convey("With empty emote_id", func() {
				req := &mkd.AssignEmoteToFollowerGroupRequest{
					ChannelId:        userID,
					RequestingUserId: userID,
					EmoteId:          "",
				}

				res, err := emoteGroupAssignAPI.AssignEmoteToFollowerGroup(context.Background(), req)
				So(err, ShouldNotBeNil)
				So(res, ShouldBeNil)
			})

			Convey("when channel_id and requesting_user_id don't match", func() {
				req := &mkd.AssignEmoteToFollowerGroupRequest{
					ChannelId:        userID,
					RequestingUserId: "1234567",
					EmoteId:          emoteID,
				}

				res, err := emoteGroupAssignAPI.AssignEmoteToFollowerGroup(context.Background(), req)
				So(err, ShouldNotBeNil)
				So(res, ShouldBeNil)
			})

			Convey("With a valid request", func() {
				req := &mkd.AssignEmoteToFollowerGroupRequest{
					ChannelId:        userID,
					RequestingUserId: userID,
					EmoteId:          emoteID,
				}

				Convey("When the user is not in the follower emotes experiment", func() {
					mockDynamicConfig.On("IsInFreemotesExperiment", userID).Return(false)

					res, err := emoteGroupAssignAPI.AssignEmoteToFollowerGroup(context.Background(), req)
					So(err, ShouldNotBeNil)
					So(res, ShouldBeNil)
				})

				Convey("When the user is in the follower emotes experiment", func() {
					mockDynamicConfig.On("IsInFreemotesExperiment", userID).Return(true)

					Convey("When the follower group is not found", func() {
						mockEmoteGroupDB.On("GetFollowerEmoteGroup", mock.Anything, userID).Return(nil, errors.New("test error"))

						res, err := emoteGroupAssignAPI.AssignEmoteToFollowerGroup(context.Background(), req)
						So(err, ShouldNotBeNil)
						So(res, ShouldBeNil)
					})

					Convey("When the follower group is found", func() {
						mockEmoteGroupDB.On("GetFollowerEmoteGroup", mock.Anything, userID).Return(&mako.EmoteGroup{
							GroupID:   groupID,
							OwnerID:   userID,
							Origin:    mako.EmoteGroupOriginFollower,
							GroupType: mako.EmoteGroupStatic,
						}, nil)

						Convey("When the other follower emotes for this channel are not found due to an error", func() {
							mockEmoteDB.On("ReadByGroupIDs", mock.Anything, groupID).Return(nil, errors.New("test error"))

							res, err := emoteGroupAssignAPI.AssignEmoteToFollowerGroup(context.Background(), req)
							So(err, ShouldNotBeNil)
							So(res, ShouldBeNil)
						})

						Convey("When the other follower emotes for this channel equal the limit for follower emotes", func() {
							mockEmoteDB.On("ReadByGroupIDs", mock.Anything, groupID).Return([]mako.Emote{
								{
									ID:          "otherEmoteID",
									OwnerID:     userID,
									GroupID:     groupID,
									State:       mako.Active,
									EmotesGroup: mkd.EmoteOrigin_Follower.String(),
								},
								{
									ID:          "otherEmoteID2",
									OwnerID:     userID,
									GroupID:     groupID,
									State:       mako.Active,
									EmotesGroup: mkd.EmoteOrigin_Follower.String(),
								},
								{
									ID:          "otherEmoteID3",
									OwnerID:     userID,
									GroupID:     groupID,
									State:       mako.Active,
									EmotesGroup: mkd.EmoteOrigin_Follower.String(),
								},
								{
									ID:          "otherEmoteID4",
									OwnerID:     userID,
									GroupID:     groupID,
									State:       mako.Active,
									EmotesGroup: mkd.EmoteOrigin_Follower.String(),
								},
								{
									ID:          "otherEmoteID5",
									OwnerID:     userID,
									GroupID:     groupID,
									State:       mako.Active,
									EmotesGroup: mkd.EmoteOrigin_Follower.String(),
								},
							}, nil)

							res, err := emoteGroupAssignAPI.AssignEmoteToFollowerGroup(context.Background(), req)
							So(err, ShouldNotBeNil)
							So(res, ShouldBeNil)
						})

						Convey("When the other follower emotes for this channel are less than the limit for follower emotes", func() {
							mockEmoteDB.On("ReadByGroupIDs", mock.Anything, groupID).Return([]mako.Emote{
								{
									ID:          "otherEmoteID",
									OwnerID:     userID,
									GroupID:     groupID,
									State:       mako.Active,
									EmotesGroup: mkd.EmoteOrigin_Follower.String(),
								},
							}, nil)

							Convey("When the emoticon manager's assign to group call fails", func() {
								mockEmoticonsManager.On("AssignToGroup", mock.Anything, userID, emoteID, groupID, mkd.EmoteOrigin_Follower).Return(mako.Emote{}, errors.New("test error"))

								res, err := emoteGroupAssignAPI.AssignEmoteToFollowerGroup(context.Background(), req)
								So(err, ShouldNotBeNil)
								So(res, ShouldBeNil)
							})

							Convey("When the emoticon manager's assign to group call succeeds", func() {
								mockEmoticonsManager.On("AssignToGroup", mock.Anything, userID, emoteID, groupID, mkd.EmoteOrigin_Follower).Return(mako.Emote{
									ID:          emoteID,
									OwnerID:     userID,
									GroupID:     groupID,
									State:       mako.Active,
									EmotesGroup: mkd.EmoteOrigin_Follower.String(),
									AssetType:   mako.StaticAssetType,
								}, nil)

								res, err := emoteGroupAssignAPI.AssignEmoteToFollowerGroup(context.Background(), req)
								So(err, ShouldBeNil)
								So(res, ShouldNotBeNil)
								So(res.Emote, ShouldNotBeNil)
								So(res.Emote.Id, ShouldEqual, emoteID)
								So(res.Emote.OwnerId, ShouldEqual, userID)
								So(res.Emote.State, ShouldEqual, mkd.EmoteState_active)
								So(res.Emote.GroupId, ShouldEqual, groupID)
							})
						})
					})
				})

				//Convey("When the EmoticonsManager fails", func() {
				//	mockEmoticonsManager.On("AssignToGroup", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(mako.Emote{}, errors.New("some_error"))
				//
				//	res, err := emoteGroupAssignAPI.AssignEmoteToFollowerGroup(context.Background(), req)
				//	So(err, ShouldNotBeNil)
				//	So(res, ShouldBeNil)
				//})
				//
				//Convey("When the EmoticonsManager succeeds", func() {
				//	mockEmoticonsManager.On("AssignToGroup", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(
				//		mako.Emote{
				//			ID:          emoteID,
				//			OwnerID:     userID,
				//			GroupID:     groupID,
				//			State:       mako.Active,
				//			EmotesGroup: mkd.EmoteOrigin_Follower.String(),
				//		}, nil)
				//
				//	res, err := emoteGroupAssignAPI.AssignEmoteToFollowerGroup(context.Background(), req)
				//	So(err, ShouldBeNil)
				//	So(res, ShouldNotBeNil)
				//	So(res.Emote, ShouldNotBeNil)
				//	So(res.Emote.Id, ShouldEqual, emoteID)
				//	So(res.Emote.OwnerId, ShouldEqual, userID)
				//	So(res.Emote.State, ShouldEqual, mkd.EmoteState_active)
				//	So(res.Emote.GroupId, ShouldEqual, groupID)
				//})
			})
		})

		Convey("Test RemoveEmoteFromGroup", func() {
			Convey("With empty requesting_user_id", func() {
				req := &mkd.RemoveEmoteFromGroupRequest{
					RequestingUserId: "",
				}

				res, err := emoteGroupAssignAPI.RemoveEmoteFromGroup(context.Background(), req)
				So(err, ShouldNotBeNil)
				So(res, ShouldBeNil)
			})

			Convey("With empty emote_id", func() {
				req := &mkd.RemoveEmoteFromGroupRequest{
					RequestingUserId: userID,
					EmoteId:          "",
				}

				res, err := emoteGroupAssignAPI.RemoveEmoteFromGroup(context.Background(), req)
				So(err, ShouldNotBeNil)
				So(res, ShouldBeNil)
			})

			Convey("With a valid request", func() {
				req := &mkd.RemoveEmoteFromGroupRequest{
					RequestingUserId: userID,
					EmoteId:          emoteID,
				}

				Convey("When the EmoticonsManager fails", func() {
					mockEmoticonsManager.On("RemoveFromGroup", mock.Anything, mock.Anything, mock.Anything).Return(mako.Emote{}, errors.New("some_error"))

					res, err := emoteGroupAssignAPI.RemoveEmoteFromGroup(context.Background(), req)
					So(err, ShouldNotBeNil)
					So(res, ShouldBeNil)
				})

				Convey("When the EmoticonsManager succeeds", func() {
					mockEmoticonsManager.On("RemoveFromGroup", mock.Anything, mock.Anything, mock.Anything).Return(
						mako.Emote{
							ID:          emoteID,
							OwnerID:     userID,
							State:       mako.Archived,
							EmotesGroup: mkd.EmoteOrigin_Archive.String(),
						}, nil)

					res, err := emoteGroupAssignAPI.RemoveEmoteFromGroup(context.Background(), req)
					So(err, ShouldBeNil)
					So(res, ShouldNotBeNil)
					So(res.Emote, ShouldNotBeNil)
					So(res.Emote.Id, ShouldEqual, emoteID)
					So(res.Emote.OwnerId, ShouldEqual, userID)
					So(res.Emote.State, ShouldEqual, mkd.EmoteState_archived)
					So(res.Emote.GroupId, ShouldBeEmpty)
				})
			})
		})
	})
}
