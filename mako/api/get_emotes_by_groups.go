package api

import (
	"context"
	"time"

	"code.justin.tv/commerce/gogogadget/strings"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/mako/clients"
	mako "code.justin.tv/commerce/mako/internal"
	mako_dashboard "code.justin.tv/commerce/mako/twirp/dashboard"
	"github.com/twitchtv/twirp"
)

// GetEmotesByGroupsAPI includes the API for getting emotes by group id
type GetEmotesByGroupsAPI struct {
	Clients clients.Clients
}

// GetEmotesByGroups returns all emotes for the given emote group ids
func (api *GetEmotesByGroupsAPI) GetEmotesByGroups(ctx context.Context, r *mako_dashboard.GetEmotesByGroupsRequest) (*mako_dashboard.GetEmotesByGroupsResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, 1*time.Second)
	defer cancel()

	emoteGroupIDs := strings.Dedupe(r.GetEmoteGroupIds())
	if len(emoteGroupIDs) != len(r.GetEmoteGroupIds()) {
		log.WithFields(log.Fields{
			"group_ids": r.GetEmoteGroupIds(),
		}).Warn("given set of group ids in GetEmotesByGroups contains duplicates")
	}

	emoteGroupIDs = strings.RemoveEmptyStrings(emoteGroupIDs)

	emotes, err := api.Clients.EmoteDBUncached.ReadByGroupIDs(ctx, emoteGroupIDs...)
	if err != nil {
		msg := "Error reading emotes by groups in GetEmotesByGroups"
		log.WithError(err).WithField("group_ids", emoteGroupIDs).Error(msg)
		return nil, twirp.InternalError(msg)
	}

	// We need to merge both the emotes and emote groups because there is not guarantee that an
	// emote group from the emotes query exists in the emote groups table nor that the query for the
	// emote group has corresponding emotes
	emoteGroupsByIds := make(map[string]*mako_dashboard.EmoteGroup)
	filteredEmotes := mako.ApplyEmoteFilter(mako.MakeEmoteFilterFromDashboardTwirp(r.GetFilter()), emotes)
	for _, emote := range filteredEmotes {
		twirpEmote := emote.ToDashboardTwirp()
		emoteGroup, exist := emoteGroupsByIds[emote.GroupID]
		if !exist {
			emoteGroupsByIds[emote.GroupID] = &mako_dashboard.EmoteGroup{
				Id:     emote.GroupID,
				Emotes: []*mako_dashboard.Emote{&twirpEmote},
				Order:  mako.GetEmoteGroupOrder(emote.GroupID),
			}
		} else {
			emoteGroup.Emotes = append(emoteGroup.Emotes, &twirpEmote)
		}
	}

	emoteGroups, err := api.Clients.EmoteGroupDB.GetEmoteGroups(ctx, emoteGroupIDs...)
	if err != nil {
		msg := "Error reading emote groups by groups in GetEmotesByGroups"
		log.WithError(err).WithField("group_ids", emoteGroupIDs).Error(msg)
		return nil, twirp.InternalError(msg)
	}

	for _, emoteGroup := range emoteGroups {
		groupType := emoteGroup.GroupType.ToTwirp()
		group, exist := emoteGroupsByIds[emoteGroup.GroupID]
		if !exist {
			emoteGroupsByIds[emoteGroup.GroupID] = &mako_dashboard.EmoteGroup{
				Id:        emoteGroup.GroupID,
				Order:     mako.GetEmoteGroupOrder(emoteGroup.GroupID),
				GroupType: groupType,
			}
		} else {
			group.GroupType = groupType
		}
	}

	// If no emote has been created in a group, we are not creating (and therefore not returning) the
	// emote group. In this case, just return an empty static group.
	for _, groupID := range emoteGroupIDs {
		_, exists := emoteGroupsByIds[groupID]
		if !exists {
			emoteGroupsByIds[groupID] = &mako_dashboard.EmoteGroup{
				Id:    groupID,
				Order: mako.GetEmoteGroupOrder(groupID),
			}
		}
	}

	var emoticonGroups []*mako_dashboard.EmoteGroup
	for _, group := range emoteGroupsByIds {
		emotes := group.Emotes
		mako.SortDashboardTwirpEmotesByOrderAscendingAndCreatedAtDescending(emotes)

		emoticonGroups = append(emoticonGroups, &mako_dashboard.EmoteGroup{
			Id:        group.Id,
			Emotes:    emotes,
			Order:     mako.GetEmoteGroupOrder(group.Id),
			GroupType: group.GroupType,
		})
	}

	return &mako_dashboard.GetEmotesByGroupsResponse{
		Groups: emoticonGroups,
	}, nil
}
