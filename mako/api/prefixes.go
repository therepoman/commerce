package api

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"regexp"
	"strings"
	"time"

	receiver "code.justin.tv/amzn/TwitchDartReceiverTwirp"
	gogogadget_strings "code.justin.tv/commerce/gogogadget/strings"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/mako/clients"
	"code.justin.tv/commerce/mako/config"
	"code.justin.tv/commerce/mako/dynamo"
	"code.justin.tv/commerce/mako/integration/user_utils"
	mako "code.justin.tv/commerce/mako/internal"
	"code.justin.tv/commerce/mako/prefixes"
	"code.justin.tv/commerce/mako/sqs/model"
	mk "code.justin.tv/commerce/mako/twirp"
	e "github.com/pkg/errors"
	"github.com/twitchtv/twirp"
)

const (
	defaultPrefixesResponseLimit        = 20
	emotePrefixReviewedNotificationType = "emote_prefix_reviewed"
)

type PrefixesAPI interface {
	GetPrefix(ctx context.Context, r *mk.GetPrefixRequest) (*mk.GetPrefixResponse, error)
	SetPrefix(ctx context.Context, r *mk.SetPrefixRequest) (*mk.SetPrefixResponse, error)
	GetRecommendedPrefix(ctx context.Context, r *mk.GetRecommendedPrefixRequest) (*mk.GetRecommendedPrefixResponse, error)
	SubmitPrefix(ctx context.Context, r *mk.SubmitPrefixRequest) (*mk.SubmitPrefixResponse, error)
	GetPrefixesByState(ctx context.Context, r *mk.GetPrefixesByStateRequest) (*mk.GetPrefixesByStateResponse, error)

	// Only used for cleaning up integration tests
	DeletePrefix(ctx context.Context, prefixBeforeDelete string, userID string) error
}

type prefixMetadata struct {
	isEditable        bool
	editState         dynamo.PrefixState
	notEditableReason error
	isPartner         bool
}

type prefixesAPI struct {
	PrefixDao   dynamo.IPrefixDao
	Config      *config.Configuration
	Clients     *clients.Clients
	Recommender prefixes.PrefixRecommender
}

func NewPrefixesAPI(dao dynamo.IPrefixDao, config *config.Configuration, clients *clients.Clients, recommender prefixes.PrefixRecommender) PrefixesAPI {
	return &prefixesAPI{
		PrefixDao:   dao,
		Config:      config,
		Clients:     clients,
		Recommender: recommender,
	}
}

func (pa *prefixesAPI) SetPrefix(ctx context.Context, r *mk.SetPrefixRequest) (*mk.SetPrefixResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, 2*time.Second)
	defer cancel()

	if r == nil {
		return nil, twirp.InvalidArgumentError("request", "cannot be nil")
	}

	if gogogadget_strings.Blank(r.GetChannelId()) {
		return nil, twirp.InvalidArgumentError("channelId", "cannot be empty")
	}

	if gogogadget_strings.Blank(r.GetPrefix()) {
		log.WithField("req", *r).Info("couldn't grab prefix from request")
		return nil, twirp.InvalidArgumentError("prefix", "cannot be empty")
	}

	if r.State == mk.PrefixState_PREFIX_UNKNOWN {
		log.WithField("req", *r).Info("couldn't get state from request")
		return nil, twirp.InvalidArgumentError("state", "cannot be empty")
	}

	logger := log.WithFields(log.Fields{
		"channelId": r.ChannelId,
		"prefix":    r.Prefix,
		"state":     r.State,
	})

	invalidErr := pa.validatePrefix(ctx, r.Prefix, r.ChannelId)
	if invalidErr != nil {
		return nil, invalidErr
	}

	previousPrefix, err := pa.PrefixDao.Put(ctx, r.ChannelId, r.Prefix, twirpStateToDbState(r.State))
	if err != nil {
		validateErr := validatePutResponse(err)
		if validateErr != nil {
			return nil, validateErr
		}

		logger.WithError(err).Error("Failed to put DAO prefix")
		return nil, twirp.InternalError("Internal failure updating prefix for channel")
	}

	err = pa.postToSnsTopicForEmotesUpdates(ctx, r.ChannelId, r.Prefix, twirpStateToDbState(r.State), previousPrefix)
	if err != nil {
		logger.WithError(err).Error("Failed to emit to SNS topic for emote updates")
		return nil, twirp.InternalError("Internal failure updating emotes with prefix change")
	}

	// Skip publishing email if this is an integration test
	if !strings.Contains(r.ChannelId, user_utils.IntegrationTestUserPrefix) {
		err = pa.publishToDart(ctx, r.ChannelId, r.Prefix, twirpStateToDbState(r.State))
		if err != nil {
			logger.WithError(err).Error("Failed to publish EmotePrefixReviewedEvent to Dart")
			return nil, twirp.InternalError("Internal failure publishing email for prefix change")
		}
	}

	return &mk.SetPrefixResponse{}, nil
}

func (pa *prefixesAPI) SubmitPrefix(ctx context.Context, r *mk.SubmitPrefixRequest) (*mk.SubmitPrefixResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, 2*time.Second)
	defer cancel()

	if r == nil {
		return nil, twirp.InvalidArgumentError("request", "cannot be nil")
	}

	if gogogadget_strings.Blank(r.GetChannelId()) {
		return nil, twirp.InvalidArgumentError("channelId", "cannot be empty")
	}

	if gogogadget_strings.Blank(r.GetPrefix()) {
		return nil, twirp.InvalidArgumentError("prefix", "cannot be empty")
	}

	logger := log.WithFields(log.Fields{
		"channelId": r.ChannelId,
		"prefix":    r.Prefix,
	})

	prefixMetadata, err := pa.getPrefixIsEditable(ctx, r.ChannelId)

	if err != nil {
		logger.WithError(err).Error("Failed to get prefix metadata on prefix submit")
		return nil, twirp.InternalError("Internal error submitting prefix")
	}

	if !prefixMetadata.isEditable {
		return nil, prefixMetadata.notEditableReason
	}

	invalidErr := pa.validatePrefix(ctx, r.Prefix, r.ChannelId)

	if invalidErr != nil {
		return nil, invalidErr
	}
	// if user is an affiliate, they can only change their prefix to the recommended prefix
	if !prefixMetadata.isPartner {
		rec, err := pa.Recommender.GetRecommendedEmoticonPrefix(ctx, r.ChannelId)
		if err != nil {
			logger.WithError(err).Error("Failed to generate an emoticon prefix while validating submitted prefix")
			return nil, twirp.InternalError("Internal error retrieving recommended prefix")
		}
		if rec != r.Prefix {
			return nil, mk.NewClientError(
				twirp.NewError(twirp.FailedPrecondition, "Submitted Prefix is not the same as Auto Generated Prefix"),
				mk.ErrCodePrefixDoesNotMatchAutoGeneratedPrefix)
		}
	}

	updatedPrefix, err := pa.PrefixDao.Put(ctx, r.ChannelId, r.Prefix, prefixMetadata.editState)
	if err != nil {
		validateErr := validatePutResponse(err)
		if validateErr != nil {
			return nil, validateErr
		}

		logger.WithError(err).Error("Failed to put DAO prefix on submit")
		return nil, twirp.InternalError("Internal failure updating prefix for channel")
	}

	err = pa.postToSnsTopicForEmotesUpdates(ctx, r.ChannelId, r.Prefix, prefixMetadata.editState, updatedPrefix)
	if err != nil {
		logger.WithError(err).Error("Failed to emit to SNS topic for emote updates")
		return nil, twirp.InternalError("Internal failure updating emotes with prefix change")
	}

	return &mk.SubmitPrefixResponse{}, nil
}

func (pa *prefixesAPI) validatePrefix(ctx context.Context, prefix string, channelId string) error {
	if len(prefix) < mako.MinPrefixLength || len(prefix) > mako.MaxPrefixLength {
		return mk.NewClientError(
			twirp.InvalidArgumentError("prefix", "must be between 3 and 10 characters"),
			mk.ErrCodeInvalidPrefixLength)
	}

	// Make sure prefix begins with a letter and consists of only lowercase letters and numbers
	matched, err := regexp.MatchString(`\A[a-z][0-9a-z]+\z`, prefix)
	if err != nil {
		return e.Wrap(err, "matching regex")
	}
	if !matched {
		return mk.NewClientError(
			twirp.InvalidArgumentError("prefix", "must start with a letter and only contain letters or numbers"),
			mk.ErrCodeInvalidPrefixRegex)
	}

	existing, err := pa.PrefixDao.Get(ctx, prefix)
	if err != nil {
		return e.Wrap(err, "checking if prefix exists")
	}
	// if the submitted prefix matches the current prefix AND is not already assigned to the user, return non-unique error
	if existing != nil && existing.OwnerId != channelId {
		return mk.NewClientError(
			twirp.InvalidArgumentError("prefix", "already exists"),
			mk.ErrCodePrefixNotUnique)
	}
	return nil
}

func (pa *prefixesAPI) getPrefixIsEditable(ctx context.Context, channelId string) (*prefixMetadata, error) {

	payoutType, err := pa.Clients.RipleyClient.GetPayoutType(ctx, channelId)
	if err != nil {
		return nil, errors.New("failed to get user payout type when submiting prefix")
	}

	if !payoutType.IsAffiliate && !payoutType.IsPartner {
		return &prefixMetadata{
			notEditableReason: mk.NewClientError(
				twirp.NewError(twirp.FailedPrecondition, "user must be partner or affiliate"),
				mk.ErrCodeInvalidUser),
			isEditable: false,
		}, nil
	}

	inGoodStanding := false

	if payoutType.IsPartner {
		partnerInGoodStanding, err := pa.Clients.SalesforceClient.InGoodStanding(ctx, channelId)
		if err != nil {
			return nil, errors.New("getting partner standing")
		}
		inGoodStanding = partnerInGoodStanding != nil && *partnerInGoodStanding
	}

	metadata := &prefixMetadata{
		isPartner: payoutType.IsPartner,
	}

	prefix, err := pa.PrefixDao.GetByOwnerId(ctx, channelId)

	if err != nil {
		return nil, errors.New("error getting existing prefix")
	}

	if prefix == nil {
		prefix = &dynamo.Prefix{
			Prefix:  "",
			State:   dynamo.PrefixUnset,
			OwnerId: channelId,
		}
	}

	switch prefix.State {
	case dynamo.PrefixRejected:
		// If the existing prefix was rejected it is always editable but also the replacement will always go through manual approval
		// regardless of the user's good standing status.
		metadata.isEditable = true
		metadata.editState = dynamo.PrefixPending
		return metadata, nil
	case dynamo.PrefixUnset:
		// If the existing prefix is unset it is always editable. Manual approval will depend on the partner's good standing status
		// An affiliate prefix should never be in unset status.
		if inGoodStanding {
			metadata.editState = dynamo.PrefixActive
		} else {
			metadata.editState = dynamo.PrefixPending
		}
		metadata.isEditable = true
		return metadata, nil
	case dynamo.PrefixActive:
		// If the user is on the testing override list, their prefix should be editable regardless of the requirements defined below
		if override := pa.Config.PrefixEditabilityOverrideList[channelId]; override {
			metadata.isEditable = true
			metadata.editState = dynamo.PrefixActive
			return metadata, nil
		}

		// Prefix was previously set, only allow updating if the user meets the requirements according to their partner/affiliate status
		lastUpdated := prefix.LastUpdated

		// Partners may only change their prefix if:
		// 1) they are in good standing
		// 2) it has been more than 60 days since their prefix was last updated.
		if payoutType.IsPartner {
			if !inGoodStanding {
				metadata.isEditable = false
				metadata.notEditableReason = mk.NewClientError(
					twirp.NewError(twirp.FailedPrecondition, "partner is not in good standing"),
					mk.ErrCodeNotInGoodStanding)
				return metadata, nil
			}

			if time.Now().Before(lastUpdated.AddDate(0, 0, 60)) {
				metadata.notEditableReason = mk.NewClientError(
					twirp.NewError(twirp.FailedPrecondition, "prefix can only be updated once every 60 days"),
					mk.ErrCodePrefixUpdateTooSoon)
				metadata.isEditable = false
				return metadata, nil
			}

		} else if payoutType.IsAffiliate {
			canAffiliateUpdate, err := pa.canAffiliateUpdate(ctx, channelId, lastUpdated)

			if err != nil {
				return nil, err
			}

			if !canAffiliateUpdate {
				metadata.notEditableReason = mk.NewClientError(
					twirp.NewError(twirp.FailedPrecondition, "Prefix can be updated once after every username change"),
					mk.ErrCodeAffiliatePrefixUpdate)
				metadata.isEditable = false
				return metadata, nil
			}
		}
		metadata.editState = dynamo.PrefixActive
		metadata.isEditable = true
		return metadata, nil

	case dynamo.PrefixUnknown:
		return nil, errors.New("cannot get existing prefix state on submit")
	}

	metadata.editState = dynamo.PrefixActive
	metadata.isEditable = true
	return metadata, nil
}

// Affiliates can only edit their prefix if:
// 1) the user has changed their name more recently than the last time the prefix was updated
func (pa *prefixesAPI) canAffiliateUpdate(ctx context.Context, userID string, prefixLastUpdatedOn time.Time) (bool, error) {
	user, err := pa.Clients.UserServiceClient.GetUserByID(ctx, userID, nil)
	if err != nil {
		return false, err
	}
	if user == nil || user.Login == nil {
		return false, fmt.Errorf("userService did not return any results for user: %s", userID)
	}
	if user.LastLoginChangeDate == nil || user.CreatedOn == nil {
		return false, nil
	}
	if user.LastLoginChangeDate.After(*user.CreatedOn) && prefixLastUpdatedOn.Before(*user.LastLoginChangeDate) {
		return true, nil
	}
	return false, nil
}

func (pa *prefixesAPI) postToSnsTopicForEmotesUpdates(ctx context.Context, channelId, prefix string, state dynamo.PrefixState, previousPrefix *dynamo.Prefix) error {
	message := model.PrefixUpdateMessage{
		ChannelID:            channelId,
		NewPrefix:            prefix,
		AlreadyUpdatedPrefix: true,
		State:                string(state),
	}

	if previousPrefix != nil {
		message.PreviousPrefix = previousPrefix.Prefix
	}

	messageJson, err := json.Marshal(message)
	if err != nil {
		return err
	}

	return pa.Clients.SnsClient.PostToTopic(ctx, pa.Config.PrefixUpdatesSNS, string(messageJson))
}

func (pa *prefixesAPI) publishToDart(ctx context.Context, channelID, prefix string, state dynamo.PrefixState) error {
	// only send event if the new state is 'active' or 'rejected'
	if state != dynamo.PrefixActive && state != dynamo.PrefixRejected {
		// dont notify for other prefix states
		return nil
	}

	_, err := pa.Clients.DartReceiverClient.PublishNotification(ctx, &receiver.PublishNotificationRequest{
		Recipient:        &receiver.PublishNotificationRequest_RecipientId{RecipientId: channelID},
		NotificationType: emotePrefixReviewedNotificationType,
		Metadata: map[string]string{
			"state":  string(state),
			"prefix": prefix,
		},
	})

	return err
}

func (pa *prefixesAPI) GetPrefix(ctx context.Context, r *mk.GetPrefixRequest) (*mk.GetPrefixResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, 2*time.Second)
	defer cancel()

	if r == nil {
		return nil, twirp.InvalidArgumentError("request", "cannot be nil")
	}

	if gogogadget_strings.Blank(r.GetChannelId()) {
		return nil, twirp.InvalidArgumentError("channelId", "cannot be empty")
	}

	log.WithField("channelID", r.ChannelId).Info("Get Prefix Call")

	prefix, err := pa.PrefixDao.GetByOwnerId(ctx, r.ChannelId)
	if err != nil {
		log.WithFields(log.Fields{"channelId": r.ChannelId}).WithError(err).Error("Failed to get prefix for channel")
		return nil, twirp.InternalError("Internal failure getting prefix for channel")
	}

	if prefix == nil {
		return &mk.GetPrefixResponse{}, nil // no prefix set for the user.
	}
	isEditable := false

	if r.ShouldCheckEditability {
		prefixMetadata, err := pa.getPrefixIsEditable(ctx, r.ChannelId)

		if err != nil || prefixMetadata == nil {
			// on ripley/salesforce failure, we'll tell them it's not editable and log stuff
			log.WithFields(log.Fields{"channelId": r.ChannelId}).WithError(err).Error("Failed to get prefix editable status for channel")
		} else {
			isEditable = prefixMetadata.isEditable
		}
	}

	return &mk.GetPrefixResponse{
		Prefix:     prefix.Prefix,
		State:      dbStateToTwirpState(prefix.State),
		IsEditable: isEditable,
	}, nil
}

func (pa *prefixesAPI) GetRecommendedPrefix(ctx context.Context, r *mk.GetRecommendedPrefixRequest) (*mk.GetRecommendedPrefixResponse, error) {
	// TODO: Once we fix thereal prefix problem - reduce this timeout back down
	ctx, cancel := context.WithTimeout(ctx, 10*time.Second)
	defer cancel()

	if r == nil {
		return nil, twirp.InvalidArgumentError("request", "cannot be nil")
	}

	if gogogadget_strings.Blank(r.GetChannelId()) {
		return nil, twirp.InvalidArgumentError("channelId", "cannot be empty")
	}

	startTime := time.Now()
	recommendation, err := pa.Recommender.GetRecommendedEmoticonPrefix(ctx, r.ChannelId)
	if err != nil {
		log.WithFields(log.Fields{"channelId": r.ChannelId}).WithError(err).Error("Failed to generate prefix for channel")
		return nil, twirp.InternalError("Internal failure generating a recommended prefix")
	} else {
		log.WithFields(log.Fields{
			"channelId": r.GetChannelId(),
			"runTime":   time.Since(startTime),
			"prefix":    recommendation,
		}).Info("Successfully generated a recommended prefix for a channel")
	}

	return &mk.GetRecommendedPrefixResponse{Prefix: recommendation}, nil
}

func (pa *prefixesAPI) GetPrefixesByState(ctx context.Context, r *mk.GetPrefixesByStateRequest) (*mk.GetPrefixesByStateResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, time.Second)
	defer cancel()

	if r == nil {
		return nil, twirp.InvalidArgumentError("request", "cannot be nil")
	}

	if r.State == mk.PrefixState_PREFIX_UNKNOWN {
		log.WithField("req", *r).Info("couldn't get state from request")
		return nil, twirp.InvalidArgumentError("state", "cannot be empty")
	}

	if r.Limit <= 0 {
		r.Limit = defaultPrefixesResponseLimit
	}

	logger := log.WithFields(log.Fields{
		"state": r.State,
		"limit": r.Limit,
	})

	prefixes, err := pa.PrefixDao.GetByState(ctx, twirpStateToDbState(r.State), int(r.Limit))
	if err != nil {
		logger.WithError(err).Error("Failed to GetByState DAO prefix")
		return nil, twirp.InternalError("Internal failure getting prefixes by state")
	}

	entries := convertDynamoPrefixesToTwirpPrefixEntries(prefixes)
	return &mk.GetPrefixesByStateResponse{
		PrefixEntries: entries,
	}, nil
}

func (pa *prefixesAPI) DeletePrefix(ctx context.Context, prefixBeforeDelete string, userID string) error {
	return pa.PrefixDao.Delete(ctx, prefixBeforeDelete, userID)
}

func convertDynamoPrefixesToTwirpPrefixEntries(dynamoPrefixes []*dynamo.Prefix) []*mk.PrefixEntry {
	resp := []*mk.PrefixEntry{}
	for _, dynamoPrefix := range dynamoPrefixes {
		resp = append(resp, &mk.PrefixEntry{
			ChannelId: dynamoPrefix.OwnerId,
			Prefix:    dynamoPrefix.Prefix,
			State:     dbStateToTwirpState(dynamoPrefix.State),
		})
	}
	return resp
}

func validatePutResponse(putError error) error {
	if putError == dynamo.PrefixAlreadyTaken {
		return mk.NewClientError(
			twirp.InvalidArgumentError("prefix", "already taken"),
			dynamo.PrefixAlreadyTaken)
	}

	return nil
}

func twirpStateToDbState(state mk.PrefixState) dynamo.PrefixState {
	switch state {
	case mk.PrefixState_PREFIX_UNSET:
		return dynamo.PrefixUnset
	case mk.PrefixState_PREFIX_ACTIVE:
		return dynamo.PrefixActive
	case mk.PrefixState_PREFIX_REJECTED:
		return dynamo.PrefixRejected
	case mk.PrefixState_PREFIX_PENDING:
		return dynamo.PrefixPending
	case mk.PrefixState_PREFIX_UNKNOWN:
	default:
		return dynamo.PrefixUnknown
	}
	return dynamo.PrefixUnknown
}

func dbStateToTwirpState(state dynamo.PrefixState) mk.PrefixState {
	switch state {
	case dynamo.PrefixUnset:
		return mk.PrefixState_PREFIX_UNSET
	case dynamo.PrefixActive:
		return mk.PrefixState_PREFIX_ACTIVE
	case dynamo.PrefixRejected:
		return mk.PrefixState_PREFIX_REJECTED
	case dynamo.PrefixPending:
		return mk.PrefixState_PREFIX_PENDING
	case dynamo.PrefixUnknown:
	default:
		return mk.PrefixState_PREFIX_UNKNOWN
	}
	return mk.PrefixState_PREFIX_UNKNOWN
}
