package api

import (
	"context"
	"errors"
	"fmt"
	"testing"
	"time"

	"code.justin.tv/commerce/mako/clients"
	mako "code.justin.tv/commerce/mako/internal"
	config_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/config"
	"code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/internal_mock"
	mako_client "code.justin.tv/commerce/mako/twirp"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestCreate3PEmoteEntitlementsAPI(t *testing.T) {
	Convey("Test Create3PEmoteEntitlementsAPI", t, func() {
		mockEntitlementsDB := new(internal_mock.EntitlementDB)
		mockDynamicConfig := new(config_mock.DynamicConfigClient)
		createEmoteEntitlementsAPI := &Create3PEmoteEntitlementsAPI{
			Clients: clients.Clients{
				EntitlementDB: mockEntitlementsDB,
			},
			DynamicConfig: mockDynamicConfig,
		}

		testContext := context.Background()
		testClientId := "this-is-a-test-client-id"
		testUserId1 := "267893713"
		testEmoteId1 := "11111111111"
		testEntitlement1 := mako_client.EmoteEntitlement3P{
			UserId:  testUserId1,
			EmoteId: testEmoteId1,
		}
		testUserId2 := "232889822"
		testEmoteId2 := "222222222222"
		testEntitlement2 := mako_client.EmoteEntitlement3P{
			UserId:  testUserId2,
			EmoteId: testEmoteId2,
		}

		Convey("GIVEN nil request THEN returns RequiredArgumentError", func() {
			mockDynamicConfig.On("GetThirdPartyEmoteEntitlementsAllowlistMap").Return(map[string][]string{})

			resp, err := createEmoteEntitlementsAPI.Create3PEmoteEntitlements(testContext, nil)

			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
			So(err.Error(), ShouldContainSubstring, "invalid_argument")
			So(err.Error(), ShouldContainSubstring, "request is required")
		})

		Convey("GIVEN empty client id THEN returns RequiredArgumentError", func() {
			mockDynamicConfig.On("GetThirdPartyEmoteEntitlementsAllowlistMap").Return(map[string][]string{})

			resp, err := createEmoteEntitlementsAPI.Create3PEmoteEntitlements(testContext, &mako_client.Create3PEmoteEntitlementsRequest{})

			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
			So(err.Error(), ShouldContainSubstring, "invalid_argument")
			So(err.Error(), ShouldContainSubstring, "client_id is required")
		})

		Convey("GIVEN request client id is not in allowlist THEN returns PermissionDeniedError", func() {
			mockDynamicConfig.On("GetThirdPartyEmoteEntitlementsAllowlistMap").Return(map[string][]string{
				"allowed-client-id": {testEmoteId1},
			})

			resp, err := createEmoteEntitlementsAPI.Create3PEmoteEntitlements(testContext, &mako_client.Create3PEmoteEntitlementsRequest{
				ClientId:     testClientId,
				Entitlements: []*mako_client.EmoteEntitlement3P{&testEntitlement1},
			})

			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
			So(err.Error(), ShouldContainSubstring, "permission_denied")
			So(err.Error(), ShouldContainSubstring, "not authorized to call Create3PEmoteEntitlements")
		})

		Convey("GIVEN request client id is in allowlist", func() {
			mockDynamicConfig.On("GetThirdPartyEmoteEntitlementsAllowlistMap").Return(map[string][]string{
				testClientId: {testEmoteId1, testEmoteId2},
			})

			Convey("GIVEN list of entitlements is empty THEN returns RequiredArgumentError", func() {
				resp, err := createEmoteEntitlementsAPI.Create3PEmoteEntitlements(testContext, &mako_client.Create3PEmoteEntitlementsRequest{
					ClientId:     testClientId,
					Entitlements: []*mako_client.EmoteEntitlement3P{},
				})

				So(resp, ShouldBeNil)
				So(err, ShouldNotBeNil)
				So(err.Error(), ShouldContainSubstring, "invalid_argument")
				So(err.Error(), ShouldContainSubstring, "entitlements is required")
			})

			Convey("GIVEN one of the entitlements is nil THEN returns InvalidArgumentError", func() {
				resp, err := createEmoteEntitlementsAPI.Create3PEmoteEntitlements(testContext, &mako_client.Create3PEmoteEntitlementsRequest{
					ClientId: testClientId,
					Entitlements: []*mako_client.EmoteEntitlement3P{
						&testEntitlement1,
						nil,
					},
				})

				So(resp, ShouldBeNil)
				So(err, ShouldNotBeNil)
				So(err.Error(), ShouldContainSubstring, "invalid_argument")
				So(err.Error(), ShouldContainSubstring, "entitlement cannot be empty")
			})

			Convey("GIVEN an entitlement doesn't contain a user id THEN returns RequiredArgumentError", func() {
				resp, err := createEmoteEntitlementsAPI.Create3PEmoteEntitlements(testContext, &mako_client.Create3PEmoteEntitlementsRequest{
					ClientId: testClientId,
					Entitlements: []*mako_client.EmoteEntitlement3P{
						&testEntitlement1,
						{
							EmoteId: testEmoteId2,
						},
					},
				})

				So(resp, ShouldBeNil)
				So(err, ShouldNotBeNil)
				So(err.Error(), ShouldContainSubstring, "invalid_argument")
				So(err.Error(), ShouldContainSubstring, "user_id is required")
			})

			Convey("GIVEN an entitlement doesn't contain an emote id THEN returns RequiredArgumentError", func() {
				resp, err := createEmoteEntitlementsAPI.Create3PEmoteEntitlements(testContext, &mako_client.Create3PEmoteEntitlementsRequest{
					ClientId: testClientId,
					Entitlements: []*mako_client.EmoteEntitlement3P{
						&testEntitlement1,
						{
							UserId: testUserId2,
						},
					},
				})

				So(resp, ShouldBeNil)
				So(err, ShouldNotBeNil)
				So(err.Error(), ShouldContainSubstring, "invalid_argument")
				So(err.Error(), ShouldContainSubstring, "emote_id is required")
			})

			Convey("GIVEN the request client id doesn't have permission to entitle a given emote THEN returns PermissionDeniedError", func() {
				resp, err := createEmoteEntitlementsAPI.Create3PEmoteEntitlements(testContext, &mako_client.Create3PEmoteEntitlementsRequest{
					ClientId: testClientId,
					Entitlements: []*mako_client.EmoteEntitlement3P{
						{
							UserId:  testUserId1,
							EmoteId: "this-id-not-in-allowlist",
						},
					},
				})

				So(resp, ShouldBeNil)
				So(err, ShouldNotBeNil)
				So(err.Error(), ShouldContainSubstring, "permission_denied")
				So(err.Error(), ShouldContainSubstring, "not authorized to entitle emote id")
			})

			Convey("GIVEN a valid and authorized request", func() {
				validRequest := mako_client.Create3PEmoteEntitlementsRequest{
					ClientId: testClientId,
					Entitlements: []*mako_client.EmoteEntitlement3P{
						&testEntitlement1,
						&testEntitlement2,
					},
				}

				Convey("THEN EntitlementsDB WriteEntitlements is called with expected entitlements", func() {
					entitlementEndTime := time.Date(2020, time.Month(12), 23, 8, 0, 0, 0, time.UTC)

					mockEntitlementsDB.On("WriteEntitlements", mock.Anything, mock.Anything, mock.Anything).Return(nil)

					createEmoteEntitlementsAPI.Create3PEmoteEntitlements(testContext, &validRequest)

					So(mockEntitlementsDB.Calls[0].Method, ShouldEqual, "WriteEntitlements")

					entitlementsArgument1 := mockEntitlementsDB.Calls[0].Arguments.Get(1).(mako.Entitlement)
					entitlementsArgument2 := mockEntitlementsDB.Calls[0].Arguments.Get(2).(mako.Entitlement)
					So(entitlementsArgument1.Type, ShouldEqual, mako.EmoteEntitlement)
					So(entitlementsArgument2.Type, ShouldEqual, mako.EmoteEntitlement)
					So(entitlementsArgument1.Source, ShouldEqual, mako.RewardsSource)
					So(entitlementsArgument2.Source, ShouldEqual, mako.RewardsSource)
					So(entitlementsArgument1.ChannelID, ShouldEqual, channelIdQaTwitchPartner)
					So(entitlementsArgument2.ChannelID, ShouldEqual, channelIdQaTwitchPartner)
					So(entitlementsArgument1.OriginID, ShouldEqual, fmt.Sprintf(originIdThirdPartyPrefix, testClientId))
					So(entitlementsArgument2.OriginID, ShouldEqual, fmt.Sprintf(originIdThirdPartyPrefix, testClientId))
					So(entitlementsArgument1.ID, ShouldEqual, testEmoteId1)
					So(entitlementsArgument2.ID, ShouldEqual, testEmoteId2)
					So(entitlementsArgument1.OwnerID, ShouldEqual, testUserId1)
					So(entitlementsArgument2.OwnerID, ShouldEqual, testUserId2)
					So(entitlementsArgument1.End.String(), ShouldEqual, entitlementEndTime.String())
					So(entitlementsArgument2.End.String(), ShouldEqual, entitlementEndTime.String())
				})

				Convey("GIVEN EntitlementsDB WriteEntitlements returns an error THEN return InternalError", func() {
					mockEntitlementsDB.On("WriteEntitlements", mock.Anything, mock.Anything, mock.Anything).Return(errors.New("dummy entitlementsdb error"))

					resp, err := createEmoteEntitlementsAPI.Create3PEmoteEntitlements(testContext, &validRequest)

					So(resp, ShouldBeNil)
					So(err, ShouldNotBeNil)
					So(err.Error(), ShouldContainSubstring, "twirp error internal")
					So(err.Error(), ShouldContainSubstring, "failed to create 3P emote entitlement")
				})

				Convey("GIVEN EntitlementsDB WriteEntitlements returns success THEN return empty response with no error", func() {
					mockEntitlementsDB.On("WriteEntitlements", mock.Anything, mock.Anything, mock.Anything).Return(nil)

					resp, err := createEmoteEntitlementsAPI.Create3PEmoteEntitlements(testContext, &validRequest)

					So(resp, ShouldNotBeNil)
					So(err, ShouldBeNil)
				})
			})
		})
	})
}
