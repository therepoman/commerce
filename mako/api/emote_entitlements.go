package api

import (
	"context"
	"errors"
	"time"

	materiatwirp "code.justin.tv/amzn/MateriaTwirp"
	mk "code.justin.tv/commerce/mako/twirp"

	"code.justin.tv/commerce/mako/clients"
	"code.justin.tv/commerce/mako/config"
	"github.com/twitchtv/twirp"
)

type EmoteEntitlementsAPI struct {
	Clients clients.Clients
	Config  *config.Configuration
}

func (e *EmoteEntitlementsAPI) GetEmoteEntitlements(ctx context.Context, r *mk.GetEmoteEntitlementsRequest) (*mk.GetEmoteEntitlementsResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, 3*time.Second)
	defer cancel()

	if len(r.OwnerId) == 0 {
		return nil, twirp.InternalErrorWith(errors.New("need to have a owner_id"))
	}

	var source materiatwirp.Source
	switch r.Group {
	case mk.EmoteGroup_ChannelPoints:
		source = materiatwirp.Source_CHANNEL_POINTS
	case mk.EmoteGroup_Rewards:
		source = materiatwirp.Source_REWARDS
	case mk.EmoteGroup_Subscriptions:
		source = materiatwirp.Source_SUBS
	case mk.EmoteGroup_HypeTrain:
		source = materiatwirp.Source_HYPE_TRAIN
	case mk.EmoteGroup_MegaCommerce:
		source = materiatwirp.Source_MEGA_COMMERCE
	}

	resp, err := e.Clients.MateriaClient.GetEntitlements(ctx, &materiatwirp.GetEntitlementsRequest{
		Domain:   materiatwirp.Domain_EMOTES,
		Source:   source,
		ItemId:   r.ItemId,
		OwnerId:  r.OwnerId,
		OriginId: r.OriginId,
	})
	if err != nil {
		return nil, twirp.InternalErrorWith(err)
	}

	entitlements := make([]*mk.EmoteEntitlement, 0)
	for _, e := range resp.Entitlements {
		if !isActive(*e) {
			continue
		}

		// Convert materia source to mako group
		var group mk.EmoteGroup
		switch e.Source {
		case materiatwirp.Source_CHANNEL_POINTS:
			group = mk.EmoteGroup_ChannelPoints
		case materiatwirp.Source_SUBS:
			group = mk.EmoteGroup_Subscriptions
		case materiatwirp.Source_REWARDS:
			group = mk.EmoteGroup_Rewards
		case materiatwirp.Source_HYPE_TRAIN:
			group = mk.EmoteGroup_HypeTrain
		case materiatwirp.Source_MEGA_COMMERCE:
			group = mk.EmoteGroup_MegaCommerce
		}

		entitlement := &mk.EmoteEntitlement{
			OwnerId:     e.OwnerId,
			ItemOwnerId: e.ItemOwnerId,
			EmoteId:     e.ItemId,
			OriginId:    e.OriginId,
			Start:       e.StartsAt,
			Group:       group,
			End: &mk.InfiniteTime{
				Time:       e.EndsAt.Time,
				IsInfinite: e.EndsAt.IsInfinite,
			},
		}

		entitlements = append(entitlements, entitlement)
	}

	return &mk.GetEmoteEntitlementsResponse{Entitlements: entitlements}, nil
}

func isActive(entitlement materiatwirp.Entitlement) bool {
	now := time.Now()

	startsAt := entitlement.StartsAt.AsTime()

	if now.Before(startsAt) {
		return false
	}

	if entitlement.EndsAt.IsInfinite {
		return true
	}

	endsAt := entitlement.EndsAt.Time.AsTime()

	return now.Before(endsAt)
}
