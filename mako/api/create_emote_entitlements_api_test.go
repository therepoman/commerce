package api

import (
	"context"
	"testing"

	"code.justin.tv/commerce/mako/clients"
	"code.justin.tv/commerce/mako/config"
	materia_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/materia"
	pubsub_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/pubsub"
	mk "code.justin.tv/commerce/mako/twirp"
	. "github.com/smartystreets/goconvey/convey"
)

func TestCreateEmoteEntitlements(t *testing.T) {
	Convey("Given CreateEmoteEntitlementsAPI", t, func() {
		mockMateria := new(materia_mock.MateriaClient)
		mockPubsub := new(pubsub_mock.IPublisher)

		createEmoteEntitlement := CreateEmoteEntitlementsAPI{
			Clients: clients.Clients{
				MateriaClient: mockMateria,
				Pubsub:        mockPubsub,
			},
			Config: &config.Configuration{
				CommunityPointsEmoteSetID: "300000028",
			},
		}

		Convey("Test CreateEmoteEntitlement", func() {
			Convey("With empty OwnerId", func() {
				req := &mk.CreateEmoteEntitlementsRequest{
					Entitlements: []*mk.EmoteEntitlement{
						{
							OriginId: "1",
							EmoteId:  "2",
							End: &mk.InfiniteTime{
								IsInfinite: true,
							},
						},
					},
				}
				_, err := createEmoteEntitlement.CreateEmoteEntitlements(context.Background(), req)
				So(err, ShouldNotBeNil)
			})

			Convey("With empty EmoteId", func() {
				req := &mk.CreateEmoteEntitlementsRequest{
					Entitlements: []*mk.EmoteEntitlement{
						{
							OriginId: "1",
							OwnerId:  "2",
							End: &mk.InfiniteTime{
								IsInfinite: true,
							},
						},
					},
				}
				_, err := createEmoteEntitlement.CreateEmoteEntitlements(context.Background(), req)
				So(err, ShouldNotBeNil)
			})

			Convey("With empty OriginId", func() {
				req := &mk.CreateEmoteEntitlementsRequest{
					Entitlements: []*mk.EmoteEntitlement{
						{
							EmoteId: "2",
							OwnerId: "1",
							End: &mk.InfiniteTime{
								IsInfinite: true,
							},
						},
					},
				}
				_, err := createEmoteEntitlement.CreateEmoteEntitlements(context.Background(), req)
				So(err, ShouldNotBeNil)
			})

			Convey("With empty End", func() {
				req := &mk.CreateEmoteEntitlementsRequest{
					Entitlements: []*mk.EmoteEntitlement{
						{
							OriginId: "1",
							EmoteId:  "2",
						},
					},
				}
				_, err := createEmoteEntitlement.CreateEmoteEntitlements(context.Background(), req)
				So(err, ShouldNotBeNil)
			})
		})
	})
}
