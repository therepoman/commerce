package api

import (
	"context"
	"sort"
	"testing"

	"code.justin.tv/commerce/mako/clients"
	"code.justin.tv/commerce/mako/clients/emoticons"
	"code.justin.tv/commerce/mako/config"
	mako "code.justin.tv/commerce/mako/internal"
	emoticons_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/emoticons"
	config_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/config"
	internal_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/internal_mock"
	client_emoticons_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/mako/clients/emoticons"
	mk "code.justin.tv/commerce/mako/twirp"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

const (
	userID    = "123"
	channelID = "channel_id"
)

func TestEmoteSetDetailsAPI(t *testing.T) {
	Convey("Test Emote Set Details API", t, func() {
		mockEntitlementsDBEmoticons := new(client_emoticons_mock.IEmoticons)
		mockMateriaEmoticons := new(emoticons_mock.IMateriaEmoticons)
		mockCampaignEmoteOwners := new(config_mock.CampaignEmoteOwners)
		mockEmoteModifiersDB := new(internal_mock.EmoteModifiersDB)
		mockEmotesDB := new(internal_mock.EmoteDB)
		mockFollowerEntitlementsDB := new(internal_mock.EntitlementDB)
		mockDynamicConfig := new(config_mock.DynamicConfigClient)
		emoticonsAggregatorConfig := emoticons.EmoticonAggregatorConfig{
			EntitlementsDBEmoticons: mockEntitlementsDBEmoticons,
			MateriaEmoticons:        mockMateriaEmoticons,
			EmoteDB:                 mockEmotesDB,
			FollowerEntitlementsDB:  mockFollowerEntitlementsDB,
			Statter:                 &statsd.NoopClient{},
			DynamicConfig:           mockDynamicConfig,
		}
		emoticonsAggregator := emoticons.NewEmoticonAggregator(emoticonsAggregatorConfig)

		emoteSetDetailsAPI := EmoteSetDetailsAPI{
			Clients: clients.Clients{
				EntitlementsDBEmoticons: mockEntitlementsDBEmoticons,
				EmoticonsAggregator:     emoticonsAggregator,
				EmoteModifiersDB:        mockEmoteModifiersDB,
			},
			CampaignConfig: mockCampaignEmoteOwners,
			Config: &config.Configuration{
				MateriaDenylist: map[string]bool{
					"nightbot": true,
				},
			},
		}

		Convey("Test GetEmoteSetDetails API", func() {
			Convey("With entitlementsDB emoticons", func() {
				testEmote := &mk.Emote{Id: "234", Pattern: "Kappa"}

				testEmoteSet := &mk.EmoteSet{
					Id: "2",
					Emotes: []*mk.Emote{
						testEmote,
					},
					ChannelId: "345",
				}
				testEmoteSetNoChannelID := &mk.EmoteSet{
					Id: "2",
					Emotes: []*mk.Emote{
						testEmote,
					},
					ChannelId: "",
				}

				emotesResp := &mk.GetEmoteSetDetailsResponse{
					EmoteSets: []*mk.EmoteSet{
						testEmoteSetNoChannelID,
					},
				}

				mockMateriaEmoticons.On("EmoticonsByGroup", mock.Anything, mock.Anything).Return(map[string]mk.EmoteSet{}, nil)
				mockEntitlementsDBEmoticons.On("EmoticonsByGroup", mock.Anything, mock.Anything).Return(map[string][]*mk.Emote{testEmoteSet.Id: testEmoteSet.Emotes}, nil)
				mockCampaignEmoteOwners.On("CampaignEmoteOwner", mock.Anything).Return("")
				mockEmoteModifiersDB.On("ReadEntitledGroupsForUser", mock.Anything, userID).Return(nil, nil)

				request := &mk.GetEmoteSetDetailsRequest{
					UserId: userID,
				}

				// call and validate
				resp, err := emoteSetDetailsAPI.GetEmoteSetDetails(context.Background(), request)
				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
				So(len(resp.EmoteSets), ShouldEqual, 1)
				So(resp, ShouldResemble, emotesResp)
			})

			Convey("With materia and entitlementsDB emoticons", func() {
				testEmote := &mk.Emote{Id: "234", Pattern: "Kappa"}

				testEmoteSet := &mk.EmoteSet{
					Id: "2",
					Emotes: []*mk.Emote{
						testEmote,
					},
					ChannelId: "",
				}

				testEmote2 := &mk.Emote{Id: "456", Pattern: "SeemsMinton"}

				testEmoteSet2 := &mk.EmoteSet{
					Id: "3",
					Emotes: []*mk.Emote{
						testEmote2,
					},
					ChannelId: "",
				}

				emotesResp := &mk.GetEmoteSetDetailsResponse{
					EmoteSets: []*mk.EmoteSet{
						testEmoteSet,
						testEmoteSet2,
					},
				}

				mockEntitlementsDBEmoticons.On("EmoticonsByGroup", mock.Anything, mock.Anything).Return(map[string][]*mk.Emote{testEmoteSet2.Id: testEmoteSet2.Emotes}, nil)
				mockMateriaEmoticons.On("EmoticonsByGroup", mock.Anything, mock.Anything).Return(map[string]mk.EmoteSet{
					testEmoteSet.Id: *testEmoteSet,
				}, nil)
				mockCampaignEmoteOwners.On("CampaignEmoteOwner", mock.Anything).Return("")
				mockEmoteModifiersDB.On("ReadEntitledGroupsForUser", mock.Anything, userID).Return(nil, nil)

				request := &mk.GetEmoteSetDetailsRequest{
					UserId: userID,
				}

				// call and validate
				resp, err := emoteSetDetailsAPI.GetEmoteSetDetails(context.Background(), request)
				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
				So(len(resp.EmoteSets), ShouldEqual, 2)
				sort.Slice(resp.EmoteSets, func(i, j int) bool {
					return resp.EmoteSets[i].Id > resp.EmoteSets[j].Id
				})
				sort.Slice(emotesResp.EmoteSets, func(i, j int) bool {
					return emotesResp.EmoteSets[i].Id > emotesResp.EmoteSets[j].Id
				})
				So(resp, ShouldResemble, emotesResp)
			})

			Convey("With materia emoticons", func() {
				testEmote := &mk.Emote{Id: "234", Pattern: "Kappa"}

				testEmoteSet := mk.EmoteSet{
					Id:        "2",
					Emotes:    []*mk.Emote{testEmote},
					ChannelId: "1234567890",
				}

				emotesResp := &mk.GetEmoteSetDetailsResponse{
					EmoteSets: []*mk.EmoteSet{&testEmoteSet},
				}

				mockEntitlementsDBEmoticons.On("EmoticonsByGroup", mock.Anything, mock.Anything).Return(nil, nil)
				mockMateriaEmoticons.On("EmoticonsByGroup", mock.Anything, mock.Anything).Return(map[string]mk.EmoteSet{testEmoteSet.Id: testEmoteSet}, nil)
				mockCampaignEmoteOwners.On("CampaignEmoteOwner", mock.Anything).Return("")
				mockEmoteModifiersDB.On("ReadEntitledGroupsForUser", mock.Anything, userID).Return(nil, nil)

				Convey("and special events owner (megacommerce, hypetrain)", func() {
					emoteSetDetailsAPI.Config = &config.Configuration{
						SpecialEventOwnerIDs: map[string]bool{"2": true},
					}
					emotesResp.EmoteSets[0].ChannelId = "2"

					request := &mk.GetEmoteSetDetailsRequest{
						UserId: userID,
					}

					// call and validate
					resp, err := emoteSetDetailsAPI.GetEmoteSetDetails(context.Background(), request)
					So(err, ShouldBeNil)
					So(resp, ShouldNotBeNil)
					So(len(resp.EmoteSets), ShouldEqual, 1)
					So(resp, ShouldResemble, emotesResp)
				})

			})

			Convey("With modified emotes enabled", func() {
				testUserID := userID
				testEmote := &mk.Emote{Id: "234", Pattern: "Kappa"}
				testChannelID := "1234"
				testGroupID := "2"
				testEmoteSet := mk.EmoteSet{
					Id:        testGroupID,
					Emotes:    []*mk.Emote{testEmote},
					ChannelId: testChannelID,
				}
				testBTEREmoteSet := mk.EmoteSet{
					Id:        "BTER",
					Emotes:    []*mk.Emote{{Id: "22", Pattern: "nope"}},
					ChannelId: testChannelID,
				}

				mockEntitlementsDBEmoticons.On("EmoticonsByGroup", mock.Anything, mock.Anything).Return(nil, nil)
				mockMateriaEmoticons.On("EmoticonsByGroup", mock.Anything, mock.Anything).Return(map[string]mk.EmoteSet{
					testEmoteSet.Id:     testEmoteSet,
					testBTEREmoteSet.Id: testBTEREmoteSet,
				}, nil)
				mockCampaignEmoteOwners.On("CampaignEmoteOwner", mock.Anything).Return("")

				expectedModifiersLength := 1

				Convey("with successful emotemodifiersdb call", func() {
					mockEmoteModifiersDB.On("ReadEntitledGroupsForUser", mock.Anything, userID).Return(
						[]mako.EmoteModifierGroup{
							{
								SourceEmoteGroupIDs: []string{testGroupID},
								Modifiers:           []string{"BW"},
								OwnerID:             testChannelID,
							},
						}, nil)
					expectedModifiersLength = 1
				})

				Convey("with special event based group", func() {
					emoteSetDetailsAPI.Config.SpecialEventOwnerIDs = map[string]bool{
						testChannelID: true,
					}

					mockEmoteModifiersDB.On("ReadEntitledGroupsForUser", mock.Anything, userID).Return(nil, nil)
					expectedModifiersLength = 0
				})

				Convey("with multiple emote modifiers only has entitled", func() {
					mockEmoteModifiersDB.On("ReadEntitledGroupsForUser", mock.Anything, userID).
						Return(
							[]mako.EmoteModifierGroup{
								{
									SourceEmoteGroupIDs: []string{testGroupID},
									Modifiers:           []string{"BW"},
									OwnerID:             testChannelID,
								},
								{
									SourceEmoteGroupIDs: []string{testGroupID, "bad"},
									Modifiers:           []string{"SG"},
									OwnerID:             testChannelID,
								},
							}, nil)
					expectedModifiersLength = 1
				})

				Convey("with denied user", func() {
					testUserID = "nightbot"
					expectedModifiersLength = 0
				})

				Convey("with cache read emotemodifiersdb error", func() {
					mockEmoteModifiersDB.On("ReadEntitledGroupsForUser", mock.Anything, userID).Return(nil, errors.New("oof"))
					mockEmoteModifiersDB.On("ReadByOwnerIDs", mock.Anything, testChannelID).Return([]mako.EmoteModifierGroup{
						{
							SourceEmoteGroupIDs: []string{testGroupID},
							Modifiers:           []string{"BW"},
							OwnerID:             testChannelID,
						},
					}, nil)
					mockEmoteModifiersDB.On("WriteEntitledGroupsForUser", mock.Anything, mock.Anything, mock.Anything).Return(nil)
					expectedModifiersLength = 1
				})

				Convey("with all emotemodifiersdb cache errors", func() {
					mockEmoteModifiersDB.On("ReadEntitledGroupsForUser", mock.Anything, userID).Return(nil, errors.New("oof"))
					mockEmoteModifiersDB.On("ReadByOwnerIDs", mock.Anything, testChannelID).Return(nil, errors.New("oof"))
					expectedModifiersLength = 0
				})

				Convey("with animated emote", func() {
					testAnimatedGroupID := "99"
					mockEmoteModifiersDB.On("ReadEntitledGroupsForUser", mock.Anything, userID).Return(
						[]mako.EmoteModifierGroup{
							{
								SourceEmoteGroupIDs: []string{testAnimatedGroupID},
								Modifiers:           []string{"BW"},
								OwnerID:             testChannelID,
							},
						}, nil)
					testAnimatedEmote := &mk.Emote{Id: "234", Pattern: "Kappa", AssetType: mk.EmoteAssetType_animated}
					testEmoteSetWithAnimatedEmote := mk.EmoteSet{
						Id:        testAnimatedGroupID,
						Emotes:    []*mk.Emote{testAnimatedEmote},
						ChannelId: testChannelID,
					}
					mockMateriaEmoticons.On("EmoticonsByGroup", mock.Anything, mock.Anything).Return(map[string]mk.EmoteSet{
						testEmoteSet.Id: testEmoteSetWithAnimatedEmote,
					}, nil)
					expectedModifiersLength = 0
				})

				request := &mk.GetEmoteSetDetailsRequest{
					UserId: testUserID,
				}

				resp, err := emoteSetDetailsAPI.GetEmoteSetDetails(context.Background(), request)
				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
				So(len(resp.EmoteSets), ShouldEqual, 2)

				respEmoteSet := resp.EmoteSets[0]
				respBTEREmoteSet := resp.EmoteSets[1]
				if respEmoteSet.Id != testEmoteSet.Id {
					respEmoteSet = resp.EmoteSets[1]
					respBTEREmoteSet = resp.EmoteSets[0]
				}
				So(len(respEmoteSet.Emotes[0].Modifiers), ShouldEqual, expectedModifiersLength)
				if expectedModifiersLength > 0 {
					// First modifier should always be BlackWhite
					So(respEmoteSet.Emotes[0].Modifiers[0], ShouldEqual, mk.Modification_BlackWhite)
				}
				So(len(respBTEREmoteSet.Emotes[0].Modifiers), ShouldEqual, 0)
				So(respEmoteSet, ShouldResemble, &testEmoteSet)
				So(respBTEREmoteSet, ShouldResemble, &testBTEREmoteSet)
			})

			Convey("With duplicated emoticons", func() {
				testEmote := &mk.Emote{Id: "234", Pattern: "Kappa"}
				testEmoteDuplicatePattern := &mk.Emote{Id: "233", Pattern: "Kappa"}

				testEmoteSet := &mk.EmoteSet{
					Id: "2",
					Emotes: []*mk.Emote{
						testEmote,
					},
					ChannelId: "",
				}

				testEmote2 := &mk.Emote{Id: "456", Pattern: "SeemsMinton"}

				testEmoteSet2 := &mk.EmoteSet{
					Id: "3",
					Emotes: []*mk.Emote{
						testEmoteDuplicatePattern,
						testEmote2,
					},
					ChannelId: "",
				}

				cleanedTestEmoteSet2 := &mk.EmoteSet{
					Id: "3",
					Emotes: []*mk.Emote{
						testEmote2,
					},
					ChannelId: "",
				}

				emotesResp := &mk.GetEmoteSetDetailsResponse{
					EmoteSets: []*mk.EmoteSet{
						testEmoteSet,
						cleanedTestEmoteSet2,
					},
				}

				mockMateriaEmoticons.On("EmoticonsByGroup", mock.Anything, mock.Anything).Return(map[string]mk.EmoteSet{
					testEmoteSet.Id: *testEmoteSet,
				}, nil)
				mockEntitlementsDBEmoticons.On("EmoticonsByGroup", mock.Anything, mock.Anything).Return(map[string][]*mk.Emote{testEmoteSet2.Id: testEmoteSet2.Emotes}, nil)
				mockCampaignEmoteOwners.On("CampaignEmoteOwner", mock.Anything).Return("")
				mockEmoteModifiersDB.On("ReadEntitledGroupsForUser", mock.Anything, userID).Return([]mako.EmoteModifierGroup{}, nil)

				request := &mk.GetEmoteSetDetailsRequest{
					UserId: userID,
				}

				// call and validate
				resp, err := emoteSetDetailsAPI.GetEmoteSetDetails(context.Background(), request)
				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
				So(len(resp.EmoteSets), ShouldEqual, 2)
				sort.Slice(resp.EmoteSets, func(i, j int) bool {
					return resp.EmoteSets[i].Id > resp.EmoteSets[j].Id
				})
				sort.Slice(emotesResp.EmoteSets, func(i, j int) bool {
					return emotesResp.EmoteSets[i].Id > emotesResp.EmoteSets[j].Id
				})
				So(resp, ShouldResemble, emotesResp)
			})

			Convey("With entitlementsDB emoticons and value in config", func() {
				testEmote := &mk.Emote{Id: "234", Pattern: "Kappa"}

				testEmoteSet := &mk.EmoteSet{
					Id: "2",
					Emotes: []*mk.Emote{
						testEmote,
					},
					ChannelId: "345",
				}
				testEmoteSetWithChannelID := &mk.EmoteSet{
					Id: "2",
					Emotes: []*mk.Emote{
						testEmote,
					},
					ChannelId: "test_channel",
				}

				emotesResp := &mk.GetEmoteSetDetailsResponse{
					EmoteSets: []*mk.EmoteSet{
						testEmoteSetWithChannelID,
					},
				}

				mockMateriaEmoticons.On("EmoticonsByGroup", mock.Anything, mock.Anything).Return(map[string]mk.EmoteSet{}, nil)
				mockEntitlementsDBEmoticons.On("EmoticonsByGroup", mock.Anything, mock.Anything).Return(map[string][]*mk.Emote{testEmoteSet.Id: testEmoteSet.Emotes}, nil)
				mockCampaignEmoteOwners.On("CampaignEmoteOwner", "234").Return("test_channel")
				mockEmoteModifiersDB.On("ReadEntitledGroupsForUser", mock.Anything, userID).Return([]mako.EmoteModifierGroup{}, nil)

				request := &mk.GetEmoteSetDetailsRequest{
					UserId: userID,
				}

				// call and validate
				resp, err := emoteSetDetailsAPI.GetEmoteSetDetails(context.Background(), request)
				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
				So(len(resp.EmoteSets), ShouldEqual, 1)
				So(resp, ShouldResemble, emotesResp)
			})

			Convey("With follower emotes", func() {
				testEmote := mako.Emote{ID: "234", Code: "Kappa", EmotesGroup: "Follower"}
				emotesResp := &mk.GetEmoteSetDetailsResponse{
					EmoteSets: []*mk.EmoteSet{
						{
							Emotes: []*mk.Emote{
								{
									Id:        "234",
									Pattern:   "Kappa",
									Type:      mk.EmoteGroup_Follower,
									Modifiers: []mk.Modification{},
								},
							},
							ChannelId: channelID,
						},
					},
				}

				mockMateriaEmoticons.On("EmoticonsByGroup", mock.Anything, mock.Anything).Return(map[string]mk.EmoteSet{}, nil)
				mockEntitlementsDBEmoticons.On("EmoticonsByGroup", mock.Anything, mock.Anything).Return(map[string][]*mk.Emote{}, nil)
				mockCampaignEmoteOwners.On("CampaignEmoteOwner", "234").Return("test_channel")
				mockEmoteModifiersDB.On("ReadEntitledGroupsForUser", mock.Anything, userID).Return([]mako.EmoteModifierGroup{}, nil)
				mockDynamicConfig.On("IsInFreemotesExperiment", channelID).Return(true)
				mockFollowerEntitlementsDB.On("ReadByOwnerID", mock.Anything, userID, channelID).Return([]mako.Entitlement{{ID: testEmote.EmotesGroup}}, nil)
				mockEmotesDB.On("ReadByGroupIDs", mock.Anything, testEmote.EmotesGroup).Return([]mako.Emote{testEmote}, nil)

				request := &mk.GetEmoteSetDetailsRequest{
					UserId:    userID,
					ChannelId: channelID,
				}

				// call and validate
				resp, err := emoteSetDetailsAPI.GetEmoteSetDetails(context.Background(), request)
				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
				So(len(resp.EmoteSets), ShouldEqual, 1)
				So(len(resp.EmoteSets[0].Emotes), ShouldEqual, 1)
				// NOTE: We have to set CreatedAt to null because our comparison library times out when trying to render
				// timestamppb timestamps.
				resp.EmoteSets[0].Emotes[0].CreatedAt = nil
				So(resp, ShouldResemble, emotesResp)
			})
		})
	})
}
