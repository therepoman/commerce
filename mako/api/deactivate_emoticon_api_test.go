package api

import (
	"context"
	"testing"

	"code.justin.tv/commerce/mako/clients"
	emoticonsmanager_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/emoticonsmanager"
	mk "code.justin.tv/commerce/mako/twirp"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestDeactivateEmoticonAPI(t *testing.T) {
	Convey("Test DeactivateEmoticonAPI", t, func() {
		mockEmoticon := new(emoticonsmanager_mock.IEmoticonsManager)
		deactivateEmoticonAPI := &DeactivateEmoticonAPI{
			Clients: clients.Clients{
				EmoticonsManager: mockEmoticon,
			},
		}

		Convey("Test DeactivateEmoticon", func() {
			Convey("With empty id", func() {
				req := &mk.DeactivateEmoticonRequest{
					Id: "",
				}

				_, err := deactivateEmoticonAPI.DeactivateEmoticon(context.Background(), req)
				So(err, ShouldNotBeNil)
			})

			Convey("With error on emoticon deactivate", func() {
				req := &mk.DeactivateEmoticonRequest{
					Id: "1234",
				}
				mockEmoticon.On("Deactivate", mock.Anything, mock.Anything).Return(errors.New("test"))

				_, err := deactivateEmoticonAPI.DeactivateEmoticon(context.Background(), req)
				So(err, ShouldNotBeNil)
			})
			Convey("With Success", func() {
				req := &mk.DeactivateEmoticonRequest{
					Id: "1234",
				}
				mockEmoticon.On("Deactivate", mock.Anything, mock.Anything).Return(nil)

				_, err := deactivateEmoticonAPI.DeactivateEmoticon(context.Background(), req)
				So(err, ShouldBeNil)
			})
		})
	})
}
