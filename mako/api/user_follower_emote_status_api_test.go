package api

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/mako/clients/ripley"
	"github.com/stretchr/testify/mock"

	"code.justin.tv/commerce/mako/clients"
	autoapproval_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/autoapproval"
	config_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/config"
	mkd "code.justin.tv/commerce/mako/twirp/dashboard"
	. "github.com/smartystreets/goconvey/convey"
)

func TestGetUserFollowerEmoteStatusAPI(t *testing.T) {
	Convey("Test GetUserFollowerEmoteStatus API", t, func() {
		mockAutoApprovalClient := new(autoapproval_mock.EligibilityFetcher)
		mockDynamicConfig := new(config_mock.DynamicConfigClient)

		getUserFollowerEmoteStatusAPI := GetUserFollowerEmoteStatusAPI{
			Clients: clients.Clients{
				AutoApprovalEligibilityFetcher: mockAutoApprovalClient,
			},
			DynamicConfig: mockDynamicConfig,
		}

		ctx := context.Background()

		Convey("with no userID provided", func() {
			req := mkd.GetUserFollowerEmoteStatusRequest{UserId: ""}

			resp, err := getUserFollowerEmoteStatusAPI.GetUserFollowerEmoteStatus(ctx, &req)
			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("with user who is not in experiment list", func() {
			userId := "userIdInNotInExperiment"
			mockDynamicConfig.On("IsInFreemotesExperiment", userId).Return(false)
			req := mkd.GetUserFollowerEmoteStatusRequest{UserId: userId}

			resp, err := getUserFollowerEmoteStatusAPI.GetUserFollowerEmoteStatus(ctx, &req)
			So(resp, ShouldNotBeNil)
			So(resp.GetStatus(), ShouldEqual, mkd.FollowerEmoteDashboardStatus_NotAllowed)
			So(err, ShouldBeNil)
		})

		Convey("with an error looking up standing status", func() {
			userId := "userIdLookupError"
			mockDynamicConfig.On("IsInFreemotesExperiment", userId).Return(true)
			mockAutoApprovalClient.On("IsUserEligibleForEmoteUploadAutoApproval", mock.Anything, userId).Return(false, ripley.UnknownPartnerType, errors.New("test error"))

			req := mkd.GetUserFollowerEmoteStatusRequest{UserId: userId}

			resp, err := getUserFollowerEmoteStatusAPI.GetUserFollowerEmoteStatus(ctx, &req)
			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("with Affiliate not in good standing in experiment list", func() {
			userId := "userIdAffiliate"
			mockDynamicConfig.On("IsInFreemotesExperiment", userId).Return(true)
			mockAutoApprovalClient.On("IsUserEligibleForEmoteUploadAutoApproval", mock.Anything, userId).Return(false, ripley.AffiliatePartnerType, nil)
			req := mkd.GetUserFollowerEmoteStatusRequest{UserId: userId}

			resp, err := getUserFollowerEmoteStatusAPI.GetUserFollowerEmoteStatus(ctx, &req)
			So(resp, ShouldNotBeNil)
			So(resp.GetStatus(), ShouldEqual, mkd.FollowerEmoteDashboardStatus_NoUpload)
			So(err, ShouldBeNil)
		})

		Convey("with Partner not in good standing in experiment list", func() {
			userId := "userIdPartner"
			mockDynamicConfig.On("IsInFreemotesExperiment", userId).Return(true)
			mockAutoApprovalClient.On("IsUserEligibleForEmoteUploadAutoApproval", mock.Anything, userId).Return(false, ripley.TraditionalPartnerType, nil)
			req := mkd.GetUserFollowerEmoteStatusRequest{UserId: userId}

			resp, err := getUserFollowerEmoteStatusAPI.GetUserFollowerEmoteStatus(ctx, &req)
			So(resp, ShouldNotBeNil)
			So(resp.GetStatus(), ShouldEqual, mkd.FollowerEmoteDashboardStatus_NoUpload)
			So(err, ShouldBeNil)
		})

		Convey("with Affiliate in good standing in experiment list", func() {
			userId := "userIdAffiliate"
			mockDynamicConfig.On("IsInFreemotesExperiment", userId).Return(true)
			mockAutoApprovalClient.On("IsUserEligibleForEmoteUploadAutoApproval", mock.Anything, userId).Return(true, ripley.AffiliatePartnerType, nil)
			req := mkd.GetUserFollowerEmoteStatusRequest{UserId: userId}

			resp, err := getUserFollowerEmoteStatusAPI.GetUserFollowerEmoteStatus(ctx, &req)
			So(resp, ShouldNotBeNil)
			So(resp.GetStatus(), ShouldEqual, mkd.FollowerEmoteDashboardStatus_Allowed)
			So(err, ShouldBeNil)
		})

		Convey("with Partner in good standing in experiment list", func() {
			userId := "userIdPartner"
			mockDynamicConfig.On("IsInFreemotesExperiment", userId).Return(true)
			mockAutoApprovalClient.On("IsUserEligibleForEmoteUploadAutoApproval", mock.Anything, userId).Return(true, ripley.TraditionalPartnerType, nil)
			req := mkd.GetUserFollowerEmoteStatusRequest{UserId: userId}

			resp, err := getUserFollowerEmoteStatusAPI.GetUserFollowerEmoteStatus(ctx, &req)
			So(resp, ShouldNotBeNil)
			So(resp.GetStatus(), ShouldEqual, mkd.FollowerEmoteDashboardStatus_Allowed)
			So(err, ShouldBeNil)
		})
	})
}
