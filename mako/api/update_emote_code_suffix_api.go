package api

import (
	"context"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/mako/clients"
	mk "code.justin.tv/commerce/mako/twirp"
	"github.com/twitchtv/twirp"
)

type UpdateEmoteCodeSuffixAPI struct {
	Clients clients.Clients
}

func (de *UpdateEmoteCodeSuffixAPI) UpdateEmoteCodeSuffix(ctx context.Context, r *mk.UpdateEmoteCodeSuffixRequest) (*mk.UpdateEmoteCodeSuffixResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, time.Second*5)
	defer cancel()

	log.Infof("Update Emote Code Suffix: %s", r.String())

	err := validateUpdateEmoteCodeSuffixRequest(r)
	if err != nil {
		return nil, err
	}

	err = de.Clients.EmoticonsManager.UpdateEmoteCodeSuffix(ctx, r.GetId(), r.GetCodeSuffix())
	if err != nil {
		log.WithError(err).Error("Error updating emote code suffix")
		return nil, twirp.InternalError("Error updating emote code suffix").
			WithMeta("request", r.String()).
			WithMeta("error", err.Error())
	}

	return &mk.UpdateEmoteCodeSuffixResponse{}, nil
}

func validateUpdateEmoteCodeSuffixRequest(req *mk.UpdateEmoteCodeSuffixRequest) error {
	if req.GetId() == "" {
		return mk.NewClientError(
			twirp.RequiredArgumentError("id"),
			mk.ErrCodeMissingRequiredArgument)
	}
	if req.GetCodeSuffix() == "" {
		return mk.NewClientError(
			twirp.RequiredArgumentError("code suffix"),
			mk.ErrCodeMissingRequiredArgument)
	}
	return nil
}
