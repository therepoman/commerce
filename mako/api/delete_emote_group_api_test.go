package api

import (
	"context"
	"testing"

	"code.justin.tv/commerce/mako/clients"
	"code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/internal_mock"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestDeleteEmoteGroupAPI(t *testing.T) {
	Convey("Test DeleteEmoteGroupAPI", t, func() {
		mockEmoteGroupDB := new(internal_mock.EmoteGroupDB)
		deleteEmoteGroupAPI := &DeleteEmoteGroupAPI{
			Clients: clients.Clients{
				EmoteGroupDB: mockEmoteGroupDB,
			},
		}

		Convey("Test DeleteEmoteGroup", func() {
			Convey("With empty group_id", func() {
				groupId := ""

				err := deleteEmoteGroupAPI.DeleteEmoteGroup(context.Background(), groupId)
				So(err, ShouldNotBeNil)
			})

			Convey("With error on emoticon deactivate", func() {
				groupId := "1234"
				mockEmoteGroupDB.On("DeleteEmoteGroup", mock.Anything, mock.Anything).Return(errors.New("test"))

				err := deleteEmoteGroupAPI.DeleteEmoteGroup(context.Background(), groupId)
				So(err, ShouldNotBeNil)
			})
			Convey("With Success", func() {
				groupId := "1234"
				mockEmoteGroupDB.On("DeleteEmoteGroup", mock.Anything, mock.Anything).Return(nil)

				err := deleteEmoteGroupAPI.DeleteEmoteGroup(context.Background(), groupId)
				So(err, ShouldBeNil)
			})
		})
	})
}
