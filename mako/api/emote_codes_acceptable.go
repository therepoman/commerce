package api

import (
	"context"
	"strings"
	"time"

	mako "code.justin.tv/commerce/mako/internal"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/mako/clients"
	"code.justin.tv/commerce/mako/clients/emotecodeacceptability"
	mk "code.justin.tv/commerce/mako/twirp"
	"github.com/twitchtv/twirp"
)

// EmoteCodeAcceptableAPI includes the APIs to establish given emote code acceptability (how offensive it is)
type EmoteCodeAcceptableAPI struct {
	Clients clients.Clients
}

func (eca *EmoteCodeAcceptableAPI) BatchValidateEmoteCodeAcceptability(ctx context.Context, r *mk.BatchValidateEmoteCodeAcceptabilityRequest) (*mk.BatchValidateEmoteCodeAcceptabilityResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, time.Second*5)
	defer cancel()

	err := validateBatchValidateEmoteCodeAcceptabilityRequest(r)
	if err != nil {
		return nil, err
	}

	inputs := make([]emotecodeacceptability.EmoteCodeAcceptabilityInput, 0, len(r.GetRequests()))
	for _, input := range r.GetRequests() {
		inputs = append(inputs, emotecodeacceptability.EmoteCodeAcceptabilityInput{
			EmoteCode:       input.EmoteCode,
			EmoteCodeSuffix: input.EmoteCodeSuffix,
			EmoteCodePrefix: mako.ExtractEmotePrefix(input.EmoteCode, input.EmoteCodeSuffix),
		})
	}

	acceptabilityResults, err := eca.Clients.EmoteCodeAcceptabilityValidator.AreEmoteCodesAcceptable(ctx, inputs, r.GetUserId())
	if err != nil {
		log.WithError(err).WithFields(log.Fields{
			"requests": r.GetRequests(),
			"userID":   r.GetUserId(),
		}).Error("Failed to validate emote code acceptability during BatchValidateEmoteCodeAcceptability")
		return nil, twirp.InternalError("Error validating emote code acceptability")
	}

	results := make([]*mk.ValidateEmoteCodeAcceptabilityResult, 0, len(acceptabilityResults))
	for _, result := range acceptabilityResults {
		results = append(results, &mk.ValidateEmoteCodeAcceptabilityResult{
			EmoteCode:       result.EmoteCode,
			EmoteCodeSuffix: result.EmoteCodeSuffix,
			IsAcceptable:    result.IsAcceptable,
		})
	}

	return &mk.BatchValidateEmoteCodeAcceptabilityResponse{
		Results: results,
	}, nil
}

func validateBatchValidateEmoteCodeAcceptabilityRequest(req *mk.BatchValidateEmoteCodeAcceptabilityRequest) error {
	if len(req.GetRequests()) <= 0 {
		return twirp.InvalidArgumentError("requests", "must contain at least one set of inputs")
	}

	for _, request := range req.GetRequests() {
		if request.EmoteCode == "" {
			return twirp.RequiredArgumentError("emote_code")
		}

		if request.EmoteCodeSuffix == "" {
			return twirp.RequiredArgumentError("emote_code_suffix")
		}

		if !strings.HasSuffix(request.EmoteCode, request.EmoteCodeSuffix) {
			return twirp.NewError(twirp.FailedPrecondition, "emote_code_suffix must be present at the end of emote_code")
		}
	}

	return nil
}
