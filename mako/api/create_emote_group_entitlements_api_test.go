package api

import (
	"context"
	"testing"
	"time"

	mako "code.justin.tv/commerce/mako/internal"

	"code.justin.tv/commerce/mako/clients"
	internal_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/internal_mock"
	mk "code.justin.tv/commerce/mako/twirp"
	"github.com/golang/protobuf/ptypes/timestamp"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestCreateEmoteGroupEntitlements(t *testing.T) {
	Convey("Given CreateEmoteGroupEntitlementsAPI", t, func() {
		mockEntitlementDB := new(internal_mock.EntitlementDB)
		mockEmoteModifiersDB := new(internal_mock.EmoteModifiersDB)
		createEmoteGroupEntitlement := CreateEmoteGroupEntitlementsAPI{
			Clients: clients.Clients{
				EmoteModifiersDB: mockEmoteModifiersDB,
				EntitlementDB:    mockEntitlementDB,
			},
		}

		Convey("Test CreateEmoteGroupEntitlement", func() {
			Convey("With empty OwnerId", func() {
				req := &mk.CreateEmoteGroupEntitlementsRequest{
					Entitlements: []*mk.EmoteGroupEntitlement{
						{
							OriginId: "1",
							GroupId:  "2",
							ItemId:   "5",
							End: &mk.InfiniteTime{
								IsInfinite: true,
							},
						},
					},
				}
				_, err := createEmoteGroupEntitlement.CreateEmoteGroupEntitlements(context.Background(), req)
				So(err, ShouldNotBeNil)
			})

			Convey("With empty GroupId", func() {
				req := &mk.CreateEmoteGroupEntitlementsRequest{
					Entitlements: []*mk.EmoteGroupEntitlement{
						{
							OriginId: "1",
							OwnerId:  "2",
							ItemId:   "5",
							End: &mk.InfiniteTime{
								IsInfinite: true,
							},
						},
					},
				}
				_, err := createEmoteGroupEntitlement.CreateEmoteGroupEntitlements(context.Background(), req)
				So(err, ShouldNotBeNil)
			})

			Convey("With empty OriginId", func() {
				req := &mk.CreateEmoteGroupEntitlementsRequest{
					Entitlements: []*mk.EmoteGroupEntitlement{
						{
							GroupId: "2",
							ItemId:  "5",
							OwnerId: "1",
							End: &mk.InfiniteTime{
								IsInfinite: true,
							},
						},
					},
				}
				_, err := createEmoteGroupEntitlement.CreateEmoteGroupEntitlements(context.Background(), req)
				So(err, ShouldNotBeNil)
			})

			Convey("With Robots Smilies GroupId", func() {
				req := &mk.CreateEmoteGroupEntitlementsRequest{
					Entitlements: []*mk.EmoteGroupEntitlement{
						{
							GroupId: mako.RobotsSmilieGroupID.ToString(),
							ItemId:  "5",
							OwnerId: "1",
							End: &mk.InfiniteTime{
								IsInfinite: true,
							},
						},
					},
				}
				_, err := createEmoteGroupEntitlement.CreateEmoteGroupEntitlements(context.Background(), req)
				So(err, ShouldNotBeNil)
			})

			Convey("With Purple Glitches Smilies GroupId", func() {
				req := &mk.CreateEmoteGroupEntitlementsRequest{
					Entitlements: []*mk.EmoteGroupEntitlement{
						{
							GroupId: mako.PurpleSmilieGroupID.ToString(),
							ItemId:  "5",
							OwnerId: "1",
							End: &mk.InfiniteTime{
								IsInfinite: true,
							},
						},
					},
				}
				_, err := createEmoteGroupEntitlement.CreateEmoteGroupEntitlements(context.Background(), req)
				So(err, ShouldNotBeNil)
			})

			Convey("With Monkeys Smilies GroupId", func() {
				req := &mk.CreateEmoteGroupEntitlementsRequest{
					Entitlements: []*mk.EmoteGroupEntitlement{
						{
							GroupId: mako.MonkeysSmilieGroupID.ToString(),
							ItemId:  "5",
							OwnerId: "1",
							End: &mk.InfiniteTime{
								IsInfinite: true,
							},
						},
					},
				}
				_, err := createEmoteGroupEntitlement.CreateEmoteGroupEntitlements(context.Background(), req)
				So(err, ShouldNotBeNil)
			})

			Convey("With empty End", func() {
				req := &mk.CreateEmoteGroupEntitlementsRequest{
					Entitlements: []*mk.EmoteGroupEntitlement{
						{
							OriginId: "1",
							GroupId:  "2",
						},
					},
				}
				_, err := createEmoteGroupEntitlement.CreateEmoteGroupEntitlements(context.Background(), req)
				So(err, ShouldNotBeNil)
			})

			Convey("Creates materia entitlements", func() {
				req := &mk.CreateEmoteGroupEntitlementsRequest{
					Entitlements: []*mk.EmoteGroupEntitlement{
						{
							OwnerId:  "ownerid1",
							OriginId: "1",
							GroupId:  "emotegroup1",
							ItemId:   "channelid1",
							Group:    mk.EmoteGroup_Subscriptions,
							Start: &timestamp.Timestamp{
								Seconds: time.Now().Unix(),
							},
							End: &mk.InfiniteTime{
								IsInfinite: true,
							},
						},
					},
				}

				mockEmoteModifiersDB.On("InvalidateEntitledGroupsCacheForUser", mock.Anything).Return(nil)
				mockEntitlementDB.On("WriteEntitlements", mock.Anything, mock.Anything).Return(nil)

				_, err := createEmoteGroupEntitlement.CreateEmoteGroupEntitlements(context.Background(), req)
				So(err, ShouldBeNil)
			})
		})
	})
}
