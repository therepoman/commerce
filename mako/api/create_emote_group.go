package api

import (
	"context"
	"time"

	mako "code.justin.tv/commerce/mako/internal"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/mako/clients"
	mk "code.justin.tv/commerce/mako/twirp"
	"github.com/twitchtv/twirp"
)

type CreateEmoteGroupAPI struct {
	Clients clients.Clients
}

var supportedOrigins = map[mk.EmoteGroup]bool{
	mk.EmoteGroup_BitsBadgeTierEmotes: true,
	mk.EmoteGroup_Subscriptions:       true,
	mk.EmoteGroup_Follower:            true,
	mk.EmoteGroup_HypeTrain:           true,
}

func (g *CreateEmoteGroupAPI) CreateEmoteGroup(ctx context.Context, req *mk.CreateEmoteGroupRequest) (*mk.CreateEmoteGroupResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, 3*time.Second)
	defer cancel()

	origin := req.GetOrigin()
	groupType := req.GetGroupType()
	ownerId := req.GetOwnerId()

	if req == nil {
		return nil, twirp.InvalidArgumentError("req", "missing request")
	} else if isOriginSupported := supportedOrigins[origin]; !isOriginSupported {
		return nil, twirp.InvalidArgumentError("origin", "only supports BitsBadgeTierEmotes, Subscriptions, Follower, and HypeTrain")
	}

	group, _, err := g.Clients.EmoteGroupDB.CreateEmoteGroup(ctx, mako.EmoteGroup{
		OwnerID:   ownerId,
		Origin:    mako.ToMakoGroupOrigin(origin),
		GroupType: mako.ToMakoGroupAssetType(groupType),
	})

	if err != nil {
		log.WithError(err).WithFields(log.Fields{
			"origin":     origin,
			"group_type": groupType,
		}).Error("Failed to create emote group")
		return nil, twirp.InternalErrorWith(err)
	}

	return &mk.CreateEmoteGroupResponse{
		GroupId: group.GroupID,
	}, nil
}
