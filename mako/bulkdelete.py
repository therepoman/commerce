from __future__ import print_function # Python 2/3 compatibility
import argparse
import sys
import boto3
from boto3.dynamodb.conditions import Attr

def main():
    # Default scope if nothing specified
    DEFAULT_SCOPE = '/emote_sets/commerce/crate/halloween2017'
    AWS_RESOURCE = 'dynamodb'
    HASH_KEY = 'userId'
    SORT_KEY = 'entitlementScopedKey'
    LAST_EVALUATED_KEY = 'LastEvaluatedKey'

    # This script mass deletes entitlements from Mako's Entitlement DB
    parser = argparse.ArgumentParser(description='Bulk delete entitlements from Mako\'s Entitlement DB.')
    parser.add_argument('-t', '--table', help='Which table', default='dev_entitlements', choices=['dev_entitlements', 'staging_entitlements', 'prod_entitlements'])
    parser.add_argument('-s', '--scope', help='What scope', default=DEFAULT_SCOPE)
    args = parser.parse_args()

    table_name = args.table
    scope = args.scope

    should_scan = confirm('Delete scope: {} from {}?'.format(scope, table_name), resp=False)
    if not should_scan:
        sys.exit(0)

    dynamodb = boto3.resource(AWS_RESOURCE)
    table = dynamodb.Table(table_name)

    response = table.scan(
        FilterExpression=Attr(SORT_KEY).begins_with(scope)
    )
    data = response['Items']

    # Fetch paginated results
    # DynamoDB only returns in 1MB max responses
    # https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Scan.html
    while response.get(LAST_EVALUATED_KEY):
        response = table.scan(
            ExclusiveStartKey=response[LAST_EVALUATED_KEY],
            FilterExpression=Attr(SORT_KEY).begins_with(scope)
        )
        data.extend(response['Items'])
        sys.stdout.write('Scanning Table: {} Items found  \r'.format(len(data)))
        sys.stdout.flush()

    should_delete = confirm('You will delete {} item(s) from {}. Are you sure?'.format(len(data), table_name), resp=False)
    if not should_delete:
        sys.exit(0)

    print('Deleting all items under scope: {} from {}'.format(scope, table_name))
    with table.batch_writer() as batch:
        for item in data:
            batch.delete_item(
                Key={
                    HASH_KEY: item[HASH_KEY],
                    SORT_KEY: item[SORT_KEY]
                }
            )

    print('Deleted {} item(s) from {}'.format(len(data), table_name))
    sys.exit(0)

def confirm(prompt=None, resp=False):
    if prompt is None:
        prompt = 'Confirm'

    if resp:
        prompt = '{} [{}]|{}: '.format(prompt, 'y', 'n')
    else:
        prompt = '{} [{}]|{}: '.format(prompt, 'n', 'y')

    while True:
        ans = input(prompt)
        if not ans:
            return resp
        if ans not in ['y', 'Y', 'n', 'N']:
            print('please enter y or n.')
            continue
        if ans == 'y' or ans == 'Y':
            return True
        if ans == 'n' or ans == 'N':
            return False

if __name__ == '__main__':
    main()
