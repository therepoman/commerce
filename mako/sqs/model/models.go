package model

import (
	mako "code.justin.tv/commerce/mako/internal"
)

type DeleteEmoteAssetsFromS3Input struct {
	EmoteID        string              `json:"emote_id"`
	EmoteAssetType mako.EmoteAssetType `json:"emote_asset_type"`
}

// PrefixUpdateMessage is the SQS message model for processing prefix updates
type PrefixUpdateMessage struct {
	ChannelID            string `json:"channelId" validate:"nonzero"`
	PreviousPrefix       string `json:"previousPrefix"`
	NewPrefix            string `json:"newPrefix"`
	AlreadyUpdatedPrefix bool   `json:"alreadyUpdatedPrefix"`
	State                string `json:"state"`
}
