package processors

import (
	"context"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/mako/dynamo"
	mako "code.justin.tv/commerce/mako/internal"
	"code.justin.tv/commerce/mako/sqs/model"
	"github.com/pkg/errors"
)

// IPrefixUpdateProcessor is the processor interface
type IPrefixUpdateProcessor interface {
	UpdatePrefix(ctx context.Context, message model.PrefixUpdateMessage) error
}

// PrefixUpdateProcessor updates emote prefixes
type PrefixUpdateProcessor struct {
	EmoteDB   mako.EmoteDB
	PrefixDao dynamo.IPrefixDao
}

func NewPrefixUpdateProcessor(emoteDB mako.EmoteDB, prefixesDao dynamo.IPrefixDao) IPrefixUpdateProcessor {
	return &PrefixUpdateProcessor{
		EmoteDB:   emoteDB,
		PrefixDao: prefixesDao,
	}
}

// UpdatePrefix updates all emotes owned by the specified ChannelID. If any error is encountered while
// updating emotes, an error is returned.
func (pup *PrefixUpdateProcessor) UpdatePrefix(ctx context.Context, message model.PrefixUpdateMessage) error {
	if message.ChannelID == "" {
		return errors.New("channelID must not be empty")
	}

	log.WithFields(log.Fields{
		"channelID":      message.ChannelID,
		"previousPrefix": message.PreviousPrefix,
		"newPrefix":      message.NewPrefix,
		"state":          message.State,
	}).Info("updating emotes for channel to use a new prefix")

	// for messages from Subs service we'll update the prefix, for internal messages we don't need to.
	if !message.AlreadyUpdatedPrefix {
		_, err := pup.PrefixDao.Put(ctx, message.ChannelID, message.NewPrefix, pup.messageStateToDynamoState(message.State))

		if err != nil {
			return errors.Wrapf(err, "failed to update prefix for this owner: %v", message.ChannelID)
		}
	}

	// query emotes dao for emotes matching the owner
	emotes, err := pup.EmoteDB.Query(ctx, mako.EmoteQuery{
		OwnerID: message.ChannelID,
	})
	if err != nil {
		return errors.Wrapf(err, "failed to get emotes for this owner: %v", message.ChannelID)
	}

	// update each emote to the new prefix in Dynamo
	for _, emote := range emotes {
		newCode := message.NewPrefix + emote.Suffix
		_, err := pup.EmoteDB.UpdatePrefix(ctx, emote.ID, newCode, message.NewPrefix)
		if err != nil {
			return errors.Wrapf(err, "failed to update prefix for channel: %v", message.ChannelID)
		}
	}
	return nil
}

func (pup *PrefixUpdateProcessor) messageStateToDynamoState(messageState string) dynamo.PrefixState {
	switch messageState {
	case "unset":
		return dynamo.PrefixUnset
	case "active":
		return dynamo.PrefixActive
	case "rejected":
		return dynamo.PrefixRejected
	case "pending":
		return dynamo.PrefixPending
	case "unknown":
		fallthrough
	default:
		return dynamo.PrefixUnknown
	}
}
