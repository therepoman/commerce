package processors

import (
	"context"
	"testing"

	"code.justin.tv/commerce/mako/internal"
	internal_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/internal_mock"
	"code.justin.tv/commerce/mako/sqs/model"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestPrefixUpdateProcessor(t *testing.T) {
	Convey("Test UpdatePrefix", t, func() {
		mockEmoteDB := new(internal_mock.EmoteDB)

		processor := NewPrefixUpdateProcessor(mockEmoteDB, nil)

		Convey("When empty message received", func() {
			message := model.PrefixUpdateMessage{}

			err := processor.UpdatePrefix(context.Background(), message)
			So(err, ShouldNotBeNil)
			mockEmoteDB.AssertExpectations(t)
		})

		Convey("When healthy message received", func() {
			message := model.PrefixUpdateMessage{
				ChannelID:            "1234",
				PreviousPrefix:       "foo",
				NewPrefix:            "foo2",
				AlreadyUpdatedPrefix: true,
			}
			emptyResults := []mako.Emote{}
			sampleResults := []mako.Emote{
				{
					ID:     "1",
					Suffix: "Bar",
				},
				{
					ID:     "2",
					Suffix: "Bar2",
				},
				{
					ID:     "3",
					Suffix: "Bar3",
				},
			}

			Convey("When Emotes DAO GetByOwnerID fails", func() {
				mockEmoteDB.On("Query", mock.Anything, mock.Anything).Return(emptyResults, errors.New("test"))

				err := processor.UpdatePrefix(context.Background(), message)
				So(err, ShouldNotBeNil)
				mockEmoteDB.AssertExpectations(t)
			})

			Convey("When Emotes DAO GetByOwnerID returns empty result", func() {
				mockEmoteDB.On("Query", mock.Anything, mock.Anything).Return(emptyResults, nil)

				err := processor.UpdatePrefix(context.Background(), message)
				So(err, ShouldBeNil)
				mockEmoteDB.AssertExpectations(t)
			})

			Convey("When Emotes DAO GetByOwnerID returns emotes", func() {
				mockEmoteDB.On("Query", mock.Anything, mock.Anything).Return(sampleResults, nil)

				Convey("When UpdatePrefix fails", func() {
					mockEmoteDB.On("UpdatePrefix", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(mako.Emote{}, errors.New("test"))
					err := processor.UpdatePrefix(context.Background(), message)
					So(err, ShouldNotBeNil)
					mockEmoteDB.AssertExpectations(t)
				})

				Convey("When UpdatePrefix suceeds", func() {
					mockEmoteDB.On("UpdatePrefix", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(mako.Emote{}, nil)
					err := processor.UpdatePrefix(context.Background(), message)
					So(err, ShouldBeNil)
					mockEmoteDB.AssertExpectations(t)
				})
			})
		})
	})
}
