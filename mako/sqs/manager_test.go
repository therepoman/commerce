package sqs

import (
	"sync"
	"testing"
	"time"

	events_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/events"
	sqs_mock "code.justin.tv/commerce/mako/mocks/github.com/aws/aws-sdk-go/service/sqs/sqsiface"
	"code.justin.tv/commerce/mako/sqs/workers"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/cactus/go-statsd-client/statsd"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

const (
	timeout = 1 * time.Second
)

func TestManager(t *testing.T) {
	Convey("Test manager", t, func() {
		stats, _ := statsd.NewNoopClient()
		mockClient := new(sqs_mock.SQSAPI)
		mockDelegator := new(events_mock.IEventDelegator)

		// Mock Get Queue Url
		getQueueUrlOutput := sqs.GetQueueUrlOutput{
			QueueUrl: aws.String("www.notreal.com"),
		}
		mockClient.On("GetQueueUrl", mock.Anything).Return(&getQueueUrlOutput, nil)

		// Mock Get Queue Attributes
		getQueueAttributesOutput := sqs.GetQueueAttributesOutput{
			Attributes: map[string]*string{
				"MessageRetentionPeriod": aws.String("3"),
				"VisibilityTimeout":      aws.String("3"),
			},
		}
		mockClient.On("GetQueueAttributes", mock.Anything).Return(&getQueueAttributesOutput, nil)

		// Mock Receive Message Request
		mockClient.On("ReceiveMessageWithContext", mock.Anything, mock.Anything).Return(&sqs.ReceiveMessageOutput{}, nil)

		Convey("When shutting down correctly", func() {
			manager := NewWorkerManager("queuename", 2, mockClient, workers.NewEventsWorker(mockDelegator), stats)

			So(manager.waitGroup, ShouldNotBeNil)

			err := manager.Shutdown(timeout)

			So(err, ShouldBeNil)
		})
	})
}

func TestWaitWithTimeout(t *testing.T) {
	Convey("Test waitWithTimeout", t, func() {
		Convey("When shutting down correctly", func() {
			var wg sync.WaitGroup
			wg.Add(2)
			go performSleep(100, &wg)
			go performSleep(100, &wg)

			timedout := waitWithTimeout(&wg, timeout)

			So(timedout, ShouldBeFalse)
		})

		Convey("When timing out", func() {
			var wg sync.WaitGroup
			wg.Add(2)
			go performSleep(1500, &wg)
			go performSleep(1500, &wg)

			timedout := waitWithTimeout(&wg, timeout)

			So(timedout, ShouldBeTrue)
		})
	})
}

func performSleep(millisecs time.Duration, wg *sync.WaitGroup) {
	duration := millisecs * time.Millisecond
	time.Sleep(duration)
	wg.Done()
}
