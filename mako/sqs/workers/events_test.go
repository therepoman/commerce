package workers

import (
	"testing"

	events_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/events"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/sqs"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestEventsWorker(t *testing.T) {
	Convey("Test EventsWorker handler", t, func() {
		mockDelegator := new(events_mock.IEventDelegator)
		worker := NewEventsWorker(mockDelegator)
		mockDelegator.On("Delegate", mock.Anything).Return(nil)

		Convey("When empty data received", func() {
			emptyData := &sqs.Message{
				Body: aws.String(""),
			}
			err := worker.Handle(emptyData)
			So(err, ShouldNotBeNil)
		})

		Convey("When garbage data received", func() {
			badData := &sqs.Message{
				Body: aws.String("kljdsflksjdg329052roi2jjf23oirj"),
			}
			err := worker.Handle(badData)
			So(err, ShouldNotBeNil)
		})

		Convey("When invalid event received", func() {
			badEvent := &sqs.Message{
				Body: aws.String("{\"foo\":\"bar\"}"),
			}
			err := worker.Handle(badEvent)
			So(err, ShouldNotBeNil)
		})

		Convey("When valid event received", func() {
			validEvent := &sqs.Message{
				Body: aws.String("{\"user_id\":\"foo\",\"scope\":\"/commerce-purchase/\",\"key\":\"key1\",\"value\":\"value1\",\"origin\":\"fuel\"}"),
			}
			err := worker.Handle(validEvent)
			So(err, ShouldBeNil)
		})
	})
}
