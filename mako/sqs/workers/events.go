package workers

import (
	"encoding/json"

	log "code.justin.tv/commerce/logrus"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/pkg/errors"

	"code.justin.tv/commerce/mako/events"
	"gopkg.in/validator.v2"
)

// EventsWorker worker definition
type EventsWorker struct {
	Delegator events.IEventDelegator
}

// NewEventsWorker creates a new sqs worker with the given delegator
func NewEventsWorker(delegator events.IEventDelegator) *EventsWorker {
	return &EventsWorker{
		Delegator: delegator,
	}
}

// Handle is the handler method for processing messages from the Events queue
func (w *EventsWorker) Handle(msg *sqs.Message) error {
	log.Info("Received new SQS message.")

	var event events.Event
	if err := json.Unmarshal([]byte(aws.StringValue(msg.Body)), &event); err != nil {
		return errors.Wrap(err, "failed to unmarshall event message")
	}

	if err := validator.Validate(event); err != nil {
		return errors.Wrap(err, "validation failed for event message")
	}

	log.WithField("event", event).Info("Successfully read event message")

	if err := w.Delegator.Delegate(event); err != nil {
		return errors.Wrap(err, "failed to delegate event message")
	}

	return nil
}
