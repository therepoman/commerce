package workers

import (
	"context"
	"errors"
	"testing"

	sfn_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/sfn"
	"code.justin.tv/eventbus/schema/pkg/user"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
)

func TestUserDestroyWorker(t *testing.T) {
	testContext := context.Background()
	testUserId := "267893713"

	tests := []struct {
		scenario string
		test     func(t *testing.T)
	}{
		{
			scenario: "GIVEN event is nil WHEN handle THEN return error",
			test: func(t *testing.T) {
				// GIVEN
				worker := userDestroyWorker{}

				// WHEN
				err := worker.Handle(testContext, nil, nil)

				// THEN
				require.Error(t, err)
				assert.Contains(t, err.Error(), "no user id given for user destroy event")
			},
		},
		{
			scenario: "GIVEN no user id given WHEN handle THEN return error",
			test: func(t *testing.T) {
				// GIVEN
				worker := userDestroyWorker{}
				testEvent := &user.UserDestroy{
					UserId: "",
				}

				// WHEN
				err := worker.Handle(testContext, nil, testEvent)

				// THEN
				require.Error(t, err)
				assert.Contains(t, err.Error(), "no user id given for user destroy event")
			},
		},
		{
			scenario: "GIVEN failure starting step function execution WHEN handle THEN return error",
			test: func(t *testing.T) {
				// GIVEN
				mockSFNWrapper := new(sfn_mock.ClientWrapper)
				mockSFNWrapper.On("ExecuteUserDestroyStateMachine", mock.Anything, mock.Anything).Return(errors.New("dummy sfn error"))
				worker := userDestroyWorker{
					SFNClient: mockSFNWrapper,
				}
				testEvent := &user.UserDestroy{
					UserId: testUserId,
				}

				// WHEN
				err := worker.Handle(testContext, nil, testEvent)

				// THEN
				require.Error(t, err)
				assert.Contains(t, err.Error(), "error creating User Destroy SFN execution")
				assert.Contains(t, err.Error(), "dummy sfn error")
			},
		},
		{
			scenario: "GIVEN success starting step function execution WHEN handle THEN return nil",
			test: func(t *testing.T) {
				// GIVEN
				mockSFNWrapper := new(sfn_mock.ClientWrapper)
				mockSFNWrapper.On("ExecuteUserDestroyStateMachine", mock.Anything, mock.Anything).Return(nil)
				worker := userDestroyWorker{
					SFNClient: mockSFNWrapper,
				}
				testEvent := &user.UserDestroy{
					UserId: testUserId,
				}

				// WHEN
				err := worker.Handle(testContext, nil, testEvent)

				// THEN
				require.NoError(t, err)
			},
		},
	}

	for _, test := range tests {
		t.Run(test.scenario, func(t *testing.T) {
			test.test(t)
		})
	}
}
