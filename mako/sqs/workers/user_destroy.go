package workers

import (
	"context"

	"code.justin.tv/commerce/gogogadget/strings"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/mako/sfn"
	eventbus "code.justin.tv/eventbus/client"
	"code.justin.tv/eventbus/schema/pkg/user"
	"github.com/pkg/errors"
)

type userDestroyWorker struct {
	SFNClient sfn.ClientWrapper
}

// NewUserDestroyWorker creates a new sqs worker to process responding to UserDestroy events
func NewUserDestroyWorker(sfn sfn.ClientWrapper) *userDestroyWorker {
	return &userDestroyWorker{
		SFNClient: sfn,
	}
}

// Handles a UserDestroy event from Event Bus. Header argument required by EventBus interface bus is not used here.
func (udw *userDestroyWorker) Handle(ctx context.Context, _ *eventbus.Header, event *user.UserDestroy) error {
	if event == nil || strings.Blank(event.UserId) {
		errMsg := "no user id given for user destroy event"
		log.Error(errMsg)
		return errors.New(errMsg)
	}

	log.WithField("userID", event.UserId).Info("Starting User Destroy Step Function")
	err := udw.SFNClient.ExecuteUserDestroyStateMachine(ctx, sfn.UserDestroyInput{
		UserID: event.UserId,
	})
	if err != nil {
		errMsg := "error creating User Destroy SFN execution"
		log.WithField("userID", event.UserId).WithError(err).Error(errMsg)
		return errors.Wrap(err, errMsg)
	}

	return nil
}
