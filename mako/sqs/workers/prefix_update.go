package workers

import (
	"context"
	"encoding/json"

	log "code.justin.tv/commerce/logrus"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/pkg/errors"

	"code.justin.tv/commerce/mako/sqs/model"
	"code.justin.tv/commerce/mako/sqs/processors"
	"gopkg.in/validator.v2"
)

// PrefixUpdateWorker worker definition
type PrefixUpdateWorker struct {
	Processor processors.IPrefixUpdateProcessor
}

// NewPrefixUpdateWorker creates a new sqs worker that processes prefix updates
func NewPrefixUpdateWorker(processor processors.IPrefixUpdateProcessor) *PrefixUpdateWorker {
	return &PrefixUpdateWorker{
		Processor: processor,
	}
}

// Handle is the handler method for processing messages from the Events queue
func (w *PrefixUpdateWorker) Handle(msg *sqs.Message) error {
	log.Info("Received new SQS message.")

	var prefixUpdateMessage model.PrefixUpdateMessage
	if err := json.Unmarshal([]byte(aws.StringValue(msg.Body)), &prefixUpdateMessage); err != nil {
		return errors.Wrap(err, "failed to unmarshall prefix update message")
	}

	if err := validator.Validate(prefixUpdateMessage); err != nil {
		return errors.Wrap(err, "validation failed for prefix update message")
	}

	log.WithField("prefixUpdateMessage", prefixUpdateMessage).Info("Successfully read prefix update message")

	err := w.Processor.UpdatePrefix(context.Background(), prefixUpdateMessage)
	if err != nil {
		return errors.Wrap(err, "failed to update prefix in PrefixUpdateWorker")
	}

	return nil
}
