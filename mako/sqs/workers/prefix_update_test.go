package workers

import (
	"errors"
	"testing"

	processors_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/sqs/processors"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/sqs"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestPrefixUpdateWorker(t *testing.T) {
	Convey("Test PrefixUpdate worker handler", t, func() {
		mockProcessor := new(processors_mock.IPrefixUpdateProcessor)
		worker := NewPrefixUpdateWorker(mockProcessor)

		Convey("When empty data received", func() {
			emptyData := &sqs.Message{
				Body: aws.String(""),
			}
			err := worker.Handle(emptyData)
			So(err, ShouldNotBeNil)
		})

		Convey("When garbage data received", func() {
			badData := &sqs.Message{
				Body: aws.String("kljdsflksjdg329052roi2jjf23oirj"),
			}
			err := worker.Handle(badData)
			So(err, ShouldNotBeNil)
		})

		Convey("When invalid message received", func() {
			badMessage := &sqs.Message{
				Body: aws.String("{\"foo\":\"bar\"}"),
			}
			err := worker.Handle(badMessage)
			So(err, ShouldNotBeNil)
		})

		Convey("When valid message received", func() {
			validMessage := &sqs.Message{
				Body: aws.String("{\"channelId\":\"123\",\"newPrefix\":\"foo2\"}"),
			}

			Convey("When processor returns successfully", func() {
				mockProcessor.On("UpdatePrefix", mock.Anything, mock.Anything).Return(nil)
				err := worker.Handle(validMessage)
				So(err, ShouldBeNil)
				mockProcessor.AssertExpectations(t)
			})

			Convey("When processor returns failure", func() {
				mockProcessor.On("UpdatePrefix", mock.Anything, mock.Anything).Return(errors.New("test"))
				err := worker.Handle(validMessage)
				So(err, ShouldNotBeNil)
				mockProcessor.AssertExpectations(t)
			})
		})
	})
}
