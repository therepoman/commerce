package sqs

import (
	"sync"

	"time"

	"code.justin.tv/chat/workerqueue"
	"github.com/cactus/go-statsd-client/statsd"

	"fmt"

	log "code.justin.tv/commerce/logrus"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/aws/aws-sdk-go/service/sqs/sqsiface"
)

type Worker interface {
	Handle(msg *sqs.Message) error
}

type WorkerManager struct {
	waitGroup   *sync.WaitGroup
	stopChannel chan struct{}
	worker      Worker
}

func NewWorkerManager(queueName string, numWorkers int, client sqsiface.SQSAPI, worker Worker, stats statsd.Statter) *WorkerManager {
	manager := new(WorkerManager)
	manager.stopChannel = make(chan struct{})

	log.Infof("Initializing %s SQS workers...", queueName)

	manager.worker = worker

	params := workerqueue.CreateWorkersParams{
		NumWorkers: numWorkers,
		QueueName:  queueName,
		Client:     client,
		Tasks: map[workerqueue.TaskVersion]workerqueue.TaskFn{
			workerqueue.FallbackTaskVersion: manager.worker.Handle,
			workerqueue.TaskVersion(1):      manager.worker.Handle,
		},
	}

	var errCh <-chan error
	var err error
	manager.waitGroup, errCh, err = workerqueue.CreateWorkers(params, manager.stopChannel, stats)
	if err != nil {
		log.Fatalf("Failed to create workers: %v", err)
	}

	go func() {
		for err := range errCh {
			log.WithError(err).Errorf("Error occurred in SQS worker")
		}
	}()

	log.Infof("Finished initializing %s SQS workers.", queueName)
	return manager
}

// Shutdown gracefully terminates SQS workers, but only waits until the timeout.
func (mgr *WorkerManager) Shutdown(teardownTimeout time.Duration) error {
	close(mgr.stopChannel)
	if waitWithTimeout(mgr.waitGroup, teardownTimeout) {
		return fmt.Errorf("Timed out while waiting for SQS workers to complete.")
	}
	return nil
}

// waitWithTimeout waits for wait group to complete, but only waits until the timeout. Returns true
// if the timeout was hit, false otherwise.
func waitWithTimeout(wg *sync.WaitGroup, timeout time.Duration) bool {
	workersDone := make(chan struct{})
	go func() {
		defer close(workersDone)
		wg.Wait()
	}()

	select {
	case <-workersDone:
		return false // done, no timeout
	case <-time.After(timeout):
		return true // done, timed out
	}
}
