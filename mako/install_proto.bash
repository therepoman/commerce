#!/usr/bin/env bash

which protoc
PROTOC_EXISTS=$?
if [ $PROTOC_EXISTS -eq 0 ]; then
    echo "Protoc already installed"
    exit 0
fi

if [ "$(uname)" == "Darwin" ]; then
    brew install protobuf
elif [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then
    sudo chmod a+w /usr/local/src
    pushd /usr/local/src
    wget https://github.com/google/protobuf/releases/download/v3.1.0/protoc-3.1.0-linux-x86_64.zip -O /usr/local/src/protoc-3.1.0-linux-x86_64.zip
    unzip -x protoc-3.1.0-linux-x86_64.zip
    if [ ! -e /usr/local/bin/protoc ]; then
        sudo ln -s `pwd`/protoc/bin/protoc /usr/local/bin/protoc
    fi
    popd
fi
exit 0