package integration

import (
	"fmt"

	"code.justin.tv/commerce/gogogadget/random"
	"code.justin.tv/commerce/mako/integration/user_utils"
	"github.com/segmentio/ksuid"
)

const (
	integrationTestUserIDTemplate = "%s%s" // hardcoded prefix + ksuid suffix
	prefixTemplate                = "%s%d" // hardcoded prefix + random int suffix
	prePrefix                     = "test"
	suffixTemplate                = "%s%d" // hardcoded prefix + random int suffix
	preSuffix                     = "Dax"
	codeTemplate                  = "%s%s" // provided prefix + provided suffix
)

func getIntegrationTestUserID() string {
	return fmt.Sprintf(integrationTestUserIDTemplate, user_utils.IntegrationTestUserPrefix, ksuid.New().String())
}

func getIntegrationTestEmotePrefix() string {
	return fmt.Sprintf(prefixTemplate, prePrefix, random.Int(0, 999999))
}

func getIntegrationTestEmoteSuffix() string {
	return fmt.Sprintf(suffixTemplate, preSuffix, random.Int(0, 999999))
}

func getIntegrationTestEmoteCode(prefix, suffix string) string {
	return fmt.Sprintf(codeTemplate, prefix, suffix)
}

func getIntegrationTestDefaultImageID() string {
	return "id"
}
