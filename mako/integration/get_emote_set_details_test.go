package integration

import (
	"context"
	"testing"
	"time"

	mako "code.justin.tv/commerce/mako/internal"
	mk "code.justin.tv/commerce/mako/twirp"
	mkd "code.justin.tv/commerce/mako/twirp/dashboard"
	"github.com/golang/protobuf/ptypes/timestamp"
	"github.com/segmentio/ksuid"
	"github.com/stretchr/testify/require"
)

type EmoteSetDetailsIntegTestParms struct {
	userID                     string
	shouldNotGenerateUserID    bool
	channelID                  string
	shouldNotGenerateChannelID bool
	shouldSkipSiteDB           bool
	shouldExpectError          bool
	expectedEmoteIDs           []string // emotes that should be returned by this GetEmoteSetDetails call
	unexpectedEmoteIDs         []string // emotes which should not be returned by this GetEmoteSetDetails call
}

func TestGetEmoteSetDetailsAPI(t *testing.T) {
	tests := []struct {
		scenario string
		test     func(ctx context.Context, t *testing.T)
	}{
		{
			scenario: "Should always load globals",
			test: func(ctx context.Context, t *testing.T) {
				// Create a global emote
				uploadResp, cleanUp := UploadAndCreateAutoResizeEmote(ctx, t, UploadParams{
					// Global emotes have no prefix, only a suffix
					EmoteCodePrefix:              "",
					IsGlobalOrSmilie:             true,
					OwnerID:                      mako.TwitchOwnerID,
					Origin:                       mkd.EmoteOrigin_Globals,
					State:                        mkd.EmoteState_active,
					ExpectEmoteByIDToReturnEmote: true,
					ExpectEmoteToHaveEmoteGroup:  true,
				})
				defer cleanUp()

				getEmoteSetDetails(t, ctx, EmoteSetDetailsIntegTestParms{
					expectedEmoteIDs: []string{
						uploadResp.EmoteID,
					},
				})
			},
		},
		{
			scenario: "Should load static subscription emotes if the user is entitled to them",
			test: func(ctx context.Context, t *testing.T) {
				// Create a static subscription emote
				uploadResp, cleanUpEmote := UploadAndCreateAutoResizeEmote(ctx, t, UploadParams{
					EmoteGroup:                   mk.EmoteGroup_Subscriptions,
					GroupType:                    mk.EmoteGroupType_static_group,
					AssetType:                    mkd.EmoteAssetType_static,
					Origin:                       mkd.EmoteOrigin_Subscriptions,
					State:                        mkd.EmoteState_active,
					ExpectEmoteByIDToReturnEmote: true,
					ExpectEmoteToHaveEmoteGroup:  true,
				})
				defer cleanUpEmote()

				viewerID := getIntegrationTestUserID()
				creatorID := uploadResp.OwnerID

				cleanUpEntitlement := createEmoteGroupEntitlements(ctx, t, createEntitlementsParams{
					viewerID:  viewerID,
					creatorID: creatorID,
					groupID:   uploadResp.GroupID,
					originID:  "Subscriptions",
					group:     mk.EmoteGroup_Subscriptions,
				})
				defer cleanUpEntitlement()

				getEmoteSetDetails(t, ctx, EmoteSetDetailsIntegTestParms{
					userID: viewerID,
					// Subscription emotes should load even if no channelID is provided
					channelID:                  "",
					shouldNotGenerateChannelID: true,
					expectedEmoteIDs: []string{
						uploadResp.EmoteID,
					},
				})
			},
		},
		{
			scenario: "Should load animated subscription emotes if the user is entitled to them",
			test: func(ctx context.Context, t *testing.T) {
				// Create a animated subscription emote
				uploadResp, cleanUpEmote := UploadAndCreateAutoResizeEmote(ctx, t, UploadParams{
					EmoteGroup:                   mk.EmoteGroup_Subscriptions,
					GroupType:                    mk.EmoteGroupType_animated_group,
					AssetType:                    mkd.EmoteAssetType_animated,
					Origin:                       mkd.EmoteOrigin_Subscriptions,
					State:                        mkd.EmoteState_active,
					ExpectEmoteByIDToReturnEmote: true,
					ExpectEmoteToHaveEmoteGroup:  true,
				})
				defer cleanUpEmote()

				viewerID := getIntegrationTestUserID()
				creatorID := uploadResp.OwnerID

				cleanUpEntitlement := createEmoteGroupEntitlements(ctx, t, createEntitlementsParams{
					viewerID:  viewerID,
					creatorID: creatorID,
					groupID:   uploadResp.GroupID,
					originID:  "Subscriptions",
					group:     mk.EmoteGroup_Subscriptions,
				})
				defer cleanUpEntitlement()

				// Give time to let the entitlement propagate before trying to read it below
				time.Sleep(time.Second)

				getEmoteSetDetails(t, ctx, EmoteSetDetailsIntegTestParms{
					userID: viewerID,
					// Subscription emotes should load even if no channelID is provided
					channelID:                  "",
					shouldNotGenerateChannelID: true,
					expectedEmoteIDs: []string{
						uploadResp.EmoteID,
					},
				})
			},
		},
		{
			scenario: "Should load this channel's follower emotes if the user is following this channel",
			test: func(ctx context.Context, t *testing.T) {
				// We have logic in the follower entitlementsDB that returns isEntitled if both the viewerID and creatorID
				// have the prefix for integration test users
				viewerID := getIntegrationTestUserID()
				creatorID := getIntegrationTestUserID()

				// Create a follower emote
				uploadResp, cleanUpEmote := UploadAndCreateAutoResizeEmote(ctx, t, UploadParams{
					OwnerID:                      creatorID,
					EmoteGroup:                   mk.EmoteGroup_Follower,
					GroupType:                    mk.EmoteGroupType_static_group,
					AssetType:                    mkd.EmoteAssetType_static,
					Origin:                       mkd.EmoteOrigin_Follower,
					State:                        mkd.EmoteState_active,
					ExpectEmoteByIDToReturnEmote: true,
					ExpectEmoteToHaveEmoteGroup:  true,
				})
				defer cleanUpEmote()

				getEmoteSetDetails(t, ctx, EmoteSetDetailsIntegTestParms{
					userID:                     viewerID,
					channelID:                  creatorID,
					shouldNotGenerateChannelID: true,
					expectedEmoteIDs: []string{
						uploadResp.EmoteID,
					},
				})
			},
		},
		{
			scenario: "Should not load any follower emotes if the user is not following this channel",
			test: func(ctx context.Context, t *testing.T) {
				// We set the creatorID to be something without the integration test user prefix, so this will actually
				// call follows service in staging for a pair of users which don't exist, which will mean that we get a
				// negative response, meaning that the user is not following the channel
				creatorID := ksuid.New().String()

				// Create a follower emote
				uploadResp, cleanUpEmote := UploadAndCreateAutoResizeEmote(ctx, t, UploadParams{
					OwnerID:                      creatorID,
					EmoteGroup:                   mk.EmoteGroup_Follower,
					GroupType:                    mk.EmoteGroupType_static_group,
					AssetType:                    mkd.EmoteAssetType_static,
					Origin:                       mkd.EmoteOrigin_Follower,
					State:                        mkd.EmoteState_active,
					ExpectEmoteByIDToReturnEmote: true,
					ExpectEmoteToHaveEmoteGroup:  true,
				})
				defer cleanUpEmote()

				viewerID := getIntegrationTestUserID()

				getEmoteSetDetails(t, ctx, EmoteSetDetailsIntegTestParms{
					userID:                     viewerID,
					channelID:                  creatorID,
					shouldNotGenerateChannelID: true,
					unexpectedEmoteIDs: []string{
						uploadResp.EmoteID,
					},
				})
			},
		},
		{
			scenario: "Should load this channel's follower emotes if the user is the same as the channel",
			test: func(ctx context.Context, t *testing.T) {
				// We set the creatorID and the viewerID to be the same, meaning we should short-circuit before calling
				// follows service, since every channel is treated as though it is following itself for the purposes of
				// follower emotes
				creatorID := getIntegrationTestUserID()
				viewerID := creatorID

				// Create a follower emote
				uploadResp, cleanUpEmote := UploadAndCreateAutoResizeEmote(ctx, t, UploadParams{
					OwnerID:                      creatorID,
					EmoteGroup:                   mk.EmoteGroup_Follower,
					GroupType:                    mk.EmoteGroupType_static_group,
					AssetType:                    mkd.EmoteAssetType_static,
					Origin:                       mkd.EmoteOrigin_Follower,
					State:                        mkd.EmoteState_active,
					ExpectEmoteByIDToReturnEmote: true,
					ExpectEmoteToHaveEmoteGroup:  true,
				})
				defer cleanUpEmote()

				getEmoteSetDetails(t, ctx, EmoteSetDetailsIntegTestParms{
					userID:                     viewerID,
					channelID:                  creatorID,
					shouldNotGenerateChannelID: true,
					expectedEmoteIDs: []string{
						uploadResp.EmoteID,
					},
				})
			},
		},
	}

	for _, test := range tests {
		t.Run(test.scenario, func(t *testing.T) {
			t.Parallel()

			ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
			test.test(ctx, t)
			cancel()
		})
	}
}

func getEmoteSetDetails(t *testing.T, ctx context.Context, params EmoteSetDetailsIntegTestParms) {
	userID := params.userID
	if !params.shouldNotGenerateChannelID {
		userID = getIntegrationTestUserID()
	}

	channelID := params.channelID
	if !params.shouldNotGenerateChannelID {
		channelID = getIntegrationTestUserID()
	}

	req := &mk.GetEmoteSetDetailsRequest{
		UserId:     userID,
		SkipSitedb: params.shouldSkipSiteDB,
		ChannelId:  channelID,
	}

	resp, err := getEmoteSetDetailsAPI.GetEmoteSetDetails(ctx, req)
	if params.shouldExpectError {
		require.Nil(t, resp)
		require.NotNil(t, err)
	} else {
		require.NotNil(t, resp)
		require.Nil(t, err)

		for _, expectedEmoteID := range params.expectedEmoteIDs {
			var foundID bool
			for _, emoteSet := range resp.GetEmoteSets() {
				for _, emote := range emoteSet.GetEmotes() {
					if emote.Id == expectedEmoteID {
						foundID = true
					}
				}
			}
			require.True(t, foundID)
		}

		for _, unexpectedEmoteID := range params.unexpectedEmoteIDs {
			var foundID bool
			for _, emoteSet := range resp.GetEmoteSets() {
				for _, emote := range emoteSet.GetEmotes() {
					if emote.Id == unexpectedEmoteID {
						foundID = true
					}
				}
			}
			require.False(t, foundID)
		}
	}
}

// This struct is included because the CreateEmoteEntitlements API's properties are hard to keep track of
type createEntitlementsParams struct {
	viewerID  string // ID of the viewer who is entitled the emotes
	creatorID string // ID of the creator who owns the emotes
	groupID   string // ID of the emote group containing these emotes
	originID  string // Shouldn't matter for integ tests, but usually is a human-readable string about where this entitlement came from
	group     mk.EmoteGroup
}

func createEmoteGroupEntitlements(ctx context.Context, t *testing.T, params createEntitlementsParams) func() {
	entitlement := mk.EmoteGroupEntitlement{
		OwnerId:  params.viewerID,
		ItemId:   params.creatorID,
		GroupId:  params.groupID,
		OriginId: params.originID,
		Group:    params.group,
		Start: &timestamp.Timestamp{
			Seconds: time.Now().Add(-1 * time.Hour).Unix(),
		},
		End: &mk.InfiniteTime{
			IsInfinite: true,
		},
	}

	resp, err := createEmoteGroupEntitlementsAPI.CreateEmoteGroupEntitlements(ctx, &mk.CreateEmoteGroupEntitlementsRequest{
		Entitlements: []*mk.EmoteGroupEntitlement{
			&entitlement,
		},
	})
	require.NotNil(t, resp)
	require.Nil(t, err)

	return func() {
		cleanUpCreateEmoteGroupEntitlements(entitlement)
	}
}

func cleanUpCreateEmoteGroupEntitlements(entitlement mk.EmoteGroupEntitlement) {
	go func(entitlement mk.EmoteGroupEntitlement) {
		_, _ = deleteEmoteGroupEntitlementsAPI.DeleteEmoteGroupEntitlements(context.Background(), &mk.DeleteEmoteGroupEntitlementsRequest{
			Entitlements: []*mk.EmoteGroupEntitlement{
				&entitlement,
			},
		})
	}(entitlement)
}
