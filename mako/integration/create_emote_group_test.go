package integration

import (
	"context"
	"testing"
	"time"

	mk "code.justin.tv/commerce/mako/twirp"
	"github.com/stretchr/testify/require"
)

func TestCreateEmoteGroup(t *testing.T) {
	t.Run("Create Static Subscription Emote Group", func(t *testing.T) {
		CreateEmoteGroupWithOriginAndGroupType(t, mk.EmoteGroup_Subscriptions, mk.EmoteGroupType_static_group)
	})

	t.Run("Create Animated Subscription Emote Group", func(t *testing.T) {
		CreateEmoteGroupWithOriginAndGroupType(t, mk.EmoteGroup_Subscriptions, mk.EmoteGroupType_animated_group)
	})

	t.Run("Create Static BTER Emote Group", func(t *testing.T) {
		CreateEmoteGroupWithOriginAndGroupType(t, mk.EmoteGroup_BitsBadgeTierEmotes, mk.EmoteGroupType_static_group)
	})

	t.Run("Create Static Follower Emote Group", func(t *testing.T) {
		CreateEmoteGroupWithOriginAndGroupType(t, mk.EmoteGroup_Follower, mk.EmoteGroupType_static_group)
	})
}

func CreateEmoteGroupWithOriginAndGroupType(t *testing.T, origin mk.EmoteGroup, groupType mk.EmoteGroupType) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()

	group, err := createEmoteGroupAPI.CreateEmoteGroup(ctx, &mk.CreateEmoteGroupRequest{
		Origin:    origin,
		GroupType: groupType,
		OwnerId:   getIntegrationTestUserID(),
	})
	require.Nil(t, err)
	require.NotNil(t, group)

	err = deleteEmoteGroupAPI.DeleteEmoteGroup(ctx, group.GroupId)
	require.Nil(t, err)
}
