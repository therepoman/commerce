package integration

import (
	"context"
	"net/http"
	"time"

	"code.justin.tv/commerce/mako/api"
	"code.justin.tv/commerce/mako/emote_utils"
	mk "code.justin.tv/commerce/mako/twirp"
	mkd "code.justin.tv/commerce/mako/twirp/dashboard"
	"code.justin.tv/foundation/twitchclient"
	"code.justin.tv/web/upload-service/rpc/uploader"
)

// TODO: refactor all helper utils below to take in a context and a *testing.T param so that we can properly do timeouts
// and properly require that the helpers set the test up as expected. When providing a helper that creates something in
// an integ test, we should also return a cleanUp function which allows us to delete that record when the test is complete.

var httpClient = &http.Client{
	Timeout: 15 * time.Second,
}

// TODO: remove dependency of integ tests on this hardcoded integration test userID. Instead, opt for integration/models.go's
// getIntegrationTestUserID function (and other similar functions) for making integ test users that are specific to just the singular
// test you're writing/running.
const integrationUserID = "262616472"

func createEmoteGroup() (string, error) {
	var makoClient = mk.NewMakoProtobufClient(emote_utils.MakoStaging, httpClient)
	resp, err := makoClient.CreateEmoteGroup(context.Background(), &mk.CreateEmoteGroupRequest{
		Origin:    mk.EmoteGroup_Subscriptions,
		GroupType: mk.EmoteGroupType_static_group,
		OwnerId:   integrationUserID,
	})

	if err != nil {
		return "", err
	}

	return resp.GetGroupId(), nil
}

func uploadEmoticonImages(uploadAPI api.GetEmoteUploadConfigAPI) (map[mkd.EmoteImageSize]mkd.EmoteUploadConfig, error) {
	emote := emote_utils.Emote{
		Regex: "WINSTON",
		Images: map[mkd.EmoteImageSize]string{
			mkd.EmoteImageSize_size_1x: "testdata/28.png",
			mkd.EmoteImageSize_size_2x: "testdata/56.png",
			mkd.EmoteImageSize_size_4x: "testdata/112.png",
		},
	}

	configResp, err := GetUploadConfigForEmote(integrationUserID, false, false, false, uploadAPI)
	if err != nil {
		return nil, err
	}

	// twitchclient does not expose the Client.Timeout field, which covers the entire request
	// Instead, we use context deadlines to cover calls
	twitchHTTPUploadClient := twitchclient.NewHTTPClient(twitchclient.ClientConf{
		Host: emote_utils.UploaderStaging,
	})
	err = emote_utils.UploadImagesForEmote(emote, configResp, httpClient, uploader.NewUploaderProtobufClient(emote_utils.UploaderStaging, twitchHTTPUploadClient))
	if err != nil {
		return nil, err
	}

	uploadConfigs := make(map[mkd.EmoteImageSize]mkd.EmoteUploadConfig)
	for _, uploadConfig := range configResp.GetUploadConfigs() {
		if uploadConfig != nil {
			uploadConfigs[uploadConfig.Size] = *uploadConfig
		}
	}
	return uploadConfigs, err
}

func GetUploadConfigForEmote(ownerId string, autoResize, isAnimated, generateStatic bool, uploadAPI api.GetEmoteUploadConfigAPI) (*mkd.GetEmoteUploadConfigResponse, error) {
	resizePlan := mkd.ResizePlan_no_resize
	var sizes []mkd.EmoteImageSize

	if autoResize {
		resizePlan = mkd.ResizePlan_auto_resize
		sizes = append(sizes, mkd.EmoteImageSize_size_original)
	} else {
		sizes = append(sizes, mkd.EmoteImageSize_size_1x, mkd.EmoteImageSize_size_2x, mkd.EmoteImageSize_size_4x)
	}

	assetType := mkd.EmoteAssetType_static

	if isAnimated {
		assetType = mkd.EmoteAssetType_animated
	}

	req := &mkd.GetEmoteUploadConfigRequest{
		UserId:                                ownerId,
		ResizePlan:                            resizePlan,
		Sizes:                                 sizes,
		AssetType:                             assetType,
		GenerateStaticVersionOfAnimatedAssets: generateStatic,
	}

	return uploadAPI.GetEmoteUploadConfig(context.Background(), req)
}
