package integration

import (
	"context"
	"testing"

	mk "code.justin.tv/commerce/mako/twirp"
	mkd "code.justin.tv/commerce/mako/twirp/dashboard"
	. "github.com/smartystreets/goconvey/convey"
)

func TestDeactivateEmoticonAPI(t *testing.T) {
	t.Skip()

	Convey("Test DeactivateEmoticon", t, func() {
		Convey("With empty id", func() {
			req := &mk.DeactivateEmoticonRequest{
				Id: "",
			}

			_, err := deactivateEmoticonAPI.DeactivateEmoticon(context.Background(), req)
			So(err, ShouldNotBeNil)
		})

		Convey("with success", func() {
			Convey("create emote for deactivation", func() {
				emoteRegex := "LUCCA"
				emoteSetId, err := createEmoteGroup()
				So(err, ShouldBeNil)

				uploadConfigurations, err := uploadEmoticonImages(getEmoteUploadConfigAPI)
				So(err, ShouldBeNil)

				res, err := createEmoticonAPI.CreateEmoticon(context.Background(), &mk.CreateEmoticonRequest{
					UserId:     integrationUserID,
					Code:       emoteRegex,
					CodeSuffix: emoteRegex,
					State:      "active",
					GroupId:    emoteSetId,
					Image28Id:  uploadConfigurations[mkd.EmoteImageSize_size_1x].Images[0].Id,
					Image56Id:  uploadConfigurations[mkd.EmoteImageSize_size_2x].Images[0].Id,
					Image112Id: uploadConfigurations[mkd.EmoteImageSize_size_4x].Images[0].Id,
				})
				So(err, ShouldBeNil)
				So(res, ShouldNotBeNil)
				So(res.Id, ShouldNotBeNil)

				Convey("deactivate created emote", func() {

					_, err = deactivateEmoticonAPI.DeactivateEmoticon(context.Background(), &mk.DeactivateEmoticonRequest{
						Id: res.Id,
					})
					So(err, ShouldBeNil)
				})
			})
		})
	})
}
