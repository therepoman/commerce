package integration

import (
	"context"
	"testing"

	mako_dashboard "code.justin.tv/commerce/mako/twirp/dashboard"
	"github.com/stretchr/testify/assert"
)

func TestDefaultEmoteImagesAPI_GetDefaultEmoteImages(t *testing.T) {
	t.Run("GIVEN a set of default emote images stored in dynamic config WHEN GetDefaultEmoteImages is called THEN expect default emote images response", func(t *testing.T) {
		//GIVEN
		expectedImages := []*mako_dashboard.DefaultEmoteImage{
			{
				Id:        "91af9b7d-f323-48cb-a0cf-dec0634a3802",
				Name:      "Cat_GG_2",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Cat",
					"GG",
					"Blue",
				},
			},
			{
				Id:        "dccb1b1c-d82c-4956-adbc-f94aa8ecb863",
				Name:      "Cat_GG_3",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Cat",
					"GG",
					"Green",
				},
			},
			{
				Id:        "d839d61f-684e-4c47-a7a0-2378275730e7",
				Name:      "Cat_GG_5",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Cat",
					"GG",
					"Orange",
				},
			},
			{
				Id:        "0dc18044-fcdb-423f-9f2d-f9f9b0b349b9",
				Name:      "Cat_GG_1",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Cat",
					"GG",
					"Purple",
				},
			},
			{
				Id:        "b1e63606-1969-4541-a8c9-8c887cd3a8e0",
				Name:      "Cat_GG_6",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Cat",
					"GG",
					"Red",
				},
			},
			{
				Id:        "a0e77436-229a-4b78-95d2-5fcd79342ea5",
				Name:      "Cat_GG_4",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Cat",
					"GG",
					"Yellow",
				},
			},
			{
				Id:        "22398e95-c055-4783-aa25-d9abe1b6e181",
				Name:      "Cat_HI_2",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Cat",
					"HI",
					"Blue",
				},
			},
			{
				Id:        "480f8d07-df58-44b8-aba7-d2a583521e77",
				Name:      "Cat_HI_3",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Cat",
					"HI",
					"Green",
				},
			},
			{
				Id:        "e15fe364-95ef-4e5f-9631-e211d71ecf0f",
				Name:      "Cat_HI_5",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Cat",
					"HI",
					"Orange",
				},
			},
			{
				Id:        "874f1b47-6f5e-4618-831c-c08abf3da12c",
				Name:      "Cat_HI_1",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Cat",
					"HI",
					"Purple",
				},
			},
			{
				Id:        "3d072690-4ee2-467f-9ef8-670c7969dfd4",
				Name:      "Cat_HI_6",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Cat",
					"HI",
					"Red",
				},
			},
			{
				Id:        "993c953e-9946-44d4-8e37-7f75c7508b40",
				Name:      "Cat_HI_4",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Cat",
					"HI",
					"Yellow",
				},
			},
			{
				Id:        "1941135b-ca01-46ef-8762-1436d4aa9506",
				Name:      "Cat_HYPE_2",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Cat",
					"HYPE",
					"Blue",
				},
			},
			{
				Id:        "686786b2-731e-4d21-9f6c-e1c9e247fd08",
				Name:      "Cat_HYPE_3",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Cat",
					"HYPE",
					"Green",
				},
			},
			{
				Id:        "84ea3abf-9c31-420a-b12c-2650666711fb",
				Name:      "Cat_HYPE_5",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Cat",
					"HYPE",
					"Orange",
				},
			},
			{
				Id:        "aa817226-1a21-4547-a4a6-c4d586bd02bc",
				Name:      "Cat_HYPE_1",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Cat",
					"HYPE",
					"Purple",
				},
			},
			{
				Id:        "9dbb5eda-f19a-4c25-a190-166feeb79f22",
				Name:      "Cat_HYPE_6",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Cat",
					"HYPE",
					"Red",
				},
			},
			{
				Id:        "0a95eb37-3f16-4122-9058-f53cbcd4c3cd",
				Name:      "Cat_HYPE_4",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Cat",
					"HYPE",
					"Yellow",
				},
			},
			{
				Id:        "eb161e93-84e1-46f8-b1bf-ae9178f1905e",
				Name:      "Cat_LOL_2",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Cat",
					"LOL",
					"Blue",
				},
			},
			{
				Id:        "823257a2-4cf5-4861-9d16-369c2d0a0998",
				Name:      "Cat_LOL_3",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Cat",
					"LOL",
					"Green",
				},
			},
			{
				Id:        "40ae6a73-dfe6-4756-b4dd-e84e781791f7",
				Name:      "Cat_LOL_5",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Cat",
					"LOL",
					"Orange",
				},
			},
			{
				Id:        "482a3443-f7a7-4d8f-8c57-84a87cd350d8",
				Name:      "Cat_LOL_1",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Cat",
					"LOL",
					"Purple",
				},
			},
			{
				Id:        "bfa3ded3-6055-4f93-a16e-50cfa0e44a57",
				Name:      "Cat_LOL_6",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Cat",
					"LOL",
					"Red",
				},
			},
			{
				Id:        "6bdad87d-00ba-4aa2-a73b-d2286a7f4bf0",
				Name:      "Cat_LOL_4",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Cat",
					"LOL",
					"Yellow",
				},
			},
			{
				Id:        "944b2788-9c64-4385-816e-9751800f1ca4",
				Name:      "Cat_LOVE_2",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Cat",
					"LOVE",
					"Blue",
				},
			},
			{
				Id:        "e8d43f57-d2e1-432e-8943-4b8b8f59f192",
				Name:      "Cat_LOVE_3",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Cat",
					"LOVE",
					"Green",
				},
			},
			{
				Id:        "fa66abbf-26ad-47b6-910f-f34b4402ccd5",
				Name:      "Cat_LOVE_5",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Cat",
					"LOVE",
					"Orange",
				},
			},
			{
				Id:        "c0bce972-9569-4faa-8794-4097bdf37ad5",
				Name:      "Cat_LOVE_1",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Cat",
					"LOVE",
					"Purple",
				},
			},
			{
				Id:        "a7c40334-cad7-45c9-b7bf-cb517c64b323",
				Name:      "Cat_LOVE_6",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Cat",
					"LOVE",
					"Red",
				},
			},
			{
				Id:        "0e477111-0064-4acd-976f-a5709e0a2e34",
				Name:      "Cat_LOVE_4",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Cat",
					"LOVE",
					"Yellow",
				},
			},
			{
				Id:        "845975a2-0dc9-48fa-8e54-98140df9ceaa",
				Name:      "Dog_GG_2",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Dog",
					"GG",
					"Blue",
				},
			},
			{
				Id:        "a1c14c2a-f24d-49e2-985b-4e10facad8c7",
				Name:      "Dog_GG_3",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Dog",
					"GG",
					"Green",
				},
			},
			{
				Id:        "e85a3b23-13ab-4420-ae95-088f5fb7dfa1",
				Name:      "Dog_GG_5",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Dog",
					"GG",
					"Orange",
				},
			},
			{
				Id:        "c62e20ae-73ac-4808-8e28-d135b5b612dd",
				Name:      "Dog_GG_1",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Dog",
					"GG",
					"Purple",
				},
			},
			{
				Id:        "e3236e92-34d8-4b5c-8191-0e6050830004",
				Name:      "Dog_GG_6",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Dog",
					"GG",
					"Red",
				},
			},
			{
				Id:        "b466559a-c051-4773-861c-97c24e26ccda",
				Name:      "Dog_GG_4",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Dog",
					"GG",
					"Yellow",
				},
			},
			{
				Id:        "35e5843c-8015-4353-ab76-4af23508e77d",
				Name:      "Dog_HI_2",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Dog",
					"HI",
					"Blue",
				},
			},
			{
				Id:        "31b4db2e-ee9a-4d7e-b291-0e4c55836ca6",
				Name:      "Dog_HI_3",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Dog",
					"HI",
					"Green",
				},
			},
			{
				Id:        "56228d8d-b97b-4d55-bdc4-7bdfa3244006",
				Name:      "Dog_HI_5",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Dog",
					"HI",
					"Orange",
				},
			},
			{
				Id:        "7bb58cd1-9364-49ed-bfd7-936e9d777b38",
				Name:      "Dog_HI_1",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Dog",
					"HI",
					"Purple",
				},
			},
			{
				Id:        "4840419d-ea56-4475-816d-53c45154833d",
				Name:      "Dog_HI_6",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Dog",
					"HI",
					"Red",
				},
			},
			{
				Id:        "4c5d57c6-94d2-4a61-ad8b-d094f2f9168e",
				Name:      "Dog_HI_4",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Dog",
					"HI",
					"Yellow",
				},
			},
			{
				Id:        "082bae63-3a4b-4f8b-85cd-e2661045bd5b",
				Name:      "Dog_HYPE_2",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Dog",
					"HYPE",
					"Blue",
				},
			},
			{
				Id:        "3eea832d-f5a3-4214-b74f-37dd05389434",
				Name:      "Dog_HYPE_3",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Dog",
					"HYPE",
					"Green",
				},
			},
			{
				Id:        "431e19ba-104e-441c-952e-7495475b4138",
				Name:      "Dog_HYPE_5",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Dog",
					"HYPE",
					"Orange",
				},
			},
			{
				Id:        "56ef4e48-674e-4a19-a10c-aa9a2aac0712",
				Name:      "Dog_HYPE_1",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Dog",
					"HYPE",
					"Purple",
				},
			},
			{
				Id:        "48b00906-5328-4d0d-8396-d01bf483fba4",
				Name:      "Dog_HYPE_6",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Dog",
					"HYPE",
					"Red",
				},
			},
			{
				Id:        "1d705ee5-7a0f-4100-8a2f-2e7440e1f9d9",
				Name:      "Dog_HYPE_4",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Dog",
					"HYPE",
					"Yellow",
				},
			},
			{
				Id:        "96c3d3af-fd34-42d2-8de1-7ec7bc98f0be",
				Name:      "Dog_LOL_2",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Dog",
					"LOL",
					"Blue",
				},
			},
			{
				Id:        "96db097c-a520-4a83-af1f-90cddcc5279a",
				Name:      "Dog_LOL_3",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Dog",
					"LOL",
					"Green",
				},
			},
			{
				Id:        "8ba221de-5c05-43c7-900a-4b5a8c811674",
				Name:      "Dog_LOL_5",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Dog",
					"LOL",
					"Orange",
				},
			},
			{
				Id:        "2f713957-a005-40c9-a6b2-594663c29849",
				Name:      "Dog_LOL_1",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Dog",
					"LOL",
					"Purple",
				},
			},
			{
				Id:        "d1ed5214-f494-4db6-af3a-87edb8c86edc",
				Name:      "Dog_LOL_6",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Dog",
					"LOL",
					"Red",
				},
			},
			{
				Id:        "be90d428-d1f5-435c-ad44-4d2746d732cc",
				Name:      "Dog_LOL_4",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Dog",
					"LOL",
					"Yellow",
				},
			},
			{
				Id:        "f1409c1c-f629-4ba4-9733-2d2a7b5c3294",
				Name:      "Dog_LOVE_2",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Dog",
					"LOVE",
					"Blue",
				},
			},
			{
				Id:        "f028da87-cc9b-4efd-bba1-636308821bc7",
				Name:      "Dog_LOVE_3",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Dog",
					"LOVE",
					"Green",
				},
			},
			{
				Id:        "2d574c45-6426-4ccc-8a14-49dc90835647",
				Name:      "Dog_LOVE_5",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Dog",
					"LOVE",
					"Orange",
				},
			},
			{
				Id:        "48b78184-1ddb-4eb7-95a6-52641079a8ca",
				Name:      "Dog_LOVE_1",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Dog",
					"LOVE",
					"Purple",
				},
			},
			{
				Id:        "6af207f6-d866-43db-826c-8ed0c7dd67ef",
				Name:      "Dog_LOVE_6",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Dog",
					"LOVE",
					"Red",
				},
			},
			{
				Id:        "5243e4ff-6253-4b0f-8910-7f96b55dec4e",
				Name:      "Dog_LOVE_4",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Dog",
					"LOVE",
					"Yellow",
				},
			},
			{
				Id:        "f1644116-13ba-4df3-94b0-6c488a81c0e8",
				Name:      "Figure_GG_2",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Figure",
					"GG",
					"Blue",
				},
			},
			{
				Id:        "04257e9b-dba6-46cc-8c0d-cbb89e939a86",
				Name:      "Figure_GG_3",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Figure",
					"GG",
					"Green",
				},
			},
			{
				Id:        "7cba6968-efc8-48a9-b807-c63ccf56c58e",
				Name:      "Figure_GG_5",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Figure",
					"GG",
					"Orange",
				},
			},
			{
				Id:        "2ea0a832-5de8-40d0-8bf8-4f0b2fd6946d",
				Name:      "Figure_GG_1",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Figure",
					"GG",
					"Purple",
				},
			},
			{
				Id:        "3e45400f-25cc-4546-bdac-8de864b5538a",
				Name:      "Figure_GG_6",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Figure",
					"GG",
					"Red",
				},
			},
			{
				Id:        "605d9631-b823-4134-b3b4-e9ae6236e183",
				Name:      "Figure_GG_4",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Figure",
					"GG",
					"Yellow",
				},
			},
			{
				Id:        "3b6b9bca-98b3-4127-997d-83ae00639d9c",
				Name:      "Figure_HI_2",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Figure",
					"HI",
					"Blue",
				},
			},
			{
				Id:        "dfe3455e-094f-49e0-927c-aa600407dbbb",
				Name:      "Figure_HI_3",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Figure",
					"HI",
					"Green",
				},
			},
			{
				Id:        "2c3f7695-22c0-44b3-8ec5-92be9f23df0b",
				Name:      "Figure_HI_5",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Figure",
					"HI",
					"Orange",
				},
			},
			{
				Id:        "8055a8ab-46d9-4260-877b-7012a7f0d7c4",
				Name:      "Figure_HI_1",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Figure",
					"HI",
					"Purple",
				},
			},
			{
				Id:        "5c9a1f7a-ffa1-4e49-80e8-1dd30787bdfd",
				Name:      "Figure_HI_6",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Figure",
					"HI",
					"Red",
				},
			},
			{
				Id:        "140239f7-9280-410e-8d1e-fbfb2b277fa0",
				Name:      "Figure_HI_4",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Figure",
					"HI",
					"Yellow",
				},
			},
			{
				Id:        "1620a8f7-5f80-4b11-bf7b-a6e7c233031d",
				Name:      "Figure_HYPE_2",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Figure",
					"HYPE",
					"Blue",
				},
			},
			{
				Id:        "07d43541-ea21-44c2-bc7c-20d7efafc3d6",
				Name:      "Figure_HYPE_3",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Figure",
					"HYPE",
					"Green",
				},
			},
			{
				Id:        "8ef5e635-2aee-4d90-91ae-c2e149bec32d",
				Name:      "Figure_HYPE_5",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Figure",
					"HYPE",
					"Orange",
				},
			},
			{
				Id:        "95283bb1-3f8e-42f6-b7f4-4ac920bb3ad5",
				Name:      "Figure_HYPE_1",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Figure",
					"HYPE",
					"Purple",
				},
			},
			{
				Id:        "671e0a7a-a0b0-41a8-a2ab-e8600587cc7d",
				Name:      "Figure_HYPE_6",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Figure",
					"HYPE",
					"Red",
				},
			},
			{
				Id:        "eef822ad-3a91-48f9-ad1a-1064c5274e70",
				Name:      "Figure_HYPE_4",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Figure",
					"HYPE",
					"Yellow",
				},
			},
			{
				Id:        "2fa08171-7c1c-4aef-8b30-acd37cb2b23c",
				Name:      "Figure_LOL_2",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Figure",
					"LOL",
					"Blue",
				},
			},
			{
				Id:        "96ca1069-7789-416b-ae8c-ce1067e3fc0c",
				Name:      "Figure_LOL_3",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Figure",
					"LOL",
					"Green",
				},
			},
			{
				Id:        "ddc503f6-0b76-4eba-aef2-b267bebd2a95",
				Name:      "Figure_LOL_5",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Figure",
					"LOL",
					"Orange",
				},
			},
			{
				Id:        "7597d4b4-46b1-44b5-90b3-f70ad2f880cc",
				Name:      "Figure_LOL_1",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Figure",
					"LOL",
					"Purple",
				},
			},
			{
				Id:        "e6707a81-2eb0-4d0f-874e-c4d99514cd92",
				Name:      "Figure_LOL_6",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Figure",
					"LOL",
					"Red",
				},
			},
			{
				Id:        "7d6f7dde-9bdf-451e-ab2f-b070b74f227e",
				Name:      "Figure_LOL_4",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Figure",
					"LOL",
					"Yellow",
				},
			},
			{
				Id:        "d18fb7da-49cd-41db-a949-618ee408cc0a",
				Name:      "Figure_LOVE_2",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Figure",
					"LOVE",
					"Blue",
				},
			},
			{
				Id:        "6f66815a-ed73-47ef-afa4-3ab06b470630",
				Name:      "Figure_LOVE_3",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Figure",
					"LOVE",
					"Green",
				},
			},
			{
				Id:        "df112314-7014-4bb4-9a3c-671850f02881",
				Name:      "Figure_LOVE_5",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Figure",
					"LOVE",
					"Orange",
				},
			},
			{
				Id:        "3635a4ed-3487-4907-a10a-1e53f3f004d9",
				Name:      "Figure_LOVE_1",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Figure",
					"LOVE",
					"Purple",
				},
			},
			{
				Id:        "99dc2e45-9e9e-4ece-bc5a-929fa588ac0d",
				Name:      "Figure_LOVE_6",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Figure",
					"LOVE",
					"Red",
				},
			},
			{
				Id:        "b725315d-3c42-412e-9cf6-b212f4e5ab65",
				Name:      "Figure_LOVE_4",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Figure",
					"LOVE",
					"Yellow",
				},
			},
			{
				Id:        "a35635c1-aa02-4e5f-a915-79fa49e5b8ef",
				Name:      "Text_GG_2",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Text",
					"GG",
					"Blue",
				},
			},
			{
				Id:        "416e6cb4-672f-4dd3-bc5b-ac6162cb50e2",
				Name:      "Text_GG_3",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Text",
					"GG",
					"Green",
				},
			},
			{
				Id:        "b68b9d47-2e90-4c86-b3f8-44c7f89d2e96",
				Name:      "Text_GG_5",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Text",
					"GG",
					"Orange",
				},
			},
			{
				Id:        "640a18d7-566d-4e56-87db-85a3fd5ea1eb",
				Name:      "Text_GG_1",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Text",
					"GG",
					"Purple",
				},
			},
			{
				Id:        "5919db15-1459-4b3c-8e74-5a08a1eacbe4",
				Name:      "Text_GG_6",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Text",
					"GG",
					"Red",
				},
			},
			{
				Id:        "fea28be2-9ecf-4511-a16d-72287ea4e96f",
				Name:      "Text_GG_4",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Text",
					"GG",
					"Yellow",
				},
			},
			{
				Id:        "4442f8e2-4517-47b0-91ab-79c6e137455e",
				Name:      "Text_HI_2",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Text",
					"HI",
					"Blue",
				},
			},
			{
				Id:        "1936f444-a218-4e1a-92c0-d8926abca548",
				Name:      "Text_HI_3",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Text",
					"HI",
					"Green",
				},
			},
			{
				Id:        "16231a52-23d9-4251-9f1e-cbaf51207650",
				Name:      "Text_HI_5",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Text",
					"HI",
					"Orange",
				},
			},
			{
				Id:        "b5d6767a-c81a-4f02-be23-b100f83145c5",
				Name:      "Text_HI_1",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Text",
					"HI",
					"Purple",
				},
			},
			{
				Id:        "ba1a89f2-640e-4b76-85ad-d498f9d85ca5",
				Name:      "Text_HI_6",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Text",
					"HI",
					"Red",
				},
			},
			{
				Id:        "d87f7da9-1116-4e73-80c7-62f9af71982e",
				Name:      "Text_HI_4",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Text",
					"HI",
					"Yellow",
				},
			},
			{
				Id:        "ac3224c7-e4ac-44cb-8625-56f4cbead668",
				Name:      "Text_HYPE_2",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Text",
					"HYPE",
					"Blue",
				},
			},
			{
				Id:        "72cbe5e8-1064-4ff0-8d30-e48de8bca425",
				Name:      "Text_HYPE_3",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Text",
					"HYPE",
					"Green",
				},
			},
			{
				Id:        "9a697e02-b95c-42fa-9d38-dce8b8f255dd",
				Name:      "Text_HYPE_5",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Text",
					"HYPE",
					"Orange",
				},
			},
			{
				Id:        "8a62d45d-e82f-414e-949d-b4cc5729d26f",
				Name:      "Text_HYPE_1",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Text",
					"HYPE",
					"Purple",
				},
			},
			{
				Id:        "1e21fc47-a0d8-471d-ad3c-92f8d5bf10a6",
				Name:      "Text_HYPE_6",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Text",
					"HYPE",
					"Red",
				},
			},
			{
				Id:        "8bf5d621-090e-4c46-a563-9ffd563ae40e",
				Name:      "Text_HYPE_4",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Text",
					"HYPE",
					"Yellow",
				},
			},
			{
				Id:        "84e96c88-9599-4b64-8ba2-e5ecd3a8736c",
				Name:      "Text_LOL_2",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Text",
					"LOL",
					"Blue",
				},
			},
			{
				Id:        "344ac396-762a-4bec-9522-f71458c44ca5",
				Name:      "Text_LOL_3",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Text",
					"LOL",
					"Green",
				},
			},
			{
				Id:        "e97ee779-29b9-4217-a938-08e2f36ca775",
				Name:      "Text_LOL_5",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Text",
					"LOL",
					"Orange",
				},
			},
			{
				Id:        "7a78ad2c-494d-4a5f-8287-39ce0d37f25b",
				Name:      "Text_LOL_1",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Text",
					"LOL",
					"Purple",
				},
			},
			{
				Id:        "1f7d56c4-c2a7-4f66-9bd8-e0752dbd2cc3",
				Name:      "Text_LOL_6",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Text",
					"LOL",
					"Red",
				},
			},
			{
				Id:        "22a35e31-6839-4ba3-82b4-c3452fafa61b",
				Name:      "Text_LOL_4",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Text",
					"LOL",
					"Yellow",
				},
			},
			{
				Id:        "ac25c130-42b4-4e91-a27b-58c518a9c569",
				Name:      "Text_LOVE_2",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Text",
					"LOVE",
					"Blue",
				},
			},
			{
				Id:        "11940d6a-12c2-4f47-b3f4-465a34753ff3",
				Name:      "Text_LOVE_3",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Text",
					"LOVE",
					"Green",
				},
			},
			{
				Id:        "5d6a2aa7-6aa7-4222-95ca-a48da7198538",
				Name:      "Text_LOVE_5",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Text",
					"LOVE",
					"Orange",
				},
			},
			{
				Id:        "2826e47e-1dfc-41be-bb0b-671701ea48ee",
				Name:      "Text_LOVE_1",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Text",
					"LOVE",
					"Purple",
				},
			},
			{
				Id:        "e80c2fc4-6ef6-40ec-af19-e513dc71e916",
				Name:      "Text_LOVE_6",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Text",
					"LOVE",
					"Red",
				},
			},
			{
				Id:        "87ae60b9-b3c2-42a0-967d-2127689564a3",
				Name:      "Text_LOVE_4",
				AssetType: mako_dashboard.EmoteAssetType_static,
				Tags: []string{
					"Text",
					"LOVE",
					"Yellow",
				},
			},
		}

		//WHEN
		resp, err := defaultEmoteImagesAPI.GetDefaultEmoteImages(context.TODO(), &mako_dashboard.GetDefaultEmoteImagesRequest{})

		//THEN
		assert.Nil(t, err)
		assert.NotNil(t, resp)
		assert.NotNil(t, resp.DefaultEmoteImages)
		assert.Subset(t, resp.DefaultEmoteImages, expectedImages, "assert has original 120")
	})
}
