package integration

import (
	"os"
	"testing"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/mako/api"
	"code.justin.tv/commerce/mako/clients"
	"code.justin.tv/commerce/mako/config"
	"code.justin.tv/commerce/mako/prefixes"
	"code.justin.tv/commerce/mako/setup"
	"github.com/cactus/go-statsd-client/statsd"
)

var (
	entitlementsAPI                 api.EntitlementsAPI
	activateEmoticonAPI             api.ActivateEmoticonAPI
	healthAPI                       api.HealthAPI
	getEmoteSetDetailsAPI           api.EmoteSetDetailsAPI
	getEmotesByCodesAPI             api.GetEmotesByCodesAPI
	emoticonsByGroupsAPI            api.EmoticonsByGroupsAPI
	emoticonsByIdsAPI               api.EmoticonsByEmoticonIdsAPI
	getAllEmoticonsByOwnerIdAPI     api.EmoticonsByOwnerAPI
	activeEmoteCodesAPI             api.ActiveEmoteCodesAPI
	createEmoticonAPI               api.CreateEmoticonAPI
	createEmoteAPI                  api.CreateEmoteAPI
	createEmoteGroupAPI             api.CreateEmoteGroupAPI
	deleteEmoteGroupAPI             api.DeleteEmoteGroupAPI
	createEmoteGroupEntitlementsAPI api.CreateEmoteGroupEntitlementsAPI
	deleteEmoteGroupEntitlementsAPI api.DeleteEmoteGroupEntitlementsAPI
	getEmoteUploadConfigAPI         api.GetEmoteUploadConfigAPI
	deactivateEmoticonAPI           api.DeactivateEmoticonAPI
	deleteEmoticonAPI               api.DeleteEmoticonAPI
	userEmoteStandingAPI            api.UserEmoteStandingAPI
	smiliesAPI                      api.SmiliesAPI
	prefixesAPI                     api.PrefixesAPI
	krakenEmoticonsAPI              api.LegacyEmoticonsAPI
	updateEmoteCodeSuffixAPI        api.UpdateEmoteCodeSuffixAPI
	emoteGroupAssignAPI             api.EmoteGroupAssignAPI
	defaultEmoteImagesAPI           api.DefaultEmoteImagesAPI
	makoClients                     *clients.Clients
	makoDaos                        *clients.DataAccessObjects
	cfg                             *config.Configuration
)

func TestMain(m *testing.M) {
	os.Setenv("INTEGRATION", "true")

	setupParams := setup.SetupMako()
	cfg = setupParams.Config

	setupAPI(setupParams.Config, setupParams.Daos, setupParams.Stats, setupParams.Clients, setupParams.DynamicConfig, setupParams.DynamicS3Configs)

	testSuiteResult := m.Run()

	log.Info("Integration tests complete.")
	os.Exit(testSuiteResult)
}

func setupAPI(cfg *config.Configuration, daos *clients.DataAccessObjects, stats statsd.Statter, clients *clients.Clients, dynamicConfig *config.Dynamic, dynamicS3Configs *config.DynamicS3Configs) {
	entitlementsAPI = api.EntitlementsAPI{}

	getEmoteSetDetailsAPI = api.EmoteSetDetailsAPI{
		Clients: *clients,
		Config:  &config.Configuration{MateriaDenylist: map[string]bool{}},
	}

	getEmotesByCodesAPI = api.GetEmotesByCodesAPI{
		Clients:       *clients,
		Stats:         stats,
		DynamicConfig: dynamicConfig,
	}

	emoticonsByIdsAPI = api.EmoticonsByEmoticonIdsAPI{
		Clients: *clients,
	}

	emoticonsByGroupsAPI = api.EmoticonsByGroupsAPI{
		Clients: *clients,
		Stats:   stats,
	}

	getAllEmoticonsByOwnerIdAPI = api.EmoticonsByOwnerAPI{
		EmoteDB: clients.EmoteDB,
	}

	healthAPI = api.HealthAPI{}

	activeEmoteCodesAPI = api.ActiveEmoteCodesAPI{}

	getEmoteUploadConfigAPI = api.NewGetEmoteUploadConfigAPI(cfg, clients.ImageUploader, clients.EmoteUploadMetadataCache)

	createEmoticonAPI = api.CreateEmoticonAPI{
		Clients: *clients,
		Stats:   stats,
	}

	createEmoteAPI = api.CreateEmoteAPI{
		DynamicConfig: dynamicConfig,
		Clients:       *clients,
		Stats:         stats,
	}

	createEmoteGroupAPI = api.CreateEmoteGroupAPI{
		Clients: *clients,
	}

	deleteEmoteGroupAPI = api.DeleteEmoteGroupAPI{
		Clients: *clients,
	}

	createEmoteGroupEntitlementsAPI = api.CreateEmoteGroupEntitlementsAPI{
		Clients: *clients,
	}

	deleteEmoteGroupEntitlementsAPI = api.DeleteEmoteGroupEntitlementsAPI{
		Clients: *clients,
	}

	deactivateEmoticonAPI = api.DeactivateEmoticonAPI{
		Clients: *clients,
	}

	activateEmoticonAPI = api.ActivateEmoticonAPI{
		Clients: *clients,
	}

	deleteEmoticonAPI = api.DeleteEmoticonAPI{
		Clients: *clients,
	}

	updateEmoteCodeSuffixAPI = api.UpdateEmoteCodeSuffixAPI{
		Clients: *clients,
	}

	userEmoteStandingAPI = api.UserEmoteStandingAPI{
		Clients: *clients,
	}

	smiliesAPI = api.SmiliesAPI{
		Clients: *clients,
	}

	defaultEmoteImagesAPI = api.DefaultEmoteImagesAPI{
		DefaultEmoteImagesDataDynamicConfig: dynamicS3Configs.DefaultEmoteImageDataDynamicS3Config,
	}

	statsdClient, _ := statsd.NewNoop()
	prefixesRecommender := prefixes.NewPrefixRecommender(daos.PrefixDao, statsdClient, clients)

	prefixesAPI = api.NewPrefixesAPI(daos.PrefixDao, cfg, clients, prefixesRecommender)

	krakenEmoticonsAPI = api.LegacyEmoticonsAPI{
		Clients: *clients,
	}

	emoteGroupAssignAPI = api.EmoteGroupAssignAPI{
		EmoticonsManager: clients.EmoticonsManager,
	}

	makoClients = clients
	makoDaos = daos
}
