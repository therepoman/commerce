package integration

import (
	"testing"

	mako "code.justin.tv/commerce/mako/internal"

	"context"

	mk "code.justin.tv/commerce/mako/twirp"
	. "github.com/smartystreets/goconvey/convey"
)

func TestEmoticonsByEmoticonIdsAPI(t *testing.T) {
	Convey("Test Emoticons By Emoticon Ids API", t, func() {
		Convey("Test GetEmoticonsByEmoticonIds API for Global Emote", func() {
			queriedIds := []string{mako.KappaEmoteID}
			expected := mk.Emoticon{
				Id:      mako.KappaEmoteID,
				Code:    "Kappa",
				GroupId: mako.GlobalEmoteGroupID,
			}

			request := &mk.GetEmoticonsByEmoticonIdsRequest{EmoticonIds: queriedIds}

			resp, err := emoticonsByIdsAPI.GetEmoticonsByEmoticonIds(context.Background(), request)
			So(err, ShouldBeNil)
			So(len(resp.Emoticons), ShouldEqual, 1)
			So(resp.Emoticons[0].Id, ShouldEqual, expected.Id)
			So(resp.Emoticons[0].Code, ShouldEqual, expected.Code)
			So(resp.Emoticons[0].GroupId, ShouldEqual, expected.GroupId)
		})

		Convey("Test GetEmoticonsByEmoticonIds API for Subscriber Emote", func() {
			queriedIds := []string{"894717"}
			expected := mk.Emoticon{
				Id:      "894717",
				Code:    "OWLnice",
				GroupId: "695138",
				OwnerId: "412022192",
			}

			request := &mk.GetEmoticonsByEmoticonIdsRequest{EmoticonIds: queriedIds}

			resp, err := emoticonsByIdsAPI.GetEmoticonsByEmoticonIds(context.Background(), request)
			So(err, ShouldBeNil)
			So(len(resp.Emoticons), ShouldEqual, 1)
			So(resp.Emoticons[0].Id, ShouldEqual, expected.Id)
			So(resp.Emoticons[0].Code, ShouldEqual, expected.Code)
			So(resp.Emoticons[0].GroupId, ShouldEqual, expected.GroupId)
			So(resp.Emoticons[0].OwnerId, ShouldEqual, expected.OwnerId)
		})

		Convey("Test GetEmoticonsByEmoticonIds API for inactive ticket product Subscriber Emote", func() {
			queriedIds := []string{"788214"}
			expected := mk.Emoticon{
				Id:      "788214",
				Code:    "OWLDVa",
				GroupId: "605142",
				OwnerId: "139075904",
			}

			request := &mk.GetEmoticonsByEmoticonIdsRequest{EmoticonIds: queriedIds}

			resp, err := emoticonsByIdsAPI.GetEmoticonsByEmoticonIds(context.Background(), request)
			So(err, ShouldBeNil)
			So(len(resp.Emoticons), ShouldEqual, 1)
			So(resp.Emoticons[0].Id, ShouldEqual, expected.Id)
			So(resp.Emoticons[0].Code, ShouldEqual, expected.Code)
			So(resp.Emoticons[0].GroupId, ShouldEqual, expected.GroupId)
			So(resp.Emoticons[0].OwnerId, ShouldEqual, expected.OwnerId)
		})
	})
}
