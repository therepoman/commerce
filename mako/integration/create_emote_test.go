package integration

import (
	"context"
	"testing"
	"time"

	"code.justin.tv/commerce/mako/emote_utils"
	mako "code.justin.tv/commerce/mako/internal"
	mk "code.justin.tv/commerce/mako/twirp"
	mkd "code.justin.tv/commerce/mako/twirp/dashboard"
	"github.com/segmentio/ksuid"
	"github.com/stretchr/testify/require"
	"golang.org/x/sync/errgroup"
)

type UploadParams struct {
	EmoteCodePrefix              string
	EmoteCodeSuffix              string
	OwnerID                      string
	IsGlobalOrSmilie             bool
	EmoteGroup                   mk.EmoteGroup
	GroupType                    mk.EmoteGroupType
	AssetType                    mkd.EmoteAssetType
	Origin                       mkd.EmoteOrigin
	State                        mkd.EmoteState
	ShouldBeInEmoteGoodStanding  bool
	EnforceModerationWorkflow    bool
	ExpectEmoteByIDToReturnEmote bool
	ExpectEmoteToHaveEmoteGroup  bool
	ExpectCreateEmoteFailure     bool
}

// Currently used for passing the parts of one emote code on to a subsequent test
type UploadResp struct {
	EmoteID     string
	OwnerID     string
	GroupID     string
	EmotePrefix string
	EmoteSuffix string
	Code        string
}

func TestUploadAndCreateEmote(t *testing.T) {
	tests := []struct {
		scenario string
		test     func(ctx context.Context, t *testing.T)
	}{
		{
			scenario: "StaticSubEmoteAutoResize",
			test: func(ctx context.Context, t *testing.T) {
				_, cleanUp := UploadAndCreateAutoResizeEmote(ctx, t, UploadParams{
					EmoteGroup:                   mk.EmoteGroup_Subscriptions,
					GroupType:                    mk.EmoteGroupType_static_group,
					AssetType:                    mkd.EmoteAssetType_static,
					Origin:                       mkd.EmoteOrigin_Subscriptions,
					State:                        mkd.EmoteState_active,
					ExpectEmoteByIDToReturnEmote: true,
					ExpectEmoteToHaveEmoteGroup:  true,
				})

				cleanUp()
			},
		},

		{
			scenario: "AnimatedSubEmoteAutoResize",
			test: func(ctx context.Context, t *testing.T) {
				_, cleanUp := UploadAndCreateAutoResizeEmote(ctx, t, UploadParams{
					EmoteGroup:                   mk.EmoteGroup_Subscriptions,
					GroupType:                    mk.EmoteGroupType_animated_group,
					AssetType:                    mkd.EmoteAssetType_animated,
					Origin:                       mkd.EmoteOrigin_Subscriptions,
					State:                        mkd.EmoteState_active,
					ExpectEmoteByIDToReturnEmote: true,
					ExpectEmoteToHaveEmoteGroup:  true,
				})

				cleanUp()
			},
		},

		{
			scenario: "BitsBadgeTierEmoteAutoResize",
			test: func(ctx context.Context, t *testing.T) {
				_, cleanUp := UploadAndCreateAutoResizeEmote(ctx, t, UploadParams{
					EmoteGroup:                   mk.EmoteGroup_BitsBadgeTierEmotes,
					GroupType:                    mk.EmoteGroupType_static_group,
					AssetType:                    mkd.EmoteAssetType_static,
					Origin:                       mkd.EmoteOrigin_BitsBadgeTierEmotes,
					State:                        mkd.EmoteState_active,
					ExpectEmoteByIDToReturnEmote: true,
					ExpectEmoteToHaveEmoteGroup:  true,
				})

				cleanUp()
			},
		},

		{
			scenario: "FollowerEmoteAutoResize",
			test: func(ctx context.Context, t *testing.T) {
				_, cleanUp := UploadAndCreateAutoResizeEmote(ctx, t, UploadParams{
					EmoteGroup:                   mk.EmoteGroup_Follower,
					GroupType:                    mk.EmoteGroupType_static_group,
					AssetType:                    mkd.EmoteAssetType_static,
					Origin:                       mkd.EmoteOrigin_Follower,
					State:                        mkd.EmoteState_active,
					ExpectEmoteByIDToReturnEmote: true,
					ExpectEmoteToHaveEmoteGroup:  true,
				})

				cleanUp()
			},
		},

		{
			scenario: "A follower emote for a user outside the experiment flag should be denied permission",
			test: func(ctx context.Context, t *testing.T) {
				_, cleanUp := UploadAndCreateAutoResizeEmote(ctx, t, UploadParams{
					// We check for whether the userID is a normal integration test user, which has a special integration test prefix
					// on the ownerID. Here, we create an integration test user who is not in the experiment allow-list, nor are they
					// in the special integration test prefix case, so we should receive an error back telling us we're not allowed to upload.
					OwnerID:                     ksuid.New().String(),
					EmoteGroup:                  mk.EmoteGroup_Subscriptions,
					GroupType:                   mk.EmoteGroupType_static_group,
					AssetType:                   mkd.EmoteAssetType_static,
					Origin:                      mkd.EmoteOrigin_Subscriptions,
					State:                       mkd.EmoteState_active,
					ExpectEmoteToHaveEmoteGroup: true,
					ExpectCreateEmoteFailure:    true,
				})

				cleanUp()
			},
		},

		{
			scenario: "A follower emote for a user not in good standing should be denied permission",
			test: func(ctx context.Context, t *testing.T) {
				_, cleanUp := UploadAndCreateAutoResizeEmote(ctx, t, UploadParams{
					EmoteGroup:                  mk.EmoteGroup_Subscriptions,
					GroupType:                   mk.EmoteGroupType_static_group,
					AssetType:                   mkd.EmoteAssetType_static,
					Origin:                      mkd.EmoteOrigin_Subscriptions,
					State:                       mkd.EmoteState_active,
					ShouldBeInEmoteGoodStanding: false,
					ExpectEmoteToHaveEmoteGroup: true,
					EnforceModerationWorkflow:   true,
					ExpectCreateEmoteFailure:    true,
				})

				cleanUp()
			},
		},

		{
			scenario: "A follower emote for a user in the experiment in good standing should be successful",
			test: func(ctx context.Context, t *testing.T) {
				_, cleanUp := UploadAndCreateAutoResizeEmote(ctx, t, UploadParams{
					EmoteGroup:                   mk.EmoteGroup_Subscriptions,
					GroupType:                    mk.EmoteGroupType_static_group,
					AssetType:                    mkd.EmoteAssetType_static,
					Origin:                       mkd.EmoteOrigin_Subscriptions,
					State:                        mkd.EmoteState_active,
					ShouldBeInEmoteGoodStanding:  true,
					ExpectEmoteToHaveEmoteGroup:  true,
					EnforceModerationWorkflow:    true,
					ExpectEmoteByIDToReturnEmote: true,
				})

				cleanUp()
			},
		},

		{
			scenario: "ArchiveEmoteAutoResize",
			test: func(ctx context.Context, t *testing.T) {
				_, cleanUp := UploadAndCreateAutoResizeEmote(ctx, t, UploadParams{
					EmoteGroup:                   mk.EmoteGroup_Archive,
					AssetType:                    mkd.EmoteAssetType_static,
					Origin:                       mkd.EmoteOrigin_Archive,
					State:                        mkd.EmoteState_archived,
					ExpectEmoteByIDToReturnEmote: false,
					ExpectEmoteToHaveEmoteGroup:  false,
				})

				cleanUp()
			},
		},

		{
			scenario: "Two of the same active emote code should be impossible",
			test: func(ctx context.Context, t *testing.T) {
				resp, cleanUp := UploadAndCreateAutoResizeEmote(ctx, t, UploadParams{
					EmoteGroup:                   mk.EmoteGroup_Subscriptions,
					GroupType:                    mk.EmoteGroupType_static_group,
					AssetType:                    mkd.EmoteAssetType_static,
					Origin:                       mkd.EmoteOrigin_Subscriptions,
					State:                        mkd.EmoteState_active,
					ExpectEmoteByIDToReturnEmote: true,
					ExpectEmoteToHaveEmoteGroup:  true,
				})
				_, cleanUp2 := UploadAndCreateAutoResizeEmote(ctx, t, UploadParams{
					EmoteCodePrefix:          resp.EmotePrefix,
					EmoteCodeSuffix:          resp.EmoteSuffix,
					OwnerID:                  resp.OwnerID,
					EmoteGroup:               mk.EmoteGroup_Subscriptions,
					GroupType:                mk.EmoteGroupType_static_group,
					AssetType:                mkd.EmoteAssetType_static,
					Origin:                   mkd.EmoteOrigin_Subscriptions,
					State:                    mkd.EmoteState_active,
					ExpectCreateEmoteFailure: true,
				})

				cleanUp()
				cleanUp2()
			},
		},

		{
			scenario: "An archive emote can have the same code as an active emote",
			test: func(ctx context.Context, t *testing.T) {
				resp, cleanUp := UploadAndCreateAutoResizeEmote(ctx, t, UploadParams{
					EmoteGroup:                   mk.EmoteGroup_Subscriptions,
					GroupType:                    mk.EmoteGroupType_static_group,
					AssetType:                    mkd.EmoteAssetType_static,
					Origin:                       mkd.EmoteOrigin_Subscriptions,
					State:                        mkd.EmoteState_active,
					ExpectEmoteByIDToReturnEmote: true,
					ExpectEmoteToHaveEmoteGroup:  true,
				})
				_, cleanUp2 := UploadAndCreateAutoResizeEmote(ctx, t, UploadParams{
					EmoteCodePrefix: resp.EmotePrefix,
					EmoteCodeSuffix: resp.EmoteSuffix,
					OwnerID:         resp.OwnerID,
					EmoteGroup:      mk.EmoteGroup_Archive,
					AssetType:       mkd.EmoteAssetType_static,
					Origin:          mkd.EmoteOrigin_Archive,
					State:           mkd.EmoteState_archived,
				})

				cleanUp()
				cleanUp2()
			},
		},

		{
			scenario: "An active emote can have the same code as an archive emote",
			test: func(ctx context.Context, t *testing.T) {
				resp, cleanUp := UploadAndCreateAutoResizeEmote(ctx, t, UploadParams{
					EmoteGroup: mk.EmoteGroup_Archive,
					AssetType:  mkd.EmoteAssetType_static,
					Origin:     mkd.EmoteOrigin_Archive,
					State:      mkd.EmoteState_archived,
				})
				_, cleanUp2 := UploadAndCreateAutoResizeEmote(ctx, t, UploadParams{
					EmoteCodePrefix:              resp.EmotePrefix,
					EmoteCodeSuffix:              resp.EmoteSuffix,
					OwnerID:                      resp.OwnerID,
					EmoteGroup:                   mk.EmoteGroup_Subscriptions,
					GroupType:                    mk.EmoteGroupType_static_group,
					AssetType:                    mkd.EmoteAssetType_static,
					Origin:                       mkd.EmoteOrigin_Subscriptions,
					State:                        mkd.EmoteState_active,
					ExpectEmoteByIDToReturnEmote: true,
					ExpectEmoteToHaveEmoteGroup:  true,
				})

				cleanUp()
				cleanUp2()
			},
		},

		{
			scenario: "Test create with a default emote",
			test: func(ctx context.Context, t *testing.T) {
				_, cleanUp := CreateEmoteFromDefaultLibraryImageSource(ctx, t, UploadParams{
					EmoteGroup:                   mk.EmoteGroup_Subscriptions,
					GroupType:                    mk.EmoteGroupType_static_group,
					AssetType:                    mkd.EmoteAssetType_static,
					Origin:                       mkd.EmoteOrigin_Subscriptions,
					State:                        mkd.EmoteState_active,
					ExpectEmoteByIDToReturnEmote: true,
					ExpectEmoteToHaveEmoteGroup:  true,
				})

				cleanUp()
			},
		},
	}

	for _, test := range tests {
		t.Run(test.scenario, func(t *testing.T) {
			t.Parallel()

			ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
			test.test(ctx, t)
			cancel()
		})
	}
}

func UploadAndCreateAutoResizeEmote(ctx context.Context, t *testing.T, params UploadParams) (UploadResp, func()) {
	var err error

	ownerID := params.OwnerID
	if params.OwnerID == "" {
		ownerID = getIntegrationTestUserID()
	}

	eg := errgroup.Group{}

	var groupID string
	if params.ExpectEmoteToHaveEmoteGroup {
		// Non-archive emotes must always be in group's slot, so we create one before creating the emote
		eg.Go(func() error {
			group, err := createEmoteGroupAPI.CreateEmoteGroup(ctx, &mk.CreateEmoteGroupRequest{
				Origin:    params.EmoteGroup,
				GroupType: params.GroupType,
				OwnerId:   ownerID,
			})
			require.Nil(t, err)
			require.NotNil(t, group)
			groupID = group.GroupId
			return err
		})
	} else if params.IsGlobalOrSmilie {
		// We are making an assumption here that the global emote groupID will always be 0 and will never be deleted
		groupID = mako.GlobalEmoteGroupID
	} else {
		groupID = ""
	}

	// Creator Emotes require a prefix, which is looked up during creation, so either use the one we've been given or set a new one
	// For non-creator emotes like Globals, use (ShouldNotHaveAPrefix: true) as a way to bypass prefix creation
	prefix := params.EmoteCodePrefix
	if prefix == "" && !params.IsGlobalOrSmilie {
		eg.Go(func() error {
			prefix = getIntegrationTestEmotePrefix()
			prefixResp, err := prefixesAPI.SetPrefix(ctx, &mk.SetPrefixRequest{
				ChannelId: ownerID,
				Prefix:    prefix,
				State:     mk.PrefixState_PREFIX_ACTIVE,
			})
			require.Nil(t, err)
			require.NotNil(t, prefixResp)
			return err
		})
	}

	// Uploading is done in three parts: get emote upload config, upload images, and create emote
	var uploadConfigResp *mkd.GetEmoteUploadConfigResponse
	eg.Go(func() error {
		uploadConfigResp, err = getEmoteUploadConfigAPI.GetEmoteUploadConfig(ctx, &mkd.GetEmoteUploadConfigRequest{
			UserId:                                ownerID,
			ResizePlan:                            mkd.ResizePlan_auto_resize,
			Sizes:                                 []mkd.EmoteImageSize{mkd.EmoteImageSize_size_original},
			AssetType:                             params.AssetType,
			GenerateStaticVersionOfAnimatedAssets: params.AssetType == mkd.EmoteAssetType_animated,
		})
		require.NotNil(t, uploadConfigResp)
		require.Len(t, uploadConfigResp.UploadConfigs, 1)
		require.Nil(t, err)
		return err
	})

	err = eg.Wait()
	require.Nil(t, err)

	// Here we upload the images
	config := uploadConfigResp.UploadConfigs[0]
	if params.AssetType == mkd.EmoteAssetType_animated {
		err = emote_utils.UploadImageForEmoteToStaging(ctx, "testdata/dax4x.gif", config.Url, config.Id)
		require.Nil(t, err)
	} else {
		err = emote_utils.UploadImageForEmoteToStaging(ctx, "testdata/dax4x.png", config.Url, config.Id)
		require.Nil(t, err)
	}

	// To create the emote we need to get the imageIDs from our upload config
	staticIds := mkd.EmoteImageIds{}
	animatedIds := mkd.EmoteImageIds{}
	for _, img := range config.Images {
		if img.AssetType == mkd.EmoteAssetType_animated {
			if img.Size == mkd.EmoteImageSize_size_1x {
				animatedIds.Image1XId = img.Id
			} else if img.Size == mkd.EmoteImageSize_size_2x {
				animatedIds.Image2XId = img.Id
			} else if img.Size == mkd.EmoteImageSize_size_4x {
				animatedIds.Image4XId = img.Id
			}
		} else if img.AssetType == mkd.EmoteAssetType_static {
			if img.Size == mkd.EmoteImageSize_size_1x {
				staticIds.Image1XId = img.Id
			} else if img.Size == mkd.EmoteImageSize_size_2x {
				staticIds.Image2XId = img.Id
			} else if img.Size == mkd.EmoteImageSize_size_4x {
				staticIds.Image4XId = img.Id
			}
		}
	}

	imgAssets := []*mkd.EmoteImageAsset{
		{
			AssetType: mkd.EmoteAssetType_static,
			ImageIds:  &staticIds,
		},
	}

	// Only append the animated-constructed image assets if we're trying to upload an animated emote
	if params.AssetType == mkd.EmoteAssetType_animated {
		imgAssets = append(imgAssets, &mkd.EmoteImageAsset{
			AssetType: mkd.EmoteAssetType_animated,
			ImageIds:  &animatedIds,
		})
	}

	codeSuffix := getIntegrationTestEmoteSuffix()
	code := getIntegrationTestEmoteCode(prefix, codeSuffix)

	var groupIDs []string
	if groupID != "" {
		// We don't want to pass "" as the groupID
		groupIDs = append(groupIDs, groupID)
	}

	// Let the prefix, group, and imageIDs propagate as necessary before CreateEmote reads from their dynamo tables
	time.Sleep(1 * time.Second)

	createResp, err := createEmoteAPI.CreateEmote(ctx, &mkd.CreateEmoteRequest{
		UserId:                    ownerID,
		GroupIds:                  groupIDs,
		Code:                      code,
		CodeSuffix:                codeSuffix,
		ImageAssets:               imgAssets,
		AssetType:                 params.AssetType,
		Origin:                    params.Origin,
		State:                     params.State,
		EnforceModerationWorkflow: params.EnforceModerationWorkflow,
		ShouldIgnorePrefix:        params.IsGlobalOrSmilie,
	})

	if params.ExpectCreateEmoteFailure {
		require.NotNil(t, err)
		require.Nil(t, createResp)
	} else {
		require.Nil(t, err)
		require.NotNil(t, createResp)
		require.NotNil(t, createResp.Emote)
		require.Equal(t, code, createResp.Emote.Code)
		require.Equal(t, codeSuffix, createResp.Emote.Suffix)
		require.Equal(t, params.State, createResp.Emote.State)
		require.Equal(t, params.AssetType, createResp.Emote.AssetType)
		require.Equal(t, params.Origin, createResp.Emote.Origin)

		if params.ExpectEmoteToHaveEmoteGroup || params.IsGlobalOrSmilie {
			require.Equal(t, groupID, createResp.GroupIds[0])
		}

		if params.ExpectEmoteByIDToReturnEmote {
			// We need about a second or two for the negative caching in emotes by id to catch up.
			time.Sleep(2 * time.Second)

			getResp, err := emoticonsByIdsAPI.GetEmoticonsByEmoticonIds(ctx, &mk.GetEmoticonsByEmoticonIdsRequest{
				EmoticonIds: []string{createResp.Id},
			})

			require.Nil(t, err)
			require.NotNil(t, getResp)
			require.Len(t, getResp.Emoticons, 1)
		}
	}

	cleanUpGroupID := groupID
	if params.IsGlobalOrSmilie {
		// Do not delete the global/smilie groupID, since it is hardcoded as 0
		cleanUpGroupID = ""
	}

	return UploadResp{
			EmoteID:     createResp.GetEmote().GetId(),
			GroupID:     groupID,
			EmotePrefix: prefix,
			EmoteSuffix: codeSuffix,
			OwnerID:     ownerID,
			Code:        createResp.GetEmote().GetCode(),
		}, func() {
			cleanUpCreateEmote(createResp.GetEmote().GetId(), cleanUpGroupID, prefix, ownerID)
		}
}

func CreateEmoteFromDefaultLibraryImageSource(ctx context.Context, t *testing.T, params UploadParams) (UploadResp, func()) {
	var err error

	ownerID := params.OwnerID
	if params.OwnerID == "" {
		ownerID = getIntegrationTestUserID()
	}

	var groupID string
	if params.ExpectEmoteToHaveEmoteGroup {
		// Non-archive emotes must always be in group's slot, so we create one before creating the emote
		group, err := createEmoteGroupAPI.CreateEmoteGroup(ctx, &mk.CreateEmoteGroupRequest{
			Origin:    params.EmoteGroup,
			GroupType: params.GroupType,
			OwnerId:   ownerID,
		})
		require.Nil(t, err)
		require.NotNil(t, group)
		groupID = group.GroupId
	} else if params.IsGlobalOrSmilie {
		// We are making an assumption here that the global emote groupID will always be 0 and will never be deleted
		groupID = mako.GlobalEmoteGroupID
	} else {
		groupID = ""
	}

	// Creator Emotes require a prefix, which is looked up during creation, so either use the one we've been given or set a new one
	// For non-creator emotes like Globals, use (ShouldNotHaveAPrefix: true) as a way to bypass prefix creation
	prefix := params.EmoteCodePrefix
	if prefix == "" && !params.IsGlobalOrSmilie {
		prefix = getIntegrationTestEmotePrefix()
		prefixResp, err := prefixesAPI.SetPrefix(ctx, &mk.SetPrefixRequest{
			ChannelId: ownerID,
			Prefix:    prefix,
			State:     mk.PrefixState_PREFIX_ACTIVE,
		})
		require.Nil(t, err)
		require.NotNil(t, prefixResp)
	}

	// To create the emote we need to get the imageIDs from our upload config
	staticIds := mkd.EmoteImageIds{}
	staticIds.Image1XId = getIntegrationTestDefaultImageID() + "-1.0"
	staticIds.Image2XId = getIntegrationTestDefaultImageID() + "-2.0"
	staticIds.Image4XId = getIntegrationTestDefaultImageID() + "-3.0"

	imgAssets := []*mkd.EmoteImageAsset{
		{
			AssetType: mkd.EmoteAssetType_static,
			ImageIds:  &staticIds,
		},
	}

	codeSuffix := getIntegrationTestEmoteSuffix()
	code := getIntegrationTestEmoteCode(prefix, codeSuffix)

	var groupIDs []string
	if groupID != "" {
		// We don't want to pass "" as the groupID
		groupIDs = append(groupIDs, groupID)
	}

	// Let the prefix, group, and imageIDs propagate as necessary before CreateEmote reads from their dynamo tables
	time.Sleep(1 * time.Second)

	createResp, err := createEmoteAPI.CreateEmote(ctx, &mkd.CreateEmoteRequest{
		UserId:                    ownerID,
		GroupIds:                  groupIDs,
		Code:                      code,
		CodeSuffix:                codeSuffix,
		ImageAssets:               imgAssets,
		AssetType:                 params.AssetType,
		Origin:                    params.Origin,
		State:                     params.State,
		EnforceModerationWorkflow: params.EnforceModerationWorkflow,
		ShouldIgnorePrefix:        params.IsGlobalOrSmilie,
		ImageSource:               mkd.ImageSource_DefaultLibraryImage,
	})

	if params.ExpectCreateEmoteFailure {
		require.NotNil(t, err)
		require.Nil(t, createResp)
	} else {
		require.Nil(t, err)
		require.NotNil(t, createResp)
		require.NotNil(t, createResp.Emote)
		require.Equal(t, code, createResp.Emote.Code)
		require.Equal(t, codeSuffix, createResp.Emote.Suffix)
		require.Equal(t, params.State, createResp.Emote.State)
		require.Equal(t, params.AssetType, createResp.Emote.AssetType)
		require.Equal(t, params.Origin, createResp.Emote.Origin)

		if params.ExpectEmoteToHaveEmoteGroup || params.IsGlobalOrSmilie {
			require.Equal(t, groupID, createResp.GroupIds[0])
		}

		if params.ExpectEmoteByIDToReturnEmote {
			// We need about a second or two for the negative caching in emotes by id to catch up.
			time.Sleep(2 * time.Second)

			getResp, err := emoticonsByIdsAPI.GetEmoticonsByEmoticonIds(ctx, &mk.GetEmoticonsByEmoticonIdsRequest{
				EmoticonIds: []string{createResp.Id},
			})

			require.Nil(t, err)
			require.NotNil(t, getResp)
			require.Len(t, getResp.Emoticons, 1)
		}
	}

	cleanUpGroupID := groupID
	if params.IsGlobalOrSmilie {
		// Do not delete the global/smilie groupID, since it is hardcoded as 0
		cleanUpGroupID = ""
	}

	return UploadResp{
			EmoteID:     createResp.GetEmote().GetId(),
			GroupID:     groupID,
			EmotePrefix: prefix,
			EmoteSuffix: codeSuffix,
			OwnerID:     ownerID,
			Code:        createResp.GetEmote().GetCode(),
		}, func() {
			cleanUpCreateEmote(createResp.GetEmote().GetId(), cleanUpGroupID, prefix, ownerID)
		}
}

func cleanUpCreateEmote(emoteID, groupID, prefix, prefixOwnerID string) {
	go func(emoteID, groupID, prefix, prefixOwnerID string) {
		if emoteID != "" {
			_, _ = deleteEmoticonAPI.DeleteEmoticon(context.Background(), &mk.DeleteEmoticonRequest{
				Id: emoteID,
			})
		}

		if groupID != "" {
			_ = deleteEmoteGroupAPI.DeleteEmoteGroup(context.Background(), groupID)
		}

		if prefixOwnerID != "" {
			_ = prefixesAPI.DeletePrefix(context.Background(), prefix, prefixOwnerID)
		}
	}(emoteID, groupID, prefix, prefixOwnerID)
}
