package integration

import (
	"context"
	"fmt"
	"io/ioutil"
	"testing"

	mk "code.justin.tv/commerce/mako/twirp"
	mkd "code.justin.tv/commerce/mako/twirp/dashboard"
	awsS3 "github.com/aws/aws-sdk-go/service/s3"
	. "github.com/smartystreets/goconvey/convey"
)

func TestCreateEmoticonAPI(t *testing.T) {
	t.Skip()

	Convey("Test CreateEmoticon", t, func() {
		Convey("With empty userId", func() {
			req := &mk.CreateEmoticonRequest{
				UserId: "",
			}

			_, err := createEmoticonAPI.CreateEmoticon(context.Background(), req)
			So(err, ShouldNotBeNil)
		})

		Convey("with success", func() {
			emoteRegex := "WINSTON"
			emoteSetId, err := createEmoteGroup()
			So(err, ShouldBeNil)

			uploadConfigurations, err := uploadEmoticonImages(getEmoteUploadConfigAPI)
			So(err, ShouldBeNil)

			res, err := createEmoticonAPI.CreateEmoticon(context.Background(), &mk.CreateEmoticonRequest{
				UserId:     integrationUserID,
				Code:       emoteRegex,
				CodeSuffix: emoteRegex,
				State:      "active",
				GroupId:    emoteSetId,
				Image28Id:  uploadConfigurations[mkd.EmoteImageSize_size_1x].Images[0].Id,
				Image56Id:  uploadConfigurations[mkd.EmoteImageSize_size_2x].Images[0].Id,
				Image112Id: uploadConfigurations[mkd.EmoteImageSize_size_4x].Images[0].Id,
			})

			So(err, ShouldBeNil)
			So(res, ShouldNotBeNil)
			So(res.Id, ShouldNotBeEmpty)
			So(res.State, ShouldEqual, "active")

			// list of scales used to create s3 key identifier to call for corresponding s3 objects
			scales := make([][]string, 3)
			scales[0] = []string{"0.5", "1.0"}
			scales[1] = []string{"1.5", "2.0"}
			scales[2] = []string{"2.5", "3.0", "3.5", "4.0", "5.0"}

			// checking that all 3 emotes created are in S3 and not empty (for every scale)
			for _, scale := range scales {
				for _, scaleId := range scale {
					emoticonKey := fmt.Sprintf("emoticon%s-%s.png", res.Id, scaleId)
					bucketName := cfg.UploadS3BucketName
					awsGetObjInput := &awsS3.GetObjectInput{
						Bucket: &bucketName,
						Key:    &emoticonKey,
					}
					result, err := makoClients.S3Client.GetObject(context.Background(), awsGetObjInput)
					So(err, ShouldBeNil)
					body, err := ioutil.ReadAll(result.Body)
					So(err, ShouldBeNil)
					So(body, ShouldNotBeNil)
					So(len(body), ShouldNotEqual, 0)
				}
			}
		})

		Convey("with failure due to invalid image upload", func() {
			emoteRegex := "BrokenImg"
			s3UploadImage := "2600000ForIntegrationTesting"
			emoteSetId, err := createEmoteGroup()
			So(err, ShouldBeNil)

			res, err := createEmoticonAPI.CreateEmoticon(context.Background(), &mk.CreateEmoticonRequest{
				UserId:     integrationUserID,
				Code:       emoteRegex,
				CodeSuffix: emoteRegex,
				State:      "active",
				GroupId:    emoteSetId,
				Image28Id:  s3UploadImage,
				Image56Id:  s3UploadImage,
				Image112Id: s3UploadImage,
			})

			// validation for invalid image sent an err
			So(err.Error(), ShouldEqual, "twirp error invalid_argument: image invalid image uploaded")
			So(res, ShouldBeNil)
		})
	})
}
