package integration

import (
	"context"
	"testing"

	mkd "code.justin.tv/commerce/mako/twirp/dashboard"
	. "github.com/smartystreets/goconvey/convey"
)

func TestGetEmoteUploadConfiguAPI(t *testing.T) {
	Convey("Test GetEmoteUploadConfigAPI", t, func() {

		Convey("Test UploadEmoticon", func() {
			Convey("With empty userID", func() {
				req := &mkd.GetEmoteUploadConfigRequest{
					UserId: "",
				}

				_, err := getEmoteUploadConfigAPI.GetEmoteUploadConfig(context.Background(), req)
				So(err, ShouldNotBeNil)
			})

			Convey("With incorrect dimensions", func() {
				req := &mkd.GetEmoteUploadConfigRequest{
					UserId: "123",
					Sizes:  []mkd.EmoteImageSize{mkd.EmoteImageSize(-1)},
				}

				_, err := getEmoteUploadConfigAPI.GetEmoteUploadConfig(context.Background(), req)
				So(err, ShouldNotBeNil)
			})

			Convey("With Success", func() {
				req := &mkd.GetEmoteUploadConfigRequest{
					UserId: "123",
					Sizes:  []mkd.EmoteImageSize{mkd.EmoteImageSize_size_1x},
				}

				_, err := getEmoteUploadConfigAPI.GetEmoteUploadConfig(context.Background(), req)
				So(err, ShouldBeNil)
			})
		})
	})
}
