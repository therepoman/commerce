package integration

import (
	"testing"

	mk "code.justin.tv/commerce/mako/twirp"
	. "github.com/smartystreets/goconvey/convey"

	"context"
)

func TestLegacyEmoticonsAPI(t *testing.T) {
	Convey("Test Legacy Emoticons API", t, func() {
		Convey("Test GetLegacyChannelEmoticons", func() {
			Convey("With success", func() {
				request := &mk.GetLegacyChannelEmoticonsRequest{
					ChannelId:   "1",
					ChannelName: "test_channel",
				}

				expected := &mk.LegacyChannelEmoticonsEmoticon{
					Regex:  "Kappa",
					State:  "active",
					Url:    "https://static-cdn.jtvnw.net/emoticons/v1/25/1.0",
					Height: 28,
					Width:  28,
				}

				response, err := krakenEmoticonsAPI.GetLegacyChannelEmoticons(context.Background(), request)
				So(err, ShouldBeNil)
				So(response, ShouldNotBeNil)
				So(response.Emoticons, ShouldContain, expected)
			})
		})
	})
}
