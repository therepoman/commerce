package integration

import (
	"context"
	"testing"

	mk "code.justin.tv/commerce/mako/twirp"
	. "github.com/smartystreets/goconvey/convey"
)

func TestSmiliesAPI(t *testing.T) {
	Convey("Test Smilies API", t, func() {
		Convey("Test GetAllSmilies API", func() {
			allSmilies := []*mk.SmiliesSet{
				{
					SmiliesSetId: mk.SmiliesSetId_ROBOTS,
					Emoticons: []*mk.Emoticon{
						{
							Id:   "1",
							Code: ":)",
						},
						{
							Id:   "2",
							Code: ":(",
						},
						{
							Id:   "3",
							Code: ":D",
						},
						{
							Id:   "4",
							Code: ">(",
						},
						{
							Id:   "5",
							Code: ":|",
						},
						{
							Id:   "6",
							Code: "O_o",
						},
						{
							Id:   "7",
							Code: "B)",
						},
						{
							Id:   "8",
							Code: ":O",
						},
						{
							Id:   "9",
							Code: "<3",
						},
						{
							Id:   "10",
							Code: ":/",
						},
						{
							Id:   "11",
							Code: ";)",
						},
						{
							Id:   "12",
							Code: ":P",
						},
						{
							Id:   "13",
							Code: ";P",
						},
						{
							Id:   "14",
							Code: "R)",
						},
					},
				},
				{
					SmiliesSetId: mk.SmiliesSetId_PURPLE,
					Emoticons: []*mk.Emoticon{
						{
							Id:   "432",
							Code: ">(",
						},
						{
							Id:   "439",
							Code: ";)",
						},
						{
							Id:   "440",
							Code: ":)",
						},
						{
							Id:   "441",
							Code: "B)",
						},
						{
							Id:   "442",
							Code: ";P",
						},
						{
							Id:   "443",
							Code: ":D",
						},
						{
							Id:   "444",
							Code: ":|",
						},
						{
							Id:   "445",
							Code: "<3",
						},
						{
							Id:   "438",
							Code: ":P",
						},
						{
							Id:   "436",
							Code: ":O",
						},

						{
							Id:   "435",
							Code: "R)",
						},
						{
							Id:   "434",
							Code: ":(",
						},
						{
							Id:   "433",
							Code: ":/",
						},
						{
							Id:   "437",
							Code: "O_o",
						},
					},
				},
				{
					SmiliesSetId: mk.SmiliesSetId_MONKEYS,
					Emoticons: []*mk.Emoticon{
						{
							Id:   "497",
							Code: "O_o",
						},
						{
							Id:   "493",
							Code: ":/",
						},
						{
							Id:   "487",
							Code: "<]",
						},
						{
							Id:   "485",
							Code: "#/",
						},
						{
							Id:   "501",
							Code: ";)",
						},
						{
							Id:   "500",
							Code: "B)",
						},
						{
							Id:   "499",
							Code: ":)",
						},
						{
							Id:   "498",
							Code: ">(",
						},
						{
							Id:   "496",
							Code: ":D",
						},
						{
							Id:   "495",
							Code: ":s",
						},
						{
							Id:   "494",
							Code: ":|",
						},
						{
							Id:   "492",
							Code: ":O",
						},
						{
							Id:   "491",
							Code: ";P",
						},
						{
							Id:   "490",
							Code: ":P",
						},
						{
							Id:   "489",
							Code: ":(",
						},
						{
							Id:   "488",
							Code: ":7",
						},
						{
							Id:   "486",
							Code: ":>",
						},
						{
							Id:   "484",
							Code: "R)",
						},
						{
							Id:   "483",
							Code: "<3",
						},
					},
				},
			}

			Convey("With Success", func() {
				request := &mk.GetAllSmiliesRequest{}

				response, err := smiliesAPI.GetAllSmilies(context.Background(), request)
				So(err, ShouldBeNil)
				for i, set := range response.SmiliesSets {
					So(set, ShouldNotBeNil)
					So(set.SmiliesSetId, ShouldEqual, allSmilies[i].SmiliesSetId)

					for j, emote := range set.Emoticons {
						So(emote.Id, ShouldEqual, allSmilies[i].Emoticons[j].Id)
						So(emote.Code, ShouldEqual, allSmilies[i].Emoticons[j].Code)
					}
				}
			})
		})

		Convey("Test GetSelectedSmilies API", func() {
			Convey("With Required Params", func() {
				request := &mk.GetSelectedSmiliesRequest{
					UserId: "123456",
				}

				response, err := smiliesAPI.GetSelectedSmilies(context.Background(), request)
				So(err, ShouldBeNil)
				So(response.SmiliesSetId, ShouldEqual, mk.SmiliesSetId_ROBOTS)
			})

			Convey("With missing userID", func() {
				request := &mk.GetSelectedSmiliesRequest{}

				_, err := smiliesAPI.GetSelectedSmilies(context.Background(), request)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("Test SetSmilies API", func() {
			Convey("With missing userID", func() {
				request := &mk.SetSmiliesRequest{
					SmiliesSetId: mk.SmiliesSetId_MONKEYS,
				}

				_, err := smiliesAPI.SetSmilies(context.Background(), request)
				So(err, ShouldNotBeNil)
			})

			Convey("With missing set id", func() {
				request := &mk.SetSmiliesRequest{
					UserId: "123456",
				}

				_, err := smiliesAPI.SetSmilies(context.Background(), request)
				So(err, ShouldNotBeNil)
			})

			Convey("With Required Args", func() {
				request := &mk.SetSmiliesRequest{
					UserId:       "36138783",
					SmiliesSetId: mk.SmiliesSetId_MONKEYS,
				}

				response, err := smiliesAPI.SetSmilies(context.Background(), request)
				So(err, ShouldBeNil)
				So(response.SmiliesSetId, ShouldEqual, mk.SmiliesSetId_MONKEYS)
			})
		})

		request := &mk.SetSmiliesRequest{
			UserId:       "36138783",
			SmiliesSetId: mk.SmiliesSetId_ROBOTS,
		}

		//Clean up, ensure smilies is set to default
		response, err := smiliesAPI.SetSmilies(context.Background(), request)
		So(err, ShouldBeNil)
		So(response.SmiliesSetId, ShouldEqual, mk.SmiliesSetId_ROBOTS)
	})
}
