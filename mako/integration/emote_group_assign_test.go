package integration

import (
	"context"
	"testing"
	"time"

	"code.justin.tv/commerce/mako/emote_utils"
	makoClient "code.justin.tv/commerce/mako/twirp"
	mako_dashboard "code.justin.tv/commerce/mako/twirp/dashboard"
	"github.com/stretchr/testify/require"
)

func TestAssignEmoteToGroup(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()

	ownerID := getIntegrationTestUserID()
	prefix := getIntegrationTestEmotePrefix()

	prefixResp, err := prefixesAPI.SetPrefix(ctx, &makoClient.SetPrefixRequest{
		ChannelId: ownerID,
		Prefix:    prefix,
		State:     makoClient.PrefixState_PREFIX_ACTIVE,
	})
	require.Nil(t, err)
	require.NotNil(t, prefixResp)

	res, err := getEmoteUploadConfigAPI.GetEmoteUploadConfig(ctx, &mako_dashboard.GetEmoteUploadConfigRequest{
		UserId:     ownerID,
		ResizePlan: mako_dashboard.ResizePlan_auto_resize,
		Sizes:      []mako_dashboard.EmoteImageSize{mako_dashboard.EmoteImageSize_size_original},
		AssetType:  mako_dashboard.EmoteAssetType_static,
	})

	require.Nil(t, err)
	require.Len(t, res.UploadConfigs, 1)

	config := res.UploadConfigs[0]

	err = emote_utils.UploadImageForEmoteToStaging(ctx, "testdata/dax4x.png", config.Url, config.Id)

	require.Nil(t, err)

	imgIds := mako_dashboard.EmoteImageIds{}

	for _, img := range config.Images {
		if img.Size == mako_dashboard.EmoteImageSize_size_1x {
			imgIds.Image1XId = img.Id
		} else if img.Size == mako_dashboard.EmoteImageSize_size_2x {
			imgIds.Image2XId = img.Id
		} else if img.Size == mako_dashboard.EmoteImageSize_size_4x {
			imgIds.Image4XId = img.Id
		}
	}

	imgAssets := []*mako_dashboard.EmoteImageAsset{
		{
			AssetType: mako_dashboard.EmoteAssetType_static,
			ImageIds:  &imgIds,
		},
	}

	codeSuffix := getIntegrationTestEmoteSuffix()
	code := getIntegrationTestEmoteCode(prefix, codeSuffix)

	createResp, err := createEmoteAPI.CreateEmote(ctx, &mako_dashboard.CreateEmoteRequest{
		UserId:                    ownerID,
		GroupIds:                  nil,
		CodeSuffix:                codeSuffix,
		ImageAssets:               imgAssets,
		AssetType:                 mako_dashboard.EmoteAssetType_static,
		Origin:                    mako_dashboard.EmoteOrigin_Archive,
		State:                     mako_dashboard.EmoteState_archived,
		EnforceModerationWorkflow: false,
	})

	defer func() {
		_, _ = deleteEmoticonAPI.DeleteEmoticon(ctx, &makoClient.DeleteEmoticonRequest{
			Id:           createResp.Id,
			Code:         code,
			DeleteImages: true,
		})
	}()

	require.Nil(t, err)
	require.NotNil(t, createResp)
	require.NotNil(t, createResp.Emote)
	require.Equal(t, mako_dashboard.EmoteState_archived, createResp.Emote.State)
	require.Empty(t, createResp.Emote.GroupId)

	// ok, so we need about a second for the negative caching in emotes by id to catch up.
	time.Sleep(1 * time.Second)

	group, err := createEmoteGroupAPI.CreateEmoteGroup(ctx, &makoClient.CreateEmoteGroupRequest{
		Origin:    makoClient.EmoteGroup_Subscriptions,
		GroupType: makoClient.EmoteGroupType_static_group,
		OwnerId:   ownerID,
	})
	require.Nil(t, err)
	require.NotNil(t, group)
	require.NotNil(t, group.GetGroupId())

	assignResp, err := emoteGroupAssignAPI.AssignEmoteToGroup(ctx, &mako_dashboard.AssignEmoteToGroupRequest{
		RequestingUserId: ownerID,
		EmoteId:          createResp.Id,
		GroupId:          group.GetGroupId(),
		Origin:           mako_dashboard.EmoteOrigin_Subscriptions,
	})

	require.Nil(t, err)
	require.NotNil(t, assignResp)
	require.NotNil(t, assignResp.Emote)
	require.Equal(t, createResp.Id, assignResp.Emote.Id)
	require.Equal(t, mako_dashboard.EmoteState_active, assignResp.Emote.State)
	require.Equal(t, group.GetGroupId(), assignResp.Emote.GroupId)
}

func TestRemoveEmoteFromGroup(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()

	ownerID := getIntegrationTestUserID()
	prefix := getIntegrationTestEmotePrefix()

	prefixResp, err := prefixesAPI.SetPrefix(ctx, &makoClient.SetPrefixRequest{
		ChannelId: ownerID,
		Prefix:    prefix,
		State:     makoClient.PrefixState_PREFIX_ACTIVE,
	})
	require.Nil(t, err)
	require.NotNil(t, prefixResp)

	res, err := getEmoteUploadConfigAPI.GetEmoteUploadConfig(ctx, &mako_dashboard.GetEmoteUploadConfigRequest{
		UserId:     ownerID,
		ResizePlan: mako_dashboard.ResizePlan_auto_resize,
		Sizes:      []mako_dashboard.EmoteImageSize{mako_dashboard.EmoteImageSize_size_original},
		AssetType:  mako_dashboard.EmoteAssetType_static,
	})

	require.Nil(t, err)
	require.Len(t, res.UploadConfigs, 1)

	config := res.UploadConfigs[0]

	err = emote_utils.UploadImageForEmoteToStaging(ctx, "testdata/dax4x.png", config.Url, config.Id)

	require.Nil(t, err)

	imgIds := mako_dashboard.EmoteImageIds{}

	for _, img := range config.Images {
		if img.Size == mako_dashboard.EmoteImageSize_size_1x {
			imgIds.Image1XId = img.Id
		} else if img.Size == mako_dashboard.EmoteImageSize_size_2x {
			imgIds.Image2XId = img.Id
		} else if img.Size == mako_dashboard.EmoteImageSize_size_4x {
			imgIds.Image4XId = img.Id
		}
	}

	imgAssets := []*mako_dashboard.EmoteImageAsset{
		{
			AssetType: mako_dashboard.EmoteAssetType_static,
			ImageIds:  &imgIds,
		},
	}

	codeSuffix := getIntegrationTestEmoteSuffix()
	code := getIntegrationTestEmoteCode(prefix, codeSuffix)

	group, err := createEmoteGroupAPI.CreateEmoteGroup(ctx, &makoClient.CreateEmoteGroupRequest{
		Origin:    makoClient.EmoteGroup_Subscriptions,
		GroupType: makoClient.EmoteGroupType_static_group,
		OwnerId:   ownerID,
	})
	require.Nil(t, err)
	require.NotNil(t, group)
	require.NotNil(t, group.GetGroupId())

	createResp, err := createEmoteAPI.CreateEmote(ctx, &mako_dashboard.CreateEmoteRequest{
		UserId:                    ownerID,
		GroupIds:                  []string{group.GetGroupId()},
		CodeSuffix:                codeSuffix,
		ImageAssets:               imgAssets,
		AssetType:                 mako_dashboard.EmoteAssetType_static,
		Origin:                    mako_dashboard.EmoteOrigin_Subscriptions,
		State:                     mako_dashboard.EmoteState_active,
		EnforceModerationWorkflow: false,
	})

	defer func() {
		_, _ = deleteEmoticonAPI.DeleteEmoticon(ctx, &makoClient.DeleteEmoticonRequest{
			Id:           createResp.Id,
			Code:         code,
			DeleteImages: true,
		})
	}()

	require.Nil(t, err)
	require.NotNil(t, createResp)
	require.NotNil(t, createResp.Emote)
	require.Equal(t, mako_dashboard.EmoteState_active, createResp.Emote.State)
	require.Equal(t, group.GetGroupId(), createResp.Emote.GroupId)

	// ok, so we need about a second for the negative caching in emotes by id to catch up.
	time.Sleep(1 * time.Second)

	removeResp, err := emoteGroupAssignAPI.RemoveEmoteFromGroup(ctx, &mako_dashboard.RemoveEmoteFromGroupRequest{
		RequestingUserId: ownerID,
		EmoteId:          createResp.Id,
	})

	require.Nil(t, err)
	require.NotNil(t, removeResp)
	require.NotNil(t, removeResp.Emote)
	require.Equal(t, createResp.Id, removeResp.Emote.Id)
	require.Equal(t, mako_dashboard.EmoteState_archived, removeResp.Emote.State)
	require.Empty(t, removeResp.Emote.GroupId)
}
