package integration

import (
	"context"
	"fmt"
	"math/rand"
	"strconv"
	"testing"
	"time"

	mako "code.justin.tv/commerce/mako/internal"
	mk "code.justin.tv/commerce/mako/twirp"
	mkd "code.justin.tv/commerce/mako/twirp/dashboard"
	. "github.com/smartystreets/goconvey/convey"
)

func TestUpdateEmoteCodeSuffixAPI(t *testing.T) {
	Convey("Test UpdateEmoteCodeSuffixAPI", t, func() {
		Convey("With empty id", func() {
			req := &mk.UpdateEmoteCodeSuffixRequest{
				Id:         "",
				CodeSuffix: "",
			}
			_, err := updateEmoteCodeSuffixAPI.UpdateEmoteCodeSuffix(context.Background(), req)
			So(err, ShouldNotBeNil)
		})

		Convey("With empty codesuffix", func() {
			req := &mk.UpdateEmoteCodeSuffixRequest{
				Id:         "1234",
				CodeSuffix: "",
			}
			_, err := updateEmoteCodeSuffixAPI.UpdateEmoteCodeSuffix(context.Background(), req)
			So(err, ShouldNotBeNil)
		})

		Convey("with success", func() {
			Convey("create emote for update", func() {
				emotePrefix := "integ"
				emoteSuffix := generateSuffix()
				emoteCode := fmt.Sprintf("%v%v", emotePrefix, emoteSuffix)

				updatedSuffix := generateSuffix()

				emoteSetId, err := createEmoteGroup()
				So(err, ShouldBeNil)

				uploadConfigurations, err := uploadEmoticonImages(getEmoteUploadConfigAPI)
				So(err, ShouldBeNil)

				res, err := createEmoticonAPI.CreateEmoticon(context.Background(), &mk.CreateEmoticonRequest{
					UserId:     integrationUserID,
					Code:       emoteCode,
					CodeSuffix: emoteSuffix,
					State:      string(mako.Active),
					GroupId:    emoteSetId,
					Domain:     string(mako.EmotesDomain),
					Image28Id:  uploadConfigurations[mkd.EmoteImageSize_size_1x].Images[0].Id,
					Image56Id:  uploadConfigurations[mkd.EmoteImageSize_size_2x].Images[0].Id,
					Image112Id: uploadConfigurations[mkd.EmoteImageSize_size_4x].Images[0].Id,
				})

				So(err, ShouldBeNil)
				So(res, ShouldNotBeNil)
				So(res.Id, ShouldNotBeNil)

				Convey("update created emote", func() {
					req := &mk.UpdateEmoteCodeSuffixRequest{
						Id:         res.Id,
						CodeSuffix: updatedSuffix,
					}

					_, err := updateEmoteCodeSuffixAPI.UpdateEmoteCodeSuffix(context.Background(), req)
					So(err, ShouldBeNil)
				})
				_, err = deleteEmoticonAPI.DeleteEmoticon(context.Background(), &mk.DeleteEmoticonRequest{
					Id:           res.Id,
					DeleteImages: true,
				})
				So(err, ShouldBeNil)
			})
		})
	})
}

func generateSuffix() string {
	maxInt := 10000
	rand.Seed(time.Now().UnixNano())
	return fmt.Sprintf("Suffix%v", strconv.Itoa(rand.Intn(maxInt)))
}
