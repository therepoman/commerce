package integration

import (
	"context"
	"testing"
	"time"

	mako "code.justin.tv/commerce/mako/internal"
	mk "code.justin.tv/commerce/mako/twirp"
	mkd "code.justin.tv/commerce/mako/twirp/dashboard"
	"github.com/segmentio/ksuid"
	"github.com/stretchr/testify/require"
)

type EmotesByCodesIntegTestParms struct {
	userID                     string
	shouldNotGenerateUserID    bool
	channelID                  string
	shouldNotGenerateChannelID bool
	codes                      []string
	shouldExpectError          bool
	shouldExpectEmptyResponse  bool
	expectedEmoteIDs           []string // emotes that should be returned by this GetEmotesByCodes call
	unexpectedEmoteIDs         []string // emotes which should not be returned by this GetEmotesByCodes call
}

func TestGetEmotesByCodesAPI(t *testing.T) {
	tests := []struct {
		scenario string
		test     func(ctx context.Context, t *testing.T)
	}{
		{
			scenario: "Should return nothing when there is no matching emote code",
			test: func(ctx context.Context, t *testing.T) {
				// Just call with random gibberish, which probably isn't an emote code
				getEmotesByCodes(t, ctx, EmotesByCodesIntegTestParms{
					codes:                     []string{ksuid.New().String()},
					shouldExpectEmptyResponse: true,
				})
			},
		},
		{
			scenario: "Should always tokenize globals",
			test: func(ctx context.Context, t *testing.T) {
				// Create a global emote
				uploadResp, cleanUp := UploadAndCreateAutoResizeEmote(ctx, t, UploadParams{
					// Global emotes have no prefix, only a suffix
					EmoteCodePrefix:              "",
					IsGlobalOrSmilie:             true,
					OwnerID:                      mako.TwitchOwnerID,
					Origin:                       mkd.EmoteOrigin_Globals,
					State:                        mkd.EmoteState_active,
					ExpectEmoteByIDToReturnEmote: true,
					ExpectEmoteToHaveEmoteGroup:  false,
				})

				defer cleanUp()

				getEmotesByCodes(t, ctx, EmotesByCodesIntegTestParms{
					userID:                     getIntegrationTestUserID(),
					channelID:                  "",
					shouldNotGenerateChannelID: true,
					codes:                      []string{uploadResp.Code},
					expectedEmoteIDs: []string{
						uploadResp.EmoteID,
					},
					unexpectedEmoteIDs: nil,
				})
			},
		},
		{
			scenario: "Should tokenize static subscription emotes if the user is entitled to them",
			test: func(ctx context.Context, t *testing.T) {
				// Create a static subscription emote
				uploadResp, cleanUpEmote := UploadAndCreateAutoResizeEmote(ctx, t, UploadParams{
					EmoteGroup:                   mk.EmoteGroup_Subscriptions,
					GroupType:                    mk.EmoteGroupType_static_group,
					AssetType:                    mkd.EmoteAssetType_static,
					Origin:                       mkd.EmoteOrigin_Subscriptions,
					State:                        mkd.EmoteState_active,
					ExpectEmoteByIDToReturnEmote: true,
					ExpectEmoteToHaveEmoteGroup:  true,
				})
				defer cleanUpEmote()

				viewerID := getIntegrationTestUserID()
				creatorID := uploadResp.OwnerID

				cleanupEntitlement := createEmoteGroupEntitlements(ctx, t, createEntitlementsParams{
					viewerID:  viewerID,
					creatorID: creatorID,
					groupID:   uploadResp.GroupID,
					originID:  "Subscriptions",
					group:     mk.EmoteGroup_Subscriptions,
				})
				defer cleanupEntitlement()

				getEmotesByCodes(t, ctx, EmotesByCodesIntegTestParms{
					userID: viewerID,
					// Subscription emotes should tokenize even if no channelID is provided
					channelID:                  "",
					shouldNotGenerateChannelID: true,
					codes:                      []string{uploadResp.Code},
					expectedEmoteIDs: []string{
						uploadResp.EmoteID,
					},
				})
			},
		},
		{
			scenario: "Should load animated subscription emotes if the user is entitled to them",
			test: func(ctx context.Context, t *testing.T) {
				// Create a animated subscription emote
				uploadResp, cleanUpEmote := UploadAndCreateAutoResizeEmote(ctx, t, UploadParams{
					EmoteGroup:                   mk.EmoteGroup_Subscriptions,
					GroupType:                    mk.EmoteGroupType_animated_group,
					AssetType:                    mkd.EmoteAssetType_animated,
					Origin:                       mkd.EmoteOrigin_Subscriptions,
					State:                        mkd.EmoteState_active,
					ExpectEmoteByIDToReturnEmote: true,
					ExpectEmoteToHaveEmoteGroup:  true,
				})
				defer cleanUpEmote()

				viewerID := getIntegrationTestUserID()
				creatorID := uploadResp.OwnerID

				cleanUpEntitlement := createEmoteGroupEntitlements(ctx, t, createEntitlementsParams{
					viewerID:  viewerID,
					creatorID: creatorID,
					groupID:   uploadResp.GroupID,
					originID:  "Subscriptions",
					group:     mk.EmoteGroup_Subscriptions,
				})
				defer cleanUpEntitlement()

				// Give time to let the entitlement propagate before trying to read it below
				time.Sleep(time.Second)

				getEmotesByCodes(t, ctx, EmotesByCodesIntegTestParms{
					userID: viewerID,
					// Subscription emotes should load even if no channelID is provided
					channelID:                  "",
					shouldNotGenerateChannelID: true,
					codes:                      []string{uploadResp.Code},
					expectedEmoteIDs: []string{
						uploadResp.EmoteID,
					},
				})

			},
		},
		{
			scenario: "Should tokenize this channel's follower emotes if the user is following this channel",
			test: func(ctx context.Context, t *testing.T) {
				// We have logic in the follower entitlementsDB that returns isEntitled if both the viewerID and creatorID
				// have the prefix for integration test users
				viewerID := getIntegrationTestUserID()
				creatorID := getIntegrationTestUserID()

				// Create a follower emote
				uploadResp, cleanUpEmote := UploadAndCreateAutoResizeEmote(ctx, t, UploadParams{
					OwnerID:                      creatorID,
					EmoteGroup:                   mk.EmoteGroup_Follower,
					GroupType:                    mk.EmoteGroupType_static_group,
					AssetType:                    mkd.EmoteAssetType_static,
					Origin:                       mkd.EmoteOrigin_Follower,
					State:                        mkd.EmoteState_active,
					ExpectEmoteByIDToReturnEmote: true,
					ExpectEmoteToHaveEmoteGroup:  true,
				})
				defer cleanUpEmote()

				getEmotesByCodes(t, ctx, EmotesByCodesIntegTestParms{
					userID:    viewerID,
					channelID: creatorID,
					codes:     []string{uploadResp.Code},
					expectedEmoteIDs: []string{
						uploadResp.EmoteID,
					},
				})
			},
		},
		{
			scenario: "Should not tokenize any follower emotes if the user is not following this channel",
			test: func(ctx context.Context, t *testing.T) {
				// We set the viewerID to be something without the integration test user prefix, so this will actually
				// call follows service in staging for a pair of users which don't exist, which will mean that we get a
				// negative response, meaning that the user is not following the channel
				viewerID := ksuid.New().String()
				creatorID := getIntegrationTestUserID()

				// Create a follower emote
				uploadResp, cleanUpEmote := UploadAndCreateAutoResizeEmote(ctx, t, UploadParams{
					OwnerID:                      creatorID,
					EmoteGroup:                   mk.EmoteGroup_Follower,
					GroupType:                    mk.EmoteGroupType_static_group,
					AssetType:                    mkd.EmoteAssetType_static,
					Origin:                       mkd.EmoteOrigin_Follower,
					State:                        mkd.EmoteState_active,
					ExpectEmoteByIDToReturnEmote: true,
					ExpectEmoteToHaveEmoteGroup:  true,
				})
				cleanUpEmote()

				getEmotesByCodes(t, ctx, EmotesByCodesIntegTestParms{
					userID:    viewerID,
					channelID: creatorID,
					codes:     []string{uploadResp.Code},
					unexpectedEmoteIDs: []string{
						uploadResp.EmoteID,
					},
					shouldExpectEmptyResponse: true,
				})
			},
		},
		{
			scenario: "Should tokenize this channel's follower emotes if the user is the same as the channel",
			test: func(ctx context.Context, t *testing.T) {
				// We set the creatorID and the viewerID to be the same, meaning we should short-circuit before calling
				// follows service, since every channel is treated as though it is following itself for the purposes of
				// follower emotes
				creatorID := getIntegrationTestUserID()
				viewerID := creatorID

				// Create a follower emote
				uploadResp, cleanUpEmote := UploadAndCreateAutoResizeEmote(ctx, t, UploadParams{
					OwnerID:                      creatorID,
					EmoteGroup:                   mk.EmoteGroup_Follower,
					GroupType:                    mk.EmoteGroupType_static_group,
					AssetType:                    mkd.EmoteAssetType_static,
					Origin:                       mkd.EmoteOrigin_Follower,
					State:                        mkd.EmoteState_active,
					ExpectEmoteByIDToReturnEmote: true,
					ExpectEmoteToHaveEmoteGroup:  true,
				})
				defer cleanUpEmote()

				getEmotesByCodes(t, ctx, EmotesByCodesIntegTestParms{
					userID:    viewerID,
					channelID: creatorID,
					codes:     []string{uploadResp.Code},
					expectedEmoteIDs: []string{
						uploadResp.EmoteID,
					},
				})
			},
		},
	}

	for _, test := range tests {
		t.Run(test.scenario, func(t *testing.T) {
			// TODO: Uncomment this after debugging what is going wrong with the tests
			//t.Parallel()

			ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
			test.test(ctx, t)
			cancel()
		})
	}
}

func getEmotesByCodes(t *testing.T, ctx context.Context, params EmotesByCodesIntegTestParms) {
	userID := params.userID
	if userID == "" && !params.shouldNotGenerateUserID {
		userID = getIntegrationTestUserID()
	}

	channelID := params.channelID
	if channelID == "" && !params.shouldNotGenerateChannelID {
		channelID = getIntegrationTestUserID()
	}

	req := &mk.GetEmotesByCodesRequest{
		OwnerId:   userID,
		ChannelId: channelID,
		Codes:     params.codes,
	}

	resp, err := getEmotesByCodesAPI.GetEmotesByCodes(ctx, req)
	if params.shouldExpectError {
		require.Nil(t, resp)
		require.NotNil(t, err)
	} else {
		require.NotNil(t, resp)
		require.Nil(t, err)

		if params.shouldExpectEmptyResponse {
			require.Zero(t, len(resp.GetEmoticons()))
		} else {
			require.NotEqual(t, 0, len(resp.GetEmoticons()))
		}

		for _, expectedEmoteID := range params.expectedEmoteIDs {
			foundID := false
			for _, emoticon := range resp.GetEmoticons() {
				if emoticon.Id == expectedEmoteID {
					foundID = true
				}
			}
			require.True(t, foundID)
		}

		for _, unexpectedEmoteID := range params.unexpectedEmoteIDs {
			foundID := false
			for _, emoticon := range resp.GetEmoticons() {
				if emoticon.Id == unexpectedEmoteID {
					foundID = true
				}
			}
			require.False(t, foundID)
		}
	}
}
