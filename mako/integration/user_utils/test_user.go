package user_utils

const (
	// This is unfortunately necessary so that we can check whether a user is an integration test user anywhere in
	// mako without having to untangle messy dependency knots
	IntegrationTestUserPrefix = "makoIntegration"
)
