package hero

import (
	"context"
	"net/http"
	"testing"

	"code.justin.tv/commerce/mako/tests"
	makotwirp "code.justin.tv/commerce/mako/twirp"
)

func TestGlobals(t *testing.T) {
	tests.TestGlobals(t, func(ctx context.Context) (makotwirp.Mako, func(), error) {
		client := makotwirp.NewMakoProtobufClient("https://main.us-west-2.prod.mako.twitch.a2z.com", http.DefaultClient)
		return client, func() {}, nil
	})
}

func TestSubs(t *testing.T) {
	tests.TestSubs(t, func(ctx context.Context) (makotwirp.Mako, func(), error) {
		client := makotwirp.NewMakoProtobufClient("https://main.us-west-2.prod.mako.twitch.a2z.com", http.DefaultClient)
		return client, func() {}, nil
	})
}

func TestChannelPoints(t *testing.T) {
	tests.TestChannelPoints(t, func(ctx context.Context) (makotwirp.Mako, func(), error) {
		client := makotwirp.NewMakoProtobufClient("https://main.us-west-2.prod.mako.twitch.a2z.com", http.DefaultClient)
		return client, func() {}, nil
	})
}

func TestT2T3Modifiers(t *testing.T) {
	tests.TestT2T3Modifiers(t, func(ctx context.Context) (makotwirp.Mako, func(), error) {
		client := makotwirp.NewMakoProtobufClient("https://main.us-west-2.prod.mako.twitch.a2z.com", http.DefaultClient)
		return client, func() {}, nil
	})
}
