package events

import (
	"context"
	"fmt"

	badgemodel "code.justin.tv/chat/badges/app/models"
	badgeclient "code.justin.tv/chat/badges/client"
	log "code.justin.tv/commerce/logrus"
)

type BadgeProcessor struct {
	BadgeClient badgeclient.Client
}

// NewBadgeProcessor creates a new badge processor
func NewBadgeProcessor(client badgeclient.Client) IEventProcessor {
	processor := &BadgeProcessor{
		BadgeClient: client,
	}
	return processor
}

// Process takes in a Badge Entitlement Event and calls badge service to grant the user the badge
func (proc *BadgeProcessor) Process(event Event) error {
	log.Infof("Evaluating event with BadgeProcessor: %+v", event)

	// TODO work with chat badges to understand what version bumps look like
	badgeParams := &badgemodel.GrantBadgeParams{
		BadgeSetVersion: "1",
	}

	// TODO key on event.value to support different actions like "grant" vs "revoke

	if err := proc.BadgeClient.GrantUserFuelBadge(context.Background(), event.UserID, event.Key, *badgeParams, nil); err != nil {
		return fmt.Errorf("Failed to call badge service for event: %+v - error: %s", event, err)
	}

	log.Infof("Successfully processed event: %+v", event)
	return nil
}
