package events_test

import (
	"fmt"
	"testing"

	"code.justin.tv/commerce/mako/events"
	events_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/events"
	"code.justin.tv/commerce/mako/scope"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

const (
	processorScope1             = "scope1"
	processorScope2             = "scope2"
	processorScopeEmoteCampaign = scope.EmoteSetsCommerceCampaignDefault + "scope"
	processorScopeBadgeCampaign = scope.BadgeSetsCommerceCampaignDefault + "scope"
	missingScope                = "missingscope"
)

func TestEventDelegator(t *testing.T) {
	Convey("Test EventDelegator", t, func() {
		//File wide init
		mockProcessor_scope1 := new(events_mock.IEventProcessor)
		mockProcessor_scope2_1 := new(events_mock.IEventProcessor)
		mockProcessor_scope2_2 := new(events_mock.IEventProcessor)
		mockProcessor_scope_emote := new(events_mock.IEventProcessor)
		mockProcessor_scope_badge := new(events_mock.IEventProcessor)

		delegator := events.EventDelegator{
			ProcessorMap: map[string][]events.IEventProcessor{
				processorScope1:                        {mockProcessor_scope1},
				processorScope2:                        {mockProcessor_scope2_1, mockProcessor_scope2_2},
				scope.EmoteSetsCommerceCampaignDefault: {mockProcessor_scope_emote},
				scope.BadgeSetsCommerceCampaignDefault: {mockProcessor_scope_badge},
			},
		}

		Convey("When invalid processor scope", func() {
			testEvent := makeEvent(missingScope)
			err := delegator.Delegate(testEvent)
			So(err, ShouldNotBeNil)
		})
		Convey("When delegating processorScope1 happy case", func() {
			testEvent := makeEvent(processorScope1)
			mockProcessor_scope1.On("Process", mock.Anything).Return(nil)
			err := delegator.Delegate(testEvent)
			So(err, ShouldBeNil)
			So(mockProcessor_scope1.AssertNumberOfCalls(t, "Process", 1), ShouldBeTrue)
			So(mockProcessor_scope2_1.AssertNotCalled(t, "Process", mock.Anything), ShouldBeTrue)
			So(mockProcessor_scope2_2.AssertNotCalled(t, "Process", mock.Anything), ShouldBeTrue)
		})
		Convey("When delegating processorScope2 happy case", func() {
			testEvent := makeEvent(processorScope2)
			mockProcessor_scope2_1.On("Process", mock.Anything).Return(nil)
			mockProcessor_scope2_2.On("Process", mock.Anything).Return(nil)
			err := delegator.Delegate(testEvent)
			So(err, ShouldBeNil)
			So(mockProcessor_scope1.AssertNotCalled(t, "Process", mock.Anything), ShouldBeTrue)
			So(mockProcessor_scope2_1.AssertNumberOfCalls(t, "Process", 1), ShouldBeTrue)
			So(mockProcessor_scope2_2.AssertNumberOfCalls(t, "Process", 1), ShouldBeTrue)
		})
		Convey("When delegating processorScope2 first processor fails", func() {
			testEvent := makeEvent(processorScope2)
			mockProcessor_scope2_1.On("Process", mock.Anything).Return(fmt.Errorf("error!!"))
			err := delegator.Delegate(testEvent)
			So(err, ShouldNotBeNil)
			So(mockProcessor_scope1.AssertNotCalled(t, "Process", mock.Anything), ShouldBeTrue)
			So(mockProcessor_scope2_1.AssertNumberOfCalls(t, "Process", 1), ShouldBeTrue)
			So(mockProcessor_scope2_2.AssertNotCalled(t, "Process", mock.Anything), ShouldBeTrue)
		})
		Convey("When delegating processorScope2 second processor fails", func() {
			testEvent := makeEvent(processorScope2)
			mockProcessor_scope2_1.On("Process", mock.Anything).Return(nil)
			mockProcessor_scope2_2.On("Process", mock.Anything).Return(fmt.Errorf("error!!"))
			err := delegator.Delegate(testEvent)
			So(err, ShouldNotBeNil)
			So(mockProcessor_scope1.AssertNotCalled(t, "Process", mock.Anything), ShouldBeTrue)
			So(mockProcessor_scope2_1.AssertNumberOfCalls(t, "Process", 1), ShouldBeTrue)
			So(mockProcessor_scope2_2.AssertNumberOfCalls(t, "Process", 1), ShouldBeTrue)
		})
		Convey("When delegating processorScopeEmoteCampaign happy case", func() {
			testEvent := makeEvent(processorScopeEmoteCampaign)
			mockProcessor_scope_emote.On("Process", mock.Anything).Return(nil)
			err := delegator.Delegate(testEvent)
			So(err, ShouldBeNil)
			So(mockProcessor_scope_emote.AssertNumberOfCalls(t, "Process", 1), ShouldBeTrue)
		})
		Convey("When delegating processorScopeBadgeCampaign happy case", func() {
			testEvent := makeEvent(processorScopeBadgeCampaign)
			mockProcessor_scope_badge.On("Process", mock.Anything).Return(nil)
			err := delegator.Delegate(testEvent)
			So(err, ShouldBeNil)
			So(mockProcessor_scope_badge.AssertNumberOfCalls(t, "Process", 1), ShouldBeTrue)
		})
	})
}

func makeEvent(scope string) events.Event {
	return events.Event{
		Key:    "key",
		Origin: "foo",
		Scope:  scope,
		UserID: "112233",
		Value:  "foo",
	}
}
