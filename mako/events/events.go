package events

import (
	"fmt"
	"strings"

	badges "code.justin.tv/chat/badges/client"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/mako/clients"
	"code.justin.tv/commerce/mako/config"
	"code.justin.tv/commerce/mako/dynamo"
	"code.justin.tv/commerce/mako/scope"
	"github.com/golang/protobuf/ptypes/timestamp"
)

// Event contains the structure of the Event coming from the Event SQS queue
type Event struct {
	UserID   string               `json:"user_id" validate:"nonzero"` // Twitch user id
	Scope    string               `json:"scope" validate:"nonzero"`   // Scope for this event, registered during onboarding. Must be a valid event scope.
	Key      string               `json:"key" validate:"nonzero"`     // The unique key for this event.
	Value    string               `json:"value" validate:"nonzero"`   // The value of the event
	Origin   string               `json:"origin" validate:"nonzero"`  // The source of the event
	Version  string               `json:"version"`                    // The emote version of the event
	Source   string               `json:"source"`                     // The emote source of the event
	GroupID  string               `json:"group_id"`                   // The emote group of the event
	OwnerID  string               `json:"owner_id"`                   // The emote owner of the event
	StartsAt *timestamp.Timestamp `json:"starts_at"`                  // The start time of the event
	EndsAt   *timestamp.Timestamp `json:"ends_at"`                    // The end time of the event. Used for entitlement expiration.
}

const EventVersionMateria = "2"

/* TODO
Processor and Delegator have the same signature as we add more processors.
We should think about the separation of responsibilities
*/

// IEventProcessor interface used for various processors
type IEventProcessor interface {
	Process(event Event) error
}

// EventDelegator delegator for events
// ProcessorMap is visible for testing purposes
type EventDelegator struct {
	ProcessorMap map[string][]IEventProcessor
}

// IEventDelegator interface for event delegator
type IEventDelegator interface {
	Delegate(event Event) error
}

// NewEventDelegator creates a new event delegator and maps the known events to their processors
func NewEventDelegator(entDao dynamo.IEntitlementDao, badgeClient badges.Client, clients *clients.Clients, cfg *config.Configuration) *EventDelegator {
	delegator := &EventDelegator{}

	// The ordering of processors in the slice initialization matters and dictates the order in which they will be run
	delegator.ProcessorMap = map[string][]IEventProcessor{
		scope.EmoteSets:                           {NewDirectEntitlementProcessor(entDao, clients)},
		scope.EmoteSetsCommerce:                   {NewDirectEntitlementProcessor(entDao, clients)},
		scope.EmoteSetsCommerceCampaignDefault:    {NewDirectEntitlementProcessor(entDao, clients)},
		scope.EmoteSetsCommerceCrate:              {NewDirectEntitlementProcessor(entDao, clients)},
		scope.EmoteSetsCommerceCrateHalloween2017: {NewBonusEventProcessor(entDao, clients, cfg)},
		scope.EmoteSetsCommerceCrateHoliday2017:   {NewDirectEntitlementProcessor(entDao, clients)},
		scope.EmoteSetsClips:                      {NewDirectEntitlementProcessor(entDao, clients)},
		scope.EmoteSetsBitsEvents:                 {NewDirectEntitlementProcessor(entDao, clients)},
		scope.BadgeSets:                           {NewDirectEntitlementProcessor(entDao, clients), NewBadgeProcessor(badgeClient)},
		scope.BadgeSetsCommerce:                   {NewDirectEntitlementProcessor(entDao, clients), NewBadgeProcessor(badgeClient)},
		scope.BadgeSetsCommerceCrate:              {NewDirectEntitlementProcessor(entDao, clients), NewBadgeProcessor(badgeClient)},
		scope.BadgeSetsCommerceCampaignDefault:    {NewDirectEntitlementProcessor(entDao, clients), NewBadgeProcessor(badgeClient)},
	}

	return delegator
}

// Delegate passes along the event message to the appropriate processor for further handling
func (del *EventDelegator) Delegate(event Event) error {
	log.Infof("Beginning to delegate for event: %+v", event)
	eventProcessors := del.getEventProcessors(event.Scope)
	if len(eventProcessors) > 0 {
		for _, currentProcessor := range eventProcessors {
			if err := currentProcessor.Process(event); err != nil {
				return fmt.Errorf("Current processor: %+v failed for event: %+v - error: %s", currentProcessor, event, err)
			}
		}
		return nil
	}
	return fmt.Errorf("Unable to delegate event: %+v", event)
}

func (del *EventDelegator) getEventProcessors(eventScope string) []IEventProcessor {
	if eventProcessors, valuePresent := del.ProcessorMap[eventScope]; valuePresent {
		return eventProcessors
	}
	if strings.HasPrefix(eventScope, scope.EmoteSetsCommerceCampaignDefault) {
		eventScope = scope.EmoteSetsCommerceCampaignDefault
	} else if strings.HasPrefix(eventScope, scope.BadgeSetsCommerceCampaignDefault) {
		eventScope = scope.BadgeSetsCommerceCampaignDefault
	}
	if eventProcessors, valuePresent := del.ProcessorMap[eventScope]; valuePresent {
		return eventProcessors
	}
	return nil
}
