package events

import (
	"context"
	"encoding/json"
	"fmt"

	materiatwirp "code.justin.tv/amzn/MateriaTwirp"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/mako/clients"
	"code.justin.tv/commerce/mako/clients/emoticons"
	"code.justin.tv/commerce/mako/clients/materia"
	"code.justin.tv/commerce/mako/clients/pubsub"
	"code.justin.tv/commerce/mako/dynamo"
	"code.justin.tv/commerce/mako/scope"
	mk "code.justin.tv/commerce/mako/twirp"
	"github.com/pkg/errors"
)

type DirectEntitlementProcessor struct {
	EntitlementDAO dynamo.IEntitlementDao
	Clients        *clients.Clients
}

// NewDirectEntitlementProcessor creates a new direct entitlement processor
func NewDirectEntitlementProcessor(entDao dynamo.IEntitlementDao, clients *clients.Clients) IEventProcessor {
	processor := &DirectEntitlementProcessor{
		EntitlementDAO: entDao,
		Clients:        clients,
	}
	return processor
}

// Process takes in a Direct Entitlement event and applies the entitlement
func (proc *DirectEntitlementProcessor) Process(event Event) error {
	log.Infof("Evaluating event with DirectEntitlementProcessor: %+v", event)

	scopedKey := mk.ScopedKey{
		Scope: event.Scope,
		Key:   event.Key,
	}

	// Validate event
	if err := scope.ValidateEntitlementScope(scopedKey.GetScope()); err != nil {
		return fmt.Errorf("Invalid entitlement scope used to process DirectEntitlementEvent: %s, %+v", scopedKey.GetScope(), event)
	}

	marshalledScopedKey, err := scope.Marshall(scopedKey)
	if err != nil {
		return fmt.Errorf("Error building scoped key during processing: %s", err)
	}

	// Grant entitlement
	// TODO key on event.value to handle different actions like "grant" vs "revoke". For now, always "grant"
	if event.Version == "" { // Legacy path: Entitle through entitlementsDAO
		entitlement := &dynamo.Entitlement{
			UserId:               event.UserID,
			EntitlementScopedKey: marshalledScopedKey,
			Origin:               event.Origin,
		}
		// TODO consider conditional Put instead of update, need to consider product requirements of overwriting
		if err := proc.EntitlementDAO.Update(entitlement); err != nil {
			return fmt.Errorf("Failed to update record for event: %+v - error: %s", event, err)
		}

		key, _ := emoticons.EntitlementsDBCacheParams(event.UserID)
		emoticons.ResetCache(key, proc.Clients.RedisCache)

	} else if event.Version == EventVersionMateria { // New path: Entitle through Materia
		ctx := context.Background()

		source := materiatwirp.Source_SOURCE_NONE
		if event.Source == materiatwirp.Source_REWARDS.String() {
			source = materiatwirp.Source_REWARDS
		}

		meta, err := json.Marshal(materia.EmoteEntitlementMetadata{
			GroupID: event.GroupID,
		})
		if err != nil {
			return errors.Wrap(err, "failed to marshal direct processor event metadata")
		}

		endsAt := &materiatwirp.InfiniteTime{
			IsInfinite: false,
			Time:       event.EndsAt,
		}
		if event.EndsAt == nil {
			endsAt.IsInfinite = true
		}
		req := &materiatwirp.UpsertEntitlementsRequest{
			Entitlements: []*materiatwirp.Entitlement{
				{
					OwnerId:     event.UserID,
					Domain:      materiatwirp.Domain_EMOTES,
					Source:      source,
					OriginId:    event.Origin,
					ItemId:      event.Key,
					ItemOwnerId: event.OwnerID,
					Meta:        meta,
					Type:        "emote",
					StartsAt:    event.StartsAt,
					EndsAt:      endsAt,
				},
			},
		}
		if _, err := proc.Clients.MateriaClient.UpsertEntitlements(ctx, req); err != nil {
			return errors.Wrap(err, "failed to create entitlement for event")
		}
	}

	psEvent := pubsub.UserSubscribeEvent{
		UserID:    event.UserID,
		ChannelID: "0", // we do not have a channel so will default to 0
	}

	if err = proc.Clients.Pubsub.UserSubscription(context.Background(), psEvent, nil); err != nil {
		log.WithField("event", event).
			WithError(err).
			Error("Could not send user-subscribe-event pubsub message")
	}

	log.Infof("Successfully processed event: %+v", event)
	return nil
}
