package events_test

import (
	"fmt"
	"testing"

	"code.justin.tv/commerce/mako/clients"
	"code.justin.tv/commerce/mako/config"
	"code.justin.tv/commerce/mako/dynamo"
	. "code.justin.tv/commerce/mako/events"
	pubsub_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/pubsub"
	sns_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/sns"
	dynamo_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/dynamo"
	redis_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/redis"
	spade_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/common/spade-client-go/spade"
	"code.justin.tv/commerce/mako/scope"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

const validEventScope = scope.EmoteSetsCommerceCrateHalloween2017

func TestBonusEventProcessor(t *testing.T) {
	Convey("Test BonusEventProcessor", t, func() {
		mockCache := new(redis_mock.IRedisClient)
		mockDao := new(dynamo_mock.IEntitlementDao)
		mockSns := new(sns_mock.ISNSClient)
		mockSpade := new(spade_mock.Client)
		mockPubsub := new(pubsub_mock.IPublisher)

		clients := &clients.Clients{
			RedisCache:  mockCache,
			SnsClient:   mockSns,
			SpadeClient: mockSpade,
			Pubsub:      mockPubsub,
		}

		cfg := &config.Configuration{
			EventEmoteCollections: map[string]config.EventEmoteCollection{
				"holidayEvent": {
					EmoteSets: map[string]bool{
						"1": true,
						"2": true,
					},
					BonusEmoteSet: "3",
				},
			},
		}

		processor := NewBonusEventProcessor(mockDao, clients, cfg)
		mockSpade.On("TrackEvent", mock.Anything, mock.Anything, mock.Anything).Return(nil)
		mockPubsub.On("UserSubscription", mock.Anything, mock.Anything, mock.Anything).Return(nil)

		Convey("When valid event received, and collection completed", func() {
			testEvent := Event{
				Key:    "1",
				Origin: "foo",
				Scope:  validEventScope,
				UserID: "112233",
				Value:  "foo",
			}

			mockCache.On("Del", mock.Anything).Return(nil)
			mockDao.On("Update", mock.Anything).Return(nil)
			mockDao.On("Get", mock.Anything, mock.Anything).Return([]*dynamo.Entitlement{
				{
					UserId:               "112233",
					EntitlementScopedKey: fmt.Sprintf("%s1", validEventScope),
				},
				{
					UserId:               "112233",
					EntitlementScopedKey: fmt.Sprintf("%s2", validEventScope),
				},
			}, nil)

			mockSns.On("PostToTopicWithMessageAttributes", mock.Anything, mock.Anything, mock.Anything).Return(nil)

			err := processor.Process(testEvent)
			So(err, ShouldBeNil)
			So(mockDao.AssertNumberOfCalls(t, "Update", 1), ShouldBeTrue)
			So(mockDao.AssertNumberOfCalls(t, "Get", 1), ShouldBeTrue)
			So(mockSns.AssertNumberOfCalls(t, "PostToTopicWithMessageAttributes", 1), ShouldBeTrue)
		})

		Convey("When valid event received, and collection not completed", func() {
			testEvent := Event{
				Key:    "1",
				Origin: "foo",
				Scope:  validEventScope,
				UserID: "112233",
				Value:  "foo",
			}

			mockCache.On("Del", mock.Anything).Return(nil)
			mockDao.On("Update", mock.Anything).Return(nil)
			mockDao.On("Get", mock.Anything, mock.Anything).Return([]*dynamo.Entitlement{
				{
					UserId:               "112233",
					EntitlementScopedKey: fmt.Sprintf("%s1", validEventScope),
				},
			}, nil)

			err := processor.Process(testEvent)
			So(err, ShouldBeNil)
			So(mockDao.AssertNumberOfCalls(t, "Update", 1), ShouldBeTrue)
			So(mockDao.AssertNumberOfCalls(t, "Get", 1), ShouldBeTrue)
			So(mockSns.AssertNumberOfCalls(t, "PostToTopicWithMessageAttributes", 0), ShouldBeTrue)
		})

		Convey("When valid event received, and not in collection", func() {
			testEvent := Event{
				Key:    "5",
				Origin: "foo",
				Scope:  validEventScope,
				UserID: "112233",
				Value:  "foo",
			}

			mockCache.On("Del", mock.Anything).Return(nil)
			mockDao.On("Update", mock.Anything).Return(nil)

			err := processor.Process(testEvent)
			So(err, ShouldBeNil)
			So(mockDao.AssertNumberOfCalls(t, "Update", 1), ShouldBeTrue)
			So(mockDao.AssertNumberOfCalls(t, "Get", 0), ShouldBeTrue)
			So(mockSns.AssertNumberOfCalls(t, "PostToTopicWithMessageAttributes", 0), ShouldBeTrue)
		})

		Convey("When invalid entitlement scope received", func() {
			testEvent := Event{
				Key:    "key",
				Origin: "foo",
				Scope:  invalidEntScope,
				UserID: "112233",
				Value:  "foo",
			}

			err := processor.Process(testEvent)
			So(err, ShouldNotBeNil)
			So(mockDao.AssertNumberOfCalls(t, "Update", 0), ShouldBeTrue)
		})

		Convey("When invalid scope received", func() {
			testEvent := Event{
				Key:    "key",
				Origin: "foo",
				Scope:  invalidScope,
				UserID: "112233",
				Value:  "foo",
			}

			err := processor.Process(testEvent)
			So(err, ShouldNotBeNil)
			So(mockDao.AssertNumberOfCalls(t, "Update", 0), ShouldBeTrue)
		})

		Convey("When dynamo call fails", func() {
			testEvent := Event{
				Key:    "key",
				Origin: "foo",
				Scope:  validEntScope,
				UserID: "112233",
				Value:  "foo",
			}

			mockDao.On("Update", mock.Anything).Return(fmt.Errorf("error!!"))

			err := processor.Process(testEvent)
			So(err, ShouldNotBeNil)
			So(mockDao.AssertNumberOfCalls(t, "Update", 1), ShouldBeTrue)
		})
	})
}
