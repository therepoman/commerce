package events

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/mako/clients"
	"code.justin.tv/commerce/mako/config"
	"code.justin.tv/commerce/mako/dynamo"
	"code.justin.tv/commerce/mako/scope"
)

const (
	halloween2017Origin  = "Halloween 2017 Event"
	dataScienceEventName = "crate_bonus_item_receive"
)

// BonusEventProcessor provides the data for Bonus Event Processor
type BonusEventProcessor struct {
	EntitlementDAO dynamo.IEntitlementDao
	Config         *config.Configuration
	Clients        *clients.Clients
}

type bonusEmoteSpadeEvent struct {
	UserID         string `json:"user_id"`
	CollectionName string `json:"collection_name"`
	RewardItemID   string `json:"reward_item_id"`
	ServerTime     int64  `json:"time"`
}

// NewBonusEventProcessor creates a new direct entitlement processor
func NewBonusEventProcessor(entDao dynamo.IEntitlementDao, clients *clients.Clients, cfg *config.Configuration) IEventProcessor {
	processor := &BonusEventProcessor{
		EntitlementDAO: entDao,
		Config:         cfg,
		Clients:        clients,
	}
	return processor
}

// Process takes in a Bonus Event event and applies the entitlement
func (proc *BonusEventProcessor) Process(event Event) error {
	directEntitlementProcessor := NewDirectEntitlementProcessor(proc.EntitlementDAO, proc.Clients)

	if err := directEntitlementProcessor.Process(event); err != nil {
		return fmt.Errorf("Error processing direct entitlement: %v", err)
	}

	isRewarded, rewardKey, collectionName, err := checkRewardBonus(proc.Config.EventEmoteCollections, proc.EntitlementDAO, event)
	if err != nil {
		log.WithError(err).Error("Error checking reward eligibility")
	}

	if isRewarded {
		log.WithFields(log.Fields{
			"userId":          event.UserID,
			"Latest emoteSet": event.Key,
		}).Info("Emote collection is completed. Granting reward")

		makoEventJSON, err := json.Marshal(newMakoEventJSON(event, rewardKey))
		if err != nil {
			log.WithError(err).Errorf("Error marshalling Mako Event SNS message")
		}

		if err := proc.Clients.SnsClient.PostToTopicWithMessageAttributes(context.Background(), proc.Config.MakoEventsSNS, string(makoEventJSON)); err != nil {
			log.WithFields(log.Fields{
				"userId": event.UserID,
			}).WithError(err).Error("Failure to send SNS message for granting bonus reward emote")
		}

		spadeEvent := &bonusEmoteSpadeEvent{
			UserID:         event.UserID,
			CollectionName: collectionName,
			RewardItemID:   rewardKey,
			ServerTime:     time.Now().Unix(),
		}

		if err := proc.Clients.SpadeClient.TrackEvent(context.Background(), dataScienceEventName, spadeEvent); err != nil {
			// Non-blocking error
			log.WithFields(log.Fields{
				"userId": event.UserID,
			}).Warnf("Failed to submit Spade event for bonus emote entitlement: %v", err)
		}
	}

	log.WithFields(log.Fields{
		"Event": event,
	}).Info("Successfully processed Bonus Event")
	return nil
}

// Temporary collections logic for crates' holiday events
func checkRewardBonus(eventEmoteCollections map[string]config.EventEmoteCollection, entitlementDao dynamo.IEntitlementDao, event Event) (bool, string, string, error) {
	for collectionName, emoteCollection := range eventEmoteCollections {
		if emoteCollection.EmoteSets[event.Key] {
			crateEmotes, err := entitlementDao.Get(event.UserID, scope.EmoteSetsCommerceCrateHalloween2017)
			if err != nil {
				log.WithError(err).Error("Error retrieving entitlement from DAO")
				return false, "", "", err
			}

			for emoteSet := range emoteCollection.EmoteSets {
				isSetInSlice, err := setInSlice(emoteSet, crateEmotes)
				if err != nil || !isSetInSlice {
					return false, "", "", err
				}
			}
			return true, emoteCollection.BonusEmoteSet, collectionName, nil
		}
	}

	return false, "", "", nil
}

func setInSlice(emoteSet string, entitlements []*dynamo.Entitlement) (bool, error) {
	for _, entitlement := range entitlements {
		entitlementScopedKey, err := scope.Unmarshall(entitlement.EntitlementScopedKey)
		if err != nil {
			log.WithFields(log.Fields{
				"entitlementScopedKey": entitlementScopedKey,
			}).Errorf("Error unmarshalling %v", err)
			return false, err
		}

		if err := scope.ValidateEntitlementScope(entitlementScopedKey.GetScope()); err != nil {
			log.WithFields(log.Fields{
				"Scope": entitlementScopedKey.GetScope(),
			}).Error("Invalid entitlement scope while parsing")
			return false, err
		}

		if entitlementScopedKey.GetKey() == emoteSet {
			return true, nil
		}
	}
	return false, nil
}

func newMakoEventJSON(event Event, rewardKey string) Event {
	return Event{
		UserID: event.UserID,
		Scope:  event.Scope,
		Key:    rewardKey,
		Value:  event.Value,
		Origin: halloween2017Origin,
	}
}
