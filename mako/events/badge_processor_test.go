package events_test

import (
	"fmt"
	"testing"

	. "code.justin.tv/commerce/mako/events"
	badges_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/chat/badges/client"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestEventScopes(t *testing.T) {
	Convey("Test BadgeEventProcessor", t, func() {
		// file-wide initialization
		mockClient := new(badges_mock.Client)
		badgeEventProcessor := NewBadgeProcessor(mockClient)

		Convey("When badge call succeeds", func() {
			badgeEvent := Event{
				Key:    "anomaly-2_1",
				Origin: "crate-reward",
				Scope:  "/badge_sets/",
				UserID: "139075904",
				Value:  "FULFILLED",
			}

			mockClient.On("GrantUserFuelBadge", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)

			err := badgeEventProcessor.Process(badgeEvent)
			So(err, ShouldBeNil)
			So(mockClient.AssertNumberOfCalls(t, "GrantUserFuelBadge", 1), ShouldBeTrue)
		})
		Convey("When badge call fails", func() {
			badgeEvent := Event{
				Key:    "anomaly-2_1",
				Origin: "crate-reward",
				Scope:  "/badge_sets/",
				UserID: "139075904",
				Value:  "FULFILLED",
			}

			mockClient.On("GrantUserFuelBadge", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(fmt.Errorf("error!"))

			err := badgeEventProcessor.Process(badgeEvent)
			So(err, ShouldNotBeNil)
			So(mockClient.AssertNumberOfCalls(t, "GrantUserFuelBadge", 1), ShouldBeTrue)
		})
	})
}
