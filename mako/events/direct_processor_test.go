package events_test

import (
	"errors"
	"fmt"
	"testing"
	"time"

	materiatwirp "code.justin.tv/amzn/MateriaTwirp"
	"code.justin.tv/commerce/mako/clients"
	. "code.justin.tv/commerce/mako/events"
	materia_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/materia"
	pubsub_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/pubsub"
	dynamo_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/dynamo"
	redis_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/redis"
	"code.justin.tv/commerce/mako/scope"
	"github.com/golang/protobuf/ptypes/timestamp"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

const (
	validEntScope   = scope.EmoteSets
	invalidEntScope = "/foobar/"
	invalidScope    = "foobar"
)

func TestDirectEntitlementProcessor(t *testing.T) {
	Convey("Test DirectEntitlementProcessor", t, func() {
		mockCache := new(redis_mock.IRedisClient)
		mockDao := new(dynamo_mock.IEntitlementDao)
		mockPubsub := new(pubsub_mock.IPublisher)
		mockMateriaClient := new(materia_mock.MateriaClient)

		clients := &clients.Clients{
			MateriaClient: mockMateriaClient,
			RedisCache:    mockCache,
			Pubsub:        mockPubsub,
		}

		processor := NewDirectEntitlementProcessor(mockDao, clients)

		Convey("When valid event received", func() {
			testEvent := Event{
				Key:    "key",
				Origin: "foo",
				Scope:  validEntScope,
				UserID: "112233",
				Value:  "foo",
			}

			mockCache.On("Del", mock.Anything).Return(nil)
			mockDao.On("Update", mock.Anything).Return(nil)
			mockPubsub.On("UserSubscription", mock.Anything, mock.Anything, mock.Anything).Return(nil)

			err := processor.Process(testEvent)
			So(err, ShouldBeNil)
			So(mockDao.AssertNumberOfCalls(t, "Update", 1), ShouldBeTrue)
			So(mockPubsub.AssertNumberOfCalls(t, "UserSubscription", 1), ShouldBeTrue)
		})
		Convey("When invalid entitlement scope received", func() {
			testEvent := Event{
				Key:    "key",
				Origin: "foo",
				Scope:  invalidEntScope,
				UserID: "112233",
				Value:  "foo",
			}

			err := processor.Process(testEvent)
			So(err, ShouldNotBeNil)
			So(mockDao.AssertNumberOfCalls(t, "Update", 0), ShouldBeTrue)
			So(mockPubsub.AssertNumberOfCalls(t, "UserSubscription", 0), ShouldBeTrue)
		})
		Convey("When invalid scope received", func() {
			testEvent := Event{
				Key:    "key",
				Origin: "foo",
				Scope:  invalidScope,
				UserID: "112233",
				Value:  "foo",
			}

			err := processor.Process(testEvent)
			So(err, ShouldNotBeNil)
			So(mockDao.AssertNumberOfCalls(t, "Update", 0), ShouldBeTrue)
			So(mockPubsub.AssertNumberOfCalls(t, "UserSubscription", 0), ShouldBeTrue)
		})
		Convey("When dynamo call fails", func() {
			testEvent := Event{
				Key:    "key",
				Origin: "foo",
				Scope:  validEntScope,
				UserID: "112233",
				Value:  "foo",
			}

			mockDao.On("Update", mock.Anything).Return(fmt.Errorf("error!!"))

			err := processor.Process(testEvent)
			So(err, ShouldNotBeNil)
			So(mockDao.AssertNumberOfCalls(t, "Update", 1), ShouldBeTrue)
			So(mockPubsub.AssertNumberOfCalls(t, "UserSubscription", 0), ShouldBeTrue)
		})
		Convey("When valid event received and pubsub failure", func() {
			testEvent := Event{
				Key:    "key",
				Origin: "foo",
				Scope:  validEntScope,
				UserID: "112233",
				Value:  "foo",
			}

			mockCache.On("Del", mock.Anything).Return(nil)
			mockDao.On("Update", mock.Anything).Return(nil)
			mockPubsub.On("UserSubscription", mock.Anything, mock.Anything, mock.Anything).Return(fmt.Errorf("error!!"))

			err := processor.Process(testEvent)
			So(err, ShouldBeNil)
			So(mockDao.AssertNumberOfCalls(t, "Update", 1), ShouldBeTrue)
			So(mockPubsub.AssertNumberOfCalls(t, "UserSubscription", 1), ShouldBeTrue)
		})
		Convey("With Materia", func() {
			testEvent := Event{
				Key:     "key",
				Origin:  "foo",
				Scope:   validEntScope,
				UserID:  "112233",
				Value:   "foo",
				Version: EventVersionMateria,
				Source:  "REWARDS",
				GroupID: "groupID",
				OwnerID: "ownerID",
				StartsAt: &timestamp.Timestamp{
					Seconds: time.Now().Unix(),
				},
				EndsAt: &timestamp.Timestamp{
					Seconds: time.Now().Unix(),
				},
			}

			Convey("When valid event with EventVersionMateria is received", func() {
				mockPubsub.On("UserSubscription", mock.Anything, mock.Anything, mock.Anything).Return(nil)
				mockMateriaClient.On("UpsertEntitlements", mock.Anything, mock.Anything).Return(&materiatwirp.UpsertEntitlementsResponse{}, nil)

				err := processor.Process(testEvent)
				So(err, ShouldBeNil)
				So(mockPubsub.AssertNumberOfCalls(t, "UserSubscription", 1), ShouldBeTrue)
				So(mockMateriaClient.AssertNumberOfCalls(t, "UpsertEntitlements", 1), ShouldBeTrue)
			})

			Convey("When permanent emote event with EventVersionMateria is received", func() {
				testEvent.EndsAt = nil
				mockPubsub.On("UserSubscription", mock.Anything, mock.Anything, mock.Anything).Return(nil)
				mockMateriaClient.On("UpsertEntitlements", mock.Anything, mock.Anything).Return(&materiatwirp.UpsertEntitlementsResponse{}, nil)

				err := processor.Process(testEvent)
				So(err, ShouldBeNil)
				So(mockPubsub.AssertNumberOfCalls(t, "UserSubscription", 1), ShouldBeTrue)
				So(mockMateriaClient.AssertNumberOfCalls(t, "UpsertEntitlements", 1), ShouldBeTrue)
			})

			Convey("When materia.UpsertEntitlements fails", func() {
				mockPubsub.On("UserSubscription", mock.Anything, mock.Anything, mock.Anything).Return(nil)
				mockMateriaClient.On("UpsertEntitlements", mock.Anything, mock.Anything).Return(nil, errors.New("woops"))

				err := processor.Process(testEvent)
				So(err, ShouldNotBeNil)
				So(mockPubsub.AssertNumberOfCalls(t, "UserSubscription", 0), ShouldBeTrue)
				So(mockMateriaClient.AssertNumberOfCalls(t, "UpsertEntitlements", 1), ShouldBeTrue)
			})
		})
	})
}
