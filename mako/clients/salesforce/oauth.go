package salesforce

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/foundation/twitchclient"
	"github.com/pkg/errors"
)

const (
	GrantType = "password"
)

// OauthClient is an interface that allows methods to fetch oauth token from Salesforce
type OauthClient interface {
	Authenticate() (*oauth, error)
}

// Object which implements OauthClient interface
type oauthClientImpl struct {
	oauthHost    string
	oauthConfig  *OauthSecretConfig
	twitchClient twitchclient.Client
}

// Salesforce secret config used for Oauth authentication
type OauthSecretConfig struct {
	ClientID     string `json:"salesforce_client_id"`
	ClientSecret string `json:"salesforce_client_secret"`
	Username     string `json:"salesforce_username"`
	Password     string `json:"salesforce_password"`
}

// Oauth response received from authentication
type oauth struct {
	AccessToken string `json:"access_token"`
	InstanceUrl string `json:"instance_url"`
	ID          string `json:"id"`
	IssuedAt    string `json:"issued_at"`
	Signature   string `json:"signature"`
}

// Creates and returns a OauthClient which allows caller to authenticate to Salesforce
func NewOauthClient(oauthHost string, oauthSecretConfig OauthSecretConfig) (OauthClient, error) {
	if len(oauthHost) == 0 {
		return nil, errors.New("Invalid Oauth host")
	}

	client, err := twitchclient.NewClient(
		twitchclient.ClientConf{
			Host: oauthHost,
			Transport: twitchclient.TransportConf{
				MaxIdleConnsPerHost: 10,
			},
		},
	)
	if err != nil {
		return nil, fmt.Errorf("Failed to create oauth twitch client: %v", err)
	}

	if err != nil {
		return nil, fmt.Errorf("Failed to get Oauth secret config: %v", err)
	}

	oauthClient := &oauthClientImpl{
		oauthHost:    oauthHost,
		oauthConfig:  &oauthSecretConfig,
		twitchClient: client,
	}

	return oauthClient, nil
}

// Authenticates and obtains a oauth token
func (c *oauthClientImpl) Authenticate() (*oauth, error) {
	// oauth request payload
	payload := url.Values{
		"grant_type":    {GrantType},
		"client_id":     {c.oauthConfig.ClientID},
		"client_secret": {c.oauthConfig.ClientSecret},
		"username":      {c.oauthConfig.Username},
		"password":      {c.oauthConfig.Password},
	}

	// Create OauthRequest
	request, err := c.twitchClient.NewRequest("POST", "/services/oauth2/token", strings.NewReader(payload.Encode()))
	request.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	if err != nil {
		fmt.Println("Request error:", err)
		return nil, err
	}

	// Send request
	response, err := c.twitchClient.Do(context.Background(), request, twitchclient.ReqOpts{})
	if err != nil {
		fmt.Println("Response error:", err)
	}

	defer func() {
		err = response.Body.Close()
		if err != nil {
			log.WithError(err).Error("Could not close response body")
		}
	}()

	switch response.StatusCode {
	case http.StatusOK:
		oauth := &oauth{}
		if err = json.NewDecoder(response.Body).Decode(oauth); err != nil {
			return nil, fmt.Errorf("Failed to decode response body: %v", err)
		}
		if err = oauth.validate(); err != nil {
			return nil, fmt.Errorf("Failed to validate Oauth token: %v", err)
		}
		return oauth, nil
	default:
		// Unexpected result
		body, readAllErr := ioutil.ReadAll(response.Body)
		if readAllErr != nil {
			readAllErr = errors.Wrapf(readAllErr, response.Status+": unable to read response body for more error information, error: %v", err)
			return nil, readAllErr
		}
		err = errors.New("Failed to authenticate with Salesforce")
		err = errors.Wrap(err, response.Status+": "+string(body))
		log.WithFields(log.Fields{
			"StatusCode":   response.StatusCode,
			"ResponseBody": string(body),
		}).WithError(err).Error(err)
		return nil, err
	}
}

// validate oauth token received from Salesforce
func (oauth *oauth) validate() error {
	if len(oauth.InstanceUrl) == 0 {
		return errors.New("Invalid instance url")
	} else if len(oauth.AccessToken) == 0 {
		return errors.New("Invalid access token")
	}

	return nil
}
