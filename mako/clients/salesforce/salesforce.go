package salesforce

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/foundation/twitchclient"
	"github.com/afex/hystrix-go/hystrix"
	"github.com/cenkalti/backoff"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

type key string

// oauthRetryCountKey is the context key for oauth retry count
const (
	oauthRetryCountKey            key = "oauthRetryCount"
	inGoodStandingMaxRetries          = 1
	SalesforceGoodStandingHystrix     = "Salesforce.InGoodStanding"
)

// APIClient is an interface that exposes methods from Salesforce API service
type APIClient interface {
	InGoodStanding(ctx context.Context, userID string) (*bool, error)
}

// Object which implements APIClient interface
type apiClientImpl struct {
	oauthClient OauthClient
	oauth       *oauth
	client      twitchclient.Client
}

// create a new TwitchClient given a host url
func newTwitchClient(host string) (twitchclient.Client, error) {
	// create http twitch client
	client, err := twitchclient.NewClient(
		twitchclient.ClientConf{
			Host: host,
			Transport: twitchclient.TransportConf{
				MaxIdleConnsPerHost: 10,
			},
		},
	)
	if err != nil {
		return nil, fmt.Errorf("Failed to create twitch client from Salesforce api host: %v", err)
	}
	return client, nil
}

// Creates a new APIClient which is oauth aware
func newAPIClient(oauth *oauth, oauthClient OauthClient) (APIClient, error) {
	if oauth == nil {
		return nil, errors.New("Oauth is nil")
	}
	if err := oauth.validate(); err != nil {
		return nil, fmt.Errorf("Failed to validate Oauth token: %v", err)
	}

	twitchClient, err := newTwitchClient(oauth.InstanceUrl)
	if err != nil {
		return nil, err
	}
	return &apiClientImpl{oauth: oauth,
		oauthClient: oauthClient,
		client:      twitchClient,
	}, nil
}

// Creates a new Salesforce API client which has oauth token from salesforce service
func NewSalesforceAPIClient(oauthHost string, oauthSecretConfig OauthSecretConfig) (APIClient, error) {
	// create oauthClient
	oauthClient, err := NewOauthClient(oauthHost, oauthSecretConfig)
	if err != nil {
		return nil, fmt.Errorf("Failed to create Oauthclient: %v", err)
	}

	// authenticate with Salesforce and obtain oauth
	oauth, err := oauthClient.Authenticate()
	if err != nil {
		return nil, fmt.Errorf("Oauth authentication failed: %v", err)
	}

	// create Salesforce api client
	apiClient, err := newAPIClient(oauth, oauthClient)
	if err != nil {
		return nil, fmt.Errorf("Failed to create API Client: %v", err)
	}
	return apiClient, nil
}

// Gets good standing status for a user from Salesforce
func (api *apiClientImpl) InGoodStanding(ctx context.Context, userID string) (*bool, error) {
	// validate userID
	if len(userID) == 0 {
		return nil, errors.New("userID is empty")
	}

	var inGoodStanding bool
	f := func() error {
		// retryCount allows to retry API call when oauth token refresh is required
		newContext := context.WithValue(ctx, oauthRetryCountKey, 0)

		// send GET request
		var responseBody string
		path := fmt.Sprintf("services/apexrest/PartnerData/inGoodStanding/?id=%s", userID)
		if err := hystrix.DoC(ctx, SalesforceGoodStandingHystrix, func(ctx context.Context) error {
			var err error
			_, responseBody, err = api.doGetRequest(newContext, path)
			if err != nil {
				return err
			}
			return nil
		}, nil); err != nil {
			logrus.WithField("userID", userID).WithError(err).Error("could not retrieve InGoodStanding from Salesforce")
			return fmt.Errorf("Failed to get InGoodStanding status from Salesforce: %v", err)
		}

		// parse response
		var err error
		inGoodStanding, err = strconv.ParseBool(responseBody)
		if err != nil {
			return fmt.Errorf("Failed to parse response body: %v", err)
		}

		return nil
	}

	b := backoff.WithMaxRetries(backoff.WithContext(newBackoff(), ctx), inGoodStandingMaxRetries)
	err := backoff.Retry(f, b)

	return &inGoodStanding, err
}

func (api *apiClientImpl) doGetRequest(ctx context.Context, path string) (*http.Response, string, error) {
	// Create request
	request, err := api.client.NewRequest("GET", path, nil)
	if err != nil {
		return nil, "", fmt.Errorf("Create http request failed: %v", err)
	}
	request.Header.Set("Authorization", fmt.Sprintf("%v %v", "Bearer", api.oauth.AccessToken))

	// send request
	response, err := api.client.Do(ctx, request, twitchclient.ReqOpts{})
	if err != nil {
		return nil, "", fmt.Errorf("Send http request failed: %v", err)
	}

	defer func() {
		err = response.Body.Close()
		if err != nil {
			err = errors.Wrap(err, "Could not close response body: %v")
			log.WithError(err).Error(err)
		}
	}()

	// read response
	respBytes, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, "", fmt.Errorf("Failed to read response bytes: %v", err)
	}

	if response.StatusCode != http.StatusOK {
		apiErrors := APIErrors{}
		if err := json.Unmarshal(respBytes, &apiErrors); err != nil {
			return nil, "", fmt.Errorf("Failed to unmarshal API error in response: %s, error: %v", string(respBytes), err)
		}
		if apiErrors.Exists() {
			// check if oauth expired
			if apiErrors.InvalidSession() {

				retryCount, ok := ctx.Value(oauthRetryCountKey).(int)
				if ok == false || retryCount == 1 {
					return nil, "", errors.New("Oauth authentication retry failed")
				}

				// re-authenticate
				oauth, err := api.oauthClient.Authenticate()
				if err != nil {
					return nil, "", fmt.Errorf("Oauth authentication failed: %v", err)
				}

				// create new client if oauth returned a new instance url
				if oauth.InstanceUrl != api.oauth.InstanceUrl {
					client, err := newTwitchClient(oauth.InstanceUrl)
					if err != nil {
						return nil, "", err
					}
					api.client = client
				}

				api.oauth = oauth
				newContext := context.WithValue(ctx, oauthRetryCountKey, 1)
				return api.doGetRequest(newContext, path)
			}
			return nil, "", errors.New("Invalid response: " + apiErrors.Error())
		}
	}

	return response, string(respBytes), nil
}

func newBackoff() backoff.BackOff {
	exponential := backoff.NewExponentialBackOff()
	exponential.InitialInterval = 10 * time.Millisecond
	exponential.MaxElapsedTime = 2 * time.Second
	exponential.Multiplier = 2
	return exponential
}
