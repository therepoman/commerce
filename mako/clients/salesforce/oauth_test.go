package salesforce

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestAuthenticate(t *testing.T) {
	Convey("when calling Authenticate", t, func() {
		oauthSecretConfig := OauthSecretConfig{
			ClientID:     "mock_client_id",
			ClientSecret: "mock_client_secret",
			Username:     "mock_username",
			Password:     "mock_password",
		}

		Convey("when the backend is available", func() {
			oauthToken := &oauth{
				AccessToken: "access_token",
				InstanceUrl: "instance_url",
				ID:          "id",
				IssuedAt:    "issued_at",
				Signature:   "signature",
			}

			ts := initSalesforceTestServer(oauthToken)
			defer ts.Close()

			c, err := NewOauthClient(ts.URL, oauthSecretConfig)
			So(err, ShouldBeNil)
			So(c, ShouldNotBeNil)

			oauth, err := c.Authenticate()
			So(err, ShouldBeNil)
			So(oauth, ShouldResemble, oauthToken)
		})
		Convey("when the backend is dead", func() {
			ts := initBrokenSalesforceTestServer()
			defer ts.Close()

			c, err := NewOauthClient(ts.URL, oauthSecretConfig)
			So(err, ShouldBeNil)
			So(c, ShouldNotBeNil)

			Oauth, err := c.Authenticate()
			So(err, ShouldNotBeNil)
			So(Oauth, ShouldBeNil)
		})
	})
}

func initSalesforceTestServer(token *oauth) *httptest.Server {
	return httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.URL.String() != fmt.Sprintf("/services/oauth2/token") {
			w.WriteHeader(http.StatusNotFound)
			return
		}

		if r.Header.Get("Content-Type") != "application/x-www-form-urlencoded" {
			w.WriteHeader(http.StatusNotFound)
			return
		}

		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		values, err := url.ParseQuery(string(body))
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		if values.Get("grant_type") != "password" {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		err = json.NewEncoder(w).Encode(token)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
		}
	}))
}

func initBrokenSalesforceTestServer() *httptest.Server {
	return httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusInternalServerError)
	}))
}
