package salesforce

import (
	"fmt"
	"strings"
)

const (
	InvalidSessionErrorCode = "INVALID_SESSION_ID"
)

// Salesforce error response if of array type
type APIErrors []*APIError

// Salesforce API response body error
type APIError struct {
	ErrorCode string `json:"errorCode"`
	Message   string `json:"message"`
}

func (e APIErrors) Exists() bool {
	if len(e) > 0 {
		return true
	}
	return false
}

func (e APIErrors) Error() string {
	return e.String()
}

func (e APIErrors) String() string {
	s := make([]string, len(e))
	for i, err := range e {
		s[i] = err.Error()
	}

	return strings.Join(s, "\n")
}

func (e APIError) Error() string {
	return e.String()
}

func (e APIError) String() string {
	return fmt.Sprintf("%#v", e)
}

func (e APIError) Exists() bool {
	if len(e.Message) > 0 || len(e.ErrorCode) > 0 {
		return true
	}
	return false
}

func (e APIErrors) InvalidSession() bool {
	for _, apiError := range e {
		if apiError.ErrorCode == InvalidSessionErrorCode {
			return true
		}
	}
	return false
}
