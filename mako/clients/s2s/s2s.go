package s2s

import (
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/mako/config"
	"code.justin.tv/sse/malachai/pkg/events"
	"code.justin.tv/sse/malachai/pkg/s2s/callee"
)

func NewClient(cfg *config.Configuration) (callee.ClientAPI, error) {
	eventsWriterClient, err := events.NewEventLogger(events.Config{
		BufferSizeLimit: 0,
	}, log.New())
	if err != nil {
		log.WithError(err).Error("Failed to create events writer client for S2S callee client")
		return nil, err
	}

	s2sCalleeClient := &callee.Client{
		ServiceName:        cfg.S2SServiceName,
		EventsWriterClient: eventsWriterClient,
		Config: &callee.Config{
			SupportWildCard: true,
			PassthroughMode: cfg.S2SPassthroughEnabled,
		},
	}

	err = s2sCalleeClient.Start()
	if err != nil {
		log.WithError(err).Error("Failed to initialize S2S Client")
		return nil, err
	}

	return s2sCalleeClient, nil
}
