package pdms

import (
	"context"
	"net/http"
	"time"

	pdms "code.justin.tv/amzn/PDMSLambdaTwirp"
	twirplambdatransport "code.justin.tv/amzn/TwirpGoLangAWSTransports/lambda"
	"github.com/afex/hystrix-go/hystrix"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials/stscreds"
	"github.com/aws/aws-sdk-go/aws/endpoints"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/lambda"
	"github.com/cenkalti/backoff"
	"google.golang.org/protobuf/types/known/timestamppb"
)

const (
	HystrixNamePDMSReportDeletion = "pdms.report.deletion"
	TimeoutMilliseconds           = 10000
	reportDeletionMaxRetries      = 1
	makoCatalogID                 = "103" // https://catalog.xarth.tv/services/103/details
)

type ClientWrapper interface {
	ReportDeletion(ctx context.Context, userID string) error
}

type clientWrapper struct {
	PDMSClient pdms.PDMSService
}

type Config struct {
	LambdaCallerRoleARN string
	LambdaARN           string
}

func NewClient(cfg Config) ClientWrapper {
	// From https://wiki.twitch.com/display/SEC/PDMS-+Delete+pipeline+Service+Onboarding
	sess := session.Must(session.NewSessionWithOptions(session.Options{
		Config: aws.Config{
			Region:              aws.String("us-west-2"),
			STSRegionalEndpoint: endpoints.RegionalSTSEndpoint,
			// We want a long timeout for PDMS, as Lambda based services can take a while to warm up on cold start.
			HTTPClient: &http.Client{Timeout: time.Duration(TimeoutMilliseconds) * time.Millisecond},
		},
	}))
	creds := stscreds.NewCredentials(sess, cfg.LambdaCallerRoleARN)
	pdmsLambdaCli := lambda.New(sess, &aws.Config{Credentials: creds})
	pdmsTransport := twirplambdatransport.NewClient(pdmsLambdaCli, cfg.LambdaARN)
	pdmsClient := pdms.NewPDMSServiceProtobufClient("", pdmsTransport)

	return &clientWrapper{
		PDMSClient: pdmsClient,
	}
}

func (c *clientWrapper) ReportDeletion(ctx context.Context, userID string) error {
	reportTimestamp := timestamppb.Now()

	f := func() error {
		return hystrix.DoC(ctx, HystrixNamePDMSReportDeletion, func(ctx context.Context) error {
			var err error
			_, err = c.PDMSClient.ReportDeletion(ctx, &pdms.ReportDeletionRequest{
				UserId:    userID,
				ServiceId: makoCatalogID,
				Timestamp: reportTimestamp,
			})
			return err
		}, nil)
	}

	b := backoff.WithMaxRetries(backoff.WithContext(newBackoff(), ctx), reportDeletionMaxRetries)
	err := backoff.Retry(f, b)
	if err != nil {
		return err
	}

	return nil
}

func newBackoff() backoff.BackOff {
	exponential := backoff.NewExponentialBackOff()
	exponential.InitialInterval = 50 * time.Millisecond
	// MaxElapsedTime is set to the total time the operation could possibly spend across all its attempts, plus 1 second to leave some room
	exponential.MaxElapsedTime = ((reportDeletionMaxRetries+1)*TimeoutMilliseconds)*time.Millisecond + time.Second
	exponential.Multiplier = 2
	return exponential
}
