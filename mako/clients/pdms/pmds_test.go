package pdms

import (
	"context"
	"errors"
	"testing"

	pdms_client_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/amzn/PDMSLambdaTwirp"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestPDMSWrapper(t *testing.T) {
	Convey("Test PDMS Client Wrapper", t, func() {
		mockPDMS := new(pdms_client_mock.PDMSService)
		clientWrapper := clientWrapper{
			PDMSClient: mockPDMS,
		}
		testUserID := "232889822"
		testContext := context.Background()

		Convey("Test ReportDeletion", func() {
			Convey("GIVEN pdms returns error for the max number of retries, THEN return error", func() {
				mockPDMS.On("ReportDeletion", testContext, mock.Anything).Return(nil, errors.New("dummy pdms error"))

				err := clientWrapper.ReportDeletion(testContext, testUserID)

				So(err, ShouldNotBeNil)
				So(err.Error(), ShouldContainSubstring, "dummy pdms error")
			})

			Convey("GIVEN pdms returns a non-nil response successfully after the max number of retries, THEN return the result", func() {
				// for the first N-many calls, error, but on the last retry, succeed
				mockPDMS.On("ReportDeletion", testContext, mock.Anything).Times(reportDeletionMaxRetries).Return(nil, errors.New("dummy pdms error"))
				mockPDMS.On("ReportDeletion", testContext, mock.Anything).Return(nil, nil)

				err := clientWrapper.ReportDeletion(testContext, testUserID)

				So(err, ShouldBeNil)
			})

			Convey("GIVEN pdms returns a non-nil response successfully immediately, THEN return the result", func() {
				mockPDMS.On("ReportDeletion", testContext, mock.Anything).Return(nil, nil)

				err := clientWrapper.ReportDeletion(testContext, testUserID)

				So(err, ShouldBeNil)
			})
		})
	})
}
