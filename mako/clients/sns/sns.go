package sns

import (
	"context"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sns"
)

// Client defines SNS struct
type Client struct {
	sns *sns.SNS
}

// ISNSClient defines the SNS client interface
type ISNSClient interface {
	PostToTopic(ctx context.Context, topic string, msg string) error
	PostToTopicWithMessageAttributes(ctx context.Context, topic string, msg string) error
}

const (
	defaultRegion               = "us-west-2"
	messageAttributeValue       = "Version"
	messageAttributeDataType    = "Number"
	messageAttributeStringValue = "1"
)

// NewDefaultConfig sets up a default region configuration
func NewDefaultConfig() *aws.Config {
	return &aws.Config{
		Region: aws.String(defaultRegion),
	}
}

// NewSnsClient creates new SNS client
func NewSnsClient() ISNSClient {
	return &Client{
		sns: sns.New(session.New(), NewDefaultConfig()),
	}
}

// PostToTopic posts to SNS ARN with message
func (s *Client) PostToTopic(ctx context.Context, topic string, msg string) error {
	publishInput := &sns.PublishInput{
		Message:  aws.String(msg),
		TopicArn: aws.String(topic),
	}
	_, err := s.sns.PublishWithContext(ctx, publishInput)
	return err
}

// PostToTopicWithMessageAttributes posts to SNS ARN with message and message attributes
func (s *Client) PostToTopicWithMessageAttributes(ctx context.Context, topic string, msg string) error {
	messageAttributes := map[string]*sns.MessageAttributeValue{
		messageAttributeValue: {
			DataType:    aws.String(messageAttributeDataType),
			StringValue: aws.String(messageAttributeStringValue),
		},
	}

	publishInput := &sns.PublishInput{
		Message:           aws.String(msg),
		TopicArn:          aws.String(topic),
		MessageAttributes: messageAttributes,
	}
	_, err := s.sns.PublishWithContext(ctx, publishInput)
	return err
}
