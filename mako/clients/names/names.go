package names

import (
	"context"
	"net/http"
	"time"

	"code.justin.tv/commerce/mako/config"
	namestwirp "code.justin.tv/safety/inference/rpc/names"
	"github.com/afex/hystrix-go/hystrix"
	"github.com/cenkalti/backoff"
	"github.com/pkg/errors"
)

const (
	HystrixNameNamesAcceptableNamesGet = "names.acceptablenames.GET"
	acceptableNamesMaxRetries          = 1
)

type ClientWrapper interface {
	AcceptableNames(ctx context.Context, names []string) (*namestwirp.AcceptableNamesResponse, error)
}

type clientWrapper struct {
	NamesClient namestwirp.Names
}

func NewClient(cfg *config.Configuration) ClientWrapper {
	return &clientWrapper{
		NamesClient: namestwirp.NewNamesProtobufClient(cfg.InferenceServiceEndpoint, &http.Client{
			Transport: &http.Transport{
				MaxIdleConnsPerHost: 10,
			},
		}),
	}
}

func (c *clientWrapper) AcceptableNames(ctx context.Context, names []string) (*namestwirp.AcceptableNamesResponse, error) {
	var resp *namestwirp.AcceptableNamesResponse

	f := func() error {
		return hystrix.DoC(ctx, HystrixNameNamesAcceptableNamesGet, func(ctx context.Context) error {
			var err error
			resp, err = c.NamesClient.AcceptableNames(ctx, &namestwirp.AcceptableNamesRequest{
				Usernames: names,
			})
			return err
		}, nil)
	}

	b := backoff.WithMaxRetries(backoff.WithContext(newBackoff(), ctx), acceptableNamesMaxRetries)
	err := backoff.Retry(f, b)
	if err != nil {
		return nil, err
	}
	if resp == nil {
		return nil, errors.Errorf("received nil response from names service for names %v", names)
	}

	return resp, err
}

func newBackoff() backoff.BackOff {
	exponential := backoff.NewExponentialBackOff()
	exponential.InitialInterval = 10 * time.Millisecond
	exponential.MaxElapsedTime = 1 * time.Second
	exponential.Multiplier = 2
	return exponential
}
