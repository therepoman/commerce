package names

import (
	"context"
	"errors"
	"testing"

	names_client_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/safety/inference/rpc/names"
	namestwirp "code.justin.tv/safety/inference/rpc/names"
	. "github.com/smartystreets/goconvey/convey"
)

func TestNamesClientWrapper(t *testing.T) {
	Convey("Test Names Client Wrapper", t, func() {
		mockNamesClient := new(names_client_mock.Names)
		clientWrapper := clientWrapper{
			NamesClient: mockNamesClient,
		}
		testNamesInput := []string{"testEmote"}
		testContext := context.Background()

		Convey("Test AcceptableNames", func() {
			Convey("Given names returns error for the max number of retries, then return error", func() {
				mockNamesClient.On("AcceptableNames", testContext, &namestwirp.AcceptableNamesRequest{
					Usernames: testNamesInput,
				}).Return(nil, errors.New("dummy names error"))

				resp, err := clientWrapper.AcceptableNames(testContext, testNamesInput)

				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})

			Convey("Given names returns nil, then return error", func() {
				mockNamesClient.On("AcceptableNames", testContext, &namestwirp.AcceptableNamesRequest{
					Usernames: testNamesInput,
				}).Return(nil, nil)

				resp, err := clientWrapper.AcceptableNames(testContext, testNamesInput)

				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})

			Convey("Given names succeeds immediately, then return the response as is", func() {
				testResp := &namestwirp.AcceptableNamesResponse{
					Tier: []namestwirp.OffensiveTier_Tier{namestwirp.OffensiveTier_VERY_LIKELY},
				}
				mockNamesClient.On("AcceptableNames", testContext, &namestwirp.AcceptableNamesRequest{
					Usernames: testNamesInput,
				}).Return(testResp, nil)

				resp, err := clientWrapper.AcceptableNames(testContext, testNamesInput)

				So(err, ShouldBeNil)
				So(resp, ShouldResemble, testResp)
			})

			Convey("Given names succeeds after the max number of retries, then return the response as is", func() {
				testResp := &namestwirp.AcceptableNamesResponse{
					Tier: []namestwirp.OffensiveTier_Tier{namestwirp.OffensiveTier_VERY_LIKELY},
				}

				// for the first N-many calls, error, but on the last retry, succeed
				mockNamesClient.On("AcceptableNames", testContext, &namestwirp.AcceptableNamesRequest{
					Usernames: testNamesInput,
				}).Times(acceptableNamesMaxRetries).Return(nil, errors.New("dummy names error"))
				mockNamesClient.On("AcceptableNames", testContext, &namestwirp.AcceptableNamesRequest{
					Usernames: testNamesInput,
				}).Return(testResp, nil)

				resp, err := clientWrapper.AcceptableNames(testContext, testNamesInput)

				So(err, ShouldBeNil)
				So(resp, ShouldResemble, testResp)
			})
		})
	})
}
