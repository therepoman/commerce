package subscriptions

import (
	"context"
	"time"

	"code.justin.tv/commerce/mako/config"
	"code.justin.tv/foundation/twitchclient"
	substwirp "code.justin.tv/revenue/subscriptions/twirp"
	"github.com/afex/hystrix-go/hystrix"
	"github.com/cenkalti/backoff"
)

const (
	HystrixNameSubscriptionsChannelEmoteLimitsGet = "subscriptions.channelemotelimits.GET"
	HystrixNameSubscriptionsChannelProductsGet    = "subscriptions.channelproducts.GET"
	getChannelEmoteLimitsMaxRetries               = 1
)

type ClientWrapper interface {
	GetChannelEmoteLimits(ctx context.Context, channelID string) (*substwirp.GetChannelEmoteLimitsResponse, error)
	GetChannelProducts(ctx context.Context, channelID string) (*substwirp.GetChannelProductsResponse, error)
}

type clientWrapper struct {
	SubscriptionsClient substwirp.Subscriptions
}

func NewClient(cfg *config.Configuration) ClientWrapper {
	return &clientWrapper{
		SubscriptionsClient: substwirp.NewSubscriptionsProtobufClient(cfg.SubscriptionsHost, twitchclient.NewHTTPClient(twitchclient.ClientConf{
			Host: cfg.SubscriptionsHost,
			Transport: twitchclient.TransportConf{
				MaxIdleConnsPerHost: 10,
			},
		})),
	}
}

func (c *clientWrapper) GetChannelEmoteLimits(ctx context.Context, channelID string) (*substwirp.GetChannelEmoteLimitsResponse, error) {
	var resp *substwirp.GetChannelEmoteLimitsResponse

	f := func() error {
		return hystrix.DoC(ctx, HystrixNameSubscriptionsChannelEmoteLimitsGet, func(ctx context.Context) error {
			var err error
			resp, err = c.SubscriptionsClient.GetChannelEmoteLimits(ctx, &substwirp.GetChannelEmoteLimitsRequest{
				ChannelId: channelID,
			})
			return err
		}, nil)
	}

	b := backoff.WithMaxRetries(backoff.WithContext(newBackoff(), ctx), getChannelEmoteLimitsMaxRetries)
	err := backoff.Retry(f, b)

	return resp, err
}

func newBackoff() backoff.BackOff {
	exponential := backoff.NewExponentialBackOff()
	exponential.InitialInterval = 10 * time.Millisecond
	exponential.MaxElapsedTime = 2 * time.Second
	exponential.Multiplier = 2
	return exponential
}

func (c *clientWrapper) GetChannelProducts(ctx context.Context, channelID string) (*substwirp.GetChannelProductsResponse, error) {
	var resp *substwirp.GetChannelProductsResponse

	err := hystrix.DoC(ctx, HystrixNameSubscriptionsChannelProductsGet, func(ctx context.Context) error {
		var err error
		resp, err = c.SubscriptionsClient.GetChannelProducts(ctx, &substwirp.GetChannelProductsRequest{ChannelId: channelID})
		return err
	}, nil)

	return resp, err
}
