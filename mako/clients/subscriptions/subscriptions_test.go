package subscriptions

import (
	"context"
	"errors"
	"testing"

	subs_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/revenue/subscriptions/twirps"
	substwirp "code.justin.tv/revenue/subscriptions/twirp"
	. "github.com/smartystreets/goconvey/convey"
)

func TestSubscriptionsClientWrapper(t *testing.T) {
	Convey("Test Subscriptions Client Wrapper", t, func() {
		mockSubsClient := new(subs_mock.Subscriptions)
		clientWrapper := clientWrapper{
			SubscriptionsClient: mockSubsClient,
		}

		ctx := context.Background()
		userID := "someUser"

		Convey("Test GetChannelEmoteLimits", func() {
			Convey("With error from Subscriptions client for max number of retries", func() {
				mockSubsClient.On("GetChannelEmoteLimits", ctx, &substwirp.GetChannelEmoteLimitsRequest{
					ChannelId: userID,
				}).Return(nil, errors.New("dummy error"))

				resp, err := clientWrapper.GetChannelEmoteLimits(ctx, userID)

				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})

			Convey("With immediate success from Subscriptions client", func() {
				testResp := &substwirp.GetChannelEmoteLimitsResponse{
					ChannelId: userID,
					Limits:    nil,
				}
				mockSubsClient.On("GetChannelEmoteLimits", ctx, &substwirp.GetChannelEmoteLimitsRequest{
					ChannelId: userID,
				}).Return(testResp, nil)

				resp, err := clientWrapper.GetChannelEmoteLimits(ctx, userID)

				So(err, ShouldBeNil)
				So(resp, ShouldResemble, testResp)
			})

			Convey("With success from Subscriptions client after max number of retries", func() {
				testResp := &substwirp.GetChannelEmoteLimitsResponse{
					ChannelId: userID,
					Limits:    nil,
				}

				// for the first N-many calls, error, but on the last retry, succeed
				mockSubsClient.On("GetChannelEmoteLimits", ctx, &substwirp.GetChannelEmoteLimitsRequest{
					ChannelId: userID,
				}).Times(getChannelEmoteLimitsMaxRetries).Return(nil, errors.New("dummy error"))
				mockSubsClient.On("GetChannelEmoteLimits", ctx, &substwirp.GetChannelEmoteLimitsRequest{
					ChannelId: userID,
				}).Return(testResp, nil)

				resp, err := clientWrapper.GetChannelEmoteLimits(ctx, userID)

				So(err, ShouldBeNil)
				So(resp, ShouldResemble, testResp)
			})
		})
	})
}
