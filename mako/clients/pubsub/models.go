package pubsub

import (
	mako "code.justin.tv/commerce/mako/internal"
	"time"
)

type ChannelEmoteUpdatesEventType string

const (
	EmoteSetsType ChannelEmoteUpdatesEventType = "emote_sets"
)

// UserSubscription is the schema of the user-subscribe-events-v1 event
type UserSubscribeEvent struct {
	UserID    string `json:"user_id"`
	ChannelID string `json:"channel_id"`
}

// ChannelEmoteUpdates is the schema of the channel-emote-updates event
type ChannelEmoteUpdatesEvent struct {
	Type ChannelEmoteUpdatesEventType `json:"type"` //Currently only "emote_sets" is supported
	Body []PubsubEmoteSet             `json:"body"`
}

// Because pubsub messages are public, we need to convert the internal mako emote and emote group shape
// into one that closely resembles GQL (i.e. is publically acceptable to share)
type PubsubEmoteSet struct {
	// Note: for now, we do not supply ProductType on this struct the way GQL does, since we do not want to have to make
	// a call to Product Catalog for every emote update. This can be revisited in the future.
	Emotes    []PubsubEmote `json:"emotes"`
	SetID     string        `json:"id"`
	OwnerID   string        `json:"owner_id"`
	AssetType string        `json:"emote_group_asset_type"`
}

type PubsubEmote struct {
	AssetType mako.EmoteAssetType `json:"asset_type"`
	CreatedAt time.Time           `json:"created_at"`
	Id        string              `json:"id"`
	Modifiers []string            `json:"modifiers"`
	Order     int64               `json:"order"`
	OwnerID   string              `json:"owner_id"`
	GroupID   string              `json:"set_id"`
	State     mako.EmoteState     `json:"state"`
	Suffix    string              `json:"suffix"`
	Code      string              `json:"text"`
	Type      string              `json:"type"`
}

func ToPubsubEmoteSet(groupID string, ownerID string, assetType string, emotes []mako.Emote) PubsubEmoteSet {
	pubsubEmotes := make([]PubsubEmote, 0, len(emotes))
	for _, emote := range emotes {
		pubsubEmote := PubsubEmote{
			AssetType: emote.AssetType,
			CreatedAt: emote.CreatedAt,
			Id:        emote.ID,
			Modifiers: emote.Modifiers,
			Order:     emote.Order,
			OwnerID:   emote.OwnerID,
			GroupID:   emote.GroupID,
			State:     emote.State,
			Suffix:    emote.Suffix,
			Code:      emote.Code,
			Type:      emote.EmotesGroup,
		}
		pubsubEmotes = append(pubsubEmotes, pubsubEmote)
	}

	return PubsubEmoteSet{
		Emotes:    pubsubEmotes,
		SetID:     groupID,
		OwnerID:   ownerID,
		AssetType: assetType,
	}
}
