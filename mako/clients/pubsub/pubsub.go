package pubsub

//go:generate mockery -name=IPublisher -output ../../mocks

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"

	pubsubService "code.justin.tv/chat/pubsub-go-pubclient/client"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/foundation/twitchclient"
)

const (
	userSubscribeTopicFormat       = "user-subscribe-events-v1.%s"
	channelEmoteUpdatesTopicFormat = "channel-emote-updates.%s"
)

// Publisher is the interface that wraps publishing of Pubsub messages using
// the HTTP client to the Pubsub service.
type IPublisher interface {
	// UserSubscription posts a message to Pubsub to notify a user of their subscription in
	// a channel.
	UserSubscription(ctx context.Context, params UserSubscribeEvent, opt *twitchclient.ReqOpts) error
	UpdateChannelEmoteGroups(ctx context.Context, channelID string, pubsubEmoteSets []PubsubEmoteSet, opt *twitchclient.ReqOpts) error
}

// NewPublisher returns a publisher wrapper of the provided pubsub PubClient
func NewPublisher(pubClient pubsubService.PubClient) IPublisher {
	return &publisher{pubClient}
}

// publisher contains both the HTTP client and the application configuration.
type publisher struct {
	pubsubService.PubClient
}

func (p *publisher) UserSubscription(ctx context.Context, params UserSubscribeEvent, opt *twitchclient.ReqOpts) error {
	message, err := json.Marshal(params)

	if err != nil {
		msg := fmt.Sprintf("PubsubPublisher: Error while marshaling UserSubscription %v", err)
		log.WithError(err).Error(msg)
		return errors.New(msg)
	}

	channelTopic := fmt.Sprintf(userSubscribeTopicFormat, params.UserID)

	err = p.Publish(ctx, []string{channelTopic}, string(message), opt)

	if err != nil {
		msg := fmt.Sprintf("PubsubPublisher: Error while publishing subscription notification %v", err)
		log.WithError(err).Error(msg)
		return errors.New(msg)
	}

	return nil
}

func (p *publisher) UpdateChannelEmoteGroups(ctx context.Context, channelID string, pusubEmoteSets []PubsubEmoteSet, opt *twitchclient.ReqOpts) error {
	// Prepare pubsub message
	messageBody := ChannelEmoteUpdatesEvent{
		Type: EmoteSetsType,
		Body: pusubEmoteSets,
	}
	message, err := json.Marshal(messageBody)
	if err != nil {
		msg := fmt.Sprintf("PubsubPublisher: Error while marshaling ChannelEmoteUpdates %v", err)
		log.WithError(err).Error(msg)
		return errors.New(msg)
	}
	channelTopic := fmt.Sprintf(channelEmoteUpdatesTopicFormat, channelID)

	// Send pubsub message
	err = p.Publish(ctx, []string{channelTopic}, string(message), opt)
	if err != nil {
		msg := fmt.Sprintf("PubsubPublisher: Error while publishing channel emote updates notification %v", err)
		log.WithError(err).Error(msg)
		return errors.New(msg)
	}

	return nil
}

func NewPublisherFake() IPublisher {
	return &publisherFake{}
}

// publisherFake is a noop client for running inside table unit tests
type publisherFake struct{}

func (p *publisherFake) UserSubscription(ctx context.Context, params UserSubscribeEvent, opt *twitchclient.ReqOpts) error {
	return nil
}

func (p *publisherFake) UpdateChannelEmoteGroups(ctx context.Context, channelID string, pubsubEmoteSets []PubsubEmoteSet, opt *twitchclient.ReqOpts) error {
	return nil
}
