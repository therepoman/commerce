package pubsub

import (
	"code.justin.tv/commerce/gogogadget/strings"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/mako/config"
	"code.justin.tv/commerce/mako/internal"
	"context"
	"fmt"
	"time"
)

func NewEmoteDBWithPubsub(base mako.EmoteDB, pubsub IPublisher, dynamicConfig config.DynamicConfigClient) mako.EmoteDB {
	return &emoteDBWithPubsub{
		base:          base,
		pubsub:        pubsub,
		dynamicConfig: dynamicConfig,
	}
}

type emoteDBWithPubsub struct {
	base          mako.EmoteDB
	pubsub        IPublisher
	dynamicConfig config.DynamicConfigClient
}

func (db *emoteDBWithPubsub) ReadByIDs(ctx context.Context, ids ...string) ([]mako.Emote, error) {
	return db.base.ReadByIDs(ctx, ids...)
}

func (db *emoteDBWithPubsub) ReadByCodes(ctx context.Context, codes ...string) ([]mako.Emote, error) {
	return db.base.ReadByCodes(ctx, codes...)
}

func (db *emoteDBWithPubsub) ReadByGroupIDs(ctx context.Context, ids ...string) ([]mako.Emote, error) {
	return db.base.ReadByGroupIDs(ctx, ids...)
}

func (db *emoteDBWithPubsub) WriteEmotes(ctx context.Context, emotes ...mako.Emote) ([]mako.Emote, error) {
	// Because WriteEmotes should only ever be used during tests or to create an emote in the CreateEmote flow,
	// we can assume that there was no groupID that any of these emotes belonged to before this write. For that reason,
	// we do not need to look up any "pre-changes" emotes.

	// Write the emotes
	emotes, err := db.base.WriteEmotes(ctx, emotes...)
	if err != nil {
		return nil, err
	}

	// Only do pubsub updates if the emotes are under an owner who is eligible for pubsub updates (via dynamic config)
	filteredEmotes := db.filterEmotesByPubsubUpdateEligibility(emotes...)

	// Get the unique groupIDs of the relevant updated emotes
	groupIDs := gatherUniqueGroupIDs(filteredEmotes...)

	// Send a pubsub for all emotes in the emote group that was changed
	go func() {
		db.sendEmoteUpdates("WriteEmotes", groupIDs...)
	}()

	return emotes, nil
}

func (db *emoteDBWithPubsub) DeleteEmote(ctx context.Context, id string) (mako.Emote, error) {
	// Delete the emote
	emote, err := db.base.DeleteEmote(ctx, id)
	if err != nil {
		return mako.Emote{}, err
	}

	// Only do pubsub updates if the emotes are under an owner who is eligible for pubsub updates (via dynamic config)
	filteredEmotes := db.filterEmotesByPubsubUpdateEligibility(emote)

	// Get the unique groupIDs of the relevant updated emotes
	groupIDs := gatherUniqueGroupIDs(filteredEmotes...)

	// Send a pubsub for all emotes in the emote group that was changed
	go func() {
		db.sendEmoteUpdates("DeleteEmote", groupIDs...)
	}()

	return emote, nil
}

func (db *emoteDBWithPubsub) UpdateState(ctx context.Context, id string, state mako.EmoteState) (mako.Emote, error) {
	// Update the emote state
	emote, err := db.base.UpdateState(ctx, id, state)
	if err != nil {
		return mako.Emote{}, err
	}

	// Only do pubsub updates if the emotes are under an owner who is eligible for pubsub updates (via dynamic config)
	filteredEmotes := db.filterEmotesByPubsubUpdateEligibility(emote)

	// Get the unique groupIDs of the relevant updated emotes
	groupIDs := gatherUniqueGroupIDs(filteredEmotes...)

	// Send a pubsub for all emotes in the emote group that was changed
	go func() {
		db.sendEmoteUpdates("UpdateState", groupIDs...)
	}()

	return emote, nil
}

func (db *emoteDBWithPubsub) UpdateCode(ctx context.Context, id, code, suffix string) (mako.Emote, error) {
	// Update the emote code
	emote, err := db.base.UpdateCode(ctx, id, code, suffix)
	if err != nil {
		return mako.Emote{}, err
	}

	// Only do pubsub updates if the emotes are under an owner who is eligible for pubsub updates (via dynamic config)
	filteredEmotes := db.filterEmotesByPubsubUpdateEligibility(emote)

	// Get the unique groupIDs of the relevant updated emotes
	groupIDs := gatherUniqueGroupIDs(filteredEmotes...)

	// Send a pubsub for all emotes in the emote group that was changed
	go func() {
		db.sendEmoteUpdates("UpdateCode", groupIDs...)
	}()

	return emote, nil
}

func (db *emoteDBWithPubsub) UpdatePrefix(ctx context.Context, id, code, prefix string) (mako.Emote, error) {
	// Update the emote prefix
	emote, err := db.base.UpdatePrefix(ctx, id, code, prefix)
	if err != nil {
		return mako.Emote{}, err
	}

	// Only do pubsub updates if the emotes are under an owner who is eligible for pubsub updates (via dynamic config)
	filteredEmotes := db.filterEmotesByPubsubUpdateEligibility(emote)

	// Get the unique groupIDs of the relevant updated emotes
	groupIDs := gatherUniqueGroupIDs(filteredEmotes...)

	// Send a pubsub for all emotes in the emote group that was changed
	go func() {
		db.sendEmoteUpdates("UpdatePrefix", groupIDs...)
	}()

	return emote, nil
}

func (db *emoteDBWithPubsub) UpdateEmote(ctx context.Context, id string, params mako.EmoteUpdate) (mako.Emote, error) {
	var emotesAffected []mako.Emote

	// If there's a groupID change coming up
	if params.GroupID != nil {
		// Look up the emote before updating so that we can make sure to publish the emotes in both its old and new groups
		emoteByIDs, err := db.base.ReadByIDs(ctx, id)
		if err != nil || len(emoteByIDs) == 0 {
			return mako.Emote{}, err
		}
		emotesAffected = append(emotesAffected, emoteByIDs[0])
	}

	// Update the emote
	emote, err := db.base.UpdateEmote(ctx, id, params)
	if err != nil {
		return mako.Emote{}, err
	}
	emotesAffected = append(emotesAffected, emote)

	// Only do pubsub updates if the emotes are under an owner who is eligible for pubsub updates (via dynamic config)
	filteredEmotes := db.filterEmotesByPubsubUpdateEligibility(emotesAffected...)

	// Get the unique groupIDs of the relevant updated emotes
	groupIDs := gatherUniqueGroupIDs(filteredEmotes...)

	// Send a pubsub for all emotes in the emote group(s) that was/were changed
	go func() {
		db.sendEmoteUpdates("UpdateEmote", groupIDs...)
	}()

	return emote, nil
}

func (db *emoteDBWithPubsub) UpdateEmotes(ctx context.Context, emotesToUpdate map[string]mako.EmoteUpdate) ([]mako.Emote, error) {
	// Note that UpdateEmotes' response contains the emote data from before and after the changes were made, so we do
	// not need to look anything up in addition to this response
	emotesToExpire, err := db.base.UpdateEmotes(ctx, emotesToUpdate)
	if err != nil {
		return nil, err
	}

	// Only do pubsub updates if the emotes are under an owner who is eligible for pubsub updates (via dynamic config)
	filteredEmotes := db.filterEmotesByPubsubUpdateEligibility(emotesToExpire...)

	// Get the unique groupIDs of the relevant updated emotes
	groupIDs := gatherUniqueGroupIDs(filteredEmotes...)

	// Send a pubsub for all emotes in the emote group(s) that was/were changed
	go func() {
		db.sendEmoteUpdates("UpdateEmotes", groupIDs...)
	}()

	return emotesToExpire, nil
}

func (db *emoteDBWithPubsub) Query(ctx context.Context, query mako.EmoteQuery) ([]mako.Emote, error) {
	return db.base.Query(ctx, query)
}

func (db *emoteDBWithPubsub) Close() error {
	return db.base.Close()
}

// This function should always be wrapped in a goroutine, which is why log rather than returning any errors
func (db *emoteDBWithPubsub) sendEmoteUpdates(operation string, groupIDs ...string) {
	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)

	if len(groupIDs) == 0 {
		return
	}
	emotes, err := db.base.ReadByGroupIDs(ctx, groupIDs...)
	if err != nil {
		log.WithError(err).WithField("groupIDs", groupIDs).Error(fmt.Sprintf("Error looking up emotes by groupID for pubsub after performing %s", operation))
	}

	//Filter to only include active emotes, since we are updating the emote-picker, which is only active emotes
	var activeEmotes = make([]mako.Emote, 0, len(emotes))
	for _, emote := range emotes {
		if emote.State == mako.Active {
			activeEmotes = append(activeEmotes, emote)
		}
	}

	emoteGroupsByOwnerID := groupEmoteGroupsByOwnerID(activeEmotes...)
	for ownerID, emotesByGroupID := range emoteGroupsByOwnerID {
		pubsubEmoteSets := make([]PubsubEmoteSet, 0, len(emotesByGroupID))
		for groupID, emotes := range emotesByGroupID {
			groupAssetType := mako.EmoteGroupStatic
			if emotes[0].AssetType == mako.AnimatedAssetType {
				groupAssetType = mako.EmoteGroupAnimated
			}

			pubsubEmoteSet := ToPubsubEmoteSet(groupID, ownerID, groupAssetType.String(), emotes)
			pubsubEmoteSets = append(pubsubEmoteSets, pubsubEmoteSet)
		}

		if len(pubsubEmoteSets) == 0 {
			continue
		}

		err := db.pubsub.UpdateChannelEmoteGroups(ctx, ownerID, pubsubEmoteSets, nil)
		if err != nil {
			log.WithError(err).WithField("ownerID", ownerID).Error(fmt.Sprintf("Error sending pubsub after performing %s", operation))
		}
	}
	cancel()
}

func (db *emoteDBWithPubsub) filterEmotesByPubsubUpdateEligibility(emotes ...mako.Emote) []mako.Emote {
	ownerIDsMap := make(map[string]bool, 0)
	filteredEmotes := make([]mako.Emote, 0, len(emotes))
	for _, emote := range emotes {
		// TODO: after rolling out to 100% and addressing any issues that come up, this dynamic config check can be removed
		// Check whether we need to look up this ownerID in the dynamic config
		_, ok := ownerIDsMap[emote.OwnerID]
		if !ok {
			// Cache result in memory for this request in case we update a 50+ emote group
			ownerIDsMap[emote.OwnerID] = db.dynamicConfig.EnableEmoteUpdatePubsub(emote.OwnerID)
		}

		// Now that the in-memory request cache has been updated, determine whether this emote should get a pubsub update
		if ownerIDsMap[emote.OwnerID] {
			filteredEmotes = append(filteredEmotes, emote)
		}
	}
	return filteredEmotes
}

func gatherUniqueGroupIDs(emotes ...mako.Emote) []string {
	if len(emotes) == 0 {
		return nil
	}

	groupIDs := make([]string, 0, len(emotes))
	for _, emote := range emotes {
		// Collect this emote's groupID if it has one
		if emote.GroupID != "" {
			groupIDs = append(groupIDs, emote.GroupID)
		}
	}

	return strings.Dedupe(groupIDs)
}

// We need all affected emotes from all affected emote groups to be sent in the pubsub update
// We need these emote groups by ownerID so we can send relevant pubsub updates on each channel topic
func groupEmoteGroupsByOwnerID(emotes ...mako.Emote) map[string]map[string][]mako.Emote {
	groupIDsByOwnerID := make(map[string]map[string][]mako.Emote, 0)

	for _, emote := range emotes {
		groupID := emote.GroupID
		ownerID := emote.OwnerID

		// If there is no entry for this channel yet, initialize an empty map
		if _, ok := groupIDsByOwnerID[ownerID]; !ok {
			groupIDsByOwnerID[ownerID] = make(map[string][]mako.Emote, 0)
		}

		// If there is no entry for this group yet, initialize an empty array
		if _, ok := groupIDsByOwnerID[ownerID][groupID]; !ok {
			groupIDsByOwnerID[ownerID][groupID] = make([]mako.Emote, 0, len(emotes))
		}

		// Add each emote under this groupID under this ownerID
		groupIDsByOwnerID[ownerID][groupID] = append(groupIDsByOwnerID[ownerID][groupID], emote)
	}

	return groupIDsByOwnerID
}
