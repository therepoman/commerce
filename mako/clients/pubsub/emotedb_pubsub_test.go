package pubsub_test

import (
	"code.justin.tv/commerce/mako/clients/pubsub"
	"context"
	"testing"
	"time"

	"code.justin.tv/commerce/mako/config"
	"code.justin.tv/commerce/mako/dynamo"
	mako "code.justin.tv/commerce/mako/internal"
	"code.justin.tv/commerce/mako/localdb"
	pubsub_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/pubsub"
	config_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/config"
	"code.justin.tv/commerce/mako/tests"
	mk "code.justin.tv/commerce/mako/twirp"
	"github.com/segmentio/ksuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

// TestLocalEmoteDBWithPubsub runs the test suite wrapping a local EmoteDB with a pubsub decorator to
// ensure that no functionality has been lost/altered. Then it wraps another local EmoteDB with mocked dependencies
// to test layer-specific functionality.
func TestLocalEmoteDBWithPubsub(t *testing.T) {
	t.Run("Base EmoteDB functionality", func(t *testing.T) {
		t.Parallel()
		tests.TestEmoteDB(t, makeEmoteDBWithPubsub(localdb.MakeEmoteDB))
	})

	t.Run("EmoteWithPubsub Specific Tests", func(t *testing.T) {
		t.Parallel()
		testPubsubLayerSpecifically(t, makeEmoteDBWithMockedPubsub(localdb.MakeEmoteDB))
	})
}

// TestLocalEmoteDBWithPubsub runs the test suite wrapping a dynamo EmoteDB with a pubsub decorator to
// ensure that no functionality has been lost/altered. Then it wraps another dynamo EmoteDB with mocked dependencies
// to test layer-specific functionality.
func TestDynamoEmoteDBWithPubsub(t *testing.T) {
	t.Run("Base EmoteDB functionality", func(t *testing.T) {
		t.Parallel()
		tests.TestEmoteDB(t, makeEmoteDBWithPubsub(dynamo.MakeEmoteDB))
	})

	t.Run("EmoteWithPubsub Specific Tests", func(t *testing.T) {
		t.Parallel()
		testPubsubLayerSpecifically(t, makeEmoteDBWithMockedPubsub(dynamo.MakeEmoteDB))
	})
}

func testPubsubLayerSpecifically(t *testing.T, makeEmoteDBWithPubsub MakeEmoteDBWithMockedPubsub) {
	t.Run("WriteEmotes", func(t *testing.T) {
		t.Parallel()
		testPubsubLayerWriteEmotes(t, makeEmoteDBWithPubsub)
	})

	t.Run("DeleteEmote", func(t *testing.T) {
		t.Parallel()
		testPubsubLayerDeleteEmote(t, makeEmoteDBWithPubsub)
	})

	t.Run("UpdateState", func(t *testing.T) {
		t.Parallel()
		testPubsubLayerUpdateState(t, makeEmoteDBWithPubsub)
	})

	t.Run("UpdateCode", func(t *testing.T) {
		t.Parallel()
		testPubsubLayerUpdateCode(t, makeEmoteDBWithPubsub)
	})

	t.Run("UpdatePrefix", func(t *testing.T) {
		t.Parallel()
		testPubsubLayerUpdatePrefix(t, makeEmoteDBWithPubsub)
	})

	t.Run("UpdateEmote", func(t *testing.T) {
		t.Parallel()
		testPubsubLayerUpdateEmote(t, makeEmoteDBWithPubsub)
	})

	t.Run("UpdateEmotes", func(t *testing.T) {
		t.Parallel()
		testPubsubLayerUpdateEmotes(t, makeEmoteDBWithPubsub)
	})
}

func makeEmoteDBWithPubsub(makeBase tests.MakeEmoteDB) tests.MakeEmoteDB {
	return func(ctx context.Context, emotes ...mako.Emote) (mako.EmoteDB, func(), error) {
		base, teardown, err := makeBase(ctx, emotes...)
		if err != nil {
			return nil, nil, err
		}

		var pubsubClient = pubsub.NewPublisherFake()
		var dynamicConfigClient = config.NewDynamicConfigFake()

		return pubsub.NewEmoteDBWithPubsub(base, pubsubClient, dynamicConfigClient), teardown, nil
	}
}

type EmoteDBWithPubsubMocksSetup struct {
	pubsubMock        *pubsub_mock.IPublisher
	dynamicConfigMock *config_mock.DynamicConfigClient
}

type MakeEmoteDBWithMockedPubsub func(ctx context.Context, mocks EmoteDBWithPubsubMocksSetup, emotes ...mako.Emote) (mako.EmoteDB, func(), error)

func makeEmoteDBWithMockedPubsub(makeBase tests.MakeEmoteDB) MakeEmoteDBWithMockedPubsub {
	return func(ctx context.Context, mocks EmoteDBWithPubsubMocksSetup, emotes ...mako.Emote) (mako.EmoteDB, func(), error) {
		base, teardown, err := makeBase(ctx, emotes...)
		if err != nil {
			return nil, nil, err
		}

		var pubsubClient pubsub.IPublisher
		if mocks.pubsubMock != nil {
			pubsubClient = mocks.pubsubMock
		} else {
			pubsubClient = pubsub.NewPublisherFake()
		}

		var dynamicConfigClient config.DynamicConfigClient
		if mocks.dynamicConfigMock != nil {
			dynamicConfigClient = mocks.dynamicConfigMock
		} else {
			dynamicConfigClient = config.NewDynamicConfigFake()
		}

		return pubsub.NewEmoteDBWithPubsub(base, pubsubClient, dynamicConfigClient), teardown, nil
	}
}

func testPubsubLayerWriteEmotes(t *testing.T, makeEmoteDBWithPubsub MakeEmoteDBWithMockedPubsub) {
	tests := []struct {
		scenario                string
		emotes                  []mako.Emote
		PubsubError             error
		EnableEmoteUpdatePubsub bool
		test                    func(ctx context.Context, t *testing.T, db mako.EmoteDB, mocks EmoteDBWithPubsubMocksSetup, emotes ...mako.Emote)
	}{
		{
			scenario:                "If the dynamic config is not enabled, send no update",
			emotes:                  nil,
			PubsubError:             nil,
			EnableEmoteUpdatePubsub: false,
			test: func(ctx context.Context, t *testing.T, db mako.EmoteDB, mocks EmoteDBWithPubsubMocksSetup, emotes ...mako.Emote) {
				resp, err := db.WriteEmotes(ctx, []mako.Emote{
					{
						ID:          ksuid.New().String(),
						OwnerID:     ksuid.New().String(),
						GroupID:     ksuid.New().String(),
						Prefix:      ksuid.New().String(),
						Suffix:      ksuid.New().String(),
						Code:        ksuid.New().String(),
						State:       mako.Active,
						Domain:      mako.EmotesDomain,
						EmotesGroup: mk.EmoteGroup_Subscriptions.String(),
					},
				}...)
				assert.NotEmpty(t, resp)
				assert.Nil(t, err)

				// Wait for async pubsub message to go out
				time.Sleep(1 * time.Second)

				mocks.pubsubMock.AssertNumberOfCalls(t, "UpdateChannelEmoteGroups", 0)
			},
		},
		{
			scenario:                "Writing an active emote in a slot (create) should result in one pubsub update",
			emotes:                  nil,
			PubsubError:             nil,
			EnableEmoteUpdatePubsub: true,
			test: func(ctx context.Context, t *testing.T, db mako.EmoteDB, mocks EmoteDBWithPubsubMocksSetup, emotes ...mako.Emote) {
				emotesToWrite := []mako.Emote{
					{
						ID:          ksuid.New().String(),
						OwnerID:     "SameOwner",
						GroupID:     "SameGroup",
						Prefix:      ksuid.New().String(),
						Suffix:      ksuid.New().String(),
						Code:        ksuid.New().String(),
						State:       mako.Active,
						Domain:      mako.EmotesDomain,
						EmotesGroup: mk.EmoteGroup_Subscriptions.String(),
					},
				}

				resp, err := db.WriteEmotes(ctx, emotesToWrite...)
				assert.NotEmpty(t, resp)
				assert.Nil(t, err)

				// Wait for async pubsub message to go out
				time.Sleep(1 * time.Second)

				mocks.pubsubMock.AssertNumberOfCalls(t, "UpdateChannelEmoteGroups", 1)
				call := mocks.pubsubMock.Calls[0]
				ownerID, ok := call.Arguments[1].(string)
				assert.True(t, ok)
				assert.Equal(t, emotesToWrite[0].OwnerID, ownerID)

				pubsubEmoteSets, ok := call.Arguments[2].([]pubsub.PubsubEmoteSet)
				assert.True(t, ok)
				assert.Equal(t, 1, len(pubsubEmoteSets))

				pubsubEmotes := pubsubEmoteSets[0].Emotes
				assert.Equal(t, 1, len(pubsubEmotes))
			},
		},
		{
			scenario:                "Writing an archive emote (create library emote) should result in no pubsub update",
			emotes:                  nil,
			PubsubError:             nil,
			EnableEmoteUpdatePubsub: true,
			test: func(ctx context.Context, t *testing.T, db mako.EmoteDB, mocks EmoteDBWithPubsubMocksSetup, emotes ...mako.Emote) {
				emotesToWrite := []mako.Emote{
					{
						ID:          ksuid.New().String(),
						OwnerID:     "SameOwner",
						Prefix:      ksuid.New().String(),
						Suffix:      ksuid.New().String(),
						Code:        ksuid.New().String(),
						State:       mako.Archived,
						Domain:      mako.EmotesDomain,
						EmotesGroup: mk.EmoteGroup_Archive.String(),
					},
				}

				resp, err := db.WriteEmotes(ctx, emotesToWrite...)
				assert.NotEmpty(t, resp)
				assert.Nil(t, err)

				// Wait for async pubsub message to go out
				time.Sleep(1 * time.Second)

				mocks.pubsubMock.AssertNumberOfCalls(t, "UpdateChannelEmoteGroups", 0)
			},
		},
	}

	for _, test := range tests {
		test := test

		t.Run(test.scenario, func(t *testing.T) {
			t.Parallel()

			ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
			defer cancel()

			pubsubMock := new(pubsub_mock.IPublisher)
			dynamicConfigMock := new(config_mock.DynamicConfigClient)

			mocks := EmoteDBWithPubsubMocksSetup{
				pubsubMock:        pubsubMock,
				dynamicConfigMock: dynamicConfigMock,
			}

			db, teardown, err := makeEmoteDBWithPubsub(ctx, mocks, test.emotes...)
			if err != nil {
				t.Fatal(err)
			}

			pubsubMock.On("UpdateChannelEmoteGroups", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(test.PubsubError)
			dynamicConfigMock.On("EnableEmoteUpdatePubsub", mock.Anything).Return(test.EnableEmoteUpdatePubsub)
			defer teardown()

			test.test(ctx, t, db, mocks, test.emotes...)
		})
	}
}

func testPubsubLayerDeleteEmote(t *testing.T, makeEmoteDBWithPubsub MakeEmoteDBWithMockedPubsub) {
	tests := []struct {
		scenario                string
		emotes                  []mako.Emote
		PubsubError             error
		EnableEmoteUpdatePubsub bool
		test                    func(ctx context.Context, t *testing.T, db mako.EmoteDB, mocks EmoteDBWithPubsubMocksSetup, emotes ...mako.Emote)
	}{
		{
			scenario: "If the dynamic config is not enabled, send no update",
			emotes: []mako.Emote{
				{
					ID:          ksuid.New().String(),
					OwnerID:     ksuid.New().String(),
					GroupID:     ksuid.New().String(),
					Prefix:      ksuid.New().String(),
					Suffix:      ksuid.New().String(),
					Code:        ksuid.New().String(),
					State:       mako.Active,
					Domain:      mako.EmotesDomain,
					EmotesGroup: mk.EmoteGroup_Subscriptions.String(),
				},
			},
			PubsubError:             nil,
			EnableEmoteUpdatePubsub: false,
			test: func(ctx context.Context, t *testing.T, db mako.EmoteDB, mocks EmoteDBWithPubsubMocksSetup, emotes ...mako.Emote) {
				resp, err := db.DeleteEmote(ctx, emotes[0].ID)
				assert.NotEmpty(t, resp)
				assert.Nil(t, err)

				// Wait for async pubsub message to go out
				time.Sleep(1 * time.Second)

				mocks.pubsubMock.AssertNumberOfCalls(t, "UpdateChannelEmoteGroups", 0)
			},
		},
		{
			scenario: "Deleting an active emote from a slot when its the only emote in the group should result in no pubsub update",
			emotes: []mako.Emote{
				{
					ID:          ksuid.New().String(),
					OwnerID:     "SameOwner",
					GroupID:     "SameGroup",
					Prefix:      ksuid.New().String(),
					Suffix:      ksuid.New().String(),
					Code:        ksuid.New().String(),
					State:       mako.Active,
					Domain:      mako.EmotesDomain,
					EmotesGroup: mk.EmoteGroup_Subscriptions.String(),
				},
			},
			PubsubError:             nil,
			EnableEmoteUpdatePubsub: true,
			test: func(ctx context.Context, t *testing.T, db mako.EmoteDB, mocks EmoteDBWithPubsubMocksSetup, emotes ...mako.Emote) {
				resp, err := db.DeleteEmote(ctx, emotes[0].ID)
				assert.NotEmpty(t, resp)
				assert.Nil(t, err)

				// Wait for async pubsub message to go out
				time.Sleep(1 * time.Second)

				mocks.pubsubMock.AssertNumberOfCalls(t, "UpdateChannelEmoteGroups", 0)
			},
		},
		{
			scenario: "Deleting an active emote from a slot when its not the only emote in the group should result in one pubsub update",
			emotes: []mako.Emote{
				{
					ID:          ksuid.New().String(),
					OwnerID:     "SameOwner",
					GroupID:     "SameGroup",
					Prefix:      ksuid.New().String(),
					Suffix:      ksuid.New().String(),
					Code:        ksuid.New().String(),
					State:       mako.Active,
					Domain:      mako.EmotesDomain,
					EmotesGroup: mk.EmoteGroup_Subscriptions.String(),
				},
				{
					ID:          ksuid.New().String(),
					OwnerID:     "SameOwner",
					GroupID:     "SameGroup",
					Prefix:      ksuid.New().String(),
					Suffix:      ksuid.New().String(),
					Code:        ksuid.New().String(),
					State:       mako.Active,
					Domain:      mako.EmotesDomain,
					EmotesGroup: mk.EmoteGroup_Subscriptions.String(),
				},
			},
			PubsubError:             nil,
			EnableEmoteUpdatePubsub: true,
			test: func(ctx context.Context, t *testing.T, db mako.EmoteDB, mocks EmoteDBWithPubsubMocksSetup, emotes ...mako.Emote) {
				resp, err := db.DeleteEmote(ctx, emotes[0].ID)
				assert.NotEmpty(t, resp)
				assert.Nil(t, err)

				// Wait for async pubsub message to go out
				time.Sleep(1 * time.Second)

				mocks.pubsubMock.AssertNumberOfCalls(t, "UpdateChannelEmoteGroups", 1)
				for _, call := range mocks.pubsubMock.Calls {
					ownerID, ok := call.Arguments[1].(string)
					assert.True(t, ok)
					assert.Equal(t, emotes[0].OwnerID, ownerID)

					pubsubEmoteSets, ok := call.Arguments[2].([]pubsub.PubsubEmoteSet)
					assert.True(t, ok)
					assert.Equal(t, 1, len(pubsubEmoteSets))

					emotes := pubsubEmoteSets[0].Emotes
					assert.Equal(t, 1, len(emotes))
				}
			},
		},
	}

	for _, test := range tests {
		test := test

		t.Run(test.scenario, func(t *testing.T) {
			t.Parallel()

			ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
			defer cancel()

			pubsubMock := new(pubsub_mock.IPublisher)
			dynamicConfigMock := new(config_mock.DynamicConfigClient)

			mocks := EmoteDBWithPubsubMocksSetup{
				pubsubMock:        pubsubMock,
				dynamicConfigMock: dynamicConfigMock,
			}

			db, teardown, err := makeEmoteDBWithPubsub(ctx, mocks, test.emotes...)
			if err != nil {
				t.Fatal(err)
			}

			pubsubMock.On("UpdateChannelEmoteGroups", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(test.PubsubError)
			dynamicConfigMock.On("EnableEmoteUpdatePubsub", mock.Anything).Return(test.EnableEmoteUpdatePubsub)
			defer teardown()

			test.test(ctx, t, db, mocks, test.emotes...)
		})
	}
}

func testPubsubLayerUpdateState(t *testing.T, makeEmoteDBWithPubsub MakeEmoteDBWithMockedPubsub) {
	tests := []struct {
		scenario                string
		emotes                  []mako.Emote
		PubsubError             error
		EnableEmoteUpdatePubsub bool
		test                    func(ctx context.Context, t *testing.T, db mako.EmoteDB, mocks EmoteDBWithPubsubMocksSetup, emotes ...mako.Emote)
	}{
		{
			scenario: "If the dynamic config is not enabled, send no update",
			emotes: []mako.Emote{
				{
					ID:          ksuid.New().String(),
					OwnerID:     ksuid.New().String(),
					GroupID:     ksuid.New().String(),
					Prefix:      ksuid.New().String(),
					Suffix:      ksuid.New().String(),
					Code:        ksuid.New().String(),
					State:       mako.Active,
					Domain:      mako.EmotesDomain,
					EmotesGroup: mk.EmoteGroup_Subscriptions.String(),
				},
			},
			PubsubError:             nil,
			EnableEmoteUpdatePubsub: false,
			test: func(ctx context.Context, t *testing.T, db mako.EmoteDB, mocks EmoteDBWithPubsubMocksSetup, emotes ...mako.Emote) {
				resp, err := db.UpdateState(ctx, emotes[0].ID, mako.Inactive)
				assert.NotEmpty(t, resp)
				assert.Nil(t, err)

				// Wait for async pubsub message to go out
				time.Sleep(1 * time.Second)

				mocks.pubsubMock.AssertNumberOfCalls(t, "UpdateChannelEmoteGroups", 0)
			},
		},
		{
			scenario: "Updating an active emote to inactive when its the only emote in the group should result in no pubsub update",
			emotes: []mako.Emote{
				{
					ID:          ksuid.New().String(),
					OwnerID:     "SameOwner",
					GroupID:     "SameGroup",
					Prefix:      ksuid.New().String(),
					Suffix:      ksuid.New().String(),
					Code:        ksuid.New().String(),
					State:       mako.Active,
					Domain:      mako.EmotesDomain,
					EmotesGroup: mk.EmoteGroup_Subscriptions.String(),
				},
			},
			PubsubError:             nil,
			EnableEmoteUpdatePubsub: true,
			test: func(ctx context.Context, t *testing.T, db mako.EmoteDB, mocks EmoteDBWithPubsubMocksSetup, emotes ...mako.Emote) {
				resp, err := db.UpdateState(ctx, emotes[0].ID, mako.Inactive)
				assert.NotEmpty(t, resp)
				assert.Nil(t, err)

				// Wait for async pubsub message to go out
				time.Sleep(1 * time.Second)

				mocks.pubsubMock.AssertNumberOfCalls(t, "UpdateChannelEmoteGroups", 0)
			},
		},
		{
			scenario: "Updating an active emote to inactive when its not the only emote in the group should result in one pubsub update",
			emotes: []mako.Emote{
				{
					ID:          ksuid.New().String(),
					OwnerID:     "SameOwner",
					GroupID:     "SameGroup",
					Prefix:      ksuid.New().String(),
					Suffix:      ksuid.New().String(),
					Code:        ksuid.New().String(),
					State:       mako.Active,
					Domain:      mako.EmotesDomain,
					EmotesGroup: mk.EmoteGroup_Subscriptions.String(),
				},
				{
					ID:          ksuid.New().String(),
					OwnerID:     "SameOwner",
					GroupID:     "SameGroup",
					Prefix:      ksuid.New().String(),
					Suffix:      ksuid.New().String(),
					Code:        ksuid.New().String(),
					State:       mako.Active,
					Domain:      mako.EmotesDomain,
					EmotesGroup: mk.EmoteGroup_Subscriptions.String(),
				},
			},
			PubsubError:             nil,
			EnableEmoteUpdatePubsub: true,
			test: func(ctx context.Context, t *testing.T, db mako.EmoteDB, mocks EmoteDBWithPubsubMocksSetup, emotes ...mako.Emote) {
				resp, err := db.UpdateState(ctx, emotes[0].ID, mako.Inactive)
				assert.NotEmpty(t, resp)
				assert.Nil(t, err)

				// Wait for async pubsub message to go out
				time.Sleep(1 * time.Second)

				mocks.pubsubMock.AssertNumberOfCalls(t, "UpdateChannelEmoteGroups", 1)
				for _, call := range mocks.pubsubMock.Calls {
					ownerID, ok := call.Arguments[1].(string)
					assert.True(t, ok)
					assert.Equal(t, emotes[0].OwnerID, ownerID)

					pubsubEmoteSets, ok := call.Arguments[2].([]pubsub.PubsubEmoteSet)
					assert.True(t, ok)
					assert.Equal(t, 1, len(pubsubEmoteSets))

					emotes := pubsubEmoteSets[0].Emotes
					assert.Equal(t, 1, len(emotes))
				}
			},
		},
	}

	for _, test := range tests {
		test := test

		t.Run(test.scenario, func(t *testing.T) {
			t.Parallel()

			ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
			defer cancel()

			pubsubMock := new(pubsub_mock.IPublisher)
			dynamicConfigMock := new(config_mock.DynamicConfigClient)

			mocks := EmoteDBWithPubsubMocksSetup{
				pubsubMock:        pubsubMock,
				dynamicConfigMock: dynamicConfigMock,
			}

			db, teardown, err := makeEmoteDBWithPubsub(ctx, mocks, test.emotes...)
			if err != nil {
				t.Fatal(err)
			}

			pubsubMock.On("UpdateChannelEmoteGroups", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(test.PubsubError)
			dynamicConfigMock.On("EnableEmoteUpdatePubsub", mock.Anything).Return(test.EnableEmoteUpdatePubsub)
			defer teardown()

			test.test(ctx, t, db, mocks, test.emotes...)
		})
	}
}

func testPubsubLayerUpdateCode(t *testing.T, makeEmoteDBWithPubsub MakeEmoteDBWithMockedPubsub) {
	tests := []struct {
		scenario                string
		emotes                  []mako.Emote
		PubsubError             error
		EnableEmoteUpdatePubsub bool
		test                    func(ctx context.Context, t *testing.T, db mako.EmoteDB, mocks EmoteDBWithPubsubMocksSetup, emotes ...mako.Emote)
	}{
		{
			scenario: "If the dynamic config is not enabled, send no update",
			emotes: []mako.Emote{
				{
					ID:          ksuid.New().String(),
					OwnerID:     ksuid.New().String(),
					GroupID:     ksuid.New().String(),
					Prefix:      ksuid.New().String(),
					Suffix:      ksuid.New().String(),
					Code:        ksuid.New().String(),
					State:       mako.Active,
					Domain:      mako.EmotesDomain,
					EmotesGroup: mk.EmoteGroup_Subscriptions.String(),
				},
			},
			PubsubError:             nil,
			EnableEmoteUpdatePubsub: false,
			test: func(ctx context.Context, t *testing.T, db mako.EmoteDB, mocks EmoteDBWithPubsubMocksSetup, emotes ...mako.Emote) {
				resp, err := db.UpdateCode(ctx, emotes[0].ID, "tooComplex", "Complex")
				assert.NotEmpty(t, resp)
				assert.Nil(t, err)

				// Wait for async pubsub message to go out
				time.Sleep(1 * time.Second)

				mocks.pubsubMock.AssertNumberOfCalls(t, "UpdateChannelEmoteGroups", 0)
			},
		},
		{
			scenario: "Updating an emote code for an archived emote should result in no pubsub update",
			emotes: []mako.Emote{
				{
					ID:          ksuid.New().String(),
					OwnerID:     "SameOwner",
					Prefix:      ksuid.New().String(),
					Suffix:      ksuid.New().String(),
					Code:        ksuid.New().String(),
					State:       mako.Archived,
					Domain:      mako.EmotesDomain,
					EmotesGroup: mk.EmoteGroup_Archive.String(),
				},
			},
			PubsubError:             nil,
			EnableEmoteUpdatePubsub: true,
			test: func(ctx context.Context, t *testing.T, db mako.EmoteDB, mocks EmoteDBWithPubsubMocksSetup, emotes ...mako.Emote) {
				resp, err := db.UpdateCode(ctx, emotes[0].ID, "tooComplex", "Complex")
				assert.NotEmpty(t, resp)
				assert.Nil(t, err)

				// Wait for async pubsub message to go out
				time.Sleep(1 * time.Second)

				mocks.pubsubMock.AssertNumberOfCalls(t, "UpdateChannelEmoteGroups", 0)
			},
		},
		{
			scenario: "Updating an emote code for an active emote in a slot should result in one pubsub update",
			emotes: []mako.Emote{
				{
					ID:          ksuid.New().String(),
					OwnerID:     "SameOwner",
					GroupID:     "SameGroup",
					Prefix:      ksuid.New().String(),
					Suffix:      ksuid.New().String(),
					Code:        ksuid.New().String(),
					State:       mako.Active,
					Domain:      mako.EmotesDomain,
					EmotesGroup: mk.EmoteGroup_Subscriptions.String(),
				},
				{
					ID:          ksuid.New().String(),
					OwnerID:     "SameOwner",
					GroupID:     "SameGroup",
					Prefix:      ksuid.New().String(),
					Suffix:      ksuid.New().String(),
					Code:        ksuid.New().String(),
					State:       mako.Active,
					Domain:      mako.EmotesDomain,
					EmotesGroup: mk.EmoteGroup_Subscriptions.String(),
				},
			},
			PubsubError:             nil,
			EnableEmoteUpdatePubsub: true,
			test: func(ctx context.Context, t *testing.T, db mako.EmoteDB, mocks EmoteDBWithPubsubMocksSetup, emotes ...mako.Emote) {
				resp, err := db.UpdateCode(ctx, emotes[0].ID, "tooComplex", "Complex")
				assert.NotEmpty(t, resp)
				assert.Nil(t, err)

				// Wait for async pubsub message to go out
				time.Sleep(1 * time.Second)

				mocks.pubsubMock.AssertNumberOfCalls(t, "UpdateChannelEmoteGroups", 1)
				for _, call := range mocks.pubsubMock.Calls {
					ownerID, ok := call.Arguments[1].(string)
					assert.True(t, ok)
					assert.Equal(t, emotes[0].OwnerID, ownerID)

					pubsubEmoteSets, ok := call.Arguments[2].([]pubsub.PubsubEmoteSet)
					assert.True(t, ok)
					assert.Equal(t, 1, len(pubsubEmoteSets))

					emotes := pubsubEmoteSets[0].Emotes
					assert.Equal(t, 2, len(emotes))
				}
			},
		},
	}

	for _, test := range tests {
		test := test

		t.Run(test.scenario, func(t *testing.T) {
			t.Parallel()

			ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
			defer cancel()

			pubsubMock := new(pubsub_mock.IPublisher)
			dynamicConfigMock := new(config_mock.DynamicConfigClient)

			mocks := EmoteDBWithPubsubMocksSetup{
				pubsubMock:        pubsubMock,
				dynamicConfigMock: dynamicConfigMock,
			}

			db, teardown, err := makeEmoteDBWithPubsub(ctx, mocks, test.emotes...)
			if err != nil {
				t.Fatal(err)
			}

			pubsubMock.On("UpdateChannelEmoteGroups", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(test.PubsubError)
			dynamicConfigMock.On("EnableEmoteUpdatePubsub", mock.Anything).Return(test.EnableEmoteUpdatePubsub)
			defer teardown()

			test.test(ctx, t, db, mocks, test.emotes...)
		})
	}
}

func testPubsubLayerUpdatePrefix(t *testing.T, makeEmoteDBWithPubsub MakeEmoteDBWithMockedPubsub) {
	tests := []struct {
		scenario                string
		emotes                  []mako.Emote
		PubsubError             error
		EnableEmoteUpdatePubsub bool
		test                    func(ctx context.Context, t *testing.T, db mako.EmoteDB, mocks EmoteDBWithPubsubMocksSetup, emotes ...mako.Emote)
	}{
		{
			scenario: "If the dynamic config is not enabled, send no update",
			emotes: []mako.Emote{
				{
					ID:          ksuid.New().String(),
					OwnerID:     ksuid.New().String(),
					GroupID:     ksuid.New().String(),
					Prefix:      ksuid.New().String(),
					Suffix:      ksuid.New().String(),
					Code:        ksuid.New().String(),
					State:       mako.Active,
					Domain:      mako.EmotesDomain,
					EmotesGroup: mk.EmoteGroup_Subscriptions.String(),
				},
			},
			PubsubError:             nil,
			EnableEmoteUpdatePubsub: false,
			test: func(ctx context.Context, t *testing.T, db mako.EmoteDB, mocks EmoteDBWithPubsubMocksSetup, emotes ...mako.Emote) {
				resp, err := db.UpdatePrefix(ctx, emotes[0].ID, "tooComplex", "foo")
				assert.NotEmpty(t, resp)
				assert.Nil(t, err)

				// Wait for async pubsub message to go out
				time.Sleep(1 * time.Second)

				mocks.pubsubMock.AssertNumberOfCalls(t, "UpdateChannelEmoteGroups", 0)
			},
		},
		{
			scenario: "Updating an emote prefix for an archived emote should result in no pubsub update",
			emotes: []mako.Emote{
				{
					ID:          ksuid.New().String(),
					OwnerID:     "SameOwner",
					Prefix:      ksuid.New().String(),
					Suffix:      ksuid.New().String(),
					Code:        ksuid.New().String(),
					State:       mako.Archived,
					Domain:      mako.EmotesDomain,
					EmotesGroup: mk.EmoteGroup_Archive.String(),
				},
			},
			PubsubError:             nil,
			EnableEmoteUpdatePubsub: true,
			test: func(ctx context.Context, t *testing.T, db mako.EmoteDB, mocks EmoteDBWithPubsubMocksSetup, emotes ...mako.Emote) {
				resp, err := db.UpdatePrefix(ctx, emotes[0].ID, "tooComplex", "foo")
				assert.NotEmpty(t, resp)
				assert.Nil(t, err)

				// Wait for async pubsub message to go out
				time.Sleep(1 * time.Second)

				mocks.pubsubMock.AssertNumberOfCalls(t, "UpdateChannelEmoteGroups", 0)
			},
		},
		{
			scenario: "Updating an emote code for an active emote in a slot should result in one pubsub update",
			emotes: []mako.Emote{
				{
					ID:          ksuid.New().String(),
					OwnerID:     "SameOwner",
					GroupID:     "SameGroup",
					Prefix:      "too",
					Suffix:      "Complex",
					Code:        "tooComplex",
					State:       mako.Active,
					Domain:      mako.EmotesDomain,
					EmotesGroup: mk.EmoteGroup_Subscriptions.String(),
				},
				{
					ID:          ksuid.New().String(),
					OwnerID:     "SameOwner",
					GroupID:     "SameGroup",
					Prefix:      ksuid.New().String(),
					Suffix:      ksuid.New().String(),
					Code:        ksuid.New().String(),
					State:       mako.Active,
					Domain:      mako.EmotesDomain,
					EmotesGroup: mk.EmoteGroup_Subscriptions.String(),
				},
			},
			PubsubError:             nil,
			EnableEmoteUpdatePubsub: true,
			test: func(ctx context.Context, t *testing.T, db mako.EmoteDB, mocks EmoteDBWithPubsubMocksSetup, emotes ...mako.Emote) {
				resp, err := db.UpdatePrefix(ctx, emotes[0].ID, "tooComplex", "foo")
				assert.NotEmpty(t, resp)
				assert.Nil(t, err)

				// Wait for async pubsub message to go out
				time.Sleep(1 * time.Second)

				mocks.pubsubMock.AssertNumberOfCalls(t, "UpdateChannelEmoteGroups", 1)
				for _, call := range mocks.pubsubMock.Calls {
					ownerID, ok := call.Arguments[1].(string)
					assert.True(t, ok)
					assert.Equal(t, emotes[0].OwnerID, ownerID)

					pubsubEmoteSets, ok := call.Arguments[2].([]pubsub.PubsubEmoteSet)
					assert.True(t, ok)
					assert.Equal(t, 1, len(pubsubEmoteSets))

					emotes := pubsubEmoteSets[0].Emotes
					assert.Equal(t, 2, len(emotes))
				}
			},
		},
	}

	for _, test := range tests {
		test := test

		t.Run(test.scenario, func(t *testing.T) {
			t.Parallel()

			ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
			defer cancel()

			pubsubMock := new(pubsub_mock.IPublisher)
			dynamicConfigMock := new(config_mock.DynamicConfigClient)

			mocks := EmoteDBWithPubsubMocksSetup{
				pubsubMock:        pubsubMock,
				dynamicConfigMock: dynamicConfigMock,
			}

			db, teardown, err := makeEmoteDBWithPubsub(ctx, mocks, test.emotes...)
			if err != nil {
				t.Fatal(err)
			}

			pubsubMock.On("UpdateChannelEmoteGroups", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(test.PubsubError)
			dynamicConfigMock.On("EnableEmoteUpdatePubsub", mock.Anything).Return(test.EnableEmoteUpdatePubsub)
			defer teardown()

			test.test(ctx, t, db, mocks, test.emotes...)
		})
	}
}

func testPubsubLayerUpdateEmote(t *testing.T, makeEmoteDBWithPubsub MakeEmoteDBWithMockedPubsub) {
	tests := []struct {
		scenario                string
		emotes                  []mako.Emote
		PubsubError             error
		EnableEmoteUpdatePubsub bool
		test                    func(ctx context.Context, t *testing.T, db mako.EmoteDB, mocks EmoteDBWithPubsubMocksSetup, emotes ...mako.Emote)
	}{
		{
			scenario: "If the dynamic config is not enabled, send no update",
			emotes: []mako.Emote{
				{
					ID:          ksuid.New().String(),
					OwnerID:     ksuid.New().String(),
					GroupID:     ksuid.New().String(),
					Prefix:      ksuid.New().String(),
					Suffix:      ksuid.New().String(),
					Code:        ksuid.New().String(),
					State:       mako.Active,
					Domain:      mako.EmotesDomain,
					EmotesGroup: mk.EmoteGroup_Subscriptions.String(),
				},
			},
			PubsubError:             nil,
			EnableEmoteUpdatePubsub: false,
			test: func(ctx context.Context, t *testing.T, db mako.EmoteDB, mocks EmoteDBWithPubsubMocksSetup, emotes ...mako.Emote) {
				newGroupID := ksuid.New().String()
				resp, err := db.UpdateEmote(ctx, emotes[0].ID, mako.EmoteUpdate{
					GroupID: &newGroupID,
				})
				assert.NotEmpty(t, resp)
				assert.Nil(t, err)

				// Wait for async pubsub message to go out
				time.Sleep(1 * time.Second)

				mocks.pubsubMock.AssertNumberOfCalls(t, "UpdateChannelEmoteGroups", 0)
			},
		},
		{
			scenario: "Updating something besides groupID should result in one pubsub update",
			emotes: []mako.Emote{
				{
					ID:          ksuid.New().String(),
					OwnerID:     ksuid.New().String(),
					GroupID:     ksuid.New().String(),
					Prefix:      ksuid.New().String(),
					Suffix:      ksuid.New().String(),
					Code:        ksuid.New().String(),
					State:       mako.Pending,
					Domain:      mako.EmotesDomain,
					EmotesGroup: mk.EmoteGroup_Subscriptions.String(),
				},
			},
			PubsubError:             nil,
			EnableEmoteUpdatePubsub: true,
			test: func(ctx context.Context, t *testing.T, db mako.EmoteDB, mocks EmoteDBWithPubsubMocksSetup, emotes ...mako.Emote) {
				newState := mako.Active
				resp, err := db.UpdateEmote(ctx, emotes[0].ID, mako.EmoteUpdate{
					State: &newState,
				})
				assert.NotEmpty(t, resp)
				assert.Nil(t, err)

				// Wait for async pubsub message to go out
				time.Sleep(1 * time.Second)

				mocks.pubsubMock.AssertNumberOfCalls(t, "UpdateChannelEmoteGroups", 1)
			},
		},
		{
			scenario: "Changing one emote from an existing group to another existing group should result in a pubsub update with two emoticonGroups",
			emotes: []mako.Emote{
				{
					ID:          ksuid.New().String(),
					OwnerID:     "SameOwner",
					GroupID:     "1",
					Prefix:      ksuid.New().String(),
					Suffix:      ksuid.New().String(),
					Code:        ksuid.New().String(),
					State:       mako.Active,
					Domain:      mako.EmotesDomain,
					EmotesGroup: mk.EmoteGroup_Subscriptions.String(),
					Order:       0,
				},
				{
					ID:          ksuid.New().String(),
					OwnerID:     "SameOwner",
					GroupID:     "1",
					Prefix:      ksuid.New().String(),
					Suffix:      ksuid.New().String(),
					Code:        ksuid.New().String(),
					State:       mako.Active,
					Domain:      mako.EmotesDomain,
					EmotesGroup: mk.EmoteGroup_Subscriptions.String(),
					Order:       1,
				},
				{
					ID:          ksuid.New().String(),
					OwnerID:     "SameOwner",
					GroupID:     "2",
					Prefix:      ksuid.New().String(),
					Suffix:      ksuid.New().String(),
					Code:        ksuid.New().String(),
					State:       mako.Active,
					Domain:      mako.EmotesDomain,
					EmotesGroup: mk.EmoteGroup_Subscriptions.String(),
					Order:       0,
				},
			},
			PubsubError:             nil,
			EnableEmoteUpdatePubsub: true,
			test: func(ctx context.Context, t *testing.T, db mako.EmoteDB, mocks EmoteDBWithPubsubMocksSetup, emotes ...mako.Emote) {
				newOrder := emotes[2].Order + 1
				newGroupID := emotes[2].GroupID
				resp, err := db.UpdateEmote(ctx, emotes[0].ID, mako.EmoteUpdate{
					GroupID: &newGroupID,
					Order:   &newOrder,
				})
				assert.NotEmpty(t, resp)
				assert.Nil(t, err)

				// Wait for async pubsub message to go out
				time.Sleep(1 * time.Second)

				mocks.pubsubMock.AssertNumberOfCalls(t, "UpdateChannelEmoteGroups", 1)
				for _, call := range mocks.pubsubMock.Calls {
					ownerID, ok := call.Arguments[1].(string)
					assert.True(t, ok)
					assert.Equal(t, emotes[0].OwnerID, ownerID)

					pubsubEmoteSets, ok := call.Arguments[2].([]pubsub.PubsubEmoteSet)
					assert.True(t, ok)
					assert.Equal(t, 2, len(pubsubEmoteSets))

					for _, pubsubEmoteSet := range pubsubEmoteSets {
						if pubsubEmoteSet.SetID == emotes[1].GroupID {
							// Group 1 should have 1 emote now
							assert.Equal(t, 1, len(pubsubEmoteSet.Emotes))
						} else {
							// Group 2 should have 2 emotes now
							assert.Equal(t, 2, len(pubsubEmoteSet.Emotes))
						}
					}
				}
			},
		},
		{
			scenario: "Changing one emote from archived to an existing group should result in one pubsub update with the new emoticonGroup",
			emotes: []mako.Emote{
				{
					ID:          ksuid.New().String(),
					OwnerID:     "SameOwner",
					GroupID:     "",
					Prefix:      ksuid.New().String(),
					Suffix:      ksuid.New().String(),
					Code:        ksuid.New().String(),
					State:       mako.Archived,
					Domain:      mako.EmotesDomain,
					EmotesGroup: mk.EmoteGroup_Archive.String(),
				},
				{
					ID:          ksuid.New().String(),
					OwnerID:     "SameOwner",
					GroupID:     ksuid.New().String(),
					Prefix:      ksuid.New().String(),
					Suffix:      ksuid.New().String(),
					Code:        ksuid.New().String(),
					State:       mako.Active,
					Domain:      mako.EmotesDomain,
					EmotesGroup: mk.EmoteGroup_Follower.String(),
					Order:       0,
				},
			},
			PubsubError:             nil,
			EnableEmoteUpdatePubsub: true,
			test: func(ctx context.Context, t *testing.T, db mako.EmoteDB, mocks EmoteDBWithPubsubMocksSetup, emotes ...mako.Emote) {
				newOrder := emotes[1].Order + 1
				newGroupID := emotes[1].GroupID
				newState := mako.Active
				resp, err := db.UpdateEmote(ctx, emotes[0].ID, mako.EmoteUpdate{
					GroupID: &newGroupID,
					Order:   &newOrder,
					State:   &newState,
				})
				assert.NotEmpty(t, resp)
				assert.Nil(t, err)

				// Wait for async pubsub message to go out
				time.Sleep(1 * time.Second)

				mocks.pubsubMock.AssertNumberOfCalls(t, "UpdateChannelEmoteGroups", 1)
				for _, call := range mocks.pubsubMock.Calls {
					ownerID, ok := call.Arguments[1].(string)
					assert.True(t, ok)
					assert.Equal(t, emotes[0].OwnerID, ownerID)

					pubsubEmoteSets, ok := call.Arguments[2].([]pubsub.PubsubEmoteSet)
					assert.True(t, ok)
					assert.Equal(t, 1, len(pubsubEmoteSets))

					for _, pubsubEmoteSet := range pubsubEmoteSets {
						// Group should now have 2 emotes
						assert.Equal(t, 2, len(pubsubEmoteSet.Emotes))
					}
				}
			},
		},
		{
			scenario: "Changing an emote from an existing group to archived should result in one pubsub update with the previous emoticonGroup",
			emotes: []mako.Emote{
				{
					ID:          ksuid.New().String(),
					OwnerID:     "SameOwner",
					GroupID:     "SameGroup",
					Prefix:      ksuid.New().String(),
					Suffix:      ksuid.New().String(),
					Code:        ksuid.New().String(),
					State:       mako.Active,
					Domain:      mako.EmotesDomain,
					EmotesGroup: mk.EmoteGroup_Follower.String(),
					Order:       1,
				},
				{
					ID:          ksuid.New().String(),
					OwnerID:     "SameOwner",
					GroupID:     "SameGroup",
					Prefix:      ksuid.New().String(),
					Suffix:      ksuid.New().String(),
					Code:        ksuid.New().String(),
					State:       mako.Active,
					Domain:      mako.EmotesDomain,
					EmotesGroup: mk.EmoteGroup_Follower.String(),
					Order:       0,
				},
			},
			PubsubError:             nil,
			EnableEmoteUpdatePubsub: true,
			test: func(ctx context.Context, t *testing.T, db mako.EmoteDB, mocks EmoteDBWithPubsubMocksSetup, emotes ...mako.Emote) {
				newOrder := int64(0)
				newGroupID := ""
				newState := mako.Archived
				resp, err := db.UpdateEmote(ctx, emotes[0].ID, mako.EmoteUpdate{
					GroupID: &newGroupID,
					Order:   &newOrder,
					State:   &newState,
				})
				assert.NotEmpty(t, resp)
				assert.Nil(t, err)

				// Wait for async pubsub message to go out
				time.Sleep(1 * time.Second)

				mocks.pubsubMock.AssertNumberOfCalls(t, "UpdateChannelEmoteGroups", 1)
				for _, call := range mocks.pubsubMock.Calls {
					ownerID, ok := call.Arguments[1].(string)
					assert.True(t, ok)
					assert.Equal(t, emotes[0].OwnerID, ownerID)

					pubsubEmoteSets, ok := call.Arguments[2].([]pubsub.PubsubEmoteSet)
					assert.True(t, ok)
					assert.Equal(t, 1, len(pubsubEmoteSets))

					for _, pubsubEmoteSet := range pubsubEmoteSets {
						// Group should now have 1 emote
						assert.Equal(t, 1, len(pubsubEmoteSet.Emotes))
					}
				}
			},
		},
	}

	for _, test := range tests {
		test := test

		t.Run(test.scenario, func(t *testing.T) {
			t.Parallel()

			ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
			defer cancel()

			pubsubMock := new(pubsub_mock.IPublisher)
			dynamicConfigMock := new(config_mock.DynamicConfigClient)

			mocks := EmoteDBWithPubsubMocksSetup{
				pubsubMock:        pubsubMock,
				dynamicConfigMock: dynamicConfigMock,
			}

			db, teardown, err := makeEmoteDBWithPubsub(ctx, mocks, test.emotes...)
			if err != nil {
				t.Fatal(err)
			}

			pubsubMock.On("UpdateChannelEmoteGroups", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(test.PubsubError)
			dynamicConfigMock.On("EnableEmoteUpdatePubsub", mock.Anything).Return(test.EnableEmoteUpdatePubsub)
			defer teardown()

			test.test(ctx, t, db, mocks, test.emotes...)
		})
	}
}

func testPubsubLayerUpdateEmotes(t *testing.T, makeEmoteDBWithPubsub MakeEmoteDBWithMockedPubsub) {
	tests := []struct {
		scenario                string
		emotes                  []mako.Emote
		PubsubError             error
		EnableEmoteUpdatePubsub bool
		test                    func(ctx context.Context, t *testing.T, db mako.EmoteDB, mocks EmoteDBWithPubsubMocksSetup, emotes ...mako.Emote)
	}{
		{
			scenario: "If the dynamic config is not enabled, send no update",
			emotes: []mako.Emote{
				{
					ID:          ksuid.New().String(),
					OwnerID:     ksuid.New().String(),
					GroupID:     ksuid.New().String(),
					Prefix:      ksuid.New().String(),
					Suffix:      ksuid.New().String(),
					Code:        ksuid.New().String(),
					State:       mako.Active,
					Domain:      mako.EmotesDomain,
					EmotesGroup: mk.EmoteGroup_Subscriptions.String(),
				},
			},
			PubsubError:             nil,
			EnableEmoteUpdatePubsub: false,
			test: func(ctx context.Context, t *testing.T, db mako.EmoteDB, mocks EmoteDBWithPubsubMocksSetup, emotes ...mako.Emote) {
				newGroupID := ksuid.New().String()
				resp, err := db.UpdateEmotes(ctx, map[string]mako.EmoteUpdate{
					emotes[0].ID: {
						GroupID: &newGroupID,
					},
				})
				assert.NotEmpty(t, resp)
				assert.Nil(t, err)

				// Wait for async pubsub message to go out
				time.Sleep(1 * time.Second)

				mocks.pubsubMock.AssertNumberOfCalls(t, "UpdateChannelEmoteGroups", 0)
			},
		},
		{
			scenario: "Updating multiple emotes' orders in the same group should result in one pubsub update",
			emotes: []mako.Emote{
				{
					ID:          ksuid.New().String(),
					OwnerID:     "SameOwner",
					GroupID:     "SameGroup",
					Prefix:      ksuid.New().String(),
					Suffix:      ksuid.New().String(),
					Code:        ksuid.New().String(),
					State:       mako.Active,
					Domain:      mako.EmotesDomain,
					EmotesGroup: mk.EmoteGroup_Subscriptions.String(),
				},
				{
					ID:          ksuid.New().String(),
					OwnerID:     "SameOwner",
					GroupID:     "SameGroup",
					Prefix:      ksuid.New().String(),
					Suffix:      ksuid.New().String(),
					Code:        ksuid.New().String(),
					State:       mako.Active,
					Domain:      mako.EmotesDomain,
					EmotesGroup: mk.EmoteGroup_Subscriptions.String(),
					Order:       1,
				},
				{
					ID:          ksuid.New().String(),
					OwnerID:     "SameOwner",
					GroupID:     "SameGroup",
					Prefix:      ksuid.New().String(),
					Suffix:      ksuid.New().String(),
					Code:        ksuid.New().String(),
					State:       mako.Active,
					Domain:      mako.EmotesDomain,
					EmotesGroup: mk.EmoteGroup_Subscriptions.String(),
					Order:       2,
				},
				{
					ID:          ksuid.New().String(),
					OwnerID:     "SameOwner",
					GroupID:     "SameGroup",
					Prefix:      ksuid.New().String(),
					Suffix:      ksuid.New().String(),
					Code:        ksuid.New().String(),
					State:       mako.Active,
					Domain:      mako.EmotesDomain,
					EmotesGroup: mk.EmoteGroup_Subscriptions.String(),
					Order:       3,
				},
				{
					ID:          ksuid.New().String(),
					OwnerID:     "SameOwner",
					GroupID:     "SameGroup",
					Prefix:      ksuid.New().String(),
					Suffix:      ksuid.New().String(),
					Code:        ksuid.New().String(),
					State:       mako.Active,
					Domain:      mako.EmotesDomain,
					EmotesGroup: mk.EmoteGroup_Subscriptions.String(),
					Order:       4,
				},
			},
			PubsubError:             nil,
			EnableEmoteUpdatePubsub: true,
			test: func(ctx context.Context, t *testing.T, db mako.EmoteDB, mocks EmoteDBWithPubsubMocksSetup, emotes ...mako.Emote) {
				newOrderEmote0 := int64(4)
				newOrderEmote1 := int64(0)
				newOrderEmote2 := int64(1)
				newOrderEmote3 := int64(2)
				newOrderEmote4 := int64(3)
				resp, err := db.UpdateEmotes(ctx, map[string]mako.EmoteUpdate{
					emotes[0].ID: {
						Order: &newOrderEmote0,
					},
					emotes[1].ID: {
						Order: &newOrderEmote1,
					},
					emotes[2].ID: {
						Order: &newOrderEmote2,
					},
					emotes[3].ID: {
						Order: &newOrderEmote3,
					},
					emotes[4].ID: {
						Order: &newOrderEmote4,
					},
				})
				assert.NotEmpty(t, resp)
				assert.Nil(t, err)

				// Wait for async pubsub message to go out
				time.Sleep(1 * time.Second)

				mocks.pubsubMock.AssertNumberOfCalls(t, "UpdateChannelEmoteGroups", 1)
				for _, call := range mocks.pubsubMock.Calls {
					ownerID, ok := call.Arguments[1].(string)
					assert.True(t, ok)
					assert.Equal(t, emotes[0].OwnerID, ownerID)

					pubsubEmoteSets, ok := call.Arguments[2].([]pubsub.PubsubEmoteSet)
					assert.True(t, ok)
					assert.Equal(t, 1, len(pubsubEmoteSets))

					emotes := pubsubEmoteSets[0].Emotes
					assert.Equal(t, 5, len(emotes))
				}
			},
		},
	}

	for _, test := range tests {
		test := test

		t.Run(test.scenario, func(t *testing.T) {
			t.Parallel()

			ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
			defer cancel()

			pubsubMock := new(pubsub_mock.IPublisher)
			dynamicConfigMock := new(config_mock.DynamicConfigClient)

			mocks := EmoteDBWithPubsubMocksSetup{
				pubsubMock:        pubsubMock,
				dynamicConfigMock: dynamicConfigMock,
			}

			db, teardown, err := makeEmoteDBWithPubsub(ctx, mocks, test.emotes...)
			if err != nil {
				t.Fatal(err)
			}

			pubsubMock.On("UpdateChannelEmoteGroups", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(test.PubsubError)
			dynamicConfigMock.On("EnableEmoteUpdatePubsub", mock.Anything).Return(test.EnableEmoteUpdatePubsub)
			defer teardown()

			test.test(ctx, t, db, mocks, test.emotes...)
		})
	}
}
