package imageuploader

import (
	"context"
	"fmt"
	"net/http"

	"code.justin.tv/web/upload-service/rpc/uploader"
	"github.com/pkg/errors"
)

// IImageUploader is the wrapper around the general upload service
type IImageUploader interface {
	// New method for handling emote uploads.
	CreateEmoteUploadRequest(ctx context.Context, params EmoteUploadParams) (*uploader.UploadResponse, error)
}

type ImageUploaderImpl struct {
	Uploader uploader.Uploader
	Config   ImageUploaderConfig
}

// ImageUploaderConfig are the config values needed to upload an image using the generic upload service
type ImageUploaderConfig struct {
	UploadServiceURL       string
	UploadCallbackSNSTopic string
	UploadS3BucketName     string
}

// NewImageUploader returns a new image uploader or errors if missing required config
func NewImageUploader(config ImageUploaderConfig, client *http.Client) (IImageUploader, error) {
	if config.UploadServiceURL == "" {
		return nil, errors.New("no upload service url")
	}

	if config.UploadS3BucketName == "" {
		return nil, errors.New("no s3 bucket provided")
	}

	if config.UploadCallbackSNSTopic == "" {
		return nil, errors.New("no sns callback")
	}

	return &ImageUploaderImpl{
		Uploader: uploader.NewUploaderProtobufClient(config.UploadServiceURL, client),
		Config:   config,
	}, nil
}

func (u *ImageUploaderImpl) CreateEmoteUploadRequest(ctx context.Context, params EmoteUploadParams) (*uploader.UploadResponse, error) {
	var validation *uploader.Validation
	var outputs []*uploader.Output

	if params.Size == SizeOriginalString { // if they're passing an original size image, it indicates they want to resize
		validation = getValidationForResizing(params.IsAnimated)
		outputs = getOutputsForResizing(params.UserID, params.IsAnimated, false)

		// If static assets are to be generated from animated assets, then also fetch the output for the static assets
		if params.IsAnimated && params.GenerateStaticVersionOfAnimatedAsset {
			outputs = append(outputs, getOutputsForResizing(params.UserID, false, true)...)
		}
	} else {
		validation = getValidationForEmoteSize(params.Size, params.IsAnimated)
		outputs = []*uploader.Output{getSingleOutput(params.UserID, params.Size, params.IsAnimated, false)}

		// If static assets are to be generated from animated assets, then also fetch the output for the static assets
		if params.IsAnimated && params.GenerateStaticVersionOfAnimatedAsset {
			outputs = append(outputs, getSingleOutput(params.UserID, params.Size, false, true))
		}
	}

	resp, err := u.Uploader.Create(ctx, &uploader.UploadRequest{
		PreValidation: validation,
		Outputs:       outputs,
		OutputPrefix:  fmt.Sprintf("s3://%s/emoticons/", u.Config.UploadS3BucketName),
		Callback: &uploader.Callback{
			SnsTopicArn: u.Config.UploadCallbackSNSTopic,
			PubsubTopic: fmt.Sprintf("emote-uploads.%s", params.UserID),
		},
	})

	if err != nil {
		return nil, err
	}

	return resp, nil
}

func getValidationForEmoteSize(emoteSize string, isAnimated bool) *uploader.Validation {
	size := GetDimensionsForEmoteSize(emoteSize)
	return &uploader.Validation{
		AspectRatio:      RequiredRatio,
		FileSizeLessThan: getMaxFileSizeForAssetType(isAnimated),
		Format:           getRequiredFormatForAssetType(isAnimated),
		MinimumSize: &uploader.Dimensions{
			Width:  size,
			Height: size,
		},
		MaximumSize: &uploader.Dimensions{
			Width:  size,
			Height: size,
		},
	}
}

func getValidationForResizing(isAnimated bool) *uploader.Validation {
	return &uploader.Validation{
		AspectRatio:      RequiredRatio,
		FileSizeLessThan: MaxAutoResizeFileSize,
		Format:           getRequiredFormatForAssetType(isAnimated),
		MinimumSize: &uploader.Dimensions{
			Width:  Size4xDimension,
			Height: Size4xDimension,
		},
		MaximumSize: &uploader.Dimensions{
			Width:  MaxResolution,
			Height: MaxResolution,
		},
	}
}

func getRequiredFormatForAssetType(isAnimated bool) string {
	if isAnimated {
		return RequiredAnimatedFormat
	}

	return RequiredStaticFormat
}

func getMaxFileSizeForAssetType(isAnimated bool) string {
	if isAnimated {
		return MaxAnimatedFileSize
	}

	return MaxStaticFileSize
}

func getOutputsForResizing(userID string, isAnimated bool, shouldTranscodeToPNG bool) []*uploader.Output {
	return []*uploader.Output{
		getSingleOutput(userID, SizeOriginalString, isAnimated, shouldTranscodeToPNG),
		getSingleResizedOutput(userID, Size4xString, isAnimated, shouldTranscodeToPNG),
		getSingleResizedOutput(userID, Size2xString, isAnimated, shouldTranscodeToPNG),
		getSingleResizedOutput(userID, Size1xString, isAnimated, shouldTranscodeToPNG),
	}
}

func getSingleOutput(userID, emoteSize string, isAnimated bool, shouldTranscodeToPNG bool) *uploader.Output {
	var transformations []*uploader.Transformation

	if shouldTranscodeToPNG {
		// Re-encode the image as PNG.
		// This can be used when extracting the first frame of an animated GIF. Otherwise, the output remains a GIF.
		transformations = append(transformations, &uploader.Transformation{
			Transformation: &uploader.Transformation_Transcode{
				Transcode: &uploader.Transcode{
					Format: RequiredStaticFormat,
				},
			},
		})
	}

	return &uploader.Output{
		Transformations: transformations,
		Name:            getImageID(userID, emoteSize, isAnimated),
		Permissions: &uploader.Permissions{
			GrantRead: "uri=http://acs.amazonaws.com/groups/global/AllUsers",
		},
		AllowAnimation: isAnimated,
		PostValidation: &uploader.Validation{
			MaximumFrames: GetFrameLimitForEmote(isAnimated),
		},
	}
}

func getSingleResizedOutput(userID, emoteSize string, isAnimated bool, shouldTranscodeToPNG bool) *uploader.Output {
	size := GetDimensionsForEmoteSize(emoteSize)

	transformations := []*uploader.Transformation{
		{
			Transformation: &uploader.Transformation_Resize{
				Resize: &uploader.Resize{
					Size: &uploader.Resize_Dimensions{
						Dimensions: &uploader.Dimensions{
							Width:  size,
							Height: size,
						},
					},
				},
			},
		},
	}

	if shouldTranscodeToPNG {
		// Re-encode the image as PNG.
		// This can be used when extracting the first frame of an animated GIF. Otherwise, the output remains a GIF.
		transformations = append(transformations, &uploader.Transformation{
			Transformation: &uploader.Transformation_Transcode{
				Transcode: &uploader.Transcode{
					Format: RequiredStaticFormat,
				},
			},
		})
	}

	return &uploader.Output{
		Transformations: transformations,
		Name:            getImageID(userID, emoteSize, isAnimated),
		Permissions: &uploader.Permissions{
			GrantRead: "uri=http://acs.amazonaws.com/groups/global/AllUsers",
		},
		AllowAnimation: isAnimated,
		PostValidation: &uploader.Validation{
			MaximumFrames: GetFrameLimitForEmote(isAnimated),
		},
	}
}

func getImageID(userID, emoteSize string, isAnimated bool) string {
	format := ImageIDStaticFormat
	if isAnimated {
		format = ImageIDAnimatedFormat
	}

	return fmt.Sprintf(format, userID, emoteSize, "{{UploadID}}")
}
