package imageuploader

import (
	log "code.justin.tv/commerce/logrus"
	mkd "code.justin.tv/commerce/mako/twirp/dashboard"
)

const (
	ImageIDStaticFormat   = "%s_%s_%s"
	ImageIDAnimatedFormat = ImageIDStaticFormat + "_animated"
	ImageS3URLFormat      = "%s/%s/emoticons/%s"

	Size4xString       = "4x"
	Size4xDimension    = uint32(112)
	Size2xString       = "2x"
	Size2xDimension    = uint32(56)
	Size1xString       = "1x"
	Size1xDimension    = uint32(28)
	SizeOriginalString = "original"

	// Resizing requirements
	RequiredStaticFormat   = "png"
	RequiredAnimatedFormat = "gif"
	MaxAutoResizeFileSize  = "1MB"
	MaxStaticFileSize      = "100KB"
	MaxAnimatedFileSize    = "512KB"
	MaxStaticFrames        = 1
	MaxAnimatedFrames      = 60
	MaxResolution          = 4096
	RequiredRatio          = float64(1)
)

type EmoteUploadParams struct {
	UserID                               string
	Size                                 string
	IsAnimated                           bool
	GenerateStaticVersionOfAnimatedAsset bool
}

func GetDimensionsForEmoteSize(emoteSize string) uint32 {
	switch emoteSize {
	case Size1xString:
		return Size1xDimension
	case Size2xString:
		return Size2xDimension
	case Size4xString:
		return Size4xDimension
	default:
		return 0 // original or unknown
	}
}

func GetFrameLimitForEmote(isAnimated bool) int32 {
	if isAnimated {
		return MaxAnimatedFrames
	}
	return MaxStaticFrames
}

func StringifySize(size mkd.EmoteImageSize) string {
	switch size {
	case mkd.EmoteImageSize_size_original:
		return SizeOriginalString
	case mkd.EmoteImageSize_size_1x:
		return Size1xString
	case mkd.EmoteImageSize_size_2x:
		return Size2xString
	case mkd.EmoteImageSize_size_4x:
		return Size4xString
	}

	log.WithField("size", size).Error("unknown image size encountered while converting twirp enum into string.")
	return "unknown"
}

func StringSizeToTwirp(size string) mkd.EmoteImageSize {
	switch size {
	case SizeOriginalString:
		return mkd.EmoteImageSize_size_original
	case Size1xString:
		return mkd.EmoteImageSize_size_1x
	case Size2xString:
		return mkd.EmoteImageSize_size_2x
	case Size4xString:
		return mkd.EmoteImageSize_size_4x
	}

	log.WithField("size", size).Error("unknown image size encountered while converting string into twirp enum.")
	return -1
}
