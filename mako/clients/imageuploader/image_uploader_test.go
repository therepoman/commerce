package imageuploader

import (
	"context"
	"testing"

	upload_service_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/web/upload-service/rpc/uploader"
	"code.justin.tv/web/upload-service/rpc/uploader"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestImageUploaderImpl_CreateEmoteUploadRequest(t *testing.T) {
	Convey("With an image uploader", t, func() {
		mockUploader := new(upload_service_mock.Uploader)
		imageUploader := &ImageUploaderImpl{
			Uploader: mockUploader,
		}

		ctx := context.Background()

		Convey("when the uploader throws an error", func() {
			animeBearErr := errors.New("ANIME_BEAR_EMOTE_OVERLOAD")

			mockUploader.On("Create", ctx, mock.Anything).Return(nil, animeBearErr)
			resp, err := imageUploader.CreateEmoteUploadRequest(ctx, EmoteUploadParams{UserID: "123"})

			So(resp, ShouldBeNil)
			So(err, ShouldEqual, animeBearErr)
		})

		Convey("when the uploader is chill and does its thing with a single sized static image", func() {
			mockUploaderResponse := uploader.UploadResponse{
				UploadId: "SUPA_COOL_UPLOAD_ID",
				Url:      "https://s3.aws.com/super-cool-uploads-here/7e1fa34b-f65c-4ff8-b35a-94dc65afb350",
			}
			mockUploader.On("Create", ctx, mock.Anything).Return(&mockUploaderResponse, nil)
			resp, err := imageUploader.CreateEmoteUploadRequest(ctx, EmoteUploadParams{UserID: "123", Size: Size2xString})

			So(err, ShouldBeNil)
			So(*resp, ShouldResemble, mockUploaderResponse)

			actualRequest := mockUploader.Calls[0].Arguments.Get(1).(*uploader.UploadRequest)
			So(actualRequest.GetPreValidation().GetMaximumSize().GetWidth(), ShouldEqual, Size2xDimension)
			So(actualRequest.GetPreValidation().GetFileSizeLessThan(), ShouldEqual, MaxStaticFileSize)
			So(actualRequest.GetPreValidation().GetFormat(), ShouldEqual, RequiredStaticFormat)
			So(actualRequest.GetOutputs(), ShouldHaveLength, 1)
			So(actualRequest.GetOutputs()[0].GetAllowAnimation(), ShouldBeFalse)
			So(actualRequest.GetOutputs()[0].GetTransformations(), ShouldBeEmpty)
			So(actualRequest.GetOutputs()[0].GetPostValidation().GetMaximumFrames(), ShouldEqual, MaxStaticFrames)
		})

		Convey("when the uploader is chill and does its thing with a single sized animated image, without generated static assets", func() {
			mockUploaderResponse := uploader.UploadResponse{
				UploadId: "SUPA_COOL_UPLOAD_ID",
				Url:      "https://s3.aws.com/super-cool-uploads-here/7e1fa34b-f65c-4ff8-b35a-94dc65afb350",
			}
			mockUploader.On("Create", ctx, mock.Anything).Return(&mockUploaderResponse, nil)
			resp, err := imageUploader.CreateEmoteUploadRequest(ctx, EmoteUploadParams{UserID: "123", Size: Size2xString, IsAnimated: true})

			So(err, ShouldBeNil)
			So(*resp, ShouldResemble, mockUploaderResponse)

			actualRequest := mockUploader.Calls[0].Arguments.Get(1).(*uploader.UploadRequest)
			So(actualRequest.GetPreValidation().GetMaximumSize().GetWidth(), ShouldEqual, Size2xDimension)
			So(actualRequest.GetPreValidation().GetFileSizeLessThan(), ShouldEqual, MaxAnimatedFileSize)
			So(actualRequest.GetPreValidation().GetFormat(), ShouldEqual, RequiredAnimatedFormat)
			So(actualRequest.GetOutputs(), ShouldHaveLength, 1)
			So(actualRequest.GetOutputs()[0].GetAllowAnimation(), ShouldBeTrue)
			So(actualRequest.GetOutputs()[0].GetTransformations(), ShouldBeEmpty)
			So(actualRequest.GetOutputs()[0].GetPostValidation().GetMaximumFrames(), ShouldEqual, MaxAnimatedFrames)
		})

		Convey("when the uploader is chill and does its thing with a single sized animated image, with generated static assets", func() {
			mockUploaderResponse := uploader.UploadResponse{
				UploadId: "SUPA_COOL_UPLOAD_ID",
				Url:      "https://s3.aws.com/super-cool-uploads-here/7e1fa34b-f65c-4ff8-b35a-94dc65afb350",
			}
			mockUploader.On("Create", ctx, mock.Anything).Return(&mockUploaderResponse, nil)
			resp, err := imageUploader.CreateEmoteUploadRequest(ctx, EmoteUploadParams{UserID: "123", Size: Size2xString, IsAnimated: true, GenerateStaticVersionOfAnimatedAsset: true})

			So(err, ShouldBeNil)
			So(*resp, ShouldResemble, mockUploaderResponse)

			actualRequest := mockUploader.Calls[0].Arguments.Get(1).(*uploader.UploadRequest)
			So(actualRequest.GetPreValidation().GetMaximumSize().GetWidth(), ShouldEqual, Size2xDimension)
			So(actualRequest.GetPreValidation().GetFileSizeLessThan(), ShouldEqual, MaxAnimatedFileSize)
			So(actualRequest.GetPreValidation().GetFormat(), ShouldEqual, RequiredAnimatedFormat)
			So(actualRequest.GetOutputs(), ShouldHaveLength, 2)
			So(actualRequest.GetOutputs()[0].GetAllowAnimation(), ShouldBeTrue)
			So(actualRequest.GetOutputs()[0].GetTransformations(), ShouldBeEmpty)
			So(actualRequest.GetOutputs()[1].GetAllowAnimation(), ShouldBeFalse)
			So(actualRequest.GetOutputs()[1].GetTransformations(), ShouldHaveLength, 1)
			So(actualRequest.GetOutputs()[1].GetTransformations()[0].GetTranscode(), ShouldNotBeNil)
			So(actualRequest.GetOutputs()[1].GetTransformations()[0].GetTranscode().Format, ShouldEqual, RequiredStaticFormat)
			So(actualRequest.GetOutputs()[0].GetPostValidation().GetMaximumFrames(), ShouldEqual, MaxAnimatedFrames)
		})

		Convey("when the uploader is chill and does its thing with a original/resizable sized static image", func() {
			mockUploaderResponse := uploader.UploadResponse{
				UploadId: "SUPA_COOL_UPLOAD_ID",
				Url:      "https://s3.aws.com/super-cool-uploads-here/c1e0ad3a-79f2-4946-933e-58f2e0bcd654",
			}
			mockUploader.On("Create", ctx, mock.Anything).Return(&mockUploaderResponse, nil)
			resp, err := imageUploader.CreateEmoteUploadRequest(ctx, EmoteUploadParams{UserID: "123", Size: SizeOriginalString})

			So(err, ShouldBeNil)
			So(*resp, ShouldResemble, mockUploaderResponse)

			actualRequest := mockUploader.Calls[0].Arguments.Get(1).(*uploader.UploadRequest)
			So(actualRequest.GetPreValidation().GetMinimumSize().GetWidth(), ShouldEqual, Size4xDimension)
			So(actualRequest.GetPreValidation().GetMaximumSize().GetWidth(), ShouldEqual, MaxResolution)
			So(actualRequest.GetPreValidation().GetFileSizeLessThan(), ShouldEqual, MaxAutoResizeFileSize)
			So(actualRequest.GetPreValidation().GetFormat(), ShouldEqual, RequiredStaticFormat)
			So(actualRequest.GetOutputs(), ShouldHaveLength, 4)
			So(actualRequest.GetOutputs()[0].GetAllowAnimation(), ShouldBeFalse)
			So(actualRequest.GetOutputs()[0].GetTransformations(), ShouldBeEmpty)
			So(actualRequest.GetOutputs()[1].GetAllowAnimation(), ShouldBeFalse)
			So(actualRequest.GetOutputs()[1].GetTransformations(), ShouldHaveLength, 1)
			So(actualRequest.GetOutputs()[1].GetTransformations()[0].GetResize(), ShouldNotBeNil)
			So(actualRequest.GetOutputs()[2].GetAllowAnimation(), ShouldBeFalse)
			So(actualRequest.GetOutputs()[2].GetTransformations(), ShouldHaveLength, 1)
			So(actualRequest.GetOutputs()[2].GetTransformations()[0].GetResize(), ShouldNotBeNil)
			So(actualRequest.GetOutputs()[3].GetAllowAnimation(), ShouldBeFalse)
			So(actualRequest.GetOutputs()[3].GetTransformations(), ShouldHaveLength, 1)
			So(actualRequest.GetOutputs()[3].GetTransformations()[0].GetResize(), ShouldNotBeNil)
			So(actualRequest.GetOutputs()[0].GetPostValidation().GetMaximumFrames(), ShouldEqual, MaxStaticFrames)
		})

		Convey("when the uploader is chill and does its thing with a original/resizable sized animated image, without generated static assets", func() {
			mockUploaderResponse := uploader.UploadResponse{
				UploadId: "SUPA_COOL_UPLOAD_ID",
				Url:      "https://s3.aws.com/super-cool-uploads-here/c1e0ad3a-79f2-4946-933e-58f2e0bcd654",
			}
			mockUploader.On("Create", ctx, mock.Anything).Return(&mockUploaderResponse, nil)
			resp, err := imageUploader.CreateEmoteUploadRequest(ctx, EmoteUploadParams{UserID: "123", Size: SizeOriginalString, IsAnimated: true})

			So(err, ShouldBeNil)
			So(*resp, ShouldResemble, mockUploaderResponse)

			actualRequest := mockUploader.Calls[0].Arguments.Get(1).(*uploader.UploadRequest)
			So(actualRequest.GetPreValidation().GetMinimumSize().GetWidth(), ShouldEqual, Size4xDimension)
			So(actualRequest.GetPreValidation().GetMaximumSize().GetWidth(), ShouldEqual, MaxResolution)
			So(actualRequest.GetPreValidation().GetFileSizeLessThan(), ShouldEqual, MaxAutoResizeFileSize)
			So(actualRequest.GetPreValidation().GetFormat(), ShouldEqual, RequiredAnimatedFormat)
			So(actualRequest.GetOutputs(), ShouldHaveLength, 4)
			So(actualRequest.GetOutputs()[0].GetAllowAnimation(), ShouldBeTrue)
			So(actualRequest.GetOutputs()[0].GetTransformations(), ShouldBeEmpty)
			So(actualRequest.GetOutputs()[1].GetAllowAnimation(), ShouldBeTrue)
			So(actualRequest.GetOutputs()[1].GetTransformations(), ShouldHaveLength, 1)
			So(actualRequest.GetOutputs()[1].GetTransformations()[0].GetResize(), ShouldNotBeNil)
			So(actualRequest.GetOutputs()[2].GetAllowAnimation(), ShouldBeTrue)
			So(actualRequest.GetOutputs()[2].GetTransformations(), ShouldHaveLength, 1)
			So(actualRequest.GetOutputs()[2].GetTransformations()[0].GetResize(), ShouldNotBeNil)
			So(actualRequest.GetOutputs()[3].GetAllowAnimation(), ShouldBeTrue)
			So(actualRequest.GetOutputs()[3].GetTransformations(), ShouldHaveLength, 1)
			So(actualRequest.GetOutputs()[3].GetTransformations()[0].GetResize(), ShouldNotBeNil)
			So(actualRequest.GetOutputs()[0].GetPostValidation().GetMaximumFrames(), ShouldEqual, MaxAnimatedFrames)
		})

		Convey("when the uploader is chill and does its thing with a original/resizable sized animated image, with generated static assets", func() {
			mockUploaderResponse := uploader.UploadResponse{
				UploadId: "SUPA_COOL_UPLOAD_ID",
				Url:      "https://s3.aws.com/super-cool-uploads-here/c1e0ad3a-79f2-4946-933e-58f2e0bcd654",
			}
			mockUploader.On("Create", ctx, mock.Anything).Return(&mockUploaderResponse, nil)
			resp, err := imageUploader.CreateEmoteUploadRequest(ctx, EmoteUploadParams{UserID: "123", Size: SizeOriginalString, IsAnimated: true, GenerateStaticVersionOfAnimatedAsset: true})

			So(err, ShouldBeNil)
			So(*resp, ShouldResemble, mockUploaderResponse)

			actualRequest := mockUploader.Calls[0].Arguments.Get(1).(*uploader.UploadRequest)
			So(actualRequest.GetPreValidation().GetMinimumSize().GetWidth(), ShouldEqual, Size4xDimension)
			So(actualRequest.GetPreValidation().GetMaximumSize().GetWidth(), ShouldEqual, MaxResolution)
			So(actualRequest.GetPreValidation().GetFileSizeLessThan(), ShouldEqual, MaxAutoResizeFileSize)
			So(actualRequest.GetPreValidation().GetFormat(), ShouldEqual, RequiredAnimatedFormat)
			So(actualRequest.GetOutputs(), ShouldHaveLength, 8)
			So(actualRequest.GetOutputs()[0].GetAllowAnimation(), ShouldBeTrue)
			So(actualRequest.GetOutputs()[0].GetTransformations(), ShouldBeEmpty)
			So(actualRequest.GetOutputs()[1].GetAllowAnimation(), ShouldBeTrue)
			So(actualRequest.GetOutputs()[1].GetTransformations(), ShouldHaveLength, 1)
			So(actualRequest.GetOutputs()[1].GetTransformations()[0].GetResize(), ShouldNotBeNil)
			So(actualRequest.GetOutputs()[2].GetAllowAnimation(), ShouldBeTrue)
			So(actualRequest.GetOutputs()[2].GetTransformations(), ShouldHaveLength, 1)
			So(actualRequest.GetOutputs()[2].GetTransformations()[0].GetResize(), ShouldNotBeNil)
			So(actualRequest.GetOutputs()[3].GetAllowAnimation(), ShouldBeTrue)
			So(actualRequest.GetOutputs()[3].GetTransformations(), ShouldHaveLength, 1)
			So(actualRequest.GetOutputs()[3].GetTransformations()[0].GetResize(), ShouldNotBeNil)
			So(actualRequest.GetOutputs()[4].GetAllowAnimation(), ShouldBeFalse)
			So(actualRequest.GetOutputs()[4].GetTransformations(), ShouldHaveLength, 1)
			So(actualRequest.GetOutputs()[4].GetTransformations()[0].GetTranscode(), ShouldNotBeNil)
			So(actualRequest.GetOutputs()[4].GetTransformations()[0].GetTranscode().Format, ShouldEqual, RequiredStaticFormat)
			So(actualRequest.GetOutputs()[5].GetAllowAnimation(), ShouldBeFalse)
			So(actualRequest.GetOutputs()[5].GetTransformations(), ShouldHaveLength, 2)
			So(actualRequest.GetOutputs()[5].GetTransformations()[0].GetResize(), ShouldNotBeNil)
			So(actualRequest.GetOutputs()[5].GetTransformations()[1].GetTranscode(), ShouldNotBeNil)
			So(actualRequest.GetOutputs()[5].GetTransformations()[1].GetTranscode().Format, ShouldEqual, RequiredStaticFormat)
			So(actualRequest.GetOutputs()[6].GetAllowAnimation(), ShouldBeFalse)
			So(actualRequest.GetOutputs()[6].GetTransformations(), ShouldHaveLength, 2)
			So(actualRequest.GetOutputs()[6].GetTransformations()[0].GetResize(), ShouldNotBeNil)
			So(actualRequest.GetOutputs()[6].GetTransformations()[1].GetTranscode(), ShouldNotBeNil)
			So(actualRequest.GetOutputs()[6].GetTransformations()[1].GetTranscode().Format, ShouldEqual, RequiredStaticFormat)
			So(actualRequest.GetOutputs()[7].GetAllowAnimation(), ShouldBeFalse)
			So(actualRequest.GetOutputs()[7].GetTransformations(), ShouldHaveLength, 2)
			So(actualRequest.GetOutputs()[7].GetTransformations()[0].GetResize(), ShouldNotBeNil)
			So(actualRequest.GetOutputs()[7].GetTransformations()[1].GetTranscode(), ShouldNotBeNil)
			So(actualRequest.GetOutputs()[7].GetTransformations()[1].GetTranscode().Format, ShouldEqual, RequiredStaticFormat)
			So(actualRequest.GetOutputs()[0].GetPostValidation().GetMaximumFrames(), ShouldEqual, MaxAnimatedFrames)
		})
	})
}

func TestGenerateSingleResizedOutput(t *testing.T) {
	Convey("Without a darn thing", t, func() {
		Convey("With a static asset", func() {
			output := getSingleResizedOutput("111", Size1xString, false, false)

			So(output.GetName(), ShouldEqual, "111_1x_{{UploadID}}")
			So(output.GetPermissions().GrantRead, ShouldEqual, "uri=http://acs.amazonaws.com/groups/global/AllUsers")
			So(output.GetTransformations(), ShouldHaveLength, 1)
			So(output.GetTransformations()[0].GetResize().GetDimensions().GetWidth(), ShouldEqual, 28)
			So(output.GetTransformations()[0].GetResize().GetDimensions().GetHeight(), ShouldEqual, 28)
			So(output.GetAllowAnimation(), ShouldBeFalse)
		})

		Convey("With a static asset to be transcoded", func() {
			output := getSingleResizedOutput("111", Size1xString, false, true)

			So(output.GetName(), ShouldEqual, "111_1x_{{UploadID}}")
			So(output.GetPermissions().GrantRead, ShouldEqual, "uri=http://acs.amazonaws.com/groups/global/AllUsers")
			So(output.GetTransformations(), ShouldHaveLength, 2)
			So(output.GetTransformations()[0].GetResize().GetDimensions().GetWidth(), ShouldEqual, 28)
			So(output.GetTransformations()[0].GetResize().GetDimensions().GetHeight(), ShouldEqual, 28)
			So(output.GetTransformations()[1].GetTranscode(), ShouldNotBeNil)
			So(output.GetTransformations()[1].GetTranscode().Format, ShouldEqual, RequiredStaticFormat)
			So(output.GetAllowAnimation(), ShouldBeFalse)
		})

		Convey("With an animated asset", func() {
			output := getSingleResizedOutput("111", Size1xString, true, false)

			So(output.GetName(), ShouldEqual, "111_1x_{{UploadID}}_animated")
			So(output.GetPermissions().GrantRead, ShouldEqual, "uri=http://acs.amazonaws.com/groups/global/AllUsers")
			So(output.GetTransformations(), ShouldHaveLength, 1)
			So(output.GetTransformations()[0].GetResize().GetDimensions().GetWidth(), ShouldEqual, 28)
			So(output.GetTransformations()[0].GetResize().GetDimensions().GetHeight(), ShouldEqual, 28)
			So(output.GetAllowAnimation(), ShouldBeTrue)
		})
	})
}

func TestGetSingleOutput(t *testing.T) {
	Convey("Without a darn thing", t, func() {
		Convey("With a static asset", func() {
			output := getSingleOutput("4444", Size4xString, false, false)

			So(output.GetName(), ShouldEqual, "4444_4x_{{UploadID}}")
			So(output.GetPermissions().GrantRead, ShouldEqual, "uri=http://acs.amazonaws.com/groups/global/AllUsers")
			So(output.GetTransformations(), ShouldBeEmpty)
			So(output.GetAllowAnimation(), ShouldBeFalse)
		})

		Convey("With a static asset to be transcoded", func() {
			output := getSingleOutput("4444", Size4xString, false, true)

			So(output.GetName(), ShouldEqual, "4444_4x_{{UploadID}}")
			So(output.GetPermissions().GrantRead, ShouldEqual, "uri=http://acs.amazonaws.com/groups/global/AllUsers")
			So(output.GetTransformations(), ShouldHaveLength, 1)
			So(output.GetTransformations()[0].GetTranscode(), ShouldNotBeNil)
			So(output.GetTransformations()[0].GetTranscode().Format, ShouldEqual, RequiredStaticFormat)
			So(output.GetAllowAnimation(), ShouldBeFalse)
		})

		Convey("With an animated asset", func() {
			output := getSingleOutput("4444", Size4xString, true, false)

			So(output.GetName(), ShouldEqual, "4444_4x_{{UploadID}}_animated")
			So(output.GetPermissions().GrantRead, ShouldEqual, "uri=http://acs.amazonaws.com/groups/global/AllUsers")
			So(output.GetTransformations(), ShouldBeEmpty)
			So(output.GetAllowAnimation(), ShouldBeTrue)
		})
	})
}

func TestGetOutputsForResizing(t *testing.T) {
	Convey("Without a darn thing", t, func() {
		Convey("With a static asset", func() {
			outputs := getOutputsForResizing("41414", false, false)

			So(outputs, ShouldHaveLength, 4)
			var foundOriginal, found1x, found2x, found4x bool
			for _, output := range outputs {
				if len(output.GetTransformations()) == 0 {
					foundOriginal = true
					So(output.GetName(), ShouldEqual, "41414_original_{{UploadID}}")
				} else {
					So(output.GetTransformations(), ShouldHaveLength, 1)
					width := output.GetTransformations()[0].GetResize().GetDimensions().GetWidth()
					switch width {
					case Size4xDimension:
						found4x = true
						So(output.GetName(), ShouldEqual, "41414_4x_{{UploadID}}")
					case Size2xDimension:
						found2x = true
						So(output.GetName(), ShouldEqual, "41414_2x_{{UploadID}}")
					case Size1xDimension:
						found1x = true
						So(output.GetName(), ShouldEqual, "41414_1x_{{UploadID}}")
					}
				}
				So(output.GetAllowAnimation(), ShouldBeFalse)
			}

			So(foundOriginal, ShouldBeTrue)
			So(found1x, ShouldBeTrue)
			So(found2x, ShouldBeTrue)
			So(found4x, ShouldBeTrue)
		})

		Convey("With a static asset to be transcoded", func() {
			outputs := getOutputsForResizing("41414", false, true)

			So(outputs, ShouldHaveLength, 4)
			var foundOriginal, found1x, found2x, found4x bool
			for _, output := range outputs {
				if len(output.GetTransformations()) == 1 {
					foundOriginal = true
					So(output.GetName(), ShouldEqual, "41414_original_{{UploadID}}")
					So(output.GetTransformations()[0].GetTranscode(), ShouldNotBeNil)
					So(output.GetTransformations()[0].GetTranscode().Format, ShouldEqual, RequiredStaticFormat)
				} else {
					So(output.GetTransformations(), ShouldHaveLength, 2)

					width := output.GetTransformations()[0].GetResize().GetDimensions().GetWidth()
					switch width {
					case Size4xDimension:
						found4x = true
						So(output.GetName(), ShouldEqual, "41414_4x_{{UploadID}}")
					case Size2xDimension:
						found2x = true
						So(output.GetName(), ShouldEqual, "41414_2x_{{UploadID}}")
					case Size1xDimension:
						found1x = true
						So(output.GetName(), ShouldEqual, "41414_1x_{{UploadID}}")
					}

					So(output.GetTransformations()[1].GetTranscode(), ShouldNotBeNil)
					So(output.GetTransformations()[1].GetTranscode().Format, ShouldEqual, RequiredStaticFormat)
				}
				So(output.GetAllowAnimation(), ShouldBeFalse)
			}

			So(foundOriginal, ShouldBeTrue)
			So(found1x, ShouldBeTrue)
			So(found2x, ShouldBeTrue)
			So(found4x, ShouldBeTrue)
		})

		Convey("With an animated asset", func() {
			outputs := getOutputsForResizing("41414", true, false)

			So(outputs, ShouldHaveLength, 4)
			var foundOriginal, found1x, found2x, found4x bool
			for _, output := range outputs {
				if len(output.GetTransformations()) == 0 {
					foundOriginal = true
					So(output.GetName(), ShouldEqual, "41414_original_{{UploadID}}_animated")
				} else {
					So(output.GetTransformations(), ShouldHaveLength, 1)
					width := output.GetTransformations()[0].GetResize().GetDimensions().GetWidth()
					switch width {
					case Size4xDimension:
						found4x = true
						So(output.GetName(), ShouldEqual, "41414_4x_{{UploadID}}_animated")
					case Size2xDimension:
						found2x = true
						So(output.GetName(), ShouldEqual, "41414_2x_{{UploadID}}_animated")
					case Size1xDimension:
						found1x = true
						So(output.GetName(), ShouldEqual, "41414_1x_{{UploadID}}_animated")
					}
				}
				So(output.GetAllowAnimation(), ShouldBeTrue)
			}

			So(foundOriginal, ShouldBeTrue)
			So(found1x, ShouldBeTrue)
			So(found2x, ShouldBeTrue)
			So(found4x, ShouldBeTrue)
		})
	})
}

func TestGetValidationForResizing(t *testing.T) {
	Convey("Without a darn thing", t, func() {
		Convey("With a static asset", func() {
			validation := getValidationForResizing(false)
			So(validation.GetMinimumSize().GetWidth(), ShouldEqual, Size4xDimension)
			So(validation.GetMinimumSize().GetHeight(), ShouldEqual, Size4xDimension)
			So(validation.GetMaximumSize().GetWidth(), ShouldEqual, MaxResolution)
			So(validation.GetMaximumSize().GetHeight(), ShouldEqual, MaxResolution)
			So(validation.GetAspectRatio(), ShouldEqual, RequiredRatio)
			So(validation.GetFormat(), ShouldEqual, RequiredStaticFormat)
			So(validation.GetFileSizeLessThan(), ShouldEqual, MaxAutoResizeFileSize)
		})

		Convey("With an animated asset", func() {
			validation := getValidationForResizing(true)
			So(validation.GetMinimumSize().GetWidth(), ShouldEqual, Size4xDimension)
			So(validation.GetMinimumSize().GetHeight(), ShouldEqual, Size4xDimension)
			So(validation.GetMaximumSize().GetWidth(), ShouldEqual, MaxResolution)
			So(validation.GetMaximumSize().GetHeight(), ShouldEqual, MaxResolution)
			So(validation.GetAspectRatio(), ShouldEqual, RequiredRatio)
			So(validation.GetFormat(), ShouldEqual, RequiredAnimatedFormat)
			So(validation.GetFileSizeLessThan(), ShouldEqual, MaxAutoResizeFileSize)
		})
	})
}

func TestGetValidationForEmoteSize(t *testing.T) {
	Convey("Without a darn thing", t, func() {
		Convey("With a static asset", func() {
			validation := getValidationForEmoteSize(Size2xString, false)
			So(validation.GetMinimumSize().GetWidth(), ShouldEqual, Size2xDimension)
			So(validation.GetMinimumSize().GetHeight(), ShouldEqual, Size2xDimension)
			So(validation.GetMaximumSize().GetWidth(), ShouldEqual, Size2xDimension)
			So(validation.GetMaximumSize().GetHeight(), ShouldEqual, Size2xDimension)
			So(validation.GetAspectRatio(), ShouldEqual, RequiredRatio)
			So(validation.GetFormat(), ShouldEqual, RequiredStaticFormat)
			So(validation.GetFileSizeLessThan(), ShouldEqual, MaxStaticFileSize)
		})

		Convey("With an animated asset", func() {
			validation := getValidationForEmoteSize(Size2xString, true)
			So(validation.GetMinimumSize().GetWidth(), ShouldEqual, Size2xDimension)
			So(validation.GetMinimumSize().GetHeight(), ShouldEqual, Size2xDimension)
			So(validation.GetMaximumSize().GetWidth(), ShouldEqual, Size2xDimension)
			So(validation.GetMaximumSize().GetHeight(), ShouldEqual, Size2xDimension)
			So(validation.GetAspectRatio(), ShouldEqual, RequiredRatio)
			So(validation.GetFormat(), ShouldEqual, RequiredAnimatedFormat)
			So(validation.GetFileSizeLessThan(), ShouldEqual, MaxAnimatedFileSize)
		})
	})
}
