package imageuploader

import (
	"testing"

	mkd "code.justin.tv/commerce/mako/twirp/dashboard"
	. "github.com/smartystreets/goconvey/convey"
)

func TestGetDimensionsForEmoteSize(t *testing.T) {
	Convey("with nothing to initialize", t, func() {
		So(GetDimensionsForEmoteSize(""), ShouldEqual, 0)
		So(GetDimensionsForEmoteSize(Size4xString), ShouldEqual, 112)
		So(GetDimensionsForEmoteSize(Size2xString), ShouldEqual, 56)
		So(GetDimensionsForEmoteSize(Size1xString), ShouldEqual, 28)
		So(GetDimensionsForEmoteSize("foo"), ShouldEqual, 0)
	})
}

func TestStringifySize(t *testing.T) {
	Convey("with nothing to initialize", t, func() {
		So(StringifySize(mkd.EmoteImageSize(0)), ShouldEqual, SizeOriginalString) //original is the default as it's zero index
		So(StringifySize(mkd.EmoteImageSize_size_original), ShouldEqual, SizeOriginalString)
		So(StringifySize(mkd.EmoteImageSize_size_4x), ShouldEqual, Size4xString)
		So(StringifySize(mkd.EmoteImageSize_size_2x), ShouldEqual, Size2xString)
		So(StringifySize(mkd.EmoteImageSize_size_1x), ShouldEqual, Size1xString)
		So(StringifySize(mkd.EmoteImageSize(99)), ShouldEqual, "unknown")
	})
}

func TestStringSizeToTwirp(t *testing.T) {
	Convey("with nothing to initialize", t, func() {
		So(StringSizeToTwirp(""), ShouldEqual, -1)
		So(StringSizeToTwirp(SizeOriginalString), ShouldEqual, mkd.EmoteImageSize_size_original)
		So(StringSizeToTwirp(Size4xString), ShouldEqual, mkd.EmoteImageSize_size_4x)
		So(StringSizeToTwirp(Size2xString), ShouldEqual, mkd.EmoteImageSize_size_2x)
		So(StringSizeToTwirp(Size1xString), ShouldEqual, mkd.EmoteImageSize_size_1x)
		So(StringSizeToTwirp("foo"), ShouldEqual, -1)
	})
}
