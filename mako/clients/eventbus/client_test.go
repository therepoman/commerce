package eventbus

import (
	"context"
	"errors"
	"testing"

	mako "code.justin.tv/commerce/mako/internal"
	client_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/eventbus/client"
	"code.justin.tv/eventbus/schema/pkg/emoticon"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestClientWrapper_EmoticonCreate(t *testing.T) {
	Convey("given an event bus client wrapper", t, func() {
		eventbusPublisher := &client_mock.Publisher{}

		wrapper := &clientWrapper{
			Client: eventbusPublisher,
		}

		ctx := context.Background()

		emoticon1 := mako.Emote{
			ID:      "walrusID",
			GroupID: "walrusGroupID",
			OwnerID: "walrusOwnerID",
			Code:    "walrusStrike",
			State:   mako.Active,
		}

		emoticon2 := mako.Emote{
			ID:      "walrusID2",
			GroupID: "walrusGroupID2",
			OwnerID: "walrusOwnerID",
			Code:    "walrusPose",
			State:   mako.Active,
		}

		emoticonsToCreate := []mako.Emote{emoticon1, emoticon2}

		Convey("should return nil", func() {
			Convey("when we successfully publish to event bus", func() {
				eventbusPublisher.On("Publish", ctx, mock.Anything).Return(nil)
			})

			err := wrapper.EmoticonCreate(ctx, emoticonsToCreate)

			So(err, ShouldBeNil)
		})

		Convey("should return err", func() {
			Convey("when we fail publish to event bus for all requests", func() {
				eventbusPublisher.On("Publish", ctx, mock.Anything).Return(errors.New("WALRUS STRIKE"))
			})

			Convey("when we fail publish to event bus a single request", func() {
				eventbusPublisher.On("Publish", ctx, mock.MatchedBy(func(create *emoticon.Create) bool {
					return create.Emoticon.Id == emoticon1.ID
				})).Return(nil)
				eventbusPublisher.On("Publish", ctx, mock.MatchedBy(func(create *emoticon.Create) bool {
					return create.Emoticon.Id == emoticon2.ID
				})).Return(errors.New("WALRUS STRIKE"))
			})

			err := wrapper.EmoticonCreate(ctx, emoticonsToCreate)

			So(err, ShouldNotBeNil)
		})
	})
}

func TestClientWrapper_EmoticonDelete(t *testing.T) {
	Convey("given an event bus client wrapper", t, func() {
		eventbusPublisher := &client_mock.Publisher{}

		wrapper := &clientWrapper{
			Client: eventbusPublisher,
		}

		ctx := context.Background()

		emoticon1 := mako.Emote{
			ID:      "walrusID",
			GroupID: "walrusGroupID",
			OwnerID: "walrusOwnerID",
			Code:    "walrusStrike",
			State:   mako.Inactive,
		}

		emoticon2 := mako.Emote{
			ID:      "walrusID2",
			GroupID: "walrusGroupID2",
			OwnerID: "walrusOwnerID",
			Code:    "walrusPose",
			State:   mako.Moderated,
		}

		emoticonsToDelete := []mako.Emote{emoticon1, emoticon2}

		Convey("should return nil", func() {
			Convey("when we successfully publish to event bus", func() {
				eventbusPublisher.On("Publish", ctx, mock.Anything).Return(nil)
			})

			err := wrapper.EmoticonDelete(ctx, emoticonsToDelete)

			So(err, ShouldBeNil)
		})

		Convey("should return err", func() {
			Convey("when we fail publish to event bus for all requests", func() {
				eventbusPublisher.On("Publish", ctx, mock.Anything).Return(errors.New("WALRUS STRIKE"))
			})

			Convey("when we fail publish to event bus a single request", func() {
				eventbusPublisher.On("Publish", ctx, mock.MatchedBy(func(delete *emoticon.Delete) bool {
					return delete.Emoticon.Id == emoticon1.ID
				})).Return(nil)
				eventbusPublisher.On("Publish", ctx, mock.MatchedBy(func(delete *emoticon.Delete) bool {
					return delete.Emoticon.Id == emoticon2.ID
				})).Return(errors.New("WALRUS STRIKE"))
			})

			err := wrapper.EmoticonDelete(ctx, emoticonsToDelete)

			So(err, ShouldNotBeNil)
		})
	})
}
