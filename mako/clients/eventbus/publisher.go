package eventbus

import (
	"context"

	"code.justin.tv/commerce/mako/clients/emoticons"
)

type Publisher interface {
	PublishEmoticonCreate(ctx context.Context, emoticonIDs []string) error
	PublishEmoticonDelete(ctx context.Context, emoticonIDs []string) error
}

type emoticonPublisher struct {
	client             Client
	emoticonAggregator emoticons.EmoticonAggregator
}

func NewPublisher(client Client, aggregator emoticons.EmoticonAggregator) *emoticonPublisher {
	return &emoticonPublisher{
		client:             client,
		emoticonAggregator: aggregator,
	}
}

func (p *emoticonPublisher) PublishEmoticonCreate(ctx context.Context, emoticonIDs []string) error {
	createdEmoticons, err := p.emoticonAggregator.GetEmoticonsByEmoteIDs(ctx, emoticonIDs)
	if err != nil {
		return err
	}

	return p.client.EmoticonCreate(ctx, createdEmoticons)
}

func (p *emoticonPublisher) PublishEmoticonDelete(ctx context.Context, emoticonIDs []string) error {
	deletedEmoticons, err := p.emoticonAggregator.GetEmoticonsByEmoteIDs(ctx, emoticonIDs)
	if err != nil {
		return err
	}

	return p.client.EmoticonDelete(ctx, deletedEmoticons)
}
