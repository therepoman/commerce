package eventbus

import (
	"context"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/mako/config"
	mako "code.justin.tv/commerce/mako/internal"
	eventbus "code.justin.tv/eventbus/client"
	"code.justin.tv/eventbus/client/mock"
	"code.justin.tv/eventbus/client/publisher"
	"code.justin.tv/eventbus/schema/pkg/emoticon"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/endpoints"
	"github.com/aws/aws-sdk-go/aws/session"
	multierror "github.com/hashicorp/go-multierror"
	"github.com/pkg/errors"
)

type Client interface {
	EmoticonCreate(ctx context.Context, emoticons []mako.Emote) error
	EmoticonDelete(ctx context.Context, emoticons []mako.Emote) error
}

type clientWrapper struct {
	Client eventbus.Publisher
}

func makoEnvToEventBusEnv(makoEnv string) publisher.Environment {
	switch makoEnv {
	case "dev":
		return publisher.EnvDevelopment
	case "staging":
		return publisher.EnvStaging
	case "prod":
		return publisher.EnvProduction
	default:
		return publisher.EnvDevelopment
	}
}

func makoStateToEventBusState(state mako.EmoteState) emoticon.State {
	switch state {
	case mako.Active:
		return emoticon.State_STATE_ACTIVE
	case mako.Pending:
		return emoticon.State_STATE_PENDING
	case mako.Archived:
		return emoticon.State_STATE_ARCHIVED
	case mako.PendingArchived:
		return emoticon.State_STATE_PENDING_ARCHIVED
	case mako.Inactive:
		return emoticon.State_STATE_INACTIVE
	case mako.Moderated:
		return emoticon.State_STATE_MODERATED
	default:
		return emoticon.State_STATE_INVALID
	}
}

func NewClient(cfg *config.Configuration) (*clientWrapper, error) {
	env := makoEnvToEventBusEnv(cfg.EnvironmentPrefix)

	if env == publisher.EnvDevelopment {
		logrus.Info("running in development, using mock eventbus publisher client")
		return &clientWrapper{
			&mock.Publisher{},
		}, nil
	}

	awsSession, err := session.NewSession(&aws.Config{
		Region:              aws.String(cfg.AWSRegion),
		STSRegionalEndpoint: endpoints.RegionalSTSEndpoint,
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to create AWS Session")
	}

	pub, err := publisher.New(publisher.Config{
		Session:     awsSession,
		Environment: env,
		EventTypes:  []string{emoticon.DeleteEventType, emoticon.CreateEventType},
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to create eventbus publisher")
	}

	return &clientWrapper{
		Client: pub,
	}, nil
}

func (c *clientWrapper) EmoticonCreate(ctx context.Context, emoticons []mako.Emote) error {
	errs := multierror.Error{}
	for _, emote := range emoticons {
		err := c.Client.Publish(ctx, &emoticon.Create{
			Emoticon: &emoticon.Emoticon{
				Id:      emote.ID,
				GroupId: emote.GroupID,
				OwnerId: emote.OwnerID,
				Token:   emote.Code,
				//TODO: Update eventbus emote event to include order
				//Order: emote.Order,
			},
			State: makoStateToEventBusState(emote.State),
		})
		if err != nil {
			logrus.WithError(err).Error("failed to publish emoticon create event")
			errs.Errors = append(errs.Errors, err)
		}
	}

	return errs.ErrorOrNil()
}

func (c *clientWrapper) EmoticonDelete(ctx context.Context, emoticons []mako.Emote) error {
	errs := multierror.Error{}
	for _, emote := range emoticons {
		err := c.Client.Publish(ctx, &emoticon.Delete{
			Emoticon: &emoticon.Emoticon{
				Id:      emote.ID,
				GroupId: emote.GroupID,
				OwnerId: emote.OwnerID,
				Token:   emote.Code,
				//TODO: Update eventbus event to include order
				//Order: emote.Order,
			},
			State: makoStateToEventBusState(emote.State),
		})
		if err != nil {
			logrus.WithError(err).Error("failed to publish emoticon delete event")
			errs.Errors = append(errs.Errors, err)
		}
	}

	return errs.ErrorOrNil()
}
