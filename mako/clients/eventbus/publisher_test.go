package eventbus

import (
	"context"
	"errors"
	"testing"

	mako "code.justin.tv/commerce/mako/internal"
	emoticons_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/emoticons"
	eventbus_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/eventbus"
	. "github.com/smartystreets/goconvey/convey"
)

func TestEmoticonPublisher_PublishEmoticonCreate(t *testing.T) {
	Convey("given an event bus publisher", t, func() {
		client := &eventbus_mock.Client{}
		emoticonAggregator := &emoticons_mock.EmoticonAggregator{}

		pub := &emoticonPublisher{
			client:             client,
			emoticonAggregator: emoticonAggregator,
		}

		ctx := context.Background()

		emoticonIDs := []string{"walrusID", "walrusID2"}

		emoticon1 := mako.Emote{
			ID:      "walrusID",
			GroupID: "walrusGroupID",
			OwnerID: "walrusOwnerID",
			Code:    "walrusStrike",
			State:   mako.Inactive,
		}

		emoticon2 := mako.Emote{
			ID:      "walrusID2",
			GroupID: "walrusGroupID2",
			OwnerID: "walrusOwnerID",
			Code:    "walrusPose",
			State:   mako.Moderated,
		}

		emoticonsToCreate := []mako.Emote{emoticon1, emoticon2}

		Convey("returns nil", func() {
			Convey("when we successfully fetch and publish", func() {
				emoticonAggregator.On("GetEmoticonsByEmoteIDs", ctx, emoticonIDs).Return(emoticonsToCreate, nil)
				client.On("EmoticonCreate", ctx, emoticonsToCreate).Return(nil)
			})

			err := pub.PublishEmoticonCreate(ctx, emoticonIDs)

			So(err, ShouldBeNil)
		})

		Convey("returns err", func() {
			Convey("when we fail to fetch", func() {
				emoticonAggregator.On("GetEmoticonsByEmoteIDs", ctx, emoticonIDs).Return(nil, errors.New("WALRUS STRIKE"))
			})

			Convey("when we fail to publish", func() {
				emoticonAggregator.On("GetEmoticonsByEmoteIDs", ctx, emoticonIDs).Return(emoticonsToCreate, nil)
				client.On("EmoticonCreate", ctx, emoticonsToCreate).Return(errors.New("WALRUS STRIKE"))
			})

			err := pub.PublishEmoticonCreate(ctx, emoticonIDs)

			So(err, ShouldNotBeNil)
		})
	})
}

func TestEmoticonPublisher_PublishEmoticonDelete(t *testing.T) {
	Convey("given an event bus publisher", t, func() {
		client := &eventbus_mock.Client{}
		emoticonAggregator := &emoticons_mock.EmoticonAggregator{}

		pub := &emoticonPublisher{
			client:             client,
			emoticonAggregator: emoticonAggregator,
		}

		ctx := context.Background()

		emoticonIDs := []string{"walrusID", "walrusID2"}

		emoticon1 := mako.Emote{
			ID:      "walrusID",
			GroupID: "walrusGroupID",
			OwnerID: "walrusOwnerID",
			Code:    "walrusStrike",
			State:   mako.Inactive,
		}

		emoticon2 := mako.Emote{
			ID:      "walrusID2",
			GroupID: "walrusGroupID2",
			OwnerID: "walrusOwnerID",
			Code:    "walrusPose",
			State:   mako.Moderated,
		}

		emoticonsToDelete := []mako.Emote{emoticon1, emoticon2}

		Convey("returns nil", func() {
			Convey("when we successfully fetch and publish", func() {
				emoticonAggregator.On("GetEmoticonsByEmoteIDs", ctx, emoticonIDs).Return(emoticonsToDelete, nil)
				client.On("EmoticonDelete", ctx, emoticonsToDelete).Return(nil)
			})

			err := pub.PublishEmoticonDelete(ctx, emoticonIDs)

			So(err, ShouldBeNil)
		})

		Convey("returns err", func() {
			Convey("when we fail to fetch", func() {
				emoticonAggregator.On("GetEmoticonsByEmoteIDs", ctx, emoticonIDs).Return(nil, errors.New("WALRUS STRIKE"))
			})

			Convey("when we fail to publish", func() {
				emoticonAggregator.On("GetEmoticonsByEmoteIDs", ctx, emoticonIDs).Return(emoticonsToDelete, nil)
				client.On("EmoticonDelete", ctx, emoticonsToDelete).Return(errors.New("WALRUS STRIKE"))
			})

			err := pub.PublishEmoticonDelete(ctx, emoticonIDs)

			So(err, ShouldNotBeNil)
		})
	})
}
