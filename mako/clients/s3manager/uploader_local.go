package s3manager

import (
	"context"
	"io/ioutil"

	"github.com/aws/aws-sdk-go/service/s3/s3manager"
)

type UploaderLocalImpl struct {
	store map[string][]byte
}

// NewLocalUploader returns a local S3Manager Uploader.
// The uploaders are stored in the supplied store map so you can share it with a downloader.
func NewLocalUploader(store map[string][]byte) *UploaderLocalImpl {
	return &UploaderLocalImpl{
		store: store,
	}
}

func (u *UploaderLocalImpl) UploadWithIterator(ctx context.Context, iter s3manager.BatchUploadIterator) error {
	var errs []s3manager.Error
	for iter.Next() {
		object := iter.UploadObject()

		body, err := ioutil.ReadAll(object.Object.Body)

		if err != nil {
			s3Err := s3manager.Error{
				OrigErr: err,
				Bucket:  object.Object.Bucket,
				Key:     object.Object.Key,
			}
			errs = append(errs, s3Err)
			continue
		}

		u.store[*object.Object.Key] = body

		if object.After == nil {
			continue
		}

		if err := object.After(); err != nil {
			s3Err := s3manager.Error{
				OrigErr: err,
				Bucket:  object.Object.Bucket,
				Key:     object.Object.Key,
			}

			errs = append(errs, s3Err)
		}
	}
	return nil
}

func (u *UploaderLocalImpl) Upload(ctx context.Context, input s3manager.UploadInput) error {
	body := []byte{}
	_, err := input.Body.Read(body)
	if err != nil {
		return err
	}

	u.store[*input.Key] = body

	return nil
}

// Exists returns true if an object has been uploaded with the given key.
func (u *UploaderLocalImpl) Exists(key string) bool {
	_, ok := u.store[key]

	return ok
}
