package s3manager

import (
	"context"

	"github.com/aws/aws-sdk-go/aws/client"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
)

type DownloaderImpl struct {
	client *s3manager.Downloader
}

// NewDownloader returns a wrapper around the s3manager.Downloader
func NewDownloader(c client.ConfigProvider) *DownloaderImpl {
	return &DownloaderImpl{
		client: s3manager.NewDownloader(c),
	}
}

// DownloadWithIterator is a wrapper around s3manager.Downloader.DownloadWithIterator. See the s3manager docs for more.
func (d *DownloaderImpl) DownloadWithIterator(ctx context.Context, iter s3manager.BatchDownloadIterator) error {
	return d.client.DownloadWithIterator(ctx, iter)
}
