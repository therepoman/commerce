package s3manager

import (
	"context"

	"github.com/aws/aws-sdk-go/aws/client"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
)

type UploaderImpl struct {
	client *s3manager.Uploader
}

// NewUploader returns a wrapper around the s3manager.Uploader
func NewUploader(c client.ConfigProvider) *UploaderImpl {
	return &UploaderImpl{
		client: s3manager.NewUploader(c),
	}
}

// UploadWithIterator is a wrapper around s3manager.Uploader.UploadWithIterator. See the s3manager docs for more.
func (u *UploaderImpl) UploadWithIterator(ctx context.Context, iter s3manager.BatchUploadIterator) error {
	return u.client.UploadWithIterator(ctx, iter)
}

// Upload is a wrapper around s3manager.Uploader.Upload. See the s3manager docs for more.
func (u *UploaderImpl) Upload(ctx context.Context, input s3manager.UploadInput) error {
	_, err := u.client.UploadWithContext(ctx, &input)
	return err
}
