package s3manager

import (
	"context"
	"errors"

	"github.com/aws/aws-sdk-go/service/s3/s3manager"
)

type DownloaderLocalImpl struct {
	store map[string][]byte
}

// NewLocalDownloader creates an in memory S3Manager Downloader.
// The downloads are read from the supplied store map so you can share it with an uploader.
func NewLocalDownloader(store map[string][]byte) *DownloaderLocalImpl {
	return &DownloaderLocalImpl{
		store: store,
	}
}

func (d *DownloaderLocalImpl) DownloadWithIterator(ctx context.Context, iter s3manager.BatchDownloadIterator) error {
	var errs []s3manager.Error
	for iter.Next() {
		object := iter.DownloadObject()

		data, found := d.store[*object.Object.Key]

		if !found {
			s3Err := s3manager.Error{
				OrigErr: errors.New("Not found"),
				Bucket:  object.Object.Bucket,
				Key:     object.Object.Key,
			}
			errs = append(errs, s3Err)
		} else {
			object.Writer.WriteAt(data, 0)
		}

		if object.After == nil {
			continue
		}

		if err := object.After(); err != nil {
			errs = append(errs, s3manager.Error{
				OrigErr: err,
				Bucket:  object.Object.Bucket,
				Key:     object.Object.Key,
			})
		}
	}

	if len(errs) > 0 {
		return s3manager.NewBatchError("BatchedDownloadIncomplete", "some objects have failed to download.", errs)
	}
	return nil
}
