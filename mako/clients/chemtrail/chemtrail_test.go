package chemtrail

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/ce-analytics/chemtrail/rpc/chemtrail"
	chemtrail_client_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/ce-analytics/chemtrail/rpc/chemtrail"
	"github.com/golang/protobuf/ptypes/timestamp"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestChemtrailClient(t *testing.T) {
	Convey("Test Chemtrail Client Wrapper", t, func() {
		mockChemtrailService := new(chemtrail_client_mock.ChemtrailService)
		clientWrapper := clientWrapper{
			Client: mockChemtrailService,
		}

		Convey("Test GetEmoteUsage", func() {
			testReq := &chemtrail.GetEmoteUsageRequest{
				UserId:        "123",
				StartTime:     &timestamp.Timestamp{},
				EndTime:       &timestamp.Timestamp{},
				UsageType:     chemtrail.EmoteUsageType_EMOTE_USAGE_TYPE_ALL,
				EmoteSortBy:   chemtrail.EmoteUsageSortBy_EMOTE_USAGE_SORT_BY_TOTAL_USAGE,
				Sort:          chemtrail.SortOrder_DESC,
				EmoteGroupIds: []string{"abc"},
				Limit:         100,
			}
			Convey("When Chemtrail returns error", func() {
				mockChemtrailService.On("GetEmoteUsage", mock.Anything, mock.Anything).Return(nil, errors.New("test chemtrail error")).Once()
				resp, err := clientWrapper.GetEmoteUsage(context.Background(), testReq)
				So(resp, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})

			Convey("When Chemtrail returns success", func() {
				mockChemtrailService.On("GetEmoteUsage", mock.Anything, mock.Anything).Return(&chemtrail.GetEmoteUsageResponse{}, nil).Once()
				resp, err := clientWrapper.GetEmoteUsage(context.Background(), testReq)
				So(resp, ShouldNotBeNil)
				So(err, ShouldBeNil)
			})
		})
	})
}
