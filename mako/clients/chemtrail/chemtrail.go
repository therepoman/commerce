package chemtrail

import (
	"context"

	lambdaTransport "code.justin.tv/amzn/TwirpGoLangAWSTransports/lambda"
	"code.justin.tv/ce-analytics/chemtrail/rpc/chemtrail"
	"code.justin.tv/commerce/mako/config"
	"github.com/afex/hystrix-go/hystrix"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/lambda"
)

const (
	HystrixNameChemtrailEmoteUsageGet = "chemtrail.emoteusage.GET"
)

type clientWrapper struct {
	Client chemtrail.ChemtrailService
}

type ClientWrapper interface {
	GetEmoteUsage(ctx context.Context, req *chemtrail.GetEmoteUsageRequest) (*chemtrail.GetEmoteUsageResponse, error)
}

func NewChemtrailClient(cfg *config.Configuration) (ClientWrapper, error) {
	awsSession, err := session.NewSession(&aws.Config{
		Region: aws.String("us-west-2"),
	})

	if err != nil {
		return nil, err
	}

	lambdaService := lambda.New(awsSession)

	chemtrailClient := chemtrail.NewChemtrailServiceProtobufClient(
		"", // Chemtrail is a lambda that we execute directly so there is no address.
		lambdaTransport.NewClient(lambdaService, cfg.ChemtrailLambdaARN))

	return &clientWrapper{
		Client: chemtrailClient,
	}, nil
}

func (cc *clientWrapper) GetEmoteUsage(ctx context.Context, req *chemtrail.GetEmoteUsageRequest) (*chemtrail.GetEmoteUsageResponse, error) {
	var resp *chemtrail.GetEmoteUsageResponse

	err := hystrix.DoC(ctx, HystrixNameChemtrailEmoteUsageGet, func(ctx context.Context) error {
		var err error
		resp, err = cc.Client.GetEmoteUsage(ctx, req)
		return err
	}, nil)

	return resp, err
}
