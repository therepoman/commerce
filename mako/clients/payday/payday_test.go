package payday

import (
	"context"
	"errors"
	"testing"

	payday_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/payday/rpc/payday"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	. "github.com/smartystreets/goconvey/convey"
)

func TestPaydayClientWrapper(t *testing.T) {
	Convey("Test Payday Client Wrapper", t, func() {
		mockPaydayClient := new(payday_mock.Payday)
		clientWrapper := clientWrapper{
			PaydayClient: mockPaydayClient,
		}

		ctx := context.Background()
		channelID := "someChannel"

		Convey("Test GetBitsTierEmoteGroups", func() {
			Convey("With error from Payday client", func() {
				mockPaydayClient.On("GetBitsTierEmoteGroups", ctx, &paydayrpc.GetBitsTierEmoteGroupsReq{
					ChannelId: channelID,
				}).Return(nil, errors.New("dummy error"))

				resp, err := clientWrapper.GetBitsTierEmoteGroups(ctx, channelID)

				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})

			Convey("With success from Payday client", func() {
				testResp := &paydayrpc.GetBitsTierEmoteGroupsResp{}
				mockPaydayClient.On("GetBitsTierEmoteGroups", ctx, &paydayrpc.GetBitsTierEmoteGroupsReq{
					ChannelId: channelID,
				}).Return(testResp, nil)

				resp, err := clientWrapper.GetBitsTierEmoteGroups(ctx, channelID)

				So(err, ShouldBeNil)
				So(resp, ShouldResemble, testResp)
			})
		})
	})
}
