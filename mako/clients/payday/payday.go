package payday

import (
	"context"

	"code.justin.tv/commerce/mako/config"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"code.justin.tv/foundation/twitchclient"
	"github.com/afex/hystrix-go/hystrix"
)

const (
	HystrixNamePaydayBitsTierEmoteGroupsGet = "payday.bitstieremotegroups.GET"
)

type ClientWrapper interface {
	GetBitsTierEmoteGroups(ctx context.Context, channelID string) (*paydayrpc.GetBitsTierEmoteGroupsResp, error)
}

type clientWrapper struct {
	PaydayClient paydayrpc.Payday
}

func NewClient(cfg *config.Configuration) ClientWrapper {
	return &clientWrapper{
		PaydayClient: paydayrpc.NewPaydayProtobufClient(cfg.PaydayHost, twitchclient.NewHTTPClient(twitchclient.ClientConf{
			Host: cfg.PaydayHost,
			Transport: twitchclient.TransportConf{
				MaxIdleConnsPerHost: 10,
			},
		})),
	}
}

func (c *clientWrapper) GetBitsTierEmoteGroups(ctx context.Context, channelID string) (*paydayrpc.GetBitsTierEmoteGroupsResp, error) {
	var resp *paydayrpc.GetBitsTierEmoteGroupsResp

	err := hystrix.DoC(ctx, HystrixNamePaydayBitsTierEmoteGroupsGet, func(ctx context.Context) error {
		var err error
		resp, err = c.PaydayClient.GetBitsTierEmoteGroups(ctx, &paydayrpc.GetBitsTierEmoteGroupsReq{ChannelId: channelID})
		return err
	}, nil)

	return resp, err
}
