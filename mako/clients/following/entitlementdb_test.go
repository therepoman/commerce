package following_test

import (
	"testing"

	"code.justin.tv/commerce/mako/hystrix"

	"code.justin.tv/commerce/mako/clients/following"
	"code.justin.tv/commerce/mako/dynamo"
	"code.justin.tv/commerce/mako/localdb"
	"code.justin.tv/commerce/mako/tests"
)

func TestFollowingEntitlementDBWithLocalEmoteGroupDB(t *testing.T) {
	hystrix.ConfigureHystrixCommandsForTest()
	tests.TestFollowingEntitlementDB(t, localdb.MakeEmoteGroupDB, following.MakeFollowingEntitlementDBForTest)
}

func TestFollowingEntitlementDBWithDynamoEmoteGroupDB(t *testing.T) {
	hystrix.ConfigureHystrixCommandsForTest()
	tests.TestFollowingEntitlementDB(t, dynamo.MakeEmoteGroupDBForTesting, following.MakeFollowingEntitlementDBForTest)
}
