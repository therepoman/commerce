package following

import (
	"context"
	"net/http"
	"strings"

	followsrpc "code.justin.tv/amzn/TwitchVXFollowingServiceECSTwirp"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/mako/config"
	"code.justin.tv/commerce/mako/integration/user_utils"
	mako "code.justin.tv/commerce/mako/internal"
	"github.com/afex/hystrix-go/hystrix"
	"github.com/pkg/errors"
	"github.com/twitchtv/twirp"
)

const (
	HystrixNameFollowingGetFollow = "following.follow.GET"
)

type entitlementDB struct {
	client                    followsrpc.TwitchVXFollowingServiceECS
	EmoteGroupDB              mako.EmoteGroupDB
	KnownOrVerifiedBotsConfig config.KnownOrVerifiedBotsConfigClient
}

func NewEntitlementDB(addr string, knownOrVerifiedBotsConfig config.KnownOrVerifiedBotsConfigClient, emoteGroupDB mako.EmoteGroupDB) mako.EntitlementDB {
	return &entitlementDB{
		client: followsrpc.NewTwitchVXFollowingServiceECSProtobufClient(addr, &http.Client{
			Transport: &http.Transport{
				MaxIdleConnsPerHost: 1000,
			},
		}),
		KnownOrVerifiedBotsConfig: knownOrVerifiedBotsConfig,
		EmoteGroupDB:              emoteGroupDB,
	}
}

// This helper function calls Following Service to check whether the ownerID follows the channel, and if they do, checks
// the prod_emote_groups table for what the groupID of this channel's follower emote group is. If there is a groupID, it
// returns a newly constructed fake follower emote group Entitlement for this user in this channel.
func (db *entitlementDB) getFollowerEntitlement(ctx context.Context, ownerID string, channelID string, optionalGroupID *string) (*mako.Entitlement, error) {
	if ownerID == "" || channelID == "" {
		return nil, nil
	}

	var isEntitled bool

	// Every user should count as following themselves, so we don't need to call out to following service
	if ownerID == channelID {
		isEntitled = true
	}

	// Rather than setting up and tearing down Follows in staging environments, we can rely on the unique look of integration test userIDs
	// to mock whether the users are following or not for integration tests
	if strings.HasPrefix(ownerID, user_utils.IntegrationTestUserPrefix) && strings.HasPrefix(channelID, user_utils.IntegrationTestUserPrefix) {
		isEntitled = true
	}

	// TODO: find a way to make determining which bots should be included below in a programmatic way instead of hard-coding
	// For bot users like Nightbot, Streamlabs, etc, we want to allow them to use follower emotes even if they're not
	// explicitly following the channel. Right now this is a hardcoded list, and we assume other bots are small enough
	// that their owners can just make them follow the channels which want to use them.
	if db.KnownOrVerifiedBotsConfig.IsKnownOrVerifiedBot(ownerID) {
		isEntitled = true
	}

	var err error
	if !isEntitled {
		err = hystrix.DoC(ctx, HystrixNameFollowingGetFollow, func(ctx context.Context) error {
			// Call to Following Service will get the follow status particular user and channel.
			res, followingServiceErr := db.client.GetFollow(ctx, &followsrpc.FollowReq{
				FromUserId:   ownerID,
				TargetUserId: channelID,
			})

			if followingServiceErr != nil {
				// Following Service returns a not-found error if the user doesn't follow the channel. In this case, we know
				// the user isn't entitled to the channel, but there shouldn't be any error propagated upward
				twirpErr, ok := followingServiceErr.(twirp.Error)
				if ok {
					if twirpErr.Code() == twirp.NotFound {
						return nil
					}
				}

				return followingServiceErr
			}

			if res != nil {
				isEntitled = true
			}

			return nil
		}, nil)
	}

	if err != nil {
		return nil, err
	}

	// Only return an entitlement if the user "isEntitled", aka the user follows the channel
	if !isEntitled {
		return nil, nil
	}

	// In the ReadByIds case, we are already provided an emote groupID, so there's no reason to look it up again. In the
	// ReadByOwnerID case, we have no groupID to pass in, so we must look it up in the underlying data-store.
	var groupID string
	if optionalGroupID == nil {
		group, err := db.EmoteGroupDB.GetFollowerEmoteGroup(ctx, channelID)
		if err != nil {
			log.WithError(err).WithField("channelID", channelID).Error("could not get follower emote group for entitlements lookup")
			return nil, nil
		}

		// If we don't find a follower emote group, return empty (this channel is not onboarded with follower emotes yet)
		if group == nil {
			return nil, nil
		}

		groupID = group.GroupID
	} else {
		groupID = *optionalGroupID
	}

	// Only return an entitlement if there is a follower emote groupID associated with this channel
	var entitlement mako.Entitlement
	if groupID != "" {
		entitlement = mako.Entitlement{
			ID:        groupID,
			OwnerID:   ownerID,
			Source:    mako.FollowerSource,
			Type:      mako.EmoteGroupEntitlement,
			OriginID:  mako.FollowerOriginID.String(),
			ChannelID: channelID,
			Metadata: mako.EntitlementMetadata{
				GroupID: groupID,
			},
		}
	}

	return &entitlement, nil
}

func (db *entitlementDB) ReadByIDs(ctx context.Context, ownerID string, keys ...string) ([]mako.Entitlement, error) {
	if keys == nil {
		return nil, nil
	}

	// A user can only be on one channel at a time, and we only allow follower emotes in a singular channel
	if len(keys) > 1 {
		return nil, errors.New("follower emotes are only supported in a single channel at a time")
	}
	key := keys[0]

	// In the Follower Emote Builder, we set up the ID as a combination of parts we need for this lookup
	// The first part is a mako.FollowerEntitlementPrefix, the rest are listed below
	keyParts := strings.Split(key, mako.FollowerEntitlementSeparator)
	if len(keyParts) != 3 {
		// We always expect Follower emote entitlement IDs to look like Follower_groupID_channelID, so there should be exactly
		// three parts if we split on the separator. If there aren't three parts, something has gone wrong up the chain.
		return nil, errors.New("invalid follower entitlement id provided")
	}

	groupID := keyParts[1]
	channelID := keyParts[2]

	entitlement, err := db.getFollowerEntitlement(ctx, ownerID, channelID, &groupID)

	if entitlement == nil {
		return nil, err
	}

	// The clients of ReadByIDs expect that the entitlement they get back will have the same ID as they called in for,
	// so we set it here. Note that to avoid having multiple places do the groupID metadata extraction logic from above,
	// we have put the groupID in the meta field of this entitlement
	entitlement.ID = key

	return []mako.Entitlement{*entitlement}, err
}

// At most, we allow one Follower Emote Entitlement per user-channel pair, so we will only ever return 0-1 entitlements here
func (db *entitlementDB) ReadByOwnerID(ctx context.Context, ownerID string, channelID string) ([]mako.Entitlement, error) {
	entitlement, err := db.getFollowerEntitlement(ctx, ownerID, channelID, nil)
	if entitlement == nil {
		return nil, err
	}

	return []mako.Entitlement{*entitlement}, err
}

func (db *entitlementDB) WriteEntitlements(ctx context.Context, entitlements ...mako.Entitlement) error {
	// We do not allow the writing of Follower Only Emote Entitlements in Mako. On a given request, we build the
	// entitlement by looking up the relevant info about whether a user is following a channel in Following Service,
	// and then we combine that data with what we have about the follower emotes and follower emote group for the given
	// channel.
	return errors.New("follower emote entitlements aren't stored anywhere")
}

func MakeFollowingEntitlementDBForTest(client followsrpc.TwitchVXFollowingServiceECS, knownOrVerifiedBotsConfig config.KnownOrVerifiedBotsConfigClient, emoteGroupDB mako.EmoteGroupDB) mako.EntitlementDB {
	return &entitlementDB{
		client:                    client,
		EmoteGroupDB:              emoteGroupDB,
		KnownOrVerifiedBotsConfig: knownOrVerifiedBotsConfig,
	}
}
