package wrapper_test

import (
	"bytes"
	"errors"
	"io/ioutil"
	"testing"

	"code.justin.tv/commerce/mako/clients/wrapper"
	golang_io_mock "code.justin.tv/commerce/mako/mocks/io"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestExtract(t *testing.T) {
	Convey("Test Extract", t, func() {
		Convey("Test ExtractBody", func() {
			Convey("with nil body", func() {
				readCloser, bytes, err := wrapper.ExtractBody(nil)
				So(readCloser, ShouldBeNil)
				So(bytes, ShouldBeNil)
				So(err, ShouldBeNil)
			})

			Convey("with error reading", func() {
				mockReadCloser := new(golang_io_mock.ReadCloser)
				mockReadCloser.On("Read", mock.Anything).Return(0, errors.New("test"))

				readCloser, bytes, err := wrapper.ExtractBody(mockReadCloser)
				So(readCloser, ShouldBeNil)
				So(bytes, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})

			Convey("with successful reading", func() {
				sampleBody := []byte("abcde")
				readCloser := ioutil.NopCloser(bytes.NewBuffer(sampleBody))

				readCloser, body, err := wrapper.ExtractBody(readCloser)
				So(readCloser, ShouldNotBeNil)
				So(err, ShouldBeNil)
				So(bytes.Compare(body, sampleBody), ShouldEqual, 0)
			})
		})
	})
}
