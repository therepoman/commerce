package s3

import (
	"context"

	awsS3 "github.com/aws/aws-sdk-go/service/s3"
)

type IS3Client interface {
	GetObject(ctx context.Context, input *awsS3.GetObjectInput) (*awsS3.GetObjectOutput, error)
	PutObject(ctx context.Context, input *awsS3.PutObjectInput) (*awsS3.PutObjectOutput, error)
	DeleteObject(ctx context.Context, input *awsS3.DeleteObjectInput) (*awsS3.DeleteObjectOutput, error)
	DeleteObjects(ctx context.Context, input *awsS3.DeleteObjectsInput) (*awsS3.DeleteObjectsOutput, error)
	ListObjectsV2WithPages(ctx context.Context, input *awsS3.ListObjectsV2Input, fn func(*awsS3.ListObjectsV2Output, bool) bool) error
}

type S3Impl struct {
	S3Client *awsS3.S3
}

func NewS3(s3client *awsS3.S3) IS3Client {
	return &S3Impl{
		S3Client: s3client,
	}
}

func (s *S3Impl) GetObject(ctx context.Context, input *awsS3.GetObjectInput) (*awsS3.GetObjectOutput, error) {
	return s.S3Client.GetObjectWithContext(ctx, input)
}

func (s *S3Impl) PutObject(ctx context.Context, input *awsS3.PutObjectInput) (*awsS3.PutObjectOutput, error) {
	return s.S3Client.PutObjectWithContext(ctx, input)
}

func (s *S3Impl) DeleteObject(ctx context.Context, input *awsS3.DeleteObjectInput) (*awsS3.DeleteObjectOutput, error) {
	return s.S3Client.DeleteObjectWithContext(ctx, input)
}

func (s *S3Impl) DeleteObjects(ctx context.Context, input *awsS3.DeleteObjectsInput) (*awsS3.DeleteObjectsOutput, error) {
	return s.S3Client.DeleteObjectsWithContext(ctx, input)
}

func (s *S3Impl) ListObjectsV2WithPages(ctx context.Context, input *awsS3.ListObjectsV2Input, fn func(*awsS3.ListObjectsV2Output, bool) bool) error {
	return s.S3Client.ListObjectsV2PagesWithContext(ctx, input, fn)
}
