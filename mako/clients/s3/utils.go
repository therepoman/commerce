package s3

import "fmt"

func GetEmoticonImageUploadS3Key(id string) string {
	return fmt.Sprintf("emoticons/%s", id)
}

func GetEmoticonS3Key(emoticonID string, scale string) string {
	return fmt.Sprintf("emoticon%s-%s.png", emoticonID, scale)
}

func GetModifiedEmoticonS3Key(emoticonID string, scale, modification string) string {
	return fmt.Sprintf("emoticon%s_%s-%s.png", emoticonID, modification, scale)
}

// GetAnimatedFrameFolderS3Key returns the S3 Key for the folder containing animated gif frames.
func GetAnimatedFrameFolderS3Key(emoteID string) string {
	return fmt.Sprintf("emote-%s", emoteID)
}

func GetAnimatedEmoteS3Key(emoteID string, theme string, scale string) string {
	return fmt.Sprintf("emoticon%s-%s-%s.gif", emoteID, theme, scale)
}

func GetDefaultImageLibraryS3Key(id string) string {
	return fmt.Sprintf("%s.png", id)
}
