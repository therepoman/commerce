package pepper

import (
	"context"
	"time"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/foundation/twitchclient"
	"code.justin.tv/sse/malachai/pkg/s2s/caller"
	"github.com/cenkalti/backoff"

	"code.justin.tv/commerce/mako/config"
	peppertwirp "code.justin.tv/revenue/pepper/rpc"
	"github.com/afex/hystrix-go/hystrix"
	"github.com/pkg/errors"
)

const (
	HystrixNamePepperRiskEvaluationGet = "pepper.riskevaluation.GET"
	riskEvaluationMaxRetries           = 1
)

type ClientWrapper interface {
	GetUserRiskEvaluation(ctx context.Context, userID string, evaluationType peppertwirp.EvaluationType) (peppertwirp.EvaluationResult, error)
}

type clientWrapper struct {
	PepperClient peppertwirp.Pepper
}

func NewClient(cfg config.Configuration) (ClientWrapper, error) {
	clientConf := twitchclient.ClientConf{
		Host: cfg.PepperServiceEndpoint,
		Transport: twitchclient.TransportConf{
			MaxIdleConnsPerHost: 10,
		},
	}

	s2sTransport, err := caller.S2STwirpTransport(
		cfg.S2SServiceName,
		nil,
		twitchclient.NewHTTPClient(clientConf),
		logrus.New(),
	)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create S2S transport for Pepper client")
	}

	return &clientWrapper{
		PepperClient: peppertwirp.NewPepperProtobufClient(cfg.PepperServiceEndpoint, s2sTransport),
	}, nil
}

func (c *clientWrapper) GetUserRiskEvaluation(ctx context.Context, userID string, evaluationType peppertwirp.EvaluationType) (peppertwirp.EvaluationResult, error) {
	var resp *peppertwirp.GetUserRiskEvaluationResponse

	f := func() error {
		return hystrix.DoC(ctx, HystrixNamePepperRiskEvaluationGet, func(ctx context.Context) error {
			var err error
			resp, err = c.PepperClient.GetUserRiskEvaluation(ctx, &peppertwirp.GetUserRiskEvaluationRequest{
				UserId:         userID,
				EvaluationType: evaluationType,
			})
			return err
		}, nil)
	}

	b := backoff.WithMaxRetries(backoff.WithContext(newBackoff(), ctx), riskEvaluationMaxRetries)
	err := backoff.Retry(f, b)
	if err != nil {
		return peppertwirp.EvaluationResult_NOT_IN_EMOTE_GOOD_STANDING, err
	}
	if resp == nil {
		return peppertwirp.EvaluationResult_NOT_IN_EMOTE_GOOD_STANDING, errors.Errorf("received nil response from pepper for user %s", userID)
	}

	return resp.Result, err
}

func newBackoff() backoff.BackOff {
	exponential := backoff.NewExponentialBackOff()
	exponential.InitialInterval = 10 * time.Millisecond
	exponential.MaxElapsedTime = 1 * time.Second
	exponential.Multiplier = 2
	return exponential
}
