package pepper

import (
	"context"
	"errors"
	"testing"

	pepper_client_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/revenue/pepper/rpc"
	peppertwirp "code.justin.tv/revenue/pepper/rpc"
	. "github.com/smartystreets/goconvey/convey"
)

func TestPepperClientWrapper(t *testing.T) {
	Convey("Test Pepper Client Wrapper", t, func() {
		mockPepperClient := new(pepper_client_mock.Pepper)
		clientWrapper := clientWrapper{
			PepperClient: mockPepperClient,
		}
		testUserID := "267893713"
		testContext := context.Background()

		Convey("Test GetUserRiskEvaluation", func() {
			Convey("Given pepper returns error for the max number of retries, then return error", func() {
				mockPepperClient.On("GetUserRiskEvaluation", testContext, &peppertwirp.GetUserRiskEvaluationRequest{
					UserId:         testUserID,
					EvaluationType: peppertwirp.EvaluationType_AFFILIATE_EMOTE_GOOD_STANDING,
				}).Return(nil, errors.New("dummy pepper error"))

				resp, err := clientWrapper.GetUserRiskEvaluation(testContext, testUserID, peppertwirp.EvaluationType_AFFILIATE_EMOTE_GOOD_STANDING)
				So(resp, ShouldEqual, peppertwirp.EvaluationResult_NOT_IN_EMOTE_GOOD_STANDING)
				So(err, ShouldNotBeNil)
				So(err.Error(), ShouldContainSubstring, "dummy pepper error")
			})

			Convey("Given pepper returns nil, then return error", func() {
				mockPepperClient.On("GetUserRiskEvaluation", testContext, &peppertwirp.GetUserRiskEvaluationRequest{
					UserId:         testUserID,
					EvaluationType: peppertwirp.EvaluationType_AFFILIATE_EMOTE_GOOD_STANDING,
				}).Return(nil, nil)

				resp, err := clientWrapper.GetUserRiskEvaluation(testContext, testUserID, peppertwirp.EvaluationType_AFFILIATE_EMOTE_GOOD_STANDING)

				So(resp, ShouldEqual, peppertwirp.EvaluationResult_NOT_IN_EMOTE_GOOD_STANDING)
				So(err, ShouldNotBeNil)
				So(err.Error(), ShouldContainSubstring, "received nil response from pepper for user")
			})

			Convey("Given pepper returns a non-nil response successfully immediately, then return the result", func() {
				mockPepperClient.On("GetUserRiskEvaluation", testContext, &peppertwirp.GetUserRiskEvaluationRequest{
					UserId:         testUserID,
					EvaluationType: peppertwirp.EvaluationType_AFFILIATE_EMOTE_GOOD_STANDING,
				}).Return(&peppertwirp.GetUserRiskEvaluationResponse{
					Result: peppertwirp.EvaluationResult_IN_EMOTE_GOOD_STANDING,
				}, nil)

				resp, err := clientWrapper.GetUserRiskEvaluation(testContext, testUserID, peppertwirp.EvaluationType_AFFILIATE_EMOTE_GOOD_STANDING)

				So(resp, ShouldEqual, peppertwirp.EvaluationResult_IN_EMOTE_GOOD_STANDING)
				So(err, ShouldBeNil)
			})

			Convey("Given pepper returns a non-nil response successfully after the max number of retries, then return the result", func() {
				// for the first N-many calls, error, but on the last retry, succeed
				mockPepperClient.On("GetUserRiskEvaluation", testContext, &peppertwirp.GetUserRiskEvaluationRequest{
					UserId:         testUserID,
					EvaluationType: peppertwirp.EvaluationType_AFFILIATE_EMOTE_GOOD_STANDING,
				}).Times(riskEvaluationMaxRetries).Return(nil, errors.New("dummy pepper error"))
				mockPepperClient.On("GetUserRiskEvaluation", testContext, &peppertwirp.GetUserRiskEvaluationRequest{
					UserId:         testUserID,
					EvaluationType: peppertwirp.EvaluationType_AFFILIATE_EMOTE_GOOD_STANDING,
				}).Return(&peppertwirp.GetUserRiskEvaluationResponse{
					Result: peppertwirp.EvaluationResult_IN_EMOTE_GOOD_STANDING,
				}, nil)

				resp, err := clientWrapper.GetUserRiskEvaluation(testContext, testUserID, peppertwirp.EvaluationType_AFFILIATE_EMOTE_GOOD_STANDING)

				So(resp, ShouldEqual, peppertwirp.EvaluationResult_IN_EMOTE_GOOD_STANDING)
				So(err, ShouldBeNil)
			})
		})
	})
}
