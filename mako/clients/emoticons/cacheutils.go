package emoticons

import (
	"fmt"
	"time"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/mako/redis"
)

const (
	defaultCacheTTL = 1 * time.Hour
)

// Emoticon sets cache key for entitlements DB
func EntitlementsDBCacheParams(id string) (string, time.Duration) {
	return fmt.Sprintf("entitlementsdb_emoticon_sets:%s", id), defaultCacheTTL
}

func ResetCache(key string, cache redis.IRedisClient) error {
	var err error

	// Retry up to 5 times. If we fail to invalidate the cache users may never see the new
	// emotes (until this api is called again).
	for i := 0; i < 5; i++ {
		if err = cache.Del(key); err != nil {
			logrus.WithError(err).Errorf("Failed to invalidate Redis cache: %s, retrying", key)
			time.Sleep(100 * time.Millisecond)
			continue
		}

		return nil
	}

	return err
}
