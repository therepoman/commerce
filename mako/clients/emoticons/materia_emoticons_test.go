package emoticons_test

import (
	"context"
	"errors"
	"testing"
	"time"

	materiatwirp "code.justin.tv/amzn/MateriaTwirp"
	. "code.justin.tv/commerce/mako/clients/emoticons"
	"code.justin.tv/commerce/mako/config"
	mako "code.justin.tv/commerce/mako/internal"
	experiments_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/experiments"
	materia_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/materia"
	config_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/config"
	"code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/internal_mock"
	mk "code.justin.tv/commerce/mako/twirp"
	"github.com/golang/protobuf/ptypes/timestamp"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
)

func TestMateriaEmoticons(t *testing.T) {
	Convey("Test MateriaEmoticons", t, func() {
		mockMateria := new(materia_mock.MateriaClient)
		mockConfig := &config.Configuration{}
		mockDynamic := new(config_mock.DynamicConfigClient)
		mockEmoteDB := new(internal_mock.EmoteDB)
		materiaEmoticonsClient := MateriaEmoticons{
			Client:        mockMateria,
			Config:        mockConfig,
			DynamicConfig: mockDynamic,
			EmoteDB:       mockEmoteDB,
		}

		mockDynamic.On("SmilieFix", mock.Anything).Return(false)
		mockDynamic.On("GetGoldenKappaAlwaysOnEnabled", mock.Anything).Return(false)

		Convey("Test EmoticonsByGroup", func() {
			ctx := context.Background()
			testUserID := "1234"
			testEmoteID := "321"
			testEmoteID2 := "4321"
			testGroupID := "17133"
			testGroupID2 := "17144"
			testChannelID := "1234567890"
			testChannelID2 := "0987654321"
			testEmoteCode := "wolfPack"
			testEmoteCode2 := "wolfCub"
			testEmoteType := mk.EmoteGroup_BitsBadgeTierEmotes
			testEmoteType2 := mk.EmoteGroup_Subscriptions

			Convey("success", func() {
				var expectedEmoteLength int

				Convey("given success calling materia and emotedb then emotes are returned", func() {
					res := &materiatwirp.GetEntitlementsResponse{
						Entitlements: []*materiatwirp.Entitlement{
							{
								ItemId: testEmoteID,
								Type:   "emote",
								Meta:   []byte("{\"group_id\": \"17133\"}"),
							},
						},
					}
					mockEmoteDB.On("ReadByIDs", mock.Anything, testEmoteID).Return(nil, nil)
					mockEmoteDB.On("ReadByGroupIDs", mock.Anything, mock.Anything, mock.Anything).Return([]mako.Emote{{ID: testEmoteID, Code: "TestCode", GroupID: testGroupID, State: mako.Active}}, nil)
					mockMateria.On("GetEntitlements", mock.Anything, mock.Anything).Return(res, nil)

					expectedEmoteLength = 1

					emoteSets, err := materiaEmoticonsClient.EmoticonsByGroup(ctx, testUserID)
					So(err, ShouldBeNil)
					So(emoteSets, ShouldNotBeNil)
					So(emoteSets[testGroupID], ShouldNotBeNil)
					So(len(emoteSets[testGroupID].Emotes), ShouldEqual, expectedEmoteLength)
				})

				Convey("given empty metadata in at least one entitlement in materia response then no error is returned", func() {
					groupEmotes := []mako.Emote{
						{ID: testEmoteID, Code: "TestCode", GroupID: testGroupID, State: mako.Active},
						{ID: testEmoteID2, Code: "TestCode2", GroupID: testGroupID, State: mako.Active},
					}

					res := &materiatwirp.GetEntitlementsResponse{
						Entitlements: []*materiatwirp.Entitlement{
							{
								ItemOwnerId: testChannelID,
								ItemId:      testEmoteID,
								Type:        "emote",
							},
							{
								ItemId: testEmoteID2,
								Type:   "emote",
								Meta:   []byte("{\"group_id\": \"17133\"}"),
							},
						},
					}

					mockEmoteDB.On("ReadByIDs", mock.Anything, mock.Anything, mock.Anything).Return(nil, nil)
					mockEmoteDB.On("ReadByGroupIDs", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(groupEmotes, nil)
					mockMateria.On("GetEntitlements", mock.Anything, mock.Anything).Return(res, nil)

					expectedEmoteLength = 2

					emoteSets, err := materiaEmoticonsClient.EmoticonsByGroup(ctx, testUserID)
					So(err, ShouldBeNil)
					So(emoteSets, ShouldNotBeNil)
					So(emoteSets[testGroupID], ShouldNotBeNil)
					So(len(emoteSets[testGroupID].Emotes), ShouldEqual, expectedEmoteLength)
				})

				Convey("given invalid metadata in at least one entitlement in materia response then no error is returned", func() {
					res := &materiatwirp.GetEntitlementsResponse{
						Entitlements: []*materiatwirp.Entitlement{
							{
								ItemId: testEmoteID,
								Type:   "emote",
								Meta:   []byte("not json"),
							},
							{
								ItemId: testEmoteID2,
								Type:   "emote",
								Meta:   []byte("{\"group_id\": \"17133\"}"),
							},
						},
					}
					mockEmoteDB.On("ReadByIDs", mock.Anything, mock.Anything, mock.Anything).Return(nil, nil)
					mockEmoteDB.On("ReadByGroupIDs", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return([]mako.Emote{{ID: testEmoteID, Code: "TestCode", GroupID: testGroupID, State: mako.Active}}, nil)
					mockMateria.On("GetEntitlements", mock.Anything, mock.Anything).Return(res, nil)

					expectedEmoteLength = 1

					emoteSets, err := materiaEmoticonsClient.EmoticonsByGroup(ctx, testUserID)
					So(err, ShouldBeNil)
					So(emoteSets, ShouldNotBeNil)
					So(emoteSets[testGroupID], ShouldNotBeNil)
					So(len(emoteSets[testGroupID].Emotes), ShouldEqual, expectedEmoteLength)
				})

				Convey("given meta.groupID in entitlement in materia", func() {
					metaGroupID := "metaGroupID"
					res := &materiatwirp.GetEntitlementsResponse{
						Entitlements: []*materiatwirp.Entitlement{
							{
								ItemId: testEmoteID,
								Type:   "emote",
								Meta:   []byte("{\"group_id\": \"metaGroupID\"}"),
							},
						},
					}
					mockEmoteDB.On("ReadByIDs", mock.Anything, mock.Anything, mock.Anything).Return(nil, nil)
					mockEmoteDB.On("ReadByGroupIDs", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return([]mako.Emote{{ID: testEmoteID, Code: "TestCode", GroupID: testGroupID, State: mako.Active}}, nil)
					mockMateria.On("GetEntitlements", mock.Anything, mock.Anything).Return(res, nil)

					expectedEmoteLength = 1

					emoteSets, err := materiaEmoticonsClient.EmoticonsByGroup(ctx, testUserID)
					So(err, ShouldBeNil)
					So(emoteSets, ShouldNotBeNil)
					So(emoteSets[metaGroupID], ShouldNotBeNil)
					So(len(emoteSets[metaGroupID].Emotes), ShouldEqual, expectedEmoteLength)
				})

				Convey("given special event (hypetrain) entitlement in materia", func() {
					hypetrainUserID := "hypetrain"
					mockConfig.SpecialEventOwnerIDs = map[string]bool{hypetrainUserID: true}
					res := &materiatwirp.GetEntitlementsResponse{
						Entitlements: []*materiatwirp.Entitlement{
							{
								ItemOwnerId: hypetrainUserID,
								ItemId:      testEmoteID,
								Type:        "emote",
							},
						},
					}
					mockEmoteDB.On("ReadByIDs", mock.Anything, mock.Anything, mock.Anything).Return(nil, nil)
					mockEmoteDB.On("ReadByGroupIDs", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return([]mako.Emote{{ID: testEmoteID, Code: "TestCode", GroupID: testGroupID, State: mako.Active}}, nil)
					mockMateria.On("GetEntitlements", mock.Anything, mock.Anything).Return(res, nil)

					expectedEmoteLength = 1

					emoteSets, err := materiaEmoticonsClient.EmoticonsByGroup(ctx, testUserID)
					So(err, ShouldBeNil)
					So(emoteSets, ShouldNotBeNil)
					So(emoteSets[hypetrainUserID], ShouldNotBeNil)
					So(len(emoteSets[hypetrainUserID].Emotes), ShouldEqual, expectedEmoteLength)
				})

				Convey("with emote group response from materia", func() {
					res := &materiatwirp.GetEntitlementsResponse{
						Entitlements: []*materiatwirp.Entitlement{
							{
								ItemId:      testGroupID,
								ItemOwnerId: testChannelID,
								Type:        "emote_group",
								OwnerId:     testUserID,
							},
						},
					}
					mockMateria.On("GetEntitlements", mock.Anything, mock.Anything).Return(res, nil)

					Convey("given emote db ReadByIDs returns error then return error", func() {
						mockEmoteDB.On("ReadByIDs", mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("dummy error from ReadByIDs"))

						_, err := materiaEmoticonsClient.EmoticonsByGroup(ctx, testUserID)

						mockEmoteDB.AssertExpectations(t)
						So(err, ShouldNotBeNil)
						So(err.Error(), ShouldContainSubstring, "dummy error from ReadByIDs")
					})

					Convey("given emote db ReadByGroupIDs returns error then return error", func() {
						mockEmoteDB.On("ReadByIDs", mock.Anything, mock.Anything).Return(nil, nil)
						mockEmoteDB.On("ReadByGroupIDs", mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("dummy error from ReadByGroupIDs"))

						_, err := materiaEmoticonsClient.EmoticonsByGroup(ctx, testUserID)

						mockEmoteDB.AssertExpectations(t)
						So(err, ShouldNotBeNil)
						So(err.Error(), ShouldContainSubstring, "dummy error from ReadByGroupIDs")
					})

					Convey("given emote db returns empty then return empty", func() {
						mockEmoteDB.On("ReadByIDs", mock.Anything, mock.Anything).Return([]mako.Emote{}, nil)
						mockEmoteDB.On("ReadByGroupIDs", mock.Anything, mock.Anything, mock.Anything).Return([]mako.Emote{}, nil)

						emoteSets, err := materiaEmoticonsClient.EmoticonsByGroup(ctx, testUserID)

						So(err, ShouldBeNil)
						So(len(emoteSets[testGroupID].Emotes), ShouldEqual, 0)
					})

					Convey("given emote db returns emotes then return emote set with those emotes", func() {
						mockEmoteDB.On("ReadByIDs", mock.Anything, mock.Anything).Return([]mako.Emote{}, nil)
						mockEmoteDB.On("ReadByGroupIDs", mock.Anything, mock.Anything, mock.Anything).Return([]mako.Emote{
							{
								ID:      testEmoteID,
								GroupID: testGroupID,
								OwnerID: testChannelID,
								State:   mako.Active,
							},
						}, nil)

						emoteSets, err := materiaEmoticonsClient.EmoticonsByGroup(ctx, testUserID)

						So(err, ShouldBeNil)
						So(emoteSets, ShouldNotBeNil)
						So(emoteSets[testGroupID], ShouldNotBeNil)
						So(len(emoteSets[testGroupID].Emotes), ShouldEqual, 1)
						So(emoteSets[testGroupID].Id, ShouldEqual, testGroupID)
						So(emoteSets[testGroupID].ChannelId, ShouldEqual, testChannelID)
					})

					Convey("given emote db returns active and inactive emotes then return emote set with only active emotes", func() {
						mockEmoteDB.On("ReadByIDs", mock.Anything, mock.Anything).Return([]mako.Emote{}, nil)
						mockEmoteDB.On("ReadByGroupIDs", mock.Anything, mock.Anything, mock.Anything).Return([]mako.Emote{
							{
								ID:          testEmoteID,
								Code:        testEmoteCode,
								GroupID:     testGroupID,
								OwnerID:     testChannelID,
								State:       mako.Active,
								EmotesGroup: testEmoteType.String(),
							},
							{
								ID:          testEmoteID2,
								Code:        testEmoteCode2,
								GroupID:     testGroupID,
								OwnerID:     testChannelID,
								State:       mako.Inactive,
								EmotesGroup: testEmoteType2.String(),
							},
						}, nil)

						emoteSets, err := materiaEmoticonsClient.EmoticonsByGroup(ctx, testUserID)

						So(err, ShouldBeNil)
						So(emoteSets, ShouldNotBeNil)
						So(emoteSets[testGroupID], ShouldNotBeNil)
						So(emoteSets[testGroupID].ChannelId, ShouldEqual, testChannelID)
						So(emoteSets[testGroupID].Id, ShouldEqual, testGroupID)
						So(len(emoteSets[testGroupID].Emotes), ShouldEqual, 1)
						So(emoteSets[testGroupID].Emotes[0].Pattern, ShouldEqual, testEmoteCode)
						So(emoteSets[testGroupID].Emotes[0].Id, ShouldEqual, testEmoteID)
						So(emoteSets[testGroupID].Emotes[0].Type, ShouldEqual, testEmoteType)
					})
				})

				Convey("with mixed emote group and emote response from materia", func() {
					res := &materiatwirp.GetEntitlementsResponse{
						Entitlements: []*materiatwirp.Entitlement{
							{
								ItemId:      testGroupID,
								ItemOwnerId: testChannelID,
								Type:        "emote_group",
								OwnerId:     testUserID,
							},
							{
								ItemId: testEmoteID2,
								Type:   "emote",
								Meta:   []byte("{\"group_id\": \"" + testGroupID2 + "\"}"),
							},
						},
					}
					mockMateria.On("GetEntitlements", mock.Anything, mock.Anything).Return(res, nil)

					Convey("given emote db returns emotes for both entitlements then return emote set with both emotes", func() {
						mockEmoteDB.On("ReadByIDs", mock.Anything, testEmoteID2).Return([]mako.Emote{
							{
								ID:          testEmoteID2,
								GroupID:     testGroupID2,
								OwnerID:     testChannelID2,
								State:       mako.Active,
								Code:        testEmoteCode2,
								EmotesGroup: testEmoteType2.String(),
							},
						}, nil)
						mockEmoteDB.On("ReadByGroupIDs", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return([]mako.Emote{
							{
								ID:          testEmoteID,
								GroupID:     testGroupID,
								OwnerID:     testChannelID,
								State:       mako.Active,
								Code:        testEmoteCode,
								EmotesGroup: testEmoteType.String(),
							},
						}, nil)

						emoteSets, err := materiaEmoticonsClient.EmoticonsByGroup(ctx, testUserID)

						So(err, ShouldBeNil)
						So(emoteSets, ShouldNotBeNil)

						So(emoteSets[testGroupID], ShouldNotBeNil)
						So(emoteSets[testGroupID].ChannelId, ShouldEqual, testChannelID)
						So(emoteSets[testGroupID].Id, ShouldEqual, testGroupID)
						So(len(emoteSets[testGroupID].Emotes), ShouldEqual, 1)
						So(emoteSets[testGroupID].Emotes[0].Pattern, ShouldEqual, testEmoteCode)
						So(emoteSets[testGroupID].Emotes[0].Id, ShouldEqual, testEmoteID)
						So(emoteSets[testGroupID].Emotes[0].Type, ShouldEqual, testEmoteType)

						So(emoteSets[testGroupID2], ShouldNotBeNil)
						So(emoteSets[testGroupID2].ChannelId, ShouldEqual, testGroupID2)
						So(emoteSets[testGroupID2].Id, ShouldEqual, testGroupID2)
						So(len(emoteSets[testGroupID2].Emotes), ShouldEqual, 1)
						So(emoteSets[testGroupID2].Emotes[0].Pattern, ShouldEqual, testEmoteCode2)
						So(emoteSets[testGroupID2].Emotes[0].Id, ShouldEqual, testEmoteID2)
						So(emoteSets[testGroupID2].Emotes[0].Type, ShouldEqual, testEmoteType2)
					})
				})
			})

			Convey("error", func() {
				Convey("with error from materia calls", func() {
					mockMateria.On("GetEntitlements", mock.Anything, mock.Anything).Return(nil, errors.New("woops"))
				})

				_, err := materiaEmoticonsClient.EmoticonsByGroup(ctx, testUserID)
				So(err, ShouldNotBeNil)
			})
		})
	})
}

type emoteMocks struct {
	Materia *materia_mock.MateriaClient
	EmoteDB *internal_mock.EmoteDB
	Dynamic *config_mock.DynamicConfigClient
}

func TestEmotes(t *testing.T) {
	// Note: Globals/smilies are disabled until a mobile fix can be produced.
	normalizedSmilies := mako.MakeNormalizedSmilies()

	tests := []struct {
		scenario string
		test     func(ctx context.Context, t *testing.T, client *MateriaEmoticons, mocks emoteMocks)
	}{
		{
			scenario: "should return globals by default",
			test: func(ctx context.Context, t *testing.T, client *MateriaEmoticons, mocks emoteMocks) {
				mocks.Dynamic.On("SmilieFix", mock.Anything).Return(true)
				mocks.Dynamic.On("GetGoldenKappaAlwaysOnEnabled", mock.Anything).Return(false)

				testUserID := "123"
				globalEmote := mako.Emote{
					ID:      "2",
					Code:    ":(",
					GroupID: "0",
					OwnerID: "twitch",
					State:   mako.Active,
				}

				mocks.EmoteDB.On("ReadByGroupIDs", mock.Anything, "0").Return([]mako.Emote{
					globalEmote,
				}, nil)

				mocks.EmoteDB.On("ReadByIDs", mock.Anything, mock.Anything).Return([]mako.Emote{}, nil)

				// Materia
				mocks.Materia.On("GetEntitlements", mock.Anything, &materiatwirp.GetEntitlementsRequest{
					OwnerId: testUserID,
					Domain:  materiatwirp.Domain_EMOTES,
				}).Return(nil, nil)

				groups, err := client.EmoticonsByGroup(ctx, testUserID)
				require.Nil(t, err)

				require.Equal(t, 1, len(groups))

				global, ok := groups["0"]
				require.True(t, ok)

				require.Equal(t, 1, len(global.Emotes))
			},
		},

		{
			scenario: "should return no duplicates for globals",
			test: func(ctx context.Context, t *testing.T, client *MateriaEmoticons, mocks emoteMocks) {
				mocks.Dynamic.On("SmilieFix", mock.Anything).Return(true)
				mocks.Dynamic.On("GetGoldenKappaAlwaysOnEnabled", mock.Anything).Return(false)

				testUserID := "123"
				globalEmote := mako.Emote{
					ID:      "9",
					Code:    "<3",
					GroupID: "0",
					OwnerID: "twitch",
					State:   mako.Active,
				}

				mocks.EmoteDB.On("ReadByGroupIDs", mock.Anything, "0").Return([]mako.Emote{
					globalEmote,
				}, nil)

				mocks.EmoteDB.On("ReadByIDs", mock.Anything, mock.Anything).Return([]mako.Emote{}, nil)

				// Materia
				mocks.Materia.On("GetEntitlements", mock.Anything, &materiatwirp.GetEntitlementsRequest{
					OwnerId: testUserID,
					Domain:  materiatwirp.Domain_EMOTES,
				}).Return(nil, nil)

				groups, err := client.EmoticonsByGroup(ctx, testUserID)
				require.Nil(t, err)

				require.Equal(t, 1, len(groups))

				global, ok := groups["0"]
				require.True(t, ok)

				require.Equal(t, 1, len(global.Emotes))
			},
		},

		{
			scenario: "should return emotes in order",
			test: func(ctx context.Context, t *testing.T, client *MateriaEmoticons, mocks emoteMocks) {
				mocks.Dynamic.On("SmilieFix", mock.Anything).Return(true)
				mocks.Dynamic.On("GetGoldenKappaAlwaysOnEnabled", mock.Anything).Return(false)

				testUserID := "123"
				globalEmote := mako.Emote{
					ID:      "9",
					Code:    "<3",
					GroupID: "0",
					OwnerID: "twitch",
					State:   mako.Active,
					Order:   1,
				}

				globalEmote2 := mako.Emote{
					ID:      "95",
					Code:    "Kappa",
					GroupID: "0",
					OwnerID: "twitch",
					State:   mako.Active,
					Order:   0,
				}

				globalEmote3 := mako.Emote{
					ID:      "helloworld",
					Code:    "LUL",
					GroupID: "0",
					OwnerID: "twitch",
					State:   mako.Active,
					Order:   2,
				}

				mocks.EmoteDB.On("ReadByGroupIDs", mock.Anything, "0").Return([]mako.Emote{
					globalEmote2,
					globalEmote3,
					globalEmote,
				}, nil)

				mocks.EmoteDB.On("ReadByIDs", mock.Anything, mock.Anything).Return([]mako.Emote{}, nil)

				// Materia
				mocks.Materia.On("GetEntitlements", mock.Anything, &materiatwirp.GetEntitlementsRequest{
					OwnerId: testUserID,
					Domain:  materiatwirp.Domain_EMOTES,
				}).Return(nil, nil)

				groups, err := client.EmoticonsByGroup(ctx, testUserID)
				require.Nil(t, err)

				require.Equal(t, 1, len(groups))

				global, ok := groups["0"]
				require.True(t, ok)

				require.Equal(t, 3, len(global.Emotes))

				// sorted by order field
				require.Equal(t, globalEmote2.ID, global.Emotes[0].Id)
				require.Equal(t, globalEmote.ID, global.Emotes[1].Id)
				require.Equal(t, globalEmote3.ID, global.Emotes[2].Id)
			},
		},

		{
			scenario: "should override globals with smilies",
			test: func(ctx context.Context, t *testing.T, client *MateriaEmoticons, mocks emoteMocks) {
				mocks.Dynamic.On("SmilieFix", mock.Anything).Return(true)
				mocks.Dynamic.On("GetGoldenKappaAlwaysOnEnabled", mock.Anything).Return(false)

				testUserID := "123"
				globalEmote := mako.Emote{
					ID:      "1",
					Code:    ":)",
					GroupID: "0",
					OwnerID: "twitch",
					State:   mako.Active,
				}

				smilieEmote := mako.Emote{
					ID:      "499",
					Code:    ":)",
					GroupID: "42",
					OwnerID: "twitch",
					State:   mako.Active,
				}

				otherEmote := mako.Emote{
					ID:      "smilie5555",
					Code:    "Foobar",
					GroupID: "42",
					OwnerID: "twitch",
					State:   mako.Active,
				}

				mocks.EmoteDB.On("ReadByGroupIDs", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return([]mako.Emote{
					globalEmote,
					smilieEmote,
					otherEmote,
				}, nil)

				mocks.EmoteDB.On("ReadByIDs", mock.Anything, mock.Anything).Return([]mako.Emote{}, nil)

				mocks.Materia.On("GetEntitlements", mock.Anything, &materiatwirp.GetEntitlementsRequest{
					OwnerId: testUserID,
					Domain:  materiatwirp.Domain_EMOTES,
				}).Return(&materiatwirp.GetEntitlementsResponse{
					Entitlements: []*materiatwirp.Entitlement{
						{
							Domain:  materiatwirp.Domain_EMOTES,
							Source:  materiatwirp.Source_SUBS,
							ItemId:  mako.SmilieGroupID,
							Type:    "emote_group",
							Meta:    []byte("{\"group_id\": \"42\"}"),
							OwnerId: testUserID,
						},
					},
				}, nil)

				groups, err := client.EmoticonsByGroup(ctx, testUserID)
				require.Nil(t, err)

				require.Equal(t, 3, len(groups)) // Also includes empty emote Set

				// Emote should be removed from this set.
				global, ok := groups["42"]
				require.True(t, ok)
				require.Equal(t, 1, len(global.Emotes))

				// Global should be overwritten
				global, ok = groups["0"]
				require.True(t, ok)
				require.Equal(t, 1, len(global.Emotes))
				require.Equal(t, smilieEmote.ID, global.Emotes[0].Id)
				require.Equal(t, normalizedSmilies[smilieEmote.Code].Code(), global.Emotes[0].Pattern)
				id, ok := normalizedSmilies[smilieEmote.Code].ID("42")
				require.True(t, ok)
				require.Equal(t, id, global.Emotes[0].Id)
			},
		},

		{
			scenario: "should filter out expired entitlements from Materia",
			test: func(ctx context.Context, t *testing.T, client *MateriaEmoticons, mocks emoteMocks) {
				mocks.Dynamic.On("SmilieFix", mock.Anything).Return(false)
				mocks.Dynamic.On("GetGoldenKappaAlwaysOnEnabled", mock.Anything).Return(false)

				testUserID := "123"
				emote := mako.Emote{
					ID:      "hello123",
					Code:    "Foobar",
					GroupID: "123",
					OwnerID: "twitch",
					State:   mako.Active,
				}

				emoteTwo := mako.Emote{
					ID:      "wut",
					Code:    "Hello",
					GroupID: "222",
					OwnerID: "twitch",
					State:   mako.Active,
				}

				mocks.EmoteDB.On("ReadByGroupIDs", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return([]mako.Emote{
					emoteTwo,
				}, nil)

				mocks.EmoteDB.On("ReadByIDs", mock.Anything, mock.Anything).Return([]mako.Emote{}, nil)

				mocks.Materia.On("GetEntitlements", mock.Anything, &materiatwirp.GetEntitlementsRequest{
					OwnerId: testUserID,
					Domain:  materiatwirp.Domain_EMOTES,
				}).Return(&materiatwirp.GetEntitlementsResponse{
					Entitlements: []*materiatwirp.Entitlement{
						{
							Domain:  materiatwirp.Domain_EMOTES,
							Source:  materiatwirp.Source_SUBS,
							ItemId:  emote.GroupID,
							Type:    "emote_group",
							Meta:    []byte("{}"),
							OwnerId: testUserID,
							EndsAt: &materiatwirp.InfiniteTime{
								Time: &timestamp.Timestamp{
									Seconds: time.Now().Add(-5 * time.Minute).UTC().Unix(),
								},
							},
						},

						{
							Domain:  materiatwirp.Domain_EMOTES,
							Source:  materiatwirp.Source_SUBS,
							ItemId:  "222",
							Type:    "emote_group",
							Meta:    []byte("{}"),
							OwnerId: testUserID,
							EndsAt: &materiatwirp.InfiniteTime{
								Time: &timestamp.Timestamp{
									Seconds: time.Now().Add(5 * time.Minute).UTC().Unix(),
								},
							},
						},
					},
				}, nil)

				groups, err := client.EmoticonsByGroup(ctx, testUserID)
				require.Nil(t, err)

				require.Equal(t, 1, len(groups))

				group, ok := groups["222"]
				require.True(t, ok)
				require.Equal(t, 1, len(group.Emotes))
				require.Equal(t, emoteTwo.ID, group.Emotes[0].Id)
				require.Equal(t, emoteTwo.Code, group.Emotes[0].Pattern)
			},
		},

		{
			scenario: "should dedupe normalized smilies",
			test: func(ctx context.Context, t *testing.T, client *MateriaEmoticons, mocks emoteMocks) {
				mocks.Dynamic.On("SmilieFix", mock.Anything).Return(true)
				mocks.Dynamic.On("GetGoldenKappaAlwaysOnEnabled", mock.Anything).Return(false)

				testUserID := "123"
				globalEmote := mako.Emote{
					ID:      "1",
					Code:    ":)",
					GroupID: "0",
					OwnerID: "twitch",
					State:   mako.Active,
				}

				smilie1Emote := mako.Emote{
					ID:      "495",
					Code:    ":S",
					GroupID: "42",
					OwnerID: "twitch",
					State:   mako.Active,
				}

				smilie2Emote := mako.Emote{
					ID:      "495",
					Code:    ":S",
					GroupID: "42",
					OwnerID: "twitch",
					State:   mako.Active,
				}

				mocks.EmoteDB.On("ReadByGroupIDs", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return([]mako.Emote{
					globalEmote,
					smilie1Emote,
					smilie2Emote,
				}, nil)

				mocks.EmoteDB.On("ReadByIDs", mock.Anything, mock.Anything).Return([]mako.Emote{}, nil)

				mocks.Materia.On("GetEntitlements", mock.Anything, &materiatwirp.GetEntitlementsRequest{
					OwnerId: testUserID,
					Domain:  materiatwirp.Domain_EMOTES,
				}).Return(&materiatwirp.GetEntitlementsResponse{
					Entitlements: []*materiatwirp.Entitlement{
						{
							Domain:  materiatwirp.Domain_EMOTES,
							Source:  materiatwirp.Source_SUBS,
							ItemId:  mako.SmilieGroupID,
							Type:    "emote_group",
							Meta:    []byte("{\"group_id\": \"42\"}"),
							OwnerId: testUserID,
						},
					},
				}, nil)

				groups, err := client.EmoticonsByGroup(ctx, testUserID)
				require.Nil(t, err)

				require.Equal(t, 3, len(groups)) // Also includes empty emote Set

				// Emote should be removed from this set.
				global, ok := groups["42"]
				require.True(t, ok)
				require.Equal(t, 1, len(global.Emotes))

				// Global should be overwritten
				global, ok = groups["42"]
				require.True(t, ok)
				require.Equal(t, 1, len(global.Emotes))
				require.Equal(t, smilie1Emote.ID, global.Emotes[0].Id)
				require.Equal(t, normalizedSmilies[smilie1Emote.Code].Code(), global.Emotes[0].Pattern)
				id, ok := normalizedSmilies[smilie1Emote.Code].ID("42")
				require.True(t, ok)
				require.Equal(t, id, global.Emotes[0].Id)
			},
		},

		{
			scenario: "GIVEN dynamic config for golden kappa returns true THEN golden kappa is returned",
			test: func(ctx context.Context, t *testing.T, client *MateriaEmoticons, mocks emoteMocks) {
				mocks.Dynamic.On("SmilieFix", mock.Anything).Return(true)
				mocks.Dynamic.On("GetGoldenKappaAlwaysOnEnabled", mock.Anything).Return(true)

				testUserID := "123"
				goldenKappa := mako.Emote{
					ID:      mako.GoldenKappaEmoteID,
					Code:    "Kappa",
					GroupID: mako.GoldenKappaGroupID,
					OwnerID: "twitch",
					State:   mako.Active,
				}

				mocks.EmoteDB.On("ReadByGroupIDs", mock.Anything, mock.Anything, mako.GoldenKappaGroupID).Return([]mako.Emote{
					goldenKappa,
				}, nil)

				mocks.EmoteDB.On("ReadByIDs", mock.Anything, mock.Anything).Return([]mako.Emote{}, nil)

				mocks.Materia.On("GetEntitlements", mock.Anything, &materiatwirp.GetEntitlementsRequest{
					OwnerId: testUserID,
					Domain:  materiatwirp.Domain_EMOTES,
				}).Return(&materiatwirp.GetEntitlementsResponse{
					Entitlements: []*materiatwirp.Entitlement{},
				}, nil)

				groups, err := client.EmoticonsByGroup(ctx, testUserID)
				require.Nil(t, err)

				require.Equal(t, 2, len(groups)) // Also includes empty emote Set

				goldenKappaGroup, ok := groups[mako.GoldenKappaGroupID]
				require.True(t, ok)
				require.Equal(t, 1, len(goldenKappaGroup.Emotes))
				require.Equal(t, mako.GoldenKappaEmoteID, goldenKappaGroup.Emotes[0].Id)
				require.Equal(t, "Kappa", goldenKappaGroup.Emotes[0].Pattern)
			},
		},
	}

	for _, test := range tests {
		test := test

		t.Run(test.scenario, func(t *testing.T) {
			mockMateria := new(materia_mock.MateriaClient)
			mockDynamic := new(config_mock.DynamicConfigClient)
			mockExperimentsClient := new(experiments_mock.ExperimentsClient)
			mockEmoteDB := new(internal_mock.EmoteDB)
			client, _ := NewMateria(mockMateria, nil, mockDynamic, mockEmoteDB)

			mockExperimentsClient.On("AreBitsTierEmotesAvailableForUser", mock.Anything).Return(true)

			test.test(context.Background(), t, client, emoteMocks{
				Materia: mockMateria,
				EmoteDB: mockEmoteDB,
				Dynamic: mockDynamic,
			})
		})
	}
}
