package emoticons_test

import (
	"context"
	"reflect"
	"testing"
	"time"

	"code.justin.tv/commerce/mako/clients/emoticons"
	mako "code.justin.tv/commerce/mako/internal"
	emoticons_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/emoticons"
	config_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/config"
	"code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/internal_mock"
	client_emoticons_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/mako/clients/emoticons"
	mk "code.justin.tv/commerce/mako/twirp"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func TestEmoticonAggregator_GetAllEmoticonsByGroup(t *testing.T) {
	Convey("Test GetAllUserEmoticonsByGroup", t, func() {
		emotes1 := map[string][]*mk.Emote{
			"group1": {
				{
					Id:      "emote1",
					Pattern: "pattern1",
				},
				{
					Id:      "emote2",
					Pattern: "pattern2",
				},
			},
			"group2": {
				{
					Id:      "emote3",
					Pattern: "pattern3",
				},
				{
					Id:      "emote4",
					Pattern: "pattern4",
				},
			},
		}
		emotes2 := map[string][]*mk.Emote{
			"group3": {
				{
					Id:      "emote5",
					Pattern: "pattern5",
				},
				{
					Id:      "emote6",
					Pattern: "pattern6",
				},
			},
			"group4": {
				{
					Id:      "emote7",
					Pattern: "pattern7",
				},
				{
					Id:      "emote8",
					Pattern: "pattern8",
				},
			},
		}
		emotes3 := map[string]mk.EmoteSet{
			"group5": {
				Id: "group5",
				Emotes: []*mk.Emote{
					{
						Id:      "emote15",
						Pattern: "pattern445",
					},
					{
						Id:      "emote16",
						Pattern: "pattern56",
					}},
				ChannelId: "1234567890",
			},
		}
		emotes4 := []mako.Emote{
			{
				ID:   "emote17",
				Code: "code1",
			},
			{
				ID:   "emote18",
				Code: "code2",
			},
		}
		now := time.Now()
		emotes5Unsorted := []mako.Emote{
			{
				ID:        "emote19",
				Code:      "code1",
				Order:     3,
				CreatedAt: now,
			},
			{
				ID:        "emote20",
				Code:      "code2",
				Order:     1,
				CreatedAt: now,
			},
			{
				ID:        "emote21",
				Code:      "code2",
				Order:     2,
				CreatedAt: now,
			},
			{
				ID:        "emote22",
				Code:      "code2",
				Order:     2,
				CreatedAt: now.Add(1000),
			},
		}
		emotes5Sorted := []mako.Emote{
			{
				ID:        "emote20",
				Code:      "code2",
				Order:     1,
				CreatedAt: now,
			},
			{
				ID:        "emote22",
				Code:      "code2",
				Order:     2,
				CreatedAt: now.Add(1000),
			},
			{
				ID:        "emote21",
				Code:      "code2",
				Order:     2,
				CreatedAt: now,
			},
			{
				ID:        "emote19",
				Code:      "code1",
				Order:     3,
				CreatedAt: now,
			},
		}
		tests := []struct {
			name                    string
			entitlementsDBEmoticons map[string][]*mk.Emote
			materiaEmoticons        map[string]mk.EmoteSet
			followerEmotes          []mako.Emote
			want                    emoticons.AllEmoticons
			wantErr                 bool
		}{
			{
				name:                    "When materiaEmoticons, entitlementsDBEmoticons, and follower return no emotes then AllEmoticons should include no emotes",
				entitlementsDBEmoticons: map[string][]*mk.Emote{},
				materiaEmoticons:        map[string]mk.EmoteSet{},
				followerEmotes:          []mako.Emote{},
				want: emoticons.AllEmoticons{
					EntitlementsDB: map[string][]*mk.Emote{},
					Materia:        map[string]mk.EmoteSet{},
					FollowerEmotes: []mako.Emote{},
				},
				wantErr: false,
			},
			{
				name:                    "When materiaEmoticons, entitlementsDBEmoticons, and followerEmotes return emotes then AllEmoticons should include all emotes correctly grouped",
				entitlementsDBEmoticons: emotes2,
				materiaEmoticons:        emotes3,
				followerEmotes:          emotes4,
				want: emoticons.AllEmoticons{
					EntitlementsDB: emotes2,
					Materia:        emotes3,
					FollowerEmotes: emotes4,
				},
				wantErr: false,
			},
			{
				name:                    "When just entitlementsDBEmoticons returns emotes then AllEmoticons should include only emotes from entitlementsDB",
				entitlementsDBEmoticons: emotes1,
				materiaEmoticons:        map[string]mk.EmoteSet{},
				followerEmotes:          []mako.Emote{},
				want: emoticons.AllEmoticons{
					EntitlementsDB: emotes1,
					Materia:        map[string]mk.EmoteSet{},
					FollowerEmotes: []mako.Emote{},
				},
				wantErr: false,
			},
			{
				name:                    "When just materiaEmoticons returns emotes then AllEmoticons should include only emotes from entitlementsDB",
				entitlementsDBEmoticons: map[string][]*mk.Emote{},
				materiaEmoticons:        emotes3,
				followerEmotes:          []mako.Emote{},
				want: emoticons.AllEmoticons{
					EntitlementsDB: map[string][]*mk.Emote{},
					Materia:        emotes3,
					FollowerEmotes: []mako.Emote{},
				},
				wantErr: false,
			},
			{
				name:                    "When just followerEmotes returns emotes then AllEmoticons should include only emotes from entitlementsDB",
				entitlementsDBEmoticons: map[string][]*mk.Emote{},
				materiaEmoticons:        map[string]mk.EmoteSet{},
				followerEmotes:          emotes4,
				want: emoticons.AllEmoticons{
					EntitlementsDB: map[string][]*mk.Emote{},
					Materia:        map[string]mk.EmoteSet{},
					FollowerEmotes: emotes4,
				},
				wantErr: false,
			},
			{
				name:                    "Follower emotes should return sorted by Order ascending and CreatedAt descending",
				entitlementsDBEmoticons: map[string][]*mk.Emote{},
				materiaEmoticons:        map[string]mk.EmoteSet{},
				followerEmotes:          emotes5Unsorted,
				want: emoticons.AllEmoticons{
					EntitlementsDB: map[string][]*mk.Emote{},
					Materia:        map[string]mk.EmoteSet{},
					FollowerEmotes: emotes5Sorted,
				},
				wantErr: false,
			},
		}
		for _, tt := range tests {
			t.Run(tt.name, func(t *testing.T) {
				userID := "some_user"
				channelID := "some_channel"

				entitlementsDBEmoticonsMock := new(client_emoticons_mock.IEmoticons)
				materiaEmoticonsMock := new(emoticons_mock.IMateriaEmoticons)
				mockDynamicConfig := new(config_mock.DynamicConfigClient)
				followerEntitlementsDBMock := new(internal_mock.EntitlementDB)
				emotesDBMock := new(internal_mock.EmoteDB)

				entitlementsDBEmoticonsMock.On("EmoticonsByGroup", mock.Anything, mock.Anything).Return(
					tt.entitlementsDBEmoticons, nil,
				)

				materiaEmoticonsMock.On("EmoticonsByGroup", mock.Anything, mock.Anything).Return(
					tt.materiaEmoticons, nil,
				)

				mockDynamicConfig.On("IsInFreemotesExperiment", channelID).Return(true)
				followerEntitlementsDBMock.On("ReadByOwnerID", mock.Anything, userID, channelID).Return([]mako.Entitlement{{ID: "emote-group"}}, nil)
				emotesDBMock.On("ReadByGroupIDs", mock.Anything, "emote-group").Return(tt.followerEmotes, nil)

				e := emoticons.NewEmoticonAggregator(emoticons.EmoticonAggregatorConfig{
					EntitlementsDBEmoticons: entitlementsDBEmoticonsMock,
					MateriaEmoticons:        materiaEmoticonsMock,
					FollowerEntitlementsDB:  followerEntitlementsDBMock,
					EmoteDB:                 emotesDBMock,
					Statter:                 &statsd.NoopClient{},
					DynamicConfig:           mockDynamicConfig,
				})
				got, err := e.GetAllUserEmoticonsByGroup(context.Background(), userID, channelID)
				if (err != nil) != tt.wantErr {
					t.Errorf("emoticonAggregator.GetAllEmoticonsByGroup() error = %v, wantErr %v", err, tt.wantErr)
					return
				}
				if !reflect.DeepEqual(got, tt.want) {
					t.Errorf("emoticonAggregator.GetAllEmoticonsByGroup() = %+v, want %+v", got, tt.want)
				}
			})
		}

		Convey("Getting Follower Emotes", func() {
			Convey("No request is made when channel ID is empty", func() {
				userID := "some_user"
				channelID := ""

				entitlementsDBEmoticonsMock := new(client_emoticons_mock.IEmoticons)
				materiaEmoticonsMock := new(emoticons_mock.IMateriaEmoticons)
				mockDynamicConfig := new(config_mock.DynamicConfigClient)
				followerEntitlementsDBMock := new(internal_mock.EntitlementDB)
				emotesDBMock := new(internal_mock.EmoteDB)

				entitlementsDBEmoticonsMock.On("EmoticonsByGroup", mock.Anything, mock.Anything).Return(
					map[string][]*mk.Emote{}, nil,
				)

				materiaEmoticonsMock.On("EmoticonsByGroup", mock.Anything, mock.Anything).Return(
					map[string]mk.EmoteSet{}, nil,
				)

				e := emoticons.NewEmoticonAggregator(emoticons.EmoticonAggregatorConfig{
					EntitlementsDBEmoticons: entitlementsDBEmoticonsMock,
					MateriaEmoticons:        materiaEmoticonsMock,
					FollowerEntitlementsDB:  followerEntitlementsDBMock,
					EmoteDB:                 emotesDBMock,
					Statter:                 &statsd.NoopClient{},
					DynamicConfig:           mockDynamicConfig,
				})

				got, err := e.GetAllUserEmoticonsByGroup(context.Background(), userID, channelID)
				So(err, ShouldBeNil)
				So(got, ShouldNotBeNil)
				mockDynamicConfig.AssertNotCalled(t, "IsInFreemotesExperiment", mock.Anything)
				followerEntitlementsDBMock.AssertNotCalled(t, "ReadByOwnerID", mock.Anything, mock.Anything, mock.Anything)
				emotesDBMock.AssertNotCalled(t, "ReadByGroupIDs", mock.Anything, mock.Anything)
			})

			Convey("No request is made when channel ID is not in freemotes experiment", func() {
				userID := "some_user"
				channelID := "channel_id"

				entitlementsDBEmoticonsMock := new(client_emoticons_mock.IEmoticons)
				materiaEmoticonsMock := new(emoticons_mock.IMateriaEmoticons)
				mockDynamicConfig := new(config_mock.DynamicConfigClient)
				followerEntitlementsDBMock := new(internal_mock.EntitlementDB)
				emotesDBMock := new(internal_mock.EmoteDB)

				entitlementsDBEmoticonsMock.On("EmoticonsByGroup", mock.Anything, mock.Anything).Return(
					map[string][]*mk.Emote{}, nil,
				)

				materiaEmoticonsMock.On("EmoticonsByGroup", mock.Anything, mock.Anything).Return(
					map[string]mk.EmoteSet{}, nil,
				)

				mockDynamicConfig.On("IsInFreemotesExperiment", channelID).Return(false)

				e := emoticons.NewEmoticonAggregator(emoticons.EmoticonAggregatorConfig{
					EntitlementsDBEmoticons: entitlementsDBEmoticonsMock,
					MateriaEmoticons:        materiaEmoticonsMock,
					FollowerEntitlementsDB:  followerEntitlementsDBMock,
					EmoteDB:                 emotesDBMock,
					Statter:                 &statsd.NoopClient{},
					DynamicConfig:           mockDynamicConfig,
				})

				got, err := e.GetAllUserEmoticonsByGroup(context.Background(), userID, channelID)
				So(err, ShouldBeNil)
				So(got, ShouldNotBeNil)
				followerEntitlementsDBMock.AssertNotCalled(t, "ReadByOwnerID", mock.Anything, mock.Anything, mock.Anything)
				emotesDBMock.AssertNotCalled(t, "ReadByGroupIDs", mock.Anything, mock.Anything)
			})

			Convey("Error when retrieving follower group entitlements does not cause aggregation to fail", func() {
				userID := "some_user"
				channelID := "channel_id"

				entitlementsDBEmoticonsMock := new(client_emoticons_mock.IEmoticons)
				materiaEmoticonsMock := new(emoticons_mock.IMateriaEmoticons)
				mockDynamicConfig := new(config_mock.DynamicConfigClient)
				followerEntitlementsDBMock := new(internal_mock.EntitlementDB)
				emotesDBMock := new(internal_mock.EmoteDB)

				entitlementsDBEmoticonsMock.On("EmoticonsByGroup", mock.Anything, mock.Anything).Return(
					map[string][]*mk.Emote{
						"test1": {
							{
								Id: "test-id-1",
							},
						},
					}, nil,
				)

				materiaEmoticonsMock.On("EmoticonsByGroup", mock.Anything, mock.Anything).Return(
					map[string]mk.EmoteSet{
						"test2": {
							Id: "test-id-2",
						},
					}, nil,
				)

				mockDynamicConfig.On("IsInFreemotesExperiment", channelID).Return(true)

				followerEntitlementsDBMock.On("ReadByOwnerID", mock.Anything, userID, channelID).Return(
					nil,
					errors.New("followerEntitlementsError"),
				)

				e := emoticons.NewEmoticonAggregator(emoticons.EmoticonAggregatorConfig{
					EntitlementsDBEmoticons: entitlementsDBEmoticonsMock,
					MateriaEmoticons:        materiaEmoticonsMock,
					FollowerEntitlementsDB:  followerEntitlementsDBMock,
					EmoteDB:                 emotesDBMock,
					Statter:                 &statsd.NoopClient{},
					DynamicConfig:           mockDynamicConfig,
				})

				got, err := e.GetAllUserEmoticonsByGroup(context.Background(), userID, channelID)
				So(err, ShouldBeNil)
				So(got, ShouldNotBeNil)
				So(got.EntitlementsDB["test1"][0].Id, ShouldEqual, "test-id-1")
				So(got.Materia["test2"].Id, ShouldEqual, "test-id-2")
				So(got.FollowerEmotes, ShouldBeNil)
				followerEntitlementsDBMock.AssertCalled(t, "ReadByOwnerID", mock.Anything, userID, channelID)
				emotesDBMock.AssertNotCalled(t, "ReadByGroupIDs", mock.Anything, mock.Anything)
			})

			Convey("Error when retrieving followeremotes does not cause aggregation to fail", func() {
				userID := "some_user"
				channelID := "channel_id"

				entitlementsDBEmoticonsMock := new(client_emoticons_mock.IEmoticons)
				materiaEmoticonsMock := new(emoticons_mock.IMateriaEmoticons)
				mockDynamicConfig := new(config_mock.DynamicConfigClient)
				followerEntitlementsDBMock := new(internal_mock.EntitlementDB)
				emotesDBMock := new(internal_mock.EmoteDB)

				entitlementsDBEmoticonsMock.On("EmoticonsByGroup", mock.Anything, mock.Anything).Return(
					map[string][]*mk.Emote{
						"test1": {
							{
								Id: "test-id-1",
							},
						},
					}, nil,
				)

				materiaEmoticonsMock.On("EmoticonsByGroup", mock.Anything, mock.Anything).Return(
					map[string]mk.EmoteSet{
						"test2": {
							Id: "test-id-2",
						},
					}, nil,
				)

				mockDynamicConfig.On("IsInFreemotesExperiment", channelID).Return(true)

				followerEntitlementsDBMock.On("ReadByOwnerID", mock.Anything, userID, channelID).Return(
					[]mako.Entitlement{{ID: "emote-group"}},
					nil,
				)

				emotesDBMock.On("ReadByGroupIDs", mock.Anything, "emote-group").Return(
					nil,
					errors.New("errorRetrievingFollowerEmotes"),
				)

				e := emoticons.NewEmoticonAggregator(emoticons.EmoticonAggregatorConfig{
					EntitlementsDBEmoticons: entitlementsDBEmoticonsMock,
					MateriaEmoticons:        materiaEmoticonsMock,
					FollowerEntitlementsDB:  followerEntitlementsDBMock,
					EmoteDB:                 emotesDBMock,
					Statter:                 &statsd.NoopClient{},
					DynamicConfig:           mockDynamicConfig,
				})

				got, err := e.GetAllUserEmoticonsByGroup(context.Background(), userID, channelID)
				So(err, ShouldBeNil)
				So(got, ShouldNotBeNil)
				So(got.EntitlementsDB["test1"][0].Id, ShouldEqual, "test-id-1")
				So(got.Materia["test2"].Id, ShouldEqual, "test-id-2")
				So(got.FollowerEmotes, ShouldBeNil)
				followerEntitlementsDBMock.AssertCalled(t, "ReadByOwnerID", mock.Anything, userID, channelID)
				emotesDBMock.AssertCalled(t, "ReadByGroupIDs", mock.Anything, mock.Anything)
			})
		})
	})

	Convey("Test GetEmoticonsByEmoteIDs", t, func() {
		mockEmoteDB := new(internal_mock.EmoteDB)

		emoticonsAggregatorConfig := emoticons.EmoticonAggregatorConfig{
			EmoteDB: mockEmoteDB,
			Statter: &statsd.NoopClient{},
		}
		emoticonsAggregator := emoticons.NewEmoticonAggregator(emoticonsAggregatorConfig)

		Convey("Given an empty request return an empty slice and emote db is not called", func() {
			resp, err := emoticonsAggregator.GetEmoticonsByEmoteIDs(context.Background(), []string{})

			So(resp, ShouldBeEmpty)
			So(err, ShouldBeNil)
			mockEmoteDB.AssertNotCalled(t, "ReadByIDs", mock.Anything, mock.Anything)
		})

		Convey("Given emote db returns error", func() {
			emoteID := "emotesv2_3e22a0ab6add4a4c9bbb43897d196cdc"
			mockEmoteDB.On("ReadByIDs", mock.Anything, emoteID).Return(nil, errors.New("dummy emote db error"))

			Convey("then return internal error", func() {
				resp, err := emoticonsAggregator.GetEmoticonsByEmoteIDs(context.Background(), []string{emoteID})

				So(resp, ShouldBeNil)
				So(err, ShouldNotBeNil)
				So(err.Error(), ShouldContainSubstring, "dummy emote db error")
			})
		})

		Convey("Given successful emote fetch from emote db", func() {
			queriedIds := []string{"emotesv2_3e22a0ab6add4a4c9bbb43897d196cdc", "emotesv2_3e22a0ab6add4a4c9bbb43897d196cdd"}
			testEmote1 := mako.Emote{
				ID:          queriedIds[0],
				OwnerID:     "267893713",
				GroupID:     "1234567890",
				Prefix:      "wolf",
				Suffix:      "HOWL",
				Code:        "wolfHOWL",
				State:       "active",
				Domain:      "emotes",
				EmotesGroup: "BitsBadgeTierEmotes",
			}
			testEmote2 := mako.Emote{
				ID:          queriedIds[1],
				OwnerID:     "267893713",
				GroupID:     "0987654321",
				Prefix:      "wolf",
				Suffix:      "CUB",
				Code:        "wolfCUB",
				State:       "active",
				Domain:      "emotes",
				EmotesGroup: "BitsBadgeTierEmotes",
			}
			mockEmoteDB.On("ReadByIDs", mock.Anything, queriedIds[0], queriedIds[1]).Return([]mako.Emote{testEmote1, testEmote2}, nil)

			Convey("Then return those emotes", func() {
				resp, err := emoticonsAggregator.GetEmoticonsByEmoteIDs(context.Background(), queriedIds)

				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
				So(len(resp), ShouldEqual, 2)
				if resp[0].ID == testEmote1.ID {
					So(resp[0], ShouldResemble, testEmote1)
					So(resp[1], ShouldResemble, testEmote2)
				} else if resp[0].ID == testEmote2.ID {
					So(resp[1], ShouldResemble, testEmote1)
					So(resp[0], ShouldResemble, testEmote2)
				} else {
					assert.Fail(t, "response from aggregator does not match expected emote")
				}
			})
		})
	})

	Convey("Test GetEmoticonStateMapByEmoteIDsUncached", t, func() {
		mockEmoteDB := new(internal_mock.EmoteDB)

		emoticonsAggregatorConfig := emoticons.EmoticonAggregatorConfig{
			EmoteDB: mockEmoteDB,
			Statter: &statsd.NoopClient{},
		}
		emoticonsAggregator := emoticons.NewEmoticonAggregator(emoticonsAggregatorConfig)

		Convey("Given emotes db returns error", func() {
			emoteID := "emotesv2_3e22a0ab6add4a4c9bbb43897d196cdc"
			mockEmoteDB.On("ReadByIDs", mock.Anything, mock.Anything).Return(nil, errors.New("ERROR"))

			Convey("then return internal error", func() {
				resp, err := emoticonsAggregator.GetEmoticonStateMapByEmoteIDs(context.Background(), []string{emoteID})

				So(resp, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("Given successful emote fetch from emotes db", func() {
			queriedIds := []string{"emotesv2_3e22a0ab6add4a4c9bbb43897d196cdc", "emotesv2_3e22a0ab6add4a4c9bbb43897d196cdd"}
			testEmote1 := mako.Emote{
				ID:    queriedIds[0],
				State: mako.Pending,
			}
			testEmote2 := mako.Emote{
				ID:    queriedIds[1],
				State: mako.Pending,
			}
			mockEmoteDB.On("ReadByIDs", mock.Anything, queriedIds[0], queriedIds[1]).Return([]mako.Emote{testEmote1, testEmote2}, nil)

			Convey("Then return those emotes", func() {
				resp, err := emoticonsAggregator.GetEmoticonStateMapByEmoteIDs(context.Background(), queriedIds)

				So(err, ShouldBeNil)
				So(resp, ShouldResemble, map[string]mako.EmoteState{
					testEmote1.ID: mako.Pending,
					testEmote2.ID: mako.Pending,
				})
			})
		})
	})
}
