package emoticons

import (
	"context"
	"encoding/json"
	"fmt"
	"strings"
	"time"

	"code.justin.tv/commerce/logrus"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/mako/config"
	"code.justin.tv/commerce/mako/dynamo"
	mako "code.justin.tv/commerce/mako/internal"
	"code.justin.tv/commerce/mako/redis"
	"code.justin.tv/commerce/mako/scope"
	mk "code.justin.tv/commerce/mako/twirp"
	mkd "code.justin.tv/commerce/mako/twirp/dashboard"
	"github.com/afex/hystrix-go/hystrix"
	goRedis "github.com/go-redis/redis/v7"
	"github.com/pkg/errors"
)

const (
	LegacyEntitlementHystrix = "legacy.entitlements.GET"
)

// TODO: Migrate all legacy emote (and emote group) entitlements out of Mako's entitlements dynamoDB and into Materia

type IEmoticons interface {
	EmoticonsByGroup(ctx context.Context, id string) (map[string][]*mk.Emote, error)
}

type LegacyEntitlementDB struct {
	emoticons      IEmoticons
	campaignConfig config.CampaignEmoteOwners //used for mapping campaign emotes to their respective channelID
}

func NewLegacyEntitlementDB(emoticons IEmoticons, campaignConfig config.CampaignEmoteOwners) *LegacyEntitlementDB {
	return &LegacyEntitlementDB{
		emoticons:      emoticons,
		campaignConfig: campaignConfig,
	}
}

func (db *LegacyEntitlementDB) ReadByIDs(ctx context.Context, ownerID string, ids ...string) ([]mako.Entitlement, error) {
	entitlements := make([]mako.Entitlement, 0)
	groups := make(map[string][]*mk.Emote)

	err := hystrix.DoC(ctx, LegacyEntitlementHystrix, func(ctx context.Context) error {
		var err error
		groups, err = db.emoticons.EmoticonsByGroup(ctx, ownerID)
		return err
	}, nil)

	if err != nil {
		// Since this is a legacy layer we shouldn't return a hard error.
		logrus.WithError(err).WithField("owner_id", ownerID).Error("ignoring error from reading legacy entitlements by IDs")
		return nil, nil
	}

	for _, emotes := range groups {
		for _, emote := range emotes {
			// Check if the group id was provided as an entitlement in the query. If the group wasn't provided then perhaps an individual emote was.
			if inSlice(emote.GroupId, ids...) {
				// The group was provided and we thus need to return a valid entitlement back.
				entitlements = append(entitlements, mako.Entitlement{
					ID:      emote.GroupId,
					OwnerID: ownerID,
					Source:  mako.RewardsSource,
					Type:    mako.EmoteGroupEntitlement,
					End:     nil,
				})

				continue
			}

			if inSlice(emote.Id, ids...) {
				// The emote was provided and we thus need to return a valid entitlement back.
				entitlements = append(entitlements, mako.Entitlement{
					ID:      emote.Id,
					OwnerID: ownerID,
					Source:  mako.RewardsSource,
					Type:    mako.EmoteEntitlement,
					End:     nil,
				})

				continue
			}
		}
	}

	return entitlements, nil
}

// Legacy entitlements do not support channel-only emote entitlements, so the channelID provided here is not used
func (db *LegacyEntitlementDB) ReadByOwnerID(ctx context.Context, ownerID string, channelID string) ([]mako.Entitlement, error) {
	entitlements := make([]mako.Entitlement, 0)
	groups := make(map[string][]*mk.Emote)

	// While this hystrix call to EmoticonsByGroup gets the entitlements _and_ the emotes, most of the time those emotes
	// should be redis cached, which we want more than just the single dynamo call.
	err := hystrix.DoC(ctx, LegacyEntitlementHystrix, func(ctx context.Context) error {
		var err error
		groups, err = db.emoticons.EmoticonsByGroup(ctx, ownerID)
		return err
	}, nil)

	if err != nil {
		// Since this is a legacy layer we shouldn't return a hard error.
		logrus.WithError(err).WithField("owner_id", ownerID).Error("ignoring error from reading legacy entitlements by ownerID")
		return nil, nil
	}

	for _, emotes := range groups {
		for _, emote := range emotes {
			// All entitlements in mako's dynamoDB are legacy, from before we had the ability to entitle outside of subscriptions.
			// To get around this, we built fake subscription ticket products on qa_tw_partner and then obscured their source on
			// the front-end. We had no need to store this channelID on these entitlements because it would be the same for all of them.
			channelID := mako.LegacyEntitlementsChannelID

			// For some campaigns, the corresponding channelID is stored in a dynamic S3 file. Since legacy entitlementsDB doesn't
			// store channelID, we need a way to look up by emoteID whether there is a channelID override. If not, we can default
			// to qa_tw_partner for the reasons explained above.
			campaignConfigChannelID := db.campaignConfig.CampaignEmoteOwner(emote.Id)
			if campaignConfigChannelID != "" {
				channelID = campaignConfigChannelID
			}

			entitlements = append(entitlements, mako.Entitlement{
				ID:        emote.GroupId,
				OwnerID:   ownerID,
				Source:    toLegacyEmoteSource(emote.Type.String()),
				Type:      mako.EmoteGroupEntitlement,
				OriginID:  mako.LegacyOriginID.String(),
				ChannelID: channelID,
				Start:     time.Time{},
				End:       nil,
				Metadata: mako.EntitlementMetadata{
					GroupID: emote.GroupId,
				},
			})
		}
	}

	entitlements = dedupeEntitlementsByIDs(entitlements)

	return entitlements, nil
}

func (db *LegacyEntitlementDB) WriteEntitlements(ctx context.Context, entitlements ...mako.Entitlement) error {
	return errors.New("legacy entitlementdb is a read-only data source.")
}

func inSlice(id string, ids ...string) bool {
	for _, a := range ids {
		if a == id {
			return true
		}
	}

	return false
}

func NewEntitlementsDBEmoticons(entitlementDAO dynamo.IEntitlementDao, cache redis.IRedisClient, emoteDB mako.EmoteDB, config *config.Configuration) (IEmoticons, error) {
	return &EntitlementsDBEmoticons{
		EntitlementDAO: entitlementDAO,
		RedisCache:     cache,
		EmoteDB:        emoteDB,
		Config:         config,
	}, nil
}

type EntitlementsDBEmoticons struct {
	EntitlementDAO dynamo.IEntitlementDao
	RedisCache     redis.IRedisClient
	EmoteDB        mako.EmoteDB
	Config         *config.Configuration
}

func (ed *EntitlementsDBEmoticons) EmoticonsByGroup(ctx context.Context, id string) (map[string][]*mk.Emote, error) {
	emotesByGroupId := map[string][]*mk.Emote{}
	readFromCache := false

	key, ttl := EntitlementsDBCacheParams(id)
	valBytes, err := ed.RedisCache.Get(key)
	if err == nil {
		// RedisCache hit
		var cachedEmoteGroups map[string][]*mk.Emote
		err = json.Unmarshal(valBytes, &cachedEmoteGroups)
		if err == nil {
			emotesByGroupId = cachedEmoteGroups
			readFromCache = true
		} else {
			log.Errorf("Failure unmarshalling %s from Redis: %s", key, err.Error())
		}
	}

	if !readFromCache {
		// RedisCache miss or error during cache hit processing, do a dynamo fetch to populate cache
		if err != nil && err != goRedis.Nil {
			log.Errorf("Failure retrieving %s from Redis: %s", key, err.Error())
		}

		entitlements, err := ed.EntitlementDAO.Get(id, scope.EmoteSets)
		if err != nil {
			return map[string][]*mk.Emote{}, fmt.Errorf("Error retrieving entitlement from DAO for user: %s, error: %s", id, err)
		}

		var emoteGroupsToRead []string

		// Since we are reading all of the emotes at once by their group ids, we need to keep track of which group ids
		// correspond to which emote type so they can be merged into a single group after the db read.
		isBitsEventEmoteGroup := map[string]bool{}
		bitsEventGroupId := ""
		isCrateEmoteGroup := map[string]bool{}
		crateGroupId := ""
		campaignScopePrefixByGroupId := map[string]string{}
		groupIdsByCampaignScopePrefix := map[string][]string{}

		for _, entitlement := range entitlements {
			entitlementScopedKey, err := scope.Unmarshall(entitlement.EntitlementScopedKey)
			if err != nil {
				return nil, fmt.Errorf("Error unmarshalling: %s, %v", entitlementScopedKey, err)
			}
			entitlementScope := entitlementScopedKey.GetScope()
			if err := scope.ValidateEntitlementScope(entitlementScope); err != nil {
				return nil, fmt.Errorf("Invalid entitlement scope while parsing: %s", entitlementScope)
			}

			if strings.HasPrefix(entitlementScope, scope.EmoteSetsCommerceCrate) {
				crateGroupId = entitlementScopedKey.GetKey()
				isCrateEmoteGroup[crateGroupId] = true
			} else if entitlementScope == scope.EmoteSetsBitsEvents {
				bitsEventGroupId = entitlementScopedKey.GetKey()
				isBitsEventEmoteGroup[bitsEventGroupId] = true
			} else if strings.HasPrefix(entitlementScope, scope.EmoteSetsCommerceCampaignDefault) {
				campaignGroupId := entitlementScopedKey.GetKey()
				campaignScopePrefixByGroupId[campaignGroupId] = entitlementScope
				groupIdsByCampaignScopePrefix[entitlementScope] = append(groupIdsByCampaignScopePrefix[entitlementScope], campaignGroupId)
			}

			emoteGroupsToRead = append(emoteGroupsToRead, entitlementScopedKey.GetKey())
		}

		emotes, err := ed.EmoteDB.ReadByGroupIDs(ctx, emoteGroupsToRead...)
		if err != nil {
			return nil, err
		}
		for _, emote := range emotes {
			if emote.State != mako.Active {
				continue
			}

			twirpEmote := &mk.Emote{
				Id:        emote.ID,
				Pattern:   emote.Code,
				GroupId:   emote.GroupID,
				Order:     emote.Order,
				Type:      mako.GetEmoteType(emote),
				AssetType: mako.EmoteAssetTypeAsTwirp(emote.AssetType),
			}

			// Logic to combine bits, crates and each of the campaign's emotes into a single group each
			if isBitsEventEmoteGroup[emote.GroupID] {
				emotesByGroupId[bitsEventGroupId] = append(emotesByGroupId[bitsEventGroupId], twirpEmote)
			} else if isCrateEmoteGroup[emote.GroupID] {
				emotesByGroupId[crateGroupId] = append(emotesByGroupId[crateGroupId], twirpEmote)
			} else if campaignScope, ok := campaignScopePrefixByGroupId[emote.GroupID]; ok {
				if groupIdsForCampaign := groupIdsByCampaignScopePrefix[campaignScope]; len(groupIdsForCampaign) > 0 {
					emotesByGroupId[groupIdsForCampaign[0]] = append(emotesByGroupId[groupIdsForCampaign[0]], twirpEmote)
				}
			} else {
				emotesByGroupId[emote.GroupID] = append(emotesByGroupId[emote.GroupID], twirpEmote)
			}
		}

		// Add to cache
		if valBytes, err = json.Marshal(emotesByGroupId); err != nil {
			log.Errorf("Error marshalling %s for Redis: %s", key, err.Error())
		} else {
			if err = ed.RedisCache.Set(key, valBytes, ttl); err != nil {
				log.Errorf("Error setting %s into Redis: %s", key, err.Error())
			}
		}
	}

	return emotesByGroupId, nil
}

func dedupeEntitlementsByIDs(entitlements []mako.Entitlement) []mako.Entitlement {
	entitlementMap := map[string]mako.Entitlement{}

	for _, entitlement := range entitlements {
		_, ok := entitlementMap[entitlement.ID]

		if !ok {
			entitlementMap[entitlement.ID] = entitlement
		}
	}

	dedupedEntitlements := make([]mako.Entitlement, 0, len(entitlementMap))
	for _, entitlement := range entitlementMap {
		dedupedEntitlements = append(dedupedEntitlements, entitlement)
	}

	return dedupedEntitlements
}

func toLegacyEmoteSource(emoteType string) mako.EntitlementSource {
	switch emoteType {
	case mkd.EmoteOrigin_MegaCommerce.String():
		return mako.MegaCommerceSource
	default:
		return mako.RewardsSource
	}
}
