package emoticons

import (
	"context"
	"errors"
	"fmt"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/mako/config"
	mako "code.justin.tv/commerce/mako/internal"
	mk "code.justin.tv/commerce/mako/twirp"
	"github.com/cactus/go-statsd-client/statsd"
	errwrap "github.com/pkg/errors"
	"golang.org/x/sync/errgroup"
)

const EmoticonAggregatorMetricScope = "EmoticonAggregator.%s"

type EmoticonAggregator interface {
	GetAllUserEmoticonsByGroup(ctx context.Context, userID, channelID string) (AllEmoticons, error)
	GetEmoticonsByEmoteIDs(ctx context.Context, emoteIDs []string) ([]mako.Emote, error)
	GetEmoticonStateMapByEmoteIDs(ctx context.Context, emoteIDs []string) (map[string]mako.EmoteState, error)
}

// AllEmoticons groups emotes by their source, which is required since emotes from different sources
// may be treated differently. For example: "ChannelID" is determined differently for SiteDB emotes
// and EntitlementsDB emotes.
type AllEmoticons struct {
	EntitlementsDB map[string][]*mk.Emote
	Materia        map[string]mk.EmoteSet
	FollowerEmotes []mako.Emote
}

// emoticonAggregator concurrently retrieves emoticons from all emoticon entitlement sources:
// - EntitlementsDB
// - Materia
// - Followers Service
type EmoticonAggregatorConfig struct {
	EntitlementsDBEmoticons IEmoticons
	MateriaEmoticons        IMateriaEmoticons
	EmoteDB                 mako.EmoteDB
	FollowerEntitlementsDB  mako.EntitlementDB
	Statter                 statsd.Statter
	MateriaDenylist         map[string]bool
	DynamicConfig           config.DynamicConfigClient
}

type emoticonAggregator struct {
	Config EmoticonAggregatorConfig
}

func NewEmoticonAggregator(config EmoticonAggregatorConfig) EmoticonAggregator {
	return &emoticonAggregator{
		Config: config,
	}
}

func (e *emoticonAggregator) GetAllUserEmoticonsByGroup(ctx context.Context, userID, channelID string) (AllEmoticons, error) {
	eg, ctx := errgroup.WithContext(ctx)

	// Get EntitlementsDB emoticon sets
	var entitlementsDBEmoticons map[string][]*mk.Emote
	eg.Go(func() error {
		var err error
		entitlementsDBEmoticons, err = e.Config.EntitlementsDBEmoticons.EmoticonsByGroup(ctx, userID)
		if err != nil {
			log.WithFields(log.Fields{
				"userID": userID,
				"error":  err,
			}).Error("Error retrieving entitlementsDB emoticons")
			return errors.New("Error retrieving entitlement")
		}

		return nil
	})

	// Get emote entitlement from materia
	var materiaEmoticons map[string]mk.EmoteSet
	if !e.IsUserInMateriaDenylist(userID) {
		eg.Go(func() error {
			var err error
			materiaEmoticons, err = e.Config.MateriaEmoticons.EmoticonsByGroup(ctx, userID)
			if err != nil {
				log.WithFields(log.Fields{
					"userID": userID,
					"error":  err,
				}).Error("error retrieving materia emoticons")
				return errors.New("Error retrieving entitlement")
			}

			return nil
		})
	}

	var followerEmotes []mako.Emote
	if channelID != "" && e.Config.DynamicConfig.IsInFreemotesExperiment(channelID) {
		eg.Go(func() error {
			// Follower Emote errors shouldn't result in the request failing. Instead we log errors and send a metric to alarm on.
			var err error
			followerEmoteErrorMetric := "FollowerEmoteError"

			followerEmoteEntitlements, err := e.Config.FollowerEntitlementsDB.ReadByOwnerID(ctx, userID, channelID)
			if err != nil {
				log.WithError(err).WithFields(log.Fields{
					"userID":    userID,
					"channelID": channelID,
				}).Error("Error retrieving follower group entitlements")
				e.emitFollowerServiceMetric(followerEmoteErrorMetric, 1)
				return nil
			} else {
				e.emitFollowerServiceMetric(followerEmoteErrorMetric, 0)
			}

			if len(followerEmoteEntitlements) > 0 {
				followerEmotes, err = e.Config.EmoteDB.ReadByGroupIDs(ctx, followerEmoteEntitlements[0].ID)
				if err != nil {
					log.WithError(err).WithFields(log.Fields{
						"userID":    userID,
						"channelID": channelID,
						"groupID":   followerEmoteEntitlements[0].ID,
					}).Error("Error retrieving follower emotes")
					e.emitFollowerServiceMetric(followerEmoteErrorMetric, 1)
					return nil
				} else {
					e.emitFollowerServiceMetric(followerEmoteErrorMetric, 0)
				}
				mako.SortEmotesByOrderAscendingAndCreatedAtDescending(followerEmotes)
			}

			return nil
		})
	}

	// Wait for all goroutines fetching emotesets to finish then close results channel
	err := eg.Wait()
	if err != nil {
		return AllEmoticons{}, err
	}

	return AllEmoticons{
		EntitlementsDB: entitlementsDBEmoticons,
		Materia:        materiaEmoticons,
		FollowerEmotes: followerEmotes,
	}, nil
}

func (e *emoticonAggregator) GetEmoticonsByEmoteIDs(ctx context.Context, emoteIDs []string) ([]mako.Emote, error) {
	if len(emoteIDs) == 0 {
		return []mako.Emote{}, nil
	}

	emotes, err := e.Config.EmoteDB.ReadByIDs(ctx, emoteIDs...)
	if err != nil {
		log.WithError(err).WithField("emote_ids", emoteIDs).Error("Error fetching emotes")
		return nil, err
	}

	return emotes, nil
}

// Returns a map of emoteIDs:state
func (e *emoticonAggregator) GetEmoticonStateMapByEmoteIDs(ctx context.Context, emoteIDs []string) (map[string]mako.EmoteState, error) {
	emoteStateMap := map[string]mako.EmoteState{}

	emotes, err := e.Config.EmoteDB.ReadByIDs(ctx, emoteIDs...)
	if err != nil {
		return nil, errwrap.Wrap(err, "Error reading emotes for state map")
	}

	for _, emote := range emotes {
		emoteStateMap[emote.ID] = mako.EmoteState(emote.State)
	}

	return emoteStateMap, nil
}

// IsUserInMateriaDenylist checks if a given user is in the configured denylist that
// prevents calls to Materia.
// TODO(SUBS-5172) - This is a temporary hack fix for blocking massive bots from
// causing problems for Materia. This should be removed once Materia caching is in
// place and can handle this load.
func (e *emoticonAggregator) IsUserInMateriaDenylist(userID string) bool {
	_, exists := e.Config.MateriaDenylist[userID]
	return exists
}

func (e *emoticonAggregator) emitFollowerServiceMetric(metric string, value int64) {
	go func() {
		fullMetricKey := fmt.Sprintf(EmoticonAggregatorMetricScope, metric)
		err := e.Config.Statter.Inc(fullMetricKey, value, 1.0)
		if err != nil {
			log.WithError(err).WithField("metric", fullMetricKey).Error("Failed to increment stats")
		}
	}()
}
