package emoticons_test

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"sort"
	"testing"
	"time"

	config_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/config"

	. "code.justin.tv/commerce/mako/clients/emoticons"
	"code.justin.tv/commerce/mako/dynamo"
	mako "code.justin.tv/commerce/mako/internal"
	dynamo_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/dynamo"
	"code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/internal_mock"
	redis_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/redis"
	client_emoticons_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/mako/clients/emoticons"
	"code.justin.tv/commerce/mako/scope"
	mk "code.justin.tv/commerce/mako/twirp"
	"github.com/go-redis/redis"
	goRedis "github.com/go-redis/redis/v7"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
)

const (
	userID = "123"
)

func TestEntitlementsDBEmoticons(t *testing.T) {
	Convey("Test EntitlementsDB Emoticons", t, func() {
		mockCache := new(redis_mock.IRedisClient)
		mockEntitlementDao := new(dynamo_mock.IEntitlementDao)
		mockEmoteDB := new(internal_mock.EmoteDB)
		entitlementsDBEmoticonsClient := EntitlementsDBEmoticons{
			EntitlementDAO: mockEntitlementDao,
			RedisCache:     mockCache,
			EmoteDB:        mockEmoteDB,
		}

		Convey("Test EmoticonByGroup", func() {
			Convey("With entitled emoticons", func() {
				entKey := "21889"
				dynamoResp := []*dynamo.Entitlement{
					{
						UserId:               userID,
						EntitlementScopedKey: fmt.Sprintf("%s%s", scope.EmoteSets, entKey),
						Origin:               "test",
					},
				}
				testEmote := &mk.Emote{Id: "234", Pattern: "Kappa", GroupId: entKey}

				mockCache.On("Get", mock.Anything).Return(nil, redis.Nil)
				mockCache.On("Set", mock.Anything, mock.Anything, mock.Anything).Return(nil)
				mockEntitlementDao.On("Get", mock.Anything, mock.Anything).Return(dynamoResp, nil)
				mockEmoteDB.On("ReadByGroupIDs", mock.Anything, entKey).Return([]mako.Emote{{ID: testEmote.Id, Code: testEmote.Pattern, GroupID: entKey, State: mako.Active}}, nil)

				emoteGroups, err := entitlementsDBEmoticonsClient.EmoticonsByGroup(context.Background(), userID)

				So(err, ShouldBeNil)
				So(emoteGroups, ShouldResemble, map[string][]*mk.Emote{entKey: {testEmote}})
			})

			Convey("With error from emotedb", func() {
				entKey := "21889"
				dynamoResp := []*dynamo.Entitlement{
					{
						UserId:               userID,
						EntitlementScopedKey: fmt.Sprintf("%s%s", scope.EmoteSets, entKey),
						Origin:               "test",
					},
				}

				mockCache.On("Get", mock.Anything).Return(nil, redis.Nil)
				mockCache.On("Set", mock.Anything, mock.Anything, mock.Anything).Return(nil)
				mockEntitlementDao.On("Get", mock.Anything, mock.Anything).Return(dynamoResp, nil)
				mockEmoteDB.On("ReadByGroupIDs", mock.Anything, mock.Anything).Return(nil, errors.New("dummy emotedb error"))

				emoteGroups, err := entitlementsDBEmoticonsClient.EmoticonsByGroup(context.Background(), userID)

				So(err, ShouldNotBeNil)
				So(emoteGroups, ShouldBeNil)
				So(err.Error(), ShouldContainSubstring, "dummy emotedb error")
			})

			Convey("With entitled crate emoticons", func() {
				entKey := "21889"
				entKey2 := "21890"
				dynamoResp := []*dynamo.Entitlement{
					{
						UserId:               userID,
						EntitlementScopedKey: fmt.Sprintf("%s%s", scope.EmoteSetsCommerceCrate, entKey),
						Origin:               "test",
					},
					{
						UserId:               userID,
						EntitlementScopedKey: fmt.Sprintf("%s%s", scope.EmoteSetsCommerceCrate, entKey2),
						Origin:               "test2",
					},
				}

				testEmote := &mk.Emote{Id: "234", Pattern: "Crate1"}
				testEmote2 := &mk.Emote{Id: "456", Pattern: "Crate2"}

				mockCache.On("Get", mock.Anything).Return(nil, redis.Nil)
				mockCache.On("Set", mock.Anything, mock.Anything, mock.Anything).Return(nil)
				mockEntitlementDao.On("Get", mock.Anything, mock.Anything).Return(dynamoResp, nil)
				mockEmoteDB.On("ReadByGroupIDs", mock.Anything, entKey, entKey2).Return([]mako.Emote{
					{ID: testEmote.Id, Code: testEmote.Pattern, GroupID: entKey, State: mako.Active},
					{ID: testEmote2.Id, Code: testEmote2.Pattern, GroupID: entKey2, State: mako.Active},
				}, nil)

				emoteGroups, err := entitlementsDBEmoticonsClient.EmoticonsByGroup(context.Background(), userID)

				So(err, ShouldBeNil)
				So(len(emoteGroups), ShouldEqual, 1)
			})

			Convey("With entitled bits emoticons", func() {
				entKey := "21889"
				entKey2 := "21890"
				dynamoResp := []*dynamo.Entitlement{
					{
						UserId:               userID,
						EntitlementScopedKey: fmt.Sprintf("%s%s", scope.EmoteSetsBitsEvents, entKey),
						Origin:               "test",
					},
					{
						UserId:               userID,
						EntitlementScopedKey: fmt.Sprintf("%s%s", scope.EmoteSetsBitsEvents, entKey2),
						Origin:               "test2",
					},
				}

				testEmote := &mk.Emote{Id: "234", Pattern: "Bits1"}
				testEmote2 := &mk.Emote{Id: "456", Pattern: "Bits2"}

				mockCache.On("Get", mock.Anything).Return(nil, redis.Nil)
				mockCache.On("Set", mock.Anything, mock.Anything, mock.Anything).Return(nil)
				mockEntitlementDao.On("Get", mock.Anything, mock.Anything).Return(dynamoResp, nil)
				mockEmoteDB.On("ReadByGroupIDs", mock.Anything, entKey, entKey2).Return([]mako.Emote{
					{ID: testEmote.Id, Code: testEmote.Pattern, GroupID: entKey, State: mako.Active},
					{ID: testEmote2.Id, Code: testEmote2.Pattern, GroupID: entKey2, State: mako.Active},
				}, nil)

				emoteGroups, err := entitlementsDBEmoticonsClient.EmoticonsByGroup(context.Background(), userID)

				So(err, ShouldBeNil)
				So(len(emoteGroups), ShouldEqual, 1)
			})

			Convey("With entitled campaign emoticons", func() {
				entKey := "21889"
				entKey2 := "21890"
				campaignScope := scope.EmoteSetsCommerceCampaignDefault + "campaignDomain/"
				dynamoResp := []*dynamo.Entitlement{
					{
						UserId:               userID,
						EntitlementScopedKey: fmt.Sprintf("%s%s", campaignScope, entKey),
						Origin:               "test",
					},
					{
						UserId:               userID,
						EntitlementScopedKey: fmt.Sprintf("%s%s", campaignScope, entKey2),
						Origin:               "test2",
					},
				}

				testEmote := &mk.Emote{Id: "234", Pattern: "Campaign1"}
				testEmote2 := &mk.Emote{Id: "456", Pattern: "Campaign2"}

				mockCache.On("Get", mock.Anything).Return(nil, redis.Nil)
				mockCache.On("Set", mock.Anything, mock.Anything, mock.Anything).Return(nil)
				mockEntitlementDao.On("Get", mock.Anything, mock.Anything).Return(dynamoResp, nil)
				mockEmoteDB.On("ReadByGroupIDs", mock.Anything, entKey, entKey2).Return([]mako.Emote{
					{ID: testEmote.Id, Code: testEmote.Pattern, GroupID: entKey, State: mako.Active},
					{ID: testEmote2.Id, Code: testEmote2.Pattern, GroupID: entKey2, State: mako.Active},
				}, nil)

				emoteGroups, err := entitlementsDBEmoticonsClient.EmoticonsByGroup(context.Background(), userID)

				So(err, ShouldBeNil)
				So(len(emoteGroups), ShouldEqual, 1)
			})

			Convey("With entitled campaign emoticons from more than one campaign", func() {
				entKey := "21889"
				entKey2 := "21890"
				campaignScope := scope.EmoteSetsCommerceCampaignDefault + "campaignDomain/"
				campaignScope2 := scope.EmoteSetsCommerceCampaignDefault + "campaignDomain2/"
				dynamoResp := []*dynamo.Entitlement{
					{
						UserId:               userID,
						EntitlementScopedKey: fmt.Sprintf("%s%s", campaignScope, entKey),
						Origin:               "test",
					},
					{
						UserId:               userID,
						EntitlementScopedKey: fmt.Sprintf("%s%s", campaignScope2, entKey2),
						Origin:               "test2",
					},
				}

				testEmote := &mk.Emote{Id: "234", Pattern: "Campaign1"}
				testEmote2 := &mk.Emote{Id: "456", Pattern: "Campaign2"}

				mockCache.On("Get", mock.Anything).Return(nil, redis.Nil)
				mockCache.On("Set", mock.Anything, mock.Anything, mock.Anything).Return(nil)
				mockEntitlementDao.On("Get", mock.Anything, mock.Anything).Return(dynamoResp, nil)
				mockEmoteDB.On("ReadByGroupIDs", mock.Anything, entKey, entKey2).Return([]mako.Emote{
					{ID: testEmote.Id, Code: testEmote.Pattern, GroupID: entKey, State: mako.Active},
					{ID: testEmote2.Id, Code: testEmote2.Pattern, GroupID: entKey2, State: mako.Active},
				}, nil)

				emoteGroups, err := entitlementsDBEmoticonsClient.EmoticonsByGroup(context.Background(), userID)

				So(err, ShouldBeNil)
				So(len(emoteGroups), ShouldEqual, 2)
			})

			Convey("With entitled emoticons and Redis cache hit", func() {
				entKey := "21889"
				testEmote := &mk.Emote{Id: "234", Pattern: "Kappa"}
				valBytes, _ := json.Marshal(map[string][]*mk.Emote{entKey: {testEmote}})

				mockCache.On("Get", mock.Anything).Return(valBytes, nil)

				emoteGroups, err := entitlementsDBEmoticonsClient.EmoticonsByGroup(context.Background(), userID)

				So(err, ShouldBeNil)
				So(emoteGroups, ShouldResemble, map[string][]*mk.Emote{entKey: {testEmote}})
			})

			Convey("With entitled emoticons and Redis cache hit and Redis error", func() {
				entKey := "21889"
				dynamoResp := []*dynamo.Entitlement{
					{
						UserId:               userID,
						EntitlementScopedKey: fmt.Sprintf("%s%s", scope.EmoteSets, entKey),
						Origin:               "test",
					},
				}
				testEmote := &mk.Emote{Id: "234", Pattern: "Kappa", GroupId: entKey}

				mockCache.On("Get", mock.Anything).Return(nil, goRedis.Nil)
				mockCache.On("Set", mock.Anything, mock.Anything, mock.Anything).Return(nil)
				mockEntitlementDao.On("Get", mock.Anything, mock.Anything).Return(dynamoResp, nil)
				mockEmoteDB.On("ReadByGroupIDs", mock.Anything, entKey).Return([]mako.Emote{{ID: testEmote.Id, Code: testEmote.Pattern, GroupID: entKey, State: mako.Active}}, nil)

				emoteGroups, err := entitlementsDBEmoticonsClient.EmoticonsByGroup(context.Background(), userID)

				So(err, ShouldBeNil)
				So(emoteGroups, ShouldResemble, map[string][]*mk.Emote{entKey: {testEmote}})
			})
		})
	})
}

func TestLegacyEntitlementDB_ReadByOwnerID(t *testing.T) {
	testOwnerID := "123456789"
	testChannelID := "234567890"

	tests := []struct {
		scenario         string
		emoticonsByGroup map[string][]*mk.Emote
		expected         []mako.Entitlement
		expectedErr      error
	}{
		{
			scenario: "For a group emote entitlement of a single emote",
			emoticonsByGroup: map[string][]*mk.Emote{
				"456": {
					{
						Id:        "123",
						Pattern:   "testCode1",
						Modifiers: nil,
						GroupId:   "456",
						AssetType: mk.EmoteAssetType_static,
						Type:      mk.EmoteGroup_MegaCommerce,
						Order:     0,
					},
				},
			},
			expected: []mako.Entitlement{
				{
					ID:        "456",
					OwnerID:   testOwnerID,
					Source:    mako.MegaCommerceSource,
					Type:      mako.EmoteGroupEntitlement,
					OriginID:  mako.LegacyOriginID.String(),
					ChannelID: mako.LegacyEntitlementsChannelID,
					Start:     time.Time{},
					Metadata: mako.EntitlementMetadata{
						GroupID: "456",
					},
				},
			},
			expectedErr: nil,
		},

		{
			scenario: "For a group emote entitlement of a group of emotes",
			emoticonsByGroup: map[string][]*mk.Emote{
				"456": {
					{
						Id:        "123",
						Pattern:   "testCode1",
						Modifiers: nil,
						GroupId:   "456",
						AssetType: mk.EmoteAssetType_static,
						Type:      mk.EmoteGroup_MegaCommerce,
						Order:     0,
					},
					{
						Id:        "234",
						Pattern:   "testCode2",
						Modifiers: nil,
						GroupId:   "456",
						AssetType: mk.EmoteAssetType_static,
						Type:      mk.EmoteGroup_MegaCommerce,
						Order:     1,
					},
				},
			},
			expected: []mako.Entitlement{
				{
					ID:        "456",
					OwnerID:   testOwnerID,
					Source:    mako.MegaCommerceSource,
					Type:      mako.EmoteGroupEntitlement,
					OriginID:  mako.LegacyOriginID.String(),
					ChannelID: mako.LegacyEntitlementsChannelID,
					Start:     time.Time{},
					Metadata: mako.EntitlementMetadata{
						GroupID: "456",
					},
				},
			},
			expectedErr: nil,
		},

		{
			scenario: "For a group entitlement of a single emote and a group entitlement of a group of emotes",
			emoticonsByGroup: map[string][]*mk.Emote{
				"789": {
					{
						Id:        "234",
						Pattern:   "testCode2",
						Modifiers: nil,
						GroupId:   "789",
						AssetType: mk.EmoteAssetType_static,
						Type:      mk.EmoteGroup_MegaCommerce,
						Order:     0,
					},
				},
				"69420": {
					{
						Id:        "345",
						Pattern:   "testCode3",
						Modifiers: nil,
						GroupId:   "69420",
						AssetType: mk.EmoteAssetType_static,
						Type:      mk.EmoteGroup_MegaCommerce,
						Order:     0,
					},
					{
						Id:        "567",
						Pattern:   "testCode4",
						Modifiers: nil,
						GroupId:   "69420",
						AssetType: mk.EmoteAssetType_static,
						Type:      mk.EmoteGroup_MegaCommerce,
						Order:     0,
					},
					{
						Id:        "8910",
						Pattern:   "testCode5",
						Modifiers: nil,
						GroupId:   "69420",
						AssetType: mk.EmoteAssetType_static,
						Type:      mk.EmoteGroup_MegaCommerce,
						Order:     0,
					},
				},
			},
			expected: []mako.Entitlement{
				{
					ID:        "789",
					OwnerID:   testOwnerID,
					Source:    mako.MegaCommerceSource,
					Type:      mako.EmoteGroupEntitlement,
					OriginID:  mako.LegacyOriginID.String(),
					ChannelID: mako.LegacyEntitlementsChannelID,
					Start:     time.Time{},
					Metadata: mako.EntitlementMetadata{
						GroupID: "789",
					},
				},
				{
					ID:        "69420",
					OwnerID:   testOwnerID,
					Source:    mako.MegaCommerceSource,
					Type:      mako.EmoteGroupEntitlement,
					OriginID:  mako.LegacyOriginID.String(),
					ChannelID: mako.LegacyEntitlementsChannelID,
					Start:     time.Time{},
					Metadata: mako.EntitlementMetadata{
						GroupID: "69420",
					},
				},
			},
			expectedErr: nil,
		},
	}

	for _, test := range tests {
		t.Run(test.scenario, func(t *testing.T) {
			mockEntitlementsDBEmoticons := new(client_emoticons_mock.IEmoticons)
			mockCampaignEmoteOwners := new(config_mock.CampaignEmoteOwners)

			e := NewLegacyEntitlementDB(mockEntitlementsDBEmoticons, mockCampaignEmoteOwners)

			mockEntitlementsDBEmoticons.On("EmoticonsByGroup", mock.Anything, mock.Anything).Return(
				test.emoticonsByGroup, nil,
			)

			mockCampaignEmoteOwners.On("CampaignEmoteOwner", mock.Anything).Return("")

			resp, err := e.ReadByOwnerID(context.Background(), testOwnerID, testChannelID)

			requireEntitlements(t, test.expected, resp)
			require.Equal(t, err, test.expectedErr)
		})

	}
}

func requireEntitlements(t *testing.T, expected []mako.Entitlement, actual []mako.Entitlement) {
	require.Equal(t, len(expected), len(actual))

	sortByIDAscending(expected)
	sortByIDAscending(actual)

	for i := range expected {
		require.Equal(t, expected[i].ID, actual[i].ID)
		require.Equal(t, expected[i].Type, actual[i].Type)
		require.Equal(t, expected[i].Source, actual[i].Source)
		require.Equal(t, expected[i].ChannelID, actual[i].ChannelID)
		require.Equal(t, expected[i].OriginID, actual[i].OriginID)
		require.Equal(t, expected[i].Metadata.GroupID, actual[i].Metadata.GroupID)
	}

}

func sortByIDAscending(slice []mako.Entitlement) {
	sort.Slice(slice, func(i, j int) bool {
		if slice[i].ID < slice[j].ID {
			return true
		}
		return false
	})
}
