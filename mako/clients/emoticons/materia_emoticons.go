package emoticons

import (
	"context"
	"encoding/json"
	"errors"
	"strconv"
	"time"

	materiatwirp "code.justin.tv/amzn/MateriaTwirp"
	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/mako/clients/materia"
	"code.justin.tv/commerce/mako/config"
	mako "code.justin.tv/commerce/mako/internal"
	mk "code.justin.tv/commerce/mako/twirp"
	"github.com/afex/hystrix-go/hystrix"
	"google.golang.org/protobuf/types/known/timestamppb"
)

const (
	// HystrixNameMateriaEntitlementsGet is the identifier for the hystrix circuit for entitlements GET from Materia
	HystrixNameMateriaEntitlementsGet string = "materia.entitlements.GET"
)

type IMateriaEmoticons interface {
	EmoticonsByGroup(ctx context.Context, ownerID string) (map[string]mk.EmoteSet, error)
}

type MateriaEmoticons struct {
	Client            materia.MateriaClient
	Config            *config.Configuration
	DynamicConfig     config.DynamicConfigClient
	EmoteDB           mako.EmoteDB
	normalizedSmilies map[string]mako.Normalize
}

func NewMateria(materia materia.MateriaClient, cfg *config.Configuration, dynamicConfig config.DynamicConfigClient, emoteDB mako.EmoteDB) (*MateriaEmoticons, error) {
	return &MateriaEmoticons{
		Client:            materia,
		Config:            cfg,
		DynamicConfig:     dynamicConfig,
		EmoteDB:           emoteDB,
		normalizedSmilies: mako.MakeNormalizedSmilies(),
	}, nil
}

func (mat *MateriaEmoticons) readEntitlements(ctx context.Context, id string) (entitlements []*materiatwirp.Entitlement, err error) {
	err = hystrix.DoC(ctx, HystrixNameMateriaEntitlementsGet, func(ctx context.Context) error {
		// Call to Materia will read **all** emoticons for a particular owner/user. Each entitlement in Materia
		// is grouped by "source" and thus every source will be returned.
		res, err := mat.Client.GetEntitlements(ctx, &materiatwirp.GetEntitlementsRequest{
			OwnerId: id,
			Domain:  materiatwirp.Domain_EMOTES,
		})

		if err == nil && res != nil {
			entitlements = res.Entitlements
		}

		return err
	}, nil)

	return
}

func (mat *MateriaEmoticons) EmoticonsByGroup(ctx context.Context, ownerID string) (map[string]mk.EmoteSet, error) {
	if mat.DynamicConfig == nil {
		return map[string]mk.EmoteSet{}, nil
	}

	// Smilie fix is only loaded if the dynamic config allowlisted specific users or bucketed users in a rollout.
	smilieFix := mat.DynamicConfig.SmilieFix(ownerID)

	var entitlements []*materiatwirp.Entitlement
	var err error

	entitlements, err = mat.readEntitlements(ctx, ownerID)
	if err != nil {
		return nil, err
	}

	alreadyAdded := make(map[string]bool)
	emoteGroups := make(map[string][]string)
	emoteChannelsByGroupID := make(map[string]string)
	groupIDByEmoteID := make(map[string]string)

	// Add global emotes to the set so we load them at the end.
	if smilieFix {
		emoteGroups[strconv.Itoa(mako.DefaultEmoticonSet)] = []string{}
	}

	// Global overrides are used for smilies. If any smilies are loaded then we add them to this queue
	// so we can replace any global emotes with the smilie alternative.
	globalOverrides := make([]string, 0)

	for _, entitlement := range entitlements {
		// Mako's entitlement cache can be stale compared to what is stored in Materia so there needs to be
		// a check that ensures the TTL of the entitlement has not expired.
		if entitlement.EndsAt != nil && !entitlement.EndsAt.IsInfinite && entitlement.EndsAt.Time != nil {
			endsAt := time.Unix(entitlement.EndsAt.Time.Seconds, 0).UTC()

			if endsAt.Before(time.Now().UTC()) {
				continue
			}
		}

		if _, found := alreadyAdded[entitlement.ItemId]; !found {
			alreadyAdded[entitlement.ItemId] = true

			switch entitlement.Type {
			case "emote_group":
				var meta materia.EmoteEntitlementMetadata

				if len(entitlement.Meta) > 0 {
					if err := json.Unmarshal(entitlement.Meta, &meta); err != nil {
						logrus.WithError(err).WithFields(logrus.Fields{
							"item_id": entitlement.ItemId,
							"meta":    entitlement.Meta,
							"source":  entitlement.Source,
						}).Error("failed to unmarshal meta field of materia entitlement")
					}
				}

				// NOTE: Mobile cannot currently support the new global/smilies so we need to disable loading smilies.
				if smilieFix {
					primeEmoteSetID := strconv.Itoa(mako.PrimeEmoticonSet)
					turboEmoteSetID := strconv.Itoa(mako.TurboEmoticonSet)

					// We need to take any emotes in these emote sets and replace any default global emotes if the emote
					// code/pattern match. The global overrides will happen at the end of this entire flow, after all the sets
					// have all the emotes fetched.
					if entitlement.ItemId == primeEmoteSetID || entitlement.ItemId == turboEmoteSetID {
						globalOverrides = append(globalOverrides, entitlement.ItemId)
					}

					// Turbo has the concept of Smilies that allow users to customize global emotes with a user setting
					// that configures which style of emotes to use (robot, monkey, etc...). If the meta's GroupID field
					// is set that means the user has configured a non-default Smilies and we need to fetch that group's
					// emotes (because it's a weird freaking group).
					if entitlement.ItemId == mako.SmilieGroupID && meta.GroupID != "" {
						emoteGroups[meta.GroupID] = []string{}
						globalOverrides = append(globalOverrides, meta.GroupID)
					}
				}
				// END OF NOTE

				emoteGroups[entitlement.ItemId] = []string{}
				emoteChannelsByGroupID[entitlement.ItemId] = entitlement.ItemOwnerId
			case "emote":
				itemOwnerID := entitlement.ItemOwnerId
				// since the Meta field can be empty, but the twirp type is []byte, it is possible for Meta to be nil.
				// Therefore, we check length to cover both nil and empty case before calling Unmarshal
				if len(entitlement.Meta) > 0 {
					var meta materia.EmoteEntitlementMetadata
					if err := json.Unmarshal(entitlement.Meta, &meta); err != nil {
						logrus.WithError(err).WithFields(logrus.Fields{
							"item_id": entitlement.ItemId,
							"meta":    entitlement.Meta,
							"source":  entitlement.Source,
						}).Error("failed to unmarshal meta field of materia entitlement")
					}

					if meta.GroupID != "" {
						itemOwnerID = meta.GroupID
						groupIDByEmoteID[entitlement.ItemId] = itemOwnerID
					}
				}
				// If the entitlement belongs to a special event user (HypeTrain, MegaCommerce)
				//  then the emotes queried later should be grouped by the itemOwnerID.
				if mat.Config.SpecialEventOwnerIDs[itemOwnerID] {
					groupIDByEmoteID[entitlement.ItemId] = itemOwnerID
				}
				emoteChannelsByGroupID[itemOwnerID] = itemOwnerID
				emoteGroups[itemOwnerID] = append(emoteGroups[itemOwnerID], entitlement.ItemId)
			}
		}
	}

	result := make(map[string]mk.EmoteSet)
	var emoteIdsToRead []string
	var emoteGroupIdsToRead = []string{}

	for emoteGroupId, emoteIds := range emoteGroups {
		emoteIdsToRead = append(emoteIdsToRead, emoteIds...)
		emoteGroupIdsToRead = append(emoteGroupIdsToRead, emoteGroupId)
	}

	// Add GoldenKappa to the response if the user is a lucky memer
	if mako.IsLuckyGoldenKappa(ownerID, mat.DynamicConfig) {
		emoteGroupIdsToRead = append(emoteGroupIdsToRead, mako.GoldenKappaGroupID)
	}

	emotesFromIds, err := mat.EmoteDB.ReadByIDs(ctx, emoteIdsToRead...)
	if err != nil {
		return nil, err
	}

	emotesFromGroupIds, err := mat.EmoteDB.ReadByGroupIDs(ctx, emoteGroupIdsToRead...)
	if err != nil {
		return nil, err
	}

	emotesByGroupId := make(map[string][]*mk.Emote)
	dedupeMapByEmoteId := make(map[string]bool)
	var allEmotesFromDB []mako.Emote

	// Filter down to only active emotes
	for _, emote := range append(emotesFromIds, emotesFromGroupIds...) {
		if emote.State == mako.Active {
			allEmotesFromDB = append(allEmotesFromDB, emote)
		}
	}

	for _, emote := range allEmotesFromDB {
		// Skip emotes we've already processed in case there is overlap from both EmoteDB reads
		if dedupeMapByEmoteId[emote.ID] {
			continue
		}
		dedupeMapByEmoteId[emote.ID] = true

		// Emotes can be entitled under a group id that is different to what is in EmoteDB.
		// For example, a HypeTrain emote can be entitled to the HypeTrain group id rather
		// than the ticket product group it was uploaded to. Channel Points modified emotes
		// are also entitled to a specific group rather than the group its original is based off of.
		// This simply applies the group id that comes from the entitlement instead.
		groupID := emote.GroupID
		if entitlementGroupID := groupIDByEmoteID[emote.ID]; entitlementGroupID != "" {
			groupID = entitlementGroupID
		}

		emotesByGroupId[groupID] = append(emotesByGroupId[groupID], &mk.Emote{
			Id:        emote.ID,
			Pattern:   emote.Code,
			AssetType: mako.EmoteAssetTypeAsTwirp(emote.AssetType),
			Type:      mako.GetEmoteType(emote),
			Order:     emote.Order,
			CreatedAt: timestamppb.New(emote.CreatedAt),
		})
	}

	for groupId, emotes := range emotesByGroupId {
		// https://jira.twitch.com/browse/DIGI-451
		for i, emote := range emotes {
			normalized, ok := mat.normalizedSmilies[emote.Pattern]

			if normalizedID, groupExists := normalized.ID(groupId); ok && groupExists {
				emotes[i].Pattern = normalized.Code()
				emotes[i].Id = normalizedID
			}
		}

		// Dedupe emotes again here because there are more denormalized emotes in the database than what we want to present to the user.
		emotes = dedupeEmotes(emotes)
		mako.SortTwirpEmotesByOrderAscendingAndCreatedAtDescending(emotes)

		emoteSet := mk.EmoteSet{
			Id:        groupId,
			Emotes:    emotes,
			ChannelId: emoteChannelsByGroupID[groupId],
		}

		result[groupId] = emoteSet
	}

	// Return empty emoteGroups as well.
	for _, groupID := range emoteGroupIdsToRead {
		if _, ok := emotesByGroupId[groupID]; ok {
			continue
		}
		emoteSet := mk.EmoteSet{
			Id:        groupID,
			ChannelId: emoteChannelsByGroupID[groupID],
		}
		result[groupID] = emoteSet
	}

	if smilieFix {
		globalID := strconv.Itoa(mako.DefaultEmoticonSet)
		globalEmoteSet, ok := result[globalID]
		if !ok {
			return nil, errors.New("failed to load global emotes in materia")
		}

		newGlobalEmotes := make([]*mk.Emote, 0)
		uniqueKeys := make(map[string]struct{})

		for _, emote := range globalEmoteSet.Emotes {
			// Go over every global override and try to find a match, replacing the existing global emote with something else.
			for _, emoteSetID := range globalOverrides {
				emoteSet, exists := result[emoteSetID]
				if !exists {
					continue
				}

				var newEmotes []*mk.Emote
				for _, overrideEmote := range emoteSet.Emotes {
					if emote.Pattern == overrideEmote.Pattern {
						emote = overrideEmote
					} else {
						newEmotes = append(newEmotes, overrideEmote)
					}
				}

				// Replace the existing emote set with a new one that doesn't have the override emote so that override sets will move their emotes to the global set.
				result[emoteSetID] = mk.EmoteSet{
					Id:     emoteSetID,
					Emotes: newEmotes,
				}
			}

			if _, ok := uniqueKeys[emote.Pattern]; !ok {
				newGlobalEmotes = append(newGlobalEmotes, emote)
				uniqueKeys[emote.Pattern] = struct{}{}
			}
		}

		result[globalID] = mk.EmoteSet{
			Id:     globalID,
			Emotes: newGlobalEmotes,
		}
	}

	return result, nil
}

func dedupeEmotes(emotes []*mk.Emote) []*mk.Emote {
	keys := make(map[string]struct{})
	newEmotes := make([]*mk.Emote, 0, len(emotes))

	for _, emote := range emotes {
		if _, exists := keys[emote.Id]; exists {
			continue
		}

		keys[emote.Id] = struct{}{}
		newEmotes = append(newEmotes, emote)
	}

	return newEmotes
}
