package emoticonstateupdater

import (
	"context"

	mako "code.justin.tv/commerce/mako/internal"
	"github.com/pkg/errors"
)

type IEmoticonStateUpdater interface {
	UpdateEmoticonsState(ctx context.Context, emoticonIDs []string, newState mako.EmoteState) error
}

func NewEmoticonStateUpdater(db mako.EmoteDB) IEmoticonStateUpdater {
	return &EmoticonStateUpdaterImpl{
		EmoteDB: db,
	}
}

type EmoticonStateUpdaterImpl struct {
	EmoteDB mako.EmoteDB
}

func (e *EmoticonStateUpdaterImpl) UpdateEmoticonsState(ctx context.Context, emoticonIDs []string, newState mako.EmoteState) error {
	var dynamoEmoteIDs []string
	for _, emoteID := range emoticonIDs {
		dynamoEmoteIDs = append(dynamoEmoteIDs, emoteID)
	}

	// Since Dynamo doesn't have the ability to batch update items, we kick off the updates through the db in parallel
	dynamoErrChan := make(chan error)
	for _, emoteID := range dynamoEmoteIDs {
		go func(emoteID string) {
			emotes, err := e.EmoteDB.ReadByIDs(ctx, emoteID)
			if err != nil {
				dynamoErrChan <- errors.Wrap(err, "failed to get emoticon in dynamo")
				return
			}

			if len(emotes) > 0 {
				_, err := e.EmoteDB.UpdateState(ctx, emoteID, newState)
				dynamoErrChan <- errors.Wrap(err, "failed to update emoticons state in dynamodb")
				return
			}

			dynamoErrChan <- nil
		}(emoteID)
	}

	for i := 0; i < len(dynamoEmoteIDs); i++ {
		select {
		case err := <-dynamoErrChan:
			if err != nil {
				return err
			}
		case <-ctx.Done():
			return errors.New("timed out updating emote states in dynamo")
		}
	}

	return nil
}
