package emoticonstateupdater_test

import (
	"context"
	"testing"

	"code.justin.tv/commerce/mako/clients/emoticonstateupdater"
	mako "code.justin.tv/commerce/mako/internal"
	internal_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/internal_mock"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestEmoticonsStateUpdater(t *testing.T) {
	Convey("Test Emoticons Manager", t, func() {
		mockEmoteDB := new(internal_mock.EmoteDB)
		emoticonStateUpdater := emoticonstateupdater.EmoticonStateUpdaterImpl{
			EmoteDB: mockEmoteDB,
		}

		Convey("Test UpdateEmoticonsState", func() {
			subsEmoteIDs := []string{"1", "12", "123"}
			bitsEmoteIDs := []string{"emotesv2_a28bf49f78d64aecb2d5d73afe94dfc4", "emotesv2_c400aa6c2d6a4be79df158cce2c77948", "emotesv2_f146f4badbbe40bf9cb05734813b5872"}
			var allIDs []string
			allIDs = append(allIDs, subsEmoteIDs...)
			allIDs = append(allIDs, bitsEmoteIDs...)
			testCtx := context.Background()

			Convey("Given subs emotes", func() {
				Convey("Given emotes exist in dynamo", func() {
					mockEmoteDB.On("ReadByIDs", mock.Anything, subsEmoteIDs[0]).Return([]mako.Emote{{ID: subsEmoteIDs[0]}}, nil)
					mockEmoteDB.On("ReadByIDs", mock.Anything, subsEmoteIDs[1]).Return([]mako.Emote{{ID: subsEmoteIDs[1]}}, nil)
					mockEmoteDB.On("ReadByIDs", mock.Anything, subsEmoteIDs[2]).Return([]mako.Emote{{ID: subsEmoteIDs[2]}}, nil)

					Convey("when dynamo UpdateEmoteStateById returns an error", func() {
						mockEmoteDB.On("UpdateState", testCtx, subsEmoteIDs[0], mako.Active).Return(mako.Emote{}, errors.New("dummy dynamoDB error"))
						mockEmoteDB.On("UpdateState", testCtx, subsEmoteIDs[1], mako.Active).Return(mako.Emote{}, errors.New("dummy dynamoDB error"))
						mockEmoteDB.On("UpdateState", testCtx, subsEmoteIDs[2], mako.Active).Return(mako.Emote{}, errors.New("dummy dynamoDB error"))

						err := emoticonStateUpdater.UpdateEmoticonsState(testCtx, subsEmoteIDs, mako.Active)

						So(err, ShouldNotBeNil)
						So(err.Error(), ShouldContainSubstring, "dummy dynamoDB error")
						So(err.Error(), ShouldContainSubstring, "failed to update emoticons state in dynamodb")
					})

					Convey("when dynamo UpdateEmoteStateById succeeds", func() {
						mockEmoteDB.On("UpdateState", testCtx, subsEmoteIDs[0], mako.Active).Return(mako.Emote{}, nil)
						mockEmoteDB.On("UpdateState", testCtx, subsEmoteIDs[1], mako.Active).Return(mako.Emote{}, nil)
						mockEmoteDB.On("UpdateState", testCtx, subsEmoteIDs[2], mako.Active).Return(mako.Emote{}, nil)

						err := emoticonStateUpdater.UpdateEmoticonsState(testCtx, subsEmoteIDs, mako.Active)

						So(err, ShouldBeNil)
					})
				})

				Convey("Given GetById Dao call fails", func() {
					mockEmoteDB.On("ReadByIDs", testCtx, subsEmoteIDs[0]).Return(nil, errors.New("dummy dynamoDB error"))
					mockEmoteDB.On("ReadByIDs", testCtx, subsEmoteIDs[1]).Return(nil, errors.New("dummy dynamoDB error"))
					mockEmoteDB.On("ReadByIDs", testCtx, subsEmoteIDs[2]).Return(nil, errors.New("dummy dynamoDB error"))

					Convey("dynamo UpdateEmoteStateById is not called and an error is returned", func() {

						err := emoticonStateUpdater.UpdateEmoticonsState(testCtx, subsEmoteIDs, mako.Active)

						So(err, ShouldNotBeNil)
						So(err.Error(), ShouldContainSubstring, "dummy dynamoDB error")
						mockEmoteDB.AssertNotCalled(t, "UpdateState", mock.Anything, mock.Anything, mock.Anything)
					})
				})

				Convey("Given emotes do not exist in dynamo", func() {
					mockEmoteDB.On("ReadByIDs", mock.Anything, subsEmoteIDs[0]).Return(nil, nil)
					mockEmoteDB.On("ReadByIDs", mock.Anything, subsEmoteIDs[1]).Return(nil, nil)
					mockEmoteDB.On("ReadByIDs", mock.Anything, subsEmoteIDs[2]).Return(nil, nil)

					Convey("dynamo UpdateEmoteStateById is not called and no error is returned", func() {

						err := emoticonStateUpdater.UpdateEmoticonsState(testCtx, subsEmoteIDs, mako.Active)

						So(err, ShouldBeNil)
						mockEmoteDB.AssertExpectations(t)
						mockEmoteDB.AssertNotCalled(t, "UpdateState", mock.Anything, mock.Anything, mock.Anything)
					})
				})
			})

			Convey("Given bits emotes", func() {
				Convey("Given emotes exist in dynamo", func() {
					mockEmoteDB.On("ReadByIDs", mock.Anything, bitsEmoteIDs[0]).Return([]mako.Emote{{ID: bitsEmoteIDs[0]}}, nil)
					mockEmoteDB.On("ReadByIDs", mock.Anything, bitsEmoteIDs[1]).Return([]mako.Emote{{ID: bitsEmoteIDs[1]}}, nil)
					mockEmoteDB.On("ReadByIDs", mock.Anything, bitsEmoteIDs[2]).Return([]mako.Emote{{ID: bitsEmoteIDs[2]}}, nil)

					Convey("when dynamo UpdateEmoteStateById returns an error", func() {
						mockEmoteDB.On("UpdateState", testCtx, bitsEmoteIDs[0], mako.Active).Return(mako.Emote{}, errors.New("dummy dynamoDB error"))
						mockEmoteDB.On("UpdateState", testCtx, bitsEmoteIDs[1], mako.Active).Return(mako.Emote{}, errors.New("dummy dynamoDB error"))
						mockEmoteDB.On("UpdateState", testCtx, bitsEmoteIDs[2], mako.Active).Return(mako.Emote{}, errors.New("dummy dynamoDB error"))

						err := emoticonStateUpdater.UpdateEmoticonsState(testCtx, bitsEmoteIDs, mako.Active)

						So(err, ShouldNotBeNil)
						So(err.Error(), ShouldContainSubstring, "dummy dynamoDB error")
						So(err.Error(), ShouldContainSubstring, "failed to update emoticons state in dynamodb")
					})

					Convey("when dynamo UpdateEmoteStateById succeeds", func() {
						mockEmoteDB.On("UpdateState", testCtx, bitsEmoteIDs[0], mako.Active).Return(mako.Emote{}, nil)
						mockEmoteDB.On("UpdateState", testCtx, bitsEmoteIDs[1], mako.Active).Return(mako.Emote{}, nil)
						mockEmoteDB.On("UpdateState", testCtx, bitsEmoteIDs[2], mako.Active).Return(mako.Emote{}, nil)

						err := emoticonStateUpdater.UpdateEmoticonsState(testCtx, bitsEmoteIDs, mako.Active)

						So(err, ShouldBeNil)
					})
				})
			})

			Convey("Given a mix of subs and bits emotes", func() {
				Convey("Given emotes exist in dynamo", func() {
					mockEmoteDB.On("ReadByIDs", mock.Anything, allIDs[0]).Return([]mako.Emote{{ID: allIDs[0]}}, nil)
					mockEmoteDB.On("ReadByIDs", mock.Anything, allIDs[1]).Return([]mako.Emote{{ID: allIDs[1]}}, nil)
					mockEmoteDB.On("ReadByIDs", mock.Anything, allIDs[2]).Return([]mako.Emote{{ID: allIDs[2]}}, nil)
					mockEmoteDB.On("ReadByIDs", mock.Anything, allIDs[3]).Return([]mako.Emote{{ID: allIDs[3]}}, nil)
					mockEmoteDB.On("ReadByIDs", mock.Anything, allIDs[4]).Return([]mako.Emote{{ID: allIDs[4]}}, nil)
					mockEmoteDB.On("ReadByIDs", mock.Anything, allIDs[5]).Return([]mako.Emote{{ID: allIDs[5]}}, nil)

					Convey("when dynamo UpdateEmoteStateById returns an error", func() {
						mockEmoteDB.On("UpdateState", testCtx, allIDs[0], mako.Active).Return(mako.Emote{}, errors.New("dummy dynamoDB error"))
						mockEmoteDB.On("UpdateState", testCtx, allIDs[1], mako.Active).Return(mako.Emote{}, errors.New("dummy dynamoDB error"))
						mockEmoteDB.On("UpdateState", testCtx, allIDs[2], mako.Active).Return(mako.Emote{}, errors.New("dummy dynamoDB error"))
						mockEmoteDB.On("UpdateState", testCtx, allIDs[3], mako.Active).Return(mako.Emote{}, errors.New("dummy dynamoDB error"))
						mockEmoteDB.On("UpdateState", testCtx, allIDs[4], mako.Active).Return(mako.Emote{}, errors.New("dummy dynamoDB error"))
						mockEmoteDB.On("UpdateState", testCtx, allIDs[5], mako.Active).Return(mako.Emote{}, errors.New("dummy dynamoDB error"))

						err := emoticonStateUpdater.UpdateEmoticonsState(testCtx, allIDs, mako.Active)

						So(err, ShouldNotBeNil)
						So(err.Error(), ShouldContainSubstring, "dummy dynamoDB error")
						So(err.Error(), ShouldContainSubstring, "failed to update emoticons state in dynamodb")
					})

					Convey("when dynamo UpdateEmoteStateById succeeds", func() {
						mockEmoteDB.On("UpdateState", testCtx, allIDs[0], mako.Active).Return(mako.Emote{}, nil)
						mockEmoteDB.On("UpdateState", testCtx, allIDs[1], mako.Active).Return(mako.Emote{}, nil)
						mockEmoteDB.On("UpdateState", testCtx, allIDs[2], mako.Active).Return(mako.Emote{}, nil)
						mockEmoteDB.On("UpdateState", testCtx, allIDs[3], mako.Active).Return(mako.Emote{}, nil)
						mockEmoteDB.On("UpdateState", testCtx, allIDs[4], mako.Active).Return(mako.Emote{}, nil)
						mockEmoteDB.On("UpdateState", testCtx, allIDs[5], mako.Active).Return(mako.Emote{}, nil)

						err := emoticonStateUpdater.UpdateEmoticonsState(testCtx, allIDs, mako.Active)

						So(err, ShouldBeNil)
					})
				})
			})
		})
	})
}
