package ripley

import (
	"context"
	"net/http"

	"code.justin.tv/commerce/mako/config"
	ripley "code.justin.tv/revenue/ripley/rpc"
	"github.com/afex/hystrix-go/hystrix"
	"github.com/pkg/errors"
)

type PartnerType string

const (
	AffiliatePartnerType   = PartnerType("affiliate")   // Affiliate partners
	TraditionalPartnerType = PartnerType("traditional") // Traditional partners
	NonPartnerPartnerType  = PartnerType("non-partner") // Neither an affiliate partner nor a traditional partner
	UnknownPartnerType     = PartnerType("")            // We don't know their type due to downstream service call failures.

	HystrixNameRipleyPayoutGet = "ripley.payout.GET"
)

type ClientWrapper interface {
	GetPayoutType(ctx context.Context, channelID string) (*ripley.PayoutType, error)
	GetPartnerType(ctx context.Context, channelID string) (PartnerType, error)
}

type clientWrapper struct {
	RipleyClient ripley.Ripley
}

func NewClient(cfg config.Configuration) ClientWrapper {
	return &clientWrapper{
		RipleyClient: ripley.NewRipleyProtobufClient(cfg.RipleyServiceEndpoint, http.DefaultClient),
	}
}

func (c *clientWrapper) GetPayoutType(ctx context.Context, channelID string) (*ripley.PayoutType, error) {
	var resp *ripley.GetPayoutTypeResponse
	var err error
	err = hystrix.DoC(ctx, HystrixNameRipleyPayoutGet, func(ctx context.Context) error {
		resp, err = c.RipleyClient.GetPayoutType(ctx, &ripley.GetPayoutTypeRequest{
			ChannelId: channelID,
		})
		return err
	}, nil)

	if err != nil {
		return nil, err
	}
	if resp == nil {
		return nil, errors.Errorf("received nil response from ripley for channel %s", channelID)
	}

	return resp.PayoutType, err
}

func (c *clientWrapper) GetPartnerType(ctx context.Context, channelID string) (PartnerType, error) {
	resp, err := c.GetPayoutType(ctx, channelID)
	if err != nil || resp == nil {
		return UnknownPartnerType, err
	}

	if resp.IsPartner {
		return TraditionalPartnerType, nil
	} else if resp.IsAffiliate {
		return AffiliatePartnerType, nil
	} else {
		return NonPartnerPartnerType, nil
	}
}

func IsPartner(partnerType PartnerType) bool {
	return partnerType == TraditionalPartnerType
}

func IsAffiliate(partnerType PartnerType) bool {
	return partnerType == AffiliatePartnerType
}
