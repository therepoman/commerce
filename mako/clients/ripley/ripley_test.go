package ripley

import (
	"context"
	"testing"

	"code.justin.tv/commerce/mako/mocks/code.justin.tv/revenue/ripley/rpc"
	"code.justin.tv/revenue/ripley/rpc"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
)

func TestRipleyClientWrapper(t *testing.T) {
	Convey("Test Ripley Client Wrapper", t, func() {
		mockRipleyClient := new(ripley_client_mock.Ripley)
		clientWrapper := clientWrapper{
			RipleyClient: mockRipleyClient,
		}
		testChannelId := "267893713"
		testContext := context.Background()

		Convey("Test GetPayoutType", func() {
			Convey("Given ripley returns error then return error", func() {
				mockRipleyClient.On("GetPayoutType", testContext, &riptwirp.GetPayoutTypeRequest{ChannelId: testChannelId}).Return(nil, errors.New("dummy ripley error"))

				resp, err := clientWrapper.GetPayoutType(testContext, testChannelId)

				So(resp, ShouldBeNil)
				So(err, ShouldNotBeNil)
				So(err.Error(), ShouldContainSubstring, "dummy ripley error")
			})

			Convey("Given ripley returns nil then return error", func() {
				mockRipleyClient.On("GetPayoutType", testContext, &riptwirp.GetPayoutTypeRequest{ChannelId: testChannelId}).Return(nil, nil)

				resp, err := clientWrapper.GetPayoutType(testContext, testChannelId)

				So(resp, ShouldBeNil)
				So(err, ShouldNotBeNil)
				So(err.Error(), ShouldContainSubstring, "received nil response from ripley for channel")
			})

			Convey("Given ripley returns a payout type in the response then return that payout type", func() {
				mockRipleyClient.On("GetPayoutType", testContext, &riptwirp.GetPayoutTypeRequest{ChannelId: testChannelId}).Return(&riptwirp.GetPayoutTypeResponse{
					PayoutType: &riptwirp.PayoutType{
						ChannelId: testChannelId,
						IsPartner: true,
					},
				}, nil)

				resp, err := clientWrapper.GetPayoutType(testContext, testChannelId)

				So(resp, ShouldNotBeNil)
				So(err, ShouldBeNil)
				So(resp.ChannelId, ShouldEqual, testChannelId)
				So(resp.IsPartner, ShouldBeTrue)
			})
		})

		Convey("Test GetPartnerType", func() {
			Convey("Given ripley returns error then return unknown partner type and error", func() {
				mockRipleyClient.On("GetPayoutType", testContext, &riptwirp.GetPayoutTypeRequest{ChannelId: testChannelId}).Return(nil, errors.New("dummy ripley error"))

				resp, err := clientWrapper.GetPartnerType(testContext, testChannelId)

				So(resp, ShouldEqual, UnknownPartnerType)
				So(err, ShouldNotBeNil)
				So(err.Error(), ShouldContainSubstring, "dummy ripley error")
			})

			Convey("Given ripley returns nil resp then return unknown partner type and error", func() {
				mockRipleyClient.On("GetPayoutType", testContext, &riptwirp.GetPayoutTypeRequest{ChannelId: testChannelId}).Return(nil, nil)

				resp, err := clientWrapper.GetPartnerType(testContext, testChannelId)

				So(resp, ShouldEqual, UnknownPartnerType)
				So(err, ShouldNotBeNil)
				So(err.Error(), ShouldContainSubstring, "received nil response from ripley for channel")
			})

			Convey("Given ripley returns nil for partner type then return unknown partner type", func() {
				mockRipleyClient.On("GetPayoutType", testContext, &riptwirp.GetPayoutTypeRequest{ChannelId: testChannelId}).Return(&riptwirp.GetPayoutTypeResponse{
					PayoutType: nil,
				}, nil)

				resp, err := clientWrapper.GetPartnerType(testContext, testChannelId)

				So(resp, ShouldEqual, UnknownPartnerType)
				So(err, ShouldBeNil)
			})

			Convey("Given ripley returns partner payout type in response then return traditional partner type", func() {
				mockRipleyClient.On("GetPayoutType", testContext, &riptwirp.GetPayoutTypeRequest{ChannelId: testChannelId}).Return(&riptwirp.GetPayoutTypeResponse{
					PayoutType: &riptwirp.PayoutType{
						ChannelId: testChannelId,
						IsPartner: true,
					},
				}, nil)

				resp, err := clientWrapper.GetPartnerType(testContext, testChannelId)

				So(resp, ShouldEqual, TraditionalPartnerType)
				So(err, ShouldBeNil)
			})

			Convey("Given ripley returns affiliate payout type in response then return affiliate partner type", func() {
				mockRipleyClient.On("GetPayoutType", testContext, &riptwirp.GetPayoutTypeRequest{ChannelId: testChannelId}).Return(&riptwirp.GetPayoutTypeResponse{
					PayoutType: &riptwirp.PayoutType{
						ChannelId:   testChannelId,
						IsAffiliate: true,
					},
				}, nil)

				resp, err := clientWrapper.GetPartnerType(testContext, testChannelId)

				So(resp, ShouldEqual, AffiliatePartnerType)
				So(err, ShouldBeNil)
			})

			Convey("Given ripley returns neither partner nor affiliate payout type in response then return non-partner type", func() {
				mockRipleyClient.On("GetPayoutType", testContext, &riptwirp.GetPayoutTypeRequest{ChannelId: testChannelId}).Return(&riptwirp.GetPayoutTypeResponse{
					PayoutType: &riptwirp.PayoutType{
						ChannelId:   testChannelId,
						IsAffiliate: false,
						IsPartner:   false,
					},
				}, nil)

				resp, err := clientWrapper.GetPartnerType(testContext, testChannelId)

				So(resp, ShouldEqual, NonPartnerPartnerType)
				So(err, ShouldBeNil)
			})
		})

		Convey("Test IsPartner and IsAffiliate", func() {
			So(IsPartner(TraditionalPartnerType), ShouldBeTrue)
			So(IsPartner(AffiliatePartnerType), ShouldBeFalse)
			So(IsPartner(NonPartnerPartnerType), ShouldBeFalse)
			So(IsPartner(UnknownPartnerType), ShouldBeFalse)

			So(IsAffiliate(TraditionalPartnerType), ShouldBeFalse)
			So(IsAffiliate(AffiliatePartnerType), ShouldBeTrue)
			So(IsAffiliate(NonPartnerPartnerType), ShouldBeFalse)
			So(IsAffiliate(UnknownPartnerType), ShouldBeFalse)
		})
	})
}
