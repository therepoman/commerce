package paint

import (
	"context"
	"net/http"

	"code.justin.tv/subs/paint/paintrpc"
)

var DefaultModifications = []paintrpc.Modification{
	paintrpc.Modification_BW,
	paintrpc.Modification_HF,
	paintrpc.Modification_SG,
	paintrpc.Modification_TK,
	paintrpc.Modification_SQ,
	//paintrpc.Modification_PK,
	//paintrpc.Modification_RD,
	//paintrpc.Modification_SA,
	//paintrpc.Modification_EV,
	//paintrpc.Modification_RA,
	//paintrpc.Modification_HE,
	//paintrpc.Modification_KI,
	//paintrpc.Modification_HB,
	//paintrpc.Modification_BE,
	//paintrpc.Modification_LH,
	//paintrpc.Modification_CL,
	//paintrpc.Modification_WD,
	//paintrpc.Modification_RB,
	//paintrpc.Modification_BN,
	//paintrpc.Modification_EG,
	//paintrpc.Modification_EB,
	//paintrpc.Modification_FC,
	//paintrpc.Modification_BT,
	//paintrpc.Modification_SO,
	paintrpc.Modification_UN,
	paintrpc.Modification_FF,
	paintrpc.Modification_FB,
	//paintrpc.Modification_MC,
	//paintrpc.Modification_GR,
	//paintrpc.Modification_WH,
	//paintrpc.Modification_PM,
	//paintrpc.Modification_SF,
	//paintrpc.Modification_SM,
}

// DefaultSizes represents all the sizes that modifications are created for.
var DefaultSizes = []string{
	"1.0",
	"2.0",
	"3.0",
	"4.0",
}

// Modified emotes used to be created in these sizes as well, though we no longer do we will
// keep a record of them for cases like deleting where we need to know all the sizes an emote
// could have had.
var LegacySizes = []string{
	"1.0",
	"2.0",
	"2.5",
	"3.0",
	"3.5",
	"4.0",
}

// Modifications returns a slice of all the modifications available.
func Modifications() []string {
	value := make([]string, 0)

	for _, modification := range paintrpc.Modification_name {
		value = append(value, modification)
	}

	return value
}

type clientWrapper struct {
	Client paintrpc.Paint
}

type PaintClient interface {
	ModifyEmote(ctx context.Context, req *paintrpc.ModifyEmoteRequest) (res *paintrpc.ModifyEmoteResponse, err error)
}

func NewClient(host string) PaintClient {
	return &clientWrapper{
		Client: paintrpc.NewPaintProtobufClient(host, http.DefaultClient),
	}
}

func (p *clientWrapper) ModifyEmote(ctx context.Context, req *paintrpc.ModifyEmoteRequest) (res *paintrpc.ModifyEmoteResponse, err error) {
	return p.Client.ModifyEmote(ctx, req)
}
