package pendingemoticons_test

import (
	"context"
	"strconv"
	"testing"
	"time"

	receiver "code.justin.tv/amzn/TwitchDartReceiverTwirp"
	"code.justin.tv/commerce/mako/clients/pendingemoticons"
	"code.justin.tv/commerce/mako/clients/pendingemoticons/models"
	"code.justin.tv/commerce/mako/dynamo"
	mako "code.justin.tv/commerce/mako/internal"
	receiver_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/amzn/TwitchDartReceiverTwirp"
	emoticons_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/emoticons"
	emoticonstateupdater_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/emoticonstateupdater"
	eventbus_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/eventbus"
	dynamo_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/dynamo"
	spade_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/common/spade-client-go/spade"
	stats_mock "code.justin.tv/commerce/mako/mocks/github.com/cactus/go-statsd-client/statsd"
	"code.justin.tv/common/spade-client-go/spade"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

const autoApprovalAdminID = "__autoapproved__"

func TestPendingEmoticonsClient(t *testing.T) {
	Convey("Test PendingEmoticons Client", t, func() {
		mockEmoticonAggregator := new(emoticons_mock.EmoticonAggregator)
		mockEmoticonStateUpdater := new(emoticonstateupdater_mock.IEmoticonStateUpdater)
		mockPendingEmoticonsDAO := new(dynamo_mock.IPendingEmotesDao)
		mockSpadeClient := new(spade_mock.Client)
		mockDartReceiverClient := new(receiver_mock.Receiver)
		mockStats := new(stats_mock.Statter)
		mockEmoticonPublisher := new(eventbus_mock.Publisher)

		client := pendingemoticons.PendingEmoticonsClient{
			EmoticonStateUpdater: mockEmoticonStateUpdater,
			EmoticonAggregator:   mockEmoticonAggregator,
			PendingEmoticonsDAO:  mockPendingEmoticonsDAO,
			SpadeClient:          mockSpadeClient,
			DartReceiverClient:   mockDartReceiverClient,
			Stats:                mockStats,
			EmoticonPublisher:    mockEmoticonPublisher,
		}

		mockStats.On("Inc", mock.Anything, mock.Anything, mock.Anything).Return(nil)

		Convey("Test GetPendingEmoticonsByIDs", func() {
			Convey("when PendingEmoticonsDAO.GetPendingEmoticonsByIDs returns an error, we should return an error", func() {
				emoticonIDs := []string{"1", "2"}

				mockPendingEmoticonsDAO.On("GetPendingEmoticonsByIDs", mock.Anything, emoticonIDs).Return([]*dynamo.PendingEmote{}, errors.New("ERROR"))

				_, err := client.GetPendingEmoticonsByIDs(context.Background(), emoticonIDs)

				So(err, ShouldNotBeNil)
			})

			Convey("when PendingEmoticonsDAO.GetPendingEmoticonsByIDs does not return an error, we should return the pending emotes", func() {
				emoticonIDs := []string{"1", "2"}
				mockPendingEmotes := []*dynamo.PendingEmote{}

				mockPendingEmoticonsDAO.On("GetPendingEmoticonsByIDs", mock.Anything, emoticonIDs).Return(mockPendingEmotes, nil)

				pendingEmotes, err := client.GetPendingEmoticonsByIDs(context.Background(), emoticonIDs)

				So(err, ShouldBeNil)
				So(pendingEmotes, ShouldResemble, mockPendingEmotes)
			})
		})

		Convey("Test DeletePendingEmoticons", func() {
			Convey("when PendingEmoticonsDAO.DeletePendingEmoticons returns an error, we should return an error", func() {
				emoticonIDs := []string{"1", "2"}

				mockPendingEmoticonsDAO.On("DeletePendingEmoticons", mock.Anything, emoticonIDs).Return(errors.New("ERROR"))

				err := client.DeletePendingEmoticons(context.Background(), emoticonIDs)

				So(err, ShouldNotBeNil)
			})

			Convey("when PendingEmoticonsDAO.DeletePendingEmoticons does not return an error, we should return nil", func() {
				emoticonIDs := []string{"1", "2"}

				mockPendingEmoticonsDAO.On("DeletePendingEmoticons", mock.Anything, emoticonIDs).Return(nil)

				err := client.DeletePendingEmoticons(context.Background(), emoticonIDs)

				So(err, ShouldBeNil)
			})
		})

		Convey("Test DeletePendingEmoticonIfExists", func() {
			Convey("when PendingEmoticonsDAO.GetPendingEmoticonsByIDs returns an error, we should return an error", func() {
				mockPendingEmoticonsDAO.On("GetPendingEmoticonsByIDs", mock.Anything, []string{"1"}).Return([]*dynamo.PendingEmote{}, errors.New("ERROR"))

				err := client.DeletePendingEmoticonIfExists(context.Background(), "1")

				So(err, ShouldNotBeNil)
			})

			Convey("when PendingEmoticonsDAO.GetPendingEmoticonsByIDs returns no items, we should return nil", func() {
				mockPendingEmotes := []*dynamo.PendingEmote{}

				mockPendingEmoticonsDAO.On("GetPendingEmoticonsByIDs", mock.Anything, []string{"1"}).Return(mockPendingEmotes, nil)

				err := client.DeletePendingEmoticonIfExists(context.Background(), "1")

				So(err, ShouldBeNil)
			})

			Convey("when PendingEmoticonsDAO.GetPendingEmoticonsByIDs does not return an error", func() {
				mockPendingEmotes := []*dynamo.PendingEmote{
					{
						EmoteId: "1",
					},
				}

				mockPendingEmoticonsDAO.On("GetPendingEmoticonsByIDs", mock.Anything, []string{"1"}).Return(mockPendingEmotes, nil)

				Convey("when PendingEmoticonsDAO.DeletePendingEmoticons returns an error, we should return an error", func() {
					mockPendingEmoticonsDAO.On("DeletePendingEmoticons", mock.Anything, []string{"1"}).Return(errors.New("ERROR"))

					err := client.DeletePendingEmoticonIfExists(context.Background(), "1")

					So(err, ShouldNotBeNil)
				})

				Convey("when PendingEmoticonsDAO.DeletePendingEmoticons does not return an error, we should return nil", func() {
					mockPendingEmoticonsDAO.On("DeletePendingEmoticons", mock.Anything, []string{"1"}).Return(nil)

					err := client.DeletePendingEmoticonIfExists(context.Background(), "1")

					So(err, ShouldBeNil)
				})
			})
		})

		Convey("Test ReviewPendingEmoticons", func() {
			Convey("when EmoticonAggregator.GetEmoticonStateMapByEmoteIDs errors, we should error", func() {
				mockReviews := []models.EmoticonReview{
					{
						EmoticonID: "123",
						Approve:    true,
					},
				}

				mockAdminID := "michael"

				expectedIDs := []string{"123"}

				mockEmoticonAggregator.On("GetEmoticonStateMapByEmoteIDs", mock.Anything, expectedIDs).Return(map[string]mako.EmoteState{}, errors.New("ERROR"))

				_, err := client.ReviewPendingEmoticons(context.Background(), mockReviews, mockAdminID)

				So(err, ShouldNotBeNil)
			})

			Convey("when EmoticonAggregator.GetEmoticonStateMapByEmoteIDs does not error", func() {
				Convey("when updating emoticon state fails, we should return the failed review state", func() {
					mockReviews := []models.EmoticonReview{
						// Approved, will fail
						{
							EmoticonID: "123",
							Approve:    true,
						},
						// Rejected, will fail
						{
							EmoticonID:        "456",
							Approve:           false,
							RejectReason:      "Bad",
							RejectDescription: "Included a bad thing",
						},
					}

					mockAdminID := "michael"

					expectedIDs := []string{"123", "456"}

					mockEmoticonState := map[string]mako.EmoteState{
						"123": mako.Pending,
						"456": mako.Pending,
					}

					expectedApprovedIDs := []string{"123"}
					expectedRejectedIDs := []string{"456"}

					mockEmoticonAggregator.On("GetEmoticonStateMapByEmoteIDs", mock.Anything, expectedIDs).Return(mockEmoticonState, nil)
					mockEmoticonPublisher.On("PublishEmoticonCreate", mock.Anything, expectedApprovedIDs).Return(nil)
					mockEmoticonStateUpdater.On("UpdateEmoticonsState", mock.Anything, expectedApprovedIDs, mako.Active).Return(errors.New("ERROR")).Once()
					mockEmoticonStateUpdater.On("UpdateEmoticonsState", mock.Anything, expectedRejectedIDs, mako.Inactive).Return(errors.New("ERROR")).Once()

					resp, err := client.ReviewPendingEmoticons(context.Background(), mockReviews, mockAdminID)

					So(err, ShouldBeNil)
					So(resp, ShouldResemble, []models.ReviewEmoticonResponse{
						{
							EmoticonID: "123",
							Succeeded:  false,
							NewState:   mako.Pending,
						},
						{
							EmoticonID: "456",
							Succeeded:  false,
							NewState:   mako.Pending,
						},
					})
				})

				Convey("when updating emoticon state does not fail", func() {
					mockReviews := []models.EmoticonReview{
						// Approved, active
						{
							EmoticonID: "123",
							Approve:    true,
						},
						// Approved, archived
						{
							EmoticonID: "xyz",
							Approve:    true,
						},
						// Rejected
						{
							EmoticonID:        "456",
							Approve:           false,
							RejectReason:      "Bad",
							RejectDescription: "Included a bad thing",
						},
						// Already reviewed
						{
							EmoticonID: "abc",
							Approve:    true,
						},
						// Not found
						{
							EmoticonID: "def",
							Approve:    true,
						},
						// Rejected without email
						{
							EmoticonID:             "foo",
							Approve:                false,
							RejectReason:           "--NO EMAIL--",
							RejectDescription:      "<email not sent to user, use for DMCA takedowns>",
							ShouldSkipNotification: true,
						},
					}

					expectedIDs := []string{"123", "xyz", "456", "abc", "def", "foo"}

					mockEmoticonState := map[string]mako.EmoteState{
						"123": mako.Pending,
						"xyz": mako.PendingArchived,
						"456": mako.Pending,
						"abc": mako.Active,
						"foo": mako.Pending,
					}

					mockApprovedPendingEmoticons := []*dynamo.PendingEmote{
						{
							EmoteId:   "123",
							OwnerId:   "michaelttv",
							Code:      "Zoomer",
							AssetType: "static",
						},
						{
							EmoteId:   "xyz",
							OwnerId:   "michaelttv",
							Code:      "Daytona",
							AssetType: "static",
						},
					}

					mockRejectedPendingEmoticons := []*dynamo.PendingEmote{
						{
							EmoteId:   "456",
							OwnerId:   "michaelttv",
							Code:      "Boomer",
							AssetType: "animated",
						},
						{
							EmoteId:   "foo",
							OwnerId:   "aiden",
							Code:      "Kappa",
							AssetType: "static",
						},
					}

					expectedApprovedIDs := []string{"123", "xyz"}
					expectedRejectedIDs := []string{"456", "foo"}

					dartReceiverApprovalMatcher := mock.MatchedBy(func(request *receiver.PublishNotificationRequest) bool {
						return request.Metadata["state"] == pendingemoticons.DartApprovedState && request.Metadata["is_animated_emote"] == "false" && (request.Metadata["emote_id"] == "123" || request.Metadata["emote_id"] == "xyz")
					})

					dartReceiverRejectionMatcher := mock.MatchedBy(func(request *receiver.PublishNotificationRequest) bool {
						return request.Metadata["state"] == pendingemoticons.DartRejectedState && request.Metadata["emote_id"] == "456" && request.Metadata["is_animated_emote"] == "true"
					})

					dartReceiverSkipMatcher := mock.MatchedBy(func(request *receiver.PublishNotificationRequest) bool {
						return request.Metadata["emote_id"] == "foo"
					})

					expectedEmoticonIDsToDelete := []string{"123", "xyz", "456", "foo", "abc"}

					mockEmoticonAggregator.On("GetEmoticonStateMapByEmoteIDs", mock.Anything, expectedIDs).Return(mockEmoticonState, nil)
					mockEmoticonPublisher.On("PublishEmoticonCreate", mock.Anything, mock.Anything).Return(nil)
					mockEmoticonStateUpdater.On("UpdateEmoticonsState", mock.Anything, expectedApprovedIDs[:1], mako.Active).Return(nil).Once()
					mockEmoticonStateUpdater.On("UpdateEmoticonsState", mock.Anything, expectedApprovedIDs[1:], mako.Archived).Return(nil).Once()
					mockEmoticonStateUpdater.On("UpdateEmoticonsState", mock.Anything, expectedRejectedIDs, mako.Inactive).Return(nil).Once()
					mockDartReceiverClient.On("PublishNotification", mock.Anything, dartReceiverApprovalMatcher).Return(&receiver.PublishNotificationResponse{}, nil).Once()
					mockDartReceiverClient.On("PublishNotification", mock.Anything, dartReceiverRejectionMatcher).Return(&receiver.PublishNotificationResponse{}, nil).Once()
					mockDartReceiverClient.On("PublishNotification", mock.Anything, dartReceiverSkipMatcher).Return(nil, errors.New("should never be called"))
					mockDartReceiverClient.On("PublishNotification", mock.Anything, mock.Anything).Return(&receiver.PublishNotificationResponse{}, nil)
					mockPendingEmoticonsDAO.On("GetPendingEmoticonsByIDs", mock.Anything, expectedApprovedIDs).Return(mockApprovedPendingEmoticons, nil).Once()
					mockPendingEmoticonsDAO.On("GetPendingEmoticonsByIDs", mock.Anything, expectedRejectedIDs).Return(mockRejectedPendingEmoticons, nil).Once()
					mockPendingEmoticonsDAO.On("DeletePendingEmoticons", mock.Anything, expectedEmoticonIDsToDelete).Return(nil)
					mockEmoticonAggregator.On("GetEmoticonsByEmoteIDs", mock.Anything, append(expectedApprovedIDs, expectedRejectedIDs...)).Return([]mako.Emote{
						{
							ID: "123",
						},
						{
							ID: "xyz",
						},
						{
							ID: "456",
						},
						{
							ID: "foo",
						},
					}, nil).Once()
					mockSpadeClient.On("TrackEvents", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)

					Convey("when the emote was manually approved", func() {
						mockAdminID := "michael"

						resp, err := client.ReviewPendingEmoticons(context.Background(), mockReviews, mockAdminID)

						So(err, ShouldBeNil)

						Convey("we should return the new review state", func() {
							So(resp, ShouldResemble, []models.ReviewEmoticonResponse{
								{
									EmoticonID: "123",
									Succeeded:  true,
									NewState:   mako.Active,
								},
								{
									EmoticonID: "xyz",
									Succeeded:  true,
									NewState:   mako.Archived,
								},
								{
									EmoticonID: "456",
									Succeeded:  true,
									NewState:   mako.Inactive,
								},
								{
									EmoticonID: "foo",
									Succeeded:  true,
									NewState:   mako.Inactive,
								},
								{
									EmoticonID: "abc",
									Succeeded:  true,
									NewState:   mako.Active,
								},
								{
									EmoticonID: "def",
									Succeeded:  false,
									NewState:   mako.Unknown,
								},
							})
						})

						Convey("we should update the state for approved pending emoticons", func() {
							mockEmoticonStateUpdater.AssertCalled(t, "UpdateEmoticonsState", mock.Anything, expectedApprovedIDs[:1], mako.Active)
						})

						Convey("we should update the state for approved pending_archived emoticons", func() {
							mockEmoticonStateUpdater.AssertCalled(t, "UpdateEmoticonsState", mock.Anything, expectedApprovedIDs[1:], mako.Archived)
						})

						Convey("we should update the state for rejected emoticons", func() {
							mockEmoticonStateUpdater.AssertCalled(t, "UpdateEmoticonsState", mock.Anything, expectedRejectedIDs, mako.Inactive)
						})

						Convey("we should send a Dart notification for approved emoticons", func() {
							mockDartReceiverClient.AssertCalled(t, "PublishNotification", mock.Anything, dartReceiverApprovalMatcher)
						})

						Convey("we should send a Dart notification for rejected emoticons", func() {
							mockDartReceiverClient.AssertCalled(t, "PublishNotification", mock.Anything, dartReceiverRejectionMatcher)
						})

						Convey("we should delete the pending emoticons", func() {
							mockPendingEmoticonsDAO.AssertCalled(t, "DeletePendingEmoticons", mock.Anything, expectedEmoticonIDsToDelete)
						})

						Convey("we should not email if SkipEmail was true", func() {
							mockDartReceiverClient.AssertNotCalled(t, "PublishNotification", mock.Anything, dartReceiverSkipMatcher)
						})

						Convey("we should track events for approved and rejected emotes, with IsAutoApproval set to false", func() {
							time.Sleep(500 * time.Millisecond) // Wait for async work to be done
							mockSpadeClient.AssertNumberOfCalls(t, "TrackEvents", 1)
							So(mockSpadeClient.Calls[0].Arguments[1].(spade.Event).Properties.(models.EmoticonReviewEvent).IsAutoApproval, ShouldBeFalse)

							Convey("we should track assetType", func() {
								So(mockSpadeClient.Calls[0].Arguments[1].(spade.Event).Properties.(models.EmoticonReviewEvent).AssetType, ShouldEqual, "static")
							})
						})

					})

					Convey("when the emote was auto-approved (through a script)", func() {
						mockAdminID := pendingemoticons.AutoApprovalAdminID

						_, err := client.ReviewPendingEmoticons(context.Background(), mockReviews, mockAdminID)

						So(err, ShouldBeNil)

						Convey("we should send a Dart notification for approved emoticons only", func() {
							mockDartReceiverClient.AssertNumberOfCalls(t, "PublishNotification", 2)
						})

						Convey("we should track events for approved and rejected emotes, with IsAutoApproval set to true", func() {
							time.Sleep(500 * time.Millisecond) // Wait for async work to be done
							mockSpadeClient.AssertNumberOfCalls(t, "TrackEvents", 1)
							So(mockSpadeClient.Calls[0].Arguments[1].(spade.Event).Properties.(models.EmoticonReviewEvent).IsAutoApproval, ShouldBeTrue)
						})
					})
				})
			})
		})

		Convey("Test DeferPendingEmoticons", func() {
			mockEmoticonIDs := []string{"123", "456"}
			mockAdminID := "jelhay"

			Convey("when EmoticonAggregator.GetEmoticonStateMapByEmoteIDs errors, we should error", func() {
				mockEmoticonAggregator.On("GetEmoticonStateMapByEmoteIDs", mock.Anything, mockEmoticonIDs).Return(map[string]mako.EmoteState{}, errors.New("ERROR"))

				resp, err := client.DeferPendingEmoticons(context.Background(), mockEmoticonIDs, mockAdminID)

				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})

			Convey("when EmoticonAggregator.GetEmoticonStateMapByEmoteIDs does not error and all emotes are still pending", func() {
				Convey("when all the emote are still in pending state", func() {
					mockEmoticonState := map[string]mako.EmoteState{
						"123": mako.Pending,
						"456": mako.PendingArchived,
					}

					mockEmoticonAggregator.On("GetEmoticonStateMapByEmoteIDs", mock.Anything, mockEmoticonIDs).Return(mockEmoticonState, nil)

					Convey("when PendingEmoticonsDAO.GetPendingEmoticonsByIDs errors, we should error", func() {
						mockPendingEmoticonsDAO.On("GetPendingEmoticonsByIDs", mock.Anything, mockEmoticonIDs).Return(nil, errors.New("ERROR"))

						resp, err := client.DeferPendingEmoticons(context.Background(), mockEmoticonIDs, mockAdminID)

						So(err, ShouldNotBeNil)
						So(resp, ShouldBeNil)
					})

					Convey("when PendingEmoticonsDAO.GetPendingEmoticonsByIDs does not error", func() {
						mockPendingEmoticons := []*dynamo.PendingEmote{
							{
								EmoteId: "123",
								OwnerId: "12349876",
								Code:    "Beaver",
							},
							{
								EmoteId: "456",
								OwnerId: "12349876",
								Code:    "Weaver",
							},
						}

						mockPendingEmoticonsDAO.On("GetPendingEmoticonsByIDs", mock.Anything, mockEmoticonIDs).Return(mockPendingEmoticons, nil)

						Convey("when PendingEmoticonsDAO.UpdatePendingEmoticonsReviewState errors, we should error", func() {
							mockPendingEmoticonsDAO.On("UpdatePendingEmoticonsReviewState", mock.Anything, mockPendingEmoticons, dynamo.PendingEmoteReviewStateDeferred).Return(errors.New("ERROR"))

							resp, err := client.DeferPendingEmoticons(context.Background(), mockEmoticonIDs, mockAdminID)

							So(err, ShouldNotBeNil)
							So(resp, ShouldBeNil)
						})

						Convey("when PendingEmoticonsDAO.UpdatePendingEmoticonsReviewState does not error, we should return a response", func() {
							mockPendingEmoticonsDAO.On("UpdatePendingEmoticonsReviewState", mock.Anything, mockPendingEmoticons, dynamo.PendingEmoteReviewStateDeferred).Return(nil)
							mockEmoticonAggregator.On("GetEmoticonsByEmoteIDs", mock.Anything, mockEmoticonIDs).Return([]mako.Emote{
								{
									ID: "123",
								},
								{
									ID: "456",
								},
							}, nil).Once()
							mockSpadeClient.On("TrackEvents", mock.Anything, mock.Anything, mock.Anything).Return(nil)

							resp, err := client.DeferPendingEmoticons(context.Background(), mockEmoticonIDs, mockAdminID)

							So(err, ShouldBeNil)
							So(resp, ShouldResemble, []*models.DeferPendingEmoticonResponse{
								{
									EmoticonID: "123",
									Succeeded:  true,
								},
								{
									EmoticonID: "456",
									Succeeded:  true,
								},
							})

							time.Sleep(500 * time.Millisecond) // Wait for async work to be done
							mockSpadeClient.AssertNumberOfCalls(t, "TrackEvents", 1)
						})
					})
				})

				Convey("when one of the emotes is not in pending state anymore, it should be marked as failed", func() {
					mockEmoticonState := map[string]mako.EmoteState{
						"123": mako.Pending,
						"456": mako.Active,
					}

					mockEmoticonAggregator.On("GetEmoticonStateMapByEmoteIDs", mock.Anything, mockEmoticonIDs).Return(mockEmoticonState, nil)

					mockPendingEmoticons := []*dynamo.PendingEmote{
						{
							EmoteId: "123",
							OwnerId: "12349876",
							Code:    "Beaver",
						},
					}

					mockPendingEmoticonsDAO.On("GetPendingEmoticonsByIDs", mock.Anything, []string{"123"}).Return(mockPendingEmoticons, nil)
					mockPendingEmoticonsDAO.On("UpdatePendingEmoticonsReviewState", mock.Anything, mockPendingEmoticons, dynamo.PendingEmoteReviewStateDeferred).Return(nil)
					mockEmoticonAggregator.On("GetEmoticonsByEmoteIDs", mock.Anything, []string{"123"}).Return([]mako.Emote{
						{
							ID: "123",
						},
					}, nil).Once()
					mockSpadeClient.On("TrackEvents", mock.Anything, mock.Anything, mock.Anything).Return(nil)

					resp, err := client.DeferPendingEmoticons(context.Background(), mockEmoticonIDs, mockAdminID)

					So(err, ShouldBeNil)
					So(resp, ShouldResemble, []*models.DeferPendingEmoticonResponse{
						{
							EmoticonID: "123",
							Succeeded:  true,
						},
						{
							EmoticonID: "456",
							Succeeded:  false,
						},
					})

					time.Sleep(500 * time.Millisecond) // Wait for async work to be done
					mockSpadeClient.AssertNumberOfCalls(t, "TrackEvents", 1)
				})

				Convey("when one of the emotes is not returned in the state map, it should be marked as failed", func() {
					mockEmoticonState := map[string]mako.EmoteState{
						"123": mako.Pending,
					}

					mockEmoticonAggregator.On("GetEmoticonStateMapByEmoteIDs", mock.Anything, mockEmoticonIDs).Return(mockEmoticonState, nil)

					mockPendingEmoticons := []*dynamo.PendingEmote{
						{
							EmoteId: "123",
							OwnerId: "12349876",
							Code:    "Beaver",
						},
					}

					mockPendingEmoticonsDAO.On("GetPendingEmoticonsByIDs", mock.Anything, []string{"123"}).Return(mockPendingEmoticons, nil)
					mockPendingEmoticonsDAO.On("UpdatePendingEmoticonsReviewState", mock.Anything, mockPendingEmoticons, dynamo.PendingEmoteReviewStateDeferred).Return(nil)
					mockEmoticonAggregator.On("GetEmoticonsByEmoteIDs", mock.Anything, []string{"123"}).Return([]mako.Emote{
						{
							ID: "123",
						},
					}, nil).Once()
					mockSpadeClient.On("TrackEvents", mock.Anything, mock.Anything, mock.Anything).Return(nil)

					resp, err := client.DeferPendingEmoticons(context.Background(), mockEmoticonIDs, mockAdminID)

					So(err, ShouldBeNil)
					So(resp, ShouldResemble, []*models.DeferPendingEmoticonResponse{
						{
							EmoticonID: "123",
							Succeeded:  true,
						},
						{
							EmoticonID: "456",
							Succeeded:  false,
						},
					})

					time.Sleep(500 * time.Millisecond) // Wait for async work to be done
					mockSpadeClient.AssertNumberOfCalls(t, "TrackEvents", 1)
				})
			})
		})

		Convey("Test GetPendingEmoticons", func() {
			Convey("Test Partner filter", func() {
				Convey("when PendingEmoticonsDAO.GetPendingEmoticonsWithFilters returns an error, we should return an error", func() {
					typeFilter := models.PendingEmoticonFilterPartner
					reviewStateFilter := models.PendingEmoticonReviewStateFilterNone
					assetTypeFilter := models.PendingEmoteAssetTypeFilterAll
					after := time.Now().Add(-time.Minute)
					limit := 1
					bucketCount := 1
					bucketNumber := 0

					mockPendingEmoticonsDAO.On("GetPendingEmoticonsWithFilters", mock.Anything, dynamo.PendingEmotesFilter{
						Type:        dynamo.Partner,
						ReviewState: dynamo.PendingEmoteReviewStateNone,
						After:       after,
					}, mock.Anything, mock.Anything).Return(nil, errors.New("ERROR"))

					_, err := client.GetPendingEmoticons(context.Background(), limit, typeFilter, reviewStateFilter, assetTypeFilter, after, bucketCount, bucketNumber)

					So(err, ShouldNotBeNil)
				})

				Convey("when dynamo does not error", func() {
					Convey("when dynamo returns a LastEvaluatedKey, we should query until LastEvaluatedKey is nil", func() {
						typeFilter := models.PendingEmoticonFilterPartner
						reviewStateFilter := models.PendingEmoticonReviewStateFilterNone
						assetTypeFilter := models.PendingEmoteAssetTypeFilterAll
						after := time.Now().Add(-time.Minute)
						limit := 1
						bucketCount := 1
						bucketNumber := 0

						newCursor := dynamo.Cursor{}

						mockDynamoResp1 := &dynamo.GetPendingEmoticonsResponse{
							LastEvaluatedKey: newCursor,
							Items:            []*dynamo.PendingEmote{},
						}

						mockDynamoResp2 := &dynamo.GetPendingEmoticonsResponse{
							LastEvaluatedKey: nil,
							Items:            []*dynamo.PendingEmote{},
						}

						mockPendingEmoticonsDAO.On("GetPendingEmoticonsWithFilters", mock.Anything, dynamo.PendingEmotesFilter{
							Type:        dynamo.Partner,
							ReviewState: dynamo.PendingEmoteReviewStateNone,
							After:       after,
						}, mock.Anything, mock.Anything).Return(mockDynamoResp1, nil).Once()
						mockPendingEmoticonsDAO.On("GetPendingEmoticonsWithFilters", mock.Anything, dynamo.PendingEmotesFilter{
							Type:        dynamo.Partner,
							ReviewState: dynamo.PendingEmoteReviewStateNone,
							After:       after,
						}, mock.Anything, newCursor).Return(mockDynamoResp2, nil).Once()

						mockEmoticonAggregator.On("GetEmoticonStateMapByEmoteIDs", mock.Anything, mock.Anything).Return(map[string]mako.EmoteState{}, nil)

						_, err := client.GetPendingEmoticons(context.Background(), limit, typeFilter, reviewStateFilter, assetTypeFilter, after, bucketCount, bucketNumber)

						So(err, ShouldBeNil)
						mockPendingEmoticonsDAO.AssertNumberOfCalls(t, "GetPendingEmoticonsWithFilters", 2)
					})

					Convey("we should error if EmoticonAggregator.GetEmoticonStateMapByEmoteIDs returns an error", func() {
						typeFilter := models.PendingEmoticonFilterPartner
						reviewStateFilter := models.PendingEmoticonReviewStateFilterNone
						assetTypeFilter := models.PendingEmoteAssetTypeFilterAll
						after := time.Now().Add(-time.Minute)
						limit := 1
						bucketCount := 1
						bucketNumber := 0

						mockDynamoResp := &dynamo.GetPendingEmoticonsResponse{
							LastEvaluatedKey: nil,
							Items: []*dynamo.PendingEmote{
								{
									EmoteId: "123",
								},
							},
						}

						mockPendingEmoticonsDAO.On("GetPendingEmoticonsWithFilters", mock.Anything, dynamo.PendingEmotesFilter{
							Type:        dynamo.Partner,
							ReviewState: dynamo.PendingEmoteReviewStateNone,
							After:       after,
						}, mock.Anything, mock.Anything).Return(mockDynamoResp, nil)
						mockEmoticonAggregator.On("GetEmoticonStateMapByEmoteIDs", mock.Anything, []string{"123"}).Return(map[string]mako.EmoteState{}, errors.New("ERROR"))

						_, err := client.GetPendingEmoticons(context.Background(), limit, typeFilter, reviewStateFilter, assetTypeFilter, after, bucketCount, bucketNumber)

						So(err, ShouldNotBeNil)
					})

					Convey("we should delete items that are no longer pending according to the canonical source of truth", func() {
						typeFilter := models.PendingEmoticonFilterPartner
						reviewStateFilter := models.PendingEmoticonReviewStateFilterNone
						assetTypeFilter := models.PendingEmoteAssetTypeFilterAll
						after := time.Now().Add(-time.Minute)
						limit := 1
						bucketCount := 1
						bucketNumber := 0

						mockDynamoResp := &dynamo.GetPendingEmoticonsResponse{
							LastEvaluatedKey: nil,
							Items: []*dynamo.PendingEmote{
								{
									EmoteId: "123",
								},
								{
									EmoteId: "456",
								},
							},
						}

						expectedIDList := []string{"123", "456"}

						mockEmoteStates := map[string]mako.EmoteState{
							"123": mako.Pending,
							"456": mako.Inactive,
						}

						expectedEmoticonsToDelete := []string{"456"}

						mockPendingEmoticonsDAO.On("GetPendingEmoticonsWithFilters", mock.Anything, dynamo.PendingEmotesFilter{
							Type:        dynamo.Partner,
							ReviewState: dynamo.PendingEmoteReviewStateNone,
							After:       after,
						}, mock.Anything, mock.Anything).Return(mockDynamoResp, nil)
						mockEmoticonAggregator.On("GetEmoticonStateMapByEmoteIDs", mock.Anything, expectedIDList).Return(mockEmoteStates, nil)
						mockPendingEmoticonsDAO.On("DeletePendingEmoticons", mock.Anything, expectedEmoticonsToDelete).Return(nil)

						_, err := client.GetPendingEmoticons(context.Background(), limit, typeFilter, reviewStateFilter, assetTypeFilter, after, bucketCount, bucketNumber)

						time.Sleep(time.Millisecond * 100) // wait for goroutine

						So(err, ShouldBeNil)
						mockPendingEmoticonsDAO.AssertNumberOfCalls(t, "DeletePendingEmoticons", 1)
					})
				})

				Convey("we should return the subset of pending emotes belonging to the bucket", func() {
					typeFilter := models.PendingEmoticonFilterPartner
					reviewStateFilter := models.PendingEmoticonReviewStateFilterNone
					assetTypeFilter := models.PendingEmoteAssetTypeFilterAll
					after := time.Now().Add(-time.Minute)
					limit := 2
					bucketCount := 2
					bucketNumber := 0

					mockDynamoResp := &dynamo.GetPendingEmoticonsResponse{
						LastEvaluatedKey: nil,
						Items: []*dynamo.PendingEmote{
							{
								EmoteId: "123",
							},
							{
								EmoteId: "456",
							},
							{
								EmoteId: "789",
							},
							{
								EmoteId: "abc",
							},
							{
								EmoteId: "def",
							},
							{
								EmoteId: "ghi",
							},
						},
					}

					expectedIDList := []string{"123", "456", "789", "abc", "def", "ghi"}

					mockEmoteStates := map[string]mako.EmoteState{
						"123": mako.Pending,
						"456": mako.Pending,
						"789": mako.Pending,
						"abc": mako.PendingArchived,
						"def": mako.Pending,
						"ghi": mako.Pending,
					}

					mockPendingEmoticonsDAO.On("GetPendingEmoticonsWithFilters", mock.Anything, dynamo.PendingEmotesFilter{
						Type:        dynamo.Partner,
						ReviewState: dynamo.PendingEmoteReviewStateNone,
						After:       after,
					}, mock.Anything, mock.Anything).Return(mockDynamoResp, nil)
					mockEmoticonAggregator.On("GetEmoticonStateMapByEmoteIDs", mock.Anything, expectedIDList).Return(mockEmoteStates, nil)

					pendingEmotes, err := client.GetPendingEmoticons(context.Background(), limit, typeFilter, reviewStateFilter, assetTypeFilter, after, bucketCount, bucketNumber)

					So(err, ShouldBeNil)
					So(pendingEmotes, ShouldResemble, []*dynamo.PendingEmote{
						{
							EmoteId: "789",
						},
						{
							EmoteId: "abc",
						},
					})
				})
			})

			Convey("Test Affiliate filter", func() {
				Convey("we should get Affiliate pending emoticons", func() {
					Convey("we should get affiliate pending emotes from dynamo", func() {
						typeFilter := models.PendingEmoticonFilterAffiliate
						reviewStateFilter := models.PendingEmoticonReviewStateFilterDeferred
						assetTypeFilter := models.PendingEmoteAssetTypeFilterAll
						after := time.Now().Add(-time.Minute)
						limit := 1
						bucketCount := 1
						bucketNumber := 0

						mockDynamoResp := &dynamo.GetPendingEmoticonsResponse{
							LastEvaluatedKey: nil,
							Items: []*dynamo.PendingEmote{
								{
									EmoteId: "123",
								},
							},
						}

						expectedIDList := []string{"123"}

						mockEmoteStates := map[string]mako.EmoteState{
							"123": mako.Pending,
						}

						mockPendingEmoticonsDAO.On("GetPendingEmoticonsWithFilters", mock.Anything, dynamo.PendingEmotesFilter{
							Type:        dynamo.Affiliate,
							ReviewState: dynamo.PendingEmoteReviewStateDeferred,
							After:       after,
						}, mock.Anything, mock.Anything).Return(mockDynamoResp, nil)
						mockEmoticonAggregator.On("GetEmoticonStateMapByEmoteIDs", mock.Anything, expectedIDList).Return(mockEmoteStates, nil)

						pendingEmotes, err := client.GetPendingEmoticons(context.Background(), limit, typeFilter, reviewStateFilter, assetTypeFilter, after, bucketCount, bucketNumber)

						So(err, ShouldBeNil)
						So(pendingEmotes, ShouldResemble, []*dynamo.PendingEmote{
							{
								EmoteId: "123",
							},
						})
					})
					Convey("we should limit the returned emotes to the limit count", func() {
						limit := 30
						typeFilter := models.PendingEmoticonFilterAffiliate
						reviewStateFilter := models.PendingEmoticonReviewStateFilterDeferred
						assetTypeFilter := models.PendingEmoteAssetTypeFilterAll
						after := time.Now().Add(-time.Minute)
						bucketNumber := 0
						bucketCount := 3
						startId := 0
						totalPending := 100

						mockDynamoItems := make([]*dynamo.PendingEmote, 0, limit)
						mockDynamoStates := make(map[string]mako.EmoteState, limit)
						for i := startId; i < totalPending; i++ {
							id := strconv.Itoa(i)
							mockDynamoItems = append(mockDynamoItems, &dynamo.PendingEmote{
								EmoteId: id,
							})
							mockDynamoStates[id] = mako.Pending
						}

						mockDynamoResp := &dynamo.GetPendingEmoticonsResponse{
							LastEvaluatedKey: nil,
							Items:            mockDynamoItems,
						}

						mockPendingEmoticonsDAO.On("GetPendingEmoticonsWithFilters", mock.Anything, dynamo.PendingEmotesFilter{
							Type:        dynamo.Affiliate,
							ReviewState: dynamo.PendingEmoteReviewStateDeferred,
							After:       after,
						}, mock.Anything, mock.Anything).Return(mockDynamoResp, nil).Once()
						mockEmoticonAggregator.On("GetEmoticonStateMapByEmoteIDs", mock.Anything, mock.Anything).Return(mockDynamoStates, nil).Once()
						mockPendingEmoticonsDAO.On("DeletePendingEmoticons", mock.Anything, mock.Anything).Return(nil)

						pendingEmotes, err := client.GetPendingEmoticons(context.Background(), limit, typeFilter, reviewStateFilter, assetTypeFilter, after, bucketCount, bucketNumber)

						So(err, ShouldBeNil)
						So(pendingEmotes, ShouldHaveLength, limit)
					})
				})
			})

			Convey("Test Animated filter", func() {
				Convey("we should only get animated pending emoticons", func() {
					typeFilter := models.PendingEmoticonFilterPartner
					reviewStateFilter := models.PendingEmoticonReviewStateFilterNone
					assetTypeFilter := models.PendingEmoteAssetTypeFilterAnimated

					after := time.Now().Add(-time.Minute)
					limit := 10
					bucketCount := 1
					bucketNumber := 0

					mockDynamoResp := &dynamo.GetPendingEmoticonsResponse{
						LastEvaluatedKey: nil,
						Items: []*dynamo.PendingEmote{
							{
								EmoteId:   "123",
								AssetType: string(mako.AnimatedAssetType),
							},
							{
								EmoteId:   "456",
								AssetType: string(mako.AnimatedAssetType),
							},
							{
								EmoteId:   "789",
								AssetType: string(mako.AnimatedAssetType),
							},
						},
					}

					expectedIDList := []string{"123", "456", "789"}

					mockEmoteStates := map[string]mako.EmoteState{
						"123": mako.Pending,
						"456": mako.Pending,
						"789": mako.PendingArchived,
					}

					mockPendingEmoticonsDAO.On("GetPendingEmoticonsWithFilters", mock.Anything, dynamo.PendingEmotesFilter{
						Type:        dynamo.Partner,
						ReviewState: dynamo.PendingEmoteReviewStateNone,
						After:       after,
						AssetType:   "animated",
					}, mock.Anything, mock.Anything).Return(mockDynamoResp, nil)
					mockEmoticonAggregator.On("GetEmoticonStateMapByEmoteIDs", mock.Anything, expectedIDList).Return(mockEmoteStates, nil)

					pendingEmotes, err := client.GetPendingEmoticons(context.Background(), limit, typeFilter, reviewStateFilter, assetTypeFilter, after, bucketCount, bucketNumber)

					So(err, ShouldBeNil)
					So(pendingEmotes, ShouldResemble, []*dynamo.PendingEmote{
						{
							EmoteId:   "123",
							AssetType: string(mako.AnimatedAssetType),
						},
						{
							EmoteId:   "456",
							AssetType: string(mako.AnimatedAssetType),
						},
						{
							EmoteId:   "789",
							AssetType: string(mako.AnimatedAssetType),
						},
					})

				})
			})

			Convey("Test All filter", func() {
				Convey("we should get all pending emoticons", func() {
					Convey("we should get both affiliate and partner pending emotes from dynamo", func() {
						typeFilter := models.PendingEmoticonFilterAll
						reviewStateFilter := models.PendingEmoticonReviewStateFilterAll
						assetTypeFilter := models.PendingEmoteAssetTypeFilterAll
						after := time.Now().Add(-time.Minute)
						limit := 2
						bucketCount := 1
						bucketNumber := 0

						mockAffiliateDynamoResp := &dynamo.GetPendingEmoticonsResponse{
							LastEvaluatedKey: nil,
							Items: []*dynamo.PendingEmote{
								{
									EmoteId: "123",
								},
							},
						}

						mockPartnerDynamoResp := &dynamo.GetPendingEmoticonsResponse{
							LastEvaluatedKey: nil,
							Items: []*dynamo.PendingEmote{
								{
									EmoteId: "abc",
								},
							},
						}

						expectedAffiliateEmoteIDList := []string{"123"}
						expectedPartnerEmoteIDList := []string{"abc"}

						mockAffiliateEmoteStates := map[string]mako.EmoteState{
							"123": mako.Pending,
						}

						mockPartnerEmoteStates := map[string]mako.EmoteState{
							"abc": mako.Pending,
						}

						mockPendingEmoticonsDAO.On("GetPendingEmoticonsWithFilters", mock.Anything, dynamo.PendingEmotesFilter{
							Type:        dynamo.Affiliate,
							ReviewState: "",
							After:       after,
						}, mock.Anything, mock.Anything).Return(mockAffiliateDynamoResp, nil).Once()
						mockPendingEmoticonsDAO.On("GetPendingEmoticonsWithFilters", mock.Anything, dynamo.PendingEmotesFilter{
							Type:        dynamo.Partner,
							ReviewState: "",
							After:       after,
						}, mock.Anything, mock.Anything).Return(mockPartnerDynamoResp, nil).Once()
						mockEmoticonAggregator.On("GetEmoticonStateMapByEmoteIDs", mock.Anything, expectedAffiliateEmoteIDList).Return(mockAffiliateEmoteStates, nil).Once()
						mockEmoticonAggregator.On("GetEmoticonStateMapByEmoteIDs", mock.Anything, expectedPartnerEmoteIDList).Return(mockPartnerEmoteStates, nil).Once()

						pendingEmotes, err := client.GetPendingEmoticons(context.Background(), limit, typeFilter, reviewStateFilter, assetTypeFilter, after, bucketCount, bucketNumber)

						So(err, ShouldBeNil)
						So(pendingEmotes, ShouldResemble, []*dynamo.PendingEmote{
							{
								EmoteId: "abc",
							},
							{
								EmoteId: "123",
							},
						})
					})
					Convey("we should limit the returned emotes to the limit count", func() {
						limit := 30
						typeFilter := models.PendingEmoticonFilterAll
						reviewStateFilter := models.PendingEmoticonReviewStateFilterAll
						assetTypeFilter := models.PendingEmoteAssetTypeFilterAll
						after := time.Now().Add(-time.Minute)
						bucketNumber := 0
						bucketCount := 3

						affiliateStartId := 0
						partnerStartId := 100
						total := 100

						createMockPendingEmotes := func(start, limit int) ([]*dynamo.PendingEmote, map[string]mako.EmoteState) {
							mockDynamoItems := make([]*dynamo.PendingEmote, 0, limit)
							mockDynamoStates := make(map[string]mako.EmoteState, limit)
							for i := start; i < total; i++ {
								id := strconv.Itoa(i)
								mockDynamoItems = append(mockDynamoItems, &dynamo.PendingEmote{
									EmoteId: id,
								})
								mockDynamoStates[id] = mako.Pending
							}
							return mockDynamoItems, mockDynamoStates
						}

						mockAffiliateDynamoItems, mockAffiliateEmoteState := createMockPendingEmotes(affiliateStartId, 40)
						mockPartnerDynamoItems, mockPartnerEmoteState := createMockPendingEmotes(partnerStartId, 10)

						mockAffiliateDynamoResp := &dynamo.GetPendingEmoticonsResponse{
							LastEvaluatedKey: nil,
							Items:            mockAffiliateDynamoItems,
						}

						mockPartnerDynamoResp := &dynamo.GetPendingEmoticonsResponse{
							LastEvaluatedKey: nil,
							Items:            mockPartnerDynamoItems,
						}

						mockPendingEmoticonsDAO.On("GetPendingEmoticonsWithFilters", mock.Anything, dynamo.PendingEmotesFilter{
							Type:        dynamo.Partner,
							ReviewState: "",
							After:       after,
						}, mock.Anything, mock.Anything).Return(mockPartnerDynamoResp, nil).Once()
						mockPendingEmoticonsDAO.On("GetPendingEmoticonsWithFilters", mock.Anything, dynamo.PendingEmotesFilter{
							Type:        dynamo.Affiliate,
							ReviewState: "",
							After:       after,
						}, mock.Anything, mock.Anything).Return(mockAffiliateDynamoResp, nil).Once()
						mockEmoticonAggregator.On("GetEmoticonStateMapByEmoteIDs", mock.Anything, mock.Anything).Return(mockPartnerEmoteState, nil).Once()
						mockEmoticonAggregator.On("GetEmoticonStateMapByEmoteIDs", mock.Anything, mock.Anything).Return(mockAffiliateEmoteState, nil).Once()
						mockPendingEmoticonsDAO.On("DeletePendingEmoticons", mock.Anything, mock.Anything).Return(nil)

						pendingEmotes, err := client.GetPendingEmoticons(context.Background(), limit, typeFilter, reviewStateFilter, assetTypeFilter, after, bucketCount, bucketNumber)

						So(err, ShouldBeNil)
						So(pendingEmotes, ShouldHaveLength, limit)
					})
				})
			})
		})

		Convey("Test CreatePendingEmoticon", func() {
			Convey("when PendingEmoticonsDAO.CreatePendingEmoticon returns an error, we should return an error", func() {
				mockPendingEmoticonsDAO.On("CreatePendingEmoticon", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("ERROR"))

				_, err := client.CreatePendingEmoticon(context.Background(), "123", "michael", "Zoomer", "some-owner-id", dynamo.Affiliate, dynamo.PendingEmoteReviewStateNone, mako.StaticAssetType, mako.NoTemplate)

				So(err, ShouldNotBeNil)
			})

			Convey("when PendingEmoticonsDAO.CreatePendingEmoticon returns a nil pending emoticon, we should return an error", func() {
				mockPendingEmoticonsDAO.On("CreatePendingEmoticon", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil, nil)

				_, err := client.CreatePendingEmoticon(context.Background(), "123", "michael", "Zoomer", "some-owner-id", dynamo.Affiliate, dynamo.PendingEmoteReviewStateNone, mako.StaticAssetType, mako.NoTemplate)

				So(err, ShouldNotBeNil)
			})

			Convey("when PendingEmoticonsDAO.CreatePendingEmoticon returns the pending emoticon, we should return it", func() {
				mockPendingEmoticon := dynamo.PendingEmote{}

				mockPendingEmoticonsDAO.On("CreatePendingEmoticon", mock.Anything, "123", "michael", "Zoomer", "some-owner-id", dynamo.Affiliate, mock.Anything, mock.Anything, mock.Anything).Return(&mockPendingEmoticon, nil)

				resp, err := client.CreatePendingEmoticon(context.Background(), "123", "michael", "Zoomer", "some-owner-id", dynamo.Affiliate, dynamo.PendingEmoteReviewStateNone, mako.StaticAssetType, mako.NoTemplate)

				So(err, ShouldBeNil)
				So(resp, ShouldResemble, mockPendingEmoticon)
			})
		})
	})
}
