package pendingemoticons

import (
	"context"
	"fmt"
	"strconv"
	"time"

	receiver "code.justin.tv/amzn/TwitchDartReceiverTwirp"
	gogorandom "code.justin.tv/commerce/gogogadget/random"
	log "code.justin.tv/commerce/logrus"
	emoticonsclient "code.justin.tv/commerce/mako/clients/emoticons"
	"code.justin.tv/commerce/mako/clients/emoticonstateupdater"
	"code.justin.tv/commerce/mako/clients/eventbus"
	"code.justin.tv/commerce/mako/clients/pendingemoticons/models"
	"code.justin.tv/commerce/mako/dynamo"
	mako "code.justin.tv/commerce/mako/internal"
	mk "code.justin.tv/commerce/mako/twirp"
	"code.justin.tv/common/spade-client-go/spade"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/cenkalti/backoff"
	"github.com/pkg/errors"
)

const (
	DartApprovedState = "approved"
	DartRejectedState = "rejected"

	DeleteEmoticonRetryMaxElapsedTimeSeconds = 2
	DeleteEmoticonRetryInitialIntervalMS     = 100
	DeleteEmoticonRetryMultiplier            = 2

	trackingTimeout = 5 * time.Second

	AutoApprovalAdminID           = "__autoapproved__"
	autoApprovalNotificationType  = "queue_crusher_emote_autoapproval_confirmation"
	emoteReviewedNotificationType = "emote_reviewed"

	reviewPendingEmoticonsMetricScope = "ReviewPendingEmoticons.%s"
)

type IPendingEmoticonsClient interface {
	GetPendingEmoticonsByIDs(ctx context.Context, emoticonIDs []string) ([]*dynamo.PendingEmote, error)
	ReviewPendingEmoticons(ctx context.Context, reviews []models.EmoticonReview, adminID string) ([]models.ReviewEmoticonResponse, error)
	DeferPendingEmoticons(ctx context.Context, emoticonIDs []string, adminID string) ([]*models.DeferPendingEmoticonResponse, error)
	GetPendingEmoticons(ctx context.Context, limit int, filter models.PendingEmoticonFilter, reviewStateFilter models.PendingEmoticonReviewStateFilter, assetTypeFilter models.PendingEmoteAssetTypeFilter, after time.Time, bucketCount int, bucketNumber int) ([]*dynamo.PendingEmote, error)
	CreatePendingEmoticon(ctx context.Context, id, prefix, code, ownerID string, accountType dynamo.AccountType, reviewState dynamo.PendingEmoteReviewState, assetType mako.EmoteAssetType, animatedTemplate mako.AnimatedEmoteTemplate) (dynamo.PendingEmote, error)
	DeletePendingEmoticons(ctx context.Context, emoticonIDs []string) error
	DeletePendingEmoticonIfExists(ctx context.Context, emoticonID string) error
}

type PendingEmoticonsClient struct {
	PendingEmoticonsDAO  dynamo.IPendingEmotesDao
	EmoticonAggregator   emoticonsclient.EmoticonAggregator
	EmoticonStateUpdater emoticonstateupdater.IEmoticonStateUpdater
	SpadeClient          spade.Client
	DartReceiverClient   receiver.Receiver
	Stats                statsd.Statter
	EmoticonPublisher    eventbus.Publisher
}

func NewPendingEmoticonsClient(
	pendingEmoticonsDAO dynamo.IPendingEmotesDao,
	emoticonAggregator emoticonsclient.EmoticonAggregator,
	emoticonStateUpdater emoticonstateupdater.IEmoticonStateUpdater,
	spadeClient spade.Client,
	dartReceiverClient receiver.Receiver,
	statsClient statsd.Statter,
	emoticonPublisher eventbus.Publisher) IPendingEmoticonsClient {

	return &PendingEmoticonsClient{
		PendingEmoticonsDAO:  pendingEmoticonsDAO,
		EmoticonAggregator:   emoticonAggregator,
		EmoticonStateUpdater: emoticonStateUpdater,
		SpadeClient:          spadeClient,
		DartReceiverClient:   dartReceiverClient,
		Stats:                statsClient,
		EmoticonPublisher:    emoticonPublisher,
	}
}

func (p *PendingEmoticonsClient) GetPendingEmoticonsByIDs(ctx context.Context, emoticonIDs []string) ([]*dynamo.PendingEmote, error) {
	return p.PendingEmoticonsDAO.GetPendingEmoticonsByIDs(ctx, emoticonIDs)
}

func (p *PendingEmoticonsClient) ReviewPendingEmoticons(ctx context.Context, reviews []models.EmoticonReview, adminID string) ([]models.ReviewEmoticonResponse, error) {
	// Get list of IDs for reviews
	emoticonIDs := make([]string, len(reviews))
	for i, review := range reviews {
		emoticonIDs[i] = review.EmoticonID
	}

	emoteStateMap, err := p.EmoticonAggregator.GetEmoticonStateMapByEmoteIDs(ctx, emoticonIDs)
	if err != nil {
		msg := "Unable to query current state of reviewed emoticons"
		log.WithField("emoticonIDs", emoticonIDs).WithError(err).Error(msg)
		return nil, errors.Wrap(err, msg)
	}

	alreadyReviewed := make([]string, 0, len(reviews))
	emoteReviewsMap := make(map[string]models.EmoticonReview, len(reviews))
	approvedActive := make([]string, 0, len(reviews))
	approvedArchived := make([]string, 0, len(reviews))
	rejected := make([]string, 0, len(reviews))
	failed := make([]string, 0, len(reviews))
	notFound := make([]string, 0, len(reviews))

	for _, review := range reviews {
		emoteReviewsMap[review.EmoticonID] = review
		state, ok := emoteStateMap[review.EmoticonID]
		if !ok {
			// We failed to look up current status in the Emotes store - we don't want to send another e-mail if it has already been
			// approved or rejected so we fail out this emote here
			notFound = append(notFound, review.EmoticonID)
		} else if !isPendingEmoteState(state) {
			// We know this has already been acted on - we can remove it from dynamo and report success back to the caller
			alreadyReviewed = append(alreadyReviewed, review.EmoticonID)
		} else {
			if review.Approve {
				// It's still pending in the Emotes store and the admin wants it approved
				if state == mako.Pending {
					// The emote is designated to become active after approval
					approvedActive = append(approvedActive, review.EmoticonID)
				} else {
					// The emote is designated to become archived after approval
					approvedArchived = append(approvedArchived, review.EmoticonID)
				}
			} else {
				// It's still pending in the Emotes store and the admin wants it rejected
				rejected = append(rejected, review.EmoticonID)
			}
		}
	}

	if len(approvedActive) > 0 {
		approvedActiveError := p.EmoticonStateUpdater.UpdateEmoticonsState(ctx, approvedActive, mako.Active)
		if approvedActiveError != nil {
			log.WithField("emoticonIDs", approvedActive).WithError(approvedActiveError).Error("Failed to approve pending emoticons")
			failed = append(failed, approvedActive...)
			approvedActive = nil
		}
	}

	if len(approvedArchived) > 0 {
		approvedArchivedError := p.EmoticonStateUpdater.UpdateEmoticonsState(ctx, approvedArchived, mako.Archived)
		if approvedArchivedError != nil {
			log.WithField("emoticonIDs", approvedArchived).WithError(approvedArchivedError).Error("Failed to approve pending_archived emoticons")
			failed = append(failed, approvedArchived...)
			approvedArchived = nil
		}
	}

	approvedAll := append(approvedActive, approvedArchived...)

	if len(rejected) > 0 {
		rejectionError := p.EmoticonStateUpdater.UpdateEmoticonsState(ctx, rejected, mako.Inactive)
		if rejectionError != nil {
			log.WithField("emoticonIDs", rejected).WithError(rejectionError).Error("Failed to reject pending emoticons")
			failed = append(failed, rejected...)
			rejected = nil
		}
	}

	var approvedEmoticons []*dynamo.PendingEmote
	var rejectedEmoticons []*dynamo.PendingEmote

	// As part of running scripts that automatically approve emotes for users deemed in good standing, we want to send a special
	// notification to users through DART.
	// TODO: Remove this once we are done running emote auto-approval scripts (check DIGI-110 in Jira for more details)
	isAutoApproval := p.areEmoteReviewsAutoApproved(adminID)

	// Keep a count to report to metrics
	skippedEmailCount := 0

	// TODO (DIGI-909): Potentially send different metadata for archived emotes once the DART template is updated
	if len(approvedAll) > 0 {
		approvedEmoticons, err = p.GetPendingEmoticonsByIDs(ctx, approvedAll)
		if err != nil {
			log.WithField("emoticonIDs", approvedAll).WithError(err).Error("Failed to get OwnerIDs to send emoticon approval emails")
		} else {
			for _, emoticon := range approvedEmoticons {
				if emoteReviewsMap[emoticon.EmoteId].ShouldSkipNotification {
					// skip emailing user
					skippedEmailCount++
					log.WithFields(log.Fields{
						"channelID": emoticon.OwnerId,
						"emoteID":   emoticon.EmoteId,
					}).Debug("Skipping emailing user about approval.")
					continue
				}
				if isAutoApproval {
					_, err := p.DartReceiverClient.PublishNotification(ctx, &receiver.PublishNotificationRequest{
						Recipient:        &receiver.PublishNotificationRequest_RecipientId{RecipientId: emoticon.OwnerId},
						NotificationType: autoApprovalNotificationType,
					})
					if err != nil {
						log.WithError(err).WithFields(log.Fields{
							"channelID": emoticon.OwnerId,
							"emoteID":   emoticon.EmoteId,
						}).Error("Failed to notify broadcaster of auto-approval of emoticon")
					}
				} else {
					_, err := p.DartReceiverClient.PublishNotification(ctx, &receiver.PublishNotificationRequest{
						Recipient:        &receiver.PublishNotificationRequest_RecipientId{RecipientId: emoticon.OwnerId},
						NotificationType: emoteReviewedNotificationType,
						Metadata: map[string]string{
							"emote_id":          emoticon.EmoteId,
							"emote_code":        emoticon.Code,
							"state":             DartApprovedState,
							"is_animated_emote": strconv.FormatBool(emoticon.AssetType == string(mako.AnimatedAssetType)),
						},
					})
					if err != nil {
						log.WithFields(log.Fields{
							"channelID": emoticon.OwnerId,
							"emoteID":   emoticon.EmoteId,
						}).WithError(err).Error("Failed to notify broadcaster of approval of emoticon")
					}
				}
			}
			go func() {
				err := p.EmoticonPublisher.PublishEmoticonCreate(context.Background(), approvedAll)
				if err != nil {
					log.WithFields(log.Fields{
						"emoteIDs": approvedAll,
					}).WithError(err).Error("failed to publish emoticon create messages")
				}
			}()
		}
	}

	if len(rejected) > 0 {
		rejectedEmoticons, err = p.GetPendingEmoticonsByIDs(ctx, rejected)
		if err != nil {
			log.WithField("emoticonIDs", rejected).WithError(err).Error("Failed to get OwnerIDs to send emoticon rejection emails")
		} else {
			if !isAutoApproval {
				for _, emoticon := range rejectedEmoticons {
					review := emoteReviewsMap[emoticon.EmoteId]
					if review.ShouldSkipNotification {
						// skip emailing user
						skippedEmailCount++
						log.WithFields(log.Fields{
							"channelID": emoticon.OwnerId,
							"emoteID":   emoticon.EmoteId,
						}).Debug("Skipping emailing user about this rejection.")
						continue
					}
					_, err := p.DartReceiverClient.PublishNotification(ctx, &receiver.PublishNotificationRequest{
						Recipient:        &receiver.PublishNotificationRequest_RecipientId{RecipientId: emoticon.OwnerId},
						NotificationType: emoteReviewedNotificationType,
						Metadata: map[string]string{
							"emote_id":          emoticon.EmoteId,
							"emote_code":        emoticon.Code,
							"state":             DartRejectedState,
							"reason":            fmt.Sprintf("%s: %s", review.RejectReason, review.RejectDescription),
							"is_animated_emote": strconv.FormatBool(emoticon.AssetType == string(mako.AnimatedAssetType)),
						},
					})
					if err != nil {
						log.WithFields(log.Fields{
							"channelID": emoticon.OwnerId,
							"emoteID":   emoticon.EmoteId,
						}).WithError(err).Error("Failed to notify broadcaster of rejection of emoticon")
					}
				}
			}
		}
	}

	if len(approvedEmoticons) > 0 || len(rejectedEmoticons) > 0 {
		// Send data science events
		go p.trackEmoteReviewEvents(approvedEmoticons, rejectedEmoticons, emoteReviewsMap, adminID, isAutoApproval)
	}

	// Delete from dynamo
	emoticonsToDelete := make([]string, 0, len(approvedAll)+len(rejected)+len(alreadyReviewed))
	emoticonsToDelete = append(emoticonsToDelete, approvedAll...)
	emoticonsToDelete = append(emoticonsToDelete, rejected...)
	emoticonsToDelete = append(emoticonsToDelete, alreadyReviewed...)
	if len(emoticonsToDelete) > 0 {
		deletePendingEmoticonsFn := func() error {
			err = p.PendingEmoticonsDAO.DeletePendingEmoticons(ctx, emoticonsToDelete)
			return err
		}

		err = backoff.Retry(deletePendingEmoticonsFn, newExponentialBackOff(
			DeleteEmoticonRetryInitialIntervalMS,
			DeleteEmoticonRetryMultiplier,
			DeleteEmoticonRetryMaxElapsedTimeSeconds,
		))
		if err != nil {
			log.WithField("emoticonIDs", emoticonsToDelete).WithError(err).Error("Failed to delete reviewed emoticons from dynamo")
		}
	}

	p.incrementStats("failed.review.count", len(failed))
	p.incrementStats("alreadyApproved.review.count", len(alreadyReviewed))
	p.incrementStats("approved.review.count", len(approvedAll))
	p.incrementStats("rejected.review.count", len(rejected))
	p.incrementStats("notFound.review.count", len(notFound))
	p.incrementStats("skippedEmails.review.count", skippedEmailCount)

	// Build response
	resp := make([]models.ReviewEmoticonResponse, 0, len(reviews))
	for _, emoticon := range approvedActive {
		resp = append(resp, models.ReviewEmoticonResponse{
			EmoticonID: emoticon,
			Succeeded:  true,
			NewState:   mako.Active,
		})
	}
	for _, emoticon := range approvedArchived {
		resp = append(resp, models.ReviewEmoticonResponse{
			EmoticonID: emoticon,
			Succeeded:  true,
			NewState:   mako.Archived,
		})
	}
	for _, emoticon := range rejected {
		resp = append(resp, models.ReviewEmoticonResponse{
			EmoticonID: emoticon,
			Succeeded:  true,
			NewState:   mako.Inactive,
		})
	}
	for _, emoticon := range alreadyReviewed {
		resp = append(resp, models.ReviewEmoticonResponse{
			EmoticonID: emoticon,
			Succeeded:  true,
			NewState:   emoteStateMap[emoticon],
		})
	}
	for _, emoticon := range failed {
		resp = append(resp, models.ReviewEmoticonResponse{
			EmoticonID: emoticon,
			Succeeded:  false,
			NewState:   emoteStateMap[emoticon],
		})
	}
	for _, emoticon := range notFound {
		resp = append(resp, models.ReviewEmoticonResponse{
			EmoticonID: emoticon,
			Succeeded:  false,
			NewState:   mako.Unknown,
		})
	}

	return resp, nil
}

func (p *PendingEmoticonsClient) trackEmoteReviewEvents(approvedEmotes []*dynamo.PendingEmote, rejectedEmotes []*dynamo.PendingEmote, emoteReviewsMap map[string]models.EmoticonReview, adminID string, isAutoApproval bool) {
	ctx, cancel := context.WithTimeout(context.Background(), trackingTimeout)
	defer cancel()

	trackingTime := time.Now().Unix()
	var eventsToTrack []spade.Event

	allEmotes := append(approvedEmotes, rejectedEmotes...)

	emoteTypeMap, err := p.getEmoteTypesForTracking(ctx, allEmotes)
	if err != nil {
		var emoteIDs []string
		for _, emote := range allEmotes {
			emoteIDs = append(emoteIDs, emote.EmoteId)
		}
		log.WithField("emoteIDs", emoteIDs).WithError(err).Error("Failed to get emote types for tracking review events")
		return
	}

	for _, emote := range approvedEmotes {
		review := emoteReviewsMap[emote.EmoteId]
		eventsToTrack = append(eventsToTrack, spade.Event{
			Name: models.EmoticonReviewEventName,
			Properties: models.EmoticonReviewEvent{
				Time:                   trackingTime,
				ChannelID:              emote.OwnerId,
				LDAPReviewerID:         adminID,
				EmoteID:                emote.EmoteId,
				EmotePrefix:            emote.Prefix,
				EmoteSuffix:            emote.Code,
				EmoteRegex:             emote.Prefix + emote.Code,
				Action:                 models.EmoticonReviewEventActionApprove,
				EmoteType:              emoteTypeMap[emote.EmoteId],
				IsAutoApproval:         isAutoApproval,
				AssetType:              emote.AssetType,
				ShouldSkipNotification: review.ShouldSkipNotification,
			},
		})
	}

	for _, emote := range rejectedEmotes {
		review := emoteReviewsMap[emote.EmoteId]
		eventsToTrack = append(eventsToTrack, spade.Event{
			Name: models.EmoticonReviewEventName,
			Properties: models.EmoticonReviewEvent{
				Time:                   trackingTime,
				ChannelID:              emote.OwnerId,
				LDAPReviewerID:         adminID,
				EmoteID:                emote.EmoteId,
				EmotePrefix:            emote.Prefix,
				EmoteSuffix:            emote.Code,
				EmoteRegex:             emote.Prefix + emote.Code,
				Action:                 models.EmoticonReviewEventActionReject,
				RejectionReason:        review.RejectReason,
				EmoteType:              emoteTypeMap[emote.EmoteId],
				IsAutoApproval:         isAutoApproval,
				AssetType:              emote.AssetType,
				ShouldSkipNotification: review.ShouldSkipNotification,
			},
		})
	}

	if len(eventsToTrack) > 0 {
		err := p.SpadeClient.TrackEvents(ctx, eventsToTrack...)
		if err != nil {
			log.WithError(err).Error("Failed to track emote review spade events")
		}
	}
}

func (p *PendingEmoticonsClient) getEmoteTypesForTracking(ctx context.Context, pendingEmotes []*dynamo.PendingEmote) (map[string]string, error) {
	var pendingEmoteIDs []string
	for _, pendingEmote := range pendingEmotes {
		pendingEmoteIDs = append(pendingEmoteIDs, pendingEmote.EmoteId)
	}

	emotes, err := p.EmoticonAggregator.GetEmoticonsByEmoteIDs(ctx, pendingEmoteIDs)
	if err != nil {
		return nil, err
	}

	emoteMap := map[string]mako.Emote{}
	for _, emote := range emotes {
		emoteMap[emote.ID] = emote
	}

	emoteTypeMap := map[string]string{}

	for _, pendingEmote := range pendingEmotes {
		if emote, ok := emoteMap[pendingEmote.EmoteId]; ok {
			if emote.EmotesGroup == mk.EmoteGroup_Subscriptions.String() {
				emoteTypeMap[pendingEmote.EmoteId] = models.EmoticonReviewEventEmoteTypeSubscription
			} else if emote.EmotesGroup == mk.EmoteGroup_BitsBadgeTierEmotes.String() {
				emoteTypeMap[pendingEmote.EmoteId] = models.EmoticonReviewEventEmoteTypeBitsBadgeTier
			} else if emote.EmotesGroup == mk.EmoteGroup_Archive.String() {
				emoteTypeMap[pendingEmote.EmoteId] = models.EmoticonReviewEventEmoteTypeArchive
			} else {
				// Unexpected type
				log.WithField("emoteID", pendingEmote.EmoteId).Warn("Found unexpected emote type during pending emote event tracking")
			}
		}
	}

	return emoteTypeMap, nil
}

func (p *PendingEmoticonsClient) areEmoteReviewsAutoApproved(adminID string) bool {
	// The way we are differentiating auto-approvals from the usual manual approvals is through the adminID, which is set to a specific constant.
	// TODO: Remove this once we are done running emote auto-approval scripts (check DIGI-110 in Jira for more details)
	return adminID == AutoApprovalAdminID
}

func (p *PendingEmoticonsClient) DeferPendingEmoticons(ctx context.Context, emoticonIDs []string, adminID string) ([]*models.DeferPendingEmoticonResponse, error) {
	emoteStateMap, err := p.EmoticonAggregator.GetEmoticonStateMapByEmoteIDs(ctx, emoticonIDs)
	if err != nil {
		msg := "Failed to get current state of deferred emoticons"
		log.WithField("emoticonIDs", emoticonIDs).WithError(err).Error(msg)
		return nil, errors.Wrap(err, msg)
	}

	var deferredEmoticonIDs, failedEmoticonIDs []string

	for _, emoticonID := range emoticonIDs {
		state, ok := emoteStateMap[emoticonID]
		if !ok {
			// Failed to look up current status in the Emotes store
			failedEmoticonIDs = append(failedEmoticonIDs, emoticonID)
		} else if !isPendingEmoteState(state) {
			// The emote is not in pending or pending_archived state, so we cannot defer it
			failedEmoticonIDs = append(failedEmoticonIDs, emoticonID)
		} else {
			deferredEmoticonIDs = append(deferredEmoticonIDs, emoticonID)
		}
	}

	deferredEmoticons, getErr := p.GetPendingEmoticonsByIDs(ctx, deferredEmoticonIDs)
	if getErr != nil {
		msg := "Failed to get pending emoticons to defer"
		log.WithField("emoticonIDs", deferredEmoticonIDs).WithError(getErr).Error(msg)
		return nil, errors.Wrap(getErr, msg)
	}

	updateErr := p.PendingEmoticonsDAO.UpdatePendingEmoticonsReviewState(ctx, deferredEmoticons, dynamo.PendingEmoteReviewStateDeferred)
	if updateErr != nil {
		msg := "Failed to update pending emoticons review state to deferred"
		log.WithField("emoticonIDs", deferredEmoticonIDs).WithError(updateErr).Error(msg)
		return nil, errors.Wrap(updateErr, msg)
	}

	if len(deferredEmoticons) > 0 {
		// Send data science events
		go p.trackEmoteReviewDeferEvents(deferredEmoticons, adminID)
	}

	// Build response
	resp := make([]*models.DeferPendingEmoticonResponse, 0, len(emoticonIDs))
	for _, emoticonID := range deferredEmoticonIDs {
		resp = append(resp, &models.DeferPendingEmoticonResponse{
			EmoticonID: emoticonID,
			Succeeded:  true,
		})
	}
	for _, emoticonID := range failedEmoticonIDs {
		resp = append(resp, &models.DeferPendingEmoticonResponse{
			EmoticonID: emoticonID,
			Succeeded:  false,
		})
	}

	return resp, nil
}

func (p *PendingEmoticonsClient) trackEmoteReviewDeferEvents(deferredEmotes []*dynamo.PendingEmote, adminID string) {
	ctx, cancel := context.WithTimeout(context.Background(), trackingTimeout)
	defer cancel()

	trackingTime := time.Now().Unix()
	var eventsToTrack []spade.Event

	emoteTypeMap, err := p.getEmoteTypesForTracking(ctx, deferredEmotes)
	if err != nil {
		var emoteIDs []string
		for _, emote := range deferredEmotes {
			emoteIDs = append(emoteIDs, emote.EmoteId)
		}
		log.WithField("emoteIDs", emoteIDs).WithError(err).Error("Failed to get emote types for tracking defer events")
		return
	}

	for _, emote := range deferredEmotes {
		eventsToTrack = append(eventsToTrack, spade.Event{
			Name: models.EmoticonReviewDeferEventName,
			Properties: models.EmoticonReviewDeferEvent{
				Time:           trackingTime,
				ChannelID:      emote.OwnerId,
				LDAPReviewerID: adminID,
				EmoteID:        emote.EmoteId,
				EmotePrefix:    emote.Prefix,
				EmoteSuffix:    emote.Code,
				EmoteRegex:     emote.Prefix + emote.Code,
				EmoteType:      emoteTypeMap[emote.EmoteId],
			},
		})
	}

	if len(eventsToTrack) > 0 {
		err := p.SpadeClient.TrackEvents(ctx, eventsToTrack...)
		if err != nil {
			log.WithError(err).Error("Failed to track emote review defer spade events")
		}
	}
}

/**
Returns PendingEmoticons, optionally filterable by account type. Optionally returns a subset (aka bucket) of the pending
emotes to simplify/remove deduplication of emote approval parallel work.

Examples:

# All Emotes
PendingEmote{1, 2, 3, 4}

# 1 bucket
BucketCount=1, BucketNumber=0: Returns PendingEmote{1, 2, 3, 4}

# 2 buckets
BucketCount=2, BucketNumber=1: Returns PendingEmote{2, 3}
BucketCount=2, BucketNumber=1: Returns PendingEmote{1, 4}

# 4 buckets
BucketCount=4, BucketNumber=0: Returns PendingEmote{2}
BucketCount=4, BucketNumber=1: Returns PendingEmote{3}
BucketCount=4, BucketNumber=2: Returns PendingEmote{1}
BucketCount=4, BucketNumber=3: Returns PendingEmote{4}
*/
func (p *PendingEmoticonsClient) GetPendingEmoticons(ctx context.Context, limit int, filter models.PendingEmoticonFilter, reviewStateFilter models.PendingEmoticonReviewStateFilter, assetTypeFilter models.PendingEmoteAssetTypeFilter, after time.Time, bucketCount int, bucketNumber int) ([]*dynamo.PendingEmote, error) {
	var emoticons []*dynamo.PendingEmote
	var err error

	var reviewState dynamo.PendingEmoteReviewState
	switch reviewStateFilter {
	case models.PendingEmoticonReviewStateFilterNone:
		reviewState = dynamo.PendingEmoteReviewStateNone
	case models.PendingEmoticonReviewStateFilterDeferred:
		reviewState = dynamo.PendingEmoteReviewStateDeferred
	default:
		// No filter. Ignore review state when fetching pending emotes.
		reviewState = ""
	}

	var assetType mako.EmoteAssetType

	switch assetTypeFilter {
	case models.PendingEmoteAssetTypeFilterStatic:
		assetType = mako.StaticAssetType
	case models.PendingEmoteAssetTypeFilterAnimated:
		assetType = mako.AnimatedAssetType
	default:
		// This handles the All filter
		assetType = ""
	}

	switch filter {
	case models.PendingEmoticonFilterPartner:
		emoticons, err = p.getPendingEmoticonsWithFilters(ctx, dynamo.Partner, reviewState, assetType, after, limit, bucketCount, bucketNumber)
		if err != nil {
			return nil, errors.Wrap(err, "Unable to get pending partner emoticons from dynamo")
		}
	case models.PendingEmoticonFilterAffiliate:
		emoticons, err = p.getPendingEmoticonsWithFilters(ctx, dynamo.Affiliate, reviewState, assetType, after, limit, bucketCount, bucketNumber)
		if err != nil {
			return nil, errors.Wrap(err, "Unable to get pending affiliate emoticons from dynamo")
		}
	case models.PendingEmoticonFilterAll:
		emoticons, err = p.getPendingEmoticonsWithFilters(ctx, dynamo.Partner, reviewState, assetType, after, limit, bucketCount, bucketNumber)
		if err != nil {
			// We failed to get partner emotes but we might be able to get affiliate emotes
			emoticons = make([]*dynamo.PendingEmote, 0, limit)
			log.WithError(err).Error("Unable to get pending partner emoticons from dynamo")
		}

		remaining := limit - len(emoticons)
		if remaining > 0 {
			affiliateEmotes, err := p.getPendingEmoticonsWithFilters(ctx, dynamo.Affiliate, reviewState, assetType, after, limit, bucketCount, bucketNumber)
			if err != nil {
				// We failed to get affiliate emotes but we might be able to get partner emotes
				affiliateEmotes = make([]*dynamo.PendingEmote, 0)
				log.WithError(err).Error("Unable to get pending affiliate emoticons from dynamo")
			}

			emoticons = append(emoticons, affiliateEmotes...)
		}
	}

	if len(emoticons) > limit {
		emoticons = emoticons[:limit]
	}

	return emoticons, nil
}

func (p *PendingEmoticonsClient) getPendingEmoticonsWithFilters(ctx context.Context, accountType dynamo.AccountType, reviewState dynamo.PendingEmoteReviewState, assetType mako.EmoteAssetType, after time.Time, limit, bucketCount, bucketNumber int) ([]*dynamo.PendingEmote, error) {
	pendingEmoticons := make([]*dynamo.PendingEmote, 0, limit)
	hasMore := true
	var exclusiveStartKey dynamo.Cursor

	for len(pendingEmoticons) < limit && hasMore {
		// Get records from dynamo
		// Fetch many more than we need to guarantee we fill out the requested buckets
		dynamoResp, err := p.PendingEmoticonsDAO.GetPendingEmoticonsWithFilters(
			ctx,
			dynamo.PendingEmotesFilter{
				Type:        accountType,
				ReviewState: reviewState,
				After:       after,
				AssetType:   assetType,
			},
			int64((limit-len(pendingEmoticons))*bucketCount*2),
			exclusiveStartKey,
		)

		if err != nil {
			return nil, err
		}

		exclusiveStartKey = dynamoResp.LastEvaluatedKey
		if exclusiveStartKey == nil {
			hasMore = false
		}

		// Look up the dynamo emoticons in the emotes store
		ids := make([]string, len(dynamoResp.Items))
		for i, item := range dynamoResp.Items {
			ids[i] = item.EmoteId
		}

		emoticonStateMap, err := p.EmoticonAggregator.GetEmoticonStateMapByEmoteIDs(ctx, ids)
		if err != nil {
			return nil, err
		}

		// Build the output array with only the emoticons are still pending
		emoticonsToDelete := make([]string, 0) // Don't pre-allocate because this should be rare
		for _, item := range dynamoResp.Items {
			if isPendingEmoteState(emoticonStateMap[item.EmoteId]) {
				numericRepresentation, err := gogorandom.IntFromStringSeed(item.EmoteId, bucketCount)

				// Returns the subset of emotes belonging to the bucket
				if err != nil || numericRepresentation%bucketCount == bucketNumber {
					pendingEmoticons = append(pendingEmoticons, item)
				}
			} else {
				// If the emote is not still pending according to the Emotes store, delete it
				emoticonsToDelete = append(emoticonsToDelete, item.EmoteId)
			}
		}

		// Go clean up the emoticons that have already been approved or rejected but are still in dynamo
		if len(emoticonsToDelete) > 0 {
			go func() {
				err := p.PendingEmoticonsDAO.DeletePendingEmoticons(context.Background(), emoticonsToDelete)
				if err != nil {
					log.WithError(err).Error(fmt.Sprintf("Background task to delete pending emoticons from dynamo failed for emoticons: %v", emoticonsToDelete))
				}
			}()

		}
	}

	return pendingEmoticons, nil
}

func (p *PendingEmoticonsClient) CreatePendingEmoticon(ctx context.Context, id, prefix, code, ownerID string, accountType dynamo.AccountType, reviewState dynamo.PendingEmoteReviewState, assetType mako.EmoteAssetType, animatedTemplate mako.AnimatedEmoteTemplate) (dynamo.PendingEmote, error) {
	pendingEmoticon, err := p.PendingEmoticonsDAO.CreatePendingEmoticon(ctx, id, prefix, code, ownerID, accountType, reviewState, assetType, animatedTemplate)

	if err != nil {
		return dynamo.PendingEmote{}, err
	}

	if pendingEmoticon == nil {
		return dynamo.PendingEmote{}, errors.New("PendingEmoticonsDAO.CreatePendingEmoticon returned a nil emoticon")
	}

	return *pendingEmoticon, nil
}

func (p *PendingEmoticonsClient) DeletePendingEmoticons(ctx context.Context, emoticonIDs []string) error {
	return p.PendingEmoticonsDAO.DeletePendingEmoticons(ctx, emoticonIDs)
}

func (p *PendingEmoticonsClient) DeletePendingEmoticonIfExists(ctx context.Context, emoticonID string) error {
	pendingEmoticonToDelete := []string{emoticonID}

	pendingEmoticons, err := p.GetPendingEmoticonsByIDs(ctx, pendingEmoticonToDelete)

	if err != nil {
		return err
	}

	if len(pendingEmoticons) == 0 {
		return nil
	}

	return p.DeletePendingEmoticons(ctx, pendingEmoticonToDelete)
}

func newExponentialBackOff(initialInterval, multiplier, maxElapsedTime int) *backoff.ExponentialBackOff {
	b := backoff.NewExponentialBackOff()
	b.InitialInterval = time.Duration(initialInterval) * time.Millisecond
	b.Multiplier = float64(multiplier)
	b.MaxElapsedTime = time.Duration(maxElapsedTime) * time.Second
	b.Reset()
	return b
}

func (p *PendingEmoticonsClient) incrementStats(metric string, count int) {
	go func() {
		err := p.Stats.Inc(fmt.Sprintf(reviewPendingEmoticonsMetricScope, metric), int64(count), 1.0)
		if err != nil {
			log.WithError(err).Error("Failed to increment stats")
		}
	}()
}

func isPendingEmoteState(state mako.EmoteState) bool {
	return state == mako.Pending || state == mako.PendingArchived
}
