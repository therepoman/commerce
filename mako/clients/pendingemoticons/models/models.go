package models

import mako "code.justin.tv/commerce/mako/internal"

type EmoticonReview struct {
	EmoticonID             string
	Approve                bool
	RejectReason           string
	RejectDescription      string
	ShouldSkipNotification bool
}

type ReviewEmoticonResponse struct {
	EmoticonID string
	Succeeded  bool
	NewState   mako.EmoteState
}

type PendingEmoticonFilter string

const (
	PendingEmoticonFilterAll       PendingEmoticonFilter = "All"
	PendingEmoticonFilterAffiliate PendingEmoticonFilter = "Affiliate"
	PendingEmoticonFilterPartner   PendingEmoticonFilter = "Partner"
)

type PendingEmoticonReviewStateFilter string

const (
	PendingEmoticonReviewStateFilterAll      PendingEmoticonReviewStateFilter = "All"
	PendingEmoticonReviewStateFilterNone     PendingEmoticonReviewStateFilter = "None"
	PendingEmoticonReviewStateFilterDeferred PendingEmoticonReviewStateFilter = "Deferred"
)

type PendingEmoteAssetTypeFilter string

const (
	PendingEmoteAssetTypeFilterAll      PendingEmoteAssetTypeFilter = "All"
	PendingEmoteAssetTypeFilterStatic   PendingEmoteAssetTypeFilter = "Static"
	PendingEmoteAssetTypeFilterAnimated PendingEmoteAssetTypeFilter = "Animated"
)

type DeferPendingEmoticonResponse struct {
	EmoticonID string
	Succeeded  bool
}

// Data science models
const (
	EmoticonReviewEventName                   = "emoticon_review"
	EmoticonReviewEventActionApprove          = "approve"
	EmoticonReviewEventActionReject           = "reject"
	EmoticonReviewEventEmoteTypeSubscription  = "subscription"
	EmoticonReviewEventEmoteTypeBitsBadgeTier = "bits_badge_tier"
	EmoticonReviewEventEmoteTypeArchive       = "archive"

	EmoticonReviewDeferEventName = "emoticon_review_defer"
)

type EmoticonReviewEvent struct {
	Time                   int64  `json:"time"`
	ChannelID              string `json:"channel_id"`
	LDAPReviewerID         string `json:"ldap_reviewer_id"`
	EmoteID                string `json:"emote_id"`
	EmotePrefix            string `json:"emote_prefix"`
	EmoteSuffix            string `json:"emote_suffix"`
	EmoteRegex             string `json:"emote_regex"`
	Action                 string `json:"action"`
	RejectionReason        string `json:"rejection_reason"`
	EmoteType              string `json:"emote_type"`
	IsAutoApproval         bool   `json:"is_autoapproval"`
	AssetType              string `json:"asset_type"`
	ShouldSkipNotification bool   `json:"should_skip_notification"`
}

type EmoticonReviewDeferEvent struct {
	Time           int64  `json:"time"`
	ChannelID      string `json:"channel_id"`
	LDAPReviewerID string `json:"ldap_reviewer_id"`
	EmoteID        string `json:"emote_id"`
	EmotePrefix    string `json:"emote_prefix"`
	EmoteSuffix    string `json:"emote_suffix"`
	EmoteRegex     string `json:"emote_regex"`
	EmoteType      string `json:"emote_type"`
}
