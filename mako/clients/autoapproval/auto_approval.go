package autoapproval

import (
	"context"
	"errors"
	"fmt"
	"math/rand"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/mako/clients/emotesinference"
	"code.justin.tv/commerce/mako/clients/pepper"
	"code.justin.tv/commerce/mako/clients/ripley"
	"code.justin.tv/commerce/mako/clients/salesforce"
	mako "code.justin.tv/commerce/mako/internal"
	peppertwirp "code.justin.tv/revenue/pepper/rpc"
)

const (
	// Percentage (0 - 100) of emote upload params to sample and recommend for review anyways
	emoteUploadParamsSamplePercentage = 5
)

type EligibilityFetcher interface {
	IsUserEligibleForEmoteUploadAutoApproval(ctx context.Context, userID string) (bool, ripley.PartnerType, error)
	AreEmoteUploadParamsEligibleForAutoApproval(ctx context.Context, image4xID string, emoteCode string, userID string) (bool, error)
}

type eligibilityFetcher struct {
	Config EligibilityFetcherConfig
}

type EligibilityFetcherConfig struct {
	EmoteStandingOverrides mako.EmoteStandingOverridesDB
	PepperClient           pepper.ClientWrapper
	RipleyClient           ripley.ClientWrapper
	SalesforceClient       salesforce.APIClient
	EmotesInferenceClient  emotesinference.ClientWrapper
}

func NewEligibilityFetcher(cfg EligibilityFetcherConfig) EligibilityFetcher {
	return &eligibilityFetcher{
		Config: cfg,
	}
}

// IsUserEligibleForEmoteUploadAutoApproval checks Ripley, Salesforce, and Pepper for account status for the given user
// to determine if they are eligible for emote auto approval. Eligibility is based on Affiliate/Partner status and whether
// the account is in good standing. Also returns the partner status for use by callers.
func (e *eligibilityFetcher) IsUserEligibleForEmoteUploadAutoApproval(ctx context.Context, userID string) (bool, ripley.PartnerType, error) {
	partnerType, err := e.Config.RipleyClient.GetPartnerType(ctx, userID)
	if err != nil {
		return false, ripley.UnknownPartnerType, fmt.Errorf("error calling RipleyClient.GetPartnerType: %w", err)
	}

	override, err := e.Config.EmoteStandingOverrides.ReadByUserID(ctx, userID)
	if err != nil {
		// If we cannot look up if the user has an override in dynamo, then default back to whatever Ripley has on the
		// backend and log errors that we can alarm on
		log.WithError(err).WithField("userID", userID).Error("unable to get emote standing override from dynamo")
	}

	if override != nil && override.Override != "" {
		switch override.Override {
		case mako.InEmoteGoodStanding:
			return true, partnerType, nil
		case mako.NotInEmoteGoodStanding:
			return false, partnerType, nil
		default:
			// If we get back an invalid override, log the error so we can alarm on it.
			// Otherwise proceed as though no override exists and ask for the status accordingly
			log.WithField("userID", userID).WithField("override", override.Override).Error("invalid override returned from emote_standing_overrides table")
		}
	}

	switch {
	case ripley.IsPartner(partnerType):
		return e.isPartnerInEmoteGoodStanding(ctx, partnerType, userID)
	case ripley.IsAffiliate(partnerType):
		return e.isAffiliateInEmoteGoodStanding(ctx, partnerType, userID)
	default:
		return false, partnerType, nil
	}
}

func (e *eligibilityFetcher) isPartnerInEmoteGoodStanding(ctx context.Context, partnerType ripley.PartnerType, userID string) (bool, ripley.PartnerType, error) {
	isInGoodStanding, err := e.Config.SalesforceClient.InGoodStanding(ctx, userID)
	if err != nil {
		return false, partnerType, fmt.Errorf("error calling SalesforceClient.InGoodStanding: %w", err)
	}

	if isInGoodStanding == nil {
		msg := "unexpected nil response from Salesforce"
		log.WithField("userID", userID).Error(msg)
		return false, partnerType, errors.New(msg)
	}

	return *isInGoodStanding, partnerType, err
}

func (e *eligibilityFetcher) isAffiliateInEmoteGoodStanding(ctx context.Context, partnerType ripley.PartnerType, userID string) (bool, ripley.PartnerType, error) {
	resp, err := e.Config.PepperClient.GetUserRiskEvaluation(ctx, userID, peppertwirp.EvaluationType_AFFILIATE_EMOTE_GOOD_STANDING)
	if err != nil {
		return false, partnerType, fmt.Errorf("error calling PepperClient.GetUserRiskEvaluation: %w", err)
	}

	switch resp {
	case peppertwirp.EvaluationResult_IN_EMOTE_GOOD_STANDING:
		return true, partnerType, nil
	case peppertwirp.EvaluationResult_NOT_IN_EMOTE_GOOD_STANDING:
		// Not in good emote standing is the default, so we fallthrough to the default case
		fallthrough
	default:
		return false, partnerType, nil
	}
}

// AreEmoteUploadParamsEligibleForAutoApproval checks whether the given subset of emote creation params are eligible for auto-approving
// the emote upload they come from. This eligibility is checked via the emotes inference client. If the inference service recommends
// the emote for review, then the params are not eligible for emote upload auto-approval. If it does not, we still sample a certain
// percentage of requests and return that they're not eligible.
func (e *eligibilityFetcher) AreEmoteUploadParamsEligibleForAutoApproval(ctx context.Context, image4xID string, emoteCode string, userID string) (bool, error) {
	emoteClassification, err := e.Config.EmotesInferenceClient.ClassifyEmote(ctx, image4xID, emoteCode, userID)
	if err != nil {
		return false, fmt.Errorf("error calling EmotesInferenceClient.ClassifyEmote: %w", err)
	}

	if !emoteClassification.RecommendReview {
		// If the emote params are not recommended for review, sample a certain percentage of them for review anyways
		shouldSampleParams := (rand.Float64() * 100) < emoteUploadParamsSamplePercentage
		return !shouldSampleParams, nil
	}

	return false, nil
}
