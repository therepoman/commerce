package autoapproval

import (
	"context"
	"math/rand"
	"testing"

	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/mako/clients/ripley"
	emotesinference_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/emotesinference"
	pepper_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/pepper"
	ripley_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/ripley"
	salesforce_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/salesforce"
	"code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/internal_mock"
	peppertwirp "code.justin.tv/revenue/pepper/rpc"
	emotesinference "code.justin.tv/safety/inference/rpc/emotes"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestAutoApprovalEligibilityFetcher(t *testing.T) {
	Convey("Test Auto Approval Eligibility Fetcher", t, func() {

		mockPepperClientWrapper := new(pepper_mock.ClientWrapper)
		mockRipleyClientWrapper := new(ripley_mock.ClientWrapper)
		mockSalesforceClientWrapper := new(salesforce_mock.APIClient)
		mockEmoteStandingOverrideDB := new(internal_mock.EmoteStandingOverridesDB)
		mockEmotesInferenceClientWrapper := new(emotesinference_mock.ClientWrapper)

		eligibilityFetcher := eligibilityFetcher{EligibilityFetcherConfig{
			EmoteStandingOverrides: mockEmoteStandingOverrideDB,
			PepperClient:           mockPepperClientWrapper,
			RipleyClient:           mockRipleyClientWrapper,
			SalesforceClient:       mockSalesforceClientWrapper,
			EmotesInferenceClient:  mockEmotesInferenceClientWrapper,
		}}
		testContext := context.Background()
		testChannelId := "267893713"

		Convey("Test IsUserEligibleForEmoteUploadAutoApproval", func() {
			Convey("Given ripley returns error then return false, unknown and error", func() {
				mockRipleyClientWrapper.On("GetPartnerType", testContext, testChannelId).Return(ripley.UnknownPartnerType, errors.New("dummy ripley error"))
				mockEmoteStandingOverrideDB.On("ReadByUserID", testContext, testChannelId).Return(nil, nil)

				isEligible, partnerType, err := eligibilityFetcher.IsUserEligibleForEmoteUploadAutoApproval(testContext, testChannelId)

				So(isEligible, ShouldBeFalse)
				So(partnerType, ShouldEqual, ripley.UnknownPartnerType)
				So(err, ShouldNotBeNil)
				So(err.Error(), ShouldContainSubstring, "dummy ripley error")
			})

			Convey("Given ripley returns traditional partner type", func() {
				mockRipleyClientWrapper.On("GetPartnerType", testContext, testChannelId).Return(ripley.TraditionalPartnerType, nil)
				mockEmoteStandingOverrideDB.On("ReadByUserID", mock.Anything, testChannelId).Return(nil, nil)

				Convey("Given salesforce returns error then return false and error", func() {
					mockSalesforceClientWrapper.On("InGoodStanding", testContext, testChannelId).Return(nil, errors.New("dummy salesforce error"))

					isEligible, _, err := eligibilityFetcher.IsUserEligibleForEmoteUploadAutoApproval(testContext, testChannelId)

					So(isEligible, ShouldBeFalse)
					So(err, ShouldNotBeNil)
					So(err.Error(), ShouldContainSubstring, "dummy salesforce error")
				})

				Convey("Given salesforce returns nil then return false and error", func() {
					mockSalesforceClientWrapper.On("InGoodStanding", testContext, testChannelId).Return(nil, nil)

					isEligible, _, err := eligibilityFetcher.IsUserEligibleForEmoteUploadAutoApproval(testContext, testChannelId)

					So(isEligible, ShouldBeFalse)
					So(err, ShouldNotBeNil)
				})

				Convey("Given salesforce returns true for good standing then return true and traditional partner type", func() {
					mockSalesforceClientWrapper.On("InGoodStanding", testContext, testChannelId).Return(pointers.BoolP(true), nil)

					isEligible, partnerType, err := eligibilityFetcher.IsUserEligibleForEmoteUploadAutoApproval(testContext, testChannelId)

					So(isEligible, ShouldBeTrue)
					So(partnerType, ShouldEqual, ripley.TraditionalPartnerType)
					So(err, ShouldBeNil)
				})

				Convey("Given salesforce returns false for good standing then return false and traditional partner type", func() {
					mockSalesforceClientWrapper.On("InGoodStanding", testContext, testChannelId).Return(pointers.BoolP(false), nil)

					isEligible, partnerType, err := eligibilityFetcher.IsUserEligibleForEmoteUploadAutoApproval(testContext, testChannelId)

					So(isEligible, ShouldBeFalse)
					So(partnerType, ShouldEqual, ripley.TraditionalPartnerType)
					So(err, ShouldBeNil)
				})
			})

			Convey("Given ripley returns affiliate partner type", func() {
				mockRipleyClientWrapper.On("GetPartnerType", testContext, testChannelId).Return(ripley.AffiliatePartnerType, nil)
				mockEmoteStandingOverrideDB.On("ReadByUserID", mock.Anything, testChannelId).Return(nil, nil)

				Convey("Given pepper returns error then return false and error", func() {
					mockPepperClientWrapper.On("GetUserRiskEvaluation", testContext, testChannelId, peppertwirp.EvaluationType_AFFILIATE_EMOTE_GOOD_STANDING).Return(peppertwirp.EvaluationResult_NOT_IN_EMOTE_GOOD_STANDING, errors.New("dummy pepper error"))

					isEligible, _, err := eligibilityFetcher.IsUserEligibleForEmoteUploadAutoApproval(testContext, testChannelId)

					So(isEligible, ShouldBeFalse)
					So(err, ShouldNotBeNil)
					So(err.Error(), ShouldContainSubstring, "dummy pepper error")
				})

				Convey("Given pepper returns in good standing then return true and affiliate partner type", func() {
					mockPepperClientWrapper.On("GetUserRiskEvaluation", testContext, testChannelId, peppertwirp.EvaluationType_AFFILIATE_EMOTE_GOOD_STANDING).Return(peppertwirp.EvaluationResult_IN_EMOTE_GOOD_STANDING, nil)

					isEligible, partnerType, err := eligibilityFetcher.IsUserEligibleForEmoteUploadAutoApproval(testContext, testChannelId)

					So(isEligible, ShouldBeTrue)
					So(partnerType, ShouldEqual, ripley.AffiliatePartnerType)
					So(err, ShouldBeNil)
				})

				Convey("Given pepper returns not in good standing then return true and affiliate partner type", func() {
					mockPepperClientWrapper.On("GetUserRiskEvaluation", testContext, testChannelId, peppertwirp.EvaluationType_AFFILIATE_EMOTE_GOOD_STANDING).Return(peppertwirp.EvaluationResult_NOT_IN_EMOTE_GOOD_STANDING, nil)

					isEligible, partnerType, err := eligibilityFetcher.IsUserEligibleForEmoteUploadAutoApproval(testContext, testChannelId)

					So(isEligible, ShouldBeFalse)
					So(partnerType, ShouldEqual, ripley.AffiliatePartnerType)
					So(err, ShouldBeNil)
				})
			})

			Convey("Given ripley returns non-partner partner type return false and non-partner partner type", func() {
				mockRipleyClientWrapper.On("GetPartnerType", testContext, testChannelId).Return(ripley.NonPartnerPartnerType, nil)
				mockEmoteStandingOverrideDB.On("ReadByUserID", mock.Anything, testChannelId).Return(nil, nil)

				isEligible, partnerType, err := eligibilityFetcher.IsUserEligibleForEmoteUploadAutoApproval(testContext, testChannelId)

				So(isEligible, ShouldBeFalse)
				So(partnerType, ShouldEqual, ripley.NonPartnerPartnerType)
				So(err, ShouldBeNil)
			})
		})

		Convey("Test AreEmoteUploadParamsEligibleForAutoApproval", func() {
			image4xID := "1234_4x_6683497f-4434-42ec-8ff5-1b73003ad3fe"
			emoteCode := "someCode"
			userID := "someUser"

			Convey("With an error from the emote inference client", func() {
				mockEmotesInferenceClientWrapper.On("ClassifyEmote", mock.Anything, image4xID, emoteCode, userID).Return(nil, errors.New("dummy error"))

				res, err := eligibilityFetcher.AreEmoteUploadParamsEligibleForAutoApproval(testContext, image4xID, emoteCode, userID)

				So(res, ShouldBeFalse)
				So(err, ShouldNotBeNil)
			})

			Convey("With recommendation for review from the emote inference client", func() {
				mockEmotesInferenceClientWrapper.On("ClassifyEmote", mock.Anything, image4xID, emoteCode, userID).Return(&emotesinference.ClassifyEmoteResponse{
					RecommendReview: true,
				}, nil)

				res, err := eligibilityFetcher.AreEmoteUploadParamsEligibleForAutoApproval(testContext, image4xID, emoteCode, userID)

				So(res, ShouldBeFalse)
				So(err, ShouldBeNil)
			})

			Convey("With no recommendation for review from the emote inference client, but params are sampled", func() {
				mockEmotesInferenceClientWrapper.On("ClassifyEmote", mock.Anything, image4xID, emoteCode, userID).Return(&emotesinference.ClassifyEmoteResponse{
					RecommendReview: false,
				}, nil)
				rand.Seed(9) // This seed results in a random float value less than 0.05, which samples the params consistently

				res, err := eligibilityFetcher.AreEmoteUploadParamsEligibleForAutoApproval(testContext, image4xID, emoteCode, userID)

				So(res, ShouldBeFalse)
				So(err, ShouldBeNil)
			})

			Convey("With no recommendation for review from the emote inference client, and params are not sampled", func() {
				mockEmotesInferenceClientWrapper.On("ClassifyEmote", mock.Anything, image4xID, emoteCode, userID).Return(&emotesinference.ClassifyEmoteResponse{
					RecommendReview: false,
				}, nil)
				rand.Seed(1) // This seed results in a random float value greater than 0.05, which does not sample the params consistently

				res, err := eligibilityFetcher.AreEmoteUploadParamsEligibleForAutoApproval(testContext, image4xID, emoteCode, userID)

				So(res, ShouldBeTrue)
				So(err, ShouldBeNil)
			})
		})
	})
}
