package emotecodeacceptability

import (
	"context"
	"strings"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/mako/clients/names"
	"code.justin.tv/common/spade-client-go/spade"
	namestwirp "code.justin.tv/safety/inference/rpc/names"
)

const (
	offensiveTierThreshold = namestwirp.OffensiveTier_VERY_LIKELY
)

type AcceptabilityValidator interface {
	AreEmoteCodesAcceptable(ctx context.Context, input []EmoteCodeAcceptabilityInput, userID string) ([]EmoteCodeAcceptabilityResult, error)
}

type acceptabilityValidator struct {
	NamesClient names.ClientWrapper
	SpadeClient spade.Client
}

func NewAcceptabilityValidator(namesClient names.ClientWrapper, spadeClient spade.Client) AcceptabilityValidator {
	return &acceptabilityValidator{
		NamesClient: namesClient,
		SpadeClient: spadeClient,
	}
}

// Checks if the provided emote codes and their suffixes are "acceptable". "Acceptable" here means that they do not contain any
// "bad" or offensive words within them.
func (a *acceptabilityValidator) AreEmoteCodesAcceptable(ctx context.Context, inputs []EmoteCodeAcceptabilityInput, userID string) ([]EmoteCodeAcceptabilityResult, error) {
	emoteInputsToCheck := make([]string, 0, len(inputs))
	for _, input := range inputs {
		// To cut down on the number of calls to the batch AcceptableNames API in Names service, we group together three
		// calls per emote, always in the order prefix, full code, suffix
		emoteInputsToCheck = append(emoteInputsToCheck, input.EmoteCodePrefix, input.EmoteCode, input.EmoteCodeSuffix)
	}

	namesResp, err := a.NamesClient.AcceptableNames(ctx, emoteInputsToCheck)
	if err != nil {
		return nil, err
	}

	results := make([]EmoteCodeAcceptabilityResult, 0, len(inputs))
	for i, input := range inputs {
		isPrefixUnacceptable := namesResp.Tier[3*i] >= offensiveTierThreshold     // Prefixes are under index 0, 3, 6, etc.
		isFullCodeUnacceptable := namesResp.Tier[3*i+1] >= offensiveTierThreshold // Full codes are under index 1, 4, 7, etc.
		isSuffixUnacceptable := namesResp.Tier[3*i+2] >= offensiveTierThreshold   // Suffixes are under index 2, 5, 8, etc.

		// The emote code is unacceptable if any of the following is true:
		// 1. The emote code suffix is part of the banned list of suffixes.
		// 2. The emote code suffix is deemed offensive.
		// 3. The full emote code is deemed offensive. This does not apply if the prefix by itself is offensive because this will always prevent certain users from uploading emotes.
		isAcceptable := !(isEmoteCodeSuffixBanned(input.EmoteCodeSuffix) || isSuffixUnacceptable || (isFullCodeUnacceptable && !isPrefixUnacceptable))

		results = append(results, EmoteCodeAcceptabilityResult{
			EmoteCodePrefix: input.EmoteCodePrefix,
			EmoteCode:       input.EmoteCode,
			EmoteCodeSuffix: input.EmoteCodeSuffix,
			IsAcceptable:    isAcceptable,
		})
	}

	go a.trackEmoticonAcceptabilityEvents(results, userID)

	return results, nil
}

func isEmoteCodeSuffixBanned(emoteCodeSuffix string) bool {
	return bannedEmoteCodeSuffixes[strings.ToLower(emoteCodeSuffix)]
}

func (a *acceptabilityValidator) trackEmoticonAcceptabilityEvents(results []EmoteCodeAcceptabilityResult, userID string) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	trackingTime := time.Now().Unix()
	var eventsToTrack []spade.Event

	for _, result := range results {
		if !result.IsAcceptable {
			var source string
			var automodThreshold string
			if isEmoteCodeSuffixBanned(result.EmoteCodeSuffix) {
				source = emoticonAcceptabilityEventSourceBannedSuffix
			} else {
				source = emoticonAcceptabilityEventSourceAutomod
				automodThreshold = offensiveTierThreshold.String()
			}

			eventsToTrack = append(eventsToTrack, spade.Event{
				Name: emoticonAcceptabilityEventName,
				Properties: EmoticonAcceptabilitySpadeEvent{
					Time:             trackingTime,
					ChannelID:        userID,
					EmotePrefix:      result.EmoteCodePrefix,
					EmoteSuffix:      result.EmoteCodeSuffix,
					EmoteCode:        result.EmoteCode,
					Source:           source,
					AutomodThreshold: automodThreshold,
				},
			})
		}
	}

	if len(eventsToTrack) > 0 {
		err := a.SpadeClient.TrackEvents(ctx, eventsToTrack...)
		if err != nil {
			log.WithError(err).Error("Failed to track emoticon acceptability spade events")
		}
	}
}

var bannedEmoteCodeSuffixes = map[string]bool{
	"simp":          true,
	"fu":            true,
	"ohshit":        true,
	"clappincheeks": true,
	"loser":         true,
	"choke":         true,
	"poop":          true,
	"ass":           true,
	"shit":          true,
	"turd":          true,
	"cyka":          true,
	"cykablyat":     true,
	"kkk":           true,
	"nazi":          true,
	"hitler":        true,
	"suicide":       true,
	"hentai":        true,
	"ahegao":        true,
	"retard":        true,
	"tard":          true,
	"fap":           true,
	"stalin":        true,
	"boob":          true,
	"boobs":         true,
	"fuck":          true,
	"fucking":       true,
	"bigot":         true,
	"bylat":         true,
	"bitch":         true,
	"assclap":       true,
	"penis":         true,
	"cock":          true,
	"tit":           true,
	"tits":          true,
	"vagina":        true,
	"vag":           true,
	"queef":         true,
	"cunt":          true,
	"clit":          true,
	"clitoris":      true,
	"orgasm":        true,
	"whore":         true,
	"nigga":         true,
	"slut":          true,
	"fag":           true,
	"faggot":        true,
	"nigger":        true,
	"asshole":       true,
	"motherfucker":  true,
	"arse":          true,
	"holocaust":     true,
	"putain":        true,
	"merde":         true,
	"pussy":         true,
	"pidor":         true,
	"pidaras":       true,
	"maricón":       true,
	"beaner":        true,
	"jigaboo":       true,
	"kike":          true,
	"spic":          true,
	"abo":           true,
	"chink":         true,
	"downie":        true,
	"mongoloid":     true,
	"paki":          true,
	"shemale":       true,
	"wetback":       true,
	"homo":          true,
	"semen":         true,
	"rape":          true,
	"raper":         true,
	"rapist":        true,
	"cum":           true,
	"anal":          true,
	"nig":           true,
	"milf":          true,
	"1488":          true,
	"jism":          true,
	"seigheil":      true,
	"dyke":          true,
	"cuck":          true,
	"slag":          true,
	"twink":         true,
	"downy":         true,
	"schlong":       true,
	"tranny":        true,
	"thot":          true,
	"libtard":       true,
	"dong":          true,
	"sodomy":        true,
	"incel":         true,
	"felch":         true,
	"puta":          true,
	"maricon":       true,
	"bukkake":       true,
	"futanari":      true,
	"gook":          true,
	"wank":          true,
	"wanker":        true,
	"spank":         true,
	"bdsm":          true,
	"maga":          true,
	"dumbass":       true,
	"headass":       true,
	"bastard":       true,
}
