package emotecodeacceptability

type EmoteCodeAcceptabilityInput struct {
	EmoteCodePrefix string
	EmoteCode       string
	EmoteCodeSuffix string
}

type EmoteCodeAcceptabilityResult struct {
	EmoteCodePrefix string
	EmoteCode       string
	EmoteCodeSuffix string
	IsAcceptable    bool
}

const (
	emoticonAcceptabilityEventName               = "emoticon_acceptability"
	emoticonAcceptabilityEventSourceAutomod      = "automod"
	emoticonAcceptabilityEventSourceBannedSuffix = "banned_suffix"
)

type EmoticonAcceptabilitySpadeEvent struct {
	Time             int64  `json:"time"`
	ChannelID        string `json:"channel_id"`
	EmotePrefix      string `json:"emote_prefix"`
	EmoteSuffix      string `json:"emote_suffix"`
	EmoteCode        string `json:"emote_code"`
	Source           string `json:"source"`
	AutomodThreshold string `json:"automod_threshold"`
}
