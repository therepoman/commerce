package emotecodeacceptability

import (
	"context"
	"errors"
	"testing"
	"time"

	mako "code.justin.tv/commerce/mako/internal"
	names_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/names"
	spade_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/common/spade-client-go/spade"
	namestwirp "code.justin.tv/safety/inference/rpc/names"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestEmoteCodeAcceptabilityValidator(t *testing.T) {
	Convey("Test Emote Code Acceptability Validator", t, func() {
		mockNamesClientWrapper := new(names_mock.ClientWrapper)
		mockSpadeClient := new(spade_mock.Client)

		acceptabilityValidator := acceptabilityValidator{
			NamesClient: mockNamesClientWrapper,
			SpadeClient: mockSpadeClient,
		}

		testContext := context.Background()
		testUserID := "267893713"
		testEmoteCode1 := "testEmote"
		testEmoteCode2 := "testEmote2"
		testEmoteCodeSuffix1 := "Emote"
		testEmoteCodeSuffix2 := "Emote2"

		testInput := []EmoteCodeAcceptabilityInput{
			{
				EmoteCodePrefix: mako.ExtractEmotePrefix(testEmoteCode1, testEmoteCodeSuffix1),
				EmoteCode:       testEmoteCode1,
				EmoteCodeSuffix: testEmoteCodeSuffix1,
			},
			{
				EmoteCodePrefix: mako.ExtractEmotePrefix(testEmoteCode2, testEmoteCodeSuffix2),
				EmoteCode:       testEmoteCode2,
				EmoteCodeSuffix: testEmoteCodeSuffix2,
			},
		}

		mockSpadeClient.On("TrackEvents", mock.Anything, mock.Anything, mock.Anything).Return(nil)

		Convey("Test AreEmoteCodesAcceptable", func() {
			Convey("When AcceptableNames returns an error, then error", func() {
				mockNamesClientWrapper.On("AcceptableNames", mock.Anything, []string{
					testInput[0].EmoteCodePrefix,
					testInput[0].EmoteCode,
					testInput[0].EmoteCodeSuffix,
					testInput[1].EmoteCodePrefix,
					testInput[1].EmoteCode,
					testInput[1].EmoteCodeSuffix,
				}).Return(nil, errors.New("names error"))

				resp, err := acceptabilityValidator.AreEmoteCodesAcceptable(testContext, testInput, testUserID)

				So(resp, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})

			Convey("When AcceptableNames succeeds", func() {
				Convey("When none of the emote codes are found offensive, then response should reflect this", func() {
					mockNamesClientWrapper.On("AcceptableNames", mock.Anything, []string{
						testInput[0].EmoteCodePrefix,
						testInput[0].EmoteCode,
						testInput[0].EmoteCodeSuffix,
						testInput[1].EmoteCodePrefix,
						testInput[1].EmoteCode,
						testInput[1].EmoteCodeSuffix,
					}).Return(&namestwirp.AcceptableNamesResponse{
						Tier: []namestwirp.OffensiveTier_Tier{
							namestwirp.OffensiveTier_VERY_UNLIKELY,
							namestwirp.OffensiveTier_VERY_UNLIKELY,
							namestwirp.OffensiveTier_VERY_UNLIKELY,
							namestwirp.OffensiveTier_LIKELY,
							namestwirp.OffensiveTier_LIKELY,
							namestwirp.OffensiveTier_LIKELY,
						},
					}, nil)

					resp, err := acceptabilityValidator.AreEmoteCodesAcceptable(testContext, testInput, testUserID)

					So(len(resp), ShouldEqual, 2)
					So(resp[0].IsAcceptable, ShouldBeTrue)
					So(resp[1].IsAcceptable, ShouldBeTrue)
					So(err, ShouldBeNil)

					time.Sleep(250 * time.Millisecond) // Wait for async work to be done
					mockSpadeClient.AssertNumberOfCalls(t, "TrackEvents", 0)
				})

				Convey("When one of the emote codes is found offensive", func() {
					Convey("When found offensive due to containing a banned emote code suffix, then it should be deemed unacceptable", func() {
						testEmoteCodeWithBannedSuffix := "testFuck"
						testBannedEmoteCodeSuffix := "Fuck"

						testInput := []EmoteCodeAcceptabilityInput{
							{
								EmoteCodePrefix: mako.ExtractEmotePrefix(testEmoteCode1, testEmoteCodeSuffix1),
								EmoteCode:       testEmoteCode1,
								EmoteCodeSuffix: testEmoteCodeSuffix1,
							},
							{
								EmoteCodePrefix: mako.ExtractEmotePrefix(testEmoteCodeWithBannedSuffix, testBannedEmoteCodeSuffix),
								EmoteCode:       testEmoteCodeWithBannedSuffix,
								EmoteCodeSuffix: testBannedEmoteCodeSuffix,
							},
						}

						mockNamesClientWrapper.On("AcceptableNames", mock.Anything, []string{
							testInput[0].EmoteCodePrefix,
							testInput[0].EmoteCode,
							testInput[0].EmoteCodeSuffix,
							testInput[1].EmoteCodePrefix,
							testInput[1].EmoteCode,
							testInput[1].EmoteCodeSuffix,
						}).Return(&namestwirp.AcceptableNamesResponse{
							Tier: []namestwirp.OffensiveTier_Tier{
								namestwirp.OffensiveTier_VERY_UNLIKELY,
								namestwirp.OffensiveTier_VERY_UNLIKELY,
								namestwirp.OffensiveTier_VERY_UNLIKELY,
								namestwirp.OffensiveTier_POSSIBLE,
								namestwirp.OffensiveTier_POSSIBLE,
								namestwirp.OffensiveTier_POSSIBLE,
							},
						}, nil)

						resp, err := acceptabilityValidator.AreEmoteCodesAcceptable(testContext, testInput, testUserID)

						So(len(resp), ShouldEqual, 2)
						So(resp[0].IsAcceptable, ShouldBeTrue)
						So(resp[1].IsAcceptable, ShouldBeFalse)
						So(err, ShouldBeNil)

						time.Sleep(250 * time.Millisecond) // Wait for async work to be done
						mockSpadeClient.AssertNumberOfCalls(t, "TrackEvents", 1)
					})

					Convey("When the suffix is found offensive by AcceptableNames, then it should be deemed unacceptable", func() {
						mockNamesClientWrapper.On("AcceptableNames", mock.Anything, []string{
							testInput[0].EmoteCodePrefix,
							testInput[0].EmoteCode,
							testInput[0].EmoteCodeSuffix,
							testInput[1].EmoteCodePrefix,
							testInput[1].EmoteCode,
							testInput[1].EmoteCodeSuffix,
						}).Return(&namestwirp.AcceptableNamesResponse{
							Tier: []namestwirp.OffensiveTier_Tier{
								namestwirp.OffensiveTier_VERY_UNLIKELY,
								namestwirp.OffensiveTier_VERY_UNLIKELY,
								namestwirp.OffensiveTier_VERY_UNLIKELY,
								namestwirp.OffensiveTier_POSSIBLE,
								namestwirp.OffensiveTier_POSSIBLE,
								namestwirp.OffensiveTier_VERY_LIKELY,
							},
						}, nil)

						resp, err := acceptabilityValidator.AreEmoteCodesAcceptable(testContext, testInput, testUserID)

						So(len(resp), ShouldEqual, 2)
						So(resp[0].IsAcceptable, ShouldBeTrue)
						So(resp[1].IsAcceptable, ShouldBeFalse)
						So(err, ShouldBeNil)

						time.Sleep(250 * time.Millisecond) // Wait for async work to be done
						mockSpadeClient.AssertNumberOfCalls(t, "TrackEvents", 1)
					})

					Convey("When the full code is found offensive and the prefix not offensive by AcceptableNames, then it should be deemed unacceptable", func() {
						mockNamesClientWrapper.On("AcceptableNames", mock.Anything, []string{
							testInput[0].EmoteCodePrefix,
							testInput[0].EmoteCode,
							testInput[0].EmoteCodeSuffix,
							testInput[1].EmoteCodePrefix,
							testInput[1].EmoteCode,
							testInput[1].EmoteCodeSuffix,
						}).Return(&namestwirp.AcceptableNamesResponse{
							Tier: []namestwirp.OffensiveTier_Tier{
								namestwirp.OffensiveTier_VERY_UNLIKELY,
								namestwirp.OffensiveTier_VERY_UNLIKELY,
								namestwirp.OffensiveTier_VERY_UNLIKELY,
								namestwirp.OffensiveTier_POSSIBLE,
								namestwirp.OffensiveTier_VERY_LIKELY,
								namestwirp.OffensiveTier_POSSIBLE,
							},
						}, nil)

						resp, err := acceptabilityValidator.AreEmoteCodesAcceptable(testContext, testInput, testUserID)

						So(len(resp), ShouldEqual, 2)
						So(resp[0].IsAcceptable, ShouldBeTrue)
						So(resp[1].IsAcceptable, ShouldBeFalse)
						So(err, ShouldBeNil)

						time.Sleep(250 * time.Millisecond) // Wait for async work to be done
						mockSpadeClient.AssertNumberOfCalls(t, "TrackEvents", 1)
					})

					Convey("When the full code and prefix are found offensive by AcceptableNames, then it should be deemed acceptable", func() {
						mockNamesClientWrapper.On("AcceptableNames", mock.Anything, []string{
							testInput[0].EmoteCodePrefix,
							testInput[0].EmoteCode,
							testInput[0].EmoteCodeSuffix,
							testInput[1].EmoteCodePrefix,
							testInput[1].EmoteCode,
							testInput[1].EmoteCodeSuffix,
						}).Return(&namestwirp.AcceptableNamesResponse{
							Tier: []namestwirp.OffensiveTier_Tier{
								namestwirp.OffensiveTier_VERY_UNLIKELY,
								namestwirp.OffensiveTier_VERY_UNLIKELY,
								namestwirp.OffensiveTier_VERY_UNLIKELY,
								namestwirp.OffensiveTier_VERY_LIKELY,
								namestwirp.OffensiveTier_VERY_LIKELY,
								namestwirp.OffensiveTier_POSSIBLE,
							},
						}, nil)

						resp, err := acceptabilityValidator.AreEmoteCodesAcceptable(testContext, testInput, testUserID)

						So(len(resp), ShouldEqual, 2)
						So(resp[0].IsAcceptable, ShouldBeTrue)
						So(resp[1].IsAcceptable, ShouldBeTrue)
						So(err, ShouldBeNil)

						time.Sleep(250 * time.Millisecond) // Wait for async work to be done
						// No tracking occurs when emote codes are acceptable
						mockSpadeClient.AssertNumberOfCalls(t, "TrackEvents", 0)
					})
				})
			})
		})
	})
}
