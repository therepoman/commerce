package clients

import (
	"context"
	"fmt"
	"net/http"
	"time"

	log "code.justin.tv/commerce/logrus"

	receiver "code.justin.tv/amzn/TwitchDartReceiverTwirp"
	pubsubService "code.justin.tv/chat/pubsub-go-pubclient/client"
	"code.justin.tv/commerce/mako/clients/autoapproval"
	"code.justin.tv/commerce/mako/clients/chemtrail"
	"code.justin.tv/commerce/mako/clients/emotecodeacceptability"
	"code.justin.tv/commerce/mako/clients/emotelimits"
	"code.justin.tv/commerce/mako/clients/emotesinference"
	"code.justin.tv/commerce/mako/clients/emoticongroups"
	"code.justin.tv/commerce/mako/clients/emoticons"
	makoEmoticons "code.justin.tv/commerce/mako/clients/emoticons"
	"code.justin.tv/commerce/mako/clients/emoticonsmanager"
	"code.justin.tv/commerce/mako/clients/emoticonstateupdater"
	"code.justin.tv/commerce/mako/clients/eventbus"
	"code.justin.tv/commerce/mako/clients/experiments"
	"code.justin.tv/commerce/mako/clients/following"
	"code.justin.tv/commerce/mako/clients/imageuploader"
	"code.justin.tv/commerce/mako/clients/materia"
	"code.justin.tv/commerce/mako/clients/names"
	"code.justin.tv/commerce/mako/clients/paint"
	"code.justin.tv/commerce/mako/clients/payday"
	"code.justin.tv/commerce/mako/clients/pendingemoticons"
	"code.justin.tv/commerce/mako/clients/pepper"
	"code.justin.tv/commerce/mako/clients/productemotegroups"
	"code.justin.tv/commerce/mako/clients/pubsub"
	"code.justin.tv/commerce/mako/clients/ripley"
	"code.justin.tv/commerce/mako/clients/s3"
	"code.justin.tv/commerce/mako/clients/salesforce"
	"code.justin.tv/commerce/mako/clients/sns"
	"code.justin.tv/commerce/mako/clients/spade"
	"code.justin.tv/commerce/mako/clients/subscriptions"
	"code.justin.tv/commerce/mako/clients/wrapper"
	"code.justin.tv/commerce/mako/config"
	"code.justin.tv/commerce/mako/dynamo"
	makoHystrix "code.justin.tv/commerce/mako/hystrix"
	mako "code.justin.tv/commerce/mako/internal"
	"code.justin.tv/commerce/mako/redis"
	makoRedisWrapper "code.justin.tv/commerce/mako/redis"
	"code.justin.tv/commerce/mako/sfn"
	sandstorm "code.justin.tv/commerce/sandstorm-client"
	spadeClient "code.justin.tv/common/spade-client-go/spade"
	"code.justin.tv/foundation/twitchclient"
	"code.justin.tv/systems/sandstorm/manager"
	userService "code.justin.tv/web/users-service/client/usersclient_internal"
	metricCollector "github.com/afex/hystrix-go/hystrix/metric_collector"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	awsS3 "github.com/aws/aws-sdk-go/service/s3"
	sqssdk "github.com/aws/aws-sdk-go/service/sqs"
	"github.com/aws/aws-sdk-go/service/sqs/sqsiface"
	"github.com/cactus/go-statsd-client/statsd"
	goRedis "github.com/go-redis/redis/v7"
	"github.com/pkg/errors"
)

// Clients exposes methods to use other clients
type Clients struct {
	EmoteDB                         mako.EmoteDB
	EmoteDBUncached                 mako.EmoteDB
	EntitlementDB                   mako.EntitlementDB
	MultiSourceEntitlementDB        mako.EntitlementDB
	EmoteModifiersDB                mako.EmoteModifiersDB
	EmoteGroupDB                    mako.EmoteGroupDB
	EmoteStandingOverridesDB        mako.EmoteStandingOverridesDB
	RedisCache                      makoRedisWrapper.IRedisClient
	EmoteUploadMetadataCache        mako.EmoteUploadMetadataCache
	ClearedGlobalsCache             mako.ClearedGlobalsCache
	EntitlementsDBEmoticons         makoEmoticons.IEmoticons
	SnsClient                       sns.ISNSClient
	SpadeClient                     spadeClient.Client
	Pubsub                          pubsub.IPublisher
	ImageUploader                   imageuploader.IImageUploader
	S3Client                        s3.IS3Client
	EmoticonsManager                emoticonsmanager.IEmoticonsManager
	SubscriptionsClient             subscriptions.ClientWrapper
	PaydayClient                    payday.ClientWrapper
	EmoticonsAggregator             makoEmoticons.EmoticonAggregator
	PaintClient                     paint.PaintClient
	MateriaClient                   materia.MateriaClient
	PendingEmoticonsClient          pendingemoticons.IPendingEmoticonsClient
	RipleyClient                    ripley.ClientWrapper
	PepperClient                    pepper.ClientWrapper
	AutoApprovalEligibilityFetcher  autoapproval.EligibilityFetcher
	EmoteCodeAcceptabilityValidator emotecodeacceptability.AcceptabilityValidator
	EmoteLimitsManager              emotelimits.EmoteLimitsManager
	SalesforceClient                salesforce.APIClient
	ExperimentsClient               experiments.ExperimentsClient
	UserServiceClient               userService.InternalClient
	DartReceiverClient              receiver.Receiver
	StepFunctionsClient             sfn.ClientWrapper
	SQSClient                       sqsiface.SQSAPI
	ChemtrailClient                 chemtrail.ClientWrapper
	ProductEmoteGroupsClient        productemotegroups.ProductEmoteGroups
}

type DataAccessObjects struct {
	EntitlementDao   dynamo.IEntitlementDao
	PrefixDao        dynamo.IPrefixDao
	PendingEmotesDao dynamo.IPendingEmotesDao
	SettingsDao      dynamo.ISettingsDao
}

// SetupClients initializes all the clients
func SetupClients(cfg *config.Configuration, dynamicS3Configs *config.DynamicS3Configs, dynamicConfig *config.Dynamic, stats statsd.Statter, daos DataAccessObjects) (*Clients, error) {
	sandstormConfig := sandstorm.Config{
		AWSRegion:    cfg.AWSRegion,
		Duration:     900 * time.Second,
		ExpiryWindow: 10 * time.Second,
		RoleARN:      cfg.SandstormRoleArn,
	}
	sandstorm, err := sandstorm.New(sandstormConfig)
	if err != nil {
		return nil, fmt.Errorf("Failed to connect to sandstorm %v", err)
	}

	subscriptionsClient := subscriptions.NewClient(cfg)

	emoteLimitsManager := emotelimits.NewEmoteLimitsManager(subscriptionsClient, dynamicConfig)

	goRedisClient := goRedis.NewClusterClient(&goRedis.ClusterOptions{
		Addrs:        cfg.RedisHosts,
		ReadOnly:     true,
		DialTimeout:  time.Duration(cfg.RedisDialTimeoutSeconds) * time.Second,
		ReadTimeout:  time.Duration(cfg.RedisReadTimeoutSeconds) * time.Second,
		WriteTimeout: time.Duration(cfg.RedisWriteTimeoutSeconds) * time.Second,
		PoolSize:     cfg.RedisNodeConnectionPoolSize,
		PoolTimeout:  time.Duration(cfg.RedisPoolTimeoutSeconds) * time.Second,
		MaxRetries:   cfg.RedisMaxRetryCount,
	})

	makoRedisWrapper := makoRedisWrapper.NewGoRedisClient(goRedisClient)

	awsSession, err := session.NewSession()
	if err != nil {
		return nil, err
	}

	awsS3Client := awsS3.New(awsSession, aws.NewConfig().WithRegion(cfg.AWSRegion))
	s3Client := s3.NewS3(awsS3Client)

	clearedGlobalsCache := mako.NewClearedGlobalsCache(cfg.ClearedGlobals)

	snsClient := sns.NewSnsClient()

	sqsClient := sqssdk.New(session.New(), aws.NewConfig().WithRegion(cfg.AWSRegion))
	if sqsClient == nil {
		return nil, fmt.Errorf("failed to initialize sqs client")
	}

	spadeClient, err := spade.NewSpadeClient(cfg)
	if err != nil {
		return nil, fmt.Errorf("Failed to initialize Spade client: %v", err)
	}

	transport := twitchclient.TransportConf{
		MaxIdleConnsPerHost: 100,
	}

	pubsubConfig := twitchclient.ClientConf{
		Host:      cfg.PubsubHost,
		Transport: transport,
		Stats:     stats,
		RoundTripperWrappers: []func(http.RoundTripper) http.RoundTripper{
			wrapper.NewRetryRoundTripWrapper(stats, "pubsub"),
			wrapper.NewHystrixRoundTripWrapper("pubsub", cfg.PubsubTimeoutMilliseconds),
		},
	}
	pubsubClient, err := pubsubService.NewPubClient(pubsubConfig)
	if err != nil {
		return nil, fmt.Errorf("Failed to initialize Pubsub client: %v", err)
	}

	pubsubPublisher := pubsub.NewPublisher(pubsubClient)

	var emoteGroupDB mako.EmoteGroupDB
	emoteGroupDB = dynamo.NewEmoteGroupDB(dynamo.EmoteGroupDBConfig{
		Table:  cfg.EmoteGroupDBDynamoTable,
		Region: cfg.AWSRegion,
	})
	emoteGroupDB = emoticongroups.NewEmoteGroupDB(emoteGroupDB)
	emoteGroupDB = mako.NewEmoteGroupDBWithStats(emoteGroupDB, stats, "dynamodb")
	emoteGroupDB, err = mako.NewEmoteGroupDBWithCache(emoteGroupDB, time.Minute)
	if err != nil {
		return nil, err
	}
	emoteGroupDB = mako.NewEmoteGroupDBWithStats(emoteGroupDB, stats, "total")

	emoteDB, emoteDBUncached, err := makeEmoteDBs(emoteDBConfig{
		redisAddr:                  cfg.EmoteDBCacheRedisHost,
		redisConnectionPoolSize:    cfg.EmoteRedisConnectionPoolSize,
		redisMinIdleConnections:    cfg.EmoteRedisMinIdleConnections,
		dynamoTable:                cfg.EmoteDBDynamoTable,
		region:                     cfg.AWSRegion,
		stats:                      stats,
		waitForBloomFilterOnDeploy: cfg.WaitForBloomFilterOnDeploy,
		dynamicConfig:              dynamicConfig,
		pubsub:                     pubsubPublisher,
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to make EmoteDB")
	}

	// Prime caches before taking requests
	primeCaches(emoteDB)

	emoteUploadMetadataCache := redis.NewEmoteUploadMetadataCache(redis.EmoteUploadMetadataCacheConfig{
		Addr:           cfg.EmoteDBCacheRedisHost,
		Namespace:      "emote-upload-metadata",
		ClusterEnabled: true,
	})

	entitlementsDBEmoticonsClient, err := makoEmoticons.NewEntitlementsDBEmoticons(daos.EntitlementDao, makoRedisWrapper, emoteDB, cfg)
	if err != nil {
		return nil, fmt.Errorf("Failed to initialize entitlementsDB client: %v", err)
	}

	var entitlementDB mako.EntitlementDB

	// EntitlementDB: Materia
	entitlementDB = materia.NewEntitlementDB(cfg.MateriaHost)
	entitlementDB = mako.NewEntitlementDBWithStats(entitlementDB, stats, "materia")

	// EntitlementDB: Following
	followingEntitlementDB := following.NewEntitlementDB(cfg.FollowingServiceHost, dynamicS3Configs.KnownOrVerifiedBotsDynamicS3Config, emoteGroupDB)
	followingEntitlementDB = mako.NewEntitlementDBWithStats(followingEntitlementDB, stats, "following")
	followingEntitlementDB = redis.NewFollowingEntitlementsDBWithCache(followingEntitlementDB, redis.FollowingEntitlementsDBCacheConfig{
		Addr:               cfg.EmoteDBCacheRedisHost,
		Namespace:          "following-entitlements",
		ClusterEnabled:     true,
		ConnectionPoolSize: cfg.FollowingEntitlementsDBRedisConnectionPoolSize,
		MinIdleConnections: cfg.FollowingEntitlementsDBRedisMinIdleConnections,
	})
	followingEntitlementDB = mako.NewEntitlementDBWithStats(followingEntitlementDB, stats, "following-redis")

	// EntitlementDB: Multiple with all EntitlementDBs put together
	multiSourceEntitlementDB := mako.NewMultiSourceEntitlementDB(
		entitlementDB,
		emoticons.NewLegacyEntitlementDB(entitlementsDBEmoticonsClient, dynamicS3Configs.CampaignEmoteOwnersDynamicS3Config),
		followingEntitlementDB,
	)
	multiSourceEntitlementDB = mako.NewEntitlementDBWithStats(multiSourceEntitlementDB, stats, "materia-AND-legacy-AND-following")

	var emoteModsDB mako.EmoteModifiersDB
	emoteModsDB = dynamo.NewEmoteModifierGroupDB(dynamo.EmoteModifierGroupDBConfig{
		Table:  cfg.EmoteModifierGroupsTable,
		Region: cfg.AWSRegion,
	})
	emoteModsDB = mako.NewEmoteModifiersDBWithStats(emoteModsDB, stats, "dynamodb")

	emoteModsDB = redis.NewEmoteModifiersDBWithCache(emoteModsDB, redis.EmoteModifiersDBCacheConfig{
		Addr:               cfg.EmoteDBCacheRedisHost,
		Namespace:          "emote-modifier-groups",
		ClusterEnabled:     true,
		ConnectionPoolSize: cfg.EmoteModifiersRedisConnectionPoolSize,
		MinIdleConnections: cfg.EmoteModifiersRedisMinIdleConnections,
	})
	emoteModsDB = mako.NewEmoteModifiersDBWithStats(emoteModsDB, stats, "redis")

	emoteStandingOverridesDB := dynamo.NewEmoteStandingOverrideDB(dynamo.EmoteStandingOverrideDBConfig{
		Table:  cfg.EmoteStandingOverridesTable,
		Region: cfg.AWSRegion,
	})

	imageUploaderClient, err := newImageUploaderClient(cfg, stats)
	if err != nil {
		return nil, err
	}

	experimentsClient, err := experiments.NewExperimentsClient(cfg)
	if err != nil {
		return nil, err
	}

	userServiceClient, err := userService.NewClient(twitchclient.ClientConf{
		Host:  cfg.UserServiceHost,
		Stats: stats,
	})
	if err != nil {
		return nil, err
	}

	materiaClient := materia.NewClient(cfg.MateriaHost)

	materiaEmoticons, err := makoEmoticons.NewMateria(materiaClient, cfg, dynamicConfig, emoteDB)
	if err != nil {
		return nil, err
	}

	emoticonAggregator := makoEmoticons.NewEmoticonAggregator(makoEmoticons.EmoticonAggregatorConfig{
		EntitlementsDBEmoticons: entitlementsDBEmoticonsClient,
		MateriaEmoticons:        materiaEmoticons,
		EmoteDB:                 emoteDB,
		FollowerEntitlementsDB:  followingEntitlementDB,
		MateriaDenylist:         cfg.MateriaDenylist,
		Statter:                 stats,
		DynamicConfig:           dynamicConfig,
	})

	dartReceiverClient := receiver.NewReceiverProtobufClient(cfg.DartReceiverHost, twitchclient.NewHTTPClient(twitchclient.ClientConf{
		Host: cfg.DartReceiverHost,
	}))

	emoticonStateUpdaterClient := emoticonstateupdater.NewEmoticonStateUpdater(
		emoteDB,
	)

	eventbusClientWrapper, err := eventbus.NewClient(cfg)
	if err != nil {
		return nil, err
	}

	emoticonPublisher := eventbus.NewPublisher(eventbusClientWrapper, emoticonAggregator)

	pendingEmoticonsClient := pendingemoticons.NewPendingEmoticonsClient(
		daos.PendingEmotesDao,
		emoticonAggregator,
		emoticonStateUpdaterClient,
		spadeClient,
		dartReceiverClient,
		stats,
		emoticonPublisher,
	)

	ripleyClient := ripley.NewClient(*cfg)

	// Pepper client
	pepperClient, err := pepper.NewClient(*cfg)
	if err != nil {
		return nil, err
	}

	salesforceClient, err := getSalesforceClient(cfg, sandstorm)
	if err != nil {
		return nil, err
	}

	autoApprovalEligibilityFetcher := autoapproval.NewEligibilityFetcher(autoapproval.EligibilityFetcherConfig{
		PepperClient:           pepperClient,
		RipleyClient:           ripleyClient,
		SalesforceClient:       salesforceClient,
		EmoteStandingOverrides: emoteStandingOverridesDB,
		EmotesInferenceClient:  emotesinference.NewClient(cfg),
	})

	sfnClient := sfn.NewSFNClient(cfg, stats)

	namesClient := names.NewClient(cfg)

	emoteCodeAcceptabilityValidator := emotecodeacceptability.NewAcceptabilityValidator(namesClient, spadeClient)

	emoteManagerS3Uploader := &emoticonsmanager.EmoteManagerS3UploaderImpl{
		S3Client:                       s3Client,
		AnimatedEmoteFrameS3BucketName: cfg.AnimatedEmoteFramesS3BucketName,
		UploadS3BucketName:             cfg.UploadS3BucketName,
	}

	animatedEmoteManager := &emoticonsmanager.AnimatedEmoteManagerImpl{
		AnimatedEmoteFrameS3BucketName: cfg.AnimatedEmoteFramesS3BucketName,
		S3Client:                       s3Client,
		EmoteManagerS3Uploader:         emoteManagerS3Uploader,
		SFNClient:                      sfnClient,
	}

	paintClient := paint.NewClient(cfg.PaintHost)

	emoticonsManager := &emoticonsmanager.EmoticonsManagerImpl{
		S3Client:                        s3Client,
		UploadS3BucketName:              cfg.UploadS3BucketName,
		Stats:                           stats,
		EmoteDB:                         emoteDB,
		EmoteDBUncached:                 emoteDBUncached,
		EmoteGroupDB:                    emoteGroupDB,
		PrefixDao:                       daos.PrefixDao,
		PendingEmoticonsClient:          pendingEmoticonsClient,
		AutoApprovalEligibilityFetcher:  autoApprovalEligibilityFetcher,
		EmoticonPublisher:               emoticonPublisher,
		EmoteCodeAcceptabilityValidator: emoteCodeAcceptabilityValidator,
		EmoteLimitsManager:              emoteLimitsManager,
		UploadMetadataCache:             emoteUploadMetadataCache,
		SpadeClient:                     spadeClient,
		DynamicConfig:                   dynamicConfig,
		AnimatedEmoteManager:            animatedEmoteManager,
		EmoteManagerS3Uploader:          emoteManagerS3Uploader,
		PaintClient:                     paintClient,
		SQSClient:                       sqsClient,
		DeleteS3ImagesQueueURL:          cfg.DeleteS3ImagesQueueURL,
		DefaultImageLibraryS3Bucket:     cfg.DefaultImageLibraryS3Bucket,
	}

	chemtrailClient, err := chemtrail.NewChemtrailClient(cfg)
	if err != nil {
		return nil, err
	}
	paydayClient := payday.NewClient(cfg)

	productEmoteGroupsClient := productemotegroups.NewProductEmoteGroups(paydayClient, subscriptionsClient, emoteGroupDB)

	configureHystrix(cfg, stats)

	return &Clients{
		EmoteDB:                         emoteDB,
		EmoteDBUncached:                 emoteDBUncached,
		EntitlementDB:                   entitlementDB,
		MultiSourceEntitlementDB:        multiSourceEntitlementDB,
		EmoteModifiersDB:                emoteModsDB,
		EmoteGroupDB:                    emoteGroupDB,
		EmoteStandingOverridesDB:        emoteStandingOverridesDB,
		RedisCache:                      makoRedisWrapper,
		EmoteUploadMetadataCache:        emoteUploadMetadataCache,
		ClearedGlobalsCache:             clearedGlobalsCache,
		EntitlementsDBEmoticons:         entitlementsDBEmoticonsClient,
		SnsClient:                       snsClient,
		SpadeClient:                     spadeClient,
		Pubsub:                          pubsubPublisher,
		ImageUploader:                   imageUploaderClient,
		S3Client:                        s3Client,
		EmoticonsManager:                emoticonsManager,
		SubscriptionsClient:             subscriptionsClient,
		PaydayClient:                    paydayClient,
		EmoticonsAggregator:             emoticonAggregator,
		PaintClient:                     paintClient,
		MateriaClient:                   materiaClient,
		PendingEmoticonsClient:          pendingEmoticonsClient,
		RipleyClient:                    ripleyClient,
		PepperClient:                    pepperClient,
		AutoApprovalEligibilityFetcher:  autoApprovalEligibilityFetcher,
		EmoteCodeAcceptabilityValidator: emoteCodeAcceptabilityValidator,
		EmoteLimitsManager:              emoteLimitsManager,
		SalesforceClient:                salesforceClient,
		ExperimentsClient:               experimentsClient,
		UserServiceClient:               userServiceClient,
		DartReceiverClient:              dartReceiverClient,
		StepFunctionsClient:             sfnClient,
		SQSClient:                       sqsClient,
		ChemtrailClient:                 chemtrailClient,
		ProductEmoteGroupsClient:        productEmoteGroupsClient,
	}, nil
}

func newImageUploaderClient(cfg *config.Configuration, stats statsd.Statter) (imageuploader.IImageUploader, error) {
	twitchHTTPUploadClient := twitchclient.NewHTTPClient(twitchclient.ClientConf{
		Host:  cfg.UploadServiceURL,
		Stats: stats,
	})

	imageUploaderClient, err := imageuploader.NewImageUploader(imageuploader.ImageUploaderConfig{
		UploadServiceURL:       cfg.UploadServiceURL,
		UploadCallbackSNSTopic: cfg.UploadCallbackSNSTopic,
		UploadS3BucketName:     cfg.UploadS3BucketName,
	}, twitchHTTPUploadClient)

	if err != nil {
		return nil, fmt.Errorf("Failed to initialize Image Uploader client: %v", err)
	}

	return imageUploaderClient, nil
}

func getSalesforceClient(cfg *config.Configuration, sandstorm *manager.Manager) (salesforce.APIClient, error) {
	salesforceClientID, err := sandstorm.Get(fmt.Sprintf(cfg.SalesforceClientID, cfg.SandstormEnvironment))
	if err != nil {
		return nil, err
	}

	salesforceClientSecret, err := sandstorm.Get(fmt.Sprintf(cfg.SalesforceClientSecret, cfg.SandstormEnvironment))
	if err != nil {
		return nil, err
	}

	salesforceUsername, err := sandstorm.Get(fmt.Sprintf(cfg.SalesforceUsername, cfg.SandstormEnvironment))
	if err != nil {
		return nil, err
	}

	salesforcePassword, err := sandstorm.Get(fmt.Sprintf(cfg.SalesforcePassword, cfg.SandstormEnvironment))
	if err != nil {
		return nil, err
	}

	oauthSecretConfig := salesforce.OauthSecretConfig{
		ClientID:     string(salesforceClientID.Plaintext),
		ClientSecret: string(salesforceClientSecret.Plaintext),
		Username:     string(salesforceUsername.Plaintext),
		Password:     string(salesforcePassword.Plaintext),
	}

	return salesforce.NewSalesforceAPIClient(cfg.SalesforceOauthHost, oauthSecretConfig)
}

func configureHystrix(cfg *config.Configuration, stats statsd.Statter) {
	makoHystrix.ConfigureHystrixCommands(cfg)

	collector := makoHystrix.NewHystrixMetricsCollector(stats)
	metricCollector.Registry.Register(func(name string) metricCollector.MetricCollector {
		return collector.NewHystrixCommandMetricsCollector(name)
	})
}

type emoteDBConfig struct {
	redisAddr                  string
	redisConnectionPoolSize    int
	redisMinIdleConnections    int
	dynamoTable                string
	region                     string
	stats                      statsd.Statter
	waitForBloomFilterOnDeploy bool
	pubsub                     pubsub.IPublisher
	dynamicConfig              config.DynamicConfigClient
}

func makeEmoteDBs(c emoteDBConfig) (mako.EmoteDB, mako.EmoteDB, error) {
	var emoteDB mako.EmoteDB
	var emoteDBUncached mako.EmoteDB
	var err error

	emoteDBUncached = dynamo.NewEmoteDB(dynamo.EmoteDBConfig{
		Table:  c.dynamoTable,
		Region: c.region,
	})

	emoteDBUncached = mako.NewEmoteDBWithStats(emoteDBUncached, c.stats, "dynamodb")
	emoteDBUncached = pubsub.NewEmoteDBWithPubsub(emoteDBUncached, c.pubsub, c.dynamicConfig)
	emoteDB = redis.NewEmoteDBWithCache(emoteDBUncached, redis.EmoteDBCacheConfig{
		Addr:                       c.redisAddr,
		Namespace:                  "emotes",
		ClusterEnabled:             true,
		ConnectionPoolSize:         c.redisConnectionPoolSize,
		MinIdleConnections:         c.redisMinIdleConnections,
		BloomFilter:                false,
		Statter:                    c.stats,
		WaitForBloomFilterOnDeploy: c.waitForBloomFilterOnDeploy,
	})
	emoteDB = mako.NewEmoteDBWithStats(emoteDB, c.stats, "redis")
	emoteDB, err = mako.NewEmoteDBWithCache(emoteDB, 5*time.Minute)
	if err != nil {
		return nil, nil, err
	}

	emoteDB = mako.NewEmoteDBLogicLayer(emoteDB)
	emoteDBUncached = mako.NewEmoteDBLogicLayer(emoteDBUncached)

	// HACK ALERT, we're going to wait 30 seconds for the Redis clients to initialize their idle connections
	// before we start taking traffic and handling requests.
	time.Sleep(30 * time.Second)

	return emoteDB, emoteDBUncached, nil
}

func primeCaches(emoteDB mako.EmoteDB) {
	emoteGroupIDsForCachePriming := []string{
		mako.GlobalEmoteGroupID,
		fmt.Sprint(mako.TurboEmoticonSet),
		fmt.Sprint(mako.Prime2EmoticonSet),
		fmt.Sprint(mako.PrimeEmoticonSet),
		mako.TwoFactorEmoteGroupID,
		"610186276", // Hindsight MegaCommerce Emotes
		"592920959", // Kpop MegaCommerce Emotes
		"564265402", // Hyperscape MegaCommerce Emotes
		"537206155", // Pride MegaCommerce Emotes
		"488737509", // Streamer Luv MegaCommerce Emotes
		"472873131", // Holiday MegaCommerce
		"477339272", // Hypetrain Emotes,
		"33",        // PurpleSmilie
		"42",        // MonkeysSmilie

	}
	cachePrimeContext, cachePrimeContextCancel := context.WithTimeout(context.Background(), time.Second)
	defer cachePrimeContextCancel()

	_, err := emoteDB.ReadByGroupIDs(cachePrimeContext, emoteGroupIDsForCachePriming...)
	if err != nil {
		// Failure here shouldn't prevent server startup
		log.WithError(err).Error("error priming emoteDB cache")
	}
}
