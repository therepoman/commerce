package emoticonsmanager

import (
	"context"

	"code.justin.tv/commerce/mako/clients/s3"
	"code.justin.tv/commerce/mako/sfn"
	"github.com/pkg/errors"
	"golang.org/x/sync/errgroup"
)

type AnimatedEmoteFrameSplitParams struct {
	EmoteID string `json:"emote_id"`
}

type AnimatedEmoteManagerImpl struct {
	S3Client                          s3.IS3Client
	AnimatedEmoteFrameSplitLambdaName string
	AnimatedEmoteFrameS3BucketName    string
	EmoteManagerS3Uploader            EmoteManagerS3Uploader
	SFNClient                         sfn.ClientWrapper
}

type UploadAnimatedEmoteImagesParams struct {
	EmoticonID      string
	Image1x         []byte
	Image2x         []byte
	Image4x         []byte
	Image1xAnimated []byte
	Image2xAnimated []byte
	Image4xAnimated []byte
}

type AnimatedEmoteManager interface {
	ExecuteFrameSplittingSFN(ctx context.Context, emoteID string) error
	UploadAnimatedEmoteImages(ctx context.Context, params UploadAnimatedEmoteImagesParams) error
}

func (e *AnimatedEmoteManagerImpl) ExecuteFrameSplittingSFN(ctx context.Context, emoteID string) error {
	err := e.SFNClient.ExecuteAnimatedEmoteFrameSplittingStateMachine(ctx, sfn.AnimatedEmoteFrameExtractionInput{
		EmoteID: emoteID,
	})

	if err != nil {
		return errors.Wrap(err, "failed to execute animated emote frame split sfn")
	}
	return nil
}

func (e *AnimatedEmoteManagerImpl) UploadAnimatedEmoteImages(ctx context.Context, params UploadAnimatedEmoteImagesParams) error {
	eg := errgroup.Group{}

	eg.Go(func() error {
		return e.EmoteManagerS3Uploader.UploadEmoteImage(ctx, params.EmoticonID, Image1X, params.Image1x)
	})

	eg.Go(func() error {
		return e.EmoteManagerS3Uploader.UploadEmoteImage(ctx, params.EmoticonID, Image2X, params.Image2x)
	})

	eg.Go(func() error {
		return e.EmoteManagerS3Uploader.UploadEmoteImage(ctx, params.EmoticonID, Image4X, params.Image4x)
	})

	// For animated emotes we are uploading duplicate versions for each theme mode.
	// We may be adding the ability to upload separate assets for each theme so this extra uploading ensures
	// existing emotes will still have versions for each theme when we enable that feature.
	eg.Go(func() error {
		return e.EmoteManagerS3Uploader.UploadAnimatedEmoteImage(ctx, params.EmoticonID, Image1X, params.Image1xAnimated, "light")
	})

	eg.Go(func() error {
		return e.EmoteManagerS3Uploader.UploadAnimatedEmoteImage(ctx, params.EmoticonID, Image1X, params.Image1xAnimated, "dark")
	})

	eg.Go(func() error {
		return e.EmoteManagerS3Uploader.UploadAnimatedEmoteImage(ctx, params.EmoticonID, Image2X, params.Image2xAnimated, "light")
	})

	eg.Go(func() error {
		return e.EmoteManagerS3Uploader.UploadAnimatedEmoteImage(ctx, params.EmoticonID, Image2X, params.Image2xAnimated, "dark")
	})

	eg.Go(func() error {
		return e.EmoteManagerS3Uploader.UploadAnimatedEmoteImage(ctx, params.EmoticonID, Image4X, params.Image4xAnimated, "light")
	})

	eg.Go(func() error {
		return e.EmoteManagerS3Uploader.UploadAnimatedEmoteImage(ctx, params.EmoticonID, Image4X, params.Image4xAnimated, "dark")
	})

	if err := eg.Wait(); err != nil {
		return errors.Wrap(err, "failed to upload animated emote")
	}

	return nil
}
