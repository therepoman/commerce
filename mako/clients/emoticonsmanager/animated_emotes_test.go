package emoticonsmanager_test

import (
	"context"
	"errors"
	"testing"

	. "code.justin.tv/commerce/mako/clients/emoticonsmanager"
	s3_manager_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/emoticonsmanager"
	s3_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/s3"
	sfn_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/sfn"

	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestAnimatedEmotesManager(t *testing.T) {

	mockS3Client := new(s3_mock.IS3Client)
	mockS3Uploader := new(s3_manager_mock.EmoteManagerS3Uploader)
	mockSFNWrapper := new(sfn_mock.ClientWrapper)

	animatedEmotesManager := AnimatedEmoteManagerImpl{
		S3Client:                          mockS3Client,
		AnimatedEmoteFrameSplitLambdaName: "framelambda",
		AnimatedEmoteFrameS3BucketName:    "frameS3Bucket",
		EmoteManagerS3Uploader:            mockS3Uploader,
		SFNClient:                         mockSFNWrapper,
	}

	Convey("UploadAnimatedEmoteImages", t, func() {
		Convey("InvokeFrameSplittingLambda", func() {
			Convey("Success", func() {
				mockSFNWrapper.On("ExecuteAnimatedEmoteFrameSplittingStateMachine", mock.Anything, mock.Anything).Return(nil, nil).Once()
				animatedEmotesManager.ExecuteFrameSplittingSFN(context.Background(), "abc123")
				mockSFNWrapper.AssertNumberOfCalls(t, "ExecuteAnimatedEmoteFrameSplittingStateMachine", 1)
			})
		})

		Convey("UploadAnimatedEmoteImages", func() {
			Convey("with success", func() {
				params := UploadAnimatedEmoteImagesParams{
					EmoticonID:      "abc123",
					Image1x:         []byte{},
					Image2x:         []byte{},
					Image4x:         []byte{},
					Image1xAnimated: []byte{},
					Image2xAnimated: []byte{},
					Image4xAnimated: []byte{},
				}

				mockS3Uploader.On("UploadEmoteImage", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil).Times(3)
				mockS3Uploader.On("UploadAnimatedEmoteImage", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil).Times(6)
				err := animatedEmotesManager.UploadAnimatedEmoteImages(context.Background(), params)
				So(err, ShouldBeNil)
				mockS3Uploader.AssertNumberOfCalls(t, "UploadEmoteImage", 3)
				mockS3Uploader.AssertNumberOfCalls(t, "UploadAnimatedEmoteImage", 6)
			})

			Convey("with error", func() {
				params := UploadAnimatedEmoteImagesParams{
					EmoticonID:      "abc123",
					Image1x:         []byte{},
					Image2x:         []byte{},
					Image4x:         []byte{},
					Image1xAnimated: []byte{},
					Image2xAnimated: []byte{},
					Image4xAnimated: []byte{},
				}

				mockS3Uploader.On("UploadEmoteImage", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(errors.New("err"))
				mockS3Uploader.On("UploadAnimatedEmoteImage", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)
				err := animatedEmotesManager.UploadAnimatedEmoteImages(context.Background(), params)
				So(err, ShouldNotBeNil)
			})
		})
	})
}
