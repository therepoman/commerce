package emoticonsmanager

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"regexp"
	"strings"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/mako/clients/autoapproval"
	"code.justin.tv/commerce/mako/clients/emotecodeacceptability"
	"code.justin.tv/commerce/mako/clients/emotelimits"
	"code.justin.tv/commerce/mako/clients/eventbus"
	"code.justin.tv/commerce/mako/clients/paint"
	"code.justin.tv/commerce/mako/clients/pendingemoticons"
	"code.justin.tv/commerce/mako/clients/ripley"
	"code.justin.tv/commerce/mako/clients/s3"
	"code.justin.tv/commerce/mako/config"
	"code.justin.tv/commerce/mako/dynamo"
	mako "code.justin.tv/commerce/mako/internal"
	"code.justin.tv/commerce/mako/sqs/model"
	mk "code.justin.tv/commerce/mako/twirp"
	mkd "code.justin.tv/commerce/mako/twirp/dashboard"
	"code.justin.tv/common/spade-client-go/spade"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	awsS3 "github.com/aws/aws-sdk-go/service/s3"
	sqssdk "github.com/aws/aws-sdk-go/service/sqs"
	"github.com/aws/aws-sdk-go/service/sqs/sqsiface"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/pkg/errors"
	"golang.org/x/sync/errgroup"
)

const (
	Image1X = "1X"
	Image2X = "2X"
	Image4X = "4X"
)

const statsFormat = "emoticons_manager.%s"

var (
	InvalidCodeSuffixFormat             = errors.New("Invalid Suffix")
	InvalidImageUpload                  = errors.New("Invalid Image uploaded")
	EmoticonCodeNotUnique               = errors.New("Emoticon code already used")
	EmoticonCodeDoesNotExist            = errors.New("Emoticon code does not exist")
	EmoticonCodeUnacceptable            = errors.New("Emoticon code is unacceptable")
	UserOwnedEmoteLimitReached          = errors.New("User owned emote limit was reached")
	Image1XIDNotFound                   = errors.New("Image1XID not found")
	Image2XIDNotFound                   = errors.New("Image2XID not found")
	Image4XIDNotFound                   = errors.New("Image4XID not found")
	Image1XAnimatedIDNotFound           = errors.New("Image1XID for animated not found")
	Image2XAnimatedIDNotFound           = errors.New("Image2XID for animated not found")
	Image4XAnimatedIDNotFound           = errors.New("Image4XID for animated not found")
	UserNotPermitted                    = errors.New("User not permitted")
	EmoteDoesNotExist                   = errors.New("Emote does not exist")
	GroupNotFound                       = errors.New("Group was not found or was empty")
	EmoteAssetTypeDoesNotMatchGroupType = errors.New("Emote asset type does not match the type of the emote group")
	InvalidEmoteOrders                  = errors.New("Request would result in an invalid set of orders")
	InvalidEmoteState                   = errors.New("Invalid emote state")
	SuffixRegex                         = regexp.MustCompile(`^[0-9a-zA-Z]+$`)

	// Scales contains the currently used sizes for newly created emotes.
	// We should ideally only need 1.0, 2.0, and 3.0, but iOS currently uses 4.0, so we are keeping that size to support
	// that case.
	Scales = map[string][]string{
		Image1X: {"1.0"},
		Image2X: {"2.0"},
		Image4X: {"3.0", "4.0"},
	}
	// LegacyScales contains legacy sizes that were historically created for emotes. These are kept around for use cases
	// like deletion to make sure that all sizes are accounted for.
	LegacyScales = map[string][]string{
		Image1X: {"0.5", "1.0"},
		Image2X: {"1.5", "2.0"},
		Image4X: {"2.5", "3.0", "3.5", "4.0", "5.0"},
	}

	// If an emote origin is listed in this map, then users can only upload or assign assets if that user has emote
	// auto approval enabled for their account
	onlyAutoApprovalOrigin = map[mkd.EmoteOrigin]bool{
		mkd.EmoteOrigin_Follower: true,
	}

	// OrderedEmoteStates includes the states which should be considered ordered within an emote group
	OrderedEmoteStates = []mako.EmoteState{mako.Active, mako.Pending}
)

type CreateParams struct {
	UserID            string
	CodeSuffix        string
	State             mako.EmoteState
	GroupID           string
	Image1xID         string
	Image2xID         string
	Image4xID         string
	Image1xAnimatedID string
	Image2xAnimatedID string
	Image4xAnimatedID string
	Domain            mako.Domain
	EmoteOrigin       mkd.EmoteOrigin
	IsAnimated        bool
	ShouldBeArchived  bool
	// This determines whether we should look up the channel's prefix and prepend it to the suffix to create the code.
	// This should only be passed as true for emotes not created via the emotes dashboard. (i.e. globals, hype train, etc)
	ShouldIgnorePrefix        bool
	AnimatedEmoteTemplate     mako.AnimatedEmoteTemplate
	ImageSource               mako.ImageSource
	EnforceModerationWorkflow bool
}

type DeleteParams struct {
	EmoticonID            string
	DeleteS3Images        bool
	RequiresPermittedUser bool
	UserID                string
}

type UpdateOrdersParams struct {
	UserID    string
	GroupID   string
	NewOrders map[string]int64
}

type DeleteResponse struct {
	EmoticonID string
}

// IEmoticonsManager manages the life cycle of Emoticons. It writes changes to the appropriate
// authoritative datastores.
//
// - For Bits Tier emotes, the primary datastore is the Dynamo table. There is no secondary.
// - For all other emotes, the primary datastore is still the Dynamo table.
//     SiteDB is still used for incremental emote IDs and for the legacy "all emotes" 3rd party kraken API.
type IEmoticonsManager interface {
	Create(ctx context.Context, params *CreateParams) (mako.Emote, error)
	Delete(ctx context.Context, params DeleteParams) error
	Deactivate(ctx context.Context, emoticonID string) error
	Activate(ctx context.Context, emoticonID string) error
	UpdateOrders(ctx context.Context, params UpdateOrdersParams) ([]mako.Emote, error)
	AssignToGroup(ctx context.Context, userID string, emoteID string, groupID string, emoteOrigin mkd.EmoteOrigin) (mako.Emote, error)
	RemoveFromGroup(ctx context.Context, userID string, emoteID string) (mako.Emote, error)
	UpdateEmoteCodeSuffix(ctx context.Context, emoticonID string, codeSuffix string) error
	UpdateEmoteOrigins(ctx context.Context, params UpdateOriginsParams) ([]mako.Emote, error)
}

type EmoticonsManagerImpl struct {
	S3Client                        s3.IS3Client
	PrefixDao                       dynamo.IPrefixDao
	EmoteDB                         mako.EmoteDB
	EmoteDBUncached                 mako.EmoteDB
	EmoteGroupDB                    mako.EmoteGroupDB
	UploadS3BucketName              string
	DefaultImageLibraryS3Bucket     string
	DeleteS3ImagesQueueURL          string
	Stats                           statsd.Statter
	PendingEmoticonsClient          pendingemoticons.IPendingEmoticonsClient
	AutoApprovalEligibilityFetcher  autoapproval.EligibilityFetcher
	EmoticonPublisher               eventbus.Publisher
	EmoteCodeAcceptabilityValidator emotecodeacceptability.AcceptabilityValidator
	EmoteLimitsManager              emotelimits.EmoteLimitsManager
	UploadMetadataCache             mako.EmoteUploadMetadataCache
	SpadeClient                     spade.Client
	DynamicConfig                   config.DynamicConfigClient
	AnimatedEmoteManager            AnimatedEmoteManager
	EmoteManagerS3Uploader          EmoteManagerS3Uploader
	PaintClient                     paint.PaintClient
	SQSClient                       sqsiface.SQSAPI
}

const (
	channelTypePartner    = "partner"
	channelTypeAffililate = "affiliate"
)

type emoteUploadSpadeEvent struct {
	Time              time.Time `json:"time"`
	ChannelID         string    `json:"channel_id"`
	EmoteID           string    `json:"emote_id"`
	UploadType        string    `json:"upload_type"` // Simple / Advanced
	EmoteSuffix       string    `json:"emote_suffix"`
	EmotePrefix       string    `json:"emote_prefix"`
	EmoteCode         string    `json:"emote_code"`
	EmoteType         string    `json:"emote_type"` // Subscriptions, bits etc
	EmoteGroupID      string    `json:"emote_group_id"`
	GoodStanding      bool      `json:"good_standing"`
	ChannelType       string    `json:"channel_type"` // Partner / Affiliate
	AssetType         string    `json:"asset_type"`
	AnimationTemplate string    `json:"animation_type"`
	IsAutoApproved    bool      `json:"is_auto_approved"`
	State             string    `json:"state"`
	Image1xID         string    `json:"image1x_id"`
	Image2xID         string    `json:"image2x_id"`
	Image4xID         string    `json:"image4x_id"`
	Image1xAnimatedID string    `json:"image1x_animated_id"`
	Image2xAnimatedID string    `json:"image2x_animated_id"`
	Image4xAnimatedID string    `json:"image4x_animated_id"`
}

type defaultEmoteUploadSpadeEvent struct {
	Time           time.Time `json:"time"`
	ChannelID      string    `json:"channel_id"`
	EmoteID        string    `json:"emote_id"`
	EmoteSuffix    string    `json:"emote_suffix"`
	EmotePrefix    string    `json:"emote_prefix"`
	EmoteCode      string    `json:"emote_code"`
	EmoteType      string    `json:"emote_type"` // Subscriptions, bits etc
	EmoteGroupID   string    `json:"emote_group_id"`
	GoodStanding   bool      `json:"good_standing"`
	ChannelType    string    `json:"channel_type"` // Partner / Affiliate
	AssetType      string    `json:"asset_type"`
	IsAutoApproved bool      `json:"is_auto_approved"`
	DefaultEmoteID string    `json:"default_emote_id"`
}

func (e *EmoticonsManagerImpl) validateCodeAndCodeSuffix(ctx context.Context, code, codeSuffix, userID string, enforceModerationWorkflow bool, isArchived bool) error {
	// Emote code suffix first character must be uppercase
	if codeSuffix[0] != strings.ToUpper(codeSuffix)[0] {
		return InvalidCodeSuffixFormat
	}

	// Emote code contains special characters
	if !SuffixRegex.MatchString(codeSuffix) {
		return InvalidCodeSuffixFormat
	}

	// Emote code suffix is not the suffix of the full emoticon code
	if !strings.HasSuffix(code, codeSuffix) {
		return InvalidCodeSuffixFormat
	}

	// Emote code suffix too long
	if len(codeSuffix) > mako.MaxSuffixLength {
		return InvalidCodeSuffixFormat
	}

	eg := errgroup.Group{}

	eg.Go(func() error {
		isUniqueAmongActiveAndPendingEmotes, isUniqueToUser, err := e.checkEmoteCodeUniqueness(ctx, code, userID)
		if err != nil {
			return err
		}
		// If the emote is not archived, we want to make sure that the code is unique among active and pending emotes.
		// Or, no matter what the emote's state is, we want to make sure that the code is not already taken by another user.
		if (!isArchived && !isUniqueAmongActiveAndPendingEmotes) || (!isUniqueToUser) {
			// Emote code is not appropriately unique
			return EmoticonCodeNotUnique
		}
		return nil
	})

	if enforceModerationWorkflow {
		eg.Go(func() error {
			acceptabilityResults, err := e.EmoteCodeAcceptabilityValidator.AreEmoteCodesAcceptable(ctx, []emotecodeacceptability.EmoteCodeAcceptabilityInput{
				{
					EmoteCode:       code,
					EmoteCodeSuffix: codeSuffix,
					EmoteCodePrefix: mako.ExtractEmotePrefix(code, codeSuffix),
				},
			}, userID)
			if err != nil {
				return err
			}

			// Emote code is unacceptable (offensive)
			for _, result := range acceptabilityResults {
				if !result.IsAcceptable {
					return EmoticonCodeUnacceptable
				}
			}

			return nil
		})
	}

	return eg.Wait()
}

func (e *EmoticonsManagerImpl) Create(ctx context.Context, params *CreateParams) (mako.Emote, error) {
	prefix := ""
	var code string

	if !params.ShouldIgnorePrefix {
		prefixResp, err := e.PrefixDao.GetByOwnerId(ctx, params.UserID)
		if err != nil {
			return mako.Emote{}, err
		}

		if prefixResp == nil || prefixResp.Prefix == "" {
			msg := "could not find prefix for channel"
			log.WithField("params", params.UserID).Error(msg)
			return mako.Emote{}, errors.New(msg)
		}
		prefix = prefixResp.Prefix
		code = fmt.Sprintf("%s%s", prefix, params.CodeSuffix)
	} else {
		code = params.CodeSuffix
	}

	err := e.validateCodeAndCodeSuffix(ctx, code, params.CodeSuffix, params.UserID, params.EnforceModerationWorkflow, params.ShouldBeArchived)
	if err != nil {
		return mako.Emote{}, err
	}

	shouldCreatePendingEmoteRecord := false
	isAutoApproveEligible := false
	isUserAutoApproveEligible := false
	isFollowerEmote := params.EmoteOrigin == mkd.EmoteOrigin_Follower
	accountPartnerType := ripley.UnknownPartnerType
	if params.EnforceModerationWorkflow {
		eg := errgroup.Group{}

		eg.Go(func() error {
			var err error
			isUserAutoApproveEligible, accountPartnerType, err = e.AutoApprovalEligibilityFetcher.IsUserEligibleForEmoteUploadAutoApproval(ctx, params.UserID)
			if err != nil {
				return errors.Wrap(err, "failed fetching auto approval eligibility")
			}
			if !ripley.IsAffiliate(accountPartnerType) && !ripley.IsPartner(accountPartnerType) {
				return errors.New("user must be a partner or affiliate to create emotes")
			}
			_, ok := onlyAutoApprovalOrigin[params.EmoteOrigin]
			// Skip the approval check for the case of default emotes
			if ok && !isUserAutoApproveEligible && (params.ImageSource != mako.DefaultLibraryImage) {
				return UserNotPermitted
			}
			return nil
		})

		eg.Go(func() error {
			var err error
			// Only enforce emote limit when request is coming from a broadcaster
			isOwnedEmoteLimitReached, err := e.isUserOwnedEmoteLimitReached(ctx, params.UserID, params.IsAnimated, isFollowerEmote)
			if err != nil {
				return errors.Wrap(err, "failed to determine whether user emote limit was reached")
			} else if isOwnedEmoteLimitReached {
				return UserOwnedEmoteLimitReached
			}
			return nil
		})

		areEmoteParamsAutoApproveEligible := false
		if !params.IsAnimated && (params.ImageSource != mako.DefaultLibraryImage) {
			// Only static emotes are supported in upload param auto-approval checks. Default emotes also do not need
			// to check auto-approval.
			eg.Go(func() error {
				var err error
				areEmoteParamsAutoApproveEligible, err = e.AutoApprovalEligibilityFetcher.AreEmoteUploadParamsEligibleForAutoApproval(ctx, params.Image4xID, code, params.UserID)
				if err != nil {
					// If we get an error, we don't want to fail emote creation. Instead, we will default to the params not being eligible for auto-approval.
					log.WithFields(log.Fields{
						"image4xID": params.Image4xID,
						"code":      code,
						"userID":    params.UserID,
					}).WithError(err).Error("failed fetching emote upload params auto approval eligibility")
				}
				return nil
			})
		}

		if err := eg.Wait(); err != nil {
			return mako.Emote{}, err
		}

		// If either the user themselves or their emote creation params are eligible for auto-approval,
		// or the emote is being created from a default emote, then the request can be auto-approved.
		isAutoApproveEligible = isUserAutoApproveEligible || areEmoteParamsAutoApproveEligible || (params.ImageSource == mako.DefaultLibraryImage)

		if isAutoApproveEligible {
			if params.ShouldBeArchived {
				params.State = mako.Archived
			} else {
				params.State = mako.Active
			}
		} else {
			shouldCreatePendingEmoteRecord = true
			if params.ShouldBeArchived {
				params.State = mako.PendingArchived
			} else {
				params.State = mako.Pending
			}
		}
	} else if params.ShouldBeArchived {
		params.State = mako.Archived
	}

	// retrieve uploaded emoticon image assets
	var image1x []byte
	var image2x []byte
	var image4x []byte
	var image1xAnimated []byte
	var image2xAnimated []byte
	var image4xAnimated []byte
	eg := errgroup.Group{}

	timer := e.getMetricTimer("s3.get")
	eg.Go(func() error {
		var err error
		image1x, err = e.getEmoticonImageFromS3(ctx, params.Image1xID, Image1XIDNotFound, params.ImageSource)
		return err
	})

	eg.Go(func() error {
		var err error
		image2x, err = e.getEmoticonImageFromS3(ctx, params.Image2xID, Image2XIDNotFound, params.ImageSource)
		return err
	})

	eg.Go(func() error {
		var err error
		image4x, err = e.getEmoticonImageFromS3(ctx, params.Image4xID, Image4XIDNotFound, params.ImageSource)
		return err
	})

	if params.IsAnimated {
		eg.Go(func() error {
			var err error
			image1xAnimated, err = e.getEmoticonImageFromS3(ctx, params.Image1xAnimatedID, Image1XAnimatedIDNotFound, params.ImageSource)
			return err
		})

		eg.Go(func() error {
			var err error
			image2xAnimated, err = e.getEmoticonImageFromS3(ctx, params.Image2xAnimatedID, Image2XAnimatedIDNotFound, params.ImageSource)
			return err
		})

		eg.Go(func() error {
			var err error
			image4xAnimated, err = e.getEmoticonImageFromS3(ctx, params.Image4xAnimatedID, Image4XAnimatedIDNotFound, params.ImageSource)
			return err
		})
	}

	if err := eg.Wait(); err != nil {
		return mako.Emote{}, err
	}
	timer()

	order := 0
	// Only compute the order for the emote if it is not going to be archived and it's not a global emote
	if !params.ShouldBeArchived && params.GroupID != mako.GlobalEmoteGroupID {
		timer = e.getMetricTimer("dynamo.getByGroup")
		// Get count of existing pending and active emotes to determine the new emote's order
		numExistingEmotes, err := e.countOrderedEmotesInGroup(ctx, params.GroupID)
		if err != nil {
			return mako.Emote{}, errors.Wrap(err, "Failed reading existing emotes to determine order")
		}
		timer()
		order = numExistingEmotes
	}

	// Generate a v2 UUID
	emoticonID := mako.GetEmotesV2EmoteId()

	// upload emoticon assets to s3 bucket by scale
	timer = e.getMetricTimer("s3.put")
	var staticTimer func() = nil
	var animatedTimer func() = nil
	if !params.IsAnimated {
		staticTimer = e.getMetricTimer("s3.put.static")
		err = e.uploadEmoteImages(ctx, emoticonID, image1x, image2x, image4x)
	} else {
		animatedTimer = e.getMetricTimer("s3.put.animated")
		err = e.AnimatedEmoteManager.UploadAnimatedEmoteImages(ctx, UploadAnimatedEmoteImagesParams{
			EmoticonID:      emoticonID,
			Image1x:         image1x,
			Image2x:         image2x,
			Image4x:         image4x,
			Image1xAnimated: image1xAnimated,
			Image2xAnimated: image2xAnimated,
			Image4xAnimated: image4xAnimated,
		})
	}
	if err != nil {
		return mako.Emote{}, errors.Wrap(err, "failed to upload emoticons")
	}
	timer()
	if staticTimer != nil {
		staticTimer()
	}
	if animatedTimer != nil {
		animatedTimer()
	}

	assetType := mako.StaticAssetType
	if params.IsAnimated {
		assetType = mako.AnimatedAssetType
	}

	// insert emoticon into Dynamo using the previously established emoticonID
	timer = e.getMetricTimer("dynamo.insert")
	now := time.Now()
	newEmote := mako.Emote{
		ID:                emoticonID,
		Code:              code,
		OwnerID:           params.UserID,
		Suffix:            params.CodeSuffix,
		GroupID:           params.GroupID,
		Prefix:            prefix,
		State:             params.State,
		Domain:            params.Domain,
		EmotesGroup:       params.EmoteOrigin.String(),
		CreatedAt:         now,
		UpdatedAt:         now,
		Order:             int64(order),
		AssetType:         assetType,
		AnimationTemplate: params.AnimatedEmoteTemplate,
		ImageSource:       params.ImageSource,
	}
	_, err = e.EmoteDB.WriteEmotes(ctx, newEmote)
	if err != nil {
		return mako.Emote{}, err
	}
	timer()

	// If the caller of Create specifies that an emote auto approval check was required and the resulting state is pending
	// create a pending emote record in dynamo for the moderation queue
	if shouldCreatePendingEmoteRecord {
		var emoticonType dynamo.AccountType
		if ripley.IsPartner(accountPartnerType) {
			emoticonType = dynamo.Partner
		} else if ripley.IsAffiliate(accountPartnerType) {
			emoticonType = dynamo.Affiliate
		} else {
			emoticonType = dynamo.None
		}

		// All pending emotes should default to no review state
		pendingEmoteReviewState := dynamo.PendingEmoteReviewStateNone

		timer = e.getMetricTimer("pendingemote.create")
		_, err := e.PendingEmoticonsClient.CreatePendingEmoticon(ctx, emoticonID, prefix, params.CodeSuffix, params.UserID, emoticonType, pendingEmoteReviewState, assetType, params.AnimatedEmoteTemplate)
		if err != nil {
			return mako.Emote{}, errors.Wrap(err, "failed creating pending emoticon")
		}
		timer()
	}

	// If the asset is animated we need to extract the frames pngs for review.
	if params.IsAnimated {
		err = e.AnimatedEmoteManager.ExecuteFrameSplittingSFN(ctx, emoticonID)
		if err != nil {
			// If the emote is going to need approval, we should error as Safety cannot approve without seeing all the frames
			if params.EnforceModerationWorkflow && !isAutoApproveEligible {
				return mako.Emote{}, errors.Wrap(err, "failed creating animated emote frames")
			}

			log.WithField("emote_id", emoticonID).WithError(err)
		}
	}

	go func() {
		err := e.EmoticonPublisher.PublishEmoticonCreate(context.Background(), []string{emoticonID})
		if err != nil {
			log.WithField("emoticonID", emoticonID).WithError(err).Error("failed to publish emoticon create from emoticons manager")
		}
	}()

	// EnforceModerationWorkflow is being used here as a proxy for whether or not this is coming through the standard
	// partner/affiliate dashboard flow vs admin panel or a direct mako call. We do not want to send this datascience event
	// if this is an admin upload.
	if params.EnforceModerationWorkflow {
		go func() {
			uploadMethod, err := e.UploadMetadataCache.ReadUploadMethod(context.Background(), params.Image4xID)
			if err != nil {
				// Method should be returned as UNKNOWN here so we log and continue
				log.WithField("emoteID", emoticonID).WithError(err).Error("failed to get upload method for Emote_Upload datascience event")
			}

			channelType := ""
			if ripley.IsPartner(accountPartnerType) {
				channelType = channelTypePartner
			} else if ripley.IsAffiliate(accountPartnerType) {
				// The second if is technically not necessary but being explicit here
				channelType = channelTypeAffililate
			}

			animationType := "static"
			if params.IsAnimated {
				if params.AnimatedEmoteTemplate == mako.NoTemplate {
					animationType = "custom"
				} else {
					animationType = string(params.AnimatedEmoteTemplate)
				}
			}

			if params.ImageSource == mako.DefaultLibraryImage {
				// Default image ids are in the format {id}-1.0, {id}-2.0, etc.
				// Remove the last part so that we just send the id with the spade event.
				id := strings.ReplaceAll(params.Image1xID, "-1.0", "")

				err = e.SpadeClient.TrackEvent(context.Background(), "default_emote_user_create", defaultEmoteUploadSpadeEvent{
					Time:           time.Now(),
					ChannelID:      params.UserID,
					EmoteID:        emoticonID,
					EmoteSuffix:    params.CodeSuffix,
					EmotePrefix:    prefix,
					EmoteCode:      code,
					EmoteType:      params.EmoteOrigin.String(),
					EmoteGroupID:   params.GroupID,
					GoodStanding:   isUserAutoApproveEligible,
					ChannelType:    channelType,
					AssetType:      string(assetType),
					IsAutoApproved: isAutoApproveEligible,
					DefaultEmoteID: id,
				})
			} else {
				err = e.SpadeClient.TrackEvent(context.Background(), "emote_upload", emoteUploadSpadeEvent{
					Time:              time.Now(),
					ChannelID:         params.UserID,
					EmoteID:           emoticonID,
					UploadType:        uploadMethod,
					EmoteSuffix:       params.CodeSuffix,
					EmotePrefix:       prefix,
					EmoteCode:         code,
					EmoteType:         params.EmoteOrigin.String(),
					EmoteGroupID:      params.GroupID,
					GoodStanding:      isUserAutoApproveEligible,
					ChannelType:       channelType,
					AssetType:         string(assetType),
					AnimationTemplate: animationType,
					IsAutoApproved:    isAutoApproveEligible,
					State:             string(params.State),
					Image1xID:         params.Image1xID,
					Image2xID:         params.Image2xID,
					Image4xID:         params.Image4xID,
					Image1xAnimatedID: params.Image1xAnimatedID,
					Image2xAnimatedID: params.Image2xAnimatedID,
					Image4xAnimatedID: params.Image4xAnimatedID,
				})
			}
		}()
	}

	return newEmote, nil
}

// Delete deletes the input emoticon from the appropriate datastores.
func (e *EmoticonsManagerImpl) Delete(ctx context.Context, params DeleteParams) error {
	// first, lookup the emote to make sure it exists
	emotes, err := e.EmoteDBUncached.ReadByIDs(ctx, params.EmoticonID)
	if err != nil {
		return errors.Wrap(err, "failed to lookup emote in dynamo")
	}

	if len(emotes) == 0 {
		log.WithField("emoteId", params.EmoticonID).Warn("tried to delete an emote that doesn't exist")
		return EmoteDoesNotExist
	}

	// It should not be possible to get more than 1 by querying just 1 ID so just grab the first one in the response array
	emoteToDelete := emotes[0]

	if params.RequiresPermittedUser {
		if params.UserID == "" || emoteToDelete.OwnerID != params.UserID {
			log.WithFields(log.Fields{
				"userId":  params.UserID,
				"emoteId": emoteToDelete.ID,
			}).Warnf("user not permitted to delete emote")
			return UserNotPermitted
		}
		log.WithFields(log.Fields{
			"userId":  params.UserID,
			"emoteId": emoteToDelete.ID,
		}).Info("user is permitted to delete emote")
	}

	err = e.PendingEmoticonsClient.DeletePendingEmoticonIfExists(ctx, params.EmoticonID)
	if err != nil {
		return errors.Wrap(err, "failed to delete pending emoticon")
	}

	// for all emotesdelete emoticon record from dynamodb if it exists
	timer := e.getMetricTimer("dynamo.delete")
	_, err = e.EmoteDB.DeleteEmote(ctx, params.EmoticonID)
	if err != nil {
		return errors.Wrap(err, "failed deleting emoticon from dynamodb")
	}

	timer()

	if params.DeleteS3Images {
		err = e.sendAssetS3DeleteMessage(emoteToDelete)
		if err != nil {
			return errors.Wrap(err, "failed sending delete message to sqs")
		}
	}

	go func() {
		err := e.EmoticonPublisher.PublishEmoticonDelete(context.Background(), []string{emoteToDelete.ID})
		if err != nil {
			log.WithField("emoticonID", emoteToDelete.ID).WithError(err).Error("failed to publish emoticon delete message while deleting emoticon")
		}
	}()

	// If the emote belonged to a group, update the orders for the remaining emotes
	if emoteToDelete.GroupID != "" {
		timer = e.getMetricTimer("dynamo.reorder")
		err = e.normalizeEmoteOrdersInGroup(ctx, emoteToDelete.GroupID)
		if err != nil {
			return errors.Wrap(err, "failed reordering remaining emotes in group")
		}
		timer()
	}

	return nil
}

// Deactivate sets the emote to an 'inactive' state, effectively hiding it from users.
func (e *EmoticonsManagerImpl) Deactivate(ctx context.Context, emoticonID string) error {
	// for all emotes, also update in dynamo (if exists)
	emotes, err := e.EmoteDBUncached.ReadByIDs(ctx, emoticonID)
	if err != nil {
		return errors.Wrap(err, "failed to get emoticon in dynamo")
	}

	if len(emotes) == 0 {
		return nil
	}

	timer := e.getMetricTimer("dynamo.update")
	_, err = e.EmoteDB.UpdateState(ctx, emoticonID, mako.Inactive)
	if err != nil {
		return err
	}
	timer()

	// If the emote belonged to a group, update the orders for the remaining emotes
	if emotes[0].GroupID != "" {
		timer = e.getMetricTimer("dynamo.reorder")
		err = e.normalizeEmoteOrdersInGroup(ctx, emotes[0].GroupID)
		if err != nil {
			return errors.Wrap(err, "failed reordering remaining emotes in group")
		}
		timer()
	}

	go func() {
		err := e.EmoticonPublisher.PublishEmoticonDelete(context.Background(), []string{emoticonID})
		if err != nil {
			log.WithField("emoticonID", emoticonID).WithError(err).Error("failed to publish emoticon delete message while deactivating emoticon")
		}
	}()

	return nil
}

// Activate sets the emote to an 'active' state allowing users to see and use it.
func (e *EmoticonsManagerImpl) Activate(ctx context.Context, emoticonID string) error {
	// for all emotes, also update in dynamo (if exists)
	emote, err := e.EmoteDBUncached.ReadByIDs(ctx, emoticonID)
	if err != nil {
		return errors.Wrap(err, "failed to get emoticon in dynamo")
	}

	if emote == nil {
		return nil
	}

	_, err = e.EmoteDB.UpdateState(ctx, emoticonID, mako.Active)
	if err != nil {
		return err
	}

	go func() {
		err := e.EmoticonPublisher.PublishEmoticonCreate(context.Background(), []string{emoticonID})
		if err != nil {
			log.WithField("emoticonID", emoticonID).WithError(err).Error("failed to publish emoticon activate from emoticons manager")
		}
	}()

	return nil
}

// UpdateOrders takes an array of updated emote orders, validates them and then writes them to the DB and
// then returns the entire new state of the group after the order updates have been applied.
func (e *EmoticonsManagerImpl) UpdateOrders(ctx context.Context, params UpdateOrdersParams) ([]mako.Emote, error) {
	// Get all existing orderable emotes in the requested group
	emotes, err := e.EmoteDB.Query(ctx, mako.EmoteQuery{
		GroupID: params.GroupID,
		States:  OrderedEmoteStates,
	})
	if err != nil {
		return nil, errors.Wrap(err, "Failed to read emotes by group when preparing to reorder")
	}

	// Since most groups are currently only defined in mako in terms of the emotes within them, if the dynamo query by group returns 0 results then
	// that group doesn't exist as far as mako is concerned
	if len(emotes) < 1 {
		return nil, GroupNotFound
	}

	// We make the (currently true for user-owned emotes) assumption that all emotes in a particular group have the same owner
	if emotes[0].OwnerID != params.UserID {
		return nil, UserNotPermitted
	}

	// Overwrite the orders in the slice in memory and keep track of any that are changed from their DB version
	emoteUpdatesByID := make(map[string]mako.EmoteUpdate, len(params.NewOrders))

	for i, emote := range emotes {
		newOrder, ok := params.NewOrders[emote.ID]
		if ok {
			// Remove the emoteIDs we've already found from the request map
			delete(params.NewOrders, emote.ID)

			if newOrder != emote.Order {
				emotes[i].Order = newOrder
				emoteUpdatesByID[emote.ID] = mako.EmoteUpdate{Order: &newOrder}
			}
		}
	}

	// If we didn't fully exhaust the newOrders map then at least one emote in that map was not present in the group
	// We should return 400 in this case
	if len(params.NewOrders) > 0 {
		return nil, EmoteDoesNotExist
	}

	// Sort by order after the emotes have their new orders
	mako.SortEmotesByOrderAscendingAndCreatedAtDescending(emotes)

	// We're not actually making any new changes to the existing emotes, just return the existing sorted emotes and return 200
	if len(emoteUpdatesByID) < 1 {
		return emotes, nil
	}

	// Check if the resulting orders are valid
	for i, emote := range emotes {
		if emote.Order != int64(i) {
			return nil, InvalidEmoteOrders
		}
	}

	// We're good to go... write the new orders to the database, and drop the result since we will return all emotes in the group
	_, err = e.EmoteDB.UpdateEmotes(ctx, emoteUpdatesByID)
	if err != nil {
		return nil, err
	}

	// We're returning the entire resulting sorted group here, not just the emotes that were modified
	return emotes, nil
}

// UpdateEmoteCodeSuffix updates the code suffix of an existing emoticon.
func (e *EmoticonsManagerImpl) UpdateEmoteCodeSuffix(ctx context.Context, emoticonID, codeSuffix string) error {
	// lookup the existing emote by ID
	emotes, err := e.EmoteDBUncached.ReadByIDs(ctx, emoticonID)
	if err != nil {
		return errors.Wrap(err, "failed to get emoticon in dynamo")
	}

	if emotes == nil || len(emotes) < 1 {
		return errors.New("emote not found")
	}
	existingEmote := emotes[0]
	newCode := existingEmote.Prefix + codeSuffix
	isArchived := existingEmote.State == mako.Archived || existingEmote.State == mako.PendingArchived

	// validate the new code and check for code collisions
	err = e.validateCodeAndCodeSuffix(ctx, newCode, codeSuffix, existingEmote.OwnerID, true, isArchived)
	if err != nil {
		return errors.Wrap(err, "invalid code and suffix")
	}

	_, err = e.EmoteDB.UpdateCode(ctx, emoticonID, newCode, codeSuffix)
	return err
}

// checkEmoteCodeUniqueness checks various things related to emote code uniqueness. Specifically, it checks whether the code is currently
// being used in any active or pending emotes, and whether it is taken by other users.
func (e *EmoticonsManagerImpl) checkEmoteCodeUniqueness(ctx context.Context, code string, userID string) (bool, bool, error) {
	emotes, err := e.EmoteDB.Query(ctx, mako.EmoteQuery{
		Code:   code,
		Domain: mako.EmotesDomain,
		States: []mako.EmoteState{mako.Active, mako.Pending, mako.Archived, mako.PendingArchived},
	})
	if err != nil {
		return false, false, err
	}

	isUniqueAmongActiveAndPendingEmotes := true
	isUniqueToUser := true
	for _, emote := range emotes {
		if emote.State == mako.Active || emote.State == mako.Pending {
			// This code is already being used by an active or pending emote
			isUniqueAmongActiveAndPendingEmotes = false
		}
		if emote.OwnerID != userID {
			// Another user is already using this code
			isUniqueToUser = false
		}

		// Check whether we can break early since we have finished all checks
		if !isUniqueAmongActiveAndPendingEmotes && !isUniqueToUser {
			break
		}
	}

	return isUniqueAmongActiveAndPendingEmotes, isUniqueToUser, nil
}

func (e *EmoticonsManagerImpl) isUserOwnedEmoteLimitReached(ctx context.Context, userID string, isAnimated bool, isFollowerEmote bool) (bool, error) {
	// Get the user's emote limits
	emoteLimits, err := e.EmoteLimitsManager.GetUserEmoteLimits(ctx, userID)
	if err != nil {
		return false, err
	}

	// Get owned emotes
	ownedEmotes, err := e.EmoteDB.Query(ctx, mako.EmoteQuery{
		OwnerID: userID,
		Domain:  mako.EmotesDomain,
		States:  []mako.EmoteState{mako.Active, mako.Pending, mako.Archived, mako.PendingArchived},
	})
	if err != nil {
		return false, err
	}

	// Count relevant emotes
	numStaticEmotes := int32(0)
	numAnimatedEmotes := int32(0)
	numFollowerEmotes := int32(0)
	for _, emote := range ownedEmotes {
		if emote.AssetType == mako.AnimatedAssetType {
			numAnimatedEmotes++
		} else {
			numStaticEmotes++
		}

		if emote.EmotesGroup == mk.EmoteGroup_Follower.String() {
			numFollowerEmotes++
		}
	}

	// Check all emote limits
	if (isAnimated && numAnimatedEmotes >= emoteLimits.OwnedAnimatedEmoteLimit) ||
		(!isAnimated && numStaticEmotes >= emoteLimits.OwnedStaticEmoteLimit) ||
		(isFollowerEmote && numFollowerEmotes >= emoteLimits.OwnedFollowerEmoteLimit) {
		return true, nil
	}

	// No limits reached
	return false, nil
}

// countEmotesInGroup counts all active and pending emotes in the provided group. This is used to determine what
// a newly uploaded emote's order value should be.
func (e *EmoticonsManagerImpl) countOrderedEmotesInGroup(ctx context.Context, groupID string) (int, error) {
	emotes, err := e.EmoteDB.Query(ctx, mako.EmoteQuery{
		GroupID: groupID,
		States:  OrderedEmoteStates,
	})
	if err != nil {
		return 0, err
	}

	return len(emotes), nil
}

// normalizeEmoteOrdersInGroup will take the remaining emotes in a group and renormalize their orders so that the orders
// are strictly increasing integers with no gaps.
func (e EmoticonsManagerImpl) normalizeEmoteOrdersInGroup(ctx context.Context, groupID string) error {
	emotes, err := e.EmoteDB.Query(ctx, mako.EmoteQuery{
		GroupID: groupID,
		States:  OrderedEmoteStates,
	})
	if err != nil {
		return err
	}

	mako.SortEmotesByOrderAscendingAndCreatedAtDescending(emotes)

	emoteUpdatesByID := make(map[string]mako.EmoteUpdate, 0)
	for i, emote := range emotes {
		if emote.Order != int64(i) {
			// We have to write this value to a new var or else the for-loop will mess with our pointers
			newOrder := int64(i)
			emote.Order = newOrder
			emoteUpdatesByID[emote.ID] = mako.EmoteUpdate{
				Order: &newOrder,
			}
		}
	}

	if len(emoteUpdatesByID) == 0 {
		return nil
	}

	_, err = e.EmoteDB.UpdateEmotes(ctx, emoteUpdatesByID)
	if err != nil {
		return err
	}

	return nil
}

func (e *EmoticonsManagerImpl) uploadEmoteImages(
	ctx context.Context,
	emoticonID string,
	image1x []byte,
	image2x []byte,
	image4x []byte) error {

	eg := errgroup.Group{}

	eg.Go(func() error {
		return e.EmoteManagerS3Uploader.UploadEmoteImage(ctx, emoticonID, Image1X, image1x)
	})

	eg.Go(func() error {
		return e.EmoteManagerS3Uploader.UploadEmoteImage(ctx, emoticonID, Image2X, image2x)
	})

	eg.Go(func() error {
		return e.EmoteManagerS3Uploader.UploadEmoteImage(ctx, emoticonID, Image4X, image4x)
	})

	if err := eg.Wait(); err != nil {
		return errors.Wrap(err, "failed to upload emoticon")
	}

	return nil
}

func (e *EmoticonsManagerImpl) getEmoticonImageFromS3(ctx context.Context, imageID string, imageNotFoundError error, imageSource mako.ImageSource) ([]byte, error) {
	input := &awsS3.GetObjectInput{
		Bucket: aws.String(e.UploadS3BucketName),
		Key:    aws.String(s3.GetEmoticonImageUploadS3Key(imageID)),
	}

	// Default library emotes are stored in our default library bucket
	if imageSource == mako.DefaultLibraryImage {
		input = &awsS3.GetObjectInput{
			Bucket: aws.String(e.DefaultImageLibraryS3Bucket),
			Key:    aws.String(s3.GetDefaultImageLibraryS3Key(imageID)),
		}
	}

	result, err := e.S3Client.GetObject(ctx, input)
	if err != nil {
		if aerr, ok := err.(awserr.Error); ok {
			switch aerr.Code() {
			case awsS3.ErrCodeNoSuchKey:
				return nil, imageNotFoundError
			default:
				return nil, errors.New(aerr.Error())
			}
		} else {
			return nil, errors.New(aerr.Error())
		}
	}

	// validates the result body's content
	body, err := ioutil.ReadAll(result.Body)
	if err != nil {
		return nil, err
	}
	if body == nil || len(body) == 0 {
		return nil, InvalidImageUpload
	}

	return body, err
}

func (e *EmoticonsManagerImpl) getMetricTimer(metricName string) func() {
	start := time.Now()
	return func() {
		e.Stats.TimingDuration(fmt.Sprintf(statsFormat, metricName), time.Since(start), 1.0)
	}
}

func (e *EmoticonsManagerImpl) sendAssetS3DeleteMessage(emote mako.Emote) error {
	input := model.DeleteEmoteAssetsFromS3Input{
		EmoteID:        emote.ID,
		EmoteAssetType: emote.AssetType,
	}

	body, err := json.Marshal(input)
	if err != nil {
		return err
	}
	bodyString := string(body)

	_, err = e.SQSClient.SendMessage(&sqssdk.SendMessageInput{
		QueueUrl:    &e.DeleteS3ImagesQueueURL,
		MessageBody: &bodyString,
	})

	return err
}
