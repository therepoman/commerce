package emoticonsmanager

import (
	"bytes"
	"context"

	"code.justin.tv/commerce/mako/clients/s3"
	"github.com/aws/aws-sdk-go/aws"
	awsS3 "github.com/aws/aws-sdk-go/service/s3"
	"golang.org/x/sync/errgroup"
)

type EmoteManagerS3UploaderImpl struct {
	S3Client                       s3.IS3Client
	AnimatedEmoteFrameS3BucketName string
	UploadS3BucketName             string
}

type EmoteManagerS3Uploader interface {
	UploadAnimatedEmoteImage(ctx context.Context, emoticonID string, imageSize string, imageBody []byte, theme string) error
	UploadEmoteImage(ctx context.Context, emoticonID string, imageSize string, imageBody []byte) (err error)
}

func (e *EmoteManagerS3UploaderImpl) uploadToBucket(ctx context.Context, bucket string, key string, imageBody []byte, isAnimated bool) error {
	contentType := "image/png"

	if isAnimated {
		contentType = "image/gif"
	}

	_, err := e.S3Client.PutObject(ctx, &awsS3.PutObjectInput{
		ACL:         aws.String(awsS3.ObjectCannedACLPublicRead),
		ContentType: aws.String(contentType),
		Bucket:      aws.String(bucket),
		Key:         aws.String(key),
		Body:        bytes.NewReader(imageBody),
	})

	return err
}

func (e *EmoteManagerS3UploaderImpl) UploadAnimatedEmoteImage(ctx context.Context, emoticonID string, imageSize string, imageBody []byte, theme string) error {
	eg, egCtx := errgroup.WithContext(ctx)
	for _, scale := range Scales[imageSize] {
		scale := scale
		eg.Go(func() error {
			key := s3.GetAnimatedEmoteS3Key(emoticonID, theme, scale)
			return e.uploadEmoteImage(egCtx, key, imageBody, true)
		})
	}
	return eg.Wait()
}

func (e *EmoteManagerS3UploaderImpl) UploadEmoteImage(ctx context.Context, emoticonID string, imageSize string, imageBody []byte) error {
	eg, egCtx := errgroup.WithContext(ctx)
	for _, scale := range Scales[imageSize] {
		scale := scale
		eg.Go(func() error {
			key := s3.GetEmoticonS3Key(emoticonID, scale)
			return e.uploadEmoteImage(egCtx, key, imageBody, false)
		})
	}
	return eg.Wait()
}

func (e *EmoteManagerS3UploaderImpl) uploadEmoteImage(ctx context.Context, key string, imageBody []byte, isAnimated bool) (err error) {
	// Retry 5 times since sometimes S3 is flaky
	for i := 0; i < 5; i++ {
		if err = e.uploadToBucket(ctx, e.UploadS3BucketName, key, imageBody, isAnimated); err != nil {
			continue
		}

		break
	}

	// Return if the retry loop was never successful and thus the last `err` value
	// should be returned.
	if err != nil {
		return
	}

	return
}
