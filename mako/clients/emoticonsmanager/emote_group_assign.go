package emoticonsmanager

import (
	"context"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/mako/clients/paint"
	mako "code.justin.tv/commerce/mako/internal"
	mkd "code.justin.tv/commerce/mako/twirp/dashboard"
	"code.justin.tv/subs/paint/paintrpc"
	"github.com/pkg/errors"
)

func (e *EmoticonsManagerImpl) AssignToGroup(ctx context.Context, userID string, emoteID string, groupID string, emoteOrigin mkd.EmoteOrigin) (mako.Emote, error) {
	// Get the specified emote
	emotes, err := e.EmoteDBUncached.ReadByIDs(ctx, emoteID)
	if err != nil {
		return mako.Emote{}, errors.Wrap(err, "Failed to get emote for group assignment")
	}

	// Check if it exists
	if len(emotes) == 0 {
		return mako.Emote{}, EmoteDoesNotExist
	}

	emote := emotes[0]

	// Check that the user making this request owns the emote
	if emote.OwnerID != userID {
		return mako.Emote{}, UserNotPermitted
	}

	// Check if the emote is already in this group
	if emote.GroupID == groupID {
		// Emote is already assigned to this group, so there is nothing to do
		return emote, nil
	}

	// Check that the emote is in a valid state for group assignment
	if !isEmoteStateValidForGroupAssignment(emote.State) {
		return mako.Emote{}, InvalidEmoteState
	}

	// For emotes which require auto approval to be uploaded or assigned, we validate that the user is in emote good standing
	_, ok := onlyAutoApprovalOrigin[emoteOrigin]
	if ok {
		isApproved, _, err := e.AutoApprovalEligibilityFetcher.IsUserEligibleForEmoteUploadAutoApproval(ctx, userID)
		if err != nil {
			return mako.Emote{}, errors.Wrap(err, "Failed to check emote upload auto approval for group assignment")
		}

		if !isApproved {
			return mako.Emote{}, UserNotPermitted
		}
	}

	// Check that the emote's code is unique among all active and pending emotes
	// If it is already in a group, there is no need to do this
	if emote.GroupID == "" {
		isEmoteCodeUnique, err := e.isEmoteCodeUniqueAmongActiveAndPendingEmotes(ctx, emote.Code)
		if err != nil {
			return mako.Emote{}, errors.Wrap(err, "Failed to check emote code uniqueness for group assignment")
		}
		if !isEmoteCodeUnique {
			return mako.Emote{}, EmoticonCodeNotUnique
		}
	}

	// Get the emote group that we're trying to assign this emote to and see whether the assetType and the group type match
	emoteGroups, err := e.EmoteGroupDB.GetEmoteGroups(ctx, groupID)
	if err != nil {
		return mako.Emote{}, errors.Wrap(err, "Failed to get emote group by id for group assignment")
	}

	// Emote Groups default to being static
	emoteGroupType := mako.EmoteGroupStatic
	if len(emoteGroups) > 0 {
		emoteGroupType = emoteGroups[0].GroupType
	}

	if emote.AssetType.ToGroupType() != emoteGroupType {
		return mako.Emote{}, EmoteAssetTypeDoesNotMatchGroupType
	}

	// Get the number of existing emotes in the specified group in order to assign the correct order to the emote
	numExistingEmotes, err := e.countOrderedEmotesInGroup(ctx, groupID)
	if err != nil {
		return mako.Emote{}, errors.Wrap(err, "Failed to count existing emotes in group for group assignment")
	}
	newOrder := int64(numExistingEmotes)

	// Determine what the emote's new state should be
	// Default to the emote's current state in case it is not archived or pending_archived
	newState := emote.State
	if emote.State == mako.Archived {
		newState = mako.Active
	} else if emote.State == mako.PendingArchived {
		newState = mako.Pending
	}

	// Update the emote's new origin / type / emotesGroup based on where the request came from
	newOrigin := emoteOrigin.String()

	// Assign the emote to the new group
	prevGroupID := emote.GroupID
	newGroupID := groupID

	// Update the emote
	updatedEmote, err := e.EmoteDB.UpdateEmote(ctx, emoteID, mako.EmoteUpdate{
		GroupID:     &newGroupID,
		State:       &newState,
		Order:       &newOrder,
		EmotesGroup: &newOrigin,
	})
	if err != nil {
		return mako.Emote{}, errors.Wrap(err, "Failed to update emote in EmoteDB for group assignment")
	}

	// Re-order the emotes in the emote's old group (if it had one) to correct their orders
	if prevGroupID != "" {
		err = e.normalizeEmoteOrdersInGroup(ctx, prevGroupID)
		if err != nil {
			return mako.Emote{}, errors.Wrap(err, "Failed reordering remaining emotes in old group after new group assignment")
		}
	}

	// Modify (or re-modify) the emote if it is now part of a group that requires modifications
	if mako.IsModifiable(updatedEmote.EmotesGroup, updatedEmote.AssetType) {
		go func() {
			if _, err := e.PaintClient.ModifyEmote(context.Background(), &paintrpc.ModifyEmoteRequest{
				EmoteId:       updatedEmote.ID,
				Modifications: paint.DefaultModifications,
			}); err != nil {
				// We only want to log the failure
				log.WithFields(log.Fields{
					"emote_id": updatedEmote.ID,
				}).WithError(err).Error("Failed to modify emote after new group assignment")
			}
		}()
	}

	return updatedEmote, nil
}

func (e *EmoticonsManagerImpl) RemoveFromGroup(ctx context.Context, userID string, emoteID string) (mako.Emote, error) {
	// Get the specified emote
	emotes, err := e.EmoteDBUncached.ReadByIDs(ctx, emoteID)
	if err != nil {
		return mako.Emote{}, errors.Wrap(err, "Failed to get emote for group removal")
	}

	// Check if it exists
	if len(emotes) == 0 {
		return mako.Emote{}, EmoteDoesNotExist
	}

	emote := emotes[0]

	// Check that the user making this request owns the emote
	if emote.OwnerID != userID {
		return mako.Emote{}, UserNotPermitted
	}

	// Check if the emote already has no group
	if emote.GroupID == "" {
		// Emote is already without a group, so there is nothing to do
		return emote, nil
	}

	// Reset the emote's order since the order is no longer applicable
	newOrder := int64(0)

	// Determine what the emote's new state should be
	// Default to the emote's current state in case it is not active or pending
	newState := emote.State
	if emote.State == mako.Active {
		newState = mako.Archived
	} else if emote.State == mako.Pending {
		newState = mako.PendingArchived
	}

	// Update the emote's origin / type / emotesGroup to Archive
	newOrigin := mkd.EmoteOrigin_Archive.String()

	// Remove the emote's groupID
	prevGroupID := emote.GroupID
	newGroupID := ""

	// Write the updated emote
	updatedEmote, err := e.EmoteDB.UpdateEmote(ctx, emoteID, mako.EmoteUpdate{
		GroupID:     &newGroupID,
		State:       &newState,
		Order:       &newOrder,
		EmotesGroup: &newOrigin,
	})
	if err != nil {
		return mako.Emote{}, errors.Wrap(err, "Failed to update emote in EmoteDB for group removal")
	}

	// Re-order the emotes in the emote's old group to correct their orders
	err = e.normalizeEmoteOrdersInGroup(ctx, prevGroupID)
	if err != nil {
		return mako.Emote{}, errors.Wrap(err, "Failed reordering remaining emotes in old group after group removal")
	}

	return updatedEmote, nil
}

// isEmoteCodeUniqueAmongActiveAndPendingEmotes returns whether the given emote code is unique among all active and pending emotes
func (e *EmoticonsManagerImpl) isEmoteCodeUniqueAmongActiveAndPendingEmotes(ctx context.Context, code string) (bool, error) {
	emotes, err := e.EmoteDB.Query(ctx, mako.EmoteQuery{
		Code:   code,
		Domain: mako.EmotesDomain,
		States: []mako.EmoteState{
			mako.Active, mako.Pending,
		},
	})
	if err != nil {
		return false, err
	}

	return len(emotes) == 0, nil
}

func isEmoteStateValidForGroupAssignment(state mako.EmoteState) bool {
	return state == mako.Active || state == mako.Pending || state == mako.Archived || state == mako.PendingArchived
}
