package emoticonsmanager_test

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"io/ioutil"
	"strings"
	"testing"
	"time"

	"code.justin.tv/commerce/mako/clients/emotecodeacceptability"
	"code.justin.tv/commerce/mako/clients/emotelimits"
	"code.justin.tv/commerce/mako/clients/emoticonsmanager"
	. "code.justin.tv/commerce/mako/clients/emoticonsmanager"
	"code.justin.tv/commerce/mako/clients/ripley"
	makos3 "code.justin.tv/commerce/mako/clients/s3"
	"code.justin.tv/commerce/mako/dynamo"
	mako "code.justin.tv/commerce/mako/internal"
	"code.justin.tv/commerce/mako/localdb"
	autoapproval_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/autoapproval"
	emotecodeacceptability_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/emotecodeacceptability"
	emotelimits_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/emotelimits"
	emoticonsmanager_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/emoticonsmanager"
	eventbus_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/eventbus"
	pending_emoticons_client_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/pendingemoticons"
	s3_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/s3"
	config_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/config"
	dynamo_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/dynamo"
	"code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/internal_mock"
	spade_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/common/spade-client-go/spade"
	sqs_mock "code.justin.tv/commerce/mako/mocks/github.com/aws/aws-sdk-go/service/sqs/sqsiface"
	stats_mock "code.justin.tv/commerce/mako/mocks/github.com/cactus/go-statsd-client/statsd"
	"code.justin.tv/commerce/mako/tests"
	mako_client "code.justin.tv/commerce/mako/twirp"
	mkd "code.justin.tv/commerce/mako/twirp/dashboard"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/service/s3"
	awsS3 "github.com/aws/aws-sdk-go/service/s3"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
)

func TestEmoticonsManager(t *testing.T) {
	Convey("Test Emoticons Manager", t, func() {
		mockS3Client := new(s3_mock.IS3Client)
		mockStats := new(stats_mock.Statter)
		mockEmoteDB := new(internal_mock.EmoteDB)
		mockEmoteDBUncached := new(internal_mock.EmoteDB)
		mockPrefixesDAO := new(dynamo_mock.IPrefixDao)
		mockPendingEmoticonsClient := new(pending_emoticons_client_mock.IPendingEmoticonsClient)
		mockAutoApprovalEligibilityFetcher := new(autoapproval_mock.EligibilityFetcher)
		mockEmoteCodeAcceptabilityValidator := new(emotecodeacceptability_mock.AcceptabilityValidator)
		mockEmoteLimitsManager := new(emotelimits_mock.EmoteLimitsManager)
		mockEmoticonPublisher := new(eventbus_mock.Publisher)
		mockSpadeClient := new(spade_mock.Client)
		mockMetadataCache := new(internal_mock.EmoteUploadMetadataCache)
		mockDynamicConfig := new(config_mock.DynamicConfigClient)
		mockAnimatedEmoteManager := new(emoticonsmanager_mock.AnimatedEmoteManager)
		mockEmoteManagerS3Uploader := new(emoticonsmanager_mock.EmoteManagerS3Uploader)
		mockSQSClient := new(sqs_mock.SQSAPI)

		emoticon := EmoticonsManagerImpl{
			S3Client:                        mockS3Client,
			Stats:                           mockStats,
			EmoteDB:                         mockEmoteDB,
			EmoteDBUncached:                 mockEmoteDBUncached,
			PrefixDao:                       mockPrefixesDAO,
			PendingEmoticonsClient:          mockPendingEmoticonsClient,
			AutoApprovalEligibilityFetcher:  mockAutoApprovalEligibilityFetcher,
			UploadS3BucketName:              "test-bucket",
			EmoticonPublisher:               mockEmoticonPublisher,
			EmoteCodeAcceptabilityValidator: mockEmoteCodeAcceptabilityValidator,
			EmoteLimitsManager:              mockEmoteLimitsManager,
			UploadMetadataCache:             mockMetadataCache,
			SpadeClient:                     mockSpadeClient,
			DynamicConfig:                   mockDynamicConfig,
			AnimatedEmoteManager:            mockAnimatedEmoteManager,
			EmoteManagerS3Uploader:          mockEmoteManagerS3Uploader,
			SQSClient:                       mockSQSClient,
			DefaultImageLibraryS3Bucket:     "test-library-bucket",
		}
		mockStats.On("TimingDuration", mock.Anything, mock.Anything, mock.Anything).Return(nil)
		mockSpadeClient.On("TrackEvent", mock.Anything, mock.Anything, mock.Anything).Return(nil)
		mockMetadataCache.On("ReadUploadMethod", mock.Anything, mock.Anything).Return(mako.EmoteUploadMethodSimple, nil)

		sampleBitsBadgeTierEmoteId := "emotesv2_636c2d56c4de4e989277b8694a66b105"
		sampleRegularEmoteId := "12345"
		sampleFollowerEmoteId := "99123"
		sampleV2Id := "emotesv2_1d4565d7c8fc47d7bf3a536e590da666"
		sampleCodePrefix := "prefix"
		sampleCodeSuffix := "Bar"

		existingOrderedEmotes := []mako.Emote{
			{ID: "TheOldFirstOne", Order: 0, GroupID: "ordered", State: mako.Active},
			{ID: "TheOldSecondOne", Order: 1, GroupID: "ordered", State: mako.Active},
			{ID: "TheOldThirdOne", Order: 2, GroupID: "ordered", State: mako.Active},
		}

		sampleEmote := mako.Emote{
			ID:      sampleRegularEmoteId,
			Prefix:  sampleCodePrefix,
			Suffix:  sampleCodeSuffix,
			OwnerID: "666",
			State:   mako.Active,
		}

		sampleBitsBadgeTierEmote := mako.Emote{
			ID:          sampleBitsBadgeTierEmoteId,
			EmotesGroup: mako_client.EmoteGroup_BitsBadgeTierEmotes.String(),
			OwnerID:     "666",
			State:       mako.Active,
		}

		sampleFollowerEmote := mako.Emote{
			ID:          sampleFollowerEmoteId,
			Prefix:      sampleCodePrefix,
			Suffix:      sampleCodeSuffix,
			EmotesGroup: mako_client.EmoteGroup_Follower.String(),
			OwnerID:     "666",
			State:       mako.Active,
		}

		sampleArchivedEmote := mako.Emote{
			ID:          sampleV2Id,
			Prefix:      sampleCodePrefix,
			Suffix:      sampleCodeSuffix,
			OwnerID:     "666",
			State:       mako.Archived,
			EmotesGroup: mako_client.EmoteGroup_Archive.String(),
		}

		Convey("Test Create", func() {
			mockEmoticonPublisher.On("PublishEmoticonCreate", mock.Anything, mock.Anything).Return(nil)

			mockAnimatedEmoteManager.On("UploadAnimatedEmoteImages", mock.Anything, mock.Anything).Return(nil)
			mockAnimatedEmoteManager.On("ExecuteFrameSplittingSFN", mock.Anything, mock.Anything).Return(nil)

			Convey("With InvalidCodeSuffix", func() {
				_, err := emoticon.Create(context.Background(), &CreateParams{
					CodeSuffix:         "invalid",
					ShouldIgnorePrefix: true,
				})

				So(err, ShouldEqual, InvalidCodeSuffixFormat)
			})

			Convey("With InvalidCodeSuffix with special characters", func() {
				_, err := emoticon.Create(context.Background(), &CreateParams{
					CodeSuffix:         "F@oo",
					ShouldIgnorePrefix: true,
				})

				So(err, ShouldEqual, InvalidCodeSuffixFormat)
			})

			Convey("With InvalidCodeSuffix with underscore", func() {
				_, err := emoticon.Create(context.Background(), &CreateParams{
					CodeSuffix:         "F_oo",
					ShouldIgnorePrefix: true,
				})

				So(err, ShouldEqual, InvalidCodeSuffixFormat)
			})

			Convey("Where suffix is too long", func() {
				_, err := emoticon.Create(context.Background(), &CreateParams{
					CodeSuffix:         "123456789012345678901",
					ShouldIgnorePrefix: true,
				})

				So(err, ShouldEqual, InvalidCodeSuffixFormat)
			})

			Convey("Where prefix cannot be found for this channel", func() {
				mockPrefixesDAO.On("GetByOwnerId", mock.Anything, mock.Anything).Return(nil, errors.New("test error"))

				_, err := emoticon.Create(context.Background(), &CreateParams{
					CodeSuffix: "123456789012345678901",
				})

				So(err, ShouldNotBeNil)
			})

			Convey("Where prefix is found successfully for this channel", func() {
				prefix := "chuck"
				dynamoResp := dynamo.Prefix{
					Prefix: prefix,
					State:  dynamo.PrefixActive,
				}
				mockPrefixesDAO.On("GetByOwnerId", mock.Anything, mock.Anything).Return(&dynamoResp, nil)

				Convey("Where emotedb returns an error during validation", func() {
					mockEmoteDB.On("Query", mock.Anything, mock.Anything).Return(nil, errors.New("emotedb error"))

					_, err := emoticon.Create(context.Background(), &CreateParams{
						CodeSuffix: "Foo",
					})

					So(err, ShouldNotBeNil)
				})

				Convey("Where emote code is already taken by an active emote in emotes db", func() {
					mockEmoteDB.On("Query", mock.Anything, mock.Anything).Return([]mako.Emote{sampleBitsBadgeTierEmote}, nil)

					_, err := emoticon.Create(context.Background(), &CreateParams{
						CodeSuffix: "Foo",
					})

					So(err, ShouldEqual, EmoticonCodeNotUnique)
					// query should be for all useable states
					query := mockEmoteDB.Calls[0].Arguments.Get(1).(mako.EmoteQuery)
					So(query.States, ShouldResemble, []mako.EmoteState{
						mako.Active, mako.Pending, mako.Archived, mako.PendingArchived,
					})
				})

				Convey("Where emote code is already taken by others", func() {
					mockEmoteDB.On("Query", mock.Anything, mock.Anything).Return([]mako.Emote{{
						ID:      "123",
						OwnerID: "012",
						Prefix:  "chuck1",
						Suffix:  "2",
						Code:    "chuck12",
						State:   mako.Archived,
					}}, nil)

					_, err := emoticon.Create(context.Background(), &CreateParams{
						CodeSuffix:       "12",
						ShouldBeArchived: true,
						UserID:           "425",
					})

					So(err, ShouldEqual, EmoticonCodeNotUnique)
					// query should be for all useable states
					query := mockEmoteDB.Calls[0].Arguments.Get(1).(mako.EmoteQuery)
					So(query.States, ShouldResemble, []mako.EmoteState{
						mako.Active, mako.Pending, mako.Archived, mako.PendingArchived,
					})
				})

				Convey("Where AcceptableNames returns an error during validation", func() {
					mockEmoteDB.On("Query", mock.Anything, mock.Anything).Return(nil, nil)
					mockEmoteCodeAcceptabilityValidator.On("AreEmoteCodesAcceptable", mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("names error"))

					_, err := emoticon.Create(context.Background(), &CreateParams{
						CodeSuffix:                "Foo",
						EnforceModerationWorkflow: true,
					})

					So(err, ShouldNotBeNil)
				})

				Convey("Where emote code is deemed unacceptable", func() {
					offensiveCode := "chuckFuck"
					offensiveCodeSuffix := "Fuck"

					mockEmoteDB.On("Query", mock.Anything, mock.Anything).Return(nil, nil)
					mockEmoteCodeAcceptabilityValidator.On("AreEmoteCodesAcceptable", mock.Anything, mock.Anything, mock.Anything).Return([]emotecodeacceptability.EmoteCodeAcceptabilityResult{
						{
							EmoteCode:       offensiveCode,
							EmoteCodeSuffix: offensiveCodeSuffix,
							IsAcceptable:    false,
						},
					}, nil)

					_, err := emoticon.Create(context.Background(), &CreateParams{
						CodeSuffix:                offensiveCodeSuffix,
						EnforceModerationWorkflow: true,
					})

					So(err, ShouldEqual, EmoticonCodeUnacceptable)
				})

				Convey("Where emote moderation workflow is not enforced, so emote code acceptability is not checked", func() {
					offensiveCodeSuffix := "Fuck"

					mockEmoteDB.On("Query", mock.Anything, mock.Anything).Return(nil, nil)
					mockS3Client.On("GetObject", mock.Anything, mock.Anything).Return(&s3.GetObjectOutput{Body: ioutil.NopCloser(newRepeatReader([]byte("foo")))}, nil).Once()
					mockS3Client.On("GetObject", mock.Anything, mock.Anything).Return(&s3.GetObjectOutput{Body: ioutil.NopCloser(newRepeatReader([]byte("foo")))}, nil).Once()
					mockS3Client.On("GetObject", mock.Anything, mock.Anything).Return(&s3.GetObjectOutput{Body: ioutil.NopCloser(newRepeatReader([]byte("foo")))}, nil).Once()
					mockEmoteManagerS3Uploader.On("UploadEmoteImage", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)
					mockEmoteDB.On("WriteEmotes", mock.Anything, mock.Anything).Return(nil, nil)

					result, err := emoticon.Create(context.Background(), &CreateParams{
						UserID:                    "0",
						GroupID:                   "0",
						CodeSuffix:                offensiveCodeSuffix,
						State:                     "active",
						Image1xID:                 "9d928ab5-1482-4be8-90cb-11357ab3c5c2",
						Image2xID:                 "9d928ab5-1482-4be8-90cb-11357ab3c5c3",
						Image4xID:                 "9d928ab5-1482-4be8-90cb-11357ab3c5c4",
						EnforceModerationWorkflow: false,
					})

					So(err, ShouldBeNil)
					So(result, ShouldNotBeNil)
					So(result.Code, ShouldEqual, makeEmoteCode(prefix, offensiveCodeSuffix))
					So(strings.HasPrefix(result.ID, "emotesv2"), ShouldBeTrue)
					So(strings.Contains(result.ID, "-"), ShouldBeFalse)
					mockEmoteCodeAcceptabilityValidator.AssertNumberOfCalls(t, "AreEmoteCodesAcceptable", 0)
				})

				Convey("When we fail to count existing ordered emotes", func() {
					mockEmoteDB.On("Query", mock.Anything, mock.Anything).Return(nil, nil).Once()
					mockEmoteDB.On("Query", mock.Anything, mock.Anything).Return(nil, errors.New("Some Error")).Once()
					mockS3Client.On("GetObject", mock.Anything, mock.Anything).Return(&s3.GetObjectOutput{Body: ioutil.NopCloser(newRepeatReader([]byte("foo")))}, nil).Once()
					mockS3Client.On("GetObject", mock.Anything, mock.Anything).Return(&s3.GetObjectOutput{Body: ioutil.NopCloser(newRepeatReader([]byte("foo")))}, nil).Once()
					mockS3Client.On("GetObject", mock.Anything, mock.Anything).Return(&s3.GetObjectOutput{Body: ioutil.NopCloser(newRepeatReader([]byte("foo")))}, nil).Once()

					_, err := emoticon.Create(context.Background(), &CreateParams{
						CodeSuffix: "Foo",
						Image1xID:  "9d928ab5-1482-4be8-90cb-11357ab3c5c2",
						Image2xID:  "9d928ab5-1482-4be8-90cb-11357ab3c5c3",
						Image4xID:  "9d928ab5-1482-4be8-90cb-11357ab3c5c4",
					})

					So(err, ShouldNotBeNil)
					So(err.Error(), ShouldContainSubstring, "Failed reading")
				})

				Convey("Where image doesn't exist in s3", func() {
					mockS3Client.On("GetObject", mock.Anything, mock.Anything).Return(nil, awserr.New(s3.ErrCodeNoSuchKey, "", errors.New("test")))
					mockEmoteDB.On("Query", mock.Anything, mock.Anything).Return(nil, nil)

					_, err := emoticon.Create(context.Background(), &CreateParams{
						CodeSuffix: "Foo",
						Image1xID:  "9d928ab5-1482-4be8-90cb-11357ab3c5c2",
						Image2xID:  "9d928ab5-1482-4be8-90cb-11357ab3c5c3",
						Image4xID:  "9d928ab5-1482-4be8-90cb-11357ab3c5c4",
					})

					So(err, ShouldNotBeNil)
					So(err.Error(), ShouldContainSubstring, "not found")
				})

				Convey("Where image uploaded is invalid", func() {
					mockEmoteDB.On("Query", mock.Anything, mock.Anything).Return(nil, nil)
					mockS3Client.On("GetObject", mock.Anything, mock.Anything).Return(&s3.GetObjectOutput{Body: ioutil.NopCloser(bytes.NewReader([]byte("")))}, nil)
					mockEmoteManagerS3Uploader.On("UploadEmoteImage", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(errors.New("test"))

					result, err := emoticon.Create(context.Background(), &CreateParams{
						CodeSuffix: "Foo",
						Image1xID:  "9d928ab5-1482-4be8-90cb-11357ab3c5c2",
					})

					So(err, ShouldEqual, InvalidImageUpload)
					So(result, ShouldResemble, mako.Emote{})
					So(mockEmoteManagerS3Uploader.AssertNumberOfCalls(t, "UploadEmoteImage", 0), ShouldBeTrue)
				})

				Convey("When error uploading image to s3, should delete emoticon", func() {
					mockEmoteDB.On("Query", mock.Anything, mock.Anything).Return(nil, nil)
					mockS3Client.On("GetObject", mock.Anything, mock.Anything).Return(&s3.GetObjectOutput{Body: ioutil.NopCloser(newRepeatReader([]byte("foo")))}, nil).Once()
					mockS3Client.On("GetObject", mock.Anything, mock.Anything).Return(&s3.GetObjectOutput{Body: ioutil.NopCloser(newRepeatReader([]byte("foo")))}, nil).Once()
					mockS3Client.On("GetObject", mock.Anything, mock.Anything).Return(&s3.GetObjectOutput{Body: ioutil.NopCloser(newRepeatReader([]byte("foo")))}, nil).Once()
					mockEmoteManagerS3Uploader.On("UploadEmoteImage", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(errors.New("test"))

					mockEmoteDB.On("WriteEmotes", mock.Anything, mock.Anything).Return(nil, nil)

					result, err := emoticon.Create(context.Background(), &CreateParams{
						CodeSuffix: "Foo",
						Image1xID:  "9d928ab5-1482-4be8-90cb-11357ab3c5c2",
						Image2xID:  "9d928ab5-1482-4be8-90cb-11357ab3c5c3",
						Image4xID:  "9d928ab5-1482-4be8-90cb-11357ab3c5c4",
					})

					time.Sleep(time.Second)

					So(result, ShouldResemble, mako.Emote{})
					So(err, ShouldNotBeNil)
				})

				Convey("With success and the emote is static", func() {
					ownerID := "60795173"
					mockEmoteDB.On("Query", mock.Anything, mock.Anything).Return(nil, nil).Once()
					mockEmoteDB.On("Query", mock.Anything, mock.Anything).Return(existingOrderedEmotes, nil).Once()
					mockS3Client.On("GetObject", mock.Anything, mock.Anything).Return(&s3.GetObjectOutput{Body: ioutil.NopCloser(newRepeatReader([]byte("foo")))}, nil).Once()
					mockS3Client.On("GetObject", mock.Anything, mock.Anything).Return(&s3.GetObjectOutput{Body: ioutil.NopCloser(newRepeatReader([]byte("foo")))}, nil).Once()
					mockS3Client.On("GetObject", mock.Anything, mock.Anything).Return(&s3.GetObjectOutput{Body: ioutil.NopCloser(newRepeatReader([]byte("foo")))}, nil).Once()
					mockEmoteManagerS3Uploader.On("UploadEmoteImage", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)
					mockEmoteDB.On("WriteEmotes", mock.Anything, mock.Anything).Return(nil, nil)

					suffix := "Foo"
					beforeCreate := time.Now()
					result, err := emoticon.Create(context.Background(), &CreateParams{
						UserID:     ownerID,
						CodeSuffix: suffix,
						State:      "active",
						Image1xID:  "9d928ab5-1482-4be8-90cb-11357ab3c5c2",
						Image2xID:  "9d928ab5-1482-4be8-90cb-11357ab3c5c3",
						Image4xID:  "9d928ab5-1482-4be8-90cb-11357ab3c5c4",
					})

					So(err, ShouldBeNil)
					So(result, ShouldNotBeNil)
					So(strings.HasPrefix(result.ID, "emotesv2"), ShouldBeTrue)
					So(strings.Contains(result.ID, "-"), ShouldBeFalse)
					So(result.Code, ShouldEqual, makeEmoteCode(prefix, suffix))
					emoteForDynamo := mockEmoteDB.Calls[2].Arguments.Get(1).(mako.Emote)
					So(emoteForDynamo.OwnerID, ShouldEqual, ownerID)
					So(emoteForDynamo.UpdatedAt.Before(time.Now()), ShouldBeTrue)
					So(emoteForDynamo.UpdatedAt.After(beforeCreate), ShouldBeTrue)
					So(emoteForDynamo.CreatedAt.Before(time.Now()), ShouldBeTrue)
					So(emoteForDynamo.CreatedAt.After(beforeCreate), ShouldBeTrue)
					So(emoteForDynamo.Order, ShouldEqual, len(existingOrderedEmotes))
					// The returned emote should be the exact same as the emote that is written to Dynamo
					So(result, ShouldResemble, emoteForDynamo)
				})

				Convey("With success and the emote is animated", func() {
					mockEmoteDB.On("Query", mock.Anything, mock.Anything).Return(nil, nil).Once()
					mockEmoteDB.On("Query", mock.Anything, mock.Anything).Return(existingOrderedEmotes, nil).Once()
					mockS3Client.On("GetObject", mock.Anything, mock.Anything).Return(&s3.GetObjectOutput{Body: ioutil.NopCloser(newRepeatReader([]byte("foo")))}, nil).Once()
					mockS3Client.On("GetObject", mock.Anything, mock.Anything).Return(&s3.GetObjectOutput{Body: ioutil.NopCloser(newRepeatReader([]byte("foo")))}, nil).Once()
					mockS3Client.On("GetObject", mock.Anything, mock.Anything).Return(&s3.GetObjectOutput{Body: ioutil.NopCloser(newRepeatReader([]byte("foo")))}, nil).Once()
					mockEmoteManagerS3Uploader.On("UploadEmoteImage", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)
					mockEmoteDB.On("WriteEmotes", mock.Anything, mock.Anything).Return(nil, nil)

					mockAnimatedEmoteManager.On("ExecuteFrameSplittingSFN", mock.Anything, mock.Anything).Return(nil)
					mockS3Client.On("GetObject", mock.Anything, mock.Anything).Return(&s3.GetObjectOutput{Body: ioutil.NopCloser(newRepeatReader([]byte("foo")))}, nil).Once()
					mockS3Client.On("GetObject", mock.Anything, mock.Anything).Return(&s3.GetObjectOutput{Body: ioutil.NopCloser(newRepeatReader([]byte("foo")))}, nil).Once()
					mockS3Client.On("GetObject", mock.Anything, mock.Anything).Return(&s3.GetObjectOutput{Body: ioutil.NopCloser(newRepeatReader([]byte("foo")))}, nil).Once()
					result, err := emoticon.Create(context.Background(), &CreateParams{
						UserID:     "60795173",
						CodeSuffix: "Foo",
						State:      "active",
						Image1xID:  "9d928ab5-1482-4be8-90cb-11357ab3c5c2",
						Image2xID:  "9d928ab5-1482-4be8-90cb-11357ab3c5c3",
						Image4xID:  "9d928ab5-1482-4be8-90cb-11357ab3c5c4",
						IsAnimated: true,
					})
					So(err, ShouldBeNil)
					So(result, ShouldNotBeNil)
				})

				Convey("When creating a bits badge tier emote and writing to dynamo fails", func() {
					mockEmoteDB.On("Query", mock.Anything, mock.Anything).Return(nil, nil)
					mockS3Client.On("GetObject", mock.Anything, mock.Anything).Return(&s3.GetObjectOutput{Body: ioutil.NopCloser(newRepeatReader([]byte("foo")))}, nil).Once()
					mockS3Client.On("GetObject", mock.Anything, mock.Anything).Return(&s3.GetObjectOutput{Body: ioutil.NopCloser(newRepeatReader([]byte("foo")))}, nil).Once()
					mockS3Client.On("GetObject", mock.Anything, mock.Anything).Return(&s3.GetObjectOutput{Body: ioutil.NopCloser(newRepeatReader([]byte("foo")))}, nil).Once()
					mockEmoteManagerS3Uploader.On("UploadEmoteImage", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)
					mockEmoteDB.On("WriteEmotes", mock.Anything, mock.Anything).Return(nil, errors.New("dummy dynamo error"))

					params := CreateParams{
						UserID:      "267893713",
						CodeSuffix:  "TEST",
						State:       "active",
						Domain:      "emotes",
						Image1xID:   "9d928ab5-1482-4be8-90cb-11357ab3c5c2",
						Image2xID:   "9d928ab5-1482-4be8-90cb-11357ab3c5c3",
						Image4xID:   "9d928ab5-1482-4be8-90cb-11357ab3c5c4",
						EmoteOrigin: mkd.EmoteOrigin_BitsBadgeTierEmotes,
						GroupID:     "BitsBadgeTier-267893713-1000",
					}

					result, err := emoticon.Create(context.Background(), &params)

					So(err, ShouldNotBeNil)
					So(result, ShouldResemble, mako.Emote{})
				})

				Convey("When creating a bits badge tier emote and writing to dynamo succeeds", func() {
					mockEmoteDB.On("Query", mock.Anything, mock.Anything).Return(nil, nil)
					mockS3Client.On("GetObject", mock.Anything, mock.Anything).Return(&s3.GetObjectOutput{Body: ioutil.NopCloser(newRepeatReader([]byte("foo")))}, nil).Once()
					mockS3Client.On("GetObject", mock.Anything, mock.Anything).Return(&s3.GetObjectOutput{Body: ioutil.NopCloser(newRepeatReader([]byte("foo")))}, nil).Once()
					mockS3Client.On("GetObject", mock.Anything, mock.Anything).Return(&s3.GetObjectOutput{Body: ioutil.NopCloser(newRepeatReader([]byte("foo")))}, nil).Once()
					mockEmoteManagerS3Uploader.On("UploadEmoteImage", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)
					mockEmoteDB.On("WriteEmotes", mock.Anything, mock.Anything).Return(nil, nil)

					suffix := "TEST"
					params := CreateParams{
						UserID:      "267893713",
						CodeSuffix:  suffix,
						State:       "active",
						Domain:      "emotes",
						Image1xID:   "9d928ab5-1482-4be8-90cb-11357ab3c5c2",
						Image2xID:   "9d928ab5-1482-4be8-90cb-11357ab3c5c3",
						Image4xID:   "9d928ab5-1482-4be8-90cb-11357ab3c5c4",
						EmoteOrigin: mkd.EmoteOrigin_BitsBadgeTierEmotes,
						GroupID:     "BitsBadgeTier-267893713-1000",
					}

					result, err := emoticon.Create(context.Background(), &params)

					So(err, ShouldBeNil)
					So(result, ShouldNotBeNil)
					So(result.Code, ShouldEqual, makeEmoteCode(prefix, suffix))
					So(result.State, ShouldEqual, params.State)
					So(result.GroupID, ShouldEqual, params.GroupID)
					So(result.OwnerID, ShouldEqual, params.UserID)
					So(result.Suffix, ShouldEqual, params.CodeSuffix)
					So(result.Domain, ShouldEqual, params.Domain)
					So(strings.HasPrefix(result.ID, "emotesv2"), ShouldBeTrue)
					So(strings.Contains(result.ID, "-"), ShouldBeFalse)
				})

				Convey("When creating a follower emote and writing to dynamo fails", func() {
					mockEmoteDB.On("Query", mock.Anything, mock.Anything).Return(nil, nil)
					mockS3Client.On("GetObject", mock.Anything, mock.Anything).Return(&s3.GetObjectOutput{Body: ioutil.NopCloser(newRepeatReader([]byte("foo")))}, nil).Once()
					mockS3Client.On("GetObject", mock.Anything, mock.Anything).Return(&s3.GetObjectOutput{Body: ioutil.NopCloser(newRepeatReader([]byte("foo")))}, nil).Once()
					mockS3Client.On("GetObject", mock.Anything, mock.Anything).Return(&s3.GetObjectOutput{Body: ioutil.NopCloser(newRepeatReader([]byte("foo")))}, nil).Once()
					mockEmoteManagerS3Uploader.On("UploadEmoteImage", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)
					mockEmoteDB.On("WriteEmotes", mock.Anything, mock.Anything).Return(nil, errors.New("dummy dynamo error"))

					params := CreateParams{
						UserID:      "267893713",
						CodeSuffix:  "TEST",
						State:       "active",
						Domain:      "emotes",
						Image1xID:   "9d928ab5-1482-4be8-90cb-11357ab3c5c2",
						Image2xID:   "9d928ab5-1482-4be8-90cb-11357ab3c5c3",
						Image4xID:   "9d928ab5-1482-4be8-90cb-11357ab3c5c4",
						EmoteOrigin: mkd.EmoteOrigin_Follower,
						GroupID:     mako_client.EmoteGroup_Follower.String(),
					}

					result, err := emoticon.Create(context.Background(), &params)

					So(err, ShouldNotBeNil)
					So(result, ShouldResemble, mako.Emote{})
				})

				Convey("When creating a follower tier emote and writing to dynamo succeeds", func() {
					mockEmoteDB.On("Query", mock.Anything, mock.Anything).Return(nil, nil)
					mockS3Client.On("GetObject", mock.Anything, mock.Anything).Return(&s3.GetObjectOutput{Body: ioutil.NopCloser(newRepeatReader([]byte("foo")))}, nil).Once()
					mockS3Client.On("GetObject", mock.Anything, mock.Anything).Return(&s3.GetObjectOutput{Body: ioutil.NopCloser(newRepeatReader([]byte("foo")))}, nil).Once()
					mockS3Client.On("GetObject", mock.Anything, mock.Anything).Return(&s3.GetObjectOutput{Body: ioutil.NopCloser(newRepeatReader([]byte("foo")))}, nil).Once()
					mockEmoteManagerS3Uploader.On("UploadEmoteImage", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)
					mockEmoteDB.On("WriteEmotes", mock.Anything, mock.Anything).Return(nil, nil)

					suffix := "TEST"
					params := CreateParams{
						UserID:      "267893713",
						CodeSuffix:  suffix,
						State:       "active",
						Domain:      "emotes",
						Image1xID:   "9d928ab5-1482-4be8-90cb-11357ab3c5c2",
						Image2xID:   "9d928ab5-1482-4be8-90cb-11357ab3c5c3",
						Image4xID:   "9d928ab5-1482-4be8-90cb-11357ab3c5c4",
						EmoteOrigin: mkd.EmoteOrigin_Follower,
						GroupID:     mako_client.EmoteGroup_Follower.String(),
					}

					result, err := emoticon.Create(context.Background(), &params)

					So(err, ShouldBeNil)
					So(result, ShouldNotBeNil)
					So(result.Code, ShouldEqual, makeEmoteCode(prefix, suffix))
					So(result.State, ShouldEqual, params.State)
					So(result.GroupID, ShouldEqual, params.GroupID)
					So(result.OwnerID, ShouldEqual, params.UserID)
					So(result.Suffix, ShouldEqual, params.CodeSuffix)
					So(result.Domain, ShouldEqual, params.Domain)
					So(strings.HasPrefix(result.ID, "emotesv2"), ShouldBeTrue)
					So(strings.Contains(result.ID, "-"), ShouldBeFalse)
				})

				Convey("When user eligibility fetcher returns error", func() {
					mockEmoteDB.On("Query", mock.Anything, mock.Anything).Return(nil, nil)
					mockEmoteCodeAcceptabilityValidator.On("AreEmoteCodesAcceptable", mock.Anything, mock.Anything, mock.Anything).Return([]emotecodeacceptability.EmoteCodeAcceptabilityResult{{IsAcceptable: true}}, nil)
					mockEmoteLimitsManager.On("GetUserEmoteLimits", mock.Anything, mock.Anything).Return(emotelimits.UserEmoteLimits{
						OwnedStaticEmoteLimit:   10,
						OwnedAnimatedEmoteLimit: 10,
					}, nil)

					params := CreateParams{
						UserID:                    "267893713",
						CodeSuffix:                "TEST",
						State:                     "active",
						Image1xID:                 "9d928ab5-1482-4be8-90cb-11357ab3c5c2",
						Image2xID:                 "9d928ab5-1482-4be8-90cb-11357ab3c5c3",
						Image4xID:                 "9d928ab5-1482-4be8-90cb-11357ab3c5c4",
						EmoteOrigin:               mkd.EmoteOrigin_BitsBadgeTierEmotes,
						GroupID:                   "BitsBadgeTier-267893713-1000",
						EnforceModerationWorkflow: true,
					}

					mockAutoApprovalEligibilityFetcher.On("IsUserEligibleForEmoteUploadAutoApproval", mock.Anything, params.UserID).Return(false, ripley.UnknownPartnerType, errors.New("dummy fetcher error"))
					mockAutoApprovalEligibilityFetcher.On("AreEmoteUploadParamsEligibleForAutoApproval", mock.Anything, mock.Anything, mock.Anything, params.UserID).Return(false, nil)

					result, err := emoticon.Create(context.Background(), &params)

					So(result, ShouldResemble, mako.Emote{})
					So(err, ShouldNotBeNil)
					So(err.Error(), ShouldContainSubstring, "dummy fetcher error")
					So(err.Error(), ShouldContainSubstring, "failed fetching auto approval eligibility")
				})

				Convey("When partner and upload params are not eligible for auto-approval and success", func() {
					mockEmoteDB.On("Query", mock.Anything, mock.Anything).Return(nil, nil)
					mockEmoteCodeAcceptabilityValidator.On("AreEmoteCodesAcceptable", mock.Anything, mock.Anything, mock.Anything).Return([]emotecodeacceptability.EmoteCodeAcceptabilityResult{{IsAcceptable: true}}, nil)
					mockEmoteLimitsManager.On("GetUserEmoteLimits", mock.Anything, mock.Anything).Return(emotelimits.UserEmoteLimits{
						OwnedStaticEmoteLimit:   10,
						OwnedAnimatedEmoteLimit: 10,
					}, nil)
					mockS3Client.On("GetObject", mock.Anything, mock.Anything).Return(&s3.GetObjectOutput{Body: ioutil.NopCloser(newRepeatReader([]byte("foo")))}, nil).Once()
					mockS3Client.On("GetObject", mock.Anything, mock.Anything).Return(&s3.GetObjectOutput{Body: ioutil.NopCloser(newRepeatReader([]byte("foo")))}, nil).Once()
					mockS3Client.On("GetObject", mock.Anything, mock.Anything).Return(&s3.GetObjectOutput{Body: ioutil.NopCloser(newRepeatReader([]byte("foo")))}, nil).Once()
					mockEmoteManagerS3Uploader.On("UploadEmoteImage", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)
					mockEmoteDB.On("WriteEmotes", mock.Anything, mock.Anything).Return(nil, nil)

					params := CreateParams{
						UserID:                    "267893713",
						CodeSuffix:                "TEST",
						State:                     "active",
						Domain:                    "emotes",
						Image1xID:                 "9d928ab5-1482-4be8-90cb-11357ab3c5c2",
						Image2xID:                 "9d928ab5-1482-4be8-90cb-11357ab3c5c3",
						Image4xID:                 "9d928ab5-1482-4be8-90cb-11357ab3c5c4",
						EmoteOrigin:               mkd.EmoteOrigin_BitsBadgeTierEmotes,
						GroupID:                   "BitsBadgeTier-267893713-1000",
						EnforceModerationWorkflow: true,
					}

					mockAutoApprovalEligibilityFetcher.On("IsUserEligibleForEmoteUploadAutoApproval", mock.Anything, params.UserID).Return(false, ripley.TraditionalPartnerType, nil)
					mockAutoApprovalEligibilityFetcher.On("AreEmoteUploadParamsEligibleForAutoApproval", mock.Anything, mock.Anything, mock.Anything, params.UserID).Return(false, nil)
					mockPendingEmoticonsClient.On("CreatePendingEmoticon", mock.Anything, mock.Anything, mock.Anything, params.CodeSuffix, params.UserID, dynamo.Partner, dynamo.PendingEmoteReviewStateNone, mock.Anything, mock.Anything).Return(dynamo.PendingEmote{}, nil)

					result, err := emoticon.Create(context.Background(), &params)
					So(err, ShouldBeNil)
					So(result, ShouldNotBeNil)

					mockPendingEmoticonsClient.AssertCalled(t, "CreatePendingEmoticon", mock.Anything, mock.Anything, mock.Anything, params.CodeSuffix, params.UserID, dynamo.Partner, dynamo.PendingEmoteReviewStateNone, mock.Anything, mock.Anything)

					time.Sleep(time.Second) //Add a 1-second buffer for this to avoid race conditions
					mockSpadeClient.AssertCalled(t, "TrackEvent", mock.Anything, "emote_upload", mock.Anything)
				})

				Convey("When partner and upload params are not eligible for auto-approval and pending emotes dao fails", func() {
					mockEmoteDB.On("Query", mock.Anything, mock.Anything).Return(nil, nil)
					mockEmoteCodeAcceptabilityValidator.On("AreEmoteCodesAcceptable", mock.Anything, mock.Anything, mock.Anything).Return([]emotecodeacceptability.EmoteCodeAcceptabilityResult{{IsAcceptable: true}}, nil)
					mockEmoteLimitsManager.On("GetUserEmoteLimits", mock.Anything, mock.Anything).Return(emotelimits.UserEmoteLimits{
						OwnedStaticEmoteLimit:   10,
						OwnedAnimatedEmoteLimit: 10,
					}, nil)
					mockS3Client.On("GetObject", mock.Anything, mock.Anything).Return(&s3.GetObjectOutput{Body: ioutil.NopCloser(newRepeatReader([]byte("foo")))}, nil).Once()
					mockS3Client.On("GetObject", mock.Anything, mock.Anything).Return(&s3.GetObjectOutput{Body: ioutil.NopCloser(newRepeatReader([]byte("foo")))}, nil).Once()
					mockS3Client.On("GetObject", mock.Anything, mock.Anything).Return(&s3.GetObjectOutput{Body: ioutil.NopCloser(newRepeatReader([]byte("foo")))}, nil).Once()
					mockEmoteManagerS3Uploader.On("UploadEmoteImage", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)
					mockEmoteDB.On("WriteEmotes", mock.Anything, mock.Anything).Return(nil, nil)

					params := CreateParams{
						UserID:                    "267893713",
						CodeSuffix:                "TEST",
						State:                     "active",
						Domain:                    "emotes",
						Image1xID:                 "9d928ab5-1482-4be8-90cb-11357ab3c5c2",
						Image2xID:                 "9d928ab5-1482-4be8-90cb-11357ab3c5c3",
						Image4xID:                 "9d928ab5-1482-4be8-90cb-11357ab3c5c4",
						EmoteOrigin:               mkd.EmoteOrigin_BitsBadgeTierEmotes,
						GroupID:                   "BitsBadgeTier-267893713-1000",
						EnforceModerationWorkflow: true,
					}

					mockAutoApprovalEligibilityFetcher.On("IsUserEligibleForEmoteUploadAutoApproval", mock.Anything, params.UserID).Return(false, ripley.TraditionalPartnerType, nil)
					mockAutoApprovalEligibilityFetcher.On("AreEmoteUploadParamsEligibleForAutoApproval", mock.Anything, mock.Anything, mock.Anything, params.UserID).Return(false, nil)
					mockPendingEmoticonsClient.On("CreatePendingEmoticon", mock.Anything, mock.Anything, mock.Anything, params.CodeSuffix, params.UserID, dynamo.Partner, dynamo.PendingEmoteReviewStateNone, mock.Anything, mock.Anything).Return(dynamo.PendingEmote{}, errors.New("dummy pending emotes error"))

					_, err := emoticon.Create(context.Background(), &params)

					So(err, ShouldNotBeNil)
					So(err.Error(), ShouldContainSubstring, "dummy pending emotes error")
					So(err.Error(), ShouldContainSubstring, "failed creating pending emoticon")
				})

				Convey("When partner is eligible for auto-approval and success", func() {
					mockEmoteDB.On("Query", mock.Anything, mock.Anything).Return(nil, nil)
					mockEmoteCodeAcceptabilityValidator.On("AreEmoteCodesAcceptable", mock.Anything, mock.Anything, mock.Anything).Return([]emotecodeacceptability.EmoteCodeAcceptabilityResult{{IsAcceptable: true}}, nil)
					mockEmoteLimitsManager.On("GetUserEmoteLimits", mock.Anything, mock.Anything).Return(emotelimits.UserEmoteLimits{
						OwnedStaticEmoteLimit:   10,
						OwnedAnimatedEmoteLimit: 10,
					}, nil)
					mockS3Client.On("GetObject", mock.Anything, mock.Anything).Return(&s3.GetObjectOutput{Body: ioutil.NopCloser(newRepeatReader([]byte("foo")))}, nil).Once()
					mockS3Client.On("GetObject", mock.Anything, mock.Anything).Return(&s3.GetObjectOutput{Body: ioutil.NopCloser(newRepeatReader([]byte("foo")))}, nil).Once()
					mockS3Client.On("GetObject", mock.Anything, mock.Anything).Return(&s3.GetObjectOutput{Body: ioutil.NopCloser(newRepeatReader([]byte("foo")))}, nil).Once()
					mockEmoteManagerS3Uploader.On("UploadEmoteImage", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)
					mockEmoteDB.On("WriteEmotes", mock.Anything, mock.Anything).Return(nil, nil)

					params := CreateParams{
						UserID:                    "267893713",
						CodeSuffix:                "TEST",
						State:                     "active",
						Domain:                    "emotes",
						Image1xID:                 "9d928ab5-1482-4be8-90cb-11357ab3c5c2",
						Image2xID:                 "9d928ab5-1482-4be8-90cb-11357ab3c5c3",
						Image4xID:                 "9d928ab5-1482-4be8-90cb-11357ab3c5c4",
						EmoteOrigin:               mkd.EmoteOrigin_BitsBadgeTierEmotes,
						GroupID:                   "BitsBadgeTier-267893713-1000",
						EnforceModerationWorkflow: true,
					}

					mockAutoApprovalEligibilityFetcher.On("IsUserEligibleForEmoteUploadAutoApproval", mock.Anything, params.UserID).Return(true, ripley.TraditionalPartnerType, nil)
					mockAutoApprovalEligibilityFetcher.On("AreEmoteUploadParamsEligibleForAutoApproval", mock.Anything, mock.Anything, mock.Anything, params.UserID).Return(false, nil)

					result, err := emoticon.Create(context.Background(), &params)
					So(err, ShouldBeNil)
					So(result, ShouldNotBeNil)

					mockPendingEmoticonsClient.AssertNotCalled(t, "CreatePendingEmoticon", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything)
				})

				Convey("When partner is not eligible for auto-approval, but upload params are, and success", func() {
					mockEmoteDB.On("Query", mock.Anything, mock.Anything).Return(nil, nil)
					mockEmoteCodeAcceptabilityValidator.On("AreEmoteCodesAcceptable", mock.Anything, mock.Anything, mock.Anything).Return([]emotecodeacceptability.EmoteCodeAcceptabilityResult{{IsAcceptable: true}}, nil)
					mockEmoteLimitsManager.On("GetUserEmoteLimits", mock.Anything, mock.Anything).Return(emotelimits.UserEmoteLimits{
						OwnedStaticEmoteLimit:   10,
						OwnedAnimatedEmoteLimit: 10,
					}, nil)
					mockS3Client.On("GetObject", mock.Anything, mock.Anything).Return(&s3.GetObjectOutput{Body: ioutil.NopCloser(newRepeatReader([]byte("foo")))}, nil).Once()
					mockS3Client.On("GetObject", mock.Anything, mock.Anything).Return(&s3.GetObjectOutput{Body: ioutil.NopCloser(newRepeatReader([]byte("foo")))}, nil).Once()
					mockS3Client.On("GetObject", mock.Anything, mock.Anything).Return(&s3.GetObjectOutput{Body: ioutil.NopCloser(newRepeatReader([]byte("foo")))}, nil).Once()
					mockEmoteManagerS3Uploader.On("UploadEmoteImage", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)
					mockEmoteDB.On("WriteEmotes", mock.Anything, mock.Anything).Return(nil, nil)

					params := CreateParams{
						UserID:                    "267893713",
						CodeSuffix:                "TEST",
						State:                     "active",
						Domain:                    "emotes",
						Image1xID:                 "9d928ab5-1482-4be8-90cb-11357ab3c5c2",
						Image2xID:                 "9d928ab5-1482-4be8-90cb-11357ab3c5c3",
						Image4xID:                 "9d928ab5-1482-4be8-90cb-11357ab3c5c4",
						EmoteOrigin:               mkd.EmoteOrigin_BitsBadgeTierEmotes,
						GroupID:                   "BitsBadgeTier-267893713-1000",
						EnforceModerationWorkflow: true,
					}

					mockAutoApprovalEligibilityFetcher.On("IsUserEligibleForEmoteUploadAutoApproval", mock.Anything, params.UserID).Return(false, ripley.TraditionalPartnerType, nil)
					mockAutoApprovalEligibilityFetcher.On("AreEmoteUploadParamsEligibleForAutoApproval", mock.Anything, mock.Anything, mock.Anything, params.UserID).Return(true, nil)

					result, err := emoticon.Create(context.Background(), &params)
					So(err, ShouldBeNil)
					So(result, ShouldNotBeNil)

					mockPendingEmoticonsClient.AssertNotCalled(t, "CreatePendingEmoticon", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything)
					mockAutoApprovalEligibilityFetcher.AssertCalled(t, "AreEmoteUploadParamsEligibleForAutoApproval", mock.Anything, mock.Anything, mock.Anything, params.UserID)
				})

				Convey("When non-partner is making the request", func() {
					mockEmoteDB.On("Query", mock.Anything, mock.Anything).Return(nil, nil)
					mockEmoteCodeAcceptabilityValidator.On("AreEmoteCodesAcceptable", mock.Anything, mock.Anything, mock.Anything).Return([]emotecodeacceptability.EmoteCodeAcceptabilityResult{{IsAcceptable: true}}, nil)
					mockEmoteLimitsManager.On("GetUserEmoteLimits", mock.Anything, mock.Anything).Return(emotelimits.UserEmoteLimits{
						OwnedStaticEmoteLimit:   10,
						OwnedAnimatedEmoteLimit: 10,
					}, nil)

					params := CreateParams{
						UserID:                    "267893713",
						CodeSuffix:                "TEST",
						State:                     "active",
						Domain:                    "emotes",
						Image1xID:                 "9d928ab5-1482-4be8-90cb-11357ab3c5c2",
						Image2xID:                 "9d928ab5-1482-4be8-90cb-11357ab3c5c3",
						Image4xID:                 "9d928ab5-1482-4be8-90cb-11357ab3c5c4",
						EmoteOrigin:               mkd.EmoteOrigin_BitsBadgeTierEmotes,
						GroupID:                   "BitsBadgeTier-267893713-1000",
						EnforceModerationWorkflow: true,
					}

					mockAutoApprovalEligibilityFetcher.On("IsUserEligibleForEmoteUploadAutoApproval", mock.Anything, params.UserID).Return(false, ripley.NonPartnerPartnerType, nil)
					mockAutoApprovalEligibilityFetcher.On("AreEmoteUploadParamsEligibleForAutoApproval", mock.Anything, mock.Anything, mock.Anything, params.UserID).Return(true, nil)

					_, err := emoticon.Create(context.Background(), &params)

					So(err, ShouldNotBeNil)
					So(err.Error(), ShouldContainSubstring, "user must be a partner or affiliate to create emotes")
				})

				Convey("When emote is archived without approval check", func() {
					mockEmoteDB.On("Query", mock.Anything, mock.Anything).Return(nil, nil)
					mockS3Client.On("GetObject", mock.Anything, mock.Anything).Return(&s3.GetObjectOutput{Body: ioutil.NopCloser(newRepeatReader([]byte("foo")))}, nil).Once()
					mockS3Client.On("GetObject", mock.Anything, mock.Anything).Return(&s3.GetObjectOutput{Body: ioutil.NopCloser(newRepeatReader([]byte("foo")))}, nil).Once()
					mockS3Client.On("GetObject", mock.Anything, mock.Anything).Return(&s3.GetObjectOutput{Body: ioutil.NopCloser(newRepeatReader([]byte("foo")))}, nil).Once()
					mockEmoteManagerS3Uploader.On("UploadEmoteImage", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)
					mockEmoteDB.On("WriteEmotes", mock.Anything, mock.Anything).Return(nil, nil)

					params := &CreateParams{
						UserID:                    "267893713",
						GroupID:                   "",
						CodeSuffix:                "Foo",
						State:                     mako.Active,
						EmoteOrigin:               mkd.EmoteOrigin_Archive,
						Image1xID:                 "9d928ab5-1482-4be8-90cb-11357ab3c5c2",
						Image2xID:                 "9d928ab5-1482-4be8-90cb-11357ab3c5c3",
						Image4xID:                 "9d928ab5-1482-4be8-90cb-11357ab3c5c4",
						EnforceModerationWorkflow: false,
						ShouldBeArchived:          true,
					}

					result, err := emoticon.Create(context.Background(), params)

					So(err, ShouldBeNil)
					So(result, ShouldNotBeNil)
					So(result.State, ShouldEqual, mako.Archived)
					So(result.Order, ShouldEqual, 0)
					mockEmoteDB.AssertNumberOfCalls(t, "Query", 1) // One code uniqueness check, no emote counting
				})

				Convey("When emote is archived and partner and upload params are not eligible for auto-approval", func() {
					mockEmoteDB.On("Query", mock.Anything, mock.Anything).Return(nil, nil)
					mockEmoteCodeAcceptabilityValidator.On("AreEmoteCodesAcceptable", mock.Anything, mock.Anything, mock.Anything).Return([]emotecodeacceptability.EmoteCodeAcceptabilityResult{{IsAcceptable: true}}, nil)
					mockEmoteLimitsManager.On("GetUserEmoteLimits", mock.Anything, mock.Anything).Return(emotelimits.UserEmoteLimits{
						OwnedStaticEmoteLimit:   10,
						OwnedAnimatedEmoteLimit: 10,
					}, nil)
					mockS3Client.On("GetObject", mock.Anything, mock.Anything).Return(&s3.GetObjectOutput{Body: ioutil.NopCloser(newRepeatReader([]byte("foo")))}, nil).Once()
					mockS3Client.On("GetObject", mock.Anything, mock.Anything).Return(&s3.GetObjectOutput{Body: ioutil.NopCloser(newRepeatReader([]byte("foo")))}, nil).Once()
					mockS3Client.On("GetObject", mock.Anything, mock.Anything).Return(&s3.GetObjectOutput{Body: ioutil.NopCloser(newRepeatReader([]byte("foo")))}, nil).Once()
					mockEmoteManagerS3Uploader.On("UploadEmoteImage", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)
					mockEmoteDB.On("WriteEmotes", mock.Anything, mock.Anything).Return(nil, nil)

					params := &CreateParams{
						UserID:                    "267893713",
						GroupID:                   "",
						CodeSuffix:                "Foo",
						State:                     mako.Active,
						EmoteOrigin:               mkd.EmoteOrigin_Archive,
						Image1xID:                 "9d928ab5-1482-4be8-90cb-11357ab3c5c2",
						Image2xID:                 "9d928ab5-1482-4be8-90cb-11357ab3c5c3",
						Image4xID:                 "9d928ab5-1482-4be8-90cb-11357ab3c5c4",
						EnforceModerationWorkflow: true,
						ShouldBeArchived:          true,
					}

					mockAutoApprovalEligibilityFetcher.On("IsUserEligibleForEmoteUploadAutoApproval", mock.Anything, params.UserID).Return(false, ripley.TraditionalPartnerType, nil)
					mockAutoApprovalEligibilityFetcher.On("AreEmoteUploadParamsEligibleForAutoApproval", mock.Anything, mock.Anything, mock.Anything, params.UserID).Return(false, nil)
					mockPendingEmoticonsClient.On("CreatePendingEmoticon", mock.Anything, mock.Anything, mock.Anything, params.CodeSuffix, params.UserID, dynamo.Partner, dynamo.PendingEmoteReviewStateNone, mock.Anything, mock.Anything).Return(dynamo.PendingEmote{}, nil)

					result, err := emoticon.Create(context.Background(), params)
					So(err, ShouldBeNil)
					So(result, ShouldNotBeNil)
					So(result.State, ShouldEqual, mako.PendingArchived)
					mockPendingEmoticonsClient.AssertCalled(t, "CreatePendingEmoticon", mock.Anything, mock.Anything, mock.Anything, params.CodeSuffix, params.UserID, dynamo.Partner, dynamo.PendingEmoteReviewStateNone, mock.Anything, mock.Anything)

					time.Sleep(time.Second) //Add a 1-second buffer for this to avoid race conditions
					mockSpadeClient.AssertCalled(t, "TrackEvent", mock.Anything, "emote_upload", mock.Anything)
				})

				Convey("When emote is archived and partner is eligible for auto-approval", func() {
					mockEmoteDB.On("Query", mock.Anything, mock.Anything).Return(nil, nil)
					mockEmoteCodeAcceptabilityValidator.On("AreEmoteCodesAcceptable", mock.Anything, mock.Anything, mock.Anything).Return([]emotecodeacceptability.EmoteCodeAcceptabilityResult{{IsAcceptable: true}}, nil)
					mockEmoteLimitsManager.On("GetUserEmoteLimits", mock.Anything, mock.Anything).Return(emotelimits.UserEmoteLimits{
						OwnedStaticEmoteLimit:   10,
						OwnedAnimatedEmoteLimit: 10,
					}, nil)
					mockS3Client.On("GetObject", mock.Anything, mock.Anything).Return(&s3.GetObjectOutput{Body: ioutil.NopCloser(newRepeatReader([]byte("foo")))}, nil).Once()
					mockS3Client.On("GetObject", mock.Anything, mock.Anything).Return(&s3.GetObjectOutput{Body: ioutil.NopCloser(newRepeatReader([]byte("foo")))}, nil).Once()
					mockS3Client.On("GetObject", mock.Anything, mock.Anything).Return(&s3.GetObjectOutput{Body: ioutil.NopCloser(newRepeatReader([]byte("foo")))}, nil).Once()
					mockEmoteManagerS3Uploader.On("UploadEmoteImage", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)
					mockEmoteDB.On("WriteEmotes", mock.Anything, mock.Anything).Return(nil, nil)

					params := &CreateParams{
						UserID:                    "267893713",
						GroupID:                   "",
						CodeSuffix:                "Foo",
						State:                     mako.Active,
						EmoteOrigin:               mkd.EmoteOrigin_Archive,
						Image1xID:                 "9d928ab5-1482-4be8-90cb-11357ab3c5c2",
						Image2xID:                 "9d928ab5-1482-4be8-90cb-11357ab3c5c3",
						Image4xID:                 "9d928ab5-1482-4be8-90cb-11357ab3c5c4",
						EnforceModerationWorkflow: true,
						ShouldBeArchived:          true,
					}

					mockAutoApprovalEligibilityFetcher.On("IsUserEligibleForEmoteUploadAutoApproval", mock.Anything, params.UserID).Return(true, ripley.TraditionalPartnerType, nil)
					mockAutoApprovalEligibilityFetcher.On("AreEmoteUploadParamsEligibleForAutoApproval", mock.Anything, mock.Anything, mock.Anything, params.UserID).Return(false, nil)

					result, err := emoticon.Create(context.Background(), params)
					So(err, ShouldBeNil)
					So(result, ShouldNotBeNil)
					So(result.State, ShouldEqual, mako.Archived)

					mockPendingEmoticonsClient.AssertNotCalled(t, "CreatePendingEmoticon", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything)
				})

				Convey("When user has reached their owned static emote limit", func() {
					mockEmoteDB.On("Query", mock.Anything, mock.Anything).Return(nil, nil)
					mockEmoteCodeAcceptabilityValidator.On("AreEmoteCodesAcceptable", mock.Anything, mock.Anything, mock.Anything).Return([]emotecodeacceptability.EmoteCodeAcceptabilityResult{{IsAcceptable: true}}, nil)
					mockEmoteLimitsManager.On("GetUserEmoteLimits", mock.Anything, mock.Anything).Return(emotelimits.UserEmoteLimits{
						OwnedStaticEmoteLimit:   0,
						OwnedAnimatedEmoteLimit: 10,
					}, nil)

					params := CreateParams{
						UserID:                    "267893713",
						CodeSuffix:                "TEST",
						State:                     "active",
						Image1xID:                 "9d928ab5-1482-4be8-90cb-11357ab3c5c2",
						Image2xID:                 "9d928ab5-1482-4be8-90cb-11357ab3c5c3",
						Image4xID:                 "9d928ab5-1482-4be8-90cb-11357ab3c5c4",
						EmoteOrigin:               mkd.EmoteOrigin_BitsBadgeTierEmotes,
						GroupID:                   "BitsBadgeTier-267893713-1000",
						EnforceModerationWorkflow: true,
					}

					mockAutoApprovalEligibilityFetcher.On("IsUserEligibleForEmoteUploadAutoApproval", mock.Anything, params.UserID).Return(true, ripley.TraditionalPartnerType, nil)
					mockAutoApprovalEligibilityFetcher.On("AreEmoteUploadParamsEligibleForAutoApproval", mock.Anything, mock.Anything, mock.Anything, params.UserID).Return(true, nil)

					result, err := emoticon.Create(context.Background(), &params)

					So(result, ShouldResemble, mako.Emote{})
					So(err, ShouldEqual, UserOwnedEmoteLimitReached)
				})

				Convey("When user has reached their owned follower emote limit", func() {
					mockEmoteDB.On("Query", mock.Anything, mock.Anything).Return(nil, nil)
					mockEmoteCodeAcceptabilityValidator.On("AreEmoteCodesAcceptable", mock.Anything, mock.Anything, mock.Anything).Return([]emotecodeacceptability.EmoteCodeAcceptabilityResult{{IsAcceptable: true}}, nil)
					mockEmoteLimitsManager.On("GetUserEmoteLimits", mock.Anything, mock.Anything).Return(emotelimits.UserEmoteLimits{
						OwnedStaticEmoteLimit:   10,
						OwnedAnimatedEmoteLimit: 10,
						OwnedFollowerEmoteLimit: 0,
					}, nil)

					params := CreateParams{
						UserID:                    "267893713",
						CodeSuffix:                "TEST",
						State:                     "active",
						Image1xID:                 "9d928ab5-1482-4be8-90cb-11357ab3c5c2",
						Image2xID:                 "9d928ab5-1482-4be8-90cb-11357ab3c5c3",
						Image4xID:                 "9d928ab5-1482-4be8-90cb-11357ab3c5c4",
						EmoteOrigin:               mkd.EmoteOrigin_Follower,
						GroupID:                   "BitsBadgeTier-267893713-1000",
						EnforceModerationWorkflow: true,
					}

					mockAutoApprovalEligibilityFetcher.On("IsUserEligibleForEmoteUploadAutoApproval", mock.Anything, params.UserID).Return(true, ripley.TraditionalPartnerType, nil)
					mockAutoApprovalEligibilityFetcher.On("AreEmoteUploadParamsEligibleForAutoApproval", mock.Anything, mock.Anything, mock.Anything, params.UserID).Return(true, nil)

					result, err := emoticon.Create(context.Background(), &params)

					So(result, ShouldResemble, mako.Emote{})
					So(err, ShouldEqual, UserOwnedEmoteLimitReached)
				})

				Convey("When user has reached their owned follower emote limit 2", func() {
					mockEmoteDB.On("Query", mock.Anything, mock.Anything).Return(nil, nil).Once()
					mockEmoteCodeAcceptabilityValidator.On("AreEmoteCodesAcceptable", mock.Anything, mock.Anything, mock.Anything).Return([]emotecodeacceptability.EmoteCodeAcceptabilityResult{{IsAcceptable: true}}, nil)
					mockEmoteDB.On("Query", mock.Anything, mock.Anything).Return([]mako.Emote{sampleFollowerEmote}, nil)
					mockEmoteLimitsManager.On("GetUserEmoteLimits", mock.Anything, mock.Anything).Return(emotelimits.UserEmoteLimits{
						OwnedStaticEmoteLimit:   10,
						OwnedAnimatedEmoteLimit: 10,
						OwnedFollowerEmoteLimit: 1,
					}, nil)

					params := CreateParams{
						UserID:                    "267893713",
						CodeSuffix:                "TEST",
						State:                     "active",
						Image1xID:                 "9d928ab5-1482-4be8-90cb-11357ab3c5c2",
						Image2xID:                 "9d928ab5-1482-4be8-90cb-11357ab3c5c3",
						Image4xID:                 "9d928ab5-1482-4be8-90cb-11357ab3c5c4",
						EmoteOrigin:               mkd.EmoteOrigin_Follower,
						GroupID:                   "BitsBadgeTier-267893713-1000",
						EnforceModerationWorkflow: true,
					}

					mockAutoApprovalEligibilityFetcher.On("IsUserEligibleForEmoteUploadAutoApproval", mock.Anything, params.UserID).Return(true, ripley.TraditionalPartnerType, nil)
					mockAutoApprovalEligibilityFetcher.On("AreEmoteUploadParamsEligibleForAutoApproval", mock.Anything, mock.Anything, mock.Anything, params.UserID).Return(true, nil)

					result, err := emoticon.Create(context.Background(), &params)

					So(result, ShouldResemble, mako.Emote{})
					So(err, ShouldEqual, UserOwnedEmoteLimitReached)
				})

				Convey("When user has reached their owned animated emote limit", func() {
					mockEmoteDB.On("Query", mock.Anything, mock.Anything).Return(nil, nil)
					mockEmoteCodeAcceptabilityValidator.On("AreEmoteCodesAcceptable", mock.Anything, mock.Anything, mock.Anything).Return([]emotecodeacceptability.EmoteCodeAcceptabilityResult{{IsAcceptable: true}}, nil)
					mockEmoteLimitsManager.On("GetUserEmoteLimits", mock.Anything, mock.Anything).Return(emotelimits.UserEmoteLimits{
						OwnedStaticEmoteLimit:   10,
						OwnedAnimatedEmoteLimit: 0,
					}, nil)

					params := CreateParams{
						UserID:                    "267893713",
						CodeSuffix:                "TEST",
						State:                     "active",
						Image1xID:                 "9d928ab5-1482-4be8-90cb-11357ab3c5c2",
						Image2xID:                 "9d928ab5-1482-4be8-90cb-11357ab3c5c3",
						Image4xID:                 "9d928ab5-1482-4be8-90cb-11357ab3c5c4",
						EmoteOrigin:               mkd.EmoteOrigin_BitsBadgeTierEmotes,
						GroupID:                   "BitsBadgeTier-267893713-1000",
						EnforceModerationWorkflow: true,
						IsAnimated:                true,
					}

					mockAutoApprovalEligibilityFetcher.On("IsUserEligibleForEmoteUploadAutoApproval", mock.Anything, params.UserID).Return(true, ripley.TraditionalPartnerType, nil)

					result, err := emoticon.Create(context.Background(), &params)

					So(result, ShouldResemble, mako.Emote{})
					So(err, ShouldEqual, UserOwnedEmoteLimitReached)
					mockAutoApprovalEligibilityFetcher.AssertNumberOfCalls(t, "AreEmoteUploadParamsEligibleForAutoApproval", 0)
				})

				Convey("When default emote", func() {
					mockEmoteDB.On("Query", mock.Anything, mock.Anything).Return(nil, nil)
					mockEmoteCodeAcceptabilityValidator.On("AreEmoteCodesAcceptable", mock.Anything, mock.Anything, mock.Anything).Return([]emotecodeacceptability.EmoteCodeAcceptabilityResult{{IsAcceptable: true}}, nil)
					mockEmoteLimitsManager.On("GetUserEmoteLimits", mock.Anything, mock.Anything).Return(emotelimits.UserEmoteLimits{
						OwnedStaticEmoteLimit:   10,
						OwnedAnimatedEmoteLimit: 10,
					}, nil)
					input_1x := &awsS3.GetObjectInput{
						Bucket: aws.String("test-library-bucket"),
						Key:    aws.String(makos3.GetDefaultImageLibraryS3Key("9d928ab5-1x")),
					}
					mockS3Client.On("GetObject", mock.Anything, input_1x).Return(&s3.GetObjectOutput{Body: ioutil.NopCloser(newRepeatReader([]byte("foo")))}, nil).Once()

					input_2x := &awsS3.GetObjectInput{
						Bucket: aws.String("test-library-bucket"),
						Key:    aws.String(makos3.GetDefaultImageLibraryS3Key("9d928ab5-2x")),
					}
					mockS3Client.On("GetObject", mock.Anything, input_2x).Return(&s3.GetObjectOutput{Body: ioutil.NopCloser(newRepeatReader([]byte("foo")))}, nil).Once()

					input_4x := &awsS3.GetObjectInput{
						Bucket: aws.String("test-library-bucket"),
						Key:    aws.String(makos3.GetDefaultImageLibraryS3Key("9d928ab5-4x")),
					}
					mockS3Client.On("GetObject", mock.Anything, input_4x).Return(&s3.GetObjectOutput{Body: ioutil.NopCloser(newRepeatReader([]byte("foo")))}, nil).Once()
					mockEmoteManagerS3Uploader.On("UploadEmoteImage", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)
					mockEmoteDB.On("WriteEmotes", mock.Anything, mock.Anything).Return(nil, nil)

					params := &CreateParams{
						UserID:                    "0",
						GroupID:                   "1234",
						CodeSuffix:                "PogChamp",
						State:                     mako.Active,
						EmoteOrigin:               mkd.EmoteOrigin_Subscriptions,
						Image1xID:                 "9d928ab5-1x",
						Image2xID:                 "9d928ab5-2x",
						Image4xID:                 "9d928ab5-4x",
						EnforceModerationWorkflow: true,
						ShouldBeArchived:          false,
						ShouldIgnorePrefix:        true,
						ImageSource:               mako.DefaultLibraryImage,
					}

					mockAutoApprovalEligibilityFetcher.On("IsUserEligibleForEmoteUploadAutoApproval", mock.Anything, params.UserID).Return(false, ripley.TraditionalPartnerType, nil)
					mockAutoApprovalEligibilityFetcher.On("AreEmoteUploadParamsEligibleForAutoApproval", mock.Anything, mock.Anything, mock.Anything, params.UserID).Return(false, nil)

					result, err := emoticon.Create(context.Background(), params)
					So(err, ShouldBeNil)
					So(result, ShouldNotBeNil)
					So(result.Order, ShouldEqual, 0)
					So(result.Code, ShouldEqual, params.CodeSuffix)
					So(result.ImageSource, ShouldEqual, mako.DefaultLibraryImage)

					mockPendingEmoticonsClient.AssertNotCalled(t, "CreatePendingEmoticon", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything)
					mockAutoApprovalEligibilityFetcher.AssertNotCalled(t, "AreEmoteUploadParamsEligibleForAutoApproval", mock.Anything, mock.Anything, mock.Anything, params.UserID)
				})

				Convey("When default emote and code is bad", func() {
					mockEmoteDB.On("Query", mock.Anything, mock.Anything).Return(nil, nil)
					mockEmoteCodeAcceptabilityValidator.On("AreEmoteCodesAcceptable", mock.Anything, mock.Anything, mock.Anything).Return([]emotecodeacceptability.EmoteCodeAcceptabilityResult{{IsAcceptable: false}}, nil)
					mockEmoteLimitsManager.On("GetUserEmoteLimits", mock.Anything, mock.Anything).Return(emotelimits.UserEmoteLimits{
						OwnedStaticEmoteLimit:   10,
						OwnedAnimatedEmoteLimit: 10,
					}, nil)
					input_1x := &awsS3.GetObjectInput{
						Bucket: aws.String("test-library-bucket"),
						Key:    aws.String(makos3.GetDefaultImageLibraryS3Key("9d928ab5-1x")),
					}
					mockS3Client.On("GetObject", mock.Anything, input_1x).Return(&s3.GetObjectOutput{Body: ioutil.NopCloser(newRepeatReader([]byte("foo")))}, nil).Once()

					input_2x := &awsS3.GetObjectInput{
						Bucket: aws.String("test-library-bucket"),
						Key:    aws.String(makos3.GetDefaultImageLibraryS3Key("9d928ab5-2x")),
					}
					mockS3Client.On("GetObject", mock.Anything, input_2x).Return(&s3.GetObjectOutput{Body: ioutil.NopCloser(newRepeatReader([]byte("foo")))}, nil).Once()

					input_4x := &awsS3.GetObjectInput{
						Bucket: aws.String("test-library-bucket"),
						Key:    aws.String(makos3.GetDefaultImageLibraryS3Key("9d928ab5-4x")),
					}
					mockS3Client.On("GetObject", mock.Anything, input_4x).Return(&s3.GetObjectOutput{Body: ioutil.NopCloser(newRepeatReader([]byte("foo")))}, nil).Once()
					mockEmoteManagerS3Uploader.On("UploadEmoteImage", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)
					mockEmoteDB.On("WriteEmotes", mock.Anything, mock.Anything).Return(nil)

					params := &CreateParams{
						UserID:                    "0",
						GroupID:                   "1234",
						CodeSuffix:                "BadPogChamp",
						State:                     mako.Active,
						EmoteOrigin:               mkd.EmoteOrigin_Subscriptions,
						Image1xID:                 "9d928ab5-1x",
						Image2xID:                 "9d928ab5-2x",
						Image4xID:                 "9d928ab5-4x",
						EnforceModerationWorkflow: true,
						ShouldBeArchived:          false,
						ShouldIgnorePrefix:        true,
						ImageSource:               mako.DefaultLibraryImage,
					}

					mockAutoApprovalEligibilityFetcher.On("IsUserEligibleForEmoteUploadAutoApproval", mock.Anything, params.UserID).Return(false, ripley.TraditionalPartnerType, nil)
					mockAutoApprovalEligibilityFetcher.On("AreEmoteUploadParamsEligibleForAutoApproval", mock.Anything, mock.Anything, mock.Anything, params.UserID).Return(false, nil)

					result, err := emoticon.Create(context.Background(), params)
					So(err, ShouldNotBeNil)
					So(result.Code, ShouldEqual, "")

					mockPendingEmoticonsClient.AssertNotCalled(t, "CreatePendingEmoticon", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything)
					mockAutoApprovalEligibilityFetcher.AssertNotCalled(t, "AreEmoteUploadParamsEligibleForAutoApproval", mock.Anything, mock.Anything, mock.Anything, params.UserID)
				})
			})

			Convey("When emote is global", func() {
				mockEmoteDB.On("Query", mock.Anything, mock.Anything).Return(nil, nil)
				mockS3Client.On("GetObject", mock.Anything, mock.Anything).Return(&s3.GetObjectOutput{Body: ioutil.NopCloser(newRepeatReader([]byte("foo")))}, nil).Once()
				mockS3Client.On("GetObject", mock.Anything, mock.Anything).Return(&s3.GetObjectOutput{Body: ioutil.NopCloser(newRepeatReader([]byte("foo")))}, nil).Once()
				mockS3Client.On("GetObject", mock.Anything, mock.Anything).Return(&s3.GetObjectOutput{Body: ioutil.NopCloser(newRepeatReader([]byte("foo")))}, nil).Once()
				mockEmoteManagerS3Uploader.On("UploadEmoteImage", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)
				mockEmoteDB.On("WriteEmotes", mock.Anything, mock.Anything).Return(nil, nil)

				params := &CreateParams{
					UserID:                    "0",
					GroupID:                   mako.GlobalEmoteGroupID,
					CodeSuffix:                "PogChamp",
					State:                     mako.Active,
					EmoteOrigin:               mkd.EmoteOrigin_Globals,
					Image1xID:                 "9d928ab5-1482-4be8-90cb-11357ab3c5c2",
					Image2xID:                 "9d928ab5-1482-4be8-90cb-11357ab3c5c3",
					Image4xID:                 "9d928ab5-1482-4be8-90cb-11357ab3c5c4",
					EnforceModerationWorkflow: false,
					ShouldBeArchived:          false,
					ShouldIgnorePrefix:        true,
				}

				result, err := emoticon.Create(context.Background(), params)

				So(err, ShouldBeNil)
				So(result, ShouldNotBeNil)
				So(result.Order, ShouldEqual, 0)
				So(result.Code, ShouldEqual, params.CodeSuffix)
				mockEmoteDB.AssertNumberOfCalls(t, "Query", 1) // Only code uniqueness check, no emote counting to get order
			})

		})

		Convey("Test Delete", func() {
			mockEmoticonPublisher.On("PublishEmoticonDelete", mock.Anything, mock.Anything).Return(nil)

			Convey("When dynamo emoticon lookup returns nothing", func() {
				// Use a fake EmoteDB with zero emotes in it for this test instead of mocks
				fakeEmoteDB := localdb.NewEmoteDB()
				emoticon.EmoteDB = fakeEmoteDB
				emoticon.EmoteDBUncached = fakeEmoteDB

				deleteParams := DeleteParams{
					EmoticonID:     sampleRegularEmoteId,
					DeleteS3Images: false,
				}

				err := emoticon.Delete(context.Background(), deleteParams)
				So(err, ShouldNotBeNil)
				So(err, ShouldEqual, EmoteDoesNotExist)
			})

			Convey("When dynamo emoticon lookup returns error", func() {
				mockEmoteDBUncached.On("ReadByIDs", mock.Anything, mock.Anything).Return([]mako.Emote{}, fmt.Errorf("test error"))
				deleteParams := DeleteParams{
					EmoticonID:     sampleRegularEmoteId,
					DeleteS3Images: false,
				}

				err := emoticon.Delete(context.Background(), deleteParams)
				So(err, ShouldNotBeNil)
				mockEmoteDBUncached.AssertExpectations(t)
			})

			Convey("When requires permitted user is true and provided", func() {
				deleteParams := DeleteParams{
					EmoticonID:            sampleRegularEmoteId,
					DeleteS3Images:        false,
					RequiresPermittedUser: true,
					UserID:                "99999",
				}

				Convey("When emoticon has no ownerId", func() {
					sampleEmote.OwnerID = ""
					fakeEmoteDB, _, err := localdb.MakeEmoteDB(context.Background(), sampleEmote)
					So(err, ShouldBeNil)
					emoticon.EmoteDB = fakeEmoteDB
					emoticon.EmoteDBUncached = fakeEmoteDB

					err = emoticon.Delete(context.Background(), deleteParams)
					So(err, ShouldNotBeNil)
					So(err, ShouldEqual, UserNotPermitted)

					// The emote should not be deleted
					existingEmotes, err := fakeEmoteDB.ReadByIDs(context.Background(), sampleEmote.ID)
					So(err, ShouldBeNil)
					So(len(existingEmotes), ShouldEqual, 1)
				})

				Convey("When returned emoticon has the wrong ownerid", func() {
					fakeEmoteDB, _, err := localdb.MakeEmoteDB(context.Background(), sampleEmote)
					So(err, ShouldBeNil)
					emoticon.EmoteDB = fakeEmoteDB
					emoticon.EmoteDBUncached = fakeEmoteDB

					err = emoticon.Delete(context.Background(), deleteParams)
					So(err, ShouldNotBeNil)
					So(err, ShouldEqual, UserNotPermitted)

					// The emote should not be deleted
					existingEmotes, err := fakeEmoteDB.ReadByIDs(context.Background(), sampleEmote.ID)
					So(err, ShouldBeNil)
					So(len(existingEmotes), ShouldEqual, 1)
				})
			})

			Convey("When requires permitted user is true and not provided", func() {
				deleteParams := DeleteParams{
					EmoticonID:            sampleRegularEmoteId,
					DeleteS3Images:        false,
					RequiresPermittedUser: true,
					UserID:                "",
				}

				fakeEmoteDB, _, err := localdb.MakeEmoteDB(context.Background(), sampleEmote)
				So(err, ShouldBeNil)
				emoticon.EmoteDB = fakeEmoteDB
				emoticon.EmoteDBUncached = fakeEmoteDB

				err = emoticon.Delete(context.Background(), deleteParams)
				So(err, ShouldNotBeNil)
				So(err, ShouldEqual, UserNotPermitted)

				// The emote should not be deleted
				existingEmotes, err := fakeEmoteDB.ReadByIDs(context.Background(), sampleEmote.ID)
				So(err, ShouldBeNil)
				So(len(existingEmotes), ShouldEqual, 1)
			})

			Convey("When DeletePendingEmoticonIfExists fails", func() {
				mockEmoteDBUncached.On("ReadByIDs", mock.Anything, mock.Anything).Return([]mako.Emote{sampleEmote}, nil)

				mockPendingEmoticonsClient.On("DeletePendingEmoticonIfExists", mock.Anything, mock.Anything).Return(fmt.Errorf("test error"))

				deleteParams := DeleteParams{
					EmoticonID:     sampleRegularEmoteId,
					DeleteS3Images: false,
				}

				err := emoticon.Delete(context.Background(), deleteParams)
				So(err, ShouldNotBeNil)
				mockPendingEmoticonsClient.AssertExpectations(t)
			})

			Convey("When DeletePendingEmoticonIfExists succeeds", func() {
				mockPendingEmoticonsClient.On("DeletePendingEmoticonIfExists", mock.Anything, mock.Anything).Return(nil)

				Convey("using a bits badge tier emote", func() {
					deleteParams := DeleteParams{
						EmoticonID:     sampleBitsBadgeTierEmoteId,
						DeleteS3Images: false,
					}
					mockEmoteDBUncached.On("ReadByIDs", mock.Anything, mock.Anything).Return([]mako.Emote{sampleBitsBadgeTierEmote}, nil)

					Convey("when emotes dao delete fails", func() {
						mockEmoteDB.On("DeleteEmote", mock.Anything, mock.Anything).Return(mako.Emote{}, fmt.Errorf("test error"))

						err := emoticon.Delete(context.Background(), deleteParams)
						So(err, ShouldNotBeNil)
						mockPendingEmoticonsClient.AssertExpectations(t)
						mockEmoteDB.AssertExpectations(t)
					})

					Convey("when deletion succeeds", func() {
						fakeEmoteDB, _, err := localdb.MakeEmoteDB(context.Background(), sampleBitsBadgeTierEmote)
						So(err, ShouldBeNil)
						emoticon.EmoteDB = fakeEmoteDB
						emoticon.EmoteDBUncached = fakeEmoteDB

						err = emoticon.Delete(context.Background(), deleteParams)
						So(err, ShouldBeNil)
						mockPendingEmoticonsClient.AssertExpectations(t)

						// The emote should be deleted
						emotes, err := fakeEmoteDB.ReadByIDs(context.Background(), sampleBitsBadgeTierEmoteId)
						So(err, ShouldBeNil)
						So(len(emotes), ShouldEqual, 0)
					})

					Convey("it does not reorder any emotes when the deleted emote's groupID is empty", func() {
						mockEmoteDB.On("DeleteEmote", mock.Anything, mock.Anything).Return(mako.Emote{}, nil)

						err := emoticon.Delete(context.Background(), deleteParams)
						So(err, ShouldBeNil)

						// No re-ordering should occur
						mockEmoteDB.AssertNumberOfCalls(t, "Query", 0)
						mockEmoteDB.AssertNumberOfCalls(t, "WriteEmotes", 0)
					})
				})
				Convey("using a regular emote", func() {
					deleteParams := DeleteParams{
						EmoticonID:     sampleRegularEmoteId,
						DeleteS3Images: false,
					}

					mockEmoteDBUncached.On("ReadByIDs", mock.Anything, mock.Anything).Return([]mako.Emote{sampleEmote}, nil)

					Convey("when emotes dao delete fails", func() {
						mockEmoteDB.On("DeleteEmote", mock.Anything, mock.Anything).Return(mako.Emote{}, fmt.Errorf("test error"))

						err := emoticon.Delete(context.Background(), deleteParams)
						So(err, ShouldNotBeNil)
						mockPendingEmoticonsClient.AssertExpectations(t)
						mockEmoteDB.AssertExpectations(t)
					})

					Convey("when emotes dao delete succeeds", func() {
						fakeEmoteDB, _, err := localdb.MakeEmoteDB(context.Background(), sampleEmote)
						So(err, ShouldBeNil)
						emoticon.EmoteDB = fakeEmoteDB
						emoticon.EmoteDBUncached = fakeEmoteDB

						err = emoticon.Delete(context.Background(), deleteParams)
						So(err, ShouldBeNil)
						mockPendingEmoticonsClient.AssertExpectations(t)

						emotes, err := fakeEmoteDB.ReadByIDs(context.Background(), sampleEmote.ID)
						So(err, ShouldBeNil)
						So(len(emotes), ShouldEqual, 0)
					})

					Convey("it reorders the emotes as expected", func() {
						deleteParams := DeleteParams{
							EmoticonID:     existingOrderedEmotes[0].ID,
							DeleteS3Images: false,
						}
						fakeEmoteDB, _, err := localdb.MakeEmoteDB(context.Background(), existingOrderedEmotes...)
						So(err, ShouldBeNil)
						emoticon.EmoteDB = fakeEmoteDB
						emoticon.EmoteDBUncached = fakeEmoteDB

						err = emoticon.Delete(context.Background(), deleteParams)
						So(err, ShouldBeNil)

						emotes, err := fakeEmoteDB.ReadByGroupIDs(context.Background(), existingOrderedEmotes[0].GroupID)
						So(err, ShouldBeNil)
						So(len(emotes), ShouldEqual, len(existingOrderedEmotes)-1)

						// Verify that the remaining emotes are properly ordered
						mako.SortEmotesByOrderAscendingAndCreatedAtDescending(emotes)
						for i, emote := range emotes {
							So(emote.ID, ShouldEqual, existingOrderedEmotes[i+1].ID)
							So(emote.Order, ShouldEqual, i)
						}
					})
				})
			})
		})

		Convey("Test Delete with S3 Delete true", func() {
			mockEmoticonPublisher.On("PublishEmoticonDelete", mock.Anything, mock.Anything).Return(nil)
			fakeEmoteDB, _, err := localdb.MakeEmoteDB(context.Background(), sampleEmote)
			So(err, ShouldBeNil)
			emoticon.EmoteDB = fakeEmoteDB
			emoticon.EmoteDBUncached = fakeEmoteDB

			deleteS3Image := true
			mockPendingEmoticonsClient.On("DeletePendingEmoticonIfExists", mock.Anything, mock.Anything).Return(nil)

			Convey("With Success", func() {
				ctx := context.Background()
				mockSQSClient.On("SendMessage", mock.Anything).Return(nil, nil)

				err := emoticon.Delete(ctx, DeleteParams{
					EmoticonID:     sampleEmote.ID,
					DeleteS3Images: deleteS3Image,
				})

				So(err, ShouldBeNil)
				mockSQSClient.AssertExpectations(t)
			})

			Convey("With Failure", func() {
				ctx := context.Background()
				mockSQSClient.On("SendMessage", mock.Anything).Return(nil, errors.New("boom"))

				err := emoticon.Delete(ctx, DeleteParams{
					EmoticonID:     sampleEmote.ID,
					DeleteS3Images: deleteS3Image,
				})

				So(err, ShouldNotBeNil)
				mockSQSClient.AssertExpectations(t)
			})
		})

		Convey("Test Delete with S3 Delete false", func() {
			mockEmoticonPublisher.On("PublishEmoticonDelete", mock.Anything, mock.Anything).Return(nil)
			fakeEmoteDB, _, err := localdb.MakeEmoteDB(context.Background(), sampleEmote)
			So(err, ShouldBeNil)
			emoticon.EmoteDB = fakeEmoteDB
			emoticon.EmoteDBUncached = fakeEmoteDB

			deleteS3Image := false
			mockPendingEmoticonsClient.On("DeletePendingEmoticonIfExists", mock.Anything, mock.Anything).Return(nil)

			Convey("With Success", func() {
				ctx := context.Background()

				mockS3Client.On("DeleteObject", mock.Anything, mock.Anything).Return(nil, nil)

				emotes, err := fakeEmoteDB.ReadByIDs(ctx, sampleEmote.ID)
				So(err, ShouldBeNil)
				So(len(emotes), ShouldEqual, 1)

				err = emoticon.Delete(context.Background(), DeleteParams{
					EmoticonID:     sampleEmote.ID,
					DeleteS3Images: deleteS3Image,
				})
				So(err, ShouldBeNil)

				emotes, err = fakeEmoteDB.ReadByIDs(ctx, sampleEmote.ID)
				So(err, ShouldBeNil)
				So(len(emotes), ShouldEqual, 0)
			})
		})

		Convey("Test Delete for bits badge tier emote", func() {
			mockEmoticonPublisher.On("PublishEmoticonDelete", mock.Anything, mock.Anything).Return(nil)
			mockPendingEmoticonsClient.On("DeletePendingEmoticonIfExists", mock.Anything, mock.Anything).Return(nil)

			emoteID := "c0e1e8b1-1c7c-4f7a-a7da-b471eaed0a24"
			testEmote := mako.Emote{
				ID:          emoteID,
				OwnerID:     "267893713",
				GroupID:     "BitsBadgeTier-267893713-1000",
				Prefix:      "wolf",
				Suffix:      "HOWL",
				Code:        "wolfHOWL",
				State:       "active",
				Domain:      "emotes",
				EmotesGroup: "BitsBadgeTierEmotes",
			}

			Convey("With failure deleting from dynamo then return error", func() {
				mockEmoteDBUncached.On("ReadByIDs", mock.Anything, mock.Anything).Return([]mako.Emote{testEmote}, nil)
				mockEmoteDB.On("DeleteEmote", mock.Anything, emoteID).Return(mako.Emote{}, errors.New("dummy dynamo error"))

				err := emoticon.Delete(context.Background(), DeleteParams{
					EmoticonID:     emoteID,
					DeleteS3Images: false,
				})

				So(err, ShouldNotBeNil)
				So(err.Error(), ShouldContainSubstring, "dummy dynamo error")
				So(err.Error(), ShouldContainSubstring, "failed deleting emoticon from dynamodb")
			})

			Convey("With success deleting from dynamo", func() {
				fakeEmoteDB, _, err := localdb.MakeEmoteDB(context.Background(), testEmote)
				So(err, ShouldBeNil)
				emoticon.EmoteDB = fakeEmoteDB
				emoticon.EmoteDBUncached = fakeEmoteDB

				Convey("With success deleting from redis then return nil", func() {
					err := emoticon.Delete(context.Background(), DeleteParams{
						EmoticonID:     emoteID,
						DeleteS3Images: false,
					})

					So(err, ShouldBeNil)
				})
			})
		})

		Convey("Test Deactivate", func() {
			mockEmoticonPublisher.On("PublishEmoticonDelete", mock.Anything, mock.Anything).Return(nil)

			Convey("with Bits Badge Tier emoticon", func() {
				Convey("When emotes dao get fails", func() {
					mockEmoteDBUncached.On("ReadByIDs", mock.Anything, mock.Anything).Return(nil, fmt.Errorf("test error"))
					err := emoticon.Deactivate(context.Background(), sampleBitsBadgeTierEmoteId)
					So(err, ShouldNotBeNil)
					mockEmoteDBUncached.AssertExpectations(t)
				})

				Convey("When emotes dao get succeeds but update fails", func() {
					mockEmoteDBUncached.On("ReadByIDs", mock.Anything, mock.Anything).Return([]mako.Emote{sampleBitsBadgeTierEmote}, nil)
					mockEmoteDB.On("UpdateState", mock.Anything, mock.Anything, mock.Anything).Return(mako.Emote{}, fmt.Errorf("test error"))
					err := emoticon.Deactivate(context.Background(), sampleBitsBadgeTierEmoteId)
					So(err, ShouldNotBeNil)
					mockEmoteDB.AssertExpectations(t)
					mockEmoteDBUncached.AssertExpectations(t)
				})

				Convey("On Success", func() {
					fakeEmoteDB, _, err := localdb.MakeEmoteDB(context.Background(), sampleBitsBadgeTierEmote)
					So(err, ShouldBeNil)
					emoticon.EmoteDB = fakeEmoteDB
					emoticon.EmoteDBUncached = fakeEmoteDB

					err = emoticon.Deactivate(context.Background(), sampleBitsBadgeTierEmoteId)
					So(err, ShouldBeNil)

					emotes, err := fakeEmoteDB.ReadByIDs(context.Background(), sampleBitsBadgeTierEmoteId)
					So(err, ShouldBeNil)
					So(len(emotes), ShouldEqual, 1)
					So(emotes[0].State, ShouldEqual, mako.Inactive)
				})

				Convey("it does not reorder any emotes when the deleted emote's groupID is empty", func() {
					mockEmoteDBUncached.On("ReadByIDs", mock.Anything, mock.Anything).Return([]mako.Emote{sampleBitsBadgeTierEmote}, nil)
					mockEmoteDB.On("UpdateState", mock.Anything, mock.Anything, mock.Anything).Return(mako.Emote{}, nil)
					err := emoticon.Deactivate(context.Background(), sampleBitsBadgeTierEmote.ID)
					So(err, ShouldBeNil)

					// No re-ordering should occur
					mockEmoteDB.AssertNumberOfCalls(t, "Query", 0)
					mockEmoteDB.AssertNumberOfCalls(t, "WriteEmotes", 0)
				})
			})

			Convey("with regular emoticon", func() {
				Convey("when emotesdao getbyid fails", func() {
					mockEmoteDBUncached.On("ReadByIDs", mock.Anything, mock.Anything).Return(nil, fmt.Errorf("test error"))
					err := emoticon.Deactivate(context.Background(), sampleRegularEmoteId)
					So(err, ShouldNotBeNil)
					mockEmoteDBUncached.AssertExpectations(t)
				})

				Convey("when emotesdao getbyid succeeds with nothing found", func() {
					// An Emote exists but the ID doesn't match the one used below
					fakeEmoteDB, _, err := localdb.MakeEmoteDB(context.Background(), sampleBitsBadgeTierEmote)
					So(err, ShouldBeNil)
					emoticon.EmoteDB = fakeEmoteDB
					emoticon.EmoteDBUncached = fakeEmoteDB

					err = emoticon.Deactivate(context.Background(), sampleEmote.ID)
					So(err, ShouldBeNil)

					// The existing emote should not be modified
					emotes, err := fakeEmoteDB.ReadByIDs(context.Background(), sampleBitsBadgeTierEmoteId)
					So(err, ShouldBeNil)
					So(len(emotes), ShouldEqual, 1)
					So(emotes[0], ShouldResemble, sampleBitsBadgeTierEmote)
				})

				Convey("when emotesdao getbyid finds the emote but update state fails", func() {
					mockEmoteDBUncached.On("ReadByIDs", mock.Anything, mock.Anything).Return([]mako.Emote{sampleEmote}, nil)
					mockEmoteDB.On("UpdateState", mock.Anything, mock.Anything, mock.Anything).Return(mako.Emote{}, fmt.Errorf("test error"))
					err := emoticon.Deactivate(context.Background(), sampleRegularEmoteId)
					So(err, ShouldNotBeNil)
					mockEmoteDB.AssertExpectations(t)
					mockEmoteDBUncached.AssertExpectations(t)
				})

				Convey("when emotesdao update state succeeds", func() {
					fakeEmoteDB, _, err := localdb.MakeEmoteDB(context.Background(), sampleEmote)
					So(err, ShouldBeNil)
					emoticon.EmoteDB = fakeEmoteDB
					emoticon.EmoteDBUncached = fakeEmoteDB

					err = emoticon.Deactivate(context.Background(), sampleRegularEmoteId)
					So(err, ShouldBeNil)

					emotes, err := fakeEmoteDB.ReadByIDs(context.Background(), sampleRegularEmoteId)
					So(err, ShouldBeNil)
					So(len(emotes), ShouldEqual, 1)
					So(emotes[0].State, ShouldEqual, mako.Inactive)
				})
			})

			Convey("Test Activate", func() {
				mockEmoticonPublisher.On("PublishEmoticonCreate", mock.Anything, mock.Anything).Return(nil)

				Convey("with Bits Badge Tier emoticon", func() {
					Convey("When emotes dao get fails", func() {
						mockEmoteDBUncached.On("ReadByIDs", mock.Anything, mock.Anything).Return([]mako.Emote{sampleEmote}, fmt.Errorf("test error"))
						err := emoticon.Activate(context.Background(), sampleBitsBadgeTierEmoteId)
						So(err, ShouldNotBeNil)
						mockEmoteDBUncached.AssertExpectations(t)
					})

					Convey("When emotes dao get succeeds", func() {
						mockEmoteDBUncached.On("ReadByIDs", mock.Anything, mock.Anything).Return([]mako.Emote{sampleEmote}, nil)

						Convey("When emotes dao update fails", func() {
							mockEmoteDB.On("UpdateState", mock.Anything, mock.Anything, mock.Anything).Return(mako.Emote{}, fmt.Errorf("test error"))
							err := emoticon.Activate(context.Background(), sampleBitsBadgeTierEmoteId)
							So(err, ShouldNotBeNil)
							mockEmoteDB.AssertExpectations(t)
						})
						Convey("On Success", func() {
							mockEmoteDB.On("UpdateState", mock.Anything, mock.Anything, mock.Anything).Return(mako.Emote{}, nil)
							err := emoticon.Activate(context.Background(), sampleBitsBadgeTierEmoteId)
							So(err, ShouldBeNil)
							mockEmoteDB.AssertExpectations(t)
						})
					})
				})

				Convey("with regular emoticon", func() {
					Convey("when emotesdao getbyid fails", func() {
						mockEmoteDBUncached.On("ReadByIDs", mock.Anything, mock.Anything).Return(nil, fmt.Errorf("test error"))
						err := emoticon.Activate(context.Background(), sampleRegularEmoteId)
						So(err, ShouldNotBeNil)
						mockEmoteDBUncached.AssertExpectations(t)
					})

					Convey("when emotesdao getbyid succeeds with nothing found", func() {
						mockEmoteDBUncached.On("ReadByIDs", mock.Anything, mock.Anything).Return(nil, nil)
						err := emoticon.Activate(context.Background(), sampleRegularEmoteId)
						So(err, ShouldBeNil)
						mockEmoteDBUncached.AssertExpectations(t)
					})

					Convey("when emotesdao getbyid succeeds with emote found", func() {
						mockEmoteDBUncached.On("ReadByIDs", mock.Anything, mock.Anything).Return([]mako.Emote{sampleEmote}, nil)

						Convey("when emotesdao update state fails", func() {
							mockEmoteDB.On("UpdateState", mock.Anything, mock.Anything, mock.Anything).Return(mako.Emote{}, fmt.Errorf("test error"))
							err := emoticon.Activate(context.Background(), sampleRegularEmoteId)
							So(err, ShouldNotBeNil)
							mockEmoteDB.AssertExpectations(t)
						})

						Convey("when emotesdao update state succeeds", func() {
							mockEmoteDB.On("UpdateState", mock.Anything, mock.Anything, mock.Anything).Return(mako.Emote{}, nil)
							err := emoticon.Activate(context.Background(), sampleRegularEmoteId)
							So(err, ShouldBeNil)
							mockEmoteDB.AssertExpectations(t)
						})
					})
				})
			})

			Convey("Test UpdateEmoteCodeSuffix", func() {
				mockEmoteCodeAcceptabilityValidator.On("AreEmoteCodesAcceptable", mock.Anything, mock.Anything, mock.Anything).Return([]emotecodeacceptability.EmoteCodeAcceptabilityResult{{IsAcceptable: true}}, nil)
				Convey("When dynamo already has the code", func() {
					Convey("with bits badge tier emote", func() {
						mockEmoteDBUncached.On("ReadByIDs", mock.Anything, mock.Anything).Return([]mako.Emote{sampleBitsBadgeTierEmote}, nil)
						mockEmoteDB.On("Query", mock.Anything, mock.Anything).Return([]mako.Emote{sampleBitsBadgeTierEmote}, nil)
						err := emoticon.UpdateEmoteCodeSuffix(context.Background(), sampleBitsBadgeTierEmoteId, sampleCodeSuffix)
						So(err, ShouldNotBeNil)
						mockEmoteDB.AssertExpectations(t)
						mockEmoteDBUncached.AssertExpectations(t)
					})

					Convey("with subs emote", func() {
						mockEmoteDBUncached.On("ReadByIDs", mock.Anything, mock.Anything).Return([]mako.Emote{sampleEmote}, nil)
						mockEmoteDB.On("Query", mock.Anything, mock.Anything).Return([]mako.Emote{sampleEmote}, nil)

						err := emoticon.UpdateEmoteCodeSuffix(context.Background(), sampleRegularEmoteId, sampleCodeSuffix)
						So(err, ShouldNotBeNil)
						mockEmoteDB.AssertExpectations(t)
						mockEmoteDBUncached.AssertExpectations(t)
					})

					Convey("with archived emote", func() {
						mockEmoteDBUncached.On("ReadByIDs", mock.Anything, mock.Anything).Return([]mako.Emote{sampleArchivedEmote}, nil)
						sampleOtherEmote := sampleEmote
						sampleOtherEmote.OwnerID = "someotherid"
						mockEmoteDB.On("Query", mock.Anything, mock.Anything).Return([]mako.Emote{sampleOtherEmote}, nil)

						err := emoticon.UpdateEmoteCodeSuffix(context.Background(), sampleRegularEmoteId, sampleCodeSuffix)
						So(err, ShouldNotBeNil)
						mockEmoteDB.AssertExpectations(t)
						mockEmoteDBUncached.AssertExpectations(t)
					})
				})

				Convey("When emotes dao ReadyByIDs fails", func() {
					mockEmoteDBUncached.On("ReadByIDs", mock.Anything, mock.Anything).Return(nil, fmt.Errorf("test error"))
					err := emoticon.UpdateEmoteCodeSuffix(context.Background(), sampleRegularEmoteId, sampleCodeSuffix)
					So(err, ShouldNotBeNil)
					mockEmoteDBUncached.AssertExpectations(t)
				})

				Convey("When emotes dao getbyid succeeds with nothing found", func() {
					mockEmoteDBUncached.On("ReadByIDs", mock.Anything, mock.Anything).Return(nil, nil)
					err := emoticon.UpdateEmoteCodeSuffix(context.Background(), sampleRegularEmoteId, sampleCodeSuffix)
					So(err, ShouldNotBeNil)
					mockEmoteDBUncached.AssertExpectations(t)
				})

				Convey("When emotes dao getbyid succeeds with emote found", func() {
					mockEmoteDBUncached.On("ReadByIDs", mock.Anything, mock.Anything).Return([]mako.Emote{sampleEmote}, nil)
					mockEmoteDB.On("Query", mock.Anything, mock.Anything).Return(nil, nil)

					Convey("When emotes dao update code fails", func() {
						mockEmoteDB.On("UpdateCode", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(mako.Emote{}, fmt.Errorf("test error"))
						err := emoticon.UpdateEmoteCodeSuffix(context.Background(), sampleRegularEmoteId, sampleCodeSuffix)
						So(err, ShouldNotBeNil)
						mockEmoteDB.AssertExpectations(t)
					})

					Convey("When emotes dao update code succeeds", func() {
						mockEmoteDB.On("UpdateCode", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(mako.Emote{}, nil)
						err := emoticon.UpdateEmoteCodeSuffix(context.Background(), sampleRegularEmoteId, sampleCodeSuffix)
						So(err, ShouldBeNil)
						mockEmoteDB.AssertExpectations(t)
					})
				})
			})

		})
	})
}

func TestUpdateOrders(t *testing.T) {
	testGroupID := "group1234"
	testUserID := "user1234"

	emote1 := mako.Emote{ID: "emote1", GroupID: testGroupID, OwnerID: testUserID, Order: 0, State: mako.Active}
	emote2 := mako.Emote{ID: "emote2", GroupID: testGroupID, OwnerID: testUserID, Order: 1, State: mako.Active}
	emote3 := mako.Emote{ID: "emote3", GroupID: testGroupID, OwnerID: testUserID, Order: 2, State: mako.Active}

	sampleEmotes := []mako.Emote{emote1, emote2, emote3}

	// Failure cases
	failureCases := []struct {
		Scenario       string
		ExistingEmotes []mako.Emote
		Params         emoticonsmanager.UpdateOrdersParams
		ExpectedError  error
	}{
		{
			Scenario:       "because the user does not own the emotes",
			ExistingEmotes: sampleEmotes,
			Params: emoticonsmanager.UpdateOrdersParams{
				UserID:  "someOtherUser",
				GroupID: testGroupID,
				NewOrders: map[string]int64{
					"emote1": 1,
					"emote2": 0,
				},
			},
			ExpectedError: emoticonsmanager.UserNotPermitted,
		},
		{
			Scenario:       "because the group is empty",
			ExistingEmotes: sampleEmotes,
			Params: emoticonsmanager.UpdateOrdersParams{
				UserID:  testUserID,
				GroupID: "someOtherGroup",
				NewOrders: map[string]int64{
					"emote1": 1,
					"emote2": 0,
				},
			},
			ExpectedError: emoticonsmanager.GroupNotFound,
		},
		{
			Scenario:       "because one of the emotes doesn't exist",
			ExistingEmotes: sampleEmotes,
			Params: emoticonsmanager.UpdateOrdersParams{
				UserID:  testUserID,
				GroupID: testGroupID,
				NewOrders: map[string]int64{
					"emote1":            1,
					"emote2":            0,
					"whatIsThisCrazyID": 2,
				},
			},
			ExpectedError: emoticonsmanager.EmoteDoesNotExist,
		},
		{
			Scenario:       "because it contains duplicate orders",
			ExistingEmotes: sampleEmotes,
			Params: emoticonsmanager.UpdateOrdersParams{
				UserID:  testUserID,
				GroupID: testGroupID,
				NewOrders: map[string]int64{
					"emote1": 0,
					"emote2": 0,
				},
			},
			ExpectedError: emoticonsmanager.InvalidEmoteOrders,
		},
		{
			Scenario:       "because it conflicts with an existing unmodified order",
			ExistingEmotes: sampleEmotes,
			Params: emoticonsmanager.UpdateOrdersParams{
				UserID:  testUserID,
				GroupID: testGroupID,
				NewOrders: map[string]int64{
					"emote1": 1, // emote2 already has order 1 and is not being modified
				},
			},
			ExpectedError: emoticonsmanager.InvalidEmoteOrders,
		},
		{
			Scenario:       "because it results in orders with a gap",
			ExistingEmotes: sampleEmotes,
			Params: emoticonsmanager.UpdateOrdersParams{
				UserID:  testUserID,
				GroupID: testGroupID,
				NewOrders: map[string]int64{
					"emote1": 0,
					"emote2": 6,
				},
			},
			ExpectedError: emoticonsmanager.InvalidEmoteOrders,
		},
		{
			Scenario:       "because it contains an order that is never valid",
			ExistingEmotes: sampleEmotes,
			Params: emoticonsmanager.UpdateOrdersParams{
				UserID:  testUserID,
				GroupID: testGroupID,
				NewOrders: map[string]int64{
					"emote1": -1,
				},
			},
			ExpectedError: emoticonsmanager.InvalidEmoteOrders,
		},
	}

	for _, tc := range failureCases {
		t.Run(fmt.Sprintf("When it fails %s", tc.Scenario), func(t *testing.T) {
			tc := tc
			t.Parallel()

			fakeEmoteDB, _, err := localdb.MakeEmoteDB(context.Background(), tc.ExistingEmotes...)
			emoteManager := EmoticonsManagerImpl{
				EmoteDB: fakeEmoteDB,
			}
			require.Nil(t, err)

			resp, err := emoteManager.UpdateOrders(context.Background(), tc.Params)
			require.NotNil(t, err)
			require.EqualValues(t, tc.ExpectedError, err)
			require.Nil(t, resp)
		})
	}

	// Success Cases
	successCases := []struct {
		Scenario         string
		ExistingEmotes   []mako.Emote
		Params           emoticonsmanager.UpdateOrdersParams
		ExpectedResponse []mako.Emote
	}{
		{
			Scenario:       "with all emotes specified",
			ExistingEmotes: sampleEmotes,
			Params: emoticonsmanager.UpdateOrdersParams{
				UserID:  testUserID,
				GroupID: testGroupID,
				NewOrders: map[string]int64{
					"emote1": 1,
					"emote2": 0,
					"emote3": 2,
				},
			},
			ExpectedResponse: []mako.Emote{
				copyEmoteWithNewOrder(emote2, 0),
				copyEmoteWithNewOrder(emote1, 1),
				emote3,
			},
		},
		{
			Scenario:       "with a no-op",
			ExistingEmotes: sampleEmotes,
			Params: emoticonsmanager.UpdateOrdersParams{
				UserID:  testUserID,
				GroupID: testGroupID,
				NewOrders: map[string]int64{
					"emote1": 0,
					"emote2": 1,
					"emote3": 2,
				},
			},
			ExpectedResponse: sampleEmotes,
		},
		{
			Scenario:       "with not all emotes specified",
			ExistingEmotes: sampleEmotes,
			Params: emoticonsmanager.UpdateOrdersParams{
				UserID:  testUserID,
				GroupID: testGroupID,
				NewOrders: map[string]int64{
					"emote1": 1,
					"emote2": 0,
				},
			},
			ExpectedResponse: []mako.Emote{
				copyEmoteWithNewOrder(emote2, 0),
				copyEmoteWithNewOrder(emote1, 1),
				emote3,
			},
		},
	}

	for _, tc := range successCases {
		t.Run(fmt.Sprintf("When it succeeds %s", tc.Scenario), func(t *testing.T) {
			tc := tc
			t.Parallel()

			fakeEmoteDB, _, err := localdb.MakeEmoteDB(context.Background(), tc.ExistingEmotes...)
			emoteManager := EmoticonsManagerImpl{
				EmoteDB: fakeEmoteDB,
			}
			require.Nil(t, err)

			resp, err := emoteManager.UpdateOrders(context.Background(), tc.Params)
			require.Nil(t, err)
			require.NotNil(t, resp)

			// Order matters for this response
			require.Equal(t, len(tc.ExpectedResponse), len(resp))
			for i, emote := range resp {
				tests.RequireEmoteEquality(t, tc.ExpectedResponse[i], emote)
			}

			dbEmotes, err := fakeEmoteDB.Query(context.Background(), mako.EmoteQuery{
				GroupID: tc.Params.GroupID,
				States:  emoticonsmanager.OrderedEmoteStates,
			})

			require.Nil(t, err)
			// Order is not guaranteed for this response
			tests.RequireEmoteSliceEquality(t, tc.ExpectedResponse, dbEmotes)
		})
	}
}

type RepeatReader struct {
	s []byte
	i int64 // curr index
}

func (r *RepeatReader) Read(b []byte) (int, error) {
	if r.i >= int64(len(r.s)) {
		r.i = 0
		return 0, io.EOF
	}
	n := copy(b, r.s[r.i:])
	r.i += int64(n)
	return n, nil
}

func newRepeatReader(b []byte) *RepeatReader {
	return &RepeatReader{
		s: b,
		i: 0,
	}
}

func copyEmoteWithNewOrder(emote mako.Emote, order int64) mako.Emote {
	newEmote := emote
	newEmote.Order = order
	return newEmote
}

func makeEmoteCode(prefix, suffix string) string {
	return fmt.Sprintf("%s%s", prefix, suffix)
}
