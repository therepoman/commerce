package emoticonsmanager_test

import (
	"context"
	"testing"

	em "code.justin.tv/commerce/mako/clients/emoticonsmanager"
	mako "code.justin.tv/commerce/mako/internal"
	"code.justin.tv/commerce/mako/localdb"
	"code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/internal_mock"
	mkd "code.justin.tv/commerce/mako/twirp/dashboard"
	"github.com/pkg/errors"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
)

func TestUpdateEmoteOrigins(t *testing.T) {
	testUserID := "someUser"
	testEmoteID1 := "emotesv2_1234abcd"
	testEmoteID2 := "emotesv2_5678defg"
	testEmoteID3 := "emotesv2_9012hijk"
	testEmoteID4 := "emotesv2_3456lmno"
	testEmoteID5 := "emotesv2_7890pqrs"
	testEmoteID6 := "emotesv2_1234tuvw"

	emote1 := mako.Emote{ID: testEmoteID1, Code: "testEmote1", OwnerID: testUserID, State: mako.Active, EmotesGroup: mkd.EmoteOrigin_None.String(), Domain: mako.EmotesDomain}
	emote2 := mako.Emote{ID: testEmoteID2, Code: "testEmote2", OwnerID: testUserID, State: mako.Active, EmotesGroup: mkd.EmoteOrigin_MegaCommerce.String(), Domain: mako.EmotesDomain}
	emote3 := mako.Emote{ID: testEmoteID3, Code: "testEmote3", OwnerID: testUserID, State: mako.Active, EmotesGroup: mkd.EmoteOrigin_Subscriptions.String(), Domain: mako.EmotesDomain}
	emote4 := mako.Emote{ID: testEmoteID4, Code: "testEmote4", OwnerID: testUserID, State: mako.Inactive, EmotesGroup: mkd.EmoteOrigin_Subscriptions.String(), Domain: mako.EmotesDomain}
	emote5 := mako.Emote{ID: testEmoteID5, Code: "testEmote5", OwnerID: testUserID, State: mako.Pending, EmotesGroup: mkd.EmoteOrigin_Subscriptions.String(), Domain: mako.EmotesDomain}
	emote6 := mako.Emote{ID: testEmoteID6, Code: "testEmote5", OwnerID: testUserID, State: mako.Archived, EmotesGroup: mkd.EmoteOrigin_Subscriptions.String(), Domain: mako.EmotesDomain}

	sampleEmotes := []mako.Emote{emote1, emote2, emote3, emote4, emote5, emote6}
	sampleEmoteIDs := make([]string, 0, len(sampleEmotes))
	for _, emote := range sampleEmotes {
		sampleEmoteIDs = append(sampleEmoteIDs, emote.ID)
	}

	tests := []struct {
		scenario string
		test     func(t *testing.T)
	}{
		{
			scenario: "GIVEN EmoteDB ReadByIDs errors WHEN update emote origin THEN return error",
			test: func(t *testing.T) {
				// GIVEN
				mockEmoteDB := new(internal_mock.EmoteDB)
				mockEmoteDBUncached := new(internal_mock.EmoteDB)
				emoticonsManager := em.EmoticonsManagerImpl{
					EmoteDB:         mockEmoteDB,
					EmoteDBUncached: mockEmoteDBUncached,
				}
				mockEmoteDBUncached.On("ReadByIDs", mock.Anything, mock.Anything).Return(nil, errors.New("some_error"))

				// WHEN
				res, err := emoticonsManager.UpdateEmoteOrigins(context.Background(), makeMegaCommerceUpdateOriginParam([]string{emote1.ID}))

				// THEN
				require.Error(t, err)
				require.Contains(t, err.Error(), "some_error")
				require.Nil(t, res)
			},
		},
		{
			scenario: "GIVEN emote does not exist WHEN update emote origin THEN return error",
			test: func(t *testing.T) {
				// GIVEN
				fakeEmoteDB, _, err := localdb.MakeEmoteDB(context.Background(), sampleEmotes...)
				emoticonsManager := em.EmoticonsManagerImpl{
					EmoteDB:         fakeEmoteDB,
					EmoteDBUncached: fakeEmoteDB,
				}

				// WHEN
				res, err := emoticonsManager.UpdateEmoteOrigins(context.Background(), makeMegaCommerceUpdateOriginParam([]string{"notARealEmoteID"}))

				// THEN
				require.Error(t, err)
				require.Equal(t, em.EmoteDoesNotExist, err)
				require.Nil(t, res)
			},
		},
		{
			scenario: "GIVEN emote is already the specified origin WHEN update emote origin THEN return unchanged emote and no error",
			test: func(t *testing.T) {
				// GIVEN
				fakeEmoteDB, _, err := localdb.MakeEmoteDB(context.Background(), sampleEmotes...)
				emoticonsManager := em.EmoticonsManagerImpl{
					EmoteDB:         fakeEmoteDB,
					EmoteDBUncached: fakeEmoteDB,
				}

				// WHEN
				res, err := emoticonsManager.UpdateEmoteOrigins(context.Background(), makeMegaCommerceUpdateOriginParam([]string{emote2.ID}))

				// THEN
				require.NoError(t, err)
				require.Equal(t, []mako.Emote{emote2}, res)
			},
		},
		{
			scenario: "GIVEN any emote state WHEN update emote origin THEN return updated emotes",
			test: func(t *testing.T) {
				// GIVEN
				fakeEmoteDB, _, err := localdb.MakeEmoteDB(context.Background(), sampleEmotes...)
				emoticonsManager := em.EmoticonsManagerImpl{
					EmoteDB:         fakeEmoteDB,
					EmoteDBUncached: fakeEmoteDB,
				}

				// WHEN
				res, err := emoticonsManager.UpdateEmoteOrigins(context.Background(), makeMegaCommerceUpdateOriginParam(sampleEmoteIDs))

				// THEN
				require.NoError(t, err)
				require.Equal(t, len(sampleEmoteIDs), len(res))

				for _, emote := range res {
					require.Equal(t, mkd.EmoteOrigin_MegaCommerce.String(), emote.EmotesGroup)
				}
			},
		},
		{
			scenario: "GIVEN updating the emote errors WHEN update emote origin THEN return error",
			test: func(t *testing.T) {
				// GIVEN
				mockEmoteDB := new(internal_mock.EmoteDB)
				mockEmoteDBUncached := new(internal_mock.EmoteDB)
				emoticonsManager := em.EmoticonsManagerImpl{
					EmoteDB:         mockEmoteDB,
					EmoteDBUncached: mockEmoteDBUncached,
				}
				mockEmoteDBUncached.On("ReadByIDs", mock.Anything, mock.Anything).Return([]mako.Emote{emote1}, nil)
				mockEmoteDB.On("UpdateEmote", mock.Anything, mock.Anything, mock.Anything).Return(mako.Emote{}, errors.New("some_error"))

				// WHEN
				res, err := emoticonsManager.UpdateEmoteOrigins(context.Background(), makeMegaCommerceUpdateOriginParam([]string{emote1.ID}))

				// THEN
				require.Error(t, err)
				require.Contains(t, err.Error(), "some_error")
				require.Nil(t, res)
			},
		},
	}

	for _, test := range tests {
		t.Run(test.scenario, func(t *testing.T) {
			test.test(t)
		})
	}
}

func makeMegaCommerceUpdateOriginParam(emoteIDs []string) em.UpdateOriginsParams {
	newOrigins := make(map[string]mkd.EmoteOrigin)

	for _, emoteID := range emoteIDs {
		newOrigins[emoteID] = mkd.EmoteOrigin_MegaCommerce
	}

	return em.UpdateOriginsParams{
		NewOrigins: newOrigins,
	}
}
