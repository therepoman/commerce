package emoticonsmanager_test

import (
	"bytes"
	"context"
	"errors"
	"testing"

	. "code.justin.tv/commerce/mako/clients/emoticonsmanager"
	s3_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/s3"
	"github.com/aws/aws-sdk-go/aws"
	awsS3 "github.com/aws/aws-sdk-go/service/s3"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestEmoteS3Uploader(t *testing.T) {
	mockS3Client := new(s3_mock.IS3Client)

	testAnimatedFrameBucket := "testframebucket"
	testUploadS3Bucket := "testuploadbucket"

	s3Uploader := EmoteManagerS3UploaderImpl{
		S3Client:                       mockS3Client,
		AnimatedEmoteFrameS3BucketName: testAnimatedFrameBucket,
		UploadS3BucketName:             testUploadS3Bucket,
	}

	testEmoteId := "ab123"
	testImageBytes := []byte{}
	testImageSize := Image1X

	Convey("Test UploadEmoteImage", t, func() {
		expectedKey := "emoticonab123-1.0.png"
		Convey("With success", func() {
			mockS3Client.On("PutObject", mock.Anything, mock.Anything).Return(nil, nil).Times(len(Scales[testImageSize]))
			s3Uploader.UploadEmoteImage(context.Background(), testEmoteId, testImageSize, testImageBytes)
			mockS3Client.AssertNumberOfCalls(t, "PutObject", len(Scales[testImageSize]))

			So(mockS3Client.Calls[0].Arguments.Get(1), ShouldResemble, &awsS3.PutObjectInput{
				ACL:         aws.String(awsS3.ObjectCannedACLPublicRead),
				ContentType: aws.String("image/png"),
				Bucket:      aws.String(testUploadS3Bucket),
				Key:         aws.String(expectedKey),
				Body:        bytes.NewReader(testImageBytes),
			})
		})

		Convey("retries with error uploading to uploads bucket", func() {
			mockS3Client.Calls = []mock.Call{}

			mockS3Client.On("PutObject", mock.Anything, mock.Anything).Return(nil, errors.New("test error"))
			err := s3Uploader.UploadEmoteImage(context.Background(), testEmoteId, testImageSize, testImageBytes)

			mockS3Client.AssertNumberOfCalls(t, "PutObject", 5)
			So(err, ShouldNotBeNil)
		})

	})

	Convey("Test UploadAnimatedEmoteImage", t, func() {
		expectedKey := "emoticonab123-light-1.0.gif"
		Convey("With success", func() {
			mockS3Client.Calls = []mock.Call{}
			mockS3Client.On("PutObject", mock.Anything, mock.Anything).Return(nil, nil)
			s3Uploader.UploadAnimatedEmoteImage(context.Background(), testEmoteId, testImageSize, testImageBytes, "light")

			So(mockS3Client.Calls[0].Arguments.Get(1), ShouldResemble, &awsS3.PutObjectInput{
				ACL:         aws.String(awsS3.ObjectCannedACLPublicRead),
				ContentType: aws.String("image/gif"),
				Bucket:      aws.String(testUploadS3Bucket),
				Key:         aws.String(expectedKey),
				Body:        bytes.NewReader(testImageBytes),
			})
		})
	})
}
