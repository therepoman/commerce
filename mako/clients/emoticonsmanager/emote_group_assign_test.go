package emoticonsmanager_test

import (
	"context"
	"testing"
	"time"

	"code.justin.tv/commerce/mako/clients/ripley"

	autoapproval_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/autoapproval"

	em "code.justin.tv/commerce/mako/clients/emoticonsmanager"
	mako "code.justin.tv/commerce/mako/internal"
	"code.justin.tv/commerce/mako/localdb"
	paint_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/paint"
	"code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/internal_mock"
	mkd "code.justin.tv/commerce/mako/twirp/dashboard"
	"github.com/pkg/errors"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
)

func TestAssignToGroup(t *testing.T) {
	testUserID := "someUser"
	testEmoteID1 := "emotesv2_1234abcd"
	testEmoteID2 := "emotesv2_5678defg"
	testEmoteID3 := "emotesv2_9012hijk"
	testEmoteID4 := "emotesv2_3456lmno"
	testEmoteID5 := "emotesv2_7890pqrs"
	testEmoteID6 := "emotesv2_1234tuvw"
	testEmoteID7 := "emotesv2_5678xyza"
	testEmoteID8 := "emotesv2_9102bcde"
	testGroupID := "someGroup"
	testGroupID2 := "anotherGroup"
	testGroupID3 := "someAnimatedGroup"
	testOrigin := mkd.EmoteOrigin_Subscriptions
	testFollowerOrigin := mkd.EmoteOrigin_Follower

	emote1 := mako.Emote{ID: testEmoteID1, Code: "testEmote1", OwnerID: testUserID, State: mako.Archived, EmotesGroup: mkd.EmoteOrigin_Archive.String(), Domain: mako.EmotesDomain, AssetType: mako.StaticAssetType}
	emote2 := mako.Emote{ID: testEmoteID2, Code: "testEmote2", OwnerID: testUserID, State: mako.PendingArchived, EmotesGroup: mkd.EmoteOrigin_Archive.String(), Domain: mako.EmotesDomain}
	emote3 := mako.Emote{ID: testEmoteID3, Code: "testEmote3", GroupID: testGroupID, OwnerID: testUserID, Order: 0, State: mako.Active, EmotesGroup: mkd.EmoteOrigin_Subscriptions.String(), Domain: mako.EmotesDomain}
	emote4 := mako.Emote{ID: testEmoteID4, Code: "testEmote4", OwnerID: testUserID, State: mako.Inactive, Domain: mako.EmotesDomain}
	emote5 := mako.Emote{ID: testEmoteID5, Code: "testEmote5", GroupID: testGroupID, OwnerID: testUserID, Order: 1, State: mako.Pending, EmotesGroup: mkd.EmoteOrigin_Subscriptions.String(), Domain: mako.EmotesDomain}
	emote6 := mako.Emote{ID: testEmoteID6, Code: "testEmote5", OwnerID: testUserID, State: mako.Archived, EmotesGroup: mkd.EmoteOrigin_Archive.String(), Domain: mako.EmotesDomain}
	emote7 := mako.Emote{ID: testEmoteID7, Code: "testEmote7", GroupID: testGroupID2, OwnerID: testUserID, State: mako.Active, EmotesGroup: mkd.EmoteOrigin_Subscriptions.String(), Domain: mako.EmotesDomain}
	emote8 := mako.Emote{ID: testEmoteID8, Code: "testEmote8", GroupID: testGroupID3, OwnerID: testUserID, State: mako.Active, EmotesGroup: mkd.EmoteOrigin_Subscriptions.String(), Domain: mako.EmotesDomain, AssetType: mako.AnimatedAssetType}

	sampleEmotes := []mako.Emote{emote1, emote2, emote3, emote4, emote5, emote6, emote7, emote8}

	emoteGroup1 := mako.EmoteGroup{GroupID: testGroupID, OwnerID: testUserID}
	emoteGroup2 := mako.EmoteGroup{GroupID: testGroupID2, OwnerID: testUserID, GroupType: mako.EmoteGroupStatic}
	emoteGroup3 := mako.EmoteGroup{GroupID: testGroupID3, OwnerID: testUserID, GroupType: mako.EmoteGroupAnimated}
	sampleEmoteGroups := []mako.EmoteGroup{emoteGroup1, emoteGroup2, emoteGroup3}

	tests := []struct {
		scenario string
		test     func(t *testing.T)
	}{
		{
			scenario: "GIVEN EmoteDB ReadByIDs errors WHEN assign emote to group THEN return error",
			test: func(t *testing.T) {
				// GIVEN
				mockEmoteDBUncached := new(internal_mock.EmoteDB)
				emoticonsManager := em.EmoticonsManagerImpl{
					EmoteDBUncached: mockEmoteDBUncached,
				}
				mockEmoteDBUncached.On("ReadByIDs", mock.Anything, testEmoteID1).Return(nil, errors.New("some_error"))

				// WHEN
				res, err := emoticonsManager.AssignToGroup(context.Background(), testUserID, testEmoteID1, testGroupID, testOrigin)

				// THEN
				require.Error(t, err)
				require.Equal(t, res, mako.Emote{})
				require.Contains(t, err.Error(), "some_error")
			},
		},
		{
			scenario: "GIVEN emote does not exist WHEN assign emote to group THEN return error",
			test: func(t *testing.T) {
				// GIVEN
				fakeEmoteDB, _, err := localdb.MakeEmoteDB(context.Background(), sampleEmotes...)
				emoticonsManager := em.EmoticonsManagerImpl{
					EmoteDBUncached: fakeEmoteDB,
				}

				// WHEN
				res, err := emoticonsManager.AssignToGroup(context.Background(), testUserID, "nonexistent", testGroupID, testOrigin)

				// THEN
				require.Error(t, err)
				require.Equal(t, err, em.EmoteDoesNotExist)
				require.Equal(t, res, mako.Emote{})
			},
		},
		{
			scenario: "GIVEN user does not own the emote WHEN assign emote to group THEN return error",
			test: func(t *testing.T) {
				// GIVEN
				fakeEmoteDB, _, err := localdb.MakeEmoteDB(context.Background(), sampleEmotes...)
				emoticonsManager := em.EmoticonsManagerImpl{
					EmoteDBUncached: fakeEmoteDB,
				}

				// WHEN
				res, err := emoticonsManager.AssignToGroup(context.Background(), "nonowner", testEmoteID1, testGroupID, testOrigin)

				// THEN
				require.Error(t, err)
				require.Equal(t, err, em.UserNotPermitted)
				require.Equal(t, res, mako.Emote{})
			},
		},
		{
			scenario: "GIVEN emote is already in the specified group WHEN assign emote to group THEN return unchanged emote and no error",
			test: func(t *testing.T) {
				// GIVEN
				fakeEmoteDB, _, err := localdb.MakeEmoteDB(context.Background(), sampleEmotes...)
				emoticonsManager := em.EmoticonsManagerImpl{
					EmoteDBUncached: fakeEmoteDB,
				}

				// WHEN
				res, err := emoticonsManager.AssignToGroup(context.Background(), testUserID, testEmoteID3, testGroupID, testOrigin)

				// THEN
				require.NoError(t, err)
				require.Equal(t, res, emote3)
			},
		},
		{
			scenario: "GIVEN emote has an invalid state WHEN assign emote to group THEN return error",
			test: func(t *testing.T) {
				// GIVEN
				fakeEmoteDB, _, err := localdb.MakeEmoteDB(context.Background(), sampleEmotes...)
				emoticonsManager := em.EmoticonsManagerImpl{
					EmoteDB:         fakeEmoteDB,
					EmoteDBUncached: fakeEmoteDB,
				}

				// WHEN
				res, err := emoticonsManager.AssignToGroup(context.Background(), testUserID, testEmoteID4, testGroupID, testOrigin)

				// THEN
				require.Error(t, err)
				require.Equal(t, err, em.InvalidEmoteState)
				require.Equal(t, res, mako.Emote{})
			},
		},
		{
			scenario: "GIVEN emote has a nonunique among existing active and pending emotes WHEN assign emote to group THEN return error",
			test: func(t *testing.T) {
				// GIVEN
				fakeEmoteDB, _, err := localdb.MakeEmoteDB(context.Background(), sampleEmotes...)
				emoticonsManager := em.EmoticonsManagerImpl{
					EmoteDB:         fakeEmoteDB,
					EmoteDBUncached: fakeEmoteDB,
				}

				// WHEN
				res, err := emoticonsManager.AssignToGroup(context.Background(), testUserID, testEmoteID6, testGroupID, testOrigin)

				// THEN
				require.Error(t, err)
				require.Equal(t, err, em.EmoticonCodeNotUnique)
				require.Equal(t, res, mako.Emote{})
			},
		},
		{
			scenario: "GIVEN emote has static assetType and group has animated type WHEN assign emote to group THEN return error",
			test: func(t *testing.T) {
				// GIVEN
				fakeEmoteDB, _, err := localdb.MakeEmoteDB(context.Background(), sampleEmotes...)
				require.Nil(t, err)

				fakeEmoteGroupDB, _, err := localdb.MakeEmoteGroupDB(context.Background(), sampleEmoteGroups...)
				require.Nil(t, err)

				emoticonsManager := em.EmoticonsManagerImpl{
					EmoteDB:         fakeEmoteDB,
					EmoteDBUncached: fakeEmoteDB,
					EmoteGroupDB:    fakeEmoteGroupDB,
				}

				// WHEN
				res, err := emoticonsManager.AssignToGroup(context.Background(), testUserID, testEmoteID1, testGroupID3, testOrigin)

				// THEN
				require.Error(t, err)
				require.Equal(t, err, em.EmoteAssetTypeDoesNotMatchGroupType)
				require.Equal(t, res, mako.Emote{})
			},
		},
		{
			scenario: "GIVEN emote has animated assetType and group has static type WHEN assign emote to group THEN return error",
			test: func(t *testing.T) {
				// GIVEN
				fakeEmoteDB, _, err := localdb.MakeEmoteDB(context.Background(), sampleEmotes...)
				require.Nil(t, err)

				fakeEmoteGroupDB, _, err := localdb.MakeEmoteGroupDB(context.Background(), sampleEmoteGroups...)
				require.Nil(t, err)

				emoticonsManager := em.EmoticonsManagerImpl{
					EmoteDB:         fakeEmoteDB,
					EmoteDBUncached: fakeEmoteDB,
					EmoteGroupDB:    fakeEmoteGroupDB,
				}

				// WHEN
				res, err := emoticonsManager.AssignToGroup(context.Background(), testUserID, testEmoteID8, testGroupID2, testOrigin)

				// THEN
				require.Error(t, err)
				require.Equal(t, err, em.EmoteAssetTypeDoesNotMatchGroupType)
				require.Equal(t, res, mako.Emote{})
			},
		},
		{
			scenario: "GIVEN emote has animated assetType and group has default type WHEN assign emote to group THEN return error",
			test: func(t *testing.T) {
				// GIVEN
				fakeEmoteDB, _, err := localdb.MakeEmoteDB(context.Background(), sampleEmotes...)
				require.Nil(t, err)

				fakeEmoteGroupDB, _, err := localdb.MakeEmoteGroupDB(context.Background(), sampleEmoteGroups...)
				require.Nil(t, err)

				emoticonsManager := em.EmoticonsManagerImpl{
					EmoteDB:         fakeEmoteDB,
					EmoteDBUncached: fakeEmoteDB,
					EmoteGroupDB:    fakeEmoteGroupDB,
				}

				// WHEN
				res, err := emoticonsManager.AssignToGroup(context.Background(), testUserID, testEmoteID8, testGroupID, testOrigin)

				// THEN
				require.Error(t, err)
				require.Equal(t, err, em.EmoteAssetTypeDoesNotMatchGroupType)
				require.Equal(t, res, mako.Emote{})
			},
		},
		{
			scenario: "GIVEN updating the emote errors WHEN assign emote to group THEN return error",
			test: func(t *testing.T) {
				// GIVEN
				mockEmoteDB := new(internal_mock.EmoteDB)
				mockEmoteDBUncached := new(internal_mock.EmoteDB)
				mockEmoteGroupDB := new(internal_mock.EmoteGroupDB)
				emoticonsManager := em.EmoticonsManagerImpl{
					EmoteDB:         mockEmoteDB,
					EmoteDBUncached: mockEmoteDBUncached,
					EmoteGroupDB:    mockEmoteGroupDB,
				}
				mockEmoteDBUncached.On("ReadByIDs", mock.Anything, testEmoteID1).Return([]mako.Emote{emote1}, nil)
				mockEmoteDB.On("Query", mock.Anything, mako.EmoteQuery{
					Code:   emote1.Code,
					Domain: mako.EmotesDomain,
					States: []mako.EmoteState{
						mako.Active, mako.Pending,
					},
				}).Return(nil, nil) // emote code uniqueness check passes
				mockEmoteDB.On("Query", mock.Anything, mako.EmoteQuery{
					GroupID: testGroupID,
					States:  em.OrderedEmoteStates,
				}).Return([]mako.Emote{emote3, emote5}, nil) // emote group count
				mockEmoteGroupDB.On("GetEmoteGroups", mock.Anything, testGroupID).Return([]mako.EmoteGroup{
					{
						GroupID:   testGroupID,
						OwnerID:   testUserID,
						Origin:    mako.EmoteGroupOrigin(testOrigin),
						GroupType: mako.EmoteGroupStatic,
					},
				}, nil)
				mockEmoteDB.On("UpdateEmote", mock.Anything, testEmoteID1, mock.Anything).Return(mako.Emote{}, errors.New("some_error"))

				// WHEN
				res, err := emoticonsManager.AssignToGroup(context.Background(), testUserID, testEmoteID1, testGroupID, testOrigin)

				// THEN
				require.Error(t, err)
				require.Equal(t, res, mako.Emote{})
				require.Contains(t, err.Error(), "some_error")
			},
		},
		{
			scenario: "GIVEN normalizing emote orders errors due to query failure WHEN assign emote to group THEN return error",
			test: func(t *testing.T) {
				// GIVEN
				mockEmoteDB := new(internal_mock.EmoteDB)
				mockEmoteDBUncached := new(internal_mock.EmoteDB)
				mockEmoteGroupDB := new(internal_mock.EmoteGroupDB)
				emoticonsManager := em.EmoticonsManagerImpl{
					EmoteDB:         mockEmoteDB,
					EmoteDBUncached: mockEmoteDBUncached,
					EmoteGroupDB:    mockEmoteGroupDB,
				}
				mockEmoteDBUncached.On("ReadByIDs", mock.Anything, testEmoteID7).Return([]mako.Emote{emote7}, nil)
				mockEmoteDB.On("Query", mock.Anything, mako.EmoteQuery{
					Code:   emote7.Code,
					Domain: mako.EmotesDomain,
					States: []mako.EmoteState{
						mako.Active, mako.Pending,
					},
				}).Return(nil, nil) // emote code uniqueness check passes
				mockEmoteDB.On("Query", mock.Anything, mako.EmoteQuery{
					GroupID: testGroupID,
					States:  em.OrderedEmoteStates,
				}).Return([]mako.Emote{emote3, emote5}, nil) // emote group count
				mockEmoteGroupDB.On("GetEmoteGroups", mock.Anything, testGroupID).Return([]mako.EmoteGroup{
					{
						GroupID:   testGroupID,
						OwnerID:   testUserID,
						Origin:    mako.EmoteGroupOrigin(testOrigin),
						GroupType: mako.EmoteGroupStatic,
					},
				}, nil)
				mockEmoteDB.On("UpdateEmote", mock.Anything, testEmoteID7, mock.Anything).Return(mako.Emote{}, nil)
				mockEmoteDB.On("Query", mock.Anything, mako.EmoteQuery{
					GroupID: emote7.GroupID,
					States:  em.OrderedEmoteStates,
				}).Return(nil, errors.New("some_error")) // normalize count query

				// WHEN
				res, err := emoticonsManager.AssignToGroup(context.Background(), testUserID, testEmoteID7, testGroupID, testOrigin)

				// THEN
				require.Error(t, err)
				require.Equal(t, res, mako.Emote{})
				require.Contains(t, err.Error(), "some_error")
			},
		},
		{
			scenario: "GIVEN successful archived emote group assignment WHEN assign emote to group THEN return changed emote and no error",
			test: func(t *testing.T) {
				// GIVEN
				fakeEmoteDB, _, err := localdb.MakeEmoteDB(context.Background(), sampleEmotes...)
				fakeEmoteGroupDB, _, err := localdb.MakeEmoteGroupDB(context.Background(), sampleEmoteGroups...)
				mockPaint := new(paint_mock.PaintClient)
				emoticonsManager := em.EmoticonsManagerImpl{
					EmoteDB:         fakeEmoteDB,
					EmoteDBUncached: fakeEmoteDB,
					EmoteGroupDB:    fakeEmoteGroupDB,
					PaintClient:     mockPaint,
				}
				mockPaint.On("ModifyEmote", mock.Anything, mock.Anything).Return(nil, nil)

				// WHEN
				res, err := emoticonsManager.AssignToGroup(context.Background(), testUserID, testEmoteID1, testGroupID, testOrigin)

				// THEN
				require.NoError(t, err)
				require.Equal(t, res.State, mako.Active)
				require.Equal(t, res.Order, int64(2))
				require.Equal(t, res.EmotesGroup, testOrigin.String())
				require.Equal(t, res.GroupID, testGroupID)

				dbEmotes, err := fakeEmoteDB.ReadByGroupIDs(context.Background(), testGroupID)

				require.Equal(t, len(dbEmotes), 3)

				time.Sleep(50 * time.Millisecond) // wait for goroutine
				mockPaint.AssertNumberOfCalls(t, "ModifyEmote", 1)
			},
		},
		{
			scenario: "GIVEN successful pending_archived emote BitsBadgeTierEmotes group assignment WHEN assign emote to group THEN return changed emote and no error",
			test: func(t *testing.T) {
				// GIVEN
				fakeEmoteDB, _, err := localdb.MakeEmoteDB(context.Background(), sampleEmotes...)
				fakeEmoteGroupDB, _, err := localdb.MakeEmoteGroupDB(context.Background(), sampleEmoteGroups...)
				mockPaint := new(paint_mock.PaintClient)
				emoticonsManager := em.EmoticonsManagerImpl{
					EmoteDB:         fakeEmoteDB,
					EmoteDBUncached: fakeEmoteDB,
					EmoteGroupDB:    fakeEmoteGroupDB,
					PaintClient:     mockPaint,
				}

				// WHEN
				res, err := emoticonsManager.AssignToGroup(context.Background(), testUserID, testEmoteID2, testGroupID, mkd.EmoteOrigin_BitsBadgeTierEmotes)

				// THEN
				require.NoError(t, err)
				require.Equal(t, res.State, mako.Pending)
				require.Equal(t, res.Order, int64(2))
				require.Equal(t, res.EmotesGroup, mkd.EmoteOrigin_BitsBadgeTierEmotes.String())
				require.Equal(t, res.GroupID, testGroupID)

				dbEmotes, err := fakeEmoteDB.ReadByGroupIDs(context.Background(), testGroupID)

				require.Equal(t, len(dbEmotes), 3)

				mockPaint.AssertNumberOfCalls(t, "ModifyEmote", 0)
			},
		},
		{
			scenario: "GIVEN successful active emote group assignment WHEN assign emote to group THEN return changed emote and no error",
			test: func(t *testing.T) {
				// GIVEN
				fakeEmoteDB, _, err := localdb.MakeEmoteDB(context.Background(), sampleEmotes...)
				fakeEmoteGroupDB, _, err := localdb.MakeEmoteGroupDB(context.Background(), sampleEmoteGroups...)
				mockPaint := new(paint_mock.PaintClient)
				emoticonsManager := em.EmoticonsManagerImpl{
					EmoteDB:         fakeEmoteDB,
					EmoteDBUncached: fakeEmoteDB,
					EmoteGroupDB:    fakeEmoteGroupDB,
					PaintClient:     mockPaint,
				}
				mockPaint.On("ModifyEmote", mock.Anything, mock.Anything).Return(nil, nil)

				// WHEN
				res, err := emoticonsManager.AssignToGroup(context.Background(), testUserID, testEmoteID3, testGroupID2, testOrigin)

				// THEN
				require.NoError(t, err)
				require.Equal(t, res.State, mako.Active)
				require.Equal(t, res.Order, int64(1))
				require.Equal(t, res.EmotesGroup, testOrigin.String())
				require.Equal(t, res.GroupID, testGroupID2)

				dbEmotes, err := fakeEmoteDB.ReadByGroupIDs(context.Background(), testGroupID2)
				require.Equal(t, len(dbEmotes), 2)

				// Check that emotes in old group are normalized
				dbEmotes, err = fakeEmoteDB.ReadByGroupIDs(context.Background(), testGroupID)
				require.Equal(t, len(dbEmotes), 1)
				require.Equal(t, dbEmotes[0].Order, int64(0))

				time.Sleep(50 * time.Millisecond) // wait for goroutine
				mockPaint.AssertNumberOfCalls(t, "ModifyEmote", 1)
			},
		},
		{
			scenario: "GIVEN user not in good standing WHEN assign emote to follower group THEN return error",
			test: func(t *testing.T) {
				// GIVEN
				mockEligibilityFetcher := new(autoapproval_mock.EligibilityFetcher)
				fakeEmoteDB, _, err := localdb.MakeEmoteDB(context.Background(), sampleEmotes...)
				fakeEmoteGroupDB, _, err := localdb.MakeEmoteGroupDB(context.Background(), sampleEmoteGroups...)
				mockPaint := new(paint_mock.PaintClient)
				emoticonsManager := em.EmoticonsManagerImpl{
					AutoApprovalEligibilityFetcher: mockEligibilityFetcher,
					EmoteDB:                        fakeEmoteDB,
					EmoteDBUncached:                fakeEmoteDB,
					EmoteGroupDB:                   fakeEmoteGroupDB,
					PaintClient:                    mockPaint,
				}
				mockEligibilityFetcher.On("IsUserEligibleForEmoteUploadAutoApproval", mock.Anything, testUserID).Return(false, ripley.PartnerType(""), nil)
				mockPaint.On("ModifyEmote", mock.Anything, mock.Anything).Return(nil, nil)

				// WHEN
				res, err := emoticonsManager.AssignToGroup(context.Background(), testUserID, testEmoteID3, testGroupID2, testFollowerOrigin)

				// THEN
				require.Error(t, err)
				require.Equal(t, res, mako.Emote{})
			},
		},
		{
			scenario: "GIVEN user not in good standing WHEN assign emote to follower group THEN return error",
			test: func(t *testing.T) {
				// GIVEN
				mockEligibilityFetcher := new(autoapproval_mock.EligibilityFetcher)
				fakeEmoteDB, _, err := localdb.MakeEmoteDB(context.Background(), sampleEmotes...)
				fakeEmoteGroupDB, _, err := localdb.MakeEmoteGroupDB(context.Background(), sampleEmoteGroups...)
				mockPaint := new(paint_mock.PaintClient)
				emoticonsManager := em.EmoticonsManagerImpl{
					AutoApprovalEligibilityFetcher: mockEligibilityFetcher,
					EmoteDB:                        fakeEmoteDB,
					EmoteDBUncached:                fakeEmoteDB,
					EmoteGroupDB:                   fakeEmoteGroupDB,
					PaintClient:                    mockPaint,
				}
				mockEligibilityFetcher.On("IsUserEligibleForEmoteUploadAutoApproval", mock.Anything, testUserID).Return(false, ripley.PartnerType(""), errors.New("test error"))
				mockPaint.On("ModifyEmote", mock.Anything, mock.Anything).Return(nil, nil)

				// WHEN
				res, err := emoticonsManager.AssignToGroup(context.Background(), testUserID, testEmoteID3, testGroupID2, testFollowerOrigin)

				// THEN
				require.Error(t, err)
				require.Equal(t, res, mako.Emote{})
				require.Contains(t, err.Error(), "test error")
			},
		},
		{
			scenario: "GIVEN user in good standing WHEN assign emote to follower group THEN return changed emote and no error",
			test: func(t *testing.T) {
				// GIVEN
				mockEligibilityFetcher := new(autoapproval_mock.EligibilityFetcher)
				fakeEmoteDB, _, err := localdb.MakeEmoteDB(context.Background(), sampleEmotes...)
				fakeEmoteGroupDB, _, err := localdb.MakeEmoteGroupDB(context.Background(), sampleEmoteGroups...)
				mockPaint := new(paint_mock.PaintClient)
				emoticonsManager := em.EmoticonsManagerImpl{
					AutoApprovalEligibilityFetcher: mockEligibilityFetcher,
					EmoteDB:                        fakeEmoteDB,
					EmoteDBUncached:                fakeEmoteDB,
					EmoteGroupDB:                   fakeEmoteGroupDB,
					PaintClient:                    mockPaint,
				}
				mockEligibilityFetcher.On("IsUserEligibleForEmoteUploadAutoApproval", mock.Anything, testUserID).Return(true, ripley.PartnerType(""), nil)
				mockPaint.On("ModifyEmote", mock.Anything, mock.Anything).Return(nil, nil)

				// WHEN
				res, err := emoticonsManager.AssignToGroup(context.Background(), testUserID, testEmoteID3, testGroupID2, testFollowerOrigin)

				// THEN
				require.NoError(t, err)
				require.Equal(t, res.State, mako.Active)
				require.Equal(t, res.Order, int64(1))
				require.Equal(t, res.EmotesGroup, testFollowerOrigin.String())
				require.Equal(t, res.GroupID, testGroupID2)

				dbEmotes, err := fakeEmoteDB.ReadByGroupIDs(context.Background(), testGroupID2)
				require.Equal(t, len(dbEmotes), 2)

				// Check that emotes in old group are normalized
				dbEmotes, err = fakeEmoteDB.ReadByGroupIDs(context.Background(), testGroupID)
				require.Equal(t, len(dbEmotes), 1)
				require.Equal(t, dbEmotes[0].Order, int64(0))

				time.Sleep(50 * time.Millisecond) // wait for goroutine
				mockPaint.AssertNumberOfCalls(t, "ModifyEmote", 0)
			},
		},
	}

	for _, test := range tests {
		t.Run(test.scenario, func(t *testing.T) {
			test.test(t)
		})
	}
}

func TestRemoveFromGroup(t *testing.T) {
	testUserID := "someUser"
	testEmoteID1 := "emotesv2_1234abcd"
	testEmoteID2 := "emotesv2_5678defg"
	testEmoteID3 := "emotesv2_9012hijk"
	testGroupID := "someGroup"

	emote1 := mako.Emote{ID: testEmoteID1, Code: "testEmote1", GroupID: testGroupID, OwnerID: testUserID, Order: 0, State: mako.Active, EmotesGroup: mkd.EmoteOrigin_Subscriptions.String(), Domain: mako.EmotesDomain}
	emote2 := mako.Emote{ID: testEmoteID2, Code: "testEmote2", GroupID: testGroupID, OwnerID: testUserID, Order: 1, State: mako.Pending, EmotesGroup: mkd.EmoteOrigin_Subscriptions.String(), Domain: mako.EmotesDomain}
	emote3 := mako.Emote{ID: testEmoteID3, Code: "testEmote3", OwnerID: testUserID, State: mako.Archived, EmotesGroup: mkd.EmoteOrigin_Archive.String(), Domain: mako.EmotesDomain}

	sampleEmotes := []mako.Emote{emote1, emote2, emote3}

	tests := []struct {
		scenario string
		test     func(t *testing.T)
	}{
		{
			scenario: "GIVEN EmoteDB ReadByIDs errors WHEN remove emote from group THEN return error",
			test: func(t *testing.T) {
				// GIVEN
				mockEmoteDB := new(internal_mock.EmoteDB)
				mockEmoteDBUncached := new(internal_mock.EmoteDB)
				emoticonsManager := em.EmoticonsManagerImpl{
					EmoteDB:         mockEmoteDB,
					EmoteDBUncached: mockEmoteDBUncached,
				}
				mockEmoteDBUncached.On("ReadByIDs", mock.Anything, testEmoteID1).Return(nil, errors.New("some_error"))

				// WHEN
				res, err := emoticonsManager.RemoveFromGroup(context.Background(), testUserID, testEmoteID1)

				// THEN
				require.Error(t, err)
				require.Equal(t, res, mako.Emote{})
				require.Contains(t, err.Error(), "some_error")
			},
		},
		{
			scenario: "GIVEN emote does not exist WHEN remove emote from group THEN return error",
			test: func(t *testing.T) {
				// GIVEN
				fakeEmoteDB, _, err := localdb.MakeEmoteDB(context.Background(), sampleEmotes...)
				emoticonsManager := em.EmoticonsManagerImpl{
					EmoteDB:         fakeEmoteDB,
					EmoteDBUncached: fakeEmoteDB,
				}

				// WHEN
				res, err := emoticonsManager.RemoveFromGroup(context.Background(), testUserID, "nonexistent")

				// THEN
				require.Error(t, err)
				require.Equal(t, err, em.EmoteDoesNotExist)
				require.Equal(t, res, mako.Emote{})
			},
		},
		{
			scenario: "GIVEN user does not own the emote WHEN remove emote from group THEN return error",
			test: func(t *testing.T) {
				// GIVEN
				fakeEmoteDB, _, err := localdb.MakeEmoteDB(context.Background(), sampleEmotes...)
				emoticonsManager := em.EmoticonsManagerImpl{
					EmoteDB:         fakeEmoteDB,
					EmoteDBUncached: fakeEmoteDB,
				}

				// WHEN
				res, err := emoticonsManager.RemoveFromGroup(context.Background(), "nonowner", testEmoteID1)

				// THEN
				require.Error(t, err)
				require.Equal(t, err, em.UserNotPermitted)
				require.Equal(t, res, mako.Emote{})
			},
		},
		{
			scenario: "GIVEN emote is already without a group WHEN remove emote from group THEN return unchanged emote and no error",
			test: func(t *testing.T) {
				// GIVEN
				fakeEmoteDB, _, err := localdb.MakeEmoteDB(context.Background(), sampleEmotes...)
				emoticonsManager := em.EmoticonsManagerImpl{
					EmoteDB:         fakeEmoteDB,
					EmoteDBUncached: fakeEmoteDB,
				}

				// WHEN
				res, err := emoticonsManager.RemoveFromGroup(context.Background(), testUserID, testEmoteID3)

				// THEN
				require.NoError(t, err)
				require.Equal(t, res, emote3)
			},
		},
		{
			scenario: "GIVEN updating the emote errors WHEN remove emote from group THEN return error",
			test: func(t *testing.T) {
				// GIVEN
				mockEmoteDB := new(internal_mock.EmoteDB)
				mockEmoteDBUncached := new(internal_mock.EmoteDB)
				emoticonsManager := em.EmoticonsManagerImpl{
					EmoteDB:         mockEmoteDB,
					EmoteDBUncached: mockEmoteDBUncached,
				}
				mockEmoteDBUncached.On("ReadByIDs", mock.Anything, testEmoteID1).Return([]mako.Emote{emote1}, nil)
				mockEmoteDB.On("UpdateEmote", mock.Anything, testEmoteID1, mock.Anything).Return(mako.Emote{}, errors.New("some_error"))

				// WHEN
				res, err := emoticonsManager.RemoveFromGroup(context.Background(), testUserID, testEmoteID1)

				// THEN
				require.Error(t, err)
				require.Equal(t, res, mako.Emote{})
				require.Contains(t, err.Error(), "some_error")
			},
		},
		{
			scenario: "GIVEN normalizing emote orders errors due to query failure WHEN remove emote from group THEN return error",
			test: func(t *testing.T) {
				// GIVEN
				mockEmoteDB := new(internal_mock.EmoteDB)
				mockEmoteDBUncached := new(internal_mock.EmoteDB)
				emoticonsManager := em.EmoticonsManagerImpl{
					EmoteDB:         mockEmoteDB,
					EmoteDBUncached: mockEmoteDBUncached,
				}
				mockEmoteDBUncached.On("ReadByIDs", mock.Anything, testEmoteID1).Return([]mako.Emote{emote1}, nil)
				mockEmoteDB.On("UpdateEmote", mock.Anything, testEmoteID1, mock.Anything).Return(mako.Emote{}, nil)
				mockEmoteDB.On("Query", mock.Anything, mako.EmoteQuery{
					GroupID: emote1.GroupID,
					States:  em.OrderedEmoteStates,
				}).Return(nil, errors.New("some_error")) // normalize count query

				// WHEN
				res, err := emoticonsManager.RemoveFromGroup(context.Background(), testUserID, testEmoteID1)

				// THEN
				require.Error(t, err)
				require.Equal(t, res, mako.Emote{})
				require.Contains(t, err.Error(), "some_error")
			},
		},
		{
			scenario: "GIVEN successful active emote group removal WHEN remove emote from group THEN return changed emote and no error",
			test: func(t *testing.T) {
				// GIVEN
				fakeEmoteDB, _, err := localdb.MakeEmoteDB(context.Background(), sampleEmotes...)
				emoticonsManager := em.EmoticonsManagerImpl{
					EmoteDB:         fakeEmoteDB,
					EmoteDBUncached: fakeEmoteDB,
				}

				// WHEN
				res, err := emoticonsManager.RemoveFromGroup(context.Background(), testUserID, testEmoteID1)

				// THEN
				require.NoError(t, err)
				require.Equal(t, res.State, mako.Archived)
				require.Equal(t, res.Order, int64(0))
				require.Equal(t, res.EmotesGroup, mkd.EmoteOrigin_Archive.String())
				require.Empty(t, res.GroupID)

				// Check that remaining emotes are normalized
				dbEmotes, err := fakeEmoteDB.ReadByGroupIDs(context.Background(), testGroupID)
				require.Equal(t, len(dbEmotes), 1)
				require.Equal(t, dbEmotes[0].Order, int64(0))
			},
		},
		{
			scenario: "GIVEN successful pending emote group removal WHEN remove emote from group THEN return changed emote and no error",
			test: func(t *testing.T) {
				// GIVEN
				fakeEmoteDB, _, err := localdb.MakeEmoteDB(context.Background(), sampleEmotes...)
				mockPaint := new(paint_mock.PaintClient)
				emoticonsManager := em.EmoticonsManagerImpl{
					EmoteDB:         fakeEmoteDB,
					EmoteDBUncached: fakeEmoteDB,
					PaintClient:     mockPaint,
				}

				// WHEN
				res, err := emoticonsManager.RemoveFromGroup(context.Background(), testUserID, testEmoteID2)

				// THEN
				require.NoError(t, err)
				require.Equal(t, res.State, mako.PendingArchived)
				require.Equal(t, res.Order, int64(0))
				require.Equal(t, res.EmotesGroup, mkd.EmoteOrigin_Archive.String())
				require.Empty(t, res.GroupID)

				// Check that remaining emotes are normalized
				dbEmotes, err := fakeEmoteDB.ReadByGroupIDs(context.Background(), testGroupID)
				require.Equal(t, len(dbEmotes), 1)
				require.Equal(t, dbEmotes[0].Order, int64(0))
			},
		},
	}

	for _, test := range tests {
		t.Run(test.scenario, func(t *testing.T) {
			test.test(t)
		})
	}
}
