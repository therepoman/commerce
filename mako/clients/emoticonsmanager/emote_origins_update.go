package emoticonsmanager

import (
	"context"

	mkd "code.justin.tv/commerce/mako/twirp/dashboard"

	mako "code.justin.tv/commerce/mako/internal"
	"github.com/pkg/errors"
)

type UpdateOriginsParams struct {
	NewOrigins map[string]mkd.EmoteOrigin
}

func (e *EmoticonsManagerImpl) UpdateEmoteOrigins(ctx context.Context, params UpdateOriginsParams) ([]mako.Emote, error) {
	// Extract emoteIDs from params
	emoteIDs := make([]string, 0, len(params.NewOrigins))
	for emoteID, _ := range params.NewOrigins {
		emoteIDs = append(emoteIDs, emoteID)
	}

	// Get the specified emotes
	emotes, err := e.EmoteDBUncached.ReadByIDs(ctx, emoteIDs...)
	if err != nil {
		return nil, errors.Wrap(err, "Failed to get emote for origin update")
	}

	// Check if any of them does not exist
	if len(emotes) < len(emoteIDs) {
		return nil, EmoteDoesNotExist
	}

	newEmotes := make([]mako.Emote, 0, len(emotes))
	for _, emote := range emotes {
		// Get the emote's new origin
		newOrigin := params.NewOrigins[emote.ID].String()

		// If new origin and existing origin are the same, then skip the write
		if newOrigin == emote.EmotesGroup {
			newEmotes = append(newEmotes, emote)
			continue
		}

		// Update the emote with its new origin
		newEmote, err := e.EmoteDB.UpdateEmote(ctx, emote.ID, mako.EmoteUpdate{EmotesGroup: &newOrigin})
		if err != nil {
			return nil, errors.Wrap(err, "Failed to update emote in EmoteDB for origin update")
		}

		newEmotes = append(newEmotes, newEmote)
	}

	// Return the updated versions of the emotes
	return newEmotes, nil
}
