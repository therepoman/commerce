package materia

import (
	"context"
	"net/http"

	materiatwirp "code.justin.tv/amzn/MateriaTwirp"
)

type EmoteEntitlementMetadata struct {
	GroupID string `json:"group_id"`
}

type clientWrapper struct {
	Client materiatwirp.Materia
}

type MateriaClient interface {
	CreateEntitlements(ctx context.Context, req *materiatwirp.CreateEntitlementsRequest) (res *materiatwirp.CreateEntitlementsResponse, err error)
	UpsertEntitlements(ctx context.Context, req *materiatwirp.UpsertEntitlementsRequest) (res *materiatwirp.UpsertEntitlementsResponse, err error)
	DeleteEntitlements(ctx context.Context, req *materiatwirp.DeleteEntitlementsRequest) (res *materiatwirp.DeleteEntitlementsResponse, err error)
	GetEntitlements(ctx context.Context, req *materiatwirp.GetEntitlementsRequest) (res *materiatwirp.GetEntitlementsResponse, err error)
}

func NewClient(host string) MateriaClient {
	return &clientWrapper{
		Client: materiatwirp.NewMateriaProtobufClient(host, &http.Client{
			Transport: &http.Transport{
				MaxIdleConnsPerHost: 1000,
			},
		}),
	}
}

func (p *clientWrapper) CreateEntitlements(ctx context.Context, req *materiatwirp.CreateEntitlementsRequest) (res *materiatwirp.CreateEntitlementsResponse, err error) {
	return p.Client.CreateEntitlements(ctx, req)
}

func (p *clientWrapper) UpsertEntitlements(ctx context.Context, req *materiatwirp.UpsertEntitlementsRequest) (res *materiatwirp.UpsertEntitlementsResponse, err error) {
	return p.Client.UpsertEntitlements(ctx, req)
}

func (p *clientWrapper) DeleteEntitlements(ctx context.Context, req *materiatwirp.DeleteEntitlementsRequest) (res *materiatwirp.DeleteEntitlementsResponse, err error) {
	return p.Client.DeleteEntitlements(ctx, req)
}

func (p *clientWrapper) GetEntitlements(ctx context.Context, req *materiatwirp.GetEntitlementsRequest) (res *materiatwirp.GetEntitlementsResponse, err error) {
	return p.Client.GetEntitlements(ctx, req)
}
