package materia

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
	"sync"
	"time"

	materiatwirp "code.justin.tv/amzn/MateriaTwirp"
	mako "code.justin.tv/commerce/mako/internal"
	"github.com/afex/hystrix-go/hystrix"
	"github.com/cenkalti/backoff"
	"github.com/golang/protobuf/ptypes/timestamp"
	"github.com/pkg/errors"
)

const (
	HystrixNameMateriaEntitlementsGet = "materiav2.entitlements.GET"
	concurrentWaitGroups              = 6
	numRetries                        = 1
)

type EntitlementDB struct {
	client materiatwirp.Materia
}

func NewEntitlementDB(addr string) *EntitlementDB {
	return &EntitlementDB{
		client: materiatwirp.NewMateriaProtobufClient(addr, &http.Client{
			Transport: &http.Transport{
				MaxIdleConnsPerHost: 1000,
			},
		}),
	}
}

func (db *EntitlementDB) batchQuery(ctx context.Context, ownerID string, keys ...string) ([]mako.Entitlement, error) {
	var keysCh = make(chan string)
	var items = make(chan []mako.Entitlement, len(keys))
	var errs = make(chan error, len(keys))
	var wg sync.WaitGroup
	wg.Add(concurrentWaitGroups)

	for i := 0; i < concurrentWaitGroups; i++ {
		go func() {
			defer wg.Done()

			for {
				select {
				case <-ctx.Done():
				case id, ok := <-keysCh:
					if !ok {
						return
					}

					var results []mako.Entitlement

					if err := backoff.Retry(func() error {
						res, err := db.client.GetEntitlements(ctx, &materiatwirp.GetEntitlementsRequest{
							OwnerId: ownerID,
							Domain:  materiatwirp.Domain_EMOTES,
							ItemId:  id,
						})

						if err != nil {
							return err
						}

						results, err = toMakoEntitlements(res.GetEntitlements()...)
						return err
					}, backoff.WithMaxRetries(backoff.WithContext(newBackoff(), ctx), numRetries)); err != nil {
						errs <- err
						return
					}

					items <- results
				}
			}
		}()
	}

	for _, key := range keys {
		keysCh <- key
	}

	close(keysCh)

	entitlements := make([]mako.Entitlement, 0, len(keys))

	for range keys {
		select {
		case <-ctx.Done():
			return entitlements, ctx.Err()
		case err := <-errs:
			return entitlements, err
		case items := <-items:
			entitlements = append(entitlements, items...)
		}
	}

	return entitlements, nil
}

func (db *EntitlementDB) ReadByIDs(ctx context.Context, ownerID string, keys ...string) ([]mako.Entitlement, error) {
	if len(keys) == 0 {
		return nil, nil
	}

	var entitlements []mako.Entitlement

	if err := hystrix.DoC(ctx, HystrixNameMateriaEntitlementsGet, func(ctx context.Context) error {
		var err error
		entitlements, err = db.batchQuery(ctx, ownerID, keys...)
		return err
	}, nil); err != nil {
		return nil, err
	}

	return entitlements, nil
}

// Materia entitlements do not support channel-only emote entitlements yet, so the channelID provided here is not used
func (db *EntitlementDB) ReadByOwnerID(ctx context.Context, ownerID string, channelID string) ([]mako.Entitlement, error) {
	if ownerID == "" {
		return nil, nil
	}

	var entitlements []mako.Entitlement

	if err := hystrix.DoC(ctx, HystrixNameMateriaEntitlementsGet, func(ctx context.Context) error {
		var err error

		var materiaEntitlements []*materiatwirp.Entitlement
		// Call to Materia will read **all** emoticons for a particular owner/user.
		res, err := db.client.GetEntitlements(ctx, &materiatwirp.GetEntitlementsRequest{
			OwnerId: ownerID,
			Domain:  materiatwirp.Domain_EMOTES,
			States: []materiatwirp.State{
				// For now, Mako only cares about active entitlements
				materiatwirp.State_STATE_ACTIVE,
			},
		})

		if err == nil && res != nil {
			materiaEntitlements = res.Entitlements
		}

		entitlements, err = toMakoEntitlements(materiaEntitlements...)
		return err
	}, nil); err != nil {
		return nil, err
	}

	return entitlements, nil
}

func (db *EntitlementDB) WriteEntitlements(ctx context.Context, entitlements ...mako.Entitlement) error {
	var materiaEntitlements []*materiatwirp.Entitlement

	for _, entitlement := range entitlements {

		endsAt := materiatwirp.InfiniteTime{}
		if entitlement.End == nil {
			endsAt.IsInfinite = true
		} else {
			endsAt.Time = &timestamp.Timestamp{
				Seconds: entitlement.End.Unix(),
			}
		}

		meta, err := json.Marshal(entitlement.Metadata)
		if err != nil {
			return errors.Wrap(err, "failed to marshal entitlement metadata field")
		}

		source, err := makeSource(entitlement)
		if err != nil {
			return err
		}

		materiaEntitlements = append(materiaEntitlements, &materiatwirp.Entitlement{
			OwnerId:  entitlement.OwnerID,
			Domain:   materiatwirp.Domain_EMOTES,
			Source:   source,
			OriginId: entitlement.OriginID,
			ItemId:   entitlement.ID,
			// ItemOwnerId is typically a channel_id and represents the owner of the emote group.
			ItemOwnerId: entitlement.ChannelID,
			StartsAt: &timestamp.Timestamp{
				Seconds: entitlement.Start.Unix(),
			},
			Meta:   meta,
			Type:   string(entitlement.Type),
			EndsAt: &endsAt,
		})
	}

	if _, err := db.client.UpsertEntitlements(ctx, &materiatwirp.UpsertEntitlementsRequest{
		Entitlements: materiaEntitlements,
	}); err != nil {
		return errors.Wrap(err, "failed to upsert entitlements in Materia")
	}

	return nil
}

func toMakoEntitlements(materiaEntitlements ...*materiatwirp.Entitlement) ([]mako.Entitlement, error) {
	entitlements := make([]mako.Entitlement, 0, len(materiaEntitlements))
	for _, materiaEntitlement := range materiaEntitlements {
		entitlement, err := toMakoEntitlement(materiaEntitlement)
		if err != nil {
			return nil, err
		}

		entitlements = append(entitlements, entitlement)
	}

	return entitlements, nil
}

func toMakoEntitlement(materiaEntitlement *materiatwirp.Entitlement) (mako.Entitlement, error) {
	if materiaEntitlement == nil {
		return mako.Entitlement{}, nil
	}

	var end *time.Time
	if !materiaEntitlement.EndsAt.GetIsInfinite() {
		seconds := time.Unix(materiaEntitlement.EndsAt.GetTime().Seconds, 0)
		end = &seconds
	}

	var meta mako.EntitlementMetadata
	if len(materiaEntitlement.Meta) != 0 {
		if err := json.Unmarshal(materiaEntitlement.Meta, &meta); err != nil {
			return mako.Entitlement{}, errors.Wrapf(err, "failed to unmarshal entitlement id '%s'", materiaEntitlement.ItemId)
		}
	}

	entitlement := mako.Entitlement{
		OwnerID:   materiaEntitlement.OwnerId,
		ChannelID: materiaEntitlement.ItemOwnerId,
		ID:        materiaEntitlement.ItemId,
		OriginID:  materiaEntitlement.OriginId,
		Source:    mako.EntitlementSource(strings.ToLower(materiaEntitlement.Source.String())),
		Type:      mako.EntitlementType(materiaEntitlement.Type),
		Start:     time.Unix(materiaEntitlement.StartsAt.Seconds, 0),
		End:       end,
		Metadata:  meta,
	}

	return entitlement, nil
}

func makeSource(entitlement mako.Entitlement) (materiatwirp.Source, error) {
	var source materiatwirp.Source
	switch entitlement.Source {
	case mako.BitsBadgeTierSource:
		source = materiatwirp.Source_BITS
	case mako.SubscriptionsSource:
		source = materiatwirp.Source_SUBS
	case mako.ChannelPointsSource:
		source = materiatwirp.Source_CHANNEL_POINTS
	case mako.HypeTrainSource:
		source = materiatwirp.Source_HYPE_TRAIN
	case mako.MegaCommerceSource:
		source = materiatwirp.Source_MEGA_COMMERCE
	case mako.RewardsSource:
		source = materiatwirp.Source_REWARDS
	default:
		return source, fmt.Errorf("the source '%s' is not supported by WriteEntitlements", entitlement.Source)
	}

	return source, nil
}

func newBackoff() backoff.BackOff {
	exponential := backoff.NewExponentialBackOff()
	exponential.InitialInterval = 10 * time.Millisecond
	exponential.MaxElapsedTime = 1 * time.Second
	exponential.Multiplier = 2
	return exponential
}
