package experiments

import (
	"fmt"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/mako/config"
	"code.justin.tv/discovery/experiments"
	"code.justin.tv/discovery/experiments/experiment"
	"code.justin.tv/discovery/experiments/overridepolling"
)

const (
	experimentOverridesFile            = "experiment-overrides.json"
	experimentsPollingInterval         = 5 * time.Minute
	experimentOverridesPollingInterval = 5 * time.Minute
)

type clientWrapper struct {
	Client experiments.Experiments
}

type ExperimentsClient interface {
	StartGlobalOverridePolling()
	Close() error
}

func NewExperimentsClient(cfg *config.Configuration) (ExperimentsClient, error) {
	experimentsClient, err := experiments.New(experiments.Config{
		Platform: experiment.LegacyPlatform,
		// Mako does not need integration with Spade yet for experiments. We can change this flag when we do and
		// set up the other Spade config params.
		DisableSpadeTracking: true,
		PollingInterval:      experimentsPollingInterval,
		ErrorHandler: func(err error) {
			log.WithError(err).Error("Experiments client encountered an error")
		},
		GlobalOverridesConfig: &overridepolling.GlobalOverridesPollingConfig{
			Bucket:          cfg.ExperimentOverridesS3BucketName,
			Key:             experimentOverridesFile,
			PollingInterval: experimentOverridesPollingInterval,
		},
	})
	if err != nil {
		return nil, fmt.Errorf("Failed to initialize Experiments client: %v", err)
	}

	registerDefaultGroups(experimentsClient)

	return &clientWrapper{
		Client: experimentsClient,
	}, nil
}

func registerDefaultGroups(client experiments.Experiments) {
	// Call client.RegisterDefault(...) for every added experiment in order to define default behavior in case of failure to Treat()
}

func (c *clientWrapper) StartGlobalOverridePolling() {
	c.Client.StartGlobalOverridePolling()
}

func (c *clientWrapper) Close() error {
	return c.Client.Close()
}
