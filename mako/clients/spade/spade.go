package spade

import (
	"fmt"

	"golang.org/x/net/context"

	"code.justin.tv/commerce/mako/config"
	spadeClient "code.justin.tv/common/spade-client-go/spade"
)

type SpadeClientNoOp struct{}

type TwoFactorEmoteEntitlementEvent struct {
	Time     int64  `json:"time"`
	UserID   string `json:"user_id"`
	Entitled bool   `json:"entitled"`
}

func NewSpadeClient(cfg *config.Configuration) (spadeClient.Client, error) {
	if cfg.EnvironmentPrefix == "prod" {
		sc, err := spadeClient.NewClient()
		if err != nil {
			return nil, fmt.Errorf("Failed to initialize Spade client: %v", err)
		}

		return sc, nil
	}

	return &SpadeClientNoOp{}, nil
}

func (scn *SpadeClientNoOp) TrackEvent(ctx context.Context, event string, properties interface{}) error {
	return nil
}

func (scn *SpadeClientNoOp) TrackEvents(ctx context.Context, events ...spadeClient.Event) error {
	return nil
}
