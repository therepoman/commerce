package emoticongroups

import (
	"context"
	"math"
	"strconv"
	"testing"

	mako "code.justin.tv/commerce/mako/internal"
	"code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/internal_mock"

	mk "code.justin.tv/commerce/mako/twirp"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestEmoticonGroupsV2Client_CreateEmoteGroup(t *testing.T) {
	Convey("Testing CreateEmoteGroup", t, func() {
		emoteGroupsDao := new(internal_mock.EmoteGroupDB)
		mockOwnerId := "ownerId"

		ctx := context.Background()
		origin := mk.EmoteGroup_BitsBadgeTierEmotes
		client := emoteGroupDB{
			EmoteGroupsDao: emoteGroupsDao,
		}

		Convey("when the provided origin is not supported", func() {
			origin = mk.EmoteGroup_Globals

			Convey("we should error", func() {
				group, _, err := client.CreateEmoteGroup(ctx, mako.EmoteGroup{
					OwnerID:   mockOwnerId,
					GroupType: mako.EmoteGroupStatic,
					Origin:    mako.ToMakoGroupOrigin(mk.EmoteGroup_Globals),
				})

				So(err, ShouldNotBeNil)
				So(group.GroupID, ShouldBeEmpty)
			})
		})

		Convey("when EmoteGroupsDao.CreateEmoteGroup does not error", func() {
			mockEmoteGroup := mako.EmoteGroup{
				GroupID:   "123",
				GroupType: mako.EmoteGroupStatic,
			}

			emoteGroupsDao.On("CreateEmoteGroup", mock.Anything, mock.Anything).Return(mockEmoteGroup, false, nil)

			Convey("we should return the groupID", func() {
				group, _, err := client.CreateEmoteGroup(ctx, mako.EmoteGroup{
					OwnerID:   mockOwnerId,
					GroupType: mako.EmoteGroupStatic,
					Origin:    mako.ToMakoGroupOrigin(origin),
				})

				So(err, ShouldBeNil)
				So(group.GroupID, ShouldEqual, "123")
			})
		})

		Convey("when EmoteGroupsDao.CreateEmoteGroup does not error with animated group", func() {
			mockEmoteGroup := mako.EmoteGroup{
				GroupID:   "123",
				GroupType: mako.EmoteGroupAnimated,
			}

			emoteGroupsDao.On("CreateEmoteGroup", mock.Anything, mock.Anything).Return(mockEmoteGroup, false, nil)

			Convey("we should return the groupID", func() {
				group, _, err := client.CreateEmoteGroup(ctx, mako.EmoteGroup{
					OwnerID:   mockOwnerId,
					GroupType: mako.EmoteGroupAnimated,
					Origin:    mako.ToMakoGroupOrigin(origin),
				})

				So(err, ShouldBeNil)
				So(group.GroupID, ShouldEqual, "123")
			})
		})

		Convey("when EmoteGroupsDao.CreateEmoteGroup errors, but cannot be retried, we should return the error", func() {
			emoteGroupsDao.On("CreateEmoteGroup", mock.Anything, mock.Anything).Return(mako.EmoteGroup{}, false, errors.New("ERROR"))

			Convey("we should error", func() {
				group, _, err := client.CreateEmoteGroup(ctx, mako.EmoteGroup{
					OwnerID:   mockOwnerId,
					GroupType: mako.EmoteGroupAnimated,
					Origin:    mako.ToMakoGroupOrigin(origin),
				})

				So(err, ShouldNotBeNil)
				So(group.GroupID, ShouldEqual, "")
			})
		})

		Convey("when EmoteGroupsDao.CreateEmoteGroup fails a conditional write, but succeeds on retry, we should return the groupID", func() {
			emoteGroupsDao.On("CreateEmoteGroup", mock.Anything, mock.Anything).Return(mako.EmoteGroup{}, true, nil).Once()

			mockEmoteGroup := mako.EmoteGroup{
				GroupID: "456",
			}

			emoteGroupsDao.On("CreateEmoteGroup", mock.Anything, mock.Anything).Return(mockEmoteGroup, false, nil).Once()

			group, _, err := client.CreateEmoteGroup(ctx, mako.EmoteGroup{
				OwnerID:   mockOwnerId,
				GroupType: mako.EmoteGroupStatic,
				Origin:    mako.ToMakoGroupOrigin(origin),
			})

			So(err, ShouldBeNil)
			So(group.GroupID, ShouldEqual, "456")
			emoteGroupsDao.AssertNumberOfCalls(t, "CreateEmoteGroup", 2)
		})

		Convey("when a follower emote group is created twice for the same owner, we should return an error", func() {
			mockEmoteGroup := mako.EmoteGroup{
				GroupID: "1729",
				OwnerID: "24601",
				Origin:  mako.EmoteGroupOriginFollower,
			}
			emoteGroupsDao.On("CreateEmoteGroup", mock.Anything, mock.Anything).Return(mockEmoteGroup, false, nil).Once()
			emoteGroupsDao.On("GetFollowerEmoteGroup", mock.Anything, mockEmoteGroup.OwnerID).Return(nil, nil).Once()

			_, _, _ = client.CreateEmoteGroup(ctx, mako.EmoteGroup{
				OwnerID:   mockEmoteGroup.OwnerID,
				GroupType: mockEmoteGroup.GroupType,
				Origin:    mockEmoteGroup.Origin,
			})

			emoteGroupsDao.On("GetFollowerEmoteGroup", mock.Anything, mockEmoteGroup.OwnerID).Return(&mockEmoteGroup, nil).Once()

			// Try to create another group for the same owner

			_, conditionalFailure, err := client.CreateEmoteGroup(ctx, mako.EmoteGroup{
				OwnerID:   mockEmoteGroup.OwnerID,
				GroupType: mockEmoteGroup.GroupType,
				Origin:    mockEmoteGroup.Origin,
			})

			So(err, ShouldNotBeNil)
			So(conditionalFailure, ShouldBeFalse)
		})

		Convey("when EmoteGroupsDao.CreateEmoteGroup exceeds the max retry attempts, we should return the error", func() {
			emoteGroupsDao.On("CreateEmoteGroup", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(mako.EmoteGroup{}, true, nil)

			group, _, err := client.CreateEmoteGroup(ctx, mako.EmoteGroup{
				OwnerID:   mockOwnerId,
				GroupType: mako.EmoteGroupStatic,
				Origin:    mako.ToMakoGroupOrigin(origin),
			})

			So(err, ShouldNotBeNil)
			So(group.GroupID, ShouldEqual, "")
			emoteGroupsDao.AssertNumberOfCalls(t, "CreateEmoteGroup", 5)
		})
	})
}

func TestEmoticonGroupsV2Client_makeRandomBitsTierEmoteGroupID(t *testing.T) {
	Convey("Test makeRandomBitsTierEmoteGroupID util", t, func() {
		Convey("we should return a random string that can be parsed as an int", func() {
			id := makeRandomBitsTierEmoteGroupID()

			intID, err := strconv.Atoi(id)

			So(intID, ShouldBeGreaterThanOrEqualTo, BitsTierEmoteGroupIDOffset)
			So(intID, ShouldBeLessThanOrEqualTo, math.MaxInt32)
			So(err, ShouldBeNil)
		})
	})
}

func TestEmoticonGroupsV2Client_makeRandomSubscriptionGroupID(t *testing.T) {
	Convey("Test makeRandomSubscriptionGroupID util", t, func() {
		Convey("we should return a random string that can be parsed as an int", func() {
			id := makeRandomSubscriptionGroupID()

			intID, err := strconv.Atoi(id)

			So(intID, ShouldBeGreaterThanOrEqualTo, SubscriptionEmoteGroupIDOffset)
			So(intID, ShouldBeLessThan, BitsTierEmoteGroupIDOffset)
			So(err, ShouldBeNil)
		})
	})
}

func TestEmoticonGroupsV2Client_GetFollowerEmoteGroup(t *testing.T) {
	Convey("Testing GetFollowerEmoteGroup", t, func() {
		emoteGroupsDao := new(internal_mock.EmoteGroupDB)

		ctx := context.Background()
		channelID := "channel_id"
		groupID := "group_id"
		client := emoteGroupDB{
			EmoteGroupsDao: emoteGroupsDao,
		}

		Convey("error retrieving group", func() {
			emoteGroupsDao.On("GetFollowerEmoteGroup", mock.Anything, channelID).Return(nil, errors.New("ERROR"))

			group, err := client.GetFollowerEmoteGroup(ctx, channelID)

			So(err, ShouldNotBeNil)
			So(group, ShouldBeNil)
		})

		Convey("success", func() {
			emoteGroupsDao.On("GetFollowerEmoteGroup", mock.Anything, channelID).Return(&mako.EmoteGroup{GroupID: groupID}, nil)

			group, err := client.GetFollowerEmoteGroup(ctx, channelID)

			So(err, ShouldBeNil)
			So(group, ShouldNotBeNil)
			So(group.GroupID, ShouldEqual, groupID)
		})
	})
}
