package emoticongroups

import (
	"context"
	"fmt"
	"math"
	"math/rand"
	"time"

	mako "code.justin.tv/commerce/mako/internal"

	mk "code.justin.tv/commerce/mako/twirp"
	"github.com/pkg/errors"
	uuid "github.com/satori/go.uuid"
)

const (
	// As of 11/14/19, the largest EmoticonGroupID is 300_636_159, which includes an
	// offset of ~298MM when transitioning from siteDB. Therefore, only ~2.3MM emote groups
	// have been created to date. Offsetting by 200MM should give us plenty of time to get off the old
	// SQL table before producing collisions.
	// Everything above this number is reserved for bits tier emotes until Mobile clients stop using this offset
	// to determine if an emote is of type bits tier.
	// iOS: https://jira.twitch.com/browse/IOS-14807
	// Android: https://jira.twitch.com/browse/AND-11802
	BitsTierEmoteGroupIDOffset = 500000000

	// As of 10/02/20, the largest emote groupID is 302,401,566.
	// Since groupIDs greater than 500,000,000 are reserved for bits tier emotes (for now), we can only allow sub emotes
	// to generate groupIDs from this value here up until 499,999,999.
	// Once groupIDs of 500,000,000+ are no longer reserved for bits tier emotes (due to migrating them to UUIDs), we can
	// allow sub emotes to use that range of integers as well.
	// Ultimately, once sub emote groupIDs also start supporting UUIDs, we can remove this offset logic as well.
	SubscriptionEmoteGroupIDOffset = 310000000

	// We don't have the luxury of auto-incrementing IDs like SQL, so we will generate random 32-bit ints,
	// and conditionally put them to dynamo to ensure uniqueness. In the unlikely event there is a collision,
	// we can retry a few times to be client-friendly.
	maxCreationAttempts = 5
)

var emoteGroupIDCreationFunctionsByOrigin = map[string]func() string{
	mk.EmoteGroup_BitsBadgeTierEmotes.String(): makeRandomBitsTierEmoteGroupID,
	mk.EmoteGroup_Subscriptions.String():       makeRandomSubscriptionGroupID,
	mk.EmoteGroup_Follower.String():            makeRandomUUIDGroupID, // Because this is a new origin type, we can use UUIDs here
	mk.EmoteGroup_HypeTrain.String():           makeRandomUUIDGroupID,
}

type emoteGroupDB struct {
	EmoteGroupsDao mako.EmoteGroupDB
}

func NewEmoteGroupDB(emoteGroupsDao mako.EmoteGroupDB) mako.EmoteGroupDB {
	return &emoteGroupDB{
		EmoteGroupsDao: emoteGroupsDao,
	}
}

/**
short->medium term: To support mobile clients that depend on emoteGroupID being int-parseable, we will generate
	random 32-bit ints as IDs, ensuring uniqueness with the backing Dynamo table.
long term: As soon as all clients have been updated to handle string IDs, cut over all new emoteGroups to use UUIDs
*/
func (e *emoteGroupDB) CreateEmoteGroup(ctx context.Context, group mako.EmoteGroup) (emoteGroup mako.EmoteGroup, conditionalCheckFailed bool, err error) {
	makeEmoteGroupID, ok := emoteGroupIDCreationFunctionsByOrigin[group.Origin.String()]
	if !ok {
		return mako.EmoteGroup{}, false, errors.New("unsupported origin provided")
	}

	if group.Origin == mako.EmoteGroupOriginFollower {
		// enforce uniqueness by OwnerID.
		//
		// Note: This read - check - write approach is a race condition; the right approach
		// for uniqueness enforcement on a secondary field is to make a separate table that
		// uses that field as a primary key and do a double-write inside a transaction.
		//
		// We have decided that the race condition is okay for now because the access pattern
		// for creating an emote group is such that it is extremely unlikely to ever be
		// triggered.
		existingGroup, err := e.EmoteGroupsDao.GetFollowerEmoteGroup(ctx, group.OwnerID)
		if err != nil {
			return mako.EmoteGroup{}, false, err
		}
		if existingGroup != nil {
			return mako.EmoteGroup{}, false, errors.New("That owner already has a follower emote group")
		}
	}

	attempt := 0

	for attempt < maxCreationAttempts {
		candidateID := makeEmoteGroupID()

		emoteGroup, conditionalCheckFailed, err := e.EmoteGroupsDao.CreateEmoteGroup(ctx, mako.EmoteGroup{
			GroupID:   candidateID,
			OwnerID:   group.OwnerID,
			Origin:    group.Origin,
			GroupType: group.GroupType,
		})

		if err != nil {
			return mako.EmoteGroup{}, false, err
		}

		// Try to create the emote group again if the random int that we generated collides with an existing
		// entry in the table.
		if conditionalCheckFailed {
			attempt++
			continue
		}

		return emoteGroup, false, nil
	}

	// We return false for conditional check failed because this layer with its retry logic is the only layer of EmoteGroupDB
	// that cares about whether the conditional check failed. Other upward clients will simply ignore it or pass it up.
	return mako.EmoteGroup{}, false, errors.New("exceeded max retries creating emote group")
}

func (e *emoteGroupDB) GetEmoteGroups(ctx context.Context, ids ...string) ([]mako.EmoteGroup, error) {
	return e.EmoteGroupsDao.GetEmoteGroups(ctx, ids...)
}

func (e *emoteGroupDB) GetFollowerEmoteGroup(ctx context.Context, channelID string) (*mako.EmoteGroup, error) {
	group, err := e.EmoteGroupsDao.GetFollowerEmoteGroup(ctx, channelID)
	if err != nil || group == nil {
		return nil, err
	}
	return group, nil
}

func (e *emoteGroupDB) DeleteEmoteGroup(ctx context.Context, id string) error {
	err := e.EmoteGroupsDao.DeleteEmoteGroup(ctx, id)
	if err != nil {
		return err
	}
	return nil
}

// Generate a random group ID for bits tier emotes, required to be between 500,000,000 and MaxInt32
func makeRandomBitsTierEmoteGroupID() string {
	randSeed := rand.New(rand.NewSource(time.Now().UnixNano()))
	id := randSeed.Int31n(math.MaxInt32-BitsTierEmoteGroupIDOffset+1) + BitsTierEmoteGroupIDOffset

	return fmt.Sprint(id)
}

// Generate a random group ID for subscription emotes, required to be between 310,000,000 and 499,999,999
func makeRandomSubscriptionGroupID() string {
	randSeed := rand.New(rand.NewSource(time.Now().UnixNano()))
	id := randSeed.Int31n(BitsTierEmoteGroupIDOffset-SubscriptionEmoteGroupIDOffset) + SubscriptionEmoteGroupIDOffset

	return fmt.Sprint(id)
}

// Generate a random UUID emote group for types that are not restricted due to legacy reasons.
func makeRandomUUIDGroupID() string {
	return uuid.Must(uuid.NewV4()).String()
}
