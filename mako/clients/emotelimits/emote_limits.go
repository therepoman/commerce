package emotelimits

import (
	"context"

	"code.justin.tv/commerce/mako/clients/subscriptions"
	"code.justin.tv/commerce/mako/config"
)

const FollowerEmoteLimit = 5 // Hardcoded to 5 for now as per EXPR-315

type UserEmoteLimits struct {
	OwnedStaticEmoteLimit   int32
	OwnedAnimatedEmoteLimit int32
	OwnedFollowerEmoteLimit int32
}

type EmoteLimitsManager interface {
	GetUserEmoteLimits(ctx context.Context, userID string) (UserEmoteLimits, error)
}

type emoteLimitsManager struct {
	SubscriptionsClient subscriptions.ClientWrapper
	DynamicConfig       config.DynamicConfigClient
}

func NewEmoteLimitsManager(subscriptionsClient subscriptions.ClientWrapper, dynamicConfig config.DynamicConfigClient) EmoteLimitsManager {
	return &emoteLimitsManager{
		SubscriptionsClient: subscriptionsClient,
		DynamicConfig:       dynamicConfig,
	}
}

func (e *emoteLimitsManager) GetUserEmoteLimits(ctx context.Context, userID string) (UserEmoteLimits, error) {
	subscriptionEmoteLimits, err := e.SubscriptionsClient.GetChannelEmoteLimits(ctx, userID)
	if err != nil {
		return UserEmoteLimits{}, err
	}

	totalStaticSubEmoteLimit := int32(0)
	totalAnimatedSubEmoteLimit := int32(0)
	for _, limit := range subscriptionEmoteLimits.Limits {
		totalStaticSubEmoteLimit += limit.EmoteLimit
		totalAnimatedSubEmoteLimit += limit.AnimatedEmoteLimit
	}

	/*
			The current user emote limits are defined as follows:
			- The user may own [2 x total static sub emote slots + 10 rough estimate for BTER slots + 5 follower slots] static emotes
		      or [5 x total static sub emote slots] static emotes if the AnimatedEmotesForAffiliates feature has launched.
			- The user may own [2 x total animated sub emote slots] animated emotes or [5 x total animated sub emote slots]
		      if the AnimatedEmotesForAffiliates feature has launched.
	*/
	staticEmoteLimit := 2*totalStaticSubEmoteLimit + 15
	animatedEmoteLimit := 2 * totalAnimatedSubEmoteLimit
	if e.DynamicConfig != nil && e.DynamicConfig.LibraryLimitIncreaseLaunched(userID) {
		staticEmoteLimit = 5 * totalStaticSubEmoteLimit
		animatedEmoteLimit = 5 * totalAnimatedSubEmoteLimit
	}

	return UserEmoteLimits{
		OwnedStaticEmoteLimit:   staticEmoteLimit,
		OwnedAnimatedEmoteLimit: animatedEmoteLimit,
		OwnedFollowerEmoteLimit: FollowerEmoteLimit,
	}, nil
}
