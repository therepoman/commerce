package emotelimits

import (
	"context"
	"errors"
	"testing"

	subscriptions_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/subscriptions"
	config_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/config"
	substwirp "code.justin.tv/revenue/subscriptions/twirp"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestEmoteLimitsManager(t *testing.T) {
	Convey("Test Emote Limits Manager", t, func() {
		mockSubsClient := new(subscriptions_mock.ClientWrapper)
		mockDynamicConfig := new(config_mock.DynamicConfigClient)
		emoteLimitsManager := emoteLimitsManager{
			SubscriptionsClient: mockSubsClient,
			DynamicConfig:       mockDynamicConfig,
		}

		ctx := context.Background()
		userID := "someUser"

		Convey("Test GetUserEmoteLimits", func() {
			Convey("With error from Subscriptions client", func() {
				mockSubsClient.On("GetChannelEmoteLimits", mock.Anything, userID).Return(nil, errors.New("dummy error"))

				res, err := emoteLimitsManager.GetUserEmoteLimits(ctx, userID)

				So(res, ShouldResemble, UserEmoteLimits{})
				So(err, ShouldNotBeNil)
			})

			Convey("With success from Subscriptions client and limit increase has not launched", func() {
				mockSubsClient.On("GetChannelEmoteLimits", mock.Anything, userID).Return(&substwirp.GetChannelEmoteLimitsResponse{
					ChannelId: userID,
					Limits: []*substwirp.ProductEmoteLimit{
						{
							EmoteLimit:         10,
							AnimatedEmoteLimit: 2,
						},
						{
							EmoteLimit:         5,
							AnimatedEmoteLimit: 0,
						},
						{
							EmoteLimit:         5,
							AnimatedEmoteLimit: 0,
						},
					},
				}, nil)
				mockDynamicConfig.On("LibraryLimitIncreaseLaunched", userID).Return(false)

				res, err := emoteLimitsManager.GetUserEmoteLimits(ctx, userID)

				So(res, ShouldResemble, UserEmoteLimits{
					OwnedStaticEmoteLimit:   55,
					OwnedAnimatedEmoteLimit: 4,
					OwnedFollowerEmoteLimit: 5,
				})
				So(err, ShouldBeNil)
			})

			Convey("With success from Subscriptions client and limit increase has launched", func() {
				mockSubsClient.On("GetChannelEmoteLimits", mock.Anything, userID).Return(&substwirp.GetChannelEmoteLimitsResponse{
					ChannelId: userID,
					Limits: []*substwirp.ProductEmoteLimit{
						{
							EmoteLimit:         10,
							AnimatedEmoteLimit: 2,
						},
						{
							EmoteLimit:         5,
							AnimatedEmoteLimit: 0,
						},
						{
							EmoteLimit:         5,
							AnimatedEmoteLimit: 0,
						},
					},
				}, nil)
				mockDynamicConfig.On("LibraryLimitIncreaseLaunched", userID).Return(true)

				res, err := emoteLimitsManager.GetUserEmoteLimits(ctx, userID)

				So(res, ShouldResemble, UserEmoteLimits{
					OwnedStaticEmoteLimit:   100,
					OwnedAnimatedEmoteLimit: 10,
					OwnedFollowerEmoteLimit: 5,
				})
				So(err, ShouldBeNil)
			})
		})
	})
}
