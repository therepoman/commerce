package emotesinference

import (
	"context"
	"errors"
	"testing"

	emotesinference_client_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/safety/inference/rpc/emotes"
	emotesinference "code.justin.tv/safety/inference/rpc/emotes"
	. "github.com/smartystreets/goconvey/convey"
)

func TestEmoteInferenceClientWrapper(t *testing.T) {
	Convey("Test Emotes Inference Client Wrapper", t, func() {
		mockEmotesInferenceClient := new(emotesinference_client_mock.Emotes)
		clientWrapper := clientWrapper{
			EmotesInferenceClient: mockEmotesInferenceClient,
		}
		ctx := context.Background()
		image4xID := "1234_4x_6683497f-4434-42ec-8ff5-1b73003ad3fe"
		emoteCode := "someCode"
		userID := "someUser"

		Convey("Test ClassifyEmote", func() {
			Convey("With error from Emotes Inference client for the max number of retries", func() {
				mockEmotesInferenceClient.On("ClassifyEmote", ctx, &emotesinference.ClassifyEmoteRequest{
					Image4XId: image4xID,
					EmoteCode: emoteCode,
					UserId:    userID,
				}).Return(nil, errors.New("dummy error"))

				resp, err := clientWrapper.ClassifyEmote(ctx, image4xID, emoteCode, userID)

				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})

			Convey("With immediate success from Emotes Inference client", func() {
				testResp := &emotesinference.ClassifyEmoteResponse{
					Image4XId:       image4xID,
					EmoteCode:       emoteCode,
					RecommendReview: true,
				}
				mockEmotesInferenceClient.On("ClassifyEmote", ctx, &emotesinference.ClassifyEmoteRequest{
					Image4XId: image4xID,
					EmoteCode: emoteCode,
					UserId:    userID,
				}).Return(testResp, nil)

				resp, err := clientWrapper.ClassifyEmote(ctx, image4xID, emoteCode, userID)

				So(err, ShouldBeNil)
				So(resp, ShouldResemble, testResp)
			})

			Convey("With success from Emotes Inference client after max number of retries", func() {
				testResp := &emotesinference.ClassifyEmoteResponse{
					Image4XId:       image4xID,
					EmoteCode:       emoteCode,
					RecommendReview: true,
				}

				// for the first N-many calls, error, but on the last retry, succeed
				mockEmotesInferenceClient.On("ClassifyEmote", ctx, &emotesinference.ClassifyEmoteRequest{
					Image4XId: image4xID,
					EmoteCode: emoteCode,
					UserId:    userID,
				}).Times(classifyEmoteMaxRetries).Return(nil, errors.New("dummy error"))
				mockEmotesInferenceClient.On("ClassifyEmote", ctx, &emotesinference.ClassifyEmoteRequest{
					Image4XId: image4xID,
					EmoteCode: emoteCode,
					UserId:    userID,
				}).Return(testResp, nil)

				resp, err := clientWrapper.ClassifyEmote(ctx, image4xID, emoteCode, userID)

				So(err, ShouldBeNil)
				So(resp, ShouldResemble, testResp)
			})
		})
	})
}
