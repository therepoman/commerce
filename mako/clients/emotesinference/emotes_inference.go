package emotesinference

import (
	"context"
	"time"

	"code.justin.tv/commerce/mako/config"
	"code.justin.tv/foundation/twitchclient"
	emotesinference "code.justin.tv/safety/inference/rpc/emotes"
	"github.com/afex/hystrix-go/hystrix"
	"github.com/cenkalti/backoff"
)

const (
	HystrixNameEmotesInferenceEmoteClassify = "emotesinference.emote.classify"
	classifyEmoteMaxRetries                 = 1
)

type ClientWrapper interface {
	ClassifyEmote(ctx context.Context, image4xID string, emoteCode string, userID string) (*emotesinference.ClassifyEmoteResponse, error)
}

type clientWrapper struct {
	EmotesInferenceClient emotesinference.Emotes
}

func NewClient(cfg *config.Configuration) ClientWrapper {
	return &clientWrapper{
		EmotesInferenceClient: emotesinference.NewEmotesProtobufClient(cfg.InferenceServiceEndpoint, twitchclient.NewHTTPClient(twitchclient.ClientConf{
			Host: cfg.InferenceServiceEndpoint,
			Transport: twitchclient.TransportConf{
				MaxIdleConnsPerHost: 10,
			},
		})),
	}
}

func (c *clientWrapper) ClassifyEmote(ctx context.Context, image4xID string, emoteCode string, userID string) (*emotesinference.ClassifyEmoteResponse, error) {
	var resp *emotesinference.ClassifyEmoteResponse

	f := func() error {
		return hystrix.DoC(ctx, HystrixNameEmotesInferenceEmoteClassify, func(ctx context.Context) error {
			var err error
			resp, err = c.EmotesInferenceClient.ClassifyEmote(ctx, &emotesinference.ClassifyEmoteRequest{
				Image4XId: image4xID,
				EmoteCode: emoteCode,
				UserId:    userID,
			})
			return err
		}, nil)
	}

	b := backoff.WithMaxRetries(backoff.WithContext(newBackoff(), ctx), classifyEmoteMaxRetries)
	err := backoff.Retry(f, b)

	return resp, err
}

func newBackoff() backoff.BackOff {
	exponential := backoff.NewExponentialBackOff()
	exponential.InitialInterval = 10 * time.Millisecond
	exponential.MaxElapsedTime = 1100 * time.Millisecond
	exponential.Multiplier = 2
	return exponential
}
