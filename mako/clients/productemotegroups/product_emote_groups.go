package productemotegroups

import (
	"context"

	"code.justin.tv/commerce/mako/clients/payday"
	"code.justin.tv/commerce/mako/clients/subscriptions"
	mako "code.justin.tv/commerce/mako/internal"
	mkd "code.justin.tv/commerce/mako/twirp/dashboard"
	"github.com/pkg/errors"
	"golang.org/x/sync/errgroup"
)

type productEmoteGroups struct {
	PaydayClient        payday.ClientWrapper
	SubscriptionsClient subscriptions.ClientWrapper
	EmoteGroupDB        mako.EmoteGroupDB
}

type ProductEmoteGroups interface {
	GetProductEmoteGroups(ctx context.Context, ownerID string) ([]*mkd.EmoteGroup, error)
}

func NewProductEmoteGroups(paydayCilent payday.ClientWrapper, subsClient subscriptions.ClientWrapper, emoteGroupDB mako.EmoteGroupDB) ProductEmoteGroups {
	return &productEmoteGroups{
		PaydayClient:        paydayCilent,
		SubscriptionsClient: subsClient,
		EmoteGroupDB:        emoteGroupDB,
	}
}

func (e *productEmoteGroups) GetProductEmoteGroups(ctx context.Context, ownerID string) ([]*mkd.EmoteGroup, error) {
	eg, egCtx := errgroup.WithContext(ctx)
	var followerEmoteGroup *mkd.EmoteGroup
	var subscriptionEmoteGroups []*mkd.EmoteGroup
	var bitsTierEmoteGroups []*mkd.EmoteGroup

	eg.Go(func() error {
		var err error
		followerEmoteGroup, err = e.getFollowerEmoteGroup(egCtx, ownerID)
		return err
	})

	eg.Go(func() error {
		var err error
		subscriptionEmoteGroups, err = e.getSubscriptionEmoteGroups(egCtx, ownerID)
		return err
	})

	eg.Go(func() error {
		var err error
		bitsTierEmoteGroups, err = e.getBitsTierEmoteGroups(egCtx, ownerID)
		return err
	})

	// NOTE (jelhay): We could consider allowing partial success and returning what we have when one of our downstreams fails,
	// but this could make the user experience weirder than we can accept.
	if err := eg.Wait(); err != nil {
		return nil, err
	}

	var emoteGroups []*mkd.EmoteGroup
	if followerEmoteGroup != nil {
		emoteGroups = append(emoteGroups, followerEmoteGroup)
	}
	emoteGroups = append(emoteGroups, subscriptionEmoteGroups...)
	emoteGroups = append(emoteGroups, bitsTierEmoteGroups...)
	return emoteGroups, nil
}

func (e *productEmoteGroups) getFollowerEmoteGroup(ctx context.Context, ownerID string) (*mkd.EmoteGroup, error) {
	followerEmoteGroup, err := e.EmoteGroupDB.GetFollowerEmoteGroup(ctx, ownerID)
	if err != nil {
		msg := "error getting follower emote group"
		return nil, errors.Wrap(err, msg)
	} else if followerEmoteGroup != nil {
		return &mkd.EmoteGroup{
			Id:          followerEmoteGroup.GroupID,
			GroupType:   followerEmoteGroup.GroupType.ToTwirp(),
			ProductType: mkd.EmoteGroupProductType_FollowerTier,
		}, nil
	}

	return nil, nil
}

func (e *productEmoteGroups) getSubscriptionEmoteGroups(ctx context.Context, ownerID string) ([]*mkd.EmoteGroup, error) {
	channelProductsResp, err := e.SubscriptionsClient.GetChannelProducts(ctx, ownerID)
	if err != nil {
		msg := "error getting channel subscription products"
		return nil, errors.Wrap(err, msg)
	}

	var emoteGroupIDs []string
	emoteGroupsByIDs := make(map[string]*mkd.EmoteGroup)

	// Get tier info for each emote group
	for _, product := range channelProductsResp.GetProducts() {
		// Currently, Tier 1 products can contain multiple emote sets (static, animated), so EmoticonSetIds should be used.
		// Higher tiers contain the emote sets of lower tiers in EmoticonSetIds, so EmoticonSetId should be used to get that
		// tier's specific emote set.
		if product.GetTier() == "1000" {
			emoteGroupIDs = append(emoteGroupIDs, product.GetEmoticonSetIds()...)
			for _, groupID := range product.GetEmoticonSetIds() {
				emoteGroupsByIDs[groupID] = &mkd.EmoteGroup{
					Id:          groupID,
					ProductType: mkd.EmoteGroupProductType_SubscriptionTier1,
				}
			}
		} else {
			emoteGroupIDs = append(emoteGroupIDs, product.GetEmoticonSetId())
			emoteGroupsByIDs[product.GetEmoticonSetId()] = &mkd.EmoteGroup{
				Id:          product.GetEmoticonSetId(),
				ProductType: subscriptionTierToEmoteGroupProductType(product.GetTier()),
			}
		}
	}

	if len(emoteGroupIDs) > 0 {
		// Get asset type of each group
		subscriptionEmoteGroups, err := e.EmoteGroupDB.GetEmoteGroups(ctx, emoteGroupIDs...)
		if err != nil {
			msg := "error getting subscription emote groups"
			return nil, errors.Wrap(err, msg)
		}

		// Not all emote groups are currently stored in Mako's emote groups DB, so this is why we still need to rely
		// on the emoteGroupsByIDs map as the complete set of emote groups
		for _, subscriptionEmoteGroup := range subscriptionEmoteGroups {
			group, ok := emoteGroupsByIDs[subscriptionEmoteGroup.GroupID]
			if ok {
				group.GroupType = subscriptionEmoteGroup.GroupType.ToTwirp()
			}
		}
	}

	emoteGroups := make([]*mkd.EmoteGroup, 0, len(emoteGroupIDs))
	for _, groupID := range emoteGroupIDs {
		// We're not directly iterating on the map in order to return a consistent order of the list every run
		emoteGroups = append(emoteGroups, emoteGroupsByIDs[groupID])
	}

	return emoteGroups, nil
}

func (e *productEmoteGroups) getBitsTierEmoteGroups(ctx context.Context, ownerID string) ([]*mkd.EmoteGroup, error) {
	bitsTierEmoteGroupsResp, err := e.PaydayClient.GetBitsTierEmoteGroups(ctx, ownerID)
	if err != nil {
		msg := "error getting bits tier emote groups"
		return nil, errors.Wrap(err, msg)
	}

	emoteGroups := make([]*mkd.EmoteGroup, 0, len(bitsTierEmoteGroupsResp.GetEmoteGroups()))
	for _, group := range bitsTierEmoteGroupsResp.GetEmoteGroups() {
		emoteGroups = append(emoteGroups, &mkd.EmoteGroup{
			Id:          group.GetEmoteGroupId(),
			GroupType:   mkd.EmoteGroupType_static_group,
			ProductType: bitsTierThresholdToEmoteGroupProductType(group.GetBitsTierThreshold()),
		})
	}

	return emoteGroups, nil
}

func subscriptionTierToEmoteGroupProductType(subscriptionTier string) mkd.EmoteGroupProductType {
	switch subscriptionTier {
	case "1000":
		return mkd.EmoteGroupProductType_SubscriptionTier1
	case "2000":
		return mkd.EmoteGroupProductType_SubscriptionTier2
	case "3000":
		return mkd.EmoteGroupProductType_SubscriptionTier3
	default:
		return mkd.EmoteGroupProductType_UnknownProduct
	}
}

func bitsTierThresholdToEmoteGroupProductType(bitsTierThreshold int64) mkd.EmoteGroupProductType {
	switch bitsTierThreshold {
	case 1:
		return mkd.EmoteGroupProductType_BitsTier1
	case 100:
		return mkd.EmoteGroupProductType_BitsTier100
	case 1000:
		return mkd.EmoteGroupProductType_BitsTier1000
	case 5000:
		return mkd.EmoteGroupProductType_BitsTier5000
	case 10000:
		return mkd.EmoteGroupProductType_BitsTier10000
	case 25000:
		return mkd.EmoteGroupProductType_BitsTier25000
	case 50000:
		return mkd.EmoteGroupProductType_BitsTier50000
	case 75000:
		return mkd.EmoteGroupProductType_BitsTier75000
	case 100000:
		return mkd.EmoteGroupProductType_BitsTier100000
	case 200000:
		return mkd.EmoteGroupProductType_BitsTier200000
	case 300000:
		return mkd.EmoteGroupProductType_BitsTier300000
	case 400000:
		return mkd.EmoteGroupProductType_BitsTier400000
	case 500000:
		return mkd.EmoteGroupProductType_BitsTier500000
	case 600000:
		return mkd.EmoteGroupProductType_BitsTier600000
	case 700000:
		return mkd.EmoteGroupProductType_BitsTier700000
	case 800000:
		return mkd.EmoteGroupProductType_BitsTier800000
	case 900000:
		return mkd.EmoteGroupProductType_BitsTier900000
	case 1000000:
		return mkd.EmoteGroupProductType_BitsTier1000000
	case 1250000:
		return mkd.EmoteGroupProductType_BitsTier1250000
	case 1500000:
		return mkd.EmoteGroupProductType_BitsTier1500000
	case 1750000:
		return mkd.EmoteGroupProductType_BitsTier1750000
	case 2000000:
		return mkd.EmoteGroupProductType_BitsTier2000000
	case 2500000:
		return mkd.EmoteGroupProductType_BitsTier2500000
	case 3000000:
		return mkd.EmoteGroupProductType_BitsTier3000000
	case 3500000:
		return mkd.EmoteGroupProductType_BitsTier3500000
	case 4000000:
		return mkd.EmoteGroupProductType_BitsTier4000000
	case 4500000:
		return mkd.EmoteGroupProductType_BitsTier4500000
	case 5000000:
		return mkd.EmoteGroupProductType_BitsTier5000000
	default:
		return mkd.EmoteGroupProductType_UnknownProduct
	}
}
