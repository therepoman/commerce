package productemotegroups

import (
	"context"
	"testing"

	mako "code.justin.tv/commerce/mako/internal"
	payday_client_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/payday"
	subscriptions_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/subscriptions"
	"code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/internal_mock"
	mkd "code.justin.tv/commerce/mako/twirp/dashboard"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	substwirp "code.justin.tv/revenue/subscriptions/twirp"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestProductEmoteGroups(t *testing.T) {
	Convey("Test GetProductEmoteGroups client", t, func() {
		mockSubscriptionsClient := new(subscriptions_mock.ClientWrapper)
		mockPaydayClient := new(payday_client_mock.ClientWrapper)
		mockEmoteGroupDB := new(internal_mock.EmoteGroupDB)

		client := NewProductEmoteGroups(mockPaydayClient, mockSubscriptionsClient, mockEmoteGroupDB)

		ctx := context.Background()
		userID := "someUser"
		followerGroupID := "follower-group-id"
		subsTier1StaticGroupID := "subs-tier1-static-group-id"
		subsTier1AnimatedGroupID := "subs-tier1-animated-group-id"
		subsTier2GroupID := "subs-tier2-group-id"
		bitsTier1KGroupID := "bits-tier1k-group-id"
		bitsTier5KGroupID := "bits-tier5k-group-id"

		Convey("with error from GetFollowerEmoteGroup", func() {
			mockEmoteGroupDB.On("GetFollowerEmoteGroup", mock.Anything, userID).Return(nil, errors.New("dummy error"))
			mockSubscriptionsClient.On("GetChannelProducts", mock.Anything, userID).Return(&substwirp.GetChannelProductsResponse{}, nil)
			mockEmoteGroupDB.On("GetEmoteGroups", mock.Anything, mock.Anything).Return(nil, nil)
			mockPaydayClient.On("GetBitsTierEmoteGroups", mock.Anything, userID).Return(&paydayrpc.GetBitsTierEmoteGroupsResp{}, nil)

			resp, err := client.GetProductEmoteGroups(ctx, userID)
			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("with error from GetChannelProducts", func() {
			mockEmoteGroupDB.On("GetFollowerEmoteGroup", mock.Anything, userID).Return(nil, nil)
			mockSubscriptionsClient.On("GetChannelProducts", mock.Anything, userID).Return(nil, errors.New("dummy error"))
			mockEmoteGroupDB.On("GetEmoteGroups", mock.Anything, mock.Anything).Return(nil, nil)
			mockPaydayClient.On("GetBitsTierEmoteGroups", mock.Anything, userID).Return(&paydayrpc.GetBitsTierEmoteGroupsResp{}, nil)

			resp, err := client.GetProductEmoteGroups(ctx, userID)
			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("with error from GetEmoteGroups", func() {
			mockEmoteGroupDB.On("GetFollowerEmoteGroup", mock.Anything, userID).Return(nil, nil)
			mockSubscriptionsClient.On("GetChannelProducts", mock.Anything, userID).Return(&substwirp.GetChannelProductsResponse{
				Products: []*substwirp.Product{
					{
						Tier:          "2000",
						EmoticonSetId: "1234",
					},
				},
			}, nil)
			mockEmoteGroupDB.On("GetEmoteGroups", mock.Anything, mock.Anything).Return(nil, errors.New("dummy error"))
			mockPaydayClient.On("GetBitsTierEmoteGroups", mock.Anything, userID).Return(&paydayrpc.GetBitsTierEmoteGroupsResp{}, nil)

			resp, err := client.GetProductEmoteGroups(ctx, userID)
			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("with error from GetBitsTierEmoteGroups", func() {
			mockEmoteGroupDB.On("GetFollowerEmoteGroup", mock.Anything, userID).Return(nil, nil)
			mockSubscriptionsClient.On("GetChannelProducts", mock.Anything, userID).Return(&substwirp.GetChannelProductsResponse{}, nil)
			mockEmoteGroupDB.On("GetEmoteGroups", mock.Anything, mock.Anything).Return(nil, nil)
			mockPaydayClient.On("GetBitsTierEmoteGroups", mock.Anything, userID).Return(nil, errors.New("dummy error"))

			resp, err := client.GetProductEmoteGroups(ctx, userID)
			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("with emote groups from all sources", func() {
			mockEmoteGroupDB.On("GetFollowerEmoteGroup", mock.Anything, userID).Return(&mako.EmoteGroup{
				GroupID:   followerGroupID,
				Origin:    mako.EmoteGroupOriginFollower,
				GroupType: mako.EmoteGroupStatic,
			}, nil)
			mockSubscriptionsClient.On("GetChannelProducts", mock.Anything, userID).Return(&substwirp.GetChannelProductsResponse{
				Products: []*substwirp.Product{
					{
						Tier:           "1000",
						EmoticonSetIds: []string{subsTier1StaticGroupID, subsTier1AnimatedGroupID},
					},
					{
						Tier:          "2000",
						EmoticonSetId: subsTier2GroupID,
					},
				},
			}, nil)
			mockEmoteGroupDB.On("GetEmoteGroups", mock.Anything, subsTier1StaticGroupID, subsTier1AnimatedGroupID, subsTier2GroupID).Return([]mako.EmoteGroup{
				{
					GroupID:   subsTier1AnimatedGroupID,
					GroupType: mako.EmoteGroupAnimated,
				},
			}, nil)
			mockPaydayClient.On("GetBitsTierEmoteGroups", mock.Anything, userID).Return(&paydayrpc.GetBitsTierEmoteGroupsResp{
				EmoteGroups: []*paydayrpc.BitsTierEmoteGroup{
					{
						BitsTierThreshold: 1000,
						EmoteGroupId:      bitsTier1KGroupID,
					},
					{
						BitsTierThreshold: 5000,
						EmoteGroupId:      bitsTier5KGroupID,
					},
				},
			}, nil)

			resp, err := client.GetProductEmoteGroups(ctx, userID)
			So(err, ShouldBeNil)
			So(resp, ShouldResemble, []*mkd.EmoteGroup{
				{
					Id:          followerGroupID,
					GroupType:   mkd.EmoteGroupType_static_group,
					ProductType: mkd.EmoteGroupProductType_FollowerTier,
				},
				{
					Id:          subsTier1StaticGroupID,
					GroupType:   mkd.EmoteGroupType_static_group,
					ProductType: mkd.EmoteGroupProductType_SubscriptionTier1,
				},
				{
					Id:          subsTier1AnimatedGroupID,
					GroupType:   mkd.EmoteGroupType_animated_group,
					ProductType: mkd.EmoteGroupProductType_SubscriptionTier1,
				},
				{
					Id:          subsTier2GroupID,
					GroupType:   mkd.EmoteGroupType_static_group,
					ProductType: mkd.EmoteGroupProductType_SubscriptionTier2,
				},
				{
					Id:          bitsTier1KGroupID,
					GroupType:   mkd.EmoteGroupType_static_group,
					ProductType: mkd.EmoteGroupProductType_BitsTier1000,
				},
				{
					Id:          bitsTier5KGroupID,
					GroupType:   mkd.EmoteGroupType_static_group,
					ProductType: mkd.EmoteGroupProductType_BitsTier5000,
				},
			})
		})
	})
}
