package mako_test

import (
	"context"
	"testing"

	mako "code.justin.tv/commerce/mako/internal"
	"code.justin.tv/commerce/mako/localdb"
	"code.justin.tv/commerce/mako/tests"
	"github.com/cactus/go-statsd-client/statsd"
)

// TestLocalEntitlementDBWithStats runs the test suite wrapping a local EntitlementDB with a stats decorator to
// ensure that no functionality has been lost/altered.
func TestLocalEntitlementDBWithStats(t *testing.T) {
	tests.TestEntitlementDB(t, makeEntitlementDBWithStats(localdb.MakeEntitlementDB))
}

func makeEntitlementDBWithStats(makeBase tests.MakeEntitlementDB) tests.MakeEntitlementDB {
	return func(ctx context.Context, entitlements ...mako.Entitlement) (mako.EntitlementDB, func(), error) {
		base, teardown, err := makeBase(ctx, entitlements...)
		if err != nil {
			return nil, nil, err
		}

		noop, _ := statsd.NewNoop()
		return mako.NewEntitlementDBWithStats(base, noop, ""), teardown, nil
	}
}
