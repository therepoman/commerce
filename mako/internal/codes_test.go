package mako_test

import (
	"sort"
	"testing"

	mako "code.justin.tv/commerce/mako/internal"
	"code.justin.tv/commerce/mako/localdb"
	"code.justin.tv/commerce/mako/tests"
	"github.com/stretchr/testify/assert"
)

func TestLocalGetEmotesByCodes(t *testing.T) {
	tests.TestEmoteCodes(t, tests.EmoteCodesConfig{
		MakeEmoteDB:         localdb.MakeEmoteDB,
		MakeEntitlementDB:   localdb.MakeEntitlementDB,
		MakeEmoteModifierDB: localdb.MakeEmoteModifierDB,
	})
}

func TestNormalizeEmoteCodes(t *testing.T) {
	tests := []struct {
		scenario          string
		codes             []string
		expectedCodes     []string
		expectedModifiers map[string][]string
	}{
		{
			scenario:          "code too long",
			codes:             []string{"longprefix123456789012345678901828303"},
			expectedCodes:     []string{},
			expectedModifiers: map[string][]string{},
		},
		{
			scenario:      "long code with modifier but not too long",
			codes:         []string{"longprefix12345689012345678901_AB"},
			expectedCodes: []string{"longprefix12345689012345678901"},
			expectedModifiers: map[string][]string{
				"longprefix12345689012345678901": {"AB"},
			},
		},
		{
			scenario:      "expect 1 code",
			codes:         []string{"Foobar"},
			expectedCodes: []string{"Foobar"},
			expectedModifiers: map[string][]string{
				"Foobar": {""},
			},
		},

		{
			scenario:      "expect 1 modifier",
			codes:         []string{"Foobar_TK"},
			expectedCodes: []string{"Foobar"},
			expectedModifiers: map[string][]string{
				"Foobar": {"TK"},
			},
		},

		{
			scenario:      "expect 1 modifier + 1 normal code",
			codes:         []string{"Foobar_TK", "Foobar"},
			expectedCodes: []string{"Foobar"},
			expectedModifiers: map[string][]string{
				"Foobar": {"", "TK"},
			},
		},

		{
			scenario:      "expect 2 modifier + 1 normal code",
			codes:         []string{"Foobar_TK", "Foobar", "Foobar_SG"},
			expectedCodes: []string{"Foobar"},
			expectedModifiers: map[string][]string{
				"Foobar": {"", "SG", "TK"},
			},
		},

		{
			scenario:      "expect reverse order of 1 modifier + 1 normal code",
			codes:         []string{"Hello", "Hello_AG"},
			expectedCodes: []string{"Hello"},
			expectedModifiers: map[string][]string{
				"Hello": {"", "AG"},
			},
		},

		{
			scenario:      "expect 3 normal codes",
			codes:         []string{"Hello", "Gas", "Woop"},
			expectedCodes: []string{"Hello", "Gas", "Woop"},
			expectedModifiers: map[string][]string{
				"Hello": {""},
				"Gas":   {""},
				"Woop":  {""},
			},
		},

		{
			scenario:      "expect 3 normal codes + 1 modifier",
			codes:         []string{"Hello", "Gas", "Woop", "Efe_BW"},
			expectedCodes: []string{"Hello", "Gas", "Woop", "Efe"},
			expectedModifiers: map[string][]string{
				"Hello": {""},
				"Gas":   {""},
				"Woop":  {""},
				"Efe":   {"BW"},
			},
		},

		{
			scenario:          "expect 0 non-english codes",
			codes:             []string{"منور", "CRÉATEUR", "↑"},
			expectedCodes:     []string{},
			expectedModifiers: map[string][]string{},
		},

		{
			scenario:      "remove duplicate codes",
			codes:         []string{"<3", "<3", "<3", "<3", "nekono3Love", "nekono3Love", "nekono3Love", "nekono3Love"},
			expectedCodes: []string{"<3", "nekono3Love"},
			expectedModifiers: map[string][]string{
				"<3":          {""},
				"nekono3Love": {""},
			},
		},

		{
			scenario:          "/ is invalid code",
			codes:             []string{"Kappa_/123"},
			expectedCodes:     []string{},
			expectedModifiers: map[string][]string{},
		},

		{
			scenario:          ": is invalid code",
			codes:             []string{":Hello", "Foo:", "F:H", "J_:/"},
			expectedCodes:     []string{},
			expectedModifiers: map[string][]string{},
		},
	}

	for _, test := range tests {
		test := test

		t.Run(test.scenario, func(t *testing.T) {
			t.Parallel()

			codes, modifiers := mako.NormalizeEmoteCodes(test.codes...)

			// assert.True(t, reflect.DeepEqual(test.expectedCodes, codes))
			// assert.True(t, reflect.DeepEqual(test.expectedModifiers, modifiers))
			assert.Equal(t, test.expectedCodes, codes)

			expected := make(map[string][]string)

			for key, modifiers := range modifiers {
				for modifier := range modifiers {
					expected[key] = append(expected[key], modifier)
				}

				sort.Strings(expected[key])
			}

			assert.Equal(t, test.expectedModifiers, expected)
		})
	}
}
