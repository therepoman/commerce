package mako

import (
	"sync"

	"github.com/segmentio/ksuid"
)

type MatchType int

const (
	RequiresEmoteGroupEntitlement MatchType = iota
	RequiresMultipleEmoteGroupEntitlements
	RequiresEmoteEntitlement
	AlwaysEntitled
)

func (m MatchType) String() string {
	switch m {
	case RequiresEmoteGroupEntitlement:
		return "RequiresEmoteGroupEntitlement"
	case RequiresMultipleEmoteGroupEntitlements:
		return "RequiresMultipleEmoteGroupEntitlements"
	case RequiresEmoteEntitlement:
		return "RequiresEmoteEntitlement"
	case AlwaysEntitled:
		return "AlwaysEntitled"
	}

	return "unknown"
}

const (
	debugBuilder = false
)

// match is used for rules to return information about how to match an emote to an entitlement. Returning a nil `match` means nothing was matched.
type match struct {
	// emote usually the same emote that was given as input will be returned. In some cases emotes will be modified
	// and returned (such as modified emotes).
	emote Emote
	// entitlementIDs is a list of entitlements that need to check if the user is entitled to via the `EntitlementDB` interface.
	entitlementIDs []string
	// matchType determines if the emote can be entitled without checking the `EntitlementDB`, or if the entitlement
	// needs to be a certain type (see `MatchType`).
	matchType MatchType
}

// emoteMatcher is used for all the rules to determine how an emote should be entitled. Every emote and modifier
// found from the codes given to `GetEmotesByCodes` will execute each matcher.
type emoteMatcher func(emote Emote, modifier string) *match

// emoteFilter is used to quickly decide if a rule supports an emote or not.
type emoteFilter func(emote Emote, modifier string) bool

// emoteBuilder is an abstraction that allows each tenant/use case of emotes to be isolated from each other and constructed
// at a higher-level via a set of matching rules. Changes in one builder's matching rules should never effected another builder's
// behaviour.
type emoteBuilder struct {
	match emoteMatcher

	// Optional builder method that is used to further decide if an entitlement + emote matches the rules.
	canEntitle func(Emote, Entitlement) bool

	filters []emoteFilter

	/* emotes are a collection of all matched emotes, by type, so that they can be linked
	* to entitlements later.
	*
	* If a rule returns an entitlement id of `123` and match type of `RequiresGroupEntitlement` the map
	* will look like:
	*
	*   {
	*      RequiresEmoteGroupEntitlement: {
	*        123: []Emote{...}
	*      }
	*   }
	*
	* Then when an entitlement is read from `EntitlementDB` that has an id of `123` and `EmoteGroupEntitlement`
	* comes back it can be linked to the matching rule directly.
	 */
	emotes map[MatchType]map[string][]Emote

	/* Whether or not this builder should be run in parallel with the other builders. Generally only used for T2/T3 where
	* we potentially make a redis/DB call per request
	 */
	isParallel bool

	// A lock mechanism to make sure we don't run into race conditions on writing to `match`, `emotes`, or other internal data
	mu sync.Mutex

	// The name of this builder
	name string

	// A uuid to help debugging, gives a unique id per builder per request
	requestID ksuid.KSUID
}

// newEmoteBuilder returns an empty builder with a default entitlement matcher (returns `true` for any matched entitlements).
func newEmoteBuilder() *emoteBuilder {
	return &emoteBuilder{
		emotes: make(map[MatchType]map[string][]Emote),
		// canEntitleEmote is provided a default here as most of the rules simply returned true.
		canEntitle: func(emote Emote, entitlement Entitlement) bool {
			return true
		},
	}
}

// match sets the matcher rule on the builder with the one provided.
func (b *emoteBuilder) withMatch(f emoteMatcher) *emoteBuilder {
	b.match = f
	return b
}

func (b *emoteBuilder) withFilters(filters ...emoteFilter) *emoteBuilder {
	b.filters = append(b.filters, filters...)
	return b
}

func (b *emoteBuilder) withName(val string) *emoteBuilder {
	b.name = val
	return b
}

func (b *emoteBuilder) parallel() *emoteBuilder {
	b.isParallel = true
	return b
}

// entitle overrides the default entitlement matcher.
func (b *emoteBuilder) withCanEntitle(f func(emote Emote, entitlement Entitlement) bool) *emoteBuilder {
	b.canEntitle = f
	return b
}

func (b *emoteBuilder) findEmoteMatches(emote Emote, modifier string) (entitlementIDs []string, entitledEmotes []Emote) {
	if b.match == nil {
		return nil, nil
	}

	for _, filter := range b.filters {
		if !filter(emote, modifier) {
			return nil, nil
		}
	}

	match := b.match(emote, modifier)
	if match == nil {
		return nil, nil
	}

	if match.matchType == AlwaysEntitled {

		return nil, []Emote{match.emote}
	}

	// Checking for no entitlementIDs needs to happen after checking for `AlwaysEntitled` as those
	// matches don't load any entitlements.
	if len(match.entitlementIDs) == 0 {
		return nil, nil
	}

	b.mu.Lock()
	defer b.mu.Unlock()

	// Maps need to be initialize the first time we find the given `match.matchType`.
	if _, ok := b.emotes[match.matchType]; !ok {
		b.emotes[match.matchType] = make(map[string][]Emote)
	}

	for _, id := range match.entitlementIDs {
		b.emotes[match.matchType][id] = append(b.emotes[match.matchType][id], match.emote)
	}

	entitlementIDs = append(entitlementIDs, match.entitlementIDs...)

	return entitlementIDs, nil
}

func (b *emoteBuilder) matchEntitlements(entitlements ...Entitlement) []Emote {
	var entitledEmotes []Emote

	entitlementsByID := make(map[string]Entitlement)
	for _, e := range entitlements {
		entitlementsByID[e.ID] = e
	}

	for matchType, matchedEmotes := range b.emotes {
		switch matchType {
		case RequiresEmoteEntitlement, RequiresEmoteGroupEntitlement:
			entitledEmotes = append(entitledEmotes, b.matchSingleRequiredEntitlement(matchedEmotes, entitlementsByID)...)
		case RequiresMultipleEmoteGroupEntitlements:
			entitledEmotes = append(entitledEmotes, b.matchMultipleRequiredEntitlements(matchedEmotes, entitlementsByID)...)
		default:
			continue
		}
	}

	return entitledEmotes
}

func (b *emoteBuilder) matchSingleRequiredEntitlement(matchedEmotes map[string][]Emote, entitlements map[string]Entitlement) []Emote {
	var entitledEmotes []Emote

	for entitlementID, emotes := range matchedEmotes {
		for _, emote := range emotes {
			if entitlement, ok := entitlements[entitlementID]; ok && b.canEntitle(emote, entitlement) {
				entitledEmotes = append(entitledEmotes, emote)
			}
		}
	}

	return entitledEmotes
}

// note: we do not call b.canEntitle in this function because canEntitle does not
// support needing to check multiple entitlement IDs for a single emote
func (b *emoteBuilder) matchMultipleRequiredEntitlements(matchedEmotes map[string][]Emote, entitlements map[string]Entitlement) []Emote {
	var entitledEmotes []Emote

	// for emotes that require multiple entitlements to use, we
	// create a reverse mapping from `emote -> [entitlementIDs]`, where
	// the `entitlementIDs` is a list of all entitlement IDs that must be
	// entitled for the user to use the emote.
	emoteIDToEntitlements := make(map[string][]string)
	emoteIDToEmote := make(map[string]Emote)
	for entitlementID, emotes := range matchedEmotes {
		for _, emote := range emotes {
			emoteIDToEntitlements[emote.ID] = append(emoteIDToEntitlements[emote.ID], entitlementID)
			emoteIDToEmote[emote.ID] = emote
		}
	}

	for emoteID, entitlementIDs := range emoteIDToEntitlements {
		shouldEntitle := true
		for _, id := range entitlementIDs {
			if _, ok := entitlements[id]; !ok {
				shouldEntitle = false
			}
		}

		if shouldEntitle {
			entitledEmotes = append(entitledEmotes, emoteIDToEmote[emoteID])
		}
	}

	return entitledEmotes
}
