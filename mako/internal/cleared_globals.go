package mako

import (
	mk "code.justin.tv/commerce/mako/twirp"
)

const (
	ClearedGlobalsGroup = "CLEARED_GLOBALS"
)

// ClearedGlobalsCache is an in-memory representation of the cleared globals list as read from json configs on service startup
type ClearedGlobalsCache interface {
	GetClearedGlobals() map[string][]*mk.Emoticon
}

type emoteCacherInMemory struct {
	// ClearedGlobals are the subset of Global Emotes which have been cleared for 3rd party use
	ClearedGlobals []*mk.Emote
}

// NewClearedGlobalsCache keeps track of the cleared globals list as read from json configs on service startup
func NewClearedGlobalsCache(clearedGlobals []*mk.Emote) ClearedGlobalsCache {
	cacher := &emoteCacherInMemory{
		ClearedGlobals: clearedGlobals,
	}
	return cacher
}

// This function returns the Cleared Globals list as read from Json on service startup
func (emc *emoteCacherInMemory) GetClearedGlobals() map[string][]*mk.Emoticon {

	// Merge Emote info with Owner/Product info
	emoticons := []*mk.Emoticon{}
	for i, emoteFromCache := range emc.ClearedGlobals {
		emoticons = append(emoticons, &mk.Emoticon{
			Id:        emoteFromCache.Id,
			Code:      emoteFromCache.Pattern,
			GroupId:   ClearedGlobalsGroup,
			OwnerId:   "",
			ProductId: "",
			State:     mk.EmoteState_active,
			Domain:    mk.Domain_emotes,
			Order:     int64(i),
			AssetType: mk.EmoteAssetType_static,
		})
	}

	emoticonsByGroup := map[string][]*mk.Emoticon{
		ClearedGlobalsGroup: emoticons,
	}

	return emoticonsByGroup
}

type clearedGlobalsFake struct {
	clearedGlobals []*mk.Emoticon
}

func (cgf *clearedGlobalsFake) GetClearedGlobals() map[string][]*mk.Emoticon {
	return map[string][]*mk.Emoticon{
		ClearedGlobalsGroup: cgf.clearedGlobals,
	}
}

func MakeClearedGlobalsForTesting(globals []*mk.Emoticon) ClearedGlobalsCache {
	return &clearedGlobalsFake{
		clearedGlobals: globals,
	}
}
