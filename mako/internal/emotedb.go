package mako

import (
	"context"
	"fmt"
	"strings"
	"time"

	uuid "github.com/satori/go.uuid"

	mk "code.justin.tv/commerce/mako/twirp"
	mako_dashboard "code.justin.tv/commerce/mako/twirp/dashboard"
	"github.com/dgraph-io/ristretto"
	"google.golang.org/protobuf/types/known/timestamppb"
)

type Domain string
type EmoteState string
type EmoteAssetType string
type AnimatedEmoteTemplate string
type ImageSource string

const (
	Active          EmoteState = "active"
	Inactive        EmoteState = "inactive"
	Moderated       EmoteState = "moderated"
	Pending         EmoteState = "pending"
	PendingArchived EmoteState = "pending_archived"
	Archived        EmoteState = "archived"
	Unknown         EmoteState = "unknown"

	EmotesDomain   Domain = "emotes"
	StickersDomain Domain = "stickers"

	AnimatedAssetType EmoteAssetType = "animated"
	StaticAssetType   EmoteAssetType = "static"

	NoTemplate AnimatedEmoteTemplate = "NO_TEMPLATE"
	Shake      AnimatedEmoteTemplate = "SHAKE"
	Roll       AnimatedEmoteTemplate = "ROLL"
	Spin       AnimatedEmoteTemplate = "SPIN"
	Rave       AnimatedEmoteTemplate = "RAVE"
	SlideIn    AnimatedEmoteTemplate = "SLIDEIN"
	SlideOut   AnimatedEmoteTemplate = "SLIDEOUT"

	UserImage           ImageSource = "user_image"
	DefaultLibraryImage ImageSource = "default_library_image"

	ActiveStickerGroupID = "sticker-pack"

	EmotesV2Prefix       = "emotesv2"
	emotesV2PrefixFormat = EmotesV2Prefix + "_%s"

	/*************************************
	      Legacy Hardcoded Emote Sets
	**************************************/
	// DefaultEmoticonSet is the set used for Global Emotes
	DefaultEmoticonSet = 0
	// PrimeEmoticonSet is the emote set associated with the original ad-free Twitch Prime product
	PrimeEmoticonSet = 19194
	// Prime2EmoticonSet is the emote set associated with the new ad-full Twitch Prime product
	Prime2EmoticonSet = 1002436
	// TurboEmoticonSet is the emote set associated with the active Twitch Turbo product
	TurboEmoticonSet = 793
)

// EmoteQuery is used to support complex queries against the `EmoteDB`. One of the index keys are required
// for queries.
//
// All other fields are optional filter fields.
type EmoteQuery struct {
	// OwnerID is an index key. This key is used to queyr all emotes for a given owner/channel.
	OwnerID string
	// GroupID is an index key. This key is used to query all emotes for a given emote group.
	GroupID string
	// Code is an index key. This key is used to query all emotes with a given code.
	Code string

	// Domain is a filter key.
	Domain Domain
	// States is a filter key. This filter will return emotes that have any of the states
	// provided in the slice.
	States []EmoteState
	// EmoteGroup is a filter key.
	EmoteGroup string
	// AssetType is a filter key
	AssetType EmoteAssetType
}

// EmoteUpdate is used to support UpdateItem for multiple emote attributes at a time.
// Fields that are non-nil trigger an update with the specified value, while nil fields do not trigger an update.
// At least one field is required.
type EmoteUpdate struct {
	// GroupID is used to update an emote's GroupID
	GroupID *string
	// State is used to update an emote's State
	State *EmoteState
	// Order is used to update an emote's Order
	Order *int64
	// EmotesGroup is used to update an emote's EmotesGroup
	EmotesGroup *string
	// TODO: Add any other emote fields that get updated here
	ImageSource *ImageSource
}

// Used for comparing an emote's assetType to the group's type that the emote is being assigned to
func (e EmoteAssetType) ToGroupType() EmoteGroupType {
	switch e {
	case AnimatedAssetType:
		return EmoteGroupAnimated
	default:
		return EmoteGroupStatic
	}
}

func ParseEmoteState(state string) EmoteState {
	newState := EmoteState(state)

	switch EmoteState(state) {
	case Active:
		return newState
	case Inactive:
		return newState
	case Moderated:
		return newState
	case Pending:
		return newState
	case PendingArchived:
		return newState
	case Archived:
		return newState
	}

	return Inactive
}

func ParseDomain(domain string) Domain {
	newDomain := Domain(domain)

	switch newDomain {
	case EmotesDomain:
		return newDomain
	case StickersDomain:
		return newDomain
	}

	return EmotesDomain
}

func GetEmotesV2EmoteId() string {
	// TODO: Now that the image CDN URLs support hyphens, we can remove the hyphen trimming logic and use the UUIDs as they are
	// See: https://git.xarth.tv/systems/twitch_static/commit/041d72d4498b3f124283ad9149bcd04d5b800590
	newUUID := uuid.Must(uuid.NewV4()).String()
	trimmedUUID := strings.Replace(newUUID, "-", "", -1)
	return fmt.Sprintf(emotesV2PrefixFormat, trimmedUUID)
}

func IsValidEmotesV2EmoteId(emoteID string) bool {
	return strings.HasPrefix(emoteID, EmotesV2Prefix)
}

// This function is for creating standalone modified emotes for features like channel points emote entitlements.
// For T2/T3 modified emotes, we store the modifiers on the emotes themselves, so as not to clog the emote picker.
func MakeModifiedEmote(emote Emote, modifier string, emotesGroup string, ownerID string, groupID string) Emote {
	modifiedEmote := emote
	modifierSuffix := "_" + modifier

	modifiedEmote.ID = emote.ID + modifierSuffix
	modifiedEmote.Code = emote.Code + modifierSuffix
	modifiedEmote.Suffix = emote.Suffix + modifierSuffix

	modifiedEmote.EmotesGroup = emotesGroup

	modifiedEmote.OwnerID = ownerID
	modifiedEmote.GroupID = groupID

	return modifiedEmote
}

type Emote struct {
	// Unique Primary Key
	ID                string
	OwnerID           string
	GroupID           string
	Prefix            string
	Suffix            string
	Code              string
	State             EmoteState
	Domain            Domain
	CreatedAt         time.Time
	UpdatedAt         time.Time
	Order             int64
	EmotesGroup       string
	ProductID         string
	AssetType         EmoteAssetType
	AnimationTemplate AnimatedEmoteTemplate
	Modifiers         []string
	ImageSource       ImageSource
}

func (e Emote) ToTwirp() mk.Emoticon {
	return mk.Emoticon{
		Id:                    e.ID,
		Code:                  e.Code,
		Suffix:                e.Suffix,
		GroupId:               e.GroupID,
		OwnerId:               e.OwnerID,
		Domain:                mk.Domain(mk.Domain_value[string(e.Domain)]),
		State:                 mk.EmoteState(mk.EmoteState_value[string(e.State)]),
		Type:                  GetEmoteType(e),
		ProductId:             e.ProductID,
		Order:                 e.Order,
		AssetType:             EmoteAssetTypeAsTwirp(e.AssetType),
		CreatedAt:             timestamppb.New(e.CreatedAt),
		AnimatedEmoteTemplate: mk.AnimatedEmoteTemplate(mk.AnimatedEmoteTemplate_value[string(e.AnimationTemplate)]),
	}
}

// Deprecated - This is being used by GetEmoteSetDetails to convert follower emotes.
func (e Emote) ToLegacyTwirpEmote() mk.Emote {
	return mk.Emote{
		Id:                    e.ID,
		Pattern:               e.Code,
		Modifiers:             ModifierCodesToModifiers(e.Modifiers...),
		GroupId:               e.GroupID,
		AssetType:             EmoteAssetTypeAsTwirp(e.AssetType),
		Type:                  GetEmoteType(e),
		Order:                 e.Order,
		CreatedAt:             timestamppb.New(e.CreatedAt),
		AnimatedEmoteTemplate: mk.AnimatedEmoteTemplate(mk.AnimatedEmoteTemplate_value[string(e.AnimationTemplate)]),
	}
}

func (e Emote) ToDashboardTwirp() mako_dashboard.Emote {
	return mako_dashboard.Emote{
		Id:                    e.ID,
		Code:                  e.Code,
		Suffix:                e.Suffix,
		GroupId:               e.GroupID,
		OwnerId:               e.OwnerID,
		Domain:                mako_dashboard.Domain(mako_dashboard.Domain_value[string(e.Domain)]),
		State:                 mako_dashboard.EmoteState(mako_dashboard.EmoteState_value[string(e.State)]),
		Order:                 e.Order,
		AssetType:             EmoteAssetTypeAsDashboardTwirp(e.AssetType),
		CreatedAt:             timestamppb.New(e.CreatedAt),
		AnimatedEmoteTemplate: mako_dashboard.AnimatedEmoteTemplate(mako_dashboard.AnimatedEmoteTemplate_value[string(e.AnimationTemplate)]),
		Origin:                mako_dashboard.EmoteOrigin(mako_dashboard.EmoteOrigin_value[e.EmotesGroup]),
	}
}

// ToTwirpEmoticonGroup is the next best way to convert from internal to twirp, since we don't have a struct for internal emoticon groups
func ToTwirpEmoticonGroup(emotes []Emote) mk.EmoticonGroup {
	var twirpEmotes []*mk.Emoticon
	groupID := ""
	for _, emote := range emotes {
		groupID = emote.GroupID
		twirpEmote := emote.ToTwirp()
		twirpEmotes = append(twirpEmotes, &twirpEmote)
	}
	SortTwirpEmoticonsByOrderAscendingAndCreatedAtDescending(twirpEmotes)

	return mk.EmoticonGroup{
		Id:        groupID,
		Emoticons: twirpEmotes,
		Order:     GetEmoteGroupOrder(groupID),
	}
}

// EmoteDB is responsible for reading and writing `Emote`s to a persistent store.
type EmoteDB interface {
	// ReadByIDs is a way to quickly read emotes by multiple ids.
	ReadByIDs(ctx context.Context, ids ...string) ([]Emote, error)
	// ReadByCodes is used in place of `Query()` when you want to query multiple codes.
	ReadByCodes(ctx context.Context, codes ...string) ([]Emote, error)
	// ReadByGroupIDs queries all the emotes for a particular group.
	ReadByGroupIDs(ctx context.Context, ids ...string) ([]Emote, error)
	// WriteEmotes durably stores a set of emotes to the database. EmoteID is a unique primary key.
	// Attempts to write an emote with a key that already exists will overwrite the existing emote.
	// WriteEmotes should only be used creating new emote(s), or else we may run into race conditions.
	// If you want to update multiple emotes at a time, use UpdateEmotes instead.
	WriteEmotes(ctx context.Context, emotes ...Emote) ([]Emote, error)
	// DeleteEmote deletes a single emote.
	DeleteEmote(ctx context.Context, id string) (Emote, error)
	// UpdateState updates an emote's state.
	UpdateState(ctx context.Context, id string, state EmoteState) (Emote, error)
	// UpdateCode updates an emote's code and suffix.
	UpdateCode(ctx context.Context, id, code, suffix string) (Emote, error)
	// UpdatePrefix updates an emote's code and prefix.
	UpdatePrefix(ctx context.Context, id, code, prefix string) (Emote, error)
	// UpdateEmote updates multiple emote attributes at a time.
	UpdateEmote(ctx context.Context, id string, params EmoteUpdate) (Emote, error)
	// UpdateEmotes updates multiple attributes of distinct emotes at a time.
	// Returns all existing and updated emotes for cache-busting purposes
	UpdateEmotes(ctx context.Context, emoteUpdates map[string]EmoteUpdate) ([]Emote, error)
	// Query is used to perform complex queries on emotes. Because of the filtering logic available, most cached EmoteDB implementations bypass cache for this operation.
	Query(ctx context.Context, query EmoteQuery) ([]Emote, error)
	Close() error
}

func NewEmoteDBWithCache(base EmoteDB, ttl time.Duration) (EmoteDB, error) {
	cache, err := ristretto.NewCache(&ristretto.Config{
		// 10x the maximum number of expected keys.
		NumCounters: 60000000,
		MaxCost:     6000000,
		BufferItems: 64,
	})
	if err != nil {
		return nil, err
	}

	return &emoteDBCache{
		base:  base,
		cache: cache,
		ttl:   ttl,
	}, nil
}

type emoteDBCache struct {
	base  EmoteDB
	cache *ristretto.Cache
	ttl   time.Duration
}

func (db *emoteDBCache) readCache(index string, ids ...string) ([]Emote, []string) {
	emotes := make([]Emote, 0, len(ids))
	missing := make([]string, 0, len(ids))

	for _, id := range ids {
		if id == "" {
			continue
		}

		value, exists := db.cache.Get(index + "/" + id)
		if !exists {
			missing = append(missing, id)
			continue
		}

		cachedEmotes, ok := value.([]Emote)
		if !ok {
			missing = append(missing, id)
			continue
		}

		emotes = append(emotes, cachedEmotes...)
	}

	return emotes, missing
}

func (db *emoteDBCache) writeCache(index string, requestKeys []string, emotes ...Emote) {
	keys := make(map[string][]Emote)

	for _, emote := range emotes {
		var key string
		switch index {
		case "code":
			key = emote.Code
		case "group":
			key = emote.GroupID
		case "id":
			key = emote.ID
		default:
			continue
		}

		if key == "" {
			continue
		}

		keys[key] = append(keys[key], emote)
	}

	// Negative Caching: If no emote was found in the base layer db for the given key, cache an empty slice.
	// Skip this for the code index because we don't want to fill the cache with random words from chat.
	if index != "code" {
		for _, key := range requestKeys {
			if _, ok := keys[key]; !ok {
				keys[key] = []Emote{}
			}
		}
	}

	for key, emotes := range keys {
		db.cache.SetWithTTL(index+"/"+key, emotes, 1, db.ttl)
	}
}

func (db *emoteDBCache) ReadByIDs(ctx context.Context, ids ...string) ([]Emote, error) {
	emotes, missing := db.readCache("id", ids...)
	if len(missing) == 0 {
		return emotes, nil
	}

	dbEmotes, err := db.base.ReadByIDs(ctx, missing...)
	if err != nil {
		return nil, err
	}

	db.writeCache("id", missing, dbEmotes...)

	emotes = append(emotes, dbEmotes...)
	return emotes, nil
}

func (db *emoteDBCache) ReadByCodes(ctx context.Context, codes ...string) ([]Emote, error) {
	emotes, missing := db.readCache("code", codes...)
	if len(missing) == 0 {
		return emotes, nil
	}

	dbEmotes, err := db.base.ReadByCodes(ctx, missing...)
	if err != nil {
		return nil, err
	}

	db.writeCache("code", missing, dbEmotes...)

	emotes = append(emotes, dbEmotes...)
	return emotes, nil
}

func (db *emoteDBCache) ReadByGroupIDs(ctx context.Context, ids ...string) ([]Emote, error) {
	emotes, missing := db.readCache("group", ids...)
	if len(missing) == 0 {
		return emotes, nil
	}

	dbEmotes, err := db.base.ReadByGroupIDs(ctx, missing...)
	if err != nil {
		return nil, err
	}

	db.writeCache("group", missing, dbEmotes...)

	emotes = append(emotes, dbEmotes...)
	return emotes, nil
}

func (db *emoteDBCache) WriteEmotes(ctx context.Context, emotes ...Emote) ([]Emote, error) {
	db.expireEmotes(emotes...)

	return db.base.WriteEmotes(ctx, emotes...)
}

func (db *emoteDBCache) expireEmotes(emotes ...Emote) {
	for _, emote := range emotes {
		if emote.Code != "" {
			db.cache.Del("code" + "/" + emote.Code)
		}
		if emote.GroupID != "" {
			db.cache.Del("group" + "/" + emote.GroupID)
		}
		if emote.ID != "" {
			db.cache.Del("id" + "/" + emote.ID)
		}
	}
}

func (db *emoteDBCache) DeleteEmote(ctx context.Context, id string) (Emote, error) {
	emotes, err := db.base.ReadByIDs(ctx, id)
	if err != nil {
		return Emote{}, err
	}

	db.expireEmotes(emotes...)
	return db.base.DeleteEmote(ctx, id)
}

func (db *emoteDBCache) UpdateState(ctx context.Context, id string, state EmoteState) (Emote, error) {
	emotes, err := db.base.ReadByIDs(ctx, id)
	if err != nil {
		return Emote{}, err
	}

	db.expireEmotes(emotes...)
	return db.base.UpdateState(ctx, id, state)
}

func (db *emoteDBCache) UpdateCode(ctx context.Context, id, code, suffix string) (Emote, error) {
	emotes, err := db.base.ReadByIDs(ctx, id)
	if err != nil {
		return Emote{}, err
	}

	db.expireEmotes(emotes...)
	return db.base.UpdateCode(ctx, id, code, suffix)
}

func (db *emoteDBCache) UpdatePrefix(ctx context.Context, id, code, prefix string) (Emote, error) {
	emotes, err := db.base.ReadByIDs(ctx, id)
	if err != nil {
		return Emote{}, err
	}

	db.expireEmotes(emotes...)
	return db.base.UpdatePrefix(ctx, id, code, prefix)
}

func (db *emoteDBCache) UpdateEmote(ctx context.Context, id string, params EmoteUpdate) (Emote, error) {
	existingEmotes, err := db.base.ReadByIDs(ctx, id)
	if err != nil {
		return Emote{}, err
	}

	updatedEmote, err := db.base.UpdateEmote(ctx, id, params)
	if err != nil {
		return Emote{}, err
	}

	// Expire both old and new emotes to ensure all potentially changed index keys are expired
	emotesToExpire := append(existingEmotes, updatedEmote)
	db.expireEmotes(emotesToExpire...)

	return updatedEmote, nil
}

func (db *emoteDBCache) UpdateEmotes(ctx context.Context, emoteUpdates map[string]EmoteUpdate) ([]Emote, error) {
	emotesToExpire, err := db.base.UpdateEmotes(ctx, emoteUpdates)
	if err != nil {
		return nil, err
	}

	// Expire all emotes returned to ensure all potentially changed index keys are expired
	db.expireEmotes(emotesToExpire...)

	return emotesToExpire, nil
}

func (db *emoteDBCache) Query(ctx context.Context, query EmoteQuery) ([]Emote, error) {
	return db.base.Query(ctx, query)
}

func (db *emoteDBCache) Close() error {
	return nil
}
