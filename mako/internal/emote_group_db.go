package mako

import (
	"context"
	"fmt"
	"strconv"
	"time"

	"github.com/pkg/errors"

	mkd "code.justin.tv/commerce/mako/twirp/dashboard"

	mk "code.justin.tv/commerce/mako/twirp"
	"github.com/dgraph-io/ristretto"

	log "code.justin.tv/commerce/logrus"
)

const (
	MaxInt64 = int64(^uint64(0) >> 1)

	EmoteGroupStatic   EmoteGroupType = "static_group"
	EmoteGroupAnimated EmoteGroupType = "animated_group"

	EmoteGroupOwnerIDCachePrefix string = "ownerid"
	EmoteGroupGroupIDCachePrefix string = "groups"

	EmoteGroupCacheKeyFormat = "%s/%s"
)

// We hardcode follower emote origin so that it can be used in the dynamo layer for GetFollowerEmoteGroup
// without having to access Twirp
var EmoteGroupOriginFollower = EmoteGroupOrigin(mkd.EmoteOrigin_Follower.String())

type EmoteGroupType string
type EmoteGroupOrigin string

func (ego EmoteGroupOrigin) String() string {
	return string(ego)
}

func (egt EmoteGroupType) String() string {
	return string(egt)
}

func (egt EmoteGroupType) ToTwirp() mkd.EmoteGroupType {
	switch egt {
	case EmoteGroupAnimated:
		return mkd.EmoteGroupType_animated_group
	default:
		return mkd.EmoteGroupType_static_group
	}
}

func ToMakoGroupAssetType(assetType mk.EmoteGroupType) EmoteGroupType {
	result := EmoteGroupStatic

	if assetType == mk.EmoteGroupType_animated_group {
		result = EmoteGroupAnimated
	}

	return result
}

func ToMakoGroupOrigin(origin mk.EmoteGroup) EmoteGroupOrigin {
	name, ok := mk.EmoteGroup_name[int32(origin)]
	if !ok {
		return EmoteGroupOrigin(mk.EmoteGroup_None)
	}
	return EmoteGroupOrigin(name)
}

func GetEmoteGroupOrder(groupID string) int64 {
	// TODO: get emote group order from product-type lookup table rather than having a hard dependency on groupID being a number
	groupIDNumber, err := strconv.ParseInt(groupID, 10, 64)
	if err != nil {
		groupIDNumber = MaxInt64 //default to the largest integer if we get a non-numeric groupID, since that's newer than subs/bits
		log.WithField("groupID", groupID).Warn("non-numeric groupID used before switch over to prod_emote_groups lookup table")
	}

	return groupIDNumber
}

type EmoteGroup struct {
	GroupID   string
	OwnerID   string
	Origin    EmoteGroupOrigin
	GroupType EmoteGroupType
	CreatedAt time.Time
}
type EmoteGroupDB interface {
	CreateEmoteGroup(ctx context.Context, group EmoteGroup) (emoteGroup EmoteGroup, conditionalCheckFailed bool, err error)
	GetEmoteGroups(ctx context.Context, ids ...string) ([]EmoteGroup, error)
	GetFollowerEmoteGroup(ctx context.Context, channelID string) (*EmoteGroup, error)
	DeleteEmoteGroup(ctx context.Context, groupID string) error
}

type emoteGroupDBCache struct {
	base  EmoteGroupDB
	cache *ristretto.Cache
	ttl   time.Duration
}

func NewEmoteGroupDBWithCache(base EmoteGroupDB, ttl time.Duration) (EmoteGroupDB, error) {
	cache, err := ristretto.NewCache(&ristretto.Config{
		// 10x the maximum number of expected keys.
		NumCounters: 2500000,
		// We expect approximately 250K channels on the Follower Emote Experiment List, and only minor dashboard traffic
		// for any other emote group origin besides Follower
		MaxCost:     250000,
		BufferItems: 64,
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to create in-memory cache for emoteGroupDB")
	}

	return &emoteGroupDBCache{
		base:  base,
		cache: cache,
		ttl:   ttl,
	}, nil
}

func (db *emoteGroupDBCache) CreateEmoteGroup(ctx context.Context, group EmoteGroup) (emoteGroup EmoteGroup, conditionalCheckFailed bool, err error) {
	return db.base.CreateEmoteGroup(ctx, group)
}

func (db *emoteGroupDBCache) readCache(prefix string, ids ...string) ([]EmoteGroup, []string) {
	groups := make([]EmoteGroup, 0, len(ids))
	missing := make([]string, 0, len(ids))

	for _, id := range ids {
		if id == "" {
			continue
		}

		value, exists := db.cache.Get(fmt.Sprintf(EmoteGroupCacheKeyFormat, prefix, id))
		if !exists {
			missing = append(missing, id)
			continue
		}

		cachedGroups, ok := value.([]EmoteGroup)
		if !ok {
			missing = append(missing, id)
			continue
		}

		groups = append(groups, cachedGroups...)
	}

	return groups, missing
}

func (db *emoteGroupDBCache) writeCache(prefix string, requestKeys []string, groups ...EmoteGroup) {
	keys := make(map[string][]EmoteGroup)

	for _, group := range groups {
		var key string
		switch prefix {
		case EmoteGroupOwnerIDCachePrefix:
			key = group.OwnerID
		case EmoteGroupGroupIDCachePrefix:
			key = group.GroupID
		default:
			continue
		}

		keys[key] = append(keys[key], group)
	}

	if prefix == EmoteGroupOwnerIDCachePrefix {
		for _, key := range requestKeys {
			if _, ok := keys[key]; !ok {
				keys[key] = []EmoteGroup{}
			}
		}
	}

	for key, groups := range keys {
		db.cache.SetWithTTL(fmt.Sprintf(EmoteGroupCacheKeyFormat, prefix, key), groups, 1, db.ttl)
	}
}

func (db *emoteGroupDBCache) expireEmoteGroups(emoteGroups ...EmoteGroup) {
	for _, emoteGroup := range emoteGroups {
		if emoteGroup.GroupID != "" {
			db.cache.Del(fmt.Sprintf(EmoteGroupCacheKeyFormat, EmoteGroupGroupIDCachePrefix, emoteGroup.GroupID))
		}
		if emoteGroup.OwnerID != "" {
			db.cache.Del(fmt.Sprintf(EmoteGroupCacheKeyFormat, EmoteGroupOwnerIDCachePrefix, emoteGroup.OwnerID))
		}
	}
}

func (db *emoteGroupDBCache) GetEmoteGroups(ctx context.Context, ids ...string) ([]EmoteGroup, error) {
	groups, missing := db.readCache(EmoteGroupGroupIDCachePrefix, ids...)
	if len(missing) == 0 {
		return groups, nil
	}

	dbGroups, err := db.base.GetEmoteGroups(ctx, missing...)
	if err != nil {
		return nil, err
	}

	db.writeCache(EmoteGroupGroupIDCachePrefix, missing, dbGroups...)
	groups = append(groups, dbGroups...)
	return groups, nil
}

func (db *emoteGroupDBCache) GetFollowerEmoteGroup(ctx context.Context, channelID string) (*EmoteGroup, error) {
	groups, missing := db.readCache(EmoteGroupOwnerIDCachePrefix, channelID)
	if len(missing) == 0 {
		if len(groups) == 0 {
			// empty group slice means that there is no follower group, and nil, nil represents that
			return nil, nil
		}
		return &groups[0], nil
	}
	dbGroup, err := db.base.GetFollowerEmoteGroup(ctx, channelID)
	if err != nil {
		return nil, err
	}
	keys := []string{channelID}
	if dbGroup == nil {
		// this should write an empty EmoteGroup slice to the cache
		db.writeCache(EmoteGroupOwnerIDCachePrefix, keys)
	} else {
		db.writeCache(EmoteGroupOwnerIDCachePrefix, keys, *dbGroup)
	}
	return dbGroup, nil
}

func (db *emoteGroupDBCache) DeleteEmoteGroup(ctx context.Context, id string) error {
	groups, err := db.base.GetEmoteGroups(ctx, id)
	if err != nil {
		return err
	}

	db.expireEmoteGroups(groups...)

	return db.base.DeleteEmoteGroup(ctx, id)
}
