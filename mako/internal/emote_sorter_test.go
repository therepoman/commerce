package mako_test

import (
	"math/rand"
	"testing"
	"time"

	mako "code.justin.tv/commerce/mako/internal"
	mako_client "code.justin.tv/commerce/mako/twirp"
	mako_dashboard "code.justin.tv/commerce/mako/twirp/dashboard"
	"github.com/golang/protobuf/ptypes/timestamp"
	. "github.com/smartystreets/goconvey/convey"
)

func TestEmoteSorter(t *testing.T) {
	Convey("Test SortEmotesByOrderAndCreatedOnAscending", t, func() {
		testCases := []struct {
			testCase      string
			expectedOrder []mako.Emote
		}{
			{
				testCase: "With no orders",
				expectedOrder: []mako.Emote{
					{ID: "rangerKenobi", Order: 0, CreatedAt: time.Unix(1505088000, 0)},
					{ID: "blackjaW", Order: 0, CreatedAt: time.Unix(1502064000, 0)},
					{ID: "johnny", Order: 0, CreatedAt: time.Unix(1497830400, 0)},
					{ID: "tooTrue", Order: 0, CreatedAt: time.Unix(1466380800, 0)},
					{ID: "twwHappy", Order: 0, CreatedAt: time.Unix(1453075200, 0)},
					{ID: "xander", Order: 0, CreatedAt: time.Unix(1436140800, 0)},
				},
			},
			{
				testCase: "With distinct orders",
				expectedOrder: []mako.Emote{
					{ID: "rangerKenobi", Order: 0, CreatedAt: time.Unix(1505088000, 0)},
					{ID: "tooTrue", Order: 1, CreatedAt: time.Unix(1466380800, 0)},
					{ID: "xander", Order: 2, CreatedAt: time.Unix(1436140800, 0)},
					{ID: "blackjaW", Order: 3, CreatedAt: time.Unix(1502064000, 0)},
					{ID: "johnny", Order: 4, CreatedAt: time.Unix(1497830400, 0)},
					{ID: "twwHappy", Order: 5, CreatedAt: time.Unix(1453075200, 0)},
				},
			},
			{
				testCase: "With a tie in orders",
				expectedOrder: []mako.Emote{
					{ID: "rangerKenobi", Order: 0, CreatedAt: time.Unix(1505088000, 0)},
					{ID: "tooTrue", Order: 1, CreatedAt: time.Unix(1466380800, 0)},
					{ID: "xander", Order: 2, CreatedAt: time.Unix(1436140800, 0)},
					{ID: "blackjaW", Order: 3, CreatedAt: time.Unix(1502064000, 0)},
					{ID: "johnny", Order: 3, CreatedAt: time.Unix(1497830400, 0)},
					{ID: "twwHappy", Order: 5, CreatedAt: time.Unix(1453075200, 0)},
				},
			},
		}

		for _, test := range testCases {
			Convey(test.testCase, func() {
				testEmotes := append([]mako.Emote{}, test.expectedOrder...)
				rand.Seed(time.Now().UnixNano())
				rand.Shuffle(len(testEmotes), func(i, j int) { testEmotes[i], testEmotes[j] = testEmotes[j], testEmotes[i] })

				mako.SortEmotesByOrderAscendingAndCreatedAtDescending(testEmotes)

				So(len(testEmotes), ShouldEqual, len(test.expectedOrder))
				for i, emote := range testEmotes {
					So(emote, ShouldResemble, test.expectedOrder[i])
				}
			})
		}
	})

	Convey("Test SortTwirpEmotesByOrderAscendingAndCreatedAtDescending", t, func() {
		testCases := []struct {
			testCase      string
			expectedOrder []*mako_client.Emote
		}{
			{
				testCase: "With no orders",
				expectedOrder: []*mako_client.Emote{
					{Id: "rangerKenobi", Order: 0, CreatedAt: &timestamp.Timestamp{Seconds: 1505088000}},
					{Id: "blackjaW", Order: 0, CreatedAt: &timestamp.Timestamp{Seconds: 1502064000}},
					{Id: "johnny", Order: 0, CreatedAt: &timestamp.Timestamp{Seconds: 1497830400}},
					{Id: "tooTrue", Order: 0, CreatedAt: &timestamp.Timestamp{Seconds: 1466380800}},
					{Id: "twwHappy", Order: 0, CreatedAt: &timestamp.Timestamp{Seconds: 1453075200}},
					{Id: "xander", Order: 0, CreatedAt: &timestamp.Timestamp{Seconds: 1436140800}},
				},
			},
			{
				testCase: "With distinct orders",
				expectedOrder: []*mako_client.Emote{
					{Id: "rangerKenobi", Order: 0, CreatedAt: &timestamp.Timestamp{Seconds: 1505088000}},
					{Id: "tooTrue", Order: 1, CreatedAt: &timestamp.Timestamp{Seconds: 1466380800}},
					{Id: "xander", Order: 2, CreatedAt: &timestamp.Timestamp{Seconds: 1436140800}},
					{Id: "blackjaW", Order: 3, CreatedAt: &timestamp.Timestamp{Seconds: 1502064000}},
					{Id: "johnny", Order: 4, CreatedAt: &timestamp.Timestamp{Seconds: 1497830400}},
					{Id: "twwHappy", Order: 5, CreatedAt: &timestamp.Timestamp{Seconds: 1453075200}},
				},
			},
			{
				testCase: "With a tie in orders",
				expectedOrder: []*mako_client.Emote{
					{Id: "rangerKenobi", Order: 0, CreatedAt: &timestamp.Timestamp{Seconds: 1505088000}},
					{Id: "tooTrue", Order: 1, CreatedAt: &timestamp.Timestamp{Seconds: 1466380800}},
					{Id: "xander", Order: 2, CreatedAt: &timestamp.Timestamp{Seconds: 1436140800}},
					{Id: "blackjaW", Order: 3, CreatedAt: &timestamp.Timestamp{Seconds: 1502064000}},
					{Id: "johnny", Order: 3, CreatedAt: &timestamp.Timestamp{Seconds: 1497830400}},
					{Id: "twwHappy", Order: 5, CreatedAt: &timestamp.Timestamp{Seconds: 1453075200}},
				},
			},
			{
				testCase: "With a tie in orders and nil emotes",
				expectedOrder: []*mako_client.Emote{
					{Id: "rangerKenobi", Order: 0, CreatedAt: &timestamp.Timestamp{Seconds: 1505088000}},
					{Id: "tooTrue", Order: 1, CreatedAt: &timestamp.Timestamp{Seconds: 1466380800}},
					{Id: "xander", Order: 2, CreatedAt: &timestamp.Timestamp{Seconds: 1436140800}},
					{Id: "blackjaW", Order: 3, CreatedAt: &timestamp.Timestamp{Seconds: 1502064000}},
					{Id: "johnny", Order: 3, CreatedAt: &timestamp.Timestamp{Seconds: 1497830400}},
					{Id: "twwHappy", Order: 5, CreatedAt: &timestamp.Timestamp{Seconds: 1453075200}},
					nil,
					nil,
				},
			},
			{
				testCase: "With nil emotes and nil timestamps",
				expectedOrder: []*mako_client.Emote{
					{Id: "rangerKenobi", Order: 0, CreatedAt: &timestamp.Timestamp{Seconds: 1505088000}},
					{Id: "tooTrue", Order: 1, CreatedAt: &timestamp.Timestamp{Seconds: 1466380800}},
					{Id: "xander", Order: 2, CreatedAt: &timestamp.Timestamp{Seconds: 1436140800}},
					{Id: "blackjaW", Order: 3, CreatedAt: &timestamp.Timestamp{Seconds: 1502064000}},
					{Id: "johnny", Order: 3, CreatedAt: nil},
					{Id: "twwHappy", Order: 5, CreatedAt: &timestamp.Timestamp{Seconds: 1453075200}},
					nil,
					nil,
				},
			},
		}

		for _, test := range testCases {
			Convey(test.testCase, func() {
				testEmotes := append([]*mako_client.Emote{}, test.expectedOrder...)
				rand.Seed(time.Now().UnixNano())
				rand.Shuffle(len(testEmotes), func(i, j int) { testEmotes[i], testEmotes[j] = testEmotes[j], testEmotes[i] })

				mako.SortTwirpEmotesByOrderAscendingAndCreatedAtDescending(testEmotes)

				So(len(testEmotes), ShouldEqual, len(test.expectedOrder))
				for i, emote := range testEmotes {
					So(emote, ShouldResemble, test.expectedOrder[i])
				}
			})
		}
	})

	Convey("Test SortTwirpEmoticonsByOrderAscendingAndCreatedAtDescending", t, func() {
		testCases := []struct {
			testCase      string
			expectedOrder []*mako_client.Emoticon
		}{
			{
				testCase: "With no orders",
				expectedOrder: []*mako_client.Emoticon{
					{Id: "rangerKenobi", Order: 0, CreatedAt: &timestamp.Timestamp{Seconds: 1505088000}},
					{Id: "blackjaW", Order: 0, CreatedAt: &timestamp.Timestamp{Seconds: 1502064000}},
					{Id: "johnny", Order: 0, CreatedAt: &timestamp.Timestamp{Seconds: 1497830400}},
					{Id: "tooTrue", Order: 0, CreatedAt: &timestamp.Timestamp{Seconds: 1466380800}},
					{Id: "twwHappy", Order: 0, CreatedAt: &timestamp.Timestamp{Seconds: 1453075200}},
					{Id: "xander", Order: 0, CreatedAt: &timestamp.Timestamp{Seconds: 1436140800}},
				},
			},
			{
				testCase: "With distinct orders",
				expectedOrder: []*mako_client.Emoticon{
					{Id: "rangerKenobi", Order: 0, CreatedAt: &timestamp.Timestamp{Seconds: 1505088000}},
					{Id: "tooTrue", Order: 1, CreatedAt: &timestamp.Timestamp{Seconds: 1466380800}},
					{Id: "xander", Order: 2, CreatedAt: &timestamp.Timestamp{Seconds: 1436140800}},
					{Id: "blackjaW", Order: 3, CreatedAt: &timestamp.Timestamp{Seconds: 1502064000}},
					{Id: "johnny", Order: 4, CreatedAt: &timestamp.Timestamp{Seconds: 1497830400}},
					{Id: "twwHappy", Order: 5, CreatedAt: &timestamp.Timestamp{Seconds: 1453075200}},
				},
			},
			{
				testCase: "With a tie in orders",
				expectedOrder: []*mako_client.Emoticon{
					{Id: "rangerKenobi", Order: 0, CreatedAt: &timestamp.Timestamp{Seconds: 1505088000}},
					{Id: "tooTrue", Order: 1, CreatedAt: &timestamp.Timestamp{Seconds: 1466380800}},
					{Id: "xander", Order: 2, CreatedAt: &timestamp.Timestamp{Seconds: 1436140800}},
					{Id: "blackjaW", Order: 3, CreatedAt: &timestamp.Timestamp{Seconds: 1502064000}},
					{Id: "johnny", Order: 3, CreatedAt: &timestamp.Timestamp{Seconds: 1497830400}},
					{Id: "twwHappy", Order: 5, CreatedAt: &timestamp.Timestamp{Seconds: 1453075200}},
				},
			},
			{
				testCase: "With a tie in orders and nil emotes",
				expectedOrder: []*mako_client.Emoticon{
					{Id: "rangerKenobi", Order: 0, CreatedAt: &timestamp.Timestamp{Seconds: 1505088000}},
					{Id: "tooTrue", Order: 1, CreatedAt: &timestamp.Timestamp{Seconds: 1466380800}},
					{Id: "xander", Order: 2, CreatedAt: &timestamp.Timestamp{Seconds: 1436140800}},
					{Id: "blackjaW", Order: 3, CreatedAt: &timestamp.Timestamp{Seconds: 1502064000}},
					{Id: "johnny", Order: 3, CreatedAt: &timestamp.Timestamp{Seconds: 1497830400}},
					{Id: "twwHappy", Order: 5, CreatedAt: &timestamp.Timestamp{Seconds: 1453075200}},
					nil,
					nil,
				},
			},
			{
				testCase: "With nil emotes and nil timestamps",
				expectedOrder: []*mako_client.Emoticon{
					{Id: "rangerKenobi", Order: 0, CreatedAt: &timestamp.Timestamp{Seconds: 1505088000}},
					{Id: "tooTrue", Order: 1, CreatedAt: &timestamp.Timestamp{Seconds: 1466380800}},
					{Id: "xander", Order: 2, CreatedAt: &timestamp.Timestamp{Seconds: 1436140800}},
					{Id: "blackjaW", Order: 3, CreatedAt: &timestamp.Timestamp{Seconds: 1502064000}},
					{Id: "johnny", Order: 3, CreatedAt: nil},
					{Id: "twwHappy", Order: 5, CreatedAt: &timestamp.Timestamp{Seconds: 1453075200}},
					nil,
					nil,
				},
			},
		}

		for _, test := range testCases {
			Convey(test.testCase, func() {
				testEmotes := append([]*mako_client.Emoticon{}, test.expectedOrder...)
				rand.Seed(time.Now().UnixNano())
				rand.Shuffle(len(testEmotes), func(i, j int) { testEmotes[i], testEmotes[j] = testEmotes[j], testEmotes[i] })

				mako.SortTwirpEmoticonsByOrderAscendingAndCreatedAtDescending(testEmotes)

				So(len(testEmotes), ShouldEqual, len(test.expectedOrder))
				for i, emote := range testEmotes {
					So(emote, ShouldResemble, test.expectedOrder[i])
				}
			})
		}
	})

	Convey("Test SortDashboardTwirpEmotesByOrderAscendingAndCreatedAtDescending", t, func() {
		testCases := []struct {
			testCase      string
			expectedOrder []*mako_dashboard.Emote
		}{
			{
				testCase: "With no orders",
				expectedOrder: []*mako_dashboard.Emote{
					{Id: "rangerKenobi", Order: 0, CreatedAt: &timestamp.Timestamp{Seconds: 1505088000}},
					{Id: "blackjaW", Order: 0, CreatedAt: &timestamp.Timestamp{Seconds: 1502064000}},
					{Id: "johnny", Order: 0, CreatedAt: &timestamp.Timestamp{Seconds: 1497830400}},
					{Id: "tooTrue", Order: 0, CreatedAt: &timestamp.Timestamp{Seconds: 1466380800}},
					{Id: "twwHappy", Order: 0, CreatedAt: &timestamp.Timestamp{Seconds: 1453075200}},
					{Id: "xander", Order: 0, CreatedAt: &timestamp.Timestamp{Seconds: 1436140800}},
				},
			},
			{
				testCase: "With distinct orders",
				expectedOrder: []*mako_dashboard.Emote{
					{Id: "rangerKenobi", Order: 0, CreatedAt: &timestamp.Timestamp{Seconds: 1505088000}},
					{Id: "tooTrue", Order: 1, CreatedAt: &timestamp.Timestamp{Seconds: 1466380800}},
					{Id: "xander", Order: 2, CreatedAt: &timestamp.Timestamp{Seconds: 1436140800}},
					{Id: "blackjaW", Order: 3, CreatedAt: &timestamp.Timestamp{Seconds: 1502064000}},
					{Id: "johnny", Order: 4, CreatedAt: &timestamp.Timestamp{Seconds: 1497830400}},
					{Id: "twwHappy", Order: 5, CreatedAt: &timestamp.Timestamp{Seconds: 1453075200}},
				},
			},
			{
				testCase: "With a tie in orders",
				expectedOrder: []*mako_dashboard.Emote{
					{Id: "rangerKenobi", Order: 0, CreatedAt: &timestamp.Timestamp{Seconds: 1505088000}},
					{Id: "tooTrue", Order: 1, CreatedAt: &timestamp.Timestamp{Seconds: 1466380800}},
					{Id: "xander", Order: 2, CreatedAt: &timestamp.Timestamp{Seconds: 1436140800}},
					{Id: "blackjaW", Order: 3, CreatedAt: &timestamp.Timestamp{Seconds: 1502064000}},
					{Id: "johnny", Order: 3, CreatedAt: &timestamp.Timestamp{Seconds: 1497830400}},
					{Id: "twwHappy", Order: 5, CreatedAt: &timestamp.Timestamp{Seconds: 1453075200}},
				},
			},
			{
				testCase: "With a tie in orders and nil emotes",
				expectedOrder: []*mako_dashboard.Emote{
					{Id: "rangerKenobi", Order: 0, CreatedAt: &timestamp.Timestamp{Seconds: 1505088000}},
					{Id: "tooTrue", Order: 1, CreatedAt: &timestamp.Timestamp{Seconds: 1466380800}},
					{Id: "xander", Order: 2, CreatedAt: &timestamp.Timestamp{Seconds: 1436140800}},
					{Id: "blackjaW", Order: 3, CreatedAt: &timestamp.Timestamp{Seconds: 1502064000}},
					{Id: "johnny", Order: 3, CreatedAt: &timestamp.Timestamp{Seconds: 1497830400}},
					{Id: "twwHappy", Order: 5, CreatedAt: &timestamp.Timestamp{Seconds: 1453075200}},
					nil,
					nil,
				},
			},
			{
				testCase: "With nil emotes and nil timestamps",
				expectedOrder: []*mako_dashboard.Emote{
					{Id: "rangerKenobi", Order: 0, CreatedAt: &timestamp.Timestamp{Seconds: 1505088000}},
					{Id: "tooTrue", Order: 1, CreatedAt: &timestamp.Timestamp{Seconds: 1466380800}},
					{Id: "xander", Order: 2, CreatedAt: &timestamp.Timestamp{Seconds: 1436140800}},
					{Id: "blackjaW", Order: 3, CreatedAt: &timestamp.Timestamp{Seconds: 1502064000}},
					{Id: "johnny", Order: 3, CreatedAt: nil},
					{Id: "twwHappy", Order: 5, CreatedAt: &timestamp.Timestamp{Seconds: 1453075200}},
					nil,
					nil,
				},
			},
		}

		for _, test := range testCases {
			Convey(test.testCase, func() {
				testEmotes := append([]*mako_dashboard.Emote{}, test.expectedOrder...)
				rand.Seed(time.Now().UnixNano())
				rand.Shuffle(len(testEmotes), func(i, j int) { testEmotes[i], testEmotes[j] = testEmotes[j], testEmotes[i] })

				mako.SortDashboardTwirpEmotesByOrderAscendingAndCreatedAtDescending(testEmotes)

				So(len(testEmotes), ShouldEqual, len(test.expectedOrder))
				for i, emote := range testEmotes {
					So(emote, ShouldResemble, test.expectedOrder[i])
				}
			})
		}
	})
}
