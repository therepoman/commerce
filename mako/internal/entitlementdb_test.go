package mako_test

import (
	"testing"

	"code.justin.tv/commerce/mako/localdb"
	"code.justin.tv/commerce/mako/tests"
)

func TestMultiSourceEntitlementDB(t *testing.T) {
	tests.TestMultiSourceEntitlementDB(t, localdb.MakeEntitlementDB, localdb.MakeEntitlementDB, localdb.MakeEntitlementDB)
}
