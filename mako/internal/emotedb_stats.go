package mako

import (
	"context"
	"time"

	"github.com/cactus/go-statsd-client/statsd"
)

func NewEmoteDBWithStats(base EmoteDB, stats statsd.Statter, engine string) EmoteDB {
	if engine == "" {
		engine = "default"
	}

	if base == nil {
		panic("expected a non-nil EmoteDB for stats decorator")
	}

	if stats == nil {
		panic("expected a non-nil Statter for stats decorator")
	}

	return &emoteDBWithStats{
		base:   base,
		stats:  stats,
		engine: engine,
	}
}

type emoteDBWithStats struct {
	base   EmoteDB
	stats  statsd.Statter
	engine string
}

func (db *emoteDBWithStats) latency(operation string, f func() error) {
	name := "emotedb." + db.engine + "." + operation

	start := time.Now()
	if err := f(); err != nil {
		go db.stats.Inc(name+".errors", 1, 1.0)
	} else {
		go db.stats.Inc(name+".successes", 1, 1.0)
	}

	go db.stats.TimingDuration(name+".latency", time.Since(start), 1.0)
}

func (db *emoteDBWithStats) ReadByIDs(ctx context.Context, ids ...string) (emotes []Emote, err error) {
	db.latency("ReadByIDs", func() error {
		emotes, err = db.base.ReadByIDs(ctx, ids...)
		return err
	})

	return
}

func (db *emoteDBWithStats) ReadByCodes(ctx context.Context, codes ...string) (emotes []Emote, err error) {
	db.latency("ReadByCodes", func() error {
		emotes, err = db.base.ReadByCodes(ctx, codes...)
		return err
	})

	return
}

func (db *emoteDBWithStats) ReadByGroupIDs(ctx context.Context, ids ...string) (emotes []Emote, err error) {
	db.latency("ReadByGroupIDs", func() error {
		emotes, err = db.base.ReadByGroupIDs(ctx, ids...)
		return err
	})

	return
}
func (db *emoteDBWithStats) WriteEmotes(ctx context.Context, emotes ...Emote) (writtenEmotes []Emote, err error) {
	db.latency("WriteEmotes", func() error {
		writtenEmotes, err = db.base.WriteEmotes(ctx, emotes...)
		return err
	})

	return
}

func (db *emoteDBWithStats) DeleteEmote(ctx context.Context, id string) (emote Emote, err error) {
	db.latency("DeleteEmotes", func() error {
		emote, err = db.base.DeleteEmote(ctx, id)
		return err
	})

	return
}

func (db *emoteDBWithStats) UpdateState(ctx context.Context, id string, state EmoteState) (emote Emote, err error) {
	db.latency("UpdateState", func() error {
		emote, err = db.base.UpdateState(ctx, id, state)
		return err
	})

	return
}
func (db *emoteDBWithStats) UpdateCode(ctx context.Context, id, code, suffix string) (emote Emote, err error) {
	db.latency("UpdateCode", func() error {
		emote, err = db.base.UpdateCode(ctx, id, code, suffix)
		return err
	})

	return
}

func (db *emoteDBWithStats) UpdatePrefix(ctx context.Context, id, code, prefix string) (emote Emote, err error) {
	db.latency("UpdatePrefix", func() error {
		emote, err = db.base.UpdatePrefix(ctx, id, code, prefix)
		return err
	})

	return
}

func (db *emoteDBWithStats) UpdateEmote(ctx context.Context, id string, params EmoteUpdate) (emote Emote, err error) {
	db.latency("UpdateEmote", func() error {
		emote, err = db.base.UpdateEmote(ctx, id, params)
		return err
	})

	return
}

func (db *emoteDBWithStats) UpdateEmotes(ctx context.Context, emoteUpdates map[string]EmoteUpdate) (emotesToExpire []Emote, err error) {
	db.latency("UpdateEmotes", func() error {
		emotesToExpire, err = db.base.UpdateEmotes(ctx, emoteUpdates)
		return err
	})

	return
}

func (db *emoteDBWithStats) Query(ctx context.Context, query EmoteQuery) (emotes []Emote, err error) {
	db.latency("Query", func() error {
		emotes, err = db.base.Query(ctx, query)
		return err
	})

	return
}

func (db *emoteDBWithStats) Close() error {
	return db.base.Close()
}
