package mako

import (
	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/mako/twirp"
	"code.justin.tv/commerce/mako/twirp/dashboard"
)

type EmoteFilter struct {
	Domain Domain
	States []EmoteState
}

func MakeEmoteFilterFromTwirp(emoteFilter *mako_client.EmoteFilter) EmoteFilter {
	if emoteFilter == nil {
		return EmoteFilter{}
	}

	var states []EmoteState

	for _, state := range emoteFilter.States {
		states = append(states, ParseEmoteState(state.String()))
	}

	return EmoteFilter{
		Domain: ParseDomain(emoteFilter.Domain.String()),
		States: states,
	}
}

func MakeEmoteFilterFromDashboardTwirp(emoteFilter *mako_dashboard.EmoteFilter) EmoteFilter {
	if emoteFilter == nil {
		return EmoteFilter{}
	}

	var states []EmoteState

	for _, state := range emoteFilter.States {
		states = append(states, ParseEmoteState(state.String()))
	}

	return EmoteFilter{
		Domain: ParseDomain(emoteFilter.Domain.String()),
		States: states,
	}
}

// Applies a domain and state filter to the given emotes retrieved. The default filter values are
// EmoteState: "active"
// Domain: "emotes"
// If either filter value is not specified, the default will be used instead.
func ApplyEmoteFilter(filter EmoteFilter, emotes []Emote) []Emote {
	var states []EmoteState
	if len(filter.States) > 0 {
		states = filter.States
	} else {
		states = []EmoteState{Active}
	}

	var domain Domain
	if strings.Blank(string(filter.Domain)) {
		domain = EmotesDomain
	} else {
		domain = filter.Domain
	}

	filteredEmotes := make([]Emote, 0)
	for _, emote := range emotes {
		if string(domain) != string(emote.Domain) {
			continue
		}

		if len(states) > 0 {
			for _, allowedState := range states {
				if emote.State == allowedState {
					filteredEmotes = append(filteredEmotes, emote)
					break
				}
			}
		} else {
			filteredEmotes = append(filteredEmotes, emote)
		}
	}
	return filteredEmotes
}
