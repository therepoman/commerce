package mako

import (
	log "code.justin.tv/commerce/logrus"
	mk "code.justin.tv/commerce/mako/twirp"
)

func GetEmoteType(emote Emote) mk.EmoteGroup {
	group, ok := mk.EmoteGroup_value[emote.EmotesGroup]

	//TODO: should we wrap this in an experiment for quicker rollbacks?
	//TODO: should we wrap this in sampling logic to limit log spam?
	if !ok {
		log.WithField("emote", emote).Warn("unable to determine type of emote from stored emote group")
	}

	return mk.EmoteGroup(group)
}
