package mako

import (
	"context"
	"time"

	"github.com/cactus/go-statsd-client/statsd"
)

func NewEmoteModifiersDBWithStats(base EmoteModifiersDB, stats statsd.Statter, engine string) EmoteModifiersDB {
	if engine == "" {
		engine = "default"
	}

	if base == nil {
		panic("expected a non-nil EmoteModifiersDB for stats decorator")
	}

	if stats == nil {
		panic("expected a non-nil Statter for stats decorator")
	}

	return &emoteModifiersDBWithStats{
		base:   base,
		stats:  stats,
		engine: engine,
	}
}

type emoteModifiersDBWithStats struct {
	base   EmoteModifiersDB
	stats  statsd.Statter
	engine string
}

func (db *emoteModifiersDBWithStats) latency(operation string, batchSize int, f func() error) {
	name := "emotemodifiersdb." + db.engine + "." + operation

	start := time.Now()
	err := f()

	go func(err error, start time.Time) {
		if err != nil {
			_ = db.stats.Inc(name+".errors", 1, 1.0)
		} else {
			_ = db.stats.Inc(name+".successes", 1, 1.0)
			_ = db.stats.Inc(name+".successes.total", int64(batchSize), 1.0)
		}
		_ = db.stats.TimingDuration(name+".latency", time.Since(start), 1.0)
	}(err, start)
}

func (db *emoteModifiersDBWithStats) CreateGroups(ctx context.Context, groups ...EmoteModifierGroup) (emoteModifierGroups []EmoteModifierGroup, err error) {
	db.latency("CreateGroups", len(groups), func() error {
		emoteModifierGroups, err = db.base.CreateGroups(ctx, groups...)
		return err
	})

	return
}

func (db *emoteModifiersDBWithStats) UpdateGroups(ctx context.Context, groups ...EmoteModifierGroup) (emoteModifierGroups []EmoteModifierGroup, err error) {
	db.latency("UpdateGroups", len(groups), func() error {
		emoteModifierGroups, err = db.base.UpdateGroups(ctx, groups...)
		return err
	})

	return
}

func (db *emoteModifiersDBWithStats) ReadByIDs(ctx context.Context, ids ...string) (emoteModifierGroups []EmoteModifierGroup, err error) {
	db.latency("ReadByIDs", len(ids), func() error {
		emoteModifierGroups, err = db.base.ReadByIDs(ctx, ids...)
		return err
	})

	return
}

func (db *emoteModifiersDBWithStats) ReadByOwnerIDs(ctx context.Context, ownerIDs ...string) (emoteModifierGroups []EmoteModifierGroup, err error) {
	db.latency("ReadByOwnerIDs", len(ownerIDs), func() error {
		emoteModifierGroups, err = db.base.ReadByOwnerIDs(ctx, ownerIDs...)
		return err
	})

	return
}

func (db *emoteModifiersDBWithStats) DeleteByIDs(ctx context.Context, ids ...string) (err error) {
	db.latency("DeleteByIDs", len(ids), func() error {
		err = db.base.DeleteByIDs(ctx, ids...)
		return err
	})

	return
}

func (db *emoteModifiersDBWithStats) ReadEntitledGroupsForUser(ctx context.Context, userID string) (emoteModifierGroups []EmoteModifierGroup, err error) {
	db.latency("ReadEntitledGroupsForUser", 1, func() error {
		emoteModifierGroups, err = db.base.ReadEntitledGroupsForUser(ctx, userID)
		return err
	})

	return
}

func (db *emoteModifiersDBWithStats) WriteEntitledGroupsForUser(ctx context.Context, userID string, groups ...EmoteModifierGroup) (err error) {
	db.latency("WriteEntitledGroupsForUser", 1, func() error {
		err = db.base.WriteEntitledGroupsForUser(ctx, userID, groups...)
		return err
	})

	return
}

func (db *emoteModifiersDBWithStats) InvalidateEntitledGroupsCacheForUser(userID string) (err error) {
	db.latency("InvalidateEntitledGroupsCacheForUser", 1, func() error {
		err = db.base.InvalidateEntitledGroupsCacheForUser(userID)
		return err
	})

	return
}
