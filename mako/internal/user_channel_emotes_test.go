package mako_test

import (
	"testing"

	mako "code.justin.tv/commerce/mako/internal"

	"code.justin.tv/commerce/mako/localdb"
	"code.justin.tv/commerce/mako/tests"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/require"
)

func TestLocalGetUserChannelEmotes(t *testing.T) {
	tests.TestUserChannelEmotes(t, tests.UserChannelEmotesConfig{
		MakeEmoteDB:         localdb.MakeEmoteDB,
		MakeEntitlementDB:   localdb.MakeEntitlementDB,
		MakeEmoteModifierDB: localdb.MakeEmoteModifierDB,
	})
}

func TestNormalizeEntitlementID(t *testing.T) {
	Convey("NormalizeEntitlementID", t, func() {
		Convey("with a standard v1 emote entitlementID, return that emoteID and no modifiers", func() {
			entitlementID := "123456789"
			baseID, modifiers := mako.NormalizeEntitlementID(entitlementID)

			require.Equal(t, baseID, entitlementID)
			require.Nil(t, modifiers)
		})

		Convey("with a modified v1 emote entitlementID, return that emoteID and one modifier", func() {
			entitlementID := "123456789_HF"
			baseID, modifiers := mako.NormalizeEntitlementID(entitlementID)

			require.Equal(t, baseID, "123456789")
			require.NotNil(t, modifiers)
			require.Equal(t, len(modifiers), 1)
			require.Equal(t, modifiers[0], "HF")
		})

		Convey("with a standard v2 emote entitlementID, return that emoteID and no modifiers", func() {
			entitlementID := "emotesv2_defbb1648b2e4db1b13d74bb5bdb0d36"
			baseID, modifiers := mako.NormalizeEntitlementID(entitlementID)

			require.Equal(t, baseID, entitlementID)
			require.Nil(t, modifiers)
		})

		Convey("with a modified v2 emote entitlementID, return that emoteID and no modifiers", func() {
			entitlementID := "emotesv2_defbb1648b2e4db1b13d74bb5bdb0d36_SG"
			baseID, modifiers := mako.NormalizeEntitlementID(entitlementID)

			require.Equal(t, baseID, "emotesv2_defbb1648b2e4db1b13d74bb5bdb0d36")
			require.NotNil(t, modifiers)
			require.Equal(t, len(modifiers), 1)
			require.Equal(t, modifiers[0], "SG")
		})

		Convey("with an entitlementID based on a groupID", func() {
			entitlementID := "690000000"
			baseID, modifiers := mako.NormalizeEntitlementID(entitlementID)

			require.Equal(t, baseID, entitlementID)
			require.Nil(t, modifiers)
		})

		Convey("with an entitlementID based on a hardcoded string", func() {
			entitlementID := "smilie"
			baseID, modifiers := mako.NormalizeEntitlementID(entitlementID)

			require.Equal(t, baseID, entitlementID)
			require.Nil(t, modifiers)
		})
	})
}
