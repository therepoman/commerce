package mako

import (
	"context"
)

// Used for emote Auto-Resize datascience
const (
	EmoteUploadMethodAdvanced = "advanced" // The user chose 3 emotes manually
	EmoteUploadMethodSimple   = "simple"   // The user used auto-resize
	EmoteUploadMethodUnknown  = "unknown"  // Used only if the redis key is not found
)

// EmoteUploadMetadataCache is used for datascience and stores what upload method was used for a given emoteUploadID
type EmoteUploadMetadataCache interface {
	StoreUploadMethod(ctx context.Context, uploadID, method string) error
	ReadUploadMethod(ctx context.Context, uploadID string) (string, error)
}

// SanitizeUploadMethod will return the provided upload method if it is a valid upload method expected to be seen in the cache or unknown otherwise
func SanitizeUploadMethod(method string) string {
	switch method {
	case EmoteUploadMethodSimple:
		fallthrough
	case EmoteUploadMethodAdvanced:
		return method

	default:
		return EmoteUploadMethodUnknown
	}
}
