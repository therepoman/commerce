package mako_test

import (
	"testing"

	"code.justin.tv/commerce/mako/localdb"
	"code.justin.tv/commerce/mako/tests"
)

func TestLocalSmilies(t *testing.T) {
	tests.TestSmilies(t, localdb.MakeEntitlementDB)
}
