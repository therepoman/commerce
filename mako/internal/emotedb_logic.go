package mako

import (
	"context"
	"regexp"
	"strings"

	gogogadget_strings "code.justin.tv/commerce/gogogadget/strings"
)

func NewEmoteDBLogicLayer(base EmoteDB) EmoteDB {
	return &emoteDBLogicLayer{
		base:           base,
		modifierRegexp: regexp.MustCompile("[_A-Za-z0-9]+_([A-Z]+)"),
	}
}

type emoteDBLogicLayer struct {
	base EmoteDB

	// ModifierRegexp is the regex used to parse modified emotes.
	modifierRegexp *regexp.Regexp
}

func (db *emoteDBLogicLayer) ReadByIDs(ctx context.Context, ids ...string) ([]Emote, error) {
	var idsToRead []string
	modifiersByEmoteId := make(map[string][]string)
	nonModifiedEmoteIds := make(map[string]bool)

	for _, emoticonId := range ids {
		idToRead := emoticonId

		// modifiers will return a slice matching something similar to `[5544_BW BW]` (if it matches) where
		// the first index is the entire string and the subsequent index is the modifier code.
		modifiers := db.modifierRegexp.FindStringSubmatch(emoticonId)

		if len(modifiers) > 0 {
			// Remove the _BW (or whatever modifier code) from the emoticon id so that we can fetch
			// the original emote from the cache.
			idToRead = strings.Replace(emoticonId, "_"+modifiers[1], "", 1)
			modifiersByEmoteId[idToRead] = append(modifiersByEmoteId[idToRead], modifiers[1])
		} else {
			nonModifiedEmoteIds[idToRead] = true
		}

		idsToRead = append(idsToRead, idToRead)
	}

	// If the request contained both modified and unmodified emote ids, we would now have duplicates, so we dedupe
	dedupedIdsToRead := gogogadget_strings.Dedupe(idsToRead)

	emotesFromDb, err := db.base.ReadByIDs(ctx, dedupedIdsToRead...)
	if err != nil {
		return nil, err
	}

	var resultEmotes []Emote
	for _, emote := range emotesFromDb {
		// If the originally requested emote is a modified one (or more than one), return an emote with the modified id and code
		// and with the hard-coded modified emotes group id and owner id for each of the modifiers.
		if modifiers, ok := modifiersByEmoteId[emote.ID]; ok {
			for _, modifier := range modifiers {
				emoteFromDb := emote
				emoteFromDb.ID += "_" + modifier
				emoteFromDb.Code += "_" + modifier
				emoteFromDb.Suffix += "_" + modifier

				resultEmotes = append(resultEmotes, emoteFromDb)
			}
		}

		// If the originally requested emote is not a modified one, return the normal emote
		// Specifically don't use an "else" here so we can "redupe" for requests that ask for both modified and un-modified emotes
		if nonModifiedEmoteIds[emote.ID] {
			resultEmotes = append(resultEmotes, emote)
		}
	}

	return resultEmotes, nil
}

func (db *emoteDBLogicLayer) ReadByCodes(ctx context.Context, codes ...string) ([]Emote, error) {
	return db.base.ReadByCodes(ctx, codes...)
}

func (db *emoteDBLogicLayer) ReadByGroupIDs(ctx context.Context, ids ...string) ([]Emote, error) {
	return db.base.ReadByGroupIDs(ctx, ids...)
}

func (db *emoteDBLogicLayer) WriteEmotes(ctx context.Context, emotes ...Emote) ([]Emote, error) {
	return db.base.WriteEmotes(ctx, emotes...)
}

func (db *emoteDBLogicLayer) DeleteEmote(ctx context.Context, id string) (Emote, error) {
	return db.base.DeleteEmote(ctx, id)
}

func (db *emoteDBLogicLayer) UpdateState(ctx context.Context, id string, state EmoteState) (Emote, error) {
	return db.base.UpdateState(ctx, id, state)
}

func (db *emoteDBLogicLayer) UpdateCode(ctx context.Context, id, code, suffix string) (Emote, error) {
	return db.base.UpdateCode(ctx, id, code, suffix)
}

func (db *emoteDBLogicLayer) UpdatePrefix(ctx context.Context, id, code, prefix string) (Emote, error) {
	return db.base.UpdatePrefix(ctx, id, code, prefix)
}

func (db *emoteDBLogicLayer) UpdateEmote(ctx context.Context, id string, params EmoteUpdate) (Emote, error) {
	return db.base.UpdateEmote(ctx, id, params)
}

func (db *emoteDBLogicLayer) UpdateEmotes(ctx context.Context, emoteUpdates map[string]EmoteUpdate) ([]Emote, error) {
	return db.base.UpdateEmotes(ctx, emoteUpdates)
}

func (db *emoteDBLogicLayer) Query(ctx context.Context, query EmoteQuery) ([]Emote, error) {
	return db.base.Query(ctx, query)
}

func (db *emoteDBLogicLayer) Close() error {
	return db.base.Close()
}
