package mako_test

import (
	"context"
	"testing"
	"time"

	mako "code.justin.tv/commerce/mako/internal"
	"code.justin.tv/commerce/mako/localdb"
	"code.justin.tv/commerce/mako/tests"
	"github.com/pkg/errors"
	"github.com/stretchr/testify/assert"
)

func TestLocalEmoteGroupDB(t *testing.T) {
	tests.TestEmoteGroupDB(t, localdb.MakeEmoteGroupDB)
}

func TestCachedEmoteGroupDBWithLocal(t *testing.T) {
	tests.TestEmoteGroupDB(t, MakeEmoteGroupDBCache(localdb.MakeEmoteGroupDB))
}

func MakeEmoteGroupDBCache(makeDB tests.MakeEmoteGroupDB) tests.MakeEmoteGroupDB {
	return func(ctx context.Context, emoteGroups ...mako.EmoteGroup) (db mako.EmoteGroupDB, teardown func(), err error) {
		base, baseTeardown, err := makeDB(ctx)
		if err != nil {
			return nil, nil, errors.Wrap(err, "failed to initialize base EmoteGroupDB")
		}

		teardown = func() {
			baseTeardown()
		}

		for _, emoteGroup := range emoteGroups {
			base.CreateEmoteGroup(ctx, emoteGroup)
		}

		db, err = mako.NewEmoteGroupDBWithCache(base, 60*time.Second)
		if err != nil {
			return nil, nil, err
		}

		return db, teardown, nil
	}
}

func TestGetEmoteGroupOrder(t *testing.T) {
	tests := []struct {
		scenario string
		test     func(ctx context.Context, t *testing.T)
	}{
		{
			scenario: "should return the group_id when it is numerical",
			test: func(ctx context.Context, t *testing.T) {
				groupID := "12345"

				order := mako.GetEmoteGroupOrder(groupID)
				assert.Equal(t, int64(12345), order)
			},
		}, {
			scenario: "should return max int when group_id is non-numerical",
			test: func(ctx context.Context, t *testing.T) {
				groupID := "abcdef"

				order := mako.GetEmoteGroupOrder(groupID)
				assert.Equal(t, mako.MaxInt64, order)
			},
		},
	}

	for _, test := range tests {
		test := test

		t.Run(test.scenario, func(t *testing.T) {
			test.test(context.Background(), t)
		})
	}
}
