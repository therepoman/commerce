package mako

import (
	"context"
	"testing"
	"time"

	mkd "code.justin.tv/commerce/mako/twirp/dashboard"
	"github.com/stretchr/testify/require"

	mk "code.justin.tv/commerce/mako/twirp"
	"code.justin.tv/subs/paint/paintrpc"
	"github.com/stretchr/testify/assert"
)

func TestIsPermanentModifier(t *testing.T) {
	tests := []struct {
		scenario string
		fn       func(ctx context.Context, t *testing.T)
	}{
		{
			scenario: "Should return true for all permanent modifiers",
			fn: func(ctx context.Context, t *testing.T) {
				permanentMods := []mk.Modification{
					mk.Modification_BlackWhite,
					mk.Modification_HorizontalFlip,
					mk.Modification_Sunglasses,
					mk.Modification_Thinking,
					mk.Modification_Squished,
				}

				for _, mod := range permanentMods {
					assert.True(t, IsPermanentModifier(mk.Modification(mod)))
				}
			},
		},
		{
			scenario: "Should return false for all seasonal modifiers",
			fn: func(ctx context.Context, t *testing.T) {
				seasonalMods := []mk.Modification{
					mk.Modification_Pumpkin,
					mk.Modification_Reindeer,
					mk.Modification_Santa,
					mk.Modification_RedEnvelope,
					mk.Modification_LunarRat,
					mk.Modification_HeartEyes,
					mk.Modification_KissImprint,
					mk.Modification_HeartBroken,
					mk.Modification_Beard,
					mk.Modification_LuckyHat,
					mk.Modification_CloverEyes,
					mk.Modification_WomensDay,
					mk.Modification_RosieBandana,
					mk.Modification_BunnyEars,
					mk.Modification_EasterEggs,
					mk.Modification_EasterBasket,
					mk.Modification_FlowerCrown,
					mk.Modification_BubbleTea,
					mk.Modification_Sombrero,
					mk.Modification_Unicorn,
					mk.Modification_PrideFan,
					mk.Modification_FeatherBoa,
					mk.Modification_Overlay,
					mk.Modification_Maracas,
					mk.Modification_Guitar,
					mk.Modification_WitchHat,
					mk.Modification_Pumpkin2,
					mk.Modification_Snowflake,
					mk.Modification_Snowman,
				}

				for _, mod := range seasonalMods {
					assert.False(t, IsPermanentModifier(mk.Modification(mod)))
				}
			},
		},
	}

	for _, test := range tests {
		test := test
		t.Run(test.scenario, func(t *testing.T) {
			t.Parallel()

			ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
			defer cancel()

			test.fn(ctx, t)
		})
	}
}

func TestModifierCodeToModifier(t *testing.T) {
	tests := []struct {
		scenario string
		fn       func(ctx context.Context, t *testing.T)
	}{
		{
			scenario: "Should convert valid codes to modifier",
			fn: func(ctx context.Context, t *testing.T) {
				// GIVEN: valid codes
				for idx, code := range paintrpc.Modification_name {

					// WHEN: call ModifierCodeToModifier
					mod := ModifierCodeToModifier(code)

					// THEN: correct mako modification is returned.
					expected := mk.Modification(idx + 1)
					assert.Equal(t, mod, expected)
				}
			},
		},
		{
			scenario: "Should convert invalid codes to ModificationNone",
			fn: func(ctx context.Context, t *testing.T) {
				// GIVEN: invalid codes
				for _, code := range []string{"foo", "bar"} {

					// WHEN: call ModifierCodeToModifier
					mod := ModifierCodeToModifier(code)

					// THEN: ModificationNone is returned
					expected := mk.Modification_ModificationNone
					assert.Equal(t, mod, expected)
				}
			},
		},
	}

	for _, test := range tests {
		test := test
		t.Run(test.scenario, func(t *testing.T) {
			t.Parallel()

			ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
			defer cancel()

			test.fn(ctx, t)
		})
	}
}

func TestGetModifierCode(t *testing.T) {
	tests := []struct {
		scenario string
		fn       func(ctx context.Context, t *testing.T)
	}{
		{
			scenario: "Should return correct modifier code",
			fn: func(ctx context.Context, t *testing.T) {
				// GIVEN: correct inputs
				inputs := make([]mk.Modification, 0, len(mk.Modification_name))
				for _, mod := range mk.Modification_value {
					inputs = append(inputs, mk.Modification(mod))
				}

				// WHEN: calling GetModiferCode
				for _, mod := range inputs {
					code := GetModifierCode(mod)

					// THEN: correct codes are returned.
					if mod == mk.Modification_ModificationNone {
						assert.Equal(t, "", code)
					} else {
						name, ok := paintrpc.Modification_name[int32(mod-1)]
						assert.True(t, ok)
						assert.Equal(t, name, code)
					}
				}
			},
		},
		{
			scenario: "Should return empty string for unknown value",
			fn: func(ctx context.Context, t *testing.T) {
				// GIVEN: unknown value
				mod := mk.Modification(9999)

				// WHEN: calling GetModiferCode
				code := GetModifierCode(mod)

				// THEN: empty string is returned.
				assert.Equal(t, "", code)
			},
		},
	}

	for _, test := range tests {
		test := test
		t.Run(test.scenario, func(t *testing.T) {
			t.Parallel()

			ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
			defer cancel()

			test.fn(ctx, t)
		})
	}
}

func TestGetModifierSuffix(t *testing.T) {
	tests := []struct {
		scenario string
		fn       func(ctx context.Context, t *testing.T)
	}{
		{
			scenario: "Should return correct modifier codes with suffixes",
			fn: func(ctx context.Context, t *testing.T) {
				// GIVEN: correct inputs
				inputs := make([]mk.Modification, 0, len(mk.Modification_name))
				for _, mod := range mk.Modification_value {
					inputs = append(inputs, mk.Modification(mod))
				}

				// WHEN: calling GetModifierSuffix
				for _, mod := range inputs {
					code := GetModifierSuffix(mod)

					// THEN: correct codes are returned.
					if mod == mk.Modification_ModificationNone {
						assert.Equal(t, "", code)
					} else {
						name, ok := paintrpc.Modification_name[int32(mod-1)]
						assert.True(t, ok)
						assert.Equal(t, "_"+name, code)
					}
				}
			},
		},
		{
			scenario: "Should return empty string for unknown value",
			fn: func(ctx context.Context, t *testing.T) {
				// GIVEN: unknown value
				mod := mk.Modification(9999)

				// WHEN: calling GetModiferCode
				code := GetModifierSuffix(mod)

				// THEN: empty string is returned.
				assert.Equal(t, "", code)
			},
		},
	}

	for _, test := range tests {
		test := test
		t.Run(test.scenario, func(t *testing.T) {
			t.Parallel()

			ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
			defer cancel()

			test.fn(ctx, t)
		})
	}
}

// Test whether the PaintRPC modifications are in sync with the Mako modifications.
func TestAreModificationsInSync(t *testing.T) {
	// Mako should always 1 more modification than Paint (since Mako has "NONE" as modification 0).
	assert.Equal(t, len(paintrpc.Modification_name)+1, len(mk.Modification_name))

	// Every Paint modifier code should map to a Mako modification.
	for paintModifierCode := range paintrpc.Modification_value {
		makoModifier := ModifierCodeToModifier(paintModifierCode)
		assert.NotEqual(t, mk.Modification_ModificationNone, makoModifier)
	}
}

func TestIsModifiable(t *testing.T) {
	tests := []struct {
		scenario  string
		origin    string
		assetType EmoteAssetType
		expected  bool
	}{
		{
			scenario:  "static subscription emotes are modifiable",
			origin:    mkd.EmoteOrigin_Subscriptions.String(),
			assetType: StaticAssetType,
			expected:  true,
		},
		{
			scenario:  "animated subscription emotes are not modifiable",
			origin:    mkd.EmoteOrigin_Subscriptions.String(),
			assetType: AnimatedAssetType,
			expected:  false,
		},
		{
			scenario:  "static non-subscription emotes are not modifiable",
			origin:    mkd.EmoteOrigin_BitsBadgeTierEmotes.String(),
			assetType: StaticAssetType,
			expected:  false,
		},
	}

	for _, test := range tests {
		t.Run(test.scenario, func(t *testing.T) {
			result := IsModifiable(test.origin, test.assetType)
			require.Equal(t, result, test.expected)
		})
	}
}
