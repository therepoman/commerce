package mako_test

import (
	"context"
	"strings"
	"testing"
	"time"

	mako "code.justin.tv/commerce/mako/internal"
	"code.justin.tv/commerce/mako/localdb"
	"code.justin.tv/commerce/mako/tests"
	mk "code.justin.tv/commerce/mako/twirp"
	"github.com/pkg/errors"
	"google.golang.org/protobuf/types/known/timestamppb"

	. "github.com/smartystreets/goconvey/convey"
)

func TestLocalEmoteDB(t *testing.T) {
	tests.TestEmoteDB(t, localdb.MakeEmoteDB)
}

func TestCachedEmoteDBWithLocal(t *testing.T) {
	tests.TestEmoteDB(t, MakeEmoteDBCache(localdb.MakeEmoteDB))
}

func TestCachedEmoteDBWithLocalCacheTests(t *testing.T) {
	tests.TestCachedEmoteDB(t, tests.CachedEmoteDBConfig{
		Base:  localdb.MakeEmoteDB,
		Cache: makeEmoteDBCache,
		TestsToSkip: map[string]bool{
			// For the in-memory caching layer, emote codes are not negatively cached
			tests.ReadByCodesNegativeCacheTestName: true,
		},
	})
}

func makeEmoteDBCache(base mako.EmoteDB) tests.MakeEmoteDB {
	return MakeEmoteDBCache(func(ctx context.Context, emotes ...mako.Emote) (mako.EmoteDB, func(), error) {
		return base, func() {}, nil
	})
}

func MakeEmoteDBCache(makeDB tests.MakeEmoteDB) tests.MakeEmoteDB {
	return func(ctx context.Context, emotes ...mako.Emote) (db mako.EmoteDB, teardown func(), err error) {
		base, baseTeardown, err := makeDB(ctx)
		if err != nil {
			return nil, nil, errors.Wrap(err, "failed to initialize base EmoteDB")
		}

		teardown = func() {
			baseTeardown()
		}

		base.WriteEmotes(ctx, emotes...)

		db, err = mako.NewEmoteDBWithCache(base, 60*time.Second)
		if err != nil {
			return nil, nil, err
		}

		return db, teardown, nil
	}
}

func TestModels_GetEmotesV2EmoteId(t *testing.T) {
	Convey("GetEmotesV2EmoteId returns a string with the emotesv2 prefix", t, func() {
		emoteID := mako.GetEmotesV2EmoteId()

		So(strings.HasPrefix(emoteID, mako.EmotesV2Prefix), ShouldBeTrue)
	})
}

func TestModels_IsValidEmotesV2EmoteId(t *testing.T) {
	Convey("Given a valid emotesV2 ID, return true", t, func() {
		So(mako.IsValidEmotesV2EmoteId("emotesv2_3e22a0ab6add4a4c9bbb43897d196cdc"), ShouldBeTrue)
	})

	Convey("Given a siteDB emotes ID, return false", t, func() {
		So(mako.IsValidEmotesV2EmoteId("1234567890"), ShouldBeFalse)
	})
}

func TestToLegacyTwirpEmote(t *testing.T) {
	Convey("Given an emote return a Twirp Emote", t, func() {
		now := time.Now()
		emote := mako.Emote{
			ID:                "test_id",
			OwnerID:           "owner_id",
			GroupID:           "group_id",
			Prefix:            "foo",
			Suffix:            "Bar",
			Code:              "fooBar",
			State:             mako.Active,
			Domain:            mako.EmotesDomain,
			CreatedAt:         now,
			UpdatedAt:         now,
			Order:             0,
			EmotesGroup:       "Follower",
			ProductID:         "product_id",
			AssetType:         mako.StaticAssetType,
			Modifiers:         []string{"BW"},
			AnimationTemplate: mako.NoTemplate,
		}

		expectedLegacyTwirpEmote := mk.Emote{
			Id:                    "test_id",
			Pattern:               "fooBar",
			Modifiers:             []mk.Modification{mk.Modification_BlackWhite},
			GroupId:               "group_id",
			AssetType:             mk.EmoteAssetType_static,
			Type:                  mk.EmoteGroup_Follower,
			Order:                 0,
			CreatedAt:             timestamppb.New(now),
			AnimatedEmoteTemplate: mk.AnimatedEmoteTemplate_NO_TEMPLATE,
		}

		twirpEmote := emote.ToLegacyTwirpEmote()

		So(twirpEmote, ShouldResemble, expectedLegacyTwirpEmote)
	})
}
