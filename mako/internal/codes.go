package mako

import (
	"context"
	"math/rand"
	"strings"
	"sync"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/mako/config"
	"github.com/segmentio/ksuid"
)

const (
	GlobalEmoteGroupID    = "0"
	TwoFactorEmoteGroupID = "300374282"
)

type GetEmotesByCodesParams struct {
	EmoteDB          EmoteDB
	EntitlementDB    EntitlementDB
	EmoteModifiersDB EmoteModifiersDB
	Dynamic          config.DynamicConfigClient
	UserID           string
	ChannelID        string
	Codes            []string
}

// GetEmotesByCodes takes a slice of emote codes and returns any emotes within that list that were entitled to the given owner.
func GetEmotesByCodes(ctx context.Context, params GetEmotesByCodesParams) ([]Emote, error) {
	entitledEmotes := make([]Emote, 0)
	normalizedCodes, modifiers := NormalizeEmoteCodes(params.Codes...)

	emotes, err := params.EmoteDB.ReadByCodes(ctx, normalizedCodes...)
	if err != nil {
		return nil, err
	}

	if len(emotes) == 0 {
		return nil, nil
	}

	var entitlementIDs []string
	var builders = []*emoteBuilder{
		globalEmotes(params.UserID, params.Dynamic),
		smilieEmotes(),
		groupEmotes(),
		singleEmotes(),
		singleModifiedEmotes(),
		followerEmotes(params.UserID, params.ChannelID),
		twoFactorEmotes(),
		t2t3ModifiedEmotes(ctx, params.EmoteModifiersDB),
	}

	var wg sync.WaitGroup
	var mu sync.Mutex

	runBuilder := func(builder *emoteBuilder, emote Emote, modifier string) {
		builderEntitlementIDs, builderEntitledEmotes := builder.findEmoteMatches(emote, modifier)

		mu.Lock()
		entitlementIDs = append(entitlementIDs, builderEntitlementIDs...)
		entitledEmotes = append(entitledEmotes, builderEntitledEmotes...)
		mu.Unlock()
	}

	for _, emote := range emotes {
		emote := emote

		if emote.State != Active {
			continue
		}

		for _, builder := range builders {
			builder := builder

			// Generate a new request id so we can trace builder operations
			builder.requestID, err = ksuid.NewRandom()
			if err != nil {
				builder.requestID = ksuid.Nil
			}

			for modifier := range modifiers[emote.Code] {
				modifier := modifier

				if builder.isParallel {
					wg.Add(1)

					go func() {
						defer wg.Done()

						runBuilder(builder, emote, modifier)
					}()
				} else {
					runBuilder(builder, emote, modifier)
				}
			}
		}
	}

	// Wait for all the parallel builders to finish.
	wg.Wait()

	// Check for all entitlements that are stored somewhere (e.g. Materia, Following Service, etc)
	entitlements, err := params.EntitlementDB.ReadByIDs(ctx, params.UserID, dedupeIDs(entitlementIDs...)...)
	if err != nil {
		log.WithError(err).WithFields(log.Fields{
			"codes":            params.Codes,
			"normalized_codes": normalizedCodes,
			"entitled_emotes":  entitledEmotes,
			"entitlement_ids":  entitlementIDs,
			"user_id":          params.UserID,
		}).Error("GetEmotesByCodes: failed to read entitlements by id")
		return nil, err
	}

	for _, builder := range builders {
		entitledEmotes = append(entitledEmotes, builder.matchEntitlements(entitlements...)...)
	}

	return dedupeEmotesByCodes(entitledEmotes), nil
}

func dedupeIDs(ids ...string) []string {
	var uniques = make(map[string]struct{})
	var deduped []string

	for _, key := range ids {
		if _, ok := uniques[key]; !ok {
			deduped = append(deduped, key)
			uniques[key] = struct{}{}
		}
	}

	return deduped
}

func dedupeEmotesByCodes(emotes []Emote) []Emote {
	newEmotes := make([]Emote, 0, len(emotes))
	emotesByCodes := make(map[string]Emote)

	for _, emote := range emotes {
		if _, exists := emotesByCodes[emote.Code]; !exists {
			emotesByCodes[emote.Code] = emote
		} else if emote.GroupID != GlobalEmoteGroupID {
			emotesByCodes[emote.Code] = emote
		}
	}

	for _, emote := range emotesByCodes {
		newEmotes = append(newEmotes, emote)
	}

	return newEmotes
}

// normalizeEmoteCodes accepts a list of emote codes such as `Foobar`, `Foobar_TK`, `ChuckBlue`
// and returns the code without any modifiers while also returning a map containing any modifiers that were
// found.
//
// Foobar => ["Foobar", "TK", "SG"]
func NormalizeEmoteCodes(codes ...string) ([]string, map[string]map[string]struct{}) {
	normalizedCodes := make([]string, 0, len(codes))
	modifiers := make(map[string]map[string]struct{})

	// Normalize codes to remove any modified emotes.
	for _, code := range codes {
		// Remove invalid codes
		if isInvalidEmoteCode(code) {
			continue
		}

		codeParts := strings.Split(code, "_")
		normalizedCode := codeParts[0]

		// If the entire code is '_' then we can't split otherwise we have an empty string.
		if code == "_" {
			codeParts = []string{code}
			normalizedCode = code
		}

		// Do not split a global emote and think it's a modified emote if it has the same `_` seperator in it.
		for _, smilie := range specialSmilies {
			if code == smilie {
				codeParts = []string{code}
				normalizedCode = code
			}
		}

		// Only add a code to the list once.
		if _, ok := modifiers[normalizedCode]; !ok {
			modifiers[normalizedCode] = make(map[string]struct{})
			normalizedCodes = append(normalizedCodes, normalizedCode)
		}

		if len(codeParts) > 1 {
			modifier := codeParts[1]
			modifiers[normalizedCode][modifier] = struct{}{}
		} else {
			// Base emotes will appear as an empty modification.
			modifiers[normalizedCode][""] = struct{}{}
		}
	}

	return normalizedCodes, modifiers
}

const validEmoteChars = "abcdefghijklmnopqrstuvwxyz0123456789_"

func isInvalidEmoteCode(code string) bool {
	// To be valid, the emote code must be either a smilie or in english, and the code must be below the max code length
	return (!isEnglishChars(code) && !isSmilie(code)) || len(code) > MaxCodeLengthWithModifier
}

func isSmilie(value string) bool {
	_, ok := validSmilies[value]
	return ok
}

func isTwoFactorEmote(emote Emote) bool {
	return emote.GroupID == TwoFactorEmoteGroupID
}

func isEnglishChars(value string) bool {
	for _, char := range strings.ToLower(value) {
		if !strings.Contains(validEmoteChars, string(char)) {
			return false
		}
	}

	return true
}

func RandomEmote() string {
	b := make([]byte, 8)
	for i := range b {
		b[i] = validEmoteChars[rand.Intn(len(validEmoteChars))]
		if b[i] == '_' {
			b[i] = 'T'
		}
	}
	return string(b)
}

func makeGoldenKappa(emote Emote) Emote {
	emote.ID = GoldenKappaEmoteID
	return emote
}

func ExtractEmotePrefix(code string, suffix string) string {
	return strings.TrimSuffix(code, suffix)
}
