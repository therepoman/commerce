package mako_test

import (
	"context"
	"errors"
	"testing"

	mako "code.justin.tv/commerce/mako/internal"
	"code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/internal_mock"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestEmoteDBLogicLayer(t *testing.T) {
	Convey("Test ReadByIDs", t, func() {
		mockEmoteDB := new(internal_mock.EmoteDB)

		emoteDBLogicLayer := mako.NewEmoteDBLogicLayer(mockEmoteDB)

		ids := []string{"987654321", "emotesv2_636c2d56c4de4e989277b8694a66b1333"}
		testEmote1 := mako.Emote{
			ID:          ids[0],
			OwnerID:     "267893713",
			GroupID:     "123456789",
			Prefix:      "wolf",
			Suffix:      "HOWL",
			Code:        "wolfHOWL",
			State:       "active",
			Domain:      "emotes",
			EmotesGroup: "Subscriptions",
		}
		testEmote2 := mako.Emote{
			ID:          ids[1],
			OwnerID:     "267893713",
			GroupID:     "23456789",
			Prefix:      "wolf",
			Suffix:      "CUB",
			Code:        "wolfCUB",
			State:       "active",
			Domain:      "emotes",
			EmotesGroup: "BitsBadgeTierEmotes",
		}

		Convey("given base db returns error", func() {
			mockEmoteDB.On("ReadByIDs", mock.Anything, ids[0]).Return(nil, errors.New("dummy emotedb error"))

			Convey("return that error", func() {
				emotes, err := emoteDBLogicLayer.ReadByIDs(context.Background(), ids[0])

				So(emotes, ShouldBeNil)
				So(err, ShouldNotBeNil)
				So(err.Error(), ShouldEqual, "dummy emotedb error")
				mockEmoteDB.AssertExpectations(t)
			})
		})

		Convey("given a request for a non-modified emote id, the emotedb is called with that same id and the returned emote is returned", func() {
			mockEmoteDB.On("ReadByIDs", mock.Anything, ids[0]).Return([]mako.Emote{testEmote1}, nil)

			emotes, err := emoteDBLogicLayer.ReadByIDs(context.Background(), ids[0])

			So(emotes, ShouldNotBeNil)
			So(err, ShouldBeNil)
			So(len(emotes), ShouldEqual, 1)
			So(emotes[0], ShouldResemble, testEmote1)
			mockEmoteDB.AssertExpectations(t)
		})

		Convey("given a request for a non-modified V2 emote id, the emotedb is called with that same id and the returned emote is returned", func() {
			mockEmoteDB.On("ReadByIDs", mock.Anything, ids[1]).Return([]mako.Emote{testEmote2}, nil)

			emotes, err := emoteDBLogicLayer.ReadByIDs(context.Background(), ids[1])

			So(emotes, ShouldNotBeNil)
			So(err, ShouldBeNil)
			So(len(emotes), ShouldEqual, 1)
			So(emotes[0], ShouldResemble, testEmote2)
			mockEmoteDB.AssertExpectations(t)
		})

		Convey("given a request for a modified emote id, the emotedb is called with the unmodified emote id and the modified emote is returned", func() {
			modifier := "BW"
			modifiedEmoteId := ids[0] + "_" + modifier
			mockEmoteDB.On("ReadByIDs", mock.Anything, ids[0]).Return([]mako.Emote{testEmote1}, nil)

			emotes, err := emoteDBLogicLayer.ReadByIDs(context.Background(), modifiedEmoteId)

			So(emotes, ShouldNotBeNil)
			So(err, ShouldBeNil)
			So(len(emotes), ShouldEqual, 1)
			mockEmoteDB.AssertExpectations(t)

			So(emotes[0].ID, ShouldEqual, modifiedEmoteId)
			So(emotes[0].Code, ShouldEqual, testEmote1.Code+"_"+modifier)
			So(emotes[0].Suffix, ShouldEqual, testEmote1.Suffix+"_"+modifier)
			So(emotes[0].GroupID, ShouldEqual, testEmote1.GroupID)
			So(emotes[0].OwnerID, ShouldEqual, testEmote1.OwnerID)
		})

		Convey("given a request for a modified V2 emote id, the emotedb is called with the unmodified emote id and the modified emote is returned", func() {
			modifier := "BW"
			modifiedEmoteId := ids[1] + "_" + modifier
			mockEmoteDB.On("ReadByIDs", mock.Anything, ids[1]).Return([]mako.Emote{testEmote2}, nil)

			emotes, err := emoteDBLogicLayer.ReadByIDs(context.Background(), modifiedEmoteId)

			So(emotes, ShouldNotBeNil)
			So(err, ShouldBeNil)
			So(len(emotes), ShouldEqual, 1)
			mockEmoteDB.AssertExpectations(t)

			So(emotes[0].ID, ShouldEqual, modifiedEmoteId)
			So(emotes[0].Code, ShouldEqual, testEmote2.Code+"_"+modifier)
			So(emotes[0].Suffix, ShouldEqual, testEmote2.Suffix+"_"+modifier)
			So(emotes[0].GroupID, ShouldEqual, testEmote2.GroupID)
			So(emotes[0].OwnerID, ShouldEqual, testEmote2.OwnerID)
		})

		Convey("given a request for both modified and non-modified emote ids, the emotedb is called with the unmodified emote ids and the returned emotes are returned", func() {
			modifier := "BW"
			modifiedEmoteId := ids[1] + "_" + modifier
			mockEmoteDB.On("ReadByIDs", mock.Anything, ids[0], ids[1]).Return([]mako.Emote{testEmote1, testEmote2}, nil)

			emotes, err := emoteDBLogicLayer.ReadByIDs(context.Background(), []string{ids[0], modifiedEmoteId}...)

			So(emotes, ShouldNotBeNil)
			So(err, ShouldBeNil)
			So(len(emotes), ShouldEqual, 2)
			mockEmoteDB.AssertExpectations(t)

			if emotes[0].ID == testEmote1.ID {
				So(emotes[0], ShouldResemble, testEmote1)

				So(emotes[1].ID, ShouldEqual, modifiedEmoteId)
				So(emotes[1].Code, ShouldEqual, testEmote2.Code+"_"+modifier)
				So(emotes[1].Suffix, ShouldEqual, testEmote2.Suffix+"_"+modifier)
				So(emotes[1].GroupID, ShouldEqual, testEmote2.GroupID)
				So(emotes[1].OwnerID, ShouldEqual, testEmote2.OwnerID)
			} else {
				So(emotes[1], ShouldResemble, testEmote1)

				So(emotes[0].ID, ShouldEqual, modifiedEmoteId)
				So(emotes[0].Code, ShouldEqual, testEmote2.Code+"_"+modifier)
				So(emotes[0].Suffix, ShouldEqual, testEmote2.Suffix+"_"+modifier)
				So(emotes[0].GroupID, ShouldEqual, testEmote2.GroupID)
				So(emotes[0].OwnerID, ShouldEqual, testEmote2.OwnerID)
			}
		})

		Convey("given a request for a non-modified emote id and a modified emote id for the same emote, the emotedb is called with only one emote id (un-modified) and both emotes are returned", func() {
			modifier := "BW"
			modifiedEmoteId := ids[0] + "_" + modifier
			mockEmoteDB.On("ReadByIDs", mock.Anything, ids[0]).Return([]mako.Emote{testEmote1}, nil)

			emotes, err := emoteDBLogicLayer.ReadByIDs(context.Background(), ids[0], modifiedEmoteId)

			So(emotes, ShouldNotBeNil)
			So(err, ShouldBeNil)
			So(len(emotes), ShouldEqual, 2)
			mockEmoteDB.AssertExpectations(t)

			if emotes[0].ID == testEmote1.ID {
				So(emotes[0], ShouldResemble, testEmote1)

				So(emotes[1].ID, ShouldEqual, modifiedEmoteId)
				So(emotes[1].Code, ShouldEqual, testEmote1.Code+"_"+modifier)
				So(emotes[1].Suffix, ShouldEqual, testEmote1.Suffix+"_"+modifier)
				So(emotes[1].GroupID, ShouldEqual, testEmote1.GroupID)
				So(emotes[1].OwnerID, ShouldEqual, testEmote1.OwnerID)
			} else {
				So(emotes[1], ShouldResemble, testEmote1)

				So(emotes[0].ID, ShouldEqual, modifiedEmoteId)
				So(emotes[0].Code, ShouldEqual, testEmote1.Code+"_"+modifier)
				So(emotes[0].Suffix, ShouldEqual, testEmote1.Suffix+"_"+modifier)
				So(emotes[0].GroupID, ShouldEqual, testEmote1.GroupID)
				So(emotes[0].OwnerID, ShouldEqual, testEmote1.OwnerID)
			}
		})

		Convey("given a request for more than one modifier for the same emote, the emotedb is called with only one emote id (un-modified) and all modified emotes are returned", func() {
			modifier1 := "BW"
			modifier2 := "SG"
			modifiedEmoteId1 := ids[0] + "_" + modifier1
			modifiedEmoteId2 := ids[0] + "_" + modifier2
			mockEmoteDB.On("ReadByIDs", mock.Anything, ids[0]).Return([]mako.Emote{testEmote1}, nil)

			emotes, err := emoteDBLogicLayer.ReadByIDs(context.Background(), modifiedEmoteId1, modifiedEmoteId2)

			So(emotes, ShouldNotBeNil)
			So(err, ShouldBeNil)
			So(len(emotes), ShouldEqual, 2)
			mockEmoteDB.AssertExpectations(t)

			if emotes[0].ID == modifiedEmoteId1 {
				So(emotes[0].ID, ShouldEqual, modifiedEmoteId1)
				So(emotes[0].Code, ShouldEqual, testEmote1.Code+"_"+modifier1)
				So(emotes[0].Suffix, ShouldEqual, testEmote1.Suffix+"_"+modifier1)
				So(emotes[0].GroupID, ShouldEqual, testEmote1.GroupID)
				So(emotes[0].OwnerID, ShouldEqual, testEmote1.OwnerID)

				So(emotes[1].ID, ShouldEqual, modifiedEmoteId2)
				So(emotes[1].Code, ShouldEqual, testEmote1.Code+"_"+modifier2)
				So(emotes[1].Suffix, ShouldEqual, testEmote1.Suffix+"_"+modifier2)
				So(emotes[1].GroupID, ShouldEqual, testEmote1.GroupID)
				So(emotes[1].OwnerID, ShouldEqual, testEmote1.OwnerID)
			} else {
				So(emotes[1].ID, ShouldEqual, modifiedEmoteId1)
				So(emotes[1].Code, ShouldEqual, testEmote1.Code+"_"+modifier1)
				So(emotes[1].Suffix, ShouldEqual, testEmote1.Suffix+"_"+modifier1)
				So(emotes[1].GroupID, ShouldEqual, testEmote1.GroupID)
				So(emotes[1].OwnerID, ShouldEqual, testEmote1.OwnerID)

				So(emotes[0].ID, ShouldEqual, modifiedEmoteId2)
				So(emotes[0].Code, ShouldEqual, testEmote1.Code+"_"+modifier2)
				So(emotes[0].Suffix, ShouldEqual, testEmote1.Suffix+"_"+modifier2)
				So(emotes[0].GroupID, ShouldEqual, testEmote1.GroupID)
				So(emotes[0].OwnerID, ShouldEqual, testEmote1.OwnerID)
			}
		})
	})
}
