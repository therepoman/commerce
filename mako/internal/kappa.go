package mako

import (
	"fmt"
	"time"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/mako/config"
	"github.com/dgryski/dgohash"
)

const (
	KappaEmoteID       = "25"
	GoldenKappaEmoteID = "80393"
	GoldenKappaGroupID = "15940"
)

func IsLuckyGoldenKappa(userID string, dynamic config.DynamicConfigClient) bool {
	if dynamic.GetGoldenKappaAlwaysOnEnabled(userID) {
		return true
	}

	var goldenKappaDefaultChance = uint32(1000000)

	hash := dgohash.NewJenkins32()
	startOfDay := time.Now().Truncate(24 * time.Hour).Unix()
	_, err := hash.Write([]byte(fmt.Sprintf("%s:%d", userID, startOfDay)))
	if err != nil {
		logrus.WithError(err).Error("Error generating GoldenKappa hash")
		return false
	}
	chance := goldenKappaDefaultChance
	return hash.Sum32()%chance == 0
}
