package mako_test

import (
	"context"
	"testing"

	mako "code.justin.tv/commerce/mako/internal"
	"code.justin.tv/commerce/mako/localdb"
	"code.justin.tv/commerce/mako/tests"
	"github.com/cactus/go-statsd-client/statsd"
)

// TestLocalEmoteDBWithStats runs the test suite wrapping a local EmoteDB with a stats decorator to
// ensure that no functionality has been lost/altered.
func TestLocalEmoteDBWithStats(t *testing.T) {
	tests.TestEmoteDB(t, makeEmoteDBWithStats(localdb.MakeEmoteDB))
}

func makeEmoteDBWithStats(makeBase tests.MakeEmoteDB) tests.MakeEmoteDB {
	return func(ctx context.Context, emotes ...mako.Emote) (mako.EmoteDB, func(), error) {
		base, teardown, err := makeBase(ctx, emotes...)
		if err != nil {
			return nil, nil, err
		}

		noop, _ := statsd.NewNoop()
		return mako.NewEmoteDBWithStats(base, noop, ""), teardown, nil
	}
}
