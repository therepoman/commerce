package mako

const (
	MinPrefixLength           = 3  // prefixes must be at least three characters long
	MaxPrefixLength           = 10 // prefixes can be no longer than 10 characters
	MaxSuffixLength           = 20 // max suffix length is 20 according to the front-end
	MaxCodeLength             = MaxPrefixLength + MaxSuffixLength
	MaxModifierLength         = 3 // Includes the _ separator.
	MaxCodeLengthWithModifier = MaxCodeLength + MaxModifierLength
)
