package mako

import (
	"context"
	"time"

	"github.com/cactus/go-statsd-client/statsd"
)

func NewEmoteGroupDBWithStats(base EmoteGroupDB, stats statsd.Statter, engine string) EmoteGroupDB {
	if engine == "" {
		engine = "default"
	}

	if base == nil {
		panic("expected a non-nil EmoteGroupDB for stats decorator")
	}

	if stats == nil {
		panic("expected a non-nil Statter for stats decorator")
	}

	return &emoteGroupDBWithStats{
		base:   base,
		stats:  stats,
		engine: engine,
	}
}

type emoteGroupDBWithStats struct {
	base   EmoteGroupDB
	stats  statsd.Statter
	engine string
}

func (db *emoteGroupDBWithStats) latency(operation string, batchSize int, f func() error) {
	name := "emotegroupdb." + db.engine + "." + operation

	start := time.Now()
	err := f()

	go func(err error, start time.Time) {
		if err != nil {
			_ = db.stats.Inc(name+".errors", 1, 1.0)
		} else {
			_ = db.stats.Inc(name+".successes", 1, 1.0)
			_ = db.stats.Inc(name+".successes.total", int64(batchSize), 1.0)
		}
		_ = db.stats.TimingDuration(name+".latency", time.Since(start), 1.0)
	}(err, start)
}

func (db *emoteGroupDBWithStats) CreateEmoteGroup(ctx context.Context, group EmoteGroup) (emoteGroup EmoteGroup, conditionalCheckFailed bool, err error) {
	db.latency("CreateEmoteGroup", 1, func() error {
		emoteGroup, conditionalCheckFailed, err = db.base.CreateEmoteGroup(ctx, group)
		return err
	})

	return
}

func (db *emoteGroupDBWithStats) GetEmoteGroups(ctx context.Context, ids ...string) (emoteGroups []EmoteGroup, err error) {
	db.latency("GetEmoteGroups", len(ids), func() error {
		emoteGroups, err = db.base.GetEmoteGroups(ctx, ids...)
		return err
	})

	return
}

func (db *emoteGroupDBWithStats) GetFollowerEmoteGroup(ctx context.Context, channelID string) (emoteGroup *EmoteGroup, err error) {
	db.latency("GetFollowerEmoteGroup", 1, func() error {
		emoteGroup, err = db.base.GetFollowerEmoteGroup(ctx, channelID)
		return err
	})

	return
}

func (db *emoteGroupDBWithStats) DeleteEmoteGroup(ctx context.Context, id string) (err error) {
	db.latency("DeleteEmoteGroup", 1, func() error {
		err = db.base.DeleteEmoteGroup(ctx, id)
		return err
	})

	return
}
