package mako_test

import (
	"testing"

	"code.justin.tv/commerce/mako/internal"
	. "github.com/smartystreets/goconvey/convey"
)

func TestEmoteFilter(t *testing.T) {
	foodEmote := mako.Emote{ID: "food", State: mako.Active, Domain: mako.EmotesDomain}
	saladEmote := mako.Emote{ID: " salad", State: mako.Moderated, Domain: mako.EmotesDomain}
	pizzaEmote := mako.Emote{ID: "pizza", State: mako.Pending, Domain: mako.EmotesDomain}
	tacoEmote := mako.Emote{ID: " taco", State: mako.Inactive, Domain: mako.StickersDomain}

	allEmotes := []mako.Emote{foodEmote, saladEmote, pizzaEmote, tacoEmote}

	Convey("Test ApplyEmoteFilter", t, func() {
		Convey("When filter is empty and given emotes is nil", func() {
			So(mako.ApplyEmoteFilter(mako.EmoteFilter{}, nil), ShouldResemble, []mako.Emote{})
		})

		Convey("When the filter is empty and given emotes is an empty list", func() {
			So(mako.ApplyEmoteFilter(mako.EmoteFilter{}, []mako.Emote{}), ShouldResemble, []mako.Emote{})
		})

		Convey("When the filter is empty and given emotes has a single active emote, return that emote", func() {
			So(mako.ApplyEmoteFilter(mako.EmoteFilter{}, []mako.Emote{foodEmote}), ShouldResemble, []mako.Emote{foodEmote})
		})

		Convey("When the filter is empty and given a full list of emotes, return only active emote", func() {
			So(mako.ApplyEmoteFilter(mako.EmoteFilter{}, allEmotes), ShouldResemble, []mako.Emote{foodEmote})
		})

		Convey("When the filter says only emotes domain and given a full list of emotes, return only active emote", func() {
			So(mako.ApplyEmoteFilter(
				mako.EmoteFilter{Domain: mako.EmotesDomain}, allEmotes),
				ShouldResemble,
				[]mako.Emote{foodEmote})
		})

		Convey("When the filter says only active emotes and given a full list of emotes, return only active emote", func() {
			So(mako.ApplyEmoteFilter(
				mako.EmoteFilter{States: []mako.EmoteState{mako.Active}}, allEmotes),
				ShouldResemble,
				[]mako.Emote{foodEmote})
		})

		Convey("When the filter says only active and pending emotes and given a full list of emotes, return active and pending emotes", func() {
			So(mako.ApplyEmoteFilter(
				mako.EmoteFilter{States: []mako.EmoteState{mako.Active, mako.Pending}}, allEmotes),
				ShouldResemble,
				[]mako.Emote{foodEmote, pizzaEmote})
		})

		Convey("When the filter says active, pending and moderated states and given a full list of emotes, return all emotes (not stickers)", func() {
			So(mako.ApplyEmoteFilter(
				mako.EmoteFilter{States: []mako.EmoteState{mako.Active, mako.Pending, mako.Moderated}}, allEmotes),
				ShouldResemble,
				[]mako.Emote{foodEmote, saladEmote, pizzaEmote})
		})
	})
}
