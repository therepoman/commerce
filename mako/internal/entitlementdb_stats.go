package mako

import (
	"context"
	"time"

	"github.com/cactus/go-statsd-client/statsd"
)

func NewEntitlementDBWithStats(base EntitlementDB, stats statsd.Statter, engine string) EntitlementDB {
	if engine == "" {
		engine = "default"
	}

	if base == nil {
		panic("expected a non-nil EmoteDB for stats decorator")
	}

	if stats == nil {
		panic("expected a non-nil Statter for stats decorator")
	}

	return &entitlementDBWithStats{
		base:   base,
		stats:  stats,
		engine: engine,
	}
}

type entitlementDBWithStats struct {
	base   EntitlementDB
	stats  statsd.Statter
	engine string
}

func (db *entitlementDBWithStats) latency(operation string, batchSize int, f func() error) {
	name := "entitlementdb." + db.engine + "." + operation

	start := time.Now()
	if err := f(); err != nil {
		_ = db.stats.Inc(name+".errors", 1, 1.0)
	} else {
		_ = db.stats.Inc(name+".successes", 1, 1.0)
		_ = db.stats.Inc(name+".successes.total", int64(batchSize), 1.0)
	}

	_ = db.stats.TimingDuration(name+".latency", time.Since(start), 1.0)
}

func (db *entitlementDBWithStats) ReadByIDs(ctx context.Context, ownerID string, ids ...string) (entitlements []Entitlement, err error) {
	db.latency("ReadByIDs", len(ids), func() error {
		entitlements, err = db.base.ReadByIDs(ctx, ownerID, ids...)
		return err
	})

	return
}

func (db *entitlementDBWithStats) ReadByOwnerID(ctx context.Context, ownerID string, channelID string) (entitlements []Entitlement, err error) {
	db.latency("ReadByOwnerID", 1, func() error {
		entitlements, err = db.base.ReadByOwnerID(ctx, ownerID, channelID)
		return err
	})

	return
}

func (db *entitlementDBWithStats) WriteEntitlements(ctx context.Context, entitlements ...Entitlement) (err error) {
	db.latency("WriteEntitlements", len(entitlements), func() error {
		err = db.base.WriteEntitlements(ctx, entitlements...)
		return err
	})

	return
}
