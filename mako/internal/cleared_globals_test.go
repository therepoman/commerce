package mako_test

import (
	"testing"

	mako "code.justin.tv/commerce/mako/internal"
	mk "code.justin.tv/commerce/mako/twirp"
	. "github.com/smartystreets/goconvey/convey"
)

func TestClearedGlobalsCache(t *testing.T) {
	Convey("Test Emote in memory cacher", t, func() {
		clearedGlobals := []*mk.Emote{
			{
				Id:      "1234",
				Pattern: "Test",
			},
		}

		clearedGlobalsCache := mako.NewClearedGlobalsCache(clearedGlobals)

		Convey("Test GetClearedGlobals", func() {
			emotesByGroup := clearedGlobalsCache.GetClearedGlobals()
			So(len(emotesByGroup), ShouldEqual, 1)
			So(emotesByGroup[mako.ClearedGlobalsGroup], ShouldNotBeNil)
			So(emotesByGroup[mako.ClearedGlobalsGroup], ShouldResemble, []*mk.Emoticon{
				{
					Id:      "1234",
					Code:    "Test",
					GroupId: mako.ClearedGlobalsGroup,
				},
			})
		})
	})
}
