package mako

import (
	"context"
	"errors"

	log "code.justin.tv/commerce/logrus"

	mk "code.justin.tv/commerce/mako/twirp"
)

const (
	InEmoteGoodStanding    = EmoteStandingOverride("IN_EMOTE_GOOD_STANDING")
	NotInEmoteGoodStanding = EmoteStandingOverride("NOT_IN_EMOTE_GOOD_STANDING")
)

type EmoteStandingOverride string

func (o EmoteStandingOverride) String() string {
	return string(o)
}

type UserEmoteStandingOverride struct {
	UserID   string
	Override EmoteStandingOverride
}

func (eso UserEmoteStandingOverride) ToTwirp() (mk.UserEmoteStandingOverride, error) {
	var overrideKind mk.EmoteStandingOverride

	switch eso.Override {
	case NotInEmoteGoodStanding:
		overrideKind = mk.EmoteStandingOverride_NotInEmoteGoodStanding
	case InEmoteGoodStanding:
		overrideKind = mk.EmoteStandingOverride_InEmoteGoodStanding
	default:
		msg := "invalid override found while converting to twirp emote standing override"
		log.WithField("override", eso).Error(msg)
		return mk.UserEmoteStandingOverride{}, errors.New(msg)
	}

	twirpESO := mk.UserEmoteStandingOverride{
		UserId:   eso.UserID,
		Override: overrideKind,
	}
	return twirpESO, nil
}

func ToMako(twirpEmoteStandingOverride mk.UserEmoteStandingOverride) (UserEmoteStandingOverride, error) {
	var overrideKind EmoteStandingOverride
	switch twirpEmoteStandingOverride.Override {
	case mk.EmoteStandingOverride_NotInEmoteGoodStanding:
		overrideKind = NotInEmoteGoodStanding
	case mk.EmoteStandingOverride_InEmoteGoodStanding:
		overrideKind = InEmoteGoodStanding
	default:
		msg := "invalid override found while converting to mako emote standing override"
		log.WithField("override", twirpEmoteStandingOverride).Error(msg)
		return UserEmoteStandingOverride{}, errors.New(msg)
	}

	emoteStandingOverride := UserEmoteStandingOverride{
		UserID:   twirpEmoteStandingOverride.UserId,
		Override: overrideKind,
	}

	return emoteStandingOverride, nil
}

// This is the public interface for the emote standing overrides DB
type EmoteStandingOverridesDB interface {
	// Update or create the given emote standing overrides.
	UpdateOverrides(ctx context.Context, override ...UserEmoteStandingOverride) ([]UserEmoteStandingOverride, error)
	// Look up and return the override with the given UserID
	ReadByUserID(ctx context.Context, userID string) (*UserEmoteStandingOverride, error)
}
