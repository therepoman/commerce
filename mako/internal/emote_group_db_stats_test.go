package mako_test

import (
	"context"
	"testing"

	"code.justin.tv/commerce/mako/dynamo"
	mako "code.justin.tv/commerce/mako/internal"
	"code.justin.tv/commerce/mako/tests"
	"github.com/cactus/go-statsd-client/statsd"
)

func TestLocalEmoteGroupDBWithStats(t *testing.T) {
	tests.TestEmoteGroupDB(t, makeEmoteGroupDBWithStats(dynamo.MakeEmoteGroupDBForTesting))
}

func makeEmoteGroupDBWithStats(makeBase tests.MakeEmoteGroupDB) tests.MakeEmoteGroupDB {
	return func(ctx context.Context, groups ...mako.EmoteGroup) (mako.EmoteGroupDB, func(), error) {
		base, teardown, err := makeBase(ctx, groups...)
		if err != nil {
			return nil, teardown, err
		}

		noop, _ := statsd.NewNoop()
		return mako.NewEmoteGroupDBWithStats(base, noop, ""), teardown, nil
	}
}
