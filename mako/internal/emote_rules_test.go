package mako

import (
	"testing"

	"code.justin.tv/commerce/mako/config"
	"github.com/stretchr/testify/require"
)

type ruleTest struct {
	scenario               string
	entitlements           []Entitlement
	codes                  []string
	emotes                 []Emote
	expectedEntitlementIDs []string
	expectedEmotes         []Emote
	builder                func() *emoteBuilder
}

// Look in tests/test_codes.go for more tests around rules and codes.

func TestGroupRules(t *testing.T) {
	dynamicConfig := config.NewDynamicConfigFake()

	tests := []ruleTest{
		{
			scenario: "Kappa global",
			builder: func() *emoteBuilder {
				return globalEmotes("ownerdoesntmatter", dynamicConfig)
			},
			codes: []string{"Kappa", "Foobar", "ItDoesntMatter"},
			emotes: []Emote{
				{
					ID:      "123",
					Code:    "Kappa",
					GroupID: GlobalEmoteGroupID,
				},
			},
			expectedEmotes: []Emote{
				{
					ID:      "123",
					Code:    "Kappa",
					GroupID: GlobalEmoteGroupID,
				},
			},
		},

		{
			scenario: "return Smilies",
			builder: func() *emoteBuilder {
				return globalEmotes("ownerdoesntmatter", dynamicConfig)
			},
			codes: []string{":)"},
			emotes: []Emote{
				{
					ID:      "123",
					Code:    ":)",
					GroupID: GlobalEmoteGroupID,
				},
			},
			expectedEmotes: []Emote{
				{
					ID:      "123",
					Code:    ":)",
					GroupID: GlobalEmoteGroupID,
				},
			},
		},

		{
			scenario: "return global only Smilies",
			builder: func() *emoteBuilder {
				return globalEmotes("ownerdoesntmatter", dynamicConfig)
			},
			codes: []string{":)"},
			emotes: []Emote{
				{
					ID:      "123",
					Code:    ":)",
					GroupID: GlobalEmoteGroupID,
				},
				{
					ID:      "555",
					Code:    ":)",
					GroupID: string(PurpleSmilieGroupID),
				},
			},
			expectedEmotes: []Emote{
				{
					ID:      "123",
					Code:    ":)",
					GroupID: GlobalEmoteGroupID,
				},
			},
		},

		{
			scenario: "no modifiers on globals",
			builder: func() *emoteBuilder {
				return globalEmotes("ownerdoesntmatter", dynamicConfig)
			},
			codes: []string{"Kappa_BW"},
			emotes: []Emote{
				{
					ID:      "123",
					Code:    "Kappa",
					GroupID: GlobalEmoteGroupID,
				},
			},
			expectedEmotes: []Emote{},
		},

		{
			scenario: "return o_O",
			builder: func() *emoteBuilder {
				return globalEmotes("ownerdoesntmatter", dynamicConfig)
			},
			codes: []string{"o_O"},
			emotes: []Emote{
				{
					ID:      "123",
					Code:    "o_O",
					GroupID: GlobalEmoteGroupID,
				},
			},
			expectedEmotes: []Emote{
				{
					ID:      "123",
					Code:    "o_O",
					GroupID: GlobalEmoteGroupID,
				},
			},
		},
	}

	runRuleTest(t, tests...)
}

func runRuleTest(t *testing.T, tests ...ruleTest) {
	for _, test := range tests {
		test := test

		t.Run(test.scenario, func(t *testing.T) {
			builder := test.builder()

			emotes := make([]Emote, 0)
			codes, modifiers := NormalizeEmoteCodes(test.codes...)

			for _, givenEmote := range test.emotes {
				var found bool
				for _, code := range codes {
					if givenEmote.Code != code {
						continue
					}

					found = true

					for modifier := range modifiers[code] {
						entitlementIDs, entitledEmotes := builder.findEmoteMatches(givenEmote, modifier)

						emotes = append(emotes, entitledEmotes...)

						require.Equal(t, len(test.expectedEntitlementIDs), len(entitlementIDs))
						require.Equal(t, test.expectedEntitlementIDs, entitlementIDs)

						newEmotes := builder.matchEntitlements(test.entitlements...)
						if len(newEmotes) > 0 {
							emotes = append(emotes, newEmotes...)
						}
					}
				}

				if !found {
					t.Fatal("could not find code")
				}
			}

			require.Equal(t, test.expectedEmotes, emotes)
		})
	}
}
