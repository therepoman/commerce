package mako

import (
	log "code.justin.tv/commerce/logrus"
	"github.com/segmentio/ksuid"
)

// entitlementMatch is used for rules to return information about how to match an emote to an entitlement. Returning a nil `entitlementMatch` means nothing was matched.
type entitlementMatch struct {
	// the same entitlement that was given as input will be returned.
	entitlement Entitlement
	// emoteIDs is a list of emoteIDs that need to be looked up via the `EmotesDB` interface.
	emoteIDs []string
	// emoteGroupIDs is a list of groupIDs that need to be looked up via the `EmotesDB` interface.
	emoteGroupIDs []string
	// matchType determines if the emote can be looked up without checking the `EmotesDB`, or if the Emote
	// needs to be a certain type (see `MatchType`).
	matchType MatchType
}

// emoteEntitlementMatcher is used for all the entitlement rules to determine how to look up emotes for an entitlement. Every
// entitlement found from the user and channel given to GetUserChannelEmotes will execute each matcher.
type emoteEntitlementMatcher func(entitlement Entitlement) *entitlementMatch

// emoteEntitlementFilter is used to quickly decide if a rule supports an emote or not.
type emoteEntitlementFilter func(entitlement Entitlement) bool

// emoteAugmentor is used generally to augment a DB emote with one or many virtual emotes.
// In the golden kappa case, the original emote is completely replaced by the new one.
// However, in most use cases, the original emote should be returned with the virtual augmented emotes.
type emoteAugmentor func(Emote, ...Entitlement) []Emote

// emoteEntitlementBuilder is an abstraction that allows each tenant/use case of entitlements to be isolated from each other and constructed
// at a higher-level via a set of matching rules. Changes in one builder's matching rules should never effect another builder's
// behaviour.
type emoteEntitlementBuilder struct {
	// Emote entitlement matcher allows us to get an emoteID or emoteGroupID or multiple from an entitlement
	match emoteEntitlementMatcher

	// Optional Augment allows for a builder to add virtual emotes which do not exist in emotesDB
	augment emoteAugmentor

	// Optional CanEntitle allows us to make sure each of the emotes that we looked up by emoteID or emoteGroupID match an entitlement
	canEntitle func(Emote, []Entitlement) bool

	entitlementFilters []emoteEntitlementFilter

	/* entitlements are a collection of all matched emoteIDs or emoteGroupIDs, by match type, so that they can be linked
	* to entitlements later.
	*
	* If a rule returns an entitlement id of `123` and match type of `RequiresGroupEntitlement` the map
	* will look like:
	*
	*   {
	*      RequiresEmoteGroupEntitlement: {
	*        123: []string{...}
	*      }
	*   }
	 */
	entitlements map[MatchType][]entitlementMatch

	/* multiEntitlements are a collection of entitlements for a single shared key (like channelID) among them.
	* When we need to determine if a user has a multientitlement, we need to see whether they are entitled to each of the
	* entitlements associated with a the given shared key. In the case of Tier 2 / Tier 3 subscriber emotes, this means
	* having an entitlement for both Tier 1 + Tier 2 or for Tier 1 + Tier 2 + Tier 3 in a single channel.
	 */
	multiEntitlements map[string][]Entitlement

	// The name of this builder
	name string

	// A uuid to help debugging, gives a unique id per builder per request
	requestID ksuid.KSUID
}

// newEmoteEntitlementBuilder returns an empty builder with a default entitlement matcher (returns `true` for any matched entitlements).
func newEmoteEntitlementBuilder() *emoteEntitlementBuilder {
	return &emoteEntitlementBuilder{
		entitlements:      make(map[MatchType][]entitlementMatch),
		multiEntitlements: make(map[string][]Entitlement),
		// canEntitleEmote is provided a default here as most of the rules simply return true.
		canEntitle: func(emote Emote, entitlement []Entitlement) bool {
			return true
		},
	}
}

// match allows us to map an entitlement to an emote or a group of emotes.
func (b *emoteEntitlementBuilder) withMatch(f emoteEntitlementMatcher) *emoteEntitlementBuilder {
	b.match = f
	return b
}

func (b *emoteEntitlementBuilder) withFilters(filters ...emoteEntitlementFilter) *emoteEntitlementBuilder {
	b.entitlementFilters = append(b.entitlementFilters, filters...)
	return b
}

func (b *emoteEntitlementBuilder) withName(val string) *emoteEntitlementBuilder {
	b.name = val
	return b
}

func (b *emoteEntitlementBuilder) withEmoteAugmentation(f emoteAugmentor) *emoteEntitlementBuilder {
	b.augment = f
	return b
}

// entitle allows us to map an emote to an entitlement or a group of entitlements
func (b *emoteEntitlementBuilder) withCanEntitle(f func(emote Emote, entitlement []Entitlement) bool) *emoteEntitlementBuilder {
	b.canEntitle = f
	return b
}

func (b *emoteEntitlementBuilder) findEntitlementMatches(entitlement Entitlement) ([]string, []string) {
	if b.match == nil {
		return nil, nil
	}

	var emoteIDs []string
	var emoteGroupIDs []string

	// Entitlement must pass all filters for this builder to be able to be matched
	for _, filter := range b.entitlementFilters {
		if !filter(entitlement) {
			return nil, nil
		}
	}

	// Map this entitlement to one or more emotes or emote groups to look up
	match := b.match(entitlement)
	if match == nil {
		return nil, nil
	}

	// No entitlements were matched at the emote or emote group level
	if len(match.emoteIDs) == 0 && len(match.emoteGroupIDs) == 0 {
		return nil, nil
	}

	switch match.matchType {
	case RequiresEmoteEntitlement:
		b.entitlements[match.matchType] = append(b.entitlements[match.matchType], *match)
		emoteIDs = append(emoteIDs, match.emoteIDs...)
	case AlwaysEntitled, RequiresEmoteGroupEntitlement:
		// The only "always entitled" emote groups are Globals and default Smilies
		b.entitlements[match.matchType] = append(b.entitlements[match.matchType], *match)
		emoteGroupIDs = append(emoteGroupIDs, match.emoteGroupIDs...)
	case RequiresMultipleEmoteGroupEntitlements:
		b.multiEntitlements[match.entitlement.ChannelID] = append(b.multiEntitlements[match.entitlement.ChannelID], match.entitlement)
		emoteGroupIDs = append(emoteGroupIDs, match.emoteGroupIDs...)
	}

	if debugBuilder {
		log.WithFields(log.Fields{
			"request_id":    b.requestID.String(),
			"entitlement":   entitlement,
			"matchType":     match.matchType.String(),
			"emoteIDs":      emoteIDs,
			"emoteGroupIDs": emoteGroupIDs,
		}).Info("Builder: debug findEmoteEntitlementMatches")
	}

	return emoteIDs, emoteGroupIDs
}

func (b *emoteEntitlementBuilder) augmentEmotes(emotes ...Emote) []Emote {
	if b.augment == nil {
		return nil
	}

	augmentedEmotes := make([]Emote, 0, len(emotes))
	for _, emote := range emotes {
		for _, matches := range b.entitlements {
			for _, match := range matches {
				newEmotes := b.augment(emote, match.entitlement)

				if newEmotes == nil {
					// No augmentations, just append the existing emote
					augmentedEmotes = append(augmentedEmotes, emote)
				} else {
					augmentedEmotes = append(augmentedEmotes, newEmotes...)
				}
			}
		}

		for _, entitlements := range b.multiEntitlements {
			newEmotes := b.augment(emote, entitlements...)

			if newEmotes == nil {
				// No augmentations, just append the existing emote
				augmentedEmotes = append(augmentedEmotes, emote)
			} else {
				augmentedEmotes = append(augmentedEmotes, newEmotes...)
			}
		}
	}

	return augmentedEmotes
}

func (b *emoteEntitlementBuilder) matchEmotes(emotes ...Emote) []Emote {
	var entitledEmotes []Emote

	emotesByID := make(map[string]Emote)
	for _, e := range emotes {
		emotesByID[e.ID] = e
	}

	for matchType, matches := range b.entitlements {
		for _, match := range matches {
			switch matchType {
			case RequiresEmoteEntitlement, RequiresEmoteGroupEntitlement:
				entitledEmotes = append(entitledEmotes, b.matchRequiredEntitlement([]Entitlement{match.entitlement}, emotes)...)
			default:
				continue
			}
		}
	}

	for _, entitlements := range b.multiEntitlements {
		entitledEmotes = append(entitledEmotes, b.matchRequiredEntitlement(entitlements, emotes)...)
	}

	return entitledEmotes
}

func (b *emoteEntitlementBuilder) matchRequiredEntitlement(entitlements []Entitlement, emotes []Emote) []Emote {
	var entitledEmotes []Emote

	for _, emote := range emotes {
		if b.canEntitle(emote, entitlements) {
			entitledEmotes = append(entitledEmotes, emote)
		}
	}

	return entitledEmotes
}
