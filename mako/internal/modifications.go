package mako

import (
	mk "code.justin.tv/commerce/mako/twirp"
	mkd "code.justin.tv/commerce/mako/twirp/dashboard"
	"code.justin.tv/subs/paint/paintrpc"
)

// These are the types of emotes that should be modified during CreateEmote.
var EmoteTypesToModify = map[mkd.EmoteOrigin]bool{
	mkd.EmoteOrigin_Subscriptions: true,
}

var ModifiableEmotesByEntitlementSource = map[EntitlementSource]bool{
	SubscriptionsSource: true,
}

var (
	permanentModifiers = map[mk.Modification]bool{
		mk.Modification_BlackWhite:     true,
		mk.Modification_HorizontalFlip: true,
		mk.Modification_Sunglasses:     true,
		mk.Modification_Thinking:       true,
		mk.Modification_Squished:       true,
	}
)

func IsPermanentModifier(mod mk.Modification) bool {
	_, ok := permanentModifiers[mod]
	return ok
}

func ModifierCodesToModifiers(codes ...string) []mk.Modification {
	result := make([]mk.Modification, 0, len(codes))
	for _, code := range codes {
		result = append(result, ModifierCodeToModifier(code))
	}
	return result
}

func ModifierCodeToModifier(code string) mk.Modification {
	// NOTE: This assumes that the paint and mako modification lists are kept in sync
	if paintmod, ok := paintrpc.Modification_value[code]; ok {
		return mk.Modification(paintmod + 1)
	}
	return mk.Modification_ModificationNone
}

func ModifiersToModifierCodes(mods ...mk.Modification) []string {
	results := make([]string, 0, len(mods))
	for _, mod := range mods {
		results = append(results, GetModifierCode(mod))
	}
	return results
}

func GetModifierCode(mod mk.Modification) string {
	paintmod := int32(mod - 1)
	if code, ok := paintrpc.Modification_name[paintmod]; ok {
		return code
	}
	return ""
}

func GetModifierSuffix(mod mk.Modification) string {
	code := GetModifierCode(mod)
	if code != "" {
		return "_" + code
	}
	return ""
}

// IsModifiable is used to determine if an emote can have modifiers.
func IsModifiable(origin string, assetType EmoteAssetType) bool {
	emoteOrigin, ok := mkd.EmoteOrigin_value[origin]
	if !ok {
		return false
	}
	return ShouldModify(mkd.EmoteOrigin(emoteOrigin), EmoteAssetTypeAsDashboardTwirp(assetType))
}

// Returns true if an emote with the given origin and assetType should be modified during creation.
func ShouldModify(origin mkd.EmoteOrigin, assetType mkd.EmoteAssetType) bool {
	modifyForOrigin, ok := EmoteTypesToModify[origin]
	return ok && modifyForOrigin && assetType == mkd.EmoteAssetType_static
}
