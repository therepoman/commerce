package mako

import (
	mkd "code.justin.tv/commerce/mako/twirp/dashboard"

	"code.justin.tv/commerce/mako/config"
)

// globalEmoteEntitlements are always entitled automatically and do not support any modifications.
// Golden kappa is a special case that checks if the user is lucky enough to replace a Kappa emote with a golden version of the emote.
func globalEmoteEntitlements(ownerID string, dynamic config.DynamicConfigClient) *emoteEntitlementBuilder {
	return newEmoteEntitlementBuilder().
		withName("Globals").
		withFilters(onlyGlobalEntitlements, noSmiliesEntitlements, onlyGroupEmoteEntitlements).
		withMatch(func(entitlement Entitlement) *entitlementMatch {
			return &entitlementMatch{
				entitlement:   entitlement,
				emoteGroupIDs: []string{entitlement.ID},
				matchType:     AlwaysEntitled,
			}
		}).
		withEmoteAugmentation(func(emote Emote, entitlements ...Entitlement) []Emote {
			if emote.ID == KappaEmoteID {
				if IsLuckyGoldenKappa(ownerID, dynamic) {
					return []Emote{makeGoldenKappa(emote)}
				}
			}

			return []Emote{emote}
		}).
		withCanEntitle(func(emote Emote, entitlements []Entitlement) bool {
			// Global Emote Entitlements only require a single entitlement to ensure. Any more or fewer is a misconfiguration
			if len(entitlements) != 1 {
				return false
			}
			entitlement := entitlements[0]

			return emote.GroupID == entitlement.ID && emote.EmotesGroup == mkd.EmoteOrigin_Globals.String()
		})
}

// smilieEmoteEntitlements power a deadly feature called smilies. We load the chosen smilie group entitlement from Materia,
// and if we didn't find one, we add a default smilies entitlement before calling into the builder
func smilieEmoteEntitlements() *emoteEntitlementBuilder {
	return newEmoteEntitlementBuilder().
		withName("Smilies").
		withFilters(noGlobalEntitlements, onlySmiliesEntitlements).
		withMatch(func(entitlement Entitlement) *entitlementMatch {
			return &entitlementMatch{
				entitlement:   entitlement,
				emoteGroupIDs: []string{entitlement.Metadata.GroupID},
				matchType:     RequiresEmoteGroupEntitlement,
			}
		}).
		withCanEntitle(func(emote Emote, entitlements []Entitlement) bool {
			// Smilie Emote Entitlements only require a single entitlement to ensure. Any more or fewer is a misconfiguration
			if len(entitlements) != 1 {
				return false
			}
			entitlement := entitlements[0]

			return emote.GroupID == entitlement.Metadata.GroupID && isSmilie(emote.Code) && emote.EmotesGroup == mkd.EmoteOrigin_Smilies.String()
		})
}

// groupEmoteEntitlements are used for e.g., subscription tiers, bits, follower emotes, etc. These are base emotes and
// are entitled on the `GroupID` of the emote, thus if the group is entitled, any emote in that group will be.
func groupEmoteEntitlements() *emoteEntitlementBuilder {
	return newEmoteEntitlementBuilder().
		withName("Groups").
		withFilters(noGlobalEntitlements, noSmiliesEntitlements, onlyGroupEmoteEntitlements).
		withMatch(func(entitlement Entitlement) *entitlementMatch {
			return &entitlementMatch{
				entitlement:   entitlement,
				emoteGroupIDs: []string{entitlement.ID},
				matchType:     RequiresEmoteGroupEntitlement,
			}
		}).
		withCanEntitle(func(emote Emote, entitlements []Entitlement) bool {
			// Group Emote Entitlements only require a single entitlement to ensure. Any more or fewer is a misconfiguration
			if len(entitlements) != 1 {
				return false
			}
			entitlement := entitlements[0]

			return emote.GroupID == entitlement.ID
		})
}

// singleEmoteEntitlements are entitled by `emote.ID` and have no modifications present.
func singleEmoteEntitlements(SpecialEventOwnerIDs map[string]bool) *emoteEntitlementBuilder {
	return newEmoteEntitlementBuilder().
		withName("Single Emotes").
		withFilters(noGlobalEntitlements, noSmiliesEntitlements, noModifiedEmoteEntitlementIDs, onlySingleEmoteEntitlements).
		withMatch(func(entitlement Entitlement) *entitlementMatch {
			return &entitlementMatch{
				entitlement: entitlement,
				emoteIDs:    []string{entitlement.ID},
				matchType:   RequiresEmoteEntitlement,
			}
		}).
		withEmoteAugmentation(func(emote Emote, entitlements ...Entitlement) []Emote {
			// Single Emote Entitlements only require a single entitlement to ensure. Any more or fewer is a misconfiguration
			if len(entitlements) != 1 {
				return nil
			}
			entitlement := entitlements[0]

			// If this emote was entitled as part of a special event (megacommerce, hype train, drops, etc) which
			// is configured to have the ownerID of the emote overwritten, then we should augment that emote by overwritting
			// the ownerID here.
			if emote.ID == entitlement.ID && SpecialEventOwnerIDs[entitlement.ChannelID] {
				modifiedEmote := emote
				modifiedEmote.OwnerID = entitlement.ChannelID
				return []Emote{modifiedEmote}
			}

			return nil
		}).
		withCanEntitle(func(emote Emote, entitlements []Entitlement) bool {
			// Single Emote Entitlements only require a single entitlement to ensure. Any more or fewer is a misconfiguration
			if len(entitlements) != 1 {
				return false
			}
			entitlement := entitlements[0]

			return emote.ID == entitlement.ID
		})
}

// singleModifiedEmoteEntitlements are entitled by an emoteID with a specific modified concatenated on as a suffix. An example of
// this is in channel points modified emotes
func singleModifiedEmoteEntitlements(CommunityPointsOwnerID string, CommunityPointsEmoteSetID string) *emoteEntitlementBuilder {
	return newEmoteEntitlementBuilder().
		withName("Single Modified Emotes").
		withFilters(noGlobalEntitlements, noSmiliesEntitlements, onlyModifiedEmoteEntitlementIDs, onlySingleEmoteEntitlements).
		withMatch(func(entitlement Entitlement) *entitlementMatch {
			baseEntitlementID, _ := NormalizeEntitlementID(entitlement.ID)

			return &entitlementMatch{
				entitlement: entitlement,
				emoteIDs:    []string{baseEntitlementID},
				matchType:   RequiresEmoteEntitlement,
			}
		}).
		withEmoteAugmentation(func(emote Emote, entitlements ...Entitlement) []Emote {
			// Single Modified Emote Entitlements only require a single entitlement to ensure. Any more or fewer is a misconfiguration
			if len(entitlements) != 1 {
				return nil
			}

			// Skip if emote is not modifiable
			if !IsModifiable(emote.EmotesGroup, emote.AssetType) {
				return nil
			}

			entitlement := entitlements[0]

			baseEntitlementID, modifiers := NormalizeEntitlementID(entitlement.ID)

			if baseEntitlementID != emote.ID || modifiers == nil {
				return []Emote{emote}
			}

			modifiedEmotes := make([]Emote, 0, len(modifiers))
			for _, modifier := range modifiers {
				ownerID := emote.OwnerID
				groupID := emote.GroupID
				if entitlement.Source.toEmoteType() == mkd.EmoteOrigin_ChannelPoints.String() {
					ownerID = CommunityPointsOwnerID
					groupID = CommunityPointsEmoteSetID
				}
				modifiedEmote := MakeModifiedEmote(emote, modifier, entitlement.Source.toEmoteType(), ownerID, groupID)
				modifiedEmotes = append(modifiedEmotes, modifiedEmote)
			}

			return append(modifiedEmotes, emote)
		}).
		withCanEntitle(func(emote Emote, entitlements []Entitlement) bool {
			// Single Modified Emote Entitlements only require a single entitlement to ensure. Any more or fewer is a misconfiguration
			if len(entitlements) != 1 {
				return false
			}
			entitlement := entitlements[0]

			return emote.ID == entitlement.ID
		})
}

/* T2/T3 Modified Emote Entitlements refer to the multi-entitlement Tier 2 and Tier 3 Subscription Emotes product.
* When a user is subscribed at Tier 2 or higher in a channel, they are entitled to Tier 1 and Tier 2 (and/or Tier 3)
* emotes. Those regular subscription emotes are handled via the groupEmoteEntitlements builder. They are also entitled
* to a set of modifiers for all emotes in that channel. Rather than constructing individual modified permutations of emotes,
* like we would with the single modified emote entitlements builder, we instead opt for storing the set of available
* modifiers on the emotes themselves. In doing so, we keep the response size smaller and keep the emote picker less cluttered.
 */
func t2t3ModifiedEmoteEntitlements(modifierGroups []EmoteModifierGroup) *emoteEntitlementBuilder {
	return newEmoteEntitlementBuilder().
		withName("T2/T3 Modified Emotes").
		withFilters(noGlobalEntitlements, noSmiliesEntitlements, onlyGroupEmoteEntitlements, onlySubscriptionsEntitlements).
		withMatch(func(entitlement Entitlement) *entitlementMatch {
			// If one of the groups' SourceEmoteGroupIDs matches this emote_group entitlement's ID, then add all of the
			// source emote groupIDs to the match
			for _, modifierGroup := range modifierGroups {
				for _, groupID := range modifierGroup.SourceEmoteGroupIDs {
					if groupID == entitlement.ID {
						return &entitlementMatch{
							entitlement:   entitlement,
							emoteGroupIDs: modifierGroup.SourceEmoteGroupIDs,
							matchType:     RequiresMultipleEmoteGroupEntitlements,
						}
					}
				}
			}

			return nil
		}).
		withEmoteAugmentation(func(emote Emote, entitlements ...Entitlement) []Emote {
			if modifierGroups == nil {
				return nil
			}

			// Skip if emote is not modifiable
			if !IsModifiable(emote.EmotesGroup, emote.AssetType) {
				return nil
			}

			entitlementGroupIDs := make([]string, 0, len(entitlements))
			for _, entitlement := range entitlements {
				// We've already filtered before this, so we know entitlement.ID is a groupID
				entitlementGroupIDs = append(entitlementGroupIDs, entitlement.ID)
			}

			modifiedEmote := emote

			// If all of the source emote modifier groupIDs match a groupID from the entitlements, then add all modifierGroups
			// in the emote modifier group to that emote to show that that emote is entitled via T2/T3
			for _, modifierGroup := range modifierGroups {
				// T2/T3 multi-entitlements are only entitled if the user has all the corresponding SourceEmoteGroupIDs
				// entitled, so if the number of groupIDs from each do not match, we can skip to the next modifierGroup
				if len(modifierGroup.SourceEmoteGroupIDs) > len(entitlementGroupIDs) {
					continue
				}

				// Check for a 1:1 mapping of SourceEmoteGroupIDs and EntitlementGroupIDs
				entitlementGroupIDLookup := make(map[string]bool)
				for _, entitlementGroupID := range entitlementGroupIDs {
					entitlementGroupIDLookup[entitlementGroupID] = true
				}

				for _, sourceEmoteGroupID := range modifierGroup.SourceEmoteGroupIDs {
					found := entitlementGroupIDLookup[sourceEmoteGroupID]

					// Every sourceEmoteGroupID must match an entitlementGroupID. If any fails, then no modifiers can be
					// augmented to this emote.
					if !found {
						return nil
					}
				}

				// At this point, we know the multi-entitlement is entitled properly, so we just need to add the
				// corresponding modifiers to the emotes
				modifiedEmote.Modifiers = append(modifiedEmote.Modifiers, modifierGroup.Modifiers...)
			}

			return []Emote{modifiedEmote}
		}).
		withCanEntitle(func(emote Emote, entitlements []Entitlement) bool {
			// T2/T3 modified emote entitlements require being entitled to either T1+T2 or T1+T2+T3
			if len(entitlements) <= 1 {
				return false
			}

			// T2/T3 modified emotes have modifiers specified on the emote
			if emote.Modifiers == nil {
				return false
			}

			// We already checked in withEmoteAugmentation that all of the multi-entitlements were present,
			// so this is just a final double check that this emote has a group entitlement which matches its groupID
			for _, entitlement := range entitlements {
				if entitlement.ID == emote.GroupID {
					return true
				}
			}

			return false
		})
}

// Two factor emotes are entitled when a user sets up 2FA on their account. However, the implementation is odd, so we have
// to handle the hacky nature in a specific one-off builder rather than using the group emote entitlements builder.
func twoFactorEmoteEntitlements() *emoteEntitlementBuilder {
	return newEmoteEntitlementBuilder().
		withName("2FA Emotes").
		withFilters(noGlobalEntitlements, noSmiliesEntitlements, onlyTwoFactorEntitlements).
		withMatch(func(entitlement Entitlement) *entitlementMatch {
			return &entitlementMatch{
				entitlement:   entitlement,
				emoteGroupIDs: []string{TwoFactorEmoteGroupID},

				// 2FA emotes are entitled in materia with a hack that sets the type of entitlement
				// to EMOTE and sets data in the metadata column even though this is a
				// group of emotes being entitled
				matchType: RequiresEmoteGroupEntitlement,
			}
		}).
		withCanEntitle(func(emote Emote, entitlements []Entitlement) bool {
			// Two Factor Emote Entitlements only require a single entitlement to ensure. Any more or fewer is a misconfiguration
			if len(entitlements) != 1 {
				return false
			}

			return emote.GroupID == TwoFactorEmoteGroupID
		})
}

func onlyGlobalEntitlements(entitlement Entitlement) bool {
	return entitlement.Source == GlobalsSource && entitlement.ID == GlobalEmoteGroupID
}

func noGlobalEntitlements(entitlement Entitlement) bool {
	return !onlyGlobalEntitlements(entitlement)
}

func onlySmiliesEntitlements(entitlement Entitlement) bool {
	return entitlement.Source == SmiliesSource && entitlement.ID == SmilieGroupID
}

func noSmiliesEntitlements(entitlement Entitlement) bool {
	return !onlySmiliesEntitlements(entitlement)
}

func onlyModifiedEmoteEntitlementIDs(entitlement Entitlement) bool {
	_, modifiers := NormalizeEntitlementID(entitlement.ID)

	return len(modifiers) > 0
}

func noModifiedEmoteEntitlementIDs(entitlement Entitlement) bool {
	return !onlyModifiedEmoteEntitlementIDs(entitlement)
}

// TODO: Ideally we would not depend on feature-related info to filter
func onlySubscriptionsEntitlements(entitlement Entitlement) bool {
	return entitlement.Source == SubscriptionsSource
}

func onlySingleEmoteEntitlements(entitlement Entitlement) bool {
	return entitlement.Type == EmoteEntitlement
}

func onlyGroupEmoteEntitlements(entitlement Entitlement) bool {
	return entitlement.Type == EmoteGroupEntitlement
}

func onlyTwoFactorEntitlements(entitlement Entitlement) bool {
	// 2FA emotes are entitled in materia with a hack that sets the type of
	// entitlement to EMOTE and sets data in the metadata column even though this is a
	// group of emotes being entitled
	return entitlement.OriginID == TwoFactorOriginID.String()
}
