package mako

import (
	mk "code.justin.tv/commerce/mako/twirp"
	mkd "code.justin.tv/commerce/mako/twirp/dashboard"
)

func EmoteAssetTypeAsTwirp(assetType EmoteAssetType) mk.EmoteAssetType {
	twirpAssetType := mk.EmoteAssetType_static
	if assetType == AnimatedAssetType {
		twirpAssetType = mk.EmoteAssetType_animated
	}
	return twirpAssetType
}

func EmoteAssetTypeAsDashboardTwirp(assetType EmoteAssetType) mkd.EmoteAssetType {
	twirpAssetType := mkd.EmoteAssetType_static
	if assetType == AnimatedAssetType {
		twirpAssetType = mkd.EmoteAssetType_animated
	}
	return twirpAssetType
}
