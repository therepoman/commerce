package mako

import (
	"context"
	"fmt"

	mkd "code.justin.tv/commerce/mako/twirp/dashboard"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/mako/config"
)

// groupEmotes are used for e.g., subscription tiers, bits. These are base emotes and are entitled
// on the `GroupID` of the emote, thus any emote in that group, if entitled, will be valid.
func groupEmotes() *emoteBuilder {
	return newEmoteBuilder().
		withName("Groups").
		withFilters(noGlobals, noSmilies, noModifiers, noFollowerEmotes).
		withMatch(func(emote Emote, modifier string) *match {
			return &match{
				emote:          emote,
				entitlementIDs: []string{emote.GroupID},
				matchType:      RequiresEmoteGroupEntitlement,
			}
		})
}

// globalEmotes are always entitled automatically and do not support any modifications.
// Golden kappa is a special case that checks if the user is lucky enough to replace a Kappa emote with a golden version of the emote.
func globalEmotes(ownerID string, dynamic config.DynamicConfigClient) *emoteBuilder {
	return newEmoteBuilder().
		withName("Globals").
		withFilters(onlyGlobals, noModifiers).
		withMatch(func(emote Emote, modifier string) *match {
			if emote.Code == "Kappa" && IsLuckyGoldenKappa(ownerID, dynamic) {
				return &match{
					emote:     makeGoldenKappa(emote),
					matchType: AlwaysEntitled,
				}
			}

			return &match{
				emote:     emote,
				matchType: AlwaysEntitled,
			}
		})
}

// singleEmotes are entitled by `emote.ID` and have no modifications present.
func singleEmotes() *emoteBuilder {
	return newEmoteBuilder().
		withName("Single Emotes").
		withFilters(noGlobals, noSmilies, noModifiers, noFollowerEmotes).
		withMatch(func(emote Emote, modifier string) *match {
			return &match{
				emote:          emote,
				entitlementIDs: []string{emote.ID},
				matchType:      RequiresEmoteEntitlement,
			}
		})
}

// singleModifiedEmotes are the modified variant and check if a single emote such as `Abc` has
// a modifier such sa `Abc_BW`. Single emote modifiers are entitled via `emote.ID + "_" + modifier`
// where `modifier` can be `BW`, `SG`, etc...
func singleModifiedEmotes() *emoteBuilder {
	return newEmoteBuilder().
		withName("Single Modified Emotes").
		withFilters(noGlobals, noSmilies, onlyModifiers, noFollowerEmotes).
		withMatch(func(emote Emote, modifier string) *match {
			emote.ID += "_" + modifier
			emote.Code += "_" + modifier

			return &match{
				emote:          emote,
				entitlementIDs: []string{emote.ID},
				matchType:      RequiresEmoteEntitlement,
			}
		})
}

// smilieEmotes power a deadly feature called smilies. Anytime a smilie is present a special emote group
// `SmilieGroupID` must be loaded.
func smilieEmotes() *emoteBuilder {
	return newEmoteBuilder().
		withName("Smilies").
		withFilters(onlySmilies, noModifiers).
		withMatch(func(emote Emote, modifier string) *match {
			return &match{
				emote:          emote,
				entitlementIDs: []string{SmilieGroupID},
				matchType:      RequiresEmoteGroupEntitlement,
			}
		}).
		withCanEntitle(func(emote Emote, entitlement Entitlement) bool {
			if entitlement.ID == SmilieGroupID && emote.GroupID == entitlement.Metadata.GroupID {
				return true
			}

			return false
		})
}

func t2t3ModifiedEmotes(ctx context.Context, db EmoteModifiersDB) *emoteBuilder {
	return newEmoteBuilder().
		withName("T2/T3 Modified Emotes").
		parallel().
		withFilters(noGlobals, noSmilies, onlyModifiers, noAnimated, noFollowerEmotes).
		withMatch(func(emote Emote, modifier string) *match {
			groups, err := db.ReadByOwnerIDs(ctx, emote.OwnerID)
			if err != nil {
				logrus.WithField("ownerID", emote.OwnerID).
					WithError(err).
					Error("could not retrieve emote modifier groups for given ownerID")

				return nil
			}

			for _, group := range groups {
				for _, m := range group.Modifiers {
					if m == modifier {
						emote.Code = fmt.Sprintf("%s_%s", emote.Code, modifier)
						emote.ID = fmt.Sprintf("%s_%s", emote.ID, modifier)

						return &match{
							emote:          emote,
							entitlementIDs: append(group.SourceEmoteGroupIDs, emote.GroupID),
							matchType:      RequiresMultipleEmoteGroupEntitlements,
						}
					}
				}
			}

			return nil
		})
}

func followerEmotes(userID string, channelID string) *emoteBuilder {
	return newEmoteBuilder().
		withName("Follower Emotes").
		withFilters(onlyFollowerEmotes, thisChannelOnly(channelID), noModifiers, noAnimated).
		withMatch(func(emote Emote, modifier string) *match {
			// In order for us to determine whether a user follows a channel, both need to be populated
			if userID == "" || channelID == "" {
				return nil
			}

			return &match{
				emote:          emote,
				entitlementIDs: []string{GenerateFollowerEntitlementID(emote.GroupID, channelID)},

				// Follower emote entitlements depend on a user-channel follow link, which are far too numerous to store
				// in our usual EntitlementsDB. Instead, we look up whether a user follows a channel per request as a
				// way to tell whether the user should be entitled to that channel's follower emote set
				matchType: RequiresEmoteGroupEntitlement,
			}
		}).
		withCanEntitle(func(emote Emote, entitlement Entitlement) bool {
			return emote.GroupID == entitlement.Metadata.GroupID
		})
}

func twoFactorEmotes() *emoteBuilder {
	return newEmoteBuilder().
		withName("2FA Emotes").
		withFilters(onlyTwoFactor, noModifiers).
		withMatch(func(emote Emote, modifier string) *match {
			return &match{
				emote:          emote,
				entitlementIDs: []string{TwoFactorEmoteGroupID},

				// the matchType is RequiresEmoteEntitlement because 2FA emotes
				// are entitled in materia with a hack that sets the type of entitlement
				// to EMOTE and sets data in the metadata column even though this is a
				// group of emotes being entitled
				matchType: RequiresEmoteEntitlement,
			}
		})
}

func noGlobals(emote Emote, modifier string) bool {
	return emote.GroupID != GlobalEmoteGroupID
}

func onlyGlobals(emote Emote, modifier string) bool {
	return emote.GroupID == GlobalEmoteGroupID
}

func noSmilies(emote Emote, modifier string) bool {
	return !isSmilie(emote.Code)
}

func onlySmilies(emote Emote, modifier string) bool {
	return isSmilie(emote.Code)
}

func noModifiers(emote Emote, modifier string) bool {
	return modifier == ""
}

func onlyModifiers(emote Emote, modifier string) bool {
	return modifier != "" && IsModifiable(emote.EmotesGroup, emote.AssetType)
}

func onlyFollowerEmotes(emote Emote, modifier string) bool {
	return emote.EmotesGroup == mkd.EmoteOrigin_Follower.String()
}

func noFollowerEmotes(emote Emote, modifier string) bool {
	return !onlyFollowerEmotes(emote, modifier)
}

func thisChannelOnly(channelID string) func(emote Emote, modifier string) bool {
	return func(emote Emote, modifier string) bool {
		return emote.OwnerID == channelID
	}
}

func onlyTwoFactor(emote Emote, modifier string) bool {
	return isTwoFactorEmote(emote)
}

func noAnimated(emote Emote, modifier string) bool {
	return emote.AssetType != AnimatedAssetType
}

func GenerateFollowerEntitlementID(groupID string, channelID string) string {
	return FollowerEntitlementIDPrefix + FollowerEntitlementSeparator + groupID + FollowerEntitlementSeparator + channelID
}
