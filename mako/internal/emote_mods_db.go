package mako

import (
	"context"

	mk "code.justin.tv/commerce/mako/twirp"
)

type EmoteModifierGroup struct {
	ID                  string
	OwnerID             string
	Modifiers           []string
	SourceEmoteGroupIDs []string
}

// This is the public interface for the emote modifiers DB
type EmoteModifiersDB interface {
	// Creates emote modifier groups. This will generate an ID for the groups, and return it in the resulting object.
	CreateGroups(ctx context.Context, groups ...EmoteModifierGroup) ([]EmoteModifierGroup, error)
	// Update the given modifier groups. Only the values provided in the request will be updated.
	UpdateGroups(ctx context.Context, groups ...EmoteModifierGroup) ([]EmoteModifierGroup, error)
	// Look up and return the groups with the given IDs
	ReadByIDs(ctx context.Context, ids ...string) ([]EmoteModifierGroup, error)
	// Look up and return the groups with the given ownerIDs
	ReadByOwnerIDs(ctx context.Context, ownerIDs ...string) ([]EmoteModifierGroup, error)
	// Delete groups with the given IDs
	DeleteByIDs(ctx context.Context, ids ...string) error
	// Reads all entitled emote modifier groups for a specific user.
	ReadEntitledGroupsForUser(ctx context.Context, userID string) ([]EmoteModifierGroup, error)
	// Writes all entitled emote modifier groups for a specific user.
	WriteEntitledGroupsForUser(ctx context.Context, userID string, groups ...EmoteModifierGroup) error
	// Invalidate entitled emote modifier groups for a specific user.
	InvalidateEntitledGroupsCacheForUser(userID string) error
}

func (group EmoteModifierGroup) ToTwirp() *mk.EmoteModifierGroup {
	return &mk.EmoteModifierGroup{
		Id:                  group.ID,
		OwnerId:             group.OwnerID,
		Modifiers:           ModifierCodesToModifiers(group.Modifiers...),
		SourceEmoteGroupIds: group.SourceEmoteGroupIDs,
	}
}

func EmoteModifierGroupsToTwirp(groups ...EmoteModifierGroup) []*mk.EmoteModifierGroup {
	result := make([]*mk.EmoteModifierGroup, 0, len(groups))
	for _, group := range groups {
		result = append(result, group.ToTwirp())
	}
	return result
}

func TwirpToEmoteModifierGroup(group *mk.EmoteModifierGroup) EmoteModifierGroup {
	return EmoteModifierGroup{
		ID:                  group.Id,
		OwnerID:             group.OwnerId,
		Modifiers:           ModifiersToModifierCodes(group.Modifiers...),
		SourceEmoteGroupIDs: group.SourceEmoteGroupIds,
	}
}
