package mako

import (
	"context"
	"strings"
	"sync"
	"time"

	"github.com/pkg/errors"

	"github.com/hashicorp/go-multierror"

	"golang.org/x/sync/errgroup"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/mako/config"
	mk "code.justin.tv/commerce/mako/twirp"
	"github.com/segmentio/ksuid"
)

type GetUserChannelEmotesParams struct {
	EmoteDB                   EmoteDB
	EntitlementDB             EntitlementDB
	EmoteModifiersDB          EmoteModifiersDB
	Dynamic                   config.DynamicConfigClient
	SpecialEventOwnerIDs      map[string]bool
	CommunityPointsOwnerID    string
	CommunityPointsEmoteSetID string
	UserID                    string
	ChannelID                 string
}

func GetUserChannelEmotes(ctx context.Context, params GetUserChannelEmotesParams) ([]Emote, error) {
	entitledEmoteIDs := make([]string, 0)
	entitledEmoteGroupIDs := make([]string, 0)

	// 1. Get all entitlements and modifiers in parallel from all entitlement sources
	entitlements, modifiers, err := getUserEntitlementsInParallel(ctx, params)
	if err != nil {
		// We don't return an error here because we can still optimistically return a partially successful response
		log.WithFields(log.Fields{
			"userID":    params.UserID,
			"channelID": params.ChannelID,
		}).WithError(err).Error("error getting user entitlements in parallel for GetUserChannelEmotes")
	}

	// 2. Add defaults to the entitlements list, since they're not saved in Materia
	entitlements = addEntitlementsNotStoredInEntitlementsDB(entitlements, globalsEntitlement, defaultSmiliesEntitlement)

	// TODO: this cache priming logic should ideally live inside EmoteModifiersDB, not here
	// If no modifiers were returned, then the redis cache is probably not primed, so fall back to reading from dynamoDB
	if modifiers == nil {
		channelIDs := make([]string, 0, len(entitlements))
		for _, entitlement := range entitlements {
			// Only get modifiers for non-special event channels
			// Only get modifiers for supported modifier sources
			if params.SpecialEventOwnerIDs[entitlement.ChannelID] || !ModifiableEmotesByEntitlementSource[entitlement.Source] {
				continue
			}

			channelIDs = append(channelIDs, entitlement.ChannelID)
		}

		// Only call into EmoteModifiersDB if there are modifiers to look up by channelIDs
		if len(channelIDs) > 0 {
			var err error
			modifiers, err = params.EmoteModifiersDB.ReadByOwnerIDs(ctx, channelIDs...)
			if err != nil {
				log.WithField("channelIDs", channelIDs).WithError(err).Error("could not retrieve emote modifier groups for given channelIDs")
			} else {
				modifiers = dedupeEmoteModifierGroupsByModifierIDs(modifiers)

				// We need to copy the groups to avoid changing anything in the underlying struct while async writing to redis
				modifierGroupsCopy := modifiers
				go func() {
					ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
					defer cancel()
					if err := params.EmoteModifiersDB.WriteEntitledGroupsForUser(ctx, params.UserID, modifierGroupsCopy...); err != nil {
						log.WithField("userID", params.UserID).WithError(err).Error("could not write emote modifier groups for given user")
					}
				}()
			}
		}
	}

	var builders = []*emoteEntitlementBuilder{
		globalEmoteEntitlements(params.UserID, params.Dynamic),
		smilieEmoteEntitlements(),
		groupEmoteEntitlements(),
		singleEmoteEntitlements(params.SpecialEventOwnerIDs),
		singleModifiedEmoteEntitlements(params.CommunityPointsOwnerID, params.CommunityPointsEmoteSetID),
		t2t3ModifiedEmoteEntitlements(modifiers),
		twoFactorEmoteEntitlements(),
	}

	// 4. Apply "some builder method" for all builder rules to get a list of emote ids and emote group entitlements to be fetched from the EmoteDB.
	runBuilder := func(builder *emoteEntitlementBuilder, entitlement Entitlement) {
		builderEntitledEmoteIDs, builderEntitledEmoteGroupIDs := builder.findEntitlementMatches(entitlement)

		if len(builderEntitledEmoteIDs) > 0 {
			entitledEmoteIDs = append(entitledEmoteIDs, builderEntitledEmoteIDs...)
		}
		if len(builderEntitledEmoteGroupIDs) > 0 {
			entitledEmoteGroupIDs = append(entitledEmoteGroupIDs, builderEntitledEmoteGroupIDs...)
		}
	}

	for _, entitlement := range entitlements {
		entitlement := entitlement

		for _, builder := range builders {
			builder := builder

			// Generate a new request id so we can trace builder operations
			var err error
			builder.requestID, err = ksuid.NewRandom()
			if err != nil {
				builder.requestID = ksuid.Nil
			}

			runBuilder(builder, entitlement)
		}
	}

	entitledEmoteIDs = dedupeIDs(entitledEmoteIDs...)
	entitledEmoteGroupIDs = dedupeIDs(entitledEmoteGroupIDs...)

	var foundEmotes = make([]Emote, 0)
	// 5. Fetch emotes with EmoteDB.ReadByIDs using the output from 4
	emotesByID, err := params.EmoteDB.ReadByIDs(ctx, entitledEmoteIDs...)
	if err != nil {
		log.WithError(err).WithFields(log.Fields{
			"channelID": params.ChannelID,
			"userID":    params.UserID,
		}).Error("GetUserChannelEmotes: failed to read emotes by IDs")
		return nil, err
	}
	foundEmotes = append(foundEmotes, onlyActiveFilter(emotesByID)...)

	// 6. Fetch emotes with EmoteDB.ReadByGroupIDs using the output from 4
	emotesByGroupID, err := params.EmoteDB.ReadByGroupIDs(ctx, entitledEmoteGroupIDs...)
	if err != nil {
		log.WithError(err).WithFields(log.Fields{
			"channelID": params.ChannelID,
			"userID":    params.UserID,
		}).Error("GetUserChannelEmotes: failed to read emotes by groupIDs")
		return nil, err
	}
	foundEmotes = append(foundEmotes, onlyActiveFilter(emotesByGroupID)...)

	// 7. Augment/Replace certain emotes (i.e. Kappa, tooSmart) with others (i.e. GoldenKappa, tooSmart_HF)
	var augmentedEmotes = make([]Emote, 0, len(foundEmotes))
	for _, builder := range builders {
		newEmotes := builder.augmentEmotes(foundEmotes...)
		if len(newEmotes) > 0 {
			augmentedEmotes = append(augmentedEmotes, newEmotes...)
		}
	}

	//8. Make sure that all fetched emotes match back to one or more builder's matches
	var matchedEmotes = make([]Emote, 0, len(augmentedEmotes))
	for _, builder := range builders {
		newEmotes := builder.matchEmotes(augmentedEmotes...)
		if len(newEmotes) > 0 {
			matchedEmotes = append(matchedEmotes, newEmotes...)
		}
	}

	// 9. Dedupe Emotes, with a preference for keeping the most augmented version of the emote
	emotes := dedupeEmotesByCodes(matchedEmotes)

	return emotes, nil
}

func addEntitlementsNotStoredInEntitlementsDB(entitlements []Entitlement, adders ...func([]Entitlement) []Entitlement) []Entitlement {
	for _, adder := range adders {
		entitlements = adder(entitlements)
	}

	return entitlements
}

func globalsEntitlement(entitlements []Entitlement) []Entitlement {
	return append(entitlements, Entitlement{
		ID:       GlobalEmoteGroupID,
		Source:   GlobalsSource,
		Type:     EmoteGroupEntitlement,
		OriginID: GlobalsOriginID.String(),
	})
}

// Every user is entitled to a base set of default smilies. However, certain users can choose a different set, which gets
// stored in Materia. Here we look for whether the user needs to be given the default smilies group entitlement or not
func defaultSmiliesEntitlement(entitlements []Entitlement) []Entitlement {
	hasSpecificSmilies := false
	for _, entitlement := range entitlements {
		if entitlement.Source == SmiliesSource {
			hasSpecificSmilies = true
		}
	}

	if hasSpecificSmilies {
		return entitlements
	}

	return append(entitlements, Entitlement{
		ID:       RobotsSmilieGroupID.ToString(),
		Source:   SmiliesSource,
		Type:     EmoteGroupEntitlement,
		OriginID: SmilieGroupID,
	})
}

func onlyActiveFilter(emotes []Emote) []Emote {
	var filteredEmotes = make([]Emote, 0, len(emotes))
	for _, emote := range emotes {
		if emote.State == Active {
			filteredEmotes = append(filteredEmotes, emote)
		}
	}
	return filteredEmotes
}

/* Entitlement IDs have historically been numbers because they represented indexes in SiteDB.
*  Today, entitlementIDs can be emoteIDs or groupIDs of regular or modified emotes, among other options.
*  That means that these IDs could be one of any of the following forms:
*    1. 303975434 (an emote ID like would be found in siteDB)
*    2. 304423307_HF (an emote ID like would be found in siteDB + modifier suffix)
*    3. emotesv2_11992c55c9974343b3f288ceced6747b (a string emote ID like we use with newer emotes)
*    4. emotesv2_11992c55c9974343b3f288ceced6747b_HF (a string emote ID like we use with newer emotes + modifier suffix)
*    5. smilie (a hardcoded string for a special, one-off kind of feature)
*    6. 123456 (an emote group ID)
 */
func NormalizeEntitlementID(id string) (baseID string, modifiers []string) {
	idParts := strings.Split(id, "_")
	isValidEmotesV2EmoteId := IsValidEmotesV2EmoteId(id)
	if isValidEmotesV2EmoteId && len(idParts) > 2 {
		// ID looks like emotesV2_uuid_BW
		baseID = strings.Join([]string{idParts[0], idParts[1]}, "_")

		modifier := idParts[len(idParts)-1]
		allowedModifier := ModifierCodeToModifier(modifier)
		if allowedModifier != mk.Modification_ModificationNone {
			modifiers = []string{modifier}
		}
	} else if isValidEmotesV2EmoteId {
		// ID looks like emotesV2_uuid
		baseID = strings.Join([]string{idParts[0], idParts[1]}, "_")
	} else if len(idParts) > 1 {
		// ID looks like 123456789_BW
		baseID = idParts[0]

		modifier := idParts[len(idParts)-1]
		allowedModifier := ModifierCodeToModifier(modifier)
		if allowedModifier != mk.Modification_ModificationNone {
			modifiers = []string{modifier}
		}
	} else {
		// ID looks like 123456789 or smilie
		return id, nil
	}

	return baseID, modifiers
}

func dedupeEmoteModifierGroupsByModifierIDs(emoteModifierGroup []EmoteModifierGroup) []EmoteModifierGroup {
	modifierGroupByIDMap := make(map[string]EmoteModifierGroup)
	for _, group := range emoteModifierGroup {
		modifierGroupByIDMap[group.ID] = group
	}

	dedupedModifierGroups := make([]EmoteModifierGroup, 0, len(modifierGroupByIDMap))
	for _, group := range modifierGroupByIDMap {
		dedupedModifierGroups = append(dedupedModifierGroups, group)
	}

	return dedupedModifierGroups
}

func getUserEntitlementsInParallel(ctx context.Context, params GetUserChannelEmotesParams) ([]Entitlement, []EmoteModifierGroup, error) {
	var eg errgroup.Group
	var mu sync.Mutex
	var errMu sync.Mutex
	var errs multierror.Error

	var allEntitlements []Entitlement
	var allModifiers []EmoteModifierGroup

	// Fetch all emote entitlements from Materia, Mako.Entitlements dynamoDB, and Following service using the user id and channel id
	eg.Go(
		func() error {
			entitlements, err := params.EntitlementDB.ReadByOwnerID(ctx, params.UserID, params.ChannelID)
			// We specifically don't propagate errors up, since we may still be able to deliver a partial payload like globals + smilies
			if err != nil {
				err = errors.Wrap(err, "failed to read entitlements during GetUserChannelEmotes")
				errMu.Lock()
				errs.Errors = append(errs.Errors, err)
				errMu.Unlock()
			}

			mu.Lock()
			allEntitlements = append(allEntitlements, entitlements...)
			mu.Unlock()

			return nil
		},
	)

	// Get modifiers for all related channels from the cache
	eg.Go(
		func() error {
			modifiers, err := params.EmoteModifiersDB.ReadEntitledGroupsForUser(ctx, params.UserID)
			// We specifically don't propagate errors up, since we may still be able to deliver a partial payload like globals + smilies
			if err != nil {
				err = errors.Wrap(err, "could not retrieve entitled emote modifier groups for given user, falling back on fetching by owner IDs")
				errMu.Lock()
				errs.Errors = append(errs.Errors, err)
				errMu.Unlock()
			}

			mu.Lock()
			allModifiers = append(allModifiers, modifiers...)
			mu.Unlock()

			return nil
		},
	)

	// We specifically don't propagate the errors up because we want to be able to handle an optimistic payload for partial downstream failures
	err := eg.Wait()
	if err != nil {
		err = errors.Wrap(err, "timed out waiting to get entitlements and modifiers for GetUserChannelEmotes")
		errs.Errors = append(errs.Errors, err)
	}

	return allEntitlements, allModifiers, errs.ErrorOrNil()
}
