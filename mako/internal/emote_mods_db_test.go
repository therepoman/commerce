package mako

import (
	"context"
	"testing"
	"time"

	mk "code.justin.tv/commerce/mako/twirp"
	"github.com/segmentio/ksuid"
	"github.com/stretchr/testify/assert"
)

func TestEmoteModifierGroup_ToTwirp(t *testing.T) {
	tests := []struct {
		scenario string
		fn       func(ctx context.Context, t *testing.T)
	}{
		{
			scenario: "Should return correct modifier code",
			fn: func(ctx context.Context, t *testing.T) {
				// GIVEN: emote mod group
				group := EmoteModifierGroup{
					ID:        ksuid.New().String(),
					OwnerID:   ksuid.New().String(),
					Modifiers: []string{"BW", "HF", "SQ"},
					SourceEmoteGroupIDs: []string{
						ksuid.New().String(),
					},
				}

				// WHEN: call ToTwirp()
				result := group.ToTwirp()

				// THEN: successfully converted to a mako twirp emote group
				assert.NotNil(t, result)
				assert.Equal(t, group.ID, result.Id)
				assert.Equal(t, group.OwnerID, result.OwnerId)
				assert.Len(t, result.SourceEmoteGroupIds, 1)
				assert.Equal(t, group.SourceEmoteGroupIDs[0], result.SourceEmoteGroupIds[0])
				assert.Len(t, result.Modifiers, 3)
				assert.Contains(t, result.Modifiers, mk.Modification_BlackWhite)
				assert.Contains(t, result.Modifiers, mk.Modification_HorizontalFlip)
				assert.Contains(t, result.Modifiers, mk.Modification_Squished)
			},
		},
	}

	for _, test := range tests {
		test := test
		t.Run(test.scenario, func(t *testing.T) {
			t.Parallel()

			ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
			defer cancel()

			test.fn(ctx, t)
		})
	}
}

func TestTwirpToEmoteModifierGroup(t *testing.T) {
	tests := []struct {
		scenario string
		fn       func(ctx context.Context, t *testing.T)
	}{
		{
			scenario: "Should return correct modifier code",
			fn: func(ctx context.Context, t *testing.T) {
				// GIVEN: twirp emote mod group
				group := &mk.EmoteModifierGroup{
					Id:      ksuid.New().String(),
					OwnerId: ksuid.New().String(),
					Modifiers: []mk.Modification{
						mk.Modification_HorizontalFlip,
						mk.Modification_Thinking,
					},
					SourceEmoteGroupIds: []string{
						ksuid.New().String(),
					},
				}

				// WHEN: call TwirpToEmoteModifierGroup
				result := TwirpToEmoteModifierGroup(group)

				// THEN: return internal emote mod group
				assert.Equal(t, group.Id, result.ID)
				assert.Equal(t, group.OwnerId, result.OwnerID)
				assert.Len(t, result.SourceEmoteGroupIDs, 1)
				assert.Equal(t, group.SourceEmoteGroupIds[0], result.SourceEmoteGroupIDs[0])
				assert.Len(t, result.Modifiers, 2)
				assert.Contains(t, result.Modifiers, "HF")
				assert.Contains(t, result.Modifiers, "TK")
			},
		},
	}

	for _, test := range tests {
		test := test
		t.Run(test.scenario, func(t *testing.T) {
			t.Parallel()

			ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
			defer cancel()

			test.fn(ctx, t)
		})
	}
}
