package mako

import (
	"context"
	"strings"
	"sync"
	"time"

	"github.com/hashicorp/go-multierror"

	"golang.org/x/sync/errgroup"

	log "code.justin.tv/commerce/logrus"

	mkd "code.justin.tv/commerce/mako/twirp/dashboard"
)

type EntitlementType string
type EntitlementSource string
type OriginID string

func (o OriginID) String() string {
	return string(o)
}

const (
	EmoteEntitlement      EntitlementType = "emote"
	EmoteGroupEntitlement EntitlementType = "emote_group"
	EmptyEntitlement      EntitlementType = "empty"

	GlobalsSource       EntitlementSource = "globals" // this is always faked because we don't store globals in Materia
	SmiliesSource       EntitlementSource = "smilies"
	ChannelPointsSource EntitlementSource = "channelPoints"
	BitsBadgeTierSource EntitlementSource = "bitsBadgeTier"
	SubscriptionsSource EntitlementSource = "subscriptions"
	RewardsSource       EntitlementSource = "rewards"
	HypeTrainSource     EntitlementSource = "hypetrain"
	MegaCommerceSource  EntitlementSource = "megacommerce"
	FollowerSource      EntitlementSource = "follower" // this is always faked because we dont' store follower emote entitlements in Materia

	TwoFactorOriginID OriginID = "2FA"
	GlobalsOriginID   OriginID = "GLOBALS"
	LegacyOriginID    OriginID = "REWARDS"
	FollowerOriginID  OriginID = "FOLLOWER"

	LegacyEntitlementsChannelID = "139075904" //QaTwPartner

	FollowerEntitlementIDPrefix  = "Follower"
	FollowerEntitlementSeparator = "_"
)

type EntitlementDB interface {
	ReadByIDs(ctx context.Context, ownerID string, ids ...string) ([]Entitlement, error)
	ReadByOwnerID(ctx context.Context, ownerID string, channelID string) ([]Entitlement, error)
	WriteEntitlements(ctx context.Context, entitlements ...Entitlement) error
}

type Entitlement struct {
	ID        string
	OwnerID   string
	Source    EntitlementSource
	Type      EntitlementType
	OriginID  string
	ChannelID string
	Start     time.Time
	End       *time.Time
	Metadata  EntitlementMetadata
}

type EntitlementMetadata struct {
	GroupID string `json:"group_id"`
}

func (es EntitlementSource) toEmoteType() string {
	switch es {
	case GlobalsSource:
		return mkd.EmoteOrigin_Globals.String()
	case SmiliesSource:
		return mkd.EmoteOrigin_Smilies.String()
	case ChannelPointsSource:
		return mkd.EmoteOrigin_ChannelPoints.String()
	case BitsBadgeTierSource:
		return mkd.EmoteOrigin_BitsBadgeTierEmotes.String()
	case SubscriptionsSource:
		return mkd.EmoteOrigin_Subscriptions.String()
	case RewardsSource:
		return mkd.EmoteOrigin_Rewards.String()
	case HypeTrainSource:
		return mkd.EmoteOrigin_HypeTrain.String()
	case MegaCommerceSource:
		return mkd.EmoteOrigin_MegaCommerce.String()
	default:
		return mkd.EmoteOrigin_LimitedTime.String()
	}
}

func NewMultiSourceEntitlementDB(primary, legacy, follower EntitlementDB) EntitlementDB {
	primarySource := EntitlementDBSource{
		EntitlementDB: primary,
		name:          "primary",
	}

	// TODO: This legacy entitlementsDB should be removed when the entitlements have fully migrated to Materia. (https://jira.xarth.tv/browse/DIGI-220)
	legacySource := EntitlementDBSource{
		EntitlementDB: legacy,
		name:          "legacy",
	}

	followerSource := EntitlementDBSource{
		EntitlementDB: follower,
		name:          "follower",
	}

	return &multiSourceEntitlementDB{
		primary:  primarySource,
		legacy:   legacySource,
		follower: followerSource,
		sources:  []EntitlementDBSource{primarySource, legacySource, followerSource},
	}
}

type EntitlementDBSource struct {
	EntitlementDB
	name string
}

type multiSourceEntitlementDB struct {
	primary  EntitlementDBSource
	legacy   EntitlementDBSource
	follower EntitlementDBSource
	sources  []EntitlementDBSource
}

// This abstraction allows us to get entitlements across a number of backend sources of truth in parallel. For normally
// entitled emotes, we look up in the primary source of truth: Materia, and in the legacy source of truth: Mako EntitlementsDB.
// However, as the various ways of getting emote entitlements grows beyond just what can reasonably fit in Materia, we need
// to be able to support looking up entitlements that are constructed at request-time based off of data whose source of
// truth comes from other services. For instance, Follower Only Emotes depend on a user following a channel in order for
// the user to be entitled to those emotes in that channel
func (db *multiSourceEntitlementDB) ReadByIDs(ctx context.Context, ownerID string, ids ...string) ([]Entitlement, error) {
	eg, ctx := errgroup.WithContext(ctx)
	var errs multierror.Error
	var mu sync.Mutex
	var errMu sync.Mutex

	idsByDB := make(map[EntitlementDBSource][]string)
	for _, id := range ids {
		if strings.HasPrefix(id, FollowerEntitlementIDPrefix) {
			idsByDB[db.follower] = append(idsByDB[db.follower], id)
		} else {
			// All primary and legacy entitlement IDs look the same, so we can't tell them apart
			idsByDB[db.primary] = append(idsByDB[db.primary], id)
			idsByDB[db.legacy] = append(idsByDB[db.legacy], id)
		}
	}

	// Parallel call to all the various entitlementDBs looking for the relevant entitlements by ID
	entitlements := make([]Entitlement, 0)
	for entitlementDBSource, specificIds := range idsByDB {
		eg.Go(
			func(source EntitlementDBSource, ids ...string) func() error {
				return func() error {
					specificEntitlements, err := source.ReadByIDs(ctx, ownerID, ids...)
					if err != nil {
						// If there is an error, then we may still get a partially successful response back from other sources,
						// so log the error and return nil
						log.WithError(err).Error("failed to read entitlements by IDs for " + source.name)
						errMu.Lock()
						errs.Errors = append(errs.Errors, err)
						errMu.Unlock()
					}
					mu.Lock()
					entitlements = append(entitlements, specificEntitlements...)
					mu.Unlock()

					return nil
				}
			}(entitlementDBSource, specificIds...),
		)
	}

	// We specifically do not immediately return any errors so that we can maintain a partially responsive endpoint
	err := eg.Wait()
	if err != nil {
		log.WithError(err).WithFields(log.Fields{
			"ownerID": ownerID,
			"ids":     ids,
		}).Error("error looking up entitlements by ID in parallel")
		errs.Errors = append(errs.Errors, err)
	}

	// We return the entitlements and error here in case we get partial failure/success
	return entitlements, errs.ErrorOrNil()
}

func (db *multiSourceEntitlementDB) ReadByOwnerID(ctx context.Context, ownerID string, channelID string) ([]Entitlement, error) {
	eg, ctx := errgroup.WithContext(ctx)
	var errs multierror.Error
	var mu sync.Mutex
	var errMu sync.Mutex

	// Parallel call to all the various entitlementDBs looking for the relevant entitlements by OwnerID
	entitlements := make([]Entitlement, 0)
	for _, entitlementDBSource := range db.sources {
		eg.Go(
			func(source EntitlementDBSource) func() error {
				return func() error {
					sourceEntitlements, err := source.ReadByOwnerID(ctx, ownerID, channelID)
					if err != nil {
						// If there is an error, then we may still get a partially successful response back from other sources,
						// so log the error and return nil
						log.WithError(err).Error("failed to read entitlements by OwnerID for " + source.name)

						errMu.Lock()
						errs.Errors = append(errs.Errors, err)
						errMu.Unlock()
					}
					mu.Lock()
					entitlements = append(entitlements, sourceEntitlements...)
					mu.Unlock()

					return nil
				}
			}(entitlementDBSource),
		)
	}

	// We specifically do not immediately return any errors so that we can maintain a partially responsive endpoint
	err := eg.Wait()
	if err != nil {
		log.WithError(err).WithFields(log.Fields{
			"ownerID":   ownerID,
			"channelID": channelID,
		}).Error("error looking up entitlements by OwnerID in parallel")
		errs.Errors = append(errs.Errors, err)
	}

	// We return the entitlements and error here in case we get partial failure/success
	return entitlements, errs.ErrorOrNil()
}

func (db *multiSourceEntitlementDB) WriteEntitlements(ctx context.Context, entitlements ...Entitlement) error {
	return db.primary.WriteEntitlements(ctx, entitlements...)
}
