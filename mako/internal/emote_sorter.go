package mako

import (
	"sort"

	mako_client "code.justin.tv/commerce/mako/twirp"
	mako_dashboard "code.justin.tv/commerce/mako/twirp/dashboard"
)

// SortEmotesByOrderAscendingAndCreatedAtDescending will sort emotes by their order ascending. If multiple emotes have the same order,
// then the ties will be broken by returning the newest created emote first.
func SortEmotesByOrderAscendingAndCreatedAtDescending(emotes []Emote) {
	sort.Slice(emotes, func(i, j int) bool {
		a := emotes[i]
		b := emotes[j]

		if a.Order == b.Order {
			return a.CreatedAt.After(b.CreatedAt)
		}

		return a.Order < b.Order
	})
}

// SortTwirpEmotesByOrderAscendingAndCreatedAtDescending sorts the given emotes by their order ascending.
// If two emotes have the same order, then they are sorted by their creation date descending.
func SortTwirpEmotesByOrderAscendingAndCreatedAtDescending(emotes []*mako_client.Emote) {
	sort.Slice(emotes, func(i, j int) bool {
		if emotes[i] == nil {
			return false
		}

		if emotes[j] == nil {
			return true
		}

		if emotes[i].Order == emotes[j].Order {
			createdAtA := emotes[i].CreatedAt.AsTime()
			createdAtB := emotes[j].CreatedAt.AsTime()

			return createdAtA.After(createdAtB)
		}

		return emotes[i].Order < emotes[j].Order
	})
}

// SortTwirpEmoticonsByOrderAscendingAndCreatedAtDescending sorts the given emotes by their order ascending.
// If two emotes have the same order, then they are sorted by their creation date descending.
func SortTwirpEmoticonsByOrderAscendingAndCreatedAtDescending(emotes []*mako_client.Emoticon) {
	sort.Slice(emotes, func(i, j int) bool {
		if emotes[i] == nil {
			return false
		}

		if emotes[j] == nil {
			return true
		}

		if emotes[i].Order == emotes[j].Order {
			createdAtA := emotes[i].CreatedAt.AsTime()
			createdAtB := emotes[j].CreatedAt.AsTime()

			return createdAtA.After(createdAtB)
		}

		return emotes[i].Order < emotes[j].Order
	})
}

// SortDashboardTwirpEmotesByOrderAscendingAndCreatedAtDescending sorts the given emotes by their order ascending.
// If two emotes have the same order, then they are sorted by their creation date descending.
func SortDashboardTwirpEmotesByOrderAscendingAndCreatedAtDescending(emotes []*mako_dashboard.Emote) {
	sort.Slice(emotes, func(i, j int) bool {
		if emotes[i] == nil {
			return false
		}

		if emotes[j] == nil {
			return true
		}

		if emotes[i].Order == emotes[j].Order {
			createdAtA := emotes[i].CreatedAt.AsTime()
			createdAtB := emotes[j].CreatedAt.AsTime()

			return createdAtA.After(createdAtB)
		}

		return emotes[i].Order < emotes[j].Order
	})
}
