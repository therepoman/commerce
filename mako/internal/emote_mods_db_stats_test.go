package mako_test

import (
	"context"
	"testing"

	"code.justin.tv/commerce/mako/dynamo"
	mako "code.justin.tv/commerce/mako/internal"
	"code.justin.tv/commerce/mako/tests"
	"github.com/cactus/go-statsd-client/statsd"
)

// TestLocalEmoteModifiersDBWithStats runs the test suite wrapping a dynamo EmoteModifiersDB with a stats decorator to
// ensure that no functionality has been lost/altered.
func TestLocalEmoteModifiersDBWithStats(t *testing.T) {
	tests.TestEmoteModifiersDB(t, makeEmoteModifiersDBWithStats(dynamo.MakeEmoteModifiersDBForTesting))
}

func makeEmoteModifiersDBWithStats(makeBase tests.MakeEmoteModifiersDB) tests.MakeEmoteModifiersDB {
	return func(ctx context.Context) (mako.EmoteModifiersDB, error) {
		base, err := makeBase(ctx)
		if err != nil {
			return nil, err
		}

		noop, _ := statsd.NewNoop()
		return mako.NewEmoteModifiersDBWithStats(base, noop, ""), nil
	}
}
