package mako

import (
	"context"
	"fmt"
	"time"

	"github.com/pkg/errors"
)

type Smilie string

const (
	RobotsSmilieGroupID  Smilie = "0"
	PurpleSmilieGroupID  Smilie = "33"
	MonkeysSmilieGroupID Smilie = "42"
	SmilieGroupID        string = "smilie"
	TwitchOwnerID        string = "twitch"
)

func (s Smilie) ToString() string {
	return string(s)
}

var (
	// specialSmilies are global emotes that currently conflict with modified emotes by using the `_` separator. So we need to know about them
	// so we don't split the emote in two.
	specialSmilies = []string{
		"O_o",
		"O_O",
		"o_O",
		"o_o",
	}

	// Since smilies do not change over time, we are storing them all in memory so that we don't have to call any DB or Redis for them
	allSmilies = []Emote{
		{ID: "1", OwnerID: TwitchOwnerID, GroupID: "0", Prefix: "", Suffix: ":)", Code: ":)", State: Active, Domain: EmotesDomain, Order: 0, EmotesGroup: "Smilies"},
		{ID: "2", OwnerID: TwitchOwnerID, GroupID: "0", Prefix: "", Suffix: ":(", Code: ":(", State: Active, Domain: EmotesDomain, Order: 1, EmotesGroup: "Smilies"},
		{ID: "3", OwnerID: TwitchOwnerID, GroupID: "0", Prefix: "", Suffix: ":D", Code: ":D", State: Active, Domain: EmotesDomain, Order: 2, EmotesGroup: "Smilies"},
		{ID: "4", OwnerID: TwitchOwnerID, GroupID: "0", Prefix: "", Suffix: ">(", Code: ">(", State: Active, Domain: EmotesDomain, Order: 3, EmotesGroup: "Smilies"},
		{ID: "5", OwnerID: TwitchOwnerID, GroupID: "0", Prefix: "", Suffix: ":|", Code: ":|", State: Active, Domain: EmotesDomain, Order: 4, EmotesGroup: "Smilies"},
		{ID: "6", OwnerID: TwitchOwnerID, GroupID: "0", Prefix: "", Suffix: "O_o", Code: "O_o", State: Active, Domain: EmotesDomain, Order: 5, EmotesGroup: "Smilies"},
		{ID: "7", OwnerID: TwitchOwnerID, GroupID: "0", Prefix: "", Suffix: "B)", Code: "B)", State: Active, Domain: EmotesDomain, Order: 6, EmotesGroup: "Smilies"},
		{ID: "8", OwnerID: TwitchOwnerID, GroupID: "0", Prefix: "", Suffix: ":O", Code: ":O", State: Active, Domain: EmotesDomain, Order: 7, EmotesGroup: "Smilies"},
		{ID: "9", OwnerID: TwitchOwnerID, GroupID: "0", Prefix: "", Suffix: "<3", Code: "<3", State: Active, Domain: EmotesDomain, Order: 8, EmotesGroup: "Smilies"},
		{ID: "10", OwnerID: TwitchOwnerID, GroupID: "0", Prefix: "", Suffix: ":/", Code: ":/", State: Active, Domain: EmotesDomain, Order: 9, EmotesGroup: "Smilies"},
		{ID: "11", OwnerID: TwitchOwnerID, GroupID: "0", Prefix: "", Suffix: ";)", Code: ";)", State: Active, Domain: EmotesDomain, Order: 10, EmotesGroup: "Smilies"},
		{ID: "12", OwnerID: TwitchOwnerID, GroupID: "0", Prefix: "", Suffix: ":P", Code: ":P", State: Active, Domain: EmotesDomain, Order: 11, EmotesGroup: "Smilies"},
		{ID: "13", OwnerID: TwitchOwnerID, GroupID: "0", Prefix: "", Suffix: ";P", Code: ";P", State: Active, Domain: EmotesDomain, Order: 12, EmotesGroup: "Smilies"},
		{ID: "14", OwnerID: TwitchOwnerID, GroupID: "0", Prefix: "", Suffix: "R)", Code: "R)", State: Active, Domain: EmotesDomain, Order: 13, EmotesGroup: "Smilies"},

		{ID: "432", OwnerID: TwitchOwnerID, GroupID: "33", Prefix: "", Suffix: ">(", Code: ">(", State: Active, Domain: EmotesDomain, Order: 0, EmotesGroup: "Smilies"},
		{ID: "439", OwnerID: TwitchOwnerID, GroupID: "33", Prefix: "", Suffix: ";)", Code: ";)", State: Active, Domain: EmotesDomain, Order: 1, EmotesGroup: "Smilies"},
		{ID: "440", OwnerID: TwitchOwnerID, GroupID: "33", Prefix: "", Suffix: ":)", Code: ":)", State: Active, Domain: EmotesDomain, Order: 2, EmotesGroup: "Smilies"},
		{ID: "441", OwnerID: TwitchOwnerID, GroupID: "33", Prefix: "", Suffix: "B)", Code: "B)", State: Active, Domain: EmotesDomain, Order: 3, EmotesGroup: "Smilies"},
		{ID: "442", OwnerID: TwitchOwnerID, GroupID: "33", Prefix: "", Suffix: ";P", Code: ";P", State: Active, Domain: EmotesDomain, Order: 4, EmotesGroup: "Smilies"},
		{ID: "443", OwnerID: TwitchOwnerID, GroupID: "33", Prefix: "", Suffix: ":D", Code: ":D", State: Active, Domain: EmotesDomain, Order: 5, EmotesGroup: "Smilies"},
		{ID: "444", OwnerID: TwitchOwnerID, GroupID: "33", Prefix: "", Suffix: ":|", Code: ":|", State: Active, Domain: EmotesDomain, Order: 6, EmotesGroup: "Smilies"},
		{ID: "445", OwnerID: TwitchOwnerID, GroupID: "33", Prefix: "", Suffix: "<3", Code: "<3", State: Active, Domain: EmotesDomain, Order: 7, EmotesGroup: "Smilies"},
		{ID: "438", OwnerID: TwitchOwnerID, GroupID: "33", Prefix: "", Suffix: ":P", Code: ":P", State: Active, Domain: EmotesDomain, Order: 8, EmotesGroup: "Smilies"},
		{ID: "436", OwnerID: TwitchOwnerID, GroupID: "33", Prefix: "", Suffix: ":O", Code: ":O", State: Active, Domain: EmotesDomain, Order: 9, EmotesGroup: "Smilies"},
		{ID: "435", OwnerID: TwitchOwnerID, GroupID: "33", Prefix: "", Suffix: "R)", Code: "R)", State: Active, Domain: EmotesDomain, Order: 10, EmotesGroup: "Smilies"},
		{ID: "434", OwnerID: TwitchOwnerID, GroupID: "33", Prefix: "", Suffix: ":(", Code: ":(", State: Active, Domain: EmotesDomain, Order: 11, EmotesGroup: "Smilies"},
		{ID: "433", OwnerID: TwitchOwnerID, GroupID: "33", Prefix: "", Suffix: ":/", Code: ":/", State: Active, Domain: EmotesDomain, Order: 12, EmotesGroup: "Smilies"},
		{ID: "437", OwnerID: TwitchOwnerID, GroupID: "33", Prefix: "", Suffix: "O_o", Code: "O_o", State: Active, Domain: EmotesDomain, Order: 13, EmotesGroup: "Smilies"},

		{ID: "497", OwnerID: TwitchOwnerID, GroupID: "42", Prefix: "", Suffix: "O_o", Code: "O_o", State: Active, Domain: EmotesDomain, Order: 0, EmotesGroup: "Smilies"},
		{ID: "493", OwnerID: TwitchOwnerID, GroupID: "42", Prefix: "", Suffix: ":/", Code: ":/", State: Active, Domain: EmotesDomain, Order: 1, EmotesGroup: "Smilies"},
		{ID: "487", OwnerID: TwitchOwnerID, GroupID: "42", Prefix: "", Suffix: "<]", Code: "<]", State: Active, Domain: EmotesDomain, Order: 2, EmotesGroup: "Smilies"},
		{ID: "485", OwnerID: TwitchOwnerID, GroupID: "42", Prefix: "", Suffix: "#/", Code: "#/", State: Active, Domain: EmotesDomain, Order: 3, EmotesGroup: "Smilies"},
		{ID: "501", OwnerID: TwitchOwnerID, GroupID: "42", Prefix: "", Suffix: ";)", Code: ";)", State: Active, Domain: EmotesDomain, Order: 4, EmotesGroup: "Smilies"},
		{ID: "500", OwnerID: TwitchOwnerID, GroupID: "42", Prefix: "", Suffix: "B)", Code: "B)", State: Active, Domain: EmotesDomain, Order: 5, EmotesGroup: "Smilies"},
		{ID: "499", OwnerID: TwitchOwnerID, GroupID: "42", Prefix: "", Suffix: ":)", Code: ":)", State: Active, Domain: EmotesDomain, Order: 6, EmotesGroup: "Smilies"},
		{ID: "498", OwnerID: TwitchOwnerID, GroupID: "42", Prefix: "", Suffix: ">(", Code: ">(", State: Active, Domain: EmotesDomain, Order: 7, EmotesGroup: "Smilies"},
		{ID: "496", OwnerID: TwitchOwnerID, GroupID: "42", Prefix: "", Suffix: ":D", Code: ":D", State: Active, Domain: EmotesDomain, Order: 8, EmotesGroup: "Smilies"},
		{ID: "495", OwnerID: TwitchOwnerID, GroupID: "42", Prefix: "", Suffix: ":s", Code: ":s", State: Active, Domain: EmotesDomain, Order: 9, EmotesGroup: "Smilies"},
		{ID: "494", OwnerID: TwitchOwnerID, GroupID: "42", Prefix: "", Suffix: ":|", Code: ":|", State: Active, Domain: EmotesDomain, Order: 10, EmotesGroup: "Smilies"},
		{ID: "492", OwnerID: TwitchOwnerID, GroupID: "42", Prefix: "", Suffix: ":O", Code: ":O", State: Active, Domain: EmotesDomain, Order: 11, EmotesGroup: "Smilies"},
		{ID: "491", OwnerID: TwitchOwnerID, GroupID: "42", Prefix: "", Suffix: ";P", Code: ";P", State: Active, Domain: EmotesDomain, Order: 12, EmotesGroup: "Smilies"},
		{ID: "490", OwnerID: TwitchOwnerID, GroupID: "42", Prefix: "", Suffix: ":P", Code: ":P", State: Active, Domain: EmotesDomain, Order: 13, EmotesGroup: "Smilies"},
		{ID: "489", OwnerID: TwitchOwnerID, GroupID: "42", Prefix: "", Suffix: ":(", Code: ":(", State: Active, Domain: EmotesDomain, Order: 14, EmotesGroup: "Smilies"},
		{ID: "488", OwnerID: TwitchOwnerID, GroupID: "42", Prefix: "", Suffix: ":7", Code: ":7", State: Active, Domain: EmotesDomain, Order: 15, EmotesGroup: "Smilies"},
		{ID: "486", OwnerID: TwitchOwnerID, GroupID: "42", Prefix: "", Suffix: ":>", Code: ":>", State: Active, Domain: EmotesDomain, Order: 16, EmotesGroup: "Smilies"},
		{ID: "484", OwnerID: TwitchOwnerID, GroupID: "42", Prefix: "", Suffix: "R)", Code: "R)", State: Active, Domain: EmotesDomain, Order: 17, EmotesGroup: "Smilies"},
		{ID: "483", OwnerID: TwitchOwnerID, GroupID: "42", Prefix: "", Suffix: "<3", Code: "<3", State: Active, Domain: EmotesDomain, Order: 18, EmotesGroup: "Smilies"},

		{ID: "555555557", OwnerID: TwitchOwnerID, GroupID: "0", Code: ":-)", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555558", OwnerID: TwitchOwnerID, GroupID: "0", Code: ":(", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555559", OwnerID: TwitchOwnerID, GroupID: "0", Code: ":-(", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555560", OwnerID: TwitchOwnerID, GroupID: "0", Code: ":D", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555561", OwnerID: TwitchOwnerID, GroupID: "0", Code: ":-D", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555562", OwnerID: TwitchOwnerID, GroupID: "0", Code: ">(", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555563", OwnerID: TwitchOwnerID, GroupID: "0", Code: ":|", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555564", OwnerID: TwitchOwnerID, GroupID: "0", Code: ":-|", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555565", OwnerID: TwitchOwnerID, GroupID: "0", Code: ":z", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555566", OwnerID: TwitchOwnerID, GroupID: "0", Code: ":-z", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555567", OwnerID: TwitchOwnerID, GroupID: "0", Code: ":Z", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555568", OwnerID: TwitchOwnerID, GroupID: "0", Code: ":-Z", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555569", OwnerID: TwitchOwnerID, GroupID: "0", Code: "O_o", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555570", OwnerID: TwitchOwnerID, GroupID: "0", Code: "O.o", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555571", OwnerID: TwitchOwnerID, GroupID: "0", Code: "O_O", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555572", OwnerID: TwitchOwnerID, GroupID: "0", Code: "O.O", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555573", OwnerID: TwitchOwnerID, GroupID: "0", Code: "o_O", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555574", OwnerID: TwitchOwnerID, GroupID: "0", Code: "o.O", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555575", OwnerID: TwitchOwnerID, GroupID: "0", Code: "o_o", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555576", OwnerID: TwitchOwnerID, GroupID: "0", Code: "o.o", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555577", OwnerID: TwitchOwnerID, GroupID: "0", Code: "B)", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555578", OwnerID: TwitchOwnerID, GroupID: "0", Code: "B-)", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555579", OwnerID: TwitchOwnerID, GroupID: "0", Code: "8-)", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555580", OwnerID: TwitchOwnerID, GroupID: "0", Code: ":O", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555581", OwnerID: TwitchOwnerID, GroupID: "0", Code: ":-O", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555582", OwnerID: TwitchOwnerID, GroupID: "0", Code: ":o", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555583", OwnerID: TwitchOwnerID, GroupID: "0", Code: ":-o", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555584", OwnerID: TwitchOwnerID, GroupID: "0", Code: "<3", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555585", OwnerID: TwitchOwnerID, GroupID: "0", Code: ":/", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555586", OwnerID: TwitchOwnerID, GroupID: "0", Code: ":-/", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555587", OwnerID: TwitchOwnerID, GroupID: "0", Code: ":\\", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555588", OwnerID: TwitchOwnerID, GroupID: "0", Code: ":-\\", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555589", OwnerID: TwitchOwnerID, GroupID: "0", Code: ";)", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555590", OwnerID: TwitchOwnerID, GroupID: "0", Code: ";-)", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555591", OwnerID: TwitchOwnerID, GroupID: "0", Code: ":P", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555592", OwnerID: TwitchOwnerID, GroupID: "0", Code: ":-P", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555593", OwnerID: TwitchOwnerID, GroupID: "0", Code: ":p", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555594", OwnerID: TwitchOwnerID, GroupID: "0", Code: ":-p", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555595", OwnerID: TwitchOwnerID, GroupID: "0", Code: ";P", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555596", OwnerID: TwitchOwnerID, GroupID: "0", Code: ";-P", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555597", OwnerID: TwitchOwnerID, GroupID: "0", Code: ";p", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555598", OwnerID: TwitchOwnerID, GroupID: "0", Code: ";-p", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555599", OwnerID: TwitchOwnerID, GroupID: "0", Code: "R)", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555600", OwnerID: TwitchOwnerID, GroupID: "0", Code: "R-)", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555601", OwnerID: TwitchOwnerID, GroupID: "33", Code: ">(", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555602", OwnerID: TwitchOwnerID, GroupID: "33", Code: ":/", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555603", OwnerID: TwitchOwnerID, GroupID: "33", Code: ":-/", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555604", OwnerID: TwitchOwnerID, GroupID: "33", Code: ":\\", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555605", OwnerID: TwitchOwnerID, GroupID: "33", Code: ":-\\", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555606", OwnerID: TwitchOwnerID, GroupID: "33", Code: ":(", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555607", OwnerID: TwitchOwnerID, GroupID: "33", Code: ":-(", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555608", OwnerID: TwitchOwnerID, GroupID: "33", Code: "R)", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555609", OwnerID: TwitchOwnerID, GroupID: "33", Code: "R-)", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555610", OwnerID: TwitchOwnerID, GroupID: "33", Code: ":O", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555611", OwnerID: TwitchOwnerID, GroupID: "33", Code: ":-O", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555612", OwnerID: TwitchOwnerID, GroupID: "33", Code: ":o", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555613", OwnerID: TwitchOwnerID, GroupID: "33", Code: ":-o", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555614", OwnerID: TwitchOwnerID, GroupID: "33", Code: "O_o", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555615", OwnerID: TwitchOwnerID, GroupID: "33", Code: "O.o", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555616", OwnerID: TwitchOwnerID, GroupID: "33", Code: "O_O", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555617", OwnerID: TwitchOwnerID, GroupID: "33", Code: "O.O", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555618", OwnerID: TwitchOwnerID, GroupID: "33", Code: "o_O", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555619", OwnerID: TwitchOwnerID, GroupID: "33", Code: "o.O", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555620", OwnerID: TwitchOwnerID, GroupID: "33", Code: "o_o", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555621", OwnerID: TwitchOwnerID, GroupID: "33", Code: "o.o", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555622", OwnerID: TwitchOwnerID, GroupID: "33", Code: ":P", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555623", OwnerID: TwitchOwnerID, GroupID: "33", Code: ":-P", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555624", OwnerID: TwitchOwnerID, GroupID: "33", Code: ":p", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555625", OwnerID: TwitchOwnerID, GroupID: "33", Code: ":-p", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555626", OwnerID: TwitchOwnerID, GroupID: "33", Code: ";)", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555627", OwnerID: TwitchOwnerID, GroupID: "33", Code: ";-)", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555628", OwnerID: TwitchOwnerID, GroupID: "33", Code: ":)", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555629", OwnerID: TwitchOwnerID, GroupID: "33", Code: ":-)", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555630", OwnerID: TwitchOwnerID, GroupID: "33", Code: "B)", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555631", OwnerID: TwitchOwnerID, GroupID: "33", Code: "B-)", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555632", OwnerID: TwitchOwnerID, GroupID: "33", Code: "8-)", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555633", OwnerID: TwitchOwnerID, GroupID: "33", Code: ";P", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555634", OwnerID: TwitchOwnerID, GroupID: "33", Code: ";-P", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555635", OwnerID: TwitchOwnerID, GroupID: "33", Code: ";p", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555636", OwnerID: TwitchOwnerID, GroupID: "33", Code: ";-p", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555637", OwnerID: TwitchOwnerID, GroupID: "33", Code: ":D", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555638", OwnerID: TwitchOwnerID, GroupID: "33", Code: ":-D", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555639", OwnerID: TwitchOwnerID, GroupID: "33", Code: ":|", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555640", OwnerID: TwitchOwnerID, GroupID: "33", Code: ":-|", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555641", OwnerID: TwitchOwnerID, GroupID: "33", Code: ":z", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555642", OwnerID: TwitchOwnerID, GroupID: "33", Code: ":-z", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555643", OwnerID: TwitchOwnerID, GroupID: "33", Code: ":Z", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555644", OwnerID: TwitchOwnerID, GroupID: "33", Code: ":-Z", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555645", OwnerID: TwitchOwnerID, GroupID: "33", Code: "<3", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555648", OwnerID: TwitchOwnerID, GroupID: "42", Code: "R-)", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555650", OwnerID: TwitchOwnerID, GroupID: "42", Code: "#-/", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555651", OwnerID: TwitchOwnerID, GroupID: "42", Code: "#\\", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555652", OwnerID: TwitchOwnerID, GroupID: "42", Code: "#-\\", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555656", OwnerID: TwitchOwnerID, GroupID: "42", Code: ":-7", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555657", OwnerID: TwitchOwnerID, GroupID: "42", Code: ":L", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555658", OwnerID: TwitchOwnerID, GroupID: "42", Code: ":-L", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555660", OwnerID: TwitchOwnerID, GroupID: "42", Code: ":-(", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555662", OwnerID: TwitchOwnerID, GroupID: "42", Code: ":-P", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555663", OwnerID: TwitchOwnerID, GroupID: "42", Code: ":p", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555664", OwnerID: TwitchOwnerID, GroupID: "42", Code: ":-p", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555666", OwnerID: TwitchOwnerID, GroupID: "42", Code: ";-P", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555667", OwnerID: TwitchOwnerID, GroupID: "42", Code: ";p", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555668", OwnerID: TwitchOwnerID, GroupID: "42", Code: ";-p", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555670", OwnerID: TwitchOwnerID, GroupID: "42", Code: ":-O", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555671", OwnerID: TwitchOwnerID, GroupID: "42", Code: ":o", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555672", OwnerID: TwitchOwnerID, GroupID: "42", Code: ":-o", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555674", OwnerID: TwitchOwnerID, GroupID: "42", Code: ":-/", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555675", OwnerID: TwitchOwnerID, GroupID: "42", Code: ":\\", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555676", OwnerID: TwitchOwnerID, GroupID: "42", Code: ":-\\", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555678", OwnerID: TwitchOwnerID, GroupID: "42", Code: ":-|", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555679", OwnerID: TwitchOwnerID, GroupID: "42", Code: ":z", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555680", OwnerID: TwitchOwnerID, GroupID: "42", Code: ":-z", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555681", OwnerID: TwitchOwnerID, GroupID: "42", Code: ":Z", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555682", OwnerID: TwitchOwnerID, GroupID: "42", Code: ":-Z", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555683", OwnerID: TwitchOwnerID, GroupID: "42", Code: ":S", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555684", OwnerID: TwitchOwnerID, GroupID: "42", Code: ":-S", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555685", OwnerID: TwitchOwnerID, GroupID: "42", Code: ":s", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555686", OwnerID: TwitchOwnerID, GroupID: "42", Code: ":-s", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555687", OwnerID: TwitchOwnerID, GroupID: "42", Code: ":D", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555688", OwnerID: TwitchOwnerID, GroupID: "42", Code: ":-D", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555689", OwnerID: TwitchOwnerID, GroupID: "42", Code: "O_o", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555690", OwnerID: TwitchOwnerID, GroupID: "42", Code: "O.o", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555691", OwnerID: TwitchOwnerID, GroupID: "42", Code: "O_O", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555692", OwnerID: TwitchOwnerID, GroupID: "42", Code: "O.O", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555693", OwnerID: TwitchOwnerID, GroupID: "42", Code: "o_O", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555694", OwnerID: TwitchOwnerID, GroupID: "42", Code: "o.O", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555695", OwnerID: TwitchOwnerID, GroupID: "42", Code: "o_o", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555696", OwnerID: TwitchOwnerID, GroupID: "42", Code: "o.o", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555699", OwnerID: TwitchOwnerID, GroupID: "42", Code: "B-)", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
		{ID: "555555701", OwnerID: TwitchOwnerID, GroupID: "42", Code: ";-)", State: Active, Domain: EmotesDomain, EmotesGroup: "Smilies"},
	}

	validSmilies = map[string]struct{}{
		":)":   {},
		":-)":  {},
		":(":   {},
		":-(":  {},
		":D":   {},
		":-D":  {},
		">(":   {},
		":|":   {},
		":-|":  {},
		":z":   {},
		":-z":  {},
		":Z":   {},
		":-Z":  {},
		"O_o":  {},
		"O.o":  {},
		"O_O":  {},
		"O.O":  {},
		"o_O":  {},
		"o.O":  {},
		"o_o":  {},
		"o.o":  {},
		"B)":   {},
		"B-)":  {},
		"8-)":  {},
		":O":   {},
		":-O":  {},
		":o":   {},
		":-o":  {},
		"<3":   {},
		":/":   {},
		":-/":  {},
		":\\":  {},
		":-\\": {},
		";)":   {},
		";-)":  {},
		":P":   {},
		":-P":  {},
		":p":   {},
		":-p":  {},
		";P":   {},
		";-P":  {},
		";p":   {},
		";-p":  {},
		"R)":   {},
		"R-)":  {},
		":-()": {},
		"#/":   {},
		"#-/":  {},
		"#\\":  {},
		"#-\\": {},
		":>":   {},
		"<]":   {},
		":7":   {},
		":-7":  {},
		":L":   {},
		":-L":  {},
		":S":   {},
		":-S":  {},
		":s":   {},
		":-s":  {},
	}
)

type groupID = string
type emoteID = string

type Normalize struct {
	// emoteIDs maps a set of groups with the `codes` inputs to return a new emote id. Each emote code
	// may have a denormalized emote id so this allows it to accept N emote codes and return a normalized
	// emote id for all of them to the clients (e.g., mobile).
	emoteIDs map[groupID]emoteID
	// codes is a slice of denormalized codes that should be translated to a new code + emote id.
	codes []string
	// newCode optionally translate all the given codes into a single emote code. If
	// newCode isn't defined then the first element of the `codes` slice is used as the new
	// code.
	newCode string
}

func (n Normalize) Code() string {
	if n.newCode != "" {
		return n.newCode
	}

	return n.codes[0]
}

func (n Normalize) ID(groupID string) (string, bool) {
	id, ok := n.emoteIDs[groupID]
	return id, ok
}

// NormalizedSmilies is used to Normalize all the similar smilie emote codes since they are denormalized. Each code may reference
// to the same actual smilie, just with a slightly different code. The normalizedSmilies will be used to deduplicate similar smilies.
var NormalizedSmilies = []Normalize{
	{
		emoteIDs: map[groupID]emoteID{
			"0":  "1",
			"33": "440",
			"42": "499",
		},
		codes: []string{":)", ":-)"},
		// newCode is a regex as mobile has issues with certain codes.
		newCode: "\\:-?\\)",
	},
	{
		emoteIDs: map[groupID]emoteID{
			"0":  "2",
			"33": "434",
			"42": "489",
		},
		codes: []string{":(", ":-("},
		// newCode is a regex as mobile has issues with certain codes.
		newCode: "\\:-?\\(",
	},
	{
		emoteIDs: map[groupID]emoteID{
			"0":  "3",
			"33": "443",
			"42": "496",
		},
		codes: []string{":D", ":-D"},
	},
	{
		emoteIDs: map[groupID]emoteID{
			"0":  "4",
			"33": "432",
			"42": "498",
		},
		codes: []string{">("},
	},
	{
		emoteIDs: map[groupID]emoteID{
			"0":  "5",
			"33": "444",
			"42": "494",
		},
		codes: []string{":|", ":-|", ":z", ":-z", ":Z", ":-Z"},
	},
	{
		emoteIDs: map[groupID]emoteID{
			"0":  "6",
			"33": "437",
			"42": "497",
		},
		codes: []string{"O_o", "O.o", "O_O", "O.O", "o_O", "o.O", "o_o", "o.o"},
	},
	{
		emoteIDs: map[groupID]emoteID{
			"0":  "7",
			"33": "441",
			"42": "500",
		},
		codes:   []string{"B)", "B-)", "8-)"},
		newCode: "B-?\\)",
	},
	{
		emoteIDs: map[groupID]emoteID{
			"0":  "8",
			"33": "436",
			"42": "492",
		},
		codes: []string{":O", ":-O", ":o", ":-o"},
	},
	{
		emoteIDs: map[groupID]emoteID{
			"0":  "9",
			"33": "445",
			"42": "483",
		},
		codes: []string{"<3"},
	},
	{
		emoteIDs: map[groupID]emoteID{
			"0":  "10",
			"33": "433",
			"42": "493",
		},
		codes: []string{":\\", ":-/", ":/", ":-\\"},
	},
	{
		emoteIDs: map[groupID]emoteID{
			"0":  "11",
			"33": "439",
			"42": "501",
		},
		codes: []string{";)", ";-)"},
	},
	{
		emoteIDs: map[groupID]emoteID{
			"0":  "12",
			"33": "438",
			"42": "490",
		},
		codes: []string{":p", ":P", ":-P", ":-p"},
	},
	{
		emoteIDs: map[groupID]emoteID{
			"0":  "13",
			"33": "442",
			"42": "491",
		},
		codes: []string{";p", ";P", ";-P", ";-p"},
	},
	{
		emoteIDs: map[groupID]emoteID{
			"0":  "14",
			"33": "435",
			"42": "484",
		},
		codes: []string{"R)", "R-)"},
	},
	{
		emoteIDs: map[groupID]emoteID{
			"42": "485",
		},
		codes: []string{"#/", "#-/", "#\\", "#-\\"},
	},
	{
		emoteIDs: map[groupID]emoteID{
			"42": "486",
		},
		codes: []string{":>"},
	},
	{
		emoteIDs: map[groupID]emoteID{
			"42": "487",
		},
		codes: []string{"<]"},
	},
	{
		emoteIDs: map[groupID]emoteID{
			"42": "488",
		},
		codes: []string{":7", ":-7", ":L", ":-L"},
	},
	{
		emoteIDs: map[groupID]emoteID{
			"42": "495",
		},
		codes: []string{":S", ":-S", ":s", ":-s"},
	},
}

// MakeNormalizedSmilies creates set of emote codes -> Normalize so that reads are faster.
func MakeNormalizedSmilies() map[string]Normalize {
	set := make(map[string]Normalize)

	for _, value := range NormalizedSmilies {
		for _, code := range value.codes {
			set[code] = value
		}
	}

	return set
}

// ReadUserSmilies tries to read which Smilies a user has selected for their turbo emote set.
//
// Users are automatically entitled to the `Robots` group if no smilie entitlement was found.
func ReadUserSmilies(ctx context.Context, db EntitlementDB, ownerID string) (Smilie, error) {
	entitlements, err := db.ReadByIDs(ctx, ownerID, SmilieGroupID)
	if err != nil {
		return RobotsSmilieGroupID, errors.Wrapf(err, "failed to read smilies entitlement for user '%s'", ownerID)
	}

	if len(entitlements) == 0 {
		return RobotsSmilieGroupID, nil
	}

	smilie := entitlements[0]

	switch Smilie(smilie.Metadata.GroupID) {
	case PurpleSmilieGroupID:
		return PurpleSmilieGroupID, nil
	case MonkeysSmilieGroupID:
		return MonkeysSmilieGroupID, nil
	}

	return RobotsSmilieGroupID, nil
}

// Returns all smilies, grouped into their respective sets by GroupID,
// with their emotes sorted correctly
func GetAllSmilies() map[Smilie][]Emote {
	// Collect normalized emoteIDs into a quick lookup
	robotsIDs := make(map[string]bool)
	purplesIDs := make(map[string]bool)
	monkeysIDs := make(map[string]bool)
	for _, normalized := range NormalizedSmilies {
		robotID, _ := normalized.ID(string(RobotsSmilieGroupID))
		robotsIDs[robotID] = true
		purpleID, _ := normalized.ID(string(PurpleSmilieGroupID))
		purplesIDs[purpleID] = true
		monkeyID, _ := normalized.ID(string(MonkeysSmilieGroupID))
		monkeysIDs[monkeyID] = true
	}

	// Match each emote in the set of all smilies to the normalized emoteIDs for their given emote group
	// so that we dedupe and group by type (groupID) of smilie
	var robots []Emote
	var purples []Emote
	var monkeys []Emote
	for _, emote := range allSmilies {
		switch emote.GroupID {
		case string(RobotsSmilieGroupID):
			if robotsIDs[emote.ID] {
				robots = append(robots, emote)
			}
		case string(PurpleSmilieGroupID):
			if purplesIDs[emote.ID] {
				purples = append(purples, emote)
			}
		case string(MonkeysSmilieGroupID):
			if monkeysIDs[emote.ID] {
				monkeys = append(monkeys, emote)
			}
		}
	}

	SortEmotesByOrderAscendingAndCreatedAtDescending(robots)
	SortEmotesByOrderAscendingAndCreatedAtDescending(purples)
	SortEmotesByOrderAscendingAndCreatedAtDescending(monkeys)

	// Return smilies by groupID just as they were collected above
	return map[Smilie][]Emote{
		RobotsSmilieGroupID:  robots,
		PurpleSmilieGroupID:  purples,
		MonkeysSmilieGroupID: monkeys,
	}
}

func UpdateUserSmilies(ctx context.Context, db EntitlementDB, ownerID string, smilie Smilie) error {
	switch smilie {
	case RobotsSmilieGroupID:
	case PurpleSmilieGroupID:
	case MonkeysSmilieGroupID:
	default:
		return fmt.Errorf("unknown smilie '%s', failed to update for user '%s'", string(smilie), ownerID)
	}

	entitlement := Entitlement{
		ID:      SmilieGroupID,
		Source:  SubscriptionsSource,
		Type:    EmoteGroupEntitlement,
		OwnerID: ownerID,
		Metadata: EntitlementMetadata{
			GroupID: string(smilie),
		},
		OriginID:  "smilie",
		ChannelID: TwitchOwnerID,
		Start:     time.Now(),
		End:       nil,
	}

	if err := db.WriteEntitlements(ctx, entitlement); err != nil {
		return errors.Wrapf(err, "failed to update smilies entitlement for user '%s'", ownerID)
	}

	return nil
}
