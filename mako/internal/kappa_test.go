package mako

import (
	"testing"

	config_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/config"
	. "github.com/smartystreets/goconvey/convey"
)

func TestIsLuckyGoldenKappa(t *testing.T) {
	Convey("Test IsLuckyGoldenKappa", t, func() {
		mockDynamicConfig := new(config_mock.DynamicConfigClient)
		testUserId := "232889822"

		Convey("GIVEN dynamic config returns true THEN returns true", func() {
			mockDynamicConfig.On("GetGoldenKappaAlwaysOnEnabled", testUserId).Return(true)

			result := IsLuckyGoldenKappa(testUserId, mockDynamicConfig)

			So(result, ShouldBeTrue)
		})
	})
}
