package emote_utils

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"path/filepath"
	"sync"
	"time"

	"code.justin.tv/foundation/twitchclient"

	mkd "code.justin.tv/commerce/mako/twirp/dashboard"
	"code.justin.tv/web/upload-service/rpc/uploader"
	"github.com/pkg/errors"
)

// Emote contains a map of the emote size to the emote file location
type Emote struct {
	Regex  string
	Images map[mkd.EmoteImageSize]string
}

// MakoStaging is the staging URL for mako
const MakoStaging = "https://main.us-west-2.beta.mako.twitch.a2z.com"

// MakoProd is the prod URL for mako
const MakoProd = "https://main.us-west-2.prod.mako.twitch.a2z.com"

// UploaderStaging is the staging URL for the uploader
const UploaderStaging = "https://staging.upload-service.twitch.a2z.com"

// UploaderProd is the prod URL for the uploader
const UploaderProd = "https://prod.upload-service.twitch.a2z.com"

func GetUploadConfigForEmote(ownerId string, autoResize, isAnimated, generateStatic bool, client mkd.Mako) (*mkd.GetEmoteUploadConfigResponse, error) {
	resizePlan := mkd.ResizePlan_no_resize
	sizes := []mkd.EmoteImageSize{}

	if autoResize {
		resizePlan = mkd.ResizePlan_auto_resize
		sizes = append(sizes, mkd.EmoteImageSize_size_original)
	} else {
		sizes = append(sizes, mkd.EmoteImageSize_size_1x, mkd.EmoteImageSize_size_2x, mkd.EmoteImageSize_size_4x)
	}

	assetType := mkd.EmoteAssetType_static

	if isAnimated {
		assetType = mkd.EmoteAssetType_animated
	}

	req := &mkd.GetEmoteUploadConfigRequest{
		UserId:                                ownerId,
		ResizePlan:                            resizePlan,
		Sizes:                                 sizes,
		AssetType:                             assetType,
		GenerateStaticVersionOfAnimatedAssets: generateStatic,
	}

	return client.GetEmoteUploadConfig(context.Background(), req)
}

func UploadImageForEmoteToStaging(ctx context.Context, imagePath, url, imageID string) error {
	imageFile, err := os.Open(imagePath)
	if err != nil {
		return err
	}

	var contentLength int64
	fileInfo, err := imageFile.Stat()

	if err != nil {
		return err
	}

	contentLength = fileInfo.Size()

	contentType := "image/png"

	extension := filepath.Ext(imagePath)
	if extension == ".gif" {
		contentType = "image/gif"
	}

	req, err := http.NewRequest(http.MethodPut, url, imageFile)

	if err != nil {
		return err
	}

	req = req.WithContext(ctx)

	req.Header.Add("Content-Type", contentType)
	req.ContentLength = contentLength

	httpClient := &http.Client{}

	resp, err := httpClient.Do(req)

	if err != nil {
		return err
	}

	if resp.StatusCode < 200 || resp.StatusCode > 299 {
		return errors.Errorf("Unexpected status code while uploading image file\n image=\"%v\"\n", imagePath)
	}

	imageFile.Close()
	resp.Body.Close()

	uploadClient := uploader.NewUploaderProtobufClient(UploaderStaging, twitchclient.NewHTTPClient(twitchclient.ClientConf{Host: UploaderStaging}))

	success := false
	var uploadErr error
	ticker := time.NewTicker(1 * time.Second)

	for i := 0; i < 5; i++ {
		resp, err := uploadClient.Status(ctx, &uploader.StatusRequest{
			UploadId: imageID,
		})

		if err != nil {
			uploadErr = err
			continue
		}

		if resp != nil {
			if resp.Status == uploader.Status_COMPLETE || resp.Status == uploader.Status_POSTPROCESS_COMPLETE {
				success = true
				uploadErr = nil
				break
			} else if resp.Status > uploader.Status_COMPLETE {
				uploadErr = errors.Errorf("Bad upload. Status: %s", resp.Status)
			}
		}

		<-ticker.C
	}

	ticker.Stop()

	if !success {
		return uploadErr
	}

	return nil
}

// UploadImagesForEmote uploads the images for the emote
func UploadImagesForEmote(emote Emote, uploadConfigurations *mkd.GetEmoteUploadConfigResponse, httpClient *http.Client, upload uploader.Uploader) error {
	// Create a context for various API actions. Currently unused, but who knows.
	ctx := context.Background()
	for _, uploadConfig := range uploadConfigurations.GetUploadConfigs() {
		imagePath := emote.Images[uploadConfig.Size]
		imageFile, err := os.Open(imagePath)
		if err != nil {
			return err
		}

		var contentLength int64
		fileInfo, err := imageFile.Stat()
		if err != nil {
			return err
		}
		contentLength = fileInfo.Size()

		req, err := http.NewRequest(http.MethodPut, uploadConfig.Url, imageFile)
		if err != nil {
			return err
		}

		req.Header.Add("Content-Type", "image/png")
		req.ContentLength = contentLength

		resp, err := httpClient.Do(req)
		if err != nil {
			return err
		}

		if resp.StatusCode < 200 || resp.StatusCode > 299 {
			// TODO: better error message
			return errors.Errorf("Unexpected status code while uploading image file\n image=\"%v\"\n", imagePath)
		}

		_ = imageFile.Close()
		_ = resp.Body.Close()
	}

	// wait on upload status of each image upload via Upload service
	var wg sync.WaitGroup
	var waitErrs []error
	var errMutex sync.Mutex

	for _, conf := range uploadConfigurations.GetUploadConfigs() {
		wg.Add(1)

		// shadow uploadID to prevent loop variable race
		uploadID := conf.Id

		// TODO: Add max waits
		go func() {
			ticker := time.NewTicker(1 * time.Second)
			defer ticker.Stop()
			defer wg.Done()

			i := 0
			for {
				i += 1
				// Timeout after two seconds. If the response has not completed in 2 seconds, it is unlikely to succeed later.
				reqCtx, cancel := context.WithTimeout(ctx, 2*time.Second)
				defer cancel()
				resp, err := upload.Status(reqCtx, &uploader.StatusRequest{
					UploadId: uploadID,
				})
				// Retry transient timeout errors. Only allow a total of 5 requests, including successful polls.
				if err != nil && reqCtx.Err() != nil && i < 5 {
					fmt.Printf("Retrying: %d, UploadID:%q, Error: %v\n", i, uploadID, reqCtx.Err())
					continue
				}
				if resp != nil {
					fmt.Printf("Attempt: %d, UploadID:%q, Status: %s\n", i, uploadID, uploader.Status_name[int32(resp.Status)])
				}
				if err != nil {
					fmt.Printf("Attempt: %d, UploadID:%q, Error: %v\n", i, uploadID, err)
				}
				if err != nil {
					errMutex.Lock()
					waitErrs = append(waitErrs, err)
					errMutex.Unlock()
					return
				}
				// Either COMPLETE or POSTPROCESS_COMPLETE might indicate success. Current reason for both appearing is unknown.
				// Eddie believes they should probably all appear POSTPROCESS_COMPLETE. (?)
				if resp.Status == uploader.Status_COMPLETE {
					return
				}
				// Codes higher than COMPLETE indicate an error
				if resp.Status > uploader.Status_COMPLETE {
					err := errors.Errorf("Uploader indicates failed workflow for emote image upload.\n  Upload ID: %s\n  Message: %s\n  Emote regex: %s\n", resp.UploadId, resp.Message, emote.Regex)
					errMutex.Lock()
					waitErrs = append(waitErrs, err)
					errMutex.Unlock()
					return
				}

				// wait on tick
				<-ticker.C
			}
		}()
	}

	wg.Wait()

	if len(waitErrs) > 0 {
		return errors.Errorf("Received error(s) while polling upload status.\n  Errors: %v\n  Emote regex: %s\n", waitErrs, emote.Regex)
	}

	return nil
}
