package emote_utils

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"html/template"
	"image"
	"image/gif"
	"image/png"
	"log"
	"time"

	"code.justin.tv/commerce/mako/clients/s3"

	s3m "code.justin.tv/commerce/mako/clients/s3manager"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	awsS3 "github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"github.com/cactus/go-statsd-client/statsd"
)

const imagePathFmt = "https://%s.s3.%s.amazonaws.com/%s"

var scales = []string{
	"1.0",
	"2.0",
	"3.0",
}

// This template produces an html file with each frame image.
// It also contains some buttons to change the background color so reviewers can check that if needed.
const tpl = `
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>{{.Title}}</title>
	</head>
	<script>
		function changeTheme(text, bg) {
			document.getElementById("main").style = "background-color:"+bg+";"
			document.getElementById("title").style = "color:"+text+";"
			Array.from(document.getElementsByClassName("scale-heading")).forEach(elem => elem.style = "color:"+text+";")
		}
		function themeChangeDark() { changeTheme("rgb(247, 247, 248)", "rgb(14, 14, 16)") }
		function themeChangeLight() { changeTheme("rgb(14, 14, 16)", "rgb(247, 247, 248)") }
		function themeChangeNeutral() { changeTheme("rgb(14, 14, 16)", "#aaa") }
	</script>
	<body id="main" style="background-color:#aaa;">
		<div style="display:flex;align-items:center;">
			<h2 id="title">{{.Title}}</h2>
			<button type="button" onclick="themeChangeLight()" style="margin: 0 10px; height:40px">Light Mode</button>
			<button type="button" onclick="themeChangeDark()" style="margin: 0 10px; height:40px">Dark Mode</button>
			<button type="button" onclick="themeChangeNeutral()" style="margin: 0 10px; height:40px">Neutral</button>
		</div>
		{{range $scale, $frames := .Images}}
			<div style="padding:10px;margin-bottom:5px">
				<h4 class="scale-heading">{{$scale}}</h4>
				<div style="display:flex;flex-wrap:wrap;">
					{{range $frames}}<div style="display:flex;margin:10px"><img src="{{ . }}"/></div>{{end}}
				</div>
			</div>
		{{end}}
	</body>
</html>`

type templateData struct {
	Title  string
	Images map[string][]string
}

type S3Uploader interface {
	UploadWithIterator(ctx context.Context, iter s3manager.BatchUploadIterator) error
	Upload(ctx context.Context, input s3manager.UploadInput) error
}

type S3Downloader interface {
	DownloadWithIterator(ctx context.Context, iter s3manager.BatchDownloadIterator) error
}

// GifFrameExtractor holds the clients for the gif extraction process.
type GifFrameExtractor struct {
	s3Downloader       S3Downloader
	s3Uploader         S3Uploader
	emoteUploadsBucket string
	framesBucket       string
	awsRegion          string
	logger             *log.Logger
	statter            *statsd.Statter
}

// NewGifFrameExtractor creates a new GifFrameExtractor with the given AWS Region, S3 Bucket, S3Uploader and S3Downloader.
func NewGifFrameExtractor(awsRegion, emoteUploadsBucket, framesBucket string, uploader S3Uploader, downloader S3Downloader, logger *log.Logger, statter *statsd.Statter) GifFrameExtractor {
	return GifFrameExtractor{
		s3Downloader:       downloader,
		s3Uploader:         uploader,
		emoteUploadsBucket: emoteUploadsBucket,
		framesBucket:       framesBucket,
		awsRegion:          awsRegion,
		logger:             logger,
		statter:            statter,
	}
}

// NewGifFrameExtractorWithS3 creates a new GifFrameExtractor with the given AWS Region and S3 Bucket. It creates new S3Uploader and S3Downloader.
func NewGifFrameExtractorWithS3(awsRegion, emoteUploadsBucket, framesBucket string, logger *log.Logger, statter *statsd.Statter) GifFrameExtractor {
	sess := session.New(&aws.Config{
		Region: aws.String(awsRegion),
	})

	downloader := s3m.NewDownloader(sess)
	uploader := s3m.NewUploader(sess)

	return GifFrameExtractor{
		s3Downloader:       downloader,
		s3Uploader:         uploader,
		emoteUploadsBucket: emoteUploadsBucket,
		framesBucket:       framesBucket,
		awsRegion:          awsRegion,
		logger:             logger,
		statter:            statter,
	}
}

// ExtractAndUploadFrames pulls the frames for the emote with the given emoteID and uploads them.
func (fe *GifFrameExtractor) ExtractAndUploadFrames(ctx context.Context, emoteID string) error {
	timer := fe.getMetricTimer("extract")
	frames, err := fe.extractFrames(ctx, emoteID)

	if err != nil {
		fe.logger.Printf("Error extracting frames for Emote: %s, err: %v\n", emoteID, err)
		return err
	}
	timer()

	timer = fe.getMetricTimer("upload_frames")
	frameKeys, err := fe.uploadFrames(ctx, emoteID, frames)

	if err != nil {
		fe.logger.Printf("Error uploading frames for Emote: %s, err: %v\n", emoteID, err)
		return err
	}
	timer()

	timer = fe.getMetricTimer("generate_index")
	err = fe.generateIndexHTML(ctx, emoteID, frameKeys)

	if err != nil {
		fe.logger.Printf("Error generating frame html doc for Emote: %s, err: %v\n", emoteID, err)
		return err
	}
	timer()

	return nil
}

func (fe *GifFrameExtractor) extractFrames(ctx context.Context, emoteID string) (map[string][]*image.Paletted, error) {
	gifs, err := fe.fetchGifs(ctx, emoteID)

	if err != nil {
		return nil, err
	}

	frames := map[string][]*image.Paletted{}

	for scale, gif := range gifs {
		frames[scale] = gif.Image
	}

	return frames, nil
}

func (fe *GifFrameExtractor) fetchGifs(ctx context.Context, emoteID string) (map[string]*gif.GIF, error) {
	// Fetch the 1x, 2x and 4x gif assets. The assets will be the same for light and dark mode so we just grab the light mode here.
	// In the future if we decide to have separate assets for light/dark mode we will need to fetch both.

	buffers := map[string]*aws.WriteAtBuffer{}
	downloads := []s3manager.BatchDownloadObject{}

	for _, scale := range scales {
		writer := aws.NewWriteAtBuffer([]byte{})
		buffers[scale] = writer

		downloads = append(downloads, s3manager.BatchDownloadObject{
			Object: &awsS3.GetObjectInput{
				Bucket: aws.String(fe.emoteUploadsBucket),
				Key:    aws.String(s3.GetAnimatedEmoteS3Key(emoteID, "light", scale)),
			},
			Writer: writer,
		})
	}

	err := fe.s3Downloader.DownloadWithIterator(ctx, &s3manager.DownloadObjectsIterator{
		Objects: downloads,
	})

	if err != nil {
		if batcherr, ok := err.(*s3manager.BatchError); ok {
			if batcherr.Code() == awsS3.ErrCodeNoSuchKey {
				// If one or more image assets are not a gif or emote does not exist then bail out.
				notFound := []string{}
				for _, aerr := range batcherr.Errors {
					notFound = append(notFound, *aerr.Key)
				}
				return nil, fmt.Errorf("\nGif(s) (%v) could not be found", notFound)
			}
		}
		return nil, err
	}

	gifs := map[string]*gif.GIF{}

	for scale, buffer := range buffers {
		body := buffer.Bytes()

		if body == nil || len(body) == 0 {
			return nil, fmt.Errorf("invalid image for scale %s: body has no contents", scale)
		}

		gifImage, err := gif.DecodeAll(bytes.NewReader(body))

		if err != nil {
			return nil, err
		}

		gifs[scale] = gifImage
	}

	return gifs, nil
}

// uploadFrames returns the S3 Keys for all of the uploaded frames.
func (fe *GifFrameExtractor) uploadFrames(ctx context.Context, emoteID string, frames map[string][]*image.Paletted) (map[string][]string, error) {
	if len(frames) < 1 {
		return nil, errors.New("No frames found from the gif")
	}

	uploads := []s3manager.BatchUploadObject{}
	keysByScale := map[string][]string{}

	folder := s3.GetAnimatedFrameFolderS3Key(emoteID)

	for scale, framesForScale := range frames {
		keys := []string{}

		// For each frame we need to encode it as a png and batch it for upload.
		for idx, img := range framesForScale {
			buffer := bytes.NewBuffer([]byte{})
			err := png.Encode(buffer, img)

			if err != nil {
				return nil, err
			}

			key := GetFrameS3Key(folder, scale, idx)
			keys = append(keys, key)

			uploads = append(uploads, s3manager.BatchUploadObject{
				Object: &s3manager.UploadInput{
					ACL:         aws.String(awsS3.ObjectCannedACLPublicRead),
					Body:        buffer,
					Key:         aws.String(key),
					ContentType: aws.String("image/png"),
					Bucket:      aws.String(fe.framesBucket),
				},
			})
		}
		keysByScale[scale] = keys
	}

	err := fe.s3Uploader.UploadWithIterator(ctx, &s3manager.UploadObjectsIterator{
		Objects: uploads,
	})

	if err != nil {
		return nil, err
	}

	return keysByScale, nil
}

func (fe *GifFrameExtractor) generateIndexHTML(ctx context.Context, emoteID string, frameKeys map[string][]string) error {
	t, err := template.New("frames").Parse(tpl)

	if err != nil {
		return err
	}

	imgsByScale := map[string][]string{}

	for scale, keys := range frameKeys {
		imgSrcs := []string{}
		for _, key := range keys {
			imgSrcs = append(imgSrcs, fmt.Sprintf(imagePathFmt, fe.framesBucket, fe.awsRegion, key))
		}
		imgsByScale[scale] = imgSrcs
	}

	data := templateData{
		Title:  fmt.Sprintf("emote-%s", emoteID),
		Images: imgsByScale,
	}

	buffer := bytes.NewBuffer([]byte{})
	err = t.Execute(buffer, data)

	if err != nil {
		return err
	}

	key := GetFrameIndexHTMLS3Key(emoteID)

	err = fe.s3Uploader.Upload(ctx, s3manager.UploadInput{
		ACL:         aws.String(awsS3.ObjectCannedACLPublicRead),
		ContentType: aws.String("text/html"),
		Bucket:      aws.String(fe.framesBucket),
		Key:         aws.String(key),
		Body:        bytes.NewReader(buffer.Bytes()),
	})

	return err
}

func (fe *GifFrameExtractor) getMetricTimer(metricName string) func() {
	if fe.statter == nil {
		return func() {}
	}
	start := time.Now()
	return func() {
		(*fe.statter).TimingDuration(fmt.Sprintf("animated_emote_frame_extract.%s", metricName), time.Since(start), 1.0)
	}
}

// GetFrameS3Key returns the S3 Key for a single frame of an animated emote.
func GetFrameS3Key(folder, scale string, idx int) string {
	return fmt.Sprintf("%s/frame%s-%d.png", folder, scale, idx)
}

// GetFrameIndexHTMLS3Key returns the S3 Key for the html file that displays all frames for a given animated emote.
func GetFrameIndexHTMLS3Key(emoteID string) string {
	return fmt.Sprintf("%s/index.html", s3.GetAnimatedFrameFolderS3Key(emoteID))
}
