package redis

import (
	"context"
	"encoding/json"
	"fmt"
	"os"
	"time"

	"code.justin.tv/chat/rediczar"
	"code.justin.tv/chat/rediczar/redefault"
	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/logrus"
	mako "code.justin.tv/commerce/mako/internal"
	"code.justin.tv/commerce/mako/tests"
	"github.com/afex/hystrix-go/hystrix"
	"github.com/go-redis/redis/v7"
	"github.com/pkg/errors"
	"github.com/segmentio/ksuid"
)

const (
	EmoteModifiersDBUserHystrix  = "redis.emoteModifierGroups.users.GET"
	EmoteModifiersDBOwnerHystrix = "redis.emoteModifierGroups.owners.GET"

	emoteModCacheTTL       = 10 * time.Minute
	emoteModEntitlementTTL = 2 * time.Minute
)

type EmoteModifiersDBCacheConfig struct {
	Addr      string
	Namespace string
	// Bool to determine whether to use redis cluster mode. If 'false', cluster mode will be disabled.
	ClusterEnabled bool
	// Determines the max size of the connection pool to redis.
	ConnectionPoolSize int
	MinIdleConnections int
}

type EmoteModifiersDBCache struct {
	ns string

	// base is the underlying implementation that the cache is decorating.
	base mako.EmoteModifiersDB

	// client is either a Redis cluster or single Redis server instance.
	client *rediczar.Client
}

func NewEmoteModifiersDBWithCache(base mako.EmoteModifiersDB, c EmoteModifiersDBCacheConfig) *EmoteModifiersDBCache {
	var cmdable redis.Cmdable

	// Default from rediczar
	var connectionPoolSize = 30
	if c.ConnectionPoolSize > 0 {
		connectionPoolSize = c.ConnectionPoolSize
	}

	if c.ClusterEnabled {
		cmdable = redefault.NewClusterClient(c.Addr, &redefault.ClusterOpts{
			ReadOnly:     true,
			PoolSize:     connectionPoolSize,
			MinIdleConns: c.MinIdleConnections,
		})
	} else {
		cmdable = redis.NewClient(&redis.Options{Addr: c.Addr})
	}

	client := &rediczar.Client{
		Commands: &rediczar.PrefixedCommands{
			Redis:     cmdable,
			KeyPrefix: c.Namespace,
		},
	}

	return &EmoteModifiersDBCache{
		base:   base,
		ns:     c.Namespace,
		client: client,
	}
}

func (db *EmoteModifiersDBCache) CreateGroups(ctx context.Context, groups ...mako.EmoteModifierGroup) ([]mako.EmoteModifierGroup, error) {
	groups, err := db.base.CreateGroups(ctx, groups...)
	if err != nil {
		return nil, err
	}
	var ownerIDs []string
	var ids []string
	for _, group := range groups {
		ownerIDs = append(ownerIDs, group.OwnerID)
		ids = append(ids, group.ID)
	}

	db.invalidateCacheByOwnerIDs(ctx, ownerIDs...)
	db.invalidateCacheForIDs(ctx, ids...)
	return groups, nil
}

func (db *EmoteModifiersDBCache) UpdateGroups(ctx context.Context, groups ...mako.EmoteModifierGroup) ([]mako.EmoteModifierGroup, error) {
	groups, err := db.base.UpdateGroups(ctx, groups...)
	if err != nil {
		return nil, err
	}
	var ownerIDs []string
	var ids []string
	for _, group := range groups {
		ownerIDs = append(ownerIDs, group.OwnerID)
		ids = append(ids, group.ID)
	}

	db.invalidateCacheByOwnerIDs(ctx, ownerIDs...)
	db.invalidateCacheForIDs(ctx, ids...)
	return groups, nil
}

func (db *EmoteModifiersDBCache) ReadByIDs(ctx context.Context, ids ...string) ([]mako.EmoteModifierGroup, error) {
	if len(ids) == 0 {
		return nil, errors.New("empty ownerID list given")
	}

	var allGroups []mako.EmoteModifierGroup
	keys := make([]string, 0, len(ids))
	for _, id := range ids {
		keys = append(keys, makeEmoteModifierIDCacheKey(id))
	}

	var staleIDs []string
	var cachedGroups []interface{}
	if err := hystrix.DoC(ctx, EmoteModifiersDBOwnerHystrix, func(ctx context.Context) error {
		var redisErr error
		cachedGroups, redisErr = db.client.PipelinedGet(ctx, keys...)
		return redisErr
	}, nil); err != nil {
		logrus.WithField("ids", ids).WithError(err).Error("could not retrieve cached emote modifier groups by ids")
		// All keys are stale, so fallback to dynamo for each key.
		staleIDs = append(staleIDs, ids...)
	}

	for i, groupJson := range cachedGroups {
		// A nil record in a PipelinedGet response means a cache miss.
		if groupJson == nil {
			staleIDs = append(staleIDs, ids[i])
		} else {
			var group mako.EmoteModifierGroup
			if err := json.Unmarshal([]byte(groupJson.(string)), &group); err != nil {
				staleIDs = append(staleIDs, ids[i])
			} else {
				allGroups = append(allGroups, group)
			}
		}
	}

	if len(staleIDs) > 0 {
		groups, err := db.base.ReadByIDs(ctx, staleIDs...)
		if err != nil {
			return nil, err
		}
		allGroups = append(allGroups, groups...)

		// Cache groups to Redis
		go func() {
			var pairs []interface{}
			for _, group := range groups {
				groupJson, err := json.Marshal(group)
				if err != nil {
					logrus.WithField("pairs", pairs).WithError(err).Error(err, "could not write group to emote modifiers cache")
				} else {
					pairs = append(pairs, makeEmoteModifierIDCacheKey(group.ID), groupJson)
				}
			}

			ctx, cancel := context.WithTimeout(context.Background(), time.Second)
			defer cancel()

			ttl := jitterDuration(emoteModCacheTTL, 0.75, 1.0)
			if err := db.client.MSetWithTTL(ctx, ttl, pairs...); err != nil {
				logrus.WithField("pairs", pairs).WithError(err).Error(err, "could not write group to emote modifiers cache")
			}
		}()
	}

	return allGroups, nil
}

func (db *EmoteModifiersDBCache) ReadByOwnerIDs(ctx context.Context, ownerIDs ...string) ([]mako.EmoteModifierGroup, error) {
	if len(ownerIDs) == 0 {
		return nil, errors.New("empty ownerID list given")
	}

	var staleOwnerIDs []string
	keys := make([]string, 0, len(ownerIDs))
	for _, id := range ownerIDs {
		keys = append(keys, makeEmoteModifierOwnerCacheKey(id))
	}
	var cachedResults []interface{}
	if err := hystrix.DoC(ctx, EmoteModifiersDBOwnerHystrix, func(ctx context.Context) error {
		var redisErr error
		cachedResults, redisErr = db.client.PipelinedGet(ctx, keys...)
		return redisErr
	}, nil); err != nil {
		logrus.WithField("ownerIDs", ownerIDs).WithError(err).Error("could not retrieve cached emote modifier groups by ownerID")
		// All keys are stale, so fallback to dynamo for each key.
		staleOwnerIDs = append(staleOwnerIDs, ownerIDs...)
	}

	groupsByOwnerMap := map[string][]mako.EmoteModifierGroup{}

	// If staleOwnerIDs is already populated, that means the
	// cache retrieval failed and this can be skipped entirely.
	if len(staleOwnerIDs) == 0 {
		for i, result := range cachedResults {
			// A nil record in a PipelinedGet response means a cache miss.
			if result == nil {
				staleOwnerIDs = append(staleOwnerIDs, ownerIDs[i])
				continue
			}
			var groupsByOwner []mako.EmoteModifierGroup
			if err := json.Unmarshal([]byte(result.(string)), &groupsByOwner); err != nil {
				staleOwnerIDs = append(staleOwnerIDs, ownerIDs[i])
			} else {
				groupsByOwnerMap[ownerIDs[i]] = groupsByOwner
			}
		}
	}

	if len(staleOwnerIDs) > 0 {
		// Retrieve non-cached emote mod groups from base.
		groups, err := db.base.ReadByOwnerIDs(ctx, staleOwnerIDs...)
		if err != nil {
			return nil, errors.Wrap(err, "could not retrieve emote modifier groups from base")
		}

		for _, group := range groups {
			groupsByOwnerMap[group.OwnerID] = append(groupsByOwnerMap[group.OwnerID], group)
		}

		// Cache groups to Redis
		go func() {
			var pairs []interface{}
			for _, ownerID := range staleOwnerIDs {
				groupsJson, err := json.Marshal(groupsByOwnerMap[ownerID])
				if err != nil {
					logrus.WithField("pairs", pairs).WithError(err).Error(err, "could not write to emote modifiers cache for owners")
				} else {
					pairs = append(pairs, makeEmoteModifierOwnerCacheKey(ownerID), groupsJson)
				}
			}

			ctx, cancel := context.WithTimeout(context.Background(), time.Second)
			defer cancel()

			ttl := jitterDuration(emoteModCacheTTL, 0.75, 1.0)
			if err := db.client.MSetWithTTL(ctx, ttl, pairs...); err != nil {
				logrus.WithField("pairs", pairs).WithError(err).Error(err, "could not write to emote modifiers cache for owners")
			}
		}()
	}

	var allGroups []mako.EmoteModifierGroup
	for _, ownerID := range ownerIDs {
		allGroups = append(allGroups, groupsByOwnerMap[ownerID]...)
	}
	return allGroups, nil
}

func (db *EmoteModifiersDBCache) DeleteByIDs(ctx context.Context, ids ...string) error {
	groups, err := db.base.ReadByIDs(ctx, ids...)
	if err != nil {
		return err
	}
	var ownerIDs []string
	for _, group := range groups {
		ownerIDs = append(ownerIDs, group.OwnerID)
	}

	if err := db.base.DeleteByIDs(ctx, ids...); err != nil {
		return err
	}

	db.invalidateCacheForIDs(ctx, ids...)
	db.invalidateCacheByOwnerIDs(ctx, ownerIDs...)

	return nil
}

func (db *EmoteModifiersDBCache) ReadEntitledGroupsForUser(ctx context.Context, userID string) ([]mako.EmoteModifierGroup, error) {
	var allGroups []mako.EmoteModifierGroup
	var entitledGroupsCacheValue string
	if err := hystrix.DoC(ctx, EmoteModifiersDBUserHystrix, func(ctx context.Context) error {
		var redisErr error
		entitledGroupsCacheValue, redisErr = db.client.Get(ctx, makeEntitledGroupsCacheKey(userID))
		if redisErr != nil && redisErr != redis.Nil {
			return redisErr
		}

		return nil
	}, nil); err != nil {
		return nil, errors.Wrapf(err, "could not retrieve cached emote modifier groups entitlements for user %s", userID)
	}

	if strings.Blank(entitledGroupsCacheValue) {
		return nil, nil
	}

	if err := json.Unmarshal([]byte(entitledGroupsCacheValue), &allGroups); err != nil {
		return nil, errors.Wrapf(err, "could not unmarshal cached emote modifier groups entitlements for user %s (cache value %s)", userID, entitledGroupsCacheValue)
	}
	return allGroups, nil
}

func (db *EmoteModifiersDBCache) WriteEntitledGroupsForUser(ctx context.Context, userID string, groups ...mako.EmoteModifierGroup) error {
	if len(groups) == 0 {
		return nil
	}

	groupsJSON, err := json.Marshal(groups)
	if err != nil {
		return err
	}
	if _, err := db.client.Set(ctx, makeEntitledGroupsCacheKey(userID), groupsJSON, emoteModEntitlementTTL); err != nil {
		return err
	}

	return nil
}

func (db *EmoteModifiersDBCache) InvalidateEntitledGroupsCacheForUser(userID string) error {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
	defer cancel()

	_, err := db.client.Del(ctx, makeEntitledGroupsCacheKey(userID))
	return err
}

func (db *EmoteModifiersDBCache) invalidateCacheByOwnerIDs(ctx context.Context, ownerIDs ...string) {
	if len(ownerIDs) == 0 {
		return
	}

	ownerIDKeys := make([]string, 0, len(ownerIDs))
	for _, ownerID := range ownerIDs {
		ownerIDKeys = append(ownerIDKeys, makeEmoteModifierOwnerCacheKey(ownerID))
	}

	if _, err := db.client.PipelinedDel(ctx, ownerIDKeys...); err != nil {
		logrus.WithField("ownerIDs", ownerIDs).WithError(err).Error(err, "could not invalidate emote modifiers cache for owners")
	}

}

func (db *EmoteModifiersDBCache) invalidateCacheForIDs(ctx context.Context, ids ...string) {
	if len(ids) == 0 {
		return
	}

	idKeys := make([]string, 0, len(ids))
	for _, id := range ids {
		idKeys = append(idKeys, makeEmoteModifierIDCacheKey(id))
	}

	if _, err := db.client.PipelinedDel(ctx, idKeys...); err != nil {
		logrus.WithField("ids", ids).WithError(err).Error(err, "could not invalidate emote modifiers cache for ids")
	}
}

// This should technically be `emoteModifierGroupOwner:*`. Leaving for now until we can safely migrate to a new key.
func makeEmoteModifierOwnerCacheKey(id string) string {
	return fmt.Sprintf("emoteModifierGroup:%s", id)
}

func makeEmoteModifierIDCacheKey(id string) string {
	return fmt.Sprintf("emoteModifierGroupID:%s", id)
}

func makeEntitledGroupsCacheKey(userID string) string {
	return fmt.Sprintf("entitledGroups:%s", userID)
}

func MakeEmoteModifiersDBCacheForTesting(makeDB tests.MakeEmoteModifiersDB) tests.MakeEmoteModifiersDB {
	return func(ctx context.Context) (db mako.EmoteModifiersDB, err error) {
		addr := "localhost:6379"

		for _, index := range defaultIndices {
			hystrix.ConfigureCommand(index.hystrix, hystrix.CommandConfig{
				Timeout: 5000,
			})
		}

		// Jenkins require a different Redis address for tests.
		if os.Getenv("DOCKER_TEST") != "" {
			addr = "jenkins_redis:6379"
		}

		// Run the tests in a separate namespace, ensuring tests can run in parallel.
		ns := ksuid.New().String()
		base, err := makeDB(ctx)
		if err != nil {
			return nil, errors.Wrap(err, "failed to initialize base EmoteModifiersDB")
		}

		db = NewEmoteModifiersDBWithCache(base, EmoteModifiersDBCacheConfig{
			Addr:           addr,
			Namespace:      ns,
			ClusterEnabled: false,
		})

		return db, nil
	}
}
