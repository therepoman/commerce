package redis

import (
	"context"
	"testing"

	"code.justin.tv/commerce/mako/dynamo"
	mako "code.justin.tv/commerce/mako/internal"
	"code.justin.tv/commerce/mako/localdb"
	"code.justin.tv/commerce/mako/tests"
)

func TestRedisEmoteDBCacheLocal(t *testing.T) {
	tests.TestEmoteDB(t, MakeEmoteDB(localdb.MakeEmoteDB))
}

func TestRedisEmoteDBCacheDynamo(t *testing.T) {
	tests.TestEmoteDB(t, MakeEmoteDB(dynamo.MakeEmoteDB))
}

// TestCachedEmoteDBWithLocal runs the caching test suite against the Redis cache decorator
// while using a local implementation of the `EmoteDB`.
func TestCachedEmoteDBWithLocal(t *testing.T) {
	tests.TestCachedEmoteDB(t, tests.CachedEmoteDBConfig{
		Base:  localdb.MakeEmoteDB,
		Cache: makeEmoteDBCache,
	})
}

// TestCachedEmoteDBWithDynamo runs the caching test suite against the Redis cache decorator
// while using the real DynamoDB implementation of the `EmoteDB`. This is effectively an integration test
// to ensure combining both Redis + DynamoDB works correctly.
func TestCachedEmoteDBWithDynamo(t *testing.T) {
	tests.TestCachedEmoteDB(t, tests.CachedEmoteDBConfig{
		Base:  dynamo.MakeEmoteDB,
		Cache: makeEmoteDBCache,
	})
}

func makeEmoteDBCache(base mako.EmoteDB) tests.MakeEmoteDB {
	return MakeEmoteDB(func(ctx context.Context, emotes ...mako.Emote) (mako.EmoteDB, func(), error) {
		return base, func() {}, nil
	})
}

func TestLocalEmoteDBWithRedisCacheEmoteCodes(t *testing.T) {
	tests.TestEmoteCodes(t, tests.EmoteCodesConfig{
		MakeEmoteDB:         MakeEmoteDB(localdb.MakeEmoteDB),
		MakeEntitlementDB:   localdb.MakeEntitlementDB,
		MakeEmoteModifierDB: localdb.MakeEmoteModifierDB,
	})
}

func TestDynamoEmoteDBWithRedisCacheEmoteCodes(t *testing.T) {
	tests.TestEmoteCodes(t, tests.EmoteCodesConfig{
		MakeEmoteDB:         MakeEmoteDB(dynamo.MakeEmoteDB),
		MakeEntitlementDB:   localdb.MakeEntitlementDB,
		MakeEmoteModifierDB: localdb.MakeEmoteModifierDB,
	})
}

func TestLocalEmoteDBWithRedisCacheUserChannelEmotes(t *testing.T) {
	tests.TestUserChannelEmotes(t, tests.UserChannelEmotesConfig{
		MakeEmoteDB:         MakeEmoteDB(localdb.MakeEmoteDB),
		MakeEntitlementDB:   localdb.MakeEntitlementDB,
		MakeEmoteModifierDB: localdb.MakeEmoteModifierDB,
	})
}

func TestDynamoEmoteDBWithRedisCacheUserChannelEmotes(t *testing.T) {
	tests.TestUserChannelEmotes(t, tests.UserChannelEmotesConfig{
		MakeEmoteDB:         MakeEmoteDB(dynamo.MakeEmoteDB),
		MakeEntitlementDB:   localdb.MakeEntitlementDB,
		MakeEmoteModifierDB: localdb.MakeEmoteModifierDB,
	})
}
