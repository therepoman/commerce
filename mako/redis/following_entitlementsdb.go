package redis

import (
	"context"
	"encoding/json"
	"fmt"
	"os"
	"time"

	"code.justin.tv/commerce/mako/config"

	followsrpc "code.justin.tv/amzn/TwitchVXFollowingServiceECSTwirp"
	"code.justin.tv/chat/rediczar"
	"code.justin.tv/chat/rediczar/redefault"
	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/mako/clients/following"
	mako "code.justin.tv/commerce/mako/internal"
	"github.com/afex/hystrix-go/hystrix"
	"github.com/go-redis/redis/v7"
	"github.com/pkg/errors"
	"github.com/segmentio/ksuid"
)

const (
	FollowingEntitlementsOwnerCacheKey  = "followingEntitlementsOwner"
	FollowingEntitlementsIDCacheKey     = "followingEntitlementsID"
	FollowingEntitlementsDBOwnerHystrix = "redis.followingEntitlements.owners.GET"
	FollowingEntitlementsDBIDHystrix    = "redis.followingEntitlements.id.GET"

	followingEntitlementsTTL = 10 * time.Minute
)

type FollowingEntitlementsDBCache struct {
	ns string

	// base is the underlying implementation that the cache is decorating.
	base mako.EntitlementDB

	// client is either a Redis cluster or single Redis server instance.
	client *rediczar.Client
}

type FollowingEntitlementsDBCacheConfig struct {
	Addr      string
	Namespace string
	// Bool to determine whether to use redis cluster mode. If 'false', cluster mode will be disabled.
	ClusterEnabled bool
	// Determines the max size of the connection pool to redis.
	ConnectionPoolSize int
	MinIdleConnections int
}

func NewFollowingEntitlementsDBWithCache(base mako.EntitlementDB, c FollowingEntitlementsDBCacheConfig) *FollowingEntitlementsDBCache {
	var cmdable redis.Cmdable

	// Default from rediczar
	var connectionPoolSize = 30
	if c.ConnectionPoolSize > 0 {
		connectionPoolSize = c.ConnectionPoolSize
	}

	if c.ClusterEnabled {
		cmdable = redefault.NewClusterClient(c.Addr, &redefault.ClusterOpts{
			ReadOnly:     true,
			PoolSize:     connectionPoolSize,
			MinIdleConns: c.MinIdleConnections,
		})
	} else {
		cmdable = redis.NewClient(&redis.Options{Addr: c.Addr})
	}

	client := &rediczar.Client{
		Commands: &rediczar.PrefixedCommands{
			Redis:     cmdable,
			KeyPrefix: c.Namespace,
		},
	}

	return &FollowingEntitlementsDBCache{
		base:   base,
		ns:     c.Namespace,
		client: client,
	}
}

func (db *FollowingEntitlementsDBCache) ReadByIDs(ctx context.Context, ownerID string, keys ...string) ([]mako.Entitlement, error) {
	if keys == nil {
		return nil, nil
	}

	// A user can only be on one channel at a time, and we only allow follower emotes in a singular channel
	if len(keys) > 1 {
		return nil, errors.New("follower emotes are only supported in a single channel at a time")
	}

	key := keys[0]
	keyStale := false
	var cachedResult string

	if err := hystrix.DoC(ctx, FollowingEntitlementsDBIDHystrix, func(ctx context.Context) error {
		var redisErr error
		cachedResult, redisErr = db.client.Get(ctx, makeFollowingEntitlementsIDCacheKey(key))
		// Key not found errors are returned as redis.Nil. With no negative caching, this could return an influx of
		// errors and cause the hystrix circuit to open. Instead, of returning error on key not found, we can
		// set a stale key and return nil instead.
		if redisErr == redis.Nil {
			keyStale = true
			return nil
		}
		return redisErr
	}, nil); err != nil {
		logrus.WithField("key", key).WithError(err).Error("could not retrieve cached follower entitlements by ids")
		keyStale = true
	}

	var entitlements []mako.Entitlement

	// If key is stale, cache retrieval failed and this section can be skipped.
	if !keyStale {
		var entitlement mako.Entitlement
		// Failing to unmarshal the data could mean a cache miss or no follower entitlements.
		if err := json.Unmarshal([]byte(cachedResult), &entitlement); err != nil {
			keyStale = true
		} else {
			entitlements = append(entitlements, entitlement)
		}
	}

	// Retrieve non-cached Following entitlements from base if key is stale.
	if keyStale {
		followerEntitlements, err := db.base.ReadByIDs(ctx, ownerID, keys...)
		if err != nil {
			return nil, err
		}
		entitlements = followerEntitlements

		// Cache Following entitlements to Redis.
		go func() {
			// There can only ever be 0 or 1 following entitlements.
			if len(entitlements) > 0 {
				entitlementsJson, err := json.Marshal(entitlements[0])

				// NOTE: We do not do any negative caching here. A good portion of traffic to this flow will be
				// user/channel pairs where the user is not following.  If we implemented negative caching, it could
				// cause a 10 minutes delay (or TTL) after following before the cache is invalidated and the user
				// is able to see their follower emotes.
				// If we do intend to add negative caching in the future, we can try reducing the TTL to something
				// that product is comfortable with, or hook into following events in Mako to invalidate the cache.
				if err != nil {
					logrus.WithError(err).Error(err, "could not write to following entitlements cache for owners")
				} else {
					ctx, cancel := context.WithTimeout(context.Background(), time.Second)
					defer cancel()

					ttl := jitterDuration(followingEntitlementsTTL, 0.75, 1.0)
					if _, err := db.client.Set(ctx, makeFollowingEntitlementsIDCacheKey(key), entitlementsJson, ttl); err != nil {
						logrus.WithField("cacheValue", entitlementsJson).WithError(err).Error(err, "could not write to following entitlements cache for owners")
					}
				}
			}
		}()
	}

	return entitlements, nil
}

// At most, we allow one Follower Emote Entitlement per user-channel pair, so we will only ever return 0-1 entitlements here
func (db *FollowingEntitlementsDBCache) ReadByOwnerID(ctx context.Context, ownerID string, channelID string) ([]mako.Entitlement, error) {
	ownerIDStale := false
	var cachedResult string

	if err := hystrix.DoC(ctx, FollowingEntitlementsDBOwnerHystrix, func(ctx context.Context) error {
		var redisErr error
		cachedResult, redisErr = db.client.Get(ctx, makeFollowingEntitlementsOwnerCacheKey(ownerID))
		// Key not found errors are returned as redis.Nil. With no negative caching, this could return an influx of
		// errors and cause the hystrix circuit to open. Instead, of returning error on key not found, we can
		// set a stale key and return nil instead.
		if redisErr == redis.Nil {
			ownerIDStale = true
			return nil
		}
		return redisErr
	}, nil); err != nil {
		logrus.WithField("ownerID", ownerID).WithError(err).Error("could not retrieve cached follower entitlements by ownerID")
		ownerIDStale = true
	}

	var entitlements []mako.Entitlement

	// If ownerID is stale, cache retrieval failed and this section can be skipped.
	if !ownerIDStale {
		var entitlement mako.Entitlement
		// Failing to unmarshal the data could mean a cache miss or no follower entitlements.
		if err := json.Unmarshal([]byte(cachedResult), &entitlement); err != nil {
			ownerIDStale = true
		} else {
			entitlements = append(entitlements, entitlement)
		}
	}

	// Retrieve non-cached Following entitlements from base if ownerID is stale.
	if ownerIDStale {
		// There can only be 0 or 1 Following entitlements.
		followerEntitlements, err := db.base.ReadByOwnerID(ctx, ownerID, channelID)
		if err != nil {
			return nil, err
		}
		entitlements = followerEntitlements

		// Cache Following entitlements to Redis.
		go func() {
			// There can only ever be 0 or 1 following entitlements.
			if len(entitlements) > 0 {
				entitlementsJson, err := json.Marshal(entitlements[0])

				// NOTE: We do not do any negative caching here. A good portion of traffic to this flow will be
				// user/channel pairs where the user is not following.  If we implemented negative caching, it could
				// cause a 10 minutes delay (or TTL) after following before the cache is invalidated and the user
				// is able to see their follower emotes.
				// If we do intend to add negative caching in the future, we can try reducing the TTL to something
				// that product is comfortable with, or hook into following events in Mako to invalidate the cache.
				if err != nil {
					logrus.WithError(err).Error(err, "could not write to following entitlements cache for owners")
				} else {
					ctx, cancel := context.WithTimeout(context.Background(), time.Second)
					defer cancel()

					ttl := jitterDuration(followingEntitlementsTTL, 0.75, 1.0)
					if _, err := db.client.Set(ctx, makeFollowingEntitlementsIDCacheKey(ownerID), entitlementsJson, ttl); err != nil {
						logrus.WithField("cacheValue", entitlementsJson).WithError(err).Error(err, "could not write to following entitlements cache for owners")
					}
				}
			}
		}()
	}

	return entitlements, nil
}

func (db *FollowingEntitlementsDBCache) WriteEntitlements(ctx context.Context, entitlements ...mako.Entitlement) error {
	// We do not allow the writing of Follower Only Emote Entitlements in Mako so we do not support any caching. This
	// return a "follower emote entitlements aren't stored anywhere" error.
	return db.base.WriteEntitlements(ctx, entitlements...)
}

func makeFollowingEntitlementsOwnerCacheKey(id string) string {
	return fmt.Sprintf(FollowingEntitlementsOwnerCacheKey+":%s", id)
}

func makeFollowingEntitlementsIDCacheKey(id string) string {
	return fmt.Sprintf(FollowingEntitlementsIDCacheKey+":%s", id)
}

func MakeFollowingEntitlementsDBCacheForTest(client followsrpc.TwitchVXFollowingServiceECS, knownOrVerifiedBotsConfig config.KnownOrVerifiedBotsConfigClient, emoteGroupDB mako.EmoteGroupDB) (db mako.EntitlementDB) {
	baseDB := following.MakeFollowingEntitlementDBForTest(client, knownOrVerifiedBotsConfig, emoteGroupDB)

	addr := "localhost:6379"
	// Jenkins require a different Redis address for tests.
	if os.Getenv("DOCKER_TEST") != "" {
		addr = "jenkins_redis:6379"
	}

	// Run the tests in a separate namespace, ensuring tests can run in parallel.
	ns := ksuid.New().String()

	return NewFollowingEntitlementsDBWithCache(baseDB, FollowingEntitlementsDBCacheConfig{
		Addr:           addr,
		Namespace:      ns,
		ClusterEnabled: false,
	})
}
