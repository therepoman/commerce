package redis

import (
	"context"
	"encoding/json"
	"fmt"
	"os"
	"strings"
	"sync"
	"time"

	"code.justin.tv/chat/rediczar"
	"code.justin.tv/chat/rediczar/redefault"
	gadgetstrings "code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/logrus"
	log "code.justin.tv/commerce/logrus"
	mako "code.justin.tv/commerce/mako/internal"
	"code.justin.tv/commerce/mako/tests"
	"github.com/afex/hystrix-go/hystrix"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/cenkalti/backoff"
	"github.com/go-redis/redis/v7"
	"github.com/pkg/errors"
	"github.com/segmentio/ksuid"
	"github.com/willf/bloom"
)

const (
	numBitsForBloomFilter   = 190000000
	numHashesForBloomFilter = 13
)

var (
	// defaultNamespace is used to prefix keys in Redis. Namespaces are used so that tests can use a different namespace
	// for each run, making them isolated.
	defaultNamespace               = "mako"
	EmoteDBHystrix                 = "redis.emotes.GET"
	EmoteDBCodeHystrix             = "redis.emotes.codes.GET"
	EmoteDBCodeSetHystrix          = "redis.emotes.codesSet.GET"
	bloomFilterRefresh             = 5 * time.Minute
	bloomFilterInitialRefreshDelay = 30 * time.Second

	// indexByID is a primary index in Redis stored as individual keys. The key contains an array of emoticons.
	// 	e.g., idIndex:$id => "[{...}]"
	// Invalidation: The key is deleted anytime the emote is updated.
	indexByID = index{
		name:    "idIndex",
		ttl:     10 * time.Minute,
		hystrix: EmoteDBHystrix,
	}

	// indexByCode is a primary index in Redis stored as individual keys. The key contains an array of emoticons.
	// 	e.g., codeIndex:$code => "[{...}]"
	// Invalidation: The key is deleted anytime the emote is updated.
	indexByCode = index{
		name:    "codeIndex",
		ttl:     24 * time.Hour,
		hystrix: EmoteDBCodeHystrix,
	}

	// indexByGroupID is a primary index in Redis stored as individual keys. The key contains an array of emoticons.
	// 	e.g., groupIdIndex:$groupID => "[{...}]"
	// Invalidation: The key is deleted anytime an emote within the group is created, updated, or deleted.
	indexByGroupID = index{
		name:    "groupIdIndex",
		ttl:     24 * time.Hour,
		hystrix: EmoteDBHystrix,
	}

	// indexByCodeSet is an index stored in Redis that is a large set that only contains emote codes so that we can easily
	// check if an emote code exists in Mako or not.
	//
	// **NOTE**
	//
	// This index is akin to a bloom filter. It is not a source of truth for emote codes and is only used as an optimization
	// to avoid fetching known non emote codes from the underlying datastore. The index will contain all known emote codes
	// over time so even if an emote was deleted and a new emote wants to use the same code, this index will allow requests
	// to go to the underlying store to be the source of truth.
	//
	// Invalidation: The key is never invalidated as it needs to contain all the emote codes in the set.
	indexByCodeSet = index{
		name:      "codeSetIndex",
		hystrix:   EmoteDBCodeSetHystrix,
		longlived: true,
	}
)

type index struct {
	name string
	ttl  time.Duration
	// longlived signals if the index expires at all or not.
	longlived bool
	hystrix   string
}

var (
	defaultIndices = []index{
		indexByID,
		indexByCode,
		indexByGroupID,
		indexByCodeSet,
	}
)

// EmoteDBCache is a decorator that extends the functionality of an `EmoteDB` by caching the data in Redis.
// Data is cached on reads and invalidated on writes.
type EmoteDBCache struct {
	// ns provides a key namespace, mostly used for tests.
	ns string

	// base is the underlying implementation that the cache is decorating.
	base mako.EmoteDB

	// client is either a Redis cluster or single Redis server instance.
	// NOTE: DO NOT use this client for SCAN or SSCAN operations. Read-replicas are enabled on this client and will
	// not give consistent results across batches.
	client *rediczar.Client

	// primaryOnlyClient is either a Redis cluster or single Redis server instance. It has the Read-Only flag turned
	// off in order to support strongly-consistent reads, and will use only the primary node(s) for doing iterative scan operations.
	primaryOnlyClient *rediczar.Client

	// filter stores all known emote codes so requests can easily ignore non-emote codes. Each filter takes up
	// a very small amount of memory so we don't need to store all the emote codes in-memory.
	filter   *bloom.BloomFilter
	filterMu sync.RWMutex

	// ready signals if the in-memory bloom filter is ready or not. Until the filter is loaded in-memory
	// requests will be sent to Redis to check if codes are in a set.
	ready bool

	// ctx controls the background job's lifecycle.
	cancel context.CancelFunc

	// client for logging metrics
	stats statsd.Statter

	// waitForBloomFilterOnDeploy to determine whether the first population of the bloom filter should be synchronous
	// and block the deploy.
	waitForBloomFilterOnDeploy bool
}

type EmoteDBCacheConfig struct {
	Addr      string
	Namespace string
	// Bool to determine whether to use redis cluster mode. If 'false', cluster mode will be disabled.
	ClusterEnabled bool
	// Determines the max size of the connection pool to redis.
	ConnectionPoolSize int
	// Minimum idle connections to maintain
	MinIdleConnections int
	Statter            statsd.Statter
	BloomFilter        bool
	// Bool to determine whether the first population of the bloom filter should be synchronous and block the deploy.
	WaitForBloomFilterOnDeploy bool
}

func makeClient(addr string, c EmoteDBCacheConfig) *rediczar.Client {
	var client *rediczar.Client

	// Default from rediczar
	var connectionPoolSize = 30
	if c.ConnectionPoolSize > 0 {
		connectionPoolSize = c.ConnectionPoolSize
	}

	if c.ClusterEnabled {
		client = &rediczar.Client{
			Commands: &rediczar.PrefixedCommands{
				Redis: redefault.NewClusterClient(addr, &redefault.ClusterOpts{
					ReadOnly:     true,
					PoolSize:     connectionPoolSize,
					MinIdleConns: c.MinIdleConnections,
				}),
				KeyPrefix: c.Namespace,
			},
		}
	} else {
		client = &rediczar.Client{
			Commands: &rediczar.PrefixedCommands{
				Redis: redis.NewClient(&redis.Options{
					Addr: addr,
				}),
				KeyPrefix: c.Namespace,
			},
		}
	}

	return client
}

func makePrimaryOnlyClient(addr string, c EmoteDBCacheConfig) *rediczar.Client {
	var primaryOnlyClient *rediczar.Client

	if c.ClusterEnabled {
		primaryOnlyClient = &rediczar.Client{
			Commands: &rediczar.PrefixedCommands{
				Redis: redefault.NewClusterClient(addr, &redefault.ClusterOpts{
					ReadOnly: false,
				}),
				KeyPrefix: c.Namespace,
			},
		}
	} else {
		primaryOnlyClient = &rediczar.Client{
			Commands: &rediczar.PrefixedCommands{
				Redis: redis.NewClient(&redis.Options{
					Addr: addr,
				}),
				KeyPrefix: c.Namespace,
			},
		}
	}

	return primaryOnlyClient
}

// NewEmoteDBWithCache returns an implementation of the `EmoteDB` interface with caching functionality built on-top.
// It supports either a clustered Redis or a normal Redis server (which the tests use).
func NewEmoteDBWithCache(base mako.EmoteDB, c EmoteDBCacheConfig) *EmoteDBCache {
	if c.Namespace == "" {
		c.Namespace = defaultNamespace
	}

	client := makeClient(c.Addr, c)
	primaryOnlyClient := makePrimaryOnlyClient(c.Addr, c)

	ctx, cancel := context.WithCancel(context.Background())

	cache := &EmoteDBCache{
		base:              base,
		ns:                c.Namespace,
		client:            client,
		primaryOnlyClient: primaryOnlyClient,
		// Set the filter to have 190 million bits and 13 hashes.
		// This gives us an expected ~0.0001 false positive probability with the assumption that the underlying set
		// contains 10 million emote codes: https://hur.st/bloomfilter/?n=10000000&p=&m=190000000&k=13
		filter:                     bloom.New(numBitsForBloomFilter, numHashesForBloomFilter),
		cancel:                     cancel,
		stats:                      c.Statter,
		waitForBloomFilterOnDeploy: c.WaitForBloomFilterOnDeploy,
	}

	if c.BloomFilter {
		cache.syncBloomFilterLoop(ctx)
	}

	return cache
}

// syncBloomFilterLoop scans the Redis Set for codes to populate the bloom filter once synchronously
// (after a short randomized delay), then kicks off a background goroutine that keeps checking the set in Redis for any
// new items. Each time we synchronize we copy the bloom filter so we don't race against other requests while
// adding items to it.
func (db *EmoteDBCache) syncBloomFilterLoop(ctx context.Context) {
	if db.waitForBloomFilterOnDeploy {
		err := db.syncBloomFilter(ctx)
		if err != nil {
			logrus.WithError(err).Error("failed to sync the bloom filter for emote codes")
		}
	}

	go func() {
		ticker := time.NewTicker(jitterDuration(bloomFilterRefresh, 0.8, 1.0))
		defer ticker.Stop()

		// Delay the first bloom filter refresh by a random short duration to decrease load on Redis after deploys
		time.Sleep(jitterDuration(bloomFilterInitialRefreshDelay, 0, 1.0))

		for {
			select {
			case <-ctx.Done():
				logrus.Info("bloom filter goroutine shutdown")
				return
			case <-ticker.C:
			}

			err := db.syncBloomFilter(ctx)
			if err != nil {
				logrus.WithError(err).Error("failed to sync the bloom filter for emote codes")

				time.Sleep(time.Second)
				continue
			}
		}
	}()
}

func (db *EmoteDBCache) syncBloomFilter(ctx context.Context) error {
	// Ensure we copy as the bloom filter operations aren't thread safe.
	db.filterMu.RLock()
	filterCopy := db.filter.Copy()
	db.filterMu.RUnlock()

	var err error
	var start = time.Now()

	filterCopy, err = db.readNextCodes(ctx, filterCopy)
	if err != nil {
		return err
	}

	logrus.WithFields(logrus.Fields{
		"latency": time.Since(start),
	}).Info("redis.EmoteDB synchronized bloom filter successfully")

	db.filterMu.Lock()
	db.filter = filterCopy
	db.ready = true
	db.filterMu.Unlock()

	return nil
}

func (db *EmoteDBCache) readNextCodes(ctx context.Context, filter *bloom.BloomFilter) (*bloom.BloomFilter, error) {
	start := time.Now()
	cursor := uint64(0)
	count := 0

	for {
		var err error
		var keys []string

		keys, cursor, err = db.primaryOnlyClient.SScan(ctx, indexByCodeSet.name, cursor, "", 100)
		if err != nil {
			return nil, err
		}

		count += len(keys)

		if count%1000 == 0 {
			logrus.WithFields(logrus.Fields{
				"count":    count,
				"duration": time.Since(start),
				"cursor":   cursor,
			}).Info("redis.EmoteDB syncing bloom filter")
		}

		for _, key := range keys {
			filter = filter.AddString(key)
		}

		if cursor == 0 {
			logrus.WithFields(logrus.Fields{
				"count":    count,
				"duration": time.Since(start),
				"cursor":   cursor,
			}).Info("redis.EmoteDB syncing bloom filter complete")

			break
		}
	}

	return filter, nil
}

func (db *EmoteDBCache) readCachedEmotes(ctx context.Context, index index, requestKeys ...string) ([]mako.Emote, []string, error) {
	missing := make([]string, 0, len(requestKeys))
	emotes := make([]mako.Emote, 0, len(requestKeys))

	if len(requestKeys) == 0 {
		return emotes, missing, nil
	}

	redisKeys := make([]string, 0, len(requestKeys))
	nonEmptyRequestKeys := make([]string, 0, len(requestKeys))
	for _, key := range requestKeys {
		if key == "" {
			continue
		}

		redisKeys = append(redisKeys, makeKey(index.name, key))
		nonEmptyRequestKeys = append(nonEmptyRequestKeys, key)
	}

	var items []interface{}
	err := hystrix.DoC(ctx, index.hystrix, func(ctx context.Context) error {
		var redisErr error

		if index == indexByCode {
			items, redisErr = db.client.MGet(ctx, redisKeys...)
		} else {
			items, redisErr = db.client.PipelinedGet(ctx, redisKeys...)
		}

		return redisErr
	}, nil)
	if err != nil {
		if strings.Contains(err.Error(), hystrix.ErrTimeout.Error()) {
			// Log Warn and do not bubble up. These are expected due to our aggressive hystrix timeout strategy
			log.WithFields(log.Fields{
				"index":       index,
				"requestKeys": requestKeys,
			}).WithError(err).Warn("timeout reading emotes from the redis cache")
			return nil, requestKeys, nil
		}
		return nil, requestKeys, errors.Wrap(err, "failed to perform a pipeline 'GET'")
	}

	for i, item := range items {
		if item == nil {
			missing = append(missing, nonEmptyRequestKeys[i])
			continue
		}

		keyValue, ok := item.(string)
		if !ok {
			missing = append(missing, nonEmptyRequestKeys[i])
			continue
		}

		if gadgetstrings.Blank(keyValue) {
			missing = append(missing, nonEmptyRequestKeys[i])
			continue
		}

		var newEmotes []mako.Emote
		if err := json.Unmarshal([]byte(keyValue), &newEmotes); err != nil {
			return nil, requestKeys, errors.Wrapf(err, "failed to unmarshal emotes (cache value %s) in redis cache (index '%s') (index value '%s')", keyValue, index.name, nonEmptyRequestKeys[i])
		}

		emotes = append(emotes, newEmotes...)
	}

	return emotes, missing, nil
}

func (db *EmoteDBCache) expireEmotesCache(ctx context.Context, emotes ...mako.Emote) error {
	var keys []string
	for _, index := range defaultIndices {
		if index.longlived {
			continue
		}

		for _, emote := range emotes {
			var key string
			switch index {
			case indexByID:
				key = emote.ID
			case indexByCode:
				key = emote.Code
			case indexByGroupID:
				key = emote.GroupID
			default:
				return fmt.Errorf("invalid index provided to writeEmotesCache(): '%s'", index.name)
			}

			if key == "" {
				continue
			}

			keys = append(keys, makeKey(index.name, key))
		}

		if len(keys) == 0 {
			continue
		}
	}

	if _, err := db.client.PipelinedDel(ctx, keys...); err != nil {
		return errors.Wrap(err, "failed to expire keys")
	}

	return nil
}

func (db *EmoteDBCache) writeEmotesCache(ctx context.Context, index index, requestKeys []string, emotes ...mako.Emote) error {
	pipeline := db.client.Pipeline(ctx)
	defer pipeline.Close()

	emotesByKey := make(map[string][]mako.Emote)

	for _, emote := range emotes {
		var key string

		switch index {
		case indexByID:
			key = emote.ID
		case indexByCode:
			key = emote.Code
		case indexByGroupID:
			key = emote.GroupID
		default:
			continue
		}

		emotesByKey[key] = append(emotesByKey[key], emote)
	}

	// Negative Caching: If no emote was found in the db for the given key, cache an empty slice in Redis.
	// This should be safe given that all cache records are invalidated on writes.
	for _, key := range requestKeys {
		if _, ok := emotesByKey[key]; !ok {
			emotesByKey[key] = []mako.Emote{}
		}
	}

	for id, grouped := range emotesByKey {
		bytes, err := json.Marshal(grouped)
		if err != nil {
			return errors.Wrapf(err, "failed to marshal as collection for key '%v'", id)
		}

		if err := pipeline.Set(makeKey(index.name, id), string(bytes), index.ttl).Err(); err != nil {
			return errors.Wrapf(err, "failed to set index '%s' in redis cache for key '%s'", index.name, id)
		}
	}

	if _, err := pipeline.ExecContext(ctx); err != nil {
		return errors.Wrap(err, "failed to execute write pipeline")
	}

	return nil
}

func (db *EmoteDBCache) ReadByIDs(ctx context.Context, ids ...string) ([]mako.Emote, error) {
	emotes, missingIDs, err := db.readCachedEmotes(ctx, indexByID, ids...)
	if err == nil && len(missingIDs) == 0 {
		return emotes, nil
	} else if err != nil {
		// Any failure with the cache should fallback to the primary datastore.
		log.WithError(err).WithField("ids", ids).Error("failed to read emotes by ids from the redis cache")
	}

	dbEmotes, err := db.base.ReadByIDs(ctx, missingIDs...)
	if err != nil {
		return nil, errors.Wrap(err, "failed to read emotes by ids from the database")
	}

	emotes = append(emotes, dbEmotes...)

	if err := db.writeEmotesCache(ctx, indexByID, missingIDs, dbEmotes...); err != nil {
		// Prevent any cache failures from preventing a successful response.
		log.WithError(err).Error("failed to write to emotes cache during ReadByIDs")
	}

	return emotes, nil
}

func (db *EmoteDBCache) readEmoteSet(codes ...string) []string {
	validCodes := make([]string, 0)

	db.filterMu.RLock()
	bloomReady := db.ready

	// Avoid hitting Redis if the in-memory bloom filter has been loaded.
	if bloomReady {
		for _, code := range codes {
			if db.filter.TestString(code) {
				validCodes = append(validCodes, code)
			}
		}

		db.filterMu.RUnlock()
		db.stats.Inc("emotedb.bloomfilter.positive.count", int64(len(validCodes)), 1.0)
		db.stats.Inc("emotedb.bloomfilter.total.count", int64(len(codes)), 1.0)
		return validCodes
	}

	db.filterMu.RUnlock()

	// If filter is not ready, assume all given codes are valid and fall back to the underlying cache layer
	return codes
}

func (db *EmoteDBCache) ReadByCodes(ctx context.Context, codes ...string) ([]mako.Emote, error) {
	emotes, missingCodes, err := db.readCachedEmotes(ctx, indexByCode, codes...)
	if err == nil && len(missingCodes) == 0 {
		return emotes, nil
	} else if err != nil {
		// Any failure with the cache should fallback to the primary datastore.
		log.WithError(err).WithField("codes", codes).Error("failed to read emotes by codes from the redis cache")
	}

	dbEmotes, err := db.base.ReadByCodes(ctx, missingCodes...)
	if err != nil {
		return nil, errors.Wrapf(err, "failed to read emotes by codes from the database: %v", missingCodes)
	}

	emotes = append(emotes, dbEmotes...)

	go func() {
		ctx, cancel := context.WithTimeout(context.Background(), time.Second)
		defer cancel()

		if err := db.writeEmotesCache(ctx, indexByCode, missingCodes, dbEmotes...); err != nil {
			// Prevent any cache failures from preventing a successful response.
			log.WithError(err).Error("failed to write to the redis cache during ReadByCodes")
		}
	}()

	return emotes, nil
}

func (db *EmoteDBCache) ReadByGroupIDs(ctx context.Context, ids ...string) ([]mako.Emote, error) {
	emotes, missingGroupIDs, err := db.readCachedEmotes(ctx, indexByGroupID, ids...)
	if err == nil && len(missingGroupIDs) == 0 {
		return emotes, nil
	} else if err != nil {
		// Any failure with the cache should fallback to the primary datastore.
		log.WithError(err).WithField("group_ids", ids).Error("failed to read emotes by group from the redis cache")
	}

	dbEmotes, err := db.base.ReadByGroupIDs(ctx, missingGroupIDs...)
	if err != nil {
		return nil, errors.Wrap(err, "failed to read emotes by group from the underlying db")
	}

	emotes = append(emotes, dbEmotes...)

	go func() {
		ctx, cancel := context.WithTimeout(context.Background(), time.Second)
		defer cancel()

		if err := db.writeEmotesCache(ctx, indexByGroupID, missingGroupIDs, dbEmotes...); err != nil {
			// Prevent any cache failures from preventing a successful response.
			log.WithError(err).Error("failed to write to the redis cache during ReadByGroupIDs")
		}
	}()

	return emotes, nil
}

func (db *EmoteDBCache) WriteEmotes(ctx context.Context, emotes ...mako.Emote) ([]mako.Emote, error) {
	if len(emotes) == 0 {
		return nil, nil
	}

	var codes []interface{}
	for _, emote := range emotes {
		codes = append(codes, emote.Code)
	}

	// Adding the emote codes to the cache happens before writing to the underlying database to prevent
	// codes from missing in the index. This index doesn't have a TTL and so we would rather the client retry their request
	// to ensure the emote exists in both the redis cache + underlying database.
	addSet := func() error {
		_, err := db.client.SAdd(ctx, indexByCodeSet.name, codes...)
		return err
	}

	if err := backoff.Retry(addSet, backoff.WithMaxRetries(
		backoff.WithContext(newBackoff(), ctx),
		3,
	)); err != nil {
		return nil, errors.Wrap(err, "failed to add emote codes to set")
	}

	emotes, err := db.base.WriteEmotes(ctx, emotes...)
	if err != nil {
		return nil, err
	}

	if err := db.expireEmotesCache(context.Background(), emotes...); err != nil {
		return nil, errors.Wrap(err, "failed to expire emotes cache after creating")
	}

	return emotes, nil
}

// DeleteEmote tries to read the emote from the underlying store, expires the cache for all the indexes
// and then delegates to the underlying store for the delete.
//
// The first read is required to expire all the caches for that emote as the `id` isn't enough data to find each key.
func (db *EmoteDBCache) DeleteEmote(ctx context.Context, id string) (mako.Emote, error) {
	emotes, err := db.base.ReadByIDs(ctx, id)
	if err != nil {
		return mako.Emote{}, err
	}

	// Don't fail if the emote doesn't exist.
	if len(emotes) == 0 {
		return mako.Emote{}, nil
	}

	emote, err := db.base.DeleteEmote(ctx, id)
	if err != nil {
		return mako.Emote{}, err
	}

	// Fail here to prevent zombie records existing in the cache but not in the underlying store.
	if err := db.expireEmotesCache(ctx, emotes...); err != nil {
		return mako.Emote{}, errors.Wrap(err, "failed to expire emotes cache before deleting")
	}

	return emote, nil
}

// UpdateCode tries to read the emote from the underlying store, expires the cache for all the indexes
// and then delegates to the underlying store for the update.
//
// The first read is required to expire all the caches for that emote as the `id` isn't enough data to find each key.
func (db *EmoteDBCache) UpdateCode(ctx context.Context, id, code, suffix string) (mako.Emote, error) {
	emotes, err := db.base.ReadByIDs(ctx, id)
	if err != nil {
		return mako.Emote{}, err
	}

	if len(emotes) == 0 {
		return mako.Emote{}, fmt.Errorf("failed to update emote code, did not find emote '%s'", id)
	}

	// Adding the emote codes to the cache happens before writing to the underlying database to prevent
	// codes from missing in the index. This index doesn't have a TTL and so we would rather the client retry their request
	// to ensure the emote exists in both the redis cache + underlying database.
	addSet := func() error {
		_, err := db.client.SAdd(ctx, indexByCodeSet.name, code)
		return err
	}

	if err := backoff.Retry(addSet, backoff.WithMaxRetries(
		backoff.WithContext(newBackoff(), ctx),
		3,
	)); err != nil {
		return mako.Emote{}, errors.Wrap(err, "failed to add emote codes to set")
	}

	emote, err := db.base.UpdateCode(ctx, id, code, suffix)
	if err != nil {
		return mako.Emote{}, err
	}

	// Fail here to prevent zombie records existing in the cache but not in the underlying store.
	if err := db.expireEmotesCache(ctx, emotes...); err != nil {
		return mako.Emote{}, errors.Wrapf(err, "failed to expire emotes cache while updating code for emote with id %s", id)
	}

	return emote, nil
}

// UpdatePrefix tries to read the emote from the underlying store, expires the cache for all the indexes
// and then delegates to the underlying store for the update.
//
// The first read is required to expire all the caches for that emote as the `id` isn't enough data to find each key.
func (db *EmoteDBCache) UpdatePrefix(ctx context.Context, id, code, prefix string) (mako.Emote, error) {
	emotes, err := db.base.ReadByIDs(ctx, id)
	if err != nil {
		return mako.Emote{}, err
	}

	if len(emotes) == 0 {
		return mako.Emote{}, fmt.Errorf("failed to update emote prefix, did not find emote '%s'", id)
	}

	// Adding the emote codes to the cache happens before writing to the underlying database to prevent
	// codes from missing in the index. This index doesn't have a TTL and so we would rather the client retry their request
	// to ensure the emote exists in both the redis cache + underlying database.
	addSet := func() error {
		_, err := db.client.SAdd(ctx, indexByCodeSet.name, code)
		return err
	}

	if err := backoff.Retry(addSet, backoff.WithMaxRetries(
		backoff.WithContext(newBackoff(), ctx),
		3,
	)); err != nil {
		return mako.Emote{}, errors.Wrap(err, "failed to add emote codes to set")
	}

	emote, err := db.base.UpdatePrefix(ctx, id, code, prefix)
	if err != nil {
		return mako.Emote{}, err
	}

	// Fail here to prevent zombie records existing in the cache but not in the underlying store.
	if err := db.expireEmotesCache(ctx, emotes...); err != nil {
		return mako.Emote{}, errors.Wrapf(err, "failed to expire emotes cache while updating prefix for emote with id %s", id)
	}

	return emote, nil
}

// UpdateState tries to read the emote from the underlying store, expires the cache for all the indexes
// and then delegates to the underlying store for the update.
//
// The first read is required to expire all the caches for that emote as the `id` isn't enough data to find each key.
func (db *EmoteDBCache) UpdateState(ctx context.Context, id string, state mako.EmoteState) (mako.Emote, error) {
	emotes, err := db.base.ReadByIDs(ctx, id)
	if err != nil {
		return mako.Emote{}, err
	}

	if len(emotes) == 0 {
		return mako.Emote{}, fmt.Errorf("failed to update emote state, did not find emote '%s'", id)
	}

	emote, err := db.base.UpdateState(ctx, id, state)
	if err != nil {
		return mako.Emote{}, err
	}

	// Fail here to prevent zombie records existing in the cache but not in the underlying store.
	if err := db.expireEmotesCache(ctx, emotes...); err != nil {
		return mako.Emote{}, errors.Wrapf(err, "failed to expire emotes cache while updating state for emote with id %s", id)
	}

	return emote, nil
}

// UpdateEmote tries to read the emote from the underlying store, delegates to the underlying store for the update,
// and then expires the cache for all the indexes (both old and new emotes).
//
// The first read is required to expire all the caches for that emote as the `id` isn't enough data to find each key.
func (db *EmoteDBCache) UpdateEmote(ctx context.Context, id string, params mako.EmoteUpdate) (mako.Emote, error) {
	existingEmotes, err := db.base.ReadByIDs(ctx, id)
	if err != nil {
		return mako.Emote{}, err
	}

	if len(existingEmotes) == 0 {
		return mako.Emote{}, fmt.Errorf("failed to update emote, did not find emote '%s'", id)
	}

	updatedEmote, err := db.base.UpdateEmote(ctx, id, params)
	if err != nil {
		return mako.Emote{}, err
	}

	emotesToExpire := append(existingEmotes, updatedEmote)

	// Expire both old and new emotes to ensure all potentially changed index keys are expired.
	if err := db.expireEmotesCache(ctx, emotesToExpire...); err != nil {
		// Fail here to prevent zombie records existing in the cache but not in the underlying store.
		return mako.Emote{}, errors.Wrapf(err, "failed to expire emotes cache while updating emote with id %s", id)
	}

	return updatedEmote, nil
}

func (db *EmoteDBCache) UpdateEmotes(ctx context.Context, emoteUpdatesByID map[string]mako.EmoteUpdate) ([]mako.Emote, error) {
	var ids []string
	for id := range emoteUpdatesByID {
		ids = append(ids, id)
	}

	emotesToExpire, err := db.base.UpdateEmotes(ctx, emoteUpdatesByID)
	if err != nil {
		return nil, err
	}

	// Expire all emotes returned to ensure all potentially changed index keys are expired.
	if err := db.expireEmotesCache(ctx, emotesToExpire...); err != nil {
		// Fail here to prevent zombie records existing in the cache but not in the underlying store.
		return nil, errors.Wrapf(err, "failed to expire emotes cache while updating emote with ids %s", ids)
	}

	return emotesToExpire, nil
}

// Query is used for more complex queries and thus doesn't read from any caches as this method supports adding filters which would
// complicate the caching.
func (db *EmoteDBCache) Query(ctx context.Context, query mako.EmoteQuery) ([]mako.Emote, error) {
	return db.base.Query(ctx, query)
}

func (db *EmoteDBCache) Close() error {
	db.cancel()
	return db.base.Close()
}

func MakeEmoteDB(makeDB tests.MakeEmoteDB) tests.MakeEmoteDB {
	return func(ctx context.Context, emotes ...mako.Emote) (db mako.EmoteDB, teardown func(), err error) {
		addr := "localhost:6379"

		for _, index := range defaultIndices {
			hystrix.ConfigureCommand(index.hystrix, hystrix.CommandConfig{
				Timeout:               5000,
				MaxConcurrentRequests: 1000,
			})
		}

		// Jenkins require a different Redis address for tests.
		if os.Getenv("DOCKER_TEST") != "" {
			addr = "jenkins_redis:6379"
		}

		// Run the tests in a separate namespace, ensuring tests can run in parallel.
		ns := ksuid.New().String()
		base, baseTeardown, err := makeDB(ctx)
		if err != nil {
			return nil, nil, errors.Wrap(err, "failed to initialize base EmoteDB")
		}

		noopStats, _ := statsd.NewNoop()

		db = NewEmoteDBWithCache(base, EmoteDBCacheConfig{
			Addr:                       addr,
			Namespace:                  ns,
			ClusterEnabled:             false,
			Statter:                    noopStats,
			WaitForBloomFilterOnDeploy: false,
		})
		teardown = func() {
			baseTeardown()
		}

		// Each test provides test data that should be written to Redis before starting.
		_, err = db.WriteEmotes(ctx, emotes...)
		if err != nil {
			return
		}

		return
	}
}

func makeKey(index string, val string) string {
	// For the code emote index we use hash tags to force Redis to always hash the key
	// to the same hashslot. If all the emote keys are stored in the same slot (roughly 2M-3M keys)
	// then we can use the MGet command to query many keys in a single command. This is much much faster
	// than distributing the keys over multiple hashslots and then using pipelined GETs.
	if index == indexByCode.name {
		return fmt.Sprintf("{%s}:%s", index, val)
	}

	return fmt.Sprintf("%s:%s", index, val)
}

func newBackoff() backoff.BackOff {
	exponential := backoff.NewExponentialBackOff()
	exponential.InitialInterval = 10 * time.Millisecond
	exponential.MaxElapsedTime = 1 * time.Second
	exponential.Multiplier = 2
	return exponential
}
