package redis

import (
	"context"
	"fmt"
	"os"
	"time"

	"code.justin.tv/chat/rediczar"
	"code.justin.tv/chat/rediczar/redefault"
	mako "code.justin.tv/commerce/mako/internal"
	"code.justin.tv/commerce/mako/tests"
	"github.com/afex/hystrix-go/hystrix"
	"github.com/go-redis/redis/v7"
	"github.com/segmentio/ksuid"
)

const (
	uploadMetadataRedisTTL = time.Hour * 24
)

// EmoteUploadMetadataCacheConfig contains the configs needed to create an EmoteUploadMetadataCache
type EmoteUploadMetadataCacheConfig struct {
	Addr      string
	Namespace string
	// Bool to determine whether to use redis cluster mode. If 'false', cluster mode will be disabled.
	ClusterEnabled bool
}

type emoteUploadMetadataCache struct {
	namespace string

	// client is either a Redis cluster or single Redis server instance.
	client *rediczar.Client
}

// NewEmoteUploadMetadataCache creates an EmoteUploadMetadataCache
func NewEmoteUploadMetadataCache(conf EmoteUploadMetadataCacheConfig) mako.EmoteUploadMetadataCache {
	var cmdable redis.Cmdable
	if conf.ClusterEnabled {
		cmdable = redefault.NewClusterClient(conf.Addr, nil)
	} else {
		cmdable = redis.NewClient(&redis.Options{Addr: conf.Addr})
	}

	client := &rediczar.Client{
		Commands: &rediczar.PrefixedCommands{
			Redis:     cmdable,
			KeyPrefix: conf.Namespace,
		},
	}

	return &emoteUploadMetadataCache{
		namespace: conf.Namespace,
		client:    client,
	}
}

// StoreUploadMethod stores the upload method used for a given uploadID in redis
func (db *emoteUploadMetadataCache) StoreUploadMethod(ctx context.Context, uploadID, method string) error {
	_, err := db.client.Set(ctx, makeEmoteUploadMethodCacheKey(uploadID), method, uploadMetadataRedisTTL)
	if err != nil && err != redis.Nil {
		return err
	}

	return nil
}

// ReadUploadMethod returns the upload method used for a given uploadID from redis
func (db *emoteUploadMetadataCache) ReadUploadMethod(ctx context.Context, uploadID string) (string, error) {
	resp, err := db.client.Get(ctx, makeEmoteUploadMethodCacheKey(uploadID))
	if err != nil && err != redis.Nil {
		return mako.EmoteUploadMethodUnknown, err
	}

	return mako.SanitizeUploadMethod(resp), nil
}

func makeEmoteUploadMethodCacheKey(id string) string {
	return fmt.Sprintf("emoteUploadMethod:%s", id)
}

// MakeEmoteUploadMetadataCacheForTesting connects to redis running on localhost for running tests
func MakeEmoteUploadMetadataCacheForTesting() tests.MakeEmoteUploadMetadataCache {
	return func(ctx context.Context) (db mako.EmoteUploadMetadataCache, err error) {
		addr := "localhost:6379"

		for _, index := range defaultIndices {
			hystrix.ConfigureCommand(index.hystrix, hystrix.CommandConfig{
				Timeout: 5000,
			})
		}

		// Jenkins require a different Redis address for tests.
		if os.Getenv("DOCKER_TEST") != "" {
			addr = "jenkins_redis:6379"
		}

		// Run the tests in a separate namespace, ensuring tests can run in parallel.
		namespace := ksuid.New().String()

		db = NewEmoteUploadMetadataCache(EmoteUploadMetadataCacheConfig{
			Addr:           addr,
			Namespace:      namespace,
			ClusterEnabled: false,
		})

		return db, nil
	}
}
