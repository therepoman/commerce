package redis

import (
	"testing"

	"code.justin.tv/commerce/mako/dynamo"
	"code.justin.tv/commerce/mako/tests"
)

func TestEmoteModsDB(t *testing.T) {
	tests.TestEmoteModifiersDB(t, MakeEmoteModifiersDBCacheForTesting(dynamo.MakeEmoteModifiersDBForTesting))
}
func TestEmoteModsDBCache(t *testing.T) {
	tests.TestEmoteModifiersDBCache(t, MakeEmoteModifiersDBCacheForTesting(dynamo.MakeEmoteModifiersDBForTesting))
}
