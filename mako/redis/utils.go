package redis

import (
	"math/rand"
	"time"
)

// jitterDuration returns a random time duration in range [low*d, high*d).
// Caller should ensure low <= high.
func jitterDuration(d time.Duration, low, high float64) time.Duration {
	return time.Duration((float64(d) * low) + (float64(d) * (high - low) * rand.Float64()))
}
