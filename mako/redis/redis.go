package redis

import (
	"time"

	goRedis "github.com/go-redis/redis/v7"
)

type IRedisClient interface {
	Get(string) ([]byte, error)
	Set(string, interface{}, time.Duration) error
	Exists(string) (int64, error)
	Del(string) error
}

type goRedisClient struct {
	GoRedisClient *goRedis.ClusterClient
}

func NewGoRedisClient(client *goRedis.ClusterClient) IRedisClient {
	return &goRedisClient{
		GoRedisClient: client,
	}
}

func (rc *goRedisClient) Get(key string) ([]byte, error) {
	return rc.GoRedisClient.Get(key).Bytes()
}

func (rc *goRedisClient) Set(key string, value interface{}, expiration time.Duration) error {
	return rc.GoRedisClient.Set(key, value, expiration).Err()
}

func (rc *goRedisClient) Exists(key string) (int64, error) {
	return rc.GoRedisClient.Exists(key).Result()
}

func (rc *goRedisClient) Del(key string) error {
	return rc.GoRedisClient.Del(key).Err()
}
