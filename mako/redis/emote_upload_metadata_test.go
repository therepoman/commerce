package redis

import (
	"testing"

	"code.justin.tv/commerce/mako/tests"
)

func TestEmoteUploadMetatdataCache(t *testing.T) {
	tests.TestEmoteUploadMetadataCache(t, MakeEmoteUploadMetadataCacheForTesting())
}
