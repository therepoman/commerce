package setup

import (
	"fmt"
	"net/http"
	"os"

	"github.com/cactus/go-statsd-client/statsd"

	badges "code.justin.tv/chat/badges/client"
	"code.justin.tv/chat/ecsgomaxprocs"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/mako/clients"
	"code.justin.tv/commerce/mako/clients/wrapper"
	"code.justin.tv/commerce/mako/config"
	"code.justin.tv/commerce/mako/dynamo"
	"code.justin.tv/commerce/mako/events"
	"code.justin.tv/commerce/mako/sqs/processors"
	"code.justin.tv/commerce/mako/stats"
	"code.justin.tv/common/godynamo/client"
	"code.justin.tv/eventbus/client/subscriber/sqsclient"
	"code.justin.tv/foundation/twitchclient"
)

type SetupParams struct {
	Config                     *config.Configuration
	DynamicConfig              *config.Dynamic
	DynamicS3Configs           *config.DynamicS3Configs
	EntitlementDao             dynamo.IEntitlementDao
	PrefixDao                  dynamo.IPrefixDao
	PendingEmotesDao           dynamo.IPendingEmotesDao
	SettingsDao                dynamo.ISettingsDao
	Stats                      statsd.Statter
	Daos                       *clients.DataAccessObjects
	Clients                    *clients.Clients
	Delegator                  *events.EventDelegator
	PrefixUpdateProcessor      processors.IPrefixUpdateProcessor
	EmotePrefixUpdateProcessor processors.IPrefixUpdateProcessor
	EventBusSQSClient          *sqsclient.SQSClient
}

type fixerLogger struct {
}

func (l *fixerLogger) Log(values ...interface{}) {
	log.Info(values...)
}

func SetupMako() *SetupParams {
	environment := config.GetEnvironment()
	if environment == "production" {
		environment = "prod"
	}

	cfg, err := config.LoadConfigForEnvironment(environment)

	if err != nil {
		log.Fatal(err)
	}

	if os.Getenv("INTEGRATION") != "" {
		cfg.Preload = true
	}

	deploymentStage := os.Getenv("DEPLOYMENT_STAGE")

	// Deployment stage env should always be set via coalescence with ENVIRONMENT in Terraform beanstalk config.
	// Nevertheless, defend against sending stats with an empty dimension.
	if deploymentStage == "" {
		deploymentStage = environment

		if os.Getenv("CANARY") == "true" {
			deploymentStage += "-canary"
		}

		if os.Getenv("ECS") == "true" {
			deploymentStage += "-ecs"
		}
	}

	log.SetLevel(log.InfoLevel)
	log.SetFormatter(&log.TextFormatter{FullTimestamp: true, TimestampFormat: "02/Jan/2006:15:04:05 -0700"})
	log.WithFields(log.Fields{
		"environment":     environment,
		"deploymentStage": deploymentStage,
	}).Info("Initializing Mako...")

	f := ecsgomaxprocs.Fixer{
		Log: &fixerLogger{},
	}

	if err := f.Exec(); err != nil {
		fmt.Println("failed to optimize gomaxprocs")
	}

	statter := stats.SetupStats(deploymentStage, cfg)

	dynamicConfig, err := config.NewDynamic(cfg.AWSRegion, statter)
	if err != nil {
		log.WithError(err).Fatal("failed to initialize SSM dynamic config")
	}

	campaignEmoteOwnersDynamicS3Config, err := config.NewS3Dynamic(cfg.AWSRegion, cfg.ConfigS3Endpoint, cfg.ConfigS3Bucket, cfg.CampaignEmoteOwnersConfigS3Filename, statter)
	if err != nil {
		log.WithError(err).Fatal("failed to initialize campaign emote owners S3 dynamic config")
	}

	knownOrVerifiedBotsDynamicS3Config, err := config.NewS3Dynamic(cfg.AWSRegion, cfg.ConfigS3Endpoint, cfg.ConfigS3Bucket, cfg.KnownOrVerifiedBotsS3Filename, statter)
	if err != nil {
		log.WithError(err).Fatal("failed to initialize known or verified bots S3 dynamic config")
	}

	defaultEmoteImagesDataDynamicS3Config, err := config.NewS3Dynamic(cfg.AWSRegion, cfg.DefaultEmoteImagesDataS3Endpoint, cfg.DefaultEmoteImagesDataS3Bucket, cfg.DefaultEmoteImagesDataS3Filename, statter)
	if err != nil {
		log.WithError(err).Fatal("failed to initialize default emote images S3 dynamic config")
	}

	dynamoClient := client.NewClient(&client.DynamoClientConfig{
		AwsRegion:   cfg.AWSRegion,
		TablePrefix: cfg.EnvironmentPrefix,
	})

	badgeConfig := twitchclient.ClientConf{
		Host: cfg.BadgeServiceEndpoint,
		Transport: twitchclient.TransportConf{
			MaxIdleConnsPerHost: cfg.BadgeServiceMaxIdleConnections,
		},
		Stats: statter,
		RoundTripperWrappers: []func(http.RoundTripper) http.RoundTripper{
			wrapper.NewRetryRoundTripWrapper(statter, "badges"),
		},
	}

	badgeClient, err := badges.NewClient(badgeConfig)
	if err != nil {
		log.Fatalf("Failed to initialize badge service client: %s", err)
	}

	//TODO add a way to inject the processors into the delegator
	entitlementDao := dynamo.NewEntitlementDao(dynamoClient)
	prefixesDao := dynamo.NewPrefixDao(dynamoClient)
	settingsDao := dynamo.NewSettingsDao(dynamoClient)

	pendingEmotesDao, err := dynamo.NewPendingEmotesDao(cfg.AWSRegion, cfg.EnvironmentPrefix)
	if err != nil {
		log.Fatalf("failed to initialize pending emotes table: %s", err)
	}

	daos := clients.DataAccessObjects{
		EntitlementDao:   entitlementDao,
		PrefixDao:        prefixesDao,
		SettingsDao:      settingsDao,
		PendingEmotesDao: pendingEmotesDao,
	}

	dynamicS3Configs := &config.DynamicS3Configs{
		CampaignEmoteOwnersDynamicS3Config:   campaignEmoteOwnersDynamicS3Config,
		KnownOrVerifiedBotsDynamicS3Config:   knownOrVerifiedBotsDynamicS3Config,
		DefaultEmoteImageDataDynamicS3Config: defaultEmoteImagesDataDynamicS3Config,
	}

	clients, err := clients.SetupClients(cfg, dynamicS3Configs, dynamicConfig, statter, daos)
	if err != nil {
		log.Fatalf("Failed to initialize clients: %s", err)
	}

	delegator := events.NewEventDelegator(entitlementDao, badgeClient, clients, cfg)

	prefixUpdateProcessor := processors.NewPrefixUpdateProcessor(clients.EmoteDB, prefixesDao)

	return &SetupParams{
		Config:                cfg,
		DynamicConfig:         dynamicConfig,
		DynamicS3Configs:      dynamicS3Configs,
		EntitlementDao:        entitlementDao,
		PrefixDao:             prefixesDao,
		PendingEmotesDao:      pendingEmotesDao,
		SettingsDao:           settingsDao,
		Stats:                 statter,
		Daos:                  &daos,
		Clients:               clients,
		Delegator:             delegator,
		PrefixUpdateProcessor: prefixUpdateProcessor,
	}
}
