FROM public.ecr.aws/bitnami/golang:1.14.15 as builder

ADD . /go/src/code.justin.tv/commerce/mako
WORKDIR /go/src/code.justin.tv/commerce/mako

RUN GIT_COMMIT=$(git rev-list -1 HEAD) && \
    go build -o /mako ./cmd/mako/main.go

RUN go test -c -o /integration-tester ./integration

# Runner Container

FROM public.ecr.aws/bitnami/golang:1.14.15 as runner

WORKDIR /

COPY --from=builder /mako /mako
COPY --from=builder /integration-tester /integration-tester
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /go/src/code.justin.tv/commerce/mako/config /etc/mako/config
COPY --from=builder /go/src/code.justin.tv/commerce/mako/integration/testdata /testdata

ENTRYPOINT ["/mako"]
