package tests

import (
	"context"
	"testing"
	"time"

	mako "code.justin.tv/commerce/mako/internal"
	"github.com/segmentio/ksuid"
	"github.com/stretchr/testify/require"
)

// MakeEntitlementDB is used to initialize an `EntitlementDB` for the test suite. Implementations can call the test suite and provide
// an implementation with a way to initialize seed data and provide a teardown function.
type MakeEntitlementDB = func(ctx context.Context, entitlements ...mako.Entitlement) (mako.EntitlementDB, func(), error)

// makeEntitlement constructs a new entitlement with random seed data.
func makeEntitlement() mako.Entitlement {
	return mako.Entitlement{
		ID:        ksuid.New().String(),
		OwnerID:   ksuid.New().String(),
		Start:     time.Now(),
		Source:    mako.SubscriptionsSource,
		OriginID:  ksuid.New().String(),
		ChannelID: ksuid.New().String(),
		Type:      mako.EmoteGroupEntitlement,
		End:       nil,
	}
}

// makeEntitlementFn is a function to initialize a test Entitlement with dummy data with the ability
// for the lambda to customize fields further.
func makeEntitlementFn(f func(*mako.Entitlement)) mako.Entitlement {
	ent := makeEntitlement()
	f(&ent)
	return ent
}

// TestEntitlementDB is a **test suite** that encapsulates all the behaviour of what an entitlement system would have. The test suite
// accepts a `MakeEntitlementDB` lambda that returns an implementation of the `EntitlementDB` interface, accepting test data to initialize the
// implementation.
//
// This means that the test suite is agnostic to a particular implementation. It only knows about a generic interface. This makes
// it possible to re-use the same test suite and a different set of inputs.
func TestEntitlementDB(t *testing.T, makeDB MakeEntitlementDB) {
	tests := []struct {
		scenario     string
		entitlements []mako.Entitlement
		test         func(ctx context.Context, t *testing.T, db mako.EntitlementDB, entitlements ...mako.Entitlement)
	}{
		{
			scenario:     "GIVEN an empty list of entitlement ids THEN return empty entitlements slice and no error",
			entitlements: []mako.Entitlement{},
			test: func(ctx context.Context, t *testing.T, db mako.EntitlementDB, entitlements ...mako.Entitlement) {
				dbEntitlements, err := db.ReadByIDs(ctx, "", []string{}...)
				require.Nil(t, err)
				require.Empty(t, dbEntitlements)
			},
		},
		{
			scenario: "should return an entitlement",
			entitlements: []mako.Entitlement{
				makeEntitlement(),
			},
			test: func(ctx context.Context, t *testing.T, db mako.EntitlementDB, entitlements ...mako.Entitlement) {
				dbEntitlements, err := db.ReadByIDs(ctx, entitlements[0].OwnerID, entitlements[0].ID)
				require.Nil(t, err)
				require.Equal(t, len(entitlements), len(dbEntitlements))
				for _, entitlement := range entitlements {
					for _, dbEntitlement := range dbEntitlements {
						if entitlement.ID != dbEntitlement.ID {
							continue
						}

						requireEntitlement(t, entitlement, dbEntitlement)
					}
				}
			},
		},

		{
			scenario: "should only return an entitlement for an owner",
			entitlements: []mako.Entitlement{
				makeEntitlement(),
				makeEntitlement(),
			},
			test: func(ctx context.Context, t *testing.T, db mako.EntitlementDB, entitlements ...mako.Entitlement) {
				dbEntitlements, err := db.ReadByIDs(ctx, entitlements[0].OwnerID, entitlements[0].ID)
				require.Nil(t, err)
				require.Equal(t, 1, len(dbEntitlements))
				for _, dbEntitlement := range dbEntitlements {
					if dbEntitlement.OwnerID != entitlements[0].OwnerID {
						continue
					}

					requireEntitlement(t, entitlements[0], dbEntitlement)
				}
			},
		},

		{
			scenario: "should return multiple entitlements for an owner",
			entitlements: []mako.Entitlement{
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.OwnerID = "123"
				}),
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.OwnerID = "123"
				}),
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.OwnerID = "123"
				}),
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.OwnerID = "123"
				}),
			},
			test: func(ctx context.Context, t *testing.T, db mako.EntitlementDB, entitlements ...mako.Entitlement) {
				dbEntitlements, err := db.ReadByIDs(ctx, entitlements[0].OwnerID, makeEntitlementIDs(entitlements...)...)
				require.Nil(t, err)
				require.Equal(t, len(entitlements), len(dbEntitlements))

				for _, entitlement := range entitlements {
					for _, dbEntitlement := range dbEntitlements {
						if entitlement.ID != dbEntitlement.ID {
							continue
						}

						requireEntitlement(t, entitlement, dbEntitlement)
					}
				}
			},
		},
		{
			scenario: "should return all entitlements for an owner when querying by ownerID",
			entitlements: []mako.Entitlement{
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.OwnerID = "123"
				}),
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.OwnerID = "123"
				}),
			},
			test: func(ctx context.Context, t *testing.T, db mako.EntitlementDB, entitlements ...mako.Entitlement) {
				dbEntitlements, err := db.ReadByOwnerID(ctx, entitlements[0].OwnerID, entitlements[0].ChannelID)
				require.Nil(t, err)
				require.Equal(t, len(entitlements), len(dbEntitlements))

				for _, entitlement := range entitlements {
					for _, dbEntitlement := range dbEntitlements {
						if entitlement.ID != dbEntitlement.ID {
							continue
						}

						requireEntitlement(t, entitlement, dbEntitlement)
					}
				}
			},
		},
	}

	for _, test := range tests {
		test := test

		t.Run(test.scenario, func(t *testing.T) {
			t.Parallel()

			// Loading into local dynamo can sometimes take a while when many tests are all vying for the same resource
			// at the same time, so we provide a longer context timeout for loading into the DB and then a shorter one
			// for actually running the test.
			ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)

			db, teardown, err := makeDB(ctx, test.entitlements...)
			if err != nil {
				t.Fatal(err)
			}

			cancel()
			ctx, cancel = context.WithTimeout(context.Background(), 3*time.Second)

			test.test(ctx, t, db, test.entitlements...)
			cancel()
			teardown()
		})
	}
}

func requireEntitlement(t *testing.T, a, b mako.Entitlement) {
	t.Helper()

	require.Equal(t, a.ID, b.ID)
	require.Equal(t, a.OwnerID, b.OwnerID)
	require.Equal(t, a.Start.Format(time.RFC3339), b.Start.Format(time.RFC3339))
}

func makeEntitlementIDs(entitlements ...mako.Entitlement) []string {
	var ids []string
	for _, ent := range entitlements {
		ids = append(ids, ent.ID)
	}

	return ids
}
