package tests

import (
	"bytes"
	mk "code.justin.tv/commerce/mako/twirp"
	"context"
	"sort"
	"testing"
	"time"

	mkd "code.justin.tv/commerce/mako/twirp/dashboard"

	"code.justin.tv/commerce/mako/config"
	mako "code.justin.tv/commerce/mako/internal"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

type test struct {
	EmoteDB          mako.EmoteDB
	EntitlementDB    mako.EntitlementDB
	EmoteModifiersDB mako.EmoteModifiersDB
	DynamicConfig    config.DynamicConfigClient
	StaticConfig     config.Configuration

	Emotes       []mako.Emote
	Entitlements []mako.Entitlement
}

type EmoteCodesConfig struct {
	MakeEmoteDB         MakeEmoteDB
	MakeEntitlementDB   MakeEntitlementDB
	MakeEmoteModifierDB MakeEmoteModifiersDB
}

type testCodesCase struct {
	scenario            string
	entitlements        []mako.Entitlement
	emotes              []mako.Emote
	emoteModifierGroups []mako.EmoteModifierGroup
	test                func(ctx context.Context, t *testing.T, test test)
}

// TestEmoteCodes is a **test suite** that encapsulates all the behaviour of GetEmotesByCodes.
func TestEmoteCodes(t *testing.T, config EmoteCodesConfig) {
	t.Run("Basics", func(t *testing.T) {
		TestEmoteCodesBasics(t, config)
	})

	t.Run("GroupEmotes", func(t *testing.T) {
		TestEmoteCodesGroupEmotes(t, config)
	})

	t.Run("SingleEmotes", func(t *testing.T) {
		TestEmoteCodesSingleEmotes(t, config)
	})

	t.Run("ModifiedSingleEmotes", func(t *testing.T) {
		TestEmoteCodesModifiedSingleEmotes(t, config)
	})

	t.Run("GlobalAndSmilies", func(t *testing.T) {
		TestEmoteCodesGlobalAndSmilieEmotes(t, config)
	})

	t.Run("T2T3ModifiedEmotes", func(t *testing.T) {
		TestEmoteCodesT2T3ModifiedEmotes(t, config)
	})

	t.Run("TestEmoteCodesSpecialEmotes", func(t *testing.T) {
		TestEmoteCodesSpecialEmotes(t, config)
	})

	t.Run("TestFollowerEmotes", func(t *testing.T) {
		TestEmoteCodesFollowerEmotes(t, config)
	})
}

func TestEmoteCodesBasics(t *testing.T, c EmoteCodesConfig) {
	tests := []testCodesCase{
		{
			scenario:     "should return 0 emotes when no entitlements are found",
			entitlements: []mako.Entitlement{},
			emotes:       []mako.Emote{makeEmote()},
			test: func(ctx context.Context, t *testing.T, test test) {
				emotes, err := mako.GetEmotesByCodes(ctx, mako.GetEmotesByCodesParams{
					EmoteDB:          test.EmoteDB,
					EntitlementDB:    test.EntitlementDB,
					EmoteModifiersDB: test.EmoteModifiersDB,
					Dynamic:          test.DynamicConfig,
					UserID:           "notFound",
					ChannelID:        "notFound",
					Codes:            []string{test.Emotes[0].Code},
				})
				assert.Nil(t, err)
				require.Equal(t, 0, len(emotes))
			},
		},

		{
			scenario: "should return only active emotes if channel has inactive emotes",
			entitlements: []mako.Entitlement{
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = "123"
				}),
			},
			emotes: []mako.Emote{
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "999"
					e.GroupID = "123"
					e.Code = "Single"
					e.State = mako.Active
				}),
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "555"
					e.Code = "Single"
					e.State = mako.Inactive
				}),
			},
			test: func(ctx context.Context, t *testing.T, test test) {
				emotes, err := mako.GetEmotesByCodes(ctx, mako.GetEmotesByCodesParams{
					EmoteDB:          test.EmoteDB,
					EntitlementDB:    test.EntitlementDB,
					EmoteModifiersDB: test.EmoteModifiersDB,
					Dynamic:          test.DynamicConfig,
					UserID:           test.Entitlements[0].OwnerID,
					ChannelID:        "notFound",
					Codes:            []string{test.Emotes[0].Code},
				})
				assert.Nil(t, err)
				require.Equal(t, 1, len(emotes))
				RequireEmoteEquality(t, test.Emotes[0], emotes[0])
			},
		},

		{
			scenario: "should return no inactive emotes",
			entitlements: []mako.Entitlement{
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = "123"
				}),
			},
			emotes: []mako.Emote{
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "999"
					e.GroupID = "123"
					e.Code = "Single"
					e.State = mako.Inactive
				}),
			},
			test: func(ctx context.Context, t *testing.T, test test) {
				emotes, err := mako.GetEmotesByCodes(ctx, mako.GetEmotesByCodesParams{
					EmoteDB:          test.EmoteDB,
					EntitlementDB:    test.EntitlementDB,
					EmoteModifiersDB: test.EmoteModifiersDB,
					Dynamic:          test.DynamicConfig,
					UserID:           test.Entitlements[0].OwnerID,
					ChannelID:        "notFound",
					Codes:            []string{test.Emotes[0].Code},
				})
				assert.Nil(t, err)
				require.Equal(t, 0, len(emotes))
			},
		},

		{
			scenario: "should not send duplicate entitlement ids to EntitlementDB",
			entitlements: []mako.Entitlement{
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = "877"
					e.Type = mako.EmoteGroupEntitlement
				}),
			},
			emotes: []mako.Emote{
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "566"
					e.GroupID = "877"
					e.Code = "HELLO"
				}),
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "569"
					e.GroupID = "877"
					e.Code = "WRATH"
				}),
			},
			test: func(ctx context.Context, t *testing.T, test test) {
				// Call the method once to ensure values are cached if
				// one is used.
				_, err := mako.GetEmotesByCodes(ctx, mako.GetEmotesByCodesParams{
					EmoteDB:          test.EmoteDB,
					EntitlementDB:    test.EntitlementDB,
					EmoteModifiersDB: test.EmoteModifiersDB,
					Dynamic:          test.DynamicConfig,
					UserID:           test.Entitlements[0].OwnerID,
					ChannelID:        "notFound",
					Codes:            []string{"HELLO", "WRATH"},
				})

				assert.Nil(t, err)

				emotes, err := mako.GetEmotesByCodes(ctx, mako.GetEmotesByCodesParams{
					EmoteDB:          test.EmoteDB,
					EntitlementDB:    test.EntitlementDB,
					EmoteModifiersDB: test.EmoteModifiersDB,
					Dynamic:          test.DynamicConfig,
					UserID:           test.Entitlements[0].OwnerID,
					ChannelID:        "notFound",
					Codes:            []string{"HELLO", "WRATH"},
				})

				assert.Nil(t, err)
				assertEmotes(t, test.Emotes, emotes)
			},
		},
	}

	runCodesTests(t, c, tests...)
}

func TestEmoteCodesGlobalAndSmilieEmotes(t *testing.T, c EmoteCodesConfig) {
	tests := []testCodesCase{
		{
			scenario:     "should return global emotes",
			entitlements: []mako.Entitlement{},
			emotes: []mako.Emote{
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = mako.KappaEmoteID
					e.Code = "Kappa"
					e.GroupID = mako.GlobalEmoteGroupID
				}),
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "88"
					e.Code = "PogChamp"
					e.GroupID = mako.GlobalEmoteGroupID
				}),
			},
			test: func(ctx context.Context, t *testing.T, test test) {
				emotes, err := mako.GetEmotesByCodes(ctx, mako.GetEmotesByCodesParams{
					EmoteDB:          test.EmoteDB,
					EntitlementDB:    test.EntitlementDB,
					EmoteModifiersDB: test.EmoteModifiersDB,
					Dynamic:          test.DynamicConfig,
					UserID:           "notFound",
					ChannelID:        "notFound",
					Codes:            []string{"123", "Kappa", "PogChamp"},
				})

				assert.Nil(t, err)

				assertEmotes(t, test.Emotes, emotes)
			},
		},

		{
			scenario: "should return smilie emote if entitled",
			entitlements: []mako.Entitlement{
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = mako.SmilieGroupID
					e.Metadata.GroupID = string(mako.PurpleSmilieGroupID)
				}),
			},
			emotes: []mako.Emote{
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "433"
					e.Code = ":/"
					e.GroupID = string(mako.PurpleSmilieGroupID)
				}),
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "498"
					e.Code = ":/"
					e.GroupID = string(mako.MonkeysSmilieGroupID)
				}),
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "10"
					e.Code = ":/"
					e.GroupID = mako.GlobalEmoteGroupID
				}),
			},
			test: func(ctx context.Context, t *testing.T, test test) {
				emotes, err := mako.GetEmotesByCodes(ctx, mako.GetEmotesByCodesParams{
					EmoteDB:          test.EmoteDB,
					EntitlementDB:    test.EntitlementDB,
					EmoteModifiersDB: test.EmoteModifiersDB,
					Dynamic:          test.DynamicConfig,
					UserID:           test.Entitlements[0].OwnerID,
					ChannelID:        "notFound",
					Codes:            []string{":/"},
				})

				assert.Nil(t, err)
				require.Equal(t, 1, len(emotes))

				assert.Equal(t, "433", emotes[0].ID)
				assert.Equal(t, string(mako.PurpleSmilieGroupID), emotes[0].GroupID)
			},
		},
	}

	runCodesTests(t, c, tests...)
}

func TestEmoteCodesModifiedSingleEmotes(t *testing.T, c EmoteCodesConfig) {
	tests := []testCodesCase{
		{
			scenario: "should return single modified emote when entitled",
			entitlements: []mako.Entitlement{
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = "566_TK"
					e.Type = mako.EmoteEntitlement
				}),
			},
			emotes: []mako.Emote{
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "566"
					e.Code = "HELLO"
					e.EmotesGroup = mk.EmoteGroup_Subscriptions.String()
				}),
			},
			test: func(ctx context.Context, t *testing.T, test test) {
				emotes, err := mako.GetEmotesByCodes(ctx, mako.GetEmotesByCodesParams{
					EmoteDB:          test.EmoteDB,
					EntitlementDB:    test.EntitlementDB,
					EmoteModifiersDB: test.EmoteModifiersDB,
					Dynamic:          test.DynamicConfig,
					UserID:           test.Entitlements[0].OwnerID,
					ChannelID:        "notFound",
					Codes:            []string{"HELLO_TK"},
				})
				assert.Nil(t, err)
				require.Equal(t, len(test.Emotes), len(emotes))

				// Ordering is not guaranteed by any of the APIs.
				sort.Slice(emotes, func(i, j int) bool {
					return bytes.Compare([]byte(emotes[i].ID), []byte(emotes[j].ID)) == -1
				})

				// The second entitlement is for a different owner so we should only get back one.
				assert.Equal(t, "566_TK", emotes[0].ID)
				assert.Equal(t, "HELLO_TK", emotes[0].Code)
			},
		},

		{
			scenario: "should return modified emote and normal emote when entitled",
			entitlements: []mako.Entitlement{
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = "999_TK"
					e.Type = mako.EmoteEntitlement
					e.OwnerID = "555"
				}),
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = "999"
					e.Type = mako.EmoteEntitlement
					e.OwnerID = "555"
				}),
			},
			emotes: []mako.Emote{
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "999"
					e.Code = "HELLO"
					e.EmotesGroup = mk.EmoteGroup_Subscriptions.String()
				}),
			},
			test: func(ctx context.Context, t *testing.T, test test) {
				emotes, err := mako.GetEmotesByCodes(ctx, mako.GetEmotesByCodesParams{
					EmoteDB:          test.EmoteDB,
					EntitlementDB:    test.EntitlementDB,
					EmoteModifiersDB: test.EmoteModifiersDB,
					Dynamic:          test.DynamicConfig,
					UserID:           test.Entitlements[0].OwnerID,
					ChannelID:        "notFound",
					Codes:            []string{"HELLO", "HELLO_TK"},
				})

				assert.Nil(t, err)
				require.Equal(t, 2, len(emotes))

				// Ordering is not guaranteed by any of the APIs.
				sort.Slice(emotes, func(i, j int) bool {
					return bytes.Compare([]byte(emotes[i].ID), []byte(emotes[j].ID)) == -1
				})

				// The second entitlement is for a different owner so we should only get back one.
				assert.Equal(t, "999_TK", emotes[1].ID)
			},
		},

		{
			scenario: "should return modified emote and a group emote",
			entitlements: []mako.Entitlement{
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = "566_TK"
					e.Type = mako.EmoteEntitlement
					e.OwnerID = "myFancyOwner"
				}),
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = "Group1"
					e.Type = mako.EmoteGroupEntitlement
					e.OwnerID = "myFancyOwner"
				}),
			},
			emotes: []mako.Emote{
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "566"
					e.Code = "HELLO"
					e.EmotesGroup = mk.EmoteGroup_Subscriptions.String()
				}),
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "877"
					e.Code = "FOO"
					e.GroupID = "Group1"
					e.EmotesGroup = mk.EmoteGroup_Subscriptions.String()
				}),
			},
			test: func(ctx context.Context, t *testing.T, test test) {
				emotes, err := mako.GetEmotesByCodes(ctx, mako.GetEmotesByCodesParams{
					EmoteDB:          test.EmoteDB,
					EntitlementDB:    test.EntitlementDB,
					EmoteModifiersDB: test.EmoteModifiersDB,
					Dynamic:          test.DynamicConfig,
					UserID:           test.Entitlements[0].OwnerID,
					ChannelID:        "notFound",
					Codes:            []string{"HELLO_TK", "FOO"},
				})

				assert.Nil(t, err)
				require.Equal(t, len(test.Emotes), len(emotes))

				// Ordering is not guaranteed by any of the APIs.
				sort.Slice(emotes, func(i, j int) bool {
					return bytes.Compare([]byte(emotes[i].ID), []byte(emotes[j].ID)) == -1
				})

				// Ensure modified emotes are returned with the modifier so the CDN asset can be found.
				assert.Equal(t, "566_TK", emotes[0].ID)
				assert.Equal(t, "877", emotes[1].ID)
			},
		},

		{
			scenario: "should return modified emote id",
			entitlements: []mako.Entitlement{
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = "566_TK"
					e.Type = mako.EmoteEntitlement
					e.OwnerID = "myFancyOwner"
				}),
			},
			emotes: []mako.Emote{
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "566"
					e.Code = "HELLO"
					e.EmotesGroup = mk.EmoteGroup_Subscriptions.String()
				}),
			},
			test: func(ctx context.Context, t *testing.T, test test) {
				emotes, err := mako.GetEmotesByCodes(ctx, mako.GetEmotesByCodesParams{
					EmoteDB:          test.EmoteDB,
					EntitlementDB:    test.EntitlementDB,
					EmoteModifiersDB: test.EmoteModifiersDB,
					Dynamic:          test.DynamicConfig,
					UserID:           test.Entitlements[0].OwnerID,
					ChannelID:        "notFound",
					Codes:            []string{"HELLO_TK"},
				})

				assert.Nil(t, err)
				require.Equal(t, len(test.Emotes), len(emotes))

				if len(emotes) == 1 {
					assert.NotEqual(t, test.Emotes[0].ID, emotes[0].ID)
					assert.Equal(t, "566_TK", emotes[0].ID)
				}
			},
		},

		{
			scenario: "does not return invalid modified emotes when the base emote is in the same group as modified emotes",
			entitlements: []mako.Entitlement{
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = "123"
					e.Type = mako.EmoteGroupEntitlement
					e.OwnerID = "555"
				}),
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = "999_TK"
					e.Type = mako.EmoteEntitlement
					e.OwnerID = "555"
				}),
			},
			emotes: []mako.Emote{
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "999"
					e.Code = "HELLO"
					e.GroupID = "123"
					e.EmotesGroup = mk.EmoteGroup_Subscriptions.String()
				}),
			},
			test: func(ctx context.Context, t *testing.T, test test) {
				emotes, err := mako.GetEmotesByCodes(ctx, mako.GetEmotesByCodesParams{
					EmoteDB:          test.EmoteDB,
					EntitlementDB:    test.EntitlementDB,
					EmoteModifiersDB: test.EmoteModifiersDB,
					Dynamic:          test.DynamicConfig,
					UserID:           test.Entitlements[0].OwnerID,
					ChannelID:        "notFound",
					Codes:            []string{"HELLO_TK", "HELLO"},
				})

				assert.Nil(t, err)

				// Only "HELLO_TK" and "HELLO" should be entitled. This test case is to test for a bug
				// where "HELLO_TK_HELLO" would be improperly entitled when the order of emotes passed
				// in was [<modified_emote>, <base_emote>], so 3 emotes ended up being improperly entitled
				require.Equal(t, 2, len(emotes))
			},
		},

		{
			scenario:     "global emotes do not support modifiers",
			entitlements: []mako.Entitlement{},
			emotes: []mako.Emote{
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "123"
					e.Code = "Kappa"
					e.GroupID = "0"
					e.EmotesGroup = mk.EmoteGroup_Globals.String()
				}),
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "543"
					e.Code = "Foobar"
					e.GroupID = "0"
					e.EmotesGroup = mk.EmoteGroup_Globals.String()
				}),
			},
			test: func(ctx context.Context, t *testing.T, test test) {
				emotes, err := mako.GetEmotesByCodes(ctx, mako.GetEmotesByCodesParams{
					EmoteDB:          test.EmoteDB,
					EntitlementDB:    test.EntitlementDB,
					EmoteModifiersDB: test.EmoteModifiersDB,
					Dynamic:          test.DynamicConfig,
					UserID:           "fakeOwner",
					ChannelID:        "notFound",
					Codes:            []string{"Kappa_SG"},
				})

				assert.Nil(t, err)

				require.Equal(t, 0, len(emotes))
			},
		},

		{
			scenario: "should not return a modified emote that doesn't exist and when the base group is entitled",
			entitlements: []mako.Entitlement{
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = "123"
					e.Type = mako.EmoteGroupEntitlement
					e.OwnerID = "555"
				}),
			},
			emotes: []mako.Emote{
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "222"
					e.Code = "Hello"
					e.GroupID = "123"
					e.EmotesGroup = mk.EmoteGroup_Subscriptions.String()
				}),
			},
			test: func(ctx context.Context, t *testing.T, test test) {
				emotes, err := mako.GetEmotesByCodes(ctx, mako.GetEmotesByCodesParams{
					EmoteDB:          test.EmoteDB,
					EntitlementDB:    test.EntitlementDB,
					EmoteModifiersDB: test.EmoteModifiersDB,
					Dynamic:          test.DynamicConfig,
					UserID:           test.Entitlements[0].OwnerID,
					ChannelID:        "notFound",
					Codes:            []string{"HELLO_BW"},
				})

				assert.Nil(t, err)

				require.Equal(t, 0, len(emotes))
			},
		},
	}

	runCodesTests(t, c, tests...)
}

func TestEmoteCodesSingleEmotes(t *testing.T, c EmoteCodesConfig) {
	tests := []testCodesCase{
		{
			scenario: "should return single emote when entitled",
			entitlements: []mako.Entitlement{
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = "566"
					e.Type = mako.EmoteEntitlement
				}),
			},
			emotes: []mako.Emote{
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "566"
				}),
			},
			test: func(ctx context.Context, t *testing.T, test test) {
				emotes, err := mako.GetEmotesByCodes(ctx, mako.GetEmotesByCodesParams{
					EmoteDB:          test.EmoteDB,
					EntitlementDB:    test.EntitlementDB,
					EmoteModifiersDB: test.EmoteModifiersDB,
					Dynamic:          test.DynamicConfig,
					UserID:           test.Entitlements[0].OwnerID,
					ChannelID:        "notFound",
					Codes:            makeEmoteCodes(test.Emotes...),
				})
				assert.Nil(t, err)
				require.Equal(t, len(test.Emotes), len(emotes))

				// Ordering is not guaranteed by any of the APIs.
				sort.Slice(emotes, func(i, j int) bool {
					return bytes.Compare([]byte(emotes[i].ID), []byte(emotes[j].ID)) == -1
				})

				// The second entitlement is for a different owner so we should only get back one.
				assert.Equal(t, "566", emotes[0].ID)
			},
		},

		{
			scenario: "only entitle the emote that has a valid entitlement",
			entitlements: []mako.Entitlement{
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.Source = mako.HypeTrainSource
					e.ID = "301739499"
					e.Type = mako.EmoteEntitlement
				}),
			},
			emotes: []mako.Emote{
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "301739499"
					e.Code = "HypePunch"
					e.GroupID = "301040478"
				}),
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "23128"
					e.Code = "Choo"
					e.GroupID = "6836"
				}),
			},
			test: func(ctx context.Context, t *testing.T, test test) {
				emotes, err := mako.GetEmotesByCodes(ctx, mako.GetEmotesByCodesParams{
					EmoteDB:          test.EmoteDB,
					EntitlementDB:    test.EntitlementDB,
					EmoteModifiersDB: test.EmoteModifiersDB,
					Dynamic:          test.DynamicConfig,
					UserID:           test.Entitlements[0].OwnerID,
					ChannelID:        "notFound",
					Codes:            makeEmoteCodes(test.Emotes...),
				})

				assert.Nil(t, err)
				require.Equal(t, 1, len(emotes))

				assert.Equal(t, "301739499", emotes[0].ID)
				assert.Equal(t, "301040478", emotes[0].GroupID)
			},
		},
	}

	runCodesTests(t, c, tests...)
}

func TestEmoteCodesGroupEmotes(t *testing.T, c EmoteCodesConfig) {
	tests := []testCodesCase{
		{
			scenario: "should return an emote when an emote group entitlement is found",
			entitlements: []mako.Entitlement{
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = "123"
				}),
			},
			emotes: []mako.Emote{
				makeEmoteFn(func(e *mako.Emote) {
					e.GroupID = "123"
				}),
			},
			test: func(ctx context.Context, t *testing.T, test test) {
				emotes, err := mako.GetEmotesByCodes(ctx, mako.GetEmotesByCodesParams{
					EmoteDB:          test.EmoteDB,
					EntitlementDB:    test.EntitlementDB,
					EmoteModifiersDB: test.EmoteModifiersDB,
					Dynamic:          test.DynamicConfig,
					UserID:           test.Entitlements[0].OwnerID,
					ChannelID:        "notFound",
					Codes:            makeEmoteCodes(test.Emotes...),
				})
				assert.Nil(t, err)

				assertEmotes(t, test.Emotes, emotes)
			},
		},

		{
			scenario: "should return multiple emotes when an emote group entitlement is found",
			entitlements: []mako.Entitlement{
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = "123"
				}),
			},
			emotes: []mako.Emote{
				makeEmoteFn(func(e *mako.Emote) {
					e.GroupID = "123"
				}),
				makeEmoteFn(func(e *mako.Emote) {
					e.GroupID = "123"
				}),
				makeEmoteFn(func(e *mako.Emote) {
					e.GroupID = "123"
				}),
				makeEmoteFn(func(e *mako.Emote) {
					e.GroupID = "123"
				}),
			},
			test: func(ctx context.Context, t *testing.T, test test) {
				emotes, err := mako.GetEmotesByCodes(ctx, mako.GetEmotesByCodesParams{
					EmoteDB:          test.EmoteDB,
					EntitlementDB:    test.EntitlementDB,
					EmoteModifiersDB: test.EmoteModifiersDB,
					Dynamic:          test.DynamicConfig,
					UserID:           test.Entitlements[0].OwnerID,
					ChannelID:        "notFound",
					Codes:            makeEmoteCodes(test.Emotes...),
				})
				assert.Nil(t, err)

				assertEmotes(t, test.Emotes, emotes)
			},
		},

		{
			scenario: "should return user's emotes",
			entitlements: []mako.Entitlement{
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = "20704"
				}),
			},
			emotes: []mako.Emote{
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "300997894"
					e.Code = "miiliS"
					e.GroupID = "20704"
					e.State = mako.Active
				}),
			},
			test: func(ctx context.Context, t *testing.T, test test) {
				emotes, err := mako.GetEmotesByCodes(ctx, mako.GetEmotesByCodesParams{
					EmoteDB:          test.EmoteDB,
					EntitlementDB:    test.EntitlementDB,
					EmoteModifiersDB: test.EmoteModifiersDB,
					Dynamic:          test.DynamicConfig,
					UserID:           test.Entitlements[0].OwnerID,
					ChannelID:        "notFound",
					Codes:            makeEmoteCodes(test.Emotes...),
				})

				assert.Nil(t, err)
				require.Equal(t, 1, len(emotes))

				assert.Equal(t, test.Emotes[0].ID, emotes[0].ID)
			},
		},

		{
			scenario: "should return v0oidW",
			entitlements: []mako.Entitlement{
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = "21282"
				}),
			},
			emotes: []mako.Emote{
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "1277634"
					e.Code = "v0oidW"
					e.GroupID = "21282"
					e.State = mako.Active
				}),
			},
			test: func(ctx context.Context, t *testing.T, test test) {
				emotes, err := mako.GetEmotesByCodes(ctx, mako.GetEmotesByCodesParams{
					EmoteDB:          test.EmoteDB,
					EntitlementDB:    test.EntitlementDB,
					EmoteModifiersDB: test.EmoteModifiersDB,
					Dynamic:          test.DynamicConfig,
					UserID:           test.Entitlements[0].OwnerID,
					ChannelID:        "notFound",
					Codes:            makeEmoteCodes(test.Emotes...),
				})

				assert.Nil(t, err)
				require.Equal(t, 1, len(emotes))

				assert.Equal(t, test.Emotes[0].ID, emotes[0].ID)
			},
		},

		{
			scenario: "should return no modified emotes even when a BTER emote group entitlement is found",
			entitlements: []mako.Entitlement{
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = "123"
					e.Type = mako.EmoteGroupEntitlement
					e.Source = mako.BitsBadgeTierSource
				}),
			},
			emotes: []mako.Emote{
				makeEmoteFn(func(e *mako.Emote) {
					e.GroupID = "123"
					e.EmotesGroup = mkd.EmoteOrigin_BitsBadgeTierEmotes.String()
					e.OwnerID = "channelID"
				}),
				makeEmoteFn(func(e *mako.Emote) {
					e.GroupID = "123"
					e.EmotesGroup = mkd.EmoteOrigin_BitsBadgeTierEmotes.String()
					e.OwnerID = "channelID"
				}),
			},
			emoteModifierGroups: []mako.EmoteModifierGroup{
				makeEmoteModifierGroupFn(func(e *mako.EmoteModifierGroup) {
					e.Modifiers = []string{"HF"}
				}),
			},
			test: func(ctx context.Context, t *testing.T, test test) {
				emotes, err := mako.GetEmotesByCodes(ctx, mako.GetEmotesByCodesParams{
					EmoteDB:          test.EmoteDB,
					EntitlementDB:    test.EntitlementDB,
					EmoteModifiersDB: test.EmoteModifiersDB,
					Dynamic:          test.DynamicConfig,
					UserID:           test.Entitlements[0].OwnerID,
					ChannelID:        test.Entitlements[0].ChannelID,
					Codes:            []string{test.Emotes[0].Code + "_HF", test.Emotes[1].Code + "_HF"},
				})
				assert.Nil(t, err)

				assertEmotes(t, nil, emotes)
			},
		},
	}

	runCodesTests(t, c, tests...)
}

func TestEmoteCodesSpecialEmotes(t *testing.T, c EmoteCodesConfig) {
	tests := []testCodesCase{
		{
			scenario: "should return 2FA emote",
			entitlements: []mako.Entitlement{
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = mako.TwoFactorEmoteGroupID
					e.Type = mako.EmoteEntitlement
				}),
			},
			emotes: []mako.Emote{
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "123"
					e.GroupID = mako.TwoFactorEmoteGroupID
					e.Code = "SirPrise"
				}),
			},
			test: func(ctx context.Context, t *testing.T, test test) {
				emotes, err := mako.GetEmotesByCodes(ctx, mako.GetEmotesByCodesParams{
					EmoteDB:          test.EmoteDB,
					EntitlementDB:    test.EntitlementDB,
					EmoteModifiersDB: test.EmoteModifiersDB,
					Dynamic:          test.DynamicConfig,
					UserID:           test.Entitlements[0].OwnerID,
					ChannelID:        "notFound",
					Codes:            makeEmoteCodes(test.Emotes...),
				})

				assert.Nil(t, err)
				require.Equal(t, 1, len(emotes))
				RequireEmoteEquality(t, test.Emotes[0], emotes[0])
			},
		},
	}

	runCodesTests(t, c, tests...)
}

func TestEmoteCodesT2T3ModifiedEmotes(t *testing.T, c EmoteCodesConfig) {
	tests := []testCodesCase{
		{
			scenario:     "should return 0 emotes when owner has no entitlements",
			entitlements: []mako.Entitlement{},
			emotes: []mako.Emote{
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "999"
					e.OwnerID = "emoteOwner"
					e.GroupID = "123"
					e.Code = "Hello"
					e.State = mako.Active
				}),
			},
			emoteModifierGroups: []mako.EmoteModifierGroup{
				makeEmoteModifierGroupFn(func(e *mako.EmoteModifierGroup) {
					e.OwnerID = "ownerID"
					e.Modifiers = []string{"BW"}
					e.SourceEmoteGroupIDs = []string{"123"}
				}),
			},
			test: func(ctx context.Context, t *testing.T, test test) {
				emotes, err := mako.GetEmotesByCodes(ctx, mako.GetEmotesByCodesParams{
					EmoteDB:          test.EmoteDB,
					EntitlementDB:    test.EntitlementDB,
					EmoteModifiersDB: test.EmoteModifiersDB,
					Dynamic:          test.DynamicConfig,
					UserID:           "ownerID",
					ChannelID:        "notFound",
					Codes:            makeEmoteCodes(test.Emotes...),
				})
				assert.Nil(t, err)
				require.Equal(t, 0, len(emotes))
			},
		},
		{
			scenario: "should return 0 emotes when owner is missing a single required entitlement",
			entitlements: []mako.Entitlement{
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = "123"
					e.OwnerID = "ownerID"
				}),
			},
			emotes: []mako.Emote{
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "999"
					e.OwnerID = "emoteOwner"
					e.GroupID = "124"
					e.Code = "Single"
				}),
			},
			emoteModifierGroups: []mako.EmoteModifierGroup{
				makeEmoteModifierGroupFn(func(e *mako.EmoteModifierGroup) {
					e.OwnerID = "ownerID"
					e.Modifiers = []string{"BW"}
					e.SourceEmoteGroupIDs = []string{"124"}
				}),
			},
			test: func(ctx context.Context, t *testing.T, test test) {
				emotes, err := mako.GetEmotesByCodes(ctx, mako.GetEmotesByCodesParams{
					EmoteDB:          test.EmoteDB,
					EntitlementDB:    test.EntitlementDB,
					EmoteModifiersDB: test.EmoteModifiersDB,
					Dynamic:          test.DynamicConfig,
					UserID:           "ownerID",
					ChannelID:        "notFound",
					Codes:            []string{test.Emotes[0].Code + "_BW"},
				})

				assert.Nil(t, err)
				require.Equal(t, 0, len(emotes))
			},
		},
		{
			scenario: "should return 0 emotes when owner is missing multiple required entitlements",
			entitlements: []mako.Entitlement{
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = "123"
					e.OwnerID = "ownerID"
				}),
			},
			emotes: []mako.Emote{
				makeEmoteFn(func(e *mako.Emote) {
					e.OwnerID = "emoteOwner"
					e.GroupID = "124"
					e.Code = "Hello"
				}),
				makeEmoteFn(func(e *mako.Emote) {
					e.OwnerID = "emoteOwner"
					e.GroupID = "125"
					e.Code = "World"
				}),
			},
			emoteModifierGroups: []mako.EmoteModifierGroup{
				makeEmoteModifierGroupFn(func(e *mako.EmoteModifierGroup) {
					e.OwnerID = "ownerID"
					e.Modifiers = []string{"BW"}
					e.SourceEmoteGroupIDs = []string{"124", "125"}
				}),
			},
			test: func(ctx context.Context, t *testing.T, test test) {
				emotes, err := mako.GetEmotesByCodes(ctx, mako.GetEmotesByCodesParams{
					EmoteDB:          test.EmoteDB,
					EntitlementDB:    test.EntitlementDB,
					EmoteModifiersDB: test.EmoteModifiersDB,
					Dynamic:          test.DynamicConfig,
					UserID:           "ownerID",
					ChannelID:        "notFound",
					Codes:            []string{test.Emotes[0].Code + "_BW", test.Emotes[1].Code + "_BW"},
				})
				assert.Nil(t, err)
				require.Equal(t, 0, len(emotes))
			},
		},
		{
			scenario: "should return 0 emotes when the owner is entitled to the correct groups, but is using an unsupported modifier",
			entitlements: []mako.Entitlement{
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = "123"
					e.OwnerID = "ownerID"
				}),
			},
			emotes: []mako.Emote{
				makeEmoteFn(func(e *mako.Emote) {
					e.OwnerID = "emoteOwner"
					e.GroupID = "123"
					e.Code = "Hello"
				}),
			},
			emoteModifierGroups: []mako.EmoteModifierGroup{
				makeEmoteModifierGroupFn(func(e *mako.EmoteModifierGroup) {
					e.OwnerID = "ownerID"
					e.Modifiers = []string{"BW"}
					e.SourceEmoteGroupIDs = []string{"123"}
				}),
			},
			test: func(ctx context.Context, t *testing.T, test test) {
				emotes, err := mako.GetEmotesByCodes(ctx, mako.GetEmotesByCodesParams{
					EmoteDB:          test.EmoteDB,
					EntitlementDB:    test.EntitlementDB,
					EmoteModifiersDB: test.EmoteModifiersDB,
					Dynamic:          test.DynamicConfig,
					UserID:           "ownerID",
					ChannelID:        "notFound",
					Codes:            []string{test.Emotes[0].Code + "_SG"},
				})
				assert.Nil(t, err)
				require.Equal(t, 0, len(emotes))
			},
		},
		{
			scenario: "should return 0 emotes when the owner is entitled to the correct groups, but is attemping to modify an animated emote",
			entitlements: []mako.Entitlement{
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = "123"
					e.OwnerID = "ownerID"
				}),
			},
			emotes: []mako.Emote{
				makeEmoteFn(func(e *mako.Emote) {
					e.OwnerID = "emoteOwner"
					e.GroupID = "123"
					e.Code = "Hello"
					e.AssetType = mako.AnimatedAssetType
				}),
			},
			emoteModifierGroups: []mako.EmoteModifierGroup{
				makeEmoteModifierGroupFn(func(e *mako.EmoteModifierGroup) {
					e.OwnerID = "ownerID"
					e.Modifiers = []string{"BW"}
					e.SourceEmoteGroupIDs = []string{"123"}
				}),
			},
			test: func(ctx context.Context, t *testing.T, test test) {
				emotes, err := mako.GetEmotesByCodes(ctx, mako.GetEmotesByCodesParams{
					EmoteDB:          test.EmoteDB,
					EntitlementDB:    test.EntitlementDB,
					EmoteModifiersDB: test.EmoteModifiersDB,
					Dynamic:          test.DynamicConfig,
					UserID:           "ownerID",
					ChannelID:        "notFound",
					Codes:            []string{test.Emotes[0].Code + "_BW"},
				})
				assert.Nil(t, err)
				require.Equal(t, 0, len(emotes))
			},
		},
		{
			scenario: "should return modified emotes when the owner has the correct entitlement",
			entitlements: []mako.Entitlement{
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = "123"
					e.OwnerID = "ownerID"
				}),
			},
			emotes: []mako.Emote{
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "999"
					e.OwnerID = "emoteOwner"
					e.GroupID = "123"
					e.Code = "Hello"
					e.EmotesGroup = mk.EmoteGroup_Subscriptions.String()
				}),
			},
			emoteModifierGroups: []mako.EmoteModifierGroup{
				makeEmoteModifierGroupFn(func(e *mako.EmoteModifierGroup) {
					e.OwnerID = "emoteOwner"
					e.Modifiers = []string{"BW"}
					e.SourceEmoteGroupIDs = []string{"123"}
				}),
			},
			test: func(ctx context.Context, t *testing.T, test test) {
				emotes, err := mako.GetEmotesByCodes(ctx, mako.GetEmotesByCodesParams{
					EmoteDB:          test.EmoteDB,
					EntitlementDB:    test.EntitlementDB,
					EmoteModifiersDB: test.EmoteModifiersDB,
					Dynamic:          test.DynamicConfig,
					UserID:           "ownerID",
					ChannelID:        "notFound",
					Codes:            []string{test.Emotes[0].Code + "_BW"},
				})
				assert.Nil(t, err)
				require.Equal(t, 1, len(emotes))
				require.Equal(t, emotes[0].ID, test.Emotes[0].ID+"_BW")
			},
		},
		{
			scenario: "should return modified emotes when the owner has the correct entitlements",
			entitlements: []mako.Entitlement{
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = "123"
					e.OwnerID = "ownerID"
				}),
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = "124"
					e.OwnerID = "ownerID"
				}),
			},
			emotes: []mako.Emote{
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "999"
					e.OwnerID = "emoteOwner"
					e.GroupID = "123"
					e.Code = "Hello"
					e.EmotesGroup = mk.EmoteGroup_Subscriptions.String()
				}),
			},
			emoteModifierGroups: []mako.EmoteModifierGroup{
				makeEmoteModifierGroupFn(func(e *mako.EmoteModifierGroup) {
					e.OwnerID = "emoteOwner"
					e.Modifiers = []string{"BW"}
					e.SourceEmoteGroupIDs = []string{"123"}
				}),
				makeEmoteModifierGroupFn(func(e *mako.EmoteModifierGroup) {
					e.OwnerID = "emoteOwner"
					e.Modifiers = []string{"SG", "HF"}
					e.SourceEmoteGroupIDs = []string{"123"}
				}),
			},
			test: func(ctx context.Context, t *testing.T, test test) {
				emotes, err := mako.GetEmotesByCodes(ctx, mako.GetEmotesByCodesParams{
					EmoteDB:          test.EmoteDB,
					EntitlementDB:    test.EntitlementDB,
					EmoteModifiersDB: test.EmoteModifiersDB,
					Dynamic:          test.DynamicConfig,
					UserID:           "ownerID",
					ChannelID:        "notFound",
					Codes: []string{
						test.Emotes[0].Code + "_BW",
						test.Emotes[0].Code + "_SG",
						test.Emotes[0].Code + "_HF",
					},
				})
				assert.Nil(t, err)
				require.Equal(t, 3, len(emotes))
			},
		},
		{
			scenario: "should not allow a user to use higher tier modifiers than they are entitled to",
			entitlements: []mako.Entitlement{
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = "123"
					e.OwnerID = "ownerID"
				}),
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = "124"
					e.OwnerID = "ownerID"
				}),
			},
			emotes: []mako.Emote{
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "999"
					e.OwnerID = "emoteOwner"
					e.GroupID = "123"
					e.Code = "Hello"
					e.EmotesGroup = mk.EmoteGroup_Subscriptions.String()
				}),
			},
			emoteModifierGroups: []mako.EmoteModifierGroup{
				makeEmoteModifierGroupFn(func(e *mako.EmoteModifierGroup) {
					e.OwnerID = "emoteOwner"
					e.Modifiers = []string{"BW"}
					e.SourceEmoteGroupIDs = []string{"123"}
				}),
				makeEmoteModifierGroupFn(func(e *mako.EmoteModifierGroup) {
					e.OwnerID = "emoteOwner"
					e.Modifiers = []string{"SG", "HF"}
					e.SourceEmoteGroupIDs = []string{"123"}
				}),
			},
			test: func(ctx context.Context, t *testing.T, test test) {
				emotes, err := mako.GetEmotesByCodes(ctx, mako.GetEmotesByCodesParams{
					EmoteDB:          test.EmoteDB,
					EntitlementDB:    test.EntitlementDB,
					EmoteModifiersDB: test.EmoteModifiersDB,
					Dynamic:          test.DynamicConfig,
					UserID:           "ownerID",
					ChannelID:        "notFound",
					Codes: []string{
						test.Emotes[0].Code + "_BW",
						test.Emotes[0].Code + "_SG",
						test.Emotes[0].Code + "_HF",
						test.Emotes[0].Code + "_SQ",
						test.Emotes[0].Code + "_TK",
					},
				})
				assert.Nil(t, err)
				require.Equal(t, 3, len(emotes))
			},
		},
		{
			scenario: "should not allow t2 subscribers to use t3 emotes with t2 modifiers",
			entitlements: []mako.Entitlement{
				// entitle test user to entitlement 123
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = "123"
					e.OwnerID = "ownerID"
				}),
			},
			// create an emote that's in group 124
			emotes: []mako.Emote{
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "999"
					e.OwnerID = "emoteOwner"
					e.GroupID = "124"
					e.Code = "Hello"
				}),
			},
			// create emote modifier groups
			emoteModifierGroups: []mako.EmoteModifierGroup{
				// one group that requires entitlement to 123 (t2 sub) and allows access to BW
				makeEmoteModifierGroupFn(func(e *mako.EmoteModifierGroup) {
					e.OwnerID = "emoteOwner"
					e.Modifiers = []string{"BW"}
					e.SourceEmoteGroupIDs = []string{"123"}
				}),
				// one group that requires entitlement to 123 and 124 (t3 sub) and allows access to SG, HF
				makeEmoteModifierGroupFn(func(e *mako.EmoteModifierGroup) {
					e.OwnerID = "emoteOwner"
					e.Modifiers = []string{"SG", "HF"}
					e.SourceEmoteGroupIDs = []string{"123", "124"}
				}),
			},
			test: func(ctx context.Context, t *testing.T, test test) {
				emotes, err := mako.GetEmotesByCodes(ctx, mako.GetEmotesByCodesParams{
					EmoteDB:          test.EmoteDB,
					EntitlementDB:    test.EntitlementDB,
					EmoteModifiersDB: test.EmoteModifiersDB,
					Dynamic:          test.DynamicConfig,
					UserID:           "ownerID",
					ChannelID:        "notFound",
					Codes:            []string{test.Emotes[0].Code + "_BW"}, // Emotes[0] is a t3 emote, BW is a t2 modifier
				})
				assert.Nil(t, err)
				// user should not be allowed to use Hello_BW
				require.Equal(t, 0, len(emotes))
			},
		},
	}

	runCodesTests(t, c, tests...)
}

func TestEmoteCodesFollowerEmotes(t *testing.T, c EmoteCodesConfig) {
	tests := []testCodesCase{
		{
			scenario: "should return an emote when a follower emote group entitlement is found",
			entitlements: []mako.Entitlement{
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = mako.GenerateFollowerEntitlementID("123", "channelID")
					e.OwnerID = "ownerID"
					e.ChannelID = "channelID"
					e.Source = mako.FollowerSource
					e.Type = mako.EmoteGroupEntitlement
					e.OriginID = mako.FollowerOriginID.String()
					e.Metadata = mako.EntitlementMetadata{GroupID: "123"}
				}),
			},
			emotes: []mako.Emote{
				makeEmoteFn(func(e *mako.Emote) {
					e.GroupID = "123"
					e.EmotesGroup = mkd.EmoteOrigin_Follower.String()
					e.OwnerID = "channelID"
				}),
			},
			test: func(ctx context.Context, t *testing.T, test test) {
				emotes, err := mako.GetEmotesByCodes(ctx, mako.GetEmotesByCodesParams{
					EmoteDB:          test.EmoteDB,
					EntitlementDB:    test.EntitlementDB,
					EmoteModifiersDB: test.EmoteModifiersDB,
					Dynamic:          test.DynamicConfig,
					UserID:           test.Entitlements[0].OwnerID,
					ChannelID:        test.Emotes[0].OwnerID,
					Codes:            makeEmoteCodes(test.Emotes...),
				})
				assert.Nil(t, err)

				assertEmotes(t, test.Emotes, emotes)
			},
		},
		{
			scenario: "should return all emotes in the group when an emote follower group entitlement is found",
			entitlements: []mako.Entitlement{
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = mako.GenerateFollowerEntitlementID("123", "channelID")
					e.OwnerID = "ownerID"
					e.ChannelID = "channelID"
					e.Source = mako.FollowerSource
					e.Type = mako.EmoteGroupEntitlement
					e.OriginID = mako.FollowerOriginID.String()
					e.Metadata = mako.EntitlementMetadata{GroupID: "123"}
				}),
			},
			emotes: []mako.Emote{
				makeEmoteFn(func(e *mako.Emote) {
					e.GroupID = "123"
					e.EmotesGroup = mkd.EmoteOrigin_Follower.String()
					e.OwnerID = "channelID"
				}),
				makeEmoteFn(func(e *mako.Emote) {
					e.GroupID = "123"
					e.EmotesGroup = mkd.EmoteOrigin_Follower.String()
					e.OwnerID = "channelID"
				}),
			},
			test: func(ctx context.Context, t *testing.T, test test) {
				emotes, err := mako.GetEmotesByCodes(ctx, mako.GetEmotesByCodesParams{
					EmoteDB:          test.EmoteDB,
					EntitlementDB:    test.EntitlementDB,
					EmoteModifiersDB: test.EmoteModifiersDB,
					Dynamic:          test.DynamicConfig,
					UserID:           test.Entitlements[0].OwnerID,
					ChannelID:        test.Entitlements[0].ChannelID,
					Codes:            makeEmoteCodes(test.Emotes...),
				})
				assert.Nil(t, err)

				assertEmotes(t, test.Emotes, emotes)
			},
		},
		{
			scenario: "should not return any single emote entitlements for follower emotes",
			entitlements: []mako.Entitlement{
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = "emoteID"
					e.OwnerID = "ownerID"
					e.ChannelID = "channelID"
					e.Source = mako.FollowerSource
					e.Type = mako.EmoteGroupEntitlement
					e.OriginID = mako.FollowerOriginID.String()
					e.Metadata = mako.EntitlementMetadata{GroupID: "123"}
				}),
			},
			emotes: []mako.Emote{
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "emoteID"
					e.GroupID = "123"
					e.EmotesGroup = mkd.EmoteOrigin_Follower.String()
					e.OwnerID = "channelID"
				}),
			},
			test: func(ctx context.Context, t *testing.T, test test) {
				emotes, err := mako.GetEmotesByCodes(ctx, mako.GetEmotesByCodesParams{
					EmoteDB:          test.EmoteDB,
					EntitlementDB:    test.EntitlementDB,
					EmoteModifiersDB: test.EmoteModifiersDB,
					Dynamic:          test.DynamicConfig,
					UserID:           test.Entitlements[0].OwnerID,
					ChannelID:        test.Entitlements[0].ChannelID,
					Codes:            makeEmoteCodes(test.Emotes...),
				})
				assert.Nil(t, err)

				assertEmotes(t, nil, emotes)
			},
		},
		{
			scenario: "should not return any standard group emote entitlements for follower emotes",
			entitlements: []mako.Entitlement{
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = "123"
					e.OwnerID = "ownerID"
					e.ChannelID = "channelID"
					e.Source = mako.FollowerSource
					e.Type = mako.EmoteGroupEntitlement
					e.OriginID = mako.FollowerOriginID.String()
				}),
			},
			emotes: []mako.Emote{
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "emoteID"
					e.GroupID = "123"
					e.EmotesGroup = mkd.EmoteOrigin_Follower.String()
					e.OwnerID = "channelID"
				}),
			},
			test: func(ctx context.Context, t *testing.T, test test) {
				emotes, err := mako.GetEmotesByCodes(ctx, mako.GetEmotesByCodesParams{
					EmoteDB:          test.EmoteDB,
					EntitlementDB:    test.EntitlementDB,
					EmoteModifiersDB: test.EmoteModifiersDB,
					Dynamic:          test.DynamicConfig,
					UserID:           test.Entitlements[0].OwnerID,
					ChannelID:        test.Entitlements[0].ChannelID,
					Codes:            makeEmoteCodes(test.Emotes...),
				})
				assert.Nil(t, err)

				assertEmotes(t, nil, emotes)
			},
		},
		{
			scenario: "should return no modified emotes even when an emote follower group entitlement is found",
			entitlements: []mako.Entitlement{
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = mako.GenerateFollowerEntitlementID("123", "channelID")
					e.OwnerID = "ownerID"
					e.ChannelID = "channelID"
					e.Source = mako.FollowerSource
					e.Type = mako.EmoteGroupEntitlement
					e.OriginID = mako.FollowerOriginID.String()
					e.Metadata = mako.EntitlementMetadata{GroupID: "123"}
				}),
			},
			emotes: []mako.Emote{
				makeEmoteFn(func(e *mako.Emote) {
					e.GroupID = "123"
					e.EmotesGroup = mkd.EmoteOrigin_Follower.String()
					e.OwnerID = "channelID"
				}),
				makeEmoteFn(func(e *mako.Emote) {
					e.GroupID = "123"
					e.EmotesGroup = mkd.EmoteOrigin_Follower.String()
					e.OwnerID = "channelID"
				}),
			},
			emoteModifierGroups: []mako.EmoteModifierGroup{
				makeEmoteModifierGroupFn(func(e *mako.EmoteModifierGroup) {
					e.Modifiers = []string{"HF"}
				}),
			},
			test: func(ctx context.Context, t *testing.T, test test) {
				emotes, err := mako.GetEmotesByCodes(ctx, mako.GetEmotesByCodesParams{
					EmoteDB:          test.EmoteDB,
					EntitlementDB:    test.EntitlementDB,
					EmoteModifiersDB: test.EmoteModifiersDB,
					Dynamic:          test.DynamicConfig,
					UserID:           test.Entitlements[0].OwnerID,
					ChannelID:        test.Entitlements[0].ChannelID,
					Codes:            []string{test.Emotes[0].Code + "_HF", test.Emotes[1].Code + "_HF"},
				})
				assert.Nil(t, err)

				assertEmotes(t, nil, emotes)
			},
		},
	}

	runCodesTests(t, c, tests...)
}

func runCodesTests(t *testing.T, c EmoteCodesConfig, tests ...testCodesCase) {
	for _, testCase := range tests {
		testCase := testCase

		t.Run(testCase.scenario, func(t *testing.T) {
			t.Parallel()

			ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
			defer cancel()

			entitlementDB, entitlementTeardown, err := c.MakeEntitlementDB(ctx, testCase.entitlements...)
			if err != nil {
				t.Fatal(err)
			}

			defer entitlementTeardown()

			emoteDB, emoteTeardown, err := c.MakeEmoteDB(ctx, testCase.emotes...)
			if err != nil {
				t.Fatal(err)
			}

			defer emoteTeardown()

			emoteModifiersDB, err := c.MakeEmoteModifierDB(ctx)
			if err != nil {
				t.Fatal(err)
			}

			emoteModifiersDB.CreateGroups(ctx, testCase.emoteModifierGroups...)

			dyanmicConfig := config.NewDynamicConfigFake()

			testCase.test(ctx, t, test{
				EmoteDB:          emoteDB,
				EntitlementDB:    entitlementDB,
				EmoteModifiersDB: emoteModifiersDB,
				Emotes:           testCase.emotes,
				Entitlements:     testCase.entitlements,
				DynamicConfig:    dyanmicConfig,
			})
		})
	}
}

func assertEmotes(t *testing.T, expected []mako.Emote, given []mako.Emote) {
	require.Equal(t, len(expected), len(given))

	sortEmotes(expected...)
	sortEmotes(given...)

	for i, emote := range expected {
		RequireEmoteEquality(t, emote, given[i])
	}
}

func sortEmotes(emotes ...mako.Emote) {
	sort.Slice(emotes, func(i, j int) bool {
		return bytes.Compare([]byte(emotes[i].ID), []byte(emotes[j].ID)) == -1
	})
}
