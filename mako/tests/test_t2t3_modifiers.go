package tests

import (
	"context"
	"testing"
	"time"

	makotwirp "code.justin.tv/commerce/mako/twirp"
	"github.com/golang/protobuf/ptypes/timestamp"
	"github.com/stretchr/testify/require"
)

const (
	testAccount = "484681183" // qa_codyma_partner

	t1Product = "300944357"
	t2Product = "300944358"
	t3Product = "300944359"

	t1EmoteGroup = "300944355"
	t2EmoteGroup = "300944356"
	t3EmoteGroup = "300944357"

	t1EmoteCode = "qacodymaDog"
	t2EmoteCode = "qacodymaDog2"
	t3EmoteCode = "qacodymaDog3"

	t2ModifierGroupID = "emote-mod-group.1d6ZmdzSw74p7Xfw05izj0Oslff"
	t3ModifierGroupID = "emote-mod-group.1d6ZmfILfamiim4NAfAs5uD6ybt"
)

var (
	t2EmoteGroups = []string{}
	t3EmoteGroups = []string{}

	t2Modifiers = []makotwirp.Modification{}
	t3Modifiers = []makotwirp.Modification{}

	modificationToString = map[makotwirp.Modification]string{
		makotwirp.Modification_HorizontalFlip: "HF",
		makotwirp.Modification_Thinking:       "TK",
		makotwirp.Modification_Sunglasses:     "SG",
		makotwirp.Modification_BlackWhite:     "BW",
		makotwirp.Modification_Squished:       "SQ",
	}
)

func TestT2T3Modifiers(t *testing.T, makeClient MakeClient) {
	tests := []struct {
		scenario string
		test     func(ctx context.Context, t *testing.T, mako makotwirp.Mako)
	}{
		{
			scenario: "correctly returns all t2 modified emotes",
			test: func(ctx context.Context, t *testing.T, mako makotwirp.Mako) {
				ownerID := makeID()

				t.Logf("ownerID=%s", ownerID)

				entitlements := []*makotwirp.EmoteGroupEntitlement{}
				for _, emoteGroup := range t2EmoteGroups {
					entitlements = append(entitlements, &makotwirp.EmoteGroupEntitlement{
						OwnerId:  ownerID,
						ItemId:   testAccount,
						GroupId:  emoteGroup,
						Group:    makotwirp.EmoteGroup_Subscriptions,
						OriginId: makeID(),
						Start: &timestamp.Timestamp{
							Seconds: time.Now().Add(-5 * time.Minute).UTC().Unix(),
						},
						End: &makotwirp.InfiniteTime{
							IsInfinite: true,
						},
					})
				}

				_, err := mako.CreateEmoteGroupEntitlements(ctx, &makotwirp.CreateEmoteGroupEntitlementsRequest{
					Entitlements: entitlements,
				})

				require.Nil(t, err)

				time.Sleep(1 * time.Second)

				codes := []string{}
				for _, code := range []string{t1EmoteCode, t2EmoteCode} {
					for _, modifier := range t2Modifiers {
						codes = append(codes, code+"_"+modificationToString[modifier])
					}
				}

				resp, err := mako.GetEmotesByCodes(ctx, &makotwirp.GetEmotesByCodesRequest{
					OwnerId: ownerID,
					Codes:   codes,
				})

				require.Nil(t, err)

				findEmoteCodes(t, resp.Emoticons, codes...)

				_, err = mako.DeleteEmoteGroupEntitlements(ctx, &makotwirp.DeleteEmoteGroupEntitlementsRequest{
					Entitlements: entitlements,
				})

				require.Nil(t, err)
			},
		},
		{
			scenario: "correctly returns all t3 modified emotes",
			test: func(ctx context.Context, t *testing.T, mako makotwirp.Mako) {
				ownerID := makeID()

				t.Logf("ownerID=%s", ownerID)

				entitlements := []*makotwirp.EmoteGroupEntitlement{}
				for _, emoteGroup := range t3EmoteGroups {
					entitlements = append(entitlements, &makotwirp.EmoteGroupEntitlement{
						OwnerId:  ownerID,
						ItemId:   testAccount,
						GroupId:  emoteGroup,
						Group:    makotwirp.EmoteGroup_Subscriptions,
						OriginId: makeID(),
						Start: &timestamp.Timestamp{
							Seconds: time.Now().Add(-5 * time.Minute).UTC().Unix(),
						},
						End: &makotwirp.InfiniteTime{
							IsInfinite: true,
						},
					})
				}

				_, err := mako.CreateEmoteGroupEntitlements(ctx, &makotwirp.CreateEmoteGroupEntitlementsRequest{
					Entitlements: entitlements,
				})

				require.Nil(t, err)

				time.Sleep(1 * time.Second)

				codes := []string{}
				for _, code := range []string{t1EmoteCode, t2EmoteCode} {
					for _, modifier := range t3Modifiers {
						codes = append(codes, code+"_"+modificationToString[modifier])
					}
				}

				resp, err := mako.GetEmotesByCodes(ctx, &makotwirp.GetEmotesByCodesRequest{
					OwnerId: ownerID,
					Codes:   codes,
				})

				require.Nil(t, err)

				findEmoteCodes(t, resp.Emoticons, codes...)

				_, err = mako.DeleteEmoteGroupEntitlements(ctx, &makotwirp.DeleteEmoteGroupEntitlementsRequest{
					Entitlements: entitlements,
				})

				require.Nil(t, err)
			},
		},
	}

	mako, teardown, err := makeClient(context.Background())
	if err != nil {
		t.Fatal(err)
	}

	defer teardown()

	resp, err := mako.GetEmoteModifierGroups(context.Background(), &makotwirp.GetEmoteModifierGroupsRequest{
		ModifierGroupIds: []string{t2ModifierGroupID, t3ModifierGroupID},
	})

	require.Nil(t, err)
	require.Equal(t, 2, len(resp.Groups))

	for _, group := range resp.Groups {
		if len(group.SourceEmoteGroupIds) == 2 {
			t2EmoteGroups = group.SourceEmoteGroupIds
			t2Modifiers = group.Modifiers
		} else if len(group.SourceEmoteGroupIds) == 3 {
			t3EmoteGroups = group.SourceEmoteGroupIds
			t3Modifiers = group.Modifiers
		}
	}

	require.NotEmpty(t, t2EmoteGroups)
	require.NotEmpty(t, t3EmoteGroups)
	require.NotEmpty(t, t2Modifiers)
	require.NotEmpty(t, t3Modifiers)

	for _, test := range tests {
		test := test

		t.Run(test.scenario, func(t *testing.T) {
			ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
			defer cancel()

			test.test(ctx, t, mako)
		})
	}
}
