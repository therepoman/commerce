package tests

import (
	"context"
	"testing"
	"time"

	mako "code.justin.tv/commerce/mako/internal"
	mkd "code.justin.tv/commerce/mako/twirp/dashboard"
	"github.com/segmentio/ksuid"
	"github.com/stretchr/testify/require"
)

// MakeEmoteDB is used to initialize an `EmoteGroupDB` for the test suite. Implementations can call the test suite and provide
// an implementation with a way to initialize seed data and provide a teardown function.
type MakeEmoteGroupDB = func(ctx context.Context, emoteGroups ...mako.EmoteGroup) (emoteGroupDB mako.EmoteGroupDB, teardown func(), err error)

// makeEmoteGroupFn is a function to initialize a test EmoteGroup with dummy data with the ability
// for the lambda to customize fields further.
func makeEmoteGroupFn(f func(*mako.EmoteGroup)) mako.EmoteGroup {
	emoteGroup := makeEmoteGroup()
	f(&emoteGroup)
	return emoteGroup
}

// makeEmoteGroup constructs a new emoteGroup with random seed data.
func makeEmoteGroup() mako.EmoteGroup {
	return mako.EmoteGroup{
		GroupID:   ksuid.New().String(),
		OwnerID:   ksuid.New().String(),
		Origin:    mako.EmoteGroupOrigin(mkd.EmoteOrigin_Follower.String()),
		GroupType: mako.EmoteGroupStatic,
		CreatedAt: time.Now(),
	}
}

// TestEmoteGroupDB is a **test suite** that encapsulates all the behaviour of what an EmoteGroup database would have. The test suite
// accepts a `MakeEmoteGroupDB` lambda that returns an implementation of the `EmoteGroupDB` interface, accepting test data to initialize the
// implementation.
//
// This means that the test suite is agnostic to a particular implementation. It only knows about a generic interface. This makes
// it possible to re-use the same test suite and a different set of inputs.
func TestEmoteGroupDB(t *testing.T, makeDB MakeEmoteGroupDB) {
	t.Run("CreateEmoteGroup", func(t *testing.T) {
		TestCreateEmoteGroup(t, makeDB)
	})

	t.Run("GetEmoteGroups", func(t *testing.T) {
		TestGetEmoteGroups(t, makeDB)
	})

	t.Run("GetFollowerEmoteGroup", func(t *testing.T) {
		TestGetFollowerEmoteGroup(t, makeDB)
	})

	t.Run("DeleteEmoteGroup", func(t *testing.T) {
		TestDeleteEmoteGroup(t, makeDB)
	})
}

func TestCreateEmoteGroup(t *testing.T, makeDB MakeEmoteGroupDB) {
	tests := []struct {
		scenario    string
		emoteGroups []mako.EmoteGroup
		test        func(ctx context.Context, t *testing.T, db mako.EmoteGroupDB, emoteGroups ...mako.EmoteGroup)
	}{
		{
			scenario:    "should get no conditional check failure when an emote group is created by default",
			emoteGroups: []mako.EmoteGroup{},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteGroupDB, emoteGroups ...mako.EmoteGroup) {
				dbEmoteGroup, conditionalCheckFailed, err := db.CreateEmoteGroup(ctx, makeEmoteGroup())
				require.Nil(t, err)
				require.False(t, conditionalCheckFailed)
				require.NotEmpty(t, dbEmoteGroup.GroupID)
			},
		},
		{
			scenario: "should get a conditional check failure when an emote group with an existing groupID is created",
			emoteGroups: []mako.EmoteGroup{
				makeEmoteGroup(),
			},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteGroupDB, emoteGroups ...mako.EmoteGroup) {
				// Ensure that the group was created in the emote_groups table
				emoteGroupsLookup, err := db.GetEmoteGroups(ctx, emoteGroups[0].GroupID)
				require.Nil(t, err)
				require.NotNil(t, emoteGroupsLookup)
				require.Equal(t, emoteGroups[0].GroupID, emoteGroupsLookup[0].GroupID)

				// Create the same group as we just found
				dbEmoteGroup, conditionalCheckFailed, err2 := db.CreateEmoteGroup(ctx, emoteGroupsLookup[0])
				require.Nil(t, err2)
				require.True(t, conditionalCheckFailed)
				require.Empty(t, dbEmoteGroup.GroupID)
			},
		},
		{
			scenario: "should return a static emote group if one is created with no emote group type",
			test: func(ctx context.Context, t *testing.T, db mako.EmoteGroupDB, emoteGroups ...mako.EmoteGroup) {
				// Create the same group as we just found
				dbEmoteGroup, conditionalCheckFailed, err := db.CreateEmoteGroup(ctx, makeEmoteGroupFn(func(e *mako.EmoteGroup) {
					e.GroupType = ""
				}))
				require.Nil(t, err)
				require.False(t, conditionalCheckFailed)
				require.NotEmpty(t, dbEmoteGroup.GroupID)

				emoteGroupsLookup, err2 := db.GetEmoteGroups(ctx, dbEmoteGroup.GroupID)
				require.Nil(t, err2)
				require.NotEmpty(t, emoteGroupsLookup)
				require.Equal(t, mako.EmoteGroupStatic, emoteGroupsLookup[0].GroupType)
			},
		},
		{
			scenario: "should return an animated emote group if one is created with an animated emote group type",
			test: func(ctx context.Context, t *testing.T, db mako.EmoteGroupDB, emoteGroups ...mako.EmoteGroup) {
				// Create the same group as we just found
				dbEmoteGroup, conditionalCheckFailed, err := db.CreateEmoteGroup(ctx, makeEmoteGroupFn(func(e *mako.EmoteGroup) {
					e.GroupType = mako.EmoteGroupAnimated
				}))
				require.Nil(t, err)
				require.False(t, conditionalCheckFailed)
				require.NotEmpty(t, dbEmoteGroup.GroupID)

				emoteGroupsLookup, err2 := db.GetEmoteGroups(ctx, dbEmoteGroup.GroupID)
				require.Nil(t, err2)
				require.NotEmpty(t, emoteGroupsLookup)
				require.Equal(t, mako.EmoteGroupAnimated, emoteGroupsLookup[0].GroupType)
			},
		},
	}

	for _, test := range tests {
		test := test

		t.Run(test.scenario, func(t *testing.T) {
			t.Parallel()

			// Loading into local dynamo can sometimes take a while when many tests are all vying for the same resource
			// at the same time, so we provide a longer context timeout for loading into the DB and then a shorter one
			// for actually running the test.
			ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)

			db, teardown, err := makeDB(ctx, test.emoteGroups...)
			if err != nil {
				t.Fatal(err)
			}

			cancel()
			ctx, cancel = context.WithTimeout(context.Background(), 5*time.Second)

			test.test(ctx, t, db, test.emoteGroups...)

			cancel()
			teardown()
		})
	}
}

func TestGetEmoteGroups(t *testing.T, makeDB MakeEmoteGroupDB) {
	tests := []struct {
		scenario    string
		emoteGroups []mako.EmoteGroup
		test        func(context.Context, *testing.T, mako.EmoteGroupDB, ...mako.EmoteGroup)
	}{
		{
			scenario: "should return nothing when getting by ids if the db is empty",
			test: func(ctx context.Context, t *testing.T, db mako.EmoteGroupDB, emoteGroups ...mako.EmoteGroup) {
				dbEmoteGroups, err := db.GetEmoteGroups(ctx, "anyGroupID")
				require.Nil(t, err)
				require.Empty(t, dbEmoteGroups)
			},
		},
		{
			scenario: "should create an emoteGroup and successfully read it by id",
			emoteGroups: []mako.EmoteGroup{
				makeEmoteGroup(),
			},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteGroupDB, emoteGroups ...mako.EmoteGroup) {
				dbEmoteGroups, err := db.GetEmoteGroups(ctx, emoteGroups[0].GroupID)
				require.Nil(t, err)
				require.Equal(t, len(emoteGroups), len(dbEmoteGroups))
				RequireEmoteGroupSliceEquality(t, emoteGroups, dbEmoteGroups)
			},
		},
		{
			scenario: "should create multiple emoteGroups and successfully read them by ids",
			emoteGroups: []mako.EmoteGroup{
				makeEmoteGroup(),
				makeEmoteGroup(),
			},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteGroupDB, emoteGroups ...mako.EmoteGroup) {
				dbEmoteGroups, err := db.GetEmoteGroups(ctx, emoteGroups[0].GroupID, emoteGroups[1].GroupID)
				require.Nil(t, err)
				require.Equal(t, len(emoteGroups), len(dbEmoteGroups))
				RequireEmoteGroupSliceEquality(t, emoteGroups, dbEmoteGroups)
			},
		},
		{
			scenario: "if there is no EmoteGroupType stored in the db, getting the emote group should return a static type",
			emoteGroups: []mako.EmoteGroup{
				makeEmoteGroupFn(func(e *mako.EmoteGroup) {
					e.GroupType = ""
				}),
			},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteGroupDB, emoteGroups ...mako.EmoteGroup) {
				dbEmoteGroups, err := db.GetEmoteGroups(ctx, emoteGroups[0].GroupID)
				require.Nil(t, err)
				require.Equal(t, len(emoteGroups), len(dbEmoteGroups))
				require.NotEqual(t, emoteGroups[0].GroupType, dbEmoteGroups[0].GroupType)
				require.Equal(t, mako.EmoteGroupStatic, dbEmoteGroups[0].GroupType)
			},
		},
		{
			scenario: "if the EmoteGroupType is static, getting the emote group should return a static type",
			emoteGroups: []mako.EmoteGroup{
				makeEmoteGroupFn(func(e *mako.EmoteGroup) {
					e.GroupType = mako.EmoteGroupStatic
				}),
			},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteGroupDB, emoteGroups ...mako.EmoteGroup) {
				dbEmoteGroups, err := db.GetEmoteGroups(ctx, emoteGroups[0].GroupID)
				require.Nil(t, err)
				require.Equal(t, len(emoteGroups), len(dbEmoteGroups))
				require.Equal(t, emoteGroups[0].GroupType, dbEmoteGroups[0].GroupType)
				require.Equal(t, mako.EmoteGroupStatic, dbEmoteGroups[0].GroupType)
			},
		},
		{
			scenario: "if the EmoteGroupType is animated, getting the emote group should return an animated type",
			emoteGroups: []mako.EmoteGroup{
				makeEmoteGroupFn(func(e *mako.EmoteGroup) {
					e.GroupType = mako.EmoteGroupAnimated
				}),
			},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteGroupDB, emoteGroups ...mako.EmoteGroup) {
				dbEmoteGroups, err := db.GetEmoteGroups(ctx, emoteGroups[0].GroupID)
				require.Nil(t, err)
				require.Equal(t, len(emoteGroups), len(dbEmoteGroups))
				require.Equal(t, emoteGroups[0].GroupType, dbEmoteGroups[0].GroupType)
				require.Equal(t, mako.EmoteGroupAnimated, dbEmoteGroups[0].GroupType)
			},
		},
	}

	for _, test := range tests {
		test := test

		t.Run(test.scenario, func(t *testing.T) {
			t.Parallel()

			// Loading into local dynamo can sometimes take a while when many tests are all vying for the same resource
			// at the same time, so we provide a longer context timeout for loading into the DB and then a shorter one
			// for actually running the test.
			ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)

			db, teardown, err := makeDB(ctx, test.emoteGroups...)
			if err != nil {
				t.Fatal(err)
			}

			cancel()
			ctx, cancel = context.WithTimeout(context.Background(), 5*time.Second)

			test.test(ctx, t, db, test.emoteGroups...)

			cancel()
			teardown()
		})
	}
}

func TestGetFollowerEmoteGroup(t *testing.T, makeDB MakeEmoteGroupDB) {
	tests := []struct {
		scenario    string
		emoteGroups []mako.EmoteGroup
		test        func(context.Context, *testing.T, mako.EmoteGroupDB, ...mako.EmoteGroup)
	}{
		{
			scenario:    "should return nothing when getting follower emote group if the db is empty",
			emoteGroups: []mako.EmoteGroup{},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteGroupDB, emoteGroups ...mako.EmoteGroup) {
				dbEmoteGroup, err := db.GetFollowerEmoteGroup(ctx, "anyChannelID")
				require.Nil(t, err) //Not found is returned as nil, nil
				require.Nil(t, dbEmoteGroup)
			},
		},
		{
			scenario: "should create a follower emote group and then read it",
			emoteGroups: []mako.EmoteGroup{
				makeEmoteGroupFn(func(eg *mako.EmoteGroup) {
					eg.Origin = mako.EmoteGroupOriginFollower
				}),
			},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteGroupDB, emoteGroups ...mako.EmoteGroup) {
				dbEmoteGroup, err := db.GetFollowerEmoteGroup(ctx, emoteGroups[0].OwnerID)
				require.Nil(t, err)
				require.NotNil(t, dbEmoteGroup)
				RequireEmoteGroupEquality(t, emoteGroups[0], *dbEmoteGroup)
			},
		},
	}

	for _, test := range tests {
		test := test

		t.Run(test.scenario, func(t *testing.T) {
			t.Parallel()

			// Loading into local dynamo can sometimes take a while when many tests are all vying for the same resource
			// at the same time, so we provide a longer context timeout for loading into the DB and then a shorter one
			// for actually running the test.
			ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)

			db, teardown, err := makeDB(ctx, test.emoteGroups...)
			if err != nil {
				t.Fatal(err)
			}

			cancel()
			ctx, cancel = context.WithTimeout(context.Background(), 5*time.Second)

			test.test(ctx, t, db, test.emoteGroups...)

			cancel()
			teardown()
		})
	}
}

func TestDeleteEmoteGroup(t *testing.T, makeDB MakeEmoteGroupDB) {
	tests := []struct {
		scenario    string
		emoteGroups []mako.EmoteGroup
		test        func(context.Context, *testing.T, mako.EmoteGroupDB, ...mako.EmoteGroup)
	}{
		{
			scenario: "should delete the emote group when there is one",
			emoteGroups: []mako.EmoteGroup{
				makeEmoteGroupFn(func(eg *mako.EmoteGroup) {
					eg.GroupID = ksuid.New().String()
				}),
			},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteGroupDB, emoteGroups ...mako.EmoteGroup) {
				err := db.DeleteEmoteGroup(ctx, emoteGroups[0].GroupID)
				require.Nil(t, err)
			},
		},
		{
			scenario:    "should successfully return if there was no emote group found",
			emoteGroups: []mako.EmoteGroup{},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteGroupDB, emoteGroups ...mako.EmoteGroup) {
				err := db.DeleteEmoteGroup(ctx, "anyGroupID")
				require.Nil(t, err)
			},
		},
	}

	for _, test := range tests {
		test := test

		t.Run(test.scenario, func(t *testing.T) {
			t.Parallel()

			// Loading into local dynamo can sometimes take a while when many tests are all vying for the same resource
			// at the same time, so we provide a longer context timeout for loading into the DB and then a shorter one
			// for actually running the test.
			ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)

			db, teardown, err := makeDB(ctx, test.emoteGroups...)
			if err != nil {
				t.Fatal(err)
			}

			cancel()
			ctx, cancel = context.WithTimeout(context.Background(), 5*time.Second)

			test.test(ctx, t, db, test.emoteGroups...)

			cancel()
			teardown()
		})
	}
}
