package tests

import (
	"bytes"
	"sort"
	"testing"
	"time"

	mako "code.justin.tv/commerce/mako/internal"
	"github.com/stretchr/testify/require"
)

func RequireEmoteGroupSliceEquality(t *testing.T, a, b []mako.EmoteGroup) {
	t.Helper()

	require.Equal(t, len(a), len(b))

	sort.Slice(a, func(i, j int) bool {
		return bytes.Compare([]byte(a[i].GroupID), []byte(a[j].GroupID)) == -1
	})
	sort.Slice(b, func(i, j int) bool {
		return bytes.Compare([]byte(b[i].GroupID), []byte(b[j].GroupID)) == -1
	})

	for i, emoteGroup := range a {
		RequireEmoteGroupEquality(t, emoteGroup, b[i])
	}
}

func RequireEmoteGroupEquality(t *testing.T, a, b mako.EmoteGroup) {
	t.Helper()

	require.Equal(t, a.GroupID, b.GroupID)
	require.Equal(t, a.GroupType, b.GroupType)
	require.Equal(t, a.Origin, b.Origin)
	require.Equal(t, a.OwnerID, b.OwnerID)
	require.Equal(t, a.CreatedAt.Format(time.RFC3339), b.CreatedAt.Format(time.RFC3339))
}
