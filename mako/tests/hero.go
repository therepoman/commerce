package tests

import (
	"context"
	"net/http"
	"testing"

	makotwirp "code.justin.tv/commerce/mako/twirp"
	"github.com/segmentio/ksuid"
	"github.com/stretchr/testify/require"
)

type MakeClient func(ctx context.Context) (makotwirp.Mako, func(), error)

func makeClient(url string) makotwirp.Mako {
	if url == "" {
		url = "https://main.us-west-2.prod.mako.twitch.a2z.com"
	}

	return makotwirp.NewMakoProtobufClient(url, http.DefaultClient)
}

func testChat(ctx context.Context, t *testing.T, client makotwirp.Mako, ownerID string, codes ...string) {
	resp, err := client.GetEmotesByCodes(ctx, &makotwirp.GetEmotesByCodesRequest{
		OwnerId: ownerID,
		Codes:   codes,
	})

	require.Nil(t, err)

	findEmoteCodes(t, resp.Emoticons, codes...)
}

func makeID() string {
	return "hero-" + ksuid.New().String()
}

func findEmoteSet(t *testing.T, sets []*makotwirp.EmoteSet, ids ...string) {
	t.Helper()

	for _, emoteSetID := range ids {
		var found bool
		for _, emoteSet := range sets {
			if emoteSet.Id == emoteSetID {
				found = true
			}
		}

		if !found {
			t.Fatalf("failed to find emote set '%s'", emoteSetID)
		}
	}
}

func findEmoteCodesInSet(t *testing.T, emoteSets []*makotwirp.EmoteSet, emoteSetID string, codes ...string) {
	t.Helper()

	var foundSet bool
	for _, emoteSet := range emoteSets {
		if emoteSet.Id == emoteSetID {
			foundSet = true

			for _, code := range codes {
				var foundCode bool
				for _, emote := range emoteSet.Emotes {
					if emote.Pattern == code {
						foundCode = true
					}
				}

				if !foundCode {
					t.Fatalf("expected to find emote code '%s' in set '%s'", code, emoteSetID)
				}
			}
		}
	}

	if !foundSet {
		t.Fatalf("expected to find emote set '%s'", emoteSetID)
	}
}

func findEmoteCodes(t *testing.T, emotes []*makotwirp.Emoticon, codes ...string) {
	t.Helper()

	for _, code := range codes {
		var found bool
		for _, emote := range emotes {
			if emote.Code == code {
				found = true
			}
		}

		if !found {
			t.Fatalf("failed to find emote code '%s'", code)
		}
	}
}
