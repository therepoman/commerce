package tests

import (
	"bytes"
	mako_dashboard "code.justin.tv/commerce/mako/twirp/dashboard"
	"context"
	"fmt"
	"sort"
	"testing"
	"time"

	mako "code.justin.tv/commerce/mako/internal"
	mako_client "code.justin.tv/commerce/mako/twirp"
	"github.com/segmentio/ksuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

// These are test names that are skipped by certain runs of this test suite
const (
	ReadByCodesNegativeCacheTestName = "should not call the underlying store for non-emotes: ReadByCodes()"
	DefaultGroupID                   = "groupId"
	DefaultState                     = mako.Active
	DefaultEmotesGroup               = mako_dashboard.EmoteOrigin_None
	DefaultOrder                     = 0
)

// MakeEmoteDB is used to initialize an `EmoteDB` for the test suite. Implementations can call the test suite and provide
// an implementation with a way to initialize seed data and provide a teardown function.
type MakeEmoteDB = func(ctx context.Context, emotes ...mako.Emote) (mako.EmoteDB, func(), error)

type CachedEmoteDBConfig struct {
	Base  MakeEmoteDB
	Cache func(base mako.EmoteDB) MakeEmoteDB
	// map of test names that should be skipped for a given run of this test suite
	TestsToSkip map[string]bool
}

// makeEmoteFn is a function to initialize a test Emote with dummy data with the ability
// for the lambda to customize fields further.
func makeEmoteFn(f func(*mako.Emote)) mako.Emote {
	emote := makeEmote()
	f(&emote)
	return emote
}

// makeEmoteBatchFn is a function to initialize a batch of test Emotes with dummy data with the ability
// for the lambda to customize fields further.
func makeEmoteBatchFn(f func(*mako.Emote), size int) []mako.Emote {
	var result []mako.Emote

	for i := 0; i < size; i++ {
		emote := makeEmoteFn(f)
		result = append(result, emote)
	}
	return result
}

// makeEmote constructs a new emote with random seed data.
func makeEmote() mako.Emote {
	return mako.Emote{
		ID:          ksuid.New().String(),
		ProductID:   ksuid.New().String(),
		OwnerID:     ksuid.New().String(),
		Code:        mako.RandomEmote(),
		UpdatedAt:   time.Now(),
		CreatedAt:   time.Now(),
		Suffix:      "suffix",
		Prefix:      "abc",
		Domain:      "emotes",
		GroupID:     DefaultGroupID,
		State:       DefaultState,
		Order:       DefaultOrder,
		EmotesGroup: DefaultEmotesGroup.String(),
		AssetType:   mako.StaticAssetType,
	}
}

func makeDefaultEmote() mako.Emote {
	emote := makeEmote()
	emote.ImageSource = mako.DefaultLibraryImage
	return emote
}

func makeEmoteByOwner(owner string) mako.Emote {
	return mako.Emote{
		ID:        ksuid.New().String(),
		ProductID: ksuid.New().String(),
		OwnerID:   owner,
		Code:      ksuid.New().String(),
		UpdatedAt: time.Now(),
		CreatedAt: time.Now(),
		Suffix:    "suffix",
		Prefix:    "abc",
		Domain:    "emotes",
		GroupID:   "groupId",
		State:     "active",
	}
}

// TestEmoteDB is a **test suite** that encapsulates all the behaviour of what an emote database would have. The test suite
// accepts a `MakeEmoteDB` lambda that returns an implementation of the `EmoteDB` interface, accepting test data to initialize the
// implementation.
//
// This means that the test suite is agnostic to a particular implementation. It only knows about a generic interface. This makes
// it possible to re-use the same test suite and a different set of inputs.
func TestEmoteDB(t *testing.T, makeDB MakeEmoteDB) {
	t.Run("ReadByIDs", func(t *testing.T) {
		t.Parallel()
		TestReadEmotesByIDs(t, makeDB)
	})

	t.Run("ReadByCodes", func(t *testing.T) {
		t.Parallel()
		TestReadEmotesByCodes(t, makeDB)
	})

	t.Run("ReadByGroupIDs", func(t *testing.T) {
		t.Parallel()
		TestReadEmotesByGroupIDs(t, makeDB)
	})

	t.Run("WriteEmotes", func(t *testing.T) {
		t.Parallel()
		TestWriteEmotes(t, makeDB)
	})

	t.Run("DeleteEmote", func(t *testing.T) {
		t.Parallel()
		TestDeleteEmote(t, makeDB)
	})

	t.Run("UpdateEmoteState", func(t *testing.T) {
		t.Parallel()
		TestUpdateEmoteState(t, makeDB)
	})

	t.Run("UpdateEmoteCode", func(t *testing.T) {
		t.Parallel()
		TestUpdateEmoteCode(t, makeDB)
	})

	t.Run("UpdateEmotePrefix", func(t *testing.T) {
		t.Parallel()
		TestUpdateEmotePrefix(t, makeDB)
	})

	t.Run("UpdateEmote", func(t *testing.T) {
		t.Parallel()
		TestUpdateEmote(t, makeDB)
	})

	t.Run("UpdateEmotes", func(t *testing.T) {
		t.Parallel()
		TestUpdateEmotes(t, makeDB)
	})

	t.Run("EmoteQuery", func(t *testing.T) {
		t.Parallel()
		TestEmoteQuery(t, makeDB)
	})
}

func TestReadEmotesByIDs(t *testing.T, makeDB MakeEmoteDB) {
	tests := []struct {
		scenario string
		emotes   []mako.Emote
		test     func(ctx context.Context, t *testing.T, db mako.EmoteDB, emotes ...mako.Emote)
	}{
		{
			scenario: "should create an emoticon and read it successfully by its id",
			emotes: []mako.Emote{
				makeEmote(),
			},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteDB, emotes ...mako.Emote) {
				dbEmotes, err := db.ReadByIDs(ctx, emotes[0].ID)
				assert.Nil(t, err)
				assert.Equal(t, len(emotes), len(dbEmotes))
				for _, emote := range emotes {
					for _, dbEmote := range dbEmotes {
						RequireEmoteEquality(t, emote, dbEmote)
					}
				}
			},
		},

		{
			scenario: "read a number of emotes greater than the dynamo batch limit by their ids",
			emotes:   makeEmoteBatchFn(func(e *mako.Emote) {}, 105),
			test: func(ctx context.Context, t *testing.T, db mako.EmoteDB, emotes ...mako.Emote) {
				var ids []string

				for _, emote := range emotes {
					ids = append(ids, emote.ID)
				}

				dbEmotes, err := db.ReadByIDs(ctx, ids...)
				assert.Nil(t, err)
				assert.Equal(t, len(emotes), len(dbEmotes))

				RequireEmoteSliceEquality(t, emotes, dbEmotes)
			},
		},

		{
			scenario: "with a mix of IDs that exist and not, it returns only the emotes which exist",
			emotes:   makeEmoteBatchFn(func(e *mako.Emote) {}, 105),
			test: func(ctx context.Context, t *testing.T, db mako.EmoteDB, emotes ...mako.Emote) {
				requestedRealIDs := 10
				var ids []string
				var expectedEmotes []mako.Emote

				for i := 0; i < requestedRealIDs; i++ {
					ids = append(ids, emotes[i].ID, fmt.Sprintf("fakeID%d", i))
					expectedEmotes = append(expectedEmotes, emotes[i])
				}

				dbEmotes, err := db.ReadByIDs(ctx, ids...)
				assert.Nil(t, err)
				assert.Equal(t, requestedRealIDs, len(dbEmotes))

				RequireEmoteSliceEquality(t, expectedEmotes, dbEmotes)
			},
		},
	}

	for _, test := range tests {
		test := test

		t.Run(test.scenario, func(t *testing.T) {
			t.Parallel()

			ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
			defer cancel()

			db, teardown, err := makeDB(ctx, test.emotes...)
			if err != nil {
				t.Fatal(err)
			}

			defer teardown()

			test.test(ctx, t, db, test.emotes...)
		})
	}
}

func TestReadEmotesByCodes(t *testing.T, makeDB MakeEmoteDB) {
	tests := []struct {
		scenario string
		emotes   []mako.Emote
		test     func(ctx context.Context, t *testing.T, db mako.EmoteDB, emotes ...mako.Emote)
	}{
		{
			scenario: "return all emotes for a given code",
			emotes: []mako.Emote{
				makeEmoteFn(func(e *mako.Emote) {
					e.Code = "Kappa"
				}),
				makeEmoteFn(func(e *mako.Emote) {
					e.Code = "Kappa"
				}),
				makeEmoteFn(func(e *mako.Emote) {
					e.Code = "Kappa"
				}),
			},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteDB, emotes ...mako.Emote) {
				dbEmotes, err := db.ReadByCodes(ctx, "Kappa")
				assert.Nil(t, err)
				require.Equal(t, len(emotes), len(dbEmotes))

				RequireEmoteSliceEquality(t, emotes, dbEmotes)
			},
		},

		{
			scenario: "should create a set of emoticons and read them via their codes",
			emotes: []mako.Emote{
				makeEmote(),
				makeEmote(),
				makeEmote(),
				makeEmote(),
			},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteDB, emotes ...mako.Emote) {
				codes := []string{}

				for _, emote := range emotes {
					codes = append(codes, emote.Code)
				}

				dbEmotes, err := db.ReadByCodes(ctx, codes...)
				assert.Nil(t, err)
				assert.Equal(t, len(emotes), len(dbEmotes))

				RequireEmoteSliceEquality(t, emotes, dbEmotes)
			},
		},
	}

	for _, test := range tests {
		test := test

		t.Run(test.scenario, func(t *testing.T) {
			t.Parallel()

			ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
			defer cancel()

			db, teardown, err := makeDB(ctx, test.emotes...)
			if err != nil {
				t.Fatal(err)
			}

			defer teardown()

			test.test(ctx, t, db, test.emotes...)
		})
	}
}

func TestReadEmotesByGroupIDs(t *testing.T, makeDB MakeEmoteDB) {
	tests := []struct {
		scenario string
		emotes   []mako.Emote
		test     func(ctx context.Context, t *testing.T, db mako.EmoteDB, emotes ...mako.Emote)
	}{
		{
			scenario: "should return the group asked",
			emotes: []mako.Emote{
				makeEmoteFn(func(emote *mako.Emote) {
					emote.GroupID = "5"
					emote.ID = "0"
				}),
				makeEmoteFn(func(emote *mako.Emote) {
					emote.GroupID = "1"
					emote.ID = "1"
				}),
				makeEmoteFn(func(emote *mako.Emote) {
					emote.GroupID = "1"
					emote.ID = "2"
				}),
				makeEmoteFn(func(emote *mako.Emote) {
					emote.GroupID = "2"
					emote.ID = "3"
				}),
				makeEmoteFn(func(emote *mako.Emote) {
					emote.GroupID = "2"
					emote.ID = "4"
				}),
			},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteDB, emotes ...mako.Emote) {
				dbEmotes, err := db.ReadByGroupIDs(ctx, "5")
				assert.Nil(t, err)
				require.Equal(t, 1, len(dbEmotes))

				dbEmotes, err = db.ReadByGroupIDs(ctx, "1", "2")
				assert.Nil(t, err)
				require.Equal(t, 4, len(dbEmotes))

				// Ordering is not guaranteed by any of the APIs.
				sort.Slice(dbEmotes, func(i, j int) bool {
					return bytes.Compare([]byte(dbEmotes[i].ID), []byte(dbEmotes[j].ID)) == -1
				})

				assert.Equal(t, "1", dbEmotes[0].GroupID)
				assert.Equal(t, "1", dbEmotes[0].ID)

				assert.Equal(t, "1", dbEmotes[1].GroupID)
				assert.Equal(t, "2", dbEmotes[1].ID)

				assert.Equal(t, "2", dbEmotes[2].GroupID)
				assert.Equal(t, "3", dbEmotes[2].ID)

				assert.Equal(t, "2", dbEmotes[3].GroupID)
				assert.Equal(t, "4", dbEmotes[3].ID)

				// Read again for both groups
				dbEmotes, err = db.ReadByGroupIDs(ctx, "1", "2")
				assert.Nil(t, err)
				assert.Equal(t, 4, len(dbEmotes))

				sort.Slice(dbEmotes, func(i, j int) bool {
					return bytes.Compare([]byte(dbEmotes[i].ID), []byte(dbEmotes[j].ID)) == -1
				})

				assert.Equal(t, "1", dbEmotes[0].GroupID)
				assert.Equal(t, "1", dbEmotes[1].GroupID)
				assert.Equal(t, "2", dbEmotes[2].GroupID)
				assert.Equal(t, "2", dbEmotes[3].GroupID)
			},
		},
	}

	for _, test := range tests {
		test := test

		t.Run(test.scenario, func(t *testing.T) {
			t.Parallel()

			ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
			defer cancel()

			db, teardown, err := makeDB(ctx, test.emotes...)
			if err != nil {
				t.Fatal(err)
			}

			defer teardown()

			test.test(ctx, t, db, test.emotes...)
		})
	}
}

func TestWriteEmotes(t *testing.T, makeDB MakeEmoteDB) {
	tests := []struct {
		scenario string
		emotes   []mako.Emote
		test     func(ctx context.Context, t *testing.T, db mako.EmoteDB, emotes ...mako.Emote)
	}{
		{
			scenario: "should successfully write an emote",
			emotes:   []mako.Emote{},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteDB, emotes ...mako.Emote) {
				emote := makeEmote()

				_, err := db.WriteEmotes(ctx, emote)
				assert.Nil(t, err)

				dbEmotes, err := db.ReadByIDs(ctx, emote.ID)
				assert.Nil(t, err)
				assert.Equal(t, 1, len(dbEmotes))

				RequireEmoteEquality(t, emote, dbEmotes[0])
			},
		},
		{
			scenario: "should successfully write several emotes to a single group",
			emotes:   []mako.Emote{},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteDB, emotes ...mako.Emote) {
				testGroup := "testGroup123"
				emotesToAdd := makeEmoteBatchFn(func(e *mako.Emote) {
					e.GroupID = testGroup
				}, 10)

				_, err := db.WriteEmotes(ctx, emotesToAdd...)
				assert.Nil(t, err)

				dbEmotes, err := db.ReadByGroupIDs(ctx, testGroup)
				assert.Nil(t, err)

				RequireEmoteSliceEquality(t, emotesToAdd, dbEmotes)
			},
		},
		{
			scenario: "should successfully write a very large batch of emotes to a single group",
			emotes:   []mako.Emote{},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteDB, emotes ...mako.Emote) {
				testGroup := "testGroup123"
				emotesToAdd := makeEmoteBatchFn(func(e *mako.Emote) {
					e.GroupID = testGroup
				}, 105)

				_, err := db.WriteEmotes(ctx, emotesToAdd...)
				assert.Nil(t, err)

				dbEmotes, err := db.ReadByGroupIDs(ctx, testGroup)
				assert.Nil(t, err)

				RequireEmoteSliceEquality(t, emotesToAdd, dbEmotes)
			},
		},
		{
			scenario: "should successfully overwrite an emote",
			emotes:   []mako.Emote{makeEmote()},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteDB, emotes ...mako.Emote) {
				emoteID := emotes[0].ID
				modifiedEmote := makeEmoteFn(func(e *mako.Emote) {
					e.ID = emoteID
				})

				// Confirm that the emote exists prior to write
				dbEmotes, err := db.ReadByIDs(ctx, emoteID)
				assert.Nil(t, err)
				assert.Equal(t, 1, len(dbEmotes))
				RequireEmoteEquality(t, emotes[0], dbEmotes[0])

				// Overwrite the emote
				_, err = db.WriteEmotes(ctx, modifiedEmote)
				assert.Nil(t, err)

				// Confirm that the emote now reflects the new version
				dbEmotes, err = db.ReadByIDs(ctx, emoteID)
				assert.Nil(t, err)
				assert.Equal(t, 1, len(dbEmotes))
				RequireEmoteEquality(t, modifiedEmote, dbEmotes[0])
			},
		},
	}

	for _, test := range tests {
		test := test

		t.Run(test.scenario, func(t *testing.T) {
			t.Parallel()

			ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
			defer cancel()

			db, teardown, err := makeDB(ctx, test.emotes...)
			if err != nil {
				t.Fatal(err)
			}

			defer teardown()

			test.test(ctx, t, db, test.emotes...)
		})
	}
}

func TestDeleteEmote(t *testing.T, makeDB MakeEmoteDB) {
	tests := []struct {
		scenario string
		emotes   []mako.Emote
		test     func(ctx context.Context, t *testing.T, db mako.EmoteDB, emotes ...mako.Emote)
	}{
		{
			scenario: "should successfully delete an emoticon",
			emotes: []mako.Emote{
				makeEmote(),
			},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteDB, emotes ...mako.Emote) {
				dbEmotes, err := db.ReadByIDs(ctx, emotes[0].ID)
				assert.Nil(t, err)
				assert.Equal(t, 1, len(dbEmotes))

				_, err = db.DeleteEmote(ctx, emotes[0].ID)
				assert.Nil(t, err)

				<-time.After(2 * time.Second)

				dbEmotes, err = db.ReadByIDs(ctx, emotes[0].ID)
				assert.Nil(t, err)
				assert.Equal(t, 0, len(dbEmotes))
			},
		},
	}

	for _, test := range tests {
		test := test

		t.Run(test.scenario, func(t *testing.T) {
			t.Parallel()

			ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
			defer cancel()

			db, teardown, err := makeDB(ctx, test.emotes...)
			if err != nil {
				t.Fatal(err)
			}

			defer teardown()

			test.test(ctx, t, db, test.emotes...)
		})
	}
}

func TestUpdateEmoteState(t *testing.T, makeDB MakeEmoteDB) {
	tests := []struct {
		scenario string
		emotes   []mako.Emote
		test     func(ctx context.Context, t *testing.T, db mako.EmoteDB, emotes ...mako.Emote)
	}{
		{
			scenario: "should successfully update an emoticon's state",
			emotes: []mako.Emote{
				makeEmote(),
			},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteDB, emotes ...mako.Emote) {
				_, err := db.UpdateState(ctx, emotes[0].ID, mako.Inactive)
				assert.Nil(t, err)

				dbEmotes, err := db.ReadByIDs(ctx, emotes[0].ID)
				assert.Nil(t, err)
				require.Equal(t, 1, len(dbEmotes))
				assert.Equal(t, mako.Inactive, dbEmotes[0].State)
			},
		},
	}

	for _, test := range tests {
		test := test

		t.Run(test.scenario, func(t *testing.T) {
			t.Parallel()

			ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
			defer cancel()

			db, teardown, err := makeDB(ctx, test.emotes...)
			if err != nil {
				t.Fatal(err)
			}

			defer teardown()

			test.test(ctx, t, db, test.emotes...)
		})
	}
}

func TestUpdateEmoteCode(t *testing.T, makeDB MakeEmoteDB) {
	tests := []struct {
		scenario string
		emotes   []mako.Emote
		test     func(ctx context.Context, t *testing.T, db mako.EmoteDB, emotes ...mako.Emote)
	}{
		{
			scenario: "should successfully update an emoticon's code",
			emotes: []mako.Emote{
				makeEmote(),
			},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteDB, emotes ...mako.Emote) {
				_, err := db.UpdateCode(ctx, emotes[0].ID, "HELLO", "suffix")
				assert.Nil(t, err)

				<-time.After(2 * time.Second)

				dbEmotes, err := db.ReadByIDs(ctx, emotes[0].ID)
				assert.Nil(t, err)
				assert.Equal(t, 1, len(dbEmotes))

				for _, emote := range dbEmotes {
					assert.Equal(t, "HELLO", emote.Code)
					assert.Equal(t, "suffix", emote.Suffix)
				}
			},
		},
	}

	for _, test := range tests {
		test := test

		t.Run(test.scenario, func(t *testing.T) {
			t.Parallel()

			ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
			defer cancel()

			db, teardown, err := makeDB(ctx, test.emotes...)
			if err != nil {
				t.Fatal(err)
			}

			defer teardown()

			test.test(ctx, t, db, test.emotes...)
		})
	}
}

func TestUpdateEmotePrefix(t *testing.T, makeDB MakeEmoteDB) {
	tests := []struct {
		scenario string
		emotes   []mako.Emote
		test     func(ctx context.Context, t *testing.T, db mako.EmoteDB, emotes ...mako.Emote)
	}{
		{
			scenario: "should successfully update an emoticon's prefix",
			emotes: []mako.Emote{
				makeEmote(),
			},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteDB, emotes ...mako.Emote) {
				_, err := db.UpdatePrefix(ctx, emotes[0].ID, "prefixHELLO", "prefix")
				assert.Nil(t, err)

				<-time.After(2 * time.Second)

				dbEmotes, err := db.ReadByIDs(ctx, emotes[0].ID)
				assert.Nil(t, err)
				assert.Equal(t, 1, len(dbEmotes))

				for _, emote := range dbEmotes {
					assert.Equal(t, "prefixHELLO", emote.Code)
					assert.Equal(t, "prefix", emote.Prefix)
				}
			},
		},
	}

	for _, test := range tests {
		test := test

		t.Run(test.scenario, func(t *testing.T) {
			t.Parallel()

			ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
			defer cancel()

			db, teardown, err := makeDB(ctx, test.emotes...)
			if err != nil {
				t.Fatal(err)
			}

			defer teardown()

			test.test(ctx, t, db, test.emotes...)
		})
	}
}

func TestUpdateEmote(t *testing.T, makeDB MakeEmoteDB) {
	tests := []struct {
		scenario string
		emotes   []mako.Emote
		test     func(ctx context.Context, t *testing.T, db mako.EmoteDB, emotes ...mako.Emote)
	}{
		{
			scenario: "should successfully update all of an emoticon's specified fields",
			emotes: []mako.Emote{
				makeEmote(),
			},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteDB, emotes ...mako.Emote) {
				newGroupID := "newGroupId"
				newState := mako.Archived
				newOrder := int64(25)
				newEmotesGroup := mako_client.EmoteGroup_Archive.String()
				resp, err := db.UpdateEmote(ctx, emotes[0].ID, mako.EmoteUpdate{
					GroupID:     &newGroupID,
					State:       &newState,
					Order:       &newOrder,
					EmotesGroup: &newEmotesGroup,
				})
				assert.NoError(t, err)
				assert.Equal(t, newGroupID, resp.GroupID)
				assert.Equal(t, newState, resp.State)
				assert.Equal(t, newOrder, resp.Order)
				assert.Equal(t, newEmotesGroup, resp.EmotesGroup)

				<-time.After(2 * time.Second)

				dbEmotes, err := db.ReadByIDs(ctx, emotes[0].ID)
				assert.Nil(t, err)
				assert.Equal(t, 1, len(dbEmotes))

				for _, emote := range dbEmotes {
					assert.Equal(t, newGroupID, emote.GroupID)
					assert.Equal(t, newState, emote.State)
					assert.Equal(t, newOrder, emote.Order)
					assert.Equal(t, newEmotesGroup, emote.EmotesGroup)
				}
			},
		},
		{
			scenario: "should only update an emoticon's specified fields",
			emotes: []mako.Emote{
				makeEmote(),
			},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteDB, emotes ...mako.Emote) {
				prevGroupID := emotes[0].GroupID
				prevState := emotes[0].State
				newOrder := int64(25)
				newEmotesGroup := mako_client.EmoteGroup_Archive.String()
				resp, err := db.UpdateEmote(ctx, emotes[0].ID, mako.EmoteUpdate{
					Order:       &newOrder,
					EmotesGroup: &newEmotesGroup,
				})
				assert.NoError(t, err)
				assert.Equal(t, prevGroupID, resp.GroupID)
				assert.Equal(t, prevState, resp.State)
				assert.Equal(t, newOrder, resp.Order)
				assert.Equal(t, newEmotesGroup, resp.EmotesGroup)

				<-time.After(2 * time.Second)

				dbEmotes, err := db.ReadByIDs(ctx, emotes[0].ID)
				assert.Nil(t, err)
				assert.Equal(t, 1, len(dbEmotes))

				for _, emote := range dbEmotes {
					assert.Equal(t, prevGroupID, emote.GroupID)
					assert.Equal(t, prevState, emote.State)
					assert.Equal(t, newOrder, emote.Order)
					assert.Equal(t, newEmotesGroup, emote.EmotesGroup)
				}
			},
		},
		{
			scenario: "should fail to update an emoticon when no fields are specified",
			emotes: []mako.Emote{
				makeEmote(),
			},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteDB, emotes ...mako.Emote) {
				prevGroupID := emotes[0].GroupID
				prevState := emotes[0].State
				prevOrder := emotes[0].Order
				prevEmotesGroup := emotes[0].EmotesGroup
				resp, err := db.UpdateEmote(ctx, emotes[0].ID, mako.EmoteUpdate{})
				assert.Error(t, err)
				assert.Equal(t, mako.Emote{}, resp)

				<-time.After(2 * time.Second)

				dbEmotes, err := db.ReadByIDs(ctx, emotes[0].ID)
				assert.Nil(t, err)
				assert.Equal(t, 1, len(dbEmotes))

				for _, emote := range dbEmotes {
					assert.Equal(t, prevGroupID, emote.GroupID)
					assert.Equal(t, prevState, emote.State)
					assert.Equal(t, prevOrder, emote.Order)
					assert.Equal(t, prevEmotesGroup, emote.EmotesGroup)
				}
			},
		},
	}

	for _, test := range tests {
		test := test

		t.Run(test.scenario, func(t *testing.T) {
			t.Parallel()

			ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
			defer cancel()

			db, teardown, err := makeDB(ctx, test.emotes...)
			if err != nil {
				t.Fatal(err)
			}

			defer teardown()

			test.test(ctx, t, db, test.emotes...)
		})
	}
}

func TestUpdateEmotes(t *testing.T, makeDB MakeEmoteDB) {
	tests := []struct {
		scenario string
		emotes   []mako.Emote
		test     func(ctx context.Context, t *testing.T, db mako.EmoteDB, emotes ...mako.Emote)
	}{
		{
			scenario: "should successfully update all of an emoticon's specified fields for each emoticon",
			emotes: []mako.Emote{
				makeEmote(),
				makeEmote(),
				makeEmote(),
			},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteDB, emotes ...mako.Emote) {
				emoteUpdates := make(map[string]mako.EmoteUpdate, 0)
				for i, emote := range emotes {
					newGroupID := ksuid.New().String()
					newState := mako.Active
					newOrder := int64(i)
					newEmotesGroup := mako_client.EmoteGroup_Subscriptions.String()

					update := mako.EmoteUpdate{
						GroupID:     &newGroupID,
						State:       &newState,
						Order:       &newOrder,
						EmotesGroup: &newEmotesGroup,
					}

					emoteUpdates[emote.ID] = update
				}

				emotesToExpire, err := db.UpdateEmotes(ctx, emoteUpdates)
				assert.NoError(t, err)
				// We expect to get the existing emotes as well as the updated emotes, so 2x the length of our request
				assert.Equal(t, len(emotesToExpire), 2*len(emoteUpdates))
				for _, updatedEmote := range emotesToExpire {
					emoteUpdate := emoteUpdates[updatedEmote.ID]
					if updatedEmote.GroupID != DefaultGroupID {
						assert.Equal(t, *emoteUpdate.GroupID, updatedEmote.GroupID)
					}
					if updatedEmote.State != DefaultState {
						assert.Equal(t, *emoteUpdate.State, updatedEmote.State)
					}
					if updatedEmote.Order != DefaultOrder {
						assert.Equal(t, *emoteUpdate.Order, updatedEmote.Order)
					}
					if updatedEmote.EmotesGroup != DefaultEmotesGroup.String() {
						assert.Equal(t, *emoteUpdate.EmotesGroup, updatedEmote.EmotesGroup)
					}
				}

				<-time.After(2 * time.Second)

				var ids []string
				for _, emote := range emotes {
					ids = append(ids, emote.ID)
				}

				dbEmotes, err := db.ReadByIDs(ctx, ids...)
				assert.Nil(t, err)
				assert.Equal(t, len(emotes), len(dbEmotes))

				for _, dbEmote := range dbEmotes {
					emoteUpdate := emoteUpdates[dbEmote.ID]
					assert.Equal(t, *emoteUpdate.GroupID, dbEmote.GroupID)
					assert.Equal(t, *emoteUpdate.State, dbEmote.State)
					assert.Equal(t, *emoteUpdate.Order, dbEmote.Order)
					assert.Equal(t, *emoteUpdate.EmotesGroup, dbEmote.EmotesGroup)
				}
			},
		},
		{
			scenario: "should be usable for reordering emotes within a group",
			emotes: []mako.Emote{
				makeEmoteFn(func(e *mako.Emote) {
					e.GroupID = "sameGroupID"
					e.Order = 2
				}),
				makeEmoteFn(func(e *mako.Emote) {
					e.GroupID = "sameGroupID"
					e.Order = 0
				}),
				makeEmoteFn(func(e *mako.Emote) {
					e.GroupID = "sameGroupID"
					e.Order = 1
				}),
			},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteDB, emotes ...mako.Emote) {
				// This test requires the emotes in the setup to be in a certain order so that assigning their order to
				// be their array index changes each of their orders.
				emoteUpdates := make(map[string]mako.EmoteUpdate, 0)
				for i, emote := range emotes {
					newOrder := int64(i)
					update := mako.EmoteUpdate{
						Order: &newOrder,
					}

					emoteUpdates[emote.ID] = update
				}

				emotesToExpire, err := db.UpdateEmotes(ctx, emoteUpdates)
				assert.NoError(t, err)
				assert.Equal(t, len(emotesToExpire), 2*len(emoteUpdates))

				<-time.After(2 * time.Second)

				var ids []string
				for _, emote := range emotes {
					ids = append(ids, emote.ID)
				}

				dbEmotes, err := db.ReadByIDs(ctx, ids...)
				assert.Nil(t, err)
				assert.Equal(t, len(emotes), len(dbEmotes))

				for _, dbEmote := range dbEmotes {
					emoteUpdate := emoteUpdates[dbEmote.ID]
					assert.Equal(t, *emoteUpdate.Order, dbEmote.Order)
				}
			},
		},
	}

	for _, test := range tests {
		test := test

		t.Run(test.scenario, func(t *testing.T) {
			t.Parallel()

			ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
			defer cancel()

			db, teardown, err := makeDB(ctx, test.emotes...)
			if err != nil {
				t.Fatal(err)
			}

			defer teardown()

			test.test(ctx, t, db, test.emotes...)
		})
	}
}

func TestEmoteQuery(t *testing.T, makeDB MakeEmoteDB) {
	tests := []struct {
		scenario string
		emotes   []mako.Emote
		test     func(ctx context.Context, t *testing.T, db mako.EmoteDB, emotes ...mako.Emote)
	}{
		{
			scenario: "should create a set of emoticons and read them via their owners",
			emotes: []mako.Emote{
				makeEmoteByOwner("abc"),
				makeEmoteByOwner("abc"),
				makeEmoteByOwner("abc"),
			},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteDB, emotes ...mako.Emote) {
				dbEmotes, err := db.Query(ctx, mako.EmoteQuery{
					OwnerID: "abc",
				})

				assert.Nil(t, err)
				assert.Equal(t, len(emotes), len(dbEmotes))

				for _, dbEmote := range dbEmotes {
					for _, emote := range emotes {
						if emote.ID != dbEmote.ID {
							continue
						}

						RequireEmoteEquality(t, emote, dbEmote)
					}
				}
			},
		},

		{
			scenario: "should successfully Query() an emoticon by a code and emote group",
			emotes: []mako.Emote{
				makeEmoteFn(func(emote *mako.Emote) {
					emote.EmotesGroup = mako_client.EmoteGroup_BitsBadgeTierEmotes.String()
				}),
			},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteDB, emotes ...mako.Emote) {
				dbEmotes, err := db.Query(ctx, mako.EmoteQuery{
					Code:       emotes[0].Code,
					EmoteGroup: mako_client.EmoteGroup_BitsBadgeTierEmotes.String(),
				})

				assert.Nil(t, err)
				assert.Equal(t, 1, len(dbEmotes))

				for i, emote := range dbEmotes {
					RequireEmoteEquality(t, emotes[i], emote)
				}
			},
		},

		{
			scenario: "should successfully Query() an emoticon by a code and asset type",
			emotes: []mako.Emote{
				makeEmoteFn(func(emote *mako.Emote) {
					emote.Code = "testEmote"
					emote.AssetType = mako.StaticAssetType
				}),
				makeEmoteFn(func(emote *mako.Emote) {
					emote.Code = "testEmote"
					emote.AssetType = mako.AnimatedAssetType
				}),
			},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteDB, emotes ...mako.Emote) {
				dbEmotes, err := db.Query(ctx, mako.EmoteQuery{
					Code:      "testEmote",
					AssetType: mako.AnimatedAssetType,
				})

				assert.Nil(t, err)
				assert.Equal(t, 1, len(dbEmotes))

				RequireEmoteEquality(t, emotes[1], dbEmotes[0])
			},
		},

		{
			scenario: "should successfully Query() an emoticon by a code, domain, state",
			emotes: []mako.Emote{
				makeEmote(),
			},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteDB, emotes ...mako.Emote) {
				dbEmotes, err := db.Query(ctx, mako.EmoteQuery{
					Code:   emotes[0].Code,
					Domain: emotes[0].Domain,
					States: []mako.EmoteState{
						mako.Active,
					},
				})

				assert.Nil(t, err)
				assert.Equal(t, 1, len(dbEmotes))

				for i, emote := range dbEmotes {
					RequireEmoteEquality(t, emotes[i], emote)
				}
			},
		},

		{
			scenario: "should successfully Query() multiple emoticons by a code, domain, state",
			emotes: []mako.Emote{
				makeEmoteFn(func(emote *mako.Emote) {
					emote.Code = "123"
					emote.Domain = "555"
				}),
				makeEmoteFn(func(emote *mako.Emote) {
					emote.Code = "123"
					emote.Domain = "555"
				}),
				makeEmoteFn(func(emote *mako.Emote) {
					emote.Code = "123"
					emote.Domain = "555"
				}),
				makeEmoteFn(func(emote *mako.Emote) {
					emote.Code = "123"
					emote.Domain = "555"
				}),
			},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteDB, emotes ...mako.Emote) {
				dbEmotes, err := db.Query(ctx, mako.EmoteQuery{
					Code:   emotes[0].Code,
					Domain: emotes[0].Domain,
					States: []mako.EmoteState{
						mako.Active,
					},
				})

				assert.Nil(t, err)
				assert.Equal(t, len(emotes), len(dbEmotes))

				for _, dbEmote := range dbEmotes {
					for _, emote := range emotes {
						if emote.ID != dbEmote.ID {
							continue
						}

						RequireEmoteEquality(t, dbEmote, emote)
					}
				}
			},
		},

		{
			scenario: "should successfully Query() multiple emoticons by a code, domain, and **different state**",
			emotes: []mako.Emote{
				makeEmoteFn(func(emote *mako.Emote) {
					emote.Code = "123"
					emote.Domain = "555"
				}),
				makeEmoteFn(func(emote *mako.Emote) {
					emote.Code = "123"
					emote.Domain = "555"
				}),
				makeEmoteFn(func(emote *mako.Emote) {
					emote.Code = "123"
					emote.Domain = "555"
				}),
				makeEmoteFn(func(emote *mako.Emote) {
					emote.Code = "123"
					emote.Domain = "555"
					emote.State = "archived"
				}),
			},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteDB, emotes ...mako.Emote) {
				dbEmotes, err := db.Query(ctx, mako.EmoteQuery{
					Code:   emotes[0].Code,
					Domain: emotes[0].Domain,
					States: []mako.EmoteState{
						mako.Active,
						mako.Archived,
					},
				})

				assert.Nil(t, err)
				assert.Equal(t, len(emotes), len(dbEmotes))

				for _, dbEmote := range dbEmotes {
					for _, emote := range emotes {
						if emote.ID != dbEmote.ID {
							continue
						}

						RequireEmoteEquality(t, dbEmote, emote)
					}
				}
			},
		},

		{
			scenario: "should successfully Query() multiple emoticons by a code, domain, and a subset state",
			emotes: []mako.Emote{
				makeEmoteFn(func(emote *mako.Emote) {
					emote.Code = "123"
					emote.Domain = "555"
				}),
				makeEmoteFn(func(emote *mako.Emote) {
					emote.Code = "123"
					emote.Domain = "555"
				}),
				makeEmoteFn(func(emote *mako.Emote) {
					emote.Code = "123"
					emote.Domain = "555"
				}),
				makeEmoteFn(func(emote *mako.Emote) {
					emote.Code = "123"
					emote.Domain = "555"
					emote.State = "pending"
				}),
			},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteDB, emotes ...mako.Emote) {
				dbEmotes, err := db.Query(ctx, mako.EmoteQuery{
					Code:   emotes[0].Code,
					Domain: emotes[0].Domain,
					States: []mako.EmoteState{
						mako.Pending,
					},
				})

				assert.Nil(t, err)
				require.Equal(t, 1, len(dbEmotes))
				RequireEmoteEquality(t, dbEmotes[0], emotes[3])

			},
		},

		{
			scenario: "should call Query() but return no results",
			emotes: []mako.Emote{
				makeEmoteFn(func(emote *mako.Emote) {
					emote.Code = "123"
					emote.Domain = "555"
				}),
				makeEmoteFn(func(emote *mako.Emote) {
					emote.Code = "123"
					emote.Domain = "555"
				}),
				makeEmoteFn(func(emote *mako.Emote) {
					emote.Code = "123"
					emote.Domain = "555"
				}),
				makeEmoteFn(func(emote *mako.Emote) {
					emote.Code = "123"
					emote.Domain = "555"
					emote.State = "archived"
				}),
			},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteDB, emotes ...mako.Emote) {
				dbEmotes, err := db.Query(ctx, mako.EmoteQuery{
					Code:   "404",
					Domain: "whatDomain?",
					States: []mako.EmoteState{
						mako.Active,
						mako.Archived,
					},
				})

				assert.Nil(t, err)
				assert.Equal(t, 0, len(dbEmotes))
			},
		},

		{
			scenario: "should call Query() to return all emotes for a group id",
			emotes: []mako.Emote{
				makeEmoteFn(func(emote *mako.Emote) {
					emote.GroupID = "123"
				}),
				makeEmoteFn(func(emote *mako.Emote) {
					emote.GroupID = "123"
				}),
				makeEmoteFn(func(emote *mako.Emote) {
					emote.GroupID = "123"
				}),
				makeEmoteFn(func(emote *mako.Emote) {
					emote.GroupID = "123"
				}),
			},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteDB, emotes ...mako.Emote) {
				dbEmotes, err := db.Query(ctx, mako.EmoteQuery{
					GroupID: "123",
				})

				assert.Nil(t, err)
				require.Equal(t, len(emotes), len(dbEmotes))

				for _, dbEmote := range dbEmotes {
					for _, emote := range emotes {
						if emote.ID != dbEmote.ID {
							continue
						}

						RequireEmoteEquality(t, dbEmote, emote)
					}
				}
			},
		},
	}

	for _, test := range tests {
		test := test

		t.Run(test.scenario, func(t *testing.T) {
			t.Parallel()

			ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
			defer cancel()

			db, teardown, err := makeDB(ctx, test.emotes...)
			if err != nil {
				t.Fatal(err)
			}

			defer teardown()

			test.test(ctx, t, db, test.emotes...)
		})
	}
}

func TestCachedEmoteDB(t *testing.T, config CachedEmoteDBConfig) {
	tests := []struct {
		scenario string
		emotes   []mako.Emote
		test     func(ctx context.Context, t *testing.T, db mako.EmoteDB, channels *emoteDBWithChannels, emotes ...mako.Emote)
	}{
		{
			scenario: "should successfully call the underlying store with a cache miss",
			emotes: []mako.Emote{
				makeEmote(),
			},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteDB, channels *emoteDBWithChannels, emotes ...mako.Emote) {
				dbEmotes, err := db.ReadByIDs(ctx, emotes[0].ID)
				require.Nil(t, err)
				require.Equal(t, 1, len(dbEmotes))

				ids := <-channels.readByIDs
				require.Equal(t, 1, len(ids))
				require.Equal(t, emotes[0].ID, ids[0])

				for i, emote := range dbEmotes {
					RequireEmoteEquality(t, emotes[i], emote)
				}
			},
		},

		{
			scenario: "should not call the underlying store with a cache hit",
			emotes: []mako.Emote{
				makeEmote(),
			},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteDB, channels *emoteDBWithChannels, emotes ...mako.Emote) {
				dbEmotes, err := db.ReadByIDs(ctx, emotes[0].ID)
				assert.Nil(t, err)
				assert.Equal(t, 1, len(dbEmotes))

				<-channels.readByIDs

				<-time.After(2 * time.Second)

				// Should be a cache hit
				dbEmotes, err = db.ReadByIDs(ctx, emotes[0].ID)
				assert.Nil(t, err)
				assert.Equal(t, 1, len(dbEmotes))

				select {
				case <-channels.readByIDs:
					t.Fatal("expected a cache hit to not hit the underlying store")
				default:
				}
			},
		},

		{
			scenario: "should not call the underlying store with a cache hits for ReadByCodes()",
			emotes: []mako.Emote{
				makeEmote(),
			},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteDB, channels *emoteDBWithChannels, emotes ...mako.Emote) {
				dbEmotes, err := db.ReadByCodes(ctx, emotes[0].Code)
				assert.Nil(t, err)
				require.Equal(t, 1, len(dbEmotes))

				<-channels.readByCodes

				<-time.After(2 * time.Second)

				// Should be a cache hit
				dbEmotes, err = db.ReadByCodes(ctx, emotes[0].Code)
				assert.Nil(t, err)
				assert.Equal(t, 1, len(dbEmotes))

				select {
				case <-channels.readByCodes:
					t.Fatal("expected a cache hit to not hit the underlying store")
				default:
				}
			},
		},

		{
			scenario: "should not call the underlying store with a cache hits for ReadByGroupIDs()",
			emotes: []mako.Emote{
				makeEmoteFn(func(emote *mako.Emote) {
					emote.GroupID = "123"
				}),
				makeEmoteFn(func(emote *mako.Emote) {
					emote.GroupID = "123"
				}),
				makeEmoteFn(func(emote *mako.Emote) {
					emote.GroupID = "123"
				}),
			},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteDB, channels *emoteDBWithChannels, emotes ...mako.Emote) {
				dbEmotes, err := db.ReadByGroupIDs(ctx, emotes[0].GroupID)
				assert.Nil(t, err)
				require.Equal(t, len(emotes), len(dbEmotes))

				<-channels.readByGroupIDs

				<-time.After(2 * time.Second)

				// Should be a cache hit
				dbEmotes, err = db.ReadByGroupIDs(ctx, emotes[0].GroupID)
				assert.Nil(t, err)
				assert.Equal(t, len(emotes), len(dbEmotes))

				select {
				case <-channels.readByGroupIDs:
					t.Fatal("expected a cache hit to not hit the underlying store")
				default:
				}
			},
		},

		{
			scenario: "should not call the underlying store on second request with negative caching for ReadByIDs()",
			emotes: []mako.Emote{
				makeEmote(),
			},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteDB, channels *emoteDBWithChannels, emotes ...mako.Emote) {
				dbEmotes, err := db.ReadByIDs(ctx, emotes[0].ID, "not-an-id-in-the-db")
				assert.Nil(t, err)
				require.Equal(t, 1, len(dbEmotes))
				RequireEmoteEquality(t, emotes[0], dbEmotes[0])

				<-channels.readByIDs

				<-time.After(2 * time.Second)

				// Should be a cache hit for both ids, not just the one present in the base db
				dbEmotes, err = db.ReadByIDs(ctx, emotes[0].ID, "not-an-id-in-the-db")
				assert.Nil(t, err)
				assert.Equal(t, 1, len(dbEmotes))
				RequireEmoteEquality(t, emotes[0], dbEmotes[0])

				select {
				case <-channels.readByIDs:
					t.Fatal("expected a cache hit to not hit the underlying store")
				default:
				}
			},
		},

		{
			scenario: "should not call the underlying store on second request with negative caching for ReadByGroupIDs()",
			emotes: []mako.Emote{
				makeEmote(),
			},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteDB, channels *emoteDBWithChannels, emotes ...mako.Emote) {
				dbEmotes, err := db.ReadByGroupIDs(ctx, emotes[0].GroupID, "not-a-group-id-in-the-db")
				assert.Nil(t, err)
				assert.Equal(t, 1, len(dbEmotes))
				RequireEmoteEquality(t, emotes[0], dbEmotes[0])

				<-channels.readByGroupIDs

				<-time.After(2 * time.Second)

				// Should be a cache hit for both ids, not just the one present in the base db
				dbEmotes, err = db.ReadByGroupIDs(ctx, emotes[0].GroupID, "not-a-group-id-in-the-db")
				assert.Nil(t, err)
				assert.Equal(t, 1, len(dbEmotes))
				RequireEmoteEquality(t, emotes[0], dbEmotes[0])

				select {
				case <-channels.readByGroupIDs:
					t.Fatal("expected a cache hit to not hit the underlying store")
				default:
				}
			},
		},

		{
			scenario: "avoid overwriting existing cache entries with subsequent calls",
			emotes: []mako.Emote{
				makeEmoteFn(func(emote *mako.Emote) {
					emote.GroupID = "0"
					emote.ID = "0"
				}),
				makeEmoteFn(func(emote *mako.Emote) {
					emote.GroupID = "0"
					emote.ID = "1"
				}),
				makeEmoteFn(func(emote *mako.Emote) {
					emote.GroupID = "1"
					emote.ID = "2"
				}),
			},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteDB, channels *emoteDBWithChannels, emotes ...mako.Emote) {
				// this should read from base and write to the cache for group "0"
				dbEmotes, err := db.ReadByGroupIDs(ctx, "0")
				assert.Nil(t, err)
				require.Equal(t, 2, len(dbEmotes))

				initialBaseGroupIDRead := <-channels.readByGroupIDs
				require.Equal(t, 1, len(initialBaseGroupIDRead))
				require.Equal(t, "0", initialBaseGroupIDRead[0])

				<-time.After(2 * time.Second)

				// this read should find group 0 in the cache and lookup group 1 in the base
				dbEmotes2, err2 := db.ReadByGroupIDs(ctx, "0", "1")
				assert.Nil(t, err2)
				require.Equal(t, 3, len(dbEmotes2))

				secondBaseGroupIDRead := <-channels.readByGroupIDs
				require.Equal(t, 1, len(secondBaseGroupIDRead))
				require.Equal(t, "1", secondBaseGroupIDRead[0])

				<-time.After(2 * time.Second)

				// this read should find both groups in the cache and return
				dbEmotes3, err3 := db.ReadByGroupIDs(ctx, "0", "1")
				assert.Nil(t, err3)
				require.Equal(t, 3, len(dbEmotes3))

				select {
				case <-channels.readByGroupIDs:
					t.Fatal("expected a cache hit to not hit the underlying store")
				default:
				}
			},
		},

		{
			scenario: ReadByCodesNegativeCacheTestName,
			emotes: []mako.Emote{
				makeEmote(),
			},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteDB, channels *emoteDBWithChannels, emotes ...mako.Emote) {
				dbEmotes, err := db.ReadByCodes(ctx, emotes[0].Code, "not-a-code-in-the-db")
				assert.Nil(t, err)
				require.Equal(t, 1, len(dbEmotes))
				RequireEmoteEquality(t, emotes[0], dbEmotes[0])

				<-channels.readByCodes

				time.Sleep(2 * time.Second)

				dbEmotes, err = db.ReadByCodes(ctx, "not-a-code-in-the-db")
				assert.Nil(t, err)
				require.Equal(t, 0, len(dbEmotes))

				select {
				case <-channels.readByCodes:
					t.Fatal("expected a cache hit to not hit the underlying store")
				default:
				}
			},
		},

		{
			scenario: "should expire the keys if deleting an emoticon",
			emotes: []mako.Emote{
				makeEmote(),
			},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteDB, channels *emoteDBWithChannels, emotes ...mako.Emote) {
				dbEmotes, err := db.ReadByIDs(ctx, emotes[0].ID)
				assert.Nil(t, err)
				assert.Equal(t, 1, len(dbEmotes))

				<-channels.readByIDs

				_, err = db.DeleteEmote(ctx, emotes[0].ID)
				assert.Nil(t, err)

				select {
				case <-ctx.Done():
					t.Fatalf("expected DeleteEmote() to be called, instead got: %s", ctx.Err())
				case <-channels.deleteEmote:
				}

				// Delete emote performs an existence check against the base db before deleting
				<-channels.readByIDs

				time.Sleep(2 * time.Second)

				// Should be a cache miss as the emote doesn't exist anymore
				dbEmotes, err = db.ReadByIDs(ctx, emotes[0].ID)
				assert.Nil(t, err)
				assert.Equal(t, 0, len(dbEmotes))

				select {
				case <-channels.readByIDs:
				default:
					t.Fatal("expected DeleteEmote() to expire the cache")
				}
			},
		},
	}

	for _, test := range tests {
		test := test

		t.Run(test.scenario, func(t *testing.T) {
			t.Parallel()

			ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
			defer cancel()

			db, teardown, err := config.Base(ctx)
			if err != nil {
				t.Fatal(err)
			}

			defer teardown()

			channels := newEmoteDBWithChannels(db)

			cache, cacheTeardown, err := config.Cache(channels)(ctx, test.emotes...)
			if err != nil {
				t.Fatal(err)
			}

			defer cacheTeardown()

			if config.TestsToSkip != nil && config.TestsToSkip[test.scenario] {
				return
			}

			test.test(ctx, t, cache, channels, test.emotes...)
		})
	}
}

type emoteDBWithChannels struct {
	base mako.EmoteDB

	readByIDs      chan []string
	readByCodes    chan []string
	readByGroupIDs chan []string
	writeEmotes    chan []mako.Emote
	deleteEmote    chan string
	updateState    chan string
	updateCode     chan string
	updatePrefix   chan string
	updateEmote    chan string
	updateEmotes   chan []string
	query          chan mako.EmoteQuery
}

func newEmoteDBWithChannels(base mako.EmoteDB) *emoteDBWithChannels {
	return &emoteDBWithChannels{
		base: base,

		readByIDs:      make(chan []string, 1),
		readByCodes:    make(chan []string, 1),
		readByGroupIDs: make(chan []string, 1),
		writeEmotes:    make(chan []mako.Emote, 1),
		deleteEmote:    make(chan string, 1),
		updateState:    make(chan string, 1),
		updateCode:     make(chan string, 1),
		updatePrefix:   make(chan string, 1),
		updateEmote:    make(chan string, 1),
		updateEmotes:   make(chan []string, 1),
		query:          make(chan mako.EmoteQuery, 1),
	}
}

func (db *emoteDBWithChannels) ReadByIDs(ctx context.Context, ids ...string) ([]mako.Emote, error) {
	db.readByIDs <- ids
	return db.base.ReadByIDs(ctx, ids...)
}

func (db *emoteDBWithChannels) ReadByCodes(ctx context.Context, codes ...string) ([]mako.Emote, error) {
	db.readByCodes <- codes
	return db.base.ReadByCodes(ctx, codes...)
}

func (db *emoteDBWithChannels) ReadByGroupIDs(ctx context.Context, ids ...string) ([]mako.Emote, error) {
	db.readByGroupIDs <- ids
	return db.base.ReadByGroupIDs(ctx, ids...)
}

func (db *emoteDBWithChannels) WriteEmotes(ctx context.Context, emotes ...mako.Emote) ([]mako.Emote, error) {
	db.writeEmotes <- emotes
	return db.base.WriteEmotes(ctx, emotes...)
}

func (db *emoteDBWithChannels) UpdateCode(ctx context.Context, id, code, suffix string) (mako.Emote, error) {
	db.updateCode <- id
	return db.base.UpdateCode(ctx, id, code, suffix)
}

func (db *emoteDBWithChannels) UpdatePrefix(ctx context.Context, id, code, prefix string) (mako.Emote, error) {
	db.updatePrefix <- id
	return db.base.UpdatePrefix(ctx, id, code, prefix)
}

func (db *emoteDBWithChannels) UpdateState(ctx context.Context, id string, state mako.EmoteState) (mako.Emote, error) {
	db.updateState <- id
	return db.base.UpdateState(ctx, id, state)
}

func (db *emoteDBWithChannels) UpdateEmote(ctx context.Context, id string, params mako.EmoteUpdate) (mako.Emote, error) {
	db.updateEmote <- id
	return db.base.UpdateEmote(ctx, id, params)
}

func (db *emoteDBWithChannels) UpdateEmotes(ctx context.Context, emoteUpdatesByID map[string]mako.EmoteUpdate) ([]mako.Emote, error) {
	var ids []string
	for id := range emoteUpdatesByID {
		ids = append(ids, id)
	}

	db.updateEmotes <- ids
	return db.base.UpdateEmotes(ctx, emoteUpdatesByID)
}

func (db *emoteDBWithChannels) DeleteEmote(ctx context.Context, id string) (mako.Emote, error) {
	db.deleteEmote <- id
	return db.base.DeleteEmote(ctx, id)
}

func (db *emoteDBWithChannels) Query(ctx context.Context, query mako.EmoteQuery) ([]mako.Emote, error) {
	db.query <- query
	return db.base.Query(ctx, query)
}

func (db *emoteDBWithChannels) Close() error {
	return db.base.Close()
}

func makeEmoteCodes(emotes ...mako.Emote) []string {
	var ids []string
	for _, emote := range emotes {
		ids = append(ids, emote.Code)
	}

	return ids
}
