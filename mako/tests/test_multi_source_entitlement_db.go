package tests

import (
	"context"
	"testing"
	"time"

	mako "code.justin.tv/commerce/mako/internal"
	"github.com/stretchr/testify/require"
)

func TestMultiSourceEntitlementDB(t *testing.T, makePrimaryDB MakeEntitlementDB, makeLegacyDB MakeEntitlementDB, makeFollowerDB MakeEntitlementDB) {
	tests := []struct {
		scenario             string
		primaryEntitlements  []mako.Entitlement
		legacyEntitlements   []mako.Entitlement
		followerEntitlements []mako.Entitlement
		test                 func(ctx context.Context, t *testing.T, db mako.EntitlementDB, entitlements ...mako.Entitlement)
	}{
		/**************************** READ BY IDS ****************************/
		{
			scenario:             "If there are no entitlements, return an empty slice and no error",
			primaryEntitlements:  []mako.Entitlement{},
			legacyEntitlements:   []mako.Entitlement{},
			followerEntitlements: []mako.Entitlement{},
			test: func(ctx context.Context, t *testing.T, db mako.EntitlementDB, entitlements ...mako.Entitlement) {
				dbEntitlements, err := db.ReadByIDs(ctx, "anyOwnerID", []string{}...)
				require.Nil(t, err)
				require.Empty(t, dbEntitlements)
			},
		},
		{
			scenario:             "Primary entitlements should be returned",
			primaryEntitlements:  []mako.Entitlement{makeEntitlement()},
			legacyEntitlements:   []mako.Entitlement{},
			followerEntitlements: []mako.Entitlement{},
			test: func(ctx context.Context, t *testing.T, db mako.EntitlementDB, entitlements ...mako.Entitlement) {
				dbEntitlements, err := db.ReadByIDs(ctx, entitlements[0].OwnerID, []string{entitlements[0].ID}...)
				require.Nil(t, err)
				require.Equal(t, len(entitlements), len(dbEntitlements))
			},
		},
		{
			scenario:             "Legacy entitlements should be returned",
			primaryEntitlements:  []mako.Entitlement{},
			legacyEntitlements:   []mako.Entitlement{makeEntitlement()},
			followerEntitlements: []mako.Entitlement{},
			test: func(ctx context.Context, t *testing.T, db mako.EntitlementDB, entitlements ...mako.Entitlement) {
				dbEntitlements, err := db.ReadByIDs(ctx, entitlements[0].OwnerID, []string{entitlements[0].ID}...)
				require.Nil(t, err)
				require.Equal(t, len(entitlements), len(dbEntitlements))
			},
		},
		{
			scenario:            "Follower entitlements with the correct prefix on their IDs should be returned",
			primaryEntitlements: []mako.Entitlement{},
			legacyEntitlements:  []mako.Entitlement{},
			followerEntitlements: []mako.Entitlement{makeEntitlementFn(func(e *mako.Entitlement) {
				e.ID = mako.GenerateFollowerEntitlementID(e.ID, e.ChannelID)
			})},
			test: func(ctx context.Context, t *testing.T, db mako.EntitlementDB, entitlements ...mako.Entitlement) {
				dbEntitlements, err := db.ReadByIDs(ctx, entitlements[0].OwnerID, []string{entitlements[0].ID}...)
				require.Nil(t, err)
				require.Equal(t, len(entitlements), len(dbEntitlements))
				require.Equal(t, entitlements[0].ID, dbEntitlements[0].ID)
			},
		},
		{
			scenario:            "Follower entitlements with an incorrect prefix on their IDs should not be returned",
			primaryEntitlements: []mako.Entitlement{},
			legacyEntitlements:  []mako.Entitlement{},
			followerEntitlements: []mako.Entitlement{makeEntitlementFn(func(e *mako.Entitlement) {
				e.ID = "groupID"
			})},
			test: func(ctx context.Context, t *testing.T, db mako.EntitlementDB, entitlements ...mako.Entitlement) {
				dbEntitlements, err := db.ReadByIDs(ctx, entitlements[0].OwnerID, []string{"groupID"}...)
				require.Nil(t, err)
				require.Equal(t, 0, len(dbEntitlements))
			},
		},
		{
			scenario: "Multiple entitlements should be returned",
			primaryEntitlements: []mako.Entitlement{makeEntitlementFn(func(e *mako.Entitlement) {
				e.OwnerID = "123"
			})},
			legacyEntitlements: []mako.Entitlement{makeEntitlementFn(func(e *mako.Entitlement) {
				e.OwnerID = "123"
			})},
			followerEntitlements: []mako.Entitlement{makeEntitlementFn(func(e *mako.Entitlement) {
				e.OwnerID = "123"
				e.ID = mako.GenerateFollowerEntitlementID(e.ID, e.ChannelID)
			})},
			test: func(ctx context.Context, t *testing.T, db mako.EntitlementDB, entitlements ...mako.Entitlement) {
				dbEntitlements, err := db.ReadByIDs(ctx, entitlements[0].OwnerID, append(makeEntitlementIDs(entitlements...))...)
				require.Nil(t, err)
				require.Equal(t, len(entitlements), len(dbEntitlements))
			},
		},
		/**************************** READ BY OWNER_ID ****************************/
		{
			scenario:             "If there are no entitlements, return an empty slice and no error",
			primaryEntitlements:  []mako.Entitlement{},
			legacyEntitlements:   []mako.Entitlement{},
			followerEntitlements: []mako.Entitlement{},
			test: func(ctx context.Context, t *testing.T, db mako.EntitlementDB, entitlements ...mako.Entitlement) {
				dbEntitlements, err := db.ReadByOwnerID(ctx, "anyOwnerID", "anyChannelID")
				require.Nil(t, err)
				require.Empty(t, dbEntitlements)
			},
		},
		{
			scenario:             "Primary entitlements should be returned",
			primaryEntitlements:  []mako.Entitlement{makeEntitlement()},
			legacyEntitlements:   []mako.Entitlement{},
			followerEntitlements: []mako.Entitlement{},
			test: func(ctx context.Context, t *testing.T, db mako.EntitlementDB, entitlements ...mako.Entitlement) {
				dbEntitlements, err := db.ReadByOwnerID(ctx, entitlements[0].OwnerID, "anyChannelID")
				require.Nil(t, err)
				require.Equal(t, len(entitlements), len(dbEntitlements))
			},
		},
		{
			scenario:             "Legacy entitlements should be returned",
			primaryEntitlements:  []mako.Entitlement{},
			legacyEntitlements:   []mako.Entitlement{makeEntitlement()},
			followerEntitlements: []mako.Entitlement{},
			test: func(ctx context.Context, t *testing.T, db mako.EntitlementDB, entitlements ...mako.Entitlement) {
				dbEntitlements, err := db.ReadByOwnerID(ctx, entitlements[0].OwnerID, "anyChannelID")
				require.Nil(t, err)
				require.Equal(t, len(entitlements), len(dbEntitlements))
			},
		},
		{
			scenario:             "Follower entitlements should be returned",
			primaryEntitlements:  []mako.Entitlement{},
			legacyEntitlements:   []mako.Entitlement{},
			followerEntitlements: []mako.Entitlement{makeEntitlement()},
			test: func(ctx context.Context, t *testing.T, db mako.EntitlementDB, entitlements ...mako.Entitlement) {
				dbEntitlements, err := db.ReadByOwnerID(ctx, entitlements[0].OwnerID, entitlements[0].ChannelID)
				require.Nil(t, err)
				require.Equal(t, len(entitlements), len(dbEntitlements))
			},
		},
		{
			scenario: "Multiple entitlements should be returned",
			primaryEntitlements: []mako.Entitlement{makeEntitlementFn(func(e *mako.Entitlement) {
				e.OwnerID = "123"
			})},
			legacyEntitlements: []mako.Entitlement{makeEntitlementFn(func(e *mako.Entitlement) {
				e.OwnerID = "123"
			})},
			followerEntitlements: []mako.Entitlement{makeEntitlementFn(func(e *mako.Entitlement) {
				e.OwnerID = "123"
				e.ChannelID = "matchingChannelID"
			})},
			test: func(ctx context.Context, t *testing.T, db mako.EntitlementDB, entitlements ...mako.Entitlement) {
				dbEntitlements, err := db.ReadByOwnerID(ctx, entitlements[0].OwnerID, "matchingChannelID")
				require.Nil(t, err)
				require.Equal(t, len(entitlements), len(dbEntitlements))
			},
		},
	}

	for _, test := range tests {
		test := test

		t.Run(test.scenario, func(t *testing.T) {
			t.Parallel()

			ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)

			primaryDB, teardownPrimary, err := makePrimaryDB(ctx, test.primaryEntitlements...)
			if err != nil {
				t.Fatal(err)
			}

			legacyDB, teardownLegacy, err := makeLegacyDB(ctx, test.legacyEntitlements...)
			if err != nil {
				t.Fatal(err)
			}

			followerDB, teardownFollower, err := makeFollowerDB(ctx, test.followerEntitlements...)
			if err != nil {
				t.Fatal(err)
			}

			db := mako.NewMultiSourceEntitlementDB(primaryDB, legacyDB, followerDB)

			test.test(ctx, t, db, append(append(test.primaryEntitlements, test.legacyEntitlements...), test.followerEntitlements...)...)

			cancel()
			teardownPrimary()
			teardownLegacy()
			teardownFollower()
		})
	}
}
