package tests

import (
	"context"
	"testing"
	"time"

	makotwirp "code.justin.tv/commerce/mako/twirp"
	"github.com/golang/protobuf/ptypes/timestamp"
	"github.com/stretchr/testify/require"
)

const (
	makoIntegrationTestUserId       = "262616472"
	makoIntegrationTestEmoteGroupId = "336054351"

	makoIntegrationTestEmoteCode1 = "makointHero1"
	makoIntegrationTestEmoteCode2 = "makointHero2"
)

func TestSubs(t *testing.T, makeClient MakeClient) {
	tests := []struct {
		scenario string
		test     func(ctx context.Context, t *testing.T, mako makotwirp.Mako)
	}{
		{
			scenario: "emote picker: emote sets",
			test: func(ctx context.Context, t *testing.T, mako makotwirp.Mako) {
				ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
				defer cancel()

				ownerID := makeID()

				t.Logf("ownerID=%s", ownerID)

				_, err := mako.CreateEmoteGroupEntitlements(ctx, &makotwirp.CreateEmoteGroupEntitlementsRequest{
					Entitlements: []*makotwirp.EmoteGroupEntitlement{
						{
							OwnerId:  ownerID,
							ItemId:   makoIntegrationTestUserId,
							GroupId:  makoIntegrationTestEmoteGroupId,
							OriginId: makeID(),
							Group:    makotwirp.EmoteGroup_Subscriptions,
							Start: &timestamp.Timestamp{
								Seconds: time.Now().UTC().Unix(),
							},
							End: &makotwirp.InfiniteTime{
								IsInfinite: true,
							},
						},
					},
				})

				time.Sleep(time.Second)

				require.Nil(t, err)

				resp, err := mako.GetEmoteSetDetails(ctx, &makotwirp.GetEmoteSetDetailsRequest{
					UserId:     ownerID,
					SkipSitedb: true,
				})

				require.Nil(t, err)

				findEmoteSet(t, resp.EmoteSets, makoIntegrationTestEmoteGroupId)
			},
		},

		{
			scenario: "chat: emote sets",
			test: func(ctx context.Context, t *testing.T, mako makotwirp.Mako) {
				ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
				defer cancel()

				ownerID := makeID()

				t.Logf("ownerID=%s", ownerID)

				_, err := mako.CreateEmoteGroupEntitlements(ctx, &makotwirp.CreateEmoteGroupEntitlementsRequest{
					Entitlements: []*makotwirp.EmoteGroupEntitlement{
						{
							OwnerId:  ownerID,
							ItemId:   makoIntegrationTestUserId,
							GroupId:  makoIntegrationTestEmoteGroupId,
							OriginId: makeID(),
							Group:    makotwirp.EmoteGroup_Subscriptions,
							Start: &timestamp.Timestamp{
								Seconds: time.Now().UTC().Unix(),
							},
							End: &makotwirp.InfiniteTime{
								IsInfinite: true,
							},
						},
					},
				})

				require.Nil(t, err)

				// TODO: Remove whenever we fix the Dynamo issue in Materia as the GetEmotesByCodes API doesn't use the Redis cache in Materia.
				time.Sleep(2 * time.Second)

				codes := []string{makoIntegrationTestEmoteCode1, makoIntegrationTestEmoteCode2}
				resp, err := mako.GetEmotesByCodes(ctx, &makotwirp.GetEmotesByCodesRequest{
					OwnerId: ownerID,
					Codes:   codes,
				})

				require.Nil(t, err)

				findEmoteCodes(t, resp.Emoticons, codes...)
			},
		},
	}

	for _, test := range tests {
		test := test

		t.Run(test.scenario, func(t *testing.T) {
			ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
			defer cancel()

			client, teardown, err := makeClient(ctx)
			if err != nil {
				t.Fatal(err)
			}

			defer teardown()

			test.test(ctx, t, client)
		})
	}
}
