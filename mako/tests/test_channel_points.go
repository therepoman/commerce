package tests

import (
	"context"
	"fmt"
	"testing"
	"time"

	"github.com/golang/protobuf/ptypes/timestamp"
	"github.com/stretchr/testify/require"

	makotwirp "code.justin.tv/commerce/mako/twirp"
)

const (
	makoIntegrationTestEmoteId1 = "304713896"
	channelPointEmoteSet        = "300238151"
)

func TestChannelPoints(t *testing.T, makeClient MakeClient) {
	tests := []struct {
		scenario string
		test     func(ctx context.Context, t *testing.T, mako makotwirp.Mako)
	}{
		{
			scenario: "emote picker: single emotes",
			test: func(ctx context.Context, t *testing.T, mako makotwirp.Mako) {
				ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
				defer cancel()

				ownerID := makeID()

				t.Logf("ownerID=%s", ownerID)

				_, err := mako.CreateEmoteEntitlements(ctx, &makotwirp.CreateEmoteEntitlementsRequest{
					Entitlements: []*makotwirp.EmoteEntitlement{
						{
							OwnerId:     ownerID,
							EmoteId:     makoIntegrationTestEmoteId1,
							ItemOwnerId: makoIntegrationTestUserId,
							OriginId:    makeID(),
							Group:       makotwirp.EmoteGroup_ChannelPoints,
							Start: &timestamp.Timestamp{
								Seconds: time.Now().Add(-5 * time.Minute).UTC().Unix(),
							},
							End: &makotwirp.InfiniteTime{
								IsInfinite: true,
							},
						},
					},
				})

				require.Nil(t, err)

				time.Sleep(1 * time.Second)

				resp, err := mako.GetEmoteSetDetails(ctx, &makotwirp.GetEmoteSetDetailsRequest{
					UserId:     ownerID,
					SkipSitedb: true,
				})

				require.Nil(t, err)

				findEmoteSet(t, resp.EmoteSets, channelPointEmoteSet)
				findEmoteCodesInSet(t, resp.EmoteSets, channelPointEmoteSet, makoIntegrationTestEmoteCode1)
			},
		},

		{
			scenario: "chat: single emotes",
			test: func(ctx context.Context, t *testing.T, mako makotwirp.Mako) {

				ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
				defer cancel()

				ownerID := makeID()

				t.Logf("ownerID=%s", ownerID)

				_, err := mako.CreateEmoteEntitlements(ctx, &makotwirp.CreateEmoteEntitlementsRequest{
					Entitlements: []*makotwirp.EmoteEntitlement{
						{
							OwnerId:     ownerID,
							EmoteId:     makoIntegrationTestEmoteId1,
							ItemOwnerId: makoIntegrationTestUserId,
							OriginId:    makeID(),
							Group:       makotwirp.EmoteGroup_ChannelPoints,
							Start: &timestamp.Timestamp{
								Seconds: time.Now().Add(-5 * time.Minute).UTC().Unix(),
							},
							End: &makotwirp.InfiniteTime{
								IsInfinite: true,
							},
						},
					},
				})

				require.Nil(t, err)

				time.Sleep(1 * time.Second)

				testChat(ctx, t, mako, ownerID, makoIntegrationTestEmoteCode1)
			},
		},

		{
			scenario: "emote picker: single modified BW emotes",
			test: func(ctx context.Context, t *testing.T, mako makotwirp.Mako) {

				ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
				defer cancel()

				ownerID := makeID()
				modifier := "BW"
				modifiedEmoteId := fmt.Sprintf("%s_%s", makoIntegrationTestEmoteId1, modifier)
				modifiedEmoteCode := fmt.Sprintf("%s_%s", makoIntegrationTestEmoteCode1, modifier)

				t.Logf("ownerID=%s", ownerID)

				_, err := mako.CreateEmoteEntitlements(ctx, &makotwirp.CreateEmoteEntitlementsRequest{
					Entitlements: []*makotwirp.EmoteEntitlement{
						{
							OwnerId:     ownerID,
							EmoteId:     modifiedEmoteId,
							ItemOwnerId: makoIntegrationTestUserId,
							OriginId:    makeID(),
							Group:       makotwirp.EmoteGroup_ChannelPoints,
							Start: &timestamp.Timestamp{
								Seconds: time.Now().Add(-5 * time.Minute).UTC().Unix(),
							},
							End: &makotwirp.InfiniteTime{
								IsInfinite: true,
							},
						},
					},
				})

				require.Nil(t, err)

				time.Sleep(1 * time.Second)

				resp, err := mako.GetEmoteSetDetails(ctx, &makotwirp.GetEmoteSetDetailsRequest{
					UserId:     ownerID,
					SkipSitedb: true,
				})

				require.Nil(t, err)

				findEmoteSet(t, resp.EmoteSets, channelPointEmoteSet)
				findEmoteCodesInSet(t, resp.EmoteSets, channelPointEmoteSet, modifiedEmoteCode)
			},
		},

		{
			scenario: "chat: single modified BW emotes",
			test: func(ctx context.Context, t *testing.T, mako makotwirp.Mako) {
				ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
				defer cancel()

				ownerID := makeID()
				modifier := "BW"
				modifiedEmoteId := fmt.Sprintf("%s_%s", makoIntegrationTestEmoteId1, modifier)
				modifiedEmoteCode := fmt.Sprintf("%s_%s", makoIntegrationTestEmoteCode1, modifier)

				t.Logf("ownerID=%s", ownerID)

				_, err := mako.CreateEmoteEntitlements(ctx, &makotwirp.CreateEmoteEntitlementsRequest{
					Entitlements: []*makotwirp.EmoteEntitlement{
						{
							OwnerId:     ownerID,
							EmoteId:     modifiedEmoteId,
							ItemOwnerId: makoIntegrationTestUserId,
							OriginId:    makeID(),
							Group:       makotwirp.EmoteGroup_ChannelPoints,
							Start: &timestamp.Timestamp{
								Seconds: time.Now().Add(-5 * time.Minute).UTC().Unix(),
							},
							End: &makotwirp.InfiniteTime{
								IsInfinite: true,
							},
						},
					},
				})

				require.Nil(t, err)

				time.Sleep(1 * time.Second)

				testChat(ctx, t, mako, ownerID, modifiedEmoteCode)
			},
		},
	}

	for _, test := range tests {
		test := test

		t.Run(test.scenario, func(t *testing.T) {
			ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
			defer cancel()

			client, teardown, err := makeClient(ctx)
			if err != nil {
				t.Fatal(err)
			}

			defer teardown()

			test.test(ctx, t, client)
		})
	}
}
