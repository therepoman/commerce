package tests

import (
	"context"
	"testing"
	"time"

	mako "code.justin.tv/commerce/mako/internal"
	"github.com/segmentio/ksuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

type MakeEmoteStandingOverridesDB = func(ctx context.Context) (mako.EmoteStandingOverridesDB, error)

// This generates random test data.
func makeEmoteStandingOverride(override mako.EmoteStandingOverride) mako.UserEmoteStandingOverride {
	return mako.UserEmoteStandingOverride{
		UserID:   ksuid.New().String(),
		Override: override,
	}
}

func makeEmoteStandingOverrideWithUserID(userID string, override mako.EmoteStandingOverride) mako.UserEmoteStandingOverride {
	group := makeEmoteStandingOverride(override)
	group.UserID = userID
	return group
}

func assertEmoteStandingOverride(t *testing.T, a, b mako.UserEmoteStandingOverride) {
	t.Helper()

	assert.Equal(t, a.UserID, b.UserID, "UserID should match")
	assert.Equal(t, a.Override, b.Override, "Override should match")
}

func TestEmoteStandingOverridesDB(t *testing.T, makeDB MakeEmoteStandingOverridesDB) {
	tests := []struct {
		scenario  string
		overrides []mako.UserEmoteStandingOverride
		test      func(ctx context.Context, t *testing.T, db mako.EmoteStandingOverridesDB, overrides ...mako.UserEmoteStandingOverride)
	}{
		{
			scenario:  "update requires at least one override",
			overrides: []mako.UserEmoteStandingOverride{},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteStandingOverridesDB, overrides ...mako.UserEmoteStandingOverride) {
				// GIVEN: empty overrides list
				assert.Empty(t, overrides)

				// WHEN:
				unprocessed, err := db.UpdateOverrides(ctx, overrides...)

				// THEN:
				assert.Nil(t, unprocessed)
				assert.Error(t, err)
			},
		},
		{
			scenario:  "omitting userID during read results in error",
			overrides: []mako.UserEmoteStandingOverride{},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteStandingOverridesDB, overrides ...mako.UserEmoteStandingOverride) {
				// GIVEN: empty override
				userID := ""

				// WHEN:
				found, err := db.ReadByUserID(ctx, userID)

				// THEN:
				assert.Nil(t, found)
				assert.Error(t, err)
			},
		},
		{
			scenario: "omitting userID during an update results in error",
			overrides: []mako.UserEmoteStandingOverride{
				makeEmoteStandingOverrideWithUserID("", mako.InEmoteGoodStanding),
			},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteStandingOverridesDB, overrides ...mako.UserEmoteStandingOverride) {
				// GIVEN: an override with an empty ID
				assert.Empty(t, overrides[0].UserID)

				// WHEN: creating the overrides
				unprocessed, err := db.UpdateOverrides(ctx, overrides...)

				// THEN:
				assert.Nil(t, unprocessed)
				assert.Error(t, err)
			},
		},
		{
			scenario: "should create an emote standing override successfully",
			overrides: []mako.UserEmoteStandingOverride{
				makeEmoteStandingOverrideWithUserID(ksuid.New().String(), mako.InEmoteGoodStanding),
			},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteStandingOverridesDB, overrides ...mako.UserEmoteStandingOverride) {
				// GIVEN: an override with an ID
				assert.NotEmpty(t, overrides[0].UserID)

				// WHEN: create the override
				unprocessed, err := db.UpdateOverrides(ctx, overrides...)

				// THEN: user IDs are populated.
				require.NoError(t, err)
				assert.Len(t, unprocessed, 0)

				// Read them and make sure they were persisted.
				found, err := db.ReadByUserID(ctx, overrides[0].UserID)
				assert.Nil(t, err)
				assert.NotNil(t, found)
				// assumption here is there is only 1 group in the input list
				assertEmoteStandingOverride(t, overrides[0], *found)
			},
		},
		{
			scenario: "creating multiple emote standing overrides at the same time works as expected",
			overrides: []mako.UserEmoteStandingOverride{
				makeEmoteStandingOverrideWithUserID(ksuid.New().String(), mako.InEmoteGoodStanding),
				makeEmoteStandingOverrideWithUserID(ksuid.New().String(), mako.NotInEmoteGoodStanding),
			},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteStandingOverridesDB, overrides ...mako.UserEmoteStandingOverride) {
				// GIVEN: more than one override
				assert.Len(t, overrides, 2)

				// WHEN: create the overrides
				unprocessed, err := db.UpdateOverrides(ctx, overrides...)

				// THEN: the correct number were created.
				require.NoError(t, err)
				assert.Len(t, unprocessed, 0)

				// Read them and make sure they were persisted.
				found1, err := db.ReadByUserID(ctx, overrides[0].UserID)
				assert.NoError(t, err)
				assert.NotNil(t, found1)
				found2, err := db.ReadByUserID(ctx, overrides[1].UserID)
				assert.NoError(t, err)
				assert.NotNil(t, found2)
			},
		},
		{
			scenario: "updating an override changes dynamo entries as expected",
			overrides: []mako.UserEmoteStandingOverride{
				makeEmoteStandingOverrideWithUserID("123456789", mako.InEmoteGoodStanding),
			},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteStandingOverridesDB, overrides ...mako.UserEmoteStandingOverride) {
				// GIVEN: a valid override is inserted in the DB
				unprocessed, err := db.UpdateOverrides(ctx, overrides...)
				require.NoError(t, err)
				assert.Len(t, unprocessed, 0)

				// WHEN: updating the fields on the override
				override := makeEmoteStandingOverrideWithUserID("123456789", mako.NotInEmoteGoodStanding)
				unprocessed2, err := db.UpdateOverrides(ctx, override)

				// THEN: no error is returned, and the item is correctly updated.
				require.NoError(t, err)
				assert.Len(t, unprocessed2, 0)
				found, err := db.ReadByUserID(ctx, overrides[0].UserID)
				assert.NoError(t, err)
				assert.NotNil(t, found, 1)
				assertEmoteStandingOverride(t, override, *found)
			},
		},
		{
			scenario: "omitting override status when updating an override results in an empty override being stored",
			overrides: []mako.UserEmoteStandingOverride{
				makeEmoteStandingOverrideWithUserID(ksuid.New().String(), mako.InEmoteGoodStanding),
			},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteStandingOverridesDB, overrides ...mako.UserEmoteStandingOverride) {
				// GIVEN: a valid group is inserted in the DB
				unprocessed, err := db.UpdateOverrides(ctx, overrides...)
				require.NoError(t, err)
				assert.Len(t, unprocessed, 0)

				// WHEN: updating the fields on the group with empty override
				override := makeEmoteStandingOverrideWithUserID(overrides[0].UserID, "")
				unprocessed2, err := db.UpdateOverrides(ctx, override)

				// THEN: no error is returned, and the item gets an empty override.
				require.NoError(t, err)
				assert.Len(t, unprocessed2, 0)
				found, err := db.ReadByUserID(ctx, overrides[0].UserID)
				assert.NoError(t, err)
				assert.NotNil(t, found)
				assert.Equal(t, found.UserID, overrides[0].UserID)
				assert.Empty(t, found.Override)
			},
		},
		{
			scenario: "Attempting to update an override with no userID returns an error",
			overrides: []mako.UserEmoteStandingOverride{
				makeEmoteStandingOverrideWithUserID("", mako.InEmoteGoodStanding),
			},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteStandingOverridesDB, overrides ...mako.UserEmoteStandingOverride) {
				// GIVEN:
				assert.Empty(t, overrides[0].UserID)

				// WHEN:
				unprocessed, err := db.UpdateOverrides(ctx, overrides...)

				// THEN:
				assert.Nil(t, unprocessed)
				assert.Error(t, err)
			},
		},
	}

	for _, test := range tests {
		test := test

		t.Run(test.scenario, func(t *testing.T) {
			t.Parallel()

			// Loading into local dynamo can sometimes take a while when many tests are all vying for the same resource
			// at the same time, so we provide a longer context timeout for loading into the DB and then a shorter one
			// for actually running the test.
			ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)

			db, err := makeDB(ctx)
			if err != nil {
				t.Fatal(err)
			}

			cancel()
			ctx, cancel = context.WithTimeout(context.Background(), 3*time.Second)

			test.test(ctx, t, db, test.overrides...)

			cancel()
		})
	}
}
