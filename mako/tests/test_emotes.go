package tests

import (
	"bytes"
	"sort"
	"testing"
	"time"

	mako "code.justin.tv/commerce/mako/internal"
	"github.com/stretchr/testify/require"
)

// RequireEmoteSliceEquality requires that two slices of emotes contain exactly the same emotes but not necessarily in the same order
func RequireEmoteSliceEquality(t *testing.T, a, b []mako.Emote) {
	t.Helper()

	require.Equal(t, len(a), len(b))

	sort.Slice(a, func(i, j int) bool {
		return bytes.Compare([]byte(a[i].ID), []byte(a[j].ID)) == -1
	})
	sort.Slice(b, func(i, j int) bool {
		return bytes.Compare([]byte(b[i].ID), []byte(b[j].ID)) == -1
	})

	for i, emote := range a {
		RequireEmoteEquality(t, emote, b[i])
	}
}

// RequireEmoteEquality requires that the values of two emotes are equal
func RequireEmoteEquality(t *testing.T, a, b mako.Emote) {
	t.Helper()

	require.Equal(t, a.ID, b.ID)
	require.Equal(t, a.OwnerID, b.OwnerID)
	require.Equal(t, a.GroupID, b.GroupID)
	require.Equal(t, a.Prefix, b.Prefix)
	require.Equal(t, a.Suffix, b.Suffix)
	require.Equal(t, a.Code, b.Code)
	require.Equal(t, a.State, b.State)
	require.Equal(t, a.Domain, b.Domain)
	require.Equal(t, a.CreatedAt.Format(time.RFC3339), b.CreatedAt.Format(time.RFC3339))
	require.Equal(t, a.UpdatedAt.Format(time.RFC3339), b.UpdatedAt.Format(time.RFC3339))
	require.Equal(t, a.Order, b.Order)
	require.Equal(t, a.EmotesGroup, b.EmotesGroup)
	require.Equal(t, a.ProductID, b.ProductID)
}
