package tests

import (
	"bytes"
	"context"
	"sort"
	"testing"
	"time"

	"github.com/stretchr/testify/mock"

	"code.justin.tv/commerce/mako/config"
	mako "code.justin.tv/commerce/mako/internal"
	config_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/config"
	mkd "code.justin.tv/commerce/mako/twirp/dashboard"
	"github.com/stretchr/testify/require"
)

type UserChannelEmotesConfig struct {
	MakeEmoteDB         MakeEmoteDB
	MakeEntitlementDB   MakeEntitlementDB
	MakeEmoteModifierDB MakeEmoteModifiersDB
}

type testUserChannelEmotesCase struct {
	scenario            string
	entitlements        []mako.Entitlement
	emotes              []mako.Emote
	emoteModifierGroups []mako.EmoteModifierGroup
	test                func(ctx context.Context, t *testing.T, test test)
}

// TestUserChannelEmotes is a **test suite** that encapsulates all the behaviour of GetUserChannelEmotes.
func TestUserChannelEmotes(t *testing.T, config UserChannelEmotesConfig) {
	t.Run("Basics", func(t *testing.T) {
		TestUserChannelEmotesBasics(t, config)
	})

	t.Run("GroupEmotes", func(t *testing.T) {
		TestUserChannelEmotesGroupEmotes(t, config)
	})

	t.Run("SingleEmotes", func(t *testing.T) {
		TestUserChannelEmotesSingleEmotes(t, config)
	})

	t.Run("ModifiedSingleEmotes", func(t *testing.T) {
		TestUserChannelEmotesModifiedSingleEmotes(t, config)
	})

	t.Run("GlobalAndSmilies", func(t *testing.T) {
		TestUserChannelEmotesGlobalAndSmilieEmotes(t, config)
	})

	t.Run("T2T3ModifiedEmotes", func(t *testing.T) {
		TestUserChannelEmotesT2T3ModifiedEmotes(t, config)
	})

	t.Run("SpecialEmotes", func(t *testing.T) {
		TestUserChannelEmotesSpecialEmotes(t, config)
	})
}

func TestUserChannelEmotesBasics(t *testing.T, c UserChannelEmotesConfig) {
	tests := []testUserChannelEmotesCase{
		{
			scenario:     "should return global emotes and robot smilies when no other entitlements are found",
			entitlements: []mako.Entitlement{},
			emotes: []mako.Emote{
				makeEmoteFn(func(emote *mako.Emote) {
					emote.GroupID = mako.GlobalEmoteGroupID
					emote.ID = "123"
					emote.EmotesGroup = mkd.EmoteOrigin_Globals.String()
				}),
				makeEmoteFn(func(emote *mako.Emote) {
					emote.GroupID = mako.RobotsSmilieGroupID.ToString()
					emote.ID = "1"
					emote.EmotesGroup = mkd.EmoteOrigin_Smilies.String()
				}),
			},
			test: func(ctx context.Context, t *testing.T, test test) {
				emotes, err := mako.GetUserChannelEmotes(ctx, mako.GetUserChannelEmotesParams{
					EmoteDB:          test.EmoteDB,
					EntitlementDB:    test.EntitlementDB,
					EmoteModifiersDB: test.EmoteModifiersDB,
					Dynamic:          test.DynamicConfig,
					UserID:           "notFound",
					ChannelID:        "notFound",
				})
				require.Nil(t, err)
				require.Equal(t, len(emotes), 2)

				assertEmotes(t, test.Emotes, emotes)
			},
		},

		{
			scenario: "should return only active emotes",
			entitlements: []mako.Entitlement{
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = "123"
					e.Type = mako.EmoteGroupEntitlement
				}),
			},
			emotes: []mako.Emote{
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "111"
					e.GroupID = "123"
					e.Code = "Single"
					e.State = mako.Active
				}),
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "222"
					e.GroupID = "123"
					e.Code = "Single"
					e.State = mako.Inactive
				}),
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "333"
					e.GroupID = "123"
					e.Code = "Single"
					e.State = mako.Archived
				}),
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "444"
					e.GroupID = "123"
					e.Code = "Single"
					e.State = mako.PendingArchived
				}),
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "555"
					e.GroupID = "123"
					e.Code = "Double"
					e.State = mako.Pending
				}),
			},
			test: func(ctx context.Context, t *testing.T, test test) {
				emotes, err := mako.GetUserChannelEmotes(ctx, mako.GetUserChannelEmotesParams{
					EmoteDB:          test.EmoteDB,
					EntitlementDB:    test.EntitlementDB,
					EmoteModifiersDB: test.EmoteModifiersDB,
					Dynamic:          test.DynamicConfig,
					UserID:           test.Entitlements[0].OwnerID,
					ChannelID:        "notFound",
				})
				require.Nil(t, err)
				require.Equal(t, 1, len(emotes))
				RequireEmoteEquality(t, test.Emotes[0], emotes[0])
			},
		},

		{
			scenario: "should return no inactive emotes",
			entitlements: []mako.Entitlement{
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = "123"
				}),
			},
			emotes: []mako.Emote{
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "999"
					e.GroupID = "123"
					e.Code = "Single"
					e.State = mako.Inactive
				}),
			},
			test: func(ctx context.Context, t *testing.T, test test) {
				emotes, err := mako.GetUserChannelEmotes(ctx, mako.GetUserChannelEmotesParams{
					EmoteDB:          test.EmoteDB,
					EntitlementDB:    test.EntitlementDB,
					EmoteModifiersDB: test.EmoteModifiersDB,
					Dynamic:          test.DynamicConfig,
					UserID:           test.Entitlements[0].OwnerID,
					ChannelID:        "notFound",
				})
				require.Nil(t, err)
				require.Equal(t, 0, len(emotes))
			},
		},

		{
			scenario: "should not send duplicate group ids to EmotesDB",
			entitlements: []mako.Entitlement{
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = "877"
					e.Type = mako.EmoteGroupEntitlement
				}),
			},
			emotes: []mako.Emote{
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "566"
					e.GroupID = "877"
					e.Code = "HELLO"
				}),
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "569"
					e.GroupID = "877"
					e.Code = "WRATH"
				}),
			},
			test: func(ctx context.Context, t *testing.T, test test) {
				// Call the method once to ensure values are cached if
				// one is used.
				_, err := mako.GetUserChannelEmotes(ctx, mako.GetUserChannelEmotesParams{
					EmoteDB:          test.EmoteDB,
					EntitlementDB:    test.EntitlementDB,
					EmoteModifiersDB: test.EmoteModifiersDB,
					Dynamic:          test.DynamicConfig,
					UserID:           test.Entitlements[0].OwnerID,
					ChannelID:        "notFound",
				})

				require.Nil(t, err)

				emotes, err := mako.GetUserChannelEmotes(ctx, mako.GetUserChannelEmotesParams{
					EmoteDB:          test.EmoteDB,
					EntitlementDB:    test.EntitlementDB,
					EmoteModifiersDB: test.EmoteModifiersDB,
					Dynamic:          test.DynamicConfig,
					UserID:           test.Entitlements[0].OwnerID,
					ChannelID:        "notFound",
				})

				require.Nil(t, err)
				assertEmotes(t, test.Emotes, emotes)
			},
		},
	}

	runUserChannelEmotesTests(t, c, tests...)
}

func TestUserChannelEmotesGlobalAndSmilieEmotes(t *testing.T, c UserChannelEmotesConfig) {
	tests := []testUserChannelEmotesCase{
		{
			scenario:     "should return global emotes",
			entitlements: []mako.Entitlement{},
			emotes: []mako.Emote{
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = mako.KappaEmoteID
					e.Code = "Kappa"
					e.GroupID = mako.GlobalEmoteGroupID
					e.EmotesGroup = mkd.EmoteOrigin_Globals.String()
				}),
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "305954156"
					e.Code = "PogChamp"
					e.GroupID = mako.GlobalEmoteGroupID
					e.EmotesGroup = mkd.EmoteOrigin_Globals.String()
				}),
			},
			test: func(ctx context.Context, t *testing.T, test test) {
				emotes, err := mako.GetUserChannelEmotes(ctx, mako.GetUserChannelEmotesParams{
					EmoteDB:          test.EmoteDB,
					EntitlementDB:    test.EntitlementDB,
					EmoteModifiersDB: test.EmoteModifiersDB,
					Dynamic:          test.DynamicConfig,
					UserID:           "notFound",
					ChannelID:        "notFound",
				})

				require.Nil(t, err)

				assertEmotes(t, test.Emotes, emotes)
			},
		},

		{
			scenario:     "should return robot smilie emotes if there's no entitlement",
			entitlements: []mako.Entitlement{},
			emotes: []mako.Emote{
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "433"
					e.Code = ":/"
					e.EmotesGroup = mkd.EmoteOrigin_Smilies.String()
					e.GroupID = mako.PurpleSmilieGroupID.ToString()
				}),
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "498"
					e.Code = ":/"
					e.EmotesGroup = mkd.EmoteOrigin_Smilies.String()
					e.GroupID = mako.MonkeysSmilieGroupID.ToString()
				}),
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "10"
					e.Code = ":/"
					e.EmotesGroup = mkd.EmoteOrigin_Smilies.String()
					e.GroupID = mako.RobotsSmilieGroupID.ToString()
				}),
			},
			test: func(ctx context.Context, t *testing.T, test test) {
				emotes, err := mako.GetUserChannelEmotes(ctx, mako.GetUserChannelEmotesParams{
					EmoteDB:          test.EmoteDB,
					EntitlementDB:    test.EntitlementDB,
					EmoteModifiersDB: test.EmoteModifiersDB,
					Dynamic:          test.DynamicConfig,
					UserID:           "notFound",
					ChannelID:        "notFound",
				})

				require.Nil(t, err)
				require.Equal(t, 1, len(emotes))

				require.Equal(t, "10", emotes[0].ID)
				require.Equal(t, emotes[0].GroupID, mako.RobotsSmilieGroupID.ToString())
			},
		},

		{
			scenario: "should return the correct smilie emote if entitled to a specific smilie set",
			entitlements: []mako.Entitlement{
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = mako.SmilieGroupID
					e.Source = mako.SmiliesSource
					e.Metadata.GroupID = string(mako.PurpleSmilieGroupID)
				}),
			},
			emotes: []mako.Emote{
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "433"
					e.Code = ":/"
					e.EmotesGroup = mkd.EmoteOrigin_Smilies.String()
					e.GroupID = string(mako.PurpleSmilieGroupID)
				}),
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "498"
					e.Code = ":/"
					e.EmotesGroup = mkd.EmoteOrigin_Smilies.String()
					e.GroupID = string(mako.MonkeysSmilieGroupID)
				}),
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "10"
					e.Code = ":/"
					e.EmotesGroup = mkd.EmoteOrigin_Smilies.String()
					e.GroupID = mako.GlobalEmoteGroupID
				}),
			},
			test: func(ctx context.Context, t *testing.T, test test) {
				emotes, err := mako.GetUserChannelEmotes(ctx, mako.GetUserChannelEmotesParams{
					EmoteDB:          test.EmoteDB,
					EntitlementDB:    test.EntitlementDB,
					EmoteModifiersDB: test.EmoteModifiersDB,
					Dynamic:          test.DynamicConfig,
					UserID:           test.Entitlements[0].OwnerID,
					ChannelID:        "notFound",
				})

				require.Nil(t, err)
				require.Equal(t, 1, len(emotes))

				require.Equal(t, "433", emotes[0].ID)
				require.Equal(t, emotes[0].GroupID, string(mako.PurpleSmilieGroupID))
			},
		},
	}

	runUserChannelEmotesTests(t, c, tests...)
}

func TestUserChannelEmotesModifiedSingleEmotes(t *testing.T, c UserChannelEmotesConfig) {
	tests := []testUserChannelEmotesCase{
		{
			scenario: "should return single modified emote when entitled",
			entitlements: []mako.Entitlement{
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = "566_TK"
					e.Source = mako.ChannelPointsSource
					e.Type = mako.EmoteEntitlement
				}),
			},
			emotes: []mako.Emote{
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "566"
					e.Code = "HELLO"
					e.EmotesGroup = mkd.EmoteOrigin_Subscriptions.String()
				}),
			},
			test: func(ctx context.Context, t *testing.T, test test) {
				emotes, err := mako.GetUserChannelEmotes(ctx, mako.GetUserChannelEmotesParams{
					EmoteDB:                   test.EmoteDB,
					EntitlementDB:             test.EntitlementDB,
					EmoteModifiersDB:          test.EmoteModifiersDB,
					Dynamic:                   test.DynamicConfig,
					CommunityPointsOwnerID:    test.StaticConfig.CommunityPointsOwnerID,
					CommunityPointsEmoteSetID: test.StaticConfig.CommunityPointsEmoteSetID,
					UserID:                    test.Entitlements[0].OwnerID,
					ChannelID:                 "notFound",
				})
				require.Nil(t, err)
				require.Equal(t, len(test.Emotes), len(emotes))

				// Ordering is not guaranteed by any of the APIs.
				sort.Slice(emotes, func(i, j int) bool {
					return bytes.Compare([]byte(emotes[i].ID), []byte(emotes[j].ID)) == -1
				})

				require.Equal(t, "566_TK", emotes[0].ID)
				require.Equal(t, "HELLO_TK", emotes[0].Code)
				require.Equal(t, test.StaticConfig.CommunityPointsEmoteSetID, emotes[0].GroupID)
				require.Equal(t, test.StaticConfig.CommunityPointsOwnerID, emotes[0].OwnerID)
				require.NotEqual(t, test.Emotes[0].EmotesGroup, emotes[0].EmotesGroup)
				require.Equal(t, mkd.EmoteOrigin_ChannelPoints.String(), emotes[0].EmotesGroup)

			},
		},

		{
			scenario: "should return modified emote and normal emote when entitled",
			entitlements: []mako.Entitlement{
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = "999_TK"
					e.Type = mako.EmoteEntitlement
					e.OwnerID = "555"
					e.Source = mako.ChannelPointsSource
				}),
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = "999"
					e.Type = mako.EmoteEntitlement
					e.OwnerID = "555"
				}),
			},
			emotes: []mako.Emote{
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "999"
					e.Code = "HELLO"
					e.EmotesGroup = mkd.EmoteOrigin_Subscriptions.String()
				}),
			},
			test: func(ctx context.Context, t *testing.T, test test) {
				emotes, err := mako.GetUserChannelEmotes(ctx, mako.GetUserChannelEmotesParams{
					EmoteDB:                   test.EmoteDB,
					EntitlementDB:             test.EntitlementDB,
					EmoteModifiersDB:          test.EmoteModifiersDB,
					Dynamic:                   test.DynamicConfig,
					CommunityPointsOwnerID:    test.StaticConfig.CommunityPointsOwnerID,
					CommunityPointsEmoteSetID: test.StaticConfig.CommunityPointsEmoteSetID,
					UserID:                    test.Entitlements[0].OwnerID,
					ChannelID:                 "notFound",
				})

				require.Nil(t, err)
				require.Equal(t, 2, len(emotes))

				// Ordering is not guaranteed by any of the APIs.
				sort.Slice(emotes, func(i, j int) bool {
					return bytes.Compare([]byte(emotes[i].ID), []byte(emotes[j].ID)) == -1
				})

				require.Equal(t, test.Entitlements[1].ID, emotes[0].ID)
				require.Equal(t, test.Emotes[0].GroupID, emotes[0].GroupID)
				require.Equal(t, test.Emotes[0].OwnerID, emotes[0].OwnerID)
				require.Equal(t, test.Emotes[0].EmotesGroup, emotes[0].EmotesGroup)

				require.Equal(t, test.Entitlements[0].ID, emotes[1].ID)
				require.Equal(t, test.StaticConfig.CommunityPointsEmoteSetID, emotes[1].GroupID)
				require.Equal(t, test.StaticConfig.CommunityPointsOwnerID, emotes[1].OwnerID)
				require.NotEqual(t, test.Emotes[0].EmotesGroup, emotes[1].EmotesGroup)
				require.Equal(t, mkd.EmoteOrigin_ChannelPoints.String(), emotes[1].EmotesGroup)
			},
		},

		{
			scenario: "should return modified emote and a group emote",
			entitlements: []mako.Entitlement{
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = "566_TK"
					e.Type = mako.EmoteEntitlement
					e.OwnerID = "myFancyOwner"
				}),
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = "Group1"
					e.Type = mako.EmoteGroupEntitlement
					e.OwnerID = "myFancyOwner"
				}),
			},
			emotes: []mako.Emote{
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "566"
					e.Code = "HELLO"
					e.EmotesGroup = mkd.EmoteOrigin_Subscriptions.String()
				}),
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "877"
					e.Code = "FOO"
					e.GroupID = "Group1"
					e.EmotesGroup = mkd.EmoteOrigin_Subscriptions.String()
				}),
			},
			test: func(ctx context.Context, t *testing.T, test test) {
				emotes, err := mako.GetUserChannelEmotes(ctx, mako.GetUserChannelEmotesParams{
					EmoteDB:                   test.EmoteDB,
					EntitlementDB:             test.EntitlementDB,
					EmoteModifiersDB:          test.EmoteModifiersDB,
					Dynamic:                   test.DynamicConfig,
					CommunityPointsOwnerID:    test.StaticConfig.CommunityPointsOwnerID,
					CommunityPointsEmoteSetID: test.StaticConfig.CommunityPointsEmoteSetID,
					UserID:                    test.Entitlements[0].OwnerID,
					ChannelID:                 "notFound",
				})

				require.Nil(t, err)
				require.Equal(t, len(test.Emotes), len(emotes))

				// Ordering is not guaranteed by any of the APIs.
				sort.Slice(emotes, func(i, j int) bool {
					return bytes.Compare([]byte(emotes[i].ID), []byte(emotes[j].ID)) == -1
				})

				// Ensure modified emotes are returned with the modifier so the CDN asset can be found.
				require.Equal(t, "566_TK", emotes[0].ID)
				require.Equal(t, "877", emotes[1].ID)
			},
		},

		{
			scenario: "should return modified emote id",
			entitlements: []mako.Entitlement{
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = "566_TK"
					e.Type = mako.EmoteEntitlement
					e.OwnerID = "myFancyOwner"
				}),
			},
			emotes: []mako.Emote{
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "566"
					e.Code = "HELLO"
					e.EmotesGroup = mkd.EmoteOrigin_Subscriptions.String()
				}),
			},
			test: func(ctx context.Context, t *testing.T, test test) {
				emotes, err := mako.GetUserChannelEmotes(ctx, mako.GetUserChannelEmotesParams{
					EmoteDB:                   test.EmoteDB,
					EntitlementDB:             test.EntitlementDB,
					EmoteModifiersDB:          test.EmoteModifiersDB,
					Dynamic:                   test.DynamicConfig,
					CommunityPointsOwnerID:    test.StaticConfig.CommunityPointsOwnerID,
					CommunityPointsEmoteSetID: test.StaticConfig.CommunityPointsEmoteSetID,
					UserID:                    test.Entitlements[0].OwnerID,
					ChannelID:                 "notFound",
				})

				require.Nil(t, err)
				require.Equal(t, len(test.Emotes), len(emotes))

				if len(emotes) == 1 {
					require.NotEqual(t, test.Emotes[0].ID, emotes[0].ID)
					require.Equal(t, "566_TK", emotes[0].ID)
				}
			},
		},

		{
			scenario: "only supports legitimate emote modifiers",
			entitlements: []mako.Entitlement{
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = "123"
					e.Type = mako.EmoteGroupEntitlement
					e.OwnerID = "555"
				}),
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = "999_FAKEMODIFIERCODE"
					e.Type = mako.EmoteEntitlement
					e.OwnerID = "555"
				}),
			},
			emotes: []mako.Emote{
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "999"
					e.Code = "HELLO"
					e.GroupID = "123"
					e.EmotesGroup = mkd.EmoteOrigin_Subscriptions.String()
				}),
			},
			test: func(ctx context.Context, t *testing.T, test test) {
				emotes, err := mako.GetUserChannelEmotes(ctx, mako.GetUserChannelEmotesParams{
					EmoteDB:                   test.EmoteDB,
					EntitlementDB:             test.EntitlementDB,
					EmoteModifiersDB:          test.EmoteModifiersDB,
					Dynamic:                   test.DynamicConfig,
					CommunityPointsOwnerID:    test.StaticConfig.CommunityPointsOwnerID,
					CommunityPointsEmoteSetID: test.StaticConfig.CommunityPointsEmoteSetID,
					UserID:                    test.Entitlements[0].OwnerID,
					ChannelID:                 "notFound",
				})

				require.Nil(t, err)

				// Only "HELLO" should be entitled.
				require.Equal(t, 1, len(emotes))
				require.Equal(t, test.Emotes[0].Code, emotes[0].Code)
			},
		},

		{
			scenario: "global emotes do not support modifiers",
			entitlements: []mako.Entitlement{
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = mako.KappaEmoteID + "_HF"
					e.Type = mako.EmoteGroupEntitlement
					e.OwnerID = "555"
				}),
			},
			emotes: []mako.Emote{
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = mako.KappaEmoteID
					e.Code = "Kappa"
					e.GroupID = mako.GlobalEmoteGroupID
				}),
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "543"
					e.Code = "Foobar"
					e.GroupID = mako.GlobalEmoteGroupID
				}),
			},
			test: func(ctx context.Context, t *testing.T, test test) {
				emotes, err := mako.GetUserChannelEmotes(ctx, mako.GetUserChannelEmotesParams{
					EmoteDB:                   test.EmoteDB,
					EntitlementDB:             test.EntitlementDB,
					EmoteModifiersDB:          test.EmoteModifiersDB,
					Dynamic:                   test.DynamicConfig,
					CommunityPointsOwnerID:    test.StaticConfig.CommunityPointsOwnerID,
					CommunityPointsEmoteSetID: test.StaticConfig.CommunityPointsEmoteSetID,
					UserID:                    test.Entitlements[0].OwnerID,
					ChannelID:                 "notFound",
				})

				require.Nil(t, err)

				require.Equal(t, 2, len(emotes))
				require.NotEqual(t, test.Emotes[0].ID, test.Entitlements[0].ID)
				require.NotEqual(t, test.Emotes[1].ID, test.Entitlements[0].ID)
			},
		},

		{
			scenario: "smilies emotes do not support modifiers",
			entitlements: []mako.Entitlement{
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = mako.KappaEmoteID + "_HF"
					e.Type = mako.EmoteGroupEntitlement
					e.OwnerID = "555"
				}),
			},
			emotes: []mako.Emote{
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = mako.KappaEmoteID
					e.Code = "Kappa"
					e.GroupID = mako.GlobalEmoteGroupID
				}),
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "543"
					e.Code = "Foobar"
					e.GroupID = mako.GlobalEmoteGroupID
				}),
			},
			test: func(ctx context.Context, t *testing.T, test test) {
				emotes, err := mako.GetUserChannelEmotes(ctx, mako.GetUserChannelEmotesParams{
					EmoteDB:                   test.EmoteDB,
					EntitlementDB:             test.EntitlementDB,
					EmoteModifiersDB:          test.EmoteModifiersDB,
					Dynamic:                   test.DynamicConfig,
					CommunityPointsOwnerID:    test.StaticConfig.CommunityPointsOwnerID,
					CommunityPointsEmoteSetID: test.StaticConfig.CommunityPointsEmoteSetID,
					UserID:                    test.Entitlements[0].OwnerID,
					ChannelID:                 "notFound",
				})

				require.Nil(t, err)

				require.Equal(t, 2, len(emotes))
				require.NotEqual(t, test.Emotes[0].ID, test.Entitlements[0].ID)
				require.NotEqual(t, test.Emotes[1].ID, test.Entitlements[0].ID)
			},
		},

		{
			scenario: "should not return a modified emote if the base emote does not exist",
			entitlements: []mako.Entitlement{
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = "999_TK"
					e.Type = mako.EmoteEntitlement
					e.OwnerID = "555"
				}),
			},
			test: func(ctx context.Context, t *testing.T, test test) {
				emotes, err := mako.GetUserChannelEmotes(ctx, mako.GetUserChannelEmotesParams{
					EmoteDB:                   test.EmoteDB,
					EntitlementDB:             test.EntitlementDB,
					EmoteModifiersDB:          test.EmoteModifiersDB,
					Dynamic:                   test.DynamicConfig,
					CommunityPointsOwnerID:    test.StaticConfig.CommunityPointsOwnerID,
					CommunityPointsEmoteSetID: test.StaticConfig.CommunityPointsEmoteSetID,
					UserID:                    test.Entitlements[0].OwnerID,
					ChannelID:                 "notFound",
				})

				require.Nil(t, err)

				require.Equal(t, 0, len(emotes))
			},
		},

		{
			scenario: "should not return a modified emote if the base emote is animated",
			entitlements: []mako.Entitlement{
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = "999_TK"
					e.Type = mako.EmoteEntitlement
					e.OwnerID = "555"
				}),
			},
			emotes: []mako.Emote{
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "999"
					e.AssetType = mako.AnimatedAssetType
					e.EmotesGroup = mkd.EmoteOrigin_Subscriptions.String()
				}),
			},
			test: func(ctx context.Context, t *testing.T, test test) {
				emotes, err := mako.GetUserChannelEmotes(ctx, mako.GetUserChannelEmotesParams{
					EmoteDB:                   test.EmoteDB,
					EntitlementDB:             test.EntitlementDB,
					EmoteModifiersDB:          test.EmoteModifiersDB,
					Dynamic:                   test.DynamicConfig,
					CommunityPointsOwnerID:    test.StaticConfig.CommunityPointsOwnerID,
					CommunityPointsEmoteSetID: test.StaticConfig.CommunityPointsEmoteSetID,
					UserID:                    test.Entitlements[0].OwnerID,
					ChannelID:                 "notFound",
				})

				require.Nil(t, err)

				require.Equal(t, 0, len(emotes))
			},
		},
	}

	runUserChannelEmotesTests(t, c, tests...)
}

func TestUserChannelEmotesSingleEmotes(t *testing.T, c UserChannelEmotesConfig) {
	tests := []testUserChannelEmotesCase{
		{
			scenario: "should return single emote when entitled",
			entitlements: []mako.Entitlement{
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = "566"
					e.Type = mako.EmoteEntitlement
				}),
			},
			emotes: []mako.Emote{
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "566"
				}),
			},
			test: func(ctx context.Context, t *testing.T, test test) {
				emotes, err := mako.GetUserChannelEmotes(ctx, mako.GetUserChannelEmotesParams{
					EmoteDB:          test.EmoteDB,
					EntitlementDB:    test.EntitlementDB,
					EmoteModifiersDB: test.EmoteModifiersDB,
					Dynamic:          test.DynamicConfig,
					UserID:           test.Entitlements[0].OwnerID,
					ChannelID:        "notFound",
				})
				require.Nil(t, err)
				require.Equal(t, len(test.Emotes), len(emotes))

				sort.Slice(emotes, func(i, j int) bool {
					return bytes.Compare([]byte(emotes[i].ID), []byte(emotes[j].ID)) == -1
				})

				require.Equal(t, "566", emotes[0].ID)
			},
		},
		{
			scenario: "should only return the emote that has a valid entitlement",
			entitlements: []mako.Entitlement{
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.Source = mako.HypeTrainSource
					e.ID = "301739499"
					e.Type = mako.EmoteEntitlement
				}),
			},
			emotes: []mako.Emote{
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "301739499"
					e.Code = "HypePunch"
					e.GroupID = "301040478"
				}),
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "23128"
					e.Code = "Choo"
					e.GroupID = "6836"
				}),
			},
			test: func(ctx context.Context, t *testing.T, test test) {
				emotes, err := mako.GetUserChannelEmotes(ctx, mako.GetUserChannelEmotesParams{
					EmoteDB:          test.EmoteDB,
					EntitlementDB:    test.EntitlementDB,
					EmoteModifiersDB: test.EmoteModifiersDB,
					Dynamic:          test.DynamicConfig,
					UserID:           test.Entitlements[0].OwnerID,
					ChannelID:        "notFound",
				})

				require.Nil(t, err)
				require.Equal(t, 1, len(emotes))

				require.Equal(t, "301739499", emotes[0].ID)
				require.Equal(t, "301040478", emotes[0].GroupID)
			},
		},
		{
			scenario: "should augment the emote with a special event ownerID if one is set in the config",
			entitlements: []mako.Entitlement{
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.Source = mako.HypeTrainSource
					e.ID = "301108048"
					e.Type = mako.EmoteEntitlement
					e.ChannelID = "123456789" //Special event owner id
				}),
			},
			emotes: []mako.Emote{
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "301108048"
					e.Code = "HahaReindeer"
					e.OwnerID = "QaTwPartnerOwnerID"
				}),
			},
			test: func(ctx context.Context, t *testing.T, test test) {
				mockStaticConfig := config.Configuration{
					SpecialEventOwnerIDs: map[string]bool{
						test.Entitlements[0].ChannelID: true,
					},
				}

				emotes, err := mako.GetUserChannelEmotes(ctx, mako.GetUserChannelEmotesParams{
					EmoteDB:              test.EmoteDB,
					EntitlementDB:        test.EntitlementDB,
					EmoteModifiersDB:     test.EmoteModifiersDB,
					Dynamic:              test.DynamicConfig,
					SpecialEventOwnerIDs: mockStaticConfig.SpecialEventOwnerIDs,
					UserID:               test.Entitlements[0].OwnerID,
					ChannelID:            "notFound",
				})

				require.Nil(t, err)
				require.Equal(t, 1, len(emotes))

				require.Equal(t, "301108048", emotes[0].ID)
				require.Equal(t, test.Entitlements[0].ChannelID, emotes[0].OwnerID)
			},
		},
	}

	runUserChannelEmotesTests(t, c, tests...)
}

func TestUserChannelEmotesGroupEmotes(t *testing.T, c UserChannelEmotesConfig) {
	tests := []testUserChannelEmotesCase{
		{
			scenario: "should return an emote when an emote group entitlement is found",
			entitlements: []mako.Entitlement{
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = "123"
				}),
			},
			emotes: []mako.Emote{
				makeEmoteFn(func(e *mako.Emote) {
					e.GroupID = "123"
				}),
			},
			test: func(ctx context.Context, t *testing.T, test test) {
				emotes, err := mako.GetUserChannelEmotes(ctx, mako.GetUserChannelEmotesParams{
					EmoteDB:          test.EmoteDB,
					EntitlementDB:    test.EntitlementDB,
					EmoteModifiersDB: test.EmoteModifiersDB,
					Dynamic:          test.DynamicConfig,
					UserID:           test.Entitlements[0].OwnerID,
					ChannelID:        "notFound",
				})
				require.Nil(t, err)

				assertEmotes(t, test.Emotes, emotes)
			},
		},

		{
			scenario: "should return multiple emotes when an emote group entitlement is found",
			entitlements: []mako.Entitlement{
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = "123"
				}),
			},
			emotes: []mako.Emote{
				makeEmoteFn(func(e *mako.Emote) {
					e.GroupID = "123"
				}),
				makeEmoteFn(func(e *mako.Emote) {
					e.GroupID = "123"
				}),
				makeEmoteFn(func(e *mako.Emote) {
					e.GroupID = "123"
				}),
				makeEmoteFn(func(e *mako.Emote) {
					e.GroupID = "123"
				}),
			},
			test: func(ctx context.Context, t *testing.T, test test) {
				emotes, err := mako.GetUserChannelEmotes(ctx, mako.GetUserChannelEmotesParams{
					EmoteDB:          test.EmoteDB,
					EntitlementDB:    test.EntitlementDB,
					EmoteModifiersDB: test.EmoteModifiersDB,
					Dynamic:          test.DynamicConfig,
					UserID:           test.Entitlements[0].OwnerID,
					ChannelID:        "notFound",
				})
				require.Nil(t, err)

				assertEmotes(t, test.Emotes, emotes)
			},
		},

		{
			scenario: "should return user's emotes",
			entitlements: []mako.Entitlement{
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = "20704"
				}),
			},
			emotes: []mako.Emote{
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "300997894"
					e.Code = "miiliS"
					e.GroupID = "20704"
					e.State = mako.Active
				}),
			},
			test: func(ctx context.Context, t *testing.T, test test) {
				emotes, err := mako.GetUserChannelEmotes(ctx, mako.GetUserChannelEmotesParams{
					EmoteDB:          test.EmoteDB,
					EntitlementDB:    test.EntitlementDB,
					EmoteModifiersDB: test.EmoteModifiersDB,
					Dynamic:          test.DynamicConfig,
					UserID:           test.Entitlements[0].OwnerID,
					ChannelID:        "notFound",
				})

				require.Nil(t, err)
				require.Equal(t, 1, len(emotes))

				require.Equal(t, test.Emotes[0].ID, emotes[0].ID)
			},
		},

		{
			scenario: "should allow a user to see animated emotes",
			entitlements: []mako.Entitlement{
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = "123"
					e.OwnerID = "ownerID"
					e.ChannelID = "emoteOwner"
					e.Source = mako.SubscriptionsSource
					e.Type = mako.EmoteGroupEntitlement
				}),
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = "124"
					e.OwnerID = "ownerID"
					e.ChannelID = "emoteOwner"
					e.Source = mako.SubscriptionsSource
					e.Type = mako.EmoteGroupEntitlement
				}),
			},
			emotes: []mako.Emote{
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "999"
					e.OwnerID = "emoteOwner"
					e.GroupID = "123"
					e.Code = "Hello"
					e.AssetType = mako.AnimatedAssetType
					e.AnimationTemplate = mako.Shake
				}),
			},
			test: func(ctx context.Context, t *testing.T, test test) {
				emotes, err := mako.GetUserChannelEmotes(ctx, mako.GetUserChannelEmotesParams{
					EmoteDB:          test.EmoteDB,
					EntitlementDB:    test.EntitlementDB,
					EmoteModifiersDB: test.EmoteModifiersDB,
					Dynamic:          test.DynamicConfig,
					UserID:           "ownerID",
					ChannelID:        "notFound",
				})
				require.Nil(t, err)
				require.Equal(t, 1, len(emotes))
				require.Equal(t, test.Emotes[0].ID, emotes[0].ID)
			},
		},

		{
			scenario: "should return follower emote groups if one is entitled",
			entitlements: []mako.Entitlement{
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = "123"
					e.OwnerID = "ownerID"
					e.ChannelID = "emoteOwner"
					e.Source = mako.FollowerSource
					e.Type = mako.EmoteGroupEntitlement
				}),
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = "124"
					e.OwnerID = "ownerID"
					e.ChannelID = "emoteOwner"
					e.Source = mako.SubscriptionsSource
					e.Type = mako.EmoteGroupEntitlement
				}),
			},
			emotes: []mako.Emote{
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "999"
					e.OwnerID = "emoteOwner"
					e.GroupID = "123"
					e.Code = "Hello"
					e.EmotesGroup = mkd.EmoteOrigin_Follower.String()
				}),
			},
			test: func(ctx context.Context, t *testing.T, test test) {
				emotes, err := mako.GetUserChannelEmotes(ctx, mako.GetUserChannelEmotesParams{
					EmoteDB:          test.EmoteDB,
					EntitlementDB:    test.EntitlementDB,
					EmoteModifiersDB: test.EmoteModifiersDB,
					Dynamic:          test.DynamicConfig,
					UserID:           test.Entitlements[0].OwnerID,
					ChannelID:        test.Emotes[0].OwnerID,
				})
				require.Nil(t, err)
				require.Equal(t, 1, len(emotes))
				require.Equal(t, test.Emotes[0].ID, emotes[0].ID)
			},
		},
	}

	runUserChannelEmotesTests(t, c, tests...)
}

func TestUserChannelEmotesSpecialEmotes(t *testing.T, c UserChannelEmotesConfig) {
	tests := []testUserChannelEmotesCase{
		{
			scenario: "should return 2FA emote",
			entitlements: []mako.Entitlement{
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = mako.TwoFactorEmoteGroupID
					e.Type = mako.EmoteEntitlement
					e.OriginID = mako.TwoFactorOriginID.String()
					e.Source = mako.RewardsSource
				}),
			},
			emotes: []mako.Emote{
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "123"
					e.GroupID = mako.TwoFactorEmoteGroupID
					e.Code = "SirPrise"
				}),
			},
			test: func(ctx context.Context, t *testing.T, test test) {
				emotes, err := mako.GetUserChannelEmotes(ctx, mako.GetUserChannelEmotesParams{
					EmoteDB:          test.EmoteDB,
					EntitlementDB:    test.EntitlementDB,
					EmoteModifiersDB: test.EmoteModifiersDB,
					Dynamic:          test.DynamicConfig,
					UserID:           test.Entitlements[0].OwnerID,
					ChannelID:        "notFound",
				})

				require.Nil(t, err)
				require.Equal(t, 1, len(emotes))
				RequireEmoteEquality(t, test.Emotes[0], emotes[0])
			},
		},
		{
			scenario: "golden kappa",
			emotes: []mako.Emote{
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = mako.KappaEmoteID
					e.Code = "Kappa"
					e.GroupID = mako.GlobalEmoteGroupID
					e.EmotesGroup = mkd.EmoteOrigin_Globals.String()
				}),
			},
			test: func(ctx context.Context, t *testing.T, test test) {
				mockDynamicConfig := new(config_mock.DynamicConfigClient)

				mockDynamicConfig.On("GetGoldenKappaAlwaysOnEnabled", mock.Anything).Return(true)

				emotes, err := mako.GetUserChannelEmotes(ctx, mako.GetUserChannelEmotesParams{
					EmoteDB:          test.EmoteDB,
					EntitlementDB:    test.EntitlementDB,
					EmoteModifiersDB: test.EmoteModifiersDB,
					Dynamic:          mockDynamicConfig,
					UserID:           "notFound",
					ChannelID:        "notFound",
				})

				require.Nil(t, err)
				require.Equal(t, 1, len(emotes))
				require.Equal(t, mako.GoldenKappaEmoteID, emotes[0].ID) //User should have golden Kappa
			},
		},
	}

	runUserChannelEmotesTests(t, c, tests...)
}

func TestUserChannelEmotesT2T3ModifiedEmotes(t *testing.T, c UserChannelEmotesConfig) {
	tests := []testUserChannelEmotesCase{
		{
			scenario:     "should return no T2/T3 emotes when owner has no entitlements",
			entitlements: []mako.Entitlement{},
			emotes: []mako.Emote{
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "999"
					e.OwnerID = "emoteOwner"
					e.GroupID = "123"
					e.Code = "Hello"
					e.State = mako.Active
				}),
			},
			emoteModifierGroups: []mako.EmoteModifierGroup{
				makeEmoteModifierGroupFn(func(e *mako.EmoteModifierGroup) {
					e.OwnerID = "ownerID"
					e.Modifiers = []string{"BW"}
					e.SourceEmoteGroupIDs = []string{"123"}
				}),
			},
			test: func(ctx context.Context, t *testing.T, test test) {
				emotes, err := mako.GetUserChannelEmotes(ctx, mako.GetUserChannelEmotesParams{
					EmoteDB:          test.EmoteDB,
					EntitlementDB:    test.EntitlementDB,
					EmoteModifiersDB: test.EmoteModifiersDB,
					Dynamic:          test.DynamicConfig,
					UserID:           "notFound",
					ChannelID:        "notFound",
				})
				require.Nil(t, err)
				require.Equal(t, 0, len(emotes))
			},
		},
		{
			scenario: "should return 0 emotes when owner is missing all required entitlements",
			entitlements: []mako.Entitlement{
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = "123"
					e.OwnerID = "ownerID"
				}),
			},
			emotes: []mako.Emote{
				makeEmoteFn(func(e *mako.Emote) {
					e.OwnerID = "emoteOwner"
					e.GroupID = "124"
					e.Code = "Hello"
				}),
				makeEmoteFn(func(e *mako.Emote) {
					e.OwnerID = "emoteOwner"
					e.GroupID = "125"
					e.Code = "World"
				}),
			},
			emoteModifierGroups: []mako.EmoteModifierGroup{
				makeEmoteModifierGroupFn(func(e *mako.EmoteModifierGroup) {
					e.OwnerID = "ownerID"
					e.Modifiers = []string{"BW"}
					e.SourceEmoteGroupIDs = []string{"124", "125"}
				}),
			},
			test: func(ctx context.Context, t *testing.T, test test) {
				emotes, err := mako.GetUserChannelEmotes(ctx, mako.GetUserChannelEmotesParams{
					EmoteDB:          test.EmoteDB,
					EntitlementDB:    test.EntitlementDB,
					EmoteModifiersDB: test.EmoteModifiersDB,
					Dynamic:          test.DynamicConfig,
					UserID:           test.Entitlements[0].OwnerID,
					ChannelID:        "notFound",
				})
				require.Nil(t, err)
				require.Equal(t, 0, len(emotes))
			},
		},
		{
			scenario: "should return 0 emotes when owner is missing one of the required entitlements",
			entitlements: []mako.Entitlement{
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = "123"
					e.OwnerID = "ownerID"
				}),
			},
			emotes: []mako.Emote{
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "999"
					e.OwnerID = "emoteOwner"
					e.GroupID = "124"
					e.Code = "Single"
				}),
			},
			emoteModifierGroups: []mako.EmoteModifierGroup{
				makeEmoteModifierGroupFn(func(e *mako.EmoteModifierGroup) {
					e.OwnerID = "ownerID"
					e.Modifiers = []string{"BW"}
					e.SourceEmoteGroupIDs = []string{"123", "124"}
				}),
			},
			test: func(ctx context.Context, t *testing.T, test test) {
				emotes, err := mako.GetUserChannelEmotes(ctx, mako.GetUserChannelEmotesParams{
					EmoteDB:          test.EmoteDB,
					EntitlementDB:    test.EntitlementDB,
					EmoteModifiersDB: test.EmoteModifiersDB,
					Dynamic:          test.DynamicConfig,
					UserID:           test.Entitlements[0].OwnerID,
					ChannelID:        "notFound",
				})
				require.Nil(t, err)
				require.Equal(t, 0, len(emotes))
			},
		},
		{
			scenario: "should return 0 emotes when owner is missing multiple required entitlements",
			entitlements: []mako.Entitlement{
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = "123"
					e.OwnerID = "ownerID"
				}),
			},
			emotes: []mako.Emote{
				makeEmoteFn(func(e *mako.Emote) {
					e.OwnerID = "emoteOwner"
					e.GroupID = "124"
					e.Code = "Hello"
				}),
				makeEmoteFn(func(e *mako.Emote) {
					e.OwnerID = "emoteOwner"
					e.GroupID = "125"
					e.Code = "World"
				}),
			},
			emoteModifierGroups: []mako.EmoteModifierGroup{
				makeEmoteModifierGroupFn(func(e *mako.EmoteModifierGroup) {
					e.OwnerID = "ownerID"
					e.Modifiers = []string{"BW"}
					e.SourceEmoteGroupIDs = []string{"123", "124", "125"}
				}),
			},
			test: func(ctx context.Context, t *testing.T, test test) {
				emotes, err := mako.GetUserChannelEmotes(ctx, mako.GetUserChannelEmotesParams{
					EmoteDB:          test.EmoteDB,
					EntitlementDB:    test.EntitlementDB,
					EmoteModifiersDB: test.EmoteModifiersDB,
					Dynamic:          test.DynamicConfig,
					UserID:           test.Entitlements[0].OwnerID,
					ChannelID:        "notFound",
				})
				require.Nil(t, err)
				require.Equal(t, 0, len(emotes))
			},
		},
		{
			scenario: "should return modified emotes when the owner has the correct entitlement",
			entitlements: []mako.Entitlement{
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = "123"
					e.OwnerID = "ownerID"
					e.ChannelID = "emoteOwner"
					e.Source = mako.SubscriptionsSource
					e.Type = mako.EmoteGroupEntitlement
				}),
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = "124"
					e.OwnerID = "ownerID"
					e.ChannelID = "emoteOwner"
					e.Source = mako.SubscriptionsSource
					e.Type = mako.EmoteGroupEntitlement
				}),
			},
			emotes: []mako.Emote{
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "999"
					e.OwnerID = "emoteOwner"
					e.GroupID = "123"
					e.Code = "Hello"
					e.EmotesGroup = mkd.EmoteOrigin_Subscriptions.String()
				}),
			},
			emoteModifierGroups: []mako.EmoteModifierGroup{
				makeEmoteModifierGroupFn(func(e *mako.EmoteModifierGroup) {
					e.OwnerID = "emoteOwner"
					e.Modifiers = []string{"BW"}
					e.ID = "456"
					e.SourceEmoteGroupIDs = []string{"123", "124"}
				}),
			},
			test: func(ctx context.Context, t *testing.T, test test) {
				emotes, err := mako.GetUserChannelEmotes(ctx, mako.GetUserChannelEmotesParams{
					EmoteDB:          test.EmoteDB,
					EntitlementDB:    test.EntitlementDB,
					EmoteModifiersDB: test.EmoteModifiersDB,
					Dynamic:          test.DynamicConfig,
					UserID:           test.Entitlements[0].OwnerID,
					ChannelID:        "notFound",
				})
				require.Nil(t, err)
				require.Equal(t, 1, len(emotes))
				require.Equal(t, test.Emotes[0].ID, emotes[0].ID)
				require.NotNil(t, emotes[0].Modifiers)
				require.Equal(t, "BW", emotes[0].Modifiers[0])

			},
		},
		{
			scenario: "should return modified emotes when the owner has the correct entitlements",
			entitlements: []mako.Entitlement{
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = "123"
					e.OwnerID = "ownerID"
					e.ChannelID = "emoteOwner"
					e.Source = mako.SubscriptionsSource
					e.Type = mako.EmoteGroupEntitlement
				}),
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = "124"
					e.OwnerID = "ownerID"
					e.ChannelID = "emoteOwner"
					e.Source = mako.SubscriptionsSource
					e.Type = mako.EmoteGroupEntitlement
				}),
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = "125"
					e.OwnerID = "ownerID"
					e.ChannelID = "emoteOwner"
					e.Source = mako.SubscriptionsSource
					e.Type = mako.EmoteGroupEntitlement
				}),
			},
			emotes: []mako.Emote{
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "999"
					e.OwnerID = "emoteOwner"
					e.GroupID = "123"
					e.Code = "Hello"
					e.EmotesGroup = mkd.EmoteOrigin_Subscriptions.String()
				}),
			},
			emoteModifierGroups: []mako.EmoteModifierGroup{
				makeEmoteModifierGroupFn(func(e *mako.EmoteModifierGroup) {
					e.OwnerID = "emoteOwner"
					e.Modifiers = []string{"BW"}
					e.SourceEmoteGroupIDs = []string{"123", "124"}
				}),
				makeEmoteModifierGroupFn(func(e *mako.EmoteModifierGroup) {
					e.OwnerID = "emoteOwner"
					e.Modifiers = []string{"SG", "HF"}
					e.SourceEmoteGroupIDs = []string{"123", "124", "125"}
				}),
			},
			test: func(ctx context.Context, t *testing.T, test test) {
				emotes, err := mako.GetUserChannelEmotes(ctx, mako.GetUserChannelEmotesParams{
					EmoteDB:          test.EmoteDB,
					EntitlementDB:    test.EntitlementDB,
					EmoteModifiersDB: test.EmoteModifiersDB,
					Dynamic:          test.DynamicConfig,
					UserID:           test.Entitlements[0].OwnerID,
					ChannelID:        "notFound",
				})
				require.Nil(t, err)
				require.Equal(t, 1, len(emotes))
				require.NotNil(t, emotes[0].Modifiers)
				require.Equal(t, 3, len(emotes[0].Modifiers))
			},
		},
		{
			scenario: "should return modifiers on each of the emotes when an owner has the correct entitlements",
			entitlements: []mako.Entitlement{
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = "123"
					e.OwnerID = "ownerID"
					e.ChannelID = "emoteOwner"
					e.Source = mako.SubscriptionsSource
					e.Type = mako.EmoteGroupEntitlement
				}),
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = "124"
					e.OwnerID = "ownerID"
					e.ChannelID = "emoteOwner"
					e.Source = mako.SubscriptionsSource
					e.Type = mako.EmoteGroupEntitlement
				}),
			},
			emotes: []mako.Emote{
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "999"
					e.OwnerID = "emoteOwner"
					e.GroupID = "123"
					e.Code = "Hello1"
					e.EmotesGroup = mkd.EmoteOrigin_Subscriptions.String()
				}),
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "1000"
					e.OwnerID = "emoteOwner"
					e.GroupID = "123"
					e.Code = "Hello2"
					e.EmotesGroup = mkd.EmoteOrigin_Subscriptions.String()
				}),
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "1001"
					e.OwnerID = "emoteOwner"
					e.GroupID = "123"
					e.Code = "Hello3"
					e.EmotesGroup = mkd.EmoteOrigin_Subscriptions.String()
				}),
			},
			emoteModifierGroups: []mako.EmoteModifierGroup{
				makeEmoteModifierGroupFn(func(e *mako.EmoteModifierGroup) {
					e.OwnerID = "emoteOwner"
					e.Modifiers = []string{"BW"}
					e.SourceEmoteGroupIDs = []string{"123", "124"}
				}),
				makeEmoteModifierGroupFn(func(e *mako.EmoteModifierGroup) {
					e.OwnerID = "emoteOwner"
					e.Modifiers = []string{"SG", "HF"}
					e.SourceEmoteGroupIDs = []string{"123", "124", "125"}
				}),
			},
			test: func(ctx context.Context, t *testing.T, test test) {
				emotes, err := mako.GetUserChannelEmotes(ctx, mako.GetUserChannelEmotesParams{
					EmoteDB:          test.EmoteDB,
					EntitlementDB:    test.EntitlementDB,
					EmoteModifiersDB: test.EmoteModifiersDB,
					Dynamic:          test.DynamicConfig,
					UserID:           "ownerID",
					ChannelID:        "notFound",
				})
				require.Nil(t, err)
				require.Equal(t, 3, len(emotes))
				require.NotNil(t, emotes[0].Modifiers)
				require.Equal(t, 1, len(emotes[0].Modifiers))
				require.Equal(t, 1, len(emotes[1].Modifiers))
				require.Equal(t, 1, len(emotes[2].Modifiers))
			},
		},
		{
			scenario: "should return modifiers on each of the emotes when an owner has the correct entitlements",
			entitlements: []mako.Entitlement{
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = "123"
					e.OwnerID = "ownerID"
					e.ChannelID = "emoteOwner"
					e.Source = mako.SubscriptionsSource
					e.Type = mako.EmoteGroupEntitlement
				}),
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = "124"
					e.OwnerID = "ownerID"
					e.ChannelID = "emoteOwner"
					e.Source = mako.SubscriptionsSource
					e.Type = mako.EmoteGroupEntitlement
				}),
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = "125"
					e.OwnerID = "ownerID"
					e.ChannelID = "emoteOwner"
					e.Source = mako.SubscriptionsSource
					e.Type = mako.EmoteGroupEntitlement
				}),
			},
			emotes: []mako.Emote{
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "999"
					e.OwnerID = "emoteOwner"
					e.GroupID = "123"
					e.Code = "Hello1"
					e.EmotesGroup = mkd.EmoteOrigin_Subscriptions.String()
				}),
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "1000"
					e.OwnerID = "emoteOwner"
					e.GroupID = "123"
					e.Code = "Hello2"
					e.EmotesGroup = mkd.EmoteOrigin_Subscriptions.String()
				}),
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "1001"
					e.OwnerID = "emoteOwner"
					e.GroupID = "123"
					e.Code = "Hello3"
					e.EmotesGroup = mkd.EmoteOrigin_Subscriptions.String()
				}),
			},
			emoteModifierGroups: []mako.EmoteModifierGroup{
				makeEmoteModifierGroupFn(func(e *mako.EmoteModifierGroup) {
					e.OwnerID = "emoteOwner"
					e.Modifiers = []string{"BW"}
					e.SourceEmoteGroupIDs = []string{"123", "124"}
				}),
				makeEmoteModifierGroupFn(func(e *mako.EmoteModifierGroup) {
					e.OwnerID = "emoteOwner"
					e.Modifiers = []string{"SG", "HF"}
					e.SourceEmoteGroupIDs = []string{"123", "124", "125"}
				}),
			},
			test: func(ctx context.Context, t *testing.T, test test) {
				emotes, err := mako.GetUserChannelEmotes(ctx, mako.GetUserChannelEmotesParams{
					EmoteDB:          test.EmoteDB,
					EntitlementDB:    test.EntitlementDB,
					EmoteModifiersDB: test.EmoteModifiersDB,
					Dynamic:          test.DynamicConfig,
					UserID:           "ownerID",
					ChannelID:        "notFound",
				})
				require.Nil(t, err)
				require.Equal(t, 3, len(emotes))
				require.NotNil(t, emotes[0].Modifiers)
				require.Equal(t, 3, len(emotes[0].Modifiers))
				require.Equal(t, 3, len(emotes[1].Modifiers))
				require.Equal(t, 3, len(emotes[2].Modifiers))
			},
		},
		{
			scenario: "should not allow a user to see higher tier modifiers than they are entitled to",
			entitlements: []mako.Entitlement{
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = "123"
					e.OwnerID = "ownerID"
					e.ChannelID = "emoteOwner"
					e.Source = mako.SubscriptionsSource
					e.Type = mako.EmoteGroupEntitlement
				}),
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = "124"
					e.OwnerID = "ownerID"
					e.ChannelID = "emoteOwner"
					e.Source = mako.SubscriptionsSource
					e.Type = mako.EmoteGroupEntitlement
				}),
			},
			emotes: []mako.Emote{
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "999"
					e.OwnerID = "emoteOwner"
					e.GroupID = "123"
					e.Code = "Hello"
					e.EmotesGroup = mkd.EmoteOrigin_Subscriptions.String()
				}),
			},
			emoteModifierGroups: []mako.EmoteModifierGroup{
				makeEmoteModifierGroupFn(func(e *mako.EmoteModifierGroup) {
					e.OwnerID = "emoteOwner"
					e.Modifiers = []string{"BW"}
					e.SourceEmoteGroupIDs = []string{"123", "124"}
				}),
				makeEmoteModifierGroupFn(func(e *mako.EmoteModifierGroup) {
					e.OwnerID = "emoteOwner"
					e.Modifiers = []string{"SG", "HF"}
					e.SourceEmoteGroupIDs = []string{"123", "124", "125"}
				}),
			},
			test: func(ctx context.Context, t *testing.T, test test) {
				emotes, err := mako.GetUserChannelEmotes(ctx, mako.GetUserChannelEmotesParams{
					EmoteDB:          test.EmoteDB,
					EntitlementDB:    test.EntitlementDB,
					EmoteModifiersDB: test.EmoteModifiersDB,
					Dynamic:          test.DynamicConfig,
					UserID:           "ownerID",
					ChannelID:        "notFound",
				})
				require.Nil(t, err)
				require.Equal(t, 1, len(emotes))
				require.NotNil(t, emotes[0].Modifiers)
				require.Equal(t, 1, len(emotes[0].Modifiers))
			},
		},
		{
			scenario: "should not allow a user to see modified animated emotes",
			entitlements: []mako.Entitlement{
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = "123"
					e.OwnerID = "ownerID"
					e.ChannelID = "emoteOwner"
					e.Source = mako.SubscriptionsSource
					e.Type = mako.EmoteGroupEntitlement
				}),
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = "124"
					e.OwnerID = "ownerID"
					e.ChannelID = "emoteOwner"
					e.Source = mako.SubscriptionsSource
					e.Type = mako.EmoteGroupEntitlement
				}),
			},
			emotes: []mako.Emote{
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "999"
					e.OwnerID = "emoteOwner"
					e.GroupID = "123"
					e.Code = "Hello"
					e.EmotesGroup = mkd.EmoteOrigin_Subscriptions.String()
					e.AssetType = mako.AnimatedAssetType
					e.AnimationTemplate = mako.Shake
				}),
			},
			emoteModifierGroups: []mako.EmoteModifierGroup{
				makeEmoteModifierGroupFn(func(e *mako.EmoteModifierGroup) {
					e.OwnerID = "emoteOwner"
					e.Modifiers = []string{"BW"}
					e.SourceEmoteGroupIDs = []string{"123", "124"}
				}),
			},
			test: func(ctx context.Context, t *testing.T, test test) {
				emotes, err := mako.GetUserChannelEmotes(ctx, mako.GetUserChannelEmotesParams{
					EmoteDB:          test.EmoteDB,
					EntitlementDB:    test.EntitlementDB,
					EmoteModifiersDB: test.EmoteModifiersDB,
					Dynamic:          test.DynamicConfig,
					UserID:           "ownerID",
					ChannelID:        "notFound",
				})
				require.Nil(t, err)
				require.Equal(t, 1, len(emotes))
				require.Nil(t, emotes[0].Modifiers)
			},
		},
		{
			scenario: "should not allow a user to see modified emotes for emote types that are not allowed to be modified",
			entitlements: []mako.Entitlement{
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = "123"
					e.OwnerID = "ownerID"
					e.ChannelID = "emoteOwner"
					e.Source = mako.SubscriptionsSource
					e.Type = mako.EmoteGroupEntitlement
				}),
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = "124"
					e.OwnerID = "ownerID"
					e.ChannelID = "emoteOwner"
					e.Source = mako.SubscriptionsSource
					e.Type = mako.EmoteGroupEntitlement
				}),
			},
			emotes: []mako.Emote{
				makeEmoteFn(func(e *mako.Emote) {
					e.ID = "999"
					e.OwnerID = "emoteOwner"
					e.GroupID = "123"
					e.Code = "Hello"
					e.EmotesGroup = mkd.EmoteOrigin_BitsBadgeTierEmotes.String()
				}),
			},
			emoteModifierGroups: []mako.EmoteModifierGroup{
				makeEmoteModifierGroupFn(func(e *mako.EmoteModifierGroup) {
					e.OwnerID = "emoteOwner"
					e.Modifiers = []string{"BW"}
					e.SourceEmoteGroupIDs = []string{"123", "124"}
				}),
			},
			test: func(ctx context.Context, t *testing.T, test test) {
				emotes, err := mako.GetUserChannelEmotes(ctx, mako.GetUserChannelEmotesParams{
					EmoteDB:          test.EmoteDB,
					EntitlementDB:    test.EntitlementDB,
					EmoteModifiersDB: test.EmoteModifiersDB,
					Dynamic:          test.DynamicConfig,
					UserID:           "ownerID",
					ChannelID:        "notFound",
				})
				require.Nil(t, err)
				require.Equal(t, 1, len(emotes))
				require.Nil(t, emotes[0].Modifiers)
			},
		},
	}

	runUserChannelEmotesTests(t, c, tests...)
}

func runUserChannelEmotesTests(t *testing.T, c UserChannelEmotesConfig, tests ...testUserChannelEmotesCase) {
	for _, testCase := range tests {
		testCase := testCase

		t.Run(testCase.scenario, func(t *testing.T) {
			t.Parallel()

			ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
			defer cancel()

			entitlementDB, entitlementTeardown, err := c.MakeEntitlementDB(ctx, testCase.entitlements...)
			if err != nil {
				t.Fatal(err)
			}

			defer entitlementTeardown()

			emoteDB, emoteTeardown, err := c.MakeEmoteDB(ctx, testCase.emotes...)
			if err != nil {
				t.Fatal(err)
			}

			defer emoteTeardown()

			emoteModifiersDB, err := c.MakeEmoteModifierDB(ctx)
			if err != nil {
				t.Fatal(err)
			}

			emoteModifiersDB.CreateGroups(ctx, testCase.emoteModifierGroups...)

			dyanmicConfig := config.NewDynamicConfigFake()
			staticConfig := config.Configuration{
				SpecialEventOwnerIDs:      map[string]bool{},
				CommunityPointsEmoteSetID: "CommunityPointsEmoteSetID",
				CommunityPointsOwnerID:    "CommunityPoints",
			}

			testCase.test(ctx, t, test{
				EmoteDB:          emoteDB,
				EntitlementDB:    entitlementDB,
				EmoteModifiersDB: emoteModifiersDB,
				Emotes:           testCase.emotes,
				Entitlements:     testCase.entitlements,
				DynamicConfig:    dyanmicConfig,
				StaticConfig:     staticConfig,
			})
		})
	}
}
