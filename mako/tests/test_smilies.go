package tests

import (
	"context"
	"testing"
	"time"

	mako "code.justin.tv/commerce/mako/internal"
	"github.com/stretchr/testify/require"
)

func TestSmilies(t *testing.T, makeEntitlementDB MakeEntitlementDB) {
	tests := []struct {
		scenario     string
		entitlements []mako.Entitlement
		test         func(ctx context.Context, t *testing.T, db mako.EntitlementDB, entitlements ...mako.Entitlement)
	}{
		{
			scenario: "should return robots with no smilie entitlement",
			test: func(ctx context.Context, t *testing.T, db mako.EntitlementDB, entitlements ...mako.Entitlement) {
				smilie, err := mako.ReadUserSmilies(ctx, db, "foobar")
				require.Nil(t, err)
				require.Equal(t, smilie, mako.RobotsSmilieGroupID)
			},
		},

		{
			scenario: "should return purple smilie if set",
			entitlements: []mako.Entitlement{
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = mako.SmilieGroupID
					e.Metadata.GroupID = string(mako.PurpleSmilieGroupID)
				}),
			},
			test: func(ctx context.Context, t *testing.T, db mako.EntitlementDB, entitlements ...mako.Entitlement) {
				smilie, err := mako.ReadUserSmilies(ctx, db, entitlements[0].OwnerID)
				require.Nil(t, err)
				require.Equal(t, smilie, mako.PurpleSmilieGroupID)
			},
		},

		{
			scenario: "should return monkeys smilie if set",
			entitlements: []mako.Entitlement{
				makeEntitlementFn(func(e *mako.Entitlement) {
					e.ID = mako.SmilieGroupID
					e.Metadata.GroupID = string(mako.MonkeysSmilieGroupID)
				}),
			},
			test: func(ctx context.Context, t *testing.T, db mako.EntitlementDB, entitlements ...mako.Entitlement) {
				smilie, err := mako.ReadUserSmilies(ctx, db, entitlements[0].OwnerID)
				require.Nil(t, err)
				require.Equal(t, smilie, mako.MonkeysSmilieGroupID)
			},
		},

		{
			scenario: "should return all smilies for get all smilies",
			test: func(ctx context.Context, t *testing.T, db mako.EntitlementDB, entitlements ...mako.Entitlement) {
				smiliesMap := mako.GetAllSmilies()
				require.NotNil(t, smiliesMap[mako.MonkeysSmilieGroupID])
				require.Equal(t, 19, len(smiliesMap[mako.MonkeysSmilieGroupID]))
				require.NotNil(t, smiliesMap[mako.PurpleSmilieGroupID])
				require.Equal(t, 14, len(smiliesMap[mako.PurpleSmilieGroupID]))
				require.NotNil(t, smiliesMap[mako.RobotsSmilieGroupID])
				require.Equal(t, 14, len(smiliesMap[mako.RobotsSmilieGroupID]))
			},
		},
	}

	for _, test := range tests {
		test := test

		t.Run(test.scenario, func(t *testing.T) {
			ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
			defer cancel()

			entDB, entDBTeardown, err := makeEntitlementDB(ctx, test.entitlements...)
			if err != nil {
				t.Fatal(err)
			}

			defer entDBTeardown()

			test.test(ctx, t, entDB, test.entitlements...)
		})
	}
}
