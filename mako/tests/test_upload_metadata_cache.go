package tests

import (
	"context"
	"testing"
	"time"

	mako "code.justin.tv/commerce/mako/internal"
	"github.com/stretchr/testify/assert"
)

// MakeEmoteUploadMetadataCache is used to initialize an `EmoteUploadMetadataCache` for the test suite.
// Implementations can call the test suite and provide an implementation with a way to initialize the cache.
type MakeEmoteUploadMetadataCache = func(ctx context.Context) (mako.EmoteUploadMetadataCache, error)

// TestEmoteUploadMetadataCache is a **test suite** that encapsulates all the behaviour of what an upload metadata cache would have. The test suite
// accepts a `MakeEmoteUploadMetadataCache` lambda that returns an implementation of the `EmoteUploadMetadataCache` interface,
// accepting test data to initialize the implementation.
//
// This means that the test suite is agnostic to a particular implementation. It only knows about a generic interface. This makes
// it possible to re-use the same test suite and a different set of inputs.
func TestEmoteUploadMetadataCache(t *testing.T, makeCache MakeEmoteUploadMetadataCache) {
	tests := []struct {
		scenario string
		test     func(ctx context.Context, t *testing.T, db mako.EmoteUploadMetadataCache)
	}{
		{
			scenario: "if the cache key is not set, then the response is unknown",
			test: func(ctx context.Context, t *testing.T, db mako.EmoteUploadMetadataCache) {
				resp, err := db.ReadUploadMethod(ctx, "some_id")
				assert.Nil(t, err)
				assert.Equal(t, mako.EmoteUploadMethodUnknown, resp)
			},
		},
		{
			scenario: "set the cache key to simple and then read it",
			test: func(ctx context.Context, t *testing.T, db mako.EmoteUploadMetadataCache) {
				uploadID := "some_id"

				err := db.StoreUploadMethod(ctx, uploadID, mako.EmoteUploadMethodSimple)
				assert.Nil(t, err)

				resp, err := db.ReadUploadMethod(ctx, uploadID)
				assert.Nil(t, err)
				assert.Equal(t, mako.EmoteUploadMethodSimple, resp)
			},
		},
		{
			scenario: "set the cache key to advanced and then read it",
			test: func(ctx context.Context, t *testing.T, db mako.EmoteUploadMetadataCache) {
				uploadID := "some_id"

				err := db.StoreUploadMethod(ctx, uploadID, mako.EmoteUploadMethodAdvanced)
				assert.Nil(t, err)

				resp, err := db.ReadUploadMethod(ctx, uploadID)
				assert.Nil(t, err)
				assert.Equal(t, mako.EmoteUploadMethodAdvanced, resp)
			},
		},
		{
			scenario: "set the cache key to some garbage and then read it",
			test: func(ctx context.Context, t *testing.T, db mako.EmoteUploadMetadataCache) {
				uploadID := "some_id"

				err := db.StoreUploadMethod(ctx, uploadID, "this isn't right")
				assert.Nil(t, err)

				resp, err := db.ReadUploadMethod(ctx, uploadID)
				assert.Nil(t, err)
				assert.Equal(t, mako.EmoteUploadMethodUnknown, resp)
			},
		},
	}

	for _, test := range tests {
		test := test

		t.Run(test.scenario, func(t *testing.T) {
			t.Parallel()

			ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
			defer cancel()

			db, err := makeCache(ctx)
			if err != nil {
				t.Fatal(err)
			}

			test.test(ctx, t, db)
		})
	}
}
