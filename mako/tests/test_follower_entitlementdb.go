package tests

import (
	"context"
	"errors"
	"fmt"
	"testing"
	"time"

	config_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/config"

	followsrpc "code.justin.tv/amzn/TwitchVXFollowingServiceECSTwirp"
	"code.justin.tv/commerce/mako/config"
	"code.justin.tv/commerce/mako/integration/user_utils"
	mako "code.justin.tv/commerce/mako/internal"
	follows_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/amzn/TwitchVXFollowingServiceECSTwirp"
	"github.com/segmentio/ksuid"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"github.com/twitchtv/twirp"
)

// MakeFollowingEntitlementDB is used to initialize an `FollowingEntitlementDB` for the test suite. Implementations can call the test suite and provide
// an implementation with a way to initialize seed data.
type MakeFollowingEntitlementDB = func(client followsrpc.TwitchVXFollowingServiceECS, knownOrVerifiedBotsConfig config.KnownOrVerifiedBotsConfigClient, emoteGroupDB mako.EmoteGroupDB) (entitlementDB mako.EntitlementDB)

type FollowingEntitlementDBTestsConfig struct {
	EmoteGroupDB MakeEmoteGroupDB
}

func TestFollowingEntitlementDB(t *testing.T, makeDB MakeEmoteGroupDB, makeEntitlementDB MakeFollowingEntitlementDB) {
	t.Run("TestFollowerEmoteEntitlementDB_ReadByIDs", func(t *testing.T) {
		TestFollowingEntitlementDBReadByIDs(t, makeDB, makeEntitlementDB)
	})

	t.Run("TestFollowerEmoteEntitlementDB_ReadByOwnerID", func(t *testing.T) {
		TestFollowingEntitlementDBReadByOwnerID(t, makeDB, makeEntitlementDB)
	})
}

func TestFollowingEntitlementDBReadByIDs(t *testing.T, makeDB MakeEmoteGroupDB, makeEntitlementDB MakeFollowingEntitlementDB) {
	tests := []struct {
		scenario               string
		emoteGroups            []mako.EmoteGroup
		followResp             *followsrpc.FollowResp
		IsFollowingAllChannels bool
		followErr              error
		test                   func(ctx context.Context, t *testing.T, db mako.EntitlementDB, emoteGroups ...mako.EmoteGroup)
	}{
		{
			scenario: "when no keys are provided, return nothing",
			test: func(ctx context.Context, t *testing.T, db mako.EntitlementDB, emoteGroups ...mako.EmoteGroup) {
				dbEntitlements, err := db.ReadByIDs(ctx, "anything")
				require.Nil(t, dbEntitlements)
				require.Nil(t, err) // Error
			},
		},
		{
			scenario: "when the ID is not properly formatted, return error",
			test: func(ctx context.Context, t *testing.T, db mako.EntitlementDB, emoteGroups ...mako.EmoteGroup) {
				dbEntitlements, err := db.ReadByIDs(ctx, "anything", "anything")
				require.Nil(t, dbEntitlements)
				require.NotNil(t, err)
			},
		},
		{
			scenario: "when more than one ID is provided, return error",
			test: func(ctx context.Context, t *testing.T, db mako.EntitlementDB, emoteGroups ...mako.EmoteGroup) {
				dbEntitlements, err := db.ReadByIDs(ctx, "anything", "anything", "anything2")
				require.Nil(t, dbEntitlements)
				require.NotNil(t, err)
			},
		},
		{
			scenario: "when the userID and channelID are the same, return the entitlement without calling following service",
			emoteGroups: []mako.EmoteGroup{
				{
					GroupID:   ksuid.New().String(),
					OwnerID:   ksuid.New().String(),
					Origin:    mako.EmoteGroupOriginFollower,
					GroupType: mako.EmoteGroupStatic,
					CreatedAt: time.Now().Add(-1 * time.Hour),
				},
			},
			test: func(ctx context.Context, t *testing.T, db mako.EntitlementDB, emoteGroups ...mako.EmoteGroup) {
				id := mako.GenerateFollowerEntitlementID(emoteGroups[0].GroupID, emoteGroups[0].OwnerID)
				dbEntitlements, err := db.ReadByIDs(ctx, emoteGroups[0].OwnerID, id)
				require.NotNil(t, dbEntitlements)
				require.Nil(t, err)
				require.Equal(t, emoteGroups[0].OwnerID, dbEntitlements[0].ChannelID)
				require.Equal(t, id, dbEntitlements[0].ID)
				require.Equal(t, emoteGroups[0].OwnerID, dbEntitlements[0].OwnerID)
			},
		},
		{
			scenario: "when the userID and channelID are both prefixed by the special integration test prefix, return the entitlement without calling following service",
			emoteGroups: []mako.EmoteGroup{
				{
					GroupID:   ksuid.New().String(),
					OwnerID:   fmt.Sprintf("%s%s", user_utils.IntegrationTestUserPrefix, ksuid.New().String()),
					Origin:    mako.EmoteGroupOriginFollower,
					GroupType: mako.EmoteGroupStatic,
					CreatedAt: time.Now().Add(-1 * time.Hour),
				},
			},
			test: func(ctx context.Context, t *testing.T, db mako.EntitlementDB, emoteGroups ...mako.EmoteGroup) {
				userID := fmt.Sprintf("%s%s", user_utils.IntegrationTestUserPrefix, ksuid.New().String())
				id := mako.GenerateFollowerEntitlementID(emoteGroups[0].GroupID, emoteGroups[0].OwnerID)
				dbEntitlements, err := db.ReadByIDs(ctx, userID, id)
				require.NotNil(t, dbEntitlements)
				require.Nil(t, err)
				require.Equal(t, emoteGroups[0].OwnerID, dbEntitlements[0].ChannelID)
				require.Equal(t, id, dbEntitlements[0].ID)
				require.Equal(t, userID, dbEntitlements[0].OwnerID)
			},
		},
		{
			scenario: "when the userID is one of the 3rd party bots which is considered to be following all channels, return the entitlement without calling following service",
			emoteGroups: []mako.EmoteGroup{
				{
					GroupID:   ksuid.New().String(),
					OwnerID:   fmt.Sprintf("%s%s", user_utils.IntegrationTestUserPrefix, ksuid.New().String()),
					Origin:    mako.EmoteGroupOriginFollower,
					GroupType: mako.EmoteGroupStatic,
					CreatedAt: time.Now().Add(-1 * time.Hour),
				},
			},
			IsFollowingAllChannels: true,
			test: func(ctx context.Context, t *testing.T, db mako.EntitlementDB, emoteGroups ...mako.EmoteGroup) {
				userID := ksuid.New().String()
				id := mako.GenerateFollowerEntitlementID(emoteGroups[0].GroupID, emoteGroups[0].OwnerID)
				dbEntitlements, err := db.ReadByIDs(ctx, userID, id)
				require.NotNil(t, dbEntitlements)
				require.Nil(t, err)
				require.Equal(t, emoteGroups[0].OwnerID, dbEntitlements[0].ChannelID)
				require.Equal(t, id, dbEntitlements[0].ID)
				require.Equal(t, userID, dbEntitlements[0].OwnerID)
			},
		},
		{
			scenario: "when there is no following service response nor error, return nothing",
			test: func(ctx context.Context, t *testing.T, db mako.EntitlementDB, emoteGroups ...mako.EmoteGroup) {
				dbEntitlements, err := db.ReadByIDs(ctx, "anything", mako.GenerateFollowerEntitlementID("groupID", "channelID"))
				require.Nil(t, dbEntitlements)
				require.Nil(t, err)
			},
		},
		{
			scenario:  "when following service fails with a non-twirp error, return error",
			followErr: errors.New("test error"),
			test: func(ctx context.Context, t *testing.T, db mako.EntitlementDB, emoteGroups ...mako.EmoteGroup) {
				dbEntitlements, err := db.ReadByIDs(ctx, "anything", mako.GenerateFollowerEntitlementID("groupID", "channelID"))
				require.Nil(t, dbEntitlements)
				require.NotNil(t, err)
			},
		},
		{
			scenario:  "when following service fails with an unexpected twirp error, return error",
			followErr: twirp.InternalError("test error"),
			test: func(ctx context.Context, t *testing.T, db mako.EntitlementDB, emoteGroups ...mako.EmoteGroup) {
				dbEntitlements, err := db.ReadByIDs(ctx, "anything", mako.GenerateFollowerEntitlementID("groupID", "channelID"))
				require.Nil(t, dbEntitlements)
				require.NotNil(t, err)
			},
		},
		{
			scenario:  "when following service fails with an expected not_found error, return nothiing",
			followErr: twirp.NotFoundError("test error"),
			test: func(ctx context.Context, t *testing.T, db mako.EntitlementDB, emoteGroups ...mako.EmoteGroup) {
				dbEntitlements, err := db.ReadByIDs(ctx, "anything", mako.GenerateFollowerEntitlementID("groupID", "channelID"))
				require.Nil(t, dbEntitlements)
				require.Nil(t, err)
			},
		},
		{
			scenario: "when following service returns a successful response",
			followResp: &followsrpc.FollowResp{
				FromUserId:   "userID",
				TargetUserId: "channelID",
				FollowedAt:   "2016-06-28T20:54:33Z",
			},
			emoteGroups: []mako.EmoteGroup{
				{
					GroupID:   ksuid.New().String(),
					OwnerID:   "channelID",
					Origin:    mako.EmoteGroupOriginFollower,
					GroupType: mako.EmoteGroupStatic,
					CreatedAt: time.Now().Add(-1 * time.Hour),
				},
			},
			test: func(ctx context.Context, t *testing.T, db mako.EntitlementDB, emoteGroups ...mako.EmoteGroup) {
				id := mako.GenerateFollowerEntitlementID(emoteGroups[0].GroupID, emoteGroups[0].OwnerID)
				dbEntitlements, err := db.ReadByIDs(ctx, "userID", id)
				require.NotNil(t, dbEntitlements)
				require.Nil(t, err)
				require.Equal(t, emoteGroups[0].OwnerID, dbEntitlements[0].ChannelID)
				require.Equal(t, id, dbEntitlements[0].ID)
				require.Equal(t, "userID", dbEntitlements[0].OwnerID)
			},
		},
	}

	for _, test := range tests {
		test := test

		t.Run(test.scenario, func(t *testing.T) {
			t.Parallel()

			mockFollowsTwirp := new(follows_mock.TwitchVXFollowingServiceECS)
			mockKnownOrVerifiedBotsConfig := new(config_mock.KnownOrVerifiedBotsConfigClient)

			// Loading into local dynamo can sometimes take a while when many tests are all vying for the same resource
			// at the same time, so we provide a longer context timeout for loading into the DB and then a shorter one
			// for actually running the test.
			ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)

			emoteGroupDB, teardown, err := makeDB(ctx, test.emoteGroups...)
			if err != nil {
				t.Fatal(err)
			}
			cancel()
			ctx, cancel = context.WithTimeout(context.Background(), 3*time.Second)

			db := makeEntitlementDB(mockFollowsTwirp, mockKnownOrVerifiedBotsConfig, emoteGroupDB)
			mockFollowsTwirp.On("GetFollow", mock.Anything, mock.Anything).Return(test.followResp, test.followErr)
			mockKnownOrVerifiedBotsConfig.On("IsKnownOrVerifiedBot", mock.Anything).Return(test.IsFollowingAllChannels)

			test.test(ctx, t, db, test.emoteGroups...)
			cancel()
			teardown()
		})
	}
}

func TestFollowingEntitlementDBReadByOwnerID(t *testing.T, makeDB MakeEmoteGroupDB, makeEntitlementDB MakeFollowingEntitlementDB) {
	tests := []struct {
		scenario               string
		emoteGroups            []mako.EmoteGroup
		followResp             *followsrpc.FollowResp
		IsFollowingAllChannels bool
		followErr              error
		test                   func(ctx context.Context, t *testing.T, db mako.EntitlementDB, emoteGroups ...mako.EmoteGroup)
	}{
		{
			scenario: "when no ownerID is provided",
			test: func(ctx context.Context, t *testing.T, db mako.EntitlementDB, emoteGroups ...mako.EmoteGroup) {
				dbEntitlements, err := db.ReadByOwnerID(ctx, "", "channelID")
				require.Nil(t, dbEntitlements)
				require.Nil(t, err)
			},
		},
		{
			scenario: "when no channelID is provided",
			test: func(ctx context.Context, t *testing.T, db mako.EntitlementDB, emoteGroups ...mako.EmoteGroup) {
				dbEntitlements, err := db.ReadByOwnerID(ctx, "ownerID", "")
				require.Nil(t, dbEntitlements)
				require.Nil(t, err)
			},
		},
		{
			scenario: "when the channelID and ownerID are the same, and there is no emote group in the channel, return nothing",
			test: func(ctx context.Context, t *testing.T, db mako.EntitlementDB, emoteGroups ...mako.EmoteGroup) {
				dbEntitlements, err := db.ReadByOwnerID(ctx, "channelID", "channelID")
				require.Nil(t, dbEntitlements)
				require.Nil(t, err)
			},
		},
		{
			scenario: "when the channelID and ownerID are the same, and there is an emote group in the channel, return the entitlement",
			emoteGroups: []mako.EmoteGroup{
				{
					GroupID:   ksuid.New().String(),
					OwnerID:   ksuid.New().String(),
					Origin:    mako.EmoteGroupOriginFollower,
					GroupType: mako.EmoteGroupStatic,
					CreatedAt: time.Now().Add(-1 * time.Hour),
				},
			},
			test: func(ctx context.Context, t *testing.T, db mako.EntitlementDB, emoteGroups ...mako.EmoteGroup) {
				dbEntitlements, err := db.ReadByOwnerID(ctx, emoteGroups[0].OwnerID, emoteGroups[0].OwnerID)
				require.NotNil(t, dbEntitlements)
				require.Nil(t, err)
				require.Equal(t, len(emoteGroups), len(dbEntitlements))
				require.Equal(t, emoteGroups[0].OwnerID, dbEntitlements[0].OwnerID)
				require.Equal(t, emoteGroups[0].OwnerID, dbEntitlements[0].ChannelID)
				require.Equal(t, emoteGroups[0].GroupID, dbEntitlements[0].ID)
			},
		},
		{
			scenario: "when the userID is one of the 3rd party bots which is considered to be following all channels, return the entitlement without calling following service",
			emoteGroups: []mako.EmoteGroup{
				{
					GroupID:   ksuid.New().String(),
					OwnerID:   fmt.Sprintf("%s%s", user_utils.IntegrationTestUserPrefix, ksuid.New().String()),
					Origin:    mako.EmoteGroupOriginFollower,
					GroupType: mako.EmoteGroupStatic,
					CreatedAt: time.Now().Add(-1 * time.Hour),
				},
			},
			IsFollowingAllChannels: true,
			test: func(ctx context.Context, t *testing.T, db mako.EntitlementDB, emoteGroups ...mako.EmoteGroup) {
				dbEntitlements, err := db.ReadByOwnerID(ctx, emoteGroups[0].OwnerID, emoteGroups[0].OwnerID)
				require.NotNil(t, dbEntitlements)
				require.Nil(t, err)
				require.Equal(t, len(emoteGroups), len(dbEntitlements))
				require.Equal(t, emoteGroups[0].OwnerID, dbEntitlements[0].OwnerID)
				require.Equal(t, emoteGroups[0].OwnerID, dbEntitlements[0].ChannelID)
				require.Equal(t, emoteGroups[0].GroupID, dbEntitlements[0].ID)
			},
		},
		{
			scenario: "when there is no following service response nor error, return nothing",
			test: func(ctx context.Context, t *testing.T, db mako.EntitlementDB, emoteGroups ...mako.EmoteGroup) {
				dbEntitlements, err := db.ReadByOwnerID(ctx, "ownerID", "channelID")
				require.Nil(t, dbEntitlements)
				require.Nil(t, err)
			},
		},
		{
			scenario:  "when following service fails, return error",
			followErr: errors.New("test error"),
			test: func(ctx context.Context, t *testing.T, db mako.EntitlementDB, emoteGroups ...mako.EmoteGroup) {
				dbEntitlements, err := db.ReadByOwnerID(ctx, "ownerID", "channelID")
				require.Nil(t, dbEntitlements)
				require.NotNil(t, err)
			},
		},
		{
			scenario:  "when following service fails with an unexpected twirp error, return error",
			followErr: twirp.InternalError("test error"),
			test: func(ctx context.Context, t *testing.T, db mako.EntitlementDB, emoteGroups ...mako.EmoteGroup) {
				dbEntitlements, err := db.ReadByOwnerID(ctx, "anything", "channelID")
				require.Nil(t, dbEntitlements)
				require.NotNil(t, err)
			},
		},
		{
			scenario:  "when following service fails with an expected not_found error, return nothiing",
			followErr: twirp.NotFoundError("test error"),
			test: func(ctx context.Context, t *testing.T, db mako.EntitlementDB, emoteGroups ...mako.EmoteGroup) {
				dbEntitlements, err := db.ReadByOwnerID(ctx, "anything", "channelID")
				require.Nil(t, dbEntitlements)
				require.Nil(t, err)
			},
		},
		{
			scenario: "when there is no emote group for this channel, return nothing",
			followResp: &followsrpc.FollowResp{
				FromUserId:   "ownerID",
				TargetUserId: "channelID",
				FollowedAt:   "2016-06-28T20:54:33Z",
			},
			test: func(ctx context.Context, t *testing.T, db mako.EntitlementDB, emoteGroups ...mako.EmoteGroup) {
				dbEntitlements, err := db.ReadByOwnerID(ctx, "ownerID", "channelID")
				require.Nil(t, dbEntitlements)
				require.Nil(t, err)
			},
		},
		{
			scenario: "when there is an emote group in the channel, return the entitlement",
			emoteGroups: []mako.EmoteGroup{
				{
					GroupID:   ksuid.New().String(),
					OwnerID:   "channelID",
					Origin:    mako.EmoteGroupOriginFollower,
					GroupType: mako.EmoteGroupStatic,
					CreatedAt: time.Now().Add(-1 * time.Hour),
				},
			},
			followResp: &followsrpc.FollowResp{
				FromUserId:   "userID",
				TargetUserId: "channelID",
				FollowedAt:   "2016-06-28T20:54:33Z",
			},
			test: func(ctx context.Context, t *testing.T, db mako.EntitlementDB, emoteGroups ...mako.EmoteGroup) {
				dbEntitlements, err := db.ReadByOwnerID(ctx, "userID", "channelID")
				require.Nil(t, err)
				require.NotNil(t, dbEntitlements)
				require.Equal(t, len(emoteGroups), len(dbEntitlements))
				require.Equal(t, "userID", dbEntitlements[0].OwnerID)
				require.Equal(t, emoteGroups[0].OwnerID, dbEntitlements[0].ChannelID)
				require.Equal(t, emoteGroups[0].GroupID, dbEntitlements[0].ID)
			},
		},
	}

	for _, test := range tests {
		test := test

		t.Run(test.scenario, func(t *testing.T) {
			t.Parallel()

			mockFollowsTwirp := new(follows_mock.TwitchVXFollowingServiceECS)
			mockKnownOrVerifiedBotsConfig := new(config_mock.KnownOrVerifiedBotsConfigClient)

			// Loading into local dynamo can sometimes take a while when many tests are all vying for the same resource
			// at the same time, so we provide a longer context timeout for loading into the DB and then a shorter one
			// for actually running the test.
			ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)

			emoteGroupDB, teardown, err := makeDB(ctx, test.emoteGroups...)
			if err != nil {
				t.Fatal(err)
			}
			cancel()
			ctx, cancel = context.WithTimeout(context.Background(), 3*time.Second)

			db := makeEntitlementDB(mockFollowsTwirp, mockKnownOrVerifiedBotsConfig, emoteGroupDB)
			mockFollowsTwirp.On("GetFollow", mock.Anything, mock.Anything).Return(test.followResp, test.followErr)
			mockKnownOrVerifiedBotsConfig.On("IsKnownOrVerifiedBot", mock.Anything).Return(test.IsFollowingAllChannels)

			test.test(ctx, t, db, test.emoteGroups...)
			cancel()
			teardown()
		})
	}
}
