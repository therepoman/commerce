package tests

import (
	"context"
	"testing"
	"time"

	mako "code.justin.tv/commerce/mako/internal"
	"github.com/segmentio/ksuid"
	"github.com/stretchr/testify/require"
)

type MakeEmoteModifiersDB = func(ctx context.Context) (mako.EmoteModifiersDB, error)

// This generates random test data.
func makeEmoteModifierGroup() mako.EmoteModifierGroup {
	return mako.EmoteModifierGroup{
		ID:                  ksuid.New().String(),
		OwnerID:             ksuid.New().String(),
		Modifiers:           []string{},
		SourceEmoteGroupIDs: []string{ksuid.New().String()},
	}
}

func makeEmoteModifierGroupFn(f func(*mako.EmoteModifierGroup)) mako.EmoteModifierGroup {
	emg := makeEmoteModifierGroup()
	f(&emg)
	return emg
}

func assertEmoteModGroup(t *testing.T, a, b mako.EmoteModifierGroup) {
	t.Helper()

	require.Equal(t, a.ID, b.ID, "ID should match")
	require.Equal(t, a.OwnerID, b.OwnerID, "OwnerID should match")
	require.Equal(t, a.Modifiers, b.Modifiers, "Modifiers should match")
	require.Equal(t, a.SourceEmoteGroupIDs, b.SourceEmoteGroupIDs, "SourceEmoteGroupIDs should match")
}

func TestEmoteModifiersDB(t *testing.T, makeDB MakeEmoteModifiersDB) {
	tests := []struct {
		scenario string
		groups   []mako.EmoteModifierGroup
		test     func(ctx context.Context, t *testing.T, db mako.EmoteModifiersDB, groups ...mako.EmoteModifierGroup)
	}{
		{
			scenario: "omitting groups during create returns empty list",
			groups:   []mako.EmoteModifierGroup{},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteModifiersDB, groups ...mako.EmoteModifierGroup) {
				// GIVEN: empty groups list
				require.Empty(t, groups)

				// WHEN:
				created, err := db.CreateGroups(ctx, groups...)

				// THEN:
				require.NotNil(t, created)
				require.Empty(t, created)
				require.NoError(t, err)
			},
		},
		{
			scenario: "omitting groups during update returns empty list",
			groups:   []mako.EmoteModifierGroup{},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteModifiersDB, groups ...mako.EmoteModifierGroup) {
				// GIVEN: empty groups list
				require.Empty(t, groups)

				// WHEN:
				created, err := db.UpdateGroups(ctx, groups...)

				// THEN:
				require.NotNil(t, created)
				require.Empty(t, created)
				require.NoError(t, err)
			},
		},
		{
			scenario: "omitting groupIDs during read results in error",
			groups:   []mako.EmoteModifierGroup{},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteModifiersDB, groups ...mako.EmoteModifierGroup) {
				// GIVEN: empty groups list
				ids := make([]string, 0)

				// WHEN:
				created, err := db.ReadByIDs(ctx, ids...)

				// THEN:
				require.Empty(t, created)
				require.Error(t, err)
			},
		},
		{
			scenario: "omitting ownerIDs during read results in empty response",
			groups:   []mako.EmoteModifierGroup{},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteModifiersDB, groups ...mako.EmoteModifierGroup) {
				// GIVEN: empty ownerID list
				ids := make([]string, 0)

				// WHEN:
				created, err := db.ReadByOwnerIDs(ctx, ids...)

				// THEN:
				require.Empty(t, created)
				require.Error(t, err)
			},
		},
		{
			scenario: "creating a group without an ID results in an error",
			groups: []mako.EmoteModifierGroup{
				makeEmoteModifierGroupFn(func(e *mako.EmoteModifierGroup) {
					e.ID = ""
					e.Modifiers = []string{"BW", "HF"}
				}),
			},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteModifiersDB, groups ...mako.EmoteModifierGroup) {
				// GIVEN: a group with an empty ID
				require.Empty(t, groups[0].ID)

				// WHEN: create the groups
				created, err := db.CreateGroups(ctx, groups...)

				// THEN:
				require.Nil(t, created)
				require.Error(t, err)
			},
		},
		{
			scenario: "should create emote mod groups and assign them IDs",
			groups: []mako.EmoteModifierGroup{
				makeEmoteModifierGroupFn(func(e *mako.EmoteModifierGroup) {
					e.Modifiers = []string{"BW", "HF"}
				}),
			},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteModifiersDB, groups ...mako.EmoteModifierGroup) {
				// GIVEN: a group with an ID
				require.NotEmpty(t, groups[0].ID)

				// WHEN: create the groups
				created, err := db.CreateGroups(ctx, groups...)

				// THEN: group IDs are populated.
				require.NoError(t, err)
				require.Len(t, created, 1)
				assertEmoteModGroup(t, groups[0], created[0])

				// Read them and make sure they were persisted.
				found, err := db.ReadByIDs(ctx, created[0].ID)
				require.NoError(t, err)
				require.Equal(t, len(created), len(found))
				// assumption here is there is only 1 group in the input list
				assertEmoteModGroup(t, created[0], found[0])
			},
		},
		{
			scenario: "Creating multiple mod groups at the same time works as expected",
			groups: []mako.EmoteModifierGroup{
				makeEmoteModifierGroupFn(func(e *mako.EmoteModifierGroup) {
					e.Modifiers = []string{"SG", "HF"}
				}),
				makeEmoteModifierGroupFn(func(e *mako.EmoteModifierGroup) {
					e.Modifiers = []string{"BW"}
				}),
			},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteModifiersDB, groups ...mako.EmoteModifierGroup) {
				// GIVEN: more than one group
				require.Len(t, groups, 2)

				// WHEN: create the groups
				created, err := db.CreateGroups(ctx, groups...)

				// THEN: the correct number were created.
				require.NoError(t, err)
				require.Len(t, created, 2)

				// Read them and make sure they were persisted.
				found, err := db.ReadByIDs(ctx, created[0].ID, created[1].ID)
				require.NoError(t, err)
				require.Len(t, found, 2)
			},
		},
		{
			scenario: "Updating a group changes dynamo entries as expected",
			groups: []mako.EmoteModifierGroup{
				makeEmoteModifierGroupFn(func(e *mako.EmoteModifierGroup) {
					e.Modifiers = []string{"HF", "BW"}
				}),
			},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteModifiersDB, groups ...mako.EmoteModifierGroup) {
				// GIVEN: a valid group is inserted in the DB
				created, err := db.CreateGroups(ctx, groups...)
				require.NoError(t, err)
				require.Len(t, created, 1)

				// WHEN: updating the fields on the group
				group := makeEmoteModifierGroupFn(func(e *mako.EmoteModifierGroup) {
					e.ID = created[0].ID
					e.Modifiers = []string{"SG"}
				})
				updated, err := db.UpdateGroups(ctx, group)

				// THEN: no error is returned, and the item is correctly updated.
				require.NoError(t, err)
				require.Len(t, updated, 1)
				found, err := db.ReadByIDs(ctx, created[0].ID)
				require.NoError(t, err)
				require.Len(t, found, 1)
				assertEmoteModGroup(t, updated[0], found[0])
			},
		},
		{
			scenario: "Reading group entries by ownerIDs as expected",
			groups: []mako.EmoteModifierGroup{
				makeEmoteModifierGroupFn(func(e *mako.EmoteModifierGroup) {
					e.Modifiers = []string{"HF", "BW"}
				}),
			},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteModifiersDB, groups ...mako.EmoteModifierGroup) {
				// GIVEN: valid groups are inserted in the DB
				created, err := db.CreateGroups(ctx, groups...)
				require.NoError(t, err)
				require.Len(t, created, 1)

				time.Sleep(2 * time.Second)

				// WHEN:
				found, err := db.ReadByOwnerIDs(ctx, created[0].OwnerID)

				// THEN: no error is returned, and the items are correctly returned.
				require.NoError(t, err)
				require.Len(t, found, 1)
				assertEmoteModGroup(t, created[0], found[0])
			},
		},
		{
			scenario: "Omitting OwnerID when updating group leaves OwnerID unchanged",
			groups: []mako.EmoteModifierGroup{
				makeEmoteModifierGroupFn(func(e *mako.EmoteModifierGroup) {
					e.Modifiers = []string{"HF", "BW"}
				}),
			},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteModifiersDB, groups ...mako.EmoteModifierGroup) {
				// GIVEN: a valid group is inserted in the DB
				created, err := db.CreateGroups(ctx, groups...)
				require.NoError(t, err)
				require.Len(t, created, 1)

				// WHEN: updating the fields on the group
				group := makeEmoteModifierGroupFn(func(e *mako.EmoteModifierGroup) {
					e.ID = created[0].ID
					e.Modifiers = []string{"SG"}
				})
				group.OwnerID = ""
				updated, err := db.UpdateGroups(ctx, group)

				// THEN: no error is returned, and the item is correctly updated.
				require.NoError(t, err)
				found, err := db.ReadByIDs(ctx, created[0].ID)
				require.NoError(t, err)
				require.Len(t, found, 1)
				assertEmoteModGroup(t, updated[0], found[0])
				require.Equal(t, created[0].OwnerID, updated[0].OwnerID)
			},
		},
		{
			scenario: "Omitting SourceEmoteGroupIDs when updating group leaves SourceEmoteGroupIDs unchanged",
			groups: []mako.EmoteModifierGroup{
				makeEmoteModifierGroupFn(func(e *mako.EmoteModifierGroup) {
					e.Modifiers = []string{"HF", "BW"}
				}),
			},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteModifiersDB, groups ...mako.EmoteModifierGroup) {
				// GIVEN: a valid group is inserted in the DB
				created, err := db.CreateGroups(ctx, groups...)
				require.NoError(t, err)
				require.Len(t, created, 1)

				// WHEN: updating the fields on the group
				group := makeEmoteModifierGroupFn(func(e *mako.EmoteModifierGroup) {
					e.ID = created[0].ID
					e.Modifiers = []string{"TK"}
				})
				group.SourceEmoteGroupIDs = []string{}
				updated, err := db.UpdateGroups(ctx, group)

				// THEN: no error is returned, and the item is correctly updated.
				require.NoError(t, err)
				found, err := db.ReadByIDs(ctx, created[0].ID)
				require.NoError(t, err)
				require.Len(t, found, 1)
				assertEmoteModGroup(t, updated[0], found[0])
				require.Equal(t, created[0].SourceEmoteGroupIDs, updated[0].SourceEmoteGroupIDs)
			},
		},
		{
			scenario: "Omitting Modifiers when updating group removes modifiers from that group",
			groups: []mako.EmoteModifierGroup{
				makeEmoteModifierGroupFn(func(e *mako.EmoteModifierGroup) {
					e.Modifiers = []string{"HF", "BW"}
				}),
			},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteModifiersDB, groups ...mako.EmoteModifierGroup) {
				// GIVEN: a valid group is inserted in the DB
				created, err := db.CreateGroups(ctx, groups...)
				require.NoError(t, err)
				require.Len(t, created, 1)

				// WHEN: updating the fields on the group
				group := makeEmoteModifierGroupFn(func(e *mako.EmoteModifierGroup) {
					e.ID = created[0].ID
				})
				updated, err := db.UpdateGroups(ctx, group)

				// THEN: no error is returned, and the item is correctly updated.
				require.NoError(t, err)
				require.Len(t, updated[0].Modifiers, 0)
				found, err := db.ReadByIDs(ctx, created[0].ID)
				require.NoError(t, err)
				require.Len(t, found, 1)
				assertEmoteModGroup(t, updated[0], found[0])
			},
		},
		{
			scenario: "Attempting to update a group with no ID returns an error",
			groups: []mako.EmoteModifierGroup{
				makeEmoteModifierGroupFn(func(e *mako.EmoteModifierGroup) {
					e.ID = ""
					e.Modifiers = []string{"HF", "BW"}
				}),
			},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteModifiersDB, groups ...mako.EmoteModifierGroup) {
				// GIVEN:
				require.Empty(t, groups[0].ID)

				// WHEN:
				updated, err := db.UpdateGroups(ctx, groups...)

				// THEN:
				require.Nil(t, updated)
				require.Error(t, err)
			},
		},
		{
			scenario: "Deleting 0 rows returns an error",
			groups:   []mako.EmoteModifierGroup{},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteModifiersDB, groups ...mako.EmoteModifierGroup) {
				// GIVEN:
				ids := make([]string, 0)

				// WHEN:
				err := db.DeleteByIDs(ctx, ids...)

				// THEN:
				require.Error(t, err)
			},
		},
		{
			scenario: "Deleting multiple rows returns no error",
			groups: []mako.EmoteModifierGroup{
				makeEmoteModifierGroupFn(func(e *mako.EmoteModifierGroup) {
					e.Modifiers = []string{"BW"}
				}),
				makeEmoteModifierGroupFn(func(e *mako.EmoteModifierGroup) {
					e.Modifiers = []string{"SG"}
				}),
			},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteModifiersDB, groups ...mako.EmoteModifierGroup) {
				// GIVEN: more than one group exists in the db
				require.Len(t, groups, 2)
				created, err := db.CreateGroups(ctx, groups...)
				require.NoError(t, err)
				require.Len(t, created, 2)

				// WHEN: we delete them from the DB
				err = db.DeleteByIDs(ctx, created[0].ID, created[1].ID)

				// THEN: no error is returned
				require.NoError(t, err)
			},
		},
	}

	for _, test := range tests {
		test := test

		t.Run(test.scenario, func(t *testing.T) {
			t.Parallel()

			ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
			defer cancel()

			db, err := makeDB(ctx)
			if err != nil {
				t.Fatal(err)
			}

			test.test(ctx, t, db, test.groups...)
		})
	}
}

func TestEmoteModifiersDBCache(t *testing.T, makeDB MakeEmoteModifiersDB) {
	testUserID := "123"
	tests := []struct {
		scenario string
		groups   []mako.EmoteModifierGroup
		test     func(ctx context.Context, t *testing.T, db mako.EmoteModifiersDB, groups ...mako.EmoteModifierGroup)
	}{
		{
			scenario: "returns cached groups",
			groups: []mako.EmoteModifierGroup{
				{
					ID:                  "test-dummy-id",
					OwnerID:             "127380717",
					Modifiers:           nil,
					SourceEmoteGroupIDs: []string{"127380717"},
				},
			},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteModifiersDB, groups ...mako.EmoteModifierGroup) {
				// GIVEN: a non-empty groups list that is not cached
				created, err := db.CreateGroups(ctx, groups...)
				require.NoError(t, err)
				require.Len(t, created, 1)

				// WHEN: cached
				resultGroups, err := db.ReadByIDs(ctx, "test-dummy-id")
				require.NotNil(t, resultGroups)
				require.NoError(t, err)
				resultGroups, err = db.ReadByIDs(ctx, "test-dummy-id")

				// THEN:
				require.NotNil(t, resultGroups)
				require.NoError(t, err)
			},
		},
		{
			scenario: "returns nil when entitlements are not cached",
			groups: []mako.EmoteModifierGroup{
				{
					ID:                  "test-dummy-id",
					OwnerID:             "127380717",
					Modifiers:           nil,
					SourceEmoteGroupIDs: []string{"127380717"},
				},
			},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteModifiersDB, groups ...mako.EmoteModifierGroup) {
				// GIVEN: a non-empty groups list that is not cached
				require.NotEmpty(t, groups)

				// WHEN:
				resultGroups, err := db.ReadEntitledGroupsForUser(ctx, testUserID)

				// THEN:
				require.Nil(t, resultGroups)
				require.Nil(t, err)
			},
		},
		{
			scenario: "returns result when entitlements are cached",
			groups: []mako.EmoteModifierGroup{
				{
					ID:                  "test-dummy-id",
					OwnerID:             "127380717",
					Modifiers:           nil,
					SourceEmoteGroupIDs: []string{"127380717"},
				},
			},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteModifiersDB, groups ...mako.EmoteModifierGroup) {
				// GIVEN: groups are added to cache
				err := db.WriteEntitledGroupsForUser(ctx, testUserID, groups...)
				require.NoError(t, err)

				// WHEN:
				resultGroups, err := db.ReadEntitledGroupsForUser(ctx, testUserID)

				// THEN:
				require.NotNil(t, resultGroups)
				require.Equal(t, resultGroups, groups)
				require.NoError(t, err)
			},
		},
		{
			scenario: "invalidates cache",
			groups: []mako.EmoteModifierGroup{
				{
					ID:                  "test-dummy-id",
					OwnerID:             "127380717",
					Modifiers:           nil,
					SourceEmoteGroupIDs: []string{"127380717"},
				},
			},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteModifiersDB, groups ...mako.EmoteModifierGroup) {
				// GIVEN: groups are added to cache
				err := db.WriteEntitledGroupsForUser(ctx, testUserID, groups...)
				require.NoError(t, err)

				// WHEN: cache is invalidated
				err = db.InvalidateEntitledGroupsCacheForUser(testUserID)

				// THEN:
				require.NoError(t, err)
			},
		},
		{
			scenario: "returns nothing when groups is empty",
			groups:   []mako.EmoteModifierGroup{},
			test: func(ctx context.Context, t *testing.T, db mako.EmoteModifiersDB, groups ...mako.EmoteModifierGroup) {
				// GIVEN: groups are added to cache
				err := db.WriteEntitledGroupsForUser(ctx, testUserID, groups...)
				require.NoError(t, err)
			},
		},
	}

	for _, test := range tests {
		test := test

		t.Run(test.scenario, func(t *testing.T) {
			t.Parallel()

			ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
			defer cancel()

			db, err := makeDB(ctx)
			if err != nil {
				t.Fatal(err)
			}

			test.test(ctx, t, db, test.groups...)
		})
	}
}
