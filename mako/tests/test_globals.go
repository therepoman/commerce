package tests

import (
	"context"
	"testing"
	"time"

	makotwirp "code.justin.tv/commerce/mako/twirp"
	"github.com/stretchr/testify/require"
)

// TODO: Remove once Mobile SDK is fixed and no longer depends on regexes. See https://docs.google.com/document/d/11KaSXISaXgSdsryxfGDErKpoN67EHhTwwnrJKe2crWU/edit#
// This is copied from materia logic because of import cycles.
var normalizedSmilies = map[string]string{
	":)":   "\\:-?\\)",
	":-)":  "\\:-?\\)",
	":(":   "\\:-?\\(",
	":-(":  "\\:-?\\(",
	":D":   ":D",
	":-D":  ":D",
	">(":   ">(",
	":|":   ":|",
	":-|":  ":|",
	":z":   ":|",
	":-z":  ":|",
	":Z":   ":|",
	":-Z":  ":|",
	"O_o":  "O_o",
	"O.o":  "O_o",
	"O_O":  "O_o",
	"O.O":  "O_o",
	"o_O":  "O_o",
	"o.O":  "O_o",
	"o_o":  "O_o",
	"o.o":  "O_o",
	"B)":   "B-?\\)",
	"B-)":  "B-?\\)",
	"8-)":  "B-?\\)",
	":O":   ":O",
	":-O":  ":O",
	":o":   ":O",
	":-o":  ":O",
	"<3":   "<3",
	":/":   ":\\",
	":-/":  ":\\",
	":\\":  ":\\",
	":-\\": ":\\",
	";)":   ";)",
	";-)":  ";)",
	":P":   ":p",
	":-P":  ":p",
	":p":   ":p",
	":-p":  ":p",
	";P":   ";p",
	";-P":  ";p",
	";p":   ";p",
	";-p":  ";p",
	"R)":   "R)",
	"R-)":  "R)",
	"#/":   "#/",
	"#-/":  "#/",
	"#\\":  "#/",
	"#-\\": "#/",
	":>":   ":>",
	"<]":   "<]",
	":7":   ":7",
	":-7":  ":7",
	":L":   ":7",
	":-L":  ":7",
	":S":   ":S",
	":-S":  ":S",
	":s":   ":S",
	":-s":  ":S",
}

func TestGlobals(t *testing.T, makeClient MakeClient) {
	ownerID := "262616472"

	tests := []struct {
		scenario string
		test     func(ctx context.Context, t *testing.T, mako makotwirp.Mako)
	}{
		{
			scenario: "emote picker: normal globals",
			test: func(ctx context.Context, t *testing.T, mako makotwirp.Mako) {
				globals, err := mako.GetEmoticonsByGroups(ctx, &makotwirp.GetEmoticonsByGroupsRequest{
					EmoticonGroupKeys: []string{"0"},
				})

				require.Nil(t, err)

				var codes []string
				for _, group := range globals.Groups {
					if group.Id != "0" {
						continue
					}

					for _, emoticon := range group.Emoticons {
						if _, ok := normalizedSmilies[emoticon.Code]; ok {
							continue
						}

						// Skip Kappa because it could be golden and therefore removed from the normal globals set
						if emoticon.Code == "Kappa" {
							continue
						}

						codes = append(codes, emoticon.Code)
					}
				}

				if len(codes) == 0 {
					t.Fatal("expected more than 1 global code to be returned but found 0")
				}

				resp, err := mako.GetEmoteSetDetails(ctx, &makotwirp.GetEmoteSetDetailsRequest{
					UserId:     ownerID,
					SkipSitedb: true,
				})

				require.Nil(t, err)
				findEmoteCodesInSet(t, resp.EmoteSets, "0", codes...)
			},
		},

		{
			scenario: "emote picker: smilies globals",
			test: func(ctx context.Context, t *testing.T, mako makotwirp.Mako) {
				globals, err := mako.GetEmoticonsByGroups(ctx, &makotwirp.GetEmoticonsByGroupsRequest{
					EmoticonGroupKeys: []string{"0"},
				})

				require.Nil(t, err)

				var codes []string
				for _, group := range globals.Groups {
					if group.Id != "0" {
						continue
					}

					for _, emoticon := range group.Emoticons {
						if val, ok := normalizedSmilies[emoticon.Code]; ok {
							codes = append(codes, val)
						}
					}
				}

				if len(codes) == 0 {
					t.Fatal("expected more than 1 global code to be returned but found 0")
				}

				resp, err := mako.GetEmoteSetDetails(ctx, &makotwirp.GetEmoteSetDetailsRequest{
					UserId:     ownerID,
					SkipSitedb: true,
				})

				require.Nil(t, err)
				findEmoteCodesInSet(t, resp.EmoteSets, "0", codes...)
			},
		},

		{
			scenario: "chat: all globals",
			test: func(ctx context.Context, t *testing.T, mako makotwirp.Mako) {
				globals, err := mako.GetEmoticonsByGroups(ctx, &makotwirp.GetEmoticonsByGroupsRequest{
					EmoticonGroupKeys: []string{"0"},
				})

				require.Nil(t, err)

				var codes []string
				for _, group := range globals.Groups {
					if group.Id != "0" {
						continue
					}

					for _, emoticon := range group.Emoticons {
						codes = append(codes, emoticon.Code)

						if len(codes) > 10 {
							testChat(ctx, t, mako, ownerID, codes...)
							codes = []string{}
						}
					}
				}

			},
		},
	}

	for _, test := range tests {
		test := test

		t.Run(test.scenario, func(t *testing.T) {
			ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
			defer cancel()

			client, teardown, err := makeClient(ctx)
			if err != nil {
				t.Fatal(err)
			}

			defer teardown()

			test.test(ctx, t, client)
		})
	}
}
