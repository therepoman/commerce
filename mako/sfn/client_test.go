package sfn

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/mako/config"
	sfn_client_mock "code.justin.tv/commerce/mako/mocks/github.com/aws/aws-sdk-go/service/sfn/sfniface"
	stats_mock "code.justin.tv/commerce/mako/mocks/github.com/cactus/go-statsd-client/statsd"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/service/sfn"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
)

func TestSFNClientWrapper(t *testing.T) {
	testContext := context.Background()
	testUserId := "267893713"
	mockStats := new(stats_mock.Statter)
	mockConfig := &config.Configuration{
		UserDestroySFNARN: "test-ARN",
	}

	tests := []struct {
		scenario string
		test     func(t *testing.T)
	}{
		{
			scenario: "GIVEN error starting sfn execution WHEN ExecuteUserDestroyStateMachine THEN return error",
			test: func(t *testing.T) {
				// GIVEN
				mockSFN := new(sfn_client_mock.SFNAPI)
				mockSFN.On("StartExecutionWithContext", mock.Anything, mock.Anything).Return(nil, errors.New("dummy sfn error"))
				wrapper := sfnClientImpl{
					Statter: mockStats,
					SFN:     mockSFN,
					Config:  mockConfig,
				}
				testInput := UserDestroyInput{
					UserID: testUserId,
				}

				// WHEN
				err := wrapper.ExecuteUserDestroyStateMachine(testContext, testInput)

				// THEN
				require.Error(t, err)
				assert.Contains(t, err.Error(), "dummy sfn error")
			},
		},
		{
			scenario: "GIVEN execution already exists error starting sfn execution WHEN ExecuteUserDestroyStateMachine THEN return nil",
			test: func(t *testing.T) {
				// GIVEN
				mockSFN := new(sfn_client_mock.SFNAPI)
				mockSFN.On("StartExecutionWithContext", mock.Anything, mock.Anything).Return(
					nil, awserr.New(sfn.ErrCodeExecutionAlreadyExists, "", nil))
				wrapper := sfnClientImpl{
					Statter: mockStats,
					SFN:     mockSFN,
					Config:  mockConfig,
				}
				testInput := UserDestroyInput{
					UserID: testUserId,
				}

				// WHEN
				err := wrapper.ExecuteUserDestroyStateMachine(testContext, testInput)

				// THEN
				require.NoError(t, err)
			},
		},
		{
			scenario: "GIVEN success starting sfn execution WHEN ExecuteUserDestroyStateMachine THEN return nil",
			test: func(t *testing.T) {
				// GIVEN
				mockSFN := new(sfn_client_mock.SFNAPI)
				mockSFN.On("StartExecutionWithContext", mock.Anything, mock.Anything).Return(nil, nil)
				wrapper := sfnClientImpl{
					Statter: mockStats,
					SFN:     mockSFN,
					Config:  mockConfig,
				}
				testInput := UserDestroyInput{
					UserID: testUserId,
				}

				// WHEN
				err := wrapper.ExecuteUserDestroyStateMachine(testContext, testInput)

				// THEN
				require.NoError(t, err)
			},
		},
	}

	for _, test := range tests {
		t.Run(test.scenario, func(t *testing.T) {
			mockStats.On("Inc", mock.Anything, mock.Anything, mock.Anything).Return(nil)
			mockStats.On("TimingDuration", mock.Anything, mock.Anything, mock.Anything).Return(nil)
			test.test(t)
		})
	}
}
