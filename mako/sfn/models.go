package sfn

type UserDestroyInput struct {
	UserID string `json:"user_id"`
}

type UserDestroyOutput struct {
	UserID string `json:"user_id"`
}

type AnimatedEmoteFrameExtractionInput struct {
	EmoteID string `json:"emote_id"`
}
