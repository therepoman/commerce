package sfn

import (
	"context"
	"encoding/json"
	"time"

	"code.justin.tv/commerce/gogogadget/pointers"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/mako/config"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/session"
	sfn_client "github.com/aws/aws-sdk-go/service/sfn"
	"github.com/aws/aws-sdk-go/service/sfn/sfniface"
	"github.com/cactus/go-statsd-client/statsd"
)

const (
	startExecutionLatencyMetric = "sfn.start.execution.latency"
	startExecutionErrorMetric   = "sfn.start.execution.errors"
)

type ClientWrapper interface {
	ExecuteUserDestroyStateMachine(context.Context, UserDestroyInput) error
	ExecuteAnimatedEmoteFrameSplittingStateMachine(context.Context, AnimatedEmoteFrameExtractionInput) error
}

func NewSFNClient(cfg *config.Configuration, stats statsd.Statter) ClientWrapper {
	awsConfig := aws.NewConfig().WithRegion(cfg.AWSRegion)

	sess, err := session.NewSession(&aws.Config{
		Region: aws.String(cfg.AWSRegion),
	})
	if err != nil {
		log.WithError(err).Panic("Could not start new aws session")
	}

	client := sfn_client.New(sess, awsConfig)

	return &sfnClientImpl{
		SFN:     client,
		Config:  cfg,
		Statter: stats,
	}
}

type sfnClientImpl struct {
	SFN     sfniface.SFNAPI
	Config  *config.Configuration
	Statter statsd.Statter
}

func (s *sfnClientImpl) ExecuteUserDestroyStateMachine(ctx context.Context, input UserDestroyInput) error {
	return s.execute(ctx, s.Config.UserDestroySFNARN, input.UserID, input)
}

func (s *sfnClientImpl) ExecuteAnimatedEmoteFrameSplittingStateMachine(ctx context.Context, input AnimatedEmoteFrameExtractionInput) error {
	return s.execute(ctx, s.Config.AnimatedEmoteFrameSplittingSFNARN, input.EmoteID, input)
}

func (s *sfnClientImpl) statExecuteError() {
	s.Statter.Inc(startExecutionErrorMetric, 1, 1.0)
}

func (s *sfnClientImpl) execute(ctx context.Context, stateMachineARN string, executionName string, input interface{}) error {
	startTime := time.Now()

	defer func() {
		s.Statter.TimingDuration(startExecutionLatencyMetric, time.Since(startTime), 1.0)
	}()

	inputJSON, err := json.Marshal(input)

	if err != nil {
		s.statExecuteError()
		return err
	}

	startExecInput := &sfn_client.StartExecutionInput{
		Input:           pointers.StringP(string(inputJSON)),
		Name:            pointers.StringP(executionName),
		StateMachineArn: pointers.StringP(stateMachineARN),
	}

	_, err = s.SFN.StartExecutionWithContext(ctx, startExecInput)
	if awsErr, ok := err.(awserr.Error); ok {
		if awsErr.Code() == sfn_client.ErrCodeExecutionAlreadyExists {
			log.WithField("executionName", executionName).Info("found existing SFN execution, skipping.")
			return nil
		}
	}
	if err != nil {
		s.statExecuteError()
		return err
	}

	return nil
}
