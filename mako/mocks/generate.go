package mocks

// Use command: make generate_mocks
// To regenerate the mocks defined in this file

// Following Service (twirp)
//go:generate mockery --quiet --name=TwitchVXFollowingServiceECS --dir=$GOPATH/src/code.justin.tv/commerce/mako/vendor/code.justin.tv/amzn/TwitchVXFollowingServiceECSTwirp --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/code.justin.tv/amzn/TwitchVXFollowingServiceECSTwirp --outpkg=follows_mock

// Subscriptions (twirp)
//go:generate mockery --quiet --name=Subscriptions --dir=$GOPATH/src/code.justin.tv/commerce/mako/vendor/code.justin.tv/revenue/subscriptions/twirp --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/code.justin.tv/revenue/subscriptions/twirps --outpkg=subs_mock

// Subscriptions Client Wrapper
//go:generate mockery --quiet --name=ClientWrapper --dir=$GOPATH/src/code.justin.tv/commerce/mako/clients/subscriptions --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/subscriptions --outpkg=subscriptions_mock

// Payday (twirp)
//go:generate mockery --quiet --name=Payday --dir=$GOPATH/src/code.justin.tv/commerce/mako/vendor/code.justin.tv/commerce/payday/rpc/payday --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/payday/rpc/payday --outpkg=payday_mock

// Payday Client Wrapper
//go:generate mockery --quiet --name=ClientWrapper --dir=$GOPATH/src/code.justin.tv/commerce/mako/clients/payday --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/payday --outpkg=payday_client_mock

// ISmilies
//go:generate mockery --quiet --name=ISmilies --dir=$GOPATH/src/code.justin.tv/commerce/mako/clients/smilies --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/code.justin.tv/mako/clients/smilies --outpkg=clients_smilies_mock

// Emote DB (the destination is renamed, because of rules in Golang about packages named /internal)
//go:generate mockery --quiet --name=EmoteDB --dir=$GOPATH/src/code.justin.tv/commerce/mako/internal --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/internal_mock --outpkg=internal_mock

// EmoteUploadMetadataCache (the destination is renamed, because of rules in Golang about packages named /internal)
//go:generate mockery --quiet --name=EmoteUploadMetadataCache --dir=$GOPATH/src/code.justin.tv/commerce/mako/internal --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/internal_mock --outpkg=internal_mock

// IEmoticons
//go:generate mockery --quiet --name=IEmoticons --dir=$GOPATH/src/code.justin.tv/commerce/mako/clients/emoticons --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/code.justin.tv/mako/clients/emoticons --outpkg=client_emoticons_mock

// Badges Client
//go:generate mockery --quiet --name=Client --dir=$GOPATH/src/code.justin.tv/commerce/mako/vendor/code.justin.tv/chat/badges/client --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/code.justin.tv/chat/badges/client --outpkg=badges_mock

// ReadCloser
//go:generate mockery --quiet --name=ReadCloser --dir=$GOROOT/src/io --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/io --outpkg=golang_io_mock

// MateriaClient
//go:generate mockery --quiet --name=MateriaClient --dir=$GOPATH/src/code.justin.tv/commerce/mako/clients/materia --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/materia --outpkg=materia_mock

// IEventProcessor
//go:generate mockery --quiet --name=IEventProcessor --dir=$GOPATH/src/code.justin.tv/commerce/mako/events --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/events --outpkg=events_mock

// Statter
//go:generate mockery --quiet --name=Statter --dir=$GOPATH/src/code.justin.tv/commerce/mako/vendor/github.com/cactus/go-statsd-client/statsd --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/github.com/cactus/go-statsd-client/statsd --outpkg=stats_mock

// IS3Client
//go:generate mockery --quiet --name=IS3Client --dir=$GOPATH/src/code.justin.tv/commerce/mako/clients/s3 --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/s3 --outpkg=s3_mock

// SQSAPI
//go:generate mockery --quiet --name=SQSAPI --dir=$GOPATH/src/code.justin.tv/commerce/mako/vendor/github.com/aws/aws-sdk-go/service/sqs/sqsiface --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/github.com/aws/aws-sdk-go/service/sqs/sqsiface --outpkg=sqs_mock

// IEventDelegator
//go:generate mockery --quiet --name=IEventDelegator --dir=$GOPATH/src/code.justin.tv/commerce/mako/events --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/events --outpkg=events_mock

// IEntitlementDao
//go:generate mockery --quiet --name=IEntitlementDao --dir=$GOPATH/src/code.justin.tv/commerce/mako/dynamo --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/dynamo --outpkg=dynamo_mock

// SNS Client
//go:generate mockery --quiet --name=ISNSClient --dir=$GOPATH/src/code.justin.tv/commerce/mako/clients/sns --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/sns --outpkg=sns_mock

// PubSub Client
//go:generate mockery --quiet --name=IPublisher --dir=$GOPATH/src/code.justin.tv/commerce/mako/clients/pubsub --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/pubsub --outpkg=pubsub_mock

// Spade Client
//go:generate mockery --quiet --name=Client --dir=$GOPATH/src/code.justin.tv/commerce/mako/vendor/code.justin.tv/common/spade-client-go/spade --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/code.justin.tv/common/spade-client-go/spade --outpkg=spade_mock

// Upload Service Uploader
//go:generate mockery --quiet --name=Uploader --dir=$GOPATH/src/code.justin.tv/commerce/mako/vendor/code.justin.tv/web/upload-service/rpc/uploader --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/code.justin.tv/web/upload-service/rpc/uploader --outpkg=upload_service_mock

// Prefix Recommender
//go:generate mockery --quiet --name=PrefixRecommender --dir=$GOPATH/src/code.justin.tv/commerce/mako/prefixes --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/prefixes --outpkg=prefixes_mock

// Prefix Client
//go:generate mockery --quiet --name=PrefixesClient --dir=$GOPATH/src/code.justin.tv/commerce/mako/prefixes --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/prefixes --outpkg=prefixes_mock

// (User Service) InternalClient
//go:generate mockery --quiet --name=InternalClient --dir=$GOPATH/src/code.justin.tv/commerce/mako/vendor/code.justin.tv/web/users-service/client/usersclient_internal/ --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/code.justin.tv/web/users-service/client/usersclient_internal/ --outpkg=clients_mock

// CampaignEmoteOwners
//go:generate mockery --quiet --name=CampaignEmoteOwners --dir=$GOPATH/src/code.justin.tv/commerce/mako/config --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/config --outpkg=config_mock

// Prefixes DAO
//go:generate mockery --quiet --name=IPrefixDao --dir=$GOPATH/src/code.justin.tv/commerce/mako/dynamo --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/dynamo --outpkg=dynamo_mock

// Site DB Client
//go:generate mockery --quiet --name=ISiteDB --dir=$GOPATH/src/code.justin.tv/commerce/mako/clients/sitedb --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/sitedb --outpkg=sitedb_mock

// Paint Client.
//go:generate mockery --quiet --name=PaintClient --dir=$GOPATH/src/code.justin.tv/commerce/mako/clients/paint --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/paint --outpkg=paint_mock

// Image Uploader
//go:generate mockery --quiet --name=IImageUploader --dir=$GOPATH/src/code.justin.tv/commerce/mako/clients/imageuploader --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/imageuploader --outpkg=imageuploader_mock

// Redis Client
//go:generate mockery --quiet --name=IRedisClient --dir=$GOPATH/src/code.justin.tv/commerce/mako/redis --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/redis --outpkg=redis_mock

// Multicacher
//go:generate mockery --quiet --name=MultiCacher --dir=$GOPATH/src/code.justin.tv/commerce/mako/vendor/code.justin.tv/commerce/multicache/multicache --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/multicache/multicache --outpkg=multicache_mock

// Emoticon Aggregator
//go:generate mockery --quiet --name=EmoticonAggregator --dir=$GOPATH/src/code.justin.tv/commerce/mako/clients/emoticons --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/emoticons --outpkg=emoticons_mock

// Emoticon Manager
//go:generate mockery --quiet --name=IEmoticonsManager --dir=$GOPATH/src/code.justin.tv/commerce/mako/clients/emoticonsmanager --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/emoticonsmanager --outpkg=emoticonsmanager_mock

// Emoticon State Updater
//go:generate mockery --quiet --name=IEmoticonStateUpdater --dir=$GOPATH/src/code.justin.tv/commerce/mako/clients/emoticonstateupdater --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/emoticonstateupdater --outpkg=emoticonstateupdater_mock

// SiteDB Client
//go:generate mockery --quiet --name=ISiteDB --dir=$GOPATH/src/code.justin.tv/commerce/mako/clients/sitedb --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/sitedb --outpkg=sitedb_mock

// PendingEmoticons Client
//go:generate mockery --quiet --name=IPendingEmoticonsClient --dir=$GOPATH/src/code.justin.tv/commerce/mako/clients/pendingemoticons --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/pendingemoticons --outpkg=pending_emoticons_client_mock

// EmoteGroupDB
//go:generate mockery --quiet --name=EmoteGroupDB --dir=$GOPATH/src/code.justin.tv/commerce/mako/internal --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/internal_mock --outpkg=internal_mock

// PendingEmoticons DAO
//go:generate mockery --quiet --name=IPendingEmotesDao --dir=$GOPATH/src/code.justin.tv/commerce/mako/dynamo --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/dynamo --outpkg=dynamo_mock

// Ripley Client
//go:generate mockery --quiet --name=Ripley --dir=$GOPATH/src/code.justin.tv/commerce/mako/vendor/code.justin.tv/revenue/ripley/rpc --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/code.justin.tv/revenue/ripley/rpc --outpkg=ripley_client_mock

// Ripley Client Wrapper
//go:generate mockery --quiet --name=ClientWrapper --dir=$GOPATH/src/code.justin.tv/commerce/mako/clients/ripley --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/ripley --outpkg=ripley_mock

// Pepper Client
//go:generate mockery --quiet --name=Pepper --dir=$GOPATH/src/code.justin.tv/commerce/mako/vendor/code.justin.tv/revenue/pepper/rpc --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/code.justin.tv/revenue/pepper/rpc --outpkg=pepper_client_mock

// Pepper Client Wrapper
//go:generate mockery --quiet --name=ClientWrapper --dir=$GOPATH/src/code.justin.tv/commerce/mako/clients/pepper --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/pepper --outpkg=pepper_mock

// Salesforce Client Wrapper
//go:generate mockery --quiet --name=APIClient --dir=$GOPATH/src/code.justin.tv/commerce/mako/clients/salesforce --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/salesforce --outpkg=salesforce_mock

// Auto Approval Eligibility Fetcher
//go:generate mockery --quiet --name=EligibilityFetcher --dir=$GOPATH/src/code.justin.tv/commerce/mako/clients/autoapproval --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/autoapproval --outpkg=autoapproval_mock

// PrefixUpdateProcessor
//go:generate mockery --quiet --name=IPrefixUpdateProcessor --dir=$GOPATH/src/code.justin.tv/commerce/mako/sqs/processors --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/sqs/processors --outpkg=processors_mock

// Materia Emoticons
//go:generate mockery --quiet --name=IMateriaEmoticons --dir=$GOPATH/src/code.justin.tv/commerce/mako/clients/emoticons --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/emoticons --outpkg=emoticons_mock

// Experiments Client
//go:generate mockery --quiet --name=ExperimentsClient --dir=$GOPATH/src/code.justin.tv/commerce/mako/clients/experiments --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/experiments --outpkg=experiments_mock

// Spade Client
//go:generate mockery --quiet --name=Client --dir=$GOPATH/src/code.justin.tv/commerce/mako/vendor/code.justin.tv/common/spade-client-go/spade --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/code.justin.tv/common/spade-client-go/spade --outpkg=spade_mock

// Dart Receiver Client
//go:generate mockery --quiet --name=Receiver --dir=$GOPATH/src/code.justin.tv/commerce/mako/vendor/code.justin.tv/amzn/TwitchDartReceiverTwirp --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/code.justin.tv/amzn/TwitchDartReceiverTwirp --outpkg=receiver_mock

// Entitlement DB
//go:generate mockery --quiet --name=EntitlementDB --dir=$GOPATH/src/code.justin.tv/commerce/mako/internal --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/internal_mock --outpkg=internal_mock

// Emote Modifiers DB
//go:generate mockery --quiet --name=EmoteModifiersDB --dir=$GOPATH/src/code.justin.tv/commerce/mako/internal --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/internal_mock --outpkg=internal_mock

// RoundTripper (this one currently imports something with a full go package and requires manually fixing, which isn't correct)
//go:generate mockery --quiet --name=RoundTripper --dir=$GOROOT/src/net/http --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/net/http --outpkg=golang_http_mock

// Event Bus Client Publisher
//go:generate mockery --quiet --name=Publisher --dir=$GOPATH/src/code.justin.tv/commerce/mako/vendor/code.justin.tv/eventbus/client --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/code.justin.tv/eventbus/client --outpkg=client_mock

// Event Bus Client Wrapper
//go:generate mockery --quiet --name=Client --dir=$GOPATH/src/code.justin.tv/commerce/mako/clients/eventbus --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/eventbus --outpkg=eventbus_mock

// Event Bus Publisher
//go:generate mockery --quiet --name=Publisher --dir=$GOPATH/src/code.justin.tv/commerce/mako/clients/eventbus --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/eventbus --outpkg=eventbus_mock

// DynamicConfigClient
//go:generate mockery --quiet --name=DynamicConfigClient --dir=$GOPATH/src/code.justin.tv/commerce/mako/config --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/config --outpkg=config_mock

// KnownOrVerifiedBotsConfig
//go:generate mockery --quiet --name=KnownOrVerifiedBotsConfigClient --dir=$GOPATH/src/code.justin.tv/commerce/mako/config --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/config --outpkg=config_mock

// DefaultEmoteImagesConfig
//go:generate mockery --quiet --name=DefaultEmoteImagesDataConfigClient --dir=$GOPATH/src/code.justin.tv/commerce/mako/config --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/config --outpkg=config_mock

// Emote Standing Overrides DB
//go:generate mockery --quiet --name=EmoteStandingOverridesDB --dir=$GOPATH/src/code.justin.tv/commerce/mako/internal --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/internal_mock --outpkg=internal_mock

// Names Client
//go:generate mockery --quiet --name=Names --dir=$GOPATH/src/code.justin.tv/commerce/mako/vendor/code.justin.tv/safety/inference/rpc/names --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/code.justin.tv/safety/inference/rpc/names --outpkg=names_client_mock

// Names Client Wrapper
//go:generate mockery --quiet --name=ClientWrapper --dir=$GOPATH/src/code.justin.tv/commerce/mako/clients/names --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/names --outpkg=names_mock

// Emote Code Acceptability Validator
//go:generate mockery --quiet --name=AcceptabilityValidator --dir=$GOPATH/src/code.justin.tv/commerce/mako/clients/emotecodeacceptability --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/emotecodeacceptability --outpkg=emotecodeacceptability_mock

// Emote Limits Manager
//go:generate mockery --quiet --name=EmoteLimitsManager --dir=$GOPATH/src/code.justin.tv/commerce/mako/clients/emotelimits --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/emotelimits --outpkg=emotelimits_mock

// Mako (twirp)
//go:generate mockery --quiet --name=Mako --dir=$GOPATH/src/code.justin.tv/commerce/mako/twirp --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/twirp --outpkg=mako_client_mock

// PDMS Client
//go:generate mockery --quiet --name=PDMSService --dir=$GOPATH/src/code.justin.tv/commerce/mako/vendor/code.justin.tv/amzn/PDMSLambdaTwirp --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/code.justin.tv/amzn/PDMSLambdaTwirp --outpkg=pdms_client_mock

// PDMS Client Wrapper
//go:generate mockery --quiet --name=ClientWrapper --dir=$GOPATH/src/code.justin.tv/commerce/mako/clients/pdms --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/pdms --outpkg=pdms_mock

// SFN Client
//go:generate mockery --quiet --name=SFNAPI --dir=$GOPATH/src/code.justin.tv/commerce/mako/vendor/github.com/aws/aws-sdk-go/service/sfn/sfniface --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/github.com/aws/aws-sdk-go/service/sfn/sfniface --outpkg=sfn_client_mock

// SFN Client Wrapper
//go:generate mockery --quiet --name=ClientWrapper --dir=$GOPATH/src/code.justin.tv/commerce/mako/sfn --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/sfn --outpkg=sfn_mock

// Lambda Client
//go:generate mockery --quiet --name=LambdaAPI --dir=$GOPATH/src/code.justin.tv/commerce/mako/vendor/github.com/aws/aws-sdk-go/service/lambda/lambdaiface --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/github.com/aws/aws-sdk-go/service/lambda --outpkg=lambda_client_mock

// Animated Emote Manager
//go:generate mockery --quiet --name=AnimatedEmoteManager --dir=$GOPATH/src/code.justin.tv/commerce/mako/clients/emoticonsmanager --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/emoticonsmanager --outpkg=emoticonsmanager_mock

// Emote Manager S3 Uploader
//go:generate mockery --quiet --name=EmoteManagerS3Uploader --dir=$GOPATH/src/code.justin.tv/commerce/mako/clients/emoticonsmanager --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/emoticonsmanager --outpkg=emoticonsmanager_mock

// Emotes Inference Client
//go:generate mockery --quiet --name=Emotes --dir=$GOPATH/src/code.justin.tv/commerce/mako/vendor/code.justin.tv/safety/inference/rpc/emotes --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/code.justin.tv/safety/inference/rpc/emotes --outpkg=emotesinference_client_mock

// Emotes Inference Client Wrapper
//go:generate mockery --quiet --name=ClientWrapper --dir=$GOPATH/src/code.justin.tv/commerce/mako/clients/emotesinference --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/emotesinference --outpkg=emotesinference_mock

// All Emotes Export Emote DB Interface
//go:generate mockery --quiet --name=IAllEmotesEmoteDAO --dir=$GOPATH/src/code.justin.tv/commerce/mako/lambda/functions/all_emotes_export/dynamo --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/lambda/functions/all_emotes_export/dynamo --outpkg=allemotesexport_emotedb_mock

// Dynamo Client
//go:generate mockery --quiet --name=DynamoDBAPI --dir=$GOPATH/src/code.justin.tv/commerce/mako/vendor/github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface --outpkg=dynamo_client_mock

// Chemtrail Client
//go:generate mockery --quiet --name=ChemtrailService --dir=$GOPATH/src/code.justin.tv/commerce/mako/vendor/code.justin.tv/ce-analytics/chemtrail/rpc/chemtrail --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/code.justin.tv/ce-analytics/chemtrail/rpc/chemtrail --outpkg=chemtrail_client_mock

// Chemtrail Client Wrapper
//go:generate mockery --quiet --name=ClientWrapper --dir=$GOPATH/src/code.justin.tv/commerce/mako/clients/chemtrail --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/chemtrail --outpkg=chemtrail_mock

// ProductEmoteGroups Client
//go:generate mockery --quiet  --name=ProductEmoteGroups --dir=$GOPATH/src/code.justin.tv/commerce/mako/clients/productemotegroups --output=$GOPATH/src/code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/productemotegroups --outpkg=productemotegroups_mock
