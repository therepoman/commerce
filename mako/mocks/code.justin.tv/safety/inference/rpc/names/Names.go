// Code generated by mockery 2.9.0. DO NOT EDIT.

package names_client_mock

import (
	context "context"

	names "code.justin.tv/safety/inference/rpc/names"
	mock "github.com/stretchr/testify/mock"
)

// Names is an autogenerated mock type for the Names type
type Names struct {
	mock.Mock
}

// AcceptableNames provides a mock function with given fields: _a0, _a1
func (_m *Names) AcceptableNames(_a0 context.Context, _a1 *names.AcceptableNamesRequest) (*names.AcceptableNamesResponse, error) {
	ret := _m.Called(_a0, _a1)

	var r0 *names.AcceptableNamesResponse
	if rf, ok := ret.Get(0).(func(context.Context, *names.AcceptableNamesRequest) *names.AcceptableNamesResponse); ok {
		r0 = rf(_a0, _a1)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*names.AcceptableNamesResponse)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, *names.AcceptableNamesRequest) error); ok {
		r1 = rf(_a0, _a1)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}
