// Code generated by mockery 2.9.0. DO NOT EDIT.

package internal_mock

import (
	context "context"

	mako "code.justin.tv/commerce/mako/internal"
	mock "github.com/stretchr/testify/mock"
)

// EmoteStandingOverridesDB is an autogenerated mock type for the EmoteStandingOverridesDB type
type EmoteStandingOverridesDB struct {
	mock.Mock
}

// ReadByUserID provides a mock function with given fields: ctx, userID
func (_m *EmoteStandingOverridesDB) ReadByUserID(ctx context.Context, userID string) (*mako.UserEmoteStandingOverride, error) {
	ret := _m.Called(ctx, userID)

	var r0 *mako.UserEmoteStandingOverride
	if rf, ok := ret.Get(0).(func(context.Context, string) *mako.UserEmoteStandingOverride); ok {
		r0 = rf(ctx, userID)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*mako.UserEmoteStandingOverride)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string) error); ok {
		r1 = rf(ctx, userID)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// UpdateOverrides provides a mock function with given fields: ctx, override
func (_m *EmoteStandingOverridesDB) UpdateOverrides(ctx context.Context, override ...mako.UserEmoteStandingOverride) ([]mako.UserEmoteStandingOverride, error) {
	_va := make([]interface{}, len(override))
	for _i := range override {
		_va[_i] = override[_i]
	}
	var _ca []interface{}
	_ca = append(_ca, ctx)
	_ca = append(_ca, _va...)
	ret := _m.Called(_ca...)

	var r0 []mako.UserEmoteStandingOverride
	if rf, ok := ret.Get(0).(func(context.Context, ...mako.UserEmoteStandingOverride) []mako.UserEmoteStandingOverride); ok {
		r0 = rf(ctx, override...)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]mako.UserEmoteStandingOverride)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, ...mako.UserEmoteStandingOverride) error); ok {
		r1 = rf(ctx, override...)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}
