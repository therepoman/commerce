// Code generated by mockery 2.9.0. DO NOT EDIT.

package pdms_mock

import (
	context "context"

	mock "github.com/stretchr/testify/mock"
)

// ClientWrapper is an autogenerated mock type for the ClientWrapper type
type ClientWrapper struct {
	mock.Mock
}

// ReportDeletion provides a mock function with given fields: ctx, userID
func (_m *ClientWrapper) ReportDeletion(ctx context.Context, userID string) error {
	ret := _m.Called(ctx, userID)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, string) error); ok {
		r0 = rf(ctx, userID)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}
