// Code generated by mockery 2.9.0. DO NOT EDIT.

package eventbus_mock

import (
	context "context"

	mako "code.justin.tv/commerce/mako/internal"

	mock "github.com/stretchr/testify/mock"
)

// Client is an autogenerated mock type for the Client type
type Client struct {
	mock.Mock
}

// EmoticonCreate provides a mock function with given fields: ctx, emoticons
func (_m *Client) EmoticonCreate(ctx context.Context, emoticons []mako.Emote) error {
	ret := _m.Called(ctx, emoticons)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, []mako.Emote) error); ok {
		r0 = rf(ctx, emoticons)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// EmoticonDelete provides a mock function with given fields: ctx, emoticons
func (_m *Client) EmoticonDelete(ctx context.Context, emoticons []mako.Emote) error {
	ret := _m.Called(ctx, emoticons)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, []mako.Emote) error); ok {
		r0 = rf(ctx, emoticons)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}
