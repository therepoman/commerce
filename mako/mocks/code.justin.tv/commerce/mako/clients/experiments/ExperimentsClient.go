// Code generated by mockery 2.9.0. DO NOT EDIT.

package experiments_mock

import mock "github.com/stretchr/testify/mock"

// ExperimentsClient is an autogenerated mock type for the ExperimentsClient type
type ExperimentsClient struct {
	mock.Mock
}

// Close provides a mock function with given fields:
func (_m *ExperimentsClient) Close() error {
	ret := _m.Called()

	var r0 error
	if rf, ok := ret.Get(0).(func() error); ok {
		r0 = rf()
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// StartGlobalOverridePolling provides a mock function with given fields:
func (_m *ExperimentsClient) StartGlobalOverridePolling() {
	_m.Called()
}
