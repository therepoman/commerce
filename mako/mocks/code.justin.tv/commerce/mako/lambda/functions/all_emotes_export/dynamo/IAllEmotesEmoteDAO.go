// Code generated by mockery 2.9.0. DO NOT EDIT.

package allemotesexport_emotedb_mock

import (
	context "context"

	dynamo "code.justin.tv/commerce/mako/lambda/functions/all_emotes_export/dynamo"
	mock "github.com/stretchr/testify/mock"
)

// IAllEmotesEmoteDAO is an autogenerated mock type for the IAllEmotesEmoteDAO type
type IAllEmotesEmoteDAO struct {
	mock.Mock
}

// ScanForActiveEmotes provides a mock function with given fields: ctx, totalSegments, segment
func (_m *IAllEmotesEmoteDAO) ScanForActiveEmotes(ctx context.Context, totalSegments *int64, segment *int64) ([]dynamo.ExportEmote, error) {
	ret := _m.Called(ctx, totalSegments, segment)

	var r0 []dynamo.ExportEmote
	if rf, ok := ret.Get(0).(func(context.Context, *int64, *int64) []dynamo.ExportEmote); ok {
		r0 = rf(ctx, totalSegments, segment)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]dynamo.ExportEmote)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, *int64, *int64) error); ok {
		r1 = rf(ctx, totalSegments, segment)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}
