// Code generated by mockery 2.9.0. DO NOT EDIT.

package sfn_mock

import (
	context "context"

	sfn "code.justin.tv/commerce/mako/sfn"
	mock "github.com/stretchr/testify/mock"
)

// ClientWrapper is an autogenerated mock type for the ClientWrapper type
type ClientWrapper struct {
	mock.Mock
}

// ExecuteAnimatedEmoteFrameSplittingStateMachine provides a mock function with given fields: _a0, _a1
func (_m *ClientWrapper) ExecuteAnimatedEmoteFrameSplittingStateMachine(_a0 context.Context, _a1 sfn.AnimatedEmoteFrameExtractionInput) error {
	ret := _m.Called(_a0, _a1)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, sfn.AnimatedEmoteFrameExtractionInput) error); ok {
		r0 = rf(_a0, _a1)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// ExecuteUserDestroyStateMachine provides a mock function with given fields: _a0, _a1
func (_m *ClientWrapper) ExecuteUserDestroyStateMachine(_a0 context.Context, _a1 sfn.UserDestroyInput) error {
	ret := _m.Called(_a0, _a1)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, sfn.UserDestroyInput) error); ok {
		r0 = rf(_a0, _a1)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}
