// Code generated by mockery 2.9.0. DO NOT EDIT.

package multicache_mock

import (
	context "context"

	multicache "code.justin.tv/commerce/multicache/multicache"
	mock "github.com/stretchr/testify/mock"

	time "time"
)

// MultiCacher is an autogenerated mock type for the MultiCacher type
type MultiCacher struct {
	mock.Mock
}

// Fetch provides a mock function with given fields: ctx, cachePrefix, keys, defaultTTL, fn
func (_m *MultiCacher) Fetch(ctx context.Context, cachePrefix string, keys []multicache.CacheKeyer, defaultTTL time.Duration, fn multicache.FetchCallback) ([][]byte, error) {
	ret := _m.Called(ctx, cachePrefix, keys, defaultTTL, fn)

	var r0 [][]byte
	if rf, ok := ret.Get(0).(func(context.Context, string, []multicache.CacheKeyer, time.Duration, multicache.FetchCallback) [][]byte); ok {
		r0 = rf(ctx, cachePrefix, keys, defaultTTL, fn)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([][]byte)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string, []multicache.CacheKeyer, time.Duration, multicache.FetchCallback) error); ok {
		r1 = rf(ctx, cachePrefix, keys, defaultTTL, fn)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// Invalidate provides a mock function with given fields: ctx, cachePrefix, keys
func (_m *MultiCacher) Invalidate(ctx context.Context, cachePrefix string, keys []multicache.CacheKeyer) error {
	ret := _m.Called(ctx, cachePrefix, keys)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, string, []multicache.CacheKeyer) error); ok {
		r0 = rf(ctx, cachePrefix, keys)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}
