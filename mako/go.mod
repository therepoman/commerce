module code.justin.tv/commerce/mako

go 1.14

require (
	code.justin.tv/amzn/MateriaTwirp v0.0.0-20201202014530-3677fbe5cf17
	code.justin.tv/amzn/PDMSLambdaTwirp v0.0.0-20200727190725-5cb32af1bce8
	code.justin.tv/amzn/TwirpGoLangAWSTransports v0.0.0-20210805024623-2d1788515cf6
	code.justin.tv/amzn/TwitchDartReceiverTwirp v0.0.0-20210204230056-e3536128a10f
	code.justin.tv/amzn/TwitchTelemetry v0.0.0-20200611235125-850332f4e137 // indirect
	code.justin.tv/amzn/TwitchTelemetryStatsdMetricsSender v0.0.0-20200929010454-f8c83abba668 // indirect
	code.justin.tv/amzn/TwitchVXFollowingServiceECSTwirp v0.0.0-20210510173238-7e90e58e8020
	code.justin.tv/ce-analytics/chemtrail v0.0.0-20210811210836-00806d7fdeab
	code.justin.tv/chat/badges v1.7.1-0.20191028200752-64e2be14e06f
	code.justin.tv/chat/ecsgomaxprocs v1.0.0
	code.justin.tv/chat/golibs v3.10.0+incompatible
	code.justin.tv/chat/pubsub-go-pubclient v1.0.1-0.20190201000902-d9abe2861520
	code.justin.tv/chat/rediczar v1.16.0
	code.justin.tv/chat/workerqueue v1.1.0
	code.justin.tv/commerce/dynamicconfig v1.1.2
	code.justin.tv/commerce/gogogadget v0.0.0-20200403181836-bbb6f1ea0223
	code.justin.tv/commerce/logrus v1.1.2
	code.justin.tv/commerce/multicache v0.0.0-20191009002900-b13dd61c7def
	code.justin.tv/commerce/payday v1.0.1
	code.justin.tv/commerce/sandstorm-client v0.0.0-20200521180957-61f3fd64c3ee
	code.justin.tv/commerce/splatter v1.5.6
	code.justin.tv/common/godynamo v0.0.0-20170304001942-caad64f48dc0
	code.justin.tv/common/spade-client-go v2.1.2-0.20191022164217-445273a0e877+incompatible
	code.justin.tv/common/twirp v4.9.0+incompatible
	code.justin.tv/discovery/experiments v5.1.0+incompatible
	code.justin.tv/eventbus/client v1.7.0
	code.justin.tv/eventbus/schema v0.0.0-20210211234802-88f5047a79cf
	code.justin.tv/foundation/twitchclient v4.11.0+incompatible
	code.justin.tv/revenue/pepper v0.0.0-20200413204214-0cdc2088bd00
	code.justin.tv/revenue/ripley v1.1.1
	code.justin.tv/revenue/subscriptions v1.0.2
	code.justin.tv/safety/inference v1.15.1
	code.justin.tv/sse/malachai v0.5.9
	code.justin.tv/subs/paint v0.0.0-20201210215803-e3353a5770e8
	code.justin.tv/systems/sandstorm v1.6.6
	code.justin.tv/video/autoprof v1.1.2
	code.justin.tv/web/upload-service v5.1.0+incompatible
	code.justin.tv/web/users-service v2.12.0+incompatible
	github.com/BurntSushi/toml v0.3.1
	github.com/afex/hystrix-go v0.0.0-20180502004556-fa1af6a1f4f5
	github.com/aws/aws-lambda-go v1.20.0
	github.com/aws/aws-sdk-go v1.34.9
	github.com/cactus/go-statsd-client/statsd v0.0.0-20200423205355-cb0885a1018c
	github.com/cenkalti/backoff v2.2.1+incompatible
	github.com/dgraph-io/ristretto v0.0.2
	github.com/dgryski/dgohash v0.0.0-20140831132549-60798d4c29a0
	github.com/go-redis/redis v6.15.7+incompatible
	github.com/go-redis/redis/v7 v7.4.0
	github.com/golang/protobuf v1.5.2
	github.com/hashicorp/go-multierror v1.0.0
	github.com/pkg/errors v0.9.1
	github.com/satori/go.uuid v1.2.1-0.20181028125025-b2ce2384e17b
	github.com/segmentio/conf v1.2.0
	github.com/segmentio/events/v2 v2.4.0
	github.com/segmentio/ksuid v1.0.2
	github.com/sirupsen/logrus v1.5.0
	github.com/smartystreets/goconvey v1.6.4
	github.com/stretchr/testify v1.6.1
	github.com/tsenart/vegeta v11.4.0+incompatible
	github.com/tsenart/vegeta/v12 v12.8.4
	github.com/twitchtv/twirp v7.1.1+incompatible
	github.com/willf/bitset v1.1.10 // indirect
	github.com/willf/bloom v2.0.3+incompatible
	golang.org/x/net v0.0.0-20200904194848-62affa334b73
	golang.org/x/sync v0.0.0-20200625203802-6e8e738ad208
	golang.org/x/time v0.0.0-20191024005414-555d28b269f0
	google.golang.org/protobuf v1.26.0
	gopkg.in/validator.v2 v2.0.0-20191107172027-c3144fdedc21
)

replace gopkg.in/DATA-DOG/go-sqlmock.v1 v1.3.3 => github.com/DATA-DOG/go-sqlmock v1.3.3

replace gopkg.in/DATA-DOG/go-sqlmock.v1 => github.com/DATA-DOG/go-sqlmock v1.3.3

replace gopkg.in/jarcoal/httpmock.v1 => github.com/jarcoal/httpmock v1.0.6
