package main

import (
	"context"
	"encoding/csv"
	"io"
	"log"
	"os"

	"code.justin.tv/commerce/mako/config"
	"code.justin.tv/commerce/mako/dynamo"
	mako "code.justin.tv/commerce/mako/internal"
)

func main() {
	userCreateDefaultEmoteIDs := getIDsFromFile("./default_emote_user_create_emote_ids.csv")

	cfg, err := config.LoadConfigForEnvironment("prod")
	if err != nil {
		log.Fatal(err)
	}

	log.Printf("Running script in the %s environment\n", cfg.EnvironmentPrefix)

	emoteDB := dynamo.NewEmoteDB(dynamo.EmoteDBConfig{
		Table:  "prod_emotes",
		Region: cfg.AWSRegion,
	})

	for _, id := range userCreateDefaultEmoteIDs {
		newImageSource := mako.DefaultLibraryImage
		_, err := emoteDB.UpdateEmote(context.Background(), id, mako.EmoteUpdate{ImageSource: &newImageSource})
		if err != nil {
			// chances are that the emote has already been deleted, but print any errors to double check after the script completes
			log.Println(err)
		} else {
			log.Println("successfully updated", id)
		}
	}
	log.Println("finished")
}

func getIDsFromFile(filename string) []string {
	var ids []string

	// Open the file
	csvfile, err := os.Open(filename)
	if err != nil {
		log.Fatalln("Couldn't open the csv file", err)
	}

	// Parse the file
	r := csv.NewReader(csvfile)

	// Iterate through the records
	for {
		// Read each record from csv
		record, err := r.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal(err)
		}

		ids = append(ids, record[0])
	}

	return ids
}
