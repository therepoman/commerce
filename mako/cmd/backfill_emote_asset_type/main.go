package main

import (
	"context"
	"flag"
	"fmt"
	"sync"
	"sync/atomic"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/mako/config"
	"code.justin.tv/commerce/mako/dynamo"
	mako "code.justin.tv/commerce/mako/internal"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"golang.org/x/time/rate"
)

const (
	concurrentWaitGroups   = 100
	maxWriteTPS            = 1000
	prodEmotesTableName    = "prod_emotes"
	stagingEmotesTableName = "staging_emotes"
)

var (
	env    = flag.String("env", "staging", "Environment to run the script against (staging, prod). Defaults to staging.")
	dryrun = flag.Bool("dryrun", true, "Set this to false to perform the write operations of this script. Defaults to true.")
)

type emoteAssetTypeBackfiller struct {
	dynamoClient  *dynamodb.DynamoDB
	emoteDBWriter *dynamo.EmoteDB
	emotesTable   string
	emotes        []emote
	limiter       *rate.Limiter
}

type emote struct {
	ID          string `dynamodbav:"id"`
	OwnerID     string `dynamodbav:"ownerId"`
	GroupID     string `dynamodbav:"groupId"`
	Prefix      string `dynamodbav:"prefix"`
	Suffix      string `dynamodbav:"suffix"`
	Code        string `dynamodbav:"code"`
	State       string `dynamodbav:"state"`
	Domain      string `dynamodbav:"domain"`
	CreatedAt   int64  `dynamodbav:"createdAt"`
	UpdatedAt   int64  `dynamodbav:"updatedAt"`
	Order       int64  `dynamodbav:"order"`
	EmotesGroup string `dynamodbav:"emotesGroup"`
	ProductID   string `dynamodbav:"productId"`
	AssetType   string `dynamodbav:"assetType"`
}

func (e emote) toMako() mako.Emote {
	return mako.Emote{
		ID:          e.ID,
		OwnerID:     e.OwnerID,
		GroupID:     e.GroupID,
		ProductID:   e.ProductID,
		EmotesGroup: e.EmotesGroup,
		Prefix:      e.Prefix,
		Suffix:      e.Suffix,
		Code:        e.Code,
		State:       mako.EmoteState(e.State),
		Domain:      mako.Domain(e.Domain),
		CreatedAt:   time.Unix(0, e.CreatedAt),
		UpdatedAt:   time.Unix(0, e.UpdatedAt),
		Order:       e.Order,
		AssetType:   mako.EmoteAssetType(e.AssetType),
	}
}

func main() {
	flag.Parse()

	cfg, err := config.LoadConfigForEnvironment(*env)
	if err != nil {
		log.Fatal(err)
	}

	if cfg.EnvironmentPrefix == "prod" && !*dryrun {
		fmt.Println("!!!!Running for real in prod. Giving you 10 seconds to rethink this decision!!!!")
		time.Sleep(time.Second * 10)
	}

	log.Printf("Running script in the %s environment\n", cfg.EnvironmentPrefix)

	sess, err := session.NewSession(&aws.Config{
		Region: aws.String(cfg.AWSRegion),
	})
	if err != nil {
		log.Fatal(err)
	}

	dynamoClient := dynamodb.New(sess)

	var emotesTableName string

	switch *env {
	case "prod":
		emotesTableName = prodEmotesTableName
	default:
		emotesTableName = stagingEmotesTableName
	}

	emoteDBWriter := dynamo.NewEmoteDB(dynamo.EmoteDBConfig{
		Table:  emotesTableName,
		Region: cfg.AWSRegion,
	})

	limiter := rate.NewLimiter(maxWriteTPS, 1)

	backfiller := emoteAssetTypeBackfiller{
		dynamoClient:  dynamoClient,
		emoteDBWriter: emoteDBWriter,
		emotesTable:   emotesTableName,
		emotes:        make([]emote, 0),
		limiter:       limiter,
	}

	fmt.Printf("Scanning emotes from table %s...\n", backfiller.emotesTable)

	backfiller.scanAndUpdateEmotes()

	fmt.Printf("Done scanning emotes. Count: %d\n", len(backfiller.emotes))

	fmt.Printf("Writing emotes to table %s...\n", backfiller.emotesTable)

	backfiller.writeEmotesToDynamo()

	fmt.Println("Done writing emotes.")
}

func (b *emoteAssetTypeBackfiller) scanAndUpdateNext(lastEvaluatedKey map[string]*dynamodb.AttributeValue) map[string]*dynamodb.AttributeValue {
	input := &dynamodb.ScanInput{
		TableName: aws.String(b.emotesTable),
	}

	if lastEvaluatedKey != nil {
		input.ExclusiveStartKey = lastEvaluatedKey
	}

	result, err := b.dynamoClient.Scan(input)

	if err != nil {
		log.Fatal(err)
	}

	for _, item := range result.Items {
		var emote emote
		if err := dynamodbattribute.UnmarshalMap(item, &emote); err != nil {
			log.Fatal(err)
		}

		// Only emotes without an asset type need to be backfilled
		// Set their asset type to static before adding to backfill list
		if emote.AssetType == "" {
			emote.AssetType = string(mako.StaticAssetType)
			b.emotes = append(b.emotes, emote)
		}
	}

	return result.LastEvaluatedKey
}

func (b *emoteAssetTypeBackfiller) scanAndUpdateEmotes() {
	startKey := b.scanAndUpdateNext(nil)

	for startKey != nil {
		startKey = b.scanAndUpdateNext(startKey)
	}
}

func (b *emoteAssetTypeBackfiller) writeEmotesToDynamo() {
	var waitGroup sync.WaitGroup
	var emoteRecords = make(chan mako.Emote, len(b.emotes))
	var count int32

	waitGroup.Add(concurrentWaitGroups)
	for i := 0; i < concurrentWaitGroups; i++ {
		go func() {
			defer waitGroup.Done()

			if !*dryrun {
				for {
					select {
					case emoteRecord, ok := <-emoteRecords:
						if !ok {
							return
						}

						logfields := log.Fields{
							"emote_id":       emoteRecord.ID,
							"emote_code":     emoteRecord.Code,
							"emote_owner_id": emoteRecord.OwnerID,
						}

						b.limiter.Wait(context.Background())

						ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)

						err := b.emoteDBWriter.WriteEmotes(ctx, emoteRecord)
						if err != nil {
							log.WithFields(logfields).Error(err)
							cancel()
							continue
						}
						atomic.AddInt32(&count, 1)

						if count%1000 == 0 {
							fmt.Printf("progress: %d emotes written\n", count)
						}

						cancel()
					}
				}
			}
		}()
	}

	for _, emoteRecord := range b.emotes {
		emoteRecords <- emoteRecord.toMako()
	}

	close(emoteRecords)

	waitGroup.Wait()
}
