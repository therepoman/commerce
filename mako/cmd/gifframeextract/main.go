package main

import (
	"context"
	"flag"
	"log"
	"os"

	"code.justin.tv/commerce/mako/emote_utils"
)

const stagingUploadBucketName = "staging-emoticon-uploads"
const prodUploadBucketName = "prod-emoticon-uploads"
const stagingFramesBucketName = "staging-animated-emote-frames"
const prodFramesBucketName = "prod-animated-emote-frames"

var (
	emoteID = flag.String("emoteID", "", "Required. EmoteID for the animated emote for which you want to extract frames and upload them.")
	env     = flag.String("env", "staging", "Environment to run the script against (staging, prod). Defaults to staging")
)

func main() {
	flag.Parse()
	awsRegion := "us-west-2"

	uploadsBucket := stagingUploadBucketName
	framesBucket := stagingFramesBucketName
	if *env == "prod" {
		uploadsBucket = prodUploadBucketName
		framesBucket = prodFramesBucketName
	}

	if *emoteID == "" {
		log.Fatal("emoteID is required.")
	}

	logger := log.New(os.Stdout, "", log.LstdFlags)

	extractor := emote_utils.NewGifFrameExtractorWithS3(awsRegion, uploadsBucket, framesBucket, logger, nil)

	ctx := context.Background()
	log.Printf("Starting frame extraction. Env=%s EmoteID=%s", *env, *emoteID)
	err := extractor.ExtractAndUploadFrames(ctx, *emoteID)

	if err != nil {
		log.Fatal("error uploading frames: \n\t", err)
	}
	log.Println("Done")
}
