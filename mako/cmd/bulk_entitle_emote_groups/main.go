package main

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"io"
	"net/http"
	"os"
	"time"

	"code.justin.tv/commerce/mako/emote_utils"
	mako_client "code.justin.tv/commerce/mako/twirp"
	"github.com/golang/protobuf/ptypes"
)

var targetStaging = flag.Bool("staging", false, "Run bulk entitle emote groups against staging Mako.")
var targetProd = flag.Bool("prod", false, "Run bulk entitle emote groups against production Mako.")
var liveRun = flag.Bool("live", false, "Execute changes instead of performing default dry-run")
var shouldEntitle = flag.Bool("shouldEntitle", true, "Should we grant entitlements or delete entitlements")
var inputFilename = flag.String("input", "", "Filename containing Twitch User IDs, one per line")

var userIds = []string{
	"114571031", // tmiloadtesting
	"114745759", // tmiloadtesting2
	"114745798", // tmiloadtesting3
	"114745841", // tmiloadtesting4
	"114746009", // tmiloadtesting5
	"114746062", // tmiloadtesting6
	"114746104", // tmiloadtesting7
	"114746165", // tmiloadtesting8
	"114746216", // tmiloadtesting9
	"114746260", // tmiloadtesting10
	"120320915", // tmiloadtesting11
	"120321084", // tmiloadtesting12
	"120321163", // tmiloadtesting13
	"120321226", // tmiloadtesting14
	"120321283", // tmiloadtesting15
	"120321346", // tmiloadtesting16
	"120321632", // tmiloadtesting17
	"120321707", // tmiloadtesting18
	"120321754", // tmiloadtesting19
	"120321793", // tmiloadtesting20
}

var groupIds = []string{
	"442751796",
}

const channelID = "499158006" // qa_anarion_partner

func main() {
	flag.Parse()

	checkFatalErr(os.Stderr, validateFlags())

	makoHost := emote_utils.MakoStaging
	if *targetProd {
		makoHost = emote_utils.MakoProd
	}

	httpClient := &http.Client{
		Timeout: time.Duration(5 * time.Second),
	}

	makoClient := mako_client.NewMakoProtobufClient(makoHost, httpClient)
	entitlements := make([]*mako_client.EmoteGroupEntitlement, 0, len(userIds)*len(groupIds))
	start := ptypes.TimestampNow()

	for _, groupID := range groupIds {
		for _, userID := range userIds {
			entitlement := &mako_client.EmoteGroupEntitlement{
				OwnerId:  userID,
				ItemId:   channelID,
				GroupId:  groupID,
				OriginId: "Subscriptions",
				Group:    mako_client.EmoteGroup_Subscriptions,
				Start:    start,
				End: &mako_client.InfiniteTime{
					IsInfinite: true,
				},
			}
			entitlements = append(entitlements, entitlement)
		}
	}

	if *liveRun {
		if *shouldEntitle {
			fmt.Printf("Creating entitlements %+v", entitlements)
			createEmoteGroupEntitlementsRequest := &mako_client.CreateEmoteGroupEntitlementsRequest{
				Entitlements: entitlements,
			}
			_, err := makoClient.CreateEmoteGroupEntitlements(context.Background(), createEmoteGroupEntitlementsRequest)
			checkFatalErr(os.Stderr, err)
		} else {
			fmt.Printf("Deleting entitlements %+v", entitlements)
			deleteEmoteGroupEntitlementsRequest := &mako_client.DeleteEmoteGroupEntitlementsRequest{
				Entitlements: entitlements,
			}
			_, err := makoClient.DeleteEmoteGroupEntitlements(context.Background(), deleteEmoteGroupEntitlementsRequest)
			checkFatalErr(os.Stderr, err)
		}
	} else {
		fmt.Printf("Skipping entitlements for %+v", entitlements)
		os.Exit(0)
	}
}

func checkFatalErr(w io.Writer, err error) {
	if err != nil {
		fmt.Fprintln(w, err)
		os.Exit(1)
	}
}

func validateFlags() error {
	if *targetStaging == *targetProd {
		return errors.New("Exactly one of -staging or -prod must be specified")
	}
	return nil
}
