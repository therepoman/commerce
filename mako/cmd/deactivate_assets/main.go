package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"net/http"
	"strings"
	"time"

	"code.justin.tv/commerce/mako/config"
	mako_client "code.justin.tv/commerce/mako/twirp"
)

const (
	makoStagingURL = "https://main.us-west-2.beta.mako.twitch.a2z.com"
	makoProdURL    = "https://main.us-west-2.prod.mako.twitch.a2z.com"
)

var (
	env            = flag.String("env", "staging", "Environment to run the script against (staging, prod). Defaults to staging")
	live           = flag.Bool("live", false, "Whether to actually execute the changes or default to a dry-run")
	channelIDParam = flag.String("channel_id", "", "Channel whose assets you want to deactivate")
	delete         = flag.Bool("delete", false, "Whether we should delete the emotes from DynamoDB and S3.")
)

// TODO: add support for deactivating more than just emotes (aka future proof against domains)
func main() {
	flag.Parse()
	actuallyDelete := delete != nil && *delete

	cfg, err := config.LoadConfigForEnvironment(*env)
	if err != nil {
		log.Fatal(err)
	}

	log.Printf("Running script in the %s environment\n", strings.ToUpper(cfg.EnvironmentPrefix))

	// Initialize Mako client
	makoHost := makoStagingURL
	if *env == "prod" {
		makoHost = makoProdURL
	}
	makoClient := mako_client.NewMakoProtobufClient(makoHost, &http.Client{})

	if *channelIDParam == "" {
		log.Println("This script requires a channelIDParam in order to run")
		return
	}

	channelID := *channelIDParam

	if *env == "staging" {
		log.Println("Running in staging")
	}

	if *env == "prod" && *live {
		log.Println("!!!!Running for real in prod. Giving you 10 seconds to rethink this decision!!!!")
		time.Sleep(10 * time.Second)
	}

	ctx := context.Background()

	//Get all potentially usable emotes from the emotes dynamoDB
	allChannelEmotes, err := makoClient.GetAllEmotesByOwnerId(ctx, &mako_client.GetAllEmotesByOwnerIdRequest{
		OwnerId: channelID,
		Filter: &mako_client.EmoteFilter{
			Domain: mako_client.Domain_emotes,
		},
	})

	if err != nil {
		log.Println(fmt.Sprintf("Error trying to call GetAllEmotesByOwnerID with channelID=%s: %s", channelID, err.Error()))
		return
	}

	if allChannelEmotes == nil {
		log.Println("No emotes to deactivate")
		allChannelEmotes = &mako_client.GetAllEmotesByOwnerIdResponse{}
	}

	log.Println(fmt.Sprintf("Found %d emotes for channelID=%s", len(allChannelEmotes.GetEmoticons()), channelID))
	//Deactivate/delete all pending/active emoticons for channel_id
	for _, emote := range allChannelEmotes.GetEmoticons() {
		if actuallyDelete {
			if !*live {
				log.Printf("Dry-run: did not delete Emote %s\n", emote.Id)
				continue
			} else {
				log.Printf("Deleting Emote %s\n", emote.Id)
			}
		} else {
			if !*live {
				log.Printf("Dry-run: did not deactivate Emote %s\n", emote.Id)
				continue
			} else {
				log.Printf("Deactivating Emote %s\n", emote.Id)
			}
		}

		if actuallyDelete {
			_, err := makoClient.DeleteEmoticon(ctx, &mako_client.DeleteEmoticonRequest{
				Id:           emote.Id,
				DeleteImages: true,
			})
			if err != nil {
				log.Printf("Error trying to delete emote %s: %s\n", emote.Id, err.Error())
			}
		} else {
			_, err := makoClient.DeactivateEmoticon(ctx, &mako_client.DeactivateEmoticonRequest{
				Id: emote.Id,
			})
			if err != nil {
				log.Printf("Error trying to deactivate emote %s: %s\n", emote.Id, err.Error())
			}
		}
	}

	if actuallyDelete {
		log.Println("Done deleting emotes")
	} else {
		log.Println("Done deactivating emotes")
	}
}
