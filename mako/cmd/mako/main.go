package main

import (
	"context"
	"math/rand"
	"net/http"
	_ "net/http/pprof"
	"os"
	"syscall"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/sqs/sqsiface"
	"github.com/cactus/go-statsd-client/statsd"
	events "github.com/segmentio/events/v2"

	"code.justin.tv/commerce/dynamicconfig"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/mako/api"
	"code.justin.tv/commerce/mako/clients"
	"code.justin.tv/commerce/mako/config"
	"code.justin.tv/commerce/mako/dynamo"
	"code.justin.tv/commerce/mako/prefixes"
	"code.justin.tv/commerce/mako/setup"
	"code.justin.tv/commerce/mako/sqs"
	"code.justin.tv/commerce/mako/sqs/workers"
	eventbus "code.justin.tv/eventbus/client"
	"code.justin.tv/eventbus/client/subscriber/sqsclient"
	"code.justin.tv/eventbus/schema/pkg/user"
	"code.justin.tv/video/autoprof/profs3"
)

const (
	sqsShutdownTimeout    = 5 * time.Second
	serverShutdownTimeout = 10 * time.Second
	devEnvironment        = "dev"
)

var startup time.Time

func main() {
	startup = time.Now()

	rand.Seed(time.Now().UTC().UnixNano()) // Initialize RNG seed

	setupParams := setup.SetupMako()

	poller := dynamicconfig.Poller{
		Reloader: setupParams.DynamicConfig,
		Interval: 30 * time.Second,
		Timeout:  30 * time.Second,
	}
	go poller.Poll()
	defer poller.Stop()

	campaignEmoteOwnersS3Poller := dynamicconfig.Poller{
		Reloader: setupParams.DynamicS3Configs.CampaignEmoteOwnersDynamicS3Config,
		Interval: 10 * time.Second,
		Timeout:  10 * time.Second,
	}
	go campaignEmoteOwnersS3Poller.Poll()
	defer campaignEmoteOwnersS3Poller.Stop()

	knownOrVerifiedBotsS3Poller := dynamicconfig.Poller{
		Reloader: setupParams.DynamicS3Configs.KnownOrVerifiedBotsDynamicS3Config,
		Interval: 5 * time.Minute,
		Timeout:  1 * time.Minute,
	}
	go knownOrVerifiedBotsS3Poller.Poll()
	defer knownOrVerifiedBotsS3Poller.Stop()

	defaultEmoteImagesS3Poller := dynamicconfig.Poller{
		Reloader: setupParams.DynamicS3Configs.DefaultEmoteImageDataDynamicS3Config,
		Interval: 5 * time.Minute,
		Timeout:  1 * time.Minute,
	}
	go defaultEmoteImagesS3Poller.Poll()
	defer defaultEmoteImagesS3Poller.Stop()

	// Start polling for experiment global overrides
	setupParams.Clients.ExperimentsClient.StartGlobalOverridePolling()

	// SQS
	workerManagers := setupSQSWorkers(setupParams, setupParams.Clients.SQSClient)
	setupEventBusSQSWorkers(setupParams)

	setupAutoprof(setupParams.Config)

	// Initialize the web server and start listening
	serverErr := startWebServer(setupParams.Config, setupParams.PrefixDao, setupParams.Stats, setupParams.Clients, setupParams.DynamicS3Configs, setupParams.DynamicConfig)

	start := time.Now()

	// Stop polling from SQS, stop the workers from pulling new messages, and wait for them to drain.
	log.Info("Shutting down SQS workers...")
	for _, manager := range workerManagers {
		if err := manager.Shutdown(sqsShutdownTimeout); err != nil {
			log.WithError(err).Errorf("Error shutting down SQS WorkerManager, manager: %v", manager)
		}
	}

	// Shutdown clients that need to be explicitly shutdown
	shutdownClients(setupParams)

	if serverErr != nil {
		log.Fatalf("Error during server shutdown: %v", serverErr)
	}

	log.Infof("Shutdown complete in %v", time.Since(start))
}

func startWebServer(cfg *config.Configuration, prefixesDao dynamo.IPrefixDao, stats statsd.Statter, clients *clients.Clients, dynamicS3Configs *config.DynamicS3Configs, dynamicConfig *config.Dynamic) error {
	entitlementsAPI := api.EntitlementsAPI{}

	emoteSetDetailsAPI := api.EmoteSetDetailsAPI{
		Clients:        *clients,
		CampaignConfig: dynamicS3Configs.CampaignEmoteOwnersDynamicS3Config,
		Config:         cfg,
	}

	emoticonsByIdsAPI := api.EmoticonsByEmoticonIdsAPI{
		Clients: *clients,
	}

	emoticonsByGroupsAPI := api.EmoticonsByGroupsAPI{
		Clients: *clients,
		Stats:   stats,
	}

	healthAPI := api.HealthAPI{}

	activeEmoteCodes := api.ActiveEmoteCodesAPI{}

	uploadEmoticonAPI := api.GetEmoticonUploadConfigurationAPI{}

	uploadEmoteAPI := api.NewGetEmoteUploadConfigAPI(cfg, clients.ImageUploader, clients.EmoteUploadMetadataCache)

	createEmoticonAPI := api.CreateEmoticonAPI{
		Clients: *clients,
		Stats:   stats,
	}

	createEmoteAPI := api.CreateEmoteAPI{
		Clients:       *clients,
		DynamicConfig: dynamicConfig,
		Stats:         stats,
	}

	deleteEmoticonAPI := api.DeleteEmoticonAPI{
		Clients: *clients,
	}

	activateEmoticonAPI := api.ActivateEmoticonAPI{
		Clients: *clients,
	}

	deactivateEmoticonAPI := api.DeactivateEmoticonAPI{
		Clients: *clients,
	}

	emoteOrdersAPI := api.EmoteOrdersAPI{
		EmoticonsManager: clients.EmoticonsManager,
	}

	emoteGroupAssignAPI := api.EmoteGroupAssignAPI{
		EmoticonsManager: clients.EmoticonsManager,
		DynamicConfig:    dynamicConfig,
		EmoteGroupDB:     clients.EmoteGroupDB,
		EmoteDB:          clients.EmoteDB,
	}

	updateEmoteOriginsAPI := api.UpdateEmoteOriginsAPI{EmoticonsManager: clients.EmoticonsManager}

	updateEmoteCodeSuffixAPI := api.UpdateEmoteCodeSuffixAPI{
		Clients: *clients,
	}

	userEmoteStandingAPI := api.UserEmoteStandingAPI{
		Clients: *clients,
	}

	setUserEmoteStandingOverrideAPI := api.SetUserEmoteStandingOverrideAPI{
		Clients: *clients,
	}

	smiliesAPI := api.SmiliesAPI{
		Clients: *clients,
	}

	krakenEmoticonsAPI := api.LegacyEmoticonsAPI{
		Clients: *clients,
		Config:  cfg,
	}

	getAllEmotesByOwnerAPI := api.EmoticonsByOwnerAPI{
		Clients: *clients,
		EmoteDB: clients.EmoteDB,
	}

	emoteEntitlementsAPI := api.EmoteEntitlementsAPI{
		Clients: *clients,
		Config:  cfg,
	}

	createEmoteEntitlementsAPI := api.CreateEmoteEntitlementsAPI{
		Clients: *clients,
		Config:  cfg,
	}

	create3PEmoteEntitlementsAPI := api.Create3PEmoteEntitlementsAPI{
		Clients:       *clients,
		DynamicConfig: dynamicConfig,
	}

	pendingEmoticonsAPI := api.PendingEmoticonsAPI{
		Clients: *clients,
	}

	createEmoteGroupEntitlementsAPI := api.CreateEmoteGroupEntitlementsAPI{
		Clients: *clients,
	}

	deleteEmoteGroupEntitlementsAPI := api.DeleteEmoteGroupEntitlementsAPI{
		Clients: *clients,
	}

	emoteCodesUniqueAPI := api.EmoteCodeUniqueAPI{
		Clients: *clients,
	}

	emoteCodesAcceptableAPI := api.EmoteCodeAcceptableAPI{
		Clients: *clients,
	}

	createEmoteGroupAPI := api.CreateEmoteGroupAPI{
		Clients: *clients,
	}

	deleteEmoteGroupAPI := api.DeleteEmoteGroupAPI{
		Clients: *clients,
	}

	getEmotesByCodes := api.GetEmotesByCodesAPI{
		Clients:       *clients,
		Stats:         stats,
		DynamicConfig: dynamicConfig,
	}

	getUserChannelEmotesAPI := api.GetUserChannelEmotesAPI{
		Clients:       *clients,
		Stats:         stats,
		DynamicConfig: dynamicConfig,
		Config:        cfg,
	}

	emoteModifierGroupsAPIs := api.EmoteModifierGroupsAPIs{
		Clients: *clients,
	}

	recommender := prefixes.NewPrefixRecommender(prefixesDao, stats, clients)

	getEmotesByGroupsAPI := api.GetEmotesByGroupsAPI{
		Clients: *clients,
	}

	getEmotesByIDsAPI := api.EmotesByIDsAPI{
		Clients: *clients,
		Stats:   stats,
	}

	userEmoteLimitsAPI := api.UserEmoteLimitsAPI{
		Clients: *clients,
	}

	getUserFollowerEmoteStatusAPI := api.GetUserFollowerEmoteStatusAPI{
		Clients:       *clients,
		DynamicConfig: dynamicConfig,
	}

	getAvailableFollowerEmotes := api.GetAvailableFollowerEmotesAPI{
		Clients:       *clients,
		DynamicConfig: dynamicConfig,
	}

	userRolloutStatusAPI := api.UserRolloutStatusAPI{}

	defaultEmoteImagesAPI := api.DefaultEmoteImagesAPI{
		DefaultEmoteImagesDataDynamicConfig: dynamicS3Configs.DefaultEmoteImageDataDynamicS3Config,
	}

	userProductEmoteGroupsAPI := api.UserProductEmoteGroupsAPI{
		Clients: *clients,
	}

	channelEmoteUsageAPI := api.GetChannelEmoteUsageAPI{
		Clients: *clients,
	}

	makoServer := api.MakoServer{
		EntitlementsAPI:                   entitlementsAPI,
		HealthAPI:                         healthAPI,
		EmoteSetDetailsAPI:                emoteSetDetailsAPI,
		EmoticonsByGroupsAPI:              emoticonsByGroupsAPI,
		EmoticonsByEmoticonIdsAPI:         emoticonsByIdsAPI,
		ActiveEmoteCodesAPI:               activeEmoteCodes,
		GetEmoticonUploadConfigurationAPI: uploadEmoticonAPI,
		GetEmoteUploadConfigAPI:           uploadEmoteAPI,
		CreateEmoticonAPI:                 createEmoticonAPI,
		CreateEmoteAPI:                    createEmoteAPI,
		DeleteEmoticonAPI:                 deleteEmoticonAPI,
		ActivateEmoticonAPI:               activateEmoticonAPI,
		DeactivateEmoticonAPI:             deactivateEmoticonAPI,
		EmoteOrdersAPI:                    emoteOrdersAPI,
		EmoteGroupAssignAPI:               emoteGroupAssignAPI,
		UserEmoteStandingAPI:              userEmoteStandingAPI,
		SetUserEmoteStandingOverrideAPI:   setUserEmoteStandingOverrideAPI,
		SmiliesAPI:                        smiliesAPI,
		LegacyEmoticonsAPI:                krakenEmoticonsAPI,
		UpdateEmoteCodeSuffixAPI:          updateEmoteCodeSuffixAPI,
		PrefixesAPI:                       api.NewPrefixesAPI(prefixesDao, cfg, clients, recommender),
		EmoticonsByOwnerAPI:               getAllEmotesByOwnerAPI,
		EmoteEntitlementsAPI:              emoteEntitlementsAPI,
		CreateEmoteEntitlementsAPI:        createEmoteEntitlementsAPI,
		Create3PEmoteEntitlementsAPI:      create3PEmoteEntitlementsAPI,
		PendingEmoticonsAPI:               pendingEmoticonsAPI,
		CreateEmoteGroupEntitlementsAPI:   createEmoteGroupEntitlementsAPI,
		DeleteEmoteGroupEntitlementsAPI:   deleteEmoteGroupEntitlementsAPI,
		EmoteCodeUniqueAPI:                emoteCodesUniqueAPI,
		EmoteCodeAcceptableAPI:            emoteCodesAcceptableAPI,
		CreateEmoteGroupAPI:               createEmoteGroupAPI,
		DeleteEmoteGroupAPI:               deleteEmoteGroupAPI,
		GetEmotesByCodesAPI:               getEmotesByCodes,
		EmoteModifierGroupsAPIs:           emoteModifierGroupsAPIs,
		GetEmotesByGroupsAPI:              getEmotesByGroupsAPI,
		EmotesByIDsAPI:                    getEmotesByIDsAPI,
		UpdateEmoteOriginsAPI:             updateEmoteOriginsAPI,
		UserEmoteLimitsAPI:                userEmoteLimitsAPI,
		GetUserFollowerEmoteStatusAPI:     getUserFollowerEmoteStatusAPI,
		GetUserChannelEmotesAPI:           getUserChannelEmotesAPI,
		GetAvailableFollowerEmotesAPI:     getAvailableFollowerEmotes,
		UserRolloutStatusAPI:              userRolloutStatusAPI,
		DefaultEmoteImagesAPI:             defaultEmoteImagesAPI,
		UserProductEmoteGroupsAPI:         userProductEmoteGroupsAPI,
		GetChannelEmoteUsageAPI:           channelEmoteUsageAPI,
	}

	handler := makoServer.NewServer(stats, cfg)
	srv := http.Server{
		Addr:    ":8000",
		Handler: handler,
	}

	signalCtx, cancel := events.WithSignals(context.Background(), os.Interrupt, os.Kill, syscall.SIGTERM)

	go func() {
		log.Infof("starting http server on '%v' took %v", srv.Addr, time.Since(startup))

		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.WithError(err).Error("http server error")
			return
		}

		log.Info("http server goroutine shutdown")
	}()

	<-signalCtx.Done()
	cancel()

	log.Info("shutting down mako")

	shutdownCtx, cancel := context.WithTimeout(context.Background(), serverShutdownTimeout)
	defer cancel()

	return srv.Shutdown(shutdownCtx)
}

// Set up all SQS workers. They will automatically start polling and processing their respective queues.
// Returns a list of managers for the workers
func setupSQSWorkers(setupParams *setup.SetupParams, sqsClient sqsiface.SQSAPI) []*sqs.WorkerManager {
	var workerManagers []*sqs.WorkerManager

	eventsWorker := workers.NewEventsWorker(setupParams.Delegator)
	workerManagers = append(workerManagers, sqs.NewWorkerManager(setupParams.Config.EventsSQSQueueName, setupParams.Config.EventsSQSWorkerCount, sqsClient, eventsWorker, setupParams.Stats))

	prefixUpdateWorker := workers.NewPrefixUpdateWorker(setupParams.PrefixUpdateProcessor)
	workerManagers = append(workerManagers, sqs.NewWorkerManager(setupParams.Config.PrefixUpdatesSQSQueueName, setupParams.Config.PrefixUpdatesSQSWorkerCount, sqsClient, prefixUpdateWorker, setupParams.Stats))

	return workerManagers
}

// Set up all Event Bus SQS workers. They are separate from the above workers because Event Bus vends a mux
// and separate SQS client implementation. All workers can share the same mux.
// https://git.xarth.tv/pages/eventbus/docs/subscribers/#handle-events-in-your-application
func setupEventBusSQSWorkers(setupParams *setup.SetupParams) {
	mux := eventbus.NewMux()

	// Add new SQS workers here
	userDestroyWorker := workers.NewUserDestroyWorker(setupParams.Clients.StepFunctionsClient)
	user.RegisterDestroyHandler(mux, userDestroyWorker.Handle)

	sess, err := session.NewSession(&aws.Config{
		Region: aws.String(setupParams.Config.AWSRegion),
	})
	if err != nil {
		log.WithError(err).Error("Could not start new aws session")
		return
	}

	eventBusSQSClient, err := sqsclient.New(sqsclient.Config{
		Session:    sess,
		Dispatcher: mux.Dispatcher(),
		QueueURL:   setupParams.Config.UserDestroySQSQueueURL,
	})
	if err != nil {
		log.Fatalf("Error starting eventbus SQS client: %s", err.Error())
	}

	setupParams.EventBusSQSClient = eventBusSQSClient
}

func setupAutoprof(serviceConfig *config.Configuration) {
	sess, err := session.NewSession(aws.NewConfig().WithRegion(serviceConfig.AWSRegion))
	if err != nil {
		log.WithError(err).Error("failed to create new AWS session while bootstrapping autoprof")
		return
	}

	collector := profs3.Collector{
		S3:       s3.New(sess),
		S3Bucket: serviceConfig.AutoprofS3BucketName,
		OnError: func(err error) error {
			log.WithError(err).Error("error from autoprof S3 collector")
			return nil
		},
	}

	go collector.Run(context.Background())
}

func shutdownClients(setupParams *setup.SetupParams) {
	_ = setupParams.Clients.ExperimentsClient.Close()
	_ = setupParams.Stats.Close()
	_ = setupParams.Clients.EmoteDB.Close()
	_ = setupParams.EventBusSQSClient.Shutdown()
}
