package main

import (
	"flag"
	"fmt"

	eventbus "code.justin.tv/eventbus/client"
	"code.justin.tv/eventbus/client/testhelpers"
	"code.justin.tv/eventbus/schema/pkg/eventbus/change"
	schema "code.justin.tv/eventbus/schema/pkg/user_multi_factor_auth"
)

var (
	userID      = flag.String("userID", "", "userID of user who enabled or disabled 2FA")
	updatedAuth = flag.Bool("updatedAuth", false, "updatedAuth should be true if a user updated their 2FA")
	enabledAuth = flag.Bool("enabledAuth", false, "authEnabled should be true if a user enabled 2FA on their account")
	env         = flag.String("env", "staging", "Environment (staging or prod)")
)

const (
	stagingSQSUrl = "https://sqs.us-west-2.amazonaws.com/077362005997/eventbus_two_factor_entitlements_staging"
	prodSQSUrl    = "https://sqs.us-west-2.amazonaws.com/671452935478/eventbus_two_factor_entitlements_prod"
)

func main() {
	flag.Parse()
	message := &schema.Update{
		UserId: *userID,
		AnyEnabled: &change.BoolChange{
			Updated: *updatedAuth,
			Value:   *enabledAuth,
		},
	}

	raw, err := testhelpers.MakePayload(&eventbus.Header{}, message)
	if err != nil {
		panic(err)
	}

	snsMessage := raw.FakeSNSMessage()
	body, err := snsMessage.SQSBody()
	if err != nil {
		panic(err)
	}

	sqsUrl := stagingSQSUrl
	if *env == "prod" {
		sqsUrl = prodSQSUrl
	}

	fmt.Printf(`aws sqs send-message --queue-url "%s" --message-body '%s'`, sqsUrl, body)
}
