package main

import (
	"bufio"
	"context"
	"errors"
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	"code.justin.tv/commerce/mako/emote_utils"
	mako_client "code.justin.tv/commerce/mako/twirp"
)

var targetStaging = flag.Bool("staging", false, "Run bulk entitle emote groups against staging Mako.")
var targetProd = flag.Bool("prod", false, "Run bulk entitle emote groups against production Mako.")
var liveRun = flag.Bool("live", false, "Execute changes instead of performing default dry-run")
var shouldEntitle = flag.Bool("shouldEntitle", true, "Should we grant entitlements or delete entitlements")
var inputFilename = flag.String("input", "", "Filename containing Twitch User IDs, one per line")

func main() {
	flag.Parse()
	checkFatalErr(os.Stderr, validateFlags())

	fmt.Println("Please make sure to be on the Twitch VPN...")

	makoHost := emote_utils.MakoStaging
	if *targetProd {
		makoHost = emote_utils.MakoProd
	}

	httpClient := &http.Client{
		Timeout: time.Duration(5 * time.Second),
	}

	makoClient := mako_client.NewMakoProtobufClient(makoHost, httpClient)

	file, err := os.Open(*inputFilename)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	lineNumber := 0
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lineNumber += 1
		ownerId := strings.TrimSpace(scanner.Text())
		if ownerId != "" && ownerId[0] == '#' {
			// Allow input lines to be commented out for efficienty in case
			// the script needs to be re-run.  The script should be
			// idempotent but it still takes time to make failed
			// requests for already-existing groups
			continue
		}
		fmt.Println(ownerId)
		if *liveRun {
			_, err := makoClient.CreateEmoteGroup(
				context.Background(),
				&mako_client.CreateEmoteGroupRequest{
					Origin:    mako_client.EmoteGroup_Follower,
					GroupType: mako_client.EmoteGroupType_static_group,
					OwnerId:   ownerId,
				},
			)
			if err != nil {
				// Errors here are not fatal to the script, as the error might
				// be due to the group already existing.  I don't believe the client, as written
				// will return an error so that we can easily detect this.
				fmt.Printf("Error creating emote group for %s: %v\n", ownerId, err)
			}
		}
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

}

func checkFatalErr(w io.Writer, err error) {
	if err != nil {
		fmt.Fprintln(w, err)
		os.Exit(1)
	}
}

func validateFlags() error {
	if *targetStaging == *targetProd {
		return errors.New("Exactly one of -staging or -prod must be specified")
	}
	if *inputFilename == "" {
		return errors.New("-input must be specified")
	}

	return nil
}
