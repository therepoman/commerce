package main

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetManifest(t *testing.T) {
	testCases := []struct {
		testDataDir string
		want        *Manifest
		wantErr     bool
	}{
		{
			testDataDir: "testdata/manifest_golden",
			want: &Manifest{
				EmoteGroups: []*EmoteGroup{
					{
						Name: "ticket-product-1-emote",
						Emotes: []*Emote{
							{
								Regex: "TESTSnowahl",
							},
						},
					},
					{
						Name: "ticket-product-no-emote",
					},
					{
						Name: "ticket-product-multiple-emotes",
						Emotes: []*Emote{
							{
								Regex: "TESTSeemsMinton",
							},
							{
								Regex: "TESTBergk",
							},
						},
					},
					{
						Name: "ticket-product-1-emote-dir",
						Emotes: []*Emote{
							{
								Regex: "TESTDoomfist",
								Dir:   "doom_fist",
							},
						},
					},
				},
			},
			wantErr: false,
		},
		{
			testDataDir: "testdata/manifest_key_misspelling",
			want: &Manifest{
				EmoteGroups: []*EmoteGroup{
					{
						Name: "test1",
					},
					{
						Name: "test3",
					},
				},
			},
			wantErr: true,
		},
	}

	for _, tt := range testCases {
		t.Run(tt.testDataDir, func(t *testing.T) {
			oldDir, err := os.Getwd()
			if err != nil {
				t.Fatal(err)
			}
			defer func() {
				os.Chdir(oldDir)
			}()

			err = os.Chdir(tt.testDataDir)
			if err != nil {
				t.Fatal(err)
			}

			manifest, err := getManifest()
			gotErr := err != nil
			if gotErr {
				t.Logf("Got getManifest err (may not indicate test failure):\n%v\n", err)
			}

			assert.Equal(t, tt.wantErr, gotErr, "wantErr: %v != gotErr: %v", tt.wantErr, gotErr)

			assert.Equal(t, tt.want, manifest)
		})
	}
}
