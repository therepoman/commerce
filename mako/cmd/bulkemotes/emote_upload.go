package main

import (
	"context"
	"fmt"
	"io/ioutil"
	"path/filepath"

	"code.justin.tv/commerce/mako/emote_utils"
	mkd "code.justin.tv/commerce/mako/twirp/dashboard"
	"github.com/pkg/errors"
)

var emoteSizeToImagePath = map[mkd.EmoteImageSize]string{
	mkd.EmoteImageSize_size_1x: "28.png",
	mkd.EmoteImageSize_size_2x: "56.png",
	mkd.EmoteImageSize_size_4x: "112.png",
}

func (b *BulkUploadExecutor) UploadImages(emoteGroup EmoteGroupState, emote EmoteState) (*mkd.GetEmoteUploadConfigResponse, error) {
	uploadEmote, err := verifyImagesAndConstructEmoteForUpload(emote)
	if err != nil {
		return nil, err
	}

	uploadConfigurations, err := emote_utils.GetUploadConfigForEmote(emoteGroup.OwnerID, false, false, false, b.MakoDashboard)
	if err != nil {
		return uploadConfigurations, err
	}

	err = emote_utils.UploadImagesForEmote(uploadEmote, uploadConfigurations, b.HttpClient, b.Uploader)
	if err != nil {
		return uploadConfigurations, err
	}

	return uploadConfigurations, nil
}

func (b *BulkUploadExecutor) CreateEmote(emoteGroup EmoteGroupState, emote EmoteState) error {
	ctx := context.Background()
	configResp, err := b.UploadImages(emoteGroup, emote)
	if err != nil {
		return err
	}

	uploadConfigs := make(map[mkd.EmoteImageSize]mkd.EmoteUploadConfig)
	for _, uploadConfig := range configResp.GetUploadConfigs() {
		if uploadConfig != nil {
			uploadConfigs[uploadConfig.Size] = *uploadConfig
		}
	}

	req := mkd.CreateEmoteRequest{
		UserId:     emoteGroup.OwnerID,
		Code:       emote.Regex,
		CodeSuffix: emote.Regex,
		GroupIds:   []string{emoteGroup.ID},
		ImageAssets: []*mkd.EmoteImageAsset{
			{
				ImageIds: &mkd.EmoteImageIds{
					Image1XId: uploadConfigs[mkd.EmoteImageSize_size_1x].Images[0].Id,
					Image2XId: uploadConfigs[mkd.EmoteImageSize_size_2x].Images[0].Id,
					Image4XId: uploadConfigs[mkd.EmoteImageSize_size_4x].Images[0].Id,
				},
				AssetType: mkd.EmoteAssetType_static,
			},
		},
		Origin:                    emoteGroup.Type,
		State:                     mkd.EmoteState_active,
		EnforceModerationWorkflow: false,
		AnimatedEmoteTemplate:     mkd.AnimatedEmoteTemplate_NO_TEMPLATE,
	}

	_, _ = fmt.Fprintf(b.LogWriter, "All upload workflows successful. Creating Mako emoticon. regex=\"%s\"\n", emote.Regex)

	// when each upload status finishes, create emote via mako
	makoResp, err := b.MakoDashboard.CreateEmote(ctx, &req)
	if err != nil {
		return err
	}
	emote.ID = makoResp.Id
	emote.Created = true

	_, _ = fmt.Fprintf(b.LogWriter, "Successfully created emoticon in Mako!\n  %+v\n", emote)

	return nil
}

//TODO: validate sizes
func verifyImagesAndConstructEmoteForUpload(emote EmoteState) (emote_utils.Emote, error) {
	images := map[mkd.EmoteImageSize]string{}
	files, err := ioutil.ReadDir(emote.Dir)
	if err != nil {
		return emote_utils.Emote{}, err
	}

expected:
	for size, expectedFilename := range emoteSizeToImagePath {
		for _, file := range files {
			if file.Name() == expectedFilename {
				images[size] = filepath.Join(emote.Dir, file.Name())
				continue expected
			}
		}
		return emote_utils.Emote{}, errors.Errorf("Image file missing for emote.\nregex=\"%s\" dir=\"%s\" missing file=\"%s\"",
			emote.Regex, emote.Dir, expectedFilename)
	}

	return emote_utils.Emote{
		Regex:  emote.Regex,
		Images: images,
	}, nil
}
