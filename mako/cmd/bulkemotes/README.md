# bulkemotes

## Introduction
`package cmd/bulkemotes` is a command-line utility that implements bulk emote creation. 
We found that the manual creation process for global emote rewards tedious and labor intensive, `bulkemotes` is designed to use a manifest file that specifies emote upload order canonically, helping ensure that emotes display as desired.

## Overview
`bulkemotes` is run from the command line. It should be run within a directory that contains a `manifest.toml` file, as well as a directory for each emote to be uploaded. 
The `manifest.toml` file specifies the emote groups to create and the regexes of each emote to create within a given group. 
Additionally, the order of emote declarations in `manifest.toml` determines their upload order and consequently their display order.

## Declarations and folder structure

A single emote_group/emote declaration within `manifest.toml` looks like this:
```toml
[[emote_group]]
    name = "hype_train_pets"
    ownerID = "139075904"
    type = "HypeTrain"

    [[emote_group.emote]]
        regex = "OWLgg"
```
Note that in TOML, indentation is not significant; it's the order of declarations that determines nesting.
Fields for `emote_group` are:
* `ownerID` - owner of emotes and group, totally required, but you probably intend to use `qa_tw_partner (139075904)` 
* `type` - type of emotes, HypeTrain, BitsBadgeTierEmotes, Subscriptions or Follower.
* `name` - (OPTIONAL) does nothing, used for your notation
* `ID` - (OPTIONAL) if you provide this we won't make groups and instead will use this one.

For each emote declared in the manifest, there should be a directory containing the image assets for that emote. `bulkemotes` will by default look for a directory whose name matches the regex of the declared emote, but this can be overridden by adding a `dir` field to the emote declaration. 
The images within the directory should be named `28.png` (for the 28x28px size), `56.png` (56x56), and `112.png` (112x112). `bulkemotes` will halt execution if any image is missing.

Example folder structure:
```
example_bulkemotes/
    manifest.toml # -> declares OWLgg, OWLhighnoon
    OWLgg/
        28.png
        56.png
        112.png
    OWLhighnoon/
        28.png
        56.png
        112.png

```

See `cmd/bulkemotes/examples/emotes_to_upload` for a fleshed-out example data set.

## Running the command

To run this script and have it hit Mako and Upload Service you will need to setup a local proxy.
I recommend doing this via teleport-bastion and SSH using this command `teleport-bastion -y enable;TC=mako-prod ssh -q -N -D 8800 -C jumpbox`.
After this configure it using the environment variable (that go will respect) `HTTPS_PROXY`, or in usage `export HTTP_PROXY="socks5://localhost:8800"` before you run the script.

`bulkemotes` accepts three flags and no arguments. It should be run from within the folder that contains the `manifest.toml` file for the batch of emotes you want to upload.

The `-staging` flag tells `bulkemotes` to run against staging backends (Mako/Uploader).

The `-prod` flag tells `bulkemotes` to run against prod backends (Mako/Uploader).

The program will verify that one, and only one, of the environment flags is specified.

By default, `bulkemotes` runs a _dry run_. During a dry run, the command will:
1. Read manifest.toml and validate schema and config (helping safeguard against typos in the configuration).
1. Print a list of the ticket products and emotes it will create to stdout.
1. Exit.

Passing the `-live` flag to the command will switch to a _live run_, meaning `bulkemotes` will actually create and modify the ticket product and emote entities in the specified backends. During a live run, the command will:
1. Read manifest.toml and check that there are no unknown keys (helping safeguard against typos in the configuration).
1. Loop one-by-one through the declared emote groups. For each emote group in sequence, it will:
    1. Create the emote group in Mako.
    1. Loop one-by-one through the emote group emote declarations. For each emote declaration in sequence, it will:
        1. Verify that all expected image files exist in the emote's corresponding directory.
        1. Call `Mako(Dashboard).GetEmoteUploadConfigRequest` to get image upload urls
        1. Upload each corresponding image file to the configured `UploadURL` returned by Mako.
        1. Call `Mako(Dashboard).CreateEmote`, setting `UserId` to the TUID from the `manifest.toml`, the `Code` and `CodeSuffix` to the declared regex of the emote, the `GroupId` to the emote set ID we created earlier.

The command generally halts on _any_ error. It generously logs the actions that it's taking at each step (and in live runs, also writes to a logfile in the name format `bulkemotes_$UNIX-TIMESTAMP.log`) to aid with recovering from a partial execution. (TODO: Allow declaring a ticket product that already exists and merging new emotes into it.)
