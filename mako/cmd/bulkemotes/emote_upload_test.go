package main

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestVerifyImagesExist(t *testing.T) {
	testCases := []struct {
		testDataDir string

		emote   EmoteState
		wantErr bool
	}{
		{
			testDataDir: "testdata/emote_images_golden",
			emote: EmoteState{
				Regex: "RWD0Bubsy",
			},
			wantErr: false,
		},
		{
			testDataDir: "testdata/emote_images_missing",
			emote: EmoteState{
				Regex: "RWD2Bergk",
			},
			wantErr: true,
		},
		{
			testDataDir: "testdata/emote_images_wrong_name",
			emote: EmoteState{
				Regex: "RWD0SeemsMinton",
			},
			wantErr: true,
		},
	}

	for _, tt := range testCases {
		t.Run(tt.testDataDir, func(t *testing.T) {
			oldDir, err := os.Getwd()
			if err != nil {
				t.Fatal(err)
			}
			defer func() {
				os.Chdir(oldDir)
			}()

			err = os.Chdir(tt.testDataDir)
			if err != nil {
				t.Fatal(err)
			}

			_, testErr := verifyImagesAndConstructEmoteForUpload(tt.emote)
			assert.EqualValues(t, tt.wantErr, testErr != nil, "got err %v", testErr)
		})
	}
}
