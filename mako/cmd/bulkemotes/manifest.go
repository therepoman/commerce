package main

import (
	mako "code.justin.tv/commerce/mako/twirp"
	mkd "code.justin.tv/commerce/mako/twirp/dashboard"
	"github.com/BurntSushi/toml"
	"github.com/pkg/errors"
)

// Manifest corresponds to the document described by manifest.toml
type Manifest struct {
	EmoteGroups []*EmoteGroup `toml:"emote_group"`
}

func (m *Manifest) validate() error {
	// do some validation before we try to process the thing
	for _, emoteGroup := range m.EmoteGroups {
		if len(emoteGroup.OwnerID) == 0 {
			return errors.Errorf("OwnerID cannot be empty")
		}
		if len(emoteGroup.Type) == 0 {
			return errors.Errorf("Emote type for group cannot be empty")
		}
		if emoteGroup.getType() == mako.EmoteGroup_None {
			return errors.Errorf("Unsupported emote group, supported groups are HypeTrain, BitsBadgeTierEmotes, Subscriptions or Follower")
		}
		for _, emote := range emoteGroup.Emotes {
			if len(emote.Regex) == 0 {
				return errors.Errorf("Emote regex cannot be empty")
			}
		}
	}
	return nil
}

// EmoteGroup corresponds to an entry of the [[emote_group]] array in manifest.toml
type EmoteGroup struct {
	ID      string   `toml:"ID"`
	Name    string   `toml:"name"` // totally unused, just for reference
	OwnerID string   `toml:"ownerID"`
	Type    string   `toml:"type"`
	Emotes  []*Emote `toml:"emote"`
}

func (eg EmoteGroup) getType() mako.EmoteGroup {
	// this is an abbreviated list to just the supported types in Mako
	// reference: https://git.xarth.tv/commerce/mako/blob/master/api/create_emote_group.go#L17

	switch eg.Type {
	case "HypeTrain":
		return mako.EmoteGroup_HypeTrain
	case "BitsBadgeTierEmotes":
		return mako.EmoteGroup_BitsBadgeTierEmotes
	case "Subscriptions":
		return mako.EmoteGroup_Subscriptions
	case "Follower":
		return mako.EmoteGroup_Follower
	default:
		return mako.EmoteGroup_None
	}
}

func (eg EmoteGroup) getDashboardType() mkd.EmoteOrigin {
	// this is an abbreviated list to just the supported types in Mako
	// reference: https://git.xarth.tv/commerce/mako/blob/master/api/create_emote_group.go#L17

	switch eg.Type {
	case "HypeTrain":
		return mkd.EmoteOrigin_HypeTrain
	case "BitsBadgeTierEmotes":
		return mkd.EmoteOrigin_BitsBadgeTierEmotes
	case "Subscriptions":
		return mkd.EmoteOrigin_Subscriptions
	case "Follower":
		return mkd.EmoteOrigin_Follower
	default:
		return mkd.EmoteOrigin_None
	}
}

func (eg EmoteGroup) toEmoteGroupState(groupId string) EmoteGroupState {
	var emotes []EmoteState
	for _, emote := range eg.Emotes {
		emotes = append(emotes, emote.toEmoteState(groupId))
	}

	return EmoteGroupState{
		OwnerID: eg.OwnerID,
		ID:      groupId,
		Emotes:  emotes,
		Type:    eg.getDashboardType(),
	}
}

// Emote corresponds to an entry of the [[emote_group.emote]] arrays in manifest.toml
type Emote struct {
	Regex string
	Dir   string
}

func (e *Emote) GetDir() string {
	if e.Dir != "" {
		return e.Dir
	}

	return e.Regex
}

func (e Emote) toEmoteState(groupId string) EmoteState {
	return EmoteState{
		Created: false,
		Regex:   e.Regex,
		ID:      "",
		GroupID: groupId,
		Dir:     e.GetDir(),
	}
}

const manifestPath = "./manifest.toml"

func getManifest() (*Manifest, error) {
	output := new(Manifest)

	meta, err := toml.DecodeFile(manifestPath, output)
	if err != nil {
		return output, err
	}
	if len(meta.Undecoded()) > 0 {
		return output, errors.Errorf("Detected undecoded keys in manifest.toml. This likely indicates a typo or misspelling in a key.\nUndecoded keys: %v", meta.Undecoded())
	}

	return output, output.validate()
}
