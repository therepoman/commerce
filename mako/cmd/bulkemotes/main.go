package main

import (
	"errors"
	"flag"
	"fmt"
	"io"
	"net/http"
	"os"
	"path"
	"time"

	"code.justin.tv/commerce/mako/emote_utils"
	mako "code.justin.tv/commerce/mako/twirp"
	mkd "code.justin.tv/commerce/mako/twirp/dashboard"
	"code.justin.tv/web/upload-service/rpc/uploader"
)

var targetStaging = flag.Bool("staging", false, "Run bulk emote upload against staging Valhalla and Mako.")
var targetProd = flag.Bool("prod", false, "Run bulk emote upload against production Valhalla and Mako.")
var liveRun = flag.Bool("live", false, "Execute changes instead of performing default dry-run")

const logfilePathFormat = "bulkemotes_%d.log"

func main() {
	flag.Parse()

	checkFatalErr(os.Stderr, validateFlags())

	var logWriter io.Writer = os.Stdout
	if *liveRun {
		logfileWriter, err := createLogfileWriter()
		checkFatalErr(os.Stderr, err)

		logWriter = io.MultiWriter(os.Stdout, logfileWriter)
	}

	makoHost := emote_utils.MakoStaging
	uploaderHost := emote_utils.UploaderStaging
	if *targetProd {
		makoHost = emote_utils.MakoProd
		uploaderHost = emote_utils.UploaderProd
	}

	httpClient := &http.Client{
		Timeout: 5 * time.Second,
	}

	executor := new(BulkUploadExecutor)
	executor.LogWriter = logWriter
	executor.Mako = mako.NewMakoProtobufClient(makoHost, httpClient)
	executor.MakoDashboard = mkd.NewMakoProtobufClient(makoHost, httpClient)
	executor.Uploader = uploader.NewUploaderProtobufClient(uploaderHost, httpClient)
	executor.HttpClient = httpClient

	wd, _ := os.Getwd()
	_, _ = fmt.Fprintf(logWriter, "Reading upload manifest from %s...\n", path.Join(wd, manifestPath))

	manifest, err := getManifest()
	checkFatalErr(logWriter, err)

	if *liveRun {
		err := executor.LiveRun(manifest)
		if err != nil {
			_, _ = fmt.Fprintf(executor.LogWriter, "Encountered fatal error: %v\n", err)
			os.Exit(1)
		}
		_, _ = fmt.Fprintf(executor.LogWriter, "Finished.\n")
		os.Exit(0)
	} else {
		executor.DryRun(manifest)
		os.Exit(0)
	}
}

func checkFatalErr(w io.Writer, err error) {
	if err != nil {
		_, _ = fmt.Fprintln(w, err)
		os.Exit(1)
	}
}

func validateFlags() error {
	if *targetStaging == *targetProd {
		return errors.New("exactly one of -staging or -prod must be specified")
	}
	return nil
}

func createLogfileWriter() (io.Writer, error) {
	logfilePath := fmt.Sprintf(logfilePathFormat, time.Now().Unix())

	f, err := os.Create(logfilePath)
	if err != nil {
		return nil, err
	}

	return f, nil
}
