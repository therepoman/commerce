package main

import (
	"context"
	"fmt"
	"io"
	"net/http"

	mako "code.justin.tv/commerce/mako/twirp"
	mkd "code.justin.tv/commerce/mako/twirp/dashboard"
	"code.justin.tv/web/upload-service/rpc/uploader"
)

type BulkUploadExecutor struct {
	LogWriter     io.Writer
	Mako          mako.Mako
	MakoDashboard mkd.Mako
	Uploader      uploader.Uploader
	HttpClient    *http.Client
}

// EmoteGroupState stores the state of the emote group and it's emotes.
type EmoteGroupState struct {
	// OwnerID is the TUID of the user to whom the emote group belongs.
	// This should usually be `139075904 (qa_tw_partner)` for the purposes of this program.
	OwnerID string

	// ID is the ID of the emoticon set, also called "group_id" or "set_id".
	ID string

	// Emote type (origin)
	Type mkd.EmoteOrigin

	// Emotes is a slice of EmoteState representing the state of emotes defined under this group.
	Emotes []EmoteState
}

func (egs EmoteGroupState) String() string {
	return fmt.Sprintf("EmoteGroup %s, owner:%s, emotes:%+v", egs.ID, egs.OwnerID, egs.Emotes)
}

type EmoteState struct {
	Created bool
	Regex   string
	ID      string
	GroupID string
	Dir     string
}

func (es EmoteState) String() string {
	return fmt.Sprintf("Emote \"%s\", id:%s, created?:%t", es.Regex, es.ID, es.Created)
}

func (b *BulkUploadExecutor) DryRun(manifest *Manifest) {
	for _, eg := range manifest.EmoteGroups {
		_, _ = fmt.Fprintf(b.LogWriter, "- Would create emote group: %s, ownerID: %s, and type: %s\n", eg.Name, eg.OwnerID, eg.Type)
		for _, e := range eg.Emotes {
			_, _ = fmt.Fprintf(b.LogWriter, "	- Would create emote: %s\n", e.Regex)
		}
	}
}

func (b *BulkUploadExecutor) LiveRun(manifest *Manifest) error {
	var createdEmoteGroups []EmoteGroupState
	defer func() {
		_, _ = fmt.Fprintf(b.LogWriter, "\n------------Final state:------------\n%s\n------------------------------------\n", createdEmoteGroups)
	}()

	// create emote groups
	for _, emoteGroup := range manifest.EmoteGroups {
		if len(emoteGroup.ID) == 0 {
			resp, err := b.Mako.CreateEmoteGroup(context.Background(), &mako.CreateEmoteGroupRequest{
				Origin:    emoteGroup.getType(),
				GroupType: mako.EmoteGroupType_static_group,
				OwnerId:   emoteGroup.OwnerID,
			})
			if err != nil {
				_, _ = fmt.Fprintf(b.LogWriter, "Encountered error while creating emote group for owner %s\n", emoteGroup.OwnerID)
				return err
			}
			createdEmoteGroups = append(createdEmoteGroups, emoteGroup.toEmoteGroupState(resp.GetGroupId()))
		} else {
			createdEmoteGroups = append(createdEmoteGroups, emoteGroup.toEmoteGroupState(emoteGroup.ID))
		}
	}

	// create emotes in the groups
	for _, emoteGroup := range createdEmoteGroups {
		for _, emote := range emoteGroup.Emotes {
			err := b.CreateEmote(emoteGroup, emote)
			if err != nil {
				_, _ = fmt.Fprintf(b.LogWriter, "Encountered error while creating emote. group=\"%s\" regex=\"%s\"\n", emoteGroup.ID, emote.Regex)
				return err
			}
		}
	}

	return nil
}
