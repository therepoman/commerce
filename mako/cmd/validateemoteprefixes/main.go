package main

import (
	"context"
	"encoding/csv"
	"flag"
	"fmt"
	"os"
	"sync"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/mako/config"
	"code.justin.tv/commerce/mako/dynamo"
	mako "code.justin.tv/commerce/mako/internal"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

var (
	env        = flag.String("env", "staging", "Environment to run the script against (staging, prod). Defaults to staging")
	startIndex = flag.Int("start", 0, "The index to start for prefixes. You can use this to skip forward past prefixes that have already been checked.")
)

type badEmote struct {
	emote  mako.Emote
	prefix string
}

type emotePrefixValidator struct {
	dynamoClient    *dynamodb.DynamoDB
	emotesDAO       *dynamo.EmoteDB
	prefixTableName string
	prefixesTable   dynamo.PrefixesTable

	prefixes []dynamo.Prefix

	mismatched    sync.Map
	mismatchCount int
}

func main() {
	flag.Parse()

	cfg, err := config.LoadConfigForEnvironment(*env)
	if err != nil {
		log.Fatal(err)
	}

	log.Printf("Running script in the %s environment\n", cfg.EnvironmentPrefix)
	sess, err := session.NewSession(&aws.Config{
		Region: aws.String(cfg.AWSRegion),
	})
	if err != nil {
		log.Fatal(err)
	}

	dynamoClient := dynamodb.New(sess)

	var prefixTableName string

	switch *env {
	case "prod", "production":
		prefixTableName = "prod_prefixes"
	default:
		prefixTableName = "staging_prefixes"
	}

	fmt.Println("using emote table: ", cfg.EmoteDBDynamoTable)

	emotesDAO := dynamo.NewEmoteDB(dynamo.EmoteDBConfig{
		Table:  cfg.EmoteDBDynamoTable,
		Region: cfg.AWSRegion,
	})

	validator := emotePrefixValidator{
		dynamoClient:    dynamoClient,
		emotesDAO:       emotesDAO,
		prefixTableName: prefixTableName,
		prefixesTable:   dynamo.PrefixesTable{},

		prefixes:      make([]dynamo.Prefix, 0),
		mismatched:    sync.Map{},
		mismatchCount: 0,
	}

	fmt.Println("Scanning prefixes...")

	validator.scanPrefixes()

	fmt.Printf("Done scanning prefixes. Count: %d\n", len(validator.prefixes))

	fmt.Println("Checking emotes...")

	validator.checkEmotes()
}

func (v *emotePrefixValidator) checkEmotes() {
	var waitGroup sync.WaitGroup
	for i, p := range v.prefixes[*startIndex:] {
		if (i+(*startIndex))%10000 == 0 {
			fmt.Printf("%d prefixes checked...\n", i+(*startIndex))
		}
		waitGroup.Add(1)
		go func(p dynamo.Prefix) {
			defer waitGroup.Done()
			emotes, err := v.emotesDAO.Query(context.Background(), mako.EmoteQuery{
				OwnerID: p.OwnerId,
			})

			if err != nil {
				log.Fatal(err)
			}

			mismatched := make([]badEmote, 0)

			for _, emote := range emotes {
				if emote.Prefix != p.Prefix {
					//mismatch!!!
					mismatched = append(mismatched, badEmote{
						emote:  emote,
						prefix: p.Prefix,
					})
				}
			}

			if len(mismatched) > 0 {
				v.mismatchCount++
				v.mismatched.Store(p.OwnerId, mismatched)

				fmt.Printf("Mismatch for user %s with prefix: %s \n", p.OwnerId, p.Prefix)
				for _, e := range mismatched {
					fmt.Printf("\t emote - id: %s prefix: %s\n", e.emote.ID, e.emote.Prefix)
				}
			}
		}(p)
		time.Sleep(time.Millisecond * 5)
	}
	waitGroup.Wait()
	fmt.Printf("Done checking emotes. Found %d users with mismatched emotes", v.mismatchCount)
	v.writeOutput()
}

func (v *emotePrefixValidator) writeOutput() {
	file, err := os.Create(fmt.Sprintf("mismatched_emotes_%s.csv", time.Now().Format(time.RFC3339)))
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Writing mismatched emotes to file...")
	writer := csv.NewWriter(file)

	err = writer.Write([]string{"channel_id", "channel_prefix", "emote_id", "emote_prefix"})
	if err != nil {
		log.Fatal(err)
	}

	v.mismatched.Range(func(key, val interface{}) bool {
		for _, em := range val.([]badEmote) {
			err := writer.Write([]string{em.emote.OwnerID, em.prefix, em.emote.ID, em.emote.Prefix})
			if err != nil {
				fmt.Println("err writing output: ", err)
				return false
			}
		}
		return true
	})
}

func (v *emotePrefixValidator) scanNext(lastEvaluatedKey map[string]*dynamodb.AttributeValue) map[string]*dynamodb.AttributeValue {
	input := &dynamodb.ScanInput{
		TableName:            aws.String(v.prefixTableName),
		ProjectionExpression: aws.String(fmt.Sprintf("%s,%s", dynamo.PrefixesPrefixColumn, dynamo.PrefixesOwnerIdColumn)),
	}

	if lastEvaluatedKey != nil {
		input.ExclusiveStartKey = lastEvaluatedKey
	}

	result, err := v.dynamoClient.Scan(input)

	if err != nil {
		log.Fatal(err)
	}

	for _, item := range result.Items {
		prefix, err := v.prefixesTable.ConvertAttributeMapToRecord(item)
		if err != nil {
			log.Fatal(err)
		}
		v.prefixes = append(v.prefixes, *prefix.(*dynamo.Prefix))
	}

	return result.LastEvaluatedKey
}

func (v *emotePrefixValidator) scanPrefixes() {
	startKey := v.scanNext(nil)

	for startKey != nil {
		startKey = v.scanNext(startKey)
	}
}
