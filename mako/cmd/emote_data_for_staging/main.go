package main

import (
	"context"
	"flag"
	"fmt"
	"sync"
	"sync/atomic"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/mako/config"
	"code.justin.tv/commerce/mako/dynamo"
	"code.justin.tv/commerce/mako/internal"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
)

const (
	concurrentWaitGroups   = 100
	prodEmotesTableName    = "prod_emotes"
	stagingEmotesTableName = "staging_emotes"
)

var (
	env             = flag.String("env", "staging", "Environment to run the script against (staging, prod). Defaults to staging.")
	dryrun          = flag.Bool("dryrun", true, "Set this to false to perform the write operations of this script. Defaults to true.")
	accessKey       = flag.String("accessKey", "", "AWS Access Key for Mako. Should match the environment provided by the 'env' flag. Make sure the underlying role for the credentials has read access to DynamoDB.")
	secretAccessKey = flag.String("secretAccessKey", "", "AWS Secret Access Key for Mako. Should match the environment provided by the 'env' flag. Make sure the underlying role for the credentials has read access to DynamoDB.")
	sessionToken    = flag.String("sessionToken", "", "AWS Session Token for Mako. Should match the environment provided by the 'env' flag. Make sure the underlying role for the credentials has read access to DynamoDB.")
)

type emotesStagingBackfiller struct {
	dynamoClientScanner *dynamodb.DynamoDB
	emoteDBWriter       *dynamo.EmoteDB
	emotesTableRead     string
	emotes              []emote
}

type emote struct {
	ID          string `dynamodbav:"id"`
	OwnerID     string `dynamodbav:"ownerId"`
	GroupID     string `dynamodbav:"groupId"`
	Prefix      string `dynamodbav:"prefix"`
	Suffix      string `dynamodbav:"suffix"`
	Code        string `dynamodbav:"code"`
	State       string `dynamodbav:"state"`
	Domain      string `dynamodbav:"domain"`
	CreatedAt   int64  `dynamodbav:"createdAt"`
	UpdatedAt   int64  `dynamodbav:"updatedAt"`
	Order       int64  `dynamodbav:"order"`
	EmotesGroup string `dynamodbav:"emotesGroup"`
	ProductID   string `dynamodbav:"productId"`
}

func (e emote) toMako() mako.Emote {
	return mako.Emote{
		ID:          e.ID,
		OwnerID:     e.OwnerID,
		GroupID:     e.GroupID,
		ProductID:   e.ProductID,
		EmotesGroup: e.EmotesGroup,
		Prefix:      e.Prefix,
		Suffix:      e.Suffix,
		Code:        e.Code,
		State:       mako.EmoteState(e.State),
		Domain:      mako.Domain(e.Domain),
		CreatedAt:   time.Unix(0, e.CreatedAt),
		UpdatedAt:   time.Unix(0, e.UpdatedAt),
		Order:       e.Order,
	}
}

// Need both prod and staging creds to run this.
// Hard-code the read-only prod creds below and fetch the staging creds from the local aws profile as normal.
type credentialsProvider struct{}

func (p *credentialsProvider) Retrieve() (credentials.Value, error) {
	return credentials.Value{
		AccessKeyID:     *accessKey,
		SecretAccessKey: *secretAccessKey,
		SessionToken:    *sessionToken,
		ProviderName:    "prod_dynamo_readonly_credentials_provider",
	}, nil
}

func (p *credentialsProvider) IsExpired() bool {
	return false
}

func main() {
	flag.Parse()

	cfg, err := config.LoadConfigForEnvironment(*env)
	if err != nil {
		log.Fatal(err)
	}

	if cfg.EnvironmentPrefix == "prod" && !*dryrun {
		fmt.Println("!!!!Running for real in prod. Giving you 10 seconds to rethink this decision!!!!")
		time.Sleep(time.Second * 10)
	}

	log.Printf("Running script in the %s environment\n", cfg.EnvironmentPrefix)

	sess, err := session.NewSession(&aws.Config{
		Region:      aws.String(cfg.AWSRegion),
		Credentials: credentials.NewCredentials(&credentialsProvider{}),
	})
	if err != nil {
		log.Fatal(err)
	}

	dynamoClient := dynamodb.New(sess)

	var emotesTableNameRead string

	switch *env {
	case "prod":
		emotesTableNameRead = prodEmotesTableName
	default:
		emotesTableNameRead = stagingEmotesTableName
	}

	emoteDBWriter := dynamo.NewEmoteDB(dynamo.EmoteDBConfig{
		Table:  stagingEmotesTableName,
		Region: cfg.AWSRegion,
	})

	backfiller := emotesStagingBackfiller{
		dynamoClientScanner: dynamoClient,
		emoteDBWriter:       emoteDBWriter,
		emotesTableRead:     emotesTableNameRead,
		emotes:              make([]emote, 0),
	}

	fmt.Printf("Scanning emotes from table %s...\n", backfiller.emotesTableRead)

	backfiller.scanEmotes()

	fmt.Printf("Done scanning emotes. Count: %d\n", len(backfiller.emotes))

	fmt.Printf("Writing emotes to table %s...\n", stagingEmotesTableName)

	backfiller.writeEmotesToStagingDynamo()
}

func (b *emotesStagingBackfiller) scanNext(lastEvaluatedKey map[string]*dynamodb.AttributeValue) map[string]*dynamodb.AttributeValue {
	input := &dynamodb.ScanInput{
		TableName: aws.String(b.emotesTableRead),
	}

	if lastEvaluatedKey != nil {
		input.ExclusiveStartKey = lastEvaluatedKey
	}

	result, err := b.dynamoClientScanner.Scan(input)

	if err != nil {
		log.Fatal(err)
	}

	for _, item := range result.Items {
		var emote emote
		if err := dynamodbattribute.UnmarshalMap(item, &emote); err != nil {
			log.Fatal(err)
		}

		b.emotes = append(b.emotes, emote)
	}

	return result.LastEvaluatedKey
}

func (b *emotesStagingBackfiller) scanEmotes() {
	startKey := b.scanNext(nil)

	for startKey != nil {
		startKey = b.scanNext(startKey)
	}
}

func (b *emotesStagingBackfiller) writeEmotesToStagingDynamo() {
	var waitGroup sync.WaitGroup
	var emoteRecords = make(chan mako.Emote, len(b.emotes))
	var count int32

	waitGroup.Add(concurrentWaitGroups)
	for i := 0; i < concurrentWaitGroups; i++ {
		go func() {
			defer waitGroup.Done()

			if !*dryrun {
				for {
					select {
					case emoteRecord, ok := <-emoteRecords:
						if !ok {
							return
						}

						logfields := log.Fields{
							"emote_id":       emoteRecord.ID,
							"emote_code":     emoteRecord.Code,
							"emote_owner_id": emoteRecord.OwnerID,
						}

						ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)

						err := b.emoteDBWriter.WriteEmotes(ctx, emoteRecord)
						if err != nil {
							log.WithFields(logfields).Error(err)
							cancel()
							continue
						}
						atomic.AddInt32(&count, 1)

						if count%1000 == 0 {
							fmt.Printf("progress: %d emotes written\n", count)
						}

						cancel()
					}
				}
			}
		}()
	}

	for _, emoteRecord := range b.emotes {
		emoteRecords <- emoteRecord.toMako()
	}

	close(emoteRecords)

	waitGroup.Wait()
}
