package main

import (
	"context"
	"encoding/csv"
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"time"

	mako_client "code.justin.tv/commerce/mako/twirp"
	"code.justin.tv/safety/inference/rpc/names"
)

type emoteResult struct {
	code       string
	channelID  string
	id         string
	tier       string
	confidence string
}

const (
	makoStagingURL = "https://main.us-west-2.beta.mako.twitch.a2z.com"
	makoProdURL    = "https://main.us-west-2.prod.mako.twitch.a2z.com"
	// emoteImageURLPrefix = "https://static-cdn.jtvnw.net/emoticons/v1"
	namesStagingURL = "https://inference.service.safety-staging.twitch.a2z.com"
	namesProdURL    = "https://inference.service.safety.twitch.a2z.com"
)

var (
	inputFile = flag.String("file", "", "Path to the csv file that contains the list of channels that you want to check the emote codes for")
	env       = flag.String("env", "staging", "Environment to run the script against (staging, prod). Defaults to staging")
)

func main() {
	flag.Parse()

	// Initialize Mako client
	makoHost := makoStagingURL
	namesHost := namesStagingURL
	if *env == "prod" {
		makoHost = makoProdURL
		namesHost = namesProdURL
	}
	makoClient := mako_client.NewMakoProtobufClient(makoHost, &http.Client{})
	namesClient := names.NewNamesProtobufClient(namesHost, &http.Client{})
	// read channelIds from file
	channelIds, err := readChannelIds(*inputFile)

	if err != nil {
		log.Fatal("Could not read channelIds from input file: ", err)
	}

	results := make(map[string][]emoteResult, 0)
	likelyBad := make([]emoteResult, 0)
	veryLikelyBad := make([]emoteResult, 0)

	for _, channelID := range channelIds {
		// get emotes by channel Id
		fmt.Println("Checking emote codes for channel ", channelID)
		resp, err := makoClient.GetAllEmotesByOwnerId(context.Background(), &mako_client.GetAllEmotesByOwnerIdRequest{
			OwnerId: channelID,
			Filter: &mako_client.EmoteFilter{
				Domain: mako_client.Domain_emotes,
				States: []mako_client.EmoteState{mako_client.EmoteState_active, mako_client.EmoteState_pending},
			},
		})

		if err != nil {
			fmt.Printf("Error getting emotes for id: %s. %v\n", channelID, err)
			continue
		}

		emotes := resp.Emoticons

		emoteCodes := make([]string, 0, len(emotes))
		for _, emote := range emotes {
			emoteCodes = append(emoteCodes, emote.GetCode())
		}
		// Call inference service with emote codes
		namesResp, err := namesClient.AcceptableNames(context.Background(), &names.AcceptableNamesRequest{
			Usernames: emoteCodes,
		})

		if err != nil {
			fmt.Printf("Error calling AcceptableNames for codes: %v\nerr:%v\n", emoteCodes, err)
			continue
		}

		emoteResults := make([]emoteResult, 0, len(emotes))

		for i, emote := range emotes {
			r := emoteResult{
				id:         emote.Id,
				code:       emote.Code,
				confidence: fmt.Sprintf("%f", namesResp.Confidence[i]),
				tier:       namesResp.Tier[i].String(),
				channelID:  channelID,
			}
			emoteResults = append(emoteResults, r)
			if namesResp.Tier[i] == names.OffensiveTier_LIKELY {
				likelyBad = append(likelyBad, r)
			} else if namesResp.Tier[i] == names.OffensiveTier_VERY_LIKELY {
				veryLikelyBad = append(veryLikelyBad, r)
			}
		}
		results[channelID] = emoteResults
	}

	writeResult(results)

	fmt.Println("Done")
	fmt.Println("Likely Bad: ", len(likelyBad))
	for _, e := range likelyBad {
		fmt.Println("\t", e)
	}
	fmt.Println("Very Likely Bad: ", len(veryLikelyBad))
	for _, e := range veryLikelyBad {
		fmt.Println("\t", e)
	}
}

func writeResult(results map[string][]emoteResult) {
	cwd, err := os.Getwd()

	if err != nil {
		fmt.Printf("Error getting cwd.\nError:%v\n", err)
	}

	path := filepath.Join(cwd, fmt.Sprintf("results-%s.csv", time.Now().Format(time.RFC3339)))

	file, err := os.Create(path)

	if err != nil {
		log.Fatal(err)
	}

	writer := csv.NewWriter(file)

	fmt.Printf("Writing to %s\n", path)
	defer file.Close()

	writer.Write([]string{"channel_id", "emote_id", "code", "tier", "confidence"})

	for _, emoteResults := range results {
		for _, result := range emoteResults {
			if result != (emoteResult{}) {
				// fmt.Println(result)
				err = writer.Write([]string{result.channelID, result.id, result.code, result.tier, result.confidence})
				if err != nil {
					fmt.Printf("Error writing output to %s.\nError:%v\n", path, err)
					return
				}
			}
		}
	}

	writer.Flush()
}

func readChannelIds(filepath string) ([]string, error) {
	file, err := os.Open(filepath)
	if err != nil {
		return []string{}, err
	}
	defer file.Close()

	reader := csv.NewReader(file)

	channelIds := make([]string, 0)

	for {
		line, err := reader.Read()
		if err == io.EOF {
			break
		}

		if strings.Contains(line[0], "channel_id") {
			// header
			continue
		}

		channelIds = append(channelIds, line[0])
	}

	return channelIds, nil
}
