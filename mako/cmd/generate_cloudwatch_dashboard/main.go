package main

import (
	"flag"
	"io/ioutil"
	"regexp"
	"strconv"
	"strings"

	log "code.justin.tv/commerce/logrus"
)

const (
	VERTICAL_OFFSET         = "<VERTICAL-OFFSET>"
	SERVICE                 = "<SERVICE>"
	TWIRP_API               = "<TWIRP-API>"
	STAGE                   = "<STAGE>"
	REGION                  = "<REGION>"
	HIGH_PERCENTILE_VISIBLE = "<HIGH-PERCENTILE-VISIBLE>"
	REGEX                   = "rpc\\s+([a-zA-Z0-9]+)\\s*\\([a-zA-Z0-9]+\\)\\s*returns\\s*\\([a-zA-Z0-9]+\\)"
	METHODS_SEPARATOR       = ","
)

var highPercentileLatencyMethods = make(map[string]bool)
var omittedMethods = make(map[string]bool)

var (
	inputFile                         = flag.String("input", "twirp/mako-service.proto", "Path to twirp file")
	outputFile                        = flag.String("output", "cloudwatch-dashboard.json", "Path to file to output dashboard JSON to")
	service                           = flag.String("service", "mako", "Service the dashboard is for")
	stage                             = flag.String("stage", "prod-ecs", "Environment for the dashboard")
	region                            = flag.String("region", "us-west-2", "AWS Region")
	highPercentileLatencyMethodsParam = flag.String("highPercentileLatencyMethods", "", "Methods that we should include the P99.99 latency for.")
	omittedMethodsParam               = flag.String("omittedMethods", "", "Methods that we would like to omit from the dashboard.")
)

func main() {
	flag.Parse()

	if highPercentileLatencyMethodsParam != nil {
		for _, highPercentileLatencyMethod := range strings.Split(*highPercentileLatencyMethodsParam, METHODS_SEPARATOR) {
			highPercentileLatencyMethods[highPercentileLatencyMethod] = true
		}
	}

	if omittedMethodsParam != nil {
		for _, omittedMethod := range strings.Split(*omittedMethodsParam, METHODS_SEPARATOR) {
			omittedMethods[omittedMethod] = true
		}
	}

	template := getTemplate()

	twirpMethods := getTwirpMethods()

	dashboard := ""
	for i, method := range twirpMethods {
		if ok, _ := omittedMethods[method]; !ok {
			isHighTPSMethod := highPercentileLatencyMethods[method]
			dashboard = dashboard + doTemplateReplacement(template, i*6, method, *service, *stage, *region, isHighTPSMethod)
			if len(twirpMethods) != i+1 {
				dashboard = dashboard + ","
			}
		}
	}
	writeToFile("{ \"widgets\": [" + dashboard + "]}")
}

func getTemplate() string {
	template, err := ioutil.ReadFile("cmd/generate_cloudwatch_dashboard/cloudwatch-template.json")
	if err != nil {
		log.Fatal(err)
	}
	return string(template)
}

func doTemplateReplacement(template string, verticalOffset int, twirpAPI, service, stage, region string, highPercentileVisible bool) string {
	updatedTemplate := strings.ReplaceAll(template, VERTICAL_OFFSET, strconv.Itoa(verticalOffset))
	updatedTemplate = strings.ReplaceAll(updatedTemplate, SERVICE, service)
	updatedTemplate = strings.ReplaceAll(updatedTemplate, TWIRP_API, twirpAPI)
	updatedTemplate = strings.ReplaceAll(updatedTemplate, STAGE, stage)
	updatedTemplate = strings.ReplaceAll(updatedTemplate, REGION, region)
	updatedTemplate = strings.ReplaceAll(updatedTemplate, HIGH_PERCENTILE_VISIBLE, strconv.FormatBool(highPercentileVisible))
	return updatedTemplate

}

func writeToFile(body string) {
	err := ioutil.WriteFile(*outputFile, []byte(body), 0666)
	if err != nil {
		log.Fatal(err)
	}
}

func getTwirpMethods() []string {
	regex, err := regexp.Compile(REGEX)
	if err != nil {
		log.Fatal(err)
	}

	twirpProto, err := ioutil.ReadFile(*inputFile)
	if err != nil {
		log.Fatal(err)
	}

	methods := make([]string, 0)
	matches := regex.FindAllSubmatch([]byte(twirpProto), -1)
	for _, match := range matches {
		methods = append(methods, string(match[1]))
	}
	return methods
}
