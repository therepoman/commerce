Dashboard Generation Script
---------------------------

Easiest way to use this script is to run `make update-dashboards` which will update both the production and production canary dashboards.

NOTE: You will need to have your `mako-prod` AWS credentials set up in order to run this command.

This script works by running a regex on the twirp proto file to grab all methods and then apply the template to each of them. The template is meant to represent a single row in the dashboard. It's not a terribly complicated script, it works primarily by doing a large number of string replacement operations.

The script itself has the following params and defaults:

* `input` (default: `twirp/mako-service.proto`)
* `output` (default: `cloudwatch-dashboard.json`)
* `service` (default: `mako`)
* `stage` (default: `prod-ecs`)
* `region` (default: `us-west-2`)

TODO:
* Add ability to override ordering w/ support for text headings (ordering now is based on Twirp proto file)
* Logscan and other non-twirp derived metrics
* Integrate into Jenkins so dashboard is updated on prod/canary deploy.
