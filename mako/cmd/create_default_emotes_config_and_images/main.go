package main

import (
	"encoding/json"
	"flag"
	"fmt"
	uuid "github.com/satori/go.uuid"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"
)

var inputFilename = flag.String("input", "", "Directory contain the default emotes info")

type imagesFile struct {
	Images []emoteImage `json:"default-emote-images"`
}

type emoteImage struct {
	ID        string   `json:"id"`
	Name      string   `json:"name"`
	AssetType string   `json:"asset_type"`
	Tags      []string `json:"tags"`
}

/*
This script will take in a folder that contains sub-folders of images to be used as default emote images. The sub-folder
name will a "_" seperated name of the emote (ie "CAT_LUL_GREEN"), which will be parsed to create the tags of the image
and the image name. Each folder will be treated as containing the three emotes sizes for that image and those files
will be renamed for the correct format for the default emote it corresponds to (ie "11940d6a-12c2-4f47-b3f4-465a34753ff3-1.0.png").
The files and config will be placed in the containing folder for manual upload/editing.
 */
func main() {
	flag.Parse()

	if *inputFilename == "" {
		log.Fatalln("Need input directory")
	}

	var images []emoteImage
	folderToID := make(map[string]string, 0)
	err := filepath.Walk(*inputFilename, func(path string, info os.FileInfo, err error) error {
		if info.IsDir() && !strings.EqualFold(path, *inputFilename) {
			id := uuid.Must(uuid.NewV4())
			tags := strings.Split(info.Name(), "_")
			name := getName(info.Name())

			emoteImage := emoteImage{
				Tags:      tags,
				ID:        id.String(),
				AssetType: "static",
				Name:      name,
			}

			images = append(images, emoteImage)
			folderToID[path] = id.String()
			return nil
		}

		return nil
	})
	if err != nil {
		log.Fatalf("Erroring generating config %+v", err)
	}

	log.Printf("map path:+%v", folderToID)

	err = filepath.Walk(*inputFilename, func(path string, info os.FileInfo, err error) error {
		if !info.IsDir() {
			dir := filepath.Dir(path)
			id := folderToID[dir]
			if strings.Contains(info.Name(), "28") {
				newPath := filepath.Join(dir, fmt.Sprintf("%s-1.0.png", id))
				return os.Rename(path, newPath)
			} else if strings.Contains(info.Name(), "56") {
				newPath := filepath.Join(dir, fmt.Sprintf("%s-2.0.png", id))
				return os.Rename(path, newPath)
			} else if strings.Contains(info.Name(), "112") {
				newPath := filepath.Join(dir, fmt.Sprintf("%s-3.0.png", id))
				return os.Rename(path, newPath)
			}
		}

		return nil
	})

	if err != nil {
		log.Fatalf("Erroring renaming files %+v", err)
	}

	config := imagesFile{
		images,
	}

	file, _ := json.MarshalIndent(config, "", " ")
	_ = ioutil.WriteFile(*inputFilename+"default_emote_images_data.json", file, 0644)

	log.Printf(" file at %s", *inputFilename+"test.json")
	log.Println("Done")
}

func getName(dirName string) string {
	if strings.Contains(dirName, "Purple") {
		return strings.Replace(dirName, "Purple", "1", 1)
	}

	if strings.Contains(dirName, "Blue") {
		return strings.Replace(dirName, "Blue", "2", 1)
	}

	if strings.Contains(dirName, "Green") {
		return strings.Replace(dirName, "Green", "3", 1)
	}

	if strings.Contains(dirName, "Yellow") {
		return strings.Replace(dirName, "Yellow", "4", 1)
	}

	if strings.Contains(dirName, "Orange") {
		return strings.Replace(dirName, "Orange", "5", 1)
	}

	if strings.Contains(dirName, "Red") {
		return strings.Replace(dirName, "Red", "6", 1)
	}

	return dirName
}
