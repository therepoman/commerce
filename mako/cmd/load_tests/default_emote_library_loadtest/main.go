package main

import (
	"code.justin.tv/commerce/mako/cmd/load_tests/default_emote_library_loadtest/data"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"math/rand"
	"net/http"
	"os"
	"path"
	"time"

	"code.justin.tv/commerce/gogogadget/random"
	log "code.justin.tv/commerce/logrus"
	mako "code.justin.tv/commerce/mako/twirp"
	mako_dashboard "code.justin.tv/commerce/mako/twirp/dashboard"
	vegeta "github.com/tsenart/vegeta/v12/lib"
	"golang.org/x/sync/errgroup"
)

const (
	makoEndpoint      = "https://main.us-west-2.prod.mako.twitch.a2z.com"
	outDir            = "results"
	testDuration      = time.Minute * 30
	createEmoteRPS    = 10 //requests per second
	createFollowerEmoteRPS = 5 // requests per second
)

type loadTestUserData struct {
	followerGroupID        string
	userID         string
	prefix         string
	tier1productID string
	tier1GroupID   string
}

var testUsers = []loadTestUserData{
	{
		userID:         "655448192", // qa_naeunice_partner test for default emote library load test
		prefix:         "qa1234",
		tier1productID: "304787369",
		tier1GroupID:   "304787092",
		followerGroupID: "e2030bcb-bfa6-4561-abee-6ea4f0fb65e8",
	},
	//{
	//	followerGroupID:  "ab37ac2b-669f-4d23-9e6f-495cbaaff298", // GroupID for qa_partner_badmintonj follower emotes
	//	userID:   "672841770",                            // qa_partner_badmintonj
	//	prefix:   "badmintonb",
	//},
	//{
	//	followerGroupID:  "a299246a-337f-454a-9cae-b5e266b8c56e", // GroupID for qa_partner_magnus follower emotes
	//	userID:   "672851769",                            // qa_partner_magnus
	//	prefix:   "mgpqa",
	//},
}

// stuff we only want to init once
var (
	makoClient          mako.Mako
	makoDashboardClient mako_dashboard.Mako
)

func init() {
	rand.Seed(time.Now().UTC().UnixNano())

	makoClient = mako.NewMakoProtobufClient(makoEndpoint, http.DefaultClient)
	makoDashboardClient = mako_dashboard.NewMakoProtobufClient(makoEndpoint, http.DefaultClient)
}

func main() {
	// Uncomment the test you would like to run
	//loadTestCreateEmoteWithDefaultEmoteImage()
	loadTestCreateFollowerEmoteWithDefaultEmoteImage()
}

func loadTestCreateEmoteWithDefaultEmoteImage() {
	log.Info("Cleaning up emotes before test")
	err := deleteAllEmotesFromAllGroups()
	if err != nil {
		log.WithError(err).Fatal("[CreateEmote] Failed to delete all emotes from all groups before test")
	}

	// We need to aggressively clear out the emote data while this test is running.
	// Otherwise, the account's emote libraries will fill up too quickly.
	testing := true
	eg := errgroup.Group{}
	eg.Go(func() error {
		for testing {
			time.Sleep(time.Millisecond * 100)
			err := deleteAllEmotesFromAllGroups()
			if err != nil {
				log.WithError(err).Error("Failed delete all emotes for all groups")
			}
		}
		return nil
	})

	attack(getCreateEmoteTest(), vegeta.Rate{
		Freq: createEmoteRPS,
		Per:  time.Second,
	}, testDuration, "CreateEmote")

	testing = false
	eg.Wait()

	log.Info("Cleaning up emotes after test")
	err = deleteAllEmotesFromAllGroups()
	if err != nil {
		log.WithError(err).Fatal("[CreateEmote] Failed to delete all emotes from all groups after test")
	}
}

func getCreateEmoteTest() vegeta.Targeter {
	headers := make(http.Header)
	headers.Add("Content-Type", "application/json")
	url := makoEndpoint + "/twirp/code.justin.tv.commerce.mako.dashboard.Mako/CreateEmote"

	return func(t *vegeta.Target) error {
		testUser := getTestUser()

		// Build request
		req := getCreateEmoteRequest(testUser)

		reqBytes, err := json.Marshal(req)
		if err != nil {
			log.WithError(err).Error("Failed to marshal CreateEmote request")
			return err
		}

		t.Method = http.MethodPost
		t.URL = url
		t.Header = headers
		t.Body = reqBytes

		return err
	}
}

func getCreateEmoteRequest(testUser loadTestUserData) *mako_dashboard.CreateEmoteRequest {
	suffix := fmt.Sprintf("LT%d", random.Int(0, 1000000))
	possibleOrigins := []mako_dashboard.EmoteOrigin{
		/**
		made a really rough ratio of create emotes based on the number of CreateEmote requests on 2021-08-18 broken down by origin in Cloudwatch
		around 14% of requests were BitsBadgeTierEmotes (2359 requests)
		around 23% were Archive (3731 requests)
		around 63% were Subscriptions (10176 requests)
		broken down into a rough ratio of 1:2:6
		*/
		mako_dashboard.EmoteOrigin_BitsBadgeTierEmotes,
		mako_dashboard.EmoteOrigin_Archive,
		mako_dashboard.EmoteOrigin_Archive,
		mako_dashboard.EmoteOrigin_Subscriptions,
		mako_dashboard.EmoteOrigin_Subscriptions,
		mako_dashboard.EmoteOrigin_Subscriptions,
		mako_dashboard.EmoteOrigin_Subscriptions,
		mako_dashboard.EmoteOrigin_Subscriptions,
		mako_dashboard.EmoteOrigin_Subscriptions,
	}
	emoteOrigin := possibleOrigins[rand.Intn(9)]

	emoteState := mako_dashboard.EmoteState_active
	emoteGroupIDs := []string{testUser.tier1GroupID}
	if emoteOrigin == mako_dashboard.EmoteOrigin_Archive {
		emoteState = mako_dashboard.EmoteState_archived
		emoteGroupIDs = []string{}
	}

	imageIDLength := len(data.DefaultEmoteImageIDs)
	randomImageID := data.DefaultEmoteImageIDs[rand.Intn(imageIDLength)]

	return &mako_dashboard.CreateEmoteRequest{
		UserId:     testUser.userID,
		Code:       testUser.prefix + suffix,
		CodeSuffix: suffix,
		GroupIds:   emoteGroupIDs,
		ImageAssets: []*mako_dashboard.EmoteImageAsset{
			{
				ImageIds: &mako_dashboard.EmoteImageIds{
					Image1XId: randomImageID + "-1.0",
					Image2XId: randomImageID + "-2.0",
					Image4XId: randomImageID + "-3.0",
				},
				AssetType: mako_dashboard.EmoteAssetType_static,
			},
		},
		AssetType:                 mako_dashboard.EmoteAssetType_static,
		Origin:                    emoteOrigin,
		State:                     emoteState,
		EnforceModerationWorkflow: true,
		ImageSource:               mako_dashboard.ImageSource_DefaultLibraryImage,
	}
}

func getTestUser() loadTestUserData {
	index := random.Int(0, len(testUsers)-1)
	return testUsers[index]
}

func deleteAllEmotesFromAllGroups() error {
	eg := errgroup.Group{}

	for _, testUser := range testUsers {
		tempTestUser := testUser
		eg.Go(func() error {
			err := deleteAllEmotesForUser(tempTestUser)
			return err
		})
	}

	return eg.Wait()
}

func deleteAllEmotesForUser(testUser loadTestUserData) error {
	allEmotes, err := makoClient.GetAllEmotesByOwnerId(context.Background(), &mako.GetAllEmotesByOwnerIdRequest{
		OwnerId: testUser.userID,
		Filter: &mako.EmoteFilter{
			States: []mako.EmoteState{mako.EmoteState_active, mako.EmoteState_archived},
		},
	})
	if err != nil {
		log.WithError(err).Error("Failed to get emotes by group")
		return err
	}

	for _, emote := range allEmotes.GetEmoticons() {
		_, err := makoClient.DeleteEmoticon(context.Background(), &mako.DeleteEmoticonRequest{
			Id:                    emote.Id,
			DeleteImages:          false,
			RequiresPermittedUser: true,
			UserId:                testUser.userID,
		})
		if err != nil {
			log.WithError(err).WithField("emote_id", emote.Id).Error("Failed to delete group emotes")
			return err
		}
	}

	return nil
}

func printMetrics(title string, metrics vegeta.Metrics, errors map[string]int) {
	dir, err := os.Getwd()

	if err != nil {
		panic(err)
	}

	dest := path.Join(dir, outDir, fmt.Sprintf("%d_%s_results.txt", time.Now().Unix(), title))

	writers := make([]io.Writer, 0)
	writers = append(writers, os.Stdout)

	os.MkdirAll(path.Join(dir, outDir), os.ModePerm)
	file, err := os.Create(dest)
	if err != nil {
		log.WithError(err).WithField("dest", dest).Info("Error creating results file")
	} else {
		writers = append(writers, file)
	}

	w := io.MultiWriter(writers...)

	_, _ = fmt.Fprintf(w, "\n====== %s Results ======\n", title)

	_, _ = fmt.Fprintf(w, "Start Time: %s \n", metrics.Earliest.String())
	_, _ = fmt.Fprintf(w, "End Time: %s \n", metrics.Latest.String())
	_, _ = fmt.Fprintf(w, "Duration: %s \n", metrics.Duration.String())

	_, _ = fmt.Fprintf(w, "Requests: %d \n", metrics.Requests)
	_, _ = fmt.Fprintf(w, "Success Percentage: %f \n", metrics.Success)
	_, _ = fmt.Fprintf(w, "Request Rate: %f \n", metrics.Rate)
	_, _ = fmt.Fprintf(w, "Successful Request Throughput: %f \n\n", metrics.Throughput)

	_, _ = fmt.Fprintf(w, "Latency Average: %s \n", metrics.Latencies.Mean)
	_, _ = fmt.Fprintf(w, "Latency p50: %s \n", metrics.Latencies.P50)
	_, _ = fmt.Fprintf(w, "Latency p90: %s \n", metrics.Latencies.P90)
	_, _ = fmt.Fprintf(w, "Latency p99: %s \n", metrics.Latencies.P99)
	_, _ = fmt.Fprintf(w, "Latency Max: %s \n\n", metrics.Latencies.Max)

	_, _ = fmt.Fprintf(w, "Status Codes: %v \n", metrics.StatusCodes)
	_, _ = fmt.Fprintf(w, "Errors: %v\n", metrics.Errors)
	_, _ = fmt.Fprintf(w, "Error Sets: %v\n", errors)
}

func attack(targeter vegeta.Targeter, pacer vegeta.Pacer, dur time.Duration, name string) {
	log.Info(fmt.Sprintf("Starting attack on %s. Initial TPS: %f Final TPS: %f Duration: %s", name, pacer.Rate(time.Second), pacer.Rate(dur), dur.String()))
	attacker := vegeta.NewAttacker()
	var metrics vegeta.Metrics
	errors := map[string]int{}
	for res := range attacker.Attack(targeter, pacer, dur, name+"-Attack") {
		metrics.Add(res)
		if res.Code != 200 {
			body := string(res.Body)
			errors[body] = errors[body] + 1
			log.Error(res.Error)
		}
		// This code works, but is too slow. Since this step is run independently from the attack, it was too slow
		// to clear out emotes before the account limits were reached. Keeping it here in case it is helpful for future
		// lower-TPS tests.
		//else {
		//	// Clear up CreateEmote data
		//	if name == "CreateEmote" {
		//		var response LoadTestCreateEmoteResponse
		//		err := json.Unmarshal(res.Body, &response)
		//		if err != nil {
		//			log.WithError(err).Error("failed to unmarshal CreateEmote response after attack")
		//		}
		//
		//		err = deleteEmote(response.Id, response.Emote.OwnerId)
		//		if err != nil {
		//			log.WithError(err).Error("failed to delete emote after CreateEmote attack")
		//		}
		//	}
		//}
	}
	metrics.Close()
	printMetrics(name, metrics, unpackErrors(errors))
}

func unpackErrors(errors map[string]int) map[string]int {
	type MetricError struct {
		Message string `json:"message"`
	}

	unpackedErrors := map[string]int{}
	for err, count := range errors {
		var metricError MetricError
		json.Unmarshal([]byte(err), &metricError)
		unpackedErrors[metricError.Message] = count
	}

	return unpackedErrors
}

func loadTestCreateFollowerEmoteWithDefaultEmoteImage() {
	log.Info("Cleaning up emotes before test")
	err := deleteAllEmotesFromFollowerGroups()
	if err != nil {
		log.WithError(err).Fatal("[CreateFollowerEmote] Failed to delete all emotes from follower groups before test")
	}

	// We need to aggressively clear out the emote data while this test is running.
	// Otherwise, the account's emote libraries will fill up too quickly.
	testing := true
	eg := errgroup.Group{}
	eg.Go(func() error {
		for testing {
			err := deleteAllEmotesFromFollowerGroups()
			if err != nil {
				log.WithError(err).Error("Failed delete all emotes from follower groups")
			}
		}
		return nil
	})

	attack(getCreateFollowerEmoteTest(), vegeta.Rate{
		Freq: createFollowerEmoteRPS,
		Per:  time.Second,
	}, testDuration, "CreateFollowerEmote")

	testing = false
	eg.Wait()

	log.Info("Cleaning up emotes after test")
	err = deleteAllEmotesFromFollowerGroups()
	if err != nil {
		log.WithError(err).Fatal("[CreateFollowerEmote] Failed to delete all emotes from follower groups after test")
	}
}

func deleteAllEmotesFromFollowerGroups() error {
	eg := errgroup.Group{}

	for _, testUser := range testUsers {
		tempTestUser := testUser
		eg.Go(func() error {
			emotesByGroupsResp, err := makoDashboardClient.GetEmotesByGroups(context.Background(), &mako_dashboard.GetEmotesByGroupsRequest{
				EmoteGroupIds: []string{tempTestUser.followerGroupID},
				Filter: &mako_dashboard.EmoteFilter{
					States: []mako_dashboard.EmoteState{mako_dashboard.EmoteState_active, mako_dashboard.EmoteState_pending},
				},
			})
			if err != nil {
				log.WithError(err).Error("Failed to get emotes by group")
				return err
			}

			for _, group := range emotesByGroupsResp.Groups {
				for _, emote := range group.Emotes {
					_, err := makoClient.DeleteEmoticon(context.Background(), &mako.DeleteEmoticonRequest{
						Id:                    emote.Id,
						DeleteImages:          false,
						RequiresPermittedUser: true,
						UserId:                tempTestUser.userID,
					})
					if err != nil {
						log.WithError(err).WithField("emote_id", emote.Id).Error("Failed to delete group emotes")
						return err
					}
				}
			}

			return nil
		})
	}

	return eg.Wait()

}

func getCreateFollowerEmoteTest() vegeta.Targeter {
	headers := make(http.Header)
	headers.Add("Content-Type", "application/json")
	url := makoEndpoint + "/twirp/code.justin.tv.commerce.mako.dashboard.Mako/CreateFollowerEmote"

	return func(t *vegeta.Target) error {
		testUser := getTestUser()

		// Build request
		req := getCreateFollowerEmoteRequest(testUser)

		reqBytes, err := json.Marshal(req)
		if err != nil {
			log.WithError(err).Error("Failed to marshal CreateFollowerEmote request")
			return err
		}

		t.Method = http.MethodPost
		t.URL = url
		t.Header = headers
		t.Body = reqBytes

		return err
	}
}

func getCreateFollowerEmoteRequest(testUser loadTestUserData) *mako_dashboard.CreateFollowerEmoteRequest {
	suffix := fmt.Sprintf("LT%d", random.Int(0, 1000000))
	imageIDLength := len(data.DefaultEmoteImageIDs)
	randomImageID := data.DefaultEmoteImageIDs[rand.Intn(imageIDLength)]
	return &mako_dashboard.CreateFollowerEmoteRequest{
		UserId:     testUser.userID,
		CodeSuffix: suffix,
		ImageAssets: []*mako_dashboard.EmoteImageAsset{
			{
				ImageIds: &mako_dashboard.EmoteImageIds{
					Image1XId: randomImageID+"-1.0",
					Image2XId: randomImageID+"-2.0",
					Image4XId: randomImageID+"-3.0",
				},
				AssetType: mako_dashboard.EmoteAssetType_static,
			},
		},
		AssetType:                 mako_dashboard.EmoteAssetType_static,
		State:                     mako_dashboard.EmoteState_active,
		EnforceModerationWorkflow: true,
		ImageSource:               mako_dashboard.ImageSource_DefaultLibraryImage,
	}
}
