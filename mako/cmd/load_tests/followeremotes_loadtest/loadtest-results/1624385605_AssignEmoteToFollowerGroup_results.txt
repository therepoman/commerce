
====== AssignEmoteToFollowerGroup Results ======
Start Time: 2021-06-22 10:43:25.714479246 -0700 PDT m=+0.686293530 
End Time: 2021-06-22 11:13:25.617591232 -0700 PDT m=+1800.589405516 
Duration: 29m59.903111986s 
Requests: 18000 
Success Percentage: 0.709722 
Request Rate: 10.000538 
Successful Request Throughput: 7.097270 

Latency Average: 76.959713ms 
Latency p50: 75.749857ms 
Latency p90: 92.932865ms 
Latency p99: 227.624132ms 
Latency Max: 4.610917774s 

Status Codes: map[200:12775 412:5225] 
Errors: [412 Precondition Failed]
Error Sets: map[:1584]
