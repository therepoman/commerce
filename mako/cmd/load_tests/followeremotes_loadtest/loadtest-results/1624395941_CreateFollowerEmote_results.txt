
====== CreateFollowerEmote Results ======
Start Time: 2021-06-22 13:35:40.952058854 -0700 PDT m=+1.096641473 
End Time: 2021-06-22 14:05:40.849789181 -0700 PDT m=+1800.994371800 
Duration: 29m59.897730327s 
Requests: 18000 
Success Percentage: 0.708833 
Request Rate: 10.000568 
Successful Request Throughput: 7.088086 

Latency Average: 553.874275ms 
Latency p50: 650.832394ms 
Latency p90: 789.776277ms 
Latency p99: 1.09247909s 
Latency Max: 4.398622982s 

Status Codes: map[200:12759 400:4 412:5237] 
Errors: [412 Precondition Failed 400 Bad Request]
Error Sets: map[:4]
