package main

import (
	"context"
	"encoding/csv"
	"encoding/json"
	"fmt"
	"io"
	"math/rand"
	"net/http"
	"os"
	"path"
	"time"

	"code.justin.tv/commerce/gogogadget/random"
	"golang.org/x/sync/errgroup"

	log "code.justin.tv/commerce/logrus"
	mako "code.justin.tv/commerce/mako/twirp"
	mako_dashboard "code.justin.tv/commerce/mako/twirp/dashboard"
	vegeta "github.com/tsenart/vegeta/lib"
)

const (
	makoEndpoint = "https://main.us-west-2.prod.mako.twitch.a2z.com"
	outDir       = "loadtest-results"
	testDuration = time.Minute
)

var (
	makoClient          mako.Mako
	makoDashboardClient mako_dashboard.Mako
)

type loadTestUserData struct {
	groupID  string
	userID   string
	prefix   string
	emoteIDs []string
}

var testUsers = []loadTestUserData{
	{
		groupID:  "ab37ac2b-669f-4d23-9e6f-495cbaaff298", // GroupID for qa_partner_badmintonj follower emotes
		userID:   "672841770",                            // qa_partner_badmintonj
		prefix:   "badmintonb",
		emoteIDs: []string{"emotesv2_588c70be34584c589ada0ab66e6de32f", "emotesv2_db3a6d4852d1484991e6524208599583", "emotesv2_562d9b5f58644e87b3638082fa40d009", "emotesv2_40c0273953f04316b9a4da0a757f6e91", "emotesv2_39a9442e9b9749de91a8e65f274280dd"},
	},
	{
		groupID:  "a299246a-337f-454a-9cae-b5e266b8c56e", // GroupID for qa_partner_magnus follower emotes
		userID:   "672851769",                            // qa_partner_magnus
		prefix:   "mgpqa",
		emoteIDs: []string{"emotesv2_671f80a3b6ee416c88291016f677d5d4", "emotesv2_7f93d22a383248748aa02743b9bc2a0a", "emotesv2_f89f90485ab54baf8866e0e9e5f01926", "emotesv2_7c133db3755341398390ba8237a2aed4", "emotesv2_9457e105ea6b431d939ef711c26e8f7c"},
	},
}

func init() {
	rand.Seed(time.Now().UTC().UnixNano())

	makoClient = mako.NewMakoProtobufClient(makoEndpoint, http.DefaultClient)
	makoDashboardClient = mako_dashboard.NewMakoProtobufClient(makoEndpoint, http.DefaultClient)
}

func main() {
	// Uncomment the test you would like to run

	loadTestCreateFollowerEmote()
	// loadTestAssignEmoteToFollowerGroup()
}

func loadTestAssignEmoteToFollowerGroup() {
	log.Info("Removing emotes from Follower group before test")
	err := removeAllEmotesFromAllGroups()
	if err != nil {
		log.WithError(err).Fatal("[AssignEmoteToFollowerGroup] Failed to delete all emotes from all groups before test")
	}

	// We need to aggressively remove the emote from the Follower group to make sure we don't hit the follower emote limit quickly.
	testing := true
	eg := errgroup.Group{}
	eg.Go(func() error {
		for testing {
			err := removeAllEmotesFromAllGroups()
			if err != nil {
				log.WithError(err).Error("Failed to remove all emotes from group")
			}
		}
		return nil
	})

	attack(getAssignEmoteToFollowerGroupTest(), vegeta.Rate{
		Freq: 10,
		Per:  time.Second,
	}, 30*testDuration, "AssignEmoteToFollowerGroup")

	testing = false
	eg.Wait()

	log.Info("Cleaning up emotes after test")
	err = removeAllEmotesFromAllGroups()
	if err != nil {
		log.WithError(err).Fatal("[AssignEmoteToFollowerGroup] Failed to delete all emotes from all groups after test")
	}
}

func getAssignEmoteToFollowerGroupTest() vegeta.Targeter {
	headers := make(http.Header)
	headers.Add("Content-Type", "application/json")
	url := makoEndpoint + "/twirp/code.justin.tv.commerce.mako.dashboard.Mako/AssignEmoteToFollowerGroup"

	return func(t *vegeta.Target) error {
		testUser := getTestUser()

		// Build request
		req := getAssignEmoteToFollowerGroupRequest(testUser)

		reqBytes, err := json.Marshal(req)
		if err != nil {
			log.WithError(err).Error("Failed to marshal AssignEmoteToFollowerGroup request")
			return err
		}

		t.Method = http.MethodPost
		t.URL = url
		t.Header = headers
		t.Body = reqBytes

		return err
	}
}

func getAssignEmoteToFollowerGroupRequest(testUser loadTestUserData) *mako_dashboard.AssignEmoteToFollowerGroupRequest {
	emoteIndex := random.Int(0, len(testUser.emoteIDs)-1)
	emoteID := testUser.emoteIDs[emoteIndex]

	return &mako_dashboard.AssignEmoteToFollowerGroupRequest{
		ChannelId:        testUser.userID,
		RequestingUserId: testUser.userID,
		EmoteId:          emoteID,
	}
}

func loadTestCreateFollowerEmote() {
	log.Info("Cleaning up emotes before test")
	err := deleteAllEmotesFromAllGroups()
	if err != nil {
		log.WithError(err).Fatal("[CreateFollowerEmote] Failed to delete all emotes from all groups before test")
	}

	// We need to aggressively clear out the emote data while this test is running.
	// Otherwise, the account's emote libraries will fill up too quickly.
	testing := true
	eg := errgroup.Group{}
	eg.Go(func() error {
		for testing {
			err := deleteAllEmotesFromAllGroups()
			if err != nil {
				log.WithError(err).Error("Failed delete all emotes for all groups")
			}
		}
		return nil
	})

	attack(getCreateFollowerEmoteTest(), vegeta.Rate{
		Freq: 10,
		Per:  time.Second,
	}, testDuration*30, "CreateFollowerEmote")

	testing = false
	eg.Wait()

	log.Info("Cleaning up emotes after test")
	err = deleteAllEmotesFromAllGroups()
	if err != nil {
		log.WithError(err).Fatal("[CreateFollowerEmote] Failed to delete all emotes from all groups after test")
	}
}

func getTestUser() loadTestUserData {
	index := random.Int(0, len(testUsers)-1)
	return testUsers[index]
}

func getCreateFollowerEmoteTest() vegeta.Targeter {
	headers := make(http.Header)
	headers.Add("Content-Type", "application/json")
	url := makoEndpoint + "/twirp/code.justin.tv.commerce.mako.dashboard.Mako/CreateFollowerEmote"

	return func(t *vegeta.Target) error {
		testUser := getTestUser()

		// Build request
		req := getCreateFollowerEmoteRequest(testUser)

		reqBytes, err := json.Marshal(req)
		if err != nil {
			log.WithError(err).Error("Failed to marshal CreateFollowerEmote request")
			return err
		}

		t.Method = http.MethodPost
		t.URL = url
		t.Header = headers
		t.Body = reqBytes

		return err
	}
}

func getCreateFollowerEmoteRequest(testUser loadTestUserData) *mako_dashboard.CreateFollowerEmoteRequest {
	suffix := fmt.Sprintf("LT%d", random.Int(0, 1000000))
	return &mako_dashboard.CreateFollowerEmoteRequest{
		UserId:     testUser.userID,
		CodeSuffix: suffix,
		ImageAssets: []*mako_dashboard.EmoteImageAsset{
			{
				ImageIds: &mako_dashboard.EmoteImageIds{
					Image1XId: "267893713_1x_525b24fa-27b3-4002-8753-fb4b82578a71",
					Image2XId: "267893713_2x_525b24fa-27b3-4002-8753-fb4b82578a71",
					Image4XId: "267893713_4x_525b24fa-27b3-4002-8753-fb4b82578a71",
				},
				AssetType: mako_dashboard.EmoteAssetType_static,
			},
		},
		AssetType:                 mako_dashboard.EmoteAssetType_static,
		State:                     mako_dashboard.EmoteState_active,
		EnforceModerationWorkflow: true,
	}
}

func removeAllEmotesFromAllGroups() error {
	eg := errgroup.Group{}

	for _, testUser := range testUsers {
		tempTestUser := testUser
		eg.Go(func() error {
			err := removeAllEmotesFromGroups(tempTestUser)
			return err
		})
	}

	return eg.Wait()
}

func removeAllEmotesFromGroups(testUser loadTestUserData) error {
	for _, emoteId := range testUser.emoteIDs {
		_, err := makoDashboardClient.RemoveEmoteFromGroup(context.Background(), &mako_dashboard.RemoveEmoteFromGroupRequest{
			RequestingUserId: testUser.userID,
			EmoteId:          emoteId,
		})

		if err != nil {
			log.WithError(err).Error("Failed to remove an emote from the group")
			return err
		}
	}

	return nil
}

func deleteAllEmotesFromAllGroups() error {
	eg := errgroup.Group{}

	for _, testUser := range testUsers {
		tempTestUser := testUser
		eg.Go(func() error {
			err := deleteAllEmotesForUser(tempTestUser)
			return err
		})
	}

	return eg.Wait()
}

func deleteAllEmotesForUser(testUser loadTestUserData) error {
	emotesByGroupsResp, err := makoDashboardClient.GetEmotesByGroups(context.Background(), &mako_dashboard.GetEmotesByGroupsRequest{
		EmoteGroupIds: []string{testUser.groupID},
		Filter: &mako_dashboard.EmoteFilter{
			States: []mako_dashboard.EmoteState{mako_dashboard.EmoteState_active, mako_dashboard.EmoteState_pending},
		},
	})
	if err != nil {
		log.WithError(err).Error("Failed to get emotes by group")
		return err
	}

	for _, group := range emotesByGroupsResp.Groups {
		for _, emote := range group.Emotes {
			_, err := makoClient.DeleteEmoticon(context.Background(), &mako.DeleteEmoticonRequest{
				Id:                    emote.Id,
				DeleteImages:          false,
				RequiresPermittedUser: true,
				UserId:                testUser.userID,
			})
			if err != nil {
				log.WithError(err).WithField("emote_id", emote.Id).Error("Failed to delete group emotes")
				return err
			}
		}
	}

	return nil
}

func printMetrics(title string, metrics vegeta.Metrics, errors map[string]int) {
	dir, err := os.Getwd()

	if err != nil {
		panic(err)
	}

	dest := path.Join(dir, outDir, fmt.Sprintf("%d_%s_results.txt", time.Now().Unix(), title))

	writers := make([]io.Writer, 0)
	writers = append(writers, os.Stdout)

	os.MkdirAll(path.Join(dir, outDir), os.ModePerm)
	file, err := os.Create(dest)
	if err != nil {
		log.WithError(err).WithField("dest", dest).Info("Error creating results file")
	} else {
		writers = append(writers, file)
	}

	w := io.MultiWriter(writers...)

	_, _ = fmt.Fprintf(w, "\n====== %s Results ======\n", title)

	_, _ = fmt.Fprintf(w, "Start Time: %s \n", metrics.Earliest.String())
	_, _ = fmt.Fprintf(w, "End Time: %s \n", metrics.Latest.String())
	_, _ = fmt.Fprintf(w, "Duration: %s \n", metrics.Duration.String())

	_, _ = fmt.Fprintf(w, "Requests: %d \n", metrics.Requests)
	_, _ = fmt.Fprintf(w, "Success Percentage: %f \n", metrics.Success)
	_, _ = fmt.Fprintf(w, "Request Rate: %f \n", metrics.Rate)
	_, _ = fmt.Fprintf(w, "Successful Request Throughput: %f \n\n", metrics.Throughput)

	_, _ = fmt.Fprintf(w, "Latency Average: %s \n", metrics.Latencies.Mean)
	_, _ = fmt.Fprintf(w, "Latency p50: %s \n", metrics.Latencies.P50)
	_, _ = fmt.Fprintf(w, "Latency p90: %s \n", metrics.Latencies.P90)
	_, _ = fmt.Fprintf(w, "Latency p99: %s \n", metrics.Latencies.P99)
	_, _ = fmt.Fprintf(w, "Latency Max: %s \n\n", metrics.Latencies.Max)

	_, _ = fmt.Fprintf(w, "Status Codes: %v \n", metrics.StatusCodes)
	_, _ = fmt.Fprintf(w, "Errors: %v\n", metrics.Errors)
	_, _ = fmt.Fprintf(w, "Error Sets: %v\n", errors)
}

func attack(targeter vegeta.Targeter, pacer vegeta.Pacer, dur time.Duration, name string) {
	log.Info(fmt.Sprintf("Starting attack on %s at %f initial TPS for %s with final TPS of %f", name, pacer.Rate(time.Second), dur.String(), pacer.Rate(dur)))
	attacker := vegeta.NewAttacker()
	var metrics vegeta.Metrics
	errors := map[string]int{}
	for res := range attacker.Attack(targeter, pacer, dur, name+"-Attack") {
		metrics.Add(res)
		if res.Code != 200 {
			body := string(res.Body)
			errors[body] = errors[body] + 1
			log.Error(res.Error)
		}
	}
	metrics.Close()
	printMetrics(name, metrics, unpackErrors(errors))
}

func unpackErrors(errors map[string]int) map[string]int {
	type MetricError struct {
		Message string `json:"message"`
	}

	unpackedErrors := map[string]int{}
	for err, count := range errors {
		var metricError MetricError
		json.Unmarshal([]byte(err), &metricError)
		unpackedErrors[metricError.Message] = count
	}

	return unpackedErrors
}

func getIDsFromFile(filename string) []string {
	var ids []string

	// Open the file
	csvfile, err := os.Open(filename)
	if err != nil {
		log.Fatalln("Couldn't open the csv file", err)
	}

	// Parse the file
	r := csv.NewReader(csvfile)

	// Iterate through the records
	for {
		// Read each record from csv
		record, err := r.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal(err)
		}

		ids = append(ids, record[0])
	}

	return ids
}
