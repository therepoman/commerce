package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"math/rand"
	"net/http"
	"os"
	"path"
	"time"

	"code.justin.tv/commerce/gogogadget/random"
	log "code.justin.tv/commerce/logrus"
	mako "code.justin.tv/commerce/mako/twirp"
	mako_dashboard "code.justin.tv/commerce/mako/twirp/dashboard"
	vegeta "github.com/tsenart/vegeta/v12/lib"
	"golang.org/x/sync/errgroup"
)

const (
	makoEndpoint = "https://main.us-west-2.prod.mako.twitch.a2z.com"
	outDir       = "loadtest-results"
	testDuration = time.Minute * 30
	requestTPS   = 10
)

type loadTestUserData struct {
	userID string
	prefix string
}

var testUsers = []loadTestUserData{
	{
		userID: "725111641", //qa_naeunice_affiliate_1
		prefix: "qanaeu1",
	},
	{
		userID: "725311786", //qa_naeunice_affiliate_2
		prefix: "qanaeu2",
	},
	{
		userID: "722622612", //qa_naeunice_affiliate
		prefix: "qanaeu",
	},
	{
		userID: "655448192", //qa_naeunice_partner
		prefix: "qa12345",
	},
}

// stuff we only want to init once
var (
	makoClient mako.Mako
)

func init() {
	rand.Seed(time.Now().UTC().UnixNano())

	makoClient = mako.NewMakoProtobufClient(makoEndpoint, http.DefaultClient)
}

func main() {
	// Uncomment the test you would like to run
	loadTestCreateAnimatedEmote()
}

func loadTestCreateAnimatedEmote() {
	log.Info("Cleaning up emotes before test")
	err := deleteAllEmotesFromAllGroups()
	if err != nil {
		log.WithError(err).Fatal("[CreateAnimatedEmote] Failed to delete all emotes from all groups before test")
	}

	// We need to aggressively clear out the emote data while this test is running.
	// Otherwise, the account's emote libraries will fill up too quickly.
	testing := true
	eg := errgroup.Group{}
	eg.Go(func() error {
		for testing {
			time.Sleep(time.Millisecond * 100)
			err := deleteAllEmotesFromAllGroups()
			if err != nil {
				log.WithError(err).Error("Failed delete all emotes for all groups")
			}
		}
		return nil
	})

	attack(getCreateAnimatedEmoteTest(), vegeta.Rate{
		Freq: requestTPS,
		Per:  time.Second,
	}, testDuration, "CreateEmoteAnimated")

	testing = false
	eg.Wait()

	log.Info("Cleaning up emotes after test")
	err = deleteAllEmotesFromAllGroups()
	if err != nil {
		log.WithError(err).Fatal("[CreateAnimatedEmote] Failed to delete all emotes from all groups after test")
	}
}

func getCreateAnimatedEmoteTest() vegeta.Targeter {
	headers := make(http.Header)
	headers.Add("Content-Type", "application/json")
	url := makoEndpoint + "/twirp/code.justin.tv.commerce.mako.dashboard.Mako/CreateEmote"

	return func(t *vegeta.Target) error {
		testUser := getTestUser()

		// Build request
		req := getCreateAnimatedEmoteRequest(testUser)

		reqBytes, err := json.Marshal(req)
		if err != nil {
			log.WithError(err).Error("Failed to marshal CreateEmote request")
			return err
		}

		t.Method = http.MethodPost
		t.URL = url
		t.Header = headers
		t.Body = reqBytes

		return err
	}
}

func getCreateAnimatedEmoteRequest(testUser loadTestUserData) *mako_dashboard.CreateEmoteRequest {
	suffix := fmt.Sprintf("LT%d", random.Int(0, 1000000))

	return &mako_dashboard.CreateEmoteRequest{
		UserId:     testUser.userID,
		Code:       testUser.prefix + suffix,
		CodeSuffix: suffix,
		GroupIds:   []string{},
		ImageAssets: []*mako_dashboard.EmoteImageAsset{
			{
				ImageIds: &mako_dashboard.EmoteImageIds{
					Image1XId: "655448192_1x_49e7a984-9323-4e51-8a4e-9f20f7b99f98",
					Image2XId: "655448192_2x_49e7a984-9323-4e51-8a4e-9f20f7b99f98",
					Image4XId: "655448192_4x_49e7a984-9323-4e51-8a4e-9f20f7b99f98",
				},
				AssetType: mako_dashboard.EmoteAssetType_static,
			},
			{
				ImageIds: &mako_dashboard.EmoteImageIds{
					Image1XId: "655448192_1x_49e7a984-9323-4e51-8a4e-9f20f7b99f98_animated",
					Image2XId: "655448192_2x_49e7a984-9323-4e51-8a4e-9f20f7b99f98_animated",
					Image4XId: "655448192_4x_49e7a984-9323-4e51-8a4e-9f20f7b99f98_animated",
				},
				AssetType: mako_dashboard.EmoteAssetType_animated,
			},
		},
		AssetType: mako_dashboard.EmoteAssetType_animated,
		Origin:    mako_dashboard.EmoteOrigin_Archive,
		State:     mako_dashboard.EmoteState_archived,
	}
}

func getTestUser() loadTestUserData {
	index := random.Int(0, len(testUsers)-1)
	return testUsers[index]
}

func deleteAllEmotesFromAllGroups() error {
	eg := errgroup.Group{}

	for _, testUser := range testUsers {
		tempTestUser := testUser
		eg.Go(func() error {
			err := deleteAllEmotesForUser(tempTestUser)
			return err
		})
	}

	return eg.Wait()
}

func deleteAllEmotesForUser(testUser loadTestUserData) error {
	allEmotes, err := makoClient.GetAllEmotesByOwnerId(context.Background(), &mako.GetAllEmotesByOwnerIdRequest{
		OwnerId: testUser.userID,
		Filter: &mako.EmoteFilter{
			States: []mako.EmoteState{mako.EmoteState_active, mako.EmoteState_archived, mako.EmoteState_pending_archived, mako.EmoteState_pending},
		},
	})
	if err != nil {
		log.WithError(err).Error("Failed to get emotes by group")
		return err
	}

	for _, emote := range allEmotes.GetEmoticons() {
		_, err := makoClient.DeleteEmoticon(context.Background(), &mako.DeleteEmoticonRequest{
			Id:                    emote.Id,
			DeleteImages:          false,
			RequiresPermittedUser: true,
			UserId:                testUser.userID,
		})
		if err != nil {
			log.WithError(err).WithField("emote_id", emote.Id).Error("Failed to delete group emotes")
			return err
		}
	}

	return nil
}

func printMetrics(title string, metrics vegeta.Metrics, errors map[string]int) {
	dir, err := os.Getwd()

	if err != nil {
		panic(err)
	}

	dest := path.Join(dir, outDir, fmt.Sprintf("%d_%s_results.txt", time.Now().Unix(), title))

	writers := make([]io.Writer, 0)
	writers = append(writers, os.Stdout)

	os.MkdirAll(path.Join(dir, outDir), os.ModePerm)
	file, err := os.Create(dest)
	if err != nil {
		log.WithError(err).WithField("dest", dest).Info("Error creating results file")
	} else {
		writers = append(writers, file)
	}

	w := io.MultiWriter(writers...)

	_, _ = fmt.Fprintf(w, "\n====== %s Results ======\n", title)

	_, _ = fmt.Fprintf(w, "Start Time: %s \n", metrics.Earliest.String())
	_, _ = fmt.Fprintf(w, "End Time: %s \n", metrics.Latest.String())
	_, _ = fmt.Fprintf(w, "Duration: %s \n", metrics.Duration.String())

	_, _ = fmt.Fprintf(w, "Requests: %d \n", metrics.Requests)
	_, _ = fmt.Fprintf(w, "Success Percentage: %f \n", metrics.Success)
	_, _ = fmt.Fprintf(w, "Request Rate: %f \n", metrics.Rate)
	_, _ = fmt.Fprintf(w, "Successful Request Throughput: %f \n\n", metrics.Throughput)

	_, _ = fmt.Fprintf(w, "Latency Average: %s \n", metrics.Latencies.Mean)
	_, _ = fmt.Fprintf(w, "Latency p50: %s \n", metrics.Latencies.P50)
	_, _ = fmt.Fprintf(w, "Latency p90: %s \n", metrics.Latencies.P90)
	_, _ = fmt.Fprintf(w, "Latency p99: %s \n", metrics.Latencies.P99)
	_, _ = fmt.Fprintf(w, "Latency Max: %s \n\n", metrics.Latencies.Max)

	_, _ = fmt.Fprintf(w, "Status Codes: %v \n", metrics.StatusCodes)
	_, _ = fmt.Fprintf(w, "Errors: %v\n", metrics.Errors)
	_, _ = fmt.Fprintf(w, "Error Sets: %v\n", errors)
}

func attack(targeter vegeta.Targeter, pacer vegeta.Pacer, dur time.Duration, name string) {
	log.Info(fmt.Sprintf("Starting attack on %s. Initial TPS: %f Final TPS: %f Duration: %s", name, pacer.Rate(time.Second), pacer.Rate(dur), dur.String()))
	attacker := vegeta.NewAttacker()
	var metrics vegeta.Metrics
	errors := map[string]int{}
	for res := range attacker.Attack(targeter, pacer, dur, name+"-Attack") {
		metrics.Add(res)
		if res.Code != 200 {
			body := string(res.Body)
			errors[body] = errors[body] + 1
			log.Error(res.Error)
		}
		// This code works, but is too slow. Since this step is run independently from the attack, it was too slow
		// to clear out emotes before the account limits were reached. Keeping it here in case it is helpful for future
		// lower-TPS tests.
		//else {
		//	// Clear up CreateEmote data
		//	if name == "CreateEmote" {
		//		var response LoadTestCreateEmoteResponse
		//		err := json.Unmarshal(res.Body, &response)
		//		if err != nil {
		//			log.WithError(err).Error("failed to unmarshal CreateEmote response after attack")
		//		}
		//
		//		err = deleteEmote(response.Id, response.Emote.OwnerId)
		//		if err != nil {
		//			log.WithError(err).Error("failed to delete emote after CreateEmote attack")
		//		}
		//	}
		//}
	}
	metrics.Close()
	printMetrics(name, metrics, unpackErrors(errors))
}

func unpackErrors(errors map[string]int) map[string]int {
	type MetricError struct {
		Message string `json:"message"`
	}

	unpackedErrors := map[string]int{}
	for err, count := range errors {
		var metricError MetricError
		json.Unmarshal([]byte(err), &metricError)
		unpackedErrors[metricError.Message] = count
	}

	return unpackedErrors
}
