package main

import (
	"context"
	"encoding/csv"
	"encoding/json"
	"fmt"
	"io"
	"math/rand"
	"net/http"
	"os"
	"path"
	"time"

	"code.justin.tv/commerce/gogogadget/random"
	log "code.justin.tv/commerce/logrus"
	mako "code.justin.tv/commerce/mako/twirp"
	mako_dashboard "code.justin.tv/commerce/mako/twirp/dashboard"
	substwirp "code.justin.tv/revenue/subscriptions/twirp"
	vegeta "github.com/tsenart/vegeta/lib"
	"golang.org/x/sync/errgroup"
)

const (
	makoEndpoint          = "https://main.us-west-2.prod.mako.twitch.a2z.com"
	subscriptionsEndpoint = "https://main.us-west-2.prod.subscriptions.twitch.a2z.com"
	outDir                = "loadtest-results"
	testDuration          = time.Minute * 15
)

type loadTestUserData struct {
	groupID              string
	userID               string
	prefix               string
	tier1productID       string
	tier1groupID         string
	tier1AnimatedGroupID string
}

var testUsers = []loadTestUserData{
	{
		groupID:              "damm2loadtest1",
		userID:               "267893713",
		prefix:               "wolfpack",
		tier1productID:       "1586039",
		tier1groupID:         "1591779",
		tier1AnimatedGroupID: "457625268",
	},
	//{
	//	groupID: "damm2loadtest2",
	//	userID:  "408653515",
	//	prefix:  "qatww2",
	//},
	//{
	//	groupID: "damm2loadtest3",
	//	userID:  "499158006",
	//	prefix:  "qana",
	//},
}

// stuff we only want to init once
var (
	makoClient          mako.Mako
	makoDashboardClient mako_dashboard.Mako
)

func init() {
	rand.Seed(time.Now().UTC().UnixNano())

	makoClient = mako.NewMakoProtobufClient(makoEndpoint, http.DefaultClient)
	makoDashboardClient = mako_dashboard.NewMakoProtobufClient(makoEndpoint, http.DefaultClient)
}

func main() {
	// Uncomment the test you would like to run

	//loadTestCreateEmote()
	//loadTestCreateAnimatedEmote()
	//loadTestGetEmotesByIDs()
	//loadTestGetEmotesByGroups()
	//loadTestGetAllEmotesByOwnerId()
	//loadTestGetUserEmoteLimits()
	//loadTestAssignEmoteToProduct()
}

func loadTestGetEmotesByIDs() {
	attack(getGetEmotesByIDsTest(), vegeta.Rate{
		Freq: 10,
		Per:  time.Second,
	}, testDuration, "GetEmotesByIDs")
}

func getGetEmotesByIDsTest() vegeta.Targeter {
	headers := make(http.Header)
	headers.Add("Content-Type", "application/json")
	url := makoEndpoint + "/twirp/code.justin.tv.commerce.mako.dashboard.Mako/GetEmotesByIDs"

	emoteIds := getIDsFromFile("./data/emote_ids_100.csv")

	return func(t *vegeta.Target) error {
		req := mako_dashboard.GetEmotesByIDsRequest{
			EmoteIds: []string{
				emoteIds[rand.Intn(101)],
			},
		}

		reqBytes, err := json.Marshal(req)
		if err != nil {
			log.WithError(err).Error("Failed to marshal GetEmotesByIDs request")
			return err
		}

		t.Method = http.MethodPost
		t.URL = url
		t.Header = headers
		t.Body = reqBytes

		return err
	}
}

func loadTestGetEmotesByGroups() {
	attack(getGetEmotesByGroupsTest(), vegeta.Rate{
		Freq: 50,
		Per:  time.Second,
	}, testDuration, "GetEmotesByGroups")
}

func getGetEmotesByGroupsTest() vegeta.Targeter {
	headers := make(http.Header)
	headers.Add("Content-Type", "application/json")
	url := makoEndpoint + "/twirp/code.justin.tv.commerce.mako.dashboard.Mako/GetEmotesByGroups"

	emoteGroupIds := getIDsFromFile("./data/emote_group_ids_1000.csv")

	return func(t *vegeta.Target) error {
		req := getGetEmotesByGroupsRequest(emoteGroupIds)

		reqBytes, err := json.Marshal(req)
		if err != nil {
			log.WithError(err).Error("Failed to marshal GetEmotesByGroups request")
			return err
		}

		t.Method = http.MethodPost
		t.URL = url
		t.Header = headers
		t.Body = reqBytes

		return err
	}
}

// Returns a GetEmotesByGroupsRequest that contains [1, 20] emote group ids
func getGetEmotesByGroupsRequest(allGroupIds []string) mako_dashboard.GetEmotesByGroupsRequest {
	numIdsToRequest := rand.Intn(20) + 1
	var groupIdsToRequest []string

	for i := 0; i < numIdsToRequest; i++ {
		groupIdsToRequest = append(groupIdsToRequest, allGroupIds[rand.Intn(1001)])
	}

	return mako_dashboard.GetEmotesByGroupsRequest{
		EmoteGroupIds: groupIdsToRequest,
		Filter: &mako_dashboard.EmoteFilter{
			Domain: mako_dashboard.Domain_emotes,
			States: []mako_dashboard.EmoteState{mako_dashboard.EmoteState_active, mako_dashboard.EmoteState_pending},
		},
	}
}

func loadTestGetAllEmotesByOwnerId() {
	attack(getGetAllEmotesByOwnerIdTest(), vegeta.Rate{
		Freq: 50,
		Per:  time.Second,
	}, testDuration, "GetAllEmotesByOwnerId")
}

func getGetAllEmotesByOwnerIdTest() vegeta.Targeter {
	headers := make(http.Header)
	headers.Add("Content-Type", "application/json")
	url := makoEndpoint + "/twirp/code.justin.tv.commerce.mako.Mako/GetAllEmotesByOwnerId"

	emoteOwnerIds := getIDsFromFile("./data/emote_owner_ids_1000.csv")

	return func(t *vegeta.Target) error {
		ownerId := emoteOwnerIds[rand.Intn(1001)]

		req := mako.GetAllEmotesByOwnerIdRequest{
			OwnerId: ownerId,
			Filter: &mako.EmoteFilter{
				Domain: mako.Domain_emotes,
				States: []mako.EmoteState{mako.EmoteState_active, mako.EmoteState_pending, mako.EmoteState_archived, mako.EmoteState_pending_archived},
			},
			EnforceOwnerRestriction: true,
			RequestingUserId:        ownerId,
		}

		reqBytes, err := json.Marshal(req)
		if err != nil {
			log.WithError(err).Error("Failed to marshal GetAllEmotesByOwnerId request")
			return err
		}

		t.Method = http.MethodPost
		t.URL = url
		t.Header = headers
		t.Body = reqBytes

		return err
	}
}

func loadTestGetUserEmoteLimits() {
	// Ramp from 1 -> 50 TPS over 5 mins
	attack(getGetUserEmoteLimitsTest(), vegeta.LinearPacer{
		StartAt: vegeta.Rate{
			Freq: 1,
			Per:  time.Second,
		},
		Slope: 0.1667,
	}, 5*time.Minute, "GetUserEmoteLimits")

	// then, sustain 50 TPS for 25 mins
	attack(getGetUserEmoteLimitsTest(), vegeta.Rate{
		Freq: 50,
		Per:  time.Second,
	}, 25*time.Minute, "GetUserEmoteLimits")
}

func getGetUserEmoteLimitsTest() vegeta.Targeter {
	headers := make(http.Header)
	headers.Add("Content-Type", "application/json")
	url := makoEndpoint + "/twirp/code.justin.tv.commerce.mako.dashboard.Mako/GetUserEmoteLimits"

	emoteOwnerIds := getIDsFromFile("./data/emote_owner_ids_50000.csv")

	return func(t *vegeta.Target) error {
		ownerId := emoteOwnerIds[rand.Intn(50001)]

		req := mako_dashboard.GetUserEmoteLimitsRequest{
			UserId:           ownerId,
			RequestingUserId: ownerId,
		}

		reqBytes, err := json.Marshal(req)
		if err != nil {
			log.WithError(err).Error("Failed to marshal GetUserEmoteLimits request")
			return err
		}

		t.Method = http.MethodPost
		t.URL = url
		t.Header = headers
		t.Body = reqBytes

		return err
	}
}

func loadTestCreateEmote() {
	log.Info("Cleaning up emotes before test")
	err := deleteAllEmotesFromAllGroups()
	if err != nil {
		log.WithError(err).Fatal("[CreateEmote] Failed to delete all emotes from all groups before test")
	}

	// We need to aggressively clear out the emote data while this test is running.
	// Otherwise, the account's emote libraries will fill up too quickly.
	testing := true
	eg := errgroup.Group{}
	eg.Go(func() error {
		for testing {
			time.Sleep(time.Millisecond * 100)
			err := deleteAllEmotesFromAllGroups()
			if err != nil {
				log.WithError(err).Error("Failed delete all emotes for all groups")
			}
		}
		return nil
	})

	attack(getCreateEmoteTest(false), vegeta.Rate{
		Freq: 5,
		Per:  time.Second,
	}, testDuration, "CreateEmote")

	testing = false
	eg.Wait()

	log.Info("Cleaning up emotes after test")
	err = deleteAllEmotesFromAllGroups()
	if err != nil {
		log.WithError(err).Fatal("[CreateEmote] Failed to delete all emotes from all groups after test")
	}
}

func loadTestCreateAnimatedEmote() {
	log.Info("Cleaning up emotes before test")
	err := deleteAllEmotesFromAllGroups()
	if err != nil {
		log.WithError(err).Fatal("[CreateAnimatedEmote] Failed to delete all emotes from all groups before test")
	}

	// We need to aggressively clear out the emote data while this test is running.
	// Otherwise, the account's emote libraries will fill up too quickly.
	testing := true
	eg := errgroup.Group{}
	eg.Go(func() error {
		for testing {
			time.Sleep(time.Millisecond * 100)
			err := deleteAllEmotesFromAllGroups()
			if err != nil {
				log.WithError(err).Error("Failed delete all emotes for all groups")
			}
		}
		return nil
	})

	attack(getCreateEmoteTest(true), vegeta.Rate{
		Freq: 5,
		Per:  time.Second,
	}, testDuration, "CreateEmoteAnimated")

	testing = false
	eg.Wait()

	log.Info("Cleaning up emotes after test")
	err = deleteAllEmotesFromAllGroups()
	if err != nil {
		log.WithError(err).Fatal("[CreateAnimatedEmote] Failed to delete all emotes from all groups after test")
	}
}

func getCreateEmoteTest(isAnimated bool) vegeta.Targeter {
	headers := make(http.Header)
	headers.Add("Content-Type", "application/json")
	url := makoEndpoint + "/twirp/code.justin.tv.commerce.mako.dashboard.Mako/CreateEmote"

	return func(t *vegeta.Target) error {
		testUser := getTestUser()

		// Build request
		req := getCreateEmoteRequest(testUser)
		if isAnimated {
			req = getCreateAnimatedEmoteRequest(testUser)
		}

		reqBytes, err := json.Marshal(req)
		if err != nil {
			log.WithError(err).Error("Failed to marshal CreateEmote request")
			return err
		}

		t.Method = http.MethodPost
		t.URL = url
		t.Header = headers
		t.Body = reqBytes

		return err
	}
}

func getCreateEmoteRequest(testUser loadTestUserData) *mako_dashboard.CreateEmoteRequest {
	suffix := fmt.Sprintf("LT%d", random.Int(0, 1000000))
	possibleOrigins := []mako_dashboard.EmoteOrigin{
		mako_dashboard.EmoteOrigin_BitsBadgeTierEmotes,
		mako_dashboard.EmoteOrigin_Archive,
		mako_dashboard.EmoteOrigin_Subscriptions,
	}
	emoteOrigin := possibleOrigins[rand.Intn(3)]

	emoteState := mako_dashboard.EmoteState_active
	emoteGroupIDs := []string{testUser.tier1groupID}
	if emoteOrigin == mako_dashboard.EmoteOrigin_Archive {
		emoteState = mako_dashboard.EmoteState_archived
		emoteGroupIDs = []string{}
	}

	return &mako_dashboard.CreateEmoteRequest{
		UserId:     testUser.userID,
		Code:       testUser.prefix + suffix,
		CodeSuffix: suffix,
		GroupIds:   emoteGroupIDs,
		ImageAssets: []*mako_dashboard.EmoteImageAsset{
			{
				ImageIds: &mako_dashboard.EmoteImageIds{
					Image1XId: "267893713_1x_525b24fa-27b3-4002-8753-fb4b82578a71",
					Image2XId: "267893713_2x_525b24fa-27b3-4002-8753-fb4b82578a71",
					Image4XId: "267893713_4x_525b24fa-27b3-4002-8753-fb4b82578a71",
				},
				AssetType: mako_dashboard.EmoteAssetType_static,
			},
		},
		AssetType:                 mako_dashboard.EmoteAssetType_static,
		Origin:                    emoteOrigin,
		State:                     emoteState,
		EnforceModerationWorkflow: true,
	}
}

func getCreateAnimatedEmoteRequest(testUser loadTestUserData) *mako_dashboard.CreateEmoteRequest {
	suffix := fmt.Sprintf("LT%d", random.Int(0, 1000000))

	return &mako_dashboard.CreateEmoteRequest{
		UserId:     testUser.userID,
		Code:       testUser.prefix + suffix,
		CodeSuffix: suffix,
		GroupIds:   []string{},
		ImageAssets: []*mako_dashboard.EmoteImageAsset{
			{
				ImageIds: &mako_dashboard.EmoteImageIds{
					Image1XId: "267893713_1x_bf4d153c-71ed-44de-8584-a814f2008795",
					Image2XId: "267893713_2x_bf4d153c-71ed-44de-8584-a814f2008795",
					Image4XId: "267893713_4x_bf4d153c-71ed-44de-8584-a814f2008795",
				},
				AssetType: mako_dashboard.EmoteAssetType_static,
			},
			{
				ImageIds: &mako_dashboard.EmoteImageIds{
					Image1XId: "267893713_1x_bf4d153c-71ed-44de-8584-a814f2008795_animated",
					Image2XId: "267893713_2x_bf4d153c-71ed-44de-8584-a814f2008795_animated",
					Image4XId: "267893713_4x_bf4d153c-71ed-44de-8584-a814f2008795_animated",
				},
				AssetType: mako_dashboard.EmoteAssetType_animated,
			},
		},
		AssetType:                 mako_dashboard.EmoteAssetType_animated,
		Origin:                    mako_dashboard.EmoteOrigin_Archive,
		State:                     mako_dashboard.EmoteState_archived,
		EnforceModerationWorkflow: true,
	}
}

func loadTestAssignEmoteToProduct() {
	log.Info("Cleaning up emotes before test")
	err := deleteAllEmotesForUser(testUsers[0])
	if err != nil {
		log.WithError(err).Fatal("[AssignEmoteToProduct] Failed to delete all emotes for test user after test")
	}

	log.Info("Creating emotes for test")
	var emoteIDs []string
	for i := 0; i < 60; i++ {
		createEmoteRequest := getCreateEmoteRequest(testUsers[0])

		resp, err := makoDashboardClient.CreateEmote(context.Background(), createEmoteRequest)
		if err != nil {
			log.WithError(err).Error("error creating emote during setup of AssignEmoteToProduct test")
		}

		emoteIDs = append(emoteIDs, resp.Id)
	}

	// We need to aggressively remove emotes from groups while this test is running.
	// Otherwise, the account's emote tier slots will fill up too quickly.
	testing := true
	eg := errgroup.Group{}
	eg.Go(func() error {
		for testing {
			time.Sleep(time.Millisecond * 100)
			err := removeAllEmotesFromGroupsForUser(testUsers[0])
			if err != nil {
				log.WithError(err).Error("Failed remove all emotes from groups")
			}
		}
		return nil
	})

	attack(getAssignEmoteToProductTest(emoteIDs), vegeta.Rate{
		Freq: 10,
		Per:  time.Second,
	}, testDuration, "AssignEmoteToProduct")

	log.Info("Cleaning up emotes after test")
	err = deleteAllEmotesForUser(testUsers[0])
	if err != nil {
		log.WithError(err).Fatal("[AssignEmoteToProduct] Failed to delete all emotes for test user after test")
	}
}

func getAssignEmoteToProductTest(emoteIDs []string) vegeta.Targeter {
	headers := make(http.Header)
	headers.Add("Content-Type", "application/json")
	url := subscriptionsEndpoint + "/twirp/code.justin.tv.revenue.subscriptions.Subscriptions/AssignEmoteToProduct"

	return func(t *vegeta.Target) error {
		randEmoteInt := rand.Intn(len(emoteIDs))

		req := substwirp.AssignEmoteToProductRequest{
			RequestingUserId: testUsers[0].userID,
			EmoteId:          emoteIDs[randEmoteInt],
			ProductId:        testUsers[0].tier1productID,
		}

		reqBytes, err := json.Marshal(&req)
		if err != nil {
			log.WithError(err).Error("Failed to marshal AssignEmoteToProduct request")
			return err
		}

		t.Method = http.MethodPost
		t.URL = url
		t.Header = headers
		t.Body = reqBytes

		return err
	}
}

func getTestUser() loadTestUserData {
	index := random.Int(0, len(testUsers)-1)
	return testUsers[index]
}

func deleteAllEmotesFromAllGroups() error {
	eg := errgroup.Group{}

	for _, testUser := range testUsers {
		tempTestUser := testUser
		eg.Go(func() error {
			err := deleteAllEmotesForUser(tempTestUser)
			return err
		})
	}

	return eg.Wait()
}

func deleteAllEmotesForUser(testUser loadTestUserData) error {
	allEmotes, err := makoClient.GetAllEmotesByOwnerId(context.Background(), &mako.GetAllEmotesByOwnerIdRequest{
		OwnerId: testUser.userID,
		Filter: &mako.EmoteFilter{
			States: []mako.EmoteState{mako.EmoteState_active, mako.EmoteState_archived},
		},
	})
	if err != nil {
		log.WithError(err).Error("Failed to get emotes by group")
		return err
	}

	for _, emote := range allEmotes.GetEmoticons() {
		_, err := makoClient.DeleteEmoticon(context.Background(), &mako.DeleteEmoticonRequest{
			Id:                    emote.Id,
			DeleteImages:          false,
			RequiresPermittedUser: true,
			UserId:                testUser.userID,
		})
		if err != nil {
			log.WithError(err).WithField("emote_id", emote.Id).Error("Failed to delete group emotes")
			return err
		}
	}

	return nil
}

func removeAllEmotesFromGroupsForUser(testUser loadTestUserData) error {
	allEmotes, err := makoClient.GetAllEmotesByOwnerId(context.Background(), &mako.GetAllEmotesByOwnerIdRequest{
		OwnerId: testUser.userID,
		Filter: &mako.EmoteFilter{
			States: []mako.EmoteState{mako.EmoteState_active},
		},
	})
	if err != nil {
		log.WithError(err).Error("Failed to get emotes for user")
		return err
	}

	for _, emote := range allEmotes.GetEmoticons() {
		_, err := makoDashboardClient.RemoveEmoteFromGroup(context.Background(), &mako_dashboard.RemoveEmoteFromGroupRequest{
			RequestingUserId: testUser.userID,
			EmoteId:          emote.Id,
		})
		if err != nil {
			log.WithError(err).WithField("emote_id", emote.Id).Error("Failed to remove emote from group")
			return err
		}
	}

	return nil
}

func printMetrics(title string, metrics vegeta.Metrics, errors map[string]int) {
	dir, err := os.Getwd()

	if err != nil {
		panic(err)
	}

	dest := path.Join(dir, outDir, fmt.Sprintf("%d_%s_results.txt", time.Now().Unix(), title))

	writers := make([]io.Writer, 0)
	writers = append(writers, os.Stdout)

	os.MkdirAll(path.Join(dir, outDir), os.ModePerm)
	file, err := os.Create(dest)
	if err != nil {
		log.WithError(err).WithField("dest", dest).Info("Error creating results file")
	} else {
		writers = append(writers, file)
	}

	w := io.MultiWriter(writers...)

	_, _ = fmt.Fprintf(w, "\n====== %s Results ======\n", title)

	_, _ = fmt.Fprintf(w, "Start Time: %s \n", metrics.Earliest.String())
	_, _ = fmt.Fprintf(w, "End Time: %s \n", metrics.Latest.String())
	_, _ = fmt.Fprintf(w, "Duration: %s \n", metrics.Duration.String())

	_, _ = fmt.Fprintf(w, "Requests: %d \n", metrics.Requests)
	_, _ = fmt.Fprintf(w, "Success Percentage: %f \n", metrics.Success)
	_, _ = fmt.Fprintf(w, "Request Rate: %f \n", metrics.Rate)
	_, _ = fmt.Fprintf(w, "Successful Request Throughput: %f \n\n", metrics.Throughput)

	_, _ = fmt.Fprintf(w, "Latency Average: %s \n", metrics.Latencies.Mean)
	_, _ = fmt.Fprintf(w, "Latency p50: %s \n", metrics.Latencies.P50)
	_, _ = fmt.Fprintf(w, "Latency p90: %s \n", metrics.Latencies.P90)
	_, _ = fmt.Fprintf(w, "Latency p99: %s \n", metrics.Latencies.P99)
	_, _ = fmt.Fprintf(w, "Latency Max: %s \n\n", metrics.Latencies.Max)

	_, _ = fmt.Fprintf(w, "Status Codes: %v \n", metrics.StatusCodes)
	_, _ = fmt.Fprintf(w, "Errors: %v\n", metrics.Errors)
	_, _ = fmt.Fprintf(w, "Error Sets: %v\n", errors)
}

func attack(targeter vegeta.Targeter, pacer vegeta.Pacer, dur time.Duration, name string) {
	log.Info(fmt.Sprintf("Starting attack on %s. Initial TPS: %f Final TPS: %f Duration: %s", name, pacer.Rate(time.Second), pacer.Rate(dur), dur.String()))
	attacker := vegeta.NewAttacker()
	var metrics vegeta.Metrics
	errors := map[string]int{}
	for res := range attacker.Attack(targeter, pacer, dur, name+"-Attack") {
		metrics.Add(res)
		if res.Code != 200 {
			body := string(res.Body)
			errors[body] = errors[body] + 1
			log.Error(res.Error)
		}
		// This code works, but is too slow. Since this step is run independently from the attack, it was too slow
		// to clear out emotes before the account limits were reached. Keeping it here in case it is helpful for future
		// lower-TPS tests.
		//else {
		//	// Clear up CreateEmote data
		//	if name == "CreateEmote" {
		//		var response LoadTestCreateEmoteResponse
		//		err := json.Unmarshal(res.Body, &response)
		//		if err != nil {
		//			log.WithError(err).Error("failed to unmarshal CreateEmote response after attack")
		//		}
		//
		//		err = deleteEmote(response.Id, response.Emote.OwnerId)
		//		if err != nil {
		//			log.WithError(err).Error("failed to delete emote after CreateEmote attack")
		//		}
		//	}
		//}
	}
	metrics.Close()
	printMetrics(name, metrics, unpackErrors(errors))
}

func unpackErrors(errors map[string]int) map[string]int {
	type MetricError struct {
		Message string `json:"message"`
	}

	unpackedErrors := map[string]int{}
	for err, count := range errors {
		var metricError MetricError
		json.Unmarshal([]byte(err), &metricError)
		unpackedErrors[metricError.Message] = count
	}

	return unpackedErrors
}

func getIDsFromFile(filename string) []string {
	var ids []string

	// Open the file
	csvfile, err := os.Open(filename)
	if err != nil {
		log.Fatalln("Couldn't open the csv file", err)
	}

	// Parse the file
	r := csv.NewReader(csvfile)

	// Iterate through the records
	for {
		// Read each record from csv
		record, err := r.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal(err)
		}

		ids = append(ids, record[0])
	}

	return ids
}
