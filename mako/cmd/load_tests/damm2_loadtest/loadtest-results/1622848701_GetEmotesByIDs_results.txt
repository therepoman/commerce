
====== GetEmotesByIDs Results ======
Start Time: 2021-06-04 15:48:21.73298314 -0700 PDT m=+0.109278821 
End Time: 2021-06-04 16:18:21.628217131 -0700 PDT m=+1800.004512812 
Duration: 29m59.895233991s 
Requests: 18000 
Success Percentage: 1.000000 
Request Rate: 10.000582 
Successful Request Throughput: 9.999683 

Latency Average: 162.178314ms 
Latency p50: 161.610419ms 
Latency p90: 163.7707ms 
Latency p99: 180.511762ms 
Latency Max: 643.910776ms 

Status Codes: map[200:18000] 
Errors: []
Error Sets: map[]
