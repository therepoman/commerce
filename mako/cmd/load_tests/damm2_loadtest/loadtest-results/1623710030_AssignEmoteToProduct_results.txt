
====== AssignEmoteToProduct Results ======
Start Time: 2021-06-14 15:03:50.723514174 -0700 PDT m=+59.342356154 
End Time: 2021-06-14 15:33:50.623531967 -0700 PDT m=+1859.242373947 
Duration: 29m59.900017793s 
Requests: 18000 
Success Percentage: 0.999833 
Request Rate: 10.000555 
Successful Request Throughput: 9.997742 

Latency Average: 202.29141ms 
Latency p50: 202.990552ms 
Latency p90: 218.914617ms 
Latency p99: 285.796469ms 
Latency Max: 847.082847ms 

Status Codes: map[200:17997 412:1 500:2] 
Errors: [412 Precondition Failed 500 Internal Server Error]
Error Sets: map[:1]
