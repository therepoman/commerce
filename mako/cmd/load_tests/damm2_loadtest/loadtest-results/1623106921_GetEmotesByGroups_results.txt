
====== GetEmotesByGroups Results ======
Start Time: 2021-06-07 15:32:01.712822513 -0700 PDT m=+0.026315257 
End Time: 2021-06-07 16:02:01.691317241 -0700 PDT m=+1800.004809985 
Duration: 29m59.978494728s 
Requests: 90000 
Success Percentage: 0.999967 
Request Rate: 50.000597 
Successful Request Throughput: 49.993824 

Latency Average: 176.151967ms 
Latency p50: 174.759227ms 
Latency p90: 185.690379ms 
Latency p99: 217.611411ms 
Latency Max: 932.541863ms 

Status Codes: map[200:89997 500:3] 
Errors: [500 Internal Server Error]
Error Sets: map[:3]
