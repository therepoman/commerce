package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"math/rand"
	"net/http"
	"os"
	"path"
	"time"

	"code.justin.tv/commerce/gogogadget/random"
	log "code.justin.tv/commerce/logrus"
	mako "code.justin.tv/commerce/mako/twirp"
	mako_dashboard "code.justin.tv/commerce/mako/twirp/dashboard"
	"github.com/golang/protobuf/ptypes"
	vegeta "github.com/tsenart/vegeta/lib"
)

const (
	makoEndpoint = "https://main.us-west-2.prod.mako.twitch.a2z.com"
	outDir       = "loadtest-results"
	testDuration = time.Minute * 30
	userID       = "158418628" // qa_affiliate_forstycup
	//userID        = "267893713"      // qa_xanderwolf_partner
	prefix = "qaaffi"
	//prefix        = "wolfpack"
)

type groupIDEmoteLimit struct {
	groupID    string
	emoteLimit int
	emoteIDs   []string
}

var groupIDs = []groupIDEmoteLimit{
	{
		groupID:    "damm1loadtest0",
		emoteLimit: 10,
	},
	{
		groupID:    "damm1loadtest1",
		emoteLimit: 20,
	},
	{
		groupID:    "damm1loadtest2",
		emoteLimit: 30,
	},
	{
		groupID:    "damm1loadtest3",
		emoteLimit: 40,
	},
	{
		groupID:    "damm1loadtest4",
		emoteLimit: 50,
	},
	{
		groupID:    "damm1loadtest5",
		emoteLimit: 60,
	},
}

// stuff we only want to init once
var (
	makoClient          mako.Mako
	makoDashboardClient mako_dashboard.Mako
)

func init() {
	rand.Seed(time.Now().UTC().UnixNano())

	makoClient = mako.NewMakoProtobufClient(makoEndpoint, http.DefaultClient)
	makoDashboardClient = mako_dashboard.NewMakoProtobufClient(makoEndpoint, http.DefaultClient)
}

func main() {
	//loadTestCreateEmote()
	loadTestUpdateEmoteOrders()
}

func loadTestCreateEmote() {
	log.Info("Cleaning up emotes before test")
	err := deleteAllEmotesFromAllGroups()
	if err != nil {
		log.WithError(err).Fatal("[CreateEmote] Failed to delete all emotes from all groups before test")
	}

	attack(getCreateEmoteTest(), vegeta.Rate{
		Freq: 10,
		Per:  time.Second,
	}, testDuration, "CreateEmote")

	log.Info("Cleaning up emotes after test")
	err = deleteAllEmotesFromAllGroups()
	if err != nil {
		log.WithError(err).Fatal("[CreateEmote] Failed to delete all emotes from all groups after test")
	}
}

func loadTestUpdateEmoteOrders() {
	log.Info("Cleaning up emotes before test")
	err := deleteAllEmotesFromAllGroups()
	if err != nil {
		log.WithError(err).Fatal("[UpdateEmoteOrders] Failed to delete all emotes from all groups before test")
	}

	log.Info("Creating emotes before test")
	err = createAllEmotesInAllGroups()
	if err != nil {
		log.WithError(err).Fatal("[UpdateEmoteOrders] Failed to create all emotes in all groups before test")
	}

	// Ramp up from 20 TPS to 30 TPS over 30 minutes
	attack(getUpdateEmoteOrdersTest(), vegeta.LinearPacer{
		StartAt: vegeta.Rate{
			Freq: 20,
			Per:  time.Second,
		},
		Slope: 0.00555,
	}, testDuration, "UpdateEmoteOrders")

	// Sustain 30 TPS for 30 minutes
	attack(getUpdateEmoteOrdersTest(), vegeta.Rate{
		Freq: 30,
		Per:  time.Second,
	}, testDuration, "UpdateEmoteOrders")

	log.Info("Cleaning up emotes after test")
	err = deleteAllEmotesFromAllGroups()
	if err != nil {
		log.WithError(err).Fatal("[UpdateEmoteOrders] Failed to delete all emotes from all groups after test")
	}
}

func getCreateEmoteTest() vegeta.Targeter {
	headers := make(http.Header)
	headers.Add("Content-Type", "application/json")
	url := makoEndpoint + "/twirp/code.justin.tv.commerce.mako.dashboard.Mako/CreateEmote"

	return func(t *vegeta.Target) error {
		groupIDLimit := getGroupIDAndLimit()

		// Make sure the group can accommodate more emotes first
		err := deleteOldestEmoteFromGroupIfFull(groupIDLimit)
		if err != nil {
			log.WithError(err).Error("[CreateEmote] Failed to delete any emotes from group during test")
		}

		// Build request
		req := getCreateEmoteRequest(groupIDLimit.groupID)

		reqBytes, err := json.Marshal(req)
		if err != nil {
			log.WithError(err).Error("Failed to marshal CreateEmote request")
			return err
		}

		t.Method = http.MethodPost
		t.URL = url
		t.Header = headers
		t.Body = reqBytes

		return err
	}
}

func getGroupIDAndLimit() groupIDEmoteLimit {
	index := random.Int(0, len(groupIDs)-1)
	return groupIDs[index]
}

func getUpdateEmoteOrdersTest() vegeta.Targeter {
	headers := make(http.Header)
	headers.Add("Content-Type", "application/json")
	url := makoEndpoint + "/twirp/code.justin.tv.commerce.mako.dashboard.Mako/UpdateEmoteOrders"

	return func(t *vegeta.Target) (err error) {
		groupIDLimit := getGroupIDAndLimit()

		// Copy the created emote IDs and shuffle them
		emoteIDs := make([]string, len(groupIDLimit.emoteIDs))
		copy(emoteIDs, groupIDLimit.emoteIDs)
		rand.Shuffle(len(emoteIDs), func(i, j int) {
			emoteIDs[i], emoteIDs[j] = emoteIDs[j], emoteIDs[i]
		})

		// Build request
		var emoteOrders []*mako_dashboard.EmoteOrder
		for i, emoteID := range emoteIDs {
			emoteOrders = append(emoteOrders, &mako_dashboard.EmoteOrder{
				EmoteId: emoteID,
				GroupId: groupIDLimit.groupID,
				Order:   int32(i),
			})
		}
		req := &mako_dashboard.UpdateEmoteOrdersRequest{
			RequestingUserId: userID,
			EmoteOrders:      emoteOrders,
		}

		reqBytes, err := json.Marshal(req)
		if err != nil {
			log.WithError(err).Error("Failed to marshal UpdateEmoteOrders request")
			return err
		}

		t.Method = http.MethodPost
		t.URL = url
		t.Header = headers
		t.Body = reqBytes

		return err
	}
}

func createAllEmotesInAllGroups() error {
	for i, groupIDLimit := range groupIDs {
		emoteIDs, err := createAllEmotesInGroup(groupIDLimit)
		if err != nil {
			log.WithError(err).Error("Failed create all emotes for all groups")
			return err
		}
		groupIDs[i].emoteIDs = append(groupIDs[i].emoteIDs, emoteIDs...)
	}

	return nil
}

func createAllEmotesInGroup(groupIDLimit groupIDEmoteLimit) ([]string, error) {
	var emoteIDs []string
	for i := 0; i < groupIDLimit.emoteLimit; i++ {
		req := getCreateEmoteRequest(groupIDLimit.groupID)

		resp, err := makoDashboardClient.CreateEmote(context.Background(), req)
		if err != nil {
			log.WithError(err).Error("Failed create all emotes for group for test")
			return nil, err
		}

		emoteIDs = append(emoteIDs, resp.Id)
	}

	return emoteIDs, nil
}

func getCreateEmoteRequest(groupID string) *mako_dashboard.CreateEmoteRequest {
	suffix := fmt.Sprintf("LT%d", random.Int(0, 1000000))
	return &mako_dashboard.CreateEmoteRequest{
		UserId:     userID,
		Code:       prefix + suffix,
		CodeSuffix: suffix,
		GroupIds:   []string{groupID},
		ImageAssets: []*mako_dashboard.EmoteImageAsset{
			{
				ImageIds: &mako_dashboard.EmoteImageIds{
					Image1XId: "267893713_1x_525b24fa-27b3-4002-8753-fb4b82578a71",
					Image2XId: "267893713_2x_525b24fa-27b3-4002-8753-fb4b82578a71",
					Image4XId: "267893713_4x_525b24fa-27b3-4002-8753-fb4b82578a71",
				},
				AssetType: mako_dashboard.EmoteAssetType_static,
			},
		},
		AssetType:                 mako_dashboard.EmoteAssetType_static,
		Origin:                    mako_dashboard.EmoteOrigin_BitsBadgeTierEmotes,
		State:                     mako_dashboard.EmoteState_active,
		EnforceModerationWorkflow: true,
	}
}

func deleteOldestEmoteFromGroupIfFull(groupIDLimit groupIDEmoteLimit) error {
	emotesByGroupsResp, err := makoDashboardClient.GetEmotesByGroups(context.Background(), &mako_dashboard.GetEmotesByGroupsRequest{
		EmoteGroupIds: []string{groupIDLimit.groupID},
		Filter: &mako_dashboard.EmoteFilter{
			States: []mako_dashboard.EmoteState{mako_dashboard.EmoteState_active, mako_dashboard.EmoteState_pending},
		},
	})
	if err != nil {
		log.WithError(err).Error("Failed to get emotes by group")
		return err
	}

	for _, group := range emotesByGroupsResp.Groups {
		if len(group.Emotes) >= groupIDLimit.emoteLimit {
			var oldestEmote *mako_dashboard.Emote
			for i, emote := range group.Emotes {
				if i == 0 {
					oldestEmote = emote
					continue
				}

				emoteCreatedAt, _ := ptypes.Timestamp(emote.CreatedAt)
				oldestEmoteCreatedAt, _ := ptypes.Timestamp(oldestEmote.CreatedAt)
				if emoteCreatedAt.Before(oldestEmoteCreatedAt) {
					oldestEmote = emote
				}
			}

			_, err := makoClient.DeleteEmoticon(context.Background(), &mako.DeleteEmoticonRequest{
				Id:                    oldestEmote.Id,
				DeleteImages:          false,
				RequiresPermittedUser: true,
				UserId:                userID,
			})
			if err != nil {
				log.WithError(err).Error("Failed to delete any emote")
				return err
			}
		}
	}

	return nil
}

func deleteAllEmotesFromAllGroups() error {
	for _, groupIDLimit := range groupIDs {
		err := deleteAllEmotesFromGroup(groupIDLimit.groupID)
		if err != nil {
			log.WithError(err).Error("Failed delete all emotes for all groups")
			return err
		}
	}

	return nil
}

func deleteAllEmotesFromGroup(groupID string) error {
	emotesByGroupsResp, err := makoDashboardClient.GetEmotesByGroups(context.Background(), &mako_dashboard.GetEmotesByGroupsRequest{
		EmoteGroupIds: []string{groupID},
		Filter: &mako_dashboard.EmoteFilter{
			States: []mako_dashboard.EmoteState{mako_dashboard.EmoteState_active, mako_dashboard.EmoteState_pending},
		},
	})
	if err != nil {
		log.WithError(err).Error("Failed to get emotes by group")
		return err
	}

	for _, group := range emotesByGroupsResp.Groups {
		for _, emote := range group.Emotes {
			_, err := makoClient.DeleteEmoticon(context.Background(), &mako.DeleteEmoticonRequest{
				Id:                    emote.Id,
				DeleteImages:          false,
				RequiresPermittedUser: true,
				UserId:                userID,
			})
			if err != nil {
				log.WithError(err).Error("Failed to delete group emotes")
				return err
			}
		}
	}

	return nil
}

func printMetrics(title string, metrics vegeta.Metrics, errors map[string]int) {
	dir, err := os.Getwd()

	if err != nil {
		panic(err)
	}

	dest := path.Join(dir, outDir, fmt.Sprintf("%d_%s_results.txt", time.Now().Unix(), title))

	writers := make([]io.Writer, 0)
	writers = append(writers, os.Stdout)

	os.MkdirAll(path.Join(dir, outDir), os.ModePerm)
	file, err := os.Create(dest)
	if err != nil {
		log.WithError(err).WithField("dest", dest).Info("Error creating results file")
	} else {
		writers = append(writers, file)
	}

	w := io.MultiWriter(writers...)

	_, _ = fmt.Fprintf(w, "\n====== %s Results ======\n", title)

	_, _ = fmt.Fprintf(w, "Start Time: %s \n", metrics.Earliest.String())
	_, _ = fmt.Fprintf(w, "End Time: %s \n", metrics.Latest.String())
	_, _ = fmt.Fprintf(w, "Duration: %s \n", metrics.Duration.String())

	_, _ = fmt.Fprintf(w, "Requests: %d \n", metrics.Requests)
	_, _ = fmt.Fprintf(w, "Success Percentage: %f \n", metrics.Success)
	_, _ = fmt.Fprintf(w, "Request Rate: %f \n", metrics.Rate)
	_, _ = fmt.Fprintf(w, "Successful Request Throughput: %f \n\n", metrics.Throughput)

	_, _ = fmt.Fprintf(w, "Latency Average: %s \n", metrics.Latencies.Mean)
	_, _ = fmt.Fprintf(w, "Latency p50: %s \n", metrics.Latencies.P50)
	_, _ = fmt.Fprintf(w, "Latency p90: %s \n", metrics.Latencies.P90)
	_, _ = fmt.Fprintf(w, "Latency p99: %s \n", metrics.Latencies.P99)
	_, _ = fmt.Fprintf(w, "Latency Max: %s \n\n", metrics.Latencies.Max)

	_, _ = fmt.Fprintf(w, "Status Codes: %v \n", metrics.StatusCodes)
	_, _ = fmt.Fprintf(w, "Errors: %v\n", metrics.Errors)
	_, _ = fmt.Fprintf(w, "Error Sets: %v\n", errors)
}

func attack(targeter vegeta.Targeter, pacer vegeta.Pacer, dur time.Duration, name string) {
	log.Info("Starting attack on " + name)
	attacker := vegeta.NewAttacker()
	var metrics vegeta.Metrics
	errors := map[string]int{}
	for res := range attacker.Attack(targeter, pacer, dur, name+"-Attack") {
		metrics.Add(res)
		if res.Code != 200 {
			body := string(res.Body)
			errors[body] = errors[body] + 1
			log.Error(res.Error)
		}
	}
	metrics.Close()
	printMetrics(name, metrics, unpackErrors(errors))
}

func unpackErrors(errors map[string]int) map[string]int {
	type MetricError struct {
		Message string `json:"message"`
	}

	unpackedErrors := map[string]int{}
	for err, count := range errors {
		var metricError MetricError
		json.Unmarshal([]byte(err), &metricError)
		unpackedErrors[metricError.Message] = count
	}

	return unpackedErrors
}
