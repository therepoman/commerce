
====== UpdateEmoteOrders Results ======
Start Time: 2021-01-13 15:59:59.892354129 -0800 PST m=+137.567520375 
End Time: 2021-01-13 16:29:59.893638864 -0800 PST m=+1937.568805110 
Duration: 30m0.001284735s 
Requests: 44991 
Success Percentage: 1.000000 
Request Rate: 24.994982 
Successful Request Throughput: 24.994581 

Latency Average: 49.502218ms 
Latency p50: 43.464136ms 
Latency p90: 69.770841ms 
Latency p99: 263.710124ms 
Latency Max: 1.657487806s 

Status Codes: map[200:44991] 
Errors: []
Error Sets: map[]
