
====== UpdateEmoteOrders Results ======
Start Time: 2021-01-13 16:29:59.912943006 -0800 PST m=+1937.639922584 
End Time: 2021-01-13 16:59:59.876623176 -0800 PST m=+3737.603602754 
Duration: 29m59.96368017s 
Requests: 54000 
Success Percentage: 1.000000 
Request Rate: 30.000605 
Successful Request Throughput: 29.999995 

Latency Average: 43.184283ms 
Latency p50: 42.109585ms 
Latency p90: 58.068966ms 
Latency p99: 86.700764ms 
Latency Max: 691.828525ms 

Status Codes: map[200:54000] 
Errors: []
Error Sets: map[]
