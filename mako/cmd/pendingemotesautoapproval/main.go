package main

import (
	"context"
	"encoding/csv"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"

	"code.justin.tv/commerce/mako/clients/pendingemoticons"
	"code.justin.tv/commerce/mako/config"
	"code.justin.tv/commerce/mako/dynamo"
	mako_client "code.justin.tv/commerce/mako/twirp"
	"golang.org/x/time/rate"
)

const (
	makoStagingURL      = "https://main.us-west-2.beta.mako.twitch.a2z.com"
	makoProdURL         = "https://main.us-west-2.prod.mako.twitch.a2z.com"
	reviewBatchSize     = 5
	emoteImageURLPrefix = "https://static-cdn.jtvnw.net/emoticons/v1"
)

var (
	inputFile                     = flag.String("file", "", "Path to the csv file that contains the list of channels that are in good standing")
	live                          = flag.Bool("live", false, "Whether to actually execute the changes or default to a dry-run")
	env                           = flag.String("env", "staging", "Environment to run the script against (staging, prod). Defaults to staging")
	minDurationSinceEmoteCreation = flag.Duration("minduration", 0, "Minimum duration before now that an emote must have been created before in order to be approved. Defaults to 0. Check time.ParseDuration() docs for acceptable values.")
	shouldCreateTestPendingEmotes = flag.Bool("create", false, "Whether to populate the staging environment with test pending emotes")
)

func main() {
	flag.Parse()

	cfg, err := config.LoadConfigForEnvironment(*env)
	if err != nil {
		log.Fatal(err)
	}

	log.Printf("Running script in the %s environment\n", strings.ToUpper(cfg.EnvironmentPrefix))

	// Initialize DAO
	pendingEmotesDao, err := dynamo.NewPendingEmotesDao(cfg.AWSRegion, cfg.EnvironmentPrefix)
	if err != nil {
		log.Fatal(err)
	}

	// Initialize Mako client
	makoHost := makoStagingURL
	if *env == "prod" {
		makoHost = makoProdURL
	}
	makoClient := mako_client.NewMakoProtobufClient(makoHost, &http.Client{})

	if *env == "staging" && *shouldCreateTestPendingEmotes {
		// Only populate test emotes in staging
		populateTestPendingEmotes(makoClient)
		return
	}

	if *env == "prod" && *live {
		log.Println("!!!!Running auto-approval for real in prod. Giving you 10 seconds to rethink this decision!!!!")
		time.Sleep(10 * time.Second)
	}

	// Get all pending emoticons
	log.Println("Retrieving all pending emotes from DynamoDB")

	pendingEmotes, err := pendingEmotesDao.GetAllPendingEmoticons(context.Background())
	if err != nil {
		log.Fatal(err)
	}

	log.Printf("Successfully retrieved all %d pending emotes from DynamoDB\n", len(pendingEmotes))

	// Backup pending emoticons
	writePendingEmotesBackup(pendingEmotes)

	// Get channels in good standing map
	log.Println("Getting channels in good standing from input file")

	channelsInGoodStanding := getChannelInGoodStandingMap()

	log.Printf("Successfully got %d channels in good standing from input file\n", len(channelsInGoodStanding))

	// Determine emotes to approve
	var emotesToApprove []*dynamo.PendingEmote
	badWordsCount := 0
	for _, emote := range pendingEmotes {
		isGoodStandingRequirementMet := channelsInGoodStanding[emote.OwnerId]
		isTimeRequirementMet := time.Since(emote.CreatedAt) > *minDurationSinceEmoteCreation
		isBadWordsRequirementMet := !badWords[strings.ToLower(emote.Code)]
		isReviewStateRequirementMet := emote.ReviewState != dynamo.PendingEmoteReviewStateDeferred

		if isGoodStandingRequirementMet && isTimeRequirementMet && isReviewStateRequirementMet {
			if !isBadWordsRequirementMet {
				badWordsCount++
			} else {
				emotesToApprove = append(emotesToApprove, emote)
			}
		}
	}

	log.Printf("Found %d emotes owned by good standing channels with bad words\n", badWordsCount)
	log.Printf("Found %d emotes to be approved\n", len(emotesToApprove))

	// Record the emotes that are about to be approved
	writeEmotesToApproveToFile(emotesToApprove)

	if *live && len(emotesToApprove) > 0 {
		var reviews []*mako_client.ReviewPendingEmoticonRequest
		for _, emote := range emotesToApprove {
			reviews = append(reviews, &mako_client.ReviewPendingEmoticonRequest{
				Id:      emote.EmoteId,
				Approve: true,
			})
		}

		var results []*mako_client.ReviewPendingEmoticonResponse

		log.Printf("Calling Mako ReviewPendingEmoticons in batches with %d emote approval reviews\n", len(reviews))

		// Process reviews in batches
		for i := 0; i < len(reviews); i += reviewBatchSize {
			j := i + reviewBatchSize
			if j > len(reviews) {
				j = len(reviews)
			}

			req := &mako_client.ReviewPendingEmoticonsRequest{
				Reviews: reviews[i:j],
				AdminId: pendingemoticons.AutoApprovalAdminID,
			}

			res, err := makoClient.ReviewPendingEmoticons(context.Background(), req)
			if err != nil {
				log.Printf("Failed to call Mako ReviewPendingEmoticons to approve pending emotes for batch %d to %d\n", i+1, j)

				// Write results that succeeded before this failure
				writeResultsToFile(results)

				log.Fatal(err)
			}

			results = append(results, res.Results...)

			// Rate limit to under ~10 TPS
			time.Sleep(100 * time.Millisecond)
		}

		numFailedReviews := 0
		for _, result := range results {
			if !result.Succeeded {
				numFailedReviews++
			}
		}

		if numFailedReviews > 0 {
			log.Printf("%d emote reviews failed. See the results file for more details.", numFailedReviews)
		}

		writeResultsToFile(results)
	} else {
		log.Println("!!!This was a dry run. No emotes were actually approved!!!")
	}

	log.Println("DONE!")
}

func getChannelInGoodStandingMap() map[string]bool {
	csvFile, err := os.Open(*inputFile)
	if err != nil {
		log.Fatal(err)
	}

	reader := csv.NewReader(csvFile)

	channelMap := map[string]bool{}

	for {
		line, err := reader.Read()
		if err == io.EOF {
			break
		}

		if strings.Contains(line[0], "channel_id") {
			// Header line
			continue
		}

		channelID := line[0]

		channelMap[channelID] = true
	}

	return channelMap
}

func writePendingEmotesBackup(pendingEmotes []*dynamo.PendingEmote) {
	bytes, err := json.Marshal(pendingEmotes)
	if err != nil {
		log.Fatal(err)
	}

	path, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}

	fileName := fmt.Sprintf("pending_emotes_backup_%s", time.Now().Format(time.RFC3339))

	log.Printf("Writing full pending_emotes backup to %s/%s\n", path, fileName)

	err = ioutil.WriteFile(fileName, bytes, 0666)
	if err != nil {
		log.Fatal(err)
	}
}

func writeEmotesToApproveToFile(pendingEmotes []*dynamo.PendingEmote) {
	csvFile, err := os.Create(fmt.Sprintf("emotes_to_approve_%s.csv", time.Now().Format(time.RFC3339)))
	if err != nil {
		log.Fatal(err)
	}

	path, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}

	writer := csv.NewWriter(csvFile)

	log.Printf("Writing emotes to approve to file %s/%s\n", path, csvFile.Name())

	writer.Write([]string{"channel_id", "emote_id", "emote_code", "emote_regex", "emote_image_URL_28", "emote_image_URL_56", "emote_image_URL_112"})

	for _, emote := range pendingEmotes {
		writer.Write([]string{emote.OwnerId, emote.EmoteId, emote.Code, fmt.Sprintf("%s%s", emote.Prefix, emote.Code), fmt.Sprintf("%s/%s/1.0", emoteImageURLPrefix, emote.EmoteId), fmt.Sprintf("%s/%s/2.0", emoteImageURLPrefix, emote.EmoteId), fmt.Sprintf("%s/%s/3.0", emoteImageURLPrefix, emote.EmoteId)})
	}

	writer.Flush()

	csvFile.Close()
}

func writeResultsToFile(results []*mako_client.ReviewPendingEmoticonResponse) {
	csvFile, err := os.Create(fmt.Sprintf("results_%s.csv", time.Now().Format(time.RFC3339)))
	if err != nil {
		log.Fatal(err)
	}

	path, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}

	writer := csv.NewWriter(csvFile)

	log.Printf("Writing results to file %s/%s\n", path, csvFile.Name())

	writer.Write([]string{"emote_id", "new_status", "succeeded"})

	for _, result := range results {
		writer.Write([]string{result.Id, result.NewStatus, strconv.FormatBool(result.Succeeded)})
	}

	writer.Flush()

	csvFile.Close()
}

func populateTestPendingEmotes(makoClient mako_client.Mako) {
	limiter := rate.NewLimiter(10, 1)
	var wg sync.WaitGroup
	for i := 1270; i < 2000; i++ {
		go func(index int) {
			wg.Add(1)

			req := &mako_client.CreateEmoticonRequest{
				UserId:                    "267893713",
				Code:                      fmt.Sprintf("wolfBob%d", index),
				CodeSuffix:                fmt.Sprintf("Bob%d", index),
				State:                     "active",
				GroupId:                   "1234567890",
				Image28Id:                 "267893713_28_11c4beba-04bd-405e-99d5-99cdc7fbd5a1",
				Image56Id:                 "267893713_56_b293de93-8e0d-451e-9ba1-ac9a5a1a2c08",
				Image112Id:                "267893713_112_1aaf6c77-d9e9-4ff2-a335-abd48856ed03",
				Domain:                    "",
				EmoteGroup:                0,
				EnforceModerationWorkflow: true,
			}

			limiter.Wait(context.Background())

			_, err := makoClient.CreateEmoticon(context.Background(), req)
			if err != nil {
				log.Printf("Failed to create pending emoticon %d", index)
			}

			wg.Done()
		}(i)
	}

	wg.Wait()
	log.Println("Finished populating test pending emoticons!")
}

var badWords = map[string]bool{
	"f":             true,
	"gasm":          true,
	"booty":         true,
	"simp":          true,
	"nut":           true,
	"lewd":          true,
	"butt":          true,
	"thicc":         true,
	"fu":            true,
	"ohshit":        true,
	"1":             true,
	"cheeks":        true,
	"clappincheeks": true,
	"dank":          true,
	"420":           true,
	"e":             true,
	"loser":         true,
	"choke":         true,
	"poop":          true,
	"corona":        true,
	"ass":           true,
	"finger":        true,
	"shit":          true,
	"thiccc":        true,
	"cykablyat":     true,
	"kkk":           true,
	"nazi":          true,
	"hitler":        true,
	"suicide":       true,
	"hentai":        true,
	"ahegao":        true,
	"gun":           true,
	"retard":        true,
	"tard":          true,
	"fap":           true,
	"stalin":        true,
	"boob":          true,
	"fuck":          true,
	"bigot":         true,
	"bylat":         true,
	"bitch":         true,
	"assclap":       true,
	"penis":         true,
	"vagina":        true,
	"cunt":          true,
	"whore":         true,
	"nigga":         true,
	"slut":          true,
	"fag":           true,
	"faggot":        true,
	"nigger":        true,
	"asshole":       true,
	"motherfucker":  true,
	"arse":          true,
	"holocaust":     true,
	"putain":        true,
	"merde":         true,
	"khara":         true,
}
