package prefixes

import (
	"context"
	"errors"
	"strconv"
	"testing"

	"code.justin.tv/commerce/mako/clients"
	"code.justin.tv/commerce/mako/dynamo"
	dynamo_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/dynamo"
	clients_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/web/users-service/client/usersclient_internal"
	stats_mock "code.justin.tv/commerce/mako/mocks/github.com/cactus/go-statsd-client/statsd"
	"code.justin.tv/web/users-service/models"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestPrefixRecommendation_GetRecommendedEmoticonPrefix(t *testing.T) {
	ctx := context.Background()

	animeBearId := "an_anime_bear_id"
	animeBearLogin := "AN_ANIME_BEAR"

	Convey("A prefix recommender", t, func() {
		mockPrefixDao := new(dynamo_mock.IPrefixDao)
		mockUserService := new(clients_mock.InternalClient)
		mockStats := new(stats_mock.Statter)
		recommender := NewPrefixRecommender(mockPrefixDao, mockStats, &clients.Clients{
			UserServiceClient: mockUserService,
		})

		Convey("When user service returns an error", func() {
			mockUserService.On("GetUserByID", ctx, animeBearId, mock.Anything).Return(nil, errors.New("user service is unavailable"))

			recommendation, err := recommender.GetRecommendedEmoticonPrefix(ctx, animeBearId)
			So(err, ShouldNotBeNil)
			So(recommendation, ShouldEqual, "")
		})

		Convey("When user service returns a nil login", func() {
			mockUserService.On("GetUserByID", ctx, animeBearId, mock.Anything).Return(&models.Properties{}, nil)

			recommendation, err := recommender.GetRecommendedEmoticonPrefix(ctx, animeBearId)
			So(err, ShouldNotBeNil)
			So(recommendation, ShouldEqual, "")
		})

		Convey("When user service returns AN_ANIME_BEAR", func() {
			mockUserService.On("GetUserByID", ctx, animeBearId, mock.Anything).Return(&models.Properties{
				Login: &animeBearLogin,
			}, nil)

			Convey("when the DAO doesn't have a match", func() {
				mockPrefixDao.On("Get", ctx, "ananim").Return(nil, nil)

				recommendation, err := recommender.GetRecommendedEmoticonPrefix(ctx, animeBearId)
				So(err, ShouldBeNil)
				So(recommendation, ShouldEqual, "ananim")
			})

			// we'll test prefix generation conflict below in a separate test.

			Convey("When the DAO does has a match, but the same owner", func() {
				mockPrefixDao.On("Get", ctx, "ananim").Return(&dynamo.Prefix{
					Prefix:  "ananim",
					OwnerId: animeBearId,
				}, nil)

				recommendation, err := recommender.GetRecommendedEmoticonPrefix(ctx, animeBearId)
				So(err, ShouldBeNil)
				So(recommendation, ShouldEqual, "ananim")
			})
		})

		Convey("When the user service returns all characters login", func() {
			jibberChannelId := "jibberish"
			jibberChannelLogin := "~!@#$%^&"

			mockUserService.On("GetUserByID", ctx, jibberChannelId, mock.Anything).Return(&models.Properties{
				Login: &jibberChannelLogin,
			}, nil)

			recommendation, err := recommender.GetRecommendedEmoticonPrefix(ctx, jibberChannelId)
			So(err, ShouldNotBeNil)
			So(recommendation, ShouldEqual, "")
		})

		Convey("When the user service returns single number login", func() {
			numbersOnlyChannelId := "numbas"
			numbersOnlyLogin := "1"

			mockUserService.On("GetUserByID", ctx, numbersOnlyChannelId, mock.Anything).Return(&models.Properties{
				Login: &numbersOnlyLogin,
			}, nil)

			Convey("When the DAO does not return a match", func() {
				mockPrefixDao.On("Get", ctx, "x11").Return(nil, nil)

				recommendation, err := recommender.GetRecommendedEmoticonPrefix(ctx, numbersOnlyChannelId)
				So(err, ShouldBeNil)
				So(recommendation, ShouldEqual, "x11")
			})
		})

		Convey("When the user service returns only two char [1st is a digit, 2nd is a char]", func() {
			numberAndSingleLetterChannelId := "numba-and-letta"
			numberAndSingleLetter := "1C"

			mockUserService.On("GetUserByID", ctx, numberAndSingleLetterChannelId, mock.Anything).Return(&models.Properties{
				Login: &numberAndSingleLetter,
			}, nil)

			Convey("When the DAO does not return a match", func() {
				mockPrefixDao.On("Get", ctx, "x1c").Return(nil, nil)

				recommendation, err := recommender.GetRecommendedEmoticonPrefix(ctx, numberAndSingleLetterChannelId)
				So(err, ShouldBeNil)
				So(recommendation, ShouldEqual, "x1c")
			})
		})

		Convey("When the user service returns all numbers login", func() {
			allNumbersChannelId := "numbas-all-day"
			allNumbersLogin := "123456"

			mockUserService.On("GetUserByID", ctx, allNumbersChannelId, mock.Anything).Return(&models.Properties{
				Login: &allNumbersLogin,
			}, nil)

			Convey("When the DAO does not return a match", func() {
				mockPrefixDao.On("Get", ctx, "x12345").Return(nil, nil)

				recommendation, err := recommender.GetRecommendedEmoticonPrefix(ctx, allNumbersChannelId)
				So(err, ShouldBeNil)
				So(recommendation, ShouldEqual, "x12345")
			})
		})

		Convey("When the user service returns 2 letter characters", func() {
			shortLettersOnlyChannelId := "shorty-mconly-characters"
			shortLettersOnlyLogin := "yz"

			mockUserService.On("GetUserByID", ctx, shortLettersOnlyChannelId, mock.Anything).Return(&models.Properties{
				Login: &shortLettersOnlyLogin,
			}, nil)

			Convey("When the DAO does not return a match", func() {
				mockPrefixDao.On("Get", ctx, "yz1").Return(nil, nil)

				recommendation, err := recommender.GetRecommendedEmoticonPrefix(ctx, shortLettersOnlyChannelId)
				So(err, ShouldBeNil)
				So(recommendation, ShouldEqual, "yz1")
			})
		})
	})
}

func TestPrefixRecommendation_determineTargets(t *testing.T) {
	Convey("With no setup required", t, func() {
		Convey("with bears and initial range", func() {
			targetModifiers, targetKeys := determineTargets("bear", 1, 5000)

			So(targetKeys, ShouldHaveLength, 21)
			So(targetModifiers, ShouldHaveLength, 21)

			// keys should be the derieved from the modifiers
			So(targetModifiers, ShouldHaveLength, len(targetKeys))
			for i := 0; i < len(targetModifiers); i++ {
				So(targetKeys[i], ShouldEqual, "bear"+strconv.Itoa(targetModifiers[i]))
			}

			// spot check some values
			So(targetModifiers[0], ShouldEqual, 1)
			So(targetModifiers[3], ShouldEqual, 750)
			So(targetModifiers[10], ShouldEqual, 2500)
			So(targetModifiers[17], ShouldEqual, 4250)
			So(targetModifiers[20], ShouldEqual, 5000)
		})

		Convey("with bears and and a random range", func() {
			targetModifiers, targetKeys := determineTargets("bear", 467, 882)

			So(targetKeys, ShouldHaveLength, 21)
			So(targetModifiers, ShouldHaveLength, 21)

			So(targetModifiers, ShouldHaveLength, len(targetKeys))
			for i := 0; i < len(targetModifiers); i++ {
				So(targetKeys[i], ShouldEqual, "bear"+strconv.Itoa(targetModifiers[i]))
			}

			// spot check some values
			So(targetModifiers[0], ShouldEqual, 467)
			So(targetModifiers[3], ShouldEqual, 529)
			So(targetModifiers[10], ShouldEqual, 674)
			So(targetModifiers[17], ShouldEqual, 819)
			So(targetModifiers[20], ShouldEqual, 882)
		})

		Convey("with bears and and a tiny range", func() {
			targetModifiers, targetKeys := determineTargets("bear", 500, 505)

			So(targetKeys, ShouldHaveLength, 6)
			So(targetModifiers, ShouldHaveLength, 6)

			So(targetModifiers, ShouldHaveLength, len(targetKeys))
			for i := 0; i < len(targetModifiers); i++ {
				So(targetKeys[i], ShouldEqual, "bear"+strconv.Itoa(targetModifiers[i]))
			}

			// spot check some values
			So(targetModifiers[0], ShouldEqual, 500)
			So(targetModifiers[1], ShouldEqual, 501)
			So(targetModifiers[2], ShouldEqual, 502)
			So(targetModifiers[3], ShouldEqual, 503)
			So(targetModifiers[4], ShouldEqual, 504)
			So(targetModifiers[5], ShouldEqual, 505)
		})

		Convey("with bears and and an EVEN tinier range", func() {
			targetModifiers, targetKeys := determineTargets("bear", 503, 504)

			So(targetKeys, ShouldHaveLength, 2)
			So(targetModifiers, ShouldHaveLength, 2)

			So(targetModifiers[0], ShouldEqual, 503)
			So(targetModifiers[1], ShouldEqual, 504)
		})

		Convey("with bears and and has a range of 1", func() {
			targetModifiers, targetKeys := determineTargets("bear", 504, 504)

			So(targetKeys, ShouldHaveLength, 1)
			So(targetModifiers, ShouldHaveLength, 1)

			So(targetModifiers[0], ShouldEqual, 504)
		})

		// this is not a valid test case, but ironically it generates a spectrum of valid keys of this range still,
		// altough I guess it reveals that min and max are unintentionally reversible with my algorithm.
		Convey("with bears and and has a range of -1", func() {
			targetModifiers, targetKeys := determineTargets("bear", 503, 500)

			So(targetKeys, ShouldHaveLength, 4)
			So(targetModifiers, ShouldHaveLength, 4)
		})
	})
}

func TestPrefixRecommendation_attackCongestedPrefix(t *testing.T) {
	ctx := context.Background()

	animeBearId := "anime_bear_id"
	animeBearLogin := "ANIME_BEAR_WTF"

	Convey("A prefix recommender", t, func() {
		// we want to skip tot he part where we have an conflict
		mockPrefixDao := new(dynamo_mock.IPrefixDao)
		mockPrefixDao.On("Get", ctx, "animeb").Return(&dynamo.Prefix{
			Prefix:  "animeb",
			OwnerId: "anime_hater_459",
		}, nil)

		mockUserService := new(clients_mock.InternalClient)
		mockUserService.On("GetUserByID", ctx, animeBearId, mock.Anything).Return(&models.Properties{
			Login: &animeBearLogin,
		}, nil)

		mockStats := new(stats_mock.Statter)
		mockStats.On("Inc", prefixDangerMetric, int64(1), float32(1.0)).Return(nil)

		recommender := NewPrefixRecommender(mockPrefixDao, mockStats, &clients.Clients{
			UserServiceClient: mockUserService,
		})

		Convey("when GetBatch fails", func() {
			animeBearError := errors.New("anime bear overage, please reduce anime levels")
			mockPrefixDao.On("GetBatch", mockAnythingArrayWithContext(ctx, "animeb", 1, 5000)...).Return(nil, animeBearError)

			recommendedPrefix, err := recommender.GetRecommendedEmoticonPrefix(ctx, animeBearId)
			So(recommendedPrefix, ShouldBeEmpty)
			So(err, ShouldEqual, animeBearError)
		})

		Convey("when GetBatch returns a lot of nothing", func() {
			mockPrefixDao.On("GetBatch", mockAnythingArrayWithContext(ctx, "animeb", 1, 5000)...).Return(map[string]dynamo.Prefix{}, nil)

			recommendedPrefix, err := recommender.GetRecommendedEmoticonPrefix(ctx, animeBearId)
			So(err, ShouldBeNil)
			So(recommendedPrefix, ShouldEqual, "animeb1")
		})

		Convey("when GetBatch implies no matches in first 5k entries", func() {
			mockPrefixDao.On("GetBatch", mockAnythingArrayWithContext(ctx, "animeb", 1, 5000)...).Return(makePrefixPopulatedResponses("animeb", 1, 5000), nil)

			recommendedPrefix, err := recommender.GetRecommendedEmoticonPrefix(ctx, animeBearId)
			So(err, ShouldNotBeNil)
			So(err.Error(), ShouldEqual, "cannot find a prefix in first 5000 area")
			So(recommendedPrefix, ShouldBeEmpty)
		})

		Convey("when GetBatch will only return a value for 1", func() {
			responses := map[string]dynamo.Prefix{
				"animeb1": dynamo.Prefix{},
			}

			mockPrefixDao.On("GetBatch", mockAnythingArrayWithContext(ctx, "animeb", 1, 5000)...).Return(responses, nil)
			mockPrefixDao.On("GetBatch", mockAnythingArrayWithContext(ctx, "animeb", 2, 250)...).Return(map[string]dynamo.Prefix{}, nil)

			recommendedPrefix, err := recommender.GetRecommendedEmoticonPrefix(ctx, animeBearId)
			So(err, ShouldBeNil)
			So(recommendedPrefix, ShouldEqual, "animeb2")
			mockStats.AssertNotCalled(t, "Inc", prefixDangerMetric, int64(1), float32(1.0))
		})

		Convey("when GetBatch will only returns values for upto 25", func() {
			// so we're going to build this set once and abuse that we don't check for unexpected values in this response to let use reuse it.
			responses := map[string]dynamo.Prefix{}
			for i := 1; i <= 25; i++ {
				responses["animeb"+strconv.Itoa(i)] = dynamo.Prefix{}
			}

			mockPrefixDao.On("GetBatch", mockAnythingArrayWithContext(ctx, "animeb", 1, 5000)...).Return(responses, nil)
			mockPrefixDao.On("GetBatch", mockAnythingArrayWithContext(ctx, "animeb", 2, 250)...).Return(responses, nil)
			mockPrefixDao.On("GetBatch", mockAnythingArrayWithContext(ctx, "animeb", 15, 26)...).Return(responses, nil)

			recommendedPrefix, err := recommender.GetRecommendedEmoticonPrefix(ctx, animeBearId)
			So(err, ShouldBeNil)
			So(recommendedPrefix, ShouldEqual, "animeb26")
			mockStats.AssertNotCalled(t, "Inc", prefixDangerMetric, int64(1), float32(1.0))
		})

		Convey("when GetBatch will only returns values for upto 29", func() {
			// so we're going to build this set once and abuse that we don't check for unexpected values in this response to let use reuse it.
			responses := map[string]dynamo.Prefix{}
			for i := 1; i <= 29; i++ {
				responses["animeb"+strconv.Itoa(i)] = dynamo.Prefix{}
			}

			mockPrefixDao.On("GetBatch", mockAnythingArrayWithContext(ctx, "animeb", 1, 5000)...).Return(responses, nil)
			mockPrefixDao.On("GetBatch", mockAnythingArrayWithContext(ctx, "animeb", 2, 250)...).Return(responses, nil)
			mockPrefixDao.On("GetBatch", mockAnythingArrayWithContext(ctx, "animeb", 27, 39)...).Return(responses, nil)

			recommendedPrefix, err := recommender.GetRecommendedEmoticonPrefix(ctx, animeBearId)
			So(err, ShouldBeNil)
			So(recommendedPrefix, ShouldEqual, "animeb30")
			mockStats.AssertNotCalled(t, "Inc", prefixDangerMetric, int64(1), float32(1.0))
		})

		Convey("when GetBatch will only returns values for upto 420", func() {
			// so we're going to build this set once and abuse that we don't check for unexpected values in this response to let use reuse it.
			responses := map[string]dynamo.Prefix{}
			for i := 1; i <= 420; i++ {
				responses["animeb"+strconv.Itoa(i)] = dynamo.Prefix{}
			}

			mockPrefixDao.On("GetBatch", mockAnythingArrayWithContext(ctx, "animeb", 1, 5000)...).Return(responses, nil)
			mockPrefixDao.On("GetBatch", mockAnythingArrayWithContext(ctx, "animeb", 251, 500)...).Return(responses, nil)
			mockPrefixDao.On("GetBatch", mockAnythingArrayWithContext(ctx, "animeb", 413, 425)...).Return(responses, nil)

			recommendedPrefix, err := recommender.GetRecommendedEmoticonPrefix(ctx, animeBearId)
			So(err, ShouldBeNil)
			So(recommendedPrefix, ShouldEqual, "animeb421")
			mockStats.AssertNotCalled(t, "Inc", prefixDangerMetric, int64(1), float32(1.0))
		})

		Convey("when GetBatch will only returns values for upto 4499", func() {
			// so we're going to build this set once and abuse that we don't check for unexpected values in this response to let use reuse it.
			responses := map[string]dynamo.Prefix{}
			for i := 1; i <= 4499; i++ {
				responses["animeb"+strconv.Itoa(i)] = dynamo.Prefix{}
			}

			mockPrefixDao.On("GetBatch", mockAnythingArrayWithContext(ctx, "animeb", 1, 5000)...).Return(responses, nil)
			mockPrefixDao.On("GetBatch", mockAnythingArrayWithContext(ctx, "animeb", 4251, 4500)...).Return(responses, nil)
			mockPrefixDao.On("GetBatch", mockAnythingArrayWithContext(ctx, "animeb", 4488, 4500)...).Return(responses, nil)

			recommendedPrefix, err := recommender.GetRecommendedEmoticonPrefix(ctx, animeBearId)
			So(err, ShouldBeNil)
			So(recommendedPrefix, ShouldEqual, "animeb4500")
			mockStats.AssertNotCalled(t, "Inc", prefixDangerMetric, int64(1), float32(1.0))
		})

		Convey("when GetBatch will only returns values for upto 4500", func() {
			// so we're going to build this set once and abuse that we don't check for unexpected values in this response to let use reuse it.
			responses := map[string]dynamo.Prefix{}
			for i := 1; i <= 4500; i++ {
				responses["animeb"+strconv.Itoa(i)] = dynamo.Prefix{}
			}

			mockPrefixDao.On("GetBatch", mockAnythingArrayWithContext(ctx, "animeb", 1, 5000)...).Return(responses, nil)
			mockPrefixDao.On("GetBatch", mockAnythingArrayWithContext(ctx, "animeb", 4501, 4750)...).Return(responses, nil)

			recommendedPrefix, err := recommender.GetRecommendedEmoticonPrefix(ctx, animeBearId)
			So(err, ShouldBeNil)
			So(recommendedPrefix, ShouldEqual, "animeb4501")
			mockStats.AssertCalled(t, "Inc", prefixDangerMetric, int64(1), float32(1.0))
		})

		Convey("when GetBatch will only returns values for upto 4580", func() {
			// so we're going to build this set once and abuse that we don't check for unexpected values in this response to let use reuse it.
			responses := map[string]dynamo.Prefix{}
			for i := 1; i <= 4580; i++ {
				responses["animeb"+strconv.Itoa(i)] = dynamo.Prefix{}
			}

			mockPrefixDao.On("GetBatch", mockAnythingArrayWithContext(ctx, "animeb", 1, 5000)...).Return(responses, nil)
			mockPrefixDao.On("GetBatch", mockAnythingArrayWithContext(ctx, "animeb", 4501, 4750)...).Return(responses, nil)
			mockPrefixDao.On("GetBatch", mockAnythingArrayWithContext(ctx, "animeb", 4576, 4588)...).Return(responses, nil)

			recommendedPrefix, err := recommender.GetRecommendedEmoticonPrefix(ctx, animeBearId)
			So(err, ShouldBeNil)
			So(recommendedPrefix, ShouldEqual, "animeb4581")
			mockStats.AssertCalled(t, "Inc", prefixDangerMetric, int64(1), float32(1.0))
		})
	})
}

func mockAnythingArrayWithContext(ctx context.Context, base string, min, max int) []interface{} {
	_, keys := determineTargets(base, min, max)

	mocks := make([]interface{}, len(keys)+1)
	mocks[0] = ctx
	for i := 1; i < len(keys)+1; i++ {
		mocks[i] = keys[i-1]
	}
	return mocks
}

func makePrefixPopulatedResponses(base string, min, max int) map[string]dynamo.Prefix {
	_, keys := determineTargets(base, min, max)

	mocks := make(map[string]dynamo.Prefix, len(keys))
	for i := 0; i < len(keys); i++ {
		key := keys[i]
		mocks[key] = dynamo.Prefix{}
	}
	return mocks
}
