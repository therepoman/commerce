package prefixes

import (
	"context"
	"fmt"
	"regexp"
	"strconv"
	"strings"
	"unicode"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/mako/clients"
	"code.justin.tv/commerce/mako/dynamo"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/pkg/errors"
)

const (
	// This logic SHOULD only take 3 dynamo calls according to my math, I'm going to let there be some buffer room and cap iterations at 5.
	maxAttackIterations  = 5
	numberOfAttackSlices = 20
	prefixDangerMetric   = "PREFIX_DANGER_ZONE"

	// Currently we're around 1800 for there(al)'s. (2020-08-03)
	maxPrefixRange = 5000
)

type PrefixRecommender interface {
	GetRecommendedEmoticonPrefix(ctx context.Context, channelId string) (string, error)
}

type prefixRecommender struct {
	PrefixDao  dynamo.IPrefixDao
	Clients    *clients.Clients
	Stats      statsd.Statter
	alphaRegex *regexp.Regexp
}

func NewPrefixRecommender(dao dynamo.IPrefixDao, stats statsd.Statter, clients *clients.Clients) PrefixRecommender {
	regex, err := regexp.Compile("[^a-zA-Z0-9]")
	if err != nil || regex == nil {
		log.WithError(err).Panic("Failed to compile regular expression")
	}

	return &prefixRecommender{
		PrefixDao:  dao,
		Clients:    clients,
		Stats:      stats,
		alphaRegex: regex,
	}
}

/*
GetRecommendedEmoticonPrefix is used to generate an emoticon prefix based on the user name
Sanitize the username by
1) Remove all non alpha numeric characters from the user name and convert it to lower case
2) Utilize only the 1st 6 characters in determining the prefix
3) If the sanitized name has a digit append a char to it
4) Make sure the min length o sanitized string is 3, append char if it is not
5) Make sure that the generated prefix is not already in use. Append incremental numbers if the prefix exists
*/
func (pr *prefixRecommender) GetRecommendedEmoticonPrefix(ctx context.Context, channelId string) (string, error) {
	userInfo, err := pr.Clients.UserServiceClient.GetUserByID(ctx, channelId, nil)
	if err != nil {
		log.WithError(err).Error(fmt.Sprintf("Failed to get UserDetails for channel %s", channelId))
		return "", err
	}
	if userInfo.Login == nil {
		err = errors.New(fmt.Sprintf("User Login is null for channel %s", channelId))
		log.WithError(err).Error(err)
		return "", err
	}

	loginName := *userInfo.Login
	// Use regex to remove all non alphanumeric characters, then transform the string to lowercase
	prefix := strings.ToLower(pr.alphaRegex.ReplaceAllString(loginName, ""))

	if len(prefix) == 0 {
		err = errors.New(fmt.Sprintf("No valid characters after sanitising channel name %s", loginName))
		log.WithError(err).Error(err)
		return "", err
	}

	//Use the 1st 6 Characters of the username for generating the prefix, if the length of string is < 6 then use the length
	limit := 6
	if len(prefix) < 6 {
		limit = len(prefix)
	}
	prefix = prefix[0:limit]
	//Emoticon prefix cannot start with a Digit
	if unicode.IsDigit([]rune(prefix)[0]) {
		if limit >= 6 {
			limit = limit - 1
		}
		prefix = "x" + prefix[:limit]
	}
	//Minimum length of the prefix is 3
	if len(prefix) == 1 {
		prefix = prefix + "11"
	}
	if len(prefix) == 2 {
		prefix = prefix + "1"
	}

	existingPrefix, err := pr.PrefixDao.Get(ctx, prefix)
	if err != nil {
		err = errors.Wrapf(err, "Failed to check if prefix is unique %s", prefix)
		log.WithError(err).Error(err)
		return "", err
	} else if existingPrefix == nil { // prefix not in use and is valid, so we're good to bail now
		return prefix, nil
	} else if existingPrefix.OwnerId == channelId { // we are recommending them the prefix they already have, just short circuit and bail.
		return existingPrefix.Prefix, nil
	}

	return pr.attack(ctx, channelId, prefix)
}

// Builds an inclusive list of list of modifiers and keys (inclusive as in 1-5000, will include 5000) that slices it
// equal segments in that range and of numberOfAttackSlices slices (+1 because of inclusiveness).
func determineTargets(base string, min, max int) ([]int, []string) {
	keys := []string{}
	mods := []int{}

	modifierRange := max - min

	for i := 0; i <= numberOfAttackSlices; i++ {
		// Build the next modifier by slicing the range up and using the array index as a multiplier
		// ie if base is 'bear', min is 1 and max is 5000 then this would look generate
		// bear1, bear250, bear500, bear750 ...bear 4750 bear5000
		modifier := min + (i * modifierRange / numberOfAttackSlices)

		// Here we'll check if we're the first entry off the list (ok)
		// or if the current modifier is not the same as the last one in the list (ok)
		// if it passes we believe it will be a unique addition to the list.
		// This is needed once the slices are smaller than numberOfAttackSlices.
		if i == 0 || modifier != mods[len(mods)-1] {
			mods = append(mods, modifier)
			keys = append(keys, base+strconv.Itoa(modifier))
		}
	}
	return mods, keys
}

func (pr *prefixRecommender) attack(ctx context.Context, channelId, base string) (string, error) {
	min, max := 1, maxPrefixRange
	i := 0

	for ; i < maxAttackIterations && min < max; i++ {
		mods, keys := determineTargets(base, min, max)

		existingPrefixes, err := pr.PrefixDao.GetBatch(ctx, keys...)
		if err != nil {
			log.WithError(err).WithFields(log.Fields{
				"channelId": channelId,
				"base":      base,
				"keys":      keys,
			}).Error("Encountered error while attempting attacking dynamo table for recommended prefixes")
			return "", err
		}

		for _, mod := range mods {
			proposedPrefix := base + strconv.Itoa(mod)
			existingPrefix, ok := existingPrefixes[proposedPrefix]

			if ok {
				if existingPrefix.OwnerId == channelId { // we are recommending them the prefix they already have, just short circuit and bail.
					return existingPrefix.Prefix, nil
				} else {
					min = mod + 1 // go one past since it's taken
				}
			} else {
				max = mod // we know there isn't something up here
				break     // we don't need to look at the stuff after this.
			}
		}
	}

	if max < min { // all are in use
		log.WithFields(log.Fields{
			"channelId":   channelId,
			"base":        base,
			"minModifier": min,
			"maxModifier": max,
		}).Error("Failed to determine prefix in first 5000 area")
		return "", errors.New("cannot find a prefix in first 5000 area")
	} else if i == maxAttackIterations {
		log.WithFields(log.Fields{
			"channelId":     channelId,
			"base":          base,
			"minModifier":   min,
			"maxModifier":   max,
			"maxIterations": maxAttackIterations,
		}).Error("Failed to determine prefix before max iterations")
		return "", errors.New("could not determine prefix, before hitting iterations wall")
	} else {
		if max > (maxPrefixRange * 0.9) {
			log.WithFields(log.Fields{
				"base":                   base,
				"modifier":               max,
				"maximum allowed prefix": maxPrefixRange,
			}).Warn("Prefixes are approaching max allowed.")
			_ = pr.Stats.Inc(prefixDangerMetric, 1, 1.0)
		}
		return base + strconv.Itoa(max), nil
	}
}
