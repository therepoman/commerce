package dynamo

import (
	"strings"
	"time"

	"code.justin.tv/commerce/logrus"
	mako "code.justin.tv/commerce/mako/internal"
)

type AccountType string

const (
	Partner   = AccountType("partner")
	Affiliate = AccountType("affiliate")
	None      = AccountType("none")
)

func parseAccountType(accountType string) AccountType {
	switch strings.ToLower(accountType) {
	case "partner":
		return Partner
	case "affiliate":
		return Affiliate
	default:
		logrus.WithField("accountType", accountType).Warn("Unsupported accountType encountered!")
		return None
	}
}

type PendingEmoteReviewState string

const (
	PendingEmoteReviewStateNone     = PendingEmoteReviewState("none")
	PendingEmoteReviewStateDeferred = PendingEmoteReviewState("deferred")
)

type PendingEmotesFilter struct {
	Type        AccountType
	ReviewState PendingEmoteReviewState
	After       time.Time
	AssetType   mako.EmoteAssetType
}
