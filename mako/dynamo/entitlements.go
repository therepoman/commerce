package dynamo

import (
	"errors"

	log "code.justin.tv/commerce/logrus"
	"github.com/afex/hystrix-go/hystrix"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"

	"code.justin.tv/common/godynamo/client"
)

const (
	// --KEYS--
	// EntitlementsHashKey is the dynamo table hash key
	EntitlementsHashKey string = "userId"
	// EntitlementsSortKey is the dynamo table sort key
	EntitlementsSortKey string = "entitlementScopedKey"

	// --ATTRIBUTES--
	EntitlementsOriginAttribute string = "origin"

	// EntitlementsReadCapacity is the dynamo table read throughput
	EntitlementsReadCapacity int64 = 100
	// EntitlementsWriteCapacity is the dynamo table write throughput
	EntitlementsWriteCapacity int64 = 100

	// HystrixNameEntitlementsGet is the identifier for the hystric circuit for entitlements GET
	HystrixNameEntitlementsGet string = "dynamoDB.entitlements.GET"
)

// Entitlement metadata
type Entitlement struct {
	client.BaseDynamoTableRecord
	UserId               string // Hash Key
	EntitlementScopedKey string // Range Key
	Origin               string
}

// EntitlementDao data access object for entitlements
type EntitlementDao struct {
	table  client.DynamoTable
	client *client.DynamoClient
}

// EntitlementsTable the entitlement table
type EntitlementsTable struct{}

// IEntitlementDao interface for the entitlement DAO
type IEntitlementDao interface {
	Get(tuid string, entitlementScopedKey string) ([]*Entitlement, error)
	Put(entitlement *Entitlement) error
	Update(entitlement *Entitlement) error
	UpdateItemWithAction(entitlement *Entitlement, action string) error
	Delete(entitlement *Entitlement) error
	SetupTable() error
	DeleteTable() error
	GetClientConfig() *client.DynamoClientConfig
}

// GetTableName returns the base entitlement table name
func (t *EntitlementsTable) GetTableName() string {
	return "entitlements"
}

// NewAttributeMap creates a mapping from Entitlement to Dynamo attributes
func (e *Entitlement) NewAttributeMap() map[string]*dynamodb.AttributeValue {
	itemMap := make(map[string]*dynamodb.AttributeValue)
	client.PopulateAttributesString(itemMap, EntitlementsHashKey, e.UserId)
	client.PopulateAttributesString(itemMap, EntitlementsSortKey, e.EntitlementScopedKey)

	client.PopulateAttributesString(itemMap, EntitlementsOriginAttribute, e.Origin)

	return itemMap
}

// NewItemKey returns the key map to query against in Dynamo
func (e *Entitlement) NewItemKey() map[string]*dynamodb.AttributeValue {
	itemMap := make(map[string]*dynamodb.AttributeValue)
	client.PopulateAttributesString(itemMap, EntitlementsHashKey, e.UserId)
	client.PopulateAttributesString(itemMap, EntitlementsSortKey, e.EntitlementScopedKey)
	return itemMap
}

// ConvertAttributeMapToRecord converts the Dynamo response to an Entitlement
func (t *EntitlementsTable) ConvertAttributeMapToRecord(attributeMap map[string]*dynamodb.AttributeValue) (client.DynamoTableRecord, error) {
	userId := client.StringFromAttributes(attributeMap, EntitlementsHashKey)
	entitlementScopedKey := client.StringFromAttributes(attributeMap, EntitlementsSortKey)

	origin := client.StringFromAttributes(attributeMap, EntitlementsOriginAttribute)

	return &Entitlement{
		UserId:               userId,
		EntitlementScopedKey: entitlementScopedKey,
		Origin:               origin,
	}, nil
}

// NewCreateTableInput sets up a new entitlement table input properties
func (t *EntitlementsTable) NewCreateTableInput() *dynamodb.CreateTableInput {
	return &dynamodb.CreateTableInput{
		ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
			ReadCapacityUnits:  aws.Int64(EntitlementsReadCapacity),
			WriteCapacityUnits: aws.Int64(EntitlementsWriteCapacity),
		},
		AttributeDefinitions: []*dynamodb.AttributeDefinition{
			{
				AttributeName: aws.String(EntitlementsHashKey),
				AttributeType: aws.String("S"),
			},
			{
				AttributeName: aws.String(EntitlementsSortKey),
				AttributeType: aws.String("S"),
			},
		},
		KeySchema: []*dynamodb.KeySchemaElement{
			{
				AttributeName: aws.String(EntitlementsHashKey),
				KeyType:       aws.String("HASH"),
			},
			{
				AttributeName: aws.String(EntitlementsSortKey),
				KeyType:       aws.String("SORT"),
			},
		},
	}
}

// NewEntitlementDao constructor for EntitlementDAO
func NewEntitlementDao(client *client.DynamoClient) IEntitlementDao {
	dao := &EntitlementDao{}
	dao.client = client
	dao.table = &EntitlementsTable{}
	return dao
}

// GetTable returns the entitlement table
func (e *Entitlement) GetTable() client.DynamoTable {
	return &EntitlementsTable{}
}

// GetClientConfig returns the dynamo client configuration
func (dao *EntitlementDao) GetClientConfig() *client.DynamoClientConfig {
	return dao.client.ClientConfig
}

// Get queries for entitlements matching the provided tuid and entitlement scoped key
func (dao *EntitlementDao) Get(tuid string, entitlementScopedKey string) ([]*Entitlement, error) {
	filter := client.QueryFilter{
		Names: map[string]*string{
			"#ID":        aws.String(EntitlementsHashKey),
			"#SCOPEDKEY": aws.String(EntitlementsSortKey),
		},
		Values: map[string]*dynamodb.AttributeValue{
			":id": {
				S: aws.String(tuid),
			},
			":prefix": {
				S: aws.String(entitlementScopedKey),
			},
		},
		KeyConditionExpression: aws.String("#ID = :id AND begins_with(#SCOPEDKEY, :prefix)"),
	}

	var result []client.DynamoTableRecord
	err := hystrix.Do(HystrixNameEntitlementsGet, func() error {
		var queryErr error
		result, queryErr = dao.client.QueryTable(dao.table, filter)
		if queryErr != nil {
			log.WithError(queryErr).Error("Error querying entitlements table")
		}
		return queryErr
	}, nil)

	if err != nil {
		return nil, err
	}

	if result == nil {
		return nil, nil
	}

	entitlementResult := make([]*Entitlement, len(result))
	for i, record := range result {
		var isEntitlementEntry bool
		entitlementResult[i], isEntitlementEntry = record.(*Entitlement)
		if !isEntitlementEntry {
			return entitlementResult, errors.New("Encountered an unexpected type while converting dynamo result to entitlement record")
		}
	}

	return entitlementResult, nil
}

// Put adds a user and his or her entitlements to the entitlements table
func (dao *EntitlementDao) Put(entitlement *Entitlement) error {
	return dao.client.PutItem(entitlement)
}

// Update updates the users entitlements with new entitlements
func (dao *EntitlementDao) Update(entitlement *Entitlement) error {
	return dao.client.UpdateItem(entitlement)
}

// UpdateItemWithAction updates the users entitlements with new entitlements with action
func (dao *EntitlementDao) UpdateItemWithAction(entitlement *Entitlement, action string) error {
	return dao.client.UpdateItemWithAction(entitlement, action)
}

// Delete deletes the given entitlement from the entitlements table
func (dao *EntitlementDao) Delete(entitlement *Entitlement) error {
	return dao.client.DeleteItem(entitlement)
}

// SetupTable sets up a new dynamo table
func (dao *EntitlementDao) SetupTable() error {
	return dao.client.SetupTable(dao.table)
}

// DeleteTable deletes the dynamo table
func (dao *EntitlementDao) DeleteTable() error {
	return dao.client.DeleteTable(dao.table)
}
