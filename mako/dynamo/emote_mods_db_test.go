package dynamo

import (
	"testing"

	"code.justin.tv/commerce/mako/tests"
)

func TestDynamoEmoteModsDB(t *testing.T) {
	tests.TestEmoteModifiersDB(t, MakeEmoteModifiersDBForTesting)
}
