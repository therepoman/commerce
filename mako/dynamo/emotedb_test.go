package dynamo

import (
	"testing"

	"code.justin.tv/commerce/mako/localdb"
	"code.justin.tv/commerce/mako/tests"
)

func TestDynamoEmoteDB(t *testing.T) {
	tests.TestEmoteDB(t, MakeEmoteDB)
}

func TestDynamoEmoteCodes(t *testing.T) {
	tests.TestEmoteCodes(t, tests.EmoteCodesConfig{
		MakeEmoteDB:         MakeEmoteDB,
		MakeEntitlementDB:   localdb.MakeEntitlementDB,
		MakeEmoteModifierDB: localdb.MakeEmoteModifierDB,
	})
}

func TestDynamoUserChannelEmotes(t *testing.T) {
	tests.TestUserChannelEmotes(t, tests.UserChannelEmotesConfig{
		MakeEmoteDB:         MakeEmoteDB,
		MakeEntitlementDB:   localdb.MakeEntitlementDB,
		MakeEmoteModifierDB: localdb.MakeEmoteModifierDB,
	})
}

// TODO: DIGI-693 - The handling of Dynamo UnprocessedItems is not adequately tested throughout all batch APIs in the dynamo EmoteDB.
// This is particularly difficult to do because the Dynamo client exposed by the aws SDK is a concrete struct, not an interface.
// This makes it difficult to inject a double (either fake or mock) which EmoteDB will accept.
