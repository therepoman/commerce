package dynamo

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"time"

	"github.com/aws/aws-sdk-go/aws/credentials"

	"github.com/segmentio/ksuid"

	mako "code.justin.tv/commerce/mako/internal"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
	"github.com/pkg/errors"
)

const (
	emoteGroupsTableName  = "emote_groups"
	emoteGroupsHashKey    = "groupId"
	emoteGroupsOwnerID    = "ownerId"
	emoteGroupsOrigin     = "origin"
	emoteGroupsType       = "groupType"
	emoteGroupsOwnerIndex = "emote_groups_ownerid_index"

	// maxGetEmoteBatchAttempts is the maximum number of times we want a BatchGetItem loop to attempt to process unprocessed keys
	maxGetEmoteGroupsBatchAttempts = 3
)

type EmoteGroup struct {
	GroupID   string    `dynamodbav:"groupId"`
	OwnerID   string    `dynamodbav:"ownerId,omitempty"`
	Origin    string    `dynamodbav:"origin"`
	CreatedAt time.Time `dynamodbav:"createdAt"`
	GroupType string    `dynamodbav:"groupType"`
}

func (eg EmoteGroup) ToMako() mako.EmoteGroup {
	return mako.EmoteGroup{
		GroupID:   eg.GroupID,
		OwnerID:   eg.OwnerID,
		Origin:    mako.EmoteGroupOrigin(eg.Origin),
		GroupType: toMakoGroupType(eg.GroupType),
		CreatedAt: eg.CreatedAt,
	}
}

func convertEmoteGroup(emoteGroup mako.EmoteGroup) EmoteGroup {
	// Normally, clients do not provide a createdAt to the dynamo layer, but for running the test suite, we include this
	// info so that we can verify that the returned records were exactly what our tests expected.
	createdAt := emoteGroup.CreatedAt
	if createdAt.IsZero() {
		createdAt = time.Now()
	}

	return EmoteGroup{
		GroupID:   emoteGroup.GroupID,
		OwnerID:   emoteGroup.OwnerID,
		Origin:    emoteGroup.Origin.String(),
		GroupType: emoteGroup.GroupType.String(),
		CreatedAt: createdAt,
	}
}

type emoteGroupDB struct {
	client    *dynamodb.DynamoDB
	tableName string
}

type EmoteGroupDBConfig struct {
	// The name of the table in dynamo
	Table string
	// The AWS region to connect to
	Region string
	// Used to connect to local dynamodb for testing
	Endpoint string
	// Used to scope data in local dynamodb for testing
	Namespace string
}

func NewEmoteGroupDB(config EmoteGroupDBConfig) *emoteGroupDB {
	sess := session.Must(session.NewSession())

	conf := aws.Config{
		Region: aws.String(config.Region),
		HTTPClient: &http.Client{
			Transport: &http.Transport{
				MaxIdleConnsPerHost: 10000,
			},
		},
	}

	// Endpoint is only used when bootstrapping tests. Local dynamodb requires static credentials
	// to work and they can be anything.
	if config.Endpoint != "" {
		conf.Credentials = credentials.NewStaticCredentials(config.Namespace, config.Namespace, "")
		conf.Endpoint = aws.String(config.Endpoint)
	}

	client := dynamodb.New(sess, &conf)

	return &emoteGroupDB{client: client, tableName: config.Table}
}

func (e *emoteGroupDB) create(ctx context.Context) error {
	_, err := e.client.CreateTableWithContext(ctx, &dynamodb.CreateTableInput{
		AttributeDefinitions: []*dynamodb.AttributeDefinition{
			{
				AttributeName: aws.String(emoteGroupsHashKey),
				AttributeType: aws.String("S"),
			},
			{
				AttributeName: aws.String(emoteGroupsOwnerID),
				AttributeType: aws.String("S"),
			},
		},
		GlobalSecondaryIndexes: []*dynamodb.GlobalSecondaryIndex{
			{
				IndexName: aws.String(emoteGroupsOwnerIndex),
				KeySchema: []*dynamodb.KeySchemaElement{
					{
						AttributeName: aws.String(emoteGroupsOwnerID),
						KeyType:       aws.String("HASH"),
					},
				},
				Projection: &dynamodb.Projection{
					ProjectionType: aws.String(dynamodb.ProjectionTypeAll),
				},
				ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
					ReadCapacityUnits:  aws.Int64(10),
					WriteCapacityUnits: aws.Int64(10),
				},
			},
		},
		KeySchema: []*dynamodb.KeySchemaElement{
			{
				AttributeName: aws.String(emoteGroupsHashKey),
				KeyType:       aws.String("HASH"),
			},
		},
		ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
			ReadCapacityUnits:  aws.Int64(10),
			WriteCapacityUnits: aws.Int64(10),
		},
		TableName: &e.tableName,
	})
	if err != nil {
		return err
	}

	return nil
}

func (e *emoteGroupDB) CreateEmoteGroup(ctx context.Context, group mako.EmoteGroup) (mako.EmoteGroup, bool, error) {
	emoteGroup := convertEmoteGroup(group)

	av, err := dynamodbattribute.MarshalMap(emoteGroup)
	if err != nil {
		return mako.EmoteGroup{}, false, errors.Wrapf(err, "Failed to marshal dynamo emote groups attribute: %v", emoteGroup)
	}

	input := &dynamodb.PutItemInput{
		TableName:           aws.String(e.tableName),
		Item:                av,
		ConditionExpression: aws.String(fmt.Sprintf("attribute_not_exists(%s)", emoteGroupsHashKey)),
	}

	_, err = e.client.PutItemWithContext(ctx, input)

	// mirror previous behavior from dynamodbplus
	if IsConditionalCheckFailed(err) {
		return mako.EmoteGroup{}, true, nil
	}

	return group, false, err
}

func (e *emoteGroupDB) getEmoteGroupsByOwner(ctx context.Context, ownerID string) ([]EmoteGroup, error) {
	key := expression.KeyEqual(expression.Key(emoteGroupsOwnerID), expression.Value(ownerID))
	expr, err := expression.NewBuilder().WithKeyCondition(key).Build()
	if err != nil {
		return nil, errors.Wrap(err, "failed to build query key expression")
	}

	res, err := e.client.QueryWithContext(ctx, &dynamodb.QueryInput{
		IndexName:                 aws.String(emoteGroupsOwnerIndex),
		TableName:                 aws.String(e.tableName),
		KeyConditionExpression:    expr.KeyCondition(),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to read emote group by ownerId")
	}
	if len(res.Items) == 0 {
		return nil, nil
	}

	result := make([]EmoteGroup, 0, len(res.Items))

	for _, item := range res.Items {
		var group EmoteGroup
		err = dynamodbattribute.UnmarshalMap(item, &group)
		if err != nil {
			return nil, errors.Wrap(err, "failed to unmarshal emote group")
		}
		result = append(result, group)
	}
	return result, nil
}

func (e *emoteGroupDB) GetFollowerEmoteGroup(ctx context.Context, ownerId string) (*mako.EmoteGroup, error) {
	return e.getEmoteGroupByOwnerAndOrigin(ctx, ownerId, mako.EmoteGroupOriginFollower.String())
}

func (e *emoteGroupDB) getEmoteGroupByOwnerAndOrigin(ctx context.Context, ownerId, origin string) (*mako.EmoteGroup, error) {
	groups, err := e.getEmoteGroupsByOwner(ctx, ownerId)
	if err != nil {
		return nil, err
	}
	for _, group := range groups {
		if group.Origin == origin {
			makoGroup := group.ToMako()
			return &makoGroup, nil
		}
	}
	// We use nil, nil here to represent the "not found" case, so the caller can distinguish that from
	// actual database errors and decide whether to proceed or not.
	return nil, nil
}

func (e *emoteGroupDB) GetEmoteGroups(ctx context.Context, ids ...string) ([]mako.EmoteGroup, error) {
	results := make([]mako.EmoteGroup, 0, len(ids))

	for i := 0; i <= len(ids)/maxBatchGetItemSize; i++ {
		start := i * maxBatchGetItemSize
		end := start + maxBatchGetItemSize
		if end > len(ids) {
			end = len(ids)
		}

		batch := ids[start:end]

		if len(batch) == 0 {
			break
		}

		var keys []map[string]*dynamodb.AttributeValue
		for _, groupID := range batch {
			keys = append(keys, map[string]*dynamodb.AttributeValue{
				"groupId": {
					S: aws.String(groupID),
				},
			})
		}
		keysAndAttributes := map[string]*dynamodb.KeysAndAttributes{
			e.tableName: {
				Keys: keys,
			},
		}

		numBatchGetItemAttempts := 0
		for len(keysAndAttributes) > 0 && numBatchGetItemAttempts < maxGetEmoteGroupsBatchAttempts {
			res, err := e.client.BatchGetItemWithContext(ctx, &dynamodb.BatchGetItemInput{
				RequestItems: keysAndAttributes,
			})
			if err != nil {
				return nil, errors.Wrap(err, "failed to read emote groups by id")
			}

			for _, items := range res.Responses {
				var groups []EmoteGroup
				if err := dynamodbattribute.UnmarshalListOfMaps(items, &groups); err != nil {
					return nil, errors.Wrap(err, "failed to unmarshal group group dynamo response")
				}

				for _, group := range groups {
					results = append(results, group.ToMako())
				}
			}

			keysAndAttributes = res.UnprocessedKeys
			numBatchGetItemAttempts++
		}

		if len(keysAndAttributes) > 0 {
			return nil, errors.New("Failed to get all requested emote groups by ids. Some keys were not processed after multiple attempts.")
		}
	}

	emoteGroups := make([]mako.EmoteGroup, 0, len(results))
	for _, result := range results {
		emoteGroup := result
		emoteGroup.GroupType = getEmoteGroupType(emoteGroup)
		emoteGroups = append(emoteGroups, emoteGroup)
	}

	return emoteGroups, nil
}

func (e *emoteGroupDB) DeleteEmoteGroup(ctx context.Context, id string) error {
	input := &dynamodb.DeleteItemInput{
		TableName: aws.String(e.tableName),
		Key:       makeSingleStringMap(emoteGroupsHashKey, id),
	}

	_, err := e.client.DeleteItemWithContext(ctx, input)
	if err != nil {
		return errors.Wrapf(err, "failed to delete emoteGroup '%s' from the database", id)
	}

	return nil
}

func IsConditionalCheckFailed(err error) bool {
	awsErr, ok := err.(awserr.Error)
	return ok && awsErr.Code() == "ConditionalCheckFailedException"
}

func MakeEmoteGroupDBForTesting(ctx context.Context, groups ...mako.EmoteGroup) (mako.EmoteGroupDB, func(), error) {
	addr := "http://localhost:8005"

	// Jenkins require a different dynamodb address for tests.
	if os.Getenv("DOCKER_TEST") != "" {
		addr = "http://jenkins_dynamodb:8000"
	}

	emoteGroupDB := NewEmoteGroupDB(EmoteGroupDBConfig{
		Region:    "us-west-2",
		Namespace: ksuid.New().String(),
		Endpoint:  addr,
		Table:     emoteGroupsTableName,
	})

	teardown := func() {}

	if err := emoteGroupDB.create(ctx); err != nil {
		return nil, teardown, err
	}

	for _, group := range groups {
		_, _, err := emoteGroupDB.CreateEmoteGroup(ctx, group)
		if err != nil {
			return nil, teardown, err
		}
	}

	return emoteGroupDB, teardown, nil
}

// TODO: backfill all emote groups into prod_emote_groups and assign their group type accordingly, so we don't need to
// rely on defaulting logic like this function.
// Any animated emote group that exists will be present in the EmoteGroupDB. For all other cases, default to static
func getEmoteGroupType(emoteGroup mako.EmoteGroup) mako.EmoteGroupType {
	// All animated emote groups are present in the DB
	if emoteGroup.GroupType == mako.EmoteGroupAnimated {
		return mako.EmoteGroupAnimated
	}

	return mako.EmoteGroupStatic
}

// When creating an emote group, we want to make sure that the data is populated correctly, so we create this default for group type
func toMakoGroupType(emoteGroupType string) mako.EmoteGroupType {
	if emoteGroupType == mako.EmoteGroupAnimated.String() {
		return mako.EmoteGroupAnimated
	}
	return mako.EmoteGroupStatic
}
