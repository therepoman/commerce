package dynamo

import (
	"context"
	"fmt"
	"net"
	"net/http"
	"time"

	"code.justin.tv/chat/golibs/clients/dynamodbplus"
	mako "code.justin.tv/commerce/mako/internal"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
	"github.com/pkg/errors"
)

const (
	// pendingEmotesXactGroup is the xact string reported to statsd
	pendingEmotesXactGroup = "pendingemotesdb"

	// maxBatchWriteItemAttempts is the maximum number of times we want a BatchWriteItem loop to attempt to process unprocessed items
	maxBatchWriteItemAttempts = 5
	// maxBatchGetItemAttempts is the maximum number of times we want a BatchGetItem loop to attempt to process unprocessed keys
	maxBatchGetItemAttempts = 5
)

// PendingEmotesDB is the interface that wraps all Pending Emoticon database methods.
type IPendingEmotesDao interface {
	CreatePendingEmoticon(ctx context.Context, id, prefix, code, ownerID string, accountType AccountType, reviewState PendingEmoteReviewState, assetType mako.EmoteAssetType, animationTemplate mako.AnimatedEmoteTemplate) (*PendingEmote, error)
	DeletePendingEmoticons(ctx context.Context, emoticonIDs []string) error
	UpdatePendingEmoticonsReviewState(ctx context.Context, emoticons []*PendingEmote, newReviewState PendingEmoteReviewState) error
	GetPendingEmoticonsByIDs(ctx context.Context, emoticonIDs []string) ([]*PendingEmote, error)
	GetPendingEmoticonsWithFilters(ctx context.Context, filters PendingEmotesFilter, limit int64, startKey Cursor) (*GetPendingEmoticonsResponse, error)
	GetAllPendingEmoticons(ctx context.Context) ([]*PendingEmote, error)
}

type pendingEmotesDBImpl struct {
	db        dynamodbplus.Client
	tableName string
}

type GetPendingEmoticonsResponse struct {
	LastEvaluatedKey Cursor
	Items            []*PendingEmote
}

type Cursor map[string]*dynamodb.AttributeValue

// NewPendingEmotesDBClient builds a new PendingEmotesDB dynamo client using aws session and dynamo db
// table name.
func NewPendingEmotesDao(region, environmentPrefix string) (IPendingEmotesDao, error) {
	tableName := fmt.Sprintf("%s_%s", environmentPrefix, PendingEmotesTableName)

	// Create a DynamoDB client from session.
	httpClient := &http.Client{
		Timeout: 5 * time.Second,
		Transport: &http.Transport{
			Proxy: http.ProxyFromEnvironment,
			DialContext: (&net.Dialer{
				Timeout:   5 * time.Second,
				KeepAlive: 30 * time.Second,
			}).DialContext,
			TLSHandshakeTimeout: 5 * time.Second,
			MaxIdleConnsPerHost: 1000,
		},
	}

	db := dynamodbplus.NewClient(dynamodbplus.Config{
		Region:     region,
		XactGroup:  pendingEmotesXactGroup,
		HTTPClient: httpClient,
	})

	// Check if the table exists
	_, err := db.DescribeTable(context.Background(), &dynamodb.DescribeTableInput{TableName: aws.String(tableName)})
	if err != nil {
		return nil, err
	}

	return &pendingEmotesDBImpl{db: db, tableName: tableName}, nil
}

// CreatePendingEmoticon creates a new item in Pending Emoticons dynamo table.
func (pe *pendingEmotesDBImpl) CreatePendingEmoticon(ctx context.Context, id, prefix, code, ownerID string, accountType AccountType, reviewState PendingEmoteReviewState, assetType mako.EmoteAssetType, animationTemplate mako.AnimatedEmoteTemplate) (*PendingEmote, error) {
	timestamp := time.Now().UTC()

	pei := PendingEmote{
		EmoteId:           id,
		Prefix:            prefix,
		Code:              code,
		OwnerId:           ownerID,
		Type:              accountType,
		ReviewState:       reviewState,
		CreatedAt:         timestamp,
		UpdatedAt:         timestamp,
		TypeReviewState:   makeTypeReviewStateKey(accountType, reviewState),
		AssetType:         string(assetType),
		AnimationTemplate: string(animationTemplate),
	}

	// Marshal pending emoticon to a map of AttributeValues
	av, err := dynamodbattribute.MarshalMap(pei)
	if err != nil {
		return nil, errors.Wrapf(err, "Failed to marshal dynamo pending emoticon attribute: %v", pei)
	}

	// Create PutItemInput
	input := &dynamodb.PutItemInput{
		TableName:           aws.String(pe.tableName),
		Item:                av,
		ConditionExpression: aws.String(fmt.Sprintf("attribute_not_exists(%s)", PendingEmotesHashKey)),
	}

	// Put item in dynamodb
	if _, condFail, err := pe.db.PutItem(ctx, input); err != nil {
		return nil, errors.Wrap(err, "PendingEmoticons PutItem failed")
	} else if condFail {
		return nil, errors.New("PendingEmoticonID already exists")
	}

	return &pei, nil
}

// DeletePendingEmoticons removes a pending emoticon from the dynamo table. This should be done after it has been acted upon and is either approved or rejected
func (pe *pendingEmotesDBImpl) DeletePendingEmoticons(ctx context.Context, emoticonIDs []string) error {
	for i := 0; i < len(emoticonIDs); i += maxBatchWriteItemSize {
		end := intMin(len(emoticonIDs), i+maxBatchWriteItemSize)
		batchEmotes := emoticonIDs[i:end]
		err := pe.deletePendingEmoticonsBatch(ctx, batchEmotes)
		if err != nil {
			return err
		}
	}

	return nil
}

// deletePendingEmoticons removes a set of at most 25 pending emoticons from the dynamo table.
func (pe *pendingEmotesDBImpl) deletePendingEmoticonsBatch(ctx context.Context, emoticonIDs []string) error {
	if len(emoticonIDs) > maxBatchWriteItemSize {
		return errors.New("PendingEmoticon Delete BatchWriteItem has a limit of 25 per request")
	}

	emoteWriteRequests := make([]*dynamodb.WriteRequest, len(emoticonIDs))
	for i, emoticonID := range emoticonIDs {
		emoteWriteRequests[i] = &dynamodb.WriteRequest{
			DeleteRequest: &dynamodb.DeleteRequest{
				Key: map[string]*dynamodb.AttributeValue{
					PendingEmotesHashKey: {
						S: aws.String(emoticonID),
					},
				},
			},
		}
	}

	numBatchWriteItemAttempts := 0

	// BatchWriteItem will "succeed" if the only issue is going over consumed write capacity limits and it will return the unprocessed items.
	// The recommended solution is to run BatchWriteItem in a loop until UnprocessedItems is empty.
	for len(emoteWriteRequests) > 0 && numBatchWriteItemAttempts < maxBatchWriteItemAttempts {
		batchWriteItemInput := &dynamodb.BatchWriteItemInput{
			RequestItems: map[string][]*dynamodb.WriteRequest{
				pe.tableName: emoteWriteRequests,
			},
		}

		batchWriteItemResponse, err := pe.db.BatchWriteItem(ctx, batchWriteItemInput)
		if err != nil {
			return errors.Wrap(err, "Failed to delete pending emoticons")
		}

		emoteWriteRequests = batchWriteItemResponse.UnprocessedItems[pe.tableName]
		numBatchWriteItemAttempts++
	}

	if len(emoteWriteRequests) > 0 {
		// Not all requests were processed, we should error
		return errors.New("Failed to delete all requested pending emoticons. Some items were not processed after multiple attempts.")
	}

	return nil
}

func (pe *pendingEmotesDBImpl) UpdatePendingEmoticonsReviewState(ctx context.Context, emoticons []*PendingEmote, newReviewState PendingEmoteReviewState) error {
	for i := 0; i < len(emoticons); i += maxBatchWriteItemSize {
		end := intMin(len(emoticons), i+maxBatchWriteItemSize)
		batchEmotes := emoticons[i:end]

		err := pe.updatePendingEmoticonsReviewStateBatch(ctx, batchEmotes, newReviewState)
		if err != nil {
			return err
		}
	}

	return nil
}

// updatePendingEmoticonsReviewStateBatch updates the review state of at most 25 pending emoticons from the dynamo table.
func (pe *pendingEmotesDBImpl) updatePendingEmoticonsReviewStateBatch(ctx context.Context, emoticons []*PendingEmote, newReviewState PendingEmoteReviewState) error {
	if len(emoticons) > maxBatchWriteItemSize {
		return errors.New("PendingEmoticon Put BatchWriteItem has a limit of 25 per request")
	}

	emoteWriteRequests := make([]*dynamodb.WriteRequest, len(emoticons))
	for i, emoticon := range emoticons {
		emoticon.ReviewState = newReviewState
		emoticon.TypeReviewState = makeTypeReviewStateKey(emoticon.Type, newReviewState)

		// Marshal pending emoticon to a map of AttributeValues
		av, err := dynamodbattribute.MarshalMap(emoticon)
		if err != nil {
			return errors.Wrapf(err, "Failed to marshal dynamo pending emoticon attribute while updating review state: %v", emoticon)
		}

		emoteWriteRequests[i] = &dynamodb.WriteRequest{
			PutRequest: &dynamodb.PutRequest{Item: av},
		}
	}

	numBatchWriteItemAttempts := 0

	// BatchWriteItem will "succeed" if the only issue is going over consumed write capacity limits, and it will return the unprocessed items.
	// The recommended solution is to run BatchWriteItem in a loop until UnprocessedItems is empty.
	for len(emoteWriteRequests) > 0 && numBatchWriteItemAttempts < maxBatchWriteItemAttempts {
		batchWriteItemInput := &dynamodb.BatchWriteItemInput{
			RequestItems: map[string][]*dynamodb.WriteRequest{
				pe.tableName: emoteWriteRequests,
			},
		}

		batchWriteItemResponse, err := pe.db.BatchWriteItem(ctx, batchWriteItemInput)
		if err != nil {
			return errors.Wrap(err, "Failed to update pending emoticons review state")
		}

		// Items that failed to get processed are returned as UnprocessedItems, so we should retry these items
		emoteWriteRequests = batchWriteItemResponse.UnprocessedItems[pe.tableName]
		numBatchWriteItemAttempts++
	}

	if len(emoteWriteRequests) > 0 {
		// Not all requests were processed, we should error
		return errors.New("Failed to update all requested pending emoticons' review states. Some items were not processed after multiple attempts.")
	}

	return nil
}

// GetPendingEmoticonsByIDs is a batch api to get particular pending emoticons by their emoticon ID
func (pe *pendingEmotesDBImpl) GetPendingEmoticonsByIDs(ctx context.Context, emoticonIDs []string) ([]*PendingEmote, error) {
	if len(emoticonIDs) == 0 {
		return make([]*PendingEmote, 0), nil
	}

	var pendingEmotes []*PendingEmote

	for i := 0; i < len(emoticonIDs); i += maxBatchGetItemSize {
		end := intMin(len(emoticonIDs), i+maxBatchGetItemSize)
		batchEmoteIDs := emoticonIDs[i:end]

		batchPendingEmotes, err := pe.getpendingEmoticonsByIDsBatch(ctx, batchEmoteIDs)
		if err != nil {
			return nil, err
		}

		pendingEmotes = append(pendingEmotes, batchPendingEmotes...)
	}

	return pendingEmotes, nil
}

func (pe *pendingEmotesDBImpl) getpendingEmoticonsByIDsBatch(ctx context.Context, emoticonIDs []string) ([]*PendingEmote, error) {
	if len(emoticonIDs) > maxBatchGetItemSize {
		return nil, errors.New("PendingEmoticon BatchGetItem has a limit of 100 items per request")
	}

	keys := make([]map[string]*dynamodb.AttributeValue, len(emoticonIDs))
	for i, key := range emoticonIDs {
		keys[i] = map[string]*dynamodb.AttributeValue{
			PendingEmotesHashKey: {
				S: aws.String(key),
			},
		}
	}

	tableKeysAndAttributes := &dynamodb.KeysAndAttributes{
		Keys: keys,
	}

	numBatchGetItemAttempts := 0
	var batchPendingEmotes []*PendingEmote

	for tableKeysAndAttributes != nil && len(tableKeysAndAttributes.Keys) > 0 && numBatchGetItemAttempts < maxBatchGetItemAttempts {
		batchGetItemInput := &dynamodb.BatchGetItemInput{
			RequestItems: map[string]*dynamodb.KeysAndAttributes{
				pe.tableName: tableKeysAndAttributes,
			},
		}

		res, err := pe.db.BatchGetItem(ctx, batchGetItemInput)
		if err != nil {
			return nil, err
		}

		processedPendingEmotes, err := pe.dynamoRespToPendingEmoticons(res.Responses[pe.tableName])
		if err != nil {
			return nil, err
		}

		batchPendingEmotes = append(batchPendingEmotes, processedPendingEmotes...)

		tableKeysAndAttributes = res.UnprocessedKeys[pe.tableName]
		numBatchGetItemAttempts++
	}

	if tableKeysAndAttributes != nil && len(tableKeysAndAttributes.Keys) > 0 {
		// Not all requests were processed, we should error
		return nil, errors.New("Failed to get all requested pending emoticons by IDs. Some keys were not processed after multiple attempts.")
	}

	return batchPendingEmotes, nil
}

// GetPendingEmoticons returns the requested number of pending emoticons matching the filter emoticonType created after the time provided (for pagination)
func (pe *pendingEmotesDBImpl) GetPendingEmoticonsWithFilters(ctx context.Context, filters PendingEmotesFilter, limit int64, startKey Cursor) (*GetPendingEmoticonsResponse, error) {
	if limit == 0 {
		return &GetPendingEmoticonsResponse{Items: make([]*PendingEmote, 0)}, nil
	}

	var indexName *string
	var keyCondition expression.KeyConditionBuilder

	// Build Query Input
	if filters.Type != "" && filters.ReviewState != "" {
		// Both type and review state are specified
		indexName = aws.String(PendingEmotesTypeReviewStateGSIName)
		keyCondition = expression.KeyEqual(expression.Key(PendingEmotesTypeReviewState), expression.Value(makeTypeReviewStateKey(filters.Type, filters.ReviewState)))
	} else if filters.Type != "" {
		// Only type is specified
		indexName = aws.String(PendingEmotesTypeGSIName)
		keyCondition = expression.KeyEqual(expression.Key(PendingEmotesType), expression.Value(filters.Type))
	} else if filters.ReviewState != "" {
		// Only review state is specified
		indexName = aws.String(PendingEmotesReviewStateGSIName)
		keyCondition = expression.KeyEqual(expression.Key(PendingEmotesReviewState), expression.Value(filters.ReviewState))
	} else {
		// No filters were specified. This is invalid input, and we should error out.
		return nil, errors.New("Failed to get pending emoticons with filters. At least one filter must be specified.")
	}

	// Add the createdAt sort key condition
	keyCondition = keyCondition.And(expression.KeyGreaterThan(expression.Key(PendingEmotesCreatedAt), expression.Value(filters.After)))

	// Build expression
	builder := expression.NewBuilder().WithKeyCondition(keyCondition)

	if filters.AssetType != "" {
		builder = builder.WithFilter(expression.Name(PendingEmotesAssetType).Equal(expression.Value(string(filters.AssetType))))
	}
	expr, err := builder.Build()
	if err != nil {
		return nil, err
	}

	queryInput := &dynamodb.QueryInput{
		TableName:                 aws.String(pe.tableName),
		IndexName:                 indexName,
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		FilterExpression:          expr.Filter(),
		KeyConditionExpression:    expr.KeyCondition(),
		ExclusiveStartKey:         startKey,
		Limit:                     aws.Int64(limit),
	}

	res, err := pe.db.Query(ctx, queryInput)
	if err != nil {
		return nil, err
	}

	items, err := pe.dynamoRespToPendingEmoticons(res.Items)
	if err != nil {
		return nil, err
	}

	return &GetPendingEmoticonsResponse{
		LastEvaluatedKey: res.LastEvaluatedKey,
		Items:            items,
	}, nil
}

// GetAllPendingEmoticons returns all the pending emoticons in the table.
// NOTE: This should NOT be used except for scripting and other non-production cases.
func (pe *pendingEmotesDBImpl) GetAllPendingEmoticons(ctx context.Context) ([]*PendingEmote, error) {
	scanInput := &dynamodb.ScanInput{
		TableName: aws.String(pe.tableName),
	}

	res, err := pe.db.Scan(ctx, scanInput)
	if err != nil {
		return nil, err
	}

	pendingEmoticons, err := pe.dynamoRespToPendingEmoticons(res.Items)
	if err != nil {
		return nil, err
	}

	// Continue scanning until we've exhausted the table
	for res.LastEvaluatedKey != nil {
		scanInput.ExclusiveStartKey = res.LastEvaluatedKey

		res, err = pe.db.Scan(ctx, scanInput)
		if err != nil {
			return nil, err
		}

		pendingEmoticonsToAppend, err := pe.dynamoRespToPendingEmoticons(res.Items)
		if err != nil {
			return nil, err
		}

		pendingEmoticons = append(pendingEmoticons, pendingEmoticonsToAppend...)
	}

	return pendingEmoticons, nil
}

func (pe pendingEmotesDBImpl) dynamoRespToPendingEmoticons(items []map[string]*dynamodb.AttributeValue) ([]*PendingEmote, error) {
	var pendingEmoticons []*PendingEmote
	err := dynamodbattribute.UnmarshalListOfMaps(items, &pendingEmoticons)
	if err != nil {
		return nil, errors.Wrap(err, "failed to unmarshal dynamo response")
	}
	return pendingEmoticons, nil
}

// helper func to convert dynamo attribute value map to pending emoticon item
func (pe pendingEmotesDBImpl) convertDynamoAttributesToPendingEmoticonItem(attributes map[string]*dynamodb.AttributeValue) (*PendingEmote, error) {
	var p PendingEmote
	err := dynamodbattribute.UnmarshalMap(attributes, &p)
	if err != nil {
		return nil, err
	}
	return &p, nil
}

func intMin(a, b int) int {
	if a < b {
		return a
	} else {
		return b
	}
}

func makeTypeReviewStateKey(accountType AccountType, reviewState PendingEmoteReviewState) string {
	return fmt.Sprintf("%s-%s", accountType, reviewState)
}
