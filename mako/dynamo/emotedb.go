package dynamo

import (
	"context"
	"github.com/hashicorp/go-multierror"
	"net/http"
	"os"
	"sync"
	"time"

	"code.justin.tv/commerce/gogogadget/pointers"
	mako "code.justin.tv/commerce/mako/internal"
	"github.com/afex/hystrix-go/hystrix"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
	"github.com/cenkalti/backoff"
	"github.com/pkg/errors"
	"github.com/segmentio/ksuid"
)

const (
	emotesId              = "id"
	emotesOwnerId         = "ownerId"
	emotesGroupId         = "groupId"
	emotesPrefix          = "prefix"
	emotesSuffix          = "suffix"
	emotesCode            = "code"
	emotesSymbolicMeaning = "symbolicMeaning"
	emotesState           = "state"
	emotesDomain          = "domain"
	emotesCreatedAt       = "createdAt"
	emotesUpdatedAt       = "updatedAt"
	emotesOrder           = "order"
	emotesGroup           = "emotesGroup"
	emotesAssetType       = "assetType"
	emotesImageSource     = "imageSource"

	// The maximum number of parallel calls we will make to EmoteDB for getting emotes
	concurrentWaitGroups = 4

	// maxGetEmoteBatchAttempts is the maximum number of times we want a BatchGetItem loop to attempt to process unprocessed keys
	maxGetEmoteBatchAttempts = 3

	// maxWriteEmotesBatchAttempts is the maximum number of times we want a BatchWriteItem loop to attempt to process unprocessed keys
	maxWriteEmotesBatchAttempts = 5

	// maxUpdateEmotesBatchAttempts is the maximum number of times we want a TransactWriteItem loop to attempt to commit the changes for a given batch
	maxUpdateEmotesBatchAttempts = 5

	HystrixNameDynamoEmotesGroupGet = "dynamoDB.emotes.group.GET"
	HystrixNameDynamoEmotesCodeGet  = "dynamoDB.emotes.code.GET"
)

var emotesOwnerIDIndex = "emotes_by_owner_id_gsi"
var emotesCodeIndex = "emotes_by_code_gsi"
var emotesGroupIDIndex = "emotes_by_group_id_gsi"

type emote struct {
	ID                string `dynamodbav:"id"`
	OwnerID           string `dynamodbav:"ownerId"`
	GroupID           string `dynamodbav:"groupId,omitempty"`
	Prefix            string `dynamodbav:"prefix"`
	Suffix            string `dynamodbav:"suffix"`
	Code              string `dynamodbav:"code"`
	State             string `dynamodbav:"state"`
	Domain            string `dynamodbav:"domain"`
	CreatedAt         int64  `dynamodbav:"createdAt"`
	UpdatedAt         int64  `dynamodbav:"updatedAt"`
	Order             int64  `dynamodbav:"order"`
	EmotesGroup       string `dynamodbav:"emotesGroup"`
	ProductID         string `dynamodbav:"productId"`
	AssetType         string `dynamodbav:"assetType"`
	AnimationTemplate string `dynamodbav:"animationTemplate"`
	ImageSource       string `dynamodbav:"imageSource"`
}

func (e emote) toMako() mako.Emote {
	return mako.Emote{
		ID:                e.ID,
		OwnerID:           e.OwnerID,
		GroupID:           e.GroupID,
		ProductID:         e.ProductID,
		EmotesGroup:       e.EmotesGroup,
		Prefix:            e.Prefix,
		Suffix:            e.Suffix,
		Code:              e.Code,
		State:             mako.EmoteState(e.State),
		Domain:            mako.Domain(e.Domain),
		CreatedAt:         time.Unix(0, e.CreatedAt),
		UpdatedAt:         time.Unix(0, e.UpdatedAt),
		Order:             e.Order,
		AssetType:         mako.EmoteAssetType(e.AssetType),
		AnimationTemplate: mako.AnimatedEmoteTemplate(e.AnimationTemplate),
	}
}

func convertEmote(e mako.Emote) emote {
	return emote{
		ID:                e.ID,
		OwnerID:           e.OwnerID,
		GroupID:           e.GroupID,
		ProductID:         e.ProductID,
		EmotesGroup:       e.EmotesGroup,
		Prefix:            e.Prefix,
		Suffix:            e.Suffix,
		Code:              e.Code,
		State:             string(e.State),
		Domain:            string(e.Domain),
		CreatedAt:         e.CreatedAt.UnixNano(),
		UpdatedAt:         e.UpdatedAt.UnixNano(),
		Order:             e.Order,
		AssetType:         string(e.AssetType),
		AnimationTemplate: string(e.AnimationTemplate),
		ImageSource:       string(e.ImageSource),
	}
}

type EmoteDBConfig struct {
	// DynamoDB table name.
	Table string
	// Region sets the AWS region where the GrantDB dynamo table is.
	Region string
	// Endpoint is used to connect to a local DynamoDB instance (for local
	// development or for testing).
	Endpoint string
	// Namespace is used for testing.
	Namespace string
}

type EmoteDB struct {
	table  *string
	client *dynamodb.DynamoDB
}

// NewEmoteDB implements the `mako.EmoteDB` interface using Dynamo to store emoticon metadata.
func NewEmoteDB(config EmoteDBConfig) *EmoteDB {
	sess := session.Must(session.NewSession())

	conf := aws.Config{
		Region: aws.String(config.Region),
		HTTPClient: &http.Client{
			Transport: &http.Transport{
				MaxIdleConnsPerHost: 10000,
			},
		},
	}

	// Endpoint is only used when bootstrapping tests. Local dynamodb requires static credentials
	// to work and they can be anything.
	if config.Endpoint != "" {
		conf.Credentials = credentials.NewStaticCredentials(config.Namespace, config.Namespace, "")
		conf.Endpoint = aws.String(config.Endpoint)
	}

	client := dynamodb.New(sess, &conf)

	return &EmoteDB{
		table:  &config.Table,
		client: client,
	}
}

func (db *EmoteDB) Create(ctx context.Context) error {
	_, err := db.client.CreateTableWithContext(ctx, &dynamodb.CreateTableInput{
		TableName: db.table,
		AttributeDefinitions: []*dynamodb.AttributeDefinition{
			{
				AttributeName: aws.String(emotesId),
				AttributeType: aws.String("S"),
			},
			{
				AttributeName: aws.String(emotesOwnerId),
				AttributeType: aws.String("S"),
			},
			{
				AttributeName: aws.String(emotesGroupId),
				AttributeType: aws.String("S"),
			},
			{
				AttributeName: aws.String(emotesCode),
				AttributeType: aws.String("S"),
			},
		},
		KeySchema: []*dynamodb.KeySchemaElement{
			{
				AttributeName: aws.String(emotesId),
				KeyType:       aws.String("HASH"),
			},
		},
		ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
			ReadCapacityUnits:  aws.Int64(10),
			WriteCapacityUnits: aws.Int64(10),
		},
		GlobalSecondaryIndexes: []*dynamodb.GlobalSecondaryIndex{
			{
				IndexName: aws.String(emotesCodeIndex),
				KeySchema: []*dynamodb.KeySchemaElement{
					{
						AttributeName: aws.String(emotesCode),
						KeyType:       aws.String("HASH"),
					},
				},
				ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
					ReadCapacityUnits:  aws.Int64(10),
					WriteCapacityUnits: aws.Int64(10),
				},
				Projection: &dynamodb.Projection{
					ProjectionType: aws.String("ALL"),
				},
			},

			{
				IndexName: aws.String(emotesOwnerIDIndex),
				KeySchema: []*dynamodb.KeySchemaElement{
					{
						AttributeName: aws.String(emotesOwnerId),
						KeyType:       aws.String("HASH"),
					},
				},
				ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
					ReadCapacityUnits:  aws.Int64(10),
					WriteCapacityUnits: aws.Int64(10),
				},
				Projection: &dynamodb.Projection{
					ProjectionType: aws.String("ALL"),
				},
			},

			{
				IndexName: aws.String(emotesGroupIDIndex),
				KeySchema: []*dynamodb.KeySchemaElement{
					{
						AttributeName: aws.String(emotesGroupId),
						KeyType:       aws.String("HASH"),
					},
					{
						AttributeName: aws.String(emotesOwnerId),
						KeyType:       aws.String("RANGE"),
					},
				},
				ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
					ReadCapacityUnits:  aws.Int64(10),
					WriteCapacityUnits: aws.Int64(10),
				},
				Projection: &dynamodb.Projection{
					ProjectionType: aws.String("ALL"),
				},
			},
		},
	})

	return err
}

func (db *EmoteDB) batchQuery(ctx context.Context, query func(key string) ([]mako.Emote, error), keys ...string) ([]mako.Emote, error) {
	var keysCh = make(chan string)
	var items = make(chan []mako.Emote, len(keys))
	var errs = make(chan error, len(keys))
	var wg sync.WaitGroup
	wg.Add(concurrentWaitGroups)

	for i := 0; i < concurrentWaitGroups; i++ {
		go func() {
			defer wg.Done()

			for {
				select {
				case <-ctx.Done():
				case key, ok := <-keysCh:
					if !ok {
						return
					}

					var res []mako.Emote
					if err := backoff.Retry(func() error {
						var err error
						res, err = query(key)
						return err
					}, backoff.WithMaxRetries(backoff.WithContext(newBackoff(), ctx), 1)); err != nil {
						errs <- err
						return
					}

					items <- res
				}
			}
		}()
	}

	for _, key := range keys {
		// Filter out empty keys.
		if key == "" {
			continue
		}

		keysCh <- key
	}

	close(keysCh)

	emotes := make([]mako.Emote, 0, len(keys))

	for range keys {
		select {
		case <-ctx.Done():
			return emotes, ctx.Err()
		case err := <-errs:
			return nil, err
		case items := <-items:
			emotes = append(emotes, items...)
		}
	}

	return emotes, nil
}

func (db *EmoteDB) ReadByGroupIDs(ctx context.Context, ids ...string) ([]mako.Emote, error) {
	var emotes []mako.Emote

	query := func(ctx context.Context) error {
		var err error

		emotes, err = db.batchQuery(ctx, func(key string) ([]mako.Emote, error) {
			return db.Query(ctx, mako.EmoteQuery{
				GroupID: key,
			})
		}, ids...)

		return err
	}

	if err := hystrix.DoC(ctx, HystrixNameDynamoEmotesGroupGet, query, nil); err != nil {
		return nil, err
	}

	return emotes, nil
}

func (db *EmoteDB) ReadByIDs(ctx context.Context, ids ...string) ([]mako.Emote, error) {
	result := make([]mako.Emote, 0, len(ids))

	for i := 0; i <= len(ids)/maxBatchGetItemSize; i++ {
		start := i * maxBatchGetItemSize
		end := start + maxBatchGetItemSize
		if end > len(ids) {
			end = len(ids)
		}

		batch := ids[start:end]

		if len(batch) == 0 {
			break
		}

		var keys []map[string]*dynamodb.AttributeValue
		for _, emoteID := range batch {
			keys = append(keys, map[string]*dynamodb.AttributeValue{
				emotesId: {
					S: aws.String(emoteID),
				},
			})
		}
		keysAndAttributes := map[string]*dynamodb.KeysAndAttributes{
			*db.table: {
				Keys: keys,
			},
		}

		numBatchGetItemAttempts := 0
		for len(keysAndAttributes) > 0 && numBatchGetItemAttempts < maxGetEmoteBatchAttempts {
			res, err := db.client.BatchGetItemWithContext(ctx, &dynamodb.BatchGetItemInput{
				RequestItems: keysAndAttributes,
			})
			if err != nil {
				return nil, errors.Wrap(err, "failed to read emotes by id")
			}

			for _, items := range res.Responses {
				var emotes []emote
				if err := dynamodbattribute.UnmarshalListOfMaps(items, &emotes); err != nil {
					return nil, errors.Wrap(err, "failed to unmarshal emotes dynamo response")
				}

				for _, emote := range emotes {
					result = append(result, emote.toMako())
				}
			}

			keysAndAttributes = res.UnprocessedKeys
			numBatchGetItemAttempts++
		}

		if len(keysAndAttributes) > 0 {
			return nil, errors.New("Failed to get all requested emotes by ids. Some keys were not processed after multiple attempts.")
		}
	}

	return result, nil
}

func (db *EmoteDB) ReadByCodes(ctx context.Context, codes ...string) ([]mako.Emote, error) {
	var emotes []mako.Emote

	query := func(ctx context.Context) error {
		var err error

		emotes, err = db.batchQuery(ctx, func(key string) ([]mako.Emote, error) {
			input := dynamodb.QueryInput{
				IndexName:              &emotesCodeIndex,
				TableName:              db.table,
				KeyConditionExpression: aws.String("code = :value"),
				ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
					":value": {
						S: aws.String(key),
					},
				},
			}

			res, err := db.client.QueryWithContext(ctx, &input)
			if err != nil {
				return nil, errors.Wrap(err, "failed to read emotes by code from dynamo")
			}

			if len(res.Items) == 0 {
				return nil, nil
			}

			var emotesRes []mako.Emote
			for _, item := range res.Items {
				var emote emote
				if err := dynamodbattribute.UnmarshalMap(item, &emote); err != nil {
					return nil, errors.Wrap(err, "failed to unmarshal emote attributes")
				}

				emotesRes = append(emotesRes, emote.toMako())
			}

			return emotesRes, nil
		}, codes...)

		return err
	}

	if err := hystrix.DoC(ctx, HystrixNameDynamoEmotesCodeGet, query, nil); err != nil {
		return nil, err
	}

	return emotes, nil
}

func (db *EmoteDB) DeleteEmote(ctx context.Context, id string) (mako.Emote, error) {
	deleteItemOutput, err := db.client.DeleteItemWithContext(ctx, &dynamodb.DeleteItemInput{
		TableName: db.table,
		Key: map[string]*dynamodb.AttributeValue{
			emotesId: {
				S: &id,
			},
		},
		ReturnValues: pointers.StringP("ALL_OLD"),
	})
	if err != nil {
		return mako.Emote{}, errors.Wrapf(err, "failed to delete emote '%s' from the database", id)
	}

	var emote emote
	if err := dynamodbattribute.UnmarshalMap(deleteItemOutput.Attributes, &emote); err != nil {
		return mako.Emote{}, errors.Wrap(err, "failed to unmarshal emote attributes during delete emote")
	}

	return emote.toMako(), nil
}

func (db *EmoteDB) UpdateCode(ctx context.Context, id, code, suffix string) (mako.Emote, error) {
	updateBuilder := expression.UpdateBuilder{}
	now := time.Now().UnixNano()

	updateBuilder = updateBuilder.Set(expression.Name(emotesCode), expression.Value(code))
	updateBuilder = updateBuilder.Set(expression.Name(emotesSuffix), expression.Value(suffix))
	updateBuilder = updateBuilder.Set(expression.Name(emotesUpdatedAt), expression.Value(now))

	condition := expression.Name(emotesId).AttributeExists()

	exp, err := expression.NewBuilder().WithUpdate(updateBuilder).WithCondition(condition).Build()
	if err != nil {
		return mako.Emote{}, errors.Wrapf(err, "failed to build update code expression for emote_id: %s", id)
	}

	updateItemOutput, err := db.client.UpdateItemWithContext(ctx, &dynamodb.UpdateItemInput{
		TableName: db.table,
		Key: map[string]*dynamodb.AttributeValue{
			emotesId: {
				S: &id,
			},
		},
		UpdateExpression:          exp.Update(),
		ExpressionAttributeNames:  exp.Names(),
		ExpressionAttributeValues: exp.Values(),
		ConditionExpression:       exp.Condition(),
		ReturnValues:              pointers.StringP("ALL_NEW"),
	})
	if err != nil {
		return mako.Emote{}, errors.Wrapf(err, "failed to write emote item to the database for emote_id: %s", id)
	}

	var emote emote
	if err := dynamodbattribute.UnmarshalMap(updateItemOutput.Attributes, &emote); err != nil {
		return mako.Emote{}, errors.Wrap(err, "failed to unmarshal emote attributes during update code")
	}

	return emote.toMako(), nil
}

func (db *EmoteDB) UpdatePrefix(ctx context.Context, id, code, prefix string) (mako.Emote, error) {
	updateBuilder := expression.UpdateBuilder{}
	now := time.Now().UnixNano()

	updateBuilder = updateBuilder.Set(expression.Name(emotesCode), expression.Value(code))
	if prefix == "" {
		updateBuilder = updateBuilder.Remove(expression.Name(emotesPrefix))
	} else {
		updateBuilder = updateBuilder.Set(expression.Name(emotesPrefix), expression.Value(prefix))
	}

	updateBuilder = updateBuilder.Set(expression.Name(emotesUpdatedAt), expression.Value(now))

	condition := expression.Name(emotesId).AttributeExists()

	exp, err := expression.NewBuilder().WithUpdate(updateBuilder).WithCondition(condition).Build()
	if err != nil {
		return mako.Emote{}, errors.Wrapf(err, "failed to build update emote prefix expression for emote_id: %v", id)
	}

	updateItemOutput, err := db.client.UpdateItemWithContext(ctx, &dynamodb.UpdateItemInput{
		TableName: db.table,
		Key: map[string]*dynamodb.AttributeValue{
			emotesId: {
				S: &id,
			},
		},
		UpdateExpression:          exp.Update(),
		ExpressionAttributeNames:  exp.Names(),
		ExpressionAttributeValues: exp.Values(),
		ConditionExpression:       exp.Condition(),
		ReturnValues:              pointers.StringP("ALL_NEW"),
	})
	if err != nil {
		return mako.Emote{}, errors.Wrapf(err, "failed to write emote item to the database for emote_id: %s", id)
	}

	var emote emote
	if err := dynamodbattribute.UnmarshalMap(updateItemOutput.Attributes, &emote); err != nil {
		return mako.Emote{}, errors.Wrap(err, "failed to unmarshal emote attributes during update prefix")
	}

	return emote.toMako(), nil
}

func (db *EmoteDB) UpdateState(ctx context.Context, id string, state mako.EmoteState) (mako.Emote, error) {
	updateBuilder := expression.UpdateBuilder{}
	now := time.Now().UnixNano()

	updateBuilder = updateBuilder.Set(expression.Name(emotesState), expression.Value(string(state)))
	updateBuilder = updateBuilder.Set(expression.Name(emotesUpdatedAt), expression.Value(now))

	condition := expression.Name(emotesId).AttributeExists()

	exp, err := expression.NewBuilder().WithUpdate(updateBuilder).WithCondition(condition).Build()
	if err != nil {
		return mako.Emote{}, errors.Wrapf(err, "failed to build update state expression for emote_id: %s", id)
	}

	updateItemOutput, err := db.client.UpdateItemWithContext(ctx, &dynamodb.UpdateItemInput{
		TableName: db.table,
		Key: map[string]*dynamodb.AttributeValue{
			emotesId: {
				S: &id,
			},
		},
		UpdateExpression:          exp.Update(),
		ExpressionAttributeNames:  exp.Names(),
		ExpressionAttributeValues: exp.Values(),
		ConditionExpression:       exp.Condition(),
		ReturnValues:              pointers.StringP("ALL_NEW"),
	})
	if err != nil {
		return mako.Emote{}, errors.Wrapf(err, "failed to write emote item to the database for emote_id: %s", id)
	}

	var emote emote
	if err := dynamodbattribute.UnmarshalMap(updateItemOutput.Attributes, &emote); err != nil {
		return mako.Emote{}, errors.Wrap(err, "failed to unmarshal emote attributes during update state")
	}

	return emote.toMako(), nil
}

func (db *EmoteDB) UpdateEmote(ctx context.Context, id string, params mako.EmoteUpdate) (mako.Emote, error) {
	exp, err := buildEmoteUpdateExpression(params)
	if err != nil {
		return mako.Emote{}, errors.Wrapf(err, "failed to build update emote expression for emote_id: %v", id)
	}

	resp, err := db.client.UpdateItemWithContext(ctx, &dynamodb.UpdateItemInput{
		TableName: db.table,
		Key: map[string]*dynamodb.AttributeValue{
			emotesId: {
				S: &id,
			},
		},
		UpdateExpression:          exp.Update(),
		ExpressionAttributeNames:  exp.Names(),
		ExpressionAttributeValues: exp.Values(),
		ConditionExpression:       exp.Condition(),
		ReturnValues:              aws.String(dynamodb.ReturnValueAllNew),
	})

	if err != nil {
		return mako.Emote{}, errors.Wrapf(err, "failed to write emote item to the database for emote_id: %s", id)
	}

	var updatedEmote emote
	if err := dynamodbattribute.UnmarshalMap(resp.Attributes, &updatedEmote); err != nil {
		return mako.Emote{}, errors.Wrapf(err, "error unmarshalling emote %v after update", id)
	}

	return updatedEmote.toMako(), nil
}

func (db *EmoteDB) UpdateEmotes(ctx context.Context, emoteUpdatesByID map[string]mako.EmoteUpdate) ([]mako.Emote, error) {
	// Get all the emotes we're about to update so we can provide the previous and current emote data upwards for cache-busting
	var ids []string
	for id := range emoteUpdatesByID {
		ids = append(ids, id)
	}
	existingEmotes, err := db.ReadByIDs(ctx, ids...)
	if err != nil {
		return nil, err
	}

	// Set up a list of all the request actions without accounting for buffer size
	var remainingItemRequests []*dynamodb.TransactWriteItem
	for id, emoteUpdate := range emoteUpdatesByID {
		exp, err := buildEmoteUpdateExpression(emoteUpdate)
		if err != nil {
			return nil, err
		}

		emoteID := id
		transactWriteItem := dynamodb.TransactWriteItem{
			Update: &dynamodb.Update{
				Key: map[string]*dynamodb.AttributeValue{
					emotesId: {
						S: &emoteID,
					},
				},
				ConditionExpression:       exp.Condition(),
				ExpressionAttributeNames:  exp.Names(),
				ExpressionAttributeValues: exp.Values(),
				UpdateExpression:          exp.Update(),
				TableName:                 db.table,
			},
		}
		remainingItemRequests = append(remainingItemRequests, &transactWriteItem)
	}

	requestItemBuffer := make([]*dynamodb.TransactWriteItem, 0, maxTransactWriteItemSize)

	// We will use TransactWriteItem for the updates of these emotes. The UpdateEmotes interface allows any number of
	// emotes to be passed in but the Dynamo API only allows a limited amount (25 as specified by the maxBatchWriteItemSize
	// const in our dynamo utils). Also, TransactWriteItem may return errors if it encounters issues with
	// dynamo write capacity throttling.

	// The strategy here is to burn down the remaining items 25 at a time and if any emotes are returned as not processed we
	// will retry them along with the next batch from remainingItems.

	noProgressRetries := 0
	var multiErr error
	for len(remainingItemRequests) > 0 && noProgressRetries < maxUpdateEmotesBatchAttempts {

		// Count for how many empty slots are left in the buffer
		availableBufferCapacity := maxTransactWriteItemSize - len(requestItemBuffer)

		// If the buffer isn't full, attempt to fill it with as many items as we have remaining
		if availableBufferCapacity > 0 {
			if availableBufferCapacity > len(remainingItemRequests) {
				// The buffer can fit all the remaining items, just fill it!
				requestItemBuffer = append(requestItemBuffer, remainingItemRequests...)
				remainingItemRequests = nil
			} else {
				// There are more remaining items than buffer capacity, fill the buffer up to its max capacity
				requestItemBuffer = append(requestItemBuffer, remainingItemRequests[:availableBufferCapacity]...)
				remainingItemRequests = remainingItemRequests[availableBufferCapacity:]
			}

			continue
		}

		// The buffer is full - send the batch to dynamo
		_, err := db.client.TransactWriteItemsWithContext(ctx, &dynamodb.TransactWriteItemsInput{
			TransactItems: requestItemBuffer,
		})
		if err != nil {
			// Dynamo Transactions either all fail or all succeed, so if there was an error, then we made no progress
			multiErr = multierror.Append(multiErr, errors.Wrap(err, "failed to update emote items in the database"))
			noProgressRetries++
			continue
		}

		// Create a new empty buffer
		requestItemBuffer = make([]*dynamodb.TransactWriteItem, 0, maxTransactWriteItemSize)
	}

	// We've added all of our items to the buffer but the buffer might still not be empty
	// Keep retrying until we exhaust the chosen number of retry attempts or until we've processed all emotes
	for len(requestItemBuffer) > 0 && noProgressRetries < maxUpdateEmotesBatchAttempts {
		_, err := db.client.TransactWriteItemsWithContext(ctx, &dynamodb.TransactWriteItemsInput{
			TransactItems: requestItemBuffer,
		})
		if err != nil {
			// Dynamo Transactions either all fail or all succeed, so if there was an error, then we made no progress
			multiErr = multierror.Append(multiErr, errors.Wrap(err, "failed to update emote items in the database after adding all items to the buffer"))
			noProgressRetries++
			continue
		}
		requestItemBuffer = []*dynamodb.TransactWriteItem{}
	}

	if len(requestItemBuffer) > 0 {
		return nil, multierror.Append(multiErr, errors.New("exhausted the allowed number of retries when UpdatingEmotes (check emoteDB dynamo write capacity)"))
	}

	// Return both the updated emotes and the original emotes before the updates for cache-busting purposes
	emotesToExpire := append(existingEmotes, simulateEmoteUpdates(existingEmotes, emoteUpdatesByID)...)
	return emotesToExpire, nil
}

func (db *EmoteDB) WriteEmotes(ctx context.Context, emotes ...mako.Emote) ([]mako.Emote, error) {
	requestItemBuffer := make([]*dynamodb.WriteRequest, 0, maxBatchWriteItemSize)

	// Build the WriteItemRequests
	remainingItemRequests := make([]*dynamodb.WriteRequest, len(emotes))
	for i, emote := range emotes {
		item, err := dynamodbattribute.MarshalMap(convertEmote(emote))
		if err != nil {
			return nil, errors.Wrap(err, "failed to marshal emote item")
		}
		remainingItemRequests[i] = &dynamodb.WriteRequest{
			PutRequest: &dynamodb.PutRequest{
				Item: item,
			},
		}
	}

	// We will use BatchWriteItem for the writes of these emotes. The WriteEmotes interface allows any number of
	// emotes to be passed in but the Dynamo API only allows a limited amount (25 as specified by the maxBatchWriteItemSize
	// const in our dynamo utils). Also, BatchWriteItem may return some UnprocessedItems if it encounters issues with
	// dynamo write capacity throttling.
	// The strategy here is to burn down the remaining items 25 at a time and if any emotes are returned as not processed we
	// will retry them along with the next batch from remainingItems.

	noProgressRetries := 0
	for len(remainingItemRequests) > 0 && noProgressRetries < maxWriteEmotesBatchAttempts {

		// Count for how many empty slots are left in the buffer
		availableBufferCapacity := maxBatchWriteItemSize - len(requestItemBuffer)

		// If the buffer isn't full, attempt to fill it with as many items as we have remaining
		if availableBufferCapacity > 0 {
			if availableBufferCapacity > len(remainingItemRequests) {
				// The buffer can fit all of the remaining items, just fill it!
				requestItemBuffer = append(requestItemBuffer, remainingItemRequests...)
				remainingItemRequests = nil
			} else {
				// There are more remaining items than buffer capacity, fill the buffer up to its max capacity
				requestItemBuffer = append(requestItemBuffer, remainingItemRequests[:availableBufferCapacity]...)
				remainingItemRequests = remainingItemRequests[availableBufferCapacity:]
			}

			continue
		}

		// The buffer is full - send the batch to dynamo
		resp, err := db.client.BatchWriteItemWithContext(ctx, &dynamodb.BatchWriteItemInput{
			RequestItems: map[string][]*dynamodb.WriteRequest{
				*db.table: requestItemBuffer,
			},
		})
		if err != nil {
			return nil, errors.Wrap(err, "failed to write emote items to the database")
		}

		unprocessedItems := resp.UnprocessedItems[*db.table]

		// If dynamo returned the same number of unprocessed items as we had in the request then none of the items were processed,
		// we should keep the same input buffer and increment our noProgressRetries counter so that we don't spin endlessly if dynamo
		// is never returning success
		if len(unprocessedItems) == len(requestItemBuffer) {
			noProgressRetries++
			continue
		}

		// Create a new empty buffer
		requestItemBuffer = make([]*dynamodb.WriteRequest, 0, maxBatchWriteItemSize)
		// If we have any unprocessed items from the dynamo call add them straight onto the buffer
		if len(unprocessedItems) > 0 {
			requestItemBuffer = append(requestItemBuffer, unprocessedItems...)
		}
	}

	// We've added all of our items to the buffer but the buffer might still not be empty
	// Keep retrying until we exhaust the chosen number of retry attempts or until we've processed all emotes
	for len(requestItemBuffer) > 0 && noProgressRetries < maxWriteEmotesBatchAttempts {
		noProgressRetries++
		resp, err := db.client.BatchWriteItemWithContext(ctx, &dynamodb.BatchWriteItemInput{
			RequestItems: map[string][]*dynamodb.WriteRequest{
				*db.table: requestItemBuffer,
			},
		})
		if err != nil {
			return nil, errors.Wrap(err, "failed to write emote items to the database")
		}

		// Replace the buffer with any unprocessed items (if they exist)
		requestItemBuffer = resp.UnprocessedItems[*db.table]
	}

	if len(requestItemBuffer) > 0 {
		return nil, errors.New("exhausted the allowed number of retries when BatchWriting emotes (check emoteDB dynamo write capacity)")
	}

	return emotes, nil
}

func (db *EmoteDB) Query(ctx context.Context, query mako.EmoteQuery) ([]mako.Emote, error) {
	emotes := make([]mako.Emote, 0)

	var index string
	var filter *expression.ConditionBuilder
	var key expression.KeyConditionBuilder

	if query.OwnerID != "" && query.GroupID != "" {
		index = emotesGroupIDIndex
		key = expression.KeyEqual(expression.Key(emotesGroupId), expression.Value(query.GroupID)).
			And(expression.Key(emotesOwnerId).Equal(expression.Value(query.OwnerID)))
	} else if query.OwnerID != "" {
		key = expression.KeyEqual(expression.Key(emotesOwnerId), expression.Value(query.OwnerID))
		index = emotesOwnerIDIndex
	} else if query.Code != "" {
		index = emotesCodeIndex
		key = expression.KeyEqual(expression.Key(emotesCode), expression.Value(query.Code))
	} else if query.GroupID != "" {
		index = emotesGroupIDIndex
		key = expression.KeyEqual(expression.Key(emotesGroupId), expression.Value(query.GroupID))
	}

	// Double-check an index has been picked before moving on.
	if index == "" {
		return nil, errors.New("failed to find an index for emote query")
	}

	createFilter := func(nextFilter expression.ConditionBuilder) {
		if filter != nil {
			combinedFilter := filter.And(nextFilter)
			filter = &combinedFilter
		} else {
			filter = &nextFilter
		}
	}

	// Build any of the filters needed.
	if query.EmoteGroup != "" {
		createFilter(expression.Name(emotesGroup).Equal(expression.Value(query.EmoteGroup)))
	}

	if query.Domain != "" {
		createFilter(expression.Name(emotesDomain).Equal(expression.Value(query.Domain)))
	}

	if query.AssetType != "" {
		createFilter(expression.Name(emotesAssetType).Equal(expression.Value(query.AssetType)))
	}

	if len(query.States) != 0 {
		var filters []expression.ConditionBuilder
		for _, state := range query.States {
			expr := expression.Name(emotesState).Equal(expression.Value(string(state)))
			filters = append(filters, expr)
		}

		var base expression.ConditionBuilder
		base, filters = filters[0], filters[1:]
		for i := 0; i < len(filters); i++ {
			base = base.Or(filters[i])
		}

		createFilter(base)
	}

	builder := expression.NewBuilder().WithKeyCondition(key)
	if filter != nil {
		builder = builder.WithFilter(*filter)
	}

	expr, err := builder.Build()
	if err != nil {
		return nil, errors.Wrap(err, "failed to build dynamodb expression")
	}

	input := dynamodb.QueryInput{
		IndexName:                 &index,
		TableName:                 db.table,
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		KeyConditionExpression:    expr.KeyCondition(),
		FilterExpression:          expr.Filter(),
	}

	if err := db.client.QueryPagesWithContext(ctx, &input, func(out *dynamodb.QueryOutput, lastPage bool) bool {
		for _, item := range out.Items {
			var emote emote
			if err := dynamodbattribute.UnmarshalMap(item, &emote); err != nil {
				continue
			}

			emotes = append(emotes, emote.toMako())
		}

		return !lastPage
	}); err != nil {
		return nil, errors.Wrap(err, "failed to do dynamodb Query() on emotes")
	}

	return emotes, nil
}

func (db *EmoteDB) Close() error {
	return nil
}

func MakeEmoteDB(ctx context.Context, emotes ...mako.Emote) (db mako.EmoteDB, teardown func(), err error) {
	addr := "http://localhost:8005"

	hystrix.ConfigureCommand(HystrixNameDynamoEmotesCodeGet, hystrix.CommandConfig{
		Timeout:               10000,
		MaxConcurrentRequests: 1000,
	})

	hystrix.ConfigureCommand(HystrixNameDynamoEmotesGroupGet, hystrix.CommandConfig{
		Timeout:               10000,
		MaxConcurrentRequests: 1000,
	})

	// Jenkins require a different dynamodb address for tests.
	if os.Getenv("DOCKER_TEST") != "" {
		addr = "http://jenkins_dynamodb:8000"
	}

	emoteDB := NewEmoteDB(EmoteDBConfig{
		Region:    "us-west-2",
		Namespace: ksuid.New().String(),
		Endpoint:  addr,
		Table:     "grants",
	})

	if err = emoteDB.Create(ctx); err != nil {
		if awsErr, ok := err.(awserr.Error); ok {
			// Ignore error if table has already been created
			if awsErr.Code() != "ResourceInUseException" {
				return
			}
		}
	}

	_, err = emoteDB.WriteEmotes(ctx, emotes...)
	if err != nil {
		return
	}

	db = emoteDB
	teardown = func() {}
	return
}

func newBackoff() backoff.BackOff {
	exponential := backoff.NewExponentialBackOff()
	exponential.InitialInterval = 10 * time.Millisecond
	exponential.MaxElapsedTime = 1 * time.Second
	exponential.Multiplier = 2
	return exponential
}

func buildEmoteUpdateExpression(emoteUpdate mako.EmoteUpdate) (expression.Expression, error) {
	updateBuilder := expression.UpdateBuilder{}
	now := time.Now().UnixNano()

	numExpressions := 0

	if emoteUpdate.GroupID != nil {
		groupID := *emoteUpdate.GroupID
		if groupID == "" {
			updateBuilder = updateBuilder.Remove(expression.Name(emotesGroupId))
		} else {
			updateBuilder = updateBuilder.Set(expression.Name(emotesGroupId), expression.Value(groupID))
		}
		numExpressions++
	}

	if emoteUpdate.State != nil {
		state := *emoteUpdate.State
		updateBuilder = updateBuilder.Set(expression.Name(emotesState), expression.Value(string(state)))
		numExpressions++
	}

	if emoteUpdate.Order != nil {
		order := *emoteUpdate.Order
		updateBuilder = updateBuilder.Set(expression.Name(emotesOrder), expression.Value(order))
		numExpressions++
	}

	if emoteUpdate.EmotesGroup != nil {
		group := *emoteUpdate.EmotesGroup
		updateBuilder = updateBuilder.Set(expression.Name(emotesGroup), expression.Value(group))
		numExpressions++
	}

	if emoteUpdate.ImageSource != nil {
		imageSource := *emoteUpdate.ImageSource
		updateBuilder = updateBuilder.Set(expression.Name(emotesImageSource), expression.Value(imageSource))
		numExpressions++
	}

	if numExpressions < 1 {
		return expression.Expression{}, errors.New("cannot update emote due to unspecified update parameters")
	}

	updateBuilder = updateBuilder.Set(expression.Name(emotesUpdatedAt), expression.Value(now))

	condition := expression.Name(emotesId).AttributeExists()

	builder := expression.NewBuilder()
	builtExpression, err := builder.WithCondition(condition).WithUpdate(updateBuilder).Build()
	if err != nil {
		return expression.Expression{}, err
	}

	return builtExpression, nil
}

func simulateEmoteUpdates(existingEmotes []mako.Emote, emoteUpdates map[string]mako.EmoteUpdate) []mako.Emote {
	var updatedEmotes []mako.Emote
	for _, emote := range existingEmotes {
		emoteUpdate := emoteUpdates[emote.ID]
		updatedEmote := emote

		if emoteUpdate.EmotesGroup != nil && *emoteUpdate.EmotesGroup != updatedEmote.EmotesGroup {
			updatedEmote.EmotesGroup = *emoteUpdate.EmotesGroup
		}

		if emoteUpdate.GroupID != nil && *emoteUpdate.GroupID != updatedEmote.GroupID {
			updatedEmote.GroupID = *emoteUpdate.GroupID
		}

		if emoteUpdate.State != nil && *emoteUpdate.State != updatedEmote.State {
			updatedEmote.State = *emoteUpdate.State
		}

		if emoteUpdate.Order != nil && *emoteUpdate.Order != updatedEmote.Order {
			updatedEmote.Order = *emoteUpdate.Order
		}

		if emoteUpdate.ImageSource != nil && *emoteUpdate.ImageSource != updatedEmote.ImageSource {
			updatedEmote.ImageSource = *emoteUpdate.ImageSource
		}

		updatedEmotes = append(updatedEmotes, updatedEmote)
	}
	return updatedEmotes
}
