package dynamo

import (
	"context"
	"errors"
	"fmt"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/common/godynamo/client"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

/* In this dynamo table I'm intentionally making updates to existing users more involved so that we can use dynamo to
 * prevent duplicate prefixes.
 */

var PrefixAlreadyTaken = errors.New("Emoticon prefix is already taken")

const (
	PrefixesOwnerIdParam = ":" + PrefixesOwnerIdColumn
)

type prefixDao struct {
	table  client.DynamoTable
	client *client.DynamoClient
}

func NewPrefixDao(client *client.DynamoClient) IPrefixDao {
	return &prefixDao{
		table:  &PrefixesTable{},
		client: client,
	}
}

type IPrefixDao interface {
	GetByOwnerId(ctx context.Context, ownerId string) (*Prefix, error)
	Get(ctx context.Context, prefix string) (*Prefix, error)
	GetBatch(ctx context.Context, prefixes ...string) (map[string]Prefix, error)
	GetByState(ctx context.Context, state PrefixState, limit int) ([]*Prefix, error)
	Put(ctx context.Context, ownerId, prefix string, state PrefixState) (*Prefix, error)
	Delete(ctx context.Context, prefix string, ownerId string) error
}

func (p *prefixDao) GetByOwnerId(ctx context.Context, ownerId string) (*Prefix, error) {
	input := &dynamodb.QueryInput{
		TableName:              aws.String(p.client.GetFullTableName(p.table)),
		KeyConditionExpression: aws.String("#ownerId = :ownerId"),
		ExpressionAttributeNames: map[string]*string{
			"#ownerId": aws.String(PrefixesOwnerIdColumn),
		},
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":ownerId": {
				S: aws.String(ownerId),
			},
		},
		IndexName: &PrefixesOwnerIdGsi,
	}

	resp, err := p.client.Dynamo.QueryWithContext(ctx, input)
	if err != nil {
		return nil, err
	}

	if resp == nil || resp.Items == nil || len(resp.Items) == 0 {
		return nil, nil // doesn't exist, that's ok
	}

	if len(resp.Items) > 1 {
		p.logDuplicatePrefixes(ownerId, resp.Items)
		return nil, errors.New("multiple prefixes set for channel")
	}

	record, err := p.table.ConvertAttributeMapToRecord(resp.Items[0])
	if err != nil {
		return nil, err
	}
	return record.(*Prefix), nil
}

func (p *prefixDao) logDuplicatePrefixes(ownerId string, resp []map[string]*dynamodb.AttributeValue) {
	prefixes := make([]string, len(resp))
	for i := range resp {
		record, _ := p.table.ConvertAttributeMapToRecord(resp[i])
		prefixes[i] = record.(*Prefix).Prefix
	}
	log.WithFields(log.Fields{
		"channel_id": ownerId,
		"prefixes":   prefixes,
	}).Error("Multiple prefixes set for channel")
}

func (p *prefixDao) GetBatch(ctx context.Context, prefixes ...string) (map[string]Prefix, error) {
	var keys []map[string]*dynamodb.AttributeValue
	for _, prefix := range prefixes {
		keys = append(keys, map[string]*dynamodb.AttributeValue{
			PrefixesPrefixColumn: {
				S: aws.String(prefix),
			},
		})
	}
	keysAndAttributes := map[string]*dynamodb.KeysAndAttributes{
		p.client.GetFullTableName(p.table): {
			Keys: keys,
		},
	}

	input := &dynamodb.BatchGetItemInput{
		RequestItems: keysAndAttributes,
	}

	result, err := p.client.Dynamo.BatchGetItemWithContext(ctx, input)
	if err != nil {
		return nil, err
	}

	records := map[string]Prefix{}

	for _, dynamoPrefixes := range result.Responses {
		for _, dynamoPrefix := range dynamoPrefixes {
			record, err := p.table.ConvertAttributeMapToRecord(dynamoPrefix)
			if err != nil {
				return nil, err
			}
			if record != nil {
				prefix := *record.(*Prefix)
				records[prefix.Prefix] = prefix
			}
		}
	}

	return records, nil
}

func (p *prefixDao) Get(ctx context.Context, prefix string) (*Prefix, error) {
	getItemInput := &dynamodb.GetItemInput{
		TableName: aws.String(p.client.GetFullTableName(p.table)),
		Key:       makeSingleStringMap(PrefixesPrefixColumn, prefix),
	}

	result, err := p.client.Dynamo.GetItemWithContext(ctx, getItemInput)
	if err != nil {
		return nil, err
	} else if result == nil || result.Item == nil || len(result.Item) == 0 {
		return nil, nil //doesn't exist, that's ok
	}

	record, err := p.table.ConvertAttributeMapToRecord(result.Item)
	if err != nil {
		return nil, err
	}

	return record.(*Prefix), nil
}

func (p *prefixDao) Put(ctx context.Context, ownerId, prefix string, state PrefixState) (*Prefix, error) {
	newPrefix := &Prefix{
		OwnerId:     ownerId,
		Prefix:      prefix,
		State:       state,
		LastUpdated: time.Now(),
	}

	// verify that no one else is already using the input prefix
	existingPrefix, err := p.Get(ctx, prefix)
	if err != nil {
		return nil, err
	}

	if existingPrefix != nil && existingPrefix.OwnerId != newPrefix.OwnerId {
		return nil, PrefixAlreadyTaken
	}

	currentOwnerPrefix, err := p.GetByOwnerId(ctx, ownerId)
	if err != nil {
		return nil, err
	}

	transactionItems := make([]*dynamodb.TransactWriteItem, 0)
	// delete this owner's old entry if there is one and it is a different prefix
	if currentOwnerPrefix != nil && currentOwnerPrefix.Prefix != prefix {

		transactionItems = append(transactionItems, &dynamodb.TransactWriteItem{
			Delete: &dynamodb.Delete{
				ConditionExpression: aws.String(fmt.Sprintf("%s = %s", PrefixesOwnerIdColumn, PrefixesOwnerIdParam)),
				ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
					PrefixesOwnerIdParam: {S: aws.String(ownerId)},
				},
				Key:       makeSingleStringMap(PrefixesPrefixColumn, currentOwnerPrefix.Prefix),
				TableName: aws.String(p.client.GetFullTableName(p.table)),
			},
		})

		if currentOwnerPrefix.Prefix == newPrefix.Prefix && currentOwnerPrefix.State == newPrefix.State {
			newPrefix.LastUpdated = currentOwnerPrefix.LastUpdated
		}
	}

	// put the new prefix into the table. allow it to overwrite the existing entry, because at this point we know
	// that the entry is owned by the input ownerId, so we can safely do an update in place here.
	transactionItems = append(transactionItems, &dynamodb.TransactWriteItem{
		Put: &dynamodb.Put{
			Item:      newPrefix.NewAttributeMap(),
			TableName: aws.String(p.client.GetFullTableName(p.table)),
		},
	})

	_, err = p.client.Dynamo.TransactWriteItemsWithContext(ctx, &dynamodb.TransactWriteItemsInput{TransactItems: transactionItems})
	return currentOwnerPrefix, err
}

func (p *prefixDao) GetByState(ctx context.Context, state PrefixState, limit int) ([]*Prefix, error) {
	queryInput := &dynamodb.QueryInput{
		TableName:              aws.String(p.client.GetFullTableName(p.table)),
		IndexName:              &PrefixesStateOwnerIdGsi,
		KeyConditionExpression: aws.String("#state = :state"),
		ExpressionAttributeNames: map[string]*string{
			"#state": aws.String(PrefixesStateColumn),
		},
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":state": {
				S: aws.String(string(state)),
			},
		},
		Limit: aws.Int64(int64(limit)),
	}

	resp, err := p.client.Dynamo.QueryWithContext(ctx, queryInput)
	if err != nil {
		return nil, err
	}

	if resp == nil || len(resp.Items) == 0 {
		return nil, nil // doesn't exist, that's ok
	}

	records := make([]*Prefix, 0, len(resp.Items))

	for i := 0; i < len(resp.Items) && i < limit; i++ {
		record, err := p.table.ConvertAttributeMapToRecord(resp.Items[i])

		if err != nil {
			return nil, err
		}
		records = append(records, record.(*Prefix))
	}

	return records, nil
}

func (p *prefixDao) Delete(ctx context.Context, prefix string, ownerId string) error {
	_, err := p.client.Dynamo.DeleteItemWithContext(ctx, &dynamodb.DeleteItemInput{
		TableName:           aws.String(p.client.GetFullTableName(p.table)),
		Key:                 makeSingleStringMap(PrefixesPrefixColumn, prefix),
		ConditionExpression: aws.String(fmt.Sprintf("%s = %s", PrefixesOwnerIdColumn, PrefixesOwnerIdParam)),
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			PrefixesOwnerIdParam: {S: aws.String(ownerId)},
		},
	})

	if err != nil {
		return err
	}

	return nil
}

func makeSingleStringMap(key, value string) map[string]*dynamodb.AttributeValue {
	itemMap := make(map[string]*dynamodb.AttributeValue)
	client.PopulateAttributesString(itemMap, key, value)
	return itemMap
}
