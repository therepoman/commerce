package dynamo

import (
	"testing"

	"code.justin.tv/commerce/mako/tests"
)

func TestDynamoEmoteStandingOverridesDB(t *testing.T) {
	tests.TestEmoteStandingOverridesDB(t, MakeEmoteStandingOverridesDBForTesting)
}
