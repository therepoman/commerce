package dynamo

import (
	"code.justin.tv/common/godynamo/client"
	"github.com/pkg/errors"
)

type settingDao struct {
	table  client.DynamoTable
	client *client.DynamoClient
}

func NewSettingsDao(client *client.DynamoClient) ISettingsDao {
	return &settingDao{
		table:  &SettingsTable{},
		client: client,
	}
}

type ISettingsDao interface {
	Get(ownerId string) (map[string]string, error)
	Put(ownerId, name, value string) error
}

func (s *settingDao) Get(ownerId string) (map[string]string, error) {
	record, err := s.client.GetItem(&Settings{OwnerId: ownerId})
	if err != nil {
		return nil, err
	}

	settings := record.(*Settings)
	if settings == nil {
		return nil, errors.New("could not cast settings")
	}

	return settings.Settings, nil
}

func (s *settingDao) Put(ownerId, name, value string) error {
	//TODO: concurrency safety, need to lock on record.

	settings, err := s.Get(ownerId)
	if err != nil {
		return err
	}

	settings[name] = value
	return s.client.PutItem(&Settings{
		OwnerId:  ownerId,
		Settings: settings,
	})
}
