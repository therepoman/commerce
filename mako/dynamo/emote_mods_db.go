package dynamo

import (
	"context"
	"fmt"
	"os"
	"sync"

	mako "code.justin.tv/commerce/mako/internal"
	"github.com/afex/hystrix-go/hystrix"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
	"github.com/cenkalti/backoff"
	"github.com/pkg/errors"
	"github.com/segmentio/ksuid"
)

const (
	emoteModGroupsTable              = "emote_modifier_groups"
	emoteModGroupsOwnerIndex         = "emote_modifier_groups_index_owner_id"
	emoteModGroupHashKey             = "id"
	emoteModGroupOwnerID             = "ownerId"
	emoteModGroupModifiers           = "modifiers"
	emoteModGroupSourceEmoteGroupIDs = "sourceEmoteGroupIds"

	HystrixNameDynamoEmoteModifierGroupsGet = "dynamoDB.emote_modifier_groups.owner.GET"
)

type emoteModifierGroup struct {
	ID                  string   `dynamodbav:"id"`
	OwnerID             string   `dynamodbav:"ownerId"`
	Modifiers           []string `dynamodbav:"modifiers"`
	SourceEmoteGroupIDs []string `dynamodbav:"sourceEmoteGroupIds"`
}

func (g emoteModifierGroup) toMako() mako.EmoteModifierGroup {
	return mako.EmoteModifierGroup{
		ID:                  g.ID,
		OwnerID:             g.OwnerID,
		Modifiers:           g.Modifiers,
		SourceEmoteGroupIDs: g.SourceEmoteGroupIDs,
	}
}

func toDynamoEmoteModifierGroup(group mako.EmoteModifierGroup) emoteModifierGroup {
	return emoteModifierGroup{
		ID:                  group.ID,
		OwnerID:             group.OwnerID,
		Modifiers:           group.Modifiers,
		SourceEmoteGroupIDs: group.SourceEmoteGroupIDs,
	}
}

type EmoteModifiersDB struct {
	table  *string
	client *dynamodb.DynamoDB
}

type EmoteModifierGroupDBConfig struct {
	// The name of the table in dynamo
	Table string
	// The AWS region to connect to
	Region string
	// Used to connect to local dynamodb for testing
	Endpoint string
	// Used to scope data in local dynamodb for testing
	Namespace string
}

func NewEmoteModifierGroupDB(config EmoteModifierGroupDBConfig) *EmoteModifiersDB {
	sess := session.Must(session.NewSession())
	conf := aws.Config{
		Region: aws.String(config.Region),
	}

	// Endpoint is only used when bootstrapping tests, local dynamodb requires static credentials.
	if config.Endpoint != "" {
		conf.Credentials = credentials.NewStaticCredentials(config.Namespace, config.Namespace, "")
		conf.Endpoint = aws.String(config.Endpoint)
	}

	client := dynamodb.New(sess, &conf)

	return &EmoteModifiersDB{
		table:  aws.String(config.Table),
		client: client,
	}
}

func (db *EmoteModifiersDB) create(ctx context.Context) error {
	_, err := db.client.CreateTableWithContext(ctx, &dynamodb.CreateTableInput{
		AttributeDefinitions: []*dynamodb.AttributeDefinition{
			{
				AttributeName: aws.String(emoteModGroupHashKey),
				AttributeType: aws.String("S"),
			},
			{
				AttributeName: aws.String(emoteModGroupOwnerID),
				AttributeType: aws.String("S"),
			},
		},
		GlobalSecondaryIndexes: []*dynamodb.GlobalSecondaryIndex{
			{
				IndexName: aws.String(emoteModGroupsOwnerIndex),
				KeySchema: []*dynamodb.KeySchemaElement{
					{
						AttributeName: aws.String(emoteModGroupOwnerID),
						KeyType:       aws.String("HASH"),
					},
				},
				Projection: &dynamodb.Projection{
					ProjectionType: aws.String(dynamodb.ProjectionTypeAll),
				},
				ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
					ReadCapacityUnits:  aws.Int64(500),
					WriteCapacityUnits: aws.Int64(500),
				},
			},
		},
		KeySchema: []*dynamodb.KeySchemaElement{
			{
				AttributeName: aws.String(emoteModGroupHashKey),
				KeyType:       aws.String("HASH"),
			},
		},
		ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
			ReadCapacityUnits:  aws.Int64(500),
			WriteCapacityUnits: aws.Int64(500),
		},
		TableName: db.table,
	})
	if err != nil {
		return err
	}

	return nil
}

func (db *EmoteModifiersDB) CreateGroups(ctx context.Context, groups ...mako.EmoteModifierGroup) ([]mako.EmoteModifierGroup, error) {
	result := make([]mako.EmoteModifierGroup, 0, len(groups))
	for _, group := range groups {
		group := group

		result = append(result, group)

		item, err := dynamodbattribute.MarshalMap(toDynamoEmoteModifierGroup(group))
		if err != nil {
			return nil, errors.Wrap(err, "failed to marshal emote modifier group")
		}

		if _, err := db.client.PutItemWithContext(ctx, &dynamodb.PutItemInput{
			TableName: db.table,
			Item:      item,
		}); err != nil {
			return nil, errors.Wrap(err, "failed to write emote modifier group to the database")
		}
	}

	return result, nil
}

func (db *EmoteModifiersDB) UpdateGroups(ctx context.Context, groups ...mako.EmoteModifierGroup) ([]mako.EmoteModifierGroup, error) {
	result := make([]mako.EmoteModifierGroup, 0, len(groups))
	for _, group := range groups {

		updater := buildEmoteGroupUpdateExpressionBuilder(group)
		expr, err := expression.NewBuilder().WithUpdate(updater).Build()
		if err != nil {
			return nil, errors.Wrap(err, "building expression for update")
		}

		input := dynamodb.UpdateItemInput{
			ExpressionAttributeNames:  expr.Names(),
			ExpressionAttributeValues: expr.Values(),
			Key: map[string]*dynamodb.AttributeValue{
				emoteModGroupHashKey: {
					S: aws.String(group.ID),
				},
			},
			UpdateExpression: expr.Update(),
			ReturnValues:     aws.String(dynamodb.ReturnValueAllNew),
			TableName:        db.table,
		}

		resp, err := db.client.UpdateItemWithContext(ctx, &input)
		if err != nil {
			return nil, errors.Wrapf(err, "updating emote mod group %v", group.ID)
		}

		var found emoteModifierGroup
		if err := dynamodbattribute.UnmarshalMap(resp.Attributes, &found); err != nil {
			return nil, errors.Wrapf(err, "unmarshalling emote modifier group %v after update", group.ID)
		}
		result = append(result, found.toMako())
	}

	return result, nil
}

func buildEmoteGroupUpdateExpressionBuilder(group mako.EmoteModifierGroup) expression.UpdateBuilder {
	// The primary use-case for updating this table will be to change the emote modifiers, and we
	// need to be able to support clearing them out entirely. So always set those.
	update := expression.Set(
		expression.Name(emoteModGroupModifiers),
		expression.Value(group.Modifiers),
	)

	// The other items are optional, update them if they are included in the request.
	// A side effect of this is that it's not possible to delete an ownerID or list of source emote group IDs
	// by providing an empty or list in the request, but that's OK since these should essentially always be populated,
	// and their values shouldn't change once initially created (assuming we created them correctly)
	if group.OwnerID != "" {
		update.Set(
			expression.Name(emoteModGroupOwnerID),
			expression.Value(group.OwnerID),
		)
	}
	if len(group.SourceEmoteGroupIDs) > 0 {
		update.Set(
			expression.Name(emoteModGroupSourceEmoteGroupIDs),
			expression.Value(group.SourceEmoteGroupIDs),
		)
	}

	return update
}

func (db *EmoteModifiersDB) ReadByIDs(ctx context.Context, ids ...string) ([]mako.EmoteModifierGroup, error) {
	keys := []map[string]*dynamodb.AttributeValue{}
	groups := make([]mako.EmoteModifierGroup, 0, len(ids))

	for _, id := range ids {
		keys = append(keys, map[string]*dynamodb.AttributeValue{
			emoteModGroupHashKey: {
				S: aws.String(id),
			},
		})
	}

	res, err := db.client.BatchGetItemWithContext(ctx, &dynamodb.BatchGetItemInput{
		RequestItems: map[string]*dynamodb.KeysAndAttributes{
			*db.table: {
				Keys: keys,
			},
		},
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to read emote mod groups by ID")
	}

	for _, items := range res.Responses {
		for _, item := range items {
			var group emoteModifierGroup
			if err := dynamodbattribute.UnmarshalMap(item, &group); err != nil {
				return nil, errors.Wrap(err, "failed to unmarshal emote mod group")
			}

			groups = append(groups, group.toMako())
		}
	}

	return groups, nil
}

func (db *EmoteModifiersDB) ReadByOwnerIDs(ctx context.Context, ids ...string) ([]mako.EmoteModifierGroup, error) {
	if len(ids) == 0 {
		return nil, errors.New("empty ownerID list given")
	}

	var groups []mako.EmoteModifierGroup

	query := func(ctx context.Context) error {
		var err error

		groups, err = db.batchQuery(ctx, func(key string) ([]mako.EmoteModifierGroup, error) {
			return db.ReadByOwnerID(ctx, key)
		}, ids...)

		return err
	}

	if err := hystrix.DoC(ctx, HystrixNameDynamoEmoteModifierGroupsGet, query, nil); err != nil {
		return nil, err
	}

	return groups, nil
}

func (db *EmoteModifiersDB) ReadByOwnerID(ctx context.Context, ownerID string) ([]mako.EmoteModifierGroup, error) {
	var groups []mako.EmoteModifierGroup

	key := expression.KeyEqual(expression.Key(emoteModGroupOwnerID), expression.Value(ownerID))
	expr, err := expression.NewBuilder().WithKeyCondition(key).Build()
	if err != nil {
		return nil, errors.Wrap(err, "failed to build query key expression")
	}

	res, err := db.client.QueryWithContext(ctx, &dynamodb.QueryInput{
		IndexName:                 aws.String(emoteModGroupsOwnerIndex),
		TableName:                 db.table,
		KeyConditionExpression:    expr.KeyCondition(),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to read emote mod groups by ownerId")
	}
	if len(res.Items) == 0 {
		return nil, nil
	}

	for _, item := range res.Items {
		var group emoteModifierGroup
		if err := dynamodbattribute.UnmarshalMap(item, &group); err != nil {
			return nil, errors.Wrap(err, "failed to unmarshal emote mod group")
		}

		groups = append(groups, group.toMako())
	}

	return groups, nil
}

func (db *EmoteModifiersDB) DeleteByIDs(ctx context.Context, ids ...string) error {
	if len(ids) == 0 {
		return errors.New("cannot pass in an empty list of ids")
	}

	for i := 0; i < len(ids); i += maxBatchWriteItemSize {
		end := intMin(len(ids), i+maxBatchWriteItemSize)
		batch := ids[i:end]
		err := db.deleteByIDsBatch(ctx, batch)
		if err != nil {
			return err
		}
	}

	return nil
}

func (db *EmoteModifiersDB) deleteByIDsBatch(ctx context.Context, ids []string) error {
	if len(ids) > maxBatchWriteItemSize {
		return fmt.Errorf("limit of %d items to batch delete at a time", maxBatchWriteItemSize)
	}

	writeRequests := []*dynamodb.WriteRequest{}

	for _, id := range ids {
		writeRequests = append(writeRequests, &dynamodb.WriteRequest{
			DeleteRequest: &dynamodb.DeleteRequest{
				Key: map[string]*dynamodb.AttributeValue{
					emoteModGroupHashKey: {S: aws.String(id)},
				},
			},
		})
	}

	requestItems := map[string][]*dynamodb.WriteRequest{
		*db.table: writeRequests,
	}

	attempts := 0
	for len(requestItems) > 0 && attempts < maxBatchWriteItemAttempts {
		input := &dynamodb.BatchWriteItemInput{
			RequestItems: requestItems,
		}

		res, err := db.client.BatchWriteItemWithContext(ctx, input)
		if err != nil {
			return err
		}

		requestItems = res.UnprocessedItems
		attempts++
	}

	return nil
}

// No-op, entitlements are stored only in the redis caching layer.
func (db *EmoteModifiersDB) ReadEntitledGroupsForUser(ctx context.Context, userID string) ([]mako.EmoteModifierGroup, error) {
	return nil, errors.New("emote modifier group entitlements are not stored in dynamo")
}

// No-op, entitlements are stored only in the redis caching layer.
func (db *EmoteModifiersDB) WriteEntitledGroupsForUser(ctx context.Context, userID string, groups ...mako.EmoteModifierGroup) error {
	return errors.New("emote modifier group entitlements are not stored in dynamo")
}

// No-op, entitlements are stored only in the redis caching layer.
func (db *EmoteModifiersDB) InvalidateEntitledGroupsCacheForUser(userID string) error {
	return errors.New("emote modifier group entitlements are not stored in dynamo")
}

func (db *EmoteModifiersDB) batchQuery(ctx context.Context, query func(key string) ([]mako.EmoteModifierGroup, error), keys ...string) ([]mako.EmoteModifierGroup, error) {
	var keysCh = make(chan string)
	var items = make(chan []mako.EmoteModifierGroup, len(keys))
	var errs = make(chan error, len(keys))
	var wg sync.WaitGroup
	wg.Add(concurrentWaitGroups)

	for i := 0; i < concurrentWaitGroups; i++ {
		go func() {
			defer wg.Done()

			for {
				select {
				case <-ctx.Done():
				case key, ok := <-keysCh:
					if !ok {
						return
					}

					var res []mako.EmoteModifierGroup
					if err := backoff.Retry(func() error {
						var err error
						res, err = query(key)
						return err
					}, backoff.WithMaxRetries(backoff.WithContext(newBackoff(), ctx), 1)); err != nil {
						errs <- err
						return
					}

					items <- res
				}
			}
		}()
	}

	for _, key := range keys {
		// Filter out empty keys.
		if key == "" {
			continue
		}

		keysCh <- key
	}

	close(keysCh)

	emoteModifierGroups := make([]mako.EmoteModifierGroup, 0, len(keys))

	for range keys {
		select {
		case <-ctx.Done():
			return emoteModifierGroups, ctx.Err()
		case err := <-errs:
			return nil, err
		case items := <-items:
			emoteModifierGroups = append(emoteModifierGroups, items...)
		}
	}

	return emoteModifierGroups, nil
}

func MakeEmoteModifiersDBForTesting(ctx context.Context) (mako.EmoteModifiersDB, error) {
	addr := "http://localhost:8005"

	// Jenkins require a different dynamodb address for tests.
	if os.Getenv("DOCKER_TEST") != "" {
		addr = "http://jenkins_dynamodb:8000"
	}

	emoteModsDB := NewEmoteModifierGroupDB(EmoteModifierGroupDBConfig{
		Region:    "us-west-2",
		Namespace: ksuid.New().String(),
		Endpoint:  addr,
		Table:     emoteModGroupsTable,
	})

	if err := emoteModsDB.create(ctx); err != nil {
		return nil, err
	}

	return emoteModsDB, nil
}
