package dynamo

import (
	"strconv"
	"testing"
	"time"

	"github.com/aws/aws-sdk-go/service/dynamodb"
	. "github.com/smartystreets/goconvey/convey"
)

func TestPrefixesTable_GetTableName(t *testing.T) {
	table := PrefixesTable{}
	Convey("Given basically nothing", t, func() {
		So(table.GetTableName(), ShouldEqual, "prefixes")
	})
}

func TestPrefixesTable_ConvertAttributeMapToRecord(t *testing.T) {
	table := PrefixesTable{}
	ownerId := "7"
	prefix := "double0"
	state := "active"
	lastUpdated := time.Now()
	lastUpdatedNano := strconv.Itoa(int(lastUpdated.UnixNano()))

	Convey("Given a prefixes table", t, func() {
		Convey("with an empty attribute map", func() {
			attributeMap := map[string]*dynamodb.AttributeValue{}
			record, err := table.ConvertAttributeMapToRecord(attributeMap)

			So(err, ShouldBeNil)
			So(record, ShouldNotBeNil)

			prefix := record.(*Prefix)
			So(prefix.OwnerId, ShouldBeBlank)
			So(prefix.Prefix, ShouldBeBlank)
			So(prefix.State, ShouldBeEmpty)
		})

		Convey("with a populated attribute map", func() {
			attributeMap := map[string]*dynamodb.AttributeValue{
				PrefixesOwnerIdColumn:     {S: &ownerId},
				PrefixesPrefixColumn:      {S: &prefix},
				PrefixesStateColumn:       {S: &state},
				PrefixesLastUpdatedColumn: {S: &lastUpdatedNano},
			}
			record, err := table.ConvertAttributeMapToRecord(attributeMap)

			So(err, ShouldBeNil)
			So(record, ShouldNotBeNil)

			converted := record.(*Prefix)
			So(converted.OwnerId, ShouldEqual, ownerId)
			So(converted.Prefix, ShouldEqual, prefix)
			So(converted.State, ShouldEqual, state)
			So(converted.LastUpdated, ShouldEqual, lastUpdated)
		})
	})

}
