package dynamo

import (
	"errors"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

// Constants related to supported item limits on BatchItem and TransactItem dynamoDB APIs
const (
	maxBatchGetItemSize      = 100
	maxBatchWriteItemSize    = 25
	maxTransactWriteItemSize = 25
)

func MapOfStringsFromAttribute(attributeMap map[string]*dynamodb.AttributeValue, attributeName string) (map[string]string, error) {
	result := make(map[string]string)
	attribute, exists := attributeMap[attributeName]
	if exists && attribute.M != nil {
		for key, value := range attribute.M {
			if value.S == nil {
				return nil, errors.New("Value in string map was empty")
			}
			result[key] = *value.S
		}
	}
	return result, nil
}

func StringMapToAttributeMap(valueMap map[string]string) map[string]*dynamodb.AttributeValue {
	result := make(map[string]*dynamodb.AttributeValue)

	for key, value := range valueMap {
		result[key] = &dynamodb.AttributeValue{S: aws.String(value)}
	}

	return result
}
