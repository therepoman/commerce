package dynamo

import (
	"time"
)

const (
	PendingEmotesHashKey                = "emoteId"
	PendingEmotesTypeGSIName            = "pending_queues_index"
	PendingEmotesReviewStateGSIName     = "pending_emotes_reviewstate_index"
	PendingEmotesTypeReviewStateGSIName = "pending_emotes_type-reviewstate_index"
	PendingEmotesType                   = "type"
	PendingEmotesReviewState            = "reviewState"
	PendingEmotesTypeReviewState        = "type-reviewState"
	PendingEmotesCreatedAt              = "createdAt"
	PendingEmotesTableName              = "pending_emotes"
	PendingEmotesAssetType              = "assetType"
	PendingAnimationTemplate            = "animationTemplate"
)

type PendingEmote struct {
	EmoteId           string                  `dynamodbav:"emoteId"`
	OwnerId           string                  `dynamodbav:"ownerId"`
	Code              string                  `dynamodbav:"code"`
	Prefix            string                  `dynamodbav:"prefix"`
	Type              AccountType             `dynamodbav:"type"`
	ReviewState       PendingEmoteReviewState `dynamodbav:"reviewState"`
	SymbolicMeaning   string                  `dynamodbav:"symbolicMeaning"`
	CreatedAt         time.Time               `dynamodbav:"createdAt"`
	UpdatedAt         time.Time               `dynamodbav:"updatedAt"`
	AssetType         string                  `dynamodbav:"assetType"`
	AnimationTemplate string                  `dynamodbav:"animationTemplate"`

	// Used for querying on multiple fields
	TypeReviewState string `dynamodbav:"type-reviewState"`
}
