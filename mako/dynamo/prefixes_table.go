package dynamo

import (
	"strconv"
	"time"

	"code.justin.tv/common/godynamo/client"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

var PrefixesOwnerIdGsi = "prefix_by_owner_id_gsi"
var PrefixesStateOwnerIdGsi = "prefix_by_state_owner_id_gsi"

const (
	PrefixesPrefixColumn      = "prefix"
	PrefixesOwnerIdColumn     = "ownerId"
	PrefixesStateColumn       = "state"
	PrefixesLastUpdatedColumn = "lastupdated"
	PrefixesHashKey           = PrefixesPrefixColumn

	PrefixesReadCapacity  = 100
	PrefixesWriteCapacity = 100
)

type PrefixState string

const (
	PrefixUnknown  PrefixState = "unknown"
	PrefixActive   PrefixState = "active"
	PrefixUnset    PrefixState = "unset"
	PrefixRejected PrefixState = "rejected"
	PrefixPending  PrefixState = "pending"
)

type Prefix struct {
	client.BaseDynamoTableRecord
	OwnerId     string
	Prefix      string
	State       PrefixState
	LastUpdated time.Time
}

type PrefixesTable struct{}

func (t *PrefixesTable) GetTableName() string {
	return "prefixes"
}

func (t *PrefixesTable) ConvertAttributeMapToRecord(attributeMap map[string]*dynamodb.AttributeValue) (client.DynamoTableRecord, error) {
	prefix := client.StringFromAttributes(attributeMap, PrefixesPrefixColumn)
	ownerId := client.StringFromAttributes(attributeMap, PrefixesOwnerIdColumn)
	state := client.StringFromAttributes(attributeMap, PrefixesStateColumn)
	lastUpdatedNano := client.StringFromAttributes(attributeMap, PrefixesLastUpdatedColumn)
	var lastUpdated time.Time

	if lastUpdatedNano != "" {
		nanos, err := strconv.Atoi(lastUpdatedNano)
		if err != nil {
			return nil, err
		}
		lastUpdated = time.Unix(0, int64(nanos))
	}

	return &Prefix{
		OwnerId:     ownerId,
		Prefix:      prefix,
		State:       PrefixState(state),
		LastUpdated: lastUpdated,
	}, nil
}

func (t *PrefixesTable) NewCreateTableInput() *dynamodb.CreateTableInput {
	return &dynamodb.CreateTableInput{
		ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
			ReadCapacityUnits:  aws.Int64(PrefixesReadCapacity),
			WriteCapacityUnits: aws.Int64(PrefixesWriteCapacity),
		},
		AttributeDefinitions: []*dynamodb.AttributeDefinition{
			{
				AttributeName: aws.String(PrefixesHashKey),
				AttributeType: aws.String("S"),
			},
		},
		KeySchema: []*dynamodb.KeySchemaElement{
			{
				AttributeName: aws.String(PrefixesHashKey),
				KeyType:       aws.String("HASH"),
			},
		},
		GlobalSecondaryIndexes: []*dynamodb.GlobalSecondaryIndex{
			{
				IndexName: aws.String(PrefixesStateOwnerIdGsi),
				KeySchema: []*dynamodb.KeySchemaElement{
					{
						AttributeName: aws.String(PrefixesStateColumn),
						KeyType:       aws.String("HASH"),
					},
					{
						AttributeName: aws.String(PrefixesOwnerIdColumn),
						KeyType:       aws.String("RANGE"),
					},
				},
				Projection: &dynamodb.Projection{
					ProjectionType: aws.String(dynamodb.ProjectionTypeAll),
				},
			},
		},
	}
}

func (p *Prefix) GetTable() client.DynamoTable {
	return &PrefixesTable{}
}

func (p *Prefix) NewAttributeMap() map[string]*dynamodb.AttributeValue {
	itemMap := make(map[string]*dynamodb.AttributeValue)
	client.PopulateAttributesString(itemMap, PrefixesPrefixColumn, p.Prefix)
	client.PopulateAttributesString(itemMap, PrefixesOwnerIdColumn, p.OwnerId)
	client.PopulateAttributesString(itemMap, PrefixesStateColumn, string(p.State))
	client.PopulateAttributesString(itemMap, PrefixesLastUpdatedColumn, strconv.FormatInt(p.LastUpdated.UnixNano(), 10))
	return itemMap
}

func (p *Prefix) NewItemKey() map[string]*dynamodb.AttributeValue {
	itemMap := make(map[string]*dynamodb.AttributeValue)
	client.PopulateAttributesString(itemMap, PrefixesHashKey, p.Prefix)
	return itemMap
}
