package dynamo

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestPrefixes_makeSingleStringMap(t *testing.T) {
	Convey("Given nothing", t, func() {
		Convey("passed empies", func() {
			mappie := makeSingleStringMap("", "")
			So(mappie, ShouldHaveLength, 0)
		})
		Convey("only key is populated", func() {
			mappie := makeSingleStringMap("key!", "")
			So(mappie, ShouldHaveLength, 0)
		})

		Convey("only value is populated", func() {
			mappie := makeSingleStringMap("", "value!")
			So(mappie, ShouldHaveLength, 1)
			So(mappie, ShouldContainKey, "")
			So(*mappie[""].S, ShouldEqual, "value!")
		})

		Convey("both are populated", func() {
			mappie := makeSingleStringMap("key!", "value!")
			So(mappie, ShouldHaveLength, 1)
			So(mappie, ShouldContainKey, "key!")
			So(*mappie["key!"].S, ShouldEqual, "value!")
		})
	})
}
