package dynamo

import (
	"context"
	"os"

	"code.justin.tv/commerce/gogogadget/pointers"
	mako "code.justin.tv/commerce/mako/internal"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/pkg/errors"
	"github.com/segmentio/ksuid"
)

const (
	emoteStandingOverridesTable  = "emote_standing_overrides"
	emoteStandingOverrideHashKey = "userId"
	emoteStandingOverrideName    = "override"
)

type emoteStandingOverride struct {
	UserID   string `dynamodbav:"userId"`
	Override string `dynamodbav:"override"`
}

func (o emoteStandingOverride) toMako() mako.UserEmoteStandingOverride {
	return mako.UserEmoteStandingOverride{
		UserID:   o.UserID,
		Override: mako.EmoteStandingOverride(o.Override),
	}
}

type EmoteStandingOverridesDB struct {
	table  *string
	client *dynamodb.DynamoDB
}

type EmoteStandingOverrideDBConfig struct {
	// The name of the table in dynamo
	Table string
	// The AWS region to connect to
	Region string
	// Used to connect to local dynamodb for testing
	Endpoint string
	// Used to scope data in local dynamodb for testing
	Namespace string
}

func NewEmoteStandingOverrideDB(config EmoteStandingOverrideDBConfig) *EmoteStandingOverridesDB {
	sess := session.Must(session.NewSession())
	conf := aws.Config{
		Region: aws.String(config.Region),
	}

	// Endpoint is only used when bootstrapping tests, local dynamodb requires static credentials.
	if config.Endpoint != "" {
		conf.Credentials = credentials.NewStaticCredentials(config.Namespace, config.Namespace, "")
		conf.Endpoint = aws.String(config.Endpoint)
	}

	client := dynamodb.New(sess, &conf)

	return &EmoteStandingOverridesDB{
		table:  aws.String(config.Table),
		client: client,
	}
}

func (db *EmoteStandingOverridesDB) UpdateOverrides(ctx context.Context, overrides ...mako.UserEmoteStandingOverride) ([]mako.UserEmoteStandingOverride, error) {
	putRequests := make([]*dynamodb.WriteRequest, 0, len(overrides))
	for _, override := range overrides {
		putRequest := &dynamodb.WriteRequest{
			PutRequest: &dynamodb.PutRequest{
				Item: map[string]*dynamodb.AttributeValue{
					emoteStandingOverrideHashKey: {
						S: pointers.StringP(override.UserID),
					},
					emoteStandingOverrideName: {
						S: pointers.StringP(override.Override.String()),
					},
				},
			},
		}

		putRequests = append(putRequests, putRequest)
	}

	requestItems := map[string][]*dynamodb.WriteRequest{
		*db.table: putRequests,
	}

	attempts := 0
	for len(requestItems) > 0 && attempts < maxBatchWriteItemAttempts {
		input := &dynamodb.BatchWriteItemInput{
			RequestItems: requestItems,
		}

		res, err := db.client.BatchWriteItemWithContext(ctx, input)
		if err != nil {
			return nil, err
		}

		requestItems = res.UnprocessedItems
		attempts++
	}

	unprocessedItems := requestItems[*db.table]
	unprocessedOverrides := make([]mako.UserEmoteStandingOverride, 0, len(unprocessedItems))
	for _, unprocessedItem := range unprocessedItems {
		if unprocessedItem == nil || unprocessedItem.PutRequest == nil || unprocessedItem.PutRequest.Item == nil {
			continue
		}

		item := unprocessedItem.PutRequest.Item

		var override emoteStandingOverride
		if err := dynamodbattribute.UnmarshalMap(item, &override); err != nil {
			return nil, errors.Wrap(err, "failed to unmarshal emote standing override")
		}

		unprocessedOverrides = append(unprocessedOverrides, override.toMako())
	}

	return unprocessedOverrides, nil
}

func (db *EmoteStandingOverridesDB) ReadByUserID(ctx context.Context, userID string) (*mako.UserEmoteStandingOverride, error) {
	if userID == "" {
		return nil, errors.New("received empty userID")
	}

	res, err := db.client.GetItemWithContext(ctx, &dynamodb.GetItemInput{
		Key:       makeSingleStringMap(emoteStandingOverrideHashKey, userID),
		TableName: db.table,
	})

	if err != nil {
		return nil, errors.Wrap(err, "failed to read emote standing overrides by userID")
	}

	if res == nil || res.Item == nil {
		return nil, nil
	}

	var override emoteStandingOverride
	if err := dynamodbattribute.UnmarshalMap(res.Item, &override); err != nil {
		return nil, errors.Wrap(err, "failed to unmarshal emote standing override")
	}

	makoOverride := override.toMako()

	return &makoOverride, nil
}

func MakeEmoteStandingOverridesDBForTesting(ctx context.Context) (mako.EmoteStandingOverridesDB, error) {
	addr := "http://localhost:8005"

	// Jenkins require a different dynamodb address for tests.
	if os.Getenv("DOCKER_TEST") != "" {
		addr = "http://jenkins_dynamodb:8000"
	}

	emoteStandingOverridesDB := NewEmoteStandingOverrideDB(EmoteStandingOverrideDBConfig{
		Region:    "us-west-2",
		Namespace: ksuid.New().String(),
		Endpoint:  addr,
		Table:     emoteStandingOverridesTable,
	})

	if err := emoteStandingOverridesDB.create(ctx); err != nil {
		return nil, err
	}

	return emoteStandingOverridesDB, nil
}

func (db *EmoteStandingOverridesDB) create(ctx context.Context) error {
	_, err := db.client.CreateTableWithContext(ctx, &dynamodb.CreateTableInput{
		AttributeDefinitions: []*dynamodb.AttributeDefinition{
			{
				AttributeName: aws.String(emoteStandingOverrideHashKey),
				AttributeType: aws.String("S"),
			},
		},
		KeySchema: []*dynamodb.KeySchemaElement{
			{
				AttributeName: aws.String(emoteStandingOverrideHashKey),
				KeyType:       aws.String("HASH"),
			},
		},
		ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
			ReadCapacityUnits:  aws.Int64(500),
			WriteCapacityUnits: aws.Int64(500),
		},
		TableName: db.table,
	})

	if err != nil {
		return err
	}
	return nil
}
