package dynamo

import (
	"testing"

	"code.justin.tv/commerce/mako/tests"
)

func TestDynamoEmoteGroupsDB(t *testing.T) {
	tests.TestEmoteGroupDB(t, MakeEmoteGroupDBForTesting)
}
