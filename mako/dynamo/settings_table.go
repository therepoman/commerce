package dynamo

import (
	"code.justin.tv/common/godynamo/client"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

const (
	SettingsHashKey           = "owerId"
	SettingsSettingsAttribute = "settings"

	SettingsReadCapacity  = 100
	SettingsWriteCapacity = 100
)

type Settings struct {
	client.BaseDynamoTableRecord
	OwnerId  string
	Settings map[string]string
}

type SettingsTable struct{}

func (t *SettingsTable) GetTableName() string {
	return "settings"
}

func (t *SettingsTable) ConvertAttributeMapToRecord(attributeMap map[string]*dynamodb.AttributeValue) (client.DynamoTableRecord, error) {
	userId := client.StringFromAttributes(attributeMap, SettingsHashKey)
	settings, err := MapOfStringsFromAttribute(attributeMap, SettingsSettingsAttribute)
	if err != nil {
		return nil, err
	}

	return &Settings{
		OwnerId:  userId,
		Settings: settings,
	}, nil
}

func (t *SettingsTable) NewCreateTableInput() *dynamodb.CreateTableInput {
	return &dynamodb.CreateTableInput{
		ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
			ReadCapacityUnits:  aws.Int64(SettingsReadCapacity),
			WriteCapacityUnits: aws.Int64(SettingsWriteCapacity),
		},
		AttributeDefinitions: []*dynamodb.AttributeDefinition{
			{
				AttributeName: aws.String(SettingsHashKey),
				AttributeType: aws.String("S"),
			},
		},
		KeySchema: []*dynamodb.KeySchemaElement{
			{
				AttributeName: aws.String(SettingsHashKey),
				KeyType:       aws.String("HASH"),
			},
		},
	}
}

func (s *Settings) GetTable() client.DynamoTable {
	return &SettingsTable{}
}

func (s *Settings) NewAttributeMap() map[string]*dynamodb.AttributeValue {
	itemMap := make(map[string]*dynamodb.AttributeValue)

	client.PopulateAttributesString(itemMap, SettingsHashKey, s.OwnerId)
	if s.Settings != nil {
		itemMap[SettingsSettingsAttribute] = &dynamodb.AttributeValue{M: StringMapToAttributeMap(s.Settings)}
	}

	return itemMap
}

func (s *Settings) NewItemKey() map[string]*dynamodb.AttributeValue {
	itemMap := make(map[string]*dynamodb.AttributeValue)
	client.PopulateAttributesString(itemMap, SettingsHashKey, s.OwnerId)
	return itemMap
}
