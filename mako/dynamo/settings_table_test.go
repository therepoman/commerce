package dynamo

import (
	"testing"

	"github.com/aws/aws-sdk-go/service/dynamodb"
	. "github.com/smartystreets/goconvey/convey"
)

func TestSettingsTable_GetTableName(t *testing.T) {
	table := SettingsTable{}
	Convey("Given basically nothing", t, func() {
		So(table.GetTableName(), ShouldEqual, "settings")
	})
}

func TestSettingsTable_ConvertAttributeMapToRecord(t *testing.T) {
	table := SettingsTable{}
	ownerId := "7"

	Convey("Given a settings table", t, func() {
		Convey("with an empty attribute map", func() {
			attributeMap := map[string]*dynamodb.AttributeValue{}
			record, err := table.ConvertAttributeMapToRecord(attributeMap)

			So(err, ShouldBeNil)
			So(record, ShouldNotBeNil)

			converted := record.(*Settings)
			So(converted.OwnerId, ShouldBeBlank)
		})

		Convey("with a populated set of settings", func() {
			settings := StringMapToAttributeMap(map[string]string{"key": "value"})

			attributeMap := map[string]*dynamodb.AttributeValue{
				SettingsHashKey:           {S: &ownerId},
				SettingsSettingsAttribute: {M: settings},
			}
			record, err := table.ConvertAttributeMapToRecord(attributeMap)

			So(err, ShouldBeNil)
			So(record, ShouldNotBeNil)

			converted := record.(*Settings)
			So(converted.OwnerId, ShouldEqual, ownerId)
			So(converted.Settings["key"], ShouldEqual, "value")
		})
	})

}
