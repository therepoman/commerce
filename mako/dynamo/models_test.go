package dynamo

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestModels_ParseAccountType(t *testing.T) {
	Convey("Given a desire to test", t, func() {
		So(parseAccountType("partner"), ShouldEqual, Partner)
		So(parseAccountType("PARTNER"), ShouldEqual, Partner)
		So(parseAccountType("partNER"), ShouldEqual, Partner)
		So(parseAccountType("affiliate"), ShouldEqual, Affiliate)
		So(parseAccountType("none"), ShouldEqual, None)
		So(parseAccountType("pizza"), ShouldEqual, None)
		So(parseAccountType(""), ShouldEqual, None)
	})
}
