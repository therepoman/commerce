# Overview
<!--
In this section, please explain your change briefly, feel free to include Jira and design doc links.
-->

# Review Checklist
- [ ] You have tested your changes in staging (or your change doesn't require this)
- [ ] You have a unit test (or several) covering your changes (or your change doesn't require this)
- [ ] You have considered what will happen for cases like Nightbot or similar users who have hundreds of thousands of entitlements, and you're confident it'll be fine.
- [ ] You have updated the runbook (or your change doesn't require this).
- [ ] You have added/updated alarms where necessary.