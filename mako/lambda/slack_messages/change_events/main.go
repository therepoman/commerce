package main

import (
	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/mako/lambda/slack_messages/change_events/internal"
	"code.justin.tv/commerce/mako/lambda/slack_messages/common"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

func main() {
	lambda.Start(LambdaHandler)
}

func LambdaHandler(event events.SNSEvent) error {
	pipelineEvent, err := internal.ExtractPipelineEvent(event)
	if err != nil {
		logrus.WithError(err).Error("Failed to extract pipeline event from SNS event.")
		return err
	}

	ctxLogger := logrus.WithField("pipelineEvent", pipelineEvent)

	msg := internal.GetSlackMessageFromPipelineChange(pipelineEvent)

	msg.Channel = common.GetTargetSlackChannel()

	ctxLogger.WithField("Slack Message", *msg).Info("Attempting to send Slack message...")

	err = common.SendSlackMessage(msg)
	if err != nil {
		logrus.WithError(err).Error("Failed to send Slack message")
		return err
	}

	ctxLogger.Info("Slack message sent.")

	return nil
}
