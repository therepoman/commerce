package internal

// constants mirroring the available States of action details
const (
	ActionSucceeded = "SUCCEEDED"
	ActionFailed    = "FAILED"
	ActionStarted   = "STARTED"
)

// constants mirroring available action categories
const (
	CategoryDeploy = "Deploy"
	CategoryBuild  = "Build"
)

type PipelineEvent struct {
	Detail struct {
		Pipeline    string `json:"pipeline"`
		ExecutionID string `json:"execution-id"`
		Stage       string `json:"stage"`
		Action      string `json:"action"`
		State       string `json:"state"`
		Type        struct {
			Category string `json:"category"`
		} `json:"type"`
	} `json:"detail"`
}
