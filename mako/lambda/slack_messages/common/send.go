package common

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"time"

	"code.justin.tv/commerce/logrus"

	"github.com/pkg/errors"

	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/secretsmanager"
)

const envWebhookSecretID = "TWITCH_WEBHOOK_SECRET_ID"

var webhookURL string

func init() {
	logrus.Info("Initializing webhook URL via secretsmanager")
	s := session.Must(session.NewSession())

	secrets := secretsmanager.New(s)

	secretID := os.Getenv(envWebhookSecretID)
	if secretID == "" {
		panic(fmt.Sprintf("Expected environment variable \"%s\" to be set, but it was not", envWebhookSecretID))
	}

	secretOutput, err := secrets.GetSecretValue(&secretsmanager.GetSecretValueInput{
		SecretId: &secretID,
	})
	if err != nil {
		panic("Failed to retrieve webhookURL secret")
	}

	webhookURL = *secretOutput.SecretString
}

var httpClient *http.Client

func init() {
	httpClient = &http.Client{
		Timeout: 10 * time.Second,
	}
}

func SendSlackMessage(message *SlackMessage) error {
	bodyBytes, err := json.Marshal(message)
	if err != nil {
		return errors.Wrap(err, "Failed to marshal Slack message while attempting to send")
	}

	resp, err := httpClient.Post(webhookURL, "application/json", bytes.NewReader(bodyBytes))
	if err != nil {
		return errors.Wrap(err, "Failed to POST Slack message")
	}

	respLogger := logrus.WithField("Status", resp.Status)

	if resp.StatusCode < 400 {
		respLogger.Info("Successfully sent Slack message.")
		return nil
	} else if resp.StatusCode < 500 {
		respLogger.Errorf("Error posting message to Slack webhook: %s", resp.Status)
		// Do not propagate error - 400 level status should not be retried
		return nil
	} else {
		respLogger.Errorf("Error posting message to Slack webhook: %s", resp.Status)
		return errors.Errorf("Slack message did not post to webhook successfully: %s", resp.Status)
	}
}
