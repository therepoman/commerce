package common

import "os"

// envSlackChannelKey identifies the environment key that holds the Slack channel that notifications should be sent to.
// This must correspond to the lambda configuration's environment variable with the same purpose.
const envSlackChannelKey = "TWITCH_SLACK_CHANNEL"

func GetTargetSlackChannel() string {
	return os.Getenv(envSlackChannelKey)
}
