package internal

type Event struct {
	Region      string `json:"region"`
	ConsoleLink string `json:"consoleLink"`
	Approval    struct {
		PipelineName       string `json:"pipelineName"`
		StageName          string `json:"stageName"`
		ActionName         string `json:"actionName"`
		ApprovalReviewLink string `json:"approvalReviewLink"`
		CustomData         string `json:"customData"`
	} `json:"approval"`
}
