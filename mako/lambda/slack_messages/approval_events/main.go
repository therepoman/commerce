package main

import (
	"fmt"
	"net/url"

	"code.justin.tv/commerce/logrus"

	"github.com/aws/aws-lambda-go/lambda"

	"code.justin.tv/commerce/mako/lambda/slack_messages/common"

	"code.justin.tv/commerce/mako/lambda/slack_messages/approval_events/internal"
	"github.com/aws/aws-lambda-go/events"
)

func main() {
	lambda.Start(LambdaHandler)
}

func LambdaHandler(event events.SNSEvent) error {
	approvalEvent, err := internal.ExtractApprovalEvent(event)
	if err != nil {
		logrus.WithError(err).Error("Failed to extract approval event from SNS event.")
		return err
	}

	parsedApprovalLink, err := url.Parse(approvalEvent.Approval.ApprovalReviewLink)
	if err != nil {
		logrus.WithError(err).Error("Failed to parse approval link from ApprovalReviewLink.")
		return err
	}

	slackChannel := common.GetTargetSlackChannel()

	approvalLink := internal.GetFederatedApprovalLink(parsedApprovalLink)
	slackMessage := &common.SlackMessage{
		Channel: slackChannel,
		Attachments: []common.Attachment{
			{
				PreText:    "*Manual approval required:*",
				AuthorName: fmt.Sprintf("CodePipeline - %s", approvalEvent.Approval.PipelineName),
				Title:      fmt.Sprintf("<%s|%s>", approvalLink, approvalEvent.Approval.CustomData),
				Color:      "warning",
				MarkdownIn: []string{"pretext", "title"},
			},
		},
	}

	logrus.WithField("Slack Message", *slackMessage).Info("Attempting to send Slack message...")

	err = common.SendSlackMessage(slackMessage)
	if err != nil {
		logrus.WithError(err).Error("Failed to send Slack message.")
		return err
	}

	return nil
}
