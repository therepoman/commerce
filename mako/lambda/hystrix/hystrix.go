package hystrix

import (
	"code.justin.tv/commerce/mako/clients/pdms"
	"code.justin.tv/commerce/mako/dynamo"
	makoHystrix "code.justin.tv/commerce/mako/hystrix"
	"code.justin.tv/commerce/mako/lambda/config"
	"github.com/afex/hystrix-go/hystrix"
	metricCollector "github.com/afex/hystrix-go/hystrix/metric_collector"
	"github.com/cactus/go-statsd-client/statsd"
)

// Configure hystrix timeouts and metrics collection.
// This is only for Lambdas.
func ConfigureHystrix(cfg *config.Config, stats statsd.Statter) {
	configureHystrixCommands(cfg)

	collector := makoHystrix.NewHystrixMetricsCollector(stats)
	metricCollector.Registry.Register(func(name string) metricCollector.MetricCollector {
		return collector.NewHystrixCommandMetricsCollector(name)
	})
}

func configureHystrixCommands(cfg *config.Config) {
	hystrix.Configure(map[string]hystrix.CommandConfig{
		pdms.HystrixNamePDMSReportDeletion: {
			MaxConcurrentRequests: 200,
			Timeout:               pdms.TimeoutMilliseconds,
		},
		dynamo.HystrixNameEntitlementsGet: {
			MaxConcurrentRequests: 200,
			Timeout:               cfg.EntitlementsDynamoDBGetTimeoutMilliseconds,
		},
	})
}
