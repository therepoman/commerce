package config

var productionConfig = Config{
	AWSRegion:                "us-west-2",
	AnimatedEmoteFrameBucket: "prod-animated-emote-frames",
	EmoteUploadsBucket:       "prod-emoticon-uploads",
	EnvironmentPrefix:        "prod",
	CloudwatchRegion:         "us-west-2",
	MakoEndpoint:             "https://main.us-west-2.prod.mako.twitch.a2z.com",
	PDMSLambdaCallerRoleARN:  "arn:aws:iam::125704643346:role/PDMSLambda-CallerRole-13IIND444YKVR",
	PDMSLambdaARN:            "arn:aws:lambda:us-west-2:125704643346:function:PDMSLambda-LambdaFunction-11LXHJVSNJJPY:live",
	EntitlementsDynamoDBGetTimeoutMilliseconds: 2000,
	EmotesTable:      "prod_emotes",
	DartReceiverHost: "https://us-west-2.prod.twitchdartreceiver.s.twitch.a2z.com",
}
