package config

var stagingConfig = Config{
	AWSRegion:                "us-west-2",
	AnimatedEmoteFrameBucket: "staging-animated-emote-frames",
	EmoteUploadsBucket:       "staging-emoticon-uploads",
	EnvironmentPrefix:        "staging",
	CloudwatchRegion:         "us-west-2",
	MakoEndpoint:             "https://main.us-west-2.beta.mako.twitch.a2z.com",
	PDMSLambdaCallerRoleARN:  "arn:aws:iam::895799599216:role/PDMSLambda-CallerRole-18451FI19HSXT",
	PDMSLambdaARN:            "arn:aws:lambda:us-west-2:895799599216:function:PDMSLambda-LambdaFunction-IT8I1PE1YR81:live",
	EntitlementsDynamoDBGetTimeoutMilliseconds: 5000,
	EmotesTable:      "staging_emotes",
	DartReceiverHost: "https://us-west-2.beta.twitchdartreceiver.s.twitch.a2z.com",
}
