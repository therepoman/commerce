package config

import (
	"fmt"
	"os"
)

const (
	ENVIRONMENT   = "ENVIRONMENT"
	stagingEnv    = "staging"
	productionEnv = "production"
)

// Config contains the fields to configure Mako lambdas.
type Config struct {
	AWSRegion                  string
	AnimatedEmoteFrameBucket   string
	LegacyEmoticonS3BucketName string
	EmoteUploadsBucket         string
	EnvironmentPrefix          string
	CloudwatchRegion           string
	MakoEndpoint               string
	EmotesTable                string

	// PDMS https://wiki.twitch.com/pages/viewpage.action?pageId=219459403
	PDMSLambdaCallerRoleARN string
	PDMSLambdaARN           string

	// Hystrix timeouts
	EntitlementsDynamoDBGetTimeoutMilliseconds int

	// 2FA entitlements
	DartReceiverHost string
}

// Load loads the configuration for the current environment
func Load() *Config {
	env := os.Getenv(ENVIRONMENT)

	fmt.Printf("Loading configuration for %v\n", env)

	if env == productionEnv {
		return &productionConfig
	}

	return &stagingConfig
}
