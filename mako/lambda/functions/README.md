# Mako Lambdas


This folder (`lambda/functions`) contains the implementation for lambdas in production use in Mako. 
Each sub-folder here contains the entrypoint for a single lambda, and the name of the folder should correspond to the
name of this lambda in `terraform/mako/lambda.tf`. 


## How the build/deploy process works

Deploying lambda code is a multi-step process. It has been mostly abstracted away into the Jenkins files and some scripts,
but this document serves to explain what is going on, in the hopes that this will be easier to follow for others. 

First off, the terraform for lambdas in this repo includes terraforming a Lambda S3 bucket in both the staging and production accounts. 

Then when you open a PR, your code is first built on the Jenkins machine, then pushed to the S3 bucket using the commit hash as a unique key (see `Jenkinsfile`).
This basically just calls `scripts/build-lambdas.sh` and `scripts/push-lambdas.sh` with the right parameters.

Finally when you are ready to deploy, you can initiate the deploy through clean-deploy. This invokes the `mako-deploy` Jenkins job,
which is defined by `jenkins/Jenkinsfile`. This will run `scripts/deploy-lambda.sh` with the right parameters, which will take the
previously-built-and-uploaded code in S3 and actually deploy it so that it will receive traffic. 
  

## How to add a new lambda


To add new lambdas:

1. For every new lambda, create a new folder under lambda/functions with a main.go that includes a lambda handler (see [here](https://git.xarth.tv/subs/galleon/tree/master/lambda/functions/foobar) for an example). It is a good idea to use a stub to get started so that, when the terraform is run later, only the stub is saved in S3 and gets deployed to Lambda. Don't forget tests!
2. Get approval for Step 1 and merge to master. A Jenkins build in non-master branches will run both the build and push scripts mentioned above, but will store the .zip of the lambda in a folder in S3 designated by your git commit hash. Only builds triggered in master will update the `latest` sub-folder in S3. The `latest` folder is required by the lambda terraform.
3. In a new PR, write the lambda terraform code [here](https://git.xarth.tv/commerce/mako/blob/master/terraform/mako/lambda.tf).
4. Add new deploy targets [here](https://git.xarth.tv/commerce/mako/blob/master/deploy.json) for the new lambda. Be sure to include both staging and prod targets, and make sure the name of the target matches the `<environment>-<component>` format, where `component` is the name of the directory for your lambda under lambda/functions.
5. You can fill in the implementation of your lambda here too, or save it for a future PR. Remember, only lambda function code that is merged and build into master will be deployed to AWS lambda when using the clean-deploy targets.
6. You can now run `make plan env=staging` / `make plan env=production` for your new lambdas. Paste the output into a PR and get approval from another team member
7. Once your plan is approved, you can `make apply env=staging` / `make apply env=production` to update tfstate and persist the changes. Be sure to commit the updated tfstate
8. Get a reup and merge the new terraform + tfstate.
9. Use the new clean-deploy targets created in Step 4 to deploy the lambda code from S3 into AWS Lambda.
