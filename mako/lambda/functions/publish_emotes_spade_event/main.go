package main

import (
	"context"
	"fmt"
	"os"
	"strings"

	spade_mako "code.justin.tv/commerce/mako/clients/spade"
	"code.justin.tv/common/spade-client-go/spade"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/pkg/errors"
)

// These are the columns the data is in - see dynamo/emotedb.go
const (
	emotesIdAttribute                = "id"
	emotesCodeAttribute              = "code"
	emotesCreatedAtAttribute         = "createdAt"
	emotesDomainAttribute            = "domain"
	emotesGroupAttribute             = "emotesGroup"
	emotesGroupIdAttribute           = "groupId"
	emotesOrderAttribute             = "order"
	emotesOwnerIdAttribute           = "ownerId"
	emotesPrefixAttribute            = "prefix"
	emotesStateAttribute             = "state"
	emotesSuffixAttribute            = "suffix"
	emotesUpdatedAtAttribute         = "updatedAt"
	emotesAssetTypeAttribute         = "assetType"
	emotesAnimationTemplateAttribute = "animationTemplate"
	emotesImageSourceAttribute       = "imageSource"

	spadeEventName = "state_change_emotes"
)

// This is the shape of the event to send.
// See: https://docs.google.com/document/d/1S0EicE2yFGMRysD4JQI2mbgmCF1ZAoQg8j6m0aLcEig - data tracking spec
type EmotesSpadeEvent struct {
	Id                   string `json:"id,omitempty"`
	StorageType          string `json:"metadata_store_type,omitempty"`
	EventID              string `json:"metadata_event_id,omitempty"`
	StateChangeType      string `json:"metadata_state_change_type,omitempty"`
	StateChangedAt       int64  `json:"metadata_state_changed_at,omitempty"`
	CodeOld              string `json:"code_old,omitempty"`
	CodeNew              string `json:"code_new,omitempty"`
	CreatedAtOld         int64  `json:"created_at_old,omitempty"`
	CreatedAtNew         int64  `json:"created_at_new,omitempty"`
	DomainOld            string `json:"domain_old,omitempty"`
	DomainNew            string `json:"domain_new,omitempty"`
	EmotesGroupOld       string `json:"emotes_group_old,omitempty"`
	EmotesGroupNew       string `json:"emotes_group_new,omitempty"`
	GroupIdOld           string `json:"group_id_old,omitempty"`
	GroupIdNew           string `json:"group_id_new,omitempty"`
	OrderOld             int64  `json:"order_old,omitempty"`
	OrderNew             int64  `json:"order_new,omitempty"`
	OwnerIdOld           string `json:"owner_id_old,omitempty"`
	OwnerIdNew           string `json:"owner_id_new,omitempty"`
	PrefixOld            string `json:"prefix_old,omitempty"`
	PrefixNew            string `json:"prefix_new,omitempty"`
	StateOld             string `json:"state_old,omitempty"`
	StateNew             string `json:"state_new,omitempty"`
	SuffixOld            string `json:"suffix_old,omitempty"`
	SuffixNew            string `json:"suffix_new,omitempty"`
	UpdatedAtOld         int64  `json:"updated_at_old,omitempty"`
	UpdatedAtNew         int64  `json:"updated_at_new,omitempty"`
	AssetTypeOld         string `json:"asset_type_old,omitempty"`
	AssetTypeNew         string `json:"asset_type_new,omitempty"`
	AnimationTemplateOld string `json:"animation_template_old,omitempty"`
	AnimationTemplateNew string `json:"animation_template_new,omitempty"`
	ImageSourceOld       string `json:"image_source_old,omitempty"`
	ImageSourceNew       string `json:"image_source_new,omitempty"`
}

type lambdaHandler struct {
	s spade.Client
}

func (h *lambdaHandler) Handler(ctx context.Context, e events.DynamoDBEvent) error {
	fmt.Printf("Received event with %v records\n", len(e.Records))

	spadeEvents := make([]spade.Event, 0)

	for _, record := range e.Records {
		// validate record
		event, err := h.validateRecordAndConvertToSpadeEvent(record)
		if err != nil {
			fmt.Printf("unable to process event record: %v\n", err)
			continue
		}
		spadeEvents = append(spadeEvents, spade.Event{
			Name:       spadeEventName,
			Properties: event,
		})
	}

	if len(spadeEvents) > 0 {
		if err := h.s.TrackEvents(ctx, spadeEvents...); err != nil {
			return errors.Wrap(err, "error sending spade events")
		}
	}

	return nil
}

func (h *lambdaHandler) validateRecordAndConvertToSpadeEvent(record events.DynamoDBEventRecord) (EmotesSpadeEvent, error) {
	spadeEvent := EmotesSpadeEvent{
		StorageType:     "DynamoDB",
		EventID:         record.EventID,
		StateChangeType: toStateChangeType(record),
		StateChangedAt:  record.Change.ApproximateCreationDateTime.UTC().Unix(),
	}

	// Id is the primary key to the table, so it will never change for a given emote.
	// If not present due to delete, then fall back to old value.
	emoteId, ok := record.Change.NewImage[emotesIdAttribute]
	if !ok {
		emoteId, ok = record.Change.OldImage[emotesIdAttribute]
		if !ok {
			return EmotesSpadeEvent{}, errors.New("unable to find id attribute for emotes dynamo streams event")
		}
	}
	switch emoteId.DataType() {
	case events.DataTypeString:
		spadeEvent.Id = emoteId.String()
	default:
		return EmotesSpadeEvent{}, errors.New("id attribute was not a supported type for emotes dynamo streams event")
	}

	spadeEvent.CodeOld, spadeEvent.CodeNew = extractStringValues(record, emotesCodeAttribute)
	spadeEvent.CreatedAtOld, spadeEvent.CreatedAtNew = extractNumberValues(record, emotesCreatedAtAttribute)
	spadeEvent.DomainOld, spadeEvent.DomainNew = extractStringValues(record, emotesDomainAttribute)
	spadeEvent.EmotesGroupOld, spadeEvent.EmotesGroupNew = extractStringValues(record, emotesGroupAttribute)
	spadeEvent.GroupIdOld, spadeEvent.GroupIdNew = extractStringValues(record, emotesGroupIdAttribute)
	spadeEvent.OrderOld, spadeEvent.OrderNew = extractNumberValues(record, emotesOrderAttribute)
	spadeEvent.OwnerIdOld, spadeEvent.OwnerIdNew = extractStringValues(record, emotesOwnerIdAttribute)
	spadeEvent.PrefixOld, spadeEvent.PrefixNew = extractStringValues(record, emotesPrefixAttribute)
	spadeEvent.StateOld, spadeEvent.StateNew = extractStringValues(record, emotesStateAttribute)
	spadeEvent.SuffixOld, spadeEvent.SuffixNew = extractStringValues(record, emotesSuffixAttribute)
	spadeEvent.UpdatedAtOld, spadeEvent.UpdatedAtNew = extractNumberValues(record, emotesUpdatedAtAttribute)
	spadeEvent.AssetTypeOld, spadeEvent.AssetTypeNew = extractStringValues(record, emotesAssetTypeAttribute)
	spadeEvent.AnimationTemplateOld, spadeEvent.AnimationTemplateNew = extractStringValues(record, emotesAnimationTemplateAttribute)
	spadeEvent.ImageSourceOld, spadeEvent.ImageSourceNew = extractStringValues(record, emotesImageSourceAttribute)

	return spadeEvent, nil
}

func extractStringValues(record events.DynamoDBEventRecord, attribute string) (string, string) {
	oldValue := ""
	if value, ok := record.Change.OldImage[attribute]; ok {
		if value.DataType() == events.DataTypeString {
			oldValue = value.String()
		}
	}

	newValue := ""
	if value, ok := record.Change.NewImage[attribute]; ok {
		if value.DataType() == events.DataTypeString {
			newValue = value.String()
		}
	}
	return oldValue, newValue
}

func extractNumberValues(record events.DynamoDBEventRecord, attribute string) (int64, int64) {
	oldValue := int64(0)
	var err error
	if value, ok := record.Change.OldImage[attribute]; ok {
		if value.DataType() == events.DataTypeNumber {
			oldValue, err = value.Integer()
		}
	}
	if err != nil {
		fmt.Printf("old attribute %s was number type but could not be parsed to integer", attribute)
	}

	newValue := int64(0)
	if value, ok := record.Change.NewImage[attribute]; ok {
		if value.DataType() == events.DataTypeNumber {
			newValue, err = value.Integer()
		}
	}
	if err != nil {
		fmt.Printf("new attribute %s was number type but could not be parsed to integer", attribute)
	}

	return oldValue, newValue
}

func toStateChangeType(record events.DynamoDBEventRecord) string {
	switch record.EventName {
	case string(events.DynamoDBOperationTypeModify):
		return "update"
	case string(events.DynamoDBOperationTypeInsert):
		return "insert"
	case string(events.DynamoDBOperationTypeRemove):
		return "delete"
	}
	return ""
}

func isProd() bool {
	env := strings.ToLower(os.Getenv("ENVIRONMENT"))
	if env == "prod" || env == "production" {
		return true
	}
	return false
}

func loadSpadeClient() (spade.Client, error) {
	if isProd() {
		return spade.NewClient()
	}
	return &spade_mako.SpadeClientNoOp{}, nil
}

func main() {
	client, err := loadSpadeClient()
	if err != nil {
		panic("unable to create spade client")
	}
	h := lambdaHandler{
		s: client,
	}
	lambda.Start(h.Handler)
}
