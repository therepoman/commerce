package main

import (
	"context"
	"errors"
	"strconv"
	"testing"
	"time"

	"github.com/aws/aws-lambda-go/events"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestPublishEmotesSpadeEvent_Handler(t *testing.T) {
	t.Run("GIVEN multiple valid records WHEN handle event THEN send multiple records to spade", func(t *testing.T) {
		// GIVEN
		s := &spadeFake{}
		h := lambdaHandler{s}
		e := makeValidEvent(2)

		// WHEN
		err := h.Handler(context.Background(), e)

		// THEN
		assert.NoError(t, err)
		assert.Len(t, s.events, 2)
	})

	t.Run("GIVEN invalid record WHEN handle event THEN that event is not sent to spade", func(t *testing.T) {
		// GIVEN
		s := &spadeFake{}
		h := lambdaHandler{s}
		e := events.DynamoDBEvent{
			Records: []events.DynamoDBEventRecord{

				{
					EventID: "anyID1",
					Change:  makeValidStreamRecord(),
				},
				{
					EventID: "anyID2",
					// Record is missing change data
				},
			},
		}

		// WHEN
		err := h.Handler(context.Background(), e)

		// THEN
		assert.NoError(t, err)
		assert.Len(t, s.events, 1)
	})

	t.Run("GIVEN error calling spade WHEN handle event THEN return error", func(t *testing.T) {
		// GIVEN
		s := &spadeFake{}
		s.returnErr = errors.New("fake error")
		h := lambdaHandler{s}
		e := makeValidEvent(1)

		// WHEN
		err := h.Handler(context.Background(), e)

		// THEN
		assert.Error(t, err)
	})
}

func TestPublishEmotesSpadeEvent_Validate(t *testing.T) {
	testEventId := "7395ece3-121e-4dd9-88ad-0fae65b32a3b"

	testEmoteId := "testEmoteId"
	testEmoteCode := "wolfPack"
	testCreatedAt := int64(1591048984000000000)
	testDomain := "emotes"
	testEmoteGroup := "Subscriptions"
	testGroupId := "1591779"
	testOrder := int64(0)
	testOwnerId := "267893713"
	testPrefix := "wolf"
	testState := "active"
	testSuffix := "Pack"
	testUpdatedAt := int64(1591048985000000000)
	assetType := "animated"
	animationTemplate := "SHAKE"

	testEmoteCode2 := "wolf2Alpha"
	testCreatedAt2 := int64(1591048986000000000)
	testDomain2 := "emotes2"
	testEmoteGroup2 := "BitsTierEmoteRewards"
	testGroupId2 := "1591778"
	testOrder2 := int64(1)
	testOwnerId2 := "267893714"
	testPrefix2 := "wolf2"
	testState2 := "inactive"
	testSuffix2 := "Alpha"
	testUpdatedAt2 := int64(1591048987000000000)
	assetType2 := "static"
	animationTemplate2 := "NO_TEMPLATE"

	tests := []struct {
		scenario string
		test     func(t *testing.T)
	}{
		{
			scenario: "GIVEN insert (old_image is omitted) WHEN validate record THEN no error",
			test: func(t *testing.T) {
				// GIVEN
				h := lambdaHandler{&spadeFake{}}
				r := events.DynamoDBEventRecord{
					EventID:   testEventId,
					EventName: string(events.DynamoDBOperationTypeInsert),
					Change: events.DynamoDBStreamRecord{
						ApproximateCreationDateTime: events.SecondsEpochTime{
							Time: time.Date(2015, time.April, 9, 15, 0, 0, 0, time.UTC),
						},
						// OldImage is omitted
						NewImage: map[string]events.DynamoDBAttributeValue{
							emotesIdAttribute:                events.NewStringAttribute(testEmoteId),
							emotesCodeAttribute:              events.NewStringAttribute(testEmoteCode),
							emotesCreatedAtAttribute:         events.NewNumberAttribute(strconv.Itoa(int(testCreatedAt))),
							emotesDomainAttribute:            events.NewStringAttribute(testDomain),
							emotesGroupAttribute:             events.NewStringAttribute(testEmoteGroup),
							emotesGroupIdAttribute:           events.NewStringAttribute(testGroupId),
							emotesOrderAttribute:             events.NewNumberAttribute(strconv.Itoa(int(testOrder))),
							emotesOwnerIdAttribute:           events.NewStringAttribute(testOwnerId),
							emotesPrefixAttribute:            events.NewStringAttribute(testPrefix),
							emotesStateAttribute:             events.NewStringAttribute(testState),
							emotesSuffixAttribute:            events.NewStringAttribute(testSuffix),
							emotesUpdatedAtAttribute:         events.NewNumberAttribute(strconv.Itoa(int(testUpdatedAt))),
							emotesAssetTypeAttribute:         events.NewStringAttribute(assetType),
							emotesAnimationTemplateAttribute: events.NewStringAttribute(animationTemplate),
						},
					},
				}

				// WHEN
				event, err := h.validateRecordAndConvertToSpadeEvent(r)

				// THEN
				require.NoError(t, err)
				assert.Equal(t, "DynamoDB", event.StorageType)
				assert.Equal(t, testEventId, event.EventID)
				assert.Equal(t, "insert", event.StateChangeType)
				assert.Equal(t, int64(1428591600), event.StateChangedAt)
				assert.Equal(t, testEmoteId, event.Id)
				assert.Empty(t, event.CodeOld)
				assert.Equal(t, testEmoteCode, event.CodeNew)
				assert.Empty(t, event.CreatedAtOld)
				assert.Equal(t, testCreatedAt, event.CreatedAtNew)
				assert.Empty(t, event.DomainOld)
				assert.Equal(t, testDomain, event.DomainNew)
				assert.Empty(t, event.EmotesGroupOld)
				assert.Equal(t, testEmoteGroup, event.EmotesGroupNew)
				assert.Empty(t, event.GroupIdOld)
				assert.Equal(t, testGroupId, event.GroupIdNew)
				assert.Empty(t, event.OrderOld)
				assert.Equal(t, testOrder, event.OrderNew)
				assert.Empty(t, event.OwnerIdOld)
				assert.Equal(t, testOwnerId, event.OwnerIdNew)
				assert.Empty(t, event.PrefixOld)
				assert.Equal(t, testPrefix, event.PrefixNew)
				assert.Empty(t, event.StateOld)
				assert.Equal(t, testState, event.StateNew)
				assert.Empty(t, event.SuffixOld)
				assert.Equal(t, testSuffix, event.SuffixNew)
				assert.Empty(t, event.UpdatedAtOld)
				assert.Equal(t, testUpdatedAt, event.UpdatedAtNew)
				assert.Empty(t, event.AssetTypeOld)
				assert.Equal(t, assetType, event.AssetTypeNew)
				assert.Empty(t, event.AnimationTemplateOld)
				assert.Equal(t, animationTemplate, event.AnimationTemplateNew)
			},
		},
		{
			scenario: "GIVEN delete (new_image is omitted) WHEN validate record THEN no error",
			test: func(t *testing.T) {
				// GIVEN
				h := lambdaHandler{&spadeFake{}}
				r := events.DynamoDBEventRecord{
					EventID:   testEventId,
					EventName: string(events.DynamoDBOperationTypeRemove),
					Change: events.DynamoDBStreamRecord{
						ApproximateCreationDateTime: events.SecondsEpochTime{
							Time: time.Date(2015, time.April, 9, 15, 0, 0, 0, time.UTC),
						},
						OldImage: map[string]events.DynamoDBAttributeValue{
							emotesIdAttribute:                events.NewStringAttribute(testEmoteId),
							emotesCodeAttribute:              events.NewStringAttribute(testEmoteCode),
							emotesCreatedAtAttribute:         events.NewNumberAttribute(strconv.Itoa(int(testCreatedAt))),
							emotesDomainAttribute:            events.NewStringAttribute(testDomain),
							emotesGroupAttribute:             events.NewStringAttribute(testEmoteGroup),
							emotesGroupIdAttribute:           events.NewStringAttribute(testGroupId),
							emotesOrderAttribute:             events.NewNumberAttribute(strconv.Itoa(int(testOrder))),
							emotesOwnerIdAttribute:           events.NewStringAttribute(testOwnerId),
							emotesPrefixAttribute:            events.NewStringAttribute(testPrefix),
							emotesStateAttribute:             events.NewStringAttribute(testState),
							emotesSuffixAttribute:            events.NewStringAttribute(testSuffix),
							emotesUpdatedAtAttribute:         events.NewNumberAttribute(strconv.Itoa(int(testUpdatedAt))),
							emotesAssetTypeAttribute:         events.NewStringAttribute(assetType),
							emotesAnimationTemplateAttribute: events.NewStringAttribute(animationTemplate),
						},
						// NewImage is omitted
					},
				}

				// WHEN
				event, err := h.validateRecordAndConvertToSpadeEvent(r)

				// THEN
				require.NoError(t, err)
				assert.Equal(t, "DynamoDB", event.StorageType)
				assert.Equal(t, testEventId, event.EventID)
				assert.Equal(t, "delete", event.StateChangeType)
				assert.Equal(t, int64(1428591600), event.StateChangedAt)
				assert.Equal(t, testEmoteId, event.Id)
				assert.Empty(t, event.CodeNew)
				assert.Equal(t, testEmoteCode, event.CodeOld)
				assert.Empty(t, event.CreatedAtNew)
				assert.Equal(t, testCreatedAt, event.CreatedAtOld)
				assert.Empty(t, event.DomainNew)
				assert.Equal(t, testDomain, event.DomainOld)
				assert.Empty(t, event.EmotesGroupNew)
				assert.Equal(t, testEmoteGroup, event.EmotesGroupOld)
				assert.Empty(t, event.GroupIdNew)
				assert.Equal(t, testGroupId, event.GroupIdOld)
				assert.Empty(t, event.OrderNew)
				assert.Equal(t, testOrder, event.OrderOld)
				assert.Empty(t, event.OwnerIdNew)
				assert.Equal(t, testOwnerId, event.OwnerIdOld)
				assert.Empty(t, event.PrefixNew)
				assert.Equal(t, testPrefix, event.PrefixOld)
				assert.Empty(t, event.StateNew)
				assert.Equal(t, testState, event.StateOld)
				assert.Empty(t, event.SuffixNew)
				assert.Equal(t, testSuffix, event.SuffixOld)
				assert.Empty(t, event.UpdatedAtNew)
				assert.Equal(t, testUpdatedAt, event.UpdatedAtOld)
				assert.Empty(t, event.AssetTypeNew)
				assert.Equal(t, assetType, event.AssetTypeOld)
				assert.Empty(t, event.AnimationTemplateNew)
				assert.Equal(t, animationTemplate, event.AnimationTemplateOld)
			},
		},
		{
			scenario: "GIVEN update (both images included) WHEN validate record THEN no error",
			test: func(t *testing.T) {
				// GIVEN
				h := lambdaHandler{&spadeFake{}}
				r := events.DynamoDBEventRecord{
					EventID:   testEventId,
					EventName: string(events.DynamoDBOperationTypeModify),
					Change: events.DynamoDBStreamRecord{
						ApproximateCreationDateTime: events.SecondsEpochTime{
							Time: time.Date(2015, time.April, 9, 15, 0, 0, 0, time.UTC),
						},
						OldImage: map[string]events.DynamoDBAttributeValue{
							emotesIdAttribute:                events.NewStringAttribute(testEmoteId),
							emotesCodeAttribute:              events.NewStringAttribute(testEmoteCode),
							emotesCreatedAtAttribute:         events.NewNumberAttribute(strconv.Itoa(int(testCreatedAt))),
							emotesDomainAttribute:            events.NewStringAttribute(testDomain),
							emotesGroupAttribute:             events.NewStringAttribute(testEmoteGroup),
							emotesGroupIdAttribute:           events.NewStringAttribute(testGroupId),
							emotesOrderAttribute:             events.NewNumberAttribute(strconv.Itoa(int(testOrder))),
							emotesOwnerIdAttribute:           events.NewStringAttribute(testOwnerId),
							emotesPrefixAttribute:            events.NewStringAttribute(testPrefix),
							emotesStateAttribute:             events.NewStringAttribute(testState),
							emotesSuffixAttribute:            events.NewStringAttribute(testSuffix),
							emotesUpdatedAtAttribute:         events.NewNumberAttribute(strconv.Itoa(int(testUpdatedAt))),
							emotesAssetTypeAttribute:         events.NewStringAttribute(assetType),
							emotesAnimationTemplateAttribute: events.NewStringAttribute(animationTemplate),
						},
						NewImage: map[string]events.DynamoDBAttributeValue{
							emotesIdAttribute:                events.NewStringAttribute(testEmoteId),
							emotesCodeAttribute:              events.NewStringAttribute(testEmoteCode2),
							emotesCreatedAtAttribute:         events.NewNumberAttribute(strconv.Itoa(int(testCreatedAt2))),
							emotesDomainAttribute:            events.NewStringAttribute(testDomain2),
							emotesGroupAttribute:             events.NewStringAttribute(testEmoteGroup2),
							emotesGroupIdAttribute:           events.NewStringAttribute(testGroupId2),
							emotesOrderAttribute:             events.NewNumberAttribute(strconv.Itoa(int(testOrder2))),
							emotesOwnerIdAttribute:           events.NewStringAttribute(testOwnerId2),
							emotesPrefixAttribute:            events.NewStringAttribute(testPrefix2),
							emotesStateAttribute:             events.NewStringAttribute(testState2),
							emotesSuffixAttribute:            events.NewStringAttribute(testSuffix2),
							emotesUpdatedAtAttribute:         events.NewNumberAttribute(strconv.Itoa(int(testUpdatedAt2))),
							emotesAssetTypeAttribute:         events.NewStringAttribute(assetType2),
							emotesAnimationTemplateAttribute: events.NewStringAttribute(animationTemplate2),
						},
					},
				}

				// WHEN
				event, err := h.validateRecordAndConvertToSpadeEvent(r)

				// THEN
				require.NoError(t, err)
				assert.Equal(t, "DynamoDB", event.StorageType)
				assert.Equal(t, testEventId, event.EventID)
				assert.Equal(t, "update", event.StateChangeType)
				assert.Equal(t, int64(1428591600), event.StateChangedAt)
				assert.Equal(t, testEmoteId, event.Id)
				assert.Equal(t, testEmoteCode, event.CodeOld)
				assert.Equal(t, testEmoteCode2, event.CodeNew)
				assert.Equal(t, testCreatedAt, event.CreatedAtOld)
				assert.Equal(t, testCreatedAt2, event.CreatedAtNew)
				assert.Equal(t, testDomain, event.DomainOld)
				assert.Equal(t, testDomain2, event.DomainNew)
				assert.Equal(t, testEmoteGroup, event.EmotesGroupOld)
				assert.Equal(t, testEmoteGroup2, event.EmotesGroupNew)
				assert.Equal(t, testGroupId, event.GroupIdOld)
				assert.Equal(t, testGroupId2, event.GroupIdNew)
				assert.Equal(t, testOrder, event.OrderOld)
				assert.Equal(t, testOrder2, event.OrderNew)
				assert.Equal(t, testOwnerId, event.OwnerIdOld)
				assert.Equal(t, testOwnerId2, event.OwnerIdNew)
				assert.Equal(t, testPrefix, event.PrefixOld)
				assert.Equal(t, testPrefix2, event.PrefixNew)
				assert.Equal(t, testState, event.StateOld)
				assert.Equal(t, testState2, event.StateNew)
				assert.Equal(t, testSuffix, event.SuffixOld)
				assert.Equal(t, testSuffix2, event.SuffixNew)
				assert.Equal(t, testUpdatedAt, event.UpdatedAtOld)
				assert.Equal(t, testUpdatedAt2, event.UpdatedAtNew)
				assert.Equal(t, assetType, event.AssetTypeOld)
				assert.Equal(t, assetType2, event.AssetTypeNew)
				assert.Equal(t, animationTemplate, event.AnimationTemplateOld)
				assert.Equal(t, animationTemplate2, event.AnimationTemplateNew)
			},
		},
		{
			scenario: "GIVEN both images omitted WHEN validate record THEN return error",
			test: func(t *testing.T) {
				// GIVEN
				h := lambdaHandler{&spadeFake{}}
				r := events.DynamoDBEventRecord{
					EventID:   testEventId,
					EventName: string(events.DynamoDBOperationTypeModify),
					Change: events.DynamoDBStreamRecord{
						ApproximateCreationDateTime: events.SecondsEpochTime{
							Time: time.Date(2015, time.April, 9, 15, 0, 0, 0, time.UTC),
						},
						// both OldImage and NewImage are missing
					},
				}

				// WHEN
				event, err := h.validateRecordAndConvertToSpadeEvent(r)

				// THEN
				require.Error(t, err)
				assert.Contains(t, err.Error(), "unable to find id attribute for emotes dynamo streams event")
				assert.Empty(t, event)
			},
		},
		{
			scenario: "GIVEN emote id is not a string WHEN validate record THEN return error",
			test: func(t *testing.T) {
				// GIVEN
				h := lambdaHandler{&spadeFake{}}
				r := events.DynamoDBEventRecord{
					Change: events.DynamoDBStreamRecord{
						OldImage: map[string]events.DynamoDBAttributeValue{
							emotesIdAttribute: events.NewBooleanAttribute(true),
						},
					},
				}

				// WHEN
				_, err := h.validateRecordAndConvertToSpadeEvent(r)

				// THEN
				require.Error(t, err)
				assert.Contains(t, err.Error(), "id attribute was not a supported type for emotes dynamo streams event")
			},
		},
		{
			scenario: "GIVEN insert with some values missing or null WHEN validate record THEN no error and missing or null attributes are empty",
			test: func(t *testing.T) {
				// GIVEN
				h := lambdaHandler{&spadeFake{}}
				r := events.DynamoDBEventRecord{
					EventID:   testEventId,
					EventName: string(events.DynamoDBOperationTypeInsert),
					Change: events.DynamoDBStreamRecord{
						ApproximateCreationDateTime: events.SecondsEpochTime{
							Time: time.Date(2015, time.April, 9, 15, 0, 0, 0, time.UTC),
						},
						// OldImage is omitted
						NewImage: map[string]events.DynamoDBAttributeValue{
							emotesIdAttribute:                events.NewStringAttribute(testEmoteId),
							emotesCodeAttribute:              events.NewStringAttribute(testEmoteCode),
							emotesCreatedAtAttribute:         events.NewNumberAttribute(strconv.Itoa(int(testCreatedAt))),
							emotesGroupAttribute:             events.NewNullAttribute(),
							emotesGroupIdAttribute:           events.NewStringAttribute(testGroupId),
							emotesOwnerIdAttribute:           events.NewStringAttribute(testOwnerId),
							emotesStateAttribute:             events.NewStringAttribute(testState),
							emotesSuffixAttribute:            events.NewStringAttribute(testSuffix),
							emotesUpdatedAtAttribute:         events.NewNumberAttribute(strconv.Itoa(int(testUpdatedAt))),
							emotesAssetTypeAttribute:         events.NewStringAttribute(assetType),
							emotesAnimationTemplateAttribute: events.NewStringAttribute(animationTemplate),
						},
					},
				}

				// WHEN
				event, err := h.validateRecordAndConvertToSpadeEvent(r)

				// THEN
				require.NoError(t, err)
				assert.Equal(t, "DynamoDB", event.StorageType)
				assert.Equal(t, testEventId, event.EventID)
				assert.Equal(t, "insert", event.StateChangeType)
				assert.Equal(t, int64(1428591600), event.StateChangedAt)
				assert.Equal(t, testEmoteId, event.Id)
				assert.Empty(t, event.CodeOld)
				assert.Equal(t, testEmoteCode, event.CodeNew)
				assert.Empty(t, event.CreatedAtOld)
				assert.Equal(t, testCreatedAt, event.CreatedAtNew)
				assert.Empty(t, event.DomainOld)
				assert.Empty(t, event.DomainNew)
				assert.Empty(t, event.EmotesGroupOld)
				assert.Empty(t, event.EmotesGroupNew)
				assert.Empty(t, event.GroupIdOld)
				assert.Equal(t, testGroupId, event.GroupIdNew)
				assert.Empty(t, event.OrderOld)
				assert.Empty(t, event.OrderNew)
				assert.Empty(t, event.OwnerIdOld)
				assert.Equal(t, testOwnerId, event.OwnerIdNew)
				assert.Empty(t, event.PrefixOld)
				assert.Empty(t, event.PrefixNew)
				assert.Empty(t, event.StateOld)
				assert.Equal(t, testState, event.StateNew)
				assert.Empty(t, event.SuffixOld)
				assert.Equal(t, testSuffix, event.SuffixNew)
				assert.Empty(t, event.UpdatedAtOld)
				assert.Equal(t, testUpdatedAt, event.UpdatedAtNew)
				assert.Empty(t, event.AssetTypeOld)
				assert.Equal(t, assetType, event.AssetTypeNew)
				assert.Empty(t, event.AnimationTemplateOld)
				assert.Equal(t, animationTemplate, event.AnimationTemplateNew)
			},
		},
	}

	for _, test := range tests {
		t.Run(test.scenario, func(t *testing.T) {
			test.test(t)
		})
	}
}
