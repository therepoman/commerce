package dynamo

import (
	"context"

	mako "code.justin.tv/commerce/mako/internal"
	"code.justin.tv/commerce/mako/lambda/config"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
)

const (
	emotesState = "state"
	emoteID     = "id"
	emoteSetID  = "groupId"
	emoteCode   = "code"
	emoteType   = "emotesGroup"
)

// mini emote struct of fields we'll be using to generate the necessary s3 files for the all emotes export lambda
type ExportEmote struct {
	ID         string `dynamodbav:"id"`
	EmoteSetID string `dynamodbav:"groupId,omitempty"`
	Code       string `dynamodbav:"code"`
	EmoteType  string `dynamodbav:"emotesGroup,omitempty"`
}

//  allEmotesEmoteDB is an EmoteDB implementation used just for the all emotes export lambda
type allEmotesEmoteDB struct {
	table  *string
	client dynamodbiface.DynamoDBAPI
}

// IAllEmotesEmoteDAO is a mini dynamo implementation so we're able to scan the emotes table
type IAllEmotesEmoteDAO interface {
	ScanForActiveEmotes(ctx context.Context, totalSegments *int64, segment *int64) ([]ExportEmote, error)
}

// NewAllEmotesEmoteDB creates an db interface for use with teh all emotes export lambda
func NewAllEmotesEmoteDB(session *session.Session, cfg *config.Config) IAllEmotesEmoteDAO {
	conf := aws.Config{Region: aws.String(cfg.AWSRegion)}
	client := dynamodb.New(session, &conf)
	return &allEmotesEmoteDB{
		table:  &cfg.EmotesTable,
		client: client,
	}
}

// ScanForActiveEmotes scans the emotes table for active emotes and pulls only attributes needed for the all emotes export lambda
func (db *allEmotesEmoteDB) ScanForActiveEmotes(ctx context.Context, totalSegments *int64, segment *int64) ([]ExportEmote, error) {
	emotes := make([]ExportEmote, 0)

	builder := expression.NewBuilder().
		// only query for active emotes
		WithFilter(
			// active filter
			expression.Name(emotesState).Equal(expression.Value(mako.Active)),
		).
		// list of attributes to retrieve
		WithProjection(
			expression.NamesList(
				expression.Name(emoteID),
				expression.Name(emoteSetID),
				expression.Name(emoteCode),
				expression.Name(emoteType),
			),
		)
	expr, err := builder.Build()
	if err != nil {
		return nil, err
	}

	input := dynamodb.ScanInput{
		TableName:                 db.table,
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		FilterExpression:          expr.Filter(),
		ProjectionExpression:      expr.Projection(),
		// TotalSegments and Segment are used to execute parallel scans for a faster table scan
		TotalSegments: totalSegments,
		Segment:       segment,
	}

	if err := db.client.ScanPagesWithContext(ctx, &input, func(out *dynamodb.ScanOutput, lastPage bool) bool {
		for _, item := range out.Items {
			var emote ExportEmote
			if err := dynamodbattribute.UnmarshalMap(item, &emote); err != nil {
				continue
			}

			emotes = append(emotes, emote)
		}

		return !lastPage
	}); err != nil {
		return nil, err
	}

	return emotes, nil
}
