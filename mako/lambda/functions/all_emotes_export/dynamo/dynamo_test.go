package dynamo

import (
	"context"
	"fmt"
	"testing"

	dynamoMock "code.justin.tv/commerce/mako/mocks/github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func TestNewAllEmotesEmoteDB(t *testing.T) {
	t.Run("GIVEN emoteDB WHEN scanning for active emotes THEN return response with no error", func(t *testing.T) {
		//GIVEN
		dbMock := new(dynamoMock.DynamoDBAPI)
		emoteDAO := allEmotesEmoteDB{
			table:  nil,
			client: dbMock,
		}

		dbMock.On("ScanPagesWithContext", mock.Anything, mock.Anything, mock.AnythingOfType("func(*dynamodb.ScanOutput, bool) bool")).
			Return(nil)

		//WHEN
		var activeEmotes []ExportEmote
		activeEmotes, err := emoteDAO.ScanForActiveEmotes(context.Background(), nil, nil)

		//THEN
		assert.Nil(t, err)
		assert.NotNil(t, activeEmotes)
		assert.Equal(t, 0, len(activeEmotes))
	})
	t.Run("GIVEN emoteDB WHEN unable to scan for emotes THEN return error", func(t *testing.T) {
		//GIVEN
		dbMock := new(dynamoMock.DynamoDBAPI)
		emoteDAO := allEmotesEmoteDB{
			table:  nil,
			client: dbMock,
		}

		dbMock.On("ScanPagesWithContext", mock.Anything, mock.Anything, mock.AnythingOfType("func(*dynamodb.ScanOutput, bool) bool")).
			Return(fmt.Errorf("unable to scan emotes table"))

		//WHEN
		var activeEmotes []ExportEmote
		activeEmotes, err := emoteDAO.ScanForActiveEmotes(context.Background(), nil, nil)

		//THEN
		assert.Equal(t, fmt.Errorf("unable to scan emotes table"), err)
		assert.Nil(t, activeEmotes)
	})
}
