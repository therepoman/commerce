package main

import (
	"context"
	"fmt"
	"testing"

	"code.justin.tv/commerce/mako/lambda/functions/all_emotes_export/dynamo"
	EmoteDBMock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/lambda/functions/all_emotes_export/dynamo"
	mako_client "code.justin.tv/commerce/mako/twirp"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func Test_parallelScanEmotesTable(t *testing.T) {
	t.Run("GIVEN a set of parallel scan calls to the emotes table WHEN the parallel scan is called THEN expect no error", func(t *testing.T) {
		//GIVEN
		totalSegment = 1
		emoteChan := make(chan []dynamo.ExportEmote, totalSegment)
		dbMock := new(EmoteDBMock.IAllEmotesEmoteDAO)
		expectedResponse := map[int][]dynamo.ExportEmote{
			0: {
				{
					ID:         "1",
					EmoteSetID: "2",
					Code:       "code1",
					EmoteType:  mako_client.EmoteGroup_HypeTrain.String(),
				},
			},
		}
		//should expect exactly one call to ScanForActiveEmotes
		dbMock.On("ScanForActiveEmotes", mock.Anything, mock.Anything, mock.Anything).
			Return(expectedResponse[0], nil).Once()

		//WHEN
		err := parallelScanEmotesTable(context.Background(), dbMock, emoteChan)

		//THEN
		assert.Nil(t, err)
		resp := <-emoteChan
		//should only expect the first response
		assert.NotEqual(t, expectedResponse, resp)
	})

	t.Run("GIVEN a set of parallel scan calls to the emotes table WHEN the parallel scan fails THEN return an error", func(t *testing.T) {
		//GIVEN
		totalSegment = 1
		emoteChan := make(chan []dynamo.ExportEmote, totalSegment)
		dbMock := new(EmoteDBMock.IAllEmotesEmoteDAO)

		//should expect exactly one call to ScanForActiveEmotes
		dbMock.On("ScanForActiveEmotes", mock.Anything, mock.Anything, mock.Anything).
			Return(nil, fmt.Errorf("ScanForActiveEmotes failed")).Once()

		//WHEN
		err := parallelScanEmotesTable(context.Background(), dbMock, emoteChan)

		//THEN
		assert.Equal(t, fmt.Errorf("ScanForActiveEmotes failed"), err)
	})
}

func Test_buildJSONResponses(t *testing.T) {
	t.Run("GIVEN different emoticon formats WHEN building json responses THEN successfully build json", func(t *testing.T) {
		totalSegment = 1
		// GIVEN
		emoteChan := make(chan []dynamo.ExportEmote, totalSegment)
		emotes := []dynamo.ExportEmote{
			{
				ID:         "1",
				EmoteSetID: "2",
				Code:       "3",
				EmoteType:  mako_client.EmoteGroup_Subscriptions.String(),
			},
		}
		emoteChan <- emotes

		// WHEN
		resp, err := buildJSONResponses(emoteChan)

		// THEN
		assert.Nil(t, err)
		assert.Equal(t, "{\"emoticons\":[{\"id\":\"1\",\"emoticon_set_id\":\"2\",\"image_url_1x\":\"https://static-cdn.jtvnw.net/emoticons/v1/1/1.0\",\"name\":\"3\",\"emote_type\":\"subscriptions\"}]}", string(resp.AllEmoticons))
		assert.Equal(t, "{\"_links\":{\"self\":\"http://api.twitch.tv/kraken/chat/emoticons\"},\"emoticons\":[{\"id\":1,\"regex\":\"3\",\"images\":{\"emoticon_set\":2,\"height\":28,\"width\":28,\"url\":\"https://static-cdn.jtvnw.net/emoticons/v1/1/1.0\"}}]}", string(resp.Emoticons))
		assert.Equal(t, "{\"emoticons\":[{\"code\":\"3\",\"emoticon_set\":2,\"id\":1}]}", string(resp.EmoticonImages))
		assert.Equal(t, "{\"emoticon_sets\":{\"2\":[{\"code\":\"3\",\"id\":1}]}}", string(resp.EmoticonImagesAll))
	})
}
