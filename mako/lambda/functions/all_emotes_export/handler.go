package main

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"

	"code.justin.tv/commerce/mako/clients/s3manager"
	"code.justin.tv/commerce/mako/lambda/functions/all_emotes_export/dynamo"
	exportModels "code.justin.tv/commerce/mako/lambda/functions/all_emotes_export/models"

	"github.com/aws/aws-sdk-go/aws"
	awsS3 "github.com/aws/aws-sdk-go/service/s3"
	awsS3Manager "github.com/aws/aws-sdk-go/service/s3/s3manager"
	"github.com/pkg/errors"
	"golang.org/x/sync/errgroup"
)

type lambdaHandler struct {
	uploader *s3manager.UploaderImpl
	bucket   string
	emotesDB dynamo.IAllEmotesEmoteDAO
}

var (
	// totalSegment specifies the number of parallel scans against the emotes table
	totalSegment = int64(10)
	// S3 keys
	allEmoticonsS3Key      = fmt.Sprintf("%s/all_emoticons", emoticonsAllS3Prefix)
	emoticonsS3Key         = fmt.Sprintf("%s/emoticons", emoticonsAllS3Prefix)
	emoticonImagesS3Key    = fmt.Sprintf("%s/emoticon_images", emoticonsAllS3Prefix)
	emoticonImagesAllS3Key = fmt.Sprintf("%s/emoticon_images_all", emoticonsAllS3Prefix)
)

const (
	emoticonsAllS3Prefix      = "emoticonsall"
	emoticonS3FileContentType = "application/json"
)

//HandleRequest scans the emotes table for all active emotes and bundles them into json files for consumption by GetLegacyEmoticonAllURLs
func (h *lambdaHandler) HandleRequest(ctx context.Context) (string, error) {
	// create emote buffered channel to accept results of parallel scans for use later
	emoteChan := make(chan []dynamo.ExportEmote, totalSegment)
	// parallel scan emotes table
	err := parallelScanEmotesTable(ctx, h.emotesDB, emoteChan)
	if err != nil {
		return "", errors.Wrap(err, "failed to scan emotes table")
	}

	// build json responses to write to s3
	responses, err := buildJSONResponses(emoteChan)
	if err != nil {
		return "", errors.Wrap(err, "failed to build json responses")
	}
	// write files to s3
	err = uploadToS3(ctx, h.uploader, h.bucket, allEmoticonsS3Key, responses.AllEmoticons)
	if err != nil {
		return "", errors.Wrap(err, fmt.Sprintf("failed to upload %s", allEmoticonsS3Key))
	}
	err = uploadToS3(ctx, h.uploader, h.bucket, emoticonsS3Key, responses.Emoticons)
	if err != nil {
		return "", errors.Wrap(err, fmt.Sprintf("failed to upload %s", emoticonsS3Key))
	}
	err = uploadToS3(ctx, h.uploader, h.bucket, emoticonImagesS3Key, responses.EmoticonImages)
	if err != nil {
		return "", errors.Wrap(err, fmt.Sprintf("failed to upload %s", emoticonImagesS3Key))
	}
	err = uploadToS3(ctx, h.uploader, h.bucket, emoticonImagesAllS3Key, responses.EmoticonImagesAll)
	if err != nil {
		return "", errors.Wrap(err, fmt.Sprintf("failed to upload %s", emoticonImagesAllS3Key))
	}

	return "complete", nil
}

func parallelScanEmotesTable(ctx context.Context, emotesDB dynamo.IAllEmotesEmoteDAO, emoteChan chan []dynamo.ExportEmote) error {
	// set up errgroup to collect errors from goroutines
	var eg errgroup.Group
	for s := int64(0); s < totalSegment; s++ {
		// spin each scan segment off into its own goroutine
		segment := s
		eg.Go(func() error {
			// scan emotes table
			emotes, err := emotesDB.ScanForActiveEmotes(ctx, &totalSegment, &segment)
			// send emotes response to emote channel
			emoteChan <- emotes
			// return err
			return err
		})
	}
	// return the first non-nil error if exists from the goroutines
	if err := eg.Wait(); err != nil {
		return err
	}
	return nil
}

type responses struct {
	AllEmoticons      []byte
	Emoticons         []byte
	EmoticonImages    []byte
	EmoticonImagesAll []byte
}

func buildJSONResponses(emoteChan chan []dynamo.ExportEmote) (responses, error) {
	var allEmoticonsResult exportModels.AllEmotes

	// pull emotes out of emoteChan and build results
	for i := int64(0); i < totalSegment; i++ {
		e := <-emoteChan
		allEmoticonsResult = exportModels.AddEmotesToAllEmotes(allEmoticonsResult, e...)
	}
	legacyEmoticonsResult, err := exportModels.ConvertAllEmoticonsToLegacyEmoticons(allEmoticonsResult.Emoticons)
	if err != nil {
		return responses{}, err
	}
	emoticonImagesResult, err := exportModels.ConvertAllEmoticonsToEmoticonImages(allEmoticonsResult.Emoticons)
	if err != nil {
		return responses{}, err
	}
	emoticonImagesAllResult, err := exportModels.ConvertAllEmoticonsToEmoticonImagesAll(allEmoticonsResult.Emoticons)
	if err != nil {
		return responses{}, err
	}
	var jsonResponses responses
	// turn results into json
	jsonResponses.AllEmoticons, err = json.Marshal(allEmoticonsResult)
	if err != nil {
		return responses{}, err
	}
	jsonResponses.Emoticons, err = json.Marshal(legacyEmoticonsResult)
	if err != nil {
		return responses{}, err
	}
	jsonResponses.EmoticonImages, err = json.Marshal(emoticonImagesResult)
	if err != nil {
		return responses{}, err
	}
	jsonResponses.EmoticonImagesAll, err = json.Marshal(emoticonImagesAllResult)
	if err != nil {
		return responses{}, err
	}
	return jsonResponses, nil
}

func uploadToS3(ctx context.Context, uploader *s3manager.UploaderImpl, bucket string, key string, body []byte) error {
	err := uploader.Upload(ctx, awsS3Manager.UploadInput{
		Body:        bytes.NewReader(body),
		Bucket:      aws.String(bucket),
		Key:         aws.String(key),
		ACL:         aws.String(awsS3.ObjectCannedACLPublicRead),
		ContentType: aws.String(emoticonS3FileContentType),
	})
	if err != nil {
		return err
	}
	return nil
}
