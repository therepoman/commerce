package main

import (
	"code.justin.tv/commerce/mako/clients/s3manager"
	"code.justin.tv/commerce/mako/lambda/config"
	"code.justin.tv/commerce/mako/lambda/functions/all_emotes_export/dynamo"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws/session"
)

func main() {
	cfg := config.Load()

	awsSession, err := session.NewSession()
	if err != nil {
		panic(err)
	}

	emotesDB := dynamo.NewAllEmotesEmoteDB(awsSession, cfg)
	uploader := s3manager.NewUploader(awsSession)

	h := lambdaHandler{
		emotesDB: emotesDB,
		uploader: uploader,
		bucket:   cfg.EmoteUploadsBucket,
	}
	lambda.Start(h.HandleRequest)
}
