package models

type LegacyEmoticonsImageResponse struct {
	Emoticons []LegacyEmoticonImagesEmoticon `json:"emoticons"`
}

type LegacyEmoticonImagesEmoticon struct {
	Code        string `json:"code"`
	EmoticonSet int32  `json:"emoticon_set"`
	ID          int32  `json:"id"`
}

func ConvertAllEmoticonsToEmoticonImages(allEmoticons []*EmoteForAllEmotes) (*LegacyEmoticonsImageResponse, error) {
	var emotes []LegacyEmoticonImagesEmoticon
	for _, emote := range allEmoticons {
		emoteID, emoteSetID, err := getEmoteAndEmoteSetID(emote.Id, emote.EmoteSetID)
		if err != nil {
			//ignore emoticons that don't have int IDs or SetIDs
			continue
		}

		emotes = append(emotes, LegacyEmoticonImagesEmoticon{
			Code:        emote.Name,
			EmoticonSet: emoteSetID,
			ID:          emoteID,
		})
	}

	return &LegacyEmoticonsImageResponse{
		Emoticons: emotes,
	}, nil
}
