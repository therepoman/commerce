package models

import (
	"strconv"

	"code.justin.tv/commerce/mako/clients/imageuploader"
	"github.com/pkg/errors"
)

const (
	EmoticonsLink = "http://api.twitch.tv/kraken/chat/emoticons"
)

type LegacyEmoticonsResponse struct {
	XLinks    Links                     `json:"_links"`
	Emoticons []LegacyEmoticonsEmoticon `json:"emoticons"`
}

type Links struct {
	Self string `json:"self"`
}

type LegacyEmoticonsEmoticon struct {
	ID     int32                `json:"id"`
	Regex  string               `json:"regex"`
	Images LegacyEmoticonsImage `json:"images"`
}

type LegacyEmoticonsImage struct {
	EmoticonSet int32  `json:"emoticon_set"`
	Height      int32  `json:"height"`
	Width       int32  `json:"width"`
	Url         string `json:"url"`
}

func ConvertAllEmoticonsToLegacyEmoticons(allEmoticons []*EmoteForAllEmotes) (*LegacyEmoticonsResponse, error) {
	var emoticons []LegacyEmoticonsEmoticon
	for _, emote := range allEmoticons {
		id, emoticonSetID, err := getEmoteAndEmoteSetID(emote.Id, emote.EmoteSetID)
		if err != nil {
			//ignore emoticons that don't have int IDs or SetIDs
			continue
		}

		emoticons = append(emoticons, LegacyEmoticonsEmoticon{
			ID:    int32(id),
			Regex: emote.Name,
			Images: LegacyEmoticonsImage{
				EmoticonSet: int32(emoticonSetID),
				Height:      int32(imageuploader.Size1xDimension),
				Width:       int32(imageuploader.Size1xDimension),
				Url:         emote.ImageUrl1X,
			},
		})
	}

	return &LegacyEmoticonsResponse{
		XLinks: Links{
			Self: EmoticonsLink,
		},
		Emoticons: emoticons,
	}, nil
}

func getEmoteAndEmoteSetID(emoteID, emoticonSetID string) (int32, int32, error) {
	id, err := strconv.Atoi(emoteID)
	if err != nil {
		return 0, 0, errors.Wrap(err, "Error converting emoticon ID to int")
	}

	emoteSetID, err := strconv.Atoi(emoticonSetID)
	if err != nil {
		return 0, 0, errors.Wrap(err, "Error converting emoticonSetID to int")
	}

	return int32(id), int32(emoteSetID), nil
}
