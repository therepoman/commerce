package models

import (
	"fmt"
	"strings"

	"code.justin.tv/commerce/mako/api"
	"code.justin.tv/commerce/mako/lambda/functions/all_emotes_export/dynamo"
	mako_client "code.justin.tv/commerce/mako/twirp"
)

// All emotes blob for S3 CDN
type AllEmotes struct {
	Emoticons []*EmoteForAllEmotes `protobuf:"bytes,1,rep,name=emoticons" json:"emoticons,omitempty"`
}

// An emote structure for the AllEmotes S3 CDN
type EmoteForAllEmotes struct {
	Id         string `protobuf:"bytes,1,opt,name=id" json:"id,omitempty"`
	EmoteSetID string `protobuf:"bytes,2,opt,name=emoticon_set_id,json=emoticonSetId" json:"emoticon_set_id,omitempty"`
	ImageUrl1X string `protobuf:"bytes,3,opt,name=image_url_1x" json:"image_url_1x,omitempty"`
	Name       string `protobuf:"bytes,4,opt,name=name" json:"name,omitempty"`
	EmoteType  string `protobuf:"bytes,5,opt,name=emote_type" json:"emote_type,omitempty"`
}

const bitsTier = "bitstier"

func AddEmotesToAllEmotes(allEmotes AllEmotes, emotes ...dynamo.ExportEmote) AllEmotes {
	for _, emote := range emotes {
		emoteType := emote.EmoteType
		//hide MegaCommerce and BitsBadgeTierEmotes from the public: https://jira.xarth.tv/browse/HLX-1771
		//TODO: remove the megacommerce mapping once this backfill is complete: https://jira.xarth.tv/browse/DIGI-1109
		if emote.EmoteType == mako_client.EmoteGroup_MegaCommerce.String() {
			emoteType = mako_client.EmoteGroup_LimitedTime.String()
		}
		if emote.EmoteType == mako_client.EmoteGroup_BitsBadgeTierEmotes.String() {
			emoteType = bitsTier
		}
		allEmotes.Emoticons = append(allEmotes.Emoticons, &EmoteForAllEmotes{
			Id:         emote.ID,
			Name:       emote.Code,
			EmoteType:  strings.ToLower(emoteType),
			EmoteSetID: emote.EmoteSetID,
			ImageUrl1X: fmt.Sprintf(api.EmoticonUrl, emote.ID),
		})
	}
	return allEmotes
}
