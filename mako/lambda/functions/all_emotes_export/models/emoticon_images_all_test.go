package models

import (
	"testing"

	mako_client "code.justin.tv/commerce/mako/twirp"
	"github.com/stretchr/testify/assert"
)

func Test_ConvertAllEmotesToEmoticonImagesAll(t *testing.T) {
	t.Run("GIVEN emotes with int and non-int IDs and SetIDs WHEN building EmoticonImagesAll THEN exclude emotes with non-int IDs and SetIDs", func(t *testing.T) {
		// GIVEN
		allEmotes := AllEmotes{
			Emoticons: []*EmoteForAllEmotes{
				{
					Id:         "1",
					EmoteSetID: "2",
					ImageUrl1X: "https://static-cdn.jtvnw.net/emoticons/v1/1/1.0",
					Name:       "code1",
					EmoteType:  mako_client.EmoteGroup_Subscriptions.String(),
				},
				{
					Id:         "2",
					EmoteSetID: "2",
					ImageUrl1X: "https://static-cdn.jtvnw.net/emoticons/v1/2/1.0",
					Name:       "code8",
					EmoteType:  mako_client.EmoteGroup_BitsBadgeTierEmotes.String(),
				},
				{
					Id:         "not-an-int",
					EmoteSetID: "3",
					ImageUrl1X: "https://static-cdn.jtvnw.net/emoticons/v1/not-an-int/1.0",
					Name:       "code4",
					EmoteType:  mako_client.EmoteGroup_ChannelPoints.String(),
				},
				{
					Id:         "5",
					EmoteSetID: "not-an-int",
					ImageUrl1X: "https://static-cdn.jtvnw.net/emoticons/v1/5/1.0",
					Name:       "code6",
					EmoteType:  mako_client.EmoteGroup_HypeTrain.String(),
				},
			},
		}

		// WHEN
		emoticonImagesAll, err := ConvertAllEmoticonsToEmoticonImagesAll(allEmotes.Emoticons)

		// THEN
		assert.Nil(t, err)
		assert.Equal(t, 1, len(emoticonImagesAll.EmoticonSets))
		assert.Equal(t, 2, len(emoticonImagesAll.EmoticonSets["2"]))

		assert.Equal(t, int32(1), emoticonImagesAll.EmoticonSets["2"][0].ID)
		assert.Equal(t, "code1", emoticonImagesAll.EmoticonSets["2"][0].Code)

		assert.Equal(t, int32(2), emoticonImagesAll.EmoticonSets["2"][1].ID)
		assert.Equal(t, "code8", emoticonImagesAll.EmoticonSets["2"][1].Code)
	})
}
