package models

import (
	"fmt"
	"testing"

	"code.justin.tv/commerce/mako/api"
	"code.justin.tv/commerce/mako/lambda/functions/all_emotes_export/dynamo"
	mako_client "code.justin.tv/commerce/mako/twirp"
	"github.com/stretchr/testify/assert"
)

func Test_AddEmotesToAllEmotes(t *testing.T) {
	t.Run("GIVEN emotes WHEN building AllEmotes THEN properly generateAllEmotes", func(t *testing.T) {
		// GIVEN
		var allEmotes = AllEmotes{}
		emotes := []dynamo.ExportEmote{
			{
				ID:         "1",
				EmoteSetID: "2",
				Code:       "code1",
				EmoteType:  mako_client.EmoteGroup_BitsBadgeTierEmotes.String(),
			},
			{
				ID:         "not_a_valid_int",
				EmoteSetID: "2",
				Code:       "code2",
				EmoteType:  mako_client.EmoteGroup_Subscriptions.String(),
			},
			{
				ID:         "3",
				EmoteSetID: "not_a_valid_int",
				Code:       "code3",
				EmoteType:  mako_client.EmoteGroup_HypeTrain.String(),
			},
			{
				ID:         "4",
				EmoteSetID: "5",
				Code:       "code4",
				EmoteType:  mako_client.EmoteGroup_MegaCommerce.String(),
			},
		}

		// WHEN
		allEmotes = AddEmotesToAllEmotes(allEmotes, emotes...)

		// THEN
		assert.Equal(t, 4, len(allEmotes.Emoticons))

		assert.Equal(t, "1", allEmotes.Emoticons[0].Id)
		assert.Equal(t, "2", allEmotes.Emoticons[0].EmoteSetID)
		assert.Equal(t, "code1", allEmotes.Emoticons[0].Name)
		assert.Equal(t, "bitstier", allEmotes.Emoticons[0].EmoteType)
		assert.Equal(t, fmt.Sprintf(api.EmoticonUrl, "1"), allEmotes.Emoticons[0].ImageUrl1X)

		assert.Equal(t, "not_a_valid_int", allEmotes.Emoticons[1].Id)
		assert.Equal(t, "2", allEmotes.Emoticons[1].EmoteSetID)
		assert.Equal(t, "code2", allEmotes.Emoticons[1].Name)
		assert.Equal(t, "subscriptions", allEmotes.Emoticons[1].EmoteType)
		assert.Equal(t, fmt.Sprintf(api.EmoticonUrl, "not_a_valid_int"), allEmotes.Emoticons[1].ImageUrl1X)

		assert.Equal(t, "3", allEmotes.Emoticons[2].Id)
		assert.Equal(t, "not_a_valid_int", allEmotes.Emoticons[2].EmoteSetID)
		assert.Equal(t, "code3", allEmotes.Emoticons[2].Name)
		assert.Equal(t, "hypetrain", allEmotes.Emoticons[2].EmoteType)
		assert.Equal(t, fmt.Sprintf(api.EmoticonUrl, "3"), allEmotes.Emoticons[2].ImageUrl1X)

		assert.Equal(t, "4", allEmotes.Emoticons[3].Id)
		assert.Equal(t, "5", allEmotes.Emoticons[3].EmoteSetID)
		assert.Equal(t, "code4", allEmotes.Emoticons[3].Name)
		assert.Equal(t, "limitedtime", allEmotes.Emoticons[3].EmoteType)
		assert.Equal(t, fmt.Sprintf(api.EmoticonUrl, "4"), allEmotes.Emoticons[3].ImageUrl1X)
	})
}
