package models

import (
	"testing"

	mako_client "code.justin.tv/commerce/mako/twirp"
	"github.com/stretchr/testify/assert"
)

func Test_ConvertAllEmoticonsToLegacyEmoticons(t *testing.T) {
	t.Run("GIVEN emotes with int and non-int IDs and SetIDs WHEN building LegacyEmoticons THEN exclude emotes with non-int IDs and SetIDs", func(t *testing.T) {
		// GIVEN
		allEmotes := AllEmotes{
			Emoticons: []*EmoteForAllEmotes{
				{
					Id:         "1",
					EmoteSetID: "2",
					ImageUrl1X: "https://static-cdn.jtvnw.net/emoticons/v1/1/1.0",
					Name:       "code1",
					EmoteType:  mako_client.EmoteGroup_HypeTrain.String(),
				},
				{
					Id:         "not-an-int",
					EmoteSetID: "3",
					ImageUrl1X: "https://static-cdn.jtvnw.net/emoticons/v1/not-an-int/1.0",
					Name:       "code4",
					EmoteType:  mako_client.EmoteGroup_HypeTrain.String(),
				},
				{
					Id:         "5",
					EmoteSetID: "not-an-int",
					ImageUrl1X: "https://static-cdn.jtvnw.net/emoticons/v1/5/1.0",
					Name:       "code6",
					EmoteType:  mako_client.EmoteGroup_HypeTrain.String(),
				},
			},
		}

		// WHEN
		legacyEmoticons, err := ConvertAllEmoticonsToLegacyEmoticons(allEmotes.Emoticons)

		// THEN
		assert.Nil(t, err)
		assert.Equal(t, 1, len(legacyEmoticons.Emoticons))
		assert.Equal(t, int32(1), legacyEmoticons.Emoticons[0].ID)
		assert.Equal(t, "code1", legacyEmoticons.Emoticons[0].Regex)
		assert.Equal(t, int32(2), legacyEmoticons.Emoticons[0].Images.EmoticonSet)
		assert.Equal(t, int32(28), legacyEmoticons.Emoticons[0].Images.Height)
		assert.Equal(t, int32(28), legacyEmoticons.Emoticons[0].Images.Width)
		assert.Equal(t, "https://static-cdn.jtvnw.net/emoticons/v1/1/1.0", legacyEmoticons.Emoticons[0].Images.Url)
	})
}
