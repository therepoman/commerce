package models

import (
	"testing"

	mako_client "code.justin.tv/commerce/mako/twirp"
	"github.com/stretchr/testify/assert"
)

func Test_ConvertAllEmotesToEmoticonImages(t *testing.T) {
	t.Run("GIVEN emotes with int and non-int IDs and SetIDs WHEN building EmoticonImages THEN exclude emotes with non-int IDs and SetIDs", func(t *testing.T) {
		// GIVEN
		allEmotes := AllEmotes{
			Emoticons: []*EmoteForAllEmotes{
				{
					Id:         "1",
					EmoteSetID: "2",
					ImageUrl1X: "https://static-cdn.jtvnw.net/emoticons/v1/1/1.0",
					Name:       "code1",
					EmoteType:  mako_client.EmoteGroup_HypeTrain.String(),
				},
				{
					Id:         "not-an-int",
					EmoteSetID: "3",
					ImageUrl1X: "https://static-cdn.jtvnw.net/emoticons/v1/not-an-int/1.0",
					Name:       "code4",
					EmoteType:  mako_client.EmoteGroup_HypeTrain.String(),
				},
				{
					Id:         "5",
					EmoteSetID: "not-an-int",
					ImageUrl1X: "https://static-cdn.jtvnw.net/emoticons/v1/5/1.0",
					Name:       "code6",
					EmoteType:  mako_client.EmoteGroup_HypeTrain.String(),
				},
			},
		}

		// WHEN
		emoticonImages, err := ConvertAllEmoticonsToEmoticonImages(allEmotes.Emoticons)

		// THEN
		assert.Nil(t, err)
		assert.Equal(t, 1, len(emoticonImages.Emoticons))
		assert.Equal(t, int32(1), emoticonImages.Emoticons[0].ID)
		assert.Equal(t, int32(2), emoticonImages.Emoticons[0].EmoticonSet)
		assert.Equal(t, "code1", emoticonImages.Emoticons[0].Code)
	})
}
