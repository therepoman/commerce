package models

type LegacyEmoticonImagesAllResponse struct {
	EmoticonSets map[string][]LegacyEmoticonImagesAllEmoticon `json:"emoticon_sets"`
}

type LegacyEmoticonImagesAllEmoticon struct {
	Code string `json:"code"`
	ID   int32  `json:"id"`
}

func ConvertAllEmoticonsToEmoticonImagesAll(allEmoticons []*EmoteForAllEmotes) (*LegacyEmoticonImagesAllResponse, error) {
	emoticonSets := make(map[string][]LegacyEmoticonImagesAllEmoticon)
	for _, emote := range allEmoticons {
		emoteID, _, err := getEmoteAndEmoteSetID(emote.Id, emote.EmoteSetID)
		if err != nil {
			//ignore emoticons that don't have int IDs or SetIDs
			continue
		}

		emoticonSets[emote.EmoteSetID] = append(emoticonSets[emote.EmoteSetID], LegacyEmoticonImagesAllEmoticon{
			Code: emote.Name,
			ID:   int32(emoteID),
		})
	}

	return &LegacyEmoticonImagesAllResponse{
		EmoticonSets: emoticonSets,
	}, nil
}
