package main

import (
	"context"
	"errors"
	"testing"
	"time"

	"github.com/aws/aws-lambda-go/events"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestPublishEmoteModifierGroupsSpadeEvent_HandleEvent(t *testing.T) {
	t.Run("GIVEN multiple valid records WHEN handle event THEN send multiple records to spade", func(t *testing.T) {
		// GIVEN
		s := &spadeFake{}
		h := lambdaHandler{s}
		e := makeValidEvent(2)

		// WHEN
		err := h.Handler(context.Background(), e)

		// THEN
		assert.NoError(t, err)
		assert.Len(t, s.events, 2)
	})

	t.Run("GIVEN invalid record WHEN handle event THEN that event is not sent to spade", func(t *testing.T) {
		// GIVEN
		s := &spadeFake{}
		h := lambdaHandler{s}
		e := events.DynamoDBEvent{
			Records: []events.DynamoDBEventRecord{

				{
					EventID: "anyID1",
					Change:  makeValidStreamRecord(),
				},
				{
					EventID: "anyID2",
					// Record is missing change data
				},
			},
		}

		// WHEN
		err := h.Handler(context.Background(), e)

		// THEN
		assert.NoError(t, err)
		assert.Len(t, s.events, 1)
	})

	t.Run("GIVEN error calling spade WHEN handle event THEN return error", func(t *testing.T) {
		// GIVEN
		s := &spadeFake{}
		s.returnErr = errors.New("fake error")
		h := lambdaHandler{s}
		e := makeValidEvent(1)

		// WHEN
		err := h.Handler(context.Background(), e)

		// THEN
		assert.Error(t, err)
	})
}

func TestPublishEmoteModifierGroupsSpadeEvent_Validate(t *testing.T) {
	tests := []struct {
		scenario string
		test     func(t *testing.T)
	}{
		{
			scenario: "GIVEN insert (old_image is omitted) WHEN validate record THEN no error",
			test: func(t *testing.T) {
				// GIVEN
				h := lambdaHandler{&spadeFake{}}
				r := events.DynamoDBEventRecord{
					EventID:   "anyEventID",
					EventName: string(events.DynamoDBOperationTypeInsert),
					Change: events.DynamoDBStreamRecord{
						ApproximateCreationDateTime: events.SecondsEpochTime{
							Time: time.Date(2015, time.April, 9, 15, 0, 0, 0, time.UTC),
						},
						// OldImage is omitted
						NewImage: map[string]events.DynamoDBAttributeValue{
							idAttribute:      events.NewStringAttribute("newEmgID"),
							ownerIDAttribute: events.NewStringAttribute("newOwnerID"),
							modifiersAttribute: events.NewListAttribute([]events.DynamoDBAttributeValue{
								events.NewStringAttribute("HF"),
								events.NewStringAttribute("BW"),
							}),
							srcEmoteGroupsAttribute: events.NewListAttribute([]events.DynamoDBAttributeValue{
								events.NewStringAttribute("newEmoteGroup1"),
								events.NewStringAttribute("newEmoteGroup2"),
							}),
						},
					},
				}

				// WHEN
				event, err := h.validate(r)

				// THEN
				require.NoError(t, err)
				assert.Equal(t, "DynamoDB", event.StorageType)
				assert.Equal(t, "anyEventID", event.EventID)
				assert.Equal(t, "insert", event.StateChangeType)
				assert.Equal(t, int64(1428591600), event.StateChangedAt)
				assert.Equal(t, "newOwnerID", event.OwnerID)
				assert.Empty(t, event.OldID)
				assert.Empty(t, event.OldModifiers)
				assert.Empty(t, event.OldSrcEmoteGroups)
				assert.Equal(t, "newEmgID", event.NewID)
				assert.Equal(t, toJson([]string{"HF", "BW"}), event.NewModifiers)
				assert.Equal(t, toJson([]string{"newEmoteGroup1", "newEmoteGroup2"}), event.NewSrcEmoteGroups)
			},
		},
		{
			scenario: "GIVEN delete (new_image is omitted) WHEN validate record THEN no error",
			test: func(t *testing.T) {
				// GIVEN
				h := lambdaHandler{&spadeFake{}}
				r := events.DynamoDBEventRecord{
					EventID:   "anyEventID",
					EventName: string(events.DynamoDBOperationTypeRemove),
					Change: events.DynamoDBStreamRecord{
						ApproximateCreationDateTime: events.SecondsEpochTime{
							Time: time.Date(2015, time.April, 9, 15, 0, 0, 0, time.UTC),
						},
						OldImage: map[string]events.DynamoDBAttributeValue{
							idAttribute:      events.NewStringAttribute("oldEmgID"),
							ownerIDAttribute: events.NewStringAttribute("oldOwnerID"),
							modifiersAttribute: events.NewListAttribute([]events.DynamoDBAttributeValue{
								events.NewStringAttribute("SG"),
							}),
							srcEmoteGroupsAttribute: events.NewListAttribute([]events.DynamoDBAttributeValue{
								events.NewStringAttribute("oldEmoteGroup1"),
								events.NewStringAttribute("oldEmoteGroup2"),
							}),
						},
						// NewImage is omitted
					},
				}

				// WHEN
				event, err := h.validate(r)

				// THEN
				require.NoError(t, err)
				assert.Equal(t, "DynamoDB", event.StorageType)
				assert.Equal(t, "anyEventID", event.EventID)
				assert.Equal(t, "delete", event.StateChangeType)
				assert.Equal(t, int64(1428591600), event.StateChangedAt)
				assert.Equal(t, "oldOwnerID", event.OwnerID)
				assert.Equal(t, "oldEmgID", event.OldID)
				assert.Equal(t, toJson([]string{"SG"}), event.OldModifiers)
				assert.Equal(t, toJson([]string{"oldEmoteGroup1", "oldEmoteGroup2"}), event.OldSrcEmoteGroups)
				assert.Empty(t, event.NewID)
				assert.Empty(t, event.NewModifiers)
				assert.Empty(t, event.NewSrcEmoteGroups)
			},
		},
		{
			scenario: "GIVEN update (both images included) WHEN validate record THEN no error",
			test: func(t *testing.T) {
				// GIVEN
				h := lambdaHandler{&spadeFake{}}
				r := events.DynamoDBEventRecord{
					EventID:   "anyEventID",
					EventName: string(events.DynamoDBOperationTypeModify),
					Change: events.DynamoDBStreamRecord{
						ApproximateCreationDateTime: events.SecondsEpochTime{
							Time: time.Date(2015, time.April, 9, 15, 0, 0, 0, time.UTC),
						},
						OldImage: map[string]events.DynamoDBAttributeValue{
							idAttribute:      events.NewStringAttribute("oldEmgID"),
							ownerIDAttribute: events.NewStringAttribute("oldOwnerID"),
							modifiersAttribute: events.NewListAttribute([]events.DynamoDBAttributeValue{
								events.NewStringAttribute("SG"),
							}),
							srcEmoteGroupsAttribute: events.NewListAttribute([]events.DynamoDBAttributeValue{
								events.NewStringAttribute("oldEmoteGroup1"),
								events.NewStringAttribute("oldEmoteGroup2"),
							}),
						},
						NewImage: map[string]events.DynamoDBAttributeValue{
							idAttribute:      events.NewStringAttribute("newEmgID"),
							ownerIDAttribute: events.NewStringAttribute("newOwnerID"),
							modifiersAttribute: events.NewListAttribute([]events.DynamoDBAttributeValue{
								events.NewStringAttribute("HF"),
								events.NewStringAttribute("TK"),
							}),
							srcEmoteGroupsAttribute: events.NewListAttribute([]events.DynamoDBAttributeValue{
								events.NewStringAttribute("newEmoteGroup1"),
								events.NewStringAttribute("newEmoteGroup2"),
							}),
						},
					},
				}

				// WHEN
				event, err := h.validate(r)

				// THEN
				require.NoError(t, err)
				assert.Equal(t, "DynamoDB", event.StorageType)
				assert.Equal(t, "anyEventID", event.EventID)
				assert.Equal(t, "update", event.StateChangeType)
				assert.Equal(t, int64(1428591600), event.StateChangedAt)
				assert.Equal(t, "newOwnerID", event.OwnerID)
				assert.Equal(t, "oldEmgID", event.OldID)
				assert.Equal(t, toJson([]string{"SG"}), event.OldModifiers)
				assert.Equal(t, toJson([]string{"oldEmoteGroup1", "oldEmoteGroup2"}), event.OldSrcEmoteGroups)
				assert.Equal(t, "newEmgID", event.NewID)
				assert.Equal(t, toJson([]string{"HF", "TK"}), event.NewModifiers)
				assert.Equal(t, toJson([]string{"newEmoteGroup1", "newEmoteGroup2"}), event.NewSrcEmoteGroups)
			},
		},
		{
			scenario: "GIVEN both images omitted WHEN validate record THEN return error",
			test: func(t *testing.T) {
				// GIVEN
				h := lambdaHandler{&spadeFake{}}
				r := events.DynamoDBEventRecord{
					EventID:   "anyEventID",
					EventName: string(events.DynamoDBOperationTypeModify),
					Change: events.DynamoDBStreamRecord{
						ApproximateCreationDateTime: events.SecondsEpochTime{
							Time: time.Date(2015, time.April, 9, 15, 0, 0, 0, time.UTC),
						},
						// both OldImage and NewImage are missing
					},
				}

				// WHEN
				event, err := h.validate(r)

				// THEN
				require.Error(t, err)
				assert.Contains(t, err.Error(), "unable to find ownerID")
				assert.Empty(t, event)
			},
		},
		{
			// If CreateEmoteModifierGroups or UpdateEmoteModifierGroups is called with an empty list, the data type
			// in Dynamo will become `NULL`, and with the value `true` :shrug:
			// So this tests that we can still send events if we see that sort of data.
			scenario: "GIVEN missing lists WHEN validate record THEN no error",
			test: func(t *testing.T) {
				// GIVEN
				h := lambdaHandler{&spadeFake{}}
				r := events.DynamoDBEventRecord{
					Change: events.DynamoDBStreamRecord{
						OldImage: map[string]events.DynamoDBAttributeValue{
							ownerIDAttribute:        events.NewStringAttribute("oldOwnerID"),
							modifiersAttribute:      events.NewNullAttribute(),
							srcEmoteGroupsAttribute: events.NewNullAttribute(),
						},
					},
				}

				// WHEN
				event, err := h.validate(r)

				// THEN
				require.NoError(t, err)
				assert.Equal(t, toJson([]string{}), event.OldModifiers)
				assert.Equal(t, toJson([]string{}), event.OldSrcEmoteGroups)
			},
		},
		{
			// As a precaution, this checks that we can process ownerIDs that are integers instead of strings
			scenario: "GIVEN ownerID is an integer WHEN validate record THEN no error",
			test: func(t *testing.T) {
				// GIVEN
				h := lambdaHandler{&spadeFake{}}
				r := events.DynamoDBEventRecord{
					Change: events.DynamoDBStreamRecord{
						OldImage: map[string]events.DynamoDBAttributeValue{
							ownerIDAttribute: events.NewNumberAttribute("1234567"),
						},
					},
				}

				// WHEN
				event, err := h.validate(r)

				// THEN
				require.NoError(t, err)
				assert.Equal(t, "1234567", event.OwnerID)
			},
		},
		{
			// As a precaution, this checks that we can process ownerIDs that are integers instead of strings
			scenario: "GIVEN ownerID is a float WHEN validate record THEN return error",
			test: func(t *testing.T) {
				// GIVEN
				h := lambdaHandler{&spadeFake{}}
				r := events.DynamoDBEventRecord{
					Change: events.DynamoDBStreamRecord{
						OldImage: map[string]events.DynamoDBAttributeValue{
							ownerIDAttribute: events.NewNumberAttribute("123.4f"),
						},
					},
				}

				// WHEN
				_, err := h.validate(r)

				// THEN
				require.Error(t, err)
				assert.Contains(t, err.Error(), "ownerID was number type but could not be parsed to integer")
			},
		},
		{
			scenario: "GIVEN ownerID is not a string or int WHEN validate record THEN return error",
			test: func(t *testing.T) {
				// GIVEN
				h := lambdaHandler{&spadeFake{}}
				r := events.DynamoDBEventRecord{
					Change: events.DynamoDBStreamRecord{
						OldImage: map[string]events.DynamoDBAttributeValue{
							ownerIDAttribute: events.NewBooleanAttribute(true),
						},
					},
				}

				// WHEN
				_, err := h.validate(r)

				// THEN
				require.Error(t, err)
				assert.Contains(t, err.Error(), "ownerID was not a supported type")
			},
		},
	}

	for _, test := range tests {
		t.Run(test.scenario, func(t *testing.T) {
			test.test(t)
		})
	}
}
