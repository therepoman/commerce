package main

import (
	"context"
	"encoding/json"
	"fmt"
	"os"
	"strings"

	"code.justin.tv/common/spade-client-go/spade"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/pkg/errors"
)

// These are the columns the data is in - see dynamo/emote_mods_db.go
const (
	idAttribute             = "id"
	ownerIDAttribute        = "ownerId"
	modifiersAttribute      = "modifiers"
	srcEmoteGroupsAttribute = "sourceEmoteGroupIds"

	spadeEventName = "state_change_emote_modifier_groups"
)

// This is the shape of the event to send.
// See: https://git.xarth.tv/pages/subs/docs/projects/modified-emotes/ - data tracking spec
// NOTE: the modifiers and srcEmoteGroups are actually lists, but need to be marshaled to json strings to be sent to spade.
type EmoteModifierGroupsSpadeEvent struct {
	OwnerID           string `json:"owner_id,omitempty"`
	StorageType       string `json:"metadata_store_type,omitempty"`
	EventID           string `json:"metadata_event_id,omitempty"`
	StateChangeType   string `json:"metadata_state_change_type,omitempty"`
	StateChangedAt    int64  `json:"metadata_state_changed_at,omitempty"`
	OldID             string `json:"old_id,omitempty"`
	NewID             string `json:"new_id,omitempty"`
	OldModifiers      string `json:"old_modifiers,omitempty"`
	NewModifiers      string `json:"new_modifiers,omitempty"`
	OldSrcEmoteGroups string `json:"old_src_emote_groups,omitempty"`
	NewSrcEmoteGroups string `json:"new_src_emote_groups,omitempty"`
}

type lambdaHandler struct {
	s spade.Client
}

func (h *lambdaHandler) Handler(ctx context.Context, e events.DynamoDBEvent) error {
	fmt.Printf("Received event with %v records\n", len(e.Records))

	spadeEvents := make([]spade.Event, 0)

	for _, record := range e.Records {
		// validate record
		event, err := h.validate(record)
		if err != nil {
			fmt.Printf("unable to process event record: %v\n", err)
			continue
		}
		spadeEvents = append(spadeEvents, spade.Event{
			Name:       spadeEventName,
			Properties: event,
		})
	}

	if len(spadeEvents) > 0 {
		if err := h.s.TrackEvents(ctx, spadeEvents...); err != nil {
			return errors.Wrap(err, "error sending spade events")
		}
	}

	return nil
}

func (h *lambdaHandler) validate(record events.DynamoDBEventRecord) (EmoteModifierGroupsSpadeEvent, error) {
	spadeEvent := EmoteModifierGroupsSpadeEvent{
		StorageType:     "DynamoDB",
		EventID:         record.EventID,
		StateChangeType: toStateChangeType(record),
		StateChangedAt:  record.Change.ApproximateCreationDateTime.UTC().Unix(),
	}

	if id, ok := record.Change.OldImage[idAttribute]; ok {
		if id.DataType() == events.DataTypeString {
			spadeEvent.OldID = id.String()
		}

	}
	if id, ok := record.Change.NewImage[idAttribute]; ok {
		if id.DataType() == events.DataTypeString {
			spadeEvent.NewID = id.String()
		}
	}
	if modifiers, ok := record.Change.OldImage[modifiersAttribute]; ok {
		spadeEvent.OldModifiers = toJson(toStringList(modifiers))
	}
	if modifiers, ok := record.Change.NewImage[modifiersAttribute]; ok {
		spadeEvent.NewModifiers = toJson(toStringList(modifiers))
	}
	if sourceEmoteGroups, ok := record.Change.OldImage[srcEmoteGroupsAttribute]; ok {
		spadeEvent.OldSrcEmoteGroups = toJson(toStringList(sourceEmoteGroups))
	}
	if sourceEmoteGroups, ok := record.Change.NewImage[srcEmoteGroupsAttribute]; ok {
		spadeEvent.NewSrcEmoteGroups = toJson(toStringList(sourceEmoteGroups))
	}

	// OwnerID is a special case, it only includes the new field
	// (unless not present due to delete - then fall back to old)
	ownerID, ok := record.Change.NewImage[ownerIDAttribute]
	if !ok {
		ownerID, ok = record.Change.OldImage[ownerIDAttribute]
		if !ok {
			return EmoteModifierGroupsSpadeEvent{}, errors.New("unable to find ownerID attribute")
		}
	}
	// This is to be flexible just in case the data in the table isn't consistent.
	switch ownerID.DataType() {
	case events.DataTypeString:
		spadeEvent.OwnerID = ownerID.String()
	case events.DataTypeNumber:
		val, err := ownerID.Integer()
		if err != nil {
			return EmoteModifierGroupsSpadeEvent{}, errors.New("ownerID was number type but could not be parsed to integer")
		}
		spadeEvent.OwnerID = fmt.Sprintf("%v", val)
	default:
		return EmoteModifierGroupsSpadeEvent{}, errors.New("ownerID was not a supported type")
	}

	return spadeEvent, nil
}

func toJson(list []string) string {
	bytes, err := json.Marshal(list)
	if err != nil {
		fmt.Printf("error marshaling string list: %v", err)
		return ""
	}
	return string(bytes)
}

func toStringList(attr events.DynamoDBAttributeValue) []string {
	if attr.DataType() != events.DataTypeList {
		return []string{}
	}
	attrList := attr.List()
	result := make([]string, 0, len(attrList))
	for _, item := range attrList {
		if item.DataType() == events.DataTypeString {
			result = append(result, item.String())
		}
	}
	return result
}

func toStateChangeType(record events.DynamoDBEventRecord) string {
	switch record.EventName {
	case string(events.DynamoDBOperationTypeModify):
		return "update"
	case string(events.DynamoDBOperationTypeInsert):
		return "insert"
	case string(events.DynamoDBOperationTypeRemove):
		return "delete"
	}
	return ""
}

func isProd() bool {
	env := strings.ToLower(os.Getenv("ENVIRONMENT"))
	if env == "prod" || env == "production" {
		return true
	}
	return false
}

// Simple no-op implementation of the spade client, used to make testing easier while only
// sending events in prod.
type spadeClientWrapper struct{}

func (scn *spadeClientWrapper) TrackEvent(ctx context.Context, event string, properties interface{}) error {
	return nil
}
func (scn *spadeClientWrapper) TrackEvents(ctx context.Context, events ...spade.Event) error {
	return nil
}

func loadSpadeClient() (spade.Client, error) {
	if isProd() {
		return spade.NewClient()
	}
	return &spadeClientWrapper{}, nil
}

func main() {
	client, err := loadSpadeClient()
	if err != nil {
		panic("unable to create spade client")
	}
	h := lambdaHandler{
		s: client,
	}
	lambda.Start(h.Handler)
}
