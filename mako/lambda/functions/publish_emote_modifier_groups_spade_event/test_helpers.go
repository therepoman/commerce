package main

import (
	"context"
	"fmt"

	"code.justin.tv/common/spade-client-go/spade"
	"github.com/aws/aws-lambda-go/events"
)

func makeValidEvent(numRecords int) events.DynamoDBEvent {
	e := events.DynamoDBEvent{
		Records: make([]events.DynamoDBEventRecord, 0, numRecords),
	}
	for i := 0; i < numRecords; i++ {
		e.Records = append(e.Records, events.DynamoDBEventRecord{
			EventID: fmt.Sprintf("anyID%v", i+1),
			Change:  makeValidStreamRecord(),
		})
	}
	return e
}

func makeValidStreamRecord() events.DynamoDBStreamRecord {
	return events.DynamoDBStreamRecord{
		NewImage: map[string]events.DynamoDBAttributeValue{
			ownerIDAttribute: events.NewStringAttribute("anyOwnerID"),
		},
	}
}

// Going to try a hand-rolled fake here.
// see: https://git.xarth.tv/pages/subs/golang/testing/fakes-mocks/

type spadeFake struct {
	events    []spade.Event
	returnErr error
}

func (s *spadeFake) TrackEvents(ctx context.Context, events ...spade.Event) error {
	s.events = events
	if s.returnErr != nil {
		return s.returnErr
	}
	return nil
}

func (s *spadeFake) TrackEvent(ctx context.Context, event string, properties interface{}) error {
	// required to satisfy the spade.Client interface - not used by our code.
	return nil
}
