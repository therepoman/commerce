package main

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/mako/clients/emoticonsmanager"
	"code.justin.tv/commerce/mako/clients/paint"
	"code.justin.tv/commerce/mako/clients/s3"
	mako "code.justin.tv/commerce/mako/internal"
	"code.justin.tv/commerce/mako/lambda/config"
	"code.justin.tv/commerce/mako/sqs/model"
	"code.justin.tv/commerce/mako/stats"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/session"
	awsS3 "github.com/aws/aws-sdk-go/service/s3"
	"github.com/pkg/errors"
	"github.com/twitchtv/twirp/hooks/statsd"
)

type lambdaHandler struct {
	S3Client                       s3.IS3Client
	statter                        statsd.Statter
	EmoteUploadsBucket             string
	AnimatedEmoteFrameS3BucketName string
}

var (
	themes = []string{
		"light",
		"dark",
	}
)

func (h *lambdaHandler) Handler(ctx context.Context, sqsEvent events.SQSEvent) error {
	// Batch size polled from SQS is 1.
	if len(sqsEvent.Records) != 1 {
		return errors.Errorf("batch size from sqs is %+v, input size is restricted to 1", len(sqsEvent.Records))
	}

	message := sqsEvent.Records[0]

	var msg model.DeleteEmoteAssetsFromS3Input
	if err := json.Unmarshal([]byte(message.Body), &msg); err != nil {
		return errors.Wrap(err, "unable to unmarshal sqs body")
	}

	if strings.Blank(msg.EmoteID) {
		return errors.New("emoteID is required")
	}

	fmt.Printf("calling delete from s3 lambda for %+v", msg)

	deleteStart := time.Now()
	if msg.EmoteAssetType == mako.AnimatedAssetType {
		if err := h.deleteAnimatedEmoteS3Images(ctx, msg.EmoteID); err != nil {
			return errors.Wrap(err, "failed to delete animated emote images")
		}

		if err := h.deleteAnimatedEmoteFrames(ctx, msg.EmoteID); err != nil {
			return errors.Wrap(err, "failed to delete animated emote frames")
		}

		h.statter.TimingDuration("delete_s3_images_lambda.s3.animated_delete", time.Since(deleteStart), 1.0)
	} else {
		if err := h.deleteStaticEmoteS3Images(ctx, msg.EmoteID); err != nil {
			return errors.Wrap(err, "failed deleting static image assets from s3 bucket")
		}
		h.statter.TimingDuration("delete_s3_images_lambda.s3.static_delete", time.Since(deleteStart), 1.0)
	}

	return nil
}

func (h *lambdaHandler) deleteStaticEmoteS3Images(ctx context.Context, emoteId string) error {
	for imageSize := range emoticonsmanager.LegacyScales {
		for _, scale := range emoticonsmanager.LegacyScales[imageSize] {
			if err := h.deleteFromBucket(ctx, h.EmoteUploadsBucket, s3.GetEmoticonS3Key(emoteId, scale)); err != nil {
				return err
			}
		}
	}

	for _, modification := range paint.Modifications() {
		for _, size := range paint.LegacySizes {
			if err := h.deleteFromBucket(ctx, h.EmoteUploadsBucket, s3.GetModifiedEmoticonS3Key(emoteId, size, modification)); err != nil {
				return err
			}
		}
	}

	return nil
}

func (h *lambdaHandler) deleteAnimatedEmoteS3Images(ctx context.Context, emoteId string) error {
	for imageSize := range emoticonsmanager.LegacyScales {
		for _, scale := range emoticonsmanager.LegacyScales[imageSize] {
			// delete static images
			if err := h.deleteFromBucket(ctx, h.EmoteUploadsBucket, s3.GetEmoticonS3Key(emoteId, scale)); err != nil {
				return err
			}

			// delete all themed gifs
			for _, theme := range themes {
				if err := h.deleteFromBucket(ctx, h.EmoteUploadsBucket, s3.GetAnimatedEmoteS3Key(emoteId, theme, scale)); err != nil {
					return err
				}
			}
		}
	}

	return nil
}

func (h *lambdaHandler) deleteAnimatedEmoteFrames(ctx context.Context, emoteID string) error {
	folder := s3.GetAnimatedFrameFolderS3Key(emoteID)
	req := &awsS3.ListObjectsV2Input{
		Bucket: aws.String(h.AnimatedEmoteFrameS3BucketName),
		Prefix: aws.String(folder),
	}

	var err error
	s3err := h.S3Client.ListObjectsV2WithPages(ctx, req, func(page *awsS3.ListObjectsV2Output, lastPage bool) bool {
		del := &awsS3.DeleteObjectsInput{
			Bucket: aws.String(h.AnimatedEmoteFrameS3BucketName),
			Delete: &awsS3.Delete{Quiet: aws.Bool(true)},
		}

		for _, value := range page.Contents {
			del.Delete.Objects = append(del.Delete.Objects, &awsS3.ObjectIdentifier{Key: value.Key})
		}
		if len(del.Delete.Objects) > 0 {
			resp, rerr := h.S3Client.DeleteObjects(ctx, del)
			if rerr != nil {
				err = rerr
				return false
			}
			if errs := resp.Errors; len(errs) > 0 {
				err = fmt.Errorf("failed to delete key %q: %v",
					aws.StringValue(errs[0].Key),
					aws.StringValue(errs[0].Message))
				return false
			}
		}
		if lastPage {
			return false
		}

		return true
	})

	if s3err != nil {
		return s3err
	}

	if err != nil {
		return err
	}

	return nil
}

func (h *lambdaHandler) deleteFromBucket(ctx context.Context, bucketName string, key string) error {
	if _, err := h.S3Client.DeleteObject(ctx, &awsS3.DeleteObjectInput{
		Bucket: aws.String(bucketName),
		Key:    aws.String(key),
	}); err != nil {
		// Emoticon may not exist in this bucket so don't return an error on file not found.
		if !IsFileNotFoundError(err) {
			return errors.Wrap(err, "failed to Delete S3 object")
		} else {
			fmt.Printf("key not found when deleting from s3 bucket %+v", err)
		}
	}

	return nil
}

func IsFileNotFoundError(err error) bool {
	aerr, ok := err.(awserr.Error)
	return ok && aerr != nil && aerr.Code() == awsS3.ErrCodeNoSuchKey
}

func main() {
	cfg := config.Load()
	statter := stats.SetupStatsWithMinBufferSize(cfg.EnvironmentPrefix, cfg.CloudwatchRegion)

	awsSession, err := session.NewSession()
	if err != nil {
		panic("unable to create new session")
	}

	awsS3Client := awsS3.New(awsSession, aws.NewConfig().WithRegion(cfg.AWSRegion))
	s3Client := s3.NewS3(awsS3Client)

	h := lambdaHandler{
		S3Client:                       s3Client,
		statter:                        statter,
		EmoteUploadsBucket:             cfg.EmoteUploadsBucket,
		AnimatedEmoteFrameS3BucketName: cfg.AnimatedEmoteFrameBucket,
	}

	lambda.Start(h.Handler)
}
