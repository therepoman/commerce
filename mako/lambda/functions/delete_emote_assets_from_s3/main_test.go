package main

import (
	"context"
	"encoding/json"
	"testing"

	mako "code.justin.tv/commerce/mako/internal"
	s3_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/s3"
	stats_mock "code.justin.tv/commerce/mako/mocks/github.com/cactus/go-statsd-client/statsd"
	"code.justin.tv/commerce/mako/sqs/model"
	"github.com/aws/aws-lambda-go/events"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
)

func TestDeleteEmoteAssetsFromS3Handler(t *testing.T) {
	testContext := context.Background()

	tests := []struct {
		scenario string
		test     func(t *testing.T)
	}{
		{
			scenario: "GIVEN empty input WHEN handle input THEN return error",
			test: func(t *testing.T) {
				// GIVEN
				handler := lambdaHandler{}

				// WHEN
				err := handler.Handler(testContext, events.SQSEvent{
					Records: []events.SQSMessage{},
				})

				// THEN
				require.Error(t, err)
				assert.Contains(t, err.Error(), "batch size from sqs is 0, input size is restricted to 1")
			},
		},
		{
			scenario: "GIVEN too much input WHEN handle input THEN return error",
			test: func(t *testing.T) {
				// GIVEN
				handler := lambdaHandler{}

				// WHEN
				err := handler.Handler(testContext, events.SQSEvent{
					Records: []events.SQSMessage{
						{
							Body: "123",
						},
						{
							Body: "123",
						},
					},
				})

				// THEN
				require.Error(t, err)
				assert.Contains(t, err.Error(), "batch size from sqs is 2, input size is restricted to 1")
			},
		},
		{
			scenario: "GIVEN given garbage message WHEN handle input THEN return error",
			test: func(t *testing.T) {
				// GIVEN
				handler := lambdaHandler{}
				sqsEvent := events.SQSEvent{
					Records: []events.SQSMessage{
						{
							Body: "123",
						},
					},
				}

				// WHEN
				err := handler.Handler(testContext, sqsEvent)

				// THEN
				require.Error(t, err)
				assert.Contains(t, err.Error(), "unable to unmarshal sqs body")
			},
		},
		{
			scenario: "GIVEN empty emote id WHEN handle input THEN return error",
			test: func(t *testing.T) {
				// GIVEN
				handler := lambdaHandler{}

				input := model.DeleteEmoteAssetsFromS3Input{
					EmoteID:        "",
					EmoteAssetType: mako.StaticAssetType,
				}

				mReq, err := json.Marshal(input)
				if err != nil {
					panic(err)
				}

				sqsEvent := events.SQSEvent{
					Records: []events.SQSMessage{
						{
							Body: string(mReq),
						},
					},
				}

				// WHEN
				err = handler.Handler(testContext, sqsEvent)

				// THEN
				require.Error(t, err)
				assert.Contains(t, err.Error(), "emoteID is required")
			},
		},
		{
			scenario: "GIVEN emote id given WHEN static emote THEN delete animated emote frames successfully",
			test: func(t *testing.T) {
				// GIVEN
				mockS3Client := new(s3_mock.IS3Client)
				mockStats := new(stats_mock.Statter)
				handler := lambdaHandler{
					S3Client: mockS3Client,
					statter:  mockStats,
				}
				input := model.DeleteEmoteAssetsFromS3Input{
					EmoteID:        "1234",
					EmoteAssetType: mako.StaticAssetType,
				}
				mockStats.On("TimingDuration", mock.Anything, mock.Anything, mock.Anything).Return(nil)
				mockS3Client.On("DeleteObject", mock.Anything, mock.Anything).Return(nil, nil)

				mReq, err := json.Marshal(input)
				if err != nil {
					panic(err)
				}

				sqsEvent := events.SQSEvent{
					Records: []events.SQSMessage{
						{
							Body: string(mReq),
						},
					},
				}

				// WHEN
				err = handler.Handler(testContext, sqsEvent)

				// THEN
				require.NoError(t, err)
				mockS3Client.AssertExpectations(t)
				mockS3Client.AssertNumberOfCalls(t, "DeleteObject", 213)
			},
		},
		{
			scenario: "GIVEN emote id given WHEN animated emote THEN delete animated emote frames successfully",
			test: func(t *testing.T) {
				// GIVEN
				mockS3Client := new(s3_mock.IS3Client)
				mockStats := new(stats_mock.Statter)
				handler := lambdaHandler{
					S3Client: mockS3Client,
					statter:  mockStats,
				}
				input := model.DeleteEmoteAssetsFromS3Input{
					EmoteID:        "1234",
					EmoteAssetType: mako.AnimatedAssetType,
				}

				mockStats.On("TimingDuration", mock.Anything, mock.Anything, mock.Anything).Return(nil)
				mockS3Client.On("DeleteObject", mock.Anything, mock.Anything).Return(nil, nil)
				mockS3Client.On("ListObjectsV2WithPages", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil, nil)

				// WHEN
				mReq, err := json.Marshal(input)
				if err != nil {
					panic(err)
				}

				sqsEvent := events.SQSEvent{
					Records: []events.SQSMessage{
						{
							Body: string(mReq),
						},
					},
				}

				// WHEN
				err = handler.Handler(testContext, sqsEvent)

				// THEN
				require.NoError(t, err)
				mockS3Client.AssertExpectations(t)
			},
		},
	}

	for _, test := range tests {
		t.Run(test.scenario, func(t *testing.T) {
			test.test(t)
		})
	}
}
