package main

import (
	"context"
	"errors"
	"log"
	"os"
	"time"

	"code.justin.tv/commerce/mako/emote_utils"
	"code.justin.tv/commerce/mako/lambda/config"
	"code.justin.tv/commerce/mako/stats"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/twitchtv/twirp/hooks/statsd"
)

// GifFrameSplitParams holds the parameters for the gif frame split lambda.
type GifFrameSplitParams struct {
	EmoteID string `json:"emote_id"`
}

// Handler is the gif frame extract lambda handler
type Handler struct {
	extractor emote_utils.GifFrameExtractor
	logger    *log.Logger
	statter   statsd.Statter
}

// Handler executes the gif frame extraction.
func (h *Handler) Handler(ctx context.Context, params GifFrameSplitParams) error {
	if params.EmoteID == "" {
		return errors.New("emoteID is required")
	}

	h.logger.Printf("Extracting and uploading frames for EmoteID: %s\n", params.EmoteID)
	start := time.Now()

	err := h.extractor.ExtractAndUploadFrames(ctx, params.EmoteID)

	if err != nil {
		h.logger.Printf("Error extracting animated emote gif frames - EmoteID: %s\nErr: %s", params.EmoteID, err.Error())
	}

	h.statter.TimingDuration("animated_emote_frame_extract.duration", time.Since(start), 1.0)

	return err
}

func main() {
	cfg := config.Load()

	logger := log.New(os.Stdout, "", log.LstdFlags)
	statter := stats.SetupStatsWithMinBufferSize(cfg.EnvironmentPrefix, cfg.CloudwatchRegion)
	extractor := emote_utils.NewGifFrameExtractorWithS3(cfg.AWSRegion, cfg.EmoteUploadsBucket, cfg.AnimatedEmoteFrameBucket, logger, &statter)

	h := &Handler{
		extractor: extractor,
		logger:    logger,
		statter:   statter,
	}

	lambda.Start(h.Handler)
}
