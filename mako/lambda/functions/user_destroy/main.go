package main

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/mako/dynamo"
	"code.justin.tv/commerce/mako/lambda/config"
	lambdaHystrix "code.justin.tv/commerce/mako/lambda/hystrix"
	"code.justin.tv/commerce/mako/scope"
	"code.justin.tv/commerce/mako/sfn"
	"code.justin.tv/commerce/mako/stats"
	mako_client "code.justin.tv/commerce/mako/twirp"
	"code.justin.tv/common/godynamo/client"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/pkg/errors"
)

type lambdaHandler struct {
	mako           mako_client.Mako
	prefixDAO      dynamo.IPrefixDao
	entitlementDAO dynamo.IEntitlementDao
}

func (h *lambdaHandler) Handler(ctx context.Context, input sfn.UserDestroyInput) (*sfn.UserDestroyOutput, error) {
	fmt.Printf("Received user destroy request for userId %s\n", input.UserID)

	if strings.Blank(input.UserID) {
		return nil, errors.New("no user id given")
	}

	userToDelete := input.UserID

	// 1. Delete Emotes
	allEmoteResponse, err := h.mako.GetAllEmotesByOwnerId(ctx, &mako_client.GetAllEmotesByOwnerIdRequest{
		OwnerId: userToDelete,
		Filter: &mako_client.EmoteFilter{
			Domain: mako_client.Domain_emotes,
		},
	})
	if err != nil {
		return nil, errors.Wrapf(err, "error fetching all emotes for user %s", userToDelete)
	}

	fmt.Printf("Found %d emote to delete for userId %s\n", len(allEmoteResponse.GetEmoticons()), userToDelete)

	// TODO: Parallelize this if execution timing on this Lambda becomes a concern
	for _, emote := range allEmoteResponse.GetEmoticons() {
		if emote == nil {
			continue
		}

		_, err := h.mako.DeleteEmoticon(ctx, &mako_client.DeleteEmoticonRequest{
			Id:           emote.Id,
			DeleteImages: true,
		})
		if err != nil {
			return nil, errors.Wrapf(err, "error deleting emote with id %s for user %s", emote.Id, userToDelete)
		}
	}

	// 2. Delete Prefix
	prefixRecord, err := h.prefixDAO.GetByOwnerId(ctx, userToDelete)
	if err != nil {
		return nil, errors.Wrapf(err, "error fetching prefix for user %s", userToDelete)
	}

	if prefixRecord != nil {
		fmt.Printf("Found prefix %s to delete for userId %s\n", prefixRecord.Prefix, userToDelete)
		err := h.prefixDAO.Delete(ctx, prefixRecord.Prefix, prefixRecord.OwnerId)
		if err != nil {
			return nil, errors.Wrapf(err, "error deleting prefix %s for user %s", userToDelete, prefixRecord.Prefix)
		}
	}

	// 3. Delete Emote Modifier Groups
	emoteModifierGroupsResp, err := h.mako.GetEmoteModifierGroupsByOwnerId(ctx, &mako_client.GetEmoteModifierGroupsByOwnerIdRequest{
		OwnerId: userToDelete,
	})
	if err != nil {
		return nil, errors.Wrapf(err, "error fetching emote modifier groups for user %s", userToDelete)
	}

	var emoteModifierGroupIdsToDelete []string
	for _, modifierGroup := range emoteModifierGroupsResp.GetGroups() {
		emoteModifierGroupIdsToDelete = append(emoteModifierGroupIdsToDelete, modifierGroup.Id)
	}

	fmt.Printf("Found %d emote modifier groups to delete for userId %s\n", len(emoteModifierGroupIdsToDelete), userToDelete)

	if len(emoteModifierGroupIdsToDelete) > 0 {
		_, err = h.mako.DeleteEmoteModifierGroups(ctx, &mako_client.DeleteEmoteModifierGroupsRequest{
			ModifierGroupIds: emoteModifierGroupIdsToDelete,
		})

		if err != nil {
			return nil, errors.Wrapf(err, "error deleting emote modifier groups with ids %v for user %s", emoteModifierGroupIdsToDelete, userToDelete)
		}
	}

	// 4. Delete Mako Entitlements
	entitlements, err := h.entitlementDAO.Get(userToDelete, scope.EmoteSets)
	if err != nil {
		return nil, errors.Wrapf(err, "error fetching mako entitlements for user %s", userToDelete)
	}

	fmt.Printf("Found %d emote entitlements to delete for userId %s\n", len(entitlements), userToDelete)

	// TODO: Parallelize this if execution timing on this Lambda becomes a concern
	for _, entitlement := range entitlements {
		if entitlement == nil {
			continue
		}

		err := h.entitlementDAO.Delete(entitlement)
		if err != nil {
			return nil, errors.Wrapf(err, "error deleting mako entitlement with key %s for user %s", entitlement.EntitlementScopedKey, userToDelete)
		}
	}

	return &sfn.UserDestroyOutput{
		UserID: userToDelete,
	}, nil
}

func main() {
	cfg := config.Load()
	// Use a longer timeout because Mako::DeleteEmoticon takes a while when DeleteImages is set to true
	httpClient := &http.Client{
		Timeout: time.Duration(10 * time.Second),
	}
	makoClient := mako_client.NewMakoProtobufClient(cfg.MakoEndpoint, httpClient)

	dynamoClient := client.NewClient(&client.DynamoClientConfig{
		AwsRegion:   cfg.AWSRegion,
		TablePrefix: cfg.EnvironmentPrefix,
	})

	prefixesDao := dynamo.NewPrefixDao(dynamoClient)
	entitlementDao := dynamo.NewEntitlementDao(dynamoClient)

	stats := stats.SetupStatsWithMinBufferSize(cfg.EnvironmentPrefix, cfg.CloudwatchRegion)
	lambdaHystrix.ConfigureHystrix(cfg, stats)

	h := lambdaHandler{
		mako:           makoClient,
		prefixDAO:      prefixesDao,
		entitlementDAO: entitlementDao,
	}
	lambda.Start(h.Handler)
}
