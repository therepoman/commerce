package main

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/mako/dynamo"
	dynamo_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/dynamo"
	mako_client_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/twirp"
	"code.justin.tv/commerce/mako/sfn"
	mako_client "code.justin.tv/commerce/mako/twirp"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
)

func TestUserDestroyHandler(t *testing.T) {
	testContext := context.Background()

	testUserId := "267893713"

	tests := []struct {
		scenario string
		test     func(t *testing.T)
	}{
		{
			scenario: "GIVEN no user id given WHEN handle input THEN return error",
			test: func(t *testing.T) {
				// GIVEN
				handler := lambdaHandler{}
				input := sfn.UserDestroyInput{
					UserID: "",
				}

				// WHEN
				_, err := handler.Handler(testContext, input)

				// THEN
				require.Error(t, err)
				assert.Contains(t, err.Error(), "no user id given")
			},
		},
		{
			scenario: "GIVEN user id provided WHEN handle input THEN Mako GetAllEmotesByOwnerId is called with emote domain filter",
			test: func(t *testing.T) {
				// GIVEN
				makoMock := new(mako_client_mock.Mako)
				prefixDaoMock := new(dynamo_mock.IPrefixDao)
				entitlementsDaoMock := new(dynamo_mock.IEntitlementDao)
				handler := lambdaHandler{
					mako:           makoMock,
					prefixDAO:      prefixDaoMock,
					entitlementDAO: entitlementsDaoMock,
				}
				input := sfn.UserDestroyInput{
					UserID: testUserId,
				}
				makoMock.On("GetAllEmotesByOwnerId", mock.Anything, mock.Anything).Return(nil, nil)
				prefixDaoMock.On("GetByOwnerId", mock.Anything, mock.Anything).Return(nil, nil)
				makoMock.On("GetEmoteModifierGroupsByOwnerId", mock.Anything, mock.Anything).Return(nil, nil)
				entitlementsDaoMock.On("Get", mock.Anything, mock.Anything).Return(nil, nil)

				// WHEN
				_, err := handler.Handler(testContext, input)

				// THEN
				require.NoError(t, err)
				actualRequest := makoMock.Calls[0].Arguments[1].(*mako_client.GetAllEmotesByOwnerIdRequest)
				require.Equal(t, mako_client.Domain_emotes, actualRequest.Filter.Domain)
			},
		},
		{
			scenario: "GIVEN Mako returns error for GetAllEmotesByOwnerId WHEN handle input THEN return error",
			test: func(t *testing.T) {
				// GIVEN
				makoMock := new(mako_client_mock.Mako)
				prefixDaoMock := new(dynamo_mock.IPrefixDao)
				entitlementsDaoMock := new(dynamo_mock.IEntitlementDao)
				handler := lambdaHandler{
					mako:           makoMock,
					prefixDAO:      prefixDaoMock,
					entitlementDAO: entitlementsDaoMock,
				}
				input := sfn.UserDestroyInput{
					UserID: testUserId,
				}
				makoMock.On("GetAllEmotesByOwnerId", mock.Anything, mock.Anything).Return(nil, errors.New("dummy GetAllEmotesByOwnerId mako error"))

				// WHEN
				_, err := handler.Handler(testContext, input)

				// THEN
				require.Error(t, err)
				assert.Contains(t, err.Error(), "dummy GetAllEmotesByOwnerId mako error")
			},
		},
		{
			scenario: "GIVEN Mako returns no emotes for GetAllEmotesByOwnerId WHEN handle input THEN DeleteEmoticon is not called and no error is returned",
			test: func(t *testing.T) {
				// GIVEN
				makoMock := new(mako_client_mock.Mako)
				prefixDaoMock := new(dynamo_mock.IPrefixDao)
				entitlementsDaoMock := new(dynamo_mock.IEntitlementDao)
				handler := lambdaHandler{
					mako:           makoMock,
					prefixDAO:      prefixDaoMock,
					entitlementDAO: entitlementsDaoMock,
				}
				input := sfn.UserDestroyInput{
					UserID: testUserId,
				}
				makoMock.On("GetAllEmotesByOwnerId", mock.Anything, mock.Anything).Return(&mako_client.GetAllEmotesByOwnerIdResponse{}, nil)
				prefixDaoMock.On("GetByOwnerId", mock.Anything, mock.Anything).Return(nil, nil)
				makoMock.On("GetEmoteModifierGroupsByOwnerId", mock.Anything, mock.Anything).Return(nil, nil)
				entitlementsDaoMock.On("Get", mock.Anything, mock.Anything).Return(nil, nil)

				// WHEN
				output, err := handler.Handler(testContext, input)

				// THEN
				require.NoError(t, err)
				require.NotNil(t, output)
				require.Equal(t, testUserId, output.UserID)
				makoMock.AssertNotCalled(t, "DeleteEmoticon", mock.Anything, mock.Anything)
			},
		},

		{
			scenario: "GIVEN Mako returns emotes for GetAllEmotesByOwnerId WHEN handle input THEN DeleteEmoticon is called once per emote and no error is returned",
			test: func(t *testing.T) {
				// GIVEN
				makoMock := new(mako_client_mock.Mako)
				prefixDaoMock := new(dynamo_mock.IPrefixDao)
				entitlementsDaoMock := new(dynamo_mock.IEntitlementDao)
				handler := lambdaHandler{
					mako:           makoMock,
					prefixDAO:      prefixDaoMock,
					entitlementDAO: entitlementsDaoMock,
				}
				input := sfn.UserDestroyInput{
					UserID: testUserId,
				}
				makoMock.On("GetAllEmotesByOwnerId", mock.Anything, mock.Anything).Return(&mako_client.GetAllEmotesByOwnerIdResponse{
					Emoticons: []*mako_client.Emoticon{
						{
							Id: "123",
						},
						{
							Id: "124",
						},
					},
				}, nil)
				makoMock.On("DeleteEmoticon", mock.Anything, mock.Anything).Return(nil, nil)
				prefixDaoMock.On("GetByOwnerId", mock.Anything, mock.Anything).Return(nil, nil)
				makoMock.On("GetEmoteModifierGroupsByOwnerId", mock.Anything, mock.Anything).Return(nil, nil)
				entitlementsDaoMock.On("Get", mock.Anything, mock.Anything).Return(nil, nil)

				// WHEN
				output, err := handler.Handler(testContext, input)

				// THEN
				require.NoError(t, err)
				require.NotNil(t, output)
				require.Equal(t, testUserId, output.UserID)
				makoMock.AssertNumberOfCalls(t, "DeleteEmoticon", 2)
			},
		},

		{
			scenario: "GIVEN Mako returns error for DeleteEmoticon WHEN handle input THEN return error",
			test: func(t *testing.T) {
				// GIVEN
				makoMock := new(mako_client_mock.Mako)
				prefixDaoMock := new(dynamo_mock.IPrefixDao)
				entitlementsDaoMock := new(dynamo_mock.IEntitlementDao)
				handler := lambdaHandler{
					mako:           makoMock,
					prefixDAO:      prefixDaoMock,
					entitlementDAO: entitlementsDaoMock,
				}
				input := sfn.UserDestroyInput{
					UserID: testUserId,
				}
				makoMock.On("GetAllEmotesByOwnerId", mock.Anything, mock.Anything).Return(&mako_client.GetAllEmotesByOwnerIdResponse{
					Emoticons: []*mako_client.Emoticon{
						{
							Id: "123",
						},
						{
							Id: "124",
						},
					},
				}, nil)
				makoMock.On("DeleteEmoticon", mock.Anything, mock.Anything).Return(nil, errors.New("dummy DeleteEmoticon mako error"))

				// WHEN
				_, err := handler.Handler(testContext, input)

				// THEN
				require.Error(t, err)
				assert.Contains(t, err.Error(), "dummy DeleteEmoticon mako error")
			},
		},

		{
			scenario: "GIVEN prefix DAO returns error for GetByOwnerId WHEN handle input THEN return error",
			test: func(t *testing.T) {
				// GIVEN
				makoMock := new(mako_client_mock.Mako)
				prefixDaoMock := new(dynamo_mock.IPrefixDao)
				entitlementsDaoMock := new(dynamo_mock.IEntitlementDao)
				handler := lambdaHandler{
					mako:           makoMock,
					prefixDAO:      prefixDaoMock,
					entitlementDAO: entitlementsDaoMock,
				}
				input := sfn.UserDestroyInput{
					UserID: testUserId,
				}
				prefixDaoMock.On("GetByOwnerId", mock.Anything, mock.Anything).Return(nil, errors.New("dummy prefix get error"))
				makoMock.On("GetAllEmotesByOwnerId", mock.Anything, mock.Anything).Return(nil, nil)

				// WHEN
				_, err := handler.Handler(testContext, input)

				// THEN
				require.Error(t, err)
				assert.Contains(t, err.Error(), "dummy prefix get error")
			},
		},

		{
			scenario: "GIVEN prefix DAO returns no prefix for GetByOwnerId WHEN handle input THEN prefix delete is not called and no error is returned",
			test: func(t *testing.T) {
				// GIVEN
				makoMock := new(mako_client_mock.Mako)
				prefixDaoMock := new(dynamo_mock.IPrefixDao)
				entitlementsDaoMock := new(dynamo_mock.IEntitlementDao)
				handler := lambdaHandler{
					mako:           makoMock,
					prefixDAO:      prefixDaoMock,
					entitlementDAO: entitlementsDaoMock,
				}
				input := sfn.UserDestroyInput{
					UserID: testUserId,
				}
				prefixDaoMock.On("GetByOwnerId", mock.Anything, mock.Anything).Return(nil, nil)
				makoMock.On("GetAllEmotesByOwnerId", mock.Anything, mock.Anything).Return(nil, nil)
				makoMock.On("GetEmoteModifierGroupsByOwnerId", mock.Anything, mock.Anything).Return(nil, nil)
				entitlementsDaoMock.On("Get", mock.Anything, mock.Anything).Return(nil, nil)

				// WHEN
				output, err := handler.Handler(testContext, input)

				// THEN
				require.NoError(t, err)
				prefixDaoMock.AssertNotCalled(t, "Delete", mock.Anything, mock.Anything, mock.Anything)
				require.Equal(t, testUserId, output.UserID)
			},
		},

		{
			scenario: "GIVEN prefix exists AND prefix delete returns error WHEN handle input THEN return error",
			test: func(t *testing.T) {
				// GIVEN
				makoMock := new(mako_client_mock.Mako)
				prefixDaoMock := new(dynamo_mock.IPrefixDao)
				entitlementsDaoMock := new(dynamo_mock.IEntitlementDao)
				handler := lambdaHandler{
					mako:           makoMock,
					prefixDAO:      prefixDaoMock,
					entitlementDAO: entitlementsDaoMock,
				}
				input := sfn.UserDestroyInput{
					UserID: testUserId,
				}
				testPrefix := "testPrefix"
				prefixDaoMock.On("GetByOwnerId", mock.Anything, mock.Anything).Return(&dynamo.Prefix{
					Prefix:  testPrefix,
					OwnerId: testUserId,
				}, nil)
				prefixDaoMock.On("Delete", mock.Anything, testPrefix, testUserId).Return(errors.New("dummy prefix delete error"))
				makoMock.On("GetAllEmotesByOwnerId", mock.Anything, mock.Anything).Return(nil, nil)

				// WHEN
				_, err := handler.Handler(testContext, input)

				// THEN
				require.Error(t, err)
				assert.Contains(t, err.Error(), "dummy prefix delete error")
			},
		},

		{
			scenario: "GIVEN prefix exists AND prefix delete succeeds WHEN handle input THEN return no error",
			test: func(t *testing.T) {
				// GIVEN
				makoMock := new(mako_client_mock.Mako)
				prefixDaoMock := new(dynamo_mock.IPrefixDao)
				entitlementsDaoMock := new(dynamo_mock.IEntitlementDao)
				handler := lambdaHandler{
					mako:           makoMock,
					prefixDAO:      prefixDaoMock,
					entitlementDAO: entitlementsDaoMock,
				}
				input := sfn.UserDestroyInput{
					UserID: testUserId,
				}
				testPrefix := "testPrefix"
				prefixDaoMock.On("GetByOwnerId", mock.Anything, mock.Anything).Return(&dynamo.Prefix{
					Prefix:  testPrefix,
					OwnerId: testUserId,
				}, nil)
				prefixDaoMock.On("Delete", mock.Anything, testPrefix, testUserId).Return(nil)
				makoMock.On("GetAllEmotesByOwnerId", mock.Anything, mock.Anything).Return(nil, nil)
				makoMock.On("GetEmoteModifierGroupsByOwnerId", mock.Anything, mock.Anything).Return(nil, nil)
				entitlementsDaoMock.On("Get", mock.Anything, mock.Anything).Return(nil, nil)

				// WHEN
				output, err := handler.Handler(testContext, input)

				// THEN
				require.NoError(t, err)
				require.Equal(t, testUserId, output.UserID)
			},
		},

		{
			scenario: "GIVEN mako returns error for GetEmoteModifierGroupsByOwnerId WHEN handle input THEN return error",
			test: func(t *testing.T) {
				// GIVEN
				makoMock := new(mako_client_mock.Mako)
				prefixDaoMock := new(dynamo_mock.IPrefixDao)
				entitlementsDaoMock := new(dynamo_mock.IEntitlementDao)
				handler := lambdaHandler{
					mako:           makoMock,
					prefixDAO:      prefixDaoMock,
					entitlementDAO: entitlementsDaoMock,
				}
				input := sfn.UserDestroyInput{
					UserID: testUserId,
				}
				makoMock.On("GetAllEmotesByOwnerId", mock.Anything, mock.Anything).Return(nil, nil)
				prefixDaoMock.On("GetByOwnerId", mock.Anything, mock.Anything).Return(nil, nil)
				makoMock.On("GetEmoteModifierGroupsByOwnerId", mock.Anything, mock.Anything).Return(nil, errors.New("dummy emote modifiers get error"))

				// WHEN
				_, err := handler.Handler(testContext, input)

				// THEN
				require.Error(t, err)
				assert.Contains(t, err.Error(), "dummy emote modifiers get error")
			},
		},

		{
			scenario: "GIVEN mako returns no emote modifier groups for GetEmoteModifierGroupsByOwnerId WHEN handle input THEN emote modifier groups delete is not called and no error is returned",
			test: func(t *testing.T) {
				// GIVEN
				makoMock := new(mako_client_mock.Mako)
				prefixDaoMock := new(dynamo_mock.IPrefixDao)
				entitlementsDaoMock := new(dynamo_mock.IEntitlementDao)
				handler := lambdaHandler{
					mako:           makoMock,
					prefixDAO:      prefixDaoMock,
					entitlementDAO: entitlementsDaoMock,
				}
				input := sfn.UserDestroyInput{
					UserID: testUserId,
				}
				makoMock.On("GetAllEmotesByOwnerId", mock.Anything, mock.Anything).Return(nil, nil)
				prefixDaoMock.On("GetByOwnerId", mock.Anything, mock.Anything).Return(nil, nil)
				makoMock.On("GetEmoteModifierGroupsByOwnerId", mock.Anything, mock.Anything).Return(nil, nil)
				entitlementsDaoMock.On("Get", mock.Anything, mock.Anything).Return(nil, nil)

				// WHEN
				output, err := handler.Handler(testContext, input)

				// THEN
				require.NoError(t, err)
				makoMock.AssertNotCalled(t, "DeleteEmoteModifierGroups", mock.Anything, mock.Anything)
				require.Equal(t, testUserId, output.UserID)
			},
		},

		{
			scenario: "GIVEN mako returns emote modifier groups AND modifier groups delete returns error WHEN handle input THEN return error",
			test: func(t *testing.T) {
				// GIVEN
				makoMock := new(mako_client_mock.Mako)
				prefixDaoMock := new(dynamo_mock.IPrefixDao)
				entitlementsDaoMock := new(dynamo_mock.IEntitlementDao)
				handler := lambdaHandler{
					mako:           makoMock,
					prefixDAO:      prefixDaoMock,
					entitlementDAO: entitlementsDaoMock,
				}
				input := sfn.UserDestroyInput{
					UserID: testUserId,
				}
				testEmoteModifierGroupId := "test_id"
				makoMock.On("GetAllEmotesByOwnerId", mock.Anything, mock.Anything).Return(nil, nil)
				prefixDaoMock.On("GetByOwnerId", mock.Anything, mock.Anything).Return(nil, nil)
				makoMock.On("GetEmoteModifierGroupsByOwnerId", mock.Anything, mock.Anything).Return(&mako_client.GetEmoteModifierGroupsByOwnerIdResponse{
					Groups: []*mako_client.EmoteModifierGroup{
						{
							Id: testEmoteModifierGroupId,
						},
					},
				}, nil)
				makoMock.On("DeleteEmoteModifierGroups", mock.Anything, &mako_client.DeleteEmoteModifierGroupsRequest{
					ModifierGroupIds: []string{testEmoteModifierGroupId},
				}).Return(nil, errors.New("dummy emote modifiers delete error"))

				// WHEN
				_, err := handler.Handler(testContext, input)

				// THEN
				require.Error(t, err)
				assert.Contains(t, err.Error(), "dummy emote modifiers delete error")
			},
		},

		{
			scenario: "GIVEN mako returns emote modifier groups AND modifier groups delete succeeds WHEN handle input THEN return no error",
			test: func(t *testing.T) {
				// GIVEN
				makoMock := new(mako_client_mock.Mako)
				prefixDaoMock := new(dynamo_mock.IPrefixDao)
				entitlementsDaoMock := new(dynamo_mock.IEntitlementDao)
				handler := lambdaHandler{
					mako:           makoMock,
					prefixDAO:      prefixDaoMock,
					entitlementDAO: entitlementsDaoMock,
				}
				input := sfn.UserDestroyInput{
					UserID: testUserId,
				}
				testEmoteModifierGroupId := "test_id"
				testEmoteModifierGroupId2 := "test_id_2"
				prefixDaoMock.On("GetByOwnerId", mock.Anything, mock.Anything).Return(nil, nil)
				makoMock.On("GetAllEmotesByOwnerId", mock.Anything, mock.Anything).Return(nil, nil)
				makoMock.On("GetEmoteModifierGroupsByOwnerId", mock.Anything, mock.Anything).Return(&mako_client.GetEmoteModifierGroupsByOwnerIdResponse{
					Groups: []*mako_client.EmoteModifierGroup{
						{
							Id: testEmoteModifierGroupId,
						},
						{
							Id: testEmoteModifierGroupId2,
						},
					},
				}, nil)
				makoMock.On("DeleteEmoteModifierGroups", mock.Anything, &mako_client.DeleteEmoteModifierGroupsRequest{
					ModifierGroupIds: []string{testEmoteModifierGroupId, testEmoteModifierGroupId2},
				}).Return(nil, nil)
				entitlementsDaoMock.On("Get", mock.Anything, mock.Anything).Return(nil, nil)

				// WHEN
				output, err := handler.Handler(testContext, input)

				// THEN
				require.NoError(t, err)
				require.Equal(t, testUserId, output.UserID)
			},
		},

		/////////

		{
			scenario: "GIVEN entitlement dao returns error for Get WHEN handle input THEN return error",
			test: func(t *testing.T) {
				// GIVEN
				makoMock := new(mako_client_mock.Mako)
				prefixDaoMock := new(dynamo_mock.IPrefixDao)
				entitlementsDaoMock := new(dynamo_mock.IEntitlementDao)
				handler := lambdaHandler{
					mako:           makoMock,
					prefixDAO:      prefixDaoMock,
					entitlementDAO: entitlementsDaoMock,
				}
				input := sfn.UserDestroyInput{
					UserID: testUserId,
				}
				makoMock.On("GetAllEmotesByOwnerId", mock.Anything, mock.Anything).Return(nil, nil)
				prefixDaoMock.On("GetByOwnerId", mock.Anything, mock.Anything).Return(nil, nil)
				makoMock.On("GetEmoteModifierGroupsByOwnerId", mock.Anything, mock.Anything).Return(nil, nil)
				entitlementsDaoMock.On("Get", mock.Anything, mock.Anything).Return(nil, errors.New("dummy entitlements get error"))

				// WHEN
				_, err := handler.Handler(testContext, input)

				// THEN
				require.Error(t, err)
				assert.Contains(t, err.Error(), "dummy entitlements get error")
			},
		},

		{
			scenario: "GIVEN entitlements dao returns no entitlements for Get WHEN handle input THEN entitlements dao delete is not called and no error is returned",
			test: func(t *testing.T) {
				// GIVEN
				makoMock := new(mako_client_mock.Mako)
				prefixDaoMock := new(dynamo_mock.IPrefixDao)
				entitlementsDaoMock := new(dynamo_mock.IEntitlementDao)
				handler := lambdaHandler{
					mako:           makoMock,
					prefixDAO:      prefixDaoMock,
					entitlementDAO: entitlementsDaoMock,
				}
				input := sfn.UserDestroyInput{
					UserID: testUserId,
				}
				makoMock.On("GetAllEmotesByOwnerId", mock.Anything, mock.Anything).Return(nil, nil)
				prefixDaoMock.On("GetByOwnerId", mock.Anything, mock.Anything).Return(nil, nil)
				makoMock.On("GetEmoteModifierGroupsByOwnerId", mock.Anything, mock.Anything).Return(nil, nil)
				entitlementsDaoMock.On("Get", mock.Anything, mock.Anything).Return(nil, nil)

				// WHEN
				output, err := handler.Handler(testContext, input)

				// THEN
				require.NoError(t, err)
				entitlementsDaoMock.AssertNotCalled(t, "Delete", mock.Anything, mock.Anything)
				require.Equal(t, testUserId, output.UserID)
			},
		},

		{
			scenario: "GIVEN entitlements dao returns entitlements for Get AND entitlements delete returns error WHEN handle input THEN return error",
			test: func(t *testing.T) {
				// GIVEN
				makoMock := new(mako_client_mock.Mako)
				prefixDaoMock := new(dynamo_mock.IPrefixDao)
				entitlementsDaoMock := new(dynamo_mock.IEntitlementDao)
				handler := lambdaHandler{
					mako:           makoMock,
					prefixDAO:      prefixDaoMock,
					entitlementDAO: entitlementsDaoMock,
				}
				input := sfn.UserDestroyInput{
					UserID: testUserId,
				}
				testEntitlementScopedKey := "test_entitlement_key"
				testEntitlement := dynamo.Entitlement{
					UserId:               testUserId,
					EntitlementScopedKey: testEntitlementScopedKey,
				}
				makoMock.On("GetAllEmotesByOwnerId", mock.Anything, mock.Anything).Return(nil, nil)
				prefixDaoMock.On("GetByOwnerId", mock.Anything, mock.Anything).Return(nil, nil)
				makoMock.On("GetEmoteModifierGroupsByOwnerId", mock.Anything, mock.Anything).Return(nil, nil)
				entitlementsDaoMock.On("Get", mock.Anything, mock.Anything).Return([]*dynamo.Entitlement{
					&testEntitlement,
				}, nil)
				entitlementsDaoMock.On("Delete", &testEntitlement).Return(errors.New("dummy entitlements error"))

				// WHEN
				_, err := handler.Handler(testContext, input)

				// THEN
				require.Error(t, err)
				assert.Contains(t, err.Error(), "dummy entitlements error")
			},
		},

		{
			scenario: "GIVEN entitlements dao returns entitlements for Get WHEN handle input THEN entitlements dao delete is called once per entitlement and no error is returned",
			test: func(t *testing.T) {
				// GIVEN
				makoMock := new(mako_client_mock.Mako)
				prefixDaoMock := new(dynamo_mock.IPrefixDao)
				entitlementsDaoMock := new(dynamo_mock.IEntitlementDao)
				handler := lambdaHandler{
					mako:           makoMock,
					prefixDAO:      prefixDaoMock,
					entitlementDAO: entitlementsDaoMock,
				}
				input := sfn.UserDestroyInput{
					UserID: testUserId,
				}
				testEntitlementScopedKey1 := "test_entitlement_key1"
				testEntitlement1 := dynamo.Entitlement{
					UserId:               testUserId,
					EntitlementScopedKey: testEntitlementScopedKey1,
				}
				testEntitlementScopedKey2 := "test_entitlement_key2"
				testEntitlement2 := dynamo.Entitlement{
					UserId:               testUserId,
					EntitlementScopedKey: testEntitlementScopedKey2,
				}
				makoMock.On("GetAllEmotesByOwnerId", mock.Anything, mock.Anything).Return(nil, nil)
				prefixDaoMock.On("GetByOwnerId", mock.Anything, mock.Anything).Return(nil, nil)
				makoMock.On("GetEmoteModifierGroupsByOwnerId", mock.Anything, mock.Anything).Return(nil, nil)
				entitlementsDaoMock.On("Get", mock.Anything, mock.Anything).Return([]*dynamo.Entitlement{
					&testEntitlement1, &testEntitlement2,
				}, nil)
				entitlementsDaoMock.On("Delete", mock.Anything).Return(nil)

				// WHEN
				output, err := handler.Handler(testContext, input)

				// THEN
				require.NoError(t, err)
				require.Equal(t, testUserId, output.UserID)
				entitlementsDaoMock.AssertNumberOfCalls(t, "Delete", 2)
			},
		},
	}

	for _, test := range tests {
		t.Run(test.scenario, func(t *testing.T) {
			test.test(t)
		})
	}
}
