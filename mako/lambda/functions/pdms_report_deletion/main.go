package main

import (
	"context"
	"fmt"

	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/mako/clients/pdms"
	"code.justin.tv/commerce/mako/lambda/config"
	lambdaHystrix "code.justin.tv/commerce/mako/lambda/hystrix"
	"code.justin.tv/commerce/mako/sfn"
	"code.justin.tv/commerce/mako/stats"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/pkg/errors"
)

type lambdaHandler struct {
	pdms pdms.ClientWrapper
}

func (h *lambdaHandler) Handler(ctx context.Context, input sfn.UserDestroyInput) error {
	fmt.Printf("Received report deletion for userId %s", input.UserID)

	if strings.Blank(input.UserID) {
		return errors.New("no user id given")
	}

	err := h.pdms.ReportDeletion(ctx, input.UserID)
	if err != nil {
		return errors.Wrapf(err, "error reporting deletion for user %s", input.UserID)
	}

	return nil
}

func main() {
	cfg := config.Load()

	pdmsClient := pdms.NewClient(pdms.Config{
		LambdaCallerRoleARN: cfg.PDMSLambdaCallerRoleARN,
		LambdaARN:           cfg.PDMSLambdaARN,
	})

	stats := stats.SetupStatsWithMinBufferSize(cfg.EnvironmentPrefix, cfg.CloudwatchRegion)
	lambdaHystrix.ConfigureHystrix(cfg, stats)

	h := lambdaHandler{
		pdms: pdmsClient,
	}
	lambda.Start(h.Handler)
}
