package main

import (
	"context"
	"testing"

	pdms_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/clients/pdms"
	"code.justin.tv/commerce/mako/sfn"
	"github.com/pkg/errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
)

func TestReportDeletionHandler(t *testing.T) {
	testContext := context.Background()

	testUserId := "267893713"

	tests := []struct {
		scenario string
		test     func(t *testing.T)
	}{
		{
			scenario: "GIVEN no user id given WHEN handle input THEN return error",
			test: func(t *testing.T) {
				// GIVEN
				handler := lambdaHandler{}
				input := sfn.UserDestroyInput{
					UserID: "",
				}

				// WHEN
				err := handler.Handler(testContext, input)

				// THEN
				require.Error(t, err)
				assert.Contains(t, err.Error(), "no user id given")
			},
		},
		{
			scenario: "GIVEN PDMS returns error for ReportDeletion WHEN handle input THEN return error",
			test: func(t *testing.T) {
				// GIVEN
				pdmsMock := new(pdms_mock.ClientWrapper)
				handler := lambdaHandler{pdmsMock}
				input := sfn.UserDestroyInput{
					UserID: testUserId,
				}
				pdmsMock.On("ReportDeletion", mock.Anything, mock.Anything).Return(errors.New("dummy pdms error"))

				// WHEN
				err := handler.Handler(testContext, input)

				// THEN
				require.Error(t, err)
				assert.Contains(t, err.Error(), "dummy pdms error")
			},
		},
		{
			scenario: "GIVEN PDMS returns success for ReportDeletion WHEN handle input THEN return no error",
			test: func(t *testing.T) {
				// GIVEN
				pdmsMock := new(pdms_mock.ClientWrapper)
				handler := lambdaHandler{pdmsMock}
				input := sfn.UserDestroyInput{
					UserID: testUserId,
				}
				pdmsMock.On("ReportDeletion", mock.Anything, mock.Anything).Return(nil)

				// WHEN
				err := handler.Handler(testContext, input)

				// THEN
				require.NoError(t, err)
			},
		},
	}

	for _, test := range tests {
		t.Run(test.scenario, func(t *testing.T) {
			test.test(t)
		})
	}
}
