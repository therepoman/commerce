package main

import (
	mako_client "code.justin.tv/commerce/mako/twirp"
	"net/http"

	receiver "code.justin.tv/amzn/TwitchDartReceiverTwirp"
	spade_mako "code.justin.tv/commerce/mako/clients/spade"
	"code.justin.tv/commerce/mako/lambda/config"
	"code.justin.tv/common/spade-client-go/spade"
	eventbus "code.justin.tv/eventbus/client"
	"code.justin.tv/eventbus/client/subscriber/lambdafunc"
	schema "code.justin.tv/eventbus/schema/pkg/user_multi_factor_auth"
	"github.com/aws/aws-lambda-go/lambda"
)

func main() {
	cfg := config.Load()

	var spadeClient spade.Client
	var err error
	if cfg.EnvironmentPrefix == "prod" {
		spadeClient, err = spade.NewClient()
		if err != nil {
			panic(err)
		}
	} else {
		spadeClient = &spade_mako.SpadeClientNoOp{}
	}

	dartReceiver := receiver.NewReceiverProtobufClient(cfg.DartReceiverHost, http.DefaultClient)

	makoClient := mako_client.NewMakoProtobufClient(cfg.MakoEndpoint, http.DefaultClient)

	tf := TwoFactorEntitlementLambda{
		SpadeClient:       spadeClient,
		DartReceiver:      dartReceiver,
		MakoClient: makoClient,
	}

	// set up lambda handler according to eventbus docs: https://git.xarth.tv/pages/eventbus/docs/subscribers/lambda_handlers/
	mux := eventbus.NewMux()
	schema.RegisterUserMultiFactorAuthUpdateHandler(mux, tf.EntitleTwoFactorEmoteRewards)
	lambda.Start(lambdafunc.NewSQS(mux.Dispatcher()))
}
