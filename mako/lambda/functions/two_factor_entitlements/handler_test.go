package main

import (
	"context"
	"fmt"
	"testing"

	receiver_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/amzn/TwitchDartReceiverTwirp"
	mako_client_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/commerce/mako/twirp"
	spade_mock "code.justin.tv/commerce/mako/mocks/code.justin.tv/common/spade-client-go/spade"
	"code.justin.tv/eventbus/schema/pkg/eventbus/change"
	schema "code.justin.tv/eventbus/schema/pkg/user_multi_factor_auth"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func TestTwoFactorEntitlementLambda_Handle(t *testing.T) {
	t.Run("GIVEN a 2FA entitlement event that doesn't enable or disable 2FA WHEN handling the 2FA entitlement event THEN expect no error and no action", func(t *testing.T) {
		lambdaHandler, _ := setUpMocks()
		// GIVEN
		updateEvent := &schema.Update{
			UserId:     "user-id",
			AnyEnabled: nil,
		}
		// WHEN
		err := lambdaHandler.EntitleTwoFactorEmoteRewards(context.Background(), nil, updateEvent)
		// THEN
		assert.Nil(t, err)
	})

	t.Run("GIVEN a user that enables 2FA WHEN handling the 2FA entitlement event THEN expect no error", func(t *testing.T) {
		lambdaHandler, mocks := setUpMocks()
		// GIVEN
		updateEvent := &schema.Update{
			UserId: "user-id",
			AnyEnabled: &change.BoolChange{
				Value: true,
			},
		}
		// create emote group entitlement
		mocks.mako.On("CreateEmoteGroupEntitlements", mock.Anything, mock.Anything).Return(nil, nil).Once()
		// notify user when they receive 2FA entitlement
		mocks.dartReceiver.On("PublishNotification", mock.Anything, mock.Anything).Return(nil, nil).Once()
		// send event to spade that 2FA entitlement was granted
		mocks.spade.On("TrackEvent", mock.Anything, twoFactorDataScienceEventName, mock.Anything).Return(nil).Once()

		// WHEN
		err := lambdaHandler.EntitleTwoFactorEmoteRewards(context.Background(), nil, updateEvent)

		// THEN
		assert.Nil(t, err)
	})

	t.Run("GIVEN a user that enables 2FA WHEN handling the 2FA entitlement event and CreateEmoteGroupEntitlements fails THEN expect error", func(t *testing.T) {
		lambdaHandler, mocks := setUpMocks()
		// GIVEN
		updateEvent := &schema.Update{
			UserId: "user-id",
			AnyEnabled: &change.BoolChange{
				Value: true,
			},
		}

		// create emote group entitlement fails
		mocks.mako.On("CreateEmoteGroupEntitlements", mock.Anything, mock.Anything).Return(nil, fmt.Errorf("create emotegroup entitlements fails")).Once()

		// WHEN
		err := lambdaHandler.EntitleTwoFactorEmoteRewards(context.Background(), nil, updateEvent)

		// THEN
		assert.NotNil(t, err)
	})

	t.Run("GIVEN a user that enables 2FA WHEN handling the 2FA entitlement event and send dart notification fails THEN expect no error", func(t *testing.T) {
		lambdaHandler, mocks := setUpMocks()
		// GIVEN
		updateEvent := &schema.Update{
			UserId: "user-id",
			AnyEnabled: &change.BoolChange{
				Value: true,
			},
		}
		// create emote group entitlement
		mocks.mako.On("CreateEmoteGroupEntitlements", mock.Anything, mock.Anything).Return(nil, nil).Once()
		// fail to notify user when they receive 2FA entitlement
		mocks.dartReceiver.On("PublishNotification", mock.Anything, mock.Anything).Return(nil, fmt.Errorf("failed to send dart notification")).Once()
		// send event to spade that 2FA entitlement was granted
		mocks.spade.On("TrackEvent", mock.Anything, twoFactorDataScienceEventName, mock.Anything).Return(nil).Once()

		// WHEN
		err := lambdaHandler.EntitleTwoFactorEmoteRewards(context.Background(), nil, updateEvent)

		// THEN
		assert.Nil(t, err)
	})

	t.Run("GIVEN a user that enables 2FA WHEN handling the 2FA entitlement event and send spade event fails THEN expect no error", func(t *testing.T) {
		lambdaHandler, mocks := setUpMocks()
		// GIVEN
		updateEvent := &schema.Update{
			UserId: "user-id",
			AnyEnabled: &change.BoolChange{
				Value: true,
			},
		}
		// create emote group entitlement
		mocks.mako.On("CreateEmoteGroupEntitlements", mock.Anything, mock.Anything).Return(nil, nil).Once()
		// notify user when they receive 2FA entitlement
		mocks.dartReceiver.On("PublishNotification", mock.Anything, mock.Anything).Return(nil, nil).Once()
		// fail to send event to spade that 2FA entitlement was granted
		mocks.spade.On("TrackEvent", mock.Anything, twoFactorDataScienceEventName, mock.Anything).Return(fmt.Errorf("failed to send spade event")).Once()

		// WHEN
		err := lambdaHandler.EntitleTwoFactorEmoteRewards(context.Background(), nil, updateEvent)

		// THEN
		assert.Nil(t, err)
	})

	t.Run("GIVEN a user that removes 2FA WHEN handling the 2FA entitlement event THEN expect no error", func(t *testing.T) {
		lambdaHandler, mocks := setUpMocks()
		// GIVEN
		updateEvent := &schema.Update{
			UserId: "user-id",
			AnyEnabled: &change.BoolChange{
				Value: false,
			},
		}
		// create emote group entitlement
		mocks.mako.On("CreateEmoteGroupEntitlements", mock.Anything, mock.Anything).Return(nil, nil).Once()
		// send event to spade that 2FA entitlement was removed
		mocks.spade.On("TrackEvent", mock.Anything, twoFactorDataScienceEventName, mock.Anything).Return(nil).Once()

		// WHEN
		err := lambdaHandler.EntitleTwoFactorEmoteRewards(context.Background(), nil, updateEvent)

		// THEN
		assert.Nil(t, err)
	})
}

type twoFactorMocks struct {
	spade        *spade_mock.Client
	dartReceiver *receiver_mock.Receiver
	mako         *mako_client_mock.Mako
}

func setUpMocks() (TwoFactorEntitlementLambda, twoFactorMocks) {
	mockSpadeClient := new(spade_mock.Client)
	mockDartReceiverClient := new(receiver_mock.Receiver)
	makoMock := new(mako_client_mock.Mako)

	return TwoFactorEntitlementLambda{
			SpadeClient:  mockSpadeClient,
			DartReceiver: mockDartReceiverClient,
			MakoClient:   makoMock,
		}, twoFactorMocks{
			spade:        mockSpadeClient,
			dartReceiver: mockDartReceiverClient,
			mako:         makoMock,
		}
}
