package main

import (
	"context"
	"fmt"
	"time"

	dart "code.justin.tv/amzn/TwitchDartReceiverTwirp"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/mako/clients/spade"
	mako_client "code.justin.tv/commerce/mako/twirp"
	goSpade "code.justin.tv/common/spade-client-go/spade"
	eventbus "code.justin.tv/eventbus/client"
	schema "code.justin.tv/eventbus/schema/pkg/user_multi_factor_auth"

	"github.com/pkg/errors"
	"google.golang.org/protobuf/types/known/timestamppb"
)

type TwoFactorEntitlementLambda struct {
	SpadeClient  goSpade.Client
	DartReceiver dart.Receiver
	MakoClient   mako_client.Mako
}

const (
	twoFactorRewardId             = "300374282"
	twoFactorEmoteDomain          = "2FA"
	qaTwPartnerId                 = "139075904"
	dartNotificationTypeId        = "two_factor_emote_rewards"
	twoFactorDataScienceEventName = "two_factor_reward_entitlement"
	tenMinutesAgo                 = -time.Minute * 10
)

func (s *TwoFactorEntitlementLambda) EntitleTwoFactorEmoteRewards(ctx context.Context, _ *eventbus.Header, updateEvent *schema.Update) error {
	if updateEvent.AnyEnabled != nil {
		if updateEvent.AnyEnabled.Value {
			// entitle 2FA emote reward
			err := s.processEvent(ctx, updateEvent, true)
			if err != nil {
				return errors.Wrap(err, "Error processing 2FA entitlement")
			}
			metadata := make(map[string]string)
			metadata["user_id"] = updateEvent.UserId

			_, err = s.DartReceiver.PublishNotification(ctx, &dart.PublishNotificationRequest{
				Recipient:                      &dart.PublishNotificationRequest_RecipientId{RecipientId: updateEvent.UserId},
				NotificationType:               dartNotificationTypeId,
				Metadata:                       metadata,
				DropNotificationBeforeDelivery: false,
			})

			if err != nil {
				log.WithError(err).Error("Error calling Dart for 2FA entitlement notification")
			}
		} else {
			// remove 2FA emote reward
			err := s.processEvent(ctx, updateEvent, false)
			if err != nil {
				return errors.Wrap(err, "Error processing 2FA entitlement removal")
			}
		}
	}
	log.Infof("Successfully processed 2FA emote entitlement event: %+v", updateEvent)
	return nil
}

func (s *TwoFactorEntitlementLambda) processEvent(ctx context.Context, event *schema.Update, entitle2FAReward bool) error {
	timeNow := time.Now()
	var startsAt *timestamppb.Timestamp
	var endsAt *timestamppb.Timestamp
	if entitle2FAReward {
		// set start time to now and end time to nil, which will be interpreted as infinite to entitle user to 2FA emote rewards
		// https://git.xarth.tv/commerce/mako/blob/5bad3091b3917652c60baff83ea984e5fe2db479/events/direct_processor.go#L84
		startsAt = timestamppb.New(timeNow)
		endsAt = nil
	} else {
		// set end time to now to remove a user's 2FA emote rewards
		startsAt = timestamppb.New(timeNow.Add(tenMinutesAgo))
		endsAt = timestamppb.New(timeNow)
	}

	req := &mako_client.CreateEmoteGroupEntitlementsRequest{
		Entitlements: []*mako_client.EmoteGroupEntitlement{
			{
				OwnerId:  event.UserId,
				ItemId:   qaTwPartnerId,
				GroupId:  twoFactorRewardId,
				OriginId: twoFactorEmoteDomain,
				Group:    mako_client.EmoteGroup_TwoFactor,
				Start:    startsAt,
				End: &mako_client.InfiniteTime{
					Time:       endsAt,
					IsInfinite: entitle2FAReward,
				},
			},
		},
	}

	_, err := s.MakoClient.CreateEmoteGroupEntitlements(ctx, req)
	if err != nil {
		return errors.Wrap(err, fmt.Sprintf("Error calling CreateEmoteGroupEntitlements %+v", req))
	}
	err = s.SpadeClient.TrackEvent(ctx, twoFactorDataScienceEventName, spade.TwoFactorEmoteEntitlementEvent{
		Time:     time.Now().Unix(),
		Entitled: entitle2FAReward,
		UserID:   event.UserId,
	})

	if err != nil {
		log.WithError(err).Warnf("Error attempting to send spade event for 2FA Entitlement for user: %s", event.UserId)
	}

	return nil
}
