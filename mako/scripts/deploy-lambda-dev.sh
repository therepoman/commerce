#!/bin/bash

set -e

# This script exists to allow you to deploy lambdas directly from your laptop.
# NOTE: This should ONLY be used for quickly iterating in Staging
# To use it, first run ./build-lambdas.sh to build the lambda, then run this with the name of the lambda.
# Example:
#   cd $GOPATH/src/code.justin.tv/commerce/mako
#   ./scripts/build-lambdas.sh lambda/functions
#   AWS_PROFILE=mako-devo ./scripts/deploy-lambda-dev.sh <name>


# Flags
builddir=./builds
name=$1

echo "-> [$name] deploying lambda..."

aws lambda update-function-code \
    --function-name mako-$name \
    --zip-file fileb://$builddir/$name.zip \
    --publish