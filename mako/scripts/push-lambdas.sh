#!/bin/bash

set -e

# Flags
s3bucket=$2
version=$3

# User Config
builddir=./builds
lambdadir=$1

push() {
    name=$1
    version=$2
    s3path=$name/$version

    echo "-> [$name] pushing to $s3bucket for version '$version'...."

    aws s3 cp $builddir/$name.zip s3://$s3bucket/$s3path/$name.zip
    aws s3 cp --content-type text/plain $builddir/$name.hash s3://$s3bucket/$s3path/hash
}

buildall() {
    for path in $lambdadir/*; do
        if [[ -d $path ]]; then
            name=$(basename $path)
            push $name $version &
        fi
    done
    wait
}

buildall