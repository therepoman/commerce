#!/bin/bash

set -e

# Flags
name=$1
s3bucket=$2
version=$3

name="${name//\/}"
version="${version//\/}"

echo "-> [$name] deploying lambda version $version from $s3bucket ..."

aws lambda update-function-code \
    --function-name mako-$name \
    --s3-bucket $s3bucket \
    --s3-key $name/$version/$name.zip \
    --publish