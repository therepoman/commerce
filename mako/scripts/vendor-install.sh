#!/bin/bash

which glide &> /dev/null || curl https://glide.sh/get | sh

glide install --strip-vcs --strip-vendor
./scripts/vendor-clean.sh
