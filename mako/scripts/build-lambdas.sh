#!/bin/bash

set -e

# Config
builddir=./builds
lambdadir=$1

clean() {
  echo "-> Cleaning build directory"
  rm -rf $builddir
}

build() {
    path=$1
    name=$2
    bin=$builddir/$name/$name
    currentdir=`pwd`

    GOOS=linux go build -o $bin $path/*.go
    cd $(dirname $bin) && zip -r ../$name.zip * > /dev/null && cd $currentdir

    openssl dgst -sha256 -binary $builddir/$name.zip | openssl enc -base64 > $builddir/$name.hash

    hash=$(cat $builddir/$name.hash)

    echo "-> [$name] computed hash: $hash"
}

buildall() {
    for path in $lambdadir/*; do
        if [[ -d $path ]]; then
            name=$(basename $path)
            echo "-> [$name] building lambda..."

            build $path $name &
        fi
    done
    wait
}

clean
buildall