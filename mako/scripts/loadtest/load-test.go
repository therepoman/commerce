package main

import (
	"context"
	"encoding/csv"
	"encoding/json"
	"fmt"
	"io"
	"math/rand"
	"net/http"
	"os"
	"time"

	makotwirp "code.justin.tv/commerce/mako/twirp"
	"github.com/segmentio/conf"
	"github.com/segmentio/ksuid"
	vegeta "github.com/tsenart/vegeta/lib"
)

var emotesByUser map[string][]string

type req struct {
	OwnerID string   `json:"owner_id"`
	Codes   []string `json:"codes"`
}

type config struct {
	Addr           string        `conf:"addr"`
	Rate           int           `conf:"rate"`
	Duration       time.Duration `conf:"duration"`
	MaxCodes       int           `conf:"max-codes"`
	UniqueMessages int           `conf:"unique-messages"`
	UsersPath      string        `conf:"users-path"`
	NumUsers       int           `conf:"num-users"`
}

func main() {
	rand.Seed(time.Now().UnixNano())

	config := config{
		Rate:           5,
		Duration:       5 * time.Second,
		MaxCodes:       30,
		UniqueMessages: 30,
		Addr:           "https://main.us-west-2.beta.mako.twitch.a2z.com",
		NumUsers:       100,
	}

	emotesByUser = make(map[string][]string)

	conf.Load(&config)

	f, err := os.OpenFile(config.UsersPath, 0666, os.ModePerm)
	if err != nil {
		fmt.Println(err)
		return
	}

	defer f.Close()

	csv := csv.NewReader(f)
	_, _ = csv.Read()

	users := make([]string, 0, 1000)

	for {
		row, err := csv.Read()
		if err != nil && err == io.EOF {
			break
		} else if err != nil {
			fmt.Println(err)
			return
		}

		users = append(users, row[0])
	}

	allUsers := make([]string, 0)
	for i := 0; i < config.NumUsers; i++ {
		allUsers = append(allUsers, users[randr(0, len(users)-1)])
	}

	mako := makotwirp.NewMakoProtobufClient(config.Addr, http.DefaultClient)

	for _, userID := range allUsers {
		emotes := make([]string, 0)
		resp, err := mako.GetActiveEmoteCodes(context.Background(), &makotwirp.GetActiveEmoteCodesRequest{
			UserId: userID,
		})

		if err != nil {
			fmt.Println(err)
			return
		}

		for code, _ := range resp.GetCodes() {
			emotes = append(emotes, code)
		}

		emotesByUser[userID] = emotes
	}

	reqs := generateReqs()
	makeLoadTest(config, reqs)
}

func randr(min, max int) int {
	return rand.Intn(max-min+1) + min
}

func generateReqs() []req {
	reqs := make([]req, 0)

	for userID, emotes := range emotesByUser {
		for i := 0; i < 50; i++ {
			codes := []string{}
			totalWords := randr(1, 10)
			totalEmotes := randr(0, totalWords)

			for k := 0; k < totalEmotes; k++ {
				if len(emotes) == 0 {
					continue
				}

				randomCode := randr(0, len(emotes)-1)
				codes = append(codes, emotes[randomCode])
			}

			for k := len(codes); k < totalWords; k++ {
				codes = append(codes, ksuid.New().String())
			}

			reqs = append(reqs, req{
				OwnerID: userID,
				Codes:   codes,
			})
		}
	}

	return reqs
}

func makeLoadTest(c config, reqs []req) {
	header := make(http.Header)
	header.Add("Content-Type", "application/json")

	targets := make([]vegeta.Target, 0)
	for _, req := range reqs {
		body, err := json.Marshal(req)
		if err != nil {
			panic(err)
		}

		fmt.Println(string(body))

		// Targets are round-robined.
		targets = append(targets, vegeta.Target{
			Method: "POST",
			URL:    c.Addr + makotwirp.MakoPathPrefix + "GetEmotesByCodes",
			Body:   body,
			Header: header,
		})

	}

	targeter := vegeta.NewStaticTargeter(targets...)
	attacker := vegeta.NewAttacker()
	metrics := vegeta.Metrics{}
	rate := vegeta.Rate{
		Freq: c.Rate,
		Per:  time.Second,
	}

	for res := range attacker.Attack(targeter, rate, c.Duration, "Mako Load Test") {
		metrics.Add(res)
	}

	metrics.Close()

	fmt.Printf("metrics: %+v\n", metrics)
}
