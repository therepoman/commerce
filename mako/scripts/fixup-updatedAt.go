package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"
	"sync"
	"sync/atomic"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
)

// Example usage: go run cmd/backfill-gsi/main.go -table TableName -count 4 -throttle 200 -dryrun

var (
	tableFlag       string = "prod_emotes"
	workerCountFlag int    = 5
	rateFlag        int    = 1000
	dryRunFlag      bool   = true
	counter         int32
	totalCounter    int32
)

type emote struct {
	ID          string `dynamodbav:"id"`
	OwnerID     string `dynamodbav:"ownerId"`
	GroupID     string `dynamodbav:"groupId"`
	Prefix      string `dynamodbav:"prefix"`
	Suffix      string `dynamodbav:"suffix"`
	Code        string `dynamodbav:"code"`
	State       string `dynamodbav:"state"`
	Domain      string `dynamodbav:"domain"`
	CreatedAt   int64  `dynamodbav:"createdAt"`
	UpdatedAt   int64  `dynamodbav:"updatedAt"`
	Order       int64  `dynamodbav:"order"`
	EmotesGroup string `dynamodbav:"emotesGroup"`
	ProductID   string `dynamodbav:"productId"`
}

type strEmote struct {
	ID          string `dynamodbav:"id"`
	OwnerID     string `dynamodbav:"ownerId"`
	GroupID     string `dynamodbav:"groupId"`
	Prefix      string `dynamodbav:"prefix"`
	Suffix      string `dynamodbav:"suffix"`
	Code        string `dynamodbav:"code"`
	State       string `dynamodbav:"state"`
	Domain      string `dynamodbav:"domain"`
	CreatedAt   int64  `dynamodbav:"createdAt"`
	UpdatedAt   string `dynamodbav:"updatedAt"`
	Order       int64  `dynamodbav:"order"`
	EmotesGroup string `dynamodbav:"emotesGroup"`
	ProductID   string `dynamodbav:"productId"`
}

func main() {
	// if !dryRunFlag {
	// 	fmt.Println("!!!! Dry run was turned OFF, you have 5 seconds before continuing...")
	// 	time.Sleep(5 * time.Second)
	// }

	fmt.Println("starting backfill...")

	sess := session.Must(session.NewSessionWithOptions(session.Options{
		SharedConfigState: session.SharedConfigEnable,
		Config: aws.Config{
			Region: aws.String("us-west-2"),
		},
	}))

	// Create DynamoDB client
	db := dynamodb.New(sess)

	waitGroup := &sync.WaitGroup{}
	scanner := Scanner{
		db:        db,
		waitGroup: waitGroup,
	}

	for i := 0; i < workerCountFlag; i++ {
		waitGroup.Add(1)
		go scanner.runScanThread(i)
	}
	waitGroup.Wait()

	fmt.Println("Operation completed!")
}

type Scanner struct {
	db        *dynamodb.DynamoDB
	waitGroup *sync.WaitGroup
	counter   uint64
}

func (s *Scanner) runScanThread(segment int) {
	defer s.waitGroup.Done()

	var lastEvaluatedKey map[string]*dynamodb.AttributeValue

	params := &dynamodb.ScanInput{
		TableName:     aws.String(tableFlag),
		Segment:       aws.Int64(int64(segment)),
		TotalSegments: aws.Int64(int64(workerCountFlag)),
	}

	for {
		if lastEvaluatedKey != nil {
			params.ExclusiveStartKey = lastEvaluatedKey
		}

		result, err := s.db.Scan(params)
		if err != nil {
			fmt.Printf("Query API call failed. \nsegment: %v \nparams: %v \nerror: %v", segment, params, err)
			continue
		}

		for _, i := range result.Items {
			var item emote
			if err = dynamodbattribute.UnmarshalMap(i, &item); err != nil {
				total := atomic.AddInt32(&counter, 1)

				if total%30 == 0 {
					fmt.Println("total fixed = ", total)
				}

				var strEmote strEmote
				if err := dynamodbattribute.UnmarshalMap(i, &strEmote); err != nil {
					fmt.Println("error: ", err)
					return
				}

				nano, err := strconv.Atoi(strEmote.UpdatedAt)
				if err != nil {
					fmt.Println("error", err, strEmote)
					return
				}

				update := expression.Set(
					expression.Name("updatedAt"),
					expression.Value(nano),
				)

				updateExpr, err := expression.NewBuilder().
					WithUpdate(update).
					Build()
				if err != nil {
					fmt.Printf("Error building expression: %v\n", err)
					os.Exit(1)
				}

				if dryRunFlag {
					fmt.Println("")
					fmt.Println("Key = ", strEmote.ID)
					for name, field := range updateExpr.Names() {
						key := strings.Replace(name, "#", ":", -1)
						fmt.Printf("[Updated Field] => %v: %v\n", *field, *updateExpr.Values()[key].N)
					}

					fmt.Printf("\n")
				} else {
					if _, err := s.db.UpdateItem(&dynamodb.UpdateItemInput{
						ExpressionAttributeNames:  updateExpr.Names(),
						UpdateExpression:          updateExpr.Update(),
						ExpressionAttributeValues: updateExpr.Values(),
						Key: map[string]*dynamodb.AttributeValue{
							"id": {
								S: aws.String(strEmote.ID),
							},
						},
						TableName: aws.String(tableFlag),
					}); err != nil {
						fmt.Printf("error updating key: %v\n", err)
						return
					}
				}

				continue
			} else {
				total := atomic.AddInt32(&totalCounter, 1)
				fixed := atomic.LoadInt32(&counter)

				if total%1000 == 0 {
					fmt.Printf("%v total, %v fixed\n", total, fixed)
				}
			}
		}

		if len(result.LastEvaluatedKey) == 0 {
			break
		}

		// Break out of loop if there is nothing left to scan
		lastEvaluatedKey = result.LastEvaluatedKey
		if len(result.Items) == 0 {
			break
		}
	}

	fmt.Printf("Segment %v completed! Updated %v records!  errors: %v \n", segment, atomic.LoadUint64(&s.counter), atomic.LoadInt32(&counter))
}
