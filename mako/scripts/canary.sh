#!/bin/sh

set -e

export AWS_PROFILE=twitch-mako-aws

id=$(aws ecs list-tasks --service=mako-canary --cluster=sonar | jq -r '.taskArns[0]')

res=$(aws ecs describe-tasks --tasks=$id --cluster=sonar)
containerInstanceArn=$(echo $res | jq -r '.tasks[0].containerInstanceArn')
port=$(echo $res | jq -r '.tasks[0].containers[0].networkBindings[1].hostPort')

ec2Id=$(aws ecs describe-container-instances --container-instances=$containerInstanceArn --cluster=sonar | jq -r '.containerInstances[0].ec2InstanceId')

ip=$(aws ec2 describe-instances --instance-ids=$ec2Id | jq -r '.Reservations[0].Instances[0].PrivateIpAddress')

printf $ip:$port

unset AWS_PROFILE