#!/bin/bash

set -e
PROFILE=mako-prod
TASK=$(aws --profile ${PROFILE} ecs list-tasks --cluster sonar --service-name mako-canary --desired-status RUNNING | jq -r '.taskArns[0]')
INSTANCE_ARN=$(aws --profile ${PROFILE} ecs describe-tasks --cluster sonar --tasks ${TASK} --query 'tasks[*].containerInstanceArn' --output text)
INSTANCE_ID=$(aws --profile ${PROFILE} ecs describe-container-instances --cluster sonar --container-instances ${INSTANCE_ARN} --query 'containerInstances[*].ec2InstanceId' --output text)
IP_ADDR=$(aws --profile ${PROFILE} ec2 describe-instances --filters "Name=instance-state-name,Values=running" "Name=instance-id,Values=${INSTANCE_ID}" --query 'Reservations[*].Instances[*].[PrivateIpAddress][0]' --output text)
PORT=$(aws --profile ${PROFILE} ecs describe-tasks --cluster sonar --tasks ${TASK} --query 'tasks[*].containers[*].networkBindings[?containerPort==`8000`].hostPort' | jq 'flatten[0]')


echo "================================================"
echo "You can now access Mako canary through localhost:8000"
echo "Canary instance and port: ${IP_ADDR}:${PORT}"
echo "================================================"

TC=mako-prod ssh -v -L 8000:${IP_ADDR}:${PORT} jumpbox
