package middleware

import (
	"context"
	"net/http"
)

const (
	AuthenticatedUserIDContextKey = "user-id"
)

func AuthenticatedUserID(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		authenticatedUserID := r.Header.Get("User-ID")
		ctx := context.WithValue(r.Context(), AuthenticatedUserIDContextKey, authenticatedUserID)
		h.ServeHTTP(w, r.WithContext(ctx))
	})
}
