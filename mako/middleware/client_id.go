package middleware

import (
	"context"
	"net/http"
)

type contextKey = string

const (
	ClientIDContextKey = contextKey("client-id")
)

func ClientIDMiddleware(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		clientID := r.Header.Get("Twitch-Client-ID")
		ctx := context.WithValue(r.Context(), ClientIDContextKey, clientID)
		h.ServeHTTP(w, r.WithContext(ctx))
	})
}
