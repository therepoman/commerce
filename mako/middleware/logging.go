package middleware

import (
	"context"

	log "code.justin.tv/commerce/logrus"
)

const (
	// This should be provided by GQL in the request context for free
	// from here https://git.xarth.tv/edge/graphql/blob/1b8798be3cb6b2ace0d6692b64621e0b9de97296/internal/geoip/middleware.go#L25
	GQLIPAddressHeaderKey = "ip-address"
)

// GetLogFieldsFromGQLRequest returns a log.Fields object containing info about the client provided by GQL, including
// IP Address, Client ID and Authenticated User ID.
//
// Note: This info either comes from GQL adding to the request context, or from this package's middleware
// extracting headers from the request and adding to the request context.
//
// Note: Some of this data, like IP address, is Personally Identifiable Information (PII). Take this into consideration
// when logging this information, and only do so when necessary.
func GetLogFieldsFromGQLRequest(ctx context.Context) log.Fields {
	if ctx == nil {
		return log.Fields{}
	}

	clientIDHeader := ctx.Value(ClientIDContextKey)
	clientIPAddressHeader := ctx.Value(GQLIPAddressHeaderKey)
	xForwardedForIPAddressHeader := ctx.Value(XForwardedForContextKey)
	authenticatedUserIDHeader := ctx.Value(AuthenticatedUserIDContextKey)

	return log.Fields{
		"clientID":               getStringFromHeader(clientIDHeader),
		"clientIPAddress":        getStringFromHeader(clientIPAddressHeader),
		"xForwardedForIPAddress": getStringFromHeader(xForwardedForIPAddressHeader),
		"authenticatedUserID":    getStringFromHeader(authenticatedUserIDHeader),
	}
}

// GetScrubbedLogFieldsFromGQLRequest returns a log.Fields object containing info about the client provided by GQL, including
// Client ID and Authenticated User ID.
//
// Note: This info either comes from GQL adding to the request context, or from this package's middleware
// extracting headers from the request and adding to the request context.
//
// This variation of GetLogFieldsFromGQLRequest does not log IP Addresses
func GetScrubbedLogFieldsFromGQLRequest(ctx context.Context) log.Fields {
	if ctx == nil {
		return log.Fields{}
	}

	clientIDHeader := ctx.Value(ClientIDContextKey)
	authenticatedUserIDHeader := ctx.Value(AuthenticatedUserIDContextKey)

	return log.Fields{
		"clientID":            getStringFromHeader(clientIDHeader),
		"authenticatedUserID": getStringFromHeader(authenticatedUserIDHeader),
	}
}

func getStringFromHeader(header interface{}) string {
	if header != nil {
		val, ok := header.(string)
		if ok {
			return val
		}
	}

	return ""
}
