package middleware

import (
	"net/http"
	"runtime/debug"

	log "code.justin.tv/commerce/logrus"
)

// PanicLogger logs panics as errors and prints stack trace. Note that it does
// not do any recovery. Recovery is handled by Twirp.
func PanicLogger(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			if err := recover(); err != nil {
				err, ok := err.(error)
				if !ok {
					log.Errorf("Panic encountered: %+v", err)
				} else {
					log.WithError(err).Error("Panic encountered")
				}
				debug.PrintStack()
			}
		}()

		h.ServeHTTP(w, r)
	})
}
