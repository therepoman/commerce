package middleware

import (
	"context"
	"net/http"
)

const (
	XForwardedForContextKey = "x-forwarded-for"
)

func XForwardedForMiddleware(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		forwardedForIPAddress := r.Header.Get("X-Forwarded-For")
		ctx := context.WithValue(r.Context(), XForwardedForContextKey, forwardedForIPAddress)
		h.ServeHTTP(w, r.WithContext(ctx))
	})
}
