package iamgenerator

import "time"

// Service represents an s2s service
type Service struct {
	ID                       string    `dynamodbav:"service_id"`
	Name                     string    `dynamodbav:"service_name"`
	RoleArn                  string    `dynamodbav:"role_arn"`
	ServicesReadOnlyRoleName string    `dynamodbav:"ro_role_shard_name"`
	AllowedArns              []string  `dynamodbav:"allowed_arns"`
	CapabilitiesTopicArn     string    `dynamodbav:"capabilities_topic_arn"`
	LastUpdatedByUser        string    `dynamodbav:"last_updated_by_user"`
	Private                  bool      `dynamodbav:"private"`
	SandstormRoleArn         string    `dynamodbav:"sandstorm_role_arn"`
	SandstormRoleName        string    `dynamodbav:"sandstorm_role_name"`
	SandstormSecretName      string    `dynamodbav:"sandstorm_secret_name"`
	CreatedAt                time.Time `dynamodbav:"created_at,unixtime"`
	UpdatedAt                time.Time `dynamodbav:"updated_at,unixtime"`
}
