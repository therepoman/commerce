package iamgenerator

import (
	"code.justin.tv/sse/malachai/pkg/config"
	"github.com/aws/aws-sdk-go/aws"
)

// Config is the configuration for IAMGenerator
type Config struct {
	CapabilitiesTableArn        string
	DiscoveryTableArn           string
	Environment                 string
	InventoryTableArn           string
	MalachaiAccountID           string
	PubkeyInvalidationTopicArn  string
	Region                      string
	RootUserArn                 string
	SandstormRoleArn            string
	SandstormKMSKeyID           string
	SandstormSecretsTableName   string
	SandstormTopicArn           string
	ServiceRegistrationTableArn string

	AWSConfigBase *aws.Config
}

// fillDefaults fills empty values with defaults
func (c *Config) fillDefaults() (err error) {
	resources, err := config.GetResources(c.Environment)
	if err != nil {
		return
	}

	if c.Region == "" {
		c.Region = resources.Region
	}

	if c.MalachaiAccountID == "" {
		c.MalachaiAccountID = resources.MalachaiAccountID
	}

	if c.CapabilitiesTableArn == "" {
		c.CapabilitiesTableArn = resources.CapabilitiesTableArn
	}

	if c.ServiceRegistrationTableArn == "" {
		c.ServiceRegistrationTableArn = resources.ServicesTableArn
	}

	if c.DiscoveryTableArn == "" {
		c.DiscoveryTableArn = resources.DiscoveryTableArn
	}

	if c.PubkeyInvalidationTopicArn == "" {
		c.PubkeyInvalidationTopicArn = resources.PubkeyInvalidationTopicARN
	}

	if c.RootUserArn == "" {
		c.RootUserArn = resources.RootUserArn
	}

	if c.SandstormSecretsTableName == "" {
		c.SandstormSecretsTableName = resources.SandstormSecretsTableName
	}

	if c.SandstormKMSKeyID == "" {
		c.SandstormKMSKeyID = resources.SandstormKMSKeyID
	}

	if c.SandstormRoleArn == "" {
		c.SandstormRoleArn = resources.SandstormRoleArn
	}

	if c.SandstormTopicArn == "" {
		c.SandstormTopicArn = resources.SandstormTopicArn
	}

	if c.InventoryTableArn == "" {
		c.InventoryTableArn = resources.InventoryTableArn
	}

	return
}
