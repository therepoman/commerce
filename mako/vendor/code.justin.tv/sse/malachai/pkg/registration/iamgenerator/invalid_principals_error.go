package iamgenerator

import (
	"fmt"
	"strings"

	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/service/iam"
)

// InvalidPrincipalsError is returned when invalid principals are sent when
// updating a role.
type InvalidPrincipalsError struct {
	Principals []string
}

// Error implements error
func (err InvalidPrincipalsError) Error() string {
	return fmt.Sprintf("Following principals are invalid: %s", strings.Join(err.Principals, ", "))
}

func isInvalidPrincipalsError(err error) bool {
	if awsErr, ok := err.(awserr.Error); ok {
		if awsErr.Code() == iam.ErrCodeMalformedPolicyDocumentException {
			return true
		}
	}
	return false
}
