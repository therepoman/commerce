package registration

import "strings"

// Sorter sorts arrays of services
type Sorter struct {
	Services []*Service
	By       func(s1, s2 *Service) bool
}

// Len implements sort.Interface.
func (s Sorter) Len() int {
	return len(s.Services)
}

// Swap implements sort.Interface.
func (s Sorter) Swap(i, j int) {
	s.Services[i], s.Services[j] = s.Services[j], s.Services[i]
}

// Less implements sort.Interface.
func (s Sorter) Less(i, j int) bool {
	return s.By(s.Services[i], s.Services[j])
}

// ByID compares services by id
func ByID(s1, s2 *Service) bool {
	return strings.Compare(s1.ID, s2.ID) < 0
}

// ByNameCaseInsensitive compares services by name
func ByNameCaseInsensitive(s1, s2 *Service) bool {
	return strings.Compare(strings.ToLower(s1.Name), strings.ToLower(s2.Name)) < 0
}
