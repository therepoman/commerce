package backoff

import (
	"errors"
	"time"
)

// Func is the func to retry based off of
type Func func() error

var (
	errInvalidMaxTries = errors.New("maxTries must be at least 1")
)

// ConstantRetry retries a function
func ConstantRetry(fn Func, maxTries int, rate time.Duration) (err error) {
	if maxTries < 1 {
		err = errInvalidMaxTries
		return
	}

	for nTry := 0; nTry < maxTries; nTry++ {
		err = fn()
		if err == nil {
			return
		}
		time.Sleep(rate)
	}
	return
}
