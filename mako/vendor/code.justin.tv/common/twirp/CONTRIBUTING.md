# Contributing #

## Setup ##

Twirp uses a golang centric development process, so some things may not be
familliar to non-go users. We'll try and be gentle getting you quickly to a
working development environment.

### Requirements ###

You will need git, golang 1.9+, python 2.7 installed and on the path. Install
them however you feel.

### Checkout ###

We are going to assume you're working on twirp in it's own workspace. If you're
not, likely you know what you are doing and can figure out the delta.

```
mkdir ws-twirp
cd ws-twirp
export GOPATH=`pwd`
export PATH=$GOPATH/bin:$PATH
go get code.justin.tv/common/twirp
cd src/code.justin.tv/common/twirp
git checkout -b my-work origin/develop
make
```

This will setup your environment, move you to a new git branch and build the
project. Hopefully nothing fails, file issues if it does.

## Developer Loop ##

Generally you want to make changes and run `make`, which will install all
dependencies we know about, build the core, and run all of the tests that we
have against all of the language we support.

If you are focusing on a specific language and want to just run it, run `make
test_go_client` or equivalent.

## Contributing Code ##

Twirp uses github pull requests. Fork a branch from `develop`, hack away at your
changes, run the test suite with `make`, and submit a PR.

## Releasing Versions ##

Notes for the core maintainers, @spencer and @marioizquierdo. Releasing versions
is their responsibility.

Twirp uses [Semantic versioning](http://semver.org/): `v<major>.<minor>.<patch>`.

 * Increment major if you're making a backwards-incompatible change.
 * Increment minor if you're adding a feature that's backwards-compatible.
 * Increment patch if you're making a bugfix.

To make a release, remember to update the version number in
[protoc-gen-twirp/version.go](./protoc-gen-twirp/version.go).

Twirp uses Github releases. To make a new release:
 1. Make a PR and get the changes approved. Include the version change in the PR
    (if you want multiple PRs on the same version, make a version branch and
    merge all those PRs on that branch).
 2. When your branch is merged in master, make a git tag: `git tag <version
    number>` and `git push origin --tags` on the merge commit to annotate the
    release.
 3. Go to Github https://git-aws.internal.justin.tv/common/twirp/releases and
    "Draft a new release".
 4. Make sure to document changes, specially when upgrade instructions are
    needed.
