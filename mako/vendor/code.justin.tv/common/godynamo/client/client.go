package client

import (
	"errors"
	"fmt"
	"strings"
	"strconv"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

const (
	dynamoActiveStatus                  = "ACTIVE"
	defaultWaitTimeout    time.Duration = 10000 * time.Millisecond
	lastUpdatedColumnName               = "lastUpdated"
)

type DynamoClientConfig struct {
	AwsRegion                         string
	TablePrefix                       string
	TableSuffix                       string
	EndpointOverride                  string
	CredentialsOverride               *credentials.Credentials
	TableExistenceWaitTimeoutOverride time.Duration
	TableStatusWaitTimeoutOverride    time.Duration
}

type DynamoClient struct {
	Dynamo       *dynamodb.DynamoDB
	ClientConfig *DynamoClientConfig
}

type ScanFilter struct {
	Expression *string
	Names      map[string]*string
	Values     map[string]*dynamodb.AttributeValue
}

type QueryFilter struct {
	KeyConditionExpression *string
	FilterExpression       *string
	Names                  map[string]*string
	Values                 map[string]*dynamodb.AttributeValue
	Limit                  *int64
	Descending             bool
	IndexName              *string
}

//NewClient creates a new dynamo client using the provided configuration
func NewClient(config *DynamoClientConfig) *DynamoClient {
	awsConfig := &aws.Config{
		Region:      aws.String(config.AwsRegion),
		Credentials: config.CredentialsOverride,
	}
	dynamo := dynamodb.New(session.New(), awsConfig)
	if config.EndpointOverride != "" {
		dynamo.Endpoint = config.EndpointOverride
	}
	return &DynamoClient{
		Dynamo:       dynamo,
		ClientConfig: config,
	}
}

func (c *DynamoClient) GetFullTableName(table BaseDynamoTable) string {
	var parts []string

	if c.ClientConfig.TablePrefix != "" {
		parts = append(parts, c.ClientConfig.TablePrefix)
	}

	parts = append(parts, table.GetTableName())

	if c.ClientConfig.TableSuffix != "" {
		parts = append(parts, c.ClientConfig.TableSuffix)
	}

	return strings.Join(parts, "_")
}

func (c *DynamoClient) PutItem(record DynamoTableRecord) error {
	fullTableName := c.GetFullTableName(record.GetTable())
	record.UpdateWithCurrentTimestamp()

	itemMap := record.NewAttributeMap()
	PopulateAttributesTime(itemMap, lastUpdatedColumnName, record.GetTimestamp())
	putItemInput := &dynamodb.PutItemInput{
		TableName: aws.String(string(fullTableName)),
		Item:      itemMap,
	}

	_, err := c.Dynamo.PutItem(putItemInput)
	if err != nil {
		return err
	}
	return nil
}

func populateAttributesUpdateTime(attributeUpdateMap map[string]*dynamodb.AttributeValueUpdate, attributeName string, time time.Time) {
	if time.UnixNano() > 0 {
		unixNanoStr := strconv.FormatInt(time.UnixNano(), 10)
		attributeUpdateMap[attributeName] = &dynamodb.AttributeValueUpdate{
			Action: aws.String("PUT"),
			Value:  &dynamodb.AttributeValue{N: aws.String(unixNanoStr)},
		}
	}
}

func attributeMapToAttributeUpdateMapWithAction(key map[string]*dynamodb.AttributeValue, attributeMap map[string]*dynamodb.AttributeValue, lastUpdated time.Time, action string) map[string]*dynamodb.AttributeValueUpdate {
	attributeUpdateMap := make(map[string]*dynamodb.AttributeValueUpdate)
	for attr, attrVal := range attributeMap {
		_, isKey := key[attr]
		if !isKey && attrVal != nil {
			attrUpdate := &dynamodb.AttributeValueUpdate{
				Action: aws.String(action),
				Value:  attrVal,
			}
			attributeUpdateMap[attr] = attrUpdate
		}
	}

	populateAttributesUpdateTime(attributeUpdateMap, lastUpdatedColumnName, lastUpdated)
	return attributeUpdateMap
}

func (c *DynamoClient) UpdateItem(record DynamoTableRecord) error {
	err := c.UpdateItemWithAction(record, "PUT")
	if err != nil {
		return err
	}
	return nil
}

func (c *DynamoClient) UpdateItemWithAction(record DynamoTableRecord, action string) error {
	fullTableName := c.GetFullTableName(record.GetTable())
	record.UpdateWithCurrentTimestamp()

	itemMap := record.NewAttributeMap()
	key := record.NewItemKey()
	updateItemInput := &dynamodb.UpdateItemInput{
		TableName:        aws.String(string(fullTableName)),
		Key:              key,
		AttributeUpdates: attributeMapToAttributeUpdateMapWithAction(key, itemMap, record.GetTimestamp(), action),
	}

	_, err := c.Dynamo.UpdateItem(updateItemInput)
	if err != nil {
		return err
	}
	return nil
}

func (c *DynamoClient) GetItem(record DynamoTableRecord) (DynamoTableRecord, error) {
	fullTableName := c.GetFullTableName(record.GetTable())

	getItemInput := &dynamodb.GetItemInput{
		TableName: aws.String(string(fullTableName)),
		Key:       record.NewItemKey(),
	}

	result, err := c.Dynamo.GetItem(getItemInput)
	if err != nil {
		return nil, errors.New(fmt.Sprintf("DynamoClient: Encountered an error calling dynamo GetItem, %v", err))
	}

	// Result map is empty if no record is found
	if len(result.Item) != 0 {
		lastUpdatedTime := OptionalTimeFromAttributes(result.Item, lastUpdatedColumnName)
		if lastUpdatedTime == nil {
			return nil, errors.New(fmt.Sprintf("DynamoClient: Encountered an error getting lastUpdated time after dynamo GetItem, %v", err))
		}

		record, err := record.GetTable().ConvertAttributeMapToRecord(result.Item)
		if err != nil {
			return nil, errors.New(fmt.Sprintf("DynamoClient: Encountered an error converting dyanmo GetItem result to record, %v", err))
		}

		record.ApplyTimestamp(*lastUpdatedTime)
		return record, nil
	} else {
		return nil, nil
	}
}

func (c *DynamoClient) DeleteItem(record DynamoTableRecord) error {
	fullTableName := c.GetFullTableName(record.GetTable())

	deleteItemInput := &dynamodb.DeleteItemInput{
		TableName: aws.String(string(fullTableName)),
		Key:       record.NewItemKey(),
	}

	_, err := c.Dynamo.DeleteItem(deleteItemInput)
	if err != nil {
		return err
	}
	return nil
}

func (c *DynamoClient) convertToRecordList(table DynamoTable, attributeValues []map[string]*dynamodb.AttributeValue) ([]DynamoTableRecord, error) {
	records := make([]DynamoTableRecord, len(attributeValues))

	for i, attributes := range attributeValues {
		toAdd, err := table.ConvertAttributeMapToRecord(attributes)
		if err != nil {
			return nil, err
		}
		records[i] = toAdd
	}
	return records, nil
}

func (c *DynamoClient) ScanTable(table DynamoTable, filter ScanFilter) (records []DynamoTableRecord, err error) {
	fullTableName := c.GetFullTableName(table)

	input := &dynamodb.ScanInput{
		TableName:                 aws.String(string(fullTableName)),
		FilterExpression:          filter.Expression,
		ExpressionAttributeNames:  filter.Names,
		ExpressionAttributeValues: filter.Values,
	}

	output, err := c.Dynamo.Scan(input)
	if err != nil {
		return nil, err
	}

	records, err = c.convertToRecordList(table, output.Items)
	if err != nil {
		return nil, err
	}

	// Continue scanning until we've exhausted the table
	for output.LastEvaluatedKey != nil {
		input.ExclusiveStartKey = output.LastEvaluatedKey
		output, err = c.Dynamo.Scan(input)
		if err != nil {
			return nil, err
		}
		toAppend, err := c.convertToRecordList(table, output.Items)
		if err != nil {
			return nil, err
		}

		records = append(records, toAppend...)
	}

	return records, nil
}

func (c *DynamoClient) QueryTable(table DynamoTable, filter QueryFilter) (records []DynamoTableRecord, err error) {
	fullTableName := c.GetFullTableName(table)

	input := &dynamodb.QueryInput{
		TableName:                 aws.String(string(fullTableName)),
		KeyConditionExpression:    filter.KeyConditionExpression,
		FilterExpression:          filter.FilterExpression,
		ExpressionAttributeNames:  filter.Names,
		ExpressionAttributeValues: filter.Values,
		Limit:     filter.Limit,
		IndexName: filter.IndexName,
	}

	if filter.Descending {
		input.ScanIndexForward = aws.Bool(false)
	}

	output, err := c.Dynamo.Query(input)
	if err != nil {
		return nil, err
	}

	records, err = c.convertToRecordList(table, output.Items)
	if err != nil {
		return nil, err
	}

	// Continue querying until we've exhausted the table or hit the limit
	for output.LastEvaluatedKey != nil && (filter.Limit == nil || int64(len(records)) < *filter.Limit) {
		input.ExclusiveStartKey = output.LastEvaluatedKey
		output, err = c.Dynamo.Query(input)
		if err != nil {
			return nil, err
		}
		toAppend, err := c.convertToRecordList(table, output.Items)
		if err != nil {
			return nil, err
		}

		records = append(records, toAppend...)
	}

	return records, nil
}

func (c *DynamoClient) describeTable(table DynamoTable) (*dynamodb.DescribeTableOutput, error) {
	fullTableName := c.GetFullTableName(table)
	dti := &dynamodb.DescribeTableInput{
		TableName: aws.String(string(fullTableName)),
	}

	dto, err := c.Dynamo.DescribeTable(dti)
	if err != nil {
		return nil, err
	}

	return dto, nil
}

func (c *DynamoClient) GetTableStatus(table DynamoTable) (string, error) {
	dto, err := c.describeTable(table)
	if err != nil {
		return "", err
	}
	return *dto.Table.TableStatus, nil
}

func (c *DynamoClient) TableExists(targetTable DynamoTable) (bool, error) {
	fullTableName := c.GetFullTableName(targetTable)
	var lastTableName *string
	for {
		params := &dynamodb.ListTablesInput{}
		if lastTableName != nil {
			params.ExclusiveStartTableName = lastTableName
		}
		result, err := c.Dynamo.ListTables(params)
		if err != nil {
			return false, err
		}
		for _, tableName := range result.TableNames {
			if *tableName == string(fullTableName) {
				return true, nil
			}
		}
		if result.LastEvaluatedTableName == nil {
			return false, nil
		}
		lastTableName = result.LastEvaluatedTableName
	}
}

func (c *DynamoClient) DeleteTable(table DynamoTable) error {
	c.WaitForTableToBeActive(table)
	fullTableName := c.GetFullTableName(table)
	params := &dynamodb.DeleteTableInput{
		TableName: aws.String(string(fullTableName)),
	}
	_, err := c.Dynamo.DeleteTable(params)
	if err != nil {
		return err
	}
	return nil
}

func (c *DynamoClient) waitForTableExistence(targetTable DynamoTable, shouldTableExist bool) error {
	// Creates a go routine that writes to a channel once table existence in expected state
	ch := make(chan error, 1)
	go func() {
		for {
			exists, err := c.TableExists(targetTable)
			if err != nil {
				ch <- err
				return
			}
			if exists == shouldTableExist {
				ch <- nil
				return
			}
		}
	}()

	timeout := defaultWaitTimeout
	if c.ClientConfig.TableExistenceWaitTimeoutOverride != 0 {
		timeout = c.ClientConfig.TableExistenceWaitTimeoutOverride
	}

	// Selects the first channel to respond. Either table existence check finishes or times out
	select {
	case result := <-ch:
		return result
	case <-time.After(timeout):
		fullTableName := c.GetFullTableName(targetTable)
		if shouldTableExist {
			return errors.New(fmt.Sprintf("DynamoClient: Timeout while waiting for table %s to exist", string(fullTableName)))
		} else {
			return errors.New(fmt.Sprintf("DynamoClient: Timeout while waiting for table %s to not exist", string(fullTableName)))
		}
	}
}

func (c *DynamoClient) WaitForTableToNotExist(targetTable DynamoTable) error {
	return c.waitForTableExistence(targetTable, false)
}

func (c *DynamoClient) WaitForTableToExist(targetTable DynamoTable) error {
	return c.waitForTableExistence(targetTable, true)
}

func (c *DynamoClient) WaitForTableStatus(targetTable DynamoTable, targetTableStatus string) error {
	// Creates a goroutine that writes to a channel once table in expected status
	ch := make(chan error, 1)
	go func() {
		for {
			status, err := c.GetTableStatus(targetTable)
			if err != nil {
				ch <- err
				return
			}
			if status == targetTableStatus {
				ch <- nil
				return
			}
		}
	}()

	timeout := defaultWaitTimeout
	if c.ClientConfig.TableStatusWaitTimeoutOverride != 0 {
		timeout = c.ClientConfig.TableStatusWaitTimeoutOverride
	}

	// Selects the first channel to respond. Either table status check finishes or times out
	select {
	case result := <-ch:
		return result
	case <-time.After(timeout):
		fullTableName := c.GetFullTableName(targetTable)
		return errors.New(fmt.Sprintf("DynamoClient: Timeout while waiting for table %s to move to status %s", string(fullTableName), targetTableStatus))
	}
}

func (c *DynamoClient) WaitForTableToExistInStatus(targetTable DynamoTable, targetTableStatus string) error {
	err := c.WaitForTableToExist(targetTable)
	if err != nil {
		return err
	}

	err = c.WaitForTableStatus(targetTable, targetTableStatus)
	if err != nil {
		return err
	}
	return nil
}

func (c *DynamoClient) WaitForTableToBeActive(targetTable DynamoTable) error {
	return c.WaitForTableToExistInStatus(targetTable, dynamoActiveStatus)
}

func (c *DynamoClient) CreateTable(input *dynamodb.CreateTableInput) error {
	_, err := c.Dynamo.CreateTable(input)
	return err
}

func (c *DynamoClient) SetupTable(table DynamoTable) error {
	fullTableName := c.GetFullTableName(table)

	tableExists, err := c.TableExists(table)
	if err != nil {
		return err
	}

	if !tableExists {
		cti := table.NewCreateTableInput()
		// No matter what table name they specify in the input, we overwrite it with the full table name
		// which includes prefix and suffix.
		cti.TableName = aws.String(string(fullTableName))
		err = c.CreateTable(cti)
		if err != nil {
			return err
		}

		err = c.WaitForTableToBeActive(table)
		if err != nil {
			return err
		}
	}

	return nil
}
