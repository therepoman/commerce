package client

import (
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"time"
)

type BaseDynamoTable interface {
	GetTableName() string
}

type DynamoTable interface {
	BaseDynamoTable
	NewCreateTableInput() *dynamodb.CreateTableInput
	ConvertAttributeMapToRecord(attributeMap map[string]*dynamodb.AttributeValue) (DynamoTableRecord, error)
}

type DynamoTableRecord interface {
	GetTable() DynamoTable
	NewAttributeMap() map[string]*dynamodb.AttributeValue
	NewItemKey() map[string]*dynamodb.AttributeValue
	GetTimestamp() time.Time
	UpdateWithCurrentTimestamp()
	ApplyTimestamp(timestamp time.Time)
	Equals(other DynamoTableRecord) bool
}
