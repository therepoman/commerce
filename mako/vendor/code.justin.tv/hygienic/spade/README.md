# spade
[![GoDoc](https://img.shields.io/badge/godoc-reference-5272B4.svg?style=flat-square)](https://godoc.internal.justin.tv/code.justin.tv/hygienic/spade)
[![Build Status](https://jenkins.internal.justin.tv/buildStatus/icon?job=hygienic/spade/master)](https://jenkins.internal.justin.tv/job/hygienic/job/spade/job/master/)

Spade is a client to send events to spade.

**Spade.Client.Statter and Spade.Client.TaggingStats has been deprecated. Use the Reporter field**

# Usage

```go
var dconf distconf.Distconf
// ... setup distconf if you want

// Load spade config
config := spadedconf.Config{}
err := config.Load(&dconf)
if err != nil {
  return err
}

// Enable ability to combine small batches (optional)
config.CombineSmallBatches = true

// Create a spade client
spadeClient := spade.Client{
  Config: &config,
  // Add an optional Reporter for metrics. See examples in the next section of the README
}

err = spadeClient.Setup()
if err != nil {
  return err
}

// Must call Start() to drain the channel
go func() {
  err := spadeClient.Start()
  if err != nil {
    fmt.Printf("error starting spade client: %v", err)
  }
}()

// Queue an event
spadeClient.QueueEvents(spade.Event{
  Name: "feed_server_post",
  Properties: struct {
    AuthorID string `json:"author_id"`
  }{
    AuthorID: "1234",
  },
})

// ...
// When done, close the spade client
err = spadeClient.Close()
if err != nil {
  return err
}

return nil
```

## Metrics

The spade client's Reporter can be initialized with an implementation-specific Reporter.

spade.MultiReporter can be used to chain together multiple reporters to dual send

### StatsD

```go
import (
  "code.justin.tv/hygienic/spade/spademetrics/statsdmetrics"
)

spadeClient := spade.Client{
  Config: &config,
  Reporter: &statsdmetrics.Reporter{
    Statter: statter,
    SampleRate: 0.1,
    // Optional logger
  }
}
```

### TaggingStatter

```go
import (
  "code.justin.tv/hygienic/spade/spademetrics/taggingstatsmetrics"
)

spadeClient := spade.Client{
  Config: &config,
  Reporter: &taggingstatsmetrics.Reporter{
    Statter: taggingStatter,
  }
}
```

### TwitchTelemetry

```go
import (
  "code.justin.tv/hygienic/spade/spademetrics/telemetrymetrics"
)

spadeClient := spade.Client{
  Config: &config,
  Reporter: &telemetrymetrics.Reporter{
    SampleReporter: sampleReporter, // sampleReporter is a *telemetry.SampleReporter
  }
}
```
