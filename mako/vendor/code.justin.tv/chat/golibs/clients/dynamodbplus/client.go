package dynamodbplus

import (
	"context"
	"errors"
	"fmt"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/request"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"

	"code.justin.tv/chat/golibs/errx"
	"code.justin.tv/chat/timing"
)

// NewClient returns a client that satisfies a dynamodbplus.Client interface.
func NewClient(conf Config) Client {
	awsConf := &aws.Config{
		Region:      aws.String(conf.Region),
		Credentials: conf.Credentials,
	}
	if conf.HTTPClient != nil {
		awsConf.HTTPClient = conf.HTTPClient
	}
	if conf.DisableSSL {
		awsConf.DisableSSL = aws.Bool(true)
	}
	if conf.Endpoint != "" {
		awsConf.Endpoint = aws.String(conf.Endpoint)
	}

	return &clientImpl{
		db:        dynamodb.New(session.New(awsConf)),
		xactGroup: conf.XactGroup,
	}
}

// NewFromAWSConfig returns a client that satisfies a dynamodbplus.Client interface.
func NewFromAWSConfig(conf ConfigFromAWSConfig) (Client, error) {
	if conf.AwsConfig == nil {
		return nil, errors.New("missing aws config")
	}
	session, err := session.NewSession(conf.AwsConfig)
	if err != nil {
		return nil, err
	}
	return &clientImpl{
		db:        dynamodb.New(session),
		xactGroup: conf.XactGroup,
	}, nil
}

type clientImpl struct {
	db        dynamodbiface.DynamoDBAPI
	xactGroup string
}

func (c *clientImpl) UpdateItem(ctx context.Context, in *dynamodb.UpdateItemInput) (*dynamodb.UpdateItemOutput, bool, error) {
	xact, ok := timing.XactFromContext(ctx)
	if ok {
		sub := xact.Sub(c.xactGroup)
		sub.Start()
		defer sub.End()
	}

	in.ReturnConsumedCapacity = aws.String("TOTAL")

	out, err := c.db.UpdateItemWithContext(ctx, in)
	if err != nil && IsConditionalCheckFailed(err) {
		c.trackConsumedCapacity(ctx, Write, failedConditionConsumedCapacity(in.TableName))
		return nil, true, nil
	}

	c.trackConsumedCapacity(ctx, Write, out.ConsumedCapacity)
	if err != nil {
		return nil, false, c.awsFields(ctx, err)
	}
	return out, false, nil
}

func (c *clientImpl) PutItem(ctx context.Context, in *dynamodb.PutItemInput) (*dynamodb.PutItemOutput, bool, error) {
	xact, ok := timing.XactFromContext(ctx)
	if ok {
		sub := xact.Sub(c.xactGroup)
		sub.Start()
		defer sub.End()
	}

	in.ReturnConsumedCapacity = aws.String("TOTAL")

	out, err := c.db.PutItemWithContext(ctx, in)
	if err != nil && IsConditionalCheckFailed(err) {
		c.trackConsumedCapacity(ctx, Write, failedConditionConsumedCapacity(in.TableName))
		return nil, true, nil
	}
	c.trackConsumedCapacity(ctx, Write, out.ConsumedCapacity)
	if err != nil {
		return nil, false, c.awsFields(ctx, err)
	}
	return out, false, nil
}

func (c *clientImpl) DeleteItem(ctx context.Context, in *dynamodb.DeleteItemInput) (*dynamodb.DeleteItemOutput, bool, error) {
	xact, ok := timing.XactFromContext(ctx)
	if ok {
		sub := xact.Sub(c.xactGroup)
		sub.Start()
		defer sub.End()
	}

	in.ReturnConsumedCapacity = aws.String("TOTAL")

	out, err := c.db.DeleteItemWithContext(ctx, in)
	if err != nil && IsConditionalCheckFailed(err) {
		c.trackConsumedCapacity(ctx, Write, failedConditionConsumedCapacity(in.TableName))
		return nil, true, nil
	}

	c.trackConsumedCapacity(ctx, Write, out.ConsumedCapacity)
	if err != nil {
		return nil, false, c.awsFields(ctx, err)
	}
	return out, false, nil
}

func (c *clientImpl) DescribeTable(ctx context.Context, in *dynamodb.DescribeTableInput) (*dynamodb.DescribeTableOutput, error) {
	xact, ok := timing.XactFromContext(ctx)
	if ok {
		sub := xact.Sub(c.xactGroup)
		sub.Start()
		defer sub.End()
	}

	return c.db.DescribeTableWithContext(ctx, in)
}

func (c *clientImpl) GetItem(ctx context.Context, in *dynamodb.GetItemInput) (*dynamodb.GetItemOutput, error) {
	xact, ok := timing.XactFromContext(ctx)
	if ok {
		sub := xact.Sub(c.xactGroup)
		sub.Start()
		defer sub.End()
	}

	in.ReturnConsumedCapacity = aws.String("TOTAL")

	out, err := c.db.GetItemWithContext(ctx, in)
	c.trackConsumedCapacity(ctx, Read, out.ConsumedCapacity)
	if err != nil {
		return nil, c.awsFields(ctx, err)
	}
	return out, nil
}

func (c *clientImpl) BatchGetItem(ctx context.Context, in *dynamodb.BatchGetItemInput) (*dynamodb.BatchGetItemOutput, error) {
	xact, ok := timing.XactFromContext(ctx)
	if ok {
		sub := xact.Sub(c.xactGroup)
		sub.Start()
		defer sub.End()
	}

	in.ReturnConsumedCapacity = aws.String("TOTAL")

	out, err := c.db.BatchGetItemWithContext(ctx, in)
	for _, consumedCapacity := range out.ConsumedCapacity {
		c.trackConsumedCapacity(ctx, Read, consumedCapacity)
	}
	if err != nil {
		return nil, c.awsFields(ctx, err)
	}
	return out, nil
}

func (c *clientImpl) BatchWriteItem(ctx context.Context, in *dynamodb.BatchWriteItemInput) (*dynamodb.BatchWriteItemOutput, error) {
	xact, ok := timing.XactFromContext(ctx)
	if ok {
		sub := xact.Sub(c.xactGroup)
		sub.Start()
		defer sub.End()
	}

	in.ReturnConsumedCapacity = aws.String("TOTAL")

	out, err := c.db.BatchWriteItemWithContext(ctx, in)
	for _, consumedCapacity := range out.ConsumedCapacity {
		c.trackConsumedCapacity(ctx, Write, consumedCapacity)
	}
	if err != nil {
		return nil, c.awsFields(ctx, err)
	}
	return out, nil
}

func (c *clientImpl) Scan(ctx context.Context, in *dynamodb.ScanInput) (*dynamodb.ScanOutput, error) {
	xact, ok := timing.XactFromContext(ctx)
	if ok {
		sub := xact.Sub(c.xactGroup)
		sub.Start()
		defer sub.End()
	}

	in.ReturnConsumedCapacity = aws.String("TOTAL")

	out, err := c.db.ScanWithContext(ctx, in)
	c.trackConsumedCapacity(ctx, Read, out.ConsumedCapacity)
	if err != nil {
		return nil, c.awsFields(ctx, err)
	}
	return out, nil
}

// ScanPages iterates over the pages of a Scan operation. The 'fn' function is called on each page. Iteration ends when this function returns 'false'.
// In most cases, 'fn' should return 'false' when out.LastEvaluatedKey is empty, indicating that there are no more pages to be scanned.
func (c *clientImpl) ScanPages(ctx context.Context, in *dynamodb.ScanInput, fn func(*dynamodb.ScanOutput, bool) bool, opts ...request.Option) error {
	xact, ok := timing.XactFromContext(ctx)
	if ok {
		sub := xact.Sub(c.xactGroup)
		sub.Start()
		defer sub.End()
	}

	// Wrap 'fn' with capacity tracking.
	in.ReturnConsumedCapacity = aws.String("TOTAL")
	fnWithCapacityTracking := func(out *dynamodb.ScanOutput, last bool) bool {
		c.trackConsumedCapacity(ctx, Read, out.ConsumedCapacity)
		return fn(out, last)
	}

	if err := c.db.ScanPagesWithContext(ctx, in, fnWithCapacityTracking, opts...); err != nil {
		c.awsFields(ctx, err)
	}
	return nil
}

func (c *clientImpl) Query(ctx context.Context, in *dynamodb.QueryInput) (*dynamodb.QueryOutput, error) {
	xact, ok := timing.XactFromContext(ctx)
	if ok {
		sub := xact.Sub(c.xactGroup)
		sub.Start()
		defer sub.End()
	}

	in.ReturnConsumedCapacity = aws.String("TOTAL")

	out, err := c.db.QueryWithContext(ctx, in)
	c.trackConsumedCapacity(ctx, Read, out.ConsumedCapacity)
	if err != nil {
		return nil, c.awsFields(ctx, err)
	}
	return out, nil
}

func (c *clientImpl) CreateTable(ctx context.Context, in *dynamodb.CreateTableInput) (*dynamodb.CreateTableOutput, error) {
	xact, ok := timing.XactFromContext(ctx)
	if ok {
		sub := xact.Sub(c.xactGroup)
		sub.Start()
		defer sub.End()
	}

	output, err := c.db.CreateTableWithContext(ctx, in)
	return output, c.awsFields(ctx, err)
}

func (c *clientImpl) DeleteTable(ctx context.Context, in *dynamodb.DeleteTableInput) (*dynamodb.DeleteTableOutput, error) {
	xact, ok := timing.XactFromContext(ctx)
	if ok {
		sub := xact.Sub(c.xactGroup)
		sub.Start()
		defer sub.End()
	}

	output, err := c.db.DeleteTableWithContext(ctx, in)
	return output, c.awsFields(ctx, err)
}

func (c *clientImpl) trackConsumedCapacity(ctx context.Context, op opType, consumedCapacity *dynamodb.ConsumedCapacity) {
	capacity, ok := CapacityFromContext(ctx)
	if !ok || consumedCapacity == nil {
		return
	}
	key := fmt.Sprintf("%s_%s", *consumedCapacity.TableName, op)
	capacity.Lock()
	capacity.m[key] += float32(*consumedCapacity.CapacityUnits)
	capacity.Unlock()
}

func failedConditionConsumedCapacity(tableName *string) *dynamodb.ConsumedCapacity {
	return &dynamodb.ConsumedCapacity{
		TableName: tableName,
		// A failed conditional write stil consumes one write capactiy.
		// Source: http://docs.aws.amazon.com/amazondynamodb/latest/developerguide/WorkingWithItems.html
		CapacityUnits: aws.Float64(1),
	}
}

// awsFields copies any AWS-specific error fields from the given error into its errx.Fields field, then returns the
// modified error.
func (c *clientImpl) awsFields(ctx context.Context, err error) error {
	var fields errx.Fields

	if reqerr, ok := err.(awserr.RequestFailure); ok {
		fields = errx.Fields{
			"aws_request_id":  reqerr.RequestID(),
			"aws_status_code": reqerr.StatusCode(),
		}
	}

	return errx.New(err, fields)
}
