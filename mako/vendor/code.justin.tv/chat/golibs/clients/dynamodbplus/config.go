package dynamodbplus

import (
	"net/http"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
)

// Config defines variable parameters to initialize a DynamoDB client.
type Config struct {
	// Endpoint can be used to point the client at a local dynamodb instance.
	// Otherwise, there is no need to set this for dynamodb instances in AWS.
	Endpoint string

	// Region is the AWS region where the DynamoDB tables live.
	Region string

	// Credentials are AWS access/secret keys. It is optional and if left nil,
	// it will fall back to EC2 roles or ~/.aws/credentials.
	Credentials *credentials.Credentials

	// HTTPClient overrides the default HTTP client used by the AWS SDK.
	// It is recommended to set this client for production services.
	HTTPClient *http.Client

	// DisableSSL disables SSL when sending requests if set to true. Defaults to `false`
	DisableSSL bool

	// XactGroup is optional. Defaults to "dynamodb".
	XactGroup string
}

type ConfigFromAWSConfig struct {
	// Native AWS configuration
	AwsConfig *aws.Config

	// XactGroup is optional. Defaults to "dynamodb".
	XactGroup string
}
