package dynamodbplus

import (
	"encoding/base64"
	"encoding/json"

	"github.com/aws/aws-sdk-go/service/dynamodb"
)

// EncodeCursor encodes a LastEvaluatedKey response from DynamoDB into a string.
func EncodeCursor(key map[string]*dynamodb.AttributeValue) (string, error) {
	if len(key) == 0 {
		return "", nil
	}

	buf, err := json.Marshal(key)
	if err != nil {
		return "", err
	}

	cursor := base64.URLEncoding.WithPadding(base64.NoPadding).EncodeToString(buf)
	return cursor, nil
}

// DecodeCursor decodes a cursor string into a type that can be used in an
// ExclusiveStartKey field.
func DecodeCursor(cursor string) (map[string]*dynamodb.AttributeValue, error) {
	if cursor == "" {
		return nil, nil
	}

	cursorJSON, err := base64.URLEncoding.WithPadding(base64.NoPadding).DecodeString(cursor)
	if err != nil {
		return nil, err
	}

	var key map[string]*dynamodb.AttributeValue
	if err := json.Unmarshal([]byte(cursorJSON), &key); err != nil {
		return nil, err
	}
	return key, nil
}
