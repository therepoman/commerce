package dynamodbplus

import (
	"context"
	"fmt"
	"sync"

	"github.com/cactus/go-statsd-client/statsd"
)

type contextKey int
type opType string

const (
	CapacityKey contextKey = iota
)

const (
	Read  = opType("read")
	Write = opType("write")
)

type Capacity struct {
	sync.Mutex
	m     map[string]float32
	stats statsd.Statter
}

func CapacityContext(ctx context.Context, c *Capacity) context.Context {
	return context.WithValue(ctx, CapacityKey, c)
}

func CapacityFromContext(ctx context.Context) (*Capacity, bool) {
	c, ok := ctx.Value(CapacityKey).(*Capacity)
	return c, ok
}

func NewCapacity(stats statsd.Statter) *Capacity {
	return &Capacity{
		m:     map[string]float32{},
		stats: stats,
	}
}

func (c *Capacity) Reset() {
	c.m = map[string]float32{}
}

func (c *Capacity) FlushStats(statName string) {
	for k, v := range c.m {
		key := fmt.Sprintf("%s.%s", statName, k)
		_ = c.stats.Inc(key, int64(v*10), 1.0)
	}
}
