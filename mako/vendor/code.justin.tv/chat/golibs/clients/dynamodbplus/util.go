package dynamodbplus

import (
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/golang/protobuf/ptypes/struct"

	"code.justin.tv/chat/golibs/errx"
)

// BoolAttr returns a BOOL AttributeValue from a bool.
func BoolAttr(b bool) *dynamodb.AttributeValue {
	return &dynamodb.AttributeValue{BOOL: &b}
}

// Int64Attr returns an N AttributeValue from an int64.
func Int64Attr(i int64) *dynamodb.AttributeValue {
	return &dynamodb.AttributeValue{N: aws.String(strconv.FormatInt(i, 10))}
}

// Float64Attr returns an N AttributeValue from a float64.
func Float64Attr(f float64) *dynamodb.AttributeValue {
	return &dynamodb.AttributeValue{N: aws.String(strconv.FormatFloat(f, 'f', -1, 64))}
}

// StringAttr returns an S AttributeValue from a string.
func StringAttr(s string) *dynamodb.AttributeValue {
	return &dynamodb.AttributeValue{S: aws.String(s)}
}

// MapAttr returns an M AttributeValue from a map of AttributeValues.
func MapAttr(m map[string]*dynamodb.AttributeValue) *dynamodb.AttributeValue {
	return &dynamodb.AttributeValue{M: m}
}

// ListAttr returns an L AttributeValue from an arbitrary number of AttributeValues.
func ListAttr(l ...*dynamodb.AttributeValue) *dynamodb.AttributeValue {
	return &dynamodb.AttributeValue{L: l}
}

// NullAttr returns a NULL AttributeValue.
func NullAttr() *dynamodb.AttributeValue {
	return &dynamodb.AttributeValue{NULL: aws.Bool(true)}
}

// RFC3339TimeAttr returns an RFC 3339 S AttributeValue from a time.Time.

// See UnixTimeAttr to use Unix time. RFC 3339 time is easier for humans to read and more precise, while Unix time is
// easier to sort and is the only layout usable in DynamoDB TTL fields.
func RFC3339TimeAttr(t time.Time, layout string) *dynamodb.AttributeValue {
	return &dynamodb.AttributeValue{S: aws.String(t.Format(time.RFC3339))}
}

// UnixTimeAttr returns a Unix time N AttributeValue from a time.Time.
//
// See TimeAttr to use RFC 3339 format. RFC 3339 time is easier for humans to read and more precise, while Unix time is
// easier to sort and is the only layout usable in DynamoDB TTL fields.
func UnixTimeAttr(t time.Time) *dynamodb.AttributeValue {
	return &dynamodb.AttributeValue{N: aws.String(strconv.FormatInt(t.Unix(), 10))}
}

// ProtoStructAttr returns an M AttributeValue from a protobuf ptypes structpb.Struct.
func ProtoStructAttr(s *structpb.Struct) (*dynamodb.AttributeValue, error) {
	m := make(map[string]*dynamodb.AttributeValue)
	for k, v := range s.GetFields() {
		attr, err := ProtoStructValueAttr(v)
		if err != nil {
			return nil, err
		}
		m[k] = attr
	}
	// DynamoDB doesn't allow empty maps :(
	if len(m) == 0 {
		return NullAttr(), nil
	}
	return &dynamodb.AttributeValue{M: m}, nil
}

// ProtoStructValueAttr returns an AttributeValue from a protobuf ptypes structpb.Value.
func ProtoStructValueAttr(v *structpb.Value) (*dynamodb.AttributeValue, error) {
	switch v.Kind.(type) {
	case *structpb.Value_NullValue:
		return NullAttr(), nil
	case *structpb.Value_NumberValue:
		return Float64Attr(v.GetNumberValue()), nil
	case *structpb.Value_StringValue:
		return StringAttr(v.GetStringValue()), nil
	case *structpb.Value_BoolValue:
		return BoolAttr(v.GetBoolValue()), nil
	case *structpb.Value_StructValue:
		return ProtoStructAttr(v.GetStructValue())
	case *structpb.Value_ListValue:
		return ProtoStructListAttr(v.GetListValue())
	}
	return nil, errx.New("structpb had an unexpected Kind type", errx.Fields{
		"kind": v.Kind,
	})
}

// ProtoStructListAttr returns an L AttributeValue from a protobuf ptypes structpb.ListValue.
func ProtoStructListAttr(p *structpb.ListValue) (*dynamodb.AttributeValue, error) {
	var l []*dynamodb.AttributeValue
	for _, v := range p.GetValues() {
		attr, err := ProtoStructValueAttr(v)
		if err != nil {
			return nil, err
		}
		l = append(l, attr)
	}
	return &dynamodb.AttributeValue{L: l}, nil
}

// BuildUpdateExpression transforms
// 	{
// 		"foo": ":foo",
// 		"bar": "baz"
// 	}
// to "SET foo = :foo, bar = baz"
func BuildUpdateExpression(updates map[string]string) *string {
	if len(updates) == 0 {
		return nil
	}

	tokens := make([]string, 0, len(updates))
	for k, v := range updates {
		if k != "" && v != "" {
			tokens = append(tokens, fmt.Sprintf("%s = %s", k, v))
		}
	}
	return aws.String("SET " + strings.Join(tokens, ", "))
}

// BuildConditionExpression transforms
// ["foo > :foo", "bar = baz"]
// to "foo > :foo AND bar = baz"
func BuildConditionExpression(conditions []string) *string {
	if len(conditions) == 0 {
		return nil
	}

	return aws.String(strings.Join(conditions, " AND "))
}

// IsConditionalCheckFailed returns true if the error indicates a DynamoDB request failed
// the conditional statement. The error passed in should be the error immediately returned
// by an aws-sdk-go DynamoDB interface method.
func IsConditionalCheckFailed(err error) bool {
	awsErr, ok := err.(awserr.Error)
	return ok && awsErr.Code() == dynamodb.ErrCodeConditionalCheckFailedException
}

// StringFromItem extracts a string from a map of AttributeValues.
func StringFromItem(item map[string]*dynamodb.AttributeValue, key string) (string, error) {
	val, ok := item[key]
	if !ok || val.S == nil {
		return "", fmt.Errorf("string key %s not found in item", key)
	}
	return *val.S, nil
}

// Int64FromItem extracts an int64 from a map of AttributeValues.
func Int64FromItem(item map[string]*dynamodb.AttributeValue, key string) (int64, error) {
	val, ok := item[key]
	if !ok || val.N == nil {
		return 0, fmt.Errorf("int64 key %s not found in item", key)
	}
	intVal, err := strconv.ParseInt(*val.N, 10, 64)
	if err != nil {
		return 0, fmt.Errorf("parsing int64 from string %s: %v", key, err)
	}
	return intVal, nil
}

// Float64FromItem extracts a float64 from a map of AttributeValues.
func Float64FromItem(item map[string]*dynamodb.AttributeValue, key string) (float64, error) {
	val, ok := item[key]
	if !ok || val.N == nil {
		return 0, fmt.Errorf("float64 key %s not found in item", key)
	}
	floatVal, err := strconv.ParseFloat(*val.N, 64)
	if err != nil {
		return 0, fmt.Errorf("parsing float64 from string %s: %v", key, err)
	}
	return floatVal, nil
}

// BoolFromItem extracts a boolean from a map of AttributeValues.
func BoolFromItem(item map[string]*dynamodb.AttributeValue, key string) (bool, error) {
	val, ok := item[key]
	if !ok || val.BOOL == nil {
		return false, fmt.Errorf("bool key %s not found in item", key)
	}
	return *val.BOOL, nil
}

// MapFromItem extracts a map from a map of AttributeValues.
func MapFromItem(item map[string]*dynamodb.AttributeValue, key string) (map[string]*dynamodb.AttributeValue, error) {
	val, ok := item[key]
	if !ok || val.M == nil {
		return nil, fmt.Errorf("map key %s not found in item", key)
	}
	return val.M, nil
}

// ListFromItem extracts a list from a map of AttributeValues.
func ListFromItem(item map[string]*dynamodb.AttributeValue, key string) ([]*dynamodb.AttributeValue, error) {
	val, ok := item[key]
	if !ok || val.L == nil {
		return nil, fmt.Errorf("list key %s not found in item", key)
	}
	return val.L, nil
}

// UnixTimeFromItem extracts a time.Time from a map of AttributeValues containing a Unix time N field.
//
// See RFC3339TimeFromItem if you're using RFC 3339 format. RFC 3339 time is easier for humans to read and more precise,
// while Unix time is easier to sort and is the only layout usable in DynamoDB TTL fields.
func UnixTimeFromItem(item map[string]*dynamodb.AttributeValue, key string) (time.Time, error) {
	val, ok := item[key]
	if !ok || val.N == nil {
		return time.Time{}, fmt.Errorf("time key %s not found in item N", key)
	}
	intVal, err := strconv.ParseInt(*val.N, 10, 64)
	if err != nil {
		return time.Time{}, fmt.Errorf("parsing int64 from string %s: %v", key, err)
	}
	return time.Unix(intVal, 0), nil
}

// RFC3339TimeFromItem extracts a time.Time from a map of AttributeValues containing an RFC 3339 S field.
//
// See UnixTimeFromItem if you're using Unix time format. RFC 3339 time is easier for humans to read and more precise,
// while Unix time is easier to sort and is the only layout usable in DynamoDB TTL fields.
func RFC3339TimeFromItem(item map[string]*dynamodb.AttributeValue, key string) (time.Time, error) {
	val, ok := item[key]
	if !ok || val.S == nil {
		return time.Time{}, fmt.Errorf("time key %s not found in item S", key)
	}
	t, err := time.Parse(*val.S, time.RFC3339)
	if err != nil {
		return time.Time{}, errx.Wrap(err, fmt.Sprintf("can't parse time from item value %s", key))
	}
	return t, nil
}
