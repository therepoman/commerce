export SHELL := /bin/bash
export PATH := $(PWD)/bin:$(PWD):$(PATH)
export GOBIN := $(PWD)/bin

# Default to not using any 3P proxies
export GOPROXY := direct
export GOPRIVATE := *

default: build lint check-gen test

ci:
	./ci.sh
.PHONY: ci

# Verify dependencies and test code building
build:
	go mod verify
	go build -mod=readonly ./...
.PHONY: build

# Run lints
lint: install-tools
	revive -config revive.toml ./...
	golangci-lint run --config .golangci.yml
.PHONY: lint

# Install lint tools into GOBIN
install-tools:
	@mkdir -p bin
	go install github.com/maxbrunsfeld/counterfeiter/v6
	go install github.com/mgechev/revive
	go install github.com/golangci/golangci-lint/cmd/golangci-lint
.PHONY: install-tools

# Run go test target for development
test:
	go test -race ./...
.PHONY: test

# Run integration tests
integration_test:
	go test -count=1 -tags integration -run "Integration" -race ./...
.PHONY: integration_test

# Run code generation
gen:
	go generate ./...
.PHONY: gen

# Check that code-generation does not produce any differences
check-gen: gen
	@((git diff --quiet HEAD -- *.gen.go **/*.gen.go) || \
	(echo "Generated code difference detected! Please run 'make gen' and commit the changes"; false))
.PHONY: check-gen

bench:
	go test -bench . -run=$$^ -benchmem ./bench
.PHONY: bench

# Run this one time to add lint dependencies to go.mod
add-lint-dependencies:
	go get github.com/mgechev/revive@v1.0.2
	go get github.com/golangci/golangci-lint@v1.27.0
.PHONY: add-lint-dependencies
