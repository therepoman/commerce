package rediczar

import (
	"context"
	"time"
)

// CommandsCacheMigrator helps migrate cache data from the Source (Commands) to Destination. Cache commands are dual-written to both the Source and Destination.
// The redis commands with expirations (in commands.gen.go) are considered cache commands
//
// Writes to the destination are synchronous. Write errors are not bubbled up, but can be optionally logged if a Logger is provided
type CommandsCacheMigrator struct {
	// Commands is the primary client, and it must be non-nil
	Commands

	// Destination is a secondary target for dual-writes, and it must be non-nil. Errors writing to Destination are not returned; errors are logged to the optional Logger
	Destination Commands

	// Enabled returns whether to enable writing to Destination. If nil, then migration is disabled
	Enabled func() bool

	// Logger is optional to log errors writing to Destination
	Logger Logger
}

// Del executes the Del command on the source client or source and destination clients
func (c *CommandsCacheMigrator) Del(ctx context.Context, keys ...string) (int64, error) {
	if !c.enabled() {
		return c.Commands.Del(ctx, keys...)
	}

	res1, err1 := c.Commands.Del(ctx, keys...)
	if err1 != nil {
		return 0, err1
	}

	_, err2 := c.Destination.Del(ctx, keys...)
	if err2 != nil {
		c.log("err", err2, "calling Del on Destination redis Commands client")
	}

	return res1, nil
}

// PExpire executes the PExpire command on the source client or source and destination clients
func (c *CommandsCacheMigrator) PExpire(ctx context.Context, key string, expiration time.Duration) (bool, error) {
	if !c.enabled() {
		return c.Commands.PExpire(ctx, key, expiration)
	}

	res1, err1 := c.Commands.PExpire(ctx, key, expiration)
	if err1 != nil {
		return false, err1
	}

	_, err2 := c.Destination.PExpire(ctx, key, expiration)
	if err2 != nil {
		c.log("err", err2, "calling PExpire on Destination redis Commands client")
	}

	return res1, nil
}

// Expire executes the Expire command on the source client or source and destination clients
func (c *CommandsCacheMigrator) Expire(ctx context.Context, key string, expiration time.Duration) (bool, error) {
	if !c.enabled() {
		return c.Commands.Expire(ctx, key, expiration)
	}

	res1, err1 := c.Commands.Expire(ctx, key, expiration)
	if err1 != nil {
		return false, err1
	}

	_, err2 := c.Destination.Expire(ctx, key, expiration)
	if err2 != nil {
		c.log("err", err2, "calling Expire on Destination redis Commands client")
	}

	return res1, nil
}

// Set executes the Set command on the source client or source and destination clients
func (c *CommandsCacheMigrator) Set(ctx context.Context, key string, value interface{}, expiration time.Duration) (string, error) {
	if !c.enabled() {
		return c.Commands.Set(ctx, key, value, expiration)
	}

	res1, err1 := c.Commands.Set(ctx, key, value, expiration)
	if err1 != nil {
		return "", err1
	}

	_, err2 := c.Destination.Set(ctx, key, value, expiration)
	if err2 != nil {
		c.log("err", err2, "calling Set on Destination redis Commands client")
	}

	return res1, nil
}

// SetNX executes the SetNX command on the source client or source and destination clients
func (c *CommandsCacheMigrator) SetNX(ctx context.Context, key string, value interface{}, expiration time.Duration) (bool, error) {
	if !c.enabled() {
		return c.Commands.SetNX(ctx, key, value, expiration)
	}

	res1, err1 := c.Commands.SetNX(ctx, key, value, expiration)
	if err1 != nil {
		return false, err1
	}

	_, err2 := c.Destination.SetNX(ctx, key, value, expiration)
	if err2 != nil {
		c.log("err", err2, "calling SetNX on Destination redis Commands client")
	}

	return res1, nil
}

// SetXX executes the SetXX command on the source client or source and destination clients
func (c *CommandsCacheMigrator) SetXX(ctx context.Context, key string, value interface{}, expiration time.Duration) (bool, error) {
	if !c.enabled() {
		return c.Commands.SetXX(ctx, key, value, expiration)
	}

	res1, err1 := c.Commands.SetXX(ctx, key, value, expiration)
	if err1 != nil {
		return false, err1
	}

	_, err2 := c.Destination.SetXX(ctx, key, value, expiration)
	if err2 != nil {
		c.log("err", err2, "calling SetXX on Destination redis Commands client")
	}

	return res1, nil
}

// HMSet executes the HMSet command on the source client or source and destination clients
func (c *CommandsCacheMigrator) HMSet(ctx context.Context, key string, fields map[string]interface{}) (string, error) {
	if !c.enabled() {
		return c.Commands.HMSet(ctx, key, fields)
	}

	res1, err1 := c.Commands.HMSet(ctx, key, fields)
	if err1 != nil {
		return "", err1
	}

	_, err2 := c.Destination.HMSet(ctx, key, fields)
	if err2 != nil {
		c.log("err", err2, "calling HMSet on Destination redis Commands client")
	}

	return res1, nil
}

// HSet executes the HSet command on the source client or source and destination clients
func (c *CommandsCacheMigrator) HSet(ctx context.Context, key string, field string, value interface{}) (bool, error) {
	if !c.enabled() {
		return c.Commands.HSet(ctx, key, field, value)
	}

	res1, err1 := c.Commands.HSet(ctx, key, field, value)
	if err1 != nil {
		return false, err1
	}

	_, err2 := c.Destination.HSet(ctx, key, field, value)
	if err2 != nil {
		c.log("err", err2, "calling HSet on Destination redis Commands client")
	}

	return res1, nil
}

// HSetNX executes the HSetNX command on the source client or source and destination clients
func (c *CommandsCacheMigrator) HSetNX(ctx context.Context, key string, field string, value interface{}) (bool, error) {
	if !c.enabled() {
		return c.Commands.HSetNX(ctx, key, field, value)
	}

	res1, err1 := c.Commands.HSetNX(ctx, key, field, value)
	if err1 != nil {
		return false, err1
	}

	_, err2 := c.Destination.HSetNX(ctx, key, field, value)
	if err2 != nil {
		c.log("err", err2, "calling HSetNX on Destination redis Commands client")
	}

	return res1, nil
}

// HDel executes the HDel command on the source client or source and destination clients
func (c *CommandsCacheMigrator) HDel(ctx context.Context, key string, fields ...string) (int64, error) {
	if !c.enabled() {
		return c.Commands.HDel(ctx, key, fields...)
	}

	res1, err1 := c.Commands.HDel(ctx, key, fields...)
	if err1 != nil {
		return 0, err1
	}

	_, err2 := c.Destination.HDel(ctx, key, fields...)
	if err2 != nil {
		c.log("err", err2, "calling HDel on Destination redis Commands client")
	}

	return res1, nil
}

func (c *CommandsCacheMigrator) enabled() bool {
	if c.Enabled == nil {
		return false
	}

	return c.Enabled()
}

func (c *CommandsCacheMigrator) log(vals ...interface{}) {
	if c.Logger != nil {
		c.Logger.Log(vals...)
	}
}

var _ Commands = (*CommandsCacheMigrator)(nil)

// ThickClientCacheMigrator helps migrate cache data from the Source (ThickClient) to Destination. Each prebuilt cache command is dual-written to both the Source and Destination.
// The prebuilt cache commands come from client_pipeline.go
//
// Writes to the destination are synchronous. Write errors are not bubbled up, but can be optionally logged if a Logger is provided
type ThickClientCacheMigrator struct {
	// ThickClient is the primary client, and it must be non-nil
	ThickClient

	// Destination is a secondary target for dual-writes, and it must be non-nil. Errors writing to Destination are not returned; errors are logged to the optional Logger
	Destination ThickClient

	// Enabled returns whether to enable writing to Destination. If nil, then migration is disabled
	Enabled func() bool

	// Logger is optional to log errors writing to Destination
	Logger Logger
}

// PipelinedDel executes the PipelinedDel command on the source client or source and destination clients
func (c *ThickClientCacheMigrator) PipelinedDel(ctx context.Context, keys ...string) (int64, error) {
	if !c.enabled() {
		return c.ThickClient.PipelinedDel(ctx, keys...)
	}

	res1, err1 := c.ThickClient.PipelinedDel(ctx, keys...)
	if err1 != nil {
		return 0, err1
	}

	_, err2 := c.Destination.PipelinedDel(ctx, keys...)
	if err2 != nil {
		c.log("err", err2, "calling PipelinedDel on Destination redis ThickClient")
	}

	return res1, nil
}

// MSetNXWithTTL executes the MSetNXWithTTL command on the source client or source and destination clients
func (c *ThickClientCacheMigrator) MSetNXWithTTL(ctx context.Context, ttl time.Duration, pairs ...interface{}) ([]bool, error) {
	if !c.enabled() {
		return c.ThickClient.MSetNXWithTTL(ctx, ttl, pairs...)
	}

	res1, err1 := c.ThickClient.MSetNXWithTTL(ctx, ttl, pairs...)
	if err1 != nil {
		return nil, err1
	}

	_, err2 := c.Destination.MSetNXWithTTL(ctx, ttl, pairs...)
	if err2 != nil {
		c.log("err", err2, "calling MSetNXWithTTL on Destination redis ThickClient")
	}

	return res1, nil
}

// MSetWithTTL executes the MSetWithTTL command on the source client or source and destination clients
func (c *ThickClientCacheMigrator) MSetWithTTL(ctx context.Context, ttl time.Duration, pairs ...interface{}) error {
	if !c.enabled() {
		return c.ThickClient.MSetWithTTL(ctx, ttl, pairs...)
	}

	err1 := c.ThickClient.MSetWithTTL(ctx, ttl, pairs...)
	if err1 != nil {
		return err1
	}

	err2 := c.Destination.MSetWithTTL(ctx, ttl, pairs...)
	if err2 != nil {
		c.log("err", err2, "calling MSetWithTTL on Destination redis ThickClient")
	}

	return nil
}

func (c *ThickClientCacheMigrator) enabled() bool {
	if c.Enabled == nil {
		return false
	}

	return c.Enabled()
}

func (c *ThickClientCacheMigrator) log(vals ...interface{}) {
	if c.Logger != nil {
		c.Logger.Log(vals...)
	}
}

var _ ThickClient = (*ThickClientCacheMigrator)(nil)
