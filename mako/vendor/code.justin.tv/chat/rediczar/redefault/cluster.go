package redefault

import (
	"context"
	"net"
	"time"

	"github.com/go-redis/redis/v7"
)

const (
	// MaxRedirects sets how many times the cluster client follows redirects or does retries. Reducing this number
	// prevents the client from generating too much load on retries. Error rates were low when testing node failover with
	// this setting. 1 is the default because it allows at least one MOVED or ASKED to be followed
	//
	// The go-redis library defaults to 8, which is quite high and has caused redis clusters to go into a death spiral from excessive retries
	defaultMaxRedirects = 1
)

// ClusterOpts specifies options for the cluster client
type ClusterOpts struct {
	// ReadOnly will read from replicas. Leave this as false for strong read-after-write consistency. Set this to true if you want
	// to spread load across read replicas.
	ReadOnly bool
	// Dialer specifies how new network connections will be created. Defaults to a standard net.Dialer with a 1s timeout and a 30s keep alive
	Dialer func(ctx context.Context, network, addr string) (net.Conn, error)
	// PoolSize determines the maximum number of connections to hold per redis node. Defaults to 30.
	PoolSize int
	// MinIdleConns determines the minimum number of idle connections to maintain per redis node. Defaults to 0.
	MinIdleConns int
	// MaxRedirects specifies how many redirects to follow on MOVED/ASKED. Defaults to 1.
	MaxRedirects int
	// ReadTimeout defaults to 1s. This is how long the client will wait to read a response from a redis node.
	ReadTimeout time.Duration
	// WriteTimeout defaults to 500ms. This is how long the client will wait to write a request to a redis node.
	WriteTimeout time.Duration
}

// fillDefaults with settings load tested in https://git.xarth.tv/chat/redismembench
// The load test generates 80k RPS (broken down into 65K GET RPS and 15K SetNX RPS) using 30 processes; each process generates ~2.6k rps.
func (c *ClusterOpts) fillDefaults() {
	if c.PoolSize <= 0 {
		c.PoolSize = defaultPoolSize
	}

	if c.MaxRedirects <= 0 {
		c.MaxRedirects = defaultMaxRedirects
	}

	if c.ReadTimeout <= 0 {
		c.ReadTimeout = defaultReadTimeout
	}

	if c.WriteTimeout <= 0 {
		c.WriteTimeout = defaultWriteTimeout
	}

	if c.Dialer == nil {
		d := net.Dialer{
			Timeout:   defaultDialerTimeout,
			KeepAlive: defaultKeepAlive,
		}
		c.Dialer = d.DialContext
	}
}

// NewClusterClient creates a new cluster client with a rate-limiting dialer, a fixed pool size, and reasonable defaults.
// opts is optional and can be left as nil. The rate-limiting will allow 1 connection per second, and a burst for first-time
// initialization of the cluster client. This function is intended to handle boiler plate, but can be copy-pasted if full
// customization is desired.
func NewClusterClient(addr string, opts *ClusterOpts) *redis.ClusterClient {
	if opts == nil {
		opts = &ClusterOpts{}
	}

	// Fill defaults
	opts.fillDefaults()

	// Create the cluster client
	client := redis.NewClusterClient(&redis.ClusterOptions{
		Addrs:        []string{addr},
		MaxRedirects: opts.MaxRedirects,
		ReadOnly:     opts.ReadOnly,
		Dialer:       dialerWithRateLimiting(opts.PoolSize, opts.Dialer),
		// The Read/Write timeouts work well when the cluster is under memory pressure to minimize errors and new connections.
		ReadTimeout:        opts.ReadTimeout,
		WriteTimeout:       opts.WriteTimeout,
		MinIdleConns:       opts.MinIdleConns,
		PoolSize:           opts.PoolSize,
		IdleTimeout:        -1,
		IdleCheckFrequency: -1,
	})

	return client
}
