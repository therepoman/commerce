package workerqueue

import (
	"errors"

	"github.com/aws/aws-sdk-go/aws/awserr"
)

var (
	// ErrNoMessageInQueue indicates no job is in the queue for a worker to handle.
	ErrNoMessageInQueue = errors.New("no message in queue")

	// ErrMaxVisibilityTimeoutTooLow indicates the max duration a task can be hidden
	// is lower than the default visibility timeout configured on the queue.
	ErrMaxVisibilityTimeoutTooLow = errors.New("MaxVisibilityTimeout is less than VisibilityTimeout")
)

const (
	invalidParameterValueCode = "InvalidParameterValue"
	requestErrorCode          = "RequestError"
	internalErrorCode         = "InternalError"
)

func isInvalidParameterValueError(err error) bool {
	awsErr, ok := err.(awserr.Error)
	return ok && awsErr.Code() == invalidParameterValueCode
}

func isTransientError(err error) bool {
	awsErr, ok := err.(awserr.Error)
	if ok {
		code := awsErr.Code()
		return code == requestErrorCode || code == internalErrorCode
	}

	return false
}
