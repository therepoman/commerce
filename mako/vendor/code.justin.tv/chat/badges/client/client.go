package badges

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"strconv"
	"time"

	"context"

	"code.justin.tv/chat/badges/app/models"
	"code.justin.tv/foundation/twitchclient"
)

const (
	defaultStatSampleRate = 0.1
	defaultTimingXactName = "badges"
)

// ClientConfig wraps wraps twitchclient.ClientConf so you can set custom timeout values
type ClientConfig struct {
	// HTTPConfig is the default http configuration
	HTTPConfig twitchclient.ClientConf

	// CtxTimeout is how long the client should wait for a http call before timing out
	CtxTimeout time.Duration
}

// Client is an interface for hitting Badges endpoints
type Client interface {
	// AvailableGlobalBadges gets the badges that are available to a user in the global scope
	AvailableGlobalBadges(ctx context.Context, userID string, reqOpts *twitchclient.ReqOpts) (*models.GetAvailableBadgesResponse, error)
	// DeselectGlobalBadge unselects a user's global badge preference. They will have
	// no badge selected globally after calling
	DeselectGlobalBadge(ctx context.Context, userID string, reqOpts *twitchclient.ReqOpts) error
	// SelectGlobalBadge selects a badge for a user globally.
	// Returns false if the user does not have permissions to set the badge.
	SelectGlobalBadge(ctx context.Context, userID string, params models.SelectBadgeParams, reqOpts *twitchclient.ReqOpts) (bool, error)
	// SelectTurboGlobalBadge selects the "turbo" badge for a user globally.
	// Returns false if the user does not have turbo
	SelectTurboGlobalBadge(ctx context.Context, userID string, reqOpts *twitchclient.ReqOpts) (bool, error)
	// AvailableChannelBadges gets the badges that are available to a user in the given channel scope
	AvailableChannelBadges(ctx context.Context, userID, channelID string, reqOpts *twitchclient.ReqOpts) (*models.GetAvailableBadgesResponse, error)
	// DeselectChannelBadge unselects a user's badge on a specific channel. They
	// will have no badge selected on that channel after calling.
	DeselectChannelBadge(ctx context.Context, userID, channelID string, reqOpts *twitchclient.ReqOpts) error
	// SelectChannelBadge selects a badge for a user globally.
	// Returns false if the user does not have permissions to set the badge.
	SelectChannelBadge(ctx context.Context, userID, channelID string, params models.SelectBadgeParams, reqOpts *twitchclient.ReqOpts) (bool, error)
	// GetAvailableBitsBadgeTier gets the bits badge that is available to a user in the given channel scope
	GetAvailableBitsBadge(ctx context.Context, userID, channelID string, reqOpts *twitchclient.ReqOpts) (*models.GetAvailableBitsBadgeResponse, error)
	// SelectBitsBadge selects the "bits" badge for a user on a channel, optionally allows to select a different version
	// Returns false if the user does not have a bits badge or a bits badge with this version available on the channel
	SelectBitsBadge(ctx context.Context, userID, channelID string, badgeSetVersion *string, reqOpts *twitchclient.ReqOpts) (bool, error)
	// RevokeBitsBadge removes the "bits" badge for a user on a channel
	// Returns error if the operation was not successful
	RevokeBitsBadge(ctx context.Context, userID, channelID string, reqOpts *twitchclient.ReqOpts) error
	// GrantBitsBadge entitles a user to the specified "bits" badge in the specified channel
	// Returns error if the operation was not successful
	GrantBitsBadge(ctx context.Context, userID, channelID string, params models.GrantBadgeParams, reqOpts *twitchclient.ReqOpts) error
	// RevokeBitsLeaderBadge removes the "bits-leader" badge for a user on a channel
	// Returns error if the operation was not successful
	RevokeBitsLeaderBadge(ctx context.Context, userID, channelID string, reqOpts *twitchclient.ReqOpts) error
	// GrantBitsBadge entitles a user to the specified "bits-leader" badge in the specified channel
	// Returns error if the operation was not successful
	GrantBitsLeaderBadge(ctx context.Context, userID, channelID string, params models.GrantBadgeParams, reqOpts *twitchclient.ReqOpts) error
	// GrantBitsCharityBadge entitles a user to the specified "bits charity" badge globally
	// Returns error if the operation was not successful
	GrantBitsCharityBadge(ctx context.Context, userID string, params models.GrantBadgeParams, reqOpts *twitchclient.ReqOpts) error
	// RevokeBitsBadge removes the "clipchamp" badge for a user on a channel
	// Returns error if the operation was not successful
	RevokeClipChampBadge(ctx context.Context, userID, channelID string, reqOpts *twitchclient.ReqOpts) error
	// BulkGrantClipChampBadge entitles a batch of users the "clip champ" badge in the specified channel
	// Returns error if the operation was not successful
	BulkGrantClipChampBadge(ctx context.Context, params models.BulkGrantBadgeParams, reqOpts *twitchclient.ReqOpts) error
	// GetClipChampBadge gets the clip champ badge for a user on a specified channel.
	GetClipChampBadge(ctx context.Context, userID, channelID string, reqOpts *twitchclient.ReqOpts) (*models.GetClipChampBadgeResponse, error)
	// GetSelectedGlobalBadge returns the badge that the user has selected to display globally
	// Returns nil if no badge is selected
	GetSelectedGlobalBadge(ctx context.Context, userID string, reqOpts *twitchclient.ReqOpts) (*models.Badge, error)
	// GetSelectedChannelBadge returns the badge that the user has selected to display for this channel
	// Returns nil if no badge is selected
	GetSelectedChannelBadge(ctx context.Context, userID, channelID string, reqOpts *twitchclient.ReqOpts) (*models.Badge, error)
	// UserBadges returns a user's badges in whichever scope(s) are requested
	// It takes metadata in the request (params.Metadata) and uses it in lieu of
	// hitting other services, if possible.
	UserBadges(ctx context.Context, userID string, params models.UserBadgesRequestParams, reqOpts *twitchclient.ReqOpts) (*models.UserBadgesResponse, error)
	// GetSubscriberImages returns a channel's subscriber badge image URLs
	GetSubscriberImages(ctx context.Context, channelID string, reqOpts *twitchclient.ReqOpts) (*models.BadgeSet, error)
	// UploadSubscriberImages uploads a a channel's subscriber images for some set
	// of badge_set_versions to the Badges Service, and returns the generated URLs
	UploadSubscriberImages(ctx context.Context, channelID string, params models.UploadImagesParams, reqOpts *twitchclient.ReqOpts) (*models.UploadImagesResponse, error)
	// RemoveSubscriberImage unlinks a channel's subscriber badge of a specified
	// version from its current image
	RemoveSubscriberImage(ctx context.Context, channelID string, badgeSetVersion string, reqOpts *twitchclient.ReqOpts) error
	// UpdateUserSubscriberTier updates a user's sub tier/version on a channel
	UpdateUserSubscriberTier(ctx context.Context, userID, channelID string, badgeSetVersion string, reqOpts *twitchclient.ReqOpts) error
	// BulkUpdateUserSubscriberTier updates many user's sub tiers/versions on a
	// channel. It returns an array of the IDs that it successfully processed
	BulkUpdateUserSubscriberTier(ctx context.Context, channelID string, params models.BulkGrantBadgeParams, reqOpts *twitchclient.ReqOpts) ([]int64, error)
	// DeleteUserSubscriberTier removes a user's sub tier/version on a channel.
	// They will have no sub badge after calling this.
	DeleteUserSubscriberTier(ctx context.Context, userID, channelID string, reqOpts *twitchclient.ReqOpts) error
	// BulkDeleteUserSubscriberTier removes many user's sub tiers/versions on a
	// channel. It returns an array of the IDs that it successfully processed
	BulkDeleteUserSubscriberTier(ctx context.Context, channelID string, params models.BulkRemoveBadgeParams, reqOpts *twitchclient.ReqOpts) ([]int64, error)
	// RevokeSubGifterBadge removes the "sub-gifter" badge for a user on a channel
	// Returns error if the operation was not successful
	RevokeSubGifterBadge(ctx context.Context, userID, channelID string, reqOpts *twitchclient.ReqOpts) error
	// GrantSubGifterBadge entitles a user to the specified "sub-gifter" badge in the specified channel
	// Returns the sub-gifter badge that was entitled prior to this operation (possibly nil) or an error if the operation was not successful
	GrantSubGifterBadge(ctx context.Context, userID, channelID string, params models.GrantBadgeParams, reqOpts *twitchclient.ReqOpts) (*models.GrantSubGifterBadgeResponse, error)
	// RevokeSubGiftLeaderBadge removes the "sub-gift-leader" badge for a user on a channel
	// Returns error if the operation was not successful
	RevokeSubGiftLeaderBadge(ctx context.Context, userID, channelID string, reqOpts *twitchclient.ReqOpts) error
	// GrantSubGiftLeaderBadge entitles a user to the specified "sub-gift-leader" badge in the specified channel
	// Returns an error if the operation was not successful
	GrantSubGiftLeaderBadge(ctx context.Context, userID, channelID string, params models.GrantBadgeParams, reqOpts *twitchclient.ReqOpts) error
	// GrantUserFuelBadge grants a fuel badge to a user, globally
	GrantUserFuelBadge(ctx context.Context, userID, badgeSetID string, params models.GrantBadgeParams, reqOpts *twitchclient.ReqOpts) error
	// DeleteUserFuelBadge revokes a fuel badge from a user, globally
	DeleteUserFuelBadge(ctx context.Context, userID, badgeSetID string, reqOpts *twitchclient.ReqOpts) error
	// GetBitsImages returns a channel's bits badge image URLs
	GetBitsImages(ctx context.Context, channelID string, reqOpts *twitchclient.ReqOpts) (*models.BadgeSet, error)
	// UploadBitsImages uploads a a channel's bits images for some set
	// of badge_set_versions to the Badges Service, and returns the generated URLs
	UploadBitsImages(ctx context.Context, channelID string, params models.UploadBitsImagesParams, reqOpts *twitchclient.ReqOpts) (*models.BitsUploadImagesResponse, error)
	// RemoveBitsImage unlinks a channel's bits badge of a specified
	// version from its current image
	RemoveBitsImage(ctx context.Context, channelID string, badgeSetVersion string, reqOpts *twitchclient.ReqOpts) error
	// RevokePartnerBadge removes the "partner" badge for a user globally
	// Returns error if the operation was not successful
	RevokePartnerBadge(ctx context.Context, userID string, reqOpts *twitchclient.ReqOpts) error
	// GrantPartnerBadge entitles a user to the "partner" badge globally
	// Returns error if the operation was not successful
	GrantPartnerBadge(ctx context.Context, userID string, reqOpts *twitchclient.ReqOpts) error
	// GlobalDisplayInfo returns badge image URLs and info for global badges
	GlobalDisplayInfo(ctx context.Context, languageCode *string, reqOpts *twitchclient.ReqOpts) (*models.GetBadgeDisplayResponse, error)
	// ChannelDisplayInfo returns badge image URLs and info for a channel's badges
	ChannelDisplayInfo(ctx context.Context, channelID string, languageCode *string, skipCache bool, reqOpts *twitchclient.ReqOpts) (*models.GetBadgeDisplayResponse, error)
	// BulkGetDisplayInfo returns badge image URLs and info for a set of badges.
	BulkGetDisplayInfo(ctx context.Context, ids []models.BadgeID, languageCode string, fallbackToGlobalScope bool, reqOpts *twitchclient.ReqOpts) ([]*models.DisplayInfo, error)
	GrantPledgeBadge(ctx context.Context, userID, sharedSecret string, reqOpts *twitchclient.ReqOpts) error
	RevokePledgeBadge(ctx context.Context, userID, sharedSecret string, reqOpts *twitchclient.ReqOpts) error
	// GrantUserGlobalOWLBadge grants an OWL badge to a user, globally
	GrantUserGlobalOWLBadge(ctx context.Context, userID, badgeSetID string, params models.GrantBadgeParams, reqOpts *twitchclient.ReqOpts) error
	// DeleteUserGlobalOWLBadge revokes an OWL badge from a user, globally
	DeleteUserGlobalOWLBadge(ctx context.Context, userID, badgeSetID string, reqOpts *twitchclient.ReqOpts) error
	// GrantUserChannelOWLBadge grants an OWL badge to a user in a channel
	GrantUserChannelOWLBadge(ctx context.Context, userID, channelID, badgeSetID string, params models.GrantBadgeParams, reqOpts *twitchclient.ReqOpts) error
	// DeleteUserChannelOWLBadge revokes an OWL badge from a user in a channel
	DeleteUserChannelOWLBadge(ctx context.Context, userID, channelID, badgeSetID string, reqOpts *twitchclient.ReqOpts) error
}

// NewClient creates an http client to call Badges endpoints.
func NewClient(conf twitchclient.ClientConf) (Client, error) {
	if conf.TimingXactName == "" {
		conf.TimingXactName = defaultTimingXactName
	}
	twitchClient, err := twitchclient.NewClient(conf)
	if err != nil {
		return nil, err
	}

	return &clientImpl{http: twitchClient}, nil
}

type clientImpl struct {
	http twitchclient.Client
}

func (c *clientImpl) AvailableGlobalBadges(ctx context.Context, userID string, reqOpts *twitchclient.ReqOpts) (*models.GetAvailableBadgesResponse, error) {
	url := url.URL{Path: fmt.Sprintf("/v1/badges/users/%s/global/available", userID)}
	req, err := c.http.NewRequest("GET", url.String(), nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.badges.available_global_badges",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.http.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}
	defer close(resp.Body)

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected response code %d during call to AvailableGlobalBadges", resp.StatusCode)
	}

	var decoded models.GetAvailableBadgesResponse
	if err = json.NewDecoder(resp.Body).Decode(&decoded); err != nil {
		return nil, err
	}

	return &decoded, nil
}

func (c *clientImpl) DeselectGlobalBadge(ctx context.Context, userID string, reqOpts *twitchclient.ReqOpts) error {
	url := url.URL{Path: fmt.Sprintf("/v1/badges/users/%s/global/selected", userID)}
	req, err := c.http.NewRequest("DELETE", url.String(), nil)
	if err != nil {
		return err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.badges.deselect_global_badge",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.http.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}
	defer close(resp.Body)

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("unexpected response code %d during call to DeselectGlobalBadge", resp.StatusCode)
	}

	return nil
}

func (c *clientImpl) SelectTurboGlobalBadge(ctx context.Context, userID string, reqOpts *twitchclient.ReqOpts) (bool, error) {
	params := models.SelectBadgeParams{BadgeSetID: "turbo"}
	return c.SelectGlobalBadge(ctx, userID, params, reqOpts)
}

func (c *clientImpl) SelectGlobalBadge(ctx context.Context, userID string, params models.SelectBadgeParams, reqOpts *twitchclient.ReqOpts) (bool, error) {
	url := url.URL{Path: fmt.Sprintf("/v1/badges/users/%s/global/selected", userID)}

	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return false, err
	}
	req, err := c.http.NewRequest("POST", url.String(), bytes.NewReader(bodyBytes))
	if err != nil {
		return false, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.badges.select_global_badge",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.http.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return false, err
	}
	defer close(resp.Body)

	if resp.StatusCode == http.StatusForbidden {
		// User does not have global badge available
		return false, nil
	} else if resp.StatusCode != http.StatusOK {
		return false, fmt.Errorf("unexpected response code %d during call to SelectGlobalBadge", resp.StatusCode)
	}

	return true, nil
}

func (c *clientImpl) AvailableChannelBadges(ctx context.Context, userID, channelID string, reqOpts *twitchclient.ReqOpts) (*models.GetAvailableBadgesResponse, error) {
	url := url.URL{Path: fmt.Sprintf("/v1/badges/users/%s/channels/%s/available", userID, channelID)}
	req, err := c.http.NewRequest("GET", url.String(), nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.badges.available_channel_badges",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.http.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}
	defer close(resp.Body)

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected response code %d during call to AvailableChannelBadges", resp.StatusCode)
	}

	var decoded models.GetAvailableBadgesResponse
	if err := json.NewDecoder(resp.Body).Decode(&decoded); err != nil {
		return nil, err
	}

	return &decoded, nil
}

func (c *clientImpl) GetAvailableBitsBadge(ctx context.Context, userID, channelID string, reqOpts *twitchclient.ReqOpts) (*models.GetAvailableBitsBadgeResponse, error) {
	url := url.URL{Path: fmt.Sprintf("/v1/badges/users/%s/channels/%s/bits", userID, channelID)}
	req, err := c.http.NewRequest("GET", url.String(), nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.badges.get_available_bits_badge",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.http.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}
	defer close(resp.Body)

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected response code %d during call to GetAvailableBitsBadge", resp.StatusCode)
	}

	var decoded models.GetAvailableBitsBadgeResponse
	if err := json.NewDecoder(resp.Body).Decode(&decoded); err != nil {
		return nil, err
	}

	return &decoded, nil
}

func (c *clientImpl) DeselectChannelBadge(ctx context.Context, userID, channelID string, reqOpts *twitchclient.ReqOpts) error {
	url := url.URL{Path: fmt.Sprintf("/v1/badges/users/%s/channels/%s/selected", userID, channelID)}
	req, err := c.http.NewRequest("DELETE", url.String(), nil)
	if err != nil {
		return err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.badges.deselect_channel_badge",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.http.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}
	defer close(resp.Body)

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("unexpected response code %d during call to DeselectChannelBadge", resp.StatusCode)
	}

	return nil
}

func (c *clientImpl) SelectBitsBadge(ctx context.Context, userID, channelID string, bitsBadgeversion *string, reqOpts *twitchclient.ReqOpts) (bool, error) {
	url := url.URL{Path: fmt.Sprintf("/v1/badges/users/%s/channels/%s/bits_select", userID, channelID)}
	bodyBytes, err := json.Marshal(models.SelectBadgeParams{BadgeSetID: models.BadgeSetBits, BadgeSetVersion: bitsBadgeversion})
	if err != nil {
		return false, err
	}

	req, err := c.http.NewRequest("POST", url.String(), bytes.NewReader(bodyBytes))
	if err != nil {
		return false, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.badges.select_bits_badge",
		StatSampleRate: defaultStatSampleRate,
	})

	response, err := c.http.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return false, err
	}

	if http.StatusOK != response.StatusCode {
		return false, fmt.Errorf("could not select bits badge status code was %d", response.StatusCode)
	}

	return true, nil
}

func (c *clientImpl) RevokeBitsBadge(ctx context.Context, userID, channelID string, reqOpts *twitchclient.ReqOpts) error {
	url := url.URL{Path: fmt.Sprintf("/v1/badges/users/%s/channels/%s/bits", userID, channelID)}
	req, err := c.http.NewRequest("DELETE", url.String(), nil)
	if err != nil {
		return err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.badges.revoke_bits_badge",
		StatSampleRate: defaultStatSampleRate,
	})

	response, err := c.http.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}

	if http.StatusOK != response.StatusCode {
		return fmt.Errorf("could not revoke chat badge status code was %d", response.StatusCode)
	}

	return nil
}

func (c *clientImpl) GrantBitsBadge(ctx context.Context, userID, channelID string, params models.GrantBadgeParams, reqOpts *twitchclient.ReqOpts) error {
	url := url.URL{Path: fmt.Sprintf("/v1/badges/users/%s/channels/%s/bits", userID, channelID)}
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return err
	}

	req, err := c.http.NewRequest("POST", url.String(), bytes.NewReader(bodyBytes))
	if err != nil {
		return err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.badges.grant_bits_badge",
		StatSampleRate: defaultStatSampleRate,
	})

	response, err := c.http.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}

	if http.StatusOK != response.StatusCode {
		return fmt.Errorf("could not grant bits badge status code was %d", response.StatusCode)
	}

	return nil
}

func (c *clientImpl) RevokeBitsLeaderBadge(ctx context.Context, userID, channelID string, reqOpts *twitchclient.ReqOpts) error {
	url := url.URL{Path: fmt.Sprintf("/v1/badges/users/%s/channels/%s/bits_leader", userID, channelID)}
	req, err := c.http.NewRequest("DELETE", url.String(), nil)
	if err != nil {
		return err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.badges.revoke_bits_leader_badge",
		StatSampleRate: defaultStatSampleRate,
	})

	response, err := c.http.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}

	if http.StatusOK != response.StatusCode {
		return fmt.Errorf("could not revoke chat badge status code was %d", response.StatusCode)
	}

	return nil
}

func (c *clientImpl) GrantBitsCharityBadge(ctx context.Context, userID string, params models.GrantBadgeParams, reqOpts *twitchclient.ReqOpts) error {
	url := url.URL{Path: fmt.Sprintf("/v1/badges/users/%s/global/bits_charity", userID)}
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return err
	}

	req, err := c.http.NewRequest("POST", url.String(), bytes.NewReader(bodyBytes))
	if err != nil {
		return err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.badges.grant_bits_charity_badge",
		StatSampleRate: defaultStatSampleRate,
	})

	response, err := c.http.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}

	if http.StatusOK != response.StatusCode {
		return fmt.Errorf("could not grant chat badge status code was %d", response.StatusCode)
	}

	return nil
}

func (c *clientImpl) GrantBitsLeaderBadge(ctx context.Context, userID, channelID string, params models.GrantBadgeParams, reqOpts *twitchclient.ReqOpts) error {
	url := url.URL{Path: fmt.Sprintf("/v1/badges/users/%s/channels/%s/bits_leader", userID, channelID)}
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return err
	}

	req, err := c.http.NewRequest("POST", url.String(), bytes.NewReader(bodyBytes))
	if err != nil {
		return err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.badges.grant_bits_leader_badge",
		StatSampleRate: defaultStatSampleRate,
	})

	response, err := c.http.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}

	if http.StatusOK != response.StatusCode {
		return fmt.Errorf("could not grant chat badge status code was %d", response.StatusCode)
	}

	return nil
}

func (c *clientImpl) RevokeClipChampBadge(ctx context.Context, userID, channelID string, reqOpts *twitchclient.ReqOpts) error {
	url := url.URL{Path: fmt.Sprintf("/v1/badges/users/%s/channels/%s/clipchamp", userID, channelID)}
	req, err := c.http.NewRequest("DELETE", url.String(), nil)
	if err != nil {
		return err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.badges.revoke_clipchamp_badge",
		StatSampleRate: defaultStatSampleRate,
	})

	response, err := c.http.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}

	if http.StatusOK != response.StatusCode {
		return fmt.Errorf("could not revoke chat badge status code was %d", response.StatusCode)
	}

	return nil
}

func (c *clientImpl) BulkGrantClipChampBadge(ctx context.Context, params models.BulkGrantBadgeParams, reqOpts *twitchclient.ReqOpts) error {
	url := url.URL{Path: "/v1/badges/bulk_clipchamp"}
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return err
	}

	req, err := c.http.NewRequest("POST", url.String(), bytes.NewReader(bodyBytes))
	if err != nil {
		return err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.badges.bulk_grant_clipchamp_badge",
		StatSampleRate: defaultStatSampleRate,
	})

	response, err := c.http.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}

	if http.StatusOK != response.StatusCode {
		return fmt.Errorf("could not grant chat badge status code was %d", response.StatusCode)
	}

	return nil
}

func (c *clientImpl) GetClipChampBadge(ctx context.Context, userID, channelID string, reqOpts *twitchclient.ReqOpts) (*models.GetClipChampBadgeResponse, error) {
	url := url.URL{Path: fmt.Sprintf("/v1/badges/users/%s/channels/%s/clipchamp", userID, channelID)}
	req, err := c.http.NewRequest("GET", url.String(), nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.badges.get_clip_champ_badge",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.http.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}
	defer close(resp.Body)

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected response code %d during call to GetClipChampBadge", resp.StatusCode)
	}

	var decoded models.GetClipChampBadgeResponse
	if err := json.NewDecoder(resp.Body).Decode(&decoded); err != nil {
		return nil, err
	}

	return &decoded, nil
}

func (c *clientImpl) SelectChannelBadge(ctx context.Context, userID, channelID string, params models.SelectBadgeParams, reqOpts *twitchclient.ReqOpts) (bool, error) {
	url := url.URL{Path: fmt.Sprintf("/v1/badges/users/%s/channels/%s/selected", userID, channelID)}

	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return false, err
	}
	req, err := c.http.NewRequest("POST", url.String(), bytes.NewReader(bodyBytes))
	if err != nil {
		return false, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.badges.select_channel_badge",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.http.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return false, err
	}
	defer close(resp.Body)

	if resp.StatusCode == http.StatusForbidden {
		// User does not have the badge available
		return false, nil
	} else if resp.StatusCode != http.StatusOK {
		return false, fmt.Errorf("unexpected response code %d during call to SelectChannelBadge", resp.StatusCode)
	}

	return true, nil
}

func (c *clientImpl) GetSelectedGlobalBadge(ctx context.Context, userID string, reqOpts *twitchclient.ReqOpts) (*models.Badge, error) {
	params := models.UserBadgesRequestParams{
		GlobalSelected: true,
	}

	resp, err := c.UserBadges(ctx, userID, params, reqOpts)
	if err != nil {
		return nil, err
	}

	return resp.GlobalSelected, nil
}

func (c *clientImpl) GetSelectedChannelBadge(ctx context.Context, userID, channelID string, reqOpts *twitchclient.ReqOpts) (*models.Badge, error) {
	channelIDInt, err := strconv.ParseInt(channelID, 10, 64)
	if err != nil {
		return nil, fmt.Errorf("invalid channel id: %s", err)
	}

	params := models.UserBadgesRequestParams{
		ChannelSelected: &channelIDInt,
	}

	resp, err := c.UserBadges(ctx, userID, params, reqOpts)
	if err != nil {
		return nil, err
	}

	return resp.ChannelSelected, nil
}

func (c *clientImpl) UserBadges(ctx context.Context, userID string, params models.UserBadgesRequestParams, reqOpts *twitchclient.ReqOpts) (*models.UserBadgesResponse, error) {
	url := url.URL{Path: fmt.Sprintf("/v1/badges/users/%s", userID)}

	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return nil, err
	}
	req, err := c.http.NewRequest("POST", url.String(), bytes.NewReader(bodyBytes))
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.badges.user_badges",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.http.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}
	defer close(resp.Body)

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected response code %d during call to UserBadges", resp.StatusCode)
	}

	var decoded models.UserBadgesResponse
	if err := json.NewDecoder(resp.Body).Decode(&decoded); err != nil {
		return nil, err
	}
	return &decoded, nil
}

func (c *clientImpl) GetSubscriberImages(ctx context.Context, channelID string, reqOpts *twitchclient.ReqOpts) (*models.BadgeSet, error) {
	url := fmt.Sprintf("/v1/badges/images/channels/%s/subs", channelID)
	req, err := c.http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.badges.get_subscriber_images",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.http.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}
	defer close(resp.Body)

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected response code %d during call to GetSubscriberImages", resp.StatusCode)
	}

	var decoded models.BadgeSet
	if err := json.NewDecoder(resp.Body).Decode(&decoded); err != nil {
		return nil, err
	}
	return &decoded, nil
}

func (c *clientImpl) UploadSubscriberImages(ctx context.Context, channelID string, params models.UploadImagesParams, reqOpts *twitchclient.ReqOpts) (*models.UploadImagesResponse, error) {
	url := fmt.Sprintf("/v1/badges/images/channels/%s/subs", channelID)
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return nil, err
	}
	req, err := c.http.NewRequest("POST", url, bytes.NewReader(bodyBytes))
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.badges.upload_subscriber_images",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.http.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}
	defer close(resp.Body)

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected response code %d during call to UploadSubscriberImages", resp.StatusCode)
	}

	var decoded models.UploadImagesResponse
	if err := json.NewDecoder(resp.Body).Decode(&decoded); err != nil {
		return nil, err
	}
	return &decoded, nil
}

func (c *clientImpl) RemoveSubscriberImage(ctx context.Context, channelID string, badgeSetVersion string, reqOpts *twitchclient.ReqOpts) error {
	url := fmt.Sprintf("/v1/badges/images/channels/%s/subs/remove_image", channelID)
	bodyBytes, err := json.Marshal(models.RemoveImageParams{BadgeSetVersion: badgeSetVersion})
	if err != nil {
		return err
	}
	req, err := c.http.NewRequest("POST", url, bytes.NewReader(bodyBytes))
	if err != nil {
		return err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.badges.remove_subscriber_image",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.http.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}
	defer close(resp.Body)

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("unexpected response code %d during call to RemoveSubscriberImage", resp.StatusCode)
	}

	return nil
}

func (c *clientImpl) UpdateUserSubscriberTier(ctx context.Context, userID, channelID string, badgeSetVersion string, reqOpts *twitchclient.ReqOpts) error {
	url := fmt.Sprintf("/v1/badges/users/%s/channels/%s/subs", userID, channelID)
	bodyBytes, err := json.Marshal(models.GrantBadgeParams{BadgeSetVersion: badgeSetVersion})
	if err != nil {
		return err
	}
	req, err := c.http.NewRequest("POST", url, bytes.NewReader(bodyBytes))
	if err != nil {
		return err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.badges.update_user_subscriber_tier",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.http.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}
	defer close(resp.Body)

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("unexpected response code %d during call to UpdateUserSubscriberTier", resp.StatusCode)
	}

	return nil
}

func (c *clientImpl) BulkUpdateUserSubscriberTier(ctx context.Context, channelID string, params models.BulkGrantBadgeParams, reqOpts *twitchclient.ReqOpts) ([]int64, error) {
	url := fmt.Sprintf("/v1/badges/channels/%s/subs/bulk_update", channelID)
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return nil, err
	}
	req, err := c.http.NewRequest("POST", url, bytes.NewReader(bodyBytes))
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.badges.bulk_update_user_subscriber_tier",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.http.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}
	defer close(resp.Body)

	var decoded models.BulkRequestResponse
	if err = json.NewDecoder(resp.Body).Decode(&decoded); err != nil {
		return nil, err
	}

	return decoded.ProcessedIDs, nil
}

func (c *clientImpl) DeleteUserSubscriberTier(ctx context.Context, userID, channelID string, reqOpts *twitchclient.ReqOpts) error {
	url := fmt.Sprintf("/v1/badges/users/%s/channels/%s/subs", userID, channelID)
	req, err := c.http.NewRequest("DELETE", url, nil)
	if err != nil {
		return err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.badges.delete_user_subscriber_tier",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.http.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}
	defer close(resp.Body)

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("unexpected response code %d during call to DeleteUserSubscriberTier", resp.StatusCode)
	}

	return nil
}

func (c *clientImpl) BulkDeleteUserSubscriberTier(ctx context.Context, channelID string, params models.BulkRemoveBadgeParams, reqOpts *twitchclient.ReqOpts) ([]int64, error) {
	url := fmt.Sprintf("/v1/badges/channels/%s/subs/bulk_delete", channelID)
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return nil, err
	}
	req, err := c.http.NewRequest("POST", url, bytes.NewReader(bodyBytes))
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.badges.bulk_delete_user_subscriber_tier",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.http.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}
	defer close(resp.Body)

	var decoded models.BulkRequestResponse
	if err = json.NewDecoder(resp.Body).Decode(&decoded); err != nil {
		return nil, err
	}

	return decoded.ProcessedIDs, nil
}

func (c *clientImpl) RevokeSubGifterBadge(ctx context.Context, userID, channelID string, reqOpts *twitchclient.ReqOpts) error {
	url := url.URL{Path: fmt.Sprintf("/v1/badges/users/%s/channels/%s/sub_gifter", userID, channelID)}
	req, err := c.http.NewRequest("DELETE", url.String(), nil)
	if err != nil {
		return err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.badges.revoke_sub_gifter_badge",
		StatSampleRate: defaultStatSampleRate,
	})

	response, err := c.http.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}
	defer close(response.Body)

	if http.StatusOK != response.StatusCode {
		return fmt.Errorf("could not revoke chat badge status code was %d", response.StatusCode)
	}

	return nil
}

func (c *clientImpl) GrantSubGifterBadge(ctx context.Context, userID, channelID string, params models.GrantBadgeParams, reqOpts *twitchclient.ReqOpts) (*models.GrantSubGifterBadgeResponse, error) {
	url := url.URL{Path: fmt.Sprintf("/v1/badges/users/%s/channels/%s/sub_gifter", userID, channelID)}
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return nil, err
	}

	req, err := c.http.NewRequest("POST", url.String(), bytes.NewReader(bodyBytes))
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.badges.grant_sub_gifter_badge",
		StatSampleRate: defaultStatSampleRate,
	})

	response, err := c.http.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}
	defer close(response.Body)

	if http.StatusOK != response.StatusCode {
		return nil, fmt.Errorf("could not grant chat badge status code was %d", response.StatusCode)
	}

	var decoded models.GrantSubGifterBadgeResponse
	if err := json.NewDecoder(response.Body).Decode(&decoded); err != nil {
		return nil, err
	}
	return &decoded, nil
}

func (c *clientImpl) RevokeSubGiftLeaderBadge(ctx context.Context, userID, channelID string, reqOpts *twitchclient.ReqOpts) error {
	url := url.URL{Path: fmt.Sprintf("/v1/badges/users/%s/channels/%s/sub_gift_leader", userID, channelID)}
	req, err := c.http.NewRequest("DELETE", url.String(), nil)
	if err != nil {
		return err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.badges.revoke_sub_gift_leader_badge",
		StatSampleRate: defaultStatSampleRate,
	})

	response, err := c.http.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}
	defer close(response.Body)

	if http.StatusOK != response.StatusCode {
		return fmt.Errorf("could not revoke chat badge status code was %d", response.StatusCode)
	}

	return nil
}

func (c *clientImpl) GrantSubGiftLeaderBadge(ctx context.Context, userID, channelID string, params models.GrantBadgeParams, reqOpts *twitchclient.ReqOpts) error {
	url := url.URL{Path: fmt.Sprintf("/v1/badges/users/%s/channels/%s/sub_gift_leader", userID, channelID)}
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return err
	}

	req, err := c.http.NewRequest("POST", url.String(), bytes.NewReader(bodyBytes))
	if err != nil {
		return err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.badges.grant_sub_gift_leader_badge",
		StatSampleRate: defaultStatSampleRate,
	})

	response, err := c.http.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}
	defer close(response.Body)

	if http.StatusOK != response.StatusCode {
		return fmt.Errorf("could not grant chat badge status code was %d", response.StatusCode)
	}

	return nil
}

func (c *clientImpl) GrantUserFuelBadge(ctx context.Context, userID, badgeSetID string, params models.GrantBadgeParams, reqOpts *twitchclient.ReqOpts) error {
	url := fmt.Sprintf("/v1/badges/users/%s/global/fuel/%s", userID, badgeSetID)
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return err
	}
	req, err := c.http.NewRequest("POST", url, bytes.NewReader(bodyBytes))
	if err != nil {
		return err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.badges.grant_user_fuel_badge",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.http.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}
	defer close(resp.Body)

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("unexpected response code %d during call to GrantUserFuelBadge", resp.StatusCode)
	}

	return nil
}

func (c *clientImpl) DeleteUserFuelBadge(ctx context.Context, userID, badgeSetID string, reqOpts *twitchclient.ReqOpts) error {
	url := fmt.Sprintf("/v1/badges/users/%s/global/fuel/%s", userID, badgeSetID)
	req, err := c.http.NewRequest("DELETE", url, nil)
	if err != nil {
		return err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.badges.delete_user_fuel_badge",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.http.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}
	defer close(resp.Body)

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("unexpected response code %d during call to DeleteUserFuelBadge", resp.StatusCode)
	}

	return nil
}

func (c *clientImpl) GetBitsImages(ctx context.Context, channelID string, reqOpts *twitchclient.ReqOpts) (*models.BadgeSet, error) {
	url := fmt.Sprintf("/v1/badges/images/channels/%s/bits", channelID)
	req, err := c.http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.badges.get_bits_images",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.http.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}
	defer close(resp.Body)

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected response code %d during call to GetBitsImages", resp.StatusCode)
	}

	var decoded models.BadgeSet
	if err := json.NewDecoder(resp.Body).Decode(&decoded); err != nil {
		return nil, err
	}
	return &decoded, nil
}

func (c *clientImpl) UploadBitsImages(ctx context.Context, channelID string, params models.UploadBitsImagesParams, reqOpts *twitchclient.ReqOpts) (*models.BitsUploadImagesResponse, error) {
	url := fmt.Sprintf("/v1/badges/images/channels/%s/bits", channelID)
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return nil, err
	}
	req, err := c.http.NewRequest("POST", url, bytes.NewReader(bodyBytes))
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.badges.upload_bits_images",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.http.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}
	defer close(resp.Body)

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected response code %d during call to UploadBitsImages", resp.StatusCode)
	}

	var decoded models.BitsUploadImagesResponse
	if err := json.NewDecoder(resp.Body).Decode(&decoded); err != nil {
		return nil, err
	}
	return &decoded, nil
}

func (c *clientImpl) RemoveBitsImage(ctx context.Context, channelID string, badgeSetVersion string, reqOpts *twitchclient.ReqOpts) error {
	url := fmt.Sprintf("/v1/badges/images/channels/%s/bits/remove_image", channelID)
	bodyBytes, err := json.Marshal(models.RemoveImageParams{BadgeSetVersion: badgeSetVersion})
	if err != nil {
		return err
	}
	req, err := c.http.NewRequest("POST", url, bytes.NewReader(bodyBytes))
	if err != nil {
		return err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.badges.remove_bits_image",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.http.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}
	defer close(resp.Body)

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("unexpected response code %d during call to RemoveBitsImage", resp.StatusCode)
	}

	return nil
}

func (c *clientImpl) RevokePartnerBadge(ctx context.Context, userID string, reqOpts *twitchclient.ReqOpts) error {
	url := url.URL{Path: fmt.Sprintf("/v1/badges/users/%s/global/partner", userID)}
	req, err := c.http.NewRequest("DELETE", url.String(), nil)
	if err != nil {
		return err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.badges.revoke_partner_badge",
		StatSampleRate: defaultStatSampleRate,
	})

	response, err := c.http.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}

	if http.StatusOK != response.StatusCode {
		return fmt.Errorf("could not revoke chat badge status code was %d", response.StatusCode)
	}

	return nil
}

func (c *clientImpl) GrantPartnerBadge(ctx context.Context, userID string, reqOpts *twitchclient.ReqOpts) error {
	url := url.URL{Path: fmt.Sprintf("/v1/badges/users/%s/global/partner", userID)}
	bodyBytes, err := json.Marshal(models.GrantBadgeParams{})
	if err != nil {
		return err
	}

	req, err := c.http.NewRequest("POST", url.String(), bytes.NewReader(bodyBytes))
	if err != nil {
		return err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.badges.grant_partner_badge",
		StatSampleRate: defaultStatSampleRate,
	})

	response, err := c.http.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}

	if http.StatusOK != response.StatusCode {
		return fmt.Errorf("could not grant chat badge status code was %d", response.StatusCode)
	}

	return nil
}

func (c *clientImpl) GlobalDisplayInfo(ctx context.Context, languageCode *string, reqOpts *twitchclient.ReqOpts) (*models.GetBadgeDisplayResponse, error) {
	values := url.Values{}
	if languageCode != nil {
		values.Add("language", *languageCode)
	}

	path := fmt.Sprintf("/v1/badges/global/display?%s", values.Encode())
	req, err := c.http.NewRequest("GET", path, nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.badges.global_display",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.http.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}
	defer close(resp.Body)

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected response code %d during call to GlobalDisplayInfo", resp.StatusCode)
	}

	var decoded models.GetBadgeDisplayResponse
	if err := json.NewDecoder(resp.Body).Decode(&decoded); err != nil {
		return nil, err
	}

	return &decoded, nil
}

func (c *clientImpl) ChannelDisplayInfo(ctx context.Context, channelID string, languageCode *string, skipCache bool, reqOpts *twitchclient.ReqOpts) (*models.GetBadgeDisplayResponse, error) {
	values := url.Values{}

	values.Add("skip_cache", strconv.FormatBool(skipCache))
	if languageCode != nil {
		values.Add("language", *languageCode)
	}

	path := fmt.Sprintf("/v1/badges/channels/%s/display?%s", channelID, values.Encode())
	req, err := c.http.NewRequest("GET", path, nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.badges.channel_display",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.http.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}
	defer close(resp.Body)

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected response code %d during call to ChannelDisplayInfo", resp.StatusCode)
	}

	var decoded models.GetBadgeDisplayResponse
	if err := json.NewDecoder(resp.Body).Decode(&decoded); err != nil {
		return nil, err
	}

	return &decoded, nil
}

// BulkDisplay returns display information for the specified set of badges for a
// specific language for a single scope (i.e. global or within a particular channel).
//
// Looking up a badge by channel ID returns <nil> if a unique badge is not
// found for that channel ID. If FallbackToGlobalScope is true, the backend
// falls back to looking up the badge in the global scope (i.e. no channel ID).
func (c *clientImpl) BulkGetDisplayInfo(ctx context.Context, ids []models.BadgeID, languageCode string, fallbackToGlobalScope bool, reqOpts *twitchclient.ReqOpts) ([]*models.DisplayInfo, error) {
	values := url.Values{}
	values.Add("language", languageCode)
	values.Add("fallback_to_global_scope", strconv.FormatBool(fallbackToGlobalScope))

	params := models.BulkDisplayParams{IDs: ids}
	b, err := json.Marshal(params)
	if err != nil {
		return nil, err
	}

	path := "/v1/badges/bulk_display?" + values.Encode()
	body := bytes.NewReader(b)
	req, err := c.http.NewRequest("POST", path, body)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.badges.bulk_display",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.http.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}
	defer close(resp.Body)

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected response code %d during call to BatchDisplayInfo", resp.StatusCode)
	}

	var decoded models.BulkDisplayResponse
	if err := json.NewDecoder(resp.Body).Decode(&decoded); err != nil {
		return nil, err
	}

	return decoded.Badges, nil
}

func (c *clientImpl) GrantUserGlobalOWLBadge(ctx context.Context, userID, badgeSetID string, params models.GrantBadgeParams, reqOpts *twitchclient.ReqOpts) error {
	url := fmt.Sprintf("/v1/badges/users/%s/channels/0/owl/%s", userID, badgeSetID)
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return err
	}
	req, err := c.http.NewRequest("POST", url, bytes.NewReader(bodyBytes))
	if err != nil {
		return err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.badges.grant_user_owl_badge",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.http.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}
	defer close(resp.Body)

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("unexpected response code %d during call to GrantUserGlobalOWLBadge", resp.StatusCode)
	}

	return nil
}

func (c *clientImpl) DeleteUserGlobalOWLBadge(ctx context.Context, userID, badgeSetID string, reqOpts *twitchclient.ReqOpts) error {
	url := fmt.Sprintf("/v1/badges/users/%s/channels/0/owl/%s", userID, badgeSetID)
	req, err := c.http.NewRequest("DELETE", url, nil)
	if err != nil {
		return err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.badges.delete_user_owl_badge",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.http.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}
	defer close(resp.Body)

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("unexpected response code %d during call to DeleteUserGlobalOWLBadge", resp.StatusCode)
	}

	return nil
}

func (c *clientImpl) GrantUserChannelOWLBadge(ctx context.Context, userID, channelID, badgeSetID string, params models.GrantBadgeParams, reqOpts *twitchclient.ReqOpts) error {
	url := fmt.Sprintf("/v1/badges/users/%s/channels/%s/owl/%s", userID, channelID, badgeSetID)
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return err
	}
	req, err := c.http.NewRequest("POST", url, bytes.NewReader(bodyBytes))
	if err != nil {
		return err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.badges.grant_user_owl_badge",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.http.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}
	defer close(resp.Body)

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("unexpected response code %d during call to GrantUserChannelOWLBadge", resp.StatusCode)
	}

	return nil
}

func (c *clientImpl) DeleteUserChannelOWLBadge(ctx context.Context, userID, channelID, badgeSetID string, reqOpts *twitchclient.ReqOpts) error {
	url := fmt.Sprintf("/v1/badges/users/%s/channels/%s/owl/%s", userID, channelID, badgeSetID)
	req, err := c.http.NewRequest("DELETE", url, nil)
	if err != nil {
		return err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.badges.delete_user_owl_badge",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.http.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}
	defer close(resp.Body)

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("unexpected response code %d during call to DeleteUserChannelOWLBadge", resp.StatusCode)
	}

	return nil
}

func (c *clientImpl) GrantPledgeBadge(ctx context.Context, userID, sharedSecret string, reqOpts *twitchclient.ReqOpts) error {
	url := url.URL{Path: fmt.Sprintf("/v1/badges/users/%s/global/pledge", userID)}
	bodyBytes, err := json.Marshal(models.GrantBadgeParams{})
	if err != nil {
		return err
	}

	req, err := c.http.NewRequest("POST", url.String(), bytes.NewReader(bodyBytes))
	if err != nil {
		return err
	}

	addSecretToRequest(req, models.IntelAuthHeader, sharedSecret)

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.badges.grant_pledge_badge",
		StatSampleRate: defaultStatSampleRate,
	})

	response, err := c.http.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}

	if response.StatusCode == http.StatusForbidden {
		return &twitchclient.Error{
			StatusCode: http.StatusForbidden,
			Message:    "Incorrect secret key",
		}
	} else if response.StatusCode != http.StatusOK {
		return fmt.Errorf("could not grant chat badge status code was %d", response.StatusCode)
	}

	return nil
}

func (c *clientImpl) RevokePledgeBadge(ctx context.Context, userID, sharedSecret string, reqOpts *twitchclient.ReqOpts) error {
	url := url.URL{Path: fmt.Sprintf("/v1/badges/users/%s/global/pledge", userID)}
	req, err := c.http.NewRequest("DELETE", url.String(), nil)
	if err != nil {
		return err
	}

	addSecretToRequest(req, models.IntelAuthHeader, sharedSecret)

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.badges.revoke_pledge_badge",
		StatSampleRate: defaultStatSampleRate,
	})

	response, err := c.http.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}

	if response.StatusCode == http.StatusForbidden {
		return &twitchclient.Error{
			StatusCode: http.StatusForbidden,
			Message:    "Incorrect secret key",
		}
	} else if response.StatusCode != http.StatusOK {
		return fmt.Errorf("could not revoke chat badge status code was %d", response.StatusCode)
	}

	return nil
}

func addSecretToRequest(request *http.Request, header, secret string) {
	request.Header.Add(header, "Bearer "+secret)
}

func close(closer io.Closer) {
	if err := closer.Close(); err != nil {
		log.Printf("error closing response body: %v", err)
	}
}
