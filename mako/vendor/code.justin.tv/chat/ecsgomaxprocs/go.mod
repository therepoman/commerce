module code.justin.tv/chat/ecsgomaxprocs

go 1.13

require (
	github.com/fatih/color v1.9.0 // indirect
	github.com/fatih/structtag v1.2.0 // indirect
	github.com/go-critic/go-critic v0.4.1 // indirect
	github.com/golangci/golangci-lint v1.21.0
	github.com/mgechev/dots v0.0.0-20190921121421-c36f7dcfbb81 // indirect
	github.com/mgechev/revive v0.0.0-20191017201419-88015ccf8e97
	github.com/olekukonko/tablewriter v0.0.4 // indirect
	github.com/securego/gosec v0.0.0-20200103095621-79fbf3af8d83 // indirect
	github.com/spf13/viper v1.6.1 // indirect
	github.com/stretchr/testify v1.5.1
	github.com/uudashr/gocognit v1.0.1 // indirect
	golang.org/x/tools v0.0.0-20200204192400-7124308813f3 // indirect
)
