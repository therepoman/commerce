# ecsgomaxprocs

[![Build Status](https://jenkins.internal.justin.tv/buildStatus/icon?job=chat/ecsgomaxprocs/master)](https://jenkins.internal.justin.tv/job/chat/job/ecsgomaxprocs/job/master/)

When running Golang applications on ECS EC2 or Fargate, it's possible for there to be a mismatch between `GOMAXPROCS` and the container's CPU quota (CFS bandwidth control). This can cause the Go runtime to overestimate the number of CPUs and can cause CPU throttling/worse performance when under load. If the ECS task has a task CPU size set (hard limit) then this can occur, but not when there is a container CPU (soft limit).

This library can be deprecated when [golang/go#33803](https://github.com/golang/go/issues/33803) is resolved to have the Go runtime aware of a container's CPU quota.

This library will query the ECS task's metadata endpoint to get the task CPU size, and if it exists and is less than `runtime.NumCPU`, then it will set `GOMAXPROCS` to the task CPU size. If the `GOMAXPROCS` environment value is set, then that value takes precedent.

On Fargate it is possible to get a mismatched CPU host when using 1vCPU or 2vCPU. The Fargate service has been observed to place a 1vCPU ECS task onto a 2vCPU host.

It would be prudent to run this library in your application for a few reasons:
* Fargate could place 4vCPU tasks onto >=4vCPU hosts in the future. Fargate can [recycle tasks](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/task-recycle.html) for security or hardware degradation purposes.
* Rescheduling ECS EC2 tasks with a CPU hard limit onto larger EC2 host could cause a performance regression

This library is not needed in Fulton when running on **Fargate**, `GOMAXPROCS` will automatically be set to a lower value if needed in [FultonGoLangBootstrap](https://code.amazon.com/packages/FultonGoLangBootstrap/commits/c7d054744c0a10b3fe4f3084387aaa797ac7e638#)

References:
* In-depth AWS blog post on [ECS CPU and memory resources](https://aws.amazon.com/blogs/containers/how-amazon-ecs-manages-cpu-and-memory-resources/)
    * Task CPU sizes use [cgroup CPU quotas](https://github.com/aws/amazon-ecs-agent/blob/7fb101189ae57170a2cc2976d086bec5a4146b97/agent/api/task/task_linux.go#L94-L127)
    * Container CPU uses [CPU shares](https://github.com/aws/amazon-ecs-agent/blob/7fb101189ae57170a2cc2976d086bec5a4146b97/agent/api/task/task_linux.go#L144-L163)
* [Initial slack discussion](https://twitch.slack.com/archives/C9BUPDUC8/p1578592330104000)
* [CFS Bandwidth Control](https://www.kernel.org/doc/Documentation/scheduler/sched-bwc.txt)
* [uber-go/automaxprocs](https://github.com/uber-go/automaxprocs) is a similar library that reads cgroup files. It does not work on Fargate.

## Usage

Place the below in your Go application's initialization code. This library currently only supports [task metadata endpoint version 3](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/task-metadata-endpoint.html)

```go
var f ecsgomaxprocs.Fixer
if err := f.Exec(); err != nil {
  // log error
}
```

## ECS Test Executions

### Fargate

CPU on Fargate always uses CPU hard limits.

1vCPU
```
2020/02/15 07:09:22 runtime.NumCPU 2 task CPU 1 setting GOMAXPROCS to task CPU because there is a mismatch
```

2vCPU
```
2020/02/15 07:12:02 runtime.NumCPU 4 task CPU 2 setting GOMAXPROCS to task CPU because there is a mismatch
```

4vCPU
```
2020/02/15 07:13:35 runtime.NumCPU() is equal to task CPU. Nothing to do
```

### ECS EC2

ECS container instance has CPU 4096 and Memory 10148 (c5n.xlarge)

1vCPU hard limit
```
2020/02/15 07:40:14 runtime.NumCPU 4 task CPU 1 setting GOMAXPROCS to task CPU because there is a mismatch
```

4vCPU hard limit
```
2020/02/15 07:39:10 runtime.NumCPU() is equal to task CPU. Nothing to do
```

1vCPU soft limit
```
2020/02/15 07:45:44 No task CPU limit is set. Nothing to do
```

## ECS Load Test

### Setup

* Go 1.13.4
* Load is generated on a m5.8xlarge instance. [vegeta](https://github.com/tsenart/vegeta) is used to generate load against the Twirp JSON endpoint
* 3x m5.xlarge (4vCPU) ECS container instances. ECS agent `1.35.0`. ECS optimized AMI `amzn2-ami-ecs-hvm-2.0.20191212-x86_64-ebs (ami-0cbd7a68124b9cff9)`
* ALB in front of ECS service
	* ECS tasks are evenly spread among 3 AZs
	* When running tasks with 2 vCPU it's possible that two tasks are placed onto the same host so they are manually rebalanced by draining container instances to rebalance across AZs
* Twirp RPC handler makes call to DynamoDB PutItem to a random key with a random value
* Prewarmed ALB to 40 LB capacity units
* Combinations tested:
	* **4vcpu_hard**: 3x ECS tasks with 4vCPU task CPU (baseline)
	* **2vcpu_hard**: 3x ECS tasks with 2vCPU task CPU
	* **2vcpu\_hard\_ecsgomaxprocs**: 3x ECS tasks with 2vCPU task CPU running `ecsgomaxprocs`
	* **2.5vcpu_hard**: 3x ECS tasks with 2.5vCPU task CPU
	* **2.5vcpu_\hard\_ecsgomaxprocs**: 3x ECS tasks with 2.5vCPU task CPU running `ecsgomaxprocs`
	* **2vcpu_soft**: 3x ECS tasks with 2vCPU container CPU (soft limit)

ECS AVG CPU is retrieved from CloudWatch metrics. P90 and P99 metrics are retrieved using `vegeta report results.bin`

### 5K RPS

[Result files](https://git-aws.internal.justin.tv/chat/ecsgomaxprocs_loadtestresults/tree/master/5krps). Skipped testing 2.5vCPU since there is no CPU contention at 2vCPU.

| Combination                  | ECS AVG CPU | P90 | P99 |
|------------------------------|---------|-----|-----|
| 4vcpu_hard                   | 20% | 6.47ms | 11.15ms |
| 2vcpu_hard                   | 39% | 6.47ms | 11.58ms |
| 2vcpu\_hard\_ecsgomaxprocs   | 35% | 6.93ms | 12.54ms |
| 2vcpu_soft                   | 40% | 6.8ms | 11.13ms |

There is an insignificance difference between P90/P99 latencies at low CPU contention.

### 10K RPS

[Result files](https://git-aws.internal.justin.tv/chat/ecsgomaxprocs_loadtestresults/tree/master/10krps)

| Combination                  | ECS AVG CPU | P90 | P99 |
|------------------------------|---------|-----|-----|
| 4vcpu_hard                   | 35% | 7.42ms | 14.72ms |
| 2vcpu_hard                   | 71% | 7.16ms | **463.97ms** |
| 2vcpu\_hard\_ecsgomaxprocs   | 60% | 8.62ms | 67.73ms |
| 2vcpu_soft                   | 73% | 7.11ms | 14.09ms |
| 2.5vcpu_hard                 | 59% | 6.86ms | **165.64ms** |
| 2.5vcpu\_hard\_ecsgomaxprocs | 47% | 8.64ms | 46.97ms |

There is a significant difference in P99 latency when there is CPU contention. Running `ecsgomaxprocs` improves P99 latency when there is a mismatch between GOMAXPROCS and the container's CPU quota.

## Development

Build: `make build`

Test: `make test`

Lint: `make lint`

Run `make add-lint-dependencies` one time to pin tool dependencies

The builder image used in Jenkins can be changed in [ci.sh](/ci.sh)

`testdata/` contains ECS task metadata JSON responses
