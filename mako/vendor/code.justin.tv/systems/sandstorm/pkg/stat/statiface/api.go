package statiface

import (
	"code.justin.tv/systems/sandstorm/pkg/stat"
	"github.com/aws/aws-sdk-go/service/cloudwatch"
)

// API is the interface that stats.Client implements
type API interface {
	Increment(stat.Metric)
	Measure(stat.Metric, float64)
	WithMeasuredResult(string, []*cloudwatch.Dimension, func() error) error
}
