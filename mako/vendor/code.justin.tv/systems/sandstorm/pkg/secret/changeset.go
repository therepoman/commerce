package secret

// Changeset input for Callback function
type Changeset struct {
	SecretInfo map[string]int64
}

// SecretsHaveChanged returns true if a secret was updated
func (c *Changeset) SecretsHaveChanged() bool {
	if c.SecretInfo == nil || len(c.SecretInfo) == 0 {
		return false
	}
	return true
}

// Update mrege given secrets with current state
func (c *Changeset) Update(input map[string]int64) {
	if input == nil || len(input) == 0 {
		return
	}
	if c.SecretInfo == nil {
		c.SecretInfo = make(map[string]int64)
	}
	for secretName, updatedAt := range input {
		c.SecretInfo[secretName] = updatedAt
	}
	return
}

// SecretHasBeenUpdated check if secret has been updated.
func (c *Changeset) SecretHasBeenUpdated(secretName string) bool {
	if c.SecretInfo == nil {
		return false
	}
	_, ok := c.SecretInfo[secretName]
	return ok
}

// GetSecretUpdatedAt returns the name and version of updated secret.
func (c *Changeset) GetSecretUpdatedAt(secretName string) (updatedAt int64) {
	if c.SecretInfo == nil {
		return
	}
	updatedAt = c.SecretInfo[secretName]
	return
}
