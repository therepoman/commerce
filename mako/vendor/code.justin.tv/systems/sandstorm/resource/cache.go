package resource

import "sync"

// configs are cached in memory here
var (
	cacheMap     = map[string]*Config{}
	cacheMapLock = &sync.Mutex{}
)

func loadCacheConfig(env string) (cfg *Config) {
	cacheMapLock.Lock()
	defer cacheMapLock.Unlock()

	cfg, ok := cacheMap[env]
	if !ok {
		cfg = nil
	}
	return
}

func storeCacheConfig(env string, cfg *Config) {
	cacheMapLock.Lock()
	defer cacheMapLock.Unlock()

	cacheMap[env] = cfg
}

func deleteCacheConfig(env string) {
	cacheMapLock.Lock()
	defer cacheMapLock.Unlock()

	delete(cacheMap, env)
}
