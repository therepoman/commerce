// NOTE this is a generated file! do not edit!

package user_multi_factor_auth

import (
	"context"

	"github.com/golang/protobuf/proto"

	eventbus "code.justin.tv/eventbus/client"
)

const (
	UpdateEventType = "UserMultiFactorAuthUpdate"
)

type Update = UserMultiFactorAuthUpdate

type UserMultiFactorAuthUpdateHandler func(context.Context, *eventbus.Header, *UserMultiFactorAuthUpdate) error

func (h UserMultiFactorAuthUpdateHandler) Handler() eventbus.Handler {
	return func(ctx context.Context, message eventbus.RawMessage) error {
		dst := &UserMultiFactorAuthUpdate{}
		err := proto.Unmarshal(message.Payload, dst)
		if err != nil {
			return err
		}
		return h(ctx, message.Header, dst)
	}
}

func RegisterUserMultiFactorAuthUpdateHandler(mux *eventbus.Mux, f UserMultiFactorAuthUpdateHandler) {
	mux.RegisterHandler(UpdateEventType, f.Handler())
}

func RegisterUpdateHandler(mux *eventbus.Mux, f UserMultiFactorAuthUpdateHandler) {
	RegisterUserMultiFactorAuthUpdateHandler(mux, f)
}

func (*UserMultiFactorAuthUpdate) EventBusName() string {
	return UpdateEventType
}
