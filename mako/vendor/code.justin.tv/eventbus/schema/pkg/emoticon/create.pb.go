// Code generated by protoc-gen-go. DO NOT EDIT.
// source: emoticon/create.proto

package emoticon

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"
import _ "code.justin.tv/eventbus/schema/pkg/eventbus/owner"

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion2 // please upgrade the proto package

// State represents the State the emoticon is in.
type State int32

const (
	// An invalid state.
	State_STATE_INVALID State = 0
	// The emoticon is active and usable on the site.
	State_STATE_ACTIVE State = 1
	// The emoticon is inactive on the site and cannot be used.
	State_STATE_INACTIVE State = 2
	// The emoticon has been moderated and assets have been deleted.
	State_STATE_MODERATED State = 3
	// The emoticon is in a pending state and will become active if approved by moderation.
	State_STATE_PENDING State = 4
	// The emoticon has been archived.
	State_STATE_ARCHIVED State = 5
	// The emoticon is in a pending state and will become archived if approved by moderation.
	State_STATE_PENDING_ARCHIVED State = 6
)

var State_name = map[int32]string{
	0: "STATE_INVALID",
	1: "STATE_ACTIVE",
	2: "STATE_INACTIVE",
	3: "STATE_MODERATED",
	4: "STATE_PENDING",
	5: "STATE_ARCHIVED",
	6: "STATE_PENDING_ARCHIVED",
}
var State_value = map[string]int32{
	"STATE_INVALID":          0,
	"STATE_ACTIVE":           1,
	"STATE_INACTIVE":         2,
	"STATE_MODERATED":        3,
	"STATE_PENDING":          4,
	"STATE_ARCHIVED":         5,
	"STATE_PENDING_ARCHIVED": 6,
}

func (x State) String() string {
	return proto.EnumName(State_name, int32(x))
}
func (State) EnumDescriptor() ([]byte, []int) {
	return fileDescriptor_create_ba6bb01543b69b47, []int{0}
}

// EmoticonCreate fires when an Emote has been successfully created.
type EmoticonCreate struct {
	// The emoticon information.
	Emoticon *Emoticon `protobuf:"bytes,1,opt,name=emoticon,proto3" json:"emoticon,omitempty"`
	// The state of the emoticon.
	State                State    `protobuf:"varint,2,opt,name=state,proto3,enum=emoticon.State" json:"state,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *EmoticonCreate) Reset()         { *m = EmoticonCreate{} }
func (m *EmoticonCreate) String() string { return proto.CompactTextString(m) }
func (*EmoticonCreate) ProtoMessage()    {}
func (*EmoticonCreate) Descriptor() ([]byte, []int) {
	return fileDescriptor_create_ba6bb01543b69b47, []int{0}
}
func (m *EmoticonCreate) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_EmoticonCreate.Unmarshal(m, b)
}
func (m *EmoticonCreate) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_EmoticonCreate.Marshal(b, m, deterministic)
}
func (dst *EmoticonCreate) XXX_Merge(src proto.Message) {
	xxx_messageInfo_EmoticonCreate.Merge(dst, src)
}
func (m *EmoticonCreate) XXX_Size() int {
	return xxx_messageInfo_EmoticonCreate.Size(m)
}
func (m *EmoticonCreate) XXX_DiscardUnknown() {
	xxx_messageInfo_EmoticonCreate.DiscardUnknown(m)
}

var xxx_messageInfo_EmoticonCreate proto.InternalMessageInfo

func (m *EmoticonCreate) GetEmoticon() *Emoticon {
	if m != nil {
		return m.Emoticon
	}
	return nil
}

func (m *EmoticonCreate) GetState() State {
	if m != nil {
		return m.State
	}
	return State_STATE_INVALID
}

// Emoticon is a container object for information regarding emoticons.
type Emoticon struct {
	// The unique identifier of the emoticon.
	Id string `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	// The owner ID for the emoticon. Could be either a channel ID or a team ID, which is deprecated.
	OwnerId string `protobuf:"bytes,2,opt,name=owner_id,json=ownerId,proto3" json:"owner_id,omitempty"`
	// The group ID of the emote, to signify what emoticon group it belongs to.
	GroupId string `protobuf:"bytes,3,opt,name=group_id,json=groupId,proto3" json:"group_id,omitempty"`
	// The token of the emoticon that is used in Chat.
	Token                string   `protobuf:"bytes,4,opt,name=token,proto3" json:"token,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *Emoticon) Reset()         { *m = Emoticon{} }
func (m *Emoticon) String() string { return proto.CompactTextString(m) }
func (*Emoticon) ProtoMessage()    {}
func (*Emoticon) Descriptor() ([]byte, []int) {
	return fileDescriptor_create_ba6bb01543b69b47, []int{1}
}
func (m *Emoticon) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Emoticon.Unmarshal(m, b)
}
func (m *Emoticon) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Emoticon.Marshal(b, m, deterministic)
}
func (dst *Emoticon) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Emoticon.Merge(dst, src)
}
func (m *Emoticon) XXX_Size() int {
	return xxx_messageInfo_Emoticon.Size(m)
}
func (m *Emoticon) XXX_DiscardUnknown() {
	xxx_messageInfo_Emoticon.DiscardUnknown(m)
}

var xxx_messageInfo_Emoticon proto.InternalMessageInfo

func (m *Emoticon) GetId() string {
	if m != nil {
		return m.Id
	}
	return ""
}

func (m *Emoticon) GetOwnerId() string {
	if m != nil {
		return m.OwnerId
	}
	return ""
}

func (m *Emoticon) GetGroupId() string {
	if m != nil {
		return m.GroupId
	}
	return ""
}

func (m *Emoticon) GetToken() string {
	if m != nil {
		return m.Token
	}
	return ""
}

func init() {
	proto.RegisterType((*EmoticonCreate)(nil), "emoticon.EmoticonCreate")
	proto.RegisterType((*Emoticon)(nil), "emoticon.Emoticon")
	proto.RegisterEnum("emoticon.State", State_name, State_value)
}

func init() { proto.RegisterFile("emoticon/create.proto", fileDescriptor_create_ba6bb01543b69b47) }

var fileDescriptor_create_ba6bb01543b69b47 = []byte{
	// 311 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x54, 0x91, 0x51, 0x4b, 0xc2, 0x50,
	0x14, 0xc7, 0xdb, 0x74, 0xa6, 0xa7, 0x9c, 0xf3, 0x64, 0xb1, 0x7c, 0x12, 0x21, 0x90, 0xa0, 0x09,
	0xf6, 0xd6, 0xdb, 0x72, 0x97, 0xba, 0x50, 0x16, 0xd7, 0xe1, 0x43, 0x2f, 0xa2, 0xdb, 0x2d, 0x46,
	0x6c, 0x57, 0xe6, 0xb5, 0xa0, 0x4f, 0x12, 0x7d, 0x8f, 0xbe, 0x5f, 0x78, 0xaf, 0x73, 0xf4, 0x78,
	0x7e, 0xff, 0xdf, 0xf9, 0xef, 0xb0, 0x0b, 0xa7, 0x3c, 0x15, 0x32, 0x89, 0x44, 0x36, 0x8c, 0x72,
	0xbe, 0x90, 0xdc, 0x5b, 0xe5, 0x42, 0x0a, 0xac, 0x17, 0xb8, 0xdb, 0xe1, 0x1f, 0x3c, 0x93, 0xcb,
	0xcd, 0x7a, 0x28, 0x3e, 0x33, 0x9e, 0xeb, 0xbc, 0xff, 0x05, 0x36, 0xd9, 0x19, 0x63, 0xb5, 0x87,
	0x1e, 0xec, 0x77, 0x5c, 0xa3, 0x67, 0x0c, 0x8e, 0x46, 0xe8, 0x15, 0xc0, 0x2b, 0x5c, 0xb6, 0x77,
	0xf0, 0x02, 0xac, 0xb5, 0x5c, 0x48, 0xee, 0x9a, 0x3d, 0x63, 0x60, 0x8f, 0x5a, 0xa5, 0x3c, 0xdd,
	0x62, 0xa6, 0xd3, 0x9b, 0xf6, 0xcf, 0xaf, 0xdb, 0x94, 0x7c, 0x91, 0x5e, 0x45, 0x22, 0x4d, 0x79,
	0x1e, 0xf1, 0xfe, 0x2b, 0xd4, 0x8b, 0x3e, 0xb4, 0xc1, 0x4c, 0x62, 0xf5, 0xbd, 0x06, 0x33, 0x93,
	0x18, 0xcf, 0xa1, 0xae, 0xce, 0x9c, 0x27, 0xb1, 0x2a, 0x6e, 0xb0, 0x43, 0x35, 0x53, 0x15, 0xbd,
	0xe5, 0x62, 0xb3, 0xda, 0x46, 0x15, 0x1d, 0xa9, 0x99, 0xc6, 0xd8, 0x01, 0x4b, 0x8a, 0x77, 0x9e,
	0xb9, 0x55, 0xc5, 0xf5, 0x70, 0xf9, 0x6d, 0x80, 0xa5, 0x6e, 0xc1, 0x36, 0x34, 0xa7, 0xa1, 0x1f,
	0x92, 0x39, 0x9d, 0xcc, 0xfc, 0x07, 0x1a, 0x38, 0x07, 0xe8, 0xc0, 0xb1, 0x46, 0xfe, 0x38, 0xa4,
	0x33, 0xe2, 0x18, 0x88, 0x60, 0x17, 0xd2, 0x8e, 0x99, 0x78, 0x02, 0x2d, 0xcd, 0x1e, 0x9f, 0x02,
	0xc2, 0xfc, 0x90, 0x04, 0x4e, 0xa5, 0x6c, 0x7b, 0x26, 0x93, 0x80, 0x4e, 0xee, 0x9c, 0x6a, 0xb9,
	0xeb, 0xb3, 0xf1, 0x3d, 0x9d, 0x91, 0xc0, 0xb1, 0xb0, 0x0b, 0x67, 0xff, 0xb4, 0x32, 0xab, 0xdd,
	0xc2, 0xcb, 0xfe, 0x47, 0x2e, 0x6b, 0xea, 0x45, 0xae, 0xff, 0x02, 0x00, 0x00, 0xff, 0xff, 0x42,
	0xcd, 0x0b, 0x33, 0xca, 0x01, 0x00, 0x00,
}
