// NOTE this is a generated file! do not edit!

package user

import (
	"context"

	"github.com/golang/protobuf/proto"

	eventbus "code.justin.tv/eventbus/client"
)

const (
	UndeleteEventType = "UserUndelete"
)

type Undelete = UserUndelete

type UserUndeleteHandler func(context.Context, *eventbus.Header, *UserUndelete) error

func (h UserUndeleteHandler) Handler() eventbus.Handler {
	return func(ctx context.Context, message eventbus.RawMessage) error {
		dst := &UserUndelete{}
		err := proto.Unmarshal(message.Payload, dst)
		if err != nil {
			return err
		}
		return h(ctx, message.Header, dst)
	}
}

func RegisterUserUndeleteHandler(mux *eventbus.Mux, f UserUndeleteHandler) {
	mux.RegisterHandler(UndeleteEventType, f.Handler())
}

func RegisterUndeleteHandler(mux *eventbus.Mux, f UserUndeleteHandler) {
	RegisterUserUndeleteHandler(mux, f)
}

func (*UserUndelete) EventBusName() string {
	return UndeleteEventType
}
