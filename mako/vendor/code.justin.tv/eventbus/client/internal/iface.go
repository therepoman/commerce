package internal

import "github.com/golang/protobuf/proto"

type Message interface {
	proto.Message
	EventBusName() string
}
