package testhelpers

import (
	"encoding/json"
	"time"
)

// SNSMessage is a subset of fields from
// https://docs.aws.amazon.com/sns/latest/dg/sns-message-and-json-formats.html#http-notification-json
type SNSMessage struct {
	MessageID string    `json:"MessageId"`
	Type      string    `json:"Type"`
	TopicArn  string    `json:"TopicArn,omitempty"`
	Timestamp time.Time `json:"Timestamp"`
	Subject   string    `json:"Subject,omitempty"`
	Message   string    `json:"Message"`

	MessageAttributes map[string]interface{} `json:"MessageAttributes,omitempty"`
	Signature         string                 `json:"Signature,omitempty"`
	SignatureVersion  string                 `json:"SignatureVersion,omitempty"`
	SigningCertURL    string                 `json:"SigningCertUrl,omitempty"`
	UnsubscribeURL    string                 `json:"UnsubscribeUrl,omitempty"`
}

// SQSBody is a string ready to be put into the `Message` field of an SQS message.
// Essentially this is just the JSON-encoded value of this message struct.
func (m *SNSMessage) SQSBody() (string, error) {
	buf, err := json.Marshal(m)
	if err != nil {
		return "", err
	}
	return string(buf), nil
}
