package publisher

import (
	"code.justin.tv/eventbus/client/encryption"

	"github.com/aws/aws-sdk-go/aws/client"
)

type Config struct {
	Session     client.ConfigProvider // Required: aws config provider or session
	Environment Environment           // Required: see Environment constants.
	EventTypes  []string              // Required: which events we will publish

	// RouteFetcher can be used to override fetching of routing data from another
	// transport or URL.
	// Leaving it nil (the default) uses the preferred event bus production fetcher.
	RouteFetcher RouteFetcher

	// AuthorizedFieldClientOptions Use the following configuration options to tune how encryption behaves for authorized fields.
	// These options are passed to the OLE client that drives authorized field cryptograpgic operations.
	// If left unset these fields will receive default values.
	AuthorizedFieldClientOptions []encryption.AuthorizedFieldClientOption
}
