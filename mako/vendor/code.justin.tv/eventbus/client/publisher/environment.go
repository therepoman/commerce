package publisher

type Environment string

var validEnvironments = map[Environment]bool{
	EnvProduction:  true,
	EnvStaging:     true,
	EnvDevelopment: true,
	EnvLocal:       true,
}

func IsValidEnvironment(env Environment) bool {
	return validEnvironments[env]
}

func IsMissingEnvironment(env Environment) bool {
	return env == ""
}
