package experiment

// Default defines a fallback group to use if fetching the experiment fails.
type Default struct {
	ID           string
	Kind         Type
	DefaultGroup string
}
