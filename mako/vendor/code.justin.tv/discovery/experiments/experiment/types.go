package experiment

import (
	"fmt"
)

// Type represents an experiment type
type Type int

var (
	// DeviceIDType represents an experiment on device_id
	DeviceIDType = Type(1)
	// UserType represents an experiment on user_id
	UserType = Type(2)
	// ChannelType represents an experiment on channel_id
	ChannelType = Type(3)
)

// Platform represents a platform for which to retrieve experiments
type Platform string

var (
	// LegacyPlatform is the default platform, for all existing experiments
	LegacyPlatform = Platform("experiments.json")
	// AbbyPlatform is the backend platform for AML team
	AbbyPlatform = Platform("abby-experiments.json")
	// TwilightPlatform is the platform for twilight-related experiments
	TwilightPlatform = Platform("twilight-experiments.json")
)

// Errors that are returned by this framework
var (
	ErrInvalidType = fmt.Errorf("experiment type incorrect")
	ErrNoDefault   = fmt.Errorf("all experiments must have a default")
	ErrOverRan     = fmt.Errorf("overran end of exp group list")
)

// Group represents an experimental group.
type Group struct {
	Weight float64 `json:"weight"`
	Value  string  `json:"value"`
}
