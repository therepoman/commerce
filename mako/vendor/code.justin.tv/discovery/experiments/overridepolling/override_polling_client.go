package overridepolling

import (
	"context"
	"encoding/json"
	"sync"
	"time"

	"code.justin.tv/discovery/experiments/clients"
	"code.justin.tv/discovery/experiments/experiment"
	"code.justin.tv/hygienic/errors"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
)

const (
	getOverridesErrorMessage            = "getting overrides failed"
	overridesClientCreationErrorMessage = "both bucket and key should not be empty"
	defaultPollingInterval              = time.Minute
	pollingTimeout                      = 500 * time.Millisecond
)

// FetchAdminIDsClient is a client that will fetch the admin ids to register those as overrides
// Need to be passed in during the creation of Experiments client
type FetchAdminIDsClient interface {
	GetAdminUserIDs(ctx context.Context) ([]string, error)
}

// GlobalOverridesPollingConfig is the config that needs to be passed in during creation of Experiments client
// will use the config to do the polling
type GlobalOverridesPollingConfig struct {
	Bucket              string //the bucket where the file is stored
	Key                 string //the name of the file that contains the users that need to be overridden
	PollingInterval     time.Duration
	FetchAdminIDsClient FetchAdminIDsClient //Fetch admin ids, need to pass in when OverrideAllStaff is used
}

// ExperimentInfo stores the information used to register overrides
type ExperimentInfo struct {
	KeyIDs           []string        `json:"keyids"` //either a list of userID or a list of deviceID
	ExpID            string          `json:"expid"`
	ExpName          string          `json:"expname"`
	Kind             experiment.Type `json:"kind"`
	Group            string          `json:"group"`
	OverrideAllStaff bool            `json:"overridestaff"`
}

type overridesPollingClientImpl struct {
	s3                  clients.S3Client
	s3Key               string
	s3Bucket            string
	pollingInterval     time.Duration
	errorHandler        func(error)
	globalOverrides     map[string]map[string]string
	globalOverridesLock sync.RWMutex

	fetchAdminIDsClient FetchAdminIDsClient
	cancelFunc          context.CancelFunc

	closed chan struct{}
}

// GlobalOverridesPollingClient is the interface of the client that will poll users from S3
type GlobalOverridesPollingClient interface {
	StartPolling()
	GetGlobalOverrideGroup(expID, id string) (string, bool)
	Close()
}

// OverridesPollingClientDependencies is the dependency that can be injected into override polling client
type OverridesPollingClientDependencies struct {
	Client clients.S3Client
}

// NewOverridesPollingClientWithDependencies is used in testing with a s3 client injection
func NewOverridesPollingClientWithDependencies(config *GlobalOverridesPollingConfig, errorHandler func(error), dependencies *OverridesPollingClientDependencies) (GlobalOverridesPollingClient, error) {
	return newOverridesPollingClientWithDependencies(config, errorHandler, dependencies)
}

// Inject a mock of s3 client, facilitate testing
func newOverridesPollingClientWithDependencies(config *GlobalOverridesPollingConfig, errorHandler func(error), dependencies *OverridesPollingClientDependencies) (*overridesPollingClientImpl, error) {
	// Create an S3 client that will connect to S3
	s3Client := dependencies.Client
	if s3Client == nil {
		sess, err := session.NewSession(&aws.Config{
			Region: aws.String("us-west-2"),
		})
		if err != nil {
			return nil, err
		}
		s3Client = s3.New(sess)
	}

	if config.PollingInterval <= 0 {
		config.PollingInterval = defaultPollingInterval
	}
	if config.Bucket == "" || config.Key == "" {
		return nil, errors.New(overridesClientCreationErrorMessage)
	}

	return &overridesPollingClientImpl{
		s3:                  s3Client,
		s3Key:               config.Key,
		s3Bucket:            config.Bucket,
		fetchAdminIDsClient: config.FetchAdminIDsClient,
		pollingInterval:     config.PollingInterval,
		errorHandler:        errorHandler,
		globalOverrides:     map[string]map[string]string{},
		closed:              make(chan struct{}),
	}, nil
}

// NewOverridesPollingClient will create a polling client with the provided config
func NewOverridesPollingClient(config *GlobalOverridesPollingConfig, errorHandler func(error)) (GlobalOverridesPollingClient, error) {
	op, err := newOverridesPollingClientWithDependencies(config, errorHandler, &OverridesPollingClientDependencies{})
	if err != nil {
		return nil, err
	}
	return op, nil
}

func (op *overridesPollingClientImpl) Close() {
	if op.cancelFunc != nil {
		op.cancelFunc()
	}
}

func (op *overridesPollingClientImpl) getOverrides(ctx context.Context) ([]ExperimentInfo, error) {
	obj, err := op.s3.GetObjectWithContext(ctx, &s3.GetObjectInput{
		Bucket: aws.String(op.s3Bucket),
		Key:    aws.String(op.s3Key),
	})
	if err != nil {
		return nil, err
	}

	var overridesSlice []ExperimentInfo
	decoder := json.NewDecoder(obj.Body)
	err = decoder.Decode(&overridesSlice)
	if err != nil {
		return nil, errors.Wrap(err, getOverridesErrorMessage)
	}

	return overridesSlice, nil
}

func (op *overridesPollingClientImpl) StartPolling() {
	ctx, cancelFunc := context.WithCancel(context.Background())
	op.cancelFunc = cancelFunc
	go func() {
		for {
			op.poll(ctx)
			select {
			case <-time.NewTimer(op.pollingInterval).C:
				continue
			case <-ctx.Done():
			}

			break
		}
		close(op.closed)
	}()
}

func (op *overridesPollingClientImpl) poll(ctx context.Context) {
	ctx, cancelFunc := context.WithTimeout(ctx, pollingTimeout)
	defer cancelFunc()

	overridesSlice, err := op.getOverrides(ctx)
	if err != nil {
		if ctx.Err() == context.Canceled {
			return
		}

		if op.errorHandler != nil {
			op.errorHandler(err)
		}
		return
	}

	op.globalOverridesLock.Lock()
	defer op.globalOverridesLock.Unlock()
	op.globalOverrides = map[string]map[string]string{}
	for _, override := range overridesSlice {
		_, ok := op.globalOverrides[override.ExpID]
		if !ok {
			op.globalOverrides[override.ExpID] = map[string]string{}
		}

		for _, id := range override.KeyIDs {
			op.globalOverrides[override.ExpID][id] = override.Group
		}

		if override.OverrideAllStaff && op.fetchAdminIDsClient != nil {
			adminIDs, err := op.fetchAdminIDsClient.GetAdminUserIDs(ctx)
			if err == nil {
				for _, id := range adminIDs {
					// Only adding it when the id is not assigned, enforcing precedences of KeyIDs > OverrideAllStaff
					if _, ok := op.globalOverrides[override.ExpID][id]; ok {
						continue
					}
					op.globalOverrides[override.ExpID][id] = override.Group
				}
			} else {
				op.errorHandler(err)
			}
		}
	}
}

func (op *overridesPollingClientImpl) GetGlobalOverrideGroup(expID, id string) (string, bool) {
	op.globalOverridesLock.RLock()
	defer op.globalOverridesLock.RUnlock()
	if overrides, overridesOk := op.globalOverrides[expID]; overridesOk {
		group, ok := overrides[id]
		return group, ok
	}
	return "", false
}
