# experiments

[![GoDoc](https://godoc.org/github.com/golang/gddo?status.svg)](https://godoc.internal.justin.tv/code.justin.tv/discovery/experiments)

Package experiments facilitates treating requests into groups based on experiment configurations defined in
[Minixperiment](https://git-aws.internal.justin.tv/stats/minixperiment).  It can be used for various purposes such as

  * split testing (configuring multiple treatments with different group assignment ratios)
  * gradual rollout (progressively changing the size of the defined treatment groups to increase the number of
    users/clients assigned to the "new" treatment group)
  * feature flags (configuring a single treatment group experiment with a value that is used to turn a newly-released
    featured on or off)

Package experiments supports user and device based experiments.  This is specified when the experiment is defined in
Minixperiment.

Package experiments supports tracking experiment treatments.  Specifically, upon each successful treatment, it sends the
following tracking event to Spade.

```
“experiment_branch”: {
    id: string,
    experiment_group: string,
    experiment_id: string,
    experiment_name: string,
    experiment_type: string,
    platform: “backend-client”,
    user_id: string,   // user_id will be null if this is a device experiment
    device_id: string, // device_id will be null if this is a user experiment
    // All other fields will be null
}

```

## Usage

```go
import "code.justin.tv/discovery/experiments"
```

A runnable example of using this library is available in `example/`. More details on how to integrate are below.


## Quick start example

Let's say we own a Feeds service and wanted to run an experiment with our GetFeed endpoint where we vary the number of
posts that we serve.  The experiment will have two groups:

  * the *control* group - 99% of users, who should get 3 posts
  * the *variant* group - 1% of users, who should get 5 posts

We'll explore how we would use Minixperiment and package experiments in our service to divide requests based on the
requester's user ID.

### Define the experiment in Minixperiment

Package experiments supports using experiments defined in Minixperiment.  Use the
[Minixperiment Web Administration Tool](https://minixperiment.prod.us-west2.justin.tv/) to define our experiment.

```yaml
Name: pulse_post_count
UUID: b9160ccd-9f20-4f92-9baa-7a65e6798a7b
Experiment Type: User ID
Publish to: Abby
Groups:
  - Value: control
    Weight: 99
  - Value: variant
    Weight: 1
```

### Set up access to experiments in S3

The Experiments library requires access to the `s3://minixperiment-prod/` bucket to retrieve experiments.
For production, the machines that the consumer service are run on should be allowed to access S3.
The role policy should also have the following:

```
  {
      "Action": "s3:GetObject",
      "Resource": [
          "arn:aws:s3:::minixperiment-prod/*"
      ],
      "Effect": "Allow"
  }
```


### Initialize Experiments

Create a new `Experiments` instance by calling the `experiments.New` function.  The Experiments instance will be
configured to poll for a JSON file that contains experiment definitions.

`experiments.New` takes a configuration object.  Package `experiments` defines a few configuration structs to support
integrating with configuration frameworks.  We'll use `experiments.StaticConfig` here because we want to initialize
using hardcoded values.  We are required to provide:

  * The Spade endpoint that tracking events should be sent to
  * The platform for which to retrieve experiments

The config struct also has optional fields to allow us to specify how errors are handled, and other settings.

`AbbyPlaform` needs to be imported from `code.justin.tv/discovery/experiments/experiment`

```go
var exps experiments.Experiments

func setupService() {
	// Initialize an Experiments instance
	var err error
	exps, err = experiments.New(experiments.Config{
		SpadeEndpoint:       "https://spade.internal.di.twitch.a2z.com",
		Platform:             experiment.AbbyPlatform,
		// Set a function to handle errors in goroutines started by Experiments such as errors sending
		// events to Spade, or errors fetching experiments from Minixperiment
		ErrorHandler: func(err error) {
			log.Printf("%v", err)
		},
	})
  if err != nil {
    // this means experiments can't run, probably should terminate the program
  }

	// etc
}
```

### Register default groups

Before treating requests we are **required** to register a default group for the experiment. This default is used as
the fallback if there is a problem fetching from Minixperiment.

```go
const expID = "b9160ccd-9f20-4f92-9baa-7a65e6798a7b"
const expName = "pulse_post_count"

exps.RegisterDefault(experiments.UserType, expName, expID, "control")
```

*Note*: The one exception to this requirement is if `TreatWithDefault` is used, in which case a default group can be provided with
the request. This is described in more detail in the [treating with a default](#treating-with-a-default) section below.

### Register local overrides (if applicable)

If certain users (developers, product managers, etc) should have access to particular branches of the experiment regardless of their bucketing, an override can be added locally.

```
err := exps.RegisterOverride(experiments.UserType, expName, expID, "variant", "bezos", "emmett")
```

`RegisterOverride` should be called only after `RegisterDefault` is called. It will return errors if there is a mismatch with the registered default.

### Polling global overrides (if applicable)
Global overrides will allow users to modify overrides without making code changes since overrides will be polled from a file on S3. The overrides registered in this way will behave exactly the same as local overrides but have a lower precedence. If the global overrides conflict with the local overrides (e.g. assign an id to two groups in an experiment), the local overrides will be used. 

**Note:** Every application that is polling global overrides should make sure that they have read permission of relevant S3 bucket and the AWS credentials are not expired. Otherwise, errors will be encountered every polling cycle. 

##### Workflow under global overrides
- Create/modify the overrides file. 
    
    The overrides file should be a json file with a list of the objects that have the following fields
    ```json    
    [
      {
        "keyids": [
          "bezos",
          "emmett"
        ],
        "expid": "b9160ccd-9f20-4f92-9baa-7a65e6798a7b",
        "expname": "pulse_post_count",
        "kind": 2,
        "group": "variant",
        "overridestaff": false,
      }
    ]    
    ```
- Upload the overrides file to S3

  Run the makefile command `make upload_overrides S3_BUCKET=my-bucket LOCAL_OVERRIDES_FILE=/path/to/overrides/file`. Users can also supply a `S3_KEY` argument if the local file has different names from the S3 file. This command will download the S3 file (if exists) and output a raw diff between the two files. Then, it will prompt user for input to continue the upload process.

- Setup polling client

  Import the override polling client at `code.justin.tv/discovery/experiments/overridepolling`

  In the configuration object mentioned above, we pass in the overrides polling config.
  ```go
  experiments.Config{
    SpadeEndpoint:       "https://spade.internal.di.twitch.a2z.com",
    Platform:             experiment.AbbyPlatform,
    // other configurations...
    GlobalOverridesConfig: &overridepolling.GlobalOverridesPollingConfig{
      PollingInterval: time.Minute, 
      Bucket:          "my-bucket", // the name of bucket we are polling from
      Key:             "overrides-file.json", // the key of the file on S3
    },
  }
  ```

- Start the polling

  We can start the polling by just calling `StartGlobalOverridePolling`. This function is under `Experiments` interface. 

### FetchAdminIDsClient Interface for global overrides
If the developers would like to use the `overridestaff` to override all staff members into a specific group, they need to implement the interface. This interface only contains one function call `GetAdminUserIDs(ctx context.Context) ([]string, error)`. For each polling cycle, the function will be invoked every time when the `overridestaff` option is set to `true` for an override. 

### Treating requests into groups

To treat a request into a group, call the `Treat` method on the `Experiments` instance.

```go
func getFeed(userID string) (*Feed, error) {
	group, err := exps.Treat(experiment.UserType, expName, expID, userID)
	if err != nil {
		return nil, err
	}

	postsLimit := 3
	if group == "variant" {
		postsLimit = 5
	}

	// etc
}
```

NOTE: Treat returns an error if:

  * A default group was not registered for the experiment
  * The given experiment type did not match the experiment type of the corresponding experiment defined in
    Minixperiment, or the experiment type specified when registering the default

Treat does not return an error and returns the default if the experiment ID does not match any of the experiments fetched
from Minixperiment.

### Treating with a default

If you need to treat a request without registering a default for the associated experiment beforehand, you
can use the `TreatWithDefault` method instead. This is not recommended unless `Treat` does not work for your use case,
such as when creating experiments dynamically. Note that defining overrides on an experiment is not possible with this approach.

```go
def := experiment.Default{
	ID:           expID,
	Kind:         experiment.UserType,
	DefaultGroup: "control",
}

group, err := exps.TreatWithDefault(experiment.UserType, expName, expID, userID, def)
if err != nil {
    return nil, err
}
```

### Cleanup

`Experiments` creates background goroutines that do tasks like polling Minixperiment.  To stop these goroutines, call
the `Close` method.

```go
func shutdown() {
	_ = exps.Close()

	// etc
}
```

## Development

### Run Tests

	make test

Coverage should be as high as possible.

### Update Dependencies

Requires [glide](https://glide.sh/) and [glide-vc](https://github.com/sgotti/glide-vc#install) to be installed.

	make update

### Generate Mocks

Requires [vektra/mockery](https://github.com/vektra/mockery#installation) to be installed.

    make mocks

### Generate Assignments Test File

Package experiments includes a unit test that verifies that Experiments assigns requests to the same groups that
minixperiment-client-js does for a predefined experiment.  It works by reading a pre-generated JSON file that contains
device IDs, and the group that minixperiment-client-js assigns those IDs to.

This repository contains a NodeJS script that can be used to regenerate the JSON file.  It requires NodeJS, which can
be installed using Homebrew.

    brew install node


The script has dependencies which need to be downloaded before it can be run.  Navigate to the `gen-test-groups`
directory and run

    npm install

The script can then be run using the `test_assignments` Makefile target,

    make test_assignments

## Missing features

  * Overriding (TBD)
