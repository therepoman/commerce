package experiments

import (
	"context"
	"sync"
	"time"

	"code.justin.tv/discovery/experiments/experiment"
)

// DefaultPollingInterval is the default amount of time to wait before re-fetching experiments from the experiments
// endpoint.
var DefaultPollingInterval = time.Minute

type mxProviderDependencies struct {
	client minixperimentClient
}

// newMinixperimentProvider returns a provider that provides access to experiments defined in an experiments.json file
// generated by Minixperiment.  The provider periodically downloads the file and caches its contents in memory.
func newMinixperimentProvider(platform experiment.Platform, pollingInterval time.Duration, errorHandler func(error)) (Provider, error) {
	m, err := newMXProviderWithDeps(platform, pollingInterval, errorHandler, &mxProviderDependencies{})
	if err != nil {
		return nil, err
	}

	m.start()
	return m, nil
}

// newMXProviderWithDeps sets up a minixperiment provider, by injecting the given dependencies and config.  It is
// meant to facilitate testing.
func newMXProviderWithDeps(platform experiment.Platform, pollingInterval time.Duration, errorHandler func(error), dependencies *mxProviderDependencies) (*minixperimentProvider, error) {
	client := dependencies.client
	var err error
	if client == nil {
		client, err = newS3MinixperimentClient(platform)
		if err != nil {
			return nil, err
		}
	}

	return &minixperimentProvider{
		client:          client,
		experiments:     make(map[string]*Experiment),
		pollingInterval: pollingInterval,
		errorHandler:    errorHandler,
		closed:          make(chan struct{}),
	}, nil
}

type minixperimentProvider struct {
	client          minixperimentClient
	experiments     map[string]*Experiment
	experimentsLock sync.RWMutex

	pollingInterval time.Duration
	errorHandler    func(error)

	cancelFunc     context.CancelFunc
	cancelFuncLock sync.Mutex

	// closed is a channel that minixperimentProvider closes after it has finished stopping its background
	// goroutine.
	closed chan struct{}
}

var _ Provider = &minixperimentProvider{}

func (m *minixperimentProvider) GetExperiment(experimentID string) *Experiment {
	m.experimentsLock.RLock()
	defer m.experimentsLock.RUnlock()
	return m.experiments[experimentID]
}

func (m *minixperimentProvider) Close() {
	var cancelFunc context.CancelFunc

	m.cancelFuncLock.Lock()
	cancelFunc = m.cancelFunc
	m.cancelFuncLock.Unlock()

	if cancelFunc != nil {
		cancelFunc()
	}
}

func (m *minixperimentProvider) start() {
	ctx, cancelFunc := context.WithCancel(context.Background())

	m.cancelFuncLock.Lock()
	m.cancelFunc = cancelFunc
	m.cancelFuncLock.Unlock()

	// Get experiments right away
	m.getAndSaveExperiments(ctx)

	go m.pollForExperiments(ctx)
}

func (m *minixperimentProvider) pollForExperiments(ctx context.Context) {
	for {
		m.getAndSaveExperiments(ctx)

		select {
		case <-time.NewTimer(m.pollingInterval).C:
			// Wait before fetching experiments again
			continue
		case <-ctx.Done():
			// Exit when the context is closed
		}

		break
	}

	close(m.closed)
}

func (m *minixperimentProvider) getAndSaveExperiments(ctx context.Context) {
	c, cancelFunc := context.WithTimeout(ctx, 15*time.Second)
	defer cancelFunc()

	exps, err := m.client.getExperiments(c)
	if err != nil {
		// If the error is due to the context being closed, just exit. (We don't want to fill logs with
		// "context cancelled"
		if ctx.Err() == context.Canceled {
			return
		}
		// Log other errors
		if m.errorHandler != nil {
			m.errorHandler(err)
		}
		return
	}

	idsToExps := make(map[string]*Experiment)
	for _, exp := range exps {
		idsToExps[exp.ID] = exp
	}

	m.experimentsLock.Lock()
	m.experiments = idsToExps
	m.experimentsLock.Unlock()
}
