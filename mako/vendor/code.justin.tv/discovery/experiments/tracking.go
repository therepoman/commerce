package experiments

import (
	"bytes"
	"fmt"

	"code.justin.tv/hygienic/spade"
)

// Tracking contains the Spade client methods that Experiments requires, and  is meant to facilitate mocking in tests.
type Tracking interface {
	QueueEvents(events ...spade.Event)
	Close() error
}

var _ Tracking = &spade.Client{}

// NewTrackingClient returns a client that can send tracking events to Spade.
func NewTrackingClient(config Config) Tracking {
	client := &spade.Client{
		Config: &spade.DynamicConfig{
			SpadeHostFunc: func() string {
				return config.SpadeEndpoint
			},
			BacklogSizeFunc: func() int64 {
				return config.SpadeBacklogSize
			},
		},
		Logger: &spadeLogger{
			errorHandler: config.ErrorHandler,
		},
	}
	client.Setup()

	// Start a goroutine to send queued tracking events to Spade.
	go func() {
		_ = client.Start() // Start always returns nil
	}()

	return client
}

// spadeLogger facilitates handling errors that get logged by spade.Client.
type spadeLogger struct {
	errorHandler func(error)
}

var _ spade.Logger = &spadeLogger{}

// Log gets called by spade.Client to log errors.  We convert these log statements back to errors, and handle them
// using Experiment's error handler.  It's hacky, but it's what we have to do until spade.Client has a more compatible
// interface.
func (t *spadeLogger) Log(vals ...interface{}) {
	if t.errorHandler == nil || len(vals) == 0 {
		return
	}

	// Convert the log message back into an error.
	var buffer bytes.Buffer
	for _, val := range vals {
		buffer.WriteString(fmt.Sprintf("%v ", val))
	}
	err := fmt.Errorf(buffer.String())

	t.errorHandler(err)
}
