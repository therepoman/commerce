package experiments

import (
	"context"
	"encoding/json"
	"fmt"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"

	"code.justin.tv/discovery/experiments/clients"
	"code.justin.tv/discovery/experiments/experiment"
	"code.justin.tv/hygienic/errors"
)

const (
	getExperimentsErrorMessage = "getting experiments failed"
	minixperimentBucket        = "minixperiment-prod"
)

type minixperimentClient interface {
	getExperiments(ctx context.Context) ([]*Experiment, error)
}

type minixperimentClientImpl struct {
	s3       clients.S3Client
	platform experiment.Platform
}

var _ minixperimentClient = &minixperimentClientImpl{}

func newS3MinixperimentClient(platform experiment.Platform) (*minixperimentClientImpl, error) {
	sess, err := session.NewSession(&aws.Config{
		Region: aws.String("us-west-2"),
	})
	if err != nil {
		return nil, err
	}

	client := s3.New(sess)

	return &minixperimentClientImpl{
		s3:       client,
		platform: platform,
	}, nil
}

type minixpExperiment struct {
	Name    string             `json:"name"`
	Kind    experiment.Type    `json:"t"`
	Version int                `json:"v"`
	Salt    int                `json:"s"`
	Groups  []experiment.Group `json:"groups"`
}

func (c *minixperimentClientImpl) getExperiments(ctx context.Context) ([]*Experiment, error) {
	obj, err := c.s3.GetObjectWithContext(ctx, &s3.GetObjectInput{
		Bucket: aws.String(minixperimentBucket),
		Key:    aws.String(string(c.platform)),
	})
	if err != nil {
		return nil, err
	}

	var idToExperiment map[string]*minixpExperiment
	decoder := json.NewDecoder(obj.Body)
	err = decoder.Decode(&idToExperiment)
	if err != nil {
		return nil, errors.Wrap(err, getExperimentsErrorMessage)
	}

	exps := make([]*Experiment, 0, len(idToExperiment))
	for id, minixp := range idToExperiment {
		if minixp == nil {
			continue
		}

		var salt string
		if minixp.Salt != 0 {
			salt = fmt.Sprintf("%d", minixp.Salt)
		}

		exps = append(exps, &Experiment{
			ID:      id,
			Name:    minixp.Name,
			Kind:    minixp.Kind,
			Version: minixp.Version,
			Salt:    salt,
			Groups:  minixp.Groups,
		})
	}
	return exps, nil
}
