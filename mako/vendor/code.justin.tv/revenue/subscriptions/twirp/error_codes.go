package substwirp

// These error codes are meant to be returned within a substwirp.ClientError type.

const (
	// Generic error codes
	ErrCodeInvalidUser         = "INVALID_USER"
	ErrCodeInvalidProductOwner = "INVALID_PRODUCT_OWNER"

	// Shared emote error codes
	ErrCodeCodeNotUnique = "CODE_NOT_UNIQUE"

	// CreateEmoticon
	ErrCodeInvalidEmoticonPrefix     = "INVALID_PREFIX"
	ErrCodeEmoticonLimit             = "EMOTICON_LIMIT_REACHED"
	ErrCodeCodeUnacceptable          = "CODE_UNACCEPTABLE"
	ErrCodeInvalidSuffixFormat       = "INVALID_SUFFIX_FORMAT"
	ErrCodeImage28IDNotFound         = "IMAGE28ID_NOT_FOUND"
	ErrCodeImage56IDNotFound         = "IMAGE56ID_NOT_FOUND"
	ErrCodeImage112IDNotFound        = "IMAGE112ID_NOT_FOUND"
	ErrCodeInvalidEmoteImageUpload   = "INVALID_EMOTE_IMAGE_UPLOAD"
	ErrCodeEmoteImageNotFound        = "EMOTE_IMAGE_NOT_FOUND"
	ErrCodeNotEnoughEmoteImageAssets = "NOT_ENOUGH_EMOTE_IMAGE_ASSETS"
	ErrCodeTooManyEmoteImageAssets   = "TOO_MANY_EMOTE_IMAGE_ASSETS"
	ErrCodeInvalidEmoteAssetType     = "INVALID_EMOTE_ASSET_TYPE"
	ErrCodeMissingStaticEmoteAsset   = "EMOTE_MISSING_STATIC_ASSET"
	ErrCodeMissingAnimatedEmoteAsset = "EMOTE_MISSING_ANIMATED_ASSET"
	ErrCodeCreateEmoticonUnknown     = "CREATE_EMOTICON_UNKNOWN"

	// ErrCodeProductDisplayNameLimitReached represents error when product name specific is more than allowed limit.
	ErrCodeProductDisplayNameLimitReached = "PRODUCT_DISPLAY_NAME_LIMIT_REACHED"

	// CreateChannelBadgeUploadConfig
	ErrCodeInvalidBadgeSize = "INVALID_BADGE_SIZE"

	// CreateChannelBadge
	ErrCodeInvalidBadgeImage1XID            = "INVALID_BADGE_IMAGE_1X_ID"
	ErrCodeInvalidBadgeImage2XID            = "INVALID_BADGE_IMAGE_2X_ID"
	ErrCodeInvalidBadgeImage4XID            = "INVALID_BADGE_IMAGE_4X_ID"
	ErrCodeInvalidBadgeRequiredTenureMonths = "INVALID_BADGE_REQUIRED_TENURE_MONTHS"
	ErrCodeBadgeExists                      = "BADGE_EXISTS"
	ErrCodeBadgeTimeout                     = "BADGE_TIMEOUT"

	//PartnerOff-Boarding
	ErrUserHasRedeemedSubscription          = "USER_HAS_REDEEMED_SUBSCRIPTION"
	ErrChannelNotOffBoarded                 = "CHANNEL_NOT_OFF_BOARDED"
	ErrUserSubscribed                       = "USER_HAS_EXISTING_SUBSCRIPTION"
	ErrInvalidRedeemChannelId               = "REDEEM_CHANNEL_INVALID"
	ErrRedemptionNotFound                   = "REDEMPTION_NOT_FOUND"
	ErrUserNotSubscribedToOffBoardedChannel = "USER_NOT_SUBSCRIBED_TO_OFF_BOARDED_CHANNEL"

	// AssignEmoteToProduct
	ErrCodeUserNotPermitted  = "USER_NOT_PERMITTED"
	ErrCodeEmoteDoesNotExist = "EMOTE_DOES_NOT_EXIST"
	ErrCodeInvalidEmoteState = "INVALID_EMOTE_STATE"
)

type ClientError struct {
	ErrorCode string `json:"error_code"`
}
