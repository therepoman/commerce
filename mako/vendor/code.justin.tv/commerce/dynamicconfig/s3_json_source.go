package dynamicconfig

import (
	"context"
	"encoding/json"

	"github.com/pkg/errors"
)

// S3JSONSource sources values from a JSON file in S3 storage.
// Note: The output value for each key is []byte so you can decode/unmarshall to your expected format.
type S3JSONSource struct {
	s3Source *S3MultiFileSource
	filepath string
}

// NewS3JSONSource returns a new S3JSONSource based on the given region, endpoint, bucket, and filepath.
func NewS3JSONSource(region, endpoint, bucket, filepath string) (*S3JSONSource, error) {
	s3Source, err := NewS3MultiFileSource(region, endpoint, bucket, []string{filepath})
	if err != nil {
		return nil, err
	}

	return &S3JSONSource{
		s3Source: s3Source,
		filepath: filepath,
	}, nil
}

// FetchValues returns the unmarshaled JSON object
func (s *S3JSONSource) FetchValues(ctx context.Context) (map[string]interface{}, error) {
	resultMap, err := s.s3Source.FetchValues(ctx)
	if err != nil {
		return nil, err
	}
	result, ok := resultMap[s.filepath]
	if !ok {
		return nil, errors.New("underlying s3 source did not return any value for the filepath")
	}
	var aJSON map[string]interface{}
	if err := json.Unmarshal(result.([]byte), &aJSON); err != nil {
		return nil, err
	}
	return aJSON, nil
}
