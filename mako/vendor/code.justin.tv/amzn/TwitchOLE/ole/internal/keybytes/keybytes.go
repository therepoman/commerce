package keybytes

// Zero overwrites a slice with zeroes
func Zero(b []byte) {
	for i := range b {
		b[i] = 0
	}
}

// Copy allocates a new array and copies slice elements to the new
// slice
func Copy(src []byte) []byte {
	dst := make([]byte, len(src))
	copy(dst, src)
	return dst
}
