// Code generated by protoc-gen-go. DO NOT EDIT.
// source: code.justin.tv/amzn/TwitchVXFollowingServiceECSTwirp/twirp.proto

package TwitchVXFollowingServiceECSTwirp

import (
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	_ "github.com/golang/protobuf/ptypes/empty"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type FollowsReq_SortDir int32

const (
	FollowsReq_ASC  FollowsReq_SortDir = 0
	FollowsReq_DESC FollowsReq_SortDir = 1
)

var FollowsReq_SortDir_name = map[int32]string{
	0: "ASC",
	1: "DESC",
}

var FollowsReq_SortDir_value = map[string]int32{
	"ASC":  0,
	"DESC": 1,
}

func (x FollowsReq_SortDir) String() string {
	return proto.EnumName(FollowsReq_SortDir_name, int32(x))
}

func (FollowsReq_SortDir) EnumDescriptor() ([]byte, []int) {
	return fileDescriptor_09e688a074f959d6, []int{3, 0}
}

type FollowReq struct {
	FromUserId           string   `protobuf:"bytes,1,opt,name=from_user_id,json=fromUserId,proto3" json:"from_user_id,omitempty"`
	TargetUserId         string   `protobuf:"bytes,2,opt,name=target_user_id,json=targetUserId,proto3" json:"target_user_id,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *FollowReq) Reset()         { *m = FollowReq{} }
func (m *FollowReq) String() string { return proto.CompactTextString(m) }
func (*FollowReq) ProtoMessage()    {}
func (*FollowReq) Descriptor() ([]byte, []int) {
	return fileDescriptor_09e688a074f959d6, []int{0}
}

func (m *FollowReq) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_FollowReq.Unmarshal(m, b)
}
func (m *FollowReq) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_FollowReq.Marshal(b, m, deterministic)
}
func (m *FollowReq) XXX_Merge(src proto.Message) {
	xxx_messageInfo_FollowReq.Merge(m, src)
}
func (m *FollowReq) XXX_Size() int {
	return xxx_messageInfo_FollowReq.Size(m)
}
func (m *FollowReq) XXX_DiscardUnknown() {
	xxx_messageInfo_FollowReq.DiscardUnknown(m)
}

var xxx_messageInfo_FollowReq proto.InternalMessageInfo

func (m *FollowReq) GetFromUserId() string {
	if m != nil {
		return m.FromUserId
	}
	return ""
}

func (m *FollowReq) GetTargetUserId() string {
	if m != nil {
		return m.TargetUserId
	}
	return ""
}

type BatchFollowsReq struct {
	FromUserId           string   `protobuf:"bytes,1,opt,name=from_user_id,json=fromUserId,proto3" json:"from_user_id,omitempty"`
	TargetUserIds        []string `protobuf:"bytes,2,rep,name=target_user_ids,json=targetUserIds,proto3" json:"target_user_ids,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *BatchFollowsReq) Reset()         { *m = BatchFollowsReq{} }
func (m *BatchFollowsReq) String() string { return proto.CompactTextString(m) }
func (*BatchFollowsReq) ProtoMessage()    {}
func (*BatchFollowsReq) Descriptor() ([]byte, []int) {
	return fileDescriptor_09e688a074f959d6, []int{1}
}

func (m *BatchFollowsReq) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_BatchFollowsReq.Unmarshal(m, b)
}
func (m *BatchFollowsReq) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_BatchFollowsReq.Marshal(b, m, deterministic)
}
func (m *BatchFollowsReq) XXX_Merge(src proto.Message) {
	xxx_messageInfo_BatchFollowsReq.Merge(m, src)
}
func (m *BatchFollowsReq) XXX_Size() int {
	return xxx_messageInfo_BatchFollowsReq.Size(m)
}
func (m *BatchFollowsReq) XXX_DiscardUnknown() {
	xxx_messageInfo_BatchFollowsReq.DiscardUnknown(m)
}

var xxx_messageInfo_BatchFollowsReq proto.InternalMessageInfo

func (m *BatchFollowsReq) GetFromUserId() string {
	if m != nil {
		return m.FromUserId
	}
	return ""
}

func (m *BatchFollowsReq) GetTargetUserIds() []string {
	if m != nil {
		return m.TargetUserIds
	}
	return nil
}

type FollowResp struct {
	FromUserId           string   `protobuf:"bytes,1,opt,name=from_user_id,json=fromUserId,proto3" json:"from_user_id,omitempty"`
	TargetUserId         string   `protobuf:"bytes,2,opt,name=target_user_id,json=targetUserId,proto3" json:"target_user_id,omitempty"`
	FollowedAt           string   `protobuf:"bytes,3,opt,name=followed_at,json=followedAt,proto3" json:"followed_at,omitempty"`
	BlockNotifications   bool     `protobuf:"varint,4,opt,name=block_notifications,json=blockNotifications,proto3" json:"block_notifications,omitempty"`
	Cursor               string   `protobuf:"bytes,5,opt,name=cursor,proto3" json:"cursor,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *FollowResp) Reset()         { *m = FollowResp{} }
func (m *FollowResp) String() string { return proto.CompactTextString(m) }
func (*FollowResp) ProtoMessage()    {}
func (*FollowResp) Descriptor() ([]byte, []int) {
	return fileDescriptor_09e688a074f959d6, []int{2}
}

func (m *FollowResp) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_FollowResp.Unmarshal(m, b)
}
func (m *FollowResp) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_FollowResp.Marshal(b, m, deterministic)
}
func (m *FollowResp) XXX_Merge(src proto.Message) {
	xxx_messageInfo_FollowResp.Merge(m, src)
}
func (m *FollowResp) XXX_Size() int {
	return xxx_messageInfo_FollowResp.Size(m)
}
func (m *FollowResp) XXX_DiscardUnknown() {
	xxx_messageInfo_FollowResp.DiscardUnknown(m)
}

var xxx_messageInfo_FollowResp proto.InternalMessageInfo

func (m *FollowResp) GetFromUserId() string {
	if m != nil {
		return m.FromUserId
	}
	return ""
}

func (m *FollowResp) GetTargetUserId() string {
	if m != nil {
		return m.TargetUserId
	}
	return ""
}

func (m *FollowResp) GetFollowedAt() string {
	if m != nil {
		return m.FollowedAt
	}
	return ""
}

func (m *FollowResp) GetBlockNotifications() bool {
	if m != nil {
		return m.BlockNotifications
	}
	return false
}

func (m *FollowResp) GetCursor() string {
	if m != nil {
		return m.Cursor
	}
	return ""
}

type FollowsReq struct {
	UserId               string             `protobuf:"bytes,1,opt,name=user_id,json=userId,proto3" json:"user_id,omitempty"`
	Limit                int32              `protobuf:"varint,3,opt,name=limit,proto3" json:"limit,omitempty"`
	Cursor               string             `protobuf:"bytes,4,opt,name=cursor,proto3" json:"cursor,omitempty"`
	Offset               int32              `protobuf:"varint,5,opt,name=offset,proto3" json:"offset,omitempty"`
	OrderByFollowedAt    FollowsReq_SortDir `protobuf:"varint,6,opt,name=order_by_followed_at,json=orderByFollowedAt,proto3,enum=twitchvxfollowingserviceecs.FollowsReq_SortDir" json:"order_by_followed_at,omitempty"`
	XXX_NoUnkeyedLiteral struct{}           `json:"-"`
	XXX_unrecognized     []byte             `json:"-"`
	XXX_sizecache        int32              `json:"-"`
}

func (m *FollowsReq) Reset()         { *m = FollowsReq{} }
func (m *FollowsReq) String() string { return proto.CompactTextString(m) }
func (*FollowsReq) ProtoMessage()    {}
func (*FollowsReq) Descriptor() ([]byte, []int) {
	return fileDescriptor_09e688a074f959d6, []int{3}
}

func (m *FollowsReq) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_FollowsReq.Unmarshal(m, b)
}
func (m *FollowsReq) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_FollowsReq.Marshal(b, m, deterministic)
}
func (m *FollowsReq) XXX_Merge(src proto.Message) {
	xxx_messageInfo_FollowsReq.Merge(m, src)
}
func (m *FollowsReq) XXX_Size() int {
	return xxx_messageInfo_FollowsReq.Size(m)
}
func (m *FollowsReq) XXX_DiscardUnknown() {
	xxx_messageInfo_FollowsReq.DiscardUnknown(m)
}

var xxx_messageInfo_FollowsReq proto.InternalMessageInfo

func (m *FollowsReq) GetUserId() string {
	if m != nil {
		return m.UserId
	}
	return ""
}

func (m *FollowsReq) GetLimit() int32 {
	if m != nil {
		return m.Limit
	}
	return 0
}

func (m *FollowsReq) GetCursor() string {
	if m != nil {
		return m.Cursor
	}
	return ""
}

func (m *FollowsReq) GetOffset() int32 {
	if m != nil {
		return m.Offset
	}
	return 0
}

func (m *FollowsReq) GetOrderByFollowedAt() FollowsReq_SortDir {
	if m != nil {
		return m.OrderByFollowedAt
	}
	return FollowsReq_ASC
}

type BatchFollowsResp struct {
	Follows              []*FollowResp `protobuf:"bytes,1,rep,name=follows,proto3" json:"follows,omitempty"`
	XXX_NoUnkeyedLiteral struct{}      `json:"-"`
	XXX_unrecognized     []byte        `json:"-"`
	XXX_sizecache        int32         `json:"-"`
}

func (m *BatchFollowsResp) Reset()         { *m = BatchFollowsResp{} }
func (m *BatchFollowsResp) String() string { return proto.CompactTextString(m) }
func (*BatchFollowsResp) ProtoMessage()    {}
func (*BatchFollowsResp) Descriptor() ([]byte, []int) {
	return fileDescriptor_09e688a074f959d6, []int{4}
}

func (m *BatchFollowsResp) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_BatchFollowsResp.Unmarshal(m, b)
}
func (m *BatchFollowsResp) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_BatchFollowsResp.Marshal(b, m, deterministic)
}
func (m *BatchFollowsResp) XXX_Merge(src proto.Message) {
	xxx_messageInfo_BatchFollowsResp.Merge(m, src)
}
func (m *BatchFollowsResp) XXX_Size() int {
	return xxx_messageInfo_BatchFollowsResp.Size(m)
}
func (m *BatchFollowsResp) XXX_DiscardUnknown() {
	xxx_messageInfo_BatchFollowsResp.DiscardUnknown(m)
}

var xxx_messageInfo_BatchFollowsResp proto.InternalMessageInfo

func (m *BatchFollowsResp) GetFollows() []*FollowResp {
	if m != nil {
		return m.Follows
	}
	return nil
}

type FollowsResp struct {
	Follows              []*FollowResp `protobuf:"bytes,1,rep,name=follows,proto3" json:"follows,omitempty"`
	Cursor               string        `protobuf:"bytes,3,opt,name=cursor,proto3" json:"cursor,omitempty"`
	XXX_NoUnkeyedLiteral struct{}      `json:"-"`
	XXX_unrecognized     []byte        `json:"-"`
	XXX_sizecache        int32         `json:"-"`
}

func (m *FollowsResp) Reset()         { *m = FollowsResp{} }
func (m *FollowsResp) String() string { return proto.CompactTextString(m) }
func (*FollowsResp) ProtoMessage()    {}
func (*FollowsResp) Descriptor() ([]byte, []int) {
	return fileDescriptor_09e688a074f959d6, []int{5}
}

func (m *FollowsResp) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_FollowsResp.Unmarshal(m, b)
}
func (m *FollowsResp) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_FollowsResp.Marshal(b, m, deterministic)
}
func (m *FollowsResp) XXX_Merge(src proto.Message) {
	xxx_messageInfo_FollowsResp.Merge(m, src)
}
func (m *FollowsResp) XXX_Size() int {
	return xxx_messageInfo_FollowsResp.Size(m)
}
func (m *FollowsResp) XXX_DiscardUnknown() {
	xxx_messageInfo_FollowsResp.DiscardUnknown(m)
}

var xxx_messageInfo_FollowsResp proto.InternalMessageInfo

func (m *FollowsResp) GetFollows() []*FollowResp {
	if m != nil {
		return m.Follows
	}
	return nil
}

func (m *FollowsResp) GetCursor() string {
	if m != nil {
		return m.Cursor
	}
	return ""
}

type UserIDReq struct {
	UserId               string   `protobuf:"bytes,1,opt,name=user_id,json=userId,proto3" json:"user_id,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *UserIDReq) Reset()         { *m = UserIDReq{} }
func (m *UserIDReq) String() string { return proto.CompactTextString(m) }
func (*UserIDReq) ProtoMessage()    {}
func (*UserIDReq) Descriptor() ([]byte, []int) {
	return fileDescriptor_09e688a074f959d6, []int{6}
}

func (m *UserIDReq) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_UserIDReq.Unmarshal(m, b)
}
func (m *UserIDReq) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_UserIDReq.Marshal(b, m, deterministic)
}
func (m *UserIDReq) XXX_Merge(src proto.Message) {
	xxx_messageInfo_UserIDReq.Merge(m, src)
}
func (m *UserIDReq) XXX_Size() int {
	return xxx_messageInfo_UserIDReq.Size(m)
}
func (m *UserIDReq) XXX_DiscardUnknown() {
	xxx_messageInfo_UserIDReq.DiscardUnknown(m)
}

var xxx_messageInfo_UserIDReq proto.InternalMessageInfo

func (m *UserIDReq) GetUserId() string {
	if m != nil {
		return m.UserId
	}
	return ""
}

type CountResp struct {
	Count                int32    `protobuf:"varint,1,opt,name=count,proto3" json:"count,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *CountResp) Reset()         { *m = CountResp{} }
func (m *CountResp) String() string { return proto.CompactTextString(m) }
func (*CountResp) ProtoMessage()    {}
func (*CountResp) Descriptor() ([]byte, []int) {
	return fileDescriptor_09e688a074f959d6, []int{7}
}

func (m *CountResp) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_CountResp.Unmarshal(m, b)
}
func (m *CountResp) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_CountResp.Marshal(b, m, deterministic)
}
func (m *CountResp) XXX_Merge(src proto.Message) {
	xxx_messageInfo_CountResp.Merge(m, src)
}
func (m *CountResp) XXX_Size() int {
	return xxx_messageInfo_CountResp.Size(m)
}
func (m *CountResp) XXX_DiscardUnknown() {
	xxx_messageInfo_CountResp.DiscardUnknown(m)
}

var xxx_messageInfo_CountResp proto.InternalMessageInfo

func (m *CountResp) GetCount() int32 {
	if m != nil {
		return m.Count
	}
	return 0
}

type UpsertFollowReq struct {
	FromUserId           string   `protobuf:"bytes,1,opt,name=from_user_id,json=fromUserId,proto3" json:"from_user_id,omitempty"`
	TargetUserId         string   `protobuf:"bytes,2,opt,name=target_user_id,json=targetUserId,proto3" json:"target_user_id,omitempty"`
	BlockNotifications   bool     `protobuf:"varint,3,opt,name=block_notifications,json=blockNotifications,proto3" json:"block_notifications,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *UpsertFollowReq) Reset()         { *m = UpsertFollowReq{} }
func (m *UpsertFollowReq) String() string { return proto.CompactTextString(m) }
func (*UpsertFollowReq) ProtoMessage()    {}
func (*UpsertFollowReq) Descriptor() ([]byte, []int) {
	return fileDescriptor_09e688a074f959d6, []int{8}
}

func (m *UpsertFollowReq) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_UpsertFollowReq.Unmarshal(m, b)
}
func (m *UpsertFollowReq) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_UpsertFollowReq.Marshal(b, m, deterministic)
}
func (m *UpsertFollowReq) XXX_Merge(src proto.Message) {
	xxx_messageInfo_UpsertFollowReq.Merge(m, src)
}
func (m *UpsertFollowReq) XXX_Size() int {
	return xxx_messageInfo_UpsertFollowReq.Size(m)
}
func (m *UpsertFollowReq) XXX_DiscardUnknown() {
	xxx_messageInfo_UpsertFollowReq.DiscardUnknown(m)
}

var xxx_messageInfo_UpsertFollowReq proto.InternalMessageInfo

func (m *UpsertFollowReq) GetFromUserId() string {
	if m != nil {
		return m.FromUserId
	}
	return ""
}

func (m *UpsertFollowReq) GetTargetUserId() string {
	if m != nil {
		return m.TargetUserId
	}
	return ""
}

func (m *UpsertFollowReq) GetBlockNotifications() bool {
	if m != nil {
		return m.BlockNotifications
	}
	return false
}

type UserIDsReq struct {
	UserIds              []string `protobuf:"bytes,1,rep,name=user_ids,json=userIds,proto3" json:"user_ids,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *UserIDsReq) Reset()         { *m = UserIDsReq{} }
func (m *UserIDsReq) String() string { return proto.CompactTextString(m) }
func (*UserIDsReq) ProtoMessage()    {}
func (*UserIDsReq) Descriptor() ([]byte, []int) {
	return fileDescriptor_09e688a074f959d6, []int{9}
}

func (m *UserIDsReq) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_UserIDsReq.Unmarshal(m, b)
}
func (m *UserIDsReq) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_UserIDsReq.Marshal(b, m, deterministic)
}
func (m *UserIDsReq) XXX_Merge(src proto.Message) {
	xxx_messageInfo_UserIDsReq.Merge(m, src)
}
func (m *UserIDsReq) XXX_Size() int {
	return xxx_messageInfo_UserIDsReq.Size(m)
}
func (m *UserIDsReq) XXX_DiscardUnknown() {
	xxx_messageInfo_UserIDsReq.DiscardUnknown(m)
}

var xxx_messageInfo_UserIDsReq proto.InternalMessageInfo

func (m *UserIDsReq) GetUserIds() []string {
	if m != nil {
		return m.UserIds
	}
	return nil
}

type BatchCountResp struct {
	Count                int32    `protobuf:"varint,1,opt,name=count,proto3" json:"count,omitempty"`
	UserId               string   `protobuf:"bytes,2,opt,name=user_id,json=userId,proto3" json:"user_id,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *BatchCountResp) Reset()         { *m = BatchCountResp{} }
func (m *BatchCountResp) String() string { return proto.CompactTextString(m) }
func (*BatchCountResp) ProtoMessage()    {}
func (*BatchCountResp) Descriptor() ([]byte, []int) {
	return fileDescriptor_09e688a074f959d6, []int{10}
}

func (m *BatchCountResp) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_BatchCountResp.Unmarshal(m, b)
}
func (m *BatchCountResp) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_BatchCountResp.Marshal(b, m, deterministic)
}
func (m *BatchCountResp) XXX_Merge(src proto.Message) {
	xxx_messageInfo_BatchCountResp.Merge(m, src)
}
func (m *BatchCountResp) XXX_Size() int {
	return xxx_messageInfo_BatchCountResp.Size(m)
}
func (m *BatchCountResp) XXX_DiscardUnknown() {
	xxx_messageInfo_BatchCountResp.DiscardUnknown(m)
}

var xxx_messageInfo_BatchCountResp proto.InternalMessageInfo

func (m *BatchCountResp) GetCount() int32 {
	if m != nil {
		return m.Count
	}
	return 0
}

func (m *BatchCountResp) GetUserId() string {
	if m != nil {
		return m.UserId
	}
	return ""
}

type BatchCountsResp struct {
	Counts               []*BatchCountResp `protobuf:"bytes,1,rep,name=counts,proto3" json:"counts,omitempty"`
	XXX_NoUnkeyedLiteral struct{}          `json:"-"`
	XXX_unrecognized     []byte            `json:"-"`
	XXX_sizecache        int32             `json:"-"`
}

func (m *BatchCountsResp) Reset()         { *m = BatchCountsResp{} }
func (m *BatchCountsResp) String() string { return proto.CompactTextString(m) }
func (*BatchCountsResp) ProtoMessage()    {}
func (*BatchCountsResp) Descriptor() ([]byte, []int) {
	return fileDescriptor_09e688a074f959d6, []int{11}
}

func (m *BatchCountsResp) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_BatchCountsResp.Unmarshal(m, b)
}
func (m *BatchCountsResp) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_BatchCountsResp.Marshal(b, m, deterministic)
}
func (m *BatchCountsResp) XXX_Merge(src proto.Message) {
	xxx_messageInfo_BatchCountsResp.Merge(m, src)
}
func (m *BatchCountsResp) XXX_Size() int {
	return xxx_messageInfo_BatchCountsResp.Size(m)
}
func (m *BatchCountsResp) XXX_DiscardUnknown() {
	xxx_messageInfo_BatchCountsResp.DiscardUnknown(m)
}

var xxx_messageInfo_BatchCountsResp proto.InternalMessageInfo

func (m *BatchCountsResp) GetCounts() []*BatchCountResp {
	if m != nil {
		return m.Counts
	}
	return nil
}

type BatchUpdateReq struct {
	UserId               string   `protobuf:"bytes,1,opt,name=user_id,json=userId,proto3" json:"user_id,omitempty"`
	Limit                int32    `protobuf:"varint,2,opt,name=limit,proto3" json:"limit,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *BatchUpdateReq) Reset()         { *m = BatchUpdateReq{} }
func (m *BatchUpdateReq) String() string { return proto.CompactTextString(m) }
func (*BatchUpdateReq) ProtoMessage()    {}
func (*BatchUpdateReq) Descriptor() ([]byte, []int) {
	return fileDescriptor_09e688a074f959d6, []int{12}
}

func (m *BatchUpdateReq) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_BatchUpdateReq.Unmarshal(m, b)
}
func (m *BatchUpdateReq) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_BatchUpdateReq.Marshal(b, m, deterministic)
}
func (m *BatchUpdateReq) XXX_Merge(src proto.Message) {
	xxx_messageInfo_BatchUpdateReq.Merge(m, src)
}
func (m *BatchUpdateReq) XXX_Size() int {
	return xxx_messageInfo_BatchUpdateReq.Size(m)
}
func (m *BatchUpdateReq) XXX_DiscardUnknown() {
	xxx_messageInfo_BatchUpdateReq.DiscardUnknown(m)
}

var xxx_messageInfo_BatchUpdateReq proto.InternalMessageInfo

func (m *BatchUpdateReq) GetUserId() string {
	if m != nil {
		return m.UserId
	}
	return ""
}

func (m *BatchUpdateReq) GetLimit() int32 {
	if m != nil {
		return m.Limit
	}
	return 0
}

type BatchUpdateResp struct {
	More                 bool     `protobuf:"varint,1,opt,name=more,proto3" json:"more,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *BatchUpdateResp) Reset()         { *m = BatchUpdateResp{} }
func (m *BatchUpdateResp) String() string { return proto.CompactTextString(m) }
func (*BatchUpdateResp) ProtoMessage()    {}
func (*BatchUpdateResp) Descriptor() ([]byte, []int) {
	return fileDescriptor_09e688a074f959d6, []int{13}
}

func (m *BatchUpdateResp) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_BatchUpdateResp.Unmarshal(m, b)
}
func (m *BatchUpdateResp) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_BatchUpdateResp.Marshal(b, m, deterministic)
}
func (m *BatchUpdateResp) XXX_Merge(src proto.Message) {
	xxx_messageInfo_BatchUpdateResp.Merge(m, src)
}
func (m *BatchUpdateResp) XXX_Size() int {
	return xxx_messageInfo_BatchUpdateResp.Size(m)
}
func (m *BatchUpdateResp) XXX_DiscardUnknown() {
	xxx_messageInfo_BatchUpdateResp.DiscardUnknown(m)
}

var xxx_messageInfo_BatchUpdateResp proto.InternalMessageInfo

func (m *BatchUpdateResp) GetMore() bool {
	if m != nil {
		return m.More
	}
	return false
}

func init() {
	proto.RegisterEnum("twitchvxfollowingserviceecs.FollowsReq_SortDir", FollowsReq_SortDir_name, FollowsReq_SortDir_value)
	proto.RegisterType((*FollowReq)(nil), "twitchvxfollowingserviceecs.FollowReq")
	proto.RegisterType((*BatchFollowsReq)(nil), "twitchvxfollowingserviceecs.BatchFollowsReq")
	proto.RegisterType((*FollowResp)(nil), "twitchvxfollowingserviceecs.FollowResp")
	proto.RegisterType((*FollowsReq)(nil), "twitchvxfollowingserviceecs.FollowsReq")
	proto.RegisterType((*BatchFollowsResp)(nil), "twitchvxfollowingserviceecs.BatchFollowsResp")
	proto.RegisterType((*FollowsResp)(nil), "twitchvxfollowingserviceecs.FollowsResp")
	proto.RegisterType((*UserIDReq)(nil), "twitchvxfollowingserviceecs.UserIDReq")
	proto.RegisterType((*CountResp)(nil), "twitchvxfollowingserviceecs.CountResp")
	proto.RegisterType((*UpsertFollowReq)(nil), "twitchvxfollowingserviceecs.UpsertFollowReq")
	proto.RegisterType((*UserIDsReq)(nil), "twitchvxfollowingserviceecs.UserIDsReq")
	proto.RegisterType((*BatchCountResp)(nil), "twitchvxfollowingserviceecs.BatchCountResp")
	proto.RegisterType((*BatchCountsResp)(nil), "twitchvxfollowingserviceecs.BatchCountsResp")
	proto.RegisterType((*BatchUpdateReq)(nil), "twitchvxfollowingserviceecs.BatchUpdateReq")
	proto.RegisterType((*BatchUpdateResp)(nil), "twitchvxfollowingserviceecs.BatchUpdateResp")
}

func init() {
	proto.RegisterFile("code.justin.tv/amzn/TwitchVXFollowingServiceECSTwirp/twirp.proto", fileDescriptor_09e688a074f959d6)
}

var fileDescriptor_09e688a074f959d6 = []byte{
	// 806 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xbc, 0x57, 0xed, 0x6e, 0x12, 0x4d,
	0x14, 0xee, 0xf2, 0xcd, 0x81, 0x42, 0xdf, 0x29, 0xaf, 0x22, 0x35, 0x11, 0x37, 0xb5, 0x25, 0xb1,
	0x42, 0x52, 0x8d, 0x7f, 0xb5, 0x85, 0x7e, 0x98, 0x18, 0x7f, 0x2c, 0xa5, 0x31, 0x9a, 0x48, 0x61,
	0x77, 0xa0, 0xa3, 0xb0, 0xb3, 0x9d, 0x19, 0x8a, 0x78, 0x09, 0x5e, 0x94, 0x77, 0xe3, 0x2f, 0x6f,
	0xc2, 0x30, 0xfb, 0xc1, 0xd2, 0xa6, 0x74, 0x6b, 0xd8, 0xfe, 0x69, 0x3a, 0x33, 0xe7, 0x9c, 0xe7,
	0x7c, 0x3c, 0xcf, 0x21, 0x0b, 0x6f, 0x75, 0x6a, 0xe0, 0xea, 0xd7, 0x11, 0x17, 0xc4, 0xac, 0x8a,
	0xcb, 0x5a, 0x67, 0xf8, 0xc3, 0xac, 0x9d, 0x8c, 0x89, 0xd0, 0xcf, 0x4f, 0x3f, 0x1e, 0xd2, 0xc1,
	0x80, 0x8e, 0x89, 0xd9, 0x6f, 0x62, 0x76, 0x49, 0x74, 0x7c, 0x50, 0x6f, 0x9e, 0x8c, 0x09, 0xb3,
	0x6a, 0x62, 0xfa, 0xb7, 0x6a, 0x31, 0x2a, 0x28, 0xda, 0x10, 0xd2, 0xfa, 0xf2, 0x7b, 0xcf, 0xb5,
	0xe6, 0xb6, 0x35, 0xd6, 0x79, 0x69, 0xa3, 0x4f, 0x69, 0x7f, 0x80, 0x6b, 0xd2, 0xb4, 0x3b, 0xea,
	0xd5, 0xf0, 0xd0, 0x12, 0x13, 0xdb, 0x53, 0x6d, 0x42, 0xda, 0x46, 0xd0, 0xf0, 0x05, 0x2a, 0x43,
	0xb6, 0xc7, 0xe8, 0xb0, 0x3d, 0xe2, 0x98, 0xb5, 0x89, 0x51, 0x54, 0xca, 0x4a, 0x25, 0xad, 0xc1,
	0xf4, 0xae, 0xc5, 0x31, 0x7b, 0x67, 0xa0, 0x4d, 0xc8, 0x89, 0x0e, 0xeb, 0x63, 0xe1, 0xd9, 0x44,
	0xa4, 0x4d, 0xd6, 0xbe, 0xb5, 0xad, 0xd4, 0xcf, 0x90, 0xdf, 0xef, 0x08, 0xfd, 0xdc, 0x8e, 0xcc,
	0x83, 0x85, 0xde, 0x82, 0xfc, 0x7c, 0x68, 0x5e, 0x8c, 0x94, 0xa3, 0x95, 0xb4, 0xb6, 0xea, 0x8f,
	0xcd, 0xd5, 0x5f, 0x0a, 0x80, 0x9b, 0x32, 0xb7, 0x96, 0x95, 0x33, 0x7a, 0x02, 0x19, 0xbb, 0x79,
	0xd8, 0x68, 0x77, 0x44, 0x31, 0xea, 0x84, 0x71, 0xae, 0xf6, 0x04, 0xaa, 0xc1, 0x7a, 0x77, 0x40,
	0xf5, 0x6f, 0x6d, 0x93, 0x0a, 0xd2, 0x23, 0x7a, 0x47, 0x10, 0x6a, 0xf2, 0x62, 0xac, 0xac, 0x54,
	0x52, 0x1a, 0x92, 0x4f, 0x1f, 0xfc, 0x2f, 0xe8, 0x01, 0x24, 0xf4, 0x11, 0xe3, 0x94, 0x15, 0xe3,
	0x32, 0x98, 0x73, 0x52, 0xff, 0x78, 0x05, 0xc8, 0xce, 0x3c, 0x84, 0xe4, 0x7c, 0xee, 0x89, 0x91,
	0x9d, 0x51, 0x01, 0xe2, 0x03, 0x32, 0x24, 0x76, 0x2e, 0x71, 0xcd, 0x3e, 0xf8, 0xa2, 0xc6, 0xfc,
	0x51, 0xa7, 0xf7, 0xb4, 0xd7, 0xe3, 0x58, 0x48, 0xb4, 0xb8, 0xe6, 0x9c, 0xd0, 0x19, 0x14, 0x28,
	0x33, 0x30, 0x6b, 0x77, 0x27, 0x6d, 0x7f, 0x81, 0x89, 0xb2, 0x52, 0xc9, 0xed, 0xd6, 0xaa, 0x0b,
	0x98, 0x53, 0x9d, 0x65, 0x59, 0x6d, 0x52, 0x26, 0x1a, 0x84, 0x69, 0xff, 0xc9, 0x60, 0xfb, 0x93,
	0x43, 0xaf, 0x31, 0xea, 0x63, 0x48, 0x3a, 0xaf, 0x28, 0x09, 0xd1, 0xbd, 0x66, 0x7d, 0x6d, 0x05,
	0xa5, 0x20, 0xd6, 0x38, 0x68, 0xd6, 0xd7, 0x14, 0xb5, 0x05, 0x6b, 0xf3, 0x5c, 0xe0, 0x16, 0xda,
	0x83, 0xa4, 0x0d, 0xc7, 0x8b, 0x4a, 0x39, 0x5a, 0xc9, 0xec, 0x6e, 0x07, 0x48, 0x63, 0xea, 0xa9,
	0xb9, 0x7e, 0xea, 0x39, 0x64, 0x96, 0x1b, 0xd1, 0xd7, 0xd8, 0xe8, 0xdc, 0xb8, 0x36, 0x21, 0x2d,
	0x29, 0xd2, 0x58, 0x34, 0x2c, 0xf5, 0x29, 0xa4, 0xeb, 0x74, 0x64, 0x0a, 0x99, 0x4d, 0x01, 0xe2,
	0xfa, 0xf4, 0x20, 0x6d, 0xe2, 0x9a, 0x7d, 0x50, 0x7f, 0x2a, 0x90, 0x6f, 0x59, 0x1c, 0x33, 0xb1,
	0x74, 0xc5, 0xdd, 0x44, 0xce, 0xe8, 0x4d, 0xe4, 0x54, 0xb7, 0x01, 0xec, 0xaa, 0x24, 0x07, 0x1f,
	0x41, 0xca, 0x13, 0x9d, 0x22, 0x45, 0x97, 0x1c, 0x39, 0x72, 0x7b, 0x03, 0x39, 0x39, 0xbf, 0x5b,
	0xaa, 0xf3, 0x77, 0x26, 0x32, 0xd7, 0x99, 0x53, 0x67, 0x19, 0xc8, 0x00, 0xf6, 0xb4, 0xea, 0x90,
	0x90, 0x4e, 0xee, 0xb0, 0x9e, 0x2f, 0x1c, 0xd6, 0x3c, 0xbc, 0xe6, 0xb8, 0x7a, 0x89, 0xb5, 0x2c,
	0xa3, 0x23, 0x70, 0x30, 0x25, 0x45, 0x7c, 0x4a, 0x52, 0x9f, 0x39, 0x89, 0xb9, 0x01, 0xb8, 0x85,
	0x10, 0xc4, 0x86, 0x94, 0x61, 0xe9, 0x9e, 0xd2, 0xe4, 0xff, 0xbb, 0xbf, 0x73, 0xb0, 0xb1, 0x60,
	0x19, 0xa3, 0x2f, 0x90, 0x3e, 0xc2, 0xce, 0x48, 0xd1, 0x56, 0x20, 0xda, 0x5d, 0x94, 0x82, 0xd2,
	0x53, 0x5d, 0x41, 0x16, 0xe4, 0x8f, 0xb0, 0xf0, 0x6b, 0x08, 0xed, 0xdc, 0xde, 0xaf, 0x99, 0x74,
	0x4b, 0x2f, 0xee, 0x60, 0x2d, 0x11, 0xbb, 0x90, 0x79, 0x4f, 0xb8, 0x70, 0xd1, 0xb6, 0x03, 0xee,
	0x88, 0x52, 0x25, 0x98, 0xa1, 0xc4, 0x30, 0x60, 0x75, 0x86, 0x81, 0x59, 0x48, 0x28, 0x67, 0x90,
	0x95, 0xc4, 0x71, 0x4b, 0x59, 0x3c, 0x1e, 0x4f, 0xe6, 0xa5, 0xc5, 0x76, 0x1e, 0x17, 0x65, 0xaf,
	0x72, 0x3e, 0x84, 0x69, 0x21, 0xcb, 0xc7, 0x30, 0x61, 0x7d, 0xa6, 0x81, 0xa0, 0x1d, 0x9b, 0xa9,
	0xbb, 0xb4, 0x13, 0x50, 0x5e, 0x6e, 0xd7, 0xfa, 0x90, 0xf5, 0xef, 0xa9, 0x5b, 0xe8, 0x76, 0x65,
	0xa5, 0xdd, 0x85, 0xda, 0x1d, 0xc8, 0x36, 0xf0, 0x00, 0x0b, 0x1c, 0x9e, 0x7a, 0xa8, 0xf3, 0xf3,
	0x73, 0x4c, 0x0c, 0xec, 0xb2, 0x20, 0xc0, 0xba, 0xf1, 0x96, 0x4a, 0x90, 0xe6, 0xcd, 0x16, 0x88,
	0xba, 0x82, 0x2e, 0x00, 0x5d, 0x01, 0x9c, 0xce, 0x2a, 0x54, 0x48, 0xe6, 0xf0, 0x43, 0xc3, 0x5c,
	0x50, 0x76, 0x3f, 0x65, 0x0a, 0xf8, 0xff, 0x3a, 0x66, 0xe8, 0x95, 0xba, 0xcd, 0xf5, 0xb3, 0x26,
	0x64, 0x48, 0x0e, 0x85, 0x6b, 0x90, 0xa1, 0xd7, 0x39, 0x86, 0xa2, 0x0f, 0xf4, 0x98, 0x18, 0x06,
	0x36, 0xef, 0xa5, 0xda, 0x09, 0x94, 0x6e, 0x00, 0x0e, 0xbb, 0xe6, 0xfd, 0xd7, 0x9f, 0x5e, 0xfd,
	0xcb, 0x77, 0x50, 0x37, 0x21, 0x3f, 0x64, 0x5e, 0xfe, 0x0d, 0x00, 0x00, 0xff, 0xff, 0x9f, 0xb1,
	0xee, 0x41, 0x46, 0x0d, 0x00, 0x00,
}
