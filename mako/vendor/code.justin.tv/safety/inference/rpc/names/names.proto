// names.proto
syntax = "proto3";

package code.justin.tv.safety.inference;
option go_package = "rpc/names";

// names service is used to get a confidence value for is a name is acceptable
// or not
service names {
  // AcceptableNames accepts a list of usernames and returns a list of
  // confidence values for each name, between 0 and 1 for how confident
  // the system is that the username is acceptable.
  // 0 = acceptable, 1 = unacceptable
  rpc AcceptableNames(AcceptableNamesRequest) returns (AcceptableNamesResponse);
}

//------------------------------------------------------------------------------
// RPC Requests/Responses
//------------------------------------------------------------------------------

// AcceptableNamesRequest is the request body of the AcceptableNames method
message AcceptableNamesRequest {
  repeated string usernames = 1; // array of usernames to test
}

// AcceptableNamesReasonsResponse is the response body of the AcceptableNames method
message AcceptableNamesResponse {
  repeated float confidence = 1; // array of confidence levels for each username
  repeated OffensiveTier.Tier tier = 2;
  repeated float tierMinThreshold = 3;
  repeated float tierMaxThreshold = 4;
}

message OffensiveTier {
  enum Tier {
    UNKNOWN = 0; // Unknown likelihood; Model was unable to determine the likelihood.
    VERY_UNLIKELY = 1; // It is very unlikely that the name is offensive
    UNLIKELY = 2; // It is unlikely that the name is offensive
    POSSIBLE = 3; // It is possible that the name is offensive
    LIKELY = 4; // It is likely that the name is offensive
    VERY_LIKELY = 5; //It is very likely that the name is offensive
  }
}
