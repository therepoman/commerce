import groovy.json.JsonSlurper
import groovy.json.JsonOutput

properties([
    parameters([
        string(name: 'GIT_COMMIT', defaultValue: '', description: 'Git Sha', ),
        string(name: 'ENVIRONMENT', defaultValue: '', description: 'Environment', ),
        string(name: 'SKADI_ID', defaultValue: '', description: 'Environment', )
    ])
])

pipeline {
    agent any

    options {
        disableConcurrentBuilds()
        timeout(time: 20, unit: 'MINUTES')
        ansiColor('xterm')
        timestamps()
    }

    environment {
        REGION      = "us-west-2"
        CLUSTER     = "sonar"
        ENVIRONMENT = "${params.ENVIRONMENT.split('-')[0]}"
        COMPONENT   = "${params.ENVIRONMENT.split('-')[1]}"
        SHA         = "${params.GIT_COMMIT}"
    }

    stages {
        stage('Deploy to Staging') {
            when {
                expression { ENVIRONMENT == 'staging' && COMPONENT == 'service' }
            }

            steps {
                withCredentials([
                    string(credentialsId: 'mako-devo-tcs-access-key', variable: 'AWS_ACCESS_KEY_ID'),
                    string(credentialsId: 'mako-devo-tcs-secret-key', variable: 'AWS_SECRET_ACCESS_KEY'),
                    file(credentialsId: 'aws_config', variable: 'AWS_CONFIG_FILE')
                ]) {
                    sh """
                            docker run \
                             -e AWS_ACCESS_KEY=${AWS_ACCESS_KEY_ID} \
                             -e AWS_SECRET_KEY=${AWS_SECRET_ACCESS_KEY} \
                             docker-registry.internal.justin.tv/subs/deployer:latest \
                                -service=mako \
                                -task=mako \
                                -cluster=${CLUSTER} \
                                -region=${REGION} \
                                -tag=${params.GIT_COMMIT}
                        """
                }
            }
        }

        stage("Deploy Lambda Staging") {
            when {
                expression { ENVIRONMENT == 'staging' && COMPONENT != 'service'}
            }

            steps {
                withCredentials([
                    string(credentialsId: 'mako-devo-tcs-access-key', variable: 'AWS_ACCESS_KEY_ID'),
                    string(credentialsId: 'mako-devo-tcs-secret-key', variable: 'AWS_SECRET_ACCESS_KEY'),
                    file(credentialsId: 'aws_config', variable: 'AWS_CONFIG_FILE')
                ]) {
                    sh "./scripts/deploy-lambda.sh ${COMPONENT} twitch-mako-staging-lambdas ${SHA}"
                }
            }
        }

        stage("Deploy to Canary") {
            when {
                expression { (ENVIRONMENT == 'canary' || ENVIRONMENT == 'production') && COMPONENT == 'service' }
            }

            steps {
                withCredentials([
                    string(credentialsId: 'mako-prod-tcs-access-key', variable: 'AWS_ACCESS_KEY_ID'),
                    string(credentialsId: 'mako-prod-tcs-secret-key', variable: 'AWS_SECRET_ACCESS_KEY'),
                    file(credentialsId: 'aws_config', variable: 'AWS_CONFIG_FILE')
                ]) {
                    sh """
                            docker run \
                             -e AWS_ACCESS_KEY=${AWS_ACCESS_KEY_ID} \
                             -e AWS_SECRET_KEY=${AWS_SECRET_ACCESS_KEY} \
                             docker-registry.internal.justin.tv/subs/deployer:latest \
                                -service=mako-canary \
                                -task=mako-canary \
                                -cluster=${CLUSTER} \
                                -region=${REGION} \
                                -tag=${params.GIT_COMMIT}
                        """
                }
            }
        }

        stage("Deploy to Inc") {
            when {
                expression { ENVIRONMENT == 'inc' && COMPONENT == 'service' }
            }

            steps {
                withCredentials([
                    string(credentialsId: 'mako-prod-tcs-access-key', variable: 'AWS_ACCESS_KEY_ID'),
                    string(credentialsId: 'mako-prod-tcs-secret-key', variable: 'AWS_SECRET_ACCESS_KEY'),
                    file(credentialsId: 'aws_config', variable: 'AWS_CONFIG_FILE')
                ]) {
                    sh """
                            docker run \
                             -e AWS_ACCESS_KEY=${AWS_ACCESS_KEY_ID} \
                             -e AWS_SECRET_KEY=${AWS_SECRET_ACCESS_KEY} \
                             docker-registry.internal.justin.tv/subs/deployer:latest \
                                -service=mako-inc-canary \
                                -task=mako-inc-canary \
                                -cluster=${CLUSTER} \
                                -region=${REGION} \
                                -tag=${params.GIT_COMMIT}
                        """
                    sh """
                            docker run \
                             -e AWS_ACCESS_KEY=${AWS_ACCESS_KEY_ID} \
                             -e AWS_SECRET_KEY=${AWS_SECRET_ACCESS_KEY} \
                             docker-registry.internal.justin.tv/subs/deployer:latest \
                                -service=mako-inc \
                                -task=mako-inc \
                                -cluster=${CLUSTER} \
                                -region=${REGION} \
                                -tag=${params.GIT_COMMIT}
                        """
                }
            }
        }

        stage("Deploy to Production") {
            when {
                expression { ENVIRONMENT == 'production' && COMPONENT == 'service' }
            }

            steps {
                withCredentials([
                    string(credentialsId: 'mako-prod-tcs-access-key', variable: 'AWS_ACCESS_KEY_ID'),
                    string(credentialsId: 'mako-prod-tcs-secret-key', variable: 'AWS_SECRET_ACCESS_KEY'),
                    file(credentialsId: 'aws_config', variable: 'AWS_CONFIG_FILE')
                ]) {
                    sh """
                            docker run \
                             -e AWS_ACCESS_KEY=${AWS_ACCESS_KEY_ID} \
                             -e AWS_SECRET_KEY=${AWS_SECRET_ACCESS_KEY} \
                             docker-registry.internal.justin.tv/subs/deployer:latest \
                                -service=mako \
                                -task=mako \
                                -cluster=${CLUSTER} \
                                -region=${REGION} \
                                -tag=${params.GIT_COMMIT}
                        """
                }
            }
        }
        stage("Deploy Lambda Production") {
            when {
                expression { ENVIRONMENT == 'production' && COMPONENT != 'service'}
            }

            steps {
                withCredentials([
                    string(credentialsId: 'mako-prod-tcs-access-key', variable: 'AWS_ACCESS_KEY_ID'),
                    string(credentialsId: 'mako-prod-tcs-secret-key', variable: 'AWS_SECRET_ACCESS_KEY'),
                    file(credentialsId: 'aws_config', variable: 'AWS_CONFIG_FILE')
                ]) {
                    sh "./scripts/deploy-lambda.sh ${COMPONENT} twitch-mako-production-lambdas ${SHA}"
                }
            }
        }
    }
}