# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/aws" {
  version     = "2.62.0"
  constraints = "~> 2.62.0"
  hashes = [
    "h1:lXmrtNsBKUPMdu4lvhsbW+m35GpUxRPAJ5Jf1ulW56E=",
    "zh:19ca5001841cccbe5581098349178033ad9ef2833218f05641284b538a282f6a",
    "zh:2a50345ee497dad4b52e6c3c2ff23731d12b9d7df625ea5658e228d1538acbeb",
    "zh:35447209ebfca371e0f8af8933accfc5bcae0aaec960a376e02599147b6e878f",
    "zh:53d12f59deb6bcda669f106d0e4e3940624d1bf9b710e79745268b71706cf6ca",
    "zh:66655e45f815d61356be861d6bbac3edbae8660654ec7dfce1457343abcb2269",
    "zh:8731c73e91ac3c2f41db3384b4a5454401b6c1ca1545b4f34af3716c1b172529",
    "zh:9211a9b6afea780267a2ec8c1a132e01fc5f9f2a249dd7cbf2925f80e167334e",
    "zh:a4878b95a26bf520458cae5c848be2b1f25e30dd3c4c41751f85379eca0f20f3",
    "zh:b9508b1f9c6493d0bcaf304f1df61368f51a9d23830d6d777b87fa0f7d6cd7c3",
    "zh:b95a36f5fadbbe1f66ce695f1b11a073a9055a388c8bead4b29e78c9cc02765b",
    "zh:de10d1887e571cfba8cbd0e146e3f9fb14b618847eaa9ef9c281f04fc5d2e0a8",
    "zh:deff3b5939387905468954de344cb653b6b6c759209f26d96e0b36beac2ebc96",
  ]
}

provider "registry.terraform.io/hashicorp/http" {
  version = "2.1.0"
  hashes = [
    "h1:GYoVrTtiSAE3AlP1fad3fFmHoPaXAPhm/DJyMcVCwZA=",
    "zh:03d82dc0887d755b8406697b1d27506bc9f86f93b3e9b4d26e0679d96b802826",
    "zh:0704d02926393ddc0cfad0b87c3d51eafeeae5f9e27cc71e193c141079244a22",
    "zh:095ea350ea94973e043dad2394f10bca4a4bf41be775ba59d19961d39141d150",
    "zh:0b71ac44e87d6964ace82979fc3cbb09eb876ed8f954449481bcaa969ba29cb7",
    "zh:0e255a170db598bd1142c396cefc59712ad6d4e1b0e08a840356a371e7b73bc4",
    "zh:67c8091cfad226218c472c04881edf236db8f2dc149dc5ada878a1cd3c1de171",
    "zh:75df05e25d14b5101d4bc6624ac4a01bb17af0263c9e8a740e739f8938b86ee3",
    "zh:b4e36b2c4f33fdc44bf55fa1c9bb6864b5b77822f444bd56f0be7e9476674d0e",
    "zh:b9b36b01d2ec4771838743517bc5f24ea27976634987c6d5529ac4223e44365d",
    "zh:ca264a916e42e221fddb98d640148b12e42116046454b39ede99a77fc52f59f4",
    "zh:fe373b2fb2cc94777a91ecd7ac5372e699748c455f44f6ea27e494de9e5e6f92",
  ]
}

provider "registry.terraform.io/hashicorp/template" {
  version     = "2.2.0"
  constraints = "2.2.0"
  hashes = [
    "h1:0wlehNaxBX7GJQnPfQwTNvvAf38Jm0Nv7ssKGMaG6Og=",
    "zh:01702196f0a0492ec07917db7aaa595843d8f171dc195f4c988d2ffca2a06386",
    "zh:09aae3da826ba3d7df69efeb25d146a1de0d03e951d35019a0f80e4f58c89b53",
    "zh:09ba83c0625b6fe0a954da6fbd0c355ac0b7f07f86c91a2a97849140fea49603",
    "zh:0e3a6c8e16f17f19010accd0844187d524580d9fdb0731f675ffcf4afba03d16",
    "zh:45f2c594b6f2f34ea663704cc72048b212fe7d16fb4cfd959365fa997228a776",
    "zh:77ea3e5a0446784d77114b5e851c970a3dde1e08fa6de38210b8385d7605d451",
    "zh:8a154388f3708e3df5a69122a23bdfaf760a523788a5081976b3d5616f7d30ae",
    "zh:992843002f2db5a11e626b3fc23dc0c87ad3729b3b3cff08e32ffb3df97edbde",
    "zh:ad906f4cebd3ec5e43d5cd6dc8f4c5c9cc3b33d2243c89c5fc18f97f7277b51d",
    "zh:c979425ddb256511137ecd093e23283234da0154b7fa8b21c2687182d9aea8b2",
  ]
}
