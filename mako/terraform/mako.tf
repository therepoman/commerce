// NOTE: this created in the deprecated terraform stack - see: terraform/modules/sns/main.tf
// Here we just import the existing topic so we don't have to hard-code the ARN
data "aws_sns_topic" "pagerduty" {
  name = "subs-pagerduty"
}

module "mako" {
  source = "./mako"

  tag = "latest"

  dns                  = "mako-${var.environment}.internal.justin.tv"
  cluster              = var.cluster
  security_group       = var.security_group
  region               = var.region
  vpc_id               = var.vpc_id
  environment          = var.environment
  subnets              = var.subnets
  aws_profile          = var.aws_profile
  pager_duty_sns_arn   = data.aws_sns_topic.pagerduty.arn
  sandstorm_account_id = var.sandstorm_account_id
  capacity_planning_arn = var.capacity_planning_arn
  upload_service_arn = var.upload_service_arn
}

