// Uncomment when we migrate to use remote state.
//resource "aws_s3_bucket" "terraform-s3-bucket" {
//  acl    = "private"
//  bucket = "${var.aws_profile}-tf"
//
//  versioning {
//    enabled = true
//  }
//
//  lifecycle {
//    prevent_destroy = true
//  }
//
//  tags {
//    Name        = "S3 Remote Terraform State Store"
//    Service     = "mako"
//    Environment = var.environment
//  }
//}
