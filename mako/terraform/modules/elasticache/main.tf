resource "aws_elasticache_subnet_group" "redis_subnet_group" {
  name       = "${var.env}-cache-subnet"
  subnet_ids = split(",", var.private_subnets)

  lifecycle {
    prevent_destroy = true
  }
}

resource "aws_elasticache_replication_group" "mako_redis" {
  replication_group_id          = "${var.env}-mako-redis"
  replication_group_description = "mako redis cache v2"
  node_type                     = var.redis_instance_size
  port                          = 6379
  parameter_group_name          = "default.redis3.2.cluster.on"
  automatic_failover_enabled    = true
  subnet_group_name             = aws_elasticache_subnet_group.redis_subnet_group.name
  security_group_ids            = [var.sg_id]

  cluster_mode {
    replicas_per_node_group = var.redis_replicas_per_group
    num_node_groups         = var.redis_groups
  }

  lifecycle {
    prevent_destroy = true
  }
}
