variable "aws_profile" {
  description = "AWS profile used to create resources"
}

variable "env" {
  description = "Whether it's production, dev, etc"
}

variable "aws_region" {
  description = "AWS region to operate in."
  default     = "us-west-2"
}

variable "private_subnets" {
  description = "Comma-separated list of private subnets"
  type = string
}

variable "sg_id" {
  description = "Id of the security group"
}

variable "redis_instance_size" {
  description = "type of instance to use for the redis fleet"
  type        = string
}

variable "redis_groups" {
  type = string
}

variable "redis_replicas_per_group" {
  type = string
}
