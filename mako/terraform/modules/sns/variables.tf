# Beanstalk general variables
variable "aws_profile" {
  description = "AWS profile used to create resources"
}

variable "aws_region" {
  description = "AWS region to operate in."
  default     = "us-west-2"
}

variable "env" {
  description = "The AWS environment. Must begin with one of the following:  dev (development),  stage (staging), or  prod (production)."
  default     = "dev"
}

variable "prefix_update_sns_topic" {
  description = "The SNS topic ARN that Subscription service publishes to when emoticon prefix is updated."
}
