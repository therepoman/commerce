resource "aws_sns_topic" "events_sns" {
  name = "${var.env}-mako-events"

  delivery_policy = <<EOF
  {
    "http": {
      "defaultHealthyRetryPolicy": {
        "minDelayTarget": 1,
        "maxDelayTarget": 2,
        "numRetries": 5,
        "numMaxDelayRetries": 0,
        "numNoDelayRetries": 3,
        "numMinDelayRetries": 0,
        "backoffFunction": "linear"
      },
      "disableSubscriptionOverrides": false,
      "defaultThrottlePolicy": {
        "maxReceivesPerSecond": 2000
      }
    }
  }
  EOF
}

resource "aws_sqs_queue" "events_dlq" {
  name = "${var.env}-mako-events-dlq"
}

resource "aws_sqs_queue" "events_sqs" {
  name           = "${var.env}-mako-events-sqs"
  redrive_policy = "{\"deadLetterTargetArn\":\"${aws_sqs_queue.events_dlq.arn}\",\"maxReceiveCount\":7}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "SNSToSQSPolicy",
      "Effect": "Allow",
      "Principal": "*",
      "Action": "SQS:SendMessage",
      "Resource": "*",
      "Condition": {
        "ArnEquals": {
          "aws:SourceArn": "${aws_sns_topic.events_sns.arn}"
        }
      }
    }
  ]
}
EOF
}

resource "aws_sns_topic_subscription" "events_subscription" {
  topic_arn            = aws_sns_topic.events_sns.arn
  protocol             = "sqs"
  endpoint             = aws_sqs_queue.events_sqs.arn
  raw_message_delivery = true
}

resource "aws_sqs_queue" "prefix_updates_dlq" {
  name = "${var.env}-prefix-updates-dlq"
}

resource "aws_sqs_queue" "prefix_updates_sqs" {
  name           = "${var.env}-prefix-updates-sqs"
  redrive_policy = "{\"deadLetterTargetArn\":\"${aws_sqs_queue.prefix_updates_dlq.arn}\",\"maxReceiveCount\":4}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "SNSToSQSPolicy",
      "Effect": "Allow",
      "Principal": "*",
      "Action": "SQS:SendMessage",
      "Resource": "*",
      "Condition": {
        "ArnEquals": {
          "aws:SourceArn": [
            "${var.prefix_update_sns_topic}",
            "${aws_sns_topic.prefix_updates_sns.arn}"
          ]
        }
      }
    }
  ]
}
EOF
}

resource "aws_sns_topic_subscription" "prefix_updates_subscription" {
  topic_arn            = var.prefix_update_sns_topic
  protocol             = "sqs"
  endpoint             = aws_sqs_queue.prefix_updates_sqs.arn
  raw_message_delivery = true
}

resource "aws_sns_topic" "prefix_updates_sns" {
  name = "${var.env}-mako-prefix-updates"

  delivery_policy = <<EOF
  {
    "http": {
      "defaultHealthyRetryPolicy": {
        "minDelayTarget": 1,
        "maxDelayTarget": 2,
        "numRetries": 5,
        "numMaxDelayRetries": 0,
        "numNoDelayRetries": 3,
        "numMinDelayRetries": 0,
        "backoffFunction": "linear"
      },
      "disableSubscriptionOverrides": false,
      "defaultThrottlePolicy": {
        "maxReceivesPerSecond": 2000
      }
    }
  }
  EOF
}

resource "aws_sns_topic_subscription" "prefix_updates_internal_subscription" {
  topic_arn            = aws_sns_topic.prefix_updates_sns.arn
  protocol             = "sqs"
  endpoint             = aws_sqs_queue.prefix_updates_sqs.arn
  raw_message_delivery = true
}

resource "aws_sns_topic" "pager_duty_sns" {
  name = "subs-pagerduty"

  policy = <<EOF
  {
    "Version": "2012-10-17",
    "Statement": [
      {
        "Sid": "Allow-Events",
        "Effect": "Allow",
        "Principal": {
          "Service": "events.amazonaws.com"
        },
        "Action": "SNS:Publish",
        "Resource": "arn:aws:sns:us-west-2:671452935478:subs-pagerduty"
      },
      {
        "Sid": "Allow-Cloudwatch",
        "Effect": "Allow",
        "Principal":
        {
          "Service": [
            "cloudwatch.amazonaws.com"
          ]
        },
        "Action": "SNS:Publish",
        "Resource": "arn:aws:sns:us-west-2:671452935478:subs-pagerduty"
      }
    ]
  }
  EOF
}

output "pager_duty_sns_arn" {
  value = aws_sns_topic.pager_duty_sns.arn
}
