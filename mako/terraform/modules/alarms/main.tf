resource "aws_cloudwatch_metric_alarm" "failed-emote-approval-rate" {
  alarm_name          = "failed-emote-approval-rate"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "3"
  metric_name         = "ReviewPendingEmoticons.failed.review.count"
  namespace           = "mako"

  dimensions = {
    Stage   = "${var.env}-ecs"
    Region  = "us-west-2"
    Service = "mako"
  }

  period                    = "300"
  statistic                 = "Sum"
  threshold                 = "1"
  alarm_description         = "[LowSev] Alarms when we've had failures approving emotes."
  datapoints_to_alarm       = "3"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"
  actions_enabled           = length(var.pager_duty_sns_arn) > 0 ? true : false
  alarm_actions             = [var.pager_duty_sns_arn]
  ok_actions                = [var.pager_duty_sns_arn]
}

resource "aws_cloudwatch_metric_alarm" "getemotesetdetails-traffic-drop" {
  alarm_name                = "GetEmoteSetDetails-traffic-drop"
  comparison_operator       = "LessThanLowerThreshold"
  evaluation_periods        = "2"
  datapoints_to_alarm       = "2"
  threshold_metric_id       = "e1"
  alarm_description         = "Monitors for traffic drops on GetEmoteSetDetails"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"
  actions_enabled           = length(var.pager_duty_sns_arn) > 0 ? true : false
  alarm_actions             = [var.pager_duty_sns_arn]
  ok_actions                = [var.pager_duty_sns_arn]

  metric_query {
    id          = "e1"
    expression  = "ANOMALY_DETECTION_BAND(m1, 3)"
    label       = "GetEmoteSetDetails Anomaly Detection"
    return_data = true
  }

  metric_query {
    id          = "m1"
    return_data = "true"

    metric {
      metric_name = "twirp.GetEmoteSetDetails.requests"
      namespace   = "mako"

      dimensions = {
        Stage   = "${var.env}-ecs"
        Region  = "us-west-2"
        Service = "mako"
      }

      period = "300"
      stat   = "Sum"
    }
  }
}

resource "aws_cloudwatch_metric_alarm" "mako_logscan_errors-low-sev" {
  alarm_name          = "mako-logscan-error-alert-low-sev"
  comparison_operator = "GreaterThanUpperThreshold"
  evaluation_periods  = "5"
  threshold_metric_id       = "e1"
  datapoints_to_alarm       = "5"
  alarm_description         = "[LowSev] This alarm is used when Mako's error levels have gone above normal levels."
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled = "${length(var.pager_duty_sns_arn) > 0 ? true : false}"
  alarm_actions   = ["${var.pager_duty_sns_arn}"]
  ok_actions      = ["${var.pager_duty_sns_arn}"]

  metric_query {
    id          = "e1"
    expression  = "ANOMALY_DETECTION_BAND(m1, 50)"
    label       = "mako-errors (expected)"
    return_data = true
  }

  metric_query {
    id = "m1"
    return_data = "true"

    metric {
      metric_name = "mako-errors"
      namespace = "mako-logs"
      period = "60"
      stat = "Sum"
    }
  }
}

resource "aws_cloudwatch_metric_alarm" "mako_logscan_errors" {
  alarm_name          = "mako-logscan-error-alert"
  comparison_operator = "GreaterThanUpperThreshold"
  evaluation_periods  = "5"
  threshold_metric_id       = "e1"
  datapoints_to_alarm       = "5"
  alarm_description         = "This alarm is used when Mako's error levels have gone above acceptable levels."
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled = length(var.pager_duty_sns_arn) > 0 ? true : false
  alarm_actions   = [var.pager_duty_sns_arn]
  ok_actions      = [var.pager_duty_sns_arn]

  metric_query {
    id          = "e1"
    expression  = "ANOMALY_DETECTION_BAND(m1, 500)"
    label       = "mako-errors (expected)"
    return_data = true
  }

  metric_query {
    id = "m1"
    return_data = "true"

    metric {
      metric_name = "mako-errors"
      namespace = "mako-logs"
      period = "60"
      stat = "Sum"
    }
  }
}

resource "aws_cloudwatch_metric_alarm" "mako_logscan_panics" {
  alarm_name          = "mako-logscan-panic-alert"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "mako-panics"
  namespace           = "mako-logs"

  period                    = "300"
  statistic                 = "Sum"
  threshold                 = "1"
  alarm_description         = "This alarm is used when Mako starts panicing"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled = length(var.pager_duty_sns_arn) > 0 ? true : false
  alarm_actions   = [var.pager_duty_sns_arn]
  ok_actions      = [var.pager_duty_sns_arn]
}
