variable "env" {
  description = "Whether it's production, dev, etc"
}

variable "pager_duty_sns_arn" {
  type    = string
  default = ""
}
