# Default provider. Most modules consumed redeclare an aws provider, so this is seldom used.
provider "aws" {
  region  = "us-west-2"
  profile = var.aws_profile
}
