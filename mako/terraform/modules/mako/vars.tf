# AWS general Config
variable "aws_profile" {
  description = "The AWS Named Profile to use when running terraform and creating AWS resources"
}

variable "service" {
  description = "Name of the service being built. Should probably leave this as the default"
  default     = "commerce/mako"
}

variable "env" {
  description = "Whether it's production, dev, etc"
}

variable "account_name" {
  description = "Name of the AWS account"
}

variable "owner" {
  description = "Owner of the account"
}

variable "upload_service_arn" {
  description = "arn for upload service"
}

# VPC Config
variable "vpc_id" {
  description = "Id of the systems-created VPC"
}

variable "private_subnets" {
  description = "Comma-separated list of private subnets"
}

variable "sg_id" {
  description = "Id of the security group"
}

# DynamoDB Config
variable "entitle_read_capacity" {
  description = "Read capacity of the entitlements dynamo table"
  default     = 5
}

variable "entitle_write_capacity" {
  description = "Write capacity of the entitlements dynamo table"
  default     = 5
}

variable "entitle_max_read" {
  description = "Max read capacity of the entitlements dynamo table"
  default     = 10
}

variable "entitle_min_read" {
  description = "Min read capacity of the entitlements dynamo table"
  default     = 5
}

variable "entitle_max_write" {
  description = "Max write capacity of the entitlements dynamo table"
  default     = 10
}

variable "entitle_min_write" {
  description = "Min write capacity of the entitlements dynamo table"
  default     = 5
}

variable "prefix_read_capacity" {
  description = "Read capacity of the prefixes dynamo table"
  default     = 5
}

variable "prefix_write_capacity" {
  description = "Write capacity of the prefixes dynamo table"
  default     = 5
}

variable "prefix_max_read" {
  description = "Max read capacity of the prefixes dynamo table"
  default     = 10
}

variable "prefix_min_read" {
  description = "Min read capacity of the prefixes dynamo table"
  default     = 5
}

variable "prefix_max_write" {
  description = "Max write capacity of the prefixes dynamo table"
  default     = 10
}

variable "prefix_min_write" {
  description = "Min write capacity of the prefixes dynamo table"
  default     = 5
}

variable "emote_read_capacity" {
  description = "Read capacity of the emotes dynamo table"
  default     = 5
}

variable "emote_write_capacity" {
  description = "Write capacity of the emotes dynamo table"
  default     = 5
}

variable "emote_max_read" {
  description = "Max read capacity of the emotes dynamo table"
  default     = 10
}

variable "emote_min_read" {
  description = "Min read capacity of the emotes dynamo table"
  default     = 5
}

variable "emote_max_write" {
  description = "Max write capacity of the emotes dynamo table"
  default     = 10
}

variable "emote_min_write" {
  description = "Min write capacity of the emotes dynamo table"
  default     = 5
}

variable "pending_emote_read_capacity" {
  description = "Read capacity of the pending emotes dynamo table"
  default     = 5
}

variable "pending_emote_write_capacity" {
  description = "Write capacity of the pending emotes dynamo table"
  default     = 5
}

variable "pending_emote_max_read" {
  description = "Max read capacity of the pending emotes dynamo table"
  default     = 10
}

variable "pending_emote_min_read" {
  description = "Min read capacity of the pending emotes dynamo table"
  default     = 5
}

variable "pending_emote_max_write" {
  description = "Max write capacity of the pending emotes dynamo table"
  default     = 10
}

variable "pending_emote_min_write" {
  description = "Min write capacity of the pending emotes dynamo table"
  default     = 5
}

variable "setting_read_capacity" {
  description = "Read capacity of the settings dynamo table"
  default     = 5
}

variable "setting_write_capacity" {
  description = "Write capacity of the settings dynamo table"
  default     = 5
}

variable "setting_max_read" {
  description = "Max read capacity of the settings dynamo table"
  default     = 10
}

variable "setting_min_read" {
  description = "Min read capacity of the settings dynamo table"
  default     = 5
}

variable "setting_max_write" {
  description = "Max write capacity of the settings dynamo table"
  default     = 10
}

variable "setting_min_write" {
  description = "Min write capacity of the settings dynamo table"
  default     = 5
}

variable "emote_group_read_capacity" {
  description = "Read capacity of the emote_groups dynamo table"
  default     = 5
}

variable "emote_group_write_capacity" {
  description = "Write capacity of the emote_groups dynamo table"
  default     = 5
}

variable "emote_group_max_read" {
  description = "Max read capacity of the emote_groups dynamo table"
  default     = 10
}

variable "emote_group_min_read" {
  description = "Min read capacity of the emote_groups dynamo table"
  default     = 5
}

variable "emote_group_max_write" {
  description = "Max write capacity of the emote_groups dynamo table"
  default     = 10
}

variable "emote_group_min_write" {
  description = "Min write capacity of the emote_groups dynamo table"
  default     = 5
}

variable "dynamo_autoscaling_role" {
  description = "IAM role used to manage dynamo autoscaling"
}

variable "redis_instance_size" {
  description = "type of instance to use for the redis fleet"
  type        = string
}

variable "redis_groups" {
  type = string
}

variable "redis_replicas_per_group" {
  type = string
}

variable "prefix_update_sns_topic" {
  description = "The SNS topic ARN that Subscription service publishes to when emoticon prefix is updated."
}

variable "cert_arn" {
  description = "ARN of the env's ACM SSL cert."
}

variable "cert_arn_beta" {
  description = "ARN of the env's beta stage ACM SSL cert."
  default     = ""
}

variable "cert_arn_canary" {
  description = "ARN of the env's canary stage ACM SSL cert."
  default     = ""
}

variable "capacity_planning_arn" {
  type        = string
  description = "ARN for the capacity planning team's script"
  default     = ""
}

variable "pager_duty_sns_arn" {
  type    = string
  default = ""
}
