module "entitle-db" {
  source      = "../dynamo"
  aws_profile = var.aws_profile
  env         = var.env

  entitle_read_capacity  = var.entitle_read_capacity
  entitle_write_capacity = var.entitle_write_capacity
  entitle_max_read       = var.entitle_max_read
  entitle_min_read       = var.entitle_min_read
  entitle_max_write      = var.entitle_max_write
  entitle_min_write      = var.entitle_min_write

  prefix_read_capacity  = var.prefix_read_capacity
  prefix_write_capacity = var.prefix_write_capacity
  prefix_max_read       = var.prefix_max_read
  prefix_min_read       = var.prefix_min_read
  prefix_max_write      = var.prefix_max_write
  prefix_min_write      = var.prefix_min_write

  emote_read_capacity  = var.emote_read_capacity
  emote_write_capacity = var.emote_write_capacity
  emote_max_read       = var.emote_max_read
  emote_min_read       = var.emote_min_read
  emote_max_write      = var.emote_max_write
  emote_min_write      = var.emote_min_write

  pending_emote_read_capacity  = var.pending_emote_read_capacity
  pending_emote_write_capacity = var.pending_emote_write_capacity
  pending_emote_max_read       = var.pending_emote_max_read
  pending_emote_min_read       = var.pending_emote_min_read
  pending_emote_max_write      = var.pending_emote_max_write
  pending_emote_min_write      = var.pending_emote_min_write

  setting_read_capacity  = var.setting_read_capacity
  setting_write_capacity = var.setting_write_capacity
  setting_max_read       = var.setting_max_read
  setting_min_read       = var.setting_min_read
  setting_max_write      = var.setting_max_write
  setting_min_write      = var.setting_min_write

  emote_group_read_capacity  = var.emote_group_read_capacity
  emote_group_write_capacity = var.emote_group_write_capacity
  emote_group_max_read       = var.emote_group_max_read
  emote_group_min_read       = var.emote_group_min_read
  emote_group_max_write      = var.emote_group_max_write
  emote_group_min_write      = var.emote_group_min_write

  autoscaling_role = var.dynamo_autoscaling_role

  pager_duty_sns_arn = module.events-sns.pager_duty_sns_arn
}

module "events-sns" {
  source                  = "../sns"
  aws_profile             = var.aws_profile
  env                     = var.env
  prefix_update_sns_topic = var.prefix_update_sns_topic
}

module "user_emotes_redis" {
  source                   = "../elasticache"
  aws_profile              = var.aws_profile
  env                      = var.env
  private_subnets          = var.private_subnets
  sg_id                    = var.sg_id
  redis_instance_size      = var.redis_instance_size
  redis_groups             = var.redis_groups
  redis_replicas_per_group = var.redis_replicas_per_group
}

module "emoticon_uploads_s3" {
  source                = "../s3"
  aws_profile           = var.aws_profile
  env                   = var.env
  upload_service_arn    = var.upload_service_arn
  capacity_planning_arn = var.capacity_planning_arn
}

module "twitch-inventory" {
  source  = "git::git+ssh://git@git.xarth.tv/terraform-modules/twitch-inventory?ref=a7207ce935fce975ee739198bcd7e2df80419680"
  account = var.aws_profile
}

module "mako-alarms" {
  source             = "../alarms"
  env                = var.env
  pager_duty_sns_arn = module.events-sns.pager_duty_sns_arn
}
