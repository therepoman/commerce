// entitlements

resource "aws_dynamodb_table" "entitle_db" {
  name           = "${var.env}_entitlements"
  read_capacity  = var.entitle_read_capacity
  write_capacity = var.entitle_write_capacity

  stream_enabled   = true
  stream_view_type = "NEW_AND_OLD_IMAGES"

  point_in_time_recovery {
    enabled = true
  }

  hash_key  = "userId"
  range_key = "entitlementScopedKey"

  attribute {
    name = "userId"
    type = "S"
  }

  attribute {
    name = "entitlementScopedKey"
    type = "S"
  }

  replica {
    region_name = "us-east-2"
  }

  lifecycle {
    # Ignored due to autoscaling
    ignore_changes = [read_capacity, write_capacity]
  }
}

module "entitlements_table_autoscaling" {
  source           = "../dynamo_table_autoscaling"
  table_name       = "${var.env}_entitlements"
  min_read         = var.entitle_min_read
  min_write        = var.entitle_min_write
  max_read         = var.entitle_max_read
  max_write        = var.entitle_max_write
  autoscaling_role = var.autoscaling_role
}

// prefixes

resource "aws_dynamodb_table" "prefix_db" {
  name           = "${var.env}_prefixes"
  read_capacity  = var.prefix_read_capacity
  write_capacity = var.prefix_write_capacity

  point_in_time_recovery {
    enabled = true
  }

  hash_key = "prefix"

  attribute {
    name = "prefix"
    type = "S"
  }

  attribute {
    name = "ownerId"
    type = "S"
  }

  attribute {
    name = "state"
    type = "S"
  }

  global_secondary_index {
    name            = "prefix_by_owner_id_gsi"
    hash_key        = "ownerId"
    projection_type = "ALL"
    read_capacity   = var.prefix_read_capacity
    write_capacity  = var.prefix_write_capacity
  }

  global_secondary_index {
    name            = "prefix_by_state_owner_id_gsi"
    hash_key        = "state"
    range_key       = "ownerId"
    projection_type = "ALL"
    read_capacity   = var.prefix_read_capacity
    write_capacity  = var.prefix_write_capacity
  }

  lifecycle {
    # Ignored due to autoscaling
    ignore_changes = [read_capacity, write_capacity, global_secondary_index]
  }
}

module "prefixes_table_autoscaling" {
  source           = "../dynamo_table_autoscaling"
  table_name       = "${var.env}_prefixes"
  min_read         = var.prefix_min_read
  min_write        = var.prefix_min_write
  max_read         = var.prefix_max_read
  max_write        = var.prefix_max_write
  autoscaling_role = var.autoscaling_role
}

module "prefixes_owner_id_gsi_autoscaling" {
  source           = "../dynamo_table_autoscaling"
  table_name       = "${var.env}_prefixes/index/prefix_by_owner_id_gsi"
  table_kind       = "index"
  min_read         = var.prefix_min_read
  min_write        = var.prefix_min_write
  max_read         = var.prefix_max_read
  max_write        = var.prefix_max_write
  autoscaling_role = var.autoscaling_role
}

module "prefixes_state_owner_id_gsi_autoscaling" {
  source           = "../dynamo_table_autoscaling"
  table_name       = "${var.env}_prefixes/index/prefix_by_state_owner_id_gsi"
  table_kind       = "index"
  min_read         = var.prefix_min_read
  min_write        = var.prefix_min_write
  max_read         = var.prefix_max_read
  max_write        = var.prefix_max_write
  autoscaling_role = var.autoscaling_role
}

// pending emotes

resource "aws_dynamodb_table" "pending_emote_db" {
  name           = "${var.env}_pending_emotes"
  read_capacity  = var.pending_emote_read_capacity
  write_capacity = var.pending_emote_write_capacity

  stream_enabled   = true
  stream_view_type = "NEW_AND_OLD_IMAGES"

  point_in_time_recovery {
    enabled = true
  }

  hash_key = "emoteId"

  attribute {
    name = "emoteId"
    type = "S"
  }

  attribute {
    name = "type"
    type = "S"
  }

  attribute {
    name = "createdAt"
    type = "S"
  }

  attribute {
    name = "reviewState"
    type = "S"
  }

  attribute {
    name = "type-reviewState"
    type = "S"
  }

  replica {
    region_name = "us-east-2"
  }

  lifecycle {
    # Ignored due to autoscaling
    ignore_changes = [read_capacity, write_capacity, global_secondary_index]
  }

  global_secondary_index {
    name            = "pending_queues_index"
    hash_key        = "type"
    range_key       = "createdAt"
    projection_type = "ALL"
    read_capacity   = var.pending_emote_read_capacity
    write_capacity  = var.pending_emote_write_capacity
  }

  global_secondary_index {
    name            = "pending_emotes_reviewstate_index"
    hash_key        = "reviewState"
    range_key       = "createdAt"
    projection_type = "ALL"
    read_capacity   = var.pending_emote_read_capacity
    write_capacity  = var.pending_emote_write_capacity
  }

  global_secondary_index {
    name            = "pending_emotes_type-reviewstate_index"
    hash_key        = "type-reviewState"
    range_key       = "createdAt"
    projection_type = "ALL"
    read_capacity   = var.pending_emote_read_capacity
    write_capacity  = var.pending_emote_write_capacity
  }
}

module "pending_emotes_table_autoscaling" {
  source           = "../dynamo_table_autoscaling"
  table_name       = "${var.env}_pending_emotes"
  min_read         = var.pending_emote_min_read
  min_write        = var.pending_emote_min_write
  max_read         = var.pending_emote_max_read
  max_write        = var.pending_emote_max_write
  autoscaling_role = var.autoscaling_role
}

// settings

resource "aws_dynamodb_table" "setting_db" {
  name           = "${var.env}_settings"
  read_capacity  = var.setting_read_capacity
  write_capacity = var.setting_write_capacity

  stream_enabled   = true
  stream_view_type = "NEW_AND_OLD_IMAGES"

  point_in_time_recovery {
    enabled = true
  }

  hash_key = "ownerId"

  attribute {
    name = "ownerId"
    type = "S"
  }

  replica {
    region_name = "us-east-2"
  }

  lifecycle {
    # Ignored due to autoscaling
    ignore_changes = [read_capacity, write_capacity]
  }
}

module "settings_table_autoscaling" {
  source           = "../dynamo_table_autoscaling"
  table_name       = "${var.env}_settings"
  min_read         = var.setting_min_read
  min_write        = var.setting_min_write
  max_read         = var.setting_max_read
  max_write        = var.setting_max_write
  autoscaling_role = var.autoscaling_role
}

// emote_groups

resource "aws_dynamodb_table" "emote_group_db" {
  name           = "${var.env}_emote_groups"
  read_capacity  = var.emote_group_read_capacity
  write_capacity = var.emote_group_write_capacity

  point_in_time_recovery {
    enabled = true
  }

  hash_key = "groupId"

  attribute {
    name = "groupId"
    type = "S"
  }

  attribute {
    name = "ownerId"
    type = "S"
  }

  global_secondary_index {
    name            = "emote_groups_ownerid_index"
    hash_key        = "ownerId"
    range_key       = "groupId"
    projection_type = "ALL"
    read_capacity   = var.emote_group_read_capacity
    write_capacity  = var.emote_group_write_capacity
  }

  lifecycle {
    # Ignored due to autoscaling
    ignore_changes = [read_capacity, write_capacity, global_secondary_index]
  }
}

module "emote_group_table_autoscaling" {
  source           = "../dynamo_table_autoscaling"
  table_name       = "${var.env}_emote_groups"
  min_read         = var.emote_group_min_read
  min_write        = var.emote_group_min_write
  max_read         = var.emote_group_max_read
  max_write        = var.emote_group_max_write
  autoscaling_role = var.autoscaling_role
}

module "emote_group_owner_id_gsi_autoscaling" {
  source           = "../dynamo_table_autoscaling"
  table_name       = "${var.env}_emote_groups/index/emote_groups_ownerid_index"
  table_kind       = "index"
  min_read         = var.emote_group_min_read
  min_write        = var.emote_group_min_write
  max_read         = var.emote_group_max_read
  max_write        = var.emote_group_max_write
  autoscaling_role = var.autoscaling_role
}

module "EmoteGroupsTableAlarms" {
  source             = "./../alarms/dynamodb-alarms"
  table              = "${var.env}_emote_groups"
  pager_duty_sns_arn = var.pager_duty_sns_arn
}

module "EmoteGroupsOwnerIDIndexAlarms" {
  source             = "./../alarms/dynamodb-alarms"
  table              = "${var.env}_emote_groups"
  index              = "emote_groups_ownerid_index"
  pager_duty_sns_arn = var.pager_duty_sns_arn
}
