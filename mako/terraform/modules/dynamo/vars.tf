# Service configuration
variable "aws_profile" {
  description = "AWS profile used to create resources"
}

variable "aws_region" {
  description = "AWS region to operate in."
  default     = "us-west-2"
}

variable "env" {
  description = "Whether it's production, dev, etc"
}

# DynamoDB configuration
variable "entitle_read_capacity" {
  description = "Read capacity of the entitlements dynamo table"
}

variable "entitle_write_capacity" {
  description = "Write capacity of the entitlements dynamo table"
}

variable "prefix_read_capacity" {
  description = "Read capacity of the prefix dynamo table"
}

variable "prefix_write_capacity" {
  description = "Write capacity of the prefix dynamo table"
}

variable "emote_read_capacity" {
  description = "Read capacity of the emotes dynamo table"
}

variable "emote_write_capacity" {
  description = "Write capacity of the emotes dynamo table"
}

variable "pending_emote_read_capacity" {
  description = "Read capacity of the pending emotes dynamo table"
}

variable "pending_emote_write_capacity" {
  description = "Write capacity of the pending emotes dynamo table"
}

variable "setting_read_capacity" {
  description = "Read capacity of the settings dynamo table"
}

variable "setting_write_capacity" {
  description = "Write capacity of the settings dynamo table"
}

variable "emote_group_read_capacity" {
  description = "Read capacity of the emote_groups dynamo table"
}

variable "emote_group_write_capacity" {
  description = "Write capacity of the emote_groups dynamo table"
}

variable "entitle_max_read" {
  description = "Max read capacity of the entitlements dynamo table"
}

variable "entitle_min_read" {
  description = "Min read capacity of the entitlements dynamo table"
}

variable "entitle_max_write" {
  description = "Max write capacity of the entitlements dynamo table"
}

variable "entitle_min_write" {
  description = "Min write capacity of the entitlements dynamo table"
}

variable "prefix_max_read" {
  description = "Max read capacity of the prefixes dynamo table"
}

variable "prefix_min_read" {
  description = "Min read capacity of the prefixes dynamo table"
}

variable "prefix_max_write" {
  description = "Max write capacity of the prefixes dynamo table"
}

variable "prefix_min_write" {
  description = "Min write capacity of the prefixes dynamo table"
}

variable "emote_max_read" {
  description = "Max read capacity of the emotes dynamo table"
}

variable "emote_min_read" {
  description = "Min read capacity of the emotes dynamo table"
}

variable "emote_max_write" {
  description = "Max write capacity of the emotes dynamo table"
}

variable "emote_min_write" {
  description = "Min write capacity of the emotes dynamo table"
}

variable "pending_emote_max_read" {
  description = "Max read capacity of the pending emotes dynamo table"
}

variable "pending_emote_min_read" {
  description = "Min read capacity of the pending emotes dynamo table"
}

variable "pending_emote_max_write" {
  description = "Max write capacity of the pending emotes dynamo table"
}

variable "pending_emote_min_write" {
  description = "Min write capacity of the pending emotes dynamo table"
}

variable "setting_max_read" {
  description = "Max read capacity of the settings dynamo table"
}

variable "setting_min_read" {
  description = "Min read capacity of the settings dynamo table"
}

variable "setting_max_write" {
  description = "Max write capacity of the settings dynamo table"
}

variable "setting_min_write" {
  description = "Min write capacity of the settings dynamo table"
}

variable "emote_group_max_read" {
  description = "Max read capacity of the emote_groups dynamo table"
}

variable "emote_group_min_read" {
  description = "Min read capacity of the emote_groups dynamo table"
}

variable "emote_group_max_write" {
  description = "Max write capacity of the emote_groups dynamo table"
}

variable "emote_group_min_write" {
  description = "Min write capacity of the emote_groups dynamo table"
}

variable "autoscaling_role" {
  description = "IAM role used to manage dynamo autoscaling"
}

variable "pager_duty_sns_arn" {
  default     = ""
  description = "ARN of the SNS topic used for Pager Duty alarms"
  type        = string
}
