variable "aws_profile" {
  description = "AWS profile used to create resources"
}

variable "env" {
  description = "Whether it's production, dev, etc"
}

variable "aws_region" {
  description = "AWS region to operate in."
  default     = "us-west-2"
}

variable "upload_service_arn" {
  description = "arn for upload service"
}

variable "capacity_planning_arn" {
  description = "arn for the script that capacity planning runs on our s3 buckets"
}
