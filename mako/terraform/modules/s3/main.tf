resource "aws_s3_bucket" "emoticon_uploads_logs_s3" {
  bucket = "${var.env}-emoticon-uploads-logs"
  acl    = "log-delivery-write"
}

resource "aws_iam_role" "emoticon_uploads_replication_role" {
  name = "tf-${var.env}-s3-replication-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "s3.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_policy" "emoticon_uploads_replication_policy" {
  name = "tf-${var.env}-s3-replication-policy"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "s3:GetReplicationConfiguration",
        "s3:ListBucket"
      ],
      "Effect": "Allow",
      "Resource": [
        "${aws_s3_bucket.emoticon_uploads_s3.arn}"
      ]
    },
    {
      "Action": [
        "s3:GetObjectVersion",
        "s3:GetObjectVersionAcl"
      ],
      "Effect": "Allow",
      "Resource": [
        "${aws_s3_bucket.emoticon_uploads_s3.arn}/*"
      ]
    },
    {
      "Action": [
        "s3:ReplicateObject",
        "s3:ReplicateDelete"
      ],
      "Effect": "Allow",
      "Resource": "${aws_s3_bucket.emoticon_uploads_replication_s3.arn}/*"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "emoticon_uploads_replication" {
  role       = aws_iam_role.emoticon_uploads_replication_role.name
  policy_arn = aws_iam_policy.emoticon_uploads_replication_policy.arn
}

resource "aws_s3_bucket" "emoticon_uploads_replication_s3" {
  bucket   = "${var.env}-emoticon-uploads-replication"
  region   = "us-east-1"
  provider = aws.east

  versioning {
    enabled = true
  }
}

resource "aws_s3_bucket" "emoticon_uploads_s3" {
  bucket = "${var.env}-emoticon-uploads"

  logging {
    target_bucket = "${var.env}-emoticon-uploads-logs"
    target_prefix = "logs/"
  }

  versioning {
    enabled = true
  }

  replication_configuration {
    role = aws_iam_role.emoticon_uploads_replication_role.arn

    rules {
      id     = "${var.env}-emoticon-uploads-replicate-rule"
      status = "Enabled"

      destination {
        bucket        = aws_s3_bucket.emoticon_uploads_replication_s3.arn
        storage_class = "STANDARD"
      }
    }
  }
}

data "aws_iam_policy_document" "s3_uploader_access" {
  statement {
    effect = "Allow"

    principals {
      type        = "AWS"
      identifiers = ["*"]
    }

    actions   = ["s3:GetObject"]
    resources = ["${aws_s3_bucket.emoticon_uploads_s3.arn}/*"]
  }

  statement {
    effect = "Allow"

    principals {
      type        = "AWS"
      identifiers = [var.capacity_planning_arn]
    }

    actions   = ["s3:ListBucket"]
    resources = [aws_s3_bucket.emoticon_uploads_s3.arn]
  }

  statement {
    effect = "Allow"

    principals {
      type        = "AWS"
      identifiers = [var.upload_service_arn]
    }

    actions   = ["s3:PutObject", "s3:PutObjectAcl"]
    resources = ["${aws_s3_bucket.emoticon_uploads_s3.arn}/*"]
  }
}

resource "aws_s3_bucket_policy" "uploader_access" {
  bucket = aws_s3_bucket.emoticon_uploads_s3.id
  policy = data.aws_iam_policy_document.s3_uploader_access.json
}

resource "aws_s3_bucket" "autoprof" {
  bucket = "mako-${var.env}-autoprof"

  lifecycle_rule {
    enabled = true

    expiration {
      days = 14
    }
  }
}
