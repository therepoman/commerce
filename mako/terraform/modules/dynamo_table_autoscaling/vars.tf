variable "table_name" {
  description = "Name of table to autoscale"
}

variable "table_kind" {
  description = "Either table or index depending on table kind being targeted"
  default     = "table"
}

variable "min_read" {
  description = "Minimum number of reads to allow"
  default     = 5
}

variable "max_read" {
  description = "Maximum number of reads to allow"
  default     = 10000
}

variable "min_write" {
  description = "Minimum number of writes to allow"
  default     = 5
}

variable "max_write" {
  description = "Maximum number of writes to allow"
  default     = 10000
}

variable "autoscaling_role" {
  description = "IAM role used to manage dynamo autoscaling"
}
