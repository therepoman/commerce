resource "aws_appautoscaling_target" "dynamodb_read_target" {
  max_capacity       = var.max_read
  min_capacity       = var.min_read
  resource_id        = "table/${var.table_name}"
  role_arn           = var.autoscaling_role
  scalable_dimension = "dynamodb:${var.table_kind}:ReadCapacityUnits"
  service_namespace  = "dynamodb"
}

resource "aws_appautoscaling_policy" "dynamodb_read_policy" {
  name               = "DynamoDBReadCapacityUtilization:${aws_appautoscaling_target.dynamodb_read_target.resource_id}"
  policy_type        = "TargetTrackingScaling"
  resource_id        = aws_appautoscaling_target.dynamodb_read_target.resource_id
  scalable_dimension = aws_appautoscaling_target.dynamodb_read_target.scalable_dimension
  service_namespace  = aws_appautoscaling_target.dynamodb_read_target.service_namespace

  target_tracking_scaling_policy_configuration {
    predefined_metric_specification {
      predefined_metric_type = "DynamoDBReadCapacityUtilization"
    }

    target_value = 70
  }

  depends_on = [aws_appautoscaling_target.dynamodb_read_target]
}

resource "aws_appautoscaling_target" "dynamodb_write_target" {
  max_capacity       = var.max_write
  min_capacity       = var.min_write
  resource_id        = "table/${var.table_name}"
  role_arn           = var.autoscaling_role
  scalable_dimension = "dynamodb:${var.table_kind}:WriteCapacityUnits"
  service_namespace  = "dynamodb"
}

resource "aws_appautoscaling_policy" "dynamodb_write_policy" {
  name               = "DynamoDBWriteCapacityUtilization:${aws_appautoscaling_target.dynamodb_write_target.resource_id}"
  policy_type        = "TargetTrackingScaling"
  resource_id        = aws_appautoscaling_target.dynamodb_write_target.resource_id
  scalable_dimension = aws_appautoscaling_target.dynamodb_write_target.scalable_dimension
  service_namespace  = aws_appautoscaling_target.dynamodb_write_target.service_namespace

  target_tracking_scaling_policy_configuration {
    predefined_metric_specification {
      predefined_metric_type = "DynamoDBWriteCapacityUtilization"
    }

    target_value = 70
  }

  depends_on = [aws_appautoscaling_target.dynamodb_write_target]
}
