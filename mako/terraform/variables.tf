variable "aws_access_key" {
  default = ""
}

variable "aws_secret_key" {
  default = ""
}

variable "aws_profile" {
}

variable "vpc_id" {
}

variable "cluster" {
  default = "sonar"
}

variable "region" {
}

variable "security_group" {
}

variable "environment" {
}

variable "subnets" {
}

variable "sandstorm_account_id" {
  type = string
}

provider "aws" {
  region     = var.region
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
  profile    = var.aws_profile
}

variable "capacity_planning_arn" {
  type        = string
  description = "ARN for the capacity planning team's script"
  default     = ""
}

variable "upload_service_arn" {
  description = "arn for upload service"
}

