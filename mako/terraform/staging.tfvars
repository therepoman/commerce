region = "us-west-2"

environment = "staging"

aws_profile = "mako-devo"

vpc_id = "vpc-b0f164d7"

security_group = "sg-cbef7fb3"

subnets = "subnet-f9fd3db0,subnet-8d49e7ea,subnet-c25e709a"

sandstorm_account_id = "734326455073"

capacity_planning_arn = "arn:aws:iam::578510050023:role/twitch-inventory-master"

upload_service_arn = "arn:aws:iam::995367761609:role/staging-upload-service"

