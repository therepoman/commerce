module "emote_modifier_groups_db_glue_export" {
  source = "git::ssh://git@git.xarth.tv/stats/db-s3-glue.git?ref=68bfa5310c1c1cfa94ccf9e44e7f73be5c8f7238"

  account_number        = data.aws_caller_identity.current.account_id
  cluster_name          = "prod_emote_modifier_groups"
  database_type         = "dynamodb"
  dynamodb_splits_count = 9
  error_sns_topic_name  = aws_sns_topic.glue_errors.name
  job_name              = "emotemodifiergroupsdb"

  # Step 2 variables
  api_key_kms_key_id      = aws_kms_alias.export_ssm.target_key_id
  api_key_parameter_name  = aws_ssm_parameter.api_key.name
  tahoe_producer_name     = "emotemodifiergroupsdb"
  tahoe_producer_role_arn = "arn:aws:iam::331582574546:role/producer-emotemodifiergroupsdb"

  create_s3_output_bucket = 1
  s3_output_bucket        = "emotemodifiergroupsdb-export-output"
  s3_output_key           = "prod_emote_modifier_groups"

  create_s3_script_bucket = 1
  s3_script_bucket        = "emotemodifiergroupsdb-export-scripts"

  table_config  = { "prod_emote_modifier_groups" = file("./table_config.json") }
  cleaning_code = file("./cleaning_code.py")

  db_password_key_id         = "" # unused
  db_password_parameter_name = "" #unused
  cluster_username           = "" # unused
  rds_subnet_group           = "" # unused
  availability_zone          = "" # unused
  subnet_id                  = "" # unused
  vpc_id                     = "" # unused

  trigger_schedule           = "0 8,20 * * ? *"
}

data "aws_caller_identity" "current" {}

resource "aws_kms_key" "export_ssm" {
  description = "Encryption key for emotemodifiergroupsdb Tahoe export API key parameter"
}

resource "aws_kms_alias" "export_ssm" {
  name_prefix   = "alias/emotemodifiergroupsdb-export-"
  target_key_id = aws_kms_key.export_ssm.key_id
}

resource "aws_ssm_parameter" "api_key" {
  name        = "/tahoe/emotemodifiergroupsdb/api_key"
  description = "emotemodifiergroupsdb export API key for Tahoe"
  type        = "SecureString"
  value       = "changeme"
  key_id      = aws_kms_key.export_ssm.key_id

  lifecycle {
    ignore_changes = [value]
  }
}

resource "aws_sns_topic" "glue_errors" {
  name = "glue-export-errors"
}

resource "aws_sns_topic_policy" "export_errors" {
  arn    = aws_sns_topic.glue_errors.arn
  policy = data.aws_iam_policy_document.export_errors_policy.json
}

data "aws_iam_policy_document" "export_errors_policy" {
  statement {
    effect = "Allow"
    actions = [
      "SNS:Publish"
    ]
    principals {
      identifiers = ["events.amazonaws.com"]
      type        = "Service"
    }
    resources = [
      aws_sns_topic.glue_errors.arn
    ]
  }
}
