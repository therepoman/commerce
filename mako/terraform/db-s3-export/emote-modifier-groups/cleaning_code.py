def ensure_fields(record):
    if "id" not in record:
        record["id"] = ""
    if "ownerId" not in record:
        record["ownerId"] = ""
    if "modifiers" not in record:
        record["modifiers"] = []
    if "sourceEmoteGroupIds" not in record:
        record["sourceEmoteGroupIds"] = []
    return record
return Map.apply(frame=frame, f=ensure_fields)