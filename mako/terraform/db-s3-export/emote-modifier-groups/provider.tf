provider "aws" {
  profile = "mako-prod"
  region  = "us-west-2"
  version = "~> 2.44"
}