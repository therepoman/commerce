output "s3_kms_key" {
  value = module.emote_modifier_groups_db_glue_export.s3_kms_key
}

output "s3_output_bucket" {
  value = module.emote_modifier_groups_db_glue_export.s3_output_bucket
}

output "glue_role" {
  value = module.emote_modifier_groups_db_glue_export.glue_role
}