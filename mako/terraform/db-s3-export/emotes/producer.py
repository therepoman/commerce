import json
import os
from boto3.session import Session
from pprint import pprint

# Create lambda client with temp credentials provided by Data Infrastructure
# These were provided to you in the "Register a Producer" step.
session = Session(aws_access_key_id=os.getenv("AWS_ACCESS_KEY_ID"),
                  aws_secret_access_key=os.getenv("AWS_SECRET_ACCESS_KEY"),
                  aws_session_token=os.getenv("AWS_SESSION_TOKEN"))
client = session.client('lambda', region_name='us-west-2')

# CHANGEME Enter your producer attributes here
body = {
    'application_name': 'emotesdb', # Format: ^[a-z][a-z0-9]{2,47}$
    'bindle_id':        'amzn1.bindle.resource.lm4kpokajw5wu74ytska', # Format: amzn1.bindle.resource.<20-alphanum>
    'principal_role':   'arn:aws:iam::671452935478:role/emotesdb-glue-export', # Format: arn:aws:iam::<aws_account_id>:<user|group|role>
    's3_source_bucket': 'emotesdb-export-output', # Optional: Format: s3 bucket name
    'source_kms_arn':   'arn:aws:kms:us-west-2:671452935478:key/e6894959-6839-4ed5-a55d-5e40dbe27efd', # Optional: Format: KMS ARN
}

# Configure producer registration request
registration_arn = 'arn:aws:lambda:us-west-2:331582574546:function:RegistrationLambdaFunction'
payload = {
    'httpMethod': 'POST',
    'path': '/twirp/twitch.fulton.example.twitchtahoeapiservice.TwitchTahoeAPIService/RegisterProducer',
    'headers': {'Content-Type': 'application/json'},
    'body': json.dumps(body)
}

# Invoke lambda to register new producer
response = client.invoke(FunctionName=registration_arn,
                         InvocationType='RequestResponse',
                         Payload=json.dumps(payload))
result = json.loads(response.get('Payload').read())

# Display producer attributes cleanly on success, otherwise dump response
if result['statusCode'] == 200:
    pprint(json.loads(result['body']))
    print("Successfully created producer")
else:
    pprint(result)
    print("Failed to create producer")