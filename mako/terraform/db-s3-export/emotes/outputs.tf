output "s3_kms_key" {
  value = module.emotes_db_glue_export.s3_kms_key
}

output "s3_output_bucket" {
  value = module.emotes_db_glue_export.s3_output_bucket
}

output "glue_role" {
  value = module.emotes_db_glue_export.glue_role
}