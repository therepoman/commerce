# Note that this points to prod only.
terraform {
  backend "s3" {
    bucket  = "mako-prod"
    key     = "tfstate/mako/emotes-export-terracode/terraform.tfstate"
    region  = "us-west-2"
    profile = "mako-prod"
  }
}