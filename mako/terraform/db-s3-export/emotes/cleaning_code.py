def ensure_fields(record):
    if "id" not in record:
        record["id"] = ""
    if "code" not in record:
        record["code"] = ""
    if "createdAt" not in record:
        record["createdAt"] = 0
    if "domain" not in record:
        record["domain"] = ""
    if "emotesGroup" not in record:
        record["emotesGroup"] = ""
    if "groupId" not in record:
        record["groupId"] = ""
    if "order" not in record:
        record["order"] = 0
    if "ownerId" not in record:
        record["ownerId"] = ""
    if "prefix" not in record:
        record["prefix"] = ""
    if "state" not in record:
        record["state"] = ""
    if "suffix" not in record:
        record["suffix"] = ""
    if "updatedAt" not in record:
        record["updatedAt"] = 0
    if "productId" not in record:
        record["productId"] = ""
    if "assetType" not in record:
        record["assetType"] = ""
    if "animationTemplate" not in record:
        record["animationTemplate"] = ""
    return record
return Map.apply(frame=frame, f=ensure_fields)
