
# Mako DB S3 Export

This folder holds the DB -> S3 glue jobs that export data to Tahoe. Each sub-folder here contains a separate Terraform stack that manages a single data export job.
These jobs are implemented using the [stats/db-s3-glue](https://git.xarth.tv/stats/db-s3-glue/) Terraform module, and **require Terraform 0.13.x**.

This implementation is inspired by the [subs/db-export-terracode](https://git.xarth.tv/subs/db-export-terracode) repo.
More info may also be found on the [subs/docs portal](https://git.xarth.tv/pages/subs/docs/projects/db-s3-glue/).  
