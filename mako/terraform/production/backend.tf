terraform {
  backend "s3" {
    bucket         = "mako-prod"
    key            = "tfstate/environments/production/terraform.tfstate"
    region         = "us-west-2"
    profile        = "mako-prod"
    encrypt        = true
    dynamodb_table = "terraform-locks"
  }
}

