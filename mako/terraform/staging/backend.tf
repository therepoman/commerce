terraform {
  backend "s3" {
    bucket         = "mako-devo"
    key            = "tfstate/environments/staging/terraform.tfstate"
    region         = "us-west-2"
    profile        = "mako-devo"
    encrypt        = true
    dynamodb_table = "terraform-locks"
  }
}

