# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/aws" {
  version     = "2.62.0"
  constraints = "~> 2.62.0"
  hashes = [
    "h1:lXmrtNsBKUPMdu4lvhsbW+m35GpUxRPAJ5Jf1ulW56E=",
    "zh:19ca5001841cccbe5581098349178033ad9ef2833218f05641284b538a282f6a",
    "zh:2a50345ee497dad4b52e6c3c2ff23731d12b9d7df625ea5658e228d1538acbeb",
    "zh:35447209ebfca371e0f8af8933accfc5bcae0aaec960a376e02599147b6e878f",
    "zh:53d12f59deb6bcda669f106d0e4e3940624d1bf9b710e79745268b71706cf6ca",
    "zh:66655e45f815d61356be861d6bbac3edbae8660654ec7dfce1457343abcb2269",
    "zh:8731c73e91ac3c2f41db3384b4a5454401b6c1ca1545b4f34af3716c1b172529",
    "zh:9211a9b6afea780267a2ec8c1a132e01fc5f9f2a249dd7cbf2925f80e167334e",
    "zh:a4878b95a26bf520458cae5c848be2b1f25e30dd3c4c41751f85379eca0f20f3",
    "zh:b9508b1f9c6493d0bcaf304f1df61368f51a9d23830d6d777b87fa0f7d6cd7c3",
    "zh:b95a36f5fadbbe1f66ce695f1b11a073a9055a388c8bead4b29e78c9cc02765b",
    "zh:de10d1887e571cfba8cbd0e146e3f9fb14b618847eaa9ef9c281f04fc5d2e0a8",
    "zh:deff3b5939387905468954de344cb653b6b6c759209f26d96e0b36beac2ebc96",
  ]
}
