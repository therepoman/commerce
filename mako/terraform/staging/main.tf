module "mako" {
  source                   = "../modules/mako"
  account_name             = "mako-devo"
  owner                    = "mako-devo@amazon.com"
  aws_profile              = "mako-devo"
  env                      = "staging"
  vpc_id                   = "vpc-b0f164d7"
  private_subnets          = "subnet-f9fd3db0,subnet-c25e709a,subnet-8d49e7ea"
  sg_id                    = "sg-cbef7fb3"
  entitle_read_capacity    = 100
  upload_service_arn       = "arn:aws:iam::995367761609:role/staging-upload-service"
  entitle_max_read         = 200
  entitle_min_read         = 100
  entitle_max_write        = 10
  entitle_min_write        = 5
  dynamo_autoscaling_role  = "arn:aws:iam::077362005997:role/aws-service-role/dynamodb.application-autoscaling.amazonaws.com/AWSServiceRoleForApplicationAutoScaling_DynamoDBTable"
  redis_instance_size      = "cache.r4.large"
  redis_groups             = 1
  redis_replicas_per_group = 2
  prefix_update_sns_topic  = "arn:aws:sns:us-west-2:958836777662:emoticon_prefix_update_staging"
  cert_arn                 = "arn:aws:acm:us-west-2:077362005997:certificate/8ca5e237-df68-4f6d-814a-5f0a84c1df4e"
  cert_arn_beta            = "arn:aws:acm:us-west-2:077362005997:certificate/93a9e590-6115-4937-b3e9-f3c4459a8bb9"
  capacity_planning_arn    = "arn:aws:iam::578510050023:role/twitch-inventory-master"
}

