region = "us-west-2"

environment = "production"

aws_profile = "mako-prod"

vpc_id = "vpc-b8e471df"

security_group = "sg-ea8f1f92"

subnets = "subnet-b808c8f1,subnet-195df37e,subnet-6557793d"

sandstorm_account_id = "734326455073"

upload_service_arn = "arn:aws:iam::435569175256:role/prod-upload-service"

capacity_planning_arn = "arn:aws:iam::578510050023:role/twitch-inventory-master"
