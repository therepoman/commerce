variable "queue_name" {
  type        = string
  description = "the name of the sqs queue"
}

variable "dead_letter_queue_name" {
  type        = string
  description = "the name of the sqs queue that will act as the dead letter queue"
}

variable "receive_wait_time_seconds" {
  type        = string
  default     = 20
  description = "the amount of time an sqs worker will wait for a message to arrive in a single ReceiveMessage action"
}

variable "message_retention_seconds" {
  type        = string
  default     = 1209600
  description = "the amount of time sqs will retain a given message on this queue"
}

variable "max_receive_count" {
  type        = string
  default     = 5
  description = "the number of times a message will be delivered before it is moved to a DLQ"
}

variable "delay_seconds" {
  type        = string
  default     = 0
  description = "the amount of time the delivery of a given message is delayed"
}

variable "visibility_timeout_seconds" {
  type        = string
  default     = 30
  description = "the amount of time SQS will keep a given message hidden from other workers after being received"
}

variable "policy_document" {
  type        = string
  default     = ""
  description = "the IAM policy to apply to this queue"
}

variable "kms_master_key_id" {
  type        = string
  default     = ""
  description = "the KMS master key ID used for encryption of messages"
}
