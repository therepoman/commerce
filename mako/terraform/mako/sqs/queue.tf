resource "aws_sqs_queue" "sqs_queue" {
  message_retention_seconds  = var.message_retention_seconds
  name                       = var.queue_name
  receive_wait_time_seconds  = var.receive_wait_time_seconds
  delay_seconds              = var.delay_seconds
  visibility_timeout_seconds = var.visibility_timeout_seconds

  policy            = var.policy_document
  kms_master_key_id = var.kms_master_key_id

  redrive_policy = <<EOF
  {
    "deadLetterTargetArn": "${aws_sqs_queue.sqs_queue_deadletter.arn}",
    "maxReceiveCount":     ${var.max_receive_count}
  }
  EOF
}

resource "aws_sqs_queue" "sqs_queue_deadletter" {
  name              = var.dead_letter_queue_name
  kms_master_key_id = var.kms_master_key_id
}

output "queue_arn" {
  value = aws_sqs_queue.sqs_queue.arn
}
