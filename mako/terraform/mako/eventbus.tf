data "http" "eventbus_cloudformation" {
  url = "https://eventbus-setup.s3-us-west-2.amazonaws.com/cloudformation.yaml"
}

resource "aws_cloudformation_stack" "eventbus" {
  name          = "EventBus"
  capabilities  = ["CAPABILITY_NAMED_IAM"]
  template_body = data.http.eventbus_cloudformation.body
}
