output "s3_emoticon_uploads" {
  value = aws_s3_bucket.s3_emoticon_uploads
}

output "default_emote_images_data_bucket_arn" {
  value = aws_s3_bucket.default_emote_images_data.arn
}

output "autoprof_bucket_arn" {
  value = aws_s3_bucket.autoprof.arn
}
