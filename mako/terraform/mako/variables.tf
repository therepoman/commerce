variable "vpc_id" {}

variable "region" {}

variable "security_group" {}

variable "environment" {}

variable "subnets" {}

variable "cluster" {
  type = string
}

variable "dns" {
  default     = ""
  description = "Twitch DNS to use for SSL certificates (e.g., mako-production.internal.justin.tv)"
}

variable "sandstorm_account_id" {
  type = string
}

variable "tag" {
  type    = string
  default = "latest"
}

variable "name" {
  type    = string
  default = ""
}

variable "aws_profile" {}

variable "pager_duty_sns_arn" {
  default     = ""
  description = "ARN of the SNS topic used for Pager Duty alarms"
  type        = string
}

variable "capacity_planning_arn" {}

variable "upload_service_arn" {}
