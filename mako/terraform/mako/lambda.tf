locals {
  lambda_env_vars = {}
}

/*-------------------------- ROLES --------------------------*/
module "lambda-role" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//lambda-role?ref=e80ce001454204ae42037e5aef41040ad766f2e0"

  name = "mako"
}

module "lambda-animated-emote-s3-role" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//lambda-role?ref=e80ce001454204ae42037e5aef41040ad766f2e0"

  name = "mako-animated-emote"
}

module "lambda-delete-emote-assets-from-s3-role" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//lambda-role?ref=e80ce001454204ae42037e5aef41040ad766f2e0"

  name = "mako-delete-from-s3-lambda-role"
}

/*-------------------------- POLICIES --------------------------*/
// Attach the stream policy to the role used by the lambdas.
resource "aws_iam_role_policy_attachment" "dynamodb-stream-policy-attachment" {
  role       = module.lambda-role.name
  policy_arn = module.dynamodb.stream-policy-arn
}

resource "aws_iam_role_policy_attachment" "s3_emote_bucket_access-attachment" {
  role       = module.lambda-animated-emote-s3-role.name
  policy_arn = aws_iam_policy.s3_emote_read_access.arn
}

data "aws_iam_policy_document" "pdms_lambda_policy_document" {
  statement {
    actions = [
      "sts:AssumeRole",
    ]

    resources = [
      local.pdms_caller_role_arn[var.environment],
    ]

    effect = "Allow"
  }
}

resource "aws_iam_policy" "pdms_lambda_policy" {
  name        = "mako-pdms-lambda-policy-${local.env_prefix[var.environment]}"
  policy      = data.aws_iam_policy_document.pdms_lambda_policy_document.json
  description = "IAM policy to allow assuming the PDMS Lambda caller role"
}

resource "aws_iam_role_policy_attachment" "pdms_lambda_policy_attachment" {
  role       = module.lambda-role.name
  policy_arn = aws_iam_policy.pdms_lambda_policy.arn
}

data "aws_iam_policy_document" "dynamo_lambda_policy_document" {
  statement {
    actions = [
      "dynamodb:DeleteItem",
      "dynamodb:Query",
      "dynamodb:Scan",
    ]

    resources = [
      "*",
    ]

    effect = "Allow"
  }
}

resource "aws_iam_policy" "dynamo_lambda_policy" {
  name        = "mako-dynamo-lambda-policy-${local.env_prefix[var.environment]}"
  policy      = data.aws_iam_policy_document.dynamo_lambda_policy_document.json
  description = "IAM policy to allow lambdas to read and write to DynamoDB tables"
}

resource "aws_iam_role_policy_attachment" "dynamo_lambda_policy_attachment" {
  role       = module.lambda-role.name
  policy_arn = aws_iam_policy.dynamo_lambda_policy.arn
}

// NOTE: Add more policy attachments below as necessary.
data "aws_iam_policy_document" "all_emotes_export_lambda_s3_policy_document" {
  statement {
    effect = "Allow"

    actions   = ["s3:PutObject", "s3:PutObjectAcl"]
    resources = ["${data.aws_s3_bucket.s3_emoticon_uploads.arn}/*"]
  }
  statement {
    effect = "Allow"

    actions   = ["s3:GetObject"]
    resources = ["${data.aws_s3_bucket.s3_emoticon_uploads.arn}/*"]
  }
}

resource "aws_iam_policy" "all_emotes_export_lambda_s3_policy" {
  name   = "mako-all-emotes-export-lambda-s3-policy-${local.env_prefix[var.environment]}"
  policy = data.aws_iam_policy_document.all_emotes_export_lambda_s3_policy_document.json
}

resource "aws_iam_role_policy_attachment" "all_emotes_export_lambda_s3_policy_attachment" {
  role       = module.lambda-role.name
  policy_arn = aws_iam_policy.all_emotes_export_lambda_s3_policy.arn
}

// NOTE: Add more policy attachments below as necessary.
data "aws_iam_policy_document" "delete_emotes_from_s3_policy_document" {
  statement {
    effect = "Allow"

    actions = [
      "sqs:GetQueueAttributes",
      "sqs:SendMessage",
      "sqs:ReceiveMessage",
      "sqs:DeleteMessage"
    ]
    resources = [module.delete_emote_assets_from_s3_queue.queue_arn]
  }

  statement {
    effect = "Allow"

    actions = [
      "s3:ListBucket",
    ]
    resources = [
      data.aws_s3_bucket.s3_emoticon_uploads.arn,
      aws_s3_bucket.s3_animated_emote_frames.arn
    ]
  }

  statement {
    effect = "Allow"

    actions = [
      "s3:GetObject",
      "s3:DeleteObject",
    ]
    resources = [
      "${data.aws_s3_bucket.s3_emoticon_uploads.arn}/*",
      "${aws_s3_bucket.s3_animated_emote_frames.arn}/*",
    ]
  }
}

resource "aws_iam_policy" "delete_emotes_from_s3_policy" {
  name   = "delete_emotes_from_s3_policy-${local.env_prefix[var.environment]}"
  policy = data.aws_iam_policy_document.delete_emotes_from_s3_policy_document.json
}

resource "aws_iam_role_policy_attachment" "sqs_attachment" {
  role       = module.lambda-delete-emote-assets-from-s3-role.name
  policy_arn = aws_iam_policy.delete_emotes_from_s3_policy.arn
}

resource "aws_iam_role_policy_attachment" "lambda_eventbus_access_attach" {
  role       = module.lambda-role.name
  policy_arn = aws_cloudformation_stack.eventbus.outputs["EventBusAccessPolicyARN"]
}


/*-------------------------- LAMBDA PERMISSIONS --------------------------*/
resource "aws_lambda_permission" "allow_cloudwatch_to_call_all_emotes_lambda" {
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  function_name = module.all-emotes-export-lambda.arn
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.every-day-at-9am-and-5pm.arn
}

/*-------------------------- S3 BUCKET FOR CODE --------------------------*/
module "lambda-bucket" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//lambda-s3-bucket?ref=e80ce001454204ae42037e5aef41040ad766f2e0"

  name        = "twitch-mako"
  environment = var.environment
  role_arn    = module.lambda-role.arn
}

/*-------------------------- LAMBDAS --------------------------*/
module "publish-emote-modifier-groups-spade-event" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//lambda?ref=e80ce001454204ae42037e5aef41040ad766f2e0"

  name            = "publish_emote_modifier_groups_spade_event"
  handler         = "publish_emote_modifier_groups_spade_event"
  description     = "Lambda to trigger spade events for changes to the emote modifier groups DynamoDB table."
  namespace       = "mako"
  tag             = "latest"
  environment     = var.environment
  bucket          = module.lambda-bucket.bucket
  role_arn        = module.lambda-role.arn
  subnet_ids      = split(",", var.subnets)
  security_groups = [var.security_group]
  env_vars        = local.lambda_env_vars
}

module "publish-emotes-spade-event" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//lambda?ref=e80ce001454204ae42037e5aef41040ad766f2e0"

  name            = "publish_emotes_spade_event"
  handler         = "publish_emotes_spade_event"
  description     = "Lambda to trigger spade events for changes to the emotes DynamoDB table."
  namespace       = "mako"
  tag             = "latest"
  environment     = var.environment
  bucket          = module.lambda-bucket.bucket
  role_arn        = module.lambda-role.arn
  subnet_ids      = split(",", var.subnets)
  security_groups = [var.security_group]
  env_vars        = local.lambda_env_vars
}

module "split-animated-emote-gif-frames" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//lambda?ref=e80ce001454204ae42037e5aef41040ad766f2e0"

  name            = "split_animated_emote_gif_frames"
  handler         = "split_animated_emote_gif_frames"
  description     = "Lambda to extract and publish the frames from an animated emote gif asset."
  namespace       = "mako"
  tag             = "latest"
  environment     = var.environment
  bucket          = module.lambda-bucket.bucket
  role_arn        = module.lambda-animated-emote-s3-role.arn
  subnet_ids      = split(",", var.subnets)
  security_groups = [var.security_group]
  env_vars        = local.lambda_env_vars
  timeout         = "30"
}

module "user-destroy-lambda" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//lambda?ref=e80ce001454204ae42037e5aef41040ad766f2e0"

  name            = "user_destroy"
  handler         = "user_destroy"
  description     = "Lambda to delete Mako user's data in response to a UserDestroy EventBus event."
  namespace       = "mako"
  tag             = "latest"
  environment     = var.environment
  bucket          = module.lambda-bucket.bucket
  role_arn        = module.lambda-role.arn
  subnet_ids      = split(",", var.subnets)
  security_groups = [var.security_group]
  env_vars        = local.lambda_env_vars
  timeout         = "300"
}

module "pdms-report-deletion-lambda" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//lambda?ref=e80ce001454204ae42037e5aef41040ad766f2e0"

  name            = "pdms_report_deletion"
  handler         = "pdms_report_deletion"
  description     = "Lambda to call PDMS to report that a userss Mako data has been deleted."
  namespace       = "mako"
  tag             = "latest"
  environment     = var.environment
  bucket          = module.lambda-bucket.bucket
  role_arn        = module.lambda-role.arn
  subnet_ids      = split(",", var.subnets)
  security_groups = [var.security_group]
  env_vars        = local.lambda_env_vars
  timeout         = "30"
}

module "all-emotes-export-lambda" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//lambda?ref=e80ce001454204ae42037e5aef41040ad766f2e0"

  name            = "all_emotes_export"
  handler         = "all_emotes_export"
  description     = "Lambda to export emotes data."
  namespace       = "mako"
  tag             = "latest"
  environment     = var.environment
  bucket          = module.lambda-bucket.bucket
  role_arn        = module.lambda-role.arn
  subnet_ids      = split(",", var.subnets)
  security_groups = [var.security_group]
  env_vars        = local.lambda_env_vars
  timeout         = "900"   //15 minutes
  memory_size     = "10240" //10,240 MB
}

module "delete_emote_assets_from_s3_lambda" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//lambda?ref=e80ce001454204ae42037e5aef41040ad766f2e0"

  name            = "delete_emote_assets_from_s3"
  handler         = "delete_emote_assets_from_s3"
  description     = "Lambda to delete emote assets from s3 upon emote deletion."
  namespace       = "mako"
  tag             = "latest"
  environment     = var.environment
  bucket          = module.lambda-bucket.bucket
  role_arn        = module.lambda-delete-emote-assets-from-s3-role.arn
  subnet_ids      = split(",", var.subnets)
  security_groups = [var.security_group]
  env_vars        = local.lambda_env_vars
  timeout         = "60"
}

module "two_factor_entitlements_lambda" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//lambda?ref=e80ce001454204ae42037e5aef41040ad766f2e0"

  name            = "two_factor_entitlements"
  handler         = "two_factor_entitlements"
  description     = "Lambda to handle 2FA emote reward entitlement events."
  namespace       = "mako"
  tag             = "latest"
  environment     = var.environment
  bucket          = module.lambda-bucket.bucket
  role_arn        = module.lambda-role.arn
  subnet_ids      = split(",", var.subnets)
  security_groups = [var.security_group]
  env_vars        = local.lambda_env_vars
  timeout         = "60"
}

/*-------------------------- LAMBDA TRIGGERS --------------------------*/
resource "aws_lambda_event_source_mapping" "publish-emote-modifier-groups-spade-event-lambda-mapping" {
  event_source_arn  = module.dynamodb.emote-modifier-groups-stream-arn
  function_name     = module.publish-emote-modifier-groups-spade-event.arn
  starting_position = "LATEST"
}

resource "aws_lambda_event_source_mapping" "publish-emotes-spade-event-lambda-mapping" {
  event_source_arn  = module.dynamodb.emotes-stream-arn
  function_name     = module.publish-emotes-spade-event.arn
  starting_position = "LATEST"
}

resource "aws_cloudwatch_event_target" "all-emotes-lambda" {
  // only run cron on production
  count     = var.environment == "production" ? 1 : 0
  rule      = aws_cloudwatch_event_rule.every-day-at-9am-and-5pm.name
  target_id = "SendToAllEmotesLambda"
  arn       = module.all-emotes-export-lambda.arn
}

resource "aws_lambda_event_source_mapping" "delete_emote_assets_from_s3_mapping" {
  event_source_arn = module.delete_emote_assets_from_s3_queue.queue_arn
  function_name    = module.delete_emote_assets_from_s3_lambda.arn

  // In order to facilitate simple retries/DLQ, the lambda can only handle a single message at a time.
  batch_size = 1
}

resource "aws_lambda_event_source_mapping" "two_factor_entitlements_eventbus_mapping" {
  event_source_arn = module.eventbus_two_factor_entitlements_queue.queue_arn
  function_name    = module.two_factor_entitlements_lambda.arn
}

/*-------------------------- LAMBDA SCHEDULERS --------------------------*/
resource "aws_cloudwatch_event_rule" "every-day-at-9am-and-5pm" {
  name        = "twice-a-day-at-9am-and-5pm"
  description = "Emit an event twice a day at 9am and 5pm PST"

  schedule_expression = "cron(0 0,16 * * ? *)" // midnight UTC and 4pm UTC == 9am PST and 5pm PST
}
