locals {
  sandstorm_role = {
    production = "mako-prod"
    staging    = "mako-staging"
  }

  config_bucket = {
    production = "mako-prod-config"
    staging    = "mako-staging-config"
  }

  policy     = var.name != "" ? format("mako-service-policy-%s", var.name) : "mako-service-policy"
  attachment = var.name != "" ? format("mako-ecs--%s", var.name) : "mako-ecs"

  pdms_caller_role_arn = {
    production = "arn:aws:iam::125704643346:role/PDMSLambda-CallerRole-13IIND444YKVR"
    staging    = "arn:aws:iam::895799599216:role/PDMSLambda-CallerRole-18451FI19HSXT"
  }
}

resource "aws_iam_policy" "mako" {
  name = local.policy
  path = "/"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
    {
        "Effect": "Allow",
        "Action": "sts:AssumeRole",
        "Resource": "arn:aws:iam::${var.sandstorm_account_id}:role/sandstorm/production/templated/role/${local.sandstorm_role[var.environment]}"
    },
    {
        "Effect": "Allow",
        "Action": "sqs:ListQueues",
        "Resource": "*"
    },
    {
        "Effect": "Allow",
        "Action": [
            "ssm:GetParameterHistory",
            "ssm:GetParametersByPath",
            "ssm:GetParameters",
            "ssm:GetParameter",
            "ssm:DescribeParameters"
        ],
        "Resource": "*"
    },
    {
        "Action": [
            "sqs:*",
            "cloudwatch:*"
        ],
        "Effect": "Allow",
        "Resource": "*"
    },
    {
        "Effect": "Allow",
        "Action": [
            "s3:*"
        ],
        "Resource": [
            "arn:aws:s3:::${local.config_bucket[var.environment]}/*",
            "arn:aws:s3:::${local.config_bucket[var.environment]}",
            "*",
            "${module.s3.autoprof_bucket_arn}/*",
            "${module.s3.default_emote_images_data_bucket_arn}/*",
            "${aws_s3_bucket.experiment_overrides.arn}/*"
        ]
    },
    {
        "Effect": "Allow",
        "Action": "s3:GetObject",
        "Resource": [
            "arn:aws:s3:::minixperiment-prod/*"
        ]
    },
    {
        "Effect": "Allow",
        "Action": [
            "dax:*",
            "dynamodb:*",
            "sns:*",
            "states:StartExecution",
            "lambda:InvokeFunction"
        ],
        "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_policy_attachment" "mako-ecs" {
  name       = local.attachment
  roles      = [module.mako.service_role, module.mako.canary_role]
  policy_arn = aws_iam_policy.mako.arn
}

// S2S Assume Role Policy and attachments
data "aws_iam_policy_document" "s2s_policy" {
  statement {
    actions = [
      "sts:AssumeRole",
    ]

    resources = [
      "arn:aws:iam::180116294062:role/malachai/*",
    ]

    effect = "Allow"
  }
}

resource "aws_iam_policy" "s2s_policy" {
  name   = "mako-s2s-policy"
  policy = data.aws_iam_policy_document.s2s_policy.json
}

resource "aws_iam_role_policy_attachment" "s2s_policy_attachment" {
  role       = module.mako.service_role
  policy_arn = aws_iam_policy.s2s_policy.arn
}

resource "aws_iam_role_policy_attachment" "s2s_policy_attachment_canary" {
  role       = module.mako.canary_role
  policy_arn = aws_iam_policy.s2s_policy.arn
}

resource "aws_iam_role_policy_attachment" "eventbus_access_attach" {
  role       = module.mako.service_role
  policy_arn = aws_cloudformation_stack.eventbus.outputs["EventBusAccessPolicyARN"]
}

resource "aws_iam_role_policy_attachment" "eventbus_access_attach_canary" {
  role       = module.mako.canary_role
  policy_arn = aws_cloudformation_stack.eventbus.outputs["EventBusAccessPolicyARN"]
}
