locals {
  user_destroy_sfn_arns = {
    production = "arn:aws:states:us-west-2:671452935478:stateMachine:user_destroy_sfn_prod"
    staging    = "arn:aws:states:us-west-2:077362005997:stateMachine:user_destroy_sfn_staging"
  }

  gif_frame_splitting_arns = {
    production = "arn:aws:states:us-west-2:671452935478:stateMachine:gif_frame_splitting_sfn_prod"
    staging    = "arn:aws:states:us-west-2:077362005997:stateMachine:gif_frame_splitting_sfn_staging"
  }

  sfn_default_retry = <<EOF
    [
      {
        "ErrorEquals": [
          "States.ALL"
        ],
        "IntervalSeconds": 1,
        "MaxAttempts": 2,
        "BackoffRate": 2.0
      }
    ]
  EOF
}

/*-------------------------- POLICIES --------------------------*/
data "aws_iam_policy_document" "lambda_execution_policy_document" {
  statement {
    actions = [
      "lambda:InvokeFunction",
      "states:StartExecution",
    ]

    effect = "Allow"

    resources = [
      "*",
    ]
  }
}

resource "aws_iam_policy" "step_functions_policy" {
  name        = "MakoStepFunctionsPolicy"
  description = "Step function policy to invoke Mako Lambda functions"
  policy      = data.aws_iam_policy_document.lambda_execution_policy_document.json
}

data "aws_iam_policy_document" "sfn_assume_role_policy_document" {
  statement {
    actions = [
      "sts:AssumeRole",
    ]

    principals {
      type = "Service"

      identifiers = [
        "states.amazonaws.com",
      ]
    }
  }
}

/*-------------------------- ROLES --------------------------*/
resource "aws_iam_role" "step_functions_role" {
  name               = "mako_step_functions_role"
  assume_role_policy = data.aws_iam_policy_document.sfn_assume_role_policy_document.json
}

resource "aws_iam_role_policy_attachment" "step-functions-role-policy-attach" {
  role       = aws_iam_role.step_functions_role.name
  policy_arn = aws_iam_policy.step_functions_policy.arn
}

/*-------------------------- STEP FUNCTIONS --------------------------*/
module "user_destroy_sfn" {
  source             = "./sfn"
  sfn_name           = "user_destroy_sfn_${local.env_prefix[var.environment]}"
  sfn_role_arn       = aws_iam_role.step_functions_role.arn
  pager_duty_sns_arn = var.pager_duty_sns_arn
  sfn_arn            = local.user_destroy_sfn_arns[var.environment]

  sfn_definition = <<EOF
{
  "Comment": "This step function handles work for the Eventbus UserDestroy event",
  "StartAt": "DeleteMakoData",
  "States": {
    "DeleteMakoData": {
      "Type": "Task",
      "Resource": "${module.user-destroy-lambda.lambda_arn}",
      "Next": "ReportDeletion",
      "Catch" : [{
        "ErrorEquals": ["States.ALL"],
        "Next": "FailState"
      }],
      "Retry": ${local.sfn_default_retry}
    },
    "ReportDeletion": {
      "Type": "Task",
      "End": true,
      "Resource": "${module.pdms-report-deletion-lambda.lambda_arn}",
      "Catch" : [{
        "ErrorEquals": ["States.ALL"],
        "Next": "FailState"
      }],
      "Retry": ${local.sfn_default_retry}
    },
    "FailState": {
      "Comment": "Failed execution",
      "Type": "Fail"
    }
  }
}
EOF
}

module "gif_frame_splitting_sf" {
  source             = "./sfn"
  sfn_name           = "gif_frame_splitting_sfn_${local.env_prefix[var.environment]}"
  sfn_role_arn       = aws_iam_role.step_functions_role.arn
  pager_duty_sns_arn = var.pager_duty_sns_arn
  sfn_arn            = local.gif_frame_splitting_arns[var.environment]
  sfn_definition     = <<EOF
{
  "Comment": "This step function handles the gif frame splitting process for animated emotes.",
  "StartAt": "SplitFrames",
  "States": {
    "SplitFrames": {
      "Type": "Task",
      "Resource": "${module.split-animated-emote-gif-frames.lambda_arn}",
      "End": true,
      "Catch" : [{
        "ErrorEquals": ["States.ALL"],
        "Next": "FailState"
      }],
      "Retry": [{
        "ErrorEquals": [
          "States.ALL"
        ],
        "IntervalSeconds": 1,
        "MaxAttempts": 4,
        "BackoffRate": 2.0
      }
    ]},
    "FailState": {
      "Comment": "Failed execution",
      "Type": "Fail"
    }
  }
}
EOF
}