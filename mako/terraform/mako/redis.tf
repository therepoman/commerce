locals {
  instance = {
    production = "cache.r5.4xlarge"
    staging    = "cache.r5.4xlarge"
  }

  replicas = {
    production = 5
    staging    = 1
  }

  nodes = {
    production = 6
    staging    = 3
  }
}

module "emotes" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//redis?ref=e80ce001454204ae42037e5aef41040ad766f2e0"

  name               = "emotedb-cache-${var.environment}"
  replicas_per_group = local.replicas[var.environment]
  node_groups        = local.nodes[var.environment]

  instance_type   = local.instance[var.environment]
  subnets         = var.subnets
  security_groups = var.security_group
  environment     = var.environment
}

resource "aws_cloudwatch_metric_alarm" "redis-cpu-utilization-low-sev" {
  alarm_name          = "redis-cpu-utilization-low-sev"
  alarm_description   = "[LowSev] Alarms when our redis cluster's average CPU utilization is high."
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = "5"
  datapoints_to_alarm = "5"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/ElastiCache"

  period             = "60"
  statistic          = "Average"
  threshold          = "50"
  treat_missing_data = "breaching"
  actions_enabled    = length(var.pager_duty_sns_arn) > 0 ? true : false
  alarm_actions      = [var.pager_duty_sns_arn]
  ok_actions         = [var.pager_duty_sns_arn]
}

resource "aws_cloudwatch_metric_alarm" "redis-engine-cpu-utilization-low-sev" {
  alarm_name          = "redis-engine-cpu-utilization-low-sev"
  alarm_description   = "[LowSev] Alarms when our redis cluster's average engine CPU utilization is high."
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = "5"
  datapoints_to_alarm = "5"
  metric_name         = "EngineCPUUtilization"
  namespace           = "AWS/ElastiCache"

  period             = "60"
  statistic          = "Average"
  threshold          = "75"
  treat_missing_data = "breaching"
  actions_enabled    = length(var.pager_duty_sns_arn) > 0 ? true : false
  alarm_actions      = [var.pager_duty_sns_arn]
  ok_actions         = [var.pager_duty_sns_arn]
}

resource "aws_cloudwatch_metric_alarm" "redis-cpu-utilization-avg" {
  alarm_name          = "redis-cpu-utilization-avg"
  alarm_description   = "Alarms when our redis cluster's average CPU utilization is high."
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = "5"
  datapoints_to_alarm = "5"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/ElastiCache"

  period             = "60"
  statistic          = "Average"
  threshold          = "75"
  treat_missing_data = "breaching"
  actions_enabled    = length(var.pager_duty_sns_arn) > 0 ? true : false
  alarm_actions      = [var.pager_duty_sns_arn]
  ok_actions         = [var.pager_duty_sns_arn]
}

resource "aws_cloudwatch_metric_alarm" "redis-cpu-utilization-max" {
  alarm_name          = "redis-cpu-utilization-max"
  alarm_description   = "Alarms when our redis cluster's max CPU utilization is high."
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = "5"
  datapoints_to_alarm = "5"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/ElastiCache"

  period             = "60"
  statistic          = "Maximum"
  threshold          = "90"
  treat_missing_data = "breaching"
  actions_enabled    = length(var.pager_duty_sns_arn) > 0 ? true : false
  alarm_actions      = [var.pager_duty_sns_arn]
  ok_actions         = [var.pager_duty_sns_arn]
}

resource "aws_cloudwatch_metric_alarm" "redis-engine-cpu-utilization-avg" {
  alarm_name          = "redis-engine-cpu-utilization-avg"
  alarm_description   = "Alarms when our redis cluster's average engine CPU utilization is high."
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = "5"
  datapoints_to_alarm = "5"
  metric_name         = "EngineCPUUtilization"
  namespace           = "AWS/ElastiCache"

  period             = "60"
  statistic          = "Average"
  threshold          = "90"
  treat_missing_data = "breaching"
  actions_enabled    = length(var.pager_duty_sns_arn) > 0 ? true : false
  alarm_actions      = [var.pager_duty_sns_arn]
  ok_actions         = [var.pager_duty_sns_arn]
}

resource "aws_cloudwatch_metric_alarm" "redis-engine-cpu-utilization-max" {
  alarm_name          = "redis-engine-cpu-utilization-max"
  alarm_description   = "Alarms when our redis cluster's max engine CPU utilization is high."
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = "5"
  datapoints_to_alarm = "5"
  metric_name         = "EngineCPUUtilization"
  namespace           = "AWS/ElastiCache"

  period             = "60"
  statistic          = "Maximum"
  threshold          = "95"
  treat_missing_data = "breaching"
  actions_enabled    = length(var.pager_duty_sns_arn) > 0 ? true : false
  alarm_actions      = [var.pager_duty_sns_arn]
  ok_actions         = [var.pager_duty_sns_arn]
}

resource "aws_cloudwatch_metric_alarm" "redis-memory-usage-percentage" {
  alarm_name          = "redis-memory-usage-percentage"
  alarm_description   = "Alarms when our redis cluster's memory utilization is high."
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = "5"
  datapoints_to_alarm = "5"
  metric_name         = "DatabaseMemoryUsagePercentage"
  namespace           = "AWS/ElastiCache"

  period             = "60"
  statistic          = "Maximum"
  threshold          = "75"
  treat_missing_data = "breaching"
  actions_enabled    = length(var.pager_duty_sns_arn) > 0 ? true : false
  alarm_actions      = [var.pager_duty_sns_arn]
  ok_actions         = [var.pager_duty_sns_arn]
}
