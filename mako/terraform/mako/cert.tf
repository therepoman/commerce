module "cert" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//privatelink-cert?ref=e80ce001454204ae42037e5aef41040ad766f2e0"

  name        = "mako"
  environment = var.environment
  zone_id     = module.route53-zone.zone_id
  alb_dns     = module.mako.dns
}
