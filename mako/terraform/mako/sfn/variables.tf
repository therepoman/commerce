variable "sfn_name" {
  type        = string
  description = "name of the step function to be created"
}

# Note: This variable value must be constructed by hand. The terraform alarms for Step Functions require the
# Step Function ARN to be passed to them, but when using this module for the first time, the Step Function has
# not been created yet. AWS Step Function ARNs have the following format:
# arn:aws:states:<region>:<aws_account_id>:stateMachine:<step_function_name>
variable "sfn_arn" {
  type        = string
  default     = ""
  description = "ARN of the step function that was created"
}

variable "sfn_role_arn" {
  type        = string
  description = "ARN of the IAM role to use for this state machine"
}

variable "sfn_definition" {
  description = "JSON of Amazon States Language that describes the step function to be created"
}

variable "pager_duty_sns_arn" {
  type        = string
  description = "ARN of the sns topic to use for pager duty actions"
}
