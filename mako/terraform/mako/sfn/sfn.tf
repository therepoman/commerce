resource "aws_sfn_state_machine" "sfn" {
  name       = var.sfn_name
  role_arn   = var.sfn_role_arn
  definition = var.sfn_definition
}

resource "aws_cloudwatch_metric_alarm" "sfn_executions_failed" {
  alarm_name                = "${aws_sfn_state_machine.sfn.name}-executions-failed"
  alarm_description         = "[LowSev] A step function execution has failed."
  comparison_operator       = "GreaterThanThreshold"
  evaluation_periods        = "1"
  datapoints_to_alarm       = "1"
  metric_name               = "ExecutionsFailed"
  namespace                 = "AWS/States"
  period                    = "300"
  statistic                 = "Sum"
  threshold                 = "0"
  unit                      = "Count"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"
  alarm_actions             = [var.pager_duty_sns_arn]
  ok_actions                = [var.pager_duty_sns_arn]

  dimensions = {
    StateMachineArn = var.sfn_arn
  }
}
