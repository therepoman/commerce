locals {
  dlq_suffix = "deadletter"

  env_prefix = {
    production = "prod"
    staging    = "staging"
  }

  eventbus_user_destroy_queue_prefix = "eventbus_user_destroy"
  eventbus_user_destroy_queue_name   = "${local.eventbus_user_destroy_queue_prefix}_${local.env_prefix[var.environment]}"
  eventbus_user_destroy_dlq_name     = "${local.eventbus_user_destroy_queue_prefix}_${local.env_prefix[var.environment]}_${local.dlq_suffix}"

  delete_emote_assets_from_s3_queue_prefix   = "delete_emote_assets_from_s3"
  delete_emote_assets_from_s3_queue_name     = "${local.delete_emote_assets_from_s3_queue_prefix}_${local.env_prefix[var.environment]}"
  delete_emote_assets_from_s3_queue_dlq_name = "${local.delete_emote_assets_from_s3_queue_prefix}_${local.env_prefix[var.environment]}_${local.dlq_suffix}"

  eventbus_two_factor_entitlements_queue_prefix = "eventbus_two_factor_entitlements"
  eventbus_two_factor_entitlements_queue_name   = "${local.eventbus_two_factor_entitlements_queue_prefix}_${local.env_prefix[var.environment]}"
  eventbus_two_factor_entitlements_dlq_name     = "${local.eventbus_two_factor_entitlements_queue_prefix}_${local.env_prefix[var.environment]}_${local.dlq_suffix}"
}

module "eventbus_user_destroy_queue" {
  source                     = "./sqs"
  queue_name                 = local.eventbus_user_destroy_queue_name
  dead_letter_queue_name     = local.eventbus_user_destroy_dlq_name
  visibility_timeout_seconds = 60
  policy_document            = aws_cloudformation_stack.eventbus.outputs["EventBusSQSPolicyDocument"]
  kms_master_key_id          = aws_cloudformation_stack.eventbus.outputs["EventBusKMSMasterKeyARN"]
}

module "eventbus_user_destroy_queue_alarms" {
  source                 = "./cloudwatch/sqs-alarms"
  queue_name             = local.eventbus_user_destroy_queue_name
  dead_letter_queue_name = local.eventbus_user_destroy_dlq_name
  pager_duty_sns_arn     = var.pager_duty_sns_arn
}

module "delete_emote_assets_from_s3_queue" {
  source                     = "./sqs"
  queue_name                 = local.delete_emote_assets_from_s3_queue_name
  dead_letter_queue_name     = local.delete_emote_assets_from_s3_queue_dlq_name
  visibility_timeout_seconds = 60
}

module "delete_emote_assets_from_s3_queue_alarms" {
  source                 = "./cloudwatch/sqs-alarms"
  queue_name             = local.delete_emote_assets_from_s3_queue_name
  dead_letter_queue_name = local.delete_emote_assets_from_s3_queue_dlq_name
  pager_duty_sns_arn     = var.pager_duty_sns_arn
}

module "eventbus_two_factor_entitlements_queue" {
  source                     = "./sqs"
  queue_name                 = local.eventbus_two_factor_entitlements_queue_name
  dead_letter_queue_name     = local.eventbus_two_factor_entitlements_dlq_name
  visibility_timeout_seconds = 60
  policy_document            = aws_cloudformation_stack.eventbus.outputs["EventBusSQSPolicyDocument"]
  kms_master_key_id          = aws_cloudformation_stack.eventbus.outputs["EventBusKMSMasterKeyARN"]
}
