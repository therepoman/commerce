locals {
  environment_prefix = {
    staging    = "beta"
    production = "prod"
  }

  // Allowlisted service ARNs
  voyager_arns = {
    staging    = "arn:aws:iam::411162717697:root"
    production = "arn:aws:iam::940853426594:root"
  }

  drops_fulfillment_service_arns = {
    staging    = "arn:aws:iam::920044005461:root"
    production = "arn:aws:iam::049096181636:root"
  }

  drops_management_service_arns = {
    staging    = "arn:aws:iam::426504223929:root"
    production = "arn:aws:iam::690832434380:root"
  }

  admin_panel_arns = {
    staging    = "arn:aws:iam::219087926005:root"
    production = "arn:aws:iam::196915980276:root"
  }

  toolkit_arns = {
    staging    = "arn:aws:iam::144996434858:root"
    production = "arn:aws:iam::849839200145:root"
  }

  popular_emotes_service_arns = {
    staging    = "arn:aws:iam::799470532062:root"
    production = "arn:aws:iam::433410480413:root"
  }

  creator_cards_arns = {
    staging    = "arn:aws:iam::725953065548:root"
    production = "arn:aws:iam::272934794774:root"
  }

  chat_arns = {
    staging    = "arn:aws:iam::960608786107:root"
    production = "arn:aws:iam::945359790842:root"
  }

  // Service endpoints
  benefits_service = {
    staging    = "com.amazonaws.vpce.us-west-2.vpce-svc-0c8a47de37b79467a"
    production = "com.amazonaws.vpce.us-west-2.vpce-svc-0ee265f1df3a8cce4"
  }

  materia_backfill_service = {
    staging    = "com.amazonaws.vpce.us-west-2.vpce-svc-0fa75b80ed4081a02"
    production = "com.amazonaws.vpce.us-west-2.vpce-svc-003f213f28f079f7e"
  }

  dartreceiver_service = {
    staging    = "com.amazonaws.vpce.us-west-2.vpce-svc-00cc5f5ddd42126a0"
    production = "com.amazonaws.vpce.us-west-2.vpce-svc-0ce6ec756b1a5acba"
  }

  following_service = {
    staging    = "com.amazonaws.vpce.us-west-2.vpce-svc-08682995a563a1413"
    production = "com.amazonaws.vpce.us-west-2.vpce-svc-02ed47f998f8f23a2"
  }

  payday_service = {
    staging    = "com.amazonaws.vpce.us-west-2.vpce-svc-019e5066db9348615"
    production = "com.amazonaws.vpce.us-west-2.vpce-svc-07a49906f8e043689"
  }

  // Service DNS
  payday_dns = {
    staging    = "main.us-west-2.beta.payday.twitch.a2z.com"
    production = "prod.payday.twitch.a2z.com"
  }
}

module "privatelink" {
  source      = "git::git+ssh://git@git.xarth.tv/subs/terracode//privatelink?ref=e80ce001454204ae42037e5aef41040ad766f2e0"
  name        = "mako"
  region      = var.region
  environment = var.environment
  alb_dns     = module.mako.dns
  cert_arn    = module.cert.arn
  vpc_id      = var.vpc_id
  subnets     = split(",", var.subnets)

  whitelisted_arns = [
    local.voyager_arns[var.environment],
    local.drops_fulfillment_service_arns[var.environment],
    local.drops_management_service_arns[var.environment],
    local.admin_panel_arns[var.environment],
    local.toolkit_arns[var.environment],
    local.popular_emotes_service_arns[var.environment],
    local.creator_cards_arns[var.environment],
    local.chat_arns[var.environment],
  ]
}

module "privatelink-benefits-service" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//privatelink-service?ref=e80ce001454204ae42037e5aef41040ad766f2e0"

  name            = "benefits"
  dns             = "us-west-2.${local.environment_prefix[var.environment]}.benefits.s.twitch.a2z.com"
  domain          = "s.twitch.a2z.com"
  endpoint        = local.benefits_service[var.environment]
  environment     = var.environment
  security_groups = [var.security_group]
  subnets         = split(",", var.subnets)
  vpc_id          = var.vpc_id
  zone            = "${local.environment_prefix[var.environment]}.benefits.s.twitch.a2z.com"
}

module "privatelink-dartreceiver-service" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//privatelink-service?ref=e80ce001454204ae42037e5aef41040ad766f2e0"

  name            = "dartreceiver"
  dns             = "us-west-2.${local.environment_prefix[var.environment]}.twitchdartreceiver.s.twitch.a2z.com"
  domain          = "s.twitch.a2z.com"
  endpoint        = local.dartreceiver_service[var.environment]
  environment     = var.environment
  security_groups = [var.security_group]
  subnets         = split(",", var.subnets)
  vpc_id          = var.vpc_id
  zone            = "${local.environment_prefix[var.environment]}.twitchdartreceiver.s.twitch.a2z.com"
}

module "privatelink-materia-backfill" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//privatelink-service?ref=e80ce001454204ae42037e5aef41040ad766f2e0"

  name            = "materiabackfill"
  dns             = "us-west-2.${local.environment_prefix[var.environment]}.backfillmateria.s.twitch.a2z.com"
  domain          = "s.twitch.a2z.com"
  endpoint        = local.materia_backfill_service[var.environment]
  environment     = var.environment
  security_groups = [var.security_group]
  subnets         = split(",", var.subnets)
  vpc_id          = var.vpc_id
  zone            = "${local.environment_prefix[var.environment]}.backfillmateria.s.twitch.a2z.com"
}

module "privatelink-materia-inc" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//privatelink-service?ref=e80ce001454204ae42037e5aef41040ad766f2e0"

  name            = "materiainc"
  dns             = "us-west-2.${local.environment_prefix[var.environment]}.materia-inc.s.twitch.a2z.com"
  domain          = "s.twitch.a2z.com"
  endpoint        = local.materia_backfill_service[var.environment]
  environment     = var.environment
  security_groups = [var.security_group]
  subnets         = split(",", var.subnets)
  vpc_id          = var.vpc_id
  zone            = "${local.environment_prefix[var.environment]}.materia-inc.s.twitch.a2z.com"
}

module "privatelink-following-service" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//privatelink-service?ref=e80ce001454204ae42037e5aef41040ad766f2e0"

  name            = "following"
  dns             = "us-west-2.${local.environment_prefix[var.environment]}.twitchvxfollowingserviceecs.s.twitch.a2z.com"
  domain          = "s.twitch.a2z.com"
  endpoint        = local.following_service[var.environment]
  environment     = var.environment
  security_groups = [var.security_group]
  subnets         = split(",", var.subnets)
  vpc_id          = var.vpc_id
  zone            = "${local.environment_prefix[var.environment]}.twitchvxfollowingserviceecs.s.twitch.a2z.com"
}

module "privatelink-payday" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//privatelink-service?ref=e80ce001454204ae42037e5aef41040ad766f2e0"

  name            = "payday"
  dns             = local.payday_dns[var.environment]
  domain          = "twitch.a2z.com"
  endpoint        = local.payday_service[var.environment]
  environment     = var.environment
  security_groups = [var.security_group]
  subnets         = split(",", var.subnets)
  vpc_id          = var.vpc_id
  zone            = "payday.twitch.a2z.com"
}

resource "aws_vpc_endpoint" "twitch-ldap-endpoint" {
  service_name        = "com.amazonaws.vpce.us-west-2.vpce-svc-0437151f68c61b808"
  security_group_ids  = [var.security_group]
  subnet_ids          = split(",", var.subnets)
  vpc_id              = var.vpc_id
  vpc_endpoint_type   = "Interface"
  private_dns_enabled = true

  tags = {
    Domain      = "twitch.a2z.com"
    Name        = "Twitch-LDAP"
    Environment = var.environment
  }

  lifecycle {
    prevent_destroy = true
  }
}
