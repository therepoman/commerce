locals {
  instances = {
    production = 65
    staging    = 4
  }

  max_asg_instances = 177
}
module "cluster" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//cluster?ref=e80ce001454204ae42037e5aef41040ad766f2e0"

  name           = "sonar"
  min            = local.instances[var.environment]
  vpc_id         = var.vpc_id
  instance_type  = "c5a.4xlarge"
  environment    = var.environment
  subnets        = var.subnets
  security_group = var.security_group
  max            = local.max_asg_instances
}

# Requirement from Twitch Security (https://wiki.xarth.tv/display/SEC/AWS+Systems+Manager+Campaign)
resource "aws_iam_role_policy_attachment" "ecs_ssm_policy_attachment" {
  role       = module.cluster.ecs_instance_role_name
  policy_arn = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
}
