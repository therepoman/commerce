locals {
  images = {
    production = "671452935478.dkr.ecr.us-west-2.amazonaws.com/mako"
    staging    = "077362005997.dkr.ecr.us-west-2.amazonaws.com/mako"
  }

  counts = {
    # Try to keep these task counts at around (1.2 * instance count). There is a bit of wiggle 
    # room with the values, so try tuning a bit. Keep space on the instances for:
    #  - Canary tasks
    #  - Ad-hoc task runs like integration tests
    #  - Additional tasks that get batch spun-up during ECS deployments
    production = 120

    staging = 3
  }

  memory = {
    production = 4096
    staging    = 4096
  }

  cpu = {
    production = 5120
    staging    = 5120
  }

  name = coalesce(var.name, "mako")
}

module "mako" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//twitch-service?ref=e80ce001454204ae42037e5aef41040ad766f2e0"

  name                     = local.name
  image                    = local.images[var.environment]
  repo                     = "mako"
  tag                      = "latest"
  port                     = 8000
  healthcheck              = "/"
  dns                      = var.dns
  healthcheck_grace_period = "120"
  debug_port               = 6000
  debug_enabled            = false

  dns_servers = ["172.17.0.1"]

  counts = {
    min = local.counts[var.environment]
    max = 500
  }

  env_vars = [
    {
      name  = "ENVIRONMENT"
      value = var.environment
    },
    {
      name  = "ECS"
      value = "true"
    },
    {
      name  = "AWS_STS_REGIONAL_ENDPOINTS"
      value = "regional"
    },
  ]

  memory         = local.memory[var.environment]
  cpu            = local.cpu[var.environment]
  canary_cpu     = local.cpu[var.environment]
  canary_memory  = local.memory[var.environment]
  cluster        = "sonar"
  security_group = var.security_group
  region         = var.region
  vpc_id         = var.vpc_id
  environment    = var.environment
  subnets        = var.subnets

  deployment = {
    minimum_percent = 100
    maximum_percent = 150
  }

  access_logs_expiration_days = 14
  log_retention_days          = 90

  fargate_platform_version = null
}

module "dynamodb" {
  source             = "./dynamodb"
  environment        = var.environment
  service_role       = module.mako.service_role
  canary_role        = module.mako.canary_role
  pager_duty_sns_arn = var.pager_duty_sns_arn
}

module "cloudwatch" {
  source             = "./cloudwatch"
  pager_duty_sns_arn = var.pager_duty_sns_arn
  environment        = var.environment
}

module "s3" {
  source = "./s3"

  aws_profile           = var.aws_profile
  capacity_planning_arn = var.capacity_planning_arn
  environment           = var.environment
  upload_service_arn    = var.upload_service_arn
}
