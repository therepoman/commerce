module "route53-zone" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//privatelink-zone?ref=e80ce001454204ae42037e5aef41040ad766f2e0"

  name        = "mako"
  environment = var.environment
}
