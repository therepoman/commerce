resource "aws_s3_bucket" "experiment_overrides" {
  bucket = "mako-${var.environment}-experiment-overrides"
}

data "aws_s3_bucket" "s3_emoticon_uploads" {
  bucket = "${var.environment == "production" ? "prod" : var.environment}-emoticon-uploads"
}

resource "aws_s3_bucket" "s3_animated_emote_frames" {
  bucket = "${var.environment == "production" ? "prod" : var.environment}-animated-emote-frames"
}

resource "aws_s3_bucket" "s3_default_emote_library_images" {
  bucket = "${var.environment == "production" ? "prod" : var.environment}-default-emote-library-images"
}

data "aws_iam_policy_document" "s3_animated_emote_frames_access" {
  statement {
    effect = "Allow"

    principals {
      type        = "AWS"
      identifiers = ["*"]
    }

    actions   = ["s3:GetObject"]
    resources = ["${aws_s3_bucket.s3_animated_emote_frames.arn}/*"]
  }

  statement {
    effect = "Allow"

    principals {
      type        = "AWS"
      identifiers = [module.lambda-animated-emote-s3-role.arn]
    }

    actions   = ["s3:PutObject", "s3:PutObjectAcl"]
    resources = ["${aws_s3_bucket.s3_animated_emote_frames.arn}/*"]
  }
}

resource "aws_s3_bucket_policy" "animated_emote_frame_access" {
  bucket = aws_s3_bucket.s3_animated_emote_frames.id
  policy = data.aws_iam_policy_document.s3_animated_emote_frames_access.json
}

data "aws_iam_policy_document" "s3_emote_bucket_access_policy" {
  statement {
    effect = "Allow"

    actions   = ["s3:GetObject"]
    resources = ["${data.aws_s3_bucket.s3_emoticon_uploads.arn}/*"]
  }
}

resource "aws_iam_policy" "s3_emote_read_access" {
  name   = "s3_emoticon_uploads_read_access"
  policy = data.aws_iam_policy_document.s3_emote_bucket_access_policy.json
}

data "aws_iam_policy_document" "s3_default_emote_library_images_bucket_access_policy" {
  statement {
    effect = "Allow"

    principals {
      type        = "AWS"
      identifiers = ["*"]
    }

    actions   = ["s3:GetObject"]
    resources = ["${aws_s3_bucket.s3_default_emote_library_images.arn}/*"]
  }
}

resource "aws_s3_bucket_policy" "s3_default_emote_library_images_read_access" {
  bucket = aws_s3_bucket.s3_default_emote_library_images.id
  policy = data.aws_iam_policy_document.s3_default_emote_library_images_bucket_access_policy.json
}
