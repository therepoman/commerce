locals {
  dns_servers = {
    production = [
      "10.197.231.47",
      "10.197.218.250",
      "10.197.223.101",
      "10.197.232.103",
    ]

    staging = [
      "10.202.125.133",
      "10.202.124.213",
      "10.202.126.110",
      "10.202.124.10",
    ]
  }
}

module "dns" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//dns?ref=e80ce001454204ae42037e5aef41040ad766f2e0"

  cluster     = "sonar"
  environment = var.environment
  dns_servers = local.dns_servers[var.environment]
}
