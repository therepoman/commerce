locals {
  environment_prefix = {
    production = "prod"
    staging    = "staging"
  }

  metric_name         = "EmoticonAggregator.FollowerEmote"
  namespace           = "mako"

  dimensions = {
    Stage   = "${local.environment_prefix[var.environment]}-ecs"
    Region  = "us-west-2"
    Service = "mako"
  }

  period                    = "60"
  statistic                 = "Sum"
}

// The EmoticonAggregator is setup so that if retrieving follower emotes fails, we still
// return Materia and EntitlementsDB emote sets. Since we can't rely on the API alarm, we
// have this check to alarm on follower emote errors.
resource "aws_cloudwatch_metric_alarm" "emoticon-aggregator-follower-emote-error-rate" {
  alarm_name          = "emoticon-aggregator-follower-emote-error-rate"
  alarm_description   = "Alarms when the EmoticonAggregator fails to load follower emotes."
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "5"
  threshold           = "5"
  datapoints_to_alarm = "3"

  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"
  actions_enabled           = length(var.pager_duty_sns_arn) > 0 ? true : false
  alarm_actions             = [var.pager_duty_sns_arn]
  ok_actions                = [var.pager_duty_sns_arn]

  metric_query {
    id          = "e1"
    expression  = "m2/m1*100"
    label       = "EmoticonAggregator.FollowerEmote error rate (%)"
    return_data = true
  }

  metric_query {
    id = "m1"

    metric {
      metric_name = "EmoticonAggregator.FollowerEmoteError"
      namespace   = "mako"
      period      = "60"
      stat        = "SampleCount"

      dimensions = {
        Service = "mako"
        Stage   = "${local.environment_prefix[var.environment]}-ecs"
        Region  = "us-west-2"
      }
    }
  }

  metric_query {
    id = "m2"

    metric {
      metric_name = "EmoticonAggregator.FollowerEmoteError"
      namespace   = "mako"
      period      = "60"
      stat        = "Sum"

      dimensions = {
        Service = "mako"
        Stage   = "${local.environment_prefix[var.environment]}-ecs"
        Region  = "us-west-2"
      }
    }
  }
}

resource "aws_cloudwatch_metric_alarm" "dynamic-config-startup-failure" {
  alarm_name          = "dynamic-config-startup-failure"
  alarm_description   = "Alarms when we've had failures initializing Mako's dynamic config."
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = "3"
  datapoints_to_alarm = "3"
  metric_name         = "dynconfig.startup.failure"
  namespace           = "mako"

  dimensions = {
    Stage   = "${local.environment_prefix[var.environment]}-ecs"
    Region  = "us-west-2"
    Service = "mako"
  }

  period                    = "300"
  statistic                 = "Sum"
  threshold                 = "1"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"
  actions_enabled           = length(var.pager_duty_sns_arn) > 0 ? true : false
  alarm_actions             = [var.pager_duty_sns_arn]
  ok_actions                = [var.pager_duty_sns_arn]
}

resource "aws_cloudwatch_metric_alarm" "dynamic-config-reload-failure" {
  alarm_name          = "dynamic-config-reload-failure"
  alarm_description   = "Alarms when we've had failures reloading Mako's dynamic config."
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = "15"
  datapoints_to_alarm = "15"
  metric_name         = "dynconfig.refresh.failure"
  namespace           = "mako"

  dimensions = {
    Stage   = "${local.environment_prefix[var.environment]}-ecs"
    Region  = "us-west-2"
    Service = "mako"
  }

  period                    = "60"
  statistic                 = "Sum"
  threshold                 = "0"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"
  actions_enabled           = length(var.pager_duty_sns_arn) > 0 ? true : false
  alarm_actions             = [var.pager_duty_sns_arn]
  ok_actions                = [var.pager_duty_sns_arn]
}

resource "aws_cloudwatch_metric_alarm" "prefix-updates-dlq-low-sev" {
  alarm_name          = "prefix-updates-dlq-low-sev"
  alarm_description   = "[LowSev] Alarms when the prefix-updates-dlq SQS queue has messages in it."
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = "1"
  datapoints_to_alarm = "1"
  metric_name         = "ApproximateNumberOfMessagesVisible"
  namespace           = "AWS/SQS"

  dimensions = {
    QueueName = "${local.environment_prefix[var.environment]}-prefix-updates-dlq"
  }

  period                    = "60"
  statistic                 = "Sum"
  threshold                 = "0"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"
  actions_enabled           = length(var.pager_duty_sns_arn) > 0 ? true : false
  alarm_actions             = [var.pager_duty_sns_arn]
  ok_actions                = [var.pager_duty_sns_arn]
}

resource "aws_cloudwatch_metric_alarm" "prefix-updates-dlq-high-sev" {
  alarm_name          = "prefix-updates-dlq-high-sev"
  alarm_description   = "Alarms when the prefix-updates-dlq SQS queue has messages in it."
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = "1"
  datapoints_to_alarm = "1"
  metric_name         = "ApproximateNumberOfMessagesVisible"
  namespace           = "AWS/SQS"

  dimensions = {
    QueueName = "${local.environment_prefix[var.environment]}-prefix-updates-dlq"
  }

  period                    = "60"
  statistic                 = "Sum"
  threshold                 = "10"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"
  actions_enabled           = length(var.pager_duty_sns_arn) > 0 ? true : false
  alarm_actions             = [var.pager_duty_sns_arn]
  ok_actions                = [var.pager_duty_sns_arn]
}

resource "aws_cloudwatch_metric_alarm" "mako_logscan_fatals" {
  alarm_name          = "mako-logscan-fatal-alert"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "mako-fatals"
  namespace           = "mako-logs"

  period                    = "300"
  statistic                 = "Sum"
  threshold                 = "1"
  alarm_description         = "This alarm is used when Mako has a fatal error"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled = length(var.pager_duty_sns_arn) > 0 ? true : false
  alarm_actions   = [var.pager_duty_sns_arn]
  ok_actions      = [var.pager_duty_sns_arn]
}
