resource "aws_cloudwatch_metric_alarm" "all-emotes-export-lambda-failure" {
  alarm_name          = "all-emotes-export-lambda-failure"
  alarm_description   = "[LowSev] Alarms when the all emotes export lambda fails."
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = "1"
  datapoints_to_alarm = "1"
  metric_name         = "Errors"
  namespace           = "AWS/Lambda"
  dimensions = {
    FunctionName = "mako-all_emotes_export"
  }

  period                    = "43200" // 12 hours
  statistic                 = "Sum"
  threshold                 = "0"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"
  actions_enabled           = length(var.pager_duty_sns_arn) > 0 ? true : false
  alarm_actions             = [var.pager_duty_sns_arn]
  ok_actions                = [var.pager_duty_sns_arn]
}

resource "aws_cloudwatch_metric_alarm" "lambda-no-invocations" {
  alarm_name          = "all_emotes_export-lambda-no-invocations"
  alarm_description   = "[LowSev] Alarms when the all_emotes_export lambda has no invocations."
  comparison_operator = "LessThanThreshold"
  evaluation_periods  = "1"
  datapoints_to_alarm = "1"
  metric_name         = "Invocations"
  namespace           = "AWS/Lambda"
  dimensions = {
    FunctionName = "mako-all_emotes_export"
  }

  period                    = "86400" //24 hours
  statistic                 = "Sum"
  threshold                 = "1"
  insufficient_data_actions = []
  treat_missing_data        = "breaching"
  actions_enabled           = length(var.pager_duty_sns_arn) > 0 ? true : false
  alarm_actions             = [var.pager_duty_sns_arn]
  ok_actions                = [var.pager_duty_sns_arn]
}
