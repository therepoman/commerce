variable "pager_duty_sns_arn" {
  default     = ""
  description = "ARN of the SNS topic used for Pager Duty alarms"
  type        = string
}

variable "table" {
  type = string
}

variable "index" {
  type    = string
  default = ""
}

variable "utilization_eval_periods" {
  default = 2
}

variable "utilization_datapoints" {
  default = 2
}

variable "throttle_eval_periods" {
  default = 2
}

variable "throttle_datapoints" {
  default = 2
}

// ProvisionedReadCapacityUnits and ProvisionedWriteCapacityUnits are only aggregated every 5 mins.
// https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/metrics-dimensions.html
variable "utilization_period" {
  default = 300
}

variable "throttle_period" {
  default = 60
}

variable "utilization_threshold" {
  default     = 80
  description = "The % of provisioned capacity that is being consumed that will trigger this alarm"
}

variable "utilization_stat" {
  default     = "Sum"
  description = "The stat to use for utilization alarms. Use Sum and then divide by period in order to compare to provisioned capacity."
}

variable "throttle_events_threshold" {
  default     = 1
  description = "The number of throttle events that will trigger this alarm"
}
