// Read Utilization
resource "aws_cloudwatch_metric_alarm" "index-read-utilization" {
  // only use this definition for GSIs
  count                     = var.index != "" ? 1 : 0
  alarm_name                = "${var.index}-read-utilization"
  alarm_description         = "[LowSev] Alarms when near maximum read capacity"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = var.utilization_eval_periods
  datapoints_to_alarm       = var.utilization_datapoints
  threshold                 = var.utilization_threshold
  insufficient_data_actions = []
  treat_missing_data        = "missing"
  actions_enabled           = length(var.pager_duty_sns_arn) > 0 ? true : false
  alarm_actions             = [var.pager_duty_sns_arn]
  ok_actions                = [var.pager_duty_sns_arn]

  metric_query {
    id          = "e1"
    expression  = "100*(m1/${var.utilization_period}/m2)"
    label       = "${var.index} Read Utilization %"
    return_data = true
  }

  metric_query {
    id = "m1"

    metric {
      metric_name = "ConsumedReadCapacityUnits"
      namespace   = "AWS/DynamoDB"
      period      = var.utilization_period
      stat        = var.utilization_stat

      dimensions = {
        TableName                = var.table
        GlobalSecondaryIndexName = var.index
      }
    }
  }

  metric_query {
    id = "m2"

    metric {
      metric_name = "ProvisionedReadCapacityUnits"
      namespace   = "AWS/DynamoDB"
      period      = var.utilization_period
      stat        = var.utilization_stat

      dimensions = {
        TableName                = var.table
        GlobalSecondaryIndexName = var.index
      }
    }
  }
}

// Write Utilization
resource "aws_cloudwatch_metric_alarm" "index-write-utilization" {
  // only use this definition for GSIs
  count                     = var.index != "" ? 1 : 0
  alarm_name                = "${var.index}-write-utilization"
  alarm_description         = "[LowSev] Alarms when near maximum write capacity"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = var.utilization_eval_periods
  datapoints_to_alarm       = var.utilization_datapoints
  threshold                 = var.utilization_threshold
  insufficient_data_actions = []
  treat_missing_data        = "missing"
  actions_enabled           = length(var.pager_duty_sns_arn) > 0 ? true : false
  alarm_actions             = [var.pager_duty_sns_arn]
  ok_actions                = [var.pager_duty_sns_arn]

  metric_query {
    id          = "e1"
    expression  = "100*(m1/${var.utilization_period}/m2)"
    label       = "${var.index} Write Utilization %"
    return_data = true
  }

  metric_query {
    id = "m1"

    metric {
      metric_name = "ConsumedWriteCapacityUnits"
      namespace   = "AWS/DynamoDB"
      period      = var.utilization_period
      stat        = var.utilization_stat

      dimensions = {
        TableName                = var.table
        GlobalSecondaryIndexName = var.index
      }
    }
  }

  metric_query {
    id = "m2"

    metric {
      metric_name = "ProvisionedWriteCapacityUnits"
      namespace   = "AWS/DynamoDB"
      period      = var.utilization_period
      stat        = var.utilization_stat

      dimensions = {
        TableName                = var.table
        GlobalSecondaryIndexName = var.index
      }
    }
  }
}

// Read throttle events
resource "aws_cloudwatch_metric_alarm" "index-read-throttle-events" {
  // only use this definition for GSIs
  count                     = var.index != "" ? 1 : 0
  alarm_name                = "${var.index}-read-throttle-events"
  alarm_description         = "Alarms when reads are being throttled"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = var.throttle_eval_periods
  datapoints_to_alarm       = var.throttle_datapoints
  threshold                 = var.throttle_events_threshold
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"
  actions_enabled           = length(var.pager_duty_sns_arn) > 0 ? true : false
  alarm_actions             = [var.pager_duty_sns_arn]
  ok_actions                = [var.pager_duty_sns_arn]

  // simple alarm:
  namespace   = "AWS/DynamoDB"
  metric_name = "ReadThrottleEvents"
  period      = var.throttle_period
  statistic   = "Sum"

  dimensions = {
    TableName                = var.table
    GlobalSecondaryIndexName = var.index
  }
}

// Write throttle events
resource "aws_cloudwatch_metric_alarm" "index-write-throttle-events" {
  // only use this definition for GSIs
  count                     = var.index != "" ? 1 : 0
  alarm_name                = "${var.index}-write-throttle-events"
  alarm_description         = "Alarms when writes are being throttled"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = var.throttle_eval_periods
  datapoints_to_alarm       = var.throttle_datapoints
  threshold                 = var.throttle_events_threshold
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"
  actions_enabled           = length(var.pager_duty_sns_arn) > 0 ? true : false
  alarm_actions             = [var.pager_duty_sns_arn]
  ok_actions                = [var.pager_duty_sns_arn]

  // simple alarm:
  namespace   = "AWS/DynamoDB"
  metric_name = "WriteThrottleEvents"
  period      = var.throttle_period
  statistic   = "Sum"

  dimensions = {
    TableName                = var.table
    GlobalSecondaryIndexName = var.index
  }
}
