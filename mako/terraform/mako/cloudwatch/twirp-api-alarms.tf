// GetEmoteSetDetails
module "GetEmoteSetDetails-alarms" {
  source               = "./twirp-api"
  api_name             = "GetEmoteSetDetails"
  alarm_prefix         = var.environment
  pager_duty_sns_arn   = var.pager_duty_sns_arn
  stage                = "${local.environment_prefix[var.environment]}-ecs"
  evaluation_periods   = 3
  datapoints_to_alarm  = 3
  period               = 300
  latency_metric       = "p99"
  latency_threshold_ms = 200
}

module "GetEmoteSetDetails-canary-alarms" {
  source               = "./twirp-api"
  api_name             = "GetEmoteSetDetails"
  alarm_prefix         = "${var.environment}-canary"
  pager_duty_sns_arn   = var.pager_duty_sns_arn
  stage                = "${local.environment_prefix[var.environment]}-canary-ecs"
  evaluation_periods   = 3
  datapoints_to_alarm  = 3
  period               = 300
  latency_metric       = "p99"
  latency_threshold_ms = 200
  low_sev              = true
}

// GetEmotesByCodes
module "GetEmotesByCodes-alarms" {
  source               = "./twirp-api"
  api_name             = "GetEmotesByCodes"
  alarm_prefix         = var.environment
  pager_duty_sns_arn   = var.pager_duty_sns_arn
  stage                = "${local.environment_prefix[var.environment]}-ecs"
  evaluation_periods   = 3
  datapoints_to_alarm  = 3
  period               = 300
  latency_metric       = "p99"
  latency_threshold_ms = 200
}

module "GetEmotesByCodes-canary-alarms" {
  source               = "./twirp-api"
  api_name             = "GetEmotesByCodes"
  alarm_prefix         = "${var.environment}-canary"
  pager_duty_sns_arn   = var.pager_duty_sns_arn
  stage                = "${local.environment_prefix[var.environment]}-canary-ecs"
  evaluation_periods   = 3
  datapoints_to_alarm  = 3
  period               = 300
  latency_metric       = "p99"
  latency_threshold_ms = 200
  low_sev              = true
}

// SetSmilies
module "SetSmilies-alarms" {
  source               = "./twirp-api"
  api_name             = "SetSmilies"
  low_sev              = true
  alarm_prefix         = var.environment
  pager_duty_sns_arn   = var.pager_duty_sns_arn
  stage                = "${local.environment_prefix[var.environment]}-ecs"
  evaluation_periods   = 3
  datapoints_to_alarm  = 3
  period               = 300
  latency_metric       = "p90"
  latency_threshold_ms = 300
}

module "SetSmilies-canary-alarms" {
  source               = "./twirp-api"
  api_name             = "SetSmilies"
  low_sev              = true
  alarm_prefix         = "${var.environment}-canary"
  pager_duty_sns_arn   = var.pager_duty_sns_arn
  stage                = "${local.environment_prefix[var.environment]}-canary-ecs"
  evaluation_periods   = 3
  datapoints_to_alarm  = 3
  period               = 300
  latency_metric       = "p90"
  latency_threshold_ms = 300
}

// GetLegacyEmoticonAllURLs
module "GetLegacyEmoticonAllURLs-alarms" {
  source               = "./twirp-api"
  api_name             = "GetLegacyEmoticonAllURLs"
  low_sev              = true
  alarm_prefix         = var.environment
  pager_duty_sns_arn   = var.pager_duty_sns_arn
  stage                = "${local.environment_prefix[var.environment]}-ecs"
  evaluation_periods   = 3
  datapoints_to_alarm  = 3
  period               = 300
  latency_metric       = "p90"
  latency_threshold_ms = 200
}

module "GetLegacyEmoticonAllURLs-canary-alarms" {
  source               = "./twirp-api"
  api_name             = "GetLegacyEmoticonAllURLs"
  low_sev              = true
  alarm_prefix         = "${var.environment}-canary"
  pager_duty_sns_arn   = var.pager_duty_sns_arn
  stage                = "${local.environment_prefix[var.environment]}-canary-ecs"
  evaluation_periods   = 3
  datapoints_to_alarm  = 3
  period               = 300
  latency_metric       = "p90"
  latency_threshold_ms = 200
}

// GetAllSmilies
module "GetAllSmilies-alarms" {
  source               = "./twirp-api"
  api_name             = "GetAllSmilies"
  low_sev              = true
  alarm_prefix         = var.environment
  pager_duty_sns_arn   = var.pager_duty_sns_arn
  stage                = "${local.environment_prefix[var.environment]}-ecs"
  evaluation_periods   = 3
  datapoints_to_alarm  = 3
  period               = 300
  latency_metric       = "p90"
  latency_threshold_ms = 400
}

module "GetAllSmilies-canary-alarms" {
  source               = "./twirp-api"
  api_name             = "GetAllSmilies"
  low_sev              = true
  alarm_prefix         = "${var.environment}-canary"
  pager_duty_sns_arn   = var.pager_duty_sns_arn
  stage                = "${local.environment_prefix[var.environment]}-canary-ecs"
  evaluation_periods   = 3
  datapoints_to_alarm  = 3
  period               = 300
  latency_metric       = "p90"
  latency_threshold_ms = 400
}

// GetSelectedSmilies
module "GetSelectedSmilies-alarms" {
  source               = "./twirp-api"
  api_name             = "GetSelectedSmilies"
  low_sev              = true
  alarm_prefix         = var.environment
  pager_duty_sns_arn   = var.pager_duty_sns_arn
  stage                = "${local.environment_prefix[var.environment]}-ecs"
  evaluation_periods   = 3
  datapoints_to_alarm  = 3
  period               = 300
  latency_metric       = "p90"
  latency_threshold_ms = 200
}

module "GetSelectedSmilies-canary-alarms" {
  source               = "./twirp-api"
  api_name             = "GetSelectedSmilies"
  low_sev              = true
  alarm_prefix         = "${var.environment}-canary"
  pager_duty_sns_arn   = var.pager_duty_sns_arn
  stage                = "${local.environment_prefix[var.environment]}-canary-ecs"
  evaluation_periods   = 3
  datapoints_to_alarm  = 3
  period               = 300
  latency_metric       = "p90"
  latency_threshold_ms = 200
}

// CreateEmoticon
module "CreateEmoticon-alarms" {
  source               = "./twirp-api"
  api_name             = "CreateEmoticon"
  alarm_prefix         = var.environment
  pager_duty_sns_arn   = var.pager_duty_sns_arn
  stage                = "${local.environment_prefix[var.environment]}-ecs"
  evaluation_periods   = 6
  datapoints_to_alarm  = 6
  period               = 300
  latency_metric       = "p90"
  latency_threshold_ms = 4500
}

module "CreateEmoticon-canary-alarms" {
  source               = "./twirp-api"
  api_name             = "CreateEmoticon"
  alarm_prefix         = "${var.environment}-canary"
  pager_duty_sns_arn   = var.pager_duty_sns_arn
  stage                = "${local.environment_prefix[var.environment]}-canary-ecs"
  evaluation_periods   = 12
  datapoints_to_alarm  = 12
  period               = 300
  latency_metric       = "p90"
  latency_threshold_ms = 5000
  low_sev              = true
}

// BatchValidateEmoteCodeAcceptability
module "BatchValidateEmoteCodeAcceptability-alarms" {
  source               = "./twirp-api"
  api_name             = "BatchValidateEmoteCodeAcceptability"
  alarm_prefix         = var.environment
  pager_duty_sns_arn   = var.pager_duty_sns_arn
  stage                = "${local.environment_prefix[var.environment]}-ecs"
  evaluation_periods   = 3
  datapoints_to_alarm  = 3
  period               = 300
  latency_metric       = "p90"
  latency_threshold_ms = 200
}

module "BatchValidateEmoteCodeAcceptability-canary-alarms" {
  source               = "./twirp-api"
  api_name             = "BatchValidateEmoteCodeAcceptability"
  alarm_prefix         = "${var.environment}-canary"
  pager_duty_sns_arn   = var.pager_duty_sns_arn
  stage                = "${local.environment_prefix[var.environment]}-canary-ecs"
  evaluation_periods   = 3
  datapoints_to_alarm  = 3
  period               = 300
  latency_metric       = "p90"
  latency_threshold_ms = 200
  low_sev              = true
}

// GetEmoteUploadConfig
module "GetEmoteUploadConfig-alarms" {
  source               = "./twirp-api"
  api_name             = "GetEmoteUploadConfig"
  alarm_prefix         = var.environment
  pager_duty_sns_arn   = var.pager_duty_sns_arn
  stage                = "${local.environment_prefix[var.environment]}-ecs"
  evaluation_periods   = 3
  datapoints_to_alarm  = 3
  period               = 300
  latency_metric       = "p90"
  latency_threshold_ms = 400
}

module "GetEmoteUploadConfig-canary-alarms" {
  source               = "./twirp-api"
  api_name             = "GetEmoteUploadConfig"
  alarm_prefix         = "${var.environment}-canary"
  pager_duty_sns_arn   = var.pager_duty_sns_arn
  stage                = "${local.environment_prefix[var.environment]}-canary-ecs"
  evaluation_periods   = 3
  datapoints_to_alarm  = 3
  period               = 300
  latency_metric       = "p90"
  latency_threshold_ms = 400
  low_sev              = true
}

// GetEmoticonUploadConfiguration
module "GetEmoticonUploadConfiguration-alarms" {
  source               = "./twirp-api"
  api_name             = "GetEmoticonUploadConfiguration"
  alarm_prefix         = var.environment
  pager_duty_sns_arn   = var.pager_duty_sns_arn
  stage                = "${local.environment_prefix[var.environment]}-ecs"
  evaluation_periods   = 6
  datapoints_to_alarm  = 6
  period               = 300
  latency_metric       = "p90"
  latency_threshold_ms = 400
}

module "GetEmoticonUploadConfiguration-canary-alarms" {
  source               = "./twirp-api"
  api_name             = "GetEmoticonUploadConfiguration"
  alarm_prefix         = "${var.environment}-canary"
  pager_duty_sns_arn   = var.pager_duty_sns_arn
  stage                = "${local.environment_prefix[var.environment]}-canary-ecs"
  evaluation_periods   = 6
  datapoints_to_alarm  = 6
  period               = 300
  latency_metric       = "p90"
  latency_threshold_ms = 400
  low_sev              = true
}

// GetLegacyChannelEmoticons
module "GetLegacyChannelEmoticons-alarms" {
  source               = "./twirp-api"
  api_name             = "GetLegacyChannelEmoticons"
  low_sev              = true
  alarm_prefix         = var.environment
  pager_duty_sns_arn   = var.pager_duty_sns_arn
  stage                = "${local.environment_prefix[var.environment]}-ecs"
  evaluation_periods   = 3
  datapoints_to_alarm  = 3
  period               = 300
  latency_metric       = "p90"
  latency_threshold_ms = 300
}

module "GetLegacyChannelEmoticons-canary-alarms" {
  source               = "./twirp-api"
  api_name             = "GetLegacyChannelEmoticons"
  low_sev              = true
  alarm_prefix         = "${var.environment}-canary"
  pager_duty_sns_arn   = var.pager_duty_sns_arn
  stage                = "${local.environment_prefix[var.environment]}-canary-ecs"
  evaluation_periods   = 3
  datapoints_to_alarm  = 3
  period               = 300
  latency_metric       = "p90"
  latency_threshold_ms = 300
}

// SetEntitlement
module "SetEntitlement-alarms" {
  source               = "./twirp-api"
  api_name             = "SetEntitlement"
  alarm_prefix         = var.environment
  pager_duty_sns_arn   = var.pager_duty_sns_arn
  stage                = "${local.environment_prefix[var.environment]}-ecs"
  evaluation_periods   = 3
  datapoints_to_alarm  = 3
  period               = 300
  latency_metric       = "p90"
  latency_threshold_ms = 100
}

module "SetEntitlement-canary-alarms" {
  source               = "./twirp-api"
  api_name             = "SetEntitlement"
  alarm_prefix         = "${var.environment}-canary"
  pager_duty_sns_arn   = var.pager_duty_sns_arn
  stage                = "${local.environment_prefix[var.environment]}-canary-ecs"
  evaluation_periods   = 3
  datapoints_to_alarm  = 3
  period               = 300
  latency_metric       = "p90"
  latency_threshold_ms = 100
  low_sev              = true
}

// GetUserEmoteStanding
module "GetUserEmoteStanding-alarms" {
  source               = "./twirp-api"
  api_name             = "GetUserEmoteStanding"
  alarm_prefix         = var.environment
  pager_duty_sns_arn   = var.pager_duty_sns_arn
  stage                = "${local.environment_prefix[var.environment]}-ecs"
  evaluation_periods   = 3
  datapoints_to_alarm  = 3
  period               = 300
  latency_metric       = "p90"
  latency_threshold_ms = 700
}

module "GetUserEmoteStanding-canary-alarms" {
  source               = "./twirp-api"
  api_name             = "GetUserEmoteStanding"
  alarm_prefix         = "${var.environment}-canary"
  pager_duty_sns_arn   = var.pager_duty_sns_arn
  stage                = "${local.environment_prefix[var.environment]}-canary-ecs"
  evaluation_periods   = 3
  datapoints_to_alarm  = 3
  period               = 300
  latency_metric       = "p90"
  latency_threshold_ms = 700
  low_sev              = true
}

// CreateEmoteGroup
module "CreateEmoteGroup-alarms" {
  source               = "./twirp-api"
  api_name             = "CreateEmoteGroup"
  alarm_prefix         = var.environment
  pager_duty_sns_arn   = var.pager_duty_sns_arn
  stage                = "${local.environment_prefix[var.environment]}-ecs"
  evaluation_periods   = 3
  datapoints_to_alarm  = 3
  period               = 300
  latency_metric       = "p90"
  latency_threshold_ms = 100
}

module "CreateEmoteGroup-canary-alarms" {
  source               = "./twirp-api"
  api_name             = "CreateEmoteGroup"
  alarm_prefix         = "${var.environment}-canary"
  pager_duty_sns_arn   = var.pager_duty_sns_arn
  stage                = "${local.environment_prefix[var.environment]}-canary-ecs"
  evaluation_periods   = 3
  datapoints_to_alarm  = 3
  period               = 300
  latency_metric       = "p90"
  latency_threshold_ms = 100
  low_sev              = true
}

// GetEmoticonsByGroups
module "GetEmoticonsByGroups-alarms" {
  source               = "./twirp-api"
  api_name             = "GetEmoticonsByGroups"
  alarm_prefix         = var.environment
  pager_duty_sns_arn   = var.pager_duty_sns_arn
  stage                = "${local.environment_prefix[var.environment]}-ecs"
  evaluation_periods   = 3
  datapoints_to_alarm  = 3
  period               = 300
  latency_metric       = "p90"
  latency_threshold_ms = 100
}

module "GetEmoticonsByGroups-canary-alarms" {
  source               = "./twirp-api"
  api_name             = "GetEmoticonsByGroups"
  alarm_prefix         = "${var.environment}-canary"
  pager_duty_sns_arn   = var.pager_duty_sns_arn
  stage                = "${local.environment_prefix[var.environment]}-canary-ecs"
  evaluation_periods   = 3
  datapoints_to_alarm  = 3
  period               = 300
  latency_metric       = "p90"
  latency_threshold_ms = 100
  low_sev              = true
}

// GetEmotesByGroups
module "GetEmotesByGroups-alarms" {
  source               = "./twirp-api"
  api_name             = "GetEmotesByGroups"
  alarm_prefix         = var.environment
  pager_duty_sns_arn   = var.pager_duty_sns_arn
  stage                = "${local.environment_prefix[var.environment]}-ecs"
  evaluation_periods   = 3
  datapoints_to_alarm  = 3
  period               = 300
  latency_metric       = "p90"
  latency_threshold_ms = 200
}

module "GetEmotesByGroups-canary-alarms" {
  source               = "./twirp-api"
  api_name             = "GetEmotesByGroups"
  alarm_prefix         = "${var.environment}-canary"
  pager_duty_sns_arn   = var.pager_duty_sns_arn
  stage                = "${local.environment_prefix[var.environment]}-canary-ecs"
  evaluation_periods   = 3
  datapoints_to_alarm  = 3
  period               = 300
  latency_metric       = "p90"
  latency_threshold_ms = 200
  low_sev              = true
}

// GetEmoticonsByEmoticonIds
module "GetEmoticonsByEmoticonIds-alarms" {
  source               = "./twirp-api"
  api_name             = "GetEmoticonsByEmoticonIds"
  alarm_prefix         = var.environment
  pager_duty_sns_arn   = var.pager_duty_sns_arn
  stage                = "${local.environment_prefix[var.environment]}-ecs"
  evaluation_periods   = 3
  datapoints_to_alarm  = 3
  period               = 300
  latency_metric       = "p90"
  latency_threshold_ms = 200
}

module "GetEmoticonsByEmoticonIds-canary-alarms" {
  source               = "./twirp-api"
  api_name             = "GetEmoticonsByEmoticonIds"
  alarm_prefix         = "${var.environment}-canary"
  pager_duty_sns_arn   = var.pager_duty_sns_arn
  stage                = "${local.environment_prefix[var.environment]}-canary-ecs"
  evaluation_periods   = 3
  datapoints_to_alarm  = 3
  period               = 300
  latency_metric       = "p90"
  latency_threshold_ms = 200
  low_sev              = true
}

// CreateEmoteGroupEntitlements
module "CreateEmoteGroupEntitlements-alarms" {
  source               = "./twirp-api"
  api_name             = "CreateEmoteGroupEntitlements"
  alarm_prefix         = var.environment
  pager_duty_sns_arn   = var.pager_duty_sns_arn
  stage                = "${local.environment_prefix[var.environment]}-ecs"
  evaluation_periods   = 3
  datapoints_to_alarm  = 3
  period               = 300
  latency_metric       = "p90"
  latency_threshold_ms = 200
}

module "CreateEmoteGroupEntitlements-canary-alarms" {
  source               = "./twirp-api"
  api_name             = "CreateEmoteGroupEntitlements"
  alarm_prefix         = "${var.environment}-canary"
  pager_duty_sns_arn   = var.pager_duty_sns_arn
  stage                = "${local.environment_prefix[var.environment]}-canary-ecs"
  evaluation_periods   = 3
  datapoints_to_alarm  = 3
  period               = 300
  latency_metric       = "p90"
  latency_threshold_ms = 200
  low_sev              = true
}

// CreateEmoteEntitlements
module "CreateEmoteEntitlements-alarms" {
  source               = "./twirp-api"
  api_name             = "CreateEmoteEntitlements"
  alarm_prefix         = var.environment
  pager_duty_sns_arn   = var.pager_duty_sns_arn
  stage                = "${local.environment_prefix[var.environment]}-ecs"
  evaluation_periods   = 3
  datapoints_to_alarm  = 3
  period               = 300
  latency_metric       = "p90"
  latency_threshold_ms = 1000
}

module "CreateEmoteEntitlements-canary-alarms" {
  source               = "./twirp-api"
  api_name             = "CreateEmoteEntitlements"
  alarm_prefix         = "${var.environment}-canary"
  pager_duty_sns_arn   = var.pager_duty_sns_arn
  stage                = "${local.environment_prefix[var.environment]}-canary-ecs"
  evaluation_periods   = 3
  datapoints_to_alarm  = 3
  period               = 300
  latency_metric       = "p90"
  latency_threshold_ms = 1000
  low_sev              = true
}

// BatchValidateCodeUniqueness
module "BatchValidateCodeUniqueness-alarms" {
  source               = "./twirp-api"
  api_name             = "BatchValidateCodeUniqueness"
  alarm_prefix         = var.environment
  pager_duty_sns_arn   = var.pager_duty_sns_arn
  stage                = "${local.environment_prefix[var.environment]}-ecs"
  evaluation_periods   = 3
  datapoints_to_alarm  = 3
  period               = 300
  latency_metric       = "p90"
  latency_threshold_ms = 100
}

module "BatchValidateCodeUniqueness-canary-alarms" {
  source               = "./twirp-api"
  api_name             = "BatchValidateCodeUniqueness"
  alarm_prefix         = "${var.environment}-canary"
  pager_duty_sns_arn   = var.pager_duty_sns_arn
  stage                = "${local.environment_prefix[var.environment]}-canary-ecs"
  evaluation_periods   = 3
  datapoints_to_alarm  = 3
  period               = 300
  latency_metric       = "p90"
  latency_threshold_ms = 100
  low_sev              = true
}

// DeleteEmoticon
module "DeleteEmoticon-alarms" {
  source               = "./twirp-api"
  api_name             = "DeleteEmoticon"
  alarm_prefix         = var.environment
  pager_duty_sns_arn   = var.pager_duty_sns_arn
  stage                = "${local.environment_prefix[var.environment]}-ecs"
  evaluation_periods   = 3
  datapoints_to_alarm  = 3
  period               = 300
  latency_metric       = "p90"
  latency_threshold_ms = 1000
}

module "DeleteEmoticon-canary-alarms" {
  source               = "./twirp-api"
  api_name             = "DeleteEmoticon"
  alarm_prefix         = "${var.environment}-canary"
  pager_duty_sns_arn   = var.pager_duty_sns_arn
  stage                = "${local.environment_prefix[var.environment]}-canary-ecs"
  evaluation_periods   = 3
  datapoints_to_alarm  = 3
  period               = 300
  latency_metric       = "p90"
  latency_threshold_ms = 1000
  low_sev              = true
}

// CreateEmote
module "CreateEmote-alarms" {
  source               = "./twirp-api"
  api_name             = "CreateEmote"
  alarm_prefix         = var.environment
  pager_duty_sns_arn   = var.pager_duty_sns_arn
  stage                = "${local.environment_prefix[var.environment]}-ecs"
  evaluation_periods   = 6
  datapoints_to_alarm  = 6
  period               = 300
  latency_metric       = "p90"
  latency_threshold_ms = 4500
}

module "CreateEmote-canary-alarms" {
  source               = "./twirp-api"
  api_name             = "CreateEmote"
  alarm_prefix         = "${var.environment}-canary"
  pager_duty_sns_arn   = var.pager_duty_sns_arn
  stage                = "${local.environment_prefix[var.environment]}-canary-ecs"
  evaluation_periods   = 12
  datapoints_to_alarm  = 12
  period               = 300
  latency_metric       = "p90"
  latency_threshold_ms = 5000
  low_sev              = true
}

// These alarms are for the T2 / T3 emote modifiers feature.
// See: https://git.xarth.tv/pages/subs/docs/projects/modified-emotes/

module "CreateEmoteModifierGroups-alarms" {
  source             = "./twirp-api"
  api_name           = "CreateEmoteModifierGroups"
  alarm_prefix       = var.environment
  pager_duty_sns_arn = var.pager_duty_sns_arn
  stage              = "${local.environment_prefix[var.environment]}-ecs"
}

module "GetEmoteModifierGroups-alarms" {
  source             = "./twirp-api"
  api_name           = "GetEmoteModifierGroups"
  alarm_prefix       = var.environment
  pager_duty_sns_arn = var.pager_duty_sns_arn
  stage              = "${local.environment_prefix[var.environment]}-ecs"
}

module "UpdateEmoteModifierGroups-alarms" {
  source             = "./twirp-api"
  api_name           = "UpdateEmoteModifierGroups"
  alarm_prefix       = var.environment
  pager_duty_sns_arn = var.pager_duty_sns_arn
  stage              = "${local.environment_prefix[var.environment]}-ecs"
}

module "DeleteEmoteModifierGroups-alarms" {
  source             = "./twirp-api"
  api_name           = "DeleteEmoteModifierGroups"
  alarm_prefix       = var.environment
  pager_duty_sns_arn = var.pager_duty_sns_arn
  stage              = "${local.environment_prefix[var.environment]}-ecs"
}

// Create3PEmoteEntitlements
module "Create3PEmoteEntitlements-alarms" {
  source               = "./twirp-api"
  api_name             = "Create3PEmoteEntitlements"
  alarm_prefix         = var.environment
  pager_duty_sns_arn   = var.pager_duty_sns_arn
  stage                = "${local.environment_prefix[var.environment]}-ecs"
  evaluation_periods   = 3
  datapoints_to_alarm  = 3
  period               = 300
  latency_metric       = "p90"
  latency_threshold_ms = 1000
}

module "Create3PEmoteEntitlements-canary-alarms" {
  source               = "./twirp-api"
  api_name             = "Create3PEmoteEntitlements"
  alarm_prefix         = "${var.environment}-canary"
  pager_duty_sns_arn   = var.pager_duty_sns_arn
  stage                = "${local.environment_prefix[var.environment]}-canary-ecs"
  evaluation_periods   = 3
  datapoints_to_alarm  = 3
  period               = 300
  latency_metric       = "p90"
  latency_threshold_ms = 1000
  low_sev              = true
}

// Update Emote Orders
module "UpdateEmoteOrders-alarms" {
  source               = "./twirp-api"
  api_name             = "UpdateEmoteOrders"
  alarm_prefix         = var.environment
  pager_duty_sns_arn   = var.pager_duty_sns_arn
  stage                = "${local.environment_prefix[var.environment]}-ecs"
  evaluation_periods   = 3
  datapoints_to_alarm  = 3
  period               = 300
  latency_metric       = "p90"
  latency_threshold_ms = 100
}

module "UpdateEmoteOrders-canary-alarms" {
  source               = "./twirp-api"
  api_name             = "UpdateEmoteOrders"
  alarm_prefix         = "${var.environment}-canary"
  pager_duty_sns_arn   = var.pager_duty_sns_arn
  stage                = "${local.environment_prefix[var.environment]}-canary-ecs"
  evaluation_periods   = 3
  datapoints_to_alarm  = 3
  period               = 300
  latency_metric       = "p90"
  latency_threshold_ms = 100
  low_sev              = true
}

// GetAllEmotesByOwnerID
module "GetAllEmotesByOwnerId-alarms" {
  source               = "./twirp-api"
  api_name             = "GetAllEmotesByOwnerId"
  alarm_prefix         = var.environment
  pager_duty_sns_arn   = var.pager_duty_sns_arn
  stage                = "${local.environment_prefix[var.environment]}-ecs"
  evaluation_periods   = 3
  datapoints_to_alarm  = 3
  period               = 300
  latency_metric       = "p90"
  latency_threshold_ms = 100
}

module "GetAllEmotesByOwnerId-canary-alarms" {
  source               = "./twirp-api"
  api_name             = "GetAllEmotesByOwnerId"
  alarm_prefix         = "${var.environment}-canary"
  pager_duty_sns_arn   = var.pager_duty_sns_arn
  stage                = "${local.environment_prefix[var.environment]}-canary-ecs"
  evaluation_periods   = 3
  datapoints_to_alarm  = 3
  period               = 300
  latency_metric       = "p90"
  latency_threshold_ms = 100
  low_sev              = true
}

// AssignEmoteToGroup
module "AssignEmoteToGroup-alarms" {
  source               = "./twirp-api"
  api_name             = "AssignEmoteToGroup"
  alarm_prefix         = var.environment
  pager_duty_sns_arn   = var.pager_duty_sns_arn
  stage                = "${local.environment_prefix[var.environment]}-ecs"
  evaluation_periods   = 3
  datapoints_to_alarm  = 3
  period               = 300
  latency_metric       = "p90"
  latency_threshold_ms = 100
}

module "AssignEmoteToGroup-canary-alarms" {
  source               = "./twirp-api"
  api_name             = "AssignEmoteToGroup"
  alarm_prefix         = "${var.environment}-canary"
  pager_duty_sns_arn   = var.pager_duty_sns_arn
  stage                = "${local.environment_prefix[var.environment]}-canary-ecs"
  evaluation_periods   = 3
  datapoints_to_alarm  = 3
  period               = 300
  latency_metric       = "p90"
  latency_threshold_ms = 100
  low_sev              = true
}

// RemoveEmoteFromGroup
module "RemoveEmoteFromGroup-alarms" {
  source               = "./twirp-api"
  api_name             = "RemoveEmoteFromGroup"
  alarm_prefix         = var.environment
  pager_duty_sns_arn   = var.pager_duty_sns_arn
  stage                = "${local.environment_prefix[var.environment]}-ecs"
  evaluation_periods   = 3
  datapoints_to_alarm  = 3
  period               = 300
  latency_metric       = "p90"
  latency_threshold_ms = 100
}

module "RemoveEmoteFromGroup-canary-alarms" {
  source               = "./twirp-api"
  api_name             = "RemoveEmoteFromGroup"
  alarm_prefix         = "${var.environment}-canary"
  pager_duty_sns_arn   = var.pager_duty_sns_arn
  stage                = "${local.environment_prefix[var.environment]}-canary-ecs"
  evaluation_periods   = 3
  datapoints_to_alarm  = 3
  period               = 300
  latency_metric       = "p90"
  latency_threshold_ms = 100
  low_sev              = true
}

// GetUserEmoteLimits
module "GetUserEmoteLimits-alarms" {
  source               = "./twirp-api"
  api_name             = "GetUserEmoteLimits"
  alarm_prefix         = var.environment
  pager_duty_sns_arn   = var.pager_duty_sns_arn
  stage                = "${local.environment_prefix[var.environment]}-ecs"
  evaluation_periods   = 3
  datapoints_to_alarm  = 3
  period               = 300
  latency_metric       = "p90"
  latency_threshold_ms = 300
}

module "GetUserEmoteLimits-canary-alarms" {
  source               = "./twirp-api"
  api_name             = "GetUserEmoteLimits"
  alarm_prefix         = "${var.environment}-canary"
  pager_duty_sns_arn   = var.pager_duty_sns_arn
  stage                = "${local.environment_prefix[var.environment]}-canary-ecs"
  evaluation_periods   = 3
  datapoints_to_alarm  = 3
  period               = 300
  latency_metric       = "p90"
  latency_threshold_ms = 300
  low_sev              = true
}

// CreateFollowerEmote
module "CreateFollowerEmote-alarms" {
  source               = "./twirp-api"
  api_name             = "CreateFollowerEmote"
  alarm_prefix         = var.environment
  pager_duty_sns_arn   = var.pager_duty_sns_arn
  stage                = "${local.environment_prefix[var.environment]}-ecs"
  evaluation_periods   = 3
  datapoints_to_alarm  = 3
  period               = 300
  latency_metric       = "p90"
  latency_threshold_ms = 5000
}

module "CreateFollowerEmote-canary-alarms" {
  source               = "./twirp-api"
  api_name             = "CreateFollowerEmote"
  alarm_prefix         = "${var.environment}-canary"
  pager_duty_sns_arn   = var.pager_duty_sns_arn
  stage                = "${local.environment_prefix[var.environment]}-canary-ecs"
  evaluation_periods   = 3
  datapoints_to_alarm  = 3
  period               = 300
  latency_metric       = "p90"
  latency_threshold_ms = 5000
  low_sev              = true
}

// AssignEmoteToFollowerSlot
module "AssignEmoteToFollowerSlot-alarms" {
  source               = "./twirp-api"
  api_name             = "AssignEmoteToFollowerSlot"
  alarm_prefix         = var.environment
  pager_duty_sns_arn   = var.pager_duty_sns_arn
  stage                = "${local.environment_prefix[var.environment]}-ecs"
  evaluation_periods   = 3
  datapoints_to_alarm  = 3
  period               = 300
  latency_metric       = "p90"
  latency_threshold_ms = 100
}

module "AssignEmoteToFollowerSlot-canary-alarms" {
  source               = "./twirp-api"
  api_name             = "AssignEmoteToFollowerSlot"
  alarm_prefix         = "${var.environment}-canary"
  pager_duty_sns_arn   = var.pager_duty_sns_arn
  stage                = "${local.environment_prefix[var.environment]}-canary-ecs"
  evaluation_periods   = 3
  datapoints_to_alarm  = 3
  period               = 300
  latency_metric       = "p90"
  latency_threshold_ms = 100
  low_sev              = true
}

// GetAvailableFollowerEmotes
module "GetAvailableFollowerEmotes-alarms" {
  source               = "./twirp-api"
  api_name             = "GetAvailableFollowerEmotes"
  alarm_prefix         = var.environment
  pager_duty_sns_arn   = var.pager_duty_sns_arn
  stage                = "${local.environment_prefix[var.environment]}-ecs"
  evaluation_periods   = 3
  datapoints_to_alarm  = 3
  period               = 300
  latency_metric       = "p90"
  latency_threshold_ms = 100
}

module "GetAvailableFollowerEmotes-canary-alarms" {
  source               = "./twirp-api"
  api_name             = "GetAvailableFollowerEmotes"
  alarm_prefix         = "${var.environment}-canary"
  pager_duty_sns_arn   = var.pager_duty_sns_arn
  stage                = "${local.environment_prefix[var.environment]}-canary-ecs"
  evaluation_periods   = 3
  datapoints_to_alarm  = 3
  period               = 300
  latency_metric       = "p90"
  latency_threshold_ms = 100
  low_sev              = true
}

// GetEmoteEntitlements
module "GetEmoteEntitlements-alarms" {
  source               = "./twirp-api"
  api_name             = "GetEmoteEntitlements"
  alarm_prefix         = var.environment
  pager_duty_sns_arn   = var.pager_duty_sns_arn
  stage                = "${local.environment_prefix[var.environment]}-ecs"
  evaluation_periods   = 3
  datapoints_to_alarm  = 3
  period               = 300
  latency_metric       = "p90"
  latency_threshold_ms = 300
}

module "GetEmoteEntitlements-canary-alarms" {
  source               = "./twirp-api"
  api_name             = "GetEmoteEntitlements"
  alarm_prefix         = "${var.environment}-canary"
  pager_duty_sns_arn   = var.pager_duty_sns_arn
  stage                = "${local.environment_prefix[var.environment]}-canary-ecs"
  evaluation_periods   = 3
  datapoints_to_alarm  = 3
  period               = 300
  latency_metric       = "p90"
  latency_threshold_ms = 300
  low_sev              = true
}

// GetUserFollowerEmoteStatus
module "GetUserFollowerEmoteStatus-alarms" {
  source               = "./twirp-api"
  api_name             = "GetUserFollowerEmoteStatus"
  alarm_prefix         = var.environment
  pager_duty_sns_arn   = var.pager_duty_sns_arn
  stage                = "${local.environment_prefix[var.environment]}-ecs"
  evaluation_periods   = 3
  datapoints_to_alarm  = 3
  period               = 300
  latency_metric       = "p90"
  latency_threshold_ms = 300
}

module "GetUserFollowerEmoteStatus-canary-alarms" {
  source               = "./twirp-api"
  api_name             = "GetUserFollowerEmoteStatus"
  alarm_prefix         = "${var.environment}-canary"
  pager_duty_sns_arn   = var.pager_duty_sns_arn
  stage                = "${local.environment_prefix[var.environment]}-canary-ecs"
  evaluation_periods   = 3
  datapoints_to_alarm  = 3
  period               = 300
  latency_metric       = "p90"
  latency_threshold_ms = 500
  low_sev              = true
}

// GetDefaultEmoteImages
module "GetDefaultEmoteImages-alarms" {
  source               = "./twirp-api"
  api_name             = "GetDefaultEmoteImages"
  alarm_prefix         = var.environment
  pager_duty_sns_arn   = var.pager_duty_sns_arn
  stage                = "${local.environment_prefix[var.environment]}-ecs"
  evaluation_periods   = 3
  datapoints_to_alarm  = 3
  period               = 300
  latency_metric       = "p90"
  latency_threshold_ms = 300
}

module "GetDefaultEmoteImages-canary-alarms" {
  source               = "./twirp-api"
  api_name             = "GetDefaultEmoteImages"
  alarm_prefix         = "${var.environment}-canary"
  pager_duty_sns_arn   = var.pager_duty_sns_arn
  stage                = "${local.environment_prefix[var.environment]}-canary-ecs"
  evaluation_periods   = 3
  datapoints_to_alarm  = 3
  period               = 300
  latency_metric       = "p90"
  latency_threshold_ms = 500
  low_sev              = true
}

// GetChannelEmoteUsage
module "GetChannelEmoteUsage-alarms" {
  source               = "./twirp-api"
  api_name             = "GetChannelEmoteUsage"
  alarm_prefix         = var.environment
  pager_duty_sns_arn   = var.pager_duty_sns_arn
  stage                = "${local.environment_prefix[var.environment]}-ecs"
  evaluation_periods   = 3
  datapoints_to_alarm  = 3
  period               = 300
  latency_metric       = "p90"
  latency_threshold_ms = 500
}

module "GetChannelEmoteUsage-canary-alarms" {
  source               = "./twirp-api"
  api_name             = "GetChannelEmoteUsage"
  alarm_prefix         = "${var.environment}-canary"
  pager_duty_sns_arn   = var.pager_duty_sns_arn
  stage                = "${local.environment_prefix[var.environment]}-canary-ecs"
  evaluation_periods   = 3
  datapoints_to_alarm  = 3
  period               = 300
  latency_metric       = "p90"
  latency_threshold_ms = 500
  low_sev              = true
}
