resource "aws_cloudwatch_metric_alarm" "twirp-latency" {
  alarm_name                = "${var.alarm_prefix}-${var.api_name}-${var.latency_metric}"
  alarm_description         = var.low_sev ? "[LowSev]" : ""
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = var.evaluation_periods
  datapoints_to_alarm       = var.datapoints_to_alarm
  threshold                 = var.latency_threshold_ms
  treat_missing_data        = var.treat_missing_data
  actions_enabled           = length(var.pager_duty_sns_arn) > 0 ? true : false
  insufficient_data_actions = []
  alarm_actions             = [var.pager_duty_sns_arn]
  ok_actions                = [var.pager_duty_sns_arn]

  metric_query {
    id          = "e1"
    expression  = "m1*1000"                                               // convert s to ms
    label       = "${var.api_name} ${var.latency_metric} in milliseconds"
    return_data = true
  }

  metric_query {
    id = "m1"

    metric {
      metric_name = "twirp.status_codes.${var.api_name}.200.latencies"
      namespace   = "mako"
      period      = var.period
      stat        = var.latency_metric

      dimensions = {
        Service = "mako"
        Stage   = var.stage
        Region  = "us-west-2"
      }
    }
  }
}
