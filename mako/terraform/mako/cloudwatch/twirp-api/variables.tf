variable "pager_duty_sns_arn" {
  default     = ""
  description = "ARN of the SNS topic used for Pager Duty alarms"
  type        = string
}

variable "alarm_prefix" {
  type        = string
  description = "the prefix for the alarm's name, i.e. production, staging, etc."
}

variable "api_name" {
  type        = string
  description = "the Twirp API name. Note that this must exactly match what appears in the .protobuf"
}

variable "low_sev" {
  default     = false
  description = "whether the alarms should be a low severity alarm"
}

variable "evaluation_periods" {
  default = 5
}

variable "extreme_error_evaluation_periods" {
  default = 3
}

variable "datapoints_to_alarm" {
  default = 5
}

variable "extreme_error_datapoints_to_alarm" {
  default = 3
}

variable "period" {
  default = 60
}

variable "extreme_error_period" {
  default = 60
}

variable "treat_missing_data" {
  default = "notBreaching"
}

variable "error_rate_threshold" {
  default = 5
}

variable "extreme_error_rate_threshold" {
  default = 50
}

variable "latency_metric" {
  default = "p99"
}

variable "latency_threshold_ms" {
  default = 500
}

variable "stage" {
  default = "prod-ecs" // use prod-ecs-canary for canary alarms
}
