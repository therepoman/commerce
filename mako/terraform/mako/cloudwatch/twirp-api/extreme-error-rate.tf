resource "aws_cloudwatch_metric_alarm" "twirp-extreme-error-rate" {
  alarm_name                = "${var.alarm_prefix}-${var.api_name}-extreme-error-rate"
  alarm_description         = var.low_sev ? "[LowSev]" : ""
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = var.extreme_error_evaluation_periods
  datapoints_to_alarm       = var.extreme_error_datapoints_to_alarm
  threshold                 = var.extreme_error_rate_threshold
  treat_missing_data        = var.treat_missing_data
  actions_enabled           = length(var.pager_duty_sns_arn) > 0 ? true : false
  insufficient_data_actions = []
  alarm_actions             = [var.pager_duty_sns_arn]
  ok_actions                = [var.pager_duty_sns_arn]

  metric_query {
    id          = "e1"
    expression  = "m2/(m1+m2)*100"
    label       = "${var.api_name} extreme error rate (%)"
    return_data = true
  }

  metric_query {
    id = "m1"

    metric {
      metric_name = "twirp.status_codes.${var.api_name}.200"
      namespace   = "mako"
      period      = var.extreme_error_period
      stat        = "Sum"

      dimensions = {
        Service = "mako"
        Stage   = var.stage
        Region  = "us-west-2"
      }
    }
  }

  metric_query {
    id = "m2"

    metric {
      metric_name = "twirp.status_codes.${var.api_name}.500"
      namespace   = "mako"
      period      = var.extreme_error_period
      stat        = "Sum"

      dimensions = {
        Service = "mako"
        Stage   = var.stage
        Region  = "us-west-2"
      }
    }
  }
}