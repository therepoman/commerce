resource "aws_cloudwatch_log_metric_filter" "error_scan" {
  name           = "mako-error-scan"
  pattern        = "level=error"
  log_group_name = var.application_logs_cloudwatch_log_group

  metric_transformation {
    name          = "mako-errors"
    namespace     = var.logscan_namespace
    value         = "1"
    default_value = "0"
  }
}

resource "aws_cloudwatch_log_metric_filter" "panic_scan" {
  name           = "mako-panic-scan"
  pattern        = "\"panic:\""
  log_group_name = var.application_logs_cloudwatch_log_group

  metric_transformation {
    name          = "mako-panics"
    namespace     = var.logscan_namespace
    value         = "1"
    default_value = "0"
  }
}

resource "aws_cloudwatch_log_metric_filter" "fatal_scan" {
  name           = "mako-fatal-scan"
  pattern        = "\"level=fatal\""
  log_group_name = var.application_logs_cloudwatch_log_group

  metric_transformation {
    name          = "mako-fatals"
    namespace     = var.logscan_namespace
    value         = "1"
    default_value = "0"
  }
}
