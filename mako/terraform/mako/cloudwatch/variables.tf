variable "logscan_namespace" {
  default     = "mako-logs"
  description = "Cloudwatch namespace that the LogScan metric belongs to"
  type        = string
}

variable "application_logs_cloudwatch_log_group" {
  default     = "mako"
  description = "Cloudwatch log group in which application logs are written"
  type        = string
}

variable "pager_duty_sns_arn" {
  default     = ""
  description = "ARN of the SNS topic used for Pager Duty alarms"
  type        = string
}

variable "environment" {
  description = "environment for the service, i.e. prod, staging, etc"
}
