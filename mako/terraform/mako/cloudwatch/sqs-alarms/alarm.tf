resource "aws_cloudwatch_metric_alarm" "sqs_dlq_messages_visible_alarm" {
  alarm_name          = "${var.dead_letter_queue_name}-messages-visible"
  alarm_description   = "[LowSev] Alarms when the ${var.dead_letter_queue_name} SQS queue has messages in it."
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = "1"
  datapoints_to_alarm = "1"
  metric_name         = "ApproximateNumberOfMessagesVisible"
  namespace           = "AWS/SQS"

  dimensions = {
    QueueName = var.dead_letter_queue_name
  }

  period                    = "60"
  statistic                 = "Sum"
  threshold                 = "0"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"
  alarm_actions             = [var.pager_duty_sns_arn]
  ok_actions                = [var.pager_duty_sns_arn]
}

resource "aws_cloudwatch_metric_alarm" "sqs_age_of_oldest_message_alarm" {
  alarm_name          = "${var.queue_name}-age-of-oldest-message"
  alarm_description   = "[LowSev] Alarms when the ${var.queue_name} SQS queue contains old messages, which may indicate they are not being processed."
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = "1"
  datapoints_to_alarm = "1"
  metric_name         = "ApproximateAgeOfOldestMessage"
  namespace           = "AWS/SQS"

  dimensions = {
    QueueName = var.queue_name
  }

  period                    = "300"
  statistic                 = "Average"
  threshold                 = "300"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"
  alarm_actions             = [var.pager_duty_sns_arn]
  ok_actions                = [var.pager_duty_sns_arn]
}
