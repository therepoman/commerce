variable "queue_name" {
  type        = string
  description = "the name of the sqs queue to alarm on"
}

variable "dead_letter_queue_name" {
  type        = string
  description = "the name of the sqs dead letter queue to alarm on"
}

variable "pager_duty_sns_arn" {
  type        = string
  description = "ARN of the sns topic to use for pager duty actions"
}
