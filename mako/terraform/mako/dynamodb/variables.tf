variable "environment" {}

variable "service_role" {}

variable "canary_role" {}

variable "pager_duty_sns_arn" {
  default = ""
}
