locals {
  attribution_global_optout_table = "${local.environment_prefix[var.environment]}_attribution_global_optout"

  attribution_global_optout_min_read = {
    production = 200
    staging    = 5
  }

  attribution_global_optout_max_read = {
    production = 400
    staging    = 10
  }

  attribution_global_optout_min_write = {
    production = 100
    staging    = 5
  }

  attribution_global_optout_max_write = {
    production = 200
    staging    = 10
  }
}

// This table holds global opt-out preferences for emote attribution.
resource "aws_dynamodb_table" "attribution_global_optout" {
  name             = local.attribution_global_optout_table
  billing_mode     = "PROVISIONED"
  read_capacity    = local.attribution_global_optout_min_read[var.environment]
  write_capacity   = local.attribution_global_optout_min_write[var.environment]
  hash_key         = "userId"
  stream_enabled   = true
  stream_view_type = "NEW_AND_OLD_IMAGES"

  point_in_time_recovery {
    enabled = true
  }

  lifecycle {
    prevent_destroy = true

    # Ignored due to autoscaling
    ignore_changes = [read_capacity, write_capacity, global_secondary_index]
  }

  attribute {
    name = "userId"
    type = "S"
  }
}

module "attribution-global-optout-dynamo-table-autoscaling" {
  source     = "git::git+ssh://git@git.xarth.tv/subs/terracode//dynamo-autoscaling?ref=e80ce001454204ae42037e5aef41040ad766f2e0"
  kind       = "table"
  table_name = local.attribution_global_optout_table
  min_read   = local.attribution_global_optout_min_read[var.environment]
  max_read   = local.attribution_global_optout_max_read[var.environment]
  min_write  = local.attribution_global_optout_min_write[var.environment]
  max_write  = local.attribution_global_optout_max_write[var.environment]
}

resource "aws_iam_policy" "attribution-global-optout-policy" {
  name = "${local.attribution_global_optout_table}-policy"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "dynamodb:*"
      ],
      "Effect": "Allow",
      "Resource": "${aws_dynamodb_table.attribution_global_optout.arn}"
    },
    {
      "Action": [
        "dynamodb:*"
      ],
      "Effect": "Allow",
      "Resource": "${aws_dynamodb_table.attribution_global_optout.arn}/index/*"
    }
  ]
}
EOF
}

resource "aws_iam_policy_attachment" "attribution-global-optout-service-attachment" {
  name       = "${aws_dynamodb_table.attribution_global_optout.name}-service-attachment"
  roles      = [var.service_role, var.canary_role]
  policy_arn = aws_iam_policy.attribution-global-optout-policy.arn
}

module "AttributionGlobalOptoutTableAlarms" {
  source             = "./../cloudwatch/dynamodb-alarms"
  table              = local.attribution_global_optout_table
  pager_duty_sns_arn = var.pager_duty_sns_arn
}
