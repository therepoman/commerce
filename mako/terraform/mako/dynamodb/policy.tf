resource "aws_iam_policy" "dynamodb-streams-policy" {
  name = "mako-dynamodb-streams-policy"

  policy = <<EOF
{
	"Version": "2012-10-17",
	"Statement": [{
		"Effect": "Allow",
		"Resource": "*",
		"Action": [
			"dynamodb:DescribeStream",
			"dynamodb:GetRecords",
			"dynamodb:GetShardIterator",
			"dynamodb:ListStreams"
		]
	}]
}
EOF
}
