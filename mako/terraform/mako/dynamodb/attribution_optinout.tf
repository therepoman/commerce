locals {
  attribution_optinout_table        = "${local.environment_prefix[var.environment]}_attribution_optinout"
  attribution_role_by_channel_index = "attribution_role_by_channel_gsi"
  attribution_optinout_min_read = {
    production = 200
    staging    = 5
  }

  attribution_optinout_max_read = {
    production = 400
    staging    = 10
  }

  attribution_optinout_min_write = {
    production = 100
    staging    = 5
  }

  attribution_optinout_max_write = {
    production = 200
    staging    = 10
  }

  attribution_role_by_channel_gsi_min_read = {
    production = 200
    staging = 5
  }

  attribution_role_by_channel_gsi_max_read = {
    production = 400
    staging = 10
  }
}

// This table holds per-channel opt-in preferences for emote attribution.
resource "aws_dynamodb_table" "attribution_optinout" {
  name             = local.attribution_optinout_table
  billing_mode     = "PROVISIONED"
  read_capacity    = local.attribution_optinout_min_read[var.environment]
  write_capacity   = local.attribution_optinout_min_write[var.environment]
  hash_key         = "userId"
  range_key        = "channelId"
  stream_enabled   = true
  stream_view_type = "NEW_AND_OLD_IMAGES"

  point_in_time_recovery {
    enabled = true
  }

  lifecycle {
    prevent_destroy = true

    # Ignored due to autoscaling
    ignore_changes = [read_capacity, write_capacity, global_secondary_index]
  }

  attribute {
    name = "userId"
    type = "S"
  }
  attribute {
    name = "channelId"
    type = "S"
  }
  attribute {
    name = "role"
    type = "S"
  }

  global_secondary_index {
    name            = local.attribution_role_by_channel_index
    hash_key        = "channelId"
    range_key       = "role"
    projection_type = "ALL"
    read_capacity   = local.attribution_role_by_channel_gsi_min_read[var.environment]
    write_capacity  = local.attribution_optinout_min_write[var.environment]
  }
}

module "attribution-optinout-dynamo-table-autoscaling" {
  source     = "git::git+ssh://git@git.xarth.tv/subs/terracode//dynamo-autoscaling?ref=e80ce001454204ae42037e5aef41040ad766f2e0"
  kind       = "table"
  table_name = local.attribution_optinout_table
  min_read   = local.attribution_optinout_min_read[var.environment]
  max_read   = local.attribution_optinout_max_read[var.environment]
  min_write  = local.attribution_optinout_min_write[var.environment]
  max_write  = local.attribution_optinout_max_write[var.environment]
}

module "attribution-role-by-channel-index-autoscaling" {
  source     = "git::git+ssh://git@git.xarth.tv/subs/terracode//dynamo-autoscaling?ref=e80ce001454204ae42037e5aef41040ad766f2e0"
  kind       = "index"
  table_name = local.attribution_optinout_table
  index_name = local.attribution_role_by_channel_index
  min_read   = local.attribution_role_by_channel_gsi_min_read[var.environment]
  max_read   = local.attribution_role_by_channel_gsi_max_read[var.environment]
  min_write  = local.attribution_optinout_min_write[var.environment]
  max_write  = local.attribution_optinout_max_write[var.environment]
}

resource "aws_iam_policy" "attribution-optinout-policy" {
  name = "${local.attribution_optinout_table}-policy"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "dynamodb:*"
      ],
      "Effect": "Allow",
      "Resource": "${aws_dynamodb_table.attribution_optinout.arn}"
    },
    {
      "Action": [
        "dynamodb:*"
      ],
      "Effect": "Allow",
      "Resource": "${aws_dynamodb_table.attribution_optinout.arn}/index/*"
    }
  ]
}
EOF
}

resource "aws_iam_policy_attachment" "attribution-optinout-service-attachment" {
  name       = "${aws_dynamodb_table.attribution_optinout.name}-service-attachment"
  roles      = [var.service_role, var.canary_role]
  policy_arn = aws_iam_policy.attribution-optinout-policy.arn
}

module "AttributionOptinoutTableAlarms" {
  source             = "./../cloudwatch/dynamodb-alarms"
  table              = local.attribution_optinout_table
  pager_duty_sns_arn = var.pager_duty_sns_arn
}
