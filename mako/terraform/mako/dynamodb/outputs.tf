output "emote-modifier-groups-stream-arn" {
  value = aws_dynamodb_table.emote-modifier-groups.stream_arn
}

output "emotes-stream-arn" {
  value = aws_dynamodb_table.emotes.stream_arn
}

output "stream-policy-arn" {
  value = aws_iam_policy.dynamodb-streams-policy.arn
}
