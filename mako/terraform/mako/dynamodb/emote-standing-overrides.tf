locals {
  emote_standing_overrides_table = "${local.environment_prefix[var.environment]}_emote_standing_overrides"

  emote_standing_overrides_min_read = {
    production = 50
    staging    = 5
  }

  emote_standing_overrides_max_read = {
    production = 2000
    staging    = 100
  }

  emote_standing_overrides_min_write = {
    production = 5
    staging    = 5
  }

  emote_standing_overrides_max_write = {
    production = 2000
    staging    = 100
  }
}

// This table holds the list of users whose emote good standing status is overwritten.
resource "aws_dynamodb_table" "emote-standing-overrides" {
  name           = local.emote_standing_overrides_table
  billing_mode   = "PROVISIONED"
  read_capacity  = local.emote_standing_overrides_min_read[var.environment]
  write_capacity = local.emote_standing_overrides_min_write[var.environment]
  hash_key       = "userId"

  point_in_time_recovery {
    enabled = true
  }

  lifecycle {
    prevent_destroy = true
  }

  attribute {
    name = "userId"
    type = "S"
  }
}

module "emote-standing-overrides-dynamo-table-autoscaling" {
  source     = "git::git+ssh://git@git.xarth.tv/subs/terracode//dynamo-autoscaling?ref=e80ce001454204ae42037e5aef41040ad766f2e0"
  kind       = "table"
  table_name = local.emote_standing_overrides_table
  min_read   = local.emote_standing_overrides_min_read[var.environment]
  max_read   = local.emote_standing_overrides_max_read[var.environment]
  min_write  = local.emote_standing_overrides_min_write[var.environment]
  max_write  = local.emote_standing_overrides_max_write[var.environment]
}

resource "aws_iam_policy" "emote-standing-overrides-policy" {
  name = "${local.emote_standing_overrides_table}-policy"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "dynamodb:*"
      ],
      "Effect": "Allow",
      "Resource": "${aws_dynamodb_table.emote-standing-overrides.arn}"
    }
  ]
}
EOF
}

resource "aws_iam_policy_attachment" "emote-standing-overrides-service-attachment" {
  name       = "${aws_dynamodb_table.emote-standing-overrides.name}-service-attachment"
  roles      = [var.service_role, var.canary_role]
  policy_arn = aws_iam_policy.emote-standing-overrides-policy.arn
}

module "EmoteStandingOverridesTableAlarms" {
  source             = "./../cloudwatch/dynamodb-alarms"
  table              = local.emote_standing_overrides_table
  pager_duty_sns_arn = var.pager_duty_sns_arn
}
