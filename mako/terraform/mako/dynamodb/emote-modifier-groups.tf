locals {
  environment_prefix = {
    production = "prod"
    staging    = "staging"
  }

  emote_modifier_groups_table         = "${local.environment_prefix[var.environment]}_emote_modifier_groups"
  emote_modifier_groups_index_ownerID = "emote_modifier_groups_index_owner_id"

  emote_modifier_groups_min_read = {
    production = 5000
    staging    = 5
  }

  emote_modifier_groups_max_read = {
    production = 10000
    staging    = 100
  }

  emote_modifier_groups_min_write = {
    production = 100
    staging    = 5
  }

  emote_modifier_groups_max_write = {
    production = 2000
    staging    = 100
  }

  emote_modifier_groups_index_ownerID_min_read = {
    production = 2000
    staging    = 5
  }

  emote_modifier_groups_index_ownerID_max_read = {
    production = 10000
    staging    = 100
  }

  emote_modifier_groups_index_ownerID_min_write = {
    production = 100
    staging    = 5
  }

  emote_modifier_groups_index_ownerID_max_write = {
    production = 2000
    staging    = 100
  }
}

// This table holds the list of modifiers that broadcasters have enabled.
resource "aws_dynamodb_table" "emote-modifier-groups" {
  name             = local.emote_modifier_groups_table
  billing_mode     = "PROVISIONED"
  read_capacity    = local.emote_modifier_groups_min_read[var.environment]
  write_capacity   = local.emote_modifier_groups_min_write[var.environment]
  hash_key         = "id"
  stream_enabled   = true
  stream_view_type = "NEW_AND_OLD_IMAGES"

  point_in_time_recovery {
    enabled = true
  }

  lifecycle {
    prevent_destroy = true
  }

  attribute {
    name = "id"
    type = "S"
  }

  attribute {
    name = "ownerId"
    type = "S"
  }

  global_secondary_index {
    name            = local.emote_modifier_groups_index_ownerID
    hash_key        = "ownerId"
    read_capacity   = local.emote_modifier_groups_index_ownerID_min_read[var.environment]
    write_capacity  = local.emote_modifier_groups_index_ownerID_min_write[var.environment]
    projection_type = "ALL"
  }
}

module "emote-modifier-groups-dynamo-table-autoscaling" {
  source     = "git::git+ssh://git@git.xarth.tv/subs/terracode//dynamo-autoscaling?ref=e80ce001454204ae42037e5aef41040ad766f2e0"
  kind       = "table"
  table_name = local.emote_modifier_groups_table
  min_read   = local.emote_modifier_groups_min_read[var.environment]
  max_read   = local.emote_modifier_groups_max_read[var.environment]
  min_write  = local.emote_modifier_groups_min_write[var.environment]
  max_write  = local.emote_modifier_groups_max_write[var.environment]
}

module "emote-modifier-groups-dynamo-index1-autoscaling" {
  source     = "git::git+ssh://git@git.xarth.tv/subs/terracode//dynamo-autoscaling?ref=e80ce001454204ae42037e5aef41040ad766f2e0"
  kind       = "index"
  table_name = local.emote_modifier_groups_table
  index_name = local.emote_modifier_groups_index_ownerID
  min_read   = local.emote_modifier_groups_index_ownerID_min_read[var.environment]
  max_read   = local.emote_modifier_groups_index_ownerID_max_read[var.environment]
  min_write  = local.emote_modifier_groups_index_ownerID_min_write[var.environment]
  max_write  = local.emote_modifier_groups_index_ownerID_max_write[var.environment]
}

resource "aws_iam_policy" "emote-modifier-groups-policy" {
  name = "${local.emote_modifier_groups_table}-policy"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "dynamodb:*"
      ],
      "Effect": "Allow",
      "Resource": "${aws_dynamodb_table.emote-modifier-groups.arn}"
    },
    {
      "Action": [
        "dynamodb:Query"
      ],
      "Effect": "Allow",
      "Resource": "${aws_dynamodb_table.emote-modifier-groups.arn}/index/*"
    }
  ]
}
EOF
}

resource "aws_iam_policy_attachment" "emote-modifier-groups-service-attachment" {
  name       = "${aws_dynamodb_table.emote-modifier-groups.name}-service-attachment"
  roles      = [var.service_role, var.canary_role]
  policy_arn = aws_iam_policy.emote-modifier-groups-policy.arn
}

// Don't forget the alarms!
module "EmoteModifierGroupsTableAlarms" {
  source             = "./../cloudwatch/dynamodb-alarms"
  table              = local.emote_modifier_groups_table
  pager_duty_sns_arn = var.pager_duty_sns_arn
}

module "EmoteModifierGroupsOwnerIDIndexAlarms" {
  source             = "./../cloudwatch/dynamodb-alarms"
  table              = local.emote_modifier_groups_table
  index              = local.emote_modifier_groups_index_ownerID
  pager_duty_sns_arn = var.pager_duty_sns_arn
}
