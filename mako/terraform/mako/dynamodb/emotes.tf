locals {
  emotes_table          = "${local.environment_prefix[var.environment]}_emotes"
  emotes_owner_id_index = "emotes_by_owner_id_gsi"
  emotes_code_index     = "emotes_by_code_gsi"
  emotes_group_id_index = "emotes_by_group_id_gsi"

  emotes_min_read = {
    production = 2000
    staging    = 5
  }

  emotes_max_read = {
    production = 10000
    staging    = 10
  }

  emotes_by_group_gsi_min_read = {
    production = 10000
    staging    = 5
  }

  emotes_by_group_gsi_max_read = {
    production = 15000
    staging    = 10
  }

  emotes_min_write = {
    production = 1000
    staging    = 5
  }

  emotes_max_write = {
    production = 10000
    staging    = 10
  }
}

// This table holds all emotes.
resource "aws_dynamodb_table" "emotes" {
  name             = local.emotes_table
  read_capacity    = local.emotes_min_read[var.environment]
  write_capacity   = local.emotes_min_write[var.environment]
  hash_key         = "id"
  stream_enabled   = true
  stream_view_type = "NEW_AND_OLD_IMAGES"

  point_in_time_recovery {
    enabled = true
  }

  lifecycle {
    prevent_destroy = true

    # Ignored due to autoscaling
    ignore_changes = [read_capacity, write_capacity, global_secondary_index]
  }

  replica {
    region_name = "us-east-2"
  }

  attribute {
    name = "id"
    type = "S"
  }

  attribute {
    name = "ownerId"
    type = "S"
  }

  attribute {
    name = "code"
    type = "S"
  }

  attribute {
    name = "groupId"
    type = "S"
  }

  global_secondary_index {
    name            = local.emotes_owner_id_index
    hash_key        = "ownerId"
    projection_type = "ALL"
    read_capacity   = local.emotes_min_read[var.environment]
    write_capacity  = local.emotes_min_write[var.environment]
  }

  global_secondary_index {
    name            = local.emotes_code_index
    hash_key        = "code"
    projection_type = "ALL"
    read_capacity   = local.emotes_min_read[var.environment]
    write_capacity  = local.emotes_min_write[var.environment]
  }

  global_secondary_index {
    name            = local.emotes_group_id_index
    hash_key        = "groupId"
    range_key       = "ownerId"
    projection_type = "ALL"
    read_capacity   = local.emotes_by_group_gsi_min_read[var.environment]
    write_capacity  = local.emotes_min_write[var.environment]
  }
}

module "emotes-dynamo-table-autoscaling" {
  source     = "git::git+ssh://git@git.xarth.tv/subs/terracode//dynamo-autoscaling?ref=e80ce001454204ae42037e5aef41040ad766f2e0"
  kind       = "table"
  table_name = local.emotes_table
  min_read   = local.emotes_min_read[var.environment]
  max_read   = local.emotes_max_read[var.environment]
  min_write  = local.emotes_min_write[var.environment]
  max_write  = local.emotes_max_write[var.environment]
}

module "emotes-dynamo-owner-id-index-autoscaling" {
  source     = "git::git+ssh://git@git.xarth.tv/subs/terracode//dynamo-autoscaling?ref=e80ce001454204ae42037e5aef41040ad766f2e0"
  kind       = "index"
  table_name = local.emotes_table
  index_name = local.emotes_owner_id_index
  min_read   = local.emotes_min_read[var.environment]
  max_read   = local.emotes_max_read[var.environment]
  min_write  = local.emotes_min_write[var.environment]
  max_write  = local.emotes_max_write[var.environment]
}

module "emotes-dynamo-code-index-autoscaling" {
  source     = "git::git+ssh://git@git.xarth.tv/subs/terracode//dynamo-autoscaling?ref=e80ce001454204ae42037e5aef41040ad766f2e0"
  kind       = "index"
  table_name = local.emotes_table
  index_name = local.emotes_code_index
  min_read   = local.emotes_min_read[var.environment]
  max_read   = local.emotes_max_read[var.environment]
  min_write  = local.emotes_min_write[var.environment]
  max_write  = local.emotes_max_write[var.environment]
}

module "emotes-dynamo-group-id-index-autoscaling" {
  source     = "git::git+ssh://git@git.xarth.tv/subs/terracode//dynamo-autoscaling?ref=e80ce001454204ae42037e5aef41040ad766f2e0"
  kind       = "index"
  table_name = local.emotes_table
  index_name = local.emotes_group_id_index
  min_read   = local.emotes_by_group_gsi_min_read[var.environment]
  max_read   = local.emotes_by_group_gsi_max_read[var.environment]
  min_write  = local.emotes_min_write[var.environment]
  max_write  = local.emotes_max_write[var.environment]
}

resource "aws_iam_policy" "emotes-policy" {
  name = "${local.emotes_table}-policy"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "dynamodb:*"
      ],
      "Effect": "Allow",
      "Resource": "${aws_dynamodb_table.emotes.arn}"
    },
    {
      "Action": [
        "dynamodb:*"
      ],
      "Effect": "Allow",
      "Resource": "${aws_dynamodb_table.emotes.arn}/index/*"
    }
  ]
}
EOF
}

resource "aws_iam_policy_attachment" "emotes-service-attachment" {
  name       = "${aws_dynamodb_table.emotes.name}-service-attachment"
  roles      = [var.service_role, var.canary_role]
  policy_arn = aws_iam_policy.emotes-policy.arn
}

module "EmotesTableAlarms" {
  source             = "./../cloudwatch/dynamodb-alarms"
  table              = local.emotes_table
  pager_duty_sns_arn = var.pager_duty_sns_arn
}

module "EmotesGroupIDIndexAlarms" {
  source             = "./../cloudwatch/dynamodb-alarms"
  table              = local.emotes_table
  index              = local.emotes_group_id_index
  pager_duty_sns_arn = var.pager_duty_sns_arn
}

module "EmotesOwnerIDIndexAlarms" {
  source             = "./../cloudwatch/dynamodb-alarms"
  table              = local.emotes_table
  index              = local.emotes_owner_id_index
  pager_duty_sns_arn = var.pager_duty_sns_arn
}

module "EmotesCodeIndexAlarms" {
  source             = "./../cloudwatch/dynamodb-alarms"
  table              = local.emotes_table
  index              = local.emotes_code_index
  pager_duty_sns_arn = var.pager_duty_sns_arn
}
