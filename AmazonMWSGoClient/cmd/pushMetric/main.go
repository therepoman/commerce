package main

import (
	"os"
	"time"

	"code.justin.tv/commerce/AmazonMWSGoClient/mws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	log "github.com/sirupsen/logrus"
)

// Pushing a single metric to MWS to demonstrate client.
func main() {

	// Metric metadata used to initialize the metric.
	isProdMetric := false
	hostSpecific := false
	marketplace := "us-west-2"
	serviceName := "TwitchJanusService"
	methodName := "getTestMethod"
	client := ""
	metricType := mws.MetricTime

	hostname, err := os.Hostname()
	if err != nil {
		log.Warn("error while getting hostname; using default value", err)
		hostname = "defaultHostname.amazon.com"
	}

	// Initialize metric object.
	metric := mws.NewMetric(isProdMetric, hostname, hostSpecific, marketplace, serviceName, methodName, client, metricType)

	// Add the actual values of the metric.
	metric.AddValue(900.0)
	metric.Unit = mws.UnitMilliseconds
	metric.Timestamp = time.Now().UTC()

	// Metric report metadata.
	minPeriod := mws.PeriodFiveMinute
	maxPeriod := mws.PeriodFiveMinute
	partitionID := ""
	environment := "Janus/NA"
	customerID := ""

	// Initialize MetricReport and add the Metric.
	metricReport := mws.NewMetricReport(isProdMetric, hostname, minPeriod, maxPeriod, partitionID, environment, customerID)
	metricReport.AddMetric(metric)

	// Wrap the MetricReport in a request object.
	req := mws.NewPutMetricsForAggregationRequest()
	req.AddMetricReport(metricReport)

	// Initialize credentials.
	id := "aws_access_key_id"
	secret := "aws_secret_access_key"
	token := "" // Not necessary for MWS.
	creds := credentials.NewStaticCredentials(id, secret, token)

	// Select an endpoint.  Make sure the region matches the credentials.
	region := mws.Regions["CMH"] // CMH is us-west-2.

	// Make the call and log any error.  Ignoring the response in this case because it will be logged.
	mwsClient := mws.AmazonMWSGoClient{}
	_, err = mwsClient.PutMetricsForAggregation(req, region, creds)
	if err != nil {
		log.Fatal(err)
	}

}
