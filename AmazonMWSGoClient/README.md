# AmazonMWSGoClient
MonitoringWebService is a client used to push metrics which can be veiwed by Amazon's internal iGraph system and trigger internal Amazon alarms.  This package is designed for teams owning mixed Twitch and Amazon services who want to consolidate metrics and alarming on the Amazon side of things.

The only API currently supported by this client is [putMetricDataForAggregation](https://w.amazon.com/index.php/Monitoring/Documentation/MonitoringWebService/PutMetricDataForAggregation).  

## Usage

See [pushMetric](https://git-aws.internal.justin.tv/commerce/AmazonMWSGoClient/blob/master/cmd/pushMetric/main.go) stand-alone for an example of how to use the MWS client.  

MWS API documentation:

- [putMetricDataForAggregation](https://w.amazon.com/index.php/Monitoring/Documentation/MonitoringWebService/PutMetricDataForAggregation)

## aws-sdk-go

AmazonMWSGoClient uses the [aws-sdk-go](https://github.com/aws/aws-sdk-go) package for request signing and credentials objects.

## Security Considerations

The pushMetric demo uses a StaticCredentials object to hold the access and secret key, allowing you to swap in your own keys for temporary testing.  Handling credentials in this way should be used for testing purposes only.  For production code, use either [SharedCredentialsProvider](https://github.com/aws/aws-sdk-go/blob/master/aws/credentials/shared_credentials_provider.go) (Sandstorm) or [EndpointProvider](https://github.com/aws/aws-sdk-go/blob/master/aws/credentials/endpointcreds/provider.go) (Odin).  
