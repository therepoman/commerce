package mws

import (
	"io"
	"net/http"
	"net/http/httptest"
	"strconv"
	"testing"
	"time"

	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/sethgrid/pester"
	. "github.com/smartystreets/goconvey/convey"
)

func TestPutMetricsForAggregation(t *testing.T) {

	Convey("nil request throws error before calling service", t, func() {

		region := getRegion("we won't need this")

		// Dummy creds because we're not actually calling the service.
		var creds = credentials.NewStaticCredentials("id", "secret", "token")

		resp, err := PutMetricsForAggregation(nil, region, creds)

		So(resp, ShouldBeNil)
		So(err, ShouldNotBeNil)
	})

	Convey("a standard request should be handled without error and the response translated", t, func() {
		attempted, committed := 1, 1

		handler := func(writer http.ResponseWriter, providedRequest *http.Request) {
			// Set the body output XML.
			n, err := io.WriteString(writer, getResponseBody(attempted, committed))
			if n <= 0 || err != nil {
				t.Fail()
			}
		}

		server := httptest.NewServer(http.HandlerFunc(handler))
		defer server.Close()

		region := getRegion(server.URL)

		// Dummy creds because we're not actually calling the service.
		var creds = credentials.NewStaticCredentials("id", "secret", "token")

		req := getRequest()

		resp, err := PutMetricsForAggregation(req, region, creds)
		if err != nil {
			t.Fatal(err)
		}

		So(resp.Attempted, ShouldEqual, 1)
		So(resp.Committed, ShouldEqual, 1)
	})

	Convey("successful response with empty body doesn't break things", t, func() {
		handler := func(writer http.ResponseWriter, providedRequest *http.Request) {
			// Return an empty body.
			n, err := io.WriteString(writer, "")
			if n != 0 || err != nil {
				t.Fail()
			}
		}

		server := httptest.NewServer(http.HandlerFunc(handler))
		defer server.Close()

		region := getRegion(server.URL)

		// Dummy creds because we're not actually calling the service.
		var creds = credentials.NewStaticCredentials("id", "secret", "token")

		req := getRequest()

		resp, err := PutMetricsForAggregation(req, region, creds)

		So(resp, ShouldBeNil)
		So(err, ShouldBeNil)
	})

	Convey("successful response with untranslatable body doesn't break things", t, func() {
		handler := func(writer http.ResponseWriter, providedRequest *http.Request) {
			// Write junk to the body.
			n, err := io.WriteString(writer, "What do you think of our new undocumented body format?")
			if n <= 0 || err != nil {
				t.Fail()
			}
		}

		server := httptest.NewServer(http.HandlerFunc(handler))
		defer server.Close()

		region := getRegion(server.URL)

		// Dummy creds because we're not actually calling the service.
		var creds = credentials.NewStaticCredentials("id", "secret", "token")

		req := getRequest()

		resp, err := PutMetricsForAggregation(req, region, creds)

		So(resp, ShouldBeNil)
		So(err, ShouldBeNil)
	})

	Convey("timed out response returns an error and doesn't otherwise break things", t, func() {
		handler := func(writer http.ResponseWriter, providedRequest *http.Request) {
			// Respond half a second after the timeout.
			time.Sleep(time.Millisecond * (httpTimeoutMs + 500))
		}

		server := httptest.NewServer(http.HandlerFunc(handler))
		defer server.Close()

		region := getRegion(server.URL)

		// Dummy creds because we're not actually calling the service.
		var creds = credentials.NewStaticCredentials("id", "secret", "token")

		req := getRequest()

		resp, err := PutMetricsForAggregation(req, region, creds)

		So(resp, ShouldBeNil)
		So(err, ShouldNotBeNil)
	})
}

func TestGetHttpClient(t *testing.T) {
	Convey("clients should initialize with the correct configuration", t, func() {
		client := getHTTPClient()
		So(client.Concurrency, ShouldEqual, 1)
		So(client.Backoff, ShouldEqual, pester.LinearJitterBackoff)
		So(client.MaxRetries, ShouldEqual, attemptLimit)
		So(client.Timeout, ShouldEqual, time.Duration(httpTimeoutMs)*time.Millisecond)
		So(client.KeepLog, ShouldEqual, true)
	})
}

func TestSerializeRequestBody(t *testing.T) {
	Convey("request body is serialized and gzipped", t, func() {
		req := getRequest()
		gzipped, err := serializeRequestBody(req)

		// Unzipping has issues, so just check that something got returned.
		So(len(gzipped), ShouldBeGreaterThan, 100)
		So(err, ShouldBeNil)
	})
}

func TestGetEndpoint(t *testing.T) {
	Convey("getEndpoint returns external endpoint if internal flag is false", t, func() {
		r := Regions["IAD"]
		So(r.UseInternalEndpoint, ShouldBeFalse)
		So(r.getEndpoint(), ShouldEqual, r.ExternalEndpoint)
	})
	Convey("getEndpoint returns internal endpoint if internal flag is true", t, func() {
		r := Regions["IAD"]
		r.UseInternalEndpoint = true
		So(r.getEndpoint(), ShouldEqual, r.InternalEndpoint)
	})
}

func getRequest() *PutMetricsForAggregationRequest {
	// Metric metadata used to initialize the metric.
	isProdMetric := false
	hostSpecific := false
	marketplace := "us-west-2"
	serviceName := "TwitchJanusService"
	methodName := "getTestMethod"
	client := ""
	metricType := MetricTime
	hostname := "host.twitch.tv"

	// Initialize metric object.
	metric := NewMetric(isProdMetric, hostname, hostSpecific, marketplace, serviceName, methodName, client, metricType)

	// Add the actual values of the metric.
	metric.AddValue(900.0)
	metric.Unit = UnitMilliseconds
	metric.Timestamp = time.Now().UTC()

	// Metric report metadata.
	minPeriod := PeriodFiveMinute
	maxPeriod := PeriodFiveMinute
	partitionID := ""
	environment := "Janus/NA"
	customerID := ""

	// Initialize MetricReport and add the Metric.
	metricReport := NewMetricReport(isProdMetric, hostname, minPeriod, maxPeriod, partitionID, environment, customerID)
	metricReport.AddMetric(metric)

	// Wrap the MetricReport in a request object.
	req := NewPutMetricsForAggregationRequest()
	req.AddMetricReport(metricReport)

	return req
}

func getResponseBody(attempted, committed int) string {
	return "<PutMetricDataForAggregationResponse xmlns=\"http://mws.amazonaws.com/doc/2007-07-07/\">\n<NumberOfAttempted>" +
		strconv.Itoa(attempted) + "</NumberOfAttempted><NumberOfCommitted>" +
		strconv.Itoa(committed) + "</NumberOfCommitted>\n</PutMetricDataForAggregationResponse>\n"
}

func getRegion(url string) Region {
	// Get any region and override the endpoints with the local endpoint.
	region := Regions["PDX"]
	region.ExternalEndpoint = url
	region.InternalEndpoint = url

	return region
}
