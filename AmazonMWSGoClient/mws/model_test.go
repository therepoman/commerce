package mws

import (
	"reflect"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestNewMetric(t *testing.T) {
	Convey("initializing a minimal metric", t, func() {
		isProdMetric := false
		hostname := "hostname"
		hostSpecific := false
		marketplace := "marketplace"
		serviceName := "service name"
		methodName := ""
		client := ""
		metricName := "metric name"

		m := NewMetric(isProdMetric, hostname, hostSpecific, marketplace, serviceName, methodName, client, metricName)

		So(m.Dimensions.DataSet, ShouldEqual, StageTest)
		So(m.Dimensions.HostGroup, ShouldEqual, "ALL")
		So(m.Dimensions.Host, ShouldEqual, "ALL")
		So(m.Dimensions.MethodName, ShouldEqual, "ALL")
		So(m.Dimensions.Client, ShouldEqual, "ALL")
		So(m.Dimensions.Marketplace, ShouldEqual, marketplace)
		So(m.Dimensions.ServiceName, ShouldEqual, serviceName)
		So(m.MetricName, ShouldEqual, metricName)
		So(m.Unit, ShouldEqual, "")
		So(len(m.Values), ShouldEqual, 0)
	})

	Convey("initializing a metric with non-default values", t, func() {
		isProdMetric := true
		hostname := "hostname"
		hostSpecific := true
		marketplace := "marketplace"
		serviceName := "service name"
		methodName := "method name"
		client := "client"
		metricName := "metric name"

		m := NewMetric(isProdMetric, hostname, hostSpecific, marketplace, serviceName, methodName, client, metricName)

		So(m.Dimensions.DataSet, ShouldEqual, StageProd)
		So(m.Dimensions.HostGroup, ShouldEqual, "NONE")
		So(m.Dimensions.Host, ShouldEqual, hostname)
		So(m.Dimensions.MethodName, ShouldEqual, methodName)
		So(m.Dimensions.Client, ShouldEqual, client)
		So(m.Dimensions.Marketplace, ShouldEqual, marketplace)
		So(m.Dimensions.ServiceName, ShouldEqual, serviceName)
		So(m.MetricName, ShouldEqual, metricName)
	})
}

func TestNewPutMetricsForAggregationRequest(t *testing.T) {
	Convey("initializing a minimal request", t, func() {
		r := NewPutMetricsForAggregationRequest()

		So(len(r.MetricReports), ShouldEqual, 0)
	})
}

func TestNewMetricReport(t *testing.T) {
	Convey("initializing a minimal MetricReport", t, func() {
		prod := false
		minPeriod := ""
		maxPeriod := ""
		partitionID := ""
		host := "hostname"
		environment := "Janus/NA"
		customerID := ""

		r := NewMetricReport(prod, host, minPeriod, maxPeriod, partitionID, environment, customerID)

		So(r.Metadata.Host, ShouldEqual, host)
		So(r.Metadata.CustomerID, ShouldEqual, customerID)
		So(r.Metadata.Environment, ShouldEqual, environment)
		So(r.Metadata.MinPeriod, ShouldEqual, PeriodOneMinute)
		So(r.Metadata.MaxPeriod, ShouldEqual, PeriodOneHour)
		So(r.Metadata.PartitionID, ShouldEqual, host)
		So(r.Metadata.Stage, ShouldEqual, StageTest)
	})

	Convey("initializing a MetricReport with non-default values", t, func() {
		prod := true
		minPeriod := PeriodFiveMinute
		maxPeriod := PeriodFiveMinute
		partitionID := "foo/bar"
		host := "hostname"
		environment := "Janus/NA"
		customerID := "12345"

		r := NewMetricReport(prod, host, minPeriod, maxPeriod, partitionID, environment, customerID)

		So(r.Metadata.Host, ShouldEqual, host)
		So(r.Metadata.CustomerID, ShouldEqual, customerID)
		So(r.Metadata.Environment, ShouldEqual, environment)
		So(r.Metadata.MinPeriod, ShouldEqual, minPeriod)
		So(r.Metadata.MaxPeriod, ShouldEqual, maxPeriod)
		So(r.Metadata.PartitionID, ShouldEqual, partitionID)
		So(r.Metadata.Stage, ShouldEqual, StageProd)
	})
}

func TestMetric_AddValue(t *testing.T) {
	Convey("adding values to a metric", t, func() {
		m := NewMetric(false, "hostname", false, "marketplace", "serviceName", "methodName", "client", "metricName")

		val := 1.0
		m.AddValue(val)
		So(len(m.Values), ShouldEqual, 1)
		So(m.Values[0], ShouldEqual, val)

		val2 := 2.0
		m.AddValue(val2)
		So(len(m.Values), ShouldEqual, 2)
		So(m.Values[0], ShouldEqual, val)
		So(m.Values[1], ShouldEqual, val2)
	})
}

func TestMetricReport_AddMetric(t *testing.T) {

	Convey("Adding a metric scoped to client and method name to a metric report should create three metrics", t, func() {
		metricReport := NewMetricReport(false, "hostname", PeriodFiveMinute, PeriodFiveMinute, "partitionId", "environment", "customerId")
		metric := NewMetric(false, "hostname", false, "marketplace", "serviceName", "methodName", "client", "metricName")
		metricReport.AddMetric(metric)
		// Adding the metric means the metric, the (method = ALL, client = ALL), and the (method = methodName, client = ALL)
		// versions are reported. metricReport will have 3 metrics.
		So(len(metricReport.Metrics), ShouldEqual, 3)
		So(MetricInSlice(metric, metricReport.Metrics), ShouldBeTrue)
		metricClientAll := NewMetric(false, "hostname", false, "marketplace", "serviceName", "methodName", "ALL", "metricName")
		So(MetricInSlice(metricClientAll, metricReport.Metrics), ShouldBeTrue)
		metricMethodNameAllClientAll := NewMetric(false, "hostname", false, "marketplace", "serviceName", "ALL", "ALL", "metricName")
		So(MetricInSlice(metricMethodNameAllClientAll, metricReport.Metrics), ShouldBeTrue)

	})

	Convey("Adding a metric scoped to method name to a metric report should create two metrics", t, func() {
		metricReport := NewMetricReport(false, "hostname", PeriodFiveMinute, PeriodFiveMinute, "partitionId", "environment", "customerId")
		metric := NewMetric(false, "hostname", false, "marketplace", "serviceName", "methodName", "ALL", "metricName")
		metricReport.AddMetric(metric)
		// Adding this metric means the metric and the (method = ALL, client = ALL) versions are reported
		// Therefore, metricReport will have 2 metrics.
		So(len(metricReport.Metrics), ShouldEqual, 2)
		So(MetricInSlice(metric, metricReport.Metrics), ShouldBeTrue)
		metricMethodNameAllClientAll := NewMetric(false, "hostname", false, "marketplace", "serviceName", "ALL", "ALL", "metricName")
		So(MetricInSlice(metricMethodNameAllClientAll, metricReport.Metrics), ShouldBeTrue)
	})

	Convey("Adding an unscoped metric to a metric report should create a single metric", t, func() {
		metricReport := NewMetricReport(false, "hostname", PeriodFiveMinute, PeriodFiveMinute, "partitionId", "environment", "customerId")
		metric := NewMetric(false, "hostname", false, "marketplace", "serviceName", "ALL", "ALL", "metricName")
		metricReport.AddMetric(metric)
		// metricReport will have a single metric
		So(len(metricReport.Metrics), ShouldEqual, 1)
		So(MetricInSlice(metric, metricReport.Metrics), ShouldBeTrue)
	})

	Convey("Adding a new metric to a metric report should not affect previously added metrics in that same metric report", t, func() {
		metricReport := NewMetricReport(false, "hostname", PeriodFiveMinute, PeriodFiveMinute, "partitionId", "environment", "customerId")
		metric := NewMetric(false, "hostname", false, "marketplace", "serviceName", "ALL", "ALL", "metricName")
		metricReport.AddMetric(metric)
		// metricReport will have a single metric
		So(len(metricReport.Metrics), ShouldEqual, 1)
		So(MetricInSlice(metric, metricReport.Metrics), ShouldBeTrue)
		// Add the second metric
		metric2 := NewMetric(false, "hostname", false, "marketplace", "serviceName", "ALL", "ALL", "metricName2")
		metricReport.AddMetric(metric2)
		// metricReport should now have two metrics
		So(len(metricReport.Metrics), ShouldEqual, 2)
		So(MetricInSlice(metric, metricReport.Metrics), ShouldBeTrue)
		So(MetricInSlice(metric2, metricReport.Metrics), ShouldBeTrue)
	})

	Convey("Adding metrics to one metric report should not affect the metrics of a different metric report", t, func() {
		metricReport := NewMetricReport(false, "hostname", PeriodFiveMinute, PeriodFiveMinute, "partitionId", "environment", "customerId")
		metric := NewMetric(false, "hostname", false, "marketplace", "serviceName", "ALL", "ALL", "metricName")
		metricReport.AddMetric(metric)
		// metricReport will have a single metric
		So(len(metricReport.Metrics), ShouldEqual, 1)
		So(MetricInSlice(metric, metricReport.Metrics), ShouldBeTrue)

		metricReport2 := NewMetricReport(false, "hostname2", PeriodFiveMinute, PeriodFiveMinute, "partitionId", "environment", "customerId")
		metric2 := NewMetric(false, "hostname2", false, "marketplace", "serviceName", "ALL", "ALL", "metricName")
		metricReport2.AddMetric(metric2)
		// metricReport2 will have a single metric
		So(len(metricReport2.Metrics), ShouldEqual, 1)
		So(MetricInSlice(metric2, metricReport2.Metrics), ShouldBeTrue)

		// Sanity check the first metricReport is unchanged
		So(len(metricReport.Metrics), ShouldEqual, 1)
		So(MetricInSlice(metric, metricReport.Metrics), ShouldBeTrue)
	})
}

func TestPutMetricsForAggregationRequest_AddMetricReport(t *testing.T) {
	Convey("adding metric reports to a request", t, func() {
		req := NewPutMetricsForAggregationRequest()
		metricReport := NewMetricReport(false, "hostname", PeriodFiveMinute, PeriodFiveMinute, "partitionId", "environment", "customerId")

		req.AddMetricReport(metricReport)
		So(len(req.MetricReports), ShouldEqual, 1)
		So(reflect.DeepEqual(req.MetricReports[0], metricReport), ShouldBeTrue)

		metricReport2 := NewMetricReport(false, "hostname", PeriodFiveMinute, PeriodFiveMinute, "partitionId", "environment", "customerId")
		req.AddMetricReport(metricReport2)
		So(len(req.MetricReports), ShouldEqual, 2)
		So(reflect.DeepEqual(req.MetricReports[0], metricReport), ShouldBeTrue)
		So(reflect.DeepEqual(req.MetricReports[1], metricReport2), ShouldBeTrue)
	})
}

// Helper method to check whether a metric is part of a given metric list
func MetricInSlice(metric Metric, allMetrics []Metric) bool {
	for _, b := range allMetrics {
		if reflect.DeepEqual(b, metric) == true {
			return true
		}
	}
	return false
}
