# Client for TACHANKA

## Adding a new endpoint to Tachanka
Add the following to tachanka-service.proto:
1) Declaration for your endpoint: ``` rpc <your endpoint name> (<your request message name>) returns (<your response message name>); ``` in the ```TachankaClient``` struct
2) Definition for the request message and response message. Message structure will have members in defined in the form ```<type> <member parameter name> = <ordinal number>;```

After this run ```make all```
