package payout

import (
	"context"

	"code.justin.tv/commerce/payday/metrics"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/hystrix"
	cmd "code.justin.tv/commerce/payday/hystrix"
	"code.justin.tv/commerce/payday/user"
	moneypenny "code.justin.tv/revenue/moneypenny/client"
)

type PayoutManager struct {
	Cfg              *config.Configuration `inject:""`
	MoneyPennyClient moneypenny.Client     `inject:""`
	Metrics          metrics.Statter       `inject:""`
}

type IPayoutManager interface {
	HasServicesTaxInfo(ctx context.Context, userId string) (bool, error)
}

func (pm *PayoutManager) isChannelOnOverride(userId string) bool {
	for _, overrideId := range pm.Cfg.SupportedCountryOverride {
		if overrideId == userId {
			return true
		}
	}
	return false
}

func (pm *PayoutManager) HasServicesTaxInfo(ctx context.Context, userId string) (bool, error) {
	if pm.isChannelOnOverride(userId) || user.IsATestAccount(userId) {
		return true, nil
	}

	var resp *moneypenny.GetPayoutEntityForChannelResponse
	var err error
	err = hystrix.Do(cmd.GetPayoutEntityForChannelCommand, func() error {
		resp, err = pm.MoneyPennyClient.GetPayoutEntityForChannel(ctx, userId, nil)
		return err
	}, nil)

	if err != nil {
		msg := "Could not retrieve payout entity from moneypenny"
		log.WithField("userId", userId).WithError(err).Error(msg)
		return false, errors.Notef(err, msg)
	}

	payoutEntityID := resp.PayoutEntityID
	if payoutEntityID == "" {
		return false, nil
	}

	return resp.TaxWithholdingRate.ServiceTaxWithholdingRate != nil, nil
}
