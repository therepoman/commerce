package payout

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/payday/config"
	metrics_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/metrics"
	moneypenny_mock "code.justin.tv/commerce/payday/mocks/moneypenny"
	moneypenny "code.justin.tv/revenue/moneypenny/client"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func TestHasServicesTaxInfo_Happy(t *testing.T) {
	mockedMoneyPennyClient := new(moneypenny_mock.Client)
	mockedMetrics := new(metrics_mock.Statter)

	payoutEntityID := "2"
	placeholderRate := 0.0
	mockedMoneyPennyClient.On("GetPayoutEntityForChannel", mock.Anything, "1", mock.Anything).Return(&moneypenny.GetPayoutEntityForChannelResponse{
		PayoutEntityID: payoutEntityID,
		TaxWithholdingRate: moneypenny.TaxWithholding{
			ServiceTaxWithholdingRate: &placeholderRate,
		},
	}, nil)

	mockedMetrics.On("Inc", "tailsoffboard_problem.moneypenny_yes", int64(0)).Return()
	mockedMetrics.On("Inc", "tailsoffboard_problem.tails_yes", int64(0)).Return()

	pm := &PayoutManager{
		Cfg:              &config.Configuration{},
		MoneyPennyClient: mockedMoneyPennyClient,
		Metrics:          mockedMetrics,
	}

	hasServices, err := pm.HasServicesTaxInfo(context.Background(), "1")

	assert.Nil(t, err)
	assert.True(t, hasServices)
}

func TestHasServicesTaxInfo_RoyaltyAndServiceDifferent(t *testing.T) {
	mockedMoneyPennyClient := new(moneypenny_mock.Client)
	mockedMetrics := new(metrics_mock.Statter)
	placeholderRate := 0.0

	mockedMoneyPennyClient.On("GetPayoutEntityForChannel", mock.Anything, "1", mock.Anything).Return(&moneypenny.GetPayoutEntityForChannelResponse{
		PayoutEntityID: "2",
		TaxWithholdingRate: moneypenny.TaxWithholding{
			ServiceTaxWithholdingRate: &placeholderRate,
		},
	}, nil)

	mockedMetrics.On("Inc", "tailsoffboard_problem.moneypenny_yes", int64(0)).Return()
	mockedMetrics.On("Inc", "tailsoffboard_problem.tails_yes", int64(0)).Return()

	pm := &PayoutManager{
		Cfg:              &config.Configuration{},
		MoneyPennyClient: mockedMoneyPennyClient,
		Metrics:          mockedMetrics,
	}

	hasServices, err := pm.HasServicesTaxInfo(context.Background(), "1")

	assert.Nil(t, err)
	assert.True(t, hasServices)
}

func TestHasServicesTaxInfo_RoyaltyOnly(t *testing.T) {
	mockedMoneyPennyClient := new(moneypenny_mock.Client)
	mockedMetrics := new(metrics_mock.Statter)

	mockedMoneyPennyClient.On("GetPayoutEntityForChannel", mock.Anything, "1", mock.Anything).Return(&moneypenny.GetPayoutEntityForChannelResponse{
		PayoutEntityID: "2",
	}, nil)

	pm := &PayoutManager{
		Cfg:              &config.Configuration{},
		MoneyPennyClient: mockedMoneyPennyClient,
		Metrics:          mockedMetrics,
	}

	mockedMetrics.On("Inc", "tailsoffboard_problem.moneypenny_yes", int64(0)).Return()
	mockedMetrics.On("Inc", "tailsoffboard_problem.tails_yes", int64(0)).Return()

	hasServices, err := pm.HasServicesTaxInfo(context.Background(), "1")

	assert.NoError(t, err)
	assert.False(t, hasServices)
}

func TestHasServicesTaxInfo_UnsupportedCountry(t *testing.T) {
	mockedMoneyPennyClient := new(moneypenny_mock.Client)
	mockedMetrics := new(metrics_mock.Statter)
	placeholderRate := 0.0

	mockedMoneyPennyClient.On("GetPayoutEntityForChannel", mock.Anything, "1", mock.Anything).Return(&moneypenny.GetPayoutEntityForChannelResponse{
		PayoutEntityID: "2",
		TaxWithholdingRate: moneypenny.TaxWithholding{
			ServiceTaxWithholdingRate: &placeholderRate,
		},
	}, nil)

	pm := &PayoutManager{
		Cfg:              &config.Configuration{},
		MoneyPennyClient: mockedMoneyPennyClient,
		Metrics:          mockedMetrics,
	}

	mockedMetrics.On("Inc", "tailsoffboard_problem.moneypenny_yes", int64(0)).Return()
	mockedMetrics.On("Inc", "tailsoffboard_problem.tails_yes", int64(0)).Return()

	hasServices, err := pm.HasServicesTaxInfo(context.Background(), "1")

	assert.Nil(t, err)
	assert.True(t, hasServices)
}

func TestHasServicesTaxInfo_NoTIMSRecords(t *testing.T) {
	mockedMoneyPennyClient := new(moneypenny_mock.Client)
	mockedMetrics := new(metrics_mock.Statter)

	mockedMoneyPennyClient.On("GetPayoutEntityForChannel", mock.Anything, "1", mock.Anything).Return(&moneypenny.GetPayoutEntityForChannelResponse{
		PayoutEntityID: "2",
	}, nil)

	pm := &PayoutManager{
		Cfg:              &config.Configuration{},
		MoneyPennyClient: mockedMoneyPennyClient,
		Metrics:          mockedMetrics,
	}

	mockedMetrics.On("Inc", "tailsoffboard_problem.moneypenny_yes", int64(0)).Return()
	mockedMetrics.On("Inc", "tailsoffboard_problem.tails_yes", int64(0)).Return()

	hasServices, err := pm.HasServicesTaxInfo(context.Background(), "1")

	assert.Nil(t, err)
	assert.False(t, hasServices)
}

func TestHasServicesTaxInfo_NoPayout(t *testing.T) {
	mockedMoneyPennyClient := new(moneypenny_mock.Client)
	mockedMetrics := new(metrics_mock.Statter)

	mockedMoneyPennyClient.On("GetPayoutEntityForChannel", mock.Anything, "1", mock.Anything).Return(&moneypenny.GetPayoutEntityForChannelResponse{
		PayoutEntityID: "",
	}, nil)

	pm := &PayoutManager{
		Cfg:              &config.Configuration{},
		MoneyPennyClient: mockedMoneyPennyClient,
		Metrics:          mockedMetrics,
	}

	hasServices, err := pm.HasServicesTaxInfo(context.Background(), "1")
	assert.NoError(t, err)
	assert.False(t, hasServices)
}

func TestHasServicesTaxInfo_Override(t *testing.T) {
	userId := "9001"
	mockedMoneyPennyClient := new(moneypenny_mock.Client)
	mockedMetrics := new(metrics_mock.Statter)

	mockedMoneyPennyClient.On("GetPayoutEntityForChannel", mock.Anything, userId, mock.Anything).Return(&moneypenny.GetPayoutEntityForChannelResponse{
		PayoutEntityID: "2",
	}, nil)

	pm := &PayoutManager{
		Cfg: &config.Configuration{
			SupportedCountryOverride: []string{userId},
		},
		MoneyPennyClient: mockedMoneyPennyClient,
		Metrics:          mockedMetrics,
	}

	hasServices, err := pm.HasServicesTaxInfo(context.Background(), userId)

	assert.NoError(t, err)
	assert.True(t, hasServices)
	assert.Equal(t, 0, len(mockedMoneyPennyClient.Calls))
}

func TestMoneyPennyThrowsError(t *testing.T) {
	userId := "9001"
	mockedMoneyPennyClient := new(moneypenny_mock.Client)
	mockedMetrics := new(metrics_mock.Statter)

	testError := errors.New("test")
	mockedMoneyPennyClient.On("GetPayoutEntityForChannel", mock.Anything, userId, mock.Anything).Return(&moneypenny.GetPayoutEntityForChannelResponse{}, testError)

	pm := &PayoutManager{
		Cfg:              &config.Configuration{},
		MoneyPennyClient: mockedMoneyPennyClient,
		Metrics:          mockedMetrics,
	}

	_, err := pm.HasServicesTaxInfo(context.Background(), userId)

	assert.Error(t, err)
	mockedMoneyPennyClient.AssertExpectations(t)
}
