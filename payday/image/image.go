package image

import (
	"archive/zip"
	"context"
	"crypto/sha1"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io"
	"strconv"
	"strings"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/actions/constants"
	"code.justin.tv/commerce/payday/actions/utils"
	"code.justin.tv/commerce/payday/cache"
	"code.justin.tv/commerce/payday/clients/uploadservice"
	"code.justin.tv/commerce/payday/cloudfront"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/datascience"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/emote"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/models/api"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"code.justin.tv/commerce/payday/s3"
	"code.justin.tv/commerce/payday/sns"
	util "code.justin.tv/commerce/payday/utils/close"
	bits_strings "code.justin.tv/commerce/payday/utils/strings"
	upload_service "code.justin.tv/web/upload-service/models"
	"github.com/aws/aws-sdk-go/aws/awserr"
	s3sdk "github.com/aws/aws-sdk-go/service/s3"
	"github.com/gofrs/uuid"
	"golang.org/x/sync/errgroup"
)

const (
	BitsAssetsS3Bucket         = "bits-assets"
	PartnerActionsS3KeyPrefix  = "partner-actions"
	ArchiveLocationS3KeyPrefix = "archive"
	CloudfrontPrefix           = "https://d3aqoihi2n8ty8.cloudfront.net"
	S3Prefix                   = "https://s3-us-west-2.amazonaws.com/bits-assets"
	CreationMethodGenerated    = "GENERATED"
	CreationMethodSource       = "SOURCE"
	CreationMethodMako         = "MAKO"
	NUCLEAR_MODERATION_TYPE    = "nuclear"
)

const (
	IndividualUpload = api.UploadType("individual")
	SmartUpload      = api.UploadType("smart")
)

type Dimension int

const (
	Dimension1  = Dimension(28)
	Dimension15 = Dimension(42)
	Dimension2  = Dimension(56)
	Dimension3  = Dimension(84)
	Dimension4  = Dimension(112)
)

const (
	bitsAssetBucketPrefix       = BitsAssetsS3Bucket + "/"
	bitsAssetS3IdentifierPrefix = "s3://" + bitsAssetBucketPrefix
)

var UploadTypes map[api.UploadType]interface{} = map[api.UploadType]interface{}{IndividualUpload: nil, SmartUpload: nil}

var totalPossibleImageTypes int

var (
	ErrImageAssetsNotFound = errors.New("One or more image asset ids provided could not be found in S3")
	ErrInvalidImageAssets  = errors.New("One or more provided image assets were invalid")
)

func init() {
	totalPossibleImageTypes = len(constants.Backgrounds) * len(constants.AnimationTypes) * len(constants.Tiers) * len(constants.Scales)
}

var scaleToImgDimension map[api.Scale]Dimension = map[api.Scale]Dimension{
	constants.Scale1:  Dimension1,
	constants.Scale15: Dimension15,
	constants.Scale2:  Dimension2,
	constants.Scale3:  Dimension3,
	constants.Scale4:  Dimension4,
}

type ImageMap map[api.Tier]map[api.Background]map[api.AnimationType]map[api.Scale]*dynamo.Image
type TierToFilenameMap map[int32]string

func (imageMap ImageMap) GetActionTiers(backgrounds bits_strings.Set, states bits_strings.Set,
	scales bits_strings.Set, lastUpdated time.Time, actionType string) ([]api.ActionTier, error) {

	tiers := make([]api.ActionTier, 0)

	for tierId, backgroundMap := range imageMap {
		tierImages := make(api.TierImages)
		for background, animationTypeMap := range backgroundMap {
			backgrounds.Add(string(background))
			for animationType, scaleMap := range animationTypeMap {
				states.Add(string(animationType))
				for scale, img := range scaleMap {
					scales.Add(string(scale))
					tierImages.Put(string(background), string(animationType), string(scale), GetCloudfrontURLFromImg(*img))

					if lastUpdated.Before(img.LastUpdated) {
						lastUpdated = img.LastUpdated
					}
				}
			}
		}

		numericTier, err := strconv.ParseInt(string(tierId), 10, 64)
		if err != nil {
			msg := fmt.Sprintf("imageMap contained non-numeric tier: %s", tierId)
			log.Error(msg, err)
			return nil, errors.Notef(err, msg)
		}

		tierColor, tierColorPresent := constants.TierToColor[tierId]
		if !tierColorPresent {
			msg := "No color found for tier"
			log.WithFields(log.Fields{
				"tierToColor": constants.TierToColor,
				"tier":        tierId,
			}).Error(msg)
			return nil, errors.New(msg)
		}

		canCheer := utils.CanCheerActionTier(numericTier, actionType)
		showInBitsCard := utils.CanShowTierInBitsCard(numericTier, actionType)

		tier := api.ActionTier{
			BitsMinimum:    numericTier,
			Id:             string(tierId),
			Color:          tierColor,
			Images:         &tierImages,
			CanCheer:       canCheer,
			ShowInBitsCard: showInBitsCard,
		}
		tiers = append(tiers, tier)
	}

	return tiers, nil
}

func (i ImageMap) Len() int {
	count := 0
	for _, backgroundMap := range i {
		for _, animationTypeMap := range backgroundMap {
			for _, scaleMap := range animationTypeMap {
				count += len(scaleMap)
			}
		}
	}
	return count
}

func (i ImageMap) AllImagesSet() bool {
	return i.Len() >= totalPossibleImageTypes
}

func (i ImageMap) PutImage(img *dynamo.Image) {
	imgTier := api.Tier(img.Tier)
	imgBackground := api.Background(img.Background)
	imgAnimationType := api.AnimationType(img.AnimationType)
	imgScale := api.Scale(img.Scale)
	i.Put(imgTier, imgBackground, imgAnimationType, imgScale, img)
}

func (i ImageMap) Put(tier api.Tier, background api.Background, animationType api.AnimationType, scale api.Scale, img *dynamo.Image) {
	_, backgroundMapPresent := i[tier]
	if !backgroundMapPresent {
		i[tier] = make(map[api.Background]map[api.AnimationType]map[api.Scale]*dynamo.Image)
	}

	_, animationTypeMapPresent := i[tier][background]
	if !animationTypeMapPresent {
		i[tier][background] = make(map[api.AnimationType]map[api.Scale]*dynamo.Image)
	}

	_, scaleMapPresent := i[tier][background][animationType]
	if !scaleMapPresent {
		i[tier][background][animationType] = make(map[api.Scale]*dynamo.Image)
	}

	i[tier][background][animationType][scale] = img
}

type S3Key string

type ImageManager struct {
	ImagesCache                 cache.IImagesCache           `inject:""`
	ImageDao                    dynamo.IImageDao             `inject:""`
	S3Client                    s3.IS3Client                 `inject:""`
	SNSClient                   sns.ISNSClient               `inject:"usWestSNSClient"`
	CustomCheermoteImageSlacker ICustomCheermoteImageSlacker `inject:""`
	PrefixManager               emote.IPrefixManager         `inject:""`
	Datascience                 datascience.DataScience      `inject:""`
	Cloudfront                  cloudfront.ICloudFrontClient `inject:""`
	Config                      *config.Configuration        `inject:""`
}

type ImageModerationSNS struct {
	ChannelId      string `json:"channel_id"`
	ModerationType string `json:"moderation_type"`
}

type IImageManager interface {
	DoesChannelHaveS3Images(channelId string) (bool, error)
	DeleteChannelS3Images(ctx context.Context, channelId string) error
	DeleteChannelImageSetTierImages(ctx context.Context, channelID string, setID string, tier api.Tier) (bool, error)
	ArchiveImagesForChannel(ctx context.Context, channelId string, imageKeys []string) error
	PublishImageModerationJob(channelId string) error
	ProcessUploadService(ctx context.Context, uploadSNSMsg upload_service.SNSCallback) error
	ProcessMakoImages(ctx context.Context, channelID, setID string, tier api.Tier, staticAsset, animatedAsset paydayrpc.EmoteImageAsset) error
	GetImages(ctx context.Context, setId string) (ImageMap, error)
	WriteImagesZip(ctx context.Context, setId string, w io.Writer) error
	GetCloudfrontURLFromKey(key S3Key) string
	TrackChannelEnabled(ctx context.Context, channel dynamo.Channel) error
}

var InvalidURLsErr = errors.New("Invalid URLs")
var S3KeyNotFoundErr = errors.New("No S3 key found for this animation type and scale provided")

func GetDimensionFromScale(scale api.Scale) (Dimension, error) {
	dimension, found := scaleToImgDimension[scale]
	if !found {
		return 0, errors.Newf("No scale-dimension mapping for scale: %s", scale)
	}
	return dimension, nil

}

func BuildAssetType(tier api.Tier, background api.Background, animationType api.AnimationType, scale api.Scale) string {
	assetType := fmt.Sprintf("%s-%s-%s-%s", tier, background, animationType, scale)
	return assetType
}

func getCloudfrontURLFromKey(key S3Key) string {
	return fmt.Sprintf("%s/%s", CloudfrontPrefix, key)
}

func GetCloudfrontURLFromImg(img dynamo.Image) string {
	return getCloudfrontURLFromKey(S3Key(img.S3Key))
}

func getArchiveLocationPrefix(channelId string) string {
	hash := sha1.New()
	_, err := hash.Write([]byte(time.Now().Format(time.RFC850)))
	if err != nil {
		log.WithError(err).Error("Failed to hash")
	}
	fileName := hex.EncodeToString(hash.Sum(nil))
	return fmt.Sprintf("%s/%s/%s", addPartnerActionsPrefix(channelId), ArchiveLocationS3KeyPrefix, fileName)
}

func addPartnerActionsPrefix(key string) string {
	return fmt.Sprintf("%s/%s", PartnerActionsS3KeyPrefix, key)
}

func (m *ImageManager) addPartnerActionsPrefixAndBucket(key string) string {
	return fmt.Sprintf("%s/%s", m.Config.BitsAssetsS3BucketName, addPartnerActionsPrefix(key))
}

func (m *ImageManager) GetCloudfrontURLFromKey(key S3Key) string {
	return getCloudfrontURLFromKey(key)
}

func (m *ImageManager) ArchiveImagesForChannel(ctx context.Context, channelId string, imageKeys []string) error {
	archivePrefix := getArchiveLocationPrefix(channelId)

	eg, egCtx := errgroup.WithContext(ctx)
	for _, imageKey := range imageKeys {
		imageKey := imageKey
		eg.Go(func() error {
			fullArchiveLocation := fmt.Sprintf("%s/%s", archivePrefix, imageKey)
			fullSourceLocation := m.addPartnerActionsPrefixAndBucket(imageKey)
			log.Infof("Archiving Channel Cheermote from %s to %s", fullSourceLocation, fullArchiveLocation)
			err := m.S3Client.CopyFile(egCtx, m.Config.BitsAssetsS3BucketName, fullSourceLocation, fullArchiveLocation)
			// If the key is missing from S3, that is ok. The first time someone uploads cheermotes (or when we cut over to the new path)
			// there will be nothing in the existing location
			if err != nil && !s3.IsKeyMissing(err) {
				msg := "Failed to archive files"
				log.WithError(err).WithFields(log.Fields{
					"keyLocation":     m.addPartnerActionsPrefixAndBucket(imageKey),
					"archiveLocation": fullArchiveLocation,
				}).Error(msg)
				return err
			}
			return nil
		})
	}

	// Wait for all goroutines to finish
	return eg.Wait()
}

func (m *ImageManager) DoesChannelHaveS3Images(channelId string) (bool, error) {
	channelPrefix := fmt.Sprintf("%s/%s", PartnerActionsS3KeyPrefix, channelId)
	fileNames, err := m.S3Client.ListFileNames(BitsAssetsS3Bucket, channelPrefix)
	if err != nil {
		msg := "Failed to list file names for prefix"
		log.WithError(err).WithField("channelPrefix", channelPrefix).Error(msg)
		return false, errors.Notef(err, msg)
	}
	return len(fileNames) > 0, nil
}

func (m *ImageManager) DeleteChannelS3Images(ctx context.Context, channelId string) error {
	channelPrefix := fmt.Sprintf("%s/%s", PartnerActionsS3KeyPrefix, channelId)

	var fileNames []string
	var continuationKey *string
	var err error
	for {
		select {
		case <-ctx.Done():
			msg := fmt.Sprintf("Timed out while deleting S3 files for channel: %s", channelId)
			log.Error(msg)
			return errors.New(msg)
		default:
		}

		fileNames, continuationKey, err = m.S3Client.PaginatedListFileNames(BitsAssetsS3Bucket, channelPrefix, continuationKey)
		if err != nil {
			msg := "Failed to list file names for prefix"
			log.WithError(err).WithField("prefix", channelPrefix).Error(msg)
			return errors.Notef(err, msg)
		}

		// No files to delete
		if len(fileNames) == 0 {
			log.WithFields(log.Fields{
				"channelPrefix": channelPrefix,
			}).Info("channel has no images needing deletion")
			return nil
		}

		err = m.S3Client.DeleteFiles(BitsAssetsS3Bucket, fileNames...)
		if err != nil {
			msg := "Failed to delete files"
			log.WithError(err).WithFields(log.Fields{
				"fileNum":       len(fileNames),
				"channelPrefix": channelPrefix,
			}).Error(msg)
			return err
		}

		if bits_strings.BlankP(continuationKey) {
			break
		}
	}
	return nil
}

// Deletes all images of the specified cheermote tier for a channel's image set
// Returns a boolean indicating that some images were deleted
func (m *ImageManager) DeleteChannelImageSetTierImages(ctx context.Context, channelID string, setID string, tier api.Tier) (bool, error) {
	logFields := log.Fields{
		"setID": setID,
		"tier":  tier,
	}

	// Get the all images in the set ID
	images, err := m.GetImages(ctx, setID)
	if err != nil {
		msg := "Error getting images for imageSetId for tier deletion"
		log.WithError(err).WithFields(logFields).Error(msg)
		return false, errors.Notef(err, msg)
	}

	// Get the image records and S3 keys of all the images in the tier to be deleted
	var imagesToDelete []*dynamo.Image
	var s3KeysToDelete []string
	for imagesTier, imagesByBackground := range images {
		if imagesTier == tier {
			for _, imagesByAnimationType := range imagesByBackground {
				for _, imagesByScale := range imagesByAnimationType {
					for _, img := range imagesByScale {
						if img != nil {
							imagesToDelete = append(imagesToDelete, img)
							s3KeysToDelete = append(s3KeysToDelete, img.S3Key)
						}
					}
				}
			}
		}
	}

	if len(imagesToDelete) < 1 {
		return false, nil
	}

	// Delete image records from DynamoDB
	err = m.ImageDao.BatchDelete(imagesToDelete)
	if err != nil {
		msg := "Error deleting tier images from DynamoDB"
		log.WithError(err).WithFields(logFields).Error(msg)
		return false, errors.Notef(err, msg)
	}

	// Delete image set from Cache
	err = m.ImagesCache.Delete(ctx, setID)
	if err != nil {
		msg := "Error deleting tier images from Cache"
		log.WithError(err).WithFields(logFields).Error(msg)
		return false, errors.Notef(err, msg)
	}

	// Archive S3 images before deletion. This is needed for moderation.
	// The archive function expects the S3 keys to be provided without the "partner-actions" prefix, so we have to trim it from the keys first.
	var s3KeysWithoutPartnerActionsPrefix []string
	for _, s3Key := range s3KeysToDelete {
		s3KeysWithoutPartnerActionsPrefix = append(s3KeysWithoutPartnerActionsPrefix, strings.TrimPrefix(s3Key, PartnerActionsS3KeyPrefix+"/"))
	}
	err = m.ArchiveImagesForChannel(ctx, channelID, s3KeysWithoutPartnerActionsPrefix)
	if err != nil {
		msg := "Error archiving tier images in S3"
		log.WithError(err).WithFields(logFields).Error(msg)
		return false, errors.Notef(err, msg)
	}

	// Delete assets from S3
	// No need to invalidate them from Cloudfront as the next image upload will do that (or if 24 hours passes, whichever comes first)
	err = m.S3Client.DeleteFiles(m.Config.BitsAssetsS3BucketName, s3KeysToDelete...)
	if err != nil {
		msg := "Error deleting tier images from S3"
		log.WithError(err).WithFields(logFields).Error(msg)
		return false, errors.Notef(err, msg)
	}

	return true, nil
}

func (m *ImageManager) PublishImageModerationJob(channelId string) error {
	jobSNS := &ImageModerationSNS{
		ChannelId:      channelId,
		ModerationType: NUCLEAR_MODERATION_TYPE,
	}
	snsJSON, err := json.Marshal(jobSNS)
	if err != nil {
		msg := "Failed to marshal imageModeration msg into json to send to SNS"
		log.WithError(err).Error(msg)
		return errors.Notef(err, msg)
	}

	err = m.SNSClient.PostToTopic(m.Config.ImageModerationSNSTopic, string(snsJSON))
	if err != nil {
		msg := "Failed to post image moderation message to topic for channelId"
		log.WithError(err).WithFields(log.Fields{
			"snsTopic":  m.Config.ImageModerationSNSTopic,
			"channelId": channelId,
		}).Error(msg)
		return errors.Notef(err, msg)
	}
	return nil
}

func (m *ImageManager) TrackChannelEnabled(ctx context.Context, channel dynamo.Channel) error {
	if bits_strings.BlankP(channel.CustomCheermoteImageSetId) {
		return errors.New("Cannot send datascience for channel. ImageSetId not set")
	}

	prefix, hasCustomCheermoteAction, err := m.PrefixManager.GetCustomCheermoteAction(context.Background(), string(channel.Id))
	if !hasCustomCheermoteAction || err == emote.TooManyPrefixesError {
		prefix = ""
	} else if err != nil {
		return errors.Notef(err, "Cannot send datascience for channel. Error geeting prefix")
	}

	data := models.BitsCheermoteEnabled{
		ImageSetId:      *channel.CustomCheermoteImageSetId,
		CheermoteString: prefix,
		ChannelId:       string(channel.Id),
	}

	return m.Datascience.TrackBitsEvent(ctx, datascience.BitsPartnerCheermoteEnabled, &data)

}

type UploadImageInput struct {
	SetId               string
	Tier                api.Tier
	Background          api.Background
	AnimationType       api.AnimationType
	Scale               api.Scale
	Image               []byte
	StartSmartUploadJob bool
	JobId               *string
	ChannelId           string
	CreationMethod      string
}

func (m *ImageManager) saveImageToDynamoDB(setId string, tier api.Tier, background api.Background, animationType api.AnimationType, scale api.Scale, s3Key string, creationMethod string) (*dynamo.Image, error) {
	assetType := BuildAssetType(tier, background, animationType, scale)
	assetIdUUID, err := uuid.NewV4()
	if err != nil {
		msg := "Failed to generate UUID"
		log.WithError(err).Error(msg)
		return nil, errors.Notef(err, msg)
	}
	newImg := &dynamo.Image{
		SetId:          setId,
		AssetType:      assetType,
		AssetId:        assetIdUUID.String(),
		S3Key:          s3Key,
		Tier:           string(tier),
		Background:     string(background),
		AnimationType:  string(animationType),
		Scale:          string(scale),
		CreationMethod: creationMethod,
	}

	err = m.ImageDao.Update(newImg)
	if err != nil {
		msg := "Failed to save image to dynamo"
		log.WithError(err).Error(msg)
		return nil, errors.Notef(err, msg)
	}

	return newImg, nil
}

// DEPRECATED: This step in the flow will be superseded by ProcessMakoImages
func (m *ImageManager) ProcessUploadService(ctx context.Context, uploadSNSMsg upload_service.SNSCallback) error {
	outputNum := len(uploadSNSMsg.Outputs)
	var creationMethod string
	setId := ""
	channelId := ""
	isSmartUpload := outputNum > 1

	if outputNum != 1 && outputNum != 5 && outputNum != 10 {
		return errors.Notef(nil, "%d is an invalid number of outputs.", outputNum)
	}

	for _, outputInfo := range uploadSNSMsg.Outputs {
		log.Infof("CustomCheermoteUpload OutputInfo: %+v", outputInfo)
		s3Key := outputInfo.Path[len(bitsAssetS3IdentifierPrefix):]
		creationMethod = CreationMethodGenerated

		imageData := strings.Split(outputInfo.Name, "/")
		if len(imageData) < 6 {
			return errors.Notef(nil, "%s is an invalid uploaded image URL.", outputInfo.Name)
		}

		channelId = imageData[0]
		setId = imageData[1]
		tier := api.Tier(imageData[2])
		background := api.Background(imageData[3])
		animationType := api.AnimationType(imageData[4])

		fileName := imageData[5]
		lastPeriod := strings.LastIndex(fileName, ".")
		if lastPeriod == -1 {
			return errors.Notef(nil, "%s is an invalid uploaded image URL, as it was missing a period in the filename", outputInfo.Name)
		}
		scale := api.Scale(fileName[0:lastPeriod])

		if !isSmartUpload ||
			(background == constants.LightBackground &&
				scale == constants.Scale4) {
			creationMethod = CreationMethodSource
		}

		newImg, err := m.saveImageToDynamoDB(setId, tier, background, animationType, scale, s3Key, creationMethod)
		if err != nil {
			return err
		}

		if creationMethod == CreationMethodSource {
			go m.CustomCheermoteImageSlacker.PostToSlack(imageData[0], newImg)
		}

		// For smart upload, only the light version assets are created
		// We need to create the dark version assets in the right location before we potentially swap traffic over (Dynamo is the source of truth)
		if isSmartUpload {
			darkBackgroundS3Key := strings.ReplaceAll(s3Key, string(constants.LightBackground), string(constants.DarkBackground))
			if s3Key == darkBackgroundS3Key {
				return errors.Newf("light asset and dark asset had same key: %s", s3Key)
			}

			// S3 requires the bucket prefix when copying keys, but only for the Source key. The "to" location does not need this
			s3KeyWithBucketPrefix := bitsAssetBucketPrefix + s3Key

			log.Infof("Smart upload detected. Copying light asset to dark asset location: from %s to %s", s3KeyWithBucketPrefix, darkBackgroundS3Key)
			// S3 requires the bucket name, as well as the bucket prefix included on each key when copying files
			err = m.S3Client.CopyFile(ctx, BitsAssetsS3Bucket, s3KeyWithBucketPrefix, darkBackgroundS3Key)
			if err != nil {
				return errors.Notef(err, "failed to copy light asset to dark asset. from: %s, to: %s", s3KeyWithBucketPrefix, darkBackgroundS3Key)
			}

			// One we have copied the file over, then update the dynamo record
			_, err = m.saveImageToDynamoDB(setId, tier, constants.DarkBackground, animationType, scale, darkBackgroundS3Key, CreationMethodGenerated)
			if err != nil {
				return err
			}
		}
	}

	invalidationPath := buildImageSetInvalidationPath(channelId, setId)

	// If this call fails, the new image won't show up for 24 hours. This is a problem, so we need to return an error
	// instead of just logging like the ImagesCache below
	// Since there's only one wildcard invalidation path, we can reuse the path as the base caller reference for idempotency
	err := m.Cloudfront.InvalidateCache(ctx, []string{invalidationPath}, invalidationPath)
	if err != nil {
		return errors.Notef(err, "failed to invalidate cloudfront cache for channelid %s, imageSetId: %s", channelId, setId)
	}

	err = m.ImagesCache.Delete(ctx, setId)
	if err != nil {
		log.WithError(err).WithField("setID", setId).Error("Error deleting images from Cache after processing upload service message")
	}

	return nil
}

func (m *ImageManager) getMakoEmoteImageUploadS3Key(uploadID string) string {
	return fmt.Sprintf("emoticons/%s", uploadID)
}

func (m *ImageManager) getMakoEmoteImageUploadS3KeyWithBucketPrefix(uploadID string) string {
	return fmt.Sprintf("%s/emoticons/%s", m.Config.MakoEmoteUploadBucket, uploadID)
}

func cheermoteDestinationS3Key(channelID, setID string, tier api.Tier, background api.Background, animationType api.AnimationType, scale api.Scale) string {
	imageKey := fmt.Sprintf(uploadservice.ImageIDFormat, channelID, setID, tier, background, animationType, scale, uploadservice.GetFileExtension(animationType))
	return addPartnerActionsPrefix(imageKey)
}

func (m *ImageManager) copyMakoImageToCheermoteDestination(ctx context.Context, channelID, setID, imageID string, tier api.Tier, background api.Background, animationType api.AnimationType, scale api.Scale) error {
	makoS3Key := m.getMakoEmoteImageUploadS3KeyWithBucketPrefix(imageID)
	cheermoteDestinationS3Key := cheermoteDestinationS3Key(channelID, setID, tier, background, animationType, scale)

	err := m.S3Client.CopyFile(ctx, m.Config.BitsAssetsS3BucketName, makoS3Key, cheermoteDestinationS3Key)
	if err != nil {
		return errors.Notef(err, "failed to copy %s scaled %s %s asset. from: %s, to: %s", scale, animationType, background, makoS3Key, cheermoteDestinationS3Key)
	}

	newImg, err := m.saveImageToDynamoDB(setID, tier, background, animationType, scale, cheermoteDestinationS3Key, CreationMethodMako)
	if err != nil {
		return err
	}

	// Only post one of each type of the images to slack per upload
	if background == constants.LightBackground && scale == constants.Scale4 {
		go m.CustomCheermoteImageSlacker.PostToSlack(channelID, newImg)
	}

	return nil
}

// validateUploadedCheermoteImageAsset will verify that the image asset uploaded into Mako's image upload bucket meets the requirements for the size and asset type which it is
// being assigned to prior to copying the asset
func (m *ImageManager) validateUploadedCheermoteImageAsset(ctx context.Context, imageID string, animationType api.AnimationType, scale api.Scale) error {
	// Required validations for cheermotes assets
	body, err := m.S3Client.LoadFile(m.Config.MakoEmoteUploadBucket, m.getMakoEmoteImageUploadS3Key(imageID))
	if err != nil {
		if aerr, ok := err.(awserr.Error); ok {
			switch aerr.Code() {
			case s3sdk.ErrCodeNoSuchKey:
				return ErrImageAssetsNotFound
			default:
				// Unexpected AWS-SDK Error
				return err
			}
		}
		// Unexpected non AWS-SDK error
		return err
	}

	if len(body) == 0 {
		return ErrInvalidImageAssets
	}

	// If the image should be static, ensure that the file is not an APNG
	if animationType == constants.StaticAnimationType {
		err = ValidateStaticPNG(body)
		if err != nil {
			return ErrInvalidImageAssets
		}
	}

	return nil
}

// validateUploadedCheermoteImageAssets will check each of the individual scales provided to ensure that they all meet the requirements for a cheermote asset of that scale in parallel.
func (m *ImageManager) validateUploadedCheermoteImageAssets(egCtx context.Context, eg *errgroup.Group, animationType api.AnimationType, imageAsset paydayrpc.EmoteImageAsset) {
	eg.Go(func() error {
		return m.validateUploadedCheermoteImageAsset(egCtx, imageAsset.Image1XId, animationType, constants.Scale1)
	})

	eg.Go(func() error {
		return m.validateUploadedCheermoteImageAsset(egCtx, imageAsset.Image2XId, animationType, constants.Scale2)
	})

	eg.Go(func() error {
		return m.validateUploadedCheermoteImageAsset(egCtx, imageAsset.Image4XId, animationType, constants.Scale4)
	})
}

// copyAllScalesFromMakoToCheermoteDestination takes an image asset which is either static or animated. In parallel it will initiate S3 copy operations and dynamo writes for all of the variations of those assets.
func (m *ImageManager) copyAllScalesFromMakoToCheermoteDestination(egCtx context.Context, eg *errgroup.Group, channelID, setID string, tier api.Tier, imageAsset paydayrpc.EmoteImageAsset, animationType api.AnimationType) {
	// Scale 1.5 and Scale 3 are not used anywhere. Until we can go through and remove all references in Payday to these scales we still need to have some image there.
	// We are currently using the mechanism which is consistant with how Emotes handle the extra scales which is using the closest smaller scaled image in these slots.
	// (Scale 1 is inserted as 1.5 and Scale 2 is inserted as Scale 3)

	// Light background assets
	eg.Go(func() error {
		return m.copyMakoImageToCheermoteDestination(egCtx, channelID, setID, imageAsset.Image1XId, tier, constants.LightBackground, animationType, constants.Scale1)
	})
	eg.Go(func() error {
		return m.copyMakoImageToCheermoteDestination(egCtx, channelID, setID, imageAsset.Image1XId, tier, constants.LightBackground, animationType, constants.Scale15)
	})
	eg.Go(func() error {
		return m.copyMakoImageToCheermoteDestination(egCtx, channelID, setID, imageAsset.Image2XId, tier, constants.LightBackground, animationType, constants.Scale2)
	})
	eg.Go(func() error {
		return m.copyMakoImageToCheermoteDestination(egCtx, channelID, setID, imageAsset.Image2XId, tier, constants.LightBackground, animationType, constants.Scale3)
	})
	eg.Go(func() error {
		return m.copyMakoImageToCheermoteDestination(egCtx, channelID, setID, imageAsset.Image4XId, tier, constants.LightBackground, animationType, constants.Scale4)
	})

	// Dark background assets
	eg.Go(func() error {
		return m.copyMakoImageToCheermoteDestination(egCtx, channelID, setID, imageAsset.Image1XId, tier, constants.DarkBackground, animationType, constants.Scale1)
	})
	eg.Go(func() error {
		return m.copyMakoImageToCheermoteDestination(egCtx, channelID, setID, imageAsset.Image1XId, tier, constants.DarkBackground, animationType, constants.Scale15)
	})
	eg.Go(func() error {
		return m.copyMakoImageToCheermoteDestination(egCtx, channelID, setID, imageAsset.Image2XId, tier, constants.DarkBackground, animationType, constants.Scale2)
	})
	eg.Go(func() error {
		return m.copyMakoImageToCheermoteDestination(egCtx, channelID, setID, imageAsset.Image2XId, tier, constants.DarkBackground, animationType, constants.Scale3)
	})
	eg.Go(func() error {
		return m.copyMakoImageToCheermoteDestination(egCtx, channelID, setID, imageAsset.Image4XId, tier, constants.DarkBackground, animationType, constants.Scale4)
	})
}

// ProcessMakoImages is used to copy cheermotes assets that were uploaded via the Mako Animated Emote Uploader flow into Payday's Cheermote S3 bucket and update the cheermote dynamo
func (m *ImageManager) ProcessMakoImages(ctx context.Context, channelID, setID string, tier api.Tier, staticAsset, animatedAsset paydayrpc.EmoteImageAsset) error {
	egValidate, egValidateCtx := errgroup.WithContext(ctx)

	// Validate static assets
	m.validateUploadedCheermoteImageAssets(egValidateCtx, egValidate, constants.StaticAnimationType, staticAsset)

	// Validate animated assets
	m.validateUploadedCheermoteImageAssets(egValidateCtx, egValidate, constants.AnimatedAnimationType, animatedAsset)

	if err := egValidate.Wait(); err != nil {
		return err
	}

	// Copy all image assets and write dynamo records in Parallel
	egCopy, egCopyCtx := errgroup.WithContext(ctx)

	// Copy Static Assets
	m.copyAllScalesFromMakoToCheermoteDestination(egCopyCtx, egCopy, channelID, setID, tier, staticAsset, constants.StaticAnimationType)

	// Copy Animated Assets
	m.copyAllScalesFromMakoToCheermoteDestination(egCopyCtx, egCopy, channelID, setID, tier, animatedAsset, constants.AnimatedAnimationType)

	if err := egCopy.Wait(); err != nil {
		return err
	}

	// Build all the paths that need invalidation in Cloudfront
	var pathsToInvalidate []string
	for background := range constants.Backgrounds {
		for animationType := range constants.AnimationTypes {
			for scale := range constants.Scales {
				pathsToInvalidate = append(pathsToInvalidate, buildSingleFileInvalidationPath(channelID, setID, tier, background, animationType, scale))
			}
		}
	}
	// Used for invalidation idempotency
	baseCallerReference := buildChannelTierInvalidationBaseCallerReference(channelID, setID, tier)

	// If this call fails, the new images won't show up for 24 hours. This is a problem, so we need to return an error
	// instead of just logging like the ImagesCache below
	err := m.Cloudfront.InvalidateCache(ctx, pathsToInvalidate, baseCallerReference)
	if err != nil {
		return errors.Notef(err, "failed to invalidate cloudfront cache for channelID: %s, imageSetId: %s, tier: %s", channelID, setID, tier)
	}

	err = m.ImagesCache.Delete(ctx, setID)
	if err != nil {
		log.WithError(err).WithField("setID", setID).Error("Error deleting images from Cache after copying new images from Mako")
	}

	return nil
}

func (m *ImageManager) GetImages(ctx context.Context, setId string) (ImageMap, error) {
	var images []*dynamo.Image
	var cachedImagesPresent bool
	var err error

	images, cachedImagesPresent = m.ImagesCache.Get(ctx, setId)
	if !cachedImagesPresent {
		images, err = m.ImageDao.GetAllBySetId(setId)
		if err != nil {
			msg := "Failed to get images by setId from dynamo"
			log.WithError(err).WithField("setId", setId).Error(msg)
			return nil, errors.Notef(err, msg)
		}

		m.ImagesCache.Set(ctx, setId, images)
	}

	result := make(ImageMap)
	for _, img := range images {
		result.PutImage(img)
	}
	return result, nil
}

func (m *ImageManager) WriteImagesZip(ctx context.Context, setId string, w io.Writer) error {
	images, err := m.GetImages(ctx, setId)
	if err != nil {
		msg := "Error getting images for imageSetId"
		log.WithError(err).WithField("setId", setId).Error(msg)
		return errors.Notef(err, msg)
	}

	type s3File struct {
		key      string
		contents []byte
		err      error
	}

	fileChan := make(chan s3File)
	totalImages := 0
	for _, imagesByBackground := range images {
		for _, imagesByAnimationType := range imagesByBackground {
			for _, imagesByScale := range imagesByAnimationType {
				for _, img := range imagesByScale {
					go func(s3Key string) {
						contents, err := m.S3Client.LoadFile(BitsAssetsS3Bucket, s3Key)
						file := s3File{
							key:      s3Key,
							contents: contents,
							err:      err,
						}
						fileChan <- file
					}(img.S3Key)
					totalImages++
				}
			}
		}
	}

	zipWriter := zip.NewWriter(w)
	defer util.Close(zipWriter)
	for i := 0; i < totalImages; i++ {
		var file s3File
		select {
		case <-ctx.Done():
			msg := "Timed out waiting for s3 responses"
			log.WithField("setId", setId).Error(msg)
			return errors.New(msg)
		case file = <-fileChan:
		}

		if file.err != nil {
			msg := "Error loading file from S3"
			log.WithError(file.err).WithField("fileName", file.key).Error(msg)
			return errors.Notef(file.err, msg)
		}

		fileWriter, err := zipWriter.Create(file.key)
		if err != nil {
			msg := "Error creating a new file within the zip"
			log.WithError(err).WithField("fileName", file.key).Error(msg)
			return errors.Notef(err, msg)
		}

		_, err = fileWriter.Write(file.contents)
		if err != nil {
			msg := "Error writing S3 file contents to file within the zip"
			log.WithError(err).WithField("fileName", file.key).Error(msg)
			return errors.Notef(err, msg)
		}
	}

	return nil
}

func buildImageSetInvalidationPath(channelId, imageSetId string) string {
	// Cloudfront requires a leading slash for directories, which doesn't match S3 syntax
	return fmt.Sprintf("/%s/%s/%s/*", PartnerActionsS3KeyPrefix, channelId, imageSetId)
}

func buildSingleFileInvalidationPath(channelID, setID string, tier api.Tier, background api.Background, animationType api.AnimationType, scale api.Scale) string {
	// Cloudfront requires a leading slash for directories, which doesn't match S3 syntax
	return fmt.Sprintf("/%s", cheermoteDestinationS3Key(channelID, setID, tier, background, animationType, scale))
}

func buildChannelTierInvalidationBaseCallerReference(channelID, setID string, tier api.Tier) string {
	return fmt.Sprintf("/%s/%s/%s/%s", PartnerActionsS3KeyPrefix, channelID, setID, tier)
}

func IsValidUploadType(uploadType api.UploadType) bool {
	_, ok := UploadTypes[uploadType]
	return ok
}

func IsValidBackground(background api.Background) bool {
	_, ok := constants.Backgrounds[background]
	return ok
}

func IsValidAnimationType(animationType api.AnimationType) bool {
	_, ok := constants.AnimationTypes[animationType]
	return ok
}

func IsValidTier(tier api.Tier) bool {
	_, ok := constants.Tiers[tier]
	return ok
}

func IsValidScale(scale api.Scale) bool {
	_, ok := constants.Scales[scale]
	return ok
}
