package image

import (
	"context"
	"errors"
	"fmt"
	"strings"
	"testing"
	"time"

	"code.justin.tv/commerce/payday/actions/constants"
	"code.justin.tv/commerce/payday/config"
	cloudfront_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/cloudfront"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"

	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/service/s3"

	"code.justin.tv/commerce/payday/dynamo"
	cache_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/cache"
	dynamo_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo"
	image_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/image"
	s3_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/s3"
	"code.justin.tv/web/upload-service/models"
	"code.justin.tv/web/upload-service/rpc/uploader"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

const (
	testChannelId         = "test_channel_id"
	testSetId             = "test_set_id"
	tier1                 = "1"
	lightBackgroundType   = "light"
	darkBackgroundType    = "dark"
	animatedAnimationType = "animated"
	animatedExtension     = "gif"
	staticAnimationType   = "static"
	staticExtension       = "png"
	scale1                = "1"
	scale15               = "1.5"
	scale2                = "2"
	scale3                = "3"
	scale4                = "4"
	testS3Bucket          = "bits-assets"
	testImageKey1         = "image_key_1"
	testImageKey2         = "image_key_2"
	testImageKey1Path     = "bits-assets/partner-actions/image_key_1"
	testImageKey2Path     = "bits-assets/partner-actions/image_key_2"
	imageArchivePrefix    = "partner-actions/test_channel_id/archive/"

	testErrorCode = "test_error_code"

	urlFormatString = "%s/%s/%s/%s/%s/%s.%s"
)

// The non-smart upload flow can upload any type of image
func buildValidOutputInfo() uploader.OutputInfo {
	name := fmt.Sprintf(urlFormatString, testChannelId, testSetId, tier1, darkBackgroundType, animatedAnimationType, scale15, animatedExtension)
	return uploader.OutputInfo{
		Name: name,
		Path: "s3://" + BitsAssetsS3Bucket + "/" + PartnerActionsS3KeyPrefix + "/" + name,
	}
}

// The smart upload flow will always upload light background images. If there are 5, it means that the static images are being generated
func buildValidStaticSmartOutputInfoSlice() []uploader.OutputInfo {
	var outputs []uploader.OutputInfo
	scales := []string{scale1, scale15, scale2, scale3, scale4}

	for i := 0; i < 5; i++ {
		name := fmt.Sprintf(urlFormatString, testChannelId, testSetId, tier1, lightBackgroundType, staticAnimationType, scales[i], staticExtension)
		output := uploader.OutputInfo{
			Name: name,
			Path: "s3://" + BitsAssetsS3Bucket + "/" + PartnerActionsS3KeyPrefix + "/" + name,
		}
		outputs = append(outputs, output)
	}
	return outputs
}

// The smart upload flow will always upload light background images. If there are 5, it means that the static and animated images are being generated
func buildValidAnimatedSmartOutputInfoSlice() []uploader.OutputInfo {
	var outputs []uploader.OutputInfo
	scales := []string{scale1, scale15, scale2, scale3, scale4}

	for i := 0; i < 5; i++ {
		name := fmt.Sprintf(urlFormatString, testChannelId, testSetId, tier1, lightBackgroundType, staticAnimationType, scales[i], staticExtension)
		output := uploader.OutputInfo{
			Name: name,
			Path: "s3://" + BitsAssetsS3Bucket + "/" + PartnerActionsS3KeyPrefix + "/" + name,
		}
		outputs = append(outputs, output)
	}

	for i := 0; i < 5; i++ {
		name := fmt.Sprintf(urlFormatString, testChannelId, testSetId, tier1, lightBackgroundType, animatedAnimationType, scales[i], animatedExtension)
		output := uploader.OutputInfo{
			Name: name,
			Path: "s3://" + BitsAssetsS3Bucket + "/" + PartnerActionsS3KeyPrefix + "/" + name,
		}
		outputs = append(outputs, output)
	}
	return outputs
}

type prefixMatcher struct {
	prefix string
}

// Convey allows you to write a custom argument matching function
func (p *prefixMatcher) Match(str string) bool {
	return strings.HasPrefix(str, p.prefix)
}

func TestImageManager(t *testing.T) {
	Convey("Given an Image Manager", t, func() {
		imagesCache := new(cache_mock.IImagesCache)
		imageDao := new(dynamo_mock.IImageDao)
		s3Mock := new(s3_mock.IS3Client)
		cloudFrontMock := new(cloudfront_mock.ICloudFrontClient)
		customCheermoteImageSlacker := new(image_mock.ICustomCheermoteImageSlacker)
		configMock := config.Configuration{
			BitsAssetsS3BucketName: testS3Bucket,
		}

		imageManager := ImageManager{
			ImagesCache:                 imagesCache,
			ImageDao:                    imageDao,
			CustomCheermoteImageSlacker: customCheermoteImageSlacker,
			S3Client:                    s3Mock,
			Cloudfront:                  cloudFrontMock,
			Config:                      &configMock,
		}

		Convey("and ArchiveImagesForChannel is called", func() {
			Convey("with valid context input", func() {
				p := prefixMatcher{prefix: imageArchivePrefix}
				Convey("and a single key", func() {
					Convey("and unknown error code should return an error", func() {
						ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
						defer cancel()

						s3Mock.On("CopyFile", mock.Anything, configMock.BitsAssetsS3BucketName, testImageKey1Path, mock.MatchedBy(p.Match)).Return(awserr.New(testErrorCode, "", errors.New("")))

						res := imageManager.ArchiveImagesForChannel(ctx, testChannelId, []string{testImageKey1})
						So(res, ShouldBeError)
						So(res.Error(), ShouldStartWith, testErrorCode)
					})
					Convey("and missing file error code should not return an error", func() {
						ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
						defer cancel()

						s3Mock.On("CopyFile", mock.Anything, configMock.BitsAssetsS3BucketName, testImageKey1Path, mock.MatchedBy(p.Match)).Return(awserr.New(s3.ErrCodeNoSuchKey, "", errors.New("")))

						res := imageManager.ArchiveImagesForChannel(ctx, testChannelId, []string{testImageKey1})
						So(res, ShouldBeNil)
					})
					Convey("and no error code should not return an error", func() {
						ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
						defer cancel()

						s3Mock.On("CopyFile", mock.Anything, configMock.BitsAssetsS3BucketName, testImageKey1Path, mock.MatchedBy(p.Match)).Return(nil)

						res := imageManager.ArchiveImagesForChannel(ctx, testChannelId, []string{testImageKey1})
						So(res, ShouldBeNil)
					})
				})
				Convey("and a multiple keys", func() {
					Convey("and all successes should not return an error", func() {
						ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
						defer cancel()

						s3Mock.On("CopyFile", mock.Anything, configMock.BitsAssetsS3BucketName, testImageKey1Path, mock.MatchedBy(p.Match)).Return(nil)
						s3Mock.On("CopyFile", mock.Anything, configMock.BitsAssetsS3BucketName, testImageKey2Path, mock.MatchedBy(p.Match)).Return(nil)

						res := imageManager.ArchiveImagesForChannel(ctx, testChannelId, []string{testImageKey1, testImageKey2})
						So(res, ShouldBeNil)
					})
					Convey("and unhandled error partway through should return an error", func() {
						ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
						defer cancel()

						s3Mock.On("CopyFile", mock.Anything, configMock.BitsAssetsS3BucketName, testImageKey1Path, mock.MatchedBy(p.Match)).Return(nil)
						s3Mock.On("CopyFile", mock.Anything, configMock.BitsAssetsS3BucketName, testImageKey2Path, mock.MatchedBy(p.Match)).Return(awserr.New(testErrorCode, "", errors.New("")))

						res := imageManager.ArchiveImagesForChannel(ctx, testChannelId, []string{testImageKey1, testImageKey2})
						So(res, ShouldBeError)
						So(res.Error(), ShouldStartWith, testErrorCode)
					})
				})
			})
		})

		Convey("and DeleteChannelImageSetTierImages is called", func() {
			channelID := "1234"
			imageSetID := "abcd"
			tier := constants.Tier1
			s3Key1 := "path/to/image1"
			s3Key2 := "path/to/image2"
			images := []*dynamo.Image{
				{
					Tier:          string(constants.Tier1),
					Background:    string(constants.DarkBackground),
					AnimationType: string(constants.AnimatedAnimationType),
					Scale:         string(constants.Scale1),
					S3Key:         s3Key1,
				},
				{
					Tier:          string(constants.Tier100),
					Background:    string(constants.DarkBackground),
					AnimationType: string(constants.AnimatedAnimationType),
					Scale:         string(constants.Scale1),
					S3Key:         s3Key2,
				},
			}

			Convey("and getting images errors, we should error", func() {
				imagesCache.On("Get", mock.Anything, imageSetID).Return(nil, false)
				imageDao.On("GetAllBySetId", imageSetID).Return(nil, errors.New("test error"))

				imagesDeleted, err := imageManager.DeleteChannelImageSetTierImages(context.Background(), channelID, imageSetID, tier)

				So(err, ShouldNotBeNil)
				So(imagesDeleted, ShouldBeFalse)
				imageDao.AssertNumberOfCalls(t, "BatchDelete", 0)
			})

			Convey("and no images are to be deleted, we should return", func() {
				imagesCache.On("Get", mock.Anything, imageSetID).Return(images[1:], true)

				imagesDeleted, err := imageManager.DeleteChannelImageSetTierImages(context.Background(), channelID, imageSetID, tier)

				So(err, ShouldBeNil)
				So(imagesDeleted, ShouldBeFalse)
			})

			Convey("and batch deleting images from dynamo errors, we should error", func() {
				imagesCache.On("Get", mock.Anything, imageSetID).Return(images, true)
				imageDao.On("BatchDelete", mock.Anything).Return(errors.New("test error"))

				imagesDeleted, err := imageManager.DeleteChannelImageSetTierImages(context.Background(), channelID, imageSetID, tier)

				So(err, ShouldNotBeNil)
				So(imagesDeleted, ShouldBeFalse)
			})

			Convey("and deleting the image set from the cache errors, we should error", func() {
				imagesCache.On("Get", mock.Anything, imageSetID).Return(images, true)
				imageDao.On("BatchDelete", mock.Anything).Return(nil)
				imagesCache.On("Delete", mock.Anything, imageSetID).Return(errors.New("test error"))

				imagesDeleted, err := imageManager.DeleteChannelImageSetTierImages(context.Background(), channelID, imageSetID, tier)

				So(err, ShouldNotBeNil)
				So(imagesDeleted, ShouldBeFalse)
			})

			Convey("and archiving images in S3 errors, we should error", func() {
				imagesCache.On("Get", mock.Anything, imageSetID).Return(images, true)
				imageDao.On("BatchDelete", images[:1]).Return(nil)
				imagesCache.On("Delete", mock.Anything, imageSetID).Return(nil)
				s3Mock.On("CopyFile", mock.Anything, configMock.BitsAssetsS3BucketName, mock.Anything, mock.Anything).Return(errors.New("test error"))

				imagesDeleted, err := imageManager.DeleteChannelImageSetTierImages(context.Background(), channelID, imageSetID, tier)

				So(err, ShouldNotBeNil)
				So(imagesDeleted, ShouldBeFalse)
			})

			Convey("and deleting images from S3 errors, we should error", func() {
				imagesCache.On("Get", mock.Anything, imageSetID).Return(images, true)
				imageDao.On("BatchDelete", images[:1]).Return(nil)
				imagesCache.On("Delete", mock.Anything, imageSetID).Return(nil)
				s3Mock.On("CopyFile", mock.Anything, configMock.BitsAssetsS3BucketName, mock.Anything, mock.Anything).Return(nil)
				s3Mock.On("DeleteFiles", configMock.BitsAssetsS3BucketName, s3Key1).Return(errors.New("test error"))

				imagesDeleted, err := imageManager.DeleteChannelImageSetTierImages(context.Background(), channelID, imageSetID, tier)

				So(err, ShouldNotBeNil)
				So(imagesDeleted, ShouldBeFalse)
			})

			Convey("and everything succeeds, we should not error", func() {
				imagesCache.On("Get", mock.Anything, imageSetID).Return(images, true)
				imageDao.On("BatchDelete", images[:1]).Return(nil)
				imagesCache.On("Delete", mock.Anything, imageSetID).Return(nil)
				s3Mock.On("CopyFile", mock.Anything, configMock.BitsAssetsS3BucketName, mock.Anything, mock.Anything).Return(nil)
				s3Mock.On("DeleteFiles", configMock.BitsAssetsS3BucketName, s3Key1).Return(nil)

				imagesDeleted, err := imageManager.DeleteChannelImageSetTierImages(context.Background(), channelID, imageSetID, tier)

				So(err, ShouldBeNil)
				So(imagesDeleted, ShouldBeTrue)
			})
		})

		Convey("and ProcessMakoImages is called", func() {
			testStaticAsset := paydayrpc.EmoteImageAsset{
				Image1XId: "testStaticImage1x",
				Image2XId: "testStaticImage2x",
				Image4XId: "testStaticImage4x",
				AssetType: paydayrpc.EmoteImageAssetType_STATIC,
			}
			testAnimatedAsset := paydayrpc.EmoteImageAsset{
				Image1XId: "testAnimatedImage1x",
				Image2XId: "testAnimatedImage2x",
				Image4XId: "testAnimatedImage4x",
				AssetType: paydayrpc.EmoteImageAssetType_ANIMATED,
			}

			Convey("Fails when assets are missing", func() {
				s3Mock.On("LoadFile", mock.Anything, mock.Anything, mock.Anything).Return(nil, awserr.New(s3.ErrCodeNoSuchKey, "", errors.New("")))
				err := imageManager.ProcessMakoImages(context.Background(), testChannelId, testSetId, constants.Tier1, testStaticAsset, testAnimatedAsset)

				So(err, ShouldNotBeNil)
				So(err, ShouldEqual, ErrImageAssetsNotFound)
			})

			Convey("Fails when assets are invalid for some other reason", func() {
				s3Mock.On("LoadFile", mock.Anything, mock.Anything, mock.Anything).Return(nil, awserr.New(testErrorCode, "", errors.New("")))
				err := imageManager.ProcessMakoImages(context.Background(), testChannelId, testSetId, constants.Tier1, testStaticAsset, testAnimatedAsset)

				So(err, ShouldNotBeNil)
				// We treat this as a generic 500 so no need to check for a specific error here
			})

			Convey("Fails when image file is blank", func() {
				s3Mock.On("LoadFile", mock.Anything, mock.Anything, mock.Anything).Return([]byte{}, nil)
				err := imageManager.ProcessMakoImages(context.Background(), testChannelId, testSetId, constants.Tier1, testStaticAsset, testAnimatedAsset)

				So(err, ShouldNotBeNil)
				So(err, ShouldEqual, ErrInvalidImageAssets)
			})

			Convey("When assets are valid", func() {
				s3Mock.On("LoadFile", mock.Anything, mock.Anything, mock.Anything).Return([]byte("surprisingly_this_string_looks_like_a_valid_image"), nil)

				Convey("When the copy operation fails", func() {
					s3Mock.On("CopyFile", mock.Anything, configMock.BitsAssetsS3BucketName, mock.Anything, mock.Anything).Return(errors.New("test error"))
					imageDao.On("Update", mock.Anything).Return(nil)

					err := imageManager.ProcessMakoImages(context.Background(), testChannelId, testSetId, constants.Tier1, testStaticAsset, testAnimatedAsset)

					So(err, ShouldNotBeNil)
					// We treat this as a generic 500 so no need to check for a specific error here
				})

				Convey("When the dynamo write fails", func() {
					s3Mock.On("CopyFile", mock.Anything, configMock.BitsAssetsS3BucketName, mock.Anything, mock.Anything).Return(nil)
					imageDao.On("Update", mock.Anything).Return(errors.New("test error"))

					err := imageManager.ProcessMakoImages(context.Background(), testChannelId, testSetId, constants.Tier1, testStaticAsset, testAnimatedAsset)

					So(err, ShouldNotBeNil)
					// We treat this as a generic 500 so no need to check for a specific error here
				})

				Convey("When the file operations succeed", func() {
					s3Mock.On("CopyFile", mock.Anything, configMock.BitsAssetsS3BucketName, mock.Anything, mock.Anything).Return(nil)
					imageDao.On("Update", mock.Anything).Return(nil)

					Convey("but cloudfront invalidation fails", func() {
						customCheermoteImageSlacker.On("PostToSlack", mock.Anything, mock.Anything).Return(nil)
						cloudFrontMock.On("InvalidateCache", mock.Anything, mock.Anything, mock.Anything).Return(errors.New("test error"))
						imagesCache.On("Delete", mock.Anything, mock.Anything).Return(nil)

						err := imageManager.ProcessMakoImages(context.Background(), testChannelId, testSetId, constants.Tier1, testStaticAsset, testAnimatedAsset)

						// Cloudfront cache invalidation failures should fail the operation
						So(err, ShouldNotBeNil)
						// We treat this as a generic 500 so no need to check for a specific error here
					})

					Convey("but publish to slack fails", func() {
						customCheermoteImageSlacker.On("PostToSlack", mock.Anything, mock.Anything).Return(errors.New("test error"))
						cloudFrontMock.On("InvalidateCache", mock.Anything, mock.Anything, mock.Anything).Return(nil)
						imagesCache.On("Delete", mock.Anything, mock.Anything).Return(nil)

						err := imageManager.ProcessMakoImages(context.Background(), testChannelId, testSetId, constants.Tier1, testStaticAsset, testAnimatedAsset)

						// A slack publish error should not block the entire operation
						So(err, ShouldBeNil)
					})

					Convey("but redis cache invalidation fails", func() {
						customCheermoteImageSlacker.On("PostToSlack", mock.Anything, mock.Anything).Return(nil)
						cloudFrontMock.On("InvalidateCache", mock.Anything, mock.Anything, mock.Anything).Return(nil)
						imagesCache.On("Delete", mock.Anything, mock.Anything).Return(errors.New("test error"))

						err := imageManager.ProcessMakoImages(context.Background(), testChannelId, testSetId, constants.Tier1, testStaticAsset, testAnimatedAsset)

						// A redis cache invalidation error should not block the entire operation
						So(err, ShouldBeNil)
					})

					Convey("validate the expected number of copy operations", func() {
						customCheermoteImageSlacker.On("PostToSlack", mock.Anything, mock.Anything).Return(nil)
						cloudFrontMock.On("InvalidateCache", mock.Anything, mock.Anything, mock.Anything).Return(nil)
						imagesCache.On("Delete", mock.Anything, mock.Anything).Return(nil)

						err := imageManager.ProcessMakoImages(context.Background(), testChannelId, testSetId, constants.Tier1, testStaticAsset, testAnimatedAsset)

						So(err, ShouldBeNil)
						imageDao.AssertNumberOfCalls(t, "Update", 20)
						s3Mock.AssertNumberOfCalls(t, "CopyFile", 20)
					})
				})
			})
		})

		Convey("Fails when given an invalid number of outputs", func() {
			output1 := uploader.OutputInfo{
				Name: "test_name_1",
			}
			output2 := uploader.OutputInfo{
				Name: "test_name_2",
			}
			output3 := uploader.OutputInfo{
				Name: "test_name_3",
			}
			outputs := []uploader.OutputInfo{output1, output2, output3}
			uploadSNSMessage := models.SNSCallback{
				Outputs: outputs,
			}

			err := imageManager.ProcessUploadService(context.Background(), uploadSNSMessage)

			So(err, ShouldNotBeNil)
			So(err.Error(), ShouldContainSubstring, "is an invalid number of outputs")
		})

		Convey("Given the correct number of outputs", func() {

			Convey("Fails when output name is invalid", func() {
				output := uploader.OutputInfo{
					Name: "invalid_output_name",
					Path: "s3://" + BitsAssetsS3Bucket + "/",
				}
				outputs := []uploader.OutputInfo{output}
				uploadSNSMessage := models.SNSCallback{
					Outputs: outputs,
				}

				err := imageManager.ProcessUploadService(context.Background(), uploadSNSMessage)

				So(err, ShouldNotBeNil)
				So(err.Error(), ShouldContainSubstring, "is an invalid uploaded image URL.")
			})

			Convey("Fails when call to update dynamo fails", func() {
				imageDao.On("Update", mock.Anything).Return(errors.New("test error"))
				outputs := []uploader.OutputInfo{buildValidOutputInfo()}
				uploadSNSMessage := models.SNSCallback{
					Outputs: outputs,
				}

				err := imageManager.ProcessUploadService(context.Background(), uploadSNSMessage)

				So(err, ShouldNotBeNil)
				So(err.Error(), ShouldContainSubstring, "Failed to save image to dynamo")
			})
			Convey("Given calls to update dynamo, post to slack, but cloudfront fails to invalidate", func() {
				imageDao.On("Update", mock.Anything).Return(nil)
				customCheermoteImageSlacker.On("PostToSlack", mock.Anything, mock.Anything).Return(nil)
				s3Mock.On("CopyFile", mock.Anything, BitsAssetsS3Bucket, mock.Anything, mock.Anything).Return(nil)
				cloudFrontMock.On("InvalidateCache", mock.Anything, mock.Anything, mock.Anything).Return(errors.New("test error"))

				Convey("Fails and calls Dynamo once with given output info for a valid non-smart upload", func() {
					outputs := []uploader.OutputInfo{buildValidOutputInfo()}
					uploadSNSMessage := models.SNSCallback{
						Outputs: outputs,
					}

					err := imageManager.ProcessUploadService(context.Background(), uploadSNSMessage)

					So(err, ShouldBeError)
				})
				Convey("Fails but calls Dynamo 10 times for a valid static smart upload", func() {
					uploadSNSMessage := models.SNSCallback{
						Outputs: buildValidStaticSmartOutputInfoSlice(),
					}

					err := imageManager.ProcessUploadService(context.Background(), uploadSNSMessage)

					So(err, ShouldBeError)
					imageDao.AssertNumberOfCalls(t, "Update", 10)
				})

				Convey("Fails but calls Dynamo 20 times for a valid animated smart upload", func() {
					uploadSNSMessage := models.SNSCallback{
						Outputs: buildValidAnimatedSmartOutputInfoSlice(),
					}

					err := imageManager.ProcessUploadService(context.Background(), uploadSNSMessage)

					So(err, ShouldBeError)
					imageDao.AssertNumberOfCalls(t, "Update", 20)
				})
			})

			Convey("Given calls to update dynamo, post to slack, cloudfront cache invalidate, and delete from redis cache succeed", func() {
				imageDao.On("Update", mock.Anything).Return(nil)
				customCheermoteImageSlacker.On("PostToSlack", mock.Anything, mock.Anything).Return(nil)
				s3Mock.On("CopyFile", mock.Anything, BitsAssetsS3Bucket, mock.Anything, mock.Anything).Return(nil)
				imagesCache.On("Delete", mock.Anything, mock.Anything).Return(nil)

				Convey("Succeeds and calls Dynamo once with given output info for a valid non-smart upload", func() {
					cloudFrontMock.On("InvalidateCache", mock.Anything, mock.Anything, mock.Anything).Return(nil)

					outputs := []uploader.OutputInfo{buildValidOutputInfo()}
					uploadSNSMessage := models.SNSCallback{
						Outputs: outputs,
					}

					err := imageManager.ProcessUploadService(context.Background(), uploadSNSMessage)

					So(err, ShouldBeNil)
					imageDao.AssertNumberOfCalls(t, "Update", 1)

					// Always invalidates the CloudFront cache only once
					cloudFrontMock.AssertNumberOfCalls(t, "InvalidateCache", 1)
					dynamoImage := imageDao.Calls[0].Arguments.Get(0).(*dynamo.Image)
					So(dynamoImage.SetId, ShouldEqual, testSetId)
					So(dynamoImage.Tier, ShouldEqual, tier1)
					So(dynamoImage.Background, ShouldEqual, darkBackgroundType)
					So(dynamoImage.AnimationType, ShouldEqual, animatedAnimationType)
					So(dynamoImage.Scale, ShouldEqual, scale15)
					So(dynamoImage.CreationMethod, ShouldEqual, CreationMethodSource)
				})

				Convey("Succeeds and calls Dynamo 10 times for a valid static smart upload", func() {
					cloudFrontMock.On("InvalidateCache", mock.Anything, mock.Anything, mock.Anything).Return(nil)
					uploadSNSMessage := models.SNSCallback{
						Outputs: buildValidStaticSmartOutputInfoSlice(),
					}

					err := imageManager.ProcessUploadService(context.Background(), uploadSNSMessage)

					So(err, ShouldBeNil)
					imageDao.AssertNumberOfCalls(t, "Update", 10)
					// Always invalidates the CloudFront cache only once
					cloudFrontMock.AssertNumberOfCalls(t, "InvalidateCache", 1)
				})

				Convey("Succeeds and calls Dynamo 20 times for a valid animated smart upload", func() {
					cloudFrontMock.On("InvalidateCache", mock.Anything, mock.Anything, mock.Anything).Return(nil)
					uploadSNSMessage := models.SNSCallback{
						Outputs: buildValidAnimatedSmartOutputInfoSlice(),
					}

					err := imageManager.ProcessUploadService(context.Background(), uploadSNSMessage)

					So(err, ShouldBeNil)
					imageDao.AssertNumberOfCalls(t, "Update", 20)
					// Always invalidates the CloudFront cache only once
					cloudFrontMock.AssertNumberOfCalls(t, "InvalidateCache", 1)
				})
			})
		})
	})
}
