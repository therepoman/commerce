package image

import (
	"bytes"
	"encoding/base64"
	"image/png"
	"io/ioutil"
	"strings"

	"code.justin.tv/commerce/payday/errors"
)

const (
	maxFileSize                 = 1024 * 500 // 500KB
	pngImageDataStartMarker     = "IDAT"
	pngAnimationDataStartMarker = "acTL"
)

var InvalidEncodingErr = errors.New("Invalid base64 encoding")
var FileTooLargeErr = errors.New("File is too large")
var AnimatedPNGErr = errors.New("Animated PNGs are not supported")
var InvalidPNGErr = errors.New("File is not a PNG")

func ValidateStaticBase64PNG(base64EncodedPNG string) error {
	reader := strings.NewReader(base64EncodedPNG)
	decoder := base64.NewDecoder(base64.StdEncoding, reader)

	pngBytes, err := ioutil.ReadAll(decoder)
	if err != nil {
		return InvalidEncodingErr
	}

	if len(pngBytes) > maxFileSize {
		return FileTooLargeErr
	}

	// Validate that image can be decoded as a png
	_, err = png.DecodeConfig(bytes.NewReader(pngBytes))
	if err != nil {
		return InvalidPNGErr
	}

	return ValidateStaticPNG(pngBytes)
}

func ValidateStaticPNG(pngBytes []byte) error {
	// To be recognized as an APNG, an `acTL` chunk must appear in the stream before any `IDAT` chunks.
	// From: https://wiki.mozilla.org/APNG_Specification
	pngStr := string(pngBytes)
	animIdx := strings.Index(pngStr, pngAnimationDataStartMarker)
	imgIdx := strings.Index(pngStr, pngImageDataStartMarker)
	if animIdx != -1 && imgIdx != -1 && animIdx < imgIdx {
		return AnimatedPNGErr
	}

	return nil
}
