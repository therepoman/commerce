package image

import (
	"bytes"
	"encoding/base64"
	"io/ioutil"
	"testing"

	"github.com/stretchr/testify/assert"
)

func readFileAsBase64String(filePath string) (string, error) {
	fileBytes, err := ioutil.ReadFile(filePath)
	if err != nil {
		return "", err
	}

	bb := &bytes.Buffer{}
	encoder := base64.NewEncoder(base64.StdEncoding, bb)
	_, _ = encoder.Write(fileBytes)
	_ = encoder.Close()
	return bb.String(), nil
}

func Test_ValidateStaticBase64PNG_Empty(t *testing.T) {
	err := ValidateStaticBase64PNG("")
	assert.Error(t, err)
}

func Test_ValidateStaticBase64PNG_InvalidEncoding(t *testing.T) {
	err := ValidateStaticBase64PNG("Garbage")
	assert.Equal(t, InvalidEncodingErr, err)

	err = ValidateStaticBase64PNG("R2FyYmFnZQ==") // "Garbage" base64 encoded
	assert.Equal(t, InvalidPNGErr, err)
}

func Test_ValidateStaticBase64PNG_ValidEncoding(t *testing.T) {
	err := ValidateStaticBase64PNG("iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mNk+P+/HgAFhAJ/wlseKgAAAABJRU5ErkJggg==") // 1x1 pixel png
	assert.NoError(t, err)
}

func Test_ValidateStaticBase64PNG_TooBig(t *testing.T) {
	file, err := readFileAsBase64String("testdata/tooBig.png")
	assert.NoError(t, err)
	err = ValidateStaticBase64PNG(file)
	assert.Equal(t, FileTooLargeErr, err)
}

func Test_ValidateStaticBase64PNG_Animated(t *testing.T) {
	file, err := readFileAsBase64String("testdata/apng.png")
	assert.NoError(t, err)
	err = ValidateStaticBase64PNG(file)
	assert.Equal(t, AnimatedPNGErr, err)
}

func Test_ValidateStaticBase64PNG_Regular(t *testing.T) {
	file, err := readFileAsBase64String("testdata/regular.png")
	assert.NoError(t, err)
	err = ValidateStaticBase64PNG(file)
	assert.NoError(t, err)
}
