package image

import (
	"context"
	"fmt"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/clients/slack"
	"code.justin.tv/commerce/payday/clients/user"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/emote"
	userUtils "code.justin.tv/commerce/payday/user"
	"code.justin.tv/commerce/payday/utils/strings"
)

type ICustomCheermoteImageSlacker interface {
	PostToSlack(channelId string, img *dynamo.Image)
}

type CustomCheermoteImageSlacker struct {
	UserService   user.IUserServiceClientWrapper `inject:""`
	PrefixManager emote.IPrefixManager           `inject:""`
	Slacker       slack.ISlacker                 `inject:""`
	Config        *config.Configuration          `inject:""`
}

func (c *CustomCheermoteImageSlacker) PostToSlack(channelId string, img *dynamo.Image) {
	if userUtils.IsATestAccount(channelId) {
		return
	}

	if c.Config == nil || strings.Blank(c.Config.CustomCheermoteChannelSlackWebhookURL) {
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	channel := channelId
	user, err := c.UserService.GetUserByID(ctx, channelId, nil)
	if err == nil && user != nil && user.Login != nil {
		channel = *user.Login
	}

	customAction, _, err := c.PrefixManager.GetCustomCheermoteAction(ctx, channelId)
	if err != nil {
		customAction = "Tier "
	}
	customAction += img.Tier

	classicCheer := fmt.Sprintf(":cheer%s:", img.Tier)
	text := fmt.Sprintf("%s %s uploaded in %s's channel %s", classicCheer, customAction, channel, classicCheer)
	msg := slack.SlackMsg{
		Attachments: []slack.SlackAttachment{
			{
				Pretext:  text,
				Fallback: text,
				ImageURL: GetCloudfrontURLFromImg(*img),
			},
		},
	}
	err = c.Slacker.Post(c.Config.CustomCheermoteChannelSlackWebhookURL, msg)
	if err != nil {
		log.WithError(err).Warn("Failed to post custom cheermotes to slack")
	}
}
