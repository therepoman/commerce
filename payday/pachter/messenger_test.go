package pachter

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/payday/config"
	sns_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sns"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestHandler_SendTransactionToPachter(t *testing.T) {
	ctx := context.Background()

	Convey("Given Pachter handler", t, func() {
		mockCfg := config.Configuration{
			PachterSNSTopic: "leOhki-pachter-sns",
		}
		mockSNS := new(sns_mock.ISNSClient)
		handler := messenger{
			Config:    &mockCfg,
			SNSClient: mockSNS,
		}

		Convey("when transaction is nil", func() {
			err := handler.SendTransactionToPachter(ctx, nil)

			So(err, ShouldNotBeNil)
			mockSNS.AssertNumberOfCalls(t, "PostToTopicWithContext", 0)
		})

		Convey("when SNS fails", func() {
			josephError := errors.New("not joseph enough error")
			mockSNS.On("PostToTopicWithContext", ctx, mockCfg.PachterSNSTopic, mock.Anything).Return(josephError)

			err := handler.SendTransactionToPachter(ctx, &paydayrpc.Transaction{})
			So(err, ShouldEqual, josephError)
		})

		Convey("when SNS works", func() {
			mockSNS.On("PostToTopicWithContext", ctx, mockCfg.PachterSNSTopic, mock.Anything).Return(nil)

			err := handler.SendTransactionToPachter(ctx, &paydayrpc.Transaction{})
			So(err, ShouldBeNil)

			mockSNS.AssertCalled(t, "PostToTopicWithContext", ctx, mockCfg.PachterSNSTopic, mock.Anything)
		})
	})
}
