package pachter

import (
	"context"
	"encoding/json"
	"errors"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/config"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"code.justin.tv/commerce/payday/sns"
)

type Messenger interface {
	SendTransactionToPachter(ctx context.Context, tx *paydayrpc.Transaction) error
}

type messenger struct {
	Config    *config.Configuration `inject:""`
	SNSClient sns.ISNSClient        `inject:"usWestSNSClient"`
}

func NewPachterMessenger() Messenger {
	return &messenger{}
}

func (m *messenger) SendTransactionToPachter(ctx context.Context, tx *paydayrpc.Transaction) error {
	if tx == nil {
		return errors.New("transaction cannot be nil")
	}

	logger := logrus.WithFields(logrus.Fields{
		"transaction": tx,
		"SNS topic":   m.Config.PachterSNSTopic,
	})
	txJson, err := json.Marshal(tx)
	if err != nil {
		logger.WithError(err).Error("unable to marshal Dynamo transaction")
		return err
	}

	err = m.SNSClient.PostToTopicWithContext(ctx, m.Config.PachterSNSTopic, string(txJson))
	if err != nil {
		logger.WithError(err).Error("failed to publish transaction to Pachter")
		return err
	}

	return nil
}
