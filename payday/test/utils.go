package test

import (
	"fmt"
	"io/ioutil"
	"math/rand"
	"time"

	"code.justin.tv/common/golibs/pubsubclient"
	"code.justin.tv/common/twitchhttp"
	"code.justin.tv/web/users-service/models"
	irc "github.com/thoj/go-ircevent"
	"golang.org/x/net/context"
	"golang.org/x/net/websocket"
)

const (
	testUserIdPrefixString = "7357"
	testUserIdSuffixDigits = 14
)

type IUserServiceClient interface {
	GetUserByID(ctx context.Context, userID string, reqOpts *twitchhttp.ReqOpts) (*models.Properties, error)
	GetUserByLogin(ctx context.Context, login string, reqOpts *twitchhttp.ReqOpts) (*models.Properties, error)
	GetUsers(ctx context.Context, userIDs []string, logins []string, emails []string, displaynames []string, ips []string, reqOpts *twitchhttp.ReqOpts) (*models.PropertiesResult, error)
	GetUserByEmail(ctx context.Context, email string, reqOpts *twitchhttp.ReqOpts) (*models.Properties, error)
	GetBannedUsers(ctx context.Context, until time.Time, reqOpts *twitchhttp.ReqOpts) (*models.PropertiesResult, error)
	BanUserByID(ctx context.Context, userID string, reportingID string, banType string, isWarning bool, banReason string, reqOpts *twitchhttp.ReqOpts) error
	UnbanUserByID(ctx context.Context, userID string, reqOpts *twitchhttp.ReqOpts) error
	AddDMCAStrike(ctx context.Context, userID string, reqOpts *twitchhttp.ReqOpts) error
	RemoveDMCAStrike(ctx context.Context, userID string, reqOpts *twitchhttp.ReqOpts) error
	SetUser(ctx context.Context, userID string, uup *models.UpdateableProperties, reqOpts *twitchhttp.ReqOpts) error
}

func HelloWebsocket() {
	//Needed for godeps
	fmt.Printf("Hello world %d %s %v", websocket.PingFrame, irc.VERSION, pubsubclient.ErrClosed)
}

var numericRunes = []rune("0123456789")
var alphanumericRunes = []rune("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

func RandomInt(min int, max int) int {
	return rand.Intn(max-min+1) + min
}

func RandomInt64(min int64, max int64) int64 {
	return rand.Int63n(max-min+1) + min
}

func nilByChance(nilChance float32) bool {
	return rand.Float32() <= nilChance
}

func RandomInt64OrNil(nilChance float32, min int64, max int64) *int64 {
	if nilByChance(nilChance) {
		return nil
	}
	val := RandomInt64(min, max)
	return &val
}

func RandomBool() bool {
	return RandomInt(0, 1) == 1
}

func RandomBoolOrNil(nilChance float32) *bool {
	if nilByChance(nilChance) {
		return nil
	}
	val := RandomBool()
	return &val
}

func RandomAlphanumericRune() rune {
	return alphanumericRunes[rand.Intn(len(alphanumericRunes))]
}

func RandomNumericRune() rune {
	return numericRunes[rand.Intn(len(numericRunes))]
}

func RandomTestUserId() string {
	return testUserIdPrefixString + RandomNumberString(testUserIdSuffixDigits)
}

func RandomNumberString(size int) string {
	chars := make([]rune, size)
	for i := 0; i < size; i++ {
		chars[i] = RandomNumericRune()
	}
	return string(chars)
}

func RandomString(size int) string {
	chars := make([]rune, size)
	for i := 0; i < size; i++ {
		chars[i] = RandomAlphanumericRune()
	}
	return string(chars)
}

func RandomStringOrNil(nilChance float32, size int) *string {
	if nilByChance(nilChance) {
		return nil
	}
	val := RandomString(size)
	return &val
}

func RandomDuration(max time.Duration) time.Duration {
	maxNs := max.Nanoseconds()
	randNs := RandomInt64(0, maxNs)
	return time.Duration(randNs)
}

func RandomTime(maxSub time.Duration) time.Time {
	randDur := RandomDuration(maxSub)
	return time.Now().Add(-randDur)
}

func Seed() {
	rand.Seed(time.Now().UTC().UnixNano())
}

func ReadFile(path string) []byte {
	raw, err := ioutil.ReadFile(path)
	if err != nil {
		panic(err)
	}
	return raw
}
