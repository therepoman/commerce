package test

import (
	"os/exec"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/dynamo/leaderboard_badge_holders"
	"code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/campaigns"
	"code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/channel_statuses"
	"code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/eligible_channels"
	"code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/user_campaign_statuses"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/hystrix"
	"github.com/aws/aws-sdk-go/aws/credentials"
)

// Set this to true when running tests with an IDE (ex: Running tests in IntelliJ)
// Runs "make test_setup" before the test (starting a local dynamodb)
// Runs "make test_cleanup" after the test (stopping the local dynamo)
const ideTesting = false

const skipDynamoTests = true

// Run tests against real dynamodb in the real aws account
const useRealDynamo = false

const awsRegion = "us-east-1"
const localDynamoPort = "9001"
const localDynamoEndpoint = "http://localhost:" + localDynamoPort
const readCapacity = dynamo.ReadCapacity(10)
const writeCapacity = dynamo.WriteCapacity(10)
const acceptableUpdateTimeDeltaLocal = "15s"
const acceptableUpdateTimeDeltaReal = "30s"
const defaultWaitTimeout = "20s"

type DynamoTestContext struct {
	AcceptableUpdateTimeDelta time.Duration
	Suffix                    string
	TestCredentials           *credentials.Credentials
	TestEndpoint              string
	Daos                      []dynamo.Dao
	WaitTimeout               time.Duration
	SkipDynamoTests           bool
}

func NewDynamoTestContext(packageUnderTest string) (*DynamoTestContext, error) {
	Seed()
	hystrix.ConfigureForTests()

	log.Infof("Creating dynamo test context")

	dynamoContext := &DynamoTestContext{
		Suffix:          packageUnderTest + "-" + RandomString(16),
		SkipDynamoTests: skipDynamoTests,
	}

	var err error
	dynamoContext.WaitTimeout, err = time.ParseDuration(defaultWaitTimeout)
	if err != nil {
		return nil, err
	}

	if ideTesting {
		// run "make test_setup" to start local dynamo
		cmd := exec.Command("/usr/bin/make", "-C", "..", "test_setup") // #nosec test code
		err := cmd.Run()
		if err != nil {
			return nil, errors.Notef(err, "Failed to run make test_setup")
		}
	}

	if useRealDynamo {
		log.Infof("Running test against real dynamo in AWS")
	} else {
		// Setup dummy credentials and use and endpoint corresponding to the locally running dynamodb
		dynamoContext.TestCredentials = credentials.NewStaticCredentials("testId", "testSecret", "testToken")
		dynamoContext.TestEndpoint = localDynamoEndpoint
	}

	if useRealDynamo {
		dynamoContext.AcceptableUpdateTimeDelta, err = time.ParseDuration(acceptableUpdateTimeDeltaReal)
	} else {
		dynamoContext.AcceptableUpdateTimeDelta, err = time.ParseDuration(acceptableUpdateTimeDeltaLocal)
	}
	if err != nil {
		return nil, err
	}

	return dynamoContext, nil
}

func (c *DynamoTestContext) Cleanup() error {
	deleteFailed := false

	// Delete all tables created for testing
	for _, dao := range c.Daos {
		// First, set the wait timeouts back to normal to make sure they have enough time to get ready for deletion
		clientConfig := dao.GetClientConfig()
		clientConfig.TableExistenceWaitTimeoutMsOverride = c.WaitTimeout
		clientConfig.TableStatusWaitTimeoutMsOverride = c.WaitTimeout

		err := dao.DeleteTable()
		if err != nil {
			deleteFailed = true
			// Continue tyring to delete the remaining tables
		}
	}

	if ideTesting {
		// Run the "make test_cleanup" command which stops local dynamo
		cmd := exec.Command("/usr/bin/make", "-C", "..", "test_cleanup") // #nosec test code
		err := cmd.Run()
		if err != nil {
			return errors.New("Failed running make test_cleanup")
		}
	}

	if deleteFailed {
		return errors.New("Failed to delete all test tables during cleanup")
	}
	return nil
}

func (c *DynamoTestContext) CreateTestClientConfig(existenceWaitTimeout, statusWaitTimeout time.Duration, prefix string) *dynamo.DynamoClientConfig {
	return &dynamo.DynamoClientConfig{
		AwsRegion:                           awsRegion,
		NewTableReadCapacity:                readCapacity,
		NewTableWriteCapacity:               writeCapacity,
		TablePrefix:                         prefix,
		TableSuffix:                         c.Suffix,
		OverwriteExistingTablesDuringSetup:  true,
		EndpointOverride:                    c.TestEndpoint,
		CredentialsOverride:                 c.TestCredentials,
		TableExistenceWaitTimeoutMsOverride: existenceWaitTimeout,
		TableStatusWaitTimeoutMsOverride:    statusWaitTimeout,
	}
}

func (c *DynamoTestContext) CreateTestClient(existenceWaitTimeout, statusWaitTimeout time.Duration, prefix string) *dynamo.DynamoClient {
	clientConfig := c.CreateTestClientConfig(existenceWaitTimeout, statusWaitTimeout, prefix)
	return dynamo.NewClient(clientConfig)
}

func (c *DynamoTestContext) CreateUserDao(existenceWaitTimeout, statusWaitTimeout time.Duration, prefix string) (dynamo.IUserDao, *dynamo.DynamoClient) {
	client := c.CreateTestClient(existenceWaitTimeout, statusWaitTimeout, prefix)
	dao := dynamo.NewUserDao(client)

	// Keep track of the dao for post-test cleanup
	c.Daos = append(c.Daos, dao)
	return dao, client
}

func (c *DynamoTestContext) CreateDefaultUserDao(prefix string) (dynamo.IUserDao, *dynamo.DynamoClient) {
	return c.CreateUserDao(c.WaitTimeout, c.WaitTimeout, prefix)
}

func (c *DynamoTestContext) CreateChannelDao(existenceWaitTimeout, statusWaitTimeout time.Duration, prefix string) (dynamo.IChannelDao, *dynamo.DynamoClient) {
	client := c.CreateTestClient(existenceWaitTimeout, statusWaitTimeout, prefix)
	dao := dynamo.NewChannelDao(client)

	// Keep track of the dao for post-test cleanup
	c.Daos = append(c.Daos, dao)
	return dao, client
}

func (c *DynamoTestContext) CreateDefaultChannelDao(prefix string) (dynamo.IChannelDao, *dynamo.DynamoClient) {
	return c.CreateChannelDao(c.WaitTimeout, c.WaitTimeout, prefix)
}

func (c *DynamoTestContext) CreateBadgeTierDao(existenceWaitTimeout, statusWaitTimeout time.Duration, prefix string) (dynamo.IBadgeTierDao, *dynamo.DynamoClient) {
	client := c.CreateTestClient(existenceWaitTimeout, statusWaitTimeout, prefix)
	dao := dynamo.NewBadgeTierDao(client)

	// Keep track of the dao for post-test cleanup
	c.Daos = append(c.Daos, dao)
	return dao, client
}

func (c *DynamoTestContext) CreateDefaultBadgeTierDao(prefix string) (dynamo.IBadgeTierDao, *dynamo.DynamoClient) {
	return c.CreateBadgeTierDao(c.WaitTimeout, c.WaitTimeout, prefix)
}

func (c *DynamoTestContext) CreateSponsoredCheermoteCampaignsDao(existenceWaitTimeout, statusWaitTimeout time.Duration, prefix string) (campaigns.ISponsoredCheermoteCampaignDao, *dynamo.DynamoClient) {
	client := c.CreateTestClient(existenceWaitTimeout, statusWaitTimeout, prefix)
	dao := campaigns.NewSponsoredCheermoteCampaignDao(client)

	c.Daos = append(c.Daos, dao)
	return dao, client
}

func (c *DynamoTestContext) CreateDefaultSponsoredCheermoteCampaignsDao(prefix string) (campaigns.ISponsoredCheermoteCampaignDao, *dynamo.DynamoClient) {
	return c.CreateSponsoredCheermoteCampaignsDao(c.WaitTimeout, c.WaitTimeout, prefix)
}

func (c *DynamoTestContext) CreateSponsoredCheermoteChannelStatusesDao(existenceWaitTimeout, statusWaitTimeout time.Duration, prefix string) (channel_statuses.ISponsoredCheermoteChannelStatusesDao, *dynamo.DynamoClient) {
	client := c.CreateTestClient(existenceWaitTimeout, statusWaitTimeout, prefix)
	dao := channel_statuses.NewSponsoredCheermoteChannelStatusesDao(client)

	c.Daos = append(c.Daos, dao)
	return dao, client
}

func (c *DynamoTestContext) CreateDefaultSponsoredCheermoteChannelStatusesDao(prefix string) (channel_statuses.ISponsoredCheermoteChannelStatusesDao, *dynamo.DynamoClient) {
	return c.CreateSponsoredCheermoteChannelStatusesDao(c.WaitTimeout, c.WaitTimeout, prefix)
}

func (c *DynamoTestContext) CreateSponsoredCheermoteUserStatusesDao(existenceWaitTimeout, statusWaitTimeout time.Duration, prefix string) (user_campaign_statuses.ISponsoredCheermoteUserStatusesDao, *dynamo.DynamoClient) {
	client := c.CreateTestClient(existenceWaitTimeout, statusWaitTimeout, prefix)
	dao := user_campaign_statuses.NewSponsoredCheermoteUserStatusesDao(client)

	c.Daos = append(c.Daos, dao)
	return dao, client
}

func (c *DynamoTestContext) CreateDefaultSponsoredCheermoteUserStatusesDao(prefix string) (user_campaign_statuses.ISponsoredCheermoteUserStatusesDao, *dynamo.DynamoClient) {
	return c.CreateSponsoredCheermoteUserStatusesDao(c.WaitTimeout, c.WaitTimeout, prefix)
}

func (c *DynamoTestContext) CreateLeaderboardBadgeHoldersDao(existenceWaitTimeout, statusWaitTimeout time.Duration, prefix string) (leaderboard_badge_holders.ILeaderboardBadgeHoldersDao, *dynamo.DynamoClient) {
	client := c.CreateTestClient(existenceWaitTimeout, statusWaitTimeout, prefix)
	dao := leaderboard_badge_holders.NewLeaderboardBadgesDao(client)

	c.Daos = append(c.Daos, dao)
	return dao, client
}

func (c *DynamoTestContext) CreateSponsoredCheermoteEligibleChannelsDao(existenceWaitTimeout, statusWaitTimeout time.Duration, prefix string) (eligible_channels.ISponsoredCheermoteEligibleChannelsDao, *dynamo.DynamoClient) {
	client := c.CreateTestClient(existenceWaitTimeout, statusWaitTimeout, prefix)
	dao := eligible_channels.NewSponsoredCheermoteEligibleChannelsDao(client)

	c.Daos = append(c.Daos, dao)
	return dao, client
}

func (c *DynamoTestContext) CreateDefaultSponsoredCheermoteEligibleChannelsDao(prefix string) (eligible_channels.ISponsoredCheermoteEligibleChannelsDao, *dynamo.DynamoClient) {
	return c.CreateSponsoredCheermoteEligibleChannelsDao(c.WaitTimeout, c.WaitTimeout, prefix)
}

func (c *DynamoTestContext) CreateDefaultLeaderboardBadgeHoldersDao(prefix string) (leaderboard_badge_holders.ILeaderboardBadgeHoldersDao, *dynamo.DynamoClient) {
	return c.CreateLeaderboardBadgeHoldersDao(c.WaitTimeout, c.WaitTimeout, prefix)
}

func (c *DynamoTestContext) CreateTransactionsDao(existenceWaitTimeout, statusWaitTimeout time.Duration, prefix string) (dynamo.ITransactionsDao, *dynamo.DynamoClient) {
	client := c.CreateTestClient(existenceWaitTimeout, statusWaitTimeout, prefix)
	dao := dynamo.NewTransactionsDao(client)

	// Keep track of the dao for post-test cleanup
	c.Daos = append(c.Daos, dao)
	return dao, client
}

func (c *DynamoTestContext) CreateDefaultTransactionsDao(prefix string) (dynamo.ITransactionsDao, *dynamo.DynamoClient) {
	return c.CreateTransactionsDao(c.WaitTimeout, c.WaitTimeout, prefix)
}

func (c *DynamoTestContext) CreateTaxInfoDao(existenceWaitTimeout, statusWaitTimeout time.Duration, prefix string) (dynamo.ITaxInfoDao, *dynamo.DynamoClient) {
	client := c.CreateTestClient(existenceWaitTimeout, statusWaitTimeout, prefix)
	dao := dynamo.NewTaxInfoDao(client)

	// Keep track of the dao for post-test cleanup
	c.Daos = append(c.Daos, dao)
	return dao, client
}

func (c *DynamoTestContext) CreateDefaultTaxInfoDao(prefix string) (dynamo.ITaxInfoDao, *dynamo.DynamoClient) {
	return c.CreateTaxInfoDao(c.WaitTimeout, c.WaitTimeout, prefix)
}

func (c *DynamoTestContext) CreateImageDao(existenceWaitTimeout, statusWaitTimeout time.Duration, action string) (dynamo.IImageDao, *dynamo.DynamoClient) {
	client := c.CreateTestClient(existenceWaitTimeout, statusWaitTimeout, action)
	dao := dynamo.NewImageDao(client)

	// Keep track of the dao for post-test cleanup
	c.Daos = append(c.Daos, dao)
	return dao, client
}

func (c *DynamoTestContext) CreateDefaultImageDao(prefix string) (dynamo.IImageDao, *dynamo.DynamoClient) {
	return c.CreateImageDao(c.WaitTimeout, c.WaitTimeout, prefix)
}
