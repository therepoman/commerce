package post_process_message

import (
	"context"
	"errors"
	"fmt"
	"testing"

	post_process_message_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/prism_tenant/post_process_message"
	prism_models "code.justin.tv/commerce/prism/rpc/models"
	prism_tenant "code.justin.tv/commerce/prism/rpc/tenant"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestPostProcessMessage(t *testing.T) {
	Convey("Given a PostProcessMessage server", t, func() {
		channelID := "108706201"
		userID := "108706200"
		amount := 420

		bitsMessagePostProcessor := new(post_process_message_mock.PostProcessMessage)

		api := &ServerImpl{
			BitsMessagePostProcessor: bitsMessagePostProcessor,
		}

		req := &prism_tenant.PostProcessMessageReq{
			UserId:    userID,
			ChannelId: channelID,
			ConsumableAmounts: map[string]int64{
				prism_models.ConsumableType_Bits: int64(amount),
			},
			Message:     fmt.Sprintf("cheer%d coregamers", amount),
			MessageId:   "test-message",
			Anonymous:   false,
			SentMessage: fmt.Sprintf("cheer%d ***", amount),
		}

		Convey("When the request is nil", func() {
			req = nil

			Convey("Then an error is returned", func() {
				res, err := api.PostProcess(context.Background(), req)

				So(res, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("When the userID is empty", func() {
			req.UserId = ""

			Convey("Then an error is returned", func() {
				res, err := api.PostProcess(context.Background(), req)

				So(res, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("When the channelID is empty", func() {
			req.ChannelId = ""

			Convey("Then an error is returned", func() {
				res, err := api.PostProcess(context.Background(), req)

				So(res, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("When the messageID is empty", func() {
			req.MessageId = ""

			Convey("Then an error is returned", func() {
				res, err := api.PostProcess(context.Background(), req)

				So(res, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("When the message is empty", func() {
			req.Message = ""

			Convey("Then an error is returned", func() {
				res, err := api.PostProcess(context.Background(), req)

				So(res, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("When the bits amount is less than or equal to zero", func() {
			req.ConsumableAmounts[prism_models.ConsumableType_Bits] = 0

			Convey("Then an error is returned", func() {
				res, err := api.PostProcess(context.Background(), req)

				So(res, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("When the sent message is empty", func() {
			req.SentMessage = ""

			Convey("Then an error is returned", func() {
				res, err := api.PostProcess(context.Background(), req)

				So(res, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when the request passes all validation", func() {
			Convey("when the bits message fails to post process", func() {
				bitsMessagePostProcessor.On("PostProcess", mock.Anything, mock.Anything).Return(nil, errors.New("ERROR"))

				Convey("we should return the error", func() {
					res, err := api.PostProcess(context.Background(), req)

					So(res, ShouldBeNil)
					So(err, ShouldNotBeNil)
				})
			})

			Convey("when the bits message succeeds", func() {
				bitsMessagePostProcessor.On("PostProcess", mock.Anything, mock.Anything).Return(&prism_tenant.PostProcessMessageResp{}, nil)

				Convey("we should return the PostProcessMessageResp", func() {
					res, err := api.PostProcess(context.Background(), req)

					So(err, ShouldBeNil)
					So(res, ShouldNotBeNil)
				})
			})
		})
	})
}
