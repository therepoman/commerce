package post_process_message

import (
	"context"

	"code.justin.tv/commerce/payday/prism_tenant/post_process_message"
	prism_models "code.justin.tv/commerce/prism/rpc/models"
	prism_tenant "code.justin.tv/commerce/prism/rpc/tenant"
	"github.com/twitchtv/twirp"
)

type Server interface {
	PostProcess(ctx context.Context, req *prism_tenant.PostProcessMessageReq) (*prism_tenant.PostProcessMessageResp, error)
}

type ServerImpl struct {
	BitsMessagePostProcessor post_process_message.PostProcessMessage `inject:"bits-message-post-processor"`
}

func (s *ServerImpl) PostProcess(ctx context.Context, req *prism_tenant.PostProcessMessageReq) (*prism_tenant.PostProcessMessageResp, error) {
	if req == nil {
		return nil, twirp.InvalidArgumentError("Request", "cannot be nil")
	} else if req.UserId == "" {
		return nil, twirp.RequiredArgumentError("UserId")
	} else if req.ChannelId == "" {
		return nil, twirp.RequiredArgumentError("ChannelId")
	} else if req.MessageId == "" {
		return nil, twirp.RequiredArgumentError("MessageId")
	} else if req.Message == "" {
		return nil, twirp.RequiredArgumentError("Message")
	} else if req.ConsumableAmounts[prism_models.ConsumableType_Bits] <= 0 {
		return nil, twirp.InvalidArgumentError("ConsumableAmount", "must be greater than zero")
	} else if req.SentMessage == "" {
		return nil, twirp.RequiredArgumentError("SentMessage")
	}

	return s.BitsMessagePostProcessor.PostProcess(ctx, req)
}
