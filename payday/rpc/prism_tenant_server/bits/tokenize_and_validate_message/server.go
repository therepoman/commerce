package tokenize_and_validate_message

import (
	"context"

	"code.justin.tv/commerce/payday/prism_tenant/tokenize_and_validate_message"
	prism_tenant "code.justin.tv/commerce/prism/rpc/tenant"
	"github.com/twitchtv/twirp"
)

type Server interface {
	TokenizeAndValidate(ctx context.Context, req *prism_tenant.TokenizeAndValidateMessageReq) (*prism_tenant.TokenizeAndValidateMessageResp, error)
}

type ServerImpl struct {
	BitsTokenizeAndValidate tokenize_and_validate_message.TokenizeAndValidate `inject:"bits-tokenize-and-validate"`
}

func (s *ServerImpl) TokenizeAndValidate(ctx context.Context, req *prism_tenant.TokenizeAndValidateMessageReq) (*prism_tenant.TokenizeAndValidateMessageResp, error) {
	if req == nil {
		return nil, twirp.InvalidArgumentError("Request", "cannot be nil")
	} else if req.UserId == "" {
		return nil, twirp.RequiredArgumentError("UserId")
	} else if req.ChannelId == "" {
		return nil, twirp.RequiredArgumentError("ChannelId")
	} else if req.MessageId == "" {
		return nil, twirp.RequiredArgumentError("MessageId")
	} else if req.Message == "" {
		return nil, twirp.RequiredArgumentError("Message")
	}

	bitsTokenizeAndValidateResult, err := s.BitsTokenizeAndValidate.TokenizeAndValidate(ctx, req)

	if err != nil {
		return nil, err
	}

	return &bitsTokenizeAndValidateResult, nil
}
