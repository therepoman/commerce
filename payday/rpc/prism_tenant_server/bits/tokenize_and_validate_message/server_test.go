package tokenize_and_validate_message

import (
	"context"
	"fmt"
	"testing"

	tokenize_and_validate_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/prism_tenant/tokenize_and_validate_message"
	prism "code.justin.tv/commerce/prism/rpc"
	prism_models "code.justin.tv/commerce/prism/rpc/models"
	prism_tenant "code.justin.tv/commerce/prism/rpc/tenant"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestTokenizeAndValidateMessage(t *testing.T) {
	Convey("Given a TokenizeAndValidateMessage server", t, func() {
		channelID := "108706201"
		userID := "108706200"
		amount := 420

		bitsTokenizeAndValidate := new(tokenize_and_validate_mock.TokenizeAndValidate)

		api := &ServerImpl{
			BitsTokenizeAndValidate: bitsTokenizeAndValidate,
		}

		req := &prism_tenant.TokenizeAndValidateMessageReq{
			UserId:    userID,
			ChannelId: channelID,
			ConsumableAmounts: map[string]int64{
				prism_models.ConsumableType_Bits: int64(amount),
			},
			Message:   fmt.Sprintf("cheer%d", amount),
			MessageId: "test-message",
			Anonymous: false,
		}

		Convey("When the request is nil", func() {
			req = nil

			Convey("Then an error is returned", func() {
				res, err := api.TokenizeAndValidate(context.Background(), req)

				So(res, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("When the userID is empty", func() {
			req.UserId = ""

			Convey("Then an error is returned", func() {
				res, err := api.TokenizeAndValidate(context.Background(), req)

				So(res, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("When the channelID is empty", func() {
			req.ChannelId = ""

			Convey("Then an error is returned", func() {
				res, err := api.TokenizeAndValidate(context.Background(), req)

				So(res, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("When the messageID is empty", func() {
			req.MessageId = ""

			Convey("Then an error is returned", func() {
				res, err := api.TokenizeAndValidate(context.Background(), req)

				So(res, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("When the message is empty", func() {
			req.Message = ""

			Convey("Then an error is returned", func() {
				res, err := api.TokenizeAndValidate(context.Background(), req)

				So(res, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when the bits tokenize and validate call succeeds", func() {
			mockResponse := prism_tenant.TokenizeAndValidateMessageResp{
				ConsumableAmounts: map[string]int64{
					prism_models.ConsumableType_Bits: 2,
				},
				Tokens: []*prism.Token{},
			}

			bitsTokenizeAndValidate.On("TokenizeAndValidate", mock.Anything, mock.Anything).Return(mockResponse, nil)

			Convey("we should return the TokenizeAndValidateResp", func() {
				res, err := api.TokenizeAndValidate(context.Background(), req)

				So(err, ShouldBeNil)
				So(*res, ShouldResemble, mockResponse)
			})
		})
	})
}
