package process_message

import (
	"context"
	"fmt"
	"time"

	"code.justin.tv/commerce/gogogadget/audit"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/clients/cloudwatchlogger"
	"code.justin.tv/commerce/payday/metrics"
	"code.justin.tv/commerce/payday/prism_tenant/process_message"
	prism_models "code.justin.tv/commerce/prism/rpc/models"
	prism_tenant "code.justin.tv/commerce/prism/rpc/tenant"
	"github.com/twitchtv/twirp"
)

type Server interface {
	Process(ctx context.Context, req *prism_tenant.ProcessMessageReq) (*prism_tenant.ProcessMessageResp, error)
}

type ServerImpl struct {
	BitsMessageProcessor process_message.ProcessMessage    `inject:"bits-message-processor"`
	AuditLogger          cloudwatchlogger.CloudWatchLogger `inject:""`
	Statter              metrics.Statter                   `inject:""`
}

func (s *ServerImpl) Process(ctx context.Context, req *prism_tenant.ProcessMessageReq) (*prism_tenant.ProcessMessageResp, error) {
	if req == nil {
		return nil, twirp.InvalidArgumentError("Request", "cannot be nil")
	} else if req.UserId == "" {
		return nil, twirp.RequiredArgumentError("UserId")
	} else if req.ChannelId == "" {
		return nil, twirp.RequiredArgumentError("ChannelId")
	} else if req.MessageId == "" {
		return nil, twirp.RequiredArgumentError("MessageId")
	} else if req.Message == "" {
		return nil, twirp.RequiredArgumentError("Message")
	} else if req.ConsumableAmounts[prism_models.ConsumableType_Bits] <= 0 {
		return nil, twirp.InvalidArgumentError("ConsumableAmount", "must be greater than zero")
	}

	auditLoggerTimer := time.Now()
	apiName := "PrismProcess"
	msg, err := audit.CreateAccessLogMsg(ctx, apiName, *req)
	if err != nil {
		log.WithError(err).Error(fmt.Sprintf("failed to generate access log msg in %s", apiName))
	} else {
		err = s.AuditLogger.Send(ctx, msg)
		if err != nil {
			log.WithError(err).Warn(fmt.Sprintf("error while sending audit log in %s", apiName))
		}
	}
	go s.Statter.TimingDuration("PrismProcess_AuditLogger", time.Since(auditLoggerTimer))

	bitsMessageResult, err := s.BitsMessageProcessor.Process(ctx, req)

	if err != nil {
		return nil, err
	}

	return &bitsMessageResult, nil
}
