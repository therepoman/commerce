package post_process_message

import (
	"context"

	"code.justin.tv/commerce/payday/prism_tenant/post_process_message"
	prism_tenant "code.justin.tv/commerce/prism/rpc/tenant"
)

type Server interface {
	PostProcess(ctx context.Context, req *prism_tenant.PostProcessMessageReq) (*prism_tenant.PostProcessMessageResp, error)
}

type ServerImpl struct {
	VoldsMessagePostProcessor post_process_message.PostProcessMessage `inject:"volds-message-post-processor"`
}

func (s *ServerImpl) PostProcess(ctx context.Context, req *prism_tenant.PostProcessMessageReq) (*prism_tenant.PostProcessMessageResp, error) {
	// TODO: Validation
	return &prism_tenant.PostProcessMessageResp{}, nil
}
