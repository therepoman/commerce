package bits

import (
	"context"

	"code.justin.tv/commerce/payday/rpc/prism_tenant_server/volds/post_process_message"
	"code.justin.tv/commerce/payday/rpc/prism_tenant_server/volds/process_message"
	"code.justin.tv/commerce/payday/rpc/prism_tenant_server/volds/tokenize_and_validate_message"
	prism_tenant "code.justin.tv/commerce/prism/rpc/tenant"
)

type server struct {
	TokenizeAndValidateMessageServer tokenize_and_validate_message.Server `inject:"volds-tokenize-and-validate-server"`
	ProcessMessageServer             process_message.Server               `inject:"volds-process-message-server"`
	PostProcessMessageServer         post_process_message.Server          `inject:"volds-post-process-message-server"`
}

func NewServer() prism_tenant.PrismTenant {
	return &server{}
}

func (s *server) HealthCheck(context context.Context, req *prism_tenant.HealthCheckReq) (*prism_tenant.HealthCheckResp, error) {
	return &prism_tenant.HealthCheckResp{}, nil
}

func (s *server) TokenizeAndValidateMessage(ctx context.Context, req *prism_tenant.TokenizeAndValidateMessageReq) (*prism_tenant.TokenizeAndValidateMessageResp, error) {
	return s.TokenizeAndValidateMessageServer.TokenizeAndValidate(ctx, req)
}

func (s *server) ProcessMessage(ctx context.Context, req *prism_tenant.ProcessMessageReq) (*prism_tenant.ProcessMessageResp, error) {
	return s.ProcessMessageServer.Process(ctx, req)
}

func (s *server) PostProcessMessage(ctx context.Context, req *prism_tenant.PostProcessMessageReq) (*prism_tenant.PostProcessMessageResp, error) {
	return s.PostProcessMessageServer.PostProcess(ctx, req)
}
