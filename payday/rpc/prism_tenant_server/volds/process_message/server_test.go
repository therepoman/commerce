package process_message

import (
	"context"
	"errors"
	"testing"

	process_message_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/prism_tenant/process_message"
	prism "code.justin.tv/commerce/prism/rpc"
	prism_models "code.justin.tv/commerce/prism/rpc/models"
	prism_tenant "code.justin.tv/commerce/prism/rpc/tenant"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestProcessMessage(t *testing.T) {
	Convey("Given a ProcessMessage server", t, func() {
		channelID := "108706201"
		userID := "108706200"

		voldsMessageProcessor := new(process_message_mock.ProcessMessage)

		api := &ServerImpl{
			VoldsMessageProcessor: voldsMessageProcessor,
		}

		req := &prism_tenant.ProcessMessageReq{
			UserId:            userID,
			ChannelId:         channelID,
			ConsumableAmounts: map[string]int64{},
			Message:           "test",
			MessageId:         "test-message",
			Anonymous:         false,
		}

		Convey("When the request is nil", func() {
			req = nil

			Convey("Then an error is returned", func() {
				res, err := api.Process(context.Background(), req)

				So(res, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("When the userID is empty", func() {
			req.UserId = ""

			Convey("Then an error is returned", func() {
				res, err := api.Process(context.Background(), req)

				So(res, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("When the channelID is empty", func() {
			req.ChannelId = ""

			Convey("Then an error is returned", func() {
				res, err := api.Process(context.Background(), req)

				So(res, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("When the messageID is empty", func() {
			req.MessageId = ""

			Convey("Then an error is returned", func() {
				res, err := api.Process(context.Background(), req)

				So(res, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("When the message is empty", func() {
			req.Message = ""

			Convey("Then an error is returned", func() {
				res, err := api.Process(context.Background(), req)

				So(res, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when the request passes all validation", func() {
			Convey("when the message fails to process", func() {
				voldsMessageProcessor.On("Process", mock.Anything, mock.Anything).Return(prism_tenant.ProcessMessageResp{}, errors.New("ERROR"))

				Convey("we should return the error", func() {
					res, err := api.Process(context.Background(), req)

					So(res, ShouldBeNil)
					So(err, ShouldNotBeNil)
				})
			})

			Convey("when the message is processed successfully", func() {
				mockMessageProcessorResponse := prism_tenant.ProcessMessageResp{
					NewBalance:                 map[string]int64{},
					ConsumableAmountsProcessed: map[string]int64{},
					Transformations:            []*prism.Transformation{},
					MessageTreatment:           string(prism_models.DEFAULT_MESSAGE),
				}

				voldsMessageProcessor.On("Process", mock.Anything, mock.Anything).Return(mockMessageProcessorResponse, nil)

				Convey("we should return the ProcessMessageResp", func() {
					res, err := api.Process(context.Background(), req)

					So(err, ShouldBeNil)
					So(*res, ShouldResemble, mockMessageProcessorResponse)
				})
			})
		})
	})
}
