package process_message

import (
	"context"

	"code.justin.tv/commerce/payday/prism_tenant/process_message"
	prism_tenant "code.justin.tv/commerce/prism/rpc/tenant"
	"github.com/twitchtv/twirp"
)

type Server interface {
	Process(ctx context.Context, req *prism_tenant.ProcessMessageReq) (*prism_tenant.ProcessMessageResp, error)
}

type ServerImpl struct {
	VoldsMessageProcessor process_message.ProcessMessage `inject:"volds-message-processor"`
}

func (s *ServerImpl) Process(ctx context.Context, req *prism_tenant.ProcessMessageReq) (*prism_tenant.ProcessMessageResp, error) {
	if req == nil {
		return nil, twirp.InvalidArgumentError("Request", "cannot be nil")
	} else if req.UserId == "" {
		return nil, twirp.RequiredArgumentError("UserId")
	} else if req.ChannelId == "" {
		return nil, twirp.RequiredArgumentError("ChannelId")
	} else if req.MessageId == "" {
		return nil, twirp.RequiredArgumentError("MessageId")
	} else if req.Message == "" {
		return nil, twirp.RequiredArgumentError("Message")
	} // TODO: validate > 0 volds consumable amounts

	result, err := s.VoldsMessageProcessor.Process(ctx, req)

	if err != nil {
		return nil, err
	}

	return &result, nil
}
