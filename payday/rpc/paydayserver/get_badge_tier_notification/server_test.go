package get_badge_tier_notification

import (
	"context"
	"testing"

	"code.justin.tv/commerce/payday/badgetiers/models"
	"code.justin.tv/commerce/payday/errors"
	tiernotifications_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/badgetiers/products/notifications"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestServer_Get(t *testing.T) {
	Convey("given a Get API", t, func() {
		getter := new(tiernotifications_mock.Getter)

		api := &ServerImpl{
			Getter: getter,
		}

		userID := "123123123"
		channelID := "abcdefg"

		Convey("when the request is nil", func() {
			var req *paydayrpc.GetBadgeTierNotificationReq
			_, err := api.Get(context.Background(), req)

			Convey("we should return an error", func() {
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when the user ID is empty", func() {
			req := &paydayrpc.GetBadgeTierNotificationReq{
				ChannelId: channelID,
			}
			_, err := api.Get(context.Background(), req)

			Convey("we should return an error", func() {
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when the channel ID is empty", func() {
			req := &paydayrpc.GetBadgeTierNotificationReq{
				UserId: userID,
			}
			_, err := api.Get(context.Background(), req)

			Convey("we should return an error", func() {
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when the channel ID and user ID are defined", func() {
			req := &paydayrpc.GetBadgeTierNotificationReq{
				UserId:    userID,
				ChannelId: channelID,
			}

			Convey("when getter returns an error", func() {
				getter.On("Get", mock.Anything, userID, channelID).Return(nil, errors.New("ERROR"))
				_, err := api.Get(context.Background(), req)

				Convey("we should return an error", func() {
					So(err, ShouldNotBeNil)
				})
			})

			Convey("when the getter does not return a notification", func() {
				getter.On("Get", mock.Anything, userID, channelID).Return(nil, nil)

				returnValue, err := api.Get(context.Background(), req)

				Convey("we should return nil and a not found error", func() {
					So(err, ShouldBeNil)
					So(returnValue, ShouldResemble, &paydayrpc.GetBadgeTierNotificationResp{})
				})
			})

			Convey("when the getter returns a notification", func() {
				getter.On("Get", mock.Anything, userID, channelID).Return(&models.BadgeTierNotification{
					Threshold:         100,
					NotificationState: models.Show,
					NotificationId:    "some-fake-id",
					CanShare:          true,
				}, nil)

				returnValue, err := api.Get(context.Background(), req)

				Convey("we should return the tier notifications response", func() {
					So(err, ShouldBeNil)
					So(*returnValue, ShouldResemble, paydayrpc.GetBadgeTierNotificationResp{
						Threshold:         100,
						NotificationState: "SHOW",
						NotificationId:    "some-fake-id",
						CanShare:          true,
					})
				})
			})
		})
	})
}
