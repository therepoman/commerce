package get_badge_tier_notification

import (
	"context"
	"time"

	log "code.justin.tv/commerce/logrus"
	tiernotifications "code.justin.tv/commerce/payday/badgetiers/products/notifications"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"github.com/twitchtv/twirp"
)

type Server interface {
	Get(ctx context.Context, req *paydayrpc.GetBadgeTierNotificationReq) (*paydayrpc.GetBadgeTierNotificationResp, error)
}

type ServerImpl struct {
	Getter tiernotifications.Getter `inject:""`
}

func (s *ServerImpl) Get(ctx context.Context, req *paydayrpc.GetBadgeTierNotificationReq) (*paydayrpc.GetBadgeTierNotificationResp, error) {
	ctx, cancel := context.WithTimeout(ctx, 500*time.Millisecond)
	defer cancel()

	if req == nil {
		return nil, twirp.InternalError("cannot have empty request")
	}

	if req.UserId == "" {
		return nil, twirp.InvalidArgumentError("UserId", "cannot have empty user id")
	}

	if req.ChannelId == "" {
		return nil, twirp.InvalidArgumentError("ChannelId", "cannot have empty channel id")
	}

	badgeTierNotification, err := s.Getter.Get(ctx, req.UserId, req.ChannelId)

	if err != nil {
		log.WithError(err).WithFields(log.Fields{
			"userID":    req.UserId,
			"channelID": req.ChannelId,
		}).Error("could not get badge tier notification")

		return nil, twirp.InternalErrorWith(err)
	}

	if badgeTierNotification == nil {
		return &paydayrpc.GetBadgeTierNotificationResp{}, nil
	}

	return &paydayrpc.GetBadgeTierNotificationResp{
		NotificationState: string(badgeTierNotification.NotificationState),
		Threshold:         badgeTierNotification.Threshold,
		NotificationId:    badgeTierNotification.NotificationId,
		CanShare:          badgeTierNotification.CanShare,
		EmoteIds:          badgeTierNotification.EmoteIDs,
	}, nil
}
