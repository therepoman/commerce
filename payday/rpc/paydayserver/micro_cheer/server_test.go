package micro_cheer

import (
	"context"
	"testing"

	leaderboard_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/leaderboard"

	"code.justin.tv/commerce/gogogadget/random"
	"code.justin.tv/commerce/payday/clients/miniexperiments"
	"code.justin.tv/commerce/payday/clients/pubsub"
	"code.justin.tv/commerce/payday/errors"
	clients_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/miniexperiments"
	pubsub_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/pubsub"
	event_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/event"
	partner_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/partner"
	transaction_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/prism_tenant/process_message/bits/transaction"
	payday_models "code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/partner"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
	"github.com/twitchtv/twirp"
)

func TestServer_GetTotalBitsToBroadcaster(t *testing.T) {
	Convey("Given a server", t, func() {
		miniexperimentMock := new(clients_mock.MiniExperimentClient)
		partnerManagerMock := new(partner_mock.IPartnerManager)
		pubsubUtilsMock := new(pubsub_mock.IPubSubUtils)
		getBitsToBroadcasterMock := new(leaderboard_mock.BitsToBroadcasterGetter)
		giverMock := new(event_mock.PachinkoGiver)
		transactionCreatorMock := new(transaction_mock.Creator)

		server := &ServerImpl{
			MiniExperimentClient:    miniexperimentMock,
			PartnerManager:          partnerManagerMock,
			PubSubUtils:             pubsubUtilsMock,
			BitsToBroadcasterGetter: getBitsToBroadcasterMock,
			Giver:                   giverMock,
			TransactionCreator:      transactionCreatorMock,
		}

		req := &paydayrpc.MicroCheerReq{}

		Convey("when request is nil", func() {
			_, err := server.PerformMicroCheer(context.Background(), nil)
			So(err, ShouldNotBeNil)
		})

		Convey("when request is empty", func() {
			_, err := server.PerformMicroCheer(context.Background(), req)
			So(err, ShouldNotBeNil)

			Convey("when transaction id is set", func() {
				req.TransactionId = random.String(32)

				_, err := server.PerformMicroCheer(context.Background(), req)
				So(err, ShouldNotBeNil)

				Convey("when user id is set", func() {
					req.UserId = random.String(16)

					_, err := server.PerformMicroCheer(context.Background(), req)
					validateTwirpInvalidArgsError(err, "channel_id")

					Convey("when channel id is set", func() {
						req.ChannelId = random.String(16)

						_, err := server.PerformMicroCheer(context.Background(), req)
						So(err, ShouldNotBeNil)

						Convey("when quantity is set", func() {
							req.Quantity = 10

							_, err := server.PerformMicroCheer(context.Background(), req)
							validateTwirpInvalidArgsError(err, "type")

							Convey("when type is unknown", func() {
								req.Type = paydayrpc.MicroCheerType_UNKNOWN

								_, err := server.PerformMicroCheer(context.Background(), req)
								validateTwirpInvalidArgsError(err, "type")
							})

							Convey("when type is junk", func() {
								req.Type = paydayrpc.MicroCheerType(-1)

								_, err := server.PerformMicroCheer(context.Background(), req)
								validateTwirpInvalidArgsError(err, "type")
							})

							Convey("when type is PINATA", func() {
								req.Type = paydayrpc.MicroCheerType_PINATA

								Convey("when miniexperiment returns an error", func() {
									miniexperimentError := errors.New("invalid experiment")
									miniexperimentMock.On("GetTreatment", mock.Anything, req.GetChannelId()).Return("", miniexperimentError)

									_, err := server.PerformMicroCheer(context.Background(), req)
									validateTwirpInternalError(err)
								})

								Convey("when miniexperiment returns control", func() {
									miniexperimentMock.On("GetTreatment", mock.Anything, req.GetChannelId()).Return(miniexperiments.TreatmentControl, nil)

									_, err := server.PerformMicroCheer(context.Background(), req)
									validateTwirpInvalidArgsError(err, "channel_id")
								})

								Convey("when miniexperiment returns variant", func() {
									miniexperimentMock.On("GetTreatment", mock.Anything, req.GetChannelId()).Return(experimentVariant, nil)

									Convey("when partner manager returns traditional", func() {
										partnerManagerMock.On("GetPartnerType", mock.Anything, req.GetChannelId()).Return(partner.TraditionalPartnerType, nil)

										Convey("when the PubSubUtils returns a chatter count", func() {
											pubsubUtilsMock.On("GetNumberOfListeners", mock.Anything, pubsub.ChannelListenersPubSubTopic, req.ChannelId).Return(100, nil)

											Convey("when pantheon returns a nil leaderboard position", func() {
												getBitsToBroadcasterMock.On("GetBitsToBroadcaster", mock.Anything, req.GetUserId(), req.GetChannelId()).Return(int64(0), nil)

												Convey("when pachinko giver works", func() {
													giverMock.On("Give", mock.Anything, mock.Anything).Return(int32(58), payday_models.Default, nil)

													Convey("when transaction creator works", func() {
														transactionCreatorMock.On("CreateTransactionFromMicroCheer",
															mock.Anything, mock.Anything, mock.Anything, mock.Anything, partner.TraditionalPartnerType, "100", int32(58), int64(0)).Return(nil)

														_, err := server.PerformMicroCheer(context.Background(), req)
														So(err, ShouldBeNil)
													})
												})
											})
										})
									})
								})
							})
						})
					})
				})
			})
		})
	})
}

func validateTwirpInvalidArgsError(err error, arg string) {
	twirpErr := unpackTwirpError(err)
	So(twirpErr.Code(), ShouldEqual, twirp.InvalidArgument)
	So(twirpErr.Meta("argument"), ShouldEqual, arg)
}

func validateTwirpInternalError(err error) {
	twirpErr := unpackTwirpError(err)
	So(twirpErr.Code(), ShouldEqual, twirp.Internal)
}

func unpackTwirpError(err error) twirp.Error {
	So(err, ShouldNotBeNil)
	twirpErr, ok := err.(twirp.Error)
	So(ok, ShouldBeTrue)
	return twirpErr
}
