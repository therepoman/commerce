package micro_cheer

import (
	"context"
	"strconv"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/clients/miniexperiments"
	"code.justin.tv/commerce/payday/clients/pubsub"
	"code.justin.tv/commerce/payday/event"
	event_models "code.justin.tv/commerce/payday/event/models"
	"code.justin.tv/commerce/payday/leaderboard"
	"code.justin.tv/commerce/payday/middleware"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/partner"
	process_models "code.justin.tv/commerce/payday/prism_tenant/process_message/bits/models"
	"code.justin.tv/commerce/payday/prism_tenant/process_message/bits/transaction"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"github.com/twitchtv/twirp"
)

const (
	experimentName    = "BitsPinata"
	experimentVariant = "Experiment"
)

type Server interface {
	PerformMicroCheer(ctx context.Context, req *paydayrpc.MicroCheerReq) (*paydayrpc.MicroCheerResp, error)
}

type ServerImpl struct {
	Giver                   event.PachinkoGiver                  `inject:"pachinkoGiver"`
	PartnerManager          partner.IPartnerManager              `inject:""`
	TransactionCreator      transaction.Creator                  `inject:""`
	BitsToBroadcasterGetter leaderboard.BitsToBroadcasterGetter  `inject:""`
	PubSubUtils             pubsub.IPubSubUtils                  `inject:""`
	MiniExperimentClient    miniexperiments.MiniExperimentClient `inject:""`
}

func (s *ServerImpl) PerformMicroCheer(ctx context.Context, req *paydayrpc.MicroCheerReq) (*paydayrpc.MicroCheerResp, error) {

	if req == nil {
		return nil, twirp.InvalidArgumentError("body", "cannot be empty")
	}
	if len(req.GetTransactionId()) == 0 {
		return nil, twirp.InvalidArgumentError("transaction_id", "is required")
	}
	if len(req.GetChannelId()) == 0 {
		return nil, twirp.InvalidArgumentError("channel_id", "is required")
	}
	if len(req.GetUserId()) == 0 {
		return nil, twirp.InvalidArgumentError("user_id", "is required")
	}
	if req.GetQuantity() <= 0 {
		return nil, twirp.InvalidArgumentError("quantity", "is required")
	}
	if req.GetType() == paydayrpc.MicroCheerType_UNKNOWN {
		return nil, twirp.InvalidArgumentError("type", "is required and cannot be unknown")
	}
	_, ok := paydayrpc.MicroCheerType_name[int32(req.GetType())]
	if !ok {
		return nil, twirp.InvalidArgumentError("type", "is required and cannot be unknown")

	}

	if req.Type == paydayrpc.MicroCheerType_PINATA {
		pinataExperimentConfig, ok := miniexperiments.GetExperiments()[experimentName]
		if !ok {
			return nil, twirp.InternalError("BitsPinata experiment no longer active")
		}

		treatment, err := s.MiniExperimentClient.GetTreatment(
			pinataExperimentConfig,
			req.GetChannelId())
		if err != nil {
			log.WithError(err).Error("Failed to check experiment status")
			return nil, twirp.InternalError("failed to check if this experiment is still active")
		}
		if treatment != experimentVariant {
			return nil, twirp.InvalidArgumentError("channel_id", "is not part of the experiment")
		}
	}

	clientID := middleware.GetClientID(ctx)

	partnerType, err := s.PartnerManager.GetPartnerType(ctx, req.ChannelId)
	if err != nil {
		log.WithField("channel_id", req.ChannelId).WithError(err).Info("Could not get partner type")
		return nil, err
	}

	// Microcheering for compatibility with existing data science and payouts will pretend to use the default cheer.
	emoteTotals := map[string]models.BitEmoteTotal{
		"cheer": {
			CheeredTotal: req.GetQuantity(),
		},
	}

	// Microcheering has no support sponsored bits
	sponsoredAmounts := map[string]models.Bits{}

	numChatters, err := s.PubSubUtils.GetNumberOfListeners(ctx, pubsub.ChannelListenersPubSubTopic, req.ChannelId)
	if err != nil {
		return nil, err // should we error on this
	}

	giveBitsParams := event_models.Request{
		UserID:           req.UserId,
		ChannelID:        req.ChannelId,
		Amount:           models.Bits(req.GetQuantity()),
		EventID:          req.GetTransactionId(),
		EventReasonCode:  models.EventReasonCheer,
		EmoteUses:        process_models.ConvertEmoteTotalsForFabs(emoteTotals),
		ClientID:         clientID,
		PartnerType:      partnerType,
		SponsoredAmounts: sponsoredAmounts,
		NumChatters:      strconv.FormatInt(int64(numChatters), 10),
		IsAnonymous:      false,
	}

	totalBitsToBroadcasterBefore, err := s.BitsToBroadcasterGetter.GetBitsToBroadcaster(ctx, req.GetUserId(), req.GetChannelId())
	if err != nil {
		return nil, err
	}

	updatedBalance, _, err := s.Giver.Give(ctx, giveBitsParams)
	if err != nil {
		return nil, err
	}

	err = s.TransactionCreator.CreateTransactionFromMicroCheer(
		ctx, req, emoteTotals, sponsoredAmounts, partnerType,
		strconv.FormatInt(int64(numChatters), 10),
		updatedBalance,
		totalBitsToBroadcasterBefore)
	if err != nil {
		return nil, err
	}

	return &paydayrpc.MicroCheerResp{}, nil
}
