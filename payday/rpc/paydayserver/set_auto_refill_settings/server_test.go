package set_auto_refill_settings

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/payday/dynamo/auto_refill_settings"
	products_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/products"
	refill_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/refill"
	"code.justin.tv/commerce/payday/refill"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	petozi "code.justin.tv/commerce/petozi/rpc"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestServer_Set(t *testing.T) {
	userId := "test_user_id"
	offerId := "test_offer_id"

	Convey("given a Set auto refill settings API", t, func() {
		refillMock := new(refill_mock.Refill)
		fetcherMock := new(products_mock.Fetcher)

		api := &ServerImpl{
			Refill:          refillMock,
			ProductsFetcher: fetcherMock,
		}

		Convey("when request is nil, we should return an error", func() {
			resp, err := api.Set(context.Background(), nil)
			So(err, ShouldNotBeNil)
			So(resp, ShouldBeNil)
		})

		Convey("when user_id is blank, we should return an error", func() {
			resp, err := api.Set(context.Background(), &paydayrpc.SetAutoRefillSettingsReq{
				UserId:   "",
				Settings: &paydayrpc.AutoRefillSettings{},
			})
			So(err, ShouldNotBeNil)
			So(resp, ShouldBeNil)
		})

		Convey("when settings is nil, we should return an error", func() {
			resp, err := api.Set(context.Background(), &paydayrpc.SetAutoRefillSettingsReq{
				UserId:   userId,
				Settings: nil,
			})
			So(err, ShouldNotBeNil)
			So(resp, ShouldBeNil)
		})

		Convey("when settings is not nil", func() {
			settings := paydayrpc.AutoRefillSettings{
				Id:                         "test_id",
				OfferId:                    offerId,
				Threshold:                  100,
				Enabled:                    true,
				IsEmailNotificationEnabled: true,
				ChargeInstrumentCategory:   "instrument_category",
				ChargeInstrumentId:         "instrument_id",
			}

			Convey("and threshold is negative, we should return an error", func() {
				settings.Threshold = -1
				resp, err := api.Set(context.Background(), &paydayrpc.SetAutoRefillSettingsReq{
					UserId:   userId,
					Settings: &settings,
				})
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})

			Convey("and offerId is blank, we should return an error", func() {
				settings.OfferId = ""
				resp, err := api.Set(context.Background(), &paydayrpc.SetAutoRefillSettingsReq{
					UserId:   userId,
					Settings: &settings,
				})
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})

			Convey("and Charge instrument category is blank, we should return an error", func() {
				settings.ChargeInstrumentCategory = ""
				resp, err := api.Set(context.Background(), &paydayrpc.SetAutoRefillSettingsReq{
					UserId:   userId,
					Settings: &settings,
				})
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})

			Convey("and Charge instrument id is blank, we should return an error", func() {
				settings.ChargeInstrumentId = ""
				resp, err := api.Set(context.Background(), &paydayrpc.SetAutoRefillSettingsReq{
					UserId:   userId,
					Settings: &settings,
				})
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})

			Convey("when request parameters are valid", func() {
				Convey("when Set returns an error, we should return an error", func() {
					refillMock.On("Set", mock.Anything, mock.Anything, refill.ManualUpdateType).Return(nil, errors.New("ERROR"))
					fetcherMock.On("FetchAllFromPetozi", mock.Anything).Return([]*petozi.BitsProduct{{
						OfferId:  offerId,
						Quantity: 1000,
					}}, nil)

					resp, err := api.Set(context.Background(), &paydayrpc.SetAutoRefillSettingsReq{
						UserId:   userId,
						Settings: &settings,
					})
					So(err, ShouldNotBeNil)
					So(resp, ShouldBeNil)
				})
				Convey("when FetchAllFromPetozi returns an error, we should return an error.", func() {
					refillMock.On("Set", mock.Anything, mock.Anything, refill.ManualUpdateType).Return(&auto_refill_settings.AutoRefillSettingsProfile{}, nil)
					fetcherMock.On("FetchAllFromPetozi", mock.Anything).Return(nil, errors.New("ERROR"))

					resp, err := api.Set(context.Background(), &paydayrpc.SetAutoRefillSettingsReq{
						UserId:   userId,
						Settings: &settings,
					})
					So(err, ShouldNotBeNil)
					So(resp, ShouldBeNil)
				})
				Convey("when Set returns a response, we should return the response", func() {
					refillMock.On("Set", mock.Anything, mock.Anything, refill.ManualUpdateType).Return(&auto_refill_settings.AutoRefillSettingsProfile{}, nil)
					fetcherMock.On("FetchAllFromPetozi", mock.Anything).Return([]*petozi.BitsProduct{
						{
							OfferId:  "another_offer_id",
							Quantity: 100,
						},
						{
							OfferId:  offerId,
							Quantity: 1000,
						},
					}, nil)

					resp, err := api.Set(context.Background(), &paydayrpc.SetAutoRefillSettingsReq{
						UserId:   userId,
						Settings: &settings,
					})
					So(err, ShouldBeNil)
					So(resp, ShouldNotBeNil)
				})

			})
		})

	})
}
