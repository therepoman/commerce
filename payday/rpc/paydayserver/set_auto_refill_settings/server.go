package set_auto_refill_settings

import (
	"context"
	"fmt"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/dynamo/auto_refill_settings"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/products"
	"code.justin.tv/commerce/payday/refill"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	petozi "code.justin.tv/commerce/petozi/rpc"
	"github.com/twitchtv/twirp"
)

type Server interface {
	Set(ctx context.Context, req *paydayrpc.SetAutoRefillSettingsReq) (*paydayrpc.SetAutoRefillSettingsResp, error)
}

type ServerImpl struct {
	Refill          refill.Refill    `inject:""`
	ProductsFetcher products.Fetcher `inject:""`
}

func (s *ServerImpl) Set(ctx context.Context, req *paydayrpc.SetAutoRefillSettingsReq) (settingsReq *paydayrpc.SetAutoRefillSettingsResp, err error) {
	if req == nil {
		return nil, twirp.InternalError("cannot have empty request")
	}

	if req.UserId == "" {
		return nil, twirp.InvalidArgumentError("UserId", "cannot have empty user id")
	}

	if req.Settings == nil {
		return nil, twirp.InvalidArgumentError("Settings", "cannot have nil settings")
	}
	var state string
	if req.Settings.Enabled {

		if req.Settings.Threshold <= 0 {
			return nil, twirp.InvalidArgumentError("Settings.Threshhold", "cannot have threshold below 0")
		}

		if req.Settings.OfferId == "" {
			return nil, twirp.InvalidArgumentError("Settings.OfferId", "cannot have empty offer id")
		}

		if req.Settings.ChargeInstrumentCategory == "" {
			return nil, twirp.InvalidArgumentError("Settings.ChargeInstrumentCategory", "cannot have empty charge instrument category")
		}

		if req.Settings.ChargeInstrumentId == "" {
			return nil, twirp.InvalidArgumentError("Settings.ChargeInstrumentId", "cannot have empty charge instrument id")
		}

		//don't check req.Settings.Id because it's allowed for this to be nil

		state = refill.EnabledState
	} else {
		state = refill.DisabledState
	}

	profile := auto_refill_settings.AutoRefillSettingsProfile{
		UserId:                     req.UserId,
		State:                      state,
		Threshold:                  req.Settings.Threshold,
		OfferId:                    req.Settings.OfferId,
		IsEmailNotificationEnabled: req.Settings.IsEmailNotificationEnabled,
		Id:                         req.Settings.Id,
		Tpl:                        refill.BitsTPL,
		ChargeInstrumentCategory:   req.Settings.ChargeInstrumentCategory,
		ChargeInstrumentId:         req.Settings.ChargeInstrumentId,
		SKUQuantity:                req.Settings.Quantity,
	}
	product, err := s.getBitsProduct(ctx, req.Settings.OfferId)
	if err != nil {
		log.WithError(err).WithFields(log.Fields{
			"userID":  req.UserId,
			"offerID": req.Settings.OfferId},
		).Error("could not fetch product quantity from petozi")
		return nil, twirp.InternalErrorWith(err)
	} else if product != nil {
		profile.BitsQuantity = int32(product.Quantity)
		profile.SKU = product.Id
		profile.BitsType = product.BitsTypeId
	}

	newProfile, err := s.Refill.Set(ctx, profile, refill.ManualUpdateType)

	if err != nil {
		log.WithError(err).WithFields(log.Fields{"userID": req.UserId}).Error("could not set user refill settings")
		return nil, twirp.InternalErrorWith(err)
	}

	if newProfile != nil {
		return &paydayrpc.SetAutoRefillSettingsResp{
			NewSettings: &paydayrpc.AutoRefillSettings{
				Id:                         newProfile.Id,
				OfferId:                    newProfile.OfferId,
				Threshold:                  newProfile.Threshold,
				Quantity:                   newProfile.SKUQuantity,
				Enabled:                    newProfile.State == refill.EnabledState,
				IsEmailNotificationEnabled: newProfile.IsEmailNotificationEnabled,
				ChargeInstrumentCategory:   newProfile.ChargeInstrumentCategory,
				ChargeInstrumentId:         newProfile.ChargeInstrumentId,
				CreatedAt:                  newProfile.CreatedAt,
				Tpl:                        newProfile.Tpl,
				Sku:                        newProfile.SKU,
			},
		}, nil
	}

	return &paydayrpc.SetAutoRefillSettingsResp{}, nil
}

func (s *ServerImpl) getBitsProduct(ctx context.Context, offerId string) (*petozi.BitsProduct, error) {
	bitsProducts, err := s.ProductsFetcher.FetchAllFromPetozi(ctx)
	if err != nil {
		log.Errorf("Could not fetch products from Petozi: %v", err)
		return nil, err
	}
	for _, product := range bitsProducts {
		if product != nil && product.OfferId == offerId {
			return product, nil
		}
	}

	msg := fmt.Sprintf("Product with offerId %s not found in Petozi.", offerId)
	log.Error(msg)
	return nil, errors.New(msg)
}
