package create_hold

import (
	"context"
	"fmt"
	"time"

	"code.justin.tv/commerce/gogogadget/audit"
	"code.justin.tv/commerce/gogogadget/strings"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/clients/cloudwatchlogger"
	pachinko_client "code.justin.tv/commerce/payday/clients/pachinko"
	"code.justin.tv/commerce/payday/holds"
	"code.justin.tv/commerce/payday/metrics"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"github.com/gofrs/uuid"
	"github.com/twitchtv/twirp"
)

type Server interface {
	Create(ctx context.Context, req *paydayrpc.CreateHoldReq) (*paydayrpc.CreateHoldResp, error)
}

func NewServer() Server {
	return &server{}
}

type server struct {
	Validator   Validator                         `inject:""`
	Creator     holds.Creator                     `inject:"holdCreator"`
	AuditLogger cloudwatchlogger.CloudWatchLogger `inject:""`
	Statter     metrics.Statter                   `inject:""`
}

func (a *server) Create(ctx context.Context, req *paydayrpc.CreateHoldReq) (*paydayrpc.CreateHoldResp, error) {
	if err := a.Validator.IsValid(req); err != nil {
		return nil, err
	}

	if strings.Blank(req.HoldId) {
		req.HoldId = uuid.Must(uuid.NewV4()).String()
	}

	auditLoggerTimer := time.Now()
	apiName := "CreateHold"
	msg, err := audit.CreateAccessLogMsg(ctx, apiName, *req)
	if err != nil {
		log.WithError(err).Error(fmt.Sprintf("failed to generate access log msg in %s", apiName))
	} else {
		err = a.AuditLogger.Send(ctx, msg)
		if err != nil {
			log.WithError(err).Warn(fmt.Sprintf("error while sending audit log in %s", apiName))
		}
	}
	go a.Statter.TimingDuration("CreateHold_AuditLogger", time.Since(auditLoggerTimer))

	hold, err := a.Creator.Create(ctx, req)
	if err != nil {
		if err == pachinko_client.ErrInsufficientBalance {
			return nil, paydayrpc.NewClientError(twirp.NewError(twirp.FailedPrecondition, "insufficient bits balance"), paydayrpc.ClientError{
				ErrorCode: paydayrpc.ErrCodeInsufficientBitsBalance,
			})
		}

		return nil, twirp.InternalErrorWith(err)
	}

	return &paydayrpc.CreateHoldResp{
		Hold: &paydayrpc.Hold{
			HoldId:    hold.TransactionID,
			UserId:    hold.UserID,
			Amount:    req.Amount,
			ExpiresAt: req.ExpiresAt,
		},
	}, nil
}
