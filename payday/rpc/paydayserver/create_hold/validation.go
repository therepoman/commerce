package create_hold

import (
	"time"

	"code.justin.tv/commerce/gogogadget/strings"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"github.com/golang/protobuf/ptypes"
	"github.com/twitchtv/twirp"
)

type Validator interface {
	IsValid(req *paydayrpc.CreateHoldReq) error
}

func NewValidator() Validator {
	return &validator{}
}

type validator struct{}

func (v *validator) IsValid(req *paydayrpc.CreateHoldReq) error {
	if req == nil {
		return twirp.InvalidArgumentError("body", "cannot be empty")
	}

	if strings.Blank(req.UserId) {
		return twirp.InvalidArgumentError("user_id", "cannot be blank")
	}

	if req.Amount < 1 {
		return twirp.InvalidArgumentError("amount", "cannont be less than 1")
	}

	if req.ExpiresAt != nil {
		expiresAt, err := ptypes.Timestamp(req.ExpiresAt)
		if err != nil {
			return twirp.InvalidArgumentError("expires_at", "was incorrectly formatted")
		}

		if time.Now().After(expiresAt) {
			return twirp.InvalidArgumentError("expires_at", "cannot expire before now")
		}
	}

	return nil
}
