package create_hold

import (
	"testing"
	"time"

	"code.justin.tv/commerce/gogogadget/random"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"github.com/golang/protobuf/ptypes"
	. "github.com/smartystreets/goconvey/convey"
)

func TestValidator_IsValid(t *testing.T) {
	Convey("given a create hold API validator", t, func() {
		v := NewValidator()

		expiresAt, _ := ptypes.TimestampProto(time.Now().Add(5 * time.Minute))

		Convey("should pass on a valid request", func() {
			req := &paydayrpc.CreateHoldReq{
				UserId:    random.String(16),
				Amount:    int64(322),
				ExpiresAt: expiresAt,
			}

			Convey("when there's no expiration date", func() {
				req.ExpiresAt = nil
			})

			err := v.IsValid(req)

			So(err, ShouldBeNil)
		})

		Convey("should we pass on an invalid request", func() {
			req := &paydayrpc.CreateHoldReq{}

			Convey("when the req is nil", func() {
				req = nil
			})

			Convey("when there's no user ID", func() {
				req.UserId = ""
			})

			Convey("when there's no amount", func() {
				req.UserId = random.String(16)
				req.Amount = int64(322)
				req.Amount = 0
			})

			Convey("when the expiration date passed is before the current time", func() {
				req.UserId = random.String(16)
				req.Amount = int64(322)
				expiresAt, _ := ptypes.TimestampProto(time.Now().Add(time.Minute * -10))
				req.ExpiresAt = expiresAt
			})

			err := v.IsValid(req)

			So(err, ShouldNotBeNil)
		})
	})
}
