package create_hold

import (
	"context"
	"errors"
	"testing"
	"time"

	"code.justin.tv/commerce/gogogadget/random"
	"code.justin.tv/commerce/payday/clients/cloudwatchlogger"
	pachinko_client "code.justin.tv/commerce/payday/clients/pachinko"
	"code.justin.tv/commerce/payday/dynamo/holds"
	holds_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/holds"
	metrics_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/metrics"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"github.com/golang/protobuf/ptypes"
	error_pkg "github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestServer_Create(t *testing.T) {
	Convey("given a create hold server", t, func() {
		creator := new(holds_mock.Creator)
		validator := NewValidator()
		cwlogger := cloudwatchlogger.NewCloudWatchLogNoopClient()
		statter := new(metrics_mock.Statter)
		statter.On("TimingDuration", mock.Anything, mock.Anything).Return()

		a := server{
			Validator:   validator,
			Creator:     creator,
			AuditLogger: cwlogger,
			Statter:     statter,
		}

		ctx := context.Background()
		expiresAt, _ := ptypes.TimestampProto(time.Now().Add(time.Minute * 5))
		req := &paydayrpc.CreateHoldReq{
			UserId:    random.String(16),
			Amount:    int64(322),
			ExpiresAt: expiresAt,
		}

		Convey("when we are successful", func() {
			Convey("when we provide a transaction ID", func() {
				req.HoldId = random.String(32)
			})

			hold := &holds.BalanceHold{
				TransactionID: req.HoldId,
				UserID:        req.UserId,
				IsActive:      true,
			}

			creator.On("Create", ctx, req).Return(hold, nil)

			resp, err := a.Create(ctx, req)
			So(err, ShouldBeNil)
			So(resp, ShouldNotBeNil)
		})

		Convey("when we fail", func() {
			Convey("when the user has insufficient balance, we should return a validation error", func() {
				creator.On("Create", ctx, req).Return(nil, pachinko_client.ErrInsufficientBalance)
				_, err := a.Create(ctx, req)
				So(err, ShouldNotBeNil)

				var clientError paydayrpc.ClientError
				parseErr := paydayrpc.ParseClientError(error_pkg.Cause(err), &clientError)
				So(parseErr, ShouldBeNil)

				So(clientError.ErrorCode, ShouldEqual, paydayrpc.ErrCodeInsufficientBitsBalance)
			})

			Convey("when we have an invalid request", func() {
				req = nil
			})

			Convey("when the creator fails", func() {
				creator.On("Create", ctx, req).Return(nil, errors.New("WALRUS STRIKE"))
			})

			_, err := a.Create(ctx, req)
			So(err, ShouldNotBeNil)
		})
	})
}
