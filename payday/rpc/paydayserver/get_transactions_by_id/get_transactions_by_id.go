package get_transactions_by_id

import (
	"context"

	log "code.justin.tv/commerce/logrus"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"code.justin.tv/commerce/payday/transactions"
	"github.com/twitchtv/twirp"
)

type Server interface {
	Get(ctx context.Context, req *paydayrpc.GetTransactionsByIDReq) (*paydayrpc.GetTransactionsByIDResp, error)
}

type ServerImpl struct {
	Getter transactions.Getter `inject:""`
}

func (s *ServerImpl) Get(ctx context.Context, req *paydayrpc.GetTransactionsByIDReq) (*paydayrpc.GetTransactionsByIDResp, error) {
	transactionsByID, err := s.Getter.GetTransactionsByID(ctx, req.TransactionIds)
	if err != nil {
		log.WithError(err).WithField("transactionIds", req.TransactionIds).Error("Failed to get transactions")
		return nil, twirp.InternalErrorWith(err)
	}

	return &paydayrpc.GetTransactionsByIDResp{
		Transactions: transactionsByID,
	}, nil
}
