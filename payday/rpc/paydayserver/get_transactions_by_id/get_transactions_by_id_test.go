package get_transactions_by_id

import (
	"context"
	"errors"
	"testing"

	transactions_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/transactions"
	"code.justin.tv/commerce/payday/models"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestServer_Get(t *testing.T) {
	Convey("given a server", t, func() {
		getter := new(transactions_mock.Getter)

		server := &ServerImpl{
			Getter: getter,
		}

		ctx := context.Background()

		Convey("returns results from getter", func() {
			transactions := []*paydayrpc.Transaction{
				{
					TransactionId:   "1",
					ChannelId:       "123456",
					UserId:          "666",
					NumberOfBits:    100,
					RawMessage:      "cheer100",
					TransactionType: models.BitsEventTransactionType,
				},
				{
					TransactionId:   "2",
					UserId:          "5555",
					Platform:        "PHANTO",
					TransactionType: "AddBits",
				},
			}

			getter.On("GetTransactionsByID", mock.Anything, mock.Anything).Return(transactions, nil)

			resp, err := server.Get(ctx, &paydayrpc.GetTransactionsByIDReq{
				TransactionIds: []string{"1", "2"},
			})

			So(err, ShouldBeNil)
			So(resp, ShouldNotBeNil)
			So(resp.Transactions, ShouldResemble, transactions)
		})

		Convey("returns an error if getter fails", func() {
			getter.On("GetTransactionsByID", mock.Anything, mock.Anything).Return(nil, errors.New("uh oh"))

			_, err := server.Get(ctx, &paydayrpc.GetTransactionsByIDReq{
				TransactionIds: []string{"1", "2"},
			})

			So(err, ShouldNotBeNil)
		})
	})
}
