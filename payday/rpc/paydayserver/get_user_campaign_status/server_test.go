package get_user_campaign_status

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/mock"

	channel_status_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sponsored_cheermote/channel_status"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"code.justin.tv/commerce/payday/sponsored_cheermote/models"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
)

func TestServerImpl_Get(t *testing.T) {
	Convey("given a GetUserCampaignStatus server", t, func() {
		getter := new(channel_status_mock.Getter)

		server := &ServerImpl{
			Getter: getter,
		}

		userID := "123123123"
		campaignID := "savethewalrus"
		startTime := time.Date(2018, 9, 1, 0, 0, 0, 0, time.UTC)
		endTime := time.Date(2018, 10, 1, 0, 0, 0, 0, time.UTC)

		Convey("when the request is nil", func() {
			var req *paydayrpc.GetUserCampaignStatusesReq

			Convey("we should return an error", func() {
				_, err := server.Get(context.Background(), req)

				So(err, ShouldNotBeNil)
			})
		})

		Convey("when the request does not have a user ID", func() {
			req := &paydayrpc.GetUserCampaignStatusesReq{}

			Convey("we should return an error", func() {
				_, err := server.Get(context.Background(), req)

				So(err, ShouldNotBeNil)
			})
		})

		Convey("when we have a valid request", func() {
			req := &paydayrpc.GetUserCampaignStatusesReq{
				UserId: userID,
			}

			Convey("when we fail to get the campaign channel status", func() {
				getter.On("Get", mock.Anything, userID).Return(nil, errors.New("WALRUS STRIKE"))

				Convey("we should return an error", func() {
					_, err := server.Get(context.Background(), req)

					So(err, ShouldNotBeNil)
				})
			})

			Convey("when we do not find a campaign status for the user", func() {

				campaignStatus := &models.SponsoredCheermoteChannelStatus{
					ChannelID:     userID,
					CampaignID:    campaignID,
					BrandName:     "Save the Walrus",
					BrandImageURL: "https://save.walrus.com/walrus.png",
					OptedIn:       false,
					StartTime:     startTime,
					EndTime:       endTime,
				}

				getter.On("Get", mock.Anything, userID).Return([]*models.SponsoredCheermoteChannelStatus{campaignStatus}, nil)

				Convey("we should return that they are not opted in", func() {
					resp, err := server.Get(context.Background(), req)

					So(err, ShouldBeNil)
					So(resp, ShouldNotBeNil)
					So(resp.CampaignStatuses[0].OptedIn, ShouldBeFalse)
				})
			})

			Convey("when we find a campaign status for the user", func() {
				campaignStatus := &models.SponsoredCheermoteChannelStatus{
					ChannelID:     userID,
					CampaignID:    campaignID,
					BrandName:     "Save the Walrus",
					BrandImageURL: "https://save.walrus.com/walrus.png",
					OptedIn:       true,
					StartTime:     startTime,
					EndTime:       endTime,
				}

				getter.On("Get", mock.Anything, userID).Return([]*models.SponsoredCheermoteChannelStatus{campaignStatus}, nil)

				Convey("we should return the campaign status for the user", func() {
					resp, err := server.Get(context.Background(), req)

					So(err, ShouldBeNil)
					So(resp, ShouldNotBeNil)
					So(resp.CampaignStatuses[0].OptedIn, ShouldBeTrue)
				})
			})
		})
	})
}
