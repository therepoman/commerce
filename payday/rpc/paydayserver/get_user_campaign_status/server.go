package get_user_campaign_status

import (
	"context"
	"time"

	log "code.justin.tv/commerce/logrus"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"code.justin.tv/commerce/payday/sponsored_cheermote/channel_status"
	"github.com/golang/protobuf/ptypes"
	"github.com/twitchtv/twirp"
)

type Server interface {
	Get(ctx context.Context, req *paydayrpc.GetUserCampaignStatusesReq) (*paydayrpc.GetUserCampaignStatusesResp, error)
}

type ServerImpl struct {
	Getter channel_status.Getter `inject:"channelStatusGetter"`
}

func (s *ServerImpl) Get(ctx context.Context, req *paydayrpc.GetUserCampaignStatusesReq) (*paydayrpc.GetUserCampaignStatusesResp, error) {
	ctx, cancel := context.WithTimeout(ctx, 50*time.Millisecond)
	defer cancel()

	if req == nil {
		return nil, twirp.InvalidArgumentError("UserId", "cannot have empty request")
	}
	if req.UserId == "" {
		return nil, twirp.InvalidArgumentError("UserId", "cannot have empty user id")
	}

	campaignStatuses, err := s.Getter.Get(ctx, req.UserId)
	if err != nil {
		log.WithError(err).WithField("userID", req.UserId).Error("could not fetch campaign channel status for user")
		return nil, twirp.InternalErrorWith(err)
	}

	respStatuses := make([]*paydayrpc.UserCampaignStatus, 0)
	for _, status := range campaignStatuses {
		protoStartTime, _ := ptypes.TimestampProto(status.StartTime)
		protoEndTime, _ := ptypes.TimestampProto(status.EndTime)

		respStatuses = append(respStatuses, &paydayrpc.UserCampaignStatus{
			CampaignId:                status.CampaignID,
			OptedIn:                   status.OptedIn,
			BrandName:                 status.BrandName,
			BrandImageUrl:             status.BrandImageURL,
			SponsoredAmountThresholds: status.SponsoredAmountThresholds,
			StartTime:                 protoStartTime,
			EndTime:                   protoEndTime,
		})
	}

	return &paydayrpc.GetUserCampaignStatusesResp{
		CampaignStatuses: respStatuses,
	}, nil
}
