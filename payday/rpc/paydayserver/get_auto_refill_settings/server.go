package get_auto_refill_settings

import (
	"context"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/cache"
	"code.justin.tv/commerce/payday/refill"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"github.com/twitchtv/twirp"
)

type Server interface {
	Get(ctx context.Context, req *paydayrpc.GetAutoRefillSettingsReq) (*paydayrpc.GetAutoRefillSettingsResp, error)
}

type ServerImpl struct {
	Refill  refill.Refill       `inject:""`
	Balance cache.IBalanceCache `inject:""`
}

func (s *ServerImpl) Get(ctx context.Context, req *paydayrpc.GetAutoRefillSettingsReq) (settingsReq *paydayrpc.GetAutoRefillSettingsResp, err error) {
	if req == nil {
		return nil, twirp.InternalError("cannot have empty request")
	}

	if req.UserId == "" {
		return nil, twirp.InvalidArgumentError("UserId", "cannot have empty user id")
	}

	profile, err := s.Refill.Get(ctx, req.UserId)

	if err != nil {
		return nil, err
	}
	if profile == nil {
		// todo: is this okay or do we need to return the most recent dead profile?
		return &paydayrpc.GetAutoRefillSettingsResp{
			Settings: &paydayrpc.AutoRefillSettings{
				Enabled: false,
			},
		}, nil
	} else {
		go s.handleRefill(context.Background(), req.UserId)

		enabled := profile.State == refill.EnabledState
		return &paydayrpc.GetAutoRefillSettingsResp{
			Settings: &paydayrpc.AutoRefillSettings{
				Enabled:                    enabled,
				Threshold:                  profile.Threshold,
				Quantity:                   profile.SKUQuantity,
				ChargeInstrumentId:         profile.ChargeInstrumentId,
				ChargeInstrumentCategory:   profile.ChargeInstrumentCategory,
				OfferId:                    profile.OfferId,
				IsEmailNotificationEnabled: profile.IsEmailNotificationEnabled,
				Id:                         profile.Id,
				CreatedAt:                  profile.CreatedAt,
				Tpl:                        profile.Tpl,
			},
		}, nil
	}
}

func (s *ServerImpl) handleRefill(ctx context.Context, userId string) {
	err := s.Refill.CheckForProfileAndSendNotificationIfNeeded(ctx, userId)
	if err != nil {
		log.WithError(err).Error("Error attempting to handle users refill process")
	}
}
