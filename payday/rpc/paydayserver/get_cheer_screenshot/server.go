package get_cheer_screenshot

import (
	"context"
	"time"

	"code.justin.tv/commerce/payday/dynamo/cheer_images"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"github.com/twitchtv/twirp"
)

type Server interface {
	Get(ctx context.Context, req *paydayrpc.GetCheerScreenshotFromIdReq) (*paydayrpc.GetCheerScreenshotFromIdResp, error)
}

type ServerImpl struct {
	Dao cheer_images.ChannelImagesDao `inject:""`
}

// Update will create or replace the image assets for a channel's specified cheermote tier using images which have already been uploaded via Mako's animated emote upload flow
func (s *ServerImpl) Get(ctx context.Context, req *paydayrpc.GetCheerScreenshotFromIdReq) (*paydayrpc.GetCheerScreenshotFromIdResp, error) {
	_, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()

	if req == nil {
		return nil, paydayrpc.NewClientError(twirp.RequiredArgumentError("request"), paydayrpc.ErrCodeMissingRequiredAgrument)
	} else if req.ImageId == "" {
		return nil, paydayrpc.NewClientError(twirp.RequiredArgumentError("image_id"), paydayrpc.ErrCodeMissingRequiredAgrument)
	}

	image, err := s.Dao.GetImageByImageId(req.ImageId)

	if err != nil {
		return nil, twirp.InternalError(err.Error())
	}

	var imageUrl string

	if image == nil {
		return nil, twirp.NotFoundError("Image ID not found")
	} else {
		imageUrl = image.ImageURL
	}

	return &paydayrpc.GetCheerScreenshotFromIdResp{ImageUrl: imageUrl}, nil
}
