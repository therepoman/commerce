package moderate_voldemote

import (
	"context"

	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
)

type Server interface {
	Moderate(ctx context.Context, req *paydayrpc.ModerateVoldemoteReq) (*paydayrpc.ModerateVoldemoteResp, error)
}

type ServerImpl struct{}

func (s *ServerImpl) Moderate(ctx context.Context, req *paydayrpc.ModerateVoldemoteReq) (*paydayrpc.ModerateVoldemoteResp, error) {
	return &paydayrpc.ModerateVoldemoteResp{}, nil
}
