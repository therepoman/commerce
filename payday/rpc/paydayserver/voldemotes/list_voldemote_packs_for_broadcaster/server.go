package list_voldemote_packs_for_broadcaster

import (
	"context"

	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
)

type Server interface {
	List(ctx context.Context, req *paydayrpc.ListVoldemotePacksForBroadcasterReq) (*paydayrpc.ListVoldemotePacksForBroadcasterResp, error)
}

type ServerImpl struct {
}

func (s *ServerImpl) List(ctx context.Context, req *paydayrpc.ListVoldemotePacksForBroadcasterReq) (*paydayrpc.ListVoldemotePacksForBroadcasterResp, error) {
	return &paydayrpc.ListVoldemotePacksForBroadcasterResp{}, nil
}
