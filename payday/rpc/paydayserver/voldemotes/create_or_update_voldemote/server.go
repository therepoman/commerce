package create_or_update_voldemote

import (
	"context"

	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
)

type Server interface {
	CreateOrUpdate(ctx context.Context, req *paydayrpc.CreateOrUpdateVoldemoteReq) (*paydayrpc.CreateOrUpdateVoldemoteResp, error)
}

type ServerImpl struct{}

func (s *ServerImpl) CreateOrUpdate(ctx context.Context, req *paydayrpc.CreateOrUpdateVoldemoteReq) (*paydayrpc.CreateOrUpdateVoldemoteResp, error) {
	return &paydayrpc.CreateOrUpdateVoldemoteResp{
		Voldemote: &paydayrpc.Voldemote{
			Id:       "voldemote-id-1",
			OwnerId:  "channel-id-123",
			IsGlobal: false,
			TextCode: "flomicVoldLove",
			State:    paydayrpc.VoldemoteState_ACTIVE,
			ImageSet: &paydayrpc.ImageSet{
				Image1X: "http://1x-url",
				Image2X: "http://2x-url",
				Image4X: "http://4x-url",
			},
		},
	}, nil
}
