package create_or_update_voldemote_pack

import (
	"context"

	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
)

type Server interface {
	CreateOrUpdate(ctx context.Context, req *paydayrpc.CreateOrUpdateVoldemotePackReq) (*paydayrpc.CreateOrUpdateVoldemotePackResp, error)
}

type ServerImpl struct {
}

func (s *ServerImpl) CreateOrUpdate(ctx context.Context, req *paydayrpc.CreateOrUpdateVoldemotePackReq) (*paydayrpc.CreateOrUpdateVoldemotePackResp, error) {
	return &paydayrpc.CreateOrUpdateVoldemotePackResp{}, nil
}
