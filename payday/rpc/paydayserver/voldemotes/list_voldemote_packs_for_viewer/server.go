package list_voldemote_packs_for_viewer

import (
	"context"

	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
)

type Server interface {
	List(ctx context.Context, req *paydayrpc.ListVoldemotePacksForViewerReq) (*paydayrpc.ListVoldemotePacksForViewerResp, error)
}

type ServerImpl struct{}

func (s *ServerImpl) List(ctx context.Context, req *paydayrpc.ListVoldemotePacksForViewerReq) (*paydayrpc.ListVoldemotePacksForViewerResp, error) {
	return &paydayrpc.ListVoldemotePacksForViewerResp{
		VoldemotePacks: []*paydayrpc.VoldemotePack{
			{
				VoldemotePackId:      "pack-id-1",
				VoldemotePackOwnerId: "channel-id-123",
				Slots: []*paydayrpc.VoldemotePackSlot{
					{
						SlotIndex: 0,
						Voldemote: &paydayrpc.Voldemote{
							Id:       "voldemote-id-1",
							OwnerId:  "channel-id-123",
							IsGlobal: false,
							TextCode: "flomicVoldLove",
							State:    paydayrpc.VoldemoteState_ACTIVE,
							ImageSet: &paydayrpc.ImageSet{
								Image1X: "http://1x-url",
								Image2X: "http://2x-url",
								Image4X: "http://4x-url",
							},
						},
					},
				},
			},
		},
	}, nil
}
