package remove_voldemote

import (
	"context"

	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
)

type Server interface {
	Remove(ctx context.Context, req *paydayrpc.RemoveVoldemoteReq) (*paydayrpc.RemoveVoldemoteResp, error)
}

type ServerImpl struct{}

func (s *ServerImpl) Remove(ctx context.Context, req *paydayrpc.RemoveVoldemoteReq) (*paydayrpc.RemoveVoldemoteResp, error) {
	return &paydayrpc.RemoveVoldemoteResp{}, nil
}
