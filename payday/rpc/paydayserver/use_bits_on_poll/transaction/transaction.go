package transaction

import (
	"context"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	"code.justin.tv/commerce/payday/api/constants"
	pachinko_client "code.justin.tv/commerce/payday/clients/pachinko"
	"code.justin.tv/commerce/payday/clients/pantheon"
	"code.justin.tv/commerce/payday/metrics"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"code.justin.tv/commerce/payday/rpc/paydayserver/use_bits_on_poll/transaction/dynamo"
	"code.justin.tv/commerce/payday/rpc/paydayserver/use_bits_on_poll/transaction/model"
	"code.justin.tv/commerce/payday/rpc/paydayserver/use_bits_on_poll/transaction/pachinko"
	"github.com/twitchtv/twirp"
)

type FabsResponse struct {
	UpdatedBalance int
	Error          error
}

type PachinkoResponse struct {
	UpdatedBalance int
	Error          error
}

type Transactor interface {
	Execute(ctx context.Context, tx model.Transaction) (*paydayrpc.UseBitsOnPollResp, error)
}

func NewTransactor() Transactor {
	return &transactor{}
}

type transactor struct {
	Saver              dynamo.Saver                    `inject:""`
	PachinkoTransactor pachinko.Transactor             `inject:"pachinko_transactor"`
	Statter            metrics.Statter                 `inject:""`
	Pantheon           pantheon.IPantheonClientWrapper `inject:""`
}

func (t *transactor) Execute(ctx context.Context, tx model.Transaction) (*paydayrpc.UseBitsOnPollResp, error) {
	err := t.Saver.CreateTransaction(ctx, tx)
	if err != nil {
		return nil, twirp.InternalError("Downstream dependence error using bits on poll")
	}

	leaderboardEntry, err := t.Pantheon.GetLeaderboard(ctx, pantheon.PantheonBitsUsageDomain, pantheonrpc.TimeUnit_ALLTIME, tx.ChannelID, tx.UserID, 1, 1, false)
	if err != nil {
		log.WithError(err).Error("failed to fetch leaderboard for transaction creation on using bits on poll")
		return nil, twirp.InternalError(constants.DownstreamDependencyError)
	}
	totalBitsToBroadcasterBefore := int64(0)
	if leaderboardEntry != nil && leaderboardEntry.GetEntryContext() != nil && leaderboardEntry.GetEntryContext().GetEntry() != nil {
		totalBitsToBroadcasterBefore = leaderboardEntry.GetEntryContext().GetEntry().Score
	}

	updatedBalance, err := t.PachinkoTransactor.Execute(ctx, tx)
	if err != nil {
		if err == pachinko_client.ErrInsufficientBalance {
			return nil, paydayrpc.NewClientError(twirp.NewError(twirp.FailedPrecondition, "insufficient bits balance"), paydayrpc.ClientError{
				ErrorCode: paydayrpc.ErrCodeInsufficientBitsBalance,
			})
		}
		return nil, err
	}

	go func() {
		err := t.Saver.UpdateTransaction(ctx, tx, int64(updatedBalance), int64(totalBitsToBroadcasterBefore)+int64(tx.Bits))
		if err != nil {
			log.WithFields(log.Fields{
				"transactionID":             tx.VoteID,
				"totalBitsToBroadcaster":    int64(totalBitsToBroadcasterBefore) + int64(tx.Bits),
				"totalBitsAfterTransaction": int64(updatedBalance),
			}).WithError(err).Error("failed to update balance total and total bits to broadcaster on poll usage")
		}
	}()

	return &paydayrpc.UseBitsOnPollResp{
		Balance: int32(updatedBalance),
	}, nil
}
