package pachinko

import (
	"context"
	"testing"

	"code.justin.tv/commerce/gogogadget/random"
	pachinko_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/pachinko"
	"code.justin.tv/commerce/payday/rpc/paydayserver/use_bits_on_poll/transaction/model"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
)

func TestPachinkoTransactor_Execute(t *testing.T) {
	Convey("given a pachinko transactor", t, func() {
		pachinkoClient := new(pachinko_mock.PachinkoClient)

		transactor := &transactor{
			Pachinko: pachinkoClient,
		}

		ctx := context.Background()

		transactionID := random.String(32)
		tuid := random.String(16)
		amount := int32(20)

		Convey("when pachinko fails", func() {
			pachinkoClient.On("RemoveBits", ctx, transactionID, tuid, int(amount)).Return(0, errors.New("test error"))
			_, err := transactor.Execute(ctx, model.Transaction{
				VoteID: transactionID,
				UserID: tuid,
				Bits:   amount,
			})
			So(err, ShouldNotBeNil)
		})

		Convey("when pachinko succeeds", func() {
			pachinkoClient.On("RemoveBits", ctx, transactionID, tuid, int(amount)).Return(300, nil)
			resp, err := transactor.Execute(ctx, model.Transaction{
				VoteID: transactionID,
				UserID: tuid,
				Bits:   amount,
			})
			So(err, ShouldBeNil)
			So(resp, ShouldEqual, int32(300))
		})
	})
}
