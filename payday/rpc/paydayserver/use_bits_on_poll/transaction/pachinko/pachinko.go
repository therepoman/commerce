package pachinko

import (
	"context"

	"code.justin.tv/commerce/payday/clients/pachinko"
	"code.justin.tv/commerce/payday/rpc/paydayserver/use_bits_on_poll/transaction/model"
)

type Transactor interface {
	Execute(ctx context.Context, tx model.Transaction) (int32, error)
}

func NewTransactor() Transactor {
	return &transactor{}
}

type transactor struct {
	Pachinko pachinko.PachinkoClient `inject:""`
}

func (t *transactor) Execute(ctx context.Context, tx model.Transaction) (int32, error) {
	newBalance, err := t.Pachinko.RemoveBits(ctx, tx.VoteID, tx.UserID, int(tx.Bits))
	return int32(newBalance), err
}
