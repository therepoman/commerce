package transaction

import (
	"context"
	"testing"

	"code.justin.tv/commerce/gogogadget/random"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	"code.justin.tv/commerce/payday/clients/pantheon"
	pantheon_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/pantheon"
	metrics_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/metrics"
	dynamo_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/rpc/paydayserver/use_bits_on_poll/transaction/dynamo"
	pachinko_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/rpc/paydayserver/use_bits_on_poll/transaction/pachinko"
	"code.justin.tv/commerce/payday/rpc/paydayserver/use_bits_on_poll/transaction/model"
	"code.justin.tv/commerce/payday/rpc/utils/test"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
	"github.com/twitchtv/twirp"
)

func TestTransactor_Execute(t *testing.T) {
	Convey("given a transactor", t, func() {
		pachinkoTransactor := new(pachinko_mock.Transactor)
		statter := new(metrics_mock.Statter)
		dynamoSaver := new(dynamo_mock.Saver)
		pantheonClient := new(pantheon_mock.IPantheonClientWrapper)

		t := transactor{
			PachinkoTransactor: pachinkoTransactor,
			Statter:            statter,
			Saver:              dynamoSaver,
			Pantheon:           pantheonClient,
		}

		ctx := context.Background()

		tx := model.Transaction{
			VoteID:    random.String(32),
			UserID:    random.String(16),
			ChannelID: random.String(16),
			PollID:    random.String(32),
			ChoiceID:  random.String(32),
		}

		newBalance := int32(322)

		Convey("when the transaction create fails", func() {
			dynamoSaver.On("CreateTransaction", mock.Anything, tx, mock.Anything, mock.Anything).Return(errors.New("test-error"))

			_, err := t.Execute(ctx, tx)
			So(err, ShouldNotBeNil)
		})

		Convey("when the transaction create succeeds", func() {
			dynamoSaver.On("CreateTransaction", mock.Anything, tx, mock.Anything, mock.Anything).Return(nil)

			Convey("when the leaderboard fetch fails", func() {
				pantheonClient.On("GetLeaderboard", mock.Anything, pantheon.PantheonBitsUsageDomain, pantheonrpc.TimeUnit_ALLTIME, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("WALRUS STRIKE"))

				_, err := t.Execute(ctx, tx)
				test.AssertTwirpErrorCode(err, twirp.Internal)
			})

			Convey("when the leaderboard fetch succeeds", func() {
				pantheonClient.On("GetLeaderboard", mock.Anything, pantheon.PantheonBitsUsageDomain, pantheonrpc.TimeUnit_ALLTIME, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(&pantheonrpc.GetLeaderboardResp{
					EntryContext: &pantheonrpc.EntryContext{
						Entry: &pantheonrpc.LeaderboardEntry{
							EntryKey: tx.UserID,
							Score:    int64(322),
						},
					},
				}, nil)

				Convey("on successful request", func() {
					pachinkoTransactor.On("Execute", mock.Anything, tx).Return(int32(322), nil)
					dynamoSaver.On("UpdateTransaction", mock.Anything, tx, mock.Anything, mock.Anything).Return(nil)

					resp, err := t.Execute(ctx, tx)
					So(err, ShouldBeNil)
					So(resp, ShouldNotBeNil)
					So(resp.Balance, ShouldEqual, newBalance)
				})

				Convey("on failed request", func() {
					pachinkoTransactor.On("Execute", mock.Anything, tx).Return(int32(0), errors.New("WALRUS STRIKE"))

					_, err := t.Execute(ctx, tx)
					So(err, ShouldNotBeNil)
				})
			})
		})
	})
}
