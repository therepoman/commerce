package dynamo

import (
	"context"
	"time"

	"code.justin.tv/commerce/gogogadget/pointers"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/hystrix"
	cmds "code.justin.tv/commerce/payday/hystrix"
	"code.justin.tv/commerce/payday/middleware"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/oauth"
	"code.justin.tv/commerce/payday/rpc/paydayserver/use_bits_on_poll/transaction/model"
)

type Saver interface {
	CreateTransaction(ctx context.Context, tx model.Transaction) error
	UpdateTransaction(ctx context.Context, tx model.Transaction, totalBitsAfterTransaction, totalBitsToBroadcaster int64) error
}

type saver struct {
	TransactionsDao dynamo.ITransactionsDao `inject:""`
}

func NewSaver() Saver {
	return &saver{}
}

func (s *saver) CreateTransaction(ctx context.Context, tx model.Transaction) error {
	timeOfEvent := time.Now()
	clientID := middleware.GetClientID(ctx)
	platform, _ := oauth.GetClient(clientID)

	dynamoTx := dynamo.Transaction{
		TransactionId:   tx.VoteID,
		ChannelId:       pointers.StringP(tx.ChannelID),
		UserId:          pointers.StringP(tx.UserID),
		NumberOfBits:    pointers.Int64P(int64(tx.Bits)),
		TransactionType: pointers.StringP(models.UseBitsOnPollTransactionType),
		ClientApp:       pointers.StringP(platform),
		TimeOfEvent:     timeOfEvent,
		PollId:          pointers.StringP(tx.PollID),
		PollChoiceId:    pointers.StringP(tx.ChoiceID),
	}

	err := hystrix.Do(cmds.CreateNewTransactionCommand, func() error {
		return s.TransactionsDao.CreateNew(&dynamoTx)
	}, nil)

	if err != nil {
		log.WithError(err).WithField("transaction_id", tx.VoteID).Error("failed to create poll transaction record to dynamo")
		return err
	}
	return nil
}

func (s *saver) UpdateTransaction(ctx context.Context, tx model.Transaction, totalBitsAfterTransaction, totalBitsToBroadcaster int64) error {
	dynamoTx := dynamo.Transaction{
		TransactionId:             tx.VoteID,
		TotalBitsAfterTransaction: pointers.Int64P(totalBitsAfterTransaction),
		TotalBitsToBroadcaster:    pointers.Int64P(totalBitsToBroadcaster),
	}

	err := hystrix.Do(cmds.UpdateTransactionsCommand, func() error {
		return s.TransactionsDao.Update(&dynamoTx)
	}, nil)

	if err != nil {
		log.WithError(err).WithField("transaction_id", tx.VoteID).Error("failed to update poll transaction record to dynamo")
		return err
	}
	return nil
}
