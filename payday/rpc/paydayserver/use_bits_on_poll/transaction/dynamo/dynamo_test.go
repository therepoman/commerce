package dynamo

import (
	"context"
	"errors"
	"testing"

	dynamo_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/rpc/paydayserver/use_bits_on_poll/transaction/model"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestSaver_CreateTransaction(t *testing.T) {
	Convey("Given a dynamo transaction saver", t, func() {
		transactionsDAO := new(dynamo_mock.ITransactionsDao)

		saver := &saver{
			TransactionsDao: transactionsDAO,
		}

		tx := model.Transaction{
			VoteID: "test-vote-id",
		}

		Convey("Errors when dao errors", func() {
			transactionsDAO.On("CreateNew", mock.Anything).Return(errors.New("test-error"))
			err := saver.CreateTransaction(context.Background(), tx)
			So(err, ShouldNotBeNil)
		})

		Convey("Succeeds when dao succeeds", func() {
			transactionsDAO.On("CreateNew", mock.Anything).Return(nil)
			err := saver.CreateTransaction(context.Background(), tx)
			So(err, ShouldBeNil)
		})
	})
}

func TestSaver_UpdateTransaction(t *testing.T) {
	Convey("Given a dynamo transaction saver", t, func() {
		transactionsDAO := new(dynamo_mock.ITransactionsDao)

		saver := &saver{
			TransactionsDao: transactionsDAO,
		}

		tx := model.Transaction{
			VoteID: "test-vote-id",
		}

		Convey("Errors when dao errors", func() {
			transactionsDAO.On("Update", mock.Anything).Return(errors.New("test-error"))
			err := saver.UpdateTransaction(context.Background(), tx, int64(322), int64(322))
			So(err, ShouldNotBeNil)
		})

		Convey("Succeeds when dao succeeds", func() {
			transactionsDAO.On("Update", mock.Anything).Return(nil)
			err := saver.UpdateTransaction(context.Background(), tx, int64(322), int64(322))
			So(err, ShouldBeNil)
		})
	})
}
