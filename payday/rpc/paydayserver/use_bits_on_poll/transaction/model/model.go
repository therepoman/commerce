package model

type Transaction struct {
	VoteID    string
	UserID    string
	ChannelID string
	Bits      int32
	PollID    string
	ChoiceID  string
}
