package use_bits_on_poll

import (
	"context"
	"fmt"
	"time"

	"code.justin.tv/commerce/gogogadget/audit"
	"code.justin.tv/commerce/gogogadget/strings"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/clients/cloudwatchlogger"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"code.justin.tv/commerce/payday/rpc/paydayserver/use_bits_on_poll/transaction"
	"code.justin.tv/commerce/payday/rpc/paydayserver/use_bits_on_poll/transaction/model"
	"github.com/twitchtv/twirp"
)

const (
	timeoutDuration = 5 * time.Second
)

type Server interface {
	Use(ctx context.Context, req *paydayrpc.UseBitsOnPollReq) (*paydayrpc.UseBitsOnPollResp, error)
}

type ServerImpl struct {
	Transactor  transaction.Transactor            `inject:""`
	AuditLogger cloudwatchlogger.CloudWatchLogger `inject:""`
}

func (s *ServerImpl) Use(ctx context.Context, req *paydayrpc.UseBitsOnPollReq) (*paydayrpc.UseBitsOnPollResp, error) {
	ctx, cancel := context.WithTimeout(ctx, timeoutDuration)
	defer cancel()

	if req == nil {
		return nil, twirp.InternalError("Received nil request")
	}
	if strings.Blank(req.VoteId) {
		return nil, twirp.InvalidArgumentError("voteID", "VoteID should not be blank")
	}
	if strings.Blank(req.UserId) {
		return nil, twirp.InvalidArgumentError("userID", "UserID should not be blank")
	}
	if strings.Blank(req.ChannelId) {
		return nil, twirp.InvalidArgumentError("channelID", "ChannelID should not be blank")
	}
	if strings.Blank(req.PollId) {
		return nil, twirp.InvalidArgumentError("pollID", "PollID should not be blank")
	}
	if strings.Blank(req.ChoiceId) {
		return nil, twirp.InvalidArgumentError("choiceID", "ChoiceID should not be blank")
	}
	if req.BitsAmount <= 0 {
		return nil, twirp.InvalidArgumentError("bitsAmount", "BitsAmount must be positive")
	}

	apiName := "UseBitsOnPoll"
	msg, err := audit.CreateAccessLogMsg(ctx, apiName, *req)
	if err != nil {
		log.WithError(err).Error(fmt.Sprintf("failed to generate access log msg in %s", apiName))
	} else {
		err = s.AuditLogger.Send(ctx, msg)
		if err != nil {
			log.WithError(err).Warn(fmt.Sprintf("error while sending audit log in %s", apiName))
		}
	}

	tx := model.Transaction{
		VoteID:    req.VoteId,
		UserID:    req.UserId,
		ChannelID: req.ChannelId,
		Bits:      req.BitsAmount,
		PollID:    req.PollId,
		ChoiceID:  req.ChoiceId,
	}

	resp, err := s.Transactor.Execute(ctx, tx)
	if err != nil {
		log.WithField("transaction_id", req.VoteId).WithError(err).Error("error executing transaction for UseBitsOnPoll request")
		return nil, err
	}

	return resp, nil
}
