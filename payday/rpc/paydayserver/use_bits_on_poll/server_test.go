package use_bits_on_poll

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/gogogadget/random"
	"code.justin.tv/commerce/payday/clients/cloudwatchlogger"
	transaction_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/rpc/paydayserver/use_bits_on_poll/transaction"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestServer_UseBitsOnPoll(t *testing.T) {
	Convey("Given a server", t, func() {
		transactor := new(transaction_mock.Transactor)
		cwlogger := cloudwatchlogger.NewCloudWatchLogNoopClient()
		server := &ServerImpl{
			Transactor:  transactor,
			AuditLogger: cwlogger,
		}

		req := &paydayrpc.UseBitsOnPollReq{
			VoteId:     random.String(32),
			UserId:     random.String(16),
			ChannelId:  random.String(16),
			BitsAmount: int32(random.Int(1, 10000)),
			PollId:     random.String(16),
			ChoiceId:   random.String(16),
		}

		Convey("Errors when request is nil", func() {
			_, err := server.Use(context.Background(), nil)
			So(err, ShouldNotBeNil)
		})

		Convey("Errors when voteID is blank", func() {
			req.VoteId = ""
			_, err := server.Use(context.Background(), req)
			So(err, ShouldNotBeNil)
		})

		Convey("Errors when userID is blank", func() {
			req.UserId = ""
			_, err := server.Use(context.Background(), req)
			So(err, ShouldNotBeNil)
		})

		Convey("Errors when channelID is blank", func() {
			req.ChannelId = ""
			_, err := server.Use(context.Background(), req)
			So(err, ShouldNotBeNil)
		})

		Convey("Errors when pollID is blank", func() {
			req.PollId = ""
			_, err := server.Use(context.Background(), req)
			So(err, ShouldNotBeNil)
		})

		Convey("Errors when choiceID is blank", func() {
			req.ChoiceId = ""
			_, err := server.Use(context.Background(), req)
			So(err, ShouldNotBeNil)
		})

		Convey("Errors when bitsAmount is zero", func() {
			req.BitsAmount = 0
			_, err := server.Use(context.Background(), req)
			So(err, ShouldNotBeNil)
		})

		Convey("Errors when transactor errors", func() {
			transactor.On("Execute", mock.Anything, mock.Anything).Return(nil, errors.New("test-error"))
			_, err := server.Use(context.Background(), req)
			So(err, ShouldNotBeNil)
		})

		Convey("Succeeds when transactor succeeds", func() {
			resp := &paydayrpc.UseBitsOnPollResp{}
			transactor.On("Execute", mock.Anything, mock.Anything).Return(resp, nil)
			actualResp, err := server.Use(context.Background(), req)
			So(err, ShouldBeNil)
			So(actualResp, ShouldEqual, resp)
		})
	})
}
