package get_user_bits_for_campaign

import (
	"context"
	"testing"

	"github.com/stretchr/testify/mock"

	user_campaign_status_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sponsored_cheermote/user_campaign_status"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
)

func TestServer_GetUserBitsForCampaign(t *testing.T) {
	Convey("given a Get API", t, func() {
		getter := new(user_campaign_status_mock.Getter)

		api := &ServerImpl{
			Getter: getter,
		}

		userID := "123123123"
		campaignID := "walrus"

		Convey("when the request is nil", func() {
			var req *paydayrpc.GetUserBitsForCampaignReq
			_, err := api.Get(context.Background(), req)

			Convey("should return an error", func() {
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when the user ID is empty", func() {
			req := &paydayrpc.GetUserBitsForCampaignReq{}
			_, err := api.Get(context.Background(), req)

			Convey("should return an error", func() {
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when the campaign ID is empty", func() {
			req := &paydayrpc.GetUserBitsForCampaignReq{
				UserId: userID,
			}
			_, err := api.Get(context.Background(), req)

			Convey("should return an error", func() {
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when we have a valid request", func() {
			req := &paydayrpc.GetUserBitsForCampaignReq{
				UserId:     userID,
				CampaignId: campaignID,
			}

			Convey("when there's an error returned from the getter", func() {
				getter.On("Get", mock.Anything, campaignID, userID).Return(int64(0), errors.New("WALRUS STRIKE"))

				_, err := api.Get(context.Background(), req)

				Convey("should return an error", func() {
					So(err, ShouldNotBeNil)
				})
			})

			Convey("when the getter succeeds", func() {
				getter.On("Get", mock.Anything, campaignID, userID).Return(int64(322), nil)

				resp, err := api.Get(context.Background(), req)

				Convey("should return an error", func() {
					So(err, ShouldBeNil)
					So(resp, ShouldNotBeNil)
					So(resp.BitsUsed, ShouldEqual, int64(322))
				})
			})
		})
	})
}
