package get_user_bits_for_campaign

import (
	"context"

	log "code.justin.tv/commerce/logrus"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"code.justin.tv/commerce/payday/sponsored_cheermote/user_campaign_status"
	"github.com/twitchtv/twirp"
)

type Server interface {
	Get(ctx context.Context, req *paydayrpc.GetUserBitsForCampaignReq) (*paydayrpc.GetUserBitsForCampaignResp, error)
}

type ServerImpl struct {
	Getter user_campaign_status.Getter `inject:""`
}

func (s *ServerImpl) Get(ctx context.Context, req *paydayrpc.GetUserBitsForCampaignReq) (*paydayrpc.GetUserBitsForCampaignResp, error) {
	if req == nil {
		return nil, twirp.InvalidArgumentError("UserId", "cannot have empty request")
	}
	if req.UserId == "" {
		return nil, twirp.InvalidArgumentError("UserId", "cannot have empty user id")
	}
	if req.CampaignId == "" {
		return nil, twirp.InvalidArgumentError("CampaignId", "cannot have empty campaign id")
	}

	userAmount, err := s.Getter.Get(ctx, req.CampaignId, req.UserId)

	if err != nil {
		log.WithError(err).WithFields(log.Fields{"userID": req.UserId, "campaignId": req.CampaignId}).Error("could not fetch user amount for campaign")
		return nil, twirp.InternalErrorWith(err)
	}

	return &paydayrpc.GetUserBitsForCampaignResp{
		BitsUsed: userAmount,
	}, nil
}
