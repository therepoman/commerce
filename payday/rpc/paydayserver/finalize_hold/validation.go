package finalize_hold

import (
	"context"
	"fmt"

	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/payday/holds"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"github.com/twitchtv/twirp"
)

type Validator interface {
	IsValid(ctx context.Context, req *paydayrpc.FinalizeHoldReq) (*paydayrpc.Hold, error)
}

func NewValidator() Validator {
	return &validator{}
}

type validator struct {
	Getter holds.Getter `inject:"holdsGetter"`
}

var allowedTransactionTypes = map[paydayrpc.TransactionType]bool{
	paydayrpc.TransactionType_UseBitsInExtensionChallenge:    true,
	paydayrpc.TransactionType_GiveBitsToBroadcasterChallenge: true,
}

func (v *validator) IsValid(ctx context.Context, req *paydayrpc.FinalizeHoldReq) (*paydayrpc.Hold, error) {
	if req == nil {
		return nil, twirp.InvalidArgumentError("body", "cannot be blank")
	}

	if strings.Blank(req.UserId) {
		return nil, twirp.InvalidArgumentError("user_id", "cannot be blank")
	}

	if strings.Blank(req.HoldId) {
		return nil, twirp.InvalidArgumentError("hold_id", "cannot be blank")
	}

	if len(req.BitsUsageAttributions) < 1 {
		return nil, twirp.NewError(twirp.InvalidArgument, fmt.Sprintf("The BitsUsageAttributions field cannot be empty"))
	}

	sumOfBitsToUser := int64(0)
	sumOfRewardAttribution := int64(0)
	// check BitsUsageAttributions to make sure it's populated correctly
	for recipient, data := range req.BitsUsageAttributions {
		if recipient == req.UserId {
			return nil, twirp.NewError(twirp.InvalidArgument, fmt.Sprintf("recipient ID cannot be the same as the requesting user ID [%s]", recipient))
		} else if data.RewardAttribution == 0 && data.BitsRevenueAttribution == 0 {
			return nil, twirp.NewError(twirp.InvalidArgument, fmt.Sprintf("Both RewardAttribution and BitsToUserAmount user for ID [%s] cannot be blank", recipient))
		}
		sumOfBitsToUser += data.BitsRevenueAttribution
		sumOfRewardAttribution += data.RewardAttribution
	}

	// check the status of the hold
	hold, err := v.Getter.Get(ctx, req.UserId, req.HoldId)
	if err != nil {
		return nil, twirp.InternalErrorWith(err)
	}

	if hold == nil {
		return nil, twirp.NotFoundError(fmt.Sprintf("could not find hold with ID [%s]", req.HoldId))
	}

	if sumOfRewardAttribution != hold.Amount {
		return nil, twirp.NewError(twirp.InvalidArgument, fmt.Sprintf("Total Bits Reward Attribution [sum amount: %d] must equal hold amount [%d]", sumOfRewardAttribution, hold.Amount))
	}

	if (sumOfBitsToUser != 0 && req.IgnorePayout) || (!req.IgnorePayout && sumOfBitsToUser != hold.Amount) {
		return nil, twirp.NewError(twirp.InvalidArgument, fmt.Sprintf("Total Bits Revenue Attribution [sum amount: %d] must equal hold amount [%d] if IgnorePayout false.", sumOfRewardAttribution, hold.Amount))
	}

	_, txTypeOK := allowedTransactionTypes[req.TransactionType]
	if !txTypeOK {
		txTypeName := paydayrpc.TransactionType_name[int32(req.TransactionType)]
		return nil, twirp.NewError(twirp.InvalidArgument, fmt.Sprintf("transaction type not allowed [%s]", txTypeName))
	}

	return hold, nil
}
