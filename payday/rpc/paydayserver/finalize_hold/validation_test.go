package finalize_hold

import (
	"context"
	"errors"
	"testing"

	holds_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/holds"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"github.com/gofrs/uuid"
	. "github.com/smartystreets/goconvey/convey"
)

func TestValidator_IsValid(t *testing.T) {
	Convey("given a validator", t, func() {
		getter := new(holds_mock.Getter)
		validator := &validator{
			Getter: getter,
		}

		holdID := uuid.Must(uuid.NewV4()).String()
		transactionID := uuid.Must(uuid.NewV4()).String()
		userID := "user-id"

		req := &paydayrpc.FinalizeHoldReq{
			HoldId:          transactionID,
			UserId:          userID,
			TransactionType: paydayrpc.TransactionType_GiveBitsToBroadcasterChallenge,
			BitsUsageAttributions: map[string]*paydayrpc.BitsUsageAttribution{
				"recipient-id-1": {
					BitsRevenueAttribution: 10,
					RewardAttribution:      15,
				},
				"recipient-id-2": {
					BitsRevenueAttribution: 20,
					RewardAttribution:      15,
				},
				"recipient-id-3": {
					BitsRevenueAttribution: 30,
					RewardAttribution:      30,
				},
			},
			IgnorePayout: false,
		}

		badReq := &paydayrpc.FinalizeHoldReq{
			HoldId:          transactionID,
			UserId:          userID,
			TransactionType: paydayrpc.TransactionType_GiveBitsToBroadcasterChallenge,
			BitsUsageAttributions: map[string]*paydayrpc.BitsUsageAttribution{
				"recipient-id-1": {
					BitsRevenueAttribution: 10,
					RewardAttribution:      15,
				},
				"recipient-id-2": {
					BitsRevenueAttribution: 20,
					RewardAttribution:      15,
				},
				"recipient-id-3": {
					BitsRevenueAttribution: 30,
					RewardAttribution:      30,
				},
			},
			IgnorePayout: true,
		}

		badReq2 := &paydayrpc.FinalizeHoldReq{
			HoldId:          transactionID,
			UserId:          userID,
			TransactionType: paydayrpc.TransactionType_GiveBitsToBroadcasterChallenge,
			BitsUsageAttributions: map[string]*paydayrpc.BitsUsageAttribution{
				"recipient-id-1": {
					BitsRevenueAttribution: 0,
					RewardAttribution:      15,
				},
				"recipient-id-2": {
					BitsRevenueAttribution: 0,
					RewardAttribution:      15,
				},
				"recipient-id-3": {
					BitsRevenueAttribution: 0,
					RewardAttribution:      30,
				},
			},
			IgnorePayout: false,
		}

		badReq3 := &paydayrpc.FinalizeHoldReq{
			HoldId:          transactionID,
			UserId:          userID,
			TransactionType: paydayrpc.TransactionType_GiveBitsToBroadcasterChallenge,
			BitsUsageAttributions: map[string]*paydayrpc.BitsUsageAttribution{
				"recipient-id-1": {
					BitsRevenueAttribution: 20,
					RewardAttribution:      0,
				},
				"recipient-id-2": {
					BitsRevenueAttribution: 20,
					RewardAttribution:      0,
				},
				"recipient-id-3": {
					BitsRevenueAttribution: 20,
					RewardAttribution:      0,
				},
			},
			IgnorePayout: false,
		}

		hold := &paydayrpc.Hold{
			HoldId:      holdID,
			UserId:      userID,
			Amount:      60,
			IsFinalized: false,
			IsReleased:  false,
		}

		ctx := context.Background()

		Convey("with a valid request", func() {
			getter.On("Get", ctx, req.UserId, req.HoldId).Return(hold, nil)

			Convey("returns the hold to be finalized", func() {
				result, err := validator.IsValid(ctx, req)
				So(err, ShouldBeNil)
				So(result, ShouldEqual, hold)
			})
		})

		Convey("returns an error if", func() {
			getter.On("Get", ctx, req.UserId, req.HoldId).Return(hold, nil)

			Convey("request is nil", func() {
				req = nil
			})

			Convey("user ID is missing", func() {
				req.UserId = ""
			})

			Convey("hold ID is missing", func() {
				req.HoldId = ""
			})

			// need to fix this
			Convey("recipient is the same as the user", func() {
				req.BitsUsageAttributions["user-id"] = &paydayrpc.BitsUsageAttribution{}
			})

			Convey("hold cannot be found", func() {
				getter.ExpectedCalls = nil
				getter.On("Get", ctx, req.UserId, req.HoldId).Return(nil, nil)
			})

			Convey("hold getter fails", func() {
				getter.ExpectedCalls = nil
				getter.On("Get", ctx, req.UserId, req.HoldId).Return(nil, errors.New("error"))
			})

			_, err := validator.IsValid(ctx, req)
			So(err, ShouldNotBeNil)
		})

		Convey("returns a validation error if", func() {
			getter.On("Get", ctx, req.UserId, req.HoldId).Return(hold, nil)

			Convey("Revenue is set with IgnorePayout true", func() {
				_, err := validator.IsValid(ctx, badReq)
				So(err, ShouldNotBeNil)
			})

			Convey("Revenue doesn't match hold", func() {
				_, err := validator.IsValid(ctx, badReq2)
				So(err, ShouldNotBeNil)
			})

			Convey("Reward Attributions doesn't match hold", func() {
				_, err := validator.IsValid(ctx, badReq3)
				So(err, ShouldNotBeNil)
			})
		})
	})
}
