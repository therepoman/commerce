package finalize_hold

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/payday/clients/cloudwatchlogger"
	holds_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/holds"
	finalize_hold_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/rpc/paydayserver/finalize_hold"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"github.com/gofrs/uuid"
	. "github.com/smartystreets/goconvey/convey"
)

func TestServer_Do(t *testing.T) {
	Convey("given a server", t, func() {
		validator := new(finalize_hold_mock.Validator)
		finalizer := new(holds_mock.Finalizer)
		cwlogger := cloudwatchlogger.NewCloudWatchLogNoopClient()

		server := &server{
			Validator:   validator,
			Finalizer:   finalizer,
			AuditLogger: cwlogger,
		}

		holdID := uuid.Must(uuid.NewV4()).String()
		req := &paydayrpc.FinalizeHoldReq{
			HoldId: holdID,
			UserId: "user-id",
			Recipients: []*paydayrpc.Recipient{
				{
					Id:              "recipient-1",
					Amount:          10,
					TransactionType: paydayrpc.TransactionType_GiveBitsToBroadcaster,
				},
			},
		}

		hold := &paydayrpc.Hold{
			HoldId:      holdID,
			UserId:      "user-id",
			Amount:      10,
			IsFinalized: false,
		}

		ctx := context.Background()

		Convey("when request is valid", func() {
			validator.On("IsValid", ctx, req).Return(hold, nil)
			finalizer.On("Finalize", ctx, req, hold).Return(nil)

			resp, err := server.Do(ctx, req)
			So(err, ShouldBeNil)
			So(resp, ShouldNotBeNil)

			Convey("calls the finalizer", func() {
				finalizer.AssertCalled(t, "Finalize", ctx, req, hold)
			})

			Convey("returns a Hold", func() {
				So(resp.Hold, ShouldNotBeNil)
				So(resp.Hold.HoldId, ShouldEqual, holdID)
				So(resp.Hold.UserId, ShouldEqual, "user-id")
				So(resp.Hold.Amount, ShouldEqual, 10)
				So(resp.Hold.IsFinalized, ShouldBeTrue)
				So(resp.Hold.IsReleased, ShouldBeFalse)
			})
		})

		Convey("returns an error if", func() {
			Convey("request isn't valid", func() {
				validator.On("IsValid", ctx, req).Return(nil, errors.New("validator error"))
			})

			Convey("finalizer fails", func() {
				validator.On("IsValid", ctx, req).Return(hold, nil)
				finalizer.On("Finalize", ctx, req, hold).Return(errors.New("finalizer error"))
			})

			_, err := server.Do(ctx, req)
			So(err, ShouldNotBeNil)
		})
	})
}
