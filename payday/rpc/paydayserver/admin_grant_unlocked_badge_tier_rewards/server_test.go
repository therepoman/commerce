package admin_grant_unlocked_badge_tier_rewards

import (
	"context"
	"errors"
	"testing"

	bitsbadgeadmin_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/badgetiers/admin"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestServer(t *testing.T) {
	Convey("Given a server", t, func() {
		admin := new(bitsbadgeadmin_mock.BadgeTierAdmin)
		server := ServerImpl{
			Admin: admin,
		}

		req := &paydayrpc.AdminGrantUnlockedBadgeTierRewardsReq{
			UserId:    "123",
			ChannelId: "456",
		}

		totalBits := int64(123456)

		Convey("returns the total bits usage for the given user in the given channel", func() {
			admin.On("GrantUnlockedRewards", mock.Anything, req.UserId, req.ChannelId).Return(totalBits, nil)

			resp, err := server.AdminGrant(context.Background(), req)
			So(err, ShouldBeNil)
			So(resp, ShouldNotBeNil)
			So(resp.TotalBitsCheered, ShouldEqual, totalBits)
		})

		Convey("returns an error when", func() {
			Convey("request is missing", func() {
				req = nil
			})

			Convey("user ID is blank", func() {
				req.UserId = ""
			})

			Convey("channel ID is blank", func() {
				req.ChannelId = ""
			})

			Convey("admin handler errors", func() {
				admin.On("GrantUnlockedRewards", mock.Anything, req.UserId, req.ChannelId).Return(int64(0), errors.New("can't grant"))
			})

			_, err := server.AdminGrant(context.Background(), req)
			So(err, ShouldNotBeNil)
		})

	})
}
