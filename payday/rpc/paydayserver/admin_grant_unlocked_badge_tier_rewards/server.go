package admin_grant_unlocked_badge_tier_rewards

import (
	"context"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/badgetiers/admin"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"code.justin.tv/commerce/payday/utils/strings"
	"github.com/twitchtv/twirp"
)

type Server interface {
	AdminGrant(ctx context.Context, req *paydayrpc.AdminGrantUnlockedBadgeTierRewardsReq) (*paydayrpc.AdminGrantUnlockedBadgeTierRewardsResp, error)
}

type ServerImpl struct {
	Admin admin.BadgeTierAdmin `inject:""`
}

func (s *ServerImpl) AdminGrant(ctx context.Context, req *paydayrpc.AdminGrantUnlockedBadgeTierRewardsReq) (*paydayrpc.AdminGrantUnlockedBadgeTierRewardsResp, error) {
	if req == nil {
		return nil, twirp.InvalidArgumentError("request", "is nil")
	}

	if strings.Blank(req.UserId) {
		return nil, twirp.RequiredArgumentError("user ID")
	}

	if strings.Blank(req.ChannelId) {
		return nil, twirp.RequiredArgumentError("channel ID")
	}

	totalBits, err := s.Admin.GrantUnlockedRewards(ctx, req.UserId, req.ChannelId)
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"channel_id": req.ChannelId,
			"user_id":    req.UserId,
		}).WithError(err).Error("Failed to admin grant unlocked bits badge tier emotes")
		return nil, twirp.InternalErrorWith(err)
	}

	return &paydayrpc.AdminGrantUnlockedBadgeTierRewardsResp{
		UserId:           req.UserId,
		ChannelId:        req.ChannelId,
		TotalBitsCheered: totalBits,
	}, nil
}
