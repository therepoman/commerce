package delete_cheermote_tier

import (
	"context"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/actions/constants"
	"code.justin.tv/commerce/payday/cache/channel"
	"code.justin.tv/commerce/payday/image"
	"code.justin.tv/commerce/payday/models/api"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"github.com/golang/protobuf/ptypes"
	"github.com/twitchtv/twirp"
)

type Server interface {
	Delete(ctx context.Context, req *paydayrpc.DeleteCheermoteTierReq) (*paydayrpc.DeleteCheermoteTierResp, error)
}

type ServerImpl struct {
	ChannelManager channel.ChannelManager `inject:""`
	ImageManager   image.IImageManager    `inject:""`
}

func (s *ServerImpl) Delete(ctx context.Context, req *paydayrpc.DeleteCheermoteTierReq) (*paydayrpc.DeleteCheermoteTierResp, error) {
	if req == nil {
		return nil, twirp.InvalidArgumentError("request", "cannot be empty")
	} else if req.UserId == "" {
		return nil, twirp.InvalidArgumentError("user_id", "cannot be empty")
	} else if req.RequestingUserId == "" {
		return nil, twirp.InvalidArgumentError("requesting_user_id", "cannot be empty")
	} else if req.UserId != req.RequestingUserId {
		return nil, twirp.NewError(twirp.PermissionDenied, "requesting user is not permitted to delete this user's cheermote tier")
	} else if req.Tier == paydayrpc.CheermoteTier_Unknown {
		return nil, twirp.InvalidArgumentError("tier", "cannot be unknown")
	}

	tier := requestTierToInternalTier(req.Tier)
	if !image.IsValidTier(tier) {
		return nil, twirp.InvalidArgumentError("tier", "not supported")
	}

	logFields := log.Fields{
		"userID":           req.UserId,
		"requestingUserID": req.RequestingUserId,
		"tier":             req.Tier,
	}

	// Get the user's channel info
	channel, err := s.ChannelManager.Get(ctx, req.UserId)
	if err != nil {
		log.WithFields(logFields).WithError(err).Error("failed to get channel for cheermote tier deletion")
		return nil, twirp.InternalErrorWith(err)
	}

	// If the channel does not have a custom cheermote image set ID, it is not in a valid state to delete cheermote tiers
	if channel.CustomCheermoteImageSetId == nil {
		log.WithFields(logFields).Error("attempted to delete cheermote tier for channel with no custom cheermote image set ID")
		return nil, twirp.NewError(twirp.FailedPrecondition, "user does not have the specified custom cheermote")
	}

	// Delete the specified cheermote tier images
	imagesDeleted, err := s.ImageManager.DeleteChannelImageSetTierImages(ctx, req.UserId, *channel.CustomCheermoteImageSetId, tier)
	if err != nil {
		log.WithFields(logFields).WithError(err).Error("failed to delete cheermote tier for channel")
		return nil, twirp.InternalErrorWith(err)
	} else if !imagesDeleted {
		// There was nothing to delete
		return &paydayrpc.DeleteCheermoteTierResp{}, nil
	}

	return &paydayrpc.DeleteCheermoteTierResp{
		DeletedAt: ptypes.TimestampNow(),
	}, nil
}

func requestTierToInternalTier(tier paydayrpc.CheermoteTier) api.Tier {
	switch tier {
	case paydayrpc.CheermoteTier_Tier1:
		return constants.Tier1
	case paydayrpc.CheermoteTier_Tier100:
		return constants.Tier100
	case paydayrpc.CheermoteTier_Tier1000:
		return constants.Tier1000
	case paydayrpc.CheermoteTier_Tier5000:
		return constants.Tier5000
	case paydayrpc.CheermoteTier_Tier10000:
		return constants.Tier10000
	default:
		return ""
	}
}
