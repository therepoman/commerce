package delete_cheermote_tier

import (
	"context"
	"testing"

	"code.justin.tv/commerce/payday/actions/constants"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/errors"
	channel_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/cache/channel"
	image_manager_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/image_manager"
	apiModels "code.justin.tv/commerce/payday/models/api"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"code.justin.tv/commerce/payday/utils/pointers"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestServer_Delete(t *testing.T) {
	Convey("given a Delete API", t, func() {
		channelManagerMock := new(channel_mock.ChannelManager)
		imageManagerMock := new(image_manager_mock.IImageManager)

		api := &ServerImpl{
			ChannelManager: channelManagerMock,
			ImageManager:   imageManagerMock,
		}

		userID := "1234"
		tier := paydayrpc.CheermoteTier_Tier1
		imageSetID := "abcd"

		Convey("when the request is nil", func() {
			var req *paydayrpc.DeleteCheermoteTierReq
			resp, err := api.Delete(context.Background(), req)

			Convey("we should return an error", func() {
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		Convey("when userID is empty", func() {
			req := &paydayrpc.DeleteCheermoteTierReq{}
			resp, err := api.Delete(context.Background(), req)

			Convey("we should return an error", func() {
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		Convey("when requestingUserID is empty", func() {
			req := &paydayrpc.DeleteCheermoteTierReq{
				UserId: userID,
			}
			resp, err := api.Delete(context.Background(), req)

			Convey("we should return an error", func() {
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		Convey("when requestingUserID is different than userID", func() {
			req := &paydayrpc.DeleteCheermoteTierReq{
				UserId:           userID,
				RequestingUserId: "notuserid",
			}
			resp, err := api.Delete(context.Background(), req)

			Convey("we should return an error", func() {
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		Convey("when the tier is unknown", func() {
			req := &paydayrpc.DeleteCheermoteTierReq{
				UserId:           userID,
				RequestingUserId: userID,
			}
			resp, err := api.Delete(context.Background(), req)

			Convey("we should return an error", func() {
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		Convey("when the tier is not supported", func() {
			req := &paydayrpc.DeleteCheermoteTierReq{
				UserId:           userID,
				RequestingUserId: userID,
				Tier:             paydayrpc.CheermoteTier(-1),
			}
			resp, err := api.Delete(context.Background(), req)

			Convey("we should return an error", func() {
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		Convey("when the channel manager get errors", func() {
			channelManagerMock.On("Get", mock.Anything, userID).Return(nil, errors.New("test error"))

			req := &paydayrpc.DeleteCheermoteTierReq{
				UserId:           userID,
				RequestingUserId: userID,
				Tier:             tier,
			}
			resp, err := api.Delete(context.Background(), req)

			Convey("we should return an error", func() {
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		Convey("when the channel's custom cheermote image set ID is nil", func() {
			channelManagerMock.On("Get", mock.Anything, userID).Return(&dynamo.Channel{}, nil)

			req := &paydayrpc.DeleteCheermoteTierReq{
				UserId:           userID,
				RequestingUserId: userID,
				Tier:             tier,
			}
			resp, err := api.Delete(context.Background(), req)

			Convey("we should return an error", func() {
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		Convey("when the image manager errors", func() {
			channelManagerMock.On("Get", mock.Anything, userID).Return(&dynamo.Channel{
				CustomCheermoteImageSetId: &imageSetID,
			}, nil)
			imageManagerMock.On("DeleteChannelImageSetTierImages", mock.Anything, userID, imageSetID, constants.Tier1).Return(false, errors.New("test error"))

			req := &paydayrpc.DeleteCheermoteTierReq{
				UserId:           userID,
				RequestingUserId: userID,
				Tier:             tier,
			}
			resp, err := api.Delete(context.Background(), req)

			Convey("we should return an error", func() {
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		Convey("when no images were deleted", func() {
			channelManagerMock.On("Get", mock.Anything, userID).Return(&dynamo.Channel{
				CustomCheermoteImageSetId: &imageSetID,
			}, nil)
			imageManagerMock.On("DeleteChannelImageSetTierImages", mock.Anything, userID, imageSetID, constants.Tier1).Return(false, nil)

			req := &paydayrpc.DeleteCheermoteTierReq{
				UserId:           userID,
				RequestingUserId: userID,
				Tier:             tier,
			}
			resp, err := api.Delete(context.Background(), req)

			Convey("we should succeed", func() {
				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
				So(resp.DeletedAt, ShouldBeNil)
			})
		})

		Convey("when everything succeeds", func() {
			channelManagerMock.On("Get", mock.Anything, userID).Return(&dynamo.Channel{
				CustomCheermoteImageSetId: &imageSetID,
			}, nil)
			imageManagerMock.On("DeleteChannelImageSetTierImages", mock.Anything, userID, imageSetID, constants.Tier1).Return(true, nil)
			channelManagerMock.On("Update", mock.Anything, &dynamo.Channel{
				CustomCheermoteImageSetId: &imageSetID,
				CustomCheermoteStatus:     pointers.StringP(apiModels.CustomCheermoteStatusDisabled),
			}).Return(nil)

			req := &paydayrpc.DeleteCheermoteTierReq{
				UserId:           userID,
				RequestingUserId: userID,
				Tier:             tier,
			}
			resp, err := api.Delete(context.Background(), req)

			Convey("we should succeed", func() {
				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
				So(resp.DeletedAt, ShouldNotBeNil)
			})
		})
	})
}
