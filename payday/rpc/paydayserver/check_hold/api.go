package check_hold

import (
	"context"

	"code.justin.tv/commerce/payday/holds"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"github.com/twitchtv/twirp"
)

type Server interface {
	Get(ctx context.Context, req *paydayrpc.CheckHoldReq) (*paydayrpc.CheckHoldResp, error)
}

func NewServer() Server {
	return &server{}
}

type server struct {
	Validator Validator    `inject:""`
	Getter    holds.Getter `inject:"holdsGetter"`
}

func (a *server) Get(ctx context.Context, req *paydayrpc.CheckHoldReq) (*paydayrpc.CheckHoldResp, error) {
	if err := a.Validator.IsValid(req); err != nil {
		return nil, err
	}

	hold, err := a.Getter.Get(ctx, req.UserId, req.HoldId)
	if err != nil {
		return nil, twirp.InternalErrorWith(err)
	}

	resp := &paydayrpc.CheckHoldResp{}
	if hold != nil {
		resp.Hold = hold
	}

	return resp, nil
}
