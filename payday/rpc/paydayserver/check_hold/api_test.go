package check_hold

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/gogogadget/random"
	holds_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/holds"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	. "github.com/smartystreets/goconvey/convey"
)

func TestServer_Get(t *testing.T) {
	Convey("given a check hold server", t, func() {
		getter := new(holds_mock.Getter)

		server := server{
			Getter:    getter,
			Validator: NewValidator(),
		}

		ctx := context.Background()
		req := &paydayrpc.CheckHoldReq{
			HoldId: random.String(32),
			UserId: random.String(16),
		}

		Convey("when we have an invalid request", func() {
			_, err := server.Get(ctx, nil)
			So(err, ShouldNotBeNil)
		})

		Convey("when we have a valid request", func() {
			Convey("when we fail to get the hold", func() {
				getter.On("Get", ctx, req.UserId, req.HoldId).Return(nil, errors.New("WALRUS STRIKE"))

				_, err := server.Get(ctx, req)
				So(err, ShouldNotBeNil)
			})

			Convey("when we don't have a hold", func() {
				getter.On("Get", ctx, req.UserId, req.HoldId).Return(nil, nil)

				res, err := server.Get(ctx, req)
				So(err, ShouldBeNil)
				So(res, ShouldNotBeNil)
				So(res.Hold, ShouldBeNil)
			})

			Convey("when we have the hold", func() {
				getter.On("Get", ctx, req.UserId, req.HoldId).Return(&paydayrpc.Hold{
					HoldId: req.HoldId,
					UserId: req.UserId,
				}, nil)

				res, err := server.Get(ctx, req)
				So(err, ShouldBeNil)
				So(res, ShouldNotBeNil)
				So(res.Hold, ShouldNotBeNil)
			})
		})
	})
}
