package check_hold

import (
	"code.justin.tv/commerce/gogogadget/strings"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"github.com/twitchtv/twirp"
)

type Validator interface {
	IsValid(req *paydayrpc.CheckHoldReq) error
}

func NewValidator() Validator {
	return &validator{}
}

type validator struct {
}

func (v *validator) IsValid(req *paydayrpc.CheckHoldReq) error {
	if req == nil {
		return twirp.InvalidArgumentError("body", "cannot be blank")
	}

	if strings.Blank(req.UserId) {
		return twirp.InvalidArgumentError("user_id", "cannot be blank")
	}

	if strings.Blank(req.HoldId) {
		return twirp.InvalidArgumentError("hold_id", "cannot be blank")
	}

	return nil
}
