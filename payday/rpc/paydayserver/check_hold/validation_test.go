package check_hold

import (
	"testing"

	"code.justin.tv/commerce/gogogadget/random"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	. "github.com/smartystreets/goconvey/convey"
)

func TestValidator_IsValid(t *testing.T) {
	Convey("given a check hold api validator", t, func() {
		v := NewValidator()

		Convey("should return success", func() {
			err := v.IsValid(&paydayrpc.CheckHoldReq{
				HoldId: random.String(32),
				UserId: random.String(16),
			})

			So(err, ShouldBeNil)
		})

		Convey("should return error", func() {
			req := &paydayrpc.CheckHoldReq{}
			Convey("when the the req is nil", func() {
				req = nil
			})

			Convey("when the user id is blank", func() {
				req.UserId = ""
			})

			Convey("when the hold id is blank", func() {
				req.UserId = random.String(16)
				req.HoldId = ""
			})

			err := v.IsValid(req)
			So(err, ShouldNotBeNil)
		})
	})
}
