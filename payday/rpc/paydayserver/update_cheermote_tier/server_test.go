package update_cheermote_tier

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/payday/actions/constants"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/image"
	channel_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/cache/channel"
	image_manager_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/image_manager"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"code.justin.tv/commerce/payday/utils/pointers"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func getPaydayRPCErrorCode(err error) string {
	var rpcError paydayrpc.ClientError
	parseError := paydayrpc.ParseClientError(err, &rpcError)
	if parseError != nil {
		return ""
	}
	return rpcError.ErrorCode
}

func TestServer_Update(t *testing.T) {
	Convey("given an Update API", t, func() {
		channelManagerMock := new(channel_mock.ChannelManager)
		imageManagerMock := new(image_manager_mock.IImageManager)

		api := &ServerImpl{
			ChannelManager: channelManagerMock,
			ImageManager:   imageManagerMock,
		}

		channelID := "1234"
		tier := paydayrpc.CheermoteTier_Tier1
		apiTier := constants.Tier1
		imageSetID := "abcd"

		staticAsset := paydayrpc.EmoteImageAsset{AssetType: paydayrpc.EmoteImageAssetType_STATIC}
		animatedAsset := paydayrpc.EmoteImageAsset{AssetType: paydayrpc.EmoteImageAssetType_ANIMATED}

		Convey("when the request is nil", func() {
			var req *paydayrpc.UpdateCheermoteTierReq
			resp, err := api.Update(context.Background(), req)

			Convey("we should return an error", func() {
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		Convey("when userID is empty", func() {
			req := &paydayrpc.UpdateCheermoteTierReq{}
			resp, err := api.Update(context.Background(), req)

			Convey("we should return an error", func() {
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		Convey("when requestingUserID is empty", func() {
			req := &paydayrpc.UpdateCheermoteTierReq{
				ChannelId: channelID,
			}
			resp, err := api.Update(context.Background(), req)

			Convey("we should return an error", func() {
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		Convey("when requestingUserID is different than userID", func() {
			req := &paydayrpc.UpdateCheermoteTierReq{
				ChannelId:        channelID,
				RequestingUserId: "notuserid",
			}
			resp, err := api.Update(context.Background(), req)

			Convey("we should return an Invalid_Owner error", func() {
				So(err, ShouldNotBeNil)
				So(getPaydayRPCErrorCode(err), ShouldEqual, paydayrpc.ErrCodeUserNotPermitted)
				So(resp, ShouldBeNil)
			})
		})

		Convey("when the tier is unknown", func() {
			req := &paydayrpc.UpdateCheermoteTierReq{
				ChannelId:        channelID,
				RequestingUserId: channelID,
			}
			resp, err := api.Update(context.Background(), req)

			Convey("we should return an error", func() {
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		Convey("when the tier is not supported", func() {
			req := &paydayrpc.UpdateCheermoteTierReq{
				ChannelId:        channelID,
				RequestingUserId: channelID,
				Tier:             paydayrpc.CheermoteTier(-1),
			}
			resp, err := api.Update(context.Background(), req)

			Convey("we should return an error", func() {
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		Convey("when the static assets are missing", func() {
			req := &paydayrpc.UpdateCheermoteTierReq{
				ChannelId:        channelID,
				RequestingUserId: channelID,
				Tier:             tier,
				ImageAssets:      []*paydayrpc.EmoteImageAsset{&animatedAsset},
			}
			resp, err := api.Update(context.Background(), req)

			Convey("we should return an error indicating missing static assets", func() {
				So(err, ShouldNotBeNil)
				So(getPaydayRPCErrorCode(err), ShouldEqual, paydayrpc.ErrCodeMissingStaticAsset)
				So(resp, ShouldBeNil)
			})
		})

		Convey("when the animated assets are missing", func() {
			req := &paydayrpc.UpdateCheermoteTierReq{
				ChannelId:        channelID,
				RequestingUserId: channelID,
				Tier:             tier,
				ImageAssets:      []*paydayrpc.EmoteImageAsset{&staticAsset},
			}
			resp, err := api.Update(context.Background(), req)

			Convey("we should return an error indicating missing animated assets", func() {
				So(err, ShouldNotBeNil)
				So(getPaydayRPCErrorCode(err), ShouldEqual, paydayrpc.ErrCodeMissingAnimatedAsset)
				So(resp, ShouldBeNil)
			})
		})

		Convey("when extra assets are provided", func() {
			req := &paydayrpc.UpdateCheermoteTierReq{
				ChannelId:        channelID,
				RequestingUserId: channelID,
				Tier:             tier,
				ImageAssets:      []*paydayrpc.EmoteImageAsset{&staticAsset, &staticAsset, &animatedAsset},
			}
			resp, err := api.Update(context.Background(), req)

			Convey("we should return an error indicating too many assets", func() {
				So(err, ShouldNotBeNil)
				So(getPaydayRPCErrorCode(err), ShouldEqual, paydayrpc.ErrCodeTooManyImageAssets)
				So(resp, ShouldBeNil)
			})
		})

		Convey("when the channel manager get errors", func() {
			channelManagerMock.On("Get", mock.Anything, channelID).Return(nil, errors.New("test error"))

			req := &paydayrpc.UpdateCheermoteTierReq{
				ChannelId:        channelID,
				RequestingUserId: channelID,
				Tier:             tier,
				ImageAssets:      []*paydayrpc.EmoteImageAsset{&staticAsset, &animatedAsset},
			}
			resp, err := api.Update(context.Background(), req)

			Convey("we should return an error", func() {
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		Convey("when the channel is not onboarded", func() {
			channelManagerMock.On("Get", mock.Anything, channelID).Return(&dynamo.Channel{
				Onboarded:                 pointers.BoolP(false),
				CustomCheermoteImageSetId: &imageSetID,
			}, nil)

			req := &paydayrpc.UpdateCheermoteTierReq{
				ChannelId:        channelID,
				RequestingUserId: channelID,
				Tier:             tier,
				ImageAssets:      []*paydayrpc.EmoteImageAsset{&staticAsset, &animatedAsset},
			}
			resp, err := api.Update(context.Background(), req)

			Convey("it should return an error", func() {
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		Convey("when image manager fails because of missing assets", func() {
			channelManagerMock.On("Get", mock.Anything, channelID).Return(&dynamo.Channel{
				Onboarded:                 pointers.BoolP(true),
				CustomCheermoteImageSetId: &imageSetID,
			}, nil)
			imageManagerMock.On("ProcessMakoImages", mock.Anything, channelID, imageSetID, apiTier, staticAsset, animatedAsset).Return(image.ErrImageAssetsNotFound)

			req := &paydayrpc.UpdateCheermoteTierReq{
				ChannelId:        channelID,
				RequestingUserId: channelID,
				Tier:             tier,
				ImageAssets:      []*paydayrpc.EmoteImageAsset{&staticAsset, &animatedAsset},
			}
			resp, err := api.Update(context.Background(), req)

			Convey("it should return an error indicating assets not found", func() {
				So(err, ShouldNotBeNil)
				So(getPaydayRPCErrorCode(err), ShouldEqual, paydayrpc.ErrCodeImageAssetsNotFound)
				So(resp, ShouldBeNil)
			})
		})

		Convey("when image manager fails because of invalid assets", func() {
			channelManagerMock.On("Get", mock.Anything, channelID).Return(&dynamo.Channel{
				Onboarded:                 pointers.BoolP(true),
				CustomCheermoteImageSetId: &imageSetID,
			}, nil)
			imageManagerMock.On("ProcessMakoImages", mock.Anything, channelID, imageSetID, apiTier, staticAsset, animatedAsset).Return(image.ErrInvalidImageAssets)

			req := &paydayrpc.UpdateCheermoteTierReq{
				ChannelId:        channelID,
				RequestingUserId: channelID,
				Tier:             tier,
				ImageAssets:      []*paydayrpc.EmoteImageAsset{&staticAsset, &animatedAsset},
			}
			resp, err := api.Update(context.Background(), req)

			Convey("it should return an error indicating invalid assets", func() {
				So(err, ShouldNotBeNil)
				So(getPaydayRPCErrorCode(err), ShouldEqual, paydayrpc.ErrCodeInvalidImageUpload)
				So(resp, ShouldBeNil)
			})
		})

		Convey("when everything succeeds", func() {
			channelManagerMock.On("Get", mock.Anything, channelID).Return(&dynamo.Channel{
				Onboarded:                 pointers.BoolP(true),
				CustomCheermoteImageSetId: &imageSetID,
			}, nil)
			imageManagerMock.On("ProcessMakoImages", mock.Anything, channelID, imageSetID, apiTier, staticAsset, animatedAsset).Return(nil)

			req := &paydayrpc.UpdateCheermoteTierReq{
				ChannelId:        channelID,
				RequestingUserId: channelID,
				Tier:             tier,
				ImageAssets:      []*paydayrpc.EmoteImageAsset{&staticAsset, &animatedAsset},
			}
			resp, err := api.Update(context.Background(), req)

			Convey("we should succeed", func() {
				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
			})
		})

		Convey("when this is the first image in a set and the channel update fails", func() {
			channelManagerMock.On("Get", mock.Anything, channelID).Return(&dynamo.Channel{
				Onboarded: pointers.BoolP(true),
				// CustomCheermoteImageSetId is intentionally blank
			}, nil)
			channelManagerMock.On("Update", mock.Anything, mock.Anything).Return(errors.New("test error"))
			imageManagerMock.On("ProcessMakoImages", mock.Anything, channelID, mock.Anything, apiTier, staticAsset, animatedAsset).Return(nil)

			req := &paydayrpc.UpdateCheermoteTierReq{
				ChannelId:        channelID,
				RequestingUserId: channelID,
				Tier:             tier,
				ImageAssets:      []*paydayrpc.EmoteImageAsset{&staticAsset, &animatedAsset},
			}
			resp, err := api.Update(context.Background(), req)

			Convey("we should succeed and the same setID should be used in channel manager and image manager", func() {
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		Convey("when this is the first image in a set and everything succeeds", func() {
			channelManagerMock.On("Get", mock.Anything, channelID).Return(&dynamo.Channel{
				Onboarded: pointers.BoolP(true),
				// CustomCheermoteImageSetId is intentionally blank
			}, nil)
			channelManagerMock.On("Update", mock.Anything, mock.Anything).Return(nil)
			imageManagerMock.On("ProcessMakoImages", mock.Anything, channelID, mock.Anything, apiTier, staticAsset, animatedAsset).Return(nil)

			req := &paydayrpc.UpdateCheermoteTierReq{
				ChannelId:        channelID,
				RequestingUserId: channelID,
				Tier:             tier,
				ImageAssets:      []*paydayrpc.EmoteImageAsset{&staticAsset, &animatedAsset},
			}
			resp, err := api.Update(context.Background(), req)

			Convey("we should succeed and the same setID should be used in channel manager and image manager", func() {
				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)

				channelSetID := channelManagerMock.Calls[1].Arguments[1].(*dynamo.Channel).CustomCheermoteImageSetId
				imageSetID := imageManagerMock.Calls[0].Arguments[2]

				So(channelSetID, ShouldNotBeNil)
				So(*channelSetID, ShouldNotBeBlank)
				So(imageSetID, ShouldEqual, *channelSetID)
			})
		})
	})
}
