package update_cheermote_tier

import (
	"context"
	"time"

	log "code.justin.tv/commerce/logrus"
	"github.com/gofrs/uuid"
	"github.com/pkg/errors"
	"github.com/twitchtv/twirp"

	"code.justin.tv/commerce/payday/actions/constants"
	"code.justin.tv/commerce/payday/cache/channel"
	"code.justin.tv/commerce/payday/image"
	"code.justin.tv/commerce/payday/models/api"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"code.justin.tv/commerce/payday/utils/pointers"
	"code.justin.tv/commerce/payday/utils/strings"
)

type Server interface {
	Update(ctx context.Context, req *paydayrpc.UpdateCheermoteTierReq) (*paydayrpc.UpdateCheermoteTierResp, error)
}

type ServerImpl struct {
	ChannelManager channel.ChannelManager `inject:""`
	ImageManager   image.IImageManager    `inject:""`
}

// Update will create or replace the image assets for a channel's specified cheermote tier using images which have already been uploaded via Mako's animated emote upload flow
func (s *ServerImpl) Update(ctx context.Context, req *paydayrpc.UpdateCheermoteTierReq) (*paydayrpc.UpdateCheermoteTierResp, error) {
	ctx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()

	if req == nil {
		return nil, paydayrpc.NewClientError(twirp.RequiredArgumentError("request"), paydayrpc.ErrCodeMissingRequiredAgrument)
	} else if req.ChannelId == "" {
		return nil, paydayrpc.NewClientError(twirp.RequiredArgumentError("channel_id"), paydayrpc.ErrCodeMissingRequiredAgrument)
	} else if req.RequestingUserId == "" {
		return nil, paydayrpc.NewClientError(twirp.RequiredArgumentError("requesting_user_id"), paydayrpc.ErrCodeMissingRequiredAgrument)
	} else if req.ChannelId != req.RequestingUserId {
		return nil, paydayrpc.NewClientError(twirp.NewError(twirp.PermissionDenied, "requesting user is not permitted to update this user's cheermote tier"), paydayrpc.ErrCodeUserNotPermitted)
	} else if req.Tier == paydayrpc.CheermoteTier_Unknown {
		return nil, paydayrpc.NewClientError(twirp.InvalidArgumentError("tier", "cannot be unknown"), paydayrpc.ErrCodeMissingRequiredAgrument)
	} else if len(req.ImageAssets) > 2 {
		return nil, paydayrpc.NewClientError(twirp.InvalidArgumentError("image_assets", "too many were provided"), paydayrpc.ErrCodeTooManyImageAssets)
	}

	var staticAsset, animatedAsset *paydayrpc.EmoteImageAsset
	for _, asset := range req.ImageAssets {
		if asset == nil {
			// This shouldn't be valid input, but be safe and prevent panics
			continue
		}

		if asset.AssetType == paydayrpc.EmoteImageAssetType_STATIC {
			staticAsset = asset
		} else if asset.AssetType == paydayrpc.EmoteImageAssetType_ANIMATED {
			animatedAsset = asset
		}
	}
	if staticAsset == nil {
		return nil, paydayrpc.NewClientError(twirp.InvalidArgumentError("image_assets", "no static asset was provided"), paydayrpc.ErrCodeMissingStaticAsset)
	} else if animatedAsset == nil {
		return nil, paydayrpc.NewClientError(twirp.InvalidArgumentError("image_assets", "no animated asset was provided"), paydayrpc.ErrCodeMissingAnimatedAsset)
	}

	channel, err := s.ChannelManager.Get(ctx, req.ChannelId)
	if err != nil {
		log.WithError(err).WithField("ChannelID", req.ChannelId).Error("[UpdateCheermoteTier] Failed to get channel from dynamo")
		return nil, twirp.InternalErrorWith(errors.Wrap(err, "Failed to get channel from dynamo"))
	}

	if channel == nil {
		log.WithField("ChannelID", req.ChannelId).Error("[UpdateCheermoteTier] Could not find dynamo channel record")
		return nil, twirp.InternalError("Could not find dynamo channel record")
	}

	if !pointers.BoolOrDefault(channel.Onboarded, false) {
		log.WithError(err).WithField("ChannelID", req.ChannelId).Warn("[UpdateCheermoteTier] Save image request for non-bits enabled channel")
		return nil, paydayrpc.NewClientError(twirp.NewError(twirp.PermissionDenied, "Channel is not eligible to create cheermotes"), paydayrpc.ErrCodeUserNotPermitted)
	}

	var imageSetID string
	if strings.BlankP(channel.CustomCheermoteImageSetId) {
		imageSetUUID, err := uuid.NewV4()
		if err != nil {
			log.WithError(err).Error("[UpdateCheermoteTier] Error generating ImageSetID UUID")
			return nil, twirp.InternalErrorWith(errors.Wrap(err, "Error generating ImageSetID UUID"))
		}

		imageSetID = imageSetUUID.String()

		channel.CustomCheermoteImageSetId = pointers.StringP(imageSetID)
		err = s.ChannelManager.Update(ctx, channel)
		if err != nil {
			log.WithError(err).WithField("ChannelID", req.ChannelId).Error("[UpdateCheermoteTier] Could not update dynamo channel record with image set id")
			return nil, twirp.InternalErrorWith(errors.Wrap(err, "Could not update dynamo channel record with image set id"))
		}

	} else {
		imageSetID = *channel.CustomCheermoteImageSetId
	}

	tier := requestTierToInternalTier(req.Tier)
	if !image.IsValidTier(tier) {
		return nil, twirp.InvalidArgumentError("tier", "not supported")
	}

	err = s.ImageManager.ProcessMakoImages(ctx, req.ChannelId, imageSetID, tier, *staticAsset, *animatedAsset)
	if err != nil {
		log.WithError(err).WithFields(log.Fields{
			"ChannelID": req.ChannelId,
			"Tier":      tier,
		}).Error("[UpdateCheermoteTier] Failed to update cheermote tier with new images")
		return nil, convertToTwirpError(err)
	}

	return &paydayrpc.UpdateCheermoteTierResp{}, nil
}

func requestTierToInternalTier(tier paydayrpc.CheermoteTier) api.Tier {
	switch tier {
	case paydayrpc.CheermoteTier_Tier1:
		return constants.Tier1
	case paydayrpc.CheermoteTier_Tier100:
		return constants.Tier100
	case paydayrpc.CheermoteTier_Tier1000:
		return constants.Tier1000
	case paydayrpc.CheermoteTier_Tier5000:
		return constants.Tier5000
	case paydayrpc.CheermoteTier_Tier10000:
		return constants.Tier10000
	default:
		return ""
	}
}

func convertToTwirpError(imageError error) twirp.Error {
	switch imageError {
	case image.ErrImageAssetsNotFound:
		return paydayrpc.NewClientError(twirp.InvalidArgumentError("image_assets", "could not be found"), paydayrpc.ErrCodeImageAssetsNotFound)
	case image.ErrInvalidImageAssets:
		return paydayrpc.NewClientError(twirp.InvalidArgumentError("image_assets", "image files were invalid"), paydayrpc.ErrCodeInvalidImageUpload)
	default:
		return twirp.InternalErrorWith(imageError)
	}
}
