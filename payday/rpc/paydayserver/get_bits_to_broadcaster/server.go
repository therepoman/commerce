package get_bits_to_broadcaster

import (
	"context"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/leaderboard"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"github.com/twitchtv/twirp"
)

type Server interface {
	Get(ctx context.Context, req *paydayrpc.GetBitsToBroadcasterReq) (*paydayrpc.GetBitsToBroadcasterResp, error)
}

type server struct {
	BitsToBroadcasterGetter leaderboard.BitsToBroadcasterGetter `inject:""`
}

func NewServer() Server {
	return &server{}
}

func (s *server) Get(ctx context.Context, req *paydayrpc.GetBitsToBroadcasterReq) (*paydayrpc.GetBitsToBroadcasterResp, error) {
	ctx, cancel := context.WithTimeout(ctx, 2*time.Second)
	defer cancel()

	if req == nil {
		return nil, twirp.InternalError("cannot have empty request")
	}

	if req.UserId == "" {
		return nil, twirp.InvalidArgumentError("UserId", "cannot have empty User Id")
	}

	if req.ChannelId == "" {
		return nil, twirp.InvalidArgumentError("ChannelId", "cannot have empty Channel Id")
	}

	bitsToBroadcaster, err := s.BitsToBroadcasterGetter.GetBitsToBroadcaster(ctx, req.UserId, req.ChannelId)
	if err != nil {
		log.WithError(err).WithFields(log.Fields{
			"userId":    req.UserId,
			"channelId": req.ChannelId,
		}).Error("failed to get balance given to broadcaster")
		return nil, err
	}
	return &paydayrpc.GetBitsToBroadcasterResp{
		BitsToBroadcaster: int32(bitsToBroadcaster),
	}, nil
}
