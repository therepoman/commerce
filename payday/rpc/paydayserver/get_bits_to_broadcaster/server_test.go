package get_bits_to_broadcaster

import (
	"context"
	"testing"

	"code.justin.tv/commerce/payday/errors"
	leaderboard_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/leaderboard"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
	"github.com/twitchtv/twirp"
)

func TestServer_Get(t *testing.T) {
	Convey("given a server", t, func() {
		btbg := new(leaderboard_mock.BitsToBroadcasterGetter)

		server := &server{
			BitsToBroadcasterGetter: btbg,
		}

		userId := "1234"
		channelId := "264051522" // Twitch rivals [test round] Fortnite champion twitch.tv/michael
		ctx := context.Background()

		Convey("when request is nil", func() {
			resp, err := server.Get(ctx, nil)
			So(resp, ShouldBeNil)

			twirpErr, ok := err.(twirp.Error)
			So(ok, ShouldBeTrue)
			So(twirpErr.Code(), ShouldEqual, twirp.Internal)
			So(twirpErr.Msg(), ShouldEqual, "cannot have empty request")
		})

		Convey("when request is empty", func() {
			req := paydayrpc.GetBitsToBroadcasterReq{}

			resp, err := server.Get(ctx, &req)
			So(resp, ShouldBeNil)

			twirpErr, ok := err.(twirp.Error)
			So(ok, ShouldBeTrue)
			So(twirpErr.Code(), ShouldEqual, twirp.InvalidArgument)
			So(twirpErr.Msg(), ShouldEqual, "UserId cannot have empty User Id")
			So(twirpErr.Meta("argument"), ShouldEqual, "UserId")

			Convey("when request is missing channel id", func() {
				req.UserId = userId

				resp, err := server.Get(ctx, &req)
				So(resp, ShouldBeNil)

				twirpErr, ok := err.(twirp.Error)
				So(ok, ShouldBeTrue)
				So(twirpErr.Code(), ShouldEqual, twirp.InvalidArgument)
				So(twirpErr.Msg(), ShouldEqual, "ChannelId cannot have empty Channel Id")
				So(twirpErr.Meta("argument"), ShouldEqual, "ChannelId")
			})
		})

		Convey("when request is valid", func() {
			req := paydayrpc.GetBitsToBroadcasterReq{
				UserId:    userId,
				ChannelId: channelId,
			}
			Convey("when getter works", func() {
				btbg.On("GetBitsToBroadcaster", mock.Anything, userId, channelId).Return(int64(66), nil)

				resp, err := server.Get(ctx, &req)
				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
				So(resp.BitsToBroadcaster, ShouldEqual, int32(66))
			})

			Convey("when getter works errors", func() {
				animeBearError := errors.New("ANIME BEAR SHORTAGE")
				btbg.On("GetBitsToBroadcaster", mock.Anything, userId, channelId).Return(int64(0), animeBearError)

				resp, err := server.Get(ctx, &req)
				So(resp, ShouldBeNil)
				So(err, ShouldEqual, animeBearError)
			})
		})
	})
}
