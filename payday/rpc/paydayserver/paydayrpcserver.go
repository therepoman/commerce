package paydayserver

import (
	"context"
	"errors"

	bitsbadges "code.justin.tv/commerce/payday/badgetiers/products/badges"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"code.justin.tv/commerce/payday/rpc/paydayserver/admin_grant_unlocked_badge_tier_rewards"
	"code.justin.tv/commerce/payday/rpc/paydayserver/assign_emote_to_bits_tier"
	get_bits_bulk_grant_campaigns "code.justin.tv/commerce/payday/rpc/paydayserver/bits_bulk_grant_campaigns"
	"code.justin.tv/commerce/payday/rpc/paydayserver/check_bits_products_eligibility"
	"code.justin.tv/commerce/payday/rpc/paydayserver/check_hold"
	"code.justin.tv/commerce/payday/rpc/paydayserver/create_hold"
	"code.justin.tv/commerce/payday/rpc/paydayserver/delete_cheermote_tier"
	"code.justin.tv/commerce/payday/rpc/paydayserver/finalize_hold"
	"code.justin.tv/commerce/payday/rpc/paydayserver/get_auto_refill_settings"
	"code.justin.tv/commerce/payday/rpc/paydayserver/get_badge_tier_emotes"
	"code.justin.tv/commerce/payday/rpc/paydayserver/get_badge_tier_notification"
	"code.justin.tv/commerce/payday/rpc/paydayserver/get_badge_tier_summary_for_emote_group_id"
	"code.justin.tv/commerce/payday/rpc/paydayserver/get_balance"
	"code.justin.tv/commerce/payday/rpc/paydayserver/get_bits_tier_emote_groups"
	"code.justin.tv/commerce/payday/rpc/paydayserver/get_bits_to_broadcaster"
	"code.justin.tv/commerce/payday/rpc/paydayserver/get_cheer_screenshot"
	"code.justin.tv/commerce/payday/rpc/paydayserver/get_cheer_screenshots_for_channel"
	"code.justin.tv/commerce/payday/rpc/paydayserver/get_cheers"
	"code.justin.tv/commerce/payday/rpc/paydayserver/get_extension_transactions"
	"code.justin.tv/commerce/payday/rpc/paydayserver/get_transactions_by_id"
	"code.justin.tv/commerce/payday/rpc/paydayserver/get_user_bits_for_campaign"
	"code.justin.tv/commerce/payday/rpc/paydayserver/get_user_campaign_status"
	"code.justin.tv/commerce/payday/rpc/paydayserver/get_user_settings"
	"code.justin.tv/commerce/payday/rpc/paydayserver/grant_pinata_bonus_bits"
	"code.justin.tv/commerce/payday/rpc/paydayserver/is_eligible_to_use_bits_on_extension"
	"code.justin.tv/commerce/payday/rpc/paydayserver/micro_cheer"
	"code.justin.tv/commerce/payday/rpc/paydayserver/release_hold"
	"code.justin.tv/commerce/payday/rpc/paydayserver/search_transactions"
	"code.justin.tv/commerce/payday/rpc/paydayserver/set_auto_refill_settings"
	"code.justin.tv/commerce/payday/rpc/paydayserver/set_badge_tier_notification_state"
	"code.justin.tv/commerce/payday/rpc/paydayserver/set_user_campaign_status"
	"code.justin.tv/commerce/payday/rpc/paydayserver/set_user_settings"
	"code.justin.tv/commerce/payday/rpc/paydayserver/update_cheermote_tier"
	"code.justin.tv/commerce/payday/rpc/paydayserver/use_bits_on_extension"
	"code.justin.tv/commerce/payday/rpc/paydayserver/use_bits_on_poll"
	"code.justin.tv/commerce/payday/rpc/paydayserver/voldemotes/create_or_update_voldemote"
	"code.justin.tv/commerce/payday/rpc/paydayserver/voldemotes/create_or_update_voldemote_pack"
	"code.justin.tv/commerce/payday/rpc/paydayserver/voldemotes/list_voldemote_packs_for_broadcaster"
	"code.justin.tv/commerce/payday/rpc/paydayserver/voldemotes/list_voldemote_packs_for_viewer"
	"code.justin.tv/commerce/payday/rpc/paydayserver/voldemotes/list_voldemotes"
	"code.justin.tv/commerce/payday/rpc/paydayserver/voldemotes/moderate_voldemote"
	"code.justin.tv/commerce/payday/rpc/paydayserver/voldemotes/remove_voldemote"
)

type Server struct {
	IsEligibleToUseBitsOnExtensionServer     is_eligible_to_use_bits_on_extension.Server      `inject:""`
	UseBitsOnExtensionServer                 use_bits_on_extension.Server                     `inject:""`
	GetExtensionTransactionsServer           get_extension_transactions.Server                `inject:""`
	GetUserBitsForCampaignServer             get_user_bits_for_campaign.Server                `inject:""`
	GetUserCampaignStatusesServer            get_user_campaign_status.Server                  `inject:""`
	SetUserCampaignStatusServer              set_user_campaign_status.Server                  `inject:""`
	CheckBitsProductsEligibilityServer       check_bits_products_eligibility.Server           `inject:""`
	GetBitsBulkGrantCampaignsServer          get_bits_bulk_grant_campaigns.Server             `inject:""`
	SetUserSettingsServer                    set_user_settings.Server                         `inject:""`
	GetUserSettingsServer                    get_user_settings.Server                         `inject:""`
	GetBadgeTierNotificationServer           get_badge_tier_notification.Server               `inject:""`
	SetBadgeTierNotificationServer           set_badge_tier_notification_state.Server         `inject:""`
	GetUserBitsBadgesLogician                bitsbadges.Logician                              `inject:""`
	GetBadgeTierEmotesInfoByGroupIDServer    get_badge_tier_summary_for_emote_group_id.Server `inject:""`
	GetBadgeTierEmotesServer                 get_badge_tier_emotes.Server                     `inject:""`
	AdminGrantUnlockedBadgeTierRewardsServer admin_grant_unlocked_badge_tier_rewards.Server   `inject:""`
	CreateOrUpdateVoldemoteServer            create_or_update_voldemote.Server                `inject:""`
	ListVoldemotePacksForBroadcasterServer   list_voldemote_packs_for_broadcaster.Server      `inject:""`
	ListVoldemotesServer                     list_voldemotes.Server                           `inject:""`
	ModerateVoldemoteServer                  moderate_voldemote.Server                        `inject:""`
	ListVoldemotePacksForViewerServer        list_voldemote_packs_for_viewer.Server           `inject:""`
	CreateOrUpdateVoldemotePackServer        create_or_update_voldemote_pack.Server           `inject:""`
	RemoveVoldemoteServer                    remove_voldemote.Server                          `inject:""`
	CheckHoldServer                          check_hold.Server                                `inject:""`
	CreateHoldServer                         create_hold.Server                               `inject:""`
	FinalizeHoldServer                       finalize_hold.Server                             `inject:""`
	ReleaseHoldServer                        release_hold.Server                              `inject:""`
	GetTransactionsByIDServer                get_transactions_by_id.Server                    `inject:""`
	UseBitsOnPollServer                      use_bits_on_poll.Server                          `inject:""`
	GetAutoRefillSettingsServer              get_auto_refill_settings.Server                  `inject:""`
	SetAutoRefillSettingsServer              set_auto_refill_settings.Server                  `inject:""`
	GetBalanceServer                         get_balance.Server                               `inject:""`
	GetBitsToBroadcasterServer               get_bits_to_broadcaster.Server                   `inject:""`
	GetCheersServer                          get_cheers.Server                                `inject:""`
	UpdateCheermoteTierServer                update_cheermote_tier.Server                     `inject:""`
	DeleteCheermoteTierServer                delete_cheermote_tier.Server                     `inject:""`
	AssignEmoteToBitsTierServer              assign_emote_to_bits_tier.Server                 `inject:""`
	SearchTransactionsServer                 search_transactions.Server                       `inject:""`
	GetCheerScreenshotsForChannelServer      get_cheer_screenshots_for_channel.Server         `inject:""`
	GetCheerScreenshotServer                 get_cheer_screenshot.Server                      `inject:""`
	GrantPinataBonusBitsServer               grant_pinata_bonus_bits.Server                   `inject:""`
	GetBitsTierEmoteGroupsServer             get_bits_tier_emote_groups.Server                `inject:""`
	MicroCheerServer                         micro_cheer.Server                               `inject:""`
}

func NewServer() paydayrpc.Payday {
	return &Server{}
}

//Test: curl -X "POST" -H "Content-Type:application/json" --data '{}' "http://{host}/twirp/code.justin.tv.commerce.paydayrpc.Payday/HealthCheck"
func (s *Server) HealthCheck(context context.Context, req *paydayrpc.HealthCheckReq) (*paydayrpc.HealthCheckResp, error) {
	return &paydayrpc.HealthCheckResp{
		Message: "OK",
	}, nil
}

func (s *Server) IsEligibleToUseBitsOnExtension(context context.Context, req *paydayrpc.IsEligibleToUseBitsOnExtensionReq) (*paydayrpc.IsEligibleToUseBitsOnExtensionResp, error) {
	return s.IsEligibleToUseBitsOnExtensionServer.IsEligibleToUseBitsOnExtensionImpl(context, req)
}

func (s *Server) UseBitsOnExtension(context context.Context, req *paydayrpc.UseBitsOnExtensionReq) (*paydayrpc.UseBitsOnExtensionResp, error) {
	return s.UseBitsOnExtensionServer.UseBitsOnExtensionImpl(context, req)
}

func (s *Server) GetExtensionTransactions(context context.Context, req *paydayrpc.GetExtensionTransactionsReq) (*paydayrpc.GetExtensionTransactionsResp, error) {
	return s.GetExtensionTransactionsServer.GetExtensionTransactionsImpl(context, req)
}

func (s *Server) GetUserBitsForCampaign(ctx context.Context, req *paydayrpc.GetUserBitsForCampaignReq) (*paydayrpc.GetUserBitsForCampaignResp, error) {
	return s.GetUserBitsForCampaignServer.Get(ctx, req)
}

func (s *Server) GetUserCampaignStatuses(ctx context.Context, req *paydayrpc.GetUserCampaignStatusesReq) (*paydayrpc.GetUserCampaignStatusesResp, error) {
	return s.GetUserCampaignStatusesServer.Get(ctx, req)
}

func (s *Server) SetUserCampaignStatus(ctx context.Context, req *paydayrpc.SetUserCampaignStatusReq) (*paydayrpc.SetUserCampaignStatusResp, error) {
	return s.SetUserCampaignStatusServer.Set(ctx, req)
}

func (s *Server) CheckBitsProductsEligibility(ctx context.Context, req *paydayrpc.CheckBitsProductsEligibilityReq) (*paydayrpc.CheckBitsProductsEligibilityResp, error) {
	return s.CheckBitsProductsEligibilityServer.Check(ctx, req)
}

func (s *Server) GetBitsBulkGrantCampaigns(ctx context.Context, req *paydayrpc.GetBitsBulkGrantCampaignsReq) (*paydayrpc.GetBitsBulkGrantCampaignsResp, error) {
	return s.GetBitsBulkGrantCampaignsServer.Get(ctx, req)
}

func (s *Server) SetUserSettings(ctx context.Context, req *paydayrpc.SetUserSettingsReq) (*paydayrpc.SetUserSettingsResp, error) {
	return s.SetUserSettingsServer.Set(ctx, req)
}

func (s *Server) GetUserSettings(ctx context.Context, req *paydayrpc.GetUserSettingsReq) (*paydayrpc.GetUserSettingsResp, error) {
	return s.GetUserSettingsServer.Get(ctx, req)
}

func (s *Server) GetBadgeTierNotification(ctx context.Context, req *paydayrpc.GetBadgeTierNotificationReq) (*paydayrpc.GetBadgeTierNotificationResp, error) {
	return s.GetBadgeTierNotificationServer.Get(ctx, req)
}

func (s *Server) SetBadgeTierNotificationState(ctx context.Context, req *paydayrpc.SetBadgeTierNotificationStateReq) (*paydayrpc.SetBadgeTierNotificationStateResp, error) {
	return s.SetBadgeTierNotificationServer.SetBadgeTierNotificationStateImpl(ctx, req)
}

func (s *Server) GetUserBitsBadgesAvailableForChannel(ctx context.Context, req *paydayrpc.GetUserBitsBadgesAvailableForChannelReq) (*paydayrpc.GetUserBitsBadgesAvailableForChannelResp, error) {

	availableBadges, err := s.GetUserBitsBadgesLogician.GetUserBitsBadgesAvailableForChannel(ctx, req.UserId, req.ChannelId)
	if err != nil {
		return nil, err
	}

	return &paydayrpc.GetUserBitsBadgesAvailableForChannelResp{
		AvailableBadges: availableBadges,
	}, nil
}

func (s *Server) GetBitsTierSummaryForEmoteGroupID(ctx context.Context, req *paydayrpc.GetBitsTierSummaryForEmoteGroupIDReq) (*paydayrpc.GetBitsTierSummaryForEmoteGroupIDResp, error) {
	return s.GetBadgeTierEmotesInfoByGroupIDServer.Get(ctx, req)
}

func (s *Server) GetBitsBadgeTierEmotes(ctx context.Context, req *paydayrpc.GetBitsBadgeTierEmotesReq) (*paydayrpc.GetBitsBadgeTierEmotesResp, error) {
	return s.GetBadgeTierEmotesServer.Get(ctx, req)
}

func (s *Server) AdminGrantUnlockedBadgeTierRewards(ctx context.Context, req *paydayrpc.AdminGrantUnlockedBadgeTierRewardsReq) (*paydayrpc.AdminGrantUnlockedBadgeTierRewardsResp, error) {
	return s.AdminGrantUnlockedBadgeTierRewardsServer.AdminGrant(ctx, req)
}

func (s *Server) AssignEmoteToBitsTier(ctx context.Context, req *paydayrpc.AssignEmoteToBitsTierReq) (*paydayrpc.AssignEmoteToBitsTierResp, error) {
	return s.AssignEmoteToBitsTierServer.Assign(ctx, req)
}

func (s *Server) GetBitsTierEmoteGroups(ctx context.Context, req *paydayrpc.GetBitsTierEmoteGroupsReq) (*paydayrpc.GetBitsTierEmoteGroupsResp, error) {
	return s.GetBitsTierEmoteGroupsServer.Get(ctx, req)
}

func (s *Server) GetHypedChannels(ctx context.Context, req *paydayrpc.GetHypedChannelsReq) (*paydayrpc.GetHypedChannelsResp, error) {
	return nil, errors.New("deprecated")
}

func (s *Server) CreateOrUpdateVoldemote(ctx context.Context, req *paydayrpc.CreateOrUpdateVoldemoteReq) (*paydayrpc.CreateOrUpdateVoldemoteResp, error) {
	return s.CreateOrUpdateVoldemoteServer.CreateOrUpdate(ctx, req)
}

func (s *Server) ListVoldemotePacksForBroadcaster(ctx context.Context, req *paydayrpc.ListVoldemotePacksForBroadcasterReq) (*paydayrpc.ListVoldemotePacksForBroadcasterResp, error) {
	return s.ListVoldemotePacksForBroadcasterServer.List(ctx, req)
}

func (s *Server) ListVoldemotePacksForViewer(ctx context.Context, req *paydayrpc.ListVoldemotePacksForViewerReq) (*paydayrpc.ListVoldemotePacksForViewerResp, error) {
	return s.ListVoldemotePacksForViewerServer.List(ctx, req)
}

func (s *Server) ListVoldemotes(ctx context.Context, req *paydayrpc.ListVoldemotesReq) (*paydayrpc.ListVoldemotesResp, error) {
	return s.ListVoldemotesServer.List(ctx, req)
}

func (s *Server) ModerateVoldemote(ctx context.Context, req *paydayrpc.ModerateVoldemoteReq) (*paydayrpc.ModerateVoldemoteResp, error) {
	return s.ModerateVoldemoteServer.Moderate(ctx, req)
}

func (s *Server) CreateOrUpdateVoldemotePack(ctx context.Context, req *paydayrpc.CreateOrUpdateVoldemotePackReq) (*paydayrpc.CreateOrUpdateVoldemotePackResp, error) {
	return s.CreateOrUpdateVoldemotePackServer.CreateOrUpdate(ctx, req)
}

func (s *Server) RemoveVoldemote(ctx context.Context, req *paydayrpc.RemoveVoldemoteReq) (*paydayrpc.RemoveVoldemoteResp, error) {
	return s.RemoveVoldemoteServer.Remove(ctx, req)
}

func (s *Server) CheckHold(ctx context.Context, req *paydayrpc.CheckHoldReq) (*paydayrpc.CheckHoldResp, error) {
	return s.CheckHoldServer.Get(ctx, req)
}

func (s *Server) CreateHold(ctx context.Context, req *paydayrpc.CreateHoldReq) (*paydayrpc.CreateHoldResp, error) {
	return s.CreateHoldServer.Create(ctx, req)
}

func (s *Server) FinalizeHold(ctx context.Context, req *paydayrpc.FinalizeHoldReq) (*paydayrpc.FinalizeHoldResp, error) {
	return s.FinalizeHoldServer.Do(ctx, req)
}

func (s *Server) ReleaseHold(ctx context.Context, req *paydayrpc.ReleaseHoldReq) (*paydayrpc.ReleaseHoldResp, error) {
	return s.ReleaseHoldServer.Do(ctx, req)
}

func (s *Server) GetTransactionsByID(ctx context.Context, req *paydayrpc.GetTransactionsByIDReq) (*paydayrpc.GetTransactionsByIDResp, error) {
	return s.GetTransactionsByIDServer.Get(ctx, req)
}

func (s *Server) UseBitsOnPoll(ctx context.Context, req *paydayrpc.UseBitsOnPollReq) (*paydayrpc.UseBitsOnPollResp, error) {
	return s.UseBitsOnPollServer.Use(ctx, req)
}

func (s *Server) GetAutoRefillSettings(ctx context.Context, req *paydayrpc.GetAutoRefillSettingsReq) (*paydayrpc.GetAutoRefillSettingsResp, error) {
	return s.GetAutoRefillSettingsServer.Get(ctx, req)
}

func (s *Server) SetAutoRefillSettings(ctx context.Context, req *paydayrpc.SetAutoRefillSettingsReq) (*paydayrpc.SetAutoRefillSettingsResp, error) {
	return s.SetAutoRefillSettingsServer.Set(ctx, req)
}

func (s *Server) GetBalance(ctx context.Context, req *paydayrpc.GetBalanceReq) (*paydayrpc.GetBalanceResp, error) {
	return s.GetBalanceServer.Get(ctx, req)
}

func (s *Server) GetBitsToBroadcaster(ctx context.Context, req *paydayrpc.GetBitsToBroadcasterReq) (*paydayrpc.GetBitsToBroadcasterResp, error) {
	return s.GetBitsToBroadcasterServer.Get(ctx, req)
}

func (s *Server) GetGlobalCheers(ctx context.Context, req *paydayrpc.GetGlobalCheersReq) (*paydayrpc.GetCheersResp, error) {
	return s.GetCheersServer.GetGlobal(ctx, req)
}

func (s *Server) GetChannelCheers(ctx context.Context, req *paydayrpc.GetChannelCheersReq) (*paydayrpc.GetCheersResp, error) {
	return s.GetCheersServer.GetChannel(ctx, req)
}

func (s *Server) UpdateCheermoteTier(ctx context.Context, req *paydayrpc.UpdateCheermoteTierReq) (*paydayrpc.UpdateCheermoteTierResp, error) {
	return s.UpdateCheermoteTierServer.Update(ctx, req)
}

func (s *Server) DeleteCheermoteTier(ctx context.Context, req *paydayrpc.DeleteCheermoteTierReq) (*paydayrpc.DeleteCheermoteTierResp, error) {
	return s.DeleteCheermoteTierServer.Delete(ctx, req)
}

func (s *Server) SearchTransactions(ctx context.Context, req *paydayrpc.SearchTransactionsReq) (*paydayrpc.SearchTransactionsResp, error) {
	return s.SearchTransactionsServer.Search(ctx, req)
}

func (s *Server) GetCheerScreenshotsForChannel(ctx context.Context, req *paydayrpc.GetCheerScreenshotsForChannelReq) (*paydayrpc.GetCheerScreenshotsForChannelResp, error) {
	return s.GetCheerScreenshotsForChannelServer.Get(ctx, req)
}

func (s *Server) GetCheerScreenshotFromId(ctx context.Context, req *paydayrpc.GetCheerScreenshotFromIdReq) (*paydayrpc.GetCheerScreenshotFromIdResp, error) {
	return s.GetCheerScreenshotServer.Get(ctx, req)
}

func (s *Server) GrantPinataBonusBits(ctx context.Context, req *paydayrpc.GrantPinataBonusBitsReq) (*paydayrpc.GrantPinataBonusBitsResp, error) {
	return s.GrantPinataBonusBitsServer.GrantPinataBonusBits(ctx, req)
}

func (s *Server) MicroCheer(ctx context.Context, req *paydayrpc.MicroCheerReq) (*paydayrpc.MicroCheerResp, error) {
	return s.MicroCheerServer.PerformMicroCheer(ctx, req)
}
