package get_user_settings

import (
	"context"
	"time"

	log "code.justin.tv/commerce/logrus"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"code.justin.tv/commerce/payday/userstate/settings"
	"github.com/twitchtv/twirp"
)

type Server interface {
	Get(ctx context.Context, req *paydayrpc.GetUserSettingsReq) (*paydayrpc.GetUserSettingsResp, error)
}

type ServerImpl struct {
	Getter settings.Getter `inject:""`
}

func (s *ServerImpl) Get(ctx context.Context, req *paydayrpc.GetUserSettingsReq) (*paydayrpc.GetUserSettingsResp, error) {
	ctx, cancel := context.WithTimeout(ctx, 100*time.Millisecond)
	defer cancel()

	if req == nil {
		return nil, twirp.InternalError("cannot have empty request")
	}

	if req.UserId == "" {
		return nil, twirp.InvalidArgumentError("UserId", "cannot have empty user id")
	}

	userSettings, err := s.Getter.Get(ctx, req.UserId)

	if err != nil {
		log.WithError(err).WithFields(log.Fields{"userID": req.UserId}).Error("could not get user settings")
		return nil, twirp.InternalErrorWith(err)
	}

	return &paydayrpc.GetUserSettingsResp{
		SkippedFirstCheerTutorial:   userSettings.SkippedFirstCheerTutorial,
		AbandonedFirstCheerTutorial: userSettings.AbandonedFirstCheerTutorial,
	}, nil
}
