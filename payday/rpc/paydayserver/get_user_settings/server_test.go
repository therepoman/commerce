package get_user_settings

import (
	"context"
	"testing"

	"github.com/stretchr/testify/mock"

	"code.justin.tv/commerce/payday/errors"
	settings_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/userstate/settings"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"code.justin.tv/commerce/payday/userstate/models"
	. "github.com/smartystreets/goconvey/convey"
)

func TestServer_Get(t *testing.T) {
	Convey("given a Get API", t, func() {
		getter := new(settings_mock.Getter)

		api := &ServerImpl{
			Getter: getter,
		}

		userID := "123123123"

		Convey("when the request is nil", func() {
			var req *paydayrpc.GetUserSettingsReq
			_, err := api.Get(context.Background(), req)

			Convey("we should return an error", func() {
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when the user ID is empty", func() {
			req := &paydayrpc.GetUserSettingsReq{}
			_, err := api.Get(context.Background(), req)

			Convey("we should return an error", func() {
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when the user ID is defined", func() {
			req := &paydayrpc.GetUserSettingsReq{
				UserId: userID,
			}

			Convey("when getter returns an error", func() {
				getter.On("Get", mock.Anything, userID).Return(nil, errors.New("ERROR"))
				_, err := api.Get(context.Background(), req)

				Convey("we should return an error", func() {
					So(err, ShouldNotBeNil)
				})
			})

			Convey("when the getter does not return an error", func() {
				getter.On("Get", mock.Anything, userID).Return(&models.UserSettings{
					SkippedFirstCheerTutorial:   true,
					AbandonedFirstCheerTutorial: true,
				}, nil)

				returnValue, err := api.Get(context.Background(), req)

				Convey("we should return the user settings response", func() {
					So(err, ShouldBeNil)
					So(*returnValue, ShouldResemble, paydayrpc.GetUserSettingsResp{
						SkippedFirstCheerTutorial:   true,
						AbandonedFirstCheerTutorial: true,
					})
				})
			})
		})
	})
}
