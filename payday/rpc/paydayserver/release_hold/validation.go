package release_hold

import (
	"context"

	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/payday/holds"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"github.com/twitchtv/twirp"
)

type Validator interface {
	IsValid(ctx context.Context, req *paydayrpc.ReleaseHoldReq) (*paydayrpc.Hold, error)
}

func NewValidator() Validator {
	return &validator{}
}

type validator struct {
	Getter holds.Getter `inject:"holdsGetter"`
}

func (v *validator) IsValid(ctx context.Context, req *paydayrpc.ReleaseHoldReq) (*paydayrpc.Hold, error) {
	if req == nil {
		return nil, twirp.InvalidArgumentError("body", "cannot be blank")
	}

	if strings.Blank(req.UserId) {
		return nil, twirp.InvalidArgumentError("user_id", "cannot be blank")
	}

	if strings.Blank(req.HoldId) {
		return nil, twirp.InvalidArgumentError("hold_id", "cannot be blank")
	}

	hold, err := v.Getter.Get(ctx, req.UserId, req.HoldId)
	if err != nil {
		return nil, twirp.InternalErrorWith(err)
	}

	if hold == nil {
		return nil, twirp.NotFoundError("could not find hold")
	}

	return hold, nil
}
