package release_hold

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/gogogadget/random"
	"code.justin.tv/commerce/payday/clients/cloudwatchlogger"
	holds_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/holds"
	release_hold_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/rpc/paydayserver/release_hold"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	. "github.com/smartystreets/goconvey/convey"
)

func TestServer_Do(t *testing.T) {
	Convey("given a release hold API", t, func() {
		releaser := new(holds_mock.Releaser)
		validator := new(release_hold_mock.Validator)
		cwlogger := cloudwatchlogger.NewCloudWatchLogNoopClient()

		a := server{
			Releaser:    releaser,
			Validator:   validator,
			AuditLogger: cwlogger,
		}

		ctx := context.Background()
		userID := random.String(16)
		holdID := random.String(32)
		req := &paydayrpc.ReleaseHoldReq{
			HoldId: holdID,
			UserId: userID,
		}
		hold := &paydayrpc.Hold{
			HoldId: holdID,
			UserId: userID,
		}

		Convey("when we succeed", func() {
			validator.On("IsValid", ctx, req).Return(hold, nil)
			releaser.On("Release", ctx, userID, holdID, hold).Return(nil)

			res, err := a.Do(ctx, req)
			So(err, ShouldBeNil)
			So(res, ShouldNotBeNil)
			So(res.Hold, ShouldNotBeNil)
			So(res.Hold.IsReleased, ShouldBeTrue)
		})

		Convey("when we fail", func() {
			Convey("when the validator fails", func() {
				validator.On("IsValid", ctx, req).Return(nil, errors.New("WALRUS STRIKE"))
			})

			Convey("when the releaser fails", func() {
				validator.On("IsValid", ctx, req).Return(hold, nil)
				releaser.On("Release", ctx, userID, holdID, hold).Return(errors.New("WALRUS STRIKE"))
			})

			_, err := a.Do(ctx, req)
			So(err, ShouldNotBeNil)
		})
	})
}
