package release_hold

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/gogogadget/random"
	holds_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/holds"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	. "github.com/smartystreets/goconvey/convey"
)

func TestValidator_IsValid(t *testing.T) {
	Convey("given a release hold validator", t, func() {
		getter := new(holds_mock.Getter)

		validator := validator{
			Getter: getter,
		}

		ctx := context.Background()
		userID := random.String(16)
		holdID := random.String(32)

		Convey("when we succeed", func() {
			getter.On("Get", ctx, userID, holdID).Return(&paydayrpc.Hold{
				HoldId: holdID,
				UserId: userID,
			}, nil)

			hold, err := validator.IsValid(ctx, &paydayrpc.ReleaseHoldReq{
				UserId: userID,
				HoldId: holdID,
			})

			So(err, ShouldBeNil)
			So(hold, ShouldNotBeNil)
			So(hold.HoldId, ShouldEqual, holdID)
		})

		Convey("when we fail", func() {
			var req *paydayrpc.ReleaseHoldReq

			Convey("no user ID", func() {
				req = &paydayrpc.ReleaseHoldReq{}
			})

			Convey("no hold ID", func() {
				req = &paydayrpc.ReleaseHoldReq{
					UserId: userID,
				}
			})

			Convey("request body is sane", func() {
				req = &paydayrpc.ReleaseHoldReq{
					UserId: userID,
					HoldId: holdID,
				}

				Convey("hold get errors", func() {
					getter.On("Get", ctx, userID, holdID).Return(nil, errors.New("WALRUS STRIKE"))
				})

				Convey("hold get returns nothing", func() {
					getter.On("Get", ctx, userID, holdID).Return(nil, nil)
				})
			})

			_, err := validator.IsValid(ctx, req)
			So(err, ShouldNotBeNil)
		})
	})
}
