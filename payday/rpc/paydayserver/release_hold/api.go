package release_hold

import (
	"context"
	"fmt"

	"code.justin.tv/commerce/gogogadget/audit"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/clients/cloudwatchlogger"
	"code.justin.tv/commerce/payday/holds"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"github.com/twitchtv/twirp"
)

type Server interface {
	Do(ctx context.Context, req *paydayrpc.ReleaseHoldReq) (*paydayrpc.ReleaseHoldResp, error)
}

func NewServer() Server {
	return &server{}
}

type server struct {
	Validator   Validator                         `inject:""`
	Releaser    holds.Releaser                    `inject:""`
	AuditLogger cloudwatchlogger.CloudWatchLogger `inject:""`
}

func (a *server) Do(ctx context.Context, req *paydayrpc.ReleaseHoldReq) (*paydayrpc.ReleaseHoldResp, error) {
	hold, err := a.Validator.IsValid(ctx, req)
	if err != nil {
		return nil, err
	}

	apiName := "ReleaseHold"
	msg, err := audit.CreateAccessLogMsg(ctx, apiName, *req)
	if err != nil {
		log.WithError(err).Error(fmt.Sprintf("failed to generate access log msg in %s", apiName))
	} else {
		err = a.AuditLogger.Send(ctx, msg)
		if err != nil {
			log.WithError(err).Warn(fmt.Sprintf("error while sending audit log in %s", apiName))
		}
	}

	err = a.Releaser.Release(ctx, req.UserId, req.HoldId, hold)
	if err != nil {
		log.WithFields(log.Fields{
			"userID": req.UserId,
			"holdID": req.HoldId,
		}).WithError(err).Error("failed to release hold")
		return nil, twirp.InternalErrorWith(err)
	}

	return &paydayrpc.ReleaseHoldResp{
		Hold: &paydayrpc.Hold{
			HoldId:     req.HoldId,
			UserId:     req.UserId,
			HasExpired: hold.HasExpired,
			IsReleased: true,
			ExpiresAt:  hold.ExpiresAt,
			Amount:     hold.Amount,
		},
	}, nil
}
