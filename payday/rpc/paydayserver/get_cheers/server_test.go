package get_cheers

import (
	"context"
	"testing"

	"code.justin.tv/commerce/payday/actions/constants"
	"code.justin.tv/commerce/payday/cheers/utils"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/errors"
	channel_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/cache/channel"
	cheers_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/cheers"
	"code.justin.tv/commerce/payday/models"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
	"github.com/twitchtv/twirp"
)

const (
	testTemplate  = "testTemplate"
	testTemplate2 = "testTemplate2"
	testPrefix1   = "testPrefix1"
	testPrefix2   = "testPrefix2"
	testPrefix3   = "testPrefix3"
	testPrefix4   = "testPrefix4"
	testPrefix5   = "testPrefix5"
	testChannelId = "testChannelId"
	testUserId    = "testUserId"

	testCampaignEnabled      = true
	testCampaignId           = "testCampaignId"
	testUsedBits             = 100
	testTotalBits            = 1000
	testMinBitsToBeSponsored = 1
	testUserLimit            = 15
	testBrandName            = "testBrandName"
	testBrandImageUrl        = "test"
	testUserUsedBits         = 200
)

func TestServer_GetGlobalCheers(t *testing.T) {
	Convey("given a server", t, func() {
		globalCheersPoller := new(cheers_mock.GlobalCheersPoller)
		sponsoredCheermoteGetter := new(cheers_mock.SponsoredCheermoteGetter)
		channelCheermoteGetter := new(cheers_mock.ChannelCustomCheermoteGetter)
		channelManager := new(channel_mock.ChannelManager)

		server := server{
			GlobalCheersPoller:       globalCheersPoller,
			SponsoredCheermoteGetter: sponsoredCheermoteGetter,
			ChannelCheermoteGetter:   channelCheermoteGetter,
			ChannelManager:           channelManager,
		}

		Convey("When calling GetGlobal", func() {
			Convey("When global poller has no Cheers", func() {
				globalCheersPoller.On("GetCheers").Return(models.CheerGroup{})
				resp, err := server.GetGlobal(context.Background(), &paydayrpc.GetGlobalCheersReq{})
				So(resp, ShouldBeNil)

				twirpErr, ok := err.(twirp.Error)
				So(ok, ShouldBeTrue)
				So(twirpErr.Code(), ShouldEqual, twirp.Internal)
				So(twirpErr.Msg(), ShouldEqual, "received no global cheers from poller")
			})
			Convey("When global poller has Cheers", func() {
				globalCheersPoller.On("GetCheers").Return(models.CheerGroup{
					Template: testTemplate,
					Cheers:   getGlobalCheerResp(),
				})
				resp, err := server.GetGlobal(context.Background(), &paydayrpc.GetGlobalCheersReq{})
				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)

				// Validate parts of the basic response
				validateBasicResp(resp)

				// Validate the CheerGroups
				So(resp.CheerGroups, ShouldHaveLength, 1)
				So(resp.CheerGroups[0].Template, ShouldEqual, testTemplate)
				So(resp.CheerGroups[0].Cheers, ShouldHaveLength, 5)

				// Validate the Cheers in the CheerGroups
				So(resp.CheerGroups[0].Cheers[0].Type, ShouldEqual, paydayrpc.CheerType_Default)
				So(resp.CheerGroups[0].Cheers[0].Prefix, ShouldEqual, testPrefix1)
				So(resp.CheerGroups[0].Cheers[0].Campaign, ShouldBeNil)
				So(resp.CheerGroups[0].Cheers[0].Tiers, ShouldHaveLength, 1)
				So(resp.CheerGroups[0].Cheers[0].Tiers[0].MinBits, ShouldEqual, 1)
				So(resp.CheerGroups[0].Cheers[0].Tiers[0].ShowInBitsCard, ShouldEqual, true)

				So(resp.CheerGroups[0].Cheers[1].Type, ShouldEqual, paydayrpc.CheerType_GlobalFirstParty)
				So(resp.CheerGroups[0].Cheers[1].Prefix, ShouldEqual, testPrefix2)
				So(resp.CheerGroups[0].Cheers[1].Campaign, ShouldBeNil)
				So(resp.CheerGroups[0].Cheers[1].Tiers, ShouldHaveLength, 2)
				So(resp.CheerGroups[0].Cheers[1].Tiers[0].MinBits, ShouldEqual, 1)
				So(resp.CheerGroups[0].Cheers[1].Tiers[0].ShowInBitsCard, ShouldEqual, true)
				So(resp.CheerGroups[0].Cheers[1].Tiers[1].MinBits, ShouldEqual, 100)
				So(resp.CheerGroups[0].Cheers[1].Tiers[1].ShowInBitsCard, ShouldEqual, false)

				So(resp.CheerGroups[0].Cheers[2].Type, ShouldEqual, paydayrpc.CheerType_GlobalThirdParty)
				So(resp.CheerGroups[0].Cheers[2].Prefix, ShouldEqual, testPrefix3)
				So(resp.CheerGroups[0].Cheers[2].Campaign, ShouldBeNil)
				So(resp.CheerGroups[0].Cheers[2].Tiers, ShouldHaveLength, 3)

				So(resp.CheerGroups[0].Cheers[3].Type, ShouldEqual, paydayrpc.CheerType_DisplayOnly)
				So(resp.CheerGroups[0].Cheers[3].Prefix, ShouldEqual, testPrefix4)
				So(resp.CheerGroups[0].Cheers[3].Campaign, ShouldBeNil)
				So(resp.CheerGroups[0].Cheers[3].Tiers, ShouldHaveLength, 4)

				So(resp.CheerGroups[0].Cheers[4].Type, ShouldEqual, paydayrpc.CheerType_Anonymous)
				So(resp.CheerGroups[0].Cheers[4].Prefix, ShouldEqual, testPrefix5)
				So(resp.CheerGroups[0].Cheers[4].Campaign, ShouldBeNil)
				So(resp.CheerGroups[0].Cheers[4].Tiers, ShouldHaveLength, 4)
			})
		})

		Convey("When calling GetChannel", func() {
			Convey("When no channel is passed in", func() {
				resp, err := server.GetChannel(context.Background(), &paydayrpc.GetChannelCheersReq{})
				So(resp, ShouldBeNil)

				twirpErr, ok := err.(twirp.Error)
				So(ok, ShouldBeTrue)
				So(twirpErr.Code(), ShouldEqual, twirp.InvalidArgument)
				So(twirpErr.Msg(), ShouldEqual, "channel_id is required")
			})
			Convey("When error calling the channel manager", func() {
				channelManager.On("Get", mock.Anything, testChannelId).Return(nil, errors.New(""))
				resp, err := server.GetChannel(context.Background(), &paydayrpc.GetChannelCheersReq{
					ChannelId: testChannelId,
					UserId:    testUserId,
				})
				So(resp, ShouldBeNil)
				So(err, ShouldBeError)
			})
			Convey("When channel manager returns nill", func() {
				channelManager.On("Get", mock.Anything, testChannelId).Return(nil, nil)
				resp, err := server.GetChannel(context.Background(), &paydayrpc.GetChannelCheersReq{
					ChannelId: testChannelId,
					UserId:    testUserId,
				})
				So(err, ShouldBeNil)
				So(resp, ShouldResemble, &paydayrpc.GetCheersResp{})
			})
			Convey("When channel manager returns non-onboarded channel - nil", func() {
				channelManager.On("Get", mock.Anything, testChannelId).Return(&dynamo.Channel{}, nil)
				resp, err := server.GetChannel(context.Background(), &paydayrpc.GetChannelCheersReq{
					ChannelId: testChannelId,
					UserId:    testUserId,
				})
				So(err, ShouldBeNil)
				So(resp, ShouldResemble, &paydayrpc.GetCheersResp{})
			})
			Convey("When channel manager returns non-onboarded channel - false ", func() {
				falseVar := false
				channelManager.On("Get", mock.Anything, testChannelId).Return(&dynamo.Channel{
					Onboarded: &falseVar,
				}, nil)
				resp, err := server.GetChannel(context.Background(), &paydayrpc.GetChannelCheersReq{
					ChannelId: testChannelId,
					UserId:    testUserId,
				})
				So(err, ShouldBeNil)
				So(resp, ShouldResemble, &paydayrpc.GetCheersResp{})
			})
			Convey("When channel manager return opted-out channel", func() {
				falseVar := false
				trueVar := true
				channelManager.On("Get", mock.Anything, testChannelId).Return(&dynamo.Channel{
					Onboarded: &falseVar,
					OptedOut:  &trueVar,
				}, nil)
				resp, err := server.GetChannel(context.Background(), &paydayrpc.GetChannelCheersReq{
					ChannelId: testChannelId,
					UserId:    testUserId,
				})
				So(err, ShouldBeNil)
				So(resp, ShouldResemble, &paydayrpc.GetCheersResp{})
			})
			Convey("When channel manager return bits-enabled channel", func() {
				trueVar := true
				channelManager.On("Get", mock.Anything, testChannelId).Return(&dynamo.Channel{
					Onboarded: &trueVar,
				}, nil)

				Convey("When Sponsored Cheermote getter returns an error", func() {
					sponsoredCheermoteGetter.On("GetSponsoredCheers", mock.Anything, testChannelId, testUserId).Return(nil, errors.New(""))

					resp, err := server.GetChannel(context.Background(), &paydayrpc.GetChannelCheersReq{
						ChannelId: testChannelId,
						UserId:    testUserId,
					})
					So(err, ShouldBeError)
					So(resp, ShouldBeNil)
				})
				Convey("When ChannelCustom Cheermote getter returns an error", func() {
					sponsoredCheermoteGetter.On("GetSponsoredCheers", mock.Anything, testChannelId, testUserId).Return(&models.CheerGroup{
						Template: testTemplate,
						Cheers:   []models.Cheer{},
					}, nil)
					channelCheermoteGetter.On("GetCustomCheermote", mock.Anything, testChannelId, mock.Anything).Return(nil, errors.New(""))

					resp, err := server.GetChannel(context.Background(), &paydayrpc.GetChannelCheersReq{
						ChannelId: testChannelId,
						UserId:    testUserId,
					})
					So(err, ShouldBeError)
					So(resp, ShouldBeNil)
				})
				Convey("When all getter's have no error, but also no data", func() {
					sponsoredCheermoteGetter.On("GetSponsoredCheers", mock.Anything, testChannelId, testUserId).Return(&models.CheerGroup{
						Template: testTemplate,
						Cheers:   []models.Cheer{},
					}, nil)
					channelCheermoteGetter.On("GetCustomCheermote", mock.Anything, testChannelId, mock.Anything).Return(&models.CheerGroup{
						Template: testTemplate2,
						Cheers:   []models.Cheer{},
					}, nil)

					resp, err := server.GetChannel(context.Background(), &paydayrpc.GetChannelCheersReq{
						ChannelId: testChannelId,
						UserId:    testUserId,
					})

					So(err, ShouldBeNil)
					So(resp, ShouldNotBeNil)
					So(resp.CheerGroups, ShouldHaveLength, 1)

					// Validate the basic parts of the response
					validateBasicResp(resp)
				})
				Convey("When all getter's have no error, with data", func() {
					sponsoredCheermoteGetter.On("GetSponsoredCheers", mock.Anything, testChannelId, testUserId).Return(&models.CheerGroup{
						Template: testTemplate,
						Cheers: []models.Cheer{
							{
								Prefix: testPrefix1,
								Type:   models.CheerTypeSponsored,
								Campaign: &models.CheerCampaign{
									IsEnabled:            testCampaignEnabled,
									ID:                   testCampaignId,
									TotalBits:            testTotalBits,
									UsedBits:             testUsedBits,
									MinBitsToBeSponsored: testMinBitsToBeSponsored,
									SponsoredAmountThresholds: map[int64]float64{
										10: 1.0,
									},
									UserLimit:     testUserLimit,
									BrandName:     testBrandName,
									BrandImageURL: testBrandImageUrl,
									UserUsedBits:  testUserUsedBits,
								},
							},
						},
					}, nil)
					channelCheermoteGetter.On("GetCustomCheermote", mock.Anything, testChannelId, mock.Anything).Return(&models.CheerGroup{
						Template: testTemplate2,
						Cheers: []models.Cheer{
							{
								Prefix: testPrefix2,
								Type:   models.CheerTypeChannelCustom,
							},
						},
					}, nil)

					resp, err := server.GetChannel(context.Background(), &paydayrpc.GetChannelCheersReq{
						ChannelId: testChannelId,
						UserId:    testUserId,
					})

					So(err, ShouldBeNil)
					So(resp, ShouldNotBeNil)
					So(resp.CheerGroups, ShouldHaveLength, 3)

					// Validate the basic parts of the response
					validateBasicResp(resp)

					// Validate sponsored cheermote
					So(resp.CheerGroups[0].Template, ShouldEqual, testTemplate)
					So(resp.CheerGroups[0].Cheers, ShouldHaveLength, 1)
					So(resp.CheerGroups[0].Cheers[0].Prefix, ShouldEqual, testPrefix1)
					So(resp.CheerGroups[0].Cheers[0].Type, ShouldEqual, paydayrpc.CheerType_Sponsored)
					So(resp.CheerGroups[0].Cheers[0].Campaign, ShouldResemble, &paydayrpc.CheerCampaign{
						IsEnabled:            testCampaignEnabled,
						Id:                   testCampaignId,
						TotalBits:            testTotalBits,
						UsedBits:             testUsedBits,
						MinBitsToBeSponsored: testMinBitsToBeSponsored,
						SponsoredAmountThresholds: map[int64]float64{
							10: 1.0,
						},
						UserLimit:     testUserLimit,
						BrandName:     testBrandName,
						BrandImageUrl: testBrandImageUrl,
						UserUsedBits:  testUserUsedBits,
					})

					// Validate Custom Cheermote
					So(resp.CheerGroups[1].Template, ShouldEqual, testTemplate2)
					So(resp.CheerGroups[1].Cheers, ShouldHaveLength, 1)
					So(resp.CheerGroups[1].Cheers[0].Prefix, ShouldEqual, testPrefix2)
					So(resp.CheerGroups[1].Cheers[0].Type, ShouldEqual, paydayrpc.CheerType_ChannelCustom)

					// Validate Charity Cheermote
					So(resp.CheerGroups[2].Template, ShouldEqual, utils.GetGlobalActionTemplate())
					So(resp.CheerGroups[2].Cheers, ShouldHaveLength, 1)
					So(resp.CheerGroups[2].Cheers[0].Prefix, ShouldEqual, "Charity")
					So(resp.CheerGroups[2].Cheers[0].Type, ShouldEqual, paydayrpc.CheerType_DisplayOnly)
				})
			})
		})
	})
}

func validateBasicResp(resp *paydayrpc.GetCheersResp) {
	So(resp.CheerTypeDisplayOrder, ShouldResemble, []paydayrpc.CheerType{
		paydayrpc.CheerType_Sponsored,
		paydayrpc.CheerType_Default,
		paydayrpc.CheerType_ChannelCustom,
		paydayrpc.CheerType_Charity,
		paydayrpc.CheerType_GlobalFirstParty,
		paydayrpc.CheerType_GlobalThirdParty,
		paydayrpc.CheerType_Anonymous,
		paydayrpc.CheerType_DisplayOnly,
	})
	So(resp.CheerTierColors, ShouldResemble, map[int32]string{
		1:      constants.Tier1Color,
		100:    constants.Tier100Color,
		1000:   constants.Tier1000Color,
		5000:   constants.Tier5000Color,
		10000:  constants.Tier10000Color,
		100000: constants.Tier100000Color,
	})
	So(resp.Backgrounds, ShouldResemble, []string{
		string(constants.LightBackground),
		string(constants.DarkBackground),
	})
	So(resp.Scales, ShouldResemble, []string{
		string(constants.Scale1),
		string(constants.Scale15),
		string(constants.Scale2),
		string(constants.Scale3),
		string(constants.Scale4),
	})
	So(resp.DisplayTypes, ShouldResemble, []*paydayrpc.DisplayType{
		{
			Type:      string(constants.StaticAnimationType),
			Extension: string(constants.StaticExtensionType),
		},
		{
			Type:      string(constants.AnimatedAnimationType),
			Extension: string(constants.AnimatedExtensionType),
		},
	})
}

func getGlobalCheerResp() []models.Cheer {
	return []models.Cheer{
		{
			Prefix: testPrefix1,
			Type:   models.CheerTypeDefaultCheer,
			Tiers: []models.CheerTier{
				{
					MinBits:        1,
					ShowInBitsCard: true,
				},
			},
		}, {
			Prefix: testPrefix2,
			Type:   models.CheerTypeGlobalFirstParty,
			Tiers: []models.CheerTier{
				{
					MinBits:        1,
					ShowInBitsCard: true,
				},
				{
					MinBits:        100,
					ShowInBitsCard: false,
				},
			},
		},
		{
			Prefix: testPrefix3,
			Type:   models.CheerTypeGlobalThirdParty,
			Tiers: []models.CheerTier{
				{
					MinBits:        1,
					ShowInBitsCard: true,
				},
				{
					MinBits:        100,
					ShowInBitsCard: true,
				},
				{
					MinBits:        1000,
					ShowInBitsCard: true,
				},
			},
		},
		{
			Prefix: testPrefix4,
			Type:   models.CheerTypeDisplayOnly,
			Tiers: []models.CheerTier{
				{
					MinBits:        1,
					ShowInBitsCard: true,
				},
				{
					MinBits:        100,
					ShowInBitsCard: true,
				},
				{
					MinBits:        1000,
					ShowInBitsCard: true,
				},
				{
					MinBits:        5000,
					ShowInBitsCard: true,
				},
			},
		},
		{
			Prefix: testPrefix5,
			Type:   models.CheerTypeAnonymousCheer,
			Tiers: []models.CheerTier{
				{
					MinBits:        1,
					ShowInBitsCard: true,
				},
				{
					MinBits:        100,
					ShowInBitsCard: true,
				},
				{
					MinBits:        1000,
					ShowInBitsCard: true,
				},
				{
					MinBits:        5000,
					ShowInBitsCard: true,
				},
			},
		},
	}
}
