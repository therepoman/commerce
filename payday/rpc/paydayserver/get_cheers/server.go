package get_cheers

import (
	"context"
	"strconv"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/actions/constants"
	"code.justin.tv/commerce/payday/cache/channel"
	"code.justin.tv/commerce/payday/cheers"
	"code.justin.tv/commerce/payday/cheers/utils"
	"code.justin.tv/commerce/payday/dynamo/actions"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/models/api"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"github.com/twitchtv/twirp"
)

type Server interface {
	GetGlobal(ctx context.Context, req *paydayrpc.GetGlobalCheersReq) (*paydayrpc.GetCheersResp, error)
	GetChannel(ctx context.Context, req *paydayrpc.GetChannelCheersReq) (*paydayrpc.GetCheersResp, error)
}

type server struct {
	GlobalCheersPoller       cheers.GlobalCheersPoller           `inject:""`
	SponsoredCheermoteGetter cheers.SponsoredCheermoteGetter     `inject:""`
	ChannelCheermoteGetter   cheers.ChannelCustomCheermoteGetter `inject:""`
	ChannelManager           channel.ChannelManager              `inject:""`
}

func NewServer() Server {
	return &server{}
}

func (s *server) GetGlobal(ctx context.Context, req *paydayrpc.GetGlobalCheersReq) (*paydayrpc.GetCheersResp, error) {
	// This just retrieves a value from an im-memory cache, so there is no way to propagate the context. P99 is ~1ms
	// The Global call don't say anything about if a Channel is CheerEnabled
	resp := s.GlobalCheersPoller.GetCheers()
	if len(resp.Cheers) == 0 {
		return nil, twirp.InternalError("received no global cheers from poller")
	}
	return buildGetCheersResp(resp), nil
}

func (s *server) GetChannel(ctx context.Context, req *paydayrpc.GetChannelCheersReq) (*paydayrpc.GetCheersResp, error) {
	if req.ChannelId == "" {
		return nil, twirp.RequiredArgumentError("channel_id")
	}

	// Will also set 400ms in GQL
	ctx, cancel := context.WithTimeout(ctx, 400*time.Millisecond)
	defer cancel()

	channelInfo, err := s.ChannelManager.Get(ctx, req.ChannelId)
	if err != nil {
		return nil, err
	}

	// If the channel is missing or has not been onboarded to bits
	if channelInfo == nil || channelInfo.Onboarded == nil || !*channelInfo.Onboarded || (channelInfo.OptedOut != nil && *channelInfo.OptedOut) {
		return &paydayrpc.GetCheersResp{}, nil
	}

	var groupsToBuild []models.CheerGroup
	sponsoredCheerGroup, err := s.SponsoredCheermoteGetter.GetSponsoredCheers(ctx, req.ChannelId, req.UserId)
	if err != nil {
		return nil, err
	}
	if sponsoredCheerGroup != nil && len(sponsoredCheerGroup.Cheers) > 0 {
		groupsToBuild = append(groupsToBuild, *sponsoredCheerGroup)
	}

	channelCustomCheerGroup, err := s.ChannelCheermoteGetter.GetCustomCheermote(ctx, req.ChannelId, channelInfo)
	if err != nil {
		return nil, err
	}
	if channelCustomCheerGroup != nil && len(channelCustomCheerGroup.Cheers) > 0 {
		groupsToBuild = append(groupsToBuild, *channelCustomCheerGroup)
	}

	// Still return charity just so that VODs don't break
	groupsToBuild = append(groupsToBuild, models.CheerGroup{
		Template: utils.GetGlobalActionTemplate(),
		Cheers: []models.Cheer{utils.ConvertDynamoActionToCheer(&actions.Action{
			Prefix:          "charity",
			Type:            api.DisplayOnlyAction,
			CanonicalCasing: "Charity",
			StartTime:       time.Date(2018, 9, 10, 0, 0, 0, 0, time.UTC),
		})},
	})

	return buildGetCheersResp(groupsToBuild...), nil
}

func buildGetCheersResp(cheerGroups ...models.CheerGroup) *paydayrpc.GetCheersResp {
	var twirpCheerGroups []*paydayrpc.CheerGroup
	for _, apiCheerGroup := range cheerGroups {
		twirpCheerGroups = append(twirpCheerGroups, convertModelCheerGroupToTwirpCheerGroup(apiCheerGroup))
	}

	return &paydayrpc.GetCheersResp{
		CheerTypeDisplayOrder: convertModelCheerOrderToTwirpCheerOrder(utils.BuildDefaultCheerOrder()),
		Scales:                convertModelScalesToTwirpScales(utils.BuildDefaultScales()),
		DisplayTypes:          convertModelDisplayTypeToTwirpDisplayType(utils.BuildDefaultDisplayTypes()),
		Backgrounds:           convertModelBackgroundToTwirpBackground(utils.BuildDefaultBackgrounds()),
		CheerTierColors:       convertModelTierColorMapToTwirpTierColorMap(),
		CheerGroups:           twirpCheerGroups,
	}
}

func convertModelCheerTypeToTwirpCheerType(cheerType models.CheerType) paydayrpc.CheerType {
	switch cheerType {
	case models.CheerTypeChannelCustom:
		return paydayrpc.CheerType_ChannelCustom
	case models.CheerTypeGlobalFirstParty:
		return paydayrpc.CheerType_GlobalFirstParty
	case models.CheerTypeGlobalThirdParty:
		return paydayrpc.CheerType_GlobalThirdParty
	case models.CheerTypeSponsored:
		return paydayrpc.CheerType_Sponsored
	case models.CheerTypeDisplayOnly:
		return paydayrpc.CheerType_DisplayOnly
	case models.CheerTypeDefaultCheer:
		return paydayrpc.CheerType_Default
	case models.CheerTypeCharity:
		return paydayrpc.CheerType_Charity
	case models.CheerTypeAnonymousCheer:
		return paydayrpc.CheerType_Anonymous
	// If we don't match one of these, return DisplayOnly for safety
	default:
		return paydayrpc.CheerType_DisplayOnly
	}
}

func convertModelTiersToTwirpCheers(apiCheerTiers []models.CheerTier) []*paydayrpc.CheerTier {
	var twirpCheerTiers []*paydayrpc.CheerTier
	for _, apiCheerTier := range apiCheerTiers {
		tier := &paydayrpc.CheerTier{
			MinBits: apiCheerTier.MinBits,
		}

		if apiCheerTier.ShowInBitsCard {
			tier.ShowInBitsCard = true
		}
		twirpCheerTiers = append(twirpCheerTiers, tier)
	}
	return twirpCheerTiers
}

func convertModelCheerCampaignToTwirpCampaign(campaign models.CheerCampaign) *paydayrpc.CheerCampaign {
	return &paydayrpc.CheerCampaign{
		IsEnabled:                 campaign.IsEnabled,
		Id:                        campaign.ID,
		TotalBits:                 campaign.TotalBits,
		UsedBits:                  campaign.UsedBits,
		MinBitsToBeSponsored:      campaign.MinBitsToBeSponsored,
		SponsoredAmountThresholds: campaign.SponsoredAmountThresholds,
		UserLimit:                 campaign.UserLimit,
		BrandName:                 campaign.BrandName,
		BrandImageUrl:             campaign.BrandImageURL,
		UserUsedBits:              campaign.UserUsedBits,
	}
}

func convertModelCheersToTwirpCheers(cheers []models.Cheer) []*paydayrpc.Cheer {
	var twirpCheerArray []*paydayrpc.Cheer
	for _, apiCheer := range cheers {
		twirpCheerArray = append(twirpCheerArray, convertModelCheerToTwirpCheer(apiCheer))
	}
	return twirpCheerArray
}

func convertModelCheerToTwirpCheer(cheer models.Cheer) *paydayrpc.Cheer {
	twirpCheer := &paydayrpc.Cheer{
		Prefix: cheer.Prefix,
		Type:   convertModelCheerTypeToTwirpCheerType(cheer.Type),
		Tiers:  convertModelTiersToTwirpCheers(cheer.Tiers),
	}

	if cheer.Campaign != nil {
		twirpCheer.Campaign = convertModelCheerCampaignToTwirpCampaign(*cheer.Campaign)
	}
	return twirpCheer
}

func convertModelCheerGroupToTwirpCheerGroup(cheerGroup models.CheerGroup) *paydayrpc.CheerGroup {
	twirpCheerGroup := &paydayrpc.CheerGroup{
		Template: cheerGroup.Template,
		Cheers:   convertModelCheersToTwirpCheers(cheerGroup.Cheers),
	}

	return twirpCheerGroup
}

func convertModelCheerOrderToTwirpCheerOrder(orders []models.CheerType) []paydayrpc.CheerType {
	twirpOrders := make([]paydayrpc.CheerType, 0)
	for i, order := range orders {
		twirpOrder := convertModelCheerTypeToTwirpCheerType(order)

		// If we get back a DisplayOnly type that isn't in the last position, it means that someone has added a new
		// model CheerType without adding the corresponding Twirp CheerType, as the convert function will fallback to using
		// DisplayOnly. In this case, don't add the duplicated DisplayOnly to the CheerOrder
		if twirpOrder != paydayrpc.CheerType_DisplayOnly || i == len(orders)-1 {
			twirpOrders = append(twirpOrders, twirpOrder)
		}
	}
	return twirpOrders
}

func convertModelScalesToTwirpScales(scales []api.Scale) []string {
	twirpScales := make([]string, len(scales))
	for i, scale := range scales {
		twirpScales[i] = string(scale)
	}
	return twirpScales
}

func convertModelBackgroundToTwirpBackground(backgrounds []api.Background) []string {
	twirpScales := make([]string, len(backgrounds))
	for i, background := range backgrounds {
		twirpScales[i] = string(background)
	}
	return twirpScales
}

func convertModelDisplayTypeToTwirpDisplayType(displayTypes []models.DisplayType) []*paydayrpc.DisplayType {
	twirpScales := make([]*paydayrpc.DisplayType, len(displayTypes))
	for i, displayType := range displayTypes {
		twirpScales[i] = &paydayrpc.DisplayType{
			Type:      string(displayType.AnimationType),
			Extension: string(displayType.Extension),
		}
	}
	return twirpScales
}

func convertModelTierColorMapToTwirpTierColorMap() map[int32]string {
	twirpTierColorMap := make(map[int32]string)
	for tier, color := range constants.TierToColor {
		tierAsInt, err := strconv.ParseInt(string(tier), 10, 32)
		if err != nil {
			// We don't want this error type to actually cause the response to error. Just log + remove it from the list
			log.WithError(err).Errorf("Tier %s cannot be interpreted as an int32 and will be dropped from the color list", tier)
		} else {
			twirpTierColorMap[int32(tierAsInt)] = color
		}
	}
	return twirpTierColorMap
}
