package set_user_settings

import (
	"context"

	log "code.justin.tv/commerce/logrus"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"code.justin.tv/commerce/payday/userstate/settings"
	"github.com/twitchtv/twirp"
)

type Server interface {
	Set(ctx context.Context, req *paydayrpc.SetUserSettingsReq) (*paydayrpc.SetUserSettingsResp, error)
}

type ServerImpl struct {
	Setter settings.Setter `inject:""`
}

func (s *ServerImpl) Set(ctx context.Context, req *paydayrpc.SetUserSettingsReq) (*paydayrpc.SetUserSettingsResp, error) {
	if req == nil {
		return nil, twirp.InternalError("cannot have empty request")
	}

	if req.UserId == "" {
		return nil, twirp.InvalidArgumentError("UserId", "cannot have empty user id")
	}

	err := s.Setter.Set(ctx, req)

	if err != nil {
		log.WithError(err).WithFields(log.Fields{"userID": req.UserId}).Error("could not set user settings")
		return nil, twirp.InternalErrorWith(err)
	}

	return &paydayrpc.SetUserSettingsResp{}, nil
}
