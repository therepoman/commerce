package set_user_settings

import (
	"context"
	"testing"

	"github.com/stretchr/testify/mock"

	"code.justin.tv/commerce/payday/errors"
	settings_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/userstate/settings"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	. "github.com/smartystreets/goconvey/convey"
)

func TestServer_Set(t *testing.T) {
	Convey("given a Set API", t, func() {
		setter := new(settings_mock.Setter)

		api := &ServerImpl{
			Setter: setter,
		}

		userID := "123123123"

		Convey("when the request is nil", func() {
			var req *paydayrpc.SetUserSettingsReq
			_, err := api.Set(context.Background(), req)

			Convey("we should return an error", func() {
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when the user ID is empty", func() {
			req := &paydayrpc.SetUserSettingsReq{}
			_, err := api.Set(context.Background(), req)

			Convey("we should return an error", func() {
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when the user ID is defined", func() {
			req := &paydayrpc.SetUserSettingsReq{
				UserId: userID,
			}

			Convey("when setter returns an error", func() {
				setter.On("Set", mock.Anything, req).Return(errors.New("ERROR"))
				_, err := api.Set(context.Background(), req)

				Convey("we should return an error", func() {
					So(err, ShouldNotBeNil)
				})
			})

			Convey("when the setter does not return an error", func() {
				setter.On("Set", mock.Anything, req).Return(nil)

				returnValue, err := api.Set(context.Background(), req)

				Convey("we should return the user settings response", func() {
					So(err, ShouldBeNil)
					So(*returnValue, ShouldResemble, paydayrpc.SetUserSettingsResp{})
				})
			})
		})
	})
}
