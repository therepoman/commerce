package get_badge_tier_summary_for_emote_group_id

import (
	"context"

	log "code.justin.tv/commerce/logrus"
	badgetieremotes "code.justin.tv/commerce/payday/badgetiers/products/emotes"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"github.com/twitchtv/twirp"
)

type Server interface {
	Get(ctx context.Context, req *paydayrpc.GetBitsTierSummaryForEmoteGroupIDReq) (*paydayrpc.GetBitsTierSummaryForEmoteGroupIDResp, error)
}

type ServerImpl struct {
	EmotesLogician badgetieremotes.Logician `inject:""`
}

func (s *ServerImpl) Get(ctx context.Context, req *paydayrpc.GetBitsTierSummaryForEmoteGroupIDReq) (*paydayrpc.GetBitsTierSummaryForEmoteGroupIDResp, error) {
	if req == nil {
		return nil, twirp.InternalError("cannot have empty request")
	}

	if req.GroupId == "" {
		return nil, twirp.InvalidArgumentError("GroupID", "cannot have empty group id")
	}

	var userID *string
	if req.UserId != "" {
		userID = &req.UserId
	}

	emotesSummary, wasNotFound, err := s.EmotesLogician.GetBitsTierSummaryForEmoteGroupID(ctx, req.GroupId, userID)
	if err != nil {
		if wasNotFound {
			return nil, twirp.NotFoundError("no badge tier summary found for groupID")
		}
		log.WithError(err).WithFields(log.Fields{
			"groupID": req.GroupId,
			"userID":  req.UserId,
		}).Error("could not get badge tier emote info by groupID")

		return nil, twirp.InternalErrorWith(err)
	}

	return emotesSummary, nil
}
