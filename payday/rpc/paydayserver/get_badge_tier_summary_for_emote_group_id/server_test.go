package get_badge_tier_summary_for_emote_group_id

import (
	"context"
	"errors"
	"testing"

	badgetieremotes_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/badgetiers/products/emotes"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"github.com/guregu/dynamo"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
	"github.com/twitchtv/twirp"
)

func TestServer_Get(t *testing.T) {
	Convey("given a Get API", t, func() {
		logicianMock := new(badgetieremotes_mock.Logician)

		api := &ServerImpl{
			EmotesLogician: logicianMock,
		}

		groupID := "1234567890"
		var emptyStringPointer *string

		Convey("when the request is nil", func() {
			var req *paydayrpc.GetBitsTierSummaryForEmoteGroupIDReq
			_, err := api.Get(context.Background(), req)

			Convey("we should return an error", func() {
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when groupID is empty", func() {
			req := &paydayrpc.GetBitsTierSummaryForEmoteGroupIDReq{}
			_, err := api.Get(context.Background(), req)

			Convey("we should return an error", func() {
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when the logician errors", func() {
			req := &paydayrpc.GetBitsTierSummaryForEmoteGroupIDReq{
				GroupId: groupID,
			}

			logicianMock.On("GetBitsTierSummaryForEmoteGroupID", mock.Anything, groupID, emptyStringPointer).Return(nil, false, errors.New("test error"))

			resp, err := api.Get(context.Background(), req)

			Convey("we should return an error", func() {
				So(resp, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when the logician returns not found error", func() {
			req := &paydayrpc.GetBitsTierSummaryForEmoteGroupIDReq{
				GroupId: groupID,
			}

			logicianMock.On("GetBitsTierSummaryForEmoteGroupID", mock.Anything, groupID, emptyStringPointer).Return(nil, true, dynamo.ErrNotFound)

			resp, err := api.Get(context.Background(), req)

			Convey("we should return an error", func() {
				So(resp, ShouldBeNil)
				So(err, ShouldNotBeNil)

				twerr, ok := err.(twirp.Error)

				So(ok, ShouldBeTrue)
				So(twerr.Code(), ShouldEqual, twirp.NotFound)
			})
		})

		Convey("when the logician returns empty", func() {
			req := &paydayrpc.GetBitsTierSummaryForEmoteGroupIDReq{
				GroupId: groupID,
			}

			logicianMock.On("GetBitsTierSummaryForEmoteGroupID", mock.Anything, groupID, emptyStringPointer).Return(nil, false, nil)

			resp, err := api.Get(context.Background(), req)

			Convey("we should return nothing", func() {
				So(resp, ShouldBeNil)
				So(err, ShouldBeNil)
			})
		})

		Convey("when the loigician succeeds", func() {
			req := &paydayrpc.GetBitsTierSummaryForEmoteGroupIDReq{
				GroupId: groupID,
			}

			emotesInfo := &paydayrpc.GetBitsTierSummaryForEmoteGroupIDResp{
				GroupId:                 "1754946665",
				BadgeTierThreshold:      1000,
				EmoteChannelId:          "127380717",
				IsEmoteUnlockedForUser:  false,
				NumberOfBitsUntilUnlock: 1000,
			}

			logicianMock.On("GetBitsTierSummaryForEmoteGroupID", mock.Anything, groupID, emptyStringPointer).Return(emotesInfo, false, nil)

			resp, err := api.Get(context.Background(), req)

			Convey("we should return the emote info", func() {
				So(resp, ShouldNotBeNil)
				So(err, ShouldBeNil)
			})
		})
	})
}
