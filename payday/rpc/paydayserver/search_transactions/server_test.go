package search_transactions

import (
	"context"
	"testing"
	"time"

	"code.justin.tv/commerce/payday/errors"
	transactions_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/transactions"
	transaction_models "code.justin.tv/commerce/payday/models"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestServer_Search(t *testing.T) {
	Convey("given a Search API", t, func() {
		getter := new(transactions_mock.Getter)

		api := &ServerImpl{
			Getter: getter,
		}

		userID := "123123123"
		cursor := "cursor-abc"
		var emptyTime *time.Time

		Convey("when the request is nil, we should error", func() {
			_, err := api.Search(context.Background(), nil)
			So(err, ShouldNotBeNil)
		})

		Convey("when the userID and ChannelID is empty, we should error", func() {
			_, err := api.Search(context.Background(), &paydayrpc.SearchTransactionsReq{
				SearchType: paydayrpc.SearchTransactionType_ONSITE,
			})
			So(err, ShouldNotBeNil)
		})

		Convey("when the search type is unknown, we should error", func() {
			_, err := api.Search(context.Background(), &paydayrpc.SearchTransactionsReq{
				SearchType: 3,
				UserFilter: &paydayrpc.SearchTransactionsReq_UserId{
					UserId: userID,
				},
			})
			So(err, ShouldNotBeNil)
		})

		Convey("when the request is valid", func() {
			req := &paydayrpc.SearchTransactionsReq{
				SearchType: paydayrpc.SearchTransactionType_ONSITE,
				UserFilter: &paydayrpc.SearchTransactionsReq_UserId{
					UserId: userID,
				},
				Cursor: cursor,
			}

			Convey("when getter returns an error, we should error", func() {
				getter.On("GetLastUpdatedTransactionsByUserOrChannel", mock.Anything, userID, "", cursor, emptyTime, emptyTime, transaction_models.TransactionSearchTypeOnsite).Return(nil, "", errors.New("ERROR"))
				_, err := api.Search(context.Background(), req)
				So(err, ShouldNotBeNil)
			})

			Convey("when the getter does not return an error, we should return transactions and a cursor", func() {
				mockTransactions := []*paydayrpc.Transaction{
					{},
				}
				mockNewCursor := "cursor-def"
				getter.On("GetLastUpdatedTransactionsByUserOrChannel", mock.Anything, userID, "", cursor, emptyTime, emptyTime, transaction_models.TransactionSearchTypeOnsite).Return(mockTransactions, mockNewCursor, nil)
				resp, err := api.Search(context.Background(), req)

				So(err, ShouldBeNil)
				So(resp.Transactions, ShouldHaveLength, 1)
				So(resp.Cursor, ShouldEqual, mockNewCursor)
			})
		})
	})
}
