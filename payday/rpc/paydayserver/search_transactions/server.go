package search_transactions

import (
	"context"
	"time"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/models"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	transactionslogic "code.justin.tv/commerce/payday/transactions"
	"github.com/golang/protobuf/ptypes"
	"github.com/twitchtv/twirp"
)

type Server interface {
	Search(ctx context.Context, req *paydayrpc.SearchTransactionsReq) (*paydayrpc.SearchTransactionsResp, error)
}

type ServerImpl struct {
	Getter transactionslogic.Getter `inject:""`
}

func (s *ServerImpl) Search(ctx context.Context, req *paydayrpc.SearchTransactionsReq) (*paydayrpc.SearchTransactionsResp, error) {
	ctx, cancel := context.WithTimeout(ctx, 500*time.Millisecond)
	defer cancel()

	if req == nil {
		return nil, twirp.InternalError("cannot have empty request")
	}

	userID := req.GetUserId()
	channelID := req.GetChannelId()

	if userID == "" && channelID == "" {
		return nil, twirp.NewError(twirp.InvalidArgument, "must provide a user_id or channel_id")
	}

	if userID != "" && channelID != "" {
		return nil, twirp.NewError(twirp.InvalidArgument, "must provide a user_id or channel_id, but not both")
	}

	var eventAfter *time.Time
	if req.EventAfter != nil {
		timestamp, err := ptypes.Timestamp(req.EventAfter)
		if err != nil {
			return nil, twirp.InvalidArgumentError("EventAfter", "must provide a valid timestamp")
		}
		eventAfter = &timestamp
	}

	var eventBefore *time.Time
	if req.EventBefore != nil {
		timestamp, err := ptypes.Timestamp(req.EventBefore)
		if err != nil {
			return nil, twirp.InvalidArgumentError("EventBefore", "must provide a valid timestamp")
		}
		eventBefore = &timestamp
	}

	var searchType models.TransactionSearchType
	switch req.SearchType {
	case paydayrpc.SearchTransactionType_EXTENSION:
		searchType = models.TransactionSearchTypeExtension
	case paydayrpc.SearchTransactionType_ONSITE:
		searchType = models.TransactionSearchTypeOnsite
	default:
		return nil, twirp.InvalidArgumentError("SearchType", "unsupported search_type")
	}

	transactions, cursor, err := s.Getter.GetLastUpdatedTransactionsByUserOrChannel(ctx, userID, channelID, req.Cursor, eventBefore, eventAfter, searchType)

	if err != nil {
		logrus.WithError(err).WithField("req", *req).Error("failed to search transactions")

		return nil, twirp.InternalErrorWith(err)
	}

	return &paydayrpc.SearchTransactionsResp{
		Transactions: transactions,
		Cursor:       cursor,
	}, nil
}
