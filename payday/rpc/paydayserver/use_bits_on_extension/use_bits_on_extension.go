package use_bits_on_extension

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"code.justin.tv/commerce/gogogadget/audit"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	"code.justin.tv/commerce/payday/api/constants"
	"code.justin.tv/commerce/payday/balance"
	"code.justin.tv/commerce/payday/clients/cloudwatchlogger"
	"code.justin.tv/commerce/payday/clients/pachinko"
	"code.justin.tv/commerce/payday/clients/pantheon"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/extension/pubsub"
	"code.justin.tv/commerce/payday/extension/transaction"
	"code.justin.tv/commerce/payday/lock"
	"code.justin.tv/commerce/payday/metrics"
	"code.justin.tv/commerce/payday/models"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"code.justin.tv/commerce/payday/sns"
	stringutils "code.justin.tv/commerce/payday/utils/strings"
	eventbus "code.justin.tv/eventbus/client"
	"code.justin.tv/eventbus/schema/pkg/bits_use"
	"github.com/twitchtv/twirp"
)

const (
	timeoutDuration       = 5 * time.Second
	MaximumTransactionAge = 1 * time.Hour
	RedisLockKeyPrefix    = "use-bits-in-extension"
)

type Server interface {
	UseBitsOnExtensionImpl(ctx context.Context, req *paydayrpc.UseBitsOnExtensionReq) (*paydayrpc.UseBitsOnExtensionResp, error)
}

type ServerImpl struct {
	BalanceGetter               balance.Getter                    `inject:""`
	ExtensionTransactionManager transaction.Manager               `inject:""`
	ExtensionsPubsubManager     pubsub.Manager                    `inject:""`
	EventBusClient              eventbus.Publisher                `inject:""`
	SNSClient                   sns.ISNSClient                    `inject:"usWestSNSClient"`
	Config                      *config.Configuration             `inject:""`
	Pantheon                    pantheon.IPantheonClientWrapper   `inject:""`
	Pachinko                    pachinko.PachinkoClient           `inject:""`
	AuditLogger                 cloudwatchlogger.CloudWatchLogger `inject:""`
	LockingClient               lock.LockingClient                `inject:""`
	Statter                     metrics.Statter                   `inject:""`
}

func (s *ServerImpl) UseBitsOnExtensionImpl(ctx context.Context, req *paydayrpc.UseBitsOnExtensionReq) (*paydayrpc.UseBitsOnExtensionResp, error) {
	ctx, cancel := context.WithTimeout(ctx, timeoutDuration)
	defer cancel()

	if req == nil {
		return nil, twirp.InternalError("Received nil request")
	}
	if stringutils.Blank(req.Tuid) {
		return nil, twirp.InvalidArgumentError("TUID", "SKU should not be blank")
	}
	if stringutils.Blank(req.Transactionid) {
		return nil, twirp.InvalidArgumentError("TransactionID", "TransactionID should not be blank")
	}

	auditLoggerTimer := time.Now()
	apiName := "UseBitsOnExtension"
	msg, err := audit.CreateAccessLogMsg(ctx, apiName, *req)
	if err != nil {
		log.WithError(err).Error(fmt.Sprintf("failed to generate access log msg in %s", apiName))
	} else {
		err = s.AuditLogger.Send(ctx, msg)
		if err != nil {
			log.WithError(err).Warn(fmt.Sprintf("error while sending audit log in %s", apiName))
		}
	}
	go s.Statter.TimingDuration("UseBitsOnExtension_AuditLogger", time.Since(auditLoggerTimer))

	logFields := log.Fields{
		"TransactionID": req.Transactionid,
		"TUID":          req.Tuid,
	}

	obtainLockTimeStart := time.Now()
	transactionLock, err := s.LockingClient.ObtainLock(fmt.Sprintf("%s-%s", RedisLockKeyPrefix, req.Transactionid))
	go s.Statter.TimingDuration("UseBitsOnExtension_ObtainLock", time.Since(obtainLockTimeStart))
	if err != nil {
		// We want to separate these cases. The first is if the transaction is already being processed, while the
		// second is if there is a problem talking to redis itself
		if err == lock.FailedToObtainLockError {
			log.WithError(err).WithFields(logFields).Warn("failed to obtain lock for extension transaction")
			return nil, twirp.NewError(twirp.AlreadyExists, "This transaction is already being processed")
		}

		log.WithError(err).WithFields(logFields).Error("failed to call redis to get lock for extension transaction")
		return nil, twirp.InternalError("Downstream dependency error using bits on extension")
	}
	defer func() {
		transactionUnlockStart := time.Now()
		err = transactionLock.Unlock()
		go s.Statter.TimingDuration("UseBitsOnExtension_ReleaseLock", time.Since(transactionUnlockStart))
		if err != nil {
			log.WithError(err).WithFields(logFields).Error("failed to unlock redis lock for extension transaction")
		}
	}()

	getTransactionTimeStart := time.Now()
	tx, err := s.ExtensionTransactionManager.GetTransaction(req.Tuid, req.Transactionid)
	go s.Statter.TimingDuration("UseBitsOnExtension_GetTransaction", time.Since(getTransactionTimeStart))
	if err != nil {
		log.WithFields(logFields).WithError(err).Error("Error getting transaction from extension manager")
		return nil, twirp.InternalError("Downstream dependency error using bits on extension")
	}

	if tx == nil {
		return nil, twirp.NewError(twirp.FailedPrecondition, "Transaction not found")
	}

	if tx.TUID != req.Tuid {
		return nil, twirp.NewError(twirp.FailedPrecondition, "TUID in request does not match transaction tuid")
	}

	if tx.Status == transaction.StatusProcessed {
		// If the transaction has already been processed, return early with the user's current balance

		bitsBalance, err := s.getBitsBalance(ctx, tx.TUID, tx.ChannelID)
		if err != nil {
			log.WithError(err).WithFields(logFields).Error("Error getting balance")
			return nil, twirp.InternalError("Downstream dependency error using bits")
		}

		return &paydayrpc.UseBitsOnExtensionResp{
			Balance: bitsBalance,
		}, nil
	}

	transactionAge := time.Since(tx.CreateTime)
	if transactionAge.Nanoseconds() > MaximumTransactionAge.Nanoseconds() {
		log.WithFields(logFields).Warn("Received a request to use bits for an extension transaction that is too old")
		return nil, twirp.NewError(twirp.FailedPrecondition, "Transaction is too old")
	}

	getLeaderboardTimeStart := time.Now()
	leaderboardEntry, err := s.Pantheon.GetLeaderboard(ctx, pantheon.PantheonBitsUsageDomain, pantheonrpc.TimeUnit_ALLTIME, tx.ChannelID, req.Tuid, 1, 1, false)
	go s.Statter.TimingDuration("UseBitsOnExtension_GetLeaderboard", time.Since(getLeaderboardTimeStart))
	if err != nil {
		log.WithFields(logFields).WithError(err).Error("failed to fetch leaderboard for transaction creation on using bits on extension")
		return nil, twirp.InternalError(constants.DownstreamDependencyError)
	}
	totalBitsToBroadcasterBefore := int64(0)
	if leaderboardEntry != nil && leaderboardEntry.GetEntryContext() != nil && leaderboardEntry.GetEntryContext().GetEntry() != nil {
		totalBitsToBroadcasterBefore = leaderboardEntry.GetEntryContext().GetEntry().Score
	}

	var resp *paydayrpc.UseBitsOnExtensionResp
	var updatedBalance int
	if tx.Product.InDevelopment || tx.ChannelID == req.Tuid {
		resp, err = s.performTransactionForTestSKU(ctx, tx)
		if err != nil {
			return nil, err
		}
	} else {
		removeBitsTimeStart := time.Now()
		updatedBalance, err = s.Pachinko.RemoveBits(ctx, tx.TransactionID, tx.TUID, int(tx.Product.Cost.Amount))
		go s.Statter.TimingDuration("UseBitsOnExtension_RemoveBits", time.Since(removeBitsTimeStart))

		if err != nil {
			if err == pachinko.ErrInsufficientBalance {
				return nil, twirp.NewError(twirp.FailedPrecondition, "Insufficient Bits balance")
			}
			log.WithError(err).WithFields(logFields).Error("failed to remove bits for transaction when using bits on extension")
			return nil, err
		}
		resp = &paydayrpc.UseBitsOnExtensionResp{
			Balance: int32(updatedBalance),
		}
	}

	go s.publishToUseBitsOnExtensionEventSNSTopic(tx)

	tx.TotalBitsAfterTransaction = int64(updatedBalance)
	tx.TotalBitsToBroadcaster = int64(totalBitsToBroadcasterBefore) + tx.Product.Cost.Amount
	tx.IPAddress = req.IpAddress
	tx.ZipCode = req.ZipCode
	tx.CountryCode = req.CountryCode
	tx.City = req.City

	finalizeTransactionTimeStart := time.Now()
	err = s.ExtensionTransactionManager.FinalizeTransaction(tx)
	go s.Statter.TimingDuration("UseBitsOnExtension_FinalizeTransaction", time.Since(finalizeTransactionTimeStart))
	if err != nil {
		log.WithFields(logFields).WithError(err).Error("Error marking transaction as processed in extension manager after bits usage")
		return nil, twirp.InternalError("Downstream dependency error using bits")
	}

	return resp, nil
}

func (s *ServerImpl) getBitsBalance(ctx context.Context, userID string, channelID string) (int32, error) {
	getBalanceTimeStart := time.Now()
	bitsBalance, err := s.BalanceGetter.GetBalance(ctx, userID, false)
	go s.Statter.TimingDuration("UseBitsOnExtension_GetBalance", time.Since(getBalanceTimeStart))
	if err != nil {
		return 0, errors.New("Error getting balance from balance manager")
	}

	return bitsBalance, nil
}

func (s *ServerImpl) performTransactionForTestSKU(ctx context.Context, tx *transaction.Transaction) (*paydayrpc.UseBitsOnExtensionResp, error) {
	// In-development test SKUs do not create a transaction in Pachinko
	// any relevant async operations that normally occur in our SQS processor should happen here

	logFields := log.Fields{
		"TransactionID": tx.TransactionID,
		"TUID":          tx.TUID,
		"ChannelID":     tx.ChannelID,
	}

	bitsBalance, err := s.getBitsBalance(ctx, tx.TUID, tx.ChannelID)
	if err != nil {
		log.WithError(err).WithFields(logFields).Error("Error getting balance")
		return nil, twirp.InternalError("Downstream dependency error using bits")
	}

	go func() {
		useBitsOnExtensionMessage := &models.UseBitsOnExtensionMessage{
			TransactionId:             tx.TransactionID,
			RequestingTwitchUserId:    tx.TUID,
			TargetTwitchUserId:        tx.ChannelID,
			TimeOfEvent:               time.Now(),
			ExtensionID:               tx.ExtensionClientID,
			SKU:                       tx.Product.Sku,
			BitsProccessed:            int(tx.Product.Cost.Amount),
			TotalBitsAfterTransaction: int(bitsBalance),
		}
		err := s.ExtensionsPubsubManager.ProcessUseBitsOnExtensionMsg(context.Background(), useBitsOnExtensionMessage)
		if err != nil {
			log.WithError(err).Error("Failed to process Bits on extension")
		}

		err = s.EventBusClient.Publish(context.Background(), &bits_use.BitsUseCreate{
			BitsTransactionId: useBitsOnExtensionMessage.TransactionId,
			FromUserId:        useBitsOnExtensionMessage.RequestingTwitchUserId,
			From: &bits_use.BitsUseCreate_Extension{
				Extension: &bits_use.Extension{
					Id:                 useBitsOnExtensionMessage.ExtensionID,
					ProductId:          useBitsOnExtensionMessage.SKU,
					InDevelopment:      true,
					ProductDisplayName: tx.Product.DisplayName,
					// Broadcast: TODO, is this applicable?
				},
			},
			ToUserId:   tx.ChannelID,
			BitsAmount: 0, //Keep this 0 since no actual Bits are used
		})
		if err != nil {
			log.WithError(err).Error("Failed to publish EventBus event for test SKU extension transaction")
		}
	}()

	return &paydayrpc.UseBitsOnExtensionResp{
		Balance: bitsBalance,
	}, nil
}

func (s *ServerImpl) publishToUseBitsOnExtensionEventSNSTopic(tx *transaction.Transaction) {
	//Publish to audit SNS topic
	snsMessage := models.TransactionRecord{
		TransactionID:          tx.TransactionID,
		TransactionType:        models.UseBitsOnExtension.Name(),
		TimeOfEvent:            time.Now().Unix(),
		RequestingTwitchUserID: tx.TUID,
		TargetTwitchUserID:     tx.ChannelID,
		TotalBitAmount:         int(tx.Product.Cost.Amount),
		ProcessedBitAmount:     int(tx.Product.Cost.Amount),
		ExtensionClientID:      tx.ExtensionClientID,
		SKU:                    tx.Product.Sku,
		ExtensionProduct:       tx.Product,
		SponsoredAmount:        0,
	}
	snsJSON, err := json.Marshal(snsMessage)
	if err != nil {
		msg := "Error marshalling UseBitsOnExtension transaction for publishing to event SNS topic"
		log.WithError(err).Error(msg)
	}

	postToTopicTimeStart := time.Now()
	err = s.SNSClient.PostToTopic(s.Config.UseBitsOnExtensionEventSNSTopic, string(snsJSON))
	go s.Statter.TimingDuration("UseBitsOnExtension_PostToTopic", time.Since(postToTopicTimeStart))

	if err != nil {
		msg := "Failed to post UseBitsOnExtension message to event topic"
		log.WithError(err).WithFields(log.Fields{
			"snsTopic":      s.Config.UseBitsOnExtensionEventSNSTopic,
			"transactionID": tx.TransactionID,
		}).Error(msg)
	}
}
