package use_bits_on_extension_test

import (
	"context"
	"fmt"
	"testing"
	"time"

	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	"code.justin.tv/commerce/payday/clients/cloudwatchlogger"
	"code.justin.tv/commerce/payday/clients/pachinko"
	"code.justin.tv/commerce/payday/clients/pantheon"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/extension/transaction"
	"code.justin.tv/commerce/payday/lock"
	balance_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/balance"
	pachinko_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/pachinko"
	pantheon_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/pantheon"
	pubsub_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/extension/pubsub"
	transaction_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/extension/transaction"
	lock_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/lock"
	metrics_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/metrics"
	sns_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sns"
	mock_eventbus "code.justin.tv/commerce/payday/mocks/code.justin.tv/eventbus/client"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"code.justin.tv/commerce/payday/rpc/paydayserver/use_bits_on_extension"
	"code.justin.tv/commerce/payday/rpc/utils/test"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
	"github.com/twitchtv/twirp"
)

type fakeLock struct {
}

func (f fakeLock) Unlock() error {
	return nil
}

func TestServerImpl_UseBitsOnExtensionImpl(t *testing.T) {
	Convey("Given a use_bits_on_extension server", t, func() {
		extensionTransactionManager := new(transaction_mock.Manager)
		getter := new(balance_mock.Getter)
		extensionPubsubManager := new(pubsub_mock.Manager)
		snsCliet := new(sns_mock.ISNSClient)
		pantheonClient := new(pantheon_mock.IPantheonClientWrapper)
		pachinkoClient := new(pachinko_mock.PachinkoClient)
		configuration := &config.Configuration{
			UseBitsOnExtensionEventSNSTopic: "test_topic",
		}
		redisLockingCLient := new(lock_mock.LockingClient)
		cwlogger := cloudwatchlogger.NewCloudWatchLogNoopClient()
		statter := new(metrics_mock.Statter)
		eventBusClient := new(mock_eventbus.Publisher)

		server := &use_bits_on_extension.ServerImpl{
			ExtensionTransactionManager: extensionTransactionManager,
			BalanceGetter:               getter,
			ExtensionsPubsubManager:     extensionPubsubManager,
			SNSClient:                   snsCliet,
			EventBusClient:              eventBusClient,
			Config:                      configuration,
			Pantheon:                    pantheonClient,
			Pachinko:                    pachinkoClient,
			AuditLogger:                 cwlogger,
			LockingClient:               redisLockingCLient,
			Statter:                     statter,
		}

		Convey("Errors when called with a nil request", func() {
			_, err := server.UseBitsOnExtensionImpl(context.Background(), nil)
			test.AssertTwirpErrorCode(err, twirp.Internal)
		})

		Convey("Given a valid request", func() {
			testTUID := "testTUID"
			testTransactionID := "testTransactionID"
			testChannelID := "testChannelID"
			testExtensionID := "testExtensionID"
			testSKU := "testSKU"
			testProduct := transaction.FaladorProduct{
				Domain: testExtensionID,
				Sku:    testSKU,
				Cost: &transaction.FaladorCost{
					Amount: 100,
					Type:   "bits",
				},
				DisplayName:   "Test Product 1",
				InDevelopment: true,
			}
			req := &paydayrpc.UseBitsOnExtensionReq{
				Tuid:          testTUID,
				Transactionid: testTransactionID,
			}
			statter.On("TimingDuration", mock.Anything, mock.Anything).Return()

			Convey("Errors when tuid is blank", func() {
				req.Tuid = ""
				_, err := server.UseBitsOnExtensionImpl(context.Background(), req)
				test.AssertTwirpErrorCode(err, twirp.InvalidArgument)
			})

			Convey("Errors when transacionID is blank", func() {
				req.Transactionid = ""
				_, err := server.UseBitsOnExtensionImpl(context.Background(), req)
				test.AssertTwirpErrorCode(err, twirp.InvalidArgument)
			})

			Convey("Errors when failure to obtain redis lock", func() {
				redisLockingCLient.On("ObtainLock", fmt.Sprintf("%s-%s", use_bits_on_extension.RedisLockKeyPrefix, testTransactionID)).Return(nil, lock.FailedToObtainLockError).Once()
				_, err := server.UseBitsOnExtensionImpl(context.Background(), req)
				test.AssertTwirpErrorCode(err, twirp.AlreadyExists)
			})

			Convey("Errors when failure to contact redis for lock", func() {
				redisLockingCLient.On("ObtainLock", fmt.Sprintf("%s-%s", use_bits_on_extension.RedisLockKeyPrefix, testTransactionID)).Return(nil, errors.New("redis down")).Once()
				_, err := server.UseBitsOnExtensionImpl(context.Background(), req)
				test.AssertTwirpErrorCode(err, twirp.Internal)
			})

			Convey("Given a valid lock", func() {
				redisLockingCLient.On("ObtainLock", mock.Anything).Return(fakeLock{}, nil)
				Convey("Errors when extension manager errors", func() {
					extensionTransactionManager.On("GetTransaction", testTUID, testTransactionID).Return(nil, errors.New("test error"))
					_, err := server.UseBitsOnExtensionImpl(context.Background(), req)
					test.AssertTwirpErrorCode(err, twirp.Internal)
				})

				Convey("Errors when extension manager doesn't find a transaction", func() {
					extensionTransactionManager.On("GetTransaction", testTUID, testTransactionID).Return(nil, nil)
					_, err := server.UseBitsOnExtensionImpl(context.Background(), req)
					test.AssertTwirpErrorCode(err, twirp.FailedPrecondition)
				})

				Convey("Errors when extension manager returns a transaction whose user does not match", func() {
					extensionTransactionManager.On("GetTransaction", testTUID, testTransactionID).Return(&transaction.Transaction{
						TransactionID: testTransactionID,
						TUID:          "testTUID2",
					}, nil)
					_, err := server.UseBitsOnExtensionImpl(context.Background(), req)
					test.AssertTwirpErrorCode(err, twirp.FailedPrecondition)
				})

				Convey("When extension manager returns an already processed transaction", func() {
					extensionTransactionManager.On("GetTransaction", testTUID, testTransactionID).Return(&transaction.Transaction{
						TransactionID: testTransactionID,
						TUID:          testTUID,
						ChannelID:     testChannelID,
						Product:       testProduct,
						Status:        transaction.StatusProcessed,
					}, nil)

					Convey("Errors when GetBalance call fails", func() {
						getter.On("GetBalance", mock.Anything, testTUID, false).Return(int32(0), errors.New("test error"))
						_, err := server.UseBitsOnExtensionImpl(context.Background(), req)
						test.AssertTwirpErrorCode(err, twirp.Internal)
					})

					Convey("Succeeds when GetBalance call returns a valid response", func() {
						testBalanceResponse := int32(123)
						getter.On("GetBalance", mock.Anything, testTUID, false).Return(testBalanceResponse, nil)

						resp, err := server.UseBitsOnExtensionImpl(context.Background(), req)
						So(resp, ShouldNotBeNil)
						So(err, ShouldBeNil)
					})
				})

				Convey("Errors when extension manager returns an old transaction", func() {
					extensionTransactionManager.On("GetTransaction", testTUID, testTransactionID).Return(&transaction.Transaction{
						TransactionID:     testTransactionID,
						TUID:              testTUID,
						ChannelID:         testChannelID,
						Product:           testProduct,
						ExtensionClientID: testExtensionID,
						Status:            transaction.StatusPending,
						CreateTime:        time.Now().Add(-1 * use_bits_on_extension.MaximumTransactionAge),
					}, nil)

					_, err := server.UseBitsOnExtensionImpl(context.Background(), req)
					test.AssertTwirpErrorCode(err, twirp.FailedPrecondition)
				})

				Convey("When extension manager returns a valid in-development bits transaction", func() {
					extensionTransactionManager.On("GetTransaction", testTUID, testTransactionID).Return(&transaction.Transaction{
						TransactionID:     testTransactionID,
						TUID:              testTUID,
						ChannelID:         testChannelID,
						ExtensionClientID: testExtensionID,
						Product:           testProduct,
						Status:            transaction.StatusPending,
						CreateTime:        time.Now(),
					}, nil)

					extensionPubsubManager.On("ProcessUseBitsOnExtensionMsg", mock.Anything, mock.Anything).Return(nil)
					eventBusClient.On("Publish", mock.Anything, mock.Anything).Return(nil)

					Convey("when the leaderboard fetch fails", func() {
						pantheonClient.On("GetLeaderboard", mock.Anything, pantheon.PantheonBitsUsageDomain, pantheonrpc.TimeUnit_ALLTIME, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("WALRUS STRIKE"))

						_, err := server.UseBitsOnExtensionImpl(context.Background(), req)
						test.AssertTwirpErrorCode(err, twirp.Internal)
					})

					Convey("when the leaderboard fetch succeeds", func() {
						pantheonClient.On("GetLeaderboard", mock.Anything, pantheon.PantheonBitsUsageDomain, pantheonrpc.TimeUnit_ALLTIME, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(&pantheonrpc.GetLeaderboardResp{
							EntryContext: &pantheonrpc.EntryContext{
								Entry: &pantheonrpc.LeaderboardEntry{
									EntryKey: req.Tuid,
									Score:    int64(322),
								},
							},
						}, nil)

						Convey("When SNS publish succeeds", func() {
							snsCliet.On("PostToTopic", mock.Anything, mock.Anything).Return(nil)
							testBalanceResponse := int32(123)
							getter.On("GetBalance", mock.Anything, testTUID, false).Return(testBalanceResponse, nil)

							Convey("Errors when extension manager update fails", func() {
								extensionTransactionManager.On("FinalizeTransaction", mock.Anything).Return(errors.New("test error"))
								_, err := server.UseBitsOnExtensionImpl(context.Background(), req)
								So(err, ShouldNotBeNil)
							})

							Convey("Succeeds when extension manage update succeeds", func() {
								extensionTransactionManager.On("FinalizeTransaction", mock.Anything).Return(nil)
								resp, err := server.UseBitsOnExtensionImpl(context.Background(), req)
								So(err, ShouldBeNil)
								So(resp.Balance, ShouldEqual, 123)
							})
						})
					})
				})

				Convey("When extension manager returns a valid non-in-development bits transaction", func() {
					testProduct.InDevelopment = false

					tx := &transaction.Transaction{
						TransactionID:     testTransactionID,
						ChannelID:         testChannelID,
						TUID:              testTUID,
						ExtensionClientID: testExtensionID,
						Product:           testProduct,
						Status:            transaction.StatusPending,
						CreateTime:        time.Now(),
					}

					extensionTransactionManager.On("GetTransaction", testTUID, testTransactionID).Return(tx, nil)

					Convey("when the leaderboard fetch fails", func() {
						pantheonClient.On("GetLeaderboard", mock.Anything, pantheon.PantheonBitsUsageDomain, pantheonrpc.TimeUnit_ALLTIME, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("WALRUS STRIKE"))

						_, err := server.UseBitsOnExtensionImpl(context.Background(), req)
						test.AssertTwirpErrorCode(err, twirp.Internal)
					})

					Convey("when the leaderboard fetch succeeds", func() {
						pantheonClient.On("GetLeaderboard", mock.Anything, pantheon.PantheonBitsUsageDomain, pantheonrpc.TimeUnit_ALLTIME, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(&pantheonrpc.GetLeaderboardResp{
							EntryContext: &pantheonrpc.EntryContext{
								Entry: &pantheonrpc.LeaderboardEntry{
									EntryKey: req.Tuid,
									Score:    int64(322),
								},
							},
						}, nil)

						Convey("Errors when pachinko errors", func() {
							pachinkoClient.On("RemoveBits", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(0, errors.New("test error"))
							_, err := server.UseBitsOnExtensionImpl(context.Background(), req)
							So(err, ShouldNotBeNil)
						})

						Convey("Returns Failed Precondition error if pachinko error is insufficient balance", func() {
							pachinkoClient.On("RemoveBits", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(0, pachinko.ErrInsufficientBalance)
							_, err := server.UseBitsOnExtensionImpl(context.Background(), req)
							test.AssertTwirpErrorCode(err, twirp.FailedPrecondition)
						})

						Convey("When pachinko succeeds", func() {
							pachinkoClient.On("RemoveBits", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(5, nil)

							Convey("Succeeds even when publish to SNS topic fails", func() {
								snsCliet.On("PostToTopic", mock.Anything, mock.Anything).Return(errors.New("test error"))
								extensionTransactionManager.On("FinalizeTransaction", mock.Anything).Return(nil)
								_, err := server.UseBitsOnExtensionImpl(context.Background(), req)
								So(err, ShouldBeNil)
							})

							Convey("When SNS publish succeeds", func() {
								snsCliet.On("PostToTopic", mock.Anything, mock.Anything).Return(nil)

								Convey("Errors when extension manager update fails", func() {
									extensionTransactionManager.On("FinalizeTransaction", mock.Anything).Return(errors.New("test error"))
									_, err := server.UseBitsOnExtensionImpl(context.Background(), req)
									So(err, ShouldNotBeNil)
								})

								Convey("Succeeds when extension manage update succeeds", func() {
									extensionTransactionManager.On("FinalizeTransaction", mock.Anything).Return(nil)
									resp, err := server.UseBitsOnExtensionImpl(context.Background(), req)
									So(err, ShouldBeNil)
									So(resp.Balance, ShouldEqual, 5)
								})
							})

						})
					})

				})

				Convey("When extension manager returns a valid bits transaction with dev testing on own channel", func() {
					extensionTransactionManager.On("GetTransaction", testTUID, testTransactionID).Return(&transaction.Transaction{
						TransactionID:     testTransactionID,
						TUID:              testTUID,
						ChannelID:         testTUID,
						ExtensionClientID: testExtensionID,
						Product:           testProduct,
						Status:            transaction.StatusPending,
						CreateTime:        time.Now(),
					}, nil)

					extensionPubsubManager.On("ProcessUseBitsOnExtensionMsg", mock.Anything, mock.Anything).Return(nil)
					eventBusClient.On("Publish", mock.Anything, mock.Anything).Return(nil)

					Convey("when the leaderboard fetch fails", func() {
						pantheonClient.On("GetLeaderboard", mock.Anything, pantheon.PantheonBitsUsageDomain, pantheonrpc.TimeUnit_ALLTIME, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("WALRUS STRIKE"))

						_, err := server.UseBitsOnExtensionImpl(context.Background(), req)
						test.AssertTwirpErrorCode(err, twirp.Internal)
					})

					Convey("when the leaderboard fetch succeeds", func() {
						pantheonClient.On("GetLeaderboard", mock.Anything, pantheon.PantheonBitsUsageDomain, pantheonrpc.TimeUnit_ALLTIME, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(&pantheonrpc.GetLeaderboardResp{
							EntryContext: &pantheonrpc.EntryContext{
								Entry: &pantheonrpc.LeaderboardEntry{
									EntryKey: req.Tuid,
									Score:    int64(322),
								},
							},
						}, nil)

						Convey("When SNS publish succeeds", func() {
							snsCliet.On("PostToTopic", mock.Anything, mock.Anything).Return(nil)
							testBalanceResponse := int32(123)
							getter.On("GetBalance", mock.Anything, testTUID, false).Return(testBalanceResponse, nil)
							Convey("Errors when extension manager update fails", func() {
								extensionTransactionManager.On("FinalizeTransaction", mock.Anything).Return(errors.New("test error"))
								_, err := server.UseBitsOnExtensionImpl(context.Background(), req)
								So(err, ShouldNotBeNil)
							})

							Convey("Succeeds when extension manage update succeeds", func() {
								extensionTransactionManager.On("FinalizeTransaction", mock.Anything).Return(nil)
								resp, err := server.UseBitsOnExtensionImpl(context.Background(), req)
								So(err, ShouldBeNil)
								So(resp.Balance, ShouldEqual, 123)
							})
						})
					})
				})
			})
		})
	})
}
