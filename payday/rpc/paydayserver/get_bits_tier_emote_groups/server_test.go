package get_bits_tier_emote_groups

import (
	"context"
	"testing"

	"code.justin.tv/commerce/payday/dynamo/badge_tier_emote_groupids"
	"code.justin.tv/commerce/payday/errors"
	badge_tier_emote_groupids_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo/badge_tier_emote_groupids"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestServer_Get(t *testing.T) {
	Convey("given a Get API", t, func() {
		badgeTierEmoteGroupIDDAOMock := new(badge_tier_emote_groupids_mock.BadgeTierEmoteGroupIDDAO)
		api := &ServerImpl{
			BadgeTierGroupIDDAO: badgeTierEmoteGroupIDDAOMock,
		}

		channelID := "1234"
		bitsTierThreshold1 := int64(1000)
		bitsTierThreshold2 := int64(5000)
		emoteGroupID1 := "abc"
		emoteGroupID2 := "def"

		Convey("when the request is nil", func() {
			var req *paydayrpc.GetBitsTierEmoteGroupsReq
			resp, err := api.Get(context.Background(), req)

			Convey("we should return an error", func() {
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		Convey("when channelID is empty", func() {
			req := &paydayrpc.GetBitsTierEmoteGroupsReq{}
			resp, err := api.Get(context.Background(), req)

			Convey("we should return an error", func() {
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		Convey("when the DAO errors", func() {
			badgeTierEmoteGroupIDDAOMock.On("Get", mock.Anything, channelID).Return(nil, errors.New("dummy error"))
			req := &paydayrpc.GetBitsTierEmoteGroupsReq{
				ChannelId: channelID,
			}
			resp, err := api.Get(context.Background(), req)

			Convey("we should return an error", func() {
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		Convey("when the DAO succeeds", func() {
			badgeTierEmoteGroupIDDAOMock.On("Get", mock.Anything, channelID).Return([]*badge_tier_emote_groupids.BadgeTierEmoteGroupID{
				{
					Threshold: bitsTierThreshold1,
					GroupID:   emoteGroupID1,
				},
				{
					Threshold: bitsTierThreshold2,
					GroupID:   emoteGroupID2,
				},
			}, nil)
			req := &paydayrpc.GetBitsTierEmoteGroupsReq{
				ChannelId: channelID,
			}
			resp, err := api.Get(context.Background(), req)

			Convey("we should return a successful response", func() {
				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
				So(resp.EmoteGroups, ShouldHaveLength, 2)
				So(resp.EmoteGroups[0].BitsTierThreshold, ShouldEqual, bitsTierThreshold1)
				So(resp.EmoteGroups[0].EmoteGroupId, ShouldEqual, emoteGroupID1)
				So(resp.EmoteGroups[1].BitsTierThreshold, ShouldEqual, bitsTierThreshold2)
				So(resp.EmoteGroups[1].EmoteGroupId, ShouldEqual, emoteGroupID2)
			})
		})
	})
}
