package get_bits_tier_emote_groups

import (
	"context"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/dynamo/badge_tier_emote_groupids"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"github.com/twitchtv/twirp"
)

type Server interface {
	Get(ctx context.Context, req *paydayrpc.GetBitsTierEmoteGroupsReq) (*paydayrpc.GetBitsTierEmoteGroupsResp, error)
}

type ServerImpl struct {
	BadgeTierGroupIDDAO badge_tier_emote_groupids.BadgeTierEmoteGroupIDDAO `inject:""`
}

func (s *ServerImpl) Get(ctx context.Context, req *paydayrpc.GetBitsTierEmoteGroupsReq) (*paydayrpc.GetBitsTierEmoteGroupsResp, error) {
	ctx, cancel := context.WithTimeout(ctx, 1*time.Second)
	defer cancel()

	if req == nil {
		return nil, twirp.InvalidArgumentError("request", "cannot be empty")
	} else if req.GetChannelId() == "" {
		return nil, twirp.InvalidArgumentError("channel_id", "cannot be empty")
	}

	bitsTierEmoteGroups, err := s.BadgeTierGroupIDDAO.Get(ctx, req.GetChannelId())
	if err != nil {
		msg := "[GetBitsTierEmoteGroups] Error getting bits tier emote groups"
		log.WithError(err).WithField("channelID", req.GetChannelId()).Error(msg)
		return nil, twirp.InternalError(msg)
	}

	emoteGroups := make([]*paydayrpc.BitsTierEmoteGroup, 0, len(bitsTierEmoteGroups))
	for _, group := range bitsTierEmoteGroups {
		emoteGroups = append(emoteGroups, &paydayrpc.BitsTierEmoteGroup{
			BitsTierThreshold: group.Threshold,
			EmoteGroupId:      group.GroupID,
		})
	}

	return &paydayrpc.GetBitsTierEmoteGroupsResp{
		EmoteGroups: emoteGroups,
	}, nil
}
