package get_extension_transactions_test

import (
	"context"
	"testing"

	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/extension/transaction"
	transaction_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/extension/transaction"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"code.justin.tv/commerce/payday/rpc/paydayserver/get_extension_transactions"
	"code.justin.tv/commerce/payday/utils/pointers"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestServerImpl_GetExtensionTransactions(t *testing.T) {
	Convey("Given an get_extension_transactions server", t, func() {
		extensionTransactionManager := new(transaction_mock.Manager)
		server := &get_extension_transactions.ServerImpl{
			ExtensionTransactionManager: extensionTransactionManager,
		}

		Convey("Errors on a nil request", func() {
			_, err := server.GetExtensionTransactionsImpl(context.Background(), nil)
			So(err, ShouldNotBeNil)
		})

		Convey("Errors when TUID is blank", func() {
			_, err := server.GetExtensionTransactionsImpl(context.Background(), &paydayrpc.GetExtensionTransactionsReq{
				Tuid: "",
			})
			So(err, ShouldNotBeNil)
		})

		Convey("Errors when channelID is present without extensionClientID", func() {
			_, err := server.GetExtensionTransactionsImpl(context.Background(), &paydayrpc.GetExtensionTransactionsReq{
				Tuid:              "testTuid",
				ChannelId:         "testChannelId",
				ExtensionClientId: "",
			})
			So(err, ShouldNotBeNil)
		})

		Convey("Errors when extension transaction manager errors", func() {
			extensionTransactionManager.On("GetTransactions", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil, nil, errors.New("test error"))
			_, err := server.GetExtensionTransactionsImpl(context.Background(), &paydayrpc.GetExtensionTransactionsReq{
				Tuid: "testTuid",
			})
			So(err, ShouldNotBeNil)
		})

		Convey("Succeeds when extension transaction manager returns transactions", func() {
			extensionTransactionManager.On("GetTransactions", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return([]*transaction.Transaction{
				{
					TransactionID: "tx-1",
					Product: transaction.FaladorProduct{
						Cost: &transaction.FaladorCost{},
					},
				},
				{
					TransactionID: "tx-2",
					Product: transaction.FaladorProduct{
						Cost: &transaction.FaladorCost{},
					},
				},
			}, pointers.StringP("new-cursor"), nil)
			resp, err := server.GetExtensionTransactionsImpl(context.Background(), &paydayrpc.GetExtensionTransactionsReq{
				Tuid: "testTuid",
			})
			So(err, ShouldBeNil)
			So(resp.Cursor, ShouldEqual, "new-cursor")
			So(len(resp.Transactions), ShouldEqual, 2)
		})
	})
}
