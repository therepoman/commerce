package get_extension_transactions

import (
	"context"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/extension/transaction"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"code.justin.tv/commerce/payday/utils/pointers"
	"code.justin.tv/commerce/payday/utils/strings"
	"github.com/golang/protobuf/ptypes"
	"github.com/twitchtv/twirp"
)

const (
	timeoutDuration = 5 * time.Second
)

type Server interface {
	GetExtensionTransactionsImpl(ctx context.Context, req *paydayrpc.GetExtensionTransactionsReq) (*paydayrpc.GetExtensionTransactionsResp, error)
}

type ServerImpl struct {
	ExtensionTransactionManager transaction.Manager `inject:""`
}

func (s *ServerImpl) GetExtensionTransactionsImpl(ctx context.Context, req *paydayrpc.GetExtensionTransactionsReq) (*paydayrpc.GetExtensionTransactionsResp, error) {
	_, cancel := context.WithTimeout(ctx, timeoutDuration)
	defer cancel()

	if req == nil {
		return nil, twirp.InternalError("Received nil request")
	}

	if strings.Blank(req.Tuid) {
		return nil, twirp.InvalidArgumentError("tuid", "tuid is required")
	}

	if strings.NotBlank(req.ChannelId) && strings.Blank(req.ExtensionClientId) {
		return nil, twirp.InvalidArgumentError("extensionClientId", "extensionClientId is required if channel id is present")
	}

	var tuid *string
	if strings.NotBlank(req.Tuid) {
		tuid = pointers.StringP(req.Tuid)
	}

	var extensionClientID *string
	if strings.NotBlank(req.ExtensionClientId) {
		extensionClientID = pointers.StringP(req.ExtensionClientId)
	}

	var channelID *string
	if strings.NotBlank(req.ChannelId) {
		channelID = pointers.StringP(req.ChannelId)
	}

	var cursor *string
	if strings.NotBlank(req.Cursor) {
		cursor = pointers.StringP(req.Cursor)
	}

	transactions, newCursor, err := s.ExtensionTransactionManager.GetTransactions(tuid, extensionClientID, channelID, cursor)
	if err != nil {
		log.WithError(err).Error("Error getting extension transactions from extension transaction manager")
		return nil, twirp.InternalError("Downstream dependency error getting extension transactions")
	}

	return toResponse(transactions, newCursor), nil
}

func toResponse(transactions []*transaction.Transaction, cursor *string) *paydayrpc.GetExtensionTransactionsResp {
	responseTransactions := make([]*paydayrpc.ExtensionTransaction, 0, len(transactions))
	for _, tx := range transactions {
		responseTransactions = append(responseTransactions, toResponseTransaction(tx))
	}

	return &paydayrpc.GetExtensionTransactionsResp{
		Transactions: responseTransactions,
		Cursor:       pointers.StringOrDefault(cursor, ""),
	}
}

func toResponseTransaction(transaction *transaction.Transaction) *paydayrpc.ExtensionTransaction {
	if transaction == nil {
		return nil
	}

	protoCreateTime, _ := ptypes.TimestampProto(transaction.CreateTime)
	return &paydayrpc.ExtensionTransaction{
		Tuid:              transaction.TUID,
		TransactionId:     transaction.TransactionID,
		ExtensionClientId: transaction.ExtensionClientID,
		ChannelId:         transaction.ChannelID,
		SKU:               transaction.Product.Sku,
		BitsCost:          int32(transaction.Product.Cost.Amount),
		InDevelopment:     transaction.Product.InDevelopment,
		Broadcast:         transaction.Product.Broadcast,
		CreateTime:        protoCreateTime,
		Status:            string(transaction.Status),
	}
}
