package get_balance

import (
	"context"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/balance"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"github.com/twitchtv/twirp"
)

type Server interface {
	Get(ctx context.Context, req *paydayrpc.GetBalanceReq) (*paydayrpc.GetBalanceResp, error)
}

type server struct {
	BalanceGetter balance.Getter `inject:""`
}

func NewServer() Server {
	return &server{}
}

func (s *server) Get(ctx context.Context, req *paydayrpc.GetBalanceReq) (*paydayrpc.GetBalanceResp, error) {
	ctx, cancel := context.WithTimeout(ctx, 400*time.Millisecond)
	defer cancel()

	if req == nil {
		return nil, twirp.InternalError("cannot have empty request")
	}

	if req.UserId == "" {
		return nil, twirp.InvalidArgumentError("UserId", "cannot have empty User Id")
	}

	userBalance, err := s.BalanceGetter.GetBalance(ctx, req.UserId, false)
	if err != nil {
		log.WithError(err).WithField("userId", req.UserId).Error("failed to get balance.")
		return nil, err
	}
	return &paydayrpc.GetBalanceResp{
		Balance: userBalance,
	}, nil
}
