package get_balance

import (
	"context"
	"testing"

	"github.com/stretchr/testify/mock"

	"code.justin.tv/commerce/payday/errors"
	balance_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/balance"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/twitchtv/twirp"
)

func TestServer_Get(t *testing.T) {
	Convey("given a server", t, func() {

		bg := new(balance_mock.Getter)
		server := &server{
			BalanceGetter: bg,
		}

		userId := "1234"
		ctx := context.Background()

		Convey("when request is nil", func() {
			resp, err := server.Get(ctx, nil)
			So(resp, ShouldBeNil)

			twirpErr, ok := err.(twirp.Error)
			So(ok, ShouldBeTrue)
			So(twirpErr.Code(), ShouldEqual, twirp.Internal)
			So(twirpErr.Msg(), ShouldEqual, "cannot have empty request")
		})

		Convey("when request is empty", func() {
			req := paydayrpc.GetBalanceReq{}

			resp, err := server.Get(ctx, &req)
			So(resp, ShouldBeNil)

			twirpErr, ok := err.(twirp.Error)
			So(ok, ShouldBeTrue)
			So(twirpErr.Code(), ShouldEqual, twirp.InvalidArgument)
			So(twirpErr.Msg(), ShouldEqual, "UserId cannot have empty User Id")
			So(twirpErr.Meta("argument"), ShouldEqual, "UserId")
		})

		Convey("when request is valid", func() {
			req := paydayrpc.GetBalanceReq{
				UserId: userId,
			}

			Convey("when balance getter works", func() {
				bg.On("GetBalance", mock.Anything, userId, false).Return(int32(1138), nil)

				resp, err := server.Get(ctx, &req)
				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
				So(resp.Balance, ShouldEqual, int32(1138))
			})

			Convey("when balance getter errors", func() {
				animeBearError := errors.New("TOO MANY ANIME BEARS")
				bg.On("GetBalance", mock.Anything, userId, false).Return(int32(0), animeBearError)

				resp, err := server.Get(ctx, &req)
				So(resp, ShouldBeNil)
				So(err, ShouldEqual, animeBearError)
			})
		})
	})
}
