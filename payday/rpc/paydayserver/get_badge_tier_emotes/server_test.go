package get_badge_tier_emotes

import (
	"context"
	"errors"
	"testing"

	badgetieremotes_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/badgetiers/products/emotes"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestServer_Get(t *testing.T) {
	ctx := context.Background()

	Convey("given a Get API", t, func() {
		logicianMock := new(badgetieremotes_mock.Logician)

		api := &ServerImpl{
			EmotesLogician: logicianMock,
		}

		channelID := "127380717"
		userID := "1234567890"

		Convey("when the request is nil", func() {
			var req *paydayrpc.GetBitsBadgeTierEmotesReq
			_, err := api.Get(ctx, req)

			Convey("we should return an error", func() {
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when channelID is empty", func() {
			req := &paydayrpc.GetBitsBadgeTierEmotesReq{}
			_, err := api.Get(ctx, req)

			Convey("we should return an error", func() {
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when userID is empty", func() {
			req := &paydayrpc.GetBitsBadgeTierEmotesReq{
				ChannelId: channelID,
			}
			_, err := api.Get(ctx, req)

			Convey("we should return an error", func() {
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when the logician errors", func() {
			req := &paydayrpc.GetBitsBadgeTierEmotesReq{
				ChannelId: channelID,
				UserId:    userID,
			}

			logicianMock.On("GetBadgeTierEmotes", mock.Anything, channelID, userID, mock.Anything).Return(nil, errors.New("test error"))

			resp, err := api.Get(ctx, req)

			Convey("we should return an error", func() {
				So(resp, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when the logician returns empty", func() {
			req := &paydayrpc.GetBitsBadgeTierEmotesReq{
				ChannelId: channelID,
				UserId:    userID,
			}

			logicianMock.On("GetBadgeTierEmotes", mock.Anything, channelID, userID, mock.Anything).Return(nil, nil)
			resp, err := api.Get(ctx, req)
			So(resp, ShouldNotBeNil)
			So(err, ShouldBeNil)
		})

		Convey("when the loigician succeeds", func() {
			req := &paydayrpc.GetBitsBadgeTierEmotesReq{
				ChannelId: channelID,
				UserId:    userID,
			}

			emotes := []*paydayrpc.BitsBadgeTierEmote{
				{
					EmoteId:        "emotesv2_0022a3e2ef07494081eadb813c743441",
					EmoteCode:      "tooSmart",
					EmoteChannelId: channelID,
					EmoteGroupId:   "1006737386",
					BadgeTierSummary: &paydayrpc.EmoteBitsBadgeTierSummary{
						BadgeTierThreshold:      1000,
						IsEmoteUnlockedForUser:  false,
						NumberOfBitsUntilUnlock: 999,
					},
				},
			}

			logicianMock.On("GetBadgeTierEmotes", mock.Anything, channelID, userID, mock.Anything).Return(emotes, nil)

			resp, err := api.Get(ctx, req)

			Convey("we should return the emote info", func() {
				So(resp, ShouldNotBeNil)
				So(err, ShouldBeNil)
			})
		})
	})
}
