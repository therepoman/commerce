package get_badge_tier_emotes

import (
	"context"
	"time"

	badgetieremotes "code.justin.tv/commerce/payday/badgetiers/products/emotes"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"github.com/twitchtv/twirp"
)

type Server interface {
	Get(ctx context.Context, req *paydayrpc.GetBitsBadgeTierEmotesReq) (*paydayrpc.GetBitsBadgeTierEmotesResp, error)
}

type ServerImpl struct {
	EmotesLogician badgetieremotes.Logician `inject:""`
}

func (s *ServerImpl) Get(ctx context.Context, req *paydayrpc.GetBitsBadgeTierEmotesReq) (*paydayrpc.GetBitsBadgeTierEmotesResp, error) {
	ctx, cancel := context.WithTimeout(ctx, 600*time.Millisecond)
	defer cancel()

	if req == nil {
		return nil, twirp.InternalError("cannot have empty request")
	}

	if req.ChannelId == "" {
		return nil, twirp.InvalidArgumentError("ChannelID", "cannot have empty channel id")
	}

	if req.UserId == "" {
		return nil, twirp.InvalidArgumentError("UserID", "cannot have empty user id")
	}

	emotes, err := s.EmotesLogician.GetBadgeTierEmotes(ctx, req.ChannelId, req.UserId, req.Filter)
	if err != nil {
		return nil, twirp.InternalErrorWith(err)
	}

	return &paydayrpc.GetBitsBadgeTierEmotesResp{Emotes: emotes}, nil
}
