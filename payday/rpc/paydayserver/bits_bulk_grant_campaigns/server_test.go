package get_bits_bulk_grant_campaigns

import (
	"context"
	"testing"

	"code.justin.tv/commerce/payday/bulk_grant"
	bulk_grant_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/bulk_grant"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	. "github.com/smartystreets/goconvey/convey"
)

func TestServer_GetBitsBulkGrantCampaigns(t *testing.T) {
	Convey("given a Get API", t, func() {
		bulkGrantCampaignGetter := new(bulk_grant_mock.CampaignsGetter)

		api := &ServerImpl{
			BulkGrantCampaignsGetter: bulkGrantCampaignGetter,
		}

		campaignList := []bulk_grant.Campaign{{ID: "camp1"}, {ID: "camp2"}}
		campaignMap := map[string]bulk_grant.Campaign{
			"camp1": {ID: "camp1"},
			"camp2": {ID: "camp2"},
		}

		bulkGrantCampaignGetter.On("GetList").Return(campaignList)

		Convey("when we have a valid request", func() {
			req := &paydayrpc.GetBitsBulkGrantCampaignsReq{}

			resp, err := api.Get(context.Background(), req)

			Convey("should return without error", func() {

				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)

				for _, respCampaign := range resp.BulkGrantCampaigns {
					c, ok := campaignMap[respCampaign.Id]
					So(ok, ShouldBeTrue)
					So(c.Name, ShouldEqual, respCampaign.Name)
				}
			})
		})
	})
}
