package get_bits_bulk_grant_campaigns

import (
	"context"

	"code.justin.tv/commerce/payday/bulk_grant"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
)

type Server interface {
	Get(ctx context.Context, req *paydayrpc.GetBitsBulkGrantCampaignsReq) (*paydayrpc.GetBitsBulkGrantCampaignsResp, error)
}

type ServerImpl struct {
	BulkGrantCampaignsGetter bulk_grant.CampaignsGetter `inject:""`
}

func (s *ServerImpl) Get(ctx context.Context, req *paydayrpc.GetBitsBulkGrantCampaignsReq) (*paydayrpc.GetBitsBulkGrantCampaignsResp, error) {
	return &paydayrpc.GetBitsBulkGrantCampaignsResp{
		BulkGrantCampaigns: s.getCampaigns(),
	}, nil
}

func (s *ServerImpl) getCampaigns() []*paydayrpc.BitsBulkGrantCampaign {
	campaigns := make([]*paydayrpc.BitsBulkGrantCampaign, 0)
	for _, c := range s.BulkGrantCampaignsGetter.GetList() {
		campaigns = append(campaigns, &paydayrpc.BitsBulkGrantCampaign{
			Id:                      c.ID,
			Name:                    c.Name,
			BitType:                 c.BitType,
			LimitPerUser:            c.LimitPerUser,
			BitAmount:               c.BitAmount,
			AdminReason:             c.AdminReason,
			PushyNotificationOrigin: c.PushyNotificationOrigin,
		})
	}
	return campaigns
}
