package assign_emote_to_bits_tier

import (
	"context"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/badgetiers"
	badgetieremotes "code.justin.tv/commerce/payday/badgetiers/products/emotes"
	partnerManager "code.justin.tv/commerce/payday/partner"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"github.com/twitchtv/twirp"
)

type Server interface {
	Assign(ctx context.Context, req *paydayrpc.AssignEmoteToBitsTierReq) (*paydayrpc.AssignEmoteToBitsTierResp, error)
}

type ServerImpl struct {
	PartnerManager        partnerManager.IPartnerManager `inject:""`
	BitsTierEmoteAssigner badgetieremotes.Assigner       `inject:""`
}

func (s *ServerImpl) Assign(ctx context.Context, req *paydayrpc.AssignEmoteToBitsTierReq) (*paydayrpc.AssignEmoteToBitsTierResp, error) {
	ctx, cancel := context.WithTimeout(ctx, 2*time.Second)
	defer cancel()

	if req == nil {
		return nil, twirp.InvalidArgumentError("request", "cannot be empty")
	} else if req.GetChannelId() == "" {
		return nil, twirp.InvalidArgumentError("channel_id", "cannot be empty")
	} else if req.GetRequestingUserId() == "" {
		return nil, twirp.InvalidArgumentError("requesting_user_id", "cannot be empty")
	} else if req.GetChannelId() != req.GetRequestingUserId() {
		return nil, paydayrpc.NewClientError(twirp.NewError(twirp.PermissionDenied, "requesting user is not permitted to modify this channel's bits tier"), paydayrpc.ErrCodeUserNotPermitted)
	} else if req.GetEmoteId() == "" {
		return nil, twirp.InvalidArgumentError("emote_id", "cannot be empty")
	}

	// Check that the bits tier threshold is valid
	if bitsTierConfig, bitsTierExists := badgetiers.BitsChatBadgeTiers[req.GetBitsTierThreshold()]; !bitsTierExists || !bitsTierConfig.Enabled {
		return nil, twirp.InvalidArgumentError("bits_tier_threshold", "must be a valid bits tier threshold")
	}

	logFields := log.Fields{
		"channelID":         req.GetChannelId(),
		"requestingUserID":  req.GetRequestingUserId(),
		"emoteID":           req.GetEmoteId(),
		"bitsTierThreshold": req.GetBitsTierThreshold(),
	}

	// Check that the user is a partner or affiliate
	partnerType, err := s.PartnerManager.GetPartnerType(ctx, req.GetChannelId())
	if err != nil {
		msg := "[AssignEmoteToBitsTier] Failed to get channel partner type"
		log.WithFields(logFields).WithError(err).Error(msg)
		return nil, twirp.InternalError(msg)
	} else if partnerType != partnerManager.TraditionalPartnerType && partnerType != partnerManager.AffiliatePartnerType {
		return nil, paydayrpc.NewClientError(twirp.NewError(twirp.PermissionDenied, "channel has invalid partner type"), paydayrpc.ErrCodeInvalidPartnerType)
	}

	// Assign the emote to the bits tier
	err = s.BitsTierEmoteAssigner.AssignEmoteToBitsTier(ctx, req.ChannelId, req.EmoteId, req.BitsTierThreshold)
	if err != nil {
		assignErr := convertAssignError(err)
		if assignErr != nil {
			return nil, assignErr
		}

		msg := "Error assigning emote to bits tier"
		log.WithError(err).WithFields(logFields).Error(msg)
		return nil, twirp.InternalError(msg)
	}

	return &paydayrpc.AssignEmoteToBitsTierResp{
		ChannelId:         req.ChannelId,
		EmoteId:           req.EmoteId,
		BitsTierThreshold: req.BitsTierThreshold,
	}, nil
}

func convertAssignError(err error) error {
	if err == badgetieremotes.EmotesNotAllowedInTierAssignError {
		return paydayrpc.NewClientError(
			twirp.NewError(twirp.FailedPrecondition, "emotes are not allowed in the specified bits tier"),
			paydayrpc.ErrCodeEmotesNotAllowedInBitsTier)
	}

	if err == badgetieremotes.TierAlreadyFullAssignError {
		return paydayrpc.NewClientError(
			twirp.NewError(twirp.FailedPrecondition, "the specified bits tier has reached its limit of emotes"),
			paydayrpc.ErrCodeBitsTierAlreadyFullOfEmotes)
	}

	if err == badgetieremotes.UserNotPermittedAssignError {
		return paydayrpc.NewClientError(
			twirp.NewError(twirp.PermissionDenied, "user is not permitted to modify this emote"),
			paydayrpc.ErrCodeUserNotPermitted)
	}

	if err == badgetieremotes.EmoteDoesNotExistAssignError {
		return paydayrpc.NewClientError(
			twirp.NotFoundError("emote with the requested ID could not be found"),
			paydayrpc.ErrCodeEmoteDoesNotExist)
	}

	if err == badgetieremotes.InvalidEmoteStateAssignError {
		return paydayrpc.NewClientError(
			twirp.NewError(twirp.FailedPrecondition, "emote state is not valid for group assignment"),
			paydayrpc.ErrCodeInvalidEmoteState)
	}

	if err == badgetieremotes.EmoteCodeNotUniqueAssignError {
		return paydayrpc.NewClientError(
			twirp.NewError(twirp.FailedPrecondition, "emote does not have a unique code among active and pending emotes"),
			paydayrpc.ErrCodeEmoteCodeNotUnique)
	}

	return nil
}
