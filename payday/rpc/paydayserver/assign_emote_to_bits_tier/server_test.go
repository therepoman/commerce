package assign_emote_to_bits_tier

import (
	"context"
	"testing"

	"code.justin.tv/commerce/payday/errors"
	badgetieremotes_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/badgetiers/products/emotes"
	partner_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/partner"
	partnerManager "code.justin.tv/commerce/payday/partner"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestServer_Assign(t *testing.T) {
	Convey("given a Assign API", t, func() {
		partnerManagerMock := new(partner_mock.IPartnerManager)
		bitsTierEmoteAssigner := new(badgetieremotes_mock.Assigner)
		api := &ServerImpl{
			PartnerManager:        partnerManagerMock,
			BitsTierEmoteAssigner: bitsTierEmoteAssigner,
		}

		channelID := "1234"
		emoteID := "someemoteid"
		bitsTierThreshold := int64(1000)

		Convey("when the request is nil", func() {
			var req *paydayrpc.AssignEmoteToBitsTierReq
			resp, err := api.Assign(context.Background(), req)

			Convey("we should return an error", func() {
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		Convey("when channelID is empty", func() {
			req := &paydayrpc.AssignEmoteToBitsTierReq{}
			resp, err := api.Assign(context.Background(), req)

			Convey("we should return an error", func() {
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		Convey("when requestingUserID is empty", func() {
			req := &paydayrpc.AssignEmoteToBitsTierReq{
				ChannelId: channelID,
			}
			resp, err := api.Assign(context.Background(), req)

			Convey("we should return an error", func() {
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		Convey("when requestingUserID is different than channelID", func() {
			req := &paydayrpc.AssignEmoteToBitsTierReq{
				ChannelId:        channelID,
				RequestingUserId: "notchannelid",
			}
			resp, err := api.Assign(context.Background(), req)

			Convey("we should return an error", func() {
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		Convey("when emoteID is empty", func() {
			req := &paydayrpc.AssignEmoteToBitsTierReq{
				ChannelId:        channelID,
				RequestingUserId: channelID,
			}
			resp, err := api.Assign(context.Background(), req)

			Convey("we should return an error", func() {
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		Convey("when the bits tier does not exist", func() {
			req := &paydayrpc.AssignEmoteToBitsTierReq{
				ChannelId:         channelID,
				RequestingUserId:  channelID,
				EmoteId:           emoteID,
				BitsTierThreshold: 1001,
			}
			resp, err := api.Assign(context.Background(), req)

			Convey("we should return an error", func() {
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		Convey("when the partner manager errors", func() {
			partnerManagerMock.On("GetPartnerType", mock.Anything, channelID).Return(partnerManager.UnknownPartnerType, errors.New("dummy error"))

			req := &paydayrpc.AssignEmoteToBitsTierReq{
				ChannelId:         channelID,
				RequestingUserId:  channelID,
				EmoteId:           emoteID,
				BitsTierThreshold: bitsTierThreshold,
			}
			resp, err := api.Assign(context.Background(), req)

			Convey("we should return an error", func() {
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		Convey("when the channel is neither a partner nor an affiliate", func() {
			partnerManagerMock.On("GetPartnerType", mock.Anything, channelID).Return(partnerManager.NonPartnerPartnerType, nil)

			req := &paydayrpc.AssignEmoteToBitsTierReq{
				ChannelId:         channelID,
				RequestingUserId:  channelID,
				EmoteId:           emoteID,
				BitsTierThreshold: bitsTierThreshold,
			}
			resp, err := api.Assign(context.Background(), req)

			Convey("we should return an error", func() {
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		Convey("when the assigner errors", func() {
			partnerManagerMock.On("GetPartnerType", mock.Anything, channelID).Return(partnerManager.TraditionalPartnerType, nil)
			bitsTierEmoteAssigner.On("AssignEmoteToBitsTier", mock.Anything, channelID, emoteID, bitsTierThreshold).Return(errors.New("dummy error"))

			req := &paydayrpc.AssignEmoteToBitsTierReq{
				ChannelId:         channelID,
				RequestingUserId:  channelID,
				EmoteId:           emoteID,
				BitsTierThreshold: bitsTierThreshold,
			}
			resp, err := api.Assign(context.Background(), req)

			Convey("we should return an error", func() {
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		Convey("when the assigner succeeds", func() {
			partnerManagerMock.On("GetPartnerType", mock.Anything, channelID).Return(partnerManager.TraditionalPartnerType, nil)
			bitsTierEmoteAssigner.On("AssignEmoteToBitsTier", mock.Anything, channelID, emoteID, bitsTierThreshold).Return(nil)

			req := &paydayrpc.AssignEmoteToBitsTierReq{
				ChannelId:         channelID,
				RequestingUserId:  channelID,
				EmoteId:           emoteID,
				BitsTierThreshold: bitsTierThreshold,
			}
			resp, err := api.Assign(context.Background(), req)

			Convey("we should return a successful response", func() {
				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
				So(resp.ChannelId, ShouldEqual, channelID)
				So(resp.EmoteId, ShouldEqual, emoteID)
				So(resp.BitsTierThreshold, ShouldEqual, bitsTierThreshold)
			})
		})
	})
}
