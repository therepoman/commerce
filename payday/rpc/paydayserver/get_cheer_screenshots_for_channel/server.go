package get_cheer_screenshots_for_channel

import (
	"context"
	"time"

	"code.justin.tv/commerce/payday/dynamo/cheer_images"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"github.com/twitchtv/twirp"
)

type Server interface {
	Get(ctx context.Context, req *paydayrpc.GetCheerScreenshotsForChannelReq) (*paydayrpc.GetCheerScreenshotsForChannelResp, error)
}

type ServerImpl struct {
	Dao cheer_images.ChannelImagesDao `inject:""`
}

// Update will create or replace the image assets for a channel's specified cheermote tier using images which have already been uploaded via Mako's animated emote upload flow
func (s *ServerImpl) Get(ctx context.Context, req *paydayrpc.GetCheerScreenshotsForChannelReq) (*paydayrpc.GetCheerScreenshotsForChannelResp, error) {
	_, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()

	if req == nil {
		return nil, paydayrpc.NewClientError(twirp.RequiredArgumentError("request"), paydayrpc.ErrCodeMissingRequiredAgrument)
	} else if req.ChannelId == "" {
		return nil, paydayrpc.NewClientError(twirp.RequiredArgumentError("channel_id"), paydayrpc.ErrCodeMissingRequiredAgrument)
	}

	images, err := s.Dao.GetImagesByChannelId(req.ChannelId)

	if err != nil {
		return nil, twirp.InternalError(err.Error())
	}

	var imageResponse []*paydayrpc.CheerImage

	for _, image := range images {
		imageResponse = append(imageResponse, &paydayrpc.CheerImage{
			ImageUrl: image.ImageURL,
			ImageId:  image.ImageID,
		})
	}

	return &paydayrpc.GetCheerScreenshotsForChannelResp{Images: imageResponse}, nil
}
