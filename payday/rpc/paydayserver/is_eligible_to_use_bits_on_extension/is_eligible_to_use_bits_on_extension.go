package is_eligible_to_use_bits_on_extension

import (
	"context"
	"fmt"
	"time"

	falador "code.justin.tv/amzn/TwitchExtCatalogServiceTwirp"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/balance"
	"code.justin.tv/commerce/payday/cache/channel"
	"code.justin.tv/commerce/payday/clients/ems"
	"code.justin.tv/commerce/payday/clients/tmi"
	"code.justin.tv/commerce/payday/extension/transaction"
	"code.justin.tv/commerce/payday/hystrix"
	cmds "code.justin.tv/commerce/payday/hystrix"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"code.justin.tv/commerce/payday/userservice"
	"code.justin.tv/commerce/payday/utils/strings"
	"code.justin.tv/gds/gds/extensions/ems/documents"
	"code.justin.tv/gds/gds/extensions/models"
	"github.com/gofrs/uuid"
	"github.com/twitchtv/twirp"
)

const (
	timeoutDuration           = 5 * time.Second
	faladorDomainPrefix       = "twitch.ext"
	bitsCostType              = "bits"
	prompRequiredDefaultValue = true
)

type Server interface {
	IsEligibleToUseBitsOnExtensionImpl(ctx context.Context, req *paydayrpc.IsEligibleToUseBitsOnExtensionReq) (*paydayrpc.IsEligibleToUseBitsOnExtensionResp, error)
}

type ServerImpl struct {
	Falador                     falador.TwitchExtCatalogService `inject:""`
	BalanceGetter               balance.Getter                  `inject:""`
	ChannelManager              channel.ChannelManager          `inject:""`
	UserServiceChecker          userservice.Checker             `inject:""`
	TMIClient                   tmi.ITMIClient                  `inject:""`
	ExtensionTransactionManager transaction.Manager             `inject:""`
	EMSClient                   ems.IEMSClient                  `inject:""`
}

func getFaladorDomain(extensionClientID string) string {
	return fmt.Sprintf("%s.%s", faladorDomainPrefix, extensionClientID)
}

func (s *ServerImpl) IsEligibleToUseBitsOnExtensionImpl(ctx context.Context, req *paydayrpc.IsEligibleToUseBitsOnExtensionReq) (*paydayrpc.IsEligibleToUseBitsOnExtensionResp, error) {
	ctx, cancel := context.WithTimeout(ctx, timeoutDuration)

	defer cancel()

	if req == nil {
		return nil, twirp.InternalError("Received nil request")
	}
	if strings.Blank(req.SKU) {
		return nil, twirp.InvalidArgumentError("SKU", "SKU should not be blank")
	}
	if strings.Blank(req.Tuid) {
		return nil, twirp.InvalidArgumentError("TUID", "TUID should not be blank")
	}
	if strings.Blank(req.ChannelId) {
		return nil, twirp.InvalidArgumentError("ChannelID", "ChannelID should not be blank")
	}
	if strings.Blank(req.ExtensionClientId) {
		return nil, twirp.InvalidArgumentError("ExtensionClientID", "ExtensionClientID should not be blank")
	}
	if ctx.Value("Twitch-Authorization") == nil || strings.Blank(ctx.Value("Twitch-Authorization").(string)) {
		return nil, twirp.InvalidArgumentError("Twitch-Authorization", "Twitch-Authorization header should not be blank")
	}

	//Falador
	if strings.Blank(req.TransactionId) {
		transactionIdUUID, err := uuid.NewV4()
		if err != nil {
			log.WithError(err).Error("Error generating UUID")
			return nil, twirp.InternalError("Downstream dependency error checking eligibility")
		}
		req.TransactionId = transactionIdUUID.String()
	}

	logFields := log.Fields{
		"ChannelID":         req.ChannelId,
		"ExtensionClientID": req.ExtensionClientId,
		"SKU":               req.SKU,
		"TransactionID":     req.TransactionId,
		"TUID":              req.Tuid,
	}

	product, err := s.getProduct(ctx, req.Tuid, req.SKU, req.ExtensionClientId, logFields)
	if err != nil {
		return nil, err
	}

	if !product.InDevelopment {

		isMonetizationEnabled, extensionState, err := s.getEMSChecks(ctx, req)
		if err != nil {
			log.WithError(err).WithFields(logFields).Error("Error getting data from EMS")
			return nil, twirp.InternalError("Downstream dependency error checking eligibility")
		}

		// false if Extension is not bitEnabled, broadcaster opted out, or user is not whitelisted to see the extension
		if !isMonetizationEnabled {
			return nil, twirp.NewError(twirp.FailedPrecondition, "The extension is not allowed to use Bits on this channel for this user")
		}

		ch, err := s.ChannelManager.Get(ctx, req.ChannelId)
		if err != nil {
			log.WithError(err).WithFields(logFields).Error("Error getting channel from channel manager")
			return nil, twirp.InternalError("Downstream dependency error checking eligibility")
		}

		if ch == nil && req.Tuid != req.ChannelId {
			return nil, twirp.NewError(twirp.FailedPrecondition, "Channel is not eligible for bits")
		}

		//Error if extension is released and User tries to spend bits on own channel
		if req.Tuid == req.ChannelId && !isExtensionTestingState(extensionState) {
			return nil, twirp.InvalidArgumentError("Tuid", "User cannot use bits in their own channel")
		}

		//Error if channel not eligible and not themselves spending
		if req.Tuid != req.ChannelId && !channel.GetChannelEligibility(ch) {
			return nil, twirp.NewError(twirp.FailedPrecondition, "Channel is not eligible for bits")
		}
	}

	banned, err := s.UserServiceChecker.IsBanned(ctx, req.Tuid)

	if err != nil {
		log.WithError(err).WithFields(logFields).Error("Error getting user data from cache retriever")
		return nil, twirp.InternalError("Downstream dependency error checking eligibility")
	}

	if banned {
		return nil, twirp.NewError(twirp.FailedPrecondition, "User is not eligible for bits")
	}

	banned, err = s.TMIClient.CheckUserBannedInChannel(ctx, req.Tuid, req.ChannelId)
	if err != nil {
		log.WithError(err).WithFields(logFields).Error("Error getting ban status from TMI")
		return nil, twirp.InternalError("Downstream dependency error checking eligibility")
	}

	if banned {
		return nil, twirp.NewError(twirp.FailedPrecondition, "User is not eligible for bits")
	}

	bitsSkuCost := int32(product.Cost.Amount)
	err = s.ExtensionTransactionManager.StartTransaction(&transaction.Transaction{
		TransactionID:     req.TransactionId,
		ChannelID:         req.ChannelId,
		TUID:              req.Tuid,
		Product:           *product,
		ExtensionClientID: req.ExtensionClientId,
	})

	if err != nil {
		log.WithError(err).WithFields(logFields).Error("Error caching extension transaction with redis")
		return nil, twirp.InternalError("Downstream dependency error checking eligibility")
	}

	balanceResp, err := s.BalanceGetter.GetBalance(ctx, req.Tuid, false)
	if err != nil {
		log.WithError(err).WithFields(logFields).Error("Error getting balance")
		return nil, twirp.InternalError("Downstream dependency error checking eligibility")
	}

	sufficientBalance := balanceResp >= bitsSkuCost
	return &paydayrpc.IsEligibleToUseBitsOnExtensionResp{
		Balance:           balanceResp,
		PromptRequired:    prompRequiredDefaultValue,
		BitsRequired:      bitsSkuCost,
		TransactionId:     req.TransactionId,
		SufficientBalance: sufficientBalance,
	}, nil
}

func (s *ServerImpl) getProduct(ctx context.Context, userID, sku, extensionClientId string, logFields log.Fields) (*transaction.FaladorProduct, error) {
	var gpResp *falador.Product
	var err error

	err = hystrix.Do(cmds.FaladorGetProductCommand, func() error {
		gpResp, err = s.Falador.GetProduct(ctx, &falador.GetProductRequest{
			UserID: userID,
			Domain: getFaladorDomain(extensionClientId),
			Sku:    sku,
		})
		return err
	}, nil)
	if err != nil {
		if twErr, ok := err.(twirp.Error); ok && twErr.Code() == twirp.InvalidArgument {
			log.WithError(err).WithFields(logFields).Info("Invalid argument error getting product from Falador")
			return nil, twErr
		}
		log.WithError(err).WithFields(logFields).Error("Error getting product from Falador")
		return nil, twirp.InternalError("Downstream dependency error checking eligibility")
	}

	if gpResp == nil || gpResp.Cost == nil {
		log.WithError(err).WithFields(logFields).Error("Received nil response from Falador")
		return nil, twirp.InternalError("Downstream dependency error checking eligibility")
	}

	if gpResp.Cost.Type != bitsCostType {
		return nil, twirp.InvalidArgumentError("SKU", "SKU has a non-bits cost")
	}

	return &transaction.FaladorProduct{
		Sku:           gpResp.Sku,
		Domain:        gpResp.Domain,
		DisplayName:   gpResp.DisplayName,
		Broadcast:     gpResp.Broadcast,
		Expiration:    gpResp.Expiration,
		InDevelopment: gpResp.InDevelopment,
		Cost: &transaction.FaladorCost{
			Type:   gpResp.Cost.Type,
			Amount: gpResp.Cost.Amount,
		},
	}, nil
}

func (s *ServerImpl) getEMSChecks(ctx context.Context, req *paydayrpc.IsEligibleToUseBitsOnExtensionReq) (bool, string, error) {
	var extensions *documents.InstalledExtensionsDocument
	var err error

	err = hystrix.Do(cmds.GetExtensionByChannelIdCommand, func() error {
		extensions, err = s.EMSClient.GetExtensionByChannelID(ctx, req.ChannelId)
		return err
	}, nil)
	if err != nil {
		return false, "", err
	}

	for _, extension := range extensions.InstalledExtensions {
		if extension != nil && extension.Extension != nil && extension.Extension.ID == req.ExtensionClientId {
			if extension.InstallationStatus != nil && extension.InstallationStatus.Abilities != nil {
				//Checking for ability on extension since an extension can be monetized but the broadcaster opted out of that functionality
				//Returning state of extension for checks against Developer testing BiE on own channel
				return extension.InstallationStatus.Abilities.IsBitsEnabled, extension.Extension.State, nil
			}
		}
	}
	return false, "", nil
}

func isExtensionTestingState(extensionState string) bool {

	return extensionState == models.HumanNameByState[models.InTest] ||
		extensionState == models.HumanNameByState[models.ReadyForReview] ||
		extensionState == models.HumanNameByState[models.InReview] ||
		extensionState == models.HumanNameByState[models.PendingAction] ||
		extensionState == models.HumanNameByState[models.Uploading] ||
		extensionState == models.HumanNameByState[models.AssetsUploaded]
}
