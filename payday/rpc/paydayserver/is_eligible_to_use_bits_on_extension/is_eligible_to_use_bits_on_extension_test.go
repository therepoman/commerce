package is_eligible_to_use_bits_on_extension_test

import (
	"context"
	"testing"

	falador "code.justin.tv/amzn/TwitchExtCatalogServiceTwirp"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/errors"
	falador_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/amzn/TwitchExtCatalogServiceTwirp"
	balance_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/balance"
	channel_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/cache/channel"
	ems_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/ems"
	tmi_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/tmi"
	transaction_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/extension/transaction"
	userservice_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/userservice"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"code.justin.tv/commerce/payday/rpc/paydayserver/is_eligible_to_use_bits_on_extension"
	"code.justin.tv/commerce/payday/rpc/utils/test"
	"code.justin.tv/commerce/payday/utils/pointers"
	emsModels "code.justin.tv/gds/gds/extensions/ems/documents"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
	"github.com/twitchtv/twirp"
)

func TestServerImpl_IsEligibleToUseBitsOnExtensionImpl(t *testing.T) {
	Convey("Given an is_eligible server", t, func() {
		faladorClient := new(falador_mock.TwitchExtCatalogService)
		getter := new(balance_mock.Getter)
		channelManager := new(channel_mock.ChannelManager)
		tmiClient := new(tmi_mock.ITMIClient)
		extensionTransactionManager := new(transaction_mock.Manager)
		emsClient := new(ems_mock.IEMSClient)
		userServiceChecker := new(userservice_mock.Checker)
		ctx := context.Background()

		server := &is_eligible_to_use_bits_on_extension.ServerImpl{
			BalanceGetter:               getter,
			Falador:                     faladorClient,
			ChannelManager:              channelManager,
			TMIClient:                   tmiClient,
			ExtensionTransactionManager: extensionTransactionManager,
			EMSClient:                   emsClient,
			UserServiceChecker:          userServiceChecker,
		}

		Convey("Errors when called with a nil request", func() {
			_, err := server.IsEligibleToUseBitsOnExtensionImpl(ctx, nil)
			test.AssertTwirpErrorCode(err, twirp.Internal)
		})

		Convey("Given a valid request", func() {
			testTUID := "testTUID"
			testChannelID := "testChannelID"
			testTransactionID := "testTransactionID"
			testExtensionClientID := "testExtensionClientID"
			req := &paydayrpc.IsEligibleToUseBitsOnExtensionReq{
				Tuid:              testTUID,
				ExtensionClientId: testExtensionClientID,
				SKU:               "testSKU",
				TransactionId:     testTransactionID,
				ChannelId:         testChannelID,
			}

			Convey("Errors when called with blank SKU", func() {
				req.SKU = " "
				_, err := server.IsEligibleToUseBitsOnExtensionImpl(ctx, req)
				test.AssertTwirpErrorCode(err, twirp.InvalidArgument)
			})

			Convey("Errors when called with blank TUID", func() {
				req.Tuid = " "
				_, err := server.IsEligibleToUseBitsOnExtensionImpl(ctx, req)
				test.AssertTwirpErrorCode(err, twirp.InvalidArgument)
			})

			Convey("Errors when called with blank ChannelID", func() {
				req.ChannelId = " "
				_, err := server.IsEligibleToUseBitsOnExtensionImpl(ctx, req)
				test.AssertTwirpErrorCode(err, twirp.InvalidArgument)
			})

			Convey("Errors when called with blank ExtensionClientID", func() {
				req.ExtensionClientId = " "
				_, err := server.IsEligibleToUseBitsOnExtensionImpl(ctx, req)
				test.AssertTwirpErrorCode(err, twirp.InvalidArgument)
			})
			Convey("Errors when called with blank Auth header", func() {
				_, err := server.IsEligibleToUseBitsOnExtensionImpl(ctx, req)
				test.AssertTwirpErrorCode(err, twirp.InvalidArgument)
			})
			Convey("Has auth header", func() {
				ctx = context.WithValue(ctx, "Twitch-Authorization", "TOTALLY COOL")

				Convey("Returns Falador's error when it is an invalid argument error", func() {
					validationError := twirp.InvalidArgumentError("sku", "is no good")

					faladorClient.On("GetProduct", mock.Anything, mock.Anything).Return(nil, validationError)
					_, err := server.IsEligibleToUseBitsOnExtensionImpl(ctx, req)
					test.AssertTwirpErrorCode(err, twirp.InvalidArgument)
					So(err, ShouldEqual, validationError)
				})

				Convey("Errors when falador errors", func() {
					faladorClient.On("GetProduct", mock.Anything, mock.Anything).Return(nil, errors.New("test error"))
					_, err := server.IsEligibleToUseBitsOnExtensionImpl(ctx, req)
					test.AssertTwirpErrorCode(err, twirp.Internal)
				})

				Convey("Errors when falador response is nil", func() {
					faladorClient.On("GetProduct", mock.Anything, mock.Anything).Return(nil, nil)
					_, err := server.IsEligibleToUseBitsOnExtensionImpl(ctx, req)
					test.AssertTwirpErrorCode(err, twirp.Internal)
				})

				Convey("Errors when falador response product cost is nil", func() {
					faladorClient.On("GetProduct", mock.Anything, mock.Anything).Return(&falador.Product{Cost: nil}, nil)
					_, err := server.IsEligibleToUseBitsOnExtensionImpl(ctx, req)
					test.AssertTwirpErrorCode(err, twirp.Internal)
				})

				Convey("Errors when falador response product is not bits type", func() {
					faladorClient.On("GetProduct", mock.Anything, mock.Anything).Return(&falador.Product{
						Cost: &falador.Cost{Amount: 10, Type: "not-bits"},
					}, nil)
					_, err := server.IsEligibleToUseBitsOnExtensionImpl(ctx, req)
					test.AssertTwirpErrorCode(err, twirp.InvalidArgument)
				})

				Convey("When falador returns a non-in-development flagged bits product", func() {
					skuCost := int64(10)
					productSKU := "pagliacci"
					faladorClient.On("GetProduct", mock.Anything, mock.Anything).Return(&falador.Product{
						Cost:          &falador.Cost{Amount: skuCost, Type: "bits"},
						InDevelopment: false,
						Sku:           productSKU,
					}, nil)

					Convey("When EMS client errors", func() {
						emsClient.On("GetExtensionByChannelID", mock.Anything, req.ChannelId).Return(nil, errors.New("You may not eat mexican"))
						_, err := server.IsEligibleToUseBitsOnExtensionImpl(ctx, req)
						test.AssertTwirpErrorCode(err, twirp.Internal)
					})

					Convey("Errors when EMS indicates Bits is not enabled on the extension", func() {
						emsClient.On("GetExtensionByChannelID", mock.Anything, req.ChannelId).Return(&emsModels.InstalledExtensionsDocument{
							InstalledExtensions: []*emsModels.InstalledExtensionDocument{
								{
									Extension: &emsModels.ExtensionDocument{
										ID:    testExtensionClientID,
										State: "Released",
									},
									InstallationStatus: &emsModels.InstallationStatusDocument{
										Abilities: &emsModels.InstallationAbilitiesDocument{
											IsBitsEnabled: false,
										},
									},
								},
							},
						}, nil)
						_, err := server.IsEligibleToUseBitsOnExtensionImpl(ctx, req)
						test.AssertTwirpErrorCode(err, twirp.FailedPrecondition)
					})

					Convey("Errors when EMS indicates the extension is not installed for this channel or user is not whitelisted", func() {
						emsClient.On("GetExtensionByChannelID", mock.Anything, req.ChannelId).Return(&emsModels.InstalledExtensionsDocument{
							InstalledExtensions: []*emsModels.InstalledExtensionDocument{
								{
									Extension: &emsModels.ExtensionDocument{
										ID: "shrimp is the fruit of the sea",
									},
								},
							},
						}, nil)
						_, err := server.IsEligibleToUseBitsOnExtensionImpl(ctx, req)
						test.AssertTwirpErrorCode(err, twirp.FailedPrecondition)
					})

					Convey("When EMS indicates Bits is enabled on the extension and the user is whitelisted", func() {
						emsClient.On("GetExtensionByChannelID", mock.Anything, req.ChannelId).Return(&emsModels.InstalledExtensionsDocument{
							InstalledExtensions: []*emsModels.InstalledExtensionDocument{
								{
									Extension: &emsModels.ExtensionDocument{
										ID:    testExtensionClientID,
										State: "Released",
									},
									InstallationStatus: &emsModels.InstallationStatusDocument{
										Abilities: &emsModels.InstallationAbilitiesDocument{
											IsBitsEnabled: true,
										},
									},
								},
							},
						}, nil)

						Convey("Errors when channel manager call errors", func() {
							channelManager.On("Get", mock.Anything, req.ChannelId).Return(nil, errors.New("test error"))
							_, err := server.IsEligibleToUseBitsOnExtensionImpl(ctx, req)
							test.AssertTwirpErrorCode(err, twirp.Internal)
						})

						Convey("Errors when channel manager returns nil channel", func() {
							channelManager.On("Get", mock.Anything, req.ChannelId).Return(nil, nil)
							_, err := server.IsEligibleToUseBitsOnExtensionImpl(ctx, req)
							test.AssertTwirpErrorCode(err, twirp.FailedPrecondition)
						})

						Convey("Channel manager returns a channel", func() {

							Convey("Extension is not in testing state", func() {

								Convey("Errors when called with the same TUID and ChannelID", func() {
									req.Tuid = "sameID"
									req.ChannelId = "sameID"

									emsClient.On("GetExtensionByChannelID", mock.Anything, req.ChannelId).Return(&emsModels.InstalledExtensionsDocument{
										InstalledExtensions: []*emsModels.InstalledExtensionDocument{
											{
												Extension: &emsModels.ExtensionDocument{
													ID:    testExtensionClientID,
													State: "Released",
												},
												InstallationStatus: &emsModels.InstallationStatusDocument{
													Abilities: &emsModels.InstallationAbilitiesDocument{
														IsBitsEnabled: true,
													},
												},
											},
										},
									}, nil)

									channelManager.On("Get", mock.Anything, req.ChannelId).Return(&dynamo.Channel{
										Id:        dynamo.ChannelId(req.ChannelId),
										Onboarded: pointers.BoolP(true),
									}, nil)

									_, err := server.IsEligibleToUseBitsOnExtensionImpl(ctx, req)
									test.AssertTwirpErrorCode(err, twirp.InvalidArgument)
								})

								Convey("Errors when channel manager returns not onboarded channel", func() {
									channelManager.On("Get", mock.Anything, req.ChannelId).Return(&dynamo.Channel{
										Id:        dynamo.ChannelId(req.ChannelId),
										Onboarded: pointers.BoolP(false),
									}, nil)
									_, err := server.IsEligibleToUseBitsOnExtensionImpl(ctx, req)
									test.AssertTwirpErrorCode(err, twirp.FailedPrecondition)
								})

								Convey("Errors when channel manager returns opted-out channel", func() {
									channelManager.On("Get", mock.Anything, req.ChannelId).Return(&dynamo.Channel{
										Id:        dynamo.ChannelId(req.ChannelId),
										Onboarded: pointers.BoolP(true),
										OptedOut:  pointers.BoolP(true),
									}, nil)
									_, err := server.IsEligibleToUseBitsOnExtensionImpl(ctx, req)
									test.AssertTwirpErrorCode(err, twirp.FailedPrecondition)
								})

								Convey("When channel managers returns a valid bits enabled channel", func() {
									channelManager.On("Get", mock.Anything, req.ChannelId).Return(&dynamo.Channel{
										Id:        dynamo.ChannelId(req.ChannelId),
										Onboarded: pointers.BoolP(true),
									}, nil)

									//following tests are after "SKU in development" section tests
									Convey("Errors when user checker retriever errors", func() {
										userServiceChecker.On("IsBanned", mock.Anything, req.Tuid).Return(false, errors.New("test error"))
										_, err := server.IsEligibleToUseBitsOnExtensionImpl(ctx, req)
										test.AssertTwirpErrorCode(err, twirp.Internal)
									})

									Convey("Errors when user checker indicates the user is banned", func() {
										userServiceChecker.On("IsBanned", mock.Anything, req.Tuid).Return(true, nil)
										_, err := server.IsEligibleToUseBitsOnExtensionImpl(ctx, req)
										test.AssertTwirpErrorCode(err, twirp.FailedPrecondition)
									})

									Convey("When user checker indicates the user is not banned", func() {
										userServiceChecker.On("IsBanned", mock.Anything, req.Tuid).Return(false, nil)

										Convey("Errors when TMI errors checking ban status", func() {
											tmiClient.On("CheckUserBannedInChannel", mock.Anything, req.Tuid, req.ChannelId).Return(false, errors.New("test error"))
											_, err := server.IsEligibleToUseBitsOnExtensionImpl(ctx, req)
											test.AssertTwirpErrorCode(err, twirp.Internal)
										})

										Convey("Errors when TMI indicates the user is banned in the channel", func() {
											tmiClient.On("CheckUserBannedInChannel", mock.Anything, req.Tuid, req.ChannelId).Return(true, nil)
											_, err := server.IsEligibleToUseBitsOnExtensionImpl(ctx, req)
											test.AssertTwirpErrorCode(err, twirp.FailedPrecondition)
										})

										Convey("When TMI indicates the user is not banned in the channel", func() {
											tmiClient.On("CheckUserBannedInChannel", mock.Anything, req.Tuid, req.ChannelId).Return(false, nil)

											Convey("Errors when transaction caching fails", func() {
												extensionTransactionManager.On("StartTransaction", mock.Anything).Return(errors.New("test error"))
												_, err := server.IsEligibleToUseBitsOnExtensionImpl(ctx, req)
												test.AssertTwirpErrorCode(err, twirp.Internal)
											})

											Convey("When transaction caching succeeds", func() {
												extensionTransactionManager.On("StartTransaction", mock.Anything).Return(nil)

												Convey("Errors when getBalance call fails", func() {
													getter.On("GetBalance", mock.Anything, req.Tuid, false).Return(int32(0), errors.New("test error")).Once()
													_, err := server.IsEligibleToUseBitsOnExtensionImpl(ctx, req)
													test.AssertTwirpErrorCode(err, twirp.Internal)
												})

												Convey("When getBalance call returns nil resp", func() {
													getter.On("GetBalance", mock.Anything, req.Tuid, false).Return(int32(0), nil).Once()

													Convey("Uses a zero balance and succeeds", func() {
														resp, err := server.IsEligibleToUseBitsOnExtensionImpl(ctx, req)
														So(err, ShouldBeNil)
														So(resp, ShouldNotBeNil)
														So(resp.BitsRequired, ShouldEqual, int32(skuCost))
														So(resp.PromptRequired, ShouldBeTrue)
														So(resp.TransactionId, ShouldEqual, testTransactionID)
														So(resp.Balance, ShouldEqual, 0)
														So(resp.SufficientBalance, ShouldEqual, false)
													})
												})

												Convey("When getBalance call returns resp with an insufficient bits balance", func() {
													getter.On("GetBalance", mock.Anything, req.Tuid, false).Return(int32(9), nil).Once()

													Convey("Succeeds and returns sufficient balance flag as false", func() {
														resp, err := server.IsEligibleToUseBitsOnExtensionImpl(ctx, req)
														So(err, ShouldBeNil)
														So(resp, ShouldNotBeNil)
														So(resp.BitsRequired, ShouldEqual, int32(skuCost))
														So(resp.PromptRequired, ShouldBeTrue)
														So(resp.TransactionId, ShouldEqual, testTransactionID)
														So(resp.Balance, ShouldEqual, 9)
														So(resp.SufficientBalance, ShouldEqual, false)
													})
												})

												Convey("When getBalance call returns resp with an sufficient bits balance", func() {
													getter.On("GetBalance", mock.Anything, req.Tuid, false).Return(int32(10), nil).Once()

													Convey("Succeeds and returns correct response for happy path", func() {
														resp, err := server.IsEligibleToUseBitsOnExtensionImpl(ctx, req)
														So(err, ShouldBeNil)
														So(resp, ShouldNotBeNil)
														So(resp.BitsRequired, ShouldEqual, int32(skuCost))
														So(resp.PromptRequired, ShouldBeTrue)
														So(resp.TransactionId, ShouldEqual, testTransactionID)
														So(resp.Balance, ShouldEqual, 10)
														So(resp.SufficientBalance, ShouldEqual, true)
													})

													Convey("Generates a transactionID and succeeds when one is not provided", func() {
														req.TransactionId = ""
														resp, err := server.IsEligibleToUseBitsOnExtensionImpl(ctx, req)
														So(err, ShouldBeNil)
														So(resp.TransactionId, ShouldNotBeEmpty)
													})

													Convey("Uses same bits cost when falador returns in-development sku", func() {
														faladorClient.ExpectedCalls[0].Return(&falador.Product{
															Cost:          &falador.Cost{Amount: skuCost, Type: "bits"},
															InDevelopment: true,
															Sku:           productSKU,
														}, nil)
														resp, err := server.IsEligibleToUseBitsOnExtensionImpl(ctx, req)
														So(err, ShouldBeNil)
														So(resp.BitsRequired, ShouldEqual, int32(skuCost))
														So(resp.SufficientBalance, ShouldEqual, true)
													})
												})
											})
										})
									})
								})

							})

							Convey("Extension is in testing state", func() {

								Convey("When TUID and ChannelID are equal", func() {
									req.Tuid = "sameID"
									req.ChannelId = "sameID"

									emsClient.On("GetExtensionByChannelID", mock.Anything, req.ChannelId).Return(&emsModels.InstalledExtensionsDocument{
										InstalledExtensions: []*emsModels.InstalledExtensionDocument{
											{
												Extension: &emsModels.ExtensionDocument{
													ID:    testExtensionClientID,
													State: "Testing",
												},
												InstallationStatus: &emsModels.InstallationStatusDocument{
													Abilities: &emsModels.InstallationAbilitiesDocument{
														IsBitsEnabled: true,
													},
												},
											},
										},
									}, nil)

									//Following 3 cases are equal
									Convey("Channel status has no effect on extensions in testing when TUID == ChannelID", func() {
										channelManager.On("Get", mock.Anything, req.ChannelId).Return(&dynamo.Channel{
											Id:        dynamo.ChannelId(req.ChannelId),
											Onboarded: pointers.BoolP(false),
											OptedOut:  pointers.BoolP(true),
										}, nil)

										//following tests are after "SKU in development" section tests
										Convey("Errors when user checker errors", func() {
											userServiceChecker.On("IsBanned", mock.Anything, req.Tuid).Return(false, errors.New("test error"))
											_, err := server.IsEligibleToUseBitsOnExtensionImpl(ctx, req)
											test.AssertTwirpErrorCode(err, twirp.Internal)
										})

										Convey("Errors when user checker indicates the user is banned", func() {
											userServiceChecker.On("IsBanned", mock.Anything, req.Tuid).Return(true, nil)
											_, err := server.IsEligibleToUseBitsOnExtensionImpl(ctx, req)
											test.AssertTwirpErrorCode(err, twirp.FailedPrecondition)
										})

										Convey("When user checker indicates the user is not banned", func() {
											userServiceChecker.On("IsBanned", mock.Anything, req.Tuid).Return(false, nil)

											Convey("Errors when TMI errors checking ban status", func() {
												tmiClient.On("CheckUserBannedInChannel", mock.Anything, req.Tuid, req.ChannelId).Return(false, errors.New("test error"))
												_, err := server.IsEligibleToUseBitsOnExtensionImpl(ctx, req)
												test.AssertTwirpErrorCode(err, twirp.Internal)
											})

											Convey("Errors when TMI indicates the user is banned in the channel", func() {
												tmiClient.On("CheckUserBannedInChannel", mock.Anything, req.Tuid, req.ChannelId).Return(true, nil)
												_, err := server.IsEligibleToUseBitsOnExtensionImpl(ctx, req)
												test.AssertTwirpErrorCode(err, twirp.FailedPrecondition)
											})

											Convey("When TMI indicates the user is not banned in the channel", func() {
												tmiClient.On("CheckUserBannedInChannel", mock.Anything, req.Tuid, req.ChannelId).Return(false, nil)

												Convey("Errors when transaction caching fails", func() {
													extensionTransactionManager.On("StartTransaction", mock.Anything).Return(errors.New("test error"))
													_, err := server.IsEligibleToUseBitsOnExtensionImpl(ctx, req)
													test.AssertTwirpErrorCode(err, twirp.Internal)
												})

												Convey("When transaction caching succeeds", func() {
													extensionTransactionManager.On("StartTransaction", mock.Anything).Return(nil)

													Convey("Errors when getBalance call fails", func() {
														getter.On("GetBalance", mock.Anything, req.Tuid, false).Return(int32(0), errors.New("test error")).Once()
														_, err := server.IsEligibleToUseBitsOnExtensionImpl(ctx, req)
														test.AssertTwirpErrorCode(err, twirp.Internal)
													})

													Convey("When getBalance call returns nil resp", func() {
														getter.On("GetBalance", mock.Anything, req.Tuid, false).Return(int32(0), nil).Once()

														Convey("Uses a zero balance and succeeds", func() {
															resp, err := server.IsEligibleToUseBitsOnExtensionImpl(ctx, req)
															So(err, ShouldBeNil)
															So(resp, ShouldNotBeNil)
															So(resp.BitsRequired, ShouldEqual, int32(skuCost))
															So(resp.PromptRequired, ShouldBeTrue)
															So(resp.TransactionId, ShouldEqual, testTransactionID)
															So(resp.Balance, ShouldEqual, 0)
															So(resp.SufficientBalance, ShouldEqual, false)
														})
													})

													Convey("When getBalance call returns resp with an insufficient bits balance", func() {
														getter.On("GetBalance", mock.Anything, req.Tuid, false).Return(int32(9), nil).Once()

														Convey("Succeeds and returns sufficient balance flag as false", func() {
															resp, err := server.IsEligibleToUseBitsOnExtensionImpl(ctx, req)
															So(err, ShouldBeNil)
															So(resp, ShouldNotBeNil)
															So(resp.BitsRequired, ShouldEqual, int32(skuCost))
															So(resp.PromptRequired, ShouldBeTrue)
															So(resp.TransactionId, ShouldEqual, testTransactionID)
															So(resp.Balance, ShouldEqual, 9)
															So(resp.SufficientBalance, ShouldEqual, false)
														})
													})

													Convey("When getBalance call returns resp with an sufficient bits balance", func() {
														getter.On("GetBalance", mock.Anything, req.Tuid, false).Return(int32(10), nil).Once()

														Convey("Succeeds and returns correct response for happy path", func() {
															resp, err := server.IsEligibleToUseBitsOnExtensionImpl(ctx, req)
															So(err, ShouldBeNil)
															So(resp, ShouldNotBeNil)
															So(resp.BitsRequired, ShouldEqual, int32(skuCost))
															So(resp.PromptRequired, ShouldBeTrue)
															So(resp.TransactionId, ShouldEqual, testTransactionID)
															So(resp.Balance, ShouldEqual, 10)
															So(resp.SufficientBalance, ShouldEqual, true)
														})

														Convey("Generates a transactionID and succeeds when one is not provided", func() {
															req.TransactionId = ""
															resp, err := server.IsEligibleToUseBitsOnExtensionImpl(ctx, req)
															So(err, ShouldBeNil)
															So(resp.TransactionId, ShouldNotBeEmpty)
														})

														Convey("Uses same bits cost when falador returns in-development sku", func() {
															faladorClient.ExpectedCalls[0].Return(&falador.Product{
																Cost:          &falador.Cost{Amount: skuCost, Type: "bits"},
																InDevelopment: true,
																Sku:           productSKU,
															}, nil)
															resp, err := server.IsEligibleToUseBitsOnExtensionImpl(ctx, req)
															So(err, ShouldBeNil)
															So(resp.BitsRequired, ShouldEqual, int32(skuCost))
															So(resp.SufficientBalance, ShouldEqual, true)
														})
													})
												})
											})
										})
									})
								})

								Convey("When TUID and ChannelID are not equal", func() {
									req.Tuid = "sameID"
									req.ChannelId = "differentID"

									emsClient.On("GetExtensionByChannelID", mock.Anything, req.ChannelId).Return(&emsModels.InstalledExtensionsDocument{
										InstalledExtensions: []*emsModels.InstalledExtensionDocument{
											{
												Extension: &emsModels.ExtensionDocument{
													ID:    testExtensionClientID,
													State: "Testing",
												},
												InstallationStatus: &emsModels.InstallationStatusDocument{
													Abilities: &emsModels.InstallationAbilitiesDocument{
														IsBitsEnabled: true,
													},
												},
											},
										},
									}, nil)

									Convey("Errors when channel manager returns not onboarded channel", func() {
										channelManager.On("Get", mock.Anything, req.ChannelId).Return(&dynamo.Channel{
											Id:        dynamo.ChannelId(req.ChannelId),
											Onboarded: pointers.BoolP(false),
										}, nil)
										_, err := server.IsEligibleToUseBitsOnExtensionImpl(ctx, req)
										test.AssertTwirpErrorCode(err, twirp.FailedPrecondition)
									})

									Convey("Errors when channel manager returns opted-out channel", func() {
										channelManager.On("Get", mock.Anything, req.ChannelId).Return(&dynamo.Channel{
											Id:        dynamo.ChannelId(req.ChannelId),
											Onboarded: pointers.BoolP(true),
											OptedOut:  pointers.BoolP(true),
										}, nil)
										_, err := server.IsEligibleToUseBitsOnExtensionImpl(ctx, req)
										test.AssertTwirpErrorCode(err, twirp.FailedPrecondition)
									})

									Convey("When channel managers returns a valid bits enabled channel", func() {
										channelManager.On("Get", mock.Anything, req.ChannelId).Return(&dynamo.Channel{
											Id:        dynamo.ChannelId(req.ChannelId),
											Onboarded: pointers.BoolP(true),
										}, nil)

										//following tests are after "SKU in development" section tests
										Convey("Errors when user checker retriever errors", func() {
											userServiceChecker.On("IsBanned", mock.Anything, req.Tuid).Return(false, errors.New("test error"))
											_, err := server.IsEligibleToUseBitsOnExtensionImpl(ctx, req)
											test.AssertTwirpErrorCode(err, twirp.Internal)
										})

										Convey("Errors when user checker retriever indicates the user is banned", func() {
											userServiceChecker.On("IsBanned", mock.Anything, req.Tuid).Return(true, nil)
											_, err := server.IsEligibleToUseBitsOnExtensionImpl(ctx, req)
											test.AssertTwirpErrorCode(err, twirp.FailedPrecondition)
										})

										Convey("When user checker indicates the user is not banned", func() {
											userServiceChecker.On("IsBanned", mock.Anything, req.Tuid).Return(false, nil)

											Convey("Errors when TMI errors checking ban status", func() {
												tmiClient.On("CheckUserBannedInChannel", mock.Anything, req.Tuid, req.ChannelId).Return(false, errors.New("test error"))
												_, err := server.IsEligibleToUseBitsOnExtensionImpl(ctx, req)
												test.AssertTwirpErrorCode(err, twirp.Internal)
											})

											Convey("Errors when TMI indicates the user is banned in the channel", func() {
												tmiClient.On("CheckUserBannedInChannel", mock.Anything, req.Tuid, req.ChannelId).Return(true, nil)
												_, err := server.IsEligibleToUseBitsOnExtensionImpl(ctx, req)
												test.AssertTwirpErrorCode(err, twirp.FailedPrecondition)
											})

											Convey("When TMI indicates the user is not banned in the channel", func() {
												tmiClient.On("CheckUserBannedInChannel", mock.Anything, req.Tuid, req.ChannelId).Return(false, nil)

												Convey("Errors when transaction caching fails", func() {
													extensionTransactionManager.On("StartTransaction", mock.Anything).Return(errors.New("test error"))
													_, err := server.IsEligibleToUseBitsOnExtensionImpl(ctx, req)
													test.AssertTwirpErrorCode(err, twirp.Internal)
												})

												Convey("When transaction caching succeeds", func() {
													extensionTransactionManager.On("StartTransaction", mock.Anything).Return(nil)

													Convey("Errors when getBalance call fails", func() {
														getter.On("GetBalance", mock.Anything, req.Tuid, false).Return(int32(0), errors.New("test error")).Once()
														_, err := server.IsEligibleToUseBitsOnExtensionImpl(ctx, req)
														test.AssertTwirpErrorCode(err, twirp.Internal)
													})

													Convey("When getBalance call returns nil resp", func() {
														getter.On("GetBalance", mock.Anything, req.Tuid, false).Return(int32(0), nil, nil).Once()

														Convey("Uses a zero balance and succeeds", func() {
															resp, err := server.IsEligibleToUseBitsOnExtensionImpl(ctx, req)
															So(err, ShouldBeNil)
															So(resp, ShouldNotBeNil)
															So(resp.BitsRequired, ShouldEqual, int32(skuCost))
															So(resp.PromptRequired, ShouldBeTrue)
															So(resp.TransactionId, ShouldEqual, testTransactionID)
															So(resp.Balance, ShouldEqual, 0)
															So(resp.SufficientBalance, ShouldEqual, false)
														})
													})

													Convey("When getBalance call returns resp with an insufficient bits balance", func() {
														getter.On("GetBalance", mock.Anything, req.Tuid, false).Return(int32(9), nil, nil).Once()

														Convey("Succeeds and returns sufficient balance flag as false", func() {
															resp, err := server.IsEligibleToUseBitsOnExtensionImpl(ctx, req)
															So(err, ShouldBeNil)
															So(resp, ShouldNotBeNil)
															So(resp.BitsRequired, ShouldEqual, int32(skuCost))
															So(resp.PromptRequired, ShouldBeTrue)
															So(resp.TransactionId, ShouldEqual, testTransactionID)
															So(resp.Balance, ShouldEqual, 9)
															So(resp.SufficientBalance, ShouldEqual, false)
														})
													})

													Convey("When getBalance call returns resp with an sufficient bits balance", func() {
														getter.On("GetBalance", mock.Anything, req.Tuid, false).Return(int32(10), nil, nil).Once()

														Convey("Succeeds and returns correct response for happy path", func() {
															resp, err := server.IsEligibleToUseBitsOnExtensionImpl(ctx, req)
															So(err, ShouldBeNil)
															So(resp, ShouldNotBeNil)
															So(resp.BitsRequired, ShouldEqual, int32(skuCost))
															So(resp.PromptRequired, ShouldBeTrue)
															So(resp.TransactionId, ShouldEqual, testTransactionID)
															So(resp.Balance, ShouldEqual, 10)
															So(resp.SufficientBalance, ShouldEqual, true)
														})

														Convey("Generates a transactionID and succeeds when one is not provided", func() {
															req.TransactionId = ""
															resp, err := server.IsEligibleToUseBitsOnExtensionImpl(ctx, req)
															So(err, ShouldBeNil)
															So(resp.TransactionId, ShouldNotBeEmpty)
														})

														Convey("Uses same bits cost when falador returns in-development sku", func() {
															faladorClient.ExpectedCalls[0].Return(&falador.Product{
																Cost:          &falador.Cost{Amount: skuCost, Type: "bits"},
																InDevelopment: true,
																Sku:           productSKU,
															}, nil)
															resp, err := server.IsEligibleToUseBitsOnExtensionImpl(ctx, req)
															So(err, ShouldBeNil)
															So(resp.BitsRequired, ShouldEqual, int32(skuCost))
															So(resp.SufficientBalance, ShouldEqual, true)
														})
													})
												})
											})
										})
									})
								})
							})
						})

					})
				})

				Convey("When falador returns a in-development flagged bits product", func() {
					skuCost := int64(10)
					productSKU := "popcorn-shrimp-popeyes"
					faladorClient.On("GetProduct", mock.Anything, mock.Anything).Return(&falador.Product{
						Cost:          &falador.Cost{Amount: skuCost, Type: "bits"},
						InDevelopment: true,
						Sku:           productSKU,
					}, nil)

					userServiceChecker.On("IsBanned", mock.Anything, req.Tuid).Return(false, nil)
					extensionTransactionManager.On("StartTransaction", mock.Anything).Return(nil)
					tmiClient.On("CheckUserBannedInChannel", mock.Anything, req.Tuid, req.ChannelId).Return(false, nil)
					getter.On("GetBalance", mock.Anything, req.Tuid, false).Return(int32(10), nil, nil)

					Convey("Channel status has no effect on inDevelopment SKU's", func() {
						channelManager.On("Get", mock.Anything, req.ChannelId).Return(&dynamo.Channel{
							Id:        dynamo.ChannelId(req.ChannelId),
							Onboarded: pointers.BoolP(false),
							OptedOut:  pointers.BoolP(true),
						}, nil)

						Convey("Is ok when called with the same TUID and ChannelID", func() {
							req.ChannelId = req.Tuid

							tmiClient.On("CheckUserBannedInChannel", mock.Anything, req.Tuid, req.ChannelId).Return(false, nil)
							getter.On("GetBalance", mock.Anything, req.Tuid, false).Return(int32(10), nil, nil)

							resp, err := server.IsEligibleToUseBitsOnExtensionImpl(ctx, req)
							So(err, ShouldBeNil)
							So(resp.BitsRequired, ShouldEqual, int32(skuCost))
							So(resp.SufficientBalance, ShouldEqual, true)
						})

						Convey("Is making requests with different TUID and ChannelID", func() {

							resp, err := server.IsEligibleToUseBitsOnExtensionImpl(ctx, req)
							So(err, ShouldBeNil)
							So(resp.BitsRequired, ShouldEqual, int32(skuCost))
							So(resp.SufficientBalance, ShouldEqual, true)
						})
					})
				})
			})
		})
	})
}
