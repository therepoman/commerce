package set_user_campaign_status

import (
	"context"
	"testing"

	channel_status_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sponsored_cheermote/channel_status"
	eligible_channel_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sponsored_cheermote/eligible_channel"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestServerImpl_Set(t *testing.T) {
	Convey("given a GetUserCampaignStatus server", t, func() {
		setter := new(channel_status_mock.Setter)
		pubSubber := new(channel_status_mock.PubSubber)
		eligibleChannelGetter := new(eligible_channel_mock.Getter)

		server := &ServerImpl{
			Setter:                setter,
			PubSubber:             pubSubber,
			EligibleChannelGetter: eligibleChannelGetter,
		}

		userID := "123123123"
		campaignID := "savethewalrus"

		Convey("when the request is nil", func() {
			var req *paydayrpc.SetUserCampaignStatusReq

			Convey("we should return an error", func() {
				_, err := server.Set(context.Background(), req)

				So(err, ShouldNotBeNil)
			})
		})

		Convey("when the request does not have a user ID", func() {
			req := &paydayrpc.SetUserCampaignStatusReq{}

			Convey("we should return an error", func() {
				_, err := server.Set(context.Background(), req)

				So(err, ShouldNotBeNil)
			})
		})

		Convey("when the request does not have a campaign ID", func() {
			req := &paydayrpc.SetUserCampaignStatusReq{
				UserId: userID,
			}

			Convey("we should return an error", func() {
				_, err := server.Set(context.Background(), req)

				So(err, ShouldNotBeNil)
			})
		})

		Convey("when we have a valid request", func() {
			req := &paydayrpc.SetUserCampaignStatusReq{
				UserId:     userID,
				CampaignId: campaignID,
				OptedIn:    true,
			}

			Convey("when we fail to get channel's eligibility to this campaign", func() {
				eligibleChannelGetter.On("Get", mock.Anything, userID, campaignID).Return(false, errors.New("OHKI STRIKE"))

				Convey("we should return an error", func() {
					_, err := server.Set(context.Background(), req)

					So(err, ShouldNotBeNil)
				})
			})

			Convey("when the channel is not eligible to this campaign", func() {
				eligibleChannelGetter.On("Get", mock.Anything, userID, campaignID).Return(false, nil)

				Convey("we should return an error", func() {
					_, err := server.Set(context.Background(), req)

					So(err, ShouldNotBeNil)
				})
			})

			Convey("when the channel is eligible to this campaign", func() {
				eligibleChannelGetter.On("Get", mock.Anything, userID, campaignID).Return(true, nil)

				Convey("when we fail to set the campaign channel status", func() {
					setter.On("Set", mock.Anything, userID, campaignID, true).Return(errors.New("WALRUS STRIKE"))

					Convey("we should return an error", func() {
						_, err := server.Set(context.Background(), req)

						So(err, ShouldNotBeNil)
					})
				})

				Convey("when we succeed in updating the campaign channel status to opt in for the user", func() {
					setter.On("Set", mock.Anything, userID, campaignID, true).Return(nil)

					Convey("when we fail to pubsub that they have opted in", func() {
						pubSubber.On("OptIn", mock.Anything, campaignID, userID).Return(errors.New("WALRUS STRIKE"))

						Convey("we should return an error", func() {
							_, err := server.Set(context.Background(), req)

							So(err, ShouldNotBeNil)
							pubSubber.AssertCalled(t, "OptIn", mock.Anything, campaignID, userID)
						})
					})

					Convey("when we succeed to pubsub that they have opted in", func() {
						pubSubber.On("OptIn", mock.Anything, campaignID, userID).Return(nil)

						Convey("we should return that we succeeded", func() {
							resp, err := server.Set(context.Background(), req)

							So(err, ShouldBeNil)
							So(resp, ShouldNotBeNil)
							pubSubber.AssertCalled(t, "OptIn", mock.Anything, campaignID, userID)
						})
					})
				})

				Convey("when we succeed in updating the campaign channel status to opt out for the user", func() {
					req.OptedIn = false
					setter.On("Set", mock.Anything, userID, campaignID, false).Return(nil)

					Convey("when we fail to pubsub that they have opted out", func() {
						pubSubber.On("OptOut", mock.Anything, campaignID, userID).Return(errors.New("WALRUS STRIKE"))

						Convey("we should return an error", func() {
							_, err := server.Set(context.Background(), req)

							So(err, ShouldNotBeNil)
							pubSubber.AssertCalled(t, "OptOut", mock.Anything, campaignID, userID)
						})
					})

					Convey("when we succeed to pubsub that they have opted out", func() {
						pubSubber.On("OptOut", mock.Anything, campaignID, userID).Return(nil)

						Convey("we should return that we succeeded", func() {
							resp, err := server.Set(context.Background(), req)

							So(err, ShouldBeNil)
							So(resp, ShouldNotBeNil)
							pubSubber.AssertCalled(t, "OptOut", mock.Anything, campaignID, userID)
						})
					})
				})
			})
		})
	})
}
