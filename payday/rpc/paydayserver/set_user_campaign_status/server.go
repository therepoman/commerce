package set_user_campaign_status

import (
	"context"

	log "code.justin.tv/commerce/logrus"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"code.justin.tv/commerce/payday/sponsored_cheermote/channel_status"
	"code.justin.tv/commerce/payday/sponsored_cheermote/eligible_channel"
	"github.com/twitchtv/twirp"
)

type Server interface {
	Set(ctx context.Context, req *paydayrpc.SetUserCampaignStatusReq) (*paydayrpc.SetUserCampaignStatusResp, error)
}

type ServerImpl struct {
	Setter                channel_status.Setter    `inject:""`
	PubSubber             channel_status.PubSubber `inject:""`
	EligibleChannelGetter eligible_channel.Getter  `inject:""`
}

func (s *ServerImpl) Set(ctx context.Context, req *paydayrpc.SetUserCampaignStatusReq) (*paydayrpc.SetUserCampaignStatusResp, error) {
	if req == nil {
		return nil, twirp.InvalidArgumentError("UserId", "cannot have empty request")
	}
	if req.UserId == "" {
		return nil, twirp.InvalidArgumentError("UserId", "cannot have empty user id")
	}
	if req.CampaignId == "" {
		return nil, twirp.InvalidArgumentError("CampaignId", "cannot have empty campaign id")
	}

	eligible, err := s.EligibleChannelGetter.Get(ctx, req.UserId, req.CampaignId)
	if err != nil {
		log.WithError(err).WithFields(log.Fields{"ChannelID": req.UserId, "CampaignID": req.CampaignId}).Error("could not get channel eligibility")
		return nil, twirp.InternalErrorWith(err)
	}

	if !eligible {
		log.WithFields(log.Fields{"ChannelID": req.UserId, "CampaignID": req.CampaignId}).Error("channel is not eligible for this campaign")
		return nil, twirp.InternalError("channel is not eligible for this campaign")
	}

	err = s.Setter.Set(ctx, req.UserId, req.CampaignId, req.OptedIn)

	if err != nil {
		log.WithError(err).WithFields(log.Fields{"userID": req.UserId, "campaignId": req.CampaignId}).Error("could not update campaign settings for user")
		return nil, twirp.InternalErrorWith(err)
	}

	if req.OptedIn {
		err := s.PubSubber.OptIn(ctx, req.CampaignId, req.UserId)
		if err != nil {
			return nil, twirp.InternalErrorWith(err)
		}
	} else {
		err := s.PubSubber.OptOut(ctx, req.CampaignId, req.UserId)
		if err != nil {
			return nil, twirp.InternalErrorWith(err)
		}
	}

	return &paydayrpc.SetUserCampaignStatusResp{}, nil
}
