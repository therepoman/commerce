package grant_pinata_bonus_bits

import (
	"context"
	"testing"

	"code.justin.tv/commerce/payday/clients/miniexperiments"
	clients_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/miniexperiments"
	dynamo_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo"
	pachter_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/pachter"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"code.justin.tv/commerce/payday/transactions"
	"github.com/gofrs/uuid"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestServer_GrantPinataBonusBits(t *testing.T) {
	ctx := context.Background()

	Convey("given a Server API", t, func() {
		mockTransactionDao := new(dynamo_mock.ITransactionsDao)
		mockPachterMessenger := new(pachter_mock.Messenger)
		mockExperimentClient := new(clients_mock.MiniExperimentClient)

		api := &ServerImpl{
			TransactionDAO:       mockTransactionDao,
			Converter:            transactions.NewConverter(),
			Messenger:            mockPachterMessenger,
			MiniExperimentClient: mockExperimentClient,
		}

		Convey("with a nil request", func() {
			resp, err := api.GrantPinataBonusBits(ctx, nil)
			So(resp, ShouldBeNil)
			So(err, ShouldBeError)
		})

		req := paydayrpc.GrantPinataBonusBitsReq{}

		Convey("with an empty request in", func() {
			resp, err := api.GrantPinataBonusBits(ctx, &req)
			So(resp, ShouldBeNil)
			So(err, ShouldBeError)
		})

		Convey("with a request with a transaction id ", func() {
			transactionID, _ := uuid.NewV4()
			req.TransactionId = transactionID.String()

			Convey("with a request with a channel id ", func() {
				req.ChannelId = "104874624"

				Convey("with a request with a pinata bonus amount", func() {
					req.PinataBonus = 1000

					Convey("with a channel that is not in the experiment", func() {
						mockExperimentClient.On("GetTreatment", mock.Anything, req.ChannelId).Return(miniexperiments.TreatmentControl, nil)

						resp, err := api.GrantPinataBonusBits(ctx, &req)
						So(err, ShouldNotBeNil)
						So(resp, ShouldBeNil)

						mockPachterMessenger.AssertNumberOfCalls(t, "SendTransactionToPachter", 0)
					})

					Convey("with a channel that is in the experiment", func() {
						mockExperimentClient.On("GetTreatment", mock.Anything, req.ChannelId).Return("Experiment", nil)

						Convey("with a transaction DAO that lets you store without errors", func() {
							mockTransactionDao.On("CreateNew", mock.Anything).Return(nil)

							Convey("with a SNS client that posts to topic without errors", func() {
								mockPachterMessenger.On("SendTransactionToPachter", ctx, mock.Anything).Return(nil)

								resp, err := api.GrantPinataBonusBits(ctx, &req)
								So(err, ShouldBeNil)
								So(resp, ShouldNotBeNil)

								mockPachterMessenger.AssertCalled(t, "SendTransactionToPachter", ctx, mock.Anything)
							})
						})
					})
				})
			})
		})
	})
}
