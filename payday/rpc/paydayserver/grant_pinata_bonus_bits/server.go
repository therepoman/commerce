package grant_pinata_bonus_bits

import (
	"context"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/clients/miniexperiments"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/pachter"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"code.justin.tv/commerce/payday/transactions"
	"code.justin.tv/commerce/payday/utils/pointers"
	"github.com/twitchtv/twirp"
)

const (
	experimentName    = "BitsPinata"
	experimentVariant = "Experiment"
)

type Server interface {
	GrantPinataBonusBits(ctx context.Context, req *paydayrpc.GrantPinataBonusBitsReq) (*paydayrpc.GrantPinataBonusBitsResp, error)
}

type ServerImpl struct {
	TransactionDAO       dynamo.ITransactionsDao              `inject:""`
	Messenger            pachter.Messenger                    `inject:""`
	Converter            transactions.Converter               `inject:""`
	MiniExperimentClient miniexperiments.MiniExperimentClient `inject:""`
}

func (s *ServerImpl) GrantPinataBonusBits(ctx context.Context, req *paydayrpc.GrantPinataBonusBitsReq) (*paydayrpc.GrantPinataBonusBitsResp, error) {
	// validate
	if req == nil {
		return nil, twirp.InvalidArgumentError("body", "cannot be empty")
	}
	if len(req.GetTransactionId()) == 0 {
		return nil, twirp.InvalidArgumentError("transaction_id", "is required")
	}
	if len(req.GetChannelId()) == 0 {
		return nil, twirp.InvalidArgumentError("channel_id", "is required")
	}
	if req.GetPinataBonus() <= 0 {
		return nil, twirp.InvalidArgumentError("pinata_bonus", "is required")
	}

	pinataExperimentConfig, ok := miniexperiments.GetExperiments()[experimentName]
	if !ok {
		return nil, twirp.InternalError("BitsPinata experiment no longer active")
	}

	treatment, err := s.MiniExperimentClient.GetTreatment(
		pinataExperimentConfig,
		req.GetChannelId())
	if err != nil {
		log.WithError(err).Error("Failed to check experiment status")
		return nil, twirp.InternalError("failed to check if this experiment is still active")
	}
	if treatment != experimentVariant {
		return nil, twirp.InvalidArgumentError("channel_id", "is not part of the experiment")
	}

	dynamoTransaction := dynamo.Transaction{
		TransactionId:          req.GetTransactionId(),
		ChannelId:              pointers.StringP(req.GetChannelId()),
		BitsType:               pointers.StringP(string(models.PinataBits)),
		TransactionType:        pointers.StringP(models.BitsEventTransactionType),
		NumberOfBits:           pointers.Int64P(req.PinataBonus),
		TotalBitsToBroadcaster: pointers.Int64P(req.PinataBonus),
		EmoteTotals:            map[string]int64{"cheer": req.PinataBonus},
		TimeOfEvent:            time.Now(),
		LastUpdated:            time.Now(),
	}

	// write a transaction
	err = s.TransactionDAO.CreateNew(&dynamoTransaction)
	if err != nil {
		log.WithError(err).Error("Failed to record transaction for pinata")
		return nil, twirp.InternalError("failed to record transaction for pinata")
	}

	// trigger pachter SQS
	convTrans, err := s.Converter.ConvertFromTransaction(&dynamoTransaction, false)
	if err != nil {
		log.WithError(err).Error("Failed to convert transaction for Pachter processing")
		return nil, twirp.InternalError("failed to convert transaction for Pachter processing")
	}

	err = s.Messenger.SendTransactionToPachter(ctx, convTrans)
	if err != nil {
		log.WithError(err).Error("Failed to send transaction to Pachter for processing")
		return nil, twirp.InternalError("failed to send transaction to Pachter for processing")
	}

	return &paydayrpc.GrantPinataBonusBitsResp{}, nil
}
