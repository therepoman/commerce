package set_badge_tier_notification_state

import (
	"context"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/badgetiers/models"
	tiernotifications "code.justin.tv/commerce/payday/badgetiers/products/notifications"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"github.com/twitchtv/twirp"
)

type Server interface {
	SetBadgeTierNotificationStateImpl(ctx context.Context, req *paydayrpc.SetBadgeTierNotificationStateReq) (*paydayrpc.SetBadgeTierNotificationStateResp, error)
}

type ServerImpl struct {
	Setter tiernotifications.Setter `inject:""`
}

func (s *ServerImpl) SetBadgeTierNotificationStateImpl(ctx context.Context, req *paydayrpc.SetBadgeTierNotificationStateReq) (*paydayrpc.SetBadgeTierNotificationStateResp, error) {
	if req == nil {
		return nil, twirp.InternalError("cannot have empty request")
	}

	if req.UserId == "" {
		return nil, twirp.InvalidArgumentError("UserId", "cannot have empty user id")
	}

	if req.NotificationId == "" {
		return nil, twirp.InvalidArgumentError("NotificationId", "cannot have empty notification id")
	}

	if req.NotificationState == "" {
		return nil, twirp.InvalidArgumentError("NotificationState", "cannot have empty notification state")
	}

	notificationState := models.BadgeTierNotificationStatus(req.NotificationState)

	if notificationState != models.Shared && notificationState != models.Skipped {
		return nil, twirp.InvalidArgumentError("NotificationState", "can only pass shared or skipped state")
	}

	err := s.Setter.SetNotificationState(ctx, req.NotificationId, notificationState, req.Message, req.UserId)

	if err != nil {
		log.WithError(err).WithFields(log.Fields{
			"notificationID":    req.NotificationId,
			"notificationState": req.NotificationState,
		}).Error("could not set notification state")

		if _, isTwirpError := err.(twirp.Error); isTwirpError {
			return nil, err
		}
		return nil, twirp.InternalErrorWith(err)
	}

	return &paydayrpc.SetBadgeTierNotificationStateResp{}, nil
}
