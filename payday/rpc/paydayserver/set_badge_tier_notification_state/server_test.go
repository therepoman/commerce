package set_badge_tier_notification_state

import (
	"context"
	"testing"

	"github.com/stretchr/testify/mock"

	"code.justin.tv/commerce/payday/badgetiers/models"
	"code.justin.tv/commerce/payday/errors"
	tiernotifications_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/badgetiers/products/notifications"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	. "github.com/smartystreets/goconvey/convey"
)

func TestServer_SetBadgeTierNotificationStateImpl(t *testing.T) {
	Convey("given a SetBadgeTierNotificationStateImpl API", t, func() {
		setter := new(tiernotifications_mock.Setter)

		api := &ServerImpl{
			Setter: setter,
		}

		notificationID := "some-fake-id"
		notificationState := "SKIPPED"
		message := "hi"
		userID := "1234"

		Convey("when the request is nil", func() {
			var req *paydayrpc.SetBadgeTierNotificationStateReq
			_, err := api.SetBadgeTierNotificationStateImpl(context.Background(), req)

			Convey("we should return an error", func() {
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when the user_id is empty", func() {
			req := &paydayrpc.SetBadgeTierNotificationStateReq{
				NotificationState: notificationState,
				NotificationId:    notificationID,
			}
			_, err := api.SetBadgeTierNotificationStateImpl(context.Background(), req)

			Convey("we should return an error", func() {
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when the notification_id is empty", func() {
			req := &paydayrpc.SetBadgeTierNotificationStateReq{
				NotificationState: notificationState,
				UserId:            userID,
			}
			_, err := api.SetBadgeTierNotificationStateImpl(context.Background(), req)

			Convey("we should return an error", func() {
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when the notification state is empty", func() {
			req := &paydayrpc.SetBadgeTierNotificationStateReq{
				NotificationId: notificationID,
				UserId:         userID,
			}
			_, err := api.SetBadgeTierNotificationStateImpl(context.Background(), req)

			Convey("we should return an error", func() {
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when the notification state is something other than skipped or shared", func() {
			req := &paydayrpc.SetBadgeTierNotificationStateReq{
				NotificationState: "SHOW",
				NotificationId:    notificationID,
				UserId:            userID,
			}
			_, err := api.SetBadgeTierNotificationStateImpl(context.Background(), req)

			Convey("we should return an error", func() {
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when all required fields are set and valid", func() {
			req := &paydayrpc.SetBadgeTierNotificationStateReq{
				NotificationId:    notificationID,
				NotificationState: notificationState,
				Message:           "hi",
				UserId:            userID,
			}

			Convey("when setter returns an error", func() {
				setter.On("SetNotificationState", mock.Anything, notificationID, models.BadgeTierNotificationStatus(notificationState), message, userID).Return(errors.New("ERROR"))
				_, err := api.SetBadgeTierNotificationStateImpl(context.Background(), req)

				Convey("we should return an error", func() {
					So(err, ShouldNotBeNil)
				})
			})

			Convey("when the setter does not return an error", func() {
				setter.On("SetNotificationState", mock.Anything, notificationID, models.BadgeTierNotificationStatus(notificationState), message, userID).Return(nil)

				returnValue, err := api.SetBadgeTierNotificationStateImpl(context.Background(), req)

				Convey("we should return the tier notification response", func() {
					So(err, ShouldBeNil)
					So(*returnValue, ShouldResemble, paydayrpc.SetBadgeTierNotificationStateResp{})
				})
			})
		})
	})
}
