package check_bits_products_eligibility

import (
	"context"

	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/gogogadget/strings"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/products"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"github.com/twitchtv/twirp"
)

type Server interface {
	Check(ctx context.Context, req *paydayrpc.CheckBitsProductsEligibilityReq) (*paydayrpc.CheckBitsProductsEligibilityResp, error)
}

type server struct {
	Validator products.Validator `inject:""`
	Getter    products.Getter    `inject:""`
}

func NewServer() Server {
	return &server{}
}

func (s *server) Check(ctx context.Context, req *paydayrpc.CheckBitsProductsEligibilityReq) (*paydayrpc.CheckBitsProductsEligibilityResp, error) {
	if req == nil {
		return nil, twirp.InvalidArgumentError("request", "must not be nil")
	}

	if strings.Blank(req.UserId) {
		return nil, twirp.RequiredArgumentError("UserId")
	}

	if strings.Blank(req.ProductId) {
		return nil, twirp.RequiredArgumentError("ProductId")
	}

	if req.Quantity < 1 {
		return nil, twirp.RequiredArgumentError("Quantity")
	}

	if strings.Blank(req.CountryCode) {
		return nil, twirp.RequiredArgumentError("CountryCode")
	}

	if strings.Blank(req.Locale) {
		return nil, twirp.RequiredArgumentError("Locale")
	}

	if strings.Blank(req.Platform) {
		return nil, twirp.RequiredArgumentError("Platform")
	}

	platform := models.ParsePlatform(req.Platform, pointers.StringP("CheckBitsProductsEligibility_Check"), nil, nil)

	err, returnEmpty := s.Validator.Validate(ctx, req.UserId, req.Locale, req.CountryCode, platform)
	if err != nil {
		return nil, twirp.InternalErrorWith(err)
	} else if returnEmpty {
		return &paydayrpc.CheckBitsProductsEligibilityResp{IsEligible: false}, nil
	}

	productRecords, err := s.Getter.Get(ctx, req.UserId, req.Locale, platform)
	if err != nil {
		log.WithError(err).WithField("platform", req.Platform).Error("Error getting products")
		return nil, twirp.InternalErrorWith(err)
	}

	for _, productRecord := range productRecords {
		if req.ProductId == productRecord.Id {
			if req.Quantity > productRecord.MaxQuantity {
				return &paydayrpc.CheckBitsProductsEligibilityResp{IsEligible: false}, nil
			}
			return &paydayrpc.CheckBitsProductsEligibilityResp{IsEligible: true}, nil
		}
	}

	return &paydayrpc.CheckBitsProductsEligibilityResp{IsEligible: false}, nil
}
