package check_bits_products_eligibility

import (
	"context"
	"testing"

	products_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/products"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/models/api"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestServer_Check(t *testing.T) {
	Convey("Given a check bits products eligibility server", t, func() {
		validator := new(products_mock.Validator)
		getter := new(products_mock.Getter)

		server := &server{
			Validator: validator,
			Getter:    getter,
		}

		ctx := context.Background()

		userID := "123123123"
		productID := "walrusProduct"
		quantity := int32(3)
		cor := "US"
		locale := "en"
		platform := models.AMAZON

		Convey("Given an empty request", func() {
			Convey("Should return invalid argument error", func() {
				_, err := server.Check(ctx, nil)

				So(err, ShouldNotBeNil)
			})
		})

		Convey("Given an empty user ID", func() {
			req := &paydayrpc.CheckBitsProductsEligibilityReq{}

			Convey("Should return invalid argument error", func() {
				_, err := server.Check(ctx, req)

				So(err, ShouldNotBeNil)
			})
		})

		Convey("Given an empty product ID", func() {
			req := &paydayrpc.CheckBitsProductsEligibilityReq{
				UserId: userID,
			}

			Convey("Should return invalid argument error", func() {
				_, err := server.Check(ctx, req)

				So(err, ShouldNotBeNil)
			})
		})

		Convey("Given an empty quantity", func() {
			req := &paydayrpc.CheckBitsProductsEligibilityReq{
				UserId:    userID,
				ProductId: productID,
			}
			Convey("Should return invalid argument error", func() {
				_, err := server.Check(ctx, req)

				So(err, ShouldNotBeNil)
			})
		})

		Convey("Given an empty country code", func() {
			req := &paydayrpc.CheckBitsProductsEligibilityReq{
				UserId:    userID,
				ProductId: productID,
				Quantity:  quantity,
			}

			Convey("Should return invalid argument error", func() {
				_, err := server.Check(ctx, req)

				So(err, ShouldNotBeNil)
			})
		})

		Convey("Given an empty locale", func() {
			req := &paydayrpc.CheckBitsProductsEligibilityReq{
				UserId:      userID,
				ProductId:   productID,
				Quantity:    quantity,
				CountryCode: cor,
			}

			Convey("Should return invalid argument error", func() {
				_, err := server.Check(ctx, req)

				So(err, ShouldNotBeNil)
			})
		})

		Convey("Given an empty platform", func() {
			req := &paydayrpc.CheckBitsProductsEligibilityReq{
				UserId:      userID,
				ProductId:   productID,
				Quantity:    quantity,
				CountryCode: cor,
				Locale:      locale,
			}

			Convey("Should return invalid argument error", func() {
				_, err := server.Check(ctx, req)

				So(err, ShouldNotBeNil)
			})
		})

		Convey("Given an valid request", func() {
			req := &paydayrpc.CheckBitsProductsEligibilityReq{
				UserId:      userID,
				ProductId:   productID,
				Quantity:    quantity,
				CountryCode: cor,
				Locale:      locale,
				Platform:    string(platform),
			}

			Convey("When the validator returns an errors", func() {
				validator.On("Validate", mock.Anything, userID, locale, cor, platform).Return(errors.New("WALRUS STRIKE"), false)

				Convey("Then we should return an error", func() {
					_, err := server.Check(ctx, req)

					So(err, ShouldNotBeNil)
				})
			})

			Convey("When the validator returns that it should return empty", func() {
				validator.On("Validate", mock.Anything, userID, locale, cor, platform).Return(nil, true)

				Convey("Then we should return that the product is not eligible", func() {
					resp, err := server.Check(ctx, req)

					So(err, ShouldBeNil)
					So(resp, ShouldNotBeNil)
					So(resp.IsEligible, ShouldBeFalse)
				})
			})

			Convey("When the validator says we have a valid non empty response to provide", func() {
				validator.On("Validate", mock.Anything, userID, locale, cor, platform).Return(nil, false)

				Convey("When the getter returns an error", func() {
					getter.On("Get", mock.Anything, userID, locale, platform).Return(nil, errors.New("WALRUS STRIKE"))

					Convey("Then we should return an error", func() {
						_, err := server.Check(ctx, req)

						So(err, ShouldNotBeNil)
					})
				})

				Convey("When the getter returns a list of products", func() {
					product := api.Product{
						Id:          "walrus",
						MaxQuantity: 3,
					}

					getter.On("Get", mock.Anything, userID, locale, platform).Return([]api.Product{product}, nil)

					Convey("When the product is not in the list", func() {
						productID = "not-walrus"
						req.ProductId = productID

						Convey("Should return that the user is not eligible", func() {
							resp, err := server.Check(ctx, req)

							So(err, ShouldBeNil)
							So(resp, ShouldNotBeNil)
							So(resp.IsEligible, ShouldBeFalse)
						})
					})

					Convey("When the product is in the list", func() {
						productID = "walrus"
						req.ProductId = productID

						Convey("When the requested quantity is greater than the allowed max", func() {
							req.Quantity = int32(5)

							Convey("Should return that the user is not eligible", func() {
								resp, err := server.Check(ctx, req)

								So(err, ShouldBeNil)
								So(resp, ShouldNotBeNil)
								So(resp.IsEligible, ShouldBeFalse)
							})
						})

						Convey("When the requested quantity is less than or equal to the allow max", func() {
							req.Quantity = quantity

							Convey("Should return that the user is eligible", func() {
								resp, err := server.Check(ctx, req)

								So(err, ShouldBeNil)
								So(resp, ShouldNotBeNil)
								So(resp.IsEligible, ShouldBeTrue)
							})
						})
					})
				})
			})
		})
	})
}
