package handle_fulfillment

import (
	"context"
	"fmt"
	"sort"
	"strconv"
	"strings"

	"code.justin.tv/commerce/gogogadget/audit"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/clients/cloudwatchlogger"
	"code.justin.tv/commerce/payday/entitle"
	"code.justin.tv/commerce/payday/metrics"
	"code.justin.tv/commerce/payday/models/api"
	"code.justin.tv/commerce/payday/products"
	petozi "code.justin.tv/commerce/petozi/rpc"
	mulantwirp "code.justin.tv/revenue/mulan/rpc"
	offerstenantrpc "code.justin.tv/revenue/offer-tenant-schema/rpc"
	"github.com/twitchtv/twirp"
)

type Server interface {
	Handle(ctx context.Context, request *offerstenantrpc.FulfillmentRequest) (*offerstenantrpc.FulfillmentResponse, error)
}

type ServerImpl struct {
	Entitler        entitle.Parallel                  `inject:"parallelEntitler"`
	ProductsFetcher products.Fetcher                  `inject:""`
	Mulan           mulantwirp.Mulan                  `inject:""`
	AuditLogger     cloudwatchlogger.CloudWatchLogger `inject:""`
	Statter         metrics.Statter                   `inject:""`
}

const recurlyPlatformGateway = "recurly"
const xsollaPlatformGatewayPrefix = "xsolla"
const iosPlatformGateway = "apple_iap"
const androidPlatformGateway = "google_iab"

var recurlyGatewayMap = map[string]petozi.Platform{
	"vantiv":       petozi.Platform_VANTIV,
	"stripe":       petozi.Platform_STRIPE,
	"amazon":       petozi.Platform_AMAZON,
	"braintree":    petozi.Platform_PAYPAL,
	"wallet":       petozi.Platform_WALLET,
	"adyen":        petozi.Platform_ADYEN,
	"adyen_lux":    petozi.Platform_ADYEN_LUX,
	"worldpay":     petozi.Platform_WORLDPAY,
	"worldpay_lux": petozi.Platform_WORLDPAY_LUX,
}

func (s *ServerImpl) Handle(ctx context.Context, request *offerstenantrpc.FulfillmentRequest) (*offerstenantrpc.FulfillmentResponse, error) {
	if request == nil {
		return nil, twirp.InvalidArgumentError("request", "cannot be nil")
	}

	apiName := "HandleFulfillment"
	msg, err := audit.CreateAccessLogMsg(ctx, apiName, *request)
	if err != nil {
		log.WithError(err).Error(fmt.Sprintf("failed to generate access log msg in %s", apiName))
	} else {
		err = s.AuditLogger.Send(ctx, msg)
		if err != nil {
			log.WithError(err).Warn(fmt.Sprintf("error while sending audit log in %s", apiName))
		}
	}

	p, err := s.ProductsFetcher.FetchAllFromPetozi(ctx)
	if err != nil {
		log.WithError(err).Error("Error fetching Petozi catalog for Corleone request")
		return nil, twirp.InternalError("Error fetching Bits catalog")
	}

	product, productPlatformsAvailable := products.GetProductForOfferID(request.Offer.Id, p)

	if product == nil {
		log.WithError(err).Error("Error fetching Petozi catalog for Corleone request")
		return nil, twirp.InvalidArgumentError("Offer.Id", "Error: OfferId not found")
	}

	resp, err := s.Mulan.GetPurchaseOrderPaymentsByIDs(ctx, &mulantwirp.GetPurchaseOrderPaymentsByIDsRequest{
		Ids: []string{request.OriginId},
	})

	if err != nil {
		errMsg := fmt.Sprintf("Error fetching Mulan purchase order information for order: %s", request.OriginId)
		log.WithError(err).Errorf(errMsg)
		return nil, twirp.InternalError(errMsg)
	} else {
		payments := resp.PurchaseOrderPayments[request.OriginId]
		if payments != nil {
			if len(payments.PurchaseOrderPayments) > 0 {
				totalPaid := int64(0)
				// track the remaining amount of Bits left for split entitlements so that we don't miss any
				remainingQuantity := product.Quantity

				var paymentSlice []mulantwirp.PurchaseOrderPayment
				for _, payment := range payments.PurchaseOrderPayments {
					totalPaid += payment.GrossAmountUsd
					paymentSlice = append(paymentSlice, *payment)
				}

				sort.Slice(paymentSlice, func(i int, j int) bool {
					if paymentSlice[i].Gateway == paymentSlice[j].Gateway {
						if paymentSlice[i].PaymentGateway == paymentSlice[j].PaymentGateway {
							if paymentSlice[i].GrossAmountUsd == paymentSlice[j].GrossAmountUsd {
								return false
							} else {
								return paymentSlice[i].GrossAmountUsd < paymentSlice[j].GrossAmountUsd
							}
						} else {
							return paymentSlice[i].PaymentGateway < paymentSlice[j].PaymentGateway
						}
					}
					return paymentSlice[i].Gateway < paymentSlice[j].Gateway
				})

				for i, payment := range paymentSlice {
					platform := s.determinePetoziPlatformFromMulanInfo(payment.Gateway, payment.PaymentGateway, request.OwnerId, request.OriginId)

					petoziPlatform := petozi.Platform_value[platform]

					isAvailable := productPlatformsAvailable[petozi.Platform(petoziPlatform)]

					if !isAvailable {
						log.Errorf("Bits Product [%s] not available for requested platform [%s], defaulting to UNKNOWN", product.Id, platform)
						platform = petozi.Platform_name[int32(petozi.Platform_UNKNOWN)]
						go s.Statter.Inc("Entitle_Available_Bits_Product_Not_Configured_For_Platform", 1)
					} else {
						go s.Statter.Inc("Entitle_Available_Bits_Product_Not_Configured_For_Platform", 0)
					}

					entitleRequest := api.EntitleBitsRequest{
						TwitchUserId:    request.OwnerId,
						Platform:        platform,
						ProductQuantity: request.Quantity,
						ProductId:       product.Id,
						// As part of the Andes requirements we are bound to reporting all purchases in USD.
						// Payments reports to us the purchase in "Currency" "GrossAmount" and "GrossAmountUsd" fields
						// So just take the "GrossAmountUsd" field and hardcode the currency.
						Currency: "USD",
						Price:    toPriceString(payment.GrossAmountUsd),
						//TrackingId: request.TenantTracking["i dont know the key so comment this out"],
					}
					//Check to see if this tx should be split into multiple Bit type parts.
					splitTenderTransaction := len(paymentSlice) > 1
					if splitTenderTransaction {
						var quantity uint64
						quantity, remainingQuantity = calculateSplitAmounts(i, len(paymentSlice), totalPaid, payment.GrossAmountUsd, product.Quantity, remainingQuantity)

						entitleRequest.OriginOrderId = request.OriginId
						// for the transactionID based on what platform is being used
						// (assumption here is that each payment has it's own transaction,
						// however use i to ensure idempotence in the transactionID in case the same platform occurs twice
						entitleRequest.TransactionId = fmt.Sprintf("%s:SPLIT:%s:%d", request.OriginId, platform, i)
						entitleRequest.BitAmount = int32(quantity)
						entitleRequest.OverrideProductBitsAmount = true
						if quantity == 0 {
							log.Errorf("Quantity was Zero for attempted split entitlement transaction: %s", entitleRequest.TransactionId)
							continue
						}
					} else {
						// if this is a single request just use the unchanged origin id as the transactionid
						entitleRequest.TransactionId = request.OriginId
					}

					_, err = s.Entitler.Entitle(ctx, entitleRequest)

					if err != nil {
						log.WithError(err).Error("Error entitling Bits from Corleone request")
						return nil, twirp.InternalError("Error entitling Bits")
					}
				}
			} else {
				errMsg := fmt.Sprintf("Error getting purchase order information from Mulan -- no purchase information returned for originId: %s", request.OriginId)
				log.WithError(err).Errorf(errMsg)
				return nil, twirp.InternalError(errMsg)
			}
		}
	}

	response := offerstenantrpc.FulfillmentResponse{
		FulfillmentId: request.OriginId,
	}

	return &response, nil
}

// calculateSplitAmounts takes slice that is being looped through to calculate the split.
// It makes assumptions that the first time it is called that productQuantity and remainingQuantity should be equal
// It calculates the given split as a fraction of grossAmountPaid (for the item in the slice) divided by totalPaid amount
// It tracks the remaining quantity so that when it gets to the last index, it dumps the remaining amount to ensure
// all bits are delivered. There are some awkward edge cases (imagine 101 splits for 100 Bits) but in any event the user
// will get all the bits required.
//
// currentSplitIndex: the index of the slice we're iterating through
// totalNumberOfSplits: the len(s) where s is the slice we're iterating through
// totalPaid: the total amount paid (assumes homogenous currency for all payments)
// grossAmountUsd: the portion paid for this index item
// productQuantity: the total number of Bits to be divided
// remainingQuantity: productQuantity minus the number of bits allocated before the current index. Typically part of the output from calculateSplitAmounts is passed back in as an input here.
//
// return values:
// quantity of bits to allocate for the given index
// remaining quantity of Bits in the entitlement to split
func calculateSplitAmounts(currentSplitIndex, totalNumberOfSplits int, totalPaid, grossAmountUsd int64, productQuantity, remainingQuantity uint64) (uint64, uint64) {
	var quantity uint64
	// in order to ensure that we don't forget to entitle any amount of Bits, calculate the fraction until the
	// last remaining purchase goes through as expected. This solves for both a case where the payment.GrossAmountUsd array looks like {100, 100, 0, 0}
	// as well as ensures that all Bits are entitled as expected.
	if currentSplitIndex != totalNumberOfSplits-1 {
		quantity = uint64((float64(grossAmountUsd) / float64(totalPaid)) * float64(productQuantity))
		// in order to avoid floating errors not giving us the right amount, calculate the remaining quantity
		remainingQuantity -= quantity
	} else {
		quantity = remainingQuantity
		// ensure there isn't some case where we have repeated total paid == 0
		// and we entitle too many Bits
		remainingQuantity = 0
	}

	return quantity, remainingQuantity
}

func (s *ServerImpl) determinePetoziPlatformFromMulanInfo(gateway, paymentGateway, ownerId, originId string) string {
	var platform string
	// xsolla typically has their gateway come through as "xsolla_v3"
	// it's safe to assume if the gateway text includes xsolla,
	// then go ahead and mark the platform xsolla
	if strings.Contains(strings.ToLower(paymentGateway), xsollaPlatformGatewayPrefix) {
		platform = petozi.Platform_name[int32(petozi.Platform_XSOLLA)]
	} else if paymentGateway == recurlyPlatformGateway {
		petoziPlatform, found := recurlyGatewayMap[strings.ToLower(gateway)]
		// This case shouldn't happen but lets post an error message and assign the purchase to Amazon if we can't figure it out on purchase.
		if !found {
			log.Errorf("Error determining the given platform for a given Recurly Bits purchase, paymentGateway: [%s] gateway: [%s] userId: [%s] originId: [%s]",
				platform,
				gateway,
				ownerId,
				originId)
			platform = petozi.Platform_name[int32(petozi.Platform_UNKNOWN)]
			go s.Statter.Inc("Entitle_Unknown_Bits_Enttlement_Recurley_Gateway_Error", 1)
		} else {
			platform = petozi.Platform_name[int32(petoziPlatform)]
		}
	} else if paymentGateway == iosPlatformGateway {
		platform = petozi.Platform_name[int32(petozi.Platform_IOS)]
	} else if paymentGateway == androidPlatformGateway {
		platform = petozi.Platform_name[int32(petozi.Platform_ANDROID)]
	} else {
		platform = petozi.Platform_name[int32(petozi.Platform_UNKNOWN)]
		log.Errorf("Error determining the gateway for a given Bits purchase, paymentGateway: [%s] gateway: [%s] userId: [%s] originId: [%s]",
			platform,
			gateway,
			ownerId,
			originId)
		go s.Statter.Inc("Entitle_Unknown_Bits_Enttlement_Platform_Error", 1)
	}
	// Rare metrics won't produce a graph so hit cloudwatch with 0 to register for alarms.
	go s.Statter.Inc("Entitle_Unknown_Bits_Enttlement_Recurley_Gateway_Error", 0)
	go s.Statter.Inc("Entitle_Unknown_Bits_Enttlement_Platform_Error", 0)
	return platform
}

func toPriceString(grossAmountUSD int64) string {
	dollarPart := grossAmountUSD / 100
	centPart := grossAmountUSD % 100
	return strconv.Itoa(int(dollarPart)) + "." + strconv.Itoa(int(centPart))
}
