package handle_fulfillment

import (
	"context"
	"errors"
	"fmt"
	"math/rand"
	"strings"
	"testing"
	"time"

	"code.justin.tv/commerce/payday/models"

	"code.justin.tv/amzn/CorleoneTwirp"
	"code.justin.tv/commerce/payday/clients/cloudwatchlogger"
	entitle_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/entitle"
	metrics_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/metrics"
	products_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/products"
	mulan_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/revenue/mulan/rpc"
	petozi "code.justin.tv/commerce/petozi/rpc"
	mulantwirp "code.justin.tv/revenue/mulan/rpc"
	offerstenantrpc "code.justin.tv/revenue/offer-tenant-schema/rpc"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestServerImpl_Get(t *testing.T) {
	Convey("Given Admin Get All GemNotification", t, func() {
		mockProductsFetcher := new(products_mock.Fetcher)
		mockEntitler := new(entitle_mock.Parallel)
		mockMulan := new(mulan_mock.Mulan)
		cwlogger := cloudwatchlogger.NewCloudWatchLogNoopClient()
		statter := new(metrics_mock.Statter)

		ctx := context.Background()

		server := ServerImpl{
			ProductsFetcher: mockProductsFetcher,
			Entitler:        mockEntitler,
			Mulan:           mockMulan,
			AuditLogger:     cwlogger,
			Statter:         statter,
		}

		Convey("when request is nil", func() {
			resp, err := server.Handle(ctx, nil)
			So(resp, ShouldBeNil)
			So(err.Error(), ShouldEqual, "twirp error invalid_argument: request cannot be nil")
		})

		Convey("when fetcher fails", func() {
			mockProductsFetcher.On("FetchAllFromPetozi", ctx).Return(nil, errors.New("petozi failed")).Once()

			resp, err := server.Handle(ctx, &offerstenantrpc.FulfillmentRequest{})
			So(resp, ShouldBeNil)
			So(err.Error(), ShouldEqual, "twirp error internal: Error fetching Bits catalog")
		})

		offerID := "offer"
		originID := "origin"
		Convey("when fetcher doesn't fail but mulan does -- this should fail the entitlement", func() {
			var petoziList []*petozi.BitsProduct
			petoziList = append(petoziList, &petozi.BitsProduct{OfferId: offerID})
			mockProductsFetcher.On("FetchAllFromPetozi", ctx).Return(petoziList, nil).Once()

			mockMulan.On("GetPurchaseOrderPaymentsByIDs", ctx,
				&mulantwirp.GetPurchaseOrderPaymentsByIDsRequest{Ids: []string{originID}}).Return(nil, errors.New("error"))

			offer := &CorleoneTwirp.Offer{
				Id: offerID,
			}

			resp, err := server.Handle(ctx, &offerstenantrpc.FulfillmentRequest{Offer: offer, OriginId: originID})
			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("happy path for xsolla", func() {
			var petoziList []*petozi.BitsProduct
			petoziList = append(petoziList, &petozi.BitsProduct{OfferId: offerID, Quantity: 100, Platform: petozi.Platform_XSOLLA})
			mockProductsFetcher.On("FetchAllFromPetozi", ctx).Return(petoziList, nil).Once()

			purchaseOrderPaymentsMap := make(map[string]*mulantwirp.PurchaseOrderPayments)
			purchaseOrderPayments := mulantwirp.PurchaseOrderPayments{}
			var purchaseOrderPaymentsList []*mulantwirp.PurchaseOrderPayment
			purchaseOrderPaymentsList = append(purchaseOrderPaymentsList, &mulantwirp.PurchaseOrderPayment{GrossAmountUsd: 100, PaymentGateway: "xsolla_v3"})
			purchaseOrderPayments.PurchaseOrderPayments = purchaseOrderPaymentsList
			purchaseOrderPaymentsMap[originID] = &purchaseOrderPayments

			statter.On("Inc", "Entitle_Unknown_Bits_Enttlement_Recurley_Gateway_Error", int64(0)).Return()
			statter.On("Inc", "Entitle_Unknown_Bits_Enttlement_Platform_Error", int64(0)).Return()
			statter.On("Inc", "Entitle_Available_Bits_Product_Not_Configured_For_Platform", int64(0)).Return()

			mulanResponse := &mulantwirp.GetPurchaseOrderPaymentsByIDsResponse{
				PurchaseOrderPayments: purchaseOrderPaymentsMap,
			}
			mockMulan.On("GetPurchaseOrderPaymentsByIDs", ctx,
				&mulantwirp.GetPurchaseOrderPaymentsByIDsRequest{Ids: []string{originID}}).Return(mulanResponse, nil)

			offer := &CorleoneTwirp.Offer{
				Id: offerID,
			}

			mockEntitler.On("Entitle", ctx, mock.Anything).Return(int32(0), nil)
			resp, err := server.Handle(ctx, &offerstenantrpc.FulfillmentRequest{Offer: offer, OriginId: originID})
			// let the statter goroutines catch up
			time.Sleep(50 * time.Millisecond)
			So(resp, ShouldNotBeNil)
			So(resp.FulfillmentId, ShouldEqual, originID)
			So(err, ShouldBeNil)
			statter.AssertCalled(t, "Inc", "Entitle_Unknown_Bits_Enttlement_Recurley_Gateway_Error", int64(0))
			statter.AssertCalled(t, "Inc", "Entitle_Unknown_Bits_Enttlement_Platform_Error", int64(0))
			statter.AssertCalled(t, "Inc", "Entitle_Available_Bits_Product_Not_Configured_For_Platform", int64(0))
			statter.AssertNumberOfCalls(t, "Inc", 3)
		})

		// recurly (non-paypal) platforms
		for platform := range petozi.Platform_value {
			platform := platform
			switch platform {
			case "UNKNOWN", "NONE", "ANDROID", "IOS", "XSOLLA", "PAYPAL", "PHANTO":
				continue
			}
			Convey(fmt.Sprintf("happy path for recurly platorm %s", platform), func() {
				var petoziList []*petozi.BitsProduct
				petoziList = append(petoziList, &petozi.BitsProduct{OfferId: offerID, Quantity: 100, Platform: models.ParsePetoziPlatform(platform)})
				mockProductsFetcher.On("FetchAllFromPetozi", ctx).Return(petoziList, nil).Once()

				purchaseOrderPaymentsMap := make(map[string]*mulantwirp.PurchaseOrderPayments)
				purchaseOrderPayments := mulantwirp.PurchaseOrderPayments{}
				var purchaseOrderPaymentsList []*mulantwirp.PurchaseOrderPayment
				purchaseOrderPaymentsList = append(purchaseOrderPaymentsList, &mulantwirp.PurchaseOrderPayment{GrossAmountUsd: 100, PaymentGateway: "recurly", Gateway: strings.ToLower(platform)})
				purchaseOrderPayments.PurchaseOrderPayments = purchaseOrderPaymentsList
				purchaseOrderPaymentsMap[originID] = &purchaseOrderPayments

				statter.On("Inc", "Entitle_Unknown_Bits_Enttlement_Recurley_Gateway_Error", int64(0)).Return()
				statter.On("Inc", "Entitle_Unknown_Bits_Enttlement_Platform_Error", int64(0)).Return()
				statter.On("Inc", "Entitle_Available_Bits_Product_Not_Configured_For_Platform", int64(0)).Return()

				mulanResponse := &mulantwirp.GetPurchaseOrderPaymentsByIDsResponse{
					PurchaseOrderPayments: purchaseOrderPaymentsMap,
				}
				mockMulan.On("GetPurchaseOrderPaymentsByIDs", ctx,
					&mulantwirp.GetPurchaseOrderPaymentsByIDsRequest{Ids: []string{originID}}).Return(mulanResponse, nil)

				offer := &CorleoneTwirp.Offer{
					Id: offerID,
				}

				mockEntitler.On("Entitle", ctx, mock.Anything).Return(int32(0), nil)
				resp, err := server.Handle(ctx, &offerstenantrpc.FulfillmentRequest{Offer: offer, OriginId: originID})
				// let the statter goroutines catch up
				time.Sleep(50 * time.Millisecond)
				So(resp, ShouldNotBeNil)
				So(resp.FulfillmentId, ShouldEqual, originID)
				So(err, ShouldBeNil)
				statter.AssertCalled(t, "Inc", "Entitle_Unknown_Bits_Enttlement_Recurley_Gateway_Error", int64(0))
				statter.AssertCalled(t, "Inc", "Entitle_Unknown_Bits_Enttlement_Platform_Error", int64(0))
				statter.AssertCalled(t, "Inc", "Entitle_Available_Bits_Product_Not_Configured_For_Platform", int64(0))
				statter.AssertNumberOfCalls(t, "Inc", 3)
			})
		}

		Convey("no purchase gateway path", func() {
			var petoziList []*petozi.BitsProduct
			petoziList = append(petoziList, &petozi.BitsProduct{OfferId: offerID, Quantity: 100, Platform: petozi.Platform_XSOLLA})
			petoziList = append(petoziList, &petozi.BitsProduct{OfferId: offerID, Quantity: 100, Platform: petozi.Platform_UNKNOWN})
			mockProductsFetcher.On("FetchAllFromPetozi", ctx).Return(petoziList, nil).Once()

			purchaseOrderPaymentsMap := make(map[string]*mulantwirp.PurchaseOrderPayments)
			purchaseOrderPayments := mulantwirp.PurchaseOrderPayments{}
			var purchaseOrderPaymentsList []*mulantwirp.PurchaseOrderPayment
			purchaseOrderPaymentsList = append(purchaseOrderPaymentsList, &mulantwirp.PurchaseOrderPayment{GrossAmountUsd: 100})
			purchaseOrderPayments.PurchaseOrderPayments = purchaseOrderPaymentsList
			purchaseOrderPaymentsMap[originID] = &purchaseOrderPayments

			statter.On("Inc", "Entitle_Unknown_Bits_Enttlement_Recurley_Gateway_Error", int64(0)).Return()
			statter.On("Inc", "Entitle_Unknown_Bits_Enttlement_Platform_Error", int64(1)).Return()
			statter.On("Inc", "Entitle_Unknown_Bits_Enttlement_Platform_Error", int64(0)).Return()
			statter.On("Inc", "Entitle_Available_Bits_Product_Not_Configured_For_Platform", int64(0)).Return()

			mulanResponse := &mulantwirp.GetPurchaseOrderPaymentsByIDsResponse{
				PurchaseOrderPayments: purchaseOrderPaymentsMap,
			}
			mockMulan.On("GetPurchaseOrderPaymentsByIDs", ctx,
				&mulantwirp.GetPurchaseOrderPaymentsByIDsRequest{Ids: []string{originID}}).Return(mulanResponse, nil)

			offer := &CorleoneTwirp.Offer{
				Id: offerID,
			}

			mockEntitler.On("Entitle", ctx, mock.Anything).Return(int32(0), nil)
			resp, err := server.Handle(ctx, &offerstenantrpc.FulfillmentRequest{Offer: offer, OriginId: originID})
			// let the statter goroutines catch up
			time.Sleep(50 * time.Millisecond)
			So(resp, ShouldNotBeNil)
			So(resp.FulfillmentId, ShouldEqual, originID)
			So(err, ShouldBeNil)
			statter.AssertCalled(t, "Inc", "Entitle_Unknown_Bits_Enttlement_Recurley_Gateway_Error", int64(0))
			statter.AssertCalled(t, "Inc", "Entitle_Unknown_Bits_Enttlement_Platform_Error", int64(1))
			statter.AssertCalled(t, "Inc", "Entitle_Unknown_Bits_Enttlement_Platform_Error", int64(0))
			statter.AssertCalled(t, "Inc", "Entitle_Available_Bits_Product_Not_Configured_For_Platform", int64(0))
			statter.AssertNumberOfCalls(t, "Inc", 4)
		})

		Convey("purchase gateway path is for product that isn't configured for platform (Paypal)", func() {
			var petoziList []*petozi.BitsProduct
			petoziList = append(petoziList, &petozi.BitsProduct{OfferId: offerID, Quantity: 100, Platform: petozi.Platform_XSOLLA})
			petoziList = append(petoziList, &petozi.BitsProduct{OfferId: offerID, Quantity: 100, Platform: petozi.Platform_UNKNOWN})
			mockProductsFetcher.On("FetchAllFromPetozi", ctx).Return(petoziList, nil).Once()

			purchaseOrderPaymentsMap := make(map[string]*mulantwirp.PurchaseOrderPayments)
			purchaseOrderPayments := mulantwirp.PurchaseOrderPayments{}
			var purchaseOrderPaymentsList []*mulantwirp.PurchaseOrderPayment
			purchaseOrderPaymentsList = append(purchaseOrderPaymentsList, &mulantwirp.PurchaseOrderPayment{GrossAmountUsd: 100, PaymentGateway: "recurly"})
			purchaseOrderPayments.PurchaseOrderPayments = purchaseOrderPaymentsList
			purchaseOrderPaymentsMap[originID] = &purchaseOrderPayments

			statter.On("Inc", "Entitle_Unknown_Bits_Enttlement_Recurley_Gateway_Error", int64(1)).Return()
			statter.On("Inc", "Entitle_Unknown_Bits_Enttlement_Recurley_Gateway_Error", int64(0)).Return()
			statter.On("Inc", "Entitle_Unknown_Bits_Enttlement_Platform_Error", int64(0)).Return()
			statter.On("Inc", "Entitle_Available_Bits_Product_Not_Configured_For_Platform", int64(0)).Return()

			mulanResponse := &mulantwirp.GetPurchaseOrderPaymentsByIDsResponse{
				PurchaseOrderPayments: purchaseOrderPaymentsMap,
			}
			mockMulan.On("GetPurchaseOrderPaymentsByIDs", ctx,
				&mulantwirp.GetPurchaseOrderPaymentsByIDsRequest{Ids: []string{originID}}).Return(mulanResponse, nil)

			offer := &CorleoneTwirp.Offer{
				Id: offerID,
			}

			mockEntitler.On("Entitle", ctx, mock.Anything).Return(int32(0), nil)
			resp, err := server.Handle(ctx, &offerstenantrpc.FulfillmentRequest{Offer: offer, OriginId: originID})
			// let the statter goroutines catch up
			time.Sleep(50 * time.Millisecond)
			So(resp, ShouldNotBeNil)
			So(resp.FulfillmentId, ShouldEqual, originID)
			So(err, ShouldBeNil)
			statter.AssertCalled(t, "Inc", "Entitle_Unknown_Bits_Enttlement_Recurley_Gateway_Error", int64(1))
			statter.AssertCalled(t, "Inc", "Entitle_Unknown_Bits_Enttlement_Recurley_Gateway_Error", int64(0))
			statter.AssertCalled(t, "Inc", "Entitle_Unknown_Bits_Enttlement_Platform_Error", int64(0))
			statter.AssertCalled(t, "Inc", "Entitle_Available_Bits_Product_Not_Configured_For_Platform", int64(0))
			statter.AssertNumberOfCalls(t, "Inc", 4)
		})

		Convey("happy path part 2: the splitaroo", func() {
			var petoziList []*petozi.BitsProduct
			petoziList = append(petoziList, &petozi.BitsProduct{OfferId: offerID, Quantity: 100, Platform: petozi.Platform_XSOLLA})
			mockProductsFetcher.On("FetchAllFromPetozi", ctx).Return(petoziList, nil).Once()

			purchaseOrderPaymentsMap := make(map[string]*mulantwirp.PurchaseOrderPayments)
			purchaseOrderPayments := mulantwirp.PurchaseOrderPayments{}
			var purchaseOrderPaymentsList []*mulantwirp.PurchaseOrderPayment
			// as long as the split is guaranteed at least 1 bit per split, it should split any number of times, so test splitting it 100 for sanity.
			for i := 0; i < 100; i++ {
				purchaseOrderPaymentsList = append(purchaseOrderPaymentsList, &mulantwirp.PurchaseOrderPayment{GrossAmountUsd: 100, PaymentGateway: "xsolla_v2"})
			}

			purchaseOrderPayments.PurchaseOrderPayments = purchaseOrderPaymentsList
			purchaseOrderPaymentsMap[originID] = &purchaseOrderPayments

			statter.On("Inc", "Entitle_Unknown_Bits_Enttlement_Recurley_Gateway_Error", int64(0)).Return()
			statter.On("Inc", "Entitle_Unknown_Bits_Enttlement_Platform_Error", int64(0)).Return()
			statter.On("Inc", "Entitle_Available_Bits_Product_Not_Configured_For_Platform", int64(0)).Return()

			mulanResponse := &mulantwirp.GetPurchaseOrderPaymentsByIDsResponse{
				PurchaseOrderPayments: purchaseOrderPaymentsMap,
			}
			mockMulan.On("GetPurchaseOrderPaymentsByIDs", ctx,
				&mulantwirp.GetPurchaseOrderPaymentsByIDsRequest{Ids: []string{originID}}).Return(mulanResponse, nil)

			offer := &CorleoneTwirp.Offer{
				Id: offerID,
			}

			mockEntitler.On("Entitle", ctx, mock.Anything).Return(int32(0), nil).Times(100)

			resp, err := server.Handle(ctx, &offerstenantrpc.FulfillmentRequest{Offer: offer, OriginId: originID})
			So(resp, ShouldNotBeNil)
			So(resp.FulfillmentId, ShouldEqual, originID)
			So(err, ShouldBeNil)
			mockEntitler.AssertNumberOfCalls(t, "Entitle", 100)
		})

		Convey("testing split deeper", func() {
			for numberOfTests := 0; numberOfTests < 100; numberOfTests++ {
				totalPaid := int64(0)
				productQuantity := uint64(rand.Intn(25000) + 100)
				remainingQuantity := productQuantity
				randomNumberOfSplits := rand.Intn(4) + 1

				var amountPaidSlice []int64

				for i := 0; i < randomNumberOfSplits; i++ {
					randValue := int64(rand.Intn(50000))
					amountPaidSlice = append(amountPaidSlice, randValue)
					totalPaid += randValue
				}
				quantCalculated := uint64(0)
				for i, cost := range amountPaidSlice {
					quantity, newRemaining := calculateSplitAmounts(i, len(amountPaidSlice), totalPaid, cost, productQuantity, remainingQuantity)
					quantCalculated += quantity
					remainingQuantity = newRemaining
				}

				So(quantCalculated, ShouldEqual, productQuantity)
				So(remainingQuantity, ShouldEqual, uint64(0))
			}
		})

		Convey("testing split with evenly dividable static values", func() {
			totalPaid := int64(200)
			productQuantity := uint64(200)
			remainingQuantity := productQuantity

			var amountPaidSlice []int64
			amountPaidSlice = append(amountPaidSlice, int64(80))
			amountPaidSlice = append(amountPaidSlice, int64(120))

			quantCalculated := uint64(0)
			quantity, newRemaining := calculateSplitAmounts(0, len(amountPaidSlice), totalPaid, amountPaidSlice[0], productQuantity, remainingQuantity)
			quantCalculated += quantity

			So(quantity, ShouldEqual, uint64(80))
			So(newRemaining, ShouldEqual, uint64(120))

			quantity, newRemaining = calculateSplitAmounts(1, len(amountPaidSlice), totalPaid, amountPaidSlice[1], productQuantity, newRemaining)
			quantCalculated += quantity

			So(quantity, ShouldEqual, uint64(120))
			So(newRemaining, ShouldEqual, uint64(0))
			So(quantCalculated, ShouldEqual, uint64(200))
		})

		Convey("testing split with unevenly dividable static values", func() {
			totalPaid := int64(200)
			productQuantity := uint64(101)
			remainingQuantity := productQuantity

			var amountPaidSlice []int64
			amountPaidSlice = append(amountPaidSlice, int64(100))
			amountPaidSlice = append(amountPaidSlice, int64(100))

			quantCalculated := uint64(0)
			quantity, newRemaining := calculateSplitAmounts(0, len(amountPaidSlice), totalPaid, amountPaidSlice[0], productQuantity, remainingQuantity)
			quantCalculated += quantity

			So(quantity, ShouldEqual, uint64(50))
			So(newRemaining, ShouldEqual, uint64(51))

			quantity, newRemaining = calculateSplitAmounts(1, len(amountPaidSlice), totalPaid, amountPaidSlice[1], productQuantity, newRemaining)
			quantCalculated += quantity

			So(quantity, ShouldEqual, uint64(51))
			So(newRemaining, ShouldEqual, uint64(0))
			So(quantCalculated, ShouldEqual, uint64(101))
		})
	})
}
