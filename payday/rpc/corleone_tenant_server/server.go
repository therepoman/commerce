package corleone_tenant_server

import (
	"context"

	"code.justin.tv/commerce/payday/rpc/corleone_tenant_server/handle_fulfillment"
	"code.justin.tv/commerce/payday/rpc/corleone_tenant_server/is_eligible"
	offerstenantrpc "code.justin.tv/revenue/offer-tenant-schema/rpc"
	"github.com/twitchtv/twirp"
)

type server struct {
	IsEligibleServer        is_eligible.Server        `inject:""`
	HandleFulfillmentServer handle_fulfillment.Server `inject:""`
}

func NewServer() offerstenantrpc.OffersTenant {
	return &server{}
}

func (s *server) IsEligible(ctx context.Context, request *offerstenantrpc.IsEligibleRequest) (*offerstenantrpc.IsEligibleResponse, error) {

	if request == nil {
		return nil, twirp.InvalidArgumentError("request", "cannot be nil")
	}
	return s.IsEligibleServer.GetEligibility(ctx, request.OwnerId, request.Offer)
}

func (s *server) HandleFulfillment(ctx context.Context, request *offerstenantrpc.FulfillmentRequest) (*offerstenantrpc.FulfillmentResponse, error) {
	return s.HandleFulfillmentServer.Handle(ctx, request)
}

func (s *server) HandleRevocation(ctx context.Context, request *offerstenantrpc.RevocationRequest) (*offerstenantrpc.RevocationResponse, error) {
	return nil, twirp.NewError(twirp.Unimplemented, "Revocation not supported")
}
