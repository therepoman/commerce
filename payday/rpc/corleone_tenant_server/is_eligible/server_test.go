package is_eligible

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/amzn/CorleoneTwirp"
	products_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/products"
	"code.justin.tv/commerce/payday/models"
	petozi "code.justin.tv/commerce/petozi/rpc"
	. "github.com/smartystreets/goconvey/convey"
)

const userID = "userID"

func TestServerImpl_GetEligibility(t *testing.T) {
	Convey("Given Admin Get All GemNotification", t, func() {

		mockProductsFetcher := new(products_mock.Fetcher)
		ctx := context.Background()

		server := ServerImpl{
			ProductsFetcher: mockProductsFetcher,
		}

		Convey("when fetcher fails", func() {
			mockProductsFetcher.On("Fetch", ctx, userID).Return(nil, models.DEFAULT, errors.New("petozi failed")).Once()

			resp, err := server.GetEligibility(ctx, userID, []*CorleoneTwirp.Offer{})
			So(resp, ShouldBeNil)
			So(err.Error(), ShouldEqual, "twirp error internal: Issue getting products for user from Petozi")
		})

		Convey("when fetcher succeeds and finds product with no prefix", func() {

			response := make(map[string][]petozi.BitsProduct)
			var list []petozi.BitsProduct
			list = append(list, petozi.BitsProduct{OfferId: "offerID"})
			response["offerID"] = list

			mockProductsFetcher.On("Fetch", ctx, userID).Return(response, models.DEFAULT, nil).Once()

			var request []*CorleoneTwirp.Offer
			request = append(request, &CorleoneTwirp.Offer{Id: "offerID"})
			resp, err := server.GetEligibility(ctx, userID, request)
			So(resp, ShouldNotBeNil)
			So(err, ShouldBeNil)
			So(resp.Eligibility, ShouldNotBeNil)
			So(len(resp.Eligibility), ShouldEqual, 1)
			So(resp.Eligibility[0].IsEligible, ShouldBeTrue)
		})

		Convey("when fetcher succeeds and finds product with new prefix", func() {

			response := make(map[string][]petozi.BitsProduct)
			var list []petozi.BitsProduct
			list = append(list, petozi.BitsProduct{OfferId: "amzn1.twitch.commerce.offer.offerID"})
			response["offerID"] = list

			mockProductsFetcher.On("Fetch", ctx, userID).Return(response, models.DEFAULT, nil).Once()

			var request []*CorleoneTwirp.Offer
			request = append(request, &CorleoneTwirp.Offer{Id: "amzn1.twitch.commerce.offer.offerID"})
			resp, err := server.GetEligibility(ctx, userID, request)
			So(resp, ShouldNotBeNil)
			So(err, ShouldBeNil)
			So(resp.Eligibility, ShouldNotBeNil)
			So(len(resp.Eligibility), ShouldEqual, 1)
			So(resp.Eligibility[0].IsEligible, ShouldBeTrue)
			So(resp.Eligibility[0].OfferId, ShouldEqual, "amzn1.twitch.commerce.offer.offerID")
		})

	})
}
