package is_eligible

import (
	"context"

	"code.justin.tv/amzn/CorleoneTwirp"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/products"
	petozi "code.justin.tv/commerce/petozi/rpc"
	offerstenantrpc "code.justin.tv/revenue/offer-tenant-schema/rpc"
	"github.com/twitchtv/twirp"
)

type Server interface {
	GetEligibility(ctx context.Context, userId string, corleoneOffers []*CorleoneTwirp.Offer) (*offerstenantrpc.IsEligibleResponse, error)
}

type ServerImpl struct {
	ProductsFetcher products.Fetcher `inject:""`
}

func (s *ServerImpl) GetEligibility(ctx context.Context, userId string, corleoneOffers []*CorleoneTwirp.Offer) (*offerstenantrpc.IsEligibleResponse, error) {
	response := &offerstenantrpc.IsEligibleResponse{}

	availablePetoziProducts, _, err := s.ProductsFetcher.Fetch(ctx, userId)

	if err != nil {
		errMsg := "Issue getting products for user from Petozi"
		log.WithError(err).Error(errMsg)
		return nil, twirp.InternalError(errMsg)
	}

	// flatten the map into a list we can search for matches in
	var plist []petozi.BitsProduct
	for _, v := range availablePetoziProducts {
		plist = append(plist, v...)
	}

	foundProducts := GetProductForOfferIDFromSlice(corleoneOffers, plist)

	if foundProducts == nil {
		// shouldn't/wont happen given above but nil check should happen just in case
		return response, nil
	} else {
		var eligibilities []*offerstenantrpc.OfferEligibility
		for _, product := range corleoneOffers {
			p, found := foundProducts[product.Id]

			var eligibility *offerstenantrpc.OfferEligibility
			if found && p != nil {
				var shouldDisplay bool
				if len(userId) < 1 && !p.ShowWhenLoggedOut {
					shouldDisplay = false
				} else {
					shouldDisplay = true
				}
				eligibility = &offerstenantrpc.OfferEligibility{
					OfferId:             product.Id,
					IsEligible:          true,
					MaxQuantityOverride: int32(p.MaxPurchasableQuantity),
					NotEligibleReason:   "",
					ShouldDisplay:       shouldDisplay,
				}
			} else {
				eligibility = &offerstenantrpc.OfferEligibility{
					OfferId:             product.Id,
					IsEligible:          false,
					MaxQuantityOverride: 0,
					NotEligibleReason:   "Product unavailable",
					ShouldDisplay:       false,
				}
			}

			eligibilities = append(eligibilities, eligibility)
		}
		response.Eligibility = eligibilities
		return response, nil
	}
}

func GetProductForOfferIDFromSlice(offers []*CorleoneTwirp.Offer, bitsProducts []petozi.BitsProduct) map[string]*petozi.BitsProduct {
	// use a map to ensure we don't return more than one entry per offer id since multiple platforms might share offerids
	// (the information between them we use should be the same however)
	corleoneProducts := make(map[string]*petozi.BitsProduct)

	if len(offers) == 0 {
		return corleoneProducts
	}

	for _, offer := range offers {
		if offer == nil {
			continue
		}
		corleoneProducts[offer.Id] = nil
	}

	for _, product := range bitsProducts {
		_, inMap := corleoneProducts[product.OfferId]
		if inMap {
			pinnedProduct := product
			corleoneProducts[product.OfferId] = &pinnedProduct
		}
	}

	return corleoneProducts
}
