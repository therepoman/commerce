package test

import (
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/twitchtv/twirp"
)

func AssertTwirpErrorCode(err error, expectedCode twirp.ErrorCode) {
	So(err, ShouldNotBeNil)
	tErr, isTErr := err.(twirp.Error)
	So(isTErr, ShouldBeTrue)
	So(tErr.Code(), ShouldEqual, expectedCode)
}

func AssertTwirpCustomErrorType(err error, expectedCustomErrorType string) {
	var clientErr paydayrpc.ClientError
	pErr := paydayrpc.ParseClientError(err, &clientErr)
	So(pErr, ShouldBeNil)
	So(clientErr.ErrorCode, ShouldEqual, expectedCustomErrorType)
}
