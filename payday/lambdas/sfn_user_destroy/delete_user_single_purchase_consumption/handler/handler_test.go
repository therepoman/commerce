package handler

import (
	"context"
	"fmt"
	"testing"

	"code.justin.tv/commerce/payday/dynamo/single_purchase_consumption"
	single_purchase_consumption_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo/single_purchase_consumption"
	sfn_models "code.justin.tv/commerce/payday/sfn/models"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestLambdaHandler_Handle(t *testing.T) {
	Convey("given a delete_user_purchase_by_platform handler", t, func() {
		purchaseByPlatformDAO := new(single_purchase_consumption_mock.SinglePurchaseConsumptionDAO)
		handler := &LambdaHandler{
			SinglePurchaseConsumptionDAO: purchaseByPlatformDAO,
		}
		ctx := context.Background()
		userId := "test-pdms-user"
		productPrefix := "test-product"
		numOfRecords := 60
		numOfPages := 3

		testInput := sfn_models.UserDestroyInput{
			UserId: userId,
		}
		expectedOutput := &sfn_models.UserDestroyOutput{
			UserId: userId,
		}

		var testPurchaseRecords []single_purchase_consumption.SinglePurchaseConsumption
		for i := 1; i <= numOfRecords; i++ {
			testPurchaseRecords = append(testPurchaseRecords, single_purchase_consumption.SinglePurchaseConsumption{
				TwitchUserID: userId,
				ProductID:    fmt.Sprintf("%s-%d", productPrefix, i),
			})
		}

		Convey("throws an error when input is invalid", func() {
			input := sfn_models.UserDestroyInput{}
			output, err := handler.Handle(ctx, input)
			So(err, ShouldNotBeNil)
			So(output, ShouldBeNil)
		})

		Convey("given dryrun", func() {
			handler.Dryrun = true

			Convey("given no user found", func() {
				var pageKey *string
				purchaseByPlatformDAO.On("QueryWithLimit", ctx, userId, int64(queryLimit), pageKey).
					Return([]single_purchase_consumption.SinglePurchaseConsumption{}, nil, nil)
				output, err := handler.Handle(ctx, testInput)

				So(purchaseByPlatformDAO.AssertNumberOfCalls(t, "QueryWithLimit", 1), ShouldBeTrue)
				So(purchaseByPlatformDAO.AssertNumberOfCalls(t, "BatchDelete", 0), ShouldBeTrue)

				So(err, ShouldBeNil)
				So(output, ShouldResemble, expectedOutput)
			})

			Convey("given user to delete, pagination should work correctly and dryrun should not run Delete", func() {
				for i := 0; i <= numOfPages; i++ {
					pageSize := numOfRecords / numOfPages
					lowIndex := i * pageSize
					highIndex := lowIndex + pageSize

					var pageKey *string
					var newPageKey *string
					if i != 0 {
						pageKeyString := fmt.Sprintf("%s-%d", productPrefix, lowIndex)
						pageKey = &pageKeyString
					}
					if i != numOfPages {
						newPageKeyString := fmt.Sprintf("%s-%d", productPrefix, highIndex)
						newPageKey = &newPageKeyString
						purchaseByPlatformDAO.On("QueryWithLimit", ctx, userId, int64(queryLimit), pageKey).
							Return(testPurchaseRecords[lowIndex:highIndex], newPageKey, nil).Once()
					} else {
						purchaseByPlatformDAO.On("QueryWithLimit", ctx, userId, int64(queryLimit), pageKey).
							Return([]single_purchase_consumption.SinglePurchaseConsumption{}, newPageKey, nil).Once()
					}
				}
				output, err := handler.Handle(ctx, testInput)

				So(purchaseByPlatformDAO.AssertNumberOfCalls(t, "QueryWithLimit", 4), ShouldBeTrue)
				So(purchaseByPlatformDAO.AssertNumberOfCalls(t, "BatchDelete", 0), ShouldBeTrue)

				So(err, ShouldBeNil)
				So(output, ShouldResemble, expectedOutput)
			})
		})

		Convey("given real run", func() {
			handler.Dryrun = false

			Convey("given no user found", func() {
				var pageKey *string
				purchaseByPlatformDAO.On("QueryWithLimit", ctx, userId, int64(queryLimit), pageKey).
					Return([]single_purchase_consumption.SinglePurchaseConsumption{}, nil, nil)
				output, err := handler.Handle(ctx, testInput)

				So(purchaseByPlatformDAO.AssertNumberOfCalls(t, "QueryWithLimit", 1), ShouldBeTrue)
				So(purchaseByPlatformDAO.AssertNumberOfCalls(t, "BatchDelete", 0), ShouldBeTrue)

				So(err, ShouldBeNil)
				So(output, ShouldResemble, expectedOutput)
			})

			Convey("given user to delete, real run should run Delete", func() {
				for i := 0; i <= numOfPages; i++ {
					pageSize := numOfRecords / numOfPages
					lowIndex := i * pageSize
					highIndex := lowIndex + pageSize

					var pageKey *string
					var newPageKey *string
					if i != 0 {
						pageKeyString := fmt.Sprintf("%s-%d", productPrefix, lowIndex)
						pageKey = &pageKeyString
					}
					if i != numOfPages {
						newPageKeyString := fmt.Sprintf("%s-%d", productPrefix, highIndex)
						newPageKey = &newPageKeyString
						purchaseByPlatformDAO.On("QueryWithLimit", ctx, userId, int64(queryLimit), pageKey).
							Return(testPurchaseRecords[lowIndex:highIndex], newPageKey, nil).Once()
					} else {
						purchaseByPlatformDAO.On("QueryWithLimit", ctx, userId, int64(queryLimit), pageKey).
							Return([]single_purchase_consumption.SinglePurchaseConsumption{}, newPageKey, nil).Once()
					}
				}
				purchaseByPlatformDAO.On("BatchDelete", ctx, mock.Anything).Return(nil)
				output, err := handler.Handle(ctx, testInput)

				So(purchaseByPlatformDAO.AssertNumberOfCalls(t, "QueryWithLimit", 4), ShouldBeTrue)
				So(purchaseByPlatformDAO.AssertNumberOfCalls(t, "BatchDelete", 3), ShouldBeTrue)

				So(err, ShouldBeNil)
				So(output, ShouldResemble, expectedOutput)
			})
		})
	})
}
