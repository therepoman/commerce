package handler

import (
	"context"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/dynamo/single_purchase_consumption"
	"code.justin.tv/commerce/payday/lambdas/sfn_user_destroy/input_validator"
	sfnmodels "code.justin.tv/commerce/payday/sfn/models"
)

const (
	stepIdentifier      = "deleteUserSinglePurchaseConsumption"
	singlePurchaseTable = "single_purchase_consumptions"
	queryLimit          = 1000
)

type LambdaHandler struct {
	SinglePurchaseConsumptionDAO single_purchase_consumption.SinglePurchaseConsumptionDAO
	Dryrun                       bool
}

func (h *LambdaHandler) Handle(ctx context.Context, input sfnmodels.UserDestroyInput) (*sfnmodels.UserDestroyOutput, error) {
	log.Infof("Executing Step %v", stepIdentifier)

	err := input_validator.ValidateInput(input)
	if err != nil {
		return nil, err
	}

	err = h.handleDeleteUserSinglePurchaseConsumptions(ctx, input.UserId)
	if err != nil {
		log.WithError(err).Errorf("Error happened when trying to delete user entries from %s table", singlePurchaseTable)
		return nil, err
	}

	return &sfnmodels.UserDestroyOutput{
		UserId: input.UserId,
	}, nil
}

func (h *LambdaHandler) handleDeleteUserSinglePurchaseConsumptions(ctx context.Context, userID string) error {
	var pagingKey *string
	retry := true

	for retry {
		results, newPagingKey, err := h.SinglePurchaseConsumptionDAO.QueryWithLimit(ctx, userID, queryLimit, pagingKey)
		if err != nil {
			return err
		}

		if len(results) > 0 {
			log.Infof("Found %d entries for user %s in Dynamo Table %s", len(results), userID, singlePurchaseTable)

			if h.Dryrun {
				log.Infof("Dryrun, %d entries for user %s will not be deleted from Dynamo Table %s", len(results), userID, singlePurchaseTable)
			} else {
				log.Infof("Real Run! Deleting %d entries for user %s from Dynamo Table %s", len(results), userID, singlePurchaseTable)
				err := h.SinglePurchaseConsumptionDAO.BatchDelete(ctx, results)
				if err != nil {
					return err
				}
			}
		}
		if newPagingKey != nil {
			log.Infof("Got pagingKey: %s", *newPagingKey)
			pagingKey = newPagingKey
		} else {
			log.Info("No pagingKey found for the query")
		}

		retry = newPagingKey != nil
	}

	return nil
}
