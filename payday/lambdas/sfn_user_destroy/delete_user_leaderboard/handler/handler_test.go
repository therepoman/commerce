package handler

import (
	"context"
	"testing"

	pantheon_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/pantheon"
	sfn_models "code.justin.tv/commerce/payday/sfn/models"
	. "github.com/smartystreets/goconvey/convey"
)

func TestLambdaHandler_Handle(t *testing.T) {
	Convey("given a delete_user_leaderboard handler", t, func() {
		pantheonClient := new(pantheon_mock.IPantheonClientWrapper)
		handler := &LambdaHandler{
			PantheonClient: pantheonClient,
		}
		ctx := context.Background()
		userId := "test-pdms-user"
		testInput := sfn_models.UserDestroyInput{
			UserId: userId,
		}
		expectedOutput := &sfn_models.UserDestroyOutput{
			UserId: userId,
		}

		Convey("throws an error when input is invalid", func() {
			input := sfn_models.UserDestroyInput{}
			output, err := handler.Handle(ctx, input)
			So(err, ShouldNotBeNil)
			So(output, ShouldBeNil)
		})

		Convey("given dryrun", func() {
			handler.Dryrun = true
			output, err := handler.Handle(ctx, testInput)

			So(pantheonClient.AssertNumberOfCalls(t, "DeleteLeaderboards", 0), ShouldBeTrue)
			So(pantheonClient.AssertNumberOfCalls(t, "DeleteEntries", 0), ShouldBeTrue)
			So(err, ShouldBeNil)
			So(output, ShouldResemble, expectedOutput)
		})

		Convey("given real run", func() {
			handler.Dryrun = false
			pantheonClient.On("DeleteLeaderboards", ctx, userId).Return(nil)
			pantheonClient.On("DeleteEntries", ctx, userId).Return(nil)

			output, err := handler.Handle(ctx, testInput)

			So(pantheonClient.AssertNumberOfCalls(t, "DeleteLeaderboards", 1), ShouldBeTrue)
			So(pantheonClient.AssertNumberOfCalls(t, "DeleteEntries", 1), ShouldBeTrue)
			So(err, ShouldBeNil)
			So(output, ShouldResemble, expectedOutput)
		})
	})
}
