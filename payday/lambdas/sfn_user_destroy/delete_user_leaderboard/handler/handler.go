package handler

import (
	"context"
	"sync"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/clients/pantheon"
	"code.justin.tv/commerce/payday/lambdas/sfn_user_destroy/input_validator"
	sfnmodels "code.justin.tv/commerce/payday/sfn/models"
)

const (
	stepIdentifier = "deleteUserLeaderboard"
)

type LambdaHandler struct {
	PantheonClient pantheon.IPantheonClientWrapper
	Dryrun         bool
}

func (h *LambdaHandler) Handle(ctx context.Context, input sfnmodels.UserDestroyInput) (*sfnmodels.UserDestroyOutput, error) {
	log.Infof("Executing Step %s", stepIdentifier)

	err := input_validator.ValidateInput(input)
	if err != nil {
		return nil, err
	}

	fatalErrors := make(chan error)
	wgDone := make(chan bool)

	var wg sync.WaitGroup
	wg.Add(2)

	go func() {
		err := h.handleDeleteLeaderboard(ctx, input.UserId)
		if err != nil {
			log.WithError(err).Error("Error happened when trying to delete user leaderboard from Pantheon")
			fatalErrors <- err
		}
		wg.Done()
	}()
	go func() {
		err := h.handleDeleteLeaderboardEntries(ctx, input.UserId)
		if err != nil {
			log.WithError(err).Error("Error happened when trying to delete user entries in Pantheon")
			fatalErrors <- err
		}
		wg.Done()
	}()

	go func() {
		wg.Wait()
		close(wgDone)
	}()

	select {
	case err := <-fatalErrors:
		return nil, err
	case <-wgDone:
		break
	}

	return &sfnmodels.UserDestroyOutput{
		UserId: input.UserId,
	}, nil
}

func (h *LambdaHandler) handleDeleteLeaderboard(ctx context.Context, userID string) error {
	if h.Dryrun {
		log.WithField("UserID", userID).Info("Dryrun, user's leaderboard will not be deleted from Pantheon")
	} else {
		log.WithField("UserID", userID).Info("Real run! Requesting delete user's balance from Pantheon")
		err := h.PantheonClient.DeleteLeaderboards(ctx, userID)
		if err != nil {
			return err
		}
	}
	return nil
}

func (h *LambdaHandler) handleDeleteLeaderboardEntries(ctx context.Context, userID string) error {
	if h.Dryrun {
		log.WithField("UserID", userID).Info("Dryrun, user's data in all leaderboards will not be deleted from Pantheon")
	} else {
		log.WithField("UserID", userID).Info("Real run! Requesting delete user's data in all leaderboards from Pantheon")
		err := h.PantheonClient.DeleteEntries(ctx, userID)
		if err != nil {
			return err
		}
	}
	return nil
}
