package handler

import (
	"context"
	"fmt"
	"testing"

	"github.com/stretchr/testify/mock"

	"code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/user_campaign_statuses"
	user_campaign_statuses_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/user_campaign_statuses"
	sfn_models "code.justin.tv/commerce/payday/sfn/models"
	. "github.com/smartystreets/goconvey/convey"
)

func TestLambdaHandler_Handle(t *testing.T) {
	Convey("given a delete_user_spomote_user_statuses handler", t, func() {
		spomoteUserStatusesDAO := new(user_campaign_statuses_mock.ISponsoredCheermoteUserStatusesDao)
		handler := &LambdaHandler{
			SpomoteUserStatusesDAO: spomoteUserStatusesDAO,
		}
		ctx := context.Background()
		userId := "test-pdms-user"
		campaignPrefix := "test-campaign"
		numOfRecords := 60
		numOfPages := 3

		testInput := sfn_models.UserDestroyInput{
			UserId: userId,
		}
		expectedOutput := &sfn_models.UserDestroyOutput{
			UserId: userId,
		}

		var testUserStatusesResp []*user_campaign_statuses.SponsoredCheermoteUserStatus
		for i := 1; i <= numOfRecords; i++ {
			testUserStatusesResp = append(testUserStatusesResp, &user_campaign_statuses.SponsoredCheermoteUserStatus{
				UserID:     userId,
				CampaignID: fmt.Sprintf("%s-%d", campaignPrefix, i),
			})
		}

		Convey("throws an error when input is invalid", func() {
			input := sfn_models.UserDestroyInput{}
			output, err := handler.Handle(ctx, input)
			So(err, ShouldNotBeNil)
			So(output, ShouldBeNil)
		})

		Convey("given dryrun", func() {
			handler.Dryrun = true

			Convey("given no user found", func() {
				var pageKey *string
				spomoteUserStatusesDAO.On("QueryWithLimit", userId, int64(queryLimit), pageKey).
					Return([]*user_campaign_statuses.SponsoredCheermoteUserStatus{}, nil, nil)
				output, err := handler.Handle(ctx, testInput)

				So(spomoteUserStatusesDAO.AssertNumberOfCalls(t, "QueryWithLimit", 1), ShouldBeTrue)
				So(spomoteUserStatusesDAO.AssertNumberOfCalls(t, "BatchDelete", 0), ShouldBeTrue)

				So(err, ShouldBeNil)
				So(output, ShouldResemble, expectedOutput)
			})

			Convey("given user to delete, pagination should work correctly and dryrun should not run Delete", func() {
				for i := 0; i <= numOfPages; i++ {
					pageSize := numOfRecords / numOfPages
					lowIndex := i * pageSize
					highIndex := lowIndex + pageSize

					var pageKey *string
					var newPageKey *string
					if i != 0 {
						pageKeyString := fmt.Sprintf("%s-%d", campaignPrefix, lowIndex)
						pageKey = &pageKeyString
					}
					if i != numOfPages {
						newPageKeyString := fmt.Sprintf("%s-%d", campaignPrefix, highIndex)
						newPageKey = &newPageKeyString
						spomoteUserStatusesDAO.On("QueryWithLimit", userId, int64(queryLimit), pageKey).
							Return(testUserStatusesResp[lowIndex:highIndex], newPageKey, nil).Once()
					} else {
						spomoteUserStatusesDAO.On("QueryWithLimit", userId, int64(queryLimit), pageKey).
							Return([]*user_campaign_statuses.SponsoredCheermoteUserStatus{}, newPageKey, nil).Once()
					}
				}
				output, err := handler.Handle(ctx, testInput)

				So(spomoteUserStatusesDAO.AssertNumberOfCalls(t, "QueryWithLimit", 4), ShouldBeTrue)
				So(spomoteUserStatusesDAO.AssertNumberOfCalls(t, "BatchDelete", 0), ShouldBeTrue)

				So(err, ShouldBeNil)
				So(output, ShouldResemble, expectedOutput)
			})
		})

		Convey("given real run", func() {
			handler.Dryrun = false

			Convey("given no user found", func() {
				var pageKey *string
				spomoteUserStatusesDAO.On("QueryWithLimit", userId, int64(queryLimit), pageKey).
					Return([]*user_campaign_statuses.SponsoredCheermoteUserStatus{}, nil, nil)
				output, err := handler.Handle(ctx, testInput)

				So(spomoteUserStatusesDAO.AssertNumberOfCalls(t, "QueryWithLimit", 1), ShouldBeTrue)
				So(spomoteUserStatusesDAO.AssertNumberOfCalls(t, "BatchDelete", 0), ShouldBeTrue)

				So(err, ShouldBeNil)
				So(output, ShouldResemble, expectedOutput)
			})

			Convey("given user to delete, real run should run Delete", func() {
				for i := 0; i <= numOfPages; i++ {
					pageSize := numOfRecords / numOfPages
					lowIndex := i * pageSize
					highIndex := lowIndex + pageSize

					var pageKey *string
					var newPageKey *string
					if i != 0 {
						pageKeyString := fmt.Sprintf("%s-%d", campaignPrefix, lowIndex)
						pageKey = &pageKeyString
					}
					if i != numOfPages {
						newPageKeyString := fmt.Sprintf("%s-%d", campaignPrefix, highIndex)
						newPageKey = &newPageKeyString
						spomoteUserStatusesDAO.On("QueryWithLimit", userId, int64(queryLimit), pageKey).
							Return(testUserStatusesResp[lowIndex:highIndex], newPageKey, nil).Once()
					} else {
						spomoteUserStatusesDAO.On("QueryWithLimit", userId, int64(queryLimit), pageKey).
							Return([]*user_campaign_statuses.SponsoredCheermoteUserStatus{}, newPageKey, nil).Once()
					}
				}
				spomoteUserStatusesDAO.On("BatchDelete", mock.Anything).Return(nil)
				output, err := handler.Handle(ctx, testInput)

				So(spomoteUserStatusesDAO.AssertNumberOfCalls(t, "QueryWithLimit", 4), ShouldBeTrue)
				So(spomoteUserStatusesDAO.AssertNumberOfCalls(t, "BatchDelete", 3), ShouldBeTrue)

				So(err, ShouldBeNil)
				So(output, ShouldResemble, expectedOutput)
			})
		})
	})
}
