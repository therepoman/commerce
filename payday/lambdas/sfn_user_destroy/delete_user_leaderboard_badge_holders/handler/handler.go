package handler

import (
	"context"
	"sort"
	"sync"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	"code.justin.tv/commerce/payday/clients/pantheon"
	"code.justin.tv/commerce/payday/dynamo/leaderboard_badge_holders"
	"code.justin.tv/commerce/payday/lambdas/sfn_user_destroy/input_validator"
	sfnmodels "code.justin.tv/commerce/payday/sfn/models"
)

/*
We run 4 goroutines:
- 1 to delete leaderboard for user's channel
- 3 to delete user from other people's leaderboards
Because when this is being executed, we need to update the leaderboard with data from Pantheon, but we cannot
guarantee Pantheon's data is updated, instead of query for top 3, we query for top 4 and filter the user out to
get the new top 3
*/

const (
	stepIdentifier               = "deleteUserLeaderboardBadgeHolders"
	leaderboardBadgeHoldersTable = "leaderboard_badge_holders"
	queryLimit                   = 50
	pantheonContextEntry         = "" // we are only interested in top 4 users
	pantheonTopN                 = int64(4)
	pantheonContextN             = int64(0)
)

type handlerMethod func(context.Context, string) error

type LambdaHandler struct {
	LeaderboardBadgeHoldersDAO leaderboard_badge_holders.ILeaderboardBadgeHoldersDao
	PantheonClient             pantheon.IPantheonClientWrapper
	Dryrun                     bool
}

func (h *LambdaHandler) Handle(ctx context.Context, input sfnmodels.UserDestroyInput) (*sfnmodels.UserDestroyOutput, error) {
	log.Infof("Executing Step %s", stepIdentifier)

	err := input_validator.ValidateInput(input)
	if err != nil {
		return nil, err
	}

	fatalErrors := make(chan error)
	wgDone := make(chan bool)

	methods := [4]handlerMethod{
		h.handleDeleteUserLeaderboard,
		h.handleDeleteUserInFirstPlace,
		h.handleDeleteUserInSecondPlace,
		h.handleDeleteUserInThirdPlace,
	}

	var wg sync.WaitGroup
	wg.Add(len(methods))

	for i := range methods {
		method := methods[i]
		go func() {
			err := method(ctx, input.UserId)
			if err != nil {
				log.WithError(err).Errorf("Error happened when trying to delete user leaderboard info from %s", leaderboardBadgeHoldersTable)
				fatalErrors <- err
			}
			wg.Done()
		}()
	}

	go func() {
		wg.Wait()
		close(wgDone)
	}()

	select {
	case err := <-fatalErrors:
		return nil, err
	case <-wgDone:
		break
	}

	return &sfnmodels.UserDestroyOutput{
		UserId: input.UserId,
	}, nil
}

func (h *LambdaHandler) handleDeleteUserLeaderboard(ctx context.Context, userID string) error {
	if h.Dryrun {
		log.WithField("UserID", userID).Infof("Dryrun, user's data in table %s will not be deleted", leaderboardBadgeHoldersTable)
	} else {
		log.WithField("UserID", userID).Infof("Real run! Requesting delete user's data in table %s", leaderboardBadgeHoldersTable)
		err := h.LeaderboardBadgeHoldersDAO.Delete(userID)
		if err != nil {
			return err
		}
	}
	return nil
}

func (h *LambdaHandler) handleDeleteUserInFirstPlace(ctx context.Context, userID string) error {
	var pagingKey *string
	retry := true

	for retry {
		results, newPagingKey, err := h.LeaderboardBadgeHoldersDAO.QueryByFirstPlaceUserID(userID, queryLimit, pagingKey)
		if err != nil {
			return err
		}

		if len(results) > 0 {
			log.Infof("Found %d entries for user %s in first place in table %s", len(results), userID, leaderboardBadgeHoldersTable)
			leaderboardsToUpdate, err := h.buildBatchPutRequest(ctx, results, userID)
			if err != nil {
				return err
			}

			if h.Dryrun {
				log.Infof("Dryrun, %d leaderboards will not be updated in Dynamo Table %s", len(leaderboardsToUpdate), leaderboardBadgeHoldersTable)
			} else {
				log.Infof("Real Run! %d leaderboards will be updated in Dynamo Table %s", len(leaderboardsToUpdate), leaderboardBadgeHoldersTable)
				err := h.LeaderboardBadgeHoldersDAO.BatchPut(leaderboardsToUpdate)
				if err != nil {
					return err
				}
			}
		}
		if newPagingKey != nil {
			log.Infof("Got pagingKey: %s", *newPagingKey)
			pagingKey = newPagingKey
		} else {
			log.Info("No pagingKey found for the query")
		}

		retry = newPagingKey != nil
	}

	return nil
}

func (h *LambdaHandler) handleDeleteUserInSecondPlace(ctx context.Context, userID string) error {
	var pagingKey *string
	retry := true

	for retry {
		results, newPagingKey, err := h.LeaderboardBadgeHoldersDAO.QueryBySecondPlaceUserID(userID, queryLimit, pagingKey)
		if err != nil {
			return err
		}

		if len(results) > 0 {
			log.Infof("Found %d entries for user %s in second place in table %s", len(results), userID, leaderboardBadgeHoldersTable)
			leaderboardsToUpdate, err := h.buildBatchPutRequest(ctx, results, userID)
			if err != nil {
				return err
			}

			if h.Dryrun {
				log.Infof("Dryrun, %d leaderboards will not be updated in Dynamo Table %s", len(leaderboardsToUpdate), leaderboardBadgeHoldersTable)
			} else {
				log.Infof("Real Run! %d leaderboards will be updated in Dynamo Table %s", len(leaderboardsToUpdate), leaderboardBadgeHoldersTable)
				err := h.LeaderboardBadgeHoldersDAO.BatchPut(leaderboardsToUpdate)
				if err != nil {
					return err
				}
			}
		}
		if newPagingKey != nil {
			log.Infof("Got pagingKey: %s", *newPagingKey)
			pagingKey = newPagingKey
		} else {
			log.Info("No pagingKey found for the query")
		}

		retry = newPagingKey != nil
	}

	return nil
}

func (h *LambdaHandler) handleDeleteUserInThirdPlace(ctx context.Context, userID string) error {
	var pagingKey *string
	retry := true

	for retry {
		results, newPagingKey, err := h.LeaderboardBadgeHoldersDAO.QueryByThirdPlaceUserID(userID, queryLimit, pagingKey)
		if err != nil {
			return err
		}

		if len(results) > 0 {
			log.Infof("Found %d entries for user %s in third place in table %s", len(results), userID, leaderboardBadgeHoldersTable)
			leaderboardsToUpdate, err := h.buildBatchPutRequest(ctx, results, userID)
			if err != nil {
				return err
			}

			if h.Dryrun {
				log.Infof("Dryrun, %d leaderboards will not be updated in Dynamo Table %s", len(leaderboardsToUpdate), leaderboardBadgeHoldersTable)
			} else {
				log.Infof("Real Run! %d leaderboards will be updated in Dynamo Table %s", len(leaderboardsToUpdate), leaderboardBadgeHoldersTable)
				err := h.LeaderboardBadgeHoldersDAO.BatchPut(leaderboardsToUpdate)
				if err != nil {
					return err
				}
			}
		}
		if newPagingKey != nil {
			log.Infof("Got pagingKey: %s", *newPagingKey)
			pagingKey = newPagingKey
		} else {
			log.Info("No pagingKey found for the query")
		}

		retry = newPagingKey != nil
	}

	return nil
}

func (h *LambdaHandler) buildBatchPutRequest(ctx context.Context, leaderboardsToUpdate []leaderboard_badge_holders.LeaderboardBadgeHolders, userToFilter string) ([]leaderboard_badge_holders.LeaderboardBadgeHolders, error) {
	var batchPutRequest []leaderboard_badge_holders.LeaderboardBadgeHolders

	var groupingKeys []string
	for _, leaderboard := range leaderboardsToUpdate {
		groupingKeys = append(groupingKeys, leaderboard.ChannelID)
	}

	leaderboards, err := h.PantheonClient.GetLeaderboards(
		ctx, pantheon.PantheonBitsUsageDomain, pantheonrpc.TimeUnit_ALLTIME, groupingKeys, pantheonContextEntry, pantheonTopN, pantheonContextN, true)
	if err != nil {
		return batchPutRequest, err
	}

	for i, leaderboard := range leaderboards {
		top3Entries := getFilteredTop3(leaderboard.Top, userToFilter)
		log.WithFields(log.Fields{
			"ChannelID":   groupingKeys[i],
			"Top3Entries": top3Entries,
		}).Infof("%s new leaderboard top 3 constructed", groupingKeys[i])
		batchPutRequest = append(batchPutRequest, leaderboard_badge_holders.LeaderboardBadgeHolders{
			ChannelID:         groupingKeys[i],
			FirstPlaceUserID:  top3Entries[0],
			SecondPlaceUserID: top3Entries[1],
			ThirdPlaceUserID:  top3Entries[2],
			LastUpdated:       time.Now(),
		})
	}
	return batchPutRequest, nil
}

func getFilteredTop3(leaderboardEntries []*pantheonrpc.LeaderboardEntry, userToFilter string) [3]string {
	sort.Slice(leaderboardEntries, func(a, b int) bool {
		return leaderboardEntries[a].Rank < leaderboardEntries[b].Rank
	})
	top3 := [3]string{}
	count := 0
	for _, leaderboardEntry := range leaderboardEntries {
		if leaderboardEntry.EntryKey != userToFilter {
			top3[count] = leaderboardEntry.EntryKey
			count++
		}
		if count >= 3 {
			break
		}
	}
	return top3
}
