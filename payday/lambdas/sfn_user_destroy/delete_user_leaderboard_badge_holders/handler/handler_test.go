package handler

import (
	"context"
	"fmt"
	"testing"
	"time"

	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	"code.justin.tv/commerce/payday/clients/pantheon"
	"code.justin.tv/commerce/payday/dynamo/leaderboard_badge_holders"
	pantheon_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/pantheon"
	leaderboard_badge_holders_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo/leaderboard_badge_holders"
	sfn_models "code.justin.tv/commerce/payday/sfn/models"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

const (
	userId                = "test-pdms-user"
	testLeaderboardPrefix = "test-leaderboard"
	randomUserPrefix      = "random-user"
)

func TestLambdaHandler_Handle(t *testing.T) {
	Convey("given a delete_user_leaderboard_badge_holders handler", t, func() {
		leaderboarBadgeHoldersDAO := new(leaderboard_badge_holders_mock.ILeaderboardBadgeHoldersDao)
		pantheonClient := new(pantheon_mock.IPantheonClientWrapper)
		handler := &LambdaHandler{
			LeaderboardBadgeHoldersDAO: leaderboarBadgeHoldersDAO,
			PantheonClient:             pantheonClient,
		}
		ctx := context.Background()
		testInput := sfn_models.UserDestroyInput{
			UserId: userId,
		}

		// data to test pagination
		numOfRecords := 9
		numOfPages := 3
		var testFirstPlaceUserIDResp []leaderboard_badge_holders.LeaderboardBadgeHolders
		for i := 1; i <= numOfRecords; i++ {
			testFirstPlaceUserIDResp = append(testFirstPlaceUserIDResp, leaderboard_badge_holders.LeaderboardBadgeHolders{
				ChannelID:         getTestLeaderboardID(i),
				FirstPlaceUserID:  userId,
				SecondPlaceUserID: getRandomUserID(1),
				ThirdPlaceUserID:  getRandomUserID(2),
				LastUpdated:       time.Now(),
			})
		}

		var testSecondPlaceUserIDResp []leaderboard_badge_holders.LeaderboardBadgeHolders
		for i := 1; i <= numOfRecords; i++ {
			testSecondPlaceUserIDResp = append(testSecondPlaceUserIDResp, leaderboard_badge_holders.LeaderboardBadgeHolders{
				ChannelID:         getTestLeaderboardID(i),
				FirstPlaceUserID:  getRandomUserID(1),
				SecondPlaceUserID: userId,
				ThirdPlaceUserID:  getRandomUserID(2),
				LastUpdated:       time.Now(),
			})
		}

		var testThirdPlaceUserIDResp []leaderboard_badge_holders.LeaderboardBadgeHolders
		for i := 1; i <= numOfRecords; i++ {
			testThirdPlaceUserIDResp = append(testThirdPlaceUserIDResp, leaderboard_badge_holders.LeaderboardBadgeHolders{
				ChannelID:         getTestLeaderboardID(i),
				FirstPlaceUserID:  getRandomUserID(1),
				SecondPlaceUserID: getRandomUserID(2),
				ThirdPlaceUserID:  userId,
				LastUpdated:       time.Now(),
			})
		}

		// test Pantheon data (hardcoding test-pdms-user as first place, it should be filtered out anyway
		var testPantheonResp []*pantheonrpc.GetLeaderboardResp
		for i := 1; i <= numOfRecords; i++ {
			testPantheonResp = append(testPantheonResp, &pantheonrpc.GetLeaderboardResp{
				Top: []*pantheonrpc.LeaderboardEntry{
					// purposely put out of order to test sorting works
					{
						Rank:     1,
						Score:    4,
						EntryKey: userId,
					},
					{
						Rank:     3,
						Score:    2,
						EntryKey: getRandomUserID(2),
					},
					{
						Rank:     4,
						Score:    1,
						EntryKey: getRandomUserID(3),
					},
					{
						Rank:     2,
						Score:    3,
						EntryKey: getRandomUserID(1),
					},
				},
			})
		}

		// test Pantheon data for when only test user has cheered
		var testPantheonRespFirstPlaceOnly []*pantheonrpc.GetLeaderboardResp
		for i := 1; i <= numOfRecords; i++ {
			testPantheonRespFirstPlaceOnly = append(testPantheonRespFirstPlaceOnly, &pantheonrpc.GetLeaderboardResp{
				Top: []*pantheonrpc.LeaderboardEntry{
					{
						Rank:     1,
						Score:    4,
						EntryKey: userId,
					},
				},
			})
		}

		Convey("throws an error when input is invalid", func() {
			input := sfn_models.UserDestroyInput{}
			output, err := handler.Handle(ctx, input)
			So(err, ShouldNotBeNil)
			So(output, ShouldBeNil)
		})

		Convey("given dryrun", func() {
			handler.Dryrun = true

			err := handler.handleDeleteUserLeaderboard(ctx, testInput.UserId)
			So(leaderboarBadgeHoldersDAO.AssertNumberOfCalls(t, "Delete", 0), ShouldBeTrue)
			So(err, ShouldBeNil)

			for place := 1; place <= 3; place++ {
				var handlerMethod func(context.Context, string) error
				var leaderboardQueryMethodName string
				var leaderboardQueryResp []leaderboard_badge_holders.LeaderboardBadgeHolders
				switch place {
				case 1:
					handlerMethod = handler.handleDeleteUserInFirstPlace
					leaderboardQueryMethodName = "QueryByFirstPlaceUserID"
					leaderboardQueryResp = testFirstPlaceUserIDResp
				case 2:
					handlerMethod = handler.handleDeleteUserInSecondPlace
					leaderboardQueryMethodName = "QueryBySecondPlaceUserID"
					leaderboardQueryResp = testSecondPlaceUserIDResp
				case 3:
					handlerMethod = handler.handleDeleteUserInThirdPlace
					leaderboardQueryMethodName = "QueryByThirdPlaceUserID"
					leaderboardQueryResp = testThirdPlaceUserIDResp
				}

				Convey(fmt.Sprintf("given user to delete is not on any leaderboard in place %d", place), func() {
					var pageKey *string
					leaderboarBadgeHoldersDAO.On(leaderboardQueryMethodName, userId, queryLimit, pageKey).
						Return([]leaderboard_badge_holders.LeaderboardBadgeHolders{}, nil, nil)
					err = handlerMethod(ctx, testInput.UserId)
					So(err, ShouldBeNil)
					So(leaderboarBadgeHoldersDAO.AssertNumberOfCalls(t, "BatchPut", 0), ShouldBeTrue)
				})

				Convey(fmt.Sprintf("given user to delete is on leaderboard in place %d", place), func() {
					var pageKey *string
					leaderboarBadgeHoldersDAO.On(leaderboardQueryMethodName, userId, queryLimit, pageKey).
						Return(leaderboardQueryResp, nil, nil)
					pantheonClient.On("GetLeaderboards", ctx, pantheon.PantheonBitsUsageDomain, pantheonrpc.TimeUnit_ALLTIME, mock.Anything, pantheonContextEntry, pantheonTopN, pantheonContextN, true).
						Return(testPantheonResp, nil)

					err = handlerMethod(ctx, testInput.UserId)
					So(err, ShouldBeNil)
					So(leaderboarBadgeHoldersDAO.AssertNumberOfCalls(t, leaderboardQueryMethodName, 1), ShouldBeTrue)
					So(pantheonClient.AssertNumberOfCalls(t, "GetLeaderboards", 1), ShouldBeTrue)
					So(leaderboarBadgeHoldersDAO.AssertNumberOfCalls(t, "BatchPut", 0), ShouldBeTrue)
				})

			}
		})

		Convey("given real run", func() {
			handler.Dryrun = false

			leaderboarBadgeHoldersDAO.On("Delete", userId).Return(nil)
			err := handler.handleDeleteUserLeaderboard(ctx, testInput.UserId)
			So(leaderboarBadgeHoldersDAO.AssertNumberOfCalls(t, "Delete", 1), ShouldBeTrue)
			So(err, ShouldBeNil)

			leaderboarBadgeHoldersDAO.On("BatchPut", mock.Anything).Return(nil)

			for place := 1; place <= 3; place++ {
				var handlerMethod func(context.Context, string) error
				var leaderboardQueryMethodName string
				var leaderboardQueryResp []leaderboard_badge_holders.LeaderboardBadgeHolders
				switch place {
				case 1:
					handlerMethod = handler.handleDeleteUserInFirstPlace
					leaderboardQueryMethodName = "QueryByFirstPlaceUserID"
					leaderboardQueryResp = testFirstPlaceUserIDResp
				case 2:
					handlerMethod = handler.handleDeleteUserInSecondPlace
					leaderboardQueryMethodName = "QueryBySecondPlaceUserID"
					leaderboardQueryResp = testSecondPlaceUserIDResp
				case 3:
					handlerMethod = handler.handleDeleteUserInThirdPlace
					leaderboardQueryMethodName = "QueryByThirdPlaceUserID"
					leaderboardQueryResp = testThirdPlaceUserIDResp
				}

				Convey(fmt.Sprintf("given user to delete is not on any leaderboard in place %d", place), func() {
					var pageKey *string
					leaderboarBadgeHoldersDAO.On(leaderboardQueryMethodName, userId, queryLimit, pageKey).
						Return([]leaderboard_badge_holders.LeaderboardBadgeHolders{}, nil, nil)
					err = handlerMethod(ctx, testInput.UserId)
					So(err, ShouldBeNil)
					So(leaderboarBadgeHoldersDAO.AssertNumberOfCalls(t, "BatchPut", 0), ShouldBeTrue)
				})

				Convey(fmt.Sprintf("given user to delete is on leaderboard in place %d", place), func() {
					for i := 0; i <= numOfPages; i++ {
						pageSize := numOfRecords / numOfPages
						lowIndex := i * pageSize
						highIndex := lowIndex + pageSize

						var pageKey *string
						var newPageKey *string
						if i != 0 {
							pageKeyString := getTestLeaderboardID(lowIndex)
							pageKey = &pageKeyString
						}
						if i != numOfPages {
							newPageKeyString := getTestLeaderboardID(highIndex)
							newPageKey = &newPageKeyString
							leaderboarBadgeHoldersDAO.On(leaderboardQueryMethodName, userId, queryLimit, pageKey).
								Return(leaderboardQueryResp[lowIndex:highIndex], newPageKey, nil).Once()
							pantheonClient.On("GetLeaderboards", ctx, pantheon.PantheonBitsUsageDomain, pantheonrpc.TimeUnit_ALLTIME, mock.Anything, pantheonContextEntry, pantheonTopN, pantheonContextN, true).
								Return(testPantheonResp[lowIndex:highIndex], nil)
						} else {
							leaderboarBadgeHoldersDAO.On(leaderboardQueryMethodName, userId, queryLimit, pageKey).
								Return([]leaderboard_badge_holders.LeaderboardBadgeHolders{}, nil, nil).Once()
							pantheonClient.On("GetLeaderboards", ctx, pantheon.PantheonBitsUsageDomain, pantheonrpc.TimeUnit_ALLTIME, mock.Anything, pantheonContextEntry, pantheonTopN, pantheonContextN, true).
								Return([]*pantheonrpc.LeaderboardEntry{}, nil)
						}
					}

					err = handlerMethod(ctx, testInput.UserId)
					So(err, ShouldBeNil)
					So(leaderboarBadgeHoldersDAO.AssertNumberOfCalls(t, leaderboardQueryMethodName, 4), ShouldBeTrue)
					So(pantheonClient.AssertNumberOfCalls(t, "GetLeaderboards", 3), ShouldBeTrue)
					So(leaderboarBadgeHoldersDAO.AssertNumberOfCalls(t, "BatchPut", 3), ShouldBeTrue)
				})
			}

			Convey("given user to delete is the only one on the leaderboard from Pantheon data", func() {
				var pageKey *string
				leaderboarBadgeHoldersDAO.On("QueryByFirstPlaceUserID", userId, queryLimit, pageKey).
					Return(testFirstPlaceUserIDResp, nil, nil)
				pantheonClient.On("GetLeaderboards", ctx, pantheon.PantheonBitsUsageDomain, pantheonrpc.TimeUnit_ALLTIME, mock.Anything, pantheonContextEntry, pantheonTopN, pantheonContextN, true).
					Return(testPantheonRespFirstPlaceOnly, nil)
				err = handler.handleDeleteUserInFirstPlace(ctx, testInput.UserId)
				So(err, ShouldBeNil)
				So(leaderboarBadgeHoldersDAO.AssertNumberOfCalls(t, "BatchPut", 1), ShouldBeTrue)
			})
		})
	})
}

func getTestLeaderboardID(id int) string {
	return fmt.Sprintf("%s-%d", testLeaderboardPrefix, id)
}

func getRandomUserID(id int) string {
	return fmt.Sprintf("%s-%d", randomUserPrefix, id)
}
