package input_validator

import (
	"errors"

	sfnmodels "code.justin.tv/commerce/payday/sfn/models"
	"code.justin.tv/commerce/payday/utils/strings"
)

func ValidateInput(input sfnmodels.UserDestroyInput) error {
	if strings.Blank(input.UserId) {
		return errors.New("missing UserId")
	}
	return nil
}
