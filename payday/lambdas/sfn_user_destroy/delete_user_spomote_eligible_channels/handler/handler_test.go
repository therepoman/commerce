package handler

import (
	"context"
	"fmt"
	"testing"

	"code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/eligible_channels"
	eligible_channels_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/eligible_channels"
	sfn_models "code.justin.tv/commerce/payday/sfn/models"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestLambdaHandler_Handle(t *testing.T) {
	Convey("given a delete_user_spomote_eligible_channels handler", t, func() {
		spomoteEligibleChannelsDAO := new(eligible_channels_mock.ISponsoredCheermoteEligibleChannelsDao)
		handler := &LambdaHandler{
			SpomoteEligibleChannelsDAO: spomoteEligibleChannelsDAO,
		}
		ctx := context.Background()
		userId := "test-pdms-user"
		campaignPrefix := "test-campaign"
		numOfRecords := 60
		numOfPages := 3

		testInput := sfn_models.UserDestroyInput{
			UserId: userId,
		}
		expectedOutput := &sfn_models.UserDestroyOutput{
			UserId: userId,
		}

		var testEligibleChannelsResp []*eligible_channels.SponsoredCheermoteEligibleChannel
		for i := 1; i <= numOfRecords; i++ {
			testEligibleChannelsResp = append(testEligibleChannelsResp, &eligible_channels.SponsoredCheermoteEligibleChannel{
				ChannelID:  userId,
				CampaignID: fmt.Sprintf("%s-%d", campaignPrefix, i),
			})
		}

		Convey("throws an error when input is invalid", func() {
			input := sfn_models.UserDestroyInput{}
			output, err := handler.Handle(ctx, input)
			So(err, ShouldNotBeNil)
			So(output, ShouldBeNil)
		})

		Convey("given dryrun", func() {
			handler.Dryrun = true

			Convey("given no user found", func() {
				var pageKey *string
				spomoteEligibleChannelsDAO.On("QueryWithLimit", userId, int64(queryLimit), pageKey).
					Return([]*eligible_channels.SponsoredCheermoteEligibleChannel{}, nil, nil)
				output, err := handler.Handle(ctx, testInput)

				So(spomoteEligibleChannelsDAO.AssertNumberOfCalls(t, "QueryWithLimit", 1), ShouldBeTrue)
				So(spomoteEligibleChannelsDAO.AssertNumberOfCalls(t, "BatchDelete", 0), ShouldBeTrue)

				So(err, ShouldBeNil)
				So(output, ShouldResemble, expectedOutput)
			})

			Convey("given user to delete, pagination should work correctly and dryrun should not run Delete", func() {
				for i := 0; i <= numOfPages; i++ {
					pageSize := numOfRecords / numOfPages
					lowIndex := i * pageSize
					highIndex := lowIndex + pageSize

					var pageKey *string
					var newPageKey *string
					if i != 0 {
						pageKeyString := fmt.Sprintf("%s-%d", campaignPrefix, lowIndex)
						pageKey = &pageKeyString
					}
					if i != numOfPages {
						newPageKeyString := fmt.Sprintf("%s-%d", campaignPrefix, highIndex)
						newPageKey = &newPageKeyString
						spomoteEligibleChannelsDAO.On("QueryWithLimit", userId, int64(queryLimit), pageKey).
							Return(testEligibleChannelsResp[lowIndex:highIndex], newPageKey, nil).Once()
					} else {
						spomoteEligibleChannelsDAO.On("QueryWithLimit", userId, int64(queryLimit), pageKey).
							Return([]*eligible_channels.SponsoredCheermoteEligibleChannel{}, newPageKey, nil).Once()
					}
				}
				output, err := handler.Handle(ctx, testInput)

				So(spomoteEligibleChannelsDAO.AssertNumberOfCalls(t, "QueryWithLimit", 4), ShouldBeTrue)
				So(spomoteEligibleChannelsDAO.AssertNumberOfCalls(t, "BatchDelete", 0), ShouldBeTrue)

				So(err, ShouldBeNil)
				So(output, ShouldResemble, expectedOutput)
			})
		})

		Convey("given real run", func() {
			handler.Dryrun = false

			Convey("given no user found", func() {
				var pageKey *string
				spomoteEligibleChannelsDAO.On("QueryWithLimit", userId, int64(queryLimit), pageKey).
					Return([]*eligible_channels.SponsoredCheermoteEligibleChannel{}, nil, nil)
				output, err := handler.Handle(ctx, testInput)

				So(spomoteEligibleChannelsDAO.AssertNumberOfCalls(t, "QueryWithLimit", 1), ShouldBeTrue)
				So(spomoteEligibleChannelsDAO.AssertNumberOfCalls(t, "BatchDelete", 0), ShouldBeTrue)

				So(err, ShouldBeNil)
				So(output, ShouldResemble, expectedOutput)
			})

			Convey("given user to delete, real run should run Delete", func() {
				for i := 0; i <= numOfPages; i++ {
					pageSize := numOfRecords / numOfPages
					lowIndex := i * pageSize
					highIndex := lowIndex + pageSize

					var pageKey *string
					var newPageKey *string
					if i != 0 {
						pageKeyString := fmt.Sprintf("%s-%d", campaignPrefix, lowIndex)
						pageKey = &pageKeyString
					}
					if i != numOfPages {
						newPageKeyString := fmt.Sprintf("%s-%d", campaignPrefix, highIndex)
						newPageKey = &newPageKeyString
						spomoteEligibleChannelsDAO.On("QueryWithLimit", userId, int64(queryLimit), pageKey).
							Return(testEligibleChannelsResp[lowIndex:highIndex], newPageKey, nil).Once()
					} else {
						spomoteEligibleChannelsDAO.On("QueryWithLimit", userId, int64(queryLimit), pageKey).
							Return([]*eligible_channels.SponsoredCheermoteEligibleChannel{}, newPageKey, nil).Once()
					}
				}
				spomoteEligibleChannelsDAO.On("BatchDelete", mock.Anything).Return(nil)
				output, err := handler.Handle(ctx, testInput)

				So(spomoteEligibleChannelsDAO.AssertNumberOfCalls(t, "QueryWithLimit", 4), ShouldBeTrue)
				So(spomoteEligibleChannelsDAO.AssertNumberOfCalls(t, "BatchDelete", 3), ShouldBeTrue)

				So(err, ShouldBeNil)
				So(output, ShouldResemble, expectedOutput)
			})
		})
	})
}
