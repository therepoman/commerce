package handler

import (
	"context"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/eligible_channels"
	"code.justin.tv/commerce/payday/lambdas/sfn_user_destroy/input_validator"
	sfnmodels "code.justin.tv/commerce/payday/sfn/models"
)

const (
	stepIdentifier = "deleteUserSpomoteEligibleChannels"
	spomoteTable   = "sponsored_cheermote_eligible_channels"
	queryLimit     = 1000
)

type LambdaHandler struct {
	SpomoteEligibleChannelsDAO eligible_channels.ISponsoredCheermoteEligibleChannelsDao
	Dryrun                     bool
}

func (h *LambdaHandler) Handle(ctx context.Context, input sfnmodels.UserDestroyInput) (*sfnmodels.UserDestroyOutput, error) {
	log.Infof("Executing Step %s", stepIdentifier)

	err := input_validator.ValidateInput(input)
	if err != nil {
		return nil, err
	}

	err = h.handleSpomoteEligibleChannelDeletion(ctx, input.UserId)
	if err != nil {
		log.WithError(err).Errorf("Error happened when trying to delete user entries from %s table", spomoteTable)
		return nil, err
	}

	return &sfnmodels.UserDestroyOutput{
		UserId: input.UserId,
	}, nil
}

func (h *LambdaHandler) handleSpomoteEligibleChannelDeletion(ctx context.Context, userID string) error {
	var pagingKey *string
	retry := true

	for retry {
		results, newPagingKey, err := h.SpomoteEligibleChannelsDAO.QueryWithLimit(userID, queryLimit, pagingKey)
		if err != nil {
			return err
		}

		if len(results) > 0 {
			log.Infof("Found %d entries for user %s in Dynamo Table %s", len(results), userID, spomoteTable)

			if h.Dryrun {
				log.Infof("Dryrun, %d entries for user %s will not be deleted from Dynamo Table %s", len(results), userID, spomoteTable)
			} else {
				log.Infof("Real Run! Deleting %d entries for user %s from Dynamo Table %s", len(results), userID, spomoteTable)
				err := h.SpomoteEligibleChannelsDAO.BatchDelete(results)
				if err != nil {
					return err
				}
			}
		}
		if newPagingKey != nil {
			log.Infof("Got pagingKey: %s", *newPagingKey)
			pagingKey = newPagingKey
		} else {
			log.Info("No pagingKey found for the query")
		}

		retry = newPagingKey != nil
	}

	return nil
}
