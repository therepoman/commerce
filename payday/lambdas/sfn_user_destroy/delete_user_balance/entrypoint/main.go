package main

import (
	"context"
	"os"
	"time"

	"code.justin.tv/amzn/TwitchS2S2/s2s2"
	log "code.justin.tv/commerce/logrus"
	s2s2client "code.justin.tv/commerce/payday/backend/s2s2"
	"code.justin.tv/commerce/payday/clients/cloudwatchlogger"
	"code.justin.tv/commerce/payday/clients/pachinko"
	"code.justin.tv/commerce/payday/config"
	delete_user_balance_handler "code.justin.tv/commerce/payday/lambdas/sfn_user_destroy/delete_user_balance/handler"
	sfnmodels "code.justin.tv/commerce/payday/sfn/models"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/codegangsta/cli"
)

var environment string
var dryrun bool
var local bool
var userID string

func main() {
	app := cli.NewApp()
	app.Name = "DeleteUserBalance"

	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "environment, e",
			Value:       "staging",
			Usage:       "Runtime config to use. Options are: 'production', and 'staging'. Defaults to 'staging' if unset.",
			EnvVar:      "payday_APP_ENV",
			Destination: &environment,
		},
		cli.StringFlag{
			Name:        "userID",
			Usage:       "Runtime config to use. Only works if local is set to true.",
			Destination: &userID,
		},
		cli.BoolFlag{
			Name:        "dryrun",
			Usage:       "Pass in this flag to only show the entries to be deleted",
			Destination: &dryrun,
		},
		cli.BoolFlag{
			Name:        "local",
			Usage:       "Pass in this flag to run the handler directly instead of through lambda.Start()",
			Destination: &local,
		},
	}

	app.Action = createBackend

	err := app.Run(os.Args)
	if err != nil {
		panic(err)
	}
}

func createBackend(c *cli.Context) {
	err := config.Load(environment)
	if err != nil {
		log.WithError(err).Panic("error loading config")
	}
	cfg := config.Get()

	s2s2logger := cloudwatchlogger.NewCloudWatchLogNoopClient()
	var s2s2Client *s2s2.S2S2
	if cfg.S2S2.Enabled {
		s2s2logger, err = cloudwatchlogger.NewCloudWatchLogClient(cfg.CloudWatchRegion, cfg.S2S2.AuthLogGroupName)
		if err != nil {
			log.WithError(err).Fatal("Unable to create s2s2 logger")
		}

		s2s2Client, err = s2s2client.NewS2S2Client(cfg, cloudwatchlogger.AdaptToTwitchLoggingLogger(s2s2logger, cfg.S2S2.AuthLogGroupName))
		if err != nil {
			log.WithError(err).Panic("Error initializing s2s2 client")
		}
	}

	lambdaHandler := delete_user_balance_handler.LambdaHandler{
		PachinkoClient: pachinko.NewClient(cfg, s2s2Client),
		S2S2Logger:     s2s2logger,
		Dryrun:         dryrun,
	}

	if local {
		ctx, cancel := context.WithTimeout(context.Background(), time.Duration(2)*time.Minute)
		defer cancel()
		output, err := lambdaHandler.Handle(ctx, sfnmodels.UserDestroyInput{
			UserId: userID,
		})
		if err != nil {
			log.WithError(err).Error("Lambda Handler Execution Failed")
		}
		log.WithField("output", output).Info("Execution Output")
	} else {
		lambda.Start(lambdaHandler.Handle)
	}
}
