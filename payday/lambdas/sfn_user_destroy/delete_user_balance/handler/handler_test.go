package handler

import (
	"context"
	"testing"

	"code.justin.tv/commerce/payday/clients/cloudwatchlogger"
	pachinko_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/pachinko"
	sfn_models "code.justin.tv/commerce/payday/sfn/models"
	. "github.com/smartystreets/goconvey/convey"
)

func TestLambdaHandler_Handle(t *testing.T) {
	Convey("given a delete_user_balance handler", t, func() {
		pachinkoClient := new(pachinko_mock.PachinkoClient)
		handler := &LambdaHandler{
			PachinkoClient: pachinkoClient,
			S2S2Logger:     cloudwatchlogger.NewCloudWatchLogNoopClient(),
		}
		ctx := context.Background()
		userId := "test-pdms-user"
		testInput := sfn_models.UserDestroyInput{
			UserId: userId,
		}
		expectedOutput := &sfn_models.UserDestroyOutput{
			UserId: userId,
		}

		Convey("throws an error when input is invalid", func() {
			input := sfn_models.UserDestroyInput{}
			output, err := handler.Handle(ctx, input)
			So(err, ShouldNotBeNil)
			So(output, ShouldBeNil)
		})

		Convey("given dryrun", func() {
			handler.Dryrun = true
			output, err := handler.Handle(ctx, testInput)

			So(pachinkoClient.AssertNumberOfCalls(t, "DeleteBalanceByOwnerId", 0), ShouldBeTrue)
			So(err, ShouldBeNil)
			So(output, ShouldResemble, expectedOutput)
		})

		Convey("given real run", func() {
			handler.Dryrun = false
			pachinkoClient.On("DeleteBalanceByOwnerId", ctx, userId).Return(nil)

			output, err := handler.Handle(ctx, testInput)

			So(pachinkoClient.AssertNumberOfCalls(t, "DeleteBalanceByOwnerId", 1), ShouldBeTrue)
			So(err, ShouldBeNil)
			So(output, ShouldResemble, expectedOutput)
		})
	})
}
