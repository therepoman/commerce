package handler

import (
	"context"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/clients/cloudwatchlogger"
	"code.justin.tv/commerce/payday/clients/pachinko"
	"code.justin.tv/commerce/payday/lambdas/sfn_user_destroy/input_validator"
	sfnmodels "code.justin.tv/commerce/payday/sfn/models"
)

const (
	stepIdentifier = "deleteUserBalance"
)

type LambdaHandler struct {
	PachinkoClient pachinko.PachinkoClient
	S2S2Logger     cloudwatchlogger.CloudWatchLogger
	Dryrun         bool
}

func (h *LambdaHandler) Handle(ctx context.Context, input sfnmodels.UserDestroyInput) (*sfnmodels.UserDestroyOutput, error) {
	defer h.S2S2Logger.Shutdown()

	log.Infof("Executing Step %s", stepIdentifier)

	err := input_validator.ValidateInput(input)
	if err != nil {
		return nil, err
	}

	err = h.handleDeleteUserBalance(ctx, input.UserId)
	if err != nil {
		log.WithError(err).Error("Error happened when trying to delete user's balances from Pachinko")
		return nil, err
	}

	return &sfnmodels.UserDestroyOutput{
		UserId: input.UserId,
	}, nil
}

func (h *LambdaHandler) handleDeleteUserBalance(ctx context.Context, userID string) error {
	if h.Dryrun {
		log.WithField("UserID", userID).Info("Dryrun, user's balances will not be deleted from Pachinko")
	} else {
		log.WithField("UserID", userID).Info("Real run! requesting delete user's balances from Pachinko")
		err := h.PachinkoClient.DeleteBalanceByOwnerId(ctx, userID)
		if err != nil {
			return err
		}
	}

	return nil
}
