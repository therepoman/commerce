package handler

import (
	"context"
	"fmt"
	"testing"

	"code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/channel_statuses"
	channel_statuses_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/channel_statuses"
	sfn_models "code.justin.tv/commerce/payday/sfn/models"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestLambdaHandler_Handle(t *testing.T) {
	Convey("given a delete_user_spomote_channel_statuses handler", t, func() {
		spomoteChannelStatusesDAO := new(channel_statuses_mock.ISponsoredCheermoteChannelStatusesDao)
		handler := &LambdaHandler{
			SpomoteChannelStatusesDAO: spomoteChannelStatusesDAO,
		}
		ctx := context.Background()
		userId := "test-pdms-user"
		campaignPrefix := "test-campaign"
		numOfRecords := 60
		numOfPages := 3

		testInput := sfn_models.UserDestroyInput{
			UserId: userId,
		}
		expectedOutput := &sfn_models.UserDestroyOutput{
			UserId: userId,
		}

		var testChannelStatusesResp []*channel_statuses.SponsoredCheermoteChannelStatus
		for i := 1; i <= numOfRecords; i++ {
			testChannelStatusesResp = append(testChannelStatusesResp, &channel_statuses.SponsoredCheermoteChannelStatus{
				ChannelID:  userId,
				CampaignID: fmt.Sprintf("%s-%d", campaignPrefix, i),
			})
		}

		Convey("throws an error when input is invalid", func() {
			input := sfn_models.UserDestroyInput{}
			output, err := handler.Handle(ctx, input)
			So(err, ShouldNotBeNil)
			So(output, ShouldBeNil)
		})

		Convey("given dryrun", func() {
			handler.Dryrun = true

			Convey("given no user found", func() {
				var pageKey *string
				spomoteChannelStatusesDAO.On("QueryWithLimit", userId, int64(queryLimit), pageKey).
					Return([]*channel_statuses.SponsoredCheermoteChannelStatus{}, nil, nil)
				output, err := handler.Handle(ctx, testInput)

				So(spomoteChannelStatusesDAO.AssertNumberOfCalls(t, "QueryWithLimit", 1), ShouldBeTrue)
				So(spomoteChannelStatusesDAO.AssertNumberOfCalls(t, "BatchDelete", 0), ShouldBeTrue)

				So(err, ShouldBeNil)
				So(output, ShouldResemble, expectedOutput)
			})

			Convey("given user to delete, pagination should work correctly and dryrun should not run Delete", func() {
				for i := 0; i <= numOfPages; i++ {
					pageSize := numOfRecords / numOfPages
					lowIndex := i * pageSize
					highIndex := lowIndex + pageSize

					var pageKey *string
					var newPageKey *string
					if i != 0 {
						pageKeyString := fmt.Sprintf("%s-%d", campaignPrefix, lowIndex)
						pageKey = &pageKeyString
					}
					if i != numOfPages {
						newPageKeyString := fmt.Sprintf("%s-%d", campaignPrefix, highIndex)
						newPageKey = &newPageKeyString
						spomoteChannelStatusesDAO.On("QueryWithLimit", userId, int64(queryLimit), pageKey).
							Return(testChannelStatusesResp[lowIndex:highIndex], newPageKey, nil).Once()
					} else {
						spomoteChannelStatusesDAO.On("QueryWithLimit", userId, int64(queryLimit), pageKey).
							Return([]*channel_statuses.SponsoredCheermoteChannelStatus{}, newPageKey, nil).Once()
					}
				}
				output, err := handler.Handle(ctx, testInput)

				So(spomoteChannelStatusesDAO.AssertNumberOfCalls(t, "QueryWithLimit", 4), ShouldBeTrue)
				So(spomoteChannelStatusesDAO.AssertNumberOfCalls(t, "BatchDelete", 0), ShouldBeTrue)

				So(err, ShouldBeNil)
				So(output, ShouldResemble, expectedOutput)
			})
		})

		Convey("given real run", func() {
			handler.Dryrun = false

			Convey("given no user found", func() {
				var pageKey *string
				spomoteChannelStatusesDAO.On("QueryWithLimit", userId, int64(queryLimit), pageKey).
					Return([]*channel_statuses.SponsoredCheermoteChannelStatus{}, nil, nil)
				output, err := handler.Handle(ctx, testInput)

				So(spomoteChannelStatusesDAO.AssertNumberOfCalls(t, "QueryWithLimit", 1), ShouldBeTrue)
				So(spomoteChannelStatusesDAO.AssertNumberOfCalls(t, "BatchDelete", 0), ShouldBeTrue)

				So(err, ShouldBeNil)
				So(output, ShouldResemble, expectedOutput)
			})

			Convey("given user to delete, real run should run Delete", func() {
				for i := 0; i <= numOfPages; i++ {
					pageSize := numOfRecords / numOfPages
					lowIndex := i * pageSize
					highIndex := lowIndex + pageSize

					var pageKey *string
					var newPageKey *string
					if i != 0 {
						pageKeyString := fmt.Sprintf("%s-%d", campaignPrefix, lowIndex)
						pageKey = &pageKeyString
					}
					if i != numOfPages {
						newPageKeyString := fmt.Sprintf("%s-%d", campaignPrefix, highIndex)
						newPageKey = &newPageKeyString
						spomoteChannelStatusesDAO.On("QueryWithLimit", userId, int64(queryLimit), pageKey).
							Return(testChannelStatusesResp[lowIndex:highIndex], newPageKey, nil).Once()
					} else {
						spomoteChannelStatusesDAO.On("QueryWithLimit", userId, int64(queryLimit), pageKey).
							Return([]*channel_statuses.SponsoredCheermoteChannelStatus{}, newPageKey, nil).Once()
					}
				}
				spomoteChannelStatusesDAO.On("BatchDelete", mock.Anything).Return(nil)
				output, err := handler.Handle(ctx, testInput)

				So(spomoteChannelStatusesDAO.AssertNumberOfCalls(t, "QueryWithLimit", 4), ShouldBeTrue)
				So(spomoteChannelStatusesDAO.AssertNumberOfCalls(t, "BatchDelete", 3), ShouldBeTrue)

				So(err, ShouldBeNil)
				So(output, ShouldResemble, expectedOutput)
			})
		})
	})
}
