package handler

import (
	"context"
	"fmt"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/lambdas/sfn_user_destroy/input_validator"
	"code.justin.tv/commerce/payday/s3"
	sfnmodels "code.justin.tv/commerce/payday/sfn/models"
)

const (
	stepIdentifier = "deleteUserBitsOnboardEvents"
)

type LambdaHandler struct {
	S3Client                s3.IS3Client
	BitsOnboardEventsBucket string
	Dryrun                  bool
}

func (h *LambdaHandler) Handle(ctx context.Context, input sfnmodels.UserDestroyInput) (*sfnmodels.UserDestroyOutput, error) {
	log.Infof("Executing Step %s", stepIdentifier)

	err := input_validator.ValidateInput(input)
	if err != nil {
		return nil, err
	}

	return h.handleBitsOnboardEventsDeletion(ctx, input)
}

func (h *LambdaHandler) handleBitsOnboardEventsDeletion(ctx context.Context, input sfnmodels.UserDestroyInput) (*sfnmodels.UserDestroyOutput, error) {
	path := fmt.Sprintf("%s/", input.UserId)

	existingFiles, err := h.S3Client.ListFileNames(h.BitsOnboardEventsBucket, path)
	if err != nil {
		return nil, err
	}

	log := log.WithFields(log.Fields{
		"bucket": h.BitsOnboardEventsBucket,
		"files":  existingFiles,
	})
	log.Infof("Found %d files to be deleted from bucket %s", len(existingFiles), h.BitsOnboardEventsBucket)

	if len(existingFiles) > 0 {
		if h.Dryrun {
			log.Info("Dryrun, files will not be deleted from S3")
		} else {
			err := h.S3Client.DeleteFiles(h.BitsOnboardEventsBucket, existingFiles...)
			if err != nil {
				return nil, err
			}
			fileExists, err := h.S3Client.FileExists(h.BitsOnboardEventsBucket, path)
			if err != nil {
				return nil, err
			}
			if fileExists {
				msg := fmt.Sprintf("user folder (%s) has not been removed", path)
				return nil, errors.New(msg)
			}
		}
	}

	return &sfnmodels.UserDestroyOutput{
		UserId: input.UserId,
	}, nil
}
