package handler

import (
	"context"
	"fmt"
	"testing"

	s3_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/s3"
	sfn_models "code.justin.tv/commerce/payday/sfn/models"
	. "github.com/smartystreets/goconvey/convey"
)

func TestLambdaHandler_Handle(t *testing.T) {
	Convey("given a delete_user_bits_onboard_events handler", t, func() {
		s3Client := new(s3_mock.IS3Client)
		bucketname := "name-of-bucket"
		userId := "test-pdms-user"
		testPath := fmt.Sprintf("%v/", userId)
		handler := &LambdaHandler{
			S3Client:                s3Client,
			BitsOnboardEventsBucket: bucketname,
		}

		ctx := context.Background()
		testInput := sfn_models.UserDestroyInput{
			UserId: userId,
		}
		expectedOutput := &sfn_models.UserDestroyOutput{
			UserId: userId,
		}

		testListFileResp := []string{
			fmt.Sprintf("%v/", userId),
			fmt.Sprintf("%v/test-file-1", userId),
			fmt.Sprintf("%v/test-file-2", userId),
			fmt.Sprintf("%v/test-file-3", userId),
		}

		Convey("throws an error when input is invalid", func() {
			input := sfn_models.UserDestroyInput{}
			output, err := handler.Handle(ctx, input)
			So(err, ShouldNotBeNil)
			So(output, ShouldBeNil)
		})

		Convey("given dryrun", func() {
			handler.Dryrun = true

			Convey("given no files", func() {
				s3Client.On("ListFileNames", bucketname, testPath).Return([]string{}, nil)
				output, err := handler.Handle(ctx, testInput)

				So(s3Client.AssertNumberOfCalls(t, "ListFileNames", 1), ShouldBeTrue)
				So(s3Client.AssertNumberOfCalls(t, "DeleteFiles", 0), ShouldBeTrue)
				So(s3Client.AssertNumberOfCalls(t, "FileExists", 0), ShouldBeTrue)

				So(err, ShouldBeNil)
				So(output, ShouldResemble, expectedOutput)
			})

			Convey("given some files, dryrun should not run Delete", func() {
				s3Client.On("ListFileNames", bucketname, testPath).Return(testListFileResp, nil)
				output, err := handler.Handle(ctx, testInput)

				So(s3Client.AssertNumberOfCalls(t, "ListFileNames", 1), ShouldBeTrue)
				So(s3Client.AssertNumberOfCalls(t, "DeleteFiles", 0), ShouldBeTrue)
				So(s3Client.AssertNumberOfCalls(t, "FileExists", 0), ShouldBeTrue)

				So(err, ShouldBeNil)
				So(output, ShouldResemble, expectedOutput)
			})
		})

		Convey("given real run", func() {
			handler.Dryrun = false

			Convey("given no files", func() {
				s3Client.On("ListFileNames", bucketname, testPath).Return([]string{}, nil)
				output, err := handler.Handle(ctx, testInput)

				So(s3Client.AssertNumberOfCalls(t, "ListFileNames", 1), ShouldBeTrue)
				So(s3Client.AssertNumberOfCalls(t, "DeleteFiles", 0), ShouldBeTrue)
				So(s3Client.AssertNumberOfCalls(t, "FileExists", 0), ShouldBeTrue)

				So(err, ShouldBeNil)
				So(output, ShouldResemble, expectedOutput)
			})

			Convey("given some files", func() {
				s3Client.On("ListFileNames", bucketname, testPath).Return(testListFileResp, nil)
				testArgs := []interface{}{bucketname}
				for _, file := range testListFileResp {
					testArgs = append(testArgs, file)
				}
				s3Client.On("DeleteFiles", testArgs...).Return(nil)

				Convey("deletion successful", func() {
					s3Client.On("FileExists", bucketname, testPath).Return(false, nil)
					output, err := handler.Handle(ctx, testInput)

					So(s3Client.AssertNumberOfCalls(t, "ListFileNames", 1), ShouldBeTrue)
					So(s3Client.AssertNumberOfCalls(t, "DeleteFiles", 1), ShouldBeTrue)
					So(s3Client.AssertNumberOfCalls(t, "FileExists", 1), ShouldBeTrue)

					So(err, ShouldBeNil)
					So(output, ShouldResemble, expectedOutput)
				})
				Convey("deletion unsuccessful", func() {
					s3Client.On("FileExists", bucketname, testPath).Return(true, nil)
					output, err := handler.Handle(ctx, testInput)

					So(s3Client.AssertNumberOfCalls(t, "ListFileNames", 1), ShouldBeTrue)
					So(s3Client.AssertNumberOfCalls(t, "DeleteFiles", 1), ShouldBeTrue)
					So(s3Client.AssertNumberOfCalls(t, "FileExists", 1), ShouldBeTrue)

					So(err, ShouldNotBeNil)
					So(output, ShouldBeNil)
				})
			})
		})
	})
}
