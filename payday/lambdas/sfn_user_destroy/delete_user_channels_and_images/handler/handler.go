package handler

import (
	"context"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/lambdas/sfn_user_destroy/input_validator"
	sfnmodels "code.justin.tv/commerce/payday/sfn/models"
)

const (
	stepIdentifier = "deleteUserChannelsAndImages"
	channelsTable  = "channels"
	imagesTable    = "images"
)

type LambdaHandler struct {
	ChannelsDAO dynamo.IChannelDao
	ImagesDAO   dynamo.IImageDao
	Dryrun      bool
}

func (h *LambdaHandler) Handle(ctx context.Context, input sfnmodels.UserDestroyInput) (*sfnmodels.UserDestroyOutput, error) {
	log.Infof("Executing Step %s", stepIdentifier)

	err := input_validator.ValidateInput(input)
	if err != nil {
		return nil, err
	}

	err = h.handleChannelsAndImagesDeletion(ctx, dynamo.ChannelId(input.UserId))
	if err != nil {
		log.WithError(err).Errorf("Error happened when trying to delete user entries from %s and %s tables", channelsTable, imagesTable)
		return nil, err
	}

	return &sfnmodels.UserDestroyOutput{
		UserId: input.UserId,
	}, nil
}

func (h *LambdaHandler) handleChannelsAndImagesDeletion(ctx context.Context, userID dynamo.ChannelId) error {
	existingUser, err := h.ChannelsDAO.Get(userID)
	if err != nil {
		return err
	}

	if existingUser == nil {
		log.Infof("Did not find user %s in Dynamo Table %s", userID, channelsTable)
	} else {
		log.Infof("Found user %s in Dynamo Table %s", userID, channelsTable)

		existingImageSetId := existingUser.CustomCheermoteImageSetId
		if existingImageSetId == nil {
			log.Infof("Did not find custom image set from user %s in Dynamo Table %s", userID, channelsTable)
		} else {
			err := h.handleImagesDeletion(ctx, *existingImageSetId)
			if err != nil {
				return err
			}
		}

		if h.Dryrun {
			log.Infof("Dryrun, user %s will not be deleted from Dynamo Table %s", userID, channelsTable)
		} else {
			log.Infof("Real run! Deleting user %s from Dynamo Table %s", userID, channelsTable)
			err := h.ChannelsDAO.Delete(userID)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func (h *LambdaHandler) handleImagesDeletion(ctx context.Context, imageSetID string) error {
	existingImages, err := h.ImagesDAO.GetAllBySetId(imageSetID)
	if err != nil {
		return err
	}
	log.Infof("Found %v images in Dynamo Table %v", len(existingImages), imagesTable)

	if len(existingImages) != 0 {
		for _, image := range existingImages {
			if image != nil {
				log.Infof("Found image: %v", *image)
			}
		}
		if h.Dryrun {
			log.Infof("Dryrun, images will not be deleted from Dynamo Table %v", imagesTable)
		} else {
			log.Infof("Real Run! Images will be deleted from Dynamo Table %v", imagesTable)

			err := h.ImagesDAO.BatchDelete(existingImages)
			if err != nil {
				return err
			}
		}
	}

	return nil
}
