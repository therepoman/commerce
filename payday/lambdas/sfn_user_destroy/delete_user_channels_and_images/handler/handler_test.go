package handler

import (
	"context"
	"testing"

	"code.justin.tv/commerce/payday/dynamo"
	dynamo_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo"
	sfn_models "code.justin.tv/commerce/payday/sfn/models"
	. "github.com/smartystreets/goconvey/convey"
)

func TestLambdaHandler_Handle(t *testing.T) {
	Convey("given a delete_user_channels_and_images handler", t, func() {
		channelsDAO := new(dynamo_mock.IChannelDao)
		imagesDAO := new(dynamo_mock.IImageDao)
		handler := &LambdaHandler{
			ChannelsDAO: channelsDAO,
			ImagesDAO:   imagesDAO,
		}
		ctx := context.Background()
		userId := "test-pdms-user"
		setId := "test-pdms-user-images"
		testInput := sfn_models.UserDestroyInput{
			UserId: userId,
		}
		expectedOutput := &sfn_models.UserDestroyOutput{
			UserId: userId,
		}

		testChannelsResp := &dynamo.Channel{
			Id:                        dynamo.ChannelId(userId),
			CustomCheermoteImageSetId: &setId,
		}
		testImagesResp := []*dynamo.Image{
			{
				SetId:     setId,
				AssetType: "some-asset-1",
			},
			{
				SetId:     setId,
				AssetType: "some-asset-2",
			},
			{
				SetId:     setId,
				AssetType: "some-asset-3",
			},
		}

		Convey("throws an error when input is invalid", func() {
			input := sfn_models.UserDestroyInput{}
			output, err := handler.Handle(ctx, input)
			So(err, ShouldNotBeNil)
			So(output, ShouldBeNil)
		})

		Convey("given dryrun", func() {
			handler.Dryrun = true

			Convey("given no channel found", func() {
				channelsDAO.On("Get", dynamo.ChannelId(userId)).Return(nil, nil)
				output, err := handler.Handle(ctx, testInput)

				So(channelsDAO.AssertNumberOfCalls(t, "Get", 1), ShouldBeTrue)
				So(channelsDAO.AssertNumberOfCalls(t, "Delete", 0), ShouldBeTrue)
				So(imagesDAO.AssertNumberOfCalls(t, "GetAllBySetId", 0), ShouldBeTrue)
				So(imagesDAO.AssertNumberOfCalls(t, "BatchDelete", 0), ShouldBeTrue)

				So(err, ShouldBeNil)
				So(output, ShouldResemble, expectedOutput)
			})

			Convey("given channel to delete, dryrun should not run Delete", func() {
				channelsDAO.On("Get", dynamo.ChannelId(userId)).Return(testChannelsResp, nil)
				imagesDAO.On("GetAllBySetId", setId).Return(testImagesResp, nil)
				output, err := handler.Handle(ctx, testInput)

				So(channelsDAO.AssertNumberOfCalls(t, "Get", 1), ShouldBeTrue)
				So(channelsDAO.AssertNumberOfCalls(t, "Delete", 0), ShouldBeTrue)
				So(imagesDAO.AssertNumberOfCalls(t, "GetAllBySetId", 1), ShouldBeTrue)
				So(imagesDAO.AssertNumberOfCalls(t, "BatchDelete", 0), ShouldBeTrue)

				So(err, ShouldBeNil)
				So(output, ShouldResemble, expectedOutput)
			})
		})

		Convey("given real run", func() {
			handler.Dryrun = false

			Convey("given no channel found", func() {
				channelsDAO.On("Get", dynamo.ChannelId(userId)).Return(nil, nil)
				output, err := handler.Handle(ctx, testInput)

				So(channelsDAO.AssertNumberOfCalls(t, "Get", 1), ShouldBeTrue)
				So(channelsDAO.AssertNumberOfCalls(t, "Delete", 0), ShouldBeTrue)
				So(imagesDAO.AssertNumberOfCalls(t, "GetAllBySetId", 0), ShouldBeTrue)
				So(imagesDAO.AssertNumberOfCalls(t, "BatchDelete", 0), ShouldBeTrue)

				So(err, ShouldBeNil)
				So(output, ShouldResemble, expectedOutput)
			})

			Convey("given channel to delete, real run should run Delete", func() {
				channelsDAO.On("Get", dynamo.ChannelId(userId)).Return(testChannelsResp, nil)
				channelsDAO.On("Delete", dynamo.ChannelId(userId)).Return(nil)
				imagesDAO.On("GetAllBySetId", setId).Return(testImagesResp, nil)
				imagesDAO.On("BatchDelete", testImagesResp).Return(nil)
				output, err := handler.Handle(ctx, testInput)

				So(channelsDAO.AssertNumberOfCalls(t, "Get", 1), ShouldBeTrue)
				So(channelsDAO.AssertNumberOfCalls(t, "Delete", 1), ShouldBeTrue)
				So(imagesDAO.AssertNumberOfCalls(t, "GetAllBySetId", 1), ShouldBeTrue)
				So(imagesDAO.AssertNumberOfCalls(t, "BatchDelete", 1), ShouldBeTrue)

				So(err, ShouldBeNil)
				So(output, ShouldResemble, expectedOutput)

			})
		})
	})
}
