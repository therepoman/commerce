package handler

import (
	"context"
	"testing"

	"code.justin.tv/commerce/payday/dynamo"
	dynamo_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo"
	sfn_models "code.justin.tv/commerce/payday/sfn/models"
	. "github.com/smartystreets/goconvey/convey"
)

func TestLambdaHandler_Handle(t *testing.T) {
	Convey("given a delete_user handler", t, func() {
		userDAO := new(dynamo_mock.IUserDao)
		handler := &LambdaHandler{
			UserDAO: userDAO,
		}
		ctx := context.Background()
		userId := "test-pdms-user"
		testInput := sfn_models.UserDestroyInput{
			UserId: userId,
		}
		expectedOutput := &sfn_models.UserDestroyOutput{
			UserId: userId,
		}

		testUsersResp := &dynamo.User{
			Id: dynamo.UserId(userId),
		}

		Convey("throws an error when input is invalid", func() {
			input := sfn_models.UserDestroyInput{}
			output, err := handler.Handle(ctx, input)
			So(err, ShouldNotBeNil)
			So(output, ShouldBeNil)
		})

		Convey("given dryrun", func() {
			handler.Dryrun = true

			Convey("given no user found", func() {
				userDAO.On("Get", dynamo.UserId(userId)).Return(nil, nil)
				output, err := handler.Handle(ctx, testInput)

				So(userDAO.AssertNumberOfCalls(t, "Get", 1), ShouldBeTrue)
				So(userDAO.AssertNumberOfCalls(t, "Delete", 0), ShouldBeTrue)

				So(err, ShouldBeNil)
				So(output, ShouldResemble, expectedOutput)
			})

			Convey("given user to delete, dryrun should not run Delete", func() {
				userDAO.On("Get", dynamo.UserId(userId)).Return(testUsersResp, nil)
				output, err := handler.Handle(ctx, testInput)

				So(userDAO.AssertNumberOfCalls(t, "Get", 1), ShouldBeTrue)
				So(userDAO.AssertNumberOfCalls(t, "Delete", 0), ShouldBeTrue)

				So(err, ShouldBeNil)
				So(output, ShouldResemble, expectedOutput)
			})
		})

		Convey("given real run", func() {
			handler.Dryrun = false

			Convey("given no user found", func() {
				userDAO.On("Get", dynamo.UserId(userId)).Return(nil, nil)
				output, err := handler.Handle(ctx, testInput)

				So(userDAO.AssertNumberOfCalls(t, "Get", 1), ShouldBeTrue)
				So(userDAO.AssertNumberOfCalls(t, "Delete", 0), ShouldBeTrue)

				So(err, ShouldBeNil)
				So(output, ShouldResemble, expectedOutput)
			})

			Convey("given user to delete, real run should run Delete", func() {
				userDAO.On("Get", dynamo.UserId(userId)).Return(testUsersResp, nil)
				userDAO.On("Delete", dynamo.UserId(userId)).Return(nil)
				output, err := handler.Handle(ctx, testInput)

				So(userDAO.AssertNumberOfCalls(t, "Get", 1), ShouldBeTrue)
				So(userDAO.AssertNumberOfCalls(t, "Delete", 1), ShouldBeTrue)

				So(err, ShouldBeNil)
				So(output, ShouldResemble, expectedOutput)
			})
		})
	})
}
