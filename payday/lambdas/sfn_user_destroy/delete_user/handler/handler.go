package handler

import (
	"context"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/lambdas/sfn_user_destroy/input_validator"
	sfnmodels "code.justin.tv/commerce/payday/sfn/models"
)

const (
	stepIdentifier = "deleteUser"
	userTable      = "users"
)

type LambdaHandler struct {
	UserDAO dynamo.IUserDao
	Dryrun  bool
}

func (h *LambdaHandler) Handle(ctx context.Context, input sfnmodels.UserDestroyInput) (*sfnmodels.UserDestroyOutput, error) {
	log.Infof("Executing Step %s", stepIdentifier)

	err := input_validator.ValidateInput(input)
	if err != nil {
		return nil, err
	}

	err = h.handleUserDeletion(ctx, dynamo.UserId(input.UserId))
	if err != nil {
		log.WithError(err).Errorf("Error happened when trying to delete user entries from %s table", userTable)
		return nil, err
	}

	return &sfnmodels.UserDestroyOutput{
		UserId: input.UserId,
	}, nil
}

func (h *LambdaHandler) handleUserDeletion(ctx context.Context, userID dynamo.UserId) error {
	existingUser, err := h.UserDAO.Get(userID)
	if err != nil {
		return err
	}

	if existingUser == nil {
		log.Infof("Did not find user %s in Dynamo Table %s", userID, userTable)
	} else {
		log.Infof("Found user %s in Dynamo Table %s", userID, userTable)

		if h.Dryrun {
			log.Infof("Dryrun, user %s will not be deleted from Dynamo Table %s", userID, userTable)
		} else {
			log.Infof("Real run! Deleting user %s from Dynamo Table %s", userID, userTable)
			err := h.UserDAO.Delete(userID)
			if err != nil {
				return err
			}
		}
	}

	return nil
}
