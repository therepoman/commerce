package handler

import (
	"context"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/clients/pdms"
	"code.justin.tv/commerce/payday/lambdas/sfn_user_destroy/input_validator"
	sfnmodels "code.justin.tv/commerce/payday/sfn/models"
	"github.com/golang/protobuf/ptypes"
)

const (
	stepIdentifier = "reportDeletion"
)

type LambdaHandler struct {
	PDMSClient pdms.IPDMSClientWrapper
	Dryrun     bool
}

func (h *LambdaHandler) Handle(ctx context.Context, input sfnmodels.UserDestroyInput) (*sfnmodels.UserDestroyReportDeletionOutput, error) {
	log.Infof("Executing Step %s", stepIdentifier)

	err := input_validator.ValidateInput(input)
	if err != nil {
		return nil, err
	}

	timeOfDeletion, err := h.handleReportDeletion(ctx, input.UserId)
	if err != nil {
		log.WithError(err).Error("error reporting user deletion to PDMS")
		return nil, err
	}

	return &sfnmodels.UserDestroyReportDeletionOutput{
		UserId:    input.UserId,
		Timestamp: timeOfDeletion,
	}, nil
}

func (h *LambdaHandler) handleReportDeletion(ctx context.Context, userID string) (time.Time, error) {
	timeOfDeletion := time.Now()
	timeOfDeletionTwirp, err := ptypes.TimestampProto(timeOfDeletion)
	if err != nil {
		return time.Time{}, err
	}

	log := log.WithFields(log.Fields{
		"userID":    userID,
		"timestamp": timeOfDeletion,
	})
	if h.Dryrun {
		log.Info("Dryrun, will not report user deletion to PDMS")
	} else {
		log.Infof("Real Run! Reporting user deletion to PDMS")
		err := h.PDMSClient.ReportDeletion(ctx, userID, timeOfDeletionTwirp)
		if err != nil {
			// report deletion is idempotent, duplicate deletion of the same user will not return an error.
			return time.Time{}, err
		}
	}

	return timeOfDeletion, nil
}
