package handler

import (
	"context"
	"testing"

	pdms_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/pdms"
	sfn_models "code.justin.tv/commerce/payday/sfn/models"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestLambdaHandler_Handle(t *testing.T) {
	Convey("given a report_deletion handler", t, func() {
		pdmsClient := new(pdms_mock.IPDMSClientWrapper)
		handler := &LambdaHandler{
			PDMSClient: pdmsClient,
		}
		ctx := context.Background()
		userId := "test-pdms-user"

		testInput := sfn_models.UserDestroyInput{
			UserId: userId,
		}

		// The timestamp field is generated dynamically, ignoring here
		expectedOutput := &sfn_models.UserDestroyReportDeletionOutput{
			UserId: userId,
		}

		Convey("throws an error when input is invalid", func() {
			input := sfn_models.UserDestroyInput{}
			output, err := handler.Handle(ctx, input)
			So(err, ShouldNotBeNil)
			So(output, ShouldBeNil)
		})

		Convey("given dryrun", func() {
			handler.Dryrun = true

			output, err := handler.Handle(ctx, testInput)
			So(pdmsClient.AssertNumberOfCalls(t, "ReportDeletion", 0), ShouldBeTrue)
			So(err, ShouldBeNil)
			So(output.UserId, ShouldEqual, expectedOutput.UserId)
			So(output.Timestamp, ShouldNotBeNil)
		})

		Convey("given real run", func() {
			handler.Dryrun = false

			pdmsClient.On("ReportDeletion", ctx, userId, mock.Anything).
				Return(nil)

			output, err := handler.Handle(ctx, testInput)
			So(pdmsClient.AssertNumberOfCalls(t, "ReportDeletion", 1), ShouldBeTrue)
			So(err, ShouldBeNil)
			So(output.UserId, ShouldEqual, expectedOutput.UserId)
			So(output.Timestamp, ShouldNotBeNil)
		})
	})
}
