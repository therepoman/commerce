package main

import (
	"context"
	"net/http"
	"os"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/clients/pdms"
	"code.justin.tv/commerce/payday/config"
	report_deletion_handler "code.justin.tv/commerce/payday/lambdas/sfn_user_destroy/report_deletion/handler"
	sfnmodels "code.justin.tv/commerce/payday/sfn/models"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/endpoints"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/codegangsta/cli"
)

var environment string
var dryrun bool
var local bool
var userID string

func main() {
	app := cli.NewApp()
	app.Name = "ReportDeletion"

	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "environment, e",
			Value:       "staging",
			Usage:       "Runtime config to use. Options are: 'production', and 'staging'. Defaults to 'staging' if unset.",
			EnvVar:      "payday_APP_ENV",
			Destination: &environment,
		},
		cli.StringFlag{
			Name:        "userID",
			Usage:       "Runtime config to use. Only works if local is set to true.",
			Destination: &userID,
		},
		cli.BoolFlag{
			Name:        "dryrun",
			Usage:       "Pass in this flag to only show the entries to be deleted",
			Destination: &dryrun,
		},
		cli.BoolFlag{
			Name:        "local",
			Usage:       "Pass in this flag to run the handler directly instead of through lambda.Start()",
			Destination: &local,
		},
	}

	app.Action = createBackend

	err := app.Run(os.Args)
	if err != nil {
		panic(err)
	}
}

func createBackend(c *cli.Context) {
	err := config.Load(environment)
	if err != nil {
		log.WithError(err).Panicf("error loading config")
	}
	cfg := config.Get()

	// Taken from https://wiki.twitch.com/display/SEC/PDMS-+Delete+pipeline+Service+Onboarding
	sess := session.Must(session.NewSessionWithOptions(session.Options{
		Config: aws.Config{
			Region:              aws.String("us-west-2"),
			STSRegionalEndpoint: endpoints.RegionalSTSEndpoint,
			// We want a long timeout for PDMS, as Lambda based services can take a while to warm up on cold start.
			HTTPClient: &http.Client{Timeout: 10 * time.Second},
		},
	}))

	lambdaHandler := report_deletion_handler.LambdaHandler{
		PDMSClient: pdms.NewPDMSClientWrapper(cfg, sess),
		Dryrun:     dryrun,
	}

	if local {
		ctx, cancel := context.WithTimeout(context.Background(), time.Duration(2)*time.Minute)
		defer cancel()
		output, err := lambdaHandler.Handle(ctx, sfnmodels.UserDestroyInput{
			UserId: userID,
		})
		if err != nil {
			log.WithError(err).Error("Lambda Handler Execution Failed")
		}
		log.WithField("output", output).Info("Execution Output")
	} else {
		lambda.Start(lambdaHandler.Handle)
	}

}
