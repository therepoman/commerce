package handler

import (
	"context"
	"fmt"
	"testing"

	purchase_by_platform "code.justin.tv/commerce/payday/dynamo/purchase_by_most_recent_super_platform"
	purchase_by_platform_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo/purchase_by_most_recent_super_platform"
	sfn_models "code.justin.tv/commerce/payday/sfn/models"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestLambdaHandler_Handle(t *testing.T) {
	Convey("given a delete_user_purchase_by_platform handler", t, func() {
		purchaseByPlatformDAO := new(purchase_by_platform_mock.PurchaseByMostRecentSuperPlatformDAO)
		handler := &LambdaHandler{
			PurchaseByMostRecentSuperPlatformDAO: purchaseByPlatformDAO,
		}
		ctx := context.Background()
		userId := "test-pdms-user"
		platformPrefix := "test-platform"
		numOfRecords := 60
		numOfPages := 3

		testInput := sfn_models.UserDestroyInput{
			UserId: userId,
		}
		expectedOutput := &sfn_models.UserDestroyOutput{
			UserId: userId,
		}

		var testPurchaseRecords []purchase_by_platform.PurchaseByMostRecentSuperPlatform
		for i := 1; i <= numOfRecords; i++ {
			testPurchaseRecords = append(testPurchaseRecords, purchase_by_platform.PurchaseByMostRecentSuperPlatform{
				TwitchUserID:  userId,
				SuperPlatform: fmt.Sprintf("%s-%d", platformPrefix, i),
			})
		}

		Convey("throws an error when input is invalid", func() {
			input := sfn_models.UserDestroyInput{}
			output, err := handler.Handle(ctx, input)
			So(err, ShouldNotBeNil)
			So(output, ShouldBeNil)
		})

		Convey("given dryrun", func() {
			handler.Dryrun = true

			Convey("given no user found", func() {
				var pageKey *string
				purchaseByPlatformDAO.On("QueryWithLimit", ctx, userId, int64(queryLimit), pageKey).
					Return([]purchase_by_platform.PurchaseByMostRecentSuperPlatform{}, nil, nil)
				output, err := handler.Handle(ctx, testInput)

				So(purchaseByPlatformDAO.AssertNumberOfCalls(t, "QueryWithLimit", 1), ShouldBeTrue)
				So(purchaseByPlatformDAO.AssertNumberOfCalls(t, "BatchDelete", 0), ShouldBeTrue)

				So(err, ShouldBeNil)
				So(output, ShouldResemble, expectedOutput)
			})

			Convey("given user to delete, pagination should work correctly and dryrun should not run Delete", func() {
				for i := 0; i <= numOfPages; i++ {
					pageSize := numOfRecords / numOfPages
					lowIndex := i * pageSize
					highIndex := lowIndex + pageSize

					var pageKey *string
					var newPageKey *string
					if i != 0 {
						pageKeyString := fmt.Sprintf("%s-%d", platformPrefix, lowIndex)
						pageKey = &pageKeyString
					}
					if i != numOfPages {
						newPageKeyString := fmt.Sprintf("%s-%d", platformPrefix, highIndex)
						newPageKey = &newPageKeyString
						purchaseByPlatformDAO.On("QueryWithLimit", ctx, userId, int64(queryLimit), pageKey).
							Return(testPurchaseRecords[lowIndex:highIndex], newPageKey, nil).Once()
					} else {
						purchaseByPlatformDAO.On("QueryWithLimit", ctx, userId, int64(queryLimit), pageKey).
							Return([]purchase_by_platform.PurchaseByMostRecentSuperPlatform{}, newPageKey, nil).Once()
					}
				}
				output, err := handler.Handle(ctx, testInput)

				So(purchaseByPlatformDAO.AssertNumberOfCalls(t, "QueryWithLimit", 4), ShouldBeTrue)
				So(purchaseByPlatformDAO.AssertNumberOfCalls(t, "BatchDelete", 0), ShouldBeTrue)

				So(err, ShouldBeNil)
				So(output, ShouldResemble, expectedOutput)
			})
		})

		Convey("given real run", func() {
			handler.Dryrun = false

			Convey("given no user found", func() {
				var pageKey *string
				purchaseByPlatformDAO.On("QueryWithLimit", ctx, userId, int64(queryLimit), pageKey).
					Return([]purchase_by_platform.PurchaseByMostRecentSuperPlatform{}, nil, nil)
				output, err := handler.Handle(ctx, testInput)

				So(purchaseByPlatformDAO.AssertNumberOfCalls(t, "QueryWithLimit", 1), ShouldBeTrue)
				So(purchaseByPlatformDAO.AssertNumberOfCalls(t, "BatchDelete", 0), ShouldBeTrue)

				So(err, ShouldBeNil)
				So(output, ShouldResemble, expectedOutput)
			})

			Convey("given user to delete, real run should run Delete", func() {
				for i := 0; i <= numOfPages; i++ {
					pageSize := numOfRecords / numOfPages
					lowIndex := i * pageSize
					highIndex := lowIndex + pageSize

					var pageKey *string
					var newPageKey *string
					if i != 0 {
						pageKeyString := fmt.Sprintf("%s-%d", platformPrefix, lowIndex)
						pageKey = &pageKeyString
					}
					if i != numOfPages {
						newPageKeyString := fmt.Sprintf("%s-%d", platformPrefix, highIndex)
						newPageKey = &newPageKeyString
						purchaseByPlatformDAO.On("QueryWithLimit", ctx, userId, int64(queryLimit), pageKey).
							Return(testPurchaseRecords[lowIndex:highIndex], newPageKey, nil).Once()
					} else {
						purchaseByPlatformDAO.On("QueryWithLimit", ctx, userId, int64(queryLimit), pageKey).
							Return([]purchase_by_platform.PurchaseByMostRecentSuperPlatform{}, newPageKey, nil).Once()
					}
				}
				purchaseByPlatformDAO.On("BatchDelete", ctx, mock.Anything).Return(nil)
				output, err := handler.Handle(ctx, testInput)

				So(purchaseByPlatformDAO.AssertNumberOfCalls(t, "QueryWithLimit", 4), ShouldBeTrue)
				So(purchaseByPlatformDAO.AssertNumberOfCalls(t, "BatchDelete", 3), ShouldBeTrue)

				So(err, ShouldBeNil)
				So(output, ShouldResemble, expectedOutput)
			})
		})
	})
}
