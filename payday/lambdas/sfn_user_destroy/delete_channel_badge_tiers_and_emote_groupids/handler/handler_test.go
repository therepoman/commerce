package handler

import (
	"context"
	"testing"

	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/dynamo/badge_tier_emote_groupids"
	dynamo_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo"
	badge_tier_emote_groupids_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo/badge_tier_emote_groupids"
	sfn_models "code.justin.tv/commerce/payday/sfn/models"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestLambdaHandler_Handle(t *testing.T) {
	Convey("given a delete_channel_badge_tiers_and_emote_groupids handler", t, func() {
		badgeTiersDAO := new(dynamo_mock.IBadgeTierDao)
		badgeTierEmoteGroupIDsDAO := new(badge_tier_emote_groupids_mock.BadgeTierEmoteGroupIDDAO)
		handler := &LambdaHandler{
			BadgeTiersDAO:             badgeTiersDAO,
			BadgeTierEmoteGroupIDsDAO: badgeTierEmoteGroupIDsDAO,
		}
		ctx := context.Background()
		userId := "test-pdms-user"
		testInput := sfn_models.UserDestroyInput{
			UserId: userId,
		}
		expectedOutput := &sfn_models.UserDestroyOutput{
			UserId: userId,
		}

		testBadgeTiersResp := []*dynamo.BadgeTier{
			{
				ChannelId: userId,
				Threshold: 1,
			},
			{
				ChannelId: userId,
				Threshold: 100,
			},
		}
		testBadgeTierEmoteGroupIDsResp := []*badge_tier_emote_groupids.BadgeTierEmoteGroupID{
			{
				ChannelID: userId,
				Threshold: 1,
			},
			{
				ChannelID: userId,
				Threshold: 100,
			},
		}

		Convey("throws an error when input is invalid", func() {
			input := sfn_models.UserDestroyInput{}
			output, err := handler.Handle(ctx, input)
			So(err, ShouldNotBeNil)
			So(output, ShouldBeNil)
		})

		Convey("given dryrun", func() {
			handler.Dryrun = true

			Convey("given no badge tiers", func() {
				badgeTiersDAO.On("GetAll", userId, true).Return([]*dynamo.BadgeTier{}, nil)
				badgeTierEmoteGroupIDsDAO.On("Get", ctx, userId).Return([]*badge_tier_emote_groupids.BadgeTierEmoteGroupID{}, nil)
				output, err := handler.Handle(ctx, testInput)

				So(badgeTiersDAO.AssertNumberOfCalls(t, "GetAll", 1), ShouldBeTrue)
				So(badgeTierEmoteGroupIDsDAO.AssertNumberOfCalls(t, "Get", 1), ShouldBeTrue)
				So(badgeTiersDAO.AssertNumberOfCalls(t, "Delete", 0), ShouldBeTrue)
				So(badgeTierEmoteGroupIDsDAO.AssertNumberOfCalls(t, "Delete", 0), ShouldBeTrue)

				So(err, ShouldBeNil)
				So(output, ShouldResemble, expectedOutput)
			})

			Convey("given some badge tiers, dryrun should not run Delete", func() {
				badgeTiersDAO.On("GetAll", userId, true).Return(testBadgeTiersResp, nil)
				badgeTierEmoteGroupIDsDAO.On("Get", ctx, userId).Return(testBadgeTierEmoteGroupIDsResp, nil)
				output, err := handler.Handle(ctx, testInput)

				So(badgeTiersDAO.AssertNumberOfCalls(t, "GetAll", 1), ShouldBeTrue)
				So(badgeTierEmoteGroupIDsDAO.AssertNumberOfCalls(t, "Get", 1), ShouldBeTrue)
				So(badgeTiersDAO.AssertNumberOfCalls(t, "Delete", 0), ShouldBeTrue)
				So(badgeTierEmoteGroupIDsDAO.AssertNumberOfCalls(t, "Delete", 0), ShouldBeTrue)

				So(err, ShouldBeNil)
				So(output, ShouldResemble, expectedOutput)
			})
		})

		Convey("given real run", func() {
			handler.Dryrun = false

			Convey("given no badge tiers", func() {
				badgeTiersDAO.On("GetAll", userId, true).Return([]*dynamo.BadgeTier{}, nil)
				badgeTierEmoteGroupIDsDAO.On("Get", ctx, userId).Return([]*badge_tier_emote_groupids.BadgeTierEmoteGroupID{}, nil)
				output, err := handler.Handle(ctx, testInput)

				So(badgeTiersDAO.AssertNumberOfCalls(t, "GetAll", 1), ShouldBeTrue)
				So(badgeTierEmoteGroupIDsDAO.AssertNumberOfCalls(t, "Get", 1), ShouldBeTrue)
				So(badgeTiersDAO.AssertNumberOfCalls(t, "Delete", 0), ShouldBeTrue)
				So(badgeTierEmoteGroupIDsDAO.AssertNumberOfCalls(t, "Delete", 0), ShouldBeTrue)

				So(err, ShouldBeNil)
				So(output, ShouldResemble, expectedOutput)
			})

			Convey("given some badge tiers, real run should run Delete", func() {
				badgeTiersDAO.On("GetAll", userId, true).Return(testBadgeTiersResp, nil)
				badgeTierEmoteGroupIDsDAO.On("Get", ctx, userId).Return(testBadgeTierEmoteGroupIDsResp, nil)
				badgeTiersDAO.On("Delete", userId, mock.Anything).Return(nil)
				badgeTierEmoteGroupIDsDAO.On("Delete", ctx, userId, mock.Anything).Return(nil)
				output, err := handler.Handle(ctx, testInput)

				So(badgeTiersDAO.AssertNumberOfCalls(t, "GetAll", 1), ShouldBeTrue)
				So(badgeTierEmoteGroupIDsDAO.AssertNumberOfCalls(t, "Get", 1), ShouldBeTrue)
				So(badgeTiersDAO.AssertNumberOfCalls(t, "Delete", len(testBadgeTiersResp)), ShouldBeTrue)
				So(badgeTierEmoteGroupIDsDAO.AssertNumberOfCalls(t, "Delete", len(testBadgeTierEmoteGroupIDsResp)), ShouldBeTrue)

				So(err, ShouldBeNil)
				So(output, ShouldResemble, expectedOutput)
			})
		})
	})
}
