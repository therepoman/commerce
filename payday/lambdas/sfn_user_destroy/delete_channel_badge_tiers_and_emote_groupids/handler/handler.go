package handler

import (
	"context"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/dynamo/badge_tier_emote_groupids"
	"code.justin.tv/commerce/payday/lambdas/sfn_user_destroy/input_validator"
	sfnmodels "code.justin.tv/commerce/payday/sfn/models"
)

const (
	stepIdentifier         = "deleteChannelBadgeTiersAndEmoteGroupids"
	badgeTiers             = "badge-tiers"
	badgeTierEmoteGroupIDs = "badge_tier_emote_groupids"
)

type LambdaHandler struct {
	BadgeTiersDAO             dynamo.IBadgeTierDao
	BadgeTierEmoteGroupIDsDAO badge_tier_emote_groupids.BadgeTierEmoteGroupIDDAO
	Dryrun                    bool
}

func (h *LambdaHandler) Handle(ctx context.Context, input sfnmodels.UserDestroyInput) (*sfnmodels.UserDestroyOutput, error) {
	log.Infof("Executing Step %s", stepIdentifier)

	err := input_validator.ValidateInput(input)
	if err != nil {
		return nil, err
	}

	err = h.handleBadgeTiersDeletion(ctx, input.UserId)
	if err != nil {
		log.WithError(err).Errorf("Error happened when trying to delete user entries from %s table", badgeTiers)
		return nil, err
	}

	err = h.handleBadgeTierEmoteGroupIDsDeletion(ctx, input.UserId)
	if err != nil {
		log.WithError(err).Errorf("Error happened when trying to delete user entries from %s table", badgeTierEmoteGroupIDs)
		return nil, err
	}

	return &sfnmodels.UserDestroyOutput{
		UserId: input.UserId,
	}, nil
}

func (h *LambdaHandler) handleBadgeTiersDeletion(ctx context.Context, userID string) error {
	existingBadgeTiers, err := h.BadgeTiersDAO.GetAll(userID, true)
	if err != nil {
		return err
	}
	log.Infof("Found %d badgeTiers associated with the user %s", len(existingBadgeTiers), userID)

	for _, badgeTier := range existingBadgeTiers {
		threshold := badgeTier.Threshold
		log := log.WithFields(log.Fields{
			"Table":     badgeTiers,
			"UserID":    userID,
			"threshold": threshold,
			"badgeTier": badgeTier,
		})
		if h.Dryrun {
			log.Info("Dryrun, user's badgeTiers will not be deleted from Table")
		} else {
			log.Info("Real run! deleting user's badgeTiers")
			err = h.BadgeTiersDAO.Delete(userID, threshold)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func (h *LambdaHandler) handleBadgeTierEmoteGroupIDsDeletion(ctx context.Context, userID string) error {
	existingBadgeTierEmoteGroupIDs, err := h.BadgeTierEmoteGroupIDsDAO.Get(ctx, userID)
	if err != nil {
		return err
	}
	log.Infof("Found %d badgeTierEmoteGroupIDs associated with the user %s", len(existingBadgeTierEmoteGroupIDs), userID)

	for _, group := range existingBadgeTierEmoteGroupIDs {
		threshold := group.Threshold
		log := log.WithFields(log.Fields{
			"Table":     badgeTierEmoteGroupIDs,
			"UserID":    userID,
			"threshold": threshold,
			"group":     group,
		})
		if h.Dryrun {
			log.Info("Dryrun, user's badgeTierEmoteGroupID will not be deleted from Table")
		} else {
			log.Info("Real run! deleting user's badgeTierEmoteGroupID")
			err = h.BadgeTierEmoteGroupIDsDAO.Delete(ctx, userID, threshold)
			if err != nil {
				return err
			}
		}
	}

	return nil
}
