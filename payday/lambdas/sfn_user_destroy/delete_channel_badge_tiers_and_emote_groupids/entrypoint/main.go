package main

import (
	"context"
	"os"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/dynamo/badge_tier_emote_groupids"
	"code.justin.tv/commerce/payday/hystrix"
	delete_channel_badge_tiers_and_emote_groupids_handler "code.justin.tv/commerce/payday/lambdas/sfn_user_destroy/delete_channel_badge_tiers_and_emote_groupids/handler"
	sfnmodels "code.justin.tv/commerce/payday/sfn/models"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/codegangsta/cli"
)

var environment string
var dryrun bool
var local bool
var userID string

func main() {
	app := cli.NewApp()
	app.Name = "DeleteChannelBadgeTiersAndEmoteGroupIDs"

	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "environment, e",
			Value:       "staging",
			Usage:       "Runtime config to use. Options are: 'production', and 'staging'. Defaults to 'staging' if unset.",
			EnvVar:      "payday_APP_ENV",
			Destination: &environment,
		},
		cli.StringFlag{
			Name:        "userID",
			Usage:       "Runtime config to use. Only works if local is set to true.",
			Destination: &userID,
		},
		cli.BoolFlag{
			Name:        "dryrun",
			Usage:       "Pass in this flag to only show the entries to be deleted",
			Destination: &dryrun,
		},
		cli.BoolFlag{
			Name:        "local",
			Usage:       "Pass in this flag to run the handler directly instead of through lambda.Start()",
			Destination: &local,
		},
	}

	app.Action = createBackend

	err := app.Run(os.Args)
	if err != nil {
		panic(err)
	}
}

func createBackend(c *cli.Context) {
	err := config.Load(environment)
	if err != nil {
		log.WithError(err).Panicf("error loading config")
	}
	cfg := config.Get()

	sess, err := session.NewSession(&aws.Config{
		Region: aws.String(cfg.SFNRegion),
	})
	if err != nil {
		log.WithError(err).Panic("Could not start new aws session")
	}

	dynamoClientNoAudit := dynamo.NewClient(&dynamo.DynamoClientConfig{
		AwsRegion:   cfg.DynamoRegion,
		TablePrefix: cfg.DynamoTablePrefix,
	})

	lambdaHandler := delete_channel_badge_tiers_and_emote_groupids_handler.LambdaHandler{
		BadgeTiersDAO:             dynamo.NewBadgeTierDao(dynamoClientNoAudit),
		BadgeTierEmoteGroupIDsDAO: badge_tier_emote_groupids.NewBadgeTierEmoteGroupIDDAO(sess, cfg),
		Dryrun:                    dryrun,
	}

	if local {
		// override because local execution consistently takes longer than inside AWS
		hystrix.ConfigureTimeoutOverride(hystrix.GetBadgeTiersCommand, 400)
		hystrix.ConfigureTimeoutOverride(hystrix.BadgeTierEmoteGetGroupIds, 400)

		ctx, cancel := context.WithTimeout(context.Background(), time.Duration(2)*time.Minute)
		defer cancel()
		output, err := lambdaHandler.Handle(ctx, sfnmodels.UserDestroyInput{
			UserId: userID,
		})
		if err != nil {
			log.WithError(err).Error("Lambda Handler Execution Failed")
		}
		log.WithField("output", output).Info("Execution Output")
	} else {
		lambda.Start(lambdaHandler.Handle)
	}

}
