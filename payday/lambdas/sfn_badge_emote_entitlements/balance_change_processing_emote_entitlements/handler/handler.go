package handler

import (
	"context"
	"errors"

	"code.justin.tv/commerce/gogogadget/strings"
	bagetieremotes "code.justin.tv/commerce/payday/badgetiers/products/emotes"
	sfnmodels "code.justin.tv/commerce/payday/sfn/models"
	"code.justin.tv/commerce/payday/sfn/transaction_idempotency"
)

const (
	stepIdentifier = "entitleBadgeTierEmoteRewards"
)

type LambdaHandler struct {
	EmoteEntitler       bagetieremotes.Entitler          `inject:""`
	IdempotencyEnforcer transaction_idempotency.Enforcer `inject:""`
}

func validateInput(input sfnmodels.BitsUsageEntitlerInput) error {
	if strings.Blank(input.RequestingTwitchUserId) {
		return errors.New("missing RequestingTwitchUserId")
	}

	if strings.Blank(input.TargetTwitchUserId) {
		return errors.New("missing TargetTwitchUserId")
	}

	if input.BitsProcessed == 0 {
		return errors.New("missing BitsProcessed")
	}

	if input.TotalBitsToBroadcaster == 0 {
		return errors.New("missing TotalBitsToBroadcaster")
	}

	if strings.Blank(input.TransactionId) {
		return errors.New("missing TransactionId")
	}

	return nil
}

func (h *LambdaHandler) Handle(ctx context.Context, input sfnmodels.BitsUsageEntitlerInput) (*sfnmodels.EmoteEntitlerOutput, error) {
	if input.IsAnonymous {
		return nil, nil
	}
	err := validateInput(input)
	if err != nil {
		return nil, err
	}

	var cachedOutput sfnmodels.EmoteEntitlerOutput

	transactionOk, err := h.IdempotencyEnforcer.Enforce(ctx, input.TransactionId, stepIdentifier, input, &cachedOutput)
	if err != nil {
		return nil, err
	}

	if !transactionOk {
		return &cachedOutput, nil
	}

	output, err := h.handle(ctx, input)

	h.IdempotencyEnforcer.Finalize(ctx, input.TransactionId, stepIdentifier, output, err)

	return output, err
}

func (h *LambdaHandler) handle(ctx context.Context, input sfnmodels.BitsUsageEntitlerInput) (*sfnmodels.EmoteEntitlerOutput, error) {
	emoticons, err := h.EmoteEntitler.Entitle(
		ctx,
		input.RequestingTwitchUserId,
		input.TargetTwitchUserId,
		int64(input.BitsProcessed),
		int64(input.TotalBitsToBroadcaster),
		input.TransactionId)

	if err != nil {
		return nil, err
	}

	return &sfnmodels.EmoteEntitlerOutput{
		Emotes: emoticons,
	}, nil
}
