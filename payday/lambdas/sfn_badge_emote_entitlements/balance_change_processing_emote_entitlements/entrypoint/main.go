package main

import (
	"code.justin.tv/chat/rediczar/redefault"
	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/audit"
	"code.justin.tv/commerce/payday/backend/clients/ripley"
	"code.justin.tv/commerce/payday/backend/clients/user"
	"code.justin.tv/commerce/payday/backend/metrics"
	"code.justin.tv/commerce/payday/badgetiers/fetcher"
	"code.justin.tv/commerce/payday/badgetiers/logician"
	badgetieremotes "code.justin.tv/commerce/payday/badgetiers/products/emotes"
	"code.justin.tv/commerce/payday/cache/chatbadges"
	payday_ripley "code.justin.tv/commerce/payday/cache/ripley"
	user_service_cache "code.justin.tv/commerce/payday/cache/userservice"
	"code.justin.tv/commerce/payday/clients/mako"
	payday_redis "code.justin.tv/commerce/payday/clients/redis"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/dynamo/badge_tier_emote_groupids"
	"code.justin.tv/commerce/payday/dynamo/balance_change_processing"
	"code.justin.tv/commerce/payday/iam"
	paydayLambda "code.justin.tv/commerce/payday/lambda"
	"code.justin.tv/commerce/payday/lambdas/sfn_badge_emote_entitlements/balance_change_processing_emote_entitlements/handler"
	partner_manager "code.justin.tv/commerce/payday/partner"
	"code.justin.tv/commerce/payday/s3"
	"code.justin.tv/commerce/payday/sfn/transaction_idempotency"
	paydaySNS "code.justin.tv/commerce/payday/sns"
	"code.justin.tv/commerce/payday/userservice"
	"code.justin.tv/commerce/payday/utils/experiments"
	"code.justin.tv/foundation/twitchclient"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/facebookgo/inject"
)

type backend struct {
	Handler *handler.LambdaHandler `inject:""`
}

func main() {
	b := &backend{}

	cfg, err := config.LoadFromEnvVar()
	if err != nil {
		logrus.WithError(err).Panicf("error loading config")
	}

	sess, err := session.NewSession(&aws.Config{
		Region: aws.String(cfg.DynamoRegion),
	})
	if err != nil {
		logrus.WithError(err).Panic("Could not start new aws session")
	}

	redisClient := redefault.NewClusterClient(cfg.Redis.Cluster, &redefault.ClusterOpts{
		ReadOnly: true,
	})

	metricsLogger, err := metrics.NewMetricsLogger(cfg)
	if err != nil {
		logrus.WithError(err).Panic("Could not start up metrics loggers")
	}

	noopStatter, err := statsd.NewNoopClient()
	if err != nil {
		logrus.WithError(err).Panic("Could not create noop statter")
	}

	s3Client := s3.NewFromDefaultConfig()
	lambdaClient := paydayLambda.NewFromDefaultConfig()
	iamClient := iam.NewFromDefaultConfig()
	auditSetup := audit.NewAuditSetup(s3Client, lambdaClient, iamClient)

	dynamoClient := dynamo.NewClientWithAudit(&dynamo.DynamoClientConfig{
		AwsRegion:   cfg.DynamoRegion,
		TablePrefix: cfg.DynamoTablePrefix,
	}, auditSetup, &dynamo.DynamoAuditConfig{
		RecordsS3Bucket:      cfg.AuditRecordsS3Bucket,
		LambdaMemorySizeMB:   cfg.AuditLambdaMemorySizeMB,
		LambdaTimeoutSec:     cfg.AuditLambdaTimeoutSec,
		LambdaRoleName:       cfg.AuditLambdaRoleName,
		LambdaCodeS3Bucket:   cfg.AuditLambdaCodeS3Bucket,
		LambdaEventBatchSize: cfg.AuditLambdaEventBatchSize,
	})

	clientHttpTransport := twitchclient.TransportConf{}

	deps := []*inject.Object{
		{Value: b},
		{Value: &cfg},

		{Value: transaction_idempotency.NewIdempotencyEnforcer()},

		// users
		{Value: userservice.NewFetcher()},
		{Value: user_service_cache.NewCache()},
		{Value: userservice.NewRetriever()},

		// entitler
		{Value: badgetieremotes.NewEntitler()},
		{Value: logician.NewChatBadgeLogician()},
		{Value: fetcher.NewFetcher()},
		{Value: payday_ripley.NewCache()},
		{Value: chatbadges.NewCache()},

		// experiments
		{Value: experiments.NewFetcher()},

		// dynamo
		{Value: balance_change_processing.NewBalanceChangeProcessingDAO(sess, cfg)},
		{Value: dynamo.NewBadgeTierDao(dynamoClient)},
		{Value: badge_tier_emote_groupids.NewBadgeTierEmoteGroupIDDAO(sess, cfg)},

		// clients
		{Value: redisClient, Name: "clusterClient"},
		{Value: payday_redis.NewClient(cfg), Name: "redisClient"},
		{Value: metricsLogger},
		{Value: user.NewClient(cfg, clientHttpTransport, noopStatter)},
		{Value: &partner_manager.PartnerManager{RipleyClient: ripley.NewClient(cfg)}},
		{Value: paydaySNS.NewSnsClient("us-west-2"), Name: "usWestSNSClient"},
		{Value: mako.NewClient(cfg)},
		{Value: s3.NewFromDefaultConfig()},
	}

	var graph inject.Graph
	err = graph.Provide(deps...)
	if err != nil {
		logrus.WithError(err).Panic("error calling graph.Provide")
	}

	err = graph.Populate()
	if err != nil {
		logrus.WithError(err).Panic("error calling graph.Populate")
	}

	lambda.Start(b.Handler.Handle)
}
