package handler

import (
	"context"
	"encoding/json"
	"time"

	pubclient "code.justin.tv/chat/pubsub-go-pubclient/client"
	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/hystrix"
	cmds "code.justin.tv/commerce/payday/hystrix"
	"code.justin.tv/commerce/payday/metrics"
	"code.justin.tv/commerce/payday/middleware"
	"code.justin.tv/commerce/payday/models"
	sfnmodels "code.justin.tv/commerce/payday/sfn/models"
	"code.justin.tv/commerce/payday/sfn/transaction_idempotency"
	"code.justin.tv/commerce/payday/sqs/handlers"
	"code.justin.tv/commerce/payday/userservice"
	"github.com/gofrs/uuid"
)

const (
	stepIdentifier = "entitlementPubsub"

	//Max message age in seconds
	MaxMessageAge = 45.0
	//Need two namespaces to not reuse the same UUID across pubsub topics.
	uuidNamespaceV1 = "c0400ee6-bc6e-583e-a1b5-67e698524de0"
	uuidNamespaceV2 = "17cd2e45-23b3-48b9-839e-7f4b2ccdab81"
)

type LambdaHandler struct {
	UserServiceFetcher  userservice.Fetcher              `inject:""`
	PubClient           pubclient.PubClient              `inject:""`
	Statter             metrics.Statter                  `inject:""`
	TransactionsDao     dynamo.ITransactionsDao          `inject:""`
	IdempotencyEnforcer transaction_idempotency.Enforcer `inject:""`
}

func validateInput(input sfnmodels.BitsUsageEntitlerInput) error {
	if strings.Blank(input.RequestingTwitchUserId) {
		return errors.New("missing RequestingTwitchUserId")
	}

	if strings.Blank(input.TargetTwitchUserId) {
		return errors.New("missing TargetTwitchUserId")
	}

	if input.BitsProcessed == 0 {
		return errors.New("missing BitsProcessed")
	}

	if input.TotalBitsToBroadcaster == 0 {
		return errors.New("missing TotalBitsToBroadcaster")
	}

	if strings.Blank(input.TransactionId) {
		return errors.New("missing TransactionId")
	}

	if !input.ShouldPubsub {
		return errors.New("hit pubsub lambda when ShouldPubsub=false")
	}

	return nil
}

func (h *LambdaHandler) Handle(ctx context.Context, parallelInput sfnmodels.EntitlementPubsubInput) error {
	input := parallelInput.OriginalInput

	err := validateInput(input)
	if err != nil {
		return err
	}

	transactionOk, err := h.IdempotencyEnforcer.Enforce(ctx, input.TransactionId, stepIdentifier, input, nil)
	if err != nil {
		return err
	}

	if !transactionOk {
		return nil
	}

	err = h.handle(ctx, parallelInput)

	h.IdempotencyEnforcer.Finalize(ctx, input.TransactionId, stepIdentifier, nil, err)

	return err
}

func (h *LambdaHandler) handle(ctx context.Context, parallelInput sfnmodels.EntitlementPubsubInput) error {
	input := parallelInput.OriginalInput

	log := logrus.WithFields(logrus.Fields{
		"transactionID":   input.TransactionId,
		"transactionType": input.TransactionType,
		"userID":          input.RequestingTwitchUserId,
		"channelID":       input.TargetTwitchUserId,
		"bitsUsed":        int64(input.BitsProcessed),
		"bitsTotal":       int64(input.TotalBitsToBroadcaster),
	})

	inputExtraContext := input.ExtraContext
	var extraContext models.FabsGiveBitsExtraContext
	err := json.Unmarshal([]byte(inputExtraContext), &extraContext)
	if err == nil {
		ctx = context.WithValue(ctx, middleware.ClientIDContextKey, extraContext.ClientID)
	} else {
		log.Warnf("Could not get extra context from give bits event %v. Error was %v", input, err)
	}

	secondsSince := time.Since(input.TimeOfEvent).Seconds()

	if secondsSince > MaxMessageAge {
		log.WithField("input", input).Warn("Encountered old pubsub message skipping message")
		h.Statter.Inc("SkippedPubSubMessage", 1)
		return nil
	}

	err = h.sendMessage(ctx, parallelInput)
	if err != nil {
		return err
	} else {
		log.Info("Sent pubsub message successfully for entitlements")
	}

	return nil
}

func (h *LambdaHandler) sendMessage(ctx context.Context, parallelInput sfnmodels.EntitlementPubsubInput) error {
	input := parallelInput.OriginalInput
	log := logrus.WithFields(logrus.Fields{
		"transactionID":   input.TransactionId,
		"transactionType": input.TransactionType,
		"userID":          input.RequestingTwitchUserId,
		"channelID":       input.TargetTwitchUserId,
		"bitsUsed":        int64(input.BitsProcessed),
		"bitsTotal":       int64(input.TotalBitsToBroadcaster),
	})

	messagesChannel := make(chan *dynamo.Transaction, 1)
	err := hystrix.Do(cmds.GetTransactionsCommand, func() error {
		message, err := h.TransactionsDao.Get(input.TransactionId)

		if err != nil {
			return err
		}

		messagesChannel <- message
		return nil
	}, nil)

	if err != nil {
		log.WithError(err).Error("could not retrieve transaction from dynamo")
		return err
	}

	message := <-messagesChannel

	if message == nil {
		log.Error("No dynamo record found for transaction")
		return errors.New("no dynamo record found for transaction")
	}

	if strings.BlankP(message.TmiMessage) {
		log.Error("post process has not completed yet for transaction")
		return errors.New("post process has not completed yet for transaction")
	}

	// We don't want to send an external pubsub message for Microcheering experiences
	if message.IsMicroCheer {
		return nil
	}

	sender, recipient, err := h.getUserNames(ctx, parallelInput)
	if err != nil {
		log.WithError(err).Error("could not retrieve usernames for channel and user")
		return err
	}

	pubsubMessage := models.PubsubTransactionMessageV1{
		UserName:             sender,
		ChannelName:          recipient,
		UserId:               input.RequestingTwitchUserId,
		ChannelId:            input.TargetTwitchUserId,
		Time:                 input.TimeOfEvent,
		ChatMessage:          pointers.StringOrDefault(message.TmiMessage, ""),
		BitsUsed:             handlers.BitsUsedWithSponsor(input.BitsProcessed, input.SponsoredAmounts),
		TotalBitsUsed:        input.TotalBitsToBroadcaster,
		Context:              "cheer",
		BadgeEntitlement:     parallelInput.BadgeEntitlements.Badge,
		BadgeTierEntitlement: makeBadgeTierEntitlement(parallelInput),
	}

	// V1 Messages don't support anon, so we can't send them.
	// Additionally, v1 is to be deprecated in time, so we care less if this message is sent (don't return errors)
	if !input.IsAnonymous {
		go h.sendV1Message(context.Background(), parallelInput, pubsubMessage)
	}

	return h.sendV2Message(ctx, parallelInput, pubsubMessage.ConvertToV2(input.IsAnonymous))
}

func makeBadgeTierEntitlement(input sfnmodels.EntitlementPubsubInput) *models.BadgeTierEntitlement {
	badgeTierEntitlement := models.BadgeTierEntitlement{}

	if input.EmoteEntitlements.Emotes != nil {
		badgeTierEntitlement.Emoticons = *input.EmoteEntitlements.Emotes
	}

	if input.BadgeEntitlements.Badge != nil {
		badgeTierEntitlement.Badge = *input.BadgeEntitlements.Badge
	}

	return &badgeTierEntitlement
}

func (h *LambdaHandler) getUserNames(ctx context.Context, parallelInput sfnmodels.EntitlementPubsubInput) (senderUsername, recipientUsername string, err error) {
	gb := parallelInput.OriginalInput

	log := logrus.WithFields(logrus.Fields{
		"transactionID":   gb.TransactionId,
		"transactionType": gb.TransactionType,
		"userID":          gb.RequestingTwitchUserId,
		"channelID":       gb.TargetTwitchUserId,
		"bitsUsed":        int64(gb.BitsProcessed),
		"bitsTotal":       int64(gb.TotalBitsToBroadcaster),
	})

	userId := gb.RequestingTwitchUserId
	sender, err := h.UserServiceFetcher.Fetch(ctx, userId)
	if err != nil {
		input := "Failed looking up user via user service."
		log.WithError(err).Error(input)
		return "", "", errors.Notef(err, input)
	}

	if sender == nil || sender.Login == nil {
		input := "Could not find user in user service lookup for sender id"
		log.Error(input)
		return "", "", errors.New(input)
	}

	recipient, err := h.UserServiceFetcher.Fetch(ctx, gb.TargetTwitchUserId)
	if err != nil {
		input := "Failed looking up channel via user service"
		log.WithError(err).Error(input)
		return "", "", errors.Notef(err, input)
	}
	if recipient == nil || recipient.Login == nil {
		input := "Could not find channel in user service lookup for recipient id"
		log.Error(input)
		return "", "", errors.New(input)
	}

	return *sender.Login, *recipient.Login, nil
}

func (h *LambdaHandler) sendV1Message(ctx context.Context, parallelInput sfnmodels.EntitlementPubsubInput, pubsubMessage models.PubsubTransactionMessageV1) {
	input := parallelInput.OriginalInput

	log := logrus.WithFields(logrus.Fields{
		"transactionID":   input.TransactionId,
		"transactionType": input.TransactionType,
		"userID":          input.RequestingTwitchUserId,
		"channelID":       input.TargetTwitchUserId,
		"bitsUsed":        int64(input.BitsProcessed),
		"bitsTotal":       int64(input.TotalBitsToBroadcaster),
	})

	topics := []string{models.BitEventsPrivatePubsubTopicV1 + "." + input.TargetTwitchUserId}
	uuidNamespace, err := uuid.FromString(uuidNamespaceV1)
	if err != nil {
		// Should never happen
		uuidNamespaceUUID, err := uuid.NewV4()
		if err != nil {
			// Should never happen even more

			return
		}
		uuidNamespace = uuidNamespaceUUID
	}

	inputId := uuid.NewV5(uuidNamespace, input.TransactionId)

	privMsg := models.PrivateBitsMessage{
		Data:        pubsubMessage,
		Version:     models.CurrentPrivateMessageVersion,
		MessageType: models.BitEventMessageType,
		MessageId:   inputId.String(),
	}

	marshaled, err := json.Marshal(privMsg)

	if err != nil {
		log.WithError(err).WithField("pubsubMessage", pubsubMessage).Error("could not marshal pubsub message")
		return
	}

	err = h.PubClient.Publish(ctx, topics, string(marshaled), nil)
	if err != nil {
		log.WithError(err).WithField("marshaledMessage", string(marshaled)).Error("could not publish message to pubsub")
		return
	}
}

func (h *LambdaHandler) sendV2Message(ctx context.Context, parallelInput sfnmodels.EntitlementPubsubInput, pubsubMessage models.PubsubTransactionMessageV2) error {
	input := parallelInput.OriginalInput

	log := logrus.WithFields(logrus.Fields{
		"transactionID":   input.TransactionId,
		"transactionType": input.TransactionType,
		"userID":          input.RequestingTwitchUserId,
		"channelID":       input.TargetTwitchUserId,
		"bitsUsed":        int64(input.BitsProcessed),
		"bitsTotal":       int64(input.TotalBitsToBroadcaster),
	})

	topics := []string{models.BitEventsPrivatePubsubTopicV2 + "." + input.TargetTwitchUserId}
	uuidNamespace, err := uuid.FromString(uuidNamespaceV2)
	if err != nil {
		// Should never happen
		uuidNamespaceUUID, err := uuid.NewV4()
		if err != nil {
			// Should never happen even more
			return errors.Notef(err, "Error generating UUID")
		}
		uuidNamespace = uuidNamespaceUUID
	}

	inputId := uuid.NewV5(uuidNamespace, input.TransactionId)

	privMsg := models.PrivateBitsMessage{
		Data:        pubsubMessage,
		Version:     models.CurrentPrivateMessageVersion,
		MessageType: models.BitEventMessageType,
		MessageId:   inputId.String(),
	}

	marshaled, err := json.Marshal(privMsg)

	if err != nil {
		log.WithError(err).WithField("pubsubMessage", pubsubMessage).Error("could not marshal pubsub message")
		return err
	}

	err = h.PubClient.Publish(ctx, topics, string(marshaled), nil)
	if err != nil {
		log.WithError(err).WithField("marshaledMessage", string(marshaled)).Error("could not publish message to pubsub")
		return err
	}

	return nil
}
