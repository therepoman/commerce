# Staging
# 1 machine per AZ
# three AZs

# elastic network interfaces & ip addresses
resource "aws_eip" "staging-nat-2a" {
  vpc = true
}

resource "aws_eip" "staging-nat-2b" {
  vpc = true
}

resource "aws_eip" "staging-nat-2c" {
  vpc = true
}

resource "aws_subnet" "staging_subnet_a" {
  vpc_id                  = var.vpc_id
  availability_zone       = "us-west-2a"
  cidr_block              = var.cidr_stage_a
  map_public_ip_on_launch = false

  tags = {
    Name  = "${var.name}-staging-private-2a"
    Owner = var.owner
  }
}

resource "aws_nat_gateway" "staging_subnet_a_nat" {
  allocation_id = aws_eip.staging-nat-2a.id
  subnet_id     = aws_subnet.pub-2a.id
}

module "staging_route_table_a" {
  source                           = "./modules/route_table"
  az                               = "a"
  nat_gateway_id                   = aws_nat_gateway.staging_subnet_a_nat.id
  subnet_id                        = aws_subnet.staging_subnet_a.id
  twitch_aws_peering_connection_id = aws_vpc_peering_connection.twitch-aws-prod.id
  vpc_id                           = var.vpc_id
  stage                            = "staging"

  pantheon_cidr_block            = "10.201.180.0/22"
  pantheon_peering_connection_id = "pcx-06cd2121bab9160f7"

  prism_cidr_block            = "10.204.244.0/22"
  prism_peering_connection_id = "pcx-07c9371fc88550a4e"

  petozi_cidr_block            = "10.205.176.0/22"
  petozi_peering_connection_id = "pcx-05e7837c9a6ad8dc3"

  pagle_cidr_block            = "10.205.68.0/22"
  pagle_peering_connection_id = "pcx-044c5ed82d2f197f1"

  pachter_cidr_block            = "10.205.188.0/22"
  pachter_peering_connection_id = aws_vpc_peering_connection.pachter-devo-peering.id

  upload_service_cidr_block            = "10.205.200.0/22"
  upload_service_peering_connection_id = aws_vpc_peering_connection.upload-service-devo-peering.id

  chat_cidr_block            = "10.193.192.0/18"
  chat_peering_connection_id = aws_vpc_peering_connection.chat-peering.id

  pachinko_cidr_block            = "10.204.248.0/22"
  pachinko_peering_connection_id = aws_vpc_peering_connection.pachinko-devo-peering.id

  visage_cidr_block            = "10.205.220.0/22"
  visage_peering_connection_id = aws_vpc_peering_connection.visage-devo-peering.id

  poliwag_cidr_block            = "10.206.152.0/22"
  poliwag_peering_connection_id = "pcx-0ff5d2b07e6e71384"

  mako_cidr_block            = "10.201.92.0/22"
  mako_peering_connection_id = "pcx-03368ca89c31e8a5a"

  revenue_reporting_worker_cidr_block            = "10.206.208.0/22"
  revenue_reporting_worker_peering_connection_id = aws_vpc_peering_connection.revenue-reporting-worker-devo-peering.id

  amp_cidr_block    = "172.29.0.0/16"
  amp_connection_id = "pcx-0c21dfd1b1769e928"

  helix_cidr_block    = "10.205.16.0/22"
  helix_connection_id = "pcx-0500e0e2119881d4d"
}

resource "aws_subnet" "staging_subnet_b" {
  vpc_id                  = var.vpc_id
  availability_zone       = "us-west-2b"
  cidr_block              = var.cidr_stage_b
  map_public_ip_on_launch = false

  tags = {
    Name  = "${var.name}-staging-private-2b"
    Owner = var.owner
  }
}

resource "aws_nat_gateway" "staging_subnet_b_nat" {
  allocation_id = aws_eip.staging-nat-2b.id
  subnet_id     = aws_subnet.pub-2b.id
}

module "staging_route_table_b" {
  source                           = "./modules/route_table"
  az                               = "b"
  nat_gateway_id                   = aws_nat_gateway.staging_subnet_b_nat.id
  subnet_id                        = aws_subnet.staging_subnet_b.id
  twitch_aws_peering_connection_id = aws_vpc_peering_connection.twitch-aws-prod.id
  vpc_id                           = var.vpc_id
  stage                            = "staging"

  pantheon_cidr_block            = "10.201.180.0/22"
  pantheon_peering_connection_id = "pcx-06cd2121bab9160f7"

  prism_cidr_block            = "10.204.244.0/22"
  prism_peering_connection_id = "pcx-07c9371fc88550a4e"

  petozi_cidr_block            = "10.205.176.0/22"
  petozi_peering_connection_id = "pcx-05e7837c9a6ad8dc3"

  pachter_cidr_block            = "10.205.188.0/22"
  pachter_peering_connection_id = aws_vpc_peering_connection.pachter-devo-peering.id

  upload_service_cidr_block            = "10.205.200.0/22"
  upload_service_peering_connection_id = aws_vpc_peering_connection.upload-service-devo-peering.id

  chat_cidr_block            = "10.193.192.0/18"
  chat_peering_connection_id = aws_vpc_peering_connection.chat-peering.id

  pagle_cidr_block            = "10.205.68.0/22"
  pagle_peering_connection_id = "pcx-044c5ed82d2f197f1"

  pachinko_cidr_block            = "10.204.248.0/22"
  pachinko_peering_connection_id = aws_vpc_peering_connection.pachinko-devo-peering.id

  visage_cidr_block            = "10.205.220.0/22"
  visage_peering_connection_id = aws_vpc_peering_connection.visage-devo-peering.id

  poliwag_cidr_block            = "10.206.152.0/22"
  poliwag_peering_connection_id = "pcx-0ff5d2b07e6e71384"

  mako_cidr_block            = "10.201.92.0/22"
  mako_peering_connection_id = "pcx-03368ca89c31e8a5a"

  revenue_reporting_worker_cidr_block            = "10.206.208.0/22"
  revenue_reporting_worker_peering_connection_id = aws_vpc_peering_connection.revenue-reporting-worker-devo-peering.id

  amp_cidr_block    = "172.29.0.0/16"
  amp_connection_id = "pcx-0c21dfd1b1769e928"

  helix_cidr_block    = "10.205.16.0/22"
  helix_connection_id = "pcx-0500e0e2119881d4d"
}

resource "aws_subnet" "staging_subnet_c" {
  vpc_id                  = var.vpc_id
  availability_zone       = "us-west-2c"
  cidr_block              = var.cidr_stage_c
  map_public_ip_on_launch = false

  tags = {
    Name  = "${var.name}-staging-private-2c"
    Owner = var.owner
  }
}

resource "aws_nat_gateway" "staging_subnet_c_nat" {
  allocation_id = aws_eip.staging-nat-2c.id
  subnet_id     = aws_subnet.pub-2c.id
}

module "staging_route_table_c" {
  source                           = "./modules/route_table"
  az                               = "c"
  nat_gateway_id                   = aws_nat_gateway.staging_subnet_c_nat.id
  subnet_id                        = aws_subnet.staging_subnet_c.id
  twitch_aws_peering_connection_id = aws_vpc_peering_connection.twitch-aws-prod.id
  vpc_id                           = var.vpc_id
  stage                            = "staging"

  pantheon_cidr_block            = "10.201.180.0/22"
  pantheon_peering_connection_id = "pcx-06cd2121bab9160f7"

  prism_cidr_block            = "10.204.244.0/22"
  prism_peering_connection_id = "pcx-07c9371fc88550a4e"

  petozi_cidr_block            = "10.205.176.0/22"
  petozi_peering_connection_id = "pcx-05e7837c9a6ad8dc3"

  pachter_cidr_block            = "10.205.188.0/22"
  pachter_peering_connection_id = aws_vpc_peering_connection.pachter-devo-peering.id

  upload_service_cidr_block            = "10.205.200.0/22"
  upload_service_peering_connection_id = aws_vpc_peering_connection.upload-service-devo-peering.id

  chat_cidr_block            = "10.193.192.0/18"
  chat_peering_connection_id = aws_vpc_peering_connection.chat-peering.id

  pagle_cidr_block            = "10.205.68.0/22"
  pagle_peering_connection_id = "pcx-044c5ed82d2f197f1"

  pachinko_cidr_block            = "10.204.248.0/22"
  pachinko_peering_connection_id = aws_vpc_peering_connection.pachinko-devo-peering.id

  visage_cidr_block            = "10.205.220.0/22"
  visage_peering_connection_id = aws_vpc_peering_connection.visage-devo-peering.id

  poliwag_cidr_block            = "10.206.152.0/22"
  poliwag_peering_connection_id = "pcx-0ff5d2b07e6e71384"

  mako_cidr_block            = "10.201.92.0/22"
  mako_peering_connection_id = "pcx-03368ca89c31e8a5a"

  revenue_reporting_worker_cidr_block            = "10.206.208.0/22"
  revenue_reporting_worker_peering_connection_id = aws_vpc_peering_connection.revenue-reporting-worker-devo-peering.id

  amp_cidr_block    = "172.29.0.0/16"
  amp_connection_id = "pcx-0c21dfd1b1769e928"

  helix_cidr_block    = "10.205.16.0/22"
  helix_connection_id = "pcx-0500e0e2119881d4d"
}
