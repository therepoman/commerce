module "dynamo_backup-lambda" {
  source = "./modules/dynamo_backup_lambda"
}

module "payday_test_channels_autoscaling_policy" {
  source     = "./modules/dynamo_db_autoscaling_policy"
  table_name = "test_channels"
}

module "payday_test_channels_backup" {
  source                   = "./modules/scheduled_dynamo_backup_event"
  dynamo_backup_lambda_arn = module.dynamo_backup-lambda.lambda_arn
  table_name               = "test_channels"
}

module "payday_prod_images_autoscaling_policy" {
  source             = "./modules/dynamo_db_autoscaling_policy"
  table_name         = "prod_images"
  min_read_capacity  = "50"
  min_write_capacity = "50"
  read_utilization   = "70"
  write_utilization  = "70"
}

module "payday_prod_images_backup" {
  source                   = "./modules/scheduled_dynamo_backup_event"
  dynamo_backup_lambda_arn = module.dynamo_backup-lambda.lambda_arn
  table_name               = "prod_images"
}

module "payday_prod_badge_tiers_autoscaling_policy" {
  source             = "./modules/dynamo_db_autoscaling_policy"
  table_name         = "prod_badge-tiers"
  min_read_capacity  = "2000"
  min_write_capacity = "25"
  read_utilization   = "60"
  write_utilization  = "70"
}

module "payday_prod_badge_tiers_backup" {
  source                   = "./modules/scheduled_dynamo_backup_event"
  dynamo_backup_lambda_arn = module.dynamo_backup-lambda.lambda_arn
  table_name               = "prod_badge-tiers"
}

module "payday_prod_channels_autoscaling_policy" {
  source            = "./modules/dynamo_db_autoscaling_policy"
  table_name        = "prod_channels"
  min_read_capacity = "2000"
  read_utilization  = "60"
  write_utilization = "70"
}

module "payday_prod_channels_backup" {
  source                   = "./modules/scheduled_dynamo_backup_event"
  dynamo_backup_lambda_arn = module.dynamo_backup-lambda.lambda_arn
  table_name               = "prod_channels"
}

module "payday_prod_transactions_autoscaling_policy" {
  source             = "./modules/dynamo_db_autoscaling_policy"
  table_name         = "prod_transactions"
  min_read_capacity  = "1500"
  min_write_capacity = "100"
  read_utilization   = "70"
  write_utilization  = "70"
}

module "payday_prod_transactions_backup" {
  source                   = "./modules/scheduled_dynamo_backup_event"
  dynamo_backup_lambda_arn = module.dynamo_backup-lambda.lambda_arn
  table_name               = "prod_transactions"
}

module "payday_prod_users_autoscaling_policy" {
  source             = "./modules/dynamo_db_autoscaling_policy"
  table_name         = "prod_users"
  min_read_capacity  = "2000"
  min_write_capacity = "50"
  read_utilization   = "70"
  write_utilization  = "70"
}

module "payday_prod_users_backup" {
  source                   = "./modules/scheduled_dynamo_backup_event"
  dynamo_backup_lambda_arn = module.dynamo_backup-lambda.lambda_arn
  table_name               = "prod_users"
}

module "payday_prod_extension_transactions_autoscaling_policy" {
  source             = "./modules/dynamo_db_autoscaling_policy"
  table_name         = "prod_extension_transactions"
  min_read_capacity  = "100"
  min_write_capacity = "100"
  read_utilization   = "70"
  write_utilization  = "70"
}

module "payday_prod_extension_transactions_backup" {
  source                   = "./modules/scheduled_dynamo_backup_event"
  dynamo_backup_lambda_arn = module.dynamo_backup-lambda.lambda_arn
  table_name               = "prod_extension_transactions"
}

module "payday_prod_actions_autoscaling_policy" {
  source             = "./modules/dynamo_db_autoscaling_policy"
  table_name         = "prod_actions"
  min_read_capacity  = "1000"
  min_write_capacity = "25"
  read_utilization   = "70"
  write_utilization  = "70"
}

module "payday_prod_actions_backup" {
  source                   = "./modules/scheduled_dynamo_backup_event"
  dynamo_backup_lambda_arn = module.dynamo_backup-lambda.lambda_arn
  table_name               = "prod_actions"
}

module "payday_prod_sponsored_cheermote_campaigns_autoscaling_policy" {
  source             = "./modules/dynamo_db_autoscaling_policy"
  table_name         = "prod_sponsored_cheermote_campaigns"
  min_read_capacity  = "200"
  min_write_capacity = "25"
  read_utilization   = "70"
  write_utilization  = "70"
}

module "payday_prod_sponsored_cheermote_campaigns_backup" {
  source                   = "./modules/scheduled_dynamo_backup_event"
  dynamo_backup_lambda_arn = module.dynamo_backup-lambda.lambda_arn
  table_name               = "prod_sponsored_cheermote_campaigns"
}

module "payday_prod_sponsored_cheermote_channel_statuses_autoscaling_policy" {
  source             = "./modules/dynamo_db_autoscaling_policy"
  table_name         = "prod_sponsored_cheermote_channel_statuses"
  min_read_capacity  = "25"
  min_write_capacity = "25"
  read_utilization   = "70"
  write_utilization  = "70"
}

module "payday_prod_sponsored_cheermote_channel_statuses_backup" {
  source                   = "./modules/scheduled_dynamo_backup_event"
  dynamo_backup_lambda_arn = module.dynamo_backup-lambda.lambda_arn
  table_name               = "prod_sponsored_cheermote_channel_statuses"
}

module "payday_prod_sponsored_cheermote_user_statuses_autoscaling_policy" {
  source             = "./modules/dynamo_db_autoscaling_policy"
  table_name         = "prod_sponsored_cheermote_user_statuses"
  min_read_capacity  = "1000"
  min_write_capacity = "1000"
  read_utilization   = "70"
  write_utilization  = "70"
}

module "payday_prod_sponsored_cheermote_user_statuses_backup" {
  source                   = "./modules/scheduled_dynamo_backup_event"
  dynamo_backup_lambda_arn = module.dynamo_backup-lambda.lambda_arn
  table_name               = "prod_sponsored_cheermote_user_statuses"
}

module "payday_prod_admin_job_tasks_autoscaling_policy" {
  source             = "./modules/dynamo_db_autoscaling_policy"
  table_name         = "prod_admin-job-tasks"
  read_utilization   = "70"
  write_utilization  = "70"
  min_read_capacity  = "1000"
  min_write_capacity = "1000"
}

module "payday_prod_admin_job_tasks_backup" {
  source                   = "./modules/scheduled_dynamo_backup_event"
  dynamo_backup_lambda_arn = module.dynamo_backup-lambda.lambda_arn
  table_name               = "prod_admin-job-tasks"
}

module "payday_prod_admin_jobs_autoscaling_policy" {
  source             = "./modules/dynamo_db_autoscaling_policy"
  table_name         = "prod_admin-jobs"
  read_utilization   = "70"
  write_utilization  = "70"
  min_read_capacity  = "100"
  min_write_capacity = "100"
}

module "payday_prod_admin_jobs_backup" {
  source                   = "./modules/scheduled_dynamo_backup_event"
  dynamo_backup_lambda_arn = module.dynamo_backup-lambda.lambda_arn
  table_name               = "prod_admin-jobs"
}

module "payday_prod_leaderboard_badge_holders_autoscaling_policy" {
  source             = "./modules/dynamo_db_autoscaling_policy"
  table_name         = "prod_leaderboard_badge_holders"
  read_utilization   = "70"
  write_utilization  = "70"
  min_read_capacity  = "100"
  min_write_capacity = "100"
}

module "payday_prod_leaderboard_badge_holders_backup" {
  source                   = "./modules/scheduled_dynamo_backup_event"
  dynamo_backup_lambda_arn = module.dynamo_backup-lambda.lambda_arn
  table_name               = "prod_leaderboard_badge_holders"
}
