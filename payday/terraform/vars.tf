variable "r53" {
  description = "Route 53 ID to the Twitch web AWS account as an override."
  default = "ZRG00SM48517Z"
}

variable "vpc_id" {
  description = "The ID of the Payday VPC"
  default     = "vpc-4cfeb629"
}