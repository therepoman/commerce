resource "aws_sns_topic" "twitch_fulfillment_service_prod_topic" {
  name = "twitch-fulfilment-service-prod"
}

data "aws_iam_policy_document" "prod_twitch_fulfillment_policy_documents" {
  statement {
    actions = ["sns:Subscribe","sns:Publish"]
    effect = "Allow"
    principals {
      identifiers = ["arn:aws:iam::894760684893:root","arn:aws:iam::180265471281:root"]
      type = "AWS"
    }
    resources = [aws_sns_topic.twitch_fulfillment_service_prod_topic.arn]
  }
}

resource "aws_sns_topic_policy" "prod_twitch_fulfillment_policy" {
  arn = aws_sns_topic.twitch_fulfillment_service_prod_topic.arn
  policy = data.aws_iam_policy_document.prod_twitch_fulfillment_policy_documents.json
}

resource "aws_sns_topic" "twitch_fulfillment_service_staging_topic" {
  name = "twitch-fulfilment-service-staging"
}

data "aws_iam_policy_document" "staging_twitch_fulfillment_policy_document" {
  statement {
    actions = ["sns:Subscribe","sns:Publish"]
    effect = "Allow"
    principals {
      identifiers = ["arn:aws:iam::603641476712:root","arn:aws:iam::106728537780:root"]
      type = "AWS"
    }
    resources = [aws_sns_topic.twitch_fulfillment_service_staging_topic.arn]
  }
}

resource "aws_sns_topic_policy" "staging_twitch_fulfillment_policy" {
  arn = aws_sns_topic.twitch_fulfillment_service_staging_topic.arn
  policy = data.aws_iam_policy_document.staging_twitch_fulfillment_policy_document.json
}

resource "aws_sns_topic" "bits_usage_prod_topic" {
  name = "bits-usage-prod"
}

data "aws_iam_policy_document" "prod_bits_usage_policy_document" {
  statement {
    sid = "__default_statement_ID"
    actions = [
      "SNS:Publish",
      "SNS:RemovePermission",
      "SNS:SetTopicAttributes",
      "SNS:DeleteTopic",
      "SNS:ListSubscriptionsByTopic",
      "SNS:GetTopicAttributes",
      "SNS:Receive",
      "SNS:AddPermission",
      "SNS:Subscribe"
    ]
    effect = "Allow"
    principals {
      identifiers = ["*"]
      type = "AWS"
    }
    resources = [aws_sns_topic.bits_usage_prod_topic.arn]
    condition {
      test = "StringEquals"
      values = ["021561903526"]
      variable = "AWS:SourceOwner"
    }
  }
  statement {
    sid = "__console_sub_0"
    actions = ["sns:Subscribe","SNS:Receive"]
    effect = "Allow"
    principals {
      identifiers = ["arn:aws:iam::180265471281:root","arn:aws:iam::989470033077:root"]
      type = "AWS"
    }
    resources = [aws_sns_topic.bits_usage_prod_topic.arn]
  }
}

resource "aws_sns_topic_policy" "prod_bits_usage_policy" {
  arn = aws_sns_topic.bits_usage_prod_topic.arn
  policy = data.aws_iam_policy_document.prod_bits_usage_policy_document.json
}

resource "aws_sns_topic" "bits_usage_staging_topic" {
  name = "bits-usage-staging"
}

data "aws_iam_policy_document" "staging_bits_usage_policy_document" {
  statement {
    sid = "__default_statement_ID"
    actions = [
      "SNS:Publish",
      "SNS:RemovePermission",
      "SNS:SetTopicAttributes",
      "SNS:DeleteTopic",
      "SNS:ListSubscriptionsByTopic",
      "SNS:GetTopicAttributes",
      "SNS:Receive",
      "SNS:AddPermission",
      "SNS:Subscribe"
    ]
    effect = "Allow"
    principals {
      identifiers = ["*"]
      type = "AWS"
    }
    resources = [aws_sns_topic.bits_usage_staging_topic.arn]
    condition {
      test = "StringEquals"
      values = ["021561903526"]
      variable = "AWS:SourceOwner"
    }
  }
  statement {
    sid = "__console_sub_0"
    actions = ["sns:Subscribe","SNS:Receive"]
    effect = "Allow"
    principals {
      identifiers = ["arn:aws:iam::106728537780:root", "arn:aws:iam::989470033077:root"]
      type = "AWS"
    }
    resources = [aws_sns_topic.bits_usage_staging_topic.arn]
  }
}

resource "aws_sns_topic_policy" "staging_bits_usage_policy" {
  arn = aws_sns_topic.bits_usage_staging_topic.arn
  policy = data.aws_iam_policy_document.staging_bits_usage_policy_document.json
}

resource "aws_sns_topic" "bits_admin_jobs_staging_topic" {
  name = "bits-admin-jobs-staging"
}

resource "aws_sns_topic" "bits_admin_jobs_prod_topic" {
  name = "bits-admin-jobs-prod"
}

resource "aws_sns_topic" "bits_channel_settings_update_staging_topic" {
  name = "bits-channel-settings-update-staging"
}

resource "aws_sns_topic_policy" "staging_channel_settings_policy" {
  arn = aws_sns_topic.bits_channel_settings_update_staging_topic.arn
  policy = data.aws_iam_policy_document.payday_bits_channel_settings_update_staging_policy_document.json
}

data "aws_iam_policy_document" "payday_bits_channel_settings_update_staging_policy_document" {
  statement {
    sid = "__default_statement_ID"
    actions = [
      "SNS:Publish",
      "SNS:RemovePermission",
      "SNS:SetTopicAttributes",
      "SNS:DeleteTopic",
      "SNS:ListSubscriptionsByTopic",
      "SNS:GetTopicAttributes",
      "SNS:Receive",
      "SNS:AddPermission",
      "SNS:Subscribe"
    ]
    effect = "Allow"
    principals {
      identifiers = ["*"]
      type = "AWS"
    }
    resources = [aws_sns_topic.bits_channel_settings_update_staging_topic.arn]
    condition {
      test = "StringEquals"
      values = ["021561903526"]
      variable = "AWS:SourceOwner"
    }
  }
  statement {
    sid = "__console_sub_0"
    actions = ["sns:Subscribe","sns:Receive"]
    effect = "Allow"
    principals {
      identifiers = [
        "arn:aws:iam::958836777662:root"
      ]
      type = "AWS"
    }
    resources = [aws_sns_topic.bits_channel_settings_update_staging_topic.arn]
    condition {
      test = "StringEquals"
      values = ["sqs"]
      variable = "SNS:Protocol"
    }
  }
}

resource "aws_sns_topic" "bits_channel_settings_update_prod_topic" {
  name = "bits-channel-settings-update-prod"
}

resource "aws_sns_topic_policy" "prod_channel_settings_policy" {
  arn = aws_sns_topic.bits_channel_settings_update_prod_topic.arn
  policy = data.aws_iam_policy_document.payday_bits_channel_settings_update_prod_policy_document.json
}

data "aws_iam_policy_document" "payday_bits_channel_settings_update_prod_policy_document" {
  statement {
    sid = "__default_statement_ID"
    actions = [
      "SNS:Publish",
      "SNS:RemovePermission",
      "SNS:SetTopicAttributes",
      "SNS:DeleteTopic",
      "SNS:ListSubscriptionsByTopic",
      "SNS:GetTopicAttributes",
      "SNS:Receive",
      "SNS:AddPermission",
      "SNS:Subscribe"
    ]
    effect = "Allow"
    principals {
      identifiers = ["*"]
      type = "AWS"
    }
    resources = [aws_sns_topic.bits_channel_settings_update_prod_topic.arn]
    condition {
      test = "StringEquals"
      values = ["021561903526"]
      variable = "AWS:SourceOwner"
    }
  }
  statement {
    sid = "__console_sub_0"
    actions = ["sns:Subscribe","sns:Receive"]
    effect = "Allow"
    principals {
      identifiers = [
        "arn:aws:iam::522398581884:root"
      ]
      type = "AWS"
    }
    resources = [aws_sns_topic.bits_channel_settings_update_prod_topic.arn]
    condition {
      test = "StringEquals"
      values = ["sqs"]
      variable = "SNS:Protocol"
    }
  }
}

resource "aws_sns_topic" "bits_upload_emoticon_staging_topic" {
  name = "bits-upload-emoticon-event-staging"
}

resource "aws_sns_topic" "bits_upload_emoticon_prod_topic" {
  name = "bits-upload-emoticon-event-prod"
}

data "aws_iam_policy_document" "bits_upload_emoticon_staging_policy_document" {
  statement {
    sid = "__default_statement_ID"
    actions = [
      "SNS:Publish",
      "SNS:RemovePermission",
      "SNS:SetTopicAttributes",
      "SNS:DeleteTopic",
      "SNS:ListSubscriptionsByTopic",
      "SNS:GetTopicAttributes",
      "SNS:Receive",
      "SNS:AddPermission",
      "SNS:Subscribe"
    ]
    effect = "Allow"
    principals {
      identifiers = ["*"]
      type = "AWS"
    }
    resources = [aws_sns_topic.bits_upload_emoticon_staging_topic.arn]
    condition {
      test = "StringEquals"
      values = ["021561903526"]
      variable = "AWS:SourceOwner"
    }
  }
  statement {
    sid = "__console_sub_0"
    actions = [
      "sns:Publish",
      "sns:Subscribe",
      "sns:Receive"
    ]
    effect = "Allow"
    principals {
      identifiers = [
        "arn:aws:iam::995367761609:root"
      ]
      type = "AWS"
    }
    resources = [aws_sns_topic.bits_upload_emoticon_staging_topic.arn]
    condition {
      test = "StringEquals"
      values = ["sqs"]
      variable = "SNS:Protocol"
    }
  }
}

resource "aws_sns_topic_policy" "bits_upload_emoticon_staging_policy" {
  arn = aws_sns_topic.bits_upload_emoticon_staging_topic.arn
  policy = data.aws_iam_policy_document.bits_upload_emoticon_staging_policy_document.json
}

data "aws_iam_policy_document" "bits_upload_emoticon_prod_policy_document" {
  statement {
    sid = "__default_statement_ID"
    actions = [
      "SNS:Publish",
      "SNS:RemovePermission",
      "SNS:SetTopicAttributes",
      "SNS:DeleteTopic",
      "SNS:ListSubscriptionsByTopic",
      "SNS:GetTopicAttributes",
      "SNS:Receive",
      "SNS:AddPermission",
      "SNS:Subscribe"
    ]
    effect = "Allow"
    principals {
      identifiers = ["*"]
      type = "AWS"
    }
    resources = [aws_sns_topic.bits_upload_emoticon_prod_topic.arn]
    condition {
      test = "StringEquals"
      values = ["021561903526"]
      variable = "AWS:SourceOwner"
    }
  }
  statement {
    sid = "__console_sub_0"
    actions = [
      "sns:Publish",
      "sns:Subscribe",
      "sns:Receive"
    ]
    effect = "Allow"
    principals {
      identifiers = ["arn:aws:iam::435569175256:root"]
      type = "AWS"
    }
    resources = [aws_sns_topic.bits_upload_emoticon_prod_topic.arn]
  }
}

resource "aws_sns_topic_policy" "bits_upload_emoticon_prod_policy" {
  arn = aws_sns_topic.bits_upload_emoticon_prod_topic.arn
  policy = data.aws_iam_policy_document.bits_upload_emoticon_prod_policy_document.json
}

resource "aws_sns_topic" "payday_use_bits_on_extension_event_staging_topic" {
  name = "payday_use_bits_on_extension_event_staging"
}

data "aws_iam_policy_document" "payday_use_bits_on_extension_event_staging_policy_document" {
  statement {
    sid = "__default_statement_ID"
    actions = [
      "SNS:Publish",
      "SNS:RemovePermission",
      "SNS:SetTopicAttributes",
      "SNS:DeleteTopic",
      "SNS:ListSubscriptionsByTopic",
      "SNS:GetTopicAttributes",
      "SNS:Receive",
      "SNS:AddPermission",
      "SNS:Subscribe"
    ]
    effect = "Allow"
    principals {
      identifiers = ["*"]
      type = "AWS"
    }
    resources = [aws_sns_topic.bits_usage_staging_topic.arn]
    condition {
      test = "StringEquals"
      values = ["021561903526"]
      variable = "AWS:SourceOwner"
    }
  }
  statement {
    sid = "__console_sub_0"
    actions = ["sns:Subscribe","sns:Receive"]
    effect = "Allow"
    principals {
      identifiers = [
        "arn:aws:iam::245967698355:root",
        "arn:aws:iam::848090140584:root"
      ]
      type = "AWS"
    }
    resources = [aws_sns_topic.payday_use_bits_on_extension_event_staging_topic.arn]
    condition {
      test = "StringEquals"
      values = ["sqs"]
      variable = "SNS:Protocol"
    }
  }
}

resource "aws_sns_topic_policy" "payday_use_bits_on_extension_event_staging_policy" {
  arn = aws_sns_topic.payday_use_bits_on_extension_event_staging_topic.arn
  policy = data.aws_iam_policy_document.payday_use_bits_on_extension_event_staging_policy_document.json
}

resource "aws_sns_topic" "payday_use_bits_on_extension_event_prod_topic" {
  name = "payday_use_bits_on_extension_event_prod"
}

data "aws_iam_policy_document" "payday_use_bits_on_extension_event_prod_policy_document" {
  statement {
    sid = "__default_statement_ID"
    actions = [
      "SNS:Publish",
      "SNS:RemovePermission",
      "SNS:SetTopicAttributes",
      "SNS:DeleteTopic",
      "SNS:ListSubscriptionsByTopic",
      "SNS:GetTopicAttributes",
      "SNS:Receive",
      "SNS:AddPermission",
      "SNS:Subscribe"
    ]
    effect = "Allow"
    principals {
      identifiers = ["*"]
      type = "AWS"
    }
    resources = [aws_sns_topic.bits_usage_staging_topic.arn]
    condition {
      test = "StringEquals"
      values = ["021561903526"]
      variable = "AWS:SourceOwner"
    }
  }
  statement {
    sid = "__console_sub_0"
    actions = ["sns:Subscribe","sns:Receive"]
    effect = "Allow"
    principals {
      identifiers = ["arn:aws:iam::440944323647:root"]
      type = "AWS"
    }
    resources = [aws_sns_topic.payday_use_bits_on_extension_event_prod_topic.arn]
  }
}

resource "aws_sns_topic_policy" "payday_use_bits_on_extension_event_prod_policy" {
  arn = aws_sns_topic.payday_use_bits_on_extension_event_prod_topic.arn
  policy = data.aws_iam_policy_document.payday_use_bits_on_extension_event_prod_policy_document.json
}

resource "aws_sns_topic" "payday_redeem_key_prod_topic" {
  name         = "RedeemKey-Prod"
  display_name = "redeem-key"
}

data "aws_iam_policy_document" "payday_redeem_key_prod_policy_document" {
  version   = "2008-10-17"
  policy_id = "__default_policy_ID"
  statement {
    sid     = "__default_statement_ID"
    actions = [
      "SNS:Publish",
      "SNS:RemovePermission",
      "SNS:SetTopicAttributes",
      "SNS:DeleteTopic",
      "SNS:ListSubscriptionsByTopic",
      "SNS:GetTopicAttributes",
      "SNS:Receive",
      "SNS:AddPermission",
      "SNS:Subscribe"
    ]
    effect = "Allow"
    principals {
      identifiers = ["*"]
      type        = "AWS"
    }
    resources = [aws_sns_topic.payday_redeem_key_prod_topic.arn]
    condition {
      test     = "StringEquals"
      values   = ["021561903526"]
      variable = "AWS:SourceOwner"
    }
  }
  statement {
    sid     = "__console_pub_0"
    actions = ["SNS:Publish"]
    effect  = "Allow"
    principals {
      identifiers = ["arn:aws:iam::748615074154:role/prod-commerce-phanto"]
      type        = "AWS"
    }
    resources = [aws_sns_topic.payday_redeem_key_prod_topic.arn]
  }
  statement {
    sid     = "phanto_admins"
    actions = ["SNS:Publish"]
    effect  = "Allow"
    principals {
      identifiers = ["arn:aws:iam::748615074154:role/admin"]
      type        = "AWS"
    }
    resources = [aws_sns_topic.payday_redeem_key_prod_topic.arn]
  }
  statement {
    sid     = "handcraft-phanto-a2z-fix-0"
    actions = ["SNS:Publish"]
    effect  = "Allow"
    principals {
      identifiers = ["arn:aws:iam::748615074154:role/ecs-role-phanto-prod-prod-tf"]
      type        = "AWS"
    }
    resources = [aws_sns_topic.payday_redeem_key_prod_topic.arn]
  }
  statement {
    sid     = "handcraft-phanto-a2z-fix-1"
    actions = ["SNS:Publish"]
    effect  = "Allow"
    principals {
      identifiers = ["arn:aws:iam::748615074154:role/ecs-role-phanto-prod-canary-prod-tf"]
      type        = "AWS"
    }
    resources = [aws_sns_topic.payday_redeem_key_prod_topic.arn]
  }
}

resource "aws_sns_topic_policy" "payday_redeem_key_prod_policy" {
  arn    = aws_sns_topic.payday_redeem_key_prod_topic.arn
  policy = data.aws_iam_policy_document.payday_redeem_key_prod_policy_document.json
}

resource "aws_sns_topic" "pachter_ingestion_prod_topic" {
  name = "pachter-ingestion-prod"
}

resource "aws_sns_topic_policy" "pachter_ingestion_prod_policy" {
  arn    = aws_sns_topic.pachter_ingestion_prod_topic.arn
  policy = data.aws_iam_policy_document.pachter_ingestion_prod_policy_document.json
}

data "aws_iam_policy_document" "pachter_ingestion_prod_policy_document" {
  statement {
    sid     = "pachter_ingestion_prod_policy_s1"
    actions = [
      "SNS:Publish",
      "SNS:RemovePermission",
      "SNS:SetTopicAttributes",
      "SNS:DeleteTopic",
      "SNS:ListSubscriptionsByTopic",
      "SNS:GetTopicAttributes",
      "SNS:Receive",
      "SNS:AddPermission",
      "SNS:Subscribe"
    ]
    effect  = "Allow"
    principals {
      identifiers = ["arn:aws:iam::021561903526:root"] # Payday itself
      type        = "AWS"
    }
    resources = [aws_sns_topic.pachter_ingestion_prod_topic.arn]
  }
  statement {
    sid     = "pachter_ingestion_prod_policy_s2"
    actions = ["sns:Subscribe","sns:Receive"]
    effect  = "Allow"
    principals {
      identifiers = [
        "arn:aws:iam::458492755823:root" # Pachter prod
      ]
      type = "AWS"
    }
    resources = [aws_sns_topic.pachter_ingestion_prod_topic.arn]
  }
}

resource "aws_sns_topic" "pachter_ingestion_staging_topic" {
  name = "pachter-ingestion-staging"
}

resource "aws_sns_topic_policy" "pachter_ingestion_staging_policy" {
  arn    = aws_sns_topic.pachter_ingestion_staging_topic.arn
  policy = data.aws_iam_policy_document.pachter_ingestion_staging_policy_document.json
}

data "aws_iam_policy_document" "pachter_ingestion_staging_policy_document" {
  statement {
    sid     = "pachter_ingestion_staging_policy_s1"
    actions = [
      "SNS:Publish",
      "SNS:RemovePermission",
      "SNS:SetTopicAttributes",
      "SNS:DeleteTopic",
      "SNS:ListSubscriptionsByTopic",
      "SNS:GetTopicAttributes",
      "SNS:Receive",
      "SNS:AddPermission",
      "SNS:Subscribe"
    ]
    effect  = "Allow"
    principals {
      identifiers = ["arn:aws:iam::021561903526:root"] # Payday itself
      type        = "AWS"
    }
    resources = [aws_sns_topic.pachter_ingestion_staging_topic.arn]
  }
  statement {
    sid     = "pachter_ingestion_staging_policy_s2"
    actions = ["sns:Subscribe","sns:Receive"]
    effect  = "Allow"
    principals {
      identifiers = [
        "arn:aws:iam::721938063588:root" # Pachter staging
      ]
      type = "AWS"
    }
    resources = [aws_sns_topic.pachter_ingestion_staging_topic.arn]
  }
}
