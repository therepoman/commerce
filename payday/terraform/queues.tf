variable "sqs_redrive_max_retry" {
  default = 4
}

variable "fabs_bits_sns_topic_arn_prod" {
  default = "arn:aws:sns:us-east-1:277306168768:BitsUpdate"
}

variable "fabs_bits_sns_topic_arn_staging" {
  default = "arn:aws:sns:us-east-1:256445009424:BitsUpdate"
}

variable "image_moderation_sns_topic_arn_prod" {
  default = "arn:aws:sns:us-west-2:021561903526:ImageModeration-Prod"
}

variable "image_moderation_sns_topic_arn_staging" {
  default = "arn:aws:sns:us-west-2:021561903526:ImageModeration-Staging"
}

variable "pantheon_event_sns_topic_arn_staging" {
  default = "arn:aws:sns:us-west-2:192960249206:pantheon-staging-events"
}

variable "pantheon_event_sns_topic_arn_prod" {
  default = "arn:aws:sns:us-west-2:375121467453:pantheon-prod-events"
}

variable "upload_service_callback_sns_topic_arn_staging" {
  default = "arn:aws:sns:us-west-2:021561903526:UploadServiceCallBack-Staging"
}

variable "upload_service_callback_sns_topic_arn_prod" {
  default = "arn:aws:sns:us-west-2:021561903526:UploadServiceCallBack-Prod"
}

variable "automod_sns_topic_arn_staging" {
  default = "arn:aws:sns:us-west-2:021561903526:AutoMod-Staging"
}

variable "automod_sns_topic_arn_prod" {
  default = "arn:aws:sns:us-west-2:021561903526:AutoMod-Prod"
}

variable "redeem_key_code_sns_topic_arn_staging" {
  default = "arn:aws:sns:us-west-2:021561903526:RedeemKey-Staging"
}

variable "redeem_key_code_sns_topic_arn_prod" {
  default = "arn:aws:sns:us-west-2:021561903526:RedeemKey-Prod"
}

variable "payday_use_bits_on_extension_event_sns_topic_arn_staging" {
  default = "arn:aws:sns:us-west-2:021561903526:payday_use_bits_on_extension_event_staging"
}

variable "payday_use_bits_on_extension_event_sns_topic_arn_prod" {
  default = "arn:aws:sns:us-west-2:021561903526:payday_use_bits_on_extension_event_prod"
}

variable "payday_emoticon_upload_sns_topic_arn_staging" {
  default = "arn:aws:sns:us-west-2:021561903526:bits-upload-emoticon-event-staging"
}

variable "payday_emoticon_upload_sns_topic_arn_prod" {
  default = "arn:aws:sns:us-west-2:021561903526:bits-upload-emoticon-event-prod"
}

variable "everdeen_response_sns_stopic_arn_staging" {
  default = "arn:aws:sns:us-west-2:589506732331:PreauthorizedPurchaseStatus"
}

variable "everdeen_response_sns_stopic_arn_prod" {
  default = "arn:aws:sns:us-west-2:316550374861:PreauthorizedPurchaseStatus"
}

# Queue Policies
resource "aws_iam_policy_attachment" "payday_sqs_readonly_staging" {
  name       = "payday_sqs_readonly_staging"
  policy_arn = aws_iam_policy.sqs_readonly_staging.arn

  roles = [
    aws_iam_role.payday_staging.name,
  ]
}

data "aws_iam_policy_document" "sqs_readonly_staging_policy" {
  statement {
    actions = [
      "sqs:GetQueueAttributes",
      "sqs:ListQueues",
      "sqs:ReceiveMessage",
      "sqs:DeleteMessage",
    ]

    resources = [
      aws_sqs_queue.payday_pubsub_queue_staging.arn,
      aws_sqs_queue.payday_datascience_queue_staging.arn,
      aws_sqs_queue.payday_bit_events_v1_queue_staging.arn,
      aws_sqs_queue.payday_bits_cache_queue_staging.arn,
      aws_sqs_queue.payday_image_moderation_queue_staging.arn,
      aws_sqs_queue.payday_image_update_queue_staging.arn,
      module.payday_bits_on_extensions_pubsub_queue.staging_queue_arn,
      module.payday_partner_onboarding_automation_queue.staging_queue_arn,
      module.payday_channel_leaderboard_queue.staging_queue_arn,
      module.payday_bits_admin_job_queue.staging_queue_arn,
      module.pantheon_event_queue.staging_queue_arn,
      module.payday_settings_update_queue.staging_queue_arn,
      module.payday_revenue_queue.staging_queue_arn,
      module.payday_badge_queue.staging_queue_arn,
      module.payday_upload_service_callback_queue.staging_queue_arn,
      module.payday_automod_timeout_queue.staging_queue_arn,
      module.payday_prometheus_queue.staging_queue_arn,
      module.payday_pushy_queue.staging_queue_arn,
      module.payday_redeem_key_code_queue.staging_queue_arn,
      module.payday_new_user_purchase_queue.staging_queue_arn,
      module.payday_eventbus_queue.staging_queue_arn,
      module.payday_auto_refill_queue.staging_queue_arn,
      module.payday_pachinko_perseus_queue.staging_queue_arn,
      module.payday_emoticon_upload_queue.staging_queue_arn,
      module.payday_auto_refill_everdeen_response_queue.staging_queue_arn,
      module.payday_pachinko_transaction_update.staging_queue_arn,
    ]
  }
}

resource "aws_iam_policy" "sqs_readonly_staging" {
  name        = "sqs_readonly_staging"
  description = "readonly access to all SQS queues"
  policy      = data.aws_iam_policy_document.sqs_readonly_staging_policy.json
}

resource "aws_iam_policy_attachment" "payday_sqs_readonly" {
  name       = "payday_sqs_readonly"
  policy_arn = aws_iam_policy.sqs_readonly.arn

  roles = [
    aws_iam_role.payday_prod.name,
  ]
}

data "aws_iam_policy_document" "sqs_readonly_prod_policy" {
  statement {
    actions = [
      "sqs:GetQueueAttributes",
      "sqs:ListQueues",
      "sqs:ReceiveMessage",
      "sqs:DeleteMessage",
    ]

    resources = [
      aws_sqs_queue.payday_pubsub_queue.arn,
      aws_sqs_queue.payday_datascience_queue.arn,
      aws_sqs_queue.payday_bigbits_queue.arn,
      aws_sqs_queue.payday_bit_events_v1_queue.arn,
      aws_sqs_queue.payday_bits_cache_queue.arn,
      aws_sqs_queue.payday_image_moderation_queue.arn,
      aws_sqs_queue.payday_image_update_queue.arn,
      module.payday_bits_on_extensions_pubsub_queue.prod_queue_arn,
      module.payday_partner_onboarding_automation_queue.prod_queue_arn,
      module.payday_channel_leaderboard_queue.prod_queue_arn,
      module.payday_bits_admin_job_queue.prod_queue_arn,
      module.pantheon_event_queue.prod_queue_arn,
      module.payday_settings_update_queue.prod_queue_arn,
      module.payday_revenue_queue.prod_queue_arn,
      module.payday_badge_queue.prod_queue_arn,
      module.payday_upload_service_callback_queue.prod_queue_arn,
      module.payday_automod_timeout_queue.prod_queue_arn,
      module.payday_prometheus_queue.prod_queue_arn,
      module.payday_pushy_queue.prod_queue_arn,
      module.payday_redeem_key_code_queue.prod_queue_arn,
      module.payday_new_user_purchase_queue.prod_queue_arn,
      module.payday_eventbus_queue.prod_queue_arn,
      module.payday_auto_refill_queue.prod_queue_arn,
      module.payday_pachinko_perseus_queue.prod_queue_arn,
      module.payday_emoticon_upload_queue.prod_queue_arn,
      module.payday_auto_refill_everdeen_response_queue.prod_queue_arn,
      module.payday_pachinko_transaction_update.prod_queue_arn,
    ]
  }
}

resource "aws_iam_policy" "sqs_readonly" {
  name        = "sqs_readonly"
  description = "readonly access to all SQS queues"
  policy      = data.aws_iam_policy_document.sqs_readonly_prod_policy.json
}

# External Pubsub Queue
resource "aws_sqs_queue" "payday_pubsub_queue_deadletter" {
  message_retention_seconds = "1209600"
  name                      = "payday_pubsub_queue_deadletter"
}

resource "aws_cloudwatch_metric_alarm" "payday_pubsub_queue_deadletter_dlq_not_empty" {
  alarm_name          = "payday_pubsub_queue_deadletter_not_empty"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "5"
  metric_name         = "ApproximateNumberOfMessagesVisible"
  namespace           = "AWS/SQS"

  dimensions = {
    QueueName = "payday_pubsub_queue_deadletter"
  }

  period             = "300"
  statistic          = "Sum"
  threshold          = "1"
  alarm_description  = "This metric monitors one of payday's dead letter queues. It will alarm if there are too many messages in that queue."
  treat_missing_data = "notBreaching"
}

resource "aws_sqs_queue" "payday_pubsub_queue" {
  message_retention_seconds = "1209600"
  name                      = "payday_pubsub_queue"
  receive_wait_time_seconds = 20

  redrive_policy = <<EOF
{
  "deadLetterTargetArn": "${aws_sqs_queue.payday_pubsub_queue_deadletter.arn}",
  "maxReceiveCount": ${var.sqs_redrive_max_retry}
}
EOF

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "PaydayQueueSid",
      "Effect": "Allow",
      "Principal": "*",
      "Action": "SQS:SendMessage",
      "Resource": "*",
      "Condition": {
        "StringEquals": {
          "aws:SourceArn": "${var.fabs_bits_sns_topic_arn_prod}"
        }
      }
    }
  ]
}
EOF
}

resource "aws_sqs_queue" "payday_pubsub_queue_deadletter_staging" {
  message_retention_seconds = "1209600"
  name                      = "payday_pubsub_queue_deadletter_staging"
}

resource "aws_sqs_queue" "payday_pubsub_queue_staging" {
  message_retention_seconds = "1209600"
  name                      = "payday_pubsub_queue_staging"
  receive_wait_time_seconds = 20

  redrive_policy = <<EOF
{
  "deadLetterTargetArn": "${aws_sqs_queue.payday_pubsub_queue_deadletter_staging.arn}",
  "maxReceiveCount": ${var.sqs_redrive_max_retry}
}
EOF

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "PaydayQueueSid",
      "Effect": "Allow",
      "Principal": "*",
      "Action": "SQS:SendMessage",
      "Resource": "*",
      "Condition": {
        "StringEquals": {
          "aws:SourceArn": "${var.fabs_bits_sns_topic_arn_staging}"
        }
      }
    }
  ]
}
EOF
}

# Data Science Queue
resource "aws_sqs_queue" "payday_datascience_queue_deadletter" {
  message_retention_seconds  = "1209600"
  name                       = "payday_datascience_queue_deadletter"
  visibility_timeout_seconds = 5
}

resource "aws_cloudwatch_metric_alarm" "payday_datascience_queue_deadletter_not_empty" {
  alarm_name          = "payday_datascience_queue_deadletter_not_empty"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "5"
  datapoints_to_alarm = "1"
  metric_name         = "ApproximateNumberOfMessagesVisible"
  namespace           = "AWS/SQS"

  dimensions = {
    QueueName = "payday_datascience_queue_deadletter"
  }

  period             = "300"
  statistic          = "Sum"
  threshold          = "1"
  alarm_description  = "This metric monitors one of payday's dead letter queues. It will alarm if there are too many messages in that queue."
  treat_missing_data = "notBreaching"
}

resource "aws_sqs_queue" "payday_datascience_queue" {
  message_retention_seconds = "1209600"
  name                      = "payday_datascience_queue"
  receive_wait_time_seconds = 20
  delay_seconds             = 600 # Wait long enough to make sure Pachter processes the event first

  redrive_policy = <<EOF
{
  "deadLetterTargetArn": "${aws_sqs_queue.payday_datascience_queue_deadletter.arn}",
  "maxReceiveCount": 6
}
EOF

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "PaydayQueueSid",
      "Effect": "Allow",
      "Principal": "*",
      "Action": "SQS:SendMessage",
      "Resource": "*",
      "Condition": {
        "StringEquals": {
          "aws:SourceArn": "${var.fabs_bits_sns_topic_arn_prod}"
        }
      }
    }
  ]
}
EOF
}

resource "aws_sqs_queue" "payday_datascience_queue_deadletter_staging" {
  message_retention_seconds = "1209600"
  name                      = "payday_datascience_queue_deadletter_staging"
}

resource "aws_sqs_queue" "payday_datascience_queue_staging" {
  message_retention_seconds = "1209600"
  name                      = "payday_datascience_queue_staging"
  receive_wait_time_seconds = 20
  delay_seconds             = 60 # Wait long enough to make sure Pachter processes the event first

  redrive_policy = <<EOF
{
  "deadLetterTargetArn": "${aws_sqs_queue.payday_datascience_queue_deadletter_staging.arn}",
  "maxReceiveCount": ${var.sqs_redrive_max_retry}
}
EOF

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "PaydayQueueSid",
      "Effect": "Allow",
      "Principal": "*",
      "Action": "SQS:SendMessage",
      "Resource": "*",
      "Condition": {
        "StringEquals": {
          "aws:SourceArn": "${var.fabs_bits_sns_topic_arn_staging}"
        }
      }
    }
  ]
}
EOF
}

# Bigbits Queue
resource "aws_sqs_queue" "payday_bigbits_queue_deadletter" {
  name = "payday_bigbits_queue_deadletter"
}

resource "aws_sqs_queue" "payday_bigbits_queue" {
  name                      = "payday_bigbits_queue"
  receive_wait_time_seconds = 20

  redrive_policy = <<EOF
{
  "deadLetterTargetArn": "${aws_sqs_queue.payday_bigbits_queue_deadletter.arn}",
  "maxReceiveCount": ${var.sqs_redrive_max_retry}
}
EOF

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "PaydayQueueSid",
      "Effect": "Allow",
      "Principal": "*",
      "Action": "SQS:SendMessage",
      "Resource": "*",
      "Condition": {
        "StringEquals": {
          "aws:SourceArn": "${var.fabs_bits_sns_topic_arn_prod}"
        }
      }
    }
  ]
}
EOF
}

#bit_events_v1 queue
resource "aws_sqs_queue" "payday_bit_events_v1_queue_deadletter" {
  name = "payday_bit_events_v1_queue_deadletter"
}

resource "aws_cloudwatch_metric_alarm" "payday_bit_events_v1_queue_deadletter_not_empty" {
  alarm_name          = "payday_bit_events_v1_queue_deadletter_not_empty"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "5"
  metric_name         = "ApproximateNumberOfMessagesVisible"
  namespace           = "AWS/SQS"

  dimensions = {
    QueueName = "payday_bit_events_v1_queue_deadletter"
  }

  period             = "300"
  statistic          = "Sum"
  threshold          = "1"
  alarm_description  = "This metric monitors one of payday's dead letter queues. It will alarm if there are too many messages in that queue."
  treat_missing_data = "notBreaching"
}

resource "aws_sqs_queue" "payday_bit_events_v1_queue" {
  name                      = "payday_bit_events_v1_queue"
  receive_wait_time_seconds = 20

  redrive_policy = <<EOF
{
  "deadLetterTargetArn": "${aws_sqs_queue.payday_bit_events_v1_queue_deadletter.arn}",
  "maxReceiveCount": 3
}
EOF

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "PaydayQueueSid",
      "Effect": "Allow",
      "Principal": "*",
      "Action": "SQS:SendMessage",
      "Resource": "*",
      "Condition": {
        "StringEquals": {
          "aws:SourceArn": "${var.fabs_bits_sns_topic_arn_prod}"
        }
      }
    }
  ]
}
EOF
}

resource "aws_sqs_queue" "payday_bit_events_v1_queue_deadletter_staging" {
  name = "payday_bit_events_v1_queue_deadletter_staging"
}

resource "aws_sqs_queue" "payday_bit_events_v1_queue_staging" {
  name                      = "payday_bit_events_v1_queue_staging"
  receive_wait_time_seconds = 20

  redrive_policy = <<EOF
{
  "deadLetterTargetArn": "${aws_sqs_queue.payday_bit_events_v1_queue_deadletter_staging.arn}",
  "maxReceiveCount": ${var.sqs_redrive_max_retry}
}
EOF

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "PaydayQueueSid",
      "Effect": "Allow",
      "Principal": "*",
      "Action": "SQS:SendMessage",
      "Resource": "*",
      "Condition": {
        "StringEquals": {
          "aws:SourceArn": "${var.fabs_bits_sns_topic_arn_staging}"
        }
      }
    }
  ]
}
EOF
}

#Bits Cache Queue
resource "aws_sqs_queue" "payday_bits_cache_queue_deadletter" {
  name = "payday_bits_cache_queue_deadletter"
}

resource "aws_cloudwatch_metric_alarm" "payday_bits_cache_queue_deadletter_not_empty" {
  alarm_name          = "payday_bits_cache_queue_deadletter_not_empty"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "5"
  metric_name         = "ApproximateNumberOfMessagesVisible"
  namespace           = "AWS/SQS"

  dimensions = {
    QueueName = "payday_bits_cache_queue_deadletter"
  }

  period             = "300"
  statistic          = "Sum"
  threshold          = "1"
  alarm_description  = "This metric monitors one of payday's dead letter queues. It will alarm if there are too many messages in that queue."
  treat_missing_data = "notBreaching"
}

resource "aws_sqs_queue" "payday_bits_cache_queue" {
  name                      = "payday_bits_cache_queue"
  receive_wait_time_seconds = 20

  redrive_policy = <<EOF
{
  "deadLetterTargetArn": "${aws_sqs_queue.payday_bits_cache_queue_deadletter.arn}",
  "maxReceiveCount": ${var.sqs_redrive_max_retry}
}
EOF

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "PaydayQueueSid",
      "Effect": "Allow",
      "Principal": "*",
      "Action": "SQS:SendMessage",
      "Resource": "*",
      "Condition": {
        "StringEquals": {
          "aws:SourceArn": "${var.fabs_bits_sns_topic_arn_prod}"
        }
      }
    }
  ]
}
EOF
}

resource "aws_sqs_queue" "payday_bits_cache_queue_deadletter_staging" {
  name = "payday_bits_cache_queue_deadletter_staging"
}

resource "aws_sqs_queue" "payday_bits_cache_queue_staging" {
  name                      = "payday_bits_cache_queue_staging"
  receive_wait_time_seconds = 20

  redrive_policy = <<EOF
{
  "deadLetterTargetArn": "${aws_sqs_queue.payday_bits_cache_queue_deadletter_staging.arn}",
  "maxReceiveCount": ${var.sqs_redrive_max_retry}
}
EOF

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "PaydayQueueSid",
      "Effect": "Allow",
      "Principal": "*",
      "Action": "SQS:SendMessage",
      "Resource": "*",
      "Condition": {
        "StringEquals": {
          "aws:SourceArn": "${var.fabs_bits_sns_topic_arn_staging}"
        }
      }
    }
  ]
}
EOF
}

#Image moderation queue
resource "aws_sqs_queue" "payday_image_moderation_queue_deadletter" {
  name = "payday_image_moderation_queue_deadletter"
}

resource "aws_cloudwatch_metric_alarm" "payday_image_moderation_queue_deadletter_not_empty" {
  alarm_name          = "payday_image_moderation_queue_deadletter_not_empty"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "5"
  metric_name         = "ApproximateNumberOfMessagesVisible"
  namespace           = "AWS/SQS"

  dimensions = {
    QueueName = "payday_image_moderation_queue_deadletter"
  }

  period             = "300"
  statistic          = "Sum"
  threshold          = "1"
  alarm_description  = "This metric monitors one of payday's dead letter queues. It will alarm if there are too many messages in that queue."
  treat_missing_data = "notBreaching"
}

resource "aws_sqs_queue" "payday_image_moderation_queue" {
  name                      = "payday_image_moderation_queue"
  receive_wait_time_seconds = 20

  redrive_policy = <<EOF
{
  "deadLetterTargetArn": "${aws_sqs_queue.payday_image_moderation_queue_deadletter.arn}",
  "maxReceiveCount": ${var.sqs_redrive_max_retry}
}
EOF

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "PaydayQueueSid",
      "Effect": "Allow",
      "Principal": "*",
      "Action": "SQS:SendMessage",
      "Resource": "*",
      "Condition": {
        "StringEquals": {
          "aws:SourceArn": "${var.image_moderation_sns_topic_arn_prod}"
        }
      }
    }
  ]
}
EOF
}

resource "aws_sqs_queue" "payday_image_moderation_queue_deadletter_staging" {
  name = "payday_image_moderation_queue_deadletter_staging"
}

resource "aws_sqs_queue" "payday_image_moderation_queue_staging" {
  name                      = "payday_image_moderation_queue_staging"
  receive_wait_time_seconds = 20

  redrive_policy = <<EOF
{
  "deadLetterTargetArn": "${aws_sqs_queue.payday_image_moderation_queue_deadletter_staging.arn}",
  "maxReceiveCount": ${var.sqs_redrive_max_retry}
}
EOF

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "PaydayQueueSid",
      "Effect": "Allow",
      "Principal": "*",
      "Action": "SQS:SendMessage",
      "Resource": "*",
      "Condition": {
        "StringEquals": {
          "aws:SourceArn": "${var.image_moderation_sns_topic_arn_staging}"
        }
      }
    }
  ]
}
EOF
}

#Image update queue
resource "aws_cloudwatch_metric_alarm" "payday_image_update_queue_deadletter_not_empty" {
  alarm_name          = "payday_image_update_queue_deadletter_not_empty"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "5"
  metric_name         = "ApproximateNumberOfMessagesVisible"
  namespace           = "AWS/SQS"

  dimensions = {
    QueueName = aws_sqs_queue.payday_image_update_queue_deadletter.name
  }

  period             = "300"
  statistic          = "Sum"
  threshold          = "1"
  alarm_description  = "This metric monitors one of payday's dead letter queues. It will alarm if there are too many messages in that queue."
  treat_missing_data = "notBreaching"
}

resource "aws_sqs_queue" "payday_image_update_queue_deadletter" {
  name = "payday_image_update_queue_deadletter"
}

resource "aws_sqs_queue" "payday_image_update_queue" {
  name                      = "payday_image_update_queue"
  receive_wait_time_seconds = 20

  redrive_policy = <<EOF
{
  "deadLetterTargetArn": "${aws_sqs_queue.payday_image_update_queue_deadletter.arn}",
  "maxReceiveCount": ${var.sqs_redrive_max_retry}
}
EOF
}

resource "aws_sqs_queue" "payday_image_update_queue_deadletter_staging" {
  name = "payday_image_update_queue_deadletter_staging"
}

resource "aws_sqs_queue" "payday_image_update_queue_staging" {
  name                      = "payday_image_update_queue_staging"
  receive_wait_time_seconds = 20

  redrive_policy = <<EOF
{
  "deadLetterTargetArn": "${aws_sqs_queue.payday_image_update_queue_deadletter_staging.arn}",
  "maxReceiveCount": ${var.sqs_redrive_max_retry}
}
EOF
}

module "payday_partner_onboarding_automation_queue" {
  source                      = "./modules/default_queue"
  queue_name                  = "payday_partner_onboarding_automation_queue"
  subscribe_to_topics_prod    = ["arn:aws:sns:us-west-2:316550374861:OnboardingStatus"]
  subscribe_to_topics_staging = ["arn:aws:sns:us-west-2:589506732331:OnboardingStatus"]
  alarm_dlq                   = true
}

module "payday_channel_leaderboard_queue" {
  source     = "./modules/default_queue"
  queue_name = "payday_channel_leaderboard_queue"
  alarm_dlq  = true
}

module "payday_bits_admin_job_queue" {
  source                      = "./modules/default_queue"
  queue_name                  = "payday_bits_admin_job_queue"
  subscribe_to_topics_prod    = [aws_sns_topic.bits_admin_jobs_prod_topic.arn]
  subscribe_to_topics_staging = [aws_sns_topic.bits_admin_jobs_staging_topic.arn]
  alarm_dlq                   = true
}

module "payday_settings_update_queue" {
  source                      = "./modules/default_queue"
  queue_name                  = "payday_settings_update_queue"
  subscribe_to_topics_prod    = [aws_sns_topic.bits_channel_settings_update_prod_topic.arn]
  subscribe_to_topics_staging = [aws_sns_topic.bits_channel_settings_update_staging_topic.arn]
  alarm_dlq                   = true
}

module "pantheon_event_queue" {
  source                      = "./modules/default_queue"
  queue_name                  = "pantheon_event_queue"
  subscribe_to_topics_prod    = [var.pantheon_event_sns_topic_arn_prod]
  subscribe_to_topics_staging = [var.pantheon_event_sns_topic_arn_staging]
  alarm_dlq                   = true
}

module "payday_bits_on_extensions_pubsub_queue" {
  source                      = "./modules/default_queue"
  queue_name                  = "payday_bits_on_extensions_pubsub_queue"
  subscribe_to_topics_prod    = [var.fabs_bits_sns_topic_arn_prod]
  subscribe_to_topics_staging = [var.fabs_bits_sns_topic_arn_staging]
  alarm_dlq                   = true
}

module "payday_revenue_queue" {
  source                      = "./modules/default_queue"
  queue_name                  = "payday_revenue_queue"
  subscribe_to_topics_prod    = [var.fabs_bits_sns_topic_arn_prod]
  subscribe_to_topics_staging = [var.fabs_bits_sns_topic_arn_staging]
  alarm_dlq                   = true
}

module "payday_badge_queue" {
  source                      = "./modules/default_queue"
  queue_name                  = "payday_badge_queue"
  subscribe_to_topics_prod    = [var.fabs_bits_sns_topic_arn_prod]
  subscribe_to_topics_staging = [var.fabs_bits_sns_topic_arn_staging]
  alarm_dlq                   = true
}

module "payday_upload_service_callback_queue" {
  source                      = "./modules/default_queue"
  queue_name                  = "payday_upload_service_callback_queue"
  subscribe_to_topics_prod    = [var.upload_service_callback_sns_topic_arn_prod]
  subscribe_to_topics_staging = [var.upload_service_callback_sns_topic_arn_staging]
  alarm_dlq                   = true
}

module "payday_automod_timeout_queue" {
  source                      = "./modules/default_queue"
  queue_name                  = "payday_automod_timeout_queue"
  delivery_delay              = "60"
  subscribe_to_topics_prod    = [var.automod_sns_topic_arn_prod]
  subscribe_to_topics_staging = [var.automod_sns_topic_arn_staging]
  alarm_dlq                   = true
}

module "payday_prometheus_queue" {
  source                      = "./modules/default_queue"
  queue_name                  = "payday_prometheus_queue"
  subscribe_to_topics_prod    = [var.fabs_bits_sns_topic_arn_prod]
  subscribe_to_topics_staging = [var.fabs_bits_sns_topic_arn_staging]
  alarm_dlq                   = true
}

module "payday_pushy_queue" {
  source                      = "./modules/default_queue"
  queue_name                  = "payday_pushy_queue"
  subscribe_to_topics_prod    = [var.fabs_bits_sns_topic_arn_prod]
  subscribe_to_topics_staging = [var.fabs_bits_sns_topic_arn_staging]
  alarm_dlq                   = true
}

module "payday_redeem_key_code_queue" {
  source                      = "./modules/default_queue"
  queue_name                  = "payday_redeem_key_code_queue"
  subscribe_to_topics_prod    = [var.redeem_key_code_sns_topic_arn_prod]
  subscribe_to_topics_staging = [var.redeem_key_code_sns_topic_arn_staging]
  alarm_dlq                   = true
}

module "payday_new_user_purchase_queue" {
  source                      = "./modules/default_queue"
  queue_name                  = "payday_new_user_purchase_queue"
  subscribe_to_topics_prod    = [var.fabs_bits_sns_topic_arn_prod]
  subscribe_to_topics_staging = [var.fabs_bits_sns_topic_arn_staging]
  alarm_dlq                   = true
}

module "payday_eventbus_queue" {
  source     = "./modules/default_queue"
  queue_name = "payday_eventbus_queue"
}

module "payday_auto_refill_queue" {
  source                      = "./modules/default_queue"
  queue_name                  = "payday_auto_refill_queue"
  subscribe_to_topics_prod    = [var.fabs_bits_sns_topic_arn_prod]
  subscribe_to_topics_staging = [var.fabs_bits_sns_topic_arn_staging]
  alarm_dlq                   = true
  alarm_dlq_count             = 3
  low_sev_alarm_dlq           = true
  low_sev_alarm_dlq_count     = 1
}

module "payday_emoticon_upload_queue" {
  source                      = "./modules/default_queue"
  queue_name                  = "payday_emoticon_upload_queue"
  subscribe_to_topics_prod    = [var.payday_emoticon_upload_sns_topic_arn_prod]
  subscribe_to_topics_staging = [var.payday_emoticon_upload_sns_topic_arn_staging]
  alarm_dlq                   = true
  max_receive_count           = "1"
}

resource "aws_sns_topic_subscription" "payday_emoticon_upload_queue_subscription_prod" {
  topic_arn = var.payday_emoticon_upload_sns_topic_arn_prod
  protocol  = "sqs"
  endpoint  = module.payday_emoticon_upload_queue.prod_queue_arn
}

resource "aws_sns_topic_subscription" "payday_emoticon_upload_queue_subscription_staging" {
  topic_arn = var.payday_emoticon_upload_sns_topic_arn_staging
  protocol  = "sqs"
  endpoint  = module.payday_emoticon_upload_queue.staging_queue_arn
}

module "payday_pachinko_perseus_queue" {
  source                      = "./modules/default_queue"
  queue_name                  = "payday_pachinko_perseus_queues"
  subscribe_to_topics_prod    = ["arn:aws:sns:us-west-2:702056039310:bits-prod-updates"]
  subscribe_to_topics_staging = ["arn:aws:sns:us-west-2:946879801923:bits-staging-updates"]
  alarm_dlq                   = true
}

module "payday_pachinko_transaction_update" {
  source                      = "./modules/default_queue"
  queue_name                  = "payday_pachinko_transaction"
  subscribe_to_topics_prod    = ["arn:aws:sns:us-west-2:702056039310:bits-prod-updates"]
  subscribe_to_topics_staging = ["arn:aws:sns:us-west-2:946879801923:bits-staging-updates"]
  alarm_dlq                   = true
  delivery_delay              = 5
}

module "payday_auto_refill_everdeen_response_queue" {
  source                      = "./modules/default_queue"
  queue_name                  = "payday_auto_refill_everdeen_response_queue"
  subscribe_to_topics_prod    = [var.everdeen_response_sns_stopic_arn_prod]
  subscribe_to_topics_staging = [var.everdeen_response_sns_stopic_arn_staging]
  alarm_dlq                   = true
  alarm_dlq_count             = 3
  low_sev_alarm_dlq           = true
  low_sev_alarm_dlq_count     = 1
}
