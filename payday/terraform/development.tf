# Development
# 3 machines per AZ
# three AZs

# elastic network interfaces & ip addresses
resource "aws_eip" "dev-nat-2a" {
    vpc = true
}
resource "aws_eip" "dev-nat-2b" {
    vpc = true
}
resource "aws_eip" "dev-nat-2c" {
    vpc = true
}
