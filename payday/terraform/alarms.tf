# Latency Alarms
resource "aws_cloudwatch_metric_alarm" "is_eligible_to_use_bits_on_extension_latency" {
  alarm_name          = "is-eligible-to-use-bits-on-extension-latency"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "3"
  metric_name         = "TWIRP:IsEligibleToUseBitsOnExtension"
  namespace           = "payday"

  dimensions = {
    Stage   = "production"
    Region  = "us-west-2"
    Service = "payday"
  }

  period                    = "300"
  extended_statistic        = "p99"
  threshold                 = "2"
  alarm_description         = "Alarms when IsEligibleToUseBitsOnExtension API latency breaches threshold"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"
}

resource "aws_cloudwatch_metric_alarm" "use_bits_on_extension_latency" {
  alarm_name          = "use-bits-on-extension-latency"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "3"
  metric_name         = "TWIRP:UseBitsOnExtension"
  namespace           = "payday"

  dimensions = {
    Stage   = "production"
    Region  = "us-west-2"
    Service = "payday"
  }

  period                    = "300"
  extended_statistic        = "p99"
  threshold                 = "2"
  alarm_description         = "Alarms when UseBitsOnExtension API latency breaches threshold"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"
}

# 500 Error Alarms
resource "aws_cloudwatch_metric_alarm" "is_eligible_to_use_bits_on_extension_500_errors" {
  alarm_name          = "is-eligible-to-use-bits-on-extension-500-errors"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "3"
  metric_name         = "TWIRP:IsEligibleToUseBitsOnExtension.5xx"
  namespace           = "payday"

  dimensions = {
    Stage   = "production"
    Region  = "us-west-2"
    Service = "payday"
  }

  period                    = "300"
  statistic                 = "Sum"
  threshold                 = "20"
  alarm_description         = "Alarms when IsEligibleToUseBitsOnExtension API 500 status code error count breaches threshold"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"
}

resource "aws_cloudwatch_metric_alarm" "use_bits_on_extension_500_errors" {
  alarm_name          = "use-bits-on-extension-500-errors"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "3"
  metric_name         = "TWIRP:UseBitsOnExtension.5xx"
  namespace           = "payday"

  dimensions = {
    Stage   = "production"
    Region  = "us-west-2"
    Service = "payday"
  }

  period                    = "300"
  statistic                 = "Sum"
  threshold                 = "20"
  alarm_description         = "Alarms when UseBitsOnExtension API 500 status code error count breaches threshold"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"
}

resource "aws_cloudwatch_metric_alarm" "partner_type_mismatch_alarm" {
  actions_enabled     = false
  alarm_name          = "prod_partner_type_bits_eligibility_mismatch"
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = "1"
  metric_name         = "emergency-disable-for-non-partner"
  namespace           = "payday"

  dimensions = {
    Stage   = "production"
    Region  = "us-west-2"
    Service = "payday"
  }

  period             = "300"
  statistic          = "Maximum"
  threshold          = "0"
  alarm_description  = "A non-partner, non-affiliate has Bits enabled. We have emergency disabled Bits for them"
  treat_missing_data = "missing"
}

resource "aws_cloudwatch_metric_alarm" "charity_max_limit_exceeded_alarm" {
  actions_enabled     = false
  alarm_name          = "prod_CharityMaxLimitExceeded"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "charity_exceeded_limit_count"
  namespace           = "payday"

  dimensions = {
    Stage   = "production"
    Region  = "us-west-2"
    Service = "payday"
  }

  period             = "300"
  statistic          = "Average"
  threshold          = "1"
  alarm_description  = "The charity total has exceeded the max limit"
  treat_missing_data = "notBreaching"
}

resource "aws_cloudwatch_metric_alarm" "get_products_missing_country_alarm" {
  actions_enabled     = false
  alarm_name          = "GetProducts_MissingCountryParam"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "3"
  metric_name         = "GetProducts_BadRequest_NoCountry"
  namespace           = "payday"

  dimensions = {
    Stage   = "production"
    Region  = "us-west-2"
    Service = "payday"
  }

  period             = "300"
  statistic          = "Sum"
  threshold          = "15"
  alarm_description  = "Received too many requests to the GetProducts API that were missing the country parameter."
  treat_missing_data = "notBreaching"
}

resource "aws_cloudwatch_metric_alarm" "pachinko_fixed_inflight_transaction" {
  actions_enabled     = false
  alarm_name          = "Pachinko_Fixed_Inflight_Transaction"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "Pachinko_Transaction_Fixed"
  namespace           = "payday"

  dimensions = {
    Stage   = "production"
    Region  = "us-west-2"
    Service = "payday"
  }

  period             = "30"
  statistic          = "Sum"
  threshold          = "1"
  alarm_description  = "The Pachinko worker had to fix a transaction that was inflight, this indicates that there was an issue completing a transaction in payday."
  treat_missing_data = "notBreaching"
}

module "get_balance_latency_alarms" {
  source               = "./modules/api_latency_alarm"
  api_name             = "GetBalanceVisage"
  statistic            = "p90"
  warning_threshold    = "0.7"
  include_paging_alarm = true
  paging_threshold     = "2"
}

module "get_balance_twirp_latency_alarms" {
  source               = "./modules/api_latency_alarm"
  api_name             = "TWIRP:GetBalance"
  statistic            = "p99"
  warning_threshold    = "0.1"
  include_paging_alarm = true
  paging_threshold     = "0.5"
}

module "get_balance_volume_alarm" {
  source    = "./modules/api_volume_alarm"
  api_name  = "TWIRP:GetBalance"
  threshold = "130000"
}

module "get_bits_to_brodcaster_latency_alarms" {
  source               = "./modules/api_latency_alarm"
  api_name             = "TWIRP:GetBitsToBroadcaster"
  statistic            = "p99"
  warning_threshold    = "0.1"
  include_paging_alarm = true
  paging_threshold     = "0.5"
}

module "get_bits_to_brodcaster_volume_alarm" {
  source    = "./modules/api_volume_alarm"
  api_name  = "TWIRP:GetBitsToBroadcaster"
  threshold = "15000"
}

module "get_channel_info_latency_alarms" {
  source               = "./modules/api_latency_alarm"
  api_name             = "GetChannelInfoVisage"
  statistic            = "p90"
  warning_threshold    = "0.1"
  include_paging_alarm = true
  paging_threshold     = "0.5"
}

module "get_channel_info_volume_alarm" {
  source    = "./modules/api_volume_alarm"
  api_name  = "GetChannelInfoVisage"
  threshold = "450000"
}

module "get_user_info_latency_alarms" {
  source               = "./modules/api_latency_alarm"
  api_name             = "GetUserInfoVisage"
  statistic            = "p90"
  warning_threshold    = "0.1"
  include_paging_alarm = true
  paging_threshold     = "0.5"
}

module "get_user_info_volume_alarm" {
  source    = "./modules/api_volume_alarm"
  api_name  = "GetUserInfoVisage"
  threshold = "85000"
}

module "get_prices_latency_alarms" {
  source               = "./modules/api_latency_alarm"
  api_name             = "GetPricesVisage"
  statistic            = "p99"
  warning_threshold    = "0.1"
  include_paging_alarm = true
  paging_threshold     = "0.5"
}

module "get_prices_volume_alarm" {
  source    = "./modules/api_volume_alarm"
  api_name  = "GetPricesVisage"
  threshold = "7000"
}

module "get_products_latency_alarms" {
  source               = "./modules/api_latency_alarm"
  api_name             = "GetProducts"
  statistic            = "p90"
  warning_threshold    = "1"
  include_paging_alarm = true
  paging_threshold     = "2"
  period               = "60"
}

module "get_products_volume_alarm" {
  source    = "./modules/api_volume_alarm"
  api_name  = "GetProducts"
  threshold = "13000"
}

module "get_bits_usage_leaderboard_latency_alarm" {
  source               = "./modules/api_latency_alarm"
  api_name             = "GetBitsUsageLeaderboard"
  statistic            = "p99"
  warning_threshold    = "0.1"
  include_paging_alarm = true
  paging_threshold     = "0.5"
  period               = "60"
  evaluation_periods   = "5"
  missing_data         = "notBreaching"
}

module "get_actions_latency_alarms" {
  source               = "./modules/api_latency_alarm"
  api_name             = "GetActions"
  statistic            = "p99"
  warning_threshold    = "0.4"
  include_paging_alarm = true
  paging_threshold     = "0.75"
  period               = "60"
  evaluation_periods   = "5"
}

module "get_actions_volume_alarm" {
  source    = "./modules/api_volume_alarm"
  api_name  = "GetActions"
  threshold = "180000"
}

module "entitle_bits_latency_alarms" {
  source               = "./modules/api_latency_alarm"
  api_name             = "EntitleBits"
  metric_name          = "POST:stringPattern(\"/authed/entitleBits\")"
  statistic            = "p90"
  warning_threshold    = "1"
  include_paging_alarm = true
  paging_threshold     = "2"
  period               = "60"
  evaluation_periods   = "5"
}

module "entitle_bits_volume_alarm" {
  source      = "./modules/api_volume_alarm"
  api_name    = "EntitleBits"
  metric_name = "POST:stringPattern(\"/authed/entitleBits\")"
  threshold   = "10"
}

module "get_channel_settings_latency_alarm" {
  source                    = "./modules/api_latency_alarm"
  api_name                  = "GetChannelSettingsVisage"
  statistic                 = "p90"
  warning_threshold         = "1.5"
  missing_data              = "notBreaching"
  low_sample_count_evaluate = "ignore"
}

module "update_channel_settings_latency_alarm" {
  source                    = "./modules/api_latency_alarm"
  api_name                  = "UpdateChannelSettingsVisage"
  statistic                 = "p99"
  warning_threshold         = "0.2"
  missing_data              = "notBreaching"
  low_sample_count_evaluate = "ignore"
}

module "get_badge_tiers_visage_latency_alarm" {
  source            = "./modules/api_latency_alarm"
  api_name          = "GetBadgeTiersVisage"
  statistic         = "p99"
  warning_threshold = "0.5"
  missing_data      = "notBreaching"
}

module "set_badge_tiers_visage_latency_alarm" {
  source            = "./modules/api_latency_alarm"
  api_name          = "SetBadgeTiersVisage"
  statistic         = "p99"
  warning_threshold = "3"
  missing_data      = "notBreaching"
}

module "start_custom_cheermote_upload_latency_alarm" {
  source            = "./modules/api_latency_alarm"
  api_name          = "StartCustomCheermoteUpload"
  statistic         = "p99"
  warning_threshold = "2.0"
  missing_data      = "notBreaching"
}

module "amazon_purchase_volume_alarm" {
  source    = "./modules/purchase_volume_alarm"
  platform  = "Amazon"
  threshold = "1"
}

module "paypal_purchase_volume_alarm" {
  source    = "./modules/purchase_volume_alarm"
  platform  = "Paypal"
  threshold = "2"
}

module "ios_purchase_volume_alarm" {
  source    = "./modules/purchase_volume_alarm"
  platform  = "iOS"
  threshold = "1"
}

module "android_purchase_volume_alarm" {
  source    = "./modules/purchase_volume_alarm"
  platform  = "Android"
  threshold = "1"
}

module "xsolla_purchase_volume_alarm" {
  source    = "./modules/purchase_volume_alarm"
  platform  = "Xsolla"
  threshold = "1"
}

module "application_5xx_volume_alarm" {
  source            = "./modules/application_status_code_alarm"
  threshold         = "100"
  status_code_range = "5xx"
  environment_name  = "payday-production-beanstalk"
}

module "emote_entitlement_failure__get_group_id_alarm" {
  source                = "./modules/entitlement_failure_alarm"
  alarm_name            = "emote_entitlement_failure__get_group_id_alarm"
  alarm_description     = "A user should be entitled a bits emote but get group id failed. Check the logs for 'emote entitlement' to find the channel/user/threshold and then manually entitle them to that emote slot"
  metric_name           = "emote_entitlement_failure__get_group_id"
  treat_missing_data_as = "notBreaching"
}

module "emote_entitlement_failure_alarm" {
  source                = "./modules/entitlement_failure_alarm"
  alarm_name            = "emote_entitlement_failure__send_sns_message_alarm"
  alarm_description     = "A user should be entitled a bits emote but sending the sns message failed. Check the logs for 'emote entitlement' to find the channel/user/threshold and then manually entitle them to that emote slot"
  metric_name           = "emote_entitlement_failure__send_sns_message"
  treat_missing_data_as = "notBreaching"
}

resource "aws_cloudwatch_metric_alarm" "extra_emote_attached_to_bits_tier_reward" {
  actions_enabled     = false
  alarm_name          = "bits_tier_emote_rewards_extra_emote_attached_to_bits_tier_reward"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "ExtraEmoteAttachedToBitsTier"
  namespace           = "payday"

  dimensions = {
    Stage   = "production"
    Region  = "us-west-2"
    Service = "payday"
  }

  period             = "300"
  statistic          = "Sum"
  threshold          = "1"
  alarm_description  = "Found a instance where Mako said there was more than one emote in the reward tier for cheering/extensions/polls/etc"
  treat_missing_data = "notBreaching"
}

module "get_bits_badge_tier_emotes_latency_alarms" {
  source               = "./modules/api_latency_alarm"
  api_name             = "TWIRP:GetBitsBadgeTierEmotes"
  statistic            = "p90"
  warning_threshold    = "0.3"
  paging_threshold     = "0.7"
  include_paging_alarm = true
}

module "get_bits_badge_tier_emotes_volume_alarm" {
  source    = "./modules/api_volume_alarm"
  api_name  = "TWIRP:GetBitsBadgeTierEmotes"
  threshold = "90000"
}

module "get_bits_badge_tier_emotes_5xx_alarm" {
  source    = "./modules/api_5xx_alarm"
  api_name  = "TWIRP:GetBitsBadgeTierEmotes"
  threshold = "600"
}

module "update_cheermote_tier_latency_alarms" {
  source               = "./modules/api_latency_alarm"
  api_name             = "TWIRP:UpdateCheermoteTier"
  statistic            = "p90"
  warning_threshold    = "5.0"
  paging_threshold     = "6.0"
  include_paging_alarm = true
  missing_data = "notBreaching"
}

module "update_cheermote_tier_emotes_5xx_alarm" {
  source    = "./modules/api_5xx_alarm"
  api_name  = "TWIRP:UpdateCheermoteTier"
  threshold = "5"
}

module "delete_cheermote_tier_latency_alarms" {
  source               = "./modules/api_latency_alarm"
  api_name             = "TWIRP:DeleteCheermoteTier"
  statistic            = "p90"
  warning_threshold    = "3.0"
  paging_threshold     = "5.0"
  include_paging_alarm = true
  missing_data = "notBreaching"
}

module "delete_cheermote_tier_emotes_5xx_alarm" {
  source    = "./modules/api_5xx_alarm"
  api_name  = "TWIRP:DeleteCheermoteTier"
  threshold = "10"
}

module "assign_emote_to_bits_tier_latency_alarms" {
  source               = "./modules/api_latency_alarm"
  api_name             = "TWIRP:AssignEmoteToBitsTier"
  statistic            = "p90"
  warning_threshold    = "0.2"
  paging_threshold     = "0.3"
  include_paging_alarm = true
  missing_data = "notBreaching"
}

module "assign_emote_to_bits_tier_5xx_alarm" {
  source    = "./modules/api_5xx_alarm"
  api_name  = "TWIRP:AssignEmoteToBitsTier"
  threshold = "5"
}

module "get_bits_tier_emote_groups_latency_alarms" {
  source               = "./modules/api_latency_alarm"
  api_name             = "TWIRP:GetBitsTierEmoteGroups"
  statistic            = "p90"
  warning_threshold    = "0.1"
  paging_threshold     = "0.2"
  include_paging_alarm = true
  missing_data = "notBreaching"
}

module "get_bits_tier_emote_groups_5xx_alarm" {
  source    = "./modules/api_5xx_alarm"
  api_name  = "TWIRP:GetBitsTierEmoteGroups"
  threshold = "100"
}
