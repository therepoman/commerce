# Production
# n machines per AZ
# three AZs

# elastic network interfaces & ip addresses
resource "aws_eip" "nat-2a" {
  vpc = true
}

resource "aws_eip" "nat-2b" {
  vpc = true
}

resource "aws_eip" "nat-2c" {
  vpc = true
}

resource "aws_subnet" "production_subnet_a" {
  vpc_id                  = var.vpc_id
  availability_zone       = "us-west-2a"
  cidr_block              = var.cidr_prod_a
  map_public_ip_on_launch = false

  tags = {
    Name  = "${var.name}-prod-private-2a"
    Owner = var.owner
  }
}

resource "aws_nat_gateway" "production_subnet_a_nat" {
  allocation_id = aws_eip.nat-2a.id
  subnet_id     = aws_subnet.pub-2a.id
}

module "production_route_table_a" {
  source                           = "./modules/route_table"
  az                               = "a"
  nat_gateway_id                   = aws_nat_gateway.production_subnet_a_nat.id
  subnet_id                        = aws_subnet.production_subnet_a.id
  twitch_aws_peering_connection_id = aws_vpc_peering_connection.twitch-aws-prod.id
  vpc_id                           = var.vpc_id
  stage                            = "prod"

  pantheon_cidr_block            = "10.201.184.0/22"
  pantheon_peering_connection_id = "pcx-0d69a6ed881ad05b8"

  prism_cidr_block            = "10.204.252.0/22"
  prism_peering_connection_id = "pcx-0ddf8cfb7b4314045"

  petozi_cidr_block            = "10.205.180.0/22"
  petozi_peering_connection_id = "pcx-0de4ab62f783234eb"

  pagle_cidr_block            = "10.205.196.0/22"
  pagle_peering_connection_id = "pcx-02c4277a5d2cce213"

  pachter_cidr_block            = "10.205.208.0/22"
  pachter_peering_connection_id = aws_vpc_peering_connection.pachter-prod-peering.id

  upload_service_cidr_block            = "10.205.204.0/22"
  upload_service_peering_connection_id = aws_vpc_peering_connection.upload-service-prod-peering.id

  chat_cidr_block            = "10.193.192.0/18"
  chat_peering_connection_id = aws_vpc_peering_connection.chat-peering.id

  pachinko_cidr_block            = "10.205.0.0/22"
  pachinko_peering_connection_id = aws_vpc_peering_connection.pachinko-prod-peering.id

  visage_cidr_block            = "10.205.216.0/22"
  visage_peering_connection_id = aws_vpc_peering_connection.visage-prod-peering.id

  poliwag_cidr_block            = "10.206.156.0/22"
  poliwag_peering_connection_id = "pcx-0d9d9f85f6da74c22"

  mako_cidr_block            = "10.201.96.0/22"
  mako_peering_connection_id = "pcx-05ee4637dd6aaa536"

  revenue_reporting_worker_cidr_block            = "10.206.212.0/22"
  revenue_reporting_worker_peering_connection_id = aws_vpc_peering_connection.revenue-reporting-worker-prod-peering.id

  amp_cidr_block    = "172.28.0.0/16"
  amp_connection_id = "pcx-0db978eef9ccb35a8"

  helix_cidr_block    = "10.205.36.0/22"
  helix_connection_id = "pcx-022de5f407070f26b"
}

resource "aws_subnet" "production_subnet_b" {
  vpc_id                  = var.vpc_id
  availability_zone       = "us-west-2b"
  cidr_block              = var.cidr_prod_b
  map_public_ip_on_launch = false

  tags = {
    Name  = "${var.name}-prod-private-2b"
    Owner = var.owner
  }
}

resource "aws_nat_gateway" "prod_subnet_b_nat" {
  allocation_id = aws_eip.nat-2b.id
  subnet_id     = aws_subnet.pub-2b.id
}

module "prod_route_table_b" {
  source                           = "./modules/route_table"
  az                               = "b"
  nat_gateway_id                   = aws_nat_gateway.prod_subnet_b_nat.id
  subnet_id                        = aws_subnet.production_subnet_b.id
  twitch_aws_peering_connection_id = aws_vpc_peering_connection.twitch-aws-prod.id
  vpc_id                           = var.vpc_id
  stage                            = "prod"

  pantheon_cidr_block            = "10.201.184.0/22"
  pantheon_peering_connection_id = "pcx-0d69a6ed881ad05b8"

  prism_cidr_block            = "10.204.252.0/22"
  prism_peering_connection_id = "pcx-0ddf8cfb7b4314045"

  petozi_cidr_block            = "10.205.180.0/22"
  petozi_peering_connection_id = "pcx-0de4ab62f783234eb"

  pagle_cidr_block            = "10.205.196.0/22"
  pagle_peering_connection_id = "pcx-02c4277a5d2cce213"

  pachter_cidr_block            = "10.205.208.0/22"
  pachter_peering_connection_id = aws_vpc_peering_connection.pachter-prod-peering.id

  upload_service_cidr_block            = "10.205.204.0/22"
  upload_service_peering_connection_id = aws_vpc_peering_connection.upload-service-prod-peering.id

  chat_cidr_block            = "10.193.192.0/18"
  chat_peering_connection_id = aws_vpc_peering_connection.chat-peering.id

  pachinko_cidr_block            = "10.205.0.0/22"
  pachinko_peering_connection_id = aws_vpc_peering_connection.pachinko-prod-peering.id

  visage_cidr_block            = "10.205.216.0/22"
  visage_peering_connection_id = aws_vpc_peering_connection.visage-prod-peering.id

  poliwag_cidr_block            = "10.206.156.0/22"
  poliwag_peering_connection_id = "pcx-0d9d9f85f6da74c22"

  mako_cidr_block            = "10.201.96.0/22"
  mako_peering_connection_id = "pcx-05ee4637dd6aaa536"

  revenue_reporting_worker_cidr_block            = "10.206.212.0/22"
  revenue_reporting_worker_peering_connection_id = aws_vpc_peering_connection.revenue-reporting-worker-prod-peering.id

  amp_cidr_block    = "172.28.0.0/16"
  amp_connection_id = "pcx-0db978eef9ccb35a8"

  helix_cidr_block    = "10.205.36.0/22"
  helix_connection_id = "pcx-022de5f407070f26b"
}

resource "aws_subnet" "prod_subnet_c" {
  vpc_id                  = var.vpc_id
  availability_zone       = "us-west-2c"
  cidr_block              = var.cidr_prod_c
  map_public_ip_on_launch = false

  tags = {
    Name  = "${var.name}-prod-private-2c"
    Owner = var.owner
  }
}

resource "aws_nat_gateway" "prod_subnet_c_nat" {
  allocation_id = aws_eip.nat-2c.id
  subnet_id     = aws_subnet.pub-2c.id
}

module "prod_route_table_c" {
  source                           = "./modules/route_table"
  az                               = "c"
  nat_gateway_id                   = aws_nat_gateway.prod_subnet_c_nat.id
  subnet_id                        = aws_subnet.prod_subnet_c.id
  twitch_aws_peering_connection_id = aws_vpc_peering_connection.twitch-aws-prod.id
  vpc_id                           = var.vpc_id
  stage                            = "prod"

  pantheon_cidr_block            = "10.201.184.0/22"
  pantheon_peering_connection_id = "pcx-0d69a6ed881ad05b8"

  prism_cidr_block            = "10.204.252.0/22"
  prism_peering_connection_id = "pcx-0ddf8cfb7b4314045"

  petozi_cidr_block            = "10.205.180.0/22"
  petozi_peering_connection_id = "pcx-0de4ab62f783234eb"

  pagle_cidr_block            = "10.205.196.0/22"
  pagle_peering_connection_id = "pcx-02c4277a5d2cce213"

  pachter_cidr_block            = "10.205.208.0/22"
  pachter_peering_connection_id = aws_vpc_peering_connection.pachter-prod-peering.id

  upload_service_cidr_block            = "10.205.204.0/22"
  upload_service_peering_connection_id = aws_vpc_peering_connection.upload-service-prod-peering.id

  chat_cidr_block            = "10.193.192.0/18"
  chat_peering_connection_id = aws_vpc_peering_connection.chat-peering.id

  pachinko_cidr_block            = "10.205.0.0/22"
  pachinko_peering_connection_id = aws_vpc_peering_connection.pachinko-prod-peering.id

  visage_cidr_block            = "10.205.216.0/22"
  visage_peering_connection_id = aws_vpc_peering_connection.visage-prod-peering.id

  poliwag_cidr_block            = "10.206.156.0/22"
  poliwag_peering_connection_id = "pcx-0d9d9f85f6da74c22"

  mako_cidr_block            = "10.201.96.0/22"
  mako_peering_connection_id = "pcx-05ee4637dd6aaa536"

  revenue_reporting_worker_cidr_block            = "10.206.212.0/22"
  revenue_reporting_worker_peering_connection_id = aws_vpc_peering_connection.revenue-reporting-worker-prod-peering.id

  amp_cidr_block    = "172.28.0.0/16"
  amp_connection_id = "pcx-0db978eef9ccb35a8"

  helix_cidr_block    = "10.205.36.0/22"
  helix_connection_id = "pcx-022de5f407070f26b"
}
