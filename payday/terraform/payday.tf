# define variables, generally self descriptive

variable "owner" {
  default = "bits-commerce@justin.tv"
}
variable "name" {
  default = "payday"
}
variable "service" {
  default = "commerce/payday"
}
variable "cluster" {
  default = "payday"
}
variable "environment" {
  default = "prod"
}
variable "twitch_environment" {
  default = "production"
}
variable "nat_ami" {
  default = "ami-75ae8245" # amzn-ami-vpc-nat-hvm-2015.03.0.x86_64-ebs
}
variable "ami" {
  default = "ami-5e26c36d"
}
variable "type" {
  default = "c3.xlarge"
}
variable "instances" {
  default = 1
}
variable "sfo_vgsw" {
  default = "vgw-7c16c962"
}
variable "twitch-aws_cidr" {
  default = "10.192.64.0/18"
}
variable "twitch-aws_peer" {
  default = "pcx-fdeb2794"
}
variable "twitch-aws_peer_owner" {
  default = "673385534282"
}
variable "twitch-aws_peer_vpc_id" {
  default = "vpc-0213b167"
}

# CIDR BLOCK ALLOCATIONS FOR THIS ACCOUNT
variable "cidr" { # the CIDR range we got from systems for the whole VPC
  default = "10.193.64.0/18"
}

# We break TWICH AWS into 4 main CIDR Blocks:

# PRIVATE A /20 - 4096 Private Prod hosts for Availability Zone A
variable "cidr_prod_a" {
  default = "10.193.64.0/20"
}

# PRIVATE B /20 - 4096 Private Prod hosts for Availability Zone B
variable "cidr_prod_b" {
  default = "10.193.80.0/20"
}

# PRIVATE C /20 - 4096 Private Prod hosts for Availability Zone C
variable "cidr_prod_c" {
  default = "10.193.96.0/20"
}

# REST /20 - The other 4k addresses which are split as follows:

variable "cidr_non_prod" {
  default = "10.193.112.0/20"
}
#   STAGE /22 - 1024 Hosts for Staging Boxes
variable "cidr_stage" {
  default = "10.193.112.0/22"
}
#     STAGE A /24 - 256 Private A Staging Hosts
variable "cidr_stage_a" {
  default = "10.193.112.0/24"
}
#     STAGE B /24 - 256 Private B Staging Hosts
variable "cidr_stage_b" {
  default = "10.193.113.0/24"
}
#     STAGE C /24 - 256 PRIVATE C Staging Hosts
variable "cidr_stage_c" {
  default = "10.193.114.0/24"
}
#     RESERVED /24 - 256 Additional unallocated hosts for staging
variable "cidr_stage_reserved" {
  default = "10.193.115.0/24"
}
#   DEV /22 - 1024 Hosts for Dev Activities
variable "cidr_dev" {
  default = "10.193.116.0/22"
}
#     DEV A /24 - 256 Private A Hosts
variable "cidr_dev_a" {
  default = "10.193.116.0/24"
}
#     DEV B /24 - 256 Private B Hosts
variable "cidr_dev_b" {
  default = "10.193.117.0/24"
}
#     DEV C /24 - 256 Private C Hosts
variable "cidr_dev_c" {
  default = "10.193.118.0/24"
}
#     DEV RESERVED /24 - 256 Private RESERVED Hosts
variable "cidr_dev_rest" {
  default = "10.193.119.0/24"
}
#   PUB /22 - 1024 Hosts with Public Access
variable "cidr_pub" {
  default = "10.193.120.0/22"
}
#     PUB A /24 - 256 Public A Hosts
variable "cidr_pub_a" {
  default = "10.193.120.0/24"
}
#     PUB B /24 - 256 Public B Hosts
variable "cidr_pub_b" {
  default = "10.193.121.0/24"
}
#     PUB C /24 - 256 Public C Hosts
variable "cidr_pub_c" {
  default = "10.193.122.0/24"
}
#     PUB RESERVED /24 - 256 Public RESERVED Hosts
variable "cidr_pub_reserved" {
  default = "10.193.123.0/24"
}

#Misc cidr blocks that belong to twitch, needed for redshift access.
variable "cidr_misc_redshift_staging"{
	default = "185.42.204.0/22,127.0.0.1/32,38.99.10.97/32,192.168.0.0/16,45.113.128.0/22,103.53.48.0/22,192.108.239.0/24,23.160.0.0/24,192.168.240.0/21"
}

variable "cidr_twitch_vpn_1" { # cidr for users coming in over the twitch vpn
  default = "10.255.252.0/23"
}
variable "cidr_twitch_vpn_2" { # cidr for users coming in over the twitch vpn
  default = "10.255.254.0/23"
}
variable "cidr_twitch_office" { # cidr for users coming in from twitch offices
  default = "192.168.0.0/17"
}
variable "cidr_twitch_nat1" { # ?
  default = "38.99.10.96/29"
}
variable "cidr_twitch_nat2" { # ?
  default = "38.99.7.224/27"
}
variable "cidr_private" { # ? All Private Space
  default = "10.0.0.0/8"
}
variable "cidr_twitch_public1" { # twitch.tv prod hosts (e.g. tmi.twitch.tv)
  default = "199.9.248.0/21"
}
variable "cidr_twitch_public2" { # more twitch.tv prod hosts
  default = "192.16.64.0/21"
}
variable "cidr_s3_proxy" {
  default = "199.9.0.0/16"
}
variable "cidr_twitch_ntp1" {
  default = "52.223.252.125/32"
}
variable "cidr_twitch_ntp2" {
  default = "52.223.252.124/32"
}
variable "cidr_twitch_ntp3" {
  default = "52.223.252.92/32"
}
variable "cidr_twitch_ntp4" {
  default = "52.223.252.58/32"
}
variable "nat_key" {
  default = "prod-nat"
}
variable "sandstorm_agent_role_arn" {
  default = "arn:aws:iam::734326455073:role/sandstorm-agent-payday"
}

# module that contains twitch office subnet CIDRs
module "tf_twitch_subnets" {
  source = "git::git+ssh://git@git.xarth.tv/dta/tf_twitch_subnets.git?ref=c5610122d181c95382678b04384363408b3f97c0"
}

# set the default AWS region to us-west-2 (this is where our direct connect is)
provider "aws" {
  region  = "us-west-2"
}
# provider for route53 stuff and sandstorm roles & permissions
variable "twitch-aws_access_key" {}
variable "twitch-aws_secret_key" {}
provider "aws" {
  alias = "twitch-aws"
  access_key = var.twitch-aws_access_key
  secret_key = var.twitch-aws_secret_key
  region = "us-west-2"
}

# at this point we should have a vpc, dns, internet gateway, and dhcp

# create a vpc to vpc connection with twitch's main aws account
resource "aws_vpc_peering_connection" "twitch-aws-prod" {
  peer_owner_id = var.twitch-aws_peer_owner
  peer_vpc_id = var.twitch-aws_peer_vpc_id
  vpc_id = var.vpc_id
  tags = {
    Name =  "twitch-aws-owners"
  }
}

# Pachter Peer
resource "aws_vpc_peering_connection" "pachter-devo-peering" {
  peer_owner_id = "721938063588" #Account Id number of Pachter devo AWS account
  peer_vpc_id   = "vpc-08e709775fdcd16d0"
  vpc_id        = var.vpc_id
  tags = {
    Name = "twitch-pachter-aws-devo"
  }
}

resource "aws_vpc_peering_connection" "pachter-prod-peering" {
  peer_owner_id = "458492755823" #Account Id number of Pachter prod AWS account
  peer_vpc_id   = "vpc-0bab2e20261adf6c7"
  vpc_id        = var.vpc_id
  tags = {
    Name = "twitch-pachter-aws-prod"
  }
}

# Pachinko Peer
resource "aws_vpc_peering_connection" "pachinko-prod-peering" {
  peer_owner_id = "702056039310"
  peer_vpc_id   = "vpc-03f9d62c97421b8e4"
  vpc_id        = var.vpc_id
  tags = {
    Name = "twitch-pachinko-aws-prod"
  }
}

resource "aws_vpc_peering_connection" "pachinko-devo-peering" {
  peer_owner_id = "946879801923"
  peer_vpc_id   = "vpc-089fc3b0b26bcac4a"
  vpc_id        = var.vpc_id
  tags = {
    Name = "twitch-pachinko-aws-devo"
  }
}

# Visage peer
resource "aws_vpc_peering_connection" "visage-devo-peering" {
  peer_owner_id = "628671981940" #Account Id number of Visage devo AWS account
  peer_vpc_id   = "vpc-025db2a4228ed53dc"
  vpc_id        = var.vpc_id
  tags = {
    Name = "twitch-visage-dev"
  }
}

resource "aws_vpc_peering_connection" "visage-prod-peering" {
  peer_owner_id = "736332675735" #Account Id number of Visage prod AWS account
  peer_vpc_id   = "vpc-085642144b5ed6d09"
  vpc_id        = var.vpc_id
  tags = {
    Name = "twitch-visage-prod"
  }
}

# Upload service peer
resource "aws_vpc_peering_connection" "upload-service-devo-peering" {
  peer_owner_id = "995367761609" #Account Id number of Upload Service Devo AWS account
  peer_vpc_id = "vpc-06debe7b4986a6682"
  vpc_id = var.vpc_id
  tags = {
    Name = "twitch-asset-uploads-dev"
  }
}

resource "aws_vpc_peering_connection" "upload-service-prod-peering" {
  peer_owner_id = "435569175256" #Account Id number of Upload Service Prod AWS account
  peer_vpc_id = "vpc-0bfe7448c0f91996f"
  vpc_id = var.vpc_id
  tags = {
    Name = "twitch-asset-uploads-prod"
  }
}

# Chat peer
resource "aws_vpc_peering_connection" "chat-peering" {
  peer_owner_id = "603200399373" #Account Id number of Chat AWS account
  peer_vpc_id = "vpc-6183fe04"
  vpc_id = var.vpc_id
  tags = {
    Name = "twitch-chat-aws"
  }
}

# Revenue worker service peer
resource "aws_vpc_peering_connection" "revenue-reporting-worker-devo-peering" {
  peer_owner_id = "620988894980" #Account Id number of Revenue Worker Service Devo AWS account
  peer_vpc_id = "vpc-09e459e62d0885d68"
  vpc_id = var.vpc_id
  tags = {
    Name = "twitch-revenue-reporting-workers-dev"
  }
}

resource "aws_vpc_peering_connection" "revenue-reporting-worker-prod-peering" {
  peer_owner_id = "581227035814" #Account Id number of Revenue Worker Service Prod AWS account
  peer_vpc_id = "vpc-0eee98934d6e4a8ad"
  vpc_id = var.vpc_id
  tags = {
    Name = "twitch-revenue-reporting-workers-prod"
  }
}

resource "aws_vpc_peering_connection" "twitch-api-dev" {
  peer_owner_id = "327140220177" #Account Id number of Twitch API Dev AWS account (helix)
  peer_vpc_id = "vpc-0146fe67faad47fd7"
  vpc_id = var.vpc_id
  tags = {
    Name = "twitch-api-dev"
  }
}

resource "aws_vpc_peering_connection" "twitch-api-prod" {
  peer_owner_id = "028439334451" #Account Id number of Twitch API Prod AWS account (helix)
  peer_vpc_id = "vpc-0d4a45c3c5dd5180c"
  vpc_id = var.vpc_id
  tags = {
    Name = "twitch-api-prod"
  }
}

# create the routing rules which govern traffic in and out of the public (nat) subnets
resource "aws_route_table" "nat" {
  vpc_id = var.vpc_id
  tags = {
    Name = "${var.name}-${var.environment}-route-nat"
    Owner = var.owner
  }

  # internet gateway route
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "igw-a79b0fc2"
  }

  # twitch corp
  route {
    cidr_block = var.cidr_private
    gateway_id = var.sfo_vgsw
  }

  # prism prod nat
  route {
    cidr_block = "10.204.252.0/22"
    vpc_peering_connection_id = "pcx-0ddf8cfb7b4314045"
  }
}

# public subnets
resource "aws_subnet" "pub-2a" {
  vpc_id = var.vpc_id
  cidr_block = var.cidr_pub_a
  map_public_ip_on_launch = false
  availability_zone = "us-west-2a"

  tags = {
    Name = "${var.name}-${var.environment}-2a-pub"
    Owner = var.owner
  }
}
resource "aws_route_table_association" "pub-2a" {
  subnet_id = aws_subnet.pub-2a.id
  route_table_id = aws_route_table.nat.id
}

resource "aws_subnet" "pub-2b" {
  vpc_id = var.vpc_id
  cidr_block = var.cidr_pub_b
  map_public_ip_on_launch = false
  availability_zone = "us-west-2b"

  tags = {
    Name = "${var.name}-${var.environment}-2b-pub"
    Owner = var.owner
  }
}
resource "aws_route_table_association" "pub-2b" {
  subnet_id = aws_subnet.pub-2b.id
  route_table_id = aws_route_table.nat.id
}

resource "aws_subnet" "pub-2c" {
  vpc_id = var.vpc_id
  cidr_block = var.cidr_pub_c
  map_public_ip_on_launch = false
  availability_zone = "us-west-2c"

  tags = {
    Name = "${var.name}-${var.environment}-2c-pub"
    Owner = var.owner
  }
}
resource "aws_route_table_association" "pub-2c" {
  subnet_id = aws_subnet.pub-2c.id
  route_table_id = aws_route_table.nat.id
}

# host security group
resource "aws_security_group" "private-sg"{
  name = "${var.name}-${var.environment}-private-sg"
  description = "Limit access other boxes that are part of Security Group for dev"
  vpc_id = var.vpc_id
  tags = {
    Name = "${var.name}-${var.environment}-private-sg"
    Owner = var.owner
  }

  # allow all traffic from self
  ingress {
    from_port = 0
    to_port = 0
    protocol = -1
    self = true
  }

  # allow traffic from the ELB on port 8000 from twitchazon
  ingress {
    from_port = 8000
    to_port = 8000
    protocol = "tcp"
    cidr_blocks = split(",", module.tf_twitch_subnets.twitch_subnets)

  }
  ingress {
    from_port = 8001
    to_port = 8001
    protocol = "tcp"
    cidr_blocks = split(",", module.tf_twitch_subnets.twitch_subnets)
  }

  # Consul health check
  ingress {
    from_port = 8306
    to_port = 8306
    protocol = "tcp"
    cidr_blocks = split(",", module.tf_twitch_subnets.twitch_subnets)
  }

  # allow ssh from twitchazon
  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = split(",", module.tf_twitch_subnets.twitch_subnets)
  }

  # allow http out to the world from private
  egress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # allow https out to the world from private
  egress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # consul uses random ports, for now allow all exgress to private
  egress {
    from_port = 0
    to_port = 0
    protocol = -1
    cidr_blocks = [var.cidr_private]
  }

  # egress to s3 proxy
  egress {
    from_port = 9797
    to_port = 9797
    protocol = "tcp"
    cidr_blocks = [var.cidr_s3_proxy]
  }

  # egress to tmi.twitch.tv
  egress {
    from_port = 6190
    to_port = 6190
    protocol = "tcp"
    cidr_blocks = [var.cidr_twitch_public1, var.cidr_twitch_public2]
  }

  # egress for ntp
  egress {
    from_port = 123
    to_port = 123
    protocol = "udp"
    cidr_blocks = [var.cidr_twitch_ntp1, var.cidr_twitch_ntp2, var.cidr_twitch_ntp3, var.cidr_twitch_ntp4]
  }

  # ingress for ntp
  ingress {
    from_port = 123
    to_port = 123
    protocol = "udp"
    cidr_blocks = [var.cidr_twitch_ntp1, var.cidr_twitch_ntp2, var.cidr_twitch_ntp3, var.cidr_twitch_ntp4]
  }
}

resource "aws_security_group" "elb" {
  name = "${var.name}-prod-elb"
  description = "security group for paydays elb"
  vpc_id = var.vpc_id
  tags = {
    Name = "${var.name}-prod-elb-sg"
    Owner = var.owner
  }

  # allow https in from private
  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = split(",", module.tf_twitch_subnets.twitch_subnets)
  }
  ingress {
    from_port = 443
    to_port = 444
    protocol = "tcp"
    cidr_blocks = split(",", module.tf_twitch_subnets.twitch_subnets)
  }

  # allow https out to private (prod) ips
  egress {
    from_port = 8000
    to_port = 8001
    protocol = "tcp"
    cidr_blocks = [
      var.cidr_prod_a,
      var.cidr_prod_b,
      var.cidr_prod_c
    ]
  }
}

resource "aws_security_group" "staging_elb" {
  name = "${var.name}-staging-elb"
  description = "security group for payday staging elb"
  vpc_id = var.vpc_id
  tags = {
    Name = "${var.name}-staging-elb-sg"
    Owner = var.owner
  }

  # allow https in from private
  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = split(",", module.tf_twitch_subnets.twitch_subnets)
  }
  ingress {
    from_port = 443
    to_port = 444
    protocol = "tcp"
    cidr_blocks = split(",", module.tf_twitch_subnets.twitch_subnets)
  }

  # allow https out to private ips (staging)
  egress {
    from_port = 8000
    to_port = 8001
    protocol = "tcp"
    cidr_blocks = [
      var.cidr_stage_a,
      var.cidr_stage_b,
      var.cidr_stage_c
    ]
  }
}

resource "aws_iam_instance_profile" "payday_prod" {
  name = "payday_prod"
  role = aws_iam_role.payday_prod.name
}

resource "aws_iam_role" "payday_prod" {
  name = "payday_prod"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_instance_profile" "payday_staging" {
  name = "payday_staging"
  role = aws_iam_role.payday_staging.name
}

resource "aws_iam_role" "payday_staging" {
  name = "payday_staging"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

data "aws_iam_policy_document" "sandstorm_in_memory_staging_policy_document" {
  statement {
    actions = [
      "sts:AssumeRole",
    ]

    resources = [
      var.sandstorm_agent_role_arn,
      "arn:aws:iam::734326455073:role/sandstorm/production/templated/role/payday-staging"
    ]

    effect = "Allow"
  }

  statement {
    sid = "Stmt1473817796000"
    actions = [
      "s3:GetObject",
    ]

    resources = [
      "arn:aws:s3:::twitch-sandstorm/sandstorm-agent.rpm"
    ]

    effect = "Allow"
  }
}

resource "aws_iam_policy" "payday_staging_sandstorm_in_memory_policy" {
  name = "payday_staging_sandstorm_in_memory_policy"
  description = "permission to assume role for staging Sandstorm access"
  policy = data.aws_iam_policy_document.sandstorm_in_memory_staging_policy_document.json
}

resource "aws_iam_policy_attachment" "payday_staging_sandstorm_in_memory_policy_attachment" {
  name = "assume_payday_staging_sandstorm_in_memory_role"
  policy_arn = aws_iam_policy.payday_staging_sandstorm_in_memory_policy.arn
  roles = [aws_iam_role.payday_staging.name]
  users = ["payday-integration"]
}

data "aws_iam_policy_document" "sandstorm_in_memory_prod_policy_document" {
  statement {
    actions = [
      "sts:AssumeRole",
    ]

    resources = [
      var.sandstorm_agent_role_arn,
      "arn:aws:iam::734326455073:role/sandstorm/production/templated/role/payday-prod"
    ]

    effect = "Allow"
  }

  statement {
    sid = "Stmt1473817796000"
    actions = [
      "s3:GetObject",
    ]

    resources = [
      "arn:aws:s3:::twitch-sandstorm/sandstorm-agent.rpm"
    ]

    effect = "Allow"
  }
}

resource "aws_iam_policy" "payday_prod_sandstorm_in_memory_policy" {
  name = "payday_prod_sandstorm_in_memory_policy"
  description = "permission to assume role for prod Sandstorm access"
  policy = data.aws_iam_policy_document.sandstorm_in_memory_prod_policy_document.json
}

resource "aws_iam_policy_attachment" "payday_prod_sandstorm_in_memory_policy_attachment" {
  name = "assume_payday_prod_sandstorm_in_memory_role"
  policy_arn = aws_iam_policy.payday_prod_sandstorm_in_memory_policy.arn
  roles = [aws_iam_role.payday_prod.name]
}

# roles and policies for running cloudwatch logs agent to push Payday logs
resource "aws_iam_policy_attachment" "payday_push_logs" {
  name = "payday_push_logs"
  policy_arn = aws_iam_policy.push_logs.arn
  roles = [aws_iam_role.payday_prod.name, aws_iam_role.payday_staging.name]
}

resource "aws_iam_policy" "push_logs" {
  name = "push_logs"
  description = "push logs to cloudwatch and read config files from S3"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents",
        "logs:DescribeLogStreams"
      ],
      "Resource": [
        "arn:aws:logs:*:*:*"
      ]
    },
    {
      "Effect": "Allow",
      "Action": [
        "s3:GetObject"
      ],
      "Resource": [
        "arn:aws:s3:::cloudwatch-logs-configs/*"
      ]
    }
  ]
}
EOF
}

# Onboard payday to eventbus IAM policies and roles
# https://git-aws.internal.justin.tv/pages/eventbus/docs/getting_started/#run-the-cloudformation-template
resource "aws_cloudformation_stack" "eventbus" {
  name = "EventBus"
  capabilities = ["CAPABILITY_NAMED_IAM"]
  template_url = "https://eventbus-setup.s3-us-west-2.amazonaws.com/cloudformation.yaml?versionId=NlgQUug16ehUVi1IZ3ZVxHeYpQ1Ek.9K"
}

resource "aws_iam_role_policy_attachment" "eventbus_access_prod_attach" {
  role       = aws_iam_role.payday_prod.name
  policy_arn = aws_cloudformation_stack.eventbus.outputs["EventBusAccessPolicyARN"]
}

resource "aws_iam_role_policy_attachment" "eventbus_access_staging_attach" {
  role       = aws_iam_role.payday_staging.name
  policy_arn = aws_cloudformation_stack.eventbus.outputs["EventBusAccessPolicyARN"]
}

# roles and policies for lambda execution
resource "aws_iam_role" "payday_lambda" {
  name = "payday_lambda"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_policy" "payday_lambda_policy" {
  name = "payday_lambda_policy"
  description = "policy for lambda execution. provides access to dynamo streams, logs, and S3"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "s3:PutObject",
        "dynamodb:GetRecords",
        "dynamodb:GetShardIterator",
        "dynamodb:DescribeStream",
        "dynamodb:ListStreams",
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents",
        "lambda:InvokeFunction"
      ],
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_policy_attachment" "payday_lambda_policy_attachment" {
  name = "payday_lambda_policy_attachment"
  policy_arn = aws_iam_policy.payday_lambda_policy.arn
  roles = [aws_iam_role.payday_lambda.name]
}

# roles and policies for accessing S3
resource "aws_iam_role_policy_attachment" "payday_s3_prod" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
  role = aws_iam_role.payday_prod.name
}

resource "aws_iam_role_policy_attachment" "payday_s3_staging" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
  role = aws_iam_role.payday_staging.name
}

# roles and policies for accessing lambda
resource "aws_iam_policy_attachment" "payday_lambda" {
  name = "payday_lambda"
  policy_arn = "arn:aws:iam::aws:policy/AWSLambdaFullAccess"
  roles = [aws_iam_role.payday_prod.name, aws_iam_role.payday_staging.name]
  users = ["jenkins"]
}

# roles and policies for accessing IAM roles
resource "aws_iam_policy_attachment" "payday_iam" {
  name = "payday_iam"
  policy_arn = "arn:aws:iam::aws:policy/IAMReadOnlyAccess"
  roles = [aws_iam_role.payday_prod.name, aws_iam_role.payday_staging.name]
}

# roles and policies for accessing dynamo
resource "aws_iam_policy_attachment" "payday_full_dynamo_prod" {
  name = "payday_full_dynamo_prod"
  policy_arn = aws_iam_policy.full_dynamo_prod_payday.arn
  roles = [aws_iam_role.payday_prod.name]
}

resource "aws_iam_policy_attachment" "payday_full_dynamo_staging" {
  name = "payday_full_dynamo_staging"
  policy_arn = aws_iam_policy.full_dynamo_staging_payday.arn
  roles = [aws_iam_role.payday_staging.name]
}


resource "aws_iam_policy" "full_dynamo_prod_payday" {
  name = "full_dynamo_prod_payday"
  description = "full access to dynamo"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "dynamodb:*"
      ],
      "Resource": [
        "arn:aws:dynamodb:us-west-2:021561903526:table/prod_actions",
        "arn:aws:dynamodb:us-west-2:021561903526:table/prod_badge-tiers",
        "arn:aws:dynamodb:us-west-2:021561903526:table/prod_channels",
        "arn:aws:dynamodb:us-west-2:021561903526:table/prod_hashtags",
        "arn:aws:dynamodb:us-west-2:021561903526:table/prod_raincatcher-dashboard",
        "arn:aws:dynamodb:us-west-2:021561903526:table/prod_raincatcher-dashboard-hours",
        "arn:aws:dynamodb:us-west-2:021561903526:table/prod_sponsored_cheermote_campaigns",
        "arn:aws:dynamodb:us-west-2:021561903526:table/prod_sponsored_cheermote_channel_statuses",
        "arn:aws:dynamodb:us-west-2:021561903526:table/prod_users",
        "arn:aws:dynamodb:us-west-2:021561903526:table/prod_transactions",
        "arn:aws:dynamodb:us-west-2:021561903526:table/prod_tax-info-by-payout-entity-id"
      ]
    }
  ]
}
EOF
}

resource "aws_iam_policy" "full_dynamo_staging_payday" {
  name = "full_dynamo_staging_payday"
  description = "full access to dynamo"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "dynamodb:*"
      ],
      "Resource": [
        "arn:aws:dynamodb:us-west-2:021561903526:table/test_actions",
        "arn:aws:dynamodb:us-west-2:021561903526:table/test_badge-tiers",
        "arn:aws:dynamodb:us-west-2:021561903526:table/test_channels",
        "arn:aws:dynamodb:us-west-2:021561903526:table/test_hashtags",
        "arn:aws:dynamodb:us-west-2:021561903526:table/test_raincatcher-dashboard",
        "arn:aws:dynamodb:us-west-2:021561903526:table/test_raincatcher-dashboard-hours",
        "arn:aws:dynamodb:us-west-2:021561903526:table/test_sponsored_cheermote_campaigns",
        "arn:aws:dynamodb:us-west-2:021561903526:table/test_sponsored_cheermote_channel_statuses",
        "arn:aws:dynamodb:us-west-2:021561903526:table/test_users",
        "arn:aws:dynamodb:us-west-2:021561903526:table/test_transactions",
        "arn:aws:dynamodb:us-west-2:021561903526:table/test_tax-info-by-payout-entity-id"
      ]
    }
  ]
}
EOF
}

# roles and policies for cloudwatch
resource "aws_iam_policy_attachment" "payday_cloudwatch" {
  name = "payday_cloudwatch"
  policy_arn = "arn:aws:iam::aws:policy/CloudWatchFullAccess"
  roles = [aws_iam_role.payday_prod.name, aws_iam_role.payday_staging.name]
}

# roles and policies for KMS access
resource "aws_iam_role_policy_attachment" "payday_kms_assume_role" {
  policy_arn = "arn:aws:iam::aws:policy/AWSKeyManagementServicePowerUser"
  role =  aws_iam_role.payday_kms.name
}

resource "aws_iam_role_policy_attachment" "payday_kms_prod" {
  policy_arn = "arn:aws:iam::aws:policy/AWSKeyManagementServicePowerUser"
  role = aws_iam_role.payday_prod.name
}

resource "aws_iam_role_policy_attachment" "payday_kms_staging" {
  policy_arn = "arn:aws:iam::aws:policy/AWSKeyManagementServicePowerUser"
  role = aws_iam_role.payday_staging.name
}

resource "aws_iam_user_policy_attachment" "payday_kms_user" {
  policy_arn = "arn:aws:iam::aws:policy/AWSKeyManagementServicePowerUser"
  user = "kms"
}

resource "aws_iam_role" "payday_kms" {
  name = "payday_kms"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::673385534282:role/beanstalk_worker_profile",
        "Service": "ec2.amazonaws.com"
      }
    }
  ]
}
EOF
}

# roles and policies for Kinesis Firehose access
resource "aws_iam_policy_attachment" "payday_firehose" {
  name = "payday_firehose"
  policy_arn = aws_iam_policy.payday_firehose.arn
  roles = [aws_iam_role.payday_prod.name]
}

resource "aws_iam_policy" "payday_firehose" {
  name = "payday_firehose"
  description = "write access to production kinesis firehose delivery streams"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "firehose:PutRecord",
        "firehose:PutRecordBatch"
      ],
      "Effect": "Allow",
      "Resource": [
        "arn:aws:firehose:us-west-2:021561903526:deliverystream/payday-raincatcher-balanceupdates",
        "arn:aws:firehose:us-west-2:021561903526:deliverystream/payday-raincatcher-transactions",
        "arn:aws:firehose:us-west-2:021561903526:deliverystream/payday-raincatcher-emoteuses",
        "arn:aws:firehose:us-west-2:021561903526:deliverystream/payday-raincatcher-transactions-audit"
      ]
    }
  ]
}
EOF
}

resource "aws_iam_policy_attachment" "payday_firehose_staging" {
  name = "payday_firehose_staging"
  policy_arn = aws_iam_policy.payday_firehose_staging.arn
  roles = [aws_iam_role.payday_staging.name]
}

resource "aws_iam_policy" "payday_firehose_staging" {
  name = "payday_firehose_staging"
  description = "write access to staging kinesis firehose delivery streams"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "firehose:PutRecord",
        "firehose:PutRecordBatch"
      ],
      "Effect": "Allow",
      "Resource": [
        "arn:aws:firehose:us-west-2:021561903526:deliverystream/payday-raincatcher-balanceupdates-staging",
        "arn:aws:firehose:us-west-2:021561903526:deliverystream/payday-raincatcher-transactions-staging",
        "arn:aws:firehose:us-west-2:021561903526:deliverystream/payday-raincatcher-emoteuses-staging",
        "arn:aws:firehose:us-west-2:021561903526:deliverystream/payday-raincatcher-transactions-audit-staging"
      ]
    }
  ]
}
EOF
}


resource "aws_iam_policy_attachment" "payday_auto_refill_table_access_staging" {
  name = "payday_auto_refill_table_access_staging"
  policy_arn = aws_iam_policy.payday_auto_refill_table_access_staging.arn
  roles = [aws_iam_role.payday_staging.name]
}

resource "aws_iam_policy" "payday_auto_refill_table_access_staging" {
  name = "payday_auto_refill_table_access_staging"
  description = "Table access to the staging auto refill aws account"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "sts:AssumeRole",
        "dynamodb:*"
      ],
      "Effect": "Allow",
      "Resource": [
        "arn:aws:iam::482521867325:role/PaydayDynamoAccessRole",
        "arn:aws:dynamodb:us-west-2:482521867325:table/*"
      ]
    }
  ]
}
EOF
}

resource "aws_iam_policy_attachment" "payday_auto_refill_table_access_prod" {
  name = "payday_auto_refill_table_access_prod"
  policy_arn = aws_iam_policy.payday_auto_refill_table_access_prod.arn
  roles = [aws_iam_role.payday_prod.name]
}

resource "aws_iam_policy" "payday_auto_refill_table_access_prod" {
  name = "payday_auto_refill_table_access_prod"
  description = "Table access to the staging auto refill aws account"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "sts:AssumeRole",
        "dynamodb:*"
      ],
      "Effect": "Allow",
      "Resource": [
        "arn:aws:iam::437149481227:role/PaydayDynamoAccessRole",
        "arn:aws:dynamodb:us-west-2:437149481227:table/*"
      ]
    }
  ]
}
EOF
}
