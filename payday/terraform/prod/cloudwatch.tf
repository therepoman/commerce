# Raincatcher Metrics

resource "aws_cloudwatch_metric_alarm" "raincatcher_transactions_delivery" {
  alarm_name = "raincatcher-transactions-ingestion-alarm"
  namespace  = "AWS/Firehose"
  dimensions = {
    DeliveryStreamName = "payday-raincatcher-transactions"
  }
  metric_name         = "DeliveryToRedshift.Success"
  comparison_operator = "LessThanThreshold"
  statistic           = "Average"
  threshold           = "1"
  period              = "300"
  evaluation_periods  = "24"
}

resource "aws_cloudwatch_metric_alarm" "raincatcher_transactions_audit_delivery" {
  alarm_name = "raincatcher-transactions-audit-ingestion-alarm"
  namespace  = "AWS/Firehose"
  dimensions = {
    DeliveryStreamName = "payday-raincatcher-transactions-audit"
  }
  metric_name         = "DeliveryToRedshift.Success"
  comparison_operator = "LessThanThreshold"
  statistic           = "Average"
  threshold           = "1"
  period              = "300"
  evaluation_periods  = "24"
}

resource "aws_cloudwatch_metric_alarm" "raincatcher_balance_updates_delivery" {
  alarm_name = "raincatcher-balance-updates-ingestion-alarm"
  namespace  = "AWS/Firehose"
  dimensions = {
    DeliveryStreamName = "payday-raincatcher-balanceupdates"
  }
  metric_name         = "DeliveryToRedshift.Success"
  comparison_operator = "LessThanThreshold"
  statistic           = "Average"
  threshold           = "1"
  period              = "300"
  evaluation_periods  = "24"
}

resource "aws_cloudwatch_metric_alarm" "raincatcher_emote_uses_delivery" {
  alarm_name = "raincatcher-emote-uses-ingestion-alarm"
  namespace  = "AWS/Firehose"
  dimensions = {
    DeliveryStreamName = "payday-raincatcher-emoteuses"
  }
  metric_name         = "DeliveryToRedshift.Success"
  comparison_operator = "LessThanThreshold"
  statistic           = "Average"
  threshold           = "1"
  period              = "300"
  evaluation_periods  = "24"
}

resource "aws_cloudwatch_metric_alarm" "raincatcher_health" {
  alarm_name = "raincatcher-health-status-alarm"
  namespace  = "AWS/Redshift"
  dimensions = {
    ClusterIdentifier = "raincatcher-production"
  }
  comparison_operator = "LessThanThreshold"
  statistic           = "Average"
  metric_name         = "HealthStatus"
  threshold           = "1"
  period              = "300"
  evaluation_periods  = "24"
}

resource "aws_cloudwatch_metric_alarm" "bits_usage_entitlements_stepfn_executions_failed_low" {
  alarm_name                = "payday-production-bits-usage-entitlements-stepfn-executions-failed-low"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "1"
  datapoints_to_alarm       = "1"
  metric_name               = "ExecutionsFailed"
  namespace                 = "AWS/States"
  period                    = "300"
  statistic                 = "Sum"
  threshold                 = "1"
  unit                      = "Count"
  alarm_description         = "At least one Bits Usage Entitlements step function execution has failed"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    StateMachineArn = module.payday_prod.bits_usage_entitlements_sfn_arn
  }
}

resource "aws_cloudwatch_metric_alarm" "bits_usage_entitlements_stepfn_executions_failed_high" {
  alarm_name                = "payday-production-bits-usage-entitlements-stepfn-executions-failed-high"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "ExecutionsFailed"
  namespace                 = "AWS/States"
  period                    = "300"
  statistic                 = "Sum"
  threshold                 = "10"
  unit                      = "Count"
  alarm_description         = "At least ten Bits Usage Entitlements step function execution have failed for 3 consecutive periods"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    StateMachineArn = module.payday_prod.bits_usage_entitlements_sfn_arn
  }
}

resource "aws_cloudwatch_metric_alarm" "user_destroy_stepfn_executions_failed_low" {
  alarm_name                = "payday-production-user-destroy-stepfn-executions-failed-low"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "1"
  datapoints_to_alarm       = "1"
  metric_name               = "ExecutionsFailed"
  namespace                 = "AWS/States"
  period                    = "300"
  statistic                 = "Sum"
  threshold                 = "1"
  unit                      = "Count"
  alarm_description         = "At least one User Destroy step function execution has failed"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    StateMachineArn = module.payday_prod.user_destroy_sfn_arn
  }
}

resource "aws_cloudwatch_metric_alarm" "user_destroy_dlq_not_empty" {
  alarm_name                = "payday-production-user-destroy-dlq-not-empty"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "1"
  datapoints_to_alarm       = "1"
  metric_name               = "ApproximateNumberOfMessagesVisible"
  namespace                 = "AWS/SQS"
  period                    = "300"
  statistic                 = "Sum"
  threshold                 = "1"
  alarm_description         = "User Destroy SQS DLQ is not empty"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    QueueName = module.payday_prod.user_destroy_dlq_name
  }
}