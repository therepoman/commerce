resource "aws_s3_bucket" "payday-dynamo-audit-records" {
  bucket = "payday-dynamo-audit-records"
  acl = "private"

  lifecycle_rule {
    id      = "ttl"
    enabled = true

    expiration {
        days = 20
    }
    noncurrent_version_expiration {
        days = 20
    }
  }
}