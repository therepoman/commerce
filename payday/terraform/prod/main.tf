module "payday_prod" {
  source              = "../modules/payday"
  account_name        = "twitch-bits-aws"
  account_id          = "021561903526"
  owner               = "twitch-bits-oncall@amazon.com"
  env                 = "payday-prod-beanstalk"
  env_prefix          = "prod"
  vpc_id              = "vpc-4cfeb629"
  private_subnets     = "subnet-31bc5e55,subnet-30393e47,subnet-6eadf037"
  private_cidr_blocks = ["10.193.64.0/20", "10.193.80.0/20", "10.193.96.0/20"]
  iam_role_name       = "payday_prod"
  stage               = "production"
  min_read_capacity   = 50
  min_write_capacity  = 50
  max_read_capacity   = 10000
  max_write_capacity  = 10000

  #SNS/SQS
  alarm_dlq                    = true
  user_moderation_sns_arn      = "arn:aws:sns:us-west-2:603200399373:clue_production_channel_ban_events"
  pagle_completion_sns_arn     = "arn:aws:sns:us-west-2:196902209557:prod-condition-completion"
  pachinko_bits_update_sns_arn = "arn:aws:sns:us-west-2:702056039310:bits-prod-updates"


  elasticache_instance_type           = "cache.r5.2xlarge"
  elasticache_num_node_groups         = "14"
  elasticache_replicas_per_node_group = "2"

  ecs_healthcheck = "/health-check"
  // 100 tasks / 4 tasks per host * 1.2 buffer = 30 hosts
  // TODO: Over provisioning by setting min hosts to 38. EC2 downscaling does not currently gracefully drain
  //       ECS tasks, resulting in gateway timeouts for in-flight requests. Once https://github.com/aws/containers-roadmap/issues/256 is resolved,
  //       we can lower this number.
  ecs_ec2_backed_min_instances = 40
  ecs_min_instances            = 100
  ecs_max_instances            = 200
  ecs_cpu                      = 4096
  ecs_memory                   = 4096
  cpu_scale_up_threshold       = 25
  sandstorm_role               = "payday-prod"
  dns                          = "prod.payday.twitch.a2z.com"
  event_bus_iam_arn            = "arn:aws:iam::021561903526:policy/EventBus/EventBusAccess"
  auto_refill_table_account_id = 437149481227

  // Legal recommends 30 days TTL for sensitive log groups
  audit_logs_retention_in_days    = 30
  s2s_auth_logs_retention_in_days = 30

  ecs_upgrade_sns_topic = "arn:aws:sns:us-west-2:021561903526:on_terminate_lifecycle_hook_sns_topic"
  ecs_env_vars = [
    {
      name  = "payday_APP_ENV"
      value = "production"
    },
    {
      name  = "AWS_STS_REGIONAL_ENDPOINTS",
      value = "regional"
    }
  ]
}

provider "aws" {
  region  = "us-west-2"
  profile = "payday"
}

terraform {
  backend "s3" {
    bucket  = "payday-tfstate"
    key     = "tfstate/commerce/payday/terraform/prod"
    region  = "us-west-2"
    profile = "payday"
  }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 2.44"
    }
  }

  required_version = ">= 0.14"
}
