variable "num_instances_per_az" {}

variable "name" {}
variable "service" {}
variable "owner" {}
variable "environment" {}
variable "instance_profile" {}
variable "cluster" {}
variable "vpc_security_group_id" {}
variable "twitch_environment" {}
variable "elb_security_group_id" {}
variable "pub_subnets_ids" {} # for the NAT gateways
variable "zone_id" {}
variable "nat_gateway_eip_allocation_ids" {}
variable "cidr_blocks" {}
variable "vpc_id" {}
variable "sfo_vgw" {}
variable "vpc_peering_connection_id" {}
variable "admin_elb_sslcert_arn" {}
variable "admin_elb_dns_infix" {}

variable "twitch-aws_cidr" {}
variable "twitch-aws_access_key" {}
variable "twitch-aws_secret_key" {}

provider "aws" {
  alias = "twitch-aws"
  access_key = var.twitch-aws_access_key
  secret_key = var.twitch-aws_secret_key
  region = "us-west-2"
}

# module doesn't support "count" parameter :(
#### AVAILABILITY ZONE 0: "a"
module "availability_zone_a" {
  source = "../single_zone"
  name = var.name
  owner = var.owner
  service = var.service
  environment = var.environment
  instances = var.num_instances_per_az
  instance_profile = var.instance_profile
  vpc_security_group_id = var.vpc_security_group_id
  cluster = var.cluster
  twitch_environment = var.twitch_environment
  pub_subnet_id = element(split(",", var.pub_subnets_ids),0)
  zone_id = var.zone_id
  nat_gateway_eip_allocation_id = element(split(",", var.nat_gateway_eip_allocation_ids),0)
  cidr_block = element(split(",", var.cidr_blocks),0)
  vpc_id = var.vpc_id
  sfo_vgw = var.sfo_vgw
  vpc_peering_connection_id = var.vpc_peering_connection_id
  availability_zone_letter = "a"

  twitch-aws_cidr = var.twitch-aws_cidr
  twitch-aws_access_key = var.twitch-aws_access_key
  twitch-aws_secret_key = var.twitch-aws_secret_key
}
#### AVAILABILITY ZONE 1: "b"
module "availability_zone_b" {
  source = "../single_zone"
  name = var.name
  service = var.service
  owner = var.owner
  environment = var.environment
  instances = var.num_instances_per_az
  instance_profile = var.instance_profile
  vpc_security_group_id = var.vpc_security_group_id
  cluster = var.cluster
  twitch_environment = var.twitch_environment
  pub_subnet_id = element(split(",", var.pub_subnets_ids),1)
  zone_id = var.zone_id
  nat_gateway_eip_allocation_id = element(split(",", var.nat_gateway_eip_allocation_ids),1)
  cidr_block = element(split(",", var.cidr_blocks), 1)
  vpc_id = var.vpc_id
  sfo_vgw = var.sfo_vgw
  vpc_peering_connection_id = var.vpc_peering_connection_id
  availability_zone_letter = "b"

  twitch-aws_cidr = var.twitch-aws_cidr
  twitch-aws_access_key = var.twitch-aws_access_key
  twitch-aws_secret_key = var.twitch-aws_secret_key
}
#### AVAILABILITY ZONE 2: "c"
module "availability_zone_c" {
  source = "../single_zone"
  name = var.name
  service = var.service
  owner = var.owner
  environment = var.environment
  instances = var.num_instances_per_az
  instance_profile = var.instance_profile
  vpc_security_group_id = var.vpc_security_group_id
  cluster = var.cluster
  twitch_environment = var.twitch_environment
  pub_subnet_id = element(split(",", var.pub_subnets_ids),2)
  zone_id = var.zone_id
  nat_gateway_eip_allocation_id = element(split(",", var.nat_gateway_eip_allocation_ids), 2)
  cidr_block = element(split(",", var.cidr_blocks), 2)
  vpc_id = var.vpc_id
  sfo_vgw = var.sfo_vgw
  vpc_peering_connection_id = var.vpc_peering_connection_id
  availability_zone_letter = "c"

  twitch-aws_cidr = var.twitch-aws_cidr
  twitch-aws_access_key = var.twitch-aws_access_key
  twitch-aws_secret_key = var.twitch-aws_secret_key
}

resource "aws_elb" "elb" {
  name = "${var.name}-${var.environment}-elb"
  tags {
    Name = "${var.name}-${var.environment}-elb"
    Owner = var.owner
  }
  internal = true
  subnets = [
    module.availability_zone_a.subnet_id,
    module.availability_zone_b.subnet_id,
    module.availability_zone_c.subnet_id,
  ]
  security_groups = [var.elb_security_group_id]
  instances = [
    split(",", module.availability_zone_a.instances_ids),
    split(",", module.availability_zone_b.instances_ids),
    split(",", module.availability_zone_c.instances_ids),
  ]
  listener {
    instance_port = 8000
    instance_protocol = "tcp"
    lb_port = 443
    lb_protocol = "tcp"
  }
  listener {
    instance_port = 8000
    instance_protocol = "tcp"
    lb_port = 80
    lb_protocol = "tcp"
  }
  health_check {
    healthy_threshold = 2
    unhealthy_threshold = 2
    timeout = 3
    target = "HTTP:8000/health-check"
    interval = 30
  }
}
# TODO: dont hardcode to us-west-2
resource "aws_route53_record" "payday_elb" {
  provider = "aws.twitch-aws"
  zone_id = var.zone_id
  name = "${var.name}.${var.environment}.us-west2.justin.tv"
  type = "CNAME"
  ttl = "300"
  records = [aws_elb.elb.dns_name]
}

# admin ELB
resource "aws_elb" "admin-elb" {
  name = "${var.name}-${var.environment}-admin-elb"
  tags {
    Name = "${var.name}-${var.environment}-admin-elb"
    Owner = var.owner
  }
  internal = true
  subnets = [
    module.availability_zone_a.subnet_id,
    module.availability_zone_b.subnet_id,
    module.availability_zone_c.subnet_id,
  ]
  security_groups = [var.elb_security_group_id]
  instances = [
    split(",", module.availability_zone_a.instances_ids),
    split(",", module.availability_zone_b.instances_ids),
    split(",", module.availability_zone_c.instances_ids),
  ]
  listener {
    instance_port = 8001
    instance_protocol = "http"
    lb_port = 443
    lb_protocol = "https"
    ssl_certificate_id = var.admin_elb_sslcert_arn
  }
  health_check {
    healthy_threshold = 2
    unhealthy_threshold = 2
    timeout = 3
    target = "HTTP:8000/health-check"
    interval = 30
  }
}
# DNS name for admin ELB
resource "aws_route53_record" "payday_admin-elb" {
  provider = "aws.twitch-aws"
  zone_id = var.zone_id
  name = "${var.name}-admin.${var.admin_elb_dns_infix}.us-west2.justin.tv"
  type = "CNAME"
  ttl = "300"
  records = [aws_elb.admin-elb.dns_name]
}
