variable "api_name" {
  description = "name of the API"
}

variable "metric_name" {
  default = ""
  description = "optional metric name override if it's different"
}

variable "include_paging_alarm" {
  default = false
  description = "flag to determine if we should be making a paging alarm for the API"
}

variable "statistic" {
  description = "the stat to alarm from"
}

variable "period" {
  default = "300"
  description = "the period in seconds to evaluate"
}

variable "evaluation_periods" {
  default = "3"
  description = "the amount of periods to evaluate on before alarming"
}

variable "warning_threshold" {
  description = "breaching threshold for filing a sev 3"
}

variable "paging_threshold" {
  default = ""
  description = "breaching threshold for filing a sev 2"
}

variable "missing_data" {
  default = "breaching"
  description = "how to treat missing data in your metric"
}

variable "low_sample_count_evaluate" {
  default = "evaluate"
  description = "what to do when you don't have statistically significant data to evaluate"
}

resource "aws_cloudwatch_metric_alarm" "api_paging_alarm" {
  count                     = var.include_paging_alarm ? 1 : 0
  actions_enabled           = false
  alarm_name                = "Payday${var.api_name}LatencyHighSevAlarm"
  comparison_operator       = "GreaterThanThreshold"
  evaluation_periods        = "3"
  metric_name               = var.metric_name != "" ? var.metric_name : var.api_name
  namespace                 = "payday"
  dimensions                = {
    Stage = "production"
    Region = "us-west-2"
    Service = "payday"
  }
  period                    = "300"
  extended_statistic                 = var.statistic
  threshold                 = var.paging_threshold
  alarm_description         = "High sev latency alarm for the ${var.api_name} API"
  treat_missing_data        = var.missing_data
  evaluate_low_sample_count_percentiles = var.low_sample_count_evaluate
}

resource "aws_cloudwatch_metric_alarm" "api_warning_alarm" {
  actions_enabled           = false
  alarm_name                = "Payday${var.api_name}LatencyAlarm"
  comparison_operator       = "GreaterThanThreshold"
  evaluation_periods        = "3"
  metric_name               = var.metric_name != "" ? var.metric_name : var.api_name
  namespace                 = "payday"
  dimensions                = {
    Stage = "production"
    Region = "us-west-2"
    Service = "payday"
  }
  period                    = "300"
  extended_statistic                 = var.statistic
  threshold                 = var.warning_threshold
  alarm_description         = "Latency alarm for the ${var.api_name} API"
  treat_missing_data        = var.missing_data
  evaluate_low_sample_count_percentiles = var.low_sample_count_evaluate
}
