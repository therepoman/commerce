variable "platform" {
  description = "the purchase platform"
}

variable "evaluation_periods" {
  default = "3"
  description = "the amount of periods to evaluate on before alarming"
}

variable "threshold" {
  description = "breaching threshold for filing a sev 3"
}

resource "aws_cloudwatch_metric_alarm" "purchase_volume_alarm" {
  actions_enabled           = false
  alarm_name                = "${lower(var.platform)}PurchaseVolume"
  comparison_operator       = "LessThanThreshold"
  evaluation_periods        = "3"
  metric_name               = "BitsAcquired.${var.platform}"
  namespace                 = "payday"
  dimensions                = {
    Stage = "production"
    Region = "us-west-2"
    Service = "payday"
  }
  period                    = "300"
  statistic                 = "Sum"
  threshold                 = var.threshold
  alarm_description         = "${var.platform} Bits Purchase Volume"
  treat_missing_data        = "breaching"
}
