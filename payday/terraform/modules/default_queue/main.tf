variable "queue_name" {
  type = string
}

variable "receive_wait_time" {
  type = string
  default = 20
}

variable "message_retention_seconds" {
  type = string
  default = 1209600
}

variable "max_receive_count" {
  type = string
  default = 7
}

variable "delivery_delay" {
  type = string
  default = 0
}

variable "subscribe_to_topics_prod" {
  type = list(string)
  default = ["arn:aws:sns:us-east-1:277306168768:BitsUpdate"]
}

variable "subscribe_to_topics_staging" {
  type = list(string)
  default = ["arn:aws:sns:us-east-1:256445009424:BitsUpdate"]
}

variable "alarm_dlq" {
  default = false
  description = "flag to determine if we should be making a paging alarm for the DLQ."
}

variable "alarm_dlq_count" {
  default = 1
  description = "the amount of items in the dlq that should trigger the alarm"
}

variable "low_sev_alarm_dlq" {
  default = false
  description = "flag to determine if we should be making a non-paging (Sev 3) alarm for the DLQ"
}

variable "low_sev_alarm_dlq_count" {
  default = 1
  description = "the amount of items in the dlq that should trigger the non-paging (Sev 3) alarm"
}

resource "aws_sqs_queue_policy" "prod_queue_policy" {
  queue_url = aws_sqs_queue.prod_sqs_queue.id
  policy = data.aws_iam_policy_document.default_prod_queue_policy_doc.json
}

data "aws_iam_policy_document" "default_prod_queue_policy_doc" {
  statement {
    sid = "PaydayQueueSid"

    actions = [
      "SQS:SendMessage",
    ]

    principals {
      identifiers = [
        "*",
      ]
      type = "AWS"
    }

    resources = [
      aws_sqs_queue.prod_sqs_queue.arn,
    ]

    condition {
      test = "ArnEquals"
      variable = "aws:SourceArn"

      values = var.subscribe_to_topics_prod
    }
  }
}

resource "aws_sqs_queue_policy" "staging_queue_policy" {
  queue_url = aws_sqs_queue.staging_sqs_queue.id
  policy = data.aws_iam_policy_document.default_staging_queue_policy_doc.json
}

data "aws_iam_policy_document" "default_staging_queue_policy_doc" {
  statement {
    sid = "PaydayQueueSid"

    actions = [
      "SQS:SendMessage",
    ]

    principals {
      identifiers = [
        "*",
      ]
      type = "AWS"
    }

    resources = [
      aws_sqs_queue.staging_sqs_queue.arn,
    ]

    condition {
      test = "ArnEquals"
      variable = "aws:SourceArn"

      values = var.subscribe_to_topics_staging
    }
  }
}

# Prod Queue
resource "aws_sqs_queue" "prod_sqs_queue_deadletter" {
  message_retention_seconds = var.message_retention_seconds
  name = "${var.queue_name}_deadletter"
}

resource "aws_sqs_queue" "prod_sqs_queue" {
  message_retention_seconds = var.message_retention_seconds
  name = var.queue_name
  receive_wait_time_seconds = var.receive_wait_time
  delay_seconds = var.delivery_delay
  redrive_policy = <<EOF
{
  "deadLetterTargetArn": "${aws_sqs_queue.prod_sqs_queue_deadletter.arn}",
  "maxReceiveCount": ${var.max_receive_count}
}
EOF
}

resource "aws_cloudwatch_metric_alarm" "dlq_not_empty" {
  count = var.alarm_dlq ? 1 : 0
  alarm_name = "${var.queue_name}_deadletter_not_empty"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = "5"
  metric_name = "ApproximateNumberOfMessagesVisible"
  namespace = "AWS/SQS"
  dimensions = {
    QueueName = "${var.queue_name}_deadletter"
  }
  period = "60"
  statistic = "Sum"
  threshold = var.alarm_dlq_count
  alarm_description = "This metric monitors one of payday's dead letter queues. It will alarm if there are too many messages in that queue."
  treat_missing_data = "notBreaching"
}

resource "aws_cloudwatch_metric_alarm" "dlq_not_empty_low_sev" {
  count = var.low_sev_alarm_dlq ? 1 : 0
  alarm_name = "${var.queue_name}_deadletter_not_empty_low_sev"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = "5"
  metric_name = "ApproximateNumberOfMessagesVisible"
  namespace = "AWS/SQS"
  dimensions = {
    QueueName = "${var.queue_name}_deadletter"
  }
  period = "60"
  statistic = "Sum"
  threshold = var.low_sev_alarm_dlq_count
  alarm_description = "This metric monitors one of payday's dead letter queues. It will alarm (low severity) if there are too many messages in that queue."
  treat_missing_data = "notBreaching"
}

#Staging Queue
resource "aws_sqs_queue" "staging_sqs_queue_deadletter" {
  message_retention_seconds = var.message_retention_seconds
  name = "${var.queue_name}_deadletter_staging"
}

resource "aws_sqs_queue" "staging_sqs_queue" {
  message_retention_seconds = var.message_retention_seconds
  name = "${var.queue_name}_staging"
  receive_wait_time_seconds = var.receive_wait_time
  delay_seconds = var.delivery_delay
  redrive_policy = <<EOF
{
  "deadLetterTargetArn": "${aws_sqs_queue.staging_sqs_queue_deadletter.arn}",
  "maxReceiveCount": ${var.max_receive_count}
}
EOF
}

output "prod_queue_arn" {
  value = aws_sqs_queue.prod_sqs_queue.arn
}

output "staging_queue_arn" {
  value = aws_sqs_queue.staging_sqs_queue.arn
}

