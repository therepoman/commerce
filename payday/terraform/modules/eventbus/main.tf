data "aws_cloudformation_export" "eventbus_sqs_policy_document" {
  name = "eventbus-sqs-policy-document"
}

data "aws_cloudformation_export" "eventbus_kms_master_key_arn" {
  name = "eventbus-kms-master-key-arn"
}

resource "aws_sqs_queue" "eventbus_sqs" {
  name              = "payday_eventbus_${var.eventbus_event}_${var.env}"
  policy            = data.aws_cloudformation_export.eventbus_sqs_policy_document.value
  kms_master_key_id = data.aws_cloudformation_export.eventbus_kms_master_key_arn.value
  redrive_policy    = "{\"deadLetterTargetArn\":\"${aws_sqs_queue.eventbus_dlq.arn}\",\"maxReceiveCount\":7}"
}

resource "aws_sqs_queue" "eventbus_dlq" {
  name              = "payday_eventbus_${var.eventbus_event}_${var.env}_deadletter"
  kms_master_key_id = data.aws_cloudformation_export.eventbus_kms_master_key_arn.value
}

resource "aws_cloudwatch_metric_alarm" "eventbus_queue_deadletter_not_empty" {
  alarm_name          = "payday_eventbus_${var.eventbus_event}_${var.env}_queue_deadletter_not_empty"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "5"
  metric_name         = "ApproximateNumberOfMessagesVisible"
  namespace           = "AWS/SQS"

  dimensions = {
    QueueName = aws_sqs_queue.eventbus_dlq.name
  }

  period             = "300"
  statistic          = "Sum"
  threshold          = "1"
  alarm_description  = "This metric monitors one of payday's dead letter queues. It will alarm if there are too many messages in that queue."
  treat_missing_data = "notBreaching"
}
