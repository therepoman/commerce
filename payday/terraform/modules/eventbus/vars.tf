variable "env" {
  description = "Whether it's production, dev, etc"
}

variable "eventbus_event" {
  description = "Describes what eventbus event the sqs is subscribing to"
}
