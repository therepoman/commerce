output "queue_name" {
  value = aws_sqs_queue.eventbus_sqs.name
}

output "dlq_queue_name" {
  value = aws_sqs_queue.eventbus_dlq.name
}