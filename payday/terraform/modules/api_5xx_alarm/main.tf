variable "api_name" {
  description = "name of the API"
}

variable "period" {
  default     = "300"
  description = "the period in seconds to evaluate"
}

variable "evaluation_periods" {
  default     = "3"
  description = "the amount of periods to evaluate on before alarming"
}

variable "threshold" {
  description = "breaching threshold for going into an alarm state"
}

variable "missing_data" {
  default     = "notBreaching"
  description = "how to treat missing data in your metric"
}

resource "aws_cloudwatch_metric_alarm" "api_paging_alarm" {
  actions_enabled     = false
  alarm_name          = "Payday${var.api_name}500Errors"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = var.evaluation_periods
  metric_name         = "${var.api_name}.5xx"
  namespace           = "payday"

  dimensions = {
    Stage   = "production"
    Region  = "us-west-2"
    Service = "payday"
  }

  period                    = var.period
  statistic                 = "Sum"
  threshold                 = var.threshold
  alarm_description         = "Alarms when ${var.api_name} API 500 status code error count breaches threshold"
  treat_missing_data        = "notBreaching"
}
