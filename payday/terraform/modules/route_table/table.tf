variable "name" {
  default = "payday"
}

variable "owner" {
  default = "bits-commerce@justin.tv"
}

variable "twitch_aws_cidr" {
  default = "10.192.64.0/18"
}

variable "virtual_private_gateway_id" {
  default = "vgw-7c16c962"
}

variable "pantheon_cidr_block" {
  default = "10.201.184.0/22"
}

variable "pantheon_peering_connection_id" {
  default = "pcx-0d69a6ed881ad05b8"
}

variable "prism_cidr_block" {
  default = "10.204.252.0/22"
}

variable "prism_peering_connection_id" {
  default = "pcx-0ddf8cfb7b4314045"
}

variable "petozi_cidr_block" {
  default = "10.204.252.0/22"
}

variable "petozi_peering_connection_id" {
  default = "pcx-0ddf8cfb7b4314045"
}

variable "upload_service_peering_connection_id" {}

variable "upload_service_cidr_block" {}

variable "chat_cidr_block" {}

variable "chat_peering_connection_id" {}

variable "pachter_peering_connection_id" {}

variable "pachter_cidr_block" {}

variable "visage_peering_connection_id" {}

variable "visage_cidr_block" {}

variable "pagle_cidr_block" {
  default = "10.205.68.0/22"
}

variable "pagle_peering_connection_id" {
  default = "pcx-044c5ed82d2f197f1"
}

variable "pachinko_peering_connection_id" {}

variable "pachinko_cidr_block" {}

variable "poliwag_cidr_block" {}

variable "poliwag_peering_connection_id" {}

variable "mako_cidr_block" {}

variable "mako_peering_connection_id" {}

variable "revenue_reporting_worker_cidr_block" {}

variable "revenue_reporting_worker_peering_connection_id" {}

variable "amp_cidr_block" {}

variable "amp_connection_id" {}

variable helix_cidr_block {}
variable helix_connection_id {}

variable "vpc_id" {}
variable "twitch_aws_peering_connection_id" {}
variable "nat_gateway_id" {}
variable "subnet_id" {}
variable "az" {}
variable "stage" {}

resource "aws_route_table" "route_table" {
  vpc_id = var.vpc_id

  tags = {
    Name  = "${var.name}-${var.stage}-route-2${var.az}"
    Owner = var.owner
  }

  propagating_vgws = [var.virtual_private_gateway_id]

  # route twitch aws prod out to peer
  route {
    cidr_block                = var.twitch_aws_cidr
    vpc_peering_connection_id = var.twitch_aws_peering_connection_id
  }

  # route internet to our NAT
  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = var.nat_gateway_id
  }

  # TODO: jira.twitch.com/SCROOGE-2315
  route {
    cidr_block                = "10.194.192.0/18"
    vpc_peering_connection_id = "pcx-46b8ce2f"
  }

  # users service peering connection
  route {
    cidr_block                = "10.202.28.0/22"
    vpc_peering_connection_id = "pcx-fedf9097"
  }

  # GraphQL Prod peering connection
  route {
    cidr_block                = "10.203.244.0/22"
    vpc_peering_connection_id = "pcx-0ac4a7131579a8177"
  }

  # GraphQL Staging peering connection
  route {
    cidr_block                = "10.203.240.0/22"
    vpc_peering_connection_id = "pcx-05f2c6111573aa9d8"
  }

  # Pantheon peering connection
  route {
    cidr_block                = var.pantheon_cidr_block
    vpc_peering_connection_id = var.pantheon_peering_connection_id
  }

  # Fortuna peering connection - only one account apparently
  route {
    cidr_block                = "10.202.60.0/22"
    vpc_peering_connection_id = "pcx-00cb80ce9855dd1a6"
  }

  # Prism peering connection
  route {
    cidr_block                = var.prism_cidr_block
    vpc_peering_connection_id = var.prism_peering_connection_id
  }

  # Petozi peering connection
  route {
    cidr_block                = var.petozi_cidr_block
    vpc_peering_connection_id = var.petozi_peering_connection_id
  }

  # Upload service peering connection
  route {
    cidr_block                = var.upload_service_cidr_block
    vpc_peering_connection_id = var.upload_service_peering_connection_id
  }

  # Chat peering connection
  route {
    cidr_block                = var.chat_cidr_block
    vpc_peering_connection_id = var.chat_peering_connection_id
  }

  # Pachter peering connection
  route {
    cidr_block                = var.pachter_cidr_block
    vpc_peering_connection_id = var.pachter_peering_connection_id
  }

  # Pagle peering connection
  route {
    cidr_block                = var.pagle_cidr_block
    vpc_peering_connection_id = var.pagle_peering_connection_id
  }

  # Pachinko peering connection
  route {
    cidr_block                = var.pachinko_cidr_block
    vpc_peering_connection_id = var.pachinko_peering_connection_id
  }

  # Visage peering connection
  route {
    cidr_block                = var.visage_cidr_block
    vpc_peering_connection_id = var.visage_peering_connection_id
  }

  # Poliwag peering connection
  route {
    cidr_block                = var.poliwag_cidr_block
    vpc_peering_connection_id = var.poliwag_peering_connection_id
  }

  # Mako peering connection
  route {
    cidr_block                = var.mako_cidr_block
    vpc_peering_connection_id = var.mako_peering_connection_id
  }

  # Revenue reporting worker peering connection
  route {
    cidr_block                = var.revenue_reporting_worker_cidr_block
    vpc_peering_connection_id = var.revenue_reporting_worker_peering_connection_id
  }

  # AMP peering connection
  route {
    cidr_block                = var.amp_cidr_block
    vpc_peering_connection_id = var.amp_connection_id
  }

  # Helix peering connection
  route {
    cidr_block                = var.helix_cidr_block
    vpc_peering_connection_id = var.helix_connection_id
  }
}

resource "aws_route_table_association" "route_table_association" {
  subnet_id      = var.subnet_id
  route_table_id = aws_route_table.route_table.id
}
