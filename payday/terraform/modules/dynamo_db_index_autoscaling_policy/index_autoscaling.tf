variable "autoscaling_role" {
  type = string
  default = "arn:aws:iam::021561903526:role/aws-service-role/dynamodb.application-autoscaling.amazonaws.com/AWSServiceRoleForApplicationAutoScaling_DynamoDBTable"
}

variable "index_name" {
  type = string
  default = ""
}

variable "min_index_read_capacity" {
  type = string
  default = "5"
}

variable "max_index_read_capacity" {
  type = string
  default = "10000"
}

variable "min_index_write_capacity" {
  type = string
  default = "5"
}

variable "max_index_write_capacity" {
  type = string
  default = "10000"
}

variable "table_name" {
  type = string
}

variable "read_utilization" {
  type = string
  default = "50"
}

variable "write_utilization" {
  type = string
  default = "50"
}


resource "aws_appautoscaling_target" "dynamodb_index_read_target" {
  max_capacity       = var.max_index_read_capacity
  min_capacity       = var.min_index_read_capacity
  resource_id        = "table/${var.table_name}/index/${var.index_name}"
  role_arn           = var.autoscaling_role
  scalable_dimension = "dynamodb:index:ReadCapacityUnits"
  service_namespace  = "dynamodb"
}

resource "aws_appautoscaling_policy" "dynamodb_index_read_capacity_autoscaling_policy" {
  name                    = "${var.table_name}_${var.index_name}_read_scaling"
  resource_id             = "table/${var.table_name}/index/${var.index_name}"
  scalable_dimension      = "dynamodb:index:ReadCapacityUnits"
  service_namespace       = "dynamodb"
  policy_type             = "TargetTrackingScaling"

  target_tracking_scaling_policy_configuration {
    target_value = var.read_utilization
    scale_in_cooldown = 0
    scale_out_cooldown = 0

    predefined_metric_specification {
      predefined_metric_type = "DynamoDBReadCapacityUtilization"
    }
  }

  depends_on = [aws_appautoscaling_target.dynamodb_index_read_target]
}

resource "aws_appautoscaling_target" "dynamodb_index_write_target" {
  max_capacity       = var.max_index_write_capacity
  min_capacity       = var.min_index_write_capacity
  resource_id        = "table/${var.table_name}/index/${var.index_name}"
  role_arn           = var.autoscaling_role
  scalable_dimension = "dynamodb:index:WriteCapacityUnits"
  service_namespace  = "dynamodb"
}

resource "aws_appautoscaling_policy" "dynamodb_index_write_capacity_autoscaling_policy" {
  name                    = "${var.table_name}_${var.index_name}_write_scaling"
  resource_id             = "table/${var.table_name}/index/${var.index_name}"
  scalable_dimension      = "dynamodb:index:WriteCapacityUnits"
  service_namespace       = "dynamodb"
  policy_type             = "TargetTrackingScaling"

  target_tracking_scaling_policy_configuration {
    target_value = var.write_utilization
    scale_in_cooldown = 0
    scale_out_cooldown = 0

    predefined_metric_specification {
      predefined_metric_type = "DynamoDBWriteCapacityUtilization"
    }
  }

  depends_on = [aws_appautoscaling_target.dynamodb_index_write_target]
}