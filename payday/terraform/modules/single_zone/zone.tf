/* describes instances and nat gateway in a single availability zone */

variable "ami" {
  default = "ami-5e26c36d"
}
variable "type" {
  default = "c3.xlarge"
}
variable "name" {}
variable "service" {}
variable "owner" {}
variable "environment" {}
variable "instances" {}
variable "instance_profile" {}
variable "vpc_security_group_id" {}
variable "cluster" {}
variable "twitch_environment" {}
variable "zone_id" {}
variable "nat_gateway_eip_allocation_id" {} /* this needs to correspond to subnet_ids */
variable "availability_zone_letter" {}
variable "pub_subnet_id" {}
variable "cidr_block" {}
variable "vpc_id" {}
variable "sfo_vgw" {} /* propagating gateway for (SFO access?) */
variable "vpc_peering_connection_id" {}

variable "twitch-aws_cidr" {}
variable "twitch-aws_access_key" {}
variable "twitch-aws_secret_key" {}

provider "aws" {
  alias = "twitch-aws"
  access_key = var.twitch-aws_access_key
  secret_key = var.twitch-aws_secret_key
  region = "us-west-2" # TODO: don't hardcode to this region
}

resource "aws_route53_record" "payday" {
  count = var.instances
  provider = "aws.twitch-aws"
  zone_id = var.zone_id
  name = "${var.name}-${var.environment}-2${var.availability_zone_letter}-${replace(element(aws_instance.payday.*.id, count.index), "i-", "")}.${var.environment}.us-west2.justin.tv" /* CHECK THIS */
  type = "A"
  ttl = "300"
  records = [element(aws_instance.payday.*.private_ip, count.index)]
}

resource "aws_nat_gateway" "payday" {
  allocation_id = var.nat_gateway_eip_allocation_id
  subnet_id = var.pub_subnet_id
}

resource "aws_instance" "payday" {
  count = var.instances
  ami = var.ami
  instance_type = var.type
  iam_instance_profile = var.instance_profile
  subnet_id = aws_subnet.payday.id
  vpc_security_group_ids = [var.vpc_security_group_id]
  root_block_device { volume_size = 64 }
  tags {
    Name = "${var.name}-${var.environment}-2${var.availability_zone_letter}-${count.index}"
    Owner = var.owner
    Environment = var.environment
    Service = var.service
  }
  user_data = <<END_OF_STRING
#cloud-config
preserve_hostname: true
manage_etc_hosts: false
bootcmd:
 - cloud-init-per instance my_set_hostname sh -xc "echo '${var.name}-${var.environment}-2${var.availability_zone_letter}-$INSTANCE_ID' | sed -e 's/-i-/-/g' > /etc/hostname; hostname -F /etc/hostname"
 - cloud-init-per instance my_etc_hosts sh -xc "sed -i -e '/^127.0.1.1/d' /etc/hosts; echo 127.0.1.1 $(cat /etc/hostname).${var.environment}.us-west2.justin.tv $(cat /etc/hostname) >> /etc/hosts"

runcmd:
 - sleep 1
 - echo "cluster=${var.cluster}-${var.environment}" > /etc/facter/facts.d/cluster.txt
 - echo "twitch_environment=${var.twitch_environment}" > /etc/facter/facts.d/twitch_environment.txt
 - echo "app_env=${var.twitch_environment}" > /etc/facter/facts.d/app_env.txt
 - echo "clean=true" > /etc/facter/facts.d/clean.txt
 - /usr/bin/apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys E1DD270288B4E6030699E45FA1715D88E1DF1F24
 - puppet agent --test
END_OF_STRING
}

resource "aws_subnet" "payday" {
  vpc_id = var.vpc_id
  availability_zone = "us-west-2${var.availability_zone_letter}" #TODO: don't hardcode to us-west-2 region
  cidr_block = var.cidr_block
  map_public_ip_on_launch = false
  tags {
    Name = "${var.name}-${var.environment}-private-2${var.availability_zone_letter}"
    Owner = var.owner
  }
}
resource "aws_route_table" "payday" {
  vpc_id = var.vpc_id
  tags {
    Name = "${var.name}-${var.environment}-route-2${var.availability_zone_letter}"
    Owner = var.owner
  }
  propagating_vgws = [var.sfo_vgw]

  # route twitch aws prod out to peer
  route {
    cidr_block = var.twitch-aws_cidr
    vpc_peering_connection_id = var.vpc_peering_connection_id
  }
  # route internet to our NAT
  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.payday.id
  }
  # TODO: jira.twitch.com/SCROOGE-2315
  route {
    cidr_block = "10.194.192.0/18"
    vpc_peering_connection_id = "pcx-46b8ce2f"
  }

  # users service peering connection
  route {
    cidr_block = "10.202.28.0/22"
    vpc_peering_connection_id = "pcx-fedf9097"
  }
}
resource "aws_route_table_association" "payday" {
  subnet_id = aws_subnet.payday.id
  route_table_id = aws_route_table.payday.id
}

output "instances_ids" { /* returns a comma-delimited list of instance IDs */
  value = join(",", aws_instance.payday.*.id)
}
output "nat_gateway_id" {
  value = aws_nat_gateway.payday.id
}
output "subnet_id" {
  value = aws_subnet.payday.id
}
