variable "api_name" {
  description = "name of the API"
}

variable "metric_name" {
  default = ""
  description = "optional metric name override if it's different"
}

variable "period" {
  default = "300"
  description = "the period in seconds to evaluate"
}

variable "evaluation_periods" {
  default = "3"
  description = "the amount of periods to evaluate on before alarming"
}

variable "threshold" {
  description = "breaching threshold for filing a sev 2"
}

variable "missing_data" {
  default = "breaching"
  description = "how to treat missing data in your metric"
}

resource "aws_cloudwatch_metric_alarm" "api_paging_alarm" {
  actions_enabled           = false
  alarm_name                = "Payday${var.api_name}VolumeHighSevAlarm"
  comparison_operator       = "LessThanThreshold"
  evaluation_periods        = var.evaluation_periods
  metric_name               = "${var.metric_name != "" ? var.metric_name : var.api_name}.2xx"
  namespace                 = "payday"
  dimensions                = {
    Stage = "production"
    Region = "us-west-2"
    Service = "payday"
  }
  period                    = var.period
  statistic                 = "Sum"
  threshold                 = var.threshold
  alarm_description         = "High severity volume drop for ${var.api_name} API"
  treat_missing_data        = var.missing_data
}
