variable "status_code_range" {
  description = "status code range to monitor (2xx, 5xx, etc)"
}

variable "period" {
  default = "300"
  description = "the period in seconds to evaluate"
}

variable "evaluation_periods" {
  default = "3"
  description = "the amount of periods to evaluate on before alarming"
}

variable "threshold" {
  description = "breaching threshold for filing a sev 2"
}

variable "missing_data" {
  default = "ignore"
  description = "how to treat missing data in your metric"
}

variable "environment_name" {
  description = "ElasticBeanstalk environment name"
}

resource "aws_cloudwatch_metric_alarm" "application_status_code_alarm" {
  actions_enabled           = false
  alarm_name                = "PaydayApplicationRequests${var.status_code_range}VolumeHighSevAlarm"
  comparison_operator       = "GreaterThanThreshold"
  evaluation_periods        = var.evaluation_periods
  metric_name               = "ApplicationRequests${var.status_code_range}"
  namespace                 = "AWS/ElasticBeanstalk"
  dimensions                = {
    EnvironmentName = var.environment_name
  }
  period                    = var.period
  statistic                 = "Sum"
  threshold                 = var.threshold
  alarm_description         = "High severity volume increase for application ${var.status_code_range}"
  treat_missing_data        = var.missing_data
}
