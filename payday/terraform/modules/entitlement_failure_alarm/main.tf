variable "alarm_name" {
  description = "name to associate with the alarm"
}

variable "metric_name" {
  description = "cloudwatch metric name to base the alarm from"
}

variable "alarm_description" {
  description = "a human-readable description of what this alarm is related to"
}

variable "treat_missing_data_as" {
  description = "how the alarm should treat missing data should be treated: breaching|notBreaching|ignore|missing"
}

resource "aws_cloudwatch_metric_alarm" "entitlement_failure_alarm" {
  actions_enabled = false
  alarm_name                = "prod_${var.alarm_name}"
  comparison_operator       = "GreaterThanThreshold"
  evaluation_periods        = "1"
  metric_name               = var.metric_name
  namespace                 = "payday"
  dimensions                = {
    Stage = "production"
    Region = "us-west-2"
    Service = "payday"
  }
  period                    = "300"
  statistic                 = "Maximum"
  threshold                 = "0"
  alarm_description         = var.alarm_description
  treat_missing_data        = var.treat_missing_data_as
}