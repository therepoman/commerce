module "cloudwatch" {
  source = "./cloudwatch"
  stage  = var.stage
}

locals{
  audit_logs_name = "payday-${var.stage}-audit-logs"
  s2s_auth_logs_name = "payday-${var.stage}-s2s-auth-logs"
}

resource "aws_cloudwatch_log_group" "audit_logs" {
  name              = local.audit_logs_name
  retention_in_days = var.audit_logs_retention_in_days
  kms_key_id        = aws_kms_key.audit_logs.arn
}

resource "aws_cloudwatch_log_group" "s2s_auth_logs" {
  name              = local.s2s_auth_logs_name
  retention_in_days = var.s2s_auth_logs_retention_in_days
  kms_key_id        = aws_kms_key.s2s_auth_logs_kms_key.arn
}

// Glue job failure alarm
resource "aws_cloudwatch_metric_alarm" "glue_job_dbexport_error" {
  count                     = 1
  alarm_name                = "${var.env_prefix}-channels-glue-job-dbexport-error"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "1"
  datapoints_to_alarm       = "1"
  metric_name               = "ApproximateNumberOfMessagesVisible"
  namespace                 = "AWS/SQS"
  period                    = "300"
  statistic                 = "Sum"
  threshold                 = "1"
  unit                      = "Count"
  alarm_description         = "DB export Glue job failure"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    QueueName = aws_sqs_queue.channels_glue_job_dbexport_error.name
  }
}
