resource "aws_kms_key" "tahoe_api" {
  description = "${var.env_prefix} dynamodb export job tahoe key"
}

resource "aws_kms_alias" "tahoe_api" {
  name          = "alias/${var.env_prefix}/tahoe"
  target_key_id = aws_kms_key.tahoe_api.key_id
}

resource "aws_ssm_parameter" "tahoe_api_password" {
  name        = "/tahoe/payday${var.env_prefix}/password"
  description = "${var.env_prefix}'s password for the tahoe api"
  type        = "SecureString"
  value       = "CHANGE_ME"
  key_id      = aws_kms_key.tahoe_api.key_id

  lifecycle {
    ignore_changes = [value]
  }

  tags = {
    environment = var.env_prefix
  }
}
