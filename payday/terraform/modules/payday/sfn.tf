resource "aws_iam_role" "step_functions_role" {
  name = "payday_${var.stage}_step_functions_role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "states.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_policy" "step_functions_policy" {
  name        = "payday-${var.stage}-StepFunctionsPolicy"
  description = "Step function access to invoke Payday Lambda functions"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "lambda:InvokeFunction",
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "step-functions-role-policy-attach" {
  role       = aws_iam_role.step_functions_role.name
  policy_arn = aws_iam_policy.step_functions_policy.arn
}

resource "aws_sfn_state_machine" "bits_usage_entitlements" {
  name     = "payday-${var.stage}-BitsUsageEntitlements"
  role_arn = aws_iam_role.step_functions_role.arn

  definition = <<EOF
  {
    "Comment": "Handles entitlements when Bits are used",
    "StartAt": "ParallelEntitlements",
    "States": {
      "FailState": {
        "Comment": "Failed execution",
        "Type": "Fail"
      },
      "SuccessState": {
        "Type": "Succeed"
      },
      "ParallelEntitlements": {
        "Type": "Parallel",
        "Next": "ChoiceStateShouldPubsub",
        "ResultPath": "$.parallelOutput",
        "Branches": [
          ${local.bter_entitlement_step},
          ${local.badge_entitlement_step}
        ]
      },
      "ChoiceStateShouldPubsub": {
        "Type": "Choice",
        "Default": "FailState",
        "Choices": [
          {
            "Variable": "$.shouldPubsub",
            "BooleanEquals": false,
            "Next": "SuccessState"
          },
          {
            "Variable": "$.shouldPubsub",
            "BooleanEquals": true,
            "Next": "FlattenParallelOutput"
          }
        ]
      },
      "FlattenParallelOutput": {
        "Type": "Pass",
        "Next": "EntitlementPubsub",
        "Parameters": {
          "originalInput.$": "$",
          "emoteEntitlements.$": "$.parallelOutput[0].emoteEntitlements",
          "badgeEntitlements.$": "$.parallelOutput[1].badgeEntitlements"
        }
      },
      "EntitlementPubsub": {
        "Type": "Task",
        "Next": "SuccessState",
        "Resource": "${module.balance_change_processing_entitlement_pubsub.lambda_arn}",
        "Catch" : [{
          "ErrorEquals": ["States.ALL"],
          "Next": "EntitlementPubsubFailState",
          "ResultPath": "$.error"
        }],
        "Retry": ${local.default_retry}
      },
      "EntitlementPubsubFailState": {
        "Comment": "Failed to entitle emotes",
        "Type": "Fail"
      }
    }
  }
EOF
}

locals {
  default_retry = <<EOF
    [
      {
        "ErrorEquals": [
          "States.ALL"
        ],
        "IntervalSeconds": 5,
        "MaxAttempts": 3,
        "BackoffRate": 3.0
      }
    ]
EOF

  bter_entitlement_step = <<EOF
  {
    "StartAt": "EntitleBadgeTierEmoteRewards",
    "States": {
      "EntitleBadgeTierEmoteRewards": {
        "Comment": "Entitles badge tier emote rewards",
        "Type": "Task",
        "Resource": "${module.balance_change_processing_emote_entitlements.lambda_arn}",
        "ResultPath": "$.emoteEntitlements",
        "Catch" : [{
          "ErrorEquals": ["States.ALL"],
          "Next": "EntitleBadgeTierEmoteRewardsFailState",
          "ResultPath": "$.error"
        }],
        "End": true,
        "Retry": ${local.default_retry}
      },
      "EntitleBadgeTierEmoteRewardsFailState": {
        "Comment": "Failed to entitle emotes",
        "Type": "Fail"
      }
    }
  }
EOF

  badge_entitlement_step = <<EOF
  {
    "StartAt": "EntitleBitsBadge",
    "States": {
      "EntitleBitsBadge": {
        "Comment": "Entitles Bits badge",
        "Type": "Task",
        "Resource": "${module.balance_change_processing_badge_entitlements.lambda_arn}",
        "ResultPath": "$.badgeEntitlements",
        "Catch" : [{
          "ErrorEquals": ["States.ALL"],
          "Next": "EntitleBitsBadgeFailState",
          "ResultPath": "$.error"
        }],
        "End": true,
        "Retry": ${local.default_retry}
      },
      "EntitleBitsBadgeFailState": {
        "Comment": "Failed to entitle badge(s)",
        "Type": "Fail"
      }
    }
  }
EOF
}

// PDMS user destroy handling step function
resource "aws_sfn_state_machine" "handle_eventbus_user_destroy" {
  name     = "payday-${var.stage}-HandleEventbusUserDestroy"
  role_arn = aws_iam_role.step_functions_role.arn

  definition = <<EOF
  {
    "Comment": "Handles Eventbus UserDestroy event",
    "StartAt": "ParallelDeletes",
    "States": {
      "ParallelDeletes": {
        "Type": "Parallel",
        "Next": "ReportDeletion",
        "ResultPath": "$.parallelOutput",
        "Branches": [
          ${local.delete_user_bits_onboard_events_step},
          ${local.delete_channel_cheermotes_step},
          ${local.delete_user_purchase_by_platform_step},
          ${local.delete_user_single_purchase_consumption_step},
          ${local.delete_channel_badge_tiers_and_emote_groupids_step},
          ${local.delete_user_step},
          ${local.delete_user_leaderboard_step},
          ${local.delete_user_leaderboard_badge_holders_step},
          ${local.delete_user_channels_and_images_step},
          ${local.delete_user_spomote_channel_statuses_step},
          ${local.delete_user_spomote_eligible_channels_step},
          ${local.delete_user_spomote_user_statuses_step},
          ${local.delete_user_balance_step}
        ],
        "Catch" : [{
          "ErrorEquals": ["States.ALL"],
          "Next": "FailState"
        }]
      },
      "ReportDeletion": {
        "Type": "Task",
        "End": true,
        "Resource": "${module.report_deletion.lambda_arn}",
        "Catch" : [{
          "ErrorEquals": ["States.ALL"],
          "Next": "FailState"
        }],
        "Retry": ${local.default_retry}
      },
      "FailState": {
        "Comment": "Failed execution",
        "Type": "Fail"
      }
    }
  }
EOF
}

locals {
  delete_user_bits_onboard_events_step = <<EOF
  {
    "StartAt": "DeleteUserBitsOnboardEvents",
    "States": {
      "DeleteUserBitsOnboardEvents": {
        "Comment": "Deleting user from bits-onboard-events S3 bucket",
        "Type": "Task",
        "Resource": "${module.delete_user_bits_onboard_events.lambda_arn}",
        "ResultPath": "$.deletedUser",
        "Catch" : [{
          "ErrorEquals": ["States.ALL"],
          "Next": "DeleteUserBitsOnboardEventsFailState"
        }],
        "End": true,
        "Retry": ${local.default_retry}
      },
      "DeleteUserBitsOnboardEventsFailState": {
        "Comment": "Failed to delete user from bits-onboard-events S3 bucket",
        "Type": "Fail"
      }
    }
  }
EOF

  delete_channel_cheermotes_step = <<EOF
  {
    "StartAt": "DeleteChannelCheermotes",
    "States": {
      "DeleteChannelCheermotes": {
        "Comment": "Deleting user cheermotes from bits-assets/partner-actions S3 bucket",
        "Type": "Task",
        "Resource": "${module.delete_channel_cheermotes.lambda_arn}",
        "ResultPath": "$.deletedUser",
        "Catch" : [{
          "ErrorEquals": ["States.ALL"],
          "Next": "DeleteChannelCheermotesFailState"
        }],
        "End": true,
        "Retry": ${local.default_retry}
      },
      "DeleteChannelCheermotesFailState": {
        "Comment": "Failed to delete user cheermotes from bits-assets/partner-actions S3 bucket",
        "Type": "Fail"
      }
    }
  }
EOF

  delete_user_purchase_by_platform_step = <<EOF
  {
    "StartAt": "DeleteUserPurchaseByPlatform",
    "States": {
      "DeleteUserPurchaseByPlatform": {
        "Comment": "Deleting user from Dynamo Table purchase_by_platform",
        "Type": "Task",
        "Resource": "${module.delete_user_purchase_by_platform.lambda_arn}",
        "ResultPath": "$.deletedUser",
        "Catch": [{
          "ErrorEquals": ["States.ALL"],
          "Next": "DeleteUserPurchaseByPlatformFailState"
        }],
        "End": true,
        "Retry": ${local.default_retry}
      },
      "DeleteUserPurchaseByPlatformFailState": {
        "Comment": "Failed to delete user from Dynamo Table purchase_by_platform",
        "Type": "Fail"
      }
    }
  }
EOF

  delete_user_single_purchase_consumption_step = <<EOF
  {
    "StartAt": "DeleteUserSinglePurchaseConsumption",
    "States": {
      "DeleteUserSinglePurchaseConsumption": {
        "Comment": "Deleting user from Dynamo Tables single_purchase_consump",
        "Type": "Task",
        "Resource": "${module.delete_user_single_purchase_consumption.lambda_arn}",
        "ResultPath": "$.deletedUser",
        "Catch": [{
          "ErrorEquals": ["States.ALL"],
          "Next": "DeleteUserSinglePurchaseConsumptionFailState"
        }],
        "End": true,
        "Retry": ${local.default_retry}
      },
      "DeleteUserSinglePurchaseConsumptionFailState": {
        "Comment": "Failed to delete user from Dynamo Table single_purchase_consump",
        "Type": "Fail"
      }
    }
  }
EOF

  delete_channel_badge_tiers_and_emote_groupids_step = <<EOF
  {
    "StartAt": "DeleteChannelBadgeTiersAndEmoteGroupIDs",
    "States": {
      "DeleteChannelBadgeTiersAndEmoteGroupIDs": {
        "Comment": "Deleting user from Dynamo Tables: prod_badge_tiers, prod_badge_tier_emote_groupids",
        "Type": "Task",
        "Resource": "${module.delete_channel_badge_tiers_and_emote_groupids.lambda_arn}",
        "ResultPath": "$.deletedUser",
        "Catch" : [{
          "ErrorEquals": ["States.ALL"],
          "Next": "DeleteChannelBadgeTiersAndEmoteGroupIDsFailState"
        }],
        "End": true,
        "Retry": ${local.default_retry}
      },
      "DeleteChannelBadgeTiersAndEmoteGroupIDsFailState": {
        "Comment": "Failed to delete user from Dynamo Tables: prod_badge_tiers, prod_badge_tier_emote_groupids",
        "Type": "Fail"
      }
    }
  }
EOF

  delete_user_step = <<EOF
  {
    "StartAt": "DeleteUser",
    "States": {
      "DeleteUser": {
        "Comment": "Deleting user from Dynamo Table prod_users",
        "Type": "Task",
        "Resource": "${module.delete_user.lambda_arn}",
        "ResultPath": "$.deletedUser",
        "Catch" : [{
          "ErrorEquals": ["States.ALL"],
          "Next": "DeleteUserFailState"
        }],
        "End": true,
        "Retry": ${local.default_retry}
      },
      "DeleteUserFailState": {
        "Comment": "Failed to delete user from Dynamo Table prod_users",
        "Type": "Fail"
      }
    }
  }
EOF

  delete_user_leaderboard_step = <<EOF
  {
    "StartAt": "DeleteUserLeaderboard",
    "States": {
      "DeleteUserLeaderboard": {
        "Type": "Task",
        "Resource": "${module.delete_user_leaderboard.lambda_arn}",
        "ResultPath": "$.deletedUser",
        "Comment": "Deleting user leaderboard info from Pantheon",
        "Catch": [{
          "ErrorEquals": ["States.ALL"],
          "Next": "DeleteUserLeaderboardFailState"
        }],
        "End": true,
        "Retry": ${local.default_retry}
      },
      "DeleteUserLeaderboardFailState": {
        "Comment": "Failed to delete user leaderboard info from Pantheon",
        "Type": "Fail"
      }
    }
  }
EOF

  delete_user_leaderboard_badge_holders_step = <<EOF
  {
    "StartAt": "DeleteUserLeaderboardBadgeHolders",
    "States": {
      "DeleteUserLeaderboardBadgeHolders": {
        "Comment": "Deleting user from Dynamo Table leaderboard_badge_holders",
        "Type": "Task",
        "Resource": "${module.delete_user_leaderboard_badge_holders.lambda_arn}",
        "ResultPath": "$.deletedUser",
        "Catch": [{
          "ErrorEquals": ["States.ALL"],
          "Next": "DeleteUserLeaderboardBadgeHoldersFailState"
        }],
        "End": true,
        "Retry": ${local.default_retry}
      },
      "DeleteUserLeaderboardBadgeHoldersFailState": {
        "Comment": "Failed to delete user from Dynamo Table leaderboard_badge_holders",
        "Type": "Fail"
      }
    }
  }
EOF

  delete_user_channels_and_images_step = <<EOF
  {
    "StartAt": "DeleteUserChannelsAndImages",
    "States": {
      "DeleteUserChannelsAndImages": {
        "Comment": "Deleting user from Dynamo Tables: prod_channels, prod_images",
        "Type": "Task",
        "Resource": "${module.delete_user_channels_and_images.lambda_arn}",
        "ResultPath": "$.deletedUser",
        "Catch" : [{
          "ErrorEquals": ["States.ALL"],
          "Next": "DeleteUserChannelsAndImagesFailState"
        }],
        "End": true,
        "Retry": ${local.default_retry}
      },
      "DeleteUserChannelsAndImagesFailState": {
        "Comment": "Failed to delete user from Dynamo Tables: prod_channels, prod_images",
        "Type": "Fail"
      }
    }
  }
EOF

  delete_user_spomote_channel_statuses_step = <<EOF
  {
    "StartAt": "DeleteUserSpomoteChannelStatuses",
    "States": {
      "DeleteUserSpomoteChannelStatuses": {
        "Comment": "Deleting user from Dynamo Table sponsored_cheermote_channel_statuses",
        "Type": "Task",
        "Resource": "${module.delete_user_spomote_channel_statuses.lambda_arn}",
        "ResultPath": "$.deletedUser",
        "Catch": [{
          "ErrorEquals": ["States.ALL"],
          "Next": "DeleteUserSpomoteChannelStatusesFailState"
        }],
        "End": true,
        "Retry": ${local.default_retry}
      },
      "DeleteUserSpomoteChannelStatusesFailState": {
        "Comment": "Failed to delete user from Dynamo Table sponsored_cheermote_channel_statuses",
        "Type": "Fail"
      }
    }
  }
EOF

  delete_user_spomote_eligible_channels_step = <<EOF
  {
    "StartAt": "DeleteUserSpomoteEligibleChannels",
    "States": {
      "DeleteUserSpomoteEligibleChannels": {
        "Comment": "Deleting user from Dynamo Table sponsored_cheermote_eligible_channels",
        "Type": "Task",
        "Resource": "${module.delete_user_spomote_eligible_channels.lambda_arn}",
        "ResultPath": "$.deletedUser",
        "Catch": [{
          "ErrorEquals": ["States.ALL"],
          "Next": "DeleteUserSpomoteEligibleChannelsFailState"
        }],
        "End": true,
        "Retry": ${local.default_retry}
      },
      "DeleteUserSpomoteEligibleChannelsFailState": {
        "Comment": "Failed to delete user from Dynamo Table sponsored_cheermote_eligible_channels",
        "Type": "Fail"
      }
    }
  }
EOF

  delete_user_spomote_user_statuses_step = <<EOF
  {
    "StartAt": "DeleteUserSpomoteUserStatuses",
    "States": {
      "DeleteUserSpomoteUserStatuses": {
        "Comment": "Deleting user from Dynamo Table sponsored_cheermote_user_statuses",
        "Type": "Task",
        "Resource": "${module.delete_user_spomote_user_statuses.lambda_arn}",
        "ResultPath": "$.deletedUser",
        "Catch": [{
          "ErrorEquals": ["States.ALL"],
          "Next": "DeleteUserSpomoteUserStatusesFailState"
        }],
        "End": true,
        "Retry": ${local.default_retry}
      },
      "DeleteUserSpomoteUserStatusesFailState": {
        "Comment": "Failed to delete user from Dynamo Table sponsored_cheermote_user_statuses",
        "Type": "Fail"
      }
    }
  }
EOF

  delete_user_balance_step = <<EOF
  {
    "StartAt": "DeleteUserBalance",
    "States": {
      "DeleteUserBalance": {
        "Comment": "Deleting user balance from Pachinko",
        "Type": "Task",
        "Resource": "${module.delete_user_balance.lambda_arn}",
        "ResultPath": "$.deletedUser",
        "Catch" : [{
          "ErrorEquals": ["States.ALL"],
          "Next": "DeleteUserBalanceFailState"
        }],
        "End": true,
        "Retry": ${local.default_retry}
      },
      "DeleteUserBalanceFailState": {
        "Comment": "Failed to delete user balance from Pachinko",
        "Type": "Fail"
      }
    }
  }
EOF
}
