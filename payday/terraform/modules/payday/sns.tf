module "bits_emoticon_backfill" {
  source = "./sns_topic"
  topic-name = "bits-emoticon-backfill"
  sns-topic-name-suffix = var.env_prefix
}
