// Necessary for Elastic Beanstalk to check the health of the instances
resource "aws_iam_role" "health_service_role" {
  name = "${var.env}_eb_health_service_role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "elasticbeanstalk.amazonaws.com"
      },
      "Action": "sts:AssumeRole",
      "Condition": {
        "StringEquals": {
          "sts:ExternalId": "elasticbeanstalk"
        }
      }
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "health_service_role_policy" {
  name = "${var.env}_eb_health_service_role"
  role = aws_iam_role.health_service_role.name

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "AllowCloudformationOperationsOnElasticBeanstalkStacks",
            "Effect": "Allow",
            "Action": [
                "cloudformation:*"
            ],
            "Resource": [
                "arn:aws:cloudformation:*:*:stack/awseb-*",
                "arn:aws:cloudformation:*:*:stack/eb-*"
            ]
        },
        {
            "Sid": "AllowDeleteCloudwatchLogGroups",
            "Effect": "Allow",
            "Action": [
                "logs:DeleteLogGroup"
            ],
            "Resource": [
                "arn:aws:logs:*:*:log-group:/aws/elasticbeanstalk*"
            ]
        },
        {
            "Sid": "AllowS3OperationsOnElasticBeanstalkBuckets",
            "Effect": "Allow",
            "Action": [
                "s3:*"
            ],
            "Resource": [
                "arn:aws:s3:::elasticbeanstalk-*",
                "arn:aws:s3:::elasticbeanstalk-*/*"
            ]
        },
        {
            "Sid": "AllowOperations",
            "Effect": "Allow",
            "Action": [
                "autoscaling:AttachInstances",
                "autoscaling:CreateAutoScalingGroup",
                "autoscaling:CreateLaunchConfiguration",
                "autoscaling:DeleteLaunchConfiguration",
                "autoscaling:DeleteAutoScalingGroup",
                "autoscaling:DeleteScheduledAction",
                "autoscaling:DescribeAccountLimits",
                "autoscaling:DescribeAutoScalingGroups",
                "autoscaling:DescribeAutoScalingInstances",
                "autoscaling:DescribeLaunchConfigurations",
                "autoscaling:DescribeLoadBalancers",
                "autoscaling:DescribeNotificationConfigurations",
                "autoscaling:DescribeScalingActivities",
                "autoscaling:DescribeScheduledActions",
                "autoscaling:DetachInstances",
                "autoscaling:PutScheduledUpdateGroupAction",
                "autoscaling:ResumeProcesses",
                "autoscaling:SetDesiredCapacity",
                "autoscaling:SuspendProcesses",
                "autoscaling:TerminateInstanceInAutoScalingGroup",
                "autoscaling:UpdateAutoScalingGroup",
                "cloudwatch:PutMetricAlarm",
                "cloudwatch:PutMetricData",
                "ec2:AssociateAddress",
                "ec2:AllocateAddress",
                "ec2:AuthorizeSecurityGroupEgress",
                "ec2:AuthorizeSecurityGroupIngress",
                "ec2:CreateSecurityGroup",
                "ec2:DeleteSecurityGroup",
                "ec2:DescribeAccountAttributes",
                "ec2:DescribeAddresses",
                "ec2:DescribeImages",
                "ec2:DescribeInstances",
                "ec2:DescribeKeyPairs",
                "ec2:DescribeSecurityGroups",
                "ec2:DescribeSubnets",
                "ec2:DescribeVpcs",
                "ec2:DisassociateAddress",
                "ec2:ReleaseAddress",
                "ec2:RevokeSecurityGroupEgress",
                "ec2:RevokeSecurityGroupIngress",
                "ec2:TerminateInstances",
                "ecs:CreateCluster",
                "ecs:DeleteCluster",
                "ecs:DescribeClusters",
                "ecs:RegisterTaskDefinition",
                "elasticbeanstalk:*",
                "elasticloadbalancing:ApplySecurityGroupsToLoadBalancer",
                "elasticloadbalancing:ConfigureHealthCheck",
                "elasticloadbalancing:CreateLoadBalancer",
                "elasticloadbalancing:DeleteLoadBalancer",
                "elasticloadbalancing:DeregisterInstancesFromLoadBalancer",
                "elasticloadbalancing:DescribeInstanceHealth",
                "elasticloadbalancing:DescribeLoadBalancers",
                "elasticloadbalancing:DescribeTargetHealth",
                "elasticloadbalancing:RegisterInstancesWithLoadBalancer",
                "elasticloadbalancing:DescribeTargetGroups",
                "elasticloadbalancing:RegisterTargets",
                "elasticloadbalancing:DeregisterTargets",
                "iam:ListRoles",
                "iam:PassRole",
                "logs:CreateLogGroup",
                "logs:PutRetentionPolicy",
                "rds:DescribeDBEngineVersions",
                "rds:DescribeDBInstances",
                "rds:DescribeOrderableDBInstanceOptions",
                "s3:CopyObject",
                "s3:GetObject",
                "s3:GetObjectAcl",
                "s3:GetObjectMetadata",
                "s3:ListBucket",
                "s3:listBuckets",
                "s3:ListObjects",
                "sns:CreateTopic",
                "sns:GetTopicAttributes",
                "sns:ListSubscriptionsByTopic",
                "sns:Subscribe",
                "sqs:GetQueueAttributes",
                "sqs:GetQueueUrl",
                "codebuild:CreateProject",
                "codebuild:DeleteProject",
                "codebuild:BatchGetBuilds",
                "codebuild:StartBuild"
            ],
            "Resource": [
                "*"
            ]
        },
        {
            "Effect": "Allow",
            "Action": [
                "elasticloadbalancing:DescribeInstanceHealth",
                "elasticloadbalancing:DescribeLoadBalancers",
                "elasticloadbalancing:DescribeTargetHealth",
                "ec2:DescribeInstances",
                "ec2:DescribeInstanceStatus",
                "ec2:GetConsoleOutput",
                "ec2:AssociateAddress",
                "ec2:DescribeAddresses",
                "ec2:DescribeSecurityGroups",
                "sqs:GetQueueAttributes",
                "sqs:GetQueueUrl",
                "autoscaling:DescribeAutoScalingGroups",
                "autoscaling:DescribeAutoScalingInstances",
                "autoscaling:DescribeScalingActivities",
                "autoscaling:DescribeNotificationConfigurations"
            ],
            "Resource": [
                "*"
            ]
        }
    ]
}
EOF
}

// ECS/Lambda Policies

// Grant the Instance Profile full dynamo access
resource "aws_iam_role_policy_attachment" "dynamo_policy_attachment" {
  role       = module.service.service_role
  policy_arn = "arn:aws:iam::aws:policy/AmazonDynamoDBFullAccess"
}

resource "aws_iam_role_policy_attachment" "dynamo_policy_attachment_canary" {
  role       = module.service.canary_role
  policy_arn = "arn:aws:iam::aws:policy/AmazonDynamoDBFullAccess"
}

resource "aws_iam_role_policy_attachment" "dynamo_policy_attachment_lambda" {
  role       = module.lambda-role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonDynamoDBFullAccess"
}

// Grant the Instance Profile full SQS access
resource "aws_iam_role_policy_attachment" "sqs_policy_attachment" {
  role       = module.service.service_role
  policy_arn = "arn:aws:iam::aws:policy/AmazonSQSFullAccess"
}

resource "aws_iam_role_policy_attachment" "sqs_policy_attachment_canary" {
  role       = module.service.canary_role
  policy_arn = "arn:aws:iam::aws:policy/AmazonSQSFullAccess"
}

resource "aws_iam_role_policy_attachment" "sqs_policy_attachment_lambda" {
  role       = module.lambda-role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonSQSFullAccess"
}

// Grant the Instance Profile full SNS access
resource "aws_iam_role_policy_attachment" "sns_policy_attachment" {
  role       = module.service.service_role
  policy_arn = "arn:aws:iam::aws:policy/AmazonSNSFullAccess"
}

resource "aws_iam_role_policy_attachment" "sns_policy_attachment_canary" {
  role       = module.service.canary_role
  policy_arn = "arn:aws:iam::aws:policy/AmazonSNSFullAccess"
}

resource "aws_iam_role_policy_attachment" "sns_policy_attachment_lambda" {
  role       = module.lambda-role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonSNSFullAccess"
}

// Grant the Instance Profile full S3 access
resource "aws_iam_role_policy_attachment" "s3_policy_attachment" {
  role       = module.service.service_role
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
}

resource "aws_iam_role_policy_attachment" "s3_policy_attachment_canary" {
  role       = module.service.canary_role
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
}

resource "aws_iam_role_policy_attachment" "s3_policy_attachment_lambda" {
  role       = module.lambda-role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
}

// Grant the Instance Profile full SFN access
resource "aws_iam_role_policy_attachment" "sfn_policy_attachment" {
  role       = module.service.service_role
  policy_arn = "arn:aws:iam::aws:policy/AWSStepFunctionsFullAccess"
}

resource "aws_iam_role_policy_attachment" "sfn_policy_attachment_canary" {
  role       = module.service.canary_role
  policy_arn = "arn:aws:iam::aws:policy/AWSStepFunctionsFullAccess"
}

// Sandstorm policy enables sandstorm by granting the ability to assume a specifc role
resource "aws_iam_policy" "sandstorm_policy_ecs" {
  name = "sandstorm-ecs-assume-role-policy-${var.stage}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Resource": [
        "arn:aws:iam::734326455073:role/sandstorm/production/templated/role/${var.sandstorm_role}"
      ],
      "Effect": "Allow"
    }
  ]
}
EOF
}

// Grant the Instance Profile Sandstorm access
resource "aws_iam_role_policy_attachment" "sandstorm_policy_attachment" {
  role       = module.service.service_role
  policy_arn = aws_iam_policy.sandstorm_policy_ecs.arn
}

resource "aws_iam_role_policy_attachment" "sandstorm_policy_attachment_canary" {
  role       = module.service.canary_role
  policy_arn = aws_iam_policy.sandstorm_policy_ecs.arn
}

resource "aws_iam_role_policy_attachment" "sandstorm_policy_attachment_lambda" {
  role       = module.lambda-role.name
  policy_arn = aws_iam_policy.sandstorm_policy_ecs.arn
}

// CloudFront policy enables us to invalidate the CDN cache for Custom Cheermotes
resource "aws_iam_policy" "cloudfront_policy_ecs" {
  name = "cloudfront-ecs-assume-role-policy-${var.stage}"

  policy = <<EOF
{
   "Version": "2012-10-17",
   "Statement": [
      {
         "Effect": "Allow",
         "Action": [
           "cloudfront:CreateInvalidation",
           "cloudfront:GetInvalidation",
           "cloudfront:ListInvalidations"
         ],
         "Resource": "*"
      }
   ]
}
EOF
}

// Grant the Instance Cloudfront access
resource "aws_iam_role_policy_attachment" "cloudfront_policy_attachment" {
  role       = module.service.service_role
  policy_arn = aws_iam_policy.cloudfront_policy_ecs.arn
}

resource "aws_iam_role_policy_attachment" "cloudfront_policy_attachment_canary" {
  role       = module.service.canary_role
  policy_arn = aws_iam_policy.cloudfront_policy_ecs.arn
}

resource "aws_iam_role_policy_attachment" "cloudfront_policy_attachment_lambda" {
  role       = module.lambda-role.name
  policy_arn = aws_iam_policy.cloudfront_policy_ecs.arn
}

// S2S policy
resource "aws_iam_policy" "s2s_policy_ecs" {
  name = "s2s-ecs-assume-role-policy-${var.stage}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": {
    "Effect": "Allow",
    "Action": "sts:AssumeRole",
    "Resource": "arn:aws:iam::180116294062:role/malachai/*"
  }
}
EOF
}

// Grant the Instance Profile S2S access
resource "aws_iam_role_policy_attachment" "s2s_policy_attachment" {
  role       = module.service.service_role
  policy_arn = aws_iam_policy.s2s_policy_ecs.arn
}

resource "aws_iam_role_policy_attachment" "s2s_policy_attachment_canary" {
  role       = module.service.canary_role
  policy_arn = aws_iam_policy.s2s_policy_ecs.arn
}

resource "aws_iam_role_policy_attachment" "s2s_policy_attachment_lambda" {
  role       = module.lambda-role.name
  policy_arn = aws_iam_policy.s2s_policy_ecs.arn
}

// TODO: move resource creation from terraform/payday.tf to this file, and dynamically reference the arn
resource "aws_iam_role_policy_attachment" "eventbus_access_attach" {
  role       = module.service.service_role
  policy_arn = var.event_bus_iam_arn
}

resource "aws_iam_role_policy_attachment" "eventbus_access_canary_attach" {
  role       = module.service.canary_role
  policy_arn = var.event_bus_iam_arn
}

resource "aws_iam_role_policy_attachment" "eventbus_access_lambda_attach" {
  role       = module.lambda-role.name
  policy_arn = var.event_bus_iam_arn
}


# roles and policies for KMS access
resource "aws_iam_role_policy_attachment" "payday_ecs_kms_assume_role" {
  policy_arn = "arn:aws:iam::aws:policy/AWSKeyManagementServicePowerUser"
  role       = aws_iam_role.payday_ecs_kms.name
}

resource "aws_iam_role_policy_attachment" "payday_ecs_kms" {
  policy_arn = "arn:aws:iam::aws:policy/AWSKeyManagementServicePowerUser"
  role       = module.service.service_role
}

resource "aws_iam_role_policy_attachment" "payday_ecs_kms_canary" {
  policy_arn = "arn:aws:iam::aws:policy/AWSKeyManagementServicePowerUser"
  role       = module.service.canary_role
}

resource "aws_iam_role_policy_attachment" "payday_ecs_kms_lambda" {
  policy_arn = "arn:aws:iam::aws:policy/AWSKeyManagementServicePowerUser"
  role       = module.lambda-role.name
}

resource "aws_iam_user_policy_attachment" "payday_ecs_kms_user" {
  policy_arn = "arn:aws:iam::aws:policy/AWSKeyManagementServicePowerUser"
  user       = "kms"
}

resource "aws_iam_role" "payday_ecs_kms" {
  name = "payday_ecs_kms_${var.stage}"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::673385534282:role/beanstalk_worker_profile",
        "Service": "ec2.amazonaws.com"
      }
    }
  ]
}
EOF
}

# roles and policies for Kinesis Firehose access
resource "aws_iam_policy_attachment" "payday_ecs_firehose" {
  name       = "payday_ecs_firehose_${var.stage}"
  policy_arn = aws_iam_policy.payday_ecs_firehose.arn
  roles      = [module.service.service_role, module.service.canary_role, module.lambda-role.name]
}

locals {
  firehose_arn_suffix = var.stage == "staging" ? "-staging" : ""
}

resource "aws_iam_policy" "payday_ecs_firehose" {
  name        = "payday_ecs_firehose_${var.stage}"
  description = "write access to production kinesis firehose delivery streams"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "firehose:PutRecord",
        "firehose:PutRecordBatch"
      ],
      "Effect": "Allow",
      "Resource": [
        "arn:aws:firehose:us-west-2:021561903526:deliverystream/payday-raincatcher-balanceupdates${local.firehose_arn_suffix}",
        "arn:aws:firehose:us-west-2:021561903526:deliverystream/payday-raincatcher-transactions${local.firehose_arn_suffix}",
        "arn:aws:firehose:us-west-2:021561903526:deliverystream/payday-raincatcher-emoteuses${local.firehose_arn_suffix}",
        "arn:aws:firehose:us-west-2:021561903526:deliverystream/payday-raincatcher-transactions-audit${local.firehose_arn_suffix}"
      ]
    }
  ]
}
EOF
}

resource "aws_iam_policy_attachment" "payday_ecs_auto_refill_table_access" {
  name       = "payday_ecs_auto_refill_table_access_${var.stage}"
  policy_arn = aws_iam_policy.payday_ecs_auto_refill_table_access.arn
  roles      = [module.service.service_role, module.service.canary_role, module.lambda-role.name]
}

resource "aws_iam_policy" "payday_ecs_auto_refill_table_access" {
  name        = "payday_ecs_auto_refill_table_access_${var.stage}"
  description = "Table access to the auto refill aws account"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "sts:AssumeRole",
        "dynamodb:*"
      ],
      "Effect": "Allow",
      "Resource": [
        "arn:aws:iam::${var.auto_refill_table_account_id}:role/PaydayDynamoAccessRole",
        "arn:aws:dynamodb:us-west-2:${var.auto_refill_table_account_id}:table/*"
      ]
    }
  ]
}
EOF
}

// PDMS lambda role, required to report deletion
// see https://wiki.twitch.com/display/SEC/PDMS-+Delete+pipeline+Service+Onboarding
locals {
  pdms_lambda_caller_role = {
    staging    = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Action": "sts:AssumeRole",
      "Resource": [
        "arn:aws:iam::895799599216:role/PDMSLambda-CallerRole-18451FI19HSXT"
      ]
    }
  ]
}
EOF
    production = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Action": "sts:AssumeRole",
      "Resource": [
        "arn:aws:iam::125704643346:role/PDMSLambda-CallerRole-13IIND444YKVR"
      ]
    }
  ]
}
EOF
  }
}

resource "aws_iam_policy" "payday_pdms_lambda_caller_role" {
  name        = "payday_pdms_lambda_caller_role_${var.stage}"
  description = "IAM Policy required to assume the PDMS caller role"
  policy      = lookup(local.pdms_lambda_caller_role, var.stage)
}

resource "aws_iam_role_policy_attachment" "payday_ecs_pdms_lambda_caller_role_attachment" {
  role       = module.service.service_role
  policy_arn = aws_iam_policy.payday_pdms_lambda_caller_role.arn
}

resource "aws_iam_role_policy_attachment" "payday_lambda_pdms_lambda_caller_role_attachment" {
  role       = module.lambda-role.name
  policy_arn = aws_iam_policy.payday_pdms_lambda_caller_role.arn
}

// For S2S2
resource "aws_iam_policy" "s2s2_iam_policy" {
  name        = "${var.iam_role_name}_s2s2_iam_policy"
  description = "required IAM permission to use S2S2"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "execute-api:Invoke"
            ],
            "Resource": [
                "arn:aws:execute-api:*:985585625942:*"
            ],
            "Effect": "Allow"
        }
    ]
}
EOF
}
