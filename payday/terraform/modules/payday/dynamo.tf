module "dynamo_backup_lambda" {
  source = "../dynamo_backup_lambda"
}

module "payday_dynamo_tables" {
  source                                        = "./payday_dynamo_tables"
  account_id                                    = var.account_id
  prefix                                        = var.env_prefix
  stage                                         = var.stage
  min_read_capacity                             = var.min_read_capacity
  min_write_capacity                            = var.min_write_capacity
  max_read_capacity                             = var.max_read_capacity
  max_write_capacity                            = var.max_write_capacity
  dynamo_backup_lambda_arn                      = module.dynamo_backup_lambda.lambda_arn
  holds_read_capacity_prod                      = var.min_read_capacity
  in_flight_transactions_read_capacity_prod     = var.min_read_capacity
  leaderboard_badge_holders_read_capacity_prod  = "100"
  leaderboard_badge_holders_write_capacity_prod = "100"
  tahoe_api_password_name                       = aws_ssm_parameter.tahoe_api_password.name
  tahoe_api_tagert_key_id                       = aws_kms_alias.tahoe_api.target_key_id
}
