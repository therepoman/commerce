// Copied from https://git.xarth.tv/terraform-modules/account_template/blob/a8e6ab785ad359f6b77da498841d0f460caacf04/default_sg.tf
// Payday didn't have the account template terraform applied, so twitch_subnets was never created.
// Payday's old terraform created a similar SG, but it was overloaded with some EBS-specific things.
resource "aws_security_group" "twitch_subnets" {
  name        = "twitch_subnets_${var.stage}"
  description = "allow all traffic between twitch IP space"
  vpc_id      = var.vpc_id

  tags = {
    Name = "twitch_subnets_${var.stage}"
  }

  ingress {
    from_port = 0
    to_port   = 0
    protocol  = -1

    cidr_blocks = [
      "10.0.0.0/8",
      "192.16.64.0/21",
      "199.9.248.0/21",
      "185.42.204.0/22",
      "127.0.0.1/32",
      "38.99.10.96/29",
      "38.99.7.224/27",
      "38.104.129.210/32",
      "69.85.223.168/29",
      "192.168.0.0/16",
      "45.113.128.0/22",
      "103.53.48.0/22",
      "192.108.239.0/24",
      "23.160.0.0/24",
      "52.223.192.0/18",
      "98.173.4.128/27",
      "12.245.210.116/30",
      "54.240.196.173/32",
      "54.240.198.32/29",
      "204.246.162.32/28",
      "205.251.237.64/28",
      "205.251.237.96/28",
      "52.95.4.0/25",
      "54.240.217.8/29",
      "54.240.217.16/29",
      "72.21.196.64/29",
      "72.21.198.64/29",
      "99.181.64.0/18",
    ]

    self = true
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }
}

module "service" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//twitch-service?ref=7a97551c37b059b469c6734f3d64771a2d5af6ba"

  name                   = "payday-${var.stage}"
  image                  = aws_ecr_repository.repository.repository_url
  memory                 = var.ecs_memory
  logging                = var.ecs_logging
  cpu                    = var.ecs_cpu
  canary_cpu             = var.ecs_cpu
  canary_memory          = var.ecs_memory
  cpu_scale_up_threshold = var.cpu_scale_up_threshold
  alb_port               = var.ecs_alb_port
  env_vars               = var.ecs_env_vars

  counts = {
    min = var.ecs_min_instances
    max = var.ecs_max_instances
  }

  port          = 8001
  dns           = var.dns
  secondary_dns = var.secondary_dns

  access_logs_expiration_days = var.alb_access_logs_expiration_days
  fargate_platform_version    = ""
  healthcheck                 = var.ecs_healthcheck
  cluster                     = module.cluster.ecs_name
  security_group              = aws_security_group.twitch_subnets.id
  region                      = var.aws_region
  vpc_id                      = var.vpc_id
  environment                 = var.stage
  subnets                     = var.private_subnets
}

output "service_dns" {
  value = module.service.dns
}

// For S2S2
resource "aws_iam_role_policy_attachment" "s2s2_iam_policy_service_attachment" {
  role       = module.service.service_role
  policy_arn = aws_iam_policy.s2s2_iam_policy.arn
}

resource "aws_iam_role_policy_attachment" "s2s2_iam_policy_canary_attachment" {
  role       = module.service.canary_role
  policy_arn = aws_iam_policy.s2s2_iam_policy.arn
}
