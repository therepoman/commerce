variable "queue_name" {
  type        = string
  description = "name of the sqs queue, like 'emoticon-backfill'"
}

variable "sns_topic_to_subscribe_to" {
  type        = string
  description = "the sns topic you wish to listen to"
}

variable "alarm_dlq" {
  description = "a boolean for whether or not to create an alarm for the dlq"
}

variable "queue_name_suffix" {
  type        = string
  default     = ""
  description = "a way to append _staging to the end of the dlqs in staging"
}

variable "max_receive_count" {
  type    = string
  default = 7
}

variable "iam_role_name" {
  type        = string
  default     = ""
  description = "The iam role attached used by the beanstalk application"
}

variable "env_prefix" {
  type        = string
  default     = ""
  description = "Prefix to represent this environment"
}

locals {
  full_queue_name = "payday_${var.queue_name}_queue"
}

module "payday_queue" {
  source                    = "../sqs_queue"
  queue_name                = local.full_queue_name
  sns_topic_to_subscribe_to = var.sns_topic_to_subscribe_to
  alarm_dlq                 = var.alarm_dlq
  max_receive_count         = var.max_receive_count
  queue_name_suffix         = var.queue_name_suffix
  iam_role_name             = var.iam_role_name
  env_prefix                = var.env_prefix
}

output "queue_arn" {
  value = module.payday_queue.queue_arn
}