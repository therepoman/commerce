locals {
  alarm_count = var.stage == "production" ? 1 : 0
}

resource "aws_cloudwatch_metric_alarm" "error_log_alarm" {
  alarm_name          = "payday-ecs-error-logs-alarm-${var.stage}"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "3"
  metric_name         = "payday-errors-${var.stage}"
  namespace           = local.log_metric_namespace
  period              = "300"
  statistic           = "Sum"
  datapoints_to_alarm = "3"
  threshold           = "1500" // At 50k TPS, this represents 99.99% availibility
  alarm_description   = "High volume of error logs"
  treat_missing_data  = "notBreaching"
}

resource "aws_cloudwatch_metric_alarm" "canary_error_log_alarm" {
  alarm_name          = "payday-ecs-canary-error-log-alarm-${var.stage}"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "3"
  metric_name         = "payday-errors-${var.stage}-canary"
  namespace           = local.log_metric_namespace
  period              = "300"
  statistic           = "Sum"
  datapoints_to_alarm = "3"
  threshold           = "15" // At 50k TPS, this represents 99.99% availibility
  alarm_description   = "High volume of error logs in canary"
  treat_missing_data  = "notBreaching"
}

resource "aws_cloudwatch_metric_alarm" "canary_panic_log_alarm" {
  alarm_name          = "payday-ecs-canary-panic-logs-alarm-${var.stage}"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "payday-panics-${var.stage}-canary"
  namespace           = local.log_metric_namespace
  period              = "60"
  statistic           = "Sum"
  datapoints_to_alarm = "1"
  threshold           = "1"
  alarm_description   = "Panic has occurred in Payday canary."
  treat_missing_data  = "notBreaching"
}

resource "aws_cloudwatch_metric_alarm" "panic_log_alarm" {
  alarm_name          = "payday-ecs-panic-logs-alarm-${var.stage}"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "payday-panics-${var.stage}"
  namespace           = local.log_metric_namespace
  period              = "60"
  statistic           = "Sum"
  datapoints_to_alarm = "1"
  threshold           = "1"
  alarm_description   = "Panic has occurred in Payday."
  treat_missing_data  = "notBreaching"
}

# API alarms
resource "aws_cloudwatch_metric_alarm" "get_bits_tier_summary_for_emote_group_id_latency" {
  count               = local.alarm_count
  alarm_name          = "get-bits-tier-summary-for-emote-group-id-latency"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "3"
  metric_name         = "TWIRP:GetBitsTierSummaryForEmoteGroupID"
  namespace           = "payday"

  dimensions = {
    Stage   = "production"
    Region  = "us-west-2"
    Service = "payday"
  }

  period                    = "300"
  extended_statistic        = "p99"
  threshold                 = "0.2"
  alarm_description         = "Alarms when GetBitsTierSummaryForEmoteGroupID API latency breaches threshold"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"
}

resource "aws_cloudwatch_metric_alarm" "get_bits_tier_summary_for_emote_group_id_500_errors" {
  count               = local.alarm_count
  alarm_name          = "get-bits-tier-summary-for-emote-group-id-500-errors"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "3"
  metric_name         = "TWIRP:GetBitsTierSummaryForEmoteGroupID.5xx"
  namespace           = "payday"

  dimensions = {
    Stage   = "production"
    Region  = "us-west-2"
    Service = "payday"
  }

  period                    = "300"
  statistic                 = "Sum"
  threshold                 = "10"
  alarm_description         = "Alarms when GetBitsTierSummaryForEmoteGroupID API 500 status code error count breaches threshold"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"
}

/**
 * Availability twirp
 */
module "GetBitsToBroadcaster_availability" {
  source                     = "./availability"
  api_name                   = "GetBitsToBroadcaster"
  stage                      = var.stage
  low_watermark              = "0.995"
  requests_metric_name_5xx   = "TWIRP:GetBitsToBroadcaster.5xx"
  requests_metric_name_total = "TWIRP:GetBitsToBroadcaster.requests"
}

module "GetBitsBadgeTierEmotes_availability" {
  source                     = "./availability"
  api_name                   = "GetBitsBadgeTierEmotes"
  stage                      = var.stage
  low_watermark              = "0.995"
  requests_metric_name_5xx   = "TWIRP:GetBitsBadgeTierEmotes.5xx"
  requests_metric_name_total = "TWIRP:GetBitsBadgeTierEmotes.requests"
}

module "GetBalance_availability" {
  source                     = "./availability"
  api_name                   = "GetBalance"
  stage                      = var.stage
  low_watermark              = "0.995"
  requests_metric_name_5xx   = "TWIRP:GetBalance.5xx"
  requests_metric_name_total = "TWIRP:GetBalance.requests"
}

module "GetChannelCheers_availability" {
  source                     = "./availability"
  api_name                   = "GetChannelCheers"
  stage                      = var.stage
  low_watermark              = "0.995"
  requests_metric_name_5xx   = "TWIRP:GetChannelCheers.5xx"
  requests_metric_name_total = "TWIRP:GetChannelCheers.requests"
}

module "GetBitsTierSummaryForEmoteGroupID_availability" {
  source                     = "./availability"
  api_name                   = "GetBitsTierSummaryForEmoteGroupID"
  stage                      = var.stage
  low_watermark              = "0.995"
  requests_metric_name_5xx   = "TWIRP:GetBitsTierSummaryForEmoteGroupID.5xx"
  requests_metric_name_total = "TWIRP:GetBitsTierSummaryForEmoteGroupID.requests"
}

module "PostProcessMessage_availability" {
  source                     = "./availability"
  api_name                   = "PostProcessMessage"
  stage                      = var.stage
  low_watermark              = "0.995"
  requests_metric_name_5xx   = "TWIRP:/bits-prism-tenant/PostProcessMessage.5xx"
  requests_metric_name_total = "TWIRP:/bits-prism-tenant/PostProcessMessage.requests"
}

module "GetBadgeTierNotification_availability" {
  source                     = "./availability"
  api_name                   = "GetBadgeTierNotification"
  stage                      = var.stage
  low_watermark              = "0.995"
  requests_metric_name_5xx   = "TWIRP:GetBadgeTierNotification.5xx"
  requests_metric_name_total = "TWIRP:GetBadgeTierNotification.requests"
}

module "UseBitsOnPoll_availability" {
  source                     = "./availability"
  api_name                   = "UseBitsOnPoll"
  stage                      = var.stage
  low_watermark              = "0.995"
  requests_metric_name_5xx   = "TWIRP:UseBitsOnPoll.5xx"
  requests_metric_name_total = "TWIRP:UseBitsOnPoll.requests"
}

module "GetTransactionsByID_availability" {
  source                     = "./availability"
  api_name                   = "GetTransactionsByID"
  stage                      = var.stage
  low_watermark              = "0.995"
  requests_metric_name_5xx   = "TWIRP:GetTransactionsByID.5xx"
  requests_metric_name_total = "TWIRP:GetTransactionsByID.requests"
}

module "ProcessMessage_availability" {
  source                     = "./availability"
  api_name                   = "ProcessMessage"
  stage                      = var.stage
  low_watermark              = "0.995"
  requests_metric_name_5xx   = "TWIRP:/bits-prism-tenant/ProcessMessage.5xx"
  requests_metric_name_total = "TWIRP:/bits-prism-tenant/ProcessMessage.requests"
}

module "SetBadgeTierNotificationState_availability" {
  source                     = "./availability"
  api_name                   = "SetBadgeTierNotificationState"
  stage                      = var.stage
  low_watermark              = "0.995"
  requests_metric_name_5xx   = "TWIRP:SetBadgeTierNotificationState.5xx"
  requests_metric_name_total = "TWIRP:SetBadgeTierNotificationState.requests"
}

module "OffersTenant_availability" {
  source                     = "./availability"
  api_name                   = "OffersTenant"
  stage                      = var.stage
  low_watermark              = "0.995"
  requests_metric_name_5xx   = "POST:stringPattern(\"/twirp/twitch.commerce.OffersTenant/*\").5xx"
  requests_metric_name_total = "POST:stringPattern(\"/twirp/twitch.commerce.OffersTenant/*\").requests"
}

module "AdminGrantUnlockedBadgeTierRewards_availability" {
  source                     = "./availability"
  api_name                   = "AdminGrantUnlockedBadgeTierRewards"
  stage                      = var.stage
  low_watermark              = "0.995"
  requests_metric_name_5xx   = "TWIRP:AdminGrantUnlockedBadgeTierRewards.5xx"
  requests_metric_name_total = "TWIRP:AdminGrantUnlockedBadgeTierRewards.requests"
}

module "UseBitsOnExtension_availability" {
  source                     = "./availability"
  api_name                   = "UseBitsOnExtension"
  stage                      = var.stage
  low_watermark              = "0.995"
  requests_metric_name_5xx   = "TWIRP:UseBitsOnExtension.5xx"
  requests_metric_name_total = "TWIRP:UseBitsOnExtension.requests"
}

module "FinalizeHold_availability" {
  source                     = "./availability"
  api_name                   = "FinalizeHold"
  stage                      = var.stage
  low_watermark              = "0.995"
  requests_metric_name_5xx   = "TWIRP:FinalizeHold.5xx"
  requests_metric_name_total = "TWIRP:FinalizeHold.requests"
}

module "TokenizeAndValidateMessage_availability" {
  source                     = "./availability"
  api_name                   = "TokenizeAndValidateMessage"
  stage                      = var.stage
  low_watermark              = "0.995"
  requests_metric_name_5xx   = "TWIRP:/bits-prism-tenant/TokenizeAndValidateMessage.5xx"
  requests_metric_name_total = "TWIRP:/bits-prism-tenant/TokenizeAndValidateMessage.requests"
}

module "GetUserBitsBadgesAvailableForChannel_availability" {
  source                     = "./availability"
  api_name                   = "GetUserBitsBadgesAvailableForChannel"
  stage                      = var.stage
  low_watermark              = "0.995"
  requests_metric_name_5xx   = "TWIRP:GetUserBitsBadgesAvailableForChannel.5xx"
  requests_metric_name_total = "TWIRP:GetUserBitsBadgesAvailableForChannel.requests"
}

module "GetAutoRefillSettings_availability" {
  source                     = "./availability"
  api_name                   = "GetAutoRefillSettings"
  stage                      = var.stage
  low_watermark              = "0.995"
  requests_metric_name_5xx   = "TWIRP:GetAutoRefillSettings.5xx"
  requests_metric_name_total = "TWIRP:GetAutoRefillSettings.requests"
}

module "IsEligibleToUseBitsOnExtension_availability" {
  source                     = "./availability"
  api_name                   = "IsEligibleToUseBitsOnExtension"
  stage                      = var.stage
  low_watermark              = "0.985"
  traffic_low_watermark      = "100"
  requests_metric_name_5xx   = "TWIRP:IsEligibleToUseBitsOnExtension.5xx"
  requests_metric_name_total = "TWIRP:IsEligibleToUseBitsOnExtension.requests"
}

/**
 * Availability legacy
 */

module "UpdateChannelSettingsVisage_availability" {
  source                     = "./availability"
  api_name                   = "UpdateChannelSettingsVisage"
  stage                      = var.stage
  low_watermark              = "0.995"
  requests_metric_name_5xx   = "UpdateChannelSettingsVisage.5xx"
  requests_metric_name_total = "UpdateChannelSettingsVisage.requests"
}

module "SetBadgeTiersVisage_availability" {
  source                     = "./availability"
  api_name                   = "SetBadgeTiersVisage"
  stage                      = var.stage
  low_watermark              = "0.995"
  requests_metric_name_5xx   = "SetBadgeTiersVisage.5xx"
  requests_metric_name_total = "SetBadgeTiersVisage.requests"
}

module "AuthedEntitleBits_availability" {
  source                     = "./availability"
  api_name                   = "AuthedEntitleBits"
  stage                      = var.stage
  low_watermark              = "0.995"
  requests_metric_name_5xx   = "POST:stringPattern(\"/authed/entitleBits\").5xx"
  requests_metric_name_total = "POST:stringPattern(\"/authed/entitleBits\").requests"
}

module "GetUserInfoVisage_availability" {
  source                     = "./availability"
  api_name                   = "GetUserInfoVisage"
  stage                      = var.stage
  low_watermark              = "0.995"
  requests_metric_name_5xx   = "GetUserInfoVisage.5xx"
  requests_metric_name_total = "GetUserInfoVisage.requests"
}

module "GetProducts_availability" {
  source                     = "./availability"
  api_name                   = "GetProducts"
  stage                      = "./${var.stage}"
  low_watermark              = "0.995"
  requests_metric_name_5xx   = "GetProducts.5xx"
  requests_metric_name_total = "GetProducts.requests"
}

module "GetPricesVisage_availability" {
  source                     = "./availability"
  api_name                   = "GetPricesVisage"
  stage                      = "./${var.stage}"
  low_watermark              = "0.995"
  requests_metric_name_5xx   = "GetPricesVisage.5xx"
  requests_metric_name_total = "GetPricesVisage.requests"
}

module "GetCustomCheermotes_availability" {
  source                     = "./availability"
  api_name                   = "GetCustomCheermotes"
  stage                      = var.stage
  low_watermark              = "0.995"
  requests_metric_name_5xx   = "GetCustomCheermotes.5xx"
  requests_metric_name_total = "GetCustomCheermotes.requests"
}

module "GetChannelSettingsVisage_availability" {
  source                     = "./availability"
  api_name                   = "GetChannelSettingsVisage"
  stage                      = var.stage
  low_watermark              = "0.995"
  requests_metric_name_5xx   = "GetChannelSettingsVisage.5xx"
  requests_metric_name_total = "GetChannelSettingsVisage.requests"
}

module "GetChannelInfoVisage_availability" {
  source                     = "./availability"
  api_name                   = "GetChannelInfoVisage"
  stage                      = var.stage
  low_watermark              = "0.995"
  requests_metric_name_5xx   = "GetChannelInfoVisage.5xx"
  requests_metric_name_total = "GetChannelInfoVisage.requests"
}

module "GetBitsUsageLeaderboard_availability" {
  source                     = "./availability"
  api_name                   = "GetBitsUsageLeaderboard"
  stage                      = var.stage
  low_watermark              = "0.995"
  requests_metric_name_5xx   = "GetBitsUsageLeaderboard.5xx"
  requests_metric_name_total = "GetBitsUsageLeaderboard.requests"
}

module "GetBalanceVisage_availability" {
  source                     = "./availability"
  api_name                   = "GetBalanceVisage"
  stage                      = var.stage
  low_watermark              = "0.995"
  requests_metric_name_5xx   = "GetBalanceVisage.5xx"
  requests_metric_name_total = "GetBalanceVisage.requests"
}

module "GetBadgeTiersVisage_availability" {
  source                     = "./availability"
  api_name                   = "GetBadgeTiersVisage"
  stage                      = var.stage
  low_watermark              = "0.995"
  requests_metric_name_5xx   = "GetBadgeTiersVisage.5xx"
  requests_metric_name_total = "GetBadgeTiersVisage.requests"
}

module "AuthedAdmin_availability" {
  source                     = "./availability"
  api_name                   = "AuthedAdmin"
  stage                      = var.stage
  low_watermark              = "0.995"
  requests_metric_name_5xx   = "GET:stringPattern(\"/authed/admin/*\").5xx"
  requests_metric_name_total = "GET:stringPattern(\"/authed/admin/*\").requests"
}
