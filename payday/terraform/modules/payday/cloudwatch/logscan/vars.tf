variable "stage" {
  description = "Whether it's production, dev, etc"
}

variable "is_canary" {
  description = "Whether the target log group is for canary"
}

variable "log_group" {
  description = "The Cloudwatch log group"
}

variable "log_metric_namespace" {
  description = "Log Metric Namespace"
}