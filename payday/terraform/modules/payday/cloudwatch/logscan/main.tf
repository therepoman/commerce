resource "aws_cloudwatch_log_metric_filter" "error_scan" {
  name           = "payday-error-scan-${var.stage}${var.is_canary ? "-canary" : ""}"
  pattern        = "level=error"
  log_group_name = var.log_group

  metric_transformation {
    name          = "payday-errors-${var.stage}${var.is_canary ? "-canary" : ""}"
    namespace     = var.log_metric_namespace
    value         = "1"
    default_value = "0"
  }
}

resource "aws_cloudwatch_log_metric_filter" "warning_scan" {
  name           = "payday-warning-scan-${var.stage}${var.is_canary ? "-canary" : ""}"
  pattern        = "level=warn"
  log_group_name = var.log_group

  metric_transformation {
    name          = "payday-warns-${var.stage}${var.is_canary ? "-canary" : ""}"
    namespace     = var.log_metric_namespace
    value         = "1"
    default_value = "0"
  }
}

resource "aws_cloudwatch_log_metric_filter" "panic_scan" {
  name           = "payday-panic-scan-${var.stage}${var.is_canary ? "-canary" : ""}"
  pattern        = "\"panic:\""
  log_group_name = var.log_group

  metric_transformation {
    name          = "payday-panics-${var.stage}${var.is_canary ? "-canary" : ""}"
    namespace     = var.log_metric_namespace
    value         = "1"
    default_value = "0"
  }
}
