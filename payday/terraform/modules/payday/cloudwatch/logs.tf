locals {
  log_metric_namespace = "PaydayECSLogs-${var.stage}"
}

module "logscan_filters" {
  source               = "./logscan"
  stage                = var.stage
  log_group            = "payday-${var.stage}"
  is_canary            = false
  log_metric_namespace = local.log_metric_namespace
}

module "logscan_filters_canary" {
  source               = "./logscan"
  stage                = var.stage
  log_group            = "payday-${var.stage}-canary"
  is_canary            = true
  log_metric_namespace = local.log_metric_namespace
}