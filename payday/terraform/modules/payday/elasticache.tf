resource "aws_elasticache_replication_group" "payday_redis" {
  automatic_failover_enabled    = var.elasticache_auto_failover
  replication_group_id          = "cluster-${var.env_prefix}"
  replication_group_description = "payday clustered redis cluster"
  engine                        = "redis"
  engine_version                = "5.0.4"
  node_type                     = var.elasticache_instance_type
  parameter_group_name          = aws_elasticache_parameter_group.payday_redis_params.name
  port                          = var.redis_port
  subnet_group_name             = aws_elasticache_subnet_group.payday_redis_subnet_group.name
  security_group_ids            = [aws_security_group.payday_redis_security_group.id]

  cluster_mode {
    replicas_per_node_group     = var.elasticache_replicas_per_node_group
    num_node_groups             = var.elasticache_num_node_groups
  }
}

resource "aws_elasticache_parameter_group" "payday_redis_params" {
  family = "redis5.0"
  name   = "payday-params-${var.env}"

  parameter {
    name  = "maxmemory-policy"
    value = "allkeys-lru"
  }

  parameter {
    name  = "cluster-enabled"
    value = "yes"
  }
}

resource "aws_elasticache_subnet_group" "payday_redis_subnet_group" {
  name       = "payday-${var.env}-cache"
  subnet_ids = split(",", var.private_subnets)
}

resource "aws_security_group" "payday_redis_security_group" {
  name        = "payday-${var.env}-redis"
  vpc_id      = var.vpc_id
  description = "Allows communication with redis server"

  ingress {
    from_port   = var.redis_port
    protocol    = "tcp"
    to_port     = var.redis_port
    cidr_blocks = var.private_cidr_blocks
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "payday-${var.env}-redis"
  }
}

// Alarm for every node group and every replica
// You'll need to add alarms for each replica if this changes

# Alarms when the cache cpu crosses 80% for 20 minutes
resource "aws_cloudwatch_metric_alarm" "redis_cpu_utilization_replica1-cluster" {
  count = var.elasticache_num_node_groups

  alarm_name          = "RedisClusterShard000${count.index + 1}-001CPUUtilization-${var.env_prefix}"
  alarm_description   = "RedisClusterShard000${count.index + 1}-001CPUUtilization-${var.env_prefix}"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = 20
  metric_name         = "CPUUtilization"
  namespace           = "AWS/ElastiCache"
  period              = 60
  statistic           = "Average"
  threshold           = 80

  dimensions = {
    CacheClusterId = "${aws_elasticache_replication_group.payday_redis.id}-000${count.index + 1}-001"
  }
}

resource "aws_cloudwatch_metric_alarm" "redis_cpu_utilization_replica2-cluser" {
  count = var.elasticache_num_node_groups

  alarm_name          = "RedisClusterShard000${count.index + 1}-002CPUUtilization-${var.env_prefix}"
  alarm_description   = "RedisClusterShard000${count.index + 1}-002CPUUtilization-${var.env_prefix}"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = 20
  metric_name         = "CPUUtilization"
  namespace           = "AWS/ElastiCache"
  period              = 60
  statistic           = "Average"
  threshold           = 80

  dimensions = {
    CacheClusterId = "${aws_elasticache_replication_group.payday_redis.id}-000${count.index + 1}-002"
  }
}

resource "aws_cloudwatch_metric_alarm" "redis_cpu_utilization_replica3-cluser" {
  count = var.elasticache_num_node_groups

  alarm_name          = "RedisClusterShard000${count.index + 1}-003CPUUtilization-${var.env_prefix}"
  alarm_description   = "RedisClusterShard000${count.index + 1}-003CPUUtilization-${var.env_prefix}"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = 20
  metric_name         = "CPUUtilization"
  namespace           = "AWS/ElastiCache"
  period              = 60
  statistic           = "Average"
  threshold           = 80

  dimensions = {
    CacheClusterId = "${aws_elasticache_replication_group.payday_redis.id}-000${count.index + 1}-003"
  }
}

# Alarms when the cache engine cpu crosses 80% for 20 minutes
resource "aws_cloudwatch_metric_alarm" "cache_engine_avg_cpu-replica1-cluser" {
  count = var.elasticache_num_node_groups

  alarm_name          = "RedisClusterShard000${count.index + 1}-001EngineCPUUtilization-${var.env_prefix}"
  alarm_description   = "RedisClusterShard000${count.index + 1}-001EngineCPUUtilization-${var.env_prefix}"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = 20
  metric_name         = "EngineCPUUtilization"
  namespace           = "AWS/ElastiCache"
  period              = 60
  statistic           = "Average"
  threshold           = 80

  dimensions = {
    CacheClusterId = "${aws_elasticache_replication_group.payday_redis.id}-000${count.index + 1}-001"
  }
}

resource "aws_cloudwatch_metric_alarm" "cache_engine_avg_cpu-replica2-cluser" {
  count = var.elasticache_num_node_groups

  alarm_name          = "RedisClusterShard000${count.index + 1}-002EngineCPUUtilization-${var.env_prefix}"
  alarm_description   = "RedisClusterShard000${count.index + 1}-002EngineCPUUtilization-${var.env_prefix}"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = 20
  metric_name         = "EngineCPUUtilization"
  namespace           = "AWS/ElastiCache"
  period              = 60
  statistic           = "Average"
  threshold           = 80

  dimensions = {
    CacheClusterId = "${aws_elasticache_replication_group.payday_redis.id}-000${count.index + 1}-002"
  }
}

resource "aws_cloudwatch_metric_alarm" "cache_engine_avg_cpu-replica3-cluser" {
  count = var.elasticache_num_node_groups

  alarm_name          = "RedisClusterShard000${count.index + 1}-003EngineCPUUtilization-${var.env_prefix}"
  alarm_description   = "RedisClusterShard000${count.index + 1}-003EngineCPUUtilization-${var.env_prefix}"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = 20
  metric_name         = "EngineCPUUtilization"
  namespace           = "AWS/ElastiCache"
  period              = 60
  statistic           = "Average"
  threshold           = 80

  dimensions = {
    CacheClusterId = "${aws_elasticache_replication_group.payday_redis.id}-000${count.index + 1}-003"
  }
}

# Alarms when the bytes used for the cache crosses 40GB for 5 minutes
resource "aws_cloudwatch_metric_alarm" "bytes_used-replica1-cluser" {
  count = var.elasticache_num_node_groups

  alarm_name          = "RedisClusterShard000${count.index + 1}-001BytesUsedForCache-${var.env_prefix}"
  alarm_description   = "RedisClusterShard000${count.index + 1}-001BytesUsedForCache-${var.env_prefix}"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = 5
  metric_name         = "BytesUsedForCache"
  namespace           = "AWS/ElastiCache"
  period              = 60
  statistic           = "Maximum"
  threshold           = 40000000000

  dimensions = {
    CacheClusterId = "${aws_elasticache_replication_group.payday_redis.id}-000${count.index + 1}-001"
  }
}

resource "aws_cloudwatch_metric_alarm" "bytes_used-replica2-cluser" {
  count = var.elasticache_num_node_groups

  alarm_name          = "RedisClusterShard000${count.index + 1}-002BytesUsedForCache-${var.env_prefix}"
  alarm_description   = "RedisClusterShard000${count.index + 1}-002BytesUsedForCache-${var.env_prefix}"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = 5
  metric_name         = "BytesUsedForCache"
  namespace           = "AWS/ElastiCache"
  period              = 60
  statistic           = "Maximum"
  threshold           = 40000000000

  dimensions = {
    CacheClusterId = "${aws_elasticache_replication_group.payday_redis.id}-000${count.index + 1}-002"
  }
}

resource "aws_cloudwatch_metric_alarm" "bytes_used-replica3-cluser" {
  count = var.elasticache_num_node_groups

  alarm_name          = "RedisClusterShard000${count.index + 1}-003BytesUsedForCache-${var.env_prefix}"
  alarm_description   = "RedisClusterShard000${count.index + 1}-003BytesUsedForCache-${var.env_prefix}"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = 5
  metric_name         = "BytesUsedForCache"
  namespace           = "AWS/ElastiCache"
  period              = 60
  statistic           = "Maximum"
  threshold           = 40000000000

  dimensions = {
    CacheClusterId = "${aws_elasticache_replication_group.payday_redis.id}-000${count.index + 1}-003"
  }
}

# Alarms when the number of connections to the cache exceeds 2800 for 5 minutes
resource "aws_cloudwatch_metric_alarm" "avg_conns-replica1-cluser" {
  count = var.elasticache_num_node_groups

  alarm_name          = "RedisClusterShard000${count.index + 1}-001CurrConnections-${var.env_prefix}"
  alarm_description   = "RedisClusterShard000${count.index + 1}-001CurrConnections-${var.env_prefix}"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = 5
  metric_name         = "CurrConnections"
  namespace           = "AWS/ElastiCache"
  period              = 60
  statistic           = "Average"
  threshold           = 4000

  dimensions = {
    CacheClusterId = "${aws_elasticache_replication_group.payday_redis.id}-000${count.index + 1}-001"
  }
}

resource "aws_cloudwatch_metric_alarm" "avg_conns-replica2-cluser" {
  count = var.elasticache_num_node_groups

  alarm_name          = "RedisClusterShard000${count.index + 1}-002CurrConnections-${var.env_prefix}"
  alarm_description   = "RedisClusterShard000${count.index + 1}-002CurrConnections-${var.env_prefix}"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = 5
  metric_name         = "CurrConnections"
  namespace           = "AWS/ElastiCache"
  period              = 60
  statistic           = "Average"
  threshold           = 4000

  dimensions = {
    CacheClusterId = "${aws_elasticache_replication_group.payday_redis.id}-000${count.index + 1}-002"
  }
}

resource "aws_cloudwatch_metric_alarm" "avg_conns-replica3-cluser" {
  count = var.elasticache_num_node_groups

  alarm_name          = "RedisClusterShard000${count.index + 1}-003CurrConnections-${var.env_prefix}"
  alarm_description   = "RedisClusterShard000${count.index + 1}-003CurrConnections-${var.env_prefix}"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = 5
  metric_name         = "CurrConnections"
  namespace           = "AWS/ElastiCache"
  period              = 60
  statistic           = "Average"
  threshold           = 4000

  dimensions = {
    CacheClusterId = "${aws_elasticache_replication_group.payday_redis.id}-000${count.index + 1}-003"
  }
}

# Alarms when the available freeable memory drops below 1 million bytes over 5 minutes
resource "aws_cloudwatch_metric_alarm" "avg_freeable_memory-replica1-cluser" {
  count = var.elasticache_num_node_groups

  alarm_name          = "RedisClusterShard000${count.index + 1}-001FreeableMemory-${var.env_prefix}"
  alarm_description   = "RedisClusterShard000${count.index + 1}-001FreeableMemory-${var.env_prefix}"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods  = 5
  metric_name         = "FreeableMemory"
  namespace           = "AWS/ElastiCache"
  period              = 60
  statistic           = "Average"
  threshold           = 1000000

  dimensions = {
    CacheClusterId = "${aws_elasticache_replication_group.payday_redis.id}-000${count.index + 1}-001"
  }
}

resource "aws_cloudwatch_metric_alarm" "avg_freeable_memory-replica2-cluser" {
  count = var.elasticache_num_node_groups

  alarm_name          = "RedisClusterShard000${count.index + 1}-002FreeableMemory-${var.env_prefix}"
  alarm_description   = "RedisClusterShard000${count.index + 1}-002FreeableMemory-${var.env_prefix}"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods  = 5
  metric_name         = "FreeableMemory"
  namespace           = "AWS/ElastiCache"
  period              = 60
  statistic           = "Average"
  threshold           = 1000000

  dimensions = {
    CacheClusterId = "${aws_elasticache_replication_group.payday_redis.id}-000${count.index + 1}-002"
  }
}

resource "aws_cloudwatch_metric_alarm" "avg_freeable_memory-replica3-cluser" {
  count = var.elasticache_num_node_groups

  alarm_name          = "RedisClusterShard000${count.index + 1}-003FreeableMemory-${var.env_prefix}"
  alarm_description   = "RedisClusterShard000${count.index + 1}-003FreeableMemory-${var.env_prefix}"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods  = 5
  metric_name         = "FreeableMemory"
  namespace           = "AWS/ElastiCache"
  period              = 60
  statistic           = "Average"
  threshold           = 1000000

  dimensions = {
    CacheClusterId = "${aws_elasticache_replication_group.payday_redis.id}-000${count.index + 1}-003"
  }
}