locals {
  ip_addresses = [
    "10.192.69.171",
    "10.192.75.166",
    "10.192.65.79",
    "10.192.67.85"
  ]
}

resource "aws_route53_resolver_rule" "fwd_direct_connect" {
  domain_name          = var.dns
  name                 = "fwd_${var.name}_${var.env}_dx"
  rule_type            = "FORWARD"
  resolver_endpoint_id = var.outbound_resolver_id

  target_ip {
    ip = element(local.ip_addresses, 0)
  }

  target_ip {
    ip = element(local.ip_addresses, 1)
  }

  target_ip {
    ip = element(local.ip_addresses, 2)
  }

  target_ip {
    ip = element(local.ip_addresses, 3)
  }

  tags = {
    Environment = var.env
  }
}

resource "aws_route53_resolver_rule_association" "fwd_direct_connect" {
  resolver_rule_id = aws_route53_resolver_rule.fwd_direct_connect.id
  vpc_id           = var.vpc_id
}