variable "name" {
  description = "the name to associate within the rule"
}

variable "dns" {
  description = "the DNS you want to forward to Direct Connect"
}

variable "vpc_id" {
  description = "your VPC ID"
}

variable "env" {
  description = "the environment identifier"
}

variable "outbound_resolver_id" {
  description = "an outbound resolver ID from Route53 resolver."
}