module "cluster" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//cluster?ref=7a97551c37b059b469c6734f3d64771a2d5af6ba"

  name           = "payday-${var.stage}-cluster"
  min            = var.ecs_ec2_backed_min_instances
  vpc_id         = var.vpc_id
  instance_type  = "c5n.4xlarge"
  environment    = var.stage
  subnets        = var.private_subnets
  security_group = aws_security_group.twitch_subnets.id
}

//adds a lifecycle hook to an already existing Autoscaling Group.
resource "aws_autoscaling_lifecycle_hook" "on_instance_terminate_lifecycle_hook" {
  autoscaling_group_name  = module.cluster.ecs_cluster_asg_name
  lifecycle_transition    = "autoscaling:EC2_INSTANCE_TERMINATING"
  name                    = "on_instance_terminate_upgrade_complete_hook"
  notification_target_arn = var.ecs_upgrade_sns_topic
  role_arn                = aws_iam_role.lifecycle_hook_sns_write_role.arn
  heartbeat_timeout       = 3600
}

//allows the Lifecycle Hook to publish messages to SNS
resource "aws_iam_role" "lifecycle_hook_sns_write_role" {
  name = "lifecycle-hook-sns-write-role-${var.env}"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "autoscaling.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "autoscaling_notification_sns_write_policy" {
  role       = aws_iam_role.lifecycle_hook_sns_write_role.id
  policy_arn = "arn:aws:iam::aws:policy/service-role/AutoScalingNotificationAccessRole"
}
