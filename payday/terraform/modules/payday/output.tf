output "bits_usage_entitlements_sfn_arn" {
  value = aws_sfn_state_machine.bits_usage_entitlements.id
}

output "user_destroy_sfn_arn" {
  value = aws_sfn_state_machine.handle_eventbus_user_destroy.id
}

output "user_destroy_dlq_name" {
  value = module.eventbus_user_destroy_queue.dlq_queue_name
}