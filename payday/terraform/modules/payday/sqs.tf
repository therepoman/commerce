module "payday_emoticon_backfill_queue" {
  source                    = "./sqs_queue_for_payday_sns"
  queue_name                = "emoticon_backfill"
  sns_topic_to_subscribe_to = module.bits_emoticon_backfill.topic_arn
  alarm_dlq                 = var.alarm_dlq
  queue_name_suffix         = var.queue_name_suffix
  iam_role_name             = var.iam_role_name
  env_prefix                = var.env_prefix
}

module "payday_bter_user_moderation_queue" {
  source                    = "./sqs_queue_for_payday_sns"
  queue_name                = "bter_usermoderation"
  sns_topic_to_subscribe_to = var.user_moderation_sns_arn
  alarm_dlq                 = var.alarm_dlq
  queue_name_suffix         = var.queue_name_suffix
  iam_role_name             = var.iam_role_name
  env_prefix                = var.env_prefix
}

module "pagle_condition_completion_queue" {
  source                    = "./sqs_queue"
  queue_name                = "pagle_condition_completion"
  sns_topic_to_subscribe_to = var.pagle_completion_sns_arn
  alarm_dlq                 = var.alarm_dlq
  queue_name_suffix         = var.queue_name_suffix
  iam_role_name             = var.iam_role_name
  env_prefix                = var.env_prefix
}

module "pachter_ingestion_queue" {
  source                    = "./sqs_queue"
  queue_name                = "pachter_ingestion"
  sns_topic_to_subscribe_to = var.pachinko_bits_update_sns_arn
  alarm_dlq                 = var.alarm_dlq
  queue_name_suffix         = var.queue_name_suffix
  iam_role_name             = var.iam_role_name
  env_prefix                = var.env_prefix
  delivery_delay            = "5"
}

module "eventbus_user_destroy_queue" {
  source         = "../eventbus"
  env            = var.stage
  eventbus_event = "user_destroy"
}


resource "aws_sqs_queue" "channels_glue_job_dbexport_error" {
  name = "${var.env_prefix}-channels-glue-job-dbexport-error"
}

resource "aws_sns_topic_subscription" "channels_glue_job_dbexport_error_subscription" {
  topic_arn = module.payday_dynamo_tables.channels_db_export_glue_errors_arn
  protocol  = "sqs"
  endpoint  = aws_sqs_queue.channels_glue_job_dbexport_error.arn
}

resource "aws_sqs_queue_policy" "channels_glue_job_dbexport_error_queue_policy" {
  queue_url = aws_sqs_queue.channels_glue_job_dbexport_error.id
  policy    = data.aws_iam_policy_document.channels_glue_job_dbexport_error_queue_policy_doc.json
}

data "aws_iam_policy_document" "channels_glue_job_dbexport_error_queue_policy_doc" {
  statement {
    sid = "${aws_sqs_queue.channels_glue_job_dbexport_error.name}-Sid"

    actions = ["SQS:SendMessage"]

    principals {
      identifiers = ["*"]
      type        = "AWS"
    }

    resources = [aws_sqs_queue.channels_glue_job_dbexport_error.arn]

    condition {
      test     = "ArnEquals"
      variable = "aws:SourceArn"

      values = [
        module.payday_dynamo_tables.channels_db_export_glue_errors_arn
      ]
    }
  }
}
