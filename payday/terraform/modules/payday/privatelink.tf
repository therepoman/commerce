locals {
  // this is already created, and I can't import it as data in terraform
  outbound_resolver_id = "rslvr-out-b2822afd828a491f9"

  security_group = {
    staging    = "sg-0f4b2195ae78b5607" // twitch_subnets_staging
    production = "sg-0ddddcad182ea3fa0" // twitch_subnets_production
  }

  pantheon-vpce = {
    staging    = "com.amazonaws.vpce.us-west-2.vpce-svc-0e024e12c03495b3e"
    production = "com.amazonaws.vpce.us-west-2.vpce-svc-03caeabab7289eff3"
  }

  pagle-vpce = {
    staging    = "com.amazonaws.vpce.us-west-2.vpce-svc-02e9ac77309ea32ce"
    production = "com.amazonaws.vpce.us-west-2.vpce-svc-063dc08efb792ed18"
  }

  prometheus-vpce = {
    staging    = "com.amazonaws.vpce.us-west-2.vpce-svc-034e0f295eb95d20f"
    production = "com.amazonaws.vpce.us-west-2.vpce-svc-0624fd6582168338a"
  }

  ems-vpce = {
    staging    = "com.amazonaws.vpce.us-west-2.vpce-svc-094a32bb0c30b9833"
    production = "com.amazonaws.vpce.us-west-2.vpce-svc-065d9a5722b589fa5"
  }

  extension-validator-vpce = {
    staging    = "com.amazonaws.vpce.us-west-2.vpce-svc-0bda7390fcc6cf65b"
    production = "com.amazonaws.vpce.us-west-2.vpce-svc-0797f09cd5e86bcdb"
  }

  extension-service-env = {
    staging    = "staging"
    production = "prod"
  }

  dartreceiver-vpce = {
    staging    = "com.amazonaws.vpce.us-west-2.vpce-svc-00cc5f5ddd42126a0"
    production = "com.amazonaws.vpce.us-west-2.vpce-svc-0ce6ec756b1a5acba"
  }

  owl-vpce = {
    staging    = "com.amazonaws.vpce.us-west-2.vpce-svc-02a7897a39672d1a5"
    production = "com.amazonaws.vpce.us-west-2.vpce-svc-08fdcf80669bbcb87"
  }

  owl-dns = {
    staging    = "staging.owl-dev.twitch.a2z.com"
    production = "prod.owl.twitch.a2z.com"
  }

  falador-vpce = {
    staging    = "com.amazonaws.vpce.us-west-2.vpce-svc-0bf8fde2e1bda83c5"
    production = "com.amazonaws.vpce.us-west-2.vpce-svc-0d2d2de77d60634c0"
  }

  env = {
    production = "prod"
    staging    = "beta"
  }
}

module "privatelink-pantheon" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//privatelink-service?ref=7a97551c37b059b469c6734f3d64771a2d5af6ba"

  name            = "pantheon"
  endpoint        = local.pantheon-vpce[var.stage]
  environment     = var.stage
  region          = var.aws_region
  security_groups = [local.security_group[var.stage]]
  subnets         = split(",", var.private_subnets)
  vpc_id          = var.vpc_id
}

module "privatelink-pagle" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//privatelink-service?ref=7a97551c37b059b469c6734f3d64771a2d5af6ba"

  name            = "pagle"
  endpoint        = local.pagle-vpce[var.stage]
  environment     = var.stage
  region          = var.aws_region
  security_groups = [local.security_group[var.stage]]
  subnets         = split(",", var.private_subnets)
  vpc_id          = var.vpc_id
}

module "privatelink-prometheus" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//privatelink-service?ref=7a97551c37b059b469c6734f3d64771a2d5af6ba"

  name            = "prometheus"
  endpoint        = local.prometheus-vpce[var.stage]
  environment     = var.stage
  region          = var.aws_region
  security_groups = [local.security_group[var.stage]]
  subnets         = split(",", var.private_subnets)
  vpc_id          = var.vpc_id
}

module "dartreceiver-vpce-privatelink" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//vpc-endpoint?ref=7a97551c37b059b469c6734f3d64771a2d5af6ba"

  name            = "dartreceiver"
  dns             = "us-west-2.${local.env[var.stage]}.twitchdartreceiver.s.twitch.a2z.com"
  endpoint        = local.dartreceiver-vpce[var.stage]
  security_groups = [local.security_group[var.stage]]
  subnets         = split(",", var.private_subnets)
  vpc_id          = var.vpc_id
}

module "ems-privatelink" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//vpc-endpoint?ref=7a97551c37b059b469c6734f3d64771a2d5af6ba"

  name            = "ems"
  dns             = "${local.extension-service-env[var.stage]}.ems.extensions.twitch.a2z.com"
  endpoint        = local.ems-vpce[var.stage]
  security_groups = [local.security_group[var.stage]]
  subnets         = split(",", var.private_subnets)
  vpc_id          = var.vpc_id
}

module "extension-validator-privatelink" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//vpc-endpoint?ref=7a97551c37b059b469c6734f3d64771a2d5af6ba"

  name            = "extension-validator"
  dns             = "${local.extension-service-env[var.stage]}.validator.extensions.twitch.a2z.com"
  endpoint        = local.extension-validator-vpce[var.stage]
  security_groups = [local.security_group[var.stage]]
  subnets         = split(",", var.private_subnets)
  vpc_id          = var.vpc_id
}

module "clips-privatelink" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//vpc-endpoint?ref=7a97551c37b059b469c6734f3d64771a2d5af6ba"
  count  = var.stage == "staging" ? 0 : 1

  name            = "clips"
  dns             = "production.clips.twitch.a2z.com"
  endpoint        = "com.amazonaws.vpce.us-west-2.vpce-svc-04e8d94a8f6fc80ad"
  security_groups = [local.security_group[var.stage]]
  subnets         = split(",", var.private_subnets)
  vpc_id          = var.vpc_id
}

module "owl-privatelink" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//vpc-endpoint?ref=7a97551c37b059b469c6734f3d64771a2d5af6ba"

  name            = "owl"
  dns             = local.owl-dns[var.stage]
  endpoint        = local.owl-vpce[var.stage]
  security_groups = [local.security_group[var.stage]]
  subnets         = split(",", var.private_subnets)
  vpc_id          = var.vpc_id
}

module "falador-privatelink" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//vpc-endpoint?ref=7a97551c37b059b469c6734f3d64771a2d5af6ba"

  name            = "falador"
  dns             = "us-west-2.${local.env[var.stage]}.twitchextcatalogservice.s.twitch.a2z.com"
  endpoint        = local.falador-vpce[var.stage]
  security_groups = [local.security_group[var.stage]]
  subnets         = split(",", var.private_subnets)
  vpc_id          = var.vpc_id
}
