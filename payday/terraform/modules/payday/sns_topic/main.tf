variable "topic-name" {
  type = string
}

variable "sns-topic-name-suffix" {
  type = string
  default = ""
  description = "a way to append prod or staging to the end of sns topics"
}

resource "aws_sns_topic" "sns_topic" {
  name = "${var.topic-name}-event-${var.sns-topic-name-suffix}"
}

data "aws_iam_policy_document" "sns_policy_document" {
  statement {
    sid = "__default_statement_ID"
    actions = [
      "SNS:Publish",
      "SNS:RemovePermission",
      "SNS:SetTopicAttributes",
      "SNS:DeleteTopic",
      "SNS:ListSubscriptionsByTopic",
      "SNS:GetTopicAttributes",
      "SNS:Receive",
      "SNS:AddPermission",
      "SNS:Subscribe"
    ]
    effect = "Allow"
    principals {
      identifiers = ["*"]
      type = "AWS"
    }
    resources = [aws_sns_topic.sns_topic.arn]
    condition {
      test = "StringEquals"
      values = ["021561903526"]
      variable = "AWS:SourceOwner"
    }
  }
}

resource "aws_sns_topic_policy" "sns_policy" {
  arn = aws_sns_topic.sns_topic.arn
  policy = data.aws_iam_policy_document.sns_policy_document.json
}

output "topic_arn" {
  value = aws_sns_topic.sns_topic.arn
}

output "topic-name" {
  value = aws_sns_topic.sns_topic.name
}