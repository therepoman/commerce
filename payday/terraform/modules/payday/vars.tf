variable "private_cidr_blocks" {
  description = "The private subnet cidr blocks for this stage"
  type        = list(string)
}

variable "aws_region" {
  description = "AWS region to operate in."
  default     = "us-west-2"
}

variable "service" {
  description = "Name of the service being built. Should probably leave this as the default"
  default     = "commerce/payday"
}

variable "env" {
  description = "The name of the beanstalmk environment"
}

variable "stage" {
  description = "Whether it's production, dev, etc"
}

variable "account_name" {
  description = "Name of the AWS account"
}

variable "account_id" {
  description = "ID of the AWS account"
}

variable "owner" {
  description = "Owner of the account"
}

# VPC Config
variable "vpc_id" {
  description = "Id of the systems-created VPC"
}

variable "private_subnets" {
  description = "Comma-separated list of private subnets"
}

variable "iam_role_name" {
  description = "The iam role attached used by the beanstalk application"
}

variable "min_read_capacity" {
  description = "Minimum dynamodb read capacity"
}

variable "min_write_capacity" {
  description = "Minimum dynamodb write capacity"
}

variable "max_read_capacity" {
  description = "Maximum dynamodb read capacity"
}

variable "max_write_capacity" {
  description = "Maximum dynamodb write capacity"
}

variable "env_prefix" {
  description = "Prefix to represent this environment"
}

variable "elasticache_instance_type" {
  description = "Describes the type of instance to be used in our elasticache cluster"
  default     = "cache.r5.large"
}

variable "elasticache_auto_failover" {
  description = "Describes the type of failover behavior for the elasticache groups"
  default     = true
}

variable "redis_port" {
  description = "The port used by the redis server"
  default     = 6379
}

variable "elasticache_replicas_per_node_group" {
  description = "Number of read replicas per elasticache node group"
}

variable "elasticache_num_node_groups" {
  type        = number
  description = "Number of elasticache node groups"
}

## ECS Service

variable "ecs_healthcheck" {
  description = "Name of service"
}

variable "ecs_logging" {
  default     = "awslogs"
  description = "The logging driver to use (e.g., json, awslogs)"
}

variable "ecs_cpu" {
  default     = 256
  description = "How many CPU units to use"
}

variable "cpu_scale_up_threshold" {
  default     = 60
  description = "CPU Utilization threshold for scaling up"
}

variable "ecs_memory" {
  default = 256
}

variable "ecs_ec2_backed_min_instances" {
  default = 0
}

variable "ecs_min_instances" {
  default = 0
}

variable "ecs_max_instances" {
  default = 0
}

variable "ecs_alb_port" {
  default     = 80
  description = "The port the load balancer is accepting traffic from."
}

variable "ecs_env_vars" {
  type = list(object({
    name  = string
    value = string
  }))
  default = []
}

variable "sandstorm_role" {
  description = "Sandstorm role used to retrieve secrets"
}

variable "alarm_dlq" {
  description = "a boolean for whether or not to create an alarm for the dlq"
}

variable "queue_name_suffix" {
  type        = string
  default     = ""
  description = "a way to append _staging to the end of the dlqs in staging"
}

variable "dns" {
  type        = string
  description = "DNS for ECS cluster"
}

variable "secondary_dns" {
  type        = string
  default     = ""
  description = "Secondary DNS for ECS cluster (For migration)"
}

variable "event_bus_iam_arn" {
  type = string
}

variable "user_moderation_sns_arn" {
  type        = string
  description = "The ARN for the usermoderation SNS"
}

variable "pagle_completion_sns_arn" {
  type        = string
  description = "The ARN for pagle condition completion SNS"
}

variable "pachinko_bits_update_sns_arn" {
  type        = string
  description = "The ARN for the Bits balance update SNS in Pachinko"
}

variable "auto_refill_table_account_id" {
  type        = string
  description = "account ID where auto refill table lives"
}

variable "alb_access_logs_expiration_days" {
  default     = 7
  description = "Application Load Balancer Access Log Retention Period, defaults to 7 days"
}

variable "audit_logs_retention_in_days" {
  default     = 7
  description = "Access Audit Log Retention Period, defaults to 7 days"
}

variable "s2s_auth_logs_retention_in_days" {
  default     = 7
  description = "S2S Auth Log Retention Period, defaults to 7 days"
}

variable "ecs_upgrade_sns_topic" {
  description = "ARN for the ECS upgrade lambda"
}
