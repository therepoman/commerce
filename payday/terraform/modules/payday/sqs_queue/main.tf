variable "queue_name" {
  type = string
}

variable "receive_wait_time" {
  type    = string
  default = 20
}

variable "message_retention_seconds" {
  type    = string
  default = 1209600
}

variable "max_receive_count" {
  type    = string
  default = 7
}

variable "delivery_delay" {
  type    = string
  default = 0
}

variable "sns_topic_to_subscribe_to" {
  type        = string
  description = "an arn of an sns topic to subscribe this queue to"
}

variable "alarm_dlq" {
  default     = false
  description = "flag to determine if we should be making a paging alarm for the DLQ"
}

variable "queue_name_suffix" {
  type        = string
  default     = ""
  description = "a way to append _staging to the end of the dlqs in staging"
}

variable "iam_role_name" {
  type        = string
  default     = ""
  description = "The iam role attached used by the beanstalk application"
}

variable "env_prefix" {
  type        = string
  default     = ""
  description = "Prefix to represent this environment"
}

resource "aws_sqs_queue_policy" "queue_policy" {
  queue_url = aws_sqs_queue.sqs_queue.id
  policy    = data.aws_iam_policy_document.default_queue_policy_doc.json
}

data "aws_iam_policy_document" "default_queue_policy_doc" {
  statement {
    sid = "PaydayQueueSid"

    actions = [
      "SQS:SendMessage",
    ]

    principals {
      identifiers = [
        "*",
      ]

      type = "AWS"
    }

    resources = [
      aws_sqs_queue.sqs_queue.arn,
    ]

    condition {
      test     = "ArnEquals"
      variable = "aws:SourceArn"

      values = [var.sns_topic_to_subscribe_to]
    }
  }
}

resource "aws_sqs_queue" "sqs_queue_deadletter" {
  message_retention_seconds = var.message_retention_seconds
  name                      = "${var.queue_name}_deadletter${var.queue_name_suffix}"
}

resource "aws_sqs_queue" "sqs_queue" {
  message_retention_seconds = var.message_retention_seconds
  name                      = "${var.queue_name}${var.queue_name_suffix}"
  receive_wait_time_seconds = var.receive_wait_time
  delay_seconds             = var.delivery_delay

  redrive_policy = <<EOF
{
  "deadLetterTargetArn": "${aws_sqs_queue.sqs_queue_deadletter.arn}",
  "maxReceiveCount": ${var.max_receive_count}
}
EOF
}

resource "aws_cloudwatch_metric_alarm" "dlq_not_empty" {
  count               = var.alarm_dlq ? 1 : 0
  alarm_name          = "${var.queue_name}_deadletter_not_empty"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "ApproximateNumberOfMessagesVisible"
  namespace           = "AWS/SQS"

  dimensions = {
    QueueName = "${var.queue_name}_deadletter${var.queue_name_suffix}"
  }

  period             = "300"
  statistic          = "Sum"
  threshold          = "1"
  alarm_description  = "This metric monitors one of payday's dead letter queues. It will alarm if there are too many messages in that queue."
  treat_missing_data = "notBreaching"
}

resource "aws_sns_topic_subscription" "payday_queue_subscription" {
  topic_arn = var.sns_topic_to_subscribe_to
  protocol  = "sqs"
  endpoint  = aws_sqs_queue.sqs_queue.arn
}

output "queue_arn" {
  value = aws_sqs_queue.sqs_queue.arn
}

resource "aws_iam_policy_attachment" "payday_sqs_read_only" {
  name       = "payday_sqs_readonly"
  policy_arn = aws_iam_policy.sqs_read_only.arn

  roles = [
    var.iam_role_name,
  ]
}

data "aws_iam_policy_document" "sqs_readonly_prod_policy" {
  statement {
    actions = [
      "sqs:GetQueueAttributes",
      "sqs:ListQueues",
      "sqs:ReceiveMessage",
      "sqs:DeleteMessage",
    ]

    resources = [
      aws_sqs_queue.sqs_queue.arn,
    ]
  }
}

resource "aws_iam_policy" "sqs_read_only" {
  name        = "sqs_read_only_${var.queue_name}_${var.env_prefix}"
  description = "readonly access to all SQS queues"
  policy      = data.aws_iam_policy_document.sqs_readonly_prod_policy.json
}
