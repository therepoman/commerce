variable "leaderboard_badge_holders_read_capacity_prod" {
  type = string
}

variable "leaderboard_badge_holders_write_capacity_prod" {
  type = string
}

locals {
  leaderboard_badge_holders_read_capacity  = var.stage == "staging" ? var.min_read_capacity : var.leaderboard_badge_holders_read_capacity_prod
  leaderboard_badge_holders_write_capacity = var.stage == "staging" ? var.min_write_capacity : var.leaderboard_badge_holders_write_capacity_prod
}

resource "aws_dynamodb_table" "leaderboard_badge_holders" {
  name             = "${var.prefix}_leaderboard_badge_holders"
  hash_key         = "channelID"
  read_capacity    = local.leaderboard_badge_holders_read_capacity
  write_capacity   = local.leaderboard_badge_holders_write_capacity
  stream_enabled   = "true"
  stream_view_type = "NEW_AND_OLD_IMAGES"

  attribute {
    name = "channelID"
    type = "S"
  }

  attribute {
    name = "firstPlaceUserID"
    type = "S"
  }

  attribute {
    name = "secondPlaceUserID"
    type = "S"
  }

  attribute {
    name = "thirdPlaceUserID"
    type = "S"
  }

  lifecycle {
    ignore_changes = [read_capacity, write_capacity]
  }

  global_secondary_index {
    name            = "leaderboard_badge_holders_firstPlaceUserID-index"
    hash_key        = "firstPlaceUserID"
    projection_type = "ALL"
    read_capacity   = local.leaderboard_badge_holders_read_capacity
    write_capacity  = local.leaderboard_badge_holders_write_capacity
  }

  global_secondary_index {
    name            = "leaderboard_badge_holders_secondPlaceUserID-index"
    hash_key        = "secondPlaceUserID"
    projection_type = "ALL"
    read_capacity   = local.leaderboard_badge_holders_read_capacity
    write_capacity  = local.leaderboard_badge_holders_write_capacity
  }

  global_secondary_index {
    name            = "leaderboard_badge_holders_thirdPlaceUserID-index"
    hash_key        = "thirdPlaceUserID"
    projection_type = "ALL"
    read_capacity   = local.leaderboard_badge_holders_read_capacity
    write_capacity  = local.leaderboard_badge_holders_write_capacity
  }
}

module "leaderboard_badge_holders_firstPlaceUserID-gsi_autoscaling" {
  source                   = "../../dynamo_db_index_autoscaling_policy"
  table_name               = aws_dynamodb_table.leaderboard_badge_holders.name
  min_index_read_capacity  = local.leaderboard_badge_holders_read_capacity
  min_index_write_capacity = local.leaderboard_badge_holders_write_capacity
  max_index_read_capacity  = var.max_read_capacity
  max_index_write_capacity = var.max_write_capacity
  index_name               = "leaderboard_badge_holders_firstPlaceUserID-index"
}

module "leaderboard_badge_holders_secondPlaceUserID-gsi_autoscaling" {
  source                   = "../../dynamo_db_index_autoscaling_policy"
  table_name               = aws_dynamodb_table.leaderboard_badge_holders.name
  min_index_read_capacity  = local.leaderboard_badge_holders_read_capacity
  min_index_write_capacity = local.leaderboard_badge_holders_write_capacity
  max_index_read_capacity  = var.max_read_capacity
  max_index_write_capacity = var.max_write_capacity
  index_name               = "leaderboard_badge_holders_secondPlaceUserID-index"
}

module "leaderboard_badge_holders_thirdPlaceUserID-gsi_autoscaling" {
  source                   = "../../dynamo_db_index_autoscaling_policy"
  table_name               = aws_dynamodb_table.leaderboard_badge_holders.name
  min_index_read_capacity  = local.leaderboard_badge_holders_read_capacity
  min_index_write_capacity = local.leaderboard_badge_holders_write_capacity
  max_index_read_capacity  = var.max_read_capacity
  max_index_write_capacity = var.max_write_capacity
  index_name               = "leaderboard_badge_holders_thirdPlaceUserID-index"
}