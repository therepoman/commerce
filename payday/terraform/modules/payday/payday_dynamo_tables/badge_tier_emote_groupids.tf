variable "badge_tier_emote_groupids_min_read_capacity_prod" {
  type    = string
  default = "100"
}

locals {
  badge_tier_emote_groupids_min_read_capacity = var.stage == "production" ? var.badge_tier_emote_groupids_min_read_capacity_prod : var.min_read_capacity
}

resource "aws_dynamodb_table" "badge_tier_emote_groupids_table" {
  name           = "${var.prefix}_badge_tier_emote_groupids"
  hash_key       = "channel_id"
  range_key      = "threshold"
  read_capacity  = local.badge_tier_emote_groupids_min_read_capacity
  write_capacity = var.min_write_capacity
  stream_enabled = "true"
  stream_view_type = "NEW_AND_OLD_IMAGES"

  attribute {
    name = "channel_id"
    type = "S"
  }
  attribute {
    name = "threshold"
    type = "N"
  }
  attribute {
    name = "group_id"
    type = "S"
  }

  global_secondary_index {
    hash_key = "group_id"
    name = "group_id-index"
    projection_type = "ALL"
    read_capacity   = local.badge_tier_emote_groupids_min_read_capacity
    write_capacity  = var.min_write_capacity
  }

  lifecycle {
    ignore_changes = [read_capacity, write_capacity]
  }
}

module "badge_tier_emote_groupids_table_autoscaling" {
  source             = "../../dynamo_db_autoscaling_policy"
  table_name         = aws_dynamodb_table.badge_tier_emote_groupids_table.name
  min_read_capacity  = local.badge_tier_emote_groupids_min_read_capacity
  min_write_capacity = var.min_write_capacity
  max_read_capacity  = var.max_read_capacity
  max_write_capacity = var.max_write_capacity
}

module "badge_tier_emote_groupids_scheduled_backups" {
  source                   = "../../scheduled_dynamo_backup_event"
  table_name               = aws_dynamodb_table.badge_tier_emote_groupids_table.name
  dynamo_backup_lambda_arn = var.dynamo_backup_lambda_arn
}
