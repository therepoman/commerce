variable "prefix" {
  description = "prefix at the beginning of dynamo table names"
}

variable "stage" {
  description = "Current payday environment stage. i.e. staging, production"
}

variable "min_read_capacity" {
  description = "Minimum dynamodb read capacity"
}

variable "min_write_capacity" {
  description = "Minimum dynamodb write capacity"
}

variable "max_read_capacity" {
  description = "Maximum dynamodb read capacity"
}

variable "max_write_capacity" {
  description = "Maximum dynamodb write capacity"
}

variable "dynamo_backup_lambda_arn" {
  description = "ARN for lambda function that performs dynamo backups"
}

variable "account_id" {
  description = "ID of the AWS account"
}

variable "tahoe_api_password_name" {
  description = "Password for the tahoe api"
}

variable "tahoe_api_tagert_key_id" {
  description = "Target key id for the tahoe api"
}
