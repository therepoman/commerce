variable "in_flight_transactions_read_capacity_prod" {
  type = string
}

locals {
  in_flight_transactions_read_capacity = var.stage == "staging" ? var.min_read_capacity : var.in_flight_transactions_read_capacity_prod
}

resource "aws_dynamodb_table" "in_flight_transactions_table" {
  name           = "${var.prefix}_in_flight_transactions"
  hash_key       = "transaction_id"
  read_capacity  = local.in_flight_transactions_read_capacity
  write_capacity = var.min_write_capacity
  stream_enabled = "true"
  stream_view_type = "NEW_AND_OLD_IMAGES"

  attribute {
    name = "transaction_id"
    type = "S"
  }

  lifecycle {
    ignore_changes = [read_capacity, write_capacity]
  }
}

module "in_flight_transactions_table_autoscaling" {
  source             = "../../dynamo_db_autoscaling_policy"
  table_name         = "${var.prefix}_in_flight_transactions"
  min_read_capacity  = local.in_flight_transactions_read_capacity
  min_write_capacity = var.min_write_capacity
  max_read_capacity  = var.max_read_capacity
  max_write_capacity = var.max_write_capacity
}

module "in_flight_transactions_scheduled_backups" {
  source                   = "../../scheduled_dynamo_backup_event"
  table_name               = "${var.prefix}_in_flight_transactions"
  dynamo_backup_lambda_arn = var.dynamo_backup_lambda_arn
}
