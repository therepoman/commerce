variable "balance_change_processing_min_read_capacity_prod" {
  type    = string
  default = "200"
}

variable "balance_change_processing_min_write_capacity_prod" {
  type    = string
  default = "200"
}

locals {
  balance_change_processing_min_read_capacity = var.stage == "production" ? var.balance_change_processing_min_read_capacity_prod : var.min_read_capacity
  balance_change_processing_min_write_capacity = var.stage == "production" ? var.balance_change_processing_min_write_capacity_prod : var.min_write_capacity
}

resource "aws_dynamodb_table" "balance_change_processing_table" {
  name           = "${var.prefix}_balance_change_processing"
  hash_key       = "transaction_id.step_identifier"
  read_capacity  = local.balance_change_processing_min_read_capacity
  write_capacity = local.balance_change_processing_min_write_capacity
  stream_enabled = "true"
  stream_view_type = "NEW_AND_OLD_IMAGES"

  attribute {
    name = "transaction_id.step_identifier"
    type = "S"
  }

  lifecycle {
    ignore_changes = [read_capacity, write_capacity]
  }
}

module "balance_change_processing_table_autoscaling" {
  source             = "../../dynamo_db_autoscaling_policy"
  table_name         = aws_dynamodb_table.balance_change_processing_table.name
  min_read_capacity  = local.balance_change_processing_min_read_capacity
  min_write_capacity = var.min_write_capacity
  max_read_capacity  = var.max_read_capacity
  max_write_capacity = var.max_write_capacity
}

module "balance_change_processing_scheduled_backups" {
  source                   = "../../scheduled_dynamo_backup_event"
  table_name               = aws_dynamodb_table.balance_change_processing_table.name
  dynamo_backup_lambda_arn = var.dynamo_backup_lambda_arn
}
