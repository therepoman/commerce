resource "aws_dynamodb_table" "participants_table" {
  name     = "${var.prefix}_participants"
  hash_key = "challenge_id-user_id"

  read_capacity    = var.min_read_capacity
  write_capacity   = var.min_write_capacity
  stream_enabled   = "true"
  stream_view_type = "NEW_AND_OLD_IMAGES"

  attribute {
    name = "challenge_id-user_id"
    type = "S"
  }

  attribute {
    name = "user_id"
    type = "S"
  }

  attribute {
    name = "last_contribution_time"
    type = "S"
  }

  global_secondary_index {
    name            = "user_id-last_contribution_time-index"
    hash_key        = "user_id"
    range_key       = "last_contribution_time"
    write_capacity  = var.min_read_capacity
    read_capacity   = var.min_write_capacity
    projection_type = "ALL"
  }
}

module "participants_table_autoscaling" {
  source             = "../../dynamo_db_autoscaling_policy"
  table_name         = aws_dynamodb_table.participants_table.name
  min_read_capacity  = var.min_read_capacity
  min_write_capacity = var.min_write_capacity
  max_read_capacity  = var.max_read_capacity
  max_write_capacity = var.max_write_capacity
}

module "participants_table_gsi_autoscaling" {
  source                   = "../../dynamo_db_index_autoscaling_policy"
  table_name               = aws_dynamodb_table.participants_table.name
  min_index_read_capacity  = var.min_read_capacity
  min_index_write_capacity = var.min_write_capacity
  max_index_read_capacity  = var.max_read_capacity
  max_index_write_capacity = var.max_write_capacity
  index_name               = "user_id-last_contribution_time-index"
}

module "participants_table_scheduled_backups" {
  source                   = "../../scheduled_dynamo_backup_event"
  table_name               = aws_dynamodb_table.participants_table.name
  dynamo_backup_lambda_arn = var.dynamo_backup_lambda_arn
}
