resource "aws_dynamodb_table" "fabs_worker_idempotency_table" {
  name           = "${var.prefix}_fabs_worker_idempotency"
  hash_key       = "transactionID"
  read_capacity  = var.min_read_capacity
  write_capacity = var.min_write_capacity
  stream_enabled = "true"
  stream_view_type = "NEW_AND_OLD_IMAGES"

  attribute {
    name = "transactionID"
    type = "S"
  }
}

module "fabs_worker_idempotency_table_autoscaling" {
  source             = "../../dynamo_db_autoscaling_policy"
  table_name         = aws_dynamodb_table.fabs_worker_idempotency_table.name
  min_read_capacity  = var.min_read_capacity
  min_write_capacity = var.min_write_capacity
  max_read_capacity  = var.max_read_capacity
  max_write_capacity = var.max_write_capacity
}

module "fabs_worker_idempotency_scheduled_backups" {
  source                   = "../../scheduled_dynamo_backup_event"
  table_name               = aws_dynamodb_table.fabs_worker_idempotency_table.name
  dynamo_backup_lambda_arn = var.dynamo_backup_lambda_arn
}
