variable "voldemote_packs_read_capacity_prod" {
  type = string
  default = "1000"
}

locals {
  voldemote_packs_read_capacity = var.stage == "staging" ? var.min_read_capacity : var.voldemote_packs_read_capacity_prod
}

resource "aws_dynamodb_table" "voldemote_packs_table" {
  name           = "${var.prefix}_voldemote_packs"
  hash_key       = "owner_id"
  range_key      = "pack_id"
  read_capacity  = local.voldemote_packs_read_capacity
  write_capacity = var.min_write_capacity
  stream_enabled = "true"
  stream_view_type = "NEW_AND_OLD_IMAGES"

  attribute {
    name = "owner_id"
    type = "S"
  }

  attribute {
    name = "pack_id"
    type = "S"
  }
}

module "voldemote_packs_table_autoscaling" {
  source                 = "../../dynamo_db_autoscaling_policy"
  table_name             = aws_dynamodb_table.voldemote_packs_table.name
  min_read_capacity      = local.voldemote_packs_read_capacity
  min_write_capacity     = var.min_write_capacity
  max_read_capacity      = var.max_read_capacity
  max_write_capacity     = var.max_write_capacity
}

module "voldemote_packs_scheduled_backups" {
  source                   = "../../scheduled_dynamo_backup_event"
  table_name               = aws_dynamodb_table.voldemote_packs_table.name
  dynamo_backup_lambda_arn = var.dynamo_backup_lambda_arn
}