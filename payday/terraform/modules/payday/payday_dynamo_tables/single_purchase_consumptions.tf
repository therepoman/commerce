variable "single_purchase_consumptions_prod_read" {
  type    = string
  default = "500"
}

variable "single_purchase_consumptions_prod_write" {
  type    = string
  default = "50"
}

locals {
  single_purchase_consumptions_read_capacity = var.stage == "staging" ? var.min_read_capacity : var.single_purchase_consumptions_prod_read
  single_purchase_consumptions_write_capacity = var.stage == "staging" ? var.min_write_capacity : var.single_purchase_consumptions_prod_write
}

resource "aws_dynamodb_table" "single_purchase_consumptions_table" {
  name           = "${var.prefix}_single_purchase_consumptions"
  hash_key       = "twitchUserId"
  range_key      = "productId"
  read_capacity  = local.single_purchase_consumptions_read_capacity
  write_capacity = local.single_purchase_consumptions_write_capacity
  stream_enabled = "true"
  stream_view_type = "NEW_AND_OLD_IMAGES"

  attribute {
    name = "twitchUserId"
    type = "S"
  }

  attribute {
    name = "productId"
    type = "S"
  }

  lifecycle {
    ignore_changes = [
      read_capacity,
      write_capacity
    ]
  }
}

module "single_purchase_consumptions_table_autoscaling" {
  source             = "../../dynamo_db_autoscaling_policy"
  table_name         = aws_dynamodb_table.single_purchase_consumptions_table.name
  min_read_capacity  = local.single_purchase_consumptions_read_capacity
  min_write_capacity = local.single_purchase_consumptions_write_capacity
  max_read_capacity  = var.max_read_capacity
  max_write_capacity = var.max_write_capacity
}

module "single_purchase_consumptions_scheduled_backups" {
  source                   = "../../scheduled_dynamo_backup_event"
  table_name               = aws_dynamodb_table.single_purchase_consumptions_table.name
  dynamo_backup_lambda_arn = var.dynamo_backup_lambda_arn
}