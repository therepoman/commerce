variable "badge_tier_notification_read_capacity_prod" {
  type = string
  default = "3000"
}

locals {
  badge_tier_notification_read_capacity = var.stage == "staging" ? var.min_read_capacity : var.badge_tier_notification_read_capacity_prod
}

resource "aws_dynamodb_table" "badge_tier_notifications_table" {
  name           = "${var.prefix}_badge_tier_notifications"
  hash_key       = "channel_id-user_id"
  range_key      = "threshold"
  read_capacity  = local.badge_tier_notification_read_capacity
  write_capacity = var.min_write_capacity
  stream_enabled = "true"
  stream_view_type = "NEW_AND_OLD_IMAGES"

  attribute {
    name = "channel_id-user_id"
    type = "S"
  }

  attribute {
    name = "threshold"
    type = "N"
  }

  attribute {
    name = "notification_id"
    type = "S"
  }

  global_secondary_index {
    name               = "notification_id-index"
    hash_key           = "notification_id"
    write_capacity     = var.min_read_capacity
    read_capacity      = var.min_write_capacity
    projection_type    = "ALL"
  }

  ttl {
    enabled        = true
    attribute_name = "expires_at"
  }

  lifecycle {
    ignore_changes = [read_capacity, write_capacity]
  }
}

module "badge_tier_notifications_table_autoscaling" {
  source                 = "../../dynamo_db_autoscaling_policy"
  table_name             = "${var.prefix}_badge_tier_notifications"
  min_read_capacity      = local.badge_tier_notification_read_capacity
  min_write_capacity     = var.min_write_capacity
  max_read_capacity      = var.max_read_capacity
  max_write_capacity     = var.max_write_capacity
}

module "badge_tier_notifications_table_notification_id_index_autoscaling" {
  source                   = "../../dynamo_db_index_autoscaling_policy"
  table_name               = "${var.prefix}_badge_tier_notifications"
  min_index_read_capacity  = var.min_read_capacity
  min_index_write_capacity = var.min_write_capacity
  max_index_read_capacity  = var.max_read_capacity
  max_index_write_capacity = var.max_write_capacity
  index_name               = "notification_id-index"
}

module "badge_tier_notifications_scheduled_backups" {
  source                   = "../../scheduled_dynamo_backup_event"
  table_name               = "${var.prefix}_badge_tier_notifications"
  dynamo_backup_lambda_arn = var.dynamo_backup_lambda_arn
}