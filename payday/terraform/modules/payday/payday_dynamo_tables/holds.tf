variable "holds_read_capacity_prod" {
  type = string
}

locals {
  holds_read_capacity = var.stage == "staging" ? var.min_read_capacity : var.holds_read_capacity_prod
}

resource "aws_dynamodb_table" "holds_table" {
  name           = "${var.prefix}_holds"
  hash_key       = "user_id"
  range_key      = "hold_id"
  read_capacity  = local.holds_read_capacity
  write_capacity = var.min_write_capacity
  stream_enabled = "true"
  stream_view_type = "NEW_AND_OLD_IMAGES"

  attribute {
    name = "user_id"
    type = "S"
  }

  attribute {
    name = "hold_id"
    type = "S"
  }

  lifecycle {
    ignore_changes = [read_capacity, write_capacity]
  }
}

module "holds_table_autoscaling" {
  source                 = "../../dynamo_db_autoscaling_policy"
  table_name             = "${var.prefix}_holds"
  min_read_capacity      = local.holds_read_capacity
  min_write_capacity     = var.min_write_capacity
  max_read_capacity      = var.max_read_capacity
  max_write_capacity     = var.max_write_capacity
}

module "holds_scheduled_backups" {
  source                   = "../../scheduled_dynamo_backup_event"
  table_name               = "${var.prefix}_holds"
  dynamo_backup_lambda_arn = var.dynamo_backup_lambda_arn
}