resource "aws_dynamodb_table" "challenges_table" {
  name     = "${var.prefix}_challenges"
  hash_key = "challenge_id"

  read_capacity    = var.min_read_capacity
  write_capacity   = var.min_write_capacity
  stream_enabled   = "true"
  stream_view_type = "NEW_AND_OLD_IMAGES"

  attribute {
    name = "challenge_id"
    type = "S"
  }

  attribute {
    name = "owned_by"
    type = "S"
  }

  attribute {
    name = "created_time"
    type = "S"
  }

  global_secondary_index {
    name            = "owned_by-created_time-index"
    hash_key        = "owned_by"
    range_key       = "created_time"
    write_capacity  = var.min_read_capacity
    read_capacity   = var.min_write_capacity
    projection_type = "ALL"
  }
}

module "challenges_table_autoscaling" {
  source             = "../../dynamo_db_autoscaling_policy"
  table_name         = aws_dynamodb_table.challenges_table.name
  min_read_capacity  = var.min_read_capacity
  min_write_capacity = var.min_write_capacity
  max_read_capacity  = var.max_read_capacity
  max_write_capacity = var.max_write_capacity
}

module "challenges_table_gsi_autoscaling" {
  source                   = "../../dynamo_db_index_autoscaling_policy"
  table_name               = aws_dynamodb_table.challenges_table.name
  min_index_read_capacity  = var.min_read_capacity
  min_index_write_capacity = var.min_write_capacity
  max_index_read_capacity  = var.max_read_capacity
  max_index_write_capacity = var.max_write_capacity
  index_name               = "owned_by-created_time-index"
}

module "challenges_table_scheduled_backups" {
  source                   = "../../scheduled_dynamo_backup_event"
  table_name               = aws_dynamodb_table.challenges_table.name
  dynamo_backup_lambda_arn = var.dynamo_backup_lambda_arn
}
