variable "purchase_by_most_recent_super_platform_read_capacity_prod" {
  type    = string
  default = "1500"
}

locals {
  purchase_by_most_recent_super_platform_read_capacity = var.stage == "staging" ? var.min_read_capacity : var.purchase_by_most_recent_super_platform_read_capacity_prod
}

resource "aws_dynamodb_table" "purchase_by_most_recent_super_platform_table" {
  name           = "${var.prefix}_purchase_by_most_recent_super_platform"
  hash_key       = "twitchUserId"
  range_key      = "superPlatform"
  read_capacity  = local.purchase_by_most_recent_super_platform_read_capacity
  write_capacity = var.min_write_capacity
  stream_enabled = "true"
  stream_view_type = "NEW_AND_OLD_IMAGES"

  attribute {
    name = "twitchUserId"
    type = "S"
  }

  attribute {
    name = "superPlatform"
    type = "S"
  }

  lifecycle {
    ignore_changes = [read_capacity, write_capacity]
  }
}

module "purchase_by_most_recent_super_platform_table_autoscaling" {
  source             = "../../dynamo_db_autoscaling_policy"
  table_name         = aws_dynamodb_table.purchase_by_most_recent_super_platform_table.name
  min_read_capacity  = local.purchase_by_most_recent_super_platform_read_capacity
  min_write_capacity = var.min_write_capacity
  max_read_capacity  = var.max_read_capacity
  max_write_capacity = var.max_write_capacity
}

module "purchase_by_most_recent_super_platform_scheduled_backups" {
  source                   = "../../scheduled_dynamo_backup_event"
  table_name               = aws_dynamodb_table.purchase_by_most_recent_super_platform_table.name
  dynamo_backup_lambda_arn = var.dynamo_backup_lambda_arn
}
