locals {
  channels_name     = "${var.prefix}_channels"
  channels_glue_job = "${var.prefix}-channels"
}

resource "aws_sns_topic" "channels_db_export_glue_errors" {
  name = "${local.channels_name}-dbexport-error"
}

module "channels_glue_dynamo" {
  source = "git::ssh://git@git.xarth.tv/dp/db-s3-glue.git?ref=926a2ddddee44e1d528d7ac241c5dec1f1cddeca"
  database_type = "dynamodb"
  job_name = "${local.channels_glue_job}-dbexport-job"
  cluster_name = local.channels_name
  table_config = {
    (local.channels_name) = <<EOF
      {
        "worker_count": 1,
        "read_ratio": 0.2,
        "version": 7,
        "spark_optimization": true,
        "schema": [
          {"name": "id", "type": "string", "sensitivity": "userid" },
          {"name": "lastUpdated", "type": "bigint", "output_type": "timestamp"},
          {"name": "lastOnboardedEventTime", "type": "bigint", "output_type": "timestamp"},
          {"name": "optedOut", "type": "boolean"},
          {"name": "onboarded", "type": "boolean"},
          {"name": "annotation", "type": "string"},
          {"name": "minBits", "type": "bigint"},
          {"name": "minBitsEmote", "type": "bigint"},
          {"name": "pinTopCheers", "type": "boolean"},
          {"name": "pinRecentCheers", "type": "boolean"},
          {"name": "recentCheerMin", "type": "bigint"},
          {"name": "recentCheerTimeout", "type": "bigint"},
          {"name": "session", "type": "string", "sensitivity": "sessionid"},
          {"name": "customCheermoteImageSetId", "type": "string"},
          {"name": "customCheermoteStatus", "type": "string"},
          {"name": "amendmentState", "type": "string"},
          {"name": "leaderboardEnabled", "type": "boolean"},
          {"name": "isCheerLeaderboardEnabled", "type": "boolean"},
          {"name": "isSubGiftLeaderboardEnabled", "type": "boolean"},
          {"name": "defaultLeaderboard", "type": "string"},
          {"name": "leaderboardTimePeriod", "type": "string"},
          {"name": "cheerBombEventOptOut", "type": "boolean"},
          {"name": "optedOutOfVoldemort", "type": "boolean"}
        ],
        "output_fields": ["id", "lastUpdated", "lastOnboardedEventTime", "optedOut", "onboarded", "annotation", "minBits", "minBitsEmote", "pinTopCheers", "pinRecentCheers", "recentCheerMin", "recentCheerTimeout", "session", "customCheermoteImageSetId", "customCheermoteStatus", "amendmentState", "leaderboardEnabled", "isCheerLeaderboardEnabled", "isSubGiftLeaderboardEnabled", "defaultLeaderboard", "leaderboardTimePeriod", "cheerBombEventOptOut", "optedOutOfVoldemort"]
      }
EOF
  }

  spark_cleaning_code = <<EOF
return (df
  .withColumn('lastUpdated', F.when(F.col('lastUpdated').isNotNull(), F.from_unixtime(df.lastUpdated / 1000000000).cast('timestamp')))
  .withColumn('lastOnboardedEventTime', F.when(F.col('lastOnboardedEventTime').isNotNull(), F.from_unixtime(df.lastOnboardedEventTime / 1000000000).cast('timestamp')))
)
EOF

  dynamodb_splits_count      = "1"
  create_s3_output_bucket    = 1
  s3_output_bucket           = "${local.channels_glue_job}-dbexport-job-output-bucket"
  s3_output_key              = "${local.channels_glue_job}-dbexport-job-output-key"
  create_s3_script_bucket    = 1
  s3_script_bucket           = "${local.channels_glue_job}-dbexport-job-script-bucket"
  error_sns_topic_name       = aws_sns_topic.channels_db_export_glue_errors.name
  account_number             = var.account_id
  vpc_id                     = ""
  subnet_id                  = ""
  availability_zone          = ""
  rds_subnet_group           = ""
  cluster_username           = ""
  db_password_parameter_name = ""
  db_password_key_id         = ""
  api_key_parameter_name     = var.tahoe_api_password_name
  api_key_kms_key_id         = var.tahoe_api_tagert_key_id
  tahoe_producer_name        = "payday${var.prefix}dbexport"
  tahoe_producer_role_arn    = "arn:aws:iam::331582574546:role/producer-payday${var.prefix}dbexport"
}

output "s3_kms_key" {
  value = module.channels_glue_dynamo.s3_kms_key
}

output "s3_output_bucket" {
  value = module.channels_glue_dynamo.s3_output_bucket
}

output "glue_role" {
  value = module.channels_glue_dynamo.glue_role
}

output "channels_db_export_glue_errors_arn" {
  value = aws_sns_topic.channels_db_export_glue_errors.arn
}
