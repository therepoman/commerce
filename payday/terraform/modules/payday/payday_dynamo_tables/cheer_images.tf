variable "cheer_images_prod_read" {
  type    = string
  default = "500"
}

variable "cheer_images_prod_write" {
  type    = string
  default = "50"
}

locals {
  cheer_images_read_capacity = var.stage == "staging" ? var.min_read_capacity : var.cheer_images_prod_read
  cheer_images_write_capacity = var.stage == "staging" ? var.min_write_capacity : var.cheer_images_prod_write
}

resource "aws_dynamodb_table" "cheer_images_table" {
  name           = "${var.prefix}_cheer_images"
  hash_key       = "user_id"
  range_key      = "created_at"
  read_capacity  = local.cheer_images_read_capacity
  write_capacity = local.cheer_images_write_capacity
  stream_enabled = "true"
  stream_view_type = "NEW_AND_OLD_IMAGES"

  attribute {
    name = "user_id"
    type = "S"
  }

  attribute {
    name = "created_at"
    type = "S"
  }

  attribute {
    name = "image_id"
    type = "S"
  }

  global_secondary_index {
    name            = "image_id-index"
    hash_key        = "image_id"
    write_capacity  = var.min_read_capacity
    read_capacity   = var.min_write_capacity
    projection_type = "ALL"
  }

  lifecycle {
    ignore_changes = [
      read_capacity,
      write_capacity
    ]
  }
}

module "cheer_images_table_autoscaling" {
  source             = "../../dynamo_db_autoscaling_policy"
  table_name         = aws_dynamodb_table.cheer_images_table.name
  min_read_capacity  = local.cheer_images_read_capacity
  min_write_capacity = local.cheer_images_write_capacity
  max_read_capacity  = var.max_read_capacity
  max_write_capacity = var.max_write_capacity
}

module "cheer_images_table_image_id_index_autoscaling" {
  source                   = "../../dynamo_db_index_autoscaling_policy"
  table_name               = aws_dynamodb_table.cheer_images_table.name
  min_index_read_capacity  = var.min_read_capacity
  min_index_write_capacity = var.min_write_capacity
  max_index_read_capacity  = var.max_read_capacity
  max_index_write_capacity = var.max_write_capacity
  index_name               = "image_id-index"
}

module "cheer_images_scheduled_backups" {
  source                   = "../../scheduled_dynamo_backup_event"
  table_name               = aws_dynamodb_table.cheer_images_table.name
  dynamo_backup_lambda_arn = var.dynamo_backup_lambda_arn
}