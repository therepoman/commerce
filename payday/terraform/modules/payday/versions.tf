terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "2.57.0" # Pinning to specific version as per https://jira.twitch.com/browse/MOMENTS-1166
    }
  }
  required_version = ">= 0.14"
}
