module "lambda-role" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//lambda-role?ref=7a97551c37b059b469c6734f3d64771a2d5af6ba"
  name   = "payday-${var.stage}"
}

module "lambda-bucket" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//lambda-s3-bucket?ref=7a97551c37b059b469c6734f3d64771a2d5af6ba"

  name        = "payday" // terracode concats env to the bucket name for us
  environment = var.stage
  role_arn    = module.lambda-role.arn
}

// balance change processing: BTER entitlements
module "balance_change_processing_emote_entitlements" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//lambda?ref=7a97551c37b059b469c6734f3d64771a2d5af6ba"

  name            = "balance_change_processing_emote_entitlements_${var.stage}"
  handler         = "main"
  tag             = "latest"
  environment     = var.stage
  bucket          = module.lambda-bucket.bucket
  role_arn        = module.lambda-role.arn
  subnet_ids      = split(",", var.private_subnets)
  security_groups = [local.security_group[var.stage]]
  publish         = false

  env_vars = {
    payday_APP_ENV = var.stage
  }
}

// balance change processing: badge entitlements
module "balance_change_processing_badge_entitlements" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//lambda?ref=7a97551c37b059b469c6734f3d64771a2d5af6ba"

  name            = "balance_change_processing_badge_entitlements_${var.stage}"
  handler         = "main"
  tag             = "latest"
  environment     = var.stage
  bucket          = module.lambda-bucket.bucket
  role_arn        = module.lambda-role.arn
  subnet_ids      = split(",", var.private_subnets)
  security_groups = [local.security_group[var.stage]]
  publish         = false

  env_vars = {
    payday_APP_ENV = var.stage
  }
}

// balance change processing: entitlement pubsub
module "balance_change_processing_entitlement_pubsub" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//lambda?ref=7a97551c37b059b469c6734f3d64771a2d5af6ba"

  name            = "balance_change_processing_entitlement_pubsub_${var.stage}"
  handler         = "main"
  tag             = "latest"
  environment     = var.stage
  bucket          = module.lambda-bucket.bucket
  role_arn        = module.lambda-role.arn
  subnet_ids      = split(",", var.private_subnets)
  security_groups = [local.security_group[var.stage]]
  publish         = false

  env_vars = {
    payday_APP_ENV = var.stage
  }
}

module "delete_user_bits_onboard_events" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//lambda?ref=7a97551c37b059b469c6734f3d64771a2d5af6ba"

  name            = "delete_user_bits_onboard_events_${var.stage}"
  description     = "Deleting user from bits-onboard-events S3 bucket"
  handler         = "main"
  tag             = "latest"
  environment     = var.stage
  bucket          = module.lambda-bucket.bucket
  role_arn        = module.lambda-role.arn
  subnet_ids      = split(",", var.private_subnets)
  security_groups = [local.security_group[var.stage]]
  publish         = false

  env_vars = {
    payday_APP_ENV = var.stage
  }
}

module "delete_channel_cheermotes" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//lambda?ref=7a97551c37b059b469c6734f3d64771a2d5af6ba"

  name            = "delete_channel_cheermotes_${var.stage}"
  description     = "Deleting user cheermotes from bits-assets/partner-actions S3 bucket"
  handler         = "main"
  tag             = "latest"
  environment     = var.stage
  bucket          = module.lambda-bucket.bucket
  role_arn        = module.lambda-role.arn
  subnet_ids      = split(",", var.private_subnets)
  security_groups = [local.security_group[var.stage]]
  publish         = false

  env_vars = {
    payday_APP_ENV = var.stage
  }
}

module "delete_user_purchase_by_platform" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//lambda?ref=7a97551c37b059b469c6734f3d64771a2d5af6ba"

  name            = "delete_user_purchase_by_platform_${var.stage}"
  description     = "Deleting user from Dynamo Table: purchase_by_platform"
  handler         = "main"
  tag             = "latest"
  environment     = var.stage
  bucket          = module.lambda-bucket.bucket
  role_arn        = module.lambda-role.arn
  subnet_ids      = split(",", var.private_subnets)
  security_groups = [local.security_group[var.stage]]
  publish         = false

  env_vars = {
    payday_APP_ENV = var.stage
  }
}

module "delete_user_single_purchase_consumption" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//lambda?ref=7a97551c37b059b469c6734f3d64771a2d5af6ba"

  name            = "delete_user_single_purchase_consumption_${var.stage}"
  description     = "Deleting user from Dynamo Table: single_purchase_consump"
  handler         = "main"
  tag             = "latest"
  environment     = var.stage
  bucket          = module.lambda-bucket.bucket
  role_arn        = module.lambda-role.arn
  subnet_ids      = split(",", var.private_subnets)
  security_groups = [local.security_group[var.stage]]
  publish         = false

  env_vars = {
    payday_APP_ENV = var.stage
  }
}

module "delete_channel_badge_tiers_and_emote_groupids" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//lambda?ref=7a97551c37b059b469c6734f3d64771a2d5af6ba"

  name            = "delete_channel_badge_tiers_and_emote_groupids_${var.stage}"
  description     = "Deleting user from Dynamo Tables: badge_tiers, badge_tier_emote_groupids"
  handler         = "main"
  tag             = "latest"
  environment     = var.stage
  bucket          = module.lambda-bucket.bucket
  role_arn        = module.lambda-role.arn
  subnet_ids      = split(",", var.private_subnets)
  security_groups = [local.security_group[var.stage]]
  publish         = false

  env_vars = {
    payday_APP_ENV = var.stage
  }
}

module "delete_user" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//lambda?ref=7a97551c37b059b469c6734f3d64771a2d5af6ba"

  name            = "delete_user_${var.stage}"
  description     = "Deleting user from Dynamo Table users"
  handler         = "main"
  tag             = "latest"
  environment     = var.stage
  bucket          = module.lambda-bucket.bucket
  role_arn        = module.lambda-role.arn
  subnet_ids      = split(",", var.private_subnets)
  security_groups = [local.security_group[var.stage]]
  publish         = false

  env_vars = {
    payday_APP_ENV = var.stage
  }
}

module "delete_user_leaderboard_badge_holders" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//lambda?ref=7a97551c37b059b469c6734f3d64771a2d5af6ba"

  name            = "delete_user_leaderboard_badge_holders_${var.stage}"
  description     = "Deleting user from Dynamo Table leaderboard_badge_holders"
  handler         = "main"
  tag             = "latest"
  environment     = var.stage
  bucket          = module.lambda-bucket.bucket
  role_arn        = module.lambda-role.arn
  subnet_ids      = split(",", var.private_subnets)
  security_groups = [local.security_group[var.stage]]
  publish         = false

  env_vars = {
    payday_APP_ENV = var.stage
  }
}

module "delete_user_channels_and_images" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//lambda?ref=7a97551c37b059b469c6734f3d64771a2d5af6ba"

  name            = "delete_user_channels_and_images_${var.stage}"
  description     = "Deleting user from Dynamo Tables: channels, images"
  handler         = "main"
  tag             = "latest"
  environment     = var.stage
  bucket          = module.lambda-bucket.bucket
  role_arn        = module.lambda-role.arn
  subnet_ids      = split(",", var.private_subnets)
  security_groups = [local.security_group[var.stage]]
  publish         = false

  env_vars = {
    payday_APP_ENV = var.stage
  }
}

module "delete_user_spomote_channel_statuses" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//lambda?ref=7a97551c37b059b469c6734f3d64771a2d5af6ba"

  name            = "delete_user_spomote_channel_statuses_${var.stage}"
  description     = "Deleting user from Dynamo Table: sponsored_cheermote_channel_statuses"
  handler         = "main"
  tag             = "latest"
  environment     = var.stage
  bucket          = module.lambda-bucket.bucket
  role_arn        = module.lambda-role.arn
  subnet_ids      = split(",", var.private_subnets)
  security_groups = [local.security_group[var.stage]]
  publish         = false

  env_vars = {
    payday_APP_ENV = var.stage
  }
}

module "delete_user_spomote_eligible_channels" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//lambda?ref=7a97551c37b059b469c6734f3d64771a2d5af6ba"

  name            = "delete_user_spomote_eligible_channels_${var.stage}"
  description     = "Deleting user from Dynamo Table: sponsored_cheermote_eligible_channels"
  handler         = "main"
  tag             = "latest"
  environment     = var.stage
  bucket          = module.lambda-bucket.bucket
  role_arn        = module.lambda-role.arn
  subnet_ids      = split(",", var.private_subnets)
  security_groups = [local.security_group[var.stage]]
  publish         = false

  env_vars = {
    payday_APP_ENV = var.stage
  }
}

module "delete_user_spomote_user_statuses" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//lambda?ref=7a97551c37b059b469c6734f3d64771a2d5af6ba"

  name            = "delete_user_spomote_user_statuses_${var.stage}"
  description     = "Deleting user from Dynamo Table: sponsored_cheermote_user_statuses"
  handler         = "main"
  tag             = "latest"
  environment     = var.stage
  bucket          = module.lambda-bucket.bucket
  role_arn        = module.lambda-role.arn
  subnet_ids      = split(",", var.private_subnets)
  security_groups = [local.security_group[var.stage]]
  publish         = false

  env_vars = {
    payday_APP_ENV = var.stage
  }
}

module "delete_user_balance" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//lambda?ref=7a97551c37b059b469c6734f3d64771a2d5af6ba"

  name            = "delete_user_balance_${var.stage}"
  description     = "Deleting user balance from Pachinko"
  handler         = "main"
  tag             = "latest"
  environment     = var.stage
  bucket          = module.lambda-bucket.bucket
  role_arn        = module.lambda-role.arn
  subnet_ids      = split(",", var.private_subnets)
  security_groups = [local.security_group[var.stage]]
  publish         = false

  env_vars = {
    payday_APP_ENV = var.stage
  }
}

module "delete_user_leaderboard" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//lambda?ref=7a97551c37b059b469c6734f3d64771a2d5af6ba"

  name            = "delete_user_leaderboard_${var.stage}"
  description     = "Deleting user leaderboard info from Pantheon"
  handler         = "main"
  tag             = "latest"
  environment     = var.stage
  bucket          = module.lambda-bucket.bucket
  role_arn        = module.lambda-role.arn
  subnet_ids      = split(",", var.private_subnets)
  security_groups = [local.security_group[var.stage]]
  publish         = false

  env_vars = {
    payday_APP_ENV = var.stage
  }
}

module "report_deletion" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//lambda?ref=7a97551c37b059b469c6734f3d64771a2d5af6ba"

  name            = "report_deletion_${var.stage}"
  description     = "Reporting user deletion to PDMS"
  handler         = "main"
  tag             = "latest"
  environment     = var.stage
  bucket          = module.lambda-bucket.bucket
  role_arn        = module.lambda-role.arn
  subnet_ids      = split(",", var.private_subnets)
  security_groups = [local.security_group[var.stage]]
  publish         = false

  env_vars = {
    payday_APP_ENV = var.stage
  }
}

// For S2S2
resource "aws_iam_role_policy_attachment" "s2s2_iam_policy_lambda_attachment" {
  role       = module.lambda-role.name
  policy_arn = aws_iam_policy.s2s2_iam_policy.arn
}
