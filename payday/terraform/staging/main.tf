locals {
  region  = "us-west-2"
  stage   = "staging"
  vpc_id  = "vpc-4cfeb629"
  subnets = "subnet-3dc3c44a,subnet-ada8f5f4,subnet-0e86646a"
  // we are putting staging and prod records together in one hosted zone
  prod_hosted_zone_id = "Z2483KRFD7YUWI"
}

module "payday_staging" {
  source              = "../modules/payday"
  account_name        = "twitch-bits-aws"
  account_id          = "021561903526"
  owner               = "twitch-bits-oncall@amazon.com"
  env                 = "payday-staging-beanstalk"
  env_prefix          = "test"
  vpc_id              = local.vpc_id
  private_subnets     = local.subnets
  private_cidr_blocks = ["10.193.113.0/24", "10.193.114.0/24", "10.193.112.0/24"]
  iam_role_name       = "payday_staging"
  stage               = local.stage

  min_read_capacity  = 30
  min_write_capacity = 30
  max_read_capacity  = 100
  max_write_capacity = 100

  #SNS/SQS
  alarm_dlq                    = false
  queue_name_suffix            = "_staging" //  TODO: discuss having both staging and prod environments include a queue name suffix
  user_moderation_sns_arn      = "arn:aws:sns:us-west-2:603200399373:clue_staging_channel_ban_events"
  pagle_completion_sns_arn     = "arn:aws:sns:us-west-2:574977834358:staging-condition-completion"
  pachinko_bits_update_sns_arn = "arn:aws:sns:us-west-2:946879801923:bits-staging-updates"

  elasticache_num_node_groups         = "2"
  elasticache_replicas_per_node_group = "2"

  ecs_healthcheck              = "/health-check"
  ecs_ec2_backed_min_instances = 6
  ecs_min_instances            = 12
  ecs_max_instances            = 24
  ecs_cpu                      = 1024
  ecs_memory                   = 1024
  sandstorm_role               = "payday-staging"
  dns                          = "payday-admin.dev.us-west2.justin.tv"
  secondary_dns                = module.privatelink-cert.dns
  event_bus_iam_arn            = "arn:aws:iam::021561903526:policy/EventBus/EventBusAccess"
  auto_refill_table_account_id = 482521867325

  ecs_upgrade_sns_topic = "arn:aws:sns:us-west-2:021561903526:on_terminate_lifecycle_hook_sns_topic"

  ecs_env_vars = [
    {
      name  = "payday_APP_ENV"
      value = "staging"
    },
    {
      name  = "AWS_STS_REGIONAL_ENDPOINTS",
      value = "regional"
    }
  ]
}

provider "aws" {
  region  = "us-west-2"
  profile = "payday"
}

terraform {
  backend "s3" {
    bucket  = "payday-tfstate"
    key     = "tfstate/commerce/payday/terraform/staging"
    region  = "us-west-2"
    profile = "payday"
  }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 2.44"
    }
  }

  required_version = ">= 0.14"
}

/*
  Note: We're putting code for Payday Staging a2z migration here instead of
  modules/payday/privatelink.tf because Payday Prod environment was migrated
  manually. Having these applied to both Prod and Staging could have unwanted
  side effects on Prod.
*/

module "privatelink-cert" {
  source      = "git::git+ssh://git@git.xarth.tv/subs/terracode//privatelink-cert?ref=7a97551c37b059b469c6734f3d64771a2d5af6ba"
  name        = "payday"
  environment = local.stage
  zone_id     = local.prod_hosted_zone_id
  alb_dns     = module.payday_staging.service_dns
}

module "privatelink_a2z" {
  source      = "git::git+ssh://git@git.xarth.tv/subs/terracode//privatelink?ref=7a97551c37b059b469c6734f3d64771a2d5af6ba"
  name        = "payday"
  region      = local.region
  environment = local.stage
  alb_dns     = module.payday_staging.service_dns
  cert_arn    = module.privatelink-cert.arn
  vpc_id      = local.vpc_id
  subnets     = split(",", local.subnets)
  whitelisted_arns = [
    "arn:aws:iam::999515204624:root", // Percy staging
    "arn:aws:iam::261425071853:root", // Corleone Staging
    "arn:aws:iam::721938063588:root", // Pachter Staging
    "arn:aws:iam::219087926005:root", // Admin Panel Staging
    "arn:aws:iam::962912953676:root", // CoPo staging (hackathon 10/6/20)
    "arn:aws:iam::144996434858:root", // Toolkit
    "arn:aws:iam::077362005997:root", // Mako Staging
    "arn:aws:iam::525313514111:root", // Everdeen Staging
    "arn:aws:iam::945359790842:root", // Chat CB2 Staging
  ]
  # Payday has other upstreams but those are peered
}

/*
  For Direct Connect Auditing:
  Plan: https://docs.google.com/document/d/16yXhBzuiDGO_kc2D8SUW_x9C5bEvWypxRi329B4DroA/edit#
  This creates VPC Flow logs for the following VPCs:
  - Payday
  - TPSGenerator
  - Raincatcher
  Since this has no impact on Prod traffic, to show that this is not dangerous to delete I'm creating it here.
*/

resource "aws_cloudwatch_log_group" "payday_vpc_flow_logs" {
  name              = "payday_vpc_flow_logs"
  retention_in_days = 0
}

resource "aws_flow_log" "payday_vpc_flow_logs" {
  traffic_type    = "ALL"
  log_destination = aws_cloudwatch_log_group.payday_vpc_flow_logs.arn
  vpc_id          = local.vpc_id
  iam_role_arn    = aws_iam_role.vpc_flow_logs_access_role.arn
}

resource "aws_cloudwatch_log_group" "tps_generator_vpc_flow_logs" {
  name              = "tps_generator_vpc_flow_logs"
  retention_in_days = 0
}

resource "aws_flow_log" "tps_generator_vpc_flow_logs" {
  traffic_type    = "ALL"
  log_destination = aws_cloudwatch_log_group.tps_generator_vpc_flow_logs.arn
  vpc_id          = "vpc-5d4d5638"
  iam_role_arn    = aws_iam_role.vpc_flow_logs_access_role.arn
}

resource "aws_cloudwatch_log_group" "raincatcher_vpc_flow_logs" {
  name              = "raincatcher_vpc_flow_logs"
  retention_in_days = 0
}

resource "aws_flow_log" "raincatcher_vpc_flow_logs" {
  traffic_type    = "ALL"
  log_destination = aws_cloudwatch_log_group.raincatcher_vpc_flow_logs.arn
  vpc_id          = "vpc-f0598694"
  iam_role_arn    = aws_iam_role.vpc_flow_logs_access_role.arn
}

resource "aws_iam_role" "vpc_flow_logs_access_role" {
  name = "vpc_flow_logs_access_role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "vpc-flow-logs.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "vpc_flow_logs_access_role_policy" {
  name = "vpc_flow_logs_access_role_policy"
  role = aws_iam_role.vpc_flow_logs_access_role.id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents",
        "logs:DescribeLogGroups",
        "logs:DescribeLogStreams"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}
