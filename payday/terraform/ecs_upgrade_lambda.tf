module "upgrade_complete" {
  source = "git::git+ssh://git@git.xarth.tv/edge/upgrade-complete//terraform?ref=1171046271eaed04e01a77afd72896c0b7d4c94e"
  aws_account_id = "021561903526"
  function_zip_path = "${path.module}/files/upgrade-complete.zip"
}
