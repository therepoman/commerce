**THE ROOT `/terraform` DIRECTORY IS DEPRECATED - ADD NEW INFRA TO `/terraform/modules/payday`**

___________

This directory contains the Terraform configuration for Payday and this file contains instructions on how to use it.

**NOTE:** It is dangerous to build this Terraform configuration in its current form due to conflict between it and the Janus Terraform configuration!  See [this Twitch wiki page](https://twitchtv.atlassian.net/wiki/display/ENG/Infrastructure+considerations+and+tools) for details.

To better understand what Terraform is and how the commands below work, refer to [Terraform documentation](https://www.terraform.io/docs/index.html).  

# AWS Accounts and Permissions

## twitch-science-aws

See also the [Payday account docs](https://git-aws.internal.justin.tv/commerce/payday#access-to-twitch-main-aws-account).

1. If you do not already have access to the twitch-science-aws account, request access in the Systems project in Jira.  See payday/README.md for more info.
2. Once you've been given access and have logged into the account, create and record access and secret keys.

The IAM account with which you logged into the twitch-science-aws account will be referred to as the *Twitch account*.  The access and secret keys for the Twitch account will be referred to as the *Twitch access key* and *Twitch secret key*.

# Running Terraform

All Terraform commands should be run from the `code.justin.tv/commerce/payday/terraform` directory.  

Additionally, you may need to be connected to Twitch VPN in order to pull down some of the dependent configurations used by the Payday Terraform configuration.  This is not currently necessary, but may become so in the future.

Before running any Terraform commands, you may need to get dependent configuration files first by running `terraform get .`

## Credentials

You'll need to provide credentials when running Terraform.  This is best done by creating a `terraform.tfvars` file under the Terraform directory as described in the [Payday README](https://git-aws.internal.justin.tv/commerce/payday#access-to-twitch-main-aws-account).

If you don't use a tfvars file, you'll need to provide the credentials via CLI arguments: 

`-var 'twitch-aws_access_key=<Twitch access key>' -var 'twitch-aws_secret_key=<twitch secret key>'`

## Terraform plan

[Terraform documentation for the plan command.](https://www.terraform.io/docs/commands/plan.html)  Plan should be run to verify changes in configuration and to generate the new state file to be used by the Apply command.

> acbc32d4a58b:staging ckaukl$ pwd  
> /Users/ckaukl/workplace/bits/src/code.justin.tv/commerce/payday/terraform  
>  
> acbc32d4a58b:staging ckaukl$ terraform plan

Terraform will sync some dependent configuration and output the changes to state that would occur from running `terraform apply`.  

## Terraform apply

[Terraform documentation for the apply command.](https://www.terraform.io/docs/commands/apply.html)  It is strongly recommended first to run `terraform plan` with the `-out <file>` option to write the plan to a file.  Verify the changes in the file, then run apply.

> acbc32d4a58b:staging ckaukl$ pwd  
> /Users/ckaukl/workplace/janus/src/code.justin.tv/commerce/janus/terraform/staging  
>  
> \# Create a plan file first.  
> acbc32d4a58b:staging ckaukl$ terraform plan **-out newconfig.plan**
>  
> \# Apply the plan file.  
> acbc32d4a58b:staging ckaukl$ terraform apply **newconfig.plan**
