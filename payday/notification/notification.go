package notification

import (
	"encoding/csv"
	"os"

	payday_error "code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/file"
	fileUtil "code.justin.tv/commerce/payday/utils/close"
)

const (
	notificationFileNameStaging = "notification_staging"
	notificationFileNameProd    = "notification_production"
)

var filePaths = []string{"notification/", "../notification/", "../../notification/", "../../../notification/"}

type INotification interface {
	InExperimentGroup(userId string) bool
}

type Notification struct {
	usersLookup map[string]interface{}
}

func NewNotification(environment string) (INotification, error) {
	n := &Notification{}

	var err error
	if environment != "development" {
		err = n.setup(environment)
	}

	return n, err
}

func (n *Notification) setup(environment string) error {
	n.usersLookup = make(map[string]interface{})

	fileName := notificationFileNameStaging
	if environment == "production" {
		fileName = notificationFileNameProd
	}

	filePath, err := file.SearchFileInPaths(fileName, filePaths)
	if err != nil {
		return payday_error.Notef(err, "Notification: Error finding file of users in experiment: %s", fileName)
	}

	file, err := os.Open(filePath)
	if err != nil {
		return payday_error.Notef(err, "Notification: Failed while opening file: %s", filePath)
	}
	defer fileUtil.Close(file)

	reader := csv.NewReader(file)
	users, err := reader.ReadAll()
	if err != nil {
		return payday_error.Notef(err, "Notification: Failed while reading file as CSV: %s", filePath)
	}

	for _, line := range users {
		if len(line) == 0 {
			return payday_error.Notef(err, "Notification: Error loading users in notification cache -- a blank line was passed in as a TUID")
		}

		n.usersLookup[line[0]] = nil
	}

	return nil
}

func (n *Notification) InExperimentGroup(userId string) bool {
	_, isOnList := n.usersLookup[userId]

	return isOnList
}
