package middleware

import (
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

func testOrigin(t *testing.T, origin string, expected bool) {
	middleware := RequestValidationMiddleware{
		shouldValidateOrigin: true,
	}
	request, _ := http.NewRequest("POST", "/test", nil)
	request.Header.Add("Origin", origin)
	assert.Equal(t, expected, middleware.validateOrigin(request))
}

func testContentType(t *testing.T, contentType string, expected bool) {
	middleware := RequestValidationMiddleware{
		shouldValidateContentType: true,
	}
	request, _ := http.NewRequest("POST", "/test", nil)
	request.Header.Add("Content-Type", contentType)
	assert.Equal(t, expected, middleware.validateContentType(request))
}

func TestValidateOrigin(t *testing.T) {
	// Twitch TV
	testOrigin(t, "https://api.twitch.tv", true)

	// Twitch TV Slash Stuff
	testOrigin(t, "https://api.twitch.tv/stuff", true)

	// Twitch TV Slash More Stuff
	testOrigin(t, "https://api.twitch.tv/stuff/and/more/stuff", true)

	// Justin TV
	testOrigin(t, "https://api.justin.tv", true)

	// Jenkins
	testOrigin(t, "https://jenkins-master-0.prod.us-west2.justin.tv", true)

	// Staging
	testOrigin(t, "https://staging-ffa-13-api.dev.us-west2.twitch.tv", true)

	// Whitespace surrounding
	testOrigin(t, "     https://api.twitch.tv     ", true)

	// Empty
	testOrigin(t, "", true)

	// Whitespace
	testOrigin(t, "  \t \n   ", true)

	// Garbage
	testOrigin(t, "asdfdsfsd", false)

	// Malicious
	testOrigin(t, "https://api.nottwitch.tv", false)
}

func TestValidateContentType(t *testing.T) {
	// Just type
	testContentType(t, "application/json", true)
	testContentType(t, "multipart/form-data", true)

	// Whitespace
	testContentType(t, "     application/json    ", true)

	// Whitespace + semicolon
	testContentType(t, "     application/json;    ", true)

	// Extra stuff after
	testContentType(t, "    application/json;  afasf=1", true)

	// Extra stuff before and after
	testContentType(t, "  fasdfds=1;  application/json;  afasf=1;  ", false)

	// Garbage before
	testContentType(t, "asdsda=1 application/json; ", false)

	// Garbage after
	testContentType(t, "application/json asdad=1; ", false)

	// Text plain
	testContentType(t, "text/plain; ", false)
}
