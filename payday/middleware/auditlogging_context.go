package middleware

import (
	"net/http"

	"code.justin.tv/commerce/gogogadget/audit"
	"github.com/zenazn/goji/web"
)

type AuditLoggingContextMiddleware struct {
}

func NewAuditLoggingContextMiddleware() *AuditLoggingContextMiddleware {
	return &AuditLoggingContextMiddleware{}
}

func (mdl *AuditLoggingContextMiddleware) AttachRequestInfoToContext(c *web.C, h http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		// Twirp requests are already handled
		if !isTwirpRequest(r) {
			h = audit.NewMuxWrapper(h)
		}
		h.ServeHTTP(w, r)
	}
	return http.HandlerFunc(fn)
}
