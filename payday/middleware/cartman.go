package middleware

import (
	"context"
	"net/http"

	"github.com/zenazn/goji/web"
)

type CartmanMiddleware interface {
	WithCartmanToken(c *web.C, next http.Handler) http.Handler
}

func NewCartmanMiddlware() CartmanMiddleware {
	return &cartmanMiddleware{}
}

type cartmanMiddleware struct{}

func (m *cartmanMiddleware) WithCartmanToken(c *web.C, next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if len(r.Header["Twitch-Authorization"]) == 0 {
			next.ServeHTTP(w, r)
			return
		}

		jwt := r.Header["Twitch-Authorization"][0]
		ctx := context.WithValue(r.Context(), "Twitch-Authorization", jwt)
		r = r.WithContext(ctx)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}
