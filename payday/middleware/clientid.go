package middleware

import (
	"context"
	"net/http"

	"code.justin.tv/commerce/payday/cartman/decoder/selector"
	"code.justin.tv/commerce/payday/utils/strings"
	goauth "code.justin.tv/common/goauthorization"
	"code.justin.tv/foundation/twitchclient"
)

const (
	ClientIDContextKey     = "ClientIDContextKey"
	CartmanTokenContextKey = "CartmanTokenContextKey"
	clientIDHeader         = "Client-ID"
	clientIDQueryParam     = "client_id"
)

type ClientIdMiddleware struct {
	DecoderSelector selector.DecoderSelector `inject:""`
}

func (mdl *ClientIdMiddleware) GetClientId(h http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		var ctx context.Context
		var token *goauth.AuthorizationToken
		decoder, err := mdl.DecoderSelector.SelectDecoderFromRequest(r)
		if err == nil && decoder != nil {
			token, err = decoder.ParseToken(r)
		}

		requestContext := r.Context()
		if err == nil && token != nil && strings.NotBlank(token.GetClientID()) {
			newContext := context.WithValue(requestContext, ClientIDContextKey, token.GetClientID())
			newContext = context.WithValue(newContext, CartmanTokenContextKey, token)
			ctx = newContext
		} else if strings.NotBlank(r.Header.Get(clientIDHeader)) {
			ctx = context.WithValue(requestContext, ClientIDContextKey, r.Header.Get(clientIDHeader))
		} else if strings.NotBlank(r.Header.Get(twitchclient.TwitchClientIDHeader)) {
			ctx = context.WithValue(requestContext, ClientIDContextKey, r.Header.Get(twitchclient.TwitchClientIDHeader))
		} else if strings.NotBlank(r.URL.Query().Get(clientIDQueryParam)) {
			ctx = context.WithValue(requestContext, ClientIDContextKey, r.URL.Query().Get(clientIDQueryParam))
		} else {
			ctx = requestContext
		}

		h.ServeHTTP(w, r.WithContext(ctx))
	}

	return http.HandlerFunc(fn)
}

func GetClientID(ctx context.Context) string {
	tmp := ctx.Value(ClientIDContextKey)

	if tmp == nil {
		return ""
	}

	clientId, ok := tmp.(string)
	if !ok {
		return ""
	}

	return clientId
}

func CopyContextValues(ctx context.Context) (context.Context, context.CancelFunc) {
	output, cancel := context.WithCancel(context.Background())
	clientId := ctx.Value(ClientIDContextKey)
	output = context.WithValue(output, ClientIDContextKey, clientId)
	return output, cancel
}
