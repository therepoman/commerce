package middleware

import (
	"fmt"
	"net/http"
	"strings"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/api/apidef"
	"code.justin.tv/commerce/payday/metrics"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	prism_tenant "code.justin.tv/commerce/prism/rpc/tenant"
	"github.com/felixge/httpsnoop"
	"github.com/zenazn/goji/web"
)

type MetricsMiddleware struct {
	Statter metrics.Statter `inject:""`
}

func NewMetricsMiddleware() *MetricsMiddleware {
	return &MetricsMiddleware{}
}

func isTwirpRequest(r *http.Request) bool {
	if r == nil || r.URL == nil {
		return false
	}
	return strings.Contains(r.URL.Path, paydayrpc.PaydayPathPrefix) || strings.Contains(r.URL.Path, prism_tenant.PrismTenantPathPrefix)
}

func getTwirpMetricName(r *http.Request) string {
	if r == nil || r.URL == nil {
		return ""
	}
	return sanitizeMetricName(r.URL.Path)
}

func sanitizeMetricName(metricName string) string {
	//Remove payday path prefix
	metricName = strings.ReplaceAll(metricName, paydayrpc.PaydayPathPrefix, "")

	//Remove prism tenant path prefix
	metricName = strings.ReplaceAll(metricName, prism_tenant.PrismTenantPathPrefix, "/")

	return fmt.Sprintf("TWIRP:%s", metricName)
}

func (m *MetricsMiddleware) Metrics(c *web.C, next http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		metrics := httpsnoop.CaptureMetrics(next, w, r)

		namedHandler, matchPattern, isNamed, err := apidef.GetNamedHandler(c)
		if err != nil {
			log.WithError(err).Error("Error getting named handler in metrics middleware")
			return
		}

		var metricName string
		if isNamed {
			metricName = string(namedHandler.GetHandlerName())
		} else if isTwirpRequest(r) {
			metricName = getTwirpMetricName(r)
		} else {
			metricName = fmt.Sprintf("%s:%s", r.Method, matchPattern)
		}

		m.Statter.TimingDuration(metricName, metrics.Duration)
		m.Statter.Inc(fmt.Sprintf("%s.requests", metricName), 1)
		code := metrics.Code
		switch {
		case code < 200:
			m.Statter.Inc(fmt.Sprintf("%s.1xx", metricName), 1)
		case code >= 200 && code < 300:
			m.Statter.Inc(fmt.Sprintf("%s.2xx", metricName), 1)
		case code >= 300 && code < 400:
			m.Statter.Inc(fmt.Sprintf("%s.3xx", metricName), 1)
		case code >= 400 && code < 500:
			m.Statter.Inc(fmt.Sprintf("%s.4xx", metricName), 1)
		case code >= 500:
			m.Statter.Inc(fmt.Sprintf("%s.5xx", metricName), 1)
		}

	}
	return http.HandlerFunc(fn)
}
