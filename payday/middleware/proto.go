package middleware

import (
	"encoding/json"
	"net/http"
	"net/http/httputil"
	"strings"

	log "code.justin.tv/commerce/logrus"
	"github.com/zenazn/goji/web"
)

type ForwardedProtoMiddleware struct {
	forwardedProtocolEnforced bool
}

func NewForwardedProtoMiddleware(enforced bool) *ForwardedProtoMiddleware {
	return &ForwardedProtoMiddleware{forwardedProtocolEnforced: enforced}
}

func (this *ForwardedProtoMiddleware) ForwardedProto(c *web.C, h http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		forwarded := r.Header.Get("X-Forwarded-Proto")
		frontEnd := r.Header.Get("Front-End-Https")
		if this.forwardedProtocolEnforced && !(strings.EqualFold(forwarded, "https") || strings.EqualFold(frontEnd, "on")) {
			request, err := httputil.DumpRequest(r, false)
			entry := log.NewEntry(log.StandardLogger())
			if err == nil {
				entry = log.WithField("request", string(request))
			}
			entry.Info("Invalid protocol header")
			httpError := HttpError{Status: http.StatusBadRequest, Error: "Bad Request", Message: "Requests must be made over SSL"}
			js, err := json.Marshal(httpError)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}

			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(http.StatusBadRequest)
			_, err = w.Write(js)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			return
		}

		h.ServeHTTP(w, r)
	}

	return http.HandlerFunc(fn)
}
