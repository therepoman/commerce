package middleware

import (
	"encoding/json"
	"mime"
	"net/http"
	"regexp"
	"strings"

	log "code.justin.tv/commerce/logrus"
	"github.com/zenazn/goji/web"
)

const twitchOriginRegex = "(?i)^\\s*(https:\\/\\/)?(.*\\.)*(twitch|justin)\\.tv(\\/.*)?\\s*$"
const jsonContentType = "application/json"
const multipartFormdataContentType = "multipart/form-data"

var supportedContentTypes = map[string]interface{}{
	jsonContentType:              nil,
	multipartFormdataContentType: nil,
}

var twitchOriginRegexp *regexp.Regexp

func init() {
	var err error
	twitchOriginRegexp, err = regexp.Compile(twitchOriginRegex)
	if err != nil {
		panic(err)
	}
}

type RequestValidationMiddleware struct {
	shouldValidateContentType bool
	shouldValidateOrigin      bool
}

func isValidatedRequest(r *http.Request) bool {
	return strings.EqualFold(r.Method, "POST") || strings.EqualFold(r.Method, "PUT") || strings.EqualFold(r.Method, "PATCH")
}

func NewRequestValidationMiddleware(shouldValidateContentType bool, shouldValidateOrigin bool) *RequestValidationMiddleware {
	return &RequestValidationMiddleware{
		shouldValidateContentType: shouldValidateContentType,
		shouldValidateOrigin:      shouldValidateOrigin,
	}
}

func (this *RequestValidationMiddleware) writeError(w http.ResponseWriter, msg string) {
	httpError := HttpError{Status: http.StatusBadRequest, Error: "Bad Request", Message: msg}
	js, err := json.Marshal(httpError)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	} else {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusBadRequest)
		_, err := w.Write(js)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
	}
}

func (this *RequestValidationMiddleware) validateOrigin(r *http.Request) bool {
	if this.shouldValidateOrigin && isValidatedRequest(r) {
		origin := r.Header.Get("Origin")
		isTwitchOrigin := twitchOriginRegexp.MatchString(origin) || strings.TrimSpace(origin) == ""
		if !isTwitchOrigin {
			log.WithField("origin", origin).Error("Received a request that did not originate from twitch")
		}
		return isTwitchOrigin
	}
	return true
}

func (this *RequestValidationMiddleware) validateContentType(r *http.Request) bool {
	if this.shouldValidateContentType && isValidatedRequest(r) {
		contentType := r.Header.Get("Content-Type")
		mimeType, _, err := mime.ParseMediaType(contentType)
		if err != nil {
			log.WithField("contentType", contentType).Warn("Error parsing content-type")
			return false
		}

		_, supported := supportedContentTypes[mimeType]
		return supported
	}
	return true
}

func (this *RequestValidationMiddleware) RequestValidation(c *web.C, h http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		if this.validateOrigin(r) {
			if !this.validateContentType(r) {
				log.Error("Received a non-json POST / PUT")
				this.writeError(w, "Request must be json or form-data content-type")
				return
			}
		} else {
			log.Error("Received a request that did not originate from twitch")
			this.writeError(w, "Request must originate from twitch")
			return
		}

		h.ServeHTTP(w, r)
	}

	return http.HandlerFunc(fn)
}
