package football

import (
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/utils/strings"
)

type Bouncer interface {
	Bounce(channelID string) bool
}

type bouncer struct {
	Config *config.Configuration `inject:""`
}

func NewBouncer() Bouncer {
	return &bouncer{}
}

func (b *bouncer) Bounce(channelID string) bool {
	if !b.Config.BitsFootballEventEnabled {
		return false
	}

	if strings.Blank(channelID) {
		return false
	}

	// if sponsored cheering is enabled and we don't have a channel whitelist, we let them in.
	if b.Config.BitsFootballEventWhitelist == nil || len(b.Config.BitsFootballEventWhitelist) == 0 {
		return true
	}

	for _, onTheList := range b.Config.BitsFootballEventWhitelist {
		if onTheList == channelID {
			return true
		}
	}

	return false
}
