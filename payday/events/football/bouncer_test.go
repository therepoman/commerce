package football

import (
	"testing"

	"code.justin.tv/commerce/payday/config"
	. "github.com/smartystreets/goconvey/convey"
)

func TestBouncer_Bounce(t *testing.T) {
	Convey("Given a bouncer", t, func() {
		appConfig := &config.Configuration{}

		b := &bouncer{
			Config: appConfig,
		}

		channelID := "123123123"

		Convey("If the bits football event is disabled", func() {
			appConfig.BitsFootballEventEnabled = false

			Convey("When we ask the bouncer to bounce", func() {
				onTheList := b.Bounce(channelID)
				Convey("It should return false", func() {
					So(onTheList, ShouldBeFalse)
				})
			})
		})

		Convey("If the bits football event is enabled", func() {
			appConfig.BitsFootballEventEnabled = true

			Convey("When there is no channel ID (blank)", func() {
				blankChannelID := ""

				Convey("When we ask the bouncer to bounce", func() {
					onTheList := b.Bounce(blankChannelID)
					Convey("It should return false", func() {
						So(onTheList, ShouldBeFalse)
					})
				})
			})

			Convey("When there is no channel whitelist", func() {
				appConfig.BitsFootballEventWhitelist = nil

				Convey("When we ask the bouncer to bounce", func() {
					onTheList := b.Bounce(channelID)
					Convey("It should return true", func() {
						So(onTheList, ShouldBeTrue)
					})
				})
			})

			Convey("When the channel whitelist is empty", func() {
				appConfig.BitsFootballEventWhitelist = []string{}

				Convey("When we ask the bouncer to bounce", func() {
					onTheList := b.Bounce(channelID)
					Convey("It should return true", func() {
						So(onTheList, ShouldBeTrue)
					})
				})
			})

			Convey("When the channel whitelist is not empty", func() {
				appConfig.BitsFootballEventWhitelist = []string{channelID}

				Convey("When we ask the bouncer to bounce a channel not on the list", func() {
					onTheList := b.Bounce("456456456")
					Convey("It should return false", func() {
						So(onTheList, ShouldBeFalse)
					})
				})

				Convey("When we ask the bouncer to bounce a channel on the list", func() {
					onTheList := b.Bounce(channelID)
					Convey("It should return true", func() {
						So(onTheList, ShouldBeTrue)
					})
				})
			})
		})
	})
}
