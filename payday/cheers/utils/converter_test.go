package utils

import (
	"testing"

	"code.justin.tv/commerce/payday/actions"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/models/api"
	"github.com/stretchr/testify/assert"
)

func TestConvertDynamoActionTypeToCheerType(t *testing.T) {
	inputs := []struct {
		prefix           string
		dynamoCheerType  string
		desiredCheerType models.CheerType
	}{
		{
			dynamoCheerType:  api.DefaultAction,
			desiredCheerType: models.CheerTypeDefaultCheer,
		},
		{
			dynamoCheerType:  api.CharityAction,
			desiredCheerType: models.CheerTypeCharity,
		},
		{
			dynamoCheerType:  api.ChannelCustomAction,
			desiredCheerType: models.CheerTypeChannelCustom,
		},
		{
			dynamoCheerType:  api.GlobalFirstPartyAction,
			desiredCheerType: models.CheerTypeGlobalFirstParty,
		},
		{
			dynamoCheerType:  api.GlobalThirdPartyAction,
			desiredCheerType: models.CheerTypeGlobalThirdParty,
		},
		{
			dynamoCheerType:  api.SponsoredAction,
			desiredCheerType: models.CheerTypeSponsored,
		},
		{
			dynamoCheerType:  api.AnonymousAction,
			desiredCheerType: models.CheerTypeAnonymousCheer,
		},
		{
			dynamoCheerType:  api.DisplayOnlyAction,
			desiredCheerType: models.CheerTypeDisplayOnly,
		},
		{
			dynamoCheerType:  "anything else",
			desiredCheerType: models.CheerTypeDisplayOnly,
		},
		{
			// The default Cheer Prefix should be converted to DefaultCheer for V2
			prefix:           actions.CheerPrefix,
			dynamoCheerType:  api.GlobalFirstPartyAction,
			desiredCheerType: models.CheerTypeDefaultCheer,
		},
		{
			// The anonymous Cheer Prefix should be converted to AnonymousCheer for V2
			prefix:           actions.AnonymousCheerPrefix,
			dynamoCheerType:  api.DisplayOnlyAction,
			desiredCheerType: models.CheerTypeAnonymousCheer,
		},
	}

	for _, input := range inputs {
		cheerType := convertDynamoActionTypeToCheerType(input.prefix, input.dynamoCheerType)
		assert.Equal(t, input.desiredCheerType, cheerType)
	}
}
