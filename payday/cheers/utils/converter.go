package utils

import (
	"fmt"

	"code.justin.tv/commerce/payday/actions"
	"code.justin.tv/commerce/payday/actions/constants"
	"code.justin.tv/commerce/payday/actions/utils"
	dynamo "code.justin.tv/commerce/payday/dynamo/actions"
	"code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/campaigns"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/models/api"
)

const (
	pogChampCheermotePrefix = "pogchamp"
	doodleCheerPrefix       = "doodlecheer"
)

var sixTierCheers = []string{
	actions.CheerPrefix,
	doodleCheerPrefix,
}

func ConvertDynamoActionToCheer(dynamoAction *dynamo.Action) models.Cheer {
	var actionType models.CheerType
	// If an action is no longer active, force it to use the display-only type
	if !dynamoAction.IsActive() {
		actionType = models.CheerTypeDisplayOnly
	} else {
		actionType = convertDynamoActionTypeToCheerType(dynamoAction.Prefix, dynamoAction.Type)
	}

	action := models.Cheer{
		Prefix: dynamoAction.CanonicalCasing,
		Type:   actionType,
		Tiers:  BuildTiers(dynamoAction.Prefix, actionType),
	}
	return action
}

func BuildTiers(prefix string, cheerType models.CheerType) []models.CheerTier {
	// All cheers have the default tiers
	tiers := buildDefaultTiers(cheerType)

	// Pogchamp has some special tiers
	if prefix == pogChampCheermotePrefix {
		tiers = append(tiers, buildPogChampTiers(cheerType)...)
	}

	// Some Cheers also have a higher sixth tier
	for _, sixTierCheerPrefix := range sixTierCheers {
		if prefix == sixTierCheerPrefix {
			tiers = append(tiers, buildSixTier(cheerType)...)
		}
	}
	return tiers
}

func getTemplate(cheerFolder string) string {
	return fmt.Sprintf(models.CDN_TEMPLATE, cheerFolder, models.PrefixTemplateArg, models.BackgroundTemplateArg, models.AnimationTemplateArg, models.TierTemplateArg, models.ScaleTemplateArg, models.ExtensionTemplateArg)
}

func GetChannelCustomTemplate(channelId, imageSetId string) string {
	return fmt.Sprintf(models.CHANNEL_CUSTOM_CHEER_CDN_TEMPLATE, models.PartnerActionsFolder, channelId, imageSetId, models.TierTemplateArg, models.BackgroundTemplateArg, models.AnimationTemplateArg, models.ScaleTemplateArg, models.ExtensionTemplateArg)
}

func GetSponsoredActionTemplate() string {
	return getTemplate(models.SponsoredActionsFolder)
}

func GetGlobalActionTemplate() string {
	return getTemplate(models.GlobalActionsFolder)
}

func BuildDefaultCheerOrder() []models.CheerType {
	return []models.CheerType{
		models.CheerTypeSponsored,
		models.CheerTypeDefaultCheer,
		models.CheerTypeChannelCustom,
		models.CheerTypeCharity,
		models.CheerTypeGlobalFirstParty,
		models.CheerTypeGlobalThirdParty,
		models.CheerTypeAnonymousCheer,
		models.CheerTypeDisplayOnly,
	}
}

// The default scales
func BuildDefaultScales() []api.Scale {
	return []api.Scale{
		constants.Scale1,
		constants.Scale15,
		constants.Scale2,
		constants.Scale3,
		constants.Scale4,
	}
}

// The default backgrounds
func BuildDefaultBackgrounds() []api.Background {
	return []api.Background{
		constants.LightBackground,
		constants.DarkBackground,
	}
}

// The default DisplayType
func BuildDefaultDisplayTypes() []models.DisplayType {
	return []models.DisplayType{
		{
			AnimationType: constants.StaticAnimationType,
			Extension:     constants.StaticExtensionType,
		},
		{
			AnimationType: constants.AnimatedAnimationType,
			Extension:     constants.AnimatedExtensionType,
		},
	}
}

// Adds the Campaign information to the corresponding Cheer
func AddCampaignToCheer(cheer *models.Cheer, campaign *campaigns.SponsoredCheermoteCampaign, userUsedBits int64) {
	if cheer != nil && campaign != nil {
		cheer.Campaign = &models.CheerCampaign{
			ID:                        campaign.CampaignID,
			StartTime:                 campaign.StartTime,
			EndTime:                   campaign.EndTime,
			TotalBits:                 campaign.TotalBits,
			UsedBits:                  campaign.UsedBits,
			IsEnabled:                 campaign.IsEnabled,
			SponsoredAmountThresholds: campaign.SponsoredAmountThresholds,
			MinBitsToBeSponsored:      campaign.MinBitsToBeSponsored,
			UserLimit:                 campaign.UserLimit,
			UserUsedBits:              userUsedBits,
			BrandName:                 campaign.BrandName,
			BrandImageURL:             campaign.BrandImageURL,
		}
	}
}

// The default tiers
func buildDefaultTiers(cheerType models.CheerType) []models.CheerTier {
	return buildTierLevels(cheerType, 1, 100, 1000, 5000, 10000)
}

// Pogchamp has special tiers
func buildPogChampTiers(cheerType models.CheerType) []models.CheerTier {
	return buildTierLevels(cheerType, 30000, 50000, 70000, 90000)
}

// Some emotes have a higher sixth tier
func buildSixTier(cheerType models.CheerType) []models.CheerTier {
	return buildTierLevels(cheerType, 100000)
}

// Checks if the tier should be shown in the bits card
func shouldShowInBitsCard(cheerType models.CheerType, cheerAmount int32) bool {
	return cheerAmount <= utils.MaximumShownInBitsCard && cheerType != models.CheerTypeDisplayOnly && cheerType != models.CheerTypeGlobalThirdParty
}

// Convert a CheerType + amounts into a list of CheerTiers
func buildTierLevels(cheerType models.CheerType, cheerAmounts ...int32) []models.CheerTier {
	var tiers []models.CheerTier
	for _, cheerAmount := range cheerAmounts {
		tiers = append(tiers, models.CheerTier{
			MinBits:        cheerAmount,
			ShowInBitsCard: shouldShowInBitsCard(cheerType, cheerAmount),
		})
	}
	return tiers
}

// Need to convert the Dynamo ActionType to a CheerType
func convertDynamoActionTypeToCheerType(prefix, dynamoType string) models.CheerType {
	// This is configured as GlobalFirstPartyAction and cannot be changed until everyone moves to Actions API v2
	if prefix == actions.CheerPrefix && dynamoType == api.GlobalFirstPartyAction {
		return models.CheerTypeDefaultCheer
	}

	// This is configured as DisplayOnlyAction and cannot be changed until everyone moves to Actions API v2
	if prefix == actions.AnonymousCheerPrefix && dynamoType == api.DisplayOnlyAction {
		return models.CheerTypeAnonymousCheer
	}

	switch dynamoType {
	case api.DefaultAction:
		return models.CheerTypeDefaultCheer
	case api.CharityAction:
		return models.CheerTypeCharity
	case api.ChannelCustomAction:
		return models.CheerTypeChannelCustom
	case api.GlobalFirstPartyAction:
		return models.CheerTypeGlobalFirstParty
	case api.GlobalThirdPartyAction:
		return models.CheerTypeGlobalThirdParty
	case api.SponsoredAction:
		return models.CheerTypeSponsored
	case api.AnonymousAction:
		return models.CheerTypeAnonymousCheer
	case api.DisplayOnlyAction:
		return models.CheerTypeDisplayOnly
	// If we don't match one of these, return DisplayOnly for safety
	default:
		return models.CheerTypeDisplayOnly
	}
}
