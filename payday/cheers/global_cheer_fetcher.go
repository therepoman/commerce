package cheers

import (
	"code.justin.tv/commerce/gogogadget/strings"
	dynamo "code.justin.tv/commerce/payday/dynamo/actions"
)

type Fetcher interface {
	FetchGlobalCheers(prefixes []string) (map[string]*dynamo.Action, error)
}

type fetcher struct {
	Dao dynamo.IActionDao `inject:""`
}

func NewGlobalCheerFetcher() Fetcher {
	return &fetcher{}
}

func (f *fetcher) FetchGlobalCheers(prefixes []string) (map[string]*dynamo.Action, error) {
	records := make(map[string]*dynamo.Action)
	if len(prefixes) > 0 {
		dynamoActions, err := f.Dao.GetBatch(deduplicatePrefixes(prefixes))
		if err != nil {
			return nil, err
		}

		for _, action := range dynamoActions {
			if action != nil {
				records[action.Prefix] = action
			}
		}
	}

	return records, nil
}

func deduplicatePrefixes(prefixes []string) []string {
	set := strings.NewSet()
	for _, prefix := range prefixes {
		set.Add(prefix)
	}
	return set.GetList()
}
