package cheers

import (
	"context"
	"testing"

	"code.justin.tv/commerce/payday/actions/constants"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/image"
	emote_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/emote"
	image_manager_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/image_manager"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/models/api"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

const (
	testImageSetId  = "testImageSetId"
	testChannelId   = "testChannelId"
	testCheerAction = "testCheerAction"
	testTemplate    = "https://d3aqoihi2n8ty8.cloudfront.net/partner-actions/testChannelId/testImageSetId/TIER/BACKGROUND/ANIMATION/SCALE.EXTENSION"
)

func TestChannelCustomCheermoteGetter(t *testing.T) {
	Convey("Given a ChannelCustomCheermoteGetter", t, func() {
		prefixManager := new(emote_mock.IPrefixManager)
		imageManager := new(image_manager_mock.IImageManager)
		getter := channelCustomCheermoteGetter{
			PrefixManager: prefixManager,
			ImageManager:  imageManager,
		}
		Convey("passed a nil channelinfo", func() {
			ctx := context.Background()
			_, err := getter.GetCustomCheermote(ctx, testChannelId, nil)
			So(err, ShouldBeError)
		})
		Convey("passed a channelinfo with no CustomCheermoteImageSetId", func() {
			ctx := context.Background()
			resp, err := getter.GetCustomCheermote(ctx, testChannelId, &dynamo.Channel{})
			So(resp, ShouldBeNil)
			So(err, ShouldBeNil)
		})
		Convey("passed a channelinfo which is disabled", func() {
			ctx := context.Background()
			imageSet := testImageSetId
			status := api.CustomCheermoteStatusDisabled

			resp, err := getter.GetCustomCheermote(ctx, testChannelId, &dynamo.Channel{
				CustomCheermoteImageSetId: &imageSet,
				CustomCheermoteStatus:     &status,
			})
			So(resp, ShouldBeNil)
			So(err, ShouldBeNil)
		})
		Convey("passed a channelinfo which is moderated", func() {
			ctx := context.Background()
			imageSet := testImageSetId
			status := api.CustomCheermoteStatusModerated

			resp, err := getter.GetCustomCheermote(ctx, testChannelId, &dynamo.Channel{
				CustomCheermoteImageSetId: &imageSet,
				CustomCheermoteStatus:     &status,
			})
			So(resp, ShouldBeNil)
			So(err, ShouldBeNil)
		})
		Convey("passed a channelinfo which is enabled, but GetCustomCheermoteAction error", func() {
			ctx := context.Background()
			imageSet := testImageSetId
			status := api.CustomCheermoteStatusEnabled

			prefixManager.On("GetCustomCheermoteAction", mock.Anything, testChannelId).Return("", false, errors.New("test"))

			resp, err := getter.GetCustomCheermote(ctx, testChannelId, &dynamo.Channel{
				CustomCheermoteImageSetId: &imageSet,
				CustomCheermoteStatus:     &status,
			})
			So(resp, ShouldBeNil)
			So(err, ShouldBeError)
		})
		Convey("passed a channelinfo which is enabled, but doesn't have CustomCheermoteAction ", func() {
			ctx := context.Background()
			imageSet := testImageSetId
			status := api.CustomCheermoteStatusEnabled

			prefixManager.On("GetCustomCheermoteAction", mock.Anything, testChannelId).Return("", false, nil)

			resp, err := getter.GetCustomCheermote(ctx, testChannelId, &dynamo.Channel{
				CustomCheermoteImageSetId: &imageSet,
				CustomCheermoteStatus:     &status,
			})
			So(resp, ShouldBeNil)
			So(err, ShouldBeNil)
		})
		Convey("passed a channelinfo which is enabled, has a CustomCheermoteAction ", func() {
			ctx := context.Background()
			imageSet := testImageSetId
			status := api.CustomCheermoteStatusEnabled

			prefixManager.On("GetCustomCheermoteAction", mock.Anything, testChannelId).Return(testCheerAction, true, nil)
			Convey("with an error from image manager", func() {
				imageManager.On("GetImages", mock.Anything, testImageSetId).Return(nil, errors.New(""))

				resp, err := getter.GetCustomCheermote(ctx, testChannelId, &dynamo.Channel{
					CustomCheermoteImageSetId: &imageSet,
					CustomCheermoteStatus:     &status,
				})

				So(resp, ShouldBeNil)
				So(err, ShouldBeError)
			})
			Convey("with not all images set", func() {
				imageManager.On("GetImages", mock.Anything, testImageSetId).Return(buildIncompleteImageMap(), nil)

				resp, err := getter.GetCustomCheermote(ctx, testChannelId, &dynamo.Channel{
					CustomCheermoteImageSetId: &imageSet,
					CustomCheermoteStatus:     &status,
				})

				So(resp, ShouldBeNil)
				So(err, ShouldBeNil)
			})
			Convey("with all images set", func() {
				imageManager.On("GetImages", mock.Anything, testImageSetId).Return(buildCompleteImageMap(), nil)

				resp, err := getter.GetCustomCheermote(ctx, testChannelId, &dynamo.Channel{
					CustomCheermoteImageSetId: &imageSet,
					CustomCheermoteStatus:     &status,
				})

				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
				So(resp.Template, ShouldEqual, testTemplate)
				So(resp.Cheers, ShouldHaveLength, 1)
				So(resp.Cheers[0].Prefix, ShouldEqual, testCheerAction)
				So(resp.Cheers[0].Campaign, ShouldBeNil)
				So(resp.Cheers[0].Type, ShouldEqual, models.CheerTypeChannelCustom)
			})
		})
	})
}

func buildIncompleteImageMap() image.ImageMap {
	return image.ImageMap{
		constants.Tier1: {
			constants.LightBackground: {
				constants.AnimatedAnimationType: {
					constants.Scale1: &dynamo.Image{},
				},
			},
		},
	}
}

func buildCompleteImageMap() image.ImageMap {
	imageMap := make(map[api.Tier]map[api.Background]map[api.AnimationType]map[api.Scale]*dynamo.Image)
	for tier := range constants.Tiers {
		backgroundMap := make(map[api.Background]map[api.AnimationType]map[api.Scale]*dynamo.Image)
		for background := range constants.Backgrounds {
			animationMap := make(map[api.AnimationType]map[api.Scale]*dynamo.Image)
			for animation := range constants.AnimationTypes {
				scaleMap := make(map[api.Scale]*dynamo.Image)
				for scale := range constants.Scales {
					scaleMap[scale] = &dynamo.Image{}
				}
				animationMap[animation] = scaleMap
			}
			backgroundMap[background] = animationMap
		}
		imageMap[tier] = backgroundMap
	}
	return imageMap
}
