package cheers

import (
	"context"
	"errors"
	"testing"
	"time"

	dynamo_actions "code.justin.tv/commerce/payday/dynamo/actions"
	"code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/campaigns"
	dynamo_campaigns "code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/campaigns"
	actions_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sponsored_cheermote/actions"
	campaign_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sponsored_cheermote/campaign"
	channel_status_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sponsored_cheermote/channel_status"
	user_campaign_status_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sponsored_cheermote/user_campaign_status"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

const (
	testUserId             = "testUserId"
	sponsoredCheerTemplate = "https://d3aqoihi2n8ty8.cloudfront.net/sponsored-actions/PREFIX/BACKGROUND/ANIMATION/TIER/SCALE.EXTENSION"
)

func TestSponsoredCheermoteGetter(t *testing.T) {
	Convey("Given a SponsoredCheermoteGetter", t, func() {
		sponsoredCheermoteManager := new(campaign_mock.Manager)
		sponsoredCheermoteFilterer := new(channel_status_mock.Filterer)
		sponsoredActionsGetter := new(actions_mock.Getter)
		sponsoredUserGetter := new(user_campaign_status_mock.Getter)

		sponsoredCheermoteGetter := sponsoredCheermoteGetter{
			SponsoredCheermoteManager:    sponsoredCheermoteManager,
			SponsoredCheermoteFilterer:   sponsoredCheermoteFilterer,
			SponsoredActionsGetter:       sponsoredActionsGetter,
			SponsoredCheermoteUserGetter: sponsoredUserGetter,
		}

		campaign1 := &dynamo_campaigns.SponsoredCheermoteCampaign{
			CampaignID:           "C1",
			Prefix:               "p1",
			IsEnabled:            true,
			EndTime:              time.Date(3018, 1, 1, 0, 0, 0, 0, time.UTC),
			StartTime:            time.Date(2018, 1, 1, 0, 0, 0, 0, time.UTC),
			UsedBits:             int64(100),
			TotalBits:            int64(10000),
			MinBitsToBeSponsored: int64(10),
			UserLimit:            int64(1000),
		}
		campaign2 := &dynamo_campaigns.SponsoredCheermoteCampaign{
			CampaignID:           "C2",
			Prefix:               "p2",
			IsEnabled:            true,
			EndTime:              time.Date(3018, 1, 1, 0, 0, 0, 0, time.UTC),
			StartTime:            time.Date(2018, 1, 1, 0, 0, 0, 0, time.UTC),
			UsedBits:             int64(100),
			TotalBits:            int64(10000),
			MinBitsToBeSponsored: int64(10),
			UserLimit:            int64(10),
		}
		campaign3 := &dynamo_campaigns.SponsoredCheermoteCampaign{
			CampaignID:           "C3",
			Prefix:               "p3",
			IsEnabled:            true,
			EndTime:              time.Date(3018, 1, 1, 0, 0, 0, 0, time.UTC),
			StartTime:            time.Date(2018, 1, 1, 0, 0, 0, 0, time.UTC),
			UsedBits:             int64(100),
			TotalBits:            int64(10000),
			MinBitsToBeSponsored: int64(10),
			UserLimit:            int64(100),
		}
		campaign4 := &dynamo_campaigns.SponsoredCheermoteCampaign{
			CampaignID:           "C4-copy of C3 but disabled",
			Prefix:               "p3",
			IsEnabled:            false,
			EndTime:              time.Date(3018, 1, 1, 0, 0, 0, 0, time.UTC),
			StartTime:            time.Date(2018, 1, 1, 0, 0, 0, 0, time.UTC),
			UsedBits:             int64(100),
			TotalBits:            int64(10000),
			MinBitsToBeSponsored: int64(10),
			UserLimit:            int64(100),
		}

		Convey("when SponsoredCheermoteManager return 0 campaigns", func() {
			sponsoredCheermoteManager.On("GetAllCampaigns", mock.Anything).Return([]*campaigns.SponsoredCheermoteCampaign{})
			resp, err := sponsoredCheermoteGetter.GetSponsoredCheers(context.Background(), testChannelId, testUserId)
			So(err, ShouldBeNil)
			So(resp, ShouldBeNil)
		})
		Convey("when SponsoredCheermoteManager campaigns", func() {
			campaigns := []*dynamo_campaigns.SponsoredCheermoteCampaign{campaign1, campaign2, campaign3, campaign4}
			sponsoredCheermoteManager.On("GetAllCampaigns", mock.Anything).Return(campaigns, nil)

			Convey("When FilterCampaignsByChannel errors", func() {
				sponsoredCheermoteFilterer.On("Filter", mock.Anything, campaigns, testChannelId).Return(nil, errors.New("test error"))
				Convey("Handler should error", func() {
					_, err := sponsoredCheermoteGetter.GetSponsoredCheers(context.Background(), testChannelId, testUserId)
					So(err, ShouldBeError)
				})
			})
			Convey("When FilterCampaignsByChannel succeeds", func() {
				filteredCampaigns := []*dynamo_campaigns.SponsoredCheermoteCampaign{campaign1, campaign2, campaign3}
				sponsoredCheermoteFilterer.On("Filter", mock.Anything, campaigns, testChannelId).Return(filteredCampaigns, nil)

				Convey("When GetActionsFromCampaigns errors", func() {
					sponsoredActionsGetter.On("GetBatch", mock.Anything, campaigns).Return(nil, errors.New("test error"))

					Convey("Handler should error", func() {
						_, err := sponsoredCheermoteGetter.GetSponsoredCheers(context.Background(), testChannelId, testUserId)
						So(err, ShouldBeError)
					})
				})

				Convey("When GetActionsFromCampaigns succeeds with a single campaigns", func() {
					dynamoActions := map[string]*dynamo_actions.Action{
						"p1": {Prefix: "p1", CanonicalCasing: "P1"},
					}
					sponsoredActionsGetter.On("GetBatch", mock.Anything, campaigns).Return(dynamoActions, nil)

					Convey("When user has contributed", func() {
						sponsoredUserGetter.On("Get", mock.Anything, "C1", mock.Anything).Return(int64(1000000000), nil)

						Convey("When bonus campaign retrieval errors ", func() {
							sponsoredActionsGetter.On("GetBonus", mock.Anything).Return(nil, errors.New("anime-bears-not-allowed-error"))

							sponsoredCheerGroup, err := sponsoredCheermoteGetter.GetSponsoredCheers(context.Background(), testChannelId, testUserId)
							So(err, ShouldBeNil)
							So(sponsoredCheerGroup, ShouldNotBeNil)
							So(sponsoredCheerGroup.Template, ShouldEqual, sponsoredCheerTemplate)
							So(sponsoredCheerGroup.Cheers, ShouldHaveLength, 1)
							So(sponsoredCheerGroup.Cheers[0].Campaign, ShouldNotBeNil)
						})

						Convey("When bonus campaign retrieval works ", func() {
							sponsoredActionsGetter.On("GetBonus", mock.Anything).Return(&dynamo_actions.Action{
								Prefix:          "bonus",
								CreatedTime:     time.Now(),
								Type:            "display_only",
								Support100K:     false,
								LastUpdated:     time.Now(),
								CanonicalCasing: "bonus",
								StartTime:       time.Now(),
								EndTime:         nil,
								IsCharitable:    false,
							}, nil)

							sponsoredCheerGroup, err := sponsoredCheermoteGetter.GetSponsoredCheers(context.Background(), testChannelId, testUserId)
							So(err, ShouldBeNil)
							So(sponsoredCheerGroup, ShouldNotBeNil)
							So(sponsoredCheerGroup.Template, ShouldEqual, sponsoredCheerTemplate)
							So(sponsoredCheerGroup.Cheers, ShouldHaveLength, 2)
							So(sponsoredCheerGroup.Cheers[0].Campaign, ShouldNotBeNil)
							So(sponsoredCheerGroup.Cheers[0].Prefix, ShouldEqual, "P1")
							So(sponsoredCheerGroup.Cheers[1].Prefix, ShouldEqual, "bonus")
						})
					})
				})

				Convey("When GetActionsFromCampaigns succeeds with multiple campaigns", func() {
					dynamoActions := map[string]*dynamo_actions.Action{
						"p1": {Prefix: "p1", CanonicalCasing: "P1"},
						"p2": {Prefix: "p2", CanonicalCasing: "P2"},
					}
					sponsoredActionsGetter.On("GetBatch", mock.Anything, campaigns).Return(dynamoActions, nil)
					sponsoredActionsGetter.On("GetBonus", mock.Anything).Return(nil, errors.New("anime-bears-not-allowed-error"))

					Convey("When user has contributed nothing", func() {
						sponsoredUserGetter.On("Get", mock.Anything, mock.Anything, mock.Anything).Return(int64(0), nil)

						sponsoredCheerGroup, err := sponsoredCheermoteGetter.GetSponsoredCheers(context.Background(), testChannelId, testUserId)
						So(err, ShouldBeNil)
						So(sponsoredCheerGroup, ShouldNotBeNil)
						So(sponsoredCheerGroup.Template, ShouldEqual, sponsoredCheerTemplate)
						So(sponsoredCheerGroup.Cheers, ShouldHaveLength, 2)
						So(sponsoredCheerGroup.Cheers[0].Campaign, ShouldNotBeNil)
						So(sponsoredCheerGroup.Cheers[0].Campaign.UserUsedBits, ShouldEqual, 0)
						So(sponsoredCheerGroup.Cheers[1].Campaign, ShouldNotBeNil)
						So(sponsoredCheerGroup.Cheers[1].Campaign.UserUsedBits, ShouldEqual, 0)
						So(sponsoredCheerGroup.Cheers[0].Campaign.IsEnabled, ShouldBeTrue)
						So(sponsoredCheerGroup.Cheers[1].Campaign.IsEnabled, ShouldBeTrue)
					})

					Convey("When user has contributed", func() {
						sponsoredUserGetter.On("Get", mock.Anything, "C1", mock.Anything).Return(int64(1), nil)
						sponsoredUserGetter.On("Get", mock.Anything, "C2", mock.Anything).Return(int64(10), nil)
						sponsoredUserGetter.On("Get", mock.Anything, "C3", mock.Anything).Return(int64(1000000000), nil)

						sponsoredCheerGroup, err := sponsoredCheermoteGetter.GetSponsoredCheers(context.Background(), testChannelId, testUserId)
						So(err, ShouldBeNil)
						So(sponsoredCheerGroup, ShouldNotBeNil)
						So(sponsoredCheerGroup.Template, ShouldEqual, sponsoredCheerTemplate)
						So(sponsoredCheerGroup.Cheers, ShouldHaveLength, 2)
						So(sponsoredCheerGroup.Cheers[0].Campaign, ShouldNotBeNil)
						So(sponsoredCheerGroup.Cheers[1].Campaign, ShouldNotBeNil)
					})

					Convey("When getting user contribution returns an error", func() {
						sponsoredUserGetter.On("Get", mock.Anything, mock.Anything, mock.Anything).Return(int64(0), errors.New("HORRIBLE ERROR"))

						sponsoredCheerGroup, err := sponsoredCheermoteGetter.GetSponsoredCheers(context.Background(), testChannelId, testUserId)
						So(err, ShouldBeNil)
						So(sponsoredCheerGroup, ShouldNotBeNil)
						So(sponsoredCheerGroup.Template, ShouldEqual, sponsoredCheerTemplate)
						So(sponsoredCheerGroup.Cheers, ShouldHaveLength, 0)
					})
				})
			})
		})
	})
}
