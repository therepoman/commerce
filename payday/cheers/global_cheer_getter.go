package cheers

import (
	"math/rand"
	"sync"
	"time"

	"code.justin.tv/chat/golibs/graceful"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/cheers/utils"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/s3"
	yaml "gopkg.in/yaml.v2"
)

const (
	refreshDelayMaxRandomOffset = time.Second * 30
	refreshDelay                = time.Minute * 5
)

type GlobalCheersPoller interface {
	RefreshGlobalCheersAtInterval()
	GetCheers() models.CheerGroup
	Init() error
}

type globalCheersPoller struct {
	cheerGroup models.CheerGroup
	S3Client   s3.IS3Client `inject:""`
	bucket     string
	key        string
	mutex      *sync.RWMutex
	Fetcher    Fetcher `inject:""`
}

type S3Action struct {
	Prefix string `yaml:"prefix"`
}

func NewGlobalCheersPoller(bucket string, key string) GlobalCheersPoller {
	mutex := &sync.RWMutex{}

	return &globalCheersPoller{
		cheerGroup: models.CheerGroup{},
		mutex:      mutex,
		bucket:     bucket,
		key:        key,
	}
}

func createSupportedPrefixesListFromS3(s3client s3.IS3Client, bucket string, key string) ([]S3Action, error) {
	tagBytes, err := s3client.LoadFile(bucket, key)
	if err != nil {
		log.WithFields(log.Fields{
			"key":    key,
			"bucket": bucket,
		}).Error("could not load file from S3 for SupportedActions", err)
		return nil, err
	}

	return loadActionsYaml(tagBytes)
}

func loadActionsYaml(bytes []byte) ([]S3Action, error) {
	actions := make([]S3Action, 0)
	err := yaml.Unmarshal(bytes, &actions)
	if err != nil {
		return nil, err
	}

	return actions, nil
}

func (sa *globalCheersPoller) refreshActionList() error {
	s3Actions, err := createSupportedPrefixesListFromS3(sa.S3Client, sa.bucket, sa.key)
	if err != nil {
		return err
	}

	cheerGroup, err := sa.getGlobalCheersFromDynamo(s3Actions)
	if err != nil {
		return err
	}

	sa.mutex.Lock()
	defer sa.mutex.Unlock()
	sa.cheerGroup = cheerGroup
	return nil
}

func (sa *globalCheersPoller) getGlobalCheersFromDynamo(supportedActions []S3Action) (models.CheerGroup, error) {
	var prefixes []string
	for _, supportedAction := range supportedActions {
		prefixes = append(prefixes, supportedAction.Prefix)
	}

	dynamoActions, err := sa.Fetcher.FetchGlobalCheers(prefixes)
	if err != nil {
		return models.CheerGroup{}, err
	}

	var cheerGroup models.CheerGroup
	var cheers []models.Cheer

	// This ensures that Cheers are returned in the same order as they are defined in the S3 file
	for _, prefix := range prefixes {
		dynamoAction, ok := dynamoActions[prefix]
		if ok {
			cheers = append(cheers, utils.ConvertDynamoActionToCheer(dynamoAction))
		}
	}

	cheerGroup.Template = utils.GetGlobalActionTemplate()
	cheerGroup.Cheers = cheers
	return cheerGroup, nil
}

func (sa *globalCheersPoller) Init() error {
	if exists, err := sa.S3Client.BucketExists(sa.bucket); !exists || err != nil {
		log.WithError(err).WithField("bucket", sa.bucket).Error("Bucket does not exist for SupportedCheers object creation")
		return err
	}

	if exists, err := sa.S3Client.FileExists(sa.bucket, sa.key); !exists || err != nil {
		log.WithFields(log.Fields{
			"key":    sa.key,
			"bucket": sa.bucket,
		}).WithError(err).Error("Key does not exist in Bucket for SupportedCheers object creation")
		return err
	}

	err := sa.refreshActionList()
	if err != nil {
		log.WithError(err).Error("Error while refreshing Cheer list")
		return err
	}

	return nil
}

func (sa *globalCheersPoller) RefreshGlobalCheersAtInterval() {
	delayOffset := rand.Int63n(int64(refreshDelayMaxRandomOffset))
	timer := time.NewTicker(refreshDelay + time.Duration(delayOffset))

	for {
		select {
		case <-graceful.ShuttingDown():
			return
		case <-timer.C:
			err := sa.refreshActionList()
			if err != nil {
				log.WithError(err).Error("Error refreshing actions")
			}
		}
	}
}

func (sa *globalCheersPoller) GetCheers() models.CheerGroup {
	sa.mutex.RLock()
	defer sa.mutex.RUnlock()
	return sa.cheerGroup
}
