package cheers

import (
	"context"
	"strings"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/cheers/utils"
	"code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/campaigns"
	"code.justin.tv/commerce/payday/models"
	sponsored_actions "code.justin.tv/commerce/payday/sponsored_cheermote/actions"
	"code.justin.tv/commerce/payday/sponsored_cheermote/campaign"
	"code.justin.tv/commerce/payday/sponsored_cheermote/channel_status"
	"code.justin.tv/commerce/payday/sponsored_cheermote/user_campaign_status"
)

type SponsoredCheermoteGetter interface {
	GetSponsoredCheers(ctx context.Context, channelId, userId string) (*models.CheerGroup, error)
}

type sponsoredCheermoteGetter struct {
	SponsoredCheermoteManager    campaign.Manager            `inject:""`
	SponsoredCheermoteFilterer   channel_status.Filterer     `inject:""`
	SponsoredActionsGetter       sponsored_actions.Getter    `inject:""`
	SponsoredCheermoteUserGetter user_campaign_status.Getter `inject:""`
}

func NewSponsoredCheermoteGetter() SponsoredCheermoteGetter {
	return &sponsoredCheermoteGetter{}
}

func (scb *sponsoredCheermoteGetter) GetSponsoredCheers(ctx context.Context, channelId, userId string) (*models.CheerGroup, error) {
	allCampaigns := scb.SponsoredCheermoteManager.GetAllCampaigns(ctx)
	if len(allCampaigns) < 1 {
		return nil, nil
	}

	enabledCampaigns, err := scb.SponsoredCheermoteFilterer.Filter(ctx, allCampaigns, channelId)
	if err != nil {
		return nil, err
	}

	dynamoCampaignCheers, err := scb.SponsoredActionsGetter.GetBatch(ctx, allCampaigns)
	if err != nil {
		return nil, err
	}

	campaignCheers := make([]models.Cheer, 0)
	for _, dynamoCheer := range dynamoCampaignCheers {
		if dynamoCheer == nil {
			continue
		}

		cheer := utils.ConvertDynamoActionToCheer(dynamoCheer)
		campaignForAction := GetCampaignForCheer(cheer.Prefix, allCampaigns, enabledCampaigns)

		if campaignForAction == nil {
			campaignCheers = append(campaignCheers, cheer)
			continue
		}

		if !campaignForAction.IsActive() || !isEnabled(campaignForAction.CampaignID, enabledCampaigns) {
			cheer.Type = models.CheerTypeDisplayOnly
			for i := range cheer.Tiers {
				cheer.Tiers[i].ShowInBitsCard = false
			}

			campaignCheers = append(campaignCheers, cheer)
			continue
		}

		var userUsedBits int64
		if userId != "" {
			userUsedBits, err = scb.SponsoredCheermoteUserGetter.Get(ctx, campaignForAction.CampaignID, userId)
			if err != nil {
				log.WithError(err).WithFields(log.Fields{"campaignID": campaignForAction.CampaignID, "userID": userId}).Error("Failed to retrieve the user contribution against campaign")
				continue
			}
		}
		utils.AddCampaignToCheer(&cheer, campaignForAction, userUsedBits)
		campaignCheers = append(campaignCheers, cheer)
	}

	// Add bonus Cheermote
	bonusDynamoAction, err := scb.SponsoredActionsGetter.GetBonus(ctx)
	if err != nil {
		log.WithError(err).Error("Failed to get bonus cheermote")
	} else {
		bonusCheer := utils.ConvertDynamoActionToCheer(bonusDynamoAction)
		campaignCheers = append(campaignCheers, bonusCheer)
	}

	cheerGroup := models.CheerGroup{
		Template: utils.GetSponsoredActionTemplate(),
		Cheers:   campaignCheers,
	}
	return &cheerGroup, nil
}

func isEnabled(campaignId string, enabledCampaigns []*campaigns.SponsoredCheermoteCampaign) bool {
	for _, campaign := range enabledCampaigns {
		if campaignId == campaign.CampaignID {
			return true
		}
	}
	return false
}

func GetCampaignForCheer(prefix string, allCampaigns, enabledCampaigns []*campaigns.SponsoredCheermoteCampaign) *campaigns.SponsoredCheermoteCampaign {
	// Campaigns can potentially overlap, so we want to make sure we find the one that is most relevant to the user.
	// If we don't find any campaigns, we need to at least return the one that matches the prefix so we can render
	// it for Clips/VODS usecases.
	for _, campaign := range enabledCampaigns {
		if campaign != nil {
			if strings.EqualFold(campaign.Prefix, prefix) {
				return campaign
			}
		}
	}

	for _, campaign := range allCampaigns {
		if campaign != nil {
			if strings.EqualFold(campaign.Prefix, prefix) {
				return campaign
			}
		}
	}
	return nil
}
