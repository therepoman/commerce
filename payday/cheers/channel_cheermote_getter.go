package cheers

import (
	"context"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/cheers/utils"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/emote"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/image"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/models/api"
)

type ChannelCustomCheermoteGetter interface {
	GetCustomCheermote(ctx context.Context, channelId string, channelInfo *dynamo.Channel) (*models.CheerGroup, error)
}

type channelCustomCheermoteGetter struct {
	PrefixManager emote.IPrefixManager `inject:""`
	ImageManager  image.IImageManager  `inject:""`
}

func NewChannelCustomCheermoteGetter() ChannelCustomCheermoteGetter {
	return &channelCustomCheermoteGetter{}
}

func (c channelCustomCheermoteGetter) GetCustomCheermote(ctx context.Context, channelId string, channelInfo *dynamo.Channel) (*models.CheerGroup, error) {
	if channelInfo == nil {
		return nil, errors.New("passed a nil channelInfo to GetCustomCheermote")
	}

	customCheermoteImageSetId := channelInfo.CustomCheermoteImageSetId
	if customCheermoteImageSetId == nil {
		return nil, nil
	}

	customCheermoteStatus := api.DefaultCustomCheermoteStatus
	if channelInfo.CustomCheermoteStatus != nil {
		customCheermoteStatus = *channelInfo.CustomCheermoteStatus
	}

	if customCheermoteStatus != api.CustomCheermoteStatusEnabled {
		return nil, nil
	}

	prefix, hasCustomCheermoteAction, err := c.PrefixManager.GetCustomCheermoteAction(ctx, channelId)
	if err != nil {
		return nil, errors.Notef(err, "Failed to get prefix for channelId: %s", channelId)
	}

	if !hasCustomCheermoteAction {
		return nil, nil
	}

	images, err := c.ImageManager.GetImages(ctx, *customCheermoteImageSetId)
	if err != nil {
		log.WithError(err).Error("Failed to get images")
		return nil, err
	}

	if !images.AllImagesSet() {
		return nil, nil
	}

	cheerGroup := models.CheerGroup{
		Template: utils.GetChannelCustomTemplate(channelId, *customCheermoteImageSetId),
		Cheers: []models.Cheer{
			{
				Prefix: prefix,
				Type:   models.CheerTypeChannelCustom,
				Tiers:  utils.BuildTiers(prefix, models.CheerTypeChannelCustom),
			},
		},
	}
	return &cheerGroup, nil
}
