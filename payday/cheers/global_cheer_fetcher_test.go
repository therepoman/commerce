package cheers

import (
	"testing"
	"time"

	"code.justin.tv/commerce/payday/dynamo/actions"
	"code.justin.tv/commerce/payday/errors"
	dynamo_actions_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo/actions"
	"code.justin.tv/commerce/payday/models/api"
	. "github.com/smartystreets/goconvey/convey"
)

func TestFetcher(t *testing.T) {
	Convey("Given a Fetcher", t, func() {
		dao := new(dynamo_actions_mock.IActionDao)

		f := &fetcher{
			Dao: dao,
		}

		prefix1 := "test1"
		prefix2 := "test2"

		action1 := &actions.Action{
			Prefix:          prefix1,
			CanonicalCasing: "Test1",
			Support100K:     false,
			Type:            api.GlobalFirstPartyAction,
			StartTime:       time.Date(2018, 1, 1, 1, 0, 0, 0, time.UTC),
		}

		action2 := &actions.Action{
			Prefix:          prefix2,
			CanonicalCasing: "Test2",
			Support100K:     false,
			Type:            api.GlobalFirstPartyAction,
			StartTime:       time.Date(2018, 1, 1, 1, 0, 0, 0, time.UTC),
		}
		actionsResult := map[string]*actions.Action{prefix1: action1, prefix2: action2}

		Convey("When we get an error", func() {
			dao.On("GetBatch", []string{prefix1, prefix2}).Return(nil, errors.New(""))
			actual, err := f.FetchGlobalCheers([]string{prefix1, prefix2})

			So(err, ShouldBeError)
			So(actual, ShouldBeNil)

			Convey("We tried to fetch from Dynamo", func() {
				dao.AssertCalled(t, "GetBatch", []string{prefix1, prefix2})
			})
		})

		Convey("When we fetch the list of records", func() {
			dao.On("GetBatch", []string{prefix1, prefix2}).Return(actionsResult, nil)
			actual, err := f.FetchGlobalCheers([]string{prefix1, prefix2})

			So(err, ShouldBeNil)
			So(actionListsEqual(actual, actionsResult), ShouldBeTrue)

			Convey("We fetch from Dynamo", func() {
				dao.AssertCalled(t, "GetBatch", []string{prefix1, prefix2})
			})
		})
	})
}

func TestDeduplicate(t *testing.T) {
	Convey("dedupes with duplicates", t, func() {
		So(deduplicatePrefixes([]string{"sand", "desert", "skywalker", "sand"}), ShouldResemble, []string{"sand", "desert", "skywalker"})
	})

	Convey("dedupes is fine with no duplicates", t, func() {
		So(deduplicatePrefixes([]string{"ig-88"}), ShouldResemble, []string{"ig-88"})
	})

	Convey("dedupes is fine with empty", t, func() {
		So(deduplicatePrefixes([]string{}), ShouldResemble, []string{})
	})
}

func actionListsEqual(am1, am2 map[string]*actions.Action) bool {
	if len(am1) != len(am2) {
		return false
	}

	for _, a1 := range am1 {
		a2 := am2[a1.Prefix]
		if a1 == nil || a2 == nil {
			if a1 == a2 {
				continue
			}
			return false
		}

		if !a1.Equals(a2) {
			return false
		}
	}
	return true
}
