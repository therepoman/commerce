package balance

import (
	"context"
	"testing"
	"time"

	"code.justin.tv/commerce/gogogadget/random"
	pachinko_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/pachinko"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestGetter_Get(t *testing.T) {
	Convey("given a getter", t, func() {
		pachinkoClient := new(pachinko_mock.PachinkoClient)

		getter := &getter{
			PachinkoClient: pachinkoClient,
		}

		userID := random.String(16)
		ctx := context.Background()

		Convey("on successful request", func() {
			pachinkoClient.On("GetBalance", mock.Anything, userID).Return(322, nil)
			ctx, cancel := context.WithTimeout(ctx, time.Second*1)
			defer cancel()

			balance, err := getter.GetBalance(ctx, userID, false)
			So(err, ShouldBeNil)
			So(balance, ShouldEqual, int32(322))
		})

		Convey("on failed request", func() {
			pachinkoClient.On("GetBalance", mock.Anything, userID).Return(0, errors.New("WALRUS STRIKE"))
			ctx, cancel := context.WithTimeout(ctx, time.Second*1)
			defer cancel()

			balance, err := getter.GetBalance(ctx, userID, false)
			So(err, ShouldNotBeNil)
			So(balance, ShouldEqual, int32(0))
		})
	})
}
