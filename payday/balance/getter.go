package balance

import (
	"context"

	"code.justin.tv/commerce/payday/clients/pachinko"
)

type PachinkoResponse struct {
	Balance int
	Error   error
}

type Getter interface {
	GetBalance(ctx context.Context, userID string, skipCache bool) (int32, error)
}

func NewGetter() Getter {
	return &getter{}
}

type getter struct {
	PachinkoClient pachinko.PachinkoClient `inject:""`
}

func (p *getter) GetBalance(ctx context.Context, userID string, skipCache bool) (int32, error) {
	balance, err := p.PachinkoClient.GetBalance(ctx, userID)
	if err != nil {
		return 0, err
	}

	return int32(balance), nil
}
