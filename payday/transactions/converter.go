package transactions

import (
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/errors"
	extension_tx "code.justin.tv/commerce/payday/extension/transaction"
	"code.justin.tv/commerce/payday/models"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"code.justin.tv/commerce/payday/utils/pointers"
	"github.com/golang/protobuf/ptypes"
)

type Converter interface {
	ConvertFromTransaction(record *dynamo.Transaction, inFlight bool) (*paydayrpc.Transaction, error)
	ConvertFromExtensionTransaction(record *extension_tx.Transaction) *paydayrpc.Transaction
}

type converter struct {
}

func NewConverter() Converter {
	return &converter{}
}

func (c *converter) ConvertFromTransaction(record *dynamo.Transaction, inFlight bool) (*paydayrpc.Transaction, error) {
	if record == nil {
		return nil, nil
	}

	lastUpdated, err := ptypes.TimestampProto(record.LastUpdated)
	if err != nil {
		log.WithError(err).WithField("transaction_id", string(record.TransactionId)).Warn("Failed to convert transaction into Twirp message")
		lastUpdated = nil
	}

	timeOfEvent, err := ptypes.TimestampProto(record.TimeOfEvent)
	if err != nil {
		log.WithError(err).WithField("transaction_id", string(record.TransactionId)).Warn("Failed to convert transaction into Twirp message")
		timeOfEvent = nil
	}

	// translate internal transaction types
	channelId := record.ChannelId
	var transactionType string
	if record.TransactionType != nil {
		switch *record.TransactionType {
		case models.BitsEventTransactionType:
			transactionType = models.GiveBitsToBroadcasterTransactionType
		case models.WATEBTransactionType:
			transactionType = models.AddBitsTransactionType
		case models.FinalizeHoldTransactionType:
			// Overwrite the transaction type with the passed in intended transactionType from the FinalizeHold call.
			// If you really need to process the message as the "FinalizeHold" type, you can leave the message type
			// blank or you can pass the HoldTransacton type in as the actual "FinalizeHold" type
			if record.FinalizedTransactionType != nil && *record.FinalizedTransactionType != "" {
				transactionType = *record.FinalizedTransactionType
			} else {
				errorMsg := "Failed to assign transaction type from FinalizeHold message."
				log.WithError(err).WithField("transaction_id", string(record.TransactionId)).Error(errorMsg)
				return nil, errors.New(errorMsg)
			}

			// Small hack to ensure challenges have some sort of context in Pachter since challenges are 1:1
			// And Pachter picks up the channelID from this transaction model, which isn't populated by the
			// FinalizeHold DAO path.
			if len(record.Recipients) == 1 {
				if channelId == nil || *channelId == "" {
					channelId = &record.Recipients[0].ID
				}
				break
			} else {
				log.Warn("FinalizeHold called with currently unsupported amount of recipients.")
			}
		default:
			transactionType = *record.TransactionType
		}
	}

	recipients := make([]*paydayrpc.Recipient, 0, len(record.Recipients))
	bitsUsageAttributions := make(map[string]*paydayrpc.BitsUsageAttributionWithTotal)
	for _, b := range record.Recipients {
		recipients = append(recipients, &paydayrpc.Recipient{
			Id:                     b.ID,
			Amount:                 b.BitsRevenueAttributionToBroadcaster,
			TransactionType:        paydayrpc.TransactionType(paydayrpc.TransactionType_value[transactionType]),
			RewardAmount:           b.RewardAttribution,
			TotalBitsToBroadcaster: b.TotalWithBroadcasterPrior,
		})

		bitsUsageAttributions[b.ID] = &paydayrpc.BitsUsageAttributionWithTotal{
			RewardAttribution:        b.RewardAttribution,
			BitsRevenueAttribution:   b.BitsRevenueAttributionToBroadcaster,
			TotalBitsWithBroadcaster: b.TotalWithBroadcasterPrior,
		}
	}

	return &paydayrpc.Transaction{
		TransactionId:             string(record.TransactionId),
		TransactionType:           transactionType,
		ChannelId:                 pointers.StringOrDefault(channelId, ""),
		UserId:                    pointers.StringOrDefault(record.UserId, ""),
		NumberOfBits:              pointers.Int64OrDefault(record.NumberOfBits, 0),
		NumberOfBitsSponsored:     pointers.Int64OrDefault(record.NumberOfBitsSponsored, 0),
		BitsType:                  pointers.StringOrDefault(record.BitsType, ""),
		ProductId:                 pointers.StringOrDefault(record.ProductId, ""),
		RawMessage:                pointers.StringOrDefault(record.RawMessage, ""),
		Platform:                  pointers.StringOrDefault(record.Platform, ""),
		LastUpdated:               lastUpdated,
		EmoteTotals:               record.EmoteTotals,
		OperandTransactionId:      pointers.StringOrDefault(record.OperandTransactionID, ""),
		Recipients:                recipients,
		TimeOfEvent:               timeOfEvent,
		AdminId:                   pointers.StringOrDefault(record.AdminID, ""),
		AdminReason:               pointers.StringOrDefault(record.AdminReason, ""),
		ZipCode:                   pointers.StringOrDefault(record.ZipCode, ""),
		CountryCode:               pointers.StringOrDefault(record.CountryCode, ""),
		City:                      pointers.StringOrDefault(record.City, ""),
		TotalBitsToBroadcaster:    pointers.Int64OrDefault(record.TotalBitsToBroadcaster, 0),
		TotalBitsAfterTransaction: pointers.Int64OrDefault(record.TotalBitsAfterTransaction, 0),
		IpAddress:                 pointers.StringOrDefault(record.IPAddress, ""),
		TrackingId:                pointers.StringOrDefault(record.TrackingID, ""),
		PurchasePrice:             pointers.StringOrDefault(record.PurchasePrice, ""),
		SkuQuantity:               pointers.Int64OrDefault(record.SkuQuantity, 0),
		NumChatters:               pointers.StringOrDefault(record.NumChatters, ""),
		PartnerType:               pointers.StringOrDefault(record.PartnerType, ""),
		IsAnonymous:               pointers.BoolOrDefault(record.IsAnonymous, false),
		ClientId:                  pointers.StringOrDefault(record.ClientID, ""),
		PurchaseCurrency:          pointers.StringOrDefault(record.PurchasePriceCurrency, ""),
		PollId:                    pointers.StringOrDefault(record.PollId, ""),
		PollChoiceId:              pointers.StringOrDefault(record.PollChoiceId, ""),
		SponsoredAmounts:          record.SponsoredAmounts,
		InFlight:                  inFlight,
		OriginOrderId:             pointers.StringOrDefault(record.OriginOrderId, ""),
		PayoutIgnored:             pointers.BoolOrDefault(record.IgnorePayout, false),
		BitsUsageAttributions:     bitsUsageAttributions,
	}, nil
}

func (c *converter) ConvertFromExtensionTransaction(record *extension_tx.Transaction) *paydayrpc.Transaction {
	if record == nil {
		return nil
	}

	lastUpdated, err := ptypes.TimestampProto(record.LastUpdated)
	if err != nil {
		log.WithError(err).WithField("transaction_id", string(record.TransactionID)).Warn("Failed to convert extension transaction into Twirp message")
		lastUpdated = nil
	}

	numberOfBits := int64(0)
	if record.Product.Cost != nil {
		numberOfBits = record.Product.Cost.Amount
	}

	return &paydayrpc.Transaction{
		TransactionId:             record.TransactionID,
		TransactionType:           models.UseBitsOnExtensionTransactionType,
		ChannelId:                 record.ChannelID,
		UserId:                    record.TUID,
		NumberOfBits:              numberOfBits,
		ProductId:                 record.Product.Sku,
		ExtensionClientId:         record.ExtensionClientID,
		LastUpdated:               lastUpdated,
		TotalBitsAfterTransaction: record.TotalBitsAfterTransaction,
		TotalBitsToBroadcaster:    record.TotalBitsToBroadcaster,
		IpAddress:                 record.IPAddress,
		City:                      record.City,
		CountryCode:               record.CountryCode,
		ZipCode:                   record.ZipCode,
		TimeOfEvent:               lastUpdated,
	}
}
