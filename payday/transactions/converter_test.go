package transactions

import (
	"testing"
	"time"

	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/dynamo/holds"
	"code.justin.tv/commerce/payday/extension/transaction"
	"code.justin.tv/commerce/payday/models"
	. "github.com/smartystreets/goconvey/convey"
)

func TestConverter_ConvertFromTransaction(t *testing.T) {
	converter := &converter{}

	Convey("given a transaction record", t, func() {
		record := &dynamo.Transaction{
			TransactionId:         "1",
			TransactionType:       pointers.StringP(models.BitsEventTransactionType),
			BitsType:              pointers.StringP("bits type"),
			UserId:                pointers.StringP("66666"),
			ChannelId:             pointers.StringP("123456"),
			NumberOfBits:          pointers.Int64P(100),
			NumberOfBitsSponsored: pointers.Int64P(10),
			Platform:              pointers.StringP("PHANTO"),
			ProductId:             pointers.StringP("product ID"),
			LastUpdated:           time.Now(),
			RawMessage:            pointers.StringP("raw message"),
			EmoteTotals: map[string]int64{
				"prefixA": 100,
				"prefixB": 200,
			},
			Recipients: []*holds.BitsUsageAttribution{
				{
					TotalWithBroadcasterPrior:           10,
					BitsRevenueAttributionToBroadcaster: 12,
					RewardAttribution:                   9,
					ID:                                  "66666",
				},
				{
					TotalWithBroadcasterPrior:           11,
					BitsRevenueAttributionToBroadcaster: 13,
					RewardAttribution:                   14,
					ID:                                  "77777",
				},
			},
		}

		Convey("converts into a Twirp transaction", func() {
			transaction, _ := converter.ConvertFromTransaction(record, false)
			So(transaction.TransactionType, ShouldEqual, models.GiveBitsToBroadcasterTransactionType)
			So(transaction.BitsType, ShouldEqual, "bits type")
			So(transaction.UserId, ShouldEqual, "66666")
			So(transaction.ChannelId, ShouldEqual, "123456")
			So(transaction.NumberOfBits, ShouldEqual, 100)
			So(transaction.NumberOfBitsSponsored, ShouldEqual, 10)
			So(transaction.Platform, ShouldEqual, "PHANTO")
			So(transaction.ProductId, ShouldEqual, "product ID")
			So(transaction.RawMessage, ShouldEqual, "raw message")
			So(transaction.EmoteTotals, ShouldResemble, record.EmoteTotals)
			So(transaction.InFlight, ShouldBeFalse)
			So(len(transaction.Recipients), ShouldEqual, len(record.Recipients))
			So(len(transaction.BitsUsageAttributions), ShouldEqual, len(record.Recipients))
			So(transaction.BitsUsageAttributions["66666"].RewardAttribution, ShouldEqual, 9)
			So(transaction.BitsUsageAttributions["66666"].BitsRevenueAttribution, ShouldEqual, 12)
			So(transaction.BitsUsageAttributions["66666"].TotalBitsWithBroadcaster, ShouldEqual, 10)

			Convey("converts a FinalizeHold into final type", func() {
				txType := models.FinalizeHoldTransactionType
				record.TransactionType = &txType
				finalTxType := models.UseBitsOnExtensionChallengeTransactionType
				record.FinalizedTransactionType = &finalTxType
				transaction, _ := converter.ConvertFromTransaction(record, false)
				So(transaction.TransactionType, ShouldEqual, models.UseBitsOnExtensionChallengeTransactionType)
			})
		})
	})

	Convey("given an admin transaction record", t, func() {
		record := &dynamo.Transaction{
			TransactionId:   "1",
			TransactionType: pointers.StringP(models.AdminAddBitsTransactionType),
			BitsType:        pointers.StringP("bits type"),
			UserId:          pointers.StringP("66666"),
			ChannelId:       pointers.StringP("123456"),
			NumberOfBits:    pointers.Int64P(100),
			Platform:        pointers.StringP("PHANTO"),
			ProductId:       pointers.StringP("product ID"),
			LastUpdated:     time.Now(),
		}

		Convey("converts into a Twirp transaction", func() {
			transaction, _ := converter.ConvertFromTransaction(record, true)
			So(transaction.TransactionType, ShouldEqual, models.AdminAddBitsTransactionType)
			So(transaction.BitsType, ShouldEqual, "bits type")
			So(transaction.UserId, ShouldEqual, "66666")
			So(transaction.ChannelId, ShouldEqual, "123456")
			So(transaction.NumberOfBits, ShouldEqual, 100)
			So(transaction.Platform, ShouldEqual, "PHANTO")
			So(transaction.ProductId, ShouldEqual, "product ID")
			So(transaction.EmoteTotals, ShouldResemble, record.EmoteTotals)
			So(transaction.InFlight, ShouldBeTrue)
		})
	})

	Convey("given a hold execution transaction", t, func() {
		record := &dynamo.Transaction{
			TransactionId:   "1",
			TransactionType: pointers.StringP(models.AdminAddBitsTransactionType),
			BitsType:        pointers.StringP("bits type"),
			UserId:          pointers.StringP("66666"),
			ChannelId:       pointers.StringP("123456"),
			NumberOfBits:    pointers.Int64P(100),
			Platform:        pointers.StringP("PHANTO"),
			ProductId:       pointers.StringP("product ID"),
			LastUpdated:     time.Now(),
			Recipients: []*holds.BitsUsageAttribution{
				{
					ID:                                  "recipient-1",
					BitsRevenueAttributionToBroadcaster: 10,
				},
				{
					ID:                                  "recipient-2",
					BitsRevenueAttributionToBroadcaster: 30,
				},
				{
					ID:                                  "recipient-3",
					BitsRevenueAttributionToBroadcaster: 60,
				},
			},
		}
		transaction, _ := converter.ConvertFromTransaction(record, false)

		Convey("converts recipients to twirp models", func() {
			So(transaction.Recipients, ShouldHaveLength, 3)
			for i, recipient := range transaction.Recipients {
				So(recipient.Id, ShouldEqual, record.Recipients[i].ID)
				So(recipient.Amount, ShouldEqual, record.Recipients[i].BitsRevenueAttributionToBroadcaster)
			}
		})
	})

	Convey("returns nil if input is nil", t, func() {
		output, err := converter.ConvertFromTransaction(nil, false)
		So(output, ShouldBeNil)
		So(err, ShouldBeNil)
	})
}

func TestConverter_ConvertFromExtensionTransaction(t *testing.T) {
	converter := &converter{}

	Convey("given an extension transaction", t, func() {
		record := &transaction.Transaction{
			TransactionID: "2",
			Product: transaction.FaladorProduct{
				Sku: "extension sku",
				Cost: &transaction.FaladorCost{
					Type:   "Bits",
					Amount: 150,
				},
			},
		}
		Convey("converts into a Twirp transaction", func() {
			transaction := converter.ConvertFromExtensionTransaction(record)
			So(transaction.TransactionType, ShouldEqual, models.UseBitsOnExtensionTransactionType)
			So(transaction.ProductId, ShouldEqual, "extension sku")
			So(transaction.NumberOfBits, ShouldEqual, 150)
		})
	})

	Convey("returns nil if input is nil", t, func() {
		output, err := converter.ConvertFromTransaction(nil, false)
		So(output, ShouldBeNil)
		So(err, ShouldBeNil)
	})
}
