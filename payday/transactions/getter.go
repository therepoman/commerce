package transactions

import (
	"context"
	"time"

	"code.justin.tv/commerce/payday/dynamo"
	extension_tx "code.justin.tv/commerce/payday/extension/transaction"
	"code.justin.tv/commerce/payday/models"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"github.com/go-errors/errors"
)

type Getter interface {
	GetTransactionsByID(ctx context.Context, ids []string) ([]*paydayrpc.Transaction, error)
	GetLastUpdatedTransactionsByUserOrChannel(ctx context.Context, userID, channelID, cursor string, eventBefore, eventAfter *time.Time, searchType models.TransactionSearchType) ([]*paydayrpc.Transaction, string, error)
}

type getter struct {
	Converter                   Converter                     `inject:""`
	TransactionsFetcher         Fetcher                       `inject:""`
	InFlightTransactionDao      dynamo.InFlightTransactionDao `inject:""`
	ExtensionTransactionManager extension_tx.Manager          `inject:""`
}

func NewGetter() Getter {
	return &getter{}
}

func (g *getter) GetTransactionsByID(ctx context.Context, ids []string) ([]*paydayrpc.Transaction, error) {
	if len(ids) == 0 {
		return nil, nil
	}

	transactionsChan := make(chan []*paydayrpc.Transaction, 1)
	inFlightTransactionsChan := make(chan []*paydayrpc.Transaction, 1)
	extensionTransactionsChan := make(chan []*paydayrpc.Transaction, 1)
	errChan := make(chan error, 2)

	// fetch from Transactions storage
	go func() {
		transactions, err := g.fetchTransactions(ctx, ids)
		if err != nil {
			errChan <- err
		} else {
			transactionsChan <- transactions
		}
	}()

	// fetch from our in flight transactions list, this is in case pachter gets really fast
	go func() {
		transactions, err := g.fetchInflightTransactions(ctx, ids)
		if err != nil {
			errChan <- err
		} else {
			inFlightTransactionsChan <- transactions
		}
	}()

	// fetch from Extension Transactions storage
	go func() {
		extensionTransactions, err := g.fetchExtensionTransactions(ctx, ids)
		if err != nil {
			errChan <- err
		} else {
			extensionTransactionsChan <- extensionTransactions
		}
	}()

	transactionResults := make([]*paydayrpc.Transaction, 0)
	inFlightResults := make([]*paydayrpc.Transaction, 0)
	extensionResults := make([]*paydayrpc.Transaction, 0)
	transactionsRead, inFlightTransactionsRead, extensionTransactionsRead := false, false, false

	for {
		select {
		case transactions := <-transactionsChan:
			transactionResults = append(transactionResults, transactions...)
			transactionsRead = true
		case inFlightTransactions := <-inFlightTransactionsChan:
			inFlightResults = append(inFlightResults, inFlightTransactions...)
			inFlightTransactionsRead = true
		case extensionTransactions := <-extensionTransactionsChan:
			extensionResults = append(extensionResults, extensionTransactions...)
			extensionTransactionsRead = true
		case err := <-errChan:
			return nil, err
		case <-ctx.Done():
			return nil, errors.New("timed out while fetching transactions")
		}

		if len(transactionResults)+len(inFlightResults)+len(extensionResults) == len(ids) ||
			transactionsRead && inFlightTransactionsRead && extensionTransactionsRead {
			return combineResults(transactionResults, inFlightResults, extensionResults), nil
		}
	}
}

func (g *getter) GetLastUpdatedTransactionsByUserOrChannel(ctx context.Context, userID, channelID, cursor string, eventBefore, eventAfter *time.Time, searchType models.TransactionSearchType) ([]*paydayrpc.Transaction, string, error) {
	switch searchType {
	case models.TransactionSearchTypeExtension:
		transactions, cursor, err := g.ExtensionTransactionManager.GetLastUpdatedTransactionsByUserOrChannel(ctx, userID, channelID, cursor, eventBefore, eventAfter, extension_tx.StatusProcessed)
		if err != nil {
			return nil, "", err
		}

		rpcTransactions := g.extensionTransactionsToRPCTransactions(transactions)

		return rpcTransactions, cursor, nil
	case models.TransactionSearchTypeOnsite:
		transactions, cursor, err := g.TransactionsFetcher.FetchLastUpdatedTransactionsByUserOrChannel(ctx, userID, channelID, cursor, eventBefore, eventAfter)
		if err != nil {
			return nil, "", err
		}

		rpcTransactions, err := g.nonInflightTransactionsToRPCTransactions(transactions)
		if err != nil {
			return nil, "", err
		}
		return rpcTransactions, cursor, nil
	default:
		return nil, "", errors.Errorf("unrecognized SearchType %s calling GetLastUpdatedTransactionsByUserOrChannel", searchType)
	}
}

func (g *getter) fetchTransactions(ctx context.Context, ids []string) ([]*paydayrpc.Transaction, error) {
	transactions, err := g.TransactionsFetcher.FetchTransactionsByID(ctx, ids)
	if err != nil {
		return nil, err
	}

	convertedTransactions, err := g.nonInflightTransactionsToRPCTransactions(transactions)
	if err != nil {
		return nil, err
	}

	return convertedTransactions, nil
}

func (g *getter) fetchInflightTransactions(ctx context.Context, ids []string) ([]*paydayrpc.Transaction, error) {
	transactions, err := g.InFlightTransactionDao.GetTransactions(ctx, ids)
	if err != nil {
		return nil, err
	}

	convertedTransactions := make([]*paydayrpc.Transaction, 0, len(transactions))
	for _, tx := range transactions {
		transaction := tx
		convertedTransaction, err := g.Converter.ConvertFromTransaction(&transaction, true)
		if err != nil {
			return nil, err
		}
		convertedTransactions = append(convertedTransactions, convertedTransaction)
	}
	return convertedTransactions, nil
}

func (g *getter) fetchExtensionTransactions(ctx context.Context, ids []string) ([]*paydayrpc.Transaction, error) {
	transactions, err := g.ExtensionTransactionManager.GetTransactionsByID(ids)
	if err != nil {
		return nil, err
	}

	convertedTransactions := g.extensionTransactionsToRPCTransactions(transactions)

	return convertedTransactions, nil
}

func combineResults(transactions, inFlightTransactions, extensionTransactions []*paydayrpc.Transaction) []*paydayrpc.Transaction {
	finalized := make(map[string]bool)
	for _, transaction := range transactions {
		finalized[transaction.TransactionId] = true
	}

	results := append(transactions, extensionTransactions...)

	for _, inFlightTransaction := range inFlightTransactions {
		if _, ok := finalized[inFlightTransaction.TransactionId]; !ok {
			results = append(results, inFlightTransaction)
		}
	}

	return results
}

func (g *getter) extensionTransactionsToRPCTransactions(transactions []*extension_tx.Transaction) []*paydayrpc.Transaction {
	convertedTransactions := make([]*paydayrpc.Transaction, 0, len(transactions))
	for _, tx := range transactions {
		convertedTransactions = append(convertedTransactions, g.Converter.ConvertFromExtensionTransaction(tx))
	}

	return convertedTransactions
}

func (g *getter) nonInflightTransactionsToRPCTransactions(transactions []*dynamo.Transaction) ([]*paydayrpc.Transaction, error) {
	convertedTransactions := make([]*paydayrpc.Transaction, 0, len(transactions))
	for _, tx := range transactions {
		tx, err := g.Converter.ConvertFromTransaction(tx, false)
		if err != nil {
			return nil, err
		}
		convertedTransactions = append(convertedTransactions, tx)
	}

	return convertedTransactions, nil
}
