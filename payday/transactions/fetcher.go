package transactions

import (
	"context"
	"errors"
	"time"

	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/hystrix"
	cmd "code.justin.tv/commerce/payday/hystrix"
)

type Fetcher interface {
	FetchTransactionsByID(ctx context.Context, ids []string) ([]*dynamo.Transaction, error)
	FetchTransactionsByUserIDAndTransactionType(ctx context.Context, userID string, transactionType string, limit int64, descending bool, eventBefore, eventAfter *time.Time) ([]*dynamo.Transaction, error)
	FetchLastUpdatedTransactionsByUserOrChannel(ctx context.Context, userID, channelID, cursor string, eventBefore, eventAfter *time.Time) ([]*dynamo.Transaction, string, error)
}

type fetcher struct {
	DAO dynamo.ITransactionsDao `inject:""`
}

func NewFetcher() Fetcher {
	return &fetcher{}
}

func (f *fetcher) FetchTransactionsByID(ctx context.Context, ids []string) ([]*dynamo.Transaction, error) {
	if len(ids) == 0 {
		return nil, nil
	}
	return f.DAO.BatchGet(ids)
}

func (f *fetcher) FetchTransactionsByUserIDAndTransactionType(ctx context.Context, userID string, transactionType string, limit int64, descending bool, eventBefore, eventAfter *time.Time) ([]*dynamo.Transaction, error) {
	if userID == "" || transactionType == "" {
		return nil, nil
	}

	return f.DAO.GetByUserIDAndTransactionType(ctx, dynamo.GetByUserIDAndTransactionTypeArgs{
		EventBefore:     eventBefore,
		EventAfter:      eventAfter,
		Limit:           limit,
		UserID:          userID,
		TransactionType: transactionType,
		Descending:      descending,
	})
}

type FetchLastUpdatedTransactionsByUserOrChannelArgs struct {
	UserID      string
	ChannelID   string
	Cursor      string
	EventBefore *time.Time
	EventAfter  *time.Time
}

func (f *fetcher) FetchLastUpdatedTransactionsByUserOrChannel(ctx context.Context, userID, channelID, cursor string, eventBefore, eventAfter *time.Time) ([]*dynamo.Transaction, string, error) {
	var transactions []*dynamo.Transaction
	newCursor := ""
	var err error

	if userID != "" {
		err = hystrix.Do(cmd.GetLastUpdatedTransactionsByUserCommand, func() error {
			var innerErr error

			transactions, newCursor, innerErr = f.DAO.GetLastUpdatedTransactionsByUser(ctx, userID, dynamo.GetLastUpdatedTransactionsArgs{
				Cursor:      cursor,
				EventBefore: eventBefore,
				EventAfter:  eventAfter,
			})

			return innerErr
		}, nil)
	} else if channelID != "" {
		err = hystrix.Do(cmd.GetLastUpdatedTransactionsByChannelCommand, func() error {
			var innerErr error

			transactions, newCursor, innerErr = f.DAO.GetLastUpdatedTransactionsByChannel(ctx, channelID, dynamo.GetLastUpdatedTransactionsArgs{
				Cursor:      cursor,
				EventBefore: eventBefore,
				EventAfter:  eventAfter,
			})

			return innerErr
		}, nil)
	} else {
		return nil, "", errors.New("must provide a user_id or channel_id to when calling TransactionFetcher#FetchLastUpdatedTransactionsByUserOrChannel")
	}

	return transactions, newCursor, err
}
