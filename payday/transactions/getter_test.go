package transactions

import (
	"context"
	"errors"
	"testing"
	"time"

	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/extension/transaction"
	dynamo_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo"
	transaction_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/extension/transaction"
	transactions_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/transactions"
	"code.justin.tv/commerce/payday/models"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestGetter_GetTransactionsByID(t *testing.T) {
	Convey("Given a transactions getter", t, func() {
		fetcher := new(transactions_mock.Fetcher)
		inFlightDao := new(dynamo_mock.InFlightTransactionDao)
		manager := new(transaction_mock.Manager)

		getter := &getter{
			TransactionsFetcher:         fetcher,
			InFlightTransactionDao:      inFlightDao,
			ExtensionTransactionManager: manager,
			Converter:                   NewConverter(),
		}

		ids := []string{
			"1",
			"2",
			"3",
		}

		ctx, cancel := context.WithCancel(context.Background())

		tx := &dynamo.Transaction{
			TransactionId:   "1",
			TransactionType: pointers.StringP("transaction type"),
			BitsType:        pointers.StringP("bits type"),
			UserId:          pointers.StringP("66666"),
			ChannelId:       pointers.StringP("123456"),
			NumberOfBits:    pointers.Int64P(100),
			Platform:        pointers.StringP("PHANTO"),
			ProductId:       pointers.StringP("product ID"),
			LastUpdated:     time.Now(),
			RawMessage:      pointers.StringP("raw message"),
		}

		extension_tx := &transaction.Transaction{
			TransactionID: "2",
			Product: transaction.FaladorProduct{
				Sku: "extension sku",
				Cost: &transaction.FaladorCost{
					Type:   "Bits",
					Amount: 150,
				},
			},
		}

		inFlightDao.On("GetTransactions", mock.Anything, mock.Anything).Return([]dynamo.Transaction{}, nil)

		Convey("when fetcher and manager returns results", func() {
			fetcher.On("FetchTransactionsByID", mock.Anything, mock.Anything).Return([]*dynamo.Transaction{tx}, nil)
			manager.On("GetTransactionsByID", mock.Anything).Return([]*transaction.Transaction{extension_tx}, nil)

			transactions, err := getter.GetTransactionsByID(ctx, ids)
			So(err, ShouldBeNil)
			So(transactions, ShouldHaveLength, 2)
		})

		Convey("returns an error when", func() {
			Convey("transactions fetcher fails", func() {
				fetcher.On("FetchTransactionsByID", mock.Anything, mock.Anything).Return(nil, errors.New("can't fetch"))
				manager.On("GetTransactionsByID", mock.Anything).Return([]*transaction.Transaction{}, nil)
			})

			Convey("extension transactions manager fails", func() {
				fetcher.On("FetchTransactionsByID", mock.Anything, mock.Anything).Return([]*dynamo.Transaction{}, nil)
				manager.On("GetTransactionsByID", mock.Anything).Return(nil, errors.New("can't manage"))
			})

			Convey("context is cancelled", func() {
				cancel()
				fetcher.On("FetchTransactionsByID", mock.Anything, mock.Anything).After(2*time.Second).Return([]*dynamo.Transaction{}, nil)
				manager.On("GetTransactionsByID", mock.Anything).After(2*time.Second).Return([]*transaction.Transaction{}, nil)
			})

			_, err := getter.GetTransactionsByID(ctx, ids)
			So(err, ShouldNotBeNil)
		})
	})
}

func TestGetter_combineResults(t *testing.T) {

	finalizedTransaction := &paydayrpc.Transaction{
		TransactionId:   "finalized",
		TransactionType: models.AddBitsTransactionType,
		InFlight:        false,
	}

	inFlightTransaction := &paydayrpc.Transaction{
		TransactionId:   "inFlight",
		TransactionType: models.GiveBitsToBroadcasterTransactionType,
		InFlight:        true,
	}

	extensionTransaction := &paydayrpc.Transaction{
		TransactionId:   "extensions!",
		TransactionType: models.UseBitsOnExtensionTransactionType,
		InFlight:        false,
	}

	Convey("Given not a darn thing as a base", t, func() {
		Convey("what if everything was nil?", func() {
			So(combineResults(nil, nil, nil), ShouldHaveLength, 0)
		})

		Convey("what if everything is nil but inflight?", func() {
			So(combineResults(
				nil,
				[]*paydayrpc.Transaction{inFlightTransaction},
				nil), ShouldHaveLength, 1)
		})

		Convey("what if they're all unique?", func() {
			So(combineResults(
				[]*paydayrpc.Transaction{finalizedTransaction},
				[]*paydayrpc.Transaction{inFlightTransaction},
				[]*paydayrpc.Transaction{extensionTransaction}), ShouldHaveLength, 3)
		})

		Convey("what if we have a duplicate that's in flight", func() {
			results := combineResults(
				[]*paydayrpc.Transaction{finalizedTransaction},
				[]*paydayrpc.Transaction{{
					TransactionId:   "finalized",
					TransactionType: models.AddBitsTransactionType,
					InFlight:        true,
				}},
				[]*paydayrpc.Transaction{extensionTransaction})
			So(results, ShouldHaveLength, 2)

			// should have removed the inflight version
			for _, result := range results {
				So(result.InFlight, ShouldBeFalse)
			}
		})

		Convey("what if we're sneaky and they're all duplicates?", func() {
			results := combineResults(
				[]*paydayrpc.Transaction{finalizedTransaction},
				[]*paydayrpc.Transaction{{
					TransactionId:   "finalized",
					TransactionType: models.AddBitsTransactionType,
					InFlight:        true,
				}},
				[]*paydayrpc.Transaction{{
					TransactionId:   "finalized",
					TransactionType: models.UseBitsOnExtensionTransactionType,
					InFlight:        false,
				}})
			So(results, ShouldHaveLength, 2)

			// should have removed the inflight version
			for _, result := range results {
				So(result.TransactionId, ShouldEqual, "finalized")
				So(result.InFlight, ShouldBeFalse)
			}
		})
	})
}

func TestGetter_GetLastUpdatedTransactionsByUserOrChannel(t *testing.T) {
	Convey("Given a transactions getter", t, func() {
		fetcher := new(transactions_mock.Fetcher)
		manager := new(transaction_mock.Manager)

		getter := &getter{
			TransactionsFetcher:         fetcher,
			ExtensionTransactionManager: manager,
			Converter:                   NewConverter(),
		}

		userID := "abc"
		cursor := "some-cursor"
		var emptyTime *time.Time

		Convey("when searching extension transactions", func() {
			Convey("when the extension manager errors, we should error", func() {
				manager.On("GetLastUpdatedTransactionsByUserOrChannel", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil, "", errors.New("ERROR"))
				_, _, err := getter.GetLastUpdatedTransactionsByUserOrChannel(context.Background(), userID, "", "", emptyTime, emptyTime, models.TransactionSearchTypeExtension)
				So(err, ShouldNotBeNil)
			})

			Convey("when the extension manager succeeds, we should return the transactions", func() {
				mockTransactions := []*transaction.Transaction{
					{},
				}
				mockCursor := "new-cursor"
				manager.On("GetLastUpdatedTransactionsByUserOrChannel", mock.Anything, userID, "", cursor, emptyTime, emptyTime, transaction.StatusProcessed).Return(mockTransactions, mockCursor, nil)
				transactions, newCursor, err := getter.GetLastUpdatedTransactionsByUserOrChannel(context.Background(), userID, "", cursor, emptyTime, emptyTime, models.TransactionSearchTypeExtension)
				So(err, ShouldBeNil)
				So(transactions, ShouldHaveLength, 1)
				So(newCursor, ShouldEqual, mockCursor)
			})
		})

		Convey("when searching onsite transactions", func() {
			Convey("when the fetcher errors, we should error", func() {
				fetcher.On("FetchLastUpdatedTransactionsByUserOrChannel", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil, "", errors.New("ERROR"))
				_, _, err := getter.GetLastUpdatedTransactionsByUserOrChannel(context.Background(), userID, "", "", emptyTime, emptyTime, models.TransactionSearchTypeOnsite)
				So(err, ShouldNotBeNil)
			})

			Convey("when the fetcher succeeds, we should return the transactions", func() {
				mockTransactions := []*dynamo.Transaction{
					{},
				}
				mockCursor := "new-cursor"
				fetcher.On("FetchLastUpdatedTransactionsByUserOrChannel", mock.Anything, userID, "", cursor, emptyTime, emptyTime).Return(mockTransactions, mockCursor, nil)
				transactions, newCursor, err := getter.GetLastUpdatedTransactionsByUserOrChannel(context.Background(), userID, "", cursor, emptyTime, emptyTime, models.TransactionSearchTypeOnsite)
				So(err, ShouldBeNil)
				So(transactions, ShouldHaveLength, 1)
				So(newCursor, ShouldEqual, mockCursor)
			})
		})
	})
}
