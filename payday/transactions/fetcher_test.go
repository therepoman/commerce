package transactions

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/payday/dynamo"
	dynamo_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestFetcher_FetchTransactionsByID(t *testing.T) {
	Convey("Given a transactions fetcher", t, func() {
		dao := new(dynamo_mock.ITransactionsDao)

		fetcher := &fetcher{
			DAO: dao,
		}

		ids := []string{
			"id-1",
			"id-2",
			"id-3",
		}

		ctx := context.Background()

		Convey("when no IDs are provided", func() {
			ids = []string{}

			Convey("returns empty result", func() {
				transactions, err := fetcher.FetchTransactionsByID(ctx, ids)
				So(err, ShouldBeNil)
				So(transactions, ShouldHaveLength, 0)
				dao.AssertNumberOfCalls(t, "FetchTransactionsByID", 0)
			})
		})

		Convey("when transactions DAO returns multiple results", func() {
			dao.On("BatchGet", mock.Anything).Return([]*dynamo.Transaction{
				{}, {}, {},
			}, nil)

			transactions, err := fetcher.FetchTransactionsByID(ctx, ids)
			So(err, ShouldBeNil)
			So(transactions, ShouldHaveLength, 3)
		})

		Convey("when transactions DAO fails", func() {
			dao.On("BatchGet", mock.Anything).Return(nil, errors.New("fail"))
			Convey("returns an error", func() {
				_, err := fetcher.FetchTransactionsByID(ctx, ids)
				So(err, ShouldNotBeNil)
			})
		})
	})
}
