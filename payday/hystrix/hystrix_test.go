package hystrix

import (
	"context"
	"errors"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestDo(t *testing.T) {
	Convey("Testing Do", t, func() {
		Convey("when the inner function returns nil, we return nil", func() {
			err := Do("hi", func() error {
				return nil
			}, nil)

			So(err, ShouldBeNil)
		})

		Convey("when the inner function errors, we return the error", func() {
			err := Do("hi", func() error {
				return errors.New("error")
			}, nil)

			So(err, ShouldNotBeNil)
		})

		Convey("when the inner function returns a context cancellation error, we return the error", func() {
			err := Do("hi", func() error {
				return context.Canceled
			}, nil)

			So(err, ShouldNotBeNil)
		})
	})
}
