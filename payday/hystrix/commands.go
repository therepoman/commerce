package hystrix

import (
	afex "github.com/afex/hystrix-go/hystrix" //nolint:depguard
)

const (
	//Commands
	UsersCommand                               = "get_user"
	UpdateTransactionsCommand                  = "update_transactions"
	CreateNewTransactionCommand                = "create_new_transaction"
	GetTransactionsCommand                     = "get_transactions"
	GetLastUpdatedTransactionsByUserCommand    = "get_last_updated_transaction_by_user"
	GetLastUpdatedTransactionsByChannelCommand = "get_last_updated_transactions_by_channel"
	GetBalanceCommand                          = "get_balance"
	GetChannelCommand                          = "get_channel"
	GetChannelRedisCommand                     = "get_channel_redis"
	GetHighestBadgeCommand                     = "get_highest_badge"
	GetFloPricesCommand                        = "get_prices_flo"
	GetImageCommand                            = "get_image"
	GetImageByAssetIdCommand                   = "get_image_by_asset_id"
	GetChannelByImageSetIdCommand              = "get_channel_by_image_set"
	UpdateImageCommand                         = "update_image"
	BatchDeleteImagesCommand                   = "batch_delete_images"
	GetTaxInfo                                 = "get_tax_info"
	GetUserPayoutType                          = "get_user_payout_type"
	GetPayoutEntityForChannelCommand           = "get_payout_entity_for_channel"
	GetRewardCommand                           = "get_reward"
	GetRewardByFulfillmentAccountCommand       = "get_reward_by_fulfillment_account"
	UpdateRewardCommand                        = "update_reward"
	GetLeaderboardBadgeHoldersCommand          = "get_leaderboard_badge_holders"
	GetNumberOfPubSubListenersCommand          = "pub_sub_listeners"
	GetBadgeTiersCommand                       = "get_badge_tiers"
	GetExtensionByChannelIdCommand             = "get_extension_by_channel_id"
	FaladorGetProductCommand                   = "falador_get_product"

	// Mako Commands
	MakoCreateEmoteGroupEntitlements        = "mako_create_emote_group_entitlements"
	MakoGetAllEmotesByOwnerId               = "mako_get_all_emotes_by_owner_id"
	MakoGetEmoticonsByGroups                = "mako_get_emoticons_by_groups"
	MakoDashboardGetEmotesByGroups          = "mako_dashboard_get_emotes_by_groups"
	MakoCreateEmoticon                      = "mako_create_emoticon"
	MakoCreateEmote                         = "mako_create_emote"
	MakoDeleteEmoticon                      = "mako_delete_emoticon"
	MakoBatchValidateCodeUniqueness         = "mako_batch_validate_code_uniqueness"
	MakoBatchValidateEmoteCodeAcceptability = "mako_batch_validate_emote_code_acceptability"
	MakoCreateEmoteGroup                    = "mako_create_emote_group"
	MakoGetPrefix                           = "mako_get_prefix"
	MakoAssignEmoteToGroup                  = "mako_assign_emote_to_group"

	// Pachinko Commands
	PachinkoGetBalanceCommand        = "pachinko_get_balance"
	PachinkoAddTokensCommand         = "pachinko_add_tokens"
	PachinkoStartTransactionCommand  = "pachinko_start_transaction"
	PachinkoCommitTransactionCommand = "pachinko_commit_transaction"
	PachinkoCheckHoldCommand         = "pachinko_check_hold"
	PachinkoOverrideBalanceCommand   = "pachinko_override_balance"
	PachinkoDeleteBalanceByOwnerId   = "pachinko_delete_balance_by_owner_id"

	// Pantheon Commands
	GetBucketedLeaderboardCounts               = "get_bucketed_leaderboard_counts"
	GetLeaderboardCommand                      = "get_leaderboard"
	GetLeaderboardsCommand                     = "get_leaderboards"
	GetLeaderboardEntriesAboveThresholdCommand = "get_leaderboard_entries_above_threshold"
	PublishEventCommand                        = "publish_event"
	ModerateEntryCommand                       = "moderate_entry"
	PantheonDeleteLeaderboards                 = "pantheon_delete_leaderboards"
	PantheonDeleteEntries                      = "pantheon_delete_entries"

	// Pachter Commands
	PachterGetBitsSpendOrderCommand = "pachter_get_bits_spend_order"

	// Extension Transactions DAO
	GetExtensionTransactionCommand                      = "get_extension_transaction_command"
	GetLastUpdatedExtensionTransactionsByUserCommand    = "get_last_updated_extension_transactions_by_user"
	GetLastUpdatedExtensionTransactionsByChannelCommand = "get_last_updated_extension_transactions_by_channel"

	// OWL Commands
	OWLGetClientCommand = "owl_get_client"

	// Zuma Commands
	ZumaListModCommand        = "zuma_list_mods"
	ZumaGetModCommand         = "zuma_get_mod"
	ZumaEnforceMessageCommand = "zuma_enforce_message"

	// PDMS Commands
	PDMSReportDeletion  = "pdms_report_deletion"
	PDMSPromiseDeletion = "pdms_promise_deletion"
	PDMSSetTestDeletion = "pdms_set_test_deletion"

	// cache operations
	AutomodCacheGet                = "automod_cache_get"
	AutomodCacheSet                = "automod_cache_set"
	AutomodCacheDel                = "automod_cache_del"
	CampaignsCacheGet              = "campaign_cache_get"
	CampaignsCacheSet              = "campaign_cache_set"
	CampaignsCacheDel              = "campaign_cache_del"
	CampaignsUserCacheGet          = "campaign_user_cache_get"
	CampaignsUserCacheSet          = "campaign_user_cache_set"
	CampaignsUserCacheDel          = "campaign_user_cache_del"
	CampaignsChannelCacheGet       = "campaign_channel_cache_get"
	CampaignsChannelCacheSet       = "campaign_channel_cache_set"
	CampaignsChannelCacheDel       = "campaign_channel_cache_del"
	CampaignsEligibleCacheGet      = "campaign_eligible_cache_get"
	CampaignsEligibleCacheSet      = "campaign_eligible_cache_get"
	CampaignsEligibleCacheDel      = "campaign_eligible_cache_get"
	ActionsCacheGet                = "actions_cache_get"
	ActionsCacheSet                = "actions_cache_set"
	ActionsCacheDel                = "actions_cache_del"
	BalanceCacheGet                = "balance_cache_get"
	BalanceCacheSet                = "balance_cache_set"
	BalanceCacheDel                = "balance_cache_del"
	EarnedBadgesCacheGet           = "earned_badges_cache_get"
	EarnedBadgesCacheSet           = "earned_badges_cache_set"
	FloPricesCacheGet              = "flo_prices_cache_get"
	FloPricesCacheSet              = "flo_prices_cache_set"
	FloPricesCacheDel              = "flo_prices_cache_del"
	ImagesCacheGet                 = "images_cache_get"
	ImagesCacheSet                 = "images_cache_set"
	ImagesCacheDel                 = "images_cache_del"
	PrefixCacheGet                 = "prefix_cache_get"
	PrefixCacheSet                 = "prefix_cache_set"
	PrefixCacheDel                 = "prefix_cache_del"
	ProductsCacheGet               = "products_cache_get"
	ProductsCacheSet               = "products_cache_set"
	ProductsCacheDel               = "products_cache_del"
	UserCacheGet                   = "user_cache_get"
	UserCacheSet                   = "user_cache_set"
	UserCacheDel                   = "user_cache_del"
	UserServiceCacheGet            = "user_service_cache_get"
	UserServiceCacheSet            = "user_service_cache_set"
	UserServiceCacheDel            = "user_service_cache_del"
	ChatBadgesCacheGet             = "chat_badges_cache_get"
	ChatBadgesCacheSet             = "chat_badges_cache_set"
	ChatBadgesCacheDel             = "chat_badges_cache_del"
	ThrottleGet                    = "throttle_get"
	ThrottleSet                    = "throttle_set"
	ExperimentGet                  = "experiment_get"
	ExperimentSet                  = "experiment_set"
	PushyStatusCacheGet            = "pushy_status_cache_get"
	PushyStatusCacheSet            = "pushy_status_cache_set"
	TierNotificationsCacheGet      = "tier_notifications_cache_get"
	TierNotificationsCacheSet      = "tier_notifications_cache_set"
	TierNotificationsCacheDel      = "tier_notifications_cache_del"
	ProductsRecentPlatformCacheGet = "products_recent_platform_cache_get"
	ProductsRecentPlatformCacheSet = "products_recent_platform_cache_set"
	ProductsSinglePurchaseCacheGet = "products_single_purchase_cache_get"
	ProductsSinglePurchaseCacheSet = "products_single_purchase_cache_set"
	HoldsCacheGet                  = "hold_get"
	HoldsCacheSet                  = "hold_set"
	HoldsCacheDel                  = "hold_del"
	RipleyCacheGet                 = "ripley_cache_get"
	RipleyCacheSet                 = "ripley_cache_set"
	AutoReloadProfileCacheGet      = "auto_reload_profile_cache_get"
	AutoReloadProfileCacheSet      = "auto_reload_profile_cache_set"
	AutoReloadProfileCacheDel      = "auto_reload_profile_cache_del"
	EmoticonUploadConfigsCacheGet  = "emoticon_upload_config_cache_get"
	EmoticonUploadConfigsCacheSet  = "emoticon_upload_config_cahce_set"
	EmoticonUploadConfigsCacheDel  = "emoticon_upload_config_cache_del"
	EmoticonImageUploadCacheGet    = "emoticon_image_upload_cache_get"
	EmoticonImageUploadCacheSet    = "emoticon_image_upload_cache_set"
	EmoticonImageUploadCacheDel    = "emoticon_image_upload_cache_del"
	EmoticonNumberPerTierCacheGet  = "emoticon_number_per_tier_cache_get"
	EmoticonNumberPerTierCacheSet  = "emoticon_number_per_tier_cache_set"
	EmoticonNumberPerTierCacheDel  = "emoticon_number_per_tier_cache_del"
	BadgeTierEmotesCacheGet        = "badge_tier_emotes_cache_get"
	BadgeTierEmotesCacheSet        = "badge_tier_emotes_cache_set"
	BadgeTierEmotesCacheDel        = "badge_tier_emotes_cache_del"

	// Dynamo
	ActionsDBGet              = "actions_db_get"
	ActionsDBBatchGet         = "actions_db_batch_get"
	ActionsDBGetAll           = "actions_db_get_all"
	ActionsDBUpdate           = "actions_db_update"
	ActionsDBDelete           = "actions_db_delete"
	BadgeTierEmoteGetGroupIds = "badge_tier_emotes_dynamo_get_group_ids"

	//Timeouts
	TestTimeout = 30000

	// Concurrency
	DefaultMaxConcurrent = 100

	NitroGetPremiumStatusesCommand = "nitro_get_premium_status"
)

func init() {
	configureTimeout(UsersCommand, afex.DefaultTimeout)
	configureTimeout(UpdateTransactionsCommand, afex.DefaultTimeout)
	configureTimeout(CreateNewTransactionCommand, afex.DefaultTimeout)
	configureTimeout(GetTransactionsCommand, afex.DefaultTimeout)
	configureTimeout(GetLastUpdatedTransactionsByUserCommand, 1000)
	configureTimeout(GetLastUpdatedTransactionsByChannelCommand, 1000)
	configureTimeout(GetBalanceCommand, 8000)
	configureTimeout(GetChannelCommand, 250)
	configureTimeout(GetChannelRedisCommand, 100)
	configureTimeout(GetHighestBadgeCommand, 2000)
	configureTimeout(GetImageCommand, afex.DefaultTimeout)
	configureTimeout(GetImageByAssetIdCommand, afex.DefaultTimeout)
	configureTimeout(UpdateImageCommand, afex.DefaultTimeout)
	configureTimeout(BatchDeleteImagesCommand, afex.DefaultTimeout)
	configureTimeout(GetTaxInfo, afex.DefaultTimeout)
	configureTimeout(GetChannelByImageSetIdCommand, afex.DefaultTimeout)
	configureTimeout(GetUserPayoutType, 5000)
	configureTimeout(GetPayoutEntityForChannelCommand, 2000)
	configureTimeout(GetRewardCommand, afex.DefaultTimeout)
	configureTimeout(UpdateRewardCommand, afex.DefaultTimeout)
	configureTimeout(GetNumberOfPubSubListenersCommand, 500)
	configureTimeout(GetBadgeTiersCommand, 350)
	configureTimeout(GetExtensionByChannelIdCommand, 1000)
	configureTimeout(FaladorGetProductCommand, 1000)

	// Extension Transactions DAO Timeouts
	configureTimeout(GetExtensionTransactionCommand, afex.DefaultTimeout)
	configureTimeout(GetLastUpdatedExtensionTransactionsByUserCommand, 1000)
	configureTimeout(GetLastUpdatedExtensionTransactionsByChannelCommand, 1000)

	// OWL Timeouts
	configureTimeout(OWLGetClientCommand, afex.DefaultTimeout)

	// Zuma Timeouts
	configureTimeout(ZumaListModCommand, afex.DefaultTimeout)
	configureTimeout(ZumaGetModCommand, afex.DefaultTimeout)
	configureTimeout(ZumaEnforceMessageCommand, afex.DefaultTimeout)

	// Mako Timeouts
	configureTimeout(MakoCreateEmoteGroupEntitlements, 1000)
	configureTimeout(MakoGetAllEmotesByOwnerId, 1000)
	configureTimeout(MakoGetEmoticonsByGroups, 250)
	configureTimeout(MakoDashboardGetEmotesByGroups, 1000)
	configureTimeout(MakoCreateEmoticon, 5000)
	configureTimeout(MakoCreateEmote, 5000)
	configureTimeout(MakoDeleteEmoticon, 1000)
	configureTimeout(MakoBatchValidateCodeUniqueness, 1000)
	configureTimeout(MakoBatchValidateEmoteCodeAcceptability, 1000)
	configureTimeout(MakoCreateEmoteGroup, 500)
	configureTimeout(MakoGetPrefix, 1000)
	configureTimeout(MakoAssignEmoteToGroup, 1000)

	// Pachinko Timeouts
	configureTimeout(PachinkoGetBalanceCommand, 1000)
	configureTimeout(PachinkoAddTokensCommand, 1500)
	configureTimeout(PachinkoCheckHoldCommand, 1000)
	configureTimeout(PachinkoStartTransactionCommand, 1000)
	configureTimeout(PachinkoCommitTransactionCommand, 1500)
	configureTimeout(PachinkoOverrideBalanceCommand, 1000)
	configureTimeout(PachinkoDeleteBalanceByOwnerId, 1500)
	configureTimeout(PachterGetBitsSpendOrderCommand, 1000)

	// Pantheon Timeouts
	configureTimeout(GetLeaderboardCommand, 350)
	configureTimeout(GetLeaderboardsCommand, 3000)
	configureTimeout(GetLeaderboardEntriesAboveThresholdCommand, afex.DefaultTimeout)
	configureTimeout(PublishEventCommand, 3000)
	configureTimeout(ModerateEntryCommand, afex.DefaultTimeout)
	// we want to be really lenient for the following
	configureTimeout(PantheonDeleteLeaderboards, 30000)
	configureTimeout(PantheonDeleteEntries, 30000)

	// PDMS Timeouts
	configureTimeout(PDMSReportDeletion, 3000)
	configureTimeout(PDMSPromiseDeletion, 3000)
	configureTimeout(PDMSSetTestDeletion, 3000)

	// cache timeouts
	configureTimeout(AutomodCacheGet, 250)
	configureTimeout(AutomodCacheSet, 250)
	configureTimeout(AutomodCacheDel, 250)
	configureTimeout(CampaignsCacheGet, 250)
	configureTimeout(CampaignsCacheSet, 250)
	configureTimeout(CampaignsCacheDel, 250)
	configureTimeout(BalanceCacheGet, 250)
	configureTimeout(BalanceCacheSet, 250)
	configureTimeout(BalanceCacheDel, 250)
	configureTimeout(EarnedBadgesCacheGet, 250)
	configureTimeout(EarnedBadgesCacheSet, 250)
	configureTimeout(FloPricesCacheGet, 250)
	configureTimeout(FloPricesCacheSet, 250)
	configureTimeout(FloPricesCacheDel, 250)
	configureTimeout(ImagesCacheGet, 250)
	configureTimeout(ImagesCacheSet, 250)
	configureTimeout(ImagesCacheDel, 250)
	configureTimeout(PrefixCacheGet, 250)
	configureTimeout(PrefixCacheSet, 250)
	configureTimeout(PrefixCacheDel, 250)
	configureTimeout(ProductsCacheGet, 250)
	configureTimeout(ProductsCacheSet, 250)
	configureTimeout(ProductsCacheDel, 250)
	configureTimeout(CampaignsUserCacheGet, 250)
	configureTimeout(CampaignsUserCacheSet, 250)
	configureTimeout(CampaignsUserCacheDel, 250)
	configureTimeout(CampaignsChannelCacheGet, 250)
	configureTimeout(CampaignsChannelCacheSet, 250)
	configureTimeout(CampaignsChannelCacheDel, 250)
	configureTimeout(CampaignsEligibleCacheGet, 250)
	configureTimeout(CampaignsEligibleCacheSet, 250)
	configureTimeout(CampaignsEligibleCacheDel, 250)
	configureTimeout(ActionsCacheGet, 250)
	configureTimeout(ActionsCacheSet, 250)
	configureTimeout(ActionsCacheDel, 250)
	configureTimeout(UserCacheGet, 250)
	configureTimeout(UserCacheSet, 250)
	configureTimeout(UserCacheDel, 250)
	configureTimeout(UserServiceCacheGet, 250)
	configureTimeout(UserServiceCacheSet, 250)
	configureTimeout(UserServiceCacheDel, 250)
	configureTimeout(ChatBadgesCacheGet, 100)
	configureTimeout(ChatBadgesCacheSet, 250)
	configureTimeout(ChatBadgesCacheDel, 250)
	configureTimeout(ThrottleGet, 250)
	configureTimeout(ThrottleSet, 250)
	configureTimeout(ExperimentGet, 250)
	configureTimeout(ExperimentSet, 250)
	configureTimeout(PushyStatusCacheGet, 250)
	configureTimeout(PushyStatusCacheSet, 250)
	configureTimeout(TierNotificationsCacheGet, 250)
	configureTimeout(TierNotificationsCacheSet, 250)
	configureTimeout(TierNotificationsCacheDel, 250)
	configureTimeout(ProductsRecentPlatformCacheGet, afex.DefaultTimeout)
	configureTimeout(HoldsCacheGet, 250)
	configureTimeout(HoldsCacheSet, 250)
	configureTimeout(HoldsCacheDel, 250)
	configureTimeout(EmoticonUploadConfigsCacheGet, 250)
	configureTimeout(EmoticonUploadConfigsCacheSet, 250)
	configureTimeout(EmoticonUploadConfigsCacheDel, 250)
	configureTimeout(EmoticonImageUploadCacheGet, 250)
	configureTimeout(EmoticonImageUploadCacheSet, 250)
	configureTimeout(EmoticonImageUploadCacheDel, 250)
	configureTimeout(EmoticonNumberPerTierCacheGet, 250)
	configureTimeout(EmoticonNumberPerTierCacheSet, 250)
	configureTimeout(EmoticonNumberPerTierCacheDel, 250)

	// Dynamo timeouts
	configureTimeout(ActionsDBGet, 500)
	configureTimeout(ActionsDBBatchGet, 500)
	configureTimeout(ActionsDBGetAll, 500)
	configureTimeout(ActionsDBUpdate, 500)
	configureTimeout(ActionsDBDelete, 500)
	configureTimeout(BadgeTierEmoteGetGroupIds, 100)
}

func configureTimeout(cmd string, timeout int) {
	configure(cmd, timeout)
}

func configure(cmd string, timeout int) {
	afex.ConfigureCommand(cmd, afex.CommandConfig{
		Timeout: timeout,
		// We explicitly do not want to use hystrix max concurrency see: https://twitch.slack.com/archives/G5LTHELHL/p1616186139083300
		// Set this high enough so that max concurrency is never utilized, but low enough so that we are not at risk of going OOM here:
		//      https://github.com/afex/hystrix-go/blob/master/hystrix/pool.go#L17
		MaxConcurrentRequests: 1000,
		ErrorPercentThreshold: afex.DefaultErrorPercentThreshold,
	})
}

// Only used for test config
func configureForHighFailureTolerance(cmd string, timeout int) {
	afex.ConfigureCommand(cmd, afex.CommandConfig{
		Timeout:               timeout,
		MaxConcurrentRequests: 1000,
		ErrorPercentThreshold: 100,
	})
}

func ConfigureTimeoutOverride(cmd string, timeout int) {
	configureTimeout(cmd, timeout)
}

func ConfigureForTests() {
	configureTimeout(UsersCommand, TestTimeout)
	configureTimeout(UpdateTransactionsCommand, TestTimeout)
	configureTimeout(CreateNewTransactionCommand, TestTimeout)
	configureTimeout(GetTransactionsCommand, TestTimeout)
	configureTimeout(GetBalanceCommand, TestTimeout)
	configureTimeout(GetChannelCommand, TestTimeout)
	configureTimeout(GetChannelRedisCommand, TestTimeout)
	configureTimeout(GetHighestBadgeCommand, TestTimeout)
	configureTimeout(GetImageCommand, TestTimeout)
	configureTimeout(GetImageByAssetIdCommand, TestTimeout)
	configureTimeout(UpdateImageCommand, TestTimeout)
	configureTimeout(BatchDeleteImagesCommand, TestTimeout)
	configureTimeout(GetTaxInfo, TestTimeout)
	configureTimeout(GetChannelByImageSetIdCommand, TestTimeout)
	configureTimeout(GetUserPayoutType, TestTimeout)
	configureTimeout(GetPayoutEntityForChannelCommand, TestTimeout)
	configureTimeout(GetRewardCommand, TestTimeout)
	configureTimeout(UpdateRewardCommand, TestTimeout)
	configureTimeout(GetExtensionByChannelIdCommand, TestTimeout)
	configureTimeout(FaladorGetProductCommand, TestTimeout)

	// Extension Transactions DAO Timeouts
	configureTimeout(GetExtensionTransactionCommand, TestTimeout)

	// OWL Timeouts
	configureTimeout(OWLGetClientCommand, TestTimeout)

	// Zuma Timeouts
	configureTimeout(ZumaListModCommand, TestTimeout)
	configureTimeout(ZumaGetModCommand, TestTimeout)
	configureTimeout(ZumaEnforceMessageCommand, TestTimeout)

	// Mako Timeouts
	configureTimeout(MakoCreateEmoteGroupEntitlements, TestTimeout)
	configureTimeout(MakoGetAllEmotesByOwnerId, TestTimeout)
	configureTimeout(MakoGetEmoticonsByGroups, TestTimeout)
	configureTimeout(MakoDashboardGetEmotesByGroups, TestTimeout)
	configureTimeout(MakoCreateEmoticon, TestTimeout)
	configureTimeout(MakoCreateEmote, TestTimeout)
	configureTimeout(MakoDeleteEmoticon, TestTimeout)
	configureTimeout(MakoBatchValidateCodeUniqueness, TestTimeout)
	configureTimeout(MakoBatchValidateEmoteCodeAcceptability, TestTimeout)
	configureTimeout(MakoCreateEmoteGroup, TestTimeout)
	configureTimeout(MakoGetPrefix, TestTimeout)
	configureTimeout(MakoAssignEmoteToGroup, TestTimeout)

	// Pachinko Timeouts
	configureForHighFailureTolerance(PachinkoGetBalanceCommand, TestTimeout)
	configureForHighFailureTolerance(PachinkoAddTokensCommand, TestTimeout)
	configureForHighFailureTolerance(PachinkoCheckHoldCommand, TestTimeout)
	configureForHighFailureTolerance(PachinkoStartTransactionCommand, TestTimeout)
	configureForHighFailureTolerance(PachinkoCommitTransactionCommand, TestTimeout)
	configureForHighFailureTolerance(PachinkoOverrideBalanceCommand, TestTimeout)
	configureForHighFailureTolerance(PachinkoDeleteBalanceByOwnerId, TestTimeout)

	// Pantheon Timeouts
	configureTimeout(GetLeaderboardCommand, TestTimeout)
	configureTimeout(GetLeaderboardsCommand, TestTimeout)
	configureTimeout(GetLeaderboardEntriesAboveThresholdCommand, TestTimeout)
	configureTimeout(PublishEventCommand, TestTimeout)
	configureTimeout(ModerateEntryCommand, TestTimeout)
	configureTimeout(PantheonDeleteLeaderboards, TestTimeout)
	configureTimeout(PantheonDeleteEntries, TestTimeout)

	// PDMS Timeouts
	configureTimeout(PDMSReportDeletion, TestTimeout)
	configureTimeout(PDMSPromiseDeletion, TestTimeout)
	configureTimeout(PDMSSetTestDeletion, TestTimeout)

	// Pachter Timeouts
	configureForHighFailureTolerance(PachterGetBitsSpendOrderCommand, TestTimeout)
}
