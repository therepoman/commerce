package hystrix

import (
	gogoctx "code.justin.tv/commerce/gogogadget/ctx"
	hystrixgo "github.com/afex/hystrix-go/hystrix" //nolint:depguard
)

// Re-exporting these so that we don't need to constantly add exceptions for the no afex/hystrix linting rule.
// TODO: when we move to circuit, refactor away usage of these
var (
	Go         = hystrixgo.Go
	ErrTimeout = hystrixgo.ErrTimeout
)

// Do provides the same functionality as hystrix.Do but ensures the
// circuit is not affected by context.Canceled errors.
// Credit: https://git-aws.internal.justin.tv/liverecs/serving/blob/master/internal/contextutil/hystrix.go#L9
func Do(command string, fn func() error, fallback func(error) error) error {
	var innerErr error

	err := hystrixgo.Do(command, func() error {
		innerErr = fn()
		if gogoctx.IsCancelled(innerErr) {
			return nil
		}
		return innerErr
	}, fallback)

	if err == nil && innerErr != nil {
		// We don't want context cancellation to trip the hystrix circuit, but we still want to bubble them
		err = innerErr
	}
	return err
}
