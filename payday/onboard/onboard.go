package onboard

import (
	"context"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/api/utils"
	"code.justin.tv/commerce/payday/cache/channel"
	"code.justin.tv/commerce/payday/datascience"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/middleware"
	"code.justin.tv/commerce/payday/models"
	partnerManager "code.justin.tv/commerce/payday/partner"
	"code.justin.tv/commerce/payday/payout"
	"code.justin.tv/commerce/payday/utils/pointers"
)

type IOnboardManager interface {
	OnboardForBits(ctx context.Context, channelID string) error
}

type OnboardManager struct {
	ChannelManager    channel.ChannelManager         `inject:""`
	PartnerManager    partnerManager.IPartnerManager `inject:""`
	PayoutManager     payout.IPayoutManager          `inject:""`
	DataScienceClient datascience.DataScience        `inject:""`
}

func (m *OnboardManager) trackOnboardEvent(ctx context.Context, channelId string, partnerType partnerManager.PartnerType) {
	po := &models.PartnerOnboarded{ChannelID: channelId, PartnerType: string(partnerType)}
	err := m.DataScienceClient.TrackBitsEvent(ctx, datascience.PartnerOnboardedEventName, po)
	if err != nil {
		log.WithError(err).Error("Error tracking enable bits onboarding event with data science")
	}
}

func (m *OnboardManager) OnboardForBits(ctx context.Context, channelID string) error {
	ctx, cancel := context.WithTimeout(ctx, utils.DefaultTimeout)
	defer cancel()

	hasServices, err := m.PayoutManager.HasServicesTaxInfo(ctx, channelID)

	if err != nil {
		msg := "Could not get services from payout manager"
		log.WithError(err).Error(msg)
		return errors.New(msg)
	}

	if !hasServices {
		msg := "Partner does not currently have a services record"
		log.WithField("channelID", channelID).Error(msg)
		return errors.New(msg)
	}

	partnerType, err := m.PartnerManager.GetPartnerType(ctx, channelID)
	if err != nil {
		log.WithError(err).WithField("channel_id", channelID).Info("Could not get partner type")
		return errors.Notef(err, "Could not get partner type for channelID %s", channelID)
	}

	if partnerType == partnerManager.NonPartnerPartnerType {
		msg := "Received onboard request for non-partner channelID"
		log.WithField("channelID", channelID).Error(msg)
		return errors.New(msg)
	}

	var amendmentState *string
	if partnerType == partnerManager.TraditionalPartnerType {
		amendmentState = pointers.StringP(string(partnerManager.BitsAmendmentStateSigned))
	}

	updatedChannel := models.OnboardedDynamoChannel(channelID, amendmentState)
	err = m.ChannelManager.Update(ctx, updatedChannel)
	if err != nil {
		msg := "Error updating dynamo to set channel as onboarded"
		log.WithError(err).Error(msg)
		return errors.Notef(err, msg)
	}

	newCtx, cancel := middleware.CopyContextValues(ctx)
	go func() {
		defer cancel()
		m.trackOnboardEvent(newCtx, channelID, partnerType)
	}()

	return nil
}
