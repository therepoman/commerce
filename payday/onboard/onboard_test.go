package onboard_test

import (
	"context"
	"testing"

	"code.justin.tv/commerce/payday/datascience"
	"code.justin.tv/commerce/payday/dynamo"
	channel_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/cache/channel"
	partner_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/partner"
	payout_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/payout"
	"code.justin.tv/commerce/payday/onboard"
	"code.justin.tv/commerce/payday/partner"
	"github.com/go-errors/errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

var testChannel = "1"
var testError = errors.New("Test Error")

type testContext struct {
	channelID           string
	hasServices         bool
	payoutManagerErr    error
	partnerManagerErr   error
	partnerType         partner.PartnerType
	channelDAOUpdateErr error
}

type testMocks struct {
	cm             *channel_mock.ChannelManager
	pm             *partner_mock.IPartnerManager
	updatedChannel *dynamo.Channel
}

func setup(t *testing.T, c testContext) (onboard.IOnboardManager, testMocks) {
	channelManager := new(channel_mock.ChannelManager)
	partnerManager := new(partner_mock.IPartnerManager)
	payoutManager := new(payout_mock.IPayoutManager)

	updatedChannel := &dynamo.Channel{}

	payoutManager.On("HasServicesTaxInfo", mock.Anything, c.channelID).Return(c.hasServices, c.payoutManagerErr)
	partnerManager.On("GetPartnerType", mock.Anything, c.channelID).Return(c.partnerType, c.partnerManagerErr)
	channelManager.On("Update", mock.Anything, mock.Anything).Return(c.channelDAOUpdateErr).Run(func(arguments mock.Arguments) {
		*updatedChannel = *arguments[1].(*dynamo.Channel)
	})

	return &onboard.OnboardManager{
			ChannelManager:    channelManager,
			PartnerManager:    partnerManager,
			PayoutManager:     payoutManager,
			DataScienceClient: &datascience.DataScienceApiNoOp{},
		}, testMocks{
			cm:             channelManager,
			pm:             partnerManager,
			updatedChannel: updatedChannel,
		}
}

func TestOnboardForBits_NoTaxInfo(t *testing.T) {
	om, _ := setup(t, testContext{
		channelID:   testChannel,
		hasServices: false,
	})

	err := om.OnboardForBits(context.Background(), testChannel)
	assert.Error(t, err)
}

func TestOnboardForBits_PaymentManagerError(t *testing.T) {
	om, _ := setup(t, testContext{
		channelID:        testChannel,
		payoutManagerErr: testError,
	})

	err := om.OnboardForBits(context.Background(), testChannel)
	assert.Error(t, err)
}

func TestOnboardForBits_PartnerManagerErrors(t *testing.T) {
	om, _ := setup(t, testContext{
		channelID:         testChannel,
		hasServices:       true,
		partnerManagerErr: testError,
	})
	err := om.OnboardForBits(context.Background(), testChannel)
	assert.Error(t, err)
}

func TestOnboardForBits_NotPartner(t *testing.T) {
	om, _ := setup(t, testContext{
		channelID:   testChannel,
		hasServices: true,
		partnerType: partner.NonPartnerPartnerType,
	})
	err := om.OnboardForBits(context.Background(), testChannel)
	assert.Error(t, err)
}

func TestOnboardForBits_TraditionalPartner_SignsAmendment(t *testing.T) {
	om, m := setup(t, testContext{
		channelID:   testChannel,
		hasServices: true,
		partnerType: partner.TraditionalPartnerType,
	})
	err := om.OnboardForBits(context.Background(), testChannel)

	m.cm.AssertNumberOfCalls(t, "Update", 1)
	assert.Equal(t, "signed", *m.updatedChannel.AmendmentState)

	assert.NoError(t, err)
}

func TestOnboardForBits_AffiliatePartner_SkipsSigning(t *testing.T) {
	om, m := setup(t, testContext{
		channelID:   testChannel,
		hasServices: true,
		partnerType: partner.AffiliatePartnerType,
	})
	err := om.OnboardForBits(context.Background(), testChannel)

	m.cm.AssertNumberOfCalls(t, "Update", 1)
	assert.Nil(t, m.updatedChannel.AmendmentState)

	assert.NoError(t, err)
}

func TestOnboardForBits_NotPreviouslyOnboarded_ErrorsUpdatingDynamo(t *testing.T) {
	om, m := setup(t, testContext{
		channelID:           testChannel,
		partnerType:         partner.AffiliatePartnerType,
		hasServices:         true,
		channelDAOUpdateErr: testError,
	})
	err := om.OnboardForBits(context.Background(), testChannel)
	m.cm.AssertNumberOfCalls(t, "Update", 1)
	assert.Error(t, err)
}

func TestOnboardForBits_NotPreviouslyOnboarded_SucceedsUpdatingDynamo(t *testing.T) {
	om, m := setup(t, testContext{
		channelID:   testChannel,
		hasServices: true,
		partnerType: partner.AffiliatePartnerType,
	})
	err := om.OnboardForBits(context.Background(), testChannel)
	m.cm.AssertNumberOfCalls(t, "Update", 1)
	assert.NoError(t, err)
}
