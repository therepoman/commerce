package bulk_promo_notif

import (
	"time"

	log "code.justin.tv/commerce/logrus"
)

type Campaign struct {
	ID           string
	PromoTitle   string
	StartDate    time.Time
	EndDate      time.Time
	BitsAmount   int64
	Discount     int64
	ClickURL     string
	IconURL      string
	PushyPromoID string
	Date         string
}

var Campaigns = map[string]Campaign{
	"black-friday-2018": {
		ID:           "black-friday-2018",
		PromoTitle:   "Black Friday 2018",
		StartDate:    timeFromStr("21 Nov 18 10:00 PST"),
		EndDate:      timeFromStr("26 Nov 18 23:59 PST"),
		BitsAmount:   2500,
		Discount:     29,
		ClickURL:     "https://bits.twitch.tv/blizzard-of-bits",
		IconURL:      "https://d3aqoihi2n8ty8.cloudfront.net/notifications/bits-gem.png",
		PushyPromoID: "black-friday-2018",
	},
	"spring_sale_2019": {
		ID:           "spring_sale_2019",
		PromoTitle:   "Spring Sale 2019",
		BitsAmount:   2500,
		Discount:     0,
		ClickURL:     "https://bits.twitch.tv/spring-sale",
		IconURL:      "https://d3aqoihi2n8ty8.cloudfront.net/notifications/bits-gem.png",
		PushyPromoID: "spring_sale_2019",
		Date:         "3/31",
	},
}

func timeFromStr(timeStr string) time.Time {
	startTime, err := time.Parse(time.RFC822, "04 Dec 18 00:00 PST")
	if err != nil {
		log.WithError(err).WithField("time_str", timeStr).Error("error parsing time string")
		return time.Now()
	}
	return startTime
}

func GetCampaign(campaignID string) *Campaign {
	c, ok := Campaigns[campaignID]
	if !ok {
		return nil
	}
	return &c
}
