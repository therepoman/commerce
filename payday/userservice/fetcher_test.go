package userservice

import (
	"context"
	"testing"

	"github.com/stretchr/testify/mock"

	userservice_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/cache/userservice"
	userservice_mock2 "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/userservice"
	"code.justin.tv/web/users-service/models"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
)

func TestFetcher_Fetch(t *testing.T) {
	Convey("given a user service fetcher", t, func() {
		ctx := context.Background()
		cache := new(userservice_mock.Cache)
		retriever := new(userservice_mock2.Retriever)

		fetcher := &fetcher{
			Cache:     cache,
			Retriever: retriever,
		}

		userID := "123123123"
		user := &models.Properties{
			ID: userID,
		}

		Convey("when we find a record in the cache", func() {
			cache.On("Get", mock.Anything, userID).Return(user)

			Convey("then we return the cached user", func() {
				actual, err := fetcher.Fetch(ctx, userID)

				So(err, ShouldBeNil)
				So(actual, ShouldNotBeNil)
			})
		})

		Convey("when we don't find a record in the cache", func() {
			cache.On("Get", mock.Anything, userID).Return(nil)

			Convey("when we error looking up the user from the retriever", func() {
				retriever.On("Retrieve", userID).Return(nil, errors.New("WALRUS STRIKE"))

				Convey("then we return an error", func() {
					actual, err := fetcher.Fetch(ctx, userID)

					So(err, ShouldNotBeNil)
					So(actual, ShouldBeNil)
				})
			})

			Convey("when we do not find a user from the retriever", func() {
				retriever.On("Retrieve", userID).Return(nil, nil)

				Convey("then we return nil", func() {
					actual, err := fetcher.Fetch(ctx, userID)

					So(err, ShouldBeNil)
					So(actual, ShouldBeNil)
				})
			})

			Convey("when we find a user", func() {
				retriever.On("Retrieve", userID).Return(user, nil)

				Convey("then we set the user in the cache and we return the user", func() {
					cache.On("Set", mock.Anything, userID, user).Return()
					actual, err := fetcher.Fetch(ctx, userID)

					So(err, ShouldBeNil)
					So(actual, ShouldNotBeNil)

					cache.AssertCalled(t, "Set", mock.Anything, userID, user)
				})
			})
		})
	})
}
