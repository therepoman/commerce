package userservice

import (
	"context"
	"time"

	"code.justin.tv/commerce/payday/api/utils"
	"code.justin.tv/web/users-service/models"
)

type Checker interface {
	IsStaff(ctx context.Context, tuid string) bool
	IsBanned(ctx context.Context, tuid string) (bool, error)
}

func NewChecker() Checker {
	return &checker{}
}

type checker struct {
	UserServiceFetcher Fetcher `inject:""`
}

func (c *checker) IsStaff(ctx context.Context, tuid string) bool {
	if tuid == utils.DefaultTUID || tuid == "" {
		return false
	}

	user, err := c.UserServiceFetcher.Fetch(ctx, tuid)
	if err != nil {
		return false
	}

	if user != nil && user.Admin != nil {
		return *user.Admin
	}

	return false
}

func (c *checker) IsBanned(ctx context.Context, tuid string) (bool, error) {
	if tuid == utils.DefaultTUID || tuid == "" {
		return false, nil
	}

	user, err := c.UserServiceFetcher.Fetch(ctx, tuid)
	if err != nil {
		return false, err
	}

	return user != nil && isBanned(*user), nil
}

func isBanned(user models.Properties) bool {
	return (user.BannedUntil != nil && user.BannedUntil.After(time.Now())) ||
		(user.TermsOfServiceViolation != nil && *user.TermsOfServiceViolation) ||
		(user.DmcaViolation != nil && *user.DmcaViolation)
}
