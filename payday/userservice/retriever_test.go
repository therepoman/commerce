package userservice

import (
	"testing"

	user_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/user"
	"code.justin.tv/web/users-service/models"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestRetriever_Retrieve(t *testing.T) {
	Convey("given a user service retriever", t, func() {
		userServiceClient := new(user_mock.IUserServiceClientWrapper)

		retriever := &retriever{
			UserServiceClient: userServiceClient,
		}

		userID := "123123123"

		Convey("when user service returns an error", func() {
			userServiceClient.On("GetUserByID", mock.Anything, userID, mock.Anything).Return(nil, errors.New("WALRUS STRIKE"))

			Convey("then we return an error", func() {
				_, err := retriever.Retrieve(userID)

				So(err, ShouldNotBeNil)
			})
		})

		Convey("when user service is called successfully", func() {
			user := &models.Properties{
				ID: userID,
			}

			userServiceClient.On("GetUserByID", mock.Anything, userID, mock.Anything).Return(user, nil)

			Convey("then we return the user", func() {
				resp, err := retriever.Retrieve(userID)

				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
			})
		})
	})
}
