package userservice

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/mock"

	"code.justin.tv/commerce/payday/api/utils"
	"code.justin.tv/commerce/payday/errors"
	userservice_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/userservice"
	"code.justin.tv/web/users-service/models"
	. "github.com/smartystreets/goconvey/convey"
)

func TestChecker_IsBanned(t *testing.T) {
	Convey("given a user service fetcher", t, func() {
		ctx := context.Background()
		fetcher := new(userservice_mock.Fetcher)

		checker := &checker{
			UserServiceFetcher: fetcher,
		}

		userID := "123123123"
		user := &models.Properties{
			ID: userID,
		}

		Convey("when we request with default tuid", func() {
			banned, err := checker.IsBanned(ctx, utils.DefaultTUID)
			So(banned, ShouldBeFalse)
			So(err, ShouldBeNil)
		})

		Convey("when we request with empty string", func() {
			banned, err := checker.IsBanned(ctx, "")
			So(banned, ShouldBeFalse)
			So(err, ShouldBeNil)
		})

		Convey("when user service returns an error", func() {
			fetcher.On("Fetch", mock.Anything, userID).Return(nil, errors.New("ERROR"))

			banned, err := checker.IsBanned(ctx, userID)
			So(banned, ShouldBeFalse)
			So(err, ShouldNotBeNil)
		})

		Convey("when user service returns a user", func() {
			fetcher.On("Fetch", mock.Anything, userID).Return(user, nil)

			Convey("when user has no ban fields", func() {
				banned, err := checker.IsBanned(ctx, userID)
				So(banned, ShouldBeFalse)
				So(err, ShouldBeNil)
			})

			Convey("when user service returns a user timed out", func() {
				bannedUntil, err := time.Parse(time.RFC3339, "2554-01-02T15:04:05Z")
				So(err, ShouldBeNil)
				user.BannedUntil = &bannedUntil
				banned, err := checker.IsBanned(ctx, userID)
				So(banned, ShouldBeTrue)
				So(err, ShouldBeNil)
			})

			Convey("when user service returns a user with TOS violation", func() {
				tosViolation := true
				user.TermsOfServiceViolation = &tosViolation
				banned, err := checker.IsBanned(ctx, userID)
				So(banned, ShouldBeTrue)
				So(err, ShouldBeNil)
			})

			Convey("when user service returns a user with DMCA violation", func() {
				dmcaViolation := true
				user.DmcaViolation = &dmcaViolation
				banned, err := checker.IsBanned(ctx, userID)
				So(banned, ShouldBeTrue)
				So(err, ShouldBeNil)
			})
		})
	})
}
