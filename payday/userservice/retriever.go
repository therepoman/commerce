package userservice

import (
	"context"
	"time"

	"code.justin.tv/commerce/payday/clients/user"
	"code.justin.tv/commerce/payday/hystrix"
	cmd "code.justin.tv/commerce/payday/hystrix"
	"code.justin.tv/web/users-service/models"
)

const userServiceTimeout = 5 * time.Second

type Retriever interface {
	Retrieve(userID string) (*models.Properties, error)
}

func NewRetriever() Retriever {
	return &retriever{}
}

type retriever struct {
	UserServiceClient user.IUserServiceClientWrapper `inject:""`
}

func (r *retriever) Retrieve(userID string) (*models.Properties, error) {
	responseChannel := make(chan *models.Properties, 1)

	ctx, cancel := context.WithTimeout(context.Background(), userServiceTimeout)
	defer cancel()

	getUserCall := func() error {
		output, err := r.UserServiceClient.GetUserByID(ctx, userID, nil)
		if err != nil {
			return err
		}
		responseChannel <- output
		return nil
	}

	err := hystrix.Do(cmd.UsersCommand, getUserCall, nil)
	if err != nil {
		return nil, err
	}

	return <-responseChannel, nil
}
