package userservice

import (
	"context"

	"code.justin.tv/commerce/payday/cache/userservice"
	"code.justin.tv/web/users-service/models"
)

type Fetcher interface {
	Fetch(ctx context.Context, userID string) (*models.Properties, error)
}

func NewFetcher() Fetcher {
	return &fetcher{}
}

type fetcher struct {
	Cache     userservice.Cache `inject:""`
	Retriever Retriever         `inject:""`
}

func (f *fetcher) Fetch(ctx context.Context, userID string) (*models.Properties, error) {
	cachedUser := f.Cache.Get(ctx, userID)

	if cachedUser != nil {
		return cachedUser, nil
	}

	userServiceResp, err := f.Retriever.Retrieve(userID)

	if err != nil || userServiceResp == nil {
		return nil, err
	}

	f.Cache.Set(ctx, userID, userServiceResp)

	return userServiceResp, nil
}
