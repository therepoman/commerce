package models

// Event contains the structure of the Event coming from the Event SQS queue
type MakoEvent struct {
	UserID string `json:"user_id"` // Twitch user id
	Scope  string `json:"scope"`   // Scope for this event, registered during onboarding. Must be a valid event scope.
	Key    string `json:"key"`     // The unique key for this event.
	Origin string `json:"origin"`  // The source of the event
	Value  string `json:"value"`   // The value of the event
}
