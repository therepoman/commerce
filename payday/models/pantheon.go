package models

type PantheonSNSMessage struct {
	Top          []PantheonSNSLeaderboardEntry      `json:"top"`
	EntryContext PantheonSNSLeaderboardEntryContext `json:"entry_context"`
	Identifier   PantheonSNSLeaderboardIdentifier   `json:"identifier"`
	Event        PantheonSNSLeaderboardEvent        `json:"event"`
}

type PantheonSNSLeaderboardEntryContext struct {
	Entry   PantheonSNSLeaderboardEntry   `json:"entry"`
	Context []PantheonSNSLeaderboardEntry `json:"context"`
}

type PantheonSNSLeaderboardEntry struct {
	Rank     int64  `json:"rank"`
	Score    int64  `json:"score"`
	EntryKey string `json:"entry_key"`
}

type PantheonSNSLeaderboardIdentifier struct {
	Domain              string `json:"domain"`
	GroupingKey         string `json:"grouping_key"`
	TimeAggregationUnit string `json:"time_aggregation_unit"`
	TimeBucket          string `json:"time_bucket"`
}

type PantheonSNSLeaderboardEvent struct {
	Domain      string `json:"domain"`
	ID          string `json:"id"`
	TimeOfEvent int64  `json:"time_of_event"`
	GroupingKey string `json:"grouping_key"`
	EntryKey    string `json:"entry_key"`
	EventValue  int64  `json:"event_value"`
}
