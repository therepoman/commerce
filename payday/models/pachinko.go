package models

type PachinkoSNSUpdate struct {
	TransactionIDs []string `json:"transaction_ids"`
}
