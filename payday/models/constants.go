package models

import (
	"strings"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/utils/pointers"
	petozi "code.justin.tv/commerce/petozi/rpc"
)

const (
	EventReasonGenericAdminAdd    = "ActiveAdminAddBits"
	EventReasonGenericAdminRemove = "ActiveAdminRemoveBits"
	EventReasonEntitleBits        = "EntitleBits"
	EventReasonBitsForAds         = "BitsForAdsGrant"
	EventReasonCheer              = "Cheer"
	QA_BITS_PARTNER_TUID          = "116076154"
)

type BitsType string
type Platform string
type SuperPlatform string

const (
	// BEFORE CHANGING ANY OF THESE VALUES, SEE: https://twitchtv.atlassian.net/wiki/display/ENG/How+to+create+new+bit+account+types
	CB_0                           BitsType = "CB_0"
	CB_140_CBP_00140               BitsType = "CB_140_CBP_00140"
	CB_700_CBP_00140               BitsType = "CB_700_CBP_00140"
	CB_1995_CBP_00133              BitsType = "CB_1995_CBP_00133"
	CB_6440_CBP_00129              BitsType = "CB_6440_CBP_00129"
	CB_12600_CBP_00126             BitsType = "CB_12600_CBP_00126"
	CB_30800_CBP_00120             BitsType = "CB_30800_CBP_00120"
	CB_700_CBP_00100               BitsType = "CB_700_CBP_00100"
	BitsForAds                     BitsType = "BitsForAds"
	BitsForAdsX                    BitsType = "BitsForAdsX"
	BitsForAdsUpsight              BitsType = "BitsForAdsUpsight"
	BitsForCrate                   BitsType = "BitsForCrate"
	CB_400_JACK_RYAN_PROMO         BitsType = "CB_400_JACK_RYAN_PROMO"
	CB_PRIME_VIDEO_THE_PURGE_PROMO BitsType = "CB_PRIME_VIDEO_THE_PURGE_PROMO"
	CB_999_CBP_00100               BitsType = "CB_999_CBP_00100"
	CB_1000_CBP_00100              BitsType = "CB_1000_CBP_00100"
	CB_1999_CBP_00100              BitsType = "CB_1999_CBP_00100"
	CB_2000_CBP_00100              BitsType = "CB_2000_CBP_00100"
	CB_3700_CBP_00100              BitsType = "CB_3700_CBP_00100"
	CB_9999_CBP_00100              BitsType = "CB_9999_CBP_00100"
	CB_10000_CBP_00100             BitsType = "CB_10000_CBP_00100"
	CB_100_CBP_00020               BitsType = "CB_100_CBP_00020"
	CB_200_CBP_00020               BitsType = "CB_200_CBP_00020"
	CB_350_PROMO                   BitsType = "CB_350_PROMO"
	CB_2500_CBP_100                BitsType = "CB_2500_CBP_100"
	BitsForOWL                     BitsType = "BitsForOWL"
	BitsForOWL2019                 BitsType = "BitsForOWL2019"
	BitsForHGC                     BitsType = "BitsForHGC"
	BitsForCode                    BitsType = "BitsForCode"
	PinataBits                     BitsType = "PinataBits"
)

const (
	IOS          Platform = "IOS"
	ANDROID      Platform = "ANDROID"
	AMAZON       Platform = "AMAZON"
	PAYPAL       Platform = "PAYPAL"
	UPSIGHT      Platform = "UPSIGHT"
	TRUEX        Platform = "TRUEX"
	FORTUNA      Platform = "FORTUNA" // For Bits as reward processed in Fortuna service
	PHANTO       Platform = "PHANTO"  // For redeeming key code in Phanto service
	XSOLLA       Platform = "XSOLLA"
	VANTIV       Platform = "VANTIV"
	STRIPE       Platform = "STRIPE"
	WALLET       Platform = "WALLET"
	UNKNOWN      Platform = "UNKNOWN"
	ADYEN        Platform = "ADYEN"
	ADYEN_LUX    Platform = "ADYEN_LUX"
	WORLDPAY     Platform = "WORLDPAY"
	WORLDPAY_LUX Platform = "WORLDPAY_LUX"
	DEFAULT      Platform = "default"
)

const (
	SuperPlatform_IOS     SuperPlatform = "IOS"
	SuperPlatform_ANDROID SuperPlatform = "ANDROID"
	SuperPlatform_WEB     SuperPlatform = "WEB"
	SuperPlatform_UPSIGHT SuperPlatform = "UPSIGHT"
	SuperPlatform_PHANTO  SuperPlatform = "PHANTO"
	SuperPlatform_TRUEX   SuperPlatform = "TRUEX"
	SuperPlatform_NONE    SuperPlatform = "NONE"
)

var BitsTypes = []BitsType{
	CB_0,
	CB_140_CBP_00140,
	CB_700_CBP_00140,
	CB_1995_CBP_00133,
	CB_6440_CBP_00129,
	CB_12600_CBP_00126,
	CB_30800_CBP_00120,
	CB_700_CBP_00100,
	CB_2000_CBP_00100,
	CB_400_JACK_RYAN_PROMO,
	CB_PRIME_VIDEO_THE_PURGE_PROMO,
	CB_999_CBP_00100,
	CB_1000_CBP_00100,
	CB_1999_CBP_00100,
	CB_3700_CBP_00100,
	CB_9999_CBP_00100,
	CB_10000_CBP_00100,
	CB_100_CBP_00020,
	CB_200_CBP_00020,
	CB_350_PROMO,
	CB_2500_CBP_100,
	BitsForAds,
	BitsForAdsX,
	BitsForCrate,
	BitsForOWL,
	BitsForOWL2019,
	BitsForHGC,
	BitsForCode,
	PinataBits,
}

func ValidateBitsType(bitsTypeStr string) error {
	for _, bitsType := range BitsTypes {
		if bitsTypeStr == string(bitsType) {
			return nil
		}
	}
	return errors.Newf("No bitsType found named: %s", bitsTypeStr)
}

/**
 * A generic method to put platforms into the constants I want them to be, which I'm using as a poor man's enum.
 * I convert to lower so I can support any arrangement (Pachinko uses upper, api uses lower) and I got frustrated maintaining
 * two versions of this method, so I consolidated and traded a small bit of performance.
 */
func ParsePlatform(platform string, callerContext *string, transactionID *string, bitsProductName *string) Platform {
	switch strings.ToLower(platform) {
	case "amazon":
		return AMAZON
	case "paypal":
		return PAYPAL
	case "android":
		return ANDROID
	case "ios":
		return IOS
	case "upsight":
		return UPSIGHT
	case "truex":
		return TRUEX
	case "fortuna":
		return FORTUNA
	case "phanto":
		return PHANTO
	case "xsolla":
		return XSOLLA
	case "vantiv":
		return VANTIV
	case "stripe":
		return STRIPE
	case "wallet":
		return WALLET
	case "unknown":
		return UNKNOWN
	case "adyen":
		return ADYEN
	case "adyen_lux":
		return ADYEN_LUX
	case "worldpay":
		return WORLDPAY
	case "worldpay_lux":
		return WORLDPAY_LUX
	case "":
		fallthrough
	case "default":
		return DEFAULT
	default:
		log.WithFields(log.Fields{
			"transaction_id":    pointers.StringOrDefault(transactionID, ""),
			"bits_product_name": pointers.StringOrDefault(bitsProductName, ""),
			"caller_context":    pointers.StringOrDefault(callerContext, ""),
			"platform":          platform,
		}).Warn("Unhandled platform attempting to be parsed")
		return DEFAULT
	}
}

func ParsePlatforms(platforms []*string) []Platform {
	var parsedPlatforms = make([]Platform, 0)
	for _, platform := range platforms {
		if platform != nil {
			parsedPlatforms = append(parsedPlatforms, ParsePlatform(*platform, pointers.StringP("ParsePlatforms"), nil, nil))
		}
	}
	return parsedPlatforms
}

func IsPlatformWateb(platform Platform) bool {
	return platform == UPSIGHT || platform == TRUEX
}

func IsPlatformMobile(platform Platform) bool {
	return platform == IOS || platform == ANDROID
}

func ArePlatformsWeb(platforms []Platform) bool {
	for _, platform := range platforms {
		if platform == AMAZON || platform == PAYPAL || platform == XSOLLA {
			return true
		}
	}
	return false
}

func IsPlatformEntitlingService(platform Platform) bool {
	return platform == FORTUNA
}

func IsQuantityEntitlingPlatform(platform Platform) bool {
	return IsPlatformWateb(platform) || IsPlatformEntitlingService(platform)
}

func ParsePetoziPlatform(platform string) petozi.Platform {
	petoziPlatform, ok := petozi.Platform_value[strings.ToUpper(platform)]
	if !ok {
		return petozi.Platform_NONE
	}

	return petozi.Platform(petoziPlatform)
}

func ConvertPetoziPlatform(platform petozi.Platform) Platform {
	switch platform {
	case petozi.Platform_AMAZON:
		return AMAZON
	case petozi.Platform_PAYPAL:
		return PAYPAL
	case petozi.Platform_IOS:
		return IOS
	case petozi.Platform_ANDROID:
		return ANDROID
	case petozi.Platform_XSOLLA:
		return XSOLLA
	case petozi.Platform_PHANTO:
		return PHANTO
	case petozi.Platform_VANTIV:
		return VANTIV
	case petozi.Platform_STRIPE:
		return STRIPE
	case petozi.Platform_WALLET:
		return WALLET
	case petozi.Platform_UNKNOWN:
		return UNKNOWN
	case petozi.Platform_ADYEN:
		return ADYEN
	case petozi.Platform_ADYEN_LUX:
		return ADYEN_LUX
	case petozi.Platform_WORLDPAY:
		return WORLDPAY
	case petozi.Platform_WORLDPAY_LUX:
		return WORLDPAY_LUX
	case petozi.Platform_NONE:
		fallthrough
	default:
		return DEFAULT
	}
}

func ParseSuperPlatform(platform string) SuperPlatform {
	platform = strings.ToUpper(platform)

	switch Platform(platform) {
	case AMAZON, ADYEN, ADYEN_LUX, PAYPAL, STRIPE, UNKNOWN, VANTIV, WALLET, WORLDPAY, WORLDPAY_LUX, XSOLLA:
		return SuperPlatform_WEB
	case ANDROID:
		return SuperPlatform_ANDROID
	case IOS:
		return SuperPlatform_IOS
	case PHANTO:
		return SuperPlatform_PHANTO
	case TRUEX:
		return SuperPlatform_TRUEX
	case UPSIGHT:
		return SuperPlatform_UPSIGHT
	default:
		log.Warn("Unhandled platform attempting to be parsed to super platform: ", platform)
		return SuperPlatform_NONE
	}
}
