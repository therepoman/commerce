package models

const (
	// Messages available to all moderators.
	MsgTypeLoginModerationAction = "chat_login_moderation"
	// Messages available to the user that is target of a moderation action
	MsgTypeTargetedLoginModerationAction = "chat_targeted_login_moderation"
)

// data structures for telling TMI about successful bits events
type TMIMessage struct {
	SenderUserID    string `json:"sender_user_id,omitempty"`
	TargetChannelID string `json:"target_channel_id,omitempty"`
	Body            string `json:"body,omitempty"`
	NumberBits      int    `json:"number_bits"`
	IsNoop          bool   `json:"is_noop"`
}

type UserNoticeMsgParam struct {
	Key   string `json:"key"`
	Value string `json:"value"`
}

type UserNoticeParams struct {
	SenderUserID      int64                `json:"sender_user_id"`
	TargetChannelID   int64                `json:"target_channel_id"`
	Body              string               `json:"body"`
	MsgId             string               `json:"msg_id"`
	MsgParams         []UserNoticeMsgParam `json:"msg_params"`
	DefaultSystemBody string               `json:"default_system_body"`
}

type TargetUserModerationActionData struct {
	Type             string   `json:"type"`
	ModerationAction string   `json:"moderation_action"`
	Args             []string `json:"args"`
	MsgID            string   `json:"msg_id"`
	TargetUserID     string   `json:"target_user_id"`
}

type TargetUserModerationAction struct {
	Data TargetUserModerationActionData `json:"data"`
}
