package models

type TranslationString struct {
	TranslationStringKey   string `json:"key"`
	TranslationStringValue string `json:"value"`
}

type GetStringsResponse struct {
	TranslationStrings []TranslationString `json:"strings"`
}
