package models_test

import (
	"testing"
	"time"

	"code.justin.tv/commerce/payday/models"
	"github.com/stretchr/testify/assert"
)

const (
	dummyChangeID         = "0865f192-e364-43f8-a76f-c1579eb72188"
	transactionID         = "06bd8e6e-ca7b-4bec-b217-ee43771fa9e4"
	timeOfEvent           = 1450233031389
	twitchUserID1         = "65380937"
	twitchUserID2         = "39141793"
	totalBitAmount        = -25
	processedBitAmount    = -20
	addBitAmount          = 25
	publicMessage         = "giveBits25 Kappa"
	channelStatus         = ""
	adminUserID           = "13405587"
	adminReason           = "Because I felt like it"
	extraContext          = ""
	emoteCode             = "cheer"
	giveBitsContext       = "{\"emoteUses\":{\"cheer\":25},\"partner_type\":\"traditional\"}"
	bitAccountType1       = string(models.CB_0)
	bitAccountType2       = string(models.CB_140_CBP_00140)
	bitAccountType3       = string(models.CB_700_CBP_00140)
	bitAmount1            = -15
	bitAmount2            = -10
	bitAmount3            = -10
	purchasePriceString   = "1.40"
	purchasePrice         = 1.40
	purchasePriceCurrency = "USD"
	marketplace           = "ATVPDKIKX0DER"
	noPurchasePrice       = 0
	noString              = ""
)

var (
	sampleGiveBits = models.GiveBitsToBroadcasterMessage{
		TransactionId:          transactionID,
		RequestingTwitchUserId: twitchUserID1,
		TargetTwitchUserId:     twitchUserID2,
		SourceStatus:           channelStatus,
		PublicMessage:          publicMessage,
		BitsSent:               totalBitAmount,
		AccountTypeToBitAmount: map[string]int{bitAccountType1: bitAmount1, bitAccountType2: bitAmount2},
		TimeOfEvent:            time.Unix(timeOfEvent, 0),
		ExtraContext:           giveBitsContext,
	}

	expectedGiveBitsTransaction = &models.TransactionRecord{
		TransactionID:          transactionID,
		TransactionType:        "GiveBitsToBroadcaster",
		TimeOfEvent:            timeOfEvent,
		RequestingTwitchUserID: twitchUserID1,
		TargetTwitchUserID:     twitchUserID2,
		TotalBitAmount:         totalBitAmount,
		ProcessedBitAmount:     totalBitAmount,
		SourceChannelStatus:    channelStatus,
		GiveBitsPublicMessage:  publicMessage,
		AdminUserID:            "",
		AdminReason:            "",
		ExtraContext:           giveBitsContext,
		PartnerType:            "traditional",
	}

	expectedGiveBitsBalanceUpdates = []models.BalanceUpdateRecord{
		{
			ChangeID:              dummyChangeID,
			TransactionID:         transactionID,
			BitAccountType:        bitAccountType1,
			BitAmount:             bitAmount1,
			TimeOfEvent:           timeOfEvent,
			PurchasePrice:         noPurchasePrice,
			PurchasePriceCurrency: noString,
			Marketplace:           noString,
		},
		{
			ChangeID:              dummyChangeID,
			TransactionID:         transactionID,
			BitAccountType:        bitAccountType2,
			BitAmount:             bitAmount2,
			TimeOfEvent:           timeOfEvent,
			PurchasePrice:         noPurchasePrice,
			PurchasePriceCurrency: noString,
			Marketplace:           noString,
		},
	}

	expectedGiveBitsEmoteUses = []models.EmoteUseRecord{
		{
			TransactionID: transactionID,
			EmoteCode:     emoteCode,
			BitAmount:     -totalBitAmount,
			TimeOfEvent:   timeOfEvent,
		},
	}

	sampleAddBits = models.AddBitsToCustomerAccountMessage{
		TransactionId:          transactionID,
		TargetTwitchUserId:     twitchUserID1,
		TotalBits:              addBitAmount,
		AccountTypeToBitAmount: map[string]int{bitAccountType1: addBitAmount},
		AdminID:                adminUserID,
		AdminReason:            adminReason,
		TimeOfEvent:            time.Unix(timeOfEvent, 0),
		ExtraContext:           extraContext,
		PurchasePrice:          purchasePriceString,
		PurchasePriceCurrency:  purchasePriceCurrency,
		PurchaseMarketplace:    marketplace,
	}

	expectedAddBitsTransaction = &models.TransactionRecord{
		TransactionID:          transactionID,
		TransactionType:        "AddBitsToCustomerAccount",
		TimeOfEvent:            timeOfEvent,
		RequestingTwitchUserID: twitchUserID1,
		TargetTwitchUserID:     twitchUserID1,
		TotalBitAmount:         addBitAmount,
		ProcessedBitAmount:     addBitAmount,
		SourceChannelStatus:    "",
		GiveBitsPublicMessage:  "",
		AdminUserID:            adminUserID,
		AdminReason:            adminReason,
		ExtraContext:           extraContext,
	}

	expectedAddBitsBalanceUpdates = []models.BalanceUpdateRecord{
		{
			ChangeID:              dummyChangeID,
			TransactionID:         transactionID,
			BitAccountType:        bitAccountType1,
			BitAmount:             addBitAmount,
			TimeOfEvent:           timeOfEvent,
			PurchasePrice:         purchasePrice,
			PurchasePriceCurrency: purchasePriceCurrency,
			Marketplace:           marketplace,
		},
	}

	sampleRemoveBits = models.RemoveBitsFromCustomerAccountMessage{
		TransactionId:          transactionID,
		TargetTwitchUserId:     twitchUserID1,
		TotalBits:              totalBitAmount,
		ProcessedBits:          processedBitAmount,
		AccountTypeToBitAmount: map[string]int{bitAccountType2: bitAmount2, bitAccountType3: bitAmount3},
		AdminID:                adminUserID,
		AdminReason:            adminReason,
		TimeOfEvent:            time.Unix(timeOfEvent, 0),
		ExtraContext:           extraContext,
	}

	expectedRemoveBitsTransaction = &models.TransactionRecord{
		TransactionID:          transactionID,
		TransactionType:        "RemoveBitsFromCustomerAccount",
		TimeOfEvent:            timeOfEvent,
		RequestingTwitchUserID: twitchUserID1,
		TargetTwitchUserID:     twitchUserID1,
		TotalBitAmount:         totalBitAmount,
		ProcessedBitAmount:     processedBitAmount,
		SourceChannelStatus:    "",
		GiveBitsPublicMessage:  "",
		AdminUserID:            adminUserID,
		AdminReason:            adminReason,
		ExtraContext:           extraContext,
	}

	expectedRemoveBitsBalanceUpdates = []models.BalanceUpdateRecord{
		{
			ChangeID:              dummyChangeID,
			TransactionID:         transactionID,
			BitAccountType:        bitAccountType2,
			BitAmount:             bitAmount2,
			TimeOfEvent:           timeOfEvent,
			PurchasePrice:         noPurchasePrice,
			PurchasePriceCurrency: noString,
			Marketplace:           noString,
		},
		{
			ChangeID:              dummyChangeID,
			TransactionID:         transactionID,
			BitAccountType:        bitAccountType3,
			BitAmount:             bitAmount3,
			TimeOfEvent:           timeOfEvent,
			PurchasePrice:         noPurchasePrice,
			PurchasePriceCurrency: noString,
			Marketplace:           noString,
		},
	}
)

func TestGiveBitsToTransactionRecord(t *testing.T) {
	giveBitsTransaction, err := sampleGiveBits.GetTransactionRecord()
	assert.NoError(t, err)
	assert.Equal(t, expectedGiveBitsTransaction, giveBitsTransaction)
}

func TestAddBitsToTransactionRecord(t *testing.T) {
	addBitsTransaction, err := sampleAddBits.GetTransactionRecord()
	assert.NoError(t, err)
	assert.Equal(t, expectedAddBitsTransaction, addBitsTransaction)
}

func TestRemoveBitsToTransactionRecord(t *testing.T) {
	removeBitsTransaction, err := sampleRemoveBits.GetTransactionRecord()
	assert.NoError(t, err)
	assert.Equal(t, expectedRemoveBitsTransaction, removeBitsTransaction)
}

func TestGiveBitsToEmoteUseRecord(t *testing.T) {
	giveBitsEmoteUses, err := sampleGiveBits.GetEmoteUseRecords()
	assert.NoError(t, err)
	assert.Equal(t, expectedGiveBitsEmoteUses, giveBitsEmoteUses)
}

func TestAddBitsToEmoteUseRecord(t *testing.T) {
	addBitsEmoteUses, err := sampleAddBits.GetEmoteUseRecords()
	assert.NoError(t, err)
	assert.Nil(t, addBitsEmoteUses)
}

func TestRemoveBitsToEmoteUseRecord(t *testing.T) {
	removeBitsEmoteUses, err := sampleRemoveBits.GetEmoteUseRecords()
	assert.NoError(t, err)
	assert.Nil(t, removeBitsEmoteUses)
}

func TestGiveBitsToBalanceUpdateRecord(t *testing.T) {
	giveBitsBalanceUpdates := sampleGiveBits.GetBalanceUpdateRecords()
	for _, v := range giveBitsBalanceUpdates {
		v.ChangeID = dummyChangeID
		assert.Contains(t, expectedGiveBitsBalanceUpdates, v)
	}
}

func TestAddBitsToBalanceUpdateRecord(t *testing.T) {
	addBitsBalanceUpdates := sampleAddBits.GetBalanceUpdateRecords()
	for _, v := range addBitsBalanceUpdates {
		v.ChangeID = dummyChangeID
		assert.Contains(t, expectedAddBitsBalanceUpdates, v)
	}
}

func TestRemoveBitsToBalanceUpdateRecord(t *testing.T) {
	removeBitsBalanceUpdates := sampleRemoveBits.GetBalanceUpdateRecords()
	for _, v := range removeBitsBalanceUpdates {
		v.ChangeID = dummyChangeID
		assert.Contains(t, expectedRemoveBitsBalanceUpdates, v)
	}
}
