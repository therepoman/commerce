package models

import "code.justin.tv/commerce/payday/partner"

type Bits int32
type Cursor string
type Limit int32

type Balance struct {
	TotalBits                     Bits
	CanPurchase                   map[string]bool
	TotalBitsWithBroadcaster      *int32
	MaximumBitsInventoryLimit     int32
	ProductInformation            map[string]BitsProductInformation
	MostRecentPurchaseWebPlatform Platform
}

type BitsProductInformation struct {
	Platforms           []Platform
	NumberOfBits        int32
	Promotional         bool
	PromoType           *string
	PromoId             *string
	CantPurcaseReason   *string
	PurchasableQuantity *int32
}

type FabsGiveBitsExtraContext struct {
	EmoteUses   map[string]Bits     `json:"emoteUses"`
	ClientID    string              `json:"client_id,omitempty"`
	PartnerType partner.PartnerType `json:"partner_type,omitempty"`
	IPAddress   string              `json:"ip_address,omitempty"`
	ZipCode     string              `json:"zip_code,omitempty"`
	NumChatters string              `json:"num_chatters,omitempty"`
	CountryCode string              `json:"country_code,omitempty"`
	City        string              `json:"city,omitempty"`
}
