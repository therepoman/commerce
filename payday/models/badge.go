package models

import (
	"encoding/json"
	"fmt"
	"net/http"
	"sort"

	"code.justin.tv/commerce/gogogadget/pointers"
	log "code.justin.tv/commerce/logrus"
	mako_client "code.justin.tv/commerce/mako/twirp"
	mako_dashboard "code.justin.tv/commerce/mako/twirp/dashboard"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/models/api"
	"github.com/golang/protobuf/ptypes"
)

func NewBadgeTier(tier *dynamo.BadgeTier) *api.BadgeTier {
	return &api.BadgeTier{
		ChannelId: tier.ChannelId,
		Threshold: tier.Threshold,
		Enabled:   tier.Enabled,
	}
}

func NewGetBadgesResponse(tiers []*api.BadgeTier, canUploadBitsBadgeTierEmotes *bool) *api.GetBadgesResponse {
	if tiers != nil {
		sort.Slice(tiers, func(i int, j int) bool {
			if tiers[i] == nil {
				return false
			} else if tiers[j] == nil {
				return true
			} else {
				return tiers[i].Threshold < tiers[j].Threshold
			}
		})
	}

	return &api.GetBadgesResponse{
		Tiers:                    tiers,
		CanUploadBadgeTierEmotes: canUploadBitsBadgeTierEmotes,
	}
}

func ConvertJsonToGetBadgesResponse(jsonResponse []byte) (*api.GetBadgesResponse, error) {
	var response api.GetBadgesResponse
	err := json.Unmarshal(jsonResponse, &response)
	if err != nil {
		msg := "Could not convert json to GetBadgesResponse"
		log.WithError(err).Error(msg)
		return nil, errors.Notef(err, msg)
	}
	return &response, nil
}

func ExtractSetBadgesRequest(httpRequest *http.Request) (*api.SetBadgesRequest, error) {
	var requestModel api.SetBadgesRequest
	decoder := json.NewDecoder(httpRequest.Body)
	err := decoder.Decode(&requestModel)
	return &requestModel, err
}

func ToDynamoTier(channelId string, requestTier *api.BadgeTierSetting) (*dynamo.BadgeTier, error) {
	if requestTier.Enabled == nil {
		return nil, errors.Newf("API BadgeTier must have an enabled value to be converted to a DynamoDB BadgeTier")
	}

	return &dynamo.BadgeTier{
		ChannelId: channelId,
		Threshold: requestTier.Threshold,
		Enabled:   *requestTier.Enabled,
	}, nil
}

func ToDynamoTiers(channelId string, request *api.SetBadgesRequest) []*dynamo.BadgeTier {
	dynamoTiers := make([]*dynamo.BadgeTier, 0)
	for _, tier := range request.Tiers {
		dynamoTier, err := ToDynamoTier(channelId, tier)
		if err == nil {
			dynamoTiers = append(dynamoTiers, dynamoTier)
		}
	}
	return dynamoTiers
}

func ToDynamoTierMap(tiers []*dynamo.BadgeTier) map[int64]dynamo.BadgeTier {
	tierMap := map[int64]dynamo.BadgeTier{}

	for _, tier := range tiers {
		if tier == nil {
			continue
		}

		tierMap[tier.Threshold] = *tier
	}

	return tierMap
}

func ToEmoticonDeletionSettings(emoticons []mako_dashboard.Emote) []api.EmoticonSetting {
	emoteSettings := make([]api.EmoticonSetting, 0, len(emoticons))

	for _, emoticon := range emoticons {
		emoteSetting := api.EmoticonSetting{
			Code:           emoticon.Code,
			EmoteID:        pointers.StringP(emoticon.Id),
			DeleteEmoticon: pointers.BoolP(true),
		}

		emoteSettings = append(emoteSettings, emoteSetting)
	}

	return emoteSettings
}

func ToPaydayEmoticon(makoEmoticon mako_dashboard.Emote) (api.Emoticon, error) {
	stateEnum := int32(makoEmoticon.State)

	_, ok := mako_client.EmoteState_name[stateEnum]
	if !ok || (makoEmoticon.State != mako_dashboard.EmoteState_active && makoEmoticon.State != mako_dashboard.EmoteState_pending) {
		msg := "mako emoticon had an invalid emote state"
		log.WithField("makoEmoticon", makoEmoticon).Error(msg)
		return api.Emoticon{}, errors.New(msg)
	}

	createdAt, err := ptypes.Timestamp(makoEmoticon.CreatedAt)
	if err != nil {
		// Log an error, but proceed with the returned value
		log.WithField("makoEmoticon", makoEmoticon).Error("unable to convert createdAt time to protobuf timestamp while converting Mako emoticon to Payday emoticon")
	}

	emoticon := api.Emoticon{
		Code:      makoEmoticon.Code,
		Suffix:    makoEmoticon.Suffix,
		EmoteID:   makoEmoticon.Id,
		GroupID:   makoEmoticon.GroupId,
		OwnerID:   makoEmoticon.OwnerId,
		State:     fmt.Sprint(makoEmoticon.State),
		StateEnum: stateEnum,
		CreatedAt: createdAt,
	}

	return emoticon, nil
}

func ToPaydayEmoticons(makoEmoticons []mako_dashboard.Emote) ([]api.Emoticon, error) {
	var emoticons []api.Emoticon

	//Fill emoticonData from the GetEmoticonsByGroups call
	for _, makoEmoticon := range makoEmoticons {
		emoticon, err := ToPaydayEmoticon(makoEmoticon)
		if err != nil {
			return nil, err
		}

		emoticons = append(emoticons, emoticon)
	}

	return emoticons, nil
}

func ToPaydayEmoticonUploadConfiguration(channelID string, groupID string, code string, codeSuffix string, uploadConfiguration1x mako_client.GetEmoticonUploadConfigurationResponse, uploadConfiguration2x mako_client.GetEmoticonUploadConfigurationResponse, uploadConfiguration4x mako_client.GetEmoticonUploadConfigurationResponse) api.EmoticonUploadConfiguration {
	return api.EmoticonUploadConfiguration{
		Code:       code,
		CodeSuffix: codeSuffix,
		GroupID:    groupID,
		OwnerID:    channelID,
		ImageUploadConfig1X: api.ImageUploadConfig{
			UploadID:         uploadConfiguration1x.UploadId,
			EmoteSignedS3URL: uploadConfiguration1x.UploadUrl,
			ImageID:          uploadConfiguration1x.ImageId,
			EmoteImageURL:    uploadConfiguration1x.ImageUrl,
		},
		ImageUploadConfig2X: api.ImageUploadConfig{
			UploadID:         uploadConfiguration2x.UploadId,
			EmoteSignedS3URL: uploadConfiguration2x.UploadUrl,
			ImageID:          uploadConfiguration2x.ImageId,
			EmoteImageURL:    uploadConfiguration2x.ImageUrl,
		},
		ImageUploadConfig4X: api.ImageUploadConfig{
			UploadID:         uploadConfiguration4x.UploadId,
			EmoteSignedS3URL: uploadConfiguration4x.UploadUrl,
			ImageID:          uploadConfiguration4x.ImageId,
			EmoteImageURL:    uploadConfiguration4x.ImageUrl,
		},
	}
}
