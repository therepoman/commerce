package models

import (
	"testing"

	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/models/api"
	"code.justin.tv/commerce/payday/utils/pointers"

	"github.com/stretchr/testify/assert"
)

// GetLeaderboardEnabled
func TestChannelGetLeaderboardEnabled_NilChannel(t *testing.T) {
	assert.Equal(t, false, GetLeaderboardEnabled(nil))
}

func TestChannelGetLeaderboardEnabled_NilSettings(t *testing.T) {
	channel := &dynamo.Channel{}
	assert.Equal(t, api.DefaultLeaderboardEnabled, GetLeaderboardEnabled(channel))
}

func TestChannelGetLeaderboardEnabled_NilEnabledSetting(t *testing.T) {
	channel := &dynamo.Channel{
		PinTopCheers: pointers.BoolP(true),
	}
	assert.Equal(t, true, GetLeaderboardEnabled(channel))
}

func TestChannelGetLeaderboardEnabled_NotNilSetting(t *testing.T) {
	channel := &dynamo.Channel{
		LeaderboardEnabled: pointers.BoolP(true),
	}
	assert.Equal(t, true, GetLeaderboardEnabled(channel))
}

// GetIsCheerLeaderboardEnabled
func TestChannelGetIsCheerLeaderboardEnabled_NilChannel(t *testing.T) {
	assert.Equal(t, false, GetIsCheerLeaderboardEnabled(nil))
}

func TestChannelGetIsCheerLeaderboardEnabled_NilEnabledSetting(t *testing.T) {
	channel := &dynamo.Channel{
		LeaderboardEnabled: pointers.BoolP(true),
	}
	assert.Equal(t, true, GetIsCheerLeaderboardEnabled(channel))
}

func TestChannelGetIsCheerLeaderboardEnabled_NotNilSetting(t *testing.T) {
	channel := &dynamo.Channel{
		IsCheerLeaderboardEnabled: pointers.BoolP(false),
	}
	assert.Equal(t, false, GetIsCheerLeaderboardEnabled(channel))
}

// GetIsSubGiftLeaderboardEnabled
func TestChannelGetIsSubGiftLeaderboardEnabled_NilChannel(t *testing.T) {
	assert.Equal(t, false, GetIsSubGiftLeaderboardEnabled(nil))
}

func TestChannelGetIsSubGiftLeaderboardEnabled_NilEnabledSetting(t *testing.T) {
	channel := &dynamo.Channel{
		LeaderboardEnabled: pointers.BoolP(true),
	}
	assert.Equal(t, true, GetIsSubGiftLeaderboardEnabled(channel))
}

func TestChannelGetIsSubGiftLeaderboardEnabled_NotNilSetting(t *testing.T) {
	channel := &dynamo.Channel{
		IsSubGiftLeaderboardEnabled: pointers.BoolP(false),
	}
	assert.Equal(t, false, GetIsSubGiftLeaderboardEnabled(channel))
}
