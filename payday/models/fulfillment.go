package models

const BitsFulfillmentSource = "bits-fulfillment"

type TwitchFulfillmentMessage struct {
	// IdempotenceKey a key used to uniquely identify this request. It's value should allow for duplicated requests to be detected
	IdempotenceKey string `json:"idempotenceKey"`
	// Source Who is send the fulfillment request
	Source string `json:"source"`
	// SKU The sku to be fulfilled
	SKU string `json:"sku"`
	// Vendor The vendor of the product
	Vendor string `json:"vendor"`
	// TUID The twitch User ID
	TUID string `json:"tuid"`
	//Quantity how many items to fulfill
	Quantity int64 `json:"quantity"`
	//Metadata Extra metadata about this request
	Metadata map[string]string `json:"metadata"`
}
