package models

type ModerationAction struct {
	Type           string   `json:"type"`
	Action         string   `json:"moderation_action"`
	Args           []string `json:"args"`
	ModeratorLogin string   `json:"created_by"`
	MessageId      string   `json:"msg_id"`
}

type ModerationPubSubMessage struct {
	Action ModerationAction `json:"data"`
}
