package models

type PhantoSNSMessage struct {
	UserID  string `json:"user_id"`
	SKU     string `json:"sku"`
	ClaimID string `json:"claim_id"`
}
