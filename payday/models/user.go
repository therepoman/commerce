package models

import (
	"encoding/json"
	"net/http"
	"time"

	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/models/api"
)

func NewGetBannedResponse(user *dynamo.User) *api.GetBannedResponse {
	return &api.GetBannedResponse{
		User:        string(user.Id),
		Banned:      user.Banned,
		BannedBy:    string(user.BannedBy),
		Annotation:  user.Annotation,
		LastUpdated: user.LastUpdated.UnixNano(),
	}
}

func ExtractSetBannedRequest(httpRquest *http.Request) (*api.SetBannedRequest, error) {
	var requestModel api.SetBannedRequest
	decoder := json.NewDecoder(httpRquest.Body)
	err := decoder.Decode(&requestModel)
	return &requestModel, err
}

func NewGetUserInfoResponse(eligible bool, userState string) *api.GetUserInfoResponse {
	return &api.GetUserInfoResponse{
		Eligible:  eligible,
		UserState: userState,
	}
}

func SetBannedRequestToDynamoUser(r *api.SetBannedRequest, userId string) *dynamo.User {
	return &dynamo.User{
		Id:         dynamo.UserId(userId),
		Banned:     r.Banned,
		BannedBy:   dynamo.BannedById(r.BannedBy),
		Annotation: r.Annotation,
	}
}

func GetBannedResponseToDynamoUser(r *api.GetBannedResponse) *dynamo.User {
	return &dynamo.User{
		Id:          dynamo.UserId(r.User),
		Banned:      r.Banned,
		BannedBy:    dynamo.BannedById(r.BannedBy),
		Annotation:  r.Annotation,
		LastUpdated: time.Unix(0, r.LastUpdated),
	}
}
