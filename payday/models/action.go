package models

import (
	"fmt"
	"strings"
	"time"

	string_utils "code.justin.tv/commerce/gogogadget/strings"
	time_utils "code.justin.tv/commerce/gogogadget/time"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/actions/constants"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/models/api"
)

const (
	CDN_TEMPLATE                      = "https://d3aqoihi2n8ty8.cloudfront.net/%s/%s/%s/%s/%s/%s.%s"
	CHANNEL_CUSTOM_CHEER_CDN_TEMPLATE = "https://d3aqoihi2n8ty8.cloudfront.net/%s/%s/%s/%s/%s/%s/%s.%s"
	GlobalActionsFolder               = "actions"
	PartnerActionsFolder              = "partner-actions"
	SponsoredActionsFolder            = "sponsored-actions"
)

var actionTypeToActionFolder = map[string]string{
	api.GlobalFirstPartyAction: GlobalActionsFolder,
	api.GlobalThirdPartyAction: GlobalActionsFolder,
	api.ChannelCustomAction:    PartnerActionsFolder,
	api.SponsoredAction:        SponsoredActionsFolder,
	api.DisplayOnlyAction:      GlobalActionsFolder, // a lie! (for now)
}

func ConvertTemplateToURLs(action api.Action, actionsFolder string, prefix string) api.Action {
	// TODO: Remove after april fools
	if strings.EqualFold(prefix, "cheer") && isAprilFools() {
		prefix = "cheer-redesign"
	}

	extension := "png"
	for _, state := range action.States {
		if state == "animated" {
			extension = "gif"
		}
		for _, scale := range action.Scales {
			for _, background := range action.Backgrounds {
				for i, tier := range action.Tiers {
					cdnURL := fmt.Sprintf(CDN_TEMPLATE, actionsFolder, strings.ToLower(prefix), background, state, tier.Id, scale, extension)

					var images api.TierImages
					if action.Tiers[i].Images != nil {
						images = *action.Tiers[i].Images
					} else {
						images = make(api.TierImages)
					}

					images.Put(background, state, scale, cdnURL)
					images[background][state][scale] = cdnURL
					action.Tiers[i].Images = &images
				}
			}
		}
	}

	return action
}

func MapActionImageTierURLs(action api.Action, prefix string) api.Action {
	var actionsFolder string

	// special override for display only and bonus as it's used for sponsored cheermotes.
	if action.Type == api.DisplayOnlyAction && action.Prefix == constants.BonusCheerPrefix {
		actionsFolder = SponsoredActionsFolder
	} else {
		var exists bool
		actionsFolder, exists = actionTypeToActionFolder[action.Type]
		if !exists {
			actionsFolder = GlobalActionsFolder
		}
	}

	return ConvertTemplateToURLs(action, actionsFolder, prefix)
}

func isAprilFools() bool {
	startTimeStr := config.Get().AprilFoolsStartTime
	endTimeStr := config.Get().AprilFoolsEndTime

	if string_utils.Blank(startTimeStr) || string_utils.Blank(endTimeStr) {
		log.Error("april fools start and end times not configured")
		return false
	}

	startTime, err := time.Parse(time.RFC3339, startTimeStr)
	if err != nil {
		log.WithError(err).Error("error parsing april fools start time")
		return false
	}

	endTime, err := time.Parse(time.RFC3339, endTimeStr)
	if err != nil {
		log.WithError(err).Error("error parsing april fools end time")
		return false
	}

	return time_utils.IsTimeInRange(time.Now(), startTime, endTime)
}
