package models

const (
	BitsEventTransactionType = "BITS_EVENT"
	WATEBTransactionType     = "WATEB"
	AddBitsTransactionType   = "AddBits"

	AddBitsToCustomerAccountTransactionType      = "AddBitsToCustomerAccount"
	AdminAddBitsTransactionType                  = "AdminAddBits"
	RemoveBitsFromCustomerAccountTransactionType = "RemoveBitsFromCustomerAccount"
	AdminRemoveBitsTransactionType               = "AdminRemoveBits"
	UseBitsOnExtensionTransactionType            = "UseBitsOnExtension"
	UseBitsOnPollTransactionType                 = "UseBitsOnPoll"
	GiveBitsToBroadcasterTransactionType         = "GiveBitsToBroadcaster"
	CreateHoldTransactionType                    = "CreateHold"
	FinalizeHoldTransactionType                  = "FinalizeHold"
	ReleaseHoldTransactionType                   = "ReleaseHold"

	GiveBitsToBroadcasterChallengeTransactionType = "GiveBitsToBroadcasterChallenge"
	UseBitsOnExtensionChallengeTransactionType    = "UseBitsInExtensionChallenge"
)

type TransactionSearchType string

const (
	TransactionSearchTypeOnsite    = TransactionSearchType("ONSITE")
	TransactionSearchTypeExtension = TransactionSearchType("EXTENSION")
)
