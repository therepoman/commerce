package models

import (
	"testing"

	petozi "code.justin.tv/commerce/petozi/rpc"
	"github.com/stretchr/testify/assert"
)

func TestValidateBitsType_Empty(t *testing.T) {
	assert.Error(t, ValidateBitsType(""))
}

func TestValidateBitsType_Invalid(t *testing.T) {
	assert.Error(t, ValidateBitsType("Not a valid BitsType"))
}

func TestValidateBitsType_CB0(t *testing.T) {
	assert.NoError(t, ValidateBitsType("CB_0"))
}

func TestValidateBitsType_CB140(t *testing.T) {
	assert.NoError(t, ValidateBitsType("CB_140_CBP_00140"))
}

func TestValidateBitsType_CB700(t *testing.T) {
	assert.NoError(t, ValidateBitsType("CB_700_CBP_00140"))
}

func TestValidateBitsType_CB1995(t *testing.T) {
	assert.NoError(t, ValidateBitsType("CB_1995_CBP_00133"))
}

func TestValidateBitsType_CB6440(t *testing.T) {
	assert.NoError(t, ValidateBitsType("CB_6440_CBP_00129"))
}

func TestValidateBitsType_CB12600(t *testing.T) {
	assert.NoError(t, ValidateBitsType("CB_12600_CBP_00126"))
}

func TestValidateBitsType_CB30800(t *testing.T) {
	assert.NoError(t, ValidateBitsType("CB_30800_CBP_00120"))
}

func TestValidateBitsType_7FOR7(t *testing.T) {
	assert.NoError(t, ValidateBitsType("CB_700_CBP_00100"))
}

func TestValidateBitsType_BitsForAds(t *testing.T) {
	assert.NoError(t, ValidateBitsType("BitsForAds"))
}

func TestValidateBitsType_BitsForAdsX(t *testing.T) {
	assert.NoError(t, ValidateBitsType("BitsForAdsX"))
}

func TestValidateBitsType_BitsForCrate(t *testing.T) {
	assert.NoError(t, ValidateBitsType("BitsForCrate"))
}

func TestParsePlatformAmazon(t *testing.T) {
	assert.Equal(t, AMAZON, ParsePlatform("amazon", nil, nil, nil))
}

func TestParsePlatformIos(t *testing.T) {
	assert.Equal(t, IOS, ParsePlatform("ios", nil, nil, nil))
}

func TestParsePlatformAndroid(t *testing.T) {
	assert.Equal(t, ANDROID, ParsePlatform("android", nil, nil, nil))
}

func TestParsePlatformAmazonWrongCasing(t *testing.T) {
	assert.Equal(t, AMAZON, ParsePlatform("Amazon", nil, nil, nil))
}

func TestParsePlatformAmazonJunk(t *testing.T) {
	assert.Equal(t, "default", string(ParsePlatform("TACO", nil, nil, nil)))
}

func TestIsPlatformMobileExplicitIosReference(t *testing.T) {
	assert.True(t, IsPlatformMobile(IOS))
}

func TestIsPlatformMobileExplicitAndroidReference(t *testing.T) {
	assert.True(t, IsPlatformMobile(ANDROID))
}

func TestIsPlatformMobileExplicitAmazonReference(t *testing.T) {
	assert.False(t, IsPlatformMobile(AMAZON))
}

func TestIsPlatformMobileParseIos(t *testing.T) {
	assert.True(t, IsPlatformMobile(ParsePlatform("ios", nil, nil, nil)))
}

func TestParsePetoziPlatform(t *testing.T) {
	assert.Equal(t, ParsePetoziPlatform("amazon"), petozi.Platform_AMAZON)
	assert.Equal(t, ParsePetoziPlatform("AMAZON"), petozi.Platform_AMAZON)
	assert.Equal(t, ParsePetoziPlatform("Amazon"), petozi.Platform_AMAZON)
	assert.Equal(t, ParsePetoziPlatform("Adyen"), petozi.Platform_ADYEN)
	assert.Equal(t, ParsePetoziPlatform("adyen_lux"), petozi.Platform_ADYEN_LUX)
	assert.Equal(t, ParsePetoziPlatform("worldpay"), petozi.Platform_WORLDPAY)
	assert.Equal(t, ParsePetoziPlatform("WORLDPAY_LUX"), petozi.Platform_WORLDPAY_LUX)
	assert.Equal(t, ParsePetoziPlatform("PAYPAL"), petozi.Platform_PAYPAL)
	assert.Equal(t, ParsePetoziPlatform("XSOLLA"), petozi.Platform_XSOLLA)
}

func TestParseSuperPlatform(t *testing.T) {
	assert.Equal(t, ParseSuperPlatform("amazon"), SuperPlatform_WEB)
	assert.Equal(t, ParseSuperPlatform("AMAZON"), SuperPlatform_WEB)
	assert.Equal(t, ParseSuperPlatform("adyen"), SuperPlatform_WEB)
	assert.Equal(t, ParseSuperPlatform("ADYEN_LUX"), SuperPlatform_WEB)
	assert.Equal(t, ParseSuperPlatform("Worldpay"), SuperPlatform_WEB)
	assert.Equal(t, ParseSuperPlatform("worldpay_lux"), SuperPlatform_WEB)
	assert.Equal(t, ParseSuperPlatform("XSOLLA"), SuperPlatform_WEB)
	assert.Equal(t, ParseSuperPlatform("PAYPAL"), SuperPlatform_WEB)
	assert.Equal(t, ParseSuperPlatform("ANDROID"), SuperPlatform_ANDROID)
}
