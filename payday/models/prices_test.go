package models

import (
	"testing"

	flotwirp "code.justin.tv/revenue/flo/rpc"

	"github.com/stretchr/testify/assert"
)

// FormatPriceFromFlo
func TestFormatPriceFromFlo_NilPricing(t *testing.T) {
	assert.Equal(t, "", FormatPriceFromFlo(nil))
}

func TestFormatPriceFromFlo_USDPricing(t *testing.T) {
	p := &flotwirp.Pricing{
		Currency: "USD",
		Price:    499,
		Exponent: 2,
	}
	assert.Equal(t, "4.99", FormatPriceFromFlo(p))
}

func TestFormatPriceFromFlo_USDPricingTotal(t *testing.T) {
	p := &flotwirp.Pricing{
		Currency:     "USD",
		TaxInclusive: true,
		Total:        532,
		Exponent:     2,
	}
	assert.Equal(t, "5.32", FormatPriceFromFlo(p))
}

func TestFormatPriceFromFlo_JPYPricing(t *testing.T) {
	p := &flotwirp.Pricing{
		Currency: "JPY",
		Price:    600,
		Exponent: 0,
	}
	assert.Equal(t, "600", FormatPriceFromFlo(p))
}

func TestFormatPriceFromFlo_ISKPricing(t *testing.T) {
	p := &flotwirp.Pricing{
		Currency: "ISK",
		Price:    650,
		Exponent: 0,
	}
	assert.Equal(t, "650", FormatPriceFromFlo(p))
}

// old text/currency had IDR exponent = 0 but it should be 2 after using exponent from Flo
func TestFormatPriceFromFlo_IDRPricing(t *testing.T) {
	p := &flotwirp.Pricing{
		Currency: "IDR",
		Price:    499,
		Exponent: 2,
	}
	assert.Equal(t, "4.99", FormatPriceFromFlo(p))
}

func TestFormatPriceFromFlo_KWDPricing(t *testing.T) {
	p := &flotwirp.Pricing{
		Currency: "KWD",
		Price:    3990,
		Exponent: 3,
	}
	assert.Equal(t, "3.990", FormatPriceFromFlo(p))
}

func TestFormatPriceFromFlo_UYWPricing(t *testing.T) {
	p := &flotwirp.Pricing{
		Currency: "UYW",
		Price:    39900,
		Exponent: 4,
	}
	assert.Equal(t, "3.9900", FormatPriceFromFlo(p))
}
