package models

import (
	"testing"

	"code.justin.tv/commerce/payday/models/api"
	"github.com/stretchr/testify/assert"
)

func TestConvertTemplateToURLs(t *testing.T) {
	templateAction := api.Action{
		Prefix: "cheer",
		Scales: []string{"1", "1.5", "2", "3", "4"},
		Tiers: []api.ActionTier{
			{
				BitsMinimum: 1,
				Id:          "1",
				Color:       "#979797",
			},
			{
				BitsMinimum: 100,
				Id:          "100",
				Color:       "#9c3ee8",
			},
			{
				BitsMinimum: 1000,
				Id:          "1000",
				Color:       "#1db2a5",
			},
			{
				BitsMinimum: 5000,
				Id:          "5000",
				Color:       "#0099fe",
			},
			{
				BitsMinimum: 10000,
				Id:          "10000",
				Color:       "#f43021",
			},
		},
		Backgrounds: []string{"light", "dark"},
		States:      []string{"static", "animated"},
	}

	action := ConvertTemplateToURLs(templateAction, "actions", "cheer")
	images := *action.Tiers[0].Images
	assert.Equal(t, api.TierImages{
		"light": map[string]map[string]string{
			"static": {
				"1":   "https://d3aqoihi2n8ty8.cloudfront.net/actions/cheer/light/static/1/1.png",
				"1.5": "https://d3aqoihi2n8ty8.cloudfront.net/actions/cheer/light/static/1/1.5.png",
				"2":   "https://d3aqoihi2n8ty8.cloudfront.net/actions/cheer/light/static/1/2.png",
				"3":   "https://d3aqoihi2n8ty8.cloudfront.net/actions/cheer/light/static/1/3.png",
				"4":   "https://d3aqoihi2n8ty8.cloudfront.net/actions/cheer/light/static/1/4.png",
			},
			"animated": {
				"1.5": "https://d3aqoihi2n8ty8.cloudfront.net/actions/cheer/light/animated/1/1.5.gif",
				"2":   "https://d3aqoihi2n8ty8.cloudfront.net/actions/cheer/light/animated/1/2.gif",
				"3":   "https://d3aqoihi2n8ty8.cloudfront.net/actions/cheer/light/animated/1/3.gif",
				"4":   "https://d3aqoihi2n8ty8.cloudfront.net/actions/cheer/light/animated/1/4.gif",
				"1":   "https://d3aqoihi2n8ty8.cloudfront.net/actions/cheer/light/animated/1/1.gif",
			},
		},
		"dark": map[string]map[string]string{
			"static": {
				"3":   "https://d3aqoihi2n8ty8.cloudfront.net/actions/cheer/dark/static/1/3.png",
				"4":   "https://d3aqoihi2n8ty8.cloudfront.net/actions/cheer/dark/static/1/4.png",
				"1":   "https://d3aqoihi2n8ty8.cloudfront.net/actions/cheer/dark/static/1/1.png",
				"1.5": "https://d3aqoihi2n8ty8.cloudfront.net/actions/cheer/dark/static/1/1.5.png",
				"2":   "https://d3aqoihi2n8ty8.cloudfront.net/actions/cheer/dark/static/1/2.png",
			},
			"animated": {
				"1":   "https://d3aqoihi2n8ty8.cloudfront.net/actions/cheer/dark/animated/1/1.gif",
				"1.5": "https://d3aqoihi2n8ty8.cloudfront.net/actions/cheer/dark/animated/1/1.5.gif",
				"2":   "https://d3aqoihi2n8ty8.cloudfront.net/actions/cheer/dark/animated/1/2.gif",
				"3":   "https://d3aqoihi2n8ty8.cloudfront.net/actions/cheer/dark/animated/1/3.gif",
				"4":   "https://d3aqoihi2n8ty8.cloudfront.net/actions/cheer/dark/animated/1/4.gif",
			},
		},
	}, images)
}
