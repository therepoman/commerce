package models

import (
	"code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/campaigns"
	"code.justin.tv/commerce/payday/models/api"
)

type BitEmoteTotal struct {
	CheeredTotal int64
	MatchedTotal int64
	Campaign     *campaigns.SponsoredCheermoteCampaign
}

// data structures for modeling bits event APIs
//TODO: consider getting rid of this
func CreateEventRequestToBitsUsedError(req api.CreateEventRequest) (*BitsUsedError, error) {
	bue := &BitsUsedError{
		UserID:            req.UserId,
		ChannelID:         req.ChannelId,
		QuantityAttempted: req.Amount,
	}
	return bue, nil
}
