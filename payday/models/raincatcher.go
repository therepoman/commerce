package models

import (
	"encoding/json"
	"fmt"
	"strconv"
	"strings"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/extension/transaction"
	"code.justin.tv/commerce/payday/partner"
	"code.justin.tv/commerce/payday/sqs/handlers"
	"github.com/pborman/uuid"
)

const (
	TransactionIdLength       = 300
	TransactionTypeLength     = 50
	TwitchUserIdLength        = 50
	SourceChannelStatusLength = 50
	OrderIdLength             = 300
	ReceiptIdLength           = 300
	EventReasonCodeLength     = 50
	EmoteCodeLength           = 50
	PartnerTypeLength         = 50
	EmptyString               = ""
	EmptyFloatString          = "0"
	SponsoredCheermoteBitType = "SponsoredBits"
)

type TransactionRecord struct {
	TransactionID          string                     `json:"transactionid"`
	TransactionType        string                     `json:"transactiontype"`
	TimeOfEvent            int64                      `json:"timeofevent"`
	RequestingTwitchUserID string                     `json:"requestingtwitchuserid"`
	TargetTwitchUserID     string                     `json:"targettwitchuserid"`
	TotalBitAmount         int                        `json:"totalbitamount"`
	ProcessedBitAmount     int                        `json:"processedbitamount"`
	SourceChannelStatus    string                     `json:"sourcechannelstatus"`
	GiveBitsPublicMessage  string                     `json:"givebitspublicmessage"`
	AdminUserID            string                     `json:"adminuserid"`
	AdminReason            string                     `json:"adminreason"`
	RelatedTransactionID   string                     `json:"relatedtransactionid"`
	DoxOrderID             string                     `json:"doxorderid"`
	AdgReceiptID           string                     `json:"adgreceiptid"`
	EventReasonCode        string                     `json:"eventreasoncode"`
	PartnerType            string                     `json:"partnertype"`
	ExtraContext           string                     `json:"extracontext"`
	ExtensionClientID      string                     `json:"extensionclientid"`
	ExtensionProduct       transaction.FaladorProduct `json:"extensionProduct"`
	SKU                    string                     `json:"sku"`
	SponsoredAmount        int                        `json:"sponsoredamount"`
	IsAnonymous            bool                       `json:"isanonymous"`
	TrackingID             string                     `json:"transationid"`
	PollID                 string                     `json:"pollid"`
	PollChoiceID           string                     `json:"pollchoiceid"`
}

type AddBitsToCustomerAccountExtraContext struct {
	DoxOrderID   string `json:"doxOrderID"`
	AdgReceiptID string `json:"adgReceiptID"`
	TrackingID   string `json:"trackingID"`
}

type GiveBitsExtraContext struct {
	PartnerType string         `json:"partner_type"`
	EmoteUses   map[string]int `json:"emoteUses"`
}

type BitsExtensionsExtraContext struct {
	IPAddress   string `json:"ip_address,omitempty"`
	ZipCode     string `json:"zip_code,omitempty"`
	CountryCode string `json:"country_code,omitempty"`
	City        string `json:"city,omitempty"`
}

func (tr TransactionRecord) ValidateAgainstRedshiftSchema() error {
	if len(tr.TransactionID) > TransactionIdLength {
		msg := fmt.Sprintf("Transaction Id was too long for redshift: %s", tr.TransactionID)
		log.Error(msg)
		return errors.New(msg)
	}
	if len(tr.TransactionType) > TransactionTypeLength {
		msg := fmt.Sprintf("Transaction type was too long for redshift: %s", tr.TransactionType)
		log.Error(msg)
		return errors.New(msg)
	}
	if len(tr.RequestingTwitchUserID) > TwitchUserIdLength {
		msg := fmt.Sprintf("Requesting User Id was too long for redshift: %s", tr.RequestingTwitchUserID)
		log.Error(msg)
		return errors.New(msg)
	}
	if len(tr.TargetTwitchUserID) > TwitchUserIdLength {
		msg := fmt.Sprintf("Target User Id was too long for redshift: %s", tr.RequestingTwitchUserID)
		log.Error(msg)
		return errors.New(msg)
	}
	if len(tr.SourceChannelStatus) > SourceChannelStatusLength {
		msg := fmt.Sprintf("Source channel status was too long for redshift: %s", tr.SourceChannelStatus)
		log.Error(msg)
		return errors.New(msg)
	}
	if len(tr.AdminUserID) > TwitchUserIdLength {
		msg := fmt.Sprintf("Admin User Id was too long for redshift: %s", tr.AdminUserID)
		log.Error(msg)
		return errors.New(msg)
	}
	if len(tr.RelatedTransactionID) > TransactionIdLength {
		msg := fmt.Sprintf("Related Transaction Id was too long for redshift: %s", tr.RelatedTransactionID)
		log.Error(msg)
		return errors.New(msg)
	}
	if len(tr.DoxOrderID) > OrderIdLength {
		msg := fmt.Sprintf("DOX Order Id was too long for redshift: %s", tr.DoxOrderID)
		log.Error(msg)
		return errors.New(msg)
	}
	if len(tr.AdgReceiptID) > ReceiptIdLength {
		msg := fmt.Sprintf("ADG Receipt Id was too long for redshift: %s", tr.AdgReceiptID)
		log.Error(msg)
		return errors.New(msg)
	}
	if len(tr.EventReasonCode) > EventReasonCodeLength {
		msg := fmt.Sprintf("Event Reason Id was too long for redshift: %s", tr.EventReasonCode)
		log.Error(msg)
		return errors.New(msg)
	}
	if len(tr.PartnerType) > PartnerTypeLength {
		msg := fmt.Sprintf("Partner Type was too long for redshift: %s", tr.PartnerType)
		log.Error(msg)
		return errors.New(msg)
	}

	return nil
}

type EmoteUseRecord struct {
	TransactionID string `json:"transactionid"`
	EmoteCode     string `json:"emotecode"`
	BitAmount     int    `json:"bitamount"`
	TimeOfEvent   int64  `json:"timeofevent"`
}

func (eur EmoteUseRecord) ValidateAgainstRedshiftSchema() error {
	if len(eur.TransactionID) > TransactionIdLength {
		msg := fmt.Sprintf("Transaction Id was too long for redshift: %s", eur.TransactionID)
		log.Error(msg)
		return errors.New(msg)
	}

	if len(eur.EmoteCode) > EmoteCodeLength {
		msg := fmt.Sprintf("Emote code was too long for redshift: %s", eur.EmoteCode)
		log.Error(msg)
		return errors.New(msg)
	}
	return nil
}

type BalanceUpdateRecord struct {
	ChangeID              string  `json:"changeid"`
	TransactionID         string  `json:"transactionid"`
	BitAccountType        string  `json:"bitaccounttype"`
	BitAmount             int     `json:"bitamount"`
	TimeOfEvent           int64   `json:"timeofevent"`
	PurchasePrice         float64 `json:"purchaseprice"`
	PurchasePriceCurrency string  `json:"purchasepricecurrency"`
	Marketplace           string  `json:"marketplace"`
}

func (bur BalanceUpdateRecord) ValidateAgainstRedshiftSchema() error {
	if len(bur.TransactionID) > TransactionIdLength {
		msg := fmt.Sprintf("Transaction Id was too long for redshift: %s", bur.TransactionID)
		log.Error(msg)
		return errors.New(msg)
	}
	if len(bur.ChangeID) > TransactionIdLength {
		msg := fmt.Sprintf("Change Id was too long for redshift: %s", bur.ChangeID)
		log.Error(msg)
		return errors.New(msg)
	}

	return nil
}

type RaincatcherMessage interface {
	GetTransactionRecord() (*TransactionRecord, error)
	GetEmoteUseRecords() ([]EmoteUseRecord, error)
	GetBalanceUpdateRecords() []BalanceUpdateRecord
	GetTransactionID() string
	SQSMessage
}

func (gb *GiveBitsToBroadcasterMessage) GetTransactionID() string {
	return gb.TransactionId
}

func (ab *AddBitsToCustomerAccountMessage) GetTransactionID() string {
	return ab.TransactionId
}

func (rb *RemoveBitsFromCustomerAccountMessage) GetTransactionID() string {
	return rb.TransactionId
}

func (uboe *UseBitsOnExtensionMessage) GetTransactionID() string {
	return uboe.TransactionId
}

func (ubop *UseBitsOnPollMessage) GetTransactionID() string {
	return ubop.TransactionId
}

func (gb *GiveBitsToBroadcasterMessage) GetTransactionRecord() (record *TransactionRecord, err error) {
	timeOfEvent := gb.TimeOfEvent.Unix()
	giveBitsContext, err := parseGiveBitsExtraContext(gb.ExtraContext)
	if err != nil {
		log.WithError(err).WithField("transactionId", gb.TransactionId).Error("Failed to parse extra context for transaction to get PartnerType")
		return nil, err
	}

	var partnerType string
	if giveBitsContext != nil {
		partnerType = giveBitsContext.PartnerType
	}

	// Fix for sometimes accidental values during testing.
	if strings.EqualFold(partnerType, "partner") {
		partnerType = string(partner.TraditionalPartnerType)
	}

	if partner.PartnerType(partnerType) != partner.TraditionalPartnerType && partner.PartnerType(partnerType) != partner.AffiliatePartnerType {
		msg := fmt.Sprintf("Transaction in channel (%s) for invalid partner type (%s)", gb.TargetTwitchUserId, partnerType)
		log.Error(msg)
		return nil, errors.New(msg)
	}

	sponsoredAmount := handlers.BitsUsedWithSponsor(0, gb.SponsoredAmounts)

	// BitsSent is a negative number because it's a subtraction from the sender's perspective, because of this we need
	// to subtract the sponsored amount to make the total match up.
	bitsUsedWithSponsoredAmount := gb.BitsSent - sponsoredAmount

	record = &TransactionRecord{
		TransactionID:          gb.TransactionId,
		TransactionType:        gb.MessageType().Name(),
		TimeOfEvent:            timeOfEvent,
		RequestingTwitchUserID: gb.RequestingTwitchUserId,
		TargetTwitchUserID:     gb.TargetTwitchUserId,
		TotalBitAmount:         bitsUsedWithSponsoredAmount,
		ProcessedBitAmount:     bitsUsedWithSponsoredAmount,
		SourceChannelStatus:    gb.SourceStatus,
		GiveBitsPublicMessage:  gb.PublicMessage,
		AdminUserID:            "",
		AdminReason:            "",
		RelatedTransactionID:   "",
		DoxOrderID:             "",
		AdgReceiptID:           "",
		EventReasonCode:        gb.EventReasonCode,
		PartnerType:            partnerType,
		ExtraContext:           gb.ExtraContext,
		SponsoredAmount:        sponsoredAmount,
		IsAnonymous:            gb.IsAnonymous,
	}

	return record, nil
}

func (ab *AddBitsToCustomerAccountMessage) GetTransactionRecord() (record *TransactionRecord, err error) {
	timeOfEvent := ab.TimeOfEvent.Unix()

	addBitsTocustomerAccountExtraContext := &AddBitsToCustomerAccountExtraContext{}
	if ab.ExtraContext != "" {
		addBitsTocustomerAccountExtraContext, err = parseAddBitsToCustomerAccountExtraContext(ab.ExtraContext)
		if err != nil {
			log.WithError(err).WithField("transactionId", ab.TransactionId).Error("Failed to parse extra context for transaction")
			return nil, err
		}
	}

	record = &TransactionRecord{
		TransactionID:          ab.TransactionId,
		TransactionType:        ab.MessageType().Name(),
		TimeOfEvent:            timeOfEvent,
		RequestingTwitchUserID: ab.TargetTwitchUserId,
		TargetTwitchUserID:     ab.TargetTwitchUserId,
		TotalBitAmount:         ab.TotalBits,
		ProcessedBitAmount:     ab.TotalBits,
		SourceChannelStatus:    "",
		GiveBitsPublicMessage:  "",
		AdminUserID:            ab.AdminID,
		AdminReason:            ab.AdminReason,
		RelatedTransactionID:   "",
		DoxOrderID:             addBitsTocustomerAccountExtraContext.DoxOrderID,
		AdgReceiptID:           addBitsTocustomerAccountExtraContext.AdgReceiptID,
		EventReasonCode:        ab.EventReasonCode,
		PartnerType:            "",
		ExtraContext:           ab.ExtraContext,
		TrackingID:             addBitsTocustomerAccountExtraContext.TrackingID,
	}

	return record, nil
}

func (rb *RemoveBitsFromCustomerAccountMessage) GetTransactionRecord() (record *TransactionRecord, err error) {
	timeOfEvent := rb.TimeOfEvent.Unix()

	record = &TransactionRecord{
		TransactionID:          rb.TransactionId,
		TransactionType:        rb.MessageType().Name(),
		TimeOfEvent:            timeOfEvent,
		RequestingTwitchUserID: rb.TargetTwitchUserId,
		TargetTwitchUserID:     rb.TargetTwitchUserId,
		TotalBitAmount:         rb.TotalBits,
		ProcessedBitAmount:     rb.ProcessedBits,
		SourceChannelStatus:    "",
		GiveBitsPublicMessage:  "",
		AdminUserID:            rb.AdminID,
		AdminReason:            rb.AdminReason,
		RelatedTransactionID:   "",
		DoxOrderID:             "",
		AdgReceiptID:           "",
		EventReasonCode:        rb.EventReasonCode,
		PartnerType:            "",
		ExtraContext:           rb.ExtraContext,
	}

	return record, nil
}

func (uboe *UseBitsOnExtensionMessage) GetTransactionRecord() (record *TransactionRecord, err error) {
	timeOfEvent := uboe.TimeOfEvent.Unix()

	record = &TransactionRecord{
		TransactionID:          uboe.TransactionId,
		TransactionType:        uboe.MessageType().Name(),
		TimeOfEvent:            timeOfEvent,
		RequestingTwitchUserID: uboe.RequestingTwitchUserId,
		TargetTwitchUserID:     uboe.TargetTwitchUserId,
		TotalBitAmount:         uboe.BitsProccessed,
		ProcessedBitAmount:     uboe.BitsProccessed,
		SourceChannelStatus:    "",
		GiveBitsPublicMessage:  "",
		AdminUserID:            "",
		AdminReason:            "",
		RelatedTransactionID:   "",
		DoxOrderID:             "",
		AdgReceiptID:           "",
		EventReasonCode:        "",
		PartnerType:            "",
		ExtraContext:           uboe.ExtraContext,
		ExtensionClientID:      uboe.ExtensionID,
		SKU:                    uboe.SKU,
	}

	return record, nil
}

func (ubop *UseBitsOnPollMessage) GetTransactionRecord() (record *TransactionRecord, err error) {
	timeOfEvent := ubop.TimeOfEvent.Unix()

	record = &TransactionRecord{
		TransactionID:          ubop.TransactionId,
		TransactionType:        ubop.MessageType().Name(),
		TimeOfEvent:            timeOfEvent,
		RequestingTwitchUserID: ubop.RequestingTwitchUserId,
		TargetTwitchUserID:     ubop.TargetTwitchUserId,
		TotalBitAmount:         ubop.BitsProccessed,
		ProcessedBitAmount:     ubop.BitsProccessed,
		SourceChannelStatus:    "",
		GiveBitsPublicMessage:  "",
		AdminUserID:            "",
		AdminReason:            "",
		RelatedTransactionID:   "",
		DoxOrderID:             "",
		AdgReceiptID:           "",
		EventReasonCode:        "",
		PartnerType:            "",
		ExtraContext:           ubop.ExtraContext,
		PollID:                 ubop.PollId,
		PollChoiceID:           ubop.PollChoiceId,
	}

	return record, nil
}

func parseAddBitsToCustomerAccountExtraContext(context string) (result *AddBitsToCustomerAccountExtraContext, err error) {
	err = json.Unmarshal([]byte(context), &result)
	return result, err
}

func parseGiveBitsExtraContext(context string) (result *GiveBitsExtraContext, err error) {
	err = json.Unmarshal([]byte(context), &result)
	return result, err
}

func (gb *GiveBitsToBroadcasterMessage) GetEmoteUseRecords() ([]EmoteUseRecord, error) {
	giveContext, err := parseGiveBitsExtraContext(gb.ExtraContext)
	if err != nil {
		log.WithError(err).WithField("transactionId", gb.TransactionId).Errorf("Failed to parse extra context for transaction to get EmoteUses")
		return nil, err
	}

	return createEmoteUseRecords(gb.TransactionId, giveContext.EmoteUses, gb.TimeOfEvent), nil
}

func (ab *AddBitsToCustomerAccountMessage) GetEmoteUseRecords() ([]EmoteUseRecord, error) {
	return nil, nil
}

func (rb *RemoveBitsFromCustomerAccountMessage) GetEmoteUseRecords() ([]EmoteUseRecord, error) {
	return nil, nil
}

func (uboe *UseBitsOnExtensionMessage) GetEmoteUseRecords() ([]EmoteUseRecord, error) {
	return nil, nil
}

func (ubop *UseBitsOnPollMessage) GetEmoteUseRecords() ([]EmoteUseRecord, error) {
	return nil, nil
}

func createEmoteUseRecords(transactionId string, emoteUses map[string]int, timeOfEvent time.Time) []EmoteUseRecord {
	records := make([]EmoteUseRecord, 0, len(emoteUses))

	for prefix, bitAmount := range emoteUses {
		record := EmoteUseRecord{
			TransactionID: transactionId,
			EmoteCode:     prefix,
			BitAmount:     bitAmount,
			TimeOfEvent:   timeOfEvent.Unix(),
		}

		records = append(records, record)
	}

	return records
}

func (gb *GiveBitsToBroadcasterMessage) GetBalanceUpdateRecords() []BalanceUpdateRecord {
	return createBalanceUpdateRecords(gb.TransactionId, gb.AccountTypeToBitAmount, gb.TimeOfEvent, EmptyFloatString, EmptyString, EmptyString, gb.SponsoredAmounts)
}

func (ab *AddBitsToCustomerAccountMessage) GetBalanceUpdateRecords() []BalanceUpdateRecord {
	return createBalanceUpdateRecords(ab.TransactionId, ab.AccountTypeToBitAmount, ab.TimeOfEvent,
		ab.PurchasePrice, ab.PurchasePriceCurrency, ab.PurchaseMarketplace, nil)
}

func (rb *RemoveBitsFromCustomerAccountMessage) GetBalanceUpdateRecords() []BalanceUpdateRecord {
	return createBalanceUpdateRecords(rb.TransactionId, rb.AccountTypeToBitAmount, rb.TimeOfEvent, EmptyFloatString, EmptyString, EmptyString, nil)
}

func (uboe *UseBitsOnExtensionMessage) GetBalanceUpdateRecords() []BalanceUpdateRecord {
	return createBalanceUpdateRecords(uboe.TransactionId, uboe.AccountTypeToBitAmount, uboe.TimeOfEvent, EmptyFloatString, EmptyString, EmptyString, nil)
}

func (ubop *UseBitsOnPollMessage) GetBalanceUpdateRecords() []BalanceUpdateRecord {
	return createBalanceUpdateRecords(ubop.TransactionId, ubop.AccountTypeToBitAmount, ubop.TimeOfEvent, EmptyFloatString, EmptyString, EmptyString, nil)
}

func createBalanceUpdateRecords(transactionID string, bitTypeMap map[string]int, timeOfEvent time.Time,
	purchasePrice string, purchasePriceCurrency string, marketplace string, sponsoredAmounts map[string]int) []BalanceUpdateRecord {
	records := make([]BalanceUpdateRecord, 0, len(bitTypeMap))

	for bitType, bitAmount := range bitTypeMap {
		purchasePrice, _ := strconv.ParseFloat(purchasePrice, 64)
		record := BalanceUpdateRecord{
			ChangeID:              uuid.New(),
			TransactionID:         transactionID,
			BitAccountType:        bitType,
			BitAmount:             bitAmount,
			TimeOfEvent:           timeOfEvent.Unix(),
			PurchasePrice:         purchasePrice,
			PurchasePriceCurrency: purchasePriceCurrency,
			Marketplace:           marketplace,
		}

		records = append(records, record)
	}

	if len(sponsoredAmounts) > 0 {
		record := BalanceUpdateRecord{
			ChangeID:              uuid.New(),
			TransactionID:         transactionID,
			BitAccountType:        SponsoredCheermoteBitType,
			BitAmount:             -handlers.BitsUsedWithSponsor(0, sponsoredAmounts),
			TimeOfEvent:           timeOfEvent.Unix(),
			PurchasePrice:         float64(0),
			PurchasePriceCurrency: EmptyString,
			Marketplace:           EmptyString,
		}
		records = append(records, record)
	}

	return records
}
