package models

import (
	"encoding/json"
	"net/http"
	"time"

	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/models/api"
	"code.justin.tv/commerce/payday/partner"
	"code.justin.tv/commerce/payday/utils/pointers"
	timeUtils "code.justin.tv/commerce/payday/utils/time"
)

var DefaultLeaderboardTimePeriod = pantheonrpc.TimeUnit_WEEK.String()

func NewGetOptOutResponse(channel *dynamo.Channel) *api.GetOptOutResponse {
	return &api.GetOptOutResponse{
		User:        string(channel.Id),
		OptedOut:    pointers.BoolOrDefault(channel.OptedOut, false),
		Annotation:  pointers.StringOrDefault(channel.Annotation, ""),
		LastUpdated: channel.LastUpdated.UnixNano(),
	}
}

func DefaultGetSettingsResponse(eligibleForBitsProgram, hasServicesRecord, amendmentSigned bool) *api.GetSettingsResponse {
	return &api.GetSettingsResponse{
		MinBits:                     api.DefaultMinBits,
		MinBitsEmote:                api.DefaultMinBits,
		EligibleForBitsProgram:      eligibleForBitsProgram,
		Onboarded:                   false,
		AmendmentSigned:             amendmentSigned,
		HasServicesRecord:           hasServicesRecord,
		PinTopCheers:                false,
		PinRecentCheers:             false,
		RecentCheerMin:              api.DefaultRecentCheerMin,
		RecentCheerTimeout:          api.DefaultRecentCheerTimeoutSeconds,
		CanEditBadgeImages:          false,
		ShouldSeeBitsNotification:   false,
		LeaderboardEnabled:          api.DefaultLeaderboardEnabled,
		IsCheerLeaderboardEnabled:   api.DefaultLeaderboardEnabled,
		IsSubGiftLeaderboardEnabled: api.DefaultLeaderboardEnabled,
		DefaultLeaderboard:          api.DefaultLeaderboard,
		LeaderboardTimePeriod:       DefaultLeaderboardTimePeriod,
		OptedOutOfVoldemort:         api.DefaultOptedOutOfVoldemort,
	}
}

func NewGetSettingsResponse(channel *dynamo.Channel, eligibleForBitsProgram, amendmentSigned bool, hasServicesRecord bool, prefix string, partnerType partner.PartnerType, showNotification bool) *api.GetSettingsResponse {
	if channel == nil {
		return DefaultGetSettingsResponse(eligibleForBitsProgram, hasServicesRecord, amendmentSigned)
	}

	canEditBadgeImages := partnerType == partner.TraditionalPartnerType

	return &api.GetSettingsResponse{
		MinBits:                     pointers.Int64OrDefault(channel.MinBits, api.DefaultMinBits),
		MinBitsEmote:                pointers.Int64OrDefault(channel.MinBitsEmote, api.DefaultMinBits),
		EligibleForBitsProgram:      eligibleForBitsProgram,
		HasServicesRecord:           hasServicesRecord,
		Onboarded:                   pointers.BoolOrDefault(channel.Onboarded, false),
		AmendmentSigned:             amendmentSigned,
		PinTopCheers:                pointers.BoolOrDefault(channel.PinTopCheers, api.DefaultPinTopCheers),
		PinRecentCheers:             pointers.BoolOrDefault(channel.PinRecentCheers, api.DefaultPinRecentCheers),
		RecentCheerMin:              pointers.Int64OrDefault(channel.RecentCheerMin, api.DefaultRecentCheerMin),
		RecentCheerTimeout:          pointers.Int64OrDefault(timeUtils.NanosecondsToSecondsP(channel.RecentCheerTimeout), api.DefaultRecentCheerTimeoutSeconds),
		CustomCheermoteEnabled:      getCustomCheermoteEnabled(channel.CustomCheermoteStatus),
		Prefix:                      prefix,
		CanEditBadgeImages:          canEditBadgeImages,
		ShouldSeeBitsNotification:   showNotification,
		LeaderboardEnabled:          GetLeaderboardEnabled(channel),
		IsCheerLeaderboardEnabled:   GetIsCheerLeaderboardEnabled(channel),
		IsSubGiftLeaderboardEnabled: GetIsSubGiftLeaderboardEnabled(channel),
		DefaultLeaderboard:          pointers.StringOrDefault(channel.DefaultLeaderboard, api.DefaultLeaderboard),
		LeaderboardTimePeriod:       pointers.StringOrDefault(channel.LeaderboardTimePeriod, DefaultLeaderboardTimePeriod),
		CheerBombEventOptOut:        pointers.BoolOrDefault(channel.CheerBombEventOptOut, api.DefaultCheerBombEventOptOut),
		OptedOutOfVoldemort:         pointers.BoolOrDefault(channel.OptedOutOfVoldemort, api.DefaultOptedOutOfVoldemort),
	}
}

func NewGetMinBitsResponse(channel *dynamo.Channel) *api.GetMinBitsResponse {
	return &api.GetMinBitsResponse{
		User:        string(channel.Id),
		MinBits:     pointers.Int64OrDefault(channel.MinBits, 1),
		LastUpdated: channel.LastUpdated.UnixNano(),
	}
}

func ExtractSetMinBitsRequest(httpRquest *http.Request) (*api.SetMinBitsRequest, error) {
	var requestModel api.SetMinBitsRequest
	decoder := json.NewDecoder(httpRquest.Body)
	err := decoder.Decode(&requestModel)
	return &requestModel, err
}

func ExtractUpdateSettingsRequest(httpRquest *http.Request) (*api.UpdateSettingsRequest, error) {
	var requestModel api.UpdateSettingsRequest
	decoder := json.NewDecoder(httpRquest.Body)
	err := decoder.Decode(&requestModel)
	return &requestModel, err
}

func ExtractSetOptOutRequest(httpRquest *http.Request) (*api.SetOptOutRequest, error) {
	var requestModel api.SetOptOutRequest
	decoder := json.NewDecoder(httpRquest.Body)
	err := decoder.Decode(&requestModel)
	return &requestModel, err
}

func ExtractCustomCheermoteRequest(httpRequest *http.Request) (*api.UploadCustomCheermoteRequest, error) {
	var requestModel api.UploadCustomCheermoteRequest
	decoder := json.NewDecoder(httpRequest.Body)
	err := decoder.Decode(&requestModel)
	return &requestModel, err
}

func OnboardedDynamoChannel(channelId string, amendmentState *string) *dynamo.Channel {
	return &dynamo.Channel{
		Id:             dynamo.ChannelId(channelId),
		Onboarded:      pointers.BoolP(true),
		AmendmentState: amendmentState,
	}
}

func SetOptOutRequestToDynamoChannel(r *api.SetOptOutRequest, channelId string) *dynamo.Channel {
	return &dynamo.Channel{
		Id:         dynamo.ChannelId(channelId),
		OptedOut:   pointers.BoolP(r.OptedOut),
		Annotation: pointers.StringP(r.Annotation),
	}
}

func SetMinBitsRequestToDynamoChannel(r *api.SetMinBitsRequest, channelId string) *dynamo.Channel {
	return &dynamo.Channel{
		Id:      dynamo.ChannelId(channelId),
		MinBits: pointers.Int64P(r.MinBits),
	}
}

func UpdateSettingsRequestToDynamoChannel(r *api.UpdateSettingsRequest, channelId string) *dynamo.Channel {
	return &dynamo.Channel{
		Id:                          dynamo.ChannelId(channelId),
		MinBits:                     r.MinBits,
		MinBitsEmote:                r.MinBitsEmote,
		PinTopCheers:                r.PinTopCheers,
		PinRecentCheers:             r.PinRecentCheers,
		RecentCheerMin:              r.RecentCheerMin,
		RecentCheerTimeout:          timeUtils.SecondsToNanosecondsP(r.RecentCheerTimeout),
		CustomCheermoteStatus:       buildCustomCheermoteStatus(r.CustomCheermoteEnabled),
		LeaderboardEnabled:          r.LeaderboardEnabled,
		IsCheerLeaderboardEnabled:   r.IsCheerLeaderboardEnabled,
		IsSubGiftLeaderboardEnabled: r.IsSubGiftLeaderboardEnabled,
		DefaultLeaderboard:          r.DefaultLeaderboard,
		LeaderboardTimePeriod:       r.LeaderboardTimePeriod,
		CheerBombEventOptOut:        r.CheerBombEventOptOut,
		OptedOutOfVoldemort:         r.OptedOutOfVoldemort,
	}
}

/*
	First respect the leaderboard enabled value if it's explicitly set
	If not set, default to the the pin top cheer enabled value
	If that's not set, default to the api default value
*/
func GetLeaderboardEnabled(channel *dynamo.Channel) bool {
	if channel == nil {
		return false
	}

	var leaderboardEnabled bool
	if channel.LeaderboardEnabled != nil {
		leaderboardEnabled = *channel.LeaderboardEnabled
	} else if channel.PinTopCheers != nil {
		leaderboardEnabled = *channel.PinTopCheers
	} else {
		leaderboardEnabled = api.DefaultLeaderboardEnabled
	}

	return leaderboardEnabled
}

/*
 * Get IsCheerLeaderboardEnabled if it exists, otherwise
 * fallback to old setting LeaderboardEnabled
 * which controlled cheering and sub gifting leaderboards.
 */
func GetIsCheerLeaderboardEnabled(channel *dynamo.Channel) bool {
	if channel == nil {
		return false
	}
	return pointers.BoolOrDefault(channel.IsCheerLeaderboardEnabled, GetLeaderboardEnabled(channel))
}

/*
 * Get IsSubGiftLeaderboardEnabled if it exists, otherwise
 * fallback to old setting LeaderboardEnabled
 * which controlled cheering and sub gifting leaderboards.
 */
func GetIsSubGiftLeaderboardEnabled(channel *dynamo.Channel) bool {
	if channel == nil {
		return false
	}
	return pointers.BoolOrDefault(channel.IsSubGiftLeaderboardEnabled, GetLeaderboardEnabled(channel))
}

func getCustomCheermoteEnabled(status *string) bool {
	return pointers.StringOrDefault(status, api.DefaultCustomCheermoteStatus) == api.CustomCheermoteStatusEnabled
}

func buildCustomCheermoteStatus(enabled *bool) *string {
	if enabled == nil {
		return nil
	}
	if *enabled {
		return pointers.StringP(api.CustomCheermoteStatusEnabled)
	} else {
		return pointers.StringP(api.CustomCheermoteStatusDisabled)
	}
}

func GetOptOutResponseToDynamoChannel(r *api.GetOptOutResponse) *dynamo.Channel {
	return &dynamo.Channel{
		Id:          dynamo.ChannelId(r.User),
		OptedOut:    pointers.BoolP(r.OptedOut),
		Annotation:  pointers.StringP(r.Annotation),
		LastUpdated: time.Unix(0, r.LastUpdated),
	}
}

func GetMinBitsResponseToDynamoChannel(r *api.GetMinBitsResponse) *dynamo.Channel {
	return &dynamo.Channel{
		Id:          dynamo.ChannelId(r.User),
		MinBits:     pointers.Int64P(r.MinBits),
		LastUpdated: time.Unix(0, r.LastUpdated),
	}
}
