package models

import (
	"time"

	"code.justin.tv/commerce/payday/models/api"
)

type CheerType int32

const (
	CheerTypeUnknown          CheerType = 0
	CheerTypeGlobalFirstParty CheerType = 1
	CheerTypeGlobalThirdParty CheerType = 2
	CheerTypeChannelCustom    CheerType = 3
	CheerTypeSponsored        CheerType = 4
	CheerTypeDisplayOnly      CheerType = 5
	CheerTypeCharity          CheerType = 6
	CheerTypeDefaultCheer     CheerType = 7
	CheerTypeAnonymousCheer   CheerType = 8

	PrefixTemplateArg     = "PREFIX"
	BackgroundTemplateArg = "BACKGROUND"
	AnimationTemplateArg  = "ANIMATION"
	TierTemplateArg       = "TIER"
	ScaleTemplateArg      = "SCALE"
	ExtensionTemplateArg  = "EXTENSION"
)

type CheerTier struct {
	MinBits        int32
	ShowInBitsCard bool
}

type Cheer struct {
	Prefix   string
	Type     CheerType
	Tiers    []CheerTier
	Campaign *CheerCampaign
}

type DisplayType struct {
	AnimationType api.AnimationType
	Extension     api.Extension
}

type CheerGroup struct {
	Template string
	Cheers   []Cheer
}

type CheerCampaign struct {
	ID                        string
	TotalBits                 int64
	UsedBits                  int64
	StartTime                 time.Time
	EndTime                   time.Time
	IsEnabled                 bool
	MinBitsToBeSponsored      int64
	SponsoredAmountThresholds map[int64]float64
	UserLimit                 int64
	UserUsedBits              int64
	BrandName                 string
	BrandImageURL             string
}
