package models

import (
	"math"
	"strconv"

	flotwirp "code.justin.tv/revenue/flo/rpc"
)

type Price struct {
	ProductId    string
	Price        string
	Currency     string
	TaxInclusive bool
}

func FormatPriceFromFlo(pricing *flotwirp.Pricing) string {
	if pricing == nil {
		return ""
	}

	exp := int(pricing.Exponent)
	factor := math.Pow10(exp)

	var price string
	if pricing.TaxInclusive {
		price = strconv.FormatFloat(float64(pricing.Total)/factor, 'f', exp, 32)
	} else {
		price = strconv.FormatFloat(float64(pricing.Price)/factor, 'f', exp, 32)
	}

	return price
}
