package api

type AdsResponse struct {
	BitsGranted int32 `json:"bits_granted"`
}

type AdsRequest struct {
	TwitchUserId      string     `json:"user_id"`
	ChannelId         string     `json:"channel_id"`
	EngagementReceipt Engagement `json:"engagement_receipt"`
}

type Engagement struct {
	Signature               string `json:"signature"`
	SignatureArgumentString string `json:"signature_argument_string"`
	Ad                      Ad     `json:"ad"`
}

type Ad struct {
	CreativeId     string `json:"creative_id"`
	CampaignId     string `json:"campaign_id"`
	Name           string `json:"name"`
	CurrencyAmount string `json:"currency_amount"`
}
