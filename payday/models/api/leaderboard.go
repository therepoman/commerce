package api

type LeaderboardEntry struct {
	Rank     int64  `json:"rank"`
	Score    int64  `json:"score"`
	EntryKey string `json:"entry_key"`
	ID       string `json:"id"`
}

type GetLeaderboardResponse struct {
	Top              []LeaderboardEntry `json:"top"`
	Entry            LeaderboardEntry   `json:"entry"`
	EntryContext     []LeaderboardEntry `json:"entry_context"`
	ID               string             `json:"id"`
	SecondsRemaining int32              `json:"seconds_remaining"`
}

type GetLeaderboardOptions struct {
	//The max length of the Top array in the GetLeaderboardResponse
	TopNum int
	//The number of surrounding entries in the EntryContext array in the GetLeaderboardResponse
	Range int
	//The twitch user to be specified in the entry context array
	TwitchUserId string
}
