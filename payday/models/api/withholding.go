package api

type GetWithholdingByAccountIdResponse struct {
	AccountId             string            `json:"account_id"`
	AddressId             string            `json:"address_id"`
	TaxWithholdingRateMap map[string]string `json:"tax_withholding_rate"`
	TaxInterviewStatus    string            `json:"tax_interview_status"`
}

type GetWithholdingByPayoutEntityIdResponse struct {
	PayoutEntityId        string            `json:"payout_entity_id"`
	AddressId             string            `json:"address_id"`
	TaxWithholdingRateMap map[string]string `json:"tax_withholding_rate"`
	TaxInterviewStatus    map[string]string `json:"tax_interview_status"`
}
