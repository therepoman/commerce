package api

type EntitleBitsRequest struct {
	TwitchUserId              string `json:"user_id"`        // Required
	TransactionId             string `json:"transaction_id"` // Required
	ProductId                 string `json:"product_id"`     // Required for store purchases
	BitAmount                 int32  `json:"bit_amount"`     // Required for fortuna, upsight
	BitsType                  string `json:"bits_type"`      // Required for fortuna
	Platform                  string `json:"platform"`       // Required
	Price                     string `json:"price"`
	Currency                  string `json:"currency"`
	ProductQuantity           int32  `json:"product_quantity"` // requires ProductId to be set
	TrackingId                string `json:"tracking_id"`
	OriginOrderId             string `json:"origin_order_id"`
	OverrideProductBitsAmount bool   `json:"override_product_bits_amount"`
}

type EntitleBitsResponse struct {
	BitsBalance int32 `json:"bits_balance"`
}
