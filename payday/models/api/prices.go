package api

// data structures for returning pricing info via ADGDiscoveryService

type GetPricesRequest struct {
	UserID             string
	ASINs              []string
	Platform           string
	CountryOfResidence string
}

type Price struct {
	ASIN                   string `json:"asin"`
	Price                  string `json:"price"`
	USPrice                string `json:"us_price"`
	CurrencyUnit           string `json:"currency_unit"`
	BitsAmount             int    `json:"bits_amount"`
	Discount               int    `json:"discount"`
	Promo                  bool   `json:"promo"`
	PromoType              string `json:"promo_type,omitempty"`
	PromoID                string `json:"promo_id,omitempty"`
	ShowPromoWhenLoggedOut bool   `json:"show_promo_when_logged_out,omitempty"`
	PurchaseURL            string `json:"purchase_url,omitempty"`
	OfferID                string `json:"offer_id,omitempty"`
}

type GetPricesResponse struct {
	Prices           []Price `json:"prices"`
	MinimumAdBits    int64   `json:"minimum_ad_bits"`
	BitsPurchaseURL  string  `json:"bits_purchase_url"`
	PriceIncludesVAT bool    `json:"price_includes_vat"`
}
