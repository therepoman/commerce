package models

import "time"

type ErrorType int

const (
	InsufficientFundsError             = "insufficient_funds"
	InvalidCheerError                  = "invalid_cheer"
	BitsUnavailableError               = "bits_unavailable"
	NonPartnerError                    = "non_partner"
	OptOutError                        = "opt_out"
	NotOnboardedError                  = "not_onboarded"
	UserBannedError                    = "user_banned"
	AmountBelowMinBitsError            = "below_min"
	AmountBelowMinBitsEmoteError       = "below_min_emote"
	DuplicateEventIdError              = "duplicate_event_id"
	RequestThrottledError              = "request_throttled"
	BitsIneligibleChannelMismatchError = "bits_ineligible_channel_mismatch"
)

const (
	CheerContext                       = "cheer"
	CheerChallengeContext              = "cheer_challenge"
	UseBitsOnExtensionContext          = "bits_extension"
	UseBitsOnExtensionChallengeContext = "bits_extension_challenge"
	UseBitsOnPollContext               = "poll"
)

const (
	Default ErrorType = iota
	InsufficientFunds
	InvalidCheer
	AutoModCheer
	BitsUnavailable
	UserIneligible
	ChannelIneligible
	AmountBelowMinBits
	EmoteBelowMinBitsEmote
	DuplicateEventId
	RequestThrottled
	BitsIneligibleChannelMismatch
	AnonymousCheerInvalid
)

type AutoModOutcome string

const (
	AutoModApproved = AutoModOutcome("approved")
	AutoModDenied   = AutoModOutcome("denied")
	AutoModTimedout = AutoModOutcome("timedout")
)

type DataScienceEvent struct {
	Event      string      `json:"event"`
	Properties interface{} `json:"properties"`
}

type BitsUsed struct {
	TransactionId    string    `json:"transaction_id"`
	UserId           string    `json:"user_id" datascience:"userID,distinctID"`
	UserLogin        string    `json:"user_login" datascience:"username"`
	ChannelID        string    `json:"channel_id" datascience:"channelID"`
	ChannelLogin     string    `json:"channel_login" datascience:"channelname"`
	Context          string    `json:"context"`
	ContextDetails   string    `json:"context_details"`
	Promotion        string    `json:"promotion,omitempty"`
	CurrentBalance   int32     `json:"current_balance"`
	UsedTotal        int       `json:"used_total"`
	AmountTotalEmote int       `json:"amount_total_emote"`
	UsedCB0          int       `json:"used_cb0"`
	UsedCB140        int       `json:"used_cb140"`
	UsedCB700        int       `json:"used_cb700"`
	UsedCB1995       int       `json:"used_cb1995"`
	UsedCB6440       int       `json:"used_cb6440"`
	UsedCB12600      int       `json:"used_cb12600"`
	UsedCB30800      int       `json:"used_cb30800"`
	UsedCBP00100     int       `json:"used_cbp00100"`
	UsedCB2000       int       `json:"used_cb2000"`
	UsedBitsForAds   int       `json:"used_bitsforads"`
	UsedBitsForCrate int       `json:"used_bitsforcrate"`
	ServerTime       time.Time `json:"-" datascience:"servertime"`
}

type BitsUsedV2 struct {
	TransactionId         string    `json:"transaction_id"`
	UserId                string    `json:"user_id" datascience:"userID,distinctID"`
	UserLogin             string    `json:"user_login" datascience:"username"`
	ChannelID             string    `json:"channel_id" datascience:"channelID"`
	ChannelLogin          string    `json:"channel_login" datascience:"channelname"`
	Context               string    `json:"context"`
	ContextDetails        string    `json:"context_details"`
	ContextID             string    `json:"context_id"`
	Promotion             string    `json:"promotion"`
	CurrentBalance        int32     `json:"current_balance"`
	UsedTotal             int       `json:"used_total"`
	AmountTotalEmote      int       `json:"amount_total_emote"`
	UsedOfThisType        int       `json:"used_of_this_type"`
	ServerTime            time.Time `json:"-" datascience:"servertime"`
	BitType               string    `json:"bit_type"`
	NumPubSubListeners    int       `json:"num_pubsub_listeners"`
	NumOfCheermotes       int       `json:"num_of_cheermotes"`
	IsAnonymous           bool      `json:"is_anonymous"`
	AccountTypeSpendOrder int       `json:"accountTypeSpendOrder"`
	ClientApp             string    `json:"client_app"`
	SponsoredAmount       int       `json:"sponsored_amount"`
	IncludesCustomText    bool      `json:"includes_custom_text"`
	ZipCode               string    `json:"zip_code"`
	CountryCode           string    `json:"country_code"`
	City                  string    `json:"city"`
}

type BitsEmoteCheer struct {
	TransactionID      string    `json:"transaction_id"`
	UserID             string    `json:"user_id" datascience:"userID,distinctID"`
	UserLogin          string    `json:"user_login" datascience:"username"`
	ChannelID          string    `json:"channel_id" datascience:"channelID"`
	ChannelLogin       string    `json:"channel_login" datascience:"channelname"`
	EmoteType          string    `json:"emote_type"`
	EmoteTotal         int64     `json:"emote_total"`
	ServerTime         time.Time `json:"-" datascience:"servertime"`
	NumPubSubListeners int       `json:"num_pubsub_listeners"`
	EmoteIndex         int       `json:"emote_index"`
}

type BitsUsedError struct {
	UserID            string `json:"user_id" datascience:"userID,distinctID"`
	UserLogin         string `json:"user_login" datascience:"username"`
	ChannelID         string `json:"channel_id" datascience:"channelID"`
	ChannelLogin      string `json:"channel_login" datascience:"channelname"`
	Context           string `json:"context"`
	ContextDetails    string `json:"context_details"`
	QuantityAttempted int32  `json:"quantity_attempted"`
	CurrentBalance    string `json:"current_balance,omitempty"`
	ErrorType         string `json:"error_type"`
}

type BitsAcquired struct {
	TransactionID        string    `json:"transaction_id"`
	UserID               string    `json:"user_id" datascience:"userID,distinctID"`
	UserLogin            string    `json:"user_login" datascience:"username"`
	AdminID              string    `json:"admin_id,omitempty"`
	AdminReason          string    `json:"admin_reason,omitempty"`
	AcquisitionReason    string    `json:"acquisition_reason"`
	CurrentBalance       int       `json:"current_balance"`
	AcquiredTotal        int       `json:"acquired_total"`
	AcquiredCB0          int       `json:"acquired_cb0"`
	AcquiredCB140        int       `json:"acquired_cb140"`
	AcquiredCB700        int       `json:"acquired_cb700"`
	AcquiredCB1995       int       `json:"acquired_cb1995"`
	AcquiredCB6440       int       `json:"acquired_cb6440"`
	AcquiredCB12600      int       `json:"acquired_cb12600"`
	AcquiredCB30800      int       `json:"acquired_cb30800"`
	AcquiredCBP00100     int       `json:"acquired_cbp00100"`
	AcquiredCB2000       int       `json:"acquired_cb2000"`
	AcquiredBitsForAds   int       `json:"acquired_bitsforads"`
	AcquiredBitsForCrate int       `json:"acquired_bitsforcrate"`
	UsedBitsForCode      int       `json:"used_bitsforcode"`
	ServerTime           time.Time `json:"-" datascience:"servertime"`
}

type BitsAcquiredV2 struct {
	TransactionID         string    `json:"transaction_id"`
	UserID                string    `json:"user_id" datascience:"userID,distinctID"`
	UserLogin             string    `json:"user_login" datascience:"username"`
	AdminID               string    `json:"admin_id,omitempty"`
	AdminReason           string    `json:"admin_reason,omitempty"`
	AcquisitionReason     string    `json:"acquisition_reason"`
	CurrentBalance        int       `json:"current_balance"`
	AcquiredTotal         int       `json:"acquired_total"`
	AcquiredOfThisType    int       `json:"acquired_of_this_type"`
	ServerTime            time.Time `json:"-" datascience:"servertime"`
	BitType               string    `json:"bit_type"`
	PurchasePrice         string    `json:"purchase_price"`
	PurchasePriceCurrency string    `json:"purchase_price_currency"`
	PurchaseMarketplace   string    `json:"purchase_price_marketplace"`
	PurchaseOrderID       string    `json:"purchase_order_id"`
	TrackingID            string    `json:"tracking_id"`
	AccountTypeSpendOrder int       `json:"accountTypeSpendOrder"`
	Quantity              int       `json:"quantity"`
}

type BitsBalance struct {
	TransactionID     string `json:"transaction_id"`
	UserID            string `json:"user_id" datascience:"userID,distinctID"`
	UserLogin         string `json:"user_login" datascience:"username"`
	CurrentBalance    int    `json:"current_balance"`
	Reason            string `json:"reason"`
	BitsAmountChanged int    `json:"bits_amount_changed"`
}

type BitsRemoved struct {
	TransactionID       string    `json:"transaction_id"`
	UserID              string    `json:"user_id" datascience:"userID,distinctID"`
	UserLogin           string    `json:"user_login" datascience:"username"`
	AdminID             string    `json:"admin_id,omitempty"`
	AdminReason         string    `json:"admin_reason,omitempty"`
	RemovedReason       string    `json:"removed_reason"`
	CurrentBalance      int       `json:"current_balance"`
	RemovedTotal        int       `json:"removed_total"`
	RemovedCB0          int       `json:"removed_cb0"`
	RemovedCB140        int       `json:"removed_cb140"`
	RemovedCB700        int       `json:"removed_cb700"`
	RemovedCB1995       int       `json:"removed_cb1995"`
	RemovedCB6440       int       `json:"removed_cb6440"`
	RemovedCB12600      int       `json:"removed_cb12600"`
	RemovedCB30800      int       `json:"removed_cb30800"`
	RemovedCBP00100     int       `json:"removed_cbp00100"`
	RemovedCB2000       int       `json:"removed_cb2000"`
	RemovedBitsForAds   int       `json:"removed_bitsforads"`
	RemovedBitsForCrate int       `json:"removed_bitsforcrate"`
	ServerTime          time.Time `json:"-" datascience:"servertime"`
}

type BitsRemovedV2 struct {
	TransactionID         string    `json:"transaction_id"`
	UserID                string    `json:"user_id" datascience:"userID,distinctID"`
	UserLogin             string    `json:"user_login" datascience:"username"`
	AdminID               string    `json:"admin_id,omitempty"`
	AdminReason           string    `json:"admin_reason,omitempty"`
	RemovedReason         string    `json:"removed_reason"`
	CurrentBalance        int       `json:"current_balance"`
	RemovedTotal          int       `json:"removed_total"`
	RemovedOfThisType     int       `json:"removed_of_this_type"`
	BitType               string    `json:"bit_type"`
	ServerTime            time.Time `json:"-" datascience:"servertime"`
	AccountTypeSpendOrder int       `json:"accountTypeSpendOrder"`
}

type BitsClawback struct {
	TransactionID         string    `json:"transaction_id"`
	TransactionType       string    `json:"transaction_type"`
	RevokeID              string    `json:"revoke_id"`
	UserID                string    `json:"user_id" datascience:"userID,distinctID"`
	UserLogin             string    `json:"user_login" datascience:"username"`
	CostBasis             string    `json:"cost_basis"`
	Amount                int       `json:"amount"`
	AmountRequested       int       `json:"amount_requested"`
	ServerTime            time.Time `json:"-" datascience:"servertime"`
	PurchasePrice         string    `json:"purchase_price"`
	PurchasePriceCurrency string    `json:"purchase_price_currency"`
}

type PartnerOnboarded struct {
	ChannelID    string    `json:"channel_id" datascience:"userID,distinctID"`
	ChannelLogin string    `json:"channel_login" datascience:"username"`
	PartnerType  string    `json:"partner_type"`
	ServerTime   time.Time `json:"-" datascience:"servertime"`
}

type BitsChannelSettings struct {
	ChannelID                   string    `json:"channel_id" datascience:"distinctID,channelID"`
	ChannelLogin                string    `json:"channel_login" datascience:"channelname"`
	ServerTime                  time.Time `json:"-" datascience:"servertime"`
	PinCheerFeatureEnabled      *bool     `json:"pin_cheer_feature_enabled,omitempty"`
	TopCheerFeatureEnabled      *bool     `json:"top_cheer_feature_enabled,omitempty"`
	PinRecentCheerMin           *int64    `json:"pin_cheer_recent_min,omitempty"`
	PinRecentCheerTimeout       *int64    `json:"pin_cheer_recent_timeout,omitempty"`
	MinimumBits                 *int64    `json:"min_bits,omitempty"`
	MinimumBitsEmote            *int64    `json:"min_bits_emote,omitempty"`
	LeaderboardEnabled          *bool     `json:"leaderboard_enabled,omitempty"`
	IsCheerLeaderboardEnabled   *bool     `json:"is_cheer_leaderboard_enabled,omitempty"`
	IsSubGiftLeaderboardEnabled *bool     `json:"is_sub_gift_leaderboard_enabled,omitempty"`
	DefaultLeaderboard          *string   `json:"default_leaderboard,omitempty"`
	LeaderboardTimePeriod       *string   `json:"leaderboard_time_period,omitempty"`
	CheerBombEventOptOut        *bool     `json:"cheer_bomb_event_opt_out,omitempty"`
	Onboarded                   *bool     `json:"onboarded,omitempty"`
	OptedOutOfVoldemort         *bool     `json:"opted_out_of_voldemort"`
}

type BitsBadgeTierSettings struct {
	ChannelID           string `json:"channel_id" datascience:"distinctID,channelID"`
	ChannelLogin        string `json:"channel_login" datascience:"channelname"`
	BadgeAmount         int64  `json:"badge_amount"`
	BadgeFeatureEnabled bool   `json:"badge_feature_enabled"`
	Type                string `json:"custom"`
	Action              string `json:"action"`
	EmoteID             string `json:"emote_id"`
}

type BitsBadgeUploadEvent struct {
	ChannelID           string `json:"channel_id" datascience:"distinctID,channelID"`
	ChannelLogin        string `json:"channel_login" datascience:"channelname"`
	BadgeAmount         int64  `json:"badge_amount"`
	BadgeFeatureEnabled bool   `json:"badge_feature_enabled"`
	Action              string `json:"action"`
	TitleAction         string `json:"title_action"`
	CustomTitle         string `json:"title"`
}

type BadgeEntitleEvent struct {
	ChannelID     string `json:"channel_id" datascience:"distinctID,channelID"`
	ChannelLogin  string `json:"channel_login" datascience:"channelname"`
	UserID        string `json:"user_id" datascience:"userID"`
	UserLogin     string `json:"user_login" datascience:"username"`
	BadgeAmount   int64  `json:"badge_amount"`
	EarnType      string `json:"earn_type"`
	TransactionID string `json:"transaction_id,omitempty"`
	Type          string `json:"custom"`
}

type BadgeEquipEvent struct {
	ChannelID     string `json:"channel_id" datascience:"distinctID,channelID"`
	ChannelLogin  string `json:"channel_login" datascience:"channelname"`
	UserID        string `json:"user_id" datascience:"userID"`
	UserLogin     string `json:"user_login" datascience:"username"`
	BadgeAmount   int64  `json:"badge_amount"`
	EarnType      string `json:"earn_type"`
	TransactionID string `json:"transaction_id,omitempty"`
	Type          string `json:"custom"`
}

type BitsBadgeDisplaySelectionEvent struct {
	ServerTime   time.Time `json:"-" datascience:"servertime"`
	ChannelID    string    `json:"channel_id" datascience:"distinctID,channelID"`
	ChannelLogin string    `json:"channel_login" datascience:"channelname"`
	UserID       string    `json:"user_id" datascience:"userID"`
	UserLogin    string    `json:"user_login" datascience:"username"`
	BadgeName    string    `json:"badge_name"`
	BadgeAmount  int64     `json:"badge_amount"`
	Action       string    `json:"action"`
}

type BitsRewardClaimEvent struct {
	ChannelID         string `json:"channel_id" datascience:"distinctID,channelID"`
	ChannelLogin      string `json:"channel_login" datascience:"channelname"`
	UserID            string `json:"user_id" datascience:"userID"`
	UserLogin         string `json:"user_login" datascience:"username"`
	FulfilledBy       string `json:"fulfilled_by"`
	Game              string `json:"game"`
	Platform          string `json:"platform"`
	RewardType        string `json:"reward_type"`
	RewardDescription string `json:"reward_description"`
	RewardTier        int64  `json:"reward_tier"`
}

type BitsPrices struct {
	UserID     string    `json:"user_id" datascience:"distinctID,userID"`
	UserLogin  string    `json:"user_login" datascience:"username"`
	AsinList   string    `json:"asin_list"`
	ServerTime time.Time `json:"-" datascience:"servertime"`
}

type BitsPartnerCheermoteDetail struct {
	ImageSetId      string `json:"image_set_id"`
	CheermoteString string `json:"cheermote_string,omitempty"`
	ChannelId       string `json:"channel_id" datascience:"channelID,distinctID"`
	Channel         string `json:"channel" datascience:"channelname"`
	ImageSize       int64  `json:"image_size"`
	ImageSetting    string `json:"image_setting"`
	UploadType      string `json:"upload_type"`
	Tier            string `json:"tier"`
}

type BitsCheermoteEnabled struct {
	ImageSetId      string `json:"image_set_id"`
	CheermoteString string `json:"cheermote_string,omitempty"`
	ChannelId       string `json:"channel_id" datascience:"channelID,distinctID"`
	Channel         string `json:"channel" datascience:"channelname"`
}

type MobileCountryUnavailable struct {
	UserID        string `json:"user_id" datascience:"distinctID,userID"`
	Platform      string `json:"platform"`
	Country       string `json:"country"`
	FailureReason string `json:"failure_reason"`
}

type BitsLeaderboardUpdate struct {
	UserID                              string    `json:"user_id" datascience:"distinctID,userID"`
	UserLogin                           string    `json:"user_login" datascience:"username"`
	ChannelSettingLeaderboardEnabled    bool      `json:"channel_setting_leaderboard_enabled"`
	ChannelSettingLeaderboardTimePeriod string    `json:"channel_setting_leaderboard_time_period"`
	EventLeaderboardTimePeriod          string    `json:"event_leaderboard_time_period"`
	RankAfter                           int64     `json:"rank_after"`
	DiffScoreAboveAfter                 int64     `json:"diff_score_above_after"`
	DiffScoreBelowAfter                 int64     `json:"diff_score_below_after"`
	Settings                            string    `json:"settings"`
	TransactionID                       string    `json:"transaction_id"`
	ScoreAfter                          int64     `json:"score_after"`
	UsedTotal                           int64     `json:"used_total"`
	Promotion                           string    `json:"promotion"`
	ChannelLogin                        string    `json:"channel_login" datascience:"channelname"`
	ChannelID                           string    `json:"channel_id" datascience:"channelID"`
	Time                                time.Time `json:"time"`
	TimeUTC                             time.Time `json:"time_utc"`
}

type BitsUsedOnExtension struct {
	TransactionID      string    `json:"transaction_id,distinctID"`
	ExtensionClientID  string    `json:"extension_client_id"`
	SKU                string    `json:"sku"`
	UserID             string    `json:"user_id" datascience:"userID"`
	UserLogin          string    `json:"user_login" datascience:"username"`
	ChannelID          string    `json:"channel_id" datascience:"channelID"`
	ChannelLogin       string    `json:"channel_login" datascience:"channelname"`
	ExtraContext       string    `json:"extra_context"`
	TotalBitsProcessed int       `json:"total_bits_processed"`
	CostBasis          string    `json:"cost_basis"`
	Amount             int       `json:"amount"`
	Context            string    `json:"context"`
	ServerTime         time.Time `json:"-" datascience:"servertime"`
}

type BitsCheered struct {
	TransactionId    string    `json:"transaction_id"`
	UserId           string    `json:"user_id" datascience:"userID,distinctID"`
	UserLogin        string    `json:"user_login" datascience:"username"`
	ChannelID        string    `json:"channel_id" datascience:"channelID"`
	ChannelLogin     string    `json:"channel_login" datascience:"channelname"`
	Context          string    `json:"context"`
	ContextDetails   string    `json:"context_details"`
	Promotion        string    `json:"promotion"`
	CurrentBalance   int32     `json:"current_balance"`
	UsedTotal        int       `json:"used_total"`
	AmountTotalEmote int       `json:"amount_total_emote"`
	UsedOfThisType   int       `json:"used_of_this_type"`
	ServerTime       time.Time `json:"-" datascience:"servertime"`
	BitType          string    `json:"bit_type"`
}

type BitsAutoModFlaggedMessage struct {
	TransactionID string    `json:"transaction_id"`
	UserID        string    `json:"user_id" datascience:"userID,distinctID"`
	UserLogin     string    `json:"user_login" datascience:"username"`
	ChannelID     string    `json:"channel_id" datascience:"channelID"`
	ChannelLogin  string    `json:"channel_login" datascience:"channelname"`
	UsedTotal     int       `json:"used_total"`
	ServerTime    time.Time `json:"-" datascience:"servertime"`
	Message       string    `json:"message"`
	AutoModReason string    `json:"automod_reason"`
}

//Based on https://git-aws.internal.justin.tv/chat/tmi/blob/4e2b02dfb86a76e1090358b2173a70e6d28963aa/clue/logic/privmsg_tracking.go#L144-L164
//and https://git-aws.internal.justin.tv/chat/tmi/blob/86169095f61b7041dbb84beb4dff6275ced8dde6/clue/logic/automod_approve.go#L179-L190
type BitsAutoModMessageEnforcement struct {
	ActionType        AutoModOutcome `json:"action_type"`
	Body              string         `json:"body"`
	ClientID          string         `json:"client_id"`
	CanonicalClientID string         `json:"canonical_client_id"`
	Channel           string         `json:"channel" datascience:"channelname"`
	ChannelID         string         `json:"channel_id" datascience:"channelID"`
	EnforcementReason string         `json:"enforcement_reason"`
	MessageID         string         `json:"msg_id"`
	RoomID            string         `json:"room_id"`
	UserLogin         string         `json:"login" datascience:"username"`
	UserID            string         `json:"user_id" datascience:"userID,distinctID"`
	ServerTime        time.Time      `json:"server_timestamp"`
}

type BitsBadgeTierNotification struct {
	TransactionId  string    `json:"transaction_id"`
	UserId         string    `json:"user_id" datascience:"userID,distinctID"`
	UserLogin      string    `json:"user_login" datascience:"username"`
	ChannelID      string    `json:"channel_id" datascience:"channelID"`
	ChannelLogin   string    `json:"channel_login" datascience:"channelname"`
	ContextDetails string    `json:"context_details"`
	ServerTime     time.Time `json:"-" datascience:"servertime"`
	Action         string    `json:"action"`
	BadgeAmount    int64     `json:"badge_amount"`
	Type           string    `json:"custom"`
	EmoteID        string    `json:"emote_id"`
}

type BitsCheerZeroEvent struct {
	TransactionId string    `json:"transaction_id"`
	UserId        string    `json:"user_id" datascience:"userID,distinctID"`
	ChannelId     string    `json:"channel_id" datascience:"channelID"`
	Message       string    `json:"message"`
	ServerTime    time.Time `json:"server_timestamp"`
}

type BitsGemNotificationCreated struct {
	NotificationID string    `json:"notification_id"`
	Active         bool      `json:"active"`
	Title          string    `json:"title"`
	Description    string    `json:"description"`
	ServerTime     time.Time `json:"server_timestamp"`
}

type BitsGemNotificationCleared struct {
	NotificationID string    `json:"notification_id"`
	UserID         string    `json:"user_id" datascience:"userID,distinctID"`
	ServerTime     time.Time `json:"server_timestamp"`
}

type AutoRefillAction struct {
	ServerTime         time.Time `json:"server_timestamp"`
	UserID             string    `json:"user_id" datascience:"userID,distinctID"`
	Product            string    `json:"product"`
	Action             string    `json:"action"`
	ActionType         string    `json:"action_type"`
	ActionReason       string    `json:"action_reason"`
	Location           string    `json:"location"`
	SKU                string    `json:"product_sku"`
	MinimumThreshold   int       `json:"minimum_threshold"`
	ChargeInstrumentID string    `json:"charge_instrument_id"`
	PaymentProvider    string    `json:"payment_provider"`
	PaymentMethod      string    `json:"payment_method"`
	BitType            string    `json:"bit_type"`
	OfferID            string    `json:"offer_id"`
}

type AutoRefillPurchaseCompleted struct {
	ServerTime    time.Time `json:"server_timestamp"`
	UserID        string    `json:"user_id" datascience:"userID,distinctID"`
	Product       string    `json:"product"`
	TransactionID string    `json:"transaction_id"`
	SKU           string    `json:"product_sku"`
	Quantity      int       `json:"quantity"`
	RequestID     string    `json:"request_id"`
	AttemptNumber int       `json:"attempt_number"`
	BitType       string    `json:"bit_type"`
	OfferID       string    `json:"offer_id"`
}

type AutoRefillPurchaseFailure struct {
	ServerTime    time.Time `json:"server_timestamp"`
	UserID        string    `json:"user_id" datascience:"userID,distinctID"`
	Product       string    `json:"product"`
	TransactionID string    `json:"transaction_id"`
	SKU           string    `json:"product_sku"`
	Quantity      int       `json:"quantity"`
	AttemptNumber int       `json:"attempt_number"`
	FailureType   string    `json:"failure_type"`
	RequestID     string    `json:"request_id"`
	BitType       string    `json:"bit_type"`
	OfferID       string    `json:"offer_id"`
}
