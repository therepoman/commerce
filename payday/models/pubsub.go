package models

// data structures for telling Pubsub about successful transactions
import (
	"encoding/json"
	"time"

	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"

	mako_client "code.justin.tv/commerce/mako/twirp"
	"code.justin.tv/commerce/payday/models/api"
)

const (
	CurrentPrivateMessageVersion          = "1.0"
	BitEventMessageType                   = "bits_event"
	BitBalanceUpdateMessageType           = "balance_update"
	BitBadgeUpdateMessageType             = "badge_update"
	BitBadgeUpdateNotificationMessageType = "badge_update_notification"
	FirstBitsPurchaseMessageType          = "first_bits_purchase"
	ChannelSettingsUpdateMessageType      = "channel_settings_update"
	BitEventsPrivatePubsubTopicV1         = "channel-bits-events-v1"
	BitEventsPrivatePubsubTopicV2         = "channel-bits-events-v2"
	UserBitsUpdatesPubsubMessageTopic     = "user-bits-updates-v1"
	BitsEventPublicPubsubTopic            = "channel-bit-events-public"
	BitsCampaignsPubsubTopic              = "bits-campaigns-v1"
	BitsBadgeUpdateSetID                  = "bits"
	BitsLeaderboardBadgeUpdateSetID       = "bits-leader"
	BitsPublicBadgeTierUnlockShare        = "channel-bits-badge-unlocks"
)

type InnerMessage interface {
	MarshalJSON() ([]byte, error)
}

type PaydayPubsubMessage struct {
	MessageType     string      `json:"type"`
	MessageContents interface{} `json:"msg"`
	Time            time.Time   `json:"time"`
}

type PinTop struct {
	HasTopEvent bool    `json:"has_top_event"`
	Action      string  `json:"action"`
	MessageId   string  `json:"message_id"`
	UserId      *string `json:"user_id"`
	Username    *string `json:"username"`
	Message     *string `json:"message"`
	BitsUsed    *int64  `json:"bits_used"`
}

type PinRecent struct {
	HasRecentEvent      bool    `json:"has_recent_event"`
	Action              string  `json:"action"`
	MessageId           string  `json:"message_id"`
	ChannelId           string  `json:"channel_id"`
	Timestamp           string  `json:"timestamp"`
	UserId              *string `json:"user_id"`
	Username            *string `json:"username"`
	Message             *string `json:"message"`
	BitsUsed            *int64  `json:"bits_used"`
	AllottedTimeMillis  int64   `json:"allotted_time_ms"`
	TimeRemainingMillis int64   `json:"time_remaining_ms"`
}

type PubsubPinMessage struct {
	Top                *PinTop    `json:"top"`
	Recent             *PinRecent `json:"recent"`
	Action             string     `json:"action"`
	ChannelId          string     `json:"channel_id"`
	Timestamp          string     `json:"timestamp"`
	UserId             *string    `json:"user_id"`
	Username           *string    `json:"username"`
	Message            *string    `json:"message"`
	BitsUsed           *int64     `json:"bits_used"`
	AllottedTimeMillis int64      `json:"allotted_time_ms"`
}

type PubsubTransactionMessage struct {
	UserName      string    `json:"user_name"`
	ChannelName   string    `json:"channel_name"`
	UserId        string    `json:"user_id"`
	ChannelId     string    `json:"channel_id"`
	Time          time.Time `json:"time"`
	ChatMessage   string    `json:"chat_message"`
	BitsUsed      int       `json:"bits_used"`
	TotalBitsUsed int       `json:"total_bits_used"`
	Context       string    `json:"context"`
}

type PubSubBitsOnExtensionsMessage struct {
	TransactionId      string    `json:"transaction_id"`
	Time               time.Time `json:"time"`
	SKU                string    `json:"sku"`
	UserId             string    `json:"user_id"`
	DisplayName        string    `json:"display_name"`
	ChannelId          string    `json:"channel_id"`
	TransactionReceipt string    `json:"transactionReceipt"`
}

type BadgeEntitlement struct {
	NewVersion      int64 `json:"new_version"`
	PreviousVersion int64 `json:"previous_version"`
}

type BadgeTierEntitlement struct {
	Badge     BadgeEntitlement
	Emoticons []mako_client.Emoticon
}

type PubsubTransactionMessageV1 struct {
	UserName             string                `json:"user_name"`
	ChannelName          string                `json:"channel_name"`
	UserId               string                `json:"user_id"`
	ChannelId            string                `json:"channel_id"`
	Time                 time.Time             `json:"time"`
	ChatMessage          string                `json:"chat_message"`
	BitsUsed             int                   `json:"bits_used"`
	TotalBitsUsed        int                   `json:"total_bits_used"`
	Context              string                `json:"context"`
	BadgeEntitlement     *BadgeEntitlement     `json:"badge_entitlement"`
	BadgeTierEntitlement *BadgeTierEntitlement `json:"badge_tier_entitlement,omitempty"` //TODO: remove omitempty closer to launch
}

func (this *PubsubTransactionMessageV1) ConvertToV2(isAnonymous bool) PubsubTransactionMessageV2 {
	var userNamePtr, userIdPtr *string
	if isAnonymous {
		userNamePtr = nil
		userIdPtr = nil
	} else {
		userNamePtr = &this.UserName
		userIdPtr = &this.UserId
	}
	return PubsubTransactionMessageV2{
		UserName:         userNamePtr,
		UserId:           userIdPtr,
		IsAnonymous:      isAnonymous,
		ChannelName:      this.ChannelName,
		ChannelId:        this.ChannelId,
		Time:             this.Time,
		ChatMessage:      this.ChatMessage,
		BitsUsed:         this.BitsUsed,
		TotalBitsUsed:    this.TotalBitsUsed,
		Context:          this.Context,
		BadgeEntitlement: this.BadgeEntitlement,
	}
}

type PubsubTransactionMessageV2 struct {
	UserName         *string           `json:"user_name"`
	ChannelName      string            `json:"channel_name"`
	UserId           *string           `json:"user_id"`
	ChannelId        string            `json:"channel_id"`
	Time             time.Time         `json:"time"`
	ChatMessage      string            `json:"chat_message"`
	BitsUsed         int               `json:"bits_used"`
	TotalBitsUsed    int               `json:"total_bits_used"`
	IsAnonymous      bool              `json:"is_anonymous"`
	Context          string            `json:"context"`
	BadgeEntitlement *BadgeEntitlement `json:"badge_entitlement"`
}

type PrivateBitsMessage struct {
	Data        interface{} `json:"data"`
	Version     string      `json:"version"`
	MessageType string      `json:"message_type"`
	MessageId   string      `json:"message_id"`
}

type ChannelSettingsUpdatePubsubMessage struct {
	Type    string                       `json:"type"`
	Updates ChannelSettingsUpdateMessage `json:"updates"`
}

type ChannelSettingsUpdateMessage struct {
	LeaderboardEnabled          *bool                            `json:"leaderboard_enabled,omitempty"`
	IsCheerLeaderboardEnabled   *bool                            `json:"is_cheer_leaderboard_enabled,omitempty"`
	IsSubGiftLeaderboardEnabled *bool                            `json:"is_sub_gift_leaderboard_enabled,omitempty"`
	DefaultLeaderboard          *string                          `json:"default_leaderboard,omitempty"`
	LeaderboardTimePeriod       *string                          `json:"leaderboard_time_period,omitempty"`
	Leaderboard                 *[]*pantheonrpc.LeaderboardEntry `json:"leaderboard,omitempty"`
	CheerBombEventOptOut        *bool                            `json:"cheer_bomb_event_opt_out,omitempty"`
	MinimumBits                 *int64                           `json:"minimum_bits,omitempty"`
	MinimumBitsEmote            *int64                           `json:"minimum_bits_emote,omitempty"`
	OptedOutOfVoldemort         *bool                            `json:"opted_out_of_voldemort,omitempty"`
}

type SponsoredCheermotePubsubMessageType string

const (
	AddSponsoredCheermote              = SponsoredCheermotePubsubMessageType("sponsored-cheermote-add")
	RemoveSponsoredCheermote           = SponsoredCheermotePubsubMessageType("sponsored-cheermote-remove")
	UpdateSponsoredCheermoteUsed       = SponsoredCheermotePubsubMessageType("sponsored-cheermote-update-used")
	SponsoredCheermoteCampaignComplete = SponsoredCheermotePubsubMessageType("sponsored-cheermote-campaign-complete")
)

type SponsoredCheermotesUpdateMessage struct {
	Type       SponsoredCheermotePubsubMessageType `json:"type"`
	CampaignID string                              `json:"campaign_id"`
	Action     *api.Action                         `json:"action,omitempty"`      // Deprecated. Once all clients have migrated we can remove
	Template   string                              `json:"template,omitempty"`    // used when adding an Cheer
	Cheer      *Cheer                              `json:"cheer,omitempty"`       // used when adding an Cheer
	UsedAmount *int64                              `json:"used_amount,omitempty"` // used when updating the sponsored total
}

type BitsBadgeTierEmoteUpdatePubsubMessageType string

const (
	BitsBadgeTierEmotesPubsubTopicScope = "bits-badge-tier-emotes"
	BitsBadgeTierEmoteCreate            = BitsBadgeTierEmoteUpdatePubsubMessageType("bits-badge-tier-emote-create")
)

type BitsBadgeTierEmoteUpdateMessage struct {
	Type BitsBadgeTierEmoteUpdatePubsubMessageType `json:"type"`
	Data BitsBadgeTierEmoteUpdatePubsubData        `json:"data"`
}

type BitsBadgeTierEmoteUpdatePubsubData struct {
	Threshold int64      `json:"threshold"`
	Emoticons []Emoticon `json:"emoticons"`
}

type Emoticon struct {
	ID    string `json:"id"`
	Token string `json:"token"` // Emote Token is equivalent to emote Code
	State string `json:"state"`
}

type PrivateBitsMessageUnmarshaled struct {
	Data        json.RawMessage `json:"data"`
	Version     string          `json:"version"`
	MessageType string          `json:"message_type"`
	MessageId   string          `json:"message_id"`
}

type PubSubSubscriptionData struct {
	Topics    []string
	AuthToken *string `json:"auth_token"`
}

type PubSubRequest struct {
	Type  string
	Nonce string
	Data  *PubSubSubscriptionData
}

type PubSubResponseData struct {
	Topic   string
	Message string
}

type PubSubResponse struct {
	Type  string
	Nonce *string
	Data  *PubSubResponseData
	Error *string
}

type BalanceUpdates struct {
	UserId       string        `json:"user_id"`
	Balance      int64         `json:"balance"`
	ChannelTotal *ChannelTotal `json:"channel_total"`
}

type BadgeUpdate struct {
	UserID        string `json:"user_id"`
	ChannelID     string `json:"channel_id"`
	NewestVersion string `json:"newest_version"` //empty string tells the front end to revoke the badge
	SetID         string `json:"set_id"`
}

type BadgeUpdateNotification struct {
	UserID         string `json:"user_id"`
	ChannelID      string `json:"channel_id"`
	Threshold      int64  `json:"threshold"`
	NotificationID string `json:"notification_id"`
	CanShare       bool   `json:"can_share"`
}

type PublicBadgeTierUnlockShare struct {
	UserID      string    `json:"user_id"`
	UserName    string    `json:"user_name"`
	ChannelID   string    `json:"channel_id"`
	ChannelName string    `json:"channel_name"`
	Threshold   int64     `json:"badge_tier"`
	ChatMessage string    `json:"chat_message"`
	Time        time.Time `json:"time"`
}

type ChannelTotal struct {
	ChannelId    string `json:"channel_id"`
	ChannelTotal int64  `json:"total"`
}

type UserFirstPurchase struct {
	UserID       string `json:"user_id"`
	BitsAcquired int64  `json:"bits_acquired"`
}

// TODO: look into using aliasing via this technique instead http://choly.ca/post/go-json-marshalling/

func marshalJSON(obj interface{}) ([]byte, error) {
	marshalled, err := json.Marshal(obj)
	if err != nil {
		return nil, err
	}
	return json.Marshal(string(marshalled))
}

func (this *PubsubTransactionMessage) MarshalJSON() ([]byte, error) {
	var t interface{} = *this
	asserted := t.(interface{})
	return marshalJSON(asserted)
}
