package models

import (
	"time"
)

type BroadcasterRevenueRecord struct {
	Date  time.Time `json:"date"`
	Bits  int64     `json:"bits"`
	Cents int64     `json:"cents"`
}

type GetBroadcasterRevenueResponse struct {
	Records []BroadcasterRevenueRecord `json:"records"`
}
