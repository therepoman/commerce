package models

import (
	"encoding/json"
	"errors"
	"sort"
	"strconv"
	"time"

	"code.justin.tv/commerce/payday/dynamo/holds"
)

// data structures for SNS-wrapped SQS messages

type MessageType uint

const (
	GiveBitsToBroadcaster MessageType = iota
	AddBitsToCustomerAccount
	RemoveBitsFromCustomerAccount
	UseBitsOnExtension
	UseBitsOnPoll
	CreateHold
	FinalizeHold
	ReleaseHold
	GiveBitsToBroadcasterChallenge
	UseBitsOnExtensionChallenge
)

const (
	MILLISECONDS_PER_SECOND     int64 = 1000
	NANOSECONDS_PER_MILLISECOND int64 = 1000000
)

func (mt MessageType) Name() string {
	switch mt {
	case GiveBitsToBroadcaster:
		return GiveBitsToBroadcasterTransactionType
	case AddBitsToCustomerAccount:
		return AddBitsToCustomerAccountTransactionType
	case RemoveBitsFromCustomerAccount:
		return RemoveBitsFromCustomerAccountTransactionType
	case UseBitsOnExtension:
		return UseBitsOnExtensionTransactionType
	case UseBitsOnPoll:
		return UseBitsOnPollTransactionType
	case CreateHold:
		return CreateHoldTransactionType
	case FinalizeHold:
		return FinalizeHoldTransactionType
	case ReleaseHold:
		return ReleaseHoldTransactionType
	case GiveBitsToBroadcasterChallenge:
		return GiveBitsToBroadcasterChallengeTransactionType
	case UseBitsOnExtensionChallenge:
		return UseBitsOnExtensionChallengeTransactionType
	default:
		return "InvalidMessageType"
	}
}

type SNSMessage struct {
	Type              string                         `json:"Type"`
	MessageId         string                         `json:"MessageId"`
	Token             string                         `json:"Token,optional"` // Only for subscribe and unsubscribe
	TopicArn          string                         `json:"TopicArn"`
	Subject           string                         `json:"Subject,optional"`      // Only for Notification
	Message           string                         `json:"Message"`               // contains a JSON SQSMessage
	SubscribeURL      string                         `json:"SubscribeURL,optional"` // Only for subscribe and unsubscribe
	Timestamp         time.Time                      `json:"Timestamp"`
	SignatureVersion  string                         `json:"SignatureVersion"`
	Signature         string                         `json:"Signature"`
	SigningCertURL    string                         `json:"SigningCertURL"`
	UnsubscribeURL    string                         `json:"UnsubscribeURL,optional"` // Only for notifications
	MessageAttributes map[string]SNSMessageAttribute `json:"MessageAttributes,optional"`
}

type SNSMessageAttribute struct {
	Type  string `json:"Type"`
	Value string `json:"Value"`
}

type EncryptedNotification struct {
	InitializationVector string `json:"iv"`
	Payload              string `json:"payload"`
	Key                  string `json:"key"` // base-64 encoded, KMS-encrypted AES key
}

type SQSMessage interface {
	MessageType() MessageType
	IsPachinkoMessage() bool
}

type GiveBitsToBroadcasterMessage struct {
	TransactionId                         string         `json:"transactionId"`
	TransactionType                       string         `json:"transactionType"`
	RequestingTwitchUserId                string         `json:"requestingTwitchUserId"`
	TargetTwitchUserId                    string         `json:"targetTwitchUserId"`
	SourceStatus                          string         `json:"giveBitsSourceChannelStatus"`
	PublicMessage                         string         `json:"giveBitsPublicMessage"`
	BitsSent                              int            `json:"totalBits"`
	AccountTypeToBitAmount                map[string]int `json:"accountTypeToBitAmount"`
	TotalBitsToBroadcaster                int            `json:"totalBitsGivenFromUserToBroadcaster"`
	TotalBitsAfterTransaction             int            `json:"totalBitsAfterTransaction"`
	EventReasonCode                       string         `json:"eventTypeId"`
	TimeOfEvent                           time.Time      `json:"timeOfEvent"`
	ExtraContext                          string         `json:"extraContext"`
	SponsoredAmounts                      map[string]int `json:"sponsoredAmounts"`
	IsAnonymous                           bool           `json:"anonymous"`
	AccountTypeSpendOrderPriorityValueMap map[string]int `json:"accountTypeSpendOrderPriorityValueMap,omitempty"`
	IsPachinko                            bool           `json:"isPachinko,omitempty"`
	HoldId                                string         `json:"holdId"`
}

type AddBitsToCustomerAccountMessage struct {
	TransactionId                         string         `json:"transactionId"`
	TransactionType                       string         `json:"transactionType"`
	TargetTwitchUserId                    string         `json:"targetTwitchUserId"`
	TotalBits                             int            `json:"totalBits"`
	TotalBitsAfterTransaction             int            `json:"totalBitsAfterTransaction"`
	AccountTypeToBitAmount                map[string]int `json:"accountTypeToBitAmount"`
	AdminID                               string         `json:"adminId"`
	AdminReason                           string         `json:"adminReason"`
	EventReasonCode                       string         `json:"eventTypeId"`
	TimeOfEvent                           time.Time      `json:"timeOfEvent"`
	ExtraContext                          string         `json:"extraContext"`
	PurchasePrice                         string         `json:"purchasePrice"`
	PurchasePriceCurrency                 string         `json:"purchasePriceCurrency"`
	PurchaseMarketplace                   string         `json:"purchaseMarketplace"`
	AccountTypeSpendOrderPriorityValueMap map[string]int `json:"accountTypeSpendOrderPriorityValueMap,omitempty"`
	SkuQuantity                           int            `json:"skuQuantity"`
	IsPachinko                            bool           `json:"isPachinko,omitempty"`
}

type RemoveBitsFromCustomerAccountMessage struct {
	TransactionId                         string         `json:"transactionId"`
	TransactionType                       string         `json:"transactionType"`
	TargetTwitchUserId                    string         `json:"targetTwitchUserId"`
	TotalBits                             int            `json:"totalBits"`
	TotalBitsAfterTransaction             int            `json:"totalBitsAfterTransaction"`
	ProcessedBits                         int            `json:"processedBits"`
	AccountTypeToBitAmount                map[string]int `json:"accountTypeToBitAmount"`
	AdminID                               string         `json:"adminId"`
	AdminReason                           string         `json:"adminReason"`
	EventReasonCode                       string         `json:"eventTypeId"`
	TimeOfEvent                           time.Time      `json:"timeOfEvent"`
	ExtraContext                          string         `json:"extraContext"`
	AccountTypeSpendOrderPriorityValueMap map[string]int `json:"accountTypeSpendOrderPriorityValueMap,omitempty"`
	IsPachinko                            bool           `json:"isPachinko,omitempty"`
}

type UseBitsOnExtensionMessage struct {
	TransactionId                         string                                `json:"transactionId"`
	TransactionType                       string                                `json:"transactionType"`
	RequestingTwitchUserId                string                                `json:"requestingTwitchUserId"`
	TargetTwitchUserId                    string                                `json:"targetTwitchUserId"`
	TimeOfEvent                           time.Time                             `json:"timeOfEvent"`
	ExtensionID                           string                                `json:"extensionId"`
	SKU                                   string                                `json:"SKU"`
	BitsProccessed                        int                                   `json:"totalBits"`
	AccountTypeToBitAmount                map[string]int                        `json:"accountTypeToBitAmount"`
	TotalBitsAfterTransaction             int                                   `json:"totalBitsAfterTransaction"`
	ExtraContext                          string                                `json:"extraContext"`
	TotalBitsToBroadcaster                int                                   `json:"totalBitsGivenFromUserToBroadcaster"`
	AccountTypeSpendOrderPriorityValueMap map[string]int                        `json:"accountTypeSpendOrderPriorityValueMap,omitempty"`
	IsPachinko                            bool                                  `json:"isPachinko,omitempty"`
	IgnorePayout                          bool                                  `json:"ignorePayout,omitempty"`
	UsageOverrideList                     map[string]holds.BitsUsageAttribution `json:"bitsUsageAttribution,omitempty"`
	HoldId                                string                                `json:"holdId"`
}

type UseBitsOnPollMessage struct {
	TransactionId                         string         `json:"transactionId"`
	TransactionType                       string         `json:"transactionType"`
	RequestingTwitchUserId                string         `json:"requestingTwitchUserId"`
	TargetTwitchUserId                    string         `json:"targetTwitchUserId"`
	TimeOfEvent                           time.Time      `json:"timeOfEvent"`
	BitsProccessed                        int            `json:"totalBits"`
	AccountTypeToBitAmount                map[string]int `json:"accountTypeToBitAmount"`
	TotalBitsAfterTransaction             int            `json:"totalBitsAfterTransaction"`
	ExtraContext                          string         `json:"extraContext"`
	TotalBitsToBroadcaster                int            `json:"totalBitsGivenFromUserToBroadcaster"`
	AccountTypeSpendOrderPriorityValueMap map[string]int `json:"accountTypeSpendOrderPriorityValueMap,omitempty"`
	PollId                                string         `json:"pollId"`
	PollChoiceId                          string         `json:"pollChoiceId"`
	IsPachinko                            bool           `json:"isPachinko,omitempty"`
}

type CreateHoldMessage struct {
	TransactionId          string    `json:"transactionId"`
	TransactionType        string    `json:"transactionType"`
	TimeOfEvent            time.Time `json:"timeOfEvent"`
	IsPachinko             bool      `json:"isPachinko,omitempty"`
	BitsProcessed          int       `json:"bitsProcessed"`
	RequestingTwitchUserId string    `json:"requestingTwitchUserId"`
}

type FinalizeHoldMessage struct {
	TransactionId          string                                `json:"transactionId"`
	TransactionType        string                                `json:"transactionType"`
	RequestingTwitchUserId string                                `json:"requestingTwitchUserId"`
	TimeOfEvent            time.Time                             `json:"timeOfEvent"`
	IsPachinko             bool                                  `json:"isPachinko,omitempty"`
	Recipients             map[string]holds.BitsUsageAttribution `json:"recipients"`
}

type ReleaseHoldMessage struct {
	TransactionId          string    `json:"transactionId"`
	TransactionType        string    `json:"transactionType"`
	TimeOfEvent            time.Time `json:"timeOfEvent"`
	IsPachinko             bool      `json:"isPachinko,omitempty"`
	BitsReturned           int       `json:"bitsReturned"`
	RequestingTwitchUserId string    `json:"requestingTwitchUserId"`
	HoldId                 string    `json:"holdId"`
}

type ChannelSettingsMessage struct {
	ChannelId                   string  `json:"channel_id"`
	LeaderboardEnabled          *bool   `json:"leaderboard_enabled,omitempty"`
	IsCheerLeaderboardEnabled   *bool   `json:"is_cheer_leaderboard_enabled,omitempty"`
	IsSubGiftLeaderboardEnabled *bool   `json:"is_sub_gift_leaderboard_enabled,omitempty"`
	DefaultLeaderboard          *string `json:"default_leaderboard,omitempty"`
	LeaderboardTimePeriod       *string `json:"leaderboard_time_period,omitempty"`
	CheerBombEventOptOut        *bool   `json:"cheer_bomb_event_opt_out,omitempty"`
	MinimumBits                 *int64  `json:"minimum_bits,omitempty"`
	MinimumBitsEmote            *int64  `json:"minimum_bits_emote,omitempty"`
	OptedOutOfVoldemort         *bool   `json:"opted_out_of_voldemort"`
}

func (*GiveBitsToBroadcasterMessage) MessageType() MessageType {
	return GiveBitsToBroadcaster
}

func (m *GiveBitsToBroadcasterMessage) IsPachinkoMessage() bool {
	return m.IsPachinko
}

func (*AddBitsToCustomerAccountMessage) MessageType() MessageType {
	return AddBitsToCustomerAccount
}

func (m *AddBitsToCustomerAccountMessage) IsPachinkoMessage() bool {
	return m.IsPachinko
}

func (*RemoveBitsFromCustomerAccountMessage) MessageType() MessageType {
	return RemoveBitsFromCustomerAccount
}

func (m *RemoveBitsFromCustomerAccountMessage) IsPachinkoMessage() bool {
	return m.IsPachinko
}

func (*UseBitsOnExtensionMessage) MessageType() MessageType {
	return UseBitsOnExtension
}

func (m *UseBitsOnExtensionMessage) IsPachinkoMessage() bool {
	return m.IsPachinko
}

func (*UseBitsOnPollMessage) MessageType() MessageType {
	return UseBitsOnPoll
}

func (m *UseBitsOnPollMessage) IsPachinkoMessage() bool {
	return m.IsPachinko
}

func (*CreateHoldMessage) MessageType() MessageType {
	return CreateHold
}

func (m *CreateHoldMessage) IsPachinkoMessage() bool {
	return m.IsPachinko
}

func (*FinalizeHoldMessage) MessageType() MessageType {
	return FinalizeHold
}

func (m *FinalizeHoldMessage) IsPachinkoMessage() bool {
	return m.IsPachinko
}

func (*ReleaseHoldMessage) MessageType() MessageType {
	return ReleaseHold
}

func (m *ReleaseHoldMessage) IsPachinkoMessage() bool {
	return m.IsPachinko
}

func (this *GiveBitsToBroadcasterMessage) UnmarshalJSON(b []byte) error {
	var err error
	type alias GiveBitsToBroadcasterMessage

	aliased := &struct {
		*alias
		TimeOfEvent string `json:"timeOfEvent"`
	}{
		alias: (*alias)(this),
	}

	err = json.Unmarshal(b, &aliased)
	if err != nil {
		return err
	}

	this.TimeOfEvent, err = convertTime(aliased.TimeOfEvent)

	return err
}

func (this *AddBitsToCustomerAccountMessage) UnmarshalJSON(b []byte) error {
	var err error
	type alias AddBitsToCustomerAccountMessage

	aliased := &struct {
		*alias
		TimeOfEvent string `json:"timeOfEvent"`
	}{
		alias: (*alias)(this),
	}

	err = json.Unmarshal(b, &aliased)
	if err != nil {
		return err
	}

	this.TimeOfEvent, err = convertTime(aliased.TimeOfEvent)

	return err
}

func (this *RemoveBitsFromCustomerAccountMessage) UnmarshalJSON(b []byte) error {
	var err error
	type alias RemoveBitsFromCustomerAccountMessage

	aliased := &struct {
		*alias
		TimeOfEvent string `json:"timeOfEvent"`
	}{
		alias: (*alias)(this),
	}

	err = json.Unmarshal(b, &aliased)
	if err != nil {
		return err
	}

	this.TimeOfEvent, err = convertTime(aliased.TimeOfEvent)

	return err
}

func (this *UseBitsOnExtensionMessage) UnmarshalJSON(b []byte) error {
	var err error
	type alias UseBitsOnExtensionMessage

	aliased := &struct {
		*alias
		TimeOfEvent string `json:"timeOfEvent"`
	}{
		alias: (*alias)(this),
	}

	err = json.Unmarshal(b, &aliased)
	if err != nil {
		return err
	}

	this.TimeOfEvent, err = convertTime(aliased.TimeOfEvent)

	return err
}

func (this *UseBitsOnPollMessage) UnmarshalJSON(b []byte) error {
	var err error
	type alias UseBitsOnPollMessage

	aliased := &struct {
		*alias
		TimeOfEvent string `json:"timeOfEvent"`
	}{
		alias: (*alias)(this),
	}

	err = json.Unmarshal(b, &aliased)
	if err != nil {
		return err
	}

	this.TimeOfEvent, err = convertTime(aliased.TimeOfEvent)

	return err
}

func convertTime(timeStr string) (time.Time, error) {
	t, err := time.Parse(time.RFC3339, timeStr)
	if err == nil {
		return t, nil
	}

	timeMillis, err := strconv.ParseInt(timeStr, 10, 64)
	if err != nil {
		return time.Time{}, err
	}

	var timeSeconds int64 = timeMillis / MILLISECONDS_PER_SECOND
	var timeRemainderNanos int64 = (timeMillis % MILLISECONDS_PER_SECOND) * NANOSECONDS_PER_MILLISECOND

	return time.Unix(timeSeconds, timeRemainderNanos), nil
}

type SQSMessageJSON struct {
	SQSMessage
}

func (t *SQSMessageJSON) UnmarshalJSON(b []byte) (err error) {
	var o struct {
		TransactionType string `json:"transactionType"`
	}

	err = json.Unmarshal(b, &o)
	if err != nil {
		return err
	}

	switch o.TransactionType {
	case GiveBitsToBroadcasterTransactionType, GiveBitsToBroadcasterChallengeTransactionType:
		t.SQSMessage = new(GiveBitsToBroadcasterMessage)
		err = json.Unmarshal(b, t.SQSMessage)
	case AdminAddBitsTransactionType, AddBitsToCustomerAccountTransactionType:
		t.SQSMessage = new(AddBitsToCustomerAccountMessage)
		err = json.Unmarshal(b, t.SQSMessage)
	case AdminRemoveBitsTransactionType, RemoveBitsFromCustomerAccountTransactionType:
		t.SQSMessage = new(RemoveBitsFromCustomerAccountMessage)
		err = json.Unmarshal(b, t.SQSMessage)
	case UseBitsOnExtensionTransactionType, UseBitsOnExtensionChallengeTransactionType:
		t.SQSMessage = new(UseBitsOnExtensionMessage)
		err = json.Unmarshal(b, t.SQSMessage)
	case UseBitsOnPollTransactionType:
		t.SQSMessage = new(UseBitsOnPollMessage)
		err = json.Unmarshal(b, t.SQSMessage)
	case CreateHoldTransactionType:
		t.SQSMessage = new(CreateHoldMessage)
		err = json.Unmarshal(b, t.SQSMessage)
	case FinalizeHoldTransactionType:
		t.SQSMessage = new(FinalizeHoldMessage)
		err = json.Unmarshal(b, t.SQSMessage)
	case ReleaseHoldTransactionType:
		t.SQSMessage = new(ReleaseHoldMessage)
		err = json.Unmarshal(b, t.SQSMessage)
	default:
		return errors.New("Could not determine matching transactionType for unmarshaling: " + o.TransactionType)
	}

	return err
}

// This function gathers the users that need to have their Bits processed for things like Leaderboards/Emotes/Badges
// and processes them using an input helper function. This function is expected to pass in a processing function that performs the work for each user.
func (msg *UseBitsOnExtensionMessage) HandleBitsRewardAttributions(processingFunction func(broadcasterID string, numberOfBits int, totalNumberOfBitsUsedWithBroadcaster int) error) error {
	if len(msg.UsageOverrideList) > 0 {
		var bitsUsageList []holds.BitsUsageAttribution
		for broadcasterId, bitsUsageAttribution := range msg.UsageOverrideList {
			// Ensure this was set
			bitsUsageAttribution.ID = broadcasterId
			bitsUsageList = append(bitsUsageList, bitsUsageAttribution)
		}

		// Golang randomly iterates through maps, so slices generated from indexing through a Map are in a random order.
		// Sort these to keep them in a consistent order, for unit tests and other things
		sort.Slice(bitsUsageList, func(i, j int) bool { return bitsUsageList[i].ID < bitsUsageList[j].ID })
		for _, bitsUsageAttribution := range bitsUsageList {
			totalBitsToBroadcaster := bitsUsageAttribution.TotalWithBroadcasterPrior + bitsUsageAttribution.RewardAttribution

			err := processingFunction(bitsUsageAttribution.ID, int(bitsUsageAttribution.RewardAttribution), int(totalBitsToBroadcaster))
			if err != nil {
				return err
			}
		}
		return nil
	} else {
		return processingFunction(msg.TargetTwitchUserId, msg.BitsProccessed, msg.TotalBitsToBroadcaster)
	}
}

// This function gathers the users that need to have their Bits processed for things like Pachter/Gringotts Revenue Attribution, Dashboards & Payout
// and processes them using an input helper function. This function is expected to pass in a processing function that performs the work for each user.
func (msg *UseBitsOnExtensionMessage) HandleBitsRevenueAttributions(processingFunction func(broadcasterID string, numberOfBits int) error) error {
	if msg.IgnorePayout {
		return nil
	}

	if len(msg.UsageOverrideList) > 0 {
		var bitsUsageList []holds.BitsUsageAttribution
		for broadcasterId, bitsUsageAttribution := range msg.UsageOverrideList {
			// Ensure this was set
			bitsUsageAttribution.ID = broadcasterId
			bitsUsageList = append(bitsUsageList, bitsUsageAttribution)
		}

		// Golang randomly iterates through maps, so slices generated from indexing through a Map are in a random order.
		// Sort these to keep them in a consistent order, for unit tests and other things
		sort.Slice(bitsUsageList, func(i, j int) bool { return bitsUsageList[i].ID < bitsUsageList[j].ID })
		for _, bitsUsageAttribution := range bitsUsageList {
			err := processingFunction(bitsUsageAttribution.ID, int(bitsUsageAttribution.BitsRevenueAttributionToBroadcaster))
			if err != nil {
				return err
			}
		}
		return nil
	} else {
		return processingFunction(msg.TargetTwitchUserId, msg.BitsProccessed)
	}
}
