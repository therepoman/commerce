#!/bin/bash -e

# Enable Globbing
shopt -s globstar extglob

# Flags
version=$1

# User Config
s3bucket="payday-$ENVIRONMENT-lambdas"
region="us-west-2"
lambdadir=./lambdas

version="${version//\/}"

deploy(){
    # s3 path must match lambda name
    namewithenv=${name}_${ENVIRONMENT}
    # terracode prefixes default- to the function name
    functionname=default-${namewithenv}

   aws lambda --region us-west-2 update-function-code \
    --function-name $functionname \
    --s3-bucket $s3bucket \
    --s3-key $namewithenv/$version/$namewithenv.zip \
    --publish
}

deployall() {
    for mainfile in $lambdadir/**/main.go; do
        entrypoint=$(dirname $mainfile)
        path=$(dirname $entrypoint)
        name=$(basename $path)

        echo "-> [$name] deploying lambda..."
        deploy $name
    done
}

deployall