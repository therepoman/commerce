#!/bin/bash -e

# Enable Globbing
shopt -s globstar extglob

# Flags
s3bucket=$1
version=$2

# User Config
builddir=./builds
lambdadir=./lambdas

push() {
    name=$1
    # s3 path must match lambda name
    namewithenv=${name}_${ENVIRONMENT}
    s3path=$namewithenv/$version

    echo "-> [$name] pushing to s3 for version '$version'...."

    aws s3 cp $builddir/$name.zip s3://$s3bucket/$s3path/$namewithenv.zip
    aws s3 cp --content-type text/plain $builddir/$name.hash s3://$s3bucket/$s3path/hash
}

pushall() {
    for mainfile in $lambdadir/**/main.go; do
        entrypoint=$(dirname $mainfile)
        path=$(dirname $entrypoint)
        name=$(basename $path)

        echo "-> [$name] pushing lambda..."
        push $name
    done
}

pushall