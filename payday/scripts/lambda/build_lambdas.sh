#!/bin/bash -e

# Enable Globbing
shopt -s globstar extglob

# Config
builddir=./builds
lambdadir=./lambdas
configdir=config
arch=linux

clean() {
  echo "-> Cleaning build directory"
  rm -rf $builddir
}

build() {
    path=$1
    name=$2
    bin=$builddir/$name/main
    currentdir=`pwd`

    GOOS=$arch go build -o $bin $path/entrypoint/main.go
    mkdir -p $builddir/$name/$configdir
    cp -R $currentdir/$configdir $builddir/$name
    cd $(dirname $bin) && zip -r ../$name.zip * > /dev/null && cd $currentdir

    openssl dgst -sha256 -binary $builddir/$name.zip | openssl enc -base64 > $builddir/$name.hash

    hash=$(cat $builddir/$name.hash)

    echo "-> [$name] computed hash: $hash"
}

checkdup() {
    declare -A hashmap
    for mainfile in $lambdadir/**/main.go; do
        entrypoint=$(dirname $mainfile)
        path=$(dirname $entrypoint)
        name=$(basename $path)

        if [ "${hashmap[$name]}" ]; then
            echo "Duplicate lambda name found: $name. Exiting..."
            exit 1
        fi

        hashmap[$name]=1
    done
}

buildall() {
    for mainfile in $lambdadir/**/main.go; do
        entrypoint=$(dirname $mainfile)
        path=$(dirname $entrypoint)
        name=$(basename $path)

        echo "-> [$name] building lambda..."
        build $path $name
    done
}

clean
checkdup
buildall