#!/usr/bin/env bash

USERNAME=$(whoami)
aws ec2 describe-instances --filters "Name=tag:Service, Values=commerce/payday" "Name=tag:Environment, Values=staging"  | grep -E "ip-\d+-\d+-\d+-\d+\.us-west-2.compute.internal" -o | sort | uniq > hosts
pssh --user $USERNAME -P -v -h hosts -i -A -O StrictHostKeyChecking=no 'tail -f /var/log/jtv/payday.log'
