# scripts/vagrant.sh
apt-get update
apt-get install -y manta zsh unzip git mercurial vim make openjdk-7-jre lxc-docker mockery godep

git clone --depth=1 git://github.com/robbyrussell/oh-my-zsh.git /home/vagrant/.oh-my-zsh
cp /home/vagrant/.oh-my-zsh/templates/zshrc.zsh-template /home/vagrant/.zshrc
chown -R vagrant:vagrant /home/vagrant
chsh vagrant -s /usr/bin/zsh

wget -t 20 --retry-connrefused --waitretry=1 -q https://storage.googleapis.com/golang/go1.7.4.linux-amd64.tar.gz
echo "2e5baf03d1590e048c84d1d5b4b6f2540efaaea1  go1.7.4.linux-amd64.tar.gz" | sha1sum --check
tar -C /usr/local -xzf go1.7.4.linux-amd64.tar.gz

echo '
export PATH="/usr/local/go/bin:/go/bin/:$PATH"
export GOPATH="/go"
' >> /home/vagrant/.zshrc

source /home/vagrant/.zshrc

go get github.com/golang/lint/golint
go get golang.org/x/tools/cmd/goimports
go get github.com/vektra/mockery/.../
go get github.com/tools/godep

chown -R vagrant:vagrant /go
