package cloudfront

import (
	"context"
	"strconv"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/client"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/cloudfront"
	"github.com/pkg/errors"
)

const (
	defaultRegion = "us-west-2"
)

type ICloudFrontClient interface {
	InvalidateCache(ctx context.Context, paths []string, baseCallerReference string) error
}

type Client struct {
	cf             *cloudfront.CloudFront
	distributionId string
}

func NewDefaultConfig() *aws.Config {
	return &aws.Config{
		Region: aws.String(defaultRegion),
	}
}

func New(provider client.ConfigProvider, config *aws.Config, distributionId string) ICloudFrontClient {
	return &Client{
		cf:             cloudfront.New(provider, config),
		distributionId: distributionId,
	}
}

func NewFromConfig(s3Config *aws.Config, distributionId string) ICloudFrontClient {
	sess, _ := session.NewSession()
	return New(sess, s3Config, distributionId)
}

func NewFromDefaultConfig(distributionId string) ICloudFrontClient {
	return NewFromConfig(NewDefaultConfig(), distributionId)
}

func (c *Client) InvalidateCache(ctx context.Context, paths []string, baseCallerReference string) error {
	if c.distributionId == "" {
		// no-op if no distribution ID is given (for staging)
		return nil
	}

	var items []*string
	for _, path := range paths {
		items = append(items, aws.String(path))
	}

	input := &cloudfront.CreateInvalidationInput{
		DistributionId: aws.String(c.distributionId),
		InvalidationBatch: &cloudfront.InvalidationBatch{
			// This is used for idempotency. We always want to clear the cache, so combine the base reference and current timestamp
			CallerReference: aws.String(baseCallerReference + strconv.FormatInt(time.Now().UnixNano(), 10)),
			Paths: &cloudfront.Paths{
				Items:    items,
				Quantity: aws.Int64(int64(len(items))),
			},
		},
	}

	_, err := c.cf.CreateInvalidationWithContext(ctx, input)
	if err != nil {
		return errors.Wrap(err, "failed to invalidate cloudfront cache")
	}
	return nil
}
