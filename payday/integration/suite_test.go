// +build integration

package integration

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"testing"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/audit"
	"code.justin.tv/commerce/payday/backend/mux"
	"code.justin.tv/commerce/payday/backend/sandstorm"
	visageClient "code.justin.tv/commerce/payday/client"
	"code.justin.tv/commerce/payday/config"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"code.justin.tv/commerce/payday/s3"
	prism_tenant "code.justin.tv/commerce/prism/rpc/tenant"
	"code.justin.tv/foundation/twitchclient"
	"code.justin.tv/sse/malachai/pkg/s2s/caller"
	"github.com/stretchr/testify/suite"
)

const (
	sandstormSecretName = "commerce/payday/staging/qa_bits_tests_oauth_token"
)

type PaydayIntegTestSuite struct {
	suite.Suite

	paydayClient               visageClient.Client
	twirpClient                paydayrpc.Payday
	bitsPrismTenantTwirpClient prism_tenant.PrismTenant
	auditClient                *audit.AuditClient
	s3Client                   s3.IS3Client
	oAuthToken                 string
	ctx                        context.Context
}

func TestPayday(t *testing.T) {
	suite.Run(t, new(PaydayIntegTestSuite))
}

func (s *PaydayIntegTestSuite) SetupSuite() {
	cfg := LoadConfigs()

	sandstormClient := sandstorm.NewSandstorm(cfg)
	oAuthSecret, err := sandstormClient.Get(sandstormSecretName)
	if err != nil {
		log.WithError(err).Panic("could not fetch OAuth token from Sandstorm")
	} else if oAuthSecret == nil {
		log.Panic("OAuth secret value was nil")
	}
	s.oAuthToken = string(oAuthSecret.Plaintext)

	s.s3Client = s3.NewFromDefaultConfig()
	s.auditClient = audit.NewAuditClient(s.s3Client, cfg.DynamoTablePrefix)

	twitchClientConfig := twitchclient.ClientConf{
		Host: cfg.IntegrationTestsEndpoint,
	}
	if cfg.S2S.Enabled {
		s2sConfig := caller.Config{}

		rt, err := caller.NewRoundTripper(cfg.S2S.Name, &s2sConfig, nil)
		if err != nil {
			log.Panicf("Failed to create s2s round tripper: %v", err)
		}

		twitchClientConfig.RoundTripperWrappers = append(twitchClientConfig.RoundTripperWrappers,
			func(inner http.RoundTripper) http.RoundTripper {
				rt.SetInnerRoundTripper(inner)
				return rt
			},
		)
	}
	paydayClient, err := visageClient.NewClient(twitchClientConfig)
	if err != nil {
		log.Panicf("Failed to create admin payday client: %v", err)
	}
	s.paydayClient = paydayClient

	s.twirpClient = paydayrpc.NewPaydayProtobufClient(cfg.IntegrationTestsEndpoint, &http.Client{})

	prismTenantHTTPClient := &http.Client{}
	if cfg.S2S.Enabled {
		s2sConfig := caller.Config{}
		rt, err := caller.NewRoundTripper(cfg.S2S.Name, &s2sConfig, log.New())
		if err != nil {
			log.Panicf("Failed to create s2s round tripper: %v", err)
		}
		prismTenantHTTPClient.Transport = rt

	}
	s.bitsPrismTenantTwirpClient = prism_tenant.NewPrismTenantProtobufClient(
		fmt.Sprintf("%s%s", cfg.IntegrationTestsEndpoint, mux.BitsPrismTenantPrefix),
		prismTenantHTTPClient,
	)

	s.ctx = context.Background()
}

func LoadConfigs() config.Configuration {
	var err error
	environment := os.Getenv("ENVIRONMENT")

	if environment == "" {
		log.Panicf("ENVIRONMENT env variable not set")
	}

	if environment == "production" || environment == "prod" {
		log.Panicf("Please don't run these integration tests against prod!")
	}

	log.Infof("Loading config file for %s", environment)
	err = config.Load(environment)
	if err != nil {
		panic(err)
	}

	return config.Get()
}
