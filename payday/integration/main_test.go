// +build integration

// Integration tests to be run after a staging deployment.
// The integration build tag prevents these tests from being run during normal testing.
// Make sure to add the tag to each new integration test file.
// IMPORTANT: The tag MUST be defined ABOVE the package declaration and be followed by a BLANK LINE.
package integration

import (
	"errors"
	"fmt"
	"os"
	"testing"
	"time"

	"code.justin.tv/commerce/payday/test"
	"golang.org/x/net/context"
)

const (
	numAttempts = 3
	retryDelay  = time.Second * 5
)

func TestMain(m *testing.M) {
	_, cancel := context.WithCancel(context.Background())
	defer cancel()

	test.Seed()

	var exitCode int
	for i := 1; i <= numAttempts; i++ {
		fmt.Printf("\n\nBeginning Test attempt %d....\n\n", i)
		err := runTest(m)
		if err == nil {
			exitCode = 0
			break
		} else {
			exitCode = 1

			if i < numAttempts {
				fmt.Println("Tests failed sleeping 5s before retrying...")
				time.Sleep(retryDelay)
			}
		}
	}
	os.Exit(exitCode)
}

func runTest(m *testing.M) (err error) {
	exitCode := m.Run()
	if exitCode == 0 {
		return nil
	} else {
		return errors.New("Test attempt failed")
	}
}
