// +build integration

package integration

const (
	cloudwatchLogsConfigsBucket = "cloudwatch-logs-configs"
	lambdaSourceBucket          = "payday-audit-lambda-source"
	dynamoAuditRecordsBucket    = "payday-dynamo-audit-records"
	cloudwatchLogsConfigFile    = "payday-cloudwatch-logs-config-prod"
	channelsAuditFile           = "prod_channels_save_audit_record_lambda_code.zip"
	usersAuditFile              = "prod_users_save_audit_record_lambda_code.zip"
)

func (s *PaydayIntegTestSuite) TestRequiredResourcesExist() {
	// Make sure required S3 buckets exist
	exists, err := s.s3Client.BucketExists(cloudwatchLogsConfigsBucket)
	s.Require().NoError(err)
	s.Require().True(exists, "Cloudwatch logs config bucket should exist")

	exists, err = s.s3Client.BucketExists(lambdaSourceBucket)
	s.Require().NoError(err)
	s.Require().True(exists, "Lambda source bucket should exist")

	exists, err = s.s3Client.BucketExists(dynamoAuditRecordsBucket)
	s.Require().NoError(err)
	s.Require().True(exists, "Dynamo audit record bucket should exist")

	// Make sure required S3 files exist
	exists, err = s.s3Client.FileExists(cloudwatchLogsConfigsBucket, cloudwatchLogsConfigFile)
	s.Require().NoError(err)
	s.Require().True(exists, "Cloudwatch logs config file should exist")

	exists, err = s.s3Client.FileExists(lambdaSourceBucket, channelsAuditFile)
	s.Require().NoError(err)
	s.Require().True(exists, "Channels table audit source should exist")

	exists, err = s.s3Client.FileExists(lambdaSourceBucket, usersAuditFile)
	s.Require().NoError(err)
	s.Require().True(exists, "Users table audit source should exist")
}
