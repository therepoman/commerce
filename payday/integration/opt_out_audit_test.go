// +build integration

package integration

import (
	"strconv"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/audit"
	apimodel "code.justin.tv/commerce/payday/models/api"
	timeUtils "code.justin.tv/commerce/payday/utils/time"
)

const (
	optOutAnnotation   = "OptinOut test userId as part of payday integration tests."
	unOptOutAnnotation = "UnOptingOut test userId as part of payday integration tests."
)

func (s *PaydayIntegTestSuite) TestOptOutAuditing() {
	userId := s.getRandomTesUser()

	// Validate that the random user does not currently have a opt-out status
	var expectedGetOptOutResponse1 *apimodel.GetOptOutResponse = nil
	actualGetOptOutResponse1 := s.validateOptOutStatus(userId, expectedGetOptOutResponse1)

	// Opt out the user, then get opt out status and validate
	s.optOutUser(userId)
	expectedGetOptOutResponse2 := createGetOptOutResponse(userId, true, optOutAnnotation)
	actualGetOptOutResponse2 := s.validateOptOutStatus(userId, &expectedGetOptOutResponse2)

	// Un-opt out the user, then get opt out status and validate
	s.unOptOutUser(userId)
	expectedGetOptOutResponse3 := createGetOptOutResponse(userId, false, unOptOutAnnotation)
	actualGetOptOutResponse3 := s.validateOptOutStatus(userId, &expectedGetOptOutResponse3)

	// Retrieve user audit records from S3 and validate
	channelAuditFileNames, err := s.auditClient.WaitForNChannelAuditRecordFileNames(userId, 2, s3MaxWaitTime)
	s.Require().NoError(err, "Errored when waiting for channel audit records to appear in S3")
	s.validateChannelAuditRecords(channelAuditFileNames, userId, actualGetOptOutResponse1, actualGetOptOutResponse2, actualGetOptOutResponse3)
}

func (s *PaydayIntegTestSuite) validateOptOutStatus(userId string, expected *apimodel.GetOptOutResponse) *apimodel.GetOptOutResponse {
	actual, err := s.paydayClient.GetOptOut(s.ctx, userId, nil)
	s.Require().NoError(err, "Errored when getting opt-out status")
	s.Require().True(equivalentGetOptOutResponses(expected, actual), "Unexpected opt-out status response")
	return actual
}

func (s *PaydayIntegTestSuite) optOutUser(userId string) {
	log.Infof("Opting out test user %s", userId)
	optOutRequest := &apimodel.SetOptOutRequest{
		OptedOut:   true,
		Annotation: optOutAnnotation,
	}
	err := s.paydayClient.SetOptOut(s.ctx, userId, optOutRequest, nil)
	s.Require().NoError(err, "Errored when opting out test user")
}

func (s *PaydayIntegTestSuite) unOptOutUser(userId string) {
	log.Infof("Un-opting out test user %s", userId)
	unOptOutRequest := &apimodel.SetOptOutRequest{
		OptedOut:   false,
		Annotation: unOptOutAnnotation,
	}
	err := s.paydayClient.SetOptOut(s.ctx, userId, unOptOutRequest, nil)
	s.Require().NoError(err, "Errored when un-optingOut test user")
}

func (s *PaydayIntegTestSuite) validateChannelAuditRecords(fileNames []string, userId string, getOptOutResponses ...*apimodel.GetOptOutResponse) {
	log.Infof("Validating S3 audit files")
	var auditRecords [2]audit.ChannelRecord
	audit.SortFileNamesBySequenceNumber(fileNames)
	for i, channelAuditFileName := range fileNames {
		record, err := s.auditClient.LoadChannelAuditRecord(channelAuditFileName)
		s.Require().NoError(err, "Errored when when loading and parsing audit record in S3")
		auditRecords[i] = *record
	}

	expectedAuditRecord1 := createChannelRecord(userId, getOptOutResponses[0], getOptOutResponses[1])
	expectedAuditRecord2 := createChannelRecord(userId, getOptOutResponses[1], getOptOutResponses[2])
	s.Require().True(equivalentAuditChannelRecord(expectedAuditRecord1, auditRecords[0]))
	s.Require().True(equivalentAuditChannelRecord(expectedAuditRecord2, auditRecords[1]))
}

func createGetOptOutResponse(userId string, optOut bool, annotation string) apimodel.GetOptOutResponse {
	return apimodel.GetOptOutResponse{
		User:        userId,
		OptedOut:    optOut,
		Annotation:  annotation,
		LastUpdated: time.Now().UnixNano(),
	}
}

func equivalentGetOptOutResponses(expected, actual *apimodel.GetOptOutResponse) bool {
	if expected == nil && actual == nil {
		return true
	} else if (expected == nil && actual != nil) || (expected != nil && actual == nil) {
		return false
	}

	expectedTime := time.Unix(0, expected.LastUpdated)
	actualTime := time.Unix(0, actual.LastUpdated)
	equivalentBesidesTime := equivalentGetOptOutResponsesBesidesLastUpdated(*expected, *actual)
	return equivalentBesidesTime && timeUtils.IsTimeInBiDirectionalDelta(actualTime, expectedTime, acceptableTimeDelta)
}

func equivalentGetOptOutResponsesBesidesLastUpdated(expected, actual apimodel.GetOptOutResponse) bool {
	expected.LastUpdated = 0
	actual.LastUpdated = 0
	return expected == actual
}

func equivalentAuditChannelRecord(expected, actual audit.ChannelRecord) bool {
	keysMatch := expected.Keys == actual.Keys
	oldMatch := equivalentAuditChannelImage(expected.OldImage, actual.OldImage)
	newMatch := equivalentAuditChannelImage(expected.NewImage, actual.NewImage)
	return keysMatch && oldMatch && newMatch
}

func equivalentAuditChannelImage(expected, actual *audit.ChannelImage) bool {
	if expected == nil && actual == nil {
		return true
	} else if (expected == nil && actual != nil) || (expected != nil && actual == nil) {
		return false
	}

	expectedTimeI, err := strconv.Atoi(expected.LastUpdated.N)
	if err != nil {
		panic("Last update time string not numeric")
	}
	actualTimeI, err := strconv.Atoi(actual.LastUpdated.N)
	if err != nil {
		panic("Last update time string not numeric")
	}
	expectedTime := time.Unix(0, int64(expectedTimeI))
	actualTime := time.Unix(0, int64(actualTimeI))

	equivalentBesidesTime := equivalentAuditChannelImageBesidesLastUpdated(*expected, *actual)
	return equivalentBesidesTime && timeUtils.IsTimeInBiDirectionalDelta(actualTime, expectedTime, acceptableTimeDelta)
}

func equivalentAuditChannelImageBesidesLastUpdated(expected, actual audit.ChannelImage) bool {
	expected.LastUpdated.N = ""
	actual.LastUpdated.N = ""
	return expected == actual
}

func createChannelRecord(id string, old *apimodel.GetOptOutResponse, new *apimodel.GetOptOutResponse) audit.ChannelRecord {
	record := audit.ChannelRecord{
		Keys: audit.ChannelKeys{
			Id: audit.SComponent{
				S: id,
			},
		},
	}

	if old != nil {
		record.OldImage = createChannelImage(*old)
	}

	if new != nil {
		record.NewImage = createChannelImage(*new)
	}
	return record
}

func createChannelImage(response apimodel.GetOptOutResponse) *audit.ChannelImage {
	return &audit.ChannelImage{
		Id: audit.SComponent{
			S: response.User,
		},
		OptedOut: audit.BoolComponent{
			Bool: response.OptedOut,
		},
		Annotation: audit.SComponent{
			S: response.Annotation,
		},
		LastUpdated: audit.NComponent{
			N: strconv.Itoa(int(response.LastUpdated)),
		},
	}
}
