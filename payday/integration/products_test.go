// +build integration

package integration

func (s *PaydayIntegTestSuite) TestProductsAreConsistent() {
	userId := s.getRandomTesUser()

	firstResponse, err := s.paydayClient.GetBitsProducts(s.ctx, userId, "US", "en", "PAYPAL", nil)
	s.Require().NoError(err)

	secondResponse, err := s.paydayClient.GetBitsProducts(s.ctx, userId, "US", "en", "PAYPAL", nil)
	s.Require().NoError(err)

	thirdResponse, err := s.paydayClient.GetBitsProducts(s.ctx, userId, "US", "en", "PAYPAL", nil)
	s.Require().NoError(err)

	s.Require().Equal(firstResponse, secondResponse)
	s.Require().Equal(secondResponse, thirdResponse)
	s.Require().Equal(thirdResponse, firstResponse)
}

func (s *PaydayIntegTestSuite) TestMobileProductsCountryRestrictions() {
	userId := s.getRandomTesUser()

	response, err := s.paydayClient.GetBitsProducts(s.ctx, userId, "KP", "ko", "ios", nil) // north korea
	s.Require().NoError(err)
	s.Require().Len(response, 0)
}

func (s *PaydayIntegTestSuite) TestPromosToTopAndRestAreQuantitySorted() {
	userId := s.getRandomTesUser()

	response, err := s.paydayClient.GetBitsProducts(s.ctx, userId, "US", "en", "ios", nil)
	s.Require().NoError(err)
	s.Require().Equal("tv.twitch.ios.iap.bits.promotion.t7", response[0].Id)

	previousValue := 0
	for _, product := range response {
		if !product.Promo { // already checked promo behavior, for now we want to ignore it.
			s.Require().True(product.BitsAmount > previousValue) // I was unable to find anywhere on github where people used the require.Conditional and the syntax was not clear.
			previousValue = product.BitsAmount
		}
	}
}
