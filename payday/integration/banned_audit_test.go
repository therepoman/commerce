// +build integration

package integration

import (
	"strconv"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/audit"
	apimodel "code.justin.tv/commerce/payday/models/api"
	timeUtils "code.justin.tv/commerce/payday/utils/time"
)

const (
	bannedBy        = "PaydayIntegrationTests"
	banAnnotation   = "Banning test userId as part of payday integration tests."
	unBanAnnotation = "Unbanning test userId as part of payday integration tests."
)

func (s *PaydayIntegTestSuite) TestBannedAuditing() {
	userId := s.getRandomTesUser()

	// Validate that the random user does not currently have a ban status
	var expectedGetBannedResponse1 *apimodel.GetBannedResponse = nil
	actualGetBannedResponse1 := s.validateBanStatus(userId, expectedGetBannedResponse1)

	// Ban the user, then get ban status and validate
	s.banUser(userId)
	expectedGetBannedResponse2 := createGetBannedResponse(userId, true, bannedBy, banAnnotation)
	actualGetBannedResponse2 := s.validateBanStatus(userId, &expectedGetBannedResponse2)

	// Un-ban the user, then get ban status and validate (should get nil as response)
	s.unBanUser(userId)
	s.validateUnBanStatus(userId)

	// Retrieve user audit records from S3 and validate
	userAuditFileNames, err := s.auditClient.WaitForNUserAuditRecordFileNames(userId, 2, s3MaxWaitTime)
	s.Require().NoError(err, "Errored when waiting for user audit records to appear in S3")
	s.validateUserAuditRecords(userAuditFileNames, userId, actualGetBannedResponse1, actualGetBannedResponse2)
}

func (s *PaydayIntegTestSuite) validateBanStatus(userId string, expected *apimodel.GetBannedResponse) *apimodel.GetBannedResponse {
	actual, err := s.paydayClient.GetBanned(s.ctx, userId, nil)
	s.Require().NoError(err, "Errored when getting ban status")
	s.Require().True(equivalentGetBannedResponses(expected, actual), "Unexpected ban status response")
	return actual
}

func (s *PaydayIntegTestSuite) validateUnBanStatus(userId string) {
	var expected *apimodel.GetBannedResponse
	expected = nil
	actual, err := s.paydayClient.GetBanned(s.ctx, userId, nil)
	s.Require().NoError(err)
	s.Require().Equal(expected, actual)
}

func (s *PaydayIntegTestSuite) banUser(userId string) {
	log.Infof("Banning test user %s", userId)
	banRequest := &apimodel.SetBannedRequest{
		Banned:     true,
		BannedBy:   bannedBy,
		Annotation: banAnnotation,
	}
	err := s.paydayClient.SetBanned(s.ctx, userId, banRequest, nil)
	s.Require().NoError(err, "Errored when banning test user")
}

func (s *PaydayIntegTestSuite) unBanUser(userId string) {
	log.Infof("Un-banning test user %s", userId)
	unBanRequest := &apimodel.SetBannedRequest{
		Banned:     false,
		BannedBy:   bannedBy,
		Annotation: unBanAnnotation,
	}
	err := s.paydayClient.SetBanned(s.ctx, userId, unBanRequest, nil)
	s.Require().NoError(err, "Errored when un-banning test user")
}

func (s *PaydayIntegTestSuite) validateUserAuditRecords(fileNames []string, userId string, getBannedResponses ...*apimodel.GetBannedResponse) {
	log.Infof("Validating S3 audit files")
	var auditRecords [2]audit.UserRecord
	audit.SortFileNamesBySequenceNumber(fileNames)
	for i, userAuditFileName := range fileNames {
		record, err := s.auditClient.LoadUserAuditRecord(userAuditFileName)
		s.Require().NoError(err, "Errored when when loading and parsing audit record in S3")
		auditRecords[i] = *record
	}

	if len(getBannedResponses) > 1 {
		expectedAuditRecord1 := createUserRecord(userId, getBannedResponses[0], getBannedResponses[1])
		s.Require().True(equivalentAuditUserRecord(expectedAuditRecord1, auditRecords[0]))
	}

	if len(getBannedResponses) > 2 {
		expectedAuditRecord2 := createUserRecord(userId, getBannedResponses[1], getBannedResponses[2])
		s.Require().True(equivalentAuditUserRecord(expectedAuditRecord2, auditRecords[1]))
	}
}

func createGetBannedResponse(userId string, banned bool, bannedBy string, annotation string) apimodel.GetBannedResponse {
	return apimodel.GetBannedResponse{
		User:        userId,
		Banned:      banned,
		BannedBy:    bannedBy,
		Annotation:  annotation,
		LastUpdated: time.Now().UnixNano(),
	}
}

func equivalentGetBannedResponses(expected, actual *apimodel.GetBannedResponse) bool {
	if expected == nil && actual == nil {
		return true
	} else if (expected == nil && actual != nil) || (expected != nil && actual == nil) {
		return false
	}

	expectedTime := time.Unix(0, expected.LastUpdated)
	actualTime := time.Unix(0, actual.LastUpdated)
	equivalentBesidesLastUpdated := equivalentGetBannedResponsesBesidesLastUpdated(*expected, *actual)
	return equivalentBesidesLastUpdated && timeUtils.IsTimeInBiDirectionalDelta(actualTime, expectedTime, acceptableTimeDelta)
}

func equivalentGetBannedResponsesBesidesLastUpdated(expected, actual apimodel.GetBannedResponse) bool {
	expected.LastUpdated = 0
	actual.LastUpdated = 0
	return expected == actual
}

func equivalentAuditUserRecord(expected, actual audit.UserRecord) bool {
	keysMatch := expected.Keys == actual.Keys
	oldMatch := equivalentAuditUserImage(expected.OldImage, actual.OldImage)
	newMatch := equivalentAuditUserImage(expected.NewImage, actual.NewImage)
	return keysMatch && oldMatch && newMatch
}

func equivalentAuditUserImage(expected, actual *audit.UserImage) bool {
	if expected == nil && actual == nil {
		return true
	} else if (expected == nil && actual != nil) || (expected != nil && actual == nil) {
		return false
	}

	expectedTimeI, err := strconv.Atoi(expected.LastUpdated.N)
	if err != nil {
		panic("Last update time string not numeric")
	}
	actualTimeI, err := strconv.Atoi(actual.LastUpdated.N)
	if err != nil {
		panic("Last update time string not numeric")
	}
	expectedTime := time.Unix(0, int64(expectedTimeI))
	actualTime := time.Unix(0, int64(actualTimeI))

	equivalentBesidesLastUpdated := equivalentAuditUserImageBesidesLastUpdated(*expected, *actual)
	return equivalentBesidesLastUpdated && timeUtils.IsTimeInBiDirectionalDelta(actualTime, expectedTime, acceptableTimeDelta)
}

func equivalentAuditUserImageBesidesLastUpdated(expected, actual audit.UserImage) bool {
	expected.LastUpdated.N = ""
	actual.LastUpdated.N = ""
	return expected == actual
}

func createUserRecord(name string, old *apimodel.GetBannedResponse, new *apimodel.GetBannedResponse) audit.UserRecord {
	record := audit.UserRecord{
		Keys: audit.UserKeys{
			Id: audit.SComponent{
				S: name,
			},
		},
	}
	if old != nil {
		record.OldImage = createUserImage(*old)
	}

	if new != nil {
		record.NewImage = createUserImage(*new)
	}
	return record
}

func createUserImage(response apimodel.GetBannedResponse) *audit.UserImage {
	return &audit.UserImage{
		Id: audit.SComponent{
			S: response.User,
		},
		Banned: audit.BoolComponent{
			Bool: response.Banned,
		},
		BannedBy: audit.SComponent{
			S: response.BannedBy,
		},
		Annotation: audit.SComponent{
			S: response.Annotation,
		},
		LastUpdated: audit.NComponent{
			N: strconv.Itoa(int(response.LastUpdated)),
		},
	}
}
