// +build integration

package integration

func (s *PaydayIntegTestSuite) TestAddRemoveBits() {
	userId := s.getRandomTestUser("TestAddRemoveBits")
	s.validateBalance(userId, 0)
	t1 := s.addBits(userId, 1024)
	s.validateBalanceAndEvents(userId, 1024, t1)
	t2 := s.removeBits(userId, 512)
	s.validateBalanceAndEvents(userId, 512, t1, t2)
}

func (s *PaydayIntegTestSuite) TestCannotRemoveBitsBelowZero() {
	userId := s.getRandomTestUser("TestCannotRemoveBitsBelowZero")
	s.validateBalance(userId, 0)
	s.removeBitsError(userId, 1)
	s.validateBalance(userId, 0)
}
