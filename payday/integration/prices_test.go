// +build integration

package integration

import (
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/models/api"
)

func (s *PaydayIntegTestSuite) TestWithNoParams() {
	request := api.GetPricesRequest{
		CountryOfResidence: "US",
	}

	response, err := s.paydayClient.GetPrices(s.ctx, request, nil)
	s.Require().NoError(err)
	s.Require().NotEmpty(response.Prices)

	s.Require().False(response.PriceIncludesVAT)
	s.Require().Equal("USD", response.Prices[0].CurrencyUnit)
	s.Require().Regexp("https\\:\\/\\/staging\\-bitscheckout\\-www\\.dev\\.us-west2\\.twitch\\.tv\\/bits-checkout\\/select\\?asin=.*", response.Prices[0].PurchaseURL)
}

func (s *PaydayIntegTestSuite) TestWithAmazon() {
	request := api.GetPricesRequest{
		CountryOfResidence: "US",
		Platform:           string(models.AMAZON),
	}

	response, err := s.paydayClient.GetPrices(s.ctx, request, nil)
	s.Require().NoError(err)
	s.Require().NotEmpty(response.Prices)

	s.Require().Equal("USD", response.Prices[0].CurrencyUnit)
	s.Require().Regexp("https\\:\\/\\/staging\\-bitscheckout\\-www\\.dev\\.us-west2\\.twitch\\.tv\\/bits-checkout\\/purchase\\?asin=.*", response.Prices[0].PurchaseURL)
}

func (s *PaydayIntegTestSuite) TestWithPayPal() {
	request := api.GetPricesRequest{
		CountryOfResidence: "US",
		Platform:           string(models.PAYPAL),
	}

	response, err := s.paydayClient.GetPrices(s.ctx, request, nil)
	s.Require().NoError(err)
	s.Require().NotEmpty(response.Prices)

	s.Require().Regexp("https\\:\\/\\/staging\\-bitscheckout\\-www\\.dev\\.us-west2\\.twitch\\.tv\\/bits-checkout\\/summary\\?asin=.*", response.Prices[0].PurchaseURL)
}

func (s *PaydayIntegTestSuite) TestLoggedInWithAmazon() {
	request := api.GetPricesRequest{
		CountryOfResidence: "US",
		UserID:             testAdmin, // qa_bits_partner
		Platform:           string(models.AMAZON),
	}

	response, err := s.paydayClient.GetPrices(s.ctx, request, nil)
	s.Require().NoError(err)
	s.Require().NotEmpty(response.Prices)
}

func (s *PaydayIntegTestSuite) TestLoggedOutWithNonAmazonCor() {
	request := api.GetPricesRequest{
		Platform:           string(models.PAYPAL),
		CountryOfResidence: "IE",
	}

	response, err := s.paydayClient.GetPrices(s.ctx, request, nil)
	s.Require().NoError(err)
	s.Require().NotEmpty(response.Prices)

	s.Require().Equal("EUR", response.Prices[0].CurrencyUnit)
	s.Require().True(response.PriceIncludesVAT)
}
