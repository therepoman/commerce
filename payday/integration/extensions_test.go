// +build integration

package integration

import (
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
)

func (s *PaydayIntegTestSuite) TestTwirpIsEligible() {
	userId, channelId := s.getRandomTesUser(), s.getRandomTesUser()
	s.onboard(channelId)

	req := &paydayrpc.IsEligibleToUseBitsOnExtensionReq{
		Tuid:              userId,
		ChannelId:         channelId,
		ExtensionClientId: "test-extension",
		SKU:               "test-sku",
		TransactionId:     "test-transaction-id",
	}
	_, err := s.twirpClient.IsEligibleToUseBitsOnExtension(s.ctx, req)
	s.Require().Error(err)
	// TODO: Implement test once we have a test-sku in falador
}
