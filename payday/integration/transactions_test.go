// +build integration

package integration

import (
	"context"

	"code.justin.tv/commerce/payday/models"
	apimodel "code.justin.tv/commerce/payday/models/api"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"github.com/pborman/uuid"
)

func (s *PaydayIntegTestSuite) TestGetTransactionOfEntitlementByAmount() {
	transactionId := uuid.New()
	userId := s.getRandomTestUser("TestEntitleBitsByAmount")

	request := &apimodel.EntitleBitsRequest{
		TwitchUserId:  userId,
		TransactionId: transactionId,
		BitAmount:     2,
		BitsType:      string(models.CB_0),
		Platform:      string(models.TRUEX),
	}

	_, err := s.entitleBits(request)
	s.Require().NoError(err)

	resp, err := s.twirpClient.GetTransactionsByID(context.Background(), &paydayrpc.GetTransactionsByIDReq{
		TransactionIds: []string{transactionId},
	})
	s.Require().NoError(err)
	s.Require().Len(resp.Transactions, 1)
	s.Require().Equal(resp.Transactions[0].TransactionId, transactionId)
	s.Require().False(resp.Transactions[0].InFlight)
}

func (s *PaydayIntegTestSuite) TestGetTransactionOfPrismProcessMessage() {
	userId, channelId := s.getRandomTesUser(), s.getRandomTesUser()
	s.onboard(channelId)
	s.addBits(userId, 1024)

	_, event := s.bitsEventProcess(userId, channelId, 256, "cheer256", nil)

	resp, err := s.twirpClient.GetTransactionsByID(context.Background(), &paydayrpc.GetTransactionsByIDReq{
		TransactionIds: []string{event.EventId},
	})
	s.Require().NoError(err)
	s.Require().Len(resp.Transactions, 1)
	s.Require().Equal(resp.Transactions[0].TransactionId, event.EventId)
	s.Require().False(resp.Transactions[0].InFlight)
}
