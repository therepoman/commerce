// +build integration

package integration

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"github.com/pborman/uuid"

	log "code.justin.tv/commerce/logrus"
	payday_models "code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/prism/rpc/models"
	prism_tenant "code.justin.tv/commerce/prism/rpc/tenant"
	"code.justin.tv/common/golibs/pubsubclient"
)

const (
	pubsubEnpoint    = "wss://pubsub-edge.twitch.tv"
	maxPubsubRetries = 10
)

type ProcessingFunction func(string)

func (s *PaydayIntegTestSuite) TestPrismPrivatePubsubEvent() {
	s.retryablePubsubTest(func() (int, error) {
		transactionId := uuid.New()
		userId, channelId := andymcId, qaBitsTestsId
		numberOfBits := int64(100)
		s.addBits(userId, int32(numberOfBits))

		msg := fmt.Sprintf("cheer%d %s", numberOfBits, transactionId)
		req := &prism_tenant.ProcessMessageReq{
			MessageId: transactionId,
			UserId:    userId,
			ChannelId: channelId,
			Message:   msg,
			ConsumableAmounts: map[string]int64{
				models.ConsumableType_Bits: numberOfBits,
			},
		}

		errChan := make(chan error, 1)
		messageChan := make(chan payday_models.PubsubTransactionMessageV1, 2)
		pubsub := s.subscribeToPubsub(qaBitsTestsPrivateV1PubSub)

		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		defer cancel()

		s.pollPubsubMessages(ctx, pubsub, func(msg string) {
			var outerMsg payday_models.PrivateBitsMessage
			err := json.Unmarshal([]byte(msg), &outerMsg)
			if err != nil {
				errChan <- err
			} else {
				// Data is returned as a map, needs to be converted to json so it can be unmarshaled as the correct struct
				innerMsgJSON, err := json.Marshal(outerMsg.Data)
				if err != nil {
					errChan <- err
				} else {
					var innerMsg payday_models.PubsubTransactionMessageV1
					err = json.Unmarshal([]byte(innerMsgJSON), &innerMsg)
					if err != nil {
						errChan <- err
					} else {
						messageChan <- innerMsg
					}
				}
			}
		})

		resp, err := s.bitsPrismTenantTwirpClient.ProcessMessage(s.ctx, req)
		s.Require().NoError(err)
		s.Require().NotNil(resp)
		s.Require().Equal(int64(100), resp.ConsumableAmountsProcessed[models.ConsumableType_Bits])
		s.bitsEventPostProcess(userId, channelId, numberOfBits, req.MessageId, msg, &PrismOpts{SentMessage: msg})

		select {
		case err := <-errChan:
			return 0, err
		case message := <-messageChan:
			s.Require().Equal(userId, message.UserId)
			s.Require().Equal(channelId, message.ChannelId)
			s.Require().Equal(int(numberOfBits), message.BitsUsed)
			s.Require().Equal(msg, message.ChatMessage)
		case <-ctx.Done():
			return 0, ctx.Err()
		}

		return 0, nil
	}, nil)
}

func (s *PaydayIntegTestSuite) TestPrismPrivatePubsubEventCheckForDouble() {
	s.retryablePubsubTest(func() (int, error) {
		transactionId := uuid.New()
		userId, channelId := andymcId, qaBitsTestsId
		numberOfBits := int64(100)
		s.addBits(userId, int32(numberOfBits))

		msg := fmt.Sprintf("cheer%d %s", numberOfBits, transactionId)
		req := &prism_tenant.ProcessMessageReq{
			MessageId: transactionId,
			UserId:    userId,
			ChannelId: channelId,
			Message:   msg,
			ConsumableAmounts: map[string]int64{
				models.ConsumableType_Bits: numberOfBits,
			},
		}

		errChan := make(chan error, 1)
		messageChan := make(chan payday_models.PubsubTransactionMessageV1, 2)
		pubsub := s.subscribeToPubsub(qaBitsTestsPrivateV1PubSub)

		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		defer cancel()
		s.pollPubsubMessages(ctx, pubsub, func(msg string) {
			var outerMsg payday_models.PrivateBitsMessage
			err := json.Unmarshal([]byte(msg), &outerMsg)
			if err != nil {
				errChan <- err
			} else {
				// Data is returned as a map, needs to be converted to json so it can be unmarshaled as the correct struct
				innerMsgJSON, err := json.Marshal(outerMsg.Data)
				if err != nil {
					errChan <- err
				} else {
					var innerMsg payday_models.PubsubTransactionMessageV1
					err = json.Unmarshal([]byte(innerMsgJSON), &innerMsg)
					if err != nil {
						errChan <- err
					} else {
						messageChan <- innerMsg
					}
				}
			}
		})

		resp, err := s.bitsPrismTenantTwirpClient.ProcessMessage(s.ctx, req)
		s.Require().NoError(err)
		s.Require().NotNil(resp)
		s.Require().Equal(int64(100), resp.ConsumableAmountsProcessed[models.ConsumableType_Bits])
		s.bitsEventPostProcess(userId, channelId, numberOfBits, req.MessageId, msg, &PrismOpts{SentMessage: msg})

		messageCount := 0
		for i := 0; i < 2; i++ {
			select {
			case err := <-errChan:
				return 0, err
			case message := <-messageChan:
				if userId == message.UserId && channelId == message.ChannelId && int(numberOfBits) == message.BitsUsed {
					messageCount++
				}
			case <-ctx.Done():
				break
			}
		}

		return messageCount, nil
	}, func(messageCount int) {
		s.Require().Equal(1, messageCount)
	})
}

// This is still a work in progress, it's always failing with pubsub auth errors.
//func (s *PaydayIntegTestSuite) TestBalancePubSubUpdate() {
//	userId := qaBitsTestsId
//	numberOfBits := int32(100)
//
//	errChan := make(chan error, 1)
//	messageChan := make(chan payday_models.BalanceUpdates, 2)
//	pubsub := s.subscribeToPubsub(qaBitsTestsBalancePubSub)
//
//	ctx, cancel := context.WithTimeout(context.Background(), 60*time.Second)
//	defer cancel()
//	s.pollPubsubMessages(ctx, pubsub, func(msg string) {
//		var bitsMessage payday_models.PrivateBitsMessage
//		err := json.Unmarshal([]byte(msg), &bitsMessage)
//		if err != nil {
//			errChan <- err
//		} else if bitsMessage.MessageType == payday_models.BitBalanceUpdateMessageType {
//			var balanceUpdate payday_models.BalanceUpdates
//			messageChan <- balanceUpdate
//		}
//	})
//
//	response, err := s.paydayClient.GetBalance(s.ctx, userId, "", nil)
//	s.Require().NoError(err)
//	s.addBits(userId, numberOfBits)
//	s.validateBalance(userId, response.Balance+numberOfBits)
//
//	select {
//	case err := <-errChan:
//		s.Require().Fail("Error from test %s", err)
//	case message := <-messageChan:
//		s.Require().Equal(userId, message.UserId)
//		s.Require().Equal(int64(response.Balance+numberOfBits), message.Balance)
//	case <-ctx.Done():
//		s.Require().Fail("Context finished %s", ctx.Err())
//	}
//}

func (s *PaydayIntegTestSuite) subscribeToPubsub(topic string) chan *pubsubclient.Message {
	client := pubsubclient.New(pubsubEnpoint, nil)
	err := client.Subscribe(topic, s.oAuthToken)

	if err != nil {
		s.Require().NoError(err)
	}
	pubsub := getPubsubChannel(client)
	return pubsub
}

func getPubsubChannel(client pubsubclient.Client) chan *pubsubclient.Message {
	output := make(chan *pubsubclient.Message)
	go func() {
		for {
			msg, err := client.NextMessage()
			if err != nil {
				return
			} else if err == pubsubclient.ErrDisconnected {
				//see: http://bit.ly/2j7EkmW
				continue
			}

			output <- msg
		}
	}()

	return output
}

func (s *PaydayIntegTestSuite) pollPubsubMessages(ctx context.Context, channel chan *pubsubclient.Message, processingFunction ProcessingFunction) {
	go func() {
		for {
			select {
			case <-ctx.Done():
				log.Infof("context failure while polling for messages: %s", ctx.Err())
				return
			case message := <-channel:
				log.Infof("Received pubsub message %s", message.Message)
				processingFunction(message.Message)
			}
		}
	}()
}

// Listening for pubsub messages within integration tests has proven to be unreliable. If we succeed
// 1/10 times, we will consider that validation enough that our messages are being sent properly.
func (s *PaydayIntegTestSuite) retryablePubsubTest(testFunc func() (messageCount int, err error), messageCountAssertion func(count int)) {
	var err error

	for attempts := 0; attempts < maxPubsubRetries; attempts++ {
		messageCount, err := testFunc()

		if err == nil {
			if messageCountAssertion != nil {
				messageCountAssertion(messageCount)
			}

			return
		}

		log.Infof("retrying - received an error awaiting pubsub: %s ", err)
	}

	s.Require().Fail("Exhausted retries awaiting pubsub. Most recent error: %s", err)
}
