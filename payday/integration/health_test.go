// +build integration

package integration

import (
	"context"

	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
)

func (s *PaydayIntegTestSuite) TestTwirpHealthCheck() {
	resp, err := s.twirpClient.HealthCheck(context.Background(), &paydayrpc.HealthCheckReq{})
	s.Require().NoError(err)
	s.Require().Equal(&paydayrpc.HealthCheckResp{
		Message: "OK",
	}, resp)
}
