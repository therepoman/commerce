// +build integration

package integration

import (
	"strconv"
	"time"

	"code.justin.tv/commerce/gogogadget/strings"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/models"
	apimodel "code.justin.tv/commerce/payday/models/api"
	"code.justin.tv/commerce/payday/test"
	"code.justin.tv/commerce/payday/user"
	timeUtils "code.justin.tv/commerce/payday/utils/time"
	prism "code.justin.tv/commerce/prism/rpc"
	prism_models "code.justin.tv/commerce/prism/rpc/models"
	prism_tenant "code.justin.tv/commerce/prism/rpc/tenant"
	"code.justin.tv/foundation/twitchclient"
	"github.com/pborman/uuid"
	"github.com/twitchtv/twirp"
)

const (
	acceptableTimeDelta        = time.Minute * 5 // Needs to be high due to potential clock skew
	s3MaxWaitTime              = time.Second * 30
	testRealPartnerOptedOut    = "67652383"
	testRealPartnerOptedIn     = "54361543" // jtmoney357
	testRealNonPartner         = "67656031"
	testAdmin                  = "116076154"
	andymcId                   = "108706200"
	bitsTestUser1Id            = "138165611" // bits_test_1
	bitsTestUser2Id            = "138165652" // bits_test_2
	bitsTestUser3Id            = "138165685" // bits_test_3
	qaBitsTestsId              = "145903336"
	qaBitsTestsPrivateV1PubSub = "channel-bits-events-v1.145903336"
	qaBitsTestsBalancePubSub   = "user-bits-updates-v1.145903336"
)

type PrismOpts struct {
	IsAnon      bool
	RoomID      string
	GeoIPData   *prism.GeoIPMetadata
	SentMessage string
}

func (s *PaydayIntegTestSuite) createEvent(eventId string, userId string, channelId string, total int32) *apimodel.Event {
	event := &apimodel.Event{
		EventId:   eventId,
		UserId:    userId,
		ChannelId: channelId,
		Total:     int64(total),
		Timestamp: time.Now(),
	}
	return event
}

func (s *PaydayIntegTestSuite) bitsEventTokenizeAndValidateReq(userID, channelID string, amount int64, message string, opts *PrismOpts) *prism_tenant.TokenizeAndValidateMessageReq {
	req := &prism_tenant.TokenizeAndValidateMessageReq{
		MessageId: uuid.New(),
		UserId:    userID,
		ChannelId: channelID,
		Message:   message,
		ConsumableAmounts: map[string]int64{
			prism_models.ConsumableType_Bits: amount,
		},
	}

	if opts != nil {
		if opts.IsAnon {
			req.Anonymous = opts.IsAnon
		}

		if !strings.Blank(opts.RoomID) {
			req.RoomId = opts.RoomID
		}
	}

	return req
}

func (s *PaydayIntegTestSuite) bitsEventProcessReq(userID, channelID string, amount int64, message string, opts *PrismOpts) *prism_tenant.ProcessMessageReq {
	req := &prism_tenant.ProcessMessageReq{
		MessageId: uuid.New(),
		UserId:    userID,
		ChannelId: channelID,
		Message:   message,
		ConsumableAmounts: map[string]int64{
			prism_models.ConsumableType_Bits: amount,
		},
	}

	if opts != nil {
		if opts.IsAnon {
			req.Anonymous = opts.IsAnon
		}

		if !strings.Blank(opts.RoomID) {
			req.RoomId = opts.RoomID
		}

		if opts.GeoIPData != nil {
			req.GeoIpMetadata = opts.GeoIPData
		}
	}

	return req
}

func (s *PaydayIntegTestSuite) bitsEventPostProcessReq(userID, channelID string, amount int64, messageID string, message string, opts *PrismOpts) *prism_tenant.PostProcessMessageReq {
	req := &prism_tenant.PostProcessMessageReq{
		MessageId: messageID,
		UserId:    userID,
		ChannelId: channelID,
		Message:   message,
		ConsumableAmounts: map[string]int64{
			prism_models.ConsumableType_Bits: amount,
		},
	}

	if opts != nil {
		req.SentMessage = opts.SentMessage

		if opts.IsAnon {
			req.Anonymous = opts.IsAnon
		}

		if !strings.Blank(opts.RoomID) {
			req.RoomId = opts.RoomID
		}

		if opts.GeoIPData != nil {
			req.GeoIpMetadata = opts.GeoIPData
		}
	}

	return req
}

func (s *PaydayIntegTestSuite) bitsEventTokenizeAndValidate(userID, channelID string, amount int64, message string, opts *PrismOpts) *prism_tenant.TokenizeAndValidateMessageResp {
	req := s.bitsEventTokenizeAndValidateReq(userID, channelID, amount, message, opts)
	resp, err := s.bitsPrismTenantTwirpClient.TokenizeAndValidateMessage(s.ctx, req)
	s.Require().NoError(err)
	return resp
}

func (s *PaydayIntegTestSuite) bitsEventProcess(userID, channelID string, amount int64, message string, opts *PrismOpts) (*prism_tenant.ProcessMessageResp, *apimodel.Event) {
	req := s.bitsEventProcessReq(userID, channelID, amount, message, opts)
	resp, err := s.bitsPrismTenantTwirpClient.ProcessMessage(s.ctx, req)
	s.Require().NoError(err)
	s.Require().NotNil(resp)

	return resp, s.createEvent(req.MessageId, userID, channelID, int32(-amount))
}

func (s *PaydayIntegTestSuite) bitsEventPostProcess(userID, channelID string, amount int64, messageID string, message string, opts *PrismOpts) *prism_tenant.PostProcessMessageResp {
	req := s.bitsEventPostProcessReq(userID, channelID, amount, messageID, message, opts)
	resp, err := s.bitsPrismTenantTwirpClient.PostProcessMessage(s.ctx, req)
	s.Require().NoError(err)
	return resp
}

func (s *PaydayIntegTestSuite) bitsEventErrorTokenizeAndValidate(userID, channelID string, amount int64, message string, opts *PrismOpts, expectedStatus string) {
	req := s.bitsEventTokenizeAndValidateReq(userID, channelID, amount, message, opts)

	_, err := s.bitsPrismTenantTwirpClient.TokenizeAndValidateMessage(s.ctx, req)
	s.Require().Error(err)

	twirpErr, ok := err.(twirp.Error)
	s.Require().True(ok, "not a twirp error")

	var clientErr apimodel.CreateEventErrorResponse
	parseErr := prism_models.ParseClientError(twirpErr, &clientErr)
	s.Require().NoError(parseErr)

	s.Require().Equal(expectedStatus, string(clientErr.Status))
}

func (s *PaydayIntegTestSuite) bitsEventAnonymousMessageErrorTokenizeAndValidate(userID, channelID string, amount int64, message string, opts *PrismOpts, expectedMessage string) {
	req := s.bitsEventTokenizeAndValidateReq(userID, channelID, amount, message, opts)

	_, err := s.bitsPrismTenantTwirpClient.TokenizeAndValidateMessage(s.ctx, req)
	s.Require().Error(err)

	twirpErr, ok := err.(twirp.Error)
	s.Require().True(ok, "not a twirp error")
	s.Require().Equal(twirp.InvalidArgument, twirpErr.Code())

	var clientErr prism_models.InvalidAnonymousMessageErrorData
	parseErr := prism_models.ParseClientError(twirpErr, &clientErr)
	s.Require().NoError(parseErr)

	s.Require().Equal(clientErr.AnonymizedMessage, expectedMessage)
}

func (s *PaydayIntegTestSuite) bitsEventErrorProcessError(userID, channelID string, amount int64, message string, opts *PrismOpts, expectedStatus string) {
	req := s.bitsEventProcessReq(userID, channelID, amount, message, opts)
	_, err := s.bitsPrismTenantTwirpClient.ProcessMessage(s.ctx, req)
	s.Require().Error(err)

	twirpErr, ok := err.(twirp.Error)
	s.Require().True(ok, "not a twirp error")

	var clientErr apimodel.CreateEventErrorResponse
	parseErr := prism_models.ParseClientError(twirpErr, &clientErr)
	s.Require().NoError(parseErr)

	s.Require().Equal(expectedStatus, string(clientErr.Status))
}

func (s *PaydayIntegTestSuite) bitsEventErrorPostProcessError(userID, channelID string, amount int64, messageID string, message string, opts *PrismOpts, expectedStatus string) {
	req := s.bitsEventPostProcessReq(userID, channelID, amount, messageID, message, opts)
	_, err := s.bitsPrismTenantTwirpClient.PostProcessMessage(s.ctx, req)
	s.Require().Error(err)

	twirpErr, ok := err.(twirp.Error)
	s.Require().True(ok, "not a twirp error")

	var clientErr apimodel.CreateEventErrorResponse
	parseErr := prism_models.ParseClientError(twirpErr, &clientErr)
	s.Require().NoError(parseErr)

	s.Require().Equal(expectedStatus, string(clientErr.Status))
}

func (s *PaydayIntegTestSuite) setOptOut(userId string, optOut bool) {
	request := &apimodel.SetOptOutRequest{
		OptedOut:   optOut,
		Annotation: "test",
	}
	err := s.paydayClient.SetOptOut(s.ctx, userId, request, nil)
	s.Require().NoError(err)
}

func (s *PaydayIntegTestSuite) setBanned(userId string, banned bool) {
	request := &apimodel.SetBannedRequest{
		Banned:     banned,
		Annotation: "test",
	}
	err := s.paydayClient.SetBanned(s.ctx, userId, request, nil)
	s.Require().NoError(err)
}

func (s *PaydayIntegTestSuite) addBitsRequest(userId string, numBits int32) *apimodel.AddBitsRequest {
	request := &apimodel.AddBitsRequest{
		AmountOfBitsToAdd: numBits,
		TypeOfBitsToAdd:   "CB_0",
		EventId:           uuid.New(),
		TwitchUserId:      userId,
		AdminUserId:       testAdmin,
		AdminReason:       "integration_test",
	}
	return request
}

func (s *PaydayIntegTestSuite) addBits(userId string, numBits int32) *apimodel.Event {
	request := s.addBitsRequest(userId, numBits)
	err := s.paydayClient.AddBits(s.ctx, request, nil)
	s.Require().NoError(err)
	return s.createEvent(request.EventId, userId, userId, numBits)
}

func (s *PaydayIntegTestSuite) removeBitsRequest(userId string, numBits int32) *apimodel.RemoveBitsRequest {
	request := &apimodel.RemoveBitsRequest{
		AmountOfBitsToRemove: numBits,
		EventId:              uuid.New(),
		TwitchUserId:         userId,
		AdminUserId:          testAdmin,
		AdminReason:          "integration_test",
	}
	return request
}

func (s *PaydayIntegTestSuite) removeBits(userId string, numBits int32) *apimodel.Event {
	request := s.removeBitsRequest(userId, numBits)
	err := s.paydayClient.RemoveBits(s.ctx, request, nil)
	s.Require().NoError(err)
	return s.createEvent(request.EventId, userId, userId, -numBits)
}

func (s *PaydayIntegTestSuite) removeBitsError(userId string, numBits int32) {
	request := s.removeBitsRequest(userId, numBits)
	err := s.paydayClient.RemoveBits(s.ctx, request, nil)
	s.Require().Error(err)
}

//Deprecated, please use getRandomTestUser and provide some context for the logs
func (s *PaydayIntegTestSuite) getRandomTesUser() string {
	userId := test.RandomTestUserId()
	s.Require().True(user.IsATestAccount(userId), "Must be a valid test userId")
	log.Infof("Testing for random test user %s", userId)
	return userId
}

func (s *PaydayIntegTestSuite) getRandomTestUser(testName string) string {
	userId := test.RandomTestUserId()
	s.Require().True(user.IsATestAccount(userId), "Must be a valid test userId")
	log.Infof("Testing %s with random test user %s", testName, userId)
	return userId
}

func (s *PaydayIntegTestSuite) getRandomCheerMsg(amount int) string {
	return "cheer" + strconv.Itoa(amount) + " " + uuid.New()
}

func (s *PaydayIntegTestSuite) validateBalanceAndEvents(userId string, expectedBalance int32, expectedEvents ...*apimodel.Event) {
	s.validateBalance(userId, expectedBalance)
	s.validateEvents(userId, expectedEvents...)
}

func (s *PaydayIntegTestSuite) validateBalance(userId string, expectedBalance int32) *apimodel.GetBalanceResponse {
	response, err := s.paydayClient.GetBalance(s.ctx, userId, "", nil)
	s.Require().NoError(err)
	s.Require().Equal(expectedBalance, response.Balance)
	return response
}

func (s *PaydayIntegTestSuite) validateVisageBalance(userId string, expectedBalance int32) {
	response, err := s.paydayClient.GetBalance(s.ctx, userId, "", nil)
	s.Require().NoError(err)
	s.Require().Equal(expectedBalance, response.Balance)
}

func (s *PaydayIntegTestSuite) eventsToEventMap(events []*apimodel.Event) map[string]*apimodel.Event {
	eventMap := make(map[string]*apimodel.Event)
	for _, event := range events {
		eventMap[event.EventId] = event
	}
	return eventMap
}

func (s *PaydayIntegTestSuite) validateEvents(userId string, expectedEvents ...*apimodel.Event) {
	response, err := s.paydayClient.GetUserEvents(s.ctx, userId, "", int64(1000), nil)
	s.Require().NoError(err)

	for event := range expectedEvents {
		s.Require().NotNil(event, "Received a nil event for user %s", userId)
	}

	expectedEventsMap := s.eventsToEventMap(expectedEvents)
	s.validateEventsResponse(expectedEventsMap, *response)
}

func (s *PaydayIntegTestSuite) validateEventsResponse(expectedEvents map[string]*apimodel.Event, actualResponse apimodel.UserEventsResponse) {
	for _, expectedEvent := range actualResponse.Events {
		actualEvent, found := expectedEvents[expectedEvent.EventId]
		s.Require().True(found)
		s.validateEvent(expectedEvent, *actualEvent)
	}
}

func (s *PaydayIntegTestSuite) validateEvent(expectedEvent apimodel.Event, actualEvent apimodel.Event) {
	expectedTime := expectedEvent.Timestamp
	actualTime := actualEvent.Timestamp

	expectedEvent.Timestamp = time.Unix(0, 0)
	actualEvent.Timestamp = time.Unix(0, 0)

	s.Require().Equal(expectedEvent, actualEvent)
	s.Require().True(timeUtils.IsTimeInBiDirectionalDelta(actualTime, expectedTime, acceptableTimeDelta))
}

func (s *PaydayIntegTestSuite) onboard(channelId string) {
	onboarded := true

	err := s.paydayClient.UpdateChannelSettings(s.ctx, channelId, apimodel.UpdateSettingsRequest{
		Onboarded: &onboarded,
	}, nil)

	s.Require().NoError(err)
}

func (s *PaydayIntegTestSuite) statusCode(errIn error) int {
	switch err := errIn.(type) {
	case *twitchclient.Error:
		return err.StatusCode
	}
	return 0
}

func (s *PaydayIntegTestSuite) getChannelInfo(channelId string, expectStatus int) apimodel.ChannelEligibleResponse {
	resp, err := s.paydayClient.GetChannelInfo(s.ctx, channelId, nil)
	s.Require().NoError(err)
	return *resp
}

func (s *PaydayIntegTestSuite) entitleBitsByAmount(tuid string, bitsAmount int32) apimodel.EntitleBitsResponse {
	request := &apimodel.EntitleBitsRequest{
		TwitchUserId:  tuid,
		TransactionId: uuid.New(),
		BitAmount:     bitsAmount,
		// Hardcode these two, because it's safe.
		BitsType: string(models.CB_0),
		Platform: string(models.FORTUNA),
	}

	resp, err := s.entitleBits(request)
	s.Require().NoError(err)
	s.Require().NotNil(resp)
	return *resp
}

func (s *PaydayIntegTestSuite) entitleBitsBySku(tuid string, platform models.Platform, sku string) apimodel.EntitleBitsResponse {
	request := &apimodel.EntitleBitsRequest{
		TwitchUserId:  tuid,
		TransactionId: uuid.New(),
		ProductId:     sku,
		Platform:      string(platform),
	}

	resp, err := s.entitleBits(request)
	s.Require().NoError(err)
	s.Require().NotNil(resp)
	return *resp
}

func (s *PaydayIntegTestSuite) entitleBits(request *apimodel.EntitleBitsRequest) (*apimodel.EntitleBitsResponse, error) {
	resp, err := s.paydayClient.EntitleBits(s.ctx, request, nil)
	return resp, err
}
