// +build integration

package integration

import (
	"context"

	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/models/api"
)

const (
	numActionsCalls      = 10
	expectedNumBaseTiers = 5
)

func (s *PaydayIntegTestSuite) TestGetActions_NoChannelIncludeUpperTiers() {
	ctx := context.Background()

	var numActions *int
	for i := 0; i < numActionsCalls; i++ {
		resp, err := s.paydayClient.GetActions(ctx, api.GetActionsRequest{
			IncludeUpperTiers: true,
		}, nil)
		s.Require().NoError(err)
		s.Require().NotNil(resp)

		if numActions == nil {
			numActions = pointers.IntP(len(resp.Actions))
			s.Require().True(*numActions > 0)
		} else {
			s.Require().True(*numActions == len(resp.Actions))
		}

		for _, action := range resp.Actions {
			s.Require().True(len(action.Tiers) >= expectedNumBaseTiers)
		}
	}
}

func (s *PaydayIntegTestSuite) TestGetActions_NoChannelDontIncludeUpperTiers() {
	ctx := context.Background()

	var numActions *int
	for i := 0; i < numActionsCalls; i++ {
		resp, err := s.paydayClient.GetActions(ctx, api.GetActionsRequest{
			IncludeUpperTiers: false,
		}, nil)
		s.Require().NoError(err)
		s.Require().NotNil(resp)

		if numActions == nil {
			numActions = pointers.IntP(len(resp.Actions))
			s.Require().True(*numActions > 0)
		} else {
			s.Require().True(*numActions == len(resp.Actions))
		}

		for _, action := range resp.Actions {
			s.Require().True(len(action.Tiers) == expectedNumBaseTiers)
		}
	}
}

func (s *PaydayIntegTestSuite) TestGetActions_Channel() {
	ctx := context.Background()

	var numActions *int
	for i := 0; i < numActionsCalls; i++ {
		resp, err := s.paydayClient.GetActions(ctx, api.GetActionsRequest{
			IncludeUpperTiers: true,
			ChannelId:         models.QA_BITS_PARTNER_TUID,
		}, nil)
		s.Require().NoError(err)
		s.Require().NotNil(resp)

		if numActions == nil {
			numActions = pointers.IntP(len(resp.Actions))
			s.Require().True(*numActions > 0)
		} else {
			s.Require().True(*numActions == len(resp.Actions))
		}

		for _, action := range resp.Actions {
			s.Require().True(len(action.Tiers) >= expectedNumBaseTiers)
		}
	}
}
