// +build integration

package integration

import (
	"fmt"
	"time"

	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"github.com/pborman/uuid"
)

func (s *PaydayIntegTestSuite) TestAdminAddBalanceUpdate() {
	userId := s.getRandomTestUser("TestAdminAddBalanceUpdate")
	s.validateBalance(userId, 0)

	s.addBits(userId, 1)
	s.validateBalance(userId, 1)
}

func (s *PaydayIntegTestSuite) TestCheerWithBitsToBroadcaster() {
	userId := s.getRandomTestUser("TestCheerWithBitsToBroadcaster (user)")
	channelId := s.getRandomTestUser("TestCheerWithBitsToBroadcaster (channel)")
	transactionId := uuid.New()
	numOfBits := int32(86)
	s.onboard(channelId)

	s.addBits(userId, numOfBits)
	msg := fmt.Sprintf("cheer%d %s", numOfBits, transactionId)

	s.bitsEventProcess(userId, channelId, int64(numOfBits), msg, nil)

	time.Sleep(30 * time.Second) // wait for pantheon to process the event, is takes a while these days.
	resp, err := s.twirpClient.GetBitsToBroadcaster(s.ctx, &paydayrpc.GetBitsToBroadcasterReq{
		UserId:    userId,
		ChannelId: channelId,
	})
	s.Require().NoError(err)
	s.Require().Equal(numOfBits, resp.GetBitsToBroadcaster())
}
