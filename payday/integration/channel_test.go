// +build integration

package integration

func (s *PaydayIntegTestSuite) TestChannelInfo() {
	response := s.getChannelInfo(testRealPartnerOptedIn, 200)
	s.Require().Equal(true, response.Eligible)
}
