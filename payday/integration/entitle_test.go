// +build integration

package integration

import (
	"time"

	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/models/api"
	"github.com/pborman/uuid"
)

func (s *PaydayIntegTestSuite) TestEntitleBitsByAmount() {
	userId := s.getRandomTestUser("TestEntitleBitsByAmount")
	s.validateBalance(userId, 0)

	s.entitleBitsByAmount(userId, 1)
	s.validateBalance(userId, 1)
}

func (s *PaydayIntegTestSuite) TestEntitleBitsBySku() {
	userId := s.getRandomTestUser("TestEntitleBitsBySku")
	s.validateBalance(userId, 0)

	s.entitleBitsBySku(userId, models.IOS, "tv.twitch.ios.iap.bits.everyday.t2")
	s.validateBalance(userId, 95)
}

func (s *PaydayIntegTestSuite) TestEntitleIdempotent() {
	userId := s.getRandomTestUser("TestEntitleIdempotent")
	transactionId := uuid.New()
	sku := "tv.twitch.ios.iap.bits.everyday.t2"
	s.validateBalance(userId, 0)

	request := &api.EntitleBitsRequest{
		TwitchUserId:  userId,
		TransactionId: transactionId,
		ProductId:     sku,
		Platform:      string(models.IOS),
	}

	response, err := s.entitleBits(request)
	s.Require().NoError(err)
	s.Require().NotNil(response)

	// DOUBLE VALIDATE
	s.Require().Equal(int32(95), response.BitsBalance)
	s.validateBalance(userId, 95)

	response, err = s.entitleBits(request)
	s.Require().NoError(err)
	s.Require().NotNil(response)

	s.Require().Equal(int32(95), response.BitsBalance)
	s.validateBalance(userId, 95)
}

func (s *PaydayIntegTestSuite) TestEntitleIdempotentInvalid() {
	userId := s.getRandomTestUser("TestEntitleIdempotentInvalid")
	transactionId := uuid.New()
	sku := "tv.twitch.ios.iap.bits.everyday.t2"
	s.validateBalance(userId, 0)

	request := &api.EntitleBitsRequest{
		TwitchUserId:  userId,
		TransactionId: transactionId,
		ProductId:     sku,
		Platform:      string(models.IOS),
	}

	response, err := s.entitleBits(request)
	s.Require().NoError(err)
	s.Require().NotNil(response)

	// DOUBLE VALIDATE
	s.Require().Equal(int32(95), response.BitsBalance)
	s.validateBalance(userId, 95)

	request = &api.EntitleBitsRequest{
		TwitchUserId:  userId,
		TransactionId: transactionId,
		ProductId:     sku,
		Platform:      string(models.ANDROID),
	}

	response, err = s.entitleBits(request)
	s.Require().Nil(response)
	s.Require().Error(err, "Duplicate transaction id for non-duplicate request")
	s.validateBalance(userId, 95)
}

func (s *PaydayIntegTestSuite) TestEntitlePromoRemoveFromBalance() {
	userId := s.getRandomTestUser("TestEntitlePromoRemoveFromBalance")
	transactionId := uuid.New()
	promoSku := "B019WMZ15D" // should be the $3 for 300 promo (in all environments).

	s.Require().True(s.containsProduct(userId, promoSku))

	request := &api.EntitleBitsRequest{
		TwitchUserId:  userId,
		TransactionId: transactionId,
		ProductId:     promoSku,
		Platform:      string(models.PAYPAL),
	}

	entitleResponse, err := s.entitleBits(request)
	s.Require().NoError(err)
	s.Require().NotNil(entitleResponse)
	s.Require().Equal(int32(300), entitleResponse.BitsBalance)

	// I noticed a little inconsistency here, which seems to be based on redis read replicas not having the delete
	// propagated, so I'll just wait a sec to make this test consistent.
	time.Sleep(1 * time.Second)

	s.Require().False(s.containsProduct(userId, promoSku))

	// in the near future when universal checkout is complete this is going to be deprecated functionality, so because
	// of that I didn't add a new API for it AND left a special section at the bottom of this test for it's functionality
	// that will be low cost to remove when it's no longer our thing to maintain.
	balanceResp, err := s.paydayClient.GetBalance(s.ctx, userId, "", nil)
	s.Require().NoError(err)
	s.Require().NotNil(balanceResp)
	s.Require().False(balanceResp.Bundles[promoSku])
	s.Require().Equal(string(models.PAYPAL), balanceResp.MostRecentPurchaseWebPlatform)
}

func (s *PaydayIntegTestSuite) containsProduct(userId, productSKU string) bool {
	response, err := s.paydayClient.GetBitsProducts(s.ctx, userId, "US", "en", "PAYPAL", nil)
	s.Require().NoError(err)
	for _, product := range response {
		if product.Id == productSKU {
			return true
		}
	}
	return false
}
