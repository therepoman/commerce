// +build integration

package integration

import (
	"time"

	"code.justin.tv/commerce/payday/models/api"
	prism "code.justin.tv/commerce/prism/rpc"
	"code.justin.tv/commerce/prism/rpc/models"
	"github.com/pborman/uuid"
	"github.com/twitchtv/twirp"
)

func (s *PaydayIntegTestSuite) TestPrismCannotGiveToNonPartner() {
	s.bitsEventErrorTokenizeAndValidate(s.getRandomTesUser(), testRealNonPartner, 1, "cheer1", nil, string(api.ChannelIneligible))
}

func (s *PaydayIntegTestSuite) TestPrismCannotGiveToOptedOutPartner() {
	s.setOptOut(testRealPartnerOptedOut, true)
	s.bitsEventErrorTokenizeAndValidate(s.getRandomTesUser(), testRealPartnerOptedOut, 1, "cheer1", nil, string(api.ChannelIneligible))
}

func (s *PaydayIntegTestSuite) TestPrismCannotGiveWithEmptyBalance() {
	channelId := s.getRandomTesUser()
	s.onboard(channelId)
	s.bitsEventErrorProcessError(s.getRandomTesUser(), channelId, 1, "cheer1", nil, string(api.InsufficientBalance))
}

func (s *PaydayIntegTestSuite) TestPrismCannotGiveAsBannedUser() {
	userId := s.getRandomTesUser()
	s.setBanned(userId, true)
	channelId := s.getRandomTesUser()
	s.onboard(channelId)
	s.bitsEventErrorTokenizeAndValidate(userId, channelId, 1, "cheer1", nil, string(api.UserIneligible))
}

func (s *PaydayIntegTestSuite) TestPrismCannotGiveWithInvalidMessage() {
	userId, channelId := s.getRandomTesUser(), s.getRandomTesUser()
	s.onboard(channelId)

	req := s.bitsEventTokenizeAndValidateReq(userId, channelId, 1, "chairBits1", nil)

	_, err := s.bitsPrismTenantTwirpClient.TokenizeAndValidateMessage(s.ctx, req)
	s.Require().Error(err)

	twirpErr, ok := err.(twirp.Error)
	s.Require().True(ok, "not a twirp error")
	s.Require().Equal(twirp.InvalidArgument, twirpErr.Code())
}

func (s *PaydayIntegTestSuite) TestPrismCannotUseAnonCheermoteNormally() {
	userId, channelId := s.getRandomTesUser(), s.getRandomTesUser()
	s.onboard(channelId)
	s.bitsEventErrorTokenizeAndValidate(userId, channelId, 1, "anon1", nil, string(api.AnonymousCheermoteNotAllowed))
}

func (s *PaydayIntegTestSuite) TestPrismCannotUseRegularCheermotesForAnonMessage() {
	userId, channelId := s.getRandomTesUser(), s.getRandomTesUser()
	s.onboard(channelId)
	s.bitsEventAnonymousMessageErrorTokenizeAndValidate(userId, channelId, 1, "cheer1", &PrismOpts{IsAnon: true}, "anon1")
}

func (s *PaydayIntegTestSuite) TestPrismCannotUseRegularTextForAnonMessage() {
	userId, channelId := s.getRandomTesUser(), s.getRandomTesUser()
	s.onboard(channelId)
	s.bitsEventAnonymousMessageErrorTokenizeAndValidate(userId, channelId, 1, "anon1 coregamers", &PrismOpts{IsAnon: true}, "anon1")
}

func (s *PaydayIntegTestSuite) TestPrismCanUseAnonCheermoteWithAnonMessage() {
	userId, channelId := s.getRandomTesUser(), s.getRandomTesUser()
	s.onboard(channelId)
	t1 := s.addBits(userId, 1)

	tokenizeAndValidateMessageResp := s.bitsEventTokenizeAndValidate(userId, channelId, 1, "anon1", &PrismOpts{IsAnon: true})
	s.Require().Equal(int64(1), tokenizeAndValidateMessageResp.ConsumableAmounts[models.Consumable_Bits])
	s.Require().Equal(&prism.Token{Text: "anon1", Amount: int64(1)}, tokenizeAndValidateMessageResp.Tokens[0])
	s.validateEvents(userId, t1)
}

func (s *PaydayIntegTestSuite) TestPrismTokenizeAndValidate() {
	userId, channelId := s.getRandomTesUser(), s.getRandomTesUser()
	s.onboard(channelId)
	t1 := s.addBits(userId, 1024)

	tokenizeAndValidateMessageResp := s.bitsEventTokenizeAndValidate(userId, channelId, 256, "cheer256", nil)
	s.Require().Equal(int64(256), tokenizeAndValidateMessageResp.ConsumableAmounts[models.ConsumableType_Bits])
	s.Require().Equal(&prism.Token{Text: "cheer256", Amount: int64(256)}, tokenizeAndValidateMessageResp.Tokens[0])
	s.validateEvents(userId, t1)
}

func (s *PaydayIntegTestSuite) TestPrismProcessMessage() {
	userId, channelId := s.getRandomTesUser(), s.getRandomTesUser()
	s.onboard(channelId)
	t1 := s.addBits(userId, 1024)

	response, event := s.bitsEventProcess(userId, channelId, 256, "cheer256", nil)
	s.Require().Equal(int64(256), response.ConsumableAmountsProcessed[models.ConsumableType_Bits])
	s.Require().Equal(int64(1024)-int64(256), response.NewBalance[models.ConsumableType_Bits])

	s.validateBalanceAndEvents(userId, int32(1024)-int32(256), t1, event)
}

func (s *PaydayIntegTestSuite) TestPrismPostProcessMessage() {
	userId, channelId := s.getRandomTesUser(), s.getRandomTesUser()
	s.onboard(channelId)
	s.addBits(userId, 1024)

	postProcessMessageResp := s.bitsEventPostProcess(userId, channelId, 256, uuid.New(), "cheer256", &PrismOpts{
		SentMessage: "cheer256",
	})
	s.Require().NotNil(postProcessMessageResp)
}

func (s *PaydayIntegTestSuite) TestPrismManyBitsEvents() {
	userId, channelId := s.getRandomTesUser(), s.getRandomTesUser()
	s.onboard(channelId)
	t1 := s.addBits(userId, 1000)

	response, t2 := s.bitsEventProcess(userId, channelId, 100, "cheer100", nil)
	s.Require().Equal(int64(100), response.ConsumableAmountsProcessed[models.ConsumableType_Bits])
	s.Require().Equal(int64(900), response.NewBalance[models.ConsumableType_Bits])
	time.Sleep(1 * time.Second)

	response, t3 := s.bitsEventProcess(userId, channelId, 200, "cheer200", nil)
	s.Require().Equal(int64(200), response.ConsumableAmountsProcessed[models.ConsumableType_Bits])
	s.Require().Equal(int64(700), response.NewBalance[models.ConsumableType_Bits])
	time.Sleep(1 * time.Second)

	response, t4 := s.bitsEventProcess(userId, channelId, 300, "cheer300", nil)
	s.Require().Equal(int64(300), response.ConsumableAmountsProcessed[models.ConsumableType_Bits])
	s.Require().Equal(int64(400), response.NewBalance[models.ConsumableType_Bits])
	time.Sleep(1 * time.Second)

	response, t5 := s.bitsEventProcess(userId, channelId, 400, "cheer400", nil)
	s.Require().Equal(int64(400), response.ConsumableAmountsProcessed[models.ConsumableType_Bits])
	s.Require().Equal(int64(0), response.NewBalance[models.ConsumableType_Bits])

	s.validateBalanceAndEvents(userId, 0, t1, t2, t3, t4, t5)
}

func (s *PaydayIntegTestSuite) TestPrismRealPartnerBitsEvent() {
	s.setOptOut(testRealPartnerOptedIn, false)
	userId := s.getRandomTesUser()
	t1 := s.addBits(userId, 1000)
	response, event := s.bitsEventProcess(userId, testRealPartnerOptedIn, 1000, "cheer1000", nil)
	s.Require().Equal(int64(1000), response.ConsumableAmountsProcessed[models.ConsumableType_Bits])
	s.Require().Equal(int64(0), response.NewBalance[models.ConsumableType_Bits])
	s.validateBalanceAndEvents(userId, 0, event, t1)
}
