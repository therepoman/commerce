// +build integration

package integration

import (
	"context"

	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/payday/badgetiers"
	"code.justin.tv/commerce/payday/models/api"
)

func (s *PaydayIntegTestSuite) TestGetBadges() {
	channelID := s.getRandomTesUser()
	s.onboard(channelID)
	ctx := context.Background()
	getBadgesResponse, err := s.paydayClient.GetBadgeTiers(ctx, channelID, nil)
	s.Require().NoError(err)
	s.Require().NotNil(getBadgesResponse)

	s.Require().Equal(len(getBadgesResponse.Tiers), len(badgetiers.BitsChatBadgeTiers))
	for _, tier := range getBadgesResponse.Tiers {
		s.Require().Equal(badgetiers.BitsChatBadgeTiers[tier.Threshold].Enabled, tier.Enabled)
		s.Require().Equal("", tier.Title)
		s.Require().Equal("", tier.ImageURL1x)
		s.Require().Equal(int64(0), tier.UnlockedUsersCount)
		s.Require().Nil(tier.Emoticons)
		s.Require().Nil(tier.EmoticonUploadConfigurations)
	}
}

func (s *PaydayIntegTestSuite) TestSetBadges_DisableATier() {
	channelID := s.getRandomTesUser()
	s.onboard(channelID)
	ctx := context.Background()

	params := api.SetBadgesRequest{
		Tiers: []*api.BadgeTierSetting{
			{
				Enabled:   pointers.BoolP(false),
				Threshold: 5000000,
			},
		},
	}

	getBadgesResponse, err := s.paydayClient.SetBadgeTiers(ctx, channelID, params, nil)
	s.Require().NoError(err)
	s.Require().NotNil(getBadgesResponse)

	s.Require().Equal(len(getBadgesResponse.Tiers), 1)
	for _, tier := range getBadgesResponse.Tiers {
		s.Require().Equal(false, tier.Enabled)
		s.Require().Equal("", tier.Title)
		s.Require().Equal("", tier.ImageURL1x)
		s.Require().Equal(int64(0), tier.UnlockedUsersCount)
		s.Require().Nil(tier.Emoticons)
		s.Require().Nil(tier.EmoticonUploadConfigurations)
	}
}

func (s *PaydayIntegTestSuite) TestSetBadges_EnableATier() {
	channelID := s.getRandomTesUser()
	s.onboard(channelID)
	ctx := context.Background()

	params := api.SetBadgesRequest{
		Tiers: []*api.BadgeTierSetting{
			{
				Enabled:   pointers.BoolP(true),
				Threshold: 5000000,
			},
		},
	}

	getBadgesResponse, err := s.paydayClient.SetBadgeTiers(ctx, channelID, params, nil)
	s.Require().NoError(err)
	s.Require().NotNil(getBadgesResponse)

	s.Require().Equal(len(getBadgesResponse.Tiers), 1)
	for _, tier := range getBadgesResponse.Tiers {
		s.Require().Equal(true, tier.Enabled)
		s.Require().Equal("", tier.Title)
		s.Require().Equal("", tier.ImageURL1x)
		s.Require().Equal(int64(0), tier.UnlockedUsersCount)
		s.Require().Nil(tier.Emoticons)
		s.Require().Nil(tier.EmoticonUploadConfigurations)
	}
}
