// +build integration

package integration

import (
	"code.justin.tv/commerce/payday/models/api"
)

const cor = "US"

func (s *PaydayIntegTestSuite) TestGetProducts() {
	products, err := s.paydayClient.GetBitsProducts(s.ctx, andymcId, cor, "EN", "IOS", nil)
	s.Require().NoError(err)
	s.Require().NotEmpty(products)
}

func (s *PaydayIntegTestSuite) TestGetBitsPaypalPrices() {
	products, err := s.paydayClient.GetPrices(s.ctx, api.GetPricesRequest{
		UserID:             andymcId,
		Platform:           "PAYPAL",
		CountryOfResidence: cor,
		ASINs:              []string{},
	}, nil)
	s.Require().NoError(err)
	s.Require().NotEmpty(products)
}
