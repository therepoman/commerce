// +build integration

package integration

import (
	prism_models "code.justin.tv/commerce/prism/rpc/models"
	prism_tenant "code.justin.tv/commerce/prism/rpc/tenant"
	"github.com/twitchtv/twirp"
)

func (s *PaydayIntegTestSuite) TestTokenizeAndValidateSuccess() {
	userId, channelId := s.getRandomTesUser(), s.getRandomTesUser()
	s.onboard(channelId)
	s.addBits(userId, 20)

	req := &prism_tenant.TokenizeAndValidateMessageReq{
		UserId:    userId,
		ChannelId: channelId,
		Message:   "cheer10 bro",
		MessageId: "test-message-id",
		ConsumableAmounts: map[string]int64{
			prism_models.ConsumableType_Bits: int64(10),
		},
	}
	res, err := s.bitsPrismTenantTwirpClient.TokenizeAndValidateMessage(s.ctx, req)
	s.Require().NoError(err)
	s.Require().Equal(int64(10), res.ConsumableAmounts[prism_models.ConsumableType_Bits])
}

func (s *PaydayIntegTestSuite) TestTokenizeAndValidateBitsAmountMismatch() {
	userId, channelId := s.getRandomTesUser(), s.getRandomTesUser()
	s.onboard(channelId)

	req := &prism_tenant.TokenizeAndValidateMessageReq{
		UserId:    userId,
		ChannelId: channelId,
		Message:   "cheer10 bro",
		MessageId: "test-message-id",
		ConsumableAmounts: map[string]int64{
			prism_models.ConsumableType_Bits: int64(420),
		},
	}
	_, err := s.bitsPrismTenantTwirpClient.TokenizeAndValidateMessage(s.ctx, req)
	s.Require().Error(err)
}

func (s *PaydayIntegTestSuite) TestTokenizeAndValidateBitsInvalidToken() {
	userId, channelId := s.getRandomTesUser(), s.getRandomTesUser()
	s.onboard(channelId)

	req := &prism_tenant.TokenizeAndValidateMessageReq{
		UserId:    userId,
		ChannelId: channelId,
		Message:   "bonus100 hey",
		MessageId: "test-message-id",
		ConsumableAmounts: map[string]int64{
			prism_models.ConsumableType_Bits: int64(100),
		},
	}
	_, err := s.bitsPrismTenantTwirpClient.TokenizeAndValidateMessage(s.ctx, req)
	s.Require().Error(err)

	twirpErr, ok := err.(twirp.Error)
	s.Require().True(ok, "not a twirp error")
	s.Require().Equal(twirp.InvalidArgument, twirpErr.Code())
}
