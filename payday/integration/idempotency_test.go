// +build integration

package integration

import (
	"code.justin.tv/commerce/payday/models/api"
	"github.com/pborman/uuid"
)

func (s *PaydayIntegTestSuite) TestAdminAddBitsIdempotency() {
	userId := s.getRandomTestUser("TestAdminAddBitsIdempotency")

	request := s.addBitsRequest(userId, 1024)

	// First add bits request
	err := s.paydayClient.AddBits(s.ctx, request, nil)
	s.Require().NoError(err)

	// Second add bits request succeeds (with the same parameters)
	err = s.paydayClient.AddBits(s.ctx, request, nil)
	s.Require().NoError(err)

	// Third add bits request fails (with different parameters)
	request.AmountOfBitsToAdd = 2048
	err = s.paydayClient.AddBits(s.ctx, request, nil)
	s.Require().Error(err)

	// Get balance only reflects one add bits event
	s.validateBalance(userId, 1024)
}

func (s *PaydayIntegTestSuite) TestAdminRemoveBitsIdempotency() {
	userId := s.getRandomTestUser("TestAdminRemoveBitsIdempotency")

	request := api.RemoveBitsRequest{
		AmountOfBitsToRemove: 512,
		EventId:              uuid.New(),
		TwitchUserId:         userId,
		AdminUserId:          testAdmin,
		AdminReason:          "integration_test",
	}

	// give them some bits
	s.addBits(userId, 1024)

	// First remove bits request
	err := s.paydayClient.RemoveBits(s.ctx, &request, nil)
	s.Require().NoError(err)

	// Second remove bits request succeeds (with the same parameters)
	err = s.paydayClient.RemoveBits(s.ctx, &request, nil)
	s.Require().NoError(err)

	// Third remove bits request fails (with different parameters)
	request.AmountOfBitsToRemove = 2048
	err = s.paydayClient.RemoveBits(s.ctx, &request, nil)
	s.Require().Error(err)

	// Get balance only reflects one remove bits event
	s.validateBalance(userId, 512)
}
