// +build integration

package integration

import (
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"github.com/pborman/uuid"
)

func (s *PaydayIntegTestSuite) TestUseBitsOnPolls_HappyPath() {
	userId, channelId := s.getRandomTesUser(), s.getRandomTesUser()
	s.onboard(channelId)
	s.entitleBitsByAmount(userId, 10)
	pollId := "test_poll_id"
	choiceId := "test_choice_id"

	balanceResp, err := s.twirpClient.UseBitsOnPoll(s.ctx, &paydayrpc.UseBitsOnPollReq{
		VoteId:     uuid.New(),
		UserId:     userId,
		ChannelId:  channelId,
		BitsAmount: 1,
		PollId:     pollId,
		ChoiceId:   choiceId,
	})
	s.Require().NoError(err)
	s.Require().NotEmpty(balanceResp)
	s.Require().Equal(int32(9), balanceResp.Balance)
	s.validateBalance(userId, 9)
}

func (s *PaydayIntegTestSuite) TestUseBitsOnPolls_NotEnoughBits() {
	userId, channelId := s.getRandomTesUser(), s.getRandomTesUser()
	s.onboard(channelId)
	pollId := "test_poll_id"
	choiceId := "test_choice_id"

	balanceResp, err := s.twirpClient.UseBitsOnPoll(s.ctx, &paydayrpc.UseBitsOnPollReq{
		VoteId:     uuid.New(),
		UserId:     userId,
		ChannelId:  channelId,
		BitsAmount: 1,
		PollId:     pollId,
		ChoiceId:   choiceId,
	})
	s.Require().Error(err)
	s.Require().Empty(balanceResp)
}
