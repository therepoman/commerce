package utils

import (
	"testing"
	"time"

	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	. "github.com/smartystreets/goconvey/convey"
)

func TestGetLeaderboardExpirationTime(t *testing.T) {
	Convey("Given a time that a leaderboard event is processed", t, func() {
		loc, err := time.LoadLocation("America/Los_Angeles")
		if err != nil {
			panic(err)
		}

		eventTime := time.Date(2018, 1, 24, 1, 2, 3, 4, loc)

		Convey("Errors for an invalid timeunit", func() {
			timeUnit := pantheonrpc.TimeUnit(12432)

			calculatedTime, err := GetLeaderboardExpirationTime(eventTime, timeUnit)
			So(err, ShouldNotBeNil)
			So(calculatedTime, ShouldBeNil)
		})

		Convey("Returns nil time for an all_time timeunit", func() {
			timeUnit := pantheonrpc.TimeUnit_ALLTIME

			calculatedTime, err := GetLeaderboardExpirationTime(eventTime, timeUnit)
			So(err, ShouldBeNil)
			So(calculatedTime, ShouldBeNil)
		})

		Convey("For a daily timeunit", func() {

			timeUnit := pantheonrpc.TimeUnit_DAY

			Convey("it correctly predicts the next day", func() {
				correctTime := time.Date(2018, 1, 25, 0, 0, 0, 0, loc)

				calculatedTime, err := GetLeaderboardExpirationTime(eventTime, timeUnit)
				So(err, ShouldBeNil)
				So(calculatedTime, ShouldNotBeNil)

				So(calculatedTime.Year(), ShouldEqual, correctTime.Year())
				So(calculatedTime.Month(), ShouldEqual, correctTime.Month())
				So(calculatedTime.Day(), ShouldEqual, correctTime.Day())
				So(calculatedTime.Hour(), ShouldEqual, correctTime.Hour())
				So(calculatedTime.Minute(), ShouldEqual, correctTime.Minute())
				So(calculatedTime.Second(), ShouldEqual, correctTime.Second())
				So(calculatedTime.Nanosecond(), ShouldEqual, correctTime.Nanosecond())
			})

			Convey("it correctly gives the time when date is right before midnight", func() {
				eventTime = time.Date(2018, 1, 28, 11, 59, 59, 0, loc)

				correctTime := time.Date(2018, 1, 29, 0, 0, 0, 0, loc)

				calculatedTime, err := GetLeaderboardExpirationTime(eventTime, timeUnit)
				So(err, ShouldBeNil)
				So(calculatedTime, ShouldNotBeNil)

				So(calculatedTime.Year(), ShouldEqual, correctTime.Year())
				So(calculatedTime.Month(), ShouldEqual, correctTime.Month())
				So(calculatedTime.Day(), ShouldEqual, correctTime.Day())
				So(calculatedTime.Hour(), ShouldEqual, correctTime.Hour())
				So(calculatedTime.Minute(), ShouldEqual, correctTime.Minute())
				So(calculatedTime.Second(), ShouldEqual, correctTime.Second())
				So(calculatedTime.Nanosecond(), ShouldEqual, correctTime.Nanosecond())
			})

			Convey("it correctly gives the time when date is midnight", func() {
				eventTime = time.Date(2018, 1, 29, 0, 1, 0, 0, loc)

				correctTime := time.Date(2018, 1, 30, 0, 0, 0, 0, loc)

				calculatedTime, err := GetLeaderboardExpirationTime(eventTime, timeUnit)
				So(err, ShouldBeNil)
				So(calculatedTime, ShouldNotBeNil)

				So(calculatedTime.Year(), ShouldEqual, correctTime.Year())
				So(calculatedTime.Month(), ShouldEqual, correctTime.Month())
				So(calculatedTime.Day(), ShouldEqual, correctTime.Day())
				So(calculatedTime.Hour(), ShouldEqual, correctTime.Hour())
				So(calculatedTime.Minute(), ShouldEqual, correctTime.Minute())
				So(calculatedTime.Second(), ShouldEqual, correctTime.Second())
				So(calculatedTime.Nanosecond(), ShouldEqual, correctTime.Nanosecond())
			})

			Convey("it correctly gives the time when date is exactly the rotation time", func() {
				eventTime = time.Date(2018, 1, 29, 0, 0, 0, 0, loc)

				correctTime := time.Date(2018, 1, 30, 0, 0, 0, 0, loc)

				calculatedTime, err := GetLeaderboardExpirationTime(eventTime, timeUnit)
				So(err, ShouldBeNil)
				So(calculatedTime, ShouldNotBeNil)

				So(calculatedTime.Year(), ShouldEqual, correctTime.Year())
				So(calculatedTime.Month(), ShouldEqual, correctTime.Month())
				So(calculatedTime.Day(), ShouldEqual, correctTime.Day())
				So(calculatedTime.Hour(), ShouldEqual, correctTime.Hour())
				So(calculatedTime.Minute(), ShouldEqual, correctTime.Minute())
				So(calculatedTime.Second(), ShouldEqual, correctTime.Second())
				So(calculatedTime.Nanosecond(), ShouldEqual, correctTime.Nanosecond())
			})
		})

		Convey("For a weekly timeunit", func() {

			timeUnit := pantheonrpc.TimeUnit_WEEK

			Convey("it correctly predicts the next sunday", func() {
				correctTime := time.Date(2018, 1, 29, 0, 0, 0, 0, loc)

				calculatedTime, err := GetLeaderboardExpirationTime(eventTime, timeUnit)
				So(err, ShouldBeNil)
				So(calculatedTime, ShouldNotBeNil)

				So(calculatedTime.Year(), ShouldEqual, correctTime.Year())
				So(calculatedTime.Month(), ShouldEqual, correctTime.Month())
				So(calculatedTime.Day(), ShouldEqual, correctTime.Day())
				So(calculatedTime.Hour(), ShouldEqual, correctTime.Hour())
				So(calculatedTime.Minute(), ShouldEqual, correctTime.Minute())
				So(calculatedTime.Second(), ShouldEqual, correctTime.Second())
				So(calculatedTime.Nanosecond(), ShouldEqual, correctTime.Nanosecond())
			})

			Convey("it correctly gives the time when date is right before monday", func() {
				eventTime = time.Date(2018, 1, 28, 11, 59, 59, 0, loc)

				correctTime := time.Date(2018, 1, 29, 0, 0, 0, 0, loc)

				calculatedTime, err := GetLeaderboardExpirationTime(eventTime, timeUnit)
				So(err, ShouldBeNil)
				So(calculatedTime, ShouldNotBeNil)

				So(calculatedTime.Year(), ShouldEqual, correctTime.Year())
				So(calculatedTime.Month(), ShouldEqual, correctTime.Month())
				So(calculatedTime.Day(), ShouldEqual, correctTime.Day())
				So(calculatedTime.Hour(), ShouldEqual, correctTime.Hour())
				So(calculatedTime.Minute(), ShouldEqual, correctTime.Minute())
				So(calculatedTime.Second(), ShouldEqual, correctTime.Second())
				So(calculatedTime.Nanosecond(), ShouldEqual, correctTime.Nanosecond())
			})

			Convey("it correctly gives the time when date is monday", func() {
				eventTime = time.Date(2018, 1, 29, 0, 1, 0, 0, loc)

				correctTime := time.Date(2018, 2, 5, 0, 0, 0, 0, loc)

				calculatedTime, err := GetLeaderboardExpirationTime(eventTime, timeUnit)
				So(err, ShouldBeNil)
				So(calculatedTime, ShouldNotBeNil)

				So(calculatedTime.Year(), ShouldEqual, correctTime.Year())
				So(calculatedTime.Month(), ShouldEqual, correctTime.Month())
				So(calculatedTime.Day(), ShouldEqual, correctTime.Day())
				So(calculatedTime.Hour(), ShouldEqual, correctTime.Hour())
				So(calculatedTime.Minute(), ShouldEqual, correctTime.Minute())
				So(calculatedTime.Second(), ShouldEqual, correctTime.Second())
				So(calculatedTime.Nanosecond(), ShouldEqual, correctTime.Nanosecond())
			})

			Convey("it correctly gives the time when date is exactly the rotation time", func() {
				eventTime = time.Date(2018, 1, 29, 0, 0, 0, 0, loc)

				correctTime := time.Date(2018, 2, 5, 0, 0, 0, 0, loc)

				calculatedTime, err := GetLeaderboardExpirationTime(eventTime, timeUnit)
				So(err, ShouldBeNil)
				So(calculatedTime, ShouldNotBeNil)

				So(calculatedTime.Year(), ShouldEqual, correctTime.Year())
				So(calculatedTime.Month(), ShouldEqual, correctTime.Month())
				So(calculatedTime.Day(), ShouldEqual, correctTime.Day())
				So(calculatedTime.Hour(), ShouldEqual, correctTime.Hour())
				So(calculatedTime.Minute(), ShouldEqual, correctTime.Minute())
				So(calculatedTime.Second(), ShouldEqual, correctTime.Second())
				So(calculatedTime.Nanosecond(), ShouldEqual, correctTime.Nanosecond())
			})
		})

		Convey("Tests for a monthly timeunit", func() {
			timeUnit := pantheonrpc.TimeUnit_MONTH

			Convey("it correctly predicts the beginning of the month", func() {
				correctTime := time.Date(2018, 2, 1, 0, 0, 0, 0, loc)

				calculatedTime, err := GetLeaderboardExpirationTime(eventTime, timeUnit)
				So(err, ShouldBeNil)
				So(calculatedTime, ShouldNotBeNil)

				So(calculatedTime.Year(), ShouldEqual, correctTime.Year())
				So(calculatedTime.Month(), ShouldEqual, correctTime.Month())
				So(calculatedTime.Day(), ShouldEqual, correctTime.Day())
				So(calculatedTime.Hour(), ShouldEqual, correctTime.Hour())
				So(calculatedTime.Minute(), ShouldEqual, correctTime.Minute())
				So(calculatedTime.Second(), ShouldEqual, correctTime.Second())
				So(calculatedTime.Nanosecond(), ShouldEqual, correctTime.Nanosecond())
			})

			Convey("it correctly gives the time when date is right before april first", func() {
				eventTime = time.Date(2018, 3, 31, 11, 59, 59, 0, loc)

				correctTime := time.Date(2018, 4, 1, 0, 0, 0, 0, loc)

				calculatedTime, err := GetLeaderboardExpirationTime(eventTime, timeUnit)
				So(err, ShouldBeNil)
				So(calculatedTime, ShouldNotBeNil)

				So(calculatedTime.Year(), ShouldEqual, correctTime.Year())
				So(calculatedTime.Month(), ShouldEqual, correctTime.Month())
				So(calculatedTime.Day(), ShouldEqual, correctTime.Day())
				So(calculatedTime.Hour(), ShouldEqual, correctTime.Hour())
				So(calculatedTime.Minute(), ShouldEqual, correctTime.Minute())
				So(calculatedTime.Second(), ShouldEqual, correctTime.Second())
				So(calculatedTime.Nanosecond(), ShouldEqual, correctTime.Nanosecond())
			})

			Convey("it correctly gives the time when date is april first", func() {
				eventTime = time.Date(2018, 4, 1, 0, 1, 0, 0, loc)

				correctTime := time.Date(2018, 5, 1, 0, 0, 0, 0, loc)

				calculatedTime, err := GetLeaderboardExpirationTime(eventTime, timeUnit)
				So(err, ShouldBeNil)
				So(calculatedTime, ShouldNotBeNil)

				So(calculatedTime.Year(), ShouldEqual, correctTime.Year())
				So(calculatedTime.Month(), ShouldEqual, correctTime.Month())
				So(calculatedTime.Day(), ShouldEqual, correctTime.Day())
				So(calculatedTime.Hour(), ShouldEqual, correctTime.Hour())
				So(calculatedTime.Minute(), ShouldEqual, correctTime.Minute())
				So(calculatedTime.Second(), ShouldEqual, correctTime.Second())
				So(calculatedTime.Nanosecond(), ShouldEqual, correctTime.Nanosecond())
			})

			Convey("it correctly gives the time when date is exactly the rotation time", func() {
				eventTime = time.Date(2018, 4, 1, 0, 0, 0, 0, loc)

				correctTime := time.Date(2018, 5, 1, 0, 0, 0, 0, loc)

				calculatedTime, err := GetLeaderboardExpirationTime(eventTime, timeUnit)
				So(err, ShouldBeNil)
				So(calculatedTime, ShouldNotBeNil)

				So(calculatedTime.Year(), ShouldEqual, correctTime.Year())
				So(calculatedTime.Month(), ShouldEqual, correctTime.Month())
				So(calculatedTime.Day(), ShouldEqual, correctTime.Day())
				So(calculatedTime.Hour(), ShouldEqual, correctTime.Hour())
				So(calculatedTime.Minute(), ShouldEqual, correctTime.Minute())
				So(calculatedTime.Second(), ShouldEqual, correctTime.Second())
				So(calculatedTime.Nanosecond(), ShouldEqual, correctTime.Nanosecond())
			})

			Convey("it correctly gives the time when date is in December", func() {
				eventTime = time.Date(2018, 12, 5, 0, 0, 0, 0, loc)

				correctTime := time.Date(2019, 1, 1, 0, 0, 0, 0, loc)

				calculatedTime, err := GetLeaderboardExpirationTime(eventTime, timeUnit)
				So(err, ShouldBeNil)
				So(calculatedTime, ShouldNotBeNil)

				So(calculatedTime.Year(), ShouldEqual, correctTime.Year())
				So(calculatedTime.Month(), ShouldEqual, correctTime.Month())
				So(calculatedTime.Day(), ShouldEqual, correctTime.Day())
				So(calculatedTime.Hour(), ShouldEqual, correctTime.Hour())
				So(calculatedTime.Minute(), ShouldEqual, correctTime.Minute())
				So(calculatedTime.Second(), ShouldEqual, correctTime.Second())
				So(calculatedTime.Nanosecond(), ShouldEqual, correctTime.Nanosecond())
			})
		})

		Convey("Errors for a yearly timeunit", func() {
			// TODO: Add tests once we add yearly support

			timeUnit := pantheonrpc.TimeUnit_YEAR

			calculatedTime, err := GetLeaderboardExpirationTime(eventTime, timeUnit)
			So(err, ShouldNotBeNil)
			So(calculatedTime, ShouldBeNil)
		})
	})
}
