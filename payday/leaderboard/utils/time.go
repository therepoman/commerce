package utils

import (
	"time"

	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/utils/pointers"
)

var supportedTimeUnits = map[pantheonrpc.TimeUnit]interface{}{
	pantheonrpc.TimeUnit_ALLTIME: nil,
	pantheonrpc.TimeUnit_WEEK:    nil,
	pantheonrpc.TimeUnit_MONTH:   nil,
	pantheonrpc.TimeUnit_DAY:     nil,
}

func IsSupportedTimeUnit(unit pantheonrpc.TimeUnit) bool {
	_, isPresent := supportedTimeUnits[unit]
	return isPresent
}

func GetLeaderboardExpirationTime(now time.Time, unit pantheonrpc.TimeUnit) (*time.Time, error) {
	if unit == pantheonrpc.TimeUnit_ALLTIME {
		return nil, nil
	}

	if !IsSupportedTimeUnit(unit) {
		return nil, errors.New("received unsupported time_unit")
	}

	loc, err := time.LoadLocation("America/Los_Angeles")
	if err != nil {
		panic(err)
	}
	expirationDate := now.In(loc)
	if unit == pantheonrpc.TimeUnit_DAY {
		expirationDate = expirationDate.Add(24 * time.Hour)
		return pointers.TimeP(time.Date(expirationDate.Year(), expirationDate.Month(), expirationDate.Day(), 0, 0, 0, 0, loc)), nil
	} else if unit == pantheonrpc.TimeUnit_WEEK {
		if expirationDate.Weekday() == time.Monday {
			expirationDate = expirationDate.Add(7 * 24 * time.Hour)
			return pointers.TimeP(time.Date(expirationDate.Year(), expirationDate.Month(), expirationDate.Day(), 0, 0, 0, 0, loc)), nil
		}
		for expirationDate.Weekday() != time.Monday {
			expirationDate = expirationDate.Add(24 * time.Hour)
		}
	} else {
		// monthly expiration
		year := expirationDate.Year()
		month := expirationDate.Month() + 1

		if month == 13 {
			month = 1
			year = year + 1
		}

		expirationDate = time.Date(year, month, 1, 0, 0, 0, 0, loc)
	}

	return pointers.TimeP(time.Date(expirationDate.Year(), expirationDate.Month(), expirationDate.Day(), 0, 0, 0, 0, loc)), nil
}
