package leaderboard

import (
	"context"
	"encoding/json"
	"fmt"
	"strconv"
	"time"

	chatbadge_models "code.justin.tv/chat/badges/app/models"
	pubclient "code.justin.tv/chat/pubsub-go-pubclient/client"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	"code.justin.tv/commerce/payday/cache/channel"
	chatbadge_client "code.justin.tv/commerce/payday/clients/chatbadges"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/dynamo/leaderboard_badge_holders"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/leaderboard/utils"
	"code.justin.tv/commerce/payday/lock"
	"code.justin.tv/commerce/payday/models"
	"github.com/gofrs/uuid"
)

const (
	BadgeUpdateActionRemove = BadgeUpdateAction("remove")
	BadgeUpdateActionAdd    = BadgeUpdateAction("add")

	RedisLockKeyPrefix = "leaderboard-badge-lock"
)

type BadgeUpdateAction string

type LeaderboardBadgeUpdate struct {
	UserID string
	Rank   int64
	Action BadgeUpdateAction
}

type BadgeReconciler interface {
	ReconcileBadges(ctx context.Context, channelID string, lbTimeUnit pantheonrpc.TimeUnit, newBadgeHolders BadgeHolders, forceChatBadgeEntitlements bool) error
}

type BadgeReconcilerImpl struct {
	ChatBadgesClient           chatbadge_client.IChatBadgesClient                    `inject:""`
	PubClient                  pubclient.PubClient                                   `inject:""`
	LeaderboardBadgeHoldersDao leaderboard_badge_holders.ILeaderboardBadgeHoldersDao `inject:""`
	LockingClient              lock.LockingClient                                    `inject:""`
	ChannelManager             channel.ChannelManager                                `inject:""`
	Config                     *config.Configuration                                 `inject:""`
}

type LeaderboardBadge struct {
	UserID        string
	BadgePosition int64
}

type BadgeHolders []LeaderboardBadge

func (bh BadgeHolders) Contains(badge LeaderboardBadge) bool {
	for _, b := range bh {
		if b.UserID == badge.UserID && b.BadgePosition == badge.BadgePosition {
			return true
		}
	}
	return false
}

func convertBadgeHoldersDynamo(bhd *leaderboard_badge_holders.LeaderboardBadgeHolders) BadgeHolders {
	badgeHolders := make(BadgeHolders, 0)
	if bhd.FirstPlaceUserID != "" {
		badgeHolders = append(badgeHolders, LeaderboardBadge{
			UserID:        bhd.FirstPlaceUserID,
			BadgePosition: 1,
		})
	}

	if bhd.SecondPlaceUserID != "" {
		badgeHolders = append(badgeHolders, LeaderboardBadge{
			UserID:        bhd.SecondPlaceUserID,
			BadgePosition: 2,
		})
	}

	if bhd.ThirdPlaceUserID != "" {
		badgeHolders = append(badgeHolders, LeaderboardBadge{
			UserID:        bhd.ThirdPlaceUserID,
			BadgePosition: 3,
		})
	}
	return badgeHolders
}

func (br *BadgeReconcilerImpl) ReconcileBadges(ctx context.Context, channelID string, lbTimeUnit pantheonrpc.TimeUnit, newBadgeHolders BadgeHolders, forceChatBadgeEntitlements bool) error {
	channelLock, err := br.LockingClient.ObtainLock(fmt.Sprintf("%s-%s", RedisLockKeyPrefix, channelID))
	if err != nil {
		return err
	}
	defer func() {
		err = channelLock.Unlock()
		if err != nil {
			log.Error(err)
		}
	}()

	currentBadgeHoldersDynamo, err := br.LeaderboardBadgeHoldersDao.Get(channelID)
	if err != nil {
		return err
	}

	if currentBadgeHoldersDynamo == nil {
		currentBadgeHoldersDynamo = &leaderboard_badge_holders.LeaderboardBadgeHolders{
			ChannelID: channelID,
		}
	}

	currentBadgeHolders := convertBadgeHoldersDynamo(currentBadgeHoldersDynamo)
	expirationTime, err := utils.GetLeaderboardExpirationTime(time.Now(), lbTimeUnit)
	if err != nil {
		return err
	}

	usersToUpdate := determineLeaderboardUpdates(currentBadgeHolders, newBadgeHolders, forceChatBadgeEntitlements)

	usersToRemoveBadges := map[string]LeaderboardBadgeUpdate{}
	for _, user := range usersToUpdate {
		if user.UserID != "" && user.Action == BadgeUpdateActionRemove {
			usersToRemoveBadges[user.UserID] = user
		}
	}

	for userID := range usersToRemoveBadges {
		err := br.revokeUserLeaderboardBadge(ctx, userID, channelID)
		if err != nil {
			return err
		}
	}

	usersToAddBadges := map[string]LeaderboardBadgeUpdate{}
	for _, user := range usersToUpdate {
		if user.UserID != "" && user.Action == BadgeUpdateActionAdd {
			usersToAddBadges[user.UserID] = user
		}
	}

	for _, user := range usersToAddBadges {
		err := br.grantUserLeaderboardBadge(ctx, user.UserID, channelID, strconv.Itoa(int(user.Rank)), expirationTime)
		if err != nil {
			return err
		}
	}

	currentBadgeHoldersDynamo.FirstPlaceUserID = ""
	currentBadgeHoldersDynamo.SecondPlaceUserID = ""
	currentBadgeHoldersDynamo.ThirdPlaceUserID = ""
	for i, badge := range newBadgeHolders {
		switch i {
		case 0:
			currentBadgeHoldersDynamo.FirstPlaceUserID = badge.UserID
		case 1:
			currentBadgeHoldersDynamo.SecondPlaceUserID = badge.UserID
		case 2:
			currentBadgeHoldersDynamo.ThirdPlaceUserID = badge.UserID
		}
	}

	err = br.LeaderboardBadgeHoldersDao.Put(currentBadgeHoldersDynamo)
	if err == nil {
		log.WithFields(log.Fields{
			"channelID":         channelID,
			"firstPlaceUserID":  currentBadgeHoldersDynamo.FirstPlaceUserID,
			"secondPlaceUserID": currentBadgeHoldersDynamo.SecondPlaceUserID,
			"thirdPlaceUserID":  currentBadgeHoldersDynamo.ThirdPlaceUserID,
		}).Info("Dynamo successfully updated for this channel")
	}

	return err
}

func (br *BadgeReconcilerImpl) grantUserLeaderboardBadge(ctx context.Context, userID string, channelID string, rank string, expirationTime *time.Time) error {
	expirationTimestamp := int64(0)
	if expirationTime != nil {
		expirationTimestamp = expirationTime.Unix()
	}
	logger := log.WithFields(log.Fields{
		"userID":    userID,
		"channelID": channelID,
		"rank":      rank,
	})

	err := br.ChatBadgesClient.GrantBitsLeaderBadge(ctx, userID, channelID, chatbadge_models.GrantBadgeParams{
		BadgeSetVersion: rank,
		EndTimestamp:    expirationTimestamp,
	}, nil)
	if err != nil {
		logger.WithError(err).Error("could not entile leaderboard badge")
		return err
	}

	logger.Info("User was successfully granted the top cheers' badge")

	messageIdUUID, err := uuid.NewV4()
	if err != nil {
		log.WithError(err).Error("Could not generate UUID")
		return errors.Notef(err, "Could not generate UUID")
	}

	pubsubMessage := models.PrivateBitsMessage{
		Data: models.BadgeUpdate{
			UserID:        userID,
			ChannelID:     channelID,
			NewestVersion: rank,
			SetID:         models.BitsLeaderboardBadgeUpdateSetID,
		},
		MessageId:   messageIdUUID.String(),
		MessageType: models.BitBadgeUpdateMessageType,
		Version:     models.CurrentPrivateMessageVersion,
	}

	marshaled, err := json.Marshal(pubsubMessage)
	if err != nil {
		return errors.Notef(err, "Could not marshal pubsub message %v", pubsubMessage)
	}

	pubsubTopic := []string{fmt.Sprintf("%s.%s", models.UserBitsUpdatesPubsubMessageTopic, userID)}
	err = br.PubClient.Publish(ctx, pubsubTopic, string(marshaled), nil)
	if err != nil {
		log.WithFields(log.Fields{
			"userID":    userID,
			"channelID": channelID,
			"rank":      rank,
			"topic":     pubsubTopic,
		}).WithError(err).Error("could not publish pubsub for leaderboard badge update")
		return err
	}

	logger.Info("Pubsub successfully sent out to update leaderboard")

	return nil
}

func (br *BadgeReconcilerImpl) revokeUserLeaderboardBadge(ctx context.Context, userID string, channelID string) error {
	err := br.ChatBadgesClient.RevokeBitsLeaderBadge(ctx, userID, channelID, nil)
	if err != nil {
		log.WithFields(log.Fields{
			"userID":    userID,
			"channelID": channelID,
		}).WithError(err).Error("could not revoke leaderboard badge")
		return err
	}

	messageIdUUID, err := uuid.NewV4()
	if err != nil {
		log.WithError(err).Error("Could not generate UUID")
		return errors.Notef(err, "Could not generate UUID")
	}

	pubsubMessage := models.PrivateBitsMessage{
		Data: models.BadgeUpdate{
			UserID:    userID,
			ChannelID: channelID,
			SetID:     models.BitsLeaderboardBadgeUpdateSetID,
		},
		MessageId:   messageIdUUID.String(),
		MessageType: models.BitBadgeUpdateMessageType,
		Version:     models.CurrentPrivateMessageVersion,
	}

	marshaled, err := json.Marshal(pubsubMessage)

	if err != nil {
		return errors.Notef(err, "Could not marshal pubsub message %v", pubsubMessage)
	}

	pubsubTopic := []string{fmt.Sprintf("%s.%s", models.BitBadgeUpdateMessageType, userID)}
	err = br.PubClient.Publish(ctx, pubsubTopic, string(marshaled), nil)
	if err != nil {
		log.WithFields(log.Fields{
			"userID":    userID,
			"channelID": channelID,
			"topic":     pubsubTopic,
		}).WithError(err).Error("could not publish pubsub for leaderboard badge revoke")
		return err
	}

	return nil
}

func determineLeaderboardUpdates(currentBadgeHolders BadgeHolders, newBadgeHolders BadgeHolders, forceChatBadgeEntitlements bool) []LeaderboardBadgeUpdate {
	usersToUpdate := make([]LeaderboardBadgeUpdate, 0)

	for _, newBadgeHolder := range newBadgeHolders {
		if !currentBadgeHolders.Contains(newBadgeHolder) || forceChatBadgeEntitlements {
			usersToUpdate = append(usersToUpdate, LeaderboardBadgeUpdate{
				UserID: newBadgeHolder.UserID,
				Rank:   newBadgeHolder.BadgePosition,
				Action: BadgeUpdateActionAdd,
			})
		}
	}

	for _, currentBadgeHolder := range currentBadgeHolders {
		if !newBadgeHolders.Contains(currentBadgeHolder) {
			usersToUpdate = append(usersToUpdate, LeaderboardBadgeUpdate{
				UserID: currentBadgeHolder.UserID,
				Rank:   currentBadgeHolder.BadgePosition,
				Action: BadgeUpdateActionRemove,
			})
		}
	}

	return usersToUpdate
}
