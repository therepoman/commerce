package leaderboard

import (
	"context"

	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	"code.justin.tv/commerce/payday/clients/pantheon"
)

type BitsToBroadcasterGetter interface {
	GetBitsToBroadcaster(ctx context.Context, userID string, channelID string) (int64, error)
}
type bitsToBroadcasterGetter struct {
	Pantheon pantheon.IPantheonClientWrapper `inject:""`
}

func NewBitsToBroadcaster() BitsToBroadcasterGetter {
	return &bitsToBroadcasterGetter{}
}

func (b *bitsToBroadcasterGetter) GetBitsToBroadcaster(ctx context.Context, userID, channelID string) (int64, error) {
	userLeaderboard, err := b.Pantheon.GetLeaderboard(
		ctx, pantheon.PantheonBitsUsageDomain, pantheonrpc.TimeUnit_ALLTIME, channelID, userID, 0, 0, true)
	if err != nil {
		return 0, err
	}

	// user is not in the leaderboard, so they gave nothing, hence 0 and no error
	if userLeaderboard == nil || userLeaderboard.EntryContext == nil || userLeaderboard.EntryContext.Entry == nil {
		return 0, nil
	}

	return userLeaderboard.EntryContext.Entry.Score, nil
}
