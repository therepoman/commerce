package leaderboard_test

import (
	"context"
	"testing"

	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	"code.justin.tv/commerce/payday/dynamo/leaderboard_badge_holders"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/leaderboard"
	client_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/chat/pubsub-go-pubclient/client"
	chatbadges_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/chatbadges"
	leaderboard_badge_holders_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo/leaderboard_badge_holders"
	lock_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/lock"
	"code.justin.tv/commerce/payday/models"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

const (
	testLeaderboardChannelID = "123123123123"
)

func toBadgeHolders(msg models.PantheonSNSMessage) leaderboard.BadgeHolders {
	newBadgeHolders := make(leaderboard.BadgeHolders, 0)
	for _, msg := range msg.Top {
		if msg.Rank <= 3 {
			newBadgeHolders = append(newBadgeHolders, leaderboard.LeaderboardBadge{
				UserID:        msg.EntryKey,
				BadgePosition: msg.Rank,
			})
		}
		if len(newBadgeHolders) == 3 {
			break
		}
	}
	return newBadgeHolders
}

func TestObserver_Observe(t *testing.T) {
	Convey("For a lb badge reconciler", t, func() {
		chatBadgesMock := new(chatbadges_mock.IChatBadgesClient)
		pubsubMock := new(client_mock.PubClient)
		leaderboardBadgesDaoMock := new(leaderboard_badge_holders_mock.ILeaderboardBadgeHoldersDao)
		lockingClient := new(lock_mock.LockingClient)
		redisLock := new(lock_mock.Lock)

		lockingClient.On("ObtainLock", mock.Anything).Return(redisLock, nil)
		redisLock.On("Unlock").Return(nil)

		reconciler := &leaderboard.BadgeReconcilerImpl{
			PubClient:                  pubsubMock,
			ChatBadgesClient:           chatBadgesMock,
			LeaderboardBadgeHoldersDao: leaderboardBadgesDaoMock,
			LockingClient:              lockingClient,
		}
		ctx := context.Background()

		lbIdentifier := models.PantheonSNSLeaderboardIdentifier{
			Domain:              "test",
			GroupingKey:         testLeaderboardChannelID,
			TimeAggregationUnit: "WEEK",
			TimeBucket:          "foo",
		}

		entry := models.PantheonSNSLeaderboardEntry{
			Rank:     1,
			Score:    10000,
			EntryKey: "walrus",
		}

		top := []models.PantheonSNSLeaderboardEntry{
			{
				Rank:     1,
				Score:    10000,
				EntryKey: "walrus",
			},
			{
				Rank:     2,
				Score:    1000,
				EntryKey: "not-as-good-walrus",
			},
		}

		entryContext := models.PantheonSNSLeaderboardEntryContext{
			Entry:   entry,
			Context: top,
		}

		input := models.PantheonSNSMessage{
			Identifier:   lbIdentifier,
			Top:          top,
			EntryContext: entryContext,
		}

		timeunit := pantheonrpc.TimeUnit_WEEK

		Convey("errors when we cannot fetch the leaderboard", func() {
			leaderboardBadgesDaoMock.On("Get", mock.Anything).Return(nil, errors.New("nope"))

			err := reconciler.ReconcileBadges(ctx, testLeaderboardChannelID, timeunit, toBadgeHolders(input), false)
			So(err, ShouldNotBeNil)
		})

		Convey("empty leaderboard creates top 3", func() {
			leaderboardBadgesDaoMock.On("Get", mock.Anything).Return(nil, nil)
			leaderboardBadgesDaoMock.On("Put", mock.Anything).Return(nil)
			chatBadgesMock.On("GrantBitsLeaderBadge", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)
			pubsubMock.On("Publish", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)

			err := reconciler.ReconcileBadges(ctx, testLeaderboardChannelID, timeunit, toBadgeHolders(input), false)
			So(err, ShouldBeNil)
			So(len(chatBadgesMock.Calls), ShouldEqual, 2)
			So(len(pubsubMock.Calls), ShouldEqual, 2)
			So(len(leaderboardBadgesDaoMock.Calls), ShouldEqual, 2)
		})

		Convey("swaps leaderboard positions when leaders flip", func() {
			currentLeaderboardStandings := &leaderboard_badge_holders.LeaderboardBadgeHolders{
				ChannelID:         testLeaderboardChannelID,
				FirstPlaceUserID:  "not-as-good-walrus",
				SecondPlaceUserID: "walrus",
			}

			leaderboardBadgesDaoMock.On("Get", mock.Anything).Return(currentLeaderboardStandings, nil)
			leaderboardBadgesDaoMock.On("Put", mock.Anything).Return(nil)
			chatBadgesMock.On("GrantBitsLeaderBadge", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)
			chatBadgesMock.On("RevokeBitsLeaderBadge", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)
			pubsubMock.On("Publish", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)

			err := reconciler.ReconcileBadges(ctx, testLeaderboardChannelID, timeunit, toBadgeHolders(input), false)
			So(err, ShouldBeNil)
			So(len(chatBadgesMock.Calls), ShouldEqual, 4)
			So(len(pubsubMock.Calls), ShouldEqual, 4)
			So(len(leaderboardBadgesDaoMock.Calls), ShouldEqual, 2)
		})

		Convey("adds new person", func() {
			currentLeaderboardStandings := &leaderboard_badge_holders.LeaderboardBadgeHolders{
				ChannelID:        testLeaderboardChannelID,
				FirstPlaceUserID: "walrus",
			}

			top := []models.PantheonSNSLeaderboardEntry{
				{
					Rank:     1,
					Score:    10000,
					EntryKey: "walrus",
				},
				{
					Rank:     2,
					Score:    1000,
					EntryKey: "not-as-good-walrus",
				},
			}

			input.Top = top

			leaderboardBadgesDaoMock.On("Get", mock.Anything).Return(currentLeaderboardStandings, nil)
			leaderboardBadgesDaoMock.On("Put", mock.Anything).Return(nil)
			chatBadgesMock.On("GrantBitsLeaderBadge", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)
			chatBadgesMock.On("RevokeBitsLeaderBadge", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)
			pubsubMock.On("Publish", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)

			err := reconciler.ReconcileBadges(ctx, testLeaderboardChannelID, timeunit, toBadgeHolders(input), false)
			So(err, ShouldBeNil)
			So(len(chatBadgesMock.Calls), ShouldEqual, 1)
			So(len(pubsubMock.Calls), ShouldEqual, 1)
			So(len(leaderboardBadgesDaoMock.Calls), ShouldEqual, 2)
		})

		Convey("bumps leader down", func() {
			currentLeaderboardStandings := &leaderboard_badge_holders.LeaderboardBadgeHolders{
				ChannelID:        testLeaderboardChannelID,
				FirstPlaceUserID: "walrus",
			}

			top := []models.PantheonSNSLeaderboardEntry{
				{
					Rank:     1,
					Score:    10000,
					EntryKey: "not-as-good-walrus",
				},
				{
					Rank:     2,
					Score:    1000,
					EntryKey: "walrus",
				},
			}

			input.Top = top

			leaderboardBadgesDaoMock.On("Get", mock.Anything).Return(currentLeaderboardStandings, nil)
			leaderboardBadgesDaoMock.On("Put", mock.Anything).Return(nil)
			chatBadgesMock.On("GrantBitsLeaderBadge", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)
			chatBadgesMock.On("RevokeBitsLeaderBadge", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)
			pubsubMock.On("Publish", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)

			err := reconciler.ReconcileBadges(ctx, testLeaderboardChannelID, timeunit, toBadgeHolders(input), false)
			So(err, ShouldBeNil)
			So(len(chatBadgesMock.Calls), ShouldEqual, 3)
			So(len(pubsubMock.Calls), ShouldEqual, 3)
			So(len(leaderboardBadgesDaoMock.Calls), ShouldEqual, 2)
		})

		Convey("bumps people down when a new person takes first", func() {
			currentLeaderboardStandings := &leaderboard_badge_holders.LeaderboardBadgeHolders{
				ChannelID:         testLeaderboardChannelID,
				FirstPlaceUserID:  "walrus",
				SecondPlaceUserID: "not-as-good-walrus",
			}

			top := []models.PantheonSNSLeaderboardEntry{
				{
					Rank:     1,
					Score:    1000000,
					EntryKey: "new-guy",
				},
				{
					Rank:     2,
					Score:    10000,
					EntryKey: "walrus",
				},
				{
					Rank:     3,
					Score:    1000,
					EntryKey: "not-as-good-walrus",
				},
			}

			input.Top = top

			leaderboardBadgesDaoMock.On("Get", mock.Anything).Return(currentLeaderboardStandings, nil)
			leaderboardBadgesDaoMock.On("Put", mock.Anything).Return(nil)
			chatBadgesMock.On("GrantBitsLeaderBadge", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)
			chatBadgesMock.On("RevokeBitsLeaderBadge", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)
			pubsubMock.On("Publish", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)

			err := reconciler.ReconcileBadges(ctx, testLeaderboardChannelID, timeunit, toBadgeHolders(input), false)
			So(err, ShouldBeNil)
			So(len(chatBadgesMock.Calls), ShouldEqual, 5)
			So(len(pubsubMock.Calls), ShouldEqual, 5)
			So(len(leaderboardBadgesDaoMock.Calls), ShouldEqual, 2)
		})

		Convey("rotates 1 - 3", func() {
			currentLeaderboardStandings := &leaderboard_badge_holders.LeaderboardBadgeHolders{
				ChannelID:         testLeaderboardChannelID,
				FirstPlaceUserID:  "walrus",
				SecondPlaceUserID: "not-as-good-walrus",
				ThirdPlaceUserID:  "new-guy",
			}

			top := []models.PantheonSNSLeaderboardEntry{
				{
					Rank:     1,
					Score:    1000000,
					EntryKey: "new-guy",
				},
				{
					Rank:     2,
					Score:    10000,
					EntryKey: "walrus",
				},
				{
					Rank:     3,
					Score:    1000,
					EntryKey: "not-as-good-walrus",
				},
			}

			input.Top = top

			leaderboardBadgesDaoMock.On("Get", mock.Anything).Return(currentLeaderboardStandings, nil)
			leaderboardBadgesDaoMock.On("Put", mock.Anything).Return(nil)
			chatBadgesMock.On("GrantBitsLeaderBadge", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)
			chatBadgesMock.On("RevokeBitsLeaderBadge", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)
			pubsubMock.On("Publish", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)

			err := reconciler.ReconcileBadges(ctx, testLeaderboardChannelID, timeunit, toBadgeHolders(input), false)
			So(err, ShouldBeNil)
			So(len(chatBadgesMock.Calls), ShouldEqual, 6)
			So(len(pubsubMock.Calls), ShouldEqual, 6)
			So(len(leaderboardBadgesDaoMock.Calls), ShouldEqual, 2)
		})

		Convey("rotates top out", func() {
			currentLeaderboardStandings := &leaderboard_badge_holders.LeaderboardBadgeHolders{
				ChannelID:         testLeaderboardChannelID,
				FirstPlaceUserID:  "new-guy",
				SecondPlaceUserID: "walrus",
				ThirdPlaceUserID:  "not-as-good-walrus",
			}

			top := []models.PantheonSNSLeaderboardEntry{
				{
					Rank:     1,
					Score:    1000000,
					EntryKey: "new-guy",
				},
				{
					Rank:     2,
					Score:    10000,
					EntryKey: "fooUser",
				},
				{
					Rank:     3,
					Score:    1000,
					EntryKey: "walrus",
				},
				{
					Rank:     4,
					Score:    900,
					EntryKey: "not-as-good-walrus",
				},
			}

			input.Top = top

			leaderboardBadgesDaoMock.On("Get", mock.Anything).Return(currentLeaderboardStandings, nil)
			leaderboardBadgesDaoMock.On("Put", mock.Anything).Return(nil)
			chatBadgesMock.On("GrantBitsLeaderBadge", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)
			chatBadgesMock.On("RevokeBitsLeaderBadge", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)
			pubsubMock.On("Publish", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)

			err := reconciler.ReconcileBadges(ctx, testLeaderboardChannelID, timeunit, toBadgeHolders(input), false)
			So(err, ShouldBeNil)
			So(len(chatBadgesMock.Calls), ShouldEqual, 4)
			So(len(pubsubMock.Calls), ShouldEqual, 4)
			So(len(leaderboardBadgesDaoMock.Calls), ShouldEqual, 2)
		})

		Convey("publishes to pubsub and chat badge service", func() {
			currentLeaderboardStandings := &leaderboard_badge_holders.LeaderboardBadgeHolders{
				ChannelID:         testLeaderboardChannelID,
				FirstPlaceUserID:  "new-guy",
				SecondPlaceUserID: "walrus",
				ThirdPlaceUserID:  "not-as-good-walrus",
			}

			top := []models.PantheonSNSLeaderboardEntry{
				{
					Rank:     1,
					Score:    1000000,
					EntryKey: "new-guy",
				},
				{
					Rank:     2,
					Score:    10000,
					EntryKey: "fooUser",
				},
				{
					Rank:     3,
					Score:    1000,
					EntryKey: "walrus",
				},
				{
					Rank:     4,
					Score:    900,
					EntryKey: "not-as-good-walrus",
				},
			}

			input.Top = top

			leaderboardBadgesDaoMock.On("Get", mock.Anything).Return(currentLeaderboardStandings, nil)
			leaderboardBadgesDaoMock.On("Put", mock.Anything).Return(nil)
			chatBadgesMock.On("GrantBitsLeaderBadge", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)
			chatBadgesMock.On("RevokeBitsLeaderBadge", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)
			pubsubMock.On("Publish", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)

			err := reconciler.ReconcileBadges(ctx, testLeaderboardChannelID, timeunit, toBadgeHolders(input), false)

			So(err, ShouldBeNil)
			So(len(chatBadgesMock.Calls), ShouldEqual, 4)
			So(len(pubsubMock.Calls), ShouldEqual, 4)
			So(len(leaderboardBadgesDaoMock.Calls), ShouldEqual, 2)
		})
	})
}
