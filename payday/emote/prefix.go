package emote

import (
	"context"
	"fmt"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/cache"
	"code.justin.tv/commerce/payday/clients/prefixes"
	"code.justin.tv/commerce/payday/errors"
)

const (
	Cheer                = "Cheer"
	noPrefixError        = PrefixError("no prefix")
	TooManyPrefixesError = PrefixError("too many prefixes")
)

type PrefixError string

func (e PrefixError) Error() string { return string(e) }

type IPrefixManager interface {
	GetCustomCheermoteAction(ctx context.Context, channelId string) (string, bool, error)
	GetEmotePrefix(ctx context.Context, channelId string) (string, bool, error)
}

type PrefixManager struct {
	PrefixesCache  cache.IPrefixesCache     `inject:""`
	PrefixesClient prefixes.IPrefixesClient `inject:""`
}

func (p *PrefixManager) getPrefix(ctx context.Context, channelId string) (string, error) {
	var output prefixes.PrefixesResponse

	cachedPrefixes := p.PrefixesCache.Get(ctx, channelId)
	if cachedPrefixes == nil {
		prefixesResponse, err := p.PrefixesClient.GetPrefixes(ctx, channelId)
		if err != nil {
			msg := "Error getting prefix from prefix client"
			log.WithError(err).WithField("channelId", channelId).Error(msg)
			return "", errors.Notef(err, msg)
		}
		p.PrefixesCache.Set(ctx, channelId, prefixesResponse)
		output = prefixesResponse
	} else {
		output = *cachedPrefixes
	}

	if len(output.Prefixes) == 0 {
		return "", noPrefixError
	} else if len(output.Prefixes) > 1 {
		return "", TooManyPrefixesError
	}

	return output.Prefixes[0], nil
}

func (p *PrefixManager) GetCustomCheermoteAction(ctx context.Context, channelId string) (string, bool, error) {
	prefix, err := p.getPrefix(ctx, channelId)
	if err == noPrefixError {
		return "", false, nil
	}
	if err != nil {
		log.Warnf("Error getting prefix for channelId: %s. %v", channelId, err)
		return "", false, err
	}
	return fmt.Sprintf("%s%s", prefix, Cheer), true, nil
}

func (p *PrefixManager) GetEmotePrefix(ctx context.Context, channelId string) (string, bool, error) {
	prefix, err := p.getPrefix(ctx, channelId)
	if err == noPrefixError {
		return "", false, nil
	}
	if err != nil {
		log.Warnf("Error getting emote prefix for channelId: %s. %v", channelId, err)
		return "", false, err
	}
	return prefix, true, nil
}
