package golang

import (
	"runtime"
	"strings"
)

type GoVersion string

const (
	go_1_5_internal  = "go1.5"
	go_1_6_internal  = "go1.6"
	go_1_7_internal  = "go1.7"
	go_1_8_internal  = "go1.8"
	go_1_9_internal  = "go1.9"
	go_1_10_internal = "go1.10"
)

const (
	GO_1_5     = GoVersion("1.5")
	GO_1_6     = GoVersion("1.6")
	GO_1_7     = GoVersion("1.7")
	GO_1_8     = GoVersion("1.8")
	GO_1_9     = GoVersion("1.9")
	GO_1_10    = GoVersion("1.10")
	GO_UNKNOWN = GoVersion("unknown")
)

func internalGoVersion() string {
	return runtime.Version()
}

func GetGoVersion() GoVersion {
	goVersion := internalGoVersion()
	if strings.Contains(goVersion, go_1_10_internal) {
		return GO_1_10
	} else if strings.Contains(goVersion, go_1_9_internal) {
		return GO_1_9
	} else if strings.Contains(goVersion, go_1_8_internal) {
		return GO_1_8
	} else if strings.Contains(goVersion, go_1_7_internal) {
		return GO_1_7
	} else if strings.Contains(goVersion, go_1_6_internal) {
		return GO_1_6
	} else if strings.Contains(goVersion, go_1_5_internal) {
		return GO_1_5
	}
	return GO_UNKNOWN
}

func IsGo19OrNewer() bool {
	currentVer := GetGoVersion()
	return currentVer == GO_1_9 || currentVer == GO_1_10 || currentVer == GO_UNKNOWN
}
