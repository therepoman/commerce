package time

import (
	"time"

	"code.justin.tv/commerce/payday/utils/pointers"
)

func IsTimeInRange(checkTime, startTime, endTime time.Time) bool {
	return (checkTime.Equal(startTime) || checkTime.Equal(endTime)) ||
		(checkTime.After(startTime) && checkTime.Before(endTime))
}

func IsTimeInForwardDelta(checkTime, targetTime time.Time, forwardDelta time.Duration) bool {
	endTime := targetTime.Add(forwardDelta)
	return IsTimeInRange(checkTime, targetTime, endTime)
}

func IsTimeInBackwardsDelta(checkTime, targetTime time.Time, backwardsDelta time.Duration) bool {
	startTime := targetTime.Add(-1 * backwardsDelta)
	return IsTimeInRange(checkTime, startTime, targetTime)
}

func IsTimeInBiDirectionalDelta(checkTime, targetTime time.Time, delta time.Duration) bool {
	return IsTimeInForwardDelta(checkTime, targetTime, delta) || IsTimeInBackwardsDelta(checkTime, targetTime, delta)
}

func AddDurations(t1 time.Duration, t2 time.Duration) time.Duration {
	return time.Duration(t1.Nanoseconds() + t2.Nanoseconds())
}

func SecondsToNanoseconds(s int64) int64 {
	return (time.Second * time.Duration(s)).Nanoseconds()
}

func SecondsToNanosecondsP(s *int64) *int64 {
	if s != nil {
		return pointers.Int64P(SecondsToNanoseconds(*s))
	}
	return nil
}

func NanosecondsToSecondsP(ns *int64) *int64 {
	if ns != nil {
		return pointers.Int64P(int64((time.Nanosecond * time.Duration(*ns)).Seconds()))
	}
	return nil
}

func NanosecondsToMilliseconds(ns int64) int64 {
	return int64((time.Nanosecond * time.Duration(ns)) / time.Millisecond)
}

func SecondsToMilliseconds(s int64) int64 {
	return int64((time.Second * time.Duration(s)) / time.Millisecond)
}
