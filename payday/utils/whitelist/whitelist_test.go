package whitelist

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestIsWhitelistedChannelID(t *testing.T) {
	Convey("Given a list of channelIDs", t, func() {
		channelIDs := []string{"123", "456", "789"}

		Convey("and a channelID in that list", func() {
			channelID := "456"

			So(IsWhitelistedChannelID(channelID, channelIDs), ShouldBeTrue)
		})

		Convey("and a channelID not in that list", func() {
			channelID := "420"

			So(IsWhitelistedChannelID(channelID, channelIDs), ShouldBeFalse)
		})
	})
}
