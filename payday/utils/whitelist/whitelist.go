package whitelist

func IsWhitelistedChannelID(channelID string, channelIDs []string) bool {
	for _, cID := range channelIDs {
		if cID == channelID {
			return true
		}
	}

	return false
}
