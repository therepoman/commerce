package close

import log "code.justin.tv/commerce/logrus"

type Closer interface {
	Close() error
}

func Close(o Closer) {
	err := o.Close()
	if err != nil {
		log.WithError(err).Error("Failed to close")
	}
}
