package new_sku_experiment_gating

// An experiment was run where 25% of new users will be vended a 300 bit sku instead of the below one, which was 1000 Bits for $10.
// This is now here so that we can remove/disable the old one from our catalog cleanly.
const (
	OldFirstTimePurchaseSKU = "B073RTRHTZ"
)
