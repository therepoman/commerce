package sync

import (
	go_sync "sync"
)

func GetChannel(wg *go_sync.WaitGroup) chan interface{} {
	c := make(chan interface{}, 1)
	go func() {
		defer close(c)
		wg.Wait()
	}()

	return c
}
