package strings

import (
	"testing"

	"code.justin.tv/commerce/payday/utils/pointers"
	"github.com/stretchr/testify/assert"
)

func TestReverse(t *testing.T) {
	result := Reverse("abcd")
	assert.Equal(t, "dcba", result)

	result = Reverse("racecar")
	assert.Equal(t, "racecar", result)
}

func TestBlank(t *testing.T) {
	assert.True(t, BlankP(nil))
	assert.True(t, BlankP(pointers.StringP("")))
	assert.True(t, BlankP(pointers.StringP("    ")))
	assert.True(t, BlankP(pointers.StringP("    \t \t \t \n \n \t  ")))

	assert.False(t, BlankP(pointers.StringP("A")))
	assert.False(t, BlankP(pointers.StringP("        A        ")))
}

func TestNotBlank(t *testing.T) {
	assert.False(t, NotBlankP(nil))
	assert.False(t, NotBlankP(pointers.StringP("")))
	assert.False(t, NotBlankP(pointers.StringP("    ")))
	assert.False(t, NotBlankP(pointers.StringP("    \t \t \t \n \n \t  ")))

	assert.True(t, NotBlankP(pointers.StringP("A")))
	assert.True(t, NotBlankP(pointers.StringP("        A        ")))
}

func TestUniqueList(t *testing.T) {
	l := NewSet()
	assert.Equal(t, 0, len(l.GetList()))
	l.Add("A")
	assert.Equal(t, 1, len(l.GetList()))
	l.Add("A")
	assert.Equal(t, 1, len(l.GetList()))
	l.Add("B")
	assert.Equal(t, 2, len(l.GetList()))
	l.Add("B")
	assert.Equal(t, 2, len(l.GetList()))
}

func TestTruncate(t *testing.T) {
	assert.Equal(t, "", Trucate("", 0))
	assert.Equal(t, "", Trucate("", 1))
	assert.Equal(t, "", Trucate("", 2))

	assert.Equal(t, "", Trucate("a", 0))
	assert.Equal(t, "a", Trucate("a", 1))
	assert.Equal(t, "a", Trucate("a", 2))

	assert.Equal(t, "", Trucate("ab", 0))
	assert.Equal(t, "a", Trucate("ab", 1))
	assert.Equal(t, "ab", Trucate("ab", 2))
}

func TestCommatizeInt(t *testing.T) {
	result := CommatizeInt(1000)
	assert.Equal(t, "1,000", result)

	result = CommatizeInt(10000)
	assert.Equal(t, "10,000", result)

	result = CommatizeInt(100000)
	assert.Equal(t, "100,000", result)

	result = CommatizeInt(1000000)
	assert.Equal(t, "1,000,000", result)
}
