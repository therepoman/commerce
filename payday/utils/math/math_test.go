package math

import (
	"math"
	"math/big"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNumericStringToBigInt(t *testing.T) {
	bi, err := NumericStringToBigInt("")
	assert.Nil(t, bi)
	assert.Error(t, err)

	bi, err = NumericStringToBigInt(" ")
	assert.Nil(t, bi)
	assert.Error(t, err)

	bi, err = NumericStringToBigInt("-123")
	assert.Nil(t, bi)
	assert.Error(t, err)

	bi, err = NumericStringToBigInt("123A456")
	assert.Nil(t, bi)
	assert.Error(t, err)

	bi, err = NumericStringToBigInt("1")
	assert.NoError(t, err)
	assert.Equal(t, *big.NewInt(int64(1)), *bi)

	bi, err = NumericStringToBigInt("982347983247")
	assert.NoError(t, err)
	assert.Equal(t, *big.NewInt(int64(982347983247)), *bi)

	bi, err = NumericStringToBigInt("934832049329804908234098320920990329098009823890234890")
	assert.NoError(t, err)
	assert.True(t, bi.Cmp(big.NewInt(math.MaxInt64)) == 1, "Big int value is greater than max int64 value")
}
