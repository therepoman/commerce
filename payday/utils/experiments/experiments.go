package experiments

import (
	"context"
	"encoding/json"
	"fmt"
	"math/rand"
	"strconv"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/clients/redis"
	"code.justin.tv/commerce/payday/hystrix"
	cmds "code.justin.tv/commerce/payday/hystrix"
	"code.justin.tv/commerce/payday/s3"
	"code.justin.tv/commerce/payday/utils/strings"
	go_redis "github.com/go-redis/redis"
)

const (
	s3ExperimentBucket       = "payday-experiment-config"
	experimentConfigFileName = "experiment_config.json"
	experimentConfigCacheTTL = 5 * time.Minute

	// List of experiment prefixes
	PrometheusEnableExperimentPrefix     = "prometheus-enable-experiment"
	FirstCheerUserNoticeExperimentPrefix = "first-cheer-user-notice"
	BitsBadgeModerationEmailPrefix       = "bits-badge-moderation-email"
	BitsTierEmoteGroupIDOnboardPrefix    = "bits-tier-emote-groupid-onboard"
)

type ExperimentConfigFetcher interface {
	// Returns whether the experiment corresponding to the given prefix is active for the given user and/or channel
	IsExperimentActiveForUserOrChannel(ctx context.Context, experimentPrefix string, userID string, channelID string) bool
	IsExperimentActiveForUser(ctx context.Context, experimentPrefix string, userID string) bool
	IsExperimentActiveForChannel(ctx context.Context, experimentPrefix string, channelID string) bool

	// Returns whether the experiment corresponding to the given prefix is active for the given user and/or channel
	// Also return any generic interface data stored along with the experiment config
	// It is up to the consumer to type assert the interface data into whatever structure they have defined in S3
	IsExperimentActiveForUserOrChannelWithData(ctx context.Context, experimentPrefix string, userID string, channelID string) (bool, interface{})
	IsExperimentActiveForUserWithData(ctx context.Context, experimentPrefix string, userID string) (bool, interface{})
	IsExperimentActiveForChannelWithData(ctx context.Context, experimentPrefix string, channelID string) (bool, interface{})
}

type experimentConfigFetcher struct {
	S3Client    s3.IS3Client `inject:""`
	RedisClient redis.Client `inject:"redisClient"`
}

func NewFetcher() ExperimentConfigFetcher {
	return &experimentConfigFetcher{}
}

type ExperimentConfig struct {
	DialUpPercentage int         `json:"dial_up_percentage"`
	UserWhitelist    []string    `json:"user_whitelist"`
	ChannelWhitelist []string    `json:"channel_whitelist"`
	Data             interface{} `json:"data"`
}

func (fetcher *experimentConfigFetcher) IsExperimentActiveForUserOrChannelWithData(ctx context.Context, experimentPrefix string, userID string, channelID string) (bool, interface{}) {
	var experimentConfigJson string
	err := hystrix.Do(cmds.ExperimentGet, func() error {
		var err error
		experimentConfigJson, err = fetcher.RedisClient.Get(ctx, experimentPrefix).Result()

		if err != nil && err != go_redis.Nil {
			log.WithError(err).WithField("experiment_prefix", experimentPrefix).Error("Error getting experiment config out of cache")
		}
		return nil
	}, nil)

	if err != nil {
		log.WithError(err).Warn("hystrix error fetching experiment config from cache")
	}

	if strings.Blank(experimentConfigJson) {
		experimentConfigBytes, err := fetcher.S3Client.LoadFile(s3ExperimentBucket, fmt.Sprintf("%s/%s", experimentPrefix, experimentConfigFileName))

		if err != nil {
			log.WithError(err).Error("Error fetching experiment config from S3")
			return false, ""
		}

		experimentConfigJson = string(experimentConfigBytes)
		err = hystrix.Do(cmds.ExperimentSet, func() error {
			err = fetcher.RedisClient.Set(ctx, experimentPrefix, experimentConfigJson, experimentConfigCacheTTL).Err()
			if err != nil {
				log.WithError(err).WithField("experiment_prefix", experimentPrefix).Error("Error caching experiment config")
			}
			return nil
		}, nil)

		if err != nil {
			log.WithError(err).Warn("hystrix error setting experiment config in cache")
		}
	}

	var experimentConfig ExperimentConfig
	err = json.Unmarshal([]byte(experimentConfigJson), &experimentConfig)
	if err != nil {
		log.WithError(err).Error("Error unmarshalling experiment config from S3")
		return false, ""
	}

	if userID != "" && strings.Contains(experimentConfig.UserWhitelist, userID) {
		return true, experimentConfig.Data
	}

	if channelID != "" && strings.Contains(experimentConfig.ChannelWhitelist, channelID) {
		return true, experimentConfig.Data
	}

	if userID != "" {
		tuidInt, err := strconv.Atoi(userID)
		if err != nil {
			log.WithError(err).WithField("tuid", userID).Error("Error converting string tuid to int")
			return false, ""
		}

		randomNumberGenerator := rand.New(rand.NewSource(int64(tuidInt)))
		if (randomNumberGenerator.Intn(100) + 1) <= experimentConfig.DialUpPercentage {
			return true, experimentConfig.Data
		}
	} else {
		if rand.Intn(100)+1 <= experimentConfig.DialUpPercentage {
			return true, experimentConfig.Data
		}
	}

	return false, experimentConfig.Data
}

func (fetcher *experimentConfigFetcher) IsExperimentActiveForUserWithData(ctx context.Context, experimentPrefix string, userID string) (bool, interface{}) {
	return fetcher.IsExperimentActiveForUserOrChannelWithData(ctx, experimentPrefix, userID, "")
}

func (fetcher *experimentConfigFetcher) IsExperimentActiveForChannelWithData(ctx context.Context, experimentPrefix string, channelID string) (bool, interface{}) {
	return fetcher.IsExperimentActiveForUserOrChannelWithData(ctx, experimentPrefix, "", channelID)
}

func (fetcher *experimentConfigFetcher) IsExperimentActiveForUserOrChannel(ctx context.Context, experimentPrefix string, userID string, channelID string) bool {
	isActive, _ := fetcher.IsExperimentActiveForUserOrChannelWithData(ctx, experimentPrefix, userID, channelID)
	return isActive
}

func (fetcher *experimentConfigFetcher) IsExperimentActiveForUser(ctx context.Context, experimentPrefix string, userID string) bool {
	return fetcher.IsExperimentActiveForUserOrChannel(ctx, experimentPrefix, userID, "")
}

func (fetcher *experimentConfigFetcher) IsExperimentActiveForChannel(ctx context.Context, experimentPrefix string, channelID string) bool {
	return fetcher.IsExperimentActiveForUserOrChannel(ctx, experimentPrefix, "", channelID)
}
