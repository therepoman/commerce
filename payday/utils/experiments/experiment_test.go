package experiments

import (
	"context"
	"errors"
	"testing"

	redis_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/redis"
	s3_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/s3"
	"github.com/go-redis/redis"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

const (
	testExperimentPrefix    = "test-experiment-prefix"
	testUserId              = "232889822"
	testChannelId           = "116076154"
	nonWhitelistedUserId    = "001"
	anyUserId               = "002"
	nonWhitelistedChannelId = "003"
	anyWhitelistedChannelId = "004"
	anyExperimentData       = "testData"

	validJsonExperimentConfigResponseWithDialUpOff = "{" +
		"\"dial_up_percentage\": 0," +
		"\"user_whitelist\": [\"" + testUserId + "\"]," +
		"\"channel_whitelist\": [\"" + testChannelId + "\"]" +
		"}"

	validJsonExperimentConfigResponseWithDialUpOn = "{" +
		"\"dial_up_percentage\": 100," +
		"\"user_whitelist\": [\"" + testUserId + "\"]," +
		"\"channel_whitelist\": [\"" + testChannelId + "\"]" +
		"}"

	validJsonExperimentConfigResponseWithDialUpHalf = "{" +
		"\"dial_up_percentage\": 50," +
		"\"user_whitelist\": [\"" + testUserId + "\"]," +
		"\"channel_whitelist\": [\"" + testChannelId + "\"]" +
		"}"

	validJsonExperimentConfigResponseWithDialUpOnWithData = "{" +
		"\"dial_up_percentage\": 100," +
		"\"user_whitelist\": [\"" + testUserId + "\"]," +
		"\"channel_whitelist\": [\"" + testChannelId + "\"]," +
		"\"data\": " + "\"" + anyExperimentData + "\"" +
		"}"

	invalidJsonExperimentConfigResponse = "{not JSON}"
)

func TestIsExperimentActiveForUserOrChannel(t *testing.T) {
	Convey("Given an S3 client and a Redis client", t, func() {
		ctx := context.Background()
		s3Client := new(s3_mock.IS3Client)
		redisClient := new(redis_mock.Client)

		experimentConfigFetcher := experimentConfigFetcher{
			S3Client:    s3Client,
			RedisClient: redisClient,
		}

		Convey("Doesn't call S3 when experiment config is cached", func() {
			redisClient.On("Get", mock.Anything, testExperimentPrefix).Return(redis.NewStringResult(validJsonExperimentConfigResponseWithDialUpOn, nil))

			experimentConfigFetcher.IsExperimentActiveForUserOrChannel(ctx, testExperimentPrefix, testUserId, testChannelId)

			s3Client.AssertNumberOfCalls(t, "LoadFile", 0)
		})

		Convey("Calls S3 when experiment config is not cached", func() {
			s3Client.On("LoadFile", mock.Anything, mock.Anything).Return([]byte(validJsonExperimentConfigResponseWithDialUpOff), nil)
			redisClient.On("Get", mock.Anything, testExperimentPrefix).Return(redis.NewStringResult("", nil))
			redisClient.On("Set", mock.Anything, testExperimentPrefix, validJsonExperimentConfigResponseWithDialUpOff, experimentConfigCacheTTL).Return(redis.NewStatusCmd(nil))

			experimentConfigFetcher.IsExperimentActiveForUserOrChannel(ctx, testExperimentPrefix, testUserId, testChannelId)

			s3Client.AssertNumberOfCalls(t, "LoadFile", 1)
		})

		Convey("Given no cached experiment config is found", func() {
			redisClient.On("Get", mock.Anything, testExperimentPrefix).Return(redis.NewStringResult("", nil))

			Convey("Returns false when call to S3 fails", func() {
				s3Client.On("LoadFile", mock.Anything, mock.Anything).Return(nil, errors.New("test error"))

				isExperimentActive := experimentConfigFetcher.IsExperimentActiveForUserOrChannel(ctx, testExperimentPrefix, testUserId, testChannelId)

				So(isExperimentActive, ShouldBeFalse)
			})

			Convey("Returns false when result from S3 cannot be unmarshalled into ExperimentConfig", func() {
				s3Client.On("LoadFile", mock.Anything, mock.Anything).Return([]byte(invalidJsonExperimentConfigResponse), nil)
				redisClient.On("Set", mock.Anything, testExperimentPrefix, invalidJsonExperimentConfigResponse, experimentConfigCacheTTL).Return(redis.NewStatusCmd(nil))

				isExperimentActive := experimentConfigFetcher.IsExperimentActiveForUserOrChannel(ctx, testExperimentPrefix, testUserId, testChannelId)

				So(isExperimentActive, ShouldBeFalse)
			})

			Convey("Given S3 returns successfully with valid experiment config that is not dialed up", func() {
				s3Client.On("LoadFile", mock.Anything, mock.Anything).Return([]byte(validJsonExperimentConfigResponseWithDialUpOff), nil)
				redisClient.On("Set", mock.Anything, testExperimentPrefix, validJsonExperimentConfigResponseWithDialUpOff, experimentConfigCacheTTL).Return(redis.NewStatusCmd(nil))

				Convey("Returns true when user ID is whitelisted", func() {
					isExperimentActive := experimentConfigFetcher.IsExperimentActiveForUserOrChannel(ctx, testExperimentPrefix, testUserId, nonWhitelistedChannelId)

					So(isExperimentActive, ShouldBeTrue)
				})

				Convey("Returns true when channel ID is whitelisted", func() {
					isExperimentActive := experimentConfigFetcher.IsExperimentActiveForUserOrChannel(ctx, testExperimentPrefix, nonWhitelistedUserId, testChannelId)

					So(isExperimentActive, ShouldBeTrue)
				})

				Convey("Returns false when channel ID and user ID are not whitelisted", func() {
					isExperimentActive := experimentConfigFetcher.IsExperimentActiveForUserOrChannel(ctx, testExperimentPrefix, nonWhitelistedUserId, nonWhitelistedChannelId)

					So(isExperimentActive, ShouldBeFalse)
				})
			})

			Convey("Given S3 returns successfully with valid experiment config that is dialed up", func() {
				s3Client.On("LoadFile", mock.Anything, mock.Anything).Return([]byte(validJsonExperimentConfigResponseWithDialUpOn), nil)
				redisClient.On("Set", mock.Anything, testExperimentPrefix, validJsonExperimentConfigResponseWithDialUpOn, experimentConfigCacheTTL).Return(redis.NewStatusCmd(nil))

				Convey("Returns true regardless of user ID and channel ID", func() {
					isExperimentActive := experimentConfigFetcher.IsExperimentActiveForUserOrChannel(ctx, testExperimentPrefix, anyUserId, anyWhitelistedChannelId)

					So(isExperimentActive, ShouldBeTrue)
				})
			})

			Convey("Given S3 returns successfully with valid experiment config that is partially dialed up", func() {
				s3Client.On("LoadFile", mock.Anything, mock.Anything).Return([]byte(validJsonExperimentConfigResponseWithDialUpHalf), nil)
				redisClient.On("Set", mock.Anything, testExperimentPrefix, validJsonExperimentConfigResponseWithDialUpHalf, experimentConfigCacheTTL).Return(redis.NewStatusCmd(nil))

				Convey("Always returns the same treatment for a single user", func() {
					sizeOfTestRun := 100
					results := make([]bool, sizeOfTestRun)

					for i := 0; i < sizeOfTestRun; i++ {
						results[i] = experimentConfigFetcher.IsExperimentActiveForUserOrChannel(ctx, testExperimentPrefix, nonWhitelistedChannelId, anyWhitelistedChannelId)
					}

					for j := 0; j < sizeOfTestRun-1; j++ {
						So(results[j], ShouldEqual, results[j+1])
					}
				})
			})
		})
	})
}

func TestIsExperimentActiveForUserOrChannelWithData(t *testing.T) {
	Convey("Given an S3 client and a Redis client", t, func() {
		s3Client := new(s3_mock.IS3Client)
		redisClient := new(redis_mock.Client)

		experimentConfigFetcher := experimentConfigFetcher{
			S3Client:    s3Client,
			RedisClient: redisClient,
		}

		Convey("Given no cached experiment config is found", func() {
			redisClient.On("Get", mock.Anything, testExperimentPrefix).Return(redis.NewStringResult("", nil))

			Convey("Given S3 returns successfully with valid experiment config that has data and is dialed up", func() {
				s3Client.On("LoadFile", mock.Anything, mock.Anything).Return([]byte(validJsonExperimentConfigResponseWithDialUpOnWithData), nil)
				redisClient.On("Set", mock.Anything, testExperimentPrefix, validJsonExperimentConfigResponseWithDialUpOnWithData, experimentConfigCacheTTL).Return(redis.NewStatusCmd(nil))

				Convey("Returns true and data", func() {
					isExperimentActive, data := experimentConfigFetcher.IsExperimentActiveForUserOrChannelWithData(context.Background(), testExperimentPrefix, anyUserId, anyWhitelistedChannelId)

					So(isExperimentActive, ShouldBeTrue)
					So(data.(string), ShouldEqual, anyExperimentData)
				})
			})
		})
	})
}
