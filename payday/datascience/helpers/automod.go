package helpers

import (
	"context"
	"time"

	zumaapi "code.justin.tv/chat/zuma/app/api"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/datascience"
	"code.justin.tv/commerce/payday/middleware"
	"code.justin.tv/commerce/payday/models"
	modelsApi "code.justin.tv/commerce/payday/models/api"
	"code.justin.tv/commerce/payday/oauth"
)

func TrackAutomodMessage(ctx context.Context, dataScienceClient datascience.DataScience, bitsEvent *modelsApi.CreateEventRequest, actionType models.AutoModOutcome) {
	if bitsEvent == nil {
		return
	}

	clientId := middleware.GetClientID(ctx)
	platform, _ := oauth.GetClient(clientId)

	datascienceEvent := &models.BitsAutoModMessageEnforcement{
		ActionType:        actionType,
		UserID:            bitsEvent.UserId,
		ChannelID:         bitsEvent.ChannelId,
		Body:              bitsEvent.Message,
		RoomID:            bitsEvent.RoomID,
		EnforcementReason: zumaapi.AutoModEnforcementName,
		ServerTime:        time.Now(),
		MessageID:         bitsEvent.EventId,
		CanonicalClientID: platform,
		ClientID:          clientId,
	}

	go func(e *models.BitsAutoModMessageEnforcement) {
		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()

		err := dataScienceClient.TrackBitsEvent(ctx, datascience.BitsAutoModMessageEnforcement, e)
		if err != nil {
			log.WithError(err).WithField("AutoModOutcome", actionType).Error("Error tracking Bits Automod Message Enforcement event with data science")
		}
	}(datascienceEvent)
}
