package datascience

import (
	"context"

	"code.justin.tv/commerce/payday/httputils"
)

const (
	BitsUsedEventNameV2            = "bits_used_v2"
	BitsCheeredEventName           = "bits_cheered"
	BitsUsedErrorEventName         = "bits_used_error"
	BitsAcquiredEventNameV2        = "bits_acquired_v2"
	BitsBalanceEventName           = "bits_balance"
	BitsRemovedEventNameV2         = "bits_removed_v2"
	PartnerOnboardedEventName      = "bits_partner_onboarded"
	BitsChannelSettingsEventName   = "bits_channel_settings"
	BitsBadgeTierSettingsEventName = "bits_badge_tier_settings"
	BitsEmoteCheerEventName        = "bits_emote_cheer"
	BitsBadgeEntitleEventName      = "bits_badge_earned"
	BitsPricesEventName            = "bits_prices"
	BitsPartnerCheermoteEnabled    = "bits_partner_cheermote_enabled"
	BitsPartnerCheemoteDetail      = "bits_partner_cheermote_detail"
	BitsPartnerBadgeDetail         = "bits_partner_badge_detail"
	BitsRewardClaim                = "bits_reward_claim"
	BitsMobileUnavailable          = "bits_mobile_unavailable"
	BitsLeaderboardUpdate          = "bits_leaderboard_update"
	BitsUsedOnExtensionEventName   = "extension_use_bits_complete"
	BitsAutoModFlaggedMessage      = "bits_automod_flagged_message"
	BitsAutoModMessageEnforcement  = "bits_automod_message_enforcement"
	BitsBadgeTierNotification      = "bits_badge_tier_notification"
	BitsCheerZeroEventName         = "bits_cheer_zero"
	BitsGemNotificationCreatedName = "bits_gem_notification_created"
	BitsGemNotificationClearedName = "bits_gem_notification_cleared"
	BitsBadgeDisplaySelectionName  = "bits_badge_display_selection"
	AutoRefillAction               = "auto_refill_action"
	AutoRefillPurchaseSuccess      = "auto_refill_purchase_completed"
	AutoRefillPurchaseFailure      = "auto_refill_purchase_failure"
)

type DataScience interface {
	TrackBitsEvent(ctx context.Context, metricName string, properties interface{}) error
	TrackBatchBitsEvent(ctx context.Context, eventName string, properties []interface{}) error
}

type DataScienceApiParams struct {
	HttpUtilClient     httputils.HttpUtilClient
	DataScienceEnabled bool
	SpadeEnabled       bool
	SpadeURL           string
}

type DataScienceApiNoOp struct{}

func (this *DataScienceApiNoOp) TrackBitsEvent(ctx context.Context, metricName string, properties interface{}) error {
	return nil
}

func (this *DataScienceApiNoOp) TrackBatchBitsEvent(ctx context.Context, eventName string, properties []interface{}) error {
	return nil
}
