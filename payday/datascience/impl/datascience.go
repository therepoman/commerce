package impl

import (
	"context"
	"fmt"
	"reflect"
	"strings"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/datascience"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/middleware"
	"code.justin.tv/commerce/payday/oauth"
	"code.justin.tv/commerce/payday/user"
	"code.justin.tv/commerce/payday/userservice"
	"code.justin.tv/commerce/payday/utils/golang"
	string_utils "code.justin.tv/commerce/payday/utils/strings"
)

const (
	channelIdTag      = "channelID"
	channelNameTag    = "channelname"
	userIdTag         = "userID"
	userNameTag       = "username"
	adminIdTag        = "adminID"
	adminNameTag      = "adminname"
	spadeMaxBatchSize = 8
)

type DataScienceApi struct {
	trackingHelper DatascienceTrackingHelper
	spadeEnabled   bool
	UserFetcher    userservice.Fetcher `inject:""`
}

// null object pattern to allow turning off collection of metrics with
// DataScienceEnabled flag for staging and testing
func NewDataScienceApi(params datascience.DataScienceApiParams) (datascience.DataScience, error) {
	if !params.DataScienceEnabled {
		return &datascience.DataScienceApiNoOp{}, nil
	}

	httpClient := &datascienceHttpClientImpl{
		HttpUtilClient: params.HttpUtilClient,
		spadeURL:       params.SpadeURL,
	}

	return &DataScienceApi{
		trackingHelper: httpClient,
		spadeEnabled:   params.SpadeEnabled,
	}, nil
}

func NewDataScienceApiWithUserService(params datascience.DataScienceApiParams, uf userservice.Fetcher) (datascience.DataScience, error) {
	if !params.DataScienceEnabled {
		return &datascience.DataScienceApiNoOp{}, nil
	}

	httpClient := &datascienceHttpClientImpl{
		HttpUtilClient: params.HttpUtilClient,
		spadeURL:       params.SpadeURL,
	}

	return &DataScienceApi{
		trackingHelper: httpClient,
		spadeEnabled:   params.SpadeEnabled,
		UserFetcher:    uf,
	}, nil
}

func (this *DataScienceApi) isDatascienceEnabled(twitchUserId string) bool {
	if !this.spadeEnabled {
		return false
	}

	if user.IsATestAccount(twitchUserId) {
		return false
	}

	return true
}

func (this *DataScienceApi) TrackBitsEvent(ctx context.Context, metricName string, properties interface{}) error {
	spade, enabled, err := this.getDataScienceObjects(ctx, properties)
	if err != nil {
		return err
	}

	if this.spadeEnabled && enabled {
		err := this.trackingHelper.TrackSpadeEvent(ctx, metricName, spade)
		if err != nil {
			return err
		}
	}

	return nil
}

func (this *DataScienceApi) TrackBatchBitsEvent(ctx context.Context, eventName string, properties []interface{}) error {
	spadeEvents := make([]interface{}, 0)

	for _, iVal := range properties {
		spadeEvent, enabled, err := this.getDataScienceObjects(ctx, iVal)
		if err != nil {
			return err
		}

		if enabled {
			spadeEvents = append(spadeEvents, spadeEvent)
		}
	}

	if this.spadeEnabled {
		spadeEventBatch := make([]interface{}, 0)
		for i := 0; i < len(spadeEvents); i++ {
			spadeEventBatch = append(spadeEventBatch, spadeEvents[i])
			if len(spadeEventBatch) == spadeMaxBatchSize || i == len(spadeEvents)-1 {
				err := this.trackingHelper.TrackSpadeBatchEvent(ctx, eventName, spadeEventBatch)
				if err != nil {
					log.WithError(err).WithField("spadeEventBatch", spadeEventBatch).Error("Error publishing batch to spade")
					return err
				}
				spadeEventBatch = make([]interface{}, 0)
			}
		}
	}
	return nil
}

func (this *DataScienceApi) getLoginName(ctx context.Context, userId string) string {
	if userId == "" {
		return ""
	}

	userRecord, err := this.UserFetcher.Fetch(ctx, userId)
	var userLogin string
	if err != nil {
		log.WithError(err).WithField("userId", userId).Error("Error looking up user from user service for userId")
	} else {
		if userRecord != nil && userRecord.Login != nil {
			userLogin = *userRecord.Login
		} else {
			log.WithField("userId", userId).Error("User service response user is nil for userId")
		}
	}
	return userLogin
}

func (this *DataScienceApi) getDataScienceObjects(ctx context.Context, iVal interface{}) (spade interface{}, datascienceEnbled bool, errOut error) {
	val := reflect.ValueOf(iVal)

	switch val.Kind() {
	case reflect.Ptr:
		if val.IsNil() {
			return nil, false, fmt.Errorf("Passed nil pointer to type %v", val.Type())
		}

		val = val.Elem()
	default:
		return nil, false, fmt.Errorf("Cannot marshall non pointer type %v", val.Type())
	}

	t := val.Type()
	if t.Kind() != reflect.Struct {
		return nil, false, errors.New("Cannot set values for non struct type")
	}

	valueMap := make(map[string]reflect.Value)
	for i := 0; i < t.NumField(); i++ {
		f := t.Field(i)
		tag := f.Tag.Get("datascience")

		if string_utils.Blank(tag) {
			continue
		}

		fVal := val.Field(i)
		valueMap[tag] = fVal
	}

	serverTime := time.Now()
	userInfo := make(map[string]reflect.Value)
	dsEnabled := true
	for k, v := range valueMap {
		parts := strings.Split(k, ",")

		if len(parts) < 1 {
			return nil, false, errors.Newf("could not parse struct tag: %s", k)
		}

		tags := make(map[string]bool)
		for _, elem := range parts {
			tags[strings.TrimSpace(elem)] = true
		}

		for tag := range tags {
			switch tag {
			case "servertime":
				if !v.CanInterface() {
					return nil, false, errors.Newf("Cannot get server time from field %v", v)
				}

				i := v.Interface()
				t, ok := i.(time.Time)
				if !ok {
					return nil, false, errors.Newf("Cannot set non-time server time field %v", t)
				}

				if t != reflect.Zero(v.Type()).Interface() {
					serverTime = t
				}
			case channelIdTag:
				if v.Kind() != reflect.String {
					return nil, false, errors.Newf("Cannot use non string value %v as channelId", v)
				}

				_, duplicate := userInfo[channelIdTag]
				if duplicate {
					return nil, false, errors.Newf("Struct %v contained more than 1 channelID", iVal)
				}

				userInfo[channelIdTag] = v
				id := v.String()
				dsEnabled = dsEnabled && this.isDatascienceEnabled(id)
			case channelNameTag:
				if v.Kind() != reflect.String {
					return nil, false, errors.Newf("Cannot use non string value %v as channelname", v)
				} else if !v.CanSet() {
					return nil, false, errors.Newf("Cannot set channelname value %v", v)
				}

				if v.String() != "" {
					continue
				}

				_, duplicate := userInfo[channelNameTag]
				if duplicate {
					return nil, false, errors.Newf("Struct %v contained more than 1 channelname", iVal)
				}

				userInfo[channelNameTag] = v
			case userIdTag:
				if v.Kind() != reflect.String {
					return nil, false, errors.Newf("Cannot use non string value %v as userId", v)
				}

				_, duplicate := userInfo[userIdTag]
				if duplicate {
					return nil, false, errors.Newf("Struct %v contained more than 1 userID", iVal)
				}

				userInfo[userIdTag] = v
				id := v.String()
				dsEnabled = dsEnabled && this.isDatascienceEnabled(id)
			case userNameTag:
				if v.Kind() != reflect.String {
					return nil, false, errors.Newf("Cannot use non string value %v as channelname", v)
				} else if !v.CanSet() {
					return nil, false, errors.Newf("Cannot set channelname value %v", v)
				}

				if v.String() != "" {
					continue
				}

				_, duplicate := userInfo[userNameTag]
				if duplicate {
					return nil, false, errors.Newf("Struct %v contained more than 1 username", iVal)
				}

				userInfo[userNameTag] = v
			case adminIdTag:
				if v.Kind() != reflect.String {
					return nil, false, errors.Newf("Cannot use non string value %v as userId", v)
				}

				_, duplicate := userInfo[adminIdTag]
				if duplicate {
					return nil, false, errors.Newf("Struct %v contained more than 1 userID", iVal)
				}

				userInfo[adminIdTag] = v
				id := v.String()
				dsEnabled = dsEnabled && this.isDatascienceEnabled(id)
			case adminNameTag:
				if v.Kind() != reflect.String {
					return nil, false, errors.Newf("Cannot use non string value %v as channelname", v)
				} else if !v.CanSet() {
					return nil, false, errors.Newf("Cannot set channelname value %v", v)
				}

				if v.String() != "" {
					continue
				}

				_, duplicate := userInfo[adminNameTag]
				if duplicate {
					return nil, false, errors.Newf("Struct %v contained more than 1 username", iVal)
				}

				userInfo[adminNameTag] = v
			}
		}
	}

	datascienceEnbled = dsEnabled

	for tag, v := range userInfo {
		switch tag {
		case channelNameTag:
			idVal, exists := userInfo[channelIdTag]
			if !exists {
				return nil, false, errors.New("could not get user id for user name")
			}

			id := idVal.String()
			name := this.getLoginName(ctx, id)
			v.SetString(name)

		case userNameTag:
			idVal, exists := userInfo[userIdTag]
			if !exists {
				return nil, false, errors.New("could not get user id for user name")
			}

			id := idVal.String()
			name := this.getLoginName(ctx, id)
			v.SetString(name)
		case adminNameTag:
			idVal, exists := userInfo[adminIdTag]
			if !exists {
				return nil, false, errors.New("could not get user id for user name")
			}

			id := idVal.String()
			name := this.getLoginName(ctx, id)
			v.SetString(name)
		}
	}

	var clientId string
	c := ctx.Value(middleware.ClientIDContextKey)

	if c == nil {
		clientId = ""
	} else {
		tmp, ok := c.(string)
		if !ok {
			return nil, false, errors.New("Could not get client id from context")
		}
		clientId = tmp
	}

	platform, _ := oauth.GetClient(clientId)

	platformVal := reflect.ValueOf(platform)
	clientIdVal := reflect.ValueOf(clientId)
	serverTimeVal := reflect.ValueOf(serverTime.Unix())

	spadeStruct := reflect.StructOf([]reflect.StructField{
		getAnonymousStructField(val),
		{
			Name: "ServerTime",
			Type: serverTimeVal.Type(),
			Tag:  reflect.StructTag("json:\"server_time\""),
		},
		{
			Name: "ClientID",
			Type: clientIdVal.Type(),
			Tag:  reflect.StructTag("json:\"client_id,omitempty\""),
		},
		{
			Name: "Platform",
			Type: platformVal.Type(),
			Tag:  reflect.StructTag("json:\"platform,omitempty\""),
		},
	})

	spadeValue := reflect.New(spadeStruct).Elem()

	spadeValue.Field(0).Set(val)
	spadeValue.FieldByName("ServerTime").Set(serverTimeVal)
	spadeValue.FieldByName("ClientID").Set(clientIdVal)
	spadeValue.FieldByName("Platform").Set(platformVal)

	spade = spadeValue.Interface()
	errOut = nil
	return
}

func getAnonymousStructField(val reflect.Value) reflect.StructField {
	// TODO: Once everybody is fully on go1.9+, we should deprecate this check
	if golang.IsGo19OrNewer() {
		return reflect.StructField{
			Name:      "SF1",
			Anonymous: true,
			Type:      val.Type(),
		}
	} else {
		return reflect.StructField{
			Name: "",
			Type: val.Type(),
		}
	}
}
