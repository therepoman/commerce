package impl

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"net/http"
	"net/url"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/httputils"
	"code.justin.tv/commerce/payday/models"
)

type DatascienceTrackingHelper interface {
	TrackSpadeEvent(ctx context.Context, name string, properties interface{}) error
	TrackSpadeBatchEvent(ctx context.Context, name string, properties []interface{}) error
}

type datascienceHttpClientImpl struct {
	httputils.HttpUtilClient
	spadeURL string
}

func (d *datascienceHttpClientImpl) TrackSpadeEvent(ctx context.Context, name string, properties interface{}) error {
	return d.trackEvent(ctx, d.spadeURL, name, properties)
}

func (d *datascienceHttpClientImpl) TrackSpadeBatchEvent(ctx context.Context, name string, properties []interface{}) error {
	return d.trackBatchEvent(ctx, d.spadeURL, name, properties)
}

func (d *datascienceHttpClientImpl) trackEvent(ctx context.Context, baseURL string, name string, properties interface{}) error {
	req, err := createDataScienceRequest(baseURL, name, properties)
	if err != nil {
		return err
	}

	_, statusCode, err := d.HttpPerformRequest(ctx, req)
	if err != nil {
		msg := "Encountered unexpected error while tracking event"
		log.WithError(err).WithField("baseURL", baseURL).Error(msg)
		return errors.Notef(err, msg)
	}

	if statusCode != http.StatusOK && statusCode != http.StatusNoContent {
		msg := "Encountered unexpected status code while tracking event"
		log.WithError(err).WithFields(log.Fields{
			"statusCode": statusCode,
			"baseURL":    baseURL,
		}).Error(msg)
		return errors.New(msg)
	}

	return nil
}

func (d *datascienceHttpClientImpl) trackBatchEvent(ctx context.Context, baseURL string, name string, properties []interface{}) error {
	req, err := createBatchDataScienceRequest(baseURL, name, properties)
	if err != nil {
		return err
	}

	_, statusCode, err := d.HttpPerformRequest(ctx, req)
	if err != nil {
		msg := "Encountered unexpected error while tracking batch event"
		log.WithError(err).WithField("baseURL", baseURL).Error(msg)
		return errors.Notef(err, msg)
	}

	if statusCode != http.StatusOK && statusCode != http.StatusNoContent {
		msg := "Encountered unexpected status code while tracking batch event"
		log.WithFields(log.Fields{
			"statusCode": statusCode,
			"baseURL":    baseURL,
		}).Error(msg)
		return errors.New(msg)
	}

	return nil
}

func createBatchDataScienceRequest(baseURL string, name string, properties []interface{}) (*http.Request, error) {
	base, err := url.Parse(baseURL)
	if err != nil {
		return nil, err
	}

	data := make([]models.DataScienceEvent, len(properties))
	for i, property := range properties {
		data[i] = models.DataScienceEvent{Event: name, Properties: property}
	}

	return createTrackingHttpRequest(base, data)
}

func createDataScienceRequest(baseURL string, name string, properties interface{}) (*http.Request, error) {
	base, err := url.Parse(baseURL)
	if err != nil {
		return nil, err
	}

	data := models.DataScienceEvent{Event: name, Properties: properties}
	return createTrackingHttpRequest(base, data)
}

func createTrackingHttpRequest(baseUrl *url.URL, data interface{}) (*http.Request, error) {
	jsonData, err := json.Marshal(data)
	if err != nil {
		return nil, err
	}

	v := url.Values{}
	v.Add("data", base64.StdEncoding.EncodeToString(jsonData))
	httpRequest := http.Request{
		Method: "GET",
		URL:    httputils.CreateURL(baseUrl, v),
		Header: make(http.Header),
	}

	return &httpRequest, nil
}
