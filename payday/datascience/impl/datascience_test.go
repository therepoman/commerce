package impl

import (
	"context"
	"encoding/json"
	"testing"
	"time"

	"code.justin.tv/commerce/payday/middleware"
	impl_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/datascience/impl"
	userservice_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/userservice"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/oauth"
	"code.justin.tv/commerce/payday/test"
	userservice_models "code.justin.tv/web/users-service/models"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

var mockedTrackingHelper *impl_mock.DatascienceTrackingHelper
var mockedUserServiceFetcher *userservice_mock.Fetcher

func setup() {
	mockedTrackingHelper = new(impl_mock.DatascienceTrackingHelper)
	mockedUserServiceFetcher = new(userservice_mock.Fetcher)
}

func TestNoTrackBitsEventOnTestUser(t *testing.T) {
	setup()

	userID := "andymc"
	mockedUserServiceFetcher.On("Fetch", mock.Anything, mock.Anything).Return(&userservice_models.Properties{
		Login: &userID,
	}, nil)

	ds := &DataScienceApi{
		trackingHelper: mockedTrackingHelper,
		spadeEnabled:   true,
		UserFetcher:    mockedUserServiceFetcher,
	}

	gb := &models.BitsUsed{
		TransactionId: "5b1cd03b-0e36-4915-b00f-482df7612306",
		UserId:        test.RandomTestUserId(),
		ChannelID:     "39141793",
		UsedTotal:     1,
		ServerTime:    time.Unix(1450233031389, 0),
	}

	err := ds.TrackBitsEvent(context.Background(), "test event", gb)
	if err != nil {
		assert.NoError(t, err)
		return
	}

	mockedTrackingHelper.AssertNumberOfCalls(t, "TrackSpadeEvent", 0)
}

func TestCreateDataScienceRequest(t *testing.T) {
	properties := struct {
		id   int
		name string
	}{1, "foo"}

	req, err := createDataScienceRequest("http://testspade/track", "test_event", properties)
	assert.Nil(t, err)
	assert.Equal(t, req.URL.String(), "http://testspade/track?data=eyJldmVudCI6InRlc3RfZXZlbnQiLCJwcm9wZXJ0aWVzIjp7fX0%3D")
}

func TestMarshalSpadeEvent(t *testing.T) {
	setup()
	userName := "andymc"
	mockedUserServiceFetcher.On("Fetch", mock.Anything, mock.Anything).Return(&userservice_models.Properties{
		Login: &userName,
	}, nil)

	timeOfRequest := time.Now()
	onboarded := models.PartnerOnboarded{
		ChannelID:    "1234",
		ChannelLogin: "login1234",
		PartnerType:  "partner",
		ServerTime:   timeOfRequest,
	}

	ds := &DataScienceApi{
		trackingHelper: mockedTrackingHelper,
		spadeEnabled:   true,
		UserFetcher:    mockedUserServiceFetcher,
	}

	spadeValues, err := getSpadeMap(context.Background(), ds, &onboarded)
	if err != nil {
		assert.NoError(t, err)
		return
	}

	channelID, ok := spadeValues["channel_id"]
	if ok {
		assert.Equal(t, onboarded.ChannelID, channelID)
	} else {
		assert.Fail(t, "channel id not contained in json output")
		return
	}

	channelLogin, ok := spadeValues["channel_login"]
	if ok {
		assert.Equal(t, onboarded.ChannelLogin, channelLogin)
	} else {
		assert.Fail(t, "channel login not contained in json output")
		return
	}

	partnerType, ok := spadeValues["partner_type"]
	if ok {
		assert.Equal(t, onboarded.PartnerType, partnerType)
	} else {
		assert.Fail(t, "partner type not contained in json output")
		return
	}
}

func TestMarshalClientId(t *testing.T) {
	setup()
	userName := "andymc"
	mockedUserServiceFetcher.On("Fetch", mock.Anything, mock.Anything).Return(&userservice_models.Properties{
		Login: &userName,
	}, nil)

	timeOfRequest := time.Now()
	onboarded := models.PartnerOnboarded{
		ChannelID:    "1234",
		ChannelLogin: "login1234",
		PartnerType:  "partner",
		ServerTime:   timeOfRequest,
	}

	ds := &DataScienceApi{
		trackingHelper: mockedTrackingHelper,
		spadeEnabled:   true,
		UserFetcher:    mockedUserServiceFetcher,
	}

	//test for no client id set
	spadeValues, err := getSpadeMap(context.Background(), ds, &onboarded)
	if err != nil {
		assert.NoError(t, err)
		return
	}

	_, exists := spadeValues["client_id"]
	assert.False(t, exists)

	platform, ok := spadeValues["platform"]
	if ok {
		assert.Equal(t, oauth.UnknownPlatform, platform)
	} else {
		assert.Fail(t, "platform not contained in json output")
		return
	}

	//Test for unknown client id
	clientId := test.RandomString(10)
	ctx := context.WithValue(context.Background(), middleware.ClientIDContextKey, clientId)
	spadeValues, err = getSpadeMap(ctx, ds, &onboarded)
	if err != nil {
		assert.NoError(t, err)
		return
	}
	verifyClientIdAndPlatform(t, spadeValues, clientId, oauth.UnknownPlatform)

	//Test for web-client id
	clientId = oauth.WebClientClientID
	ctx = context.WithValue(context.Background(), middleware.ClientIDContextKey, clientId)
	spadeValues, err = getSpadeMap(ctx, ds, &onboarded)
	if err != nil {
		assert.NoError(t, err)
		return
	}
	verifyClientIdAndPlatform(t, spadeValues, oauth.WebClientClientID, oauth.WebClientPlatform)

	//Test android platform
	clientId = oauth.AndroidClientId
	ctx = context.WithValue(context.Background(), middleware.ClientIDContextKey, clientId)
	spadeValues, err = getSpadeMap(ctx, ds, &onboarded)
	if err != nil {
		assert.NoError(t, err)
		return
	}
	verifyClientIdAndPlatform(t, spadeValues, oauth.AndroidClientId, oauth.AndroidPlatform)

	//test iphone platform
	clientId = oauth.IPhoneClientId
	ctx = context.WithValue(context.Background(), middleware.ClientIDContextKey, clientId)
	spadeValues, err = getSpadeMap(ctx, ds, &onboarded)
	if err != nil {
		assert.NoError(t, err)
		return
	}
	verifyClientIdAndPlatform(t, spadeValues, oauth.IPhoneClientId, oauth.IPhonePlatform)

	//test ipad platform
	clientId = oauth.IPadClientId
	ctx = context.WithValue(context.Background(), middleware.ClientIDContextKey, clientId)
	spadeValues, err = getSpadeMap(ctx, ds, &onboarded)
	if err != nil {
		assert.NoError(t, err)
		return
	}
	verifyClientIdAndPlatform(t, spadeValues, oauth.IPadClientId, oauth.IPadPlatform)
}

func verifyClientIdAndPlatform(t *testing.T, values map[string]interface{}, clientId string, platform string) {
	clientIdVal, exists := values["client_id"]
	if exists {
		assert.Equal(t, clientId, clientIdVal)
	} else {
		assert.Fail(t, "Client Id was not contained in map")
	}

	platformVal, exists := values["platform"]
	if exists {
		assert.Equal(t, platform, platformVal)
	} else {
		assert.Fail(t, "platform not contained in json output")
	}
}

func getSpadeMap(ctx context.Context, ds *DataScienceApi, event interface{}) (spade map[string]interface{}, err error) {
	spadeIFace, _, err := ds.getDataScienceObjects(ctx, event)
	if err != nil {
		return nil, err
	}

	bod, err := json.Marshal(spadeIFace)
	if err != nil {
		return nil, err
	}

	spadeValues := make(map[string]interface{})
	err = json.Unmarshal(bod, &spadeValues)
	if err != nil {
		return nil, err
	}

	return spadeValues, nil
}

func testTrackEvent(t *testing.T, spadeEnabled bool) {
	setup()
	mockedTrackingHelper.On("TrackSpadeEvent", mock.Anything, mock.Anything, mock.Anything).
		Return(nil)
	userName := "andymc"
	mockedUserServiceFetcher.On("Fetch", mock.Anything, mock.Anything).Return(&userservice_models.Properties{
		Login: &userName,
	}, nil)

	ds := &DataScienceApi{
		trackingHelper: mockedTrackingHelper,
		spadeEnabled:   spadeEnabled,
		UserFetcher:    mockedUserServiceFetcher,
	}

	gb := &models.BitsUsed{
		TransactionId: "5b1cd03b-0e36-4915-b00f-482df7612306",
		UserId:        "65380937",
		ChannelID:     "39141793",
		UsedTotal:     1,
		ServerTime:    time.Unix(1450233031389, 0),
	}

	err := ds.TrackBitsEvent(context.Background(), "test event", gb)
	if err != nil {
		assert.NoError(t, err)
		return
	}

	var expected int
	if spadeEnabled {
		expected = 1
	} else {
		expected = 0
	}

	mockedTrackingHelper.AssertNumberOfCalls(t, "TrackSpadeEvent", expected)
}

func TestTrackUsedEventOnlySpade(t *testing.T) {
	testTrackEvent(t, true)
}

func TestTrackBitsEventNoTracking(t *testing.T) {
	testTrackEvent(t, false)
}
