package audit

import (
	"bytes"
	"text/template"

	"code.justin.tv/commerce/payday/file"
)

const templateFileName = "auditorLambda.js.template"

var templateFilePaths = []string{"template/", "../template/"}

type AuditLambdaTemplate struct {
	FullTableName     string
	S3Bucket          string
	RecordKeySelector string
}

func getTemplateFilePath() (string, error) {
	return file.SearchFileInPaths(templateFileName, templateFilePaths)
}

func (templateVals *AuditLambdaTemplate) GenerateCodeFile() ([]byte, error) {
	filePath, err := getTemplateFilePath()
	if err != nil {
		return nil, err
	}

	tmpl, err := template.ParseFiles(filePath)
	if err != nil {
		return nil, err
	}

	var generatedFileBuffer bytes.Buffer
	err = tmpl.Execute(&generatedFileBuffer, templateVals)
	if err != nil {
		return nil, err
	}

	return generatedFileBuffer.Bytes(), err
}
