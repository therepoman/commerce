package partner_test

import (
	"context"
	"testing"

	ripleyclient_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/backend/clients/ripley"
	"code.justin.tv/commerce/payday/partner"
	riptwirp "code.justin.tv/revenue/ripley/rpc"
	"github.com/go-errors/errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

type testContext struct {
	channelID      string
	ripleyErr      error
	partnerProgram bool
	isAffiliate    bool
}

func setup(t *testing.T, c testContext) partner.IPartnerManager {
	resp := &riptwirp.GetPayoutTypeResponse{
		PayoutType: &riptwirp.PayoutType{
			ChannelId:   c.channelID,
			IsAffiliate: c.isAffiliate,
			IsPartner:   c.partnerProgram,
		},
	}
	mockRipley := new(ripleyclient_mock.Client)
	mockRipley.On("GetPayoutType", mock.Anything, mock.Anything).Return(resp, c.ripleyErr)

	return &partner.PartnerManager{
		RipleyClient: mockRipley,
	}
}

func Test__Partner_No__Affiliate_No(t *testing.T) {
	pm := setup(t, testContext{
		channelID:      "123",
		partnerProgram: false,
		isAffiliate:    false,
	})
	partnerType, err := pm.GetPartnerType(context.Background(), "123")
	assert.NoError(t, err)
	assert.Equal(t, partner.NonPartnerPartnerType, partnerType)
}

func Test__Partner_No__Affiliate_Yes(t *testing.T) {
	pm := setup(t, testContext{
		channelID:      "123",
		partnerProgram: false,
		isAffiliate:    true,
	})
	partnerType, err := pm.GetPartnerType(context.Background(), "123")
	assert.NoError(t, err)
	assert.Equal(t, partner.AffiliatePartnerType, partnerType)
}

func Test__Partner_No__Affiliate_Err(t *testing.T) {
	pm := setup(t, testContext{
		channelID:      "123",
		partnerProgram: false,
		ripleyErr:      errors.New("test err"),
	})
	partnerType, err := pm.GetPartnerType(context.Background(), "123")
	assert.Error(t, err)
	assert.Equal(t, partner.PartnerType(""), partnerType)
}

func Test__Partner_Yes__Affiliate_No(t *testing.T) {
	pm := setup(t, testContext{
		channelID:      "123",
		partnerProgram: true,
		isAffiliate:    false,
	})
	partnerType, err := pm.GetPartnerType(context.Background(), "123")
	assert.NoError(t, err)
	assert.Equal(t, partner.TraditionalPartnerType, partnerType)
}

func Test__Partner_Yes__Affiliate_Yes(t *testing.T) {
	pm := setup(t, testContext{
		channelID:      "123",
		partnerProgram: true,
		isAffiliate:    true,
	})
	partnerType, err := pm.GetPartnerType(context.Background(), "123")
	assert.NoError(t, err)
	assert.Equal(t, partner.TraditionalPartnerType, partnerType)
}
