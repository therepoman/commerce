package partner

import (
	"context"

	"code.justin.tv/commerce/payday/backend/clients/ripley"
	"code.justin.tv/commerce/payday/user"
)

type PartnerType string

const (
	AffiliatePartnerType   = PartnerType("affiliate")   // Affiliate partners
	TraditionalPartnerType = PartnerType("traditional") // Traditional partners
	NonPartnerPartnerType  = PartnerType("non-partner") // Neither an affiliate partner nor a traditional partner
	UnknownPartnerType     = PartnerType("")            // We don't know their type due to downstream service call failures.

	BitsAmendmentStateSigned   BitsAmendmentState = "signed"
	BitsAmendmentStateUnsigned BitsAmendmentState = "unsigned"
)

var fakePartnerTuids = map[string]string{
	"qa_bits_extensions": "196291456",
	"primevideo":         "168843586",
}

type BitsAmendmentState string

type IPartnerManager interface {
	GetPartnerType(ctx context.Context, channelID string) (PartnerType, error)
}

type PartnerManager struct {
	RipleyClient ripley.Client
}

func IsEligibleForBits(partnerType PartnerType) bool {
	return partnerType == AffiliatePartnerType || partnerType == TraditionalPartnerType
}

func (m *PartnerManager) GetPartnerType(ctx context.Context, channelID string) (PartnerType, error) {
	if user.IsATestAccount(channelID) {
		return TraditionalPartnerType, nil
	}

	// TODO: Remove this hack that considers qa_bits_extensions and primevideo to be partners
	if channelID == fakePartnerTuids["qa_bits_extensions"] || channelID == fakePartnerTuids["primevideo"] {
		return TraditionalPartnerType, nil
	}

	resp, err := m.RipleyClient.GetPayoutType(ctx, channelID)
	if err != nil || resp == nil || resp.PayoutType == nil {
		return UnknownPartnerType, err
	}

	if resp.PayoutType.IsPartner {
		return TraditionalPartnerType, nil
	} else if resp.PayoutType.IsAffiliate {
		return AffiliatePartnerType, nil
	} else {
		return NonPartnerPartnerType, nil
	}
}

func GetPartnerTypeFromValue(value string) PartnerType {
	switch value {
	case "affiliate":
		return AffiliatePartnerType
	case "traditional":
		return TraditionalPartnerType
	case "non-partner":
		return NonPartnerPartnerType
	default:
		return UnknownPartnerType
	}
}
