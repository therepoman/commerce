package oauth

const (
	IPhoneClientId           = "85lcqzxpb9bqu9z6ga1ol55du"
	IPadClientId             = "p9lhq6azjkdl72hs5xnt3amqu7vv8k2"
	AndroidClientId          = "kd1unb4b3q4t58fwlpcbzcbnm76a8fp"
	WebClientClientID        = "jzkbprff40iqj646a697cyrvl0zt2m6"
	TwilightInternalClientID = "ps1pu6on4i1x19vo6pgcjyzckr7kr1"
	TwilightClientID         = "kimne78kx3ncx6brgo4mv6wki5h1ko"
	DesktopAppClientID       = "jf3xu125ejjjt5cl4osdjci6oz6p93r"
	SpotlightClientID        = "yrml35ak347op87qu9l38z2tsptbob"
	UnknownPlatform          = "unknown"
	WebClientPlatform        = "web"
	AndroidPlatform          = "Android"
	IPadPlatform             = "iPad"
	IPhonePlatform           = "iPhone"
	DesktopPlatform          = "desktop"
	SpotlightPlatform        = "spotlight"
)

func GetClient(clientId string) (clientPlatform string, whitelisted bool) {
	switch clientId {
	case IPhoneClientId:
		clientPlatform = IPhonePlatform
		whitelisted = true
	case IPadClientId:
		clientPlatform = IPadPlatform
		whitelisted = true
	case AndroidClientId:
		clientPlatform = AndroidPlatform
		whitelisted = true
	case WebClientClientID:
		clientPlatform = WebClientPlatform
		whitelisted = true
	case TwilightInternalClientID:
		clientPlatform = WebClientPlatform
		whitelisted = true
	case TwilightClientID:
		clientPlatform = WebClientPlatform
		whitelisted = true
	case DesktopAppClientID:
		clientPlatform = DesktopPlatform
		whitelisted = true
	case SpotlightClientID:
		clientPlatform = SpotlightPlatform
		whitelisted = true
	default:
		clientPlatform = UnknownPlatform
		whitelisted = false
	}
	return
}
