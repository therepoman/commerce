package tmi

import (
	"context"
	"fmt"
	"time"

	"code.justin.tv/commerce/payday/clients/slack"
	"code.justin.tv/commerce/payday/clients/user"
	"code.justin.tv/commerce/payday/config"
	userUtils "code.justin.tv/commerce/payday/user"
	"code.justin.tv/commerce/payday/utils/strings"
)

type ITMIFailureSlacker interface {
	PostToSlack(eventID string, channelID string, userID string, message string, amount int32, isAnonymous bool, chatErr *string) error
}

type TMIFailureSlacker struct {
	UserService user.IUserServiceClientWrapper `inject:""`
	Slacker     slack.ISlacker                 `inject:""`
	Config      *config.Configuration          `inject:""`
}

type NoOpTMIFailureSlacker struct {
}

func (c *NoOpTMIFailureSlacker) PostToSlack(eventID string, channelID string, userID string, message string, amount int32, isAnonymous bool, chatErr *string) error {
	return nil
}

func (c *TMIFailureSlacker) PostToSlack(eventID string, channelID string, userID string, message string, amount int32, isAnonymous bool, optionalChatErr *string) error {
	if userUtils.IsATestAccount(channelID) || userUtils.IsATestAccount(userID) {
		return nil
	}

	if c.Config == nil || strings.Blank(c.Config.TMIFailuresSlackWebhookURL) {
		return nil
	}

	channelLogin := c.getLoginFromID(channelID)
	userLogin := c.getLoginFromID(userID)

	var text string
	if isAnonymous {
		text += fmt.Sprintf(":bogashh: *ANONYMOUS CHEER* :bogashh:\n")
	}

	var chatErr string
	if optionalChatErr == nil {
		chatErr = "nil"
	} else {
		chatErr = *optionalChatErr
	}

	text += fmt.Sprintf("%s `%d`\n", getEmote(amount, isAnonymous), amount)
	text += "```"
	text += fmt.Sprintf("Transaction ID: %s\n", eventID)
	text += fmt.Sprintf("User:           %s (%s)\n", userLogin, userID)
	text += fmt.Sprintf("Channel:        %s (%s)\n", channelLogin, channelID)
	text += fmt.Sprintf("MatchedAmount:  %d\n", amount)
	text += fmt.Sprintf("Message:        %s\n", message)
	text += fmt.Sprintf("TMI Error:      %s", chatErr)
	text += "```"

	return c.Slacker.PostMsg(c.Config.TMIFailuresSlackWebhookURL, text)
}

func (c *TMIFailureSlacker) getLoginFromID(id string) string {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	login := id
	resp, err := c.UserService.GetUserByID(ctx, id, nil)
	if err == nil && resp != nil && resp.Login != nil {
		login = *resp.Login
	}
	return login
}

func getEmote(amount int32, isAnonymous bool) string {
	var emote string
	if isAnonymous {
		emote = "anon"
	} else {
		emote = "chuckCheer"
	}

	var tier int
	switch {
	case amount < 100:
		tier = 1
	case amount < 1000:
		tier = 100
	case amount < 5000:
		tier = 1000
	case amount < 10000:
		tier = 5000
	case amount < 100000:
		tier = 10000
	default:
		tier = 100000
	}

	return fmt.Sprintf(":%s%d:", emote, tier)
}
