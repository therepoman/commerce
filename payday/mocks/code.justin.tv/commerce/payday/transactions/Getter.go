// Code generated by mockery v1.0.0. DO NOT EDIT.

package transactions_mock

import context "context"
import mock "github.com/stretchr/testify/mock"
import models "code.justin.tv/commerce/payday/models"
import paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
import time "time"

// Getter is an autogenerated mock type for the Getter type
type Getter struct {
	mock.Mock
}

// GetLastUpdatedTransactionsByUserOrChannel provides a mock function with given fields: ctx, userID, channelID, cursor, eventBefore, eventAfter, searchType
func (_m *Getter) GetLastUpdatedTransactionsByUserOrChannel(ctx context.Context, userID string, channelID string, cursor string, eventBefore *time.Time, eventAfter *time.Time, searchType models.TransactionSearchType) ([]*paydayrpc.Transaction, string, error) {
	ret := _m.Called(ctx, userID, channelID, cursor, eventBefore, eventAfter, searchType)

	var r0 []*paydayrpc.Transaction
	if rf, ok := ret.Get(0).(func(context.Context, string, string, string, *time.Time, *time.Time, models.TransactionSearchType) []*paydayrpc.Transaction); ok {
		r0 = rf(ctx, userID, channelID, cursor, eventBefore, eventAfter, searchType)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]*paydayrpc.Transaction)
		}
	}

	var r1 string
	if rf, ok := ret.Get(1).(func(context.Context, string, string, string, *time.Time, *time.Time, models.TransactionSearchType) string); ok {
		r1 = rf(ctx, userID, channelID, cursor, eventBefore, eventAfter, searchType)
	} else {
		r1 = ret.Get(1).(string)
	}

	var r2 error
	if rf, ok := ret.Get(2).(func(context.Context, string, string, string, *time.Time, *time.Time, models.TransactionSearchType) error); ok {
		r2 = rf(ctx, userID, channelID, cursor, eventBefore, eventAfter, searchType)
	} else {
		r2 = ret.Error(2)
	}

	return r0, r1, r2
}

// GetTransactionsByID provides a mock function with given fields: ctx, ids
func (_m *Getter) GetTransactionsByID(ctx context.Context, ids []string) ([]*paydayrpc.Transaction, error) {
	ret := _m.Called(ctx, ids)

	var r0 []*paydayrpc.Transaction
	if rf, ok := ret.Get(0).(func(context.Context, []string) []*paydayrpc.Transaction); ok {
		r0 = rf(ctx, ids)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]*paydayrpc.Transaction)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, []string) error); ok {
		r1 = rf(ctx, ids)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}
