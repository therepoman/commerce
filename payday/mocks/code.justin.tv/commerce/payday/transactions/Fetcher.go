// Code generated by mockery v1.0.0. DO NOT EDIT.

package transactions_mock

import context "context"
import dynamo "code.justin.tv/commerce/payday/dynamo"
import mock "github.com/stretchr/testify/mock"
import time "time"

// Fetcher is an autogenerated mock type for the Fetcher type
type Fetcher struct {
	mock.Mock
}

// FetchLastUpdatedTransactionsByUserOrChannel provides a mock function with given fields: ctx, userID, channelID, cursor, eventBefore, eventAfter
func (_m *Fetcher) FetchLastUpdatedTransactionsByUserOrChannel(ctx context.Context, userID string, channelID string, cursor string, eventBefore *time.Time, eventAfter *time.Time) ([]*dynamo.Transaction, string, error) {
	ret := _m.Called(ctx, userID, channelID, cursor, eventBefore, eventAfter)

	var r0 []*dynamo.Transaction
	if rf, ok := ret.Get(0).(func(context.Context, string, string, string, *time.Time, *time.Time) []*dynamo.Transaction); ok {
		r0 = rf(ctx, userID, channelID, cursor, eventBefore, eventAfter)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]*dynamo.Transaction)
		}
	}

	var r1 string
	if rf, ok := ret.Get(1).(func(context.Context, string, string, string, *time.Time, *time.Time) string); ok {
		r1 = rf(ctx, userID, channelID, cursor, eventBefore, eventAfter)
	} else {
		r1 = ret.Get(1).(string)
	}

	var r2 error
	if rf, ok := ret.Get(2).(func(context.Context, string, string, string, *time.Time, *time.Time) error); ok {
		r2 = rf(ctx, userID, channelID, cursor, eventBefore, eventAfter)
	} else {
		r2 = ret.Error(2)
	}

	return r0, r1, r2
}

// FetchTransactionsByID provides a mock function with given fields: ctx, ids
func (_m *Fetcher) FetchTransactionsByID(ctx context.Context, ids []string) ([]*dynamo.Transaction, error) {
	ret := _m.Called(ctx, ids)

	var r0 []*dynamo.Transaction
	if rf, ok := ret.Get(0).(func(context.Context, []string) []*dynamo.Transaction); ok {
		r0 = rf(ctx, ids)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]*dynamo.Transaction)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, []string) error); ok {
		r1 = rf(ctx, ids)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// FetchTransactionsByUserIDAndTransactionType provides a mock function with given fields: ctx, userID, transactionType, limit, descending, eventBefore, eventAfter
func (_m *Fetcher) FetchTransactionsByUserIDAndTransactionType(ctx context.Context, userID string, transactionType string, limit int64, descending bool, eventBefore *time.Time, eventAfter *time.Time) ([]*dynamo.Transaction, error) {
	ret := _m.Called(ctx, userID, transactionType, limit, descending, eventBefore, eventAfter)

	var r0 []*dynamo.Transaction
	if rf, ok := ret.Get(0).(func(context.Context, string, string, int64, bool, *time.Time, *time.Time) []*dynamo.Transaction); ok {
		r0 = rf(ctx, userID, transactionType, limit, descending, eventBefore, eventAfter)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]*dynamo.Transaction)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string, string, int64, bool, *time.Time, *time.Time) error); ok {
		r1 = rf(ctx, userID, transactionType, limit, descending, eventBefore, eventAfter)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}
