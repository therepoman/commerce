// Code generated by mockery v1.0.0. DO NOT EDIT.

package cheers_mock

import context "context"
import mock "github.com/stretchr/testify/mock"
import models "code.justin.tv/commerce/payday/models"

// SponsoredCheermoteGetter is an autogenerated mock type for the SponsoredCheermoteGetter type
type SponsoredCheermoteGetter struct {
	mock.Mock
}

// GetSponsoredCheers provides a mock function with given fields: ctx, channelId, userId
func (_m *SponsoredCheermoteGetter) GetSponsoredCheers(ctx context.Context, channelId string, userId string) (*models.CheerGroup, error) {
	ret := _m.Called(ctx, channelId, userId)

	var r0 *models.CheerGroup
	if rf, ok := ret.Get(0).(func(context.Context, string, string) *models.CheerGroup); ok {
		r0 = rf(ctx, channelId, userId)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*models.CheerGroup)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string, string) error); ok {
		r1 = rf(ctx, channelId, userId)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}
