// Code generated by mockery v1.0.0. DO NOT EDIT.

package balance_mock

import context "context"
import mock "github.com/stretchr/testify/mock"

// Getter is an autogenerated mock type for the Getter type
type Getter struct {
	mock.Mock
}

// GetBalance provides a mock function with given fields: ctx, userID, skipCache
func (_m *Getter) GetBalance(ctx context.Context, userID string, skipCache bool) (int32, error) {
	ret := _m.Called(ctx, userID, skipCache)

	var r0 int32
	if rf, ok := ret.Get(0).(func(context.Context, string, bool) int32); ok {
		r0 = rf(ctx, userID, skipCache)
	} else {
		r0 = ret.Get(0).(int32)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string, bool) error); ok {
		r1 = rf(ctx, userID, skipCache)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}
