// Code generated by mockery v1.0.0. DO NOT EDIT.

package message_mock

import context "context"

import mock "github.com/stretchr/testify/mock"
import tokenizer "code.justin.tv/commerce/payday/message/tokenizer"

// ITokenizerFactory is an autogenerated mock type for the ITokenizerFactory type
type ITokenizerFactory struct {
	mock.Mock
}

// NewTokenizer provides a mock function with given fields: ctx, channelId, userId
func (_m *ITokenizerFactory) NewTokenizer(ctx context.Context, channelId string, userId string) (tokenizer.ITokenizer, error) {
	ret := _m.Called(ctx, channelId, userId)

	var r0 tokenizer.ITokenizer
	if rf, ok := ret.Get(0).(func(context.Context, string, string) tokenizer.ITokenizer); ok {
		r0 = rf(ctx, channelId, userId)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(tokenizer.ITokenizer)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string, string) error); ok {
		r1 = rf(ctx, channelId, userId)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}
