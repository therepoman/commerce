// Code generated by mockery v1.0.0. DO NOT EDIT.

package bulk_grant_mock

import bulk_grant "code.justin.tv/commerce/payday/bulk_grant"
import mock "github.com/stretchr/testify/mock"

// CampaignsGetter is an autogenerated mock type for the CampaignsGetter type
type CampaignsGetter struct {
	mock.Mock
}

// GetCampaign provides a mock function with given fields: campaignID
func (_m *CampaignsGetter) GetCampaign(campaignID string) *bulk_grant.Campaign {
	ret := _m.Called(campaignID)

	var r0 *bulk_grant.Campaign
	if rf, ok := ret.Get(0).(func(string) *bulk_grant.Campaign); ok {
		r0 = rf(campaignID)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*bulk_grant.Campaign)
		}
	}

	return r0
}

// GetList provides a mock function with given fields:
func (_m *CampaignsGetter) GetList() []bulk_grant.Campaign {
	ret := _m.Called()

	var r0 []bulk_grant.Campaign
	if rf, ok := ret.Get(0).(func() []bulk_grant.Campaign); ok {
		r0 = rf()
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]bulk_grant.Campaign)
		}
	}

	return r0
}
