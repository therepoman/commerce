// Code generated by mockery v1.0.0. DO NOT EDIT.

package badgetiers_fetcher_mock

import context "context"

import mock "github.com/stretchr/testify/mock"
import models "code.justin.tv/commerce/payday/badgetiers/models"

// Fetcher is an autogenerated mock type for the Fetcher type
type Fetcher struct {
	mock.Mock
}

// Fetch provides a mock function with given fields: ctx, channelID
func (_m *Fetcher) Fetch(ctx context.Context, channelID string) (models.BitsChatBadgeTiers, error) {
	ret := _m.Called(ctx, channelID)

	var r0 models.BitsChatBadgeTiers
	if rf, ok := ret.Get(0).(func(context.Context, string) models.BitsChatBadgeTiers); ok {
		r0 = rf(ctx, channelID)
	} else {
		r0 = ret.Get(0).(models.BitsChatBadgeTiers)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string) error); ok {
		r1 = rf(ctx, channelID)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}
