// Code generated by mockery v1.0.0. DO NOT EDIT.

package badgetieremotescache_mock

import api "code.justin.tv/commerce/payday/models/api"

import context "context"
import mock "github.com/stretchr/testify/mock"
import redis "github.com/go-redis/redis/v7"

// UploadConfigCache is an autogenerated mock type for the UploadConfigCache type
type UploadConfigCache struct {
	mock.Mock
}

// Del provides a mock function with given fields: ctx, channelID, code
func (_m *UploadConfigCache) Del(ctx context.Context, channelID string, code string) {
	_m.Called(ctx, channelID, code)
}

// DelCmd provides a mock function with given fields: pipeline, channelID, code
func (_m *UploadConfigCache) DelCmd(pipeline redis.Pipeliner, channelID string, code string) {
	_m.Called(pipeline, channelID, code)
}

// Get provides a mock function with given fields: ctx, channelID, code
func (_m *UploadConfigCache) Get(ctx context.Context, channelID string, code string) *api.EmoticonUploadConfiguration {
	ret := _m.Called(ctx, channelID, code)

	var r0 *api.EmoticonUploadConfiguration
	if rf, ok := ret.Get(0).(func(context.Context, string, string) *api.EmoticonUploadConfiguration); ok {
		r0 = rf(ctx, channelID, code)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*api.EmoticonUploadConfiguration)
		}
	}

	return r0
}

// Set provides a mock function with given fields: ctx, emoticonUploadConfig
func (_m *UploadConfigCache) Set(ctx context.Context, emoticonUploadConfig *api.EmoticonUploadConfiguration) {
	_m.Called(ctx, emoticonUploadConfig)
}
