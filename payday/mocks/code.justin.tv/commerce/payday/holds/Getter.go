// Code generated by mockery v1.0.0. DO NOT EDIT.

package holds_mock

import context "context"

import mock "github.com/stretchr/testify/mock"
import paydayrpc "code.justin.tv/commerce/payday/rpc/payday"

// Getter is an autogenerated mock type for the Getter type
type Getter struct {
	mock.Mock
}

// Get provides a mock function with given fields: ctx, userID, holdID
func (_m *Getter) Get(ctx context.Context, userID string, holdID string) (*paydayrpc.Hold, error) {
	ret := _m.Called(ctx, userID, holdID)

	var r0 *paydayrpc.Hold
	if rf, ok := ret.Get(0).(func(context.Context, string, string) *paydayrpc.Hold); ok {
		r0 = rf(ctx, userID, holdID)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*paydayrpc.Hold)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string, string) error); ok {
		r1 = rf(ctx, userID, holdID)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}
