// Code generated by mockery v1.0.0. DO NOT EDIT.

package crypto_mock

import mock "github.com/stretchr/testify/mock"
import models "code.justin.tv/commerce/payday/models"

// Encrypter is an autogenerated mock type for the Encrypter type
type Encrypter struct {
	mock.Mock
}

// EncryptMessage provides a mock function with given fields: input
func (_m *Encrypter) EncryptMessage(input []byte) (*models.EncryptedNotification, error) {
	ret := _m.Called(input)

	var r0 *models.EncryptedNotification
	if rf, ok := ret.Get(0).(func([]byte) *models.EncryptedNotification); ok {
		r0 = rf(input)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*models.EncryptedNotification)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func([]byte) error); ok {
		r1 = rf(input)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}
