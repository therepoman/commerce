// Code generated by mockery v1.0.0. DO NOT EDIT.

package userservice_mock

import mock "github.com/stretchr/testify/mock"
import models "code.justin.tv/web/users-service/models"

// Retriever is an autogenerated mock type for the Retriever type
type Retriever struct {
	mock.Mock
}

// Retrieve provides a mock function with given fields: userID
func (_m *Retriever) Retrieve(userID string) (*models.Properties, error) {
	ret := _m.Called(userID)

	var r0 *models.Properties
	if rf, ok := ret.Get(0).(func(string) *models.Properties); ok {
		r0 = rf(userID)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*models.Properties)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(string) error); ok {
		r1 = rf(userID)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}
