// Code generated by mockery v1.0.0. DO NOT EDIT.

package transformations_mock

import context "context"
import mock "github.com/stretchr/testify/mock"
import models "code.justin.tv/commerce/payday/models"
import prism "code.justin.tv/commerce/prism/rpc"
import prism_tenant "code.justin.tv/commerce/prism/rpc/tenant"
import transformations "code.justin.tv/commerce/payday/prism_tenant/process_message/bits/transformations"

// MessageTransformer is an autogenerated mock type for the MessageTransformer type
type MessageTransformer struct {
	mock.Mock
}

// GetTransformations provides a mock function with given fields: ctx, req
func (_m *MessageTransformer) GetTransformations(ctx context.Context, req *prism_tenant.ProcessMessageReq) ([]*prism.Transformation, map[string]models.BitEmoteTotal, *transformations.TransformerError) {
	ret := _m.Called(ctx, req)

	var r0 []*prism.Transformation
	if rf, ok := ret.Get(0).(func(context.Context, *prism_tenant.ProcessMessageReq) []*prism.Transformation); ok {
		r0 = rf(ctx, req)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]*prism.Transformation)
		}
	}

	var r1 map[string]models.BitEmoteTotal
	if rf, ok := ret.Get(1).(func(context.Context, *prism_tenant.ProcessMessageReq) map[string]models.BitEmoteTotal); ok {
		r1 = rf(ctx, req)
	} else {
		if ret.Get(1) != nil {
			r1 = ret.Get(1).(map[string]models.BitEmoteTotal)
		}
	}

	var r2 *transformations.TransformerError
	if rf, ok := ret.Get(2).(func(context.Context, *prism_tenant.ProcessMessageReq) *transformations.TransformerError); ok {
		r2 = rf(ctx, req)
	} else {
		if ret.Get(2) != nil {
			r2 = ret.Get(2).(*transformations.TransformerError)
		}
	}

	return r0, r1, r2
}
