// Code generated by mockery v1.0.0. DO NOT EDIT.

package sns_mock

import mock "github.com/stretchr/testify/mock"
import models "code.justin.tv/commerce/payday/models"
import prism_tenant "code.justin.tv/commerce/prism/rpc/tenant"

// SNSPublisher is an autogenerated mock type for the SNSPublisher type
type SNSPublisher struct {
	mock.Mock
}

// PublishToBitsUsageSNSTopic provides a mock function with given fields: req, emoteTotals
func (_m *SNSPublisher) PublishToBitsUsageSNSTopic(req *prism_tenant.PostProcessMessageReq, emoteTotals map[string]models.BitEmoteTotal) {
	_m.Called(req, emoteTotals)
}
