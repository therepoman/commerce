// Code generated by mockery v1.0.0. DO NOT EDIT.

package entitle_bits_badge_mock

import context "context"

import mock "github.com/stretchr/testify/mock"
import models "code.justin.tv/commerce/payday/models"
import prism_tenant "code.justin.tv/commerce/prism/rpc/tenant"

// Entitler is an autogenerated mock type for the Entitler type
type Entitler struct {
	mock.Mock
}

// EntitleBadgeTier provides a mock function with given fields: ctx, req, emoteTotals, totalBitsToBroadcasterBefore
func (_m *Entitler) EntitleBadgeTier(ctx context.Context, req *prism_tenant.ProcessMessageReq, emoteTotals map[string]models.BitEmoteTotal, totalBitsToBroadcasterBefore int64) {
	_m.Called(ctx, req, emoteTotals, totalBitsToBroadcasterBefore)
}
