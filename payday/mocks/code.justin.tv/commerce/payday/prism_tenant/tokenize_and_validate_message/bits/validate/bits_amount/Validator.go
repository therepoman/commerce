// Code generated by mockery v1.0.0. DO NOT EDIT.

package bits_amount_mock

import mock "github.com/stretchr/testify/mock"

// Validator is an autogenerated mock type for the Validator type
type Validator struct {
	mock.Mock
}

// Validate provides a mock function with given fields: allTokenValues, actualBitsAmount, requestBitsAmount
func (_m *Validator) Validate(allTokenValues []int64, actualBitsAmount int64, requestBitsAmount int64) error {
	ret := _m.Called(allTokenValues, actualBitsAmount, requestBitsAmount)

	var r0 error
	if rf, ok := ret.Get(0).(func([]int64, int64, int64) error); ok {
		r0 = rf(allTokenValues, actualBitsAmount, requestBitsAmount)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}
