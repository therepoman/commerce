// Code generated by mockery v1.0.0. DO NOT EDIT.

package token_mock

import context "context"
import mock "github.com/stretchr/testify/mock"
import prism_tenant "code.justin.tv/commerce/prism/rpc/tenant"

import validate "code.justin.tv/commerce/payday/prism_tenant/tokenize_and_validate_message/bits/validate"

// Validator is an autogenerated mock type for the Validator type
type Validator struct {
	mock.Mock
}

// Validate provides a mock function with given fields: ctx, req, tokenizedChan
func (_m *Validator) Validate(ctx context.Context, req *prism_tenant.TokenizeAndValidateMessageReq, tokenizedChan chan validate.TokenResponse) {
	_m.Called(ctx, req, tokenizedChan)
}
