// Code generated by mockery v1.0.0. DO NOT EDIT.

package cartman_mock

import goauthorization "code.justin.tv/common/goauthorization"
import http "net/http"
import mock "github.com/stretchr/testify/mock"

// IValidator is an autogenerated mock type for the IValidator type
type IValidator struct {
	mock.Mock
}

// Validate provides a mock function with given fields: r, capabilities
func (_m *IValidator) Validate(r *http.Request, capabilities *goauthorization.CapabilityClaims) error {
	ret := _m.Called(r, capabilities)

	var r0 error
	if rf, ok := ret.Get(0).(func(*http.Request, *goauthorization.CapabilityClaims) error); ok {
		r0 = rf(r, capabilities)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// ValidateAuthenticatedUserID provides a mock function with given fields: r, userID, requireFirstParty, allowAdminOverride
func (_m *IValidator) ValidateAuthenticatedUserID(r *http.Request, userID string, requireFirstParty bool, allowAdminOverride bool) error {
	ret := _m.Called(r, userID, requireFirstParty, allowAdminOverride)

	var r0 error
	if rf, ok := ret.Get(0).(func(*http.Request, string, bool, bool) error); ok {
		r0 = rf(r, userID, requireFirstParty, allowAdminOverride)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}
