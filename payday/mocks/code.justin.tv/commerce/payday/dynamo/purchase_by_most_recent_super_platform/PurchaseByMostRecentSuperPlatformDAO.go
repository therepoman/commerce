// Code generated by mockery v1.0.0. DO NOT EDIT.

package purchase_by_most_recent_super_platform_dao_mock

import context "context"
import mock "github.com/stretchr/testify/mock"
import purchase_by_most_recent_super_platform "code.justin.tv/commerce/payday/dynamo/purchase_by_most_recent_super_platform"

// PurchaseByMostRecentSuperPlatformDAO is an autogenerated mock type for the PurchaseByMostRecentSuperPlatformDAO type
type PurchaseByMostRecentSuperPlatformDAO struct {
	mock.Mock
}

// BatchDelete provides a mock function with given fields: ctx, records
func (_m *PurchaseByMostRecentSuperPlatformDAO) BatchDelete(ctx context.Context, records []purchase_by_most_recent_super_platform.PurchaseByMostRecentSuperPlatform) error {
	ret := _m.Called(ctx, records)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, []purchase_by_most_recent_super_platform.PurchaseByMostRecentSuperPlatform) error); ok {
		r0 = rf(ctx, records)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// Create provides a mock function with given fields: ctx, userID, superPlatform, platform, productID
func (_m *PurchaseByMostRecentSuperPlatformDAO) Create(ctx context.Context, userID string, superPlatform string, platform string, productID string) (*purchase_by_most_recent_super_platform.PurchaseByMostRecentSuperPlatform, error) {
	ret := _m.Called(ctx, userID, superPlatform, platform, productID)

	var r0 *purchase_by_most_recent_super_platform.PurchaseByMostRecentSuperPlatform
	if rf, ok := ret.Get(0).(func(context.Context, string, string, string, string) *purchase_by_most_recent_super_platform.PurchaseByMostRecentSuperPlatform); ok {
		r0 = rf(ctx, userID, superPlatform, platform, productID)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*purchase_by_most_recent_super_platform.PurchaseByMostRecentSuperPlatform)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string, string, string, string) error); ok {
		r1 = rf(ctx, userID, superPlatform, platform, productID)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// Get provides a mock function with given fields: ctx, userID, superPlatform
func (_m *PurchaseByMostRecentSuperPlatformDAO) Get(ctx context.Context, userID string, superPlatform string) (*purchase_by_most_recent_super_platform.PurchaseByMostRecentSuperPlatform, error) {
	ret := _m.Called(ctx, userID, superPlatform)

	var r0 *purchase_by_most_recent_super_platform.PurchaseByMostRecentSuperPlatform
	if rf, ok := ret.Get(0).(func(context.Context, string, string) *purchase_by_most_recent_super_platform.PurchaseByMostRecentSuperPlatform); ok {
		r0 = rf(ctx, userID, superPlatform)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*purchase_by_most_recent_super_platform.PurchaseByMostRecentSuperPlatform)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string, string) error); ok {
		r1 = rf(ctx, userID, superPlatform)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// QueryWithLimit provides a mock function with given fields: ctx, userID, limit, pagingKey
func (_m *PurchaseByMostRecentSuperPlatformDAO) QueryWithLimit(ctx context.Context, userID string, limit int64, pagingKey *string) ([]purchase_by_most_recent_super_platform.PurchaseByMostRecentSuperPlatform, *string, error) {
	ret := _m.Called(ctx, userID, limit, pagingKey)

	var r0 []purchase_by_most_recent_super_platform.PurchaseByMostRecentSuperPlatform
	if rf, ok := ret.Get(0).(func(context.Context, string, int64, *string) []purchase_by_most_recent_super_platform.PurchaseByMostRecentSuperPlatform); ok {
		r0 = rf(ctx, userID, limit, pagingKey)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]purchase_by_most_recent_super_platform.PurchaseByMostRecentSuperPlatform)
		}
	}

	var r1 *string
	if rf, ok := ret.Get(1).(func(context.Context, string, int64, *string) *string); ok {
		r1 = rf(ctx, userID, limit, pagingKey)
	} else {
		if ret.Get(1) != nil {
			r1 = ret.Get(1).(*string)
		}
	}

	var r2 error
	if rf, ok := ret.Get(2).(func(context.Context, string, int64, *string) error); ok {
		r2 = rf(ctx, userID, limit, pagingKey)
	} else {
		r2 = ret.Error(2)
	}

	return r0, r1, r2
}
