// Code generated by mockery v1.0.0. DO NOT EDIT.

package dynamo_mock

import context "context"
import dynamo "code.justin.tv/commerce/payday/dynamo"
import mock "github.com/stretchr/testify/mock"

// ITransactionsDao is an autogenerated mock type for the ITransactionsDao type
type ITransactionsDao struct {
	mock.Mock
}

// BatchGet provides a mock function with given fields: ids
func (_m *ITransactionsDao) BatchGet(ids []string) ([]*dynamo.Transaction, error) {
	ret := _m.Called(ids)

	var r0 []*dynamo.Transaction
	if rf, ok := ret.Get(0).(func([]string) []*dynamo.Transaction); ok {
		r0 = rf(ids)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]*dynamo.Transaction)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func([]string) error); ok {
		r1 = rf(ids)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// CreateNew provides a mock function with given fields: tx
func (_m *ITransactionsDao) CreateNew(tx *dynamo.Transaction) error {
	ret := _m.Called(tx)

	var r0 error
	if rf, ok := ret.Get(0).(func(*dynamo.Transaction) error); ok {
		r0 = rf(tx)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// Delete provides a mock function with given fields: id
func (_m *ITransactionsDao) Delete(id string) error {
	ret := _m.Called(id)

	var r0 error
	if rf, ok := ret.Get(0).(func(string) error); ok {
		r0 = rf(id)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// DeleteTable provides a mock function with given fields:
func (_m *ITransactionsDao) DeleteTable() error {
	ret := _m.Called()

	var r0 error
	if rf, ok := ret.Get(0).(func() error); ok {
		r0 = rf()
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// Get provides a mock function with given fields: id
func (_m *ITransactionsDao) Get(id string) (*dynamo.Transaction, error) {
	ret := _m.Called(id)

	var r0 *dynamo.Transaction
	if rf, ok := ret.Get(0).(func(string) *dynamo.Transaction); ok {
		r0 = rf(id)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*dynamo.Transaction)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(string) error); ok {
		r1 = rf(id)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetByUserIDAndTransactionType provides a mock function with given fields: ctx, args
func (_m *ITransactionsDao) GetByUserIDAndTransactionType(ctx context.Context, args dynamo.GetByUserIDAndTransactionTypeArgs) ([]*dynamo.Transaction, error) {
	ret := _m.Called(ctx, args)

	var r0 []*dynamo.Transaction
	if rf, ok := ret.Get(0).(func(context.Context, dynamo.GetByUserIDAndTransactionTypeArgs) []*dynamo.Transaction); ok {
		r0 = rf(ctx, args)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]*dynamo.Transaction)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, dynamo.GetByUserIDAndTransactionTypeArgs) error); ok {
		r1 = rf(ctx, args)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetClientConfig provides a mock function with given fields:
func (_m *ITransactionsDao) GetClientConfig() *dynamo.DynamoClientConfig {
	ret := _m.Called()

	var r0 *dynamo.DynamoClientConfig
	if rf, ok := ret.Get(0).(func() *dynamo.DynamoClientConfig); ok {
		r0 = rf()
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*dynamo.DynamoClientConfig)
		}
	}

	return r0
}

// GetLastUpdatedTransactionsByChannel provides a mock function with given fields: ctx, channelID, args
func (_m *ITransactionsDao) GetLastUpdatedTransactionsByChannel(ctx context.Context, channelID string, args dynamo.GetLastUpdatedTransactionsArgs) ([]*dynamo.Transaction, string, error) {
	ret := _m.Called(ctx, channelID, args)

	var r0 []*dynamo.Transaction
	if rf, ok := ret.Get(0).(func(context.Context, string, dynamo.GetLastUpdatedTransactionsArgs) []*dynamo.Transaction); ok {
		r0 = rf(ctx, channelID, args)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]*dynamo.Transaction)
		}
	}

	var r1 string
	if rf, ok := ret.Get(1).(func(context.Context, string, dynamo.GetLastUpdatedTransactionsArgs) string); ok {
		r1 = rf(ctx, channelID, args)
	} else {
		r1 = ret.Get(1).(string)
	}

	var r2 error
	if rf, ok := ret.Get(2).(func(context.Context, string, dynamo.GetLastUpdatedTransactionsArgs) error); ok {
		r2 = rf(ctx, channelID, args)
	} else {
		r2 = ret.Error(2)
	}

	return r0, r1, r2
}

// GetLastUpdatedTransactionsByUser provides a mock function with given fields: ctx, userID, args
func (_m *ITransactionsDao) GetLastUpdatedTransactionsByUser(ctx context.Context, userID string, args dynamo.GetLastUpdatedTransactionsArgs) ([]*dynamo.Transaction, string, error) {
	ret := _m.Called(ctx, userID, args)

	var r0 []*dynamo.Transaction
	if rf, ok := ret.Get(0).(func(context.Context, string, dynamo.GetLastUpdatedTransactionsArgs) []*dynamo.Transaction); ok {
		r0 = rf(ctx, userID, args)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]*dynamo.Transaction)
		}
	}

	var r1 string
	if rf, ok := ret.Get(1).(func(context.Context, string, dynamo.GetLastUpdatedTransactionsArgs) string); ok {
		r1 = rf(ctx, userID, args)
	} else {
		r1 = ret.Get(1).(string)
	}

	var r2 error
	if rf, ok := ret.Get(2).(func(context.Context, string, dynamo.GetLastUpdatedTransactionsArgs) error); ok {
		r2 = rf(ctx, userID, args)
	} else {
		r2 = ret.Error(2)
	}

	return r0, r1, r2
}

// GetLatestStreamArn provides a mock function with given fields:
func (_m *ITransactionsDao) GetLatestStreamArn() (string, error) {
	ret := _m.Called()

	var r0 string
	if rf, ok := ret.Get(0).(func() string); ok {
		r0 = rf()
	} else {
		r0 = ret.Get(0).(string)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func() error); ok {
		r1 = rf()
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// SetupTable provides a mock function with given fields:
func (_m *ITransactionsDao) SetupTable() error {
	ret := _m.Called()

	var r0 error
	if rf, ok := ret.Get(0).(func() error); ok {
		r0 = rf()
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// Update provides a mock function with given fields: tx
func (_m *ITransactionsDao) Update(tx *dynamo.Transaction) error {
	ret := _m.Called(tx)

	var r0 error
	if rf, ok := ret.Get(0).(func(*dynamo.Transaction) error); ok {
		r0 = rf(tx)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}
