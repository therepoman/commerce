// Code generated by mockery v1.0.0. DO NOT EDIT.

package eligible_channels_mock

import dynamo "code.justin.tv/commerce/payday/dynamo"
import eligible_channels "code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/eligible_channels"
import mock "github.com/stretchr/testify/mock"

// ISponsoredCheermoteEligibleChannelsDao is an autogenerated mock type for the ISponsoredCheermoteEligibleChannelsDao type
type ISponsoredCheermoteEligibleChannelsDao struct {
	mock.Mock
}

// BatchDelete provides a mock function with given fields: _a0
func (_m *ISponsoredCheermoteEligibleChannelsDao) BatchDelete(_a0 []*eligible_channels.SponsoredCheermoteEligibleChannel) error {
	ret := _m.Called(_a0)

	var r0 error
	if rf, ok := ret.Get(0).(func([]*eligible_channels.SponsoredCheermoteEligibleChannel) error); ok {
		r0 = rf(_a0)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// Delete provides a mock function with given fields: channelID, campaignID
func (_m *ISponsoredCheermoteEligibleChannelsDao) Delete(channelID string, campaignID string) error {
	ret := _m.Called(channelID, campaignID)

	var r0 error
	if rf, ok := ret.Get(0).(func(string, string) error); ok {
		r0 = rf(channelID, campaignID)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// DeleteTable provides a mock function with given fields:
func (_m *ISponsoredCheermoteEligibleChannelsDao) DeleteTable() error {
	ret := _m.Called()

	var r0 error
	if rf, ok := ret.Get(0).(func() error); ok {
		r0 = rf()
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// Get provides a mock function with given fields: channelID, campaignID
func (_m *ISponsoredCheermoteEligibleChannelsDao) Get(channelID string, campaignID string) (*eligible_channels.SponsoredCheermoteEligibleChannel, error) {
	ret := _m.Called(channelID, campaignID)

	var r0 *eligible_channels.SponsoredCheermoteEligibleChannel
	if rf, ok := ret.Get(0).(func(string, string) *eligible_channels.SponsoredCheermoteEligibleChannel); ok {
		r0 = rf(channelID, campaignID)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*eligible_channels.SponsoredCheermoteEligibleChannel)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(string, string) error); ok {
		r1 = rf(channelID, campaignID)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetClientConfig provides a mock function with given fields:
func (_m *ISponsoredCheermoteEligibleChannelsDao) GetClientConfig() *dynamo.DynamoClientConfig {
	ret := _m.Called()

	var r0 *dynamo.DynamoClientConfig
	if rf, ok := ret.Get(0).(func() *dynamo.DynamoClientConfig); ok {
		r0 = rf()
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*dynamo.DynamoClientConfig)
		}
	}

	return r0
}

// GetLatestStreamArn provides a mock function with given fields:
func (_m *ISponsoredCheermoteEligibleChannelsDao) GetLatestStreamArn() (string, error) {
	ret := _m.Called()

	var r0 string
	if rf, ok := ret.Get(0).(func() string); ok {
		r0 = rf()
	} else {
		r0 = ret.Get(0).(string)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func() error); ok {
		r1 = rf()
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// QueryWithLimit provides a mock function with given fields: channelID, limit, pagingKey
func (_m *ISponsoredCheermoteEligibleChannelsDao) QueryWithLimit(channelID string, limit int64, pagingKey *string) ([]*eligible_channels.SponsoredCheermoteEligibleChannel, *string, error) {
	ret := _m.Called(channelID, limit, pagingKey)

	var r0 []*eligible_channels.SponsoredCheermoteEligibleChannel
	if rf, ok := ret.Get(0).(func(string, int64, *string) []*eligible_channels.SponsoredCheermoteEligibleChannel); ok {
		r0 = rf(channelID, limit, pagingKey)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]*eligible_channels.SponsoredCheermoteEligibleChannel)
		}
	}

	var r1 *string
	if rf, ok := ret.Get(1).(func(string, int64, *string) *string); ok {
		r1 = rf(channelID, limit, pagingKey)
	} else {
		if ret.Get(1) != nil {
			r1 = ret.Get(1).(*string)
		}
	}

	var r2 error
	if rf, ok := ret.Get(2).(func(string, int64, *string) error); ok {
		r2 = rf(channelID, limit, pagingKey)
	} else {
		r2 = ret.Error(2)
	}

	return r0, r1, r2
}

// SetupTable provides a mock function with given fields:
func (_m *ISponsoredCheermoteEligibleChannelsDao) SetupTable() error {
	ret := _m.Called()

	var r0 error
	if rf, ok := ret.Get(0).(func() error); ok {
		r0 = rf()
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// Update provides a mock function with given fields: sponsoredCheermoteEligibleChannel
func (_m *ISponsoredCheermoteEligibleChannelsDao) Update(sponsoredCheermoteEligibleChannel *eligible_channels.SponsoredCheermoteEligibleChannel) error {
	ret := _m.Called(sponsoredCheermoteEligibleChannel)

	var r0 error
	if rf, ok := ret.Get(0).(func(*eligible_channels.SponsoredCheermoteEligibleChannel) error); ok {
		r0 = rf(sponsoredCheermoteEligibleChannel)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}
