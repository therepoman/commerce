// Code generated by mockery v1.0.0. DO NOT EDIT.

package campaign_mock

import campaigns "code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/campaigns"
import context "context"
import mock "github.com/stretchr/testify/mock"

// Fetcher is an autogenerated mock type for the Fetcher type
type Fetcher struct {
	mock.Mock
}

// Fetch provides a mock function with given fields: ctx, campaignID
func (_m *Fetcher) Fetch(ctx context.Context, campaignID string) (*campaigns.SponsoredCheermoteCampaign, error) {
	ret := _m.Called(ctx, campaignID)

	var r0 *campaigns.SponsoredCheermoteCampaign
	if rf, ok := ret.Get(0).(func(context.Context, string) *campaigns.SponsoredCheermoteCampaign); ok {
		r0 = rf(ctx, campaignID)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*campaigns.SponsoredCheermoteCampaign)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string) error); ok {
		r1 = rf(ctx, campaignID)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// FetchBatch provides a mock function with given fields: ctx, campaignIDs
func (_m *Fetcher) FetchBatch(ctx context.Context, campaignIDs []string) ([]*campaigns.SponsoredCheermoteCampaign, error) {
	ret := _m.Called(ctx, campaignIDs)

	var r0 []*campaigns.SponsoredCheermoteCampaign
	if rf, ok := ret.Get(0).(func(context.Context, []string) []*campaigns.SponsoredCheermoteCampaign); ok {
		r0 = rf(ctx, campaignIDs)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]*campaigns.SponsoredCheermoteCampaign)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, []string) error); ok {
		r1 = rf(ctx, campaignIDs)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}
