// Code generated by mockery v1.0.0. DO NOT EDIT.

package eligible_channel_mock

import context "context"

import eligible_channels "code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/eligible_channels"
import mock "github.com/stretchr/testify/mock"

// Cache is an autogenerated mock type for the Cache type
type Cache struct {
	mock.Mock
}

// Del provides a mock function with given fields: ctx, channelID, campaignID
func (_m *Cache) Del(ctx context.Context, channelID string, campaignID string) {
	_m.Called(ctx, channelID, campaignID)
}

// Get provides a mock function with given fields: ctx, channelID, campaignID
func (_m *Cache) Get(ctx context.Context, channelID string, campaignID string) (*eligible_channels.SponsoredCheermoteEligibleChannel, bool) {
	ret := _m.Called(ctx, channelID, campaignID)

	var r0 *eligible_channels.SponsoredCheermoteEligibleChannel
	if rf, ok := ret.Get(0).(func(context.Context, string, string) *eligible_channels.SponsoredCheermoteEligibleChannel); ok {
		r0 = rf(ctx, channelID, campaignID)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*eligible_channels.SponsoredCheermoteEligibleChannel)
		}
	}

	var r1 bool
	if rf, ok := ret.Get(1).(func(context.Context, string, string) bool); ok {
		r1 = rf(ctx, channelID, campaignID)
	} else {
		r1 = ret.Get(1).(bool)
	}

	return r0, r1
}

// Set provides a mock function with given fields: ctx, channelID, campaignID, eligibleChannel
func (_m *Cache) Set(ctx context.Context, channelID string, campaignID string, eligibleChannel *eligible_channels.SponsoredCheermoteEligibleChannel) {
	_m.Called(ctx, channelID, campaignID, eligibleChannel)
}
