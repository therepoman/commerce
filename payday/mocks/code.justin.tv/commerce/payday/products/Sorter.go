// Code generated by mockery v1.0.0. DO NOT EDIT.

package products_mock

import api "code.justin.tv/commerce/payday/models/api"
import mock "github.com/stretchr/testify/mock"

// Sorter is an autogenerated mock type for the Sorter type
type Sorter struct {
	mock.Mock
}

// Sort provides a mock function with given fields: _a0
func (_m *Sorter) Sort(_a0 []api.Product) {
	_m.Called(_a0)
}
