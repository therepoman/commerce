// Code generated by mockery v1.0.0. DO NOT EDIT.

package products_mock

import context "context"
import mock "github.com/stretchr/testify/mock"
import models "code.justin.tv/commerce/payday/models"

// Validator is an autogenerated mock type for the Validator type
type Validator struct {
	mock.Mock
}

// Validate provides a mock function with given fields: ctx, tuid, locale, country, platform
func (_m *Validator) Validate(ctx context.Context, tuid string, locale string, country string, platform models.Platform) (error, bool) {
	ret := _m.Called(ctx, tuid, locale, country, platform)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, string, string, string, models.Platform) error); ok {
		r0 = rf(ctx, tuid, locale, country, platform)
	} else {
		r0 = ret.Error(0)
	}

	var r1 bool
	if rf, ok := ret.Get(1).(func(context.Context, string, string, string, models.Platform) bool); ok {
		r1 = rf(ctx, tuid, locale, country, platform)
	} else {
		r1 = ret.Get(1).(bool)
	}

	return r0, r1
}
