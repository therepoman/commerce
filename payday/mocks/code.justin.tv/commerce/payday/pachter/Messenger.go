// Code generated by mockery v1.0.0. DO NOT EDIT.

package pachter_mock

import context "context"
import mock "github.com/stretchr/testify/mock"

import paydayrpc "code.justin.tv/commerce/payday/rpc/payday"

// Messenger is an autogenerated mock type for the Messenger type
type Messenger struct {
	mock.Mock
}

// SendTransactionToPachter provides a mock function with given fields: ctx, tx
func (_m *Messenger) SendTransactionToPachter(ctx context.Context, tx *paydayrpc.Transaction) error {
	ret := _m.Called(ctx, tx)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, *paydayrpc.Transaction) error); ok {
		r0 = rf(ctx, tx)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}
