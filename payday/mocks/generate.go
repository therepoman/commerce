package mocks

// Use command: make generate_mocks
// To regenerate the mocks defined in this file

// Image Manager
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=IImageManager -dir=$GOPATH/src/code.justin.tv/commerce/payday/image -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/image_manager -outpkg=image_manager_mock

// Sandstorm Secret Getter
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=SecretGetter -dir=$GOPATH/src/code.justin.tv/commerce/payday/clients/sandstorm -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/sandstorm -outpkg=sandstorm_mock

// BadgeTierEmotesCache
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=BadgeTierEmotesCache -dir=$GOPATH/src/code.justin.tv/commerce/payday/cache -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/cache -outpkg=cache_mock

// Most Recently Acquired Cache
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=MostRecentlyAcquiredCache -dir=$GOPATH/src/code.justin.tv/commerce/payday/cache -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/cache -outpkg=cache_mock

// Last 24 Hours Acquired Cache
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Last24HoursAcquiredCache -dir=$GOPATH/src/code.justin.tv/commerce/payday/cache -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/cache -outpkg=cache_mock

// Leaderboard Bits To Broadcaster Getter
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=BitsToBroadcasterGetter -dir=$GOPATH/src/code.justin.tv/commerce/payday/leaderboard -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/leaderboard -outpkg=leaderboard_mock

// Perseus Deduplicator
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Deduplicator -dir=$GOPATH/src/code.justin.tv/commerce/payday/sqs/handlers/perseus -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sqs/handlers/perseus -outpkg=perseus_mock

// Cache Purger
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Purger -dir=$GOPATH/src/code.justin.tv/commerce/payday/cache -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/cache -outpkg=cache_mock

// API Balance Getter (the one that handles all the extra crap)
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Getter -dir=$GOPATH/src/code.justin.tv/commerce/payday/api/balance -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/api/balance -outpkg=balance_api_mock

// Auto Refill - mock
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Refill -dir=$GOPATH/src/code.justin.tv/commerce/payday/refill -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/refill -outpkg=refill_mock

// Auto Refill - mockImageUploadCache
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=AutoRefillProfileCache -dir=$GOPATH/src/code.justin.tv/commerce/payday/cache/ -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/cache/ -outpkg=cache_mock

// Chatbadges Logician
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Logician -dir=$GOPATH/src/code.justin.tv/commerce/payday/badgetiers/logician -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/badgetiers/logician -outpkg=badgetiers_logician_mock

// Chatbadges Fetcher
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Fetcher -dir=$GOPATH/src/code.justin.tv/commerce/payday/badgetiers/fetcher -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/badgetiers/fetcher -outpkg=badgetiers_fetcher_mock

// Badge Tier Emotes Validator
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Validator -dir=$GOPATH/src/code.justin.tv/commerce/payday/badgetiers/products/emotes -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/badgetiers/products/emotes -outpkg=badgetieremotes_mock

// Badge Tier Emotes Entitler
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Entitler -dir=$GOPATH/src/code.justin.tv/commerce/payday/badgetiers/products/emotes -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/badgetiers/products/emotes -outpkg=badgetieremotes_mock

// Badge Tier Emotes Saver
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Saver -dir=$GOPATH/src/code.justin.tv/commerce/payday/badgetiers/products/emotes -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/badgetiers/products/emotes -outpkg=badgetieremotes_mock

// Badge Tier Emotes Filler
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Filler -dir=$GOPATH/src/code.justin.tv/commerce/payday/badgetiers/products/emotes -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/badgetiers/products/emotes -outpkg=badgetieremotes_mock

// Badge Tier Emotes Logician
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Logician -dir=$GOPATH/src/code.justin.tv/commerce/payday/badgetiers/products/emotes -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/badgetiers/products/emotes -outpkg=badgetieremotes_mock

// Badge Tier Emotes Assigner
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Assigner -dir=$GOPATH/src/code.justin.tv/commerce/payday/badgetiers/products/emotes -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/badgetiers/products/emotes -outpkg=badgetieremotes_mock

// Badge Users Filler
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Filler -dir=$GOPATH/src/code.justin.tv/commerce/payday/badgetiers/products/users -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/badgetiers/products/users -outpkg=badgeusers_mock

// Bits Badges Saver
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Saver -dir=$GOPATH/src/code.justin.tv/commerce/payday/badgetiers/products/badges -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/badgetiers/products/badges -outpkg=bitsbadges_mock

// Bits Badges Filler
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Filler -dir=$GOPATH/src/code.justin.tv/commerce/payday/badgetiers/products/badges -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/badgetiers/products/badges -outpkg=bitsbadges_mock

// Bits Badges Logician
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Logician -dir=$GOPATH/src/code.justin.tv/commerce/payday/badgetiers/products/badges -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/badgetiers/products/badges -outpkg=bitsbadges_mock

// Bits Badges Admin
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=BadgeTierAdmin -dir=$GOPATH/src/code.justin.tv/commerce/payday/badgetiers/admin -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/badgetiers/admin -outpkg=bitsbadgeadmin_mock

// Chat Badge Equipper
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=ChatBadgeEquipper -dir=$GOPATH/src/code.justin.tv/commerce/payday/badgetiers/products/badges -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/badgetiers/products/badges -outpkg=bitsbadges_mock

// Mako Client
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=MakoClient -dir=$GOPATH/src/code.justin.tv/commerce/payday/clients/mako -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/mako -outpkg=mako_client_mock

// Mako
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Mako -dir=$GOPATH/src/code.justin.tv/commerce/payday/vendor/code.justin.tv/commerce/mako/twirp -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/mako/twirp -outpkg=mako_mock

// Mako Dashboard
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Mako -dir=$GOPATH/src/code.justin.tv/commerce/payday/vendor/code.justin.tv/commerce/mako/twirp/dashboard -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/mako/twirp/dashboard -outpkg=mako_dashboard_mock

// Use Bits on Poll Transactor
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Transactor -dir=$GOPATH/src/code.justin.tv/commerce/payday/rpc/paydayserver/use_bits_on_poll/transaction -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/rpc/paydayserver/use_bits_on_poll/transaction -outpkg=transaction_mock

// Use Bits on Poll Pachinko Transactor
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Transactor -dir=$GOPATH/src/code.justin.tv/commerce/payday/rpc/paydayserver/use_bits_on_poll/transaction/pachinko -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/rpc/paydayserver/use_bits_on_poll/transaction/pachinko -outpkg=pachinko_mock

// Use Bits on Poll Dynamo Transaction Saver
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Saver -dir=$GOPATH/src/code.justin.tv/commerce/payday/rpc/paydayserver/use_bits_on_poll/transaction/dynamo -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/rpc/paydayserver/use_bits_on_poll/transaction/dynamo -outpkg=dynamo_mock

// Transactions DAO
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=ITransactionsDao -dir=$GOPATH/src/code.justin.tv/commerce/payday/dynamo -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo -outpkg=dynamo_mock

// Inflight Transactions DAO
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=InFlightTransactionDao -dir=$GOPATH/src/code.justin.tv/commerce/payday/dynamo -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo -outpkg=dynamo_mock

// Redis Client
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Client -dir=$GOPATH/src/code.justin.tv/commerce/payday/clients/redis -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/redis -outpkg=redis_mock

// Redis Pipeliner
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Pipeliner -dir=$GOPATH/src/code.justin.tv/commerce/payday/vendor/github.com/go-redis/redis/v7 -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/github.com/go-redis/redis -outpkg=redis_mock

// Pachinko Client
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=PachinkoClient -dir=$GOPATH/src/code.justin.tv/commerce/payday/clients/pachinko -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/pachinko -outpkg=pachinko_mock

// Pachinko
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Pachinko -dir=$GOPATH/src/code.justin.tv/commerce/payday/vendor/code.justin.tv/commerce/pachinko/rpc -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/pachinko/rpc -outpkg=pachinko_mock

// Prism Post Process Message
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=PostProcessMessage -dir=$GOPATH/src/code.justin.tv/commerce/payday/prism_tenant/post_process_message -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/prism_tenant/post_process_message -outpkg=post_process_message_mock

// Bits Process Message SNS Publisher
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=SNSPublisher -dir=$GOPATH/src/code.justin.tv/commerce/payday/prism_tenant/process_message/bits/sns -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/prism_tenant/process_message/bits/sns -outpkg=sns_mock

// Data Science
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=DataScience -dir=$GOPATH/src/code.justin.tv/commerce/payday/datascience -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/datascience -outpkg=datascience_mock

// Flo Pricer
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=IFloPricer -dir=$GOPATH/src/code.justin.tv/commerce/payday/prices -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/prices -outpkg=prices_mock

// Bulk Grant Poller
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=CampaignsGetter -dir=$GOPATH/src/code.justin.tv/commerce/payday/bulk_grant -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/bulk_grant -outpkg=bulk_grant_mock

// Flo Client
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Flo -dir=$GOPATH/src/code.justin.tv/commerce/payday/vendor/code.justin.tv/revenue/flo/rpc -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/revenue/flo/rpc -outpkg=flo_mock

// Flo Cache
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=FloPricesCache -dir=$GOPATH/src/code.justin.tv/commerce/payday/cache -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/cache -outpkg=cache_mock

// Extension Transaction Manager
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Manager -dir=$GOPATH/src/code.justin.tv/commerce/payday/extension/transaction -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/extension/transaction -outpkg=transaction_mock

// Extension PubSub Manager
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Manager -dir=$GOPATH/src/code.justin.tv/commerce/payday/extension/pubsub -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/extension/pubsub -outpkg=pubsub_mock

// User Service Client Wrapper
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=IUserServiceClientWrapper -dir=$GOPATH/src/code.justin.tv/commerce/payday/clients/user -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/user -outpkg=user_mock

// Cartman Validator
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=IValidator -dir=$GOPATH/src/code.justin.tv/commerce/payday/cartman -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/cartman -outpkg=cartman_mock

// Extension Transaction DAO
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=IExtensionTransactionsDAO -dir=$GOPATH/src/code.justin.tv/commerce/payday/dynamo/extension_transaction -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo/extension_transaction -outpkg=extension_transaction_mock

// Channel DAO
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=IChannelDao -dir=$GOPATH/src/code.justin.tv/commerce/payday/dynamo -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo -outpkg=dynamo_mock

// Payout Manager
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=IPayoutManager -dir=$GOPATH/src/code.justin.tv/commerce/payday/payout -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/payout -outpkg=payout_mock

// Balance Getter
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Getter -dir=$GOPATH/src/code.justin.tv/commerce/payday/balance -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/balance -outpkg=balance_mock

// Tokenizer
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=ITokenizer -dir=$GOPATH/src/code.justin.tv/commerce/payday/message/tokenizer -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/message/tokenizer -outpkg=tokenizer_mock

// Tokenizer Factory
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=ITokenizerFactory -dir=$GOPATH/src/code.justin.tv/commerce/payday/message -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/message -outpkg=message_mock

// Campaign DAO
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=ISponsoredCheermoteCampaignDao -dir=$GOPATH/src/code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/campaigns -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/campaigns -outpkg=campaigns_mock

// Channel Status DAO
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=ISponsoredCheermoteChannelStatusesDao -dir=$GOPATH/src/code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/channel_statuses -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/channel_statuses -outpkg=channel_statuses_mock

// User Status DAO
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=ISponsoredCheermoteUserStatusesDao -dir=$GOPATH/src/code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/user_campaign_statuses -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/user_campaign_statuses -outpkg=user_campaign_statuses_mock

// Action DAO
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=IActionDao -dir=$GOPATH/src/code.justin.tv/commerce/payday/dynamo/actions -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo/actions -outpkg=actions_mock

// Sponsored Cheermote Actions Getter
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Getter -dir=$GOPATH/src/code.justin.tv/commerce/payday/sponsored_cheermote/actions -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sponsored_cheermote/actions -outpkg=actions_mock

// Sponsored Cheermote Manager
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Manager -dir=$GOPATH/src/code.justin.tv/commerce/payday/sponsored_cheermote/campaign -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sponsored_cheermote/campaign -outpkg=campaign_mock

// Sponsored Cheermote User Cache
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Cache -dir=$GOPATH/src/code.justin.tv/commerce/payday/sponsored_cheermote/user_campaign_status -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sponsored_cheermote/user_campaign_status -outpkg=user_campaign_status_mock

// Sponsored Cheermote User Fetcher
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Fetcher -dir=$GOPATH/src/code.justin.tv/commerce/payday/sponsored_cheermote/user_campaign_status -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sponsored_cheermote/user_campaign_status -outpkg=user_campaign_status_mock

// Sponsored Cheermote User Getter
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Getter -dir=$GOPATH/src/code.justin.tv/commerce/payday/sponsored_cheermote/user_campaign_status -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sponsored_cheermote/user_campaign_status -outpkg=user_campaign_status_mock

// Sponsored Cheermote Cache
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Cache -dir=$GOPATH/src/code.justin.tv/commerce/payday/sponsored_cheermote/campaign -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sponsored_cheermote/campaign -outpkg=campaign_mock

// Sponsored Cheermote Poller
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Poller -dir=$GOPATH/src/code.justin.tv/commerce/payday/sponsored_cheermote/campaign -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sponsored_cheermote/campaign -outpkg=campaign_mock

// Sponsored Cheermote List Getter
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=ListGetter -dir=$GOPATH/src/code.justin.tv/commerce/payday/sponsored_cheermote/campaign -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sponsored_cheermote/campaign -outpkg=campaign_mock

// Sponsored Cheermote Getter
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Getter -dir=$GOPATH/src/code.justin.tv/commerce/payday/sponsored_cheermote/campaign -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sponsored_cheermote/campaign -outpkg=campaign_mock

// Sponsored Cheermote Fetcher
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Fetcher -dir=$GOPATH/src/code.justin.tv/commerce/payday/sponsored_cheermote/campaign -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sponsored_cheermote/campaign -outpkg=campaign_mock

// Sponsored Cheermote Campaign PubSubber
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=PubSubber -dir=$GOPATH/src/code.justin.tv/commerce/payday/sponsored_cheermote/campaign -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sponsored_cheermote/campaign -outpkg=campaign_mock

// Sponsored Cheermote Incrementer
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Incrementor -dir=$GOPATH/src/code.justin.tv/commerce/payday/sponsored_cheermote -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sponsored_cheermote -outpkg=sponsored_cheermote_mock

// Sponsored Cheermote Channel Status Cache
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Cache -dir=$GOPATH/src/code.justin.tv/commerce/payday/sponsored_cheermote/channel_status -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sponsored_cheermote/channel_status -outpkg=channel_status_mock

// Sponsored Cheermote Channel Status Fetcher
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Fetcher -dir=$GOPATH/src/code.justin.tv/commerce/payday/sponsored_cheermote/channel_status -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sponsored_cheermote/channel_status -outpkg=channel_status_mock

// Sponsored Cheermote Channel Status Filterer
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Filterer -dir=$GOPATH/src/code.justin.tv/commerce/payday/sponsored_cheermote/channel_status -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sponsored_cheermote/channel_status -outpkg=channel_status_mock

// Sponsored Cheermote Channel Status Getter
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Getter -dir=$GOPATH/src/code.justin.tv/commerce/payday/sponsored_cheermote/channel_status -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sponsored_cheermote/channel_status -outpkg=channel_status_mock

// Sponsored Cheermote Channel Status Setter
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Setter -dir=$GOPATH/src/code.justin.tv/commerce/payday/sponsored_cheermote/channel_status -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sponsored_cheermote/channel_status -outpkg=channel_status_mock

// Sponsored Cheermote Channel Status PubSubber
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=PubSubber -dir=$GOPATH/src/code.justin.tv/commerce/payday/sponsored_cheermote/channel_status -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sponsored_cheermote/channel_status -outpkg=channel_status_mock

// Sponsored Cheermote Eligible Channel Getter
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Getter -dir=$GOPATH/src/code.justin.tv/commerce/payday/sponsored_cheermote/eligible_channel -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sponsored_cheermote/eligible_channel -outpkg=eligible_channel_mock

// Sponsored Cheermote Eligible Channel Fetcher
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Fetcher -dir=$GOPATH/src/code.justin.tv/commerce/payday/sponsored_cheermote/eligible_channel -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sponsored_cheermote/eligible_channel -outpkg=eligible_channel_mock

// Sponsored Cheermote Eligible Channel Cache
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Cache -dir=$GOPATH/src/code.justin.tv/commerce/payday/sponsored_cheermote/eligible_channel -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sponsored_cheermote/eligible_channel -outpkg=eligible_channel_mock

// User Status DAO
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=ISponsoredCheermoteEligibleChannelsDao -dir=$GOPATH/src/code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/eligible_channels -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/eligible_channels -outpkg=eligible_channels_mock

// Redis Cmdable
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Cmdable -dir=$GOPATH/src/code.justin.tv/commerce/payday/vendor/github.com/go-redis/redis/v7 -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/github.com/go-redis/redis -outpkg=redis_mock

// Action Cacher
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Cache -dir=$GOPATH/src/code.justin.tv/commerce/payday/actions -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/actions -outpkg=actions_mock

// Action Fetcher
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Fetcher -dir=$GOPATH/src/code.justin.tv/commerce/payday/actions -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/actions -outpkg=actions_mock

// Bits Football Event Bouncer
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Bouncer -dir=$GOPATH/src/code.justin.tv/commerce/payday/events/football -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/events/football -outpkg=football_mock

// Onboard Manager
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=IOnboardManager -dir=$GOPATH/src/code.justin.tv/commerce/payday/onboard -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/onboard -outpkg=onboard_mock

// Pantheon Client
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=IPantheonClientWrapper -dir=$GOPATH/src/code.justin.tv/commerce/payday/clients/pantheon -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/pantheon -outpkg=pantheon_mock

// Pantheon
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Pantheon -dir=$GOPATH/src/code.justin.tv/commerce/payday/vendor/code.justin.tv/commerce/pantheon/rpc/pantheonrpc -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/pantheon/rpc/pantheonrpc -outpkg=pantheon_mock

// Petozi Client
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Petozi -dir=$GOPATH/src/code.justin.tv/commerce/payday/vendor/code.justin.tv/commerce/petozi/rpc -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/petozi/rpc -outpkg=petozi_mock

// Nitro RPC Client
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Nitro -dir=$GOPATH/src/code.justin.tv/commerce/payday/vendor/code.justin.tv/samus/nitro/rpc -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/samus/nitro/rpc -outpkg=nitro_mock

// Chat Badge Client
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=IChatBadgesClient -dir=$GOPATH/src/code.justin.tv/commerce/payday/clients/chatbadges -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/chatbadges -outpkg=chatbadges_mock

// Products Eligibility Checker
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Checker -dir=$GOPATH/src/code.justin.tv/commerce/payday/products -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/products -outpkg=products_mock

// Products Fetcher
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Fetcher -dir=$GOPATH/src/code.justin.tv/commerce/payday/products -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/products -outpkg=products_mock

// Products Validator
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Validator -dir=$GOPATH/src/code.justin.tv/commerce/payday/products -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/products -outpkg=products_mock

// Products Parser
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Parser -dir=$GOPATH/src/code.justin.tv/commerce/payday/products -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/products -outpkg=products_mock

// Products Getter
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Getter -dir=$GOPATH/src/code.justin.tv/commerce/payday/products -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/products -outpkg=products_mock

// Products Translator
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Translator -dir=$GOPATH/src/code.justin.tv/commerce/payday/products -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/products -outpkg=products_mock

// Products Sorter
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Sorter -dir=$GOPATH/src/code.justin.tv/commerce/payday/products -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/products -outpkg=products_mock

// Products Logged Out Resolver
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=LoggedOutResolver -dir=$GOPATH/src/code.justin.tv/commerce/payday/products -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/products -outpkg=products_mock

// ProductAvailabilityChecker
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=ProductAvailabilityChecker -dir=$GOPATH/src/code.justin.tv/commerce/payday/products/availability -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/products/availability -outpkg=products_mock

// Products entitlements getter
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Getter -dir=$GOPATH/src/code.justin.tv/commerce/payday/products/entitlements/getter -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/products/entitlements/getter -outpkg=products_entitlements_getter_mock

// Products purchase_by_most_recent_super_platform cache
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Cache -dir=$GOPATH/src/code.justin.tv/commerce/payday/products/purchase_by_most_recent_super_platform/cache -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/products/purchase_by_most_recent_super_platform/cache -outpkg=products_purchase_by_most_recent_super_platform_cache_mock

// Products purchase_by_most_recent_super_platform getter
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Getter -dir=$GOPATH/src/code.justin.tv/commerce/payday/products/purchase_by_most_recent_super_platform/getter -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/products/purchase_by_most_recent_super_platform/getter -outpkg=products_purchase_by_most_recent_super_platform_getter_mock

// Products single_purchase_consumption cache
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Cache -dir=$GOPATH/src/code.justin.tv/commerce/payday/products/single_purchase_consumption/cache -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/products/single_purchase_consumption/cache -outpkg=products_single_purchase_consumption_cache_mock

// Products single_purchase_consumption getter
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Getter -dir=$GOPATH/src/code.justin.tv/commerce/payday/products/single_purchase_consumption/getter -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/products/single_purchase_consumption/getter -outpkg=products_single_purchase_consumption_getter_mock

// TMI Client
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=ITMIClient -dir=$GOPATH/src/code.justin.tv/commerce/payday/clients/tmi -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/tmi -outpkg=tmi_mock

// EMS Client
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=IEMSClient -dir=$GOPATH/src/code.justin.tv/commerce/payday/clients/ems -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/ems -outpkg=ems_mock

// Bits Tokenize And Validate Balance Validator
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Validator -dir=$GOPATH/src/code.justin.tv/commerce/payday/prism_tenant/tokenize_and_validate_message/bits/validate/balance -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/prism_tenant/tokenize_and_validate_message/bits/validate/balance -outpkg=balance_mock

// Bits Tokenize And Validate Token Validator
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Validator -dir=$GOPATH/src/code.justin.tv/commerce/payday/prism_tenant/tokenize_and_validate_message/bits/validate/token -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/prism_tenant/tokenize_and_validate_message/bits/validate/token -outpkg=token_mock

// Bits Tokenize And Validate User Eligibility Validator
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Validator -dir=$GOPATH/src/code.justin.tv/commerce/payday/prism_tenant/tokenize_and_validate_message/bits/validate/user_eligibility -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/prism_tenant/tokenize_and_validate_message/bits/validate/user_eligibility -outpkg=user_eligibility_mock

// Bits Tokenize And Validate Channel Eligibility Validator
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Validator -dir=$GOPATH/src/code.justin.tv/commerce/payday/prism_tenant/tokenize_and_validate_message/bits/validate/channel_eligibility -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/prism_tenant/tokenize_and_validate_message/bits/validate/channel_eligibility -outpkg=channel_eligibility_mock

// Bits Tokenize And Validate Bits Amount Validator
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Validator -dir=$GOPATH/src/code.justin.tv/commerce/payday/prism_tenant/tokenize_and_validate_message/bits/validate/bits_amount -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/prism_tenant/tokenize_and_validate_message/bits/validate/bits_amount -outpkg=bits_amount_mock

// Bits Tokenize And Validate Channel Min Bits Validator
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Validator -dir=$GOPATH/src/code.justin.tv/commerce/payday/prism_tenant/tokenize_and_validate_message/bits/validate/channel_min_bits -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/prism_tenant/tokenize_and_validate_message/bits/validate/channel_min_bits -outpkg=channel_min_bits_mock

// Bits Tokenize And Validate Channel Min Bits Emote Validator
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Validator -dir=$GOPATH/src/code.justin.tv/commerce/payday/prism_tenant/tokenize_and_validate_message/bits/validate/channel_min_bits_emote -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/prism_tenant/tokenize_and_validate_message/bits/validate/channel_min_bits_emote -outpkg=channel_min_bits_emote_mock

// Bits Tokenize And Validate Data Science Tracker
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=DataScienceTracker -dir=$GOPATH/src/code.justin.tv/commerce/payday/prism_tenant/tokenize_and_validate_message/bits/datascience -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/prism_tenant/tokenize_and_validate_message/bits/datascience -outpkg=datascience_mock

// Tokenize And Validate TokenizeAndValidate
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=TokenizeAndValidate -dir=$GOPATH/src/code.justin.tv/commerce/payday/prism_tenant/tokenize_and_validate_message -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/prism_tenant/tokenize_and_validate_message -outpkg=tokenize_and_validate_mock

// Bits Process Message Bits Badge Entitler
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Entitler -dir=$GOPATH/src/code.justin.tv/commerce/payday/prism_tenant/process_message/bits/entitle_bits_badge -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/prism_tenant/process_message/bits/entitle_bits_badge -outpkg=entitle_bits_badge_mock

// Bits Process Message Data Science Tracker
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=DataScienceTracker -dir=$GOPATH/src/code.justin.tv/commerce/payday/prism_tenant/process_message/bits/datascience -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/prism_tenant/process_message/bits/datascience -outpkg=datascience_mock

// Bits Process Message Message Transformer
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=MessageTransformer -dir=$GOPATH/src/code.justin.tv/commerce/payday/prism_tenant/process_message/bits/transformations -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/prism_tenant/process_message/bits/transformations -outpkg=transformations_mock

// Bits Process Message Sponsored Cheermote Validator
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=SponsoredCheermoteValidator -dir=$GOPATH/src/code.justin.tv/commerce/payday/prism_tenant/process_message/bits/sponsored_cheermotes -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/prism_tenant/process_message/bits/sponsored_cheermotes -outpkg=sponsored_cheermotes_mock

// Bits Process Message Sponsored Cheermotes Bonus Message Transformer
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=SponsoredCheermoteBonusMessageTransformer -dir=$GOPATH/src/code.justin.tv/commerce/payday/prism_tenant/process_message/bits/transformations/sponsored_cheermotes_bonus -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/prism_tenant/process_message/bits/transformations/sponsored_cheermotes_bonus -outpkg=sponsored_cheermotes_bonus_mock

// Process Message ProcessMessage
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=ProcessMessage -dir=$GOPATH/src/code.justin.tv/commerce/payday/prism_tenant/process_message -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/prism_tenant/process_message -outpkg=process_message_mock

// User State Setter
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Setter -dir=$GOPATH/src/code.justin.tv/commerce/payday/userstate -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/userstate -outpkg=userstate_mock

// User State Getter
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Getter -dir=$GOPATH/src/code.justin.tv/commerce/payday/userstate -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/userstate -outpkg=userstate_mock

// User State Fetcher
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Fetcher -dir=$GOPATH/src/code.justin.tv/commerce/payday/userstate -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/userstate -outpkg=userstate_mock

// User State Searcher
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Searcher -dir=$GOPATH/src/code.justin.tv/commerce/payday/userstate -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/userstate -outpkg=userstate_mock

// User State Bouncer
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Bouncer -dir=$GOPATH/src/code.justin.tv/commerce/payday/userstate -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/userstate -outpkg=userstate_mock

// User First Purchase PubSubber
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=PubSubber -dir=$GOPATH/src/code.justin.tv/commerce/payday/userstate/firstpurchase -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/firstpurchase -outpkg=firstpurchase_mock

// User State Purchase Bits Searcher
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Searcher -dir=$GOPATH/src/code.justin.tv/commerce/payday/userstate/search/purchasebits -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/userstate/search/purchasebits -outpkg=purchasebits_mock

// User State User Cheered Searcher
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Searcher -dir=$GOPATH/src/code.justin.tv/commerce/payday/userstate/search/usercheered -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/userstate/search/usercheered -outpkg=usercheered_mock

// User State User Notice
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=UserNotice -dir=$GOPATH/src/code.justin.tv/commerce/payday/userstate/firstcheer -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/userstate/firstcheer -outpkg=firstcheerusernotice_mock

// User Settings Fetcher
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Fetcher -dir=$GOPATH/src/code.justin.tv/commerce/payday/userstate/settings -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/userstate/settings -outpkg=settings_mock

// User Settings Getter
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Getter -dir=$GOPATH/src/code.justin.tv/commerce/payday/userstate/settings -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/userstate/settings -outpkg=settings_mock

// User Settings Setter
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Setter -dir=$GOPATH/src/code.justin.tv/commerce/payday/userstate/settings -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/userstate/settings -outpkg=settings_mock

// User Service Cache
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Cache -dir=$GOPATH/src/code.justin.tv/commerce/payday/cache/userservice -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/cache/userservice -outpkg=userservice_mock

// User Service Retriever
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Retriever -dir=$GOPATH/src/code.justin.tv/commerce/payday/userservice -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/userservice -outpkg=userservice_mock

// User Service Fetcher
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Fetcher -dir=$GOPATH/src/code.justin.tv/commerce/payday/userservice -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/userservice -outpkg=userservice_mock

// User Service Checker
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Checker -dir=$GOPATH/src/code.justin.tv/commerce/payday/userservice -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/userservice -outpkg=userservice_mock

// User Cache
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Cache -dir=$GOPATH/src/code.justin.tv/commerce/payday/cache/user -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/cache/user -outpkg=user_mock

// User DAO
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=IUserDao -dir=$GOPATH/src/code.justin.tv/commerce/payday/dynamo -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo -outpkg=dynamo_mock

// User Fetcher
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Fetcher -dir=$GOPATH/src/code.justin.tv/commerce/payday/user -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/user -outpkg=user_mock

// User Setter
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Setter -dir=$GOPATH/src/code.justin.tv/commerce/payday/user -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/user -outpkg=user_mock

// User Checker
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Checker -dir=$GOPATH/src/code.justin.tv/commerce/payday/user -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/user -outpkg=user_mock

// Channel Chat Badges Cache
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Cache -dir=$GOPATH/src/code.justin.tv/commerce/payday/cache/chatbadges -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/cache/chatbadges -outpkg=chatbadges_mock

// Channel Badge Tier Users Fetcher
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Fetcher -dir=$GOPATH/src/code.justin.tv/commerce/payday/badgetiers/products/users -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/badgetiers/products/users -outpkg=badgeusers_mock

// AWS SQSAPI
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=SQSAPI -dir=$GOPATH/src/code.justin.tv/commerce/payday/vendor/github.com/aws/aws-sdk-go/service/sqs/sqsiface -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/github.com/aws/aws-sdk-go/service/sqs/sqsiface -outpkg=sqsiface_mock

// AWS CloudWatch
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=CloudWatchAPI -dir=$GOPATH/src/code.justin.tv/commerce/payday/vendor/github.com/aws/aws-sdk-go/service/cloudwatch/cloudwatchiface -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/github.com/aws/aws-sdk-go/service/cloudwatch/cloudwatchiface -outpkg=cloudwatchiface_mock

// AWS KMS
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=KMSAPI -dir=$GOPATH/src/code.justin.tv/commerce/payday/vendor/github.com/aws/aws-sdk-go/service/kms/kmsiface -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/github.com/aws/aws-sdk-go/service/kms/kmsiface -outpkg=kmsiface_mock

// Experiment Config Fetcher
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=ExperimentConfigFetcher -dir=$GOPATH/src/code.justin.tv/commerce/payday/utils/experiments -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/utils/experiments -outpkg=experiments_mock

// Uploader Service
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Uploader -dir=$GOPATH/src/code.justin.tv/commerce/payday/vendor/code.justin.tv/web/upload-service/rpc/uploader -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/web/upload-service/rpc/uploader -outpkg=uploadservice_mock

// Upload Service Image Uploader
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=IImageUploader -dir=$GOPATH/src/code.justin.tv/commerce/payday/clients/uploadservice -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/uploadservice -outpkg=uploadserviceclient_mock

// Badge Tier Notifications Dao
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=BadgeTierNotificationDAO -dir=$GOPATH/src/code.justin.tv/commerce/payday/dynamo/badge_tier_notification -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo/badge_tier_notification -outpkg=badge_tier_notification_mock

// Badge Tier Notifications Fetcher
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Fetcher -dir=$GOPATH/src/code.justin.tv/commerce/payday/badgetiers/products/notifications -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/badgetiers/products/notifications -outpkg=tiernotifications_mock

// Badge Tier Notifications Cache
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Cache -dir=$GOPATH/src/code.justin.tv/commerce/payday/badgetiers/products/notifications -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/badgetiers/products/notifications -outpkg=tiernotifications_mock

// Badge Tier Notifications Getter
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Getter -dir=$GOPATH/src/code.justin.tv/commerce/payday/badgetiers/products/notifications -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/badgetiers/products/notifications -outpkg=tiernotifications_mock

// Badge Tier Notifications Setter
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Setter -dir=$GOPATH/src/code.justin.tv/commerce/payday/badgetiers/products/notifications -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/badgetiers/products/notifications -outpkg=tiernotifications_mock

// Badge Tier Notifications UserNotice
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=UserNotice -dir=$GOPATH/src/code.justin.tv/commerce/payday/badgetiers/products/notifications -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/badgetiers/products/notifications -outpkg=tiernotifications_mock

// Badge Tier Notifications DataScience
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=DataScience -dir=$GOPATH/src/code.justin.tv/commerce/payday/badgetiers/products/notifications -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/badgetiers/products/notifications -outpkg=tiernotifications_mock

// Badge Tier Notifications MessageValidator
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=MessageValidator -dir=$GOPATH/src/code.justin.tv/commerce/payday/badgetiers/products/notifications -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/badgetiers/products/notifications -outpkg=tiernotifications_mock

// Custom Cheermote Image Slacker
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=ICustomCheermoteImageSlacker -dir=$GOPATH/src/code.justin.tv/commerce/payday/image -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/image -outpkg=image_mock

// Image Dao
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=IImageDao -dir=$GOPATH/src/code.justin.tv/commerce/payday/dynamo -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo -outpkg=dynamo_mock

// Images Cache
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=IImagesCache -dir=$GOPATH/src/code.justin.tv/commerce/payday/cache -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/cache -outpkg=cache_mock

// Tier Notifications PubSubber
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=PubSubber -dir=$GOPATH/src/code.justin.tv/commerce/payday/badgetiers/products/notifications -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/badgetiers/products/notifications -outpkg=tiernotifications_mock

// PubClient
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=PubClient -dir=$GOPATH/src/code.justin.tv/commerce/payday/vendor/code.justin.tv/chat/pubsub-go-pubclient/client -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/chat/pubsub-go-pubclient/client -outpkg=client_mock

// Ripley Client
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Client -dir=$GOPATH/src/code.justin.tv/commerce/payday/backend/clients/ripley -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/backend/clients/ripley -outpkg=ripleyclient_mock

// Statsd Statter
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Statter -dir=$GOPATH/src/code.justin.tv/commerce/payday/vendor/github.com/cactus/go-statsd-client/statsd -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/github.com/cactus/go-statsd-client/statsd -outpkg=statsd_mock

// Statsd Statter
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Statter -dir=$GOPATH/src/code.justin.tv/commerce/payday/metrics -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/metrics -outpkg=metrics_mock

// Leaderboard Badge Reconciler
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=BadgeReconciler -dir=$GOPATH/src/code.justin.tv/commerce/payday/leaderboard -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/leaderboard -outpkg=leaderboard_mock

// Channel Cache Manager
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=ChannelManager -dir=$GOPATH/src/code.justin.tv/commerce/payday/cache/channel -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/cache/channel -outpkg=channel_mock

// Datascience Tracking Helper
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Decoder -dir=$GOPATH/src/code.justin.tv/commerce/payday/vendor/code.justin.tv/common/goauthorization -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/common/goauthorization -outpkg=goauthorization_mock

// Falador Client
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=TwitchExtCatalogService -dir=$GOPATH/src/code.justin.tv/commerce/payday/vendor/code.justin.tv/amzn/TwitchExtCatalogServiceTwirp -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/amzn/TwitchExtCatalogServiceTwirp -outpkg=falador_mock

// HTTP Utils Client
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=HttpUtilClient -dir=$GOPATH/src/code.justin.tv/commerce/payday/httputils -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/httputils -outpkg=httputils_mock

// Admin Add Bits Processor
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Processor -dir=$GOPATH/src/code.justin.tv/commerce/payday/admin/add_bits -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/admin/add_bits -outpkg=add_bits_mock

// Admin Remove Bits Processor
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Processor -dir=$GOPATH/src/code.justin.tv/commerce/payday/admin/remove_bits -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/admin/remove_bits -outpkg=remove_bits_mock

// Automod Cache
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=IAutoModCache -dir=$GOPATH/src/code.justin.tv/commerce/payday/cache -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/cache -outpkg=cache_mock

// Badge Tier DAO
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=IBadgeTierDao -dir=$GOPATH/src/code.justin.tv/commerce/payday/dynamo -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo -outpkg=dynamo_mock

// Balance Cache
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=IBalanceCache -dir=$GOPATH/src/code.justin.tv/commerce/payday/cache -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/cache -outpkg=cache_mock

// Channel DAO
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=IChannelDao -dir=$GOPATH/src/code.justin.tv/commerce/payday/dynamo -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo -outpkg=dynamo_mock

// SQS Decrypter
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=IDecrypter -dir=$GOPATH/src/code.justin.tv/commerce/payday/sqs/crypto -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sqs/crypto -outpkg=crypto_mock

// SQS Encrypter
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Encrypter -dir=$GOPATH/src/code.justin.tv/commerce/payday/sqs/crypto -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sqs/crypto -outpkg=crypto_mock

// Leaderboard Badge Holders DAO
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=ILeaderboardBadgeHoldersDao -dir=$GOPATH/src/code.justin.tv/commerce/payday/dynamo/leaderboard_badge_holders -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo/leaderboard_badge_holders -outpkg=leaderboard_badge_holders_mock

// INotification
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=INotification -dir=$GOPATH/src/code.justin.tv/commerce/payday/notification -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/notification -outpkg=notification_mock

// Partner Manager
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=IPartnerManager -dir=$GOPATH/src/code.justin.tv/commerce/payday/partner -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/partner -outpkg=partner_mock

// Prefix Manager
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=IPrefixManager -dir=$GOPATH/src/code.justin.tv/commerce/payday/emote -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/emote -outpkg=emote_mock

// Revenue DAO
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=IRevenueDao -dir=$GOPATH/src/code.justin.tv/commerce/payday/dynamo -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo -outpkg=dynamo_mock

// S3 Client
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=IS3Client -dir=$GOPATH/src/code.justin.tv/commerce/payday/s3 -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/s3 -outpkg=s3_mock

// SNS Client
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=ISNSClient -dir=$GOPATH/src/code.justin.tv/commerce/payday/sns -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sns -outpkg=sns_mock

// Throttler
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=IThrottler -dir=$GOPATH/src/code.justin.tv/commerce/payday/throttle -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/throttle -outpkg=throttle_mock

// Process Message Transactions Creator
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Creator -dir=$GOPATH/src/code.justin.tv/commerce/payday/prism_tenant/process_message/bits/transaction -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/prism_tenant/process_message/bits/transaction -outpkg=transaction_mock

// Post Process Message Transactions Saver
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Saver -dir=$GOPATH/src/code.justin.tv/commerce/payday/prism_tenant/post_process_message/bits/transaction -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/prism_tenant/post_process_message/bits/transaction -outpkg=transaction_mock

// User Service Client Wrapper
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=IUserServiceClientWrapper -dir=$GOPATH/src/code.justin.tv/commerce/payday/clients/user -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/user -outpkg=user_mock

// Zuma Client Wrapper
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=IZumaClientWrapper -dir=$GOPATH/src/code.justin.tv/commerce/payday/clients/zuma -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/zuma -outpkg=zuma_mock

// Pushy Client Wrapper
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Publisher -dir=$GOPATH/src/code.justin.tv/commerce/payday/vendor/code.justin.tv/chat/pushy/client/events -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/chat/pushy/client/events -outpkg=pushy_mock

// Locking Client
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=LockingClient -dir=$GOPATH/src/code.justin.tv/commerce/payday/lock -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/lock -outpkg=lock_mock

// Lock
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Lock -dir=$GOPATH/src/code.justin.tv/commerce/payday/lock -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/lock -outpkg=lock_mock

// Actions Retriever
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Retriever -dir=$GOPATH/src/code.justin.tv/commerce/payday/actions -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/actions -outpkg=actions_mock

// Pachinko Entitler
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Pachinko -dir=$GOPATH/src/code.justin.tv/commerce/payday/entitle -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/entitle -outpkg=entitle_mock

// Entitle Validator
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Validator -dir=$GOPATH/src/code.justin.tv/commerce/payday/entitle -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/entitle -outpkg=entitle_mock

// Parallel Entitler
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Parallel -dir=$GOPATH/src/code.justin.tv/commerce/payday/entitle -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/entitle -outpkg=entitle_mock

// Pachinko Event Giver
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=PachinkoGiver -dir=$GOPATH/src/code.justin.tv/commerce/payday/event -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/event -outpkg=event_mock

// BitsEntitlementDAO DAO
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=BitsEntitlementDAO -dir=$GOPATH/src/code.justin.tv/commerce/payday/dynamo/bits_entitlement -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo/bits_entitlement -outpkg=bits_entitlement_dao_mock

// PurchaseByMostRecentSuperPlatformDAO DAO
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=PurchaseByMostRecentSuperPlatformDAO -dir=$GOPATH/src/code.justin.tv/commerce/payday/dynamo/purchase_by_most_recent_super_platform -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo/purchase_by_most_recent_super_platform -outpkg=purchase_by_most_recent_super_platform_dao_mock

// SinglePurchaseConsumptionDAO DAO
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=SinglePurchaseConsumptionDAO -dir=$GOPATH/src/code.justin.tv/commerce/payday/dynamo/single_purchase_consumption -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo/single_purchase_consumption -outpkg=single_purchase_consumption_dao_mock

// Transactions Fetcher
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Fetcher -dir=$GOPATH/src/code.justin.tv/commerce/payday/transactions -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/transactions -outpkg=transactions_mock

// Transactions Getter
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Getter -dir=$GOPATH/src/code.justin.tv/commerce/payday/transactions -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/transactions -outpkg=transactions_mock

// Holds DAO
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=DAO -dir=$GOPATH/src/code.justin.tv/commerce/payday/dynamo/holds -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo/holds -outpkg=holds_dao_mock

// Holds Cache
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Cache -dir=$GOPATH/src/code.justin.tv/commerce/payday/holds -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/holds -outpkg=holds_mock

// Holds Fetcher
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Fetcher -dir=$GOPATH/src/code.justin.tv/commerce/payday/holds -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/holds -outpkg=holds_mock

// Holds Getter
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Getter -dir=$GOPATH/src/code.justin.tv/commerce/payday/holds -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/holds -outpkg=holds_mock

// Holds creator
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Creator -dir=$GOPATH/src/code.justin.tv/commerce/payday/holds -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/holds -outpkg=holds_mock

// Holds Releaser
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Releaser -dir=$GOPATH/src/code.justin.tv/commerce/payday/holds -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/holds -outpkg=holds_mock

// ReleaseœHold API Validator
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Validator -dir=$GOPATH/src/code.justin.tv/commerce/payday/rpc/paydayserver/release_hold -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/rpc/paydayserver/release_hold -outpkg=release_hold_mock

// Holds Finalizer
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Finalizer -dir=$GOPATH/src/code.justin.tv/commerce/payday/holds -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/holds -outpkg=holds_mock

// Finalize Hold API Validator
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Validator -dir=$GOPATH/src/code.justin.tv/commerce/payday/rpc/paydayserver/finalize_hold -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/rpc/paydayserver/finalize_hold -outpkg=finalize_hold_mock

// Nitro Client
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=NitroClient -dir=$GOPATH/src/code.justin.tv/commerce/payday/clients/nitro -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/nitro -outpkg=nitro_mock

// Ripley Cache
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Cache -dir=$GOPATH/src/code.justin.tv/commerce/payday/cache/ripley -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/cache/ripley -outpkg=ripley_mock

// Perseus Message Formatter
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=MessageFormatter -dir=$GOPATH/src/code.justin.tv/commerce/payday/sqs/handlers/perseus/format -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sqs/handlers/perseus/format -outpkg=format_mock

// Perseus Formatter
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Formatter -dir=$GOPATH/src/code.justin.tv/commerce/payday/sqs/handlers/perseus -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sqs/handlers/perseus -outpkg=perseus_mock

// Perseus SNS Publisher
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=SNS -dir=$GOPATH/src/code.justin.tv/commerce/payday/sqs/handlers/perseus -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sqs/handlers/perseus -outpkg=perseus_mock

// Dart Receiver Fulton Client
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Receiver -dir=$GOPATH/src/code.justin.tv/commerce/payday/vendor/code.justin.tv/amzn/TwitchDartReceiverTwirp -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/amzn/TwitchDartReceiverTwirp -outpkg=receiver_mock

// Badge Tier GroupID DAO
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=BadgeTierEmoteGroupIDDAO -dir=$GOPATH/src/code.justin.tv/commerce/payday/dynamo/badge_tier_emote_groupids -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo/badge_tier_emote_groupids -outpkg=badge_tier_emote_groupids_mock

// Cloudfront Client
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=ICloudFrontClient -dir=$GOPATH/src/code.justin.tv/commerce/payday/cloudfront -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/cloudfront -outpkg=cloudfront_mock

// Global Cheers Poller
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=GlobalCheersPoller -dir=$GOPATH/src/code.justin.tv/commerce/payday/cheers -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/cheers -outpkg=cheers_mock

// Sponsored Cheermote Getter
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=SponsoredCheermoteGetter -dir=$GOPATH/src/code.justin.tv/commerce/payday/cheers -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/cheers -outpkg=cheers_mock

// Channel Custom Cheermote Getter
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=ChannelCustomCheermoteGetter -dir=$GOPATH/src/code.justin.tv/commerce/payday/cheers -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/cheers -outpkg=cheers_mock

// API Client Tracking
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Tracker -dir=$GOPATH/src/code.justin.tv/commerce/payday/api/clienttracking -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/api/clienttracking -outpkg=clienttracking_mock

// Pagle Client
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Pagle -dir=$GOPATH/src/code.justin.tv/commerce/payday/vendor/code.justin.tv/commerce/pagle/rpc -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/pagle/rpc -outpkg=pagle_mock

// PDMS Client
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=IPDMSClientWrapper -dir=$GOPATH/src/code.justin.tv/commerce/payday/clients/pdms -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/pdms -outpkg=pdms_mock

// ThresholdBreachProcessor
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=ThresholdBreachProcessor -dir=$GOPATH/src/code.justin.tv/commerce/payday/admin/add_bits/ -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/admin/add_bits/ -outpkg=add_bits_mock

// Pachter Messenger
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=Messenger -dir=$GOPATH/src/code.justin.tv/commerce/payday/pachter/ -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/pachter/ -outpkg=pachter_mock

// Miniexperiment Client
//go:generate $GOPATH/src/code.justin.tv/commerce/payday/_tools/bin/retool do mockery -name=MiniExperimentClient -dir=$GOPATH/src/code.justin.tv/commerce/payday/clients/miniexperiments/ -output=$GOPATH/src/code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/miniexperiments/ -outpkg=clients_mock
