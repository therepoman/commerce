package pubsub

import (
	"context"
	"encoding/json"
	"fmt"

	pubclient "code.justin.tv/chat/pubsub-go-pubclient/client"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/extension/transaction"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/userservice"
	"code.justin.tv/gds/gds/extensions/validator/client/s2s"
	validator_model "code.justin.tv/gds/gds/extensions/validator/model"
)

const (
	// "bits-ext-v1-transaction.${channelId}-${clientId}"
	useBitsOnExtensionsBroadcastTopic = "bits-ext-v1-transaction.%s-%s"

	// "bits-ext-v1-user-transaction.${userId}.${channelId}-${clientId}"
	useBitsOnExtensionsSingleUserTopic = "bits-ext-v1-user-transaction.%s.%s-%s"
)

type ManagerImpl struct {
	UserServiceFetcher          userservice.Fetcher `inject:""`
	PubClient                   pubclient.PubClient `inject:""`
	ExtensionTransactionManager transaction.Manager `inject:""`
	ValidatorClient             s2s.Client          `inject:""`
}

type Manager interface {
	ProcessUseBitsOnExtensionMsg(ctx context.Context, msg *models.UseBitsOnExtensionMessage) error
}

func (m *ManagerImpl) ProcessUseBitsOnExtensionMsg(ctx context.Context, msg *models.UseBitsOnExtensionMessage) error {
	if msg == nil {
		return errors.New("retrieved nil use bits on extensions msg")
	}

	tx, err := m.ExtensionTransactionManager.GetTransaction(msg.RequestingTwitchUserId, msg.TransactionId)
	if err != nil {
		log.WithError(err).Error("could not get extension transaction from transaction manager")
		return errors.Notef(err, "could not get extension transaction from transaction manager")
	}

	if tx == nil {
		return errors.New("no extension transaction found in transaction manager")
	}

	sender, err := m.resolveUserDisplayName(ctx, msg.RequestingTwitchUserId)
	if err != nil {
		log.WithError(err).Error("could not retrieve username")
		return errors.Note(err, "could not retrieve username")
	}

	validatorProduct := validator_model.Product{
		DomainId:    tx.Product.Domain,
		SKU:         tx.Product.Sku,
		DisplayName: tx.Product.DisplayName,
		Cost: validator_model.Cost{
			Amount: tx.Product.Cost.Amount,
			Type:   tx.Product.Cost.Type,
		},
	}

	transactionJWT, err := m.ValidatorClient.CreateBitsTransactionToken(ctx, msg.ExtensionID, msg.RequestingTwitchUserId, msg.TransactionId, msg.TimeOfEvent.String(), validatorProduct, nil)
	if err != nil {
		log.WithError(err).Error("Unable to create transaction JWT")
		return errors.Note(err, "Unable to create transaction JWT")
	}

	pubsubMessage := models.PubSubBitsOnExtensionsMessage{
		TransactionId:      msg.TransactionId,
		Time:               msg.TimeOfEvent,
		SKU:                tx.Product.Sku,
		UserId:             msg.RequestingTwitchUserId,
		DisplayName:        sender,
		ChannelId:          msg.TargetTwitchUserId,
		TransactionReceipt: transactionJWT.Token,
	}

	marshalled, err := json.Marshal(pubsubMessage)
	if err != nil {
		log.WithError(err).Error("could not marshal pubsub message")
		return errors.Note(err, "could not marshal pubsub message")
	}

	topics := []string{getPubSubTopic(tx.Product.Broadcast, msg.TargetTwitchUserId, msg.ExtensionID, msg.RequestingTwitchUserId)}
	err = m.PubClient.Publish(ctx, topics, string(marshalled), nil)
	if err != nil {
		log.WithError(err).Error("could not publish message to pubsub")
		return errors.Note(err, "could not publish message to pubsub")
	}

	return nil
}

func (m *ManagerImpl) resolveUserDisplayName(ctx context.Context, userId string) (string, error) {
	user, err := m.UserServiceFetcher.Fetch(ctx, userId)
	if err != nil {
		return "", errors.Notef(err, "failed looking up user via user service")
	}
	if user == nil || user.Displayname == nil {
		return "", errors.New("could not find user in user service lookup for id")
	}

	return *user.Displayname, nil
}

func getPubSubTopic(broadcast bool, channelID string, extensionID string, userID string) string {
	if broadcast {
		return fmt.Sprintf(useBitsOnExtensionsBroadcastTopic, channelID, extensionID)
	}
	return fmt.Sprintf(useBitsOnExtensionsSingleUserTopic, userID, channelID, extensionID)
}
