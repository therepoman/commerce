package pubsub

import (
	"context"
	"encoding/json"
	"fmt"
	"testing"
	"time"

	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/extension/transaction"
	client_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/chat/pubsub-go-pubclient/client"
	transaction_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/extension/transaction"
	userservice_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/userservice"
	gds_mock "code.justin.tv/commerce/payday/mocks/gds"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/gds/gds/extensions/validator/model"
	userservice_models "code.justin.tv/web/users-service/models"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestManagerImpl_ProcessUseBitsOnExtensionMsg(t *testing.T) {
	Convey("Given an extension pubsub manager", t, func() {
		ctx := context.Background()
		pubClient := new(client_mock.PubClient)
		userServiceFetcher := new(userservice_mock.Fetcher)
		extensionTransactionManager := new(transaction_mock.Manager)
		validatorClient := new(gds_mock.S2SClient)
		manager := &ManagerImpl{
			PubClient:                   pubClient,
			ExtensionTransactionManager: extensionTransactionManager,
			ValidatorClient:             validatorClient,
			UserServiceFetcher:          userServiceFetcher,
		}

		Convey("Errors on nil message", func() {
			err := manager.ProcessUseBitsOnExtensionMsg(ctx, nil)
			So(err, ShouldNotBeNil)
		})

		Convey("Given a test use bits on extensions msg", func() {
			testUserID := "testUserID"
			testChannelID := "108707191"
			testTransactionID := "testTransactionID"
			testExtensionID := "testExtensionID"
			msg := &models.UseBitsOnExtensionMessage{
				TransactionId:          testTransactionID,
				RequestingTwitchUserId: testUserID,
				TargetTwitchUserId:     testChannelID,
				ExtensionID:            testExtensionID,
				TimeOfEvent:            time.Now(),
			}

			Convey("Errors when extension transaction manager errors", func() {
				extensionTransactionManager.On("GetTransaction", testUserID, testTransactionID).Return(nil, errors.New("test error"))
				err := manager.ProcessUseBitsOnExtensionMsg(ctx, msg)
				So(err, ShouldNotBeNil)
			})

			Convey("Errors when extension transaction manager returns nil transaction", func() {
				extensionTransactionManager.On("GetTransaction", testUserID, testTransactionID).Return(nil, nil)
				err := manager.ProcessUseBitsOnExtensionMsg(ctx, msg)
				So(err, ShouldNotBeNil)
			})

			Convey("When extension transaction manager returns a transaction", func() {
				testProduct := transaction.FaladorProduct{
					Domain: testExtensionID,
					Sku:    "testSku",
					Cost: &transaction.FaladorCost{
						Amount: 100,
						Type:   "bits",
					},
					DisplayName: "testItem",
				}
				tx := &transaction.Transaction{
					TransactionID: testTransactionID,
					TUID:          testUserID,
					ChannelID:     testChannelID,
					Product:       testProduct,
				}
				extensionTransactionManager.On("GetTransaction", testUserID, testTransactionID).Return(tx, nil)

				Convey("Errors when user service errors", func() {
					userServiceFetcher.On("Fetch", mock.Anything, testUserID).Return(nil, errors.New("test error"))
					err := manager.ProcessUseBitsOnExtensionMsg(ctx, msg)
					So(err, ShouldNotBeNil)
				})

				Convey("Errors when user service cannot find user", func() {
					userServiceFetcher.On("Fetch", mock.Anything, testUserID).Return(nil, nil)
					err := manager.ProcessUseBitsOnExtensionMsg(ctx, msg)
					So(err, ShouldNotBeNil)
				})

				Convey("When user service returns a valid user", func() {
					testDisplayName := "testDisplayName"
					userServiceFetcher.On("Fetch", mock.Anything, testUserID).Return(&userservice_models.Properties{
						ID:          "1",
						Displayname: &testDisplayName,
					}, nil)

					Convey("When the validatorClient returns an error", func() {
						validatorClient.On("CreateBitsTransactionToken", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("Validator error!"))
						err := manager.ProcessUseBitsOnExtensionMsg(ctx, msg)
						So(err, ShouldNotBeNil)
					})

					Convey("When the validatorClient returns a token", func() {
						testPermissionData := &model.PermissionData{
							Token:  "Fake.JWT.Token",
							Grants: map[string]bool{},
						}
						validatorClient.On("CreateBitsTransactionToken", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(testPermissionData, nil)

						Convey("Errors when pubclient errors", func() {
							pubClient.On("Publish", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(errors.New("test error"))
							err := manager.ProcessUseBitsOnExtensionMsg(ctx, msg)
							So(err, ShouldNotBeNil)
						})

						Convey("When pubclient succeeds", func() {
							pubClient.On("Publish", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)

							Convey("Call succeeds and pubsub msg contains the correct values", func() {
								err := manager.ProcessUseBitsOnExtensionMsg(ctx, msg)
								So(err, ShouldBeNil)

								pubJSON := pubClient.Calls[0].Arguments.Get(2).(string)
								var pubSubMsg models.PubSubBitsOnExtensionsMessage
								err = json.Unmarshal([]byte(pubJSON), &pubSubMsg)
								So(err, ShouldBeNil)
								So(pubSubMsg.TransactionId, ShouldEqual, testTransactionID)
								So(pubSubMsg.ChannelId, ShouldEqual, testChannelID)
								So(pubSubMsg.UserId, ShouldEqual, testUserID)
								So(pubSubMsg.DisplayName, ShouldEqual, testDisplayName)
							})

							Convey("Publishes to the single user topic for a non-broadcast transaction", func() {
								tx.Product.Broadcast = false
								err := manager.ProcessUseBitsOnExtensionMsg(ctx, msg)
								So(err, ShouldBeNil)

								topics := pubClient.Calls[0].Arguments.Get(1).([]string)
								topic := topics[0]
								So(topic, ShouldEqual, fmt.Sprintf(useBitsOnExtensionsSingleUserTopic, testUserID, testChannelID, testExtensionID))
							})

							Convey("Publishes to the broadcast topic for a broadcast transaction", func() {
								tx.Product.Broadcast = true
								err := manager.ProcessUseBitsOnExtensionMsg(ctx, msg)
								So(err, ShouldBeNil)

								topics := pubClient.Calls[0].Arguments.Get(1).([]string)
								topic := topics[0]
								So(topic, ShouldEqual, fmt.Sprintf(useBitsOnExtensionsBroadcastTopic, testChannelID, testExtensionID))
							})
						})
					})
				})
			})
		})
	})
}
