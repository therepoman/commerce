package transaction

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/dynamo/extension_transaction"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/utils/strings"
)

type Status string

const (
	StatusPending   = Status("pending")
	StatusProcessed = Status("processed")
)

type FaladorCost struct {
	Amount int64  `json:"amount,omitempty"`
	Type   string `json:"type,omitempty"`
}

type FaladorProduct struct {
	Domain        string       `json:"domain,omitempty"`
	Sku           string       `json:"sku,omitempty"`
	Cost          *FaladorCost `json:"cost,omitempty"`
	InDevelopment bool         `json:"inDevelopment,omitempty"`
	DisplayName   string       `json:"displayName,omitempty"`
	Expiration    string       `json:"expiration,omitempty"`
	Broadcast     bool         `json:"broadcast,omitempty"`
}

type Transaction struct {
	TransactionID             string
	ChannelID                 string
	TUID                      string
	ExtensionClientID         string
	Product                   FaladorProduct
	Status                    Status
	CreateTime                time.Time
	LastUpdated               time.Time
	TotalBitsAfterTransaction int64
	IPAddress                 string
	ZipCode                   string
	CountryCode               string
	City                      string
	TotalBitsToBroadcaster    int64
}

func toDynamoTransaction(tx *Transaction) (*extension_transaction.ExtensionTransaction, error) {
	if tx == nil {
		return nil, errors.New("Can't convert a nil transaction")
	}

	serializedProduct, err := json.Marshal(tx.Product)
	if err != nil {
		return nil, err
	}

	return &extension_transaction.ExtensionTransaction{
		UserID:                     tx.TUID,
		ExtensionIDUserID:          fmt.Sprintf("%s.%s", tx.ExtensionClientID, tx.TUID),
		ChannelIDExtensionID:       fmt.Sprintf("%s.%s", tx.ChannelID, tx.ExtensionClientID),
		ChannelIDExtensionIDUserID: fmt.Sprintf("%s.%s.%s", tx.ChannelID, tx.ExtensionClientID, tx.TUID),
		UserIDStatus:               fmt.Sprintf("%s.%s", tx.TUID, tx.Status),
		ChannelIDStatus:            fmt.Sprintf("%s.%s", tx.ChannelID, tx.Status),
		ChannelID:                  tx.ChannelID,
		TransactionID:              tx.TransactionID,
		ExtensionID:                tx.ExtensionClientID,
		Product:                    string(serializedProduct),
		Status:                     string(tx.Status),
		CreateTime:                 tx.CreateTime,
		TotalBitsAfterTransaction:  tx.TotalBitsAfterTransaction,
		IPAddress:                  tx.IPAddress,
		ZipCode:                    tx.ZipCode,
		City:                       tx.City,
		CountryCode:                tx.CountryCode,
		TotalBitsToBroadcaster:     tx.TotalBitsToBroadcaster,
	}, nil
}

func fromDynamoTransaction(dynamoTx *extension_transaction.ExtensionTransaction) (*Transaction, error) {
	if dynamoTx == nil {
		return nil, errors.New("Can't convert a nil dynamo record")
	}

	var product FaladorProduct
	if dynamoTx.Product != "" {
		err := json.Unmarshal([]byte(dynamoTx.Product), &product)
		if err != nil {
			return nil, err
		}
	}

	return &Transaction{
		TransactionID:             dynamoTx.TransactionID,
		ChannelID:                 dynamoTx.ChannelID,
		TUID:                      dynamoTx.UserID,
		ExtensionClientID:         dynamoTx.ExtensionID,
		Product:                   product,
		Status:                    Status(dynamoTx.Status),
		CreateTime:                dynamoTx.CreateTime,
		LastUpdated:               dynamoTx.LastUpdated,
		TotalBitsToBroadcaster:    dynamoTx.TotalBitsToBroadcaster,
		CountryCode:               dynamoTx.CountryCode,
		City:                      dynamoTx.City,
		ZipCode:                   dynamoTx.ZipCode,
		IPAddress:                 dynamoTx.IPAddress,
		TotalBitsAfterTransaction: dynamoTx.TotalBitsAfterTransaction,
	}, nil
}

func fromDynamoTransactions(dynamoTransactions []*extension_transaction.ExtensionTransaction) []*Transaction {
	transactions := make([]*Transaction, 0, len(dynamoTransactions))
	for _, dynamoTransaction := range dynamoTransactions {
		tx, err := fromDynamoTransaction(dynamoTransaction)
		if err != nil {
			log.WithError(err).Error("Unable to unmarshal specified transaction")
		}
		if tx != nil {
			transactions = append(transactions, tx)
		}
	}
	return transactions
}

type ManagerImpl struct {
	ExtensionTransactionDAO extension_transaction.IExtensionTransactionsDAO `inject:""`
}

type Manager interface {
	StartTransaction(tx *Transaction) error
	FinalizeTransaction(tx *Transaction) error
	DeleteTransaction(userID, txID string) error
	GetTransaction(userID, txID string) (*Transaction, error)
	GetTransactionsByID(txIDs []string) ([]*Transaction, error)
	GetTransactions(userID *string, extensionClientID *string, channelID *string, cursor *string) ([]*Transaction, *string, error)
	GetLastUpdatedTransactionsByUserOrChannel(ctx context.Context, userID, channelID, cursor string, eventBefore, eventAfter *time.Time, status Status) ([]*Transaction, string, error)
}

func (m *ManagerImpl) DeleteTransaction(userID, txID string) error {
	return m.ExtensionTransactionDAO.Delete(userID, txID)
}

func (m *ManagerImpl) StartTransaction(tx *Transaction) error {
	if tx == nil {
		return nil
	}

	tx.CreateTime = time.Now()
	tx.Status = StatusPending
	transaction, err := toDynamoTransaction(tx)
	if err != nil {
		return err
	}

	err = m.ExtensionTransactionDAO.CreateNew(transaction)
	return err
}

func (m *ManagerImpl) FinalizeTransaction(tx *Transaction) error {
	if tx == nil {
		return nil
	}

	tx.Status = StatusProcessed
	transaction, err := toDynamoTransaction(tx)
	if err != nil {
		return err
	}

	err = m.ExtensionTransactionDAO.UpdateWithConditionalStatus(transaction, string(StatusPending))
	return err
}

func (m *ManagerImpl) GetTransaction(userID string, txID string) (*Transaction, error) {
	extensionTransaction, err := m.ExtensionTransactionDAO.Get(userID, txID)
	if err != nil {
		return nil, err
	}
	return fromDynamoTransaction(extensionTransaction)
}

func (m *ManagerImpl) GetTransactionsByID(txIDs []string) ([]*Transaction, error) {
	extensionTransactions, err := m.ExtensionTransactionDAO.GetByTransactionIDs(txIDs)
	if err != nil {
		return nil, err
	}
	return fromDynamoTransactions(extensionTransactions), nil
}

type ExtensionTransactionQueryFunction string

const (
	GetByUser                       = ExtensionTransactionQueryFunction("GetByUser")
	GetByExtensionAndUser           = ExtensionTransactionQueryFunction("GetByExtensionAndUser")
	GetByChannelAndExtensionAndUser = ExtensionTransactionQueryFunction("GetByChannelAndExtensionAndUser")
)

func (m *ManagerImpl) GetTransactions(userID *string, extensionClientID *string, channelID *string, cursor *string) ([]*Transaction, *string, error) {
	hasUserID := strings.NotBlankP(userID)
	hasExtensionClientID := strings.NotBlankP(extensionClientID)
	hasChannelID := strings.NotBlankP(channelID)

	var queryFunction ExtensionTransactionQueryFunction
	if hasUserID && !hasExtensionClientID && !hasChannelID {
		queryFunction = GetByUser
	} else if hasUserID && hasExtensionClientID && !hasChannelID {
		queryFunction = GetByExtensionAndUser
	} else if hasUserID && hasExtensionClientID && hasChannelID {
		queryFunction = GetByChannelAndExtensionAndUser
	}

	var dynamoTransactions []*extension_transaction.ExtensionTransaction
	var newCursor *string
	var err error
	switch queryFunction {
	case GetByUser:
		dynamoTransactions, newCursor, err = m.ExtensionTransactionDAO.GetAllByUser(*userID, cursor)
	case GetByExtensionAndUser:
		dynamoTransactions, newCursor, err = m.ExtensionTransactionDAO.GetAllByExtensionAndUser(*extensionClientID, *userID, cursor)
	case GetByChannelAndExtensionAndUser:
		dynamoTransactions, newCursor, err = m.ExtensionTransactionDAO.GetAllByChannelAndExtensionAndUser(*channelID, *extensionClientID, *userID, cursor)
	default:
		return nil, nil, errors.New("could not find a query function for given parameters")
	}

	if err != nil {
		return nil, nil, err
	}
	return fromDynamoTransactions(dynamoTransactions), newCursor, nil
}

func (m *ManagerImpl) GetLastUpdatedTransactionsByUserOrChannel(ctx context.Context, userID, channelID, cursor string, eventBefore, eventAfter *time.Time, status Status) ([]*Transaction, string, error) {
	var transactions []*extension_transaction.ExtensionTransaction
	newCursor := ""
	var err error

	if userID != "" {
		transactions, newCursor, err = m.ExtensionTransactionDAO.GetLastUpdatedTransactionsByUser(ctx, userID, extension_transaction.GetLastUpdatedTransactionsArgs{
			Cursor:      cursor,
			EventBefore: eventBefore,
			EventAfter:  eventAfter,
			Status:      string(status),
		})
	} else if channelID != "" {
		transactions, newCursor, err = m.ExtensionTransactionDAO.GetLastUpdatedTransactionsByChannel(ctx, channelID, extension_transaction.GetLastUpdatedTransactionsArgs{
			Cursor:      cursor,
			EventBefore: eventBefore,
			EventAfter:  eventAfter,
			Status:      string(status),
		})
	} else {
		return nil, "", errors.New("must provide a user_id or channel_id to when calling ExtensionsManager#GetLastUpdatedTransactionsByUserOrChannel")
	}

	if err != nil {
		return nil, "", err
	}

	return fromDynamoTransactions(transactions), newCursor, nil
}
