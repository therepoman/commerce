package transaction_test

import (
	"testing"

	"code.justin.tv/commerce/payday/dynamo/extension_transaction"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/extension/transaction"
	extension_transaction_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo/extension_transaction"
	"code.justin.tv/commerce/payday/utils/pointers"
	. "github.com/smartystreets/goconvey/convey"
)

func TestManagerImpl_GetTransactions(t *testing.T) {
	Convey("Given an extension transaction manager", t, func() {
		dao := new(extension_transaction_mock.IExtensionTransactionsDAO)
		em := &transaction.ManagerImpl{
			ExtensionTransactionDAO: dao,
		}

		Convey("Errors when userID is nil", func() {
			_, _, err := em.GetTransactions(nil, pointers.StringP("extensionID"), pointers.StringP("channelID"), nil)
			So(err, ShouldNotBeNil)
			So(len(dao.Calls), ShouldEqual, 0)
		})

		Convey("Errors when channelID is present with nil extensionID", func() {
			_, _, err := em.GetTransactions(pointers.StringP("userID"), nil, pointers.StringP("channelID"), nil)
			So(err, ShouldNotBeNil)
			So(len(dao.Calls), ShouldEqual, 0)
		})

		var userID *string
		var extensionClientID *string
		var channelID *string
		var cursor *string
		Convey("When only userID is present", func() {
			userID = pointers.StringP("userID")

			Convey("Errors when dao errors", func() {
				dao.On("GetAllByUser", *userID, cursor).Return(nil, nil, errors.New("test"))
				_, _, err := em.GetTransactions(userID, extensionClientID, channelID, nil)
				So(err, ShouldNotBeNil)
			})

			Convey("Succeeds when dao succeeds", func() {
				dao.On("GetAllByUser", *userID, cursor).Return([]*extension_transaction.ExtensionTransaction{
					{TransactionID: "tx-1"}, {TransactionID: "tx-2"},
				}, pointers.StringP("new-cursor"), nil)
				outTransactions, outCursor, err := em.GetTransactions(userID, extensionClientID, channelID, nil)
				So(err, ShouldBeNil)
				So(len(outTransactions), ShouldEqual, 2)
				So(*outCursor, ShouldEqual, "new-cursor")
			})
		})

		Convey("When only userID and extensionClientID are present", func() {
			userID = pointers.StringP("userID")
			extensionClientID = pointers.StringP("extensionClientID")

			Convey("Errors when dao errors", func() {
				dao.On("GetAllByExtensionAndUser", *extensionClientID, *userID, cursor).Return(nil, nil, errors.New("test"))
				_, _, err := em.GetTransactions(userID, extensionClientID, channelID, nil)
				So(err, ShouldNotBeNil)
			})

			Convey("Succeeds when dao succeeds", func() {
				dao.On("GetAllByExtensionAndUser", *extensionClientID, *userID, cursor).Return([]*extension_transaction.ExtensionTransaction{
					{TransactionID: "tx-1"}, {TransactionID: "tx-2"},
				}, pointers.StringP("new-cursor"), nil)
				outTransactions, outCursor, err := em.GetTransactions(userID, extensionClientID, channelID, nil)
				So(err, ShouldBeNil)
				So(len(outTransactions), ShouldEqual, 2)
				So(*outCursor, ShouldEqual, "new-cursor")
			})
		})

		Convey("When userID, extensionClientID, and channelID are present", func() {
			userID = pointers.StringP("userID")
			extensionClientID = pointers.StringP("extensionClientID")
			channelID = pointers.StringP("channelID")

			Convey("Errors when dao errors", func() {
				dao.On("GetAllByChannelAndExtensionAndUser", *channelID, *extensionClientID, *userID, cursor).Return(nil, nil, errors.New("test"))
				_, _, err := em.GetTransactions(userID, extensionClientID, channelID, nil)
				So(err, ShouldNotBeNil)
			})

			Convey("Succeeds when dao succeeds", func() {
				dao.On("GetAllByChannelAndExtensionAndUser", *channelID, *extensionClientID, *userID, cursor).Return([]*extension_transaction.ExtensionTransaction{
					{TransactionID: "tx-1"}, {TransactionID: "tx-2"},
				}, pointers.StringP("new-cursor"), nil)
				outTransactions, outCursor, err := em.GetTransactions(userID, extensionClientID, channelID, nil)
				So(err, ShouldBeNil)
				So(len(outTransactions), ShouldEqual, 2)
				So(*outCursor, ShouldEqual, "new-cursor")
			})
		})
	})
}
