package lambda

import (
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/errors"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/client"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/lambda"
)

const (
	defaultRegion   = "us-west-2"
	runtimeNodejs43 = "nodejs4.3"
)

type LambdaClient struct {
	lambda *lambda.Lambda
}

type CreateEventSourceMappingRequest struct {
	BatchSize      int64
	FunctionName   string
	EventSourceARN string
}

type CreateLambdaRequest struct {
	FunctionName string
	Handler      string
	Description  string
	MemorySizeMB int64
	RoleArn      string
	TimeoutSec   int64
	CodeS3Bucket string
	CodeS3Key    string
}

func NewDefaultConfig() *aws.Config {
	return &aws.Config{
		Region: aws.String(defaultRegion),
	}
}

func New(provider client.ConfigProvider, lambdaConfig *aws.Config) *LambdaClient {
	return &LambdaClient{
		lambda: lambda.New(provider, lambdaConfig),
	}
}

func NewFromConfig(lambdaConfig *aws.Config) *LambdaClient {
	sess, _ := session.NewSession()
	return New(sess, lambdaConfig)
}

func NewFromDefaultConfig() *LambdaClient {
	return NewFromConfig(NewDefaultConfig())
}

func (c *LambdaClient) LambdaExists(functionName string) (bool, error) {
	lfo, err := c.lambda.ListFunctions(&lambda.ListFunctionsInput{})
	if err != nil {
		log.WithError(err).WithField("functionName", functionName).Error("Error checking if lambda exists with function name")
		return false, err
	}
	for _, f := range lfo.Functions {
		if functionName == *f.FunctionName {
			return true, nil
		}
	}
	return false, nil
}

func (c *LambdaClient) DeleteLambda(functionName string) error {
	log.Infof("Deleting lambda %s", functionName)
	_, err := c.lambda.DeleteFunction(&lambda.DeleteFunctionInput{
		FunctionName: aws.String(functionName),
	})
	return err
}

func (c *LambdaClient) EnsureLambdaDoesNotExist(functionName string) error {
	exists, err := c.LambdaExists(functionName)
	if err == nil && exists {
		err = c.DeleteLambda(functionName)
	}
	return err
}

func (c *LambdaClient) CreateLambda(request *CreateLambdaRequest) error {
	log.Infof("Creating lambda %s", request.FunctionName)
	_, err := c.lambda.CreateFunction(&lambda.CreateFunctionInput{
		FunctionName: aws.String(request.FunctionName),
		Handler:      aws.String(request.Handler),
		Description:  aws.String(request.Description),
		MemorySize:   aws.Int64(request.MemorySizeMB),
		Publish:      aws.Bool(true),
		Role:         aws.String(request.RoleArn),
		Runtime:      aws.String(runtimeNodejs43),
		Timeout:      aws.Int64(request.TimeoutSec),
		Code: &lambda.FunctionCode{
			S3Bucket: aws.String(request.CodeS3Bucket),
			S3Key:    aws.String(request.CodeS3Key),
		},
	})
	return err
}

func (c *LambdaClient) EnsureLambdaExists(request *CreateLambdaRequest) error {
	exists, err := c.LambdaExists(request.FunctionName)
	if err == nil && !exists {
		err = c.CreateLambda(request)
	}
	return err
}

func (c *LambdaClient) CountSourceMappings(functionName string, eventSourceARN string) (int, error) {
	resp, err := c.lambda.ListEventSourceMappings(&lambda.ListEventSourceMappingsInput{
		FunctionName:   aws.String(functionName),
		EventSourceArn: aws.String(eventSourceARN),
	})
	if err != nil {
		log.WithError(err).WithFields(log.Fields{
			"functionName": functionName,
			"source":       eventSourceARN,
		}).Error("Error counting source mappings")
		return 0, err
	}
	return len(resp.EventSourceMappings), nil
}

func (c *LambdaClient) EventSourceMappingExists(functionName string, eventSourceARN string) (bool, error) {
	count, err := c.CountSourceMappings(functionName, eventSourceARN)
	if err != nil {
		return false, err
	}

	if count > 1 {
		msg := "More than one event source mapping exists between function and source"
		log.WithFields(log.Fields{
			"functionName": functionName,
			"source":       eventSourceARN,
		}).Error(msg)
		return false, errors.New(msg)
	} else if count == 1 {
		return true, nil
	} else {
		return false, nil
	}
}

func (c *LambdaClient) GetEventSourceMappingUUID(functionName string, eventSourceARN string) (string, error) {
	resp, err := c.lambda.ListEventSourceMappings(&lambda.ListEventSourceMappingsInput{
		FunctionName:   aws.String(functionName),
		EventSourceArn: aws.String(eventSourceARN),
	})

	if err != nil {
		msg := "More than one event source mapping exists between function and source"
		log.WithError(err).WithFields(log.Fields{
			"functionName": functionName,
			"source":       eventSourceARN,
		}).Error(msg)
		return "", err
	}

	if len(resp.EventSourceMappings) < 1 {
		return "", errors.New("Event source not found")
	} else {
		return *resp.EventSourceMappings[0].UUID, nil
	}
}

func (c *LambdaClient) DeleteEventSourceMapping(uuid string) error {
	log.Infof("Deleting event source mapping with UUID %s", uuid)

	_, err := c.lambda.DeleteEventSourceMapping(&lambda.DeleteEventSourceMappingInput{
		UUID: aws.String(uuid),
	})
	if err != nil {
		log.WithError(err).WithField("uuid", uuid).Error("Error deleting event source mapping with UUID")
		return err
	}
	return nil
}

func (c *LambdaClient) CreateEventSourceMapping(request *CreateEventSourceMappingRequest) error {
	log.Infof("Creating event source mapping from %s to %s", request.EventSourceARN, request.FunctionName)

	_, err := c.lambda.CreateEventSourceMapping(&lambda.CreateEventSourceMappingInput{
		BatchSize:        aws.Int64(request.BatchSize),
		Enabled:          aws.Bool(true),
		FunctionName:     aws.String(request.FunctionName),
		EventSourceArn:   aws.String(request.EventSourceARN),
		StartingPosition: aws.String(lambda.EventSourcePositionLatest),
	})
	return err
}

func (c *LambdaClient) EnsureEventSourceMappingExists(request *CreateEventSourceMappingRequest) error {
	exists, err := c.EventSourceMappingExists(request.FunctionName, request.EventSourceARN)
	if err != nil {
		return err
	}
	if !exists {
		return c.CreateEventSourceMapping(request)
	}
	return nil
}

func (c *LambdaClient) EnsureEventSourceMappingDoesNotExist(functionName string, eventSourceArn string) error {
	exists, err := c.EventSourceMappingExists(functionName, eventSourceArn)
	if err != nil {
		return err
	}
	if exists {
		uuid, err := c.GetEventSourceMappingUUID(functionName, eventSourceArn)
		if err != nil {
			return err
		}
		err = c.DeleteEventSourceMapping(uuid)
		if err != nil {
			return err
		}
	}
	return nil
}
