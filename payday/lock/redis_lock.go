package lock

import (
	"time"

	"code.justin.tv/commerce/payday/errors"
	lock "github.com/bsm/redis-lock"
)

var FailedToObtainLockError = errors.New("failed to obtain lock")

const (
	defaultLockTimeout = 5 * time.Second
	defaultWaitTimeout = 1 * time.Second
	defaultWaitRetry   = 50 * time.Millisecond
)

type redisLockClient struct {
	RedisClient lock.RedisClient
	LockOptions *lock.LockOptions
}

func NewRedisLockClient(client lock.RedisClient, options *lock.LockOptions) LockingClient {
	return &redisLockClient{
		RedisClient: client,
		LockOptions: options,
	}
}

func DefaultLockOptions() *lock.LockOptions {
	return &lock.LockOptions{
		LockTimeout: defaultLockTimeout,
		WaitTimeout: defaultWaitTimeout,
		WaitRetry:   defaultWaitRetry,
	}
}

func (r *redisLockClient) ObtainLock(key string) (Lock, error) {
	rl, err := lock.ObtainLock(r.RedisClient, key, r.LockOptions)
	if err != nil {
		return nil, err
	}

	if rl == nil {
		return nil, FailedToObtainLockError
	}

	return &redisLock{
		RedisLock: rl,
	}, nil
}

type redisLock struct {
	RedisLock *lock.Lock
}

func (l *redisLock) Unlock() error {
	return l.RedisLock.Unlock()
}
