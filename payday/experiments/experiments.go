package experiments

var ProfilerIPs = map[string]interface{}{
	"10.193.114.250": nil, // i-04b4450318a071267 (staging)
	"10.193.113.168": nil, // i-0e97101ed6193ee7a (staging)
	"10.193.70.161":  nil, // i-00003aa8977f5dd5f us-west-2a (prod)
	"10.193.84.64":   nil, // i-00667fc024f24e12a us-west-2b (prod)
	"10.193.101.99":  nil, // i-005edf7c0eacd5035 us-west-2c (prod)
}
