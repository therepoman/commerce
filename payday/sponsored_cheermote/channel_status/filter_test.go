package channel_status

import (
	"context"
	"testing"
	"time"

	"code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/campaigns"
	"code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/channel_statuses"
	channel_status_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sponsored_cheermote/channel_status"
	eligible_channel_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sponsored_cheermote/eligible_channel"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestManagerImpl_FilterCampaignsByChannel(t *testing.T) {
	Convey("Given a sponsored cheermote manager", t, func() {
		ctx := context.Background()
		channelStatusFetcher := new(channel_status_mock.Fetcher)
		eligibleChannelGetter := new(eligible_channel_mock.Getter)

		manager := &filterer{
			Fetcher:               channelStatusFetcher,
			EligibleChannelGetter: eligibleChannelGetter,
		}

		var channelIDInput = "123"
		var campaignsInput []*campaigns.SponsoredCheermoteCampaign
		Convey("When the campaigns input is an empty array", func() {
			campaignsInput = []*campaigns.SponsoredCheermoteCampaign{}

			channelStatusFetcher.On("Fetch", mock.Anything, channelIDInput, campaignsInput).Return([]*channel_statuses.SponsoredCheermoteChannelStatus{}, nil)

			Convey("Manager should succeed, calling fetcher for information", func() {
				campaignsOutput, err := manager.Filter(ctx, campaignsInput, channelIDInput)
				So(err, ShouldBeNil)
				So(campaignListsEqual(campaignsInput, campaignsOutput), ShouldBeTrue)
				channelStatusFetcher.AssertCalled(t, "Fetch", mock.Anything, channelIDInput, campaignsInput)
			})
		})

		Convey("When the channel ID input is empty", func() {
			campaignsInput = []*campaigns.SponsoredCheermoteCampaign{}
			var emptyChannelIDInput = ""

			Convey("Manager should return nil without calling fetcher", func() {
				campaignsOutput, err := manager.Filter(ctx, campaignsInput, emptyChannelIDInput)
				So(err, ShouldBeNil)
				So(campaignsOutput, ShouldBeNil)
				channelStatusFetcher.AssertNotCalled(t, "Fetcher", mock.Anything, mock.Anything, mock.Anything)
			})
		})

		Convey("When the campaigns input is a valid array of campaigns", func() {
			campaign1 := &campaigns.SponsoredCheermoteCampaign{
				CampaignID:           "Test1",
				Prefix:               "test1",
				IsEnabled:            true,
				EndTime:              time.Date(3018, 1, 1, 0, 0, 0, 0, time.UTC),
				StartTime:            time.Date(2018, 1, 1, 0, 0, 0, 0, time.UTC),
				UsedBits:             int64(100),
				TotalBits:            int64(10000),
				MinBitsToBeSponsored: int64(10),
			}

			campaign2 := &campaigns.SponsoredCheermoteCampaign{
				CampaignID:           "Test2",
				Prefix:               "test2",
				IsEnabled:            true,
				EndTime:              time.Date(3018, 1, 1, 0, 0, 0, 0, time.UTC),
				StartTime:            time.Date(2018, 1, 1, 0, 0, 0, 0, time.UTC),
				UsedBits:             int64(100),
				TotalBits:            int64(10000),
				MinBitsToBeSponsored: int64(10),
			}

			campaign3 := &campaigns.SponsoredCheermoteCampaign{
				CampaignID:           "Test3",
				Prefix:               "test3",
				IsEnabled:            true,
				EndTime:              time.Date(3018, 1, 1, 0, 0, 0, 0, time.UTC),
				StartTime:            time.Date(2018, 1, 1, 0, 0, 0, 0, time.UTC),
				UsedBits:             int64(100),
				TotalBits:            int64(10000),
				MinBitsToBeSponsored: int64(10),
				EligibilityType:      campaigns.SponsoredCheermoteEligibilityTypeBlacklist,
				ChannelBlacklist:     map[string]bool{},
			}

			campaign4 := &campaigns.SponsoredCheermoteCampaign{
				CampaignID:           "Test4",
				Prefix:               "test4",
				IsEnabled:            true,
				EndTime:              time.Date(3018, 1, 1, 0, 0, 0, 0, time.UTC),
				StartTime:            time.Date(2018, 1, 1, 0, 0, 0, 0, time.UTC),
				UsedBits:             int64(100),
				TotalBits:            int64(10000),
				MinBitsToBeSponsored: int64(10),
				EligibilityType:      campaigns.SponsoredCheermoteEligibilityTypeBlacklist,
				//Tested Channel is blacklisted in campaign 4.
				ChannelBlacklist: map[string]bool{channelIDInput: true},
			}

			campaignsInput = []*campaigns.SponsoredCheermoteCampaign{
				campaign1,
				campaign2,
				campaign3,
				campaign4,
			}

			channelStatus1 := &channel_statuses.SponsoredCheermoteChannelStatus{
				CampaignID: campaign1.CampaignID,
				ChannelID:  channelIDInput,
				OptedIn:    false,
			}

			channelStatus2 := &channel_statuses.SponsoredCheermoteChannelStatus{
				CampaignID: campaign2.CampaignID,
				ChannelID:  channelIDInput,
				OptedIn:    false,
			}

			Convey("When the fetcher errors", func() {
				channelStatusFetcher.On("Fetch", mock.Anything, channelIDInput, mock.Anything).Return(nil, errors.New("test error"))

				Convey("Manager should error", func() {
					_, err := manager.Filter(ctx, campaignsInput, channelIDInput)
					So(err, ShouldNotBeNil)
				})
			})

			Convey("When the fetcher finds no records", func() {
				channelStatusFetcher.On("Fetch", mock.Anything, channelIDInput, mock.Anything).Return([]*channel_statuses.SponsoredCheermoteChannelStatus{}, nil)

				Convey("Manager should not error, return no records", func() {
					campaignStatuses, err := manager.Filter(ctx, campaignsInput, channelIDInput)
					So(err, ShouldBeNil)
					So(campaignListsEqual([]*campaigns.SponsoredCheermoteCampaign{campaign3}, campaignStatuses), ShouldBeTrue)
				})
			})

			Convey("When the fetcher finds one record", func() {
				channelStatus1.OptedIn = true

				channelStatusFetcher.On("Fetch", mock.Anything, channelIDInput, mock.Anything).Return([]*channel_statuses.SponsoredCheermoteChannelStatus{channelStatus1}, nil)

				Convey("When the channel eligibility getter errors", func() {
					eligibleChannelGetter.On("Get", mock.Anything, channelIDInput, channelStatus1.CampaignID).Return(false, errors.New("test error"))
					_, err := manager.Filter(ctx, campaignsInput, channelIDInput)
					So(err, ShouldNotBeNil)
				})

				Convey("When the channel is not eligible for this campaign", func() {
					eligibleChannelGetter.On("Get", mock.Anything, channelIDInput, channelStatus1.CampaignID).Return(false, nil)
					campaignOutput, err := manager.Filter(ctx, campaignsInput, channelIDInput)
					So(err, ShouldBeNil)
					So(campaignListsEqual([]*campaigns.SponsoredCheermoteCampaign{campaign3}, campaignOutput), ShouldBeTrue)
				})

				Convey("When the channel is eligible for this campaign", func() {
					eligibleChannelGetter.On("Get", mock.Anything, channelIDInput, channelStatus1.CampaignID).Return(true, nil)
					Convey("Manager should only return the first record", func() {
						campaignsOutput, err := manager.Filter(ctx, campaignsInput, channelIDInput)
						So(err, ShouldBeNil)
						So(campaignListsEqual([]*campaigns.SponsoredCheermoteCampaign{campaign3, campaign1}, campaignsOutput), ShouldBeTrue)
					})
				})
			})

			Convey("When channel is eligible for each campaign returned from Fetcher", func() {
				eligibleChannelGetter.On("Get", mock.Anything, mock.Anything, mock.Anything).Return(true, nil)

				Convey("When the first record is opted in and the second is opted out", func() {
					channelStatus1.OptedIn = true
					channelStatus2.OptedIn = false

					channelStatusFetcher.On("Fetch", mock.Anything, channelIDInput, mock.Anything).Return([]*channel_statuses.SponsoredCheermoteChannelStatus{channelStatus1, channelStatus2}, nil)

					Convey("Manager should only return the first record and the third record", func() {
						campaignsOutput, err := manager.Filter(ctx, campaignsInput, channelIDInput)
						So(err, ShouldBeNil)
						So(campaignListsEqual([]*campaigns.SponsoredCheermoteCampaign{campaign3, campaign1}, campaignsOutput), ShouldBeTrue)
					})
				})

				Convey("When the first record is opted out and the second is opted in", func() {
					channelStatus1.OptedIn = false
					channelStatus2.OptedIn = true

					channelStatusFetcher.On("Fetch", mock.Anything, channelIDInput, mock.Anything).Return([]*channel_statuses.SponsoredCheermoteChannelStatus{channelStatus1, channelStatus2}, nil)

					Convey("Manager should only return the second record and third record", func() {
						campaignsOutput, err := manager.Filter(ctx, campaignsInput, channelIDInput)
						So(err, ShouldBeNil)
						So(campaignListsEqual([]*campaigns.SponsoredCheermoteCampaign{campaign3, campaign2}, campaignsOutput), ShouldBeTrue)
					})
				})

				Convey("When both records are opted in", func() {
					channelStatus1.OptedIn = true
					channelStatus2.OptedIn = true

					channelStatusFetcher.On("Fetch", mock.Anything, channelIDInput, mock.Anything).Return([]*channel_statuses.SponsoredCheermoteChannelStatus{channelStatus1, channelStatus2}, nil)

					Convey("Manager should only return the first record", func() {
						campaignsOutput, err := manager.Filter(ctx, campaignsInput, channelIDInput)
						So(err, ShouldBeNil)
						So(campaignListsEqual([]*campaigns.SponsoredCheermoteCampaign{campaign3, campaign1, campaign2}, campaignsOutput), ShouldBeTrue)
					})
				})
			})
		})
	})
}

func campaignListsEqual(cl1, cl2 []*campaigns.SponsoredCheermoteCampaign) bool {
	if len(cl1) != len(cl2) {
		return false
	}

	for i, c1 := range cl1 {
		c2 := cl2[i]
		if c1 == nil || c2 == nil {
			if c1 == c2 {
				continue
			}
			return false
		}

		if !c1.Equals(c2) {
			return false
		}
	}
	return true
}
