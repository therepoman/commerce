package channel_status

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/clients/redis"
	"code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/channel_statuses"
	"code.justin.tv/commerce/payday/hystrix"
	cmds "code.justin.tv/commerce/payday/hystrix"
	"code.justin.tv/commerce/payday/utils/strings"
	go_redis "github.com/go-redis/redis"
)

const (
	sponsoredCampaignsChannelsKeyFormat = "sponsored-campaigns-channels-v2-%s-%s"
	sponsoredCampaignsChannelsCacheTTL  = time.Minute * 5
	sponsoredCampaignsChannelsNilEntry  = "<NOT_IN_DB>"
)

type Cache interface {
	Get(ctx context.Context, channelID, campaignID string) (*channel_statuses.SponsoredCheermoteChannelStatus, bool)
	Set(ctx context.Context, channelID, campaignID string, campaignChannelSettings *channel_statuses.SponsoredCheermoteChannelStatus)
	Del(ctx context.Context, channelID, campaignID string)
}

type cache struct {
	RedisClient redis.Client `inject:"redisClient"`
}

func NewCache() Cache {
	return &cache{}
}

func (c *cache) Get(ctx context.Context, channelID, campaignID string) (*channel_statuses.SponsoredCheermoteChannelStatus, bool) {
	var campaignCacheValue string
	err := hystrix.Do(cmds.CampaignsChannelCacheGet, func() error {
		var err error
		campaignCacheValue, err = c.RedisClient.Get(ctx, fmt.Sprintf(sponsoredCampaignsChannelsKeyFormat, channelID, campaignID)).Result()
		if err != nil && err != go_redis.Nil {
			log.WithError(err).Error("Error getting Campaign out of cache")
		}
		return nil
	}, nil)
	if err != nil {
		log.WithError(err).Warn("hystrix error while getting campaign channel settings out of cache")
	}

	if strings.Blank(campaignCacheValue) {
		return nil, false
	}

	if campaignCacheValue == sponsoredCampaignsChannelsNilEntry {
		return nil, true
	}

	var sponsoredCampaign channel_statuses.SponsoredCheermoteChannelStatus
	err = json.Unmarshal([]byte(campaignCacheValue), &sponsoredCampaign)
	if err != nil {
		c.Del(ctx, channelID, campaignID)
		log.WithError(err).Error("Error unmarshaling campaign status")
		return nil, false
	}

	return &sponsoredCampaign, true
}

func (c *cache) Set(ctx context.Context, channelID, campaignID string, campaignChannelSettings *channel_statuses.SponsoredCheermoteChannelStatus) {
	var value string
	if campaignChannelSettings != nil {
		campaignJson, err := json.Marshal(*campaignChannelSettings)
		if err != nil {
			log.WithError(err).Error("Error marshaling campaign status")
			return
		}

		value = string(campaignJson)
	} else {
		value = sponsoredCampaignsChannelsNilEntry
	}

	err := hystrix.Do(cmds.CampaignsChannelCacheSet, func() error {
		err := c.RedisClient.Set(ctx, fmt.Sprintf(sponsoredCampaignsChannelsKeyFormat, channelID, campaignID), value, sponsoredCampaignsChannelsCacheTTL).Err()
		if err != nil {
			log.WithError(err).Error("Error setting campaign status as nil in cache")
		}
		return nil
	}, nil)

	if err != nil {
		log.WithError(err).Warn("hystrix error while setting campaign channel status in cache")
	}
}

func (c *cache) Del(ctx context.Context, channelID, campaignID string) {
	err := hystrix.Do(cmds.CampaignsChannelCacheDel, func() error {
		err := c.RedisClient.Del(ctx, fmt.Sprintf(sponsoredCampaignsChannelsKeyFormat, channelID, campaignID)).Err()
		if err != nil {
			log.WithError(err).Error("Error deleting campaign status from cache")
		}
		return nil
	}, nil)
	if err != nil {
		log.WithError(err).Warn("hystrix error while deleting campaign channel status in cache")
	}
}
