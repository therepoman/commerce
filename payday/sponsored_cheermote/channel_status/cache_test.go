package channel_status

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"testing"

	"github.com/stretchr/testify/mock"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/channel_statuses"
	redis_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/redis"
	"github.com/go-redis/redis"
	. "github.com/smartystreets/goconvey/convey"
)

func TestCache_Get(t *testing.T) {
	Convey("Given a sponsored cheermote channel cache", t, func() {
		ctx := context.Background()
		redisClient := new(redis_mock.Client)

		cache := &cache{
			RedisClient: redisClient,
		}

		campaignID := "save-the-walrus"
		channelID := "123123123"
		cacheKey := fmt.Sprintf(sponsoredCampaignsChannelsKeyFormat, channelID, campaignID)

		campaign := channel_statuses.SponsoredCheermoteChannelStatus{
			CampaignID: campaignID,
			OptedIn:    true,
			ChannelID:  channelID,
		}

		campaignBytes, err := json.Marshal(campaign)
		if err != nil {
			log.WithError(err).Error("Error marshaling campaign channel status")
			t.Fail()
		}

		Convey("Given a record exists in the cache", func() {
			getResp := redis.NewStringResult(string(campaignBytes), nil)
			redisClient.On("Get", mock.Anything, cacheKey).Return(getResp)

			Convey("It should return the correct record", func() {
				actual, found := cache.Get(ctx, channelID, campaignID)
				So(actual, ShouldNotBeNil)
				So(actual.CampaignID, ShouldEqual, campaignID)
				So(found, ShouldBeTrue)
			})
		})

		Convey("Given there is an error fetching the record from cache", func() {
			getResp := redis.NewStringResult("", errors.New("WALRUS ERROR"))
			redisClient.On("Get", mock.Anything, cacheKey).Return(getResp)

			Convey("It should return nil", func() {
				actual, found := cache.Get(ctx, channelID, campaignID)
				So(actual, ShouldBeNil)
				So(found, ShouldBeFalse)
			})
		})

		Convey("Given the record does not exist in the cache", func() {
			getResp := redis.NewStringResult("", nil)
			redisClient.On("Get", mock.Anything, cacheKey).Return(getResp)

			Convey("It should return nil", func() {
				actual, found := cache.Get(ctx, channelID, campaignID)
				So(actual, ShouldBeNil)
				So(found, ShouldBeFalse)
			})
		})

		Convey("Given the record exists as nil in the cache", func() {
			getResp := redis.NewStringResult(sponsoredCampaignsChannelsNilEntry, nil)
			redisClient.On("Get", mock.Anything, cacheKey).Return(getResp)

			Convey("It should return nil", func() {
				actual, found := cache.Get(ctx, channelID, campaignID)
				So(actual, ShouldBeNil)
				So(found, ShouldBeTrue)
			})
		})

		Convey("Given the record is not a SponsoredCheermoteChannelStatus", func() {
			getResp := redis.NewStringResult(string(campaignBytes[5:]), nil)
			redisClient.On("Get", mock.Anything, cacheKey).Return(getResp)
			delResp := redis.NewIntCmd("del", nil)
			redisClient.On("Del", mock.Anything, cacheKey).Return(delResp)

			Convey("It should return nil and delete the record", func() {
				actual, found := cache.Get(ctx, channelID, campaignID)
				So(actual, ShouldBeNil)
				So(found, ShouldBeFalse)
				redisClient.AssertCalled(t, "Del", mock.Anything, cacheKey)
			})
		})
	})
}
