package channel_status

import (
	"context"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/campaigns"
	"code.justin.tv/commerce/payday/sponsored_cheermote/eligible_channel"
)

type Filterer interface {
	Filter(ctx context.Context, campaigns []*campaigns.SponsoredCheermoteCampaign, channelID string) ([]*campaigns.SponsoredCheermoteCampaign, error)
}

type filterer struct {
	Fetcher               Fetcher                 `inject:""`
	EligibleChannelGetter eligible_channel.Getter `inject:""`
}

func NewFilterer() Filterer {
	return &filterer{}
}

// Filters `campaignsList` by the channel eligibility, opt-in status, and the active status
func (f *filterer) Filter(ctx context.Context, campaignsList []*campaigns.SponsoredCheermoteCampaign, channelID string) ([]*campaigns.SponsoredCheermoteCampaign, error) {
	// No channel specified in API call
	if channelID == "" {
		return nil, nil
	}

	enabledCampaigns := make([]*campaigns.SponsoredCheermoteCampaign, 0)
	campaignsForStatusFetch := make([]*campaigns.SponsoredCheermoteCampaign, 0)

	// Check for blacklist campaigns and check if the channel is blacklisted
	for _, campaign := range campaignsList {
		if campaign != nil {
			if campaign.EligibilityType == campaigns.SponsoredCheermoteEligibilityTypeBlacklist {
				if campaign.IsActive() {
					_, found := campaign.ChannelBlacklist[channelID]
					if !found {
						enabledCampaigns = append(enabledCampaigns, campaign)
					}
				}
			} else {
				if campaign.IsActive() {
					campaignsForStatusFetch = append(campaignsForStatusFetch, campaign)
				}
			}
		}
	}

	// For campaigns that are not blacklist-type campaigns, check the channel status and eligibility (whitelist)
	statuses, err := f.Fetcher.Fetch(ctx, channelID, campaignsForStatusFetch)
	if err != nil {
		return nil, err
	}

	for _, status := range statuses {
		if status == nil {
			continue
		}

		var campaign *campaigns.SponsoredCheermoteCampaign
		for _, c := range campaignsForStatusFetch {
			if c.CampaignID == status.CampaignID {
				campaign = c
			}
		}

		if campaign == nil || !campaign.IsActive() {
			continue
		}

		eligible, err := f.EligibleChannelGetter.Get(ctx, channelID, status.CampaignID)
		if err != nil {
			log.WithError(err).WithFields(log.Fields{"ChannelID": channelID, "CampaignID": status.CampaignID}).Error("could not get channel eligibility")
			return nil, err
		}

		if !eligible {
			continue
		}

		if status.OptedIn {
			enabledCampaigns = append(enabledCampaigns, campaign)
		}
	}

	return enabledCampaigns, nil
}
