package channel_status

import (
	"context"
	"testing"

	"github.com/stretchr/testify/mock"

	"code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/channel_statuses"
	channel_statuses_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/channel_statuses"
	channel_status_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sponsored_cheermote/channel_status"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
)

func TestSetter_Set(t *testing.T) {
	Convey("give a campaign channel status setter", t, func() {
		ctx := context.Background()
		dao := new(channel_statuses_mock.ISponsoredCheermoteChannelStatusesDao)
		cache := new(channel_status_mock.Cache)

		setter := &setter{
			DAO:   dao,
			Cache: cache,
		}

		userID := "123123123"
		campaignID := "savethewalrus"

		Convey("when we error on updating the dynamo record", func() {
			dao.On("Update", &channel_statuses.SponsoredCheermoteChannelStatus{
				ChannelID:  userID,
				CampaignID: campaignID,
				OptedIn:    true,
			}).Return(errors.New("WALRUS STRIKE"))

			Convey("then we should return an error", func() {
				err := setter.Set(ctx, userID, campaignID, true)

				So(err, ShouldNotBeNil)
			})
		})

		Convey("when we succeed on updating the dynamo record", func() {
			dao.On("Update", &channel_statuses.SponsoredCheermoteChannelStatus{
				ChannelID:  userID,
				CampaignID: campaignID,
				OptedIn:    true,
			}).Return(nil)

			// Deleting a record has no return value.
			cache.On("Del", mock.Anything, userID, campaignID)

			Convey("then we should return nil since we were successful", func() {
				err := setter.Set(ctx, userID, campaignID, true)

				So(err, ShouldBeNil)
			})
		})
	})
}
