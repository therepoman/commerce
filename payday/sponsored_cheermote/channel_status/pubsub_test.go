package channel_status

import (
	"context"
	"encoding/json"
	"fmt"
	"testing"

	"code.justin.tv/commerce/payday/actions"
	"code.justin.tv/commerce/payday/actions/utils"
	cheerutils "code.justin.tv/commerce/payday/cheers/utils"
	dynamo_actions "code.justin.tv/commerce/payday/dynamo/actions"
	"code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/campaigns"
	client_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/chat/pubsub-go-pubclient/client"
	actions_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sponsored_cheermote/actions"
	campaign_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sponsored_cheermote/campaign"
	models2 "code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/models/api"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestPubSubber_OptIn(t *testing.T) {
	Convey("given a channel status pubsubber", t, func() {
		pubsubClient := new(client_mock.PubClient)
		campaignGetter := new(campaign_mock.Getter)
		actionsGetter := new(actions_mock.Getter)

		pubsubber := &pubSubber{
			PubClient:      pubsubClient,
			CampaignGetter: campaignGetter,
			ActionGetter:   actionsGetter,
		}

		ctx := context.Background()
		campaignID := "savethewalrus"
		campaignPrefix := "walrus"
		userID := "123123123"

		campaignFetcherOutput := &campaigns.SponsoredCheermoteCampaign{
			CampaignID: campaignID,
			Prefix:     campaignPrefix,
		}

		Convey("When we fail to lookup the campaign", func() {
			campaignGetter.On("Get", mock.Anything, campaignID).Return(nil, errors.New("WALRUS STRIKE"))

			Convey("then we return an error", func() {
				err := pubsubber.OptIn(ctx, campaignID, userID)

				So(err, ShouldNotBeNil)
			})
		})

		Convey("when we cannot find the campaign", func() {
			campaignGetter.On("Get", mock.Anything, campaignID).Return(nil, nil)

			Convey("then we return an error", func() {
				err := pubsubber.OptIn(ctx, campaignID, userID)

				So(err, ShouldNotBeNil)
			})
		})

		Convey("when we find the campaign", func() {
			campaignGetter.On("Get", mock.Anything, campaignID).Return(campaignFetcherOutput, nil)

			Convey("When we fail to lookup the action for the campaign", func() {
				actionsGetter.On("Get", mock.Anything, campaignFetcherOutput).Return(nil, errors.New("WALRUS STRIKE"))

				Convey("then we return an error", func() {
					err := pubsubber.OptIn(ctx, campaignID, userID)

					So(err, ShouldNotBeNil)
				})
			})

			Convey("When we cannot find the action for the campaign", func() {
				actionsGetter.On("Get", mock.Anything, campaignFetcherOutput).Return(nil, nil)

				Convey("then we return an error", func() {
					err := pubsubber.OptIn(ctx, campaignID, userID)

					So(err, ShouldNotBeNil)
				})
			})

			Convey("When we find the action for the campaign", func() {
				dynamoAction := dynamo_actions.Action{
					Prefix:          "walrus",
					CanonicalCasing: "Walrus",
					Type:            api.SponsoredAction,
				}

				actionsGetter.On("Get", mock.Anything, campaignFetcherOutput).Return(&dynamoAction, nil)

				apiAction := actions.ConvertDynamoActionToAPIAction(dynamoAction)
				apiAction = utils.AddCampaignToAction(apiAction, campaignFetcherOutput, 0)

				apiCheer := models2.Cheer{
					Prefix: "Walrus",
					Type:   models2.CheerTypeSponsored,
					Tiers:  cheerutils.BuildTiers("walrus", models2.CheerTypeSponsored),
					Campaign: &models2.CheerCampaign{
						ID: campaignID,
					},
				}

				pubsubMessage := models2.SponsoredCheermotesUpdateMessage{
					Type:       models2.AddSponsoredCheermote,
					CampaignID: campaignID,
					Action:     &apiAction,
					Template:   cheerutils.GetSponsoredActionTemplate(),
					Cheer:      &apiCheer,
				}

				pubsubMsgJson, _ := json.Marshal(pubsubMessage)

				topics := []string{fmt.Sprintf("%s.%s", models2.BitsEventPublicPubsubTopic, userID)}

				Convey("When we fail to publish to pubsub", func() {
					pubsubClient.On("Publish", ctx, topics, string(pubsubMsgJson), mock.Anything).Return(errors.New("WALRUS STRIKE"))

					Convey("then we return an error", func() {
						err := pubsubber.OptIn(ctx, campaignID, userID)

						So(err, ShouldNotBeNil)
					})
				})

				Convey("When we succeed to publish to pubsub", func() {
					pubsubClient.On("Publish", ctx, topics, string(pubsubMsgJson), mock.Anything).Return(nil)

					Convey("then we return nil since we succeeded", func() {
						err := pubsubber.OptIn(ctx, campaignID, userID)

						So(err, ShouldBeNil)
					})
				})
			})
		})
	})
}

func TestPubSubber_OptOut(t *testing.T) {
	Convey("given a channel status pubsubber", t, func() {
		pubsubClient := new(client_mock.PubClient)
		campaignGetter := new(campaign_mock.Getter)
		actionsGetter := new(actions_mock.Getter)

		pubsubber := &pubSubber{
			PubClient:      pubsubClient,
			CampaignGetter: campaignGetter,
			ActionGetter:   actionsGetter,
		}

		ctx := context.Background()
		campaignID := "savethewalrus"
		userID := "123123123"

		pubsubMessage := models2.SponsoredCheermotesUpdateMessage{
			Type:       models2.RemoveSponsoredCheermote,
			CampaignID: campaignID,
		}

		pubsubMsgJson, _ := json.Marshal(pubsubMessage)

		topics := []string{fmt.Sprintf("%s.%s", models2.BitsEventPublicPubsubTopic, userID)}

		Convey("When we fail to publish to pubsub", func() {
			pubsubClient.On("Publish", ctx, topics, string(pubsubMsgJson), mock.Anything).Return(errors.New("WALRUS STRIKE"))

			Convey("then we return an error", func() {
				err := pubsubber.OptOut(ctx, campaignID, userID)

				So(err, ShouldNotBeNil)
			})
		})

		Convey("When we succeed to publish to pubsub", func() {
			pubsubClient.On("Publish", ctx, topics, string(pubsubMsgJson), mock.Anything).Return(nil)

			Convey("then we return nil since we succeeded", func() {
				err := pubsubber.OptOut(ctx, campaignID, userID)

				So(err, ShouldBeNil)
			})
		})
	})
}
