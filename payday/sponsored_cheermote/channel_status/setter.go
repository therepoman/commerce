package channel_status

import (
	"context"

	"code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/channel_statuses"
)

type Setter interface {
	Set(ctx context.Context, userID, campaignID string, optedIn bool) error
}

type setter struct {
	DAO   channel_statuses.ISponsoredCheermoteChannelStatusesDao `inject:""`
	Cache Cache                                                  `inject:""`
}

func NewSetter() Setter {
	return &setter{}
}

func (s *setter) Set(ctx context.Context, userID, campaignID string, optedIn bool) error {
	err := s.DAO.Update(&channel_statuses.SponsoredCheermoteChannelStatus{
		ChannelID:  userID,
		CampaignID: campaignID,
		OptedIn:    optedIn,
	})

	if err != nil {
		return err
	}

	s.Cache.Del(ctx, userID, campaignID)

	return nil
}
