package channel_status

import (
	"context"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/campaigns"
	"code.justin.tv/commerce/payday/sponsored_cheermote/campaign"
	"code.justin.tv/commerce/payday/sponsored_cheermote/eligible_channel"
	"code.justin.tv/commerce/payday/sponsored_cheermote/models"
)

type Getter interface {
	Get(ctx context.Context, userID string) ([]*models.SponsoredCheermoteChannelStatus, error)
}

type getter struct {
	Manager               campaign.Manager        `inject:""`
	Fetcher               Fetcher                 `inject:""`
	EligibleChannelGetter eligible_channel.Getter `inject:""`
}

func NewGetter() Getter {
	return &getter{}
}

func (g *getter) Get(ctx context.Context, userID string) ([]*models.SponsoredCheermoteChannelStatus, error) {
	supportedCampaigns := g.Manager.GetActiveCampaigns(ctx)
	// Campaigns that use the channel_statuses and eligible_channels DynamoDB tables (i.e. whitelist campaigns)
	campaignsWithChannelStatus := make([]*campaigns.SponsoredCheermoteCampaign, 0)
	// Campaigns that only use the channel blacklist to determine eligibility (i.e. blacklist campaigns)
	campaignsWithChannelBlacklist := make([]*campaigns.SponsoredCheermoteCampaign, 0)

	for _, campaign := range supportedCampaigns {
		if campaign.EligibilityType == campaigns.SponsoredCheermoteEligibilityTypeBlacklist {
			campaignsWithChannelBlacklist = append(campaignsWithChannelBlacklist, campaign)
		} else {
			campaignsWithChannelStatus = append(campaignsWithChannelStatus, campaign)
		}
	}

	statusRecords, err := g.Fetcher.Fetch(ctx, userID, campaignsWithChannelStatus)
	if err != nil {
		return nil, err
	}

	channelStatuses := make([]*models.SponsoredCheermoteChannelStatus, 0)

	// Check that channel is eligible through channel status for relevant campaigns
	for _, campaign := range campaignsWithChannelStatus {
		eligible, err := g.EligibleChannelGetter.Get(ctx, userID, campaign.CampaignID)
		if err != nil {
			log.WithError(err).WithFields(log.Fields{"ChannelID": userID, "CampaignID": campaign.CampaignID}).Error("could not get channel eligibility")
			return nil, err
		}

		if !eligible {
			continue
		}

		optedIn := false

		for _, record := range statusRecords {
			if campaign.CampaignID == record.CampaignID {
				optedIn = record.OptedIn
				break
			}
		}

		channelStatuses = append(channelStatuses, &models.SponsoredCheermoteChannelStatus{
			CampaignID:                campaign.CampaignID,
			ChannelID:                 userID,
			OptedIn:                   optedIn,
			BrandImageURL:             campaign.BrandImageURL,
			BrandName:                 campaign.BrandName,
			SponsoredAmountThresholds: campaign.SponsoredAmountThresholds,
			StartTime:                 campaign.StartTime,
			EndTime:                   campaign.EndTime,
		})
	}

	// Check that channel is eligible through channel blacklist for relevant campaigns
	for _, campaign := range campaignsWithChannelBlacklist {
		_, blacklisted := campaign.ChannelBlacklist[userID]

		if blacklisted {
			continue
		}

		channelStatuses = append(channelStatuses, &models.SponsoredCheermoteChannelStatus{
			CampaignID:                campaign.CampaignID,
			ChannelID:                 userID,
			OptedIn:                   true,
			BrandImageURL:             campaign.BrandImageURL,
			BrandName:                 campaign.BrandName,
			SponsoredAmountThresholds: campaign.SponsoredAmountThresholds,
			StartTime:                 campaign.StartTime,
			EndTime:                   campaign.EndTime,
		})
	}

	return channelStatuses, nil
}
