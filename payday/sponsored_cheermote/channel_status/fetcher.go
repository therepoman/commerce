package channel_status

import (
	"context"

	"code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/campaigns"
	"code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/channel_statuses"
)

type Fetcher interface {
	Fetch(ctx context.Context, channelID string, supportedCampaigns []*campaigns.SponsoredCheermoteCampaign) ([]*channel_statuses.SponsoredCheermoteChannelStatus, error)
}

type fetcher struct {
	Dao   channel_statuses.ISponsoredCheermoteChannelStatusesDao `inject:""`
	Cache Cache                                                  `inject:""`
}

func NewFetcher() Fetcher {
	return &fetcher{}
}

func (f *fetcher) Fetch(ctx context.Context, channelID string, supportedCampaigns []*campaigns.SponsoredCheermoteCampaign) ([]*channel_statuses.SponsoredCheermoteChannelStatus, error) {
	records := make([]*channel_statuses.SponsoredCheermoteChannelStatus, 0)
	channelStatusesToFetch := make([]*campaigns.SponsoredCheermoteCampaign, 0)
	for _, campaign := range supportedCampaigns {
		cachedDynamoRecord, existsInCache := f.Cache.Get(ctx, channelID, campaign.CampaignID)
		if cachedDynamoRecord != nil {
			records = append(records, cachedDynamoRecord)
		} else if !existsInCache {
			channelStatusesToFetch = append(channelStatusesToFetch, campaign)
		}
	}

	if len(channelStatusesToFetch) > 0 {
		dynamoCampaigns, err := f.Dao.GetBatch(channelID, channelStatusesToFetch)
		if err != nil {
			return nil, err
		}
		campaignsFoundMap := make(map[string]*channel_statuses.SponsoredCheermoteChannelStatus)
		for _, dynamoCampaign := range dynamoCampaigns {
			campaignsFoundMap[dynamoCampaign.CampaignID] = dynamoCampaign
			f.Cache.Set(ctx, channelID, dynamoCampaign.CampaignID, dynamoCampaign)
			if dynamoCampaign != nil {
				records = append(records, dynamoCampaign)
			}
		}
		// we need to loop through and figure out what channels don't have entries.
		for _, campaigns := range channelStatusesToFetch {
			_, exists := campaignsFoundMap[campaigns.CampaignID]
			if !exists {
				f.Cache.Set(ctx, channelID, campaigns.CampaignID, nil)
			}
		}
	}

	return records, nil
}
