package channel_status

import (
	"context"
	"testing"

	"github.com/stretchr/testify/mock"

	"code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/campaigns"
	"code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/channel_statuses"
	campaign_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sponsored_cheermote/campaign"
	channel_status_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sponsored_cheermote/channel_status"
	eligible_channel_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sponsored_cheermote/eligible_channel"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
)

func TestGetter_Get(t *testing.T) {
	Convey("given a channel status getter", t, func() {
		ctx := context.Background()
		fetcher := new(channel_status_mock.Fetcher)
		manager := new(campaign_mock.Manager)
		eligibleChannelGetter := new(eligible_channel_mock.Getter)

		getter := &getter{
			Fetcher:               fetcher,
			Manager:               manager,
			EligibleChannelGetter: eligibleChannelGetter,
		}

		userID := "123123123"
		campaignID := "savethewalrus"
		blacklistCampaignID := "destroythewalrus"

		Convey("When there are only eligibility (whitelist) campaigns that apply to the channel", func() {
			manager.On("GetActiveCampaigns", mock.Anything).Return([]*campaigns.SponsoredCheermoteCampaign{{CampaignID: campaignID}})

			Convey("when we error fetching the record", func() {
				fetcher.On("Fetch", mock.Anything, userID, []*campaigns.SponsoredCheermoteCampaign{{CampaignID: campaignID}}).Return(nil, errors.New("WALRUS STRIKE"))

				Convey("We should return an error", func() {
					_, err := getter.Get(ctx, userID)

					So(err, ShouldNotBeNil)
				})
			})

			Convey("when the fetcher returns nil", func() {
				fetcher.On("Fetch", mock.Anything, userID, []*campaigns.SponsoredCheermoteCampaign{{CampaignID: campaignID}}).Return(nil, nil)

				Convey("when we error getting eligible channel", func() {
					eligibleChannelGetter.On("Get", mock.Anything, userID, campaignID).Return(false, errors.New("OHKI STRIKE"))

					Convey("we should return an error", func() {
						_, err := getter.Get(ctx, userID)

						So(err, ShouldNotBeNil)
					})
				})

				Convey("when the channel is not eligible", func() {
					eligibleChannelGetter.On("Get", mock.Anything, userID, campaignID).Return(false, nil)

					Convey("we should return empty status without an err", func() {
						campaignStatus, err := getter.Get(ctx, userID)

						So(err, ShouldBeNil)
						So(len(campaignStatus), ShouldEqual, 0)
					})
				})

				Convey("when the channel is eligible for the campaign", func() {
					eligibleChannelGetter.On("Get", mock.Anything, userID, campaignID).Return(true, nil)

					Convey("we should return the only active campaign with its opt-in status false", func() {
						campaignStatus, err := getter.Get(ctx, userID)

						So(err, ShouldBeNil)
						So(campaignStatus[0].OptedIn, ShouldBeFalse)
					})
				})
			})

			Convey("when the fetcher returns an empty array", func() {
				fetcher.On("Fetch", mock.Anything, userID, []*campaigns.SponsoredCheermoteCampaign{{CampaignID: campaignID}}).Return([]*channel_statuses.SponsoredCheermoteChannelStatus{}, nil)

				Convey("when we error getting eligible channel", func() {
					eligibleChannelGetter.On("Get", mock.Anything, userID, campaignID).Return(false, errors.New("OHKI STRIKE"))

					Convey("we should return an error", func() {
						_, err := getter.Get(ctx, userID)

						So(err, ShouldNotBeNil)
					})
				})

				Convey("when the channel is not eligible", func() {
					eligibleChannelGetter.On("Get", mock.Anything, userID, campaignID).Return(false, nil)

					Convey("we should return empty status without an err", func() {
						campaignStatus, err := getter.Get(ctx, userID)

						So(err, ShouldBeNil)
						So(len(campaignStatus), ShouldEqual, 0)
					})
				})

				Convey("when the channel is eligible for the campaign", func() {
					eligibleChannelGetter.On("Get", mock.Anything, userID, campaignID).Return(true, nil)

					Convey("we should return the only active campaign with its opt-in status false", func() {
						campaignStatus, err := getter.Get(ctx, userID)

						So(err, ShouldBeNil)
						So(campaignStatus[0].OptedIn, ShouldBeFalse)
					})
				})
			})

			Convey("when the fetcher returns the cached record", func() {
				campaignStatusRecord := &channel_statuses.SponsoredCheermoteChannelStatus{
					CampaignID: campaignID,
					ChannelID:  userID,
					OptedIn:    true,
				}

				fetcher.On("Fetch", mock.Anything, userID, []*campaigns.SponsoredCheermoteCampaign{{CampaignID: campaignID}}).Return([]*channel_statuses.SponsoredCheermoteChannelStatus{campaignStatusRecord}, nil)

				Convey("when we error getting eligible channel", func() {
					eligibleChannelGetter.On("Get", mock.Anything, userID, campaignID).Return(false, errors.New("OHKI STRIKE"))

					Convey("we should return an error", func() {
						_, err := getter.Get(ctx, userID)

						So(err, ShouldNotBeNil)
					})
				})

				Convey("when the channel is not eligible", func() {
					eligibleChannelGetter.On("Get", mock.Anything, userID, campaignID).Return(false, nil)

					Convey("we should return empty status without an err", func() {
						campaignStatus, err := getter.Get(ctx, userID)

						So(err, ShouldBeNil)
						So(len(campaignStatus), ShouldEqual, 0)
					})
				})

				Convey("when the channel is eligible for the campaign", func() {
					eligibleChannelGetter.On("Get", mock.Anything, userID, campaignID).Return(true, nil)

					Convey("we should return the only active campaign with its opt-in status true, and no error", func() {
						campaignStatus, err := getter.Get(ctx, userID)

						So(err, ShouldBeNil)
						So(campaignStatus, ShouldNotBeNil)
						So(campaignStatus[0].OptedIn, ShouldBeTrue)
					})
				})
			})
		})

		Convey("When there are blacklist campaigns that apply to the channel", func() {
			campaignStatusRecord := &channel_statuses.SponsoredCheermoteChannelStatus{
				CampaignID: campaignID,
				ChannelID:  userID,
				OptedIn:    true,
			}

			fetcher.On("Fetch", mock.Anything, userID, []*campaigns.SponsoredCheermoteCampaign{{CampaignID: campaignID}}).Return([]*channel_statuses.SponsoredCheermoteChannelStatus{campaignStatusRecord}, nil)

			Convey("when we error getting eligible channel", func() {
				eligibleChannelGetter.On("Get", mock.Anything, userID, campaignID).Return(false, errors.New("OHKI STRIKE"))
				manager.On("GetActiveCampaigns", mock.Anything).Return([]*campaigns.SponsoredCheermoteCampaign{
					{
						CampaignID: campaignID,
					},
					{
						CampaignID:      blacklistCampaignID,
						EligibilityType: campaigns.SponsoredCheermoteEligibilityTypeBlacklist,
					},
				})

				Convey("we should return an error", func() {
					_, err := getter.Get(ctx, userID)

					So(err, ShouldNotBeNil)
				})
			})

			Convey("when the channel is not eligible for the first campaign", func() {
				eligibleChannelGetter.On("Get", mock.Anything, userID, campaignID).Return(false, nil)
				manager.On("GetActiveCampaigns", mock.Anything).Return([]*campaigns.SponsoredCheermoteCampaign{
					{
						CampaignID: campaignID,
					},
					{
						CampaignID:      blacklistCampaignID,
						EligibilityType: campaigns.SponsoredCheermoteEligibilityTypeBlacklist,
					},
				})

				Convey("we should only return the blacklist campaign status its opt-in status true without an err", func() {
					campaignStatus, err := getter.Get(ctx, userID)

					So(err, ShouldBeNil)
					So(len(campaignStatus), ShouldEqual, 1)
					So(campaignStatus[0].OptedIn, ShouldBeTrue)
				})
			})

			Convey("when the channel is eligible for the first campaign", func() {
				eligibleChannelGetter.On("Get", mock.Anything, userID, campaignID).Return(true, nil)
				manager.On("GetActiveCampaigns", mock.Anything).Return([]*campaigns.SponsoredCheermoteCampaign{
					{
						CampaignID: campaignID,
					},
					{
						CampaignID:      blacklistCampaignID,
						EligibilityType: campaigns.SponsoredCheermoteEligibilityTypeBlacklist,
					},
				})

				Convey("we should return both campaigns with opt-in status true, and no error", func() {
					campaignStatus, err := getter.Get(ctx, userID)

					So(err, ShouldBeNil)
					So(len(campaignStatus), ShouldEqual, 2)
					So(campaignStatus[0].OptedIn, ShouldBeTrue)
					So(campaignStatus[1].OptedIn, ShouldBeTrue)
				})
			})

			Convey("when the channel is eligible for the first campaign, but is blacklisted by the second campaign", func() {
				eligibleChannelGetter.On("Get", mock.Anything, userID, campaignID).Return(true, nil)
				manager.On("GetActiveCampaigns", mock.Anything).Return([]*campaigns.SponsoredCheermoteCampaign{
					{
						CampaignID: campaignID,
					},
					{
						CampaignID:       blacklistCampaignID,
						EligibilityType:  campaigns.SponsoredCheermoteEligibilityTypeBlacklist,
						ChannelBlacklist: map[string]bool{userID: true},
					},
				})

				Convey("we should only return the first campaign campaign with opt-in status true, and no error", func() {
					campaignStatus, err := getter.Get(ctx, userID)

					So(err, ShouldBeNil)
					So(len(campaignStatus), ShouldEqual, 1)
					So(campaignStatus[0].OptedIn, ShouldBeTrue)
				})
			})
		})
	})
}
