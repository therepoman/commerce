package channel_status

import (
	"context"
	"encoding/json"
	"fmt"

	pubclient "code.justin.tv/chat/pubsub-go-pubclient/client"
	log "code.justin.tv/commerce/logrus"
	apiActions "code.justin.tv/commerce/payday/actions"
	"code.justin.tv/commerce/payday/actions/utils"
	cheerutils "code.justin.tv/commerce/payday/cheers/utils"
	utils2 "code.justin.tv/commerce/payday/cheers/utils"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/sponsored_cheermote/actions"
	"code.justin.tv/commerce/payday/sponsored_cheermote/campaign"
)

type PubSubber interface {
	OptIn(ctx context.Context, campaignID, userID string) error
	OptOut(ctx context.Context, campaignID, userID string) error
}

type pubSubber struct {
	PubClient      pubclient.PubClient `inject:""`
	CampaignGetter campaign.Getter     `inject:"campaignGetter"`
	ActionGetter   actions.Getter      `inject:""`
}

func NewPubSubber() PubSubber {
	return &pubSubber{}
}

func (p *pubSubber) OptIn(ctx context.Context, campaignID, userID string) error {
	supportedCampaign, err := p.CampaignGetter.Get(ctx, campaignID)
	if err != nil {
		log.WithError(err).WithField("campaignID", campaignID).Error("error looking up campaign for given campaign ID while publishing opt in status")
		return err
	}

	if supportedCampaign == nil {
		msg := "could not find campaign for given campaign ID while publishing opt in status"
		log.WithField("campaignID", campaignID).Error(msg)
		return errors.Notef(err, msg)
	}

	supportedAction, err := p.ActionGetter.Get(ctx, supportedCampaign)
	if err != nil {
		log.WithError(err).WithField("campaignID", campaignID).Error("error looking up action for given campaign ID while publishing opt in status")
		return err
	}

	if supportedAction == nil {
		msg := "could not find action for given campaign ID while publishing opt in status"
		log.WithField("campaignID", campaignID).Error(msg)
		return errors.Notef(err, msg)
	}

	apiAction := apiActions.ConvertDynamoActionToAPIAction(*supportedAction)
	apiCheer := utils2.ConvertDynamoActionToCheer(supportedAction)

	// Can't add the user used bits to the pubsub, since this is a broadcast for the user opting in.
	apiAction = utils.AddCampaignToAction(apiAction, supportedCampaign, 0)
	cheerutils.AddCampaignToCheer(&apiCheer, supportedCampaign, 0)

	pubsubMessage := models.SponsoredCheermotesUpdateMessage{
		Type:       models.AddSponsoredCheermote,
		CampaignID: campaignID,
		Action:     &apiAction,
		Template:   cheerutils.GetSponsoredActionTemplate(),
		Cheer:      &apiCheer,
	}

	pubsubMsgJson, err := json.Marshal(pubsubMessage)
	if err != nil {
		msg := "Failed to marshal channel settings update pubsub message"
		log.WithError(err).Error(msg)
		return errors.Notef(err, msg)
	}

	topics := []string{fmt.Sprintf("%s.%s", models.BitsEventPublicPubsubTopic, userID)}
	err = p.PubClient.Publish(ctx, topics, string(pubsubMsgJson), nil)
	if err != nil {
		msg := "Failed to publish channel settings update to pubsub"
		log.WithError(err).Error(msg)
		return errors.Notef(err, msg)
	}

	return nil
}

func (p *pubSubber) OptOut(ctx context.Context, campaignID, userID string) error {
	pubsubMessage := models.SponsoredCheermotesUpdateMessage{
		Type:       models.RemoveSponsoredCheermote,
		CampaignID: campaignID,
	}

	pubsubMsgJson, err := json.Marshal(pubsubMessage)
	if err != nil {
		msg := "Failed to marshal channel settings update pubsub message"
		log.WithError(err).Error(msg)
		return errors.Notef(err, msg)
	}

	topics := []string{fmt.Sprintf("%s.%s", models.BitsEventPublicPubsubTopic, userID)}
	err = p.PubClient.Publish(ctx, topics, string(pubsubMsgJson), nil)
	if err != nil {
		msg := "Failed to publish channel settings update to pubsub"
		log.WithError(err).Error(msg)
		return errors.Notef(err, msg)
	}

	return nil
}
