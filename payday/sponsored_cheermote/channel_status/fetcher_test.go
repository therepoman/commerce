package channel_status

import (
	"context"
	"testing"
	"time"

	channel_status_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sponsored_cheermote/channel_status"

	"code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/campaigns"
	"code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/channel_statuses"
	channel_statuses_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/channel_statuses"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestFetcher_Fetch(t *testing.T) {
	Convey("Given a Fetcher", t, func() {
		ctx := context.Background()
		dao := new(channel_statuses_mock.ISponsoredCheermoteChannelStatusesDao)
		cache := new(channel_status_mock.Cache)

		f := &fetcher{
			Dao:   dao,
			Cache: cache,
		}

		channelID := "1233123123"

		campaign1 := &campaigns.SponsoredCheermoteCampaign{
			CampaignID: "Test1",
			IsEnabled:  true,
			EndTime:    time.Date(3018, 1, 1, 0, 0, 0, 0, time.UTC),
			StartTime:  time.Date(2018, 1, 1, 0, 0, 0, 0, time.UTC),
			Prefix:     "test1",
		}

		campaign2 := &campaigns.SponsoredCheermoteCampaign{
			CampaignID: "Test2",
			IsEnabled:  true,
			EndTime:    time.Date(3018, 1, 1, 0, 0, 0, 0, time.UTC),
			StartTime:  time.Date(2018, 1, 1, 0, 0, 0, 0, time.UTC),
			Prefix:     "test2",
		}

		channelStatus1 := &channel_statuses.SponsoredCheermoteChannelStatus{
			CampaignID: campaign1.CampaignID,
			ChannelID:  channelID,
			OptedIn:    true,
		}

		channelStatus2 := &channel_statuses.SponsoredCheermoteChannelStatus{
			CampaignID: campaign2.CampaignID,
			ChannelID:  channelID,
			OptedIn:    false,
		}

		Convey("Given a list of campaign statuses that are in the cache", func() {
			cache.On("Get", mock.Anything, channelID, campaign1.CampaignID).Return(channelStatus1, true)
			cache.On("Get", mock.Anything, channelID, campaign2.CampaignID).Return(channelStatus2, true)

			Convey("When we fetch the list of records", func() {
				actual, err := f.Fetch(ctx, channelID, []*campaigns.SponsoredCheermoteCampaign{campaign1, campaign2})

				So(err, ShouldBeNil)
				So(channelStatusListsEqual(actual, []*channel_statuses.SponsoredCheermoteChannelStatus{channelStatus1, channelStatus2}), ShouldBeTrue)

				Convey("We only fetch from the cache and do not hit Dynamo", func() {
					dao.AssertNotCalled(t, "GetBatch", mock.Anything, mock.Anything)
					cache.AssertCalled(t, "Get", mock.Anything, channelID, campaign1.CampaignID)
					cache.AssertCalled(t, "Get", mock.Anything, channelID, campaign2.CampaignID)
				})
			})
		})

		Convey("Given a list of campaigns both in the cache and not in the cache", func() {
			cache.On("Get", mock.Anything, channelID, campaign1.CampaignID).Return(channelStatus1, true)
			cache.On("Get", mock.Anything, channelID, campaign2.CampaignID).Return(nil, false)

			cache.On("Set", mock.Anything, channelID, campaign2.CampaignID, channelStatus2).Return(nil)

			dao.On("GetBatch", channelID, []*campaigns.SponsoredCheermoteCampaign{campaign2}).Return([]*channel_statuses.SponsoredCheermoteChannelStatus{channelStatus2}, nil)

			Convey("When we fetch the list of records", func() {
				actual, err := f.Fetch(ctx, channelID, []*campaigns.SponsoredCheermoteCampaign{campaign1, campaign2})

				So(err, ShouldBeNil)
				So(channelStatusListsEqual(actual, []*channel_statuses.SponsoredCheermoteChannelStatus{channelStatus1, channelStatus2}), ShouldBeTrue)

				Convey("We fetch from both the cache and Dynamo", func() {
					cache.AssertCalled(t, "Get", mock.Anything, channelID, campaign1.CampaignID)
					cache.AssertCalled(t, "Get", mock.Anything, channelID, campaign2.CampaignID)
					dao.AssertCalled(t, "GetBatch", channelID, []*campaigns.SponsoredCheermoteCampaign{campaign2})
				})

				Convey("We set the new record from Dynamo in the cache", func() {
					cache.AssertCalled(t, "Set", mock.Anything, channelID, campaign2.CampaignID, channelStatus2)
				})
			})
		})

		Convey("Given a list of campaigns not at all in the cache", func() {
			cache.On("Get", mock.Anything, channelID, campaign1.CampaignID).Return(nil, false)
			cache.On("Get", mock.Anything, channelID, campaign2.CampaignID).Return(nil, false)

			cache.On("Set", mock.Anything, channelID, campaign1.CampaignID, channelStatus1).Return(nil)
			cache.On("Set", mock.Anything, channelID, campaign2.CampaignID, channelStatus2).Return(nil)

			dao.On("GetBatch", channelID, []*campaigns.SponsoredCheermoteCampaign{campaign1, campaign2}).Return([]*channel_statuses.SponsoredCheermoteChannelStatus{channelStatus1, channelStatus2}, nil)

			Convey("When we fetch the list of records", func() {
				actual, err := f.Fetch(ctx, channelID, []*campaigns.SponsoredCheermoteCampaign{campaign1, campaign2})

				So(err, ShouldBeNil)
				So(channelStatusListsEqual(actual, []*channel_statuses.SponsoredCheermoteChannelStatus{channelStatus1, channelStatus2}), ShouldBeTrue)

				Convey("We fetch from both the cache and Dynamo", func() {
					cache.AssertCalled(t, "Get", mock.Anything, channelID, campaign1.CampaignID)
					cache.AssertCalled(t, "Get", mock.Anything, channelID, campaign2.CampaignID)
					dao.AssertCalled(t, "GetBatch", channelID, []*campaigns.SponsoredCheermoteCampaign{campaign1, campaign2})
				})

				Convey("We set the new records from Dynamo in the cache", func() {
					cache.AssertCalled(t, "Set", mock.Anything, channelID, campaign1.CampaignID, channelStatus1)
					cache.AssertCalled(t, "Set", mock.Anything, channelID, campaign2.CampaignID, channelStatus2)
				})
			})
		})
	})
}

func channelStatusListsEqual(cl1, cl2 []*channel_statuses.SponsoredCheermoteChannelStatus) bool {
	if len(cl1) != len(cl2) {
		return false
	}

	for i, c1 := range cl1 {
		c2 := cl2[i]
		if c1 == nil || c2 == nil {
			if c1 == c2 {
				continue
			}
			return false
		}

		if !c1.Equals(c2) {
			return false
		}
	}
	return true
}
