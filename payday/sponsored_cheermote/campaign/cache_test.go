package campaign

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/mock"

	"code.justin.tv/commerce/gogogadget/ttlcache"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/campaigns"
	redis_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/redis"
	"github.com/go-redis/redis"
	. "github.com/smartystreets/goconvey/convey"
)

func TestCacheImpl_Get(t *testing.T) {
	Convey("Given a sponsored cheermote cache", t, func() {
		ctx := context.Background()
		redisClient := new(redis_mock.Client)

		cache := &cache{
			RedisClient:   redisClient,
			InMemoryCache: ttlcache.NewCache(10 * time.Millisecond),
		}

		campaignID := "save-the-walrus"
		cacheKey := fmt.Sprintf(sponsoredCampaignsKeyFormat, campaignID)

		campaign := campaigns.SponsoredCheermoteCampaign{
			CampaignID:           campaignID,
			Prefix:               "save-the-walrus",
			IsEnabled:            true,
			EndTime:              time.Date(3018, 1, 1, 0, 0, 0, 0, time.UTC),
			StartTime:            time.Date(2018, 1, 1, 0, 0, 0, 0, time.UTC),
			UsedBits:             int64(100),
			TotalBits:            int64(10000),
			MinBitsToBeSponsored: int64(10),
		}

		campaignBytes, err := json.Marshal(campaign)
		if err != nil {
			log.WithError(err).Error("Error marshaling campaign")
			t.Fail()
		}

		Convey("Given a record exists in the cache", func() {
			getResp := redis.NewStringResult(string(campaignBytes), nil)
			redisClient.On("Get", mock.Anything, cacheKey).Return(getResp)

			Convey("It should return the correct record", func() {
				actual := cache.Get(ctx, campaignID)
				So(actual, ShouldNotBeNil)
				So(actual.CampaignID, ShouldEqual, campaignID)
			})
		})

		Convey("Given there is an error fetching the record from cache", func() {
			getResp := redis.NewStringResult("", errors.New("WALRUS ERROR"))
			redisClient.On("Get", mock.Anything, cacheKey).Return(getResp)

			Convey("It should return nil", func() {
				actual := cache.Get(ctx, campaignID)
				So(actual, ShouldBeNil)
			})
		})

		Convey("Given the record does not exist in the cache", func() {
			getResp := redis.NewStringResult("", nil)
			redisClient.On("Get", mock.Anything, cacheKey).Return(getResp)

			Convey("It should return nil", func() {
				actual := cache.Get(ctx, campaignID)
				So(actual, ShouldBeNil)
			})
		})

		Convey("Given the record is not a SponsoredCheermoteCampaign", func() {
			getResp := redis.NewStringResult(string(campaignBytes[5:]), nil)
			redisClient.On("Get", mock.Anything, cacheKey).Return(getResp)
			delResp := redis.NewIntCmd("del", nil)
			redisClient.On("Del", mock.Anything, cacheKey).Return(delResp)

			Convey("It should return nil and delete the record", func() {
				actual := cache.Get(ctx, campaignID)
				So(actual, ShouldBeNil)
				redisClient.AssertCalled(t, "Del", mock.Anything, cacheKey)
			})
		})
	})
}
