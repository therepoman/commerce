package campaign

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"code.justin.tv/commerce/gogogadget/ttlcache"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/clients/redis"
	"code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/campaigns"
	"code.justin.tv/commerce/payday/hystrix"
	cmds "code.justin.tv/commerce/payday/hystrix"
	"code.justin.tv/commerce/payday/utils/strings"
	go_redis "github.com/go-redis/redis"
)

const (
	sponsoredCampaignsKeyFormat = "sponsored-campaigns-%s"
	sponsoredCampaignsCacheTTL  = 15 * time.Minute

	inMemoryCacheCleanupInterval = time.Minute
	inMemoryCacheTTL             = 5 * time.Minute
)

type Cache interface {
	Get(ctx context.Context, campaignID string) *campaigns.SponsoredCheermoteCampaign
	Set(ctx context.Context, campaignID string, campaign campaigns.SponsoredCheermoteCampaign)
	Del(ctx context.Context, campaignID string)
}

type cache struct {
	RedisClient   redis.Client `inject:"redisClient"`
	InMemoryCache ttlcache.Cache
}

func NewCache() Cache {
	return &cache{
		InMemoryCache: ttlcache.NewCache(inMemoryCacheCleanupInterval),
	}
}

func (c *cache) Get(ctx context.Context, campaignID string) *campaigns.SponsoredCheermoteCampaign {
	cacheKey := fmt.Sprintf(sponsoredCampaignsKeyFormat, campaignID)

	cachedItem := c.InMemoryCache.Get(cacheKey)
	if cachedItem != nil {
		cachedAction, ok := cachedItem.(campaigns.SponsoredCheermoteCampaign)
		if ok {
			return &cachedAction
		} else {
			log.WithField("campaignID", campaignID).Error("received the wrong type when getting spomo campaign from in-memory cache")
		}
	}

	var campaignCacheValue string

	err := hystrix.Do(cmds.CampaignsCacheGet, func() error {
		var err error
		campaignCacheValue, err = c.RedisClient.Get(ctx, cacheKey).Result()
		if err != nil && err != go_redis.Nil {
			log.WithError(err).Error("Error getting Campaign out of cache")
		}
		return nil
	}, nil)

	if err != nil {
		log.WithError(err).Warn("hystrix error while fetching campaign out of cache")
		return nil
	}

	if strings.Blank(campaignCacheValue) {
		return nil
	}

	var sponsoredCampaign campaigns.SponsoredCheermoteCampaign
	err = json.Unmarshal([]byte(campaignCacheValue), &sponsoredCampaign)
	if err != nil {
		c.Del(ctx, campaignID)
		log.WithError(err).Error("Error unmarshaling campaign")
		return nil
	}

	c.InMemoryCache.Set(cacheKey, sponsoredCampaign, inMemoryCacheTTL)

	return &sponsoredCampaign
}

func (c *cache) Set(ctx context.Context, campaignID string, sponsoredCampaign campaigns.SponsoredCheermoteCampaign) {
	cacheKey := fmt.Sprintf(sponsoredCampaignsKeyFormat, campaignID)

	c.InMemoryCache.Set(cacheKey, sponsoredCampaign, inMemoryCacheTTL)

	campaignJson, err := json.Marshal(sponsoredCampaign)
	if err != nil {
		log.WithError(err).Error("Error marshaling campaign")
		return
	}

	err = hystrix.Do(cmds.CampaignsCacheSet, func() error {
		err := c.RedisClient.Set(ctx, cacheKey, string(campaignJson), sponsoredCampaignsCacheTTL).Err()
		if err != nil {
			log.WithError(err).Error("Error setting campaign in cache")
		}
		return nil
	}, nil)
	if err != nil {
		log.WithError(err).Warn("hystrix error while setting campaign in cache")
	}
}

func (c *cache) Del(ctx context.Context, campaignID string) {
	cacheKey := fmt.Sprintf(sponsoredCampaignsKeyFormat, campaignID)

	c.InMemoryCache.Delete(cacheKey)

	err := hystrix.Do(cmds.CampaignsCacheSet, func() error {
		err := c.RedisClient.Del(ctx, cacheKey).Err()
		if err != nil {
			log.WithError(err).Error("Error deleting campaign from cache")
		}
		return nil
	}, nil)
	if err != nil {
		log.WithError(err).Warn("hystrix error while deleting campaign in cache")
	}
}
