package campaign

import (
	"context"
	"testing"

	"github.com/stretchr/testify/mock"

	"code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/campaigns"
	campaign_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sponsored_cheermote/campaign"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
)

func TestGetter_Get(t *testing.T) {
	Convey("Given a campaign getter", t, func() {
		ctx := context.Background()
		fetcher := new(campaign_mock.Fetcher)

		getter := &getter{
			Fetcher: fetcher,
		}

		campaignID := "savethewalrus"

		Convey("When we error on fetching the record from the cache", func() {
			fetcher.On("Fetch", mock.Anything, campaignID).Return(nil, errors.New("WALRUS STRIKE"))

			Convey("We should return an error", func() {
				_, err := getter.Get(ctx, campaignID)

				So(err, ShouldNotBeNil)
			})
		})

		Convey("When we return a record from the fetcher", func() {
			campaign := &campaigns.SponsoredCheermoteCampaign{
				CampaignID: campaignID,
			}

			fetcher.On("Fetch", mock.Anything, campaignID).Return(campaign, nil)

			Convey("We should return the record", func() {
				record, err := getter.Get(ctx, campaignID)

				So(err, ShouldBeNil)
				So(record, ShouldNotBeNil)
				So(record.CampaignID, ShouldEqual, campaignID)
			})
		})
	})
}
