package campaign

import (
	"context"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/campaigns"
)

type Getter interface {
	Get(ctx context.Context, campaignID string) (*campaigns.SponsoredCheermoteCampaign, error)
}

type getter struct {
	Fetcher Fetcher `inject:""`
}

func NewGetter() Getter {
	return &getter{}
}

func (g *getter) Get(ctx context.Context, campaignID string) (*campaigns.SponsoredCheermoteCampaign, error) {
	record, err := g.Fetcher.Fetch(ctx, campaignID)
	if err != nil {
		log.WithField("campaignID", campaignID).WithError(err).Error("error getting campaign from fetcher")
		return nil, err
	}

	return record, nil
}
