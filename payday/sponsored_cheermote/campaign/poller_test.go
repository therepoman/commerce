package campaign

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

var campaignsYaml = `- campaignId: "walrus-foo-bar"
`

func TestSponsoredCheermoteCampaignsPoller(t *testing.T) {
	Convey("Can load sponsored campaigns files", t, func() {
		Convey("When File is empty", func() {
			campaignIDs, err := loadCampaignsYaml([]byte(""))
			So(err, ShouldBeNil)
			So(campaignIDs, ShouldBeEmpty)
		})
		Convey("Can load a valid file", func() {
			campaignIDs, err := loadCampaignsYaml([]byte(campaignsYaml))
			So(err, ShouldBeNil)
			So(campaignIDs, ShouldNotBeEmpty)
			Convey("Has correct values", func() {
				campaignID := campaignIDs[0]
				So(campaignID.CampaignID, ShouldEqual, "walrus-foo-bar")
			})
		})
	})
}
