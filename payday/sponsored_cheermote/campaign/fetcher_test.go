package campaign

import (
	"context"
	"testing"
	"time"

	"code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/campaigns"
	campaigns_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/campaigns"
	campaign_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sponsored_cheermote/campaign"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestFetcher_Fetch(t *testing.T) {
	Convey("Given a fetcher", t, func() {
		ctx := context.Background()
		dao := new(campaigns_mock.ISponsoredCheermoteCampaignDao)
		cache := new(campaign_mock.Cache)

		f := &fetcher{
			Dao:   dao,
			Cache: cache,
		}

		campaign1 := &campaigns.SponsoredCheermoteCampaign{
			CampaignID: "Test1",
			IsEnabled:  true,
			EndTime:    time.Date(3018, 1, 1, 0, 0, 0, 0, time.UTC),
			StartTime:  time.Date(2018, 1, 1, 0, 0, 0, 0, time.UTC),
			Prefix:     "test1",
		}

		Convey("Given the campaign is in the cache", func() {
			cache.On("Get", mock.Anything, campaign1.CampaignID).Return(campaign1)

			Convey("When we fetch the list of records", func() {
				actual, err := f.Fetch(ctx, campaign1.CampaignID)

				So(err, ShouldBeNil)
				So(actual, ShouldEqual, campaign1)

				Convey("We only fetch from the cache and do not hit Dynamo", func() {
					dao.AssertNotCalled(t, "GetBatch", mock.Anything)
					cache.AssertCalled(t, "Get", mock.Anything, campaign1.CampaignID)
				})
			})
		})

		Convey("Given the campaign is not in the cache", func() {
			cache.On("Get", mock.Anything, campaign1.CampaignID).Return(nil)
			cache.On("Set", mock.Anything, campaign1.CampaignID, *campaign1).Return(nil)

			dao.On("Get", campaign1.CampaignID).Return(campaign1, nil)

			Convey("When we fetch the list of records", func() {
				actual, err := f.Fetch(ctx, campaign1.CampaignID)

				So(err, ShouldBeNil)
				So(actual, ShouldEqual, campaign1)

				Convey("We fetch from both the cache and Dynamo", func() {
					cache.AssertCalled(t, "Get", mock.Anything, campaign1.CampaignID)
					dao.AssertCalled(t, "Get", campaign1.CampaignID)
				})

				Convey("We set the new record from Dynamo in the cache", func() {
					cache.AssertCalled(t, "Set", mock.Anything, campaign1.CampaignID, *campaign1)
				})
			})
		})
	})
}

func TestFetcher_FetchBatch(t *testing.T) {
	Convey("Given a Fetcher", t, func() {
		ctx := context.Background()
		dao := new(campaigns_mock.ISponsoredCheermoteCampaignDao)
		cache := new(campaign_mock.Cache)

		f := &fetcher{
			Dao:   dao,
			Cache: cache,
		}

		campaign1 := &campaigns.SponsoredCheermoteCampaign{
			CampaignID: "Test1",
			IsEnabled:  true,
			EndTime:    time.Date(3018, 1, 1, 0, 0, 0, 0, time.UTC),
			StartTime:  time.Date(2018, 1, 1, 0, 0, 0, 0, time.UTC),
			Prefix:     "test1",
		}

		campaign2 := &campaigns.SponsoredCheermoteCampaign{
			CampaignID: "Test2",
			IsEnabled:  true,
			EndTime:    time.Date(3018, 1, 1, 0, 0, 0, 0, time.UTC),
			StartTime:  time.Date(2018, 1, 1, 0, 0, 0, 0, time.UTC),
			Prefix:     "test2",
		}

		Convey("Given a list of campaigns that are in the cache", func() {
			cache.On("Get", mock.Anything, campaign1.CampaignID).Return(campaign1)
			cache.On("Get", mock.Anything, campaign2.CampaignID).Return(campaign2)

			Convey("When we fetch the list of records", func() {
				actual, err := f.FetchBatch(ctx, []string{campaign1.CampaignID, campaign2.CampaignID})

				So(err, ShouldBeNil)
				So(campaignListsEqual(actual, []*campaigns.SponsoredCheermoteCampaign{campaign1, campaign2}), ShouldBeTrue)

				Convey("We only fetch from the cache and do not hit Dynamo", func() {
					dao.AssertNotCalled(t, "GetBatch", mock.Anything)
					cache.AssertCalled(t, "Get", mock.Anything, campaign1.CampaignID)
					cache.AssertCalled(t, "Get", mock.Anything, campaign2.CampaignID)
				})
			})
		})

		Convey("Given a list of campaigns both in the cache and not in the cache", func() {
			cache.On("Get", mock.Anything, campaign1.CampaignID).Return(campaign1)
			cache.On("Get", mock.Anything, campaign2.CampaignID).Return(nil)

			cache.On("Set", mock.Anything, campaign2.CampaignID, *campaign2).Return(nil)

			dao.On("GetBatch", []string{campaign2.CampaignID}).Return([]*campaigns.SponsoredCheermoteCampaign{campaign2}, nil)

			Convey("When we fetch the list of records", func() {
				actual, err := f.FetchBatch(ctx, []string{campaign1.CampaignID, campaign2.CampaignID})

				So(err, ShouldBeNil)
				So(campaignListsEqual(actual, []*campaigns.SponsoredCheermoteCampaign{campaign1, campaign2}), ShouldBeTrue)

				Convey("We fetch from both the cache and Dynamo", func() {
					cache.AssertCalled(t, "Get", mock.Anything, campaign1.CampaignID)
					cache.AssertCalled(t, "Get", mock.Anything, campaign2.CampaignID)
					dao.AssertCalled(t, "GetBatch", []string{campaign2.CampaignID})
				})

				Convey("We set the new record from Dynamo in the cache", func() {
					cache.AssertCalled(t, "Set", mock.Anything, campaign2.CampaignID, *campaign2)
				})
			})
		})

		Convey("Given a list of campaigns not at all in the cache", func() {
			cache.On("Get", mock.Anything, campaign1.CampaignID).Return(nil)
			cache.On("Get", mock.Anything, campaign2.CampaignID).Return(nil)

			cache.On("Set", mock.Anything, campaign1.CampaignID, *campaign1).Return(nil)
			cache.On("Set", mock.Anything, campaign2.CampaignID, *campaign2).Return(nil)

			dao.On("GetBatch", []string{campaign1.CampaignID, campaign2.CampaignID}).Return([]*campaigns.SponsoredCheermoteCampaign{campaign1, campaign2}, nil)

			Convey("When we fetch the list of records", func() {
				actual, err := f.FetchBatch(ctx, []string{campaign1.CampaignID, campaign2.CampaignID})

				So(err, ShouldBeNil)
				So(campaignListsEqual(actual, []*campaigns.SponsoredCheermoteCampaign{campaign1, campaign2}), ShouldBeTrue)

				Convey("We fetch from both the cache and Dynamo", func() {
					cache.AssertCalled(t, "Get", mock.Anything, campaign1.CampaignID)
					cache.AssertCalled(t, "Get", mock.Anything, campaign2.CampaignID)
					dao.AssertCalled(t, "GetBatch", []string{campaign1.CampaignID, campaign2.CampaignID})
				})

				Convey("We set the new records from Dynamo in the cache", func() {
					cache.AssertCalled(t, "Set", mock.Anything, campaign1.CampaignID, *campaign1)
					cache.AssertCalled(t, "Set", mock.Anything, campaign2.CampaignID, *campaign2)
				})
			})
		})
	})
}

func TestFetcher_CampaignHack(t *testing.T) {
	hack_floor_value_start := 1000000
	Convey("Given a Fetcher", t, func() {
		Convey("Given a low used bits on a campaign, check if we cap the value to 1,000,000", func() {
			low_bits_used := int64(5000)
			campaign := campaigns.SponsoredCheermoteCampaign{
				TotalBits:  100000000000,
				UsedBits:   low_bits_used,
				CampaignID: "amazon-hunters-2020",
			}

			//process on a value twice to ensure nothing silly happens
			mobileBarHack(&campaign)
			mobileBarHack(&campaign)

			So(campaign.TotalBits, ShouldEqual, hack_floor_value_start)
			So(campaign.UsedBits, ShouldEqual, low_bits_used)
		})

		Convey("Given a high used bits on a campaign, check if we cap the value to 1,000,000", func() {
			high_bits_used := int64(1000000)
			very_high_total_bits := int64(100000000000)
			campaign := campaigns.SponsoredCheermoteCampaign{
				TotalBits:  very_high_total_bits,
				UsedBits:   high_bits_used,
				CampaignID: "amazon-hunters-2020",
			}

			//process on a value twice to ensure nothing silly happens
			mobileBarHack(&campaign)
			pinned_total_value := campaign.TotalBits
			pinned_used_value := campaign.UsedBits
			mobileBarHack(&campaign)

			So(campaign.TotalBits, ShouldBeLessThan, very_high_total_bits)
			So(campaign.UsedBits, ShouldEqual, high_bits_used)
			So(campaign.TotalBits, ShouldEqual, pinned_total_value)
			So(campaign.UsedBits, ShouldEqual, pinned_used_value)
			So(campaign.TotalBits, ShouldBeGreaterThan, hack_floor_value_start)
		})

		Convey("Given a non specific campaign, ensure we do nothing", func() {
			bits_used := int64(157612374)
			total_bits := int64(618346134214312)
			campaign := campaigns.SponsoredCheermoteCampaign{
				TotalBits:  total_bits,
				UsedBits:   bits_used,
				CampaignID: "amazon-hunters-2020",
			}

			//process on a value twice to ensure nothing silly happens
			mobileBarHack(&campaign)
			pinned_total_value := campaign.TotalBits
			pinned_used_value := campaign.UsedBits
			mobileBarHack(&campaign)

			So(campaign.TotalBits, ShouldBeLessThan, total_bits)
			So(campaign.UsedBits, ShouldEqual, bits_used)
			So(campaign.TotalBits, ShouldEqual, pinned_total_value)
			So(campaign.UsedBits, ShouldEqual, pinned_used_value)
		})
	})
}
