package campaign

import (
	"math/rand"
	"sync"
	"time"

	"context"

	"code.justin.tv/chat/golibs/graceful"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/campaigns"
	"code.justin.tv/commerce/payday/s3"
	"code.justin.tv/commerce/payday/sponsored_cheermote/models"
	"gopkg.in/yaml.v2"
)

const (
	refreshDelayMaxRandomOffset = time.Second * 30
	refreshDelay                = time.Minute * 5
)

type Poller interface {
	RefreshCampaignsListAtInterval()
	Init() error
}

type ListGetter interface {
	GetList(ctx context.Context) ([]*campaigns.SponsoredCheermoteCampaign, error)
}

type poller struct {
	campaigns []string
	S3Client  s3.IS3Client `inject:""`
	bucket    string
	key       string
	mutex     *sync.RWMutex
	Fetcher   Fetcher   `inject:""`
	PubSubber PubSubber `inject:""`
}

func NewPoller(bucket string, key string) Poller {
	sponsoredCampaigns := make([]string, 0)

	mutex := &sync.RWMutex{}

	return &poller{
		campaigns: sponsoredCampaigns,
		mutex:     mutex,
		bucket:    bucket,
		key:       key,
	}
}

func createSupportedCampaignsListFromS3(s3client s3.IS3Client, bucket string, key string) ([]models.S3Campaign, error) {
	tagBytes, err := s3client.LoadFile(bucket, key)
	if err != nil {
		log.WithFields(log.Fields{
			"key":    key,
			"bucket": bucket,
		}).Error("could not load file from S3 for SupportedCampaigns", err)
		return nil, err
	}

	return loadCampaignsYaml(tagBytes)
}

func loadCampaignsYaml(bytes []byte) ([]models.S3Campaign, error) {
	s3Campaigns := make([]models.S3Campaign, 0)
	err := yaml.Unmarshal(bytes, &s3Campaigns)
	if err != nil {
		return nil, err
	}

	return s3Campaigns, nil
}

func (p *poller) refreshCampaignsList(ctx context.Context) error {

	s3Campaigns, err := createSupportedCampaignsListFromS3(p.S3Client, p.bucket, p.key)
	if err != nil {
		return err
	}

	p.mutex.Lock()
	defer p.mutex.Unlock()

	s3CampaignIDs := make([]string, 0)
	for _, c := range s3Campaigns {
		s3CampaignIDs = append(s3CampaignIDs, c.CampaignID)
	}

	now := time.Now()
	campaignRecords, err := p.Fetcher.FetchBatch(ctx, s3CampaignIDs)
	if err != nil {
		return err
	}

	updatedList := make([]string, 0)
	for _, record := range campaignRecords {
		if record.IsEnabled && now.After(record.EndTime) {
			err := p.PubSubber.CompleteCampaign(context.Background(), record.CampaignID)
			if err != nil {
				log.WithField("campaignID", record.CampaignID).WithError(err).Error("could not end campaign from poller")
			}

		}
		updatedList = append(updatedList, record.CampaignID)
	}

	p.campaigns = updatedList

	return nil
}

func (p *poller) Init() error {
	ctx := context.Background()
	if exists, err := p.S3Client.BucketExists(p.bucket); !exists || err != nil {
		log.WithError(err).WithField("bucket", p.bucket).Error("Bucket does not exist for SupportedCampaigns object creation")
		return err
	}

	if exists, err := p.S3Client.FileExists(p.bucket, p.key); !exists || err != nil {
		log.WithFields(log.Fields{
			"key":    p.key,
			"bucket": p.bucket,
		}).WithError(err).Error("Key does not exist in Bucket for SupportedCampaigns object creation")
		return err
	}

	err := p.refreshCampaignsList(ctx)
	if err != nil {
		log.WithError(err).Error("Error while refreshing campaign list")
		return err
	}

	return nil
}

func (p *poller) RefreshCampaignsListAtInterval() {
	ctx := context.Background()
	delayOffset := rand.Int63n(int64(refreshDelayMaxRandomOffset))
	timer := time.NewTicker(refreshDelay + time.Duration(delayOffset))

	for {
		select {
		case <-graceful.ShuttingDown():
			return
		case <-timer.C:
			err := p.refreshCampaignsList(ctx)
			if err != nil {
				log.WithError(err).Error("Error refreshing campaigns")
			}
		}
	}
}

func (p *poller) GetList(ctx context.Context) ([]*campaigns.SponsoredCheermoteCampaign, error) {
	p.mutex.RLock()
	defer p.mutex.RUnlock()

	records, err := p.Fetcher.FetchBatch(ctx, p.campaigns)
	if err != nil {
		return []*campaigns.SponsoredCheermoteCampaign{}, err
	}

	return records, nil
}
