package campaign

import (
	"context"
	"encoding/json"
	"fmt"
	"testing"

	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/campaigns"
	client_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/chat/pubsub-go-pubclient/client"
	actions_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/actions"
	campaigns_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/campaigns"
	lock_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/lock"
	metrics_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/metrics"
	campaign_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sponsored_cheermote/campaign"
	"code.justin.tv/commerce/payday/models"
	"github.com/go-redis/redis_rate"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestPubSubber_CompleteCampaign(t *testing.T) {
	Convey("given a channel status pubsubber", t, func() {
		fetcher := new(campaign_mock.Fetcher)
		dao := new(campaigns_mock.ISponsoredCheermoteCampaignDao)
		cache := new(campaign_mock.Cache)
		pubsubClient := new(client_mock.PubClient)
		lockingClient := new(lock_mock.LockingClient)
		redisLock := new(lock_mock.Lock)
		actionCache := new(actions_mock.Cache)

		pubsubber := &pubSubber{
			DAO:           dao,
			Fetcher:       fetcher,
			Cache:         cache,
			PubClient:     pubsubClient,
			LockingClient: lockingClient,
			ActionCache:   actionCache,
			Config:        &config.Configuration{SponsoredCheermoteCampaignPubsubDefaultRateLimitPerSecond: -1},
			Throttle:      &redis_rate.Limiter{},
		}

		ctx := context.Background()
		campaignID := "savethewalrus"

		pubsubMessage := models.SponsoredCheermotesUpdateMessage{
			Type:       models.SponsoredCheermoteCampaignComplete,
			CampaignID: campaignID,
		}

		pubsubMsgJson, _ := json.Marshal(pubsubMessage)

		topics := []string{fmt.Sprintf("%s.%s", models.BitsCampaignsPubsubTopic, "updates")}

		lockingClient.On("ObtainLock", fmt.Sprintf("%s-%s", RedisLockKeyPrefix, campaignID)).Return(redisLock, nil)
		redisLock.On("Unlock").Return(nil)

		Convey("when we fail to fetch the campaign record", func() {
			fetcher.On("Fetch", mock.Anything, campaignID).Return(nil, errors.New("WALRUS STRIKE"))

			Convey("then we return an error", func() {
				err := pubsubber.CompleteCampaign(ctx, campaignID)

				So(err, ShouldNotBeNil)
			})
		})

		Convey("when we cannot find the campaign record", func() {
			fetcher.On("Fetch", mock.Anything, campaignID).Return(nil, nil)

			Convey("then we return an error", func() {
				err := pubsubber.CompleteCampaign(ctx, campaignID)

				So(err, ShouldNotBeNil)
			})
		})

		Convey("when we find the campaign record", func() {
			campaignRecord := &campaigns.SponsoredCheermoteCampaign{
				CampaignID:             campaignID,
				IsEnabled:              true,
				PubSubRateLimitSeconds: -1,
			}

			fetcher.On("Fetch", mock.Anything, campaignID).Return(campaignRecord, nil)

			Convey("When the campaign is already disabled", func() {
				campaignRecord.IsEnabled = false

				Convey("we should do nothing", func() {
					err := pubsubber.CompleteCampaign(ctx, campaignID)

					So(err, ShouldBeNil)
				})
			})

			Convey("when the campaign is not disabled", func() {
				campaignRecord.IsEnabled = true

				Convey("when we fail to update the dynamo record", func() {
					dao.On("Update", &campaigns.SponsoredCheermoteCampaign{CampaignID: campaignID, IsEnabled: false, PubSubRateLimitSeconds: -1}).Return(errors.New("WALRUS STRIKE"))

					Convey("then we return an error", func() {
						err := pubsubber.CompleteCampaign(ctx, campaignID)

						So(err, ShouldNotBeNil)
					})
				})

				Convey("when we succeed to update the dynamo record", func() {
					dao.On("Update", &campaigns.SponsoredCheermoteCampaign{CampaignID: campaignID, IsEnabled: false, PubSubRateLimitSeconds: -1}).Return(nil)
					cache.On("Del", mock.Anything, campaignID).Return()
					actionCache.On("Del", mock.Anything, campaignRecord.Prefix).Return()

					Convey("When we fail to publish to pubsub", func() {
						pubsubClient.On("Publish", ctx, topics, string(pubsubMsgJson), mock.Anything).Return(errors.New("WALRUS STRIKE"))

						Convey("then we return an error", func() {
							err := pubsubber.CompleteCampaign(ctx, campaignID)

							So(err, ShouldNotBeNil)
						})
					})

					Convey("When we succeed to publish to pubsub", func() {
						pubsubClient.On("Publish", ctx, topics, string(pubsubMsgJson), mock.Anything).Return(nil)

						Convey("then we return nil since we succeeded", func() {
							err := pubsubber.CompleteCampaign(ctx, campaignID)

							So(err, ShouldBeNil)
						})
					})
				})
			})
		})
	})
}

func TestPubSubber_TotalUpdate(t *testing.T) {
	Convey("When pubsubber is setup", t, func() {
		ctx := context.Background()

		pubsubClient := new(client_mock.PubClient)
		stats := new(metrics_mock.Statter)

		pubsubber := &pubSubber{
			PubClient: pubsubClient,
			Config:    &config.Configuration{SponsoredCheermoteCampaignPubsubDefaultRateLimitPerSecond: -1},
			Throttle:  &redis_rate.Limiter{},
			Statter:   stats,
		}

		stats.On("Inc", mock.Anything, mock.Anything)

		Convey("send total updates is called with nothing to do, it does nothing", func() {
			pubsubber.SendTotalUpdates(ctx, map[string]models.BitEmoteTotal{})
		})

		Convey("send total updates is called with non-sponsored amounts, it does nothing", func() {
			pubsubber.SendTotalUpdates(ctx, map[string]models.BitEmoteTotal{
				"bontaCheer": {
					CheeredTotal: 100,
				},
			})
		})

		Convey("send total updates is called with sponsored cheer and no campaign info, it does nothing", func() {
			pubsubber.SendTotalUpdates(ctx, map[string]models.BitEmoteTotal{
				"brand": {
					CheeredTotal: 100,
					MatchedTotal: 10,
				},
			})
		})

		Convey("send total updates is called with sponsored cheer missing matched amount, it does nothing", func() {
			pubsubber.SendTotalUpdates(ctx, map[string]models.BitEmoteTotal{
				"brand": {
					CheeredTotal: 100,
					Campaign: &campaigns.SponsoredCheermoteCampaign{
						CampaignID:             "anime-bears-best-bears",
						UsedBits:               10,
						PubSubRateLimitSeconds: -1, // rate limit of 0 avoids having to mock redis
					},
				},
			})
		})

		Convey("send total updates encounters an error when publishing, doesn't do anything but log it", func() {
			pubsubClient.On("Publish", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(errors.New("NO-ANIME-BEARS"))

			pubsubber.SendTotalUpdates(ctx, map[string]models.BitEmoteTotal{
				"brand": {
					CheeredTotal: 100,
					MatchedTotal: 10,
					Campaign: &campaigns.SponsoredCheermoteCampaign{
						CampaignID:             "anime-bears-best-bears",
						UsedBits:               10,
						PubSubRateLimitSeconds: -1, // rate limit of 0 avoids having to mock redis
					},
				},
			})
		})

		Convey("send total updates is called with a single campaign, should publish it", func() {
			pubsubClient.On("Publish", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)

			pubsubber.SendTotalUpdates(ctx, map[string]models.BitEmoteTotal{
				"brand": {
					CheeredTotal: 100,
					MatchedTotal: 10,
					Campaign: &campaigns.SponsoredCheermoteCampaign{
						CampaignID:             "anime-bears-best-bears",
						UsedBits:               10,
						PubSubRateLimitSeconds: -1, // rate limit of 0 avoids having to mock redis
					},
				},
			})

			pubsubClient.AssertCalled(t, "Publish", ctx,
				[]string{fmt.Sprintf("%s.%s", models.BitsCampaignsPubsubTopic, "updates")},
				"{\"type\":\"sponsored-cheermote-update-used\",\"campaign_id\":\"anime-bears-best-bears\",\"used_amount\":20}", mock.Anything)
		})

		Convey("send total updates is called with a two campaigns, should publish both", func() {
			pubsubClient.On("Publish", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)

			pubsubber.SendTotalUpdates(ctx, map[string]models.BitEmoteTotal{
				"brand": {
					CheeredTotal: 100,
					MatchedTotal: 10,
					Campaign: &campaigns.SponsoredCheermoteCampaign{
						CampaignID:             "anime-bears-best-bears",
						UsedBits:               10,
						PubSubRateLimitSeconds: -1, // rate limit of 0 avoids having to mock redis
					},
				},
				"coolerBrand": {
					CheeredTotal: 50,
					MatchedTotal: 5,
					Campaign: &campaigns.SponsoredCheermoteCampaign{
						CampaignID:             "walrus-for-president",
						UsedBits:               8,
						PubSubRateLimitSeconds: -1, // rate limit of 0 avoids having to mock redis
					},
				},
			})

			pubsubClient.AssertCalled(t, "Publish", ctx,
				[]string{fmt.Sprintf("%s.%s", models.BitsCampaignsPubsubTopic, "updates")},
				"{\"type\":\"sponsored-cheermote-update-used\",\"campaign_id\":\"anime-bears-best-bears\",\"used_amount\":20}", mock.Anything)
			pubsubClient.AssertCalled(t, "Publish", ctx,
				[]string{fmt.Sprintf("%s.%s", models.BitsCampaignsPubsubTopic, "updates")},
				"{\"type\":\"sponsored-cheermote-update-used\",\"campaign_id\":\"walrus-for-president\",\"used_amount\":13}", mock.Anything)
		})
	})
}
