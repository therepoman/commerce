package campaign

import (
	"context"

	"code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/campaigns"
)

type Manager interface {
	GetActiveCampaigns(ctx context.Context) []*campaigns.SponsoredCheermoteCampaign
	GetAllCampaigns(ctx context.Context) []*campaigns.SponsoredCheermoteCampaign
}

type manager struct {
	ListGetter ListGetter `inject:""`
}

func NewManager() Manager {
	return &manager{}
}

func (m *manager) GetActiveCampaigns(ctx context.Context) []*campaigns.SponsoredCheermoteCampaign {
	activeCampaigns := make([]*campaigns.SponsoredCheermoteCampaign, 0)
	campaignsList, err := m.ListGetter.GetList(ctx)
	if err != nil {
		return activeCampaigns
	}
	for _, campaign := range campaignsList {
		if campaign != nil && campaign.IsActive() {
			activeCampaigns = append(activeCampaigns, campaign)
		}
	}

	return activeCampaigns
}

func (m *manager) GetAllCampaigns(ctx context.Context) []*campaigns.SponsoredCheermoteCampaign {
	campaignList := make([]*campaigns.SponsoredCheermoteCampaign, 0)
	campaignsList, err := m.ListGetter.GetList(ctx)
	if err != nil {
		return campaignList
	}
	for _, campaign := range campaignsList {
		if campaign != nil {
			campaignList = append(campaignList, campaign)
		}
	}

	return campaignList
}
