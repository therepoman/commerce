package campaign

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	pubclient "code.justin.tv/chat/pubsub-go-pubclient/client"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/actions"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/campaigns"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/lock"
	"code.justin.tv/commerce/payday/metrics"
	"code.justin.tv/commerce/payday/models"
	"github.com/go-redis/redis_rate"
	"golang.org/x/time/rate"
)

const (
	RedisLockKeyPrefix = "bits-campaigns-lock"
	RedisThrottleKey   = "bits-campaign-throttle-v2"

	sendTotalUpdatesThrottleMetric = "SponsoredCheer_Campaign_SendTotalUpdates_Throttled"
)

type PubSubber interface {
	CompleteCampaign(ctx context.Context, campaignID string) error
	SendTotalUpdates(ctx context.Context, sponsoredCheermoteAmounts map[string]models.BitEmoteTotal)
}

type pubSubber struct {
	PubClient     pubclient.PubClient                      `inject:""`
	LockingClient lock.LockingClient                       `inject:""`
	Fetcher       Fetcher                                  `inject:""`
	DAO           campaigns.ISponsoredCheermoteCampaignDao `inject:""`
	Cache         Cache                                    `inject:""`
	ActionCache   actions.Cache                            `inject:""`
	Throttle      *redis_rate.Limiter                      `inject:""`
	Config        *config.Configuration                    `inject:""`
	Statter       metrics.Statter                          `inject:""`
}

func NewPubSubber() PubSubber {
	return &pubSubber{}
}

func (p *pubSubber) CompleteCampaign(ctx context.Context, campaignID string) error {
	campaignLock, err := p.LockingClient.ObtainLock(fmt.Sprintf("%s-%s", RedisLockKeyPrefix, campaignID))
	if err != nil {
		return err
	}
	defer func() {
		err = campaignLock.Unlock()
		if err != nil {
			log.Error(err)
		}
	}()

	campaignRecord, err := p.Fetcher.Fetch(ctx, campaignID)
	if err != nil {
		log.WithError(err).Error("could not lookup campaign that is ending")
		return err
	}

	if campaignRecord == nil {
		log.WithField("campaignID", campaignID).Error("could not lookup campaign that is ending")
		return errors.New("could not find campaign that is ending")
	}

	if !campaignRecord.IsEnabled {
		return nil
	}

	campaignRecord.IsEnabled = false
	err = p.DAO.Update(campaignRecord)

	if err != nil {
		log.WithField("campaignID", campaignID).WithError(err).Error("could not disable campaign on dynamo update")
		return err
	}

	go p.Cache.Del(context.Background(), campaignID)
	go p.ActionCache.Del(context.Background(), campaignRecord.Prefix)

	pubsubMsg := models.SponsoredCheermotesUpdateMessage{
		Type:       models.SponsoredCheermoteCampaignComplete,
		CampaignID: campaignID,
	}

	pubsubMsgJson, err := json.Marshal(pubsubMsg)
	if err != nil {
		msg := "Failed to marshal channel settings update pubsub message"
		log.WithError(err).Error(msg)
		return errors.Notef(err, msg)
	}

	topics := []string{fmt.Sprintf("%s.%s", models.BitsCampaignsPubsubTopic, "updates")}
	err = p.PubClient.Publish(ctx, topics, string(pubsubMsgJson), nil)
	if err != nil {
		msg := "Failed to publish channel settings update to pubsub"
		log.WithError(err).Error(msg)
		return errors.Notef(err, msg)
	}

	return nil
}

func (p *pubSubber) SendTotalUpdates(ctx context.Context, sponsoredCheermoteAmounts map[string]models.BitEmoteTotal) {
	for _, sponsoredCheermoteAmount := range sponsoredCheermoteAmounts {
		if sponsoredCheermoteAmount.Campaign != nil && sponsoredCheermoteAmount.MatchedTotal > 0 {
			publishRate := p.Config.SponsoredCheermoteCampaignPubsubDefaultRateLimitPerSecond
			if sponsoredCheermoteAmount.Campaign.PubSubRateLimitSeconds > 0 {
				publishRate = sponsoredCheermoteAmount.Campaign.PubSubRateLimitSeconds
			}

			_, allowed := p.Throttle.AllowRate(fmt.Sprintf("%s.%s", RedisThrottleKey, sponsoredCheermoteAmount.Campaign.CampaignID), rate.Every(time.Second*time.Duration(publishRate)))

			if allowed {
				err := p.sendPubSubUpdate(ctx, *sponsoredCheermoteAmount.Campaign, sponsoredCheermoteAmount.MatchedTotal)
				if err != nil {
					// We shouldn't bail early if one fails, in case it was a non-systematic failure and we can at least deliver the other ones.
					log.WithError(err).Error("Failed to publish pubsub message of channel total update.")
				}
				p.Statter.Inc(sendTotalUpdatesThrottleMetric, 0)
			} else {
				p.Statter.Inc(sendTotalUpdatesThrottleMetric, 1)
			}
		}
	}
}

func (p *pubSubber) sendPubSubUpdate(ctx context.Context, campaign campaigns.SponsoredCheermoteCampaign, matchedTotal int64) error {
	totalUsedBits := matchedTotal + campaign.UsedBits
	pubsubMessage := models.SponsoredCheermotesUpdateMessage{
		Type:       models.UpdateSponsoredCheermoteUsed,
		CampaignID: campaign.CampaignID,
		UsedAmount: &totalUsedBits,
	}

	pubsubMessageJson, err := json.Marshal(pubsubMessage)
	if err != nil {
		return errors.Notef(err, "Failed to marshal pubsub message for channel updates.")
	}

	topics := []string{fmt.Sprintf("%s.%s", models.BitsCampaignsPubsubTopic, "updates")}
	return p.PubClient.Publish(ctx, topics, string(pubsubMessageJson), nil)
}
