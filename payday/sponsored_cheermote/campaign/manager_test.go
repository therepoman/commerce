package campaign

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/mock"

	"code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/campaigns"
	"code.justin.tv/commerce/payday/errors"
	campaign_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sponsored_cheermote/campaign"
	. "github.com/smartystreets/goconvey/convey"
)

func TestManagerImpl_GetAllCampaigns(t *testing.T) {
	Convey("Given a sponsored cheermote manager", t, func() {
		ctx := context.Background()
		campaignPoller := new(campaign_mock.ListGetter)

		manager := &manager{
			ListGetter: campaignPoller,
		}

		campaign1 := &campaigns.SponsoredCheermoteCampaign{
			CampaignID: "Test1",
			IsEnabled:  true,
			EndTime:    time.Date(3018, 1, 1, 0, 0, 0, 0, time.UTC),
			StartTime:  time.Date(2018, 1, 1, 0, 0, 0, 0, time.UTC),
			Prefix:     "test1",
		}

		campaign2 := &campaigns.SponsoredCheermoteCampaign{
			CampaignID: "Test2",
			IsEnabled:  true,
			EndTime:    time.Date(3018, 1, 1, 0, 0, 0, 0, time.UTC),
			StartTime:  time.Date(2018, 1, 1, 0, 0, 0, 0, time.UTC),
			Prefix:     "test2",
		}

		Convey("When Getter returns an error", func() {
			campaignPoller.On("GetList", mock.Anything).Return([]*campaigns.SponsoredCheermoteCampaign{}, errors.New("WALRUS"))

			Convey("Manager should return no campaigns GetActiveCampaigns call", func() {
				actualCampaigns := manager.GetActiveCampaigns(ctx)
				So(len(actualCampaigns), ShouldEqual, 0)
			})
		})

		Convey("When Getter returns an empty list", func() {
			campaignPoller.On("GetList", mock.Anything).Return([]*campaigns.SponsoredCheermoteCampaign{}, nil)

			Convey("Manager should return no campaigns GetActiveCampaigns call", func() {
				actualCampaigns := manager.GetActiveCampaigns(ctx)
				So(len(actualCampaigns), ShouldEqual, 0)
			})
		})

		Convey("When Getter GetList succeeds", func() {
			Convey("Manager should return 2 campaigns on GetActiveCampaigns call when both are active", func() {
				expectedCampaigns := []*campaigns.SponsoredCheermoteCampaign{campaign1, campaign2}
				campaignPoller.On("GetList", mock.Anything).Return(expectedCampaigns, nil)

				actualCampaigns := manager.GetActiveCampaigns(ctx)
				So(campaignListsEqual(expectedCampaigns, actualCampaigns), ShouldBeTrue)
			})

			Convey("Manager should return 1 campaign on GetActiveCampaigns call when one is active", func() {
				campaign1.IsEnabled = false
				expectedCampaigns := []*campaigns.SponsoredCheermoteCampaign{campaign2}
				campaignsResponse := []*campaigns.SponsoredCheermoteCampaign{campaign1, campaign2}
				campaignPoller.On("GetList", mock.Anything).Return(campaignsResponse, nil)

				actualCampaigns := manager.GetActiveCampaigns(ctx)
				So(campaignListsEqual(expectedCampaigns, actualCampaigns), ShouldBeTrue)
			})

			Convey("Manager should return 0 campaigns on GetActiveCampaigns call when none are active", func() {
				campaign1.IsEnabled = false
				campaign2.IsEnabled = false
				expectedCampaigns := make([]*campaigns.SponsoredCheermoteCampaign, 0)
				campaignsResponse := []*campaigns.SponsoredCheermoteCampaign{campaign1, campaign2}
				campaignPoller.On("GetList", mock.Anything).Return(campaignsResponse, nil)

				actualCampaigns := manager.GetActiveCampaigns(ctx)
				So(campaignListsEqual(expectedCampaigns, actualCampaigns), ShouldBeTrue)
			})
		})
	})
}

func campaignListsEqual(cl1, cl2 []*campaigns.SponsoredCheermoteCampaign) bool {
	if len(cl1) != len(cl2) {
		return false
	}

	for i, c1 := range cl1 {
		c2 := cl2[i]
		if c1 == nil || c2 == nil {
			if c1 == c2 {
				continue
			}
			return false
		}

		if !c1.Equals(c2) {
			return false
		}
	}
	return true
}
