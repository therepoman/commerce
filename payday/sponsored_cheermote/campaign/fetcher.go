package campaign

import (
	"context"

	"code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/campaigns"
)

const (
	SponsoredCheermoteHackFloor      = 750000
	SponsoredCheermoteHackFloorStart = 1000000
	SponsoredCheermoteHackStop       = 20000000
)

type Fetcher interface {
	Fetch(ctx context.Context, campaignID string) (*campaigns.SponsoredCheermoteCampaign, error)
	FetchBatch(ctx context.Context, campaignIDs []string) ([]*campaigns.SponsoredCheermoteCampaign, error)
}

type fetcher struct {
	Dao   campaigns.ISponsoredCheermoteCampaignDao `inject:""`
	Cache Cache                                    `inject:""`
}

func NewFetcher() Fetcher {
	return &fetcher{}
}

func (f *fetcher) Fetch(ctx context.Context, campaignID string) (*campaigns.SponsoredCheermoteCampaign, error) {
	cachedRecord := f.Cache.Get(ctx, campaignID)
	if cachedRecord != nil {
		return cachedRecord, nil
	}

	dynamoRecord, err := f.Dao.Get(campaignID)
	if err != nil || dynamoRecord == nil {
		return nil, err
	}

	// this function hacks in the obscuration of the campaign total values for the amazon prime hunter campaign
	mobileBarHack(dynamoRecord)
	f.Cache.Set(ctx, campaignID, *dynamoRecord)
	return dynamoRecord, nil
}

func (f *fetcher) FetchBatch(ctx context.Context, campaignIDs []string) ([]*campaigns.SponsoredCheermoteCampaign, error) {
	records := make([]*campaigns.SponsoredCheermoteCampaign, 0)
	campaignIDsToFetch := make([]string, 0)
	for _, campaignID := range campaignIDs {
		cachedDynamoRecord := f.Cache.Get(ctx, campaignID)
		if cachedDynamoRecord != nil {
			// this function hacks in the obscuration of the campaign total values for the amazon prime hunter campaign
			mobileBarHack(cachedDynamoRecord)
			records = append(records, cachedDynamoRecord)
		} else {
			campaignIDsToFetch = append(campaignIDsToFetch, campaignID)
		}
	}

	if len(campaignIDsToFetch) > 0 {
		dynamoCampaigns, err := f.Dao.GetBatch(campaignIDsToFetch)
		if err != nil {
			return nil, err
		}

		for _, dynamoCampaign := range dynamoCampaigns {
			if dynamoCampaign != nil {
				// this function hacks in the obscuration of the campaign total values for the amazon prime hunter campaign
				mobileBarHack(dynamoCampaign)
				f.Cache.Set(ctx, dynamoCampaign.CampaignID, *dynamoCampaign)
				records = append(records, dynamoCampaign)
			}
		}
	}

	return records, nil
}

// This function is intended to give mobile a perpetual 75% done progress bar after the bar hits a certain threshold.
// This is done as a business request to hide the bar on desktop (to hide business values) but we still need to provide
// Mobile with a valid experience.
// TODO: either finish adding this as a configurable parameter to campaigns or remove this hack entirely.
func mobileBarHack(campaign *campaigns.SponsoredCheermoteCampaign) {
	if campaign.CampaignID == "xfinity-qa-2019" || campaign.CampaignID == "amazon-hunters-2020" {
		if campaign.UsedBits < SponsoredCheermoteHackFloor {
			campaign.TotalBits = SponsoredCheermoteHackFloorStart
		} else if campaign.UsedBits >= SponsoredCheermoteHackFloor {
			campaign.TotalBits = int64(float64(campaign.UsedBits) / 0.75)
		}
		if campaign.TotalBits >= SponsoredCheermoteHackStop {
			campaign.TotalBits = SponsoredCheermoteHackStop
		}
	}
}
