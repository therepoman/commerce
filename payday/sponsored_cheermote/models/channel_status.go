package models

import "time"

type SponsoredCheermoteChannelStatus struct {
	CampaignID                string            `json:"campaign_id"`
	ChannelID                 string            `json:"channel_id"`
	BrandName                 string            `json:"brand_name"`
	BrandImageURL             string            `json:"brand_image_url"`
	SponsoredAmountThresholds map[int64]float64 `json:"sponsored_amount_thresholds"`
	OptedIn                   bool              `json:"opted_in"`
	StartTime                 time.Time         `json:"start_time"`
	EndTime                   time.Time         `json:"end_time"`
}
