package models

import (
	"code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/campaigns"
	"code.justin.tv/commerce/payday/models"
)

type SponsoredCheermoteData struct {
	CheeredAmount models.Bits
	MatchedAmount models.Bits
	Campaign      *campaigns.SponsoredCheermoteCampaign
}

type SponsoredCheermoteAmounts map[string]SponsoredCheermoteData
