package eligible_channel

import (
	"context"
	"testing"

	"github.com/stretchr/testify/mock"

	"code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/eligible_channels"
	"code.justin.tv/commerce/payday/errors"
	eligible_channels_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/eligible_channels"
	eligible_channel_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sponsored_cheermote/eligible_channel"
	. "github.com/smartystreets/goconvey/convey"
)

func TestFetcher_Fetch(t *testing.T) {
	Convey("given a channel eligibility fetcher", t, func() {
		ctx := context.Background()
		dao := new(eligible_channels_mock.ISponsoredCheermoteEligibleChannelsDao)
		cache := new(eligible_channel_mock.Cache)

		f := &fetcher{
			Dao:   dao,
			Cache: cache,
		}

		channelID := "333333"
		campaignID := "tonkotsu-ramen-festival"

		channelEligibilityRecord := &eligible_channels.SponsoredCheermoteEligibleChannel{ChannelID: channelID, CampaignID: campaignID}

		Convey("when cache returns a record", func() {
			cache.On("Get", mock.Anything, channelID, campaignID).Return(channelEligibilityRecord, true)
			actual, err := f.Fetch(ctx, channelID, campaignID)
			So(err, ShouldBeNil)
			So(actual, ShouldEqual, channelEligibilityRecord)
		})

		Convey("when cache does not returns a record", func() {
			cache.On("Get", mock.Anything, channelID, campaignID).Return(nil, false)

			Convey("when dao errors", func() {
				dao.On("Get", channelID, campaignID).Return(nil, errors.New("you ate mexican food again"))
				actual, err := f.Fetch(ctx, channelID, campaignID)
				So(actual, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})

			Convey("when dao returns nil", func() {
				dao.On("Get", channelID, campaignID).Return(nil, nil)
				cache.On("Set", mock.Anything, channelID, campaignID, (*eligible_channels.SponsoredCheermoteEligibleChannel)(nil)).Return(nil)
				actual, err := f.Fetch(ctx, channelID, campaignID)
				So(actual, ShouldBeNil)
				So(err, ShouldBeNil)
			})

			Convey("when dao returns a record", func() {
				dao.On("Get", channelID, campaignID).Return(channelEligibilityRecord, nil)
				cache.On("Set", mock.Anything, channelID, campaignID, channelEligibilityRecord).Return(nil)
				actual, err := f.Fetch(ctx, channelID, campaignID)
				So(actual, ShouldEqual, channelEligibilityRecord)
				So(err, ShouldBeNil)
			})
		})
	})
}
