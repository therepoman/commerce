package eligible_channel

import (
	"context"
	"encoding/json"
	"fmt"
	"testing"
	"time"

	"code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/eligible_channels"
	"code.justin.tv/commerce/payday/errors"
	redis_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/redis"
	"github.com/go-redis/redis"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestCache_Get(t *testing.T) {
	Convey("Given an eligibility cache", t, func() {
		ctx := context.Background()
		redisClient := new(redis_mock.Client)

		cache := &eligibleChannelCache{
			RedisClient: redisClient,
		}

		campaignID := "pho-cyclo-cafe"
		channelID := "50505050505050"
		cacheKey := fmt.Sprintf(sponsoredEligibleChannelKeyFormat, channelID, campaignID)

		Convey("Redis return an error", func() {
			resp := redis.NewStringResult("", errors.New("looks like you went to mexican restaurant again"))
			redisClient.On("Get", mock.Anything, cacheKey).Return(resp)
			result, exists := cache.Get(ctx, channelID, campaignID)
			So(result, ShouldBeNil)
			So(exists, ShouldBeFalse)
		})

		Convey("Redis return an error for redis.Nil", func() {
			resp := redis.NewStringResult("", redis.Nil)
			redisClient.On("Get", mock.Anything, cacheKey).Return(resp)
			result, exists := cache.Get(ctx, channelID, campaignID)
			So(result, ShouldBeNil)
			So(exists, ShouldBeFalse)
		})

		Convey("Redis returns empty value", func() {
			resp := redis.NewStringResult("", nil)
			redisClient.On("Get", mock.Anything, cacheKey).Return(resp)
			result, exists := cache.Get(ctx, channelID, campaignID)
			So(result, ShouldBeNil)
			So(exists, ShouldBeFalse)
		})

		Convey("Redis returns a record", func() {
			eligibleChannel := eligible_channels.SponsoredCheermoteEligibleChannel{
				ChannelID:   channelID,
				CampaignID:  campaignID,
				LastUpdated: time.Now(),
			}

			eligibleChannelJson, err := json.Marshal(eligibleChannel)
			if err != nil {
				t.Fail()
			}

			resp := redis.NewStringResult(string(eligibleChannelJson), nil)
			redisClient.On("Get", mock.Anything, cacheKey).Return(resp)
			result, exists := cache.Get(ctx, channelID, campaignID)
			So(result, ShouldNotBeNil)
			So(exists, ShouldBeTrue)
		})
	})
}
