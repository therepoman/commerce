package eligible_channel

import (
	"context"
	"testing"

	"github.com/stretchr/testify/mock"

	"code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/eligible_channels"
	"code.justin.tv/commerce/payday/errors"
	eligible_channel_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sponsored_cheermote/eligible_channel"
	. "github.com/smartystreets/goconvey/convey"
)

func TestGetter_Get(t *testing.T) {
	Convey("given a getter", t, func() {
		ctx := context.Background()
		eligibleChannelFetcher := new(eligible_channel_mock.Fetcher)

		getter := &getter{
			Fetcher: eligibleChannelFetcher,
		}

		channelID := "123456789"
		campaignID := "mexicanfood"

		Convey("when Fetcher errors", func() {
			eligibleChannelFetcher.On("Fetch", mock.Anything, channelID, campaignID).Return(nil, errors.New("you like mexican food. error."))

			Convey("should return an error", func() {
				_, err := getter.Get(ctx, channelID, campaignID)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("When Fetcher does not find an eligibility", func() {
			eligibleChannelFetcher.On("Fetch", mock.Anything, channelID, campaignID).Return(nil, nil)

			Convey("should not return an error and tell you it's not eligible", func() {
				eligible, err := getter.Get(ctx, channelID, campaignID)
				So(err, ShouldBeNil)
				So(eligible, ShouldBeFalse)
			})
		})

		Convey("When the channel is eligible", func() {
			eligibleChannelFetcher.On("Fetch", mock.Anything, channelID, campaignID).Return(&eligible_channels.SponsoredCheermoteEligibleChannel{ChannelID: channelID, CampaignID: campaignID}, nil)

			Convey("shouldn't return dynamo record of the channel's eligibility", func() {
				eligible, err := getter.Get(ctx, channelID, campaignID)
				So(err, ShouldBeNil)
				So(eligible, ShouldBeTrue)
			})
		})
	})
}
