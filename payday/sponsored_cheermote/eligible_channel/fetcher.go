package eligible_channel

import (
	"context"

	"code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/eligible_channels"
)

type Fetcher interface {
	Fetch(ctx context.Context, channelID, campaignID string) (*eligible_channels.SponsoredCheermoteEligibleChannel, error)
}

type fetcher struct {
	Dao   eligible_channels.ISponsoredCheermoteEligibleChannelsDao `inject:""`
	Cache Cache                                                    `inject:""`
}

func NewFetcher() Fetcher {
	return &fetcher{}
}

func (f *fetcher) Fetch(ctx context.Context, channelID, campaignID string) (*eligible_channels.SponsoredCheermoteEligibleChannel, error) {
	cachedEligibilityRecord, existsInCache := f.Cache.Get(ctx, channelID, campaignID)
	if existsInCache {
		return cachedEligibilityRecord, nil
	} else {
		dynamoRecord, err := f.Dao.Get(channelID, campaignID)
		if err != nil {
			return nil, err
		}

		defer f.Cache.Set(ctx, channelID, campaignID, dynamoRecord)
		return dynamoRecord, nil
	}
}
