package eligible_channel

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/clients/redis"
	"code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/eligible_channels"
	"code.justin.tv/commerce/payday/hystrix"
	cmds "code.justin.tv/commerce/payday/hystrix"
	"code.justin.tv/commerce/payday/utils/strings"
	go_redis "github.com/go-redis/redis"
)

const (
	sponsoredEligibleChannelKeyFormat  = "sponsored-eligible-channels-v2-%s-%s"
	sponsoredEligibleChannelCacheTTL   = time.Minute * 5
	sponsoredEligibileChannelNilString = "<NOT_IN_DB>"
)

type Cache interface {
	Get(ctx context.Context, channelID, campaignID string) (*eligible_channels.SponsoredCheermoteEligibleChannel, bool)
	Set(ctx context.Context, channelID, campaignID string, eligibleChannel *eligible_channels.SponsoredCheermoteEligibleChannel)
	Del(ctx context.Context, channelID, campaignID string)
}

type eligibleChannelCache struct {
	RedisClient redis.Client `inject:"redisClient"`
}

func NewCache() Cache {
	return &eligibleChannelCache{}
}

func (c *eligibleChannelCache) Get(ctx context.Context, channelID, campaignID string) (*eligible_channels.SponsoredCheermoteEligibleChannel, bool) {
	var eligibleChannelCacheValue string
	err := hystrix.Do(cmds.CampaignsEligibleCacheGet, func() error {
		var err error
		eligibleChannelCacheValue, err = c.RedisClient.Get(ctx, fmt.Sprintf(sponsoredEligibleChannelKeyFormat, channelID, campaignID)).Result()
		if err != nil && err != go_redis.Nil {
			log.WithFields(log.Fields{
				"ChannelID":  channelID,
				"CampaignID": campaignID,
			}).WithError(err).Error("EligibleChannelCache: Error getting eligible channel from cache")
		}
		return nil
	}, nil)
	if err != nil {
		log.WithError(err).Warn("hystrix error returned while fetching channel eligibility for campaign from cache")
	}

	if strings.Blank(eligibleChannelCacheValue) {
		return nil, false
	}

	if eligibleChannelCacheValue == sponsoredEligibileChannelNilString {
		return nil, true
	}

	var eligibleChannel eligible_channels.SponsoredCheermoteEligibleChannel
	err = json.Unmarshal([]byte(eligibleChannelCacheValue), &eligibleChannel)
	if err != nil {
		c.Del(ctx, channelID, campaignID)
		log.WithFields(log.Fields{
			"ChannelID":  channelID,
			"CampaignID": campaignID,
		}).WithError(err).Error("EligibleChannelCache: Error unmarshalling eligible channel")
		return nil, false
	}

	return &eligibleChannel, true
}

func (c *eligibleChannelCache) Set(ctx context.Context, channelID, campaignID string, eligibleChannel *eligible_channels.SponsoredCheermoteEligibleChannel) {
	var value string
	if eligibleChannel != nil {
		eligibleChannelJson, err := json.Marshal(*eligibleChannel)
		if err != nil {
			log.WithFields(log.Fields{
				"ChannelID":  channelID,
				"CampaignID": campaignID,
			}).WithError(err).Error("EligibleChannelCache: Error marshaling eligible channel")
			return
		}

		value = string(eligibleChannelJson)
	} else {
		value = sponsoredEligibileChannelNilString
	}
	err := hystrix.Do(cmds.CampaignsEligibleCacheSet, func() error {
		err := c.RedisClient.Set(ctx, fmt.Sprintf(sponsoredEligibleChannelKeyFormat, channelID, campaignID), value, sponsoredEligibleChannelCacheTTL).Err()
		if err != nil {
			log.WithFields(log.Fields{
				"ChannelID":  channelID,
				"CampaignID": campaignID,
			}).WithError(err).Error("EligibleChannelCache: Error setting eligible channel in cache as nil")
		}
		return nil
	}, nil)
	if err != nil {
		log.WithError(err).Warn("hystrix error while setting channel eligibility for campaign in cache")
	}
}

func (c *eligibleChannelCache) Del(ctx context.Context, channelID, campaignID string) {
	err := hystrix.Do(cmds.CampaignsEligibleCacheDel, func() error {
		err := c.RedisClient.Del(ctx, fmt.Sprintf(sponsoredEligibleChannelKeyFormat, channelID, campaignID)).Err()
		if err != nil {
			log.WithFields(log.Fields{
				"ChannelID":  channelID,
				"CampaignID": campaignID,
			}).WithError(err).Error("EligibleChannelCache: Error deleting eligible channel from cache")
		}
		return nil
	}, nil)
	if err != nil {
		log.WithError(err).Warn("hystrix error while deleting channel eligibility for campaign from cache")
	}
}
