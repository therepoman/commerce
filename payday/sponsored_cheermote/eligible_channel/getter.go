package eligible_channel

import "context"

type Getter interface {
	Get(ctx context.Context, channelID, campaignID string) (bool, error)
}

type getter struct {
	Fetcher Fetcher `inject:""`
}

func NewGetter() Getter {
	return &getter{}
}

func (g *getter) Get(ctx context.Context, channelID, campaignID string) (bool, error) {
	eligibleChannel, err := g.Fetcher.Fetch(ctx, channelID, campaignID)
	return eligibleChannel != nil, err
}
