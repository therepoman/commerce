package actions

import (
	"context"
	"testing"

	dynamo_actions "code.justin.tv/commerce/payday/dynamo/actions"
	"code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/campaigns"
	actions_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/actions"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestGetterImpl_Get(t *testing.T) {
	Convey("Given a sponsored cheermote action Getter", t, func() {
		ctx := context.Background()
		actionsFetcher := new(actions_mock.Fetcher)

		getter := &getter{
			Fetcher: actionsFetcher,
		}

		Convey("When the campaigns input is nil", func() {
			var campaignsInput *campaigns.SponsoredCheermoteCampaign

			Convey("Getter should succeed without calling dynamo", func() {
				actionOutput, err := getter.Get(ctx, campaignsInput)
				So(err, ShouldBeNil)
				So(actionOutput, ShouldBeNil)
				actionsFetcher.AssertNotCalled(t, "Fetch", mock.Anything, mock.Anything)
			})
		})

		Convey("When the campaigns input is an array of valid campaigns", func() {
			campaignsInput := &campaigns.SponsoredCheermoteCampaign{CampaignID: "Test1", Prefix: "p1"}

			Convey("When ActionDao GetBatch errors", func() {
				actionsFetcher.On("Fetch", mock.Anything, "p1").Return(nil, errors.New("test error"))

				Convey("Getter should error", func() {
					_, err := getter.Get(ctx, campaignsInput)
					So(err, ShouldNotBeNil)
				})
			})

			Convey("When ActionDao GetBatch succeeds", func() {
				dynamoActionsOutput := &dynamo_actions.Action{
					Prefix: "p1",
				}
				actionsFetcher.On("Fetch", mock.Anything, "p1").Return(dynamoActionsOutput, nil)

				Convey("Getter should return the actions", func() {
					actionOutput, err := getter.Get(ctx, campaignsInput)
					So(err, ShouldBeNil)
					So(actionOutput, ShouldEqual, dynamoActionsOutput)
				})
			})
		})
	})
}

func TestGetterImpl_GetBatch(t *testing.T) {
	Convey("Given a sponsored cheermote action Getter", t, func() {
		ctx := context.Background()
		actionsFetcher := new(actions_mock.Fetcher)

		getter := &getter{
			Fetcher: actionsFetcher,
		}

		var campaignsInput []*campaigns.SponsoredCheermoteCampaign
		Convey("When the campaigns input is an empty array", func() {
			campaignsInput = []*campaigns.SponsoredCheermoteCampaign{}

			Convey("Getter should succeed without calling dynamo", func() {
				actionsOutput, err := getter.GetBatch(ctx, campaignsInput)
				So(err, ShouldBeNil)
				So(len(actionsOutput), ShouldEqual, 0)
				actionsFetcher.AssertNotCalled(t, "FetchBatch", mock.Anything)
			})
		})

		Convey("When the campaigns input is an array of nil values", func() {
			campaignsInput = []*campaigns.SponsoredCheermoteCampaign{nil, nil}

			Convey("Getter should succeed without calling dynamo", func() {
				actionsOutput, err := getter.GetBatch(ctx, campaignsInput)
				So(err, ShouldBeNil)
				So(len(actionsOutput), ShouldEqual, 0)
				actionsFetcher.AssertNotCalled(t, "FetchBatch", mock.Anything, mock.Anything)
			})
		})

		Convey("When the campaigns input is an array of valid campaigns", func() {
			campaignsInput = []*campaigns.SponsoredCheermoteCampaign{{CampaignID: "Test1", Prefix: "p1"}, {CampaignID: "Test2", Prefix: "p2"}}

			Convey("When ActionDao GetBatch errors", func() {
				actionsFetcher.On("FetchBatch", mock.Anything, []string{"p1", "p2"}).Return(nil, errors.New("test error"))

				Convey("Getter should error", func() {
					_, err := getter.GetBatch(ctx, campaignsInput)
					So(err, ShouldNotBeNil)
				})
			})

			Convey("When ActionDao GetBatch succeeds", func() {
				dynamoActionsOutput := map[string]*dynamo_actions.Action{
					"p1": {Prefix: "p1"},
					"p2": {Prefix: "p2"},
				}
				actionsFetcher.On("FetchBatch", mock.Anything, []string{"p1", "p2"}).Return(dynamoActionsOutput, nil)

				Convey("Getter should return the actions", func() {
					actionsOutput, err := getter.GetBatch(ctx, campaignsInput)
					So(err, ShouldBeNil)
					So(actionListsEqual(dynamoActionsOutput, actionsOutput), ShouldBeTrue)
				})
			})
		})
	})
}

func actionListsEqual(am1, am2 map[string]*dynamo_actions.Action) bool {
	if len(am1) != len(am2) {
		return false
	}

	for _, a1 := range am1 {
		a2 := am2[a1.Prefix]
		if a1 == nil || a2 == nil {
			if a1 == a2 {
				continue
			}
			return false
		}

		if !a1.Equals(a2) {
			return false
		}
	}
	return true
}
