package actions

import (
	"context"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/actions"
	"code.justin.tv/commerce/payday/actions/constants"
	dynamo_action "code.justin.tv/commerce/payday/dynamo/actions"
	"code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/campaigns"
	"code.justin.tv/commerce/payday/errors"
)

type Getter interface {
	Get(ctx context.Context, campaign *campaigns.SponsoredCheermoteCampaign) (*dynamo_action.Action, error)
	GetBatch(ctx context.Context, campaigns []*campaigns.SponsoredCheermoteCampaign) (map[string]*dynamo_action.Action, error)
	GetBonus(ctx context.Context) (*dynamo_action.Action, error)
}

type getter struct {
	Fetcher actions.Fetcher `inject:""`
}

func NewGetter() Getter {
	return &getter{}
}

func (g *getter) Get(ctx context.Context, campaignRecord *campaigns.SponsoredCheermoteCampaign) (*dynamo_action.Action, error) {
	if campaignRecord == nil {
		return nil, nil
	}

	sponsoredAction, err := g.Fetcher.Fetch(ctx, campaignRecord.Prefix)

	if err != nil {
		log.WithField("campaignID", campaignRecord.CampaignID).WithError(err).Error("could not fetch action record for given campaign")
		return nil, err
	}

	return sponsoredAction, nil
}

func (g *getter) GetBatch(ctx context.Context, campaigns []*campaigns.SponsoredCheermoteCampaign) (map[string]*dynamo_action.Action, error) {
	prefixes := make([]string, 0)
	for _, campaign := range campaigns {
		if campaign == nil {
			continue
		}

		prefixes = append(prefixes, campaign.Prefix)
	}

	if len(prefixes) <= 0 {
		return map[string]*dynamo_action.Action{}, nil
	}

	sponsoredActions, err := g.Fetcher.FetchBatch(ctx, prefixes)
	if err != nil {
		log.WithError(err).Error("could not fetch action records for campaigns")
		return nil, err
	}

	return sponsoredActions, nil
}

func (g *getter) GetBonus(ctx context.Context) (*dynamo_action.Action, error) {
	bonusAction, err := g.Fetcher.Fetch(ctx, constants.BonusCheerPrefix)

	if err != nil {
		return nil, err
	}

	if bonusAction == nil {
		return nil, errors.New("Couldn't find bonus cheermote")
	}

	return bonusAction, nil
}
