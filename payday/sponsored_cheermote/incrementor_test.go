package sponsored_cheermote

import (
	"context"
	"testing"
	"time"

	"code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/campaigns"
	campaigns_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/campaigns"
	user_campaign_statuses_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/user_campaign_statuses"
	campaign_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sponsored_cheermote/campaign"
	user_campaign_status_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sponsored_cheermote/user_campaign_status"
	"code.justin.tv/commerce/payday/models"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestIncrementor_IncrementUsedBits(t *testing.T) {
	Convey("Given an sponsored cheermote incrementor", t, func() {
		ctx := context.Background()
		campaignDao := new(campaigns_mock.ISponsoredCheermoteCampaignDao)
		userDao := new(user_campaign_statuses_mock.ISponsoredCheermoteUserStatusesDao)
		cache := new(campaign_mock.Cache)
		userCache := new(user_campaign_status_mock.Cache)

		incrementor := &incrementor{
			CampaignDAO: campaignDao,
			UserDAO:     userDao,
			Cache:       cache,
			UserCache:   userCache,
		}

		campaignDao.On("IncrementBitsUsed", mock.Anything, mock.Anything).Return(nil)
		userDao.On("IncrementBitsUsed", mock.Anything, mock.Anything, mock.Anything).Return(nil)
		cache.On("Del", mock.Anything, mock.Anything).Return(nil)
		userCache.On("Del", mock.Anything, mock.Anything, mock.Anything).Return(nil)

		Convey("with a nil campaign", func() {
			Convey("we should not increment any campaign used amount", func() {
				incrementor.IncrementUsedBits(ctx, "walrus-user", map[string]models.BitEmoteTotal{
					"walrus": {
						MatchedTotal: 100,
						Campaign:     nil,
					},
				})

				waitForAsync()

				campaignDao.AssertNotCalled(t, "IncrementBitsUsed", nil, int64(100))
				cache.AssertNotCalled(t, "Del", mock.Anything, nil)
			})
		})

		Convey("with a sponsored campaign that matches the one in the cheer", func() {
			campaign := &campaigns.SponsoredCheermoteCampaign{
				CampaignID:           "SaveTheWalrus",
				Prefix:               "walrus",
				StartTime:            time.Date(2018, 1, 1, 0, 0, 0, 0, time.UTC),
				EndTime:              time.Date(3018, 1, 1, 0, 0, 0, 0, time.UTC),
				TotalBits:            10000,
				UsedBits:             1000,
				IsEnabled:            true,
				MinBitsToBeSponsored: 100,
			}

			Convey("when incrementing", func() {
				userID := "walrus-user"

				incrementor.IncrementUsedBits(ctx, userID, map[string]models.BitEmoteTotal{
					"walrus": {
						MatchedTotal: 100,
						Campaign:     campaign,
					},
				})

				waitForAsync()

				Convey("it should call to update the total used for campaign", func() {
					campaignDao.AssertCalled(t, "IncrementBitsUsed", campaign, int64(100))
					cache.AssertCalled(t, "Del", mock.Anything, campaign.CampaignID)
				})

				Convey("it should call to update the total used for user", func() {
					userDao.AssertCalled(t, "IncrementBitsUsed", campaign.CampaignID, userID, int64(100))
					userCache.AssertCalled(t, "Del", mock.Anything, campaign.CampaignID, userID)
				})
			})
		})
	})
}

func waitForAsync() {
	time.Sleep(50 * time.Millisecond)
}
