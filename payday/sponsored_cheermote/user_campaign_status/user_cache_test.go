package user_campaign_status

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"testing"

	"github.com/stretchr/testify/mock"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/user_campaign_statuses"
	redis_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/redis"
	"github.com/go-redis/redis"
	. "github.com/smartystreets/goconvey/convey"
)

func TestUserCacheImpl_Get(t *testing.T) {
	Convey("Given a user's status", t, func() {
		ctx := context.Background()
		redisClient := new(redis_mock.Client)

		cache := &userCache{
			RedisClient: redisClient,
		}

		campaignID := "brands-are-awesome"
		userID := "brand-accepting-user"
		cacheKey := fmt.Sprintf(sponsoredCampaignUserStatusKeyFormat, campaignID, userID)

		status := user_campaign_statuses.SponsoredCheermoteUserStatus{
			CampaignID: campaignID,
			UserID:     userID,
			UsedBits:   int64(100),
		}

		statusBytes, err := json.Marshal(status)
		if err != nil {
			log.WithError(err).Error("Error marshaling user campaign status")
			t.Fail()
		}

		Convey("Given a record exists in the cache", func() {
			getResp := redis.NewStringResult(string(statusBytes), nil)
			redisClient.On("Get", mock.Anything, cacheKey).Return(getResp)

			Convey("It should return the correct record", func() {
				actual, exists := cache.Get(ctx, campaignID, userID)
				So(actual, ShouldNotBeNil)
				So(actual.CampaignID, ShouldEqual, campaignID)
				So(actual.UserID, ShouldEqual, userID)
				So(actual.UsedBits, ShouldEqual, int64(100))
				So(exists, ShouldBeTrue)
			})
		})

		Convey("Given there is an error fetching the record from cache", func() {
			getResp := redis.NewStringResult("", errors.New("BORING ERROR"))
			redisClient.On("Get", mock.Anything, cacheKey).Return(getResp)

			Convey("It should return nil", func() {
				actual, exists := cache.Get(ctx, campaignID, userID)
				So(actual, ShouldBeNil)
				So(exists, ShouldBeFalse)
			})
		})

		Convey("Given the record does not exist in the cache", func() {
			getResp := redis.NewStringResult("", nil)
			redisClient.On("Get", mock.Anything, cacheKey).Return(getResp)

			Convey("It should return nil", func() {
				actual, exists := cache.Get(ctx, campaignID, userID)
				So(actual, ShouldBeNil)
				So(exists, ShouldBeFalse)
			})
		})

		Convey("Given the record is not another type", func() {
			getResp := redis.NewStringResult(string(statusBytes[5:]), nil)
			redisClient.On("Get", mock.Anything, cacheKey).Return(getResp)
			delResp := redis.NewIntCmd("del", nil)
			redisClient.On("Del", mock.Anything, cacheKey).Return(delResp)

			Convey("It should return nil and delete the record", func() {
				actual, exists := cache.Get(ctx, campaignID, userID)
				So(exists, ShouldBeFalse)
				So(actual, ShouldBeNil)
				redisClient.AssertCalled(t, "Del", ctx, cacheKey)
			})
		})
	})
}
