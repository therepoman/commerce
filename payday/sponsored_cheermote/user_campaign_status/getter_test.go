package user_campaign_status

import (
	"context"
	"testing"

	"github.com/stretchr/testify/mock"

	"code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/user_campaign_statuses"
	user_campaign_status_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sponsored_cheermote/user_campaign_status"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
)

func TestManager_GetUserAmountContributed(t *testing.T) {
	Convey("given a getter", t, func() {
		ctx := context.Background()
		userStatusFetcher := new(user_campaign_status_mock.Fetcher)

		getter := &getter{
			Fetcher: userStatusFetcher,
		}

		userID := "123123123"
		campaignID := "walrus"

		Convey("when the user status fetcher fails", func() {
			userStatusFetcher.On("Fetch", mock.Anything, campaignID, userID).Return(nil, errors.New("WALRUS STRIKE"))

			_, err := getter.Get(ctx, campaignID, userID)

			Convey("should return an error", func() {
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when the user status fetcher does not find a record", func() {
			userStatusFetcher.On("Fetch", mock.Anything, campaignID, userID).Return(nil, nil)

			amount, err := getter.Get(ctx, campaignID, userID)

			Convey("should return an error", func() {
				So(err, ShouldBeNil)
				So(amount, ShouldEqual, int64(0))
			})
		})

		Convey("When the user status fetcher succeeds", func() {
			userStatusFetcher.On("Fetch", mock.Anything, campaignID, userID).Return(&user_campaign_statuses.SponsoredCheermoteUserStatus{
				UserID:     userID,
				CampaignID: campaignID,
				UsedBits:   int64(322),
			}, nil)

			amount, err := getter.Get(ctx, campaignID, userID)

			Convey("should return an error", func() {
				So(err, ShouldBeNil)
				So(amount, ShouldEqual, int64(322))
			})
		})
	})
}
