package user_campaign_status

import (
	"context"
	"testing"

	"code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/user_campaign_statuses"
	user_campaign_statuses_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/user_campaign_statuses"
	user_campaign_status_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sponsored_cheermote/user_campaign_status"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestFetcher_Fetch(t *testing.T) {
	Convey("given a user fetcher", t, func() {
		ctx := context.Background()
		dao := new(user_campaign_statuses_mock.ISponsoredCheermoteUserStatusesDao)
		cache := new(user_campaign_status_mock.Cache)

		f := &fetcher{
			Dao:   dao,
			Cache: cache,
		}

		userID := "123123123123"
		campaignID := "save_the_walrus"

		userCampaignStatus := &user_campaign_statuses.SponsoredCheermoteUserStatus{
			CampaignID: campaignID,
			UserID:     userID,
			UsedBits:   322,
		}

		Convey("Given a list of campaign statuses that are in the cache", func() {
			cache.On("Get", mock.Anything, campaignID, userID).Return(userCampaignStatus, true)

			Convey("When we fetch the list of records", func() {
				actual, err := f.Fetch(ctx, campaignID, userID)

				So(err, ShouldBeNil)
				So(actual, ShouldEqual, userCampaignStatus)

				Convey("We only fetch from the cache and do not hit Dynamo", func() {
					dao.AssertNotCalled(t, "Get", mock.Anything)
					cache.AssertCalled(t, "Get", mock.Anything, campaignID, userID)
				})
			})
		})

		Convey("Given a list of campaigns both in the cache and not in the cache", func() {
			cache.On("Get", mock.Anything, campaignID, userID).Return(nil, false)

			cache.On("Set", mock.Anything, campaignID, userID, userCampaignStatus).Return(nil)

			dao.On("Get", userID, campaignID).Return(userCampaignStatus, nil)

			Convey("When we fetch the list of records", func() {
				actual, err := f.Fetch(ctx, campaignID, userID)

				So(err, ShouldBeNil)
				So(actual, ShouldEqual, userCampaignStatus)

				Convey("We fetch from both the cache and Dynamo", func() {
					cache.AssertCalled(t, "Get", mock.Anything, campaignID, userID)
					dao.AssertCalled(t, "Get", userID, campaignID)
				})

				Convey("We set the new record from Dynamo in the cache", func() {
					cache.AssertCalled(t, "Set", mock.Anything, campaignID, userID, userCampaignStatus)
				})
			})
		})
	})
}
