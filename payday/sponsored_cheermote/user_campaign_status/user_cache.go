package user_campaign_status

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/clients/redis"
	"code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/user_campaign_statuses"
	"code.justin.tv/commerce/payday/hystrix"
	cmds "code.justin.tv/commerce/payday/hystrix"
	"code.justin.tv/commerce/payday/utils/strings"
	go_redis "github.com/go-redis/redis"
)

const (
	sponsoredCampaignUserStatusKeyFormat      = "sponsored-campaigns-user-status-v2-%s-%s"
	sponsoredCampaignUserStatusCacheTTL       = time.Minute * 5
	sponsoredCampaignUserStatusCacheNilString = "<NOT_IN_DB>"
)

type Cache interface {
	Get(ctx context.Context, campaignID, userID string) (*user_campaign_statuses.SponsoredCheermoteUserStatus, bool)
	Set(ctx context.Context, campaignID, userID string, status *user_campaign_statuses.SponsoredCheermoteUserStatus)
	Del(ctx context.Context, campaignID, userID string)
}

type userCache struct {
	RedisClient redis.Client `inject:"redisClient"`
}

func NewCache() Cache {
	return &userCache{}
}

func (c *userCache) Get(ctx context.Context, campaignID, userID string) (*user_campaign_statuses.SponsoredCheermoteUserStatus, bool) {
	var campaignCacheValue string
	err := hystrix.Do(cmds.CampaignsUserCacheGet, func() error {
		var err error
		campaignCacheValue, err = c.RedisClient.Get(ctx, fmt.Sprintf(sponsoredCampaignUserStatusKeyFormat, campaignID, userID)).Result()
		if err != nil && err != go_redis.Nil {
			log.WithError(err).Error("Error getting user status out of cache")
		}
		return nil
	}, nil)
	if err != nil {
		log.WithError(err).Warn("hystrix error while fetching campaign user status from cache")
	}

	if strings.Blank(campaignCacheValue) {
		return nil, false
	}

	if campaignCacheValue == sponsoredCampaignUserStatusCacheNilString {
		return nil, true
	}

	var userStatus user_campaign_statuses.SponsoredCheermoteUserStatus
	err = json.Unmarshal([]byte(campaignCacheValue), &userStatus)
	if err != nil {
		c.Del(ctx, campaignID, userID)
		log.WithError(err).Error("Error unmarshalling user status")
		return nil, false
	}

	return &userStatus, true
}

func (c *userCache) Set(ctx context.Context, campaignID, userID string, sponsoredCampaign *user_campaign_statuses.SponsoredCheermoteUserStatus) {
	var value string
	if sponsoredCampaign != nil {
		campaignJson, err := json.Marshal(*sponsoredCampaign)
		if err != nil {
			log.WithError(err).Error("Error marshaling user status")
			return
		}
		value = string(campaignJson)
	} else {
		value = sponsoredCampaignUserStatusCacheNilString
	}

	err := hystrix.Do(cmds.CampaignsUserCacheSet, func() error {
		err := c.RedisClient.Set(ctx, fmt.Sprintf(sponsoredCampaignUserStatusKeyFormat, campaignID, userID), value, sponsoredCampaignUserStatusCacheTTL).Err()
		if err != nil {
			log.WithError(err).Error("Error setting user status in cache")
		}
		return nil
	}, nil)
	if err != nil {
		log.WithError(err).Warn("hystrix errored while setting campaign user status in cache")
	}
}

func (c *userCache) Del(ctx context.Context, campaignID, userID string) {
	err := hystrix.Do(cmds.CampaignsUserCacheDel, func() error {
		err := c.RedisClient.Del(ctx, fmt.Sprintf(sponsoredCampaignUserStatusKeyFormat, campaignID, userID)).Err()
		if err != nil {
			log.WithError(err).Error("Error deleting user status from cache")
		}
		return nil
	}, nil)
	if err != nil {
		log.WithError(err).Warn("hystrix error while deleting campaign user status from cache")
	}
}
