package user_campaign_status

import (
	"context"

	"code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/user_campaign_statuses"
)

type Fetcher interface {
	Fetch(ctx context.Context, campaignID, userID string) (*user_campaign_statuses.SponsoredCheermoteUserStatus, error)
}

type fetcher struct {
	Dao   user_campaign_statuses.ISponsoredCheermoteUserStatusesDao `inject:""`
	Cache Cache                                                     `inject:""`
}

func NewFetcher() Fetcher {
	return &fetcher{}
}

func (f *fetcher) Fetch(ctx context.Context, campaignID, userID string) (*user_campaign_statuses.SponsoredCheermoteUserStatus, error) {
	cachedDynamoRecord, existsInCache := f.Cache.Get(ctx, campaignID, userID)
	if existsInCache {
		return cachedDynamoRecord, nil
	} else {
		dynamoRecord, err := f.Dao.Get(userID, campaignID)
		if err != nil {
			return nil, err
		}

		f.Cache.Set(ctx, campaignID, userID, dynamoRecord)
		return dynamoRecord, nil
	}
}
