package user_campaign_status

import "context"

type Getter interface {
	Get(ctx context.Context, campaignID, userId string) (int64, error)
}

type getter struct {
	Fetcher Fetcher `inject:""`
}

func NewGetter() Getter {
	return &getter{}
}

func (g *getter) Get(ctx context.Context, campaignID, userID string) (int64, error) {
	userStatus, err := g.Fetcher.Fetch(ctx, campaignID, userID)
	if err != nil || userStatus == nil {
		return int64(0), err
	}

	return userStatus.UsedBits, nil
}
