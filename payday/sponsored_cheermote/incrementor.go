package sponsored_cheermote

import (
	"context"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/campaigns"
	"code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/user_campaign_statuses"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/sponsored_cheermote/campaign"
	"code.justin.tv/commerce/payday/sponsored_cheermote/user_campaign_status"
)

type Incrementor interface {
	IncrementUsedBits(ctx context.Context, userId string, sponsoredCheermoteAmounts map[string]models.BitEmoteTotal)
}

type incrementor struct {
	CampaignDAO campaigns.ISponsoredCheermoteCampaignDao                  `inject:""`
	UserDAO     user_campaign_statuses.ISponsoredCheermoteUserStatusesDao `inject:""`
	Cache       campaign.Cache                                            `inject:""`
	UserCache   user_campaign_status.Cache                                `inject:""`
}

func NewIncrementor() Incrementor {
	return &incrementor{}
}

func (inc *incrementor) IncrementUsedBits(ctx context.Context, userId string, cheermoteAmounts map[string]models.BitEmoteTotal) {
	for _, data := range cheermoteAmounts {
		if data.Campaign != nil && data.MatchedTotal > 0 {
			// TODO: retry logic around increment bits
			go inc.incrementCampaignUsedBits(context.Background(), data.Campaign, data.MatchedTotal)
			go inc.incrementUserUsedBits(context.Background(), data.Campaign, data.MatchedTotal, userId)
		}
	}
}

func (inc *incrementor) incrementCampaignUsedBits(ctx context.Context, campaign *campaigns.SponsoredCheermoteCampaign, matchedAmount int64) {
	err := inc.CampaignDAO.IncrementBitsUsed(campaign, matchedAmount)
	if err != nil {
		log.WithError(err).WithFields(log.Fields{
			"campaignID":        campaign.CampaignID,
			"campaignUsedBits":  campaign.UsedBits,
			"campaignTotalBits": campaign.TotalBits,
		}).Error("Could not increment used bits for campaign")
	}

	inc.Cache.Del(ctx, campaign.CampaignID)
}

func (inc *incrementor) incrementUserUsedBits(ctx context.Context, campaign *campaigns.SponsoredCheermoteCampaign, matchedAmount int64, userId string) {
	err := inc.UserDAO.IncrementBitsUsed(campaign.CampaignID, userId, matchedAmount)
	if err != nil {
		log.WithError(err).WithFields(log.Fields{
			"campaignID":        campaign.CampaignID,
			"campaignUsedBits":  campaign.UsedBits,
			"campaignTotalBits": campaign.TotalBits,
		}).Error("Could not increment used bits for campaign")
	}

	inc.UserCache.Del(ctx, campaign.CampaignID, userId)
}
