package channel

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/clients/redis"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/hystrix"
	cmd "code.justin.tv/commerce/payday/hystrix"
	"code.justin.tv/commerce/payday/utils/strings"
	go_redis "github.com/go-redis/redis"
)

const (
	channelKeyFormat = "channel-dynamo-%s"
	channelCacheTTL  = 10 * time.Minute
)

type ChannelManager interface {
	GetByImageSetId(imageSetId string) (*dynamo.Channel, error)
	Get(ctx context.Context, channelID string) (*dynamo.Channel, error)
	Update(ctx context.Context, channel *dynamo.Channel) error
	UpdateIfStatusIsNewer(ctx context.Context, channel *dynamo.Channel) error
}

type ManagerImpl struct {
	RedisClient redis.Client       `inject:"redisClient"`
	ChannelDAO  dynamo.IChannelDao `inject:""`
}

func NewChannelManager() ChannelManager {
	return &ManagerImpl{}
}

func NewChannelManagerWithClients(redisClient redis.Client, channelDAO dynamo.IChannelDao) *ManagerImpl {
	return &ManagerImpl{
		RedisClient: redisClient,
		ChannelDAO:  channelDAO,
	}
}

func (m *ManagerImpl) GetByImageSetId(imageSetId string) (*dynamo.Channel, error) {
	return m.ChannelDAO.GetByImageSetId(imageSetId)
}

func (m *ManagerImpl) Get(ctx context.Context, channelID string) (*dynamo.Channel, error) {
	redisRecord, currentlyCached, err := m.getRedisHystrix(ctx, channelID)
	if err != nil {
		log.WithError(err).WithField("channel_id", channelID).Warn("Error retrieving channel from Redis, falling back to dynamo.")
	}

	if currentlyCached {
		return redisRecord, nil
	}

	dynamoRecord, err := m.getDynamo(channelID)
	if err != nil {
		return nil, err
	}

	go func() {
		_ = m.setInRedis(context.Background(), channelID, dynamoRecord)
	}()

	return dynamoRecord, nil
}

func (m *ManagerImpl) setInRedis(ctx context.Context, channelID string, channel *dynamo.Channel) error {
	var channelJSONStr string
	if channel != nil {
		channelJSON, err := json.Marshal(channel)
		if err != nil {
			msg := "ChannelCacheManager: Error marshalling channel"
			log.WithError(err).WithField("channelID", channelID).Error(msg)
			return errors.Notef(err, msg)
		}
		channelJSONStr = string(channelJSON)
	}

	err := m.RedisClient.Set(ctx, fmt.Sprintf(channelKeyFormat, channelID), string(channelJSONStr), channelCacheTTL).Err()
	if err != nil {
		msg := "ChannelCacheManager: Error setting channel in redis"
		log.WithError(err).WithField("channelID", channelID).Error(msg)
		return errors.Notef(err, msg)
	}

	return nil
}

func (m *ManagerImpl) Update(ctx context.Context, channel *dynamo.Channel) error {
	if channel == nil {
		return nil
	}
	channelID := string(channel.Id)

	err := m.ChannelDAO.Update(channel)
	if err != nil {
		return errors.Notef(err, "ChannelCacheManager: Error updating channel in dynamo")
	}

	_ = m.deleteFromRedis(ctx, channelID)

	return nil
}

func (m *ManagerImpl) UpdateIfStatusIsNewer(ctx context.Context, channel *dynamo.Channel) error {
	if channel == nil {
		return nil
	}

	err := m.ChannelDAO.UpdateIfStatusIsNewer(*channel)
	if err != nil {
		return err
	}

	_ = m.deleteFromRedis(ctx, string(channel.Id))

	return nil
}

func (m *ManagerImpl) getDynamo(channelID string) (*dynamo.Channel, error) {
	record, err := m.ChannelDAO.Get(dynamo.ChannelId(channelID))
	if err != nil {
		return nil, errors.Notef(err, "ChannelCacheManager: Error getting channel from dynamo")
	}
	return record, nil
}

func (m *ManagerImpl) getRedisHystrix(ctx context.Context, channelID string) (*dynamo.Channel, bool, error) {
	channelChan := make(chan *dynamo.Channel, 1)
	presentChan := make(chan bool, 1)
	fn := func() error {
		ch, isPresent, err := m.getRedis(ctx, channelID)
		if err != nil {
			return err
		}
		channelChan <- ch
		presentChan <- isPresent
		return nil
	}

	err := hystrix.Do(cmd.GetChannelRedisCommand, fn, nil)
	if err != nil {
		return nil, false, err
	}

	channel := <-channelChan
	isPresent := <-presentChan
	return channel, isPresent, nil
}

func (m *ManagerImpl) getRedis(ctx context.Context, channelID string) (*dynamo.Channel, bool, error) {
	cacheStr, err := m.RedisClient.Get(ctx, fmt.Sprintf(channelKeyFormat, channelID)).Result()
	if err != nil && err != go_redis.Nil {
		return nil, false, errors.Notef(err, "ChannelCacheManager: Error getting channel out of redis")
	}

	if err == go_redis.Nil {
		return nil, false, nil
	}

	if strings.Blank(cacheStr) {
		return nil, true, nil
	}

	var record dynamo.Channel
	err = json.Unmarshal([]byte(cacheStr), &record)
	if err != nil {
		errDelete := m.deleteFromRedis(ctx, channelID)
		if errDelete != nil {
			return nil, false, errors.Notef(errDelete, "ChannelCacheManager: Error deleting channel from dynamo")
		}
		return nil, false, errors.Notef(err, "ChannelCacheManager: Error unmarshalling channel")
	}

	return &record, true, nil
}

func (m *ManagerImpl) deleteFromRedis(ctx context.Context, channelID string) error {
	err := m.RedisClient.Del(ctx, fmt.Sprintf(channelKeyFormat, channelID)).Err()
	if err != nil {
		msg := "ChannelCacheManager: Error deleting chanel from redis"
		log.WithError(err).WithField("channelID", channelID).Error(msg)
		return errors.Notef(err, msg)
	}
	return nil
}
