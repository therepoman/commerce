package channel

import (
	"testing"

	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/models/api"
	"code.justin.tv/commerce/payday/utils/pointers"
	. "github.com/smartystreets/goconvey/convey"
)

func TestChannel_GetEligibility(t *testing.T) {
	// I need to be able to address these values, so /shrug.
	isTrue := true
	isFalse := false

	Convey("Given nothing", t, func() {

		Convey("given a nil dynamo channel", func() {
			So(GetChannelEligibility(nil), ShouldBeFalse)
		})

		Convey("given a channel", func() {
			dynamoChannel := dynamo.Channel{}
			So(GetChannelEligibility(&dynamoChannel), ShouldBeFalse)

			Convey("when onboarded", func() {
				dynamoChannel.Onboarded = &isTrue
				So(GetChannelEligibility(&dynamoChannel), ShouldBeTrue)

				Convey("when opted in", func() {
					dynamoChannel.OptedOut = &isFalse
					So(GetChannelEligibility(&dynamoChannel), ShouldBeTrue)
				})

				Convey("when opted out (explicitly)", func() {
					dynamoChannel.OptedOut = &isTrue
					So(GetChannelEligibility(&dynamoChannel), ShouldBeFalse)
				})
			})

			Convey("when not onboarded (explicitly)", func() {
				dynamoChannel.Onboarded = &isFalse
				So(GetChannelEligibility(&dynamoChannel), ShouldBeFalse)

				Convey("when opted in", func() {
					dynamoChannel.OptedOut = &isFalse
					So(GetChannelEligibility(&dynamoChannel), ShouldBeFalse)
				})

				Convey("when opted out (explicitly)", func() {
					dynamoChannel.OptedOut = &isTrue
					So(GetChannelEligibility(&dynamoChannel), ShouldBeFalse)
				})
			})
		})
	})
}

func TestChannel_GetMinBits(t *testing.T) {
	minBits := int64(55)

	Convey("Given nothing", t, func() {

		Convey("given a nil dynamo channel", func() {
			So(GetChannelMinBits(nil), ShouldEqual, api.DefaultMinBits)
		})

		Convey("given a channel", func() {
			dynamoChannel := dynamo.Channel{}
			So(GetChannelMinBits(&dynamoChannel), ShouldEqual, api.DefaultMinBits)

			Convey("with min bits set", func() {
				dynamoChannel.MinBits = &minBits
				So(GetChannelMinBits(&dynamoChannel), ShouldEqual, minBits)
			})
		})
	})
}

func TestGetChannelLeaderboardSettings(t *testing.T) {
	defaultValues := &api.LeaderboardSettings{
		LeaderboardEnabled:          false,
		IsCheerLeaderboardEnabled:   false,
		IsSubGiftLeaderboardEnabled: false,
		DefaultLeaderboard:          api.DefaultLeaderboard,
		LeaderboardTimePeriod:       models.DefaultLeaderboardTimePeriod,
	}

	assertSettingsEqualLeaderboardValues := func(settings *api.LeaderboardSettings, values *api.LeaderboardSettings) {
		So(settings.LeaderboardEnabled, ShouldEqual, values.LeaderboardEnabled)
		So(settings.IsCheerLeaderboardEnabled, ShouldEqual, values.IsCheerLeaderboardEnabled)
		So(settings.IsSubGiftLeaderboardEnabled, ShouldEqual, values.IsSubGiftLeaderboardEnabled)
		So(settings.DefaultLeaderboard, ShouldEqual, values.DefaultLeaderboard)
		So(settings.LeaderboardTimePeriod, ShouldEqual, values.LeaderboardTimePeriod)
	}

	Convey("given a nil dynamo channel should return default values", t, func() {
		settings := GetChannelLeaderboardSettings(nil)
		assertSettingsEqualLeaderboardValues(settings, defaultValues)
	})

	Convey("given a channel without leaderboard settings should return default values", t, func() {
		settings := GetChannelLeaderboardSettings(nil)
		assertSettingsEqualLeaderboardValues(settings, defaultValues)
	})

	Convey("given a channel with leaderboard settings", t, func() {
		channel := dynamo.Channel{}
		channel.LeaderboardEnabled = pointers.BoolP(false)
		channel.IsCheerLeaderboardEnabled = pointers.BoolP(true)
		channel.IsSubGiftLeaderboardEnabled = pointers.BoolP(true)
		channel.DefaultLeaderboard = pointers.StringP(api.DefaultLeaderboardCheer)
		channel.LeaderboardTimePeriod = pointers.StringP(pantheonrpc.TimeUnit_name[0])

		settings := GetChannelLeaderboardSettings(&channel)
		assertSettingsEqualLeaderboardValues(settings, &api.LeaderboardSettings{
			LeaderboardEnabled:          false,
			IsCheerLeaderboardEnabled:   true,
			IsSubGiftLeaderboardEnabled: true,
			DefaultLeaderboard:          api.DefaultLeaderboardCheer,
			LeaderboardTimePeriod:       pantheonrpc.TimeUnit_name[0],
		})
	})
}
