package channel

import (
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/models/api"
	"code.justin.tv/commerce/payday/utils/pointers"
	"code.justin.tv/commerce/payday/utils/time"
)

func GetChannelEligibility(channel *dynamo.Channel) bool {
	if channel == nil {
		return false
	}
	onboarded := pointers.BoolOrDefault(channel.Onboarded, false)
	optedOut := pointers.BoolOrDefault(channel.OptedOut, false)
	return onboarded && !optedOut
}

func GetChannelMinBits(channel *dynamo.Channel) int64 {
	if channel == nil {
		return 1
	}
	return pointers.Int64OrDefault(channel.MinBits, api.DefaultMinBits)
}

func GetChannelMinBitsEmote(channel *dynamo.Channel) int64 {
	if channel == nil {
		return 1
	}
	return pointers.Int64OrDefault(channel.MinBitsEmote, api.DefaultMinBits)
}

func GetChannelPinTopCheers(channel *dynamo.Channel) bool {
	if channel == nil {
		return false
	}
	return pointers.BoolOrDefault(channel.PinTopCheers, api.DefaultPinTopCheers)
}

func GetChannelPinRecentCheers(channel *dynamo.Channel) bool {
	if channel == nil {
		return false
	}
	return pointers.BoolOrDefault(channel.PinRecentCheers, api.DefaultPinRecentCheers)
}

func GetChannelRecentCheerMin(channel *dynamo.Channel) int64 {
	if channel == nil {
		return 0
	}
	return pointers.Int64OrDefault(channel.RecentCheerMin, api.DefaultRecentCheerMin)
}

func GetChannelRecentCheerTimeout(channel *dynamo.Channel) int64 {
	if channel == nil {
		return 0
	}
	return pointers.Int64OrDefault(channel.RecentCheerTimeout, time.SecondsToNanoseconds(api.DefaultRecentCheerTimeoutSeconds))
}

func GetChannelLeaderboardSettings(channel *dynamo.Channel) *api.LeaderboardSettings {
	if channel == nil {
		return &api.LeaderboardSettings{
			LeaderboardEnabled:          models.GetLeaderboardEnabled(channel),
			IsCheerLeaderboardEnabled:   models.GetIsCheerLeaderboardEnabled(channel),
			IsSubGiftLeaderboardEnabled: models.GetIsSubGiftLeaderboardEnabled(channel),
			DefaultLeaderboard:          api.DefaultLeaderboard,
			LeaderboardTimePeriod:       models.DefaultLeaderboardTimePeriod,
		}
	}

	return &api.LeaderboardSettings{
		LeaderboardEnabled:          models.GetLeaderboardEnabled(channel),
		IsCheerLeaderboardEnabled:   models.GetIsCheerLeaderboardEnabled(channel),
		IsSubGiftLeaderboardEnabled: models.GetIsSubGiftLeaderboardEnabled(channel),
		DefaultLeaderboard:          pointers.StringOrDefault(channel.DefaultLeaderboard, api.DefaultLeaderboard),
		LeaderboardTimePeriod:       pointers.StringOrDefault(channel.LeaderboardTimePeriod, models.DefaultLeaderboardTimePeriod),
	}
}

func GetChannelCheerBombEventOptOut(channel *dynamo.Channel) bool {
	if channel == nil {
		return false
	}
	return pointers.BoolOrDefault(channel.CheerBombEventOptOut, api.DefaultCheerBombEventOptOut)
}

func GetChannelOptedOutOfVoldemort(channel *dynamo.Channel) bool {
	if channel == nil {
		return false
	}
	return pointers.BoolOrDefault(channel.OptedOutOfVoldemort, api.DefaultOptedOutOfVoldemort)
}
