package cache

import (
	"context"
	"testing"
	"time"

	"code.justin.tv/commerce/payday/clients/redis"
	"code.justin.tv/commerce/payday/dynamo/badge_tier_emote_groupids"
	. "github.com/smartystreets/goconvey/convey"
)

func TestBadgeTierEmotes_CacheKey(t *testing.T) {
	Convey("Given a function to generate cache strings", t, func() {

		Convey("When not given an id", func() {
			actual := badgeTierEmotesKey("")
			So(actual, ShouldEqual, ".badgeTierEmotes")
		})
		Convey("Given a channel id", func() {
			actual := badgeTierEmotesKey("488")
			So(actual, ShouldEqual, "488.badgeTierEmotes")
		})
	})
}

func TestBadgeTierEmotesCache_Caching(t *testing.T) {
	ctx := context.Background()
	channelId := "489"

	client, server := redis.TestRedisClient()
	defer server.Close()

	createdAt, _ := time.Parse(time.RFC3339, "2019-01-02T15:04:05Z07:00")

	referenceEmotes := []*badge_tier_emote_groupids.BadgeTierEmoteGroupID{
		{
			ChannelID:    channelId,
			Threshold:    1000,
			GroupID:      "1k-group-cool-group",
			CreatedAt:    createdAt,
			IsBackfilled: false,
		},
		{
			ChannelID:    channelId,
			Threshold:    5000,
			GroupID:      "5k-group-cool-group",
			CreatedAt:    createdAt,
			IsBackfilled: false,
		},
	}

	emptyEmotes := []*badge_tier_emote_groupids.BadgeTierEmoteGroupID{}

	Convey("Given a cache", t, func() {
		cache := badgeTierEmotesCache{
			RedisClient: client,
		}

		Convey("get with nothing in the cache", func() {
			emotes := cache.Get(ctx, channelId)
			So(emotes, ShouldBeNil)
		})

		Convey("putting balance into cache", func() {
			cache.Set(ctx, channelId, referenceEmotes)
			emotes := cache.Get(ctx, channelId)
			So(emotes, ShouldNotBeNil)
			So(emotes, ShouldResemble, referenceEmotes)

			Convey("deleting from balance cache", func() {
				cache.Del(ctx, channelId)
				emotes := cache.Get(ctx, channelId)
				So(emotes, ShouldBeNil)
			})
		})

		Convey("empty is valid and distinct", func() {
			cache.Set(ctx, channelId, emptyEmotes)
			emotes := cache.Get(ctx, channelId)
			So(emotes, ShouldNotBeNil)
			So(emotes, ShouldResemble, emptyEmotes)
		})
	})
}
