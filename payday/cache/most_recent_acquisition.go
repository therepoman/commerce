package cache

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/clients/redis"
	"code.justin.tv/commerce/payday/dynamo/bits_entitlement"
	go_redis "github.com/go-redis/redis"
)

const (
	mostRecentAcquiredNilValue = "<nil>"
	mostRecentAcquiredFormat   = "bits-most-recently-acquired-%s"
	mostRecentAcquiredTtl      = time.Minute * 15
)

type MostRecentlyAcquiredCache interface {
	Set(ctx context.Context, userID string, mostRecentAcquisiton *bits_entitlement.BitsEntitlement)
	Get(ctx context.Context, userID string) (*bits_entitlement.BitsEntitlement, bool)
	Del(ctx context.Context, userID string)
}

type mostRecentlyAcquiredCache struct {
	RedisClient redis.Client `inject:"redisClient"`
}

func NewMostRecentlyAcquiredCache() MostRecentlyAcquiredCache {
	return &mostRecentlyAcquiredCache{}
}

func (m *mostRecentlyAcquiredCache) Set(ctx context.Context, userID string, mostRecentAcquisiton *bits_entitlement.BitsEntitlement) {
	mostRecentAcquistionForCache := mostRecentAcquiredNilValue

	if mostRecentAcquisiton != nil {
		jsonAcquisition, err := json.Marshal(mostRecentAcquisiton)
		if err != nil {
			log.WithError(err).WithFields(log.Fields{
				"userID":               userID,
				"mostRecentAcquisiton": mostRecentAcquisiton,
			}).Error("Failed serialize entry for the most recent acquisition cache")
			return
		} else {
			mostRecentAcquistionForCache = string(jsonAcquisition)
		}
	}

	err := m.RedisClient.Set(ctx, mostRecentlyAcquiredKey(userID), mostRecentAcquistionForCache, mostRecentAcquiredTtl).Err()
	if err != nil {
		log.WithError(err).WithFields(log.Fields{
			"userID":               userID,
			"mostRecentAcquisiton": mostRecentAcquisiton,
		}).Error("Failed to set entry in the most recent acquisition cache")
	}
}

func (m *mostRecentlyAcquiredCache) Get(ctx context.Context, userID string) (*bits_entitlement.BitsEntitlement, bool) {
	result, err := m.RedisClient.Get(ctx, mostRecentlyAcquiredKey(userID)).Result()
	if err != nil && err != go_redis.Nil {
		log.WithError(err).WithField("userID", userID).Error("Failed to get entry in most recent acquisition cache")
		return nil, false
	}

	if err == go_redis.Nil {
		return nil, false
	}

	if result == mostRecentAcquiredNilValue {
		return nil, true
	}

	var entitlement bits_entitlement.BitsEntitlement
	err = json.Unmarshal([]byte(result), &entitlement)
	if err != nil {
		log.WithError(err).WithFields(log.Fields{
			"userID": userID,
		}).Error("Failed deserialize entry for the most recent acquisition cache")
		return nil, false
	}

	return &entitlement, true
}

func (m *mostRecentlyAcquiredCache) Del(ctx context.Context, userID string) {
	err := m.RedisClient.Del(ctx, mostRecentlyAcquiredKey(userID)).Err()
	if err != nil {
		log.WithError(err).WithField("userID", userID).Error("Failed to delete entry in the most recent acquisition cache")
	}
}

func mostRecentlyAcquiredKey(userID string) string {
	return fmt.Sprintf(mostRecentAcquiredFormat, userID)
}
