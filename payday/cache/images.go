package cache

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/clients/redis"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/hystrix"
	cmds "code.justin.tv/commerce/payday/hystrix"
	"code.justin.tv/commerce/payday/utils/strings"
	go_redis "github.com/go-redis/redis"
)

type IImagesCache interface {
	Get(ctx context.Context, imageSetId string) ([]*dynamo.Image, bool)
	Set(ctx context.Context, imageSetId string, images []*dynamo.Image)
	Delete(ctx context.Context, imageSetId string) error
}

type ImagesCache struct {
	RedisClient redis.Client `inject:"redisClient"`
}

func (ic *ImagesCache) Get(ctx context.Context, imageSetId string) ([]*dynamo.Image, bool) {
	var imagesCacheValue string
	err := hystrix.Do(cmds.ImagesCacheGet, func() error {
		var err error
		imagesCacheValue, err = ic.RedisClient.Get(ctx, fmt.Sprintf("%s.images", imageSetId)).Result()
		if err != nil && err != go_redis.Nil {
			log.Error("ImagesCache: Error getting Images out of cache", err)
		}
		return nil
	}, nil)
	if err != nil {
		log.WithError(err).Warn("hystrix error while getting images from cache")
		return []*dynamo.Image{}, false
	}

	if strings.Blank(imagesCacheValue) {
		return []*dynamo.Image{}, false
	}

	var images []*dynamo.Image
	err = json.Unmarshal([]byte(imagesCacheValue), &images)
	if err != nil {
		deleteErr := ic.Delete(ctx, imageSetId)
		if deleteErr != nil {
			log.Error("ImagesCache: Error deleting Images after unmarshall fail", err)
		}
		log.Error("ImagesCache: Error unmarshling Images", err)
		return []*dynamo.Image{}, false

	}

	return images, true
}

func (ic *ImagesCache) Set(ctx context.Context, imageSetId string, images []*dynamo.Image) {
	imagesJson, err := json.Marshal(images)
	if err != nil {
		log.Error("ImagesCache: Error marshling Images", err)
		return
	}

	err = hystrix.Do(cmds.ImagesCacheSet, func() error {
		err := ic.RedisClient.Set(ctx, fmt.Sprintf("%s.images", imageSetId), string(imagesJson), 4*time.Hour).Err()
		if err != nil {
			log.Error("ImagesCache: Error setting Images in cache", err)
		}
		return nil
	}, nil)

	if err != nil {
		log.WithError(err).Warn("hystrix error while setting images in cache")
	}
}

func (ic *ImagesCache) Delete(ctx context.Context, imageSetId string) error {
	err := hystrix.Do(cmds.ImagesCacheDel, func() error {
		err := ic.RedisClient.Del(ctx, fmt.Sprintf("%s.images", imageSetId)).Err()
		if err != nil {
			log.Error("ImagesCache: Error deleting Images from cache", err)
		}
		return err
	}, nil)
	if err != nil {
		log.WithError(err).Warn("hystrix error while deleting images in cache")
	}
	return err
}
