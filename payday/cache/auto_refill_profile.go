package cache

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"code.justin.tv/commerce/gogogadget/strings"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/clients/redis"
	"code.justin.tv/commerce/payday/dynamo/auto_refill_settings"
	"code.justin.tv/commerce/payday/hystrix"
	cmds "code.justin.tv/commerce/payday/hystrix"
	go_redis "github.com/go-redis/redis"
)

const (
	autoReloadProfileTopic    = "%s.reload"
	autoReloadProfileCacheTTL = time.Minute * 5
	autoReloadProfileNIL      = "NIL"
)

type AutoRefillProfileCache interface {
	Get(ctx context.Context, userId string) (*auto_refill_settings.AutoRefillSettingsProfile, bool, error)
	Set(ctx context.Context, userId string, balance *auto_refill_settings.AutoRefillSettingsProfile)
	Delete(ctx context.Context, userId string)
}

type autoRefillProfileCache struct {
	RedisClient redis.Client `inject:"redisClient"`
}

func NewIAutoRefillProfileCache() AutoRefillProfileCache {
	return &autoRefillProfileCache{}
}

func (c *autoRefillProfileCache) Get(ctx context.Context, userId string) (*auto_refill_settings.AutoRefillSettingsProfile, bool, error) {
	var cacheValue string
	err := hystrix.Do(cmds.AutoReloadProfileCacheGet, func() error {
		var err error
		cacheValue, err = c.RedisClient.Get(ctx, fmt.Sprintf(autoReloadProfileTopic, userId)).Result()
		if err != nil && err != go_redis.Nil {
			log.WithError(err).Error("Error getting user autoreload profile settings")
			return err
		}
		return nil
	}, nil)

	if err != nil {
		log.WithError(err).Warn("hystrix error fetching autoreload profile from cache")
		return nil, false, err
	}

	if strings.Blank(cacheValue) {
		return nil, false, nil
	}

	if cacheValue == autoReloadProfileNIL {
		return nil, true, nil
	}

	var deserializedAutoReloadProfile auto_refill_settings.AutoRefillSettingsProfile
	err = json.Unmarshal([]byte(cacheValue), &deserializedAutoReloadProfile)
	if err != nil {
		c.Delete(ctx, userId)
		log.WithError(err).Error("Error unmarshalling autoreload profile settings")
		return nil, true, err
	}

	return &deserializedAutoReloadProfile, true, nil
}

func (c *autoRefillProfileCache) Set(ctx context.Context, userId string, autoRefillSettingsProfile *auto_refill_settings.AutoRefillSettingsProfile) {
	var serializedAutoRefillProfile string

	if autoRefillSettingsProfile != nil {
		serializedAutoRefillProfileJson, err := json.Marshal(autoRefillSettingsProfile)
		if err != nil {
			log.WithError(err).Error("Error marshaling autoreload profile settings")
			return
		}
		serializedAutoRefillProfile = string(serializedAutoRefillProfileJson)
	} else {
		serializedAutoRefillProfile = autoReloadProfileNIL
	}

	err := hystrix.Do(cmds.AutoReloadProfileCacheSet, func() error {
		err := c.RedisClient.Set(ctx, fmt.Sprintf(autoReloadProfileTopic, userId), serializedAutoRefillProfile, autoReloadProfileCacheTTL).Err()
		if err != nil {
			log.WithError(err).Error("Error setting autoreload profile settings in cache")
		}
		return nil
	}, nil)
	if err != nil {
		log.WithError(err).Warn("hystrix error setting autoreload profile in cache")
	}
}

func (c *autoRefillProfileCache) Delete(ctx context.Context, userId string) {
	err := hystrix.Do(cmds.AutoReloadProfileCacheDel, func() error {
		err := c.RedisClient.Del(ctx, fmt.Sprintf(autoReloadProfileTopic, userId)).Err()
		if err != nil {
			log.WithError(err).Error("Error deleting autoreload profile settings from cache")
		}
		return nil
	}, nil)
	if err != nil {
		log.WithError(err).Warn("hystrix error deleting autoreload profile from cache")
	}
}
