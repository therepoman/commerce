package cache

import (
	"context"
	"encoding/json"
	"fmt"
	"strconv"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/clients/redis"
	"code.justin.tv/commerce/payday/hystrix"
	cmds "code.justin.tv/commerce/payday/hystrix"
	"code.justin.tv/commerce/payday/models"
	util "code.justin.tv/commerce/payday/utils/close"
	go_redis "github.com/go-redis/redis"
)

const (
	balanceTopic        = "%s.balance"
	channelBalanceTopic = "%s.channel_balance"
)

type IBalanceCache interface {
	Get(ctx context.Context, userId string, channelId string) *models.Balance
	Set(ctx context.Context, userId string, channelId string, balance *models.Balance)
	Delete(ctx context.Context, userId string)
	DeleteWithChannel(ctx context.Context, userId string, channelId string)
}

type BalanceCache struct {
	RedisClient redis.Client `inject:"redisClient"`
}

func (bc *BalanceCache) Get(ctx context.Context, userId string, channelId string) *models.Balance {
	var balance models.Balance

	var channelBalanceValue string
	var balanceValue string

	pipe := bc.RedisClient.Pipeline(ctx)
	defer util.Close(pipe)
	err := hystrix.Do(cmds.BalanceCacheGet, func() error {
		balanceCache := pipe.Get(fmt.Sprintf(balanceTopic, userId))
		channelBalanceCache := pipe.HGet(fmt.Sprintf(channelBalanceTopic, userId), channelId)

		_, err := pipe.Exec()
		if err != nil && err != go_redis.Nil {
			log.WithError(err).Error("BalanceCache: Error executing redis pipeline")
		}

		channelBalanceValue = channelBalanceCache.Val()
		balanceValue = balanceCache.Val()
		return nil
	}, nil)
	if err != nil {
		log.WithError(err).Warn("hystrix error returned while getting balance")
	}

	if channelBalanceValue == "" && channelId != "" {
		return nil
	}

	var channelTotal int32 = 0
	if channelBalanceValue != "" {
		total, err := strconv.ParseInt(channelBalanceValue, 10, 32)
		if err != nil {
			bc.DeleteWithChannel(ctx, userId, channelId)
			log.WithError(err).Error("BalanceCache: Error parsing channel total from redis")
			return nil
		}

		channelTotal = int32(total)
	}

	if balanceValue != "" {
		err = json.Unmarshal([]byte(balanceValue), &balance)
		if err != nil {
			bc.Delete(ctx, userId)
			log.WithError(err).Error("BalanceCache: Error unmarshaling models.Balance")
			return nil
		}
		balance.TotalBitsWithBroadcaster = &channelTotal
	} else {
		return nil
	}

	return &balance
}

func (bc *BalanceCache) Set(ctx context.Context, userId string, channelId string, balance *models.Balance) {
	var emptyBalance int32 = 0
	channelEmptyBalance := &models.Balance{
		TotalBits:                     balance.TotalBits,
		CanPurchase:                   balance.CanPurchase,
		TotalBitsWithBroadcaster:      &emptyBalance,
		MaximumBitsInventoryLimit:     balance.MaximumBitsInventoryLimit,
		ProductInformation:            balance.ProductInformation,
		MostRecentPurchaseWebPlatform: balance.MostRecentPurchaseWebPlatform,
	}

	pipe := bc.RedisClient.Pipeline(ctx)
	defer util.Close(pipe)
	err := hystrix.Do(cmds.BalanceCacheSet, func() error {
		pachinkoBalance, err := json.Marshal(channelEmptyBalance)
		if err != nil {
			log.WithError(err).Error("BalanceCache: Error marshaling models.Balance")
			return nil
		}

		pipe.Set(fmt.Sprintf(balanceTopic, userId), string(pachinkoBalance), 5*time.Minute)
		if channelId != "" {
			pipe.HSet(fmt.Sprintf(channelBalanceTopic, userId), channelId, strconv.Itoa(int(*balance.TotalBitsWithBroadcaster)))
		}

		_, err = pipe.Exec()
		if err != nil && err != go_redis.Nil {
			log.WithError(err).Error("BalanceCache: Error executing redis pipeline")
		}
		return nil
	}, nil)
	if err != nil {
		log.WithError(err).Warn("hystrix returned an error while setting balance")
	}
}

func (bc *BalanceCache) Delete(ctx context.Context, userId string) {
	bc.DeleteWithChannel(ctx, userId, "")
}

func (bc *BalanceCache) DeleteWithChannel(ctx context.Context, userId string, channelId string) {
	pipe := bc.RedisClient.Pipeline(ctx)
	defer util.Close(pipe)
	err := hystrix.Do(cmds.BalanceCacheDel, func() error {
		pipe.Del(fmt.Sprintf(balanceTopic, userId))

		if channelId != "" {
			pipe.HDel(fmt.Sprintf(channelBalanceTopic, userId), channelId)
		}

		_, err := pipe.Exec()
		if err != nil && err != go_redis.Nil {
			log.WithError(err).Error("BalanceCache: Error executing redis pipeline")
		}
		return nil
	}, nil)
	if err != nil {
		log.WithError(err).Warn("hystrix error returned while deleting balance")
	}
}
