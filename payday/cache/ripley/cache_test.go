package ripley

import (
	"context"
	"errors"
	"fmt"
	"testing"

	"github.com/stretchr/testify/mock"

	redis_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/redis"
	"code.justin.tv/commerce/payday/partner"
	"github.com/go-redis/redis"
	. "github.com/smartystreets/goconvey/convey"
)

func TestCache_Get(t *testing.T) {
	Convey("given a cache getter", t, func() {
		ctx := context.Background()
		redisClient := new(redis_mock.Client)

		cache := &cache{
			RedisClient: redisClient,
		}

		userID := "1234567890"
		partnerTypeString := "traditional"
		cacheKey := fmt.Sprintf(ripleyPartnerTypeCacheFormat, userID)

		Convey("when the redis client returns an error", func() {
			resp := redis.NewStringResult("", errors.New("ERROR"))
			redisClient.On("Get", mock.Anything, cacheKey).Return(resp)
			result := cache.Get(ctx, userID)

			Convey("we should return nil", func() {
				So(result, ShouldBeNil)
			})
		})

		Convey("when the redis client returns a blank value", func() {
			resp := redis.NewStringResult("", nil)
			redisClient.On("Get", mock.Anything, cacheKey).Return(resp)
			result := cache.Get(ctx, userID)

			Convey("we should return nil", func() {
				So(result, ShouldBeNil)
			})
		})

		Convey("when the redis client returns partner type data", func() {
			Convey("we should return user settings", func() {
				resp := redis.NewStringResult(partnerTypeString, nil)
				redisClient.On("Get", mock.Anything, cacheKey).Return(resp)

				returnValue := cache.Get(ctx, userID)

				So(returnValue, ShouldNotBeNil)
				So(*returnValue, ShouldResemble, partner.PartnerType(partnerTypeString))
			})
		})
	})
}

func TestCache_Set(t *testing.T) {
	Convey("given a cache setter", t, func() {
		ctx := context.Background()
		redisClient := new(redis_mock.Client)

		cache := &cache{
			RedisClient: redisClient,
		}

		userID := "123123123"
		partnerTypeString := "traditional"
		cacheKey := fmt.Sprintf(ripleyPartnerTypeCacheFormat, userID)

		Convey("when passed an empty partner type", func() {
			Convey("we don't cache", func() {
				cache.Set(ctx, userID, partner.PartnerType(""))

				redisClient.AssertNotCalled(
					t,
					"Set",
					mock.Anything,
					cacheKey,
					"",
					ripleyPartnerTypeCacheTTL,
				)
			})
		})

		Convey("when the redis client returns an error", func() {
			Convey("we return nil", func() {
				redisClient.On(
					"Set",
					mock.Anything,
					cacheKey,
					string(partnerTypeString),
					ripleyPartnerTypeCacheTTL).Return(redis.NewStatusCmd(errors.New("ERROR")))

				cache.Set(ctx, userID, partner.PartnerType(partnerTypeString))

				redisClient.AssertCalled(
					t,
					"Set",
					mock.Anything,
					cacheKey,
					partnerTypeString,
					ripleyPartnerTypeCacheTTL)
			})
		})

		Convey("when the redis client does not return an error", func() {
			Convey("we should return nothing", func() {
				redisClient.On(
					"Set",
					mock.Anything,
					cacheKey,
					string(partnerTypeString),
					ripleyPartnerTypeCacheTTL).Return(redis.NewStatusCmd(nil))

				cache.Set(ctx, userID, partner.PartnerType(partnerTypeString))

				redisClient.AssertCalled(
					t,
					"Set",
					mock.Anything,
					cacheKey,
					partnerTypeString,
					ripleyPartnerTypeCacheTTL)
			})
		})
	})
}
