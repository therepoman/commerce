package ripley

import (
	"context"
	"fmt"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/clients/redis"
	"code.justin.tv/commerce/payday/hystrix"
	cmds "code.justin.tv/commerce/payday/hystrix"
	"code.justin.tv/commerce/payday/partner"
	"code.justin.tv/commerce/payday/utils/strings"
	go_redis "github.com/go-redis/redis"
)

const (
	ripleyPartnerTypeCacheFormat = "ripley-partner-type-%s"
	ripleyPartnerTypeCacheTTL    = 15 * time.Minute
)

type Cache interface {
	Get(ctx context.Context, userID string) *partner.PartnerType
	Set(ctx context.Context, userID string, partnerType partner.PartnerType)
}

func NewCache() Cache {
	return &cache{}
}

type cache struct {
	RedisClient redis.Client `inject:"redisClient"`
}

func (c *cache) Get(ctx context.Context, userID string) *partner.PartnerType {
	var cacheValue string
	err := hystrix.Do(cmds.RipleyCacheGet, func() error {
		var err error
		cacheValue, err = c.RedisClient.Get(ctx, fmt.Sprintf(ripleyPartnerTypeCacheFormat, userID)).Result()
		if err != nil && err != go_redis.Nil {
			log.WithError(err).Error("Error getting ripley partner type out of cache")
		}
		return nil
	}, nil)
	if err != nil {
		log.WithError(err).Warn("hystrix error fetching ripley partner type from cache")
	}

	if strings.Blank(cacheValue) {
		return nil
	}

	partnerType := partner.PartnerType(cacheValue)
	return &partnerType
}

func (c *cache) Set(ctx context.Context, userID string, partnerType partner.PartnerType) {
	if strings.Blank(string(partnerType)) {
		return
	}

	err := hystrix.Do(cmds.RipleyCacheSet, func() error {
		err := c.RedisClient.Set(ctx, fmt.Sprintf(ripleyPartnerTypeCacheFormat, userID), string(partnerType), ripleyPartnerTypeCacheTTL).Err()
		if err != nil {
			log.WithError(err).Error("Error setting ripley partner type in cache")
		}
		return nil
	}, nil)

	if err != nil {
		log.WithError(err).Warn("hystrix error setting ripley partner type in cache")
	}
}
