package cache

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/clients/redis"
	"code.justin.tv/commerce/payday/hystrix"
	cmds "code.justin.tv/commerce/payday/hystrix"
	"code.justin.tv/commerce/payday/models/api"
	"code.justin.tv/commerce/payday/utils/strings"
	go_redis "github.com/go-redis/redis"
)

const (
	automodKeyFormat = "automod-%s"
	automodCacheTTL  = 5 * time.Minute
)

type IAutoModCache interface {
	Get(ctx context.Context, userId string) *api.CreateEventRequest
	Set(ctx context.Context, userId string, req api.CreateEventRequest) error
	Delete(ctx context.Context, userId string) error
}

type AutoModCache struct {
	RedisClient redis.Client `inject:"redisClient"`
}

func (ac *AutoModCache) Get(ctx context.Context, userId string) *api.CreateEventRequest {
	var automodCacheValue string
	err := hystrix.Do(cmds.AutomodCacheGet, func() error {
		var err error
		automodCacheValue, err = ac.RedisClient.Get(ctx, fmt.Sprintf(automodKeyFormat, userId)).Result()
		if err != nil && err != go_redis.Nil {
			log.WithError(err).Error("AutoModCache: Error getting BitsEvent with bad words from cache")
		}
		return nil
	}, nil)
	if err != nil {
		log.WithError(err).Warn("hystrix error while getting automod cache entry")
	}

	if strings.Blank(automodCacheValue) {
		return nil
	}

	var automodBitsEvent api.CreateEventRequest
	err = json.Unmarshal([]byte(automodCacheValue), &automodBitsEvent)
	if err != nil {
		delErr := ac.Delete(ctx, userId)
		if delErr != nil {
			log.WithError(delErr).Error("AutomodCache: Error deleting a record")
		}
		log.WithError(err).Error("AutoModCache: Error unmarshaling BitsEvent with bad words")
		return nil
	}

	return &automodBitsEvent
}

func (ac *AutoModCache) Set(ctx context.Context, userId string, req api.CreateEventRequest) error {
	bitsEventJson, err := json.Marshal(req)
	if err != nil {
		log.WithError(err).Error("AutoModCache: Error marshaling BitsEvent")
		return err
	}

	err = hystrix.Do(cmds.AutomodCacheSet, func() error {
		err := ac.RedisClient.Set(ctx, fmt.Sprintf(automodKeyFormat, userId), string(bitsEventJson), automodCacheTTL).Err()
		if err != nil {
			log.WithError(err).Error("AutoModCache: Error setting BitsEvent with bad words in cache")
		}
		return err
	}, nil)

	return err
}

func (ac *AutoModCache) Delete(ctx context.Context, userId string) error {
	err := hystrix.Do(cmds.AutomodCacheDel, func() error {
		err := ac.RedisClient.Del(ctx, fmt.Sprintf(automodKeyFormat, userId)).Err()
		if err != nil {
			log.WithError(err).Error("AutoModCache: Error deleting BitsEvent with bad words from cache")
		}
		return err
	}, nil)

	return err
}
