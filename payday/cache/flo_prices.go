package cache

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/clients/redis"
	"code.justin.tv/commerce/payday/hystrix"
	cmds "code.justin.tv/commerce/payday/hystrix"
	"code.justin.tv/commerce/payday/models"
	util "code.justin.tv/commerce/payday/utils/close"
	"code.justin.tv/commerce/payday/utils/strings"
	go_redis "github.com/go-redis/redis"
)

type FloPricesCache interface {
	Get(ctx context.Context, productIds []string, userId string, geolocatedCountry string) ([]models.Price, *string)
	Set(ctx context.Context, userId string, prices []models.Price, floCountry, geolocatedCountry string)
	Delete(ctx context.Context, userId string, sku string, cor string)
}

type floPricesCache struct {
	RedisClient redis.Client `inject:"redisClient"`
}

const (
	floUserCountryCacheString = "%s.flo.country"
	floUserCacheString        = "%s.prices.%s.cor%s.flo"
	floCacheString            = "%s.prices.cor%s.flo"
)

func NewFloPricesCache() FloPricesCache {
	return &floPricesCache{}
}

func (pc *floPricesCache) Get(ctx context.Context, productIds []string, userId string, geolocatedCountry string) ([]models.Price, *string) {
	prices := make([]models.Price, 0)
	cachedSku := make(map[string]*go_redis.StringCmd)
	var cachedFloCountry *go_redis.StringCmd

	pipe := pc.RedisClient.Pipeline(ctx)
	defer util.Close(pipe)
	err := hystrix.Do(cmds.FloPricesCacheGet, func() error {
		for _, productId := range productIds {
			pricesCache := pipe.Get(getFloCacheString(userId, productId, geolocatedCountry))
			cachedSku[productId] = pricesCache
		}

		if strings.NotBlank(userId) {
			cachedFloCountry = pipe.Get(getFloCountryString(userId))
		}

		_, err := pipe.Exec()
		if err != nil && err != go_redis.Nil {
			log.WithError(err).Error("FloPricesCache: Error executing redis pipeline")
		}
		return nil
	}, nil)
	if err != nil {
		log.WithError(err).Warn("hystrix error while fetching flo prices from cache")
	}

	for _, productId := range productIds {
		val := cachedSku[productId]
		if val == nil {
			pc.Delete(ctx, userId, productId, geolocatedCountry)
			continue
		}

		pricesValue := cachedSku[productId].Val()

		if pricesValue == "" {
			return []models.Price{}, nil
		}

		var price models.Price
		err := json.Unmarshal([]byte(pricesValue), &price)
		if err != nil {
			pc.Delete(ctx, userId, productId, geolocatedCountry)
			log.WithField("pricesValue", pricesValue).WithError(err).Error("FloPricesCache: Error unmarshalling")
			return []models.Price{}, nil
		}

		prices = append(prices, price)
	}

	var floCountry string
	if cachedFloCountry != nil {
		floCountry = cachedFloCountry.Val()
	}
	return prices, &floCountry
}

func (pc *floPricesCache) Set(ctx context.Context, userId string, prices []models.Price, floCountry, geolocatedCountry string) {
	pipe := pc.RedisClient.Pipeline(ctx)
	defer util.Close(pipe)
	err := hystrix.Do(cmds.FloPricesCacheSet, func() error {
		for _, price := range prices {
			priceValue, err := json.Marshal(price)
			if err != nil {
				log.WithError(err).Error("FloPricesCache: Error marshling models.Price")
				return nil
			}

			pipe.Set(getFloCacheString(userId, price.ProductId, geolocatedCountry), priceValue, 5*time.Minute)
		}

		if strings.NotBlank(userId) {
			pipe.Set(getFloCountryString(userId), floCountry, 5*time.Minute)
		}

		_, err := pipe.Exec()
		if err != nil && err != go_redis.Nil {
			log.WithError(err).Error("FloPricesCache: Error executing redis pipeline")
		}
		return nil
	}, nil)

	if err != nil {
		log.WithError(err).Warn("hystrix error while setting flo prices in cache")
	}
}

func (pc *floPricesCache) Delete(ctx context.Context, userId string, productId string, geolocatedCountry string) {
	if strings.Blank(productId) {
		return
	}

	pipe := pc.RedisClient.Pipeline(ctx)
	defer util.Close(pipe)
	err := hystrix.Do(cmds.FloPricesCacheDel, func() error {
		pipe.Del(getFloCacheString(userId, productId, geolocatedCountry))
		pipe.Del(getFloCountryString(userId))

		_, err := pipe.Exec()
		if err != nil {
			log.WithError(err).Error("FloPricesCache: Error executing redis pipeline on delete")
		}
		return nil
	}, nil)
	if err != nil {
		log.WithError(err).Warn("hystrix error while deleting flo prices in cache")
	}
}

func getFloCountryString(userId string) string {
	return fmt.Sprintf(floUserCountryCacheString, userId)
}

func getFloCacheString(userId, productId, geolocatedCountry string) string {
	if strings.Blank(userId) {
		return fmt.Sprintf(floCacheString, productId, geolocatedCountry)
	} else {
		return fmt.Sprintf(floUserCacheString, userId, productId, geolocatedCountry)
	}
}
