package cache

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/clients/redis"
	"code.justin.tv/commerce/payday/hystrix"
	cmds "code.justin.tv/commerce/payday/hystrix"
	"code.justin.tv/commerce/payday/utils/strings"
	go_redis "github.com/go-redis/redis"
)

const (
	earnedBadgesCacheString = "earnedBadges.%s.user.%s.badgeId"
	oneWeek                 = 7 * 24 * time.Hour
)

type earnedBadge struct {
	UserID  string `json:"user_id"`
	BadgeID string `json:"badge_id"`
}

type IEarnedBadgesCache interface {
	Get(ctx context.Context, userID string, badgeID string) *earnedBadge
	Set(ctx context.Context, userID string, badgeID string)
}

type EarnedBadgesCache struct {
	RedisClient redis.Client `inject:"redisClient"`
}

func getBadgeKey(userID string, badgeID string) string {
	return fmt.Sprintf(earnedBadgesCacheString, userID, badgeID)
}

func (ebc *EarnedBadgesCache) Get(ctx context.Context, userID string, badgeID string) *earnedBadge {
	var badgeCacheValue string
	err := hystrix.Do(cmds.EarnedBadgesCacheGet, func() error {
		var err error
		badgeCacheValue, err = ebc.RedisClient.Get(ctx, getBadgeKey(userID, badgeID)).Result()
		if err != nil && err != go_redis.Nil {
			log.WithError(err).Error("EarnedBadgesCache: Error getting badges out of cache")
		}
		return nil
	}, nil)
	if err != nil {
		log.WithError(err).Warn("hystrix error while fetching earned badges from cache")
	}

	if strings.Blank(badgeCacheValue) || err == go_redis.Nil {
		return nil
	}

	var earnedBadgeResponse earnedBadge
	err = json.Unmarshal([]byte(badgeCacheValue), &earnedBadgeResponse)
	if err != nil {
		log.WithError(err).Error("EarnedBadgesCache: Error unmarshling badges")
		return nil
	}

	return &earnedBadgeResponse
}

func (ebc *EarnedBadgesCache) Set(ctx context.Context, userID string, badgeID string) {
	if strings.Blank(userID) || strings.Blank(badgeID) {
		return
	}

	badge := earnedBadge{
		UserID:  userID,
		BadgeID: badgeID,
	}

	badgeJson, err := json.Marshal(badge)
	if err != nil {
		log.WithError(err).Error("EarnedBadgesCache: Error marshling badge")
		return
	}

	err = hystrix.Do(cmds.EarnedBadgesCacheSet, func() error {
		err := ebc.RedisClient.Set(ctx, fmt.Sprintf(earnedBadgesCacheString, userID, badgeID), string(badgeJson), oneWeek).Err()
		if err != nil {
			log.WithError(err).Error("EarnedBadgesCache: Error setting badge in cache")
		}
		return nil
	}, nil)
	if err != nil {
		log.WithError(err).Warn("hystrix error while setting earned badges in cache")
	}
}
