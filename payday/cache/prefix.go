package cache

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/clients/prefixes"
	"code.justin.tv/commerce/payday/clients/redis"
	"code.justin.tv/commerce/payday/hystrix"
	cmds "code.justin.tv/commerce/payday/hystrix"
	"code.justin.tv/commerce/payday/utils/strings"
	go_redis "github.com/go-redis/redis"
)

const (
	prefixesKeyFormat = "channel-prefixes-%s"
	prefixesCacheTTL  = time.Hour
)

type IPrefixesCache interface {
	Get(ctx context.Context, channelId string) *prefixes.PrefixesResponse
	Set(ctx context.Context, channelId string, prefixesResponse prefixes.PrefixesResponse)
	Delete(ctx context.Context, channelId string)
}

type PrefixesCache struct {
	RedisClient redis.Client `inject:"redisClient"`
}

func (pc *PrefixesCache) Get(ctx context.Context, channelId string) *prefixes.PrefixesResponse {
	var prefixCacheValue string
	err := hystrix.Do(cmds.PrefixCacheGet, func() error {
		var err error
		prefixCacheValue, err = pc.RedisClient.Get(ctx, fmt.Sprintf(prefixesKeyFormat, channelId)).Result()
		if err != nil && err != go_redis.Nil {
			log.Error("PrefixesCache: Error getting Prefixes out of cache", err)
		}
		return nil
	}, nil)
	if err != nil {
		log.WithError(err).Warn("hystrix error while fetching prefixes from cache")
	}

	if strings.Blank(prefixCacheValue) {
		return nil
	}

	var prefixesResponse prefixes.PrefixesResponse
	err = json.Unmarshal([]byte(prefixCacheValue), &prefixesResponse)
	if err != nil {
		pc.Delete(ctx, channelId)
		log.Error("PrefixesCache: Error unmarshling prefixes", err)
		return nil
	}

	return &prefixesResponse
}

func (pc *PrefixesCache) Set(ctx context.Context, channelId string, prefixesResponse prefixes.PrefixesResponse) {
	prefixesJson, err := json.Marshal(prefixesResponse)
	if err != nil {
		log.Error("PrefixesCache: Error marshling prefixes", err)
		return
	}

	err = hystrix.Do(cmds.PrefixCacheSet, func() error {
		err := pc.RedisClient.Set(ctx, fmt.Sprintf(prefixesKeyFormat, channelId), string(prefixesJson), prefixesCacheTTL).Err()
		if err != nil {
			log.Error("PrefixesCache: Error setting prefixes in cache", err)
		}
		return nil
	}, nil)

	if err != nil {
		log.WithError(err).Warn("hystrix error while setting prefixes in cache")
	}
}

func (pc *PrefixesCache) Delete(ctx context.Context, channelId string) {
	err := hystrix.Do(cmds.PrefixCacheDel, func() error {
		err := pc.RedisClient.Del(ctx, fmt.Sprintf(prefixesKeyFormat, channelId)).Err()
		if err != nil {
			log.Error("PrefixesCache: Error deleting prefixes from cache", err)
		}
		return nil
	}, nil)

	if err != nil {
		log.WithError(err).Warn("hystrix error while deleting prefixes from cache")
	}
}
