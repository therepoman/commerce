package user

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"code.justin.tv/commerce/gogogadget/strings"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/clients/redis"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/hystrix"
	cmds "code.justin.tv/commerce/payday/hystrix"
	go_redis "github.com/go-redis/redis"
)

const (
	dynamoUserCacheKeyFormat  = "dynamo-user-%s"
	dynamoUserServiceCacheTTL = time.Minute * 5
)

type Cache interface {
	Get(ctx context.Context, userID string) *dynamo.User
	Set(ctx context.Context, userID string, user *dynamo.User)
	Del(ctx context.Context, userID string)
}

func NewCache() Cache {
	return &cache{}
}

type cache struct {
	RedisClient redis.Client `inject:"redisClient"`
}

func (c *cache) Get(ctx context.Context, userID string) *dynamo.User {
	var cacheValue string
	err := hystrix.Do(cmds.UserCacheGet, func() error {
		var err error
		cacheValue, err = c.RedisClient.Get(ctx, fmt.Sprintf(dynamoUserCacheKeyFormat, userID)).Result()
		if err != nil && err != go_redis.Nil {
			log.WithError(err).Error("Error getting dynamo user out of cache")
		}
		return nil
	}, nil)
	if err != nil {
		log.WithError(err).Warn("hystrix error while fetching dynamo user from cache")
	}

	if strings.Blank(cacheValue) {
		return nil
	}

	user := &dynamo.User{}
	err = json.Unmarshal([]byte(cacheValue), user)
	if err != nil {
		c.Del(ctx, userID)
		log.WithError(err).Error("Error unmarshalling dynamo user")
		return nil
	}

	return user
}

func (c *cache) Set(ctx context.Context, userID string, user *dynamo.User) {
	userJson, err := json.Marshal(user)
	if err != nil {
		log.WithError(err).Error("Error marshaling dynamo user")
		return
	}

	err = hystrix.Do(cmds.UserCacheSet, func() error {
		err := c.RedisClient.Set(ctx, fmt.Sprintf(dynamoUserCacheKeyFormat, userID), string(userJson), dynamoUserServiceCacheTTL).Err()
		if err != nil {
			log.WithError(err).Error("Error setting dynamo user in cache")
		}
		return nil
	}, nil)

	if err != nil {
		log.WithError(err).Warn("hystrix error while setting dynamo user in cache")
	}
}

func (c *cache) Del(ctx context.Context, userID string) {
	err := hystrix.Do(cmds.UserCacheDel, func() error {
		err := c.RedisClient.Del(ctx, fmt.Sprintf(dynamoUserCacheKeyFormat, userID)).Err()
		if err != nil {
			log.WithError(err).Error("Error deleting dynamo user from cache")
		}
		return nil
	}, nil)
	if err != nil {
		log.WithError(err).Warn("hystrix error while deleting dynamo user from cache")
	}
}
