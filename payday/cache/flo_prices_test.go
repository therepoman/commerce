package cache

import (
	"context"
	"encoding/json"
	"testing"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/clients/redis"
	"code.justin.tv/commerce/payday/models"
	. "github.com/smartystreets/goconvey/convey"
)

func TestFloCache_CacheKey(t *testing.T) {
	Convey("Given a function to generate cache strings", t, func() {

		Convey("When not given an id", func() {
			actual := getFloCacheString("", "mega-sku", "DE")
			So(actual, ShouldEqual, "mega-sku.prices.corDE.flo")
		})
		Convey("Given a user id", func() {
			actual := getFloCacheString("bonta", "super-sku", "US")
			So(actual, ShouldEqual, "bonta.prices.super-sku.corUS.flo")
		})
	})
}

func TestFloCacheImpl_Get(t *testing.T) {
	ctx := context.Background()
	client, server := redis.TestRedisClient()
	defer server.Close()

	cache := &floPricesCache{
		RedisClient: client,
	}

	userId := "116076154"
	productId := "B11381138"
	cor := "US"

	Convey("Given a cached entry for the user", t, func() {
		cacheKey := getFloCacheString(userId, productId, cor)
		price := models.Price{
			ProductId:    productId,
			Price:        "1.40",
			Currency:     "USD",
			TaxInclusive: false,
		}
		priceBytes, err := json.Marshal(price)
		if err != nil {
			log.WithError(err).Error("Error marshaling price")
			t.Fail()
		}

		Convey("Given a record exists in the cache", func() {
			client.Set(ctx, cacheKey, string(priceBytes), 5*time.Minute)

			Convey("It should return the correct record", func() {
				actual, country := cache.Get(ctx, []string{productId}, userId, cor)

				So(actual, ShouldNotBeNil)
				So(actual, ShouldHaveLength, 1)
				So(actual[0].ProductId, ShouldEqual, productId)
				So(*country, ShouldEqual, "")
			})

			Convey("Given that the country is set in the cache too", func() {
				client.Set(ctx, getFloCountryString(userId), "NZ", 5*time.Minute)

				Convey("It should return the the known country", func() {
					_, country := cache.Get(ctx, []string{productId}, userId, cor)
					So(*country, ShouldEqual, "NZ")
				})
			})
		})

		Convey("Given the record does not exist in the cache", func() {
			Convey("It should return nil", func() {
				actual, _ := cache.Get(ctx, []string{"im-missing"}, userId, cor)
				So(actual, ShouldHaveLength, 0)
			})
		})

		Convey("Given the record has a nil entry in the cache", func() {
			productId = "badProductBoo"
			nilEntryKey := getFloCacheString(userId, productId, cor)
			err = client.Set(ctx, nilEntryKey, nil, 5*time.Minute).Err()
			So(err, ShouldBeNil)

			Convey("It should return nil and delete the offending key", func() {
				actual, _ := cache.Get(ctx, []string{productId}, userId, cor)
				So(actual, ShouldHaveLength, 0)
				// key should be deleted
				val, err := client.Get(ctx, nilEntryKey).Result()
				So(err, ShouldBeNil)
				So(val, ShouldBeBlank)
			})
		})

		Convey("Given the record is not another type", func() {
			badTypeKey := getFloCacheString(userId, productId, cor+".bad")
			client.Set(ctx, badTypeKey, string(priceBytes[5:]), 5*time.Minute)

			Convey("It should return nil and delete", func() {
				actual, _ := cache.Get(ctx, []string{productId}, userId, cor+".bad")
				So(actual, ShouldHaveLength, 0)
				_, err := client.Get(ctx, badTypeKey).Result()
				So(err, ShouldNotBeNil)
			})
		})
	})
}
