package cache

import (
	"context"
	"testing"

	"code.justin.tv/commerce/payday/clients/redis"
	. "github.com/smartystreets/goconvey/convey"
)

func TestLast24Hours_CacheKey(t *testing.T) {
	Convey("Given a function to generate cache strings", t, func() {

		Convey("When not given an id", func() {
			actual := last24HoursKey("")
			So(actual, ShouldEqual, "bits-last-24-hours-acquired-")
		})
		Convey("Given a user id", func() {
			actual := last24HoursKey("488")
			So(actual, ShouldEqual, "bits-last-24-hours-acquired-488")
		})
	})
}

func TestLast24Hours_Caching(t *testing.T) {
	ctx := context.Background()
	userId := "46689"
	numOfBits := int64(48)
	client, server := redis.TestRedisClient()
	defer server.Close()

	Convey("Given a cache", t, func() {
		cache := last24HoursAcquiredCache{
			RedisClient: client,
		}

		Convey("get with nothing in the cache", func() {
			balance := cache.Get(ctx, userId)
			So(balance, ShouldBeNil)
		})

		Convey("putting balance into cache", func() {
			cache.Set(ctx, userId, numOfBits)
			balance := cache.Get(ctx, userId)
			So(balance, ShouldNotBeNil)
			So(*balance, ShouldEqual, numOfBits)

			Convey("deleting from balance cache", func() {
				cache.Del(ctx, userId)
				balance := cache.Get(ctx, userId)
				So(balance, ShouldBeNil)
			})
		})

		Convey("putting zero as balance into cache is valid", func() {
			cache.Set(ctx, userId, 0)
			balance := cache.Get(ctx, userId)
			So(balance, ShouldNotBeNil)
			So(*balance, ShouldEqual, 0)
		})
	})
}
