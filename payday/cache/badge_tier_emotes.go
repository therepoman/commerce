package cache

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"code.justin.tv/commerce/gogogadget/strings"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/clients/redis"
	"code.justin.tv/commerce/payday/dynamo/badge_tier_emote_groupids"
	"code.justin.tv/commerce/payday/hystrix"
	cmds "code.justin.tv/commerce/payday/hystrix"
	go_redis "github.com/go-redis/redis"
)

type BadgeTierEmotesCache interface {
	Get(ctx context.Context, channelId string) []*badge_tier_emote_groupids.BadgeTierEmoteGroupID
	Set(ctx context.Context, channelId string, badgeTierEmotes []*badge_tier_emote_groupids.BadgeTierEmoteGroupID)
	Del(ctx context.Context, channelId string)
}

type badgeTierEmotesCache struct {
	RedisClient redis.Client `inject:"redisClient"`
}

const (
	badgeTierEmotesCacheString = "%s.badgeTierEmotes"
	fiveMinutes                = time.Minute * 5
)

func NewBadgeTierEmotesCache() BadgeTierEmotesCache {
	return &badgeTierEmotesCache{}
}

func (bec badgeTierEmotesCache) Get(ctx context.Context, channelId string) []*badge_tier_emote_groupids.BadgeTierEmoteGroupID {
	var cacheValue string
	err := hystrix.Do(cmds.BadgeTierEmotesCacheGet, func() error {
		var err error
		cacheValue, err = bec.RedisClient.Get(ctx, badgeTierEmotesKey(channelId)).Result()
		if err != nil && err != go_redis.Nil {
			return err
		}
		return nil
	}, nil)
	if err != nil && err != go_redis.Nil {
		log.WithError(err).Error("Failed to get bits badge tier emotes in Redis")
		return nil
	}

	if strings.Blank(cacheValue) || err == go_redis.Nil {
		return nil // not in cache
	}

	var badgeTierEmotes []*badge_tier_emote_groupids.BadgeTierEmoteGroupID
	err = json.Unmarshal([]byte(cacheValue), &badgeTierEmotes)
	if err != nil {
		log.WithError(err).Error("Failed to unmarshal bits badge tier emotes into JSON")
		return nil
	}

	return badgeTierEmotes
}

func (bec badgeTierEmotesCache) Set(ctx context.Context, channelId string, badgeTierEmotes []*badge_tier_emote_groupids.BadgeTierEmoteGroupID) {
	if strings.Blank(channelId) {
		return
	}

	// we want to cache an empty array if there is nothing configured, so that we know there is nothing configured purely off cache data.
	if badgeTierEmotes == nil {
		badgeTierEmotes = []*badge_tier_emote_groupids.BadgeTierEmoteGroupID{}
	}

	badgeTierEmotesJson, err := json.Marshal(badgeTierEmotes)
	if err != nil {
		log.WithError(err).Error("Failed to marshal bits badge tier emotes into JSON")
		return
	}

	err = hystrix.Do(cmds.BadgeTierEmotesCacheSet, func() error {
		err := bec.RedisClient.Set(ctx, badgeTierEmotesKey(channelId), string(badgeTierEmotesJson), fiveMinutes).Err()
		if err != nil {
			return err
		}
		return nil
	}, nil)

	if err != nil {
		log.WithError(err).Error("Failed to set bits badge tier emotes in Redis")
	}
}

func (bec badgeTierEmotesCache) Del(ctx context.Context, channelId string) {
	err := hystrix.Do(cmds.BadgeTierEmotesCacheDel, func() error {
		return bec.RedisClient.Del(ctx, badgeTierEmotesKey(channelId)).Err()
	}, nil)

	if err != nil {
		log.WithError(err).Error("Failed to delete bits badge tier emotes from Redis")
	}
}

func badgeTierEmotesKey(channelId string) string {
	return fmt.Sprintf(badgeTierEmotesCacheString, channelId)
}
