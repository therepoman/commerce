package cache

import (
	"context"
	"fmt"
	"strconv"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/clients/redis"
	go_redis "github.com/go-redis/redis"
)

const (
	last24HoursKeyFormat = "bits-last-24-hours-acquired-%s"
	last24HoursKeyTtl    = time.Minute * 15
)

type Last24HoursAcquiredCache interface {
	Set(ctx context.Context, userID string, quantityAcquired int64)
	Get(ctx context.Context, userID string) *int64
	Del(ctx context.Context, userID string)
}

type last24HoursAcquiredCache struct {
	RedisClient redis.Client `inject:"redisClient"`
}

func NewLast24HoursAcquiredCache() Last24HoursAcquiredCache {
	return &last24HoursAcquiredCache{}
}

func (l *last24HoursAcquiredCache) Set(ctx context.Context, userID string, quantityAcquired int64) {
	err := l.RedisClient.Set(ctx, last24HoursKey(userID), strconv.FormatInt(quantityAcquired, 10), last24HoursKeyTtl).Err()
	if err != nil {
		log.WithError(err).WithFields(log.Fields{
			"userID":           userID,
			"quantityAcquired": quantityAcquired,
		}).Error("Failed to set entry in last 24 hours acquired cache.")
	}
}

func (l *last24HoursAcquiredCache) Get(ctx context.Context, userID string) *int64 {
	result, err := l.RedisClient.Get(ctx, last24HoursKey(userID)).Result()
	if err != nil && err != go_redis.Nil {
		log.WithError(err).WithField("userID", userID).Error("Failed to get entry in last 24 hours acquired cache.")
		return nil
	}

	if err == go_redis.Nil {
		return nil
	}

	quantityAcquired, err := strconv.ParseInt(result, 10, 64)

	if err != nil {
		log.WithError(err).WithFields(log.Fields{
			"userID":     userID,
			"redisValue": result,
		}).Error("Failed to get entry in last 24 hours acquired cache.")
		return nil
	}

	return &quantityAcquired
}

func (l *last24HoursAcquiredCache) Del(ctx context.Context, userID string) {
	err := l.RedisClient.Del(ctx, last24HoursKey(userID)).Err()
	if err != nil {
		log.WithError(err).WithField("userID", userID).Error("Failed to delete entry in last 24 hours acquired cache.")
	}
}

func last24HoursKey(userID string) string {
	return fmt.Sprintf(last24HoursKeyFormat, userID)
}
