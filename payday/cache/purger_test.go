package cache

import (
	"context"
	"testing"

	"github.com/stretchr/testify/mock"

	cache_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/cache"
	products_purchase_by_most_recent_super_platform_cache_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/products/purchase_by_most_recent_super_platform/cache"
	products_single_purchase_consumption_cache_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/products/single_purchase_consumption/cache"
	. "github.com/smartystreets/goconvey/convey"
)

func TestPurger_BalancePurge(t *testing.T) {
	userId := "46689"

	Convey("Given a purger", t, func() {
		bc := new(cache_mock.IBalanceCache)
		spc := new(products_single_purchase_consumption_cache_mock.Cache)
		mrpc := new(products_purchase_by_most_recent_super_platform_cache_mock.Cache)
		mrac := new(cache_mock.MostRecentlyAcquiredCache)
		l2hrac := new(cache_mock.Last24HoursAcquiredCache)

		cachePurger := purger{
			BalanceCache:              bc,
			SinglePurchaseCache:       spc,
			MostRecentPlatformCache:   mrpc,
			MostRecentlyAcquiredCache: mrac,
			Last24HoursAcquiredCache:  l2hrac,
		}

		bc.On("Delete", mock.Anything, userId).Return()
		spc.On("Del", mock.Anything, userId).Return()
		mrpc.On("Del", mock.Anything, userId).Return()
		mrac.On("Del", mock.Anything, userId).Return()
		l2hrac.On("Del", mock.Anything, userId).Return()

		Convey("when given an id", func() {
			ctx := context.Background()
			cachePurger.BalanceUpdatePurge(ctx, userId)

			bc.AssertCalled(t, "Delete", ctx, userId)
			spc.AssertCalled(t, "Del", ctx, userId)
			mrpc.AssertCalled(t, "Del", ctx, userId)
			mrac.AssertCalled(t, "Del", ctx, userId)
			l2hrac.AssertCalled(t, "Del", ctx, userId)
		})
	})
}
