package cache

import (
	"context"
	"testing"

	"code.justin.tv/commerce/payday/clients/redis"
	"code.justin.tv/commerce/payday/dynamo/bits_entitlement"
	. "github.com/smartystreets/goconvey/convey"
)

func TestMostRecentAcquisition_CacheKey(t *testing.T) {
	Convey("Given a function to generate cache strings", t, func() {

		Convey("When not given an id", func() {
			actual := mostRecentlyAcquiredKey("")
			So(actual, ShouldEqual, "bits-most-recently-acquired-")
		})
		Convey("Given a user id", func() {
			actual := mostRecentlyAcquiredKey("8667")
			So(actual, ShouldEqual, "bits-most-recently-acquired-8667")
		})
	})
}

func TestMostRecentAcquisition_Caching(t *testing.T) {
	ctx := context.Background()
	userId := "46689"
	entitlement := bits_entitlement.BitsEntitlement{}

	client, server := redis.TestRedisClient()
	defer server.Close()

	Convey("Given a cache", t, func() {
		cache := mostRecentlyAcquiredCache{
			RedisClient: client,
		}

		Convey("get with nothing in the cache", func() {
			recentEntitlement, cached := cache.Get(ctx, userId)
			So(recentEntitlement, ShouldBeNil)
			So(cached, ShouldBeFalse)
		})

		Convey("putting entitlement into cache", func() {
			cache.Set(ctx, userId, &entitlement)
			recentEntitlement, cached := cache.Get(ctx, userId)
			So(recentEntitlement, ShouldNotBeNil)
			So(*recentEntitlement, ShouldResemble, entitlement)
			So(cached, ShouldBeTrue)

			Convey("deleting entitlement from cache", func() {
				cache.Del(ctx, userId)
				recentEntitlement, cached := cache.Get(ctx, userId)
				So(recentEntitlement, ShouldBeNil)
				So(cached, ShouldBeFalse)
			})
		})

		Convey("putting nil as entitlement is valid", func() {
			cache.Set(ctx, userId, nil)
			recentEntitlement, cached := cache.Get(ctx, userId)
			So(recentEntitlement, ShouldBeNil)
			So(cached, ShouldBeTrue)
		})
	})
}
