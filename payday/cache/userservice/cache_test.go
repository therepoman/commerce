package userservice

import (
	"context"
	"encoding/json"
	"fmt"
	"testing"

	"github.com/stretchr/testify/mock"

	log "code.justin.tv/commerce/logrus"
	redis_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/redis"
	"code.justin.tv/web/users-service/models"
	"github.com/go-redis/redis"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
)

func TestCache_Get(t *testing.T) {
	Convey("given a cache getter", t, func() {
		ctx := context.Background()
		redisClient := new(redis_mock.Client)

		cache := &cache{
			RedisClient: redisClient,
		}

		userID := "123123123"
		cacheKey := fmt.Sprintf(userServiceCacheKeyFormat, userID)

		Convey("when the redis client returns an error", func() {
			resp := redis.NewStringResult("", errors.New("ERROR"))
			redisClient.On("Get", mock.Anything, cacheKey).Return(resp)
			result := cache.Get(ctx, userID)

			Convey("we should return nil", func() {
				So(result, ShouldBeNil)
			})
		})

		Convey("when the redis client returns a blank value", func() {
			resp := redis.NewStringResult("", nil)
			redisClient.On("Get", mock.Anything, cacheKey).Return(resp)
			result := cache.Get(ctx, userID)

			Convey("we should return nil", func() {
				So(result, ShouldBeNil)
			})
		})

		Convey("when the redis client returns invalid JSON", func() {
			resp := redis.NewStringResult("this is invalid json &*!#(", nil)
			delResp := redis.NewIntCmd("del", nil)
			redisClient.On("Get", mock.Anything, cacheKey).Return(resp)
			redisClient.On("Del", mock.Anything, cacheKey).Return(delResp)

			result := cache.Get(ctx, userID)

			Convey("we should return nil", func() {
				So(result, ShouldBeNil)
			})

			Convey("we should delete the record from the cache", func() {
				redisClient.AssertCalled(t, "Del", mock.Anything, cacheKey)
			})
		})

		Convey("we should return user settings", func() {
			cachedValue := &models.Properties{
				ID: userID,
			}
			cachedBytes, err := json.Marshal(cachedValue)

			if err != nil {
				log.WithError(err).Error("Error marshaling settings")
				t.Fail()
			}

			resp := redis.NewStringResult(string(cachedBytes), nil)
			redisClient.On("Get", mock.Anything, cacheKey).Return(resp)

			returnValue := cache.Get(ctx, userID)

			So(returnValue, ShouldResemble, cachedValue)
		})
	})
}

func TestCache_Set(t *testing.T) {
	Convey("given a cache setter", t, func() {
		ctx := context.Background()
		redisClient := new(redis_mock.Client)

		cache := &cache{
			RedisClient: redisClient,
		}

		userID := "123123123"
		cacheKey := fmt.Sprintf(userServiceCacheKeyFormat, userID)

		Convey("when the redis client returns an error", func() {
			Convey("we should return nothing", func() {
				cachedValue := &models.Properties{
					ID: userID,
				}

				cachedBytes, err := json.Marshal(*cachedValue)

				if err != nil {
					log.WithError(err).Error("Error marshaling settings")
					t.Fail()
				}

				redisClient.On(
					"Set",
					mock.Anything,
					cacheKey,
					string(cachedBytes),
					userServiceCacheTTL).Return(redis.NewStatusCmd(errors.New("ERROR")))

				cache.Set(ctx, userID, cachedValue)

				redisClient.AssertCalled(
					t,
					"Set",
					ctx,
					cacheKey,
					string(cachedBytes),
					userServiceCacheTTL)
			})
		})

		Convey("when the redis client does not return an error", func() {
			Convey("we should return nothing", func() {
				cachedValue := &models.Properties{
					ID: userID,
				}

				cachedBytes, err := json.Marshal(*cachedValue)

				if err != nil {
					log.WithError(err).Error("Error marshaling settings")
					t.Fail()
				}

				redisClient.On(
					"Set",
					mock.Anything,
					cacheKey,
					string(cachedBytes),
					userServiceCacheTTL).Return(redis.NewStatusCmd(nil))

				cache.Set(ctx, userID, cachedValue)

				redisClient.AssertCalled(
					t,
					"Set",
					ctx,
					cacheKey,
					string(cachedBytes),
					userServiceCacheTTL)
			})
		})
	})
}

func TestCache_Del(t *testing.T) {
	Convey("given a cache deleter", t, func() {
		ctx := context.Background()
		redisClient := new(redis_mock.Client)

		cache := &cache{
			RedisClient: redisClient,
		}

		userID := "123123123"
		cacheKey := fmt.Sprintf(userServiceCacheKeyFormat, userID)

		Convey("when the redis client returns an error", func() {
			redisClient.On("Del", mock.Anything, cacheKey).Return(redis.NewIntCmd(errors.New("ERROR")))

			Convey("we should return nothing", func() {
				cache.Del(ctx, userID)
				redisClient.AssertCalled(t, "Del", ctx, cacheKey)
			})
		})

		Convey("when the redis client does not return an error", func() {
			redisClient.On("Del", mock.Anything, cacheKey).Return(redis.NewIntCmd(nil))

			Convey("we should return nothing", func() {
				cache.Del(ctx, userID)
				redisClient.AssertCalled(t, "Del", ctx, cacheKey)
			})
		})
	})
}
