package userservice

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"code.justin.tv/commerce/gogogadget/strings"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/clients/redis"
	"code.justin.tv/commerce/payday/hystrix"
	cmds "code.justin.tv/commerce/payday/hystrix"
	"code.justin.tv/web/users-service/models"
	go_redis "github.com/go-redis/redis"
)

const (
	userServiceCacheKeyFormat = "user-service-user-%s"
	userServiceCacheTTL       = time.Minute * 5
)

type Cache interface {
	Get(ctx context.Context, userID string) *models.Properties
	Set(ctx context.Context, userID string, user *models.Properties)
	Del(ctx context.Context, userID string)
}

func NewCache() Cache {
	return &cache{}
}

type cache struct {
	RedisClient redis.Client `inject:"redisClient"`
}

func (c *cache) Get(ctx context.Context, userID string) *models.Properties {
	var cacheValue string
	err := hystrix.Do(cmds.UserServiceCacheGet, func() error {
		var err error
		cacheValue, err = c.RedisClient.Get(ctx, fmt.Sprintf(userServiceCacheKeyFormat, userID)).Result()
		if err != nil && err != go_redis.Nil {
			log.WithError(err).Error("Error getting user service user out of cache")
		}
		return nil
	}, nil)
	if err != nil {
		log.WithError(err).Warn("hystrix error fetching user service user from cache")
	}

	if strings.Blank(cacheValue) {
		return nil
	}

	user := &models.Properties{}
	err = json.Unmarshal([]byte(cacheValue), user)
	if err != nil {
		c.Del(ctx, userID)
		log.WithError(err).Error("Error unmarshalling user service user")
		return nil
	}

	return user
}

func (c *cache) Set(ctx context.Context, userID string, user *models.Properties) {
	userJson, err := json.Marshal(user)
	if err != nil {
		log.WithError(err).Error("Error marshaling user service user")
		return
	}

	err = hystrix.Do(cmds.UserServiceCacheSet, func() error {
		err := c.RedisClient.Set(ctx, fmt.Sprintf(userServiceCacheKeyFormat, userID), string(userJson), userServiceCacheTTL).Err()
		if err != nil {
			log.WithError(err).Error("Error setting user service user in cache")
		}
		return nil
	}, nil)

	if err != nil {
		log.WithError(err).Warn("hystrix error setting user service user in cache")
	}
}

func (c *cache) Del(ctx context.Context, userID string) {
	err := hystrix.Do(cmds.UserServiceCacheDel, func() error {
		err := c.RedisClient.Del(ctx, fmt.Sprintf(userServiceCacheKeyFormat, userID)).Err()
		if err != nil {
			log.WithError(err).Error("Error deleting user service user from cache")
		}
		return nil
	}, nil)
	if err != nil {
		log.WithError(err).Warn("hystrix error deleting user service user from cache")
	}
}
