package cache

import (
	"context"
	"sync"

	most_recent_platform_cache "code.justin.tv/commerce/payday/products/purchase_by_most_recent_super_platform/cache"
	single_purchase_cache "code.justin.tv/commerce/payday/products/single_purchase_consumption/cache"
)

type Purger interface {
	BalanceUpdatePurge(ctx context.Context, userId string)
}

type purger struct {
	BalanceCache              IBalanceCache                    `inject:""`
	SinglePurchaseCache       single_purchase_cache.Cache      `inject:"singlePurchaseConsumptionCache"`
	MostRecentPlatformCache   most_recent_platform_cache.Cache `inject:""`
	MostRecentlyAcquiredCache MostRecentlyAcquiredCache        `inject:""`
	Last24HoursAcquiredCache  Last24HoursAcquiredCache         `inject:""`
}

func NewPurger() Purger {
	return &purger{}
}

func (p *purger) BalanceUpdatePurge(ctx context.Context, userId string) {
	var wg sync.WaitGroup
	wg.Add(5)

	// add a wait here to all of them being done.
	go func() {
		p.BalanceCache.Delete(ctx, userId)
		wg.Done()
	}()
	go func() {
		p.SinglePurchaseCache.Del(ctx, userId)
		wg.Done()
	}()
	go func() {
		p.MostRecentPlatformCache.Del(ctx, userId)
		wg.Done()
	}()
	go func() {
		p.MostRecentlyAcquiredCache.Del(ctx, userId)
		wg.Done()
	}()
	go func() {
		p.Last24HoursAcquiredCache.Del(ctx, userId)
		wg.Done()
	}()

	wg.Wait()
}
