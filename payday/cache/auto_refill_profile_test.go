package cache

import (
	"context"
	"encoding/json"
	"fmt"
	"testing"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/clients/redis"
	"code.justin.tv/commerce/payday/dynamo/auto_refill_settings"
	. "github.com/smartystreets/goconvey/convey"
)

func TestAutoRefillCacheImpl_Get(t *testing.T) {
	ctx := context.Background()
	client, server := redis.TestRedisClient()
	defer server.Close()

	cache := &autoRefillProfileCache{
		RedisClient: client,
	}

	userId := "116076154"
	cacheKey := fmt.Sprintf(autoReloadProfileTopic, userId)

	Convey("Given a cached entry for the user", t, func() {

		profile := auto_refill_settings.AutoRefillSettingsProfile{
			UserId: userId,
		}
		profileBytes, err := json.Marshal(profile)
		if err != nil {
			log.WithError(err).Error("Error marshaling profile")
			t.Fail()
		}

		Convey("Given a record exists in the cache", func() {
			client.Set(ctx, cacheKey, string(profileBytes), 5*time.Minute)

			Convey("It should return the correct record", func() {
				actual, found, err := cache.Get(ctx, userId)

				So(actual, ShouldNotBeNil)
				So(actual.UserId, ShouldEqual, userId)
				So(found, ShouldBeTrue)
				So(err, ShouldBeNil)
			})

		})

		Convey("Given the record does not exist in the cache", func() {
			Convey("It should return nil", func() {
				actual, found, err := cache.Get(ctx, "not in cache")
				So(actual, ShouldBeNil)
				So(found, ShouldBeFalse)
				So(err, ShouldBeNil)
			})
		})
	})
}
