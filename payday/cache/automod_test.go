package cache

import (
	"context"
	"encoding/json"
	"fmt"
	"testing"

	"github.com/stretchr/testify/mock"

	log "code.justin.tv/commerce/logrus"
	redis_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/redis"
	"code.justin.tv/commerce/payday/models/api"
	"github.com/go-redis/redis"
	. "github.com/smartystreets/goconvey/convey"
)

func TestAutoMod(t *testing.T) {
	Convey("Given an automod cache", t, func() {
		ctx := context.Background()
		redisClient := new(redis_mock.Client)

		autoModCache := &AutoModCache{
			RedisClient: redisClient,
		}

		userID := "219219219"
		cacheKey := fmt.Sprintf(automodKeyFormat, userID)

		bitsEvent := api.CreateEventRequest{
			UserId:    userID,
			ChannelId: "abc",
			Amount:    1,
			Message:   "cheer1",
			EventId:   "def",
		}

		bitsEventBytes, err := json.Marshal(bitsEvent)
		if err != nil {
			log.WithError(err).Error("Error marshaling BitsEvent")
			t.Fail()
		}

		redisResp := redis.NewStringResult(string(bitsEventBytes), nil)

		Convey("Testing Get", func() {
			redisClient.On("Get", mock.Anything, cacheKey).Return(redisResp)

			Convey("Should return the record", func() {
				actual := autoModCache.Get(ctx, userID)
				So(actual, ShouldNotBeNil)
				So(actual.EventId, ShouldEqual, bitsEvent.EventId)
			})
		})

		Convey("Testing Set", func() {
			redisClient.On("Set", mock.Anything, cacheKey, string(bitsEventBytes), automodCacheTTL).Return(&redis.StatusCmd{})

			Convey("Should successfully set the record", func() {
				err := autoModCache.Set(ctx, userID, bitsEvent)
				So(err, ShouldBeNil)
			})
		})
	})
}
