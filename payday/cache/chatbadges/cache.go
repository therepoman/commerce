package chatbadges

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"code.justin.tv/commerce/gogogadget/strings"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/badgetiers/models"
	"code.justin.tv/commerce/payday/clients/redis"
	"code.justin.tv/commerce/payday/hystrix"
	cmds "code.justin.tv/commerce/payday/hystrix"
	go_redis "github.com/go-redis/redis"
)

const (
	chatbadgesCacheKeyFormat = "chatbadges-%s"
	chatbadgesCacheTTL       = time.Minute * 5
)

type Cache interface {
	Get(ctx context.Context, channelID string) *models.BitsChatBadgeTiers
	Set(ctx context.Context, channelID string, user *models.BitsChatBadgeTiers)
	Del(ctx context.Context, channelID string)
}

func NewCache() Cache {
	return &cache{}
}

type cache struct {
	RedisClient redis.Client `inject:"redisClient"`
}

func (c *cache) Get(ctx context.Context, channelID string) *models.BitsChatBadgeTiers {
	var cacheValue string
	err := hystrix.Do(cmds.ChatBadgesCacheGet, func() error {
		var err error
		cacheValue, err = c.RedisClient.Get(ctx, fmt.Sprintf(chatbadgesCacheKeyFormat, channelID)).Result()
		if err != nil && err != go_redis.Nil {
			log.WithError(err).Error("Error getting channel chatbadge tiers")
		}
		return nil
	}, nil)
	if err != nil {
		log.WithError(err).Warn("hystrix error fetching chat badges from cache")
	}

	if strings.Blank(cacheValue) {
		return nil
	}

	chatBadgeTiers := &models.BitsChatBadgeTiers{}
	err = json.Unmarshal([]byte(cacheValue), chatBadgeTiers)
	if err != nil {
		c.Del(ctx, channelID)
		log.WithError(err).Error("Error unmarshalling channel chatbadge tiers")
		return nil
	}

	return chatBadgeTiers
}

func (c *cache) Set(ctx context.Context, channelID string, user *models.BitsChatBadgeTiers) {
	chatbadgeTiersJson, err := json.Marshal(user)
	if err != nil {
		log.WithError(err).Error("Error marshaling channel chatbadge tiers")
		return
	}

	err = hystrix.Do(cmds.ChatBadgesCacheSet, func() error {
		err := c.RedisClient.Set(ctx, fmt.Sprintf(chatbadgesCacheKeyFormat, channelID), string(chatbadgeTiersJson), chatbadgesCacheTTL).Err()
		if err != nil {
			log.WithError(err).Error("Error setting channel chatbadge tiers in cache")
		}
		return nil
	}, nil)
	if err != nil {
		log.WithError(err).Warn("hystrix error setting chat badges in cache")
	}
}

func (c *cache) Del(ctx context.Context, channelID string) {
	err := hystrix.Do(cmds.ChatBadgesCacheDel, func() error {
		err := c.RedisClient.Del(ctx, fmt.Sprintf(chatbadgesCacheKeyFormat, channelID)).Err()
		if err != nil {
			log.WithError(err).Error("Error deleting channel chatbadge tiers from cache")
		}
		return nil
	}, nil)
	if err != nil {
		log.WithError(err).Warn("hystrix error deleting chat badges from cache")
	}
}
