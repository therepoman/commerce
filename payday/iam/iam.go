package iam

import (
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/errors"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/client"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/iam"
)

const defaultRegion = "us-west-2"

type IAMClient struct {
	iam *iam.IAM
}

func NewDefaultConfig() *aws.Config {
	return &aws.Config{
		Region: aws.String(defaultRegion),
	}
}

func New(provider client.ConfigProvider, config *aws.Config) *IAMClient {
	return &IAMClient{
		iam: iam.New(provider, config),
	}
}

func NewFromConfig(iamConfig *aws.Config) *IAMClient {
	sess, _ := session.NewSession()
	return New(sess, iamConfig)
}

func NewFromDefaultConfig() *IAMClient {
	return NewFromConfig(NewDefaultConfig())
}

func (c *IAMClient) GetRoleArn(roleName string) (string, error) {
	out, err := c.iam.GetRole(&iam.GetRoleInput{
		RoleName: aws.String(roleName),
	})

	if err != nil {
		log.WithField("roleName", roleName).Error("Error looking up AWS role")
		return "", err
	}

	if out != nil && out.Role != nil && out.Role.Arn != nil {
		return *out.Role.Arn, nil
	} else {
		msg := "No role found"
		log.WithField("roleName", roleName).Error(msg)
		return "", errors.New(msg)
	}
}

func (c *IAMClient) GetCurrentUser() (string, error) {
	out, err := c.iam.GetUser(&iam.GetUserInput{})
	if err != nil {
		return "", err
	}

	if out == nil || out.User == nil || out.User.UserName == nil {
		return "", nil
	}
	return *out.User.Arn, nil
}
