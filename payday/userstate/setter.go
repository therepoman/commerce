package userstate

import (
	"context"

	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/models/api"
	"code.justin.tv/commerce/payday/user"
)

type Setter interface {
	Set(ctx context.Context, tuid string, userState api.UserState) error
}

func NewSetter() Setter {
	return &setter{}
}

type setter struct {
	Fetcher    user.Fetcher `inject:""`
	UserSetter user.Setter  `inject:""`
}

func (s *setter) Set(ctx context.Context, tuid string, userState api.UserState) error {
	userRecord, err := s.Fetcher.Fetch(ctx, tuid)
	if err != nil {
		return err
	}

	if userRecord == nil {
		userRecord = &dynamo.User{
			Id: dynamo.UserId(tuid),
		}
	}

	userRecord.UserState = userState.ToString()

	err = s.UserSetter.Set(ctx, tuid, userRecord)
	if err != nil {
		return err
	}

	return nil
}
