package userstate

import (
	"testing"

	userstate_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/userstate"
	"code.justin.tv/commerce/payday/models/api"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestGetter_Get(t *testing.T) {
	Convey("given a user state getter", t, func() {
		fetcher := new(userstate_mock.Fetcher)
		searcher := new(userstate_mock.Searcher)
		setter := new(userstate_mock.Setter)

		getter := &getter{
			Fetcher:  fetcher,
			Searcher: searcher,
			Setter:   setter,
		}

		tuid := "123123123"

		Convey("when the searcher fails", func() {
			fetcher.On("Fetch", mock.Anything, tuid).Return(api.UserStateUnknown, errors.New("WALRUS STRIKE"))

			Convey("then we return an error", func() {
				_, err := getter.Get(tuid)

				So(err, ShouldNotBeNil)
			})
		})

		Convey("when the searcher succeeds", func() {
			Convey("when we have a user state set for the queried user", func() {
				fetcher.On("Fetch", mock.Anything, tuid).Return(api.UserStateCheered, nil)

				Convey("then we return the stored user state", func() {
					state, err := getter.Get(tuid)

					So(err, ShouldBeNil)
					So(state, ShouldEqual, api.UserStateCheered)
				})
			})

			Convey("when we do not have the user state determined for the queried user", func() {
				fetcher.On("Fetch", mock.Anything, tuid).Return(api.UserStateUnknown, nil)

				Convey("when the searcher fails", func() {
					searcher.On("Search", mock.Anything, tuid).Return(api.UserStateUnknown, errors.New("WALRUS STRIKE"))

					Convey("then we return an error", func() {
						_, err := getter.Get(tuid)

						So(err, ShouldNotBeNil)
					})
				})

				Convey("when the searcher succeeds", func() {
					searcher.On("Search", mock.Anything, tuid).Return(api.UserStateNew, nil)

					Convey("when the setter fails", func() {
						setter.On("Set", mock.Anything, tuid, api.UserStateNew).Return(errors.New("WALRUS STRIKE"))

						Convey("then we still return the user state calculated", func() {
							state, err := getter.Get(tuid)

							So(err, ShouldBeNil)
							So(state, ShouldEqual, api.UserStateNew)
						})
					})

					Convey("when the setter succeeds", func() {
						setter.On("Set", mock.Anything, tuid, api.UserStateNew).Return(nil)

						Convey("then we return the searched user state", func() {
							state, err := getter.Get(tuid)

							So(err, ShouldBeNil)
							So(state, ShouldEqual, api.UserStateNew)
						})
					})
				})
			})
		})
	})
}
