package usercheered

import (
	"context"
	"testing"
	"time"

	"code.justin.tv/commerce/payday/dynamo"
	transactions_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/transactions"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/models/api"
	"code.justin.tv/commerce/payday/userstate/search"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestSearcher_Search(t *testing.T) {
	Convey("given a allcheeredbits Searcher", t, func() {
		transactionsFetcher := new(transactions_mock.Fetcher)
		const tuid = "12345"

		expectedLimit := int64(1)
		expectedDescending := false
		var expectedBefore *time.Time
		var expectedAfter *time.Time

		searcher := &searcher{
			TransactionsFetcher: transactionsFetcher,
		}

		Convey("when client errors", func() {
			transactionsFetcher.On("FetchTransactionsByUserIDAndTransactionType", mock.Anything, tuid, models.BitsEventTransactionType, expectedLimit, expectedDescending, expectedBefore, expectedAfter).Return(nil, errors.New("WALRUS STRIKE"))

			Convey("then we return an error", func() {
				ctx := context.Background()
				ctx, cancel := context.WithTimeout(ctx, 1*time.Second)
				defer cancel()

				respChan := make(chan search.UserStateResponse, 1)

				go searcher.Search(context.Background(), tuid, respChan)

				for {
					select {
					case resp := <-respChan:
						So(resp.Error, ShouldNotBeNil)
						So(resp.UserState, ShouldEqual, api.UserStateUnknown)
						return
					case <-ctx.Done():
						panic("test doesn't work")
					}
				}

			})
		})

		Convey("when client returns an empty result", func() {
			transactionsFetcher.On("FetchTransactionsByUserIDAndTransactionType", mock.Anything, tuid, models.BitsEventTransactionType, expectedLimit, expectedDescending, expectedBefore, expectedAfter).Return([]*dynamo.Transaction{}, nil)

			Convey("then we return an unknown", func() {
				ctx := context.Background()
				ctx, cancel := context.WithTimeout(ctx, 1*time.Second)
				defer cancel()

				respChan := make(chan search.UserStateResponse, 1)

				go searcher.Search(context.Background(), tuid, respChan)

				for {
					select {
					case resp := <-respChan:
						So(resp.Error, ShouldBeNil)
						So(resp.UserState, ShouldEqual, api.UserStateUnknown)
						return
					case <-ctx.Done():
						panic("test doesn't work")
					}
				}

			})
		})

		Convey("when client returns an result", func() {
			transactionsFetcher.On("FetchTransactionsByUserIDAndTransactionType", mock.Anything, tuid, models.BitsEventTransactionType, expectedLimit, expectedDescending, expectedBefore, expectedAfter).Return([]*dynamo.Transaction{
				{
					TransactionId: "Hello",
				},
			}, nil)

			Convey("then we return an cheered", func() {
				ctx := context.Background()
				ctx, cancel := context.WithTimeout(ctx, 1*time.Second)
				defer cancel()

				respChan := make(chan search.UserStateResponse, 1)

				go searcher.Search(context.Background(), tuid, respChan)

				for {
					select {
					case resp := <-respChan:
						So(resp.Error, ShouldBeNil)
						So(resp.UserState, ShouldEqual, api.UserStateCheered)
						return
					case <-ctx.Done():
						panic("test doesn't work")
					}
				}

			})
		})
	})
}
