package usercheered

import (
	"context"

	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/models/api"
	"code.justin.tv/commerce/payday/transactions"
	"code.justin.tv/commerce/payday/userstate/search"
)

type Searcher interface {
	Search(ctx context.Context, tuid string, resp chan search.UserStateResponse)
}

func NewSearcher() Searcher {
	return &searcher{}
}

type searcher struct {
	TransactionsFetcher transactions.Fetcher `inject:""`
}

func (s *searcher) Search(ctx context.Context, tuid string, resp chan search.UserStateResponse) {
	cheers, err := s.TransactionsFetcher.FetchTransactionsByUserIDAndTransactionType(ctx, tuid, models.BitsEventTransactionType, 1, false, nil, nil)

	if err != nil {
		resp <- search.UserStateResponse{
			Error:     err,
			UserState: api.UserStateUnknown,
		}
		return
	}
	if len(cheers) > 0 {
		resp <- search.UserStateResponse{
			Error:     err,
			UserState: api.UserStateCheered,
		}
		return
	}

	resp <- search.UserStateResponse{
		UserState: api.UserStateUnknown,
	}
}
