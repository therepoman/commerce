package purchasebits

import (
	"context"

	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/models/api"
	platform_getter "code.justin.tv/commerce/payday/products/purchase_by_most_recent_super_platform/getter"
	"code.justin.tv/commerce/payday/userstate/search"
)

type Searcher interface {
	Search(ctx context.Context, tuid string, resp chan search.UserStateResponse)
}

func NewSearcher() Searcher {
	return &searcher{}
}

type searcher struct {
	PlatformGetter platform_getter.Getter `inject:""`
}

func (s *searcher) Search(ctx context.Context, tuid string, resp chan search.UserStateResponse) {
	acquired := s.PlatformGetter.Get(ctx, tuid)

	if acquired != models.DEFAULT {
		resp <- search.UserStateResponse{
			UserState: api.UserStateAcquired,
		}
		return
	}

	resp <- search.UserStateResponse{
		UserState: api.UserStateUnknown,
	}
}
