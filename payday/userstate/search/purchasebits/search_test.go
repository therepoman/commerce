package purchasebits

import (
	"context"
	"testing"
	"time"

	products_purchase_by_most_recent_super_platform_getter_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/products/purchase_by_most_recent_super_platform/getter"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/models/api"
	"code.justin.tv/commerce/payday/userstate/search"

	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestSearcher_Search(t *testing.T) {
	Convey("given a purchase bits Searcher", t, func() {
		platformGetter := new(products_purchase_by_most_recent_super_platform_getter_mock.Getter)
		const tuid = "12345"

		searcher := &searcher{
			PlatformGetter: platformGetter,
		}

		Convey("when platformGetter returns default", func() {
			platformGetter.On("Get", mock.Anything, tuid).Return(models.DEFAULT)

			Convey("then we return an error", func() {
				ctx := context.Background()
				ctx, cancel := context.WithTimeout(ctx, 1*time.Second)
				defer cancel()

				respChan := make(chan search.UserStateResponse, 1)

				go searcher.Search(context.Background(), tuid, respChan)

				for {
					select {
					case resp := <-respChan:
						So(resp.Error, ShouldBeNil)
						So(resp.UserState, ShouldEqual, api.UserStateUnknown)
						return
					case <-ctx.Done():
						panic("test doesn't work")
					}
				}

			})
		})

		Convey("when platformGetter succeeds", func() {
			Convey("when platformGetter returns not default", func() {
				platformGetter.On("Get", mock.Anything, tuid).Return(models.AMAZON)

				Convey("then we return an error", func() {
					ctx := context.Background()
					ctx, cancel := context.WithTimeout(ctx, 1*time.Second)
					defer cancel()

					respChan := make(chan search.UserStateResponse, 1)

					go searcher.Search(context.Background(), tuid, respChan)

					for {
						select {
						case resp := <-respChan:
							So(resp.Error, ShouldBeNil)
							So(resp.UserState, ShouldEqual, api.UserStateAcquired)
							return
						case <-ctx.Done():
							panic("test doesn't work")
						}
					}
				})
			})
		})
	})
}
