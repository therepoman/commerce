package search

import "code.justin.tv/commerce/payday/models/api"

type PrometheusResponse struct {
	Total int64
	Error error
}

type UserStateResponse struct {
	UserState api.UserState
	Error     error
}
