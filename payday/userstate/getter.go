package userstate

import (
	"context"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/models/api"
)

type Getter interface {
	Get(tuid string) (api.UserState, error)
}

func NewGetter() Getter {
	return &getter{}
}

type getter struct {
	Fetcher  Fetcher  `inject:""`
	Searcher Searcher `inject:""`
	Setter   Setter   `inject:""`
}

func (s *getter) Get(tuid string) (api.UserState, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	persistedUserState, err := s.Fetcher.Fetch(ctx, tuid)
	if err != nil {
		return api.UserStateUnknown, err
	}
	if persistedUserState != api.UserStateUnknown {
		return persistedUserState, nil
	}

	calculatedUserState, err := s.Searcher.Search(ctx, tuid)
	if err != nil {
		return api.UserStateUnknown, err
	}

	// Store the new user state so we don't have to look it up again.
	err = s.Setter.Set(ctx, tuid, calculatedUserState)
	if err != nil {
		// when we have an error setting the calculated state, we don't want to error, just send
		// back the calculated state, but we should log an error
		log.WithError(err).WithFields(log.Fields{"tuid": tuid, "state": calculatedUserState}).Error("could not set calculated user state")
		return calculatedUserState, nil
	}

	return calculatedUserState, nil
}
