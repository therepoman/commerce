package userstate

import (
	"context"
	"testing"

	experiments_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/utils/experiments"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestBouncer_IsPermitted(t *testing.T) {
	Convey("given a userstate bouncer", t, func() {
		ctx := context.Background()
		experimentFetcher := new(experiments_mock.ExperimentConfigFetcher)

		bouncer := &bouncer{
			ExperimentFetcher: experimentFetcher,
		}

		permittedUser := "123123123"

		Convey("if prometheus is enabled", func() {
			experimentFetcher.On("IsExperimentActiveForUserOrChannel", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(true)

			Convey("then we should return they are permitted", func() {
				isPermitted := bouncer.IsPermitted(ctx, permittedUser)

				So(isPermitted, ShouldBeTrue)
			})
		})

		Convey("if prometheus is not enabled", func() {
			experimentFetcher.On("IsExperimentActiveForUserOrChannel", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(false)

			Convey("then we should return they are not permitted", func() {
				isPermitted := bouncer.IsPermitted(ctx, permittedUser)

				So(isPermitted, ShouldBeFalse)
			})
		})
	})
}
