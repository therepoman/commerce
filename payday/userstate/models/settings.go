package models

type UserSettings struct {
	SkippedFirstCheerTutorial   bool `json:"skipped_first_cheer_tutorial"`
	AbandonedFirstCheerTutorial bool `json:"abandoned_first_cheer_tutorial"`
}
