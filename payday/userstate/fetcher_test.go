package userstate

import (
	"context"
	"testing"

	"github.com/stretchr/testify/mock"

	"code.justin.tv/commerce/payday/dynamo"
	user_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/user"
	userstate_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/userstate"
	"code.justin.tv/commerce/payday/models/api"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
)

func TestFetcher_Fetch(t *testing.T) {
	Convey("given a user state fetcher", t, func() {
		ctx := context.Background()
		userFetcher := new(user_mock.Fetcher)
		bouncer := new(userstate_mock.Bouncer)

		fetcher := &fetcher{
			Fetcher: userFetcher,
			Bouncer: bouncer,
		}

		tuid := "123123123"

		Convey("When user state is not enabled", func() {
			bouncer.On("IsPermitted", mock.Anything, tuid).Return(false)

			Convey("then we should return new user", func() {
				state, err := fetcher.Fetch(ctx, tuid)

				So(err, ShouldBeNil)
				So(state, ShouldEqual, api.UserStateNew)
			})
		})

		Convey("when user state is enabled", func() {
			bouncer.On("IsPermitted", mock.Anything, tuid).Return(true)

			Convey("when the user userFetcher fails", func() {
				userFetcher.On("Fetch", mock.Anything, tuid).Return(nil, errors.New("WALRUS STRIKE"))

				Convey("then we should error", func() {
					state, err := fetcher.Fetch(ctx, tuid)

					So(err, ShouldNotBeNil)
					So(state, ShouldEqual, api.UserStateUnknown)
				})
			})

			Convey("when the user userFetcher returns nil", func() {
				userFetcher.On("Fetch", mock.Anything, tuid).Return(nil, nil)

				Convey("then we should return that the state is unknown", func() {
					state, err := fetcher.Fetch(ctx, tuid)

					So(err, ShouldBeNil)
					So(state, ShouldEqual, api.UserStateUnknown)
				})
			})

			Convey("when the user userFetcher returns a user", func() {
				user := &dynamo.User{
					Id:        dynamo.UserId(tuid),
					Skipped:   false,
					UserState: api.UserStateAcquired.ToString(),
				}

				userFetcher.On("Fetch", mock.Anything, tuid).Return(user, nil)

				user.Skipped = false

				Convey("then we return the user state on the user record", func() {
					state, err := fetcher.Fetch(ctx, tuid)

					So(err, ShouldBeNil)
					So(state, ShouldEqual, api.UserStateAcquired)
				})
			})
		})
	})
}
