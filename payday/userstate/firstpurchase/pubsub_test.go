package firstpurchase

import (
	"context"
	"fmt"
	"testing"

	client_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/chat/pubsub-go-pubclient/client"
	"code.justin.tv/commerce/payday/models"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestPubSubber_Notify(t *testing.T) {
	Convey("given a first purchase pubsubber", t, func() {
		pubClient := new(client_mock.PubClient)

		pubsubber := &pubSubber{
			PubClient: pubClient,
		}

		tuid := "123123123"
		amount := int64(322)

		ctx := context.Background()

		Convey("When the pubsubber errors", func() {
			pubClient.On("Publish", ctx, []string{fmt.Sprintf("%s.%s", models.UserBitsUpdatesPubsubMessageTopic, tuid)}, mock.Anything, mock.Anything).Return(errors.New("WALRUS STRIKE"))

			Convey("then we should return an error", func() {
				err := pubsubber.Notify(ctx, tuid, amount)

				So(err, ShouldNotBeNil)
			})
		})

		Convey("When the pubsubber succeeds", func() {
			pubClient.On("Publish", ctx, []string{fmt.Sprintf("%s.%s", models.UserBitsUpdatesPubsubMessageTopic, tuid)}, mock.Anything, mock.Anything).Return(nil)

			Convey("then we should return nil", func() {
				err := pubsubber.Notify(ctx, tuid, amount)

				So(err, ShouldBeNil)
			})
		})
	})
}
