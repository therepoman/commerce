package firstpurchase

import (
	"context"
	"encoding/json"
	"fmt"

	"code.justin.tv/chat/pubsub-go-pubclient/client"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/models"
	"github.com/gofrs/uuid"
)

type PubSubber interface {
	Notify(ctx context.Context, tuid string, amount int64) error
}

func NewPubSubber() PubSubber {
	return &pubSubber{}
}

type pubSubber struct {
	PubClient client.PubClient `inject:""`
}

func (p *pubSubber) Notify(ctx context.Context, tuid string, amount int64) error {
	messageIdUUID, err := uuid.NewV4()
	if err != nil {
		return errors.Notef(err, "Error generating UUID")
	}

	msg := models.PrivateBitsMessage{
		Data: &models.UserFirstPurchase{
			UserID:       tuid,
			BitsAcquired: amount,
		},
		MessageId:   messageIdUUID.String(),
		MessageType: models.FirstBitsPurchaseMessageType,
		Version:     models.CurrentPrivateMessageVersion,
	}

	marshaled, err := json.Marshal(msg)
	if err != nil {
		return err
	}

	return p.PubClient.Publish(ctx, []string{fmt.Sprintf("%s.%s", models.UserBitsUpdatesPubsubMessageTopic, tuid)}, string(marshaled), nil)
}
