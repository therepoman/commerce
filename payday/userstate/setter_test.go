package userstate

import (
	"context"
	"testing"

	"code.justin.tv/commerce/payday/dynamo"
	user_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/user"
	"code.justin.tv/commerce/payday/models/api"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestSetter_Set(t *testing.T) {
	Convey("given a user state setter", t, func() {
		ctx := context.Background()
		fetcher := new(user_mock.Fetcher)
		userSetter := new(user_mock.Setter)

		setter := &setter{
			Fetcher:    fetcher,
			UserSetter: userSetter,
		}

		tuid := "123123123"

		Convey("when the user fetcher fails", func() {
			fetcher.On("Fetch", mock.Anything, tuid).Return(nil, errors.New("WALRUS STRIKE"))

			Convey("then we should return an error", func() {
				err := setter.Set(ctx, tuid, api.UserStateAcquired)

				So(err, ShouldNotBeNil)
			})
		})

		Convey("when the user fetcher succeeds", func() {
			fetcher.On("Fetch", mock.Anything, tuid).Return(&dynamo.User{
				Id:        dynamo.UserId(tuid),
				UserState: api.NEWUSER,
			}, nil)

			Convey("when the user update fails", func() {
				userSetter.On("Set", mock.Anything, tuid, mock.Anything).Return(errors.New("WALRUS STRIKE"))

				Convey("then we should return an error", func() {
					err := setter.Set(ctx, tuid, api.UserStateAcquired)

					So(err, ShouldNotBeNil)
				})
			})

			Convey("when the user update succeeds", func() {
				userSetter.On("Set", mock.Anything, tuid, mock.Anything).Return(nil)

				Convey("then we should return nil", func() {
					err := setter.Set(ctx, tuid, api.UserStateAcquired)

					So(err, ShouldBeNil)
				})
			})
		})
	})
}
