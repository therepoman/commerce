package firstcheer

import (
	"context"
	"testing"

	"code.justin.tv/commerce/payday/errors"
	tmi_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/tmi"
	metrics_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/metrics"
	userstate_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/userstate"
	"code.justin.tv/commerce/payday/models/api"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestUserNoticer_SendMessage(t *testing.T) {
	Convey("Given an user state user noticer", t, func() {
		ctx := context.Background()
		tmiClient := new(tmi_mock.ITMIClient)
		userStateGetter := new(userstate_mock.Getter)
		statter := new(metrics_mock.Statter)
		userStateBouncer := new(userstate_mock.Bouncer)

		userNotice := userNoticer{
			TmiClient:       tmiClient,
			UserStateGetter: userStateGetter,
			Statter:         statter,
			Bouncer:         userStateBouncer,
		}

		statter.On("TimingDuration", mock.Anything, mock.Anything).Return()

		channelId := "19942018"
		userId := "20181994"
		testMessage := "Tonkotsu ramen defeats Pho actually cheer1"
		request := api.CreateEventRequest{
			ChannelId: channelId,
			UserId:    userId,
			Message:   testMessage,
		}

		Convey("when the channel is not eligible for first time cheer user notice", func() {
			userStateBouncer.On("IsUserNoticeEnabled", mock.Anything, channelId).Return(false)

			Convey("then it should not call TMI", func() {
				sent, err := userNotice.SendMessage(ctx, request)

				userStateGetter.AssertNotCalled(t, "Get", userId)
				tmiClient.AssertNotCalled(t, "SendUserNotice", mock.Anything)
				So(sent, ShouldBeFalse)
				So(err, ShouldBeNil)
			})
		})

		Convey("when the channel is eligible for first time cheer user notice", func() {
			userStateBouncer.On("IsUserNoticeEnabled", mock.Anything, channelId).Return(true)

			Convey("when the User State Getter errors", func() {
				userStateGetter.On("Get", userId).Return(api.UserStateUnknown, errors.New("taco bell"))

				Convey("then it should not call TMI", func() {
					sent, err := userNotice.SendMessage(ctx, request)

					tmiClient.AssertNotCalled(t, "SendUserNotice", mock.Anything)
					So(sent, ShouldBeFalse)
					So(err, ShouldNotBeNil)
				})
			})

			Convey("when the User State is CHEERED", func() {
				userStateGetter.On("Get", userId).Return(api.UserStateCheered, nil)

				Convey("then it should not call TMI", func() {
					sent, err := userNotice.SendMessage(ctx, request)

					tmiClient.AssertNotCalled(t, "SendUserNotice", mock.Anything)
					So(sent, ShouldBeFalse)
					So(err, ShouldBeNil)
				})
			})

			Convey("when the user has never cheered", func() {
				userStateGetter.On("Get", userId).Return(api.UserStateNew, nil)

				Convey("when channel id is not parsable", func() {
					invalidChannelId := "notanint"
					userStateBouncer.On("IsUserNoticeEnabled", mock.Anything, invalidChannelId).Return(true)
					invalidChannelRequest := api.CreateEventRequest{
						ChannelId: invalidChannelId,
						UserId:    userId,
						Message:   testMessage,
					}

					Convey("then 'sent' should be false and should return an error", func() {
						sent, err := userNotice.SendMessage(ctx, invalidChannelRequest)

						So(sent, ShouldBeFalse)
						So(err, ShouldNotBeNil)
					})
				})

				Convey("when user id is not parsable", func() {
					invalidUserId := "notanint"
					userStateGetter.On("Get", invalidUserId).Return(api.UserStateNew, nil)
					invalidUserRequest := api.CreateEventRequest{
						ChannelId: channelId,
						UserId:    invalidUserId,
						Message:   testMessage,
					}

					Convey("then 'sent' should be false and should return an error", func() {
						sent, err := userNotice.SendMessage(ctx, invalidUserRequest)

						So(sent, ShouldBeFalse)
						So(err, ShouldNotBeNil)
					})
				})

				Convey("when TMI client errors", func() {
					tmiClient.On("SendUserNotice", mock.Anything).Return(errors.New("that is bad food"))

					Convey("then it should not mark it as 'sent'", func() {
						sent, err := userNotice.SendMessage(ctx, request)

						So(sent, ShouldBeFalse)
						So(err, ShouldNotBeNil)
					})
				})

				Convey("when TMI client succeeds", func() {
					tmiClient.On("SendUserNotice", mock.Anything).Return(nil)

					Convey("then it should mark it as 'sent'", func() {
						sent, err := userNotice.SendMessage(ctx, request)

						So(sent, ShouldBeTrue)
						So(err, ShouldBeNil)
					})
				})
			})
		})
	})
}
