package userstate

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/mock"

	purchasebits_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/userstate/search/purchasebits"
	usercheered_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/userstate/search/usercheered"
	"code.justin.tv/commerce/payday/models/api"
	"code.justin.tv/commerce/payday/userstate/search"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
)

func TestSearcher_Search(t *testing.T) {
	Convey("given a user state searcher", t, func() {
		purchaseBitsSearcher := new(purchasebits_mock.Searcher)
		userCheeredSearcher := new(usercheered_mock.Searcher)

		searcher := &searcher{
			UserCheeredSearcher:  userCheeredSearcher,
			PurchaseBitsSearcher: purchaseBitsSearcher,
		}

		tuid := "123123123"
		userCheeredSearcher.On("Search", mock.Anything, tuid, mock.Anything).Return()
		purchaseBitsSearcher.On("Search", mock.Anything, tuid, mock.Anything).Return()

		Convey("When the cheer searcher succeeds", func() {
			cheerBitsResponse := search.UserStateResponse{
				UserState: api.UserStateCheered,
				Error:     nil,
			}

			Convey("then we return the state", func() {
				cheeredChannel := make(chan search.UserStateResponse, 1)
				acquiredChannel := make(chan search.UserStateResponse, 1)

				searchChannels := searcherChannels{
					CheeredChannel:  cheeredChannel,
					AcquiredChannel: acquiredChannel,
				}

				ctx := context.Background()
				ctx, cancel := context.WithTimeout(ctx, 1*time.Second)
				defer cancel()

				searchChannels.CheeredChannel <- cheerBitsResponse

				userState, err := searcher.search(ctx, tuid, searchChannels)

				So(userState, ShouldEqual, api.UserStateCheered)
				So(err, ShouldBeNil)
			})
		})

		Convey("When the cheer searcher fails", func() {
			cheerBitsResponse := search.UserStateResponse{
				UserState: api.UserStateUnknown,
				Error:     errors.New("WALRUS STRIKE"),
			}

			Convey("then we return the error", func() {
				cheeredChannel := make(chan search.UserStateResponse, 1)
				acquiredChannel := make(chan search.UserStateResponse, 1)

				searchChannels := searcherChannels{
					CheeredChannel:  cheeredChannel,
					AcquiredChannel: acquiredChannel,
				}

				ctx := context.Background()
				ctx, cancel := context.WithTimeout(ctx, 1*time.Second)
				defer cancel()

				searchChannels.CheeredChannel <- cheerBitsResponse

				userState, err := searcher.search(ctx, tuid, searchChannels)

				So(userState, ShouldEqual, api.UserStateUnknown)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("When the acquired searcher succeeds", func() {
			gotBitsResponse := search.UserStateResponse{
				UserState: api.UserStateAcquired,
				Error:     nil,
			}

			Convey("then we return the state", func() {
				cheeredChannel := make(chan search.UserStateResponse, 1)
				acquiredChannel := make(chan search.UserStateResponse, 1)

				searchChannels := searcherChannels{
					CheeredChannel:  cheeredChannel,
					AcquiredChannel: acquiredChannel,
				}

				ctx := context.Background()
				ctx, cancel := context.WithTimeout(ctx, 1*time.Second)
				defer cancel()

				searchChannels.AcquiredChannel <- gotBitsResponse

				userState, err := searcher.search(ctx, tuid, searchChannels)

				So(userState, ShouldEqual, api.UserStateAcquired)
				So(err, ShouldBeNil)
			})
		})

		Convey("When the acquired searcher fails", func() {
			gotBitsResponse := search.UserStateResponse{
				UserState: api.UserStateUnknown,
				Error:     errors.New("WALRUS STRIKE"),
			}

			Convey("then we return the error", func() {
				cheeredChannel := make(chan search.UserStateResponse, 1)
				acquiredChannel := make(chan search.UserStateResponse, 1)

				searchChannels := searcherChannels{
					CheeredChannel:  cheeredChannel,
					AcquiredChannel: acquiredChannel,
				}

				ctx := context.Background()
				ctx, cancel := context.WithTimeout(ctx, 1*time.Second)
				defer cancel()

				searchChannels.AcquiredChannel <- gotBitsResponse

				userState, err := searcher.search(ctx, tuid, searchChannels)

				So(userState, ShouldEqual, api.UserStateUnknown)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when the context times out", func() {
			ctx := context.Background()
			ctx, cancel := context.WithTimeout(ctx, 1*time.Second)
			defer cancel()

			Convey("then we return the error", func() {
				userState, err := searcher.Search(ctx, tuid)

				So(userState, ShouldEqual, api.UserStateUnknown)
				So(err, ShouldNotBeNil)
			})
		})
	})
}
