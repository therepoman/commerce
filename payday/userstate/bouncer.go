package userstate

import (
	"context"

	"code.justin.tv/commerce/payday/utils/experiments"
)

type Bouncer interface {
	IsPermitted(ctx context.Context, tuid string) bool
	IsUserNoticeEnabled(ctx context.Context, channelId string) bool
}

func NewBouncer() Bouncer {
	return &bouncer{}
}

type bouncer struct {
	ExperimentFetcher experiments.ExperimentConfigFetcher `inject:""`
}

func (b *bouncer) IsPermitted(ctx context.Context, tuid string) bool {
	return b.ExperimentFetcher.IsExperimentActiveForUserOrChannel(ctx, experiments.PrometheusEnableExperimentPrefix, tuid, "")
}

func (b *bouncer) IsUserNoticeEnabled(ctx context.Context, channelId string) bool {
	return b.ExperimentFetcher.IsExperimentActiveForUserOrChannel(ctx, experiments.FirstCheerUserNoticeExperimentPrefix, "", channelId)
}
