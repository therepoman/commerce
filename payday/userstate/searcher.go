package userstate

import (
	"context"

	"code.justin.tv/commerce/payday/userstate/search/purchasebits"
	"code.justin.tv/commerce/payday/userstate/search/usercheered"

	"code.justin.tv/commerce/payday/models/api"
	"code.justin.tv/commerce/payday/userstate/search"

	"github.com/pkg/errors"
)

type searcherChannels struct {
	CheeredChannel  chan search.UserStateResponse
	AcquiredChannel chan search.UserStateResponse
}

type Searcher interface {
	Search(ctx context.Context, tuid string) (api.UserState, error)
}

func NewSearcher() Searcher {
	return &searcher{}
}

type searcher struct {
	UserCheeredSearcher  usercheered.Searcher  `inject:""`
	PurchaseBitsSearcher purchasebits.Searcher `inject:"purchaseBitsSearcher"`
}

func (s *searcher) Search(ctx context.Context, tuid string) (api.UserState, error) {
	cheeredChannel := make(chan search.UserStateResponse, 1)
	acquiredChannel := make(chan search.UserStateResponse, 1)

	return s.search(ctx, tuid, searcherChannels{
		CheeredChannel:  cheeredChannel,
		AcquiredChannel: acquiredChannel,
	})
}

func (s *searcher) search(ctx context.Context, tuid string, searchChannels searcherChannels) (api.UserState, error) {
	go s.UserCheeredSearcher.Search(ctx, tuid, searchChannels.CheeredChannel)
	go s.PurchaseBitsSearcher.Search(ctx, tuid, searchChannels.AcquiredChannel)

	cheersFinished, acquiredFinished := false, false

	for {
		select {
		case searchCheeredResponse := <-searchChannels.CheeredChannel:
			if searchCheeredResponse.Error != nil || searchCheeredResponse.UserState != api.UserStateUnknown {
				return searchCheeredResponse.UserState, searchCheeredResponse.Error
			}
			cheersFinished = true
		case searchAcquiredFinished := <-searchChannels.AcquiredChannel:
			if searchAcquiredFinished.Error != nil || searchAcquiredFinished.UserState != api.UserStateUnknown {
				return searchAcquiredFinished.UserState, searchAcquiredFinished.Error
			}
			acquiredFinished = true
		case <-ctx.Done():
			err := errors.New("Timed out while calling dynamo with various conditions")
			return api.UserStateUnknown, errors.Wrapf(err, "UserID: %v, cheersFinished:%v, acquiredFinished:%v", tuid, cheersFinished, acquiredFinished)
		}

		if cheersFinished && acquiredFinished {
			return api.UserStateNew, nil
		}
	}
}
