package userstate

import (
	"context"

	"code.justin.tv/commerce/payday/models/api"
	"code.justin.tv/commerce/payday/user"
)

type Fetcher interface {
	Fetch(ctx context.Context, tuid string) (api.UserState, error)
}

func NewFetcher() Fetcher {
	return &fetcher{}
}

type fetcher struct {
	Bouncer Bouncer      `inject:""`
	Fetcher user.Fetcher `inject:""`
}

func (f *fetcher) Fetch(ctx context.Context, tuid string) (api.UserState, error) {
	if !f.Bouncer.IsPermitted(ctx, tuid) {
		return api.UserStateNew, nil
	}

	userInfo, err := f.Fetcher.Fetch(ctx, tuid) // GetUser also sets the dynamo record in the cache if it's not there
	if err != nil || userInfo == nil {
		return api.UserStateUnknown, err
	}

	userState := api.NewUserState(userInfo.UserState)

	return userState, nil
}
