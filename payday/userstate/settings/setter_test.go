package settings

import (
	"context"
	"testing"

	"code.justin.tv/commerce/payday/dynamo"
	user_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/user"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestSetter_Set(t *testing.T) {
	Convey("given a user settings setter", t, func() {
		ctx := context.Background()
		userSetter := new(user_mock.Setter)

		setter := &setter{
			UserSetter: userSetter,
		}

		userID := "123123123"

		user := &dynamo.User{
			Id: dynamo.UserId(userID),
		}

		Convey("when user setter returns an error", func() {
			userSetter.On("Set", mock.Anything, userID, mock.Anything).Return(errors.New("ERROR"))

			err := setter.Set(ctx, &paydayrpc.SetUserSettingsReq{
				UserId: userID,
			})

			Convey("we should return the error", func() {
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when user setter does not return an error", func() {
			req := &paydayrpc.SetUserSettingsReq{
				UserId: userID,
			}
			userSetter.On("Set", mock.Anything, userID, mock.Anything).Return(nil)

			Convey("we should return nil", func() {
				returnValue := setter.Set(ctx, req)
				So(returnValue, ShouldBeNil)

				Convey("we should set the user", func() {
					userSetter.AssertCalled(t, "Set", mock.Anything, userID, user)
				})
			})
		})
	})
}

func TestSetter_GetUserWithUpdatedFields(t *testing.T) {
	Convey("given a user settings field updater", t, func() {
		userSetter := new(user_mock.Setter)
		userID := "123123123"

		setter := &setter{
			UserSetter: userSetter,
		}

		Convey("when skipped is passed in the request", func() {
			req := &paydayrpc.SetUserSettingsReq{
				UserId:                    userID,
				SkippedFirstCheerTutorial: true,
			}

			updatedUser := setter.getUserWithUpdatedFields(req)

			Convey("we should set the corresponding property on the user", func() {
				So(*updatedUser.SkippedFirstCheerTutorial, ShouldBeTrue)
			})
		})

		Convey("when abandoned is passed in the request", func() {
			req := &paydayrpc.SetUserSettingsReq{
				UserId:                      userID,
				AbandonedFirstCheerTutorial: true,
			}

			updatedUser := setter.getUserWithUpdatedFields(req)

			Convey("we should set the corresponding property on the user", func() {
				So(*updatedUser.AbandonedFirstCheerTutorial, ShouldBeTrue)
			})
		})
	})
}
