package settings

import (
	"context"

	"code.justin.tv/commerce/payday/userstate/models"
)

type Getter interface {
	Get(ctx context.Context, userID string) (*models.UserSettings, error)
}

type getter struct {
	Fetcher Fetcher `inject:""`
}

func NewGetter() Getter {
	return &getter{}
}

func (g *getter) Get(ctx context.Context, userID string) (*models.UserSettings, error) {
	userSettings, err := g.Fetcher.Fetch(ctx, userID)

	if err != nil {
		return nil, err
	}

	return userSettings, nil
}
