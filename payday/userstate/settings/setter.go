package settings

import (
	"context"

	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/payday/dynamo"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"code.justin.tv/commerce/payday/user"
)

type Setter interface {
	Set(ctx context.Context, req *paydayrpc.SetUserSettingsReq) error
}

type setter struct {
	UserSetter user.Setter `inject:""`
}

func NewSetter() Setter {
	return &setter{}
}

func (s *setter) Set(ctx context.Context, req *paydayrpc.SetUserSettingsReq) error {
	userID := req.UserId

	updatedUser := s.getUserWithUpdatedFields(req)
	err := s.UserSetter.Set(ctx, userID, &updatedUser)

	if err != nil {
		return err
	}

	return nil
}

func (s *setter) getUserWithUpdatedFields(req *paydayrpc.SetUserSettingsReq) dynamo.User {
	user := dynamo.User{
		Id: dynamo.UserId(req.UserId),
	}

	if req.SkippedFirstCheerTutorial {
		user.SkippedFirstCheerTutorial = pointers.BoolP(true)
	}

	if req.AbandonedFirstCheerTutorial {
		user.AbandonedFirstCheerTutorial = pointers.BoolP(true)
	}

	return user
}
