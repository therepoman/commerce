package settings

import (
	"context"

	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/payday/user"
	"code.justin.tv/commerce/payday/userstate/models"
)

type Fetcher interface {
	Fetch(ctx context.Context, userID string) (*models.UserSettings, error)
}

type fetcher struct {
	UserFetcher user.Fetcher `inject:""`
}

func NewFetcher() Fetcher {
	return &fetcher{}
}

func (f *fetcher) Fetch(ctx context.Context, userID string) (*models.UserSettings, error) {
	user, err := f.UserFetcher.Fetch(ctx, userID)

	if err != nil {
		return nil, err
	}

	userSettings := &models.UserSettings{}

	if user != nil {
		userSettings.SkippedFirstCheerTutorial = pointers.BoolOrDefault(user.SkippedFirstCheerTutorial, false)
		userSettings.AbandonedFirstCheerTutorial = pointers.BoolOrDefault(user.AbandonedFirstCheerTutorial, false)
	}

	return userSettings, nil
}
