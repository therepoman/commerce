package settings

import (
	"context"
	"testing"

	"github.com/stretchr/testify/mock"

	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/payday/dynamo"
	user_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/user"
	"code.justin.tv/commerce/payday/userstate/models"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
)

func TestFetcher_Fetch(t *testing.T) {
	Convey("given a user settings fetcher", t, func() {
		ctx := context.Background()
		userFetcher := new(user_mock.Fetcher)

		fetcher := &fetcher{
			UserFetcher: userFetcher,
		}

		userID := "123123123"

		Convey("when user fetcher returns an error", func() {
			userFetcher.On("Fetch", mock.Anything, userID).Return(nil, errors.New("ERROR"))

			Convey("we should return the error", func() {
				_, err := fetcher.Fetch(ctx, userID)

				So(err, ShouldNotBeNil)
			})
		})

		Convey("when user fetcher returns nil", func() {
			userFetcher.On("Fetch", mock.Anything, userID).Return(nil, nil)

			Convey("we should return empty user settings", func() {
				returnValue, err := fetcher.Fetch(ctx, userID)

				So(err, ShouldBeNil)
				So(*returnValue, ShouldResemble, models.UserSettings{})
			})
		})

		Convey("when user fetcher returns a user", func() {
			user := &dynamo.User{
				SkippedFirstCheerTutorial:   pointers.BoolP(true),
				AbandonedFirstCheerTutorial: pointers.BoolP(true),
			}
			settings := &models.UserSettings{
				SkippedFirstCheerTutorial:   *user.SkippedFirstCheerTutorial,
				AbandonedFirstCheerTutorial: *user.AbandonedFirstCheerTutorial,
			}
			userFetcher.On("Fetch", mock.Anything, userID).Return(user, nil)

			Convey("we should return user settings", func() {
				returnValue, err := fetcher.Fetch(ctx, userID)

				So(err, ShouldBeNil)
				So(*returnValue, ShouldResemble, *settings)
			})
		})
	})
}
