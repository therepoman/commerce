package settings

import (
	"context"
	"testing"

	"github.com/stretchr/testify/mock"

	settings_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/userstate/settings"
	"code.justin.tv/commerce/payday/userstate/models"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
)

func TestGetter_Get(t *testing.T) {
	Convey("given a user settings getter", t, func() {
		ctx := context.Background()
		fetcher := new(settings_mock.Fetcher)

		getter := &getter{
			Fetcher: fetcher,
		}

		userID := "123123123"

		Convey("when we error fetching the user settings", func() {
			fetcher.On("Fetch", mock.Anything, userID).Return(nil, errors.New("ERROR"))

			Convey("We should return an error", func() {
				_, err := getter.Get(ctx, userID)

				So(err, ShouldNotBeNil)
			})
		})

		Convey("when we succeed fetching the user settings", func() {
			userSettings := &models.UserSettings{}

			fetcher.On("Fetch", mock.Anything, userID).Return(userSettings, nil)

			Convey("we should return the user settings", func() {
				returnValue, err := getter.Get(ctx, userID)

				So(err, ShouldBeNil)
				So(returnValue, ShouldEqual, userSettings)
			})
		})
	})
}
