package refill

import (
	"context"
	"encoding/json"
	"strconv"
	"time"

	dart "code.justin.tv/amzn/TwitchDartReceiverTwirp"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/balance"
	"code.justin.tv/commerce/payday/cache"
	"code.justin.tv/commerce/payday/datascience"
	"code.justin.tv/commerce/payday/dynamo/auto_refill_settings"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/products"
	"code.justin.tv/commerce/payday/products/availability"
	petozi "code.justin.tv/commerce/petozi/rpc"
	everdeenmodel "code.justin.tv/revenue/everdeen/models"
	everdeentwirp "code.justin.tv/revenue/everdeen/rpc"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/gofrs/uuid"
	"github.com/twitchtv/twirp"
)

const (
	BitsTPL       = "bits_bundle"
	EnabledState  = "enabled"
	DisabledState = "disabled"
	UpdatedAction = "updated"
	NoAction      = "no_action"

	//todo: make sure these map to the actual keys, ask germanjohn
	DartKeyUserDisabled                 = "auto-refill-user-disabled"
	DartKeyUserDisabledTwoFactor        = "auto-refill-2fa-disable"
	DartKeyNewConfirmation              = "auto-refill-new-confirmation"
	DartKeyUpdateConfirmation           = "auto-refill-updated-confirmation"
	DartKeyUserDisabledPurchaseVelocity = "auto-refill-trans-fail-spend-limit"
	DartKeyUserDisabledCardDeclined     = "auto-refill-trans-fail-payment-issue"
	DartKeyUserDisabledInvalidPayment   = "auto-refill-trans-fail-payment-issue"

	ManualUpdateType = "MANUAL"
	AutoUpdateType   = "AUTO"

	DisabledReasonCardDeclined     = "CARD_DECLINED"
	DisabledReasonInvalidArguments = "INVALID_ARGUMENTS"
	DisabledReasonInvalidPayment   = "INVALID_PAYMENT"
	DisabledReasonPurchaseVelocity = "VELOCITY"
	DisabledReasonTwoFactor        = "2FA"

	AttemptRateLimit = 30 * time.Second
	ReasonCodeKey    = "reasonCode"
)

var RateLimitError = errors.New("Purchase Attempt Rate Limit Exceeded")

type Refill interface {
	Get(ctx context.Context, userId string) (*auto_refill_settings.AutoRefillSettingsProfile, error)
	Set(ctx context.Context, newProfile auto_refill_settings.AutoRefillSettingsProfile, reason string) (*auto_refill_settings.AutoRefillSettingsProfile, error)
	SetWithoutNotification(ctx context.Context, newProfile auto_refill_settings.AutoRefillSettingsProfile, reason string, sendDataScienceEvent bool) (profile *auto_refill_settings.AutoRefillSettingsProfile, action string, err error)
	CheckForProfileAndSendNotificationIfNeeded(ctx context.Context, userId string) error
	DisableProfileAndRecordFailure(ctx context.Context, userId string, reason string, transactionId string, requestId string) (*auto_refill_settings.AutoRefillSettingsProfile, error)
	HandleProfileHitPurchaseVelocity(ctx context.Context, profile auto_refill_settings.AutoRefillSettingsProfile) (*auto_refill_settings.AutoRefillSettingsProfile, error)
}

type refill struct {
	Dao               auto_refill_settings.AutoRefillDao       `inject:""`
	Cache             cache.AutoRefillProfileCache             `inject:""`
	BalanceClient     balance.Getter                           `inject:""`
	Limiter           availability.ProductAvailabilityChecker  `inject:""`
	Everdeen          everdeentwirp.Everdeen                   `inject:""`
	DataScienceClient datascience.DataScience                  `inject:""`
	DartReceiver      dart.Receiver                            `inject:""`
	ProductsFetcher   products.Fetcher                         `inject:""`
	RefillRecordDao   auto_refill_settings.AutoRefillRecordDao `inject:""`
	SQSClient         *sqs.SQS
	RetryQueueURL     string
}

func NewRefillManager(queueURL string) Refill {
	sess, _ := session.NewSession()
	sqs := sqs.New(sess, &aws.Config{
		Region: aws.String("us-west-2"),
	})
	return &refill{
		RetryQueueURL: queueURL,
		SQSClient:     sqs,
	}
}

type ProfileEntries struct {
	ActiveProfile   *auto_refill_settings.AutoRefillSettingsProfile
	AllProfiles     []*auto_refill_settings.AutoRefillSettingsProfile
	LastAttemptTime time.Time
}

func (r *refill) getUncachedEnabledEntries(userId string) (ProfileEntries, error) {
	// grab a list of values from the dao by userId, filtered on "Active" state.
	values, err := r.Dao.GetActiveSettings(userId)
	entries := ProfileEntries{}

	if err != nil {
		log.WithError(err).Warn("Unable to get auto reload entries from DynamoDB")
		return entries, err
	}

	if len(values) > 0 {
		// we need to filter the refill settings on Bits only entries, since in theory
		// this table could support multiple products in the future.
		// we take the most recent profile in the event there are multiple
		// but return a list of all of them so we can clean up if needed.
		for _, value := range values {
			// need to pin the value to get a pointer to it
			value := value
			if value.Tpl == BitsTPL {
				if entries.ActiveProfile == nil {
					entries.ActiveProfile = &value
				} else if entries.ActiveProfile.CreatedAt < value.CreatedAt {
					entries.ActiveProfile = &value
				}

				if entries.LastAttemptTime.Before(value.InProgressRequestTime) {
					entries.LastAttemptTime = value.InProgressRequestTime
				}
				entries.AllProfiles = append(entries.AllProfiles, &value)
			}
		}
	}
	return entries, nil
}

func (r *refill) Get(ctx context.Context, userId string) (*auto_refill_settings.AutoRefillSettingsProfile, error) {
	// grab from the cache
	cacheValue, found, err := r.Cache.Get(ctx, userId)

	if err != nil {
		log.WithError(err).Warn("Unable to get auto reload entries from cache")
		return nil, err
	}

	// values in the cache can be found as nil, so make sure this isn't the case before
	// hitting up dynamo needlessly only to find no entries.
	if cacheValue == nil && !found {
		// this assumes that disabled profiles are never relevant
		foundEntries, err := r.getUncachedEnabledEntries(userId)

		if err != nil {
			log.WithError(err).Warn("Cache was nil but unable to get auto reload entries from DynamoDB")
			return nil, err
		}

		if foundEntries.ActiveProfile != nil {
			// set whatever we find (including nil!) in the cache and return it
			r.Cache.Set(ctx, userId, foundEntries.ActiveProfile)
			return foundEntries.ActiveProfile, nil
		} else {
			// check to see if there's a disabled profile
			// todo: consider moving grabbing most recent profile first and using other query as a backup
			profile, err := r.Dao.GetMostRecentSettings(userId)
			if err != nil {
				log.WithError(err).Warn("Error pulling most recent refill profile from dynamo")
				return nil, err
			}
			r.Cache.Set(ctx, userId, profile)
			return profile, nil
		}
	} else {
		return cacheValue, nil
	}
}

func (r *refill) SetWithoutNotification(ctx context.Context, newProfile auto_refill_settings.AutoRefillSettingsProfile, reason string, sendDataScienceEvent bool) (*auto_refill_settings.AutoRefillSettingsProfile, string, error) {
	bitsProducts, err := r.ProductsFetcher.FetchAllFromPetozi(ctx)
	if err != nil {
		log.Errorf("Could not fetch products from Petozi for auto refill profile: %v", err)
		return nil, "", err
	}

	product, _ := products.GetProductForOfferID(newProfile.OfferId, bitsProducts)

	if product == nil {
		log.Errorf("Could not find products from Petozi for auto refill profile with offerID: %s", newProfile.OfferId)
		return nil, "", errors.New("Product not found")
	}
	newProfile.SKU = product.Id
	newProfile.BitsType = product.BitsTypeId
	newProfile.BitsQuantity = int32(product.Quantity)

	userId := newProfile.UserId
	// clean up any cache entries and pull active records from dynamo
	r.Cache.Delete(ctx, userId)

	foundPreviousEntryToUpdate := false

	var userSetUpNewProfile, disabledAutoRefill bool

	// if a profile Id is specified, attempt an update instead of creating a new entry
	// to do this, we just need to pull the created at if it exists
	if newProfile.Id != "" {
		previousEntry, err := r.Dao.GetSettingsByID(newProfile.Id)

		if err != nil {
			log.WithError(err).Warn("Unable to get auto reload entries from DynamoDB to update profile")
			return nil, "", err
		}

		if previousEntry != nil && userId != previousEntry.UserId {
			log.Warn("UserIds do not match: Cannot change another user's auto refill settings")
			err = twirp.NewError(twirp.PermissionDenied, "User is not allowed to edit another user's auto refill settings.")
			return nil, "", err
		}

		if previousEntry != nil {
			// created at is the table key, so update the new key with the proper timestamp
			// to overwrite the previous entry and "update" the row.
			newProfile.CreatedAt = previousEntry.CreatedAt
			foundPreviousEntryToUpdate = true
			newProfile.InProgressRequestTime = previousEntry.InProgressRequestTime
			newProfile.PauseUntilTime = previousEntry.PauseUntilTime

			if newProfile.State != previousEntry.State {
				if newProfile.State == EnabledState {
					userSetUpNewProfile = true
				} else {
					disabledAutoRefill = true
				}
				// this if case determines if there is no action to take.
			} else if newProfile.IsEmailNotificationEnabled == previousEntry.IsEmailNotificationEnabled &&
				newProfile.OfferId == previousEntry.OfferId &&
				newProfile.Threshold == previousEntry.Threshold &&
				newProfile.SKUQuantity == previousEntry.SKUQuantity &&
				newProfile.BitsQuantity == previousEntry.BitsQuantity &&
				newProfile.BitsType == previousEntry.BitsType &&
				newProfile.Tpl == previousEntry.Tpl &&
				newProfile.ChargeInstrumentCategory == previousEntry.ChargeInstrumentCategory &&
				newProfile.ChargeInstrumentId == previousEntry.ChargeInstrumentId &&
				newProfile.InProgressRequestID == previousEntry.InProgressRequestID &&
				newProfile.AttemptCount == previousEntry.AttemptCount {
				// todo: determine if this should be an error; we should no-op because nothing was done
				profile := &newProfile
				return profile, NoAction, nil
			}
		} else {
			newProfile.Id = uuid.Must(uuid.NewV4()).String()
			newProfile.CreatedAt = time.Now().Format(time.RFC3339)
		}
	} else {
		//set id to uuid if one wasn't specified and created at date for new profile
		newProfile.Id = uuid.Must(uuid.NewV4()).String()
		newProfile.CreatedAt = time.Now().Format(time.RFC3339)
	}

	// if a previous profile wasn't specified, search through and make sure to disable any previously active profiles
	if !foundPreviousEntryToUpdate {
		profiles, err := r.getUncachedEnabledEntries(userId)
		newProfile.InProgressRequestTime = profiles.LastAttemptTime // carry forward the lock attempt time

		if err != nil {
			log.WithError(err).Warn("Unable to get auto reload entries from DynamoDB before setting new profile")
			return nil, NoAction, err
		}

		if len(profiles.AllProfiles) >= 1 {
			// we need to disable any old profiles when setting a new profile. Keep this data for historical sake.
			err = r.disableActiveProfiles(profiles.AllProfiles)
			if err != nil {
				log.WithError(err).Warn("Unable to get disable old auto reload entries in DynamoDB")
				return nil, NoAction, err
			}
			if newProfile.State == DisabledState {
				disabledAutoRefill = true
			}
		} else {
			if newProfile.State == EnabledState {
				userSetUpNewProfile = true
			} else {
				disabledAutoRefill = true
			}
		}
	}

	// set the new profile
	_, err = r.Dao.SetSettings(&newProfile)
	if err != nil {
		log.WithError(err).Warn("Unable to set new auto reload entry in DynamoDB")
		return nil, NoAction, err
	}

	var actionType string

	if reason == ManualUpdateType {
		actionType = ManualUpdateType
	} else {
		actionType = AutoUpdateType
	}

	var action string
	if disabledAutoRefill {
		action = DisabledState
	} else if userSetUpNewProfile {
		action = EnabledState
	} else {
		action = UpdatedAction
	}

	if sendDataScienceEvent {
		dsErr := r.DataScienceClient.TrackBitsEvent(ctx, datascience.AutoRefillAction, &models.AutoRefillAction{
			SKU:                newProfile.SKU,
			ServerTime:         time.Now(),
			Product:            "BITS",
			UserID:             newProfile.UserId,
			Action:             action,
			MinimumThreshold:   int(newProfile.Threshold),
			ActionReason:       reason,
			ActionType:         actionType,
			Location:           "", //todo: plumb this through from top down, needs GQL
			ChargeInstrumentID: newProfile.ChargeInstrumentId,
			BitType:            newProfile.BitsType,
			OfferID:            newProfile.OfferId,
		})
		if dsErr != nil {
			log.WithError(dsErr).Warn("Unable to send auto refill notification data science event")
		}
	}

	return &newProfile, action, nil
}

func (r *refill) Set(ctx context.Context, profile auto_refill_settings.AutoRefillSettingsProfile, reason string) (*auto_refill_settings.AutoRefillSettingsProfile, error) {
	newProfile, action, err := r.SetWithoutNotification(ctx, profile, reason, true)

	if err != nil {
		return nil, err
	}

	if newProfile == nil {
		return nil, errors.New("Profile came back nil after setting")
	}

	var notificationReason string

	if reason == ManualUpdateType {
		notificationReason = action
	} else {
		notificationReason = reason
	}

	//stubbed placeholder for now to hold the email notifications for updating a profile
	if newProfile.IsEmailNotificationEnabled && notificationReason != NoAction {
		err := r.sendUpdatedSettingsNotification(ctx, *newProfile, notificationReason)
		if err != nil {
			log.WithError(err).Warn("Unable to send updated settings auto refill notification")
		}
	}

	// if the new profile is active, we need to trigger a refill notification if the user is under the threshold
	newProfile, err = r.attemptToSendAutoRefillNotification(ctx, *newProfile)

	if err != nil {
		if err == RateLimitError {
			_, err = r.handleEverdeenTwirpError(ctx, *newProfile, err)
			if err != nil {
				log.WithError(err).Errorf("Unable to send auto refill retry sqs message for user %s.", profile.UserId)
				return newProfile, err
			}
		}
		log.WithError(err).Error("Unable to send auto refill notification")
	}

	return newProfile, nil
}

// This function performs all the actions required to ensure autorefills happen for a user
// intended to be called by GetActions, GetBalance and in response to Bits Usage SQS notifications
func (r *refill) CheckForProfileAndSendNotificationIfNeeded(ctx context.Context, userId string) error {
	profile, err := r.Get(ctx, userId)

	if err != nil {
		return err
	}

	if profile != nil {
		_, err = r.attemptToSendAutoRefillNotification(ctx, *profile)
		return err
	}

	return nil
}

func (r *refill) attemptToSendAutoRefillNotification(ctx context.Context, profile auto_refill_settings.AutoRefillSettingsProfile) (*auto_refill_settings.AutoRefillSettingsProfile, error) {
	if profile.State == EnabledState {
		log.Infof("Sending retry notification: user_id: %s, profile_id: %s, current attempt count: %d", profile.UserId, profile.Id, profile.AttemptCount)

		if time.Now().Before(profile.PauseUntilTime) {
			// no op?
			log.Infof("Skipping refill notification for user %s until time %s", profile.UserId, profile.PauseUntilTime.String())
			return &profile, nil
		}

		if profile.AttemptCount >= 3 {
			// eat message and return
			newProfile, err := r.DisableProfileAndRecordFailure(context.Background(), profile.UserId, DisabledReasonCardDeclined, "", profile.InProgressRequestID)
			if err != nil {
				log.WithError(err).Errorf("Issue disabling users auto refill profile after too many purchase attempts. ProfileID: %s", profile.Id)
			}
			return newProfile, err
		}

		balance, err := r.BalanceClient.GetBalance(context.Background(), profile.UserId, false)
		if err != nil {
			log.Warnf("Error getting Balance for user %s attempting to fulfill Bits Auto Refill notification.", profile.Id)
			_, err := r.handleEverdeenTwirpError(context.Background(), profile, err)
			return nil, err
		}
		if int32(balance) < profile.Threshold {
			dailyLimitReached := false
			// instead of complicated mappings, we just map the product to weight limits against using the largest SKU
			product := petozi.BitsProduct{Quantity: 25000}
			val, err := r.Limiter.GetPurchasableQuantityByInventoryLimit(context.Background(), &product, profile.UserId)
			if err != nil {
				log.WithError(err).Error("Issue pulling Bits inventory for limit calculations for refill")
			}
			if val <= 0 {
				dailyLimitReached = true
			}
			val, err = r.Limiter.GetPurchasableByDailyPurchaseLimit(context.Background(), &product, profile.UserId)
			if err != nil {
				log.WithError(err).Error("Issue pulling Bits Daily Purchase for limit calculations for refill")
			}
			if val <= 0 {
				dailyLimitReached = true
			}

			if dailyLimitReached {
				return r.HandleProfileHitPurchaseVelocity(ctx, profile)
			} else {
				log.Info("Processing Bits Auto Refill")
				exactProfile, err := r.Dao.GetExactSettings(profile.UserId, profile.CreatedAt)
				if err != nil {
					log.WithError(err).Error("Error getting exact profile for user from DynamoDB for refills")
				}
				if exactProfile == nil {
					log.Errorf("Exact profile matching expected profile not found: userID: %s created at time: %s id: %s", profile.UserId, profile.CreatedAt, profile.Id)
					return &profile, errors.New("Error updating profile timestamp")
				}
				if exactProfile.InProgressRequestTime.Add(AttemptRateLimit).After(time.Now()) {
					log.Warn("Rate limit hit attempting to fulfill refill request")
					return &profile, RateLimitError
				}

				// this will default to PaymentInstrumentCategory_TWITCH_PAYMENT_INSTRUMENT if not set/unmapped.
				paymentInstrumentCategory := everdeentwirp.PaymentInstrumentCategory(everdeentwirp.PaymentInstrumentCategory_value[profile.ChargeInstrumentCategory])
				requestId := uuid.Must(uuid.NewV4()).String()
				log.Infof("Generating auto refill request for user: %s id: %s", profile.UserId, requestId)
				exactProfile.InProgressRequestTime = time.Now()
				exactProfile.InProgressRequestID = requestId
				exactProfile.AttemptCount = exactProfile.AttemptCount + 1
				oldProfile, err := r.Dao.SetSettings(exactProfile)

				if err != nil {
					log.WithError(err).Errorf("Error attempting to write request ID to Dynamo, request id: %s", requestId)
					return &profile, err
				}
				go r.Cache.Delete(context.Background(), profile.UserId)

				if oldProfile.InProgressRequestTime.Add(AttemptRateLimit).After(time.Now()) {
					// check to make sure we haven't had any concurrency issues
					// by verifying the profile we wrote over didn't have any values
					// that we need to respect and exit early on.
					log.Warnf("Rate limit hit attempting to fulfill refill request while checking old profile, userID: %s requestID: %s", profile.UserId, requestId)
					return exactProfile, nil
				}

				resp, err := r.Everdeen.CreatePreauthorizedPurchase(context.Background(),
					&everdeentwirp.CreatePreauthorizedPurchaseRequest{
						UserId: profile.UserId,
						OfferData: &everdeentwirp.Offer{
							Id: profile.OfferId,
						},
						PaymentInstrumentCategory: paymentInstrumentCategory,
						PaymentInstrumentId:       profile.ChargeInstrumentId,
						// double check this
						Platform:  "web",
						RequestId: requestId,
					})

				if err != nil {
					log.WithError(err).Errorf("Handling everdeen twirp error for request: %s", requestId)
					newProfile, err := r.handleEverdeenTwirpError(context.Background(), profile, err)
					if err != nil {
						log.WithError(err).Errorf("Issue handling everdeen twirp error; backing out. Request id: %s", requestId)
						return &profile, err
					}
					return newProfile, nil
				} else if resp != nil && resp.Error != "" {
					log.Errorf("Error attempting to preauthorize payment with everdeen for user: %s, error: %s. Request id: %s", profile.UserId, err, requestId)
					return &profile, errors.New(resp.Error)
				} else {
					_ = r.RefillRecordDao.WriteRecord(&auto_refill_settings.AutoRefillPurchaseAttemptRecord{
						ProfileID:     profile.Id,
						UserID:        profile.UserId,
						Status:        auto_refill_settings.StatusAttempt,
						StatusNote:    auto_refill_settings.StatusNoteNormal,
						Time:          time.Now().Format(time.RFC3339),
						BalanceAtTime: int32(balance),
						Threshold:     profile.Threshold,
						RequestID:     requestId,
					})
				}
			}
		}
	}
	return &profile, nil
}

func (r *refill) disableActiveProfiles(profiles []*auto_refill_settings.AutoRefillSettingsProfile) error {
	if len(profiles) >= 1 {
		for _, profile := range profiles {
			profile.State = DisabledState
			_, err := r.Dao.SetSettings(profile)
			if err != nil {
				//logged above
				return err
			}
		}
	}
	return nil
}

func (r *refill) handleEverdeenTwirpError(ctx context.Context, profile auto_refill_settings.AutoRefillSettingsProfile, err error) (*auto_refill_settings.AutoRefillSettingsProfile, error) {
	log.Infof("[HandleEverdeenTwirpError] %s", err.Error())

	if twerr, ok := err.(twirp.Error); ok {
		failureType := twerr.Msg()
		if failureType == "" {
			failureType = twerr.Error()
		}

		_ = r.DataScienceClient.TrackBitsEvent(ctx, datascience.AutoRefillPurchaseFailure, &models.AutoRefillPurchaseFailure{
			UserID:        profile.UserId,
			Quantity:      1,
			Product:       "BITS",
			ServerTime:    time.Now(),
			SKU:           profile.SKU, //needs mapping to petozi sku
			TransactionID: "",          // failure so no transaction ID
			FailureType:   failureType,
			RequestID:     profile.InProgressRequestID,
			AttemptNumber: int(profile.AttemptCount),
			OfferID:       profile.OfferId,
			BitType:       profile.BitsType,
		})

		switch twerr.Meta(ReasonCodeKey) {
		case everdeenmodel.ReasonTwoFADisabled.TwirpError().Meta(ReasonCodeKey):
			return r.DisableProfileAndRecordFailure(ctx, profile.UserId, DisabledReasonTwoFactor, "none", profile.InProgressRequestID)
		case everdeenmodel.ReasonInvalidArguments.TwirpError().Meta(ReasonCodeKey):
			return r.DisableProfileAndRecordFailure(ctx, profile.UserId, DisabledReasonInvalidArguments, "none", profile.InProgressRequestID)
		case everdeenmodel.ReasonIneligiblePurchase.TwirpError().Meta(ReasonCodeKey):
			return r.DisableProfileAndRecordFailure(ctx, profile.UserId, DisabledReasonPurchaseVelocity, "none", profile.InProgressRequestID)
		case everdeenmodel.ReasonInvalidPaymentInstrument.TwirpError().Meta(ReasonCodeKey):
			return r.DisableProfileAndRecordFailure(ctx, profile.UserId, DisabledReasonInvalidPayment, "none", profile.InProgressRequestID)
		case everdeenmodel.ReasonVelocityLimit.TwirpError().Meta(ReasonCodeKey):
			return r.HandleProfileHitPurchaseVelocity(ctx, profile)
		case everdeenmodel.ReasonFraudDeclined.TwirpError().Meta(ReasonCodeKey):
			return r.DisableProfileAndRecordFailure(ctx, profile.UserId, DisabledReasonCardDeclined, "none", profile.InProgressRequestID)

		default:
			err = r.sendRetryNotification(profile, err)
			return &profile, err
		}
	} else {
		log.WithError(err).Error("Error converting everdeen error into twirp error.")
		err = r.sendRetryNotification(profile, err)
		return &profile, err
	}
}

func (r *refill) sendRetryNotification(profile auto_refill_settings.AutoRefillSettingsProfile, err error) error {
	//send sqs message
	envelope := everdeenmodel.PreauthPurchaseResponse{
		UserID:     profile.UserId,
		Status:     500,
		ReasonCode: 5000,
		Message:    err.Error(),
		RequestID:  profile.InProgressRequestID,
	}

	msg, err := json.Marshal(envelope)

	if err != nil {
		log.WithError(err).Error("Error serializing json PreauthPurchaseResponse for auto refill retries.")
		return err
	}

	msg, err = json.Marshal(models.SNSMessage{
		Message: string(msg),
	})

	if err != nil {
		log.WithError(err).Error("Error serializing json SNSMessage for auto refill retries.")
		return err
	}

	strMsg := string(msg)

	_, err = r.SQSClient.SendMessage(&sqs.SendMessageInput{
		MessageBody: &strMsg,
		QueueUrl:    &r.RetryQueueURL,
	})
	if err != nil {
		log.WithError(err).Error("Error sending json SQSMessage for auto refill retries.")
		return err
	}

	return nil
}

func (r *refill) sendUpdatedSettingsNotification(ctx context.Context, profile auto_refill_settings.AutoRefillSettingsProfile, reason string) error {
	switch reason {
	case EnabledState:
		return r.craftAndSendNotification(ctx, profile, DartKeyNewConfirmation)

	case DisabledState:
		return r.craftAndSendNotification(ctx, profile, DartKeyUserDisabled)

	case UpdatedAction:
		return r.craftAndSendNotification(ctx, profile, DartKeyUpdateConfirmation)

	case DisabledReasonTwoFactor:
		return r.craftAndSendNotification(ctx, profile, DartKeyUserDisabledTwoFactor)

	case DisabledReasonPurchaseVelocity:
		_, err := r.HandleProfileHitPurchaseVelocity(ctx, profile)
		return err

	case DisabledReasonCardDeclined:
		return r.craftAndSendNotification(ctx, profile, DartKeyUserDisabledCardDeclined)

	case DisabledReasonInvalidPayment:
		return r.craftAndSendNotification(ctx, profile, DartKeyUserDisabledInvalidPayment)
	default:
		log.Errorf("Error processing what kind of DART notification needed for auto refill action reason %s", reason)
		return errors.New("Error processing auto refill notification with reason: " + reason)
	}
}

func (r *refill) craftAndSendNotification(ctx context.Context, profile auto_refill_settings.AutoRefillSettingsProfile, notificationType string) error {
	metadata := make(map[string]string)
	metadata["user_id"] = profile.UserId
	metadata["BitsRefillQuantity"] = strconv.Itoa(int(profile.BitsQuantity))
	metadata["BitsRefillThreshold"] = strconv.Itoa(int(profile.Threshold))

	request := &dart.PublishNotificationRequest{
		Recipient:                      &dart.PublishNotificationRequest_RecipientId{RecipientId: profile.UserId},
		NotificationType:               notificationType,
		Metadata:                       metadata,
		DropNotificationBeforeDelivery: false,
	}
	resp, err := r.DartReceiver.PublishNotification(ctx, request)
	if err != nil {
		log.WithError(err).Errorf("Publishing notification for auto refill notificationt type: %s", notificationType)
		return err
	}
	log.Infof("Successfully published autorefill %s notification, tracking id: %s", notificationType, resp.NotificationTraceId)

	return nil
}

func (r *refill) DisableProfileAndRecordFailure(ctx context.Context, userId string, reason string, transactionId string, requestId string) (*auto_refill_settings.AutoRefillSettingsProfile, error) {
	profile, err := r.Get(ctx, userId)

	if err != nil && profile != nil {
		log.WithError(err).Errorf("Issue getting auto refill profile to disable processing auto refill failure. Disable Reason: %s", reason)
		return nil, err
	}

	if profile.State == DisabledState {
		return profile, nil
	}

	profile.State = DisabledState
	profile.InProgressRequestID = ""
	profile.AttemptCount = 0

	_, err = r.Set(ctx, *profile, reason)

	if err != nil {
		log.WithError(err).Errorf("Issue setting auto refill profile to disable processing auto refill failure. Disable Reason: %s", reason)
		return nil, err
	}

	err = r.writeDynamoFailureRecord(*profile, reason)

	if err != nil {
		log.WithError(err).Errorf("Issue writing auto refill audit record to dynamo.")
	}

	return profile, nil
}

func (r *refill) HandleProfileHitPurchaseVelocity(ctx context.Context, profile auto_refill_settings.AutoRefillSettingsProfile) (*auto_refill_settings.AutoRefillSettingsProfile, error) {
	// send email in this case, note attempt in dynamo and back out
	profile.PauseUntilTime = time.Now().Add(time.Hour * 24)
	_, err := r.Dao.SetSettings(&profile)

	if err != nil {
		log.WithError(err).Error("Error attempting to update users profile with refill purchase velocity time")
		return nil, err
	}

	r.Cache.Delete(ctx, profile.UserId)
	err = r.craftAndSendNotification(context.Background(), profile, DartKeyUserDisabledPurchaseVelocity)

	if err != nil {
		log.WithError(err).Errorf("Issue sending auto refill purchase velocity dart notification")
	}

	err = r.writeDynamoFailureRecord(profile, DisabledReasonPurchaseVelocity)

	if err != nil {
		log.WithError(err).Errorf("Issue sending auto refill purchase velocity dynamo record")
	}

	return &profile, nil
}

func (r *refill) writeDynamoFailureRecord(profile auto_refill_settings.AutoRefillSettingsProfile, reason string) error {
	balance, err := r.BalanceClient.GetBalance(context.Background(), profile.UserId, false)

	if err != nil {
		return err
	}

	return r.RefillRecordDao.WriteRecord(&auto_refill_settings.AutoRefillPurchaseAttemptRecord{
		ProfileID:     profile.Id,
		UserID:        profile.UserId,
		Status:        auto_refill_settings.StatusFailure,
		StatusNote:    reason,
		Time:          time.Now().Format(time.RFC3339),
		BalanceAtTime: int32(balance),
		Threshold:     profile.Threshold,
		RequestID:     profile.InProgressRequestID,
		OfferID:       profile.OfferId,
	})
}
