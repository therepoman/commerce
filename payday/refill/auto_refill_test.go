package refill

import (
	"context"
	"testing"
	"time"

	cache_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/cache"

	receiver "code.justin.tv/amzn/TwitchDartReceiverTwirp"
	"code.justin.tv/commerce/payday/dynamo/auto_refill_settings"
	receiver_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/amzn/TwitchDartReceiverTwirp"
	balance_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/balance"
	datascience_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/datascience"
	mocks "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo/auto_refill_settings"
	products_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/products"
	product_availability_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/products/availability"
	petozi "code.justin.tv/commerce/petozi/rpc"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

const (
	userID                   = "userID"
	tpl                      = BitsTPL
	chargeInstrumentID       = "instrumentID"
	state                    = EnabledState
	chargeInstrumentCategory = "category"
	id                       = "ID"
	id2                      = "ID2"
	offerID                  = "offer"
)

func TestAutoRefillImpl_Get(t *testing.T) {

	Convey("Testing Get", t, func() {
		ctx := context.Background()
		mockBalance := new(balance_mock.Getter)
		mockRefillCache := new(cache_mock.AutoRefillProfileCache)
		mockDao := new(mocks.AutoRefillDao)
		mockReceiver := new(receiver_mock.Receiver)
		mockPetozi := new(products_mock.Fetcher)

		r := refill{
			BalanceClient:   mockBalance,
			Cache:           mockRefillCache,
			Dao:             mockDao,
			DartReceiver:    mockReceiver,
			ProductsFetcher: mockPetozi,
		}

		createdAt := time.Now().Format(time.RFC3339)
		Convey("Testing Get finds item in cache", func() {

			response := auto_refill_settings.AutoRefillSettingsProfile{
				UserId:                     userID,
				State:                      state,
				Tpl:                        tpl,
				ChargeInstrumentCategory:   chargeInstrumentCategory,
				ChargeInstrumentId:         chargeInstrumentID,
				OfferId:                    offerID,
				IsEmailNotificationEnabled: true,
				Threshold:                  100,
				Id:                         id,
				CreatedAt:                  createdAt,
			}
			mockRefillCache.On("Get", mock.Anything, userID).Return(&response, true, nil).Once()
			conveyedResponse, err := r.Get(ctx, userID)

			So(err, ShouldBeNil)
			So(response.UserId, ShouldEqual, conveyedResponse.UserId)
			So(response.State, ShouldEqual, conveyedResponse.State)

		})
		Convey("Testing Get finds nil item in cache", func() {
			mockRefillCache.On("Get", mock.Anything, userID).Return(nil, true, nil).Once()
			conveyedResponse, err := r.Get(ctx, userID)

			So(err, ShouldBeNil)
			So(conveyedResponse, ShouldBeNil)
		})
		Convey("Testing Get finds no item in cache but one in the DB", func() {
			var profiles []auto_refill_settings.AutoRefillSettingsProfile
			profiles = append(profiles, auto_refill_settings.AutoRefillSettingsProfile{
				UserId: userID,
				Id:     id,
				Tpl:    BitsTPL,
			})
			mockRefillCache.On("Get", mock.Anything, userID).Return(nil, false, nil).Once()
			mockDao.On("GetActiveSettings", userID).Return(profiles, nil).Once()
			mockRefillCache.On("Set", mock.Anything, userID, mock.Anything).Return().Once()
			conveyedResponse, err := r.Get(ctx, userID)
			So(err, ShouldBeNil)
			So(conveyedResponse.UserId, ShouldEqual, userID)
			So(conveyedResponse.Id, ShouldEqual, id)

		})
		Convey("Testing Get finds no item in cache but a disabled one in the DB", func() {
			var profiles []auto_refill_settings.AutoRefillSettingsProfile
			profile := auto_refill_settings.AutoRefillSettingsProfile{
				UserId: userID,
				Id:     id,
				Tpl:    BitsTPL,
			}
			mockRefillCache.On("Get", mock.Anything, userID).Return(nil, false, nil).Once()
			mockDao.On("GetActiveSettings", userID).Return(profiles, nil).Once()
			mockRefillCache.On("Set", mock.Anything, userID, mock.Anything).Return().Once()
			mockDao.On("GetMostRecentSettings", userID).Return(&profile, nil).Once()
			conveyedResponse, err := r.Get(ctx, userID)
			So(err, ShouldBeNil)
			So(conveyedResponse.UserId, ShouldEqual, userID)
			So(conveyedResponse.Id, ShouldEqual, id)
		})

		Convey("Testing Get finds no item in cache but two in the DB and returns the right one", func() {
			var profiles []auto_refill_settings.AutoRefillSettingsProfile
			profiles = append(profiles, auto_refill_settings.AutoRefillSettingsProfile{
				UserId:    userID,
				Id:        id,
				Tpl:       BitsTPL,
				CreatedAt: "2018-01-01",
			})
			// this second profile has a later creation date so it should be the one chosen
			profiles = append(profiles, auto_refill_settings.AutoRefillSettingsProfile{
				UserId:    userID,
				Id:        id2,
				Tpl:       BitsTPL,
				CreatedAt: "2019-02-02",
			})
			mockRefillCache.On("Get", mock.Anything, userID).Return(nil, false, nil).Once()
			mockDao.On("GetActiveSettings", userID).Return(profiles, nil).Once()
			mockRefillCache.On("Set", mock.Anything, userID, mock.Anything).Return().Once()
			conveyedResponse, err := r.Get(ctx, userID)
			So(err, ShouldBeNil)
			So(conveyedResponse.UserId, ShouldEqual, userID)
			So(conveyedResponse.Id, ShouldEqual, id2)
			So(conveyedResponse.Id, ShouldNotEqual, id)
		})

	})
}

func TestAutoRefillImpl_Set(t *testing.T) {

	Convey("Testing Set", t, func() {
		mockLimiter := new(product_availability_mock.ProductAvailabilityChecker)
		mockBalance := new(balance_mock.Getter)
		mockRefillCache := new(cache_mock.AutoRefillProfileCache)
		mockDao := new(mocks.AutoRefillDao)
		mockReceiver := new(receiver_mock.Receiver)
		mockDS := new(datascience_mock.DataScience)
		mockPetozi := new(products_mock.Fetcher)

		r := refill{
			Limiter:           mockLimiter,
			BalanceClient:     mockBalance,
			Cache:             mockRefillCache,
			Dao:               mockDao,
			DartReceiver:      mockReceiver,
			DataScienceClient: mockDS,
			ProductsFetcher:   mockPetozi,
		}

		Convey("Testing Set finds no previous item", func() {
			profile := auto_refill_settings.AutoRefillSettingsProfile{
				UserId:                     userID,
				Tpl:                        BitsTPL,
				Threshold:                  1000,
				IsEmailNotificationEnabled: true,
				State:                      EnabledState,
				OfferId:                    offerID,
			}

			var products []*petozi.BitsProduct
			products = append(products, &petozi.BitsProduct{
				OfferId: offerID,
				Id:      id,
			})
			mockPetozi.On("FetchAllFromPetozi", mock.Anything).Return(products, nil)
			mockDS.On("TrackBitsEvent", mock.Anything, mock.Anything, mock.Anything).Return(nil)
			mockRefillCache.On("Delete", mock.Anything, userID).Return().Times(2)
			mockRefillCache.On("Get", mock.Anything, userID).Return(nil, false, nil).Once()
			var profiles []auto_refill_settings.AutoRefillSettingsProfile
			mockDao.On("GetActiveSettings", userID).Return(profiles, nil).Once()
			mockDao.On("SetSettings", mock.Anything).Return(nil, nil).Once()
			mockDao.On("SetSettings", mock.Anything).Return(&profile, nil).Once()
			mockDao.On("GetExactSettings", mock.Anything, mock.Anything).Return(&profile, nil).Once()
			mockBalance.On("GetBalance", mock.Anything, userID, false).Return(int32(100), nil)
			mockLimiter.On("GetPurchasableQuantityByInventoryLimit", mock.Anything, mock.Anything, profile.UserId).Return(1, nil)
			mockLimiter.On("GetPurchasableByDailyPurchaseLimit", mock.Anything, mock.Anything, profile.UserId).Return(1, nil)
			mockReceiver.On("PublishNotification", mock.Anything, mock.Anything).Return(&receiver.PublishNotificationResponse{
				NotificationTraceId: "TEST_ID",
			}, nil)

			resp, err := r.Set(context.Background(), profile, ManualUpdateType)
			So(err, ShouldBeNil)
			So(resp, ShouldNotBeEmpty)
			So(len(mockReceiver.Calls), ShouldEqual, 1)
			notificationRequest := mockReceiver.Calls[0].Arguments[1].(*receiver.PublishNotificationRequest)
			So(notificationRequest.NotificationType, ShouldEqual, DartKeyNewConfirmation)
		})

		Convey("Testing Set finds the previous item with id", func() {
			profile := auto_refill_settings.AutoRefillSettingsProfile{
				Id:                         "test_id",
				UserId:                     userID,
				Tpl:                        BitsTPL,
				Threshold:                  1000,
				IsEmailNotificationEnabled: true,
				OfferId:                    offerID,
			}
			previousProfile := auto_refill_settings.AutoRefillSettingsProfile{
				Id:                         "test_id",
				UserId:                     userID,
				Tpl:                        BitsTPL,
				Threshold:                  100,
				IsEmailNotificationEnabled: true,
				OfferId:                    offerID,
			}

			var products []*petozi.BitsProduct
			products = append(products, &petozi.BitsProduct{
				OfferId: offerID,
				Id:      id,
			})
			mockPetozi.On("FetchAllFromPetozi", mock.Anything).Return(products, nil)
			mockDS.On("TrackBitsEvent", mock.Anything, mock.Anything, mock.Anything).Return(nil)
			mockRefillCache.On("Delete", mock.Anything, userID).Return().Once()
			mockRefillCache.On("Get", mock.Anything, userID).Return(nil, false, nil).Once()
			mockDao.On("GetSettingsByID", profile.Id).Return(&previousProfile, nil).Once()
			mockDao.On("SetSettings", mock.Anything).Return(nil, nil).Once()
			mockBalance.On("GetBalance", mock.Anything, userID, false).Return(int32(100), nil)
			mockLimiter.On("GetPurchasableQuantityByInventoryLimit", mock.Anything, mock.Anything, profile.UserId).Return(1, nil)
			mockLimiter.On("GetPurchasableByDailyPurchaseLimit", mock.Anything, mock.Anything, profile.UserId).Return(1, nil)
			mockReceiver.On("PublishNotification", mock.Anything, mock.Anything).Return(&receiver.PublishNotificationResponse{
				NotificationTraceId: "TEST_ID",
			}, nil)

			resp, err := r.Set(context.Background(), profile, ManualUpdateType)
			So(err, ShouldBeNil)
			So(resp, ShouldNotBeEmpty)
			So(len(mockReceiver.Calls), ShouldEqual, 1)
			notificationRequest := mockReceiver.Calls[0].Arguments[1].(*receiver.PublishNotificationRequest)
			So(notificationRequest.NotificationType, ShouldEqual, DartKeyUpdateConfirmation)
		})

		Convey("Testing Set finds the previous item with id, with user disabling it.", func() {
			profile := auto_refill_settings.AutoRefillSettingsProfile{
				Id:                         "test_id",
				UserId:                     userID,
				Tpl:                        BitsTPL,
				Threshold:                  1000,
				State:                      DisabledState,
				IsEmailNotificationEnabled: true,
				OfferId:                    offerID,
			}
			previousProfile := auto_refill_settings.AutoRefillSettingsProfile{
				Id:                         "test_id",
				UserId:                     userID,
				Tpl:                        BitsTPL,
				Threshold:                  100,
				State:                      EnabledState,
				IsEmailNotificationEnabled: true,
				OfferId:                    offerID,
			}

			var products []*petozi.BitsProduct
			products = append(products, &petozi.BitsProduct{
				OfferId: offerID,
				Id:      id,
			})
			mockPetozi.On("FetchAllFromPetozi", mock.Anything).Return(products, nil)
			mockDS.On("TrackBitsEvent", mock.Anything, mock.Anything, mock.Anything).Return(nil)
			mockRefillCache.On("Delete", mock.Anything, userID).Return().Once()
			mockRefillCache.On("Get", mock.Anything, userID).Return(nil, false, nil).Once()
			mockDao.On("GetSettingsByID", profile.Id).Return(&previousProfile, nil).Once()
			mockDao.On("SetSettings", mock.Anything).Return(nil, nil).Once()
			mockBalance.On("GetBalance", mock.Anything, userID, "", false).Return(int32(100), nil)
			mockLimiter.On("GetPurchasableQuantityByInventoryLimit", mock.Anything, mock.Anything, profile.UserId).Return(1, nil)
			mockLimiter.On("GetPurchasableByDailyPurchaseLimit", mock.Anything, mock.Anything, profile.UserId).Return(1, nil)
			mockReceiver.On("PublishNotification", mock.Anything, mock.Anything).Return(&receiver.PublishNotificationResponse{
				NotificationTraceId: "TEST_ID",
			}, nil)

			resp, err := r.Set(context.Background(), profile, ManualUpdateType)
			So(err, ShouldBeNil)
			So(resp, ShouldNotBeEmpty)
			So(len(mockReceiver.Calls), ShouldEqual, 1)
			notificationRequest := mockReceiver.Calls[0].Arguments[1].(*receiver.PublishNotificationRequest)
			So(notificationRequest.NotificationType, ShouldEqual, DartKeyUserDisabled)
		})

		Convey("Testing Set finds the previous item with id with user id not matching", func() {
			profile := auto_refill_settings.AutoRefillSettingsProfile{
				Id:                         "test_id",
				UserId:                     userID,
				Tpl:                        BitsTPL,
				Threshold:                  1000,
				IsEmailNotificationEnabled: true,
				OfferId:                    offerID,
			}
			previousProfile := auto_refill_settings.AutoRefillSettingsProfile{
				Id:                         "test_id",
				UserId:                     "another_user_id",
				Tpl:                        BitsTPL,
				Threshold:                  100,
				IsEmailNotificationEnabled: true,
				OfferId:                    offerID,
			}

			var products []*petozi.BitsProduct
			products = append(products, &petozi.BitsProduct{
				OfferId: offerID,
				Id:      id,
			})
			mockPetozi.On("FetchAllFromPetozi", mock.Anything).Return(products, nil)
			mockDS.On("TrackBitsEvent", mock.Anything, mock.Anything, mock.Anything).Return(nil)
			mockRefillCache.On("Delete", mock.Anything, userID).Return().Once()
			mockRefillCache.On("Get", mock.Anything, userID).Return(nil, false, nil).Once()
			mockDao.On("GetSettingsByID", profile.Id).Return(&previousProfile, nil).Once()
			mockDao.On("SetSettings", mock.Anything).Return(nil, nil).Once()
			mockBalance.On("GetBalance", mock.Anything, userID, "", false).Return(int32(100), nil)
			mockLimiter.On("GetPurchasableQuantityByInventoryLimit", mock.Anything, mock.Anything, profile.UserId).Return(1, nil)
			mockLimiter.On("GetPurchasableByDailyPurchaseLimit", mock.Anything, mock.Anything, profile.UserId).Return(1, nil)

			resp, err := r.Set(context.Background(), profile, ManualUpdateType)
			So(err, ShouldNotBeNil)
			So(resp, ShouldBeNil)
			So(len(mockReceiver.Calls), ShouldEqual, 0)
		})
	})
}
