package user

import (
	"context"

	"code.justin.tv/commerce/payday/cache/user"
	"code.justin.tv/commerce/payday/dynamo"
)

type Setter interface {
	Set(ctx context.Context, userID string, userUpdates *dynamo.User) error
}

func NewSetter() Setter {
	return &setter{}
}

type setter struct {
	Fetcher Fetcher         `inject:""`
	Cache   user.Cache      `inject:""`
	Dao     dynamo.IUserDao `inject:""`
}

func (s *setter) Set(ctx context.Context, userID string, userUpdates *dynamo.User) error {
	userRecord, err := s.Fetcher.Fetch(ctx, userID)
	if err != nil {
		return err
	}

	if userRecord == nil {
		err := s.Dao.Put(userUpdates)
		if err != nil {
			return err
		}
	} else {
		err := s.Dao.Update(userUpdates)
		if err != nil {
			return err
		}
	}

	s.Cache.Del(ctx, userID)

	return nil
}
