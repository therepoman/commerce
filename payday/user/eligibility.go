package user

import (
	"context"

	log "code.justin.tv/commerce/logrus"
)

type Checker interface {
	IsEligible(ctx context.Context, userID string) (bool, error)
}

func NewChecker() Checker {
	return &checker{}
}

type checker struct {
	Fetcher Fetcher `inject:""`
}

func (c *checker) IsEligible(ctx context.Context, userID string) (bool, error) {
	user, err := c.Fetcher.Fetch(ctx, userID)
	if err != nil {
		log.WithError(err).WithField("userID", userID).Error("could not fetch user")
		return false, err
	}

	if user == nil {
		return true, nil
	}

	return !user.Banned, nil
}
