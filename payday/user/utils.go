package user

import "regexp"

var testIdRegex, _ = regexp.Compile("(^7357[0-9]{14})|(^perf)") // 18-digit numbers that begin with 7357

/* Determines if a user ID belongs to a test (fake) account */
func IsATestAccount(userId string) bool {
	return testIdRegex.MatchString(userId)
}
