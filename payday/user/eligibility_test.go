package user

import (
	"context"
	"testing"

	"github.com/stretchr/testify/mock"

	"code.justin.tv/commerce/payday/dynamo"
	user_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/user"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
)

func TestChecker_IsEligible(t *testing.T) {
	Convey("given a user checker", t, func() {
		ctx := context.Background()
		fetcher := new(user_mock.Fetcher)

		checker := &checker{
			Fetcher: fetcher,
		}

		userID := "123123123"
		user := &dynamo.User{
			Id:     dynamo.UserId(userID),
			Banned: false,
		}

		Convey("when the fetcher fails", func() {
			fetcher.On("Fetch", mock.Anything, userID).Return(nil, errors.New("WALRUS STRIKE"))

			Convey("then return they are ineligible", func() {
				isEligible, err := checker.IsEligible(ctx, userID)

				So(err, ShouldNotBeNil)
				So(isEligible, ShouldBeFalse)
			})
		})

		Convey("when the fetcher returns a nil user", func() {
			fetcher.On("Fetch", mock.Anything, userID).Return(nil, nil)

			Convey("then return they are eligible", func() {
				isEligible, err := checker.IsEligible(ctx, userID)

				So(err, ShouldBeNil)
				So(isEligible, ShouldBeTrue)
			})
		})

		Convey("when the fetcher returns a user", func() {
			fetcher.On("Fetch", mock.Anything, userID).Return(user, nil)

			Convey("then return if the user is not banned", func() {
				isEligible, err := checker.IsEligible(ctx, userID)

				So(err, ShouldBeNil)
				So(isEligible, ShouldEqual, !user.Banned)
			})
		})
	})
}
