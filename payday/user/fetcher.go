package user

import (
	"context"

	"code.justin.tv/commerce/payday/cache/user"
	"code.justin.tv/commerce/payday/dynamo"
)

type Fetcher interface {
	Fetch(ctx context.Context, userID string) (*dynamo.User, error)
}

func NewFetcher() Fetcher {
	return &fetcher{}
}

type fetcher struct {
	Cache user.Cache      `inject:""`
	Dao   dynamo.IUserDao `inject:""`
}

func (f *fetcher) Fetch(ctx context.Context, userID string) (*dynamo.User, error) {
	cachedRecord := f.Cache.Get(ctx, userID)

	if cachedRecord != nil {
		return cachedRecord, nil
	}

	record, err := f.Dao.Get(dynamo.UserId(userID))
	if err != nil || record == nil {
		return nil, err
	}

	f.Cache.Set(ctx, userID, record)

	return record, nil
}
