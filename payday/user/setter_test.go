package user

import (
	"context"
	"testing"

	"github.com/stretchr/testify/mock"

	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/errors"
	usercache_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/cache/user"
	dynamo_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo"
	user_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/user"
	. "github.com/smartystreets/goconvey/convey"
)

func TestSetter_Set(t *testing.T) {
	Convey("given a user setter", t, func() {
		ctx := context.Background()
		fetcher := new(user_mock.Fetcher)
		cache := new(usercache_mock.Cache)
		dao := new(dynamo_mock.IUserDao)

		setter := &setter{
			Fetcher: fetcher,
			Cache:   cache,
			Dao:     dao,
		}

		userID := "123123123"
		user := &dynamo.User{
			Id:     dynamo.UserId(userID),
			Banned: false,
		}

		Convey("if the fetcher fails", func() {
			fetcher.On("Fetch", mock.Anything, userID).Return(nil, errors.New("WALRUS STRIKE"))

			Convey("then we return the error", func() {
				err := setter.Set(ctx, userID, user)

				So(err, ShouldNotBeNil)
			})
		})

		Convey("if the fetcher finds no user", func() {
			fetcher.On("Fetch", mock.Anything, userID).Return(nil, nil)

			Convey("if the dynamo put fails", func() {
				dao.On("Put", user).Return(errors.New("WALRUS STRIKE"))

				Convey("then we return the error", func() {
					err := setter.Set(ctx, userID, user)

					So(err, ShouldNotBeNil)
				})
			})

			Convey("if the dynamo put succeeds", func() {
				dao.On("Put", user).Return(nil)

				Convey("then we return nil and delete the cache", func() {
					cache.On("Del", mock.Anything, userID).Return()

					err := setter.Set(ctx, userID, user)

					So(err, ShouldBeNil)
					cache.AssertCalled(t, "Del", mock.Anything, userID)
				})
			})
		})

		Convey("if the fetcher finds a user", func() {
			fetcher.On("Fetch", mock.Anything, userID).Return(user, nil)

			Convey("if the dynamo update fails", func() {
				dao.On("Update", user).Return(errors.New("WALRUS STRIKE"))

				Convey("then we return the error", func() {
					err := setter.Set(ctx, userID, user)

					So(err, ShouldNotBeNil)
				})
			})

			Convey("if the dynamo update succeeds", func() {
				dao.On("Update", user).Return(nil)

				Convey("then we return nil and delete the cache", func() {
					cache.On("Del", mock.Anything, userID).Return()

					err := setter.Set(ctx, userID, user)

					So(err, ShouldBeNil)
					cache.AssertCalled(t, "Del", mock.Anything, userID)
				})
			})
		})
	})
}
