package user

import (
	"context"
	"testing"

	"github.com/stretchr/testify/mock"

	"code.justin.tv/commerce/payday/dynamo"
	user_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/cache/user"
	dynamo_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo"
	"github.com/pkg/errors"

	. "github.com/smartystreets/goconvey/convey"
)

func TestFetcher_Fetch(t *testing.T) {
	Convey("given a user fetcher", t, func() {
		ctx := context.Background()
		cache := new(user_mock.Cache)
		dao := new(dynamo_mock.IUserDao)

		fetcher := &fetcher{
			Cache: cache,
			Dao:   dao,
		}

		userID := "123123123"
		user := &dynamo.User{
			Id:     dynamo.UserId(userID),
			Banned: false,
		}

		Convey("when the cache contains an entry for the requested user", func() {
			cache.On("Get", mock.Anything, userID).Return(user)

			Convey("then we return the cached entry", func() {
				actual, err := fetcher.Fetch(ctx, userID)

				So(err, ShouldBeNil)
				So(actual, ShouldNotBeNil)
			})
		})

		Convey("when we do not have a cached entry", func() {
			cache.On("Get", mock.Anything, userID).Return(nil)

			Convey("when the user dao fails to lookup the user", func() {
				dao.On("Get", dynamo.UserId(userID)).Return(nil, errors.New("WALRUS STRIKE"))

				Convey("then we return an error", func() {
					actual, err := fetcher.Fetch(ctx, userID)

					So(err, ShouldNotBeNil)
					So(actual, ShouldBeNil)
				})
			})

			Convey("when there is no user entry", func() {
				dao.On("Get", dynamo.UserId(userID)).Return(nil, nil)

				Convey("then we return nil", func() {
					actual, err := fetcher.Fetch(ctx, userID)

					So(err, ShouldBeNil)
					So(actual, ShouldBeNil)
				})
			})

			Convey("when we find a user in the dao", func() {
				dao.On("Get", dynamo.UserId(userID)).Return(user, nil)

				Convey("then we cache the user and return the user we found", func() {
					cache.On("Set", mock.Anything, userID, user).Return()
					actual, err := fetcher.Fetch(ctx, userID)

					So(err, ShouldBeNil)
					So(actual, ShouldNotBeNil)

					cache.AssertCalled(t, "Set", mock.Anything, userID, user)
				})
			})
		})
	})
}
