package validator

import (
	"context"
	"log"

	"code.justin.tv/foundation/twitchclient"
	"code.justin.tv/gds/gds/extensions/validator/client/s2s"
	"code.justin.tv/gds/gds/extensions/validator/model"
	"code.justin.tv/sse/malachai/pkg/s2s/caller"
)

// This is a stub validator client, not to be used in production.
type ClientStub struct {
}

// This generates a stub ValidatorClient that won't sign things, since we don't read off the queue, this should be fine.
// Does not require S2S to be setup.
func NewNoOp() s2s.Client {
	return &ClientStub{}
}

// This generates a real validator client client, I consolidated this logic here to make main.go ever so slightly cleaner.
// Requires S2S to be setup.
func New(url string, name string) s2s.Client {
	config := twitchclient.ClientConf{
		Host: url,
	}
	client, err := s2s.NewClient(name, config, caller.Config{})
	if err != nil {
		log.Fatalf("Could not create Validator S2S client: %v", err)
	}

	return client
}

// This will fail to create a signed JWT for bits in extensions usage, this is not expected to be a concern in development.
func (client *ClientStub) CreateBitsTransactionToken(ctx context.Context, clientId, userId, transactionId, time string, product model.Product, reqOpts *twitchclient.ReqOpts) (*model.PermissionData, error) {
	return nil, nil
}
