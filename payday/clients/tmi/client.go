package tmi

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/httputils"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/user"
)

const USER_NOTICE_ROUTE = "/internal/usernotice"
const BANNED_CHATTER_ROUTE_FORMAT = "/rooms/%s/bans/%s"
const OFFICIAL_TWITCH_USER_ID = 12826 //twitch username

const (
	// Retries the call to TMI 8 times with exponential backoff
	// Delays works out to: 50ms 100ms 200ms 400ms 800ms 1600ms 2000ms
	retryAttempts        = 8
	initialRetryDelay    = 50 * time.Millisecond
	retryDelayMultiplier = 2
	maxRetryDelay        = 2 * time.Second
)

type ITMIClient interface {
	SendUserNotice(params models.UserNoticeParams) error
	SendSystemMessage(params SystemMessageParams) error
	CheckUserBannedInChannel(ctx context.Context, userId, channelId string) (bool, error)
}

type TMIClient struct {
	client httputils.HttpUtilClient
	host   string
}

type SendMessageParams struct {
	Sender    string
	Recipient string
	Amount    int
	Message   string
}

type SystemMessageParams struct {
	Recipient string
	Message   string
	ID        string
	Params    []models.UserNoticeMsgParam
}

type TMIResponse struct {
	Test string
}

func NewTMIClient(client httputils.HttpUtilClient, host string) ITMIClient {
	return &TMIClient{
		client: client,
		host:   host,
	}
}

func NewHttpTMIClient(host string) ITMIClient {
	return NewTMIClient(
		httputils.NewHttpUtilClientApiWithCustomRetryDelayFunc(
			retryAttempts,
			httputils.NewExponentialBackoffRetryFunction(
				initialRetryDelay,
				retryDelayMultiplier,
				maxRetryDelay)),
		host)
}

func (this *TMIClient) SendUserNotice(params models.UserNoticeParams) error {
	body, err := json.Marshal(params)

	if err != nil {
		msg := "TMIClient: Error while marshaling UserNoticeParams"
		log.WithError(err).Errorf(msg)
		return errors.New(msg)
	}

	url := this.host + USER_NOTICE_ROUTE
	_, statusCode, err := this.client.HttpPost(context.Background(), url, bytes.NewReader(body))

	if err != nil {
		msg := "TMIClient: Error while creating new request for SendUserNotice"
		log.WithError(err).Errorf(msg)
		return errors.Notef(err, msg)
	}

	if statusCode != http.StatusOK {
		msg := "TMIClient: Tmi UserNotice request failed while sending payload"
		log.WithField("statusCode", statusCode).Error(msg)
		return errors.New(msg)
	}

	return nil
}

func (this *TMIClient) SendSystemMessage(params SystemMessageParams) error {
	recipientId, err := strconv.ParseInt(params.Recipient, 10, 64)
	if err != nil {
		msg := "TMIClient: Error non-numeric recipientId used in SendSystemMessage"
		log.WithError(err).Error(msg)
		return errors.New(msg)
	}

	return this.SendUserNotice(models.UserNoticeParams{
		SenderUserID:      OFFICIAL_TWITCH_USER_ID,
		TargetChannelID:   recipientId,
		Body:              "",
		MsgId:             params.ID,
		MsgParams:         params.Params,
		DefaultSystemBody: params.Message,
	})
}

func (this *TMIClient) CheckUserBannedInChannel(ctx context.Context, userId, channelId string) (bool, error) {
	if user.IsATestAccount(userId) {
		return false, nil
	}

	url := this.host + fmt.Sprintf(BANNED_CHATTER_ROUTE_FORMAT, channelId, userId)
	_, status, err := this.client.HttpGet(ctx, url)
	if status == http.StatusNotFound {
		// TMI returned 404 - User is not banned
		return false, nil
	} else if status == http.StatusOK {
		// TMI returned success - User is banned or timed out
		return true, nil
	} else {
		// TMI returned an unexpected error
		return false, err
	}
}
