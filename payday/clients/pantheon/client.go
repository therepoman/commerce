package pantheon

import (
	"context"
	"fmt"
	"net/http"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	"code.justin.tv/commerce/payday/errors"
	httperror "code.justin.tv/commerce/payday/errors/http"
	"code.justin.tv/commerce/payday/httputils"
	"code.justin.tv/commerce/payday/hystrix"
	cmd "code.justin.tv/commerce/payday/hystrix"
)

const (
	PantheonBitsUsageDomain = "bits-usage-by-channel-v1"
)

const (
	attempts             = 3
	initialRetryDelay    = 20 * time.Millisecond
	retryDelayMultiplier = 2
	maxRetryDelay        = 100 * time.Millisecond
)

var retryDelayFunc = httputils.NewExponentialBackoffRetryFunction(initialRetryDelay, retryDelayMultiplier, maxRetryDelay)

type IPantheonClientWrapper interface {
	GetLeaderboard(ctx context.Context, domain string, timeUnit pantheonrpc.TimeUnit, groupingKey string, contextEntryKey string, topN int64, contextN int64, useInMemoryCache bool) (*pantheonrpc.GetLeaderboardResp, error)
	GetLeaderboards(ctx context.Context, domain string, timeUnit pantheonrpc.TimeUnit, groupingKeys []string, contextEntryKey string, topN int64, contextN int64, useInMemoryCache bool) ([]*pantheonrpc.GetLeaderboardResp, error)
	GetBucketedLeaderboardCounts(ctx context.Context, domain string, timeUnit pantheonrpc.TimeUnit, groupingKey string, bucketThresholds []int64, includeModeratedEntries bool) (*pantheonrpc.GetBucketedLeaderboardCountsResp, error)
	GetLeaderboardEntriesAboveThreshold(ctx context.Context, domain string, timeUnit pantheonrpc.TimeUnit, groupingKey string, threshold int64, limit int64, offset int64) (*pantheonrpc.GetLeaderboardEntriesAboveThresholdResp, error)
	DeleteLeaderboards(ctx context.Context, userID string) error
	PublishEvent(ctx context.Context, req pantheonrpc.PublishEventReq) error
	ModerateEntry(ctx context.Context, req pantheonrpc.ModerateEntryReq) error
	DeleteEntries(ctx context.Context, userID string) error
	HealthCheck(ctx context.Context, req pantheonrpc.HealthCheckReq) error
}

type PantheonClientWrapper struct {
	Pantheon pantheonrpc.Pantheon
}

func NewPantheonClientWrapper(pantheonClient pantheonrpc.Pantheon) IPantheonClientWrapper {
	return &PantheonClientWrapper{
		Pantheon: pantheonClient,
	}
}

func (p *PantheonClientWrapper) GetLeaderboard(ctx context.Context, domain string, timeUnit pantheonrpc.TimeUnit, groupingKey string, contextEntryKey string, topN int64, contextN int64, useInMemoryCache bool) (*pantheonrpc.GetLeaderboardResp, error) {
	var err error
	var resp *pantheonrpc.GetLeaderboardResp
	for attempt := 1; attempt <= attempts; attempt++ {
		resp, err = p.getLeaderboard(ctx, domain, timeUnit, groupingKey, contextEntryKey, topN, contextN, useInMemoryCache)
		if err == nil || attempt == attempts {
			break
		}

		timer := time.After(retryDelayFunc(attempt, err))
		select {
		case <-timer:
			continue
		case <-ctx.Done():
			return nil, errors.New("timed out waiting on GetLeaderboard request to succeed")
		}
	}
	return resp, err
}

func (p *PantheonClientWrapper) getLeaderboard(ctx context.Context, domain string, timeUnit pantheonrpc.TimeUnit, groupingKey string, contextEntryKey string, topN int64, contextN int64, useInMemoryCache bool) (*pantheonrpc.GetLeaderboardResp, error) {

	responseChannel := make(chan *pantheonrpc.GetLeaderboardResp, 1)

	req := pantheonrpc.GetLeaderboardReq{
		Domain:           domain,
		TimeUnit:         timeUnit,
		GroupingKey:      groupingKey,
		ContextEntryKey:  contextEntryKey,
		TopN:             topN,
		ContextN:         contextN,
		UseInMemoryCache: useInMemoryCache,
	}

	leaderboardCall := func() error {
		resp, err := p.Pantheon.GetLeaderboard(ctx, &req)
		responseChannel <- resp
		return err
	}

	if err := hystrix.Do(cmd.GetLeaderboardCommand, leaderboardCall, nil); err != nil {
		return nil, httperror.NewWithCause("Could not retrieve leaderboard", http.StatusInternalServerError, err)
	}

	apiResponse := <-responseChannel
	return apiResponse, nil
}

func (p *PantheonClientWrapper) GetLeaderboards(ctx context.Context, domain string, timeUnit pantheonrpc.TimeUnit, groupingKeys []string, contextEntryKey string, topN int64, contextN int64, useInMemoryCache bool) ([]*pantheonrpc.GetLeaderboardResp, error) {
	var err error
	var resp []*pantheonrpc.GetLeaderboardResp
	for attempt := 1; attempt <= attempts; attempt++ {
		resp, err = p.getLeaderboards(ctx, domain, timeUnit, groupingKeys, contextEntryKey, topN, contextN, useInMemoryCache)
		if err == nil || attempt == attempts {
			break
		}

		timer := time.After(retryDelayFunc(attempt, err))
		select {
		case <-timer:
			continue
		case <-ctx.Done():
			return nil, errors.New("timed out waiting on GetLeaderboards request to succeed")
		}
	}
	return resp, err
}

func (p *PantheonClientWrapper) getLeaderboards(ctx context.Context, domain string, timeUnit pantheonrpc.TimeUnit, groupingKeys []string, contextEntryKey string, topN int64, contextN int64, useInMemoryCache bool) ([]*pantheonrpc.GetLeaderboardResp, error) {
	responseChannel := make(chan *pantheonrpc.GetLeaderboardsResp, 1)

	var requestGroup []*pantheonrpc.GetLeaderboardReq
	for _, groupingKey := range groupingKeys {
		requestGroup = append(requestGroup, &pantheonrpc.GetLeaderboardReq{
			Domain:           domain,
			TimeUnit:         timeUnit,
			GroupingKey:      groupingKey,
			ContextEntryKey:  contextEntryKey,
			TopN:             topN,
			ContextN:         contextN,
			UseInMemoryCache: useInMemoryCache,
		})
	}

	req := pantheonrpc.GetLeaderboardsReq{
		LeaderboardRequests: requestGroup,
	}

	leaderboardCall := func() error {
		resp, err := p.Pantheon.GetLeaderboards(ctx, &req)
		responseChannel <- resp
		return err
	}

	if err := hystrix.Do(cmd.GetLeaderboardsCommand, leaderboardCall, nil); err != nil {
		return nil, httperror.NewWithCause("Could not retrieve leaderboards", http.StatusInternalServerError, err)
	}

	apiResponse := <-responseChannel
	return apiResponse.Leaderboards, nil
}

func (p *PantheonClientWrapper) GetBucketedLeaderboardCounts(ctx context.Context, domain string, timeUnit pantheonrpc.TimeUnit, groupingKey string, bucketThresholds []int64, includeModeratedEntries bool) (*pantheonrpc.GetBucketedLeaderboardCountsResp, error) {
	var err error
	var resp *pantheonrpc.GetBucketedLeaderboardCountsResp

	if len(bucketThresholds) == 0 {
		return resp, nil
	}

	for attempt := 1; attempt <= attempts; attempt++ {
		resp, err = p.getBucketedLeaderboardCounts(ctx, domain, timeUnit, groupingKey, bucketThresholds, includeModeratedEntries)
		if err == nil || attempt == attempts {
			break
		}

		timer := time.After(retryDelayFunc(attempt, err))
		select {
		case <-timer:
			continue
		case <-ctx.Done():
			return nil, errors.New("timed out waiting on GetBucketedLeaderboardCounts request to succeed")
		}
	}
	return resp, err
}

func (p *PantheonClientWrapper) getBucketedLeaderboardCounts(ctx context.Context, domain string, timeUnit pantheonrpc.TimeUnit, groupingKey string, bucketThresholds []int64, includeModeratedEntries bool) (*pantheonrpc.GetBucketedLeaderboardCountsResp, error) {
	responseChannel := make(chan *pantheonrpc.GetBucketedLeaderboardCountsResp, 1)

	req := pantheonrpc.GetBucketedLeaderboardCountsReq{
		Domain:                  domain,
		TimeUnit:                timeUnit,
		GroupingKey:             groupingKey,
		BucketThresholds:        bucketThresholds,
		IncludeModeratedEntries: includeModeratedEntries,
	}

	countsCall := func() error {
		resp, err := p.Pantheon.GetBucketedLeaderboardCounts(ctx, &req)
		responseChannel <- resp
		return err
	}

	if err := hystrix.Do(cmd.GetBucketedLeaderboardCounts, countsCall, nil); err != nil {
		return nil, httperror.NewWithCause("Could not retrieve bucketd leaderboard counts", http.StatusInternalServerError, err)
	}

	apiResponse := <-responseChannel
	return apiResponse, nil
}

func (p *PantheonClientWrapper) GetLeaderboardEntriesAboveThreshold(ctx context.Context, domain string, timeUnit pantheonrpc.TimeUnit, groupingKey string, threshold int64, limit int64, offset int64) (*pantheonrpc.GetLeaderboardEntriesAboveThresholdResp, error) {
	var err error
	var resp *pantheonrpc.GetLeaderboardEntriesAboveThresholdResp
	for attempt := 1; attempt <= attempts; attempt++ {
		resp, err = p.getLeaderboardEntriesAboveThreshold(ctx, domain, timeUnit, groupingKey, threshold, limit, offset)
		if err == nil || attempt == attempts {
			break
		}

		timer := time.After(retryDelayFunc(attempt, err))
		select {
		case <-timer:
			continue
		case <-ctx.Done():
			return nil, errors.New("timed out waiting on GetLeaderboardEntriesAboveThreshold request to succeed")
		}
	}
	return resp, err
}

func (p *PantheonClientWrapper) getLeaderboardEntriesAboveThreshold(ctx context.Context, domain string, timeUnit pantheonrpc.TimeUnit, groupingKey string, threshold int64, limit int64, offset int64) (*pantheonrpc.GetLeaderboardEntriesAboveThresholdResp, error) {
	responseChannel := make(chan *pantheonrpc.GetLeaderboardEntriesAboveThresholdResp, 1)

	req := pantheonrpc.GetLeaderboardEntriesAboveThresholdReq{
		Domain:      domain,
		TimeUnit:    timeUnit,
		GroupingKey: groupingKey,
		Threshold:   threshold,
		Limit:       limit,
		Offset:      offset,
	}

	leaderboardEntriesAboveThresholdCall := func() error {
		resp, err := p.Pantheon.GetLeaderboardEntriesAboveThreshold(ctx, &req)
		responseChannel <- resp
		return err
	}

	if err := hystrix.Do(cmd.GetLeaderboardEntriesAboveThresholdCommand, leaderboardEntriesAboveThresholdCall, nil); err != nil {
		return nil, httperror.NewWithCause("Could not retrieve leaderboard entries above threshold", http.StatusInternalServerError, err)
	}

	apiResponse := <-responseChannel
	return apiResponse, nil
}

func (p *PantheonClientWrapper) DeleteLeaderboards(ctx context.Context, userID string) error {
	var err error
	for attempt := 1; attempt <= attempts; attempt++ {
		err = p.deleteLeaderboards(ctx, userID)
		if err == nil || attempt == attempts {
			break
		}

		timer := time.After(retryDelayFunc(attempt, err))
		select {
		case <-timer:
			continue
		case <-ctx.Done():
			return errors.New("timed out waiting on DeleteLeaderboards request to succeed")
		}
	}
	return err
}

func (p *PantheonClientWrapper) deleteLeaderboards(ctx context.Context, userID string) error {
	req := pantheonrpc.DeleteLeaderboardsReq{
		Domain:      PantheonBitsUsageDomain,
		GroupingKey: userID,
	}
	deleteLeaderboardsCall := func() error {
		_, err := p.Pantheon.DeleteLeaderboards(ctx, &req)
		return err
	}
	if err := hystrix.Do(cmd.PantheonDeleteLeaderboards, deleteLeaderboardsCall, nil); err != nil {
		return httperror.NewWithCause("Could not delete leaderboards", http.StatusInternalServerError, err)
	}
	return nil
}

func (p *PantheonClientWrapper) PublishEvent(ctx context.Context, req pantheonrpc.PublishEventReq) error {
	publishCall := func() error {
		_, err := p.Pantheon.PublishEvent(ctx, &req)

		if err != nil {
			errorMsg := fmt.Sprintf("Failed to publish event to pantheon")
			log.WithError(err).WithField("pantheon_request", req).Error(errorMsg)
			return errors.Notef(err, errorMsg)
		}

		return nil
	}

	return hystrix.Do(cmd.PublishEventCommand, publishCall, nil)
}

func (p *PantheonClientWrapper) ModerateEntry(ctx context.Context, req pantheonrpc.ModerateEntryReq) error {
	moderateCall := func() error {
		_, err := p.Pantheon.ModerateEntry(ctx, &req)
		if err != nil {
			errorMsg := fmt.Sprintf("Failed to moderate entry in pantheon")
			log.WithError(err).WithField("pantheon_request", req).Error(errorMsg)
			return errors.Notef(err, errorMsg)
		}
		return nil
	}
	return hystrix.Do(cmd.ModerateEntryCommand, moderateCall, nil)
}

func (p *PantheonClientWrapper) DeleteEntries(ctx context.Context, userID string) error {
	var err error
	for attempt := 1; attempt <= attempts; attempt++ {
		err = p.deleteEntries(ctx, userID)
		if err == nil || attempt == attempts {
			break
		}

		timer := time.After(retryDelayFunc(attempt, err))
		select {
		case <-timer:
			continue
		case <-ctx.Done():
			return errors.New("timed out waiting on DeleteEntries request to succeed")
		}
	}
	return err
}

func (p *PantheonClientWrapper) deleteEntries(ctx context.Context, userID string) error {
	req := pantheonrpc.DeleteEntriesReq{
		Domain:   PantheonBitsUsageDomain,
		EntryKey: userID,
	}
	deleteEntriesCall := func() error {
		_, err := p.Pantheon.DeleteEntries(ctx, &req)
		return err
	}
	if err := hystrix.Do(cmd.PantheonDeleteEntries, deleteEntriesCall, nil); err != nil {
		return httperror.NewWithCause("Could not delete leaderboard entries", http.StatusInternalServerError, err)
	}
	return nil
}

func (p *PantheonClientWrapper) HealthCheck(ctx context.Context, req pantheonrpc.HealthCheckReq) error {
	_, err := p.Pantheon.HealthCheck(ctx, &req)
	if err != nil {
		errorMsg := fmt.Sprintf("Failed to ping pantheon")
		log.WithError(err).WithField("pantheon_request", req).Error(errorMsg)
		return errors.Notef(err, errorMsg)
	}
	return nil
}
