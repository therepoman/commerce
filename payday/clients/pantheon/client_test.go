package pantheon

import (
	"context"
	"testing"

	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	"code.justin.tv/commerce/payday/errors"
	pantheon_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	. "github.com/smartystreets/goconvey/convey"
)

const (
	TwitchUserId = "12345"
)

// TODO: Add in tests for other methods.

func TestPantheonClientWrapper_DeleteLeaderboards(t *testing.T) {
	ctx := context.Background()

	Convey("Given a client", t, func() {
		pantheonRPC := new(pantheon_mock.Pantheon)
		pantheonClient := NewPantheonClientWrapper(pantheonRPC)

		Convey("Deleted Leaderboard without error", func() {
			pantheonRPC.On("DeleteLeaderboards", ctx, &pantheonrpc.DeleteLeaderboardsReq{
				Domain:      PantheonBitsUsageDomain,
				GroupingKey: TwitchUserId,
			}).Return(&pantheonrpc.DeleteLeaderboardsResp{}, nil)
			err := pantheonClient.DeleteLeaderboards(ctx, TwitchUserId)
			So(err, ShouldBeNil)
		})

		Convey("Deleted Leaderboard with error", func() {
			pantheonRPC.On("DeleteLeaderboards", ctx, &pantheonrpc.DeleteLeaderboardsReq{
				Domain:      PantheonBitsUsageDomain,
				GroupingKey: TwitchUserId,
			}).Return(nil, errors.New("THIS IS A TEST ERROR!"))
			err := pantheonClient.DeleteLeaderboards(ctx, TwitchUserId)
			So(err, ShouldNotBeNil)
		})
	})
}

func TestPantheonClientWrapper_DeleteEntries(t *testing.T) {
	ctx := context.Background()

	Convey("Given a client", t, func() {
		pantheonRPC := new(pantheon_mock.Pantheon)
		pantheonClient := NewPantheonClientWrapper(pantheonRPC)

		Convey("Deleted Entries without error", func() {
			pantheonRPC.On("DeleteEntries", ctx, &pantheonrpc.DeleteEntriesReq{
				Domain:   PantheonBitsUsageDomain,
				EntryKey: TwitchUserId,
			}).Return(&pantheonrpc.DeleteEntriesResp{}, nil)
			err := pantheonClient.DeleteEntries(ctx, TwitchUserId)
			So(err, ShouldBeNil)
		})

		Convey("Deleted Entries with error", func() {
			pantheonRPC.On("DeleteEntries", ctx, &pantheonrpc.DeleteEntriesReq{
				Domain:   PantheonBitsUsageDomain,
				EntryKey: TwitchUserId,
			}).Return(nil, errors.New("THIS IS A TEST ERROR!"))
			err := pantheonClient.DeleteEntries(ctx, TwitchUserId)
			So(err, ShouldNotBeNil)
		})
	})
}
