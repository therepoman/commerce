package ems

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/httputils"
	ems "code.justin.tv/gds/gds/extensions/ems/documents"
)

const (
	// Retries the call to EMS 8 times with exponential backoff
	// Delays works out to: 50ms 100ms 200ms 400ms 800ms 1600ms 2000ms
	retryAttempts        = 8
	initialRetryDelay    = 50 * time.Millisecond
	retryDelayMultiplier = 2
	maxRetryDelay        = 2 * time.Second
	emsEndpointFormat    = "/channels/%s/extensions/v2"
)

type IEMSClient interface {
	GetExtensionByChannelID(ctx context.Context, channelId string) (*ems.InstalledExtensionsDocument, error)
}

type EMSClient struct {
	httpClient httputils.HttpUtilClient
	endpoint   string
}

func NewEMSClient(hostURL string) IEMSClient {
	return &EMSClient{
		httpClient: httputils.NewHttpUtilClientApiWithCustomRetryDelayFunc(
			retryAttempts,
			httputils.NewExponentialBackoffRetryFunction(initialRetryDelay, retryDelayMultiplier, maxRetryDelay),
		),
		endpoint: hostURL,
	}
}

func (c *EMSClient) GetExtensionByChannelID(ctx context.Context, channelId string) (*ems.InstalledExtensionsDocument, error) {
	url := c.endpoint + fmt.Sprintf(emsEndpointFormat, channelId)

	headers := map[string]string{}
	headers["Twitch-Authorization"] = ctx.Value("Twitch-Authorization").(string)

	resp, statusCode, err := c.httpClient.HttpGetWithHeaders(ctx, url, headers)

	if err != nil {
		msg := "EMSClient: Error while sending request to EMS"
		log.WithField("ChannelID", channelId).WithError(err).Error(msg)
		return nil, errors.New(msg)
	}

	if statusCode != http.StatusOK {
		msg := "EMSClient: EMS request failed"
		log.WithField("ChannelID", channelId).Error(msg)
		return nil, errors.New(msg)
	}

	response := ems.InstalledExtensionsDocument{}
	err = json.Unmarshal(resp, &response)

	if err != nil {
		msg := "EMSClient: Error unmarshalling response from EMS"
		log.WithField("ChannelID", channelId).WithError(err).Error(msg)
		return nil, err
	}

	return &response, nil
}
