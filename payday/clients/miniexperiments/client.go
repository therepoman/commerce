package miniexperiments

import (
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/discovery/experiments"
	"code.justin.tv/discovery/experiments/experiment"
)

const (
	experimentsPollingInterval = 1 * time.Minute
	TreatmentControl           = "Control"
)

type Experiment struct {
	id           string
	name         string
	expType      experiment.Type
	defaultValue string
}

type MiniExperimentClient interface {
	GetTreatment(exp Experiment, id string) (string, error)
	Close()
}

type miniExperimentClient struct {
	client experiments.Experiments
}

func NewMiniExperimentClient() MiniExperimentClient {
	client, err := experiments.New(experiments.Config{
		Platform:        experiment.LegacyPlatform,
		PollingInterval: experimentsPollingInterval,
		ErrorHandler: func(err error) {
			log.WithError(err).Error("Experiments Client encountered an error")
		},
	})
	if err != nil {
		log.WithError(err).Panic("Failed to initialize Experiments Client")
	}

	for _, exp := range GetExperiments() {
		client.RegisterDefault(exp.expType, exp.name, exp.id, exp.defaultValue)
	}

	return &miniExperimentClient{
		client: client,
	}
}

func GetExperiments() map[string]Experiment {
	return map[string]Experiment{
		"BitsPinata": {
			id:           "cheering2.0_bits_pinata",
			name:         "6fbde053-695a-44f4-a518-9de5c698b87e",
			expType:      experiment.ChannelType,
			defaultValue: TreatmentControl,
		},
	}
}

func (m *miniExperimentClient) GetTreatment(exp Experiment, id string) (string, error) {
	return m.client.Treat(exp.expType, exp.name, exp.id, id)
}

func (m *miniExperimentClient) Close() {
	err := m.client.Close()
	if err != nil {
		log.WithError(err).Error("Failed to shut down miniexperiment client")
	}
}
