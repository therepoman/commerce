package pdms

import (
	"context"
	"errors"
	"net/http"
	"time"

	pdms "code.justin.tv/amzn/PDMSLambdaTwirp"
	twirplambdatransport "code.justin.tv/amzn/TwirpGoLangAWSTransports/lambda"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/config"
	httperror "code.justin.tv/commerce/payday/errors/http"
	"code.justin.tv/commerce/payday/httputils"
	"code.justin.tv/commerce/payday/hystrix"
	cmd "code.justin.tv/commerce/payday/hystrix"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials/stscreds"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/lambda"
	"github.com/golang/protobuf/ptypes/timestamp"
)

type IPDMSClientWrapper interface {
	ReportDeletion(ctx context.Context, userID string, timestamp *timestamp.Timestamp) error
	PromiseDeletion(ctx context.Context, userID string, timestamp *timestamp.Timestamp, autoResolve bool) error
	SetTestDeletion(ctx context.Context, userID string, created, promiseDue, promiseCreated, deletedOn *timestamp.Timestamp) error
}

type PDMSClientWrapper struct {
	PDMS        pdms.PDMSService
	PDMSEnabled bool
}

const (
	paydayCatalogID      = "182" // https://catalog.xarth.tv/services/182/details
	attempts             = 3
	initialRetryDelay    = 20 * time.Millisecond
	retryDelayMultiplier = 2
	maxRetryDelay        = 100 * time.Millisecond
)

var retryDelayFunc = httputils.NewExponentialBackoffRetryFunction(initialRetryDelay, retryDelayMultiplier, maxRetryDelay)

func NewPDMSClientWrapper(cfg config.Configuration, sess *session.Session) IPDMSClientWrapper {
	// see https://wiki.twitch.com/display/SEC/PDMS-+Delete+pipeline+Service+Onboarding
	creds := stscreds.NewCredentials(sess, cfg.PDMSLambdaCallerRoleARN)
	pdmsLambdaCli := lambda.New(sess, &aws.Config{Credentials: creds})
	pdmsTransport := twirplambdatransport.NewClient(pdmsLambdaCli, cfg.PDMSLambdaARN)
	client := pdms.NewPDMSServiceProtobufClient("", pdmsTransport)
	return &PDMSClientWrapper{
		PDMS:        client,
		PDMSEnabled: cfg.PDMSEnabled,
	}
}

func (p *PDMSClientWrapper) ReportDeletion(ctx context.Context, userID string, timestamp *timestamp.Timestamp) error {
	if !p.PDMSEnabled {
		log.Info("PDMS Disabled, the ReportDeletion call will NOT report deletion")
		return nil
	}

	var err error
	for attempt := 1; attempt <= attempts; attempt++ {
		err = p.reportDeletion(ctx, userID, timestamp)
		if err == nil || attempt == attempts {
			break
		}

		timer := time.After(retryDelayFunc(attempt, err))
		select {
		case <-timer:
			continue
		case <-ctx.Done():
			return errors.New("timed out waiting on ReportDeletion request to succeed")
		}
	}
	return err
}

func (p *PDMSClientWrapper) reportDeletion(ctx context.Context, userID string, timestamp *timestamp.Timestamp) error {
	req := pdms.ReportDeletionRequest{
		UserId:    userID,
		ServiceId: paydayCatalogID,
		Timestamp: timestamp,
	}

	reportDeletionCall := func() error {
		_, err := p.PDMS.ReportDeletion(ctx, &req)
		return err
	}

	if err := hystrix.Do(cmd.PDMSReportDeletion, reportDeletionCall, nil); err != nil {
		return httperror.NewWithCause("Could not report user deletion", http.StatusInternalServerError, err)
	}

	return nil
}

func (p *PDMSClientWrapper) PromiseDeletion(ctx context.Context, userID string, timestamp *timestamp.Timestamp, autoResolve bool) error {
	if !p.PDMSEnabled {
		log.Info("PDMS Disabled, the PromiseDeletion call will NOT promise deletion")
		return nil
	}

	var err error
	for attempt := 1; attempt <= attempts; attempt++ {
		err = p.promiseDeletion(ctx, userID, timestamp, autoResolve)
		if err == nil || attempt == attempts {
			break
		}

		timer := time.After(retryDelayFunc(attempt, err))
		select {
		case <-timer:
			continue
		case <-ctx.Done():
			return errors.New("timed out waiting on PromiseDeletion request to succeed")
		}
	}
	return err
}

func (p *PDMSClientWrapper) promiseDeletion(ctx context.Context, userID string, timestamp *timestamp.Timestamp, autoResolve bool) error {
	req := pdms.PromiseDeletionRequest{
		UserId:      userID,
		ServiceId:   paydayCatalogID,
		Timestamp:   timestamp,
		AutoResolve: autoResolve,
	}

	promiseDeletionCall := func() error {
		_, err := p.PDMS.PromiseDeletion(ctx, &req)
		return err
	}

	if err := hystrix.Do(cmd.PDMSPromiseDeletion, promiseDeletionCall, nil); err != nil {
		return httperror.NewWithCause("Could not promise user deletion", http.StatusInternalServerError, err)
	}

	return nil
}

func (p *PDMSClientWrapper) SetTestDeletion(ctx context.Context, userID string, created, promiseDue, promiseCreated, deletedOn *timestamp.Timestamp) error {
	var err error
	for attempt := 1; attempt <= attempts; attempt++ {
		err = p.setTestDeletion(ctx, userID, created, promiseDue, promiseCreated, deletedOn)
		if err == nil || attempt == attempts {
			break
		}

		timer := time.After(retryDelayFunc(attempt, err))
		select {
		case <-timer:
			continue
		case <-ctx.Done():
			return errors.New("timed out waiting on SetTestDeletion request to succeed")
		}
	}
	return err
}

func (p *PDMSClientWrapper) setTestDeletion(ctx context.Context, userID string, created, promiseDue, promiseCreated, deletedOn *timestamp.Timestamp) error {
	req := pdms.SetTestDeletionRequest{
		UserId:         userID,
		ServiceId:      paydayCatalogID,
		Created:        created,
		PromiseDue:     promiseDue,
		PromiseCreated: promiseCreated,
		DeletedOn:      deletedOn,
	}

	setTestDeletionCall := func() error {
		_, err := p.PDMS.SetTestDeletion(ctx, &req)
		return err
	}

	if err := hystrix.Do(cmd.PDMSSetTestDeletion, setTestDeletionCall, nil); err != nil {
		return httperror.NewWithCause("Could not set test deletion", http.StatusInternalServerError, err)
	}

	return nil
}
