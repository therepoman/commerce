package owl

import (
	"context"

	"code.justin.tv/commerce/payday/hystrix"
	cmd "code.justin.tv/commerce/payday/hystrix"
	owl "code.justin.tv/web/owl/client"
	"code.justin.tv/web/owl/oauth2"
)

type ClientWrapper interface {
	GetClient(ctx context.Context, clientID string) (*oauth2.Client, error)
}

type ClientWrapperImpl struct {
	client owl.Client
}

func New(owlClient owl.Client) ClientWrapper {
	return &ClientWrapperImpl{
		client: owlClient,
	}
}

func (cw *ClientWrapperImpl) GetClient(ctx context.Context, clientID string) (*oauth2.Client, error) {
	responseChan := make(chan *oauth2.Client, 1)

	getClientCall := func() error {
		client, err := cw.client.GetClient(ctx, clientID, nil)
		if err != nil {
			return err
		}
		responseChan <- client
		return nil
	}

	err := hystrix.Do(cmd.OWLGetClientCommand, getClientCall, nil)
	if err != nil {
		return nil, err
	}

	resp := <-responseChan
	return resp, nil
}
