package chatbadges

import (
	"context"
	"strconv"
	"time"

	"code.justin.tv/chat/badges/app/models"
	chat_badges "code.justin.tv/chat/badges/client"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/metrics"
	"code.justin.tv/commerce/payday/user"
	"code.justin.tv/foundation/twitchclient"
)

const (
	BadgeSetID = "bits"
)

type IChatBadgesClient interface {
	GetHighestBitsBadgeVersion(ctx context.Context, userid, channel string, reqOpts *twitchclient.ReqOpts) (int64, error)
	GetSelectedChannelBadge(ctx context.Context, userID, channelID string, reqOpts *twitchclient.ReqOpts) (*models.Badge, error)
	SelectBitsBadge(ctx context.Context, userID, channelID string, threshold int64, reqOpts *twitchclient.ReqOpts) (bool, error)
	GetBitsImages(ctx context.Context, channelID string, reqOpts *twitchclient.ReqOpts) (*models.BadgeSet, error)
	RemoveBitsImage(ctx context.Context, channelID string, badgeSetVersion string, reqOpts *twitchclient.ReqOpts) error
	UploadBitsImages(ctx context.Context, channelID string, params models.UploadBitsImagesParams, reqOpts *twitchclient.ReqOpts) (*models.BitsUploadImagesResponse, error)
	RevokeBitsLeaderBadge(ctx context.Context, userID, channelID string, reqOpts *twitchclient.ReqOpts) error
	GrantBitsLeaderBadge(ctx context.Context, userID, channelID string, params models.GrantBadgeParams, reqOpts *twitchclient.ReqOpts) error
	AvailableChannelBadges(ctx context.Context, userID, channelID string, reqOpts *twitchclient.ReqOpts) (*models.GetAvailableBadgesResponse, error)
	GetAvailableBitsBadge(ctx context.Context, userID, channelID string, reqOpts *twitchclient.ReqOpts) (*models.GetAvailableBitsBadgeResponse, error)
	GrantCharityBadge(ctx context.Context, userID string, reqOpts *twitchclient.ReqOpts) error
}

type chatBadgesClient struct {
	Statter metrics.Statter `inject:""`
	Client  chat_badges.Client
}

func NewChatBadgesClient(conf twitchclient.ClientConf) (IChatBadgesClient, error) {
	parent, err := chat_badges.NewClient(conf)
	if err != nil {
		return nil, errors.Notef(err, "Could not create chatbadges client")
	}

	return &chatBadgesClient{
		Client: parent,
	}, nil
}

func NewChatBadgesClientWithMetrics(conf twitchclient.ClientConf) (IChatBadgesClient, error) {
	parent, err := chat_badges.NewClient(conf)
	if err != nil {
		return nil, errors.Notef(err, "Could not create chatbadges client")
	}

	return &chatBadgesClient{
		Client:  parent,
		Statter: metrics.NewNoopStatter(),
	}, nil
}

func (c *chatBadgesClient) AvailableChannelBadges(ctx context.Context, userID, channelID string, reqOpts *twitchclient.ReqOpts) (*models.GetAvailableBadgesResponse, error) {
	startTime := time.Now()
	defer func() {
		c.Statter.TimingDuration("ChatBadgeService_Latency_All", time.Since(startTime))
		c.Statter.TimingDuration("ChatBadgeService_Latency_AvailableChannelBadges", time.Since(startTime))
	}()

	resp, err := c.Client.AvailableChannelBadges(ctx, userID, channelID, reqOpts)
	if err != nil {
		c.Statter.Inc("ChatBadgeService_ErrorCount_All", 1)
		c.Statter.Inc("ChatBadgeService_ErrorCount_AvailableChannelBadges", 1)
	}
	return resp, err
}

func (c *chatBadgesClient) GetAvailableBitsBadge(ctx context.Context, userID, channelID string, reqOpts *twitchclient.ReqOpts) (*models.GetAvailableBitsBadgeResponse, error) {
	startTime := time.Now()
	defer func() {
		c.Statter.TimingDuration("ChatBadgeService_Latency_All", time.Since(startTime))
		c.Statter.TimingDuration("ChatBadgeService_Latency_GetAvailableBitsBadge", time.Since(startTime))
	}()

	resp, err := c.Client.GetAvailableBitsBadge(ctx, userID, channelID, reqOpts)
	if err != nil {
		c.Statter.Inc("ChatBadgeService_ErrorCount_All", 1)
		c.Statter.Inc("ChatBadgeService_ErrorCount_GetAvailableBitsBadge", 1)
	}
	return resp, err
}

func (c *chatBadgesClient) GrantBitsLeaderBadge(ctx context.Context, userID, channelID string, params models.GrantBadgeParams, reqOpts *twitchclient.ReqOpts) error {
	startTime := time.Now()
	defer func() {
		c.Statter.TimingDuration("ChatBadgeService_Latency_All", time.Since(startTime))
		c.Statter.TimingDuration("ChatBadgeService_Latency_GrantBitsLeaderBadge", time.Since(startTime))
	}()

	err := c.Client.GrantBitsLeaderBadge(ctx, userID, channelID, params, reqOpts)
	if err != nil {
		c.Statter.Inc("ChatBadgeService_ErrorCount_All", 1)
		c.Statter.Inc("ChatBadgeService_ErrorCount_GrantBitsLeaderBadge", 1)
	}
	return err
}

func (c *chatBadgesClient) RevokeBitsLeaderBadge(ctx context.Context, userID, channelID string, reqOpts *twitchclient.ReqOpts) error {
	startTime := time.Now()
	defer func() {
		c.Statter.TimingDuration("ChatBadgeService_Latency_All", time.Since(startTime))
		c.Statter.TimingDuration("ChatBadgeService_Latency_RevokeBitsLeaderBadge", time.Since(startTime))
	}()

	err := c.Client.RevokeBitsLeaderBadge(ctx, userID, channelID, reqOpts)
	if err != nil {
		c.Statter.Inc("ChatBadgeService_ErrorCount_All", 1)
		c.Statter.Inc("ChatBadgeService_ErrorCount_RevokeBitsLeaderBadge", 1)
	}
	return err
}

func (c *chatBadgesClient) UploadBitsImages(ctx context.Context, channelID string, params models.UploadBitsImagesParams, reqOpts *twitchclient.ReqOpts) (*models.BitsUploadImagesResponse, error) {
	startTime := time.Now()
	defer func() {
		c.Statter.TimingDuration("ChatBadgeService_Latency_All", time.Since(startTime))
		c.Statter.TimingDuration("ChatBadgeService_Latency_UploadBitsImages", time.Since(startTime))
	}()

	resp, err := c.Client.UploadBitsImages(ctx, channelID, params, reqOpts)
	if err != nil {
		c.Statter.Inc("ChatBadgeService_ErrorCount_All", 1)
		c.Statter.Inc("ChatBadgeService_ErrorCount_UploadBitsImages", 1)
	}
	return resp, err
}

func (c *chatBadgesClient) RemoveBitsImage(ctx context.Context, channelID string, badgeSetVersion string, reqOpts *twitchclient.ReqOpts) error {
	startTime := time.Now()
	defer func() {
		c.Statter.TimingDuration("ChatBadgeService_Latency_All", time.Since(startTime))
		c.Statter.TimingDuration("ChatBadgeService_Latency_RemoveBitsImage", time.Since(startTime))
	}()

	err := c.Client.RemoveBitsImage(ctx, channelID, badgeSetVersion, reqOpts)
	if err != nil {
		c.Statter.Inc("ChatBadgeService_ErrorCount_All", 1)
		c.Statter.Inc("ChatBadgeService_ErrorCount_RemoveBitsImage", 1)
	}
	return err
}

func (c *chatBadgesClient) GetBitsImages(ctx context.Context, channelID string, reqOpts *twitchclient.ReqOpts) (*models.BadgeSet, error) {
	startTime := time.Now()
	defer func() {
		c.Statter.TimingDuration("ChatBadgeService_Latency_All", time.Since(startTime))
		c.Statter.TimingDuration("ChatBadgeService_Latency_GetBitsImages", time.Since(startTime))
	}()

	resp, err := c.Client.GetBitsImages(ctx, channelID, reqOpts)
	if err != nil {
		c.Statter.Inc("ChatBadgeService_ErrorCount_All", 1)
		c.Statter.Inc("ChatBadgeService_ErrorCount_GetBitsImages", 1)
	}
	return resp, err
}

func (c *chatBadgesClient) GetSelectedChannelBadge(ctx context.Context, userID, channelID string, reqOpts *twitchclient.ReqOpts) (*models.Badge, error) {
	startTime := time.Now()
	defer func() {
		c.Statter.TimingDuration("ChatBadgeService_Latency_All", time.Since(startTime))
		c.Statter.TimingDuration("ChatBadgeService_Latency_GetSelectedChannelBadge", time.Since(startTime))
	}()

	selectedBadge, err := c.Client.GetSelectedChannelBadge(ctx, userID, channelID, reqOpts)
	if err != nil {
		c.Statter.Inc("ChatBadgeService_ErrorCount_All", 1)
		c.Statter.Inc("ChatBadgeService_ErrorCount_GetSelectedChannelBadge", 1)
		return selectedBadge, err
	}

	return selectedBadge, nil
}

func (c *chatBadgesClient) SelectBitsBadge(ctx context.Context, userID string, channelID string, threshold int64, reqOpts *twitchclient.ReqOpts) (bool, error) {
	startTime := time.Now()
	defer func() {
		c.Statter.TimingDuration("ChatBadgeService_Latency_All", time.Since(startTime))
		c.Statter.TimingDuration("ChatBadgeService_Latency_SelectBitsBadge", time.Since(startTime))
	}()

	badgeSetVersion := strconv.FormatInt(threshold, 10)
	hasBadge, err := c.Client.SelectBitsBadge(ctx, userID, channelID, &badgeSetVersion, reqOpts)
	if err != nil {
		c.Statter.Inc("ChatBadgeService_ErrorCount_All", 1)
		c.Statter.Inc("ChatBadgeService_ErrorCount_SelectBitsBadge", 1)
		return hasBadge, err
	}

	return hasBadge, nil
}

func (c *chatBadgesClient) GetHighestBitsBadgeVersion(ctx context.Context, userid, channel string, reqOpts *twitchclient.ReqOpts) (int64, error) {
	if user.IsATestAccount(userid) || user.IsATestAccount(channel) {
		return 0, nil
	}

	bitsBadgeResp, err := c.GetAvailableBitsBadge(ctx, userid, channel, reqOpts)

	if err != nil {
		return 0, errors.Notef(err, "Could not retrieve chat badges")
	}

	if bitsBadgeResp == nil {
		msg := "Received a nil response getting bits badge in channel"
		log.WithFields(log.Fields{
			"userId":  userid,
			"channel": channel,
		}).Error(msg)
		return 0, errors.Notef(err, msg)
	}

	maxBitsBadgeSetVersion := 0
	if bitsBadgeResp.BitsBadge != nil {
		badgeSetVersionBitsValue, err := strconv.Atoi(bitsBadgeResp.BitsBadge.BadgeSetVersion)
		if err != nil {
			msg := "Received a non-numeric bits badge set version"
			log.WithError(err).WithField("badgeSetVersion", bitsBadgeResp.BitsBadge.BadgeSetVersion).Error(msg)
			return 0, errors.Notef(err, msg)
		}
		maxBitsBadgeSetVersion = badgeSetVersionBitsValue
	}

	return int64(maxBitsBadgeSetVersion), nil
}

func (c *chatBadgesClient) GrantCharityBadge(ctx context.Context, userID string, reqOpts *twitchclient.ReqOpts) error {
	startTime := time.Now()
	defer func() {
		c.Statter.TimingDuration("ChatBadgeService_Latency_All", time.Since(startTime))
		c.Statter.TimingDuration("ChatBadgeService_Latency_GrantCharityBadge", time.Since(startTime))
	}()

	params := models.GrantBadgeParams{
		BadgeSetVersion: "1",
	}
	err := c.Client.GrantBitsCharityBadge(ctx, userID, params, reqOpts)
	if err != nil {
		c.Statter.Inc("ChatBadgeService_ErrorCount_All", 1)
		c.Statter.Inc("ChatBadgeService_ErrorCount_GrantCharityBadge", 1)
	}
	return err
}
