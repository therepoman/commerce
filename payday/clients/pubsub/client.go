package pubsub

import (
	"context"

	pubsub_control "code.justin.tv/chat/pubsub-control/client"
	"code.justin.tv/commerce/payday/hystrix"
	commands "code.justin.tv/commerce/payday/hystrix"
)

const ChannelListenersPubSubTopic = "video-playback-by-id."

type IPubSubUtils interface {
	GetNumberOfListeners(ctx context.Context, topic string, channelid string) (int, error)
}

type PubSubUtils struct {
	PubsubControl pubsub_control.Client
}

type PubSubControlResponse struct {
	NumListeners int `json:"num_listeners"`
	FailedGets   int `json:"failed_gets"`
	NumEdges     int `json:"num_edges"`
}

func NewPubSubUtils(client pubsub_control.Client) IPubSubUtils {
	return &PubSubUtils{
		PubsubControl: client,
	}
}

func (p *PubSubUtils) GetNumberOfListeners(ctx context.Context, topic string, channelid string) (int, error) {
	output := make(chan int, 1)

	f := func() error {
		combinedTopic := topic + channelid

		numListeners, err := p.PubsubControl.GetNumberListeners(ctx, combinedTopic, nil)
		if err != nil {
			return err
		}

		output <- numListeners
		return nil
	}

	errors := hystrix.Go(commands.GetNumberOfPubSubListenersCommand, f, nil)

	select {
	case out := <-output:
		return out, nil
	case err := <-errors:
		return 0, err
	}
}
