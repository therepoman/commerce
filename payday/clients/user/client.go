package user

import (
	"code.justin.tv/commerce/payday/user"
	"code.justin.tv/commerce/payday/utils/pointers"
	"code.justin.tv/foundation/twitchclient"
	users "code.justin.tv/web/users-service/client/usersclient_internal"
	user_model "code.justin.tv/web/users-service/models"
	"golang.org/x/net/context"
)

type IUserServiceClientWrapper interface {
	GetUserByID(ctx context.Context, userID string, reqOpts *twitchclient.ReqOpts) (*user_model.Properties, error)
	GetUserByLogin(ctx context.Context, login string, reqOpts *twitchclient.ReqOpts) (*user_model.Properties, error)
}

type UserServiceClientWrapper struct {
	UserServiceClient users.InternalClient
}

func NewUserServiceClientWrapper(userServiceClient users.InternalClient) IUserServiceClientWrapper {
	return &UserServiceClientWrapper{
		UserServiceClient: userServiceClient,
	}
}

func generateTestUserResponse(userID string) *user_model.Properties {
	properties := &user_model.Properties{
		ID:    userID,
		Login: pointers.StringP("test-login-" + userID),
	}
	return properties
}

func (u *UserServiceClientWrapper) GetUserByID(ctx context.Context, userID string, reqOpts *twitchclient.ReqOpts) (*user_model.Properties, error) {
	if user.IsATestAccount(userID) {
		return generateTestUserResponse(userID), nil
	} else {
		return u.UserServiceClient.GetUserByID(ctx, userID, reqOpts)
	}
}

func (u *UserServiceClientWrapper) GetUserByLogin(ctx context.Context, login string, reqOpts *twitchclient.ReqOpts) (*user_model.Properties, error) {
	return u.UserServiceClient.GetUserByLogin(ctx, login, reqOpts)
}
