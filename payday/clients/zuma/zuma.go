package zuma

import (
	"time"

	"code.justin.tv/chat/zuma/app/api"
	zuma_client "code.justin.tv/chat/zuma/client"
	"code.justin.tv/commerce/payday/hystrix"
	hystrixcommands "code.justin.tv/commerce/payday/hystrix"
	"code.justin.tv/commerce/payday/metrics"
	"code.justin.tv/commerce/payday/user"
	"code.justin.tv/foundation/twitchclient"
	"github.com/cenkalti/backoff"
	"golang.org/x/net/context"
)

type IZumaClientWrapper interface {
	GetMod(ctx context.Context, channelID, userID string, reqOpts *twitchclient.ReqOpts) (api.GetModResponse, error)
	GetModAsync(ctx context.Context, channelID, userID string, reqOpts *twitchclient.ReqOpts) (chan api.GetModResponse, chan error)
	ListMods(ctx context.Context, channelID string, reqOpts *twitchclient.ReqOpts) (api.ListModsResponse, error)
	ListModsAsync(ctx context.Context, channelID string, reqOpts *twitchclient.ReqOpts) (chan api.ListModsResponse, chan error)
	EnforceMessage(ctx context.Context, params api.EnforceMessageRequest, reqOpts *twitchclient.ReqOpts) (api.EnforceMessageResponse, error)
}

type ZumaClientWrapper struct {
	zumaClient zuma_client.Client
	Statter    metrics.Statter `inject:""`
}

type riskCategory string

func newBackoff() backoff.BackOff {
	exponetial := backoff.NewExponentialBackOff()
	exponetial.InitialInterval = 10 * time.Millisecond
	exponetial.MaxElapsedTime = 1 * time.Second
	exponetial.Multiplier = 2
	return exponetial
}

const zumaEnforceMessageAttempts = "ZumaEnforceMessageAttempts"

const (
	aggressiveCategory = riskCategory("aggressive")
	sexualCategory     = riskCategory("sexual")
	profanityCategory  = riskCategory("profanity")
	identityCategory   = riskCategory("identity")
	unknownCategory    = riskCategory("unknown")
)

var topicToCategoryMapping = map[string]riskCategory{
	"1":  aggressiveCategory,
	"4":  sexualCategory,
	"5":  profanityCategory,
	"10": identityCategory,
}

func NewZumaClientWrapper(zumaClient zuma_client.Client) IZumaClientWrapper {
	return &ZumaClientWrapper{
		zumaClient: zumaClient,
	}
}

func generateTestZumaGetModResponse(userID string) api.GetModResponse {
	return api.GetModResponse{
		IsMod: false,
	}
}

func (z *ZumaClientWrapper) GetMod(ctx context.Context, channelID, userID string, reqOpts *twitchclient.ReqOpts) (api.GetModResponse, error) {
	responseChannel, errChannel := z.GetModAsync(ctx, channelID, userID, reqOpts)
	select {
	case response := <-responseChannel:
		return response, nil
	case err := <-errChannel:
		return api.GetModResponse{}, err
	}
}

func (z *ZumaClientWrapper) GetModAsync(ctx context.Context, channelID, userID string, reqOpts *twitchclient.ReqOpts) (chan api.GetModResponse, chan error) {
	responseChannel := make(chan api.GetModResponse, 1)

	f := func() error {
		if user.IsATestAccount(channelID) || user.IsATestAccount(userID) {
			responseChannel <- generateTestZumaGetModResponse(userID)
			return nil
		} else {
			response, err := z.zumaClient.GetMod(ctx, channelID, userID, reqOpts)

			if err != nil {
				return err
			}

			responseChannel <- response
			return nil
		}
	}

	err := hystrix.Go(hystrixcommands.ZumaGetModCommand, f, nil)

	return responseChannel, err
}

func generateTestZumaListModsResponse() api.ListModsResponse {
	return api.ListModsResponse{
		Mods: []string{},
	}
}

func (z *ZumaClientWrapper) ListMods(ctx context.Context, channelID string, reqOpts *twitchclient.ReqOpts) (api.ListModsResponse, error) {
	responseChannel, errChannel := z.ListModsAsync(ctx, channelID, reqOpts)
	select {
	case response := <-responseChannel:
		return response, nil
	case err := <-errChannel:
		return api.ListModsResponse{}, err
	}
}

func (z *ZumaClientWrapper) ListModsAsync(ctx context.Context, channelID string, reqOpts *twitchclient.ReqOpts) (chan api.ListModsResponse, chan error) {
	responseChannel := make(chan api.ListModsResponse, 1)

	f := func() error {
		if user.IsATestAccount(channelID) {
			responseChannel <- generateTestZumaListModsResponse()
			return nil
		} else {
			response, err := z.zumaClient.ListMods(ctx, channelID, reqOpts)

			if err != nil {
				return err
			}

			responseChannel <- response

			return nil
		}
	}

	errs := hystrix.Go(hystrixcommands.ZumaListModCommand, f, nil)

	return responseChannel, errs
}

func (z *ZumaClientWrapper) EnforceMessage(ctx context.Context, params api.EnforceMessageRequest, reqOpts *twitchclient.ReqOpts) (response api.EnforceMessageResponse, err error) {
	if user.IsATestAccount(params.SenderID) || params.ContainerOwner != nil && user.IsATestAccount(params.ContainerOwner.ID()) {
		return generateTestZumaEnforceMessageResponse(), nil
	}

	responseChan := make(chan api.EnforceMessageResponse, 1)
	tries := 0
	f := func() error {
		tries++
		fn := func() error {
			response, err := z.zumaClient.EnforceMessage(ctx, params, reqOpts)
			responseChan <- response
			if err != nil {
				return err
			}
			return nil
		}
		return hystrix.Do(hystrixcommands.ZumaEnforceMessageCommand, fn, nil)
	}

	var b backoff.BackOff = backoff.WithContext(newBackoff(), ctx)
	b = backoff.WithMaxTries(b, 3)

	defer z.Statter.Inc(zumaEnforceMessageAttempts, int64(tries))

	err = backoff.Retry(f, b)
	response = <-responseChan
	return
}

func generateTestZumaEnforceMessageResponse() (response api.EnforceMessageResponse) {
	return api.EnforceMessageResponse{
		EnforceProperties: api.EnforceProperties{
			AllPassed: true,
		},
	}
}

func CategoryFromMostSignificantKnownTopic(topics map[string]int) riskCategory {
	largestCategory := unknownCategory
	largestCategoryValue := 0

	for topic, topicValue := range topics {
		if category, exists := topicToCategoryMapping[topic]; exists && topicValue > largestCategoryValue {
			largestCategory = category
			largestCategoryValue = topicValue
		}
	}

	return largestCategory
}
