package pachinko

import (
	"context"
	"net/http"
	"time"

	"code.justin.tv/amzn/TwitchS2S2/s2s2"
	"code.justin.tv/commerce/gogogadget/strings"
	log "code.justin.tv/commerce/logrus"
	rpc "code.justin.tv/commerce/pachinko/rpc"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/hystrix"
	cmds "code.justin.tv/commerce/payday/hystrix"
	"code.justin.tv/commerce/payday/metrics"
	"github.com/cenkalti/backoff"
	"github.com/golang/protobuf/ptypes"
	"github.com/pkg/errors"
	"github.com/twitchtv/twirp"
)

const (
	GROUP_ID                          = "BALANCE"
	PACHINKO_FAILURE_METRIC           = "Pachinko_Failure"
	BALANCE_FAILURE_METRIC            = "Pachinko_Get_Balance_Failure"
	ADD_BITS_FAILURE_METRIC           = "Pachinko_Add_Bits_Failure"
	CHECK_HOLD_FAILURE_METRIC         = "Pachinko_Check_Hold_Failure"
	OVERRIDE_BALANCE_FAILURE_METRIC   = "Pachinko_Override_Failure"
	DELETE_BALANCE_FAILURE_METRIC     = "Pachinko_Delete_Balance_Failure"
	START_TRANSACTION_FAILURE_METRIC  = "Pachinko_Start_Transaction_Failure"
	COMMIT_TRANSACTION_FAILURE_METRIC = "Pachinko_Commit_Transaction_Failure"
	COMMIT_TRANSACTION_EXPIRED_METRIC = "Pachinko_Commit_Transaction_Expired"
)

var (
	ErrAlreadyCommitted    = errors.New("pachinko: transaction is already committed")
	ErrInsufficientBalance = errors.New("pachinko: insufficient balance")
)

type PachinkoClient interface {
	GetBalance(ctx context.Context, userID string) (int, error)
	AddBits(ctx context.Context, transactionId string, userId string, bitsQuantity int) (updatedBalance int, err error)
	RemoveBits(ctx context.Context, transactionId string, userId string, bitsQuantity int) (updatedBalance int, err error)
	CheckHold(ctx context.Context, ownerID, holdID string) (*rpc.Hold, error)
	CreateHold(ctx context.Context, transactionID, userID string, bitsQuantity int, expiresAt *time.Time) (int, error)
	FinalizeHold(ctx context.Context, transactionID, holdID, userID string) (int, error)
	ReleaseHold(ctx context.Context, transactionID, holdID, userID string) (int, error)
	OverrideBalance(ctx context.Context, userID string, balance int) (int, error)
	DeleteBalanceByOwnerId(ctx context.Context, userID string) error
}

type pachinkoClient struct {
	client  rpc.Pachinko
	Statter metrics.Statter `inject:""`
}

func NewClient(cfg config.Configuration, s2s2Client *s2s2.S2S2) PachinkoClient {
	var httpClient *http.Client

	if cfg.S2S2.Enabled {
		httpClient = &http.Client{
			Transport: s2s2Client.RoundTripper(&http.Client{}),
		}
	} else {
		httpClient = http.DefaultClient
	}

	return &pachinkoClient{
		client: rpc.NewPachinkoProtobufClient(cfg.PachinkoURL, httpClient),
	}
}

func NewClientWithStatter(cfg config.Configuration, statter metrics.Statter, s2s2Client *s2s2.S2S2) PachinkoClient {
	var httpClient *http.Client

	if cfg.S2S2.Enabled {
		httpClient = &http.Client{
			Transport: s2s2Client.RoundTripper(&http.Client{}),
		}
	} else {
		httpClient = http.DefaultClient
	}

	return &pachinkoClient{
		client:  rpc.NewPachinkoProtobufClient(cfg.PachinkoURL, httpClient),
		Statter: statter,
	}
}

func (c *pachinkoClient) GetBalance(ctx context.Context, userID string) (balance int, err error) {
	f := func() error {
		return hystrix.Do(cmds.PachinkoGetBalanceCommand, func() error {
			resp, err := c.client.GetToken(ctx, &rpc.GetTokenReq{
				Domain:  rpc.Domain_BITS,
				OwnerId: userID,
				GroupId: GROUP_ID,
			})

			if err != nil {
				log.WithFields(log.Fields{
					"UserID": userID,
				}).WithError(err).Warn("failed to fetch bits balance from Pachinko")
			} else if resp == nil || resp.Token == nil {
				log.WithFields(log.Fields{
					"UserID": userID,
				}).WithError(err).Warn("no balance was returned, but should have returned a balance.")
			} else {
				balance = int(resp.Token.Quantity)
			}
			return err
		}, nil)
	}

	b := backoff.WithMaxTries(backoff.WithContext(newReadBackoff(), ctx), 3)
	err = backoff.Retry(f, b)
	if err != nil {
		go c.incrementFailureMetrics(BALANCE_FAILURE_METRIC)
	}
	return
}

func (c *pachinkoClient) AddBits(ctx context.Context, transactionId string, userId string, bitsQuantity int) (updatedBalance int, err error) {
	f := func() error {
		return hystrix.Do(cmds.PachinkoAddTokensCommand, func() error {
			resp, err := c.client.AddTokens(ctx, &rpc.AddTokensReq{
				TransactionId: transactionId,
				Token: &rpc.Token{
					OwnerId:  userId,
					GroupId:  GROUP_ID,
					Domain:   rpc.Domain_BITS,
					Quantity: int64(bitsQuantity),
				},
			})

			if err != nil {
				log.WithFields(log.Fields{
					"EventId":      transactionId,
					"TwitchUserId": userId,
					"Quantity":     bitsQuantity,
				}).WithError(err).Warn("Error doing AddTokens in Pachinko call.")
			} else {
				updatedBalance = int(resp.Token.Quantity)
			}
			return err
		}, nil)
	}

	b := backoff.WithMaxTries(backoff.WithContext(newWriteBackoff(), ctx), 3)
	err = backoff.Retry(f, b)
	if err != nil {
		go c.incrementFailureMetrics(ADD_BITS_FAILURE_METRIC)
	}
	return
}

func (c *pachinkoClient) RemoveBits(ctx context.Context, transactionId string, userId string, bitsQuantity int) (int, error) {
	return c.performTransaction(ctx, userId, &rpc.StartTransactionReq{
		TransactionId: transactionId,
		Operation:     rpc.Operation_CONSUME,
		Tokens: []*rpc.Token{
			{
				OwnerId:  userId,
				GroupId:  GROUP_ID,
				Domain:   rpc.Domain_BITS,
				Quantity: int64(bitsQuantity),
			},
		},
	})
}

func (c *pachinkoClient) CheckHold(ctx context.Context, ownerID, holdID string) (hold *rpc.Hold, err error) {
	f := func() error {
		return hystrix.Do(cmds.PachinkoCheckHoldCommand, func() error {
			resp, err := c.client.CheckHold(ctx, &rpc.CheckHoldReq{
				OwnerId:       ownerID,
				TransactionId: holdID,
			})

			if err != nil {
				log.WithFields(log.Fields{
					"OwnerId": ownerID,
					"HoldId":  holdID,
				}).WithError(err).Warn("failed to check hold")
			} else {
				hold = resp.Hold
			}
			return err
		}, nil)
	}

	b := backoff.WithMaxTries(backoff.WithContext(newReadBackoff(), ctx), 3)
	err = backoff.Retry(f, b)
	if err != nil {
		go c.incrementFailureMetrics(CHECK_HOLD_FAILURE_METRIC)
	}
	return
}

func (c *pachinkoClient) CreateHold(ctx context.Context, transactionID, userID string, bitsQuantity int, expiresAt *time.Time) (int, error) {
	req := &rpc.StartTransactionReq{
		TransactionId: transactionID,
		Operation:     rpc.Operation_CREATE_HOLD,
		Tokens: []*rpc.Token{
			{
				OwnerId:  userID,
				GroupId:  GROUP_ID,
				Domain:   rpc.Domain_BITS,
				Quantity: int64(bitsQuantity),
			},
		},
	}
	if expiresAt != nil {
		protoExpiresAt, err := ptypes.TimestampProto(*expiresAt)
		if err != nil {
			log.WithFields(log.Fields{}).WithError(err).Error("could not parse timestamp to create hold")
		}
		req.ExpiresAt = protoExpiresAt
	}

	return c.performTransaction(ctx, userID, req)
}

func (c *pachinkoClient) FinalizeHold(ctx context.Context, transactionID, holdID, userID string) (int, error) {
	return c.performTransaction(ctx, userID, &rpc.StartTransactionReq{
		TransactionId:        transactionID,
		Operation:            rpc.Operation_FINALIZE_HOLD,
		OperandTransactionId: holdID,
	})
}

func (c *pachinkoClient) ReleaseHold(ctx context.Context, transactionID, holdID, userID string) (int, error) {
	return c.performTransaction(ctx, userID, &rpc.StartTransactionReq{
		TransactionId:        transactionID,
		Operation:            rpc.Operation_RELEASE_HOLD,
		OperandTransactionId: holdID,
	})
}

func (c *pachinkoClient) OverrideBalance(ctx context.Context, userID string, balance int) (updatedBalance int, err error) {
	f := func() error {
		return hystrix.Do(cmds.PachinkoOverrideBalanceCommand, func() error {
			resp, err := c.client.OverrideBalance(ctx, &rpc.OverrideBalanceReq{
				Token: &rpc.Token{
					Domain:   rpc.Domain_BITS,
					GroupId:  GROUP_ID,
					OwnerId:  userID,
					Quantity: int64(balance),
				},
			})
			if err != nil {
				log.WithFields(log.Fields{
					"TwitchUserId": userID,
					"Quantity":     balance,
				}).WithError(err).Warn("Error doing Override Tokens in Pachinko call.")
			} else {
				updatedBalance = int(resp.GetToken().GetQuantity())
			}
			return err
		}, nil)
	}

	b := backoff.WithMaxTries(backoff.WithContext(newWriteBackoff(), ctx), 3)
	err = backoff.Retry(f, b)
	if err != nil {
		go c.incrementFailureMetrics(OVERRIDE_BALANCE_FAILURE_METRIC)
	}
	return
}

func (c *pachinkoClient) DeleteBalanceByOwnerId(ctx context.Context, userID string) (err error) {
	f := func() error {
		return hystrix.Do(cmds.PachinkoDeleteBalanceByOwnerId, func() error {
			_, err := c.client.DeleteBalanceByOwnerId(ctx, &rpc.DeleteBalanceByOwnerIdReq{
				OwnerId: userID,
			})
			if err != nil {
				log.WithFields(log.Fields{
					"TwitchUserId": userID,
				}).WithError(err).Warn("Error calling Pachinko's DeleteBalanceByOwnerId API.")
			}
			return err
		}, nil)
	}

	b := backoff.WithMaxTries(backoff.WithContext(newWriteBackoff(), ctx), 3)
	err = backoff.Retry(f, b)
	if err != nil {
		go c.incrementFailureMetrics(DELETE_BALANCE_FAILURE_METRIC)
	}
	return
}

func (c *pachinkoClient) performTransaction(ctx context.Context, userID string, req *rpc.StartTransactionReq) (int, error) {
	transactionId := req.TransactionId
	var err error

	f := func() error {
		return hystrix.Do(cmds.PachinkoStartTransactionCommand, func() error {
			var resp *rpc.StartTransactionResp
			resp, err = c.client.StartTransaction(ctx, req)

			if err != nil {
				log.WithFields(log.Fields{
					"EventId":      transactionId,
					"Operation":    req.Operation.String(),
					"TwitchUserId": userID,
				}).WithError(err).Warn("Error doing StartTransaction in Pachinko call.")
				return err
			} else if resp.ValidationError != nil {
				switch resp.ValidationError.Code {
				case rpc.ValidationErrorCode_TRANSACTION_COMMITTED:
					err = ErrAlreadyCommitted
				case rpc.ValidationErrorCode_INSUFFICIENT_BALANCE:
					err = ErrInsufficientBalance
				default:
					err = errors.Errorf("Validation error doing StartTransaction: %s", resp.ValidationError.Code.String())
				}
			} else if resp.TransactionId != req.TransactionId && strings.NotBlank(resp.TransactionId) {
				log.WithFields(log.Fields{
					"RequestTransactionId":  transactionId,
					"PachinkoTransactionId": resp.TransactionId,
				}).Warn("Pachinko is returning a different transaction id than was sent it.")
				// If Pachinko sent us a different transaction id, I guess we should use it or should we error?
				transactionId = resp.TransactionId
			}
			return nil
		}, nil)
	}

	b := backoff.WithMaxTries(backoff.WithContext(newWriteBackoff(), ctx), 3)
	retryErr := backoff.Retry(f, b)
	if retryErr != nil {
		go c.incrementFailureMetrics(START_TRANSACTION_FAILURE_METRIC)
		return 0, retryErr
	}
	if err != nil {
		go c.incrementFailureMetrics(START_TRANSACTION_FAILURE_METRIC)
		return 0, err
	}

	updatedBalance := 0
	var nonRetryableError error
	f = func() error {
		return hystrix.Do(cmds.PachinkoCommitTransactionCommand, func() error {
			resp, err := c.client.CommitTransaction(ctx, &rpc.CommitTransactionReq{
				TransactionId: transactionId,
				Domain:        rpc.Domain_BITS,
			})

			if err != nil {
				log.WithFields(log.Fields{
					"EventId":      transactionId,
					"TwitchUserId": userID,
				}).WithError(err).Warn("Error doing CommitTransaction in Pachinko call.")
				twirpErr, ok := err.(twirp.Error)
				if ok && twirp.ServerHTTPStatusFromErrorCode(twirpErr.Code()) < 500 {
					nonRetryableError = twirpErr
					return nil
				}
			} else {
				updatedBalance = int(resp.Tokens[0].Quantity)
			}
			return err
		}, nil)
	}
	b = backoff.WithMaxTries(backoff.WithContext(newWriteBackoff(), ctx), 3)
	err = backoff.Retry(f, b)
	if err != nil {
		go c.incrementFailureMetrics(COMMIT_TRANSACTION_FAILURE_METRIC)
		return updatedBalance, err
	}
	if nonRetryableError != nil {
		go c.incrementFailureMetrics(COMMIT_TRANSACTION_EXPIRED_METRIC)
		return updatedBalance, nonRetryableError
	}

	return updatedBalance, nil
}

func (c *pachinkoClient) incrementFailureMetrics(metricName string) {
	c.Statter.Inc(metricName, 1)
	c.Statter.Inc(PACHINKO_FAILURE_METRIC, 1)
}

func newReadBackoff() backoff.BackOff {
	exponential := newBackoff()
	exponential.InitialInterval = 10 * time.Millisecond
	return exponential
}

func newWriteBackoff() backoff.BackOff {
	exponential := newBackoff()
	exponential.InitialInterval = 100 * time.Millisecond
	return exponential
}

func newBackoff() *backoff.ExponentialBackOff {
	exponential := backoff.NewExponentialBackOff()
	exponential.MaxElapsedTime = 1 * time.Second
	exponential.Multiplier = 2
	return exponential
}
