package pachinko

import (
	"context"
	"testing"
	"time"

	"code.justin.tv/commerce/gogogadget/random"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/hystrix"
	pachinko_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/pachinko/rpc"
	metrics_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/metrics"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
	"github.com/twitchtv/twirp"
)

const (
	TwitchUserId = "183884402"
)

func TestPachinkoClient_GetBalance(t *testing.T) {
	ctx := context.Background()

	Convey("Given a client", t, func() {
		pachinkoRPC := new(pachinko_mock.Pachinko)
		statter := new(metrics_mock.Statter)
		pachinkoClient := pachinkoClient{
			client:  pachinkoRPC,
			Statter: statter,
		}

		statter.On("Inc", mock.Anything, mock.Anything).Return()

		Convey("that returns a balance when successful", func() {
			Convey("when we return a well formed response", func() {
				pachinkoRPC.On("GetToken", mock.Anything, mock.Anything).Return(&pachinko.GetTokenResp{
					Token: &pachinko.Token{
						Quantity: int64(2000),
					},
				}, nil)

				updatedBalance, err := pachinkoClient.GetBalance(ctx, TwitchUserId)
				So(err, ShouldBeNil)
				So(updatedBalance, ShouldEqual, 2000)
			})

			Convey("when no resp obj is returned", func() {
				pachinkoRPC.On("GetToken", mock.Anything, mock.Anything).Return(nil, nil)

				updatedBalance, err := pachinkoClient.GetBalance(ctx, TwitchUserId)
				So(err, ShouldBeNil)
				So(updatedBalance, ShouldEqual, 0)
			})

			Convey("when no token obj is returned", func() {
				pachinkoRPC.On("GetToken", mock.Anything, mock.Anything).Return(&pachinko.GetTokenResp{}, nil)

				updatedBalance, err := pachinkoClient.GetBalance(ctx, TwitchUserId)
				So(err, ShouldBeNil)
				So(updatedBalance, ShouldEqual, 0)
			})
		})

		Convey("that returns an error", func() {
			Convey("when pachinko returns an error", func() {
				pachinkoRPC.On("GetToken", mock.Anything, mock.Anything).Return(
					nil, errors.New("NO TOKENS FOR YOU!"))
			})

			updatedBalance, err := pachinkoClient.GetBalance(ctx, TwitchUserId)
			So(err, ShouldNotBeNil)
			So(updatedBalance, ShouldEqual, 0)
		})
	})
}

func TestAddBits(t *testing.T) {
	ctx := context.Background()

	Convey("Given a client", t, func() {
		pachinkoRPC := new(pachinko_mock.Pachinko)
		statter := new(metrics_mock.Statter)
		pachinkoClient := pachinkoClient{
			client:  pachinkoRPC,
			Statter: statter,
		}

		statter.On("Inc", mock.Anything, mock.Anything).Return()

		Convey("that returns a balance successfully", func() {
			pachinkoRPC.On("AddTokens", mock.Anything, mock.Anything).Return(&pachinko.AddTokensResp{
				Token: &pachinko.Token{
					Quantity: int64(2000),
				},
			}, nil)

			updatedBalance, err := pachinkoClient.AddBits(ctx, "foody-mc-food-transaction", TwitchUserId, 1337)
			So(err, ShouldBeNil)
			So(updatedBalance, ShouldEqual, 2000)
		})

		Convey("that returns an error", func() {
			pachinkoRPC.On("AddTokens", mock.Anything, mock.Anything).Return(
				nil, errors.New("NO TOKENS FOR YOU!"))

			updatedBalance, err := pachinkoClient.AddBits(ctx, "drinky-mc-drinkface-transaction", TwitchUserId, 1337)
			So(err, ShouldNotBeNil)
			So(updatedBalance, ShouldEqual, 0)
		})
	})
}

func TestRemoveBits(t *testing.T) {
	ctx := context.Background()

	transactionID := "walrus-id"

	Convey("Given a client", t, func() {
		pachinkoRPC := new(pachinko_mock.Pachinko)
		statter := new(metrics_mock.Statter)
		pachinkoClient := pachinkoClient{
			client:  pachinkoRPC,
			Statter: statter,
		}

		statter.On("Inc", mock.Anything, mock.Anything).Return()

		Convey("when StartTransaction succeeds", func() {
			pachinkoRPC.On("StartTransaction", ctx, mock.MatchedBy(func(req *pachinko.StartTransactionReq) bool {
				return req.TransactionId == transactionID
			})).Return(&pachinko.StartTransactionResp{
				TransactionId: transactionID,
			}, nil).Once()

			commitTransactionReq := &pachinko.CommitTransactionReq{
				TransactionId: transactionID,
				Domain:        pachinko.Domain_BITS,
			}

			Convey("when CommitTransaction succeeds", func() {
				pachinkoRPC.On("CommitTransaction", ctx, commitTransactionReq).Return(&pachinko.CommitTransactionResp{
					Tokens: []*pachinko.Token{
						{
							Quantity: 2099,
						},
					},
				}, nil).Once()

				updatedBalance, err := pachinkoClient.RemoveBits(ctx, transactionID, TwitchUserId, 1)
				So(err, ShouldBeNil)
				So(updatedBalance, ShouldEqual, 2099)
			})

			Convey("when CommitTransaction returns an error", func() {
				pachinkoRPC.On("CommitTransaction", ctx, commitTransactionReq).Return(nil, errors.New("WHAT WAS I JUST SAYING")).Times(4)

				updatedBalance, err := pachinkoClient.RemoveBits(ctx, transactionID, TwitchUserId, 1138)
				So(err, ShouldNotBeNil)
				So(updatedBalance, ShouldEqual, 0)
			})

			Convey("when CommitTransaction returns a non-retryable error", func() {
				pachinkoRPC.On("CommitTransaction", ctx, commitTransactionReq).Return(nil, twirp.NewError(twirp.FailedPrecondition, "WALRUS STRIKE")).Once()

				updatedBalance, err := pachinkoClient.RemoveBits(ctx, transactionID, TwitchUserId, 1138)
				So(err, ShouldNotBeNil)
				So(updatedBalance, ShouldEqual, 0)
			})
		})

		Convey("when StartTransaction returns an error", func() {
			errorTransactionID := "walrus-error"
			pachinkoRPC.On("StartTransaction", ctx, mock.MatchedBy(func(req *pachinko.StartTransactionReq) bool {
				return req.TransactionId == errorTransactionID
			})).Return(
				nil, errors.New("DON'T EVEN START WITH ME")).Times(4)

			updatedBalance, err := pachinkoClient.RemoveBits(ctx, errorTransactionID, TwitchUserId, 2070)
			So(err, ShouldNotBeNil)
			So(updatedBalance, ShouldEqual, 0)
		})

		Convey("when StartTransaction returns a validation error", func() {
			validationErrorTransactionID := "walrus-validation-error"
			pachinkoRPC.On("StartTransaction", ctx, mock.MatchedBy(func(req *pachinko.StartTransactionReq) bool {
				return req.TransactionId == validationErrorTransactionID
			})).Return(&pachinko.StartTransactionResp{
				ValidationError: &pachinko.ValidationError{
					Code: pachinko.ValidationErrorCode_INSUFFICIENT_BALANCE,
				},
			}, nil).Once()

			updatedBalance, err := pachinkoClient.RemoveBits(ctx, validationErrorTransactionID, TwitchUserId, 2070)
			So(err, ShouldNotBeNil)
			So(updatedBalance, ShouldEqual, 0)
		})
	})
}

func TestPachinkoClient_CheckHold(t *testing.T) {
	Convey("given a client", t, func() {
		pachinkoRPC := new(pachinko_mock.Pachinko)
		statter := new(metrics_mock.Statter)
		pachinkoClient := pachinkoClient{
			client:  pachinkoRPC,
			Statter: statter,
		}

		ctx := context.Background()
		ownerID := random.String(10)
		holdID := random.String(32)

		statter.On("Inc", mock.Anything, mock.Anything).Return()

		Convey("when CheckHold succeeds", func() {
			hold := &pachinko.Hold{
				TransactionId: holdID,
			}

			pachinkoRPC.On("CheckHold", ctx, &pachinko.CheckHoldReq{
				OwnerId:       ownerID,
				TransactionId: holdID,
			}).Return(&pachinko.CheckHoldResp{
				Hold: hold,
			}, nil)

			resp, err := pachinkoClient.CheckHold(ctx, ownerID, holdID)
			So(err, ShouldBeNil)
			So(resp, ShouldNotBeNil)
			So(resp.TransactionId, ShouldEqual, holdID)
		})

		Convey("when CheckHold returns an error", func() {
			pachinkoRPC.On("CheckHold", ctx, &pachinko.CheckHoldReq{
				OwnerId:       ownerID,
				TransactionId: holdID,
			}).Return(nil, errors.New("WALRUS STRIKE"))

			hold, err := pachinkoClient.CheckHold(ctx, ownerID, holdID)
			So(err, ShouldNotBeNil)
			So(hold, ShouldBeNil)
		})
	})
}

func TestPachinkoClient_CreateHold(t *testing.T) {
	ctx := context.Background()

	Convey("Given a client", t, func() {
		pachinkoRPC := new(pachinko_mock.Pachinko)
		statter := new(metrics_mock.Statter)
		pachinkoClient := pachinkoClient{
			client:  pachinkoRPC,
			Statter: statter,
		}

		expiresAt := time.Now().Add(time.Hour * 3)
		transactionID := "walrus-id"
		quantity := 1

		statter.On("Inc", mock.Anything, mock.Anything).Return()

		Convey("when StartTransaction succeeds", func() {
			pachinkoRPC.On("StartTransaction", ctx, mock.MatchedBy(func(req *pachinko.StartTransactionReq) bool {
				return req.TransactionId == transactionID
			})).Return(&pachinko.StartTransactionResp{
				TransactionId: transactionID,
			}, nil).Once()

			commitTransactionReq := &pachinko.CommitTransactionReq{
				TransactionId: transactionID,
				Domain:        pachinko.Domain_BITS,
			}

			Convey("when CommitTransaction succeeds", func() {
				pachinkoRPC.On("CommitTransaction", ctx, commitTransactionReq).Return(&pachinko.CommitTransactionResp{
					Tokens: []*pachinko.Token{
						{
							Quantity: 2099,
						},
					},
				}, nil).Once()

				updatedBalance, err := pachinkoClient.CreateHold(ctx, transactionID, TwitchUserId, quantity, &expiresAt)
				So(err, ShouldBeNil)
				So(updatedBalance, ShouldEqual, 2099)
			})

			Convey("when CommitTransaction returns an error", func() {
				pachinkoRPC.On("CommitTransaction", ctx, commitTransactionReq).Return(nil, errors.New("WHAT WAS I JUST SAYING")).Times(4)

				updatedBalance, err := pachinkoClient.CreateHold(ctx, transactionID, TwitchUserId, quantity, &expiresAt)
				So(err, ShouldNotBeNil)
				So(updatedBalance, ShouldEqual, 0)
			})

			Convey("when CommitTransaction returns a non-retryable error", func() {
				pachinkoRPC.On("CommitTransaction", ctx, commitTransactionReq).Return(nil, twirp.NewError(twirp.FailedPrecondition, "WALRUS STRIKE")).Once()

				updatedBalance, err := pachinkoClient.CreateHold(ctx, transactionID, TwitchUserId, quantity, &expiresAt)
				So(err, ShouldNotBeNil)
				So(updatedBalance, ShouldEqual, 0)
			})
		})

		Convey("when StartTransaction returns an error", func() {
			errorTransactionID := "walrus-error"
			pachinkoRPC.On("StartTransaction", ctx, mock.MatchedBy(func(req *pachinko.StartTransactionReq) bool {
				return req.TransactionId == errorTransactionID
			})).Return(
				nil, errors.New("DON'T EVEN START WITH ME")).Times(4)

			updatedBalance, err := pachinkoClient.CreateHold(ctx, errorTransactionID, TwitchUserId, quantity, &expiresAt)
			So(err, ShouldNotBeNil)
			So(updatedBalance, ShouldEqual, 0)
		})

		Convey("when StartTransaction returns a validation error", func() {
			validationErrorTransactionID := "walrus-validation-error"
			pachinkoRPC.On("StartTransaction", ctx, mock.MatchedBy(func(req *pachinko.StartTransactionReq) bool {
				return req.TransactionId == validationErrorTransactionID
			})).Return(&pachinko.StartTransactionResp{
				ValidationError: &pachinko.ValidationError{
					Code: pachinko.ValidationErrorCode_INSUFFICIENT_BALANCE,
				},
			}, nil).Once()

			updatedBalance, err := pachinkoClient.CreateHold(ctx, validationErrorTransactionID, TwitchUserId, 2070, &expiresAt)
			So(err, ShouldNotBeNil)
			So(updatedBalance, ShouldEqual, 0)
		})
	})
}

func TestPachinkoClient_FinalizeHold(t *testing.T) {
	hystrix.ConfigureForTests()
	ctx := context.Background()

	transactionID := "walrus-id"
	holdID := "walrus-hold"

	Convey("Given a client", t, func() {
		pachinkoRPC := new(pachinko_mock.Pachinko)
		statter := new(metrics_mock.Statter)
		pachinkoClient := pachinkoClient{
			client:  pachinkoRPC,
			Statter: statter,
		}

		statter.On("Inc", mock.Anything, mock.Anything).Return()

		Convey("when StartTransaction succeeds", func() {
			pachinkoRPC.On("StartTransaction", ctx, mock.MatchedBy(func(req *pachinko.StartTransactionReq) bool {
				return req.TransactionId == transactionID
			})).Return(&pachinko.StartTransactionResp{
				TransactionId: transactionID,
			}, nil).Once()

			commitTransactionReq := &pachinko.CommitTransactionReq{
				TransactionId: transactionID,
				Domain:        pachinko.Domain_BITS,
			}

			Convey("when CommitTransaction succeeds", func() {
				pachinkoRPC.On("CommitTransaction", ctx, commitTransactionReq).Return(&pachinko.CommitTransactionResp{
					Tokens: []*pachinko.Token{
						{
							Quantity: 2099,
						},
					},
				}, nil).Once()

				updatedBalance, err := pachinkoClient.FinalizeHold(ctx, transactionID, holdID, TwitchUserId)
				So(err, ShouldBeNil)
				So(updatedBalance, ShouldEqual, 2099)
			})

			Convey("when CommitTransaction returns an error", func() {
				pachinkoRPC.On("CommitTransaction", ctx, commitTransactionReq).Return(nil, errors.New("WHAT WAS I JUST SAYING")).Times(4)

				updatedBalance, err := pachinkoClient.FinalizeHold(ctx, transactionID, holdID, TwitchUserId)
				So(err, ShouldNotBeNil)
				So(updatedBalance, ShouldEqual, 0)
			})

			Convey("when CommitTransaction returns a non-retryable error", func() {
				pachinkoRPC.On("CommitTransaction", ctx, commitTransactionReq).Return(nil, twirp.NewError(twirp.FailedPrecondition, "WALRUS STRIKE")).Once()

				updatedBalance, err := pachinkoClient.FinalizeHold(ctx, transactionID, holdID, TwitchUserId)
				So(err, ShouldNotBeNil)
				So(updatedBalance, ShouldEqual, 0)
			})
		})

		Convey("when StartTransaction returns an error", func() {
			failedTransactionID := "walrus-failure"
			pachinkoRPC.On("StartTransaction", ctx, mock.MatchedBy(func(req *pachinko.StartTransactionReq) bool {
				return req.TransactionId == failedTransactionID
			})).Return(
				nil, errors.New("DON'T EVEN START WITH ME")).Times(4)

			updatedBalance, err := pachinkoClient.FinalizeHold(ctx, failedTransactionID, holdID, TwitchUserId)
			So(err, ShouldNotBeNil)
			So(updatedBalance, ShouldEqual, 0)
		})

		Convey("when StartTransaction returns a validation error", func() {
			validationErrorTransactionID := "walrus-validation-failure"
			pachinkoRPC.On("StartTransaction", ctx, mock.MatchedBy(func(req *pachinko.StartTransactionReq) bool {
				return req.TransactionId == validationErrorTransactionID
			})).Return(&pachinko.StartTransactionResp{
				ValidationError: &pachinko.ValidationError{
					Code: pachinko.ValidationErrorCode_INSUFFICIENT_BALANCE,
				},
			}, nil).Once()

			updatedBalance, err := pachinkoClient.FinalizeHold(ctx, validationErrorTransactionID, holdID, TwitchUserId)
			So(err, ShouldNotBeNil)
			So(updatedBalance, ShouldEqual, 0)
		})

		Convey("when StartTransaction returns a TRANSACTION_COMMITTED validation error", func() {
			transactionCommittedTransactionID := "walrus-transaction-committed"
			pachinkoRPC.On("StartTransaction", ctx, mock.MatchedBy(func(req *pachinko.StartTransactionReq) bool {
				return req.TransactionId == transactionCommittedTransactionID
			})).Return(&pachinko.StartTransactionResp{
				ValidationError: &pachinko.ValidationError{
					Code: pachinko.ValidationErrorCode_TRANSACTION_COMMITTED,
				},
			}, nil).Once()

			updatedBalance, err := pachinkoClient.FinalizeHold(ctx, transactionCommittedTransactionID, holdID, TwitchUserId)
			So(err, ShouldNotBeNil)
			So(err, ShouldEqual, ErrAlreadyCommitted)
			So(updatedBalance, ShouldEqual, 0)
		})
	})
}

func TestPachinkoClient_ReleaseHold(t *testing.T) {
	hystrix.ConfigureForTests()
	ctx := context.Background()

	transactionID := "walrus-id"
	holdID := "walrus-hold"

	Convey("Given a client", t, func() {
		pachinkoRPC := new(pachinko_mock.Pachinko)
		statter := new(metrics_mock.Statter)
		pachinkoClient := pachinkoClient{
			client:  pachinkoRPC,
			Statter: statter,
		}

		statter.On("Inc", mock.Anything, mock.Anything).Return()

		Convey("when StartTransaction succeeds", func() {
			pachinkoRPC.On("StartTransaction", ctx, mock.MatchedBy(func(req *pachinko.StartTransactionReq) bool {
				return req.TransactionId == transactionID
			})).Return(&pachinko.StartTransactionResp{
				TransactionId: transactionID,
			}, nil).Once()

			commitTransactionReq := &pachinko.CommitTransactionReq{
				TransactionId: transactionID,
				Domain:        pachinko.Domain_BITS,
			}

			Convey("when CommitTransaction succeeds", func() {
				pachinkoRPC.On("CommitTransaction", ctx, commitTransactionReq).Return(&pachinko.CommitTransactionResp{
					Tokens: []*pachinko.Token{
						{
							Quantity: 2099,
						},
					},
				}, nil).Once()

				updatedBalance, err := pachinkoClient.ReleaseHold(ctx, transactionID, holdID, TwitchUserId)
				So(err, ShouldBeNil)
				So(updatedBalance, ShouldEqual, 2099)
			})

			Convey("when CommitTransaction returns an error", func() {
				pachinkoRPC.On("CommitTransaction", ctx, commitTransactionReq).Return(nil, errors.New("WHAT WAS I JUST SAYING")).Times(4)

				updatedBalance, err := pachinkoClient.ReleaseHold(ctx, transactionID, holdID, TwitchUserId)
				So(err, ShouldNotBeNil)
				So(updatedBalance, ShouldEqual, 0)
			})

			Convey("when CommitTransaction returns a non-retryable error", func() {
				pachinkoRPC.On("CommitTransaction", ctx, commitTransactionReq).Return(nil, twirp.NewError(twirp.FailedPrecondition, "WALRUS STRIKE")).Once()

				updatedBalance, err := pachinkoClient.ReleaseHold(ctx, transactionID, holdID, TwitchUserId)
				So(err, ShouldNotBeNil)
				So(updatedBalance, ShouldEqual, 0)
			})
		})

		Convey("when StartTransaction returns an error", func() {
			errorTransactionID := "walrus-error"
			pachinkoRPC.On("StartTransaction", ctx, mock.MatchedBy(func(req *pachinko.StartTransactionReq) bool {
				return req.TransactionId == errorTransactionID
			})).Return(
				nil, errors.New("DON'T EVEN START WITH ME")).Times(4)

			updatedBalance, err := pachinkoClient.ReleaseHold(ctx, errorTransactionID, holdID, TwitchUserId)
			So(err, ShouldNotBeNil)
			So(updatedBalance, ShouldEqual, 0)
		})

		Convey("when StartTransaction returns a validation error", func() {
			validationErrorTransactionID := "walrus-validation-error"
			pachinkoRPC.On("StartTransaction", ctx, mock.MatchedBy(func(req *pachinko.StartTransactionReq) bool {
				return req.TransactionId == validationErrorTransactionID
			})).Return(&pachinko.StartTransactionResp{
				ValidationError: &pachinko.ValidationError{
					Code: pachinko.ValidationErrorCode_INSUFFICIENT_BALANCE,
				},
			}, nil).Once()

			updatedBalance, err := pachinkoClient.ReleaseHold(ctx, validationErrorTransactionID, holdID, TwitchUserId)
			So(err, ShouldNotBeNil)
			So(updatedBalance, ShouldEqual, 0)
		})

		Convey("when StartTransaction returns a TRANSACTION_COMMITTED validation error", func() {
			transactionCommittedID := "walrus-transaction-committed"
			pachinkoRPC.On("StartTransaction", ctx, mock.MatchedBy(func(req *pachinko.StartTransactionReq) bool {
				return req.TransactionId == transactionCommittedID
			})).Return(&pachinko.StartTransactionResp{
				ValidationError: &pachinko.ValidationError{
					Code: pachinko.ValidationErrorCode_TRANSACTION_COMMITTED,
				},
			}, nil).Once()

			updatedBalance, err := pachinkoClient.FinalizeHold(ctx, transactionCommittedID, holdID, TwitchUserId)
			So(err, ShouldNotBeNil)
			So(err, ShouldEqual, ErrAlreadyCommitted)
			So(updatedBalance, ShouldEqual, 0)
		})
	})
}

func TestPachinkoClient_DeleteBalanceByOwnerId(t *testing.T) {
	hystrix.ConfigureForTests()
	ctx := context.Background()

	testUser := "test-pdms-user"

	Convey("Given a client", t, func() {
		pachinkoRPC := new(pachinko_mock.Pachinko)
		statter := new(metrics_mock.Statter)
		pachinkoClient := pachinkoClient{
			client:  pachinkoRPC,
			Statter: statter,
		}
		statter.On("Inc", mock.Anything, mock.Anything).Return()
		Convey("when DeleteBalanceByOwnerId succeeds", func() {
			pachinkoRPC.On("DeleteBalanceByOwnerId", ctx, &(pachinko.DeleteBalanceByOwnerIdReq{
				OwnerId: testUser,
			})).Return(&pachinko.DeleteBalanceByOwnerIdResp{}, nil)

			err := pachinkoClient.DeleteBalanceByOwnerId(ctx, testUser)
			So(err, ShouldBeNil)
		})

		Convey("when DeleteBalanceByOwnerId fails", func() {
			pachinkoRPC.On("DeleteBalanceByOwnerId", ctx, &(pachinko.DeleteBalanceByOwnerIdReq{
				OwnerId: testUser,
			})).Return(nil, errors.New("WOW, SO MISTAKE, MUCH ERROR"))

			err := pachinkoClient.DeleteBalanceByOwnerId(ctx, testUser)
			So(err, ShouldNotBeNil)
		})
	})
}
