package uploadservice

import (
	"context"
	"errors"
	"strings"
	"testing"

	"code.justin.tv/commerce/payday/actions/constants"
	uploadservice_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/web/upload-service/rpc/uploader"
	"code.justin.tv/commerce/payday/models/api"
	"code.justin.tv/web/upload-service/rpc/uploader"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

const (
	testChannelId             = "001"
	testSetId                 = "test_set_id"
	testTier                  = "1"
	testBackground            = "light"
	testAnimatedAnimationType = "animated"
	testStaticAnimationType   = "static"
	testScale                 = "4"
	testUrl                   = "test_url"
	testUploadId              = "test_upload_id"
	testCloudFrontPrefix      = "https://cloudfront.net"
	testS3BucketName          = "testS3BucketName"
)

func buildValidAnimatedUploadServiceInput() UploadServiceInput {
	return UploadServiceInput{
		ChannelId:     testChannelId,
		SetId:         testSetId,
		Tier:          api.Tier(testTier),
		Background:    api.Background(testBackground),
		AnimationType: api.AnimationType(testAnimatedAnimationType),
		Scale:         api.Scale(testScale),
	}
}

func buildValidStaticUploadServiceInput() UploadServiceInput {
	return UploadServiceInput{
		ChannelId:     testChannelId,
		SetId:         testSetId,
		Tier:          api.Tier(testTier),
		Background:    api.Background(testBackground),
		AnimationType: api.AnimationType(testStaticAnimationType),
		Scale:         api.Scale(testScale),
	}
}

func TestImageUploaderImpl(t *testing.T) {
	Convey("Given an Image Uploader", t, func() {
		uploaderService := new(uploadservice_mock.Uploader)

		imageUploader := ImageUploaderImpl{
			Uploader: uploaderService,
			Config: ImageUploaderConfig{
				UploadS3BucketName: testS3BucketName,
				CloudFrontPrefix:   testCloudFrontPrefix,
			},
		}

		Convey("Given valid input to Upload function", func() {
			validUploadServiceInput := buildValidAnimatedUploadServiceInput()

			Convey("Fails when Uploader Service Create call fails", func() {
				uploaderService.On("Create", mock.Anything, mock.Anything).Return(nil, errors.New("test error"))

				_, _, err := imageUploader.Upload(context.Background(), validUploadServiceInput)

				So(err, ShouldNotBeNil)
			})

			Convey("Succeeds and returns results from Uploader Service when Uploader Service Create call succeeds", func() {
				uploadResponse := uploader.UploadResponse{
					Url:      testUrl,
					UploadId: testUploadId,
				}
				uploaderService.On("Create", mock.Anything, mock.Anything).Return(&uploadResponse, nil)

				uploadCustomCheermoteResponse, s3Urls, err := imageUploader.Upload(context.Background(), validUploadServiceInput)

				So(err, ShouldBeNil)
				So(uploadCustomCheermoteResponse.UploadURL, ShouldEqual, testUrl)
				So(uploadCustomCheermoteResponse.UploadID, ShouldEqual, testUploadId)

				// Upload only ever changes a single image
				So(len(s3Urls), ShouldEqual, 1)

				for _, url := range s3Urls {
					So(url, ShouldStartWith, testChannelId)
				}

			})
		})

		Convey("Given valid input to SmartUpload function", func() {
			validAnimatedUploadServiceInput := buildValidAnimatedUploadServiceInput()
			validStaticUploadServiceInput := buildValidStaticUploadServiceInput()
			expectedS3LocationPrefix := testChannelId + "/" + testSetId + "/" + testTier + "/" + testBackground + "/"
			expectedCloudFrontUrlPrefix := testCloudFrontPrefix + "/" + expectedS3LocationPrefix

			Convey("Fails when Uploader Service Create call fails", func() {
				uploaderService.On("Create", mock.Anything, mock.Anything).Return(nil, errors.New("test error"))

				_, _, err := imageUploader.SmartUpload(context.Background(), validAnimatedUploadServiceInput)

				So(err, ShouldNotBeNil)
			})

			Convey("Succeeds and returns results from Uploader Service when Uploader Service Create call succeeds", func() {
				uploadResponse := uploader.UploadResponse{
					Url:      testUrl,
					UploadId: testUploadId,
				}
				uploaderService.On("Create", mock.Anything, mock.Anything).Return(&uploadResponse, nil)

				uploadCustomCheermoteResponse, s3Urls, err := imageUploader.SmartUpload(context.Background(), validAnimatedUploadServiceInput)
				expectedNumberOfUrls := len(constants.AnimationTypes) * len(constants.Scales)

				So(err, ShouldBeNil)
				So(uploadCustomCheermoteResponse.UploadURL, ShouldEqual, testUrl)
				So(uploadCustomCheermoteResponse.UploadID, ShouldEqual, testUploadId)

				So(len(s3Urls), ShouldEqual, expectedNumberOfUrls)
				for _, url := range s3Urls {
					So(url, ShouldStartWith, expectedS3LocationPrefix)
				}
			})

			Convey("Given the 'animated' animation type as input, builds and returns urls for both animated and static animation types for all scales", func() {
				uploadResponse := uploader.UploadResponse{
					Url:      testUrl,
					UploadId: testUploadId,
				}
				uploaderService.On("Create", mock.Anything, mock.Anything).Return(&uploadResponse, nil)

				uploadCustomCheermoteResponse, s3Urls, err := imageUploader.SmartUpload(context.Background(), validAnimatedUploadServiceInput)

				So(err, ShouldBeNil)

				returnedImageUrls := uploadCustomCheermoteResponse.ImageURLs
				expectedNumberOfUrls := len(constants.AnimationTypes) * len(constants.Scales)

				So(len(returnedImageUrls), ShouldEqual, expectedNumberOfUrls)
				for index, url := range returnedImageUrls {
					So(url, ShouldStartWith, expectedCloudFrontUrlPrefix)

					if index < expectedNumberOfUrls/2 {
						So(strings.Contains(url, "animated"), ShouldBeTrue)
					} else {
						So(strings.Contains(url, "static"), ShouldBeTrue)
					}
				}

				So(len(s3Urls), ShouldEqual, expectedNumberOfUrls)
				for index, url := range s3Urls {
					So(url, ShouldStartWith, expectedS3LocationPrefix)

					if index < expectedNumberOfUrls/2 {
						So(strings.Contains(url, "animated"), ShouldBeTrue)
					} else {
						So(strings.Contains(url, "static"), ShouldBeTrue)
					}
				}
			})

			Convey("Given the 'static' animation type as input, builds and returns urls for static animation types for all scales", func() {
				uploadResponse := uploader.UploadResponse{
					Url:      testUrl,
					UploadId: testUploadId,
				}
				uploaderService.On("Create", mock.Anything, mock.Anything).Return(&uploadResponse, nil)

				uploadCustomCheermoteResponse, s3Urls, err := imageUploader.SmartUpload(context.Background(), validStaticUploadServiceInput)

				So(err, ShouldBeNil)

				returnedImageUrls := uploadCustomCheermoteResponse.ImageURLs
				expectedNumberOfUrls := len(constants.Scales)

				So(len(returnedImageUrls), ShouldEqual, expectedNumberOfUrls)
				for _, url := range returnedImageUrls {
					So(url, ShouldStartWith, expectedCloudFrontUrlPrefix)
					So(strings.Contains(url, "static"), ShouldBeTrue)
				}

				So(len(s3Urls), ShouldEqual, expectedNumberOfUrls)
				for _, url := range s3Urls {
					So(url, ShouldStartWith, expectedS3LocationPrefix)
					So(strings.Contains(url, "static"), ShouldBeTrue)
				}
			})
		})
	})
}
