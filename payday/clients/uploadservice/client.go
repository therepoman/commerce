package uploadservice

import (
	"context"
	"fmt"
	"net/http"

	log "code.justin.tv/commerce/logrus"

	"code.justin.tv/commerce/payday/actions/constants"
	apimodel "code.justin.tv/commerce/payday/models/api"
	"code.justin.tv/web/upload-service/rpc/uploader"
	"github.com/pkg/errors"
)

type Dimension int

const (
	ImageIDFormat = "%s/%s/%s/%s/%s/%s.%s"
)

var scaleToImgDimension map[apimodel.Scale]Dimension = map[apimodel.Scale]Dimension{
	constants.Scale1:  Dimension(28),
	constants.Scale15: Dimension(42),
	constants.Scale2:  Dimension(56),
	constants.Scale3:  Dimension(84),
	constants.Scale4:  Dimension(112),
}

// IImageUploader is the wrapper around the general upload service
type IImageUploader interface {
	Upload(ctx context.Context, input UploadServiceInput) (*apimodel.UploadCustomCheermoteResponse, []string, error)
	SmartUpload(ctx context.Context, input UploadServiceInput) (*apimodel.UploadCustomCheermoteResponse, []string, error)
	GetUploader() uploader.Uploader
}

type ImageUploaderImpl struct {
	Uploader uploader.Uploader
	Config   ImageUploaderConfig
}

// ImageUploaderConfig are the config values needed to upload image(s) using the generic upload service
type ImageUploaderConfig struct {
	UploadServiceURL       string
	UploadCallbackSNSTopic string
	UploadS3BucketName     string
	CloudFrontPrefix       string
}

type UploadServiceInput struct {
	ChannelId     string
	SetId         string
	Tier          apimodel.Tier
	Background    apimodel.Background
	AnimationType apimodel.AnimationType
	Scale         apimodel.Scale
	FileMaxSize   string
}

// NewImageUploader returns a new image uploader or errors if missing required config
func NewImageUploader(config ImageUploaderConfig, client *http.Client) (IImageUploader, error) {
	if config.UploadServiceURL == "" {
		return nil, errors.New("no upload service url")
	}

	if config.UploadS3BucketName == "" {
		return nil, errors.New("no s3 bucket provided")
	}

	if config.UploadCallbackSNSTopic == "" {
		return nil, errors.New("no sns callback")
	}

	return &ImageUploaderImpl{
		Uploader: uploader.NewUploaderProtobufClient(config.UploadServiceURL, client),
		Config:   config,
	}, nil
}

func GetFileExtension(animationType apimodel.AnimationType) string {
	if animationType == constants.StaticAnimationType {
		return "png"
	}
	return "gif"
}

func getDimensionConstraint(scale apimodel.Scale) []*uploader.Constraint {
	return []*uploader.Constraint{
		{
			Value: float64(scaleToImgDimension[scale]),
			Test:  "<=",
		},
	}
}

func getOutput(outputDestination string, resizeToScale apimodel.Scale, isAnimation bool) *uploader.Output {
	return &uploader.Output{
		Name: outputDestination,
		Permissions: &uploader.Permissions{
			GrantRead: "uri=http://acs.amazonaws.com/groups/global/AllUsers",
		},
		Transformations: []*uploader.Transformation{
			{
				Transformation: &uploader.Transformation_Resize{
					Resize: &uploader.Resize{
						Size: &uploader.Resize_Dimensions{
							Dimensions: &uploader.Dimensions{
								Width: uint32(scaleToImgDimension[resizeToScale]),
							},
						},
					},
				},
			},
		},
		AllowAnimation: isAnimation,
	}
}

func (u *ImageUploaderImpl) getOutputImageURL(s3Key string) string {
	return fmt.Sprintf("%s/%s", u.Config.CloudFrontPrefix, s3Key)
}

func (u *ImageUploaderImpl) getRequest(input UploadServiceInput) *uploader.UploadRequest {
	return &uploader.UploadRequest{
		PreValidation: &uploader.Validation{
			FileSizeLessThan:  input.FileMaxSize,
			Format:            "image",
			HeightConstraints: getDimensionConstraint(input.Scale),
			WidthConstraints:  getDimensionConstraint(input.Scale),
		},
		Callback: &uploader.Callback{
			SnsTopicArn: u.Config.UploadCallbackSNSTopic,
			PubsubTopic: "default",
		},
		OutputPrefix: fmt.Sprintf("s3://%s", u.Config.UploadS3BucketName),
	}
}

func (u *ImageUploaderImpl) GetUploader() uploader.Uploader {
	return u.Uploader
}

func (u *ImageUploaderImpl) Upload(ctx context.Context, input UploadServiceInput) (*apimodel.UploadCustomCheermoteResponse, []string, error) {
	var uploadedImageURLs []string
	var s3ImageLocations []string
	var outputs []*uploader.Output
	uploadRequest := u.getRequest(input)
	isAnimation := input.AnimationType == constants.AnimatedAnimationType

	s3Key := fmt.Sprintf(ImageIDFormat, input.ChannelId, input.SetId, input.Tier, input.Background, input.AnimationType, input.Scale, GetFileExtension(input.AnimationType))
	outputs = append(outputs, getOutput(s3Key, input.Scale, isAnimation))
	uploadedImageURLs = append(uploadedImageURLs, u.getOutputImageURL(s3Key))
	s3ImageLocations = append(s3ImageLocations, s3Key)

	uploadRequest.Outputs = outputs
	resp, err := u.Uploader.Create(ctx, uploadRequest)

	if err != nil {
		return nil, []string{}, err
	}

	log.Infof("CustomCheermotePreUpload uploadedImageURLs: %s", uploadedImageURLs)
	log.Infof("CustomCheermotePreUpload s3ImageLocations: %s", s3ImageLocations)
	return &apimodel.UploadCustomCheermoteResponse{
		UploadURL: resp.Url,
		UploadID:  resp.UploadId,
		ImageURLs: uploadedImageURLs,
	}, s3ImageLocations, nil
}

func (u *ImageUploaderImpl) SmartUpload(ctx context.Context, input UploadServiceInput) (*apimodel.UploadCustomCheermoteResponse, []string, error) {
	var uploadedImageURLs []string
	var s3ImageLocations []string
	var outputs []*uploader.Output
	uploadRequest := u.getRequest(input)
	var animationTypesToUpload []apimodel.AnimationType

	// "Animated" for animation type is also the default, so we use the first frame and upload static images as well
	if input.AnimationType == constants.AnimatedAnimationType {
		animationTypesToUpload = []apimodel.AnimationType{constants.AnimatedAnimationType, constants.StaticAnimationType}
	} else {
		animationTypesToUpload = []apimodel.AnimationType{input.AnimationType}
	}

	for _, animationType := range animationTypesToUpload {
		isAnimation := animationType == constants.AnimatedAnimationType
		for scale := range constants.Scales {
			s3Key := fmt.Sprintf(ImageIDFormat, input.ChannelId, input.SetId, input.Tier, input.Background, animationType, scale, GetFileExtension(animationType))
			outputs = append(outputs, getOutput(s3Key, scale, isAnimation))
			uploadedImageURLs = append(uploadedImageURLs, u.getOutputImageURL(s3Key))
			s3ImageLocations = append(s3ImageLocations, s3Key)
		}
	}

	uploadRequest.Outputs = outputs
	resp, err := u.Uploader.Create(context.Background(), uploadRequest)
	if err != nil {
		return nil, []string{}, err
	}

	log.Infof("CustomCheermotePreUpload uploadedImageURLs: %s", uploadedImageURLs)
	log.Infof("CustomCheermotePreUpload s3ImageLocations: %s", s3ImageLocations)
	return &apimodel.UploadCustomCheermoteResponse{
		UploadURL: resp.Url,
		UploadID:  resp.UploadId,
		ImageURLs: uploadedImageURLs,
	}, s3ImageLocations, nil
}
