package nitro

import (
	"context"
	"errors"
	"testing"

	nitro_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/samus/nitro/rpc"
	nitro "code.justin.tv/samus/nitro/rpc"
	. "github.com/smartystreets/goconvey/convey"
)

func TestNitroClient_GetPrimeStatus(t *testing.T) {
	Convey("Given Nitro client", t, func() {
		nitroRpc := new(nitro_mock.Nitro)
		client := nitroClient{
			client: nitroRpc,
		}
		ctx := context.Background()
		userID := "12345"

		Convey("when GetPremiumStatuses errors", func() {
			nitroRpc.On("GetPremiumStatuses", ctx, &nitro.GetPremiumStatusesRequest{
				TwitchUserID: userID,
			}).Return(nil, errors.New("e")).Once()

			hasPrime, err := client.GetPrimeStatus(ctx, userID)
			So(hasPrime, ShouldBeFalse)
			So(err, ShouldNotBeNil)
		})

		Convey("when GetPremiumStatuses succeeds", func() {
			nitroRpc.On("GetPremiumStatuses", ctx, &nitro.GetPremiumStatusesRequest{
				TwitchUserID: userID,
			}).Return(&nitro.GetPremiumStatusesResponse{
				HasPrime: true,
			}, nil).Once()

			hasPrime, err := client.GetPrimeStatus(ctx, userID)
			So(hasPrime, ShouldBeTrue)
			So(err, ShouldBeNil)
		})
	})
}
