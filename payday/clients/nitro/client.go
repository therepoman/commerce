package nitro

import (
	"context"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/hystrix"
	cmds "code.justin.tv/commerce/payday/hystrix"
	nitro "code.justin.tv/samus/nitro/rpc"
)

type NitroClient interface {
	GetPrimeStatus(ctx context.Context, userID string) (bool, error)
}

type nitroClient struct {
	client nitro.Nitro
}

func NewClient(cfg config.Configuration) NitroClient {
	return &nitroClient{}
}

func (n *nitroClient) GetPrimeStatus(ctx context.Context, userID string) (hasPrime bool, err error) {
	err = hystrix.Do(cmds.NitroGetPremiumStatusesCommand, func() error {
		resp, err := n.client.GetPremiumStatuses(ctx, &nitro.GetPremiumStatusesRequest{
			TwitchUserID: userID,
		})

		if err != nil {
			log.WithField("userID", userID).WithError(err).Error("Error calling Nitro")
		} else {
			hasPrime = resp.HasPrime
		}
		return err
	}, nil)
	return
}
