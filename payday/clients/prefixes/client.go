package prefixes

import (
	"context"
	"time"

	"code.justin.tv/commerce/payday/clients/mako"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/metrics"
	"code.justin.tv/commerce/payday/user"
	"github.com/cenkalti/backoff"
)

const (
	PREFIXES_FAILURE_METRIC                = "Prefixes_Failure"
	PREFIX_GET_EMOTE_PREFIX_FAILURE_METRIC = "Prefixes_Failure_Get_Emote_Prefix"
)

type PrefixesResponse struct {
	Prefixes []string `json:"prefixes"`
}

type IPrefixesClient interface {
	GetPrefixes(ctx context.Context, ownerId string) (PrefixesResponse, error)
}

type PrefixesClient struct {
	MakoClient mako.MakoClient `inject:""`
	Statter    metrics.Statter `inject:""`
}

func (p *PrefixesClient) GetPrefixes(ctx context.Context, ownerId string) (PrefixesResponse, error) {
	if user.IsATestAccount(ownerId) {
		return PrefixesResponse{}, nil
	}

	output := PrefixesResponse{}

	f := func() error {
		prefix, err := p.MakoClient.GetPrefix(ctx, ownerId)
		if err != nil {
			return errors.Notef(err, "Error performing mako service request")
		}

		output.Prefixes = []string{prefix}

		return nil
	}

	b := backoff.WithMaxTries(backoff.WithContext(newBackoff(), ctx), 3)
	err := backoff.Retry(f, b)
	if err != nil {
		go p.incrementFailureMetrics(PREFIX_GET_EMOTE_PREFIX_FAILURE_METRIC)
		return PrefixesResponse{}, err
	}

	return output, nil
}

func (p *PrefixesClient) incrementFailureMetrics(metricName string) {
	p.Statter.Inc(metricName, 1)
	p.Statter.Inc(PREFIXES_FAILURE_METRIC, 1)
}

func newBackoff() backoff.BackOff {
	exponential := backoff.NewExponentialBackOff()
	exponential.InitialInterval = 10 * time.Millisecond
	exponential.MaxElapsedTime = 1 * time.Second
	exponential.Multiplier = 2
	return exponential
}
