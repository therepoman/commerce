package sws

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"net/http"
	"time"

	"code.justin.tv/commerce/payday/clients/sandstorm"
	"code.justin.tv/commerce/payday/config"
	utils "code.justin.tv/commerce/payday/utils/close"
)

type Client interface {
	Ping(ctx context.Context) (time.Duration, error)
}

type client struct {
	Endpoint   string
	HttpClient *http.Client
}

func NewSWSClient() (Client, error) {
	cfg := config.Get()

	mgr := sandstorm.New(cfg.SandstormRole)
	rootCert, err := mgr.Get(cfg.SandtormAmazonRootCert)
	if err != nil {
		return nil, err
	}

	swsSecret, err := mgr.Get(cfg.SandtormSWSSecret)
	if err != nil {
		return nil, err
	}

	clientCert, err := tls.X509KeyPair(swsSecret.Plaintext, swsSecret.Plaintext)
	if err != nil {
		return nil, err
	}

	caCertPool := x509.NewCertPool()
	caCertPool.AppendCertsFromPEM(rootCert.Plaintext)

	tlsConfig := &tls.Config{
		RootCAs:            caCertPool,
		Certificates:       []tls.Certificate{clientCert},
		InsecureSkipVerify: false,
	}

	transport := &http.Transport{TLSClientConfig: tlsConfig}
	httpClient := &http.Client{Transport: transport}

	return &client{
		Endpoint:   cfg.SWSHostAddress,
		HttpClient: httpClient,
	}, nil
}

func (sws *client) Ping(ctx context.Context) (dur time.Duration, err error) {
	startTime := time.Now()
	defer func() {
		dur = time.Since(startTime)
	}()

	req, err := http.NewRequest("GET", fmt.Sprintf("https://%s/ping", sws.Endpoint), nil)
	if err != nil {
		return dur, err
	}

	resp, err := sws.HttpClient.Do(req.WithContext(ctx))
	if err != nil {
		return dur, err
	}
	defer utils.Close(resp.Body)

	if resp.StatusCode != 200 {
		return dur, fmt.Errorf("returned non-200 status code: %d", resp.StatusCode)
	}

	return dur, nil
}
