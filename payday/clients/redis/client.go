package redis

import (
	"context"
	"fmt"
	"math/rand"
	"time"

	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/metrics"
	metrics_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/metrics"
	"github.com/alicebob/miniredis"
	"github.com/go-redis/redis"
	"github.com/stretchr/testify/mock"
)

const (
	operationMetric         = "redis-op.%s"
	operationDurationMetric = "redis-dur.%s"
	connHitMetric           = "redis-conn-hits"
	connMissMetric          = "redis-conn-misses"
	connTimeoutMetric       = "redis-conn-timeouts"
	totalConnMetric         = "redis-total-conns"
	idleConnMetric          = "redis-idle-conns"
	staleConnMetric         = "redis-stale-conns"
)

type Client interface {
	Pipeline(ctx context.Context) redis.Pipeliner
	Del(ctx context.Context, keys ...string) *redis.IntCmd
	Dump(ctx context.Context, key string) *redis.StringCmd
	Exists(ctx context.Context, keys []string) *redis.IntCmd
	Expire(ctx context.Context, key string, tm time.Duration) *redis.BoolCmd
	ExpireAt(ctx context.Context, key string, tm time.Time) *redis.BoolCmd
	Get(ctx context.Context, key string) *redis.StringCmd
	Ping(ctx context.Context) *redis.StatusCmd
	Restore(ctx context.Context, key string, ttl time.Duration, value string) *redis.StatusCmd
	Set(ctx context.Context, key string, value interface{}, expiration time.Duration) *redis.StatusCmd
	TTL(ctx context.Context, key string) *redis.DurationCmd
	ZAdd(ctx context.Context, key string, members ...*redis.Z) *redis.IntCmd
	ZCard(ctx context.Context, key string) *redis.IntCmd
	ZCount(ctx context.Context, key, min string, max string) *redis.IntCmd
	ZRem(ctx context.Context, key string, members ...interface{}) *redis.IntCmd
	ZRevRank(ctx context.Context, key, member string) *redis.IntCmd
	ZRevRangeWithScores(ctx context.Context, key string, start, stop int64) *redis.ZSliceCmd
	ZScore(ctx context.Context, key, member string) *redis.FloatCmd
	HGet(ctx context.Context, key, field string) *redis.StringCmd
	HGetAll(ctx context.Context, key string) *redis.StringStringMapCmd
	HSet(ctx context.Context, key, field string, value interface{}) *redis.BoolCmd
	HMSet(ctx context.Context, key string, fields map[string]interface{}) *redis.StatusCmd
	HDel(ctx context.Context, key string, fields ...string) *redis.IntCmd
}

type client struct {
	RedisClient redis.Cmdable   `inject:"clusterClient"`
	Statter     metrics.Statter `inject:""`
}

func NewClient(cfg config.Configuration) Client {
	return &client{}
}

func TestRedisClient() (Client, *miniredis.Miniredis) {
	server, err := miniredis.Run()
	if err != nil {
		panic(fmt.Sprintf("Could not start miniredis server. %v", err))
	}

	statter := new(metrics_mock.Statter)
	statter.On("TimingDuration", mock.Anything, mock.Anything).Return()
	statter.On("Inc", mock.Anything, mock.Anything).Return()
	statter.On("Gauge", mock.Anything, mock.Anything).Return()

	return &client{
		RedisClient: redis.NewClient(&redis.Options{
			Addr: server.Addr(),
		}),
		Statter: statter,
	}, server
}

func (c *client) Pipeline(_ context.Context) redis.Pipeliner {
	start := time.Now()
	defer c.recordStats(c.RedisClient, "pipeline", start)
	return c.RedisClient.Pipeline()
}

func (c *client) Del(_ context.Context, keys ...string) *redis.IntCmd {
	maybeRecordStats := c.makeMaybeStatRecorder(c.RedisClient, "del", 0.01)
	defer maybeRecordStats()
	return c.RedisClient.Del(keys...)
}

func (c *client) Dump(_ context.Context, key string) *redis.StringCmd {
	start := time.Now()
	defer c.recordStats(c.RedisClient, "dump", start)
	return c.RedisClient.Dump(key)
}

func (c *client) Exists(_ context.Context, keys []string) *redis.IntCmd {
	start := time.Now()
	defer c.recordStats(c.RedisClient, "exists", start)
	return c.RedisClient.Exists(keys...)
}

func (c *client) Expire(_ context.Context, key string, dur time.Duration) *redis.BoolCmd {
	start := time.Now()
	defer c.recordStats(c.RedisClient, "expire", start)
	return c.RedisClient.Expire(key, dur)
}

func (c *client) ExpireAt(_ context.Context, key string, tm time.Time) *redis.BoolCmd {
	start := time.Now()
	defer c.recordStats(c.RedisClient, "expire-at", start)
	return c.RedisClient.ExpireAt(key, tm)
}

func (c *client) Get(_ context.Context, key string) *redis.StringCmd {
	maybeRecordStats := c.makeMaybeStatRecorder(c.RedisClient, "get", 0.0001)
	defer maybeRecordStats()
	return c.RedisClient.Get(key)
}

func (c *client) Ping(_ context.Context) *redis.StatusCmd {
	start := time.Now()
	defer c.recordStats(c.RedisClient, "ping", start)
	return c.RedisClient.Ping()
}

func (c *client) Restore(_ context.Context, key string, ttl time.Duration, value string) *redis.StatusCmd {
	start := time.Now()
	defer c.recordStats(c.RedisClient, "restore", start)
	return c.RedisClient.Restore(key, ttl, value)
}

func (c *client) Set(_ context.Context, key string, value interface{}, expiration time.Duration) *redis.StatusCmd {
	maybeRecordStats := c.makeMaybeStatRecorder(c.RedisClient, "set", 0.01)
	defer maybeRecordStats()
	return c.RedisClient.Set(key, value, expiration)
}

func (c *client) TTL(_ context.Context, key string) *redis.DurationCmd {
	start := time.Now()
	defer c.recordStats(c.RedisClient, "ttl", start)
	return c.RedisClient.TTL(key)
}

func (c *client) ZAdd(_ context.Context, key string, members ...*redis.Z) *redis.IntCmd {
	start := time.Now()
	defer c.recordStats(c.RedisClient, "z-add", start)
	return c.RedisClient.ZAdd(key, members...)
}

func (c *client) ZCard(_ context.Context, key string) *redis.IntCmd {
	start := time.Now()
	defer c.recordStats(c.RedisClient, "z-card", start)
	return c.RedisClient.ZCard(key)
}

func (c *client) ZRem(_ context.Context, key string, members ...interface{}) *redis.IntCmd {
	start := time.Now()
	defer c.recordStats(c.RedisClient, "z-rem", start)
	return c.RedisClient.ZRem(key, members...)
}

func (c *client) ZRevRank(_ context.Context, key, member string) *redis.IntCmd {
	start := time.Now()
	defer c.recordStats(c.RedisClient, "z-rev-rank", start)
	return c.RedisClient.ZRevRank(key, member)
}

func (c *client) ZRevRangeWithScores(_ context.Context, key string, start, stop int64) *redis.ZSliceCmd {
	startTime := time.Now()
	defer c.recordStats(c.RedisClient, "z-rev-range-with-scores", startTime)
	return c.RedisClient.ZRevRangeWithScores(key, start, stop)
}

func (c *client) ZScore(_ context.Context, key, member string) *redis.FloatCmd {
	start := time.Now()
	defer c.recordStats(c.RedisClient, "z-score", start)
	return c.RedisClient.ZScore(key, member)
}

func (c *client) ZCount(_ context.Context, key, min string, max string) *redis.IntCmd {
	start := time.Now()
	defer c.recordStats(c.RedisClient, "z-count", start)
	return c.RedisClient.ZCount(key, min, max)
}

func (c *client) HGet(_ context.Context, key, field string) *redis.StringCmd {
	start := time.Now()
	defer c.recordStats(c.RedisClient, "h-get", start)
	return c.RedisClient.HGet(key, field)
}

func (c *client) HGetAll(_ context.Context, key string) *redis.StringStringMapCmd {
	start := time.Now()
	defer c.recordStats(c.RedisClient, "h-get-all", start)
	return c.RedisClient.HGetAll(key)
}

func (c *client) HSet(_ context.Context, key, field string, value interface{}) *redis.BoolCmd {
	start := time.Now()
	defer c.recordStats(c.RedisClient, "h-set", start)
	return c.RedisClient.HSet(key, field, value)
}

func (c *client) HMSet(_ context.Context, key string, fields map[string]interface{}) *redis.StatusCmd {
	start := time.Now()
	defer c.recordStats(c.RedisClient, "h-m-set", start)
	return c.RedisClient.HMSet(key, fields)
}

func (c *client) HDel(_ context.Context, key string, fields ...string) *redis.IntCmd {
	start := time.Now()
	defer c.recordStats(c.RedisClient, "h-del", start)
	return c.RedisClient.HDel(key, fields...)
}

// Redis.Get, as an example, gets called at 150k+ TPS. Each call to `recordStats` requires two time.Now calls,
// which has been a large source of our CPU time.
func (c *client) makeMaybeStatRecorder(client redis.Cmdable, op string, sampleRate float32) func() {
	if sampleRate > rand.Float32() {
		start := time.Now()
		return func() {
			c.recordStats(c.RedisClient, op, start)
		}
	}

	// else, noop
	return func() {}
}

func (c *client) recordStats(client redis.Cmdable, op string, start time.Time) {
	var stats *redis.PoolStats
	if c, ok := client.(*redis.Client); ok {
		stats = c.PoolStats()
	} else if c, ok := client.(*redis.ClusterClient); ok {
		stats = c.PoolStats()
	} else {
		stats = &redis.PoolStats{}
	}

	go func() {
		c.Statter.TimingDuration(fmt.Sprintf(operationDurationMetric, op), time.Since(start))
		c.Statter.Inc(fmt.Sprintf(operationMetric, op), 1)
		c.Statter.Gauge(connHitMetric, int64(stats.Hits))
		c.Statter.Gauge(connMissMetric, int64(stats.Misses))
		c.Statter.Gauge(connTimeoutMetric, int64(stats.Timeouts))
		c.Statter.Gauge(totalConnMetric, int64(stats.TotalConns))
		c.Statter.Gauge(idleConnMetric, int64(stats.IdleConns))
		c.Statter.Gauge(staleConnMetric, int64(stats.StaleConns))
	}()
}
