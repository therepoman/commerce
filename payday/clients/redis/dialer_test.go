package redis

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

var rolloutYaml = `percent: 3
`

func TestDialer(t *testing.T) {
	Convey("Can load sponsored campaigns files", t, func() {
		Convey("When File is empty", func() {
			rolloutStruct, err := loadRolloutYaml([]byte(""))
			So(err, ShouldBeNil)
			So(rolloutStruct.Percent, ShouldEqual, uint32(0))
		})
		Convey("Can load a valid file", func() {
			rolloutStruct, err := loadRolloutYaml([]byte(rolloutYaml))
			So(err, ShouldBeNil)
			So(rolloutStruct, ShouldNotBeEmpty)
			Convey("Has correct values", func() {
				So(rolloutStruct.Percent, ShouldEqual, uint32(3))
			})
		})
	})
}
