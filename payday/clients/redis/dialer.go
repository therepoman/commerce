package redis

import (
	"math/rand"
	"sync"
	"time"

	"code.justin.tv/chat/golibs/graceful"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/s3"
	"gopkg.in/yaml.v2"
)

const (
	refreshDelayMaxRandomOffset = time.Second * 30
	refreshDelay                = time.Minute * 1
)

type Dialer interface {
	GetPercentRollout() uint32
	Init() error
	RefreshPercentRolloutAtInterval()
}

func NewDialer(bucket, key string) Dialer {
	return &dialer{
		mutex:  &sync.RWMutex{},
		bucket: bucket,
		key:    key,
	}
}

type Rollout struct {
	Percent uint32 `yaml:"percent"`
}

type dialer struct {
	rollout uint32
	bucket  string
	key     string
	mutex   *sync.RWMutex

	S3Client s3.IS3Client `inject:""`
}

func (d *dialer) Init() error {
	if exists, err := d.S3Client.BucketExists(d.bucket); !exists || err != nil {
		log.WithError(err).WithField("bucket", d.bucket).Error("Bucket does not exist for percent rollout object creation")
		return err
	}

	if exists, err := d.S3Client.FileExists(d.bucket, d.key); !exists || err != nil {
		log.WithFields(log.Fields{
			"key":    d.key,
			"bucket": d.bucket,
		}).WithError(err).Error("Key does not exist in Bucket for percent rollout object creation")
		return err
	}

	err := d.refreshPercentRollout()
	if err != nil {
		log.WithError(err).Error("Error while refreshing percent rollout list")
		return err
	}

	return nil
}

func (d *dialer) GetPercentRollout() uint32 {
	return d.rollout
}

func loadRolloutYaml(bytes []byte) (*Rollout, error) {
	var rollout Rollout
	err := yaml.Unmarshal(bytes, &rollout)
	if err != nil {
		return nil, err
	}

	return &rollout, nil
}

func (d *dialer) refreshPercentRollout() error {
	fileBytes, err := d.S3Client.LoadFile(d.bucket, d.key)
	if err != nil {
		log.WithError(err).WithFields(log.Fields{
			"key":    d.key,
			"bucket": d.bucket,
		}).Error("could not load file from S3 for percent rollout")
		return err
	}

	rollout, err := loadRolloutYaml(fileBytes)
	if err != nil {
		log.WithError(err).WithFields(log.Fields{
			"key":    d.key,
			"bucket": d.bucket,
		}).Error("could not unmarshal file for percent rollout")
		return err
	}

	d.mutex.Lock()
	defer d.mutex.Unlock()

	d.rollout = rollout.Percent

	return nil
}

func (d *dialer) RefreshPercentRolloutAtInterval() {
	delayOffset := rand.Int63n(int64(refreshDelayMaxRandomOffset))
	timer := time.NewTicker(refreshDelay + time.Duration(delayOffset))

	for {
		select {
		case <-graceful.ShuttingDown():
			return
		case <-timer.C:
			err := d.refreshPercentRollout()
			if err != nil {
				log.WithError(err).Error("Error refreshing percent rollout")
			}
		}
	}
}
