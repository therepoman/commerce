package sandstorm

import (
	"sync"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/systems/sandstorm/manager"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/credentials/stscreds"
	"github.com/aws/aws-sdk-go/aws/endpoints"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sts"
)

var once = new(sync.Once)
var sandstormManager *manager.Manager
var secretCache map[string]*manager.Secret

func init() {
	secretCache = make(map[string]*manager.Secret)
}

type SecretGetter interface {
	Get(secretName string) (*manager.Secret, error)
}

type NoOpSecretGetter struct {
}

func (dsr *NoOpSecretGetter) Get(secretName string) (*manager.Secret, error) {
	return &manager.Secret{
		Plaintext: []byte("randomSecret"),
	}, nil
}

type CachedSecretGetter struct {
}

func (csg *CachedSecretGetter) Get(secretName string) (*manager.Secret, error) {
	cachedSecret, secretCached := secretCache[secretName]
	if secretCached {
		return cachedSecret, nil
	}

	secret, err := sandstormManager.Get(secretName)
	if err != nil {
		return nil, err
	}
	secretCache[secretName] = secret
	return secret, nil

}

func ensureSandstormManagerExists(role string) {
	once.Do(func() {
		if sandstormManager == nil {
			awsConfig := &aws.Config{
				Region:              aws.String("us-west-2"),
				STSRegionalEndpoint: endpoints.RegionalSTSEndpoint,
			}
			sess, err := session.NewSession(awsConfig)
			if err != nil {
				log.WithError(err).Panic("failed to make sandstorm manager session")
			}
			stsclient := sts.New(sess)

			arp := &stscreds.AssumeRoleProvider{
				Duration:     900 * time.Second,
				ExpiryWindow: 10 * time.Second,
				RoleARN:      role,
				Client:       stsclient,
			}

			creds := credentials.NewCredentials(arp)
			awsConfig.WithCredentials(creds)

			config := manager.Config{
				AWSConfig: awsConfig,
				TableName: "sandstorm-production",
				KeyID:     "alias/sandstorm-production",
			}

			sandstormManager = manager.New(config)
		}
	})
}

func New(role string) SecretGetter {
	ensureSandstormManagerExists(role)
	return &CachedSecretGetter{}
}
