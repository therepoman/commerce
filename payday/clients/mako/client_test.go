package mako

import (
	"context"
	"errors"
	"testing"

	rpc "code.justin.tv/commerce/mako/twirp"
	mako_dashboard "code.justin.tv/commerce/mako/twirp/dashboard"
	"code.justin.tv/commerce/payday/hystrix"
	mako_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/mako/twirp"
	mako_dashboard_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/mako/twirp/dashboard"
	metrics_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/metrics"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/models/api"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
	"github.com/twitchtv/twirp"
)

const (
	graham_tuid          = "127380717"
	qa_bits_partner_tuid = "116076154"
)

var tooSmart = rpc.Emoticon{
	Id:      "186224",
	Code:    "tooSmart",
	OwnerId: graham_tuid,
	State:   rpc.EmoteState_active,
	GroupId: "1",
}

var tooCool = rpc.Emoticon{
	Id:      "2065683",
	Code:    "tooCool",
	OwnerId: graham_tuid,
	State:   rpc.EmoteState_active,
	GroupId: "2",
}

var tooSpicy = rpc.Emoticon{
	Id:      "2065647",
	Code:    "tooSpicy",
	OwnerId: graham_tuid,
	State:   rpc.EmoteState_active,
	GroupId: "3",
}

func TestMakoClient_CreateEmoteGroupEntitlements(t *testing.T) {
	hystrix.ConfigureForTests()
	ctx := context.Background()

	userIDs := []string{graham_tuid, qa_bits_partner_tuid}
	channelID := models.QA_BITS_PARTNER_TUID

	groupIDs := []string{"1", "2", "3"}

	Convey("Given a client", t, func() {
		makoRPC := new(mako_mock.Mako)
		statter := new(metrics_mock.Statter)
		makoClient := makoClient{
			client:  makoRPC,
			Statter: statter,
		}

		statter.On("Inc", mock.Anything, int64(1)).Return()

		Convey("that is successful and the length of input matches given users and groups", func() {
			makoRPC.On("CreateEmoteGroupEntitlements", mock.Anything, mock.Anything).Return(&rpc.CreateEmoteGroupEntitlementsResponse{}, nil)

			err := makoClient.CreateEmoteGroupEntitlements(ctx, userIDs, channelID, groupIDs)

			So(err, ShouldBeNil)
			So(len(makoRPC.Calls[0].Arguments.Get(1).(*rpc.CreateEmoteGroupEntitlementsRequest).Entitlements), ShouldEqual, len(userIDs)*len(groupIDs))
		})

		Convey("that errors", func() {
			makoRPC.On("CreateEmoteGroupEntitlements", mock.Anything, mock.Anything).Return(nil, errors.New("test error"))

			err := makoClient.CreateEmoteGroupEntitlements(ctx, userIDs, channelID, groupIDs)

			So(err, ShouldNotBeNil)
		})
	})
}

func TestMakoClient_GetAllEmotesByOwnerId(t *testing.T) {
	hystrix.ConfigureForTests()
	ctx := context.Background()

	ownerID := graham_tuid

	filter := &rpc.EmoteFilter{
		Domain: rpc.Domain_emotes,
		States: []rpc.EmoteState{
			rpc.EmoteState_active,
		},
	}

	Convey("Given a client", t, func() {
		makoRPC := new(mako_mock.Mako)
		statter := new(metrics_mock.Statter)
		makoClient := makoClient{
			client:  makoRPC,
			Statter: statter,
		}

		statter.On("Inc", mock.Anything, int64(1)).Return()

		Convey("that is successful", func() {
			Convey("and returns only emoticons", func() {
				makoRPC.On("GetAllEmotesByOwnerId", mock.Anything, mock.Anything).Return(&rpc.GetAllEmotesByOwnerIdResponse{Emoticons: []*rpc.Emoticon{&tooSmart, &tooCool, &tooSpicy}}, nil)

				resp, err := makoClient.GetAllEmotesByOwnerId(ctx, ownerID, filter)

				So(err, ShouldBeNil)
				So(resp, ShouldContain, tooSmart)
				So(resp, ShouldContain, tooCool)
				So(resp, ShouldContain, tooSpicy)
			})

			Convey("and returns emotes and nils", func() {
				makoRPC.On("GetAllEmotesByOwnerId", mock.Anything, mock.Anything).Return(&rpc.GetAllEmotesByOwnerIdResponse{Emoticons: []*rpc.Emoticon{nil, &tooSmart, nil, &tooCool, &tooSpicy, nil}}, nil)

				resp, err := makoClient.GetAllEmotesByOwnerId(ctx, ownerID, filter)

				So(err, ShouldBeNil)
				So(resp, ShouldContain, tooSmart)
				So(resp, ShouldContain, tooCool)
				So(resp, ShouldContain, tooSpicy)
				So(len(resp), ShouldEqual, 3)
			})

			Convey("and returns no emotes", func() {
				makoRPC.On("GetAllEmotesByOwnerId", mock.Anything, mock.Anything).Return(&rpc.GetAllEmotesByOwnerIdResponse{Emoticons: []*rpc.Emoticon{}}, nil)

				resp, err := makoClient.GetAllEmotesByOwnerId(ctx, ownerID, filter)

				So(err, ShouldBeNil)
				So(len(resp), ShouldEqual, 0)
			})
		})

		Convey("that errors", func() {
			makoRPC.On("GetAllEmotesByOwnerId", mock.Anything, mock.Anything).Return(nil, errors.New("test error"))

			resp, err := makoClient.GetAllEmotesByOwnerId(ctx, ownerID, filter)

			So(err, ShouldNotBeNil)
			So(resp, ShouldBeNil)
		})
	})
}

func TestMakoClient_CreateEmote(t *testing.T) {
	hystrix.ConfigureForTests()
	ctx := context.Background()

	userID := graham_tuid
	codeSuffix := "Dumb"
	emoticonData := api.EmoticonUploadConfiguration{
		Code:       "tooDumb",
		CodeSuffix: codeSuffix,
		GroupID:    "4",
		ImageUploadConfig1X: api.ImageUploadConfig{
			ImageID: "77426418_28_bf3e9efe-a075-4331-bc4f-9d382cf8174",
		},
		ImageUploadConfig2X: api.ImageUploadConfig{
			ImageID: "77426418_56_881bd0e4-d770-4d91-862f-d89f2b76b420",
		},
		ImageUploadConfig4X: api.ImageUploadConfig{
			ImageID: "77426418_112_d55d9678-bfef-400b-af79-b754fa9866c3",
		},
	}

	Convey("Given a client", t, func() {
		makoDashboardRPC := new(mako_dashboard_mock.Mako)
		statter := new(metrics_mock.Statter)
		makoClient := makoClient{
			dashboardClient: makoDashboardRPC,
			Statter:         statter,
		}

		statter.On("Inc", mock.Anything, int64(1)).Return()

		Convey("that is successful", func() {
			createEmoteResp := mako_dashboard.CreateEmoteResponse{
				GroupIds: []string{"4"},
				State:    mako_dashboard.EmoteState_active,
				Id:       "300676569",
				Code:     "tooDumb",
			}

			req := &mako_dashboard.CreateEmoteRequest{
				UserId:     userID,
				Code:       emoticonData.Code,
				CodeSuffix: emoticonData.CodeSuffix,
				GroupIds:   []string{emoticonData.GroupID},
				ImageAssets: []*mako_dashboard.EmoteImageAsset{
					{
						ImageIds: &mako_dashboard.EmoteImageIds{
							Image1XId: emoticonData.ImageUploadConfig1X.ImageID,
							Image2XId: emoticonData.ImageUploadConfig2X.ImageID,
							Image4XId: emoticonData.ImageUploadConfig4X.ImageID,
						},
						AssetType: mako_dashboard.EmoteAssetType_static,
					},
				},
				AssetType:                 mako_dashboard.EmoteAssetType_static,
				Origin:                    mako_dashboard.EmoteOrigin_BitsBadgeTierEmotes,
				EnforceModerationWorkflow: true,
				ImageSource:               toMakoTwirpImageSource(api.UserImage),
			}
			makoDashboardRPC.On("CreateEmote", mock.Anything, mock.Anything).Return(&createEmoteResp, nil)

			resp, err := makoClient.CreateEmote(ctx, userID, emoticonData)

			So(err, ShouldBeNil)
			So(resp, ShouldResemble, createEmoteResp)
			makoDashboardRPC.AssertCalled(t, "CreateEmote", mock.Anything, req)
		})

		Convey("that is successful with default image", func() {
			createEmoteResp := mako_dashboard.CreateEmoteResponse{
				GroupIds: []string{"4"},
				State:    mako_dashboard.EmoteState_active,
				Id:       "300676569",
				Code:     "tooDumb",
			}
			emoticonData.ImageSource = api.DefaultLibraryImage

			req := &mako_dashboard.CreateEmoteRequest{
				UserId:     userID,
				Code:       emoticonData.Code,
				CodeSuffix: emoticonData.CodeSuffix,
				GroupIds:   []string{emoticonData.GroupID},
				ImageAssets: []*mako_dashboard.EmoteImageAsset{
					{
						ImageIds: &mako_dashboard.EmoteImageIds{
							Image1XId: emoticonData.ImageUploadConfig1X.ImageID,
							Image2XId: emoticonData.ImageUploadConfig2X.ImageID,
							Image4XId: emoticonData.ImageUploadConfig4X.ImageID,
						},
						AssetType: mako_dashboard.EmoteAssetType_static,
					},
				},
				AssetType:                 mako_dashboard.EmoteAssetType_static,
				Origin:                    mako_dashboard.EmoteOrigin_BitsBadgeTierEmotes,
				EnforceModerationWorkflow: true,
				ImageSource:               toMakoTwirpImageSource(api.DefaultLibraryImage),
			}

			makoDashboardRPC.On("CreateEmote", mock.Anything, mock.Anything).Return(&createEmoteResp, nil)

			resp, err := makoClient.CreateEmote(ctx, userID, emoticonData)

			So(err, ShouldBeNil)
			So(resp, ShouldResemble, createEmoteResp)
			makoDashboardRPC.AssertCalled(t, "CreateEmote", mock.Anything, req)
		})

		Convey("we should retry non-4xx errors", func() {
			makoErr := errors.New("test error")
			makoDashboardRPC.On("CreateEmote", mock.Anything, mock.Anything).Return(nil, makoErr)

			resp, err := makoClient.CreateEmote(ctx, userID, emoticonData)

			So(err, ShouldEqual, makoErr)
			So(resp, ShouldResemble, mako_dashboard.CreateEmoteResponse{})
			So(makoDashboardRPC.AssertNumberOfCalls(t, "CreateEmote", 4), ShouldBeTrue)
		})

		Convey("we should not retry 4xx errors", func() {
			makoErr := twirp.InvalidArgumentError("foobar", "missing foobar")
			makoDashboardRPC.On("CreateEmote", mock.Anything, mock.Anything).Return(nil, makoErr)

			resp, err := makoClient.CreateEmote(ctx, userID, emoticonData)

			So(err, ShouldEqual, makoErr) // Ensure that we are bubbling the original error, as upstream bifurcates on it
			So(resp, ShouldResemble, mako_dashboard.CreateEmoteResponse{})
			So(makoDashboardRPC.AssertNumberOfCalls(t, "CreateEmote", 1), ShouldBeTrue)
		})
	})
}

func TestMakoClient_DeleteEmoticon(t *testing.T) {
	hystrix.ConfigureForTests()
	ctx := context.Background()

	emoteID := "300676569"
	code := "tooDumb"

	Convey("Given a client", t, func() {
		makoRPC := new(mako_mock.Mako)
		statter := new(metrics_mock.Statter)
		makoClient := makoClient{
			client:  makoRPC,
			Statter: statter,
		}

		statter.On("Inc", mock.Anything, int64(1)).Return()

		Convey("that is successful", func() {
			makoRPC.On("DeleteEmoticon", mock.Anything, mock.Anything).Return(&rpc.DeleteEmoticonResponse{}, nil)

			err := makoClient.DeleteEmoticon(ctx, emoteID, code, true)

			So(err, ShouldBeNil)
		})

		Convey("that errors", func() {
			makoRPC.On("DeleteEmoticon", mock.Anything, mock.Anything).Return(&rpc.DeleteEmoticonResponse{}, errors.New("test error"))

			err := makoClient.DeleteEmoticon(ctx, emoteID, code, true)

			So(err, ShouldNotBeNil)
		})
	})
}

func TestMakoClient_CreateEmoteGroup(t *testing.T) {
	hystrix.ConfigureForTests()
	ctx := context.Background()

	Convey("Given a client", t, func() {
		makoRPC := new(mako_mock.Mako)
		statter := new(metrics_mock.Statter)
		makoClient := makoClient{
			client:  makoRPC,
			Statter: statter,
		}

		testGroupID := "1234567890"

		statter.On("Inc", mock.Anything, int64(1)).Return()

		Convey("that is successful then return group id reponse", func() {
			makoRPC.On("CreateEmoteGroup", mock.Anything, &rpc.CreateEmoteGroupRequest{
				Origin: rpc.EmoteGroup_BitsBadgeTierEmotes,
			}).Return(&rpc.CreateEmoteGroupResponse{
				GroupId: testGroupID,
			}, nil)

			groupID, err := makoClient.CreateEmoteGroup(ctx)

			So(err, ShouldBeNil)
			So(groupID, ShouldEqual, testGroupID)
		})

		Convey("that errors", func() {
			makoRPC.On("CreateEmoteGroup", mock.Anything, &rpc.CreateEmoteGroupRequest{
				Origin: rpc.EmoteGroup_BitsBadgeTierEmotes,
			}).Return(&rpc.CreateEmoteGroupResponse{}, errors.New("dummy mako error"))

			groupID, err := makoClient.CreateEmoteGroup(ctx)

			So(err, ShouldNotBeNil)
			So(err.Error(), ShouldContainSubstring, "dummy mako error")
			So(groupID, ShouldBeEmpty)
		})
	})
}

func TestMakoClient_GetPrefix(t *testing.T) {
	hystrix.ConfigureForTests()
	ctx := context.Background()

	Convey("Given a client", t, func() {
		makoRPC := new(mako_mock.Mako)
		statter := new(metrics_mock.Statter)
		makoClient := makoClient{
			client:  makoRPC,
			Statter: statter,
		}

		testChannelID := "1234567890"
		testPrefix := "wolf"

		statter.On("Inc", mock.Anything, int64(1)).Return()

		Convey("that is successful then return prefix in reponse", func() {
			makoRPC.On("GetPrefix", mock.Anything, &rpc.GetPrefixRequest{
				ChannelId: testChannelID,
			}).Return(&rpc.GetPrefixResponse{
				Prefix: testPrefix,
			}, nil)

			res, err := makoClient.GetPrefix(ctx, testChannelID)

			So(err, ShouldBeNil)
			So(res, ShouldEqual, testPrefix)
			makoRPC.AssertExpectations(t)
		})

		Convey("that errors", func() {
			makoRPC.On("GetPrefix", mock.Anything, mock.Anything).Return(&rpc.GetPrefixResponse{}, errors.New("dummy mako error"))

			res, err := makoClient.GetPrefix(ctx, testChannelID)

			So(err, ShouldNotBeNil)
			So(err.Error(), ShouldContainSubstring, "dummy mako error")
			So(res, ShouldBeEmpty)
			makoRPC.AssertExpectations(t)
		})
	})
}

func TestMakoClient_GetEmotesByGroups(t *testing.T) {
	hystrix.ConfigureForTests()
	ctx := context.Background()

	groupIDs := []string{"1", "2", "3"}

	var tooSmart = mako_dashboard.Emote{
		Id:      "186224",
		Code:    "tooSmart",
		OwnerId: graham_tuid,
		State:   mako_dashboard.EmoteState_active,
		GroupId: "1",
	}

	var tooCool = mako_dashboard.Emote{
		Id:      "2065683",
		Code:    "tooCool",
		OwnerId: graham_tuid,
		State:   mako_dashboard.EmoteState_active,
		GroupId: "2",
	}

	var tooSpicy = mako_dashboard.Emote{
		Id:      "2065647",
		Code:    "tooSpicy",
		OwnerId: graham_tuid,
		State:   mako_dashboard.EmoteState_active,
		GroupId: "3",
	}

	Convey("Given a client", t, func() {
		makoDashboardRPC := new(mako_dashboard_mock.Mako)
		statter := new(metrics_mock.Statter)
		makoClient := makoClient{
			dashboardClient: makoDashboardRPC,
			Statter:         statter,
		}

		statter.On("Inc", mock.Anything, int64(1)).Return()

		Convey("that succeeds", func() {
			Convey("returning only emote groups", func() {
				emoticonGroups := []*mako_dashboard.EmoteGroup{
					{
						Id:     "1",
						Emotes: []*mako_dashboard.Emote{&tooSmart},
					}, {
						Id:     "2",
						Emotes: []*mako_dashboard.Emote{&tooCool},
					}, {
						Id:     "3",
						Emotes: []*mako_dashboard.Emote{&tooSpicy},
					},
				}

				makoDashboardRPC.On("GetEmotesByGroups", mock.Anything, mock.Anything).Return(&mako_dashboard.GetEmotesByGroupsResponse{Groups: emoticonGroups}, nil)

				resp, err := makoClient.DashboardGetEmotesByGroups(ctx, groupIDs, []mako_dashboard.EmoteState{mako_dashboard.EmoteState_active})

				So(err, ShouldBeNil)
				So(resp[0].Emotes, ShouldContain, &tooSmart)
				So(resp[1].Emotes, ShouldContain, &tooCool)
				So(resp[2].Emotes, ShouldContain, &tooSpicy)
			})

			Convey("returning some emote groups and some nils", func() {
				emoticonGroups := []*mako_dashboard.EmoteGroup{
					nil,
					{
						Id:     "1",
						Emotes: []*mako_dashboard.Emote{&tooSmart},
					},
					nil,
					{
						Id:     "2",
						Emotes: []*mako_dashboard.Emote{&tooCool},
					}, {
						Id:     "3",
						Emotes: []*mako_dashboard.Emote{&tooSpicy},
					},
					nil,
				}

				makoDashboardRPC.On("GetEmotesByGroups", mock.Anything, mock.Anything).Return(&mako_dashboard.GetEmotesByGroupsResponse{Groups: emoticonGroups}, nil)

				resp, err := makoClient.DashboardGetEmotesByGroups(ctx, groupIDs, []mako_dashboard.EmoteState{mako_dashboard.EmoteState_active})

				So(err, ShouldBeNil)
				So(resp[0].Emotes, ShouldContain, &tooSmart)
				So(resp[1].Emotes, ShouldContain, &tooCool)
				So(resp[2].Emotes, ShouldContain, &tooSpicy)
				So(len(resp), ShouldEqual, 3)
			})

			Convey("returning no emote groups", func() {
				makoDashboardRPC.On("GetEmotesByGroups", mock.Anything, mock.Anything).Return(&mako_dashboard.GetEmotesByGroupsResponse{}, nil)

				resp, err := makoClient.DashboardGetEmotesByGroups(ctx, groupIDs, []mako_dashboard.EmoteState{mako_dashboard.EmoteState_active})

				So(err, ShouldBeNil)
				So(len(resp), ShouldEqual, 0)
			})
		})

		Convey("that errors", func() {
			makoDashboardRPC.On("GetEmotesByGroups", mock.Anything, mock.Anything).Return(nil, errors.New("test error"))

			resp, err := makoClient.DashboardGetEmotesByGroups(ctx, groupIDs, []mako_dashboard.EmoteState{mako_dashboard.EmoteState_active})

			So(err, ShouldNotBeNil)
			So(resp, ShouldBeNil)
		})
	})
}

func TestMakoClient_AssignEmoteToGroup(t *testing.T) {
	hystrix.ConfigureForTests()
	ctx := context.Background()

	userID := graham_tuid
	emoteID := "someemoteid"
	groupID := "somegroupid"

	Convey("Given a client", t, func() {
		makoDashboardRPC := new(mako_dashboard_mock.Mako)
		statter := new(metrics_mock.Statter)
		makoClient := makoClient{
			dashboardClient: makoDashboardRPC,
			Statter:         statter,
		}

		statter.On("Inc", mock.Anything, int64(1)).Return()

		Convey("that is successful", func() {
			assignResp := &mako_dashboard.AssignEmoteToGroupResponse{
				Emote: &mako_dashboard.Emote{
					Id: emoteID,
				},
			}

			makoDashboardRPC.On("AssignEmoteToGroup", mock.Anything, mock.Anything).Return(assignResp, nil)

			resp, err := makoClient.AssignEmoteToGroup(ctx, userID, emoteID, groupID)

			So(err, ShouldBeNil)
			So(resp, ShouldResemble, *assignResp.Emote)
		})

		Convey("that errors", func() {
			makoDashboardRPC.On("AssignEmoteToGroup", mock.Anything, mock.Anything).Return(nil, errors.New("test error"))

			resp, err := makoClient.AssignEmoteToGroup(ctx, userID, emoteID, groupID)

			So(err, ShouldNotBeNil)
			So(resp, ShouldResemble, mako_dashboard.Emote{})
		})
	})
}
