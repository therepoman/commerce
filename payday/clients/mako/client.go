package mako

import (
	"context"
	"errors"
	"net/http"
	"time"

	log "code.justin.tv/commerce/logrus"
	rpc "code.justin.tv/commerce/mako/twirp"
	mako_dashboard "code.justin.tv/commerce/mako/twirp/dashboard"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/hystrix"
	cmds "code.justin.tv/commerce/payday/hystrix"
	"code.justin.tv/commerce/payday/metrics"
	"code.justin.tv/commerce/payday/models/api"
	"code.justin.tv/commerce/payday/utils/strings"
	"code.justin.tv/sse/malachai/pkg/s2s/caller"
	"github.com/cenkalti/backoff"
	"github.com/golang/protobuf/ptypes"
	"github.com/twitchtv/twirp"
)

const (
	BitsBadgeTierEmoteRewardsOrigin                             = "Bits-Badge-Tier-Emote-Rewards"
	MAKO_FAILURE_METRIC                                         = "Mako_Failure"
	MAKO_CREATE_EMOTE_GROUP_ENTITLEMENTS_FAILURE_METRIC         = "Mako_Failure_Create_Emote_Group_Entitlements"
	MAKO_GET_ALL_EMOTES_BY_OWNER_ID_FAILURE_METRIC              = "Mako_Failure_Get_All_Emotes_By_Owner_Id"
	MAKO_GET_EMOTICONS_BY_GROUPS_FAILURE_METRIC                 = "Mako_Failure_Get_Emoticons_By_Groups"
	MAKO_DASHBOARD_GET_EMOTES_BY_GROUPS_FAILURE_METRIC          = "Mako_Failure_Dashboard_Get_Emotes_By_Groups"
	MAKO_CREATE_EMOTE_FAILURE_METRIC                            = "Mako_Failure_Create_Emote"
	MAKO_DELETE_EMOTICON_FAILURE_METRIC                         = "Mako_Failure_Delete_Emoticon"
	MAKO_ARE_THESE_EMOTE_CODES_UNIQUE_FAILURE_METRIC            = "Mako_Failure_Are_These_Emote_Codes_Unique"
	MAKO_BATCH_VALIDATE_EMOTE_CODE_ACCEPTABILITY_FAILURE_METRIC = "Mako_Failure_Batch_Validate_Emote_Code_Acceptability"
	MAKO_CREATE_EMOTE_GROUP_FAILURE_METRIC                      = "Mako_Failure_Create_Emote_Group"
	MAKO_GET_PREFIX_FAILURE_METRIC                              = "Mako_Failure_Get_Prefix"
	MAKO_ASSIGN_EMOTE_TO_GROUP_FAILURE_METRIC                   = "Mako_Failure_Assign_Emote_To_Group"
	MAKO_CREATE_EMOTE_RETRYABLE_ERROR_METRIC                    = "Mako_Retryable_Error_Create_Emote"
)

type MakoClient interface {
	CreateEmoteGroupEntitlements(ctx context.Context, userIDs []string, channelID string, groupIDs []string) error
	GetAllEmotesByOwnerId(ctx context.Context, ownerID string, filter *rpc.EmoteFilter) ([]rpc.Emoticon, error)
	GetEmoticonsByGroups(ctx context.Context, groupIDs []string, allowedStates []rpc.EmoteState) ([]rpc.EmoticonGroup, error)
	DeleteEmoticon(ctx context.Context, emoteID string, code string, deleteImages bool) error
	CreateEmoteGroup(ctx context.Context) (string, error)
	GetPrefix(ctx context.Context, channelID string) (string, error)

	// Dashboard APIs (uncached)
	CreateEmote(ctx context.Context, userID string, emoticonUploadConfig api.EmoticonUploadConfiguration) (mako_dashboard.CreateEmoteResponse, error)
	DashboardGetEmotesByGroups(ctx context.Context, groupIDs []string, allowedStates []mako_dashboard.EmoteState) ([]mako_dashboard.EmoteGroup, error)
	AssignEmoteToGroup(ctx context.Context, userID string, emoteID string, groupID string) (mako_dashboard.Emote, error)
}

type makoClient struct {
	client          rpc.Mako
	dashboardClient mako_dashboard.Mako
	snsTopic        string
	Statter         metrics.Statter `inject:""`
}

func NewClient(cfg config.Configuration) MakoClient {
	var httpClient *http.Client

	if cfg.S2S.IsMakoEnabled() {
		rt, err := caller.NewRoundTripper(cfg.S2S.Name, &caller.Config{}, log.New())
		if err != nil {
			log.Fatalf("Failed to create s2s round tripper: %v", err)
		}

		httpClient = &http.Client{
			Transport: rt,
		}
	} else {
		httpClient = http.DefaultClient
	}

	snsTopic := cfg.EmoticonUploadCallbackSNSTopic

	return &makoClient{
		client:          rpc.NewMakoProtobufClient(cfg.MakoURL, httpClient),
		dashboardClient: mako_dashboard.NewMakoProtobufClient(cfg.MakoURL, http.DefaultClient),
		snsTopic:        snsTopic,
	}
}

func (m *makoClient) CreateEmoteGroupEntitlements(ctx context.Context, userIDs []string, channelID string, groupIDs []string) error {
	f := func() error {
		return hystrix.Do(cmds.MakoCreateEmoteGroupEntitlements, func() error {
			entitlements := make([]*rpc.EmoteGroupEntitlement, 0, len(userIDs)*len(groupIDs))

			start := ptypes.TimestampNow()

			for _, groupID := range groupIDs {
				for _, userID := range userIDs {
					entitlement := &rpc.EmoteGroupEntitlement{
						OwnerId:  userID,
						ItemId:   channelID,
						GroupId:  groupID,
						OriginId: BitsBadgeTierEmoteRewardsOrigin,
						Group:    rpc.EmoteGroup_BitsBadgeTierEmotes,
						Start:    start,
						End: &rpc.InfiniteTime{
							IsInfinite: true, //all Bits badge tier emote group entitlements are forever
						},
					}
					entitlements = append(entitlements, entitlement)
				}
			}

			createEmoteGroupEntitlementsRequest := &rpc.CreateEmoteGroupEntitlementsRequest{
				Entitlements: entitlements,
			}

			_, err := m.client.CreateEmoteGroupEntitlements(ctx, createEmoteGroupEntitlementsRequest)

			return err
		}, nil)
	}

	b := backoff.WithMaxTries(backoff.WithContext(newBackoff(), ctx), 3)
	err := backoff.Retry(f, b)
	if err != nil {
		go m.incrementFailureMetrics(MAKO_CREATE_EMOTE_GROUP_ENTITLEMENTS_FAILURE_METRIC)
	}
	return err
}

func (m *makoClient) GetAllEmotesByOwnerId(ctx context.Context, ownerID string, filter *rpc.EmoteFilter) ([]rpc.Emoticon, error) {
	var emoticons []rpc.Emoticon

	err := hystrix.Do(cmds.MakoGetAllEmotesByOwnerId, func() error {
		getAllEmotesByOwnerIDRequest := &rpc.GetAllEmotesByOwnerIdRequest{
			OwnerId: ownerID,
			Filter:  filter,
		}

		resp, err := m.client.GetAllEmotesByOwnerId(ctx, getAllEmotesByOwnerIDRequest)
		if err != nil {
			log.WithError(err).WithField("ownerID", ownerID).Warn("Could not get all emotes for ownerID")
			return err
		}

		for _, emoticon := range resp.Emoticons {
			if emoticon != nil {
				emoticons = append(emoticons, *emoticon)
			}
		}

		return nil
	}, nil)

	if err != nil {
		go m.incrementFailureMetrics(MAKO_GET_ALL_EMOTES_BY_OWNER_ID_FAILURE_METRIC)
		return nil, err
	}
	return emoticons, nil
}

func (m *makoClient) GetEmoticonsByGroups(ctx context.Context, groupIDs []string, allowedStates []rpc.EmoteState) ([]rpc.EmoticonGroup, error) {
	var emoticonGroups []rpc.EmoticonGroup

	err := hystrix.Do(cmds.MakoGetEmoticonsByGroups, func() error {
		getEmoticonsByGroupsRequest := &rpc.GetEmoticonsByGroupsRequest{
			EmoticonGroupKeys: groupIDs,
			Filter: &rpc.EmoteFilter{
				Domain: rpc.Domain_emotes,
				States: allowedStates,
			},
		}

		resp, err := m.client.GetEmoticonsByGroups(ctx, getEmoticonsByGroupsRequest)
		if err != nil {
			log.WithError(err).WithField("groupIDs", groupIDs).Warn("Could not get emoticons for groupIDs")
			return err
		}

		for _, emoticonGroup := range resp.Groups {
			if emoticonGroup != nil {
				emoticonGroups = append(emoticonGroups, *emoticonGroup)
			}
		}

		return nil
	}, nil)

	if err != nil {
		go m.incrementFailureMetrics(MAKO_GET_EMOTICONS_BY_GROUPS_FAILURE_METRIC)
		return nil, err
	}
	return emoticonGroups, nil
}

func (m *makoClient) GetEmoteUploadConfig(ctx context.Context, userID string) (*mako_dashboard.GetEmoteUploadConfigResponse, error) {
	getEmoteUploadConfigRequest := &mako_dashboard.GetEmoteUploadConfigRequest{
		UserId:     userID,
		ResizePlan: mako_dashboard.ResizePlan_no_resize,
		Sizes:      []mako_dashboard.EmoteImageSize{mako_dashboard.EmoteImageSize_size_1x, mako_dashboard.EmoteImageSize_size_2x, mako_dashboard.EmoteImageSize_size_4x},
		AssetType:  mako_dashboard.EmoteAssetType_static,
	}

	resp, err := m.dashboardClient.GetEmoteUploadConfig(ctx, getEmoteUploadConfigRequest)
	if resp == nil || err != nil {
		log.WithError(err).WithFields(log.Fields{
			"userID": userID,
		}).Warn("Could not get emote upload config")
		return nil, err
	}

	return resp, nil
}

func (m *makoClient) CreateEmote(ctx context.Context, userID string, emoticonUploadConfig api.EmoticonUploadConfiguration) (mako_dashboard.CreateEmoteResponse, error) {
	l := log.WithFields(log.Fields{
		"userID":      userID,
		"code":        emoticonUploadConfig.Code,
		"codeSuffix":  emoticonUploadConfig.CodeSuffix,
		"groupID":     emoticonUploadConfig.GroupID,
		"imageData1X": emoticonUploadConfig.ImageUploadConfig1X,
		"imageData2X": emoticonUploadConfig.ImageUploadConfig2X,
		"imageData4X": emoticonUploadConfig.ImageUploadConfig4X,
		"imageSource": emoticonUploadConfig.ImageSource,
	})

	var createEmoteResponse = mako_dashboard.CreateEmoteResponse{}

	f := func() error {
		return hystrix.Do(cmds.MakoCreateEmote, func() error {
			createEmoteRequest := &mako_dashboard.CreateEmoteRequest{
				UserId:     userID,
				Code:       emoticonUploadConfig.Code,
				CodeSuffix: emoticonUploadConfig.CodeSuffix,
				GroupIds:   []string{emoticonUploadConfig.GroupID},
				ImageAssets: []*mako_dashboard.EmoteImageAsset{
					{
						ImageIds: &mako_dashboard.EmoteImageIds{
							Image1XId: emoticonUploadConfig.ImageUploadConfig1X.ImageID,
							Image2XId: emoticonUploadConfig.ImageUploadConfig2X.ImageID,
							Image4XId: emoticonUploadConfig.ImageUploadConfig4X.ImageID,
						},
						AssetType: mako_dashboard.EmoteAssetType_static,
					},
				},
				AssetType:                 mako_dashboard.EmoteAssetType_static,
				Origin:                    mako_dashboard.EmoteOrigin_BitsBadgeTierEmotes,
				EnforceModerationWorkflow: true,
				ImageSource:               toMakoTwirpImageSource(emoticonUploadConfig.ImageSource),
			}

			resp, err := m.dashboardClient.CreateEmote(ctx, createEmoteRequest)

			if err != nil {
				// Do not retry 4xx errors
				// TODO: The value retries are providing in this flow is unclear. Once we have data on the retryable error
				//       rate, evaluate if we can remove them.
				if twirpError, ok := err.(twirp.Error); ok && twirpError.Code() == twirp.InvalidArgument {
					return backoff.Permanent(err)
				}

				go m.incrementFailureMetrics(MAKO_CREATE_EMOTE_RETRYABLE_ERROR_METRIC)
				l.WithError(err).Warn("error creating emote")
				return err
			}

			if resp == nil {
				message := "could not create emote, received a nil response from Mako"
				l.Warn(message)
				return errors.New(message)
			}

			createEmoteResponse = *resp

			return nil
		}, nil)
	}

	b := backoff.WithMaxTries(backoff.WithContext(newBackoff(), ctx), 3)
	err := backoff.Retry(f, b)
	if err != nil {
		go m.incrementFailureMetrics(MAKO_CREATE_EMOTE_FAILURE_METRIC)
		return mako_dashboard.CreateEmoteResponse{}, err
	}
	return createEmoteResponse, nil
}

func (m *makoClient) DeleteEmoticon(ctx context.Context, emoteID string, code string, deleteImages bool) error {
	err := hystrix.Do(cmds.MakoDeleteEmoticon, func() error {
		deleteEmoticonRequest := &rpc.DeleteEmoticonRequest{
			Id:           emoteID,
			Code:         code,
			DeleteImages: deleteImages,
			EmoteGroup:   rpc.EmoteGroup_BitsBadgeTierEmotes,
		}

		_, err := m.client.DeleteEmoticon(ctx, deleteEmoticonRequest)

		return err
	}, nil)

	if err != nil {
		go m.incrementFailureMetrics(MAKO_DELETE_EMOTICON_FAILURE_METRIC)
		return err
	}
	return nil
}

func (m *makoClient) BatchValidateCodeUniqueness(ctx context.Context, emoteCodes []string, states []rpc.EmoteState) (map[string]bool, error) {
	var isUnique map[string]bool

	err := hystrix.Do(cmds.MakoBatchValidateCodeUniqueness, func() error {

		areTheseEmoteCodesUniqueRequest := &rpc.BatchValidateCodeUniquenessRequest{
			EmoteCodes: emoteCodes,
			Filter: &rpc.EmoteFilter{
				Domain: rpc.Domain_emotes,
				States: states,
			},
		}

		resp, err := m.client.BatchValidateCodeUniqueness(ctx, areTheseEmoteCodesUniqueRequest)

		if resp != nil {
			isUnique = resp.IsUnique
		}

		return err
	}, nil)

	if err != nil {
		go m.incrementFailureMetrics(MAKO_ARE_THESE_EMOTE_CODES_UNIQUE_FAILURE_METRIC)
		return nil, err
	}
	return isUnique, nil
}

func (m *makoClient) BatchValidateEmoteCodeAcceptability(ctx context.Context, inputs []api.EmoteCodeAndSuffix, userID string) ([]*rpc.ValidateEmoteCodeAcceptabilityResult, error) {
	var results []*rpc.ValidateEmoteCodeAcceptabilityResult

	err := hystrix.Do(cmds.MakoBatchValidateEmoteCodeAcceptability, func() error {
		acceptabilityInputs := make([]*rpc.ValidateEmoteCodeAcceptabilityInput, 0, len(inputs))
		for _, input := range inputs {
			acceptabilityInputs = append(acceptabilityInputs, &rpc.ValidateEmoteCodeAcceptabilityInput{
				EmoteCode:       input.EmoteCode,
				EmoteCodeSuffix: input.EmoteCodeSuffix,
			})
		}

		resp, err := m.client.BatchValidateEmoteCodeAcceptability(ctx, &rpc.BatchValidateEmoteCodeAcceptabilityRequest{
			Requests: acceptabilityInputs,
			UserId:   userID,
		})

		if resp != nil {
			results = resp.GetResults()
		}

		return err
	}, nil)

	if err != nil {
		go m.incrementFailureMetrics(MAKO_BATCH_VALIDATE_EMOTE_CODE_ACCEPTABILITY_FAILURE_METRIC)
		return nil, err
	}
	return results, nil
}

func (m *makoClient) CreateEmoteGroup(ctx context.Context) (string, error) {
	var groupID string

	err := hystrix.Do(cmds.MakoCreateEmoteGroup, func() error {
		resp, err := m.client.CreateEmoteGroup(ctx, &rpc.CreateEmoteGroupRequest{
			Origin: rpc.EmoteGroup_BitsBadgeTierEmotes,
		})

		if resp != nil && strings.NotBlank(resp.GroupId) {
			groupID = resp.GroupId
		}

		return err
	}, nil)

	if err != nil {
		go m.incrementFailureMetrics(MAKO_CREATE_EMOTE_GROUP_FAILURE_METRIC)
		return "", err
	}
	return groupID, nil
}

func (m *makoClient) GetPrefix(ctx context.Context, channelID string) (string, error) {
	var prefix string

	err := hystrix.Do(cmds.MakoGetPrefix, func() error {
		resp, err := m.client.GetPrefix(ctx, &rpc.GetPrefixRequest{
			ChannelId: channelID,
		})

		if resp != nil && strings.NotBlank(resp.GetPrefix()) {
			prefix = resp.GetPrefix()
		}

		return err
	}, nil)

	if err != nil {
		go m.incrementFailureMetrics(MAKO_GET_PREFIX_FAILURE_METRIC)
		return "", err
	}
	return prefix, nil
}

func (m *makoClient) DashboardGetEmotesByGroups(ctx context.Context, groupIDs []string, allowedStates []mako_dashboard.EmoteState) ([]mako_dashboard.EmoteGroup, error) {
	var emoticonGroups []mako_dashboard.EmoteGroup

	err := hystrix.Do(cmds.MakoDashboardGetEmotesByGroups, func() error {
		getEmoticonsByGroupsRequest := &mako_dashboard.GetEmotesByGroupsRequest{
			EmoteGroupIds: groupIDs,
			Filter: &mako_dashboard.EmoteFilter{
				Domain: mako_dashboard.Domain_emotes,
				States: allowedStates,
			},
		}

		resp, err := m.dashboardClient.GetEmotesByGroups(ctx, getEmoticonsByGroupsRequest)
		if err != nil {
			log.WithError(err).WithField("groupIDs", groupIDs).Warn("Could not get emoticons for groupIDs from GetEmotesByGroups")
			return err
		}

		for _, emoticonGroup := range resp.Groups {
			if emoticonGroup != nil {
				emoticonGroups = append(emoticonGroups, *emoticonGroup)
			}
		}

		return nil
	}, nil)

	if err != nil {
		go m.incrementFailureMetrics(MAKO_DASHBOARD_GET_EMOTES_BY_GROUPS_FAILURE_METRIC)
		return nil, err
	}
	return emoticonGroups, nil
}

func (m *makoClient) AssignEmoteToGroup(ctx context.Context, userID string, emoteID string, groupID string) (mako_dashboard.Emote, error) {
	var updatedEmote mako_dashboard.Emote

	err := hystrix.Do(cmds.MakoAssignEmoteToGroup, func() error {
		resp, err := m.dashboardClient.AssignEmoteToGroup(ctx, &mako_dashboard.AssignEmoteToGroupRequest{
			RequestingUserId: userID,
			EmoteId:          emoteID,
			GroupId:          groupID,
			Origin:           mako_dashboard.EmoteOrigin_BitsBadgeTierEmotes,
		})

		if resp != nil && resp.GetEmote() != nil {
			updatedEmote = *resp.GetEmote()
		}

		return err
	}, nil)

	if err != nil {
		go m.incrementFailureMetrics(MAKO_ASSIGN_EMOTE_TO_GROUP_FAILURE_METRIC)
		return mako_dashboard.Emote{}, err
	}
	return updatedEmote, nil
}

func (m *makoClient) incrementFailureMetrics(metricName string) {
	m.Statter.Inc(metricName, 1)
	m.Statter.Inc(MAKO_FAILURE_METRIC, 1)
}

func newBackoff() backoff.BackOff {
	exponential := backoff.NewExponentialBackOff()
	exponential.InitialInterval = 10 * time.Millisecond
	exponential.MaxElapsedTime = 1 * time.Second
	exponential.Multiplier = 2
	return exponential
}

func toMakoTwirpImageSource(imageSource api.ImageSource) mako_dashboard.ImageSource {
	if imageSource == api.DefaultLibraryImage {
		return mako_dashboard.ImageSource_DefaultLibraryImage
	}

	return mako_dashboard.ImageSource_UserImage
}
