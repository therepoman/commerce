package slack

import (
	"bytes"
	"context"
	"encoding/json"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/httputils"
)

type ISlacker interface {
	Post(webHookUrl string, msg SlackMsg) error
	PostMsg(webHookUrl string, msgBody string) error
}

type Slacker struct {
	HttpClient httputils.HttpUtilClient
	WebHookUrl string
}

func NewSlacker() ISlacker {
	return &Slacker{
		HttpClient: httputils.NewDefaultHttpUtilClientApi(),
	}
}

type SlackMsg struct {
	Text        string            `json:"text"`
	UnfurlLinks bool              `json:"unfurl_links"`
	Parse       string            `json:"parse"`
	Attachments []SlackAttachment `json:"attachments"`
}

type SlackAttachment struct {
	Fallback string `json:"fallback"`
	Pretext  string `json:"pretext"`
	ImageURL string `json:"image_url"`
}

func (s *Slacker) Post(webHookUrl string, msg SlackMsg) error {
	url := webHookUrl
	requestJson, err := json.Marshal(&msg)
	if err != nil {
		log.WithError(err).Error("Could not marshal json for slack message")
		return err
	}

	requestJsonReader := bytes.NewReader(requestJson)

	resp, statusCode, err := s.HttpClient.HttpPost(context.Background(), url, requestJsonReader)

	// ToDo: Remove once we are done testing custom badge slack messages
	if msg.Attachments != nil && len(msg.Attachments) > 0 {
		stringResp := string(resp)
		message := msg.Attachments[0].Pretext
		fields := log.Fields{
			"url":         url,
			"status_code": statusCode,
			"resp":        stringResp,
			"message":     message,
		}
		log.WithFields(fields).Info("slack webhook called")
	}

	if err != nil {
		log.WithField("url", url).WithError(err).Error("Could not send message to slack webhook")
		return err
	}
	return nil
}

func (s *Slacker) PostMsg(webHookUrl string, msgBody string) error {
	msg := SlackMsg{
		Text:        msgBody,
		UnfurlLinks: true,
		Parse:       "full",
	}
	return s.Post(webHookUrl, msg)
}
