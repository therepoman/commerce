# telemetry

telemetry houses shared code for our backend services to send metrics to cloudwatch

## How to Use

add to your project using

    import telemetry "code.justin.tv/safety/telemetry"
    import twitchtelemetry "code.justin.tv/amzn/TwitchTelemetry"

configure handlers.go construtor to support the addition of a telemetry reporter

    func New(router *goji.Mux, config Config, telemetryReporter twitchtelemetry.SampleReporter) http.Handler

and configure to add the reporter as middleware to twirp hooks (with telemetry.ConfigureTelemetryMiddleware)

    twirpHooks := twirp.ChainHooks(
        multistatter.NewStatsdServerHooks(twitchconfig.Statsd((),
        telemetry.ConfigureTelemetryMiddleware(telemetryReporter).ServerHooks(),
        newErrorLogServerHooks(),
    )

create a twitch telemetry reporter (with telemetry.ConfigureTelemetry) using a unique [twitch process identifier](https://git-aws.internal.justin.tv/amzn/TwitchProcessIdentifier/blob/mainline/process_identifier.go) in main.go
if there is any custom configuration (custom flush period, buffer size, or time resolution), pass it in as a [reporterconfig](https://git-aws.internal.justin.tv/safety/telemetry/blob/master/internal/reporter_config.go) struct

    telemetryReporter := twlemetry.ConfigureTelemetry(pid, reporterconfig)

add the created sample reporter to handlers using the handlers.go constructor

    handlers := handlers.New(twitchserver.NewServer(), config, telemetryReporter)

to use the reporter to report metrics, refer to: https://docs.fulton.twitch.a2z.com/docs/metrics.html#how-to-report-additional-metrics
