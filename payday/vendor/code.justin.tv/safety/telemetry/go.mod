module code.justin.tv/safety/telemetry

go 1.15

require (
	code.justin.tv/amzn/TwitchLogging v0.0.0-20190731182733-d8aae132db1f // indirect
	code.justin.tv/amzn/TwitchProcessIdentifier v0.0.0-20191004180637-dc817f563e55
	code.justin.tv/amzn/TwitchTelemetry v0.0.0-20191113205906-40e3e1aa55c5
	code.justin.tv/amzn/TwitchTelemetryCloudWatchMetricsSender v0.0.0-20190822201853-9acf2b6ccaa1
	code.justin.tv/amzn/TwitchTelemetryMetricsMiddleware v0.0.0-20190731182748-cb62e9370b7f
	code.justin.tv/amzn/TwitchTelemetryPollingCollector v0.0.0-20190717173130-38476c0d862a
	code.justin.tv/amzn/TwitchTelemetrySystemMetricsCollector v0.0.0-20190313014904-21edc3c40628
	code.justin.tv/video/metrics-middleware/v2 v2.0.0
	github.com/StackExchange/wmi v0.0.0-20190523213315-cbe66965904d // indirect
	github.com/aws/aws-sdk-go v1.28.7 // indirect
	github.com/cep21/circuit v1.1.1
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/go-ole/go-ole v1.2.4 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/shirou/gopsutil v2.19.12+incompatible // indirect
	github.com/smartystreets/goconvey v1.6.4 // indirect
	github.com/stretchr/testify v1.4.0
	github.com/twitchtv/twirp v5.10.0+incompatible // indirect
	golang.org/x/net v0.0.0-20201110031124-69a78807bb2b // indirect
	gopkg.in/yaml.v2 v2.2.8 // indirect
)
