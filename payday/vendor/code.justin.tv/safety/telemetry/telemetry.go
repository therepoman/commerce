package telemetry

import (
	"time"

	identifier "code.justin.tv/amzn/TwitchProcessIdentifier"
	twitchtelemetry "code.justin.tv/amzn/TwitchTelemetry"
	cw "code.justin.tv/amzn/TwitchTelemetryCloudWatchMetricsSender"
	metricsmiddleware "code.justin.tv/amzn/TwitchTelemetryMetricsMiddleware"
	poller "code.justin.tv/amzn/TwitchTelemetryPollingCollector"
	agent "code.justin.tv/amzn/TwitchTelemetrySystemMetricsCollector"
	"code.justin.tv/video/metrics-middleware/v2/operation"
	"code.justin.tv/video/metrics-middleware/v2/twirpmetric"
)

// ConfigureTelemetry sets up a new reporter using telemetry
func ConfigureTelemetry(pid *identifier.ProcessIdentifier, config *ReporterConfig) twitchtelemetry.SampleReporter {
	if config == nil {
		// The default values passed as config when none is given
		config = &ReporterConfig{
			Interval: 30 * time.Second,
			Size:     100000,
			Period:   time.Minute,
		}
	}

	sender := cw.NewUnbuffered(pid, nil)
	sampleObserver := twitchtelemetry.NewBufferedAggregator(config.Interval, config.Size, config.Period, sender, nil)
	sampleReporter := twitchtelemetry.SampleReporter{
		SampleBuilder:  twitchtelemetry.SampleBuilder{ProcessIdentifier: *pid},
		SampleObserver: sampleObserver,
	}

	sampleReporter.Dimensions = twitchtelemetry.DimensionSet{}

	goPoller := poller.NewGoStatsPollingCollector(10*time.Second, &sampleReporter.SampleBuilder, sampleReporter.SampleObserver, nil)
	goPoller.Start()

	sysMetricsConfig := agent.SystemMetricsCollectorConfig{
		Collectors: []agent.SystemMetricsCollector{agent.CPUCollector, agent.NetCollector, agent.MemCollector, agent.LoadCollector},
	}
	sysMetricsPoller := agent.NewSystemMetricsPollingCollector(sysMetricsConfig, 10*time.Second, pid, sampleReporter.SampleObserver, nil)
	sysMetricsPoller.Start()

	return sampleReporter
}

// WithCustomDimensions returns a new sample reporter with new custom dimensions
func WithCustomDimensions(sampleReporter twitchtelemetry.SampleReporter, dimensions twitchtelemetry.DimensionSet) twitchtelemetry.SampleReporter {
	return twitchtelemetry.SampleReporter{
		SampleBuilder:  twitchtelemetry.SampleBuilder{ProcessIdentifier: sampleReporter.ProcessIdentifier, Dimensions: mergeDimensions(sampleReporter.SampleBuilder.Dimensions, dimensions)},
		SampleObserver: sampleReporter.SampleObserver,
	}
}

// ConfigureTelemetryMiddleware sets up middleware using created telemetry reporter
func ConfigureTelemetryMiddleware(sampleReporter twitchtelemetry.SampleReporter) *twirpmetric.Server {
	// aggregator takes care of flushing
	metricsOpMonitor := &metricsmiddleware.OperationMonitor{
		SampleReporter: sampleReporter,
		AutoFlush:      false,
	}
	opStarter := &operation.Starter{OpMonitors: []operation.OpMonitor{metricsOpMonitor}}
	metricsMiddleware := &twirpmetric.Server{Starter: opStarter}

	return metricsMiddleware
}

// mergeDimensions merges two dimension sets and return the union of the two.
// If a dimension exist in both old and new, value in new will be used
func mergeDimensions(old, new twitchtelemetry.DimensionSet) twitchtelemetry.DimensionSet {
	result := twitchtelemetry.DimensionSet{}
	for k, v := range old {
		result[k] = v
	}

	for k, v := range new {
		result[k] = v
	}

	return result
}
