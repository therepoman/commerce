package util

func DedupeSlice(l []string) []string {
	dedupedSlice := make([]string, 0)
	deduper := make(map[string]bool)
	for _, str := range l {
		if str != "" && !deduper[str] {
			deduper[str] = true
			dedupedSlice = append(dedupedSlice, str)
		}
	}
	return dedupedSlice
}
