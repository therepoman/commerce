package oauth2

import (
	"errors"
	"time"

	"code.justin.tv/web/owl/internal/util"
)

// Token is another view into an Authorization for endpoints concerned
// with the actual token values
type Token struct {
	AuthorizationID string
	TokenCleartext  string
	TokenHash       string
	Scope           []string
	ClientID        string
	ClientRowID     string
	UserID          string
	SessionID       string
	ExpiresAt       time.Time
	CreatedAt       time.Time
	UpdatedAt       time.Time
	Nonce           string
	OIDCRequired    bool
}

func newBaseToken(session *Session, scope []string, expiresAt time.Time, tokenLength int) (*Token, error) {
	tokenCleartext, err := util.RandomToken(tokenLength)
	if err != nil {
		return nil, errors.New("could not generate access token")
	}
	// Always initialize scope to empty string slice if it's sent in as nil
	if scope == nil {
		scope = []string{}
	}
	return &Token{
		AuthorizationID: session.AuthorizationID,
		SessionID:       session.SessionID,
		TokenCleartext:  tokenCleartext,
		TokenHash:       Hashify(tokenCleartext),
		Scope:           scope,
		ClientID:        session.ClientID,
		ClientRowID:     session.ClientRowID,
		UserID:          session.UserID,
		ExpiresAt:       expiresAt,
	}, nil
}

func NewAccessToken(session *Session, scope []string, expiresAt time.Time) (*Token, error) {
	return newBaseToken(session, scope, expiresAt, accessTokenLength)
}

func NewRefreshToken(session *Session, scope []string, expiresAt time.Time) (*Token, error) {
	return newBaseToken(session, scope, expiresAt, refreshTokenLength)
}

func NewAuthorizationCode(session *Session, scope []string, expiresAt time.Time, oidcRequired bool, nonce string) (*Token, error) {
	tok, err := newBaseToken(session, scope, expiresAt, authCodeLength)
	if err != nil {
		return nil, err
	}
	tok.OIDCRequired = oidcRequired
	tok.Nonce = nonce
	return tok, nil
}

func NewTokenFromCleartext(tokenCleartext string) *Token {
	token := &Token{}
	token.TokenCleartext = tokenCleartext
	token.TokenHash = Hashify(tokenCleartext)
	return token
}
