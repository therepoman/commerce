package oauth2

import (
	"time"

	"code.justin.tv/web/owl/internal/util"

	"golang.org/x/crypto/bcrypt"
)

// Client is a model struct representing an OAuth2 application client. It
// contains information such as its public identifier, administrating user
// ID, OAuth2 redirect_uri, etc.
type Client struct {
	RowID           string    `json:"id"`
	ClientID        string    `json:"client_id"`
	RedirectURI     string    `json:"redirect_uri"`
	OwnerID         *int      `json:"owner_id"`
	Category        *string   `json:"category"`
	OauthCategoryID *int      `json:"oauth_category_id"`
	Name            string    `json:"name"`
	Secret          string    `json:"client_secret,omitempty"`
	SecretHash      string    `json:"-"`
	SDKLicensing    *bool     `json:"sdk_licensing"`
	CreatedAt       time.Time `json:"created_at"`
	UpdatedAt       time.Time `json:"updated_at"`
	Permissions     `json:"permissions"`
	Settings        `json:"settings"`
}

// Settings is a substruct for Clients demarking sundry configurables for a
// client, such as whether or not the client is hidden or first party.
type Settings struct {
	IsExtension        bool `json:"is_extension"`
	IsHidden           bool `json:"hidden"`
	IsFirstParty       bool `json:"is_first_party"`
	IsFuelClient       bool `json:"is_fuel_client"`
	ShouldExpireTokens bool `json:"should_expire_tokens"`
}

// Permissions is a substruct for Clients demarking boolean flags for a client's
// capabilities, such as performing password grant or embedding Passport login
type Permissions struct {
	PasswordGrantAllowed bool `json:"password_grant_allowed"`
	AllowEmbed           bool `json:"allow_embed"`
}

// NewClientSecret generates and returns a client secret and its respective bcrypted
// digest. The secret can be returned to the client caller and the hash should
// be what is sent to permanent storage.
func NewClientSecret() (string, string, error) {
	secret, err := util.RandomToken(clientSecretLength)
	if err != nil {
		return "", "", err
	}

	secretHash, err := bcrypt.GenerateFromPassword([]byte(secret), bcrypt.DefaultCost)
	if err != nil {
		return "", "", err
	}
	return secret, string(secretHash[:]), nil
}

// NewClient takes a name, redirect_uri, category string, oauth_category mapping ID,
// and owner user_id and generates a new Client model with a client_secret
// available to be returned to the user. This client secret is stored hashed
// and will not be recoverable in subsequent calls.
func NewClient(name, redirect, category string, oauthCategory, ownerID int, isExtension bool) (*Client, error) {
	id, err := util.RandomToken(clientIDLength)
	if err != nil {
		return nil, err
	}
	secret, secretHash, err := NewClientSecret()
	if err != nil {
		return nil, err
	}

	client := new(Client)

	client.OwnerID = &ownerID
	client.Name = name
	client.RedirectURI = redirect
	client.Category = &category
	client.OauthCategoryID = &oauthCategory
	client.ClientID = id
	client.Secret = secret
	client.SecretHash = secretHash
	client.IsExtension = isExtension

	return client, nil
}
