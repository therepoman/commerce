package oauth2

var cursePropertyIDs = []string{
	"l7gdn4hwb4ph4uzmrsfdwrj4wqfgzy",  // mods.curse.com - Dev
	"t7z5k2uyi4t9xqxcssedc0j52rbnp5",  // mods.curse.com - Staging
	"a0ehi6v3bb9et1c1av04q1mg9d4klh",  // mods.curse.com - Production
	"174574z5n1n3mg4zbe6hs3s9yp10em",  // Curse App - Dev
	"103dpwvj30p7x248y0tgykxlysckjk",  // Curse App - Staging
	"jf3xu125ejjjt5cl4osdjci6oz6p93r", // Curse App - Production
	"ndesfqihz95lmakb9xnofe084sqbik5", // Curse App - Account Sync (Legacy)
	"9z6n6d1prmcaa0hbfaowf7pm4yebko",  // *.curseforge.com - Dev
	"w9kf0ssite6gyssm8zk7qb4uhwua5d",  // *.curseforge.dev - Staging
	"3p3kknvu8gvoc5bkid1atg31ficair",  // *.curseforge.dev - Production
	"bwd9m4gyrrkskd14cvplqeb88okf83",  // www.wowace.com - Dev
	"c8v6np7oh11l870k2pa4cr8of2gj89",  // www.wowace.com - Staging
	"kl7x7j5xw2zl6vbergvu2vlv4qc7xn",  // www.wowace.com - Production
	"0z479ge068fyg1tc53pgn2lc2jvs8b",  // dev.bukkit.org - Dev
	"xlt4dmbb46isqh9eqnd5kj7pn2iam5",  // dev.bukkit.org - Staging
	"89i888lt6w31qgftbkzhex5u5awptx",  // dev.bukkit.org - Production
	"lhfa98to4v8j9wu8ertmmo3s49n9kt",  // www.sc2mapster.com - Dev
	"5ww3iaw1hbwxooln35ggbym4vfc57n",  // www.sc2mapster.com - Staging
	"2gyr0zt22qrtasaljq4fxsg99cp4k8",  // www.sc2mapster.com - Production
	"4nz904cjr1m3clk2hrenzo8ybi0rdz",  // www.skyrimforge.com - Dev
	"hn9bjq5yoq4y4c6hgumopqldre6338",  // www.skyrimforge.com - Staging
	"o2omj86cd4z7s0u90trsin5r6m1n6p",  // www.skyrimforge.com - Production
	"f2etz8ms4p8mq1l9qfkcx2v4xcaqj9",  // www.feed-the-beast.com - Dev
	"str2vxq2wrxht0yi9wc5j74pi8lje0",  // www.feed-the-beast.com - Staging
	"aef7mbe5djqp2q3k9wkxdp4szr414v",  // www.feed-the-beast.com - Production
	"ej2jcvrrou83mzux1qqwp0hv6ohb7ax", // minecraftforum.net
	"k1px8w040y1mobt2a6od1gse96zpopw", // hearthpwn.com
	"btg981y5c6jugyuwcg3ukortgj22iyf", // lolnexus.com
	"nquiw61tb595zscbxktdhwe2i45py5m", // strawpoll.me
	"1z5kcs3zot7sjvtpvwe9aizy4ylv98o", // muthead.com
	"2rnqjfd5dakymxe4bfth9vdw3ot54ku", // huthead.com
	"9t7rhfncurmy25ftqehpmhj16afs1dw", // diablofans.com
	"ogfz35wfibm7y4co7bvrm05p3up77gr", // www.futhead.com
	"8q589bhxgllcuirj017k7b8xvpoklwx", // news.futhead.com
	"jhdtjhaqfhe7sjo0x3148y48a51uxan", // forums.futhead.com
	"p60oo69zntoa8sshnb21zizytq6mq8k", // heroesnexus.com
	"1mg4gtffwmisrc0nwgcbsxmyx4dsuvr", // overpwn.com
	"kovak13487etk1oafp98qc5kdaxr0dz", // gwentdb.com
	"7277ogzkvjeo1w44099v92gfg2go486", // mtgsalvation.com
	"pcswfe7307uog1u1cxczgaq4rmqayin", // www.gamepedia.com
	"s8799y3tau1yavpnbc05crj4sflyau2", // nbalive.gg
	"5dieeoldeqqnkm48d4o7nbp7nurcxh7", // innkeeper.com
	"lkwy1z7hg05cfrx5zocm560tec86bo3", // wowdb.com
	"rvnqfn7w30ab5cf3lon1gvu1jdxkaht", // 2khead.com
	"8vikisgh5835rr0rak4tprdv1rtzxtm", // azurilland.com
	"4kjaf5nrozsulajftvj279x0uc0m24n", // thelab.gg
	"i5d6ukk8r1uwjvhmqfsa7ozhxm5h269", // gungeoneers.com
	"saf93ggibhip2cydfqxz3qg7tw6xvas", // nomanssky.gg
	"m7mpsdl1ocrvf2elnbzrvyxoefy81g7", // maddenpros.com
	"sezvf77ri68iyj1ie11wxvw7pwjzi8q", // Table Top Hero
	"byz7sjklxkigvk0pqrddbpf0j2l7smx", // Innkeeper Desktop Application
}

// IsCurseProperty examines the attached Client struct's ClientID (canonical)
// and verifies whether or not it is in the cursePropertyIDs list. This
// is used to determine whether or not the given client is one of Curse's many
// web properties that have Twitch client IDs.
func (C *Client) IsCurseProperty() bool {
	for _, clientID := range cursePropertyIDs {
		if clientID == C.ClientID {
			return true
		}
	}
	return false
}
