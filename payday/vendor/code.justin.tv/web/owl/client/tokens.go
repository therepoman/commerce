package owl

import (
	"context"
	"fmt"
	"net/http"
	"net/url"

	"code.justin.tv/foundation/twitchclient"
	"code.justin.tv/web/owl/oauth2"
)

// ExchangeResponse is the format for the token exchange response
// http://tools.ietf.org/html/rfc6749#section-5.1
type ExchangeResponse struct {
	ErrorResponse

	// OAuth2
	AccessToken  string   `json:"access_token"`
	ExpiresIn    int      `json:"expires_in"`
	RefreshToken string   `json:"refresh_token"`
	Scope        []string `json:"scope"`

	// OIDC
	IDTokenClaims  []string `json:"id_token_claims,omitempty"`
	Nonce          string   `json:"nonce,omitempty"`
	OIDCRequired   bool     `json:"oidc_required,omitempty"`
	TokenType      string   `json:"token_type,omitempty"`
	UserinfoClaims []string `json:"userinfo_claims,omitempty"`
}

// ExchangeAuthorizationCode exchanges an authorization code for an access
// token.
func (c *client) ExchangeAuthorizationCode(ctx context.Context, clientID, clientSecret, code, redirectURI string, reqOpts *twitchclient.ReqOpts) (*ExchangeResponse, error) {
	query := url.Values{
		ClientIDParam:     {clientID},
		ClientSecretParam: {clientSecret},
		CodeParam:         {code},
		GrantTypeParam:    {AuthCodeGrantType},
		RedirectURIParam:  {redirectURI},
	}
	return c.exchange(ctx, query, reqOpts)
}

// ExchangePasswordCredentials handles the resource owner password credentials
// grant (https://tools.ietf.org/html/rfc6749#section-4.3). Callers MUST
// validate the owner's username/password before using this method.
func (c *client) ExchangePasswordCredentials(ctx context.Context, clientID, clientSecret, ownerID string, scopes []string, reqOpts *twitchclient.ReqOpts) (*ExchangeResponse, error) {
	query := url.Values{
		ClientIDParam:      {clientID},
		ClientSecretParam:  {clientSecret},
		GrantTypeParam:     {PasswordGrantType},
		OwnerIDParam:       {ownerID},
		PasswordValidParam: {"true"},
	}
	if scope := oauth2.BuildScope(scopes); scope != "" {
		query.Add(ScopeParam, scope)
	}
	return c.exchange(ctx, query, reqOpts)
}

func (c *client) exchange(ctx context.Context, values url.Values, reqOpts *twitchclient.ReqOpts) (*ExchangeResponse, error) {
	req, err := c.NewRequest("POST", fmt.Sprintf("/token?%s", values.Encode()), nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.owl.token",
		StatSampleRate: defaultStatSampleRate,
	})

	var response *ExchangeResponse
	_, err = c.DoJSON(ctx, &response, req, combinedReqOpts)
	if err != nil {
		if twitchclientError, ok := err.(*twitchclient.Error); ok {
			switch twitchclientError.Message {
			case ErrInvalidAuthorizationCode.Error():
				return nil, ErrInvalidAuthorizationCode
			case ErrInvalidClientCredentials.Error():
				return nil, ErrInvalidClientCredentials
			case ErrRedirectMismatch.Error():
				return nil, ErrRedirectMismatch
			}
		}
		return nil, err
	}

	return response, nil
}

// Validate makes an API call to Owl to lookup the authorization for a token.
// If the token is invalid or doesn't have a superset of scopes, returns an
// authorziation with Valid=false.
func (c *client) Validate(ctx context.Context, token string, scopes []string, reqOpts *twitchclient.ReqOpts) (*oauth2.Authorization, error) {
	scope := oauth2.BuildScope(scopes)
	query := url.Values{
		TokenParam: {token},
	}
	if len(scope) > 0 {
		query.Add(ScopeParam, scope)
	}

	req, err := c.NewRequest("GET", fmt.Sprintf("/validate?%s", query.Encode()), nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.owl.validate",
		StatSampleRate: defaultStatSampleRate,
	})

	var data *oauth2.Authorization
	_, err = c.DoJSON(ctx, &data, req, combinedReqOpts)
	if twitchclientErr, ok := err.(*twitchclient.Error); ok {
		switch twitchclientErr.StatusCode {
		case http.StatusNotFound:
			return nil, ErrInvalidClientID
		case http.StatusForbidden:
			return nil, ErrForbiddenClientID
		}
	}
	return data, err
}
