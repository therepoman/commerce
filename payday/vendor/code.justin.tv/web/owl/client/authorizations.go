package owl

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"

	"code.justin.tv/foundation/twitchclient"
	"code.justin.tv/web/owl/oauth2"
)

// AuthorizeResponse specifies the fields for both a successful and
// unsuccessful authorize request
type AuthorizeResponse struct {
	RedirectURI string `json:"redirect"`
	Status      int    `json:"status"`
}

// TokenResponse specifies the fields in a successful authorize request
type TokenResponse struct {
	AccessToken  string
	Code         string
	RefreshToken string
	Scope        string
}

// Authorize extends owl's http interface to create/read authorizations
// between a requesting OAuth Client and an end user
func (c *client) Authorize(ctx context.Context, authReq url.Values, reqOpts *twitchclient.ReqOpts) (*TokenResponse, error) {
	req, err := c.NewRequest("POST", "/authorize", bytes.NewBufferString(authReq.Encode()))
	if err != nil {
		return nil, err
	}

	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.owl.authorize",
		StatSampleRate: defaultStatSampleRate,
	})

	var res *AuthorizeResponse
	_, err = c.DoJSON(ctx, &res, req, combinedReqOpts)
	if err != nil {
		if twitchclientErr, ok := err.(*twitchclient.Error); ok {
			if twitchclientErr.StatusCode == http.StatusUnauthorized {
				return nil, ErrNoAuthorization
			}
			if twitchclientErr.Message == ErrRedirectMismatch.Error() {
				return nil, ErrRedirectMismatch
			}
		}
		return nil, err
	}

	resURI, err := url.Parse(res.RedirectURI)
	if err != nil {
		return nil, err
	}

	if resURI.RawQuery != "" {
		query := resURI.Query()
		if errCode := query.Get(ErrorParam); errCode != "" {
			return nil, &Error{
				Code:    errCode,
				Message: query.Get(ErrorDescriptionParam),
			}
		}
		return &TokenResponse{
			Code:  query.Get(CodeParam),
			Scope: query.Get(ScopeParam)}, nil
	}

	fragment, err := url.ParseQuery(resURI.Fragment)
	if err != nil {
		return nil, err
	}

	return &TokenResponse{
		AccessToken:  fragment.Get(AccessTokenParam),
		Code:         fragment.Get(CodeParam),
		RefreshToken: fragment.Get(RefreshTokenParam),
		Scope:        fragment.Get(ScopeParam),
	}, nil
}

// AuthorizationsResponse specifies the fields in a successful rqeuest to get
// a user's authorizations
type AuthorizationsResponse struct {
	Authorizations []*oauth2.Authorization `json:"authorizations"`
}

func (c *client) Authorizations(ctx context.Context, ownerID string, reqOpts *twitchclient.ReqOpts) ([]*oauth2.Authorization, error) {
	if ownerID == "" {
		return nil, &twitchclient.Error{
			StatusCode: 400,
			Message:    "No owner ID specified",
		}
	}

	query := url.Values{
		OwnerIDParam: {ownerID},
	}
	req, err := c.NewRequest("GET", fmt.Sprintf("/authorizations?%s", query.Encode()), nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.owl.authorizations",
		StatSampleRate: defaultStatSampleRate,
	})

	var data *AuthorizationsResponse
	_, err = c.DoJSON(ctx, &data, req, combinedReqOpts)
	return data.Authorizations, err
}

func (c *client) GetAuthorization(ctx context.Context, authorizationID string, reqOpts *twitchclient.ReqOpts) (*oauth2.Authorization, error) {
	if authorizationID == "" {
		return nil, &twitchclient.Error{
			StatusCode: 400,
			Message:    "No authorization ID specified",
		}
	}

	req, err := c.NewRequest("GET", fmt.Sprintf("/authorizations/%s", authorizationID), nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.owl.get_authorization",
		StatSampleRate: defaultStatSampleRate,
	})

	authorization := &oauth2.Authorization{}
	_, err = c.DoJSON(ctx, &authorization, req, combinedReqOpts)
	if twitchclientErr, ok := err.(*twitchclient.Error); ok {
		if twitchclientErr.StatusCode == http.StatusNotFound {
			return nil, ErrInvalidAuthorizationID
		}
	}

	return authorization, err
}

// CreateAuthorizationRequest specifies the parameters for a request to create
// an authorization
type CreateAuthorizationRequest struct {
	OwnerID          string   `json:"owner_id"`
	ClientIdentifier string   `json:"client_id"`
	Scope            []string `json:"scope"`
}

func (c *client) CreateAuthorization(ctx context.Context, createReq CreateAuthorizationRequest, reqOpts *twitchclient.ReqOpts) (*ExchangeResponse, error) {
	body, err := json.Marshal(createReq)
	if err != nil {
		return nil, err
	}

	req, err := c.NewRequest("POST", "/authorizations", bytes.NewBuffer(body))
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.owl.create_authorization",
		StatSampleRate: defaultStatSampleRate,
	})

	var res *ExchangeResponse
	_, err = c.DoJSON(ctx, &res, req, combinedReqOpts)
	return res, err
}

func (c *client) DeleteAuthorization(ctx context.Context, ownerID, clientID string, reqOpts *twitchclient.ReqOpts) error {
	query := url.Values{
		ClientIDParam: {clientID},
		OwnerIDParam:  {ownerID},
	}

	req, err := c.NewRequest("DELETE", fmt.Sprintf("/authorizations?%s", query.Encode()), nil)
	if err != nil {
		return err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.owl.delete_authorization",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if resp.StatusCode != http.StatusOK {
		return twitchclient.HandleFailedResponse(resp)
	}
	return err
}
