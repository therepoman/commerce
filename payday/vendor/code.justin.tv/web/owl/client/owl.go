package owl

import (
	"context"
	"net/url"

	"code.justin.tv/foundation/twitchclient"
	"code.justin.tv/web/owl/oauth2"
)

// Client implements the interface for Owl's APIs
type Client interface {
	Authorizations(ctx context.Context, ownerID string, reqOpts *twitchclient.ReqOpts) ([]*oauth2.Authorization, error)
	Authorize(ctx context.Context, authReq url.Values, reqOpts *twitchclient.ReqOpts) (*TokenResponse, error)
	ClientRowID(ctx context.Context, clientID string, reqOpts *twitchclient.ReqOpts) (string, error)
	CreateAuthorization(ctx context.Context, createReq CreateAuthorizationRequest, reqOpts *twitchclient.ReqOpts) (*ExchangeResponse, error)
	CreateClient(ctx context.Context, createReq CreateClientRequest, values url.Values, reqOpts *twitchclient.ReqOpts) (*oauth2.Client, error)
	DeleteAuthorization(ctx context.Context, ownerID, clientID string, reqOpts *twitchclient.ReqOpts) error
	DeleteClient(ctx context.Context, clientID string, values url.Values, reqOpts *twitchclient.ReqOpts) error
	DeleteOwnerSession(ctx context.Context, ownerID, sessionID string, reqOpts *twitchclient.ReqOpts) error
	DeleteOwnerSessions(ctx context.Context, ownerID string, reqOpts *twitchclient.ReqOpts) error
	ExchangeAuthorizationCode(ctx context.Context, clientID, clientSecret, code, redirectURI string, reqOpts *twitchclient.ReqOpts) (*ExchangeResponse, error)
	ExchangePasswordCredentials(ctx context.Context, clientID, clientSecret, userID string, scopes []string, reqOpts *twitchclient.ReqOpts) (*ExchangeResponse, error)
	GetAuthorization(ctx context.Context, authorizationID string, reqOpts *twitchclient.ReqOpts) (*oauth2.Authorization, error)
	GetClient(ctx context.Context, clientID string, reqOpts *twitchclient.ReqOpts) (*oauth2.Client, error)
	GetClientByRowID(ctx context.Context, clientRowID string, reqOpts *twitchclient.ReqOpts) (*oauth2.Client, error)
	GetClients(ctx context.Context, cursor string, showHidden bool, isExtension bool, filters map[FilterableColumn]string, sKey SortKey, sOrder SortOrder, reqOpts *twitchclient.ReqOpts) ([]*oauth2.Client, string, error)
	GetOwnerSessions(ctx context.Context, ownerID string, startKey map[string]AttributeWrapper, reqOpts *twitchclient.ReqOpts) ([]*oauth2.Session, map[string]AttributeWrapper, error)
	ResetClientSecret(ctx context.Context, clientID string, values url.Values, reqOpts *twitchclient.ReqOpts) (*oauth2.Client, error)
	UpdateClient(ctx context.Context, clientID string, updateReq UpdateClientRequest, values url.Values, reqOpts *twitchclient.ReqOpts) (*oauth2.Client, error)
	Validate(ctx context.Context, token string, scopes []string, reqOpts *twitchclient.ReqOpts) (*oauth2.Authorization, error)
	ValidateSecret(ctx context.Context, clientID, clientSecret string, reqOpts *twitchclient.ReqOpts) (bool, error)
}

type client struct {
	twitchclient.Client
}

const (
	defaultStatSampleRate = 0.1
	defaultTimingXactName = "owl"

	// AuthCodeGrantType is the grant type for authorization code exchanges
	AuthCodeGrantType = "authorization_code"
	// PasswordGrantType is the grant type for password exchanges
	PasswordGrantType = "password"

	// AccessTokenParam is the query param used for an access token
	AccessTokenParam = "access_token"
	// ClientIDParam is the query param used for a client ID
	ClientIDParam = "client_id"
	// ClientSecretParam is the query param used for a client secret
	ClientSecretParam = "client_secret"
	// CodeParam is the query param used for an authorization code
	CodeParam = "code"
	// ErrorParam is the query param used for an error
	ErrorParam = "error"
	// ErrorDescriptionParam is the query param used for an error description
	ErrorDescriptionParam = "error_description"
	// GrantTypeParam is the query param used for a grant type
	GrantTypeParam = "grant_type"
	// OwnerIDParam is the query param used for an owner ID
	OwnerIDParam = "owner_id"
	// PasswordValidParam is the query param used for the password valid flag
	PasswordValidParam = "password_valid"
	// RedirectURIParam is the query param used for a redirect URI
	RedirectURIParam = "redirect_uri"
	// RefreshTokenParam is the query param used for a refresh token
	RefreshTokenParam = "refresh_token"
	// ScopeParam is the query param used for requested scopes
	ScopeParam = "scope"
	// SessionKeyParam is the query param used as the offset for querying sessions
	SessionKeyParam = "session_key"
	// TokenParam is the query param used for an access token
	TokenParam = "token"

	// UserIDParam is the query param used for auditing purposes
	UserIDParam = "user_id"
	// UserTypeParam is the query param used for auditing purposes
	UserTypeParam = "user_type"
)

// NewClient instantiates a new Owl client
func NewClient(conf twitchclient.ClientConf) (Client, error) {
	confWithXactName := addXactNameIfNoneExists(conf)
	twitchClient, err := twitchclient.NewClient(confWithXactName)
	if err != nil {
		return nil, err
	}

	return &client{twitchClient}, nil
}

func addXactNameIfNoneExists(conf twitchclient.ClientConf) twitchclient.ClientConf {
	if conf.TimingXactName != "" {
		return conf
	}
	conf.TimingXactName = defaultTimingXactName
	return conf
}
