package owl

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"

	"code.justin.tv/foundation/twitchclient"
	"code.justin.tv/web/owl/oauth2"
)

//see below for expected  struct type
type AttributeWrapper interface {
}

// Pulled From "github.com/aws/aws-sdk-go/service/dynamodb"
// reference struct
//type AttributeValue struct {
//	_ struct{} `type:"structure"`
//	B []byte `type:"blob"`
//	BOOL *bool `type:"boolean"`
//	BS [][]byte `type:"list"`
//	L []*AttributeValue `type:"list"`
//	M map[string]*AttributeValue `type:"map"`
//	N *string `type:"string"`
//	NS []*string `type:"list"`
//	NULL *bool `type:"boolean"`
//	S *string `type:"string"`
//	SS []*string `type:"list"`
//}

type GetOwnerSessionsRequest struct {
	StartKey map[string]AttributeWrapper `json:"start_key"`
}

type GetOwnerSessionsResponse struct {
	Sessions         []*oauth2.Session           `json:"sessions"`
	LastEvaluatedKey map[string]AttributeWrapper `json:"last_evaluated_key"`
}

var (
	ErrSessionNotFound = errors.New("session not found")
)

func (c *client) GetOwnerSessions(ctx context.Context, ownerID string, startKey map[string]AttributeWrapper, reqOpts *twitchclient.ReqOpts) ([]*oauth2.Session, map[string]AttributeWrapper, error) {
	var sessions []*oauth2.Session
	var lastKey map[string]AttributeWrapper

	body := GetOwnerSessionsRequest{
		StartKey: startKey,
	}
	raw, err := json.Marshal(body)
	if err != nil {
		return sessions, lastKey, err
	}

	req, err := c.NewRequest("POST", fmt.Sprintf("/v2/owner/%s/sessions", ownerID), bytes.NewBuffer(raw))
	if err != nil {
		return sessions, lastKey, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.owl.get_owner_sessions",
		StatSampleRate: defaultStatSampleRate,
	})

	var resp GetOwnerSessionsResponse
	_, err = c.DoJSON(ctx, &resp, req, combinedReqOpts)
	if err != nil {
		return sessions, lastKey, err
	}
	return resp.Sessions, resp.LastEvaluatedKey, nil
}

func (c *client) DeleteOwnerSession(ctx context.Context, ownerID, sessionID string, reqOpts *twitchclient.ReqOpts) error {
	req, err := c.NewRequest("DELETE", fmt.Sprintf("/v2/owner/%s/sessions/%s", ownerID, sessionID), nil)
	if err != nil {
		return err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.owl.delete_owner_session",
		StatSampleRate: defaultStatSampleRate,
	})

	res, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}
	if res.StatusCode == http.StatusNotFound {
		return ErrSessionNotFound
	}
	return nil
}

type DeleteUserSessionsMessage struct {
	OwnerID string `json:"owner_id"`
}

func (c *client) DeleteOwnerSessions(ctx context.Context, ownerID string, reqOpts *twitchclient.ReqOpts) error {
	req, err := c.NewRequest("DELETE", fmt.Sprintf("/v2/owner/%s/sessions", ownerID), nil)
	if err != nil {
		return err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.owl.delete_owner_sessions",
		StatSampleRate: defaultStatSampleRate,
	})

	res, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}

	if res.StatusCode != http.StatusAccepted {
		return twitchclient.HandleFailedResponse(res)
	}

	return nil
}
