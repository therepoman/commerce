package models

import (
	"fmt"
	"net/http"
)

var ErrMap = map[string]*CodedError{}
var codeErrs = []*CodedError{ErrDisplaynameNotAvailable, ErrLoginNotAvailable, ErrLoginBlocked, ErrNotAllowedToChangeLogin, ErrPartnerDoNotSupportGuid, ErrPartnerNotReachable, ErrFilteredUser, ErrIPBlocked, ErrVerifyCodeExpired, ErrVerifyDoesNotMatch, ErrInvalidPhoneNumber, TooManyUsersAssociatedWithEmail, ErrNoIdentifiers, ErrBadIdentifiers, ErrTooManyIdentifiers, ErrNoChannelIdentifiers, ErrBadChannelIdentifiers}

var ErrDisplaynameNotAvailable = &CodedError{
	ErrorValue:      "The display name you selected is not available.",
	CodeValue:       "display_name_not_available",
	StatusCodeValue: http.StatusForbidden,
}
var ErrLoginNotAvailable = &CodedError{
	ErrorValue:      "The login you selected is not available.",
	CodeValue:       "login_not_available",
	StatusCodeValue: http.StatusForbidden,
}
var ErrLoginBlocked = &CodedError{
	ErrorValue:      "The login you selected is not yet available for re-use.",
	CodeValue:       "login_blocked",
	StatusCodeValue: http.StatusForbidden,
}
var ErrNotAllowedToChangeLogin = &CodedError{
	ErrorValue:      fmt.Sprintf("You are not allowed to change your login more than once every %d days.", LoginRenameCooldown),
	CodeValue:       "not_allowed_to_change_login",
	StatusCodeValue: http.StatusForbidden,
}
var ErrPartnerDoNotSupportGuid = &CodedError{
	ErrorValue:      "The partnership service does not support GUID user IDs",
	CodeValue:       "partner_not_support_guid",
	StatusCodeValue: http.StatusForbidden,
}
var ErrPartnerNotReachable = &CodedError{
	ErrorValue:      "The partnership service is unreachable",
	CodeValue:       "partner_not_reachable",
	StatusCodeValue: http.StatusForbidden,
}
var ErrFilteredUser = &CodedError{
	ErrorValue:      "User exists but was excluded by filter criteria.",
	CodeValue:       "filtered_user_requested",
	StatusCodeValue: http.StatusUnprocessableEntity,
}

var ErrIPBlocked = &CodedError{
	ErrorValue:      "The IP is blocked.",
	CodeValue:       "ip_blocked",
	StatusCodeValue: http.StatusUnprocessableEntity,
}

var TooManyUsersAssociatedWithEmail = &CodedError{
	ErrorValue:      "Too many users associated with the email.",
	CodeValue:       "too_many_users_for_email",
	StatusCodeValue: http.StatusUnprocessableEntity,
}

var ErrVerifyCodeExpired = &CodedError{
	ErrorValue:      "Verification code has expired",
	CodeValue:       "verify_code_expired",
	StatusCodeValue: http.StatusForbidden,
}
var ErrVerifyDoesNotMatch = &CodedError{
	ErrorValue:      "Verification code does not match",
	CodeValue:       "verify_code_does_not_match",
	StatusCodeValue: http.StatusBadRequest,
}
var ErrInvalidPhoneNumber = &CodedError{
	ErrorValue:      "Phone number is invalid",
	CodeValue:       "phone_number_invalid",
	StatusCodeValue: http.StatusBadRequest,
}
var ErrNotAllowedToHardDelete = &CodedError{
	CodeValue:       "not_allowed_to_hard_delete",
	StatusCodeValue: http.StatusForbidden,
}

var ErrBannedUserChannels = &CodedError{
	ErrorValue:      "Channel is unavailable",
	CodeValue:       "channel_unavailable",
	StatusCodeValue: http.StatusUnprocessableEntity,
}

var ErrNoIdentifiers = &CodedError{
	ErrorValue:      "No login names, emails, IDs or display names in request",
	StatusCodeValue: http.StatusBadRequest,
}

var ErrBadIdentifiers = &CodedError{
	ErrorValue:      "Invalid login names, emails or IDs in request",
	CodeValue:       "bad_identifiers",
	StatusCodeValue: http.StatusBadRequest,
}

var ErrTooManyIdentifiers = &CodedError{
	ErrorValue:      "Too many identifiers in request",
	CodeValue:       "too_many_identifiers",
	StatusCodeValue: http.StatusBadRequest,
}

var ErrNoChannelIdentifiers = &CodedError{
	ErrorValue:      "No channel names or IDs in request",
	CodeValue:       "no_channel_identifiers",
	StatusCodeValue: http.StatusBadRequest,
}
var ErrBadChannelIdentifiers = &CodedError{
	ErrorValue:      "Invalid channel names or IDs in request",
	CodeValue:       "bad_channel_identifiers",
	StatusCodeValue: http.StatusBadRequest,
}

var ErrReservationAlreadyExist = &CodedError{
	ErrorValue:      "Reservation already in DB. Duplication",
	CodeValue:       "reservation_already_exits",
	StatusCodeValue: http.StatusBadRequest,
}

var ErrReservationNotExist = &CodedError{
	ErrorValue:      "Reservation not exist in DB",
	CodeValue:       "reservation_not_exits",
	StatusCodeValue: http.StatusBadRequest,
}

var ErrTryToRollbackDifferentBlockRecord = &CodedError{
	ErrorValue: "Try to rollback different block record",
	CodeValue:  "rollback_different_block_record",
}

type ErrBadIdentifier struct {
	Name  string
	Value string
}

func (e *ErrBadIdentifier) Error() string {
	return "Invalid " + e.Name + " parameter requested: " + e.Value
}

func (e *ErrBadIdentifier) Code() string    { return "invalid_parameter" }
func (e *ErrBadIdentifier) ShouldLog() bool { return false }
func (e *ErrBadIdentifier) StatusCode() int { return http.StatusBadRequest }

func init() {
	for _, e := range codeErrs {
		ErrMap[e.Code()] = e
	}
}
