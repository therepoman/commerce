package models

type LoginRenameEvent struct {
	UserID   string `json:"user_id"`
	OldLogin string `json:"old_login"`
	NewLogin string `json:"new_login"`
	Type     string `json:"rename_type"`
	Env      string `json:"environment"`
}

type DisplaynameChangeEvent struct {
	UserID             string `json:"user_id"`
	OldDisplayname     string `json:"old_displayname"`
	OldDisplaynameLang string `json:"old_displayname_lang"`
	NewDisplayname     string `json:"new_displayname"`
	NewDisplaynameLang string `json:"new_displayname_lang"`
	UserLanguage       string `json:"user_language"`
	Type               string `json:"type"`
	Env                string `json:"environment"`
}

type BanUserEvent struct {
	ReporterID    string `json:"reporter_id"`
	TargetID      string `json:"target_id"`
	Reason        string `json:"reason"`
	Duration      string `json:"duration"`
	IPBanDuration int64  `json:"ip_ban_duration"`
	Type          string `json:"type"`
	Env           string `json:"environment"`
}

type SignupEvent struct {
	UserID   string `json:"user_id"`
	Login    string `json:"login"`
	IP       string `json:"ip"`
	DeviceID string `json:"device_id"`
	Env      string `json:"environment"`
}

type UpdateChannelEvent struct {
	ChannelID              string  `json:"channel_id"`
	Channel                string  `json:"channel"`
	OldStatus              *string `json:"old_status"`
	Status                 *string `json:"status"`
	OldGame                *string `json:"old_game"`
	Game                   *string `json:"game"`
	OldBroadcasterLanguage *string `json:"old_broadcaster_language"`
	BroadcasterLanguage    *string `json:"broadcaster_language"`
	OldGameID              *uint64 `json:"old_game_id"`
	GameID                 *uint64 `json:"game_id"`
	Env                    string  `json:"environment"`
}

type SetUserImagesEvent struct {
	UserID                       string  `json:"user_id"`
	PreviousImage                string  `json:"previous_image"`
	ImageType                    string  `json:"image_type"`
	DefaultProfileImageID        *string `json:"default_profile_image_id"`
	DefaultProfileBannerID       *string `json:"default_profile_banner_id"`
	DefaultChannelOfflineImageID *string `json:"default_channel_offline_image_id"`
	Env                          string  `json:"env"`
}
