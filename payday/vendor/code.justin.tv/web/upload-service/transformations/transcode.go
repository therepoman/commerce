package transformations

import "code.justin.tv/web/upload-service/rpc/uploader"

type Transcode struct {
	Format      string               `dynamodbav:"format"`
	Quality     uint                 `dynamodbav:"quality"`
	RemoveAlpha uploader.RemoveAlpha `dynamodbav:"remove_alpha"`
}

func (t *Transcode) AsProto() *uploader.Transformation {
	return &uploader.Transformation{
		Transformation: &uploader.Transformation_Transcode{
			Transcode: &uploader.Transcode{
				Format:      t.Format,
				Quality:     uint32(t.Quality),
				RemoveAlpha: t.RemoveAlpha,
			},
		},
	}
}
