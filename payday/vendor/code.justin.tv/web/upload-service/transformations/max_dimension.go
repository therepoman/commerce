package transformations

import "code.justin.tv/web/upload-service/rpc/uploader"

type MaxWidth struct {
	MaxWidth uint `dynamodbav:"max_width"`
}

type MaxHeight struct {
	MaxHeight uint `dynamodbav:"max_height"`
}

func (maxWidth *MaxWidth) AsProto() *uploader.Transformation {
	return &uploader.Transformation{
		Transformation: &uploader.Transformation_MaxWidth{
			MaxWidth: &uploader.MaxWidth{
				Width: uint32(maxWidth.MaxWidth),
			},
		},
	}
}

func (maxHeight *MaxHeight) AsProto() *uploader.Transformation {
	return &uploader.Transformation{
		Transformation: &uploader.Transformation_MaxHeight{
			MaxHeight: &uploader.MaxHeight{
				Height: uint32(maxHeight.MaxHeight),
			},
		},
	}
}
