package models

import (
	"fmt"

	"code.justin.tv/web/upload-service/transformations"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
)

type UploadCallback struct {
	ARN         string `dynamodbav:"arn"`
	Data        []byte `dynamodbav:"data,omitempty"`
	PubsubTopic string `dynamodbav:"pubsub_topic"`
}

type Monitoring struct {
	SNSTopic      string `dynamodbav:"sns_topic,omitempty"`
	GrafanaPrefix string `dynamodbav:"grafana_prefix,omitempty"`
	RollbarToken  string `dynamodbav:"rollbar_token,omitempty"`
}

type Validation struct {
	FileSizeLessThan uint64 `dynamodbav:"file_size_less_than,omitempty"`
	Format           string `dynamodbav:"format,omitempty"`

	// Aspect Ratio, MinimumSize, and MaximumSize are deprecated, replaced by the constraint fields below
	// If these fields are filled in by dynamoDB, they will be moved into the appropriate fields during
	// the worker processing
	AspectRatio float64    `dynamodbav:"aspect_ratio,omitempty"`
	MinimumSize *Dimension `dynamodbav:"minimum_size,omitempty"`
	MaximumSize *Dimension `dynamodbav:"maximum_size,omitempty"`

	AspectRatioConstraints []Constraint `dynamodbav:"aspect_ratio_constraints"`
	WidthConstraints       []Constraint `dynamodbav:"width_constraints"`
	HeightConstraints      []Constraint `dynamodbav:"height_constraints"`
}

type Dimension struct {
	Width  uint `dynamodbav:"width"`
	Height uint `dynamodbav:"height"`
}

type Constraint struct {
	Value float64 `dynamodbav:"value"`
	Test  string  `dynamodbav:"test"`
}

// Convenience method for constructing a Constraint
func Constrain(value float64, test string) Constraint {
	return Constraint{Value: value, Test: test}
}

// Doesn't need dynamodbav tags since it isn't directly serialized.
// See the OutputDDB struct below.
type Output struct {
	Transformations          []transformations.Transformation
	PostValidation           Validation
	Name                     string
	GrantFullControl         string
	GrantRead                string
	AllowAnimation           bool
	GIFTransformationTimeout uint32
}

type Upload struct {
	UploadId      string         `dynamodbav:"upload_id"`
	StatusValue   int32          `dynamodbav:"status_value"`
	StatusName    string         `dynamodbav:"status_name"`
	Outputs       []Output       `dynamodbav:"outputs"`
	PreValidation Validation     `dynamodbav:"pre_validation,omitempty"`
	OutputPrefix  string         `dynamodbav:"output_prefix"`
	Callback      UploadCallback `dynamodbav:"callback"`
	Monitoring    Monitoring     `dynamodbav:"monitoring"`
	CreateTime    int64          `dynamodbav:"create_time"`
	StatusMessage string         `dynamodbav:"status_message"`
}

type XformUnion struct {
	Crop             *transformations.Crop             `dynamodbav:"crop,omitempty"`
	MaxWidth         *transformations.MaxWidth         `dynamodbav:"max_width,omitempty"`
	MaxHeight        *transformations.MaxHeight        `dynamodbav:"max_height,omitempty"`
	AspectRatio      *transformations.AspectRatio      `dynamodbav:"aspect_ratio,omitempty"`
	ResizeDimensions *transformations.ResizeDimensions `dynamodbav:"resize_dimensions,omitempty"`
	ResizePercentage *transformations.ResizePercentage `dynamodbav:"resize_percentage,omitempty"`
	Transcode        *transformations.Transcode        `dynamodbav:"transcode,omitempty"`
	Type             string                            `dynamodbav:"type"`
}

type OutputDDB struct {
	Transformations          []XformUnion `dynamodbav:"transformations,omitempty"`
	PostValidation           Validation   `dynamodbav:"post_validation,omitempty"`
	Name                     string       `dynamodbav:"name"`
	GrantFullControl         string       `dynamodbav:"grant_full_control"`
	GrantRead                string       `dynamodbav:"grant_read"`
	AllowAnimation           bool         `dynamodbav:"allow_animation"`
	GIFTransformationTimeout uint32       `dynamodbav:"gif_transformation_timeout"`
}

// Dynamo would have a hard time deserializing []Transformation since it doesn't know which
// concrete struct to use. We need to control the serialization so that we can manually decide
// which Transformation struct to inflate when we deserialize.
func (o Output) MarshalDynamoDBAttributeValue(outAV *dynamodb.AttributeValue) error {
	ddb := OutputDDB{
		Transformations:          make([]XformUnion, len(o.Transformations)),
		PostValidation:           o.PostValidation,
		Name:                     o.Name,
		GrantFullControl:         o.GrantFullControl,
		GrantRead:                o.GrantRead,
		AllowAnimation:           o.AllowAnimation,
		GIFTransformationTimeout: o.GIFTransformationTimeout,
	}

	for i, t := range o.Transformations {
		var xf XformUnion
		if x, ok := t.(*transformations.Crop); ok {
			xf = XformUnion{Type: "Crop", Crop: x}
		} else if x, ok := t.(*transformations.MaxWidth); ok {
			xf = XformUnion{Type: "MaxWidth", MaxWidth: x}
		} else if x, ok := t.(*transformations.MaxHeight); ok {
			xf = XformUnion{Type: "MaxHeight", MaxHeight: x}
		} else if x, ok := t.(*transformations.AspectRatio); ok {
			xf = XformUnion{Type: "AspectRatio", AspectRatio: x}
		} else if x, ok := t.(*transformations.ResizeDimensions); ok {
			xf = XformUnion{Type: "ResizeDimensions", ResizeDimensions: x}
		} else if x, ok := t.(*transformations.ResizePercentage); ok {
			xf = XformUnion{Type: "ResizePercentage", ResizePercentage: x}
		} else if x, ok := t.(*transformations.Transcode); ok {
			xf = XformUnion{Type: "Transcode", Transcode: x}
		} else {
			return fmt.Errorf(
				"Unknown transformation, add to models/upload.go: %#v",
				t,
			)
		}
		ddb.Transformations[i] = xf
	}

	attrMap, err := dynamodbattribute.MarshalMap(ddb)
	if err != nil {
		return err
	}

	outAV.SetM(attrMap)
	return nil
}

// Deserializes temporarily into an OutputDDB struct and then inflates those values into the
// proper Transformations for the actual `Output`.
func (o *Output) UnmarshalDynamoDBAttributeValue(src *dynamodb.AttributeValue) error {
	ddb := OutputDDB{}
	err := dynamodbattribute.Unmarshal(src, &ddb)
	if err != nil {
		return err
	}

	o.GrantRead = ddb.GrantRead
	o.GrantFullControl = ddb.GrantFullControl
	o.Name = ddb.Name
	o.PostValidation = ddb.PostValidation
	o.Transformations = make([]transformations.Transformation, len(ddb.Transformations))
	o.AllowAnimation = ddb.AllowAnimation
	o.GIFTransformationTimeout = ddb.GIFTransformationTimeout

	for i, t := range ddb.Transformations {
		xf := transformation(t)
		if xf == nil {
			return fmt.Errorf(
				"Unknown transformation type, add to models/upload.go: %#v",
				t,
			)
		}
		o.Transformations[i] = xf
	}

	return nil
}

func transformation(t XformUnion) transformations.Transformation {
	switch t.Type {
	case "Crop":
		return &transformations.Crop{
			Top: t.Crop.Top, Left: t.Crop.Left,
			Width: t.Crop.Width, Height: t.Crop.Height,
		}
	case "MaxWidth":
		return &transformations.MaxWidth{MaxWidth: t.MaxWidth.MaxWidth}
	case "MaxHeight":
		return &transformations.MaxHeight{MaxHeight: t.MaxHeight.MaxHeight}
	case "AspectRatio":
		return &transformations.AspectRatio{Ratio: t.AspectRatio.Ratio}
	case "ResizeDimensions":
		return &transformations.ResizeDimensions{
			Height: t.ResizeDimensions.Height,
			Width:  t.ResizeDimensions.Width,
		}
	case "ResizePercentage":
		return &transformations.ResizePercentage{Percent: t.ResizePercentage.Percent}
	case "Transcode":
		return &transformations.Transcode{
			Format:      t.Transcode.Format,
			Quality:     t.Transcode.Quality,
			RemoveAlpha: t.Transcode.RemoveAlpha,
		}
	default:
		return nil
	}
}
