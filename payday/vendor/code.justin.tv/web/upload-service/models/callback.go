package models

import "code.justin.tv/web/upload-service/rpc/uploader"

type SNSCallback struct {
	UploadID string                `json:"upload_id"`
	Outputs  []uploader.OutputInfo `json:"outputs,omitempty"`
	Data     []byte                `json:"data,omitempty"`
	Status   int64                 `json:"status"`
}

// Deprecated: use rpc generated uploader.OutputInfo instead. The JSON formats are identical, and this struct
// will be kept to ensure compatibility.
type OutputInfo struct {
	Path         string `json:"path"`
	Name         string `json:"name"`
	NameTemplate string `json:"name_template"`
	Format       string `json:"format"`
	Width        uint   `json:"width"`
	Height       uint   `json:"height"`
	Dimensions   string `json:"dimensions"`
	FileSize     int64  `json:"file_size"`
}
