// NOTE this is a generated file! do not edit!

package cheer

import (
	"context"

	"github.com/golang/protobuf/proto"

	eventbus "code.justin.tv/eventbus/client"

	"code.justin.tv/eventbus/schema/pkg/eventbus/authorization"
	"code.justin.tv/eventbus/client/encryption"
)

const (
	CreateEventType = "CheerCreate"
)

type Create = CheerCreate

type CheerCreateHandler func(context.Context, *eventbus.Header, *CheerCreate) error

func (h CheerCreateHandler) Handler() eventbus.Handler {
	return func(ctx context.Context, message eventbus.RawMessage) error {
		dst := &CheerCreate{}
		err := proto.Unmarshal(message.Payload, dst)
		if err != nil {
			return err
		}
		return h(ctx, message.Header, dst)
	}
}

func RegisterCheerCreateHandler(mux *eventbus.Mux, f CheerCreateHandler) {
	mux.RegisterHandler(CreateEventType, f.Handler())
}

func RegisterCreateHandler(mux *eventbus.Mux, f CheerCreateHandler) {
	RegisterCheerCreateHandler(mux, f)
}

func (*CheerCreate) EventBusName() string {
	return CreateEventType
}
func getCheerCreateAnonymousUserIdAuthContext(environment string) authorization.Context {
	return authorization.Context{
		authorization.EventType:   "CheerCreate",
		authorization.Environment: environment,
		authorization.MessageName: "AnonymousUser",
		authorization.FieldName:   "Id",
	}
}

func (m *AnonymousUser) SetEncryptedId(p encryption.Provider, plaintext string) error {
	if m == nil {
		return authorization.ErrEncryptionNilReceiver
	}

	encCtx := getCheerCreateAnonymousUserIdAuthContext(p.Environment())
	err := authorization.ValidateContext(encCtx)
	if err != nil {
		return err
	}

	enc := p.Encrypter()
	b, err := enc.EncryptString(encCtx, plaintext)
	if err != nil {
		return err
	}
	m.Id = &authorization.String{
		OlePayload: b,
	}
	return err
}

func (m *AnonymousUser) GetDecryptedId(dec encryption.Decrypter) (string, error) {
	if m == nil {
		return "", authorization.ErrDecryptionNilReceiver
	}
	if m.GetId() == nil {
		return "", nil
	}
	return dec.DecryptString(m.GetId().GetOlePayload())
}
