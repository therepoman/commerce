package authorization

import "errors"

const (
	EventType   = "EventType"
	Environment = "Environment"
	MessageName = "MessageName"
	FieldName   = "FieldName"
)

var (
	ErrEncryptionNilReceiver = errors.New("attempted encrypt operation on nil pointer receiver")
	ErrDecryptionNilReceiver = errors.New("attempted decrypt operation on nil pointer receiver")
)
