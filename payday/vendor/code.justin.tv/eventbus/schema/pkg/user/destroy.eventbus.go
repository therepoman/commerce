// NOTE this is a generated file! do not edit!

package user

import (
	"context"

	"github.com/golang/protobuf/proto"

	eventbus "code.justin.tv/eventbus/client"
)

const (
	DestroyEventType = "UserDestroy"
)

type Destroy = UserDestroy

type UserDestroyHandler func(context.Context, *eventbus.Header, *UserDestroy) error

func (h UserDestroyHandler) Handler() eventbus.Handler {
	return func(ctx context.Context, message eventbus.RawMessage) error {
		dst := &UserDestroy{}
		err := proto.Unmarshal(message.Payload, dst)
		if err != nil {
			return err
		}
		return h(ctx, message.Header, dst)
	}
}

func RegisterUserDestroyHandler(mux *eventbus.Mux, f UserDestroyHandler) {
	mux.RegisterHandler(DestroyEventType, f.Handler())
}

func RegisterDestroyHandler(mux *eventbus.Mux, f UserDestroyHandler) {
	RegisterUserDestroyHandler(mux, f)
}

func (*UserDestroy) EventBusName() string {
	return DestroyEventType
}
