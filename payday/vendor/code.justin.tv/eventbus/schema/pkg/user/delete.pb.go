// Code generated by protoc-gen-go. DO NOT EDIT.
// source: user/delete.proto

package user

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion2 // please upgrade the proto package

// UserDelete triggers when a user asks us to delete them.
//
// NOTE that while the user is inaccessible after this time, the user has a
// period of time when they can opt to return to their account, and if this
// happens a UserUndelete event is fired.
type UserDelete struct {
	UserId               string   `protobuf:"bytes,1,opt,name=user_id,json=userId,proto3" json:"user_id,omitempty"`
	Login                string   `protobuf:"bytes,2,opt,name=login,proto3" json:"login,omitempty"`
	AssociatedDeviceIds  []string `protobuf:"bytes,3,rep,name=associated_device_ids,json=associatedDeviceIds,proto3" json:"associated_device_ids,omitempty"`
	ExtensionClientIds   []string `protobuf:"bytes,4,rep,name=extension_client_ids,json=extensionClientIds,proto3" json:"extension_client_ids,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *UserDelete) Reset()         { *m = UserDelete{} }
func (m *UserDelete) String() string { return proto.CompactTextString(m) }
func (*UserDelete) ProtoMessage()    {}
func (*UserDelete) Descriptor() ([]byte, []int) {
	return fileDescriptor_delete_ea9329ece6ed68a6, []int{0}
}
func (m *UserDelete) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_UserDelete.Unmarshal(m, b)
}
func (m *UserDelete) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_UserDelete.Marshal(b, m, deterministic)
}
func (dst *UserDelete) XXX_Merge(src proto.Message) {
	xxx_messageInfo_UserDelete.Merge(dst, src)
}
func (m *UserDelete) XXX_Size() int {
	return xxx_messageInfo_UserDelete.Size(m)
}
func (m *UserDelete) XXX_DiscardUnknown() {
	xxx_messageInfo_UserDelete.DiscardUnknown(m)
}

var xxx_messageInfo_UserDelete proto.InternalMessageInfo

func (m *UserDelete) GetUserId() string {
	if m != nil {
		return m.UserId
	}
	return ""
}

func (m *UserDelete) GetLogin() string {
	if m != nil {
		return m.Login
	}
	return ""
}

func (m *UserDelete) GetAssociatedDeviceIds() []string {
	if m != nil {
		return m.AssociatedDeviceIds
	}
	return nil
}

func (m *UserDelete) GetExtensionClientIds() []string {
	if m != nil {
		return m.ExtensionClientIds
	}
	return nil
}

func init() {
	proto.RegisterType((*UserDelete)(nil), "user.UserDelete")
}

func init() { proto.RegisterFile("user/delete.proto", fileDescriptor_delete_ea9329ece6ed68a6) }

var fileDescriptor_delete_ea9329ece6ed68a6 = []byte{
	// 178 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x44, 0xce, 0x31, 0x8b, 0xc2, 0x40,
	0x10, 0x05, 0x60, 0x72, 0xc9, 0xe5, 0xc8, 0x74, 0xb7, 0x46, 0x4c, 0x19, 0xac, 0x52, 0xa9, 0xe8,
	0x3f, 0xd0, 0x34, 0x69, 0x03, 0x36, 0x36, 0x4b, 0xcc, 0x0c, 0x32, 0x10, 0x76, 0x25, 0xb3, 0x8a,
	0x7f, 0xc7, 0x7f, 0x2a, 0x19, 0x41, 0xcb, 0xf7, 0x3e, 0x1e, 0x3c, 0xf8, 0xbf, 0x09, 0x8d, 0x6b,
	0xa4, 0x81, 0x02, 0xad, 0xae, 0xa3, 0x0f, 0xde, 0x24, 0x53, 0xb5, 0x7c, 0x46, 0x00, 0x47, 0xa1,
	0xb1, 0x56, 0x32, 0x0b, 0xf8, 0x9b, 0x6a, 0xcb, 0x58, 0x44, 0x65, 0x54, 0x65, 0x6d, 0x3a, 0xc5,
	0x06, 0x4d, 0x0e, 0xbf, 0x83, 0xbf, 0xb0, 0x2b, 0x7e, 0xb4, 0x7e, 0x07, 0xb3, 0x85, 0x79, 0x27,
	0xe2, 0x7b, 0xee, 0x02, 0xa1, 0x45, 0xba, 0x73, 0x4f, 0x96, 0x51, 0x8a, 0xb8, 0x8c, 0xab, 0xac,
	0x9d, 0x7d, 0xb1, 0x56, 0x6b, 0x50, 0xcc, 0x06, 0x72, 0x7a, 0x04, 0x72, 0xc2, 0xde, 0xd9, 0x7e,
	0x60, 0x72, 0x41, 0x27, 0x89, 0x4e, 0xcc, 0xc7, 0x0e, 0x4a, 0x0d, 0xca, 0x3e, 0x3d, 0xe9, 0xd7,
	0x73, 0xaa, 0xc7, 0x77, 0xaf, 0x00, 0x00, 0x00, 0xff, 0xff, 0xb4, 0xc0, 0x5d, 0x0f, 0xcd, 0x00,
	0x00, 0x00,
}
