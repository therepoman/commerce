package mock

import (
	"context"
	"sync"

	eventbus "code.justin.tv/eventbus/client"
	"code.justin.tv/eventbus/client/internal"
	"code.justin.tv/eventbus/client/internal/wire"
	"code.justin.tv/eventbus/client/publisher"
)

// Publisher is a mock publisher that can be used for asserting calls
type Publisher struct {
	PublishCalls []PublishCall

	// PublishHandler, if set, can be used to make a mock implementation do something.
	PublishHandler func(*Publisher, context.Context, eventbus.Message) error

	mu sync.Mutex
}

func (p *Publisher) Publish(ctx context.Context, message eventbus.Message) error {
	m := message.(internal.Message)

	call := PublishCall{
		Context:   ctx,
		Message:   message,
		EventName: m.EventBusName(),
	}

	p.mu.Lock()
	p.PublishCalls = append(p.PublishCalls, call)
	p.mu.Unlock()

	var err error
	if handler := p.PublishHandler; handler != nil {
		err = handler(p, ctx, message)
	}
	return err
}

func (p *Publisher) Close() error {
	return nil
}

type PublishCall struct {
	Context context.Context
	Message eventbus.Message

	EventName string // The event name of the Message object
}

// DispatchPublisher gives a publisher that sends directly into a given dispatcher.
//
// The dispatcher is called synchronously within the Publish call, and any
// errors incurred by the dispatcher will be returned from the call to Publish.
func DispatchPublisher(dispatcher eventbus.Dispatcher) *Publisher {
	return &Publisher{
		PublishHandler: func(p *Publisher, ctx context.Context, m eventbus.Message) error {
			return forwardDispatch(dispatcher, ctx, m)
		},
	}
}

func forwardDispatch(dispatcher eventbus.Dispatcher, ctx context.Context, message eventbus.Message) error {
	buf, err := wire.DefaultsEncode(message, string(publisher.EnvLocal))
	if err != nil {
		return err
	}
	return dispatcher.Dispatch(ctx, buf)
}
