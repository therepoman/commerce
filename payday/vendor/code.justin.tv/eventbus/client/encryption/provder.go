package encryption

type Provider interface {
	Environment() string
	Encrypter() Encrypter
}

type Encrypter interface {
	EncryptString(map[string]string, string) ([]byte, error)
	// If support for more encrypted payload types are implemented, they would live
	// in this interface (e.g. EncryptBytes, EncryptFloat, EncryptIPAddress, etc)
}

type Decrypter interface {
	DecryptString([]byte) (string, error)
	// If support for more encrypted payload types are implemented, they would live
	// in this interface (e.g. DecryptBytes, DecryptFloat, DecryptIPAddress, etc)
}
