package encryption

import "errors"

var ErrOLENotInitialized = errors.New("ole client is not initialized")
var ErrNilOLEPayload = errors.New("nil authorized field OLE payload")
