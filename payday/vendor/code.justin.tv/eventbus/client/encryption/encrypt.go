package encryption

import (
	"bytes"

	"github.com/pkg/errors"
)

func (c *AuthorizedFieldClient) EncryptString(encCtx map[string]string, plaintext string) ([]byte, error) {
	olePayload, err := c.encrypt(encCtx, []byte(plaintext))
	if err != nil {
		return nil, errors.Wrap(err, "could not encrypt authorized field")
	}

	return olePayload, nil
}

func (c *AuthorizedFieldClient) encrypt(encCtx map[string]string, plaintext []byte) ([]byte, error) {
	var buf bytes.Buffer
	encryptedBuf := c.ole.NewEncryptor(encCtx, &buf)

	_, err := encryptedBuf.Write(plaintext)
	if err != nil {
		return nil, err
	}

	return buf.Bytes(), nil
}
