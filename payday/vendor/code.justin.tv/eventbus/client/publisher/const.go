package publisher

const (
	EnvProduction  Environment = "production"
	EnvStaging     Environment = "staging"
	EnvDevelopment Environment = "development"
	EnvLocal       Environment = "local"
)

const routeURIFormat = "%s/publishers/%s/%s.json"

const routeBaseURI = "https://s3-us-west-2.amazonaws.com/eventbus-config.internal.justin.tv"
