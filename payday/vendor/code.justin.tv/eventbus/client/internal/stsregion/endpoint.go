package stsregion

import (
	"github.com/aws/aws-sdk-go/aws/client"
	"github.com/aws/aws-sdk-go/aws/endpoints"
	"github.com/aws/aws-sdk-go/service/sts"
)

func ConfiguredEndpoint(session client.ConfigProvider) endpoints.STSRegionalEndpoint {
	return session.ClientConfig(sts.EndpointsID).Config.STSRegionalEndpoint
}
