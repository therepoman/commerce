/*
Wire encoding for the event bus' binary payloads.

The Event bus payload is set up for potential future growth, and could even
support other encodings in the future, so we have a Major Version byte at the
beginning. This allows us 255 major versions, which we are unlikely to ever
make full use of.

After the major version byte, everything else is specific to that version's
format.

Encoding Version 1:

   1 byte: version, 0x01
   2 bytes: header_len uint16 network-endian(big endian) encoded integer,
            number of header bytes to follow
   <header_len> bytes of protobuf-encoded header, defined in header.proto
   <bytes of protobuf payload>

*/
package wire
