# gometrics

Report runtime metrics to statsd for any Go-based project

![grafana example](graphs.png)

## Usage

```go
package main

import (
	"log"
	"time"

	"code.justin.tv/common/gometrics"
	"github.com/cactus/go-statsd-client/statsd"
)

func main() {
	stats, err := statsd.NewClient("graphite.internal.justin.tv:8125", "myapp")
	if err != nil {
		log.Fatal(err)
	}

	gometrics.Monitor(stats, time.Second*5)

	// start your app...
}

```