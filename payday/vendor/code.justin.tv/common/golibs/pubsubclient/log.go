package pubsubclient

import (
	"log"
	"os"
)

func init() {
	WarningLogger = log.New(os.Stderr, "[pubsubclient] ", log.LstdFlags|log.Lmicroseconds)
}

// WarningLogger receives messages about things you might care about
// but which aren't errors, like overflowing buffers and sudden
// disconnections. By default, these messages are discarded.
var WarningLogger *log.Logger

func warnf(msg string, args ...interface{}) {
	WarningLogger.Printf(msg, args...)
}
