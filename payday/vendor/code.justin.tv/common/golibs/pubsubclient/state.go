package pubsubclient

type state string

const (
	waitingToConnect state = "waitingToConnect" // client should make a new connection
	connecting       state = "connecting"       // client is building connection
	connected        state = "connected"        // client has a healthy connection
	disconnected     state = "disconnected"     // client lost its connection temporarily
	closed           state = "closed"           // client is permanently shut down
)
