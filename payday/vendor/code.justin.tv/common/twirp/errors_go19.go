// +build go1.9

package twirp

import "github.com/twitchtv/twirp" // new home of twirp as of v5.0.0

// Error represents an error in a Twirp service call.
type Error = twirp.Error

// ErrorCode represents a Twirp error type.
type ErrorCode = twirp.ErrorCode

const (
	// Canceled indicates the operation was cancelled (typically by the caller).
	Canceled = twirp.Canceled

	// Unknown error.
	// For example when handling errors raised by APIs that do not return enough error information.
	Unknown = twirp.Unknown

	// InvalidArgument indicates client specified an invalid argument.
	// It indicates arguments that are problematic regardless of the state of the system
	// (i.e. a malformed file name, required argument, number out of range, etc.).
	InvalidArgument = twirp.InvalidArgument

	// DeadlineExceeded means operation expired before completion.
	// For operations that change the state of the system, this error may be
	// returned even if the operation has completed successfully (timeout).
	DeadlineExceeded = twirp.DeadlineExceeded

	// NotFound means some requested entity was not found.
	NotFound = twirp.NotFound

	// BadRoute means that the requested URL path wasn't routable to a Twirp
	// service and method. This is returned by the generated server, and usually
	// shouldn't be returned by applications. Instead, applications should use
	// NotFound or Unimplemented.
	BadRoute = twirp.BadRoute

	// AlreadyExists means an attempt to create an entity failed because one already exists.
	AlreadyExists = twirp.AlreadyExists

	// PermissionDenied indicates the caller does not have permission to
	// execute the specified operation.
	// It must not be used if the caller cannot be identified (Unauthenticated).
	PermissionDenied = twirp.PermissionDenied

	// Unauthenticated indicates the request does not have valid
	// authentication credentials for the operation.
	Unauthenticated = twirp.Unauthenticated

	// ResourceExhausted indicates some resource has been exhausted, perhaps
	// a per-user quota, or perhaps the entire file system is out of space.
	ResourceExhausted = twirp.ResourceExhausted

	// FailedPrecondition indicates operation was rejected because the
	// system is not in a state required for the operation's execution.
	// For example, doing an rmdir operation on a directory that is non-empty,
	// or on a non-directory object, or when having conflicting read-modify-write
	// on the same resource.
	FailedPrecondition = twirp.FailedPrecondition

	// Aborted indicates the operation was aborted, typically due to a
	// concurrency issue like sequencer check failures, transaction aborts, etc.
	Aborted = twirp.Aborted

	// OutOfRange means operation was attempted past the valid range.
	// E.g., seeking or reading past end of a paginated collection.
	//
	// Unlike InvalidArgument, this error indicates a problem that may
	// be fixed if the system state changes (i.e. adding more items to the collection).
	//
	// There is a fair bit of overlap between FailedPrecondition and
	// OutOfRange. We recommend using OutOfRange (the more specific
	// error) when it applies so that callers who are iterating through
	// a space can easily look for an OutOfRange error to detect when
	// they are done.
	OutOfRange = twirp.OutOfRange

	// Unimplemented indicates operation is not implemented or not supported/enabled in this service.
	Unimplemented = twirp.Unimplemented

	// Internal errors. When some invariants expected by the underlying system have been broken.
	// In other words, something bad happened in the library or backend service.
	// Do not confuse with HTTP Internal Server Error; an Internal error
	// could also happen on the client code, i.e. when parsing a server response.
	Internal = twirp.Internal

	// Unavailable indicates the service is currently unavailable.
	// This is a most likely a transient condition and may be corrected
	// by retrying with a backoff.
	Unavailable = twirp.Unavailable

	// DataLoss indicates unrecoverable data loss or corruption.
	DataLoss = twirp.DataLoss

	// NoError is the zero-value, is considered an empty error and should not be used.
	NoError = twirp.NoError
)
