# twitchhttp

[![GoDoc](http://godoc.internal.justin.tv/code.justin.tv/common/twitchhttp?status.svg)](http://godoc.internal.justin.tv/code.justin.tv/common/twitchhttp)

Package twitchhttp enables quicker creation of production-ready HTTP clients and servers.