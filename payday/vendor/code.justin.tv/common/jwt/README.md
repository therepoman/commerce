#JWT

JSON web tokens, and claims! Lean, fast and secure! Neat API!

https://godoc.internal.justin.tv/code.justin.tv/common/jwt

##Why use this package?


###It's clean
It was built with an emphasis on lean-ness and security. Jwt is 270 lines of Go code, excluding tests and examples:

```
cloc $(go list -f '{{join .GoFiles " "}}')
       6 text files.
       6 unique files.                              
       0 files ignored.

http://cloc.sourceforge.net v 1.64  T=0.01 s (564.8 files/s, 43583.8 lines/s)
-------------------------------------------------------------------------------
Language                     files          blank        comment           code
-------------------------------------------------------------------------------
Go                               6             93            100            270
-------------------------------------------------------------------------------
SUM:                             6             93            100            270
-------------------------------------------------------------------------------

```

This is ~2.5 times less than the current Go jwt package listed on jwt.io.

###It's fast
```
jwt    200000 7006 ns/op  (this package)
jwt-go 100000 12835 ns/op (github.com/dgrijalva/jwt-go)
```

###It's secure

Jwt is very lightweight, making it easy to find bugs. It's designed to be invulnerable to the known JWT exploits and it's been fuzed with go-fuzz for several days.

###It has a nice API

To parse, call DecodeAndValidate with pointers to the values you want to fill with header and claims information, the specified algorithm and the jwt bytes:

```Go
var header, claims map[string]interface{}
err := jwt.DecodeAndValidate(&header, &claims, jwt.RS256([]byte("secret")), jwtBytes)
```


To generate, pass your header, claims and Algorithm to jwt.Encode:

```Go
bt, err := jwt.Encode(header, claims, jwt.RS256([]byte("secret")))
```

Note! These examples are super poor and contrived! There are solid examples of using RSA and HMAC (symmetric and asymmetric crypto) in the godoc.


##Guided tour

Let's look at that list again:

```
 cloc --by-file $(go list -f '{{join .GoFiles " "}}')
       6 text files.
       6 unique files.                              
       0 files ignored.

http://cloc.sourceforge.net v 1.64  T=0.01 s (554.5 files/s, 42790.5 lines/s)
-------------------------------------------------------------------------------
File                             blank        comment           code
-------------------------------------------------------------------------------
parse.go                            25             36             85
rsa.go                              24             25             68
encode.go                           16             12             42
hmac.go                             17              8             38
jwt.go                               8             18             28
errors.go                            3              1              9
-------------------------------------------------------------------------------
SUM:                                93            100            270
-------------------------------------------------------------------------------

```

Most of these explain themselves:

File        | Purpose
------------|--------------------------------------------------------------------
`encode.go` | Contains logic for actually producing the jwts using an Algorithm. `
`errors.go` | Subtypes errors with helpful annotations
`hmac.go`   | Defines all the HMAC based Algorithms (HS*)
`jwt.go`    | Defines helpers such as Header, NewHeader for generating a canonical header from an Algorithm. Defines the nop algorithm, `None`. Defines the Algorithm interface.
`parse.go`  | Contains logic for parsing and validating JWT blobs; Fragment, DecodeAndValidate.
`rsa.go`    | Defines all RSA based Algorithms (RS*)

There's also `claim/Claims.go` which provides a method of validaing JWT claims.

I urge you to dig into the code if you're curious. Ugly code is a bug!

