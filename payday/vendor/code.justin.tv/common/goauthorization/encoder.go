package goauthorization

import (
	"time"

	"github.com/satori/go.uuid"

	"code.justin.tv/common/jwt"
	"code.justin.tv/common/jwt/claim"
)

// Encoder implements the methods of an encoder
type Encoder interface {
	Encode(TokenParams) *AuthorizationToken
}

type encoderImpl struct {
	iss string
	alg jwt.Algorithm
	hdr jwt.Header
}

// NewEncoder instantiates a new Encoder
func NewEncoder(alg, iss string, val []byte) (Encoder, error) {
	enc := &encoderImpl{
		iss: iss,
	}

	switch alg {
	case hmacAlg:
		enc.alg = jwt.HS512(val)
	case rsaAlg:
		key, err := jwt.ParseRSAPrivateKey(val)
		if err != nil {
			return enc, err
		}
		enc.alg = jwt.RS256(key)
	case eccAlg:
		key, err := jwt.ParseECPrivateKeyFromPEM(val)
		if err != nil {
			return enc, err
		}
		enc.alg = jwt.ES256(key)
	}
	enc.hdr = jwt.NewHeader(enc.alg)

	return enc, nil
}

// Encode constructs an authorization token with the given parameters
func (e *encoderImpl) Encode(p TokenParams) *AuthorizationToken {
	return &AuthorizationToken{
		Algorithm: e.alg,
		Header:    e.hdr,
		Claims: TokenClaims{
			Audience:         p.Aud,
			Expires:          claim.Exp(p.Exp),
			Issuer:           claim.Iss(e.iss),
			NotBefore:        claim.Nbf(p.Nbf),
			Sub:              claim.Sub{Sub: p.Sub},
			IssuedAt:         claim.Iat(time.Now()),
			JwtID:            uuid.NewV4().String(),
			Authorizations:   p.Claims,
			FirstPartyClient: p.Fpc,
			ClientID:         p.ClientID,
		},
	}
}
