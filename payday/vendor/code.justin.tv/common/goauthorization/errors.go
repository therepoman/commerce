package goauthorization

import "errors"

var (
	// ErrNoAuthorizationToken is returned when an HTTP request does not have an
	// authorization token in the Twitch-Authorization header
	ErrNoAuthorizationToken = errors.New("no authorization token in request")
	// ErrEncode is returned when a token cannot be serialized
	ErrEncode = errors.New("failed to serialized token")
	// ErrMissingCapability is returned when a capability is not present in an
	// authorization token
	ErrMissingCapability = errors.New("unable to find capability")
	// ErrInvalidAudience is returned when an authorization token has an
	// unexpected audience
	ErrInvalidAudience = errors.New("invalid audience")
	// ErrInvalidIssuer is returned when an authorization token has an unexpected
	// issuer
	ErrInvalidIssuer = errors.New("invalid issuer")
	// ErrInvalidCapability is returned when a capability does not have the
	// expected arguments
	ErrInvalidCapability = errors.New("invalid capability")
	// ErrInvalidBody is returned when the body of an HTTP request cannot be read
	ErrInvalidBody = errors.New("invalid request body")
	// ErrMissingParam is returned when a parameter cannot be found in an HTTP
	// request
	ErrMissingParam = errors.New("unable to find param")
	// ErrExpiredToken is returned when the token is expired
	ErrExpiredToken = errors.New("expired token")
	// ErrInvalidNbf is returned when the token is not yet valid
	ErrInvalidNbf = errors.New("token not yet valid")
)
