package atomic

import "sync/atomic"

// Bool allows atomic operation on bool
type Bool int32

// New Return a new instance of Bool
func New() *Bool {
	return new(Bool)
}

// Set bool to true
func (b *Bool) Set() {
	atomic.StoreInt32((*int32)(b), 1)
}

// IsSet checks if bool is set
func (b *Bool) IsSet() bool {
	return atomic.LoadInt32((*int32)(b)) == 1
}

// Unset sets the bool value to false
func (b *Bool) Unset() {
	atomic.StoreInt32((*int32)(b), 0)
}
