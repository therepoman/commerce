# Sandstorm Go SDK

## Full Documentation

[Here](https://godoc.internal.justin.tv/code.justin.tv/systems/sandstorm/manager).

## Migrating from Sirupsen/logrus to sirupsen/logrus

In May of 2017, a well used logging library's maintainer converted his handle
from Sirupsen to sirupsen (lowercase). Because of this,
[there have been several issues](https://github.com/sirupsen/logrus/issues/543)
in fixing Godeps / glide setups. Although the path forward is to use the
lowercase package, upgrading is a little tricky but doable.
`code.justin.tv/systems/sandstorm` has made this upgrade in
[version 1.3.0](https://git-aws.internal.justin.tv/systems/sandstorm/releases/tag/v1.3.0)
of the SDK and requires this upgrade.

### Move your gopath to a case sensitive filesystem

Using apple's diskutility application, this is fairly straight forward. Create
a partition that uses MacOS's Case-sensitive, Journaled file system. This is 
especially important if you commit your vendor folder to git.

### Remove Sirupsen/logrus from your gopath and vendor folder

You won't be able to execute and godeps / glide operations without this removed
because the github repository will not resolve.


### Find and replace all Sirupsen/logrus instances with sirupsen/logrus

Finally, migrate your source code to the lowercase repository name and vendor
as usual.
