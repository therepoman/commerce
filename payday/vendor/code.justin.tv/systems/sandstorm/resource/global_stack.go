package resource

// NewGlobalStack returns a stack
func NewGlobalStack(environment string) (Stack, error) {
	cfg, err := GetConfigForEnvironment(environment)
	if err != nil {
		return nil, err
	}

	return globalStack{*cfg}, nil
}

// ProductionStack returns the default prod stack
func ProductionStack() Stack {
	return globalStack{*ProductionConfig()}
}

// globalStack is a stack managed by the #secdev team
type globalStack struct {
	resourceConfig Config
}

func (gs globalStack) AccountID() string {
	return gs.resourceConfig.AccountID
}

func (gs globalStack) AgentTestingRoleARN() string {
	return gs.resourceConfig.AgentTestingRoleArn
}

func (gs globalStack) AdminRoleARN() string {
	return gs.resourceConfig.RoleArn
}

func (gs globalStack) AWSRegion() string {
	return gs.resourceConfig.AwsRegion
}

func (gs globalStack) CloudwatchRoleARN() string {
	return gs.resourceConfig.CloudwatchRoleArn
}

func (gs globalStack) HeartbeatsTableName() string {
	return "heartbeats-v2-" + gs.resourceConfig.Environment
}

func (gs globalStack) KMSPrimaryKey() KMSKey {
	return gs.resourceConfig.KMSKey
}

func (gs globalStack) InventoryAdminRoleARN() string {
	return gs.resourceConfig.InventoryAdminRoleARN
}

func (gs globalStack) InventoryRoleARN() string {
	return gs.resourceConfig.InventoryRoleARN
}

func (gs globalStack) InventoryStatusURL() string {
	return gs.resourceConfig.InventoryStatusURL
}

func (gs globalStack) ProxyURL() string {
	return "http://proxy.internal.justin.tv:9797/"
}

func (gs globalStack) SecretsQueueNamePrefix() string {
	return "sandstorm"
}

func (gs globalStack) SecretsTableName() string {
	return gs.resourceConfig.TableName
}

func (gs globalStack) SecretsTopicArn() string {
	return gs.resourceConfig.TopicArn
}

func (gs globalStack) AuditTableName() string {
	return gs.resourceConfig.TableName + "_audit"
}

func (gs globalStack) NamespaceTableName() string {
	return gs.resourceConfig.TableName + "_namespaces"
}

func (gs globalStack) PutHeartbeatLambdaFunctionName() string {
	return gs.resourceConfig.PutHeartbeatLambdaFunctionName()
}

func (gs globalStack) Environment() string {
	return gs.resourceConfig.Environment
}
