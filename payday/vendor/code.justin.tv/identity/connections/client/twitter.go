package connections

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"time"

	"code.justin.tv/foundation/twitchclient"
)

// TwitterUser defines the attributes received from Twitter API.
type TwitterUser struct {
	TwitchID          string `json:"twitch_id"`
	TwitterID         string `json:"twitter_id"`
	TwitterScreenName string `json:"twitter_screen_name,omitempty"`
	AccessToken       string `json:"access_token,omitempty"`
	AccessSecret      string `json:"access_secret,omitempty"`

	CreatedOn *time.Time `json:"created_on"`
	UpdatedOn *time.Time `json:"updated_on"`

	EncryptedAccessToken  string `json:"-"`
	EncryptedAccessSecret string `json:"-"`
}

// AuthTwitter gets a Twitter redirect URL
func (c *client) AuthTwitter(ctx context.Context, uid string, opts *twitchclient.ReqOpts) (string, error) {
	u := &url.URL{Path: fmt.Sprintf("/v2/%s/twitter/auth", uid)}
	res, err := c.do(ctx, "GET", u.String(), "authTwitter", opts)
	if err != nil {
		return "", err
	}
	defer res.Body.Close()

	if res.StatusCode != http.StatusOK {
		return "", decodeError(res)
	}

	var resp RedirectPathResp
	err = json.NewDecoder(res.Body).Decode(&resp)
	if err != nil {
		return "", ErrUnexpectedResponse
	}

	return resp.Path, nil
}

// GetTwitterUser retrieves a TwitterUser given a user ID
func (c *client) GetTwitterUser(ctx context.Context, uid string, opts *twitchclient.ReqOpts) (*TwitterUser, error) {
	u := &url.URL{Path: fmt.Sprintf("/v2/%s/twitter", uid)}
	res, err := c.do(ctx, "GET", u.String(), "getTwitterUser", opts)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	switch res.StatusCode {
	case http.StatusOK:
		var data TwitterUser
		err = json.NewDecoder(res.Body).Decode(&data)
		return &data, err
	case http.StatusNotFound:
		return nil, ErrNoConnection
	default:
		return nil, decodeError(res)
	}

}

// CallbackTwitter creates a new Twitter connection given a user ID, a Twitter
// OAuth token, and a Twitter OAuth verifier. Requires an authorization token.
func (c *client) CallbackTwitter(ctx context.Context, uid string, values url.Values, opts *twitchclient.ReqOpts) error {
	u := &url.URL{Path: fmt.Sprintf("/v2/%s/twitter/callback", uid), RawQuery: values.Encode()}
	res, err := c.do(ctx, "GET", u.String(), "callbackTwitter", opts)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	if res.StatusCode != http.StatusCreated {
		return decodeError(res)
	}

	return nil
}

// DeleteTwitter deletes a Twitter connection given a user ID. Requires an
// authorization token.
func (c *client) DeleteTwitterUser(ctx context.Context, uid string, opts *twitchclient.ReqOpts) error {
	u := &url.URL{Path: fmt.Sprintf("/v2/%s/twitter", uid)}
	res, err := c.do(ctx, "DELETE", u.String(), "deleteTwitterUser", opts)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	if res.StatusCode != http.StatusNoContent {
		return decodeError(res)
	}

	return nil
}
