package connections

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"time"

	"code.justin.tv/foundation/twitchclient"
)

// FacebookUser specifies the fields of a Facebook connection
type FacebookUser struct {
	TwitchID    string     `json:"twitch_id"`
	FacebookID  string     `json:"facebook_id"`
	Name        string     `json:"name,omitempty"`
	Profile     string     `json:"profile,omitempty"`
	AccessToken string     `json:"access_token,omitempty"`
	CreatedOn   *time.Time `json:"created_on,omitempty"`
	UpdatedOn   *time.Time `json:"updated_on,omitempty"`
}

// FacebookConnections encodes a list of facebook connections
type FacebookConnections struct {
	Connected []FacebookUser `json:"connected"`
}

func (c *client) AuthFacebook(ctx context.Context, uid string, values url.Values, opts *twitchclient.ReqOpts) (string, error) {
	u := &url.URL{Path: fmt.Sprintf("/v2/%s/facebook/auth", uid), RawQuery: values.Encode()}
	res, err := c.do(ctx, "GET", u.String(), "authFacebook", opts)
	if err != nil {
		return "", err
	}
	defer res.Body.Close()

	if res.StatusCode != http.StatusOK {
		return "", decodeError(res)
	}

	var resp RedirectPathResp
	if err := json.NewDecoder(res.Body).Decode(&resp); err != nil {
		return "", err
	}
	return resp.Path, nil
}

func (c *client) CallbackFacebook(ctx context.Context, uid string, values url.Values, opts *twitchclient.ReqOpts) error {
	u := &url.URL{Path: fmt.Sprintf("/v2/%s/facebook/callback", uid), RawQuery: values.Encode()}
	res, err := c.do(ctx, "GET", u.String(), "callbackFacebook", opts)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	if res.StatusCode != http.StatusCreated {
		return decodeError(res)
	}
	return nil
}

func (c *client) GetFacebookUser(ctx context.Context, uid string, opts *twitchclient.ReqOpts) (*FacebookUser, error) {
	u := &url.URL{Path: fmt.Sprintf("/v2/%s/facebook", uid)}
	res, err := c.do(ctx, "GET", u.String(), "getFacebookUser", opts)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	switch res.StatusCode {
	case http.StatusOK:
		var data FacebookUser
		err = json.NewDecoder(res.Body).Decode(&data)
		return &data, err
	case http.StatusNotFound:
		return nil, ErrNoConnection
	default:
		return nil, decodeError(res)
	}
}

func (c *client) DeleteFacebookUser(ctx context.Context, uid string, opts *twitchclient.ReqOpts) error {
	u := &url.URL{Path: fmt.Sprintf("/v2/%s/facebook", uid)}
	res, err := c.do(ctx, "DELETE", u.String(), "deleteFacebookUser", opts)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	if res.StatusCode != http.StatusNoContent {
		return decodeError(res)
	}
	return nil
}

func (c *client) GetFacebookConnected(ctx context.Context, facebookID string, opts *twitchclient.ReqOpts) (*FacebookUser, error) {
	u := &url.URL{Path: "/v2/facebook"}

	postData := ConnectedReq{IDs: []string{facebookID}}
	body, err := json.Marshal(postData)
	if err != nil {
		return nil, err
	}

	req, err := c.NewRequest("POST", u.String(), bytes.NewReader(body))
	if err != nil {
		return nil, err
	}
	req.Header.Add("Content-Type", "application/json")

	merged := twitchclient.MergeReqOpts(opts, twitchclient.ReqOpts{
		StatName:       fmt.Sprintf("service.connections.%s", "getFacebookConnected"),
		StatSampleRate: defaultStatSampleRate,
	})

	res, err := c.Do(ctx, req, merged)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	switch res.StatusCode {
	case http.StatusOK:
		var data FacebookConnections
		err = json.NewDecoder(res.Body).Decode(&data)
		if len(data.Connected) < 1 {
			return nil, ErrNoConnection
		}
		fbConnection := data.Connected[0]
		return &fbConnection, nil
	default:
		return nil, decodeError(res)
	}
}

func (c *client) AdminCreateFacebook(ctx context.Context, params FacebookUser, opts *twitchclient.ReqOpts) error {
	u := &url.URL{Path: "/admin/v2/facebook"}

	body, err := json.Marshal(params)
	if err != nil {
		return err
	}

	req, err := c.NewRequest("POST", u.String(), bytes.NewReader(body))
	if err != nil {
		return err
	}
	req.Header.Add("Content-Type", "application/json")

	merged := twitchclient.MergeReqOpts(opts, twitchclient.ReqOpts{
		StatName:       fmt.Sprintf("service.connections.%s", "adminCreateFacebook"),
		StatSampleRate: defaultStatSampleRate,
	})

	res, err := c.Do(ctx, req, merged)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	switch res.StatusCode {
	case http.StatusCreated:
		return nil
	default:
		return decodeError(res)
	}
}

func (c *client) AdminDeleteFacebook(ctx context.Context, uid, facebookID string, opts *twitchclient.ReqOpts) error {
	u := &url.URL{Path: "/admin/v2/facebook"}
	query := u.Query()
	query.Set("user_id", uid)
	query.Set("facebook_id", facebookID)
	u.RawQuery = query.Encode()

	req, err := c.NewRequest("DELETE", u.String(), nil)
	if err != nil {
		return err
	}
	req.Header.Add("Content-Type", "application/json")

	merged := twitchclient.MergeReqOpts(opts, twitchclient.ReqOpts{
		StatName:       fmt.Sprintf("service.connections.%s", "adminDeleteFacebook"),
		StatSampleRate: defaultStatSampleRate,
	})

	res, err := c.Do(ctx, req, merged)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	switch res.StatusCode {
	case http.StatusNoContent:
		return nil
	default:
		return decodeError(res)
	}
}
