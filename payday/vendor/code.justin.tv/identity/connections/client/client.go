package connections

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"net/url"

	"code.justin.tv/foundation/twitchclient"
)

const (
	defaultStatSampleRate = 0.1
	defaultTimingXactName = "connections"
	ver                   = "v1"

	// ManageConnectionsCapability is the Cartman capability required in the
	// authorization token for all create and delete Twitter connection requests
	ManageConnectionsCapability = "connections::manage_connections"

	// StateDelim is the string delimiter used to separate parameters in any state
	// parameters passed through providers' auth flows
	StateDelim = ":"
)

var (
	// ErrUnexpectedResponse is returned if the Connections service returns an
	// unhandled error
	ErrUnexpectedResponse = errors.New("unexpected response from connections service")
	// ErrNoConnection is returned if the user has no connections
	ErrNoConnection = errors.New("no connections found")
)

// Client defines the interface for a client of the Connections service
type Client interface {
	AuthTwitter(ctx context.Context, uid string, opts *twitchclient.ReqOpts) (string, error)
	GetTwitterUser(ctx context.Context, uid string, opts *twitchclient.ReqOpts) (*TwitterUser, error)
	CallbackTwitter(ctx context.Context, uid string, values url.Values, opts *twitchclient.ReqOpts) error
	DeleteTwitterUser(ctx context.Context, uid string, opts *twitchclient.ReqOpts) error

	AuthSteam(ctx context.Context, uid string, values url.Values, opts *twitchclient.ReqOpts) (string, error)
	CallbackSteam(ctx context.Context, uid string, values url.Values, opts *twitchclient.ReqOpts) error
	GetSteamUser(ctx context.Context, uid string, opts *twitchclient.ReqOpts) (*SteamUser, error)
	DeleteSteamUser(ctx context.Context, uid string, opts *twitchclient.ReqOpts) error

	AuthYoutube(ctx context.Context, uid string, opts *twitchclient.ReqOpts) (string, error)
	CallbackYoutube(ctx context.Context, uid string, values url.Values, opts *twitchclient.ReqOpts) error
	GetYoutubeUser(ctx context.Context, uid string, opts *twitchclient.ReqOpts) (*YoutubeUser, error)
	DeleteYoutubeUser(ctx context.Context, uid string, opts *twitchclient.ReqOpts) error

	AuthBlizzard(ctx context.Context, uid string, values url.Values, opts *twitchclient.ReqOpts) (string, error)
	CallbackBlizzard(ctx context.Context, uid string, values url.Values, opts *twitchclient.ReqOpts) error
	GetBlizzardUser(ctx context.Context, uid string, opts *twitchclient.ReqOpts) (*BlizzardUser, error)
	DeleteBlizzardUser(ctx context.Context, uid string, opts *twitchclient.ReqOpts) error
	GetBlizzardUserByBlizzardID(ctx context.Context, id string, opts *twitchclient.ReqOpts) (*BlizzardUser, error)

	AuthFacebook(ctx context.Context, uid string, values url.Values, opts *twitchclient.ReqOpts) (string, error)
	CallbackFacebook(ctx context.Context, uid string, values url.Values, opts *twitchclient.ReqOpts) error
	GetFacebookUser(ctx context.Context, uid string, opts *twitchclient.ReqOpts) (*FacebookUser, error)
	DeleteFacebookUser(ctx context.Context, uid string, opts *twitchclient.ReqOpts) error
	GetFacebookConnected(ctx context.Context, connectionID string, opts *twitchclient.ReqOpts) (*FacebookUser, error)
	AdminCreateFacebook(ctx context.Context, params FacebookUser, opts *twitchclient.ReqOpts) error
	AdminDeleteFacebook(ctx context.Context, uid, connectionID string, opts *twitchclient.ReqOpts) error
}

// Platform defines a no-op interface for providing access to the Backend
// interface
type Platform interface {
}

type client struct {
	twitchclient.Client
}

// RedirectPathResp encodes a redirect path for third party authorization.
type RedirectPathResp struct {
	Path string `json:"path"`
}

// ConnectedReq encodes the list of ids in a request for connected platforms.
type ConnectedReq struct {
	IDs []string `json:"ids"`
}

// ConnectedResp encodes the list of connected platforms.
type ConnectedResp struct {
	Connected []Platform `json:"connected"`
}

// ErrorResponse unmarshals the error message from Connections service.
type ErrorResponse struct {
	Error string `json:"error"`
}

func decodeError(res *http.Response) error {
	var errorResp ErrorResponse
	err := json.NewDecoder(res.Body).Decode(&errorResp)
	if err != nil {
		return ErrUnexpectedResponse
	}

	return errors.New(errorResp.Error)
}

// NewClient instantiates a new client for the Connections service
func NewClient(conf twitchclient.ClientConf) (Client, error) {
	if conf.TimingXactName == "" {
		conf.TimingXactName = defaultTimingXactName
	}
	twitchClient, err := twitchclient.NewClient(conf)
	return &client{Client: twitchClient}, err
}

func (c *client) do(ctx context.Context, method, path, stat string, opts *twitchclient.ReqOpts) (*http.Response, error) {
	req, err := c.NewRequest(method, path, nil)
	if err != nil {
		return nil, err
	}

	merged := twitchclient.MergeReqOpts(opts, twitchclient.ReqOpts{
		StatName:       fmt.Sprintf("service.connections.%s", stat),
		StatSampleRate: defaultStatSampleRate,
	})

	return c.Do(ctx, req, merged)
}
