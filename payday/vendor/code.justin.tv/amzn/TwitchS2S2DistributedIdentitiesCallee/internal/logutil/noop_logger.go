package logutil

// NoopLogger implements the log interface with no operation
var NoopLogger noopLogger

type noopLogger struct{}

func (noopLogger) Log(string, ...interface{}) {}
