package cachedvalidation

import (
	"code.justin.tv/amzn/TwitchS2S2DistributedIdentitiesCallee/internal/validation"
)

type cacheValue struct {
	Token      []byte
	Validation *validation.Validation
}
