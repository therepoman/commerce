package opwrap

import "code.justin.tv/video/metrics-middleware/v2/operation"

const twitchS2S2DICalleeOperationGroupName = "twitchs2s2"
const (
	statusUnknown int32 = 2
)

// ValidateAuthToken validated the token present in req header
var ValidateAuthToken = operation.Name{
	Kind:   operation.KindClient,
	Group:  twitchS2S2DICalleeOperationGroupName,
	Method: "ValidateAuthToken",
}

// LoadRootCertificate is reported when a root certificate is loaded in the
// foreground.
var LoadRootCertificate = operation.Name{
	Kind:   operation.KindClient,
	Group:  twitchS2S2DICalleeOperationGroupName,
	Method: "LoadRootCertificate",
}

// LoadRootCertificateInBackground the same as LoadRootCertificate except it is
// loaded in the background.
var LoadRootCertificateInBackground = operation.Name{
	Kind:   operation.KindClient,
	Group:  twitchS2S2DICalleeOperationGroupName,
	Method: "LoadRootCertificateInBackground",
}

// GetServiceCertificate is reported when a service certificate is loaded in the
// foreground.
var GetServiceCertificate = operation.Name{
	Kind:   operation.KindClient,
	Group:  twitchS2S2DICalleeOperationGroupName,
	Method: "GetServiceCertificate",
}

// GetServiceCertificateInBackground is reported when a service certificate is
// loaded in the background.
var GetServiceCertificateInBackground = operation.Name{
	Kind:   operation.KindClient,
	Group:  twitchS2S2DICalleeOperationGroupName,
	Method: "GetServiceCertificateInBackground",
}

// EndWithError wraps an operation End with setting error status
func EndWithError(op *operation.Op, err error) {
	if err != nil {
		status := statusUnknown
		if err, ok := err.(interface {
			StatusCode() int32
		}); ok {
			status = err.StatusCode()
		}
		op.SetStatus(operation.Status{Code: status})
	}
	op.End()
}
