package validation

import (
	"fmt"
	"net/url"
	"strings"
)

var defaultPorts = map[string]string{
	"http":  "80",
	"https": "443",
}

// NewOrigins creates a new set of origins
func NewOrigins(rawOrigins ...string) (*Origins, error) {
	if len(rawOrigins) < 1 {
		return nil, fmt.Errorf("origins defined must be at least len 1")
	}

	origins := make(map[string]interface{}, len(rawOrigins))
	for _, o := range rawOrigins {
		origin, err := parseWebOrigin(o)
		if err != nil {
			return nil, err
		}
		origins[origin] = nil
	}
	return &Origins{origins: origins}, nil
}

// Origins is a set of origins
type Origins struct {
	origins map[string]interface{}
}

// Contains returns true if a origin is in our set
func (o *Origins) Contains(in string) bool {
	_, ok := o.origins[in]
	return ok
}

func (o *Origins) String() string {
	parts := make([]string, 0, len(o.origins))
	for k := range o.origins {
		parts = append(parts, k)
	}
	return strings.Join(parts, " ")
}

func parseWebOrigin(o string) (string, error) {
	url, err := url.Parse(o)
	if err != nil {
		return "", err
	}

	scheme, hostname, port := url.Scheme, url.Hostname(), url.Port()

	if scheme == "" || hostname == "" {
		return "", fmt.Errorf("unparseable web origin: %s", o)
	}

	scheme = strings.ToLower(scheme)
	hostname = strings.ToLower(hostname)

	if defaultPorts[scheme] == port || port == "" {
		return fmt.Sprintf("%s://%s", scheme, hostname), nil
	}

	return fmt.Sprintf("%s://%s:%s", scheme, hostname, port), nil
}
