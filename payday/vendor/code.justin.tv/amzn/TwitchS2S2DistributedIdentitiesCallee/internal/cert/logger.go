package cert

import (
	"sync"
	"time"

	logging "code.justin.tv/amzn/TwitchLogging"
)

type unknownCallerLogger struct {
	logger                         logging.Logger
	initLogger                     sync.Once
	unknownCallersMap              map[string]bool
	UnknownCallerLoggerRateLimiter <-chan time.Time
	mux                            sync.Mutex
}

func (u *unknownCallerLogger) LogUnknownCaller(x5u string) {
	u.initLogger.Do(func() {
		u.logger.Log("UnknownCallersReceived", "x5us", []string{x5u})
	},
	)
	select {
	case <-u.UnknownCallerLoggerRateLimiter:
		var bufferSlice []string
		func() {
			u.mux.Lock()
			defer u.mux.Unlock()
			u.unknownCallersMap[x5u] = true
			for key := range u.unknownCallersMap {
				bufferSlice = append(bufferSlice, key)
				delete(u.unknownCallersMap, key)
			}
		}()
		u.logger.Log("UnknownCallersReceived", "x5us", bufferSlice)
	default:
		func() {
			u.mux.Lock()
			defer u.mux.Unlock()
			if len(u.unknownCallersMap) <= 100 {
				u.unknownCallersMap[x5u] = true
			}
		}()
	}
}
