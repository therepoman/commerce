package cert

import (
	"context"
	"crypto/ecdsa"
	"crypto/x509"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"sync"
	"time"

	logging "code.justin.tv/amzn/TwitchLogging"
	"code.justin.tv/amzn/TwitchS2S2DistributedIdentitiesCallee/internal/opwrap"
	"code.justin.tv/amzn/TwitchS2S2DistributedIdentitiesCallee/internal/s2s2err"
	"code.justin.tv/amzn/TwitchS2S2DistributedIdentitiesCallee/internal/service"
	"code.justin.tv/video/metrics-middleware/v2/operation"
)

const certificateRefreshRate = time.Hour

// ErrX5UNotInCache is returned when a x5u is not cached
type ErrX5UNotInCache struct {
	X5U string
}

func (err ErrX5UNotInCache) Error() string {
	return fmt.Sprintf("x5u not in cache: %s", err.X5U)
}

// ErrServiceStageNotFound is returned when an object is not found in the
// certificate store
type ErrServiceStageNotFound struct {
	CertificateStoreOrigin string
	Service                string
	Stage                  string
	URL                    string
}

func (err ErrServiceStageNotFound) Error() string {
	return fmt.Sprintf(
		"service with name=%s and stage=%s is not found in %s",
		err.Service, err.Stage, err.URL)
}

// ErrUnknown is returned when CloudFront returns an error we don't recognize
type ErrUnknown struct {
	StatusCode int
	Message    string
}

func (err ErrUnknown) Error() string {
	return fmt.Sprintf(
		"unknown error with status code<%d>: %s",
		err.StatusCode,
		err.Message,
	)
}

// HTTPClient is the interface for HTTPClient we require
type HTTPClient interface {
	Do(*http.Request) (*http.Response, error)
}

// New returns a new certificate store
func New(
	httpClient HTTPClient,
	certificateStoreOrigin url.URL,
	certificateStoreServicesDomain string,
	os *operation.Starter,
	logger logging.Logger,
	foregroundRefreshRateLimit time.Duration,
	unknownCallerLoggerRateLimiter time.Duration,
) *Certificates {
	rateLimiter := make(<-chan time.Time)
	if unknownCallerLoggerRateLimiter > 0 {
		rateLimiter = time.NewTicker(unknownCallerLoggerRateLimiter).C
	}

	certs := &Certificates{
		httpClient:                     httpClient,
		certificates:                   make(map[string]*certificate),
		certificateStoreOrigin:         certificateStoreOrigin,
		certificateStoreServicesDomain: certificateStoreServicesDomain,
		logger:                         logger,
		operationStarter:               os,
		foregroundRefreshRateLimiter:   time.NewTicker(foregroundRefreshRateLimit),
		unknownCallerLogger: &unknownCallerLogger{
			logger:                         logger,
			unknownCallersMap:              make(map[string]bool, 100),
			UnknownCallerLoggerRateLimiter: rateLimiter,
		},
	}

	return certs
}

// Certificates retrieves certificates from CloudFront by x5u.
type Certificates struct {
	httpClient                     HTTPClient
	certificatesLock               sync.RWMutex
	rootCertificates               *x509.CertPool
	certificates                   map[string]*certificate
	certificateStoreOrigin         url.URL
	certificateStoreServicesDomain string
	operationStarter               *operation.Starter
	logger                         logging.Logger
	// ticker controlling the foreground refresh of certificates
	foregroundRefreshRateLimiter *time.Ticker
	// last time certificates were refreshed
	lastRefresh         time.Time
	unknownCallerLogger *unknownCallerLogger
}

// LoadRootCertificatePool loads the root certificate pool
func (c *Certificates) LoadRootCertificatePool(ctx context.Context) (err error) {
	ctx, op := c.operationStarter.StartOp(ctx, opwrap.LoadRootCertificate)
	defer opwrap.EndWithError(op, err)

	return c.loadRootCertificatePool(ctx)
}

// same as LoadRootCertificatePool except we report the metric as background
func (c *Certificates) loadRootCertificatePoolInBackground(ctx context.Context) (err error) {
	ctx, op := c.operationStarter.StartOp(ctx, opwrap.LoadRootCertificateInBackground)
	defer opwrap.EndWithError(op, err)

	return c.loadRootCertificatePool(ctx)
}

func (c *Certificates) loadRootCertificatePool(ctx context.Context) (err error) {
	req, err := http.NewRequest(http.MethodGet, c.certificateStoreOrigin.String(), nil)
	if err != nil {
		return err
	}

	res, err := c.httpClient.Do(req)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	switch res.StatusCode {
	case http.StatusOK:
	default:
		return c.handleHTTPError(res)
	}

	var keySet struct {
		Keys []certificate `json:"keys"`
	}
	if err := json.NewDecoder(res.Body).Decode(&keySet); err != nil {
		return err
	}

	c.certificatesLock.Lock()
	defer c.certificatesLock.Unlock()

	certPool := x509.NewCertPool()
	for _, key := range keySet.Keys {
		certPool.AddCert(key.Certificate)
	}
	c.rootCertificates = certPool

	return nil
}

// WhitelistService adds a service to our certificate store
func (c *Certificates) WhitelistService(ctx context.Context, service, stage string) (err error) {
	ctx, op := c.operationStarter.StartOp(ctx, opwrap.GetServiceCertificate)
	defer opwrap.EndWithError(op, err)
	return c.loadServiceCertificates(ctx, service, stage)
}

// same as LoadServiceCertificates except metric is reported as InBackground
func (c *Certificates) loadServiceCertificatesInBackground(ctx context.Context, service, stage string) (err error) {
	ctx, op := c.operationStarter.StartOp(ctx, opwrap.GetServiceCertificateInBackground)
	defer opwrap.EndWithError(op, err)
	return c.loadServiceCertificates(ctx, service, stage)
}

func (c *Certificates) loadServiceCertificates(ctx context.Context, service, stage string) (err error) {
	url := c.certificateStoreOrigin
	url.Path = strings.Join([]string{c.certificateStoreServicesDomain, service, stage + ".json"}, "/")
	req, err := http.NewRequest(http.MethodGet, url.String(), nil)
	if err != nil {
		return err
	}

	res, err := c.httpClient.Do(req)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	switch res.StatusCode {
	case http.StatusOK:
	case http.StatusNotFound:
		return s2s2err.NewError(s2s2err.CodeServiceStageNotFound, &ErrServiceStageNotFound{
			CertificateStoreOrigin: c.certificateStoreOrigin.String(),
			Service:                service,
			Stage:                  stage,
			URL:                    url.String(),
		})
	default:
		return c.handleHTTPError(res)
	}

	var keySet struct {
		Keys []certificate `json:"keys"`
	}
	if err := json.NewDecoder(res.Body).Decode(&keySet); err != nil {
		return err
	}

	c.certificatesLock.Lock()
	defer c.certificatesLock.Unlock()

	for _, key := range keySet.Keys {
		if _, err := key.Certificate.Verify(x509.VerifyOptions{
			DNSName: key.Certificate.Subject.CommonName,
			Roots:   c.rootCertificates,
			KeyUsages: []x509.ExtKeyUsage{
				x509.ExtKeyUsageClientAuth,
			},
		}); err != nil {
			return fmt.Errorf("failed to verify certificates for %s/%s: %w", service, stage, err)
		}

		c.certificates[key.KeyID] = &key
	}

	c.lastRefresh = time.Now()

	return nil
}

// Get a public key by x509 url from CloudFront and the service it represents.
func (c *Certificates) Get(ctx context.Context, x5u string) (_ *service.Service, _ *ecdsa.PublicKey, err error) {
	c.checkRefreshInForeground(ctx)

	c.certificatesLock.RLock()
	defer c.certificatesLock.RUnlock()

	ctx, op := c.operationStarter.StartOp(ctx, opwrap.GetServiceCertificate)
	defer opwrap.EndWithError(op, err)

	cert, ok := c.certificates[x5u]
	if !ok {
		c.unknownCallerLogger.LogUnknownCaller(x5u)
		return nil, nil, s2s2err.NewError(s2s2err.CodeX5UNotInCache, &ErrX5UNotInCache{X5U: x5u})
	}

	if err := cert.Validate(); err != nil {
		return nil, nil, err
	}

	return cert.Service, cert.PublicKey, nil
}

// Refresh certificates in our cache
func (c *Certificates) Refresh(ctx context.Context) error {
	if err := c.loadRootCertificatePoolInBackground(ctx); err != nil {
		return err
	}

	if errs := c.forEachServiceStage(func(service, stage string) error {
		return c.loadServiceCertificatesInBackground(ctx, service, stage)
	}); len(errs) > 0 {
		return errs[0]
	}

	c.lastRefresh = time.Now()

	return nil
}

func (c *Certificates) checkRefreshInForeground(ctx context.Context) {
	if !time.Now().After(c.lastRefresh.Add(certificateRefreshRate)) {
		return
	}

	select {
	case <-c.foregroundRefreshRateLimiter.C:
	default:
		return
	}

	if err := c.loadRootCertificatePool(ctx); err != nil {
		c.logger.Log("S2S2DistributedIdentitiesError", "err", err.Error())
		return
	}

	if errs := c.forEachServiceStage(func(service, stage string) error {
		return c.loadServiceCertificates(ctx, service, stage)
	}); len(errs) > 0 {
		for _, err := range errs {
			c.logger.Log("S2S2DistributedIdentitiesError", "err", err.Error())
		}
		return
	}

	c.lastRefresh = time.Now()
}

func (c *Certificates) forEachServiceStage(fn func(service, stage string) error) []error {
	certsToRefresh := make(map[struct {
		Name  string
		Stage string
	}]interface{})

	func() {
		c.certificatesLock.RLock()
		defer c.certificatesLock.RUnlock()

		for _, cert := range c.certificates {
			certsToRefresh[struct {
				Name  string
				Stage string
			}{
				Name:  cert.Service.Name,
				Stage: cert.Service.Stage,
			}] = nil
		}
	}()

	workerPool := make(chan interface{}, 16)
	workerErrs := make(chan error)

	for svc := range certsToRefresh {
		workerPool <- nil
		go func() {
			defer func() {
				<-workerPool
			}()
			workerErrs <- fn(svc.Name, svc.Stage)
		}()
	}

	errs := make([]error, 0)
	for n := 0; n < len(certsToRefresh); n++ {
		if err := <-workerErrs; err != nil {
			errs = append(errs, err)
		}
	}

	return errs
}

func (c *Certificates) handleHTTPError(res *http.Response) error {
	message, err := ioutil.ReadAll(res.Body)
	if err != nil {
		message = []byte("unable to read body")
	}
	return &ErrUnknown{
		StatusCode: res.StatusCode,
		Message:    string(message),
	}
}
