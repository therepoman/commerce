package s2s2dicallee

import "context"

type contextKey string

const authenticatedSubjectContextKey = contextKey("RequestSubject")

// SetRequestSubject sets the authorized subject in the request scope.
func SetRequestSubject(ctx context.Context, s AuthenticatedSubject) context.Context {
	return context.WithValue(ctx, authenticatedSubjectContextKey, s)
}

// RequestSubject retrieves the authorized subject
func RequestSubject(ctx context.Context) (AuthenticatedSubject, bool) {
	if value, ok := ctx.Value(authenticatedSubjectContextKey).(AuthenticatedSubject); ok {
		return value, true
	}
	return nil, false
}
