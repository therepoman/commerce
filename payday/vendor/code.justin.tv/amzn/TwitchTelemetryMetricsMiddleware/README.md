# `TwitchTelemetryMetricsMiddleware`

This package represents a middleware that builds upon [Twitch-Video-MetricsMiddleware](https://code.amazon.com/packages/Twitch-Video-MetricsMiddleware/trees/mainline)
to allow for automatic metrics when a service's APIs are called or when that service calls an AWS or Twirp dependency.

We strongly encourage reading [How MetricsMiddleware Works](https://docs.fulton.twitch.a2z.com/docs/metrics.html#how-metricsmiddleware-works)
to learn more about this process.

## Referring to this package
To refer to this dependency, refer to its alias of `code.justin.tv/amzn/TwitchTelemetryMetricsMiddleware`

## Help
For help using this package, please ping the [#fulton](https://twitch.slack.com/messages/C9BUPDUC8) Slack channel