# `TwitchProcessIdentifier`

This package defines the default ProcessIdentifiers that are used throughout Fulton services for configs, logging,
and metrics. The original [Process Identifier PR FAQ](https://docs.google.com/document/d/1Oc0-Ut6mt1EKw46Iha2Tupe2rHJo3DsoJ7kw9dEvOUE/edit#heading=h.ysbnou8awcl1)
was used to construct this definition.

## Referring to this package
To refer to this dependency, refer to its alias of `code.justin.tv/amzn/TwitchProcessIdentifier`

## Help
For help using this package, please ping the [#fulton](https://twitch.slack.com/messages/C9BUPDUC8) Slack channel