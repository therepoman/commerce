package es256

// SigningKey represents a private key to sign JWTs with.
type SigningKey interface {
	Sign(value []byte) ([]byte, error)
	Validate(value, signature []byte) error
	Size() int
	Name() string
	KeyID() string
}
