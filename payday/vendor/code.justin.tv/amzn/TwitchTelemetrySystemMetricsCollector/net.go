package agent

import (
	"code.justin.tv/amzn/TwitchLogging"
	"code.justin.tv/amzn/TwitchTelemetry"
	"github.com/shirou/gopsutil/net"
	"math"
)

// NetPollingJob collects system network metrics
type NetPollingJob struct {
	lastIOCountersStats map[string]net.IOCountersStat
	logger              logging.Logger
}

// NewNetPollingJob returns a new networking polling job
func NewNetPollingJob(l logging.Logger) *NetPollingJob {
	return &NetPollingJob{
		lastIOCountersStats: make(map[string]net.IOCountersStat),
		logger:              l,
	}
}

// Fetch collects system networking metrics and returns them as TwitchTelemetry samples
func (npj *NetPollingJob) Fetch() ([]*telemetry.Sample, error) {
	curIOCountersStats, err := net.IOCounters(true)
	if err != nil {
		return nil, err
	}

	samples := []*telemetry.Sample{}
	for _, curIOCountersStat := range curIOCountersStats {
		lastIOCounterStat, found := npj.lastIOCountersStats[curIOCountersStat.Name]
		if found {
			samples = append(samples, netIfaceSamplesFromDiff(curIOCountersStat, lastIOCounterStat)...)
		}
		npj.lastIOCountersStats[curIOCountersStat.Name] = curIOCountersStat
	}

	return samples, nil
}

func baseNetIfaceSample(metricName, ifaceName string, value float64, unit string) *telemetry.Sample {
	metricID := telemetry.NewMetricID(metricName)
	metricID.AddDimension("Interface", ifaceName)
	return &telemetry.Sample{
		MetricID: *metricID,
		// Ensure we only return positive numbers
		Value: math.Max(value, 0.0),
		Unit:  unit,
	}
}

func netIfaceSamplesFromDiff(curStat, prevStat net.IOCountersStat) []*telemetry.Sample {
	diffStat := net.IOCountersStat{}

	ifaceName := curStat.Name
	samples := []*telemetry.Sample{}

	// generate diffs
	diffStat.BytesSent = curStat.BytesSent - prevStat.BytesSent
	diffStat.BytesRecv = curStat.BytesRecv - prevStat.BytesRecv
	diffStat.PacketsSent = curStat.PacketsSent - prevStat.PacketsSent
	diffStat.PacketsRecv = curStat.PacketsRecv - prevStat.PacketsRecv

	// generate samples
	samples = append(samples, baseNetIfaceSample("NetBytesSent", ifaceName, float64(diffStat.BytesSent), telemetry.UnitBytes))
	samples = append(samples, baseNetIfaceSample("NetBytesRecv", ifaceName, float64(diffStat.BytesRecv), telemetry.UnitBytes))
	samples = append(samples, baseNetIfaceSample("NetPacketsSent", ifaceName, float64(diffStat.PacketsSent), telemetry.UnitCount))
	samples = append(samples, baseNetIfaceSample("NetPacketsRecv", ifaceName, float64(diffStat.PacketsRecv), telemetry.UnitCount))
	return samples
}
