package agent

import (
	"github.com/shirou/gopsutil/load"

	logging "code.justin.tv/amzn/TwitchLogging"
	telemetry "code.justin.tv/amzn/TwitchTelemetry"
	"math"
)

// LoadPollingJob collects load average data
type LoadPollingJob struct {
	logger logging.Logger
}

// NewLoadPollingJob returns a new LoadPollingJob
func NewLoadPollingJob(l logging.Logger) *LoadPollingJob {
	return &LoadPollingJob{
		logger: l,
	}
}

// Fetch returns data about load averages on the system
func (lpj *LoadPollingJob) Fetch() ([]*telemetry.Sample, error) {
	loadAvgStat, err := load.Avg()
	if err != nil {
		return nil, err
	}
	samples := []*telemetry.Sample{}
	samples = append(samples, baseLoadSample("Load1", loadAvgStat.Load1))
	samples = append(samples, baseLoadSample("Load5", loadAvgStat.Load5))
	samples = append(samples, baseLoadSample("Load15", loadAvgStat.Load15))
	return samples, nil
}

func baseLoadSample(metricName string, value float64) *telemetry.Sample {
	metricID := telemetry.NewMetricID(metricName)
	return &telemetry.Sample{
		MetricID: *metricID,
		// Ensure we only return positive numbers
		Value: math.Max(value, 0.0),
		Unit:  telemetry.UnitCount,
	}
}
