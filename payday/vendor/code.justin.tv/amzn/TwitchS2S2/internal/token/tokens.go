package token

import (
	"context"
)

// NewOptions returns an options object
func NewOptions() *Options { return &Options{} }

// Options represents token refresh options
type Options struct {
	scope Scope
	host  string

	// Don't use cache for this request
	NoCache bool

	// This success must succeed - don't use aggressive timeouts
	MustSucceed bool
}

// WithHost returns a Options object with an audience host.
func (o *Options) WithHost(host string) *Options {
	o.host = host
	return o
}

// WithScope returns a Options object with a requesting scope.
func (o *Options) WithScope(s Scope) *Options {
	o.scope = s
	return o
}

// Scope returns the scope
func (o Options) Scope() Scope {
	return o.scope
}

// Host returns the host
func (o Options) Host() string {
	return o.host
}

// Tokens returns tokens
type Tokens interface {
	Token(ctx context.Context, options *Options) (*Token, error)
}
