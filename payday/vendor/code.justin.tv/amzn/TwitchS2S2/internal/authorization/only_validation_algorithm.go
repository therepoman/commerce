package authorization

import (
	"errors"

	"code.justin.tv/amzn/TwitchS2S2/internal/oidc"
)

// onlyValidationAlgorithm to satify jwt.Algorithm's expected interface.
type onlyValidationAlgorithm struct {
	oidc.ValidationKey
}

func (onlyValidationAlgorithm) Sign([]byte) ([]byte, error) {
	return nil, errors.New("not impemented")
}
