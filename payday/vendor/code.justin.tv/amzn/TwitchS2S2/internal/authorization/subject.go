package authorization

// NewSubject returns a new subject object.
func NewSubject(subjectID string) Subject {
	return &subject{id: subjectID}
}

// Subject is the subject on an authorization. This will always be the caller
// service.
type Subject interface {
	ID() string
}

type subject struct {
	id string
}

func (s *subject) ID() string { return s.id }
