job {
	name 'release-trace'
	using 'TEMPLATE-autobuild'
	scm {
		git {
			remote {
				github 'release/trace', 'ssh', 'git-aws.internal.justin.tv'
				credentials 'git-aws-read-key'
			}
			clean true
		}
	}
	steps {
		shell 'rm -rf .manta/'
		shell 'manta -v'
		shell '''
set -e
export AWS_PROFILE=core_deploy_artifact
export AWS_SDK_LOAD_CONFIG=1

BINGO_CHANNEL=stable
CONSUL_URL=https://consul.internal.justin.tv/v1/kv/deployed-version/video/bingo/$BINGO_CHANNEL
BINGO_SHA_RAW=$(curl -s ${CONSUL_URL})
BINGO_SHA=$(echo $BINGO_SHA_RAW | python -c 'import sys, json, base64; print base64.decodestring(json.load(sys.stdin)[0]["Value"])')
aws s3 cp s3://core-devtools-deploy-artifact/video/bingo/${BINGO_SHA}.tgz ./bingo.tgz

tar -xzf ./bingo.tgz
./bingo build --container
./bingo test --container
cp -r ./bingo-out ./.manta/bingo-out
'''
		saveDeployArtifact 'release/trace', '.manta'
	}
}

job {
	name 'release-trace-deploy'
	using 'TEMPLATE-deploy'
	wrappers {
		credentialsBinding {
			file('AWS_SHARED_CREDENTIALS_FILE_TRACE_CODEDEPLOY', '37ec8b93-5d2a-4eaa-8aff-9449d1f74355')
			file('AWS_CONFIG_FILE', 'aws_config')
			string 'dta_tools_deploy', 'dta_tools_deploy'
		}
	}
	steps {
		downloadDeployArtifact 'release/trace'
		shell '''
set -e
export AWS_SHARED_CREDENTIALS_FILE="${AWS_SHARED_CREDENTIALS_FILE_TRACE_CODEDEPLOY}"
if [ -x ./bin/build_index ]; then
	export APP="build_index"
	./bin/deploy -v -region=us-west-2 -function-name=${APP} -handler-name=${APP} -handler-file=./bin/${APP} -alias=${ENVIRONMENT} -deploy-app-name=${APP} -deploy-target-group=${ENVIRONMENT}
fi
'''
		shell 'echo "HOSTS=${HOSTS}" ; hosts="" ; for host in $(echo "${HOSTS}" | tr "," " ") ; do if ! host "$host" &>/dev/null; then consulname="$(echo "${host}" | sed -n -e "s/^\\(.*\\)\\.\\([^\\.]*\\)\\.justin\\.tv$/\\1/p")" ; consuldc="$(echo "${host}" | sed -n -e "s/^\\(.*\\)\\.\\([^\\.]*\\)\\.justin\\.tv$/\\2/p")" ; host="$(curl -o - -s "http://consul.internal.justin.tv/v1/catalog/node/${consulname}?dc=${consuldc}" | python -mjson.tool | sed -n -e "s/.*\\"Address\\":[ ]*\\"\\([0-9][0-9\\.]*\\)\\".*/\\1/p" | sed 1q)" ; fi ; hosts="${hosts:+$hosts,}${host}" ; done ; echo "HOSTS=${hosts}" ; export HOSTS="${hosts}" ; courier deploy --repo release/trace --dir /opt/twitch/trace'
	}
}
