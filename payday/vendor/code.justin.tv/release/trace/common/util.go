package common

func IsRequestKind(k Kind) bool {
	switch k {
	case Kind_REQUEST_HEAD_PREPARED,
		Kind_REQUEST_BODY_SENT,
		Kind_REQUEST_HEAD_RECEIVED,
		Kind_REQUEST_BODY_RECEIVED:
		return true
	default:
		return false
	}
}

func IsResponseKind(k Kind) bool {
	switch k {
	case Kind_RESPONSE_HEAD_PREPARED,
		Kind_RESPONSE_BODY_SENT,
		Kind_RESPONSE_HEAD_RECEIVED,
		Kind_RESPONSE_BODY_RECEIVED:
		return true
	default:
		return false
	}
}

func IsClientKind(k Kind) bool {
	switch k {
	case Kind_REQUEST_HEAD_PREPARED,
		Kind_REQUEST_BODY_SENT,
		Kind_RESPONSE_HEAD_RECEIVED,
		Kind_RESPONSE_BODY_RECEIVED:
		return true
	default:
		return false
	}
}

func IsServerKind(k Kind) bool {
	switch k {
	case Kind_REQUEST_HEAD_RECEIVED,
		Kind_REQUEST_BODY_RECEIVED,
		Kind_RESPONSE_HEAD_PREPARED,
		Kind_RESPONSE_BODY_SENT:
		return true
	default:
		return false
	}
}
