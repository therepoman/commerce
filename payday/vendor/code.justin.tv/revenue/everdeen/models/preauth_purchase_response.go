package models

// PreauthPurchaseResponse will be published to SNS topic so that the caller can respond to the async request
type PreauthPurchaseResponse struct {
	// RequestID received from CreatePreauthorizedPurchase API and will return this via SNS at the end of job
	RequestID     string `json:"request_id"`
	Status        int    `json:"status"`
	ReasonCode    int    `json:"reason_code"`
	Message       string `json:"message"`
	UserID        string `json:"user_id"`
	TransactionID string `json:"transaction_id"`
}

func NewPreauthPurchaseResponse(requestID string, code PreauthPurchaseReasonCode, userID string, txnID string) PreauthPurchaseResponse {
	resp := PreauthPurchaseResponse{
		RequestID:     requestID,
		Status:        code.Status(),
		ReasonCode:    code.Value(),
		Message:       code.Message(),
		UserID:        userID,
		TransactionID: txnID,
	}

	return resp
}
