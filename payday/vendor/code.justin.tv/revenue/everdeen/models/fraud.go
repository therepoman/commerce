package models

const (
	CheckpointAutoRefillCheckout = "auto_refill_checkout"
	ActionExceededVelocityLimits = "EXCEEDED_VELOCITY_LIMITS"
	ActionAuthorizedUser         = "AUTHORIZED_USER"
)
