package models

type CompletePurchaseEvent struct {
	IsPurchaseSuccess     bool   `json:"is_purchase_success"`
	DeviceID              string `json:"device_id,omitempty"`
	LocalStorageDeviceID  string `json:"localstorage_device_id,omitempty"`
	TabSessionID          string `json:"tab_session_id,omitempty"`
	PageSessionID         string `json:"page_session_id,omitempty"`
	CheckoutSessionID     string `json:"checkout_session_id,omitempty"`
	SessionIPAddress      string `json:"session_ip_address,omitempty"`
	SessionCountryCode    string `json:"session_country_code,omitempty"`
	SessionCity           string `json:"session_city,omitempty"`
	SessionPostalCode     string `json:"session_postal_code,omitempty"`
	SessionIsAnonymous    bool   `json:"session_is_anonymous"`
	IsGift                bool   `json:"is_gift"`
	IsAnon                bool   `json:"is_anon"`
	MysteryGiftCount      int    `json:"mystery_gift_count,omitempty"`
	PaymentProvider       string `json:"payment_provider,omitempty"`
	PaymentGateway        string `json:"payment_gateway,omitempty"`
	PaymentMethod         string `json:"payment_method,omitempty"`
	PaymentToken          string `json:"payment_token,omitempty"`
	PaymentCardBinCountry string `json:"payment_card_bin_country,omitempty"`
	Recipient             string `json:"recipient,omitempty"`
	ProductID             string `json:"product_id,omitempty"`
	ExtProductID          string `json:"ext_product_id,omitempty"`
	PurchaserID           string `json:"purchaser_id,omitempty"`
	PriceID               string `json:"price_id,omitempty"`
	AmountInCents         string `json:"amount_in_cents,omitempty"`
	Currency              string `json:"currency,omitempty"`
	PurchaseOrderOriginID string `json:"purchase_order_origin_id,omitempty"`
	PurchaseOrderID       string `json:"purchase_order_id,omitempty"`
}
