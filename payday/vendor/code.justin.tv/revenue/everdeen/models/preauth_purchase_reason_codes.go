package models

import (
	"strconv"

	"github.com/twitchtv/twirp"
)

// PreauthorizedPurchase Lambda Step Function error models

// WARNING!!: Do not change the order of these error codes.
// The order maps to the reason code values, messages, and error codes that clients are
// currently consuming!
// TODO: Make this resilient to such errors (PAY-5236)
// WARNING!!

type PreauthPurchaseReasonCode int

const (
	// Success in purchasing and entitlement
	ReasonSuccess PreauthPurchaseReasonCode = iota

	// Reason entitlement failure
	ReasonEntitleFailed PreauthPurchaseReasonCode = iota

	// Reason card declined
	ReasonCardDeclined PreauthPurchaseReasonCode = iota

	// Reason invalid arguments
	ReasonInvalidArguments PreauthPurchaseReasonCode = iota

	// Reason fraud declined
	ReasonFraudDeclined PreauthPurchaseReasonCode = iota

	// Reason velocity limit reached
	ReasonVelocityLimit PreauthPurchaseReasonCode = iota

	// Reason 2FA not enabled
	ReasonTwoFADisabled PreauthPurchaseReasonCode = iota

	// Reason unsupported payment instrument e.g. credit card is not supported for Bits Autorefill at launch
	ReasonInvalidPaymentInstrument PreauthPurchaseReasonCode = iota

	// Reason system error
	ReasonSystemError PreauthPurchaseReasonCode = iota

	// Reason ineligible purchase
	ReasonIneligiblePurchase PreauthPurchaseReasonCode = iota
)

func (reason PreauthPurchaseReasonCode) Value() int {
	var values = [...]int{
		2000, // ReasonSuccess
		5001, // ReasonEntitleFailed
		4001, // ReasonCardDeclined
		4000, // ReasonInvalidArguments
		4002, // ReasonFraudDeclined
		4003, // ReasonVelocityLimit
		4004, // ReasonTwoFADisabled
		4005, // ReasonInvalidPaymentInstrument
		5000, // ReasonSystemError
		4006, // ReasonIneligiblePurchase
	}
	return values[reason]
}

func (reason PreauthPurchaseReasonCode) Message() string {
	var values = [...]string{
		"entitlement success",        // ReasonSuccess
		"entitlement failure",        // ReasonEntitleFailed
		"card decline",               // ReasonCardDeclined
		"invalid arguments",          // ReasonInvalidArguments
		"fraud decline",              // ReasonFraudDeclined
		"velocity limit exceeded",    // ReasonVelocityLimit
		"two factor disabled",        // ReasonTwoFADisabled
		"invalid payment instrument", // ReasonInvalidPaymentInstrument
		"system error",               // ReasonSystemError
		"ineligible purchase",        // ReasonIneligiblePurchase
	}
	return values[reason]
}

func (reason PreauthPurchaseReasonCode) Status() int {
	var values = [...]int{
		200, // ReasonSuccess
		500, // ReasonEntitleFailed
		400, // ReasonCardDeclined
		400, // ReasonInvalidArguments
		400, // ReasonFraudDeclined TODO: Change to 403 after coordinating with https://git-aws.internal.justin.tv/commerce/payday/blob/16d1551809a35cc902c08cf4e90c8f178a736705/sqs/handlers/autorefill/autorefill_response.go#L49
		400, // ReasonVelocityLimit TODO: Change to 403
		400, // ReasonTwoFADisabled TODO: Change to 412
		400, // ReasonInvalidPaymentInstrument
		500, // ReasonSystemError
		400, // ReasonIneligiblePurchase
	}
	return values[reason]
}

func (reason PreauthPurchaseReasonCode) TwirpError() twirp.Error {
	var values = [...]twirp.ErrorCode{
		twirp.NoError,            // ReasonSuccess
		twirp.Internal,           // ReasonEntitleFailed
		twirp.InvalidArgument,    // ReasonCardDeclined
		twirp.InvalidArgument,    // ReasonInvalidArguments
		twirp.ResourceExhausted,  // ReasonFraudDeclined
		twirp.ResourceExhausted,  // ReasonVelocityLimit
		twirp.FailedPrecondition, // ReasonTwoFADisabled
		twirp.InvalidArgument,    // ReasonInvalidPaymentInstrument
		twirp.Internal,           // ReasonSystemError
		twirp.FailedPrecondition, // ReasonIneligiblePurchase
	}

	reasonCodeStr := strconv.Itoa(reason.Value())
	return twirp.NewError(values[reason], reason.Message()).WithMeta("reasonCode", reasonCodeStr)
}
