package models

import (
	model "code.justin.tv/revenue/payments-service-go-client/client"
)

// PreauthorizedPurchase Lambda Step Function inputs and outputs

type StartPreauthPurchaseInput struct {
	// Bool to help branch between pre / post Chevron codepaths.
	IsChevronFlow bool `json:"is_chevron_flow"`

	// Order ID
	PurchaseOrderOriginID string `json:"purchase_order_origin_id"`

	// Payment Session data
	PaymentSession *model.PaymentSession `json:"payment_session"`

	// Purchase order payment ext_transaction_id, if present then should not charge user and just go to entitlement
	ExtTransactionID string `json:"ext_transaction_id"`

	// Purchaser user ID
	UserID string `json:"user_id"`

	// Received from CreatePreauthorizedPurchase API and will return this via SNS at the end of job
	RequestID string `json:"request_id"`

	Platform string `json:"platform"`

	// TWITCH_PAYMENT_INSTRUMENT
	PaymentInstrumentCategory string `json:"payment_instrument_category"`

	// Payment method ID in payments DB
	PaymentInstrumentID string `json:"payment_instrument_id"`

	// Offer ID to get product and pricing
	OfferID     string            `json:"offer_id"`
	TagBindings map[string]string `json:"tag_bindings"`
}

type StartPreauthPurchaseOutput struct {
	// Bool to help branch between pre / post Chevron codepaths.
	IsChevronFlow bool `json:"is_chevron_flow"`

	// Order ID
	PurchaseOrderOriginID string `json:"purchase_order_origin_id"`

	// Payment Session data
	PaymentSession *model.PaymentSession `json:"payment_session"`

	// Purchase order payment ext_transaction_id, if present then should not charge user and just go to entitlement
	ExtTransactionID string `json:"ext_transaction_id"`

	// Purchaser user ID
	UserID string `json:"user_id"`

	// Received from CreatePreauthorizedPurchase API and will return this via SNS at the end of job
	RequestID string `json:"request_id"`

	Platform string `json:"platform"`

	// TWITCH_PAYMENT_INSTRUMENT
	PaymentInstrumentCategory string `json:"payment_instrument_category"`

	// Payment method ID in payments DB
	PaymentInstrumentID string `json:"payment_instrument_id"`

	// Offer ID to get product and pricing
	OfferID     string            `json:"offer_id"`
	TagBindings map[string]string `json:"tag_bindings"`

	StateResponse StateResponse `json:"state_response"`
}

type ChargePreauthPurchaseInput struct {
	// Bool to help branch between pre / post Chevron codepaths.
	IsChevronFlow bool `json:"is_chevron_flow"`

	// Order ID
	PurchaseOrderOriginID string `json:"purchase_order_origin_id"`

	// Payment Session data
	PaymentSession *model.PaymentSession `json:"payment_session"`

	// Purchaser user ID
	UserID string `json:"user_id"`

	// Received from CreatePreauthorizedPurchase API and will return this via SNS at the end of job
	RequestID string `json:"request_id"`

	Platform string `json:"platform"`

	// TWITCH_PAYMENT_INSTRUMENT
	PaymentInstrumentCategory string `json:"payment_instrument_category"`

	// Payment method ID in payments DB
	PaymentInstrumentID string `json:"payment_instrument_id"`

	// Offer ID to get product and pricing
	OfferID     string            `json:"offer_id"`
	TagBindings map[string]string `json:"tag_bindings"`
}

type ChargePreauthPurchaseOutput struct {
	// Bool to help branch between pre / post Chevron codepaths.
	IsChevronFlow bool `json:"is_chevron_flow"`

	// Order ID
	PurchaseOrderOriginID string `json:"purchase_order_origin_id"`

	// Payment Session data
	PaymentSession *model.PaymentSession `json:"payment_session"`

	// Offer data
	OfferID     string            `json:"offer_id"`
	TagBindings map[string]string `json:"tag_bindings"`

	// Entitle path
	ExtTransactionID string `json:"ext_transaction_id"`
	PaymentProvider  string `json:"payment_provider"`
	UserID           string `json:"user_id"`
	RequestID        string `json:"request_id"`
	ProductID        string `json:"product_id"`
	Platform         string `json:"platform"`
	Price            int    `json:"price"`
	Currency         string `json:"currency"`

	StateResponse StateResponse `json:"state_response"`
}

type EntitlePreauthPurchaseInput struct {
	// Bool to help branch between pre / post Chevron codepaths.
	IsChevronFlow bool `json:"is_chevron_flow"`

	// Order ID
	PurchaseOrderOriginID string `json:"purchase_order_origin_id"`

	// Payment Session data
	PaymentSession *model.PaymentSession `json:"payment_session"`

	// Offer data
	OfferID     string            `json:"offer_id"`
	TagBindings map[string]string `json:"tag_bindings"`

	ExtTransactionID string `json:"ext_transaction_id"`
	UserID           string `json:"user_id"`
	RequestID        string `json:"request_id"`

	StateResponse StateResponse `json:"state_response"`
}

type EntitlePreauthPurchaseOutput struct {
	// Bool to help branch between pre / post Chevron codepaths.
	IsChevronFlow bool `json:"is_chevron_flow"`

	// Order ID
	PurchaseOrderOriginID string `json:"purchase_order_origin_id"`

	StateResponse StateResponse `json:"state_response"`
}

type RefundPreauthPurchaseInput struct {
	// Bool to help branch between pre / post Chevron codepaths.
	IsChevronFlow bool `json:"is_chevron_flow"`

	// Order ID
	PurchaseOrderOriginID string `json:"purchase_order_origin_id"`

	// Purchase order payment ext_transaction_id, if not present then no-op
	ExtTransactionID string `json:"ext_transaction_id"`

	// Log the request ID
	JSONPayload PreauthPurchaseResponse `json:"json_payload"`
}

type RefundPreauthPurchaseOutput struct {
	// Bool to help branch between pre / post Chevron codepaths.
	IsChevronFlow bool `json:"is_chevron_flow"`

	StateResponse StateResponse `json:"state_response"`
}

type PublishPreauthPurchaseInput struct {
	StateResponse StateResponse `json:"state_response"`
}

// StateResponse plumbs through fields for the state machine to use to guide the state transitions
type StateResponse struct {
	// Payload which can be published to SNS upon success/failure
	JSONPayload PreauthPurchaseResponse `json:"json_payload"`

	// Save the payment event to be published after the purchase is confirmed successful (publish_preauth_purchase Lambda)
	// This should be passed from charge_preauth_purchase -> entitle_preauth_purchase -> publish_preauth_purchase
	PaymentEventJSON string `json:"payment_event_json"`

	// Used to pass through to refund upon failure
	ExtTransactionID string `json:"ext_transaction_id"`

	// State machine booleans
	// Whether execution failed and we should refund + publish failure
	IsFailed bool `json:"is_failed"`

	// Used to make a choice whether we need to go to ChargePayment or EntitleProduct state
	IsUserCharged bool `json:"is_user_charged"`
}
