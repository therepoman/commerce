syntax = "proto3";
package code.justin.tv.revenue.mulan;
option go_package = "mulantwirp";
import "google/protobuf/timestamp.proto";
import "google/protobuf/wrappers.proto";

// Mulan server return user's payout type
service Mulan {
    rpc QueryPurchaseProfile (QueryPurchaseProfileRequest) returns (QueryPurchaseProfileResponse);
    rpc GetPurchaseProfilesByTicketIDs (GetPurchaseProfilesByTicketIDsRequest) returns (GetPurchaseProfilesByTicketIDsResponse);
    rpc GetSubscriptionsByIDs (GetSubscriptionsByIDsRequest) returns (GetSubscriptionsByIDsResponse);
    rpc GetUnacknowledgedSubscriptionEvents (GetUnacknowledgedSubscriptionEventsRequest) returns (GetUnacknowledgedSubscriptionEventsResponse);
    rpc GetPurchaseOrdersByIDs (GetPurchaseOrdersByIDsRequest) returns (GetPurchaseOrdersByIDsResponse);
    rpc GetPurchaseOrderPaymentsByIDs (GetPurchaseOrderPaymentsByIDsRequest) returns (GetPurchaseOrderPaymentsByIDsResponse);
    rpc GetActiveSubscriptionsByUserIDAndTenant (GetActiveSubscriptionsByUserIDAndTenantRequest) returns (GetActiveSubscriptionsByUserIDAndTenantResponse);
    rpc GetAggregatedPurchaseOrderAmounts (GetAggregatedPurchaseOrderAmountsRequest) returns (GetAggregatedPurchaseOrderAmountsResponse);

}

message GetPurchaseProfilesByTicketIDsRequest {
    repeated int64 ticket_ids = 1; //required
}

message GetPurchaseProfilesByTicketIDsResponse {
    map<int64, PurchaseProfile> purchase_profiles = 1;
}

message GetUnacknowledgedSubscriptionEventsRequest {
    int64 user_id = 1;
    Platform platform = 2;
}

message SubscriptionEvent {
    string ext_product_id = 1;
    string subscription_id = 2;
    SubscriptionState status = 3;
    string owner_id = 4;
    google.protobuf.Timestamp end_date = 5;
}

message GetUnacknowledgedSubscriptionEventsResponse {
    repeated SubscriptionEvent unacknowledgedEvents  = 1;
}

message GetSubscriptionsByIDsRequest {
    repeated string ids = 1; //required
}

message GetSubscriptionsByIDsResponse {
    map<string, Subscription> subscriptions = 1;
}

message GetPurchaseOrdersByIDsRequest {
    repeated string ids = 1; //required
}

message GetPurchaseOrdersByIDsResponse {
    map<string, Purchase> purchase_orders = 1;
}

message GetPurchaseOrderPaymentsByIDsRequest {
    repeated string ids = 1; //required
}

message GetPurchaseOrderPaymentsByIDsResponse {
    map<string, PurchaseOrderPayments> purchase_order_payments = 1;
}

// Can be PurchaseOrder
message Purchase {
    string id = 1;
    string payment_provider = 2;
    int64 purchaser_id = 3;
    string platform = 4;
    string product_id = 5;
    string ext_product_id = 6;
    bool is_refundable = 7;
    google.protobuf.Timestamp paid_on = 8;
    string state_deprecated = 9 [deprecated=true]; // Deprecated in favor of "OrderState state"
    google.protobuf.Timestamp renewal_date = 10;
    string tenant = 11;
    bool is_gift = 12;
    OrderState state = 13;
    string state_details = 14;
}

message PurchaseOrderPayments {
    repeated PurchaseOrderPayment purchase_order_payments = 1;
}

message PurchaseOrderPayment {
    string payment_gateway = 1;
    string ext_transaction_id = 2;
    int64 gross_amount_usd = 3;
    int64 fees_usd = 4;
    int64 taxes_usd = 5;
    int64 gross_amount = 6;
    int64 fees = 7;
    int64 taxes = 8;
    string currency = 9;
    string state = 10;
    string details = 11;
    string gateway = 12;
}

message QueryPurchaseProfileRequest {
    int64 id = 1;
    google.protobuf.Timestamp created_on_end = 2;
    google.protobuf.Timestamp created_on_start = 3;
    string ext_purchaser_id = 4;
    string ext_subscription_id = 5;
    google.protobuf.BoolValue paying = 6;
    repeated string payment_provider = 7;
    repeated int64 purchasable_id = 8;
    string purchaser_email = 9;
    int64 purchaser_id = 10;
    string purchaser_name = 11;
    string state = 12;
    repeated int64 ticket_owner_id = 13;
    repeated int64 ticket_product_id = 14;
    google.protobuf.Timestamp updated_on_end = 15;
    google.protobuf.Timestamp updated_on_start = 16;
    google.protobuf.BoolValue will_renew = 17;
    google.protobuf.BoolValue full_details = 18;
    int64 page = 19;
    string per_page = 20;
    repeated string ids = 21;
}

message QueryPurchaseProfileResponse {
    repeated PurchaseProfile purchase_profiles = 1;
}

// PurchaseProfile
message PurchaseProfile {
    int64 id = 1;
    string state = 2;
    string platform = 3;
    string payment_provider = 4;
    string ext_subscription_id = 5;
    string ext_purchaser_id = 6;
    int64 ticket_id = 7;
    int64 purchaser_id = 8;
    string purchaser_name = 9;
    string purchaser_email = 10;
    google.protobuf.Timestamp created_on = 11;
    google.protobuf.Timestamp updated_on = 12;
    bool will_renew = 13;
    google.protobuf.Timestamp purchase_datetime = 14;
    google.protobuf.Timestamp cancel_datetime = 15;
    google.protobuf.Timestamp do_not_renew_datetime = 16;
    bool is_paying = 17;
    bool is_recurring = 18;
    bool is_test = 19;
    string product_type = 20;
    google.protobuf.Timestamp last_payment_date = 21;
    google.protobuf.Timestamp paid_on = 22;
    google.protobuf.Timestamp expires_on = 23;
    google.protobuf.Timestamp renewal_date = 24;
    bool is_refundable = 25;
    bool is_expired = 26;
    bool is_gift = 27;
    int64 ticket_owner_id = 28 [deprecated=true];
    string ticket_product_id = 29;
}

message GetActiveSubscriptionsByUserIDAndTenantRequest {
    string tenant = 1;
    int64 user_id = 2;
}

message GetActiveSubscriptionsByUserIDAndTenantResponse {
    repeated Subscription subscriptions = 1;
}

message Price {
    string currency = 1;
}

// Subscription
message Subscription {
    string id = 1;
    SubscriptionState state = 2;
    string platform = 3;
    int64 user_id = 4;
    string product_id = 5;
    string ext_subscription_id = 6;
    string ext_product_id = 7;
    string payment_gateway = 8;
    string tenant = 9;
    google.protobuf.Timestamp expires_at = 10;
    google.protobuf.Timestamp created_at = 11;
    google.protobuf.Timestamp updated_at = 12;
    google.protobuf.Timestamp purchased_at = 13;
    google.protobuf.Timestamp renewal_date = 14;
    bool is_refundable = 15;
    string tenant_id = 16;
    Price price = 17;
}

enum SubscriptionState {
    UNKNOWN = 0;
    ACTIVE = 1;
    DNR_INITIATED = 2;
    WILL_NOT_RENEW = 3;
    CANCEL_INITIATED = 4;
    CANCELLED = 5;
    ON_HOLD = 6;
}

enum OrderState {
    UNKNOWN_ORDER_STATE = 0;
    INITIATED = 1;
    FULFILLMENT_INITIATED = 2;
    REFUND_INITIATED = 3;
    REFUND_APPLIED = 4;
    REFUND_FAILED = 5;
    FAILED = 6;
    SUCCESS = 7;
    PAYMENT_PENDING = 8;
    CANCEL_BENEFITS_INITIATED = 9;
    BENEFITS_CANCELLED = 10;
    THREE_D_SECURE_CHALLENGE_REQUIRED = 11;
}

enum Platform { 
    ANDROID = 0;
    IOS = 1;
    WEB = 2;
}

message GetAggregatedPurchaseOrderAmountsRequest {
    int64 purchaser_id = 1;
    google.protobuf.Timestamp created_after = 2;
}

message GetAggregatedPurchaseOrderAmountsResponse {
    map<string, AmountAggregations> order_aggregations = 1; //Aggregated data by product_types
}

message AmountAggregations{
    repeated AggregationsPerCurrency aggregations = 1;
}

message AggregationsPerCurrency{
    string currency = 1;
    int64 gross_amount = 2;
    int64 gross_amount_usd =3;
}
