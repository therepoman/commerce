package moneypenny

import (
	"time"
)

type GetUserPayoutTypeResponse struct {
	ChannelID         string   `json:"channel_id"`
	IsAffiliate       bool     `json:"is_affiliate"`
	IsDeveloper       bool     `json:"is_developer"`
	IsPartner         bool     `json:"is_partner"`
	Category          string   `json:"category"`
	DeveloperCategory string   `json:"developer_category"`
	Tags              []string `json:"tags"`
}

type OnboardingStatusMessage struct {
	ChannelID         string      `json:"channel_id"`
	Timestamp         time.Time   `json:"timestamp"`
	Action            string      `json:"action"`
	ActionCategory    string      `json:"action_category"`
	InvitationID      string      `json:"invite_id"`
	Category          string      `json:"category"`
	DeveloperCategory string      `json:"developer_category"`
	Tags              []string    `json:"tags"`
	Features          interface{} `json:"features"`
}

type GetWorkflowStateResponse struct {
	ChannelID      string      `json:"channel_id"`
	PayoutEntityID string      `json:"payout_entity_id"`
	InvitationID   string      `json:"invitation_id"`
	WorkflowID     string      `json:"workflow_id"`
	State          string      `json:"state"`
	StateInfo      interface{} `json:"state_info"`
}

type GetEULAResponse struct {
	ChannelID   string `json:"channel_id"`
	EULAType    string `json:"eula_type"`
	EULAVersion string `json:"eula_version"`
	Body        string `json:"body"`
}

type GetPayoutEntityForChannelResponse struct {
	PayoutEntityID     string         `json:"payout_entity_id"`
	OwnerChannelID     string         `json:"owner_channel_id"`
	TaxWithholdingRate TaxWithholding `json:"tax_withholding_rate"`
}

type PayoutAttribution struct {
	UserID    string    `json:"user_id"`
	StartDate time.Time `json:"start_date"`
}

type GetPayoutAttributionsResponse struct {
	PayoutEntityID     string              `json:"payout_entity_id"`
	PayoutAttributions []PayoutAttribution `json:"payout_attributions"`
}

type CreatePayoutAttributionRequest struct {
	StartDate time.Time `json:"start_date"`
}

type PayoutEntityStruct struct {
	PayoutEntityID     string         `json:"payout_entity_id"`
	OwnerChannelID     string         `json:"owner_channel_id"`
	TaxWithholdingRate TaxWithholding `json:"tax_withholding_rate"`
	IsLegacy           bool           `json:"is_legacy"`
}

type GetPayoutEntitiesOwnedByChannelResponse struct {
	PayoutEntities []PayoutEntityStruct `json:"payout_entities"`
}

type TaxWithholding struct {
	RoyaltyTaxWithholdingRate *float64 `json:"royalty_tax_withholding_rate,omitempty"`
	ServiceTaxWithholdingRate *float64 `json:"service_tax_withholding_rate,omitempty"`
}

type SetOnboardingEULARequest struct {
	EulaType    string `json:"eula_type"`
	EulaVersion string `json:"eula_version"`
}

type GetTIMSParamsRequest struct {
	Lang       string `json:"lang"`
	IncomeType string `json:"income_type"`
	ReturnUrl  string `json:"return_url"`
}

type GetTIMSParamsResponse struct {
	AccountID             string `json:"account_id"`
	ChannelID             string `json:"channel_id"`
	PayoutEntityID        string `json:"payout_entity_id"`
	WorkflowID            string `json:"workflow_id"`
	Url                   string `json:"url"`
	IncomeType            string `json:"income_type"`
	Locale                string `json:"locale"`
	LegalName             string `json:"legal_name"`
	AddressID             string `json:"address_id"`
	ClientID              string `json:"client_id"`
	LoggedInCustomerEmail string `json:"logged_in_customer_email"`
	LoggedInCustomerID    string `json:"logged_in_customer_id"`
	ReturnURL             string `json:"return_url"`
	LogoImageUrl          string `json:"logo_image_url"`
	SiteID                string `json:"site_id"`
	Signature             string `json:"signature"`
}

type GetAccountPayableRegistrationURLResponse struct {
	URL string `json:"url"`
}

type GetPayoutUserAttributesResponse struct {
	WorkflowID     string `json:"workflow_id"`
	FirstName      string `json:"first_name"`
	MiddleName     string `json:"middle_name"`
	LastName       string `json:"last_name"`
	CompanyName    string `json:"company_name"`
	Email          string `json:"email"`
	StreetAddress  string `json:"street_address"`
	StreetAddress2 string `json:"street_address2"`
	City           string `json:"city"`
	State          string `json:"state"`
	Postal         string `json:"postal"`
	Country        string `json:"country"`
	Birthdate      string `json:"birthdate"`
	ParentName     string `json:"parent_name"`
	ParentEmail    string `json:"parent_email"`
}

type SetPayoutUserAttributesRequest struct {
	FirstName      string `json:"first_name"`
	MiddleName     string `json:"middle_name"`
	LastName       string `json:"last_name"`
	CompanyName    string `json:"company_name"`
	Email          string `json:"email"`
	StreetAddress  string `json:"street_address"`
	StreetAddress2 string `json:"street_address2"`
	City           string `json:"city"`
	State          string `json:"state"`
	Postal         string `json:"postal"`
	Country        string `json:"country"`
	Birthdate      string `json:"birthdate"`
	ParentName     string `json:"parent_name"`
	ParentEmail    string `json:"parent_email"`
}

type WorkflowEvent struct {
	WorkflowID    string    `json:"workflow_id"`
	InvitationID  string    `json:"invitation_id"`
	State         string    `json:"state"`
	PreviousState string    `json:"previous_state"`
	Transition    string    `json:"transition"`
	Timestamp     time.Time `json:"timestamp"`
	Data          string    `json:"data"`
}

type GetWorkflowEventResponse struct {
	WorkflowID string          `json:"workflow_id"`
	Events     []WorkflowEvent `json:"workflow_events"`
}

type GetWorkflowEventsResponse struct {
	ChannelID      string          `json:"channel_id"`
	PayoutEntityID string          `json:"payout_entity_id"`
	Events         []WorkflowEvent `json:"workflow_events"`
}

type GetPayoutEntityWorkflowEventsResponse struct {
	PayoutEntityID string          `json:"payout_entity_id"`
	Events         []WorkflowEvent `json:"workflow_events"`
}

type BulkCreateInvitationRequest struct {
	ChannelIDs []string `json:"channel_ids"`
	Category   CreateInvitationRequest `json:"category"`
}

type BulkCreateInvitationResponse []CreateInvitationResponse

type CreateInvitationRequest struct {
	Category string      `json:"category"`
	Tags     []string    `json:"tags"`
	Features interface{} `json:"features"`
}

type CreateInvitationResponse struct {
	ChannelID    string `json:"channel_id"`
	InvitationID string `json:"invitation_id"`
	Success      bool   `json:"success"`
	ErrorMessage string `json:"error_message,omitempty"`
}

type GetInvitationResponse struct {
	InvitationID string   `json:"invitation_id"`
	ChannelID    string   `json:"channel_id"`
	Category     string   `json:"category"`
	Tags         []string `json:"tags"`
}

type GetInvitationWorkflowsResponse struct {
	InvitationID string   `json:"invitation_id"`
	WorkflowIDs  []string `json:"workflow_id"`
}

type OffboardUserRequest struct {
	Category string `json:"category"`
}

type InviteDeveloperResponse struct {
	ChannelID    string `json:"channel_id"`
	InvitationID string `json:"invitation_id"`
	Success      bool   `json:"success"`
	ErrorMessage string `json:"error_message,omitempty"`
}

type GetDeveloperStateResponse struct {
	ChannelID    string `json:"channel_id"`
	CurrentState string `json:"current_state"`
	WorkflowType string `json:"workflow_type,omitempty"`
}

type PendingActivationResponse []ChannelPendingActivation

type ChannelPendingActivation struct {
	ChannelID      string `json:"channel_id"`
	PayoutEntityID string `json:"payout_entity_id"`
}

type ActivatePendingChannelRequest struct {
	ContractStartDate       time.Time   `json:"contract_start_date"`
	ContractEndDate         time.Time   `json:"contract_end_date"`
	ContractAutoRenewNotice string      `json:"contract_auto_renew_notice"`
	ContractRenewalTerm     string      `json:"contract_renewal_term"`
	ContractURL             string      `json:"contract_url"`
	ContractAutoRenew       bool        `json:"contract_auto_renew"`
	Features                interface{} `json:"features"`
}

type GetContractDetailsResponse struct {
	ContractStartDate       time.Time `json:"contract_start_date"`
	ContractEndDate         time.Time `json:"contract_end_date"`
	ContractAutoRenewNotice string    `json:"contract_auto_renew_notice"`
	ContractRenewalTerm     string    `json:"contract_renewal_term"`
	ContractURL             string    `json:"contract_url"`
	ContractAutoRenew       bool      `json:"contract_auto_renew"`
}

// UpdateContractDetailsRequest is the request with values needed to update contract details
type UpdateContractDetailsRequest struct {
	ContractStartDate       time.Time `json:"contract_start_date"`
	ContractEndDate         time.Time `json:"contract_end_date"`
	ContractAutoRenewNotice string    `json:"contract_auto_renew_notice"`
	ContractRenewalTerm     string    `json:"contract_renewal_term"`
	ContractURL             string    `json:"contract_url"`
	ContractAutoRenew       bool      `json:"contract_auto_renew"`
}

// UpdateContractDetailsResponse is the response returned when updating contract details
type UpdateContractDetailsResponse struct {
	InvitationID string `json:"invitation_id"`
	Success      bool   `json:"success"`
}
