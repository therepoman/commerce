// Package moneypenny contains a client for the Moneypenny service for use by Visage and other internal twitch services.
// The package also contains shared models for response objects.
package moneypenny

import (
	"bytes"
	"encoding/json"
	"net/url"

	"code.justin.tv/foundation/twitchclient"
	"golang.org/x/net/context"
	"strings"
)

const (
	defaultStatSampleRate = 0.1
	defaultTimingXactName = "moneypenny"
)

// Client provides an interface for the service client
type Client interface {
	GetWorkflowState(ctx context.Context, channelID string, reqOpts *twitchclient.ReqOpts) (*GetWorkflowStateResponse, error)
	StartNewWorkflow(ctx context.Context, channelID string, reqOpts *twitchclient.ReqOpts) (*GetWorkflowStateResponse, error)
	CancelExistingWorkflow(ctx context.Context, channelID string, reqOpts *twitchclient.ReqOpts) (*GetWorkflowStateResponse, error)
	SetOnboardingEULA(ctx context.Context, channelID string, eulaRequest SetOnboardingEULARequest, reqOpts *twitchclient.ReqOpts) (*GetWorkflowStateResponse, error)
	GetOnboardingEULA(ctx context.Context, channelID string, reqOpts *twitchclient.ReqOpts) (*GetEULAResponse, error)
	GetPayoutEntityForID(ctx context.Context, payoutEntityID string, reqOpts *twitchclient.ReqOpts) (*GetPayoutEntityForChannelResponse, error)
	GetPayoutEntityForChannel(ctx context.Context, channelID string, reqOpts *twitchclient.ReqOpts) (*GetPayoutEntityForChannelResponse, error)
	GetLivePayoutEntityForChannel(ctx context.Context, channelID string, reqOpts *twitchclient.ReqOpts) (*GetPayoutEntityForChannelResponse, error)
	GetPayoutEntitiesOwnedByChannel(ctx context.Context, channelID string, reqOpts *twitchclient.ReqOpts) (*GetPayoutEntitiesOwnedByChannelResponse, error)
	GetAccountPayableRegistrationURL(ctx context.Context, channelID string, channelName string, language string, redirectTo string, reqOpts *twitchclient.ReqOpts) (*GetAccountPayableRegistrationURLResponse, error)
	GetTIMSDetails(ctx context.Context, channelID string, timsRequest GetTIMSParamsRequest, reqOpts *twitchclient.ReqOpts) (*GetTIMSParamsResponse, error)
	GetPayoutUserAttributes(ctx context.Context, channelID string, reqOpts *twitchclient.ReqOpts) (*GetPayoutUserAttributesResponse, error)
	SetPayoutUserAttributes(ctx context.Context, channelID string, request SetPayoutUserAttributesRequest, reqOpts *twitchclient.ReqOpts) (*GetWorkflowStateResponse, error)
	GetUserPayoutType(ctx context.Context, channelID string, reqOpts *twitchclient.ReqOpts) (*GetUserPayoutTypeResponse, error)
	BatchGetUserPayoutType(ctx context.Context, channelIDs []string, reqOpts *twitchclient.ReqOpts) (map[string]*GetUserPayoutTypeResponse, error)
	GetInvitation(ctx context.Context, channelID string, reqOpts *twitchclient.ReqOpts) (*GetInvitationResponse, error)
	InviteDeveloper(ctx context.Context, channelID string, reqOpts *twitchclient.ReqOpts) (*InviteDeveloperResponse, error)
	GetDeveloperState(ctx context.Context, channelID string, reqOpts *twitchclient.ReqOpts) (*GetDeveloperStateResponse, error)
}

type client struct {
	twitchclient.Client
}

// NewClient creates a new client for use in making service calls.
func NewClient(conf twitchclient.ClientConf) (Client, error) {
	if conf.TimingXactName == "" {
		conf.TimingXactName = defaultTimingXactName
	}
	twitchClient, err := twitchclient.NewClient(conf)
	return &client{twitchClient}, err
}

// Get an on-boarding workflow state of a Payout Entity for a given ChannelID
func (c *client) GetWorkflowState(ctx context.Context, channelID string, reqOpts *twitchclient.ReqOpts) (*GetWorkflowStateResponse, error) {
	serviceUrl := "/payout/" + channelID + "/onboarding/workflow"

	req, err := c.NewRequest("GET", serviceUrl, nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.moneypenny.get_payout_entity_workflow_state",
		StatSampleRate: defaultStatSampleRate,
	})

	var getWorkflowStateResponse GetWorkflowStateResponse
	_, err = c.DoJSON(ctx, &getWorkflowStateResponse, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &getWorkflowStateResponse, nil
}

// Start a new payout on-boarding workflow for a given ChannelID
func (c *client) StartNewWorkflow(ctx context.Context, channelID string, reqOpts *twitchclient.ReqOpts) (*GetWorkflowStateResponse, error) {
	serviceUrl := "/payout/" + channelID + "/onboarding/workflow"

	req, err := c.NewRequest("POST", serviceUrl, nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.moneypenny.start_new_workflow",
		StatSampleRate: defaultStatSampleRate,
	})

	var getWorkflowStateResponse GetWorkflowStateResponse
	_, err = c.DoJSON(ctx, &getWorkflowStateResponse, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &getWorkflowStateResponse, nil
}

// Cancel existing payout on-boarding workflow for a given ChannelID
func (c *client) CancelExistingWorkflow(ctx context.Context, channelID string, reqOpts *twitchclient.ReqOpts) (*GetWorkflowStateResponse, error) {
	serviceUrl := "/payout/" + channelID + "/onboarding/workflow"

	req, err := c.NewRequest("DELETE", serviceUrl, nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.moneypenny.cancel_existing_workflow",
		StatSampleRate: defaultStatSampleRate,
	})

	var getWorkflowStateResponse GetWorkflowStateResponse
	_, err = c.DoJSON(ctx, &getWorkflowStateResponse, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &getWorkflowStateResponse, nil
}

// Sign and Set EULA for Payout on-boarding
func (c *client) SetOnboardingEULA(ctx context.Context, channelID string, eulaRequest SetOnboardingEULARequest, reqOpts *twitchclient.ReqOpts) (*GetWorkflowStateResponse, error) {
	jsonBody, err := json.Marshal(eulaRequest)
	if err != nil {
		return nil, err
	}
	serviceUrl := "/payout/" + channelID + "/onboarding/eula"

	req, err := c.NewRequest("POST", serviceUrl, bytes.NewBuffer(jsonBody))
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.moneypenny.set_onboarding_eula",
		StatSampleRate: defaultStatSampleRate,
	})

	var getWorkflowStateResponse GetWorkflowStateResponse
	_, err = c.DoJSON(ctx, &getWorkflowStateResponse, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &getWorkflowStateResponse, nil
}

// Get EULA for specific partner
func (c *client) GetOnboardingEULA(ctx context.Context, channelID string, reqOpts *twitchclient.ReqOpts) (*GetEULAResponse, error) {
	serviceUrl := "/payout/" + channelID + "/onboarding/eula"

	req, err := c.NewRequest("GET", serviceUrl, nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.moneypenny.get_onboarding_eula",
		StatSampleRate: defaultStatSampleRate,
	})

	var getEulaResponse GetEULAResponse
	_, err = c.DoJSON(ctx, &getEulaResponse, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &getEulaResponse, nil
}

// Get the PayoutEntity with the specified PayoutEntityID.
func (c *client) GetPayoutEntityForID(ctx context.Context, payoutEntityID string, reqOpts *twitchclient.ReqOpts) (*GetPayoutEntityForChannelResponse, error) {
	serviceUrl := "/payout/" + payoutEntityID

	req, err := c.NewRequest("GET", serviceUrl, nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.moneypenny.get_payout_entity",
		StatSampleRate: defaultStatSampleRate,
	})

	var getPayoutEntityForChannelResponse GetPayoutEntityForChannelResponse
	_, err = c.DoJSON(ctx, &getPayoutEntityForChannelResponse, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &getPayoutEntityForChannelResponse, nil
}

// Get the PayoutEntity owned by the given channel.
func (c *client) GetPayoutEntityForChannel(ctx context.Context, channelID string, reqOpts *twitchclient.ReqOpts) (*GetPayoutEntityForChannelResponse, error) {
	serviceUrl := "/payout/" + channelID + "/payout_entity"

	req, err := c.NewRequest("GET", serviceUrl, nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.moneypenny.get_payout_entity_for_channel",
		StatSampleRate: defaultStatSampleRate,
	})

	var getPayoutEntityForChannelResponse GetPayoutEntityForChannelResponse
	_, err = c.DoJSON(ctx, &getPayoutEntityForChannelResponse, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &getPayoutEntityForChannelResponse, nil
}

// Get the live PayoutEntity; i.e. the PayoutEntity for which this channel is currently accruing revenue.
// This is different than a channel's owned PayoutEntity for channels on pro teams, etc.
func (c *client) GetLivePayoutEntityForChannel(ctx context.Context, channelID string, reqOpts *twitchclient.ReqOpts) (*GetPayoutEntityForChannelResponse, error) {
	serviceUrl := "/payout/" + channelID + "/live_payout_entity"

	req, err := c.NewRequest("GET", serviceUrl, nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.moneypenny.get_live_payout_entity_for_channel",
		StatSampleRate: defaultStatSampleRate,
	})

	var getPayoutEntityForChannelResponse GetPayoutEntityForChannelResponse
	_, err = c.DoJSON(ctx, &getPayoutEntityForChannelResponse, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &getPayoutEntityForChannelResponse, nil
}

// Get all Payout Entities owned by a specific Channel.
// NOTE: This API is specifically built for Admin Panel to handle the case in which an affiliate upgrades to a partner and thus owns two Payout Entities.
// As such, this API should be deprecated long-term.
func (c *client) GetPayoutEntitiesOwnedByChannel(ctx context.Context, channelID string, reqOpts *twitchclient.ReqOpts) (*GetPayoutEntitiesOwnedByChannelResponse, error) {
	serviceUrl := "/payout/" + channelID + "/payout_entities"

	req, err := c.NewRequest("GET", serviceUrl, nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.moneypenny.get_payout_entities_owned_by_channel",
		StatSampleRate: defaultStatSampleRate,
	})

	var getPayoutEntitiesOwnedByChannelResponse GetPayoutEntitiesOwnedByChannelResponse
	_, err = c.DoJSON(ctx, &getPayoutEntitiesOwnedByChannelResponse, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &getPayoutEntitiesOwnedByChannelResponse, nil
}

// Account Payable is registered with third party such as Tipalti. Retrieve a registration URL using this method.
func (c *client) GetAccountPayableRegistrationURL(ctx context.Context, channelID string, channelName string, language string, redirectTo string, reqOpts *twitchclient.ReqOpts) (*GetAccountPayableRegistrationURLResponse, error) {
	query := url.Values{}
	query.Set("channelName", channelName)
	query.Set("lang", language)
	query.Set("redirectTo", redirectTo)

	serviceUrl := "/payout/" + channelID + "/onboarding/account_payable/registration_url?" + query.Encode()

	req, err := c.NewRequest("GET", serviceUrl, nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.moneypenny.get_account_payable_registration_url",
		StatSampleRate: defaultStatSampleRate,
	})

	var urlResponse GetAccountPayableRegistrationURLResponse
	_, err = c.DoJSON(ctx, &urlResponse, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &urlResponse, nil
}

// Get Information from Amazon's Tax and Identity Management System
func (c *client) GetTIMSDetails(ctx context.Context, channelID string, timsRequest GetTIMSParamsRequest, reqOpts *twitchclient.ReqOpts) (*GetTIMSParamsResponse, error) {
	jsonBody, err := json.Marshal(timsRequest)
	if err != nil {
		return nil, err
	}
	serviceUrl := "/payout/" + channelID + "/onboarding/tims"

	req, err := c.NewRequest("POST", serviceUrl, bytes.NewBuffer(jsonBody))
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.moneypenny.get_tims_details",
		StatSampleRate: defaultStatSampleRate,
	})

	var getTIMSParamsResponse GetTIMSParamsResponse
	_, err = c.DoJSON(ctx, &getTIMSParamsResponse, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &getTIMSParamsResponse, nil
}

// Get user attributes for latest workflow of channel
func (c *client) GetPayoutUserAttributes(ctx context.Context, channelID string, reqOpts *twitchclient.ReqOpts) (*GetPayoutUserAttributesResponse, error) {
	serviceUrl := "/payout/" + channelID + "/onboarding/user_attributes"

	req, err := c.NewRequest("GET", serviceUrl, nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.moneypenny.get_payout_user_attributes",
		StatSampleRate: defaultStatSampleRate,
	})

	var response GetPayoutUserAttributesResponse
	_, err = c.DoJSON(ctx, &response, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &response, nil
}

// Set user attributes for channel's workflow
func (c *client) SetPayoutUserAttributes(ctx context.Context, channelID string, request SetPayoutUserAttributesRequest, reqOpts *twitchclient.ReqOpts) (*GetWorkflowStateResponse, error) {
	jsonBody, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}

	serviceUrl := "/payout/" + channelID + "/onboarding/user_attributes"

	req, err := c.NewRequest("POST", serviceUrl, bytes.NewBuffer(jsonBody))
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.moneypenny.set_payout_user_attributes",
		StatSampleRate: defaultStatSampleRate,
	})

	var response GetWorkflowStateResponse
	_, err = c.DoJSON(ctx, &response, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &response, nil
}

// Get user payout type
func (c *client) GetUserPayoutType(ctx context.Context, channelID string, reqOpts *twitchclient.ReqOpts) (*GetUserPayoutTypeResponse, error) {
	serviceUrl := "/payout/" + channelID + "/type"

	req, err := c.NewRequest("GET", serviceUrl, nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.moneypenny.get_user_payout_type",
		StatSampleRate: defaultStatSampleRate,
	})

	var response GetUserPayoutTypeResponse
	_, err = c.DoJSON(ctx, &response, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &response, nil
}

// Batch get user payout type
func (c *client) BatchGetUserPayoutType(ctx context.Context, channelIDs []string, reqOpts *twitchclient.ReqOpts) (map[string]*GetUserPayoutTypeResponse, error) {
	idStr := strings.Join(channelIDs, ",")
	query := url.Values{}
	query.Set("channelIDs", idStr)
	serviceUrl := "/payout/types?" + query.Encode()

	req, err := c.NewRequest("GET", serviceUrl, nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.moneypenny.batch_get_user_payout_type",
		StatSampleRate: defaultStatSampleRate,
	})

	response := make(map[string]*GetUserPayoutTypeResponse)
	_, err = c.DoJSON(ctx, &response, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}
	return response, nil
}

// Get Invitation for a user
func (c *client) GetInvitation(ctx context.Context, channelID string, reqOpts *twitchclient.ReqOpts) (*GetInvitationResponse, error) {
	serviceUrl := "/payout/invite/" + channelID

	req, err := c.NewRequest("GET", serviceUrl, nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.moneypenny.get_invite",
		StatSampleRate: defaultStatSampleRate,
	})

	var response GetInvitationResponse
	_, err = c.DoJSON(ctx, &response, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &response, nil
}

func (c *client) InviteDeveloper(ctx context.Context, channelID string, reqOpts *twitchclient.ReqOpts) (*InviteDeveloperResponse, error) {
	serviceUrl := "/payout/developer/invite/" + channelID

	req, err := c.NewRequest("POST", serviceUrl, nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.moneypenny.create_developer_invite",
		StatSampleRate: defaultStatSampleRate,
	})

	var response InviteDeveloperResponse
	_, err = c.DoJSON(ctx, &response, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &response, nil
}

func (c *client) GetDeveloperState(ctx context.Context, channelID string, reqOpts *twitchclient.ReqOpts) (*GetDeveloperStateResponse, error) {
	serviceUrl := "/payout/developer/state/" + channelID

	req, err := c.NewRequest("GET", serviceUrl, nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.moneypenny.get_developer_state",
		StatSampleRate: defaultStatSampleRate,
	})

	var response GetDeveloperStateResponse
	_, err = c.DoJSON(ctx, &response, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &response, nil
}
