package encryption

import (
	"crypto"
	"crypto/aes"
	"crypto/cipher"
	"crypto/hmac"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"crypto/x509"
	"encoding/hex"
	"encoding/pem"
	"errors"
	"fmt"
	"io/ioutil"

	"github.com/sirupsen/logrus"
)

//LoadPrivateKey loads an parses a PEM encoded private key file.
func LoadPrivateKey(path string) (Signer, error) {
	file, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}
	return parsePrivateKey(file)
}

//GenerateHashkey is an internal function to generate a hashkey
func GenerateHashkey(keyString, authString string) string {
	key := []byte(keyString)
	h := hmac.New(sha256.New, key)
	_, err := h.Write([]byte(authString))
	if err != nil {
		logrus.WithError(err).Errorf("Cannot generate hashkey for authString: %v", authString)
	}
	return hex.EncodeToString(h.Sum(nil))
}

// parsePublicKey parses a PEM encoded private key.
func parsePrivateKey(pemBytes []byte) (Signer, error) {
	block, _ := pem.Decode(pemBytes)
	if block == nil {
		return nil, errors.New("ssh: no key found")
	}

	var rawkey interface{}
	switch block.Type {
	case "RSA PRIVATE KEY":
		rsaKey, err := x509.ParsePKCS1PrivateKey(block.Bytes)
		if err != nil {
			return nil, err
		}
		rawkey = rsaKey
	default:
		return nil, fmt.Errorf("ssh: unsupported key type %q", block.Type)
	}
	return newSignerFromKey(rawkey)
}

//Signer can create signatures that verify against a public key.
type Signer interface {
	// Sign returns raw signature for the given data. This method
	// will apply the hash specified for the keytype to the data.
	Sign(data []byte) ([]byte, error)
}

func newSignerFromKey(k interface{}) (Signer, error) {
	var sshKey Signer
	switch t := k.(type) {
	case *rsa.PrivateKey:
		sshKey = &rsaPrivateKey{t}
	default:
		return nil, fmt.Errorf("ssh: unsupported key type %T", k)
	}
	return sshKey, nil
}

type rsaPrivateKey struct {
	*rsa.PrivateKey
}

//Sign signs data with rsa-sha256
func (r *rsaPrivateKey) Sign(data []byte) ([]byte, error) {
	h := sha256.New()
	if _, err := h.Write(data); err != nil {
		return nil, err
	}
	d := h.Sum(nil)
	return rsa.SignPKCS1v15(rand.Reader, r.PrivateKey, crypto.SHA256, d)
}

//EncryptBytes encrypt plainText using the key and additionalData. Pad the ciphertext with a nonce if shouldPad is true
func EncryptBytes(key, plainText, additionalData []byte, shouldPad bool) ([]byte, []byte, error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, nil, err
	}

	gcm, err := cipher.NewGCM(block)
	if err != nil {
		return nil, nil, err
	}

	nonce := make([]byte, gcm.NonceSize())
	if _, err := rand.Read(nonce); err != nil {
		return nil, nil, err
	}

	var padding []byte
	if shouldPad {
		padding = nonce
	} else {
		padding = nil
	}

	return gcm.Seal(padding, nonce, plainText, additionalData), nonce, nil
}

//DecryptBytes decrypt cipherText using the key and additionalData. cipherText need to be padded
func DecryptBytes(key, cipherText, additionalData []byte) ([]byte, error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}
	gcm, err := cipher.NewGCM(block)
	if err != nil {
		return nil, err
	}

	//only Handle ciphertext that is padded
	nonce, ciphertext := cipherText[:gcm.NonceSize()], cipherText[gcm.NonceSize():]

	return gcm.Open(nil, nonce, ciphertext, additionalData)
}

//Zero replaces every byte in b with 0
func Zero(b []byte) {
	for i := range b {
		b[i] = 0
	}
}
