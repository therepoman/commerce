syntax = "proto3";

package code.justin.tv.commerce.mako.dashboard;
option go_package = "mako-dashboard";

import "google/protobuf/timestamp.proto";

service Mako {

    // Get the emote groups details for the given emote group ids.
    // This API uses uncached code paths to serve accurate data in real-time, which may affect performance.
    rpc GetEmotesByGroups(GetEmotesByGroupsRequest) returns (GetEmotesByGroupsResponse);

    // Get the emote details for the given emote ids.
    // This API uses uncached code paths to serve accurate data in real-time, which may affect performance.
    rpc GetEmotesByIDs(GetEmotesByIDsRequest) returns (GetEmotesByIDsResponse);

    // Get emote image/upload id(s) and url(s).
    rpc GetEmoteUploadConfig(GetEmoteUploadConfigRequest) returns (GetEmoteUploadConfigResponse);

    // Create emote given image IDs and emote info.
    rpc CreateEmote(CreateEmoteRequest) returns (CreateEmoteResponse);

    // Create follower emote without requiring clients to know the follower emote groupID
    rpc CreateFollowerEmote(CreateFollowerEmoteRequest) returns (CreateFollowerEmoteResponse);

    // Allows a broadcaster to reorder emotes within a single emote group
    rpc UpdateEmoteOrders(UpdateEmoteOrdersRequest) returns (UpdateEmoteOrdersResponse);

    // Assign an emote to a specified group
    rpc AssignEmoteToGroup(AssignEmoteToGroupRequest) returns (AssignEmoteToGroupResponse);

    // Assign an emote to this channel's follower emote group
    rpc AssignEmoteToFollowerGroup(AssignEmoteToFollowerGroupRequest) returns (AssignEmoteToFollowerGroupResponse);

    // Remove an emote from its assigned group
    rpc RemoveEmoteFromGroup(RemoveEmoteFromGroupRequest) returns (RemoveEmoteFromGroupResponse);

    // Admin-only: Update one or many emotes' origins
    rpc UpdateEmoteOrigins(UpdateEmoteOriginsRequest) returns (UpdateEmoteOriginsResponse);

    // Get all of a user's emote-related limits
    rpc GetUserEmoteLimits(GetUserEmoteLimitsRequest) returns (GetUserEmoteLimitsResponse);

    // Get a user's status on their ability to upload or create follower emotes.
    rpc GetUserFollowerEmoteStatus(GetUserFollowerEmoteStatusRequest) returns (GetUserFollowerEmoteStatusResponse);

    // Get a user's rollout status for various launch rollouts
    // NOTE: This is designed to be a short-lived API just for the rollout of DAM M2, but could be kept alive long term for other future rollouts
    rpc GetUserRolloutStatus(GetUserRolloutStatusRequest) returns (GetUserRolloutStatusResponse);

    // Get default emote images.
    rpc GetDefaultEmoteImages(GetDefaultEmoteImagesRequest) returns (GetDefaultEmoteImagesResponse);
}

// Request / Response Types

message GetEmotesByGroupsRequest {
    repeated string emote_group_ids = 1;
    EmoteFilter filter = 2;
}

message GetEmotesByGroupsResponse {
    repeated EmoteGroup groups = 1;
}

message GetEmotesByIDsRequest {
    repeated string emote_ids = 1;
}

message GetEmotesByIDsResponse {
    repeated Emote emotes = 1;
}

message GetEmoteUploadConfigRequest {
    string user_id = 1;
    ResizePlan resize_plan = 2;
    repeated EmoteImageSize sizes = 3;
    EmoteAssetType asset_type = 4;
    bool generate_static_version_of_animated_assets = 5;
}

message GetEmoteUploadConfigResponse {
    repeated EmoteUploadConfig upload_configs = 1;
}

message CreateEmoteRequest {
    string user_id = 1;
    string code = 2 [deprecated=true];
    string code_suffix = 3;
    repeated string group_ids = 4;
    repeated EmoteImageAsset image_assets = 5;
    EmoteAssetType asset_type = 6;
    EmoteOrigin origin = 7;
    EmoteState state = 8;
    // Set this to true if you want Mako to determine initial emote state, based on the logic below:
    // (Partner OR Affiliate) AND inGoodStanding -> Active
    // Otherwise -> Pending and added to the moderation queue
    bool enforce_moderation_workflow = 9;
    AnimatedEmoteTemplate animated_emote_template = 10;
    // Set this to true if you want to create an emote which does not prepend this channel's prefix to its code.
    // Generally this is useful for Globals, HypeTrain, MegaCommerce, etc.
    bool should_ignore_prefix = 11;
    // Source of the provided image ids. Either a user supplied image or an image existing in the default image library.
    ImageSource image_source = 12;
}

message CreateEmoteResponse {
    string id = 1 [deprecated=true];
    string code = 2 [deprecated=true];
    repeated string group_ids = 3 [deprecated=true];
    EmoteState state = 4 [deprecated=true];
    google.protobuf.Timestamp created_at = 5 [deprecated=true];
    Emote emote = 6;
}

message CreateFollowerEmoteRequest {
    string user_id = 1;
    string code_suffix = 2;
    repeated EmoteImageAsset image_assets = 4;
    EmoteAssetType asset_type = 5;
    EmoteState state = 6;
    // Set this to true if you want Mako to determine initial emote state, based on the logic below:
    // (Partner OR Affiliate) AND inGoodStanding -> Active
    // Otherwise -> Pending and added to the moderation queue
    bool enforce_moderation_workflow = 7;
    // Source of the provided image ids. Either a user supplied image or an image existing in the default image library.
    ImageSource image_source = 8;
}

message CreateFollowerEmoteResponse {
    Emote emote = 1;
}

message UpdateEmoteOrdersRequest {
    string requesting_user_id = 1;
    repeated EmoteOrder emote_orders = 2;
}

message UpdateEmoteOrdersResponse {
    repeated Emote emotes = 1; // all emotes including their newly updated orders
    // from the requested group_ids, sorted, after applying the update
}

message AssignEmoteToGroupRequest {
    string requesting_user_id = 1;
    string emote_id = 2;
    string group_id = 3;
    EmoteOrigin origin = 4;
}

message AssignEmoteToGroupResponse {
    Emote emote = 1;
}

message AssignEmoteToFollowerGroupRequest {
    string channel_id = 1;
    string requesting_user_id = 2;
    string emote_id = 3;
}

message AssignEmoteToFollowerGroupResponse {
    Emote emote = 1;
}

message RemoveEmoteFromGroupRequest {
    string requesting_user_id = 1;
    string emote_id = 2;
}

message RemoveEmoteFromGroupResponse {
    Emote emote = 1;
}

message UpdateEmoteOriginsRequest {
    repeated OriginUpdate origin_updates = 1;
}

message UpdateEmoteOriginsResponse {
    repeated Emote emotes = 1;
}

message GetUserEmoteLimitsRequest {
    string requesting_user_id = 1;
    string user_id = 2;
}

message GetUserEmoteLimitsResponse {
    // Max number of static emotes that the user can own
    int32 owned_static_emote_limit = 1;
    // Max number of animated emotes that the user can own
    int32 owned_animated_emote_limit = 2;
}

message GetUserFollowerEmoteStatusRequest {
    string user_id = 1;
}

message GetUserFollowerEmoteStatusResponse {
    FollowerEmoteDashboardStatus status = 1;
}

message GetUserRolloutStatusRequest {
    string user_id = 1;
}

message GetUserRolloutStatusResponse {
    bool is_digital_asset_manager_m2_enabled = 1;
}

message GetDefaultEmoteImagesRequest {
}

message GetDefaultEmoteImagesResponse {
    repeated DefaultEmoteImage default_emote_images = 1;
}

// Data Types

// An Emote Group contains a list of emotes tied to that emote group.
message EmoteGroup {
    string id = 1;
    repeated Emote emotes = 2;
    int64 order = 3;
    EmoteGroupType group_type = 4;
}

// EmoteGroupType indicates what kind the emotes contained in a given emote group are.
// For now, this means what kind of file (gif + png for animated, png for static) the assets are.
enum EmoteGroupType {
    static_group = 0;
    animated_group = 1;
}

// An EmoteFilter specifies a filter that will be applied to the underlying emotes before the response is created.
message EmoteFilter {
    Domain domain = 2;
    repeated EmoteState states = 3;
}

// An Emote has an identifier and a code used in clients.
message Emote {
    string id = 1;
    string code = 2;
    string group_id = 3;
    string owner_id = 4;
    EmoteState state = 5;
    Domain domain = 6;
    string suffix = 7;
    EmoteAssetType asset_type = 8;
    int64 order = 9;
    google.protobuf.Timestamp created_at = 10;
    AnimatedEmoteTemplate animated_emote_template = 11;
    EmoteOrigin origin = 12;
}

// A Domain specifies what type of asset this is.
enum Domain {
    emotes = 0;
}

// An EmoteState specifies the current state of the emote.
enum EmoteState {
    active = 0;
    inactive = 1;
    moderated = 2;
    pending = 3;
    archived = 4;
    unknown = 5;
    pending_archived = 6;
}

enum EmoteAssetType {
    static = 0;
    animated = 1;
}

message EmoteUploadConfig {
    string id = 1;
    EmoteImageSize size = 2;
    string url = 3;
    repeated EmoteImage images = 4;
    EmoteAssetType asset_type = 5;
}

message EmoteImage {
    string id = 1;
    EmoteImageSize size = 2;
    string url = 3;
    EmoteAssetType asset_type = 4;
}

enum EmoteImageSize {
    size_original = 0;
    size_1x = 1;
    size_2x = 2;
    size_4x = 3;
}

enum ResizePlan {
    no_resize = 0;
    auto_resize = 1;
}

message EmoteImageIds {
    string image1x_id = 1;
    string image2x_id = 2;
    string image4x_id = 3;
}

message EmoteImageAsset {
    EmoteImageIds image_ids = 1;
    EmoteAssetType asset_type = 2;
}

enum EmoteOrigin {
    None = 0;
    ChannelPoints = 1;
    BitsBadgeTierEmotes = 2;
    Subscriptions = 3;
    Rewards = 4;
    HypeTrain = 5;
    Prime = 6;
    Turbo = 7;
    Smilies = 8;
    Globals = 9;
    OWL2019 = 10;
    TwoFactor = 11;
    LimitedTime = 12;
    MegaCommerce = 13;
    Archive = 14;
    Follower = 15;
}

enum ImageSource {
    UserImage = 0;
    DefaultLibraryImage = 1;
}

message OriginUpdate {
    string emote_id = 1;
    EmoteOrigin origin = 2;
}

message EmoteOrder {
    string emote_id = 1;
    string group_id = 2;
    int32 order = 3; // all emote orders must be unique per group_id
}

enum AnimatedEmoteTemplate {
  NO_TEMPLATE = 0;
  SHAKE = 1;
  ROLL = 2;
  SPIN = 3;
  RAVE = 4;
  SLIDEIN = 5;
  SLIDEOUT = 6;
}

enum FollowerEmoteDashboardStatus {
  Unknown = 0;
  NotAllowed = 1;
  NoUpload = 2;
  Allowed = 3;
}

message DefaultEmoteImage {
    string id = 1;
    string name = 2;
    EmoteAssetType asset_type = 3;
    repeated string tags = 4;
    repeated EmoteImageAsset image_assets = 5;
}
