// Code generated by protoc-gen-go. DO NOT EDIT.
// source: rpc/pachter-service.proto

package pachter

import (
	fmt "fmt"
	math "math"

	proto "github.com/golang/protobuf/proto"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion2 // please upgrade the proto package

// import public "vendor/code.justin.tv/commerce/petozi/rpc/petozi-service.proto";
// I tried importnig this from Petozi with the above lines, but it freaked protobuf out to have two health checks,
// even though they were in a different package.
type Platform int32

const (
	Platform_NONE    Platform = 0
	Platform_AMAZON  Platform = 1
	Platform_PAYPAL  Platform = 2
	Platform_IOS     Platform = 3
	Platform_ANDROID Platform = 4
	Platform_XSOLLA  Platform = 5
	Platform_PHANTO  Platform = 6
)

var Platform_name = map[int32]string{
	0: "NONE",
	1: "AMAZON",
	2: "PAYPAL",
	3: "IOS",
	4: "ANDROID",
	5: "XSOLLA",
	6: "PHANTO",
}

var Platform_value = map[string]int32{
	"NONE":    0,
	"AMAZON":  1,
	"PAYPAL":  2,
	"IOS":     3,
	"ANDROID": 4,
	"XSOLLA":  5,
	"PHANTO":  6,
}

func (x Platform) String() string {
	return proto.EnumName(Platform_name, int32(x))
}

func (Platform) EnumDescriptor() ([]byte, []int) {
	return fileDescriptor_f348e9b303f982a3, []int{0}
}

type HealthCheckReq struct {
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *HealthCheckReq) Reset()         { *m = HealthCheckReq{} }
func (m *HealthCheckReq) String() string { return proto.CompactTextString(m) }
func (*HealthCheckReq) ProtoMessage()    {}
func (*HealthCheckReq) Descriptor() ([]byte, []int) {
	return fileDescriptor_f348e9b303f982a3, []int{0}
}

func (m *HealthCheckReq) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_HealthCheckReq.Unmarshal(m, b)
}
func (m *HealthCheckReq) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_HealthCheckReq.Marshal(b, m, deterministic)
}
func (m *HealthCheckReq) XXX_Merge(src proto.Message) {
	xxx_messageInfo_HealthCheckReq.Merge(m, src)
}
func (m *HealthCheckReq) XXX_Size() int {
	return xxx_messageInfo_HealthCheckReq.Size(m)
}
func (m *HealthCheckReq) XXX_DiscardUnknown() {
	xxx_messageInfo_HealthCheckReq.DiscardUnknown(m)
}

var xxx_messageInfo_HealthCheckReq proto.InternalMessageInfo

type HealthCheckResp struct {
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *HealthCheckResp) Reset()         { *m = HealthCheckResp{} }
func (m *HealthCheckResp) String() string { return proto.CompactTextString(m) }
func (*HealthCheckResp) ProtoMessage()    {}
func (*HealthCheckResp) Descriptor() ([]byte, []int) {
	return fileDescriptor_f348e9b303f982a3, []int{1}
}

func (m *HealthCheckResp) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_HealthCheckResp.Unmarshal(m, b)
}
func (m *HealthCheckResp) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_HealthCheckResp.Marshal(b, m, deterministic)
}
func (m *HealthCheckResp) XXX_Merge(src proto.Message) {
	xxx_messageInfo_HealthCheckResp.Merge(m, src)
}
func (m *HealthCheckResp) XXX_Size() int {
	return xxx_messageInfo_HealthCheckResp.Size(m)
}
func (m *HealthCheckResp) XXX_DiscardUnknown() {
	xxx_messageInfo_HealthCheckResp.DiscardUnknown(m)
}

var xxx_messageInfo_HealthCheckResp proto.InternalMessageInfo

type GetBitsMonthlyReportReq struct {
	Month                uint32   `protobuf:"varint,1,opt,name=month,proto3" json:"month,omitempty"`
	Year                 uint32   `protobuf:"varint,2,opt,name=year,proto3" json:"year,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *GetBitsMonthlyReportReq) Reset()         { *m = GetBitsMonthlyReportReq{} }
func (m *GetBitsMonthlyReportReq) String() string { return proto.CompactTextString(m) }
func (*GetBitsMonthlyReportReq) ProtoMessage()    {}
func (*GetBitsMonthlyReportReq) Descriptor() ([]byte, []int) {
	return fileDescriptor_f348e9b303f982a3, []int{2}
}

func (m *GetBitsMonthlyReportReq) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_GetBitsMonthlyReportReq.Unmarshal(m, b)
}
func (m *GetBitsMonthlyReportReq) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_GetBitsMonthlyReportReq.Marshal(b, m, deterministic)
}
func (m *GetBitsMonthlyReportReq) XXX_Merge(src proto.Message) {
	xxx_messageInfo_GetBitsMonthlyReportReq.Merge(m, src)
}
func (m *GetBitsMonthlyReportReq) XXX_Size() int {
	return xxx_messageInfo_GetBitsMonthlyReportReq.Size(m)
}
func (m *GetBitsMonthlyReportReq) XXX_DiscardUnknown() {
	xxx_messageInfo_GetBitsMonthlyReportReq.DiscardUnknown(m)
}

var xxx_messageInfo_GetBitsMonthlyReportReq proto.InternalMessageInfo

func (m *GetBitsMonthlyReportReq) GetMonth() uint32 {
	if m != nil {
		return m.Month
	}
	return 0
}

func (m *GetBitsMonthlyReportReq) GetYear() uint32 {
	if m != nil {
		return m.Year
	}
	return 0
}

type GetBitsMonthlyReportResp struct {
	ReportUrl            string   `protobuf:"bytes,1,opt,name=report_url,json=reportUrl,proto3" json:"report_url,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *GetBitsMonthlyReportResp) Reset()         { *m = GetBitsMonthlyReportResp{} }
func (m *GetBitsMonthlyReportResp) String() string { return proto.CompactTextString(m) }
func (*GetBitsMonthlyReportResp) ProtoMessage()    {}
func (*GetBitsMonthlyReportResp) Descriptor() ([]byte, []int) {
	return fileDescriptor_f348e9b303f982a3, []int{3}
}

func (m *GetBitsMonthlyReportResp) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_GetBitsMonthlyReportResp.Unmarshal(m, b)
}
func (m *GetBitsMonthlyReportResp) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_GetBitsMonthlyReportResp.Marshal(b, m, deterministic)
}
func (m *GetBitsMonthlyReportResp) XXX_Merge(src proto.Message) {
	xxx_messageInfo_GetBitsMonthlyReportResp.Merge(m, src)
}
func (m *GetBitsMonthlyReportResp) XXX_Size() int {
	return xxx_messageInfo_GetBitsMonthlyReportResp.Size(m)
}
func (m *GetBitsMonthlyReportResp) XXX_DiscardUnknown() {
	xxx_messageInfo_GetBitsMonthlyReportResp.DiscardUnknown(m)
}

var xxx_messageInfo_GetBitsMonthlyReportResp proto.InternalMessageInfo

func (m *GetBitsMonthlyReportResp) GetReportUrl() string {
	if m != nil {
		return m.ReportUrl
	}
	return ""
}

type GetEmoteUsageMonthlyReportReq struct {
	Month                uint32   `protobuf:"varint,1,opt,name=month,proto3" json:"month,omitempty"`
	Year                 uint32   `protobuf:"varint,2,opt,name=year,proto3" json:"year,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *GetEmoteUsageMonthlyReportReq) Reset()         { *m = GetEmoteUsageMonthlyReportReq{} }
func (m *GetEmoteUsageMonthlyReportReq) String() string { return proto.CompactTextString(m) }
func (*GetEmoteUsageMonthlyReportReq) ProtoMessage()    {}
func (*GetEmoteUsageMonthlyReportReq) Descriptor() ([]byte, []int) {
	return fileDescriptor_f348e9b303f982a3, []int{4}
}

func (m *GetEmoteUsageMonthlyReportReq) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_GetEmoteUsageMonthlyReportReq.Unmarshal(m, b)
}
func (m *GetEmoteUsageMonthlyReportReq) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_GetEmoteUsageMonthlyReportReq.Marshal(b, m, deterministic)
}
func (m *GetEmoteUsageMonthlyReportReq) XXX_Merge(src proto.Message) {
	xxx_messageInfo_GetEmoteUsageMonthlyReportReq.Merge(m, src)
}
func (m *GetEmoteUsageMonthlyReportReq) XXX_Size() int {
	return xxx_messageInfo_GetEmoteUsageMonthlyReportReq.Size(m)
}
func (m *GetEmoteUsageMonthlyReportReq) XXX_DiscardUnknown() {
	xxx_messageInfo_GetEmoteUsageMonthlyReportReq.DiscardUnknown(m)
}

var xxx_messageInfo_GetEmoteUsageMonthlyReportReq proto.InternalMessageInfo

func (m *GetEmoteUsageMonthlyReportReq) GetMonth() uint32 {
	if m != nil {
		return m.Month
	}
	return 0
}

func (m *GetEmoteUsageMonthlyReportReq) GetYear() uint32 {
	if m != nil {
		return m.Year
	}
	return 0
}

type GetEmoteUsageMonthlyReportResp struct {
	ReportUrl            string   `protobuf:"bytes,1,opt,name=report_url,json=reportUrl,proto3" json:"report_url,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *GetEmoteUsageMonthlyReportResp) Reset()         { *m = GetEmoteUsageMonthlyReportResp{} }
func (m *GetEmoteUsageMonthlyReportResp) String() string { return proto.CompactTextString(m) }
func (*GetEmoteUsageMonthlyReportResp) ProtoMessage()    {}
func (*GetEmoteUsageMonthlyReportResp) Descriptor() ([]byte, []int) {
	return fileDescriptor_f348e9b303f982a3, []int{5}
}

func (m *GetEmoteUsageMonthlyReportResp) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_GetEmoteUsageMonthlyReportResp.Unmarshal(m, b)
}
func (m *GetEmoteUsageMonthlyReportResp) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_GetEmoteUsageMonthlyReportResp.Marshal(b, m, deterministic)
}
func (m *GetEmoteUsageMonthlyReportResp) XXX_Merge(src proto.Message) {
	xxx_messageInfo_GetEmoteUsageMonthlyReportResp.Merge(m, src)
}
func (m *GetEmoteUsageMonthlyReportResp) XXX_Size() int {
	return xxx_messageInfo_GetEmoteUsageMonthlyReportResp.Size(m)
}
func (m *GetEmoteUsageMonthlyReportResp) XXX_DiscardUnknown() {
	xxx_messageInfo_GetEmoteUsageMonthlyReportResp.DiscardUnknown(m)
}

var xxx_messageInfo_GetEmoteUsageMonthlyReportResp proto.InternalMessageInfo

func (m *GetEmoteUsageMonthlyReportResp) GetReportUrl() string {
	if m != nil {
		return m.ReportUrl
	}
	return ""
}

type GetActualsReq struct {
	Platform             Platform `protobuf:"varint,1,opt,name=platform,proto3,enum=code.justin.tv.commerce.pachter.Platform" json:"platform,omitempty"`
	Month                uint32   `protobuf:"varint,2,opt,name=month,proto3" json:"month,omitempty"`
	Year                 uint32   `protobuf:"varint,3,opt,name=year,proto3" json:"year,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *GetActualsReq) Reset()         { *m = GetActualsReq{} }
func (m *GetActualsReq) String() string { return proto.CompactTextString(m) }
func (*GetActualsReq) ProtoMessage()    {}
func (*GetActualsReq) Descriptor() ([]byte, []int) {
	return fileDescriptor_f348e9b303f982a3, []int{6}
}

func (m *GetActualsReq) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_GetActualsReq.Unmarshal(m, b)
}
func (m *GetActualsReq) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_GetActualsReq.Marshal(b, m, deterministic)
}
func (m *GetActualsReq) XXX_Merge(src proto.Message) {
	xxx_messageInfo_GetActualsReq.Merge(m, src)
}
func (m *GetActualsReq) XXX_Size() int {
	return xxx_messageInfo_GetActualsReq.Size(m)
}
func (m *GetActualsReq) XXX_DiscardUnknown() {
	xxx_messageInfo_GetActualsReq.DiscardUnknown(m)
}

var xxx_messageInfo_GetActualsReq proto.InternalMessageInfo

func (m *GetActualsReq) GetPlatform() Platform {
	if m != nil {
		return m.Platform
	}
	return Platform_NONE
}

func (m *GetActualsReq) GetMonth() uint32 {
	if m != nil {
		return m.Month
	}
	return 0
}

func (m *GetActualsReq) GetYear() uint32 {
	if m != nil {
		return m.Year
	}
	return 0
}

type GetActualsResp struct {
	Actuals              map[string]uint32 `protobuf:"bytes,1,rep,name=actuals,proto3" json:"actuals,omitempty" protobuf_key:"bytes,1,opt,name=key,proto3" protobuf_val:"varint,2,opt,name=value,proto3"`
	XXX_NoUnkeyedLiteral struct{}          `json:"-"`
	XXX_unrecognized     []byte            `json:"-"`
	XXX_sizecache        int32             `json:"-"`
}

func (m *GetActualsResp) Reset()         { *m = GetActualsResp{} }
func (m *GetActualsResp) String() string { return proto.CompactTextString(m) }
func (*GetActualsResp) ProtoMessage()    {}
func (*GetActualsResp) Descriptor() ([]byte, []int) {
	return fileDescriptor_f348e9b303f982a3, []int{7}
}

func (m *GetActualsResp) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_GetActualsResp.Unmarshal(m, b)
}
func (m *GetActualsResp) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_GetActualsResp.Marshal(b, m, deterministic)
}
func (m *GetActualsResp) XXX_Merge(src proto.Message) {
	xxx_messageInfo_GetActualsResp.Merge(m, src)
}
func (m *GetActualsResp) XXX_Size() int {
	return xxx_messageInfo_GetActualsResp.Size(m)
}
func (m *GetActualsResp) XXX_DiscardUnknown() {
	xxx_messageInfo_GetActualsResp.DiscardUnknown(m)
}

var xxx_messageInfo_GetActualsResp proto.InternalMessageInfo

func (m *GetActualsResp) GetActuals() map[string]uint32 {
	if m != nil {
		return m.Actuals
	}
	return nil
}

type SetActualsReq struct {
	Platform             Platform          `protobuf:"varint,1,opt,name=platform,proto3,enum=code.justin.tv.commerce.pachter.Platform" json:"platform,omitempty"`
	Month                uint32            `protobuf:"varint,2,opt,name=month,proto3" json:"month,omitempty"`
	Year                 uint32            `protobuf:"varint,3,opt,name=year,proto3" json:"year,omitempty"`
	Actuals              map[string]uint32 `protobuf:"bytes,4,rep,name=actuals,proto3" json:"actuals,omitempty" protobuf_key:"bytes,1,opt,name=key,proto3" protobuf_val:"varint,2,opt,name=value,proto3"`
	XXX_NoUnkeyedLiteral struct{}          `json:"-"`
	XXX_unrecognized     []byte            `json:"-"`
	XXX_sizecache        int32             `json:"-"`
}

func (m *SetActualsReq) Reset()         { *m = SetActualsReq{} }
func (m *SetActualsReq) String() string { return proto.CompactTextString(m) }
func (*SetActualsReq) ProtoMessage()    {}
func (*SetActualsReq) Descriptor() ([]byte, []int) {
	return fileDescriptor_f348e9b303f982a3, []int{8}
}

func (m *SetActualsReq) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_SetActualsReq.Unmarshal(m, b)
}
func (m *SetActualsReq) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_SetActualsReq.Marshal(b, m, deterministic)
}
func (m *SetActualsReq) XXX_Merge(src proto.Message) {
	xxx_messageInfo_SetActualsReq.Merge(m, src)
}
func (m *SetActualsReq) XXX_Size() int {
	return xxx_messageInfo_SetActualsReq.Size(m)
}
func (m *SetActualsReq) XXX_DiscardUnknown() {
	xxx_messageInfo_SetActualsReq.DiscardUnknown(m)
}

var xxx_messageInfo_SetActualsReq proto.InternalMessageInfo

func (m *SetActualsReq) GetPlatform() Platform {
	if m != nil {
		return m.Platform
	}
	return Platform_NONE
}

func (m *SetActualsReq) GetMonth() uint32 {
	if m != nil {
		return m.Month
	}
	return 0
}

func (m *SetActualsReq) GetYear() uint32 {
	if m != nil {
		return m.Year
	}
	return 0
}

func (m *SetActualsReq) GetActuals() map[string]uint32 {
	if m != nil {
		return m.Actuals
	}
	return nil
}

type SetActualsResp struct {
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *SetActualsResp) Reset()         { *m = SetActualsResp{} }
func (m *SetActualsResp) String() string { return proto.CompactTextString(m) }
func (*SetActualsResp) ProtoMessage()    {}
func (*SetActualsResp) Descriptor() ([]byte, []int) {
	return fileDescriptor_f348e9b303f982a3, []int{9}
}

func (m *SetActualsResp) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_SetActualsResp.Unmarshal(m, b)
}
func (m *SetActualsResp) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_SetActualsResp.Marshal(b, m, deterministic)
}
func (m *SetActualsResp) XXX_Merge(src proto.Message) {
	xxx_messageInfo_SetActualsResp.Merge(m, src)
}
func (m *SetActualsResp) XXX_Size() int {
	return xxx_messageInfo_SetActualsResp.Size(m)
}
func (m *SetActualsResp) XXX_DiscardUnknown() {
	xxx_messageInfo_SetActualsResp.DiscardUnknown(m)
}

var xxx_messageInfo_SetActualsResp proto.InternalMessageInfo

type GetBitsSpendOrderReq struct {
	TransactionId        string   `protobuf:"bytes,1,opt,name=transaction_id,json=transactionId,proto3" json:"transaction_id,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *GetBitsSpendOrderReq) Reset()         { *m = GetBitsSpendOrderReq{} }
func (m *GetBitsSpendOrderReq) String() string { return proto.CompactTextString(m) }
func (*GetBitsSpendOrderReq) ProtoMessage()    {}
func (*GetBitsSpendOrderReq) Descriptor() ([]byte, []int) {
	return fileDescriptor_f348e9b303f982a3, []int{10}
}

func (m *GetBitsSpendOrderReq) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_GetBitsSpendOrderReq.Unmarshal(m, b)
}
func (m *GetBitsSpendOrderReq) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_GetBitsSpendOrderReq.Marshal(b, m, deterministic)
}
func (m *GetBitsSpendOrderReq) XXX_Merge(src proto.Message) {
	xxx_messageInfo_GetBitsSpendOrderReq.Merge(m, src)
}
func (m *GetBitsSpendOrderReq) XXX_Size() int {
	return xxx_messageInfo_GetBitsSpendOrderReq.Size(m)
}
func (m *GetBitsSpendOrderReq) XXX_DiscardUnknown() {
	xxx_messageInfo_GetBitsSpendOrderReq.DiscardUnknown(m)
}

var xxx_messageInfo_GetBitsSpendOrderReq proto.InternalMessageInfo

func (m *GetBitsSpendOrderReq) GetTransactionId() string {
	if m != nil {
		return m.TransactionId
	}
	return ""
}

type GetBitsSpendOrderResp struct {
	BitsTypes            map[string]uint32 `protobuf:"bytes,1,rep,name=bits_types,json=bitsTypes,proto3" json:"bits_types,omitempty" protobuf_key:"bytes,1,opt,name=key,proto3" protobuf_val:"varint,2,opt,name=value,proto3"`
	XXX_NoUnkeyedLiteral struct{}          `json:"-"`
	XXX_unrecognized     []byte            `json:"-"`
	XXX_sizecache        int32             `json:"-"`
}

func (m *GetBitsSpendOrderResp) Reset()         { *m = GetBitsSpendOrderResp{} }
func (m *GetBitsSpendOrderResp) String() string { return proto.CompactTextString(m) }
func (*GetBitsSpendOrderResp) ProtoMessage()    {}
func (*GetBitsSpendOrderResp) Descriptor() ([]byte, []int) {
	return fileDescriptor_f348e9b303f982a3, []int{11}
}

func (m *GetBitsSpendOrderResp) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_GetBitsSpendOrderResp.Unmarshal(m, b)
}
func (m *GetBitsSpendOrderResp) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_GetBitsSpendOrderResp.Marshal(b, m, deterministic)
}
func (m *GetBitsSpendOrderResp) XXX_Merge(src proto.Message) {
	xxx_messageInfo_GetBitsSpendOrderResp.Merge(m, src)
}
func (m *GetBitsSpendOrderResp) XXX_Size() int {
	return xxx_messageInfo_GetBitsSpendOrderResp.Size(m)
}
func (m *GetBitsSpendOrderResp) XXX_DiscardUnknown() {
	xxx_messageInfo_GetBitsSpendOrderResp.DiscardUnknown(m)
}

var xxx_messageInfo_GetBitsSpendOrderResp proto.InternalMessageInfo

func (m *GetBitsSpendOrderResp) GetBitsTypes() map[string]uint32 {
	if m != nil {
		return m.BitsTypes
	}
	return nil
}

func init() {
	proto.RegisterEnum("code.justin.tv.commerce.pachter.Platform", Platform_name, Platform_value)
	proto.RegisterType((*HealthCheckReq)(nil), "code.justin.tv.commerce.pachter.HealthCheckReq")
	proto.RegisterType((*HealthCheckResp)(nil), "code.justin.tv.commerce.pachter.HealthCheckResp")
	proto.RegisterType((*GetBitsMonthlyReportReq)(nil), "code.justin.tv.commerce.pachter.GetBitsMonthlyReportReq")
	proto.RegisterType((*GetBitsMonthlyReportResp)(nil), "code.justin.tv.commerce.pachter.GetBitsMonthlyReportResp")
	proto.RegisterType((*GetEmoteUsageMonthlyReportReq)(nil), "code.justin.tv.commerce.pachter.GetEmoteUsageMonthlyReportReq")
	proto.RegisterType((*GetEmoteUsageMonthlyReportResp)(nil), "code.justin.tv.commerce.pachter.GetEmoteUsageMonthlyReportResp")
	proto.RegisterType((*GetActualsReq)(nil), "code.justin.tv.commerce.pachter.GetActualsReq")
	proto.RegisterType((*GetActualsResp)(nil), "code.justin.tv.commerce.pachter.GetActualsResp")
	proto.RegisterMapType((map[string]uint32)(nil), "code.justin.tv.commerce.pachter.GetActualsResp.ActualsEntry")
	proto.RegisterType((*SetActualsReq)(nil), "code.justin.tv.commerce.pachter.SetActualsReq")
	proto.RegisterMapType((map[string]uint32)(nil), "code.justin.tv.commerce.pachter.SetActualsReq.ActualsEntry")
	proto.RegisterType((*SetActualsResp)(nil), "code.justin.tv.commerce.pachter.SetActualsResp")
	proto.RegisterType((*GetBitsSpendOrderReq)(nil), "code.justin.tv.commerce.pachter.GetBitsSpendOrderReq")
	proto.RegisterType((*GetBitsSpendOrderResp)(nil), "code.justin.tv.commerce.pachter.GetBitsSpendOrderResp")
	proto.RegisterMapType((map[string]uint32)(nil), "code.justin.tv.commerce.pachter.GetBitsSpendOrderResp.BitsTypesEntry")
}

func init() { proto.RegisterFile("rpc/pachter-service.proto", fileDescriptor_f348e9b303f982a3) }

var fileDescriptor_f348e9b303f982a3 = []byte{
	// 647 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xc4, 0x56, 0xdf, 0x4f, 0xd3, 0x50,
	0x14, 0xa6, 0xdb, 0xd8, 0xd8, 0xc1, 0xd5, 0x72, 0x83, 0x71, 0x36, 0x41, 0x49, 0x13, 0x13, 0x34,
	0xb1, 0x18, 0x8c, 0x06, 0x10, 0x25, 0x05, 0x16, 0x58, 0x02, 0xeb, 0xd2, 0x82, 0x51, 0x5e, 0x48,
	0x69, 0xaf, 0xae, 0xd2, 0xb5, 0x77, 0xf7, 0xde, 0x91, 0xec, 0xcd, 0x07, 0x5f, 0x7d, 0xf2, 0xc5,
	0x37, 0x5f, 0xfc, 0x2f, 0xfc, 0xe7, 0x4c, 0x7f, 0x0c, 0x56, 0x18, 0x6e, 0x1d, 0x31, 0xbe, 0xdd,
	0x73, 0xee, 0x39, 0xdf, 0xf9, 0xee, 0x77, 0x7b, 0xbf, 0x14, 0x1e, 0x50, 0x62, 0x2f, 0x13, 0xcb,
	0x6e, 0x71, 0x4c, 0x9f, 0x31, 0x4c, 0xcf, 0x5d, 0x1b, 0xab, 0x84, 0x06, 0x3c, 0x40, 0x8f, 0xec,
	0xc0, 0xc1, 0xea, 0xe7, 0x2e, 0xe3, 0xae, 0xaf, 0xf2, 0x73, 0xd5, 0x0e, 0xda, 0x6d, 0x4c, 0xc3,
	0xed, 0xb8, 0x5c, 0x91, 0x40, 0xdc, 0xc3, 0x96, 0xc7, 0x5b, 0xdb, 0x2d, 0x6c, 0x9f, 0x19, 0xb8,
	0xa3, 0xcc, 0xc1, 0xdd, 0x54, 0x86, 0x11, 0x65, 0x1b, 0xee, 0xef, 0x62, 0xbe, 0xe5, 0x72, 0x76,
	0x10, 0xf8, 0xbc, 0xe5, 0xf5, 0x0c, 0x4c, 0x02, 0xca, 0x0d, 0xdc, 0x41, 0xf3, 0x30, 0xdd, 0x0e,
	0x73, 0x55, 0x61, 0x51, 0x58, 0xaa, 0x18, 0x71, 0x80, 0x10, 0x14, 0x7a, 0xd8, 0xa2, 0xd5, 0x5c,
	0x94, 0x8c, 0xd6, 0xca, 0x1a, 0x54, 0x87, 0x83, 0x30, 0x82, 0x16, 0x00, 0x68, 0x14, 0x9d, 0x74,
	0xa9, 0x17, 0x41, 0x95, 0x8d, 0x72, 0x9c, 0x39, 0xa2, 0x9e, 0x52, 0x87, 0x85, 0x5d, 0xcc, 0x6b,
	0xed, 0x80, 0xe3, 0x23, 0x66, 0x7d, 0xc2, 0xb7, 0x60, 0xb1, 0x09, 0x0f, 0xff, 0x06, 0x35, 0x9a,
	0xcb, 0x17, 0x01, 0x2a, 0xbb, 0x98, 0x6b, 0x36, 0xef, 0x5a, 0x1e, 0x0b, 0x87, 0xd7, 0x60, 0x86,
	0x78, 0x16, 0xff, 0x18, 0xd0, 0x76, 0x54, 0x2e, 0xae, 0x3c, 0x51, 0x47, 0xc8, 0xae, 0x36, 0x93,
	0x06, 0xe3, 0xa2, 0xf5, 0xf2, 0x0c, 0xb9, 0x61, 0x67, 0xc8, 0x0f, 0x9c, 0xe1, 0x97, 0x00, 0xe2,
	0x20, 0x05, 0x46, 0xd0, 0x3b, 0x28, 0x59, 0x71, 0x58, 0x15, 0x16, 0xf3, 0x4b, 0xb3, 0x2b, 0x1b,
	0x23, 0x29, 0xa4, 0x11, 0xd4, 0x64, 0x5d, 0xf3, 0x39, 0xed, 0x19, 0x7d, 0x30, 0x79, 0x1d, 0xee,
	0x0c, 0x6e, 0x20, 0x09, 0xf2, 0x67, 0xb8, 0x97, 0xa8, 0x12, 0x2e, 0x43, 0xda, 0xe7, 0x96, 0xd7,
	0xc5, 0x7d, 0xda, 0x51, 0xb0, 0x9e, 0x5b, 0x15, 0x94, 0xef, 0x39, 0xa8, 0x98, 0xff, 0x55, 0x29,
	0x74, 0x74, 0x29, 0x4b, 0x21, 0x92, 0xe5, 0xf5, 0xc8, 0x79, 0x29, 0xc6, 0xff, 0x40, 0x15, 0x09,
	0x44, 0x33, 0xa5, 0xbc, 0xf2, 0x06, 0xe6, 0x93, 0x87, 0x61, 0x12, 0xec, 0x3b, 0x3a, 0x75, 0x30,
	0x0d, 0xd5, 0x7a, 0x0c, 0x22, 0xa7, 0x96, 0xcf, 0x2c, 0x9b, 0xbb, 0x81, 0x7f, 0xe2, 0x3a, 0xc9,
	0x80, 0xca, 0x40, 0xb6, 0xee, 0x28, 0xbf, 0x05, 0xb8, 0x37, 0xa4, 0x9f, 0x11, 0xe4, 0x00, 0x9c,
	0xba, 0x9c, 0x9d, 0xf0, 0x1e, 0xc1, 0xfd, 0xef, 0xa2, 0x36, 0xce, 0x77, 0x71, 0x1d, 0x4b, 0x0d,
	0x53, 0x87, 0x21, 0x4e, 0x2c, 0x45, 0xf9, 0xb4, 0x1f, 0xcb, 0x1b, 0x20, 0xa6, 0x37, 0xb3, 0xc8,
	0xf1, 0xf4, 0x18, 0x66, 0xfa, 0x37, 0x8c, 0x66, 0xa0, 0xd0, 0xd0, 0x1b, 0x35, 0x69, 0x0a, 0x01,
	0x14, 0xb5, 0x03, 0xed, 0x58, 0x6f, 0x48, 0x42, 0xb8, 0x6e, 0x6a, 0x1f, 0x9a, 0xda, 0xbe, 0x94,
	0x43, 0x25, 0xc8, 0xd7, 0x75, 0x53, 0xca, 0xa3, 0x59, 0x28, 0x69, 0x8d, 0x1d, 0x43, 0xaf, 0xef,
	0x48, 0x85, 0xb0, 0xe2, 0xbd, 0xa9, 0xef, 0xef, 0x6b, 0xd2, 0x74, 0x54, 0xbd, 0xa7, 0x35, 0x0e,
	0x75, 0xa9, 0xb8, 0xf2, 0xa3, 0x08, 0xa5, 0x66, 0x7c, 0x2a, 0x44, 0x61, 0x76, 0xc0, 0xd5, 0xd0,
	0xf2, 0x48, 0x19, 0xd2, 0xae, 0x28, 0x3f, 0xcf, 0xd6, 0xc0, 0x88, 0x32, 0x85, 0xbe, 0x09, 0x17,
	0x37, 0x9b, 0xb2, 0x19, 0xb4, 0x3a, 0xee, 0x25, 0x5c, 0x35, 0x3a, 0x79, 0x6d, 0xc2, 0xce, 0x88,
	0xcf, 0x4f, 0x01, 0xe4, 0x9b, 0xcd, 0x0f, 0xbd, 0x1d, 0x07, 0xfb, 0x66, 0x13, 0x96, 0x37, 0x6f,
	0xd5, 0x1f, 0x31, 0x0c, 0x00, 0x2e, 0x6d, 0x09, 0xa9, 0x99, 0x3c, 0xac, 0x23, 0x2f, 0x67, 0xf4,
	0xbc, 0x78, 0xa0, 0x99, 0x65, 0xa0, 0x99, 0x71, 0xa0, 0x79, 0x75, 0xe0, 0x57, 0x01, 0xe6, 0xae,
	0xbd, 0x30, 0xf4, 0x72, 0x92, 0x57, 0xd9, 0x91, 0x5f, 0x4d, 0xf6, 0x98, 0x95, 0xa9, 0xad, 0xf2,
	0x71, 0x29, 0x29, 0x39, 0x2d, 0x46, 0x7f, 0x0a, 0x2f, 0xfe, 0x04, 0x00, 0x00, 0xff, 0xff, 0xcc,
	0xa9, 0x4f, 0xd3, 0x46, 0x08, 0x00, 0x00,
}
