package sns

import (
	"time"
)

type RevenueEventMessage struct {
	ChannelId       string    `json:"channelid"`
	TimeOfEvent     time.Time `json:"timeofevent"`
	TransactionId   string    `json:"transactionid"`
	TransactionType string    `json:"transactiontype"`
	RevenueSource   string    `json:"revenuesource"`
	Revenue         float64   `json:"revenue"`
	ParticipantRole string    `json:"participantrole"`
	SKU             string    `json:"sku"`
	ZipCode         string    `json:"zipcode"`
}
