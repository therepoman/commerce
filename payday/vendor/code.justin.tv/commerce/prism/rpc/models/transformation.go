package models

import prism "code.justin.tv/commerce/prism/rpc"

const (
	TransformationArg_TokenToAppend   = "TokenToAppend"
	TransformationArg_CheermotePrefix = "CheermotePrefix"
	TransformationArg_TokenToFind     = "TokenToFind"
	TransformationArg_TokenToReplace  = "TokenToReplace"
	TransformationArg_TokensToKeep    = "TokensToKeep"
)

func AppendTokenToEndIfNotAlreadyPresentTransformation(tokenToAppend string) *prism.Transformation {
	return &prism.Transformation{
		Type: prism.TransformationType_APPEND_TOKEN_TO_END_IF_NOT_ALREADY_PRESENT,
		Arguments: map[string]string{
			TransformationArg_TokenToAppend: tokenToAppend,
		},
	}
}

func AppendTokenAfterLastOccurrenceOfCheermoteTransformation(tokenToAppend string, cheermotePrefix string) *prism.Transformation {
	return &prism.Transformation{
		Type: prism.TransformationType_APPEND_TOKEN_AFTER_LAST_OCCURRENCE_OF_CHEERMOTE,
		Arguments: map[string]string{
			TransformationArg_TokenToAppend:   tokenToAppend,
			TransformationArg_CheermotePrefix: cheermotePrefix,
		},
	}
}

func FindAndReplaceTokenTransformation(tokenToFind string, tokenToReplace string) *prism.Transformation {
	return &prism.Transformation{
		Type: prism.TransformationType_FIND_AND_REPLACE_TOKEN,
		Arguments: map[string]string{
			TransformationArg_TokenToFind:    tokenToFind,
			TransformationArg_TokenToReplace: tokenToReplace,
		},
	}
}

func RemoveTokensExceptTransformation(tokensToKeep string) *prism.Transformation {
	return &prism.Transformation{
		Type: prism.TransformationType_REMOVE_TOKENS_EXCEPT,
		Arguments: map[string]string{
			TransformationArg_TokensToKeep: tokensToKeep,
		},
	}
}
