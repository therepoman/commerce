package events

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/sns"
	"github.com/aws/aws-sdk-go/service/sns/snsiface"

	"code.justin.tv/chat/timing"

	"github.com/cactus/go-statsd-client/statsd"
)

type Publisher interface {
	// Publishes an Event to send notification(s)
	Publish(ctx context.Context, eventName string, data interface{}) error
}

const (
	xactGroup             = "sns"
	defaultStatSampleRate = 0.1
)

type Config struct {
	DispatchSNSARN string
	Stats          statsd.Statter
	SNSClient      snsiface.SNSAPI
}

type eventPublisherImpl struct {
	baseClient     snsiface.SNSAPI
	stats          statsd.Statter
	dispatchSNSARN string
}

func NewPublisher(conf Config) (*eventPublisherImpl, error) {
	return &eventPublisherImpl{
		baseClient:     conf.SNSClient,
		dispatchSNSARN: conf.DispatchSNSARN,
		stats:          conf.Stats,
	}, nil
}

func (p *eventPublisherImpl) Publish(ctx context.Context, eventName string, data interface{}) error {
	if eventName == "" {
		return errors.New("eventName cannot be empty")
	}
	if data == nil {
		return errors.New("data cannot be nil")
	}

	xact, ok := timing.XactFromContext(ctx)
	if ok {
		sub := xact.Sub(xactGroup)
		sub.Start()
		defer sub.End()
	}

	stat := fmt.Sprintf("service.pushy.publish_event.%v", eventName)
	_ = p.stats.Inc(stat, 1, defaultStatSampleRate)

	message, err := json.Marshal(&data)
	if err != nil {
		return err
	}

	in := &sns.PublishInput{
		Message: aws.String(string(message)),
		MessageAttributes: map[string]*sns.MessageAttributeValue{
			"event": {
				StringValue: aws.String(eventName),
				DataType:    aws.String("String"),
			},
		},
		TopicArn: aws.String(p.dispatchSNSARN),
	}

	if _, err = p.baseClient.Publish(in); err != nil {
		return fmt.Errorf("publishing to SNS: %v", err)
	}
	return nil
}
