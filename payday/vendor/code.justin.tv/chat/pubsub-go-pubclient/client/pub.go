package client

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"time"

	"code.justin.tv/common/twitchhttp"
	"github.com/cactus/go-statsd-client/statsd"
	"golang.org/x/net/context"
)

// ErrFailedRequest indicates a failure communicating with pubsub
var ErrFailedRequest = errors.New("pubsub publish failed")

const defaultStatSampleRate = 0.1
const pubTimeout = 1 * time.Second

// PubConfig defines configuration for a pubsub client
type PubConfig struct {
	// Host (required) configures the client to connect to a specific URI host.
	// If not specified, URIs created by the client will default to use "http://".
	Host string

	// Enables tracking of DNS and request timings.
	Stats statsd.Statter

	// Specify a custom stat prefix for DNS timing stats, defaults to "dns"
	DNSStatsPrefix string

	// The code.justin.tv/chat/timing sub-transaction name, defaults to "pubsub"
	TimingXactName string

	// Prefix to use for logging to statsd, defaults to "pubsub"
	StatsPrefix string
}

type clientImpl struct {
	twitchhttp.Client
	conf PubConfig
}

// NewPubClient creates a new client to communicate to pubsub.
func NewPubClient(conf PubConfig) (PubClient, error) {
	if conf.Host == "" {
		log.Printf("Using Noop Pub Client")
		return &noopClientImpl{}, nil
	}

	if conf.StatsPrefix == "" {
		conf.StatsPrefix = "pubsub"
	}
	if conf.TimingXactName == "" {
		conf.TimingXactName = "pubsub"
	}

	c, err := twitchhttp.NewClient(twitchhttp.ClientConf{
		Transport: twitchhttp.TransportConf{
			MaxIdleConnsPerHost: 100,
		},
		Host:           conf.Host,
		Stats:          conf.Stats,
		DNSStatsPrefix: conf.DNSStatsPrefix,
		TimingXactName: conf.TimingXactName,
	})
	if err != nil {
		return nil, err
	}

	return &clientImpl{Client: c, conf: conf}, nil
}

// PubClient is the interface that all pubsub clients should support.
type PubClient interface {
	Publish(context.Context, []string, string, *twitchhttp.ReqOpts) error
}

func (p *clientImpl) Publish(ctx context.Context, topics []string, message string, opts *twitchhttp.ReqOpts) error {
	if len(topics) == 0 {
		return nil
	}

	ctx, cancelFunc := context.WithTimeout(ctx, pubTimeout)
	defer cancelFunc()

	data, err := generateMessage(topics, message)
	if err != nil {
		return err
	}

	combinedReqOpts := twitchhttp.MergeReqOpts(opts, twitchhttp.ReqOpts{
		StatName:       fmt.Sprintf("%s.%s", p.conf.StatsPrefix, "publish"),
		StatSampleRate: defaultStatSampleRate,
	})

	err = p.postMessage(ctx, data, combinedReqOpts)
	return err
}

type pubMessage struct {
	Topics  []string `json:"topics"`
	Message string   `json:"message"`
}

// GenerateMessage creates the message that will be posted to the pubsub broker.
func generateMessage(topics []string, data string) (pubMessage, error) {
	ret := pubMessage{Topics: topics, Message: data}
	return ret, nil
}

// PostMessage sends a messsage to the pubsub broker.
func (p *clientImpl) postMessage(ctx context.Context, data pubMessage, reqOpts twitchhttp.ReqOpts) error {
	dataJSON, err := json.Marshal(data)
	if err != nil {
		return err
	}

	req, err := p.NewRequest("POST", "/v1/message", bytes.NewReader(dataJSON))
	if err != nil {
		return err
	}
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Content-Length", strconv.Itoa(len(dataJSON)))

	resp, err := p.Do(ctx, req, reqOpts)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return ErrFailedRequest
	}
	return nil
}

type noopClientImpl struct{}

func (p *noopClientImpl) Publish(ctx context.Context, topics []string, message string, opts *twitchhttp.ReqOpts) error {
	log.Printf("Noop Publish - topics: %v, message: %s", topics, message)
	return nil
}
