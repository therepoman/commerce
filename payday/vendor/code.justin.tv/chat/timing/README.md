timing
======

Go library to track transaction timings.

Stats will be displayed in Graphite in the following manner:

- **stats_prefix.{Xact.Name}.{result}.total**
- **stats_prefix.{Xact.Name}.{result}.{SubXact1.Name}**
- **stats_prefix.{Xact.Name}.{result}.{SubXact2.Name}**

This is useful when you want to track the timings of various dependencies of your HTTP request. Not only does it track the total duration of a transaction, but keeps track of sub-transaction timings so that you can easily correlate spikes/dips in latency to specific dependencies.

Usage
-----

    func SomeHTTPRequestHandler(rw http.ResponseWriter, req *http.Request) {
        ctx := context.Background()
        xact := timing.Xact{
            Name: "endpoint.some_request",
            Stats: stats,
        }
        xact.Start()
        ctx = timing.XactContext(ctx)

        var status int
        err := DoSomethingInteresting(ctx)
        if err != nil {
            status = http.StatusInternalServerError
        } else {
            status = http.StatusOK
        }
        rw.WriteHeader(status)
        xact.End(strconv.Itoa(status))
    }
