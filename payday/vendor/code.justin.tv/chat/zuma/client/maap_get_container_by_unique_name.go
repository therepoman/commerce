package zuma

import (
	"bytes"
	"context"
	"encoding/json"

	"code.justin.tv/chat/zuma/app/api"
	"code.justin.tv/foundation/twitchclient"
)

func (c *client) GetContainerByUniqueName(ctx context.Context, params api.GetContainerByUniqueNameRequest, reqOpts *twitchclient.ReqOpts) (api.GetContainerByUniqueNameResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.GetContainerByUniqueNameResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/maap/containers/get_by_unique_name", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.GetContainerByUniqueNameResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.maap.get_container_by_unique_name",
		StatSampleRate: defaultStatSampleRate,
	})

	var decoded api.GetContainerByUniqueNameResponse
	if _, err := c.DoJSON(ctx, &decoded, req, combinedReqOpts); err != nil {
		return api.GetContainerByUniqueNameResponse{}, err
	}
	return decoded, nil
}
