package zuma

import (
	"bytes"
	"context"
	"encoding/json"

	"code.justin.tv/chat/zuma/app/api"
	"code.justin.tv/foundation/twitchclient"
)

func (c *client) ListRitualTokensByChannel(ctx context.Context, params api.ListRitualTokensByChannelRequest, reqOpts *twitchclient.ReqOpts) (api.ListRitualTokensByChannelResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.ListRitualTokensByChannelResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/rituals/tokens/list_by_channel", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.ListRitualTokensByChannelResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.rituals.list_tokens_by_channel",
		StatSampleRate: defaultStatSampleRate,
	})

	var decoded api.ListRitualTokensByChannelResponse
	if _, err := c.DoJSON(ctx, &decoded, req, combinedReqOpts); err != nil {
		return api.ListRitualTokensByChannelResponse{}, err
	}
	return decoded, nil
}

func (c *client) RequestEligibleRitualToken(ctx context.Context, params api.RequestEligibleRitualTokenRequest, reqOpts *twitchclient.ReqOpts) (api.RequestEligibleRitualTokenResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.RequestEligibleRitualTokenResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/rituals/tokens/request_eligible", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.RequestEligibleRitualTokenResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.rituals.request_eligible",
		StatSampleRate: defaultStatSampleRate,
	})

	var decoded api.RequestEligibleRitualTokenResponse
	if _, err := c.DoJSON(ctx, &decoded, req, combinedReqOpts); err != nil {
		return api.RequestEligibleRitualTokenResponse{}, err
	}
	return decoded, nil
}

func (c *client) UpdateRitualToken(ctx context.Context, params api.UpdateRitualTokenRequest, reqOpts *twitchclient.ReqOpts) (api.UpdateRitualTokenResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.UpdateRitualTokenResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/rituals/tokens/update", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.UpdateRitualTokenResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.rituals.update_token",
		StatSampleRate: defaultStatSampleRate,
	})

	var decoded api.UpdateRitualTokenResponse
	if _, err := c.DoJSON(ctx, &decoded, req, combinedReqOpts); err != nil {
		return api.UpdateRitualTokenResponse{}, err
	}
	return decoded, nil
}
