package zuma

import (
	"bytes"
	"context"
	"encoding/json"

	"code.justin.tv/chat/zuma/app/api"
	"code.justin.tv/foundation/twitchclient"
)

func (c *client) CheckEmoticonsAccess(ctx context.Context, params api.CheckEmoticonsAccessRequest, reqOpts *twitchclient.ReqOpts) (api.CheckEmoticonsAccessResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.CheckEmoticonsAccessResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/users/emoticons/check_access", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.CheckEmoticonsAccessResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.check_emoticons_access",
		StatSampleRate: defaultStatSampleRate,
	})

	var decoded api.CheckEmoticonsAccessResponse
	if _, err := c.DoJSON(ctx, &decoded, req, combinedReqOpts); err != nil {
		return api.CheckEmoticonsAccessResponse{}, err
	}
	return decoded, nil
}
