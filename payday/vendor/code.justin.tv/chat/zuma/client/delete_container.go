package zuma

import (
	"bytes"
	"context"
	"encoding/json"

	"code.justin.tv/chat/zuma/app/api"
	"code.justin.tv/foundation/twitchclient"
)

func (c *client) DeleteContainer(ctx context.Context, params api.DeleteContainerRequest, reqOpts *twitchclient.ReqOpts) (api.DeleteContainerResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.DeleteContainerResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/maap/containers/delete", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.DeleteContainerResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.maap.delete_container",
		StatSampleRate: defaultStatSampleRate,
	})

	var decoded api.DeleteContainerResponse
	if _, err := c.DoJSON(ctx, &decoded, req, combinedReqOpts); err != nil {
		return api.DeleteContainerResponse{}, err
	}
	return decoded, nil
}
