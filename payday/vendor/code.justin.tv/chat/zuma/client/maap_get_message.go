package zuma

import (
	"bytes"
	"context"
	"encoding/json"

	"code.justin.tv/chat/zuma/app/api"
	"code.justin.tv/foundation/twitchclient"
)

func (c *client) GetMessage(ctx context.Context, params api.GetMessageRequest, reqOpts *twitchclient.ReqOpts) (api.GetMessageResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.GetMessageResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/maap/messages/get", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.GetMessageResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.maap.get_message",
		StatSampleRate: defaultStatSampleRate,
	})

	var decoded api.GetMessageResponse
	if _, err := c.DoJSON(ctx, &decoded, req, combinedReqOpts); err != nil {
		return api.GetMessageResponse{}, err
	}
	return decoded, nil
}
