package zuma

import (
	"bytes"
	"context"
	"encoding/json"

	"code.justin.tv/chat/zuma/app/api"
	"code.justin.tv/foundation/twitchclient"
)

func (c *client) EditMessage(ctx context.Context, params api.EditMessageRequest, reqOpts *twitchclient.ReqOpts) (api.EditMessageResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.EditMessageResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/maap/messages/edit", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.EditMessageResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.maap.edit_message",
		StatSampleRate: defaultStatSampleRate,
	})

	var decoded api.EditMessageResponse
	if _, err := c.DoJSON(ctx, &decoded, req, combinedReqOpts); err != nil {
		return api.EditMessageResponse{}, err
	}
	return decoded, nil
}
