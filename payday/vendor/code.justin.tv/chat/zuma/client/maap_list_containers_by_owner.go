package zuma

import (
	"bytes"
	"context"
	"encoding/json"

	"code.justin.tv/chat/zuma/app/api"
	"code.justin.tv/foundation/twitchclient"
)

func (c *client) ListContainersByOwner(ctx context.Context, params api.ListContainersByOwnerRequest, reqOpts *twitchclient.ReqOpts) (api.ListContainersByOwnerResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.ListContainersByOwnerResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/maap/containers/list_by_owner", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.ListContainersByOwnerResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.maap.list_containers_by_owner",
		StatSampleRate: defaultStatSampleRate,
	})

	var decoded api.ListContainersByOwnerResponse
	if _, err := c.DoJSON(ctx, &decoded, req, combinedReqOpts); err != nil {
		return api.ListContainersByOwnerResponse{}, err
	}
	return decoded, nil
}
