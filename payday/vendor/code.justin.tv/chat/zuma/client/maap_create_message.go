package zuma

import (
	"bytes"
	"context"
	"encoding/json"

	"code.justin.tv/chat/zuma/app/api"
	"code.justin.tv/foundation/twitchclient"
)

func (c *client) CreateMessage(ctx context.Context, params api.CreateMessageRequest, reqOpts *twitchclient.ReqOpts) (api.CreateMessageResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.CreateMessageResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/maap/messages/create", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.CreateMessageResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.maap.create_message",
		StatSampleRate: defaultStatSampleRate,
	})

	var decoded api.CreateMessageResponse
	if _, err := c.DoJSON(ctx, &decoded, req, combinedReqOpts); err != nil {
		return api.CreateMessageResponse{}, err
	}
	return decoded, nil
}
