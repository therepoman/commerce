package zuma

import (
	"bytes"
	"context"
	"encoding/json"

	"code.justin.tv/chat/zuma/app/api"
	"code.justin.tv/foundation/twitchclient"
)

func (c *client) BulkUpdateMessages(ctx context.Context, params api.BulkUpdateMessagesRequest, reqOpts *twitchclient.ReqOpts) (api.BulkUpdateMessagesResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.BulkUpdateMessagesResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/maap/messages/bulk_update", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.BulkUpdateMessagesResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.maap.bulk_update",
		StatSampleRate: defaultStatSampleRate,
	})

	var decoded api.BulkUpdateMessagesResponse
	if _, err := c.DoJSON(ctx, &decoded, req, combinedReqOpts); err != nil {
		return api.BulkUpdateMessagesResponse{}, err
	}
	return decoded, nil
}
