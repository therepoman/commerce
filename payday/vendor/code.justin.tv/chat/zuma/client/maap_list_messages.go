package zuma

import (
	"bytes"
	"context"
	"encoding/json"

	"code.justin.tv/chat/zuma/app/api"
	"code.justin.tv/foundation/twitchclient"
)

func (c *client) ListMessages(ctx context.Context, params api.ListMessagesRequest, reqOpts *twitchclient.ReqOpts) (api.ListMessagesResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.ListMessagesResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/maap/messages/list", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.ListMessagesResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.maap.list_messages",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return api.ListMessagesResponse{}, err
	}

	var listMessagesResp api.ListMessagesResponse
	err = decodeErrorCodeResponse(ctx, resp, &listMessagesResp)
	if err != nil {
		return api.ListMessagesResponse{}, err
	}

	return listMessagesResp, nil
}
