package zuma

import (
	"bytes"
	"context"
	"encoding/json"

	"code.justin.tv/chat/zuma/app/api"
	"code.justin.tv/foundation/twitchclient"
)

func (c *client) BulkGetContainerViews(ctx context.Context, params api.BulkGetContainerViewsRequest, reqOpts *twitchclient.ReqOpts) (api.BulkGetContainerViewsResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.BulkGetContainerViewsResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/maap/container_views/bulk_get", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.BulkGetContainerViewsResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.maap.bulk_get_container_views",
		StatSampleRate: defaultStatSampleRate,
	})

	var decoded api.BulkGetContainerViewsResponse
	if _, err := c.DoJSON(ctx, &decoded, req, combinedReqOpts); err != nil {
		return api.BulkGetContainerViewsResponse{}, err
	}
	return decoded, nil
}
