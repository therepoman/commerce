package graceful

import (
	"errors"
	"sync"
	"time"
)

var (
	// ShutdownTimeout specifies the time to wait for a graceful shut down
	// before Wait should move on.
	ShutdownTimeout time.Duration

	wg          sync.WaitGroup
	waitOnce    sync.Once
	waitDone    chan struct{}
	waitTimeout <-chan time.Time

	shutdownOnce sync.Once
	shuttingDown chan struct{}
)

func init() {
	reset()
}

// Go calls the specified function in a goroutine, and provides a hook
// for the program to wait some duration before forcibly
func Go(fn func()) {
	wg.Add(1)
	go func() {
		defer wg.Done()
		fn()
	}()
}

// Shutdown signals to graceful goroutines to stop.
func Shutdown() {
	shutdownOnce.Do(func() {
		close(shuttingDown)
	})
}

// ShuttingDown signals to graceful goroutines to gracefully shut down.
// If they don't shut down within `ShutdownTimeout` they may be forcefully
// shut down.
func ShuttingDown() <-chan struct{} {
	return shuttingDown
}

// Wait waits for all graceful goroutines to stop. If goroutines have not stopped
// after `ShutdownTimeout` it returns an error.
func Wait() error {
	waitOnce.Do(func() {
		waitTimeout = time.After(ShutdownTimeout)
		go func() {
			wg.Wait()
			close(waitDone)
		}()
	})

	select {
	case <-waitDone:
		return nil
	case <-waitTimeout:
		return errors.New("graceful: timeout expired before all goroutines exited")
	}
}

// resets state for tests. this is not concurrency safe
func reset() {
	ShutdownTimeout = 20 * time.Second
	wg = sync.WaitGroup{}
	waitOnce = sync.Once{}
	waitDone = make(chan struct{})
	waitTimeout = nil
	shutdownOnce = sync.Once{}
	shuttingDown = make(chan struct{})
}
