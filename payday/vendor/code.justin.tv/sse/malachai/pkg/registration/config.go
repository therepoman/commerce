package registration

import (
	"code.justin.tv/sse/malachai/pkg/config"
	"github.com/aws/aws-sdk-go/aws"
)

// dynamodb names
const (
	serviceNameKey   = "service_name"
	serviceByIDIndex = "services-by-id"
)

// Config holds options for service registration
type Config struct {
	Region             string
	RoleArn            string
	TableName          string
	Environment        string // testing or production
	InventoryTableName string

	AWSConfigBase *aws.Config

	// //Sandstorm policy generator
	SandstormKMSKeyID             string
	SandstormSecretsTableName     string
	SandstormRoleArn              string
	SandstormAuxPolicyArn         string
	SandstormSecretsTableArn      string
	SandstormSecretsAuditTableArn string
	SandstormNamespaceTableArn    string
	SandstormRoleOwnerTableName   string
}

// FillDefaults fills empty values with defaults
func (c *Config) FillDefaults() (err error) {
	if c.Environment == "" {
		c.Environment = "production"
	}

	resources, err := config.GetResources(c.Environment)
	if err != nil {
		return
	}

	if c.Region == "" {
		c.Region = resources.Region
	}

	if c.TableName == "" {
		c.TableName = resources.ServicesTable
	}

	if c.SandstormAuxPolicyArn == "" {
		c.SandstormAuxPolicyArn = resources.SandstormAuxPolicyArn
	}

	if c.SandstormRoleOwnerTableName == "" {
		c.SandstormRoleOwnerTableName = resources.SandstormRoleOwnerTableName
	}

	if c.SandstormSecretsTableArn == "" {
		c.SandstormSecretsTableArn = resources.SandstormSecretsTableArn
	}

	if c.SandstormSecretsTableName == "" {
		c.SandstormSecretsTableName = resources.SandstormSecretsTableName
	}

	if c.SandstormKMSKeyID == "" {
		c.SandstormKMSKeyID = resources.SandstormKMSKeyID
	}

	if c.SandstormRoleArn == "" {
		c.SandstormRoleArn = resources.SandstormRoleArn
	}

	if c.SandstormSecretsAuditTableArn == "" {
		c.SandstormSecretsAuditTableArn = resources.SandstormSecretsAuditTableArn
	}

	if c.SandstormNamespaceTableArn == "" {
		c.SandstormNamespaceTableArn = resources.SandstormNamespaceTableArn
	}

	if c.InventoryTableName == "" {
		c.InventoryTableName = resources.InventoryTableName
	}

	return
}
