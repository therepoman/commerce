package errors

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
)

// ExtractFromResponse checks the status code on an HTTP Response to detect if
// an error has occurred and then attempts to extract the error from the body
// when appropriate. If a service error (5XX) is present and has no error code,
// it is scrubbed into a generic error so that we don't leak implementation
// details behind our service.  Specific 5XX errors that have support for an
// error_code are interpreted as safe for end users to see.
//
// This function does not attempt to close the response body, which must be done
// even if this function returns content.  The recommended use is to defer the
// close from the logic that calls this function.
func ExtractFromHTTPResponse(resp *http.Response) (error, bool) {
	if err, found := GenericErrorForHTTPStatus(resp.StatusCode); found {
		if resp.Body != nil {
			if buffer, err := ioutil.ReadAll(resp.Body); err == nil {
				if result, err := UnmarshalErrorForHTTP(buffer); err == nil {
					return httpSafetyCheck(resp, result)
				}
			}
		}
		return err, found
	}
	return nil, false
}

// Returns a standard HTTP error for the status code that can be immediately
// queried for its error code. Returns false if the code does not indicate an error
func GenericErrorForHTTPStatus(status int) (StandardHTTPError, bool) {
	if status < 400 {
		return nil, false
	}
	if err, ok := httpErrorMap[status]; ok {
		return err, true
	}
	message := fmt.Sprintf("HTTP Error: %d", status)
	code := fmt.Sprintf("http_%d", status)
	return newImpl(message, &code, &status, nil).(StandardHTTPError), true
}

// StandardHTTPError is the type of all standard errors provided by this
// package; it allows immediate query of both the HTTP status and error code
// without any cast.
type StandardHTTPError interface {
	StructuredError
	HTTPStatusSource
	ErrorCodeSource
}

// look for 5XX without an error code and substitute the generic equivalent so
// we don't leak details about the source of the problem to end users. If an
// error code already exists assume that the service built the error for client
// consumption and that it doesn't contain dangerous details.
func httpSafetyCheck(resp *http.Response, err error) (error, bool) {
	if _, ok := err.(ErrorCodeSource); ok || resp.StatusCode < 500 {
		return err, true
	}
	return GenericErrorForHTTPStatus(resp.StatusCode)
}

var (
	// this is built by the declarations in standard_4xx and standard_5xx
	httpErrorMap = make(map[int]StandardHTTPError)
)

// use a name and status to generate a templated error and store it in the map
// by code for lookup. Used by var declarations at package initialization time
func makeHttpError(status int) StandardHTTPError {
	name := http.StatusText(status)
	code := "http_" + strings.Replace(strings.ToLower(name), " ", "_", -1)
	err := newImpl(fmt.Sprintf("%d: %s", status, name), &code, &status, nil).(StandardHTTPError)
	httpErrorMap[status] = err
	return err
}
