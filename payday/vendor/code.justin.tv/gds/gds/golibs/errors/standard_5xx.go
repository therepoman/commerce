package errors

import "net/http"

// comment source: https://en.wikipedia.org/wiki/List_of_HTTP_status_codes
var (
	// ErrInternalServerError - A generic error message, given when an unexpected condition was encountered and no more specific message is suitable
	ErrInternalServerError = makeHttpError(http.StatusInternalServerError)
	// ErrNotImplemented- The server either does not recognize the request method, or it lacks the ability to fulfil the request. Usually this implies future availability (e.g., a new feature of a web-service API)
	ErrNotImplemented = makeHttpError(http.StatusNotImplemented)
	// ErrBadGateway - The server was acting as a gateway or proxy and received an invalid response from the upstream server
	ErrBadGateway = makeHttpError(http.StatusBadGateway)
	// ErrServiceUnavailable - The server is currently unavailable (because it is overloaded or down for maintenance)
	ErrServiceUnavailable = makeHttpError(http.StatusServiceUnavailable)
	// ErrStatusGatewayTimeout - The server was acting as a gateway or proxy and did not receive a timely response from the upstream server
	ErrStatusGatewayTimeout = makeHttpError(http.StatusGatewayTimeout)
	// ErrHTTPVersionNotSupported - The server does not support the HTTP protocol version used in the request
	ErrHTTPVersionNotSupported = makeHttpError(http.StatusHTTPVersionNotSupported)
	// ErrVariantAlsoNegotiates - Transparent content negotiation for the request results in a circular reference
	ErrVariantAlsoNegotiates = makeHttpError(http.StatusVariantAlsoNegotiates)
	// ErrInsufficientStorage - The server is unable to store the representation needed to complete the request
	ErrInsufficientStorage = makeHttpError(http.StatusInsufficientStorage)
	// ErrLoopDetected - The server detected an infinite loop while processing the request (sent in lieu of 208 Already Reported)
	ErrLoopDetected = makeHttpError(http.StatusLoopDetected)
	// ErrsNotExtended - Further extensions to the request are required for the server to fulfil it
	ErrsNotExtended = makeHttpError(http.StatusNotExtended)
	// ErrNetworkAuthenticationRequired - The client needs to authenticate to gain network access. Intended for use by intercepting proxies used to control access to the network
	ErrNetworkAuthenticationRequired = makeHttpError(http.StatusNetworkAuthenticationRequired)
)
