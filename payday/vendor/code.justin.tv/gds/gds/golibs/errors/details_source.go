package errors

// Details defines the public interface for expressing error parameters
type Details map[string]interface{}

// DetailsSource returns an arbitrary map of parameters
type DetailsSource interface {
	Details() Details
}

// DetailsError is an error with Details
type DetailsError interface {
	DetailsSource
	error
}

// DetailsAdaptable can return a copy as an DetailsError
type DetailsAdaptable interface {
	WithDetails(details Details) DetailsError
}

// WithDetails attaches or replaces the error code on an existing error
func WithDetails(e error, details Details) DetailsError {
	if cast, ok := e.(DetailsAdaptable); ok {
		return cast.WithDetails(details)
	}
	return extract(e).WithDetails(details)
}

// GetDetails extracts the details from an error or returns an empty set
func GetDetails(e error) Details {
	if cast, ok := e.(DetailsSource); ok {
		return cast.Details()
	}
	return Details{}
}
