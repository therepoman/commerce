package errors

// ErrorCodeSource returns an error code when queried
type ErrorCodeSource interface {
	ErrorCode() string
}

// ErrorCodeError is an error with an error code
type ErrorCodeError interface {
	ErrorCodeSource
	error
}

// ErrorCodeAdaptable can return a copy as an ErrorCodeError
type ErrorCodeAdaptable interface {
	WithErrorCode(errorCode string) ErrorCodeError
}

// WithErrorCode attaches or replaces the error code on an existing error
func WithErrorCode(e error, errorCode string) ErrorCodeError {
	if cast, ok := e.(ErrorCodeAdaptable); ok {
		return cast.WithErrorCode(errorCode)
	}
	return extract(e).WithErrorCode(errorCode)
}

// GetErrorCode extracts an error code from an error or returns empty string by default
func GetErrorCode(e error) string {
	if cast, ok := e.(ErrorCodeSource); ok {
		return cast.ErrorCode()
	}
	return ""
}
