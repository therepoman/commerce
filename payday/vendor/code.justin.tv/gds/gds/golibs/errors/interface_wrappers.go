package errors

// Interface wrappers forward information from the common error Impl to provide
// type safety/duck typing with minimal copy and paste.
const (
	fieldErrorCode  = 1 << 0
	fieldHTTPStatus = 1 << 1
	fieldDetails    = 1 << 2
)

// allows getting to the base of an error type
type implOrWrapper interface {
	unwrap() *errorImpl
}

var _ implOrWrapper = (*errorImpl)(nil)

// place an interface wrapper around the impl that refects its current available
// fields
func (e *errorImpl) wrap() StructuredError {
	fields := 0
	if e.ErrorCode != nil {
		fields = fields + fieldErrorCode
	}
	if e.HTTPStatus != nil {
		fields = fields + fieldHTTPStatus
	}
	if e.Details != nil {
		fields = fields + fieldDetails
	}
	switch fields {
	case fieldErrorCode:
		return &errCodeWrapper{e}
	case fieldHTTPStatus:
		return &httpStatusWrapper{e}
	case fieldErrorCode + fieldHTTPStatus:
		return &errCodeHTTPStatusWrapper{e}
	case fieldDetails:
		return &detailsWrapper{e}
	case fieldErrorCode + fieldDetails:
		return &errCodeDetailsWrapper{e}
	case fieldHTTPStatus + fieldDetails:
		return &httpStatusDetailsWrapper{e}
	case fieldErrorCode + fieldHTTPStatus + fieldDetails:
		return &errCodeHTTPStatusDetailsWrapper{e}
	default: // unknown field or no fields -- don't wrap
		return e
	}
}

func (e *errorImpl) unwrap() *errorImpl { return e }

type errCodeWrapper struct{ *errorImpl }

var _ ErrorCodeError = (*errCodeWrapper)(nil)
var _ implOrWrapper = (*errCodeWrapper)(nil)

func (w *errCodeWrapper) ErrorCode() string  { return *w.errorImpl.ErrorCode }
func (w *errCodeWrapper) unwrap() *errorImpl { return w.errorImpl }

type httpStatusWrapper struct{ *errorImpl }

var _ HTTPStatusError = (*httpStatusWrapper)(nil)
var _ implOrWrapper = (*httpStatusWrapper)(nil)

func (w *httpStatusWrapper) HTTPStatus() int    { return *w.errorImpl.HTTPStatus }
func (w *httpStatusWrapper) unwrap() *errorImpl { return w.errorImpl }

type detailsWrapper struct{ *errorImpl }

var _ DetailsError = (*detailsWrapper)(nil)
var _ implOrWrapper = (*detailsWrapper)(nil)

func (w *detailsWrapper) Details() Details   { return *w.errorImpl.Details }
func (w *detailsWrapper) unwrap() *errorImpl { return w.errorImpl }

type errCodeHTTPStatusWrapper struct{ *errorImpl }

var _ ErrorCodeError = (*errCodeHTTPStatusWrapper)(nil)
var _ HTTPStatusError = (*errCodeHTTPStatusWrapper)(nil)
var _ implOrWrapper = (*httpStatusWrapper)(nil)

func (w *errCodeHTTPStatusWrapper) ErrorCode() string  { return *w.errorImpl.ErrorCode }
func (w *errCodeHTTPStatusWrapper) HTTPStatus() int    { return *w.errorImpl.HTTPStatus }
func (w *errCodeHTTPStatusWrapper) unwrap() *errorImpl { return w.errorImpl }

type errCodeDetailsWrapper struct{ *errorImpl }

var _ ErrorCodeError = (*errCodeDetailsWrapper)(nil)
var _ DetailsError = (*errCodeDetailsWrapper)(nil)
var _ implOrWrapper = (*httpStatusWrapper)(nil)

func (w *errCodeDetailsWrapper) ErrorCode() string  { return *w.errorImpl.ErrorCode }
func (w *errCodeDetailsWrapper) Details() Details   { return *w.errorImpl.Details }
func (w *errCodeDetailsWrapper) unwrap() *errorImpl { return w.errorImpl }

type httpStatusDetailsWrapper struct{ *errorImpl }

var _ HTTPStatusError = (*httpStatusDetailsWrapper)(nil)
var _ DetailsError = (*httpStatusDetailsWrapper)(nil)
var _ implOrWrapper = (*httpStatusDetailsWrapper)(nil)

func (w *httpStatusDetailsWrapper) Details() Details   { return *w.errorImpl.Details }
func (w *httpStatusDetailsWrapper) HTTPStatus() int    { return *w.errorImpl.HTTPStatus }
func (w *httpStatusDetailsWrapper) unwrap() *errorImpl { return w.errorImpl }

type errCodeHTTPStatusDetailsWrapper struct{ *errorImpl }

var _ ErrorCodeError = (*errCodeHTTPStatusDetailsWrapper)(nil)
var _ HTTPStatusError = (*errCodeHTTPStatusDetailsWrapper)(nil)
var _ DetailsError = (*errCodeHTTPStatusDetailsWrapper)(nil)
var _ implOrWrapper = (*errCodeHTTPStatusDetailsWrapper)(nil)

func (w *errCodeHTTPStatusDetailsWrapper) ErrorCode() string  { return *w.errorImpl.ErrorCode }
func (w *errCodeHTTPStatusDetailsWrapper) Details() Details   { return *w.errorImpl.Details }
func (w *errCodeHTTPStatusDetailsWrapper) HTTPStatus() int    { return *w.errorImpl.HTTPStatus }
func (w *errCodeHTTPStatusDetailsWrapper) unwrap() *errorImpl { return w.errorImpl }
