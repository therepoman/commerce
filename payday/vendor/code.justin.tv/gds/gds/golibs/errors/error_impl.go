package errors

// StructuredError can hold error codes, http status codes, and parameter maps
type StructuredError interface {
	error
	ErrorCodeAdaptable
	HTTPStatusAdaptable
	DetailsAdaptable
	implOrWrapper
}

// errorImpl holds information for any error interface; it can be wrapped to
// match the getters that its current data supports so that duck typing works
// as expected.
type errorImpl struct {
	Message    string   `json:"message"`
	ErrorCode  *string  `json:"error_code,omitempty"`
	Details    *Details `json:"details,omitempty"`
	HTTPStatus *int     `json:"status,omitempty"`
}

var _ StructuredError = (*errorImpl)(nil)

func newImpl(message string, errorCode *string, status *int, details *Details) error {
	out := &errorImpl{
		ErrorCode:  errorCode,
		HTTPStatus: status,
		Message:    message,
		Details:    details,
	}
	return out.wrap()
}

// see `errconv` subpackage for a more intelligent set of extractors but with
// more compile time dependencies that are suitable for gds services
func extract(err error) *errorImpl {
	if cast, ok := err.(implOrWrapper); ok {
		return cast.unwrap()
	}

	out := &errorImpl{Message: err.Error()}

	if cast, ok := err.(DetailsSource); ok {
		details := cast.Details()
		out.Details = &details
	}

	if cast, ok := err.(HTTPStatusSource); ok {
		status := cast.HTTPStatus()
		out.HTTPStatus = &status
	}

	if cast, ok := err.(ErrorCodeSource); ok {
		code := cast.ErrorCode()
		out.ErrorCode = &code
	}
	return out
}

func (e *errorImpl) Error() string { return e.Message }
func (e *errorImpl) WithErrorCode(code string) ErrorCodeError {
	return newImpl(e.Message, &code, e.HTTPStatus, e.Details).(ErrorCodeError)
}

func (e *errorImpl) WithHTTPStatus(status int) HTTPStatusError {
	return newImpl(e.Message, e.ErrorCode, &status, e.Details).(HTTPStatusError)
}

func (e *errorImpl) WithDetails(details Details) DetailsError {
	return newImpl(e.Message, e.ErrorCode, e.HTTPStatus, &details).(DetailsError)
}
