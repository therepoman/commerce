package models

import (
	"encoding/json"
	"errors"
	"fmt"
	"time"

	"code.justin.tv/gds/gds/extensions/ems/protocol"
)

// ExtensionState is an enum that represents the nine states an extension can
// be in during its lifecycle.
type ExtensionState int

const (
	// InTest extensions are currently being worked on by a developer.
	InTest ExtensionState = iota
	// ReadyForReview extensions are currently being examined by a developer
	// for their readiness to be reviewed (assets hosted on CDN)
	ReadyForReview = iota
	// InReview extensions are being looked at by Twitch.
	InReview = iota
	// Rejected extensions are permanently rejected; no action by the
	// developer will make them acceptable.
	Rejected = iota
	// Approved extensions are ready to be released, at the developer's
	// convenience.
	Approved = iota
	// Released extensions are currently available to users.  Only one
	// version of an extension can be in this state at any time.
	Released = iota
	// Deprecated extensions were once released, but a newer version now exists.
	Deprecated = iota
	// PendingAction extensions are ones that were reviewed by Twitch and
	// returned to the developer for fixing.
	PendingAction = iota
	// Uploading extensions are in the process of having their assets loaded
	// onto S3.
	Uploading = iota
	// AssetsUploaded extensions have had their assets uploaded already to S3.
	// This state mirrors the existing ReadyForReview state, but doesn't incur
	// an implicit upload
	AssetsUploaded = iota
	// Deleted extensions have been deleted; this is a soft delete so we can
	// undelete things.
	Deleted = iota
)

var (
	// StatesByName maps state transition request strings to internal extension states
	StatesByName = map[string]ExtensionState{
		"test":             InTest,
		"ready_for_review": ReadyForReview,
		"review":           InReview,
		"rejected":         Rejected,
		"approved":         Approved,
		"released":         Released,
		"deleted":          Deleted,
		"deprecated":       Deprecated,
		"pending_action":   PendingAction,
		"assets_uploaded":  AssetsUploaded,
	}
	// HumanNameByState maps ExtensionState to human-readable text
	HumanNameByState = map[ExtensionState]string{
		InTest:         "Testing",
		ReadyForReview: "Ready For Review",
		InReview:       "In Review",
		Released:       "Released",
		Rejected:       "Rejected",
		Approved:       "Approved",
		Deprecated:     "Deprecated",
		PendingAction:  "Pending Action",
		Uploading:      "Upload In Progress",
		AssetsUploaded: "Assets Uploaded",
		Deleted:        "Deleted",
	}
)

// MarshalJSON custom marshaler for extension states so we can read them
func (state *ExtensionState) MarshalJSON() ([]byte, error) {
	if state == nil {
		return nil, errors.New("Extension state was nil")
	}
	humanName, ok := HumanNameByState[*state]
	if ok {
		return json.Marshal(humanName)
	}
	return nil, fmt.Errorf("Unknown extension state: %v", state)
}

// UmnarshalJSON custom marshaler for extension states so we can read them
func (state *ExtensionState) UnmarshalJSON(b []byte) error {
	if state == nil {
		return errors.New("Extension state was nil")
	}
	var s string
	if err := json.Unmarshal(b, &s); err == nil {
		for st, name := range HumanNameByState {
			if name == s {
				*state = st
				return nil
			}
		}
	}
	var n int
	if err := json.Unmarshal(b, &n); err == nil {
		*state = ExtensionState(n)
		return nil
	}
	return fmt.Errorf("Could not unmarshal state \"%v\"", string(b))
}

// Extension holds the information pertinent to an extension
// note fields "IconURL" and "ScreenshotURLs" allow the sender to
// specify a full URL to the icons and screenshots instead of having us upload
// them
type Extension struct {
	Anchor                       string
	AssetHash                    string
	AssetPaths                   []string // deprecated: in views now
	AssetsManuallyUploaded       bool
	AuthorEmail                  string
	AuthorName                   string
	BitsEnabled                  bool // deprecated: Use BitsFeatureLevel instead
	BitsSupportLevel             protocol.BitsSupportLevel
	BroadcasterWhitelist         []string
	Categories                   []string
	ConfigPath                   string // deprecated: verify with science before removing
	ConfigurationLocation        string
	Description                  string
	EULATOSURL                   string `dynamodbav:",omitempty"`
	FirstReleasedAt              *time.Time
	HasChatSupport               bool
	Games                        []int
	IconPath                     string // deprecated: verify clients are migrated to IconPaths before removing
	IconURL                      string // deprecated: verify clients are migrated to IconURLs before removing
	IconPaths                    map[string]string
	IconURLs                     map[string]string
	ID                           string
	LastReleasedAt               *time.Time
	LiveConfigPath               string // deprecated: verify with science before removing
	MobileViewerPath             string // deprecated: verify with science before removing
	Name                         string
	PanelHeight                  int    // deprecated: verify with science before removing
	PrivacyPolicyURL             string `dynamodbav:",omitempty"`
	ProductionBaseURI            string
	RequestIdentityLink          bool
	RequiredBroadcasterAbilities []string
	RequiredConfigurationString  string
	ScreenshotPaths              []string
	ScreenshotURLs               []string
	SKU                          string
	State                        ExtensionState
	SubscriptionsSupportLevel    protocol.SubscriptionsSupportLevel
	Summary                      string
	SupportEmail                 string
	TestingAccounts              []string
	TestingBaseURI               string
	VendorCode                   string
	Version                      string
	ViewerPath                   string // deprecated: verify with science before removing
	ViewerSummary                string
	Views                        ExtensionViews
	WhitelistedConfigURLs        []string
	WhitelistedPanelURLs         []string
}

// SupportedAnchorTypes returns a list of all anchor types supported by the Extension
func (e *Extension) SupportedAnchorTypes() []string {
	anchors := make([]string, 0)
	if e.Views.Component.ViewerPath != "" {
		anchors = append(anchors, "component")
	}

	if e.Views.Hidden.ViewerPath != "" {
		anchors = append(anchors, "hidden")
	}

	if e.Views.Panel.ViewerPath != "" {
		anchors = append(anchors, "panel")
	}

	if e.Views.VideoOverlay.ViewerPath != "" {
		anchors = append(anchors, "video_overlay")
	}

	return anchors
}

// ExtensionViews contains all of the view-specific information
type ExtensionViews struct {
	Component    ComponentExtensionView
	Config       ConfigExtensionView
	Hidden       HiddenExtensionView
	LiveConfig   LiveConfigExtensionView
	Mobile       MobileExtensionView
	Panel        PanelExtensionView
	VideoOverlay VideoOverlayExtensionView
}

type ConfigExtensionView struct {
	ViewerPath string
}

type ComponentExtensionView struct {
	AspectHeight int // deprecated in favor of ratioX/Y below
	AspectWidth  int // deprecated in favor of ratioX/Y below
	AspectRatioX int
	AspectRatioY int
	Autoscale    bool
	ScalePixels  int
	Size         float64
	TargetHeight int
	ViewerPath   string
	Zoom         bool
	ZoomPixels   int
}

type HiddenExtensionView struct {
	ViewerPath string
}

type LiveConfigExtensionView struct {
	ViewerPath string
}

type MobileExtensionView struct {
	ViewerPath string
}

type PanelExtensionView struct {
	ViewerPath string
	Height     int
}

type VideoOverlayExtensionView struct {
	ViewerPath string
}

// CanView implements ems/auth/Permissions
func (e *Extension) CanView(user string) bool {
	return e.State == Released || In(user, e.TestingAccounts)
}

// CanInstall implements ems/auth/Permissions
func (e *Extension) CanInstall(channel string) bool {
	return len(e.BroadcasterWhitelist) == 0 || In(channel, e.BroadcasterWhitelist) || In(channel, e.TestingAccounts)
}

// Channel holds simply the ID of a channel; we don't manage channels so we
// just need a unique identifier to consume and return.
type Channel struct {
	ID string
}

const (
	ActivationStateActive              = "active"
	ActivationStateInactive            = "inactive"
	ActivationStatePending             = "pending"
	ActivationStateRejectedPermissions = "rejected_permissions"

	// looking for actions? use `whitelist/protocol/action.go` and update the map
	// when you declare your new action.
)

// IsPossiblyActive is used by ECS to filter returned records.
func IsPossiblyActive(activationState string) bool {
	return activationState == ActivationStateActive || activationState == ActivationStatePending
}

// InstalledExtension is the many-to-many relationship between
// Channels and Extensions.  It also indicates whether an installed
// Extension is active on the channel or not.
//
// The CompoundKey field is a concatenation of the other three fields,
// used to make a triple-key for uniqueness in dynamo.
type InstalledExtension struct {
	CompoundKey                 string
	ExtensionID                 string
	ChannelID                   string
	Version                     string
	ActivationConfig            ActivationConfiguration
	ActivationState             string
	RequiredConfigurationString string
}

type InstallationsPage struct {
	Installs []*InstalledExtension
	NextPage *string
}

// ActivationConfiguration holds the details of the activation on a
// particular channel (e.g., what slot is it in)
type ActivationConfiguration struct {
	Slot   string
	Anchor string
}

// InstallationCount keeps track of how many users have installed an extension on
// their channel.
type InstallationCount struct {
	ExtensionID string
	Count       int
}

// WhitelistedUser represents details for a whitelisted user and the action
// that user can take
type WhitelistedUser struct {
	UserID     string
	ActionType string
}

// MakeInstalledExtensionCompoundKey builds the triple-compound key for
// the installed extension many-to-many table.
func MakeInstalledExtensionCompoundKey(channelID, extensionID, version string) string {
	return channelID + "@" + extensionID + "@" + version
}

// NewInstalledExtension builds an InstalledExtension structure,
// computing the triple compound key along the way.
func NewInstalledExtension(channelID, extensionID, version string) *InstalledExtension {
	return &InstalledExtension{
		ExtensionID:      extensionID,
		Version:          version,
		ChannelID:        channelID,
		CompoundKey:      MakeInstalledExtensionCompoundKey(channelID, extensionID, version),
		ActivationState:  ActivationStateInactive,
		ActivationConfig: ActivationConfiguration{Slot: "", Anchor: ""},
	}
}
