# Extensions
This directory is a collection of services (and one front-end library) which power the Extensions product on Twitch.

- **Dev-assets Proxy**: An NGINX proxy which routes requests loading extension assets to the proper S3 bucket, also handles applying the proper CSP headers to loaded assets.
- **ECS**: This service provides channel-related data about an extension ID (ie: which channels is an extension installed on, which channels is an extension active on).
- **Emailsender**: Handles Extension Developer onboarding events from Payments, adds users to create-extension whitelists and sends confirmation e-mails of sign up.
- **EMS**: Monolithic service whose purpose was originally to handle extension creation, extension lifecycle, and extension management (ie: install/uninstall/activate/deactivate). But also handles routes for Extension Discovery and the Whitelist service.
- **Supervisor**: Front-end library loads extension assets into an iframe with the proper CSP attributes and ensures secure communication between Twitch clients and extension iframes.
- **Validator**: Handles extension secret creation/rotation, opaque user ID mappings, user ID linking/unlinking, and JWT creation.
- **Whitelist**: Handles management of user ID or client ID whitelists for various authorizations for extensions. (ie: reviewers, whitelisted outbound linking video extensions, etc.)

## Maintenance:
- JIRA: Multiple. Ask in [#extensions-feedback][ref-slack]
 - Slack:
	- Questions/Feedback? [#extensions-feedback][ref-slack-feedback]
	- Emergency? [#extensions-oncall][ref-slack-oncall]
 - [Pagerduty][ref-pagerduty]
 - Grafana Dashboards:
	- [Overall Health][ref-grafana-allroutes]
	- Dev-assets Proxy Health ???
	- [ECS Health][ref-grafana-ecs]
	- Emailsender Health ???
	- [EMS Health][ref-grafana-ems]
	- Supervisor Health ???
	- [Validator Health][ref-grafana-validator]
	- [Whitelist Health][ref-grafana-ems] (`has_user` && `has_client`)
	- [Visage Downstream Availability][ref-grafana-visage]
 - Cloudwatch Dashboards:
	- [EMS Lambdas][ref-cloudwatch-ems-lambdas]
 - [Runbook][ref-runbook-extensions]
	- [Dev-assets-specific][ref-runbook-dev-assets]
	- [ECS-specific][ref-runbook-ecs]
	- [EMS-specific][ref-runbook-ems]
	- [Validator-specific][ref-runbook-validator]

## Hosts
| Service          | Environment | URL                                                                   |
|:----------------:|:-----------:|-----------------------------------------------------------------------|
| Dev-assets Proxy | Production  | `https://*.ext-twitch.tv`                                             |
| Dev-assets Proxy | Staging     | `https://*.ext-assets.internal.justin.tv`                             |
| ECS              | Production  | `https://extensions-context.prod.us-west2.justin.tv`                  |
| ECS              | Staging     | `https://extensions-ecs-staging.internal.justin.tv`                   |
| EMS              | Production  | `https://extensions-manager.prod.us-west2.justin.tv`                  |
| EMS              | Staging     | `https://extensions-ems-staging.internal.justin.tv`                   |
| Supervisor       | Production  | `https://extension-files.twitch.tv/supervisor/v1/index.html`          |
| Supervisor       | Staging     | `https://extension-files.internal.justin.tv/supervisor/v1/index.html` |
| Validator        | Production  | `https://extensions-validator.prod.us-west2.justin.tv`                |
| Validator        | Staging     | `https://extensions-validator-staging.internal.justin.tv`             |
| Whitelist        | Production  | `https://extensions-manager.prod.us-west2.justin.tv`                  |
| Whitelist        | Staging     | `https://extensions-ems-staging.internal.justin.tv`                   |

## Dependencies

### Downstream Internal Services
| Service    | Usage                                                                                               |
|------------|-----------------------------------------------------------------------------------------------------|
| JAX        | Seeds the kinesis stream used to power ECS, powers jitter amount for live activation pubsub message |
| Payments   | Triggers emailsender to send e-mails to onboarded developers                                        |
| Pubsub     | Publish `extension-control` events to pubsub                                                        |
| Pushy      | Sends emails based on lifecycle events                                                              |
| Spade      | Tracking events                                                                                     |

### Datastores
| Service | Purpose |
|-----------------|--------------------------------------------------------------------------|
| DynamoDB        | Long-term storage of extensions data                                     |
| ElasticSearch   | Indexed/searchable cache of extension records                            |
| Postgres        | Extension discovery data (categories and featured carousel)              |

[ref-cloudwatch-ems-lambdas]: https://us-west-2.console.aws.amazon.com/lambda/home?region=us-west-2#/functions?f0=a3c%3D%3AZXh0ZW5zaW9ucw%3D%3D
[ref-grafana-allroutes]: https://grafana.internal.justin.tv/d/000001235/dev-success-extension-routes?refresh=30s&orgId=1&from=now-3h&to=now-1m
[ref-grafana-ecs]: https://grafana.internal.justin.tv/d/000001235/dev-success-extension-routes?refresh=30s&orgId=1&from=now-3h&to=now-1m&var-service=ecs&var-environment=production&var-route=All&var-client=All
[ref-grafana-ems]: https://grafana.internal.justin.tv/d/000001235/dev-success-extension-routes?refresh=30s&orgId=1&from=now-3h&to=now-1m&var-service=ems&var-environment=production&var-route=All&var-client=All
[ref-grafana-validator]: https://grafana.internal.justin.tv/d/000001235/dev-success-extension-routes?refresh=30s&orgId=1&from=now-3h&to=now-1m&var-service=validator&var-environment=production&var-route=All&var-client=All
[ref-grafana-visage]: https://grafana.internal.justin.tv/d/000000808/visage-downstream-availability-wall-dash?refresh=1m&orgId=1
[ref-pagerduty]: https://twitchoncall.pagerduty.com/schedules#POJGJ43
[ref-runbook-dev-assets]: https://git-aws.internal.justin.tv/gds/extensions-oncall/tree/master/docs/dev-assets-proxy
[ref-runbook-ecs]: https://git-aws.internal.justin.tv/gds/extensions-oncall/tree/master/docs/ecs
[ref-runbook-ems]: https://git-aws.internal.justin.tv/gds/extensions-oncall/tree/master/docs/ems
[ref-runbook-extensions]: https://git-aws.internal.justin.tv/gds/extensions-oncall
[ref-runbook-validator]: https://git-aws.internal.justin.tv/gds/extensions-oncall/tree/master/docs/validator
[ref-slack-feedback]: https://twitch.slack.com/messages/C2PN4AQ79
[ref-slack-oncall]: https://twitch.slack.com/messages/CBQKSQQ5B
