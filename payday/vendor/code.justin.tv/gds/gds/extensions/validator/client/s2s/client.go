package s2s

import (
	"bytes"
	"context"
	"encoding/json"
	"io"
	"net/http"

	"code.justin.tv/foundation/twitchclient"
	"code.justin.tv/gds/gds/extensions/validator/model"
	"code.justin.tv/sse/malachai/pkg/s2s/caller"
	"github.com/sirupsen/logrus"
)

const (
	defaultStatSampleRate = 0.1
)

// Client expressing actions that can be taken by the validator system using s2s auth
type Client interface {
	CreateBitsTransactionToken(ctx context.Context, clientId, userId, transactionId, time string, product model.Product, reqOpts *twitchclient.ReqOpts) (*model.PermissionData, error)
}

type ClientImpl struct {
	http twitchclient.Client
}

func NewClient(name string, clientConfig twitchclient.ClientConf, callerConfig caller.Config) (Client, error) {
	rt, err := caller.NewRoundTripper(name, &callerConfig, nil)
	if err != nil {
		logrus.Fatal(err)
		return nil, err
	}

	clientConfig.RoundTripperWrappers = append(clientConfig.RoundTripperWrappers,
		func(inner http.RoundTripper) http.RoundTripper {
			rt.SetInnerRoundTripper(inner)
			return rt
		},
	)

	client, err := twitchclient.NewClient(clientConfig)
	if err != nil {
		return nil, err
	}

	return &ClientImpl{
		http: client,
	}, nil
}

func (sci *ClientImpl) CreateBitsTransactionToken(ctx context.Context, clientId, userId, transactionId, time string, product model.Product, reqOpts *twitchclient.ReqOpts) (*model.PermissionData, error) {
	bttBody := sci.createBTTBody(transactionId, time, product)
	req, err := sci.http.NewRequest("POST", "/client/"+clientId+"/token/"+userId+"/transaction", bttBody)
	if err != nil {
		return nil, err
	}
	var out model.PermissionData
	err = sci.execute(ctx, req, "post_bits_transaction_token", &out, reqOpts)
	return &out, err
}

func (sci *ClientImpl) createBTTBody(transactionId, time string, product model.Product) io.Reader {
	out := &bytes.Buffer{}
	// Swallowing the error here since this simple a value to
	// encode won't run the risk of returning
	// json.UnsupportedValueError
	_ = json.NewEncoder(out).Encode(map[string]interface{}{"transactionId": transactionId, "time": time, "product": product})
	return out
}

func (sci *ClientImpl) execute(ctx context.Context, req *http.Request, statName string, out interface{}, reqOpts *twitchclient.ReqOpts) error {
	opts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.extensions." + statName,
		StatSampleRate: defaultStatSampleRate,
	})

	_, err := sci.http.DoJSON(ctx, out, req, opts)
	return err
}
