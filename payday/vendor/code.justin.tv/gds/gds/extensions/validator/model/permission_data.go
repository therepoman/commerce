package model

import (
	"encoding/json"
)

// PermissionData is a public facing struture that contains
// JWT token and grants
type PermissionData struct {
	Token  string          `json:"token"`
	Grants map[string]bool `json:"grants"`
}

// NewPermissionData returns a PermissionData
func NewPermissionData(token string, grants map[string]bool) (*PermissionData, error) {
	return &PermissionData{token, grants}, nil
}

// ToJSON serializes the permission data as json
func (e *PermissionData) ToJSON() []byte {
	// since e is a collection of primitives and primitive pointers, this Marshal will
	// only fail in situations like OOM that should panic rather than return.
	out, _ := json.Marshal(e)
	return out
}
