package model

// VerifiedExtensionGrants is a public facing struture that returns a filtered list
// of grants in response to a VerifyUserGrants request. It is designed for use
// by Cartman so that we can track granular permissions for users.
type VerifiedExtensionGrants struct {
	UserID         string   `json:"user_id"`
	ClientID       string   `json:"client_id"`
	FilteredGrants []string `json:"filtered_grants,omitempty"`
	Role           string   `json:"role"`
}
