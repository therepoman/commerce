package model

import (
	"encoding/json"
)

// PermissionDataCollection is a public facing struture that contains
// a map of extension id to PermissionData.
type PermissionDataCollection struct {
	PermissionData map[string]*PermissionData `json:"clients"`
}

// NewPermissionDataCollection returns a PermissionDataCollection
func NewPermissionDataCollection(data map[string]*PermissionData) (*PermissionDataCollection, error) {
	return &PermissionDataCollection{data}, nil
}

// ToJSON serializes the permission data collection as json
func (e *PermissionDataCollection) ToJSON() []byte {
	// since e is a collection of primitives and primitive pointers, this Marshal will
	// only fail in situations like OOM that should panic rather than return.
	out, _ := json.Marshal(e)
	return out
}
