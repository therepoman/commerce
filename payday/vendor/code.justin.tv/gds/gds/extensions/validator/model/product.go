package model

type Cost struct {
	Amount int64  `json:"amount"`
	Type   string `json:"type"`
}

type Product struct {
	DomainId    string `json:"domainId"`
	SKU         string `json:"sku"`
	DisplayName string `json:"displayName"`
	Cost        Cost   `json:"cost"`
}
