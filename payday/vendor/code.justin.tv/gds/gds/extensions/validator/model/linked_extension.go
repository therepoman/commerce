package model

// LinkedExtensions is a public facing struture that returns a list of
// linked extension
type LinkedExtensions struct {
	ExtensionIDs []string `json:"extension_ids"`
}
