package model

import (
	"encoding/json"
	"time"
)

const currentCollectionVersion = 1

// SecretCollection is a public facing struture that allows extension developers
// to retrieve the JWT signing secrets for an extension.
type SecretCollection struct {
	// Used to notify the public if there are backward incompatible changes to this type
	Version  int            `json:"format_version"`
	Secrets  []Secret       `json:"secrets"`
	Duration *time.Duration `json:"token_duration,omitempty"`
}

// TODO : make this return (*SecretCollection, error) to give API room for future validation

// NewSecretCollection returns a SecretCollection
func NewSecretCollection(secrets []Secret, tokenDuration *time.Duration) *SecretCollection {
	return &SecretCollection{currentCollectionVersion, secrets, tokenDuration}
}

// TokenDuration allows a SecretCollection to override the passed in default token TTL
// on a per-collection basis.
func (s *SecretCollection) TokenDuration(defaultDuration time.Duration) time.Duration {
	if s.Duration != nil {
		return *s.Duration
	}
	return defaultDuration
}

// ToJSON serializes the collection as json
func (s *SecretCollection) ToJSON() []byte {
	// since S is a collection of primitives and primitive pointers, this Marshal will
	// only fail in situations like OOM that should panic rather than return.
	out, _ := json.Marshal(s)
	return out
}
