package model

import "time"

// Secret captures a single secret for encoding/decoding JWTs along with the
// time period during which the secret is valid
type Secret struct {
	// Content is the raw secret that should be used with the JWT encoding
	// algorithm
	Content []byte `json:"content"`

	// Active defines the earliest possible time it is valid to sign a JWT; the
	// secret should be considered valid for decoding before this time
	Active time.Time `json:"active"`

	// Expires defines when this secret should no longer be used for encoding or
	// decoding JWTs
	Expires time.Time `json:"expires"`
}

// TODO : make this return (*Secret, err), add validation for time range

// NewSecret generates a new Secret from the supplied data.
func NewSecret(content []byte, active time.Time, expires time.Time) Secret {
	return Secret{content, active, expires}
}
