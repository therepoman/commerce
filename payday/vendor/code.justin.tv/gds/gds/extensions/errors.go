package extensions

import (
	"fmt"
	"net/http"

	"code.justin.tv/gds/gds/golibs/errors"
)

const (
	// ErrCodeUnauthorized is the error code for ErrUnauthorized
	ErrCodeUnauthorized = "unauthorized"

	// ErrCodeInvalidVersion is the error code for ErrInvalidVersion
	ErrCodeInvalidVersion = "invalid_version"

	// ErrCodeAnchorRequired is the error code for ErrAnchorRequired
	ErrCodeAnchorRequired = "anchor_required"

	// ErrCodeInvalidZoom is the error code for ErrInvalidZoom
	ErrCodeInvalidZoom = "invalid_zoom"

	// ErrCodeInvalidScalePixels is the error code for ErrInvalidScalePixels
	ErrCodeInvalidScalePixels = "invalid_scale_pixels"

	// ErrCodeTooManyBroadcasters is the error code for ErrTooManyBroadcasters
	ErrCodeTooManyBroadcasters = "max_broadcasters"

	// ErrCodeTooManyTesters is the error code for ErrTooManyTesters
	ErrCodeTooManyTesters = "max_testers"

	// ErrCodeTooManyWhitelistedConfigURLs is the error code for ErrTooManyWhitelistedConfigURLs
	ErrCodeTooManyWhitelistedConfigURLs = "max_whitelisted_config_urls"

	// ErrCodeTooManyWhitelistedPanelURLs is the error code for ErrTooManyWhitelistedPanelURLs
	ErrCodeTooManyWhitelistedPanelURLs = "max_whitelisted_panel_urls"

	// ErrCodeNameLength is the error code for ErrNameLength
	ErrCodeNameLength = "invalid_name_length"

	// ErrCodeDescriptionLength is the error code for ErrDescriptionLength
	ErrCodeDescriptionLength = "invalid_description_length"

	// ErrCodeSummaryLength is the error code for ErrSummaryLength
	ErrCodeSummaryLength = "invalid_summary_length"

	// ErrCodeViewerSummaryLength is the error code for ErrViewerSummaryLength
	ErrCodeViewerSummaryLength = "invalid_viewer_summary_length"

	// ErrCodeNameAlreadyExists is the error code for ErrNameAlreadyExists
	ErrCodeNameAlreadyExists = "name_already_exists"

	// ErrCodeMissingIconUrl is the error code for ErrMissingIconUrl
	ErrCodeMissingIconUrl = "missing_icon_urls"

	// ErrCodeInvalidStateTransition is the error code for ErrInvalidStateTransition
	ErrCodeInvalidStateTransition = "invalid_state_transition"

	// ErrCodeUnknownState is the error code for ErrUnknownState
	ErrCodeUnknownState = "unknown_state"

	// ErrCodeForbiddenStateTransition is the error code for ErrForbiddenStateTransition
	ErrCodeForbiddenStateTransition = "forbidden_state_transition"

	// ErrCodeWrongAnchorType is the error code for ErrWrongAnchorType
	ErrCodeWrongAnchorType = "wrong_anchor_type"

	// ErrCodeMultipleExtensionsInReview is the error code for ErrMultipleExtensionsInReview
	ErrCodeMultipleExtensionsInReview = "multiple_extensions_in_review"

	// ErrCodeBitsEnabledChange is the error code for ErrBitsEnabledChange
	ErrCodeBitsEnabledChange = "bits_enabled_change"

	// ErrCodeMissingAssetHash is the error code for ErrMissingAssetHash
	ErrCodeMissingAssetHash = "missing_asset_hash"

	// ErrCodeMissingScreenshotURLs is the error code for ErrMissingScreenshotURLs
	ErrCodeMissingScreenshotURLs = "missing_screenshot_urls"

	// ErrCodeMaxGames is the error code for ErrMaxGames
	ErrCodeMaxGames = "max_games"

	// ErrCodeGameNotFound is the error code for ErrGameNotFound
	ErrCodeGameNotFound = "game_not_found"

	// ErrCodeInvalidGameId is the error code for ErrInvalidGameId
	ErrCodeInvalidGameId = "invalid_game_id"

	// ErrCodeInvalidAuthorEmail is the error code for ErrInvalidAuthorEmail
	ErrCodeInvalidAuthorEmail = "invalid_author_email"

	// ErrCodeInvalidSupportEmail is the error code for ErrInvalidSupportEmail
	ErrCodeInvalidSupportEmail = "invalid_support_email"

	// ErrCodeAuthorNameLength is the error code for ErrAuthorNameLength
	ErrCodeAuthorNameLength = "invalid_author_name_length"

	// ErrCodeInvalidTestingBaseURL is the error code for ErrInvalidTestingBaseURL
	ErrCodeInvalidTestingBaseURL = "invalid_testing_base_uri"

	// ErrCodeInvalidTermsURL is the error code for ErrInvalidTermsURL
	ErrCodeInvalidTermsURL = "invalid_terms_uri"

	// ErrCodeInvalidPrivacyURL is the error code for ErrInvalidPrivacyURL
	ErrCodeInvalidPrivacyURL = "invalid_privacy_uri"

	// ErrCodeInvalidComponentViewerPath is the error code for ErrInvalidComponentViewerPath
	ErrCodeInvalidComponentViewerPath = "invalid_component_viewer_path"

	// ErrCodeInvalidPanelViewerPath is the error code for ErrInvalidPanelViewerPath
	ErrCodeInvalidPanelViewerPath = "invalid_panel_viewer_path"

	// ErrCodeInvalidVideoOverlayViewerPath is the error code for ErrInvalidVideoOverlayViewerPath
	ErrCodeInvalidVideoOverlayViewerPath = "invalid_video_overlay_viewer_path"

	// ErrCodeInvalidConfigViewerPath is the error code for ErrInvalidConfigViewerPath
	ErrCodeInvalidConfigViewerPath = "invalid_config_viewer_path"

	// ErrCodeInvalidLiveConfigViewerPath is the error code for ErrInvalidLiveConfigViewerPath
	ErrCodeInvalidLiveConfigViewerPath = "invalid_live_config_viewer_path"

	// ErrCodeInvalidMobileViewerPath is the error code for ErrInvalidMobileViewerPath
	ErrCodeInvalidMobileViewerPath = "invalid_mobile_viewer_path"

	// ErrCodeInvalidComponentAspectWidth is the error code for ErrInvalidComponentAspectWidth
	ErrCodeInvalidComponentAspectWidth = "invalid_component_aspect_width"

	// ErrCodeInvalidComponentAspectHeight is the error code for ErrInvalidComponentAspectHeight
	ErrCodeInvalidComponentAspectHeight = "invalid_component_aspect_height"

	// ErrCodeInvalidComponentAspectRatioX is the error code for ErrInvalidComponentAspectRatioX
	ErrCodeInvalidComponentAspectRatioX = "invalid_component_aspect_ratio_x"

	// ErrCodeInvalidComponentAspectRatioY is the error code for ErrInvalidComponentAspectRatioY
	ErrCodeInvalidComponentAspectRatioY = "invalid_component_aspect_ratio_y"

	// ErrCodeInvalidComponentTargetHeight is the error code for ErrInvalidComponentTargetHeight
	ErrCodeInvalidComponentTargetHeight = "invalid_component_target_height"

	// ErrCodeInvalidPanelHeight is the error code for ErrInvalidPanelHeight
	ErrCodeInvalidPanelHeight = "invalid_panel_height"
)

var (
	// ErrInvalidStateTransition is returned when a client tries to change the state of
	// an extension in a way that is invalid
	ErrInvalidStateTransition = errors.NewBuilder("Invalid state transition").WithErrorCode(ErrCodeInvalidStateTransition).WithHTTPStatus(http.StatusBadRequest).Build()

	// ErrUnknownState is returned when a client tries to change the state of
	// an extension to a state we don't recognize
	ErrUnknownState = errors.NewBuilder("Unknown state").WithErrorCode(ErrCodeUnknownState).WithHTTPStatus(http.StatusUnprocessableEntity).Build()

	// ErrForbiddenStateTransition is returned when a client tries to change the state of
	// an extension in a way that is not permitted (e.g., the developer tries to
	// approve their own extension)
	ErrForbiddenStateTransition = errors.NewBuilder("You are not allowed to make that state transition").WithErrorCode(ErrCodeForbiddenStateTransition).WithHTTPStatus(http.StatusForbidden).Build()

	// ErrUnauthorized is returned when an API action is not permitted by the requester's capabilities
	ErrUnauthorized = errors.NewBuilder("Sorry, you're not allowed to do that").WithErrorCode(ErrCodeUnauthorized).WithHTTPStatus(http.StatusUnauthorized).Build()

	// ErrWrongAnchorType is returned when the user attempts to activate an extension using the wrong anchor type
	// TODO : parameterize
	ErrWrongAnchorType = errors.NewBuilder("The extension can't be activated in that location").WithErrorCode(ErrCodeWrongAnchorType).WithHTTPStatus(http.StatusUnprocessableEntity).Build()

	// ErrMultipleExtensionsInReview is returned when the developer tries to have more than one extension in review at once.
	// TODO : report http.StatusConflict as a possible return in visage, update status code here
	ErrMultipleExtensionsInReview = errors.NewBuilder("There can be only one version of an extension under review at a time").WithErrorCode(ErrCodeMultipleExtensionsInReview).WithHTTPStatus(http.StatusBadRequest).Build()

	// ErrBitsEnabledChange is returned when a new extension manifest's bits enabled flag doesn't match the bits enabled
	// flag of a previous released version during state transition
	ErrBitsEnabledChange = errors.NewBuilder("The 'Bits Enabled' field cannot change once the extension has been released").WithErrorCode(ErrCodeBitsEnabledChange).WithHTTPStatus(http.StatusUnprocessableEntity).Build()

	// ErrTooManyTesters is returned when too many testing accounts appear in the extension manifest
	ErrTooManyTesters = errors.NewBuilder("Too many test accounts").WithErrorCode(ErrCodeTooManyTesters).WithHTTPStatus(http.StatusUnprocessableEntity).Build()

	// ErrTooManyBroadcasters is returned when too many broadcaster accounts appear in the extension manifest
	ErrTooManyBroadcasters = errors.NewBuilder("Too many broadcaster accounts").WithErrorCode(ErrCodeTooManyBroadcasters).WithHTTPStatus(http.StatusUnprocessableEntity).Build()

	// ErrTooManyWhitelistedPanelURLs is returned when too many panel URLs appear in the extension manifest
	ErrTooManyWhitelistedPanelURLs = errors.NewBuilder("Too many whitelisted panel URLs").WithErrorCode(ErrCodeTooManyWhitelistedPanelURLs).WithHTTPStatus(http.StatusUnprocessableEntity).Build()

	// ErrTooManyWhitelistedConfigURLs is returned when too many config URLs appear in the extension manifest
	ErrTooManyWhitelistedConfigURLs = errors.NewBuilder("Too many whitelisted config URLs").WithErrorCode(ErrCodeTooManyWhitelistedConfigURLs).WithHTTPStatus(http.StatusUnprocessableEntity).Build()

	// ErrInvalidVersion is returned when the provided version doesn't match the required regexp
	ErrInvalidVersion = errors.NewBuilder("Invalid extension version format").WithErrorCode(ErrCodeInvalidVersion).WithHTTPStatus(http.StatusUnprocessableEntity).Build()

	// ErrNameLength is returned when an extension's name exceeds the maximum allowed length
	ErrNameLength = errors.NewBuilder(fmt.Sprintf("Extension names are limited to %d characters", MaxNameLength)).WithErrorCode(ErrCodeNameLength).WithHTTPStatus(http.StatusUnprocessableEntity).Build()

	// ErrDescriptionLength is returned when an extension's description exceeds the maximum allowed length
	ErrDescriptionLength = errors.NewBuilder(fmt.Sprintf("Extension descriptions are limited to %d characters", MaxDescriptionLength)).WithErrorCode(ErrCodeDescriptionLength).WithHTTPStatus(http.StatusUnprocessableEntity).Build()

	// ErrSummaryLength is returned when an extension's summary exceeds the maximum allowed length
	ErrSummaryLength = errors.NewBuilder(fmt.Sprintf("Extension summaries are limited to %d characters", MaxSummaryLength)).WithErrorCode(ErrCodeSummaryLength).WithHTTPStatus(http.StatusUnprocessableEntity).Build()

	// ErrViewerSummaryLength is returned when an extension's viewer summary exceeds the maximum allowed length
	ErrViewerSummaryLength = errors.NewBuilder(fmt.Sprintf("Extension viewer summaries are limited to %d characters", MaxViewerSummaryLength)).WithErrorCode(ErrCodeViewerSummaryLength).WithHTTPStatus(http.StatusUnprocessableEntity).Build()

	// ErrNameAlreadyExists is returned when we try to add / update an extension to an existing name (but different ID)
	ErrNameAlreadyExists = errors.NewBuilder("An extension with that name already exists").WithErrorCode(ErrCodeNameAlreadyExists).WithHTTPStatus(http.StatusUnprocessableEntity).Build()

	// ErrMalformedSearch is returned when the user gives us a search string that we can't parse
	ErrMalformedSearch = errors.NewBuilder("Malformed search query").WithHTTPStatus(http.StatusUnprocessableEntity).Build()

	// ErrMalformedSort is returned when the user gives us a sort string that we can't parse
	ErrMalformedSort = errors.NewBuilder("Malformed sort").WithHTTPStatus(http.StatusUnprocessableEntity).Build()

	// ErrMissingAssetHash is returned when we try transition to AssetsUploaded but haven't specified the hash yet.
	ErrMissingAssetHash = errors.NewBuilder("You can't transition to assets_uploaded without specifying the asset hash first").WithErrorCode(ErrCodeMissingAssetHash).WithHTTPStatus(http.StatusBadRequest).Build()

	// ErrMissingIconURL is returned when we try transition to AssetsUploaded but haven't specified the icon URL.
	ErrMissingIconURL = errors.NewBuilder("You can't transition to assets_uploaded without specifying the full icon URL first").WithErrorCode(ErrCodeMissingIconUrl).WithHTTPStatus(http.StatusUnprocessableEntity).Build()

	// ErrMissingScreenshotURLs is returned when we try transition to AssetsUploaded but haven't specified screenshot URLs yet.
	ErrMissingScreenshotURLs = errors.NewBuilder("You can't transition to assets_uploaded without specifying the full screenshot URLs first").WithErrorCode(ErrCodeMissingScreenshotURLs).WithHTTPStatus(http.StatusUnprocessableEntity).Build()

	// ErrRequireAnchor is returned when the user attempts to create an extension version without specificying at least one anchor viewerPath
	ErrAnchorRequired = errors.NewBuilder("You must supply a viewerPath for at least one anchor").WithErrorCode(ErrCodeAnchorRequired).WithHTTPStatus(http.StatusUnprocessableEntity).Build()

	// ErrInvalidAnchor is returned when the user attempts to activate an extension and provides an invalid anchor type
	ErrInvalidAnchor = errors.NewBuilder("You must supply a valid anchor type").WithHTTPStatus(http.StatusUnprocessableEntity).Build()

	// ErrInvalidZoom is returned when an extension is configured with a component view that has a ZoomPixels value <= 0
	ErrInvalidZoom = errors.NewBuilder("Component extension ZoomPixels cannot be <= 0").WithErrorCode(ErrCodeInvalidZoom).WithHTTPStatus(http.StatusUnprocessableEntity).Build()

	// ErrInvalidScalePixels is returned when an extension is configured with a component view that has a ScalePixels value <= 0
	ErrInvalidScalePixels = errors.NewBuilder("Component extension ScalePixels cannot be <= 0").WithErrorCode(ErrCodeInvalidScalePixels).WithHTTPStatus(http.StatusUnprocessableEntity).Build()

	// ErrMaxGames is returned when an extension's games exceeds the maximum allowed count
	ErrMaxGames = errors.NewBuilder(fmt.Sprintf("Extension are limited to %d games", MaxGames)).WithErrorCode(ErrCodeMaxGames).WithHTTPStatus(http.StatusUnprocessableEntity).Build()

	// ErrGameNotFound is returned when a game is not in an extension
	// TODO : make call idempotent, remove this error
	ErrGameNotFound = errors.NewBuilder("Can't find this game on the Extension").WithErrorCode(ErrCodeGameNotFound).WithHTTPStatus(http.StatusUnprocessableEntity).Build()

	// ErrInvalidGameId is returned when a game id is invalid (not a string of pure digits or the value of the string < 0)
	ErrInvalidGameId = errors.NewBuilder("Input of game id is invalid.").WithErrorCode(ErrCodeInvalidGameId).WithHTTPStatus(http.StatusUnprocessableEntity).Build()

	// ErrInvalidAuthorEmail is returned when author e-mail associated with an extension in invalid
	ErrInvalidAuthorEmail = errors.NewBuilder("Email address is not valid").WithErrorCode(ErrCodeInvalidAuthorEmail).WithHTTPStatus(http.StatusUnprocessableEntity).Build()

	// ErrInvalidSupportEmail is returned when support e-mail associated with an extension in invalid
	ErrInvalidSupportEmail = errors.NewBuilder("Email address is not valid").WithErrorCode(ErrCodeInvalidSupportEmail).WithHTTPStatus(http.StatusUnprocessableEntity).Build()

	// ErrAuthorNameLength is returned when the author name is longer than the limit
	ErrAuthorNameLength = errors.NewBuilder(fmt.Sprintf("Author name must be fewer than %d characters", MaxAuthorNameLength)).WithErrorCode(ErrCodeAuthorNameLength).WithHTTPStatus(http.StatusUnprocessableEntity).Build()

	// ErrInvalidTestingBaseURL is returned when the testing base URL specified is not a valid URL
	ErrInvalidTestingBaseURL = errors.NewBuilder("Invalid URL specified for testing base").WithErrorCode(ErrCodeInvalidTestingBaseURL).WithHTTPStatus(http.StatusUnprocessableEntity).Build()

	// ErrInvalidTermsURL is returned when the terms of service URL specified is not a valid URL
	ErrInvalidTermsURL = errors.NewBuilder("Invalid URL specified for terms of service").WithErrorCode(ErrCodeInvalidTermsURL).WithHTTPStatus(http.StatusUnprocessableEntity).Build()

	// ErrInvalidPrivacyURL is returned when the privacy policy URL specified is not a valid URL
	ErrInvalidPrivacyURL = errors.NewBuilder("Invalid URL specified for privacy policy").WithErrorCode(ErrCodeInvalidPrivacyURL).WithHTTPStatus(http.StatusUnprocessableEntity).Build()

	// ErrInvalidComponentViewerPath is returned when the viewer path for a component view is invalid
	ErrInvalidComponentViewerPath = errors.NewBuilder("Invalid viewer path specified for Component view").WithErrorCode(ErrCodeInvalidComponentViewerPath).WithHTTPStatus(http.StatusUnprocessableEntity).Build()

	// ErrInvalidPanelViewerPath is returned when the viewer path for a panel view is invalid
	ErrInvalidPanelViewerPath = errors.NewBuilder("Invalid viewer path specified for Panel view").WithErrorCode(ErrCodeInvalidPanelViewerPath).WithHTTPStatus(http.StatusUnprocessableEntity).Build()

	// ErrInvalidVideoOverlayViewerPath is returned when the viewer path for a video overlay view is invalid
	ErrInvalidVideoOverlayViewerPath = errors.NewBuilder("Invalid viewer path specified for VideoOverlay view").WithErrorCode(ErrCodeInvalidVideoOverlayViewerPath).WithHTTPStatus(http.StatusUnprocessableEntity).Build()

	// ErrInvalidConfigViewerPath is returned when the viewer path for a config view is invalid
	ErrInvalidConfigViewerPath = errors.NewBuilder("Invalid viewer path specified for Config view").WithErrorCode(ErrCodeInvalidConfigViewerPath).WithHTTPStatus(http.StatusUnprocessableEntity).Build()

	// ErrInvalidLiveConfigViewerPath is returned when the viewer path for a live config view is invalid
	ErrInvalidLiveConfigViewerPath = errors.NewBuilder("Invalid viewer path specified for Live Config view").WithErrorCode(ErrCodeInvalidLiveConfigViewerPath).WithHTTPStatus(http.StatusUnprocessableEntity).Build()

	// ErrInvalidMobileViewerPath is returned when the viewer path for a mobile view is invalid
	ErrInvalidMobileViewerPath = errors.NewBuilder("Invalid viewer path specified for Mobile view").WithErrorCode(ErrCodeInvalidMobileViewerPath).WithHTTPStatus(http.StatusUnprocessableEntity).Build()

	// ErrInvalidComponentAspectWidth is returned when component aspect width is outside of the expected range.
	ErrInvalidComponentAspectWidth = errors.NewBuilder(fmt.Sprintf("Component aspect width must be between %d and %d", MinComponentAspectWidth/100, MaxComponentAspectWidth/100)).WithErrorCode(ErrCodeInvalidComponentAspectWidth).WithHTTPStatus(http.StatusUnprocessableEntity).Build()

	// ErrInvalidComponentAspectHeight is returned when component aspect height is outside of the expected range.
	ErrInvalidComponentAspectHeight = errors.NewBuilder(fmt.Sprintf("Component aspect height must be between %d and %d", MinComponentAspectHeight/100, MaxComponentAspectHeight/100)).WithErrorCode(ErrCodeInvalidComponentAspectHeight).WithHTTPStatus(http.StatusUnprocessableEntity).Build()

	// ErrInvalidComponentAspectRatioX is returned when component aspect ratio x is negative.
	ErrInvalidComponentAspectRatioX = errors.NewBuilder("Aspect ratio X must be positive").WithErrorCode(ErrCodeInvalidComponentAspectRatioX).WithHTTPStatus(http.StatusUnprocessableEntity).Build()

	// ErrInvalidComponentAspectRatioY is returned when component aspect ratio y is negative.
	ErrInvalidComponentAspectRatioY = errors.NewBuilder("Aspect ratio Y must be positive").WithErrorCode(ErrCodeInvalidComponentAspectRatioY).WithHTTPStatus(http.StatusUnprocessableEntity).Build()

	// ErrInvalidComponentTargetHeight is returned when component target height is outside of the expected range.
	ErrInvalidComponentTargetHeight = errors.NewBuilder(fmt.Sprintf("Component target height must be between %d and %d", MinComponentTargetHeight/100, MaxComponentTargetHeight/100)).WithErrorCode(ErrCodeInvalidComponentTargetHeight).WithHTTPStatus(http.StatusUnprocessableEntity).Build()

	// ErrInvalidPanelHeight is returned when panel height is outside of the expected range.
	ErrInvalidPanelHeight = errors.NewBuilder(fmt.Sprintf("Panel height must be between %d and %d pixels", MinPanelHeight, MaxPanelHeight)).WithErrorCode(ErrCodeInvalidPanelHeight).WithHTTPStatus(http.StatusUnprocessableEntity).Build()
)

// MaxSlotsExceededError is returned when a component exceeds its slot count
type MaxSlotsExceededError struct {
	name     string
	maxCount int
}

// NewMaxSlotsExceededError returns a MaxSlotsExceededError
func NewMaxSlotsExceededError(name string, maxCount int) error {
	return &MaxSlotsExceededError{name, maxCount}
}

// HTTPStatus implements the HTTPStatusError interface
func (m *MaxSlotsExceededError) HTTPStatus() int {
	return http.StatusConflict
}

// Error implements the Error interface
func (m *MaxSlotsExceededError) Error() string {
	return fmt.Sprintf("You can only have %d %s extensions active on a channel", m.maxCount, m.name)
}

var _ errors.HTTPStatusError = (*MaxSlotsExceededError)(nil)
