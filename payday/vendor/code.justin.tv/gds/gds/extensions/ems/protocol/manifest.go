package protocol

import (
	"time"
)

type IconSize = string

const (
	Square24Icon        IconSize = "24x24"
	Square100Icon       IconSize = "100x100"
	DiscoverySplashIcon IconSize = "300x200"
)

var IconTypes = []IconSize{
	Square24Icon,
	Square100Icon,
	DiscoverySplashIcon,
}

type ConfigurationLocation string

const (
	None   ConfigurationLocation = "none"
	Hosted ConfigurationLocation = "hosted"
	Custom ConfigurationLocation = "custom"
)

type ExtensionManifestResponse struct {
	Manifests   map[string]*ExtensionManifest
	Deleted     bool
	PagingToken *string
}

type SubscriptionsSupportLevel string

const (
	SubscriptionsSupportLevelNone     SubscriptionsSupportLevel = "none"
	SubscriptionsSupportLevelOptional SubscriptionsSupportLevel = "optional"
)

type BitsSupportLevel string

const (
	BitsSupportLevelNone     BitsSupportLevel = "none"
	BitsSupportLevelOptional BitsSupportLevel = "optional"
	BitsSupportLevelRequired BitsSupportLevel = "required"
)

func (b BitsSupportLevel) IsBitsMonetized() bool {
	if b == "" {
		return false
	}
	return (b == BitsSupportLevelOptional || b == BitsSupportLevelRequired)
}

type ExtensionManifest struct {
	ID      string
	Version string
	Format  int // 1 for this version

	AssetData            AssetInfo
	ExtensionRuntimeData ExtensionRuntime
	DiscoveryData        DiscoveryMetadata
	AccessControlData    AccessControl
	DeveloperData        DeveloperInfo
}

// TODO (5/14/19) ScreenshotPaths and IconPaths are deprecated and should be removed
type DiscoveryMetadata struct {
	Summary             string
	ViewerSummary       string
	Description         string
	ScreenshotPaths     []string
	ScreenshotURLs      []string
	Categories          []string
	EULATOSURL          string
	Games               []int
	ContentMatchedGames []int
	IconPaths           map[IconSize]string
	IconURLs            map[IconSize]string
	PrivacyPolicyURL    string
	SupportEmail        string
	Name                string
	AuthorName          string
}

type ExtensionRuntime struct {
	BitsEnabled                  bool
	BitsSupportLevel             BitsSupportLevel
	ConfigurationLocation        ConfigurationLocation
	HasChatSupport               bool
	RequestIdentityLink          bool
	RequiredBroadcasterAbilities []string
	RequiredConfigurationString  string
	SKU                          string
	SubscriptionsSupportLevel    SubscriptionsSupportLevel
	VendorCode                   string
	Views                        *ManifestViews
	WhitelistedConfigURLs        []string
	WhitelistedPanelURLs         []string
}

type AccessControl struct {
	BroadcasterWhitelist []string
	TestingAccounts      []string
}

type UpdateAssetInfoRequest struct {
	Info  AssetInfo
	Token []byte
}

type UpdateImagePathRequest struct {
	Path  string
	Type  string
	Token []byte
}

type AssetInfo struct {
	ExtensionID string
	AssetHash   string
	UploadedBy  string
	UploadedAt  *time.Time
	FileSize    string
	FileName    string
	UploadID    string
	BaseURI     *string
}

type DeveloperInfo struct {
	AuthorEmail        string
	LastTransitionedAt *time.Time
	CreatedAt          *time.Time
	TestingBaseURI     string
	VersionState       string
}

type ManifestViews struct {
	Component    *ComponentManifest
	Config       *ConfigManifest
	Hidden       *HiddenManifest
	LiveConfig   *LiveConfigManifest
	Mobile       *MobileManifest
	Panel        *PanelManifest
	VideoOverlay *VideoOverlayManifest
}

type ComponentManifest struct {
	AspectHeight int // deprecated in favor of X and Y below
	AspectWidth  int // deprecated in favor of X and Y below
	AspectRatioX int
	AspectRatioY int
	Autoscale    bool
	ScalePixels  int
	Size         int // fixed point * 10000 for consistent marshalling
	TargetHeight int // fixed point * 10000 for consistent marshalling
	ViewerPath   string
	Zoom         bool // deprecated in favor of Autoscale above
	ZoomPixels   int  // deprecated in favor of ScalePixels above
}

type ConfigManifest struct {
	ViewerPath string
}

type HiddenManifest struct {
	ViewerPath string
}

type LiveConfigManifest struct {
	ViewerPath string
}

type MobileManifest struct {
	ViewerPath string
}

type PanelManifest struct {
	Height     int
	ViewerPath string
}

type VideoOverlayManifest struct {
	ViewerPath string
}

func (m *ExtensionManifest) GetID() string {
	return m.ID
}
func (m *ExtensionManifest) GetVersion() string {
	return m.Version
}
func (m *ExtensionManifest) GetAssetHash() string {
	return m.AssetData.AssetHash
}
