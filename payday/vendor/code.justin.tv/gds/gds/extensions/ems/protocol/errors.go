package protocol

import (
	"net/http"

	"code.justin.tv/gds/gds/golibs/errors"
)

// Any new Error Codes should maintain Alphabetization.
// Any new Error Codes should map to an error following the conventions below.
const (
	// Unless otherwise commented, ErrCode{ERROR_NAME} is used by Err{ERROR_NAME} below.

	ErrCodeAssetMismatch            = "asset_mismatch"
	ErrCodeAssetNotFoundManifest    = "asset_not_found_in_manifest"
	ErrCodeBadPagingToken           = "bad_paging_token"
	ErrCodeBitsIneligibleTransition = "forbidden_transition_to_bits_required"
	// ErrMonetizationRequired
	ErrCodeChannelNotMonetized                   = "channel_not_monetized"
	ErrCodeExtensionAlreadyInstalled             = "extension_already_installed"
	ErrCodeExtensionIDAndVersionRequired         = "extension_id_and_version_required"
	ErrCodeExtensionIDRequired                   = "extension_id_required"
	ErrCodeExtensionNotInstalled                 = "extension_not_installed"
	ErrCodeInvalidAssetHash                      = "invalid_asset_hash"
	ErrCodeInvalidChannelID                      = "invalid_channel_id"
	ErrCodeInvalidIconSize                       = "invalid_icon_size"
	ErrCodeInvalidImageAssetsType                = "invalid_image_asset_type"
	ErrCodeInvalidImageUrl                       = "invalid_image_url"
	ErrCodeInvalidSubscriptionsSupportTransition = "invalid_subscriptions_support_transition"
	ErrCodeInvalidUploadImageBody                = "invalid_upload_image_body"
	ErrCodeMissingParameter                      = "missing_required_parameter"
	ErrCodeNotEnoughUploadDiscovery              = "not_enough_upload_discovery"
	ErrCodeNotEnoughUploadLogo                   = "not_enough_upload_logo"
	ErrCodeNotEnoughUploadScreenshot             = "not_enough_upload_screenshot"
	ErrCodeNotEnoughUploadTaskbar                = "not_enough_upload_taskbar"
	ErrCodeNullUploader                          = "null_uploader"
	ErrCodeTooManyUploadDiscovery                = "too_many_upload_discovery"
	ErrCodeTooManyUploadLogo                     = "too_many_upload_logo"
	ErrCodeTooManyUploadScreenshot               = "too_many_upload_screenshot"
	ErrCodeTooManyUploadTaskbar                  = "too_many_upload_taskbar"
	ErrCodeUsingRelativeWithAbsolutePaths        = "using_relative_with_absolute_paths"
	ErrCodeVersionAlreadyExists                  = "version_already_exists"
)

var (
	ErrAssetMismatch = errors.NewBuilder("Replacement record has a different ID or asset hash from original").
				WithErrorCode(ErrCodeAssetMismatch).
				WithHTTPStatus(http.StatusBadRequest).
				Build()

	// ErrAssetNotFoundManifest is returned when the asset cannot be found in the Manifest
	ErrAssetNotFoundManifest = errors.NewBuilder("The asset was not found in manifest").
					WithErrorCode(ErrCodeAssetNotFoundManifest).
					WithHTTPStatus(http.StatusNotFound).
					Build()

	ErrBadPagingToken = errors.NewBuilder("bad paging token").
				WithErrorCode(ErrCodeBadPagingToken).
				WithHTTPStatus(http.StatusBadRequest).
				Build()

	// ErrBitsIneligibleTransition is returned when a new extension manifest's desired Bits Feature Level isn't a valid
	// destination from the current Bits Feature Level
	ErrBitsIneligibleTransition = errors.NewBuilder("Invalid state transition").
					WithErrorCode(ErrCodeBitsIneligibleTransition).
					WithHTTPStatus(http.StatusBadRequest).
					Build()

	// ErrExtensionAlreadyInstalled is if the user attempts to install an
	// extension which had already been installed on their channel
	ErrExtensionAlreadyInstalled = errors.NewBuilder("extension already installed").
					WithErrorCode(ErrCodeExtensionAlreadyInstalled).
					WithHTTPStatus(http.StatusConflict).
					Build()

	ErrExtensionIDAndVersionRequired = errors.NewBuilder("extension id and version required").
						WithErrorCode(ErrCodeExtensionIDAndVersionRequired).
						WithHTTPStatus(http.StatusUnprocessableEntity).
						Build()

	ErrExtensionIDRequired = errors.NewBuilder("A valid (and non-empty) Extension ID must be provided.").
				WithErrorCode(ErrCodeExtensionIDRequired).
				WithHTTPStatus(http.StatusBadRequest).
				Build()

	// ErrExtensionNotInstalled is if the user attempts to activate or set feature flags on an
	// extension which was not already installed on their channel
	ErrExtensionNotInstalled = errors.NewBuilder("extension not installed").
					WithErrorCode(ErrCodeExtensionNotInstalled).
					WithHTTPStatus(http.StatusConflict).
					Build()

	ErrInvalidAssetHash = errors.NewBuilder("Invalid extension asset hash").
				WithErrorCode(ErrCodeInvalidAssetHash).
				WithHTTPStatus(http.StatusBadRequest).
				Build()

	// ErrInvalidChannelID is returned when the channel id is an empty string
	ErrInvalidChannelID = errors.NewBuilder("invalid channel id").
				WithErrorCode(ErrCodeInvalidChannelID).
				WithHTTPStatus(http.StatusBadRequest).
				Build()

	ErrInvalidIconSize = errors.NewBuilder("invalid icon size").
				WithErrorCode(ErrCodeInvalidIconSize).
				WithHTTPStatus(http.StatusBadRequest).
				Build()

	// ErrInvalidImageAssetsType is returned when an invalid image asset key for ImageAssetMap is provided
	ErrInvalidImageAssetsType = errors.NewBuilder("Invalid image asset type").
					WithErrorCode(ErrCodeInvalidImageAssetsType).
					WithHTTPStatus(http.StatusUnprocessableEntity).
					Build()

	ErrInvalidImageUrl = errors.NewBuilder("invalid image url").
				WithErrorCode(ErrCodeInvalidImageUrl).
				WithHTTPStatus(http.StatusBadRequest).
				Build()

	// ErrCodeInvalidSubscriptionsSupportTransition when the subscriptions support level of an extension is
	// attempted to be changed when already once released with the level set to "required"
	ErrInvalidSubscriptionsSupportTransition = errors.NewBuilder("invalid subscription support level transition").
							WithErrorCode(ErrCodeInvalidSubscriptionsSupportTransition).
							WithHTTPStatus(http.StatusBadRequest).
							Build()

	// ErrInvalidUploadImageBody is returned when an image upload request body doesn't match expectations
	ErrInvalidUploadImageBody = errors.NewBuilder("Invalid body. Must have 'logo', 'taskbar', 'discovery' and 'screenshot'").
					WithErrorCode(ErrCodeInvalidUploadImageBody).
					WithHTTPStatus(http.StatusBadRequest).
					Build()

	// ErrMissingParameter is returned when a request is missing a required parameter
	ErrMissingParameter = errors.NewBuilder("Missing required parameter").
				WithErrorCode(ErrCodeMissingParameter).
				WithHTTPStatus(http.StatusBadRequest).
				Build()

	// ErrMonetizationRequired is returned when a channel is required to be monetized to use an API and that
	// channel is not.
	ErrMonetizationRequired = errors.NewBuilder("channel is not monetized").
				WithErrorCode(ErrCodeChannelNotMonetized).
				WithHTTPStatus(http.StatusForbidden).
				Build()

	// ErrNotEnoughUploadDiscovery is returned when there is not enough discovery image to upload
	ErrNotEnoughUploadDiscovery = errors.NewBuilder("Not enough discovery, must >= 0").
					WithErrorCode(ErrCodeNotEnoughUploadDiscovery).
					WithHTTPStatus(http.StatusBadRequest).
					Build()

	// ErrNotEnoughUploadLogo is returned when there is not enough logo to upload
	ErrNotEnoughUploadLogo = errors.NewBuilder("Not enough logo for upload, must >= 0").
				WithErrorCode(ErrCodeNotEnoughUploadLogo).
				WithHTTPStatus(http.StatusBadRequest).
				Build()

	// ErrNotEnoughUploadScreenshot is returned when there is not enough screenshot to upload
	ErrNotEnoughUploadScreenshot = errors.NewBuilder("Not enough screenshot, must >= 0").
					WithErrorCode(ErrCodeNotEnoughUploadScreenshot).
					WithHTTPStatus(http.StatusBadRequest).
					Build()

	// ErrNotEnoughUploadTaskbar is returned when there is not enough taskbar icon to upload
	ErrNotEnoughUploadTaskbar = errors.NewBuilder("Not enough taskbar icon for upload, must >= 0").
					WithErrorCode(ErrCodeNotEnoughUploadTaskbar).
					WithHTTPStatus(http.StatusBadRequest).
					Build()

	// ErrNullUploader is returned by a store when a load request can't be fulfilled
	ErrNullUploader = errors.NewBuilder("Null Uploader").
			WithErrorCode(ErrCodeNullUploader).
			WithHTTPStatus(http.StatusServiceUnavailable).
			Build()

	// ErrTooManyUploadDiscovery is returned when there are too many discovery image to upload
	ErrTooManyUploadDiscovery = errors.NewBuilder("Too many discovery, must <= 1").
					WithErrorCode(ErrCodeTooManyUploadDiscovery).
					WithHTTPStatus(http.StatusBadRequest).
					Build()

	// ErrTooManyUploadLogo is returned when there is too many logo to upload
	ErrTooManyUploadLogo = errors.NewBuilder("Too many logo, must <= 1").
				WithErrorCode(ErrCodeTooManyUploadLogo).
				WithHTTPStatus(http.StatusBadRequest).
				Build()

	// ErrTooManyUploadScreenshot is returned when there are too many screenshot to upload
	ErrTooManyUploadScreenshot = errors.NewBuilder("Too many screenshot, existing + uploading screenshots must <= 6").
					WithErrorCode(ErrCodeTooManyUploadScreenshot).
					WithHTTPStatus(http.StatusBadRequest).
					Build()

	// ErrTooManyUploadTaskbar is returned when there is too many taskbar icon to upload
	ErrTooManyUploadTaskbar = errors.NewBuilder("Too many screenshot, must <= 1").
				WithErrorCode(ErrCodeTooManyUploadTaskbar).
				WithHTTPStatus(http.StatusBadRequest).
				Build()

	// ErrUnauthorized is returned when auth for a given API fails in one way or another.
	ErrUnauthorized = errors.ErrUnauthorized

	// ErrCodeUsingRelativeWithAbsolutePaths is returned encountering an extension with
	// a mix of relative and absolute paths
	ErrUsingRelativeWithAbsolutePaths = errors.NewBuilder("cannot mix relative and absolute paths").
						WithErrorCode(ErrCodeUsingRelativeWithAbsolutePaths).
						WithHTTPStatus(http.StatusConflict).
						Build()

	// ErrCodeVersionAlreadyExists is returned when trying to clone to version that already exists
	ErrVersionAlreadyExists = errors.NewBuilder("cannot clone to a version that already exists").
				WithErrorCode(ErrCodeVersionAlreadyExists).
				WithHTTPStatus(http.StatusConflict).
				Build()
)
