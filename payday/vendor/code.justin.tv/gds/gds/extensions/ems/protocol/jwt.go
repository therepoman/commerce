package protocol

// AssetJWTPayload is the payload model used for jwt verification used when processing assets.
// it is contains only when the token expires which is the default expiration of the upload service
// + 5 seconds. We sign the token, pass it through the upload service, verify the token on completion.
// This is to ensure no one can spoof asset uploads of an arbitrary zip file.
type AssetJWTPayload struct {
	ExtensionID string `json:"extension_id"`
	Version     string `json:"version"`
	Expires     int64  `json:"exp"`
}
