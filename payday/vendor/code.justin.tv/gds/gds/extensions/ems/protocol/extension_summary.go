package protocol

import (
	"time"
)

type ExtensionSummary struct {
	ID                                string
	ReleasedBitsSupportLevel          *BitsSupportLevel
	ReleasedSubscriptionsSupportLevel *string
	ReleasedVersion                   *string
	FirstReleasedAt                   *time.Time
	LastReleasedAt                    *time.Time
	Deleted                           bool
	VersionStates                     map[string]string     // does not include records for InTest
	VersionLastTransitionedAt         map[string]time.Time  // Includes records for any thing that has ever transitioned
}

