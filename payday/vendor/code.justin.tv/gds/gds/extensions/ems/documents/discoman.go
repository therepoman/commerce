package documents

import "time"

// RESPONSES

type CategoryDocument struct {
	ID          string  `json:"id"`
	Type        string  `json:"type"`
	Order       float32 `json:"order"`
	Slug        *string `json:"slug,omitempty"`
	SortKey     string  `json:"sort_key"`
	Readonly    bool    `json:"readonly"`
	Visible     bool    `json:"visible"`
	Deleted     bool    `json:"deleted"`
	Name        string  `json:"name"`
	Description string  `json:"description"`
}

type CategoryExtensionsDocument struct {
	IDs        []string `json:"ids"`
	TotalCount int32    `json:"total_count"`
}

type CategoriesDocument struct {
	Count      int                 `json:"count"`
	Categories []*CategoryDocument `json:"categories"`
}

type ExtensionCategoryMembershipDocument struct {
	ExtensionID string     `json:"extension_id"`
	CategoryID  string     `json:"category_id"`
	StartTime   *time.Time `json:"start_time,omitempty"`
	Order       *float32   `json:"order,omitempty"`
}

type ExtensionGameMembershipDocument struct {
	ExtensionID string `json:"extension_id"`
	GameID      int    `json:"game_id"`
}

type CategoryOrder struct {
	ID    string  `json:"id"`
	Order float32 `json:"order"`
}

type CategoryOrderDocument struct {
	CategoryOrdering []*CategoryOrder `json:"category_ordering"`
}

type FeaturedScheduleDocument struct {
	ID              string                    `json:"id"`
	Description     string                    `json:"description"`
	Slug            *string                   `json:"slug,omitempty"`
	CurrentCarousel *FeaturedCarouselDocument `json:"current_carousel"`
}

type FeaturedCarouselDocument struct {
	ID         string                           `json:"id"`
	ScheduleID string                           `json:"schedule_id"`
	StartTime  time.Time                        `json:"start_time"`
	Entries    *FeaturedCarouselEntriesDocument `json:"entries"`
}

type FeaturedCarouselEntryContent struct {
	ClickThrough string `json:"click_through"`
	ImageURL     string `json:"image_url"`
	Title        string `json:"title"`
}

type FeaturedCarouselEntryDocument struct {
	ID         string                       `json:"id"`
	CarouselID string                       `json:"carousel_id"`
	Content    FeaturedCarouselEntryContent `json:"content"`
	Order      float32                      `json:"float32"`
}

type FeaturedSchedulesDocument struct {
	Count     int                         `json:"count"`
	Schedules []*FeaturedScheduleDocument `json:"schedules"`
}

type FeaturedCarouselsDocument struct {
	Count     int                         `json:"count"`
	Carousels []*FeaturedCarouselDocument `json:"carousels"`
}

type FeaturedCarouselEntriesDocument struct {
	Count   int                              `json:"count"`
	Entries []*FeaturedCarouselEntryDocument `json:"entries"`
}

type ExtensionRecommendationsDocument struct {
	ExtensionIDs []string `json:"extension_ids"`
}

// REQUESTS

type AddCategoryRequest struct {
	// these are pointers so the default value of NULL can let the update
	// method know to leave the field alone.
	Order    *float32 `json:"order"`
	Readonly *bool    `json:"readonly"`
	Slug     *string  `json:"slug"`
	SortKey  *string  `json:"sort_key"`
	Type     *string  `json:"type"`
	Visible  *bool    `json:"visible"`
}

type GetCategoriesRequest struct {
	Limit          int
	Offset         int
	Type           string
	IncludeHidden  bool
	IncludeDeleted bool
	Language       string
}

type GetCategoryExtensionsRequest struct {
	Limit  int
	Offset int
}

type EditCategoryTranslationRequest struct {
	Name        string `json:"name"`
	Description string `json:"description"`
}

type OrderCategoriesRequest struct {
	Categories []string `json:"categories"`
}

type OrderCategoryRequest struct {
	Extensions []string `json:"extensions"`
}

type AddExtensionToCategoryRequest struct {
	StartTime *time.Time `json:"start_time"`
	Order     *float32   `json:"order"`
}

type AddFeaturedScheduleRequest struct {
	Description string  `json:"description"`
	Slug        *string `json:"slug"`
}

type AddFeaturedCarouselRequest struct {
	ScheduleID string    `json:"schedule_id"`
	StartTime  time.Time `json:"start_time"`
}

type AddFeaturedCarouselEntryRequest struct {
	// pointers to make sure it's optional for update
	CarouselID *string                       `json:"carousel_id"`
	Content    *FeaturedCarouselEntryContent `json:"content"`
	Order      *float32                      `json:"order"`
}

type OrderFeaturedCarouselRequest struct {
	EntryIDs []string `json:"entry_ids"`
}
