package documents

import (
	"encoding/json"
	"fmt"

	"code.justin.tv/gds/gds/extensions"

	"code.justin.tv/gds/gds/extensions/ems/protocol"
)

const (
	// VideoOverlaySlotName is the singleton slot name assigned to video overlays, since there can be only one
	VideoOverlaySlotName string = "extension-overlay-1"
)

// Deprecation after devsite migration
// Manifest is the JSON structure the client will use to upload a new extension
// into the manager service.
// note fields "IconURL" and "ScreenshotURLs" allow the sender to
// specify a full URL to the icons and screenshots instead of having us upload
// them
type Manifest struct {
	AssetHash                    string                             `json:"asset_hash"`
	AssetPaths                   []string                           `json:"asset_paths,omitempty"`
	AuthorEmail                  string                             `json:"author_email"`
	AuthorName                   string                             `json:"author_name"`
	BitsEnabled                  bool                               `json:"bits_enabled"` // deprecated: use BitsSupportLevel instead
	BitsSupportLevel             protocol.BitsSupportLevel          `json:"bits_support_level"`
	BroadcasterWhitelist         []string                           `json:"broadcaster_whitelist"`
	Categories                   []string                           `json:"categories"`
	ConfigurationLocation        string                             `json:"configuration_location"`
	Description                  string                             `json:"description"`
	EULATOSURL                   string                             `json:"eula_tos_url"`
	ExtensionID                  string                             `json:"extension_id"`
	HasChatSupport               bool                               `json:"has_chat_support"`
	Games                        []int                              `json:"games"`
	IconPath                     string                             `json:"icon_path"`
	IconPaths                    map[protocol.IconSize]string       `json:"icon_paths"`
	IconURL                      string                             `json:"icon_URL"`
	IconURLs                     map[protocol.IconSize]string       `json:"icon_urls"`
	Name                         string                             `json:"name"`
	PrivacyPolicyURL             string                             `json:"privacy_policy_url"`
	RequestIdentityLink          bool                               `json:"request_identity_link"`
	RequiredBroadcasterAbilities []string                           `json:"required_broadcaster_abilities"`
	RequiredConfigurationString  string                             `json:"required_configuration_string"`
	ScreenshotPaths              []string                           `json:"screenshot_paths"`
	ScreenshotURLs               []string                           `json:"screenshot_urls"`
	SKU                          string                             `json:"sku"`
	SubscriptionsSupportLevel    protocol.SubscriptionsSupportLevel `json:"subscriptions_support_level"`
	Summary                      string                             `json:"summary"`
	SupportEmail                 string                             `json:"support_email"`
	TestingAccounts              []string                           `json:"testing_accounts"`
	TestingBaseURI               string                             `json:"testing_base_uri"`
	VendorCode                   string                             `json:"vendor_code"`
	Version                      string                             `json:"extension_version"`
	Views                        ManifestViews                      `json:"views"`
	ViewerSummary                string                             `json:"viewer_summary"`
	WhitelistedConfigURLs        []string                           `json:"whitelisted_config_urls"`
	WhitelistedPanelURLs         []string                           `json:"whitelisted_panel_urls"`
}

type ManifestViews struct {
	Panel        PanelManifest        `json:"panel"`
	VideoOverlay VideoOverlayManifest `json:"video_overlay"`
	Component    ComponentManifest    `json:"component"`
	Mobile       MobileManifest       `json:"mobile"`
	Config       ConfigManifest       `json:"config"`
	LiveConfig   LiveConfigManifest   `json:"live_config"`
}

type PanelManifest struct {
	ViewerPath string `json:"viewer_path"`
	Height     int    `json:"height"`
}

type VideoOverlayManifest struct {
	ViewerPath string `json:"viewer_path"`
}

type ComponentManifest struct {
	ViewerPath   string  `json:"viewer_path"`
	AspectRatioX int     `json:"aspect_ratio_x"`
	AspectRatioY int     `json:"aspect_ratio_y"`
	AspectWidth  int     `json:"aspect_width"`
	AspectHeight int     `json:"aspect_height"`
	Autoscale    bool    `json:"autoscale"`
	ScalePixels  int     `json:"scale_pixels"`
	Size         float64 `json:"size"`
	TargetHeight int     `json:"target_height"`
	Zoom         bool    `json:"zoom"`
	ZoomPixels   int     `json:"zoom_pixels"`
}

type MobileManifest struct {
	ViewerPath string `json:"viewer_path"`
}

type ConfigManifest struct {
	ViewerPath string `json:"viewer_path"`
}

type LiveConfigManifest struct {
	ViewerPath string `json:"viewer_path"`
}

// StateTransition is the JSON structure the client provides when moving
// an extension from, say, testing to review.
type StateTransition struct {
	State string `json:"state"`
}

// DeprecatedExtensionActivation is the JSON structure the client provides when activating
// or deactivating an extension.  It will disappear once the front end migrates to the new
// thing below.
type DeprecatedExtensionActivation struct {
	Active bool   `json:"active"`
	Config string `json:"config"`
}

// ExtensionActivation is the JSON structure the client provides when activating
// or deactivating an extension.  It will disappear once the front end migrates to the new
// thing below.
type ExtensionActivation struct {
	Active           bool                             `json:"active"`
	ActivationConfig *ActivationConfigurationDocument `json:"activation_config"`
}

// RequiredInstallationConfiguration is the JSON structure the extension backend provides when
// letting us know that an extension is adequately configured on a channel.  The provided string
// must match the string specified in the extension's manifest in order for activation to be allowed.
type RequiredInstallationConfiguration struct {
	RequiredConfigurationString string `json:"required_configuration_string"`
}

// SearchTerm is the JSON structure that describes a field to search on, and a term to search for.
type SearchTerm struct {
	Field string  `json:"field"`
	Term  string  `json:"term"`
	Boost float64 `json:"boost"`
}

// SortField is the JSON structure that describes a field to sort on, and the sort direction.
type SortField struct {
	Field     string `json:"field"`
	Direction string `json:"direction"`
}

// GetExtensionsParams captures everything that GetExtensions needs to know to search for
// the desired extensions -- search terms, sorts, and pagination information
type GetExtensionsParams struct {
	Searches []SearchTerm `json:"searches"`
	Sorts    []SortField  `json:"sorts"`
	Offset   int          `json:"offset"`
	Limit    int          `json:"limit"`
}

// AddExtensionImageAssetsFlags is what devsite users use when requesting upload URLs for different
// extension image types.
type AddExtensionImageAssetsFlags struct {
	Logo       *int `json:"logo"`
	Taskbar    *int `json:"taskbar"`
	Discovery  *int `json:"discovery"`
	Screenshot *int `json:"screenshot"`
}

// AddExtensionZipAssetRequest is what devsite sends us to create a zip asset upload request.
// EMS passes this service to the upload service.
type AddExtensionZipAssetRequest struct {
	Filename string `json:"filename"`
}

// DeleteExtensionAssetsParams is what devsite users send when deleting extension assets
// A valid url format would be:
// https://s3-us-west-2.amazonaws.com/:upload_bucket/:id/:version/:image_type:upload_id
// Image type could be logo, taskbar, discovery or screenshot
type DeleteExtensionAssetsParams struct {
	Urls []string `json:"urls"`
}

// OauthReceipt is what the extension backend sends us to indicate the broadcaster's
// decision regarding required permissions on activation
type OauthReceipt struct {
	PermissionsGranted bool `json:"permissions_granted"`
}

// ActivationConfigurationParams represents the multiple per-channel activation details
type ActivationConfigurationParams struct {
	Activations []ActivationConfigurationParam `json:"activations"`
}

// ActivationConfigurationParam represents a single activation/deactivation of an extension on a channel
type ActivationConfigurationParam struct {
	ID      string         `json:"id"`
	Version string         `json:"version"`
	Anchor  AnyAnchorParam `json:"configuration"`
}

// DynamicManagementParam represents whether or not the extension's activation state will
// be dynamically managed
type DynamicManagementParam struct {
	GameID    int  `json:"game_id"`
	IsManaged bool `json:"is_managed"`
}

// SetFeatureFlagsDocument represents the abilities an installation has based on the requesting
// user and the channel the installation is on
type SetFeatureFlagsDocument struct {
	CanSendChat       *bool                    `json:"can_send_chat"`
	CanUseBits        *bool                    `json:"can_use_bits"`
	DynamicManagement []DynamicManagementParam `json:"dynamic_management"`
}

// SetBroadcasterOAuthDocument contains the required input to set a broadcaster OAuth on an installation
type SetBroadcasterOAuthDocument struct {
	OAuthToken string `json:"oauth_token"`
}

type AnchorType string
type AnchorSlot string

type AnchorParam interface {
	Type() AnchorType
	Slot() AnchorSlot
}

// TODO: Remove this when we can do proper wire protocol versioning.
// NullAnchorActivationParam is used to maintain backwards compatibility with the previous activationConfig struct
// where an inactive anchor would be returned as an empty config, rather than a null field.
type NullAnchorActivationParam struct {
	SlotName string `json:"slot"`
}

func (*NullAnchorActivationParam) Type() AnchorType   { return AnchorType("") }
func (p *NullAnchorActivationParam) Slot() AnchorSlot { return AnchorSlot("") }

type PanelAnchorActivationParam struct {
	SlotName string `json:"slot"`
}

func (*PanelAnchorActivationParam) Type() AnchorType   { return AnchorType("panel") }
func (p *PanelAnchorActivationParam) Slot() AnchorSlot { return AnchorSlot(p.SlotName) }

type VideoOverlayAnchorActivationParam struct {
	SlotName string `json:"slot"`
}

func (*VideoOverlayAnchorActivationParam) Type() AnchorType { return AnchorType("video_overlay") }
func (p *VideoOverlayAnchorActivationParam) Slot() AnchorSlot {
	return AnchorSlot(VideoOverlaySlotName)
}

type ComponentAnchorActivationParam struct {
	SlotName string `json:"slot"`
	X        int    `json:"x"`
	Y        int    `json:"y"`
}

func (*ComponentAnchorActivationParam) Type() AnchorType   { return AnchorType("component") }
func (p *ComponentAnchorActivationParam) Slot() AnchorSlot { return AnchorSlot(p.SlotName) }

type HiddenAnchorActivationParam struct {
	SlotName string `json:"slot"`
}

func (*HiddenAnchorActivationParam) Type() AnchorType   { return AnchorType("hidden") }
func (p *HiddenAnchorActivationParam) Slot() AnchorSlot { return AnchorSlot(p.SlotName) }

type AnyAnchorParam struct{ Value AnchorParam }

func (a AnyAnchorParam) String() string {
	return fmt.Sprintf("%+v", a.Value)
}

func (a AnyAnchorParam) MarshalJSON() ([]byte, error) {
	param := a.Value
	if param == nil {
		param = &NullAnchorActivationParam{}
	}

	data := make(map[string]interface{})
	bytes, err := json.Marshal(param)
	if err == nil {
		err = json.Unmarshal(bytes, &data)
	}
	if err != nil {
		return nil, err
	}
	data["anchor"] = string(param.Type())
	return json.Marshal(data)
}

func (a *AnyAnchorParam) UnmarshalJSON(bytes []byte) error {
	typer := struct {
		Type string `json:"anchor"`
	}{}
	if err := json.Unmarshal(bytes, &typer); err != nil {
		return err
	}

	a.Value = nil
	switch typer.Type {
	case "":
		// Handle the null anchor case separately from invalid anchors
		a.Value = &NullAnchorActivationParam{}
	case "component":
		a.Value = &ComponentAnchorActivationParam{}
	case "video_overlay":
		a.Value = &VideoOverlayAnchorActivationParam{}
	case "panel":
		a.Value = &PanelAnchorActivationParam{}
	case "hidden":
		a.Value = &HiddenAnchorActivationParam{}
	default:
		return extensions.ErrInvalidAnchor
	}
	return json.Unmarshal(bytes, &a.Value)
}

func (m *Manifest) GetID() string {
	return m.ExtensionID
}
func (m *Manifest) GetVersion() string {
	return m.Version
}
func (m *Manifest) GetAssetHash() string {
	return m.AssetHash
}
