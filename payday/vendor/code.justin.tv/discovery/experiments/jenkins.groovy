job {
    name 'discovery-experiments'
    using 'TEMPLATE-autobuild'
    concurrentBuild true

    scm {
        git {
            remote {
                github 'discovery/experiments', 'ssh', 'git.xarth.tv'
                credentials 'git-aws-read-key'
            }
            clean true
        }
    }

    wrappers {
        sshAgent 'git-aws-read-key'
        preBuildCleanup()
        timestamps()
        credentialsBinding {
            file('AWS_CONFIG_FILE', 'aws_config')
            string 'AWS_ACCESS_KEY', 'twitch-discovery-aws-access-key'
            string 'AWS_SECRET_KEY', 'twitch-discovery-aws-secret-key'
        }
    }

    steps {
        shell 'manta -v -f build.json'
        shell 'AWS_ACCESS_KEY_ID=\$AWS_ACCESS_KEY AWS_SECRET_ACCESS_KEY=\$AWS_SECRET_KEY ./.manta/main'
    }
}
