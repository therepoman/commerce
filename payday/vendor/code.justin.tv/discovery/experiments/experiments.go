// Package experiments provides a way to assign requests to groups to run experiments.
package experiments

import (
	"crypto/sha1"
	"encoding/binary"
	"math"
	"sync"
	"time"

	"code.justin.tv/discovery/experiments/experiment"
	op "code.justin.tv/discovery/experiments/overridepolling"
	"code.justin.tv/hygienic/spade"
)

// Experiments describes an experiment framework
type Experiments interface {
	// RegisterDefault add a default value for an experiment.
	RegisterDefault(kind experiment.Type, experimentName, experimentID, defaultGroup string)
	// RegisterOverride stores a local only override for a particular experiment and ids
	RegisterOverride(kind experiment.Type, experimentName, experimentID, group string, ids ...string) error
	// Treat buckets an identifier to an experiment group.
	// This function is deterministic: as long as the experiment does not change, calling it with the same parameters will
	// always return the same result.
	Treat(kind experiment.Type, experimentName, experimentID, id string) (string, error)
	// Treat behaves identically to Treat(), but allows for passing in a default rather than requiring it to be registered beforehand.
	TreatWithDefault(kind experiment.Type, experimentName, experimentID, id string, def experiment.Default) (string, error)
	// Override the id into the override group if the id is in the input treatment group, return the final group id is assigned to
	Override(kind experiment.Type, experimentName, experimentID, id, treatmentGroup, overrideGroup string) (string, error)
	// OverrideToGroup overrides the id directly to the passed in group regardless what group id is supposed to be in
	OverrideToGroup(kind experiment.Type, experimentName, experimentID, id, overrideGroup string) error
	// GetExperiment returns the Experiment that corresponds to the given name, or nil if the Experiment is
	// not defined in the datasource.
	GetExperiment(experimentID string) *Experiment
	// StartOverridePolling will start a goroutine to constantly poll users that needs to be registered as overrides from S3
	StartGlobalOverridePolling()
	// Close stops any goroutines created by the Experiments instance.
	Close() error
}

// Provider provides access to experiments stored in a datasource.
type Provider interface {
	// GetExperiment returns the Experiment that corresponds to the given name, or nil if the Experiment is
	// not defined in the datasource.
	GetExperiment(experimentID string) *Experiment

	// Close signals to the provider that it should stop any background work.
	Close()
}

// Config defines the configuration info needed by the default Experiments implementation.
//
// The Config interface allows us to configure experiments using fixed values now, and will allow us to integrate
// with configuration frameworks (like distconf) in the future.
type Config struct {
	// Platform describes which platform should be used to source experiments from
	Platform experiment.Platform

	// SpadeEndpoint is the URL to the Spade server that tracking events should be sent to.
	SpadeEndpoint string

	SpadeBacklogSize int64

	// DisableSpadeTracking disables sending tracking events to Spade.
	DisableSpadeTracking bool

	// PollingInterval is the amount of time to wait before re-fetching experiments from
	// GetSpadeEndpoint. Default is DefaultPollingInterval
	PollingInterval time.Duration

	// ErrorHandler is a function that is run on background errors that occur when fetching experiments and
	// sending tracking events.
	ErrorHandler func(error)

	// Override for the Tracking Client (most likely Spade) used to track experiment_branch events
	TrackingClient Tracking

	// Config for polling users that need to be overridden from S3
	GlobalOverridesConfig *op.GlobalOverridesPollingConfig
}

// New returns the default implementation of Experiments that supports treating users into groups based on
// experiments defined locally, and also experiments defined in minixperiment.
func New(config Config) (Experiments, error) {
	if len(config.Platform) == 0 {
		config.Platform = experiment.LegacyPlatform
	}

	if config.SpadeEndpoint == "" {
		config.SpadeEndpoint = "https://spade.internal.di.twitch.a2z.com"
	}

	if config.SpadeBacklogSize <= 0 {
		config.SpadeBacklogSize = 1024
	}

	if config.PollingInterval <= 0 {
		config.PollingInterval = DefaultPollingInterval
	}

	var trackingClient Tracking
	if config.DisableSpadeTracking {
		trackingClient = noopTrackingClient{}
	} else if config.TrackingClient != nil {
		trackingClient = config.TrackingClient
	} else {
		trackingClient = NewTrackingClient(config)
	}

	mxpProvider, err := newMinixperimentProvider(config.Platform, config.PollingInterval, config.ErrorHandler)
	if err != nil {
		return nil, err
	}

	var globalOverridesPollingClient op.GlobalOverridesPollingClient
	if config.GlobalOverridesConfig != nil {
		globalOverridesPollingClient, err = op.NewOverridesPollingClient(
			config.GlobalOverridesConfig,
			config.ErrorHandler)
		if err != nil {
			return nil, err
		}
	}

	return &experimentImpl{
		defaults:       map[string]experiment.Default{},
		localOverrides: map[string]map[string]string{},
		tracking:       trackingClient,
		providers: []Provider{
			mxpProvider,
		},
		globalOverridesPollingClient: globalOverridesPollingClient,
	}, nil
}

// Dependencies are dependencies that can be injected into the default Experiments implementation.
type Dependencies struct {
	TrackingClient               Tracking
	Providers                    []Provider
	GlobalOverridesPollingClient op.GlobalOverridesPollingClient
}

// NewWithDependencies sets up an instance of Experiments by injecting the given dependencies.  It is meant to
// facilitate testing, and for consumers to set up an Experiments instance with custom Experiment providers.
func NewWithDependencies(deps *Dependencies) Experiments {
	return newWithDependencies(deps)
}

// newWithDependencies is the internal version of NewWithDependencies
func newWithDependencies(deps *Dependencies) *experimentImpl {
	return &experimentImpl{
		defaults:                     map[string]experiment.Default{},
		localOverrides:               map[string]map[string]string{},
		tracking:                     deps.TrackingClient,
		providers:                    deps.Providers,
		globalOverridesPollingClient: deps.GlobalOverridesPollingClient,
	}
}

type experimentBranchTrackingEvent struct {
	Platform          string `json:"platform"`
	Group             string `json:"experiment_group"`
	ExperimentID      string `json:"experiment_id"`
	ExperimentType    string `json:"experiment_type"`
	ExperimentName    string `json:"experiment_name,omitempty"`
	ExperimentVersion int    `json:"experiment_version,omitempty"`
	UserID            string `json:"user_id,omitempty"`
	DeviceID          string `json:"device_id,omitempty"`
	ChannelID         string `json:"channel_id,omitempty"`
}

// Experiment contains the information on an experiment required to treat users into experimental groups.
type Experiment struct {
	ID      string
	Name    string
	Salt    string
	Version int
	Kind    experiment.Type
	Groups  []experiment.Group
}

type experimentImpl struct {
	// Map of experiment names to defaults
	defaults     map[string]experiment.Default
	defaultsLock sync.RWMutex
	// This map is used to memorize overrides that come from developers calling the API
	localOverrides     map[string]map[string]string
	localOverridesLock sync.RWMutex

	tracking                     Tracking
	providers                    []Provider
	globalOverridesPollingClient op.GlobalOverridesPollingClient
}

// Close stops any goroutines created by the Experiments instance.
func (T *experimentImpl) Close() error {
	for _, provider := range T.providers {
		provider.Close()
	}

	if T.globalOverridesPollingClient != nil {
		T.globalOverridesPollingClient.Close()
	}
	return T.tracking.Close()
}

// RegisterDefault add a default value for an experiment.
func (T *experimentImpl) RegisterDefault(kind experiment.Type, experimentName, experimentID, defaultGroup string) {
	T.defaultsLock.Lock()
	defer T.defaultsLock.Unlock()

	T.defaults[experimentID] = experiment.Default{
		ID:           experimentID,
		Kind:         kind,
		DefaultGroup: defaultGroup,
	}
}

// RegisterOverride add an override group for an experiment and id
func (T *experimentImpl) RegisterOverride(kind experiment.Type, experimentName, experimentID, group string, ids ...string) error {
	T.defaultsLock.Lock()
	def, ok := T.defaults[experimentID]
	T.defaultsLock.Unlock()
	if !ok {
		return experiment.ErrNoDefault
	}
	if def.Kind != kind {
		return experiment.ErrInvalidType
	}

	T.localOverridesLock.Lock()
	defer T.localOverridesLock.Unlock()
	_, ok = T.localOverrides[experimentID]
	if !ok {
		T.localOverrides[experimentID] = map[string]string{}
	}

	for _, id := range ids {
		T.localOverrides[experimentID][id] = group
	}
	return nil
}

// experimentIsValid checks whether an experiment can be used
func experimentIsValid(exp Experiment) bool {
	if len(exp.ID) == 0 || (exp.Kind != experiment.UserType && exp.Kind != experiment.DeviceIDType) || len(exp.Name) == 0 || len(exp.Groups) == 0 {
		return false
	}

	sum := 0.0
	for _, g := range exp.Groups {
		if g.Value == "" {
			return false
		}
		sum += g.Weight
	}

	return sum > 0
}

// Treat buckets an identifier to an experiment group.
// This function is deterministic: as long as the experiment does not change, calling it with the same parameters will
// always return the same result.
func (T *experimentImpl) Treat(kind experiment.Type, experimentName, experimentID, id string) (string, error) {
	T.defaultsLock.RLock()
	def, defaultFound := T.defaults[experimentID]
	T.defaultsLock.RUnlock()
	if !defaultFound {
		return "", experiment.ErrNoDefault
	}

	return T.TreatWithDefault(kind, experimentName, experimentID, id, def)
}

func (T *experimentImpl) TreatWithDefault(kind experiment.Type, experimentName, experimentID, id string, def experiment.Default) (string, error) {
	T.localOverridesLock.RLock()
	overrides := T.localOverrides[experimentID]
	if overrides != nil {
		group, groupOk := overrides[id]
		if groupOk {
			T.localOverridesLock.RUnlock()
			return group, nil
		}
	}
	T.localOverridesLock.RUnlock()
	//try to get group from global overrides
	if T.globalOverridesPollingClient != nil {
		group, groupOk := T.globalOverridesPollingClient.GetGlobalOverrideGroup(experimentID, id)
		if groupOk {
			return group, nil
		}
	}

	exp := T.GetExperiment(experimentID)
	if exp == nil {
		if kind != def.Kind {
			return "", experiment.ErrInvalidType
		}

		// Experiment was defaulted but not initialized (minixperiment unavailable or something), use default
		// group
		T.trackExperimentBranch(kind, experimentID, experimentName, id, def.DefaultGroup, -1)
		return def.DefaultGroup, nil
	}

	if kind != exp.Kind {
		return "", experiment.ErrInvalidType
	}

	if g, err := exp.selectTreatment(id); err == nil {
		T.trackExperimentBranch(kind, exp.ID, exp.Name, id, g, exp.Version)
		return g, nil
	}

	// Should never reach this point under normal operation
	T.trackExperimentBranch(kind, experimentID, experimentName, id, def.DefaultGroup, -2)
	return def.DefaultGroup, nil
}

func (T *experimentImpl) OverrideToGroup(kind experiment.Type, experimentName, experimentID, id, overrideGroup string) error {
	exp := T.GetExperiment(experimentID)
	if exp == nil {
		return experiment.ErrFetchExperiment
	}

	if kind != exp.Kind {
		return experiment.ErrInvalidType
	}

	if exp.Name != experimentName {
		return experiment.ErrWrongName
	}

	// Verify the passed in groups is in the list of groups
	var foundOverrideGroup bool = false
	for _, g := range exp.Groups {
		if g.Value == overrideGroup {
			foundOverrideGroup = true
			break
		}
	}
	if !foundOverrideGroup {
		return experiment.ErrInvalidGroup
	}

	T.trackExperimentBranch(kind, exp.ID, exp.Name, id, overrideGroup, exp.Version)

	return nil
}

func (T *experimentImpl) Override(kind experiment.Type, experimentName, experimentID, id, treatmentGroup, overrideGroup string) (string, error) {
	exp := T.GetExperiment(experimentID)
	if exp == nil {
		return "", experiment.ErrFetchExperiment
	}

	if kind != exp.Kind {
		return "", experiment.ErrInvalidType
	}

	if exp.Name != experimentName {
		return "", experiment.ErrWrongName
	}

	// Verify the passed in groups is in the list of groups
	var foundOverrideGroup bool = false
	var foundTreatmentGroup bool = false
	for _, g := range exp.Groups {
		if g.Value == overrideGroup {
			foundOverrideGroup = true
		}
		if g.Value == treatmentGroup {
			foundTreatmentGroup = true
		}
	}
	if !foundOverrideGroup || !foundTreatmentGroup {
		return "", experiment.ErrInvalidGroup
	}

	selectedGroup, err := exp.selectTreatment(id)
	if err != nil {
		return "", err
	}

	if selectedGroup == treatmentGroup {
		T.trackExperimentBranch(kind, exp.ID, exp.Name, id, overrideGroup, exp.Version)
		return overrideGroup, nil
	}

	T.trackExperimentBranch(kind, exp.ID, exp.Name, id, selectedGroup, exp.Version)

	return selectedGroup, nil
}

func (T *experimentImpl) GetExperiment(experimentID string) *Experiment {
	for _, provider := range T.providers {
		if exp := provider.GetExperiment(experimentID); exp != nil {
			return exp
		}
	}

	return nil
}

func (T *experimentImpl) StartGlobalOverridePolling() {
	if T.globalOverridesPollingClient != nil {
		T.globalOverridesPollingClient.StartPolling()
	}
}

func (exp *Experiment) selectTreatment(id string) (string, error) {
	// Hash the experiment ID and device/user ID to a value in the range [0, 1).
	fraction := experimentHash(exp.ID, id, exp.Salt)

	total := 0.0
	for _, g := range exp.Groups {
		total += g.Weight
	}

	for _, g := range exp.Groups {
		fraction -= (g.Weight / total)
		if fraction <= 0 {
			return g.Value, nil
		}
	}

	return "", experiment.ErrOverRan
}

// trackExperimentBranch sends an experiment_branch tracking event to Spade.
func (T *experimentImpl) trackExperimentBranch(kind experiment.Type, experimentID, experimentName, userOrDeviceID, group string, version int) {
	var experimentType string
	var userID string
	var deviceID string
	var channelID string

	switch kind {
	case experiment.DeviceIDType:
		experimentType = "device_id"
		deviceID = userOrDeviceID
	case experiment.UserType:
		experimentType = "user_id"
		userID = userOrDeviceID
		// Use userID as deviceID to alleviate science performance issues
		deviceID = userOrDeviceID
	case experiment.ChannelType:
		experimentType = "channel_id"
		channelID = userOrDeviceID
		// Use channelID as deviceID to alleviate science performance issues
		deviceID = userOrDeviceID
	}

	T.tracking.QueueEvents(spade.Event{
		Name: "experiment_branch",
		Properties: &experimentBranchTrackingEvent{
			Group:             group,
			ExperimentID:      experimentID,
			ExperimentType:    experimentType,
			ExperimentName:    experimentName,
			ExperimentVersion: version,
			UserID:            userID,
			DeviceID:          deviceID,
			ChannelID:         channelID,
			Platform:          "backend-client",
		},
	})
}

// experimentHash converts an experimentID and id pair into a float64 in the range [0, 1).
// This replicates the algorithm used by minixperiment-client-js so that requests are assigned to same groups.
func experimentHash(experimentID, id, salt string) float64 {
	seed := experimentID + id + salt

	// Hash the experiment ID and user/device ID using SHA1.
	hashBytes := sha1.Sum([]byte(seed))

	// Get the first 32 bits of the hash value as an unsigned int.
	hashUint32 := binary.BigEndian.Uint32(hashBytes[:4])

	// Convert to floating point representation.
	hashFloat := float64(hashUint32)

	// Transform to a fraction between [0, 1).
	return hashFloat / float64(math.MaxUint32+1)
}
