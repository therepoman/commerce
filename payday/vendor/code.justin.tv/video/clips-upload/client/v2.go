package clips

import (
	"bytes"
	"encoding/json"

	"code.justin.tv/foundation/twitchclient"
	"golang.org/x/net/context"
)

func (c *client) CreateLiveClipV2(ctx context.Context, params *CreateLiveClipParams, opts *twitchclient.ReqOpts) (*Clip, error) {
	body, err := json.Marshal(params)
	if err != nil {
		return nil, err
	}

	req, err := c.NewRequest("POST", "/api/v2/liveclips", bytes.NewBuffer(body))
	if err != nil {
		return nil, err
	}

	combinedOpts := twitchclient.MergeReqOpts(opts, twitchclient.ReqOpts{
		StatName:       "service.clips.create_live_clip_v2",
		StatSampleRate: defaultStatSampleRate,
	})

	clip := Clip{}
	_, err = c.DoJSON(ctx, &clip, req, combinedOpts)
	if err != nil {
		return nil, err
	}
	return &clip, nil
}
