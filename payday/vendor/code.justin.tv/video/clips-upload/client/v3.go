package clips

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"net/url"
	"strings"

	"golang.org/x/net/context"

	"code.justin.tv/foundation/twitchclient"
)

func (c *client) CreateClipV3(ctx context.Context, params *CreateClipV3Params, opts *twitchclient.ReqOpts) (*Clip, error) {
	path := "/api/v3/clips"

	body, err := json.Marshal(params)
	if err != nil {
		return nil, err
	}

	req, err := c.NewRequest("POST", path, bytes.NewBuffer(body))
	if err != nil {
		return nil, err
	}

	combinedOpts := twitchclient.MergeReqOpts(opts, twitchclient.ReqOpts{
		StatName:       "service.clips.create_clip_v3",
		StatSampleRate: defaultStatSampleRate,
	})

	clip := Clip{}
	_, err = c.DoJSON(ctx, &clip, req, combinedOpts)
	if err != nil {
		return nil, err
	}
	return &clip, nil
}

type DeleteClipBySlugV3Params struct {
	UserID string `json:"user_id"`
}

func (c *client) DeleteClipBySlugV3(ctx context.Context, userID string, slug string, opts *twitchclient.ReqOpts) error {
	path := fmt.Sprintf("/api/v3/clips/%s", slug)

	params := DeleteClipBySlugV3Params{
		UserID: userID,
	}
	body, err := json.Marshal(params)
	if err != nil {
		return err
	}

	req, err := c.NewRequest("DELETE", path, bytes.NewBuffer(body))
	if err != nil {
		return err
	}

	combinedOpts := twitchclient.MergeReqOpts(opts, twitchclient.ReqOpts{
		StatName:       "service.clips.delete_clip_by_slug_v3",
		StatSampleRate: defaultStatSampleRate,
	})
	resp, err := c.Do(ctx, req, combinedOpts)
	if err != nil {
		return err
	}

	if resp.StatusCode >= 400 {
		return twitchclient.HandleFailedResponse(resp)
	}
	return nil
}

type BatchDeleteClipsV3Params struct {
	UserID string   `json:"user_id"`
	Slugs  []string `json:"slugs"`
}

func (c *client) BatchDeleteClipsV3(ctx context.Context, userID string, slugs []string, opts *twitchclient.ReqOpts) (DeletedClips, error) {
	path := "/api/v3/clips"

	params := BatchDeleteClipsV3Params{
		UserID: userID,
		Slugs:  slugs,
	}
	body, err := json.Marshal(params)
	if err != nil {
		return nil, err
	}

	req, err := c.NewRequest("DELETE", path, bytes.NewBuffer(body))
	if err != nil {
		return nil, err
	}

	combinedOpts := twitchclient.MergeReqOpts(opts, twitchclient.ReqOpts{
		StatName:       "service.clips.batch_delete_clips_v3",
		StatSampleRate: defaultStatSampleRate,
	})

	deletedClips := DeletedClips{}
	resp, err := c.DoJSON(ctx, &deletedClips, req, combinedOpts)
	if err != nil {
		if resp != nil && resp.StatusCode == http.StatusNotFound {
			return nil, &twitchclient.Error{StatusCode: http.StatusNotFound, Message: "no clips were found"}
		}
		return nil, err
	}
	return deletedClips, nil
}

type DeleteClipByVODV3Params struct {
	UserID string `json:"user_id"`
}

func (c *client) DeleteClipsByVODV3(ctx context.Context, userID, vodID string, opts *twitchclient.ReqOpts) (DeletedClips, error) {
	path := fmt.Sprintf("/api/v3/vods/%s/clips", vodID)

	params := DeleteClipByVODV3Params{
		UserID: userID,
	}
	body, err := json.Marshal(params)
	if err != nil {
		return nil, err
	}

	req, err := c.NewRequest("DELETE", path, bytes.NewBuffer(body))
	if err != nil {
		return nil, err
	}

	combinedOpts := twitchclient.MergeReqOpts(opts, twitchclient.ReqOpts{
		StatName:       "service.clips.delete_clip_by_vod_v3",
		StatSampleRate: defaultStatSampleRate,
	})

	deletedClips := DeletedClips{}
	resp, err := c.DoJSON(ctx, &deletedClips, req, combinedOpts)
	if err != nil {
		if resp != nil && resp.StatusCode == http.StatusNotFound {
			return nil, &twitchclient.Error{StatusCode: http.StatusNotFound, Message: fmt.Sprintf("no clips were found for vod %s", vodID)}
		}
		return nil, err
	}
	return deletedClips, nil
}

type DeleteClipByBroadcastV3Params struct {
	UserID string `json:"user_id"`
}

func (c *client) DeleteClipsByBroadcastV3(ctx context.Context, userID, broadcastID string, opts *twitchclient.ReqOpts) (DeletedClips, error) {
	path := fmt.Sprintf("/api/v3/broadcasts/%s/clips", broadcastID)

	params := DeleteClipByBroadcastV3Params{
		UserID: userID,
	}
	body, err := json.Marshal(params)
	if err != nil {
		return nil, err
	}

	req, err := c.NewRequest("DELETE", path, bytes.NewBuffer(body))
	if err != nil {
		return nil, err
	}

	combinedOpts := twitchclient.MergeReqOpts(opts, twitchclient.ReqOpts{
		StatName:       "service.clips.delete_clip_by_broadcast_v3",
		StatSampleRate: defaultStatSampleRate,
	})

	deletedClips := DeletedClips{}
	resp, err := c.DoJSON(ctx, &deletedClips, req, combinedOpts)
	if err != nil {
		if resp != nil && resp.StatusCode == http.StatusNotFound {
			return nil, &twitchclient.Error{StatusCode: http.StatusNotFound, Message: fmt.Sprintf("no clips were found for broadcast %s", broadcastID)}
		}
		return nil, err
	}
	return deletedClips, nil
}

type BanUserV3Params struct {
	UserID string `json:"user_id"`
}

func (c *client) BanUserV3(ctx context.Context, channelID, requesterUserID, userToBanID string, isTemporary bool, opts *twitchclient.ReqOpts) error {
	path := fmt.Sprintf("/api/v3/channels/%s/users/%s/ban", channelID, userToBanID)
	if isTemporary {
		path = fmt.Sprintf("%s?temporary=true", path)
	}

	params := BanUserV3Params{
		UserID: requesterUserID,
	}
	body, err := json.Marshal(params)
	if err != nil {
		return err
	}

	req, err := c.NewRequest("POST", path, bytes.NewBuffer(body))
	if err != nil {
		return err
	}

	combinedOpts := twitchclient.MergeReqOpts(opts, twitchclient.ReqOpts{
		StatName:       "service.clips.ban_user_v3",
		StatSampleRate: defaultStatSampleRate,
	})
	resp, err := c.Do(ctx, req, combinedOpts)
	if err != nil {
		return err
	}

	if resp.StatusCode >= 400 {
		return twitchclient.HandleFailedResponse(resp)
	}
	return nil
}

type EditClipTitleV3Params struct {
	UserID string `json:"user_id"`
	Title  string `json:"title"`
}

func (c *client) EditClipTitleV3(ctx context.Context, userID, slug, title string, opts *twitchclient.ReqOpts) (*Clip, error) {
	path := fmt.Sprintf("/api/v3/clips/%s/title", slug)

	params := EditClipTitleV3Params{
		UserID: userID,
		Title:  title,
	}

	body, err := json.Marshal(params)
	if err != nil {
		return nil, err
	}

	req, err := c.NewRequest("POST", path, bytes.NewBuffer(body))
	if err != nil {
		return nil, err
	}

	clip := Clip{}
	combinedOpts := twitchclient.MergeReqOpts(opts, twitchclient.ReqOpts{
		StatName:       "service.clips.edit_clip_title_v3",
		StatSampleRate: defaultStatSampleRate,
	})
	_, err = c.DoJSON(ctx, &clip, req, combinedOpts)
	if err != nil {
		return nil, err
	}
	return &clip, nil
}

func (c *client) GetClipInfoV3(ctx context.Context, slug string, params *GetClipInfoParams, opts *twitchclient.ReqOpts) (*Clip, error) {
	if params == nil {
		params = NewGetClipInfoParams()
	}

	req, err := c.NewRequest("GET", fmt.Sprintf("/api/v3/clips/%s?%s", slug, params.URLParamsString()), nil)
	if err != nil {
		return nil, err
	}

	clip := Clip{}
	combinedOpts := twitchclient.MergeReqOpts(opts, twitchclient.ReqOpts{
		StatName:       "service.clips.get_clip_info_v3",
		StatSampleRate: defaultStatSampleRate,
	})
	resp, err := c.DoJSON(ctx, &clip, req, combinedOpts)
	if err != nil {
		if resp != nil {
			return nil, constructSingleClipTwitchError(resp)
		}
		return nil, err
	}
	return &clip, nil
}

func (c *client) BatchGetClipInfoV3(ctx context.Context, slugs []string, opts *twitchclient.ReqOpts) ([]*Clip, error) {

	req, err := c.NewRequest("GET", fmt.Sprintf("/api/v3/clips?slug=%s", strings.Join(slugs, ",")), nil)
	if err != nil {
		return nil, err
	}

	clips := []*Clip{}
	combinedOpts := twitchclient.MergeReqOpts(opts, twitchclient.ReqOpts{
		StatName:       "service.clips.batch_get_clip_info_v3",
		StatSampleRate: defaultStatSampleRate,
	})
	resp, err := c.DoJSON(ctx, &clips, req, combinedOpts)
	if err != nil {
		if resp != nil {
			return nil, constructSingleClipTwitchError(resp)
		}
		return nil, err
	}
	return clips, nil
}

func (c *client) GetTopClipsV3(ctx context.Context, params *GetTopClipsParams, opts *twitchclient.ReqOpts) (*PaginatedClipsResponse, error) {
	if params == nil {
		params = NewGetTopClipsParams()
	}

	req, err := c.NewRequest("GET", fmt.Sprintf("/api/v3/clips/top?%s", params.URLParamsString()), nil)
	if err != nil {
		return nil, err
	}

	topClipsResponse := PaginatedClipsResponse{}
	combinedOpts := twitchclient.MergeReqOpts(opts, twitchclient.ReqOpts{
		StatName:       "service.clips.get_top_clips_v3",
		StatSampleRate: defaultStatSampleRate,
	})
	resp, err := c.DoJSON(ctx, &topClipsResponse, req, combinedOpts)
	if err != nil {
		if resp != nil && resp.StatusCode == http.StatusNotFound {
			return nil, &twitchclient.Error{StatusCode: http.StatusNotFound, Message: "no clips were found"}
		}
		return nil, err
	}
	return &topClipsResponse, nil
}

func (c *client) GetMyClipsV3(ctx context.Context, params *GetMyClipsParams, opts *twitchclient.ReqOpts) (*PaginatedClipsResponse, error) {
	if params == nil {
		params = NewGetMyClipsParams()
	}

	req, err := c.NewRequest("GET", fmt.Sprintf("/api/v3/clips/me?%s", params.URLParamsString()), nil)
	if err != nil {
		return nil, err
	}

	myClipsResponse := PaginatedClipsResponse{}
	combinedOpts := twitchclient.MergeReqOpts(opts, twitchclient.ReqOpts{
		StatName:       "service.clips.get_my_clips_v3",
		StatSampleRate: defaultStatSampleRate,
	})
	resp, err := c.DoJSON(ctx, &myClipsResponse, req, combinedOpts)
	if err != nil {
		if resp != nil && resp.StatusCode == http.StatusNotFound {
			return nil, &twitchclient.Error{StatusCode: http.StatusNotFound, Message: "no clips were found"}
		}
		return nil, err
	}
	return &myClipsResponse, nil
}

func (c *client) GetRawClipStatusV3(ctx context.Context, slug string, opts *twitchclient.ReqOpts) (*RawStatus, error) {
	req, err := c.NewRequest("GET", fmt.Sprintf("/api/v3/clips/%s/raw/status", slug), nil)
	if err != nil {
		return nil, err
	}
	rawStatus := RawStatus{}
	combinedOpts := twitchclient.MergeReqOpts(opts, twitchclient.ReqOpts{
		StatName:       "service.clips.get_raw_clip_status_v3",
		StatSampleRate: defaultStatSampleRate,
	})
	resp, err := c.DoJSON(ctx, &rawStatus, req, combinedOpts)
	if err != nil {
		if resp != nil {
			return nil, constructSingleClipTwitchError(resp)
		}
		return nil, err
	}

	return &rawStatus, nil
}

func (c *client) GetClipStatusV3(ctx context.Context, slug string, opts *twitchclient.ReqOpts) (*Status, error) {
	req, err := c.NewRequest("GET", fmt.Sprintf("/api/v3/clips/%s/status", slug), nil)
	if err != nil {
		return nil, err
	}
	status := Status{}
	combinedOpts := twitchclient.MergeReqOpts(opts, twitchclient.ReqOpts{
		StatName:       "service.clips.get_clip_status_v3",
		StatSampleRate: defaultStatSampleRate,
	})
	resp, err := c.DoJSON(ctx, &status, req, combinedOpts)
	if err != nil {
		if resp != nil {
			return nil, constructSingleClipTwitchError(resp)
		}
		return nil, err
	}

	return &status, nil
}

type PublishClipV3Params struct {
	UserID        string       `json:"user_id"`
	Segments      SegmentInfos `json:"segments"`
	Title         *string      `json:"title"`
	Slug          string       `json:"-"`
	PreviewOffset *float64     `json:"preview_offset"`
}

func (c *client) PublishClipV3(ctx context.Context, params *PublishClipV3Params, opts *twitchclient.ReqOpts) (*Clip, error) {
	body, err := json.Marshal(params)
	if err != nil {
		return nil, err
	}

	path := fmt.Sprintf("/api/v3/clips/%s/publish", params.Slug)

	req, err := c.NewRequest("POST", path, bytes.NewBuffer(body))
	if err != nil {
		return nil, err
	}

	clip := Clip{}
	combinedOpts := twitchclient.MergeReqOpts(opts, twitchclient.ReqOpts{
		StatName:       "service.clips.publish_clip_v3",
		StatSampleRate: defaultStatSampleRate,
	})

	_, err = c.DoJSON(ctx, &clip, req, combinedOpts)
	if err != nil {
		return nil, err
	}
	return &clip, nil
}

func constructSingleClipTwitchError(resp *http.Response) error {
	if resp.StatusCode == http.StatusForbidden {
		return &twitchclient.Error{StatusCode: http.StatusForbidden, Message: "this channel has been closed due to terms of service violation"}
	} else if resp.StatusCode == http.StatusNotFound {
		return &twitchclient.Error{StatusCode: http.StatusNotFound, Message: "clip not found"}
	}

	return twitchclient.HandleFailedResponse(resp)
}

func (c *client) RelatedClipsSetsV3(ctx context.Context, slug string, params *RelatedClipsSetsParams, opts *twitchclient.ReqOpts) (*RelatedClipsSets, error) {

	req, err := c.NewRequest("GET", fmt.Sprintf("/api/v3/clips/%s/related-clips-sets?sets=%s", slug, strings.Join(params.Sets, ",")), nil)
	if err != nil {
		return nil, err
	}

	relatedClipSetsResponse := RelatedClipsSets{}
	combinedOpts := twitchclient.MergeReqOpts(opts, twitchclient.ReqOpts{
		StatName:       "service.clips.related_clips_sets_v3",
		StatSampleRate: defaultStatSampleRate,
	})
	resp, err := c.DoJSON(ctx, &relatedClipSetsResponse, req, combinedOpts)
	if err != nil {
		if resp != nil && resp.StatusCode == http.StatusNotFound {
			return nil, &twitchclient.Error{StatusCode: http.StatusNotFound, Message: "clip not found"}
		}
		return nil, err
	}
	return &relatedClipSetsResponse, nil
}

func (c *client) ViewClipV3(ctx context.Context, slug, clientIP string, opts *twitchclient.ReqOpts) (*Clip, error) {

	req, err := c.NewRequest("GET", fmt.Sprintf("/api/v3/clips/%s/view", slug), nil)
	if err != nil {
		return nil, err
	}

	req.Header.Set("X-Forwarded-For", clientIP)

	clip := Clip{}
	combinedOpts := twitchclient.MergeReqOpts(opts, twitchclient.ReqOpts{
		StatName:       "service.clips.view_clip_v3",
		StatSampleRate: defaultStatSampleRate,
	})
	resp, err := c.DoJSON(ctx, &clip, req, combinedOpts)
	if err != nil {
		if resp != nil {
			return nil, constructSingleClipTwitchError(resp)
		}
		return nil, err
	}
	return &clip, nil
}

func (c *client) CreateLiveClipV3(ctx context.Context, params *CreateLiveClipParams, opts *twitchclient.ReqOpts) (*Clip, error) {
	body, err := json.Marshal(params)
	if err != nil {
		return nil, err
	}

	req, err := c.NewRequest("POST", "/api/v3/liveclips", bytes.NewBuffer(body))
	if err != nil {
		return nil, err
	}

	combinedOpts := twitchclient.MergeReqOpts(opts, twitchclient.ReqOpts{
		StatName:       "service.clips.create_live_clip_v3",
		StatSampleRate: defaultStatSampleRate,
	})

	clip := Clip{}
	_, err = c.DoJSON(ctx, &clip, req, combinedOpts)
	if err != nil {
		return nil, err
	}
	return &clip, nil
}

func (c *client) GetClipMetadataV3(ctx context.Context, slug string, opts *twitchclient.ReqOpts) (*ClipMetadata, error) {
	req, err := c.NewRequest("GET", fmt.Sprintf("/api/v3/clips/%s/metadata", slug), nil)
	if err != nil {
		return nil, err
	}
	clipMetadata := ClipMetadata{}
	combinedOpts := twitchclient.MergeReqOpts(opts, twitchclient.ReqOpts{
		StatName:       "service.clips.get_clip_metadata_v3",
		StatSampleRate: defaultStatSampleRate,
	})
	_, err = c.DoJSON(ctx, &clipMetadata, req, combinedOpts)
	if err != nil {
		return nil, err
	}
	return &clipMetadata, nil
}

func (c *client) GetClipsByVODIDV3(ctx context.Context, params *GetClipsByVODIDParams, opts *twitchclient.ReqOpts) (*PaginatedClipsResponse, error) {
	if params == nil || params.VODID == "" {
		return nil, errors.New("BroadcastID missing from GetClipsByBroadcastIDParams")
	}

	path := fmt.Sprintf("/api/v3/vods/%s/clips", url.QueryEscape(params.VODID))

	queryParams := url.Values{}
	if params.Limit != nil {
		queryParams.Add("limit", fmt.Sprintf("%d", *params.Limit))
	}
	if params.Sort != nil {
		queryParams.Add("sort", *params.Sort)
	}
	if params.Cursor != nil {
		queryParams.Add("cursor", *params.Cursor)
	}
	if params.CuratorIDs != nil {
		queryParams.Add("curator_ids", strings.Join(params.CuratorIDs, ","))
	}

	if len(queryParams) > 0 {
		path = fmt.Sprintf("%s?%s", path, queryParams.Encode())
	}

	req, err := c.NewRequest("GET", path, nil)
	if err != nil {
		return nil, err
	}

	req.URL.RawQuery = queryParams.Encode()

	response := PaginatedClipsResponse{}
	combinedOpts := twitchclient.MergeReqOpts(opts, twitchclient.ReqOpts{
		StatName:       "service.clips.get_clips_by_vod_id_v3",
		StatSampleRate: defaultStatSampleRate,
	})
	_, err = c.DoJSON(ctx, &response, req, combinedOpts)
	if err != nil {
		return nil, err
	}
	return &response, nil
}

func (c *client) GetClipCountByBroadcastIDV3(ctx context.Context, broadcastID string, opts *twitchclient.ReqOpts) (*ClipCount, error) {
	req, err := c.NewRequest("GET", fmt.Sprintf("/api/v3/broadcasts/%s/count", broadcastID), nil)
	if err != nil {
		return nil, err
	}

	clipCount := ClipCount{}
	combinedOpts := twitchclient.MergeReqOpts(opts, twitchclient.ReqOpts{
		StatName:       "service.clips.get_broadcast_clip_count_v3",
		StatSampleRate: defaultStatSampleRate,
	})
	_, err = c.DoJSON(ctx, &clipCount, req, combinedOpts)
	if err != nil {
		return nil, err
	}
	return &clipCount, nil
}

func (c *client) GetClipsByTimeOffset(ctx context.Context, params *GetClipsByTimeOffsetParams, opts *twitchclient.ReqOpts) (*PaginatedClipsResponse, error) {
	if params == nil || params.BroadcastID == "" || params.Start == "" || params.End == "" {
		return nil, errors.New("Missing required parameter")
	}

	queryParams := url.Values{}
	queryParams.Add("start", params.Start)
	queryParams.Add("end", params.End)
	if params.Limit != nil {
		queryParams.Add("limit", fmt.Sprintf("%s", *params.Limit))
	}
	if params.Cursor != nil {
		queryParams.Add("cursor", *params.Cursor)
	}

	path := fmt.Sprintf("/api/v3/broadcasts/%s/clips?%s", url.QueryEscape(params.BroadcastID), queryParams.Encode())
	req, err := c.NewRequest("GET", path, nil)
	if err != nil {
		return nil, err
	}

	response := PaginatedClipsResponse{}
	combinedOpts := twitchclient.MergeReqOpts(opts, twitchclient.ReqOpts{
		StatName:       "service.clips.get_clips_by_timeoffset",
		StatSampleRate: defaultStatSampleRate,
	})
	_, err = c.DoJSON(ctx, &response, req, combinedOpts)
	if err != nil {
		return nil, err
	}
	return &response, nil
}

func (c *client) GetChannelConfig(ctx context.Context, channelID string, opts *twitchclient.ReqOpts) (*ChannelConfig, error) {
	req, err := c.NewRequest("GET", fmt.Sprintf("/api/v3/channel_config/%s", channelID), nil)
	if err != nil {
		return nil, err
	}

	combinedOpts := twitchclient.MergeReqOpts(opts, twitchclient.ReqOpts{
		StatName:       "service.clips.get_channel_config",
		StatSampleRate: defaultStatSampleRate,
	})
	var resp ChannelConfig
	_, err = c.DoJSON(ctx, &resp, req, combinedOpts)
	if err != nil {
		return nil, err
	}
	return &resp, nil
}

type UpdateChannelConfigParams struct {
	*ChannelConfig
	UserID string `json:"user_id"`
}

func (c *client) UpdateChannelConfig(ctx context.Context, params *UpdateChannelConfigParams, opts *twitchclient.ReqOpts) (*ChannelConfig, error) {
	body, err := json.Marshal(params)
	if err != nil {
		return nil, err
	}

	req, err := c.NewRequest("PUT", fmt.Sprintf("/api/v3/channel_config"), bytes.NewBuffer(body))
	if err != nil {
		return nil, err
	}

	combinedOpts := twitchclient.MergeReqOpts(opts, twitchclient.ReqOpts{
		StatName:       "service.clips.update_channel_config",
		StatSampleRate: defaultStatSampleRate,
	})
	channelConfig := &ChannelConfig{}
	_, err = c.DoJSON(ctx, channelConfig, req, combinedOpts)
	if err != nil {
		return nil, err
	}
	return channelConfig, nil
}
