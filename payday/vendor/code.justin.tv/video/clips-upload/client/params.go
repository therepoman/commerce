package clips

import (
	"fmt"
	"net/url"
	"strconv"
	"time"
)

// CreateClipV3Params includes the inputs necessary to create a clip. Either BroadcastID
// or VodID must be specified. If BroadcastID and VodID are both specified, BroadcastID
// will be preferred.
type CreateClipV3Params struct {
	UserID               string  `json:"user_id"`
	ChannelID            string  `json:"channel_id"`
	BroadcastID          *string `json:"broadcast_id,omitempty"`
	VodID                *string `json:"vod_id,omitempty"`
	RequesterCountryCode *string `json:"requester_country_code,omitempty"`
	Offset               float64 `json:"offset"`
}

// CreateLiveClipParams is used to create a clip of the channel's broadcast for
// the user.
// DelaySeconds parameter is used to set a custom delay for clipping, and can be set to 0 for realtime clipping.
// Default behavior sets DelaySeconds to 8 seconds if the parameter is not included.
// This is in order to estimate the delay that a viewer normally sees between stream time and clipping time.
type CreateLiveClipParams struct {
	UserID               string   `json:"user_id"`
	ChannelID            string   `json:"channel_id"`
	DelaySeconds         *float64 `json:"delay_seconds,omitempty"`
	RequesterCountryCode *string  `json:"requester_country_code,omitempty"`
}

// GetClipInfoParams translates the request parameters to a URL query string
type GetClipInfoParams struct {
	// Note that WithURLTracking should only be true if the clip URLs can be retrieved from
	// the WithURLTracking API so that we can track the URL usage externally.
	WithURLTracking bool
}

type RelatedClipsSetsParams struct {
	Sets []string `json:"sets"`
}

const (
	GameClipSet    = "game"
	SimilarClipSet = "similar"
	TopClipSet     = "top"
	ChannelClipSet = "channel"
	CuratorClipSet = "curator"
)

// NewGetClipInfoParams returns a GetClipInfoParams with default values.
// This should be used to initialize GetClipInfoParams.
func NewGetClipInfoParams() *GetClipInfoParams {
	return &GetClipInfoParams{
		WithURLTracking: false,
	}
}

const (
	ViewsAscSort      = "views_asc"
	ViewsDescSort     = "views_desc"
	CreatedAtAscSort  = "created_at_asc"
	CreatedAtDescSort = "created_at_desc"
)

// URLParamsString returns the url parameters string with the values in p.
func (p *GetClipInfoParams) URLParamsString() string {
	return fmt.Sprintf("with_url_tracking=%t", p.WithURLTracking)
}

// GetTopClipsParams translates the request parameters to a URL query string.
type GetTopClipsParams struct {
	ChannelIDs *[]string
	GameNames  *[]string
	VodIDs     *[]string
	Languages  *[]string
	Start      *time.Time
	End        *time.Time
	Trending   bool
	Limit      int64
	Cursor     string

	// Sort should be set to the sort constants defined in this file.
	Sort string

	// Note that WithURLTracking should only be true if the clip URLs can be retrieved from
	// the WithURLTracking API so that we can track the URL usage externally.
	WithURLTracking bool
}

// NewGetTopClipsParams returns a GetTopClipsParams with default values.
// This should be used to initialize GetTopClipsParams.
func NewGetTopClipsParams() *GetTopClipsParams {
	return &GetTopClipsParams{
		Trending:        false,
		Limit:           100,
		WithURLTracking: false,
		Sort:            ViewsDescSort,
	}
}

// URLParamsString returns the url parameters string with the values in p.
func (p *GetTopClipsParams) URLParamsString() string {
	urlParams := url.Values{}
	urlParams.Add("with_url_tracking", strconv.FormatBool(p.WithURLTracking))
	urlParams.Add("trending", strconv.FormatBool(p.Trending))
	urlParams.Add("limit", strconv.FormatInt(p.Limit, 10))
	urlParams.Add("sort", p.Sort)

	if p.Start != nil {
		urlParams.Add("start", p.Start.Format(time.RFC3339))
	}

	if p.End != nil {
		urlParams.Add("end", p.End.Format(time.RFC3339))
	}

	if p.Cursor != "" {
		urlParams.Add("cursor", p.Cursor)
	}

	if p.ChannelIDs != nil {
		for _, channelID := range *p.ChannelIDs {
			urlParams.Add("channel_id", channelID)
		}
	}

	if p.GameNames != nil {
		for _, game := range *p.GameNames {
			urlParams.Add("game", game)
		}
	}

	if p.VodIDs != nil {
		for _, vodID := range *p.VodIDs {
			urlParams.Add("vod_id", vodID)
		}
	}

	if p.Languages != nil {
		for _, language := range *p.Languages {
			urlParams.Add("language", language)
		}
	}

	return urlParams.Encode()
}

type GetMyClipsParams struct {
	UserID    string
	Role      string
	Game      string
	ChannelID string
	Sort      string
	Cursor    string
	Limit     int
}

const (
	RoleCurator     = "curator"
	RoleBroadcaster = "broadcaster"

	SortNewest     = "newest"
	SortOldest     = "oldest"
	SortMostViews  = "most_views"
	SortLeastViews = "least_views"
)

func NewGetMyClipsParams() *GetMyClipsParams {
	return &GetMyClipsParams{
		Role:  RoleCurator,
		Limit: 100,
		Sort:  SortNewest,
	}
}

func (p *GetMyClipsParams) URLParamsString() string {
	urlParams := url.Values{}
	urlParams.Add("user_id", p.UserID)
	urlParams.Add("role", p.Role)
	urlParams.Add("limit", strconv.FormatInt(int64(p.Limit), 10))
	urlParams.Add("sort", p.Sort)

	if p.Game != "" {
		urlParams.Add("game", p.Game)
	}

	if p.ChannelID != "" {
		urlParams.Add("channel_id", p.ChannelID)
	}

	if p.Cursor != "" {
		urlParams.Add("cursor", p.Cursor)
	}

	return urlParams.Encode()
}

type GetClipsByVODIDParams struct {
	CuratorIDs []string
	VODID      string
	Cursor     *string
	Limit      *int
	Sort       *string
}

type GetClipsByTimeOffsetParams struct {
	BroadcastID string
	Start       string
	End         string
	Cursor      *string
	Limit       *string
}
