package clips

const (
	// CreateClipV3 error codes
	ChannelNotClippable = "channel_not_clippable"
	ChannelBanned       = "channel_banned"
	UserBanned          = "user_banned"
	UserTimedOut        = "user_timed_out"
	ChannelNotLive      = "channel_not_live"

	// PublishClipV3 error codes
	AlreadyPublished     = "already_published"
	EditingWindowExpired = "editing_window_expired"
	InvalidSpeedDuration = "invalid_speed_duration"
	InvalidDuration      = "invalid_duration"
	InvalidTitle         = "invalid_title"

	// Generic error codes
	RequestThrottled = "request_throttled"
)
