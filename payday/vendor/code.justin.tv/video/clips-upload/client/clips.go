package clips

import (
	"golang.org/x/net/context"

	"code.justin.tv/foundation/twitchclient"
)

const (
	defaultStatSampleRate = 0.1
	defaultTimingXactName = "clips"
)

// Client is the interface to interact with the Clips APIs.
type Client interface {
	// CreateClipV3 is an internal endpoint to create a Clip on a stream or video.
	// If both BroadcastID and VodID are set, BroadcastID will be preferred.
	CreateClipV3(context.Context, *CreateClipV3Params, *twitchclient.ReqOpts) (*Clip, error)

	// DeleteClipBySlugV3 is an internal endpoint that allows a user to delete a
	// Clip given its slug. Note that only some users are allowed to delete a Clip.
	DeleteClipBySlugV3(context.Context, string, string, *twitchclient.ReqOpts) error

	// BatchDeleteClipsV3 is an internal endpoint to delete all clips based on a
	// list of slugs. Note that a list of DeletedClips is returned which only contains
	// the slug and id of a Clip.
	BatchDeleteClipsV3(ctx context.Context, userID string, slugs []string, opts *twitchclient.ReqOpts) (DeletedClips, error)

	// DeleteClipsByVODV3 is an internal endpoint to delete all clips from a vod.
	// Note that a list of DeletedClips is returned which only contains the slug
	// and id of a Clip.
	DeleteClipsByVODV3(ctx context.Context, userID, vodID string, opts *twitchclient.ReqOpts) (DeletedClips, error)

	// DeleteClipsByBroadcastV3 is an internal endpoint to delete all clips from
	// a broadcast. Note that a list of DeletedClips is returned which only contains
	// the slug and id of a Clip.
	DeleteClipsByBroadcastV3(ctx context.Context, userID, broadcastID string, opts *twitchclient.ReqOpts) (DeletedClips, error)

	// GetTopClipsV3 is an internal endpoint to fetch the top clips. This endpoint
	// can be filtered, sorted, and paginated with the fields in GetTopClipsParams.
	GetTopClipsV3(ctx context.Context, params *GetTopClipsParams, opts *twitchclient.ReqOpts) (*PaginatedClipsResponse, error)

	BanUserV3(ctx context.Context, channelID, requesterUserID, userToBanID string, isTemporary bool, opts *twitchclient.ReqOpts) error
	EditClipTitleV3(ctx context.Context, slug, userID, title string, opts *twitchclient.ReqOpts) (*Clip, error)
	GetClipInfoV3(ctx context.Context, slug string, params *GetClipInfoParams, opts *twitchclient.ReqOpts) (*Clip, error)
	GetMyClipsV3(ctx context.Context, params *GetMyClipsParams, opts *twitchclient.ReqOpts) (*PaginatedClipsResponse, error)
	GetRawClipStatusV3(ctx context.Context, slug string, opts *twitchclient.ReqOpts) (*RawStatus, error)
	RelatedClipsSetsV3(ctx context.Context, slug string, params *RelatedClipsSetsParams, opts *twitchclient.ReqOpts) (*RelatedClipsSets, error)
	GetClipStatusV3(ctx context.Context, slug string, opts *twitchclient.ReqOpts) (*Status, error)
	ViewClipV3(ctx context.Context, slug, clientIP string, opts *twitchclient.ReqOpts) (*Clip, error)
	BatchGetClipInfoV3(ctx context.Context, slugs []string, opts *twitchclient.ReqOpts) ([]*Clip, error)
	CreateLiveClipV3(ctx context.Context, params *CreateLiveClipParams, opts *twitchclient.ReqOpts) (*Clip, error)
	GetClipMetadataV3(ctx context.Context, slug string, opts *twitchclient.ReqOpts) (*ClipMetadata, error)
	UnauthedDeleteClipsMediaV3(ctx context.Context, items []DeleteClipMediaItem, opts *twitchclient.ReqOpts) error

	// PublishClipV3 is an internal endpoint to edit a clip via segments.
	PublishClipV3(ctx context.Context, params *PublishClipV3Params, opts *twitchclient.ReqOpts) (*Clip, error)

	// CreateLiveClipV2 is an internal endpoint to create a clip for a user from a channel's broadcast.
	CreateLiveClipV2(ctx context.Context, params *CreateLiveClipParams, opts *twitchclient.ReqOpts) (*Clip, error)

	// UnauthedBatchDeleteClipsV3 is the unauthed version of BatchDeleteClipsV3.
	// This function is intended for internal use only, e.g. for DMCA'd clips.
	UnauthedBatchDeleteClipsV3(ctx context.Context, slugs []string, opts *twitchclient.ReqOpts) (DeletedClips, error)

	// UnauthedDeleteClipsByVODV3 is the unauthed version of DeleteClipsByVODV3.
	// This function is intended for internal use only, e.g. for clips on a DMCA'd VODs.
	UnauthedDeleteClipsByVODV3(ctx context.Context, vodID string, opts *twitchclient.ReqOpts) (DeletedClips, error)

	// UnauthedDeleteClipsByBroadcastV3 is the unauthed version of DeleteClipsByBroadcastV3.
	// This function is intended for internal use only, e.g. for clips on a DMCA'd broadcast.
	UnauthedDeleteClipsByBroadcastV3(ctx context.Context, broadcastID string, opts *twitchclient.ReqOpts) (DeletedClips, error)

	// Fetch clips given a vod ID
	GetClipsByVODIDV3(ctx context.Context, params *GetClipsByVODIDParams, opts *twitchclient.ReqOpts) (*PaginatedClipsResponse, error)

	// GetClipCountByBroadcastIDV3 returns number of clips made during a broadcast.
	GetClipCountByBroadcastIDV3(ctx context.Context, broadcastID string, opts *twitchclient.ReqOpts) (*ClipCount, error)

	// GetClipsByTimeOffset fetches clips based on a time offset
	GetClipsByTimeOffset(ctx context.Context, params *GetClipsByTimeOffsetParams, opts *twitchclient.ReqOpts) (*PaginatedClipsResponse, error)

	// GetChannelConfig gets clip configuration settings for a channel.
	GetChannelConfig(ctx context.Context, channelID string, opts *twitchclient.ReqOpts) (*ChannelConfig, error)

	// UpdateChannelConfig updates clip configuration settings for a channel.
	UpdateChannelConfig(ctx context.Context, params *UpdateChannelConfigParams, opts *twitchclient.ReqOpts) (*ChannelConfig, error)
}

type client struct {
	twitchclient.Client
}

// NewClient takes a twitchclient.ClientConf and returns a client that implements
// the Clips Client interface.
func NewClient(conf twitchclient.ClientConf) (Client, error) {
	if conf.TimingXactName == "" {
		conf.TimingXactName = defaultTimingXactName
	}
	twitchClient, err := twitchclient.NewClient(conf)
	return &client{twitchClient}, err
}
