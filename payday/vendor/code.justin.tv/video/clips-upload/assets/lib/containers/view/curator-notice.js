/* globals window: false */
import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import { AcknowledgementActions } from 'lib/actions/view/acknowledgement';
import { Balloon } from 'lib/components/common/balloon';
import { BalloonWrapper } from 'lib/components/common/balloon-wrapper';
import { CloseX } from 'lib/components/common/close-x';
import { DEFAULT_TRANSLATE as t } from 'lib/lang/i18n';
import { isInIframe } from 'lib/util/DomHelpers';

require('./curator-notice.sass');

export class CuratorNoticeComponent extends Component {
  componentDidMount() {
    if (this.props.user.loggedIn) {
      this.props.checkAcknowledgement(this.props.user);
    }

  }

  componentWillReceiveProps(nextProps) {
    if (this.props.user.loginName != nextProps.user.loginName) {
      nextProps.checkAcknowledgement(nextProps.user);
    }
  }

  render() {
    const { showNotice, onDismissHandler } = this.props;
    let hiddenClass = showNotice ? 'show' : 'hide';
    let classNames = `legal-acknowledgement ${hiddenClass}`;
    let instructions = 'Clips you create are public and may be shared with the Twitch community. ' +
      'To delete your clip, click on the gear icon in the player.';

    let buttonClasses = [
      'flex__item',
      'flex__item--noShrink',
      'button',
      'button--text',
      'button--icon-only',
      'legal-acknowledgement__close',
    ].join(' ');

    return (
      <div className={classNames}>
        <div className="legal-acknowledgement__inner">
          <BalloonWrapper>
            <Balloon
              direction="up"
              arrow="no-tail"
              additionalClasses="balloon--purple balloon--static balloon--fullWdith"
            >
              <div className="flex flex--nowrap">
                <div className="flex__item flex__item--grow">
                  {t(instructions)}
                </div>
                <button className={buttonClasses} onClick={onDismissHandler}>
                  <figure>
                    <CloseX />
                  </figure>
                </button>
              </div>
            </Balloon>
          </BalloonWrapper>
        </div>
      </div>
    );
  }
}

CuratorNoticeComponent.propTypes = {
  showNotice: PropTypes.bool,
  onDismissHandler: PropTypes.func.isRequired,
  checkAcknowledgement: PropTypes.func.isRequired,
  user: PropTypes.shape({
    loggedIn: PropTypes.bool,
    loginName: PropTypes.string,
  }).isRequired,
};

CuratorNoticeComponent.defaultPropTypes = {
  showNotice: true,
};

const mapStateToProps = ({ curatorAcknowledgement: { showNotice }, user, clip: { curator } }) => ({
  showNotice: !isInIframe(window) && showNotice && (curator.login == user.loginName),
  user,
});

const mapDispatchToProps = (dispatch) => ({
  onDismissHandler(user) {
    dispatch(AcknowledgementActions.setShowNotice(false));
    dispatch(AcknowledgementActions.sendAcknowledgement(user));
  },
  checkAcknowledgement(user) { dispatch(AcknowledgementActions.getAcknowledgement(user)); },
});

const mergeProps = (stateProps, dispatchProps) => {
  return Object.assign({}, stateProps, dispatchProps, {
    onDismissHandler: () => dispatchProps.onDismissHandler(stateProps.user),
    checkAcknowledgement(user) { if (user.loggedIn) { dispatchProps.checkAcknowledgement(user); } },
  });
};

export const CuratorNotice = connect(mapStateToProps, mapDispatchToProps, mergeProps)(CuratorNoticeComponent);
