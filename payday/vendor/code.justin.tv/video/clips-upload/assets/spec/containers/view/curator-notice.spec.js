import React from 'react';
import { shallow, mount } from 'enzyme';

import { CuratorNoticeComponent } from 'lib/containers/view/curator-notice';

describe('containers/view/curator - component', () => {
  let user;
  let onDismissHandlerSpy;
  let checkAcknowledgementSpy;
  let component;
  beforeEach(() => {
    user = { loginName: 'loginName' };
    onDismissHandlerSpy = jasmine.createSpy('onDismissHandler');
    checkAcknowledgementSpy = jasmine.createSpy('checkAcknowledgement');
    component = shallow(<CuratorNoticeComponent
            user={user} onDismissHandler={onDismissHandlerSpy}
            checkAcknowledgement={checkAcknowledgementSpy} />);

  });
  it('should render', () => {
    const message = 'Clips you create are public and may be shared with the Twitch community. ' +
            'To delete your clip, click on the gear icon in the player.';
    expect(component.contains(message)).toEqual(true);
  });

  it('should call the dismiss handler on click of the close button', () => {
    component.find('.legal-acknowledgement__close').simulate('click');
    expect(onDismissHandlerSpy).toHaveBeenCalled();
  });

  describe('when showNotice is true', () => {
    beforeEach(() => {
      component = shallow(
        <CuratorNoticeComponent user={user} onDismissHandler={onDismissHandlerSpy}
        checkAcknowledgement={checkAcknowledgementSpy} showNotice={true} clipJustCreated={true}/>
      );
    });

    it('should show the balloon', () => {
      expect(component.hasClass('hide')).toEqual(false);
      expect(component.hasClass('show')).toEqual(true);
    });

  });

  describe('when showNotice is false', () => {
    beforeEach(() => {
      component = shallow(
        <CuratorNoticeComponent user={user} onDismissHandler={onDismissHandlerSpy}
        checkAcknowledgement={checkAcknowledgementSpy} showNotice={false} clipJustCreated={true}/>
      );
    });

    it('should hide the balloon', () => {
      expect(component.hasClass('hide')).toEqual(true);
      expect(component.hasClass('show')).toEqual(false);
    });
  });

  describe('lifecycle hooks', () => {
    describe('when the component mounts', () => {
      describe('when the user is logged in', () => {
        beforeEach(() => {
          user.loggedIn = true;

          component = mount(<CuratorNoticeComponent
                        user={user} onDismissHandler={onDismissHandlerSpy}
                        checkAcknowledgement={checkAcknowledgementSpy} clipJustCreated={true} />);
        });

        it('should call the checkAcknowledgement prop', () => {
          expect(checkAcknowledgementSpy).toHaveBeenCalledWith(user);
        });
      });

      describe('when the user is not logged in', () => {
        beforeEach(() => {
          user.loggedIn = false;

          component = mount(<CuratorNoticeComponent
                        user={user} onDismissHandler={onDismissHandlerSpy}
                        checkAcknowledgement={checkAcknowledgementSpy} clipJustCreated={true} />);
        });
        it('should not call the checkAcknowledgement prop', () => {
          expect(checkAcknowledgementSpy).not.toHaveBeenCalled();
        });
      });
    });

    describe('when the user prop does not change', () => {
      it('should call the checkAcknowledgement prop with the new user', () => {
        const newUser = { loginName: 'loginName' };
        const newCheckAcknowledgementSpy = jasmine.createSpy('checkAcknowledgement');
        component.setProps({ user: newUser, checkAcknowledgement: newCheckAcknowledgementSpy });

        expect(newCheckAcknowledgementSpy).not.toHaveBeenCalled();
      });
    });
  });
});
