job {
  name "video-autoprof"
  using 'TEMPLATE-autobuild'
  wrappers {
    credentialsBinding {
      file('COURIERD_PRIVATE_KEY', 'courierd')
      file('AWS_CONFIG_FILE', 'aws_config')
      string 'AWS_ACCESS_KEY_ID', 'core-deploy-artifacts-access-key'
      string 'AWS_SECRET_ACCESS_KEY', 'core-deploy-artifacts-secret-key'
    }
  }
  scm {
    git {
      remote {
        github 'video/autoprof', 'ssh', 'git-aws.internal.justin.tv'
        credentials 'git-aws-read-key'
      }
      clean true
    }
  }
  steps {
    shell """
bingo lint --container
bingo test --container
"""
  }
}
