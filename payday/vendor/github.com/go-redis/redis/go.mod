module github.com/go-redis/redis

require github.com/go-redis/redis/v7 v7.0.0-beta.4

replace github.com/go-redis/redis/v7 => ./v7
