package main

import (
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"time"

	util "code.justin.tv/commerce/payday/utils/close"
)

func fetchHealth() error {
	fmt.Println("Pinging Health Check")
	resp, err := http.Get("http://localhost:8000/health-check")
	if err != nil {
		return err
	}
	defer util.Close(resp.Body)
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	if string(body) != "OK" {
		return errors.New("Bad response")
	}
	return nil
}

func main() {
	for i := 0; i < 20; i++ {
		err := fetchHealth()
		if err == nil {
			fmt.Println("Success!")
			os.Exit(0)
		}
		fmt.Println(err)
		time.Sleep(time.Second)
	}
	os.Exit(1)
}
