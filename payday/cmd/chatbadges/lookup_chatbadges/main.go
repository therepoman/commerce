package main

import (
	"context"

	"code.justin.tv/commerce/payday/clients/chatbadges"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/foundation/twitchclient"
)

const (
	environment = "staging"
)

func main() {
	err := config.Load(environment)
	if err != nil {
		panic(err)
	}

	cfg := config.Get()
	if err := cfg.Validate(); err != nil {
		panic(err)
	}

	channelIDToLookUp := "119568860"
	userIDToLookUp := "127380717"

	chatBadgeCfg := twitchclient.ClientConf{
		Host: cfg.ChatBadgesHostAddress,
	}
	chatBadgesClient, err := chatbadges.NewChatBadgesClientWithMetrics(chatBadgeCfg)
	if err != nil {
		panic(err)
	}
	chatBadgeResp, err := chatBadgesClient.AvailableChannelBadges(context.Background(), userIDToLookUp, channelIDToLookUp, nil)
	if err != nil {
		panic(err)
	}
	for _, badge := range chatBadgeResp.AvailableBadges {
		println(badge.BadgeSetID)
		println(badge.BadgeSetVersion)
	}
}
