package main

import (
	"bytes"
	"context"
	"encoding/csv"
	"fmt"
	"io/ioutil"
	"path/filepath"

	chatbadge_models "code.justin.tv/chat/badges/app/models"
	"code.justin.tv/commerce/payday/audit"
	"code.justin.tv/commerce/payday/clients/chatbadges"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/dynamo/leaderboard_badge_holders"
	"code.justin.tv/commerce/payday/iam"
	"code.justin.tv/commerce/payday/lambda"
	"code.justin.tv/commerce/payday/s3"
	"code.justin.tv/foundation/twitchclient"
)

const (
	environment = "staging"
)

func main() {
	err := config.Load(environment)
	if err != nil {
		panic(err)
	}

	cfg := config.Get()
	if err := cfg.Validate(); err != nil {
		panic(err)
	}

	s3Client := s3.NewFromDefaultConfig()
	lambdaClient := lambda.NewFromDefaultConfig()
	iamClient := iam.NewFromDefaultConfig()
	auditSetup := audit.NewAuditSetup(s3Client, lambdaClient, iamClient)

	dynamoClient := dynamo.NewClientWithAudit(&dynamo.DynamoClientConfig{
		AwsRegion:   cfg.DynamoRegion,
		TablePrefix: cfg.DynamoTablePrefix,
	}, auditSetup, &dynamo.DynamoAuditConfig{
		RecordsS3Bucket:      cfg.AuditRecordsS3Bucket,
		LambdaMemorySizeMB:   cfg.AuditLambdaMemorySizeMB,
		LambdaTimeoutSec:     cfg.AuditLambdaTimeoutSec,
		LambdaRoleName:       cfg.AuditLambdaRoleName,
		LambdaCodeS3Bucket:   cfg.AuditLambdaCodeS3Bucket,
		LambdaEventBatchSize: cfg.AuditLambdaEventBatchSize,
	})
	leaderboardBadgeHolderDAO := leaderboard_badge_holders.NewLeaderboardBadgesDao(dynamoClient)

	inFile, err := filepath.Abs("cmd/chatbadges/backfill_leaderboard_chatbadges/channels_with_alltime_leaderboards.csv")
	if err != nil {
		panic(err)
	}

	fileBytes, err := ioutil.ReadFile(inFile)
	if err != nil {
		panic(err)
	}

	fileBytesReader := bytes.NewReader(fileBytes)
	csvReader := csv.NewReader(fileBytesReader)

	rows, err := csvReader.ReadAll()
	if err != nil {
		panic(err)
	}

	chatBadgeCfg := twitchclient.ClientConf{
		Host: cfg.ChatBadgesHostAddress,
	}
	chatBadgesClient, err := chatbadges.NewChatBadgesClientWithMetrics(chatBadgeCfg)
	if err != nil {
		panic(err)
	}

	updated := 0

	totalRows := len(rows)
	for rowNum, row := range rows {
		fmt.Printf("%d / %d\n", rowNum+1, totalRows)
		tuid := row[0]

		badgeHolders, err := leaderboardBadgeHolderDAO.Get(tuid)
		if err != nil {
			panic(err)
		}

		if badgeHolders == nil {
			continue
		}

		entitleLeaderboardBadge(chatBadgesClient, badgeHolders.FirstPlaceUserID, tuid, "1")
		updated++
		entitleLeaderboardBadge(chatBadgesClient, badgeHolders.SecondPlaceUserID, tuid, "2")
		updated++
		entitleLeaderboardBadge(chatBadgesClient, badgeHolders.ThirdPlaceUserID, tuid, "3")
		updated++
	}

	fmt.Printf("Updated: %d badges from %d users \n", updated, totalRows)

}

func entitleLeaderboardBadge(chatBadgesClient chatbadges.IChatBadgesClient, userID string, channelID string, rank string) {
	if userID == "" {
		return
	}

	expirationTimestamp := int64(0)

	err := chatBadgesClient.GrantBitsLeaderBadge(context.Background(), userID, channelID, chatbadge_models.GrantBadgeParams{
		BadgeSetVersion: rank,
		EndTimestamp:    expirationTimestamp,
	}, nil)

	if err != nil {
		panic(err)
	}
}
