package main

import (
	"bytes"
	"encoding/csv"
	"fmt"
	"io/ioutil"
	"path/filepath"

	"code.justin.tv/commerce/payday/audit"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/iam"
	"code.justin.tv/commerce/payday/lambda"
	"code.justin.tv/commerce/payday/s3"
)

const (
	environment = "staging"
)

var newBadgeTiersToDisable = [...]int64{1250000, 1500000, 1750000, 2000000, 2500000, 3000000, 3500000, 4000000, 4500000, 5000000}

func main() {
	err := config.Load(environment)
	if err != nil {
		panic(err)
	}

	cfg := config.Get()
	if err := cfg.Validate(); err != nil {
		panic(err)
	}

	s3Client := s3.NewFromDefaultConfig()
	lambdaClient := lambda.NewFromDefaultConfig()
	iamClient := iam.NewFromDefaultConfig()
	auditSetup := audit.NewAuditSetup(s3Client, lambdaClient, iamClient)

	dynamoClient := dynamo.NewClientWithAudit(&dynamo.DynamoClientConfig{
		AwsRegion:   cfg.DynamoRegion,
		TablePrefix: cfg.DynamoTablePrefix,
	}, auditSetup, &dynamo.DynamoAuditConfig{
		RecordsS3Bucket:      cfg.AuditRecordsS3Bucket,
		LambdaMemorySizeMB:   cfg.AuditLambdaMemorySizeMB,
		LambdaTimeoutSec:     cfg.AuditLambdaTimeoutSec,
		LambdaRoleName:       cfg.AuditLambdaRoleName,
		LambdaCodeS3Bucket:   cfg.AuditLambdaCodeS3Bucket,
		LambdaEventBatchSize: cfg.AuditLambdaEventBatchSize,
	})
	badgeTierDao := dynamo.NewBadgeTierDao(dynamoClient)

	inFile, err := filepath.Abs("cmd/chatbadges/backfill_chatbadges/users_with_custom_cheerbadges.csv")
	if err != nil {
		panic(err)
	}

	fileBytes, err := ioutil.ReadFile(inFile)
	if err != nil {
		panic(err)
	}

	fileBytesReader := bytes.NewReader(fileBytes)
	csvReader := csv.NewReader(fileBytesReader)

	rows, err := csvReader.ReadAll()
	if err != nil {
		panic(err)
	}

	updated := []string{}

	totalRows := len(rows)
	for rowNum, row := range rows {
		fmt.Printf("%d / %d\n", rowNum+1, totalRows)
		tuid := row[0]

		for _, badgeTierThreshold := range newBadgeTiersToDisable {
			badgeTier := dynamo.BadgeTier{
				ChannelId: tuid,
				Enabled:   false,
				Threshold: badgeTierThreshold,
			}

			err := badgeTierDao.Put(&badgeTier)
			if err != nil {
				panic(err)
			}
			updated = append(updated, tuid)
		}
	}

	fmt.Printf("Updated: %d badges from %d users \n", len(updated), len(updated)/len(newBadgeTiersToDisable))

}
