package main

import (
	"encoding/json"
	"fmt"
	"os"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/sqs/crypto"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	awssqs "github.com/aws/aws-sdk-go/service/sqs"
	"github.com/codegangsta/cli"
)

var queueUrl string
var sqsClient *awssqs.SQS
var decrypter *crypto.KMSCipher

const (
	sqsRegion        = "us-west-2"
	kmsRegion        = "us-east-1"
	kmsMasterKey     = "alias/twitchBitsNotificationKey"
	prodPubSubDlqUrl = "https://sqs.us-west-2.amazonaws.com/021561903526/payday_prometheus_queue_deadletter"
)

func main() {
	messageDumper := cli.NewApp()
	messageDumper.Name = "Payday Message Dumper"
	messageDumper.Usage = "Makes it rain (by dumping out decrypted SQS messages)"

	messageDumper.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "queue, q",
			Value:       prodPubSubDlqUrl,
			Usage:       "SQS queue URL. Defaults to prod pub sub DLQ.",
			Destination: &queueUrl,
		},
	}

	sess, _ := session.NewSession()
	sqsClient = awssqs.New(sess, &aws.Config{Region: aws.String(sqsRegion)})
	decrypter = crypto.NewKMSCipher(config.Configuration{
		KMSRegion:       kmsRegion,
		KMSMasterKeyARN: kmsMasterKey,
	})

	messageDumper.Action = dumpMessages
	err := messageDumper.Run(os.Args)
	if err != nil {
		panic(err)
	}
}

func pollMessages() ([]*awssqs.Message, error) {
	messages := make([]*awssqs.Message, 0)

	for {
		params := &awssqs.ReceiveMessageInput{
			QueueUrl: aws.String(queueUrl),
			AttributeNames: []*string{
				aws.String("All"),
			},
			MaxNumberOfMessages: aws.Int64(10),
			MessageAttributeNames: []*string{
				aws.String("All"),
			},
			VisibilityTimeout: aws.Int64(10),
			WaitTimeSeconds:   aws.Int64(1),
		}

		resp, err := sqsClient.ReceiveMessage(params)
		if err != nil {
			log.WithError(err).Error("Unable to receive message from SQS queue")
			return nil, err
		}

		if len(resp.Messages) > 0 {
			messages = append(messages, resp.Messages...)
		} else {
			return messages, nil
		}
	}
}

func dumpMessages(c *cli.Context) {
	log.Infof("Polling messages....")
	messages, err := pollMessages()

	log.Infof("Found %d messages", len(messages))
	if err != nil {
		return
	}

	transactionIds := make([]string, 0)
	for _, message := range messages {
		transactionId, err := dumpMessage(message)
		if err != nil {
			return
		}
		transactionIds = append(transactionIds, transactionId)
	}

	// Throw these into the `fix_in_flight_transactions` script
	fmt.Println("Transaction Ids:")
	for _, transactionId := range transactionIds {
		fmt.Println(transactionId)
	}
}

type balanceUpdate struct {
	TransactionId string `json:"transactionId"`
}

func dumpMessage(message *awssqs.Message) (string, error) {
	var sns models.SNSMessage

	err := json.Unmarshal([]byte(*message.Body), &sns)
	if err != nil {
		log.WithError(err).Error("Unable to unmarshal SNS response from queue")
		return "", err
	}

	var ciphertextNotification models.EncryptedNotification
	err = json.Unmarshal([]byte(sns.Message), &ciphertextNotification)
	if err != nil {
		log.WithError(err).Error("Unable to unmarshal SNS message into encrypted FABS notification from queue")
		return "", err
	}

	plaintextNotificationJSON, err := decrypter.DecryptMessage(ciphertextNotification)
	if err != nil {
		log.WithError(err).Error("Failed to decrypt a notification from FABS")
		return "", err
	}

	var balanceUpdate balanceUpdate
	err = json.Unmarshal(plaintextNotificationJSON, &balanceUpdate)
	if err != nil {
		log.WithError(err).Error("Unable to unmarshal plaintextNotificationJSON message into balanceUpdate")
		return "", err
	}

	log.Infof(*message.Body)
	log.Infof(string(plaintextNotificationJSON))

	return balanceUpdate.TransactionId, nil
}
