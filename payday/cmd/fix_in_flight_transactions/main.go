package main

import (
	"context"
	"encoding/csv"
	"fmt"
	"os"

	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/dynamo"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
)

const (
	environment = "production"
	dryrun      = true
)

// Run this script if there are in-flight transactions that did not successfully get moved to the main transactions table
// Process:
// 1. Paste transaction ids into the transactions.csv file
// 2. Run this with the dryrun flag set to true (this prints the planned operations)
// 3. Confirm that the printed output is the desired behavior
// 4. Run this with dryrun flag set to false (this will create the required transactions records)
// 5. Run this once more with dryrun flag set to false (this will delete the redundant in-flight records)
func main() {
	gopath := os.Getenv("GOPATH")
	paydayDir := fmt.Sprintf("%s/src/code.justin.tv/commerce/payday", gopath)

	err := config.Load(environment)
	if err != nil {
		panic(err)
	}

	cfg := config.Get()
	if err = cfg.Validate(); err != nil {
		panic(err)
	}

	awsSession, err := session.NewSession(&aws.Config{
		Region: aws.String("us-west-2"),
	})
	if err != nil {
		panic(err)
	}

	dynamoClient := dynamo.NewClient(&dynamo.DynamoClientConfig{
		AwsRegion:   cfg.DynamoRegion,
		TablePrefix: cfg.DynamoTablePrefix,
	})

	inFlightDAO := dynamo.NewInFlightTransactionDao(awsSession, cfg)
	txDAO := dynamo.NewTransactionsDao(dynamoClient)

	txFile, err := os.Open(paydayDir + "/cmd/fix_in_flight_transactions/transactions.csv")
	if err != nil {
		panic(err)
	}

	txReader := csv.NewReader(txFile)
	txRows, err := txReader.ReadAll()
	if err != nil {
		panic(err)
	}

	for rowIdx, txRow := range txRows {
		if len(txRow) != 1 {
			panic(fmt.Sprintf("Invalid row on line num: %v", rowIdx+1))
		}
		txID := txRow[0]
		fmt.Printf("Processing TX: %v \n", txID)

		inFlightRecord, err := inFlightDAO.GetTransaction(context.Background(), txID)
		if err != nil {
			panic(err)
		}

		if inFlightRecord == nil {
			fmt.Println("No in-flight transaction record")
		} else {
			fmt.Println("Found in-flight transaction record")

		}

		txRecord, err := txDAO.Get(txID)
		if err != nil {
			panic(err)
		}

		if txRecord == nil {
			fmt.Println("No primary transaction record")
		} else {
			fmt.Println("Found primary transaction record")
		}

		if txRecord == nil && inFlightRecord == nil {
			fmt.Println("Cannot fix due to missing in-flight record")
		} else if txRecord == nil && inFlightRecord != nil {
			fmt.Println("Creating primary tx record from in-flight record")
			if !dryrun {
				err := txDAO.CreateNew(inFlightRecord)
				if err != nil {
					panic(err)
				}
				fmt.Println("Done - primary record created")
			} else {
				fmt.Println("Skipping due to dryrun")
			}
		} else if txRecord != nil && inFlightRecord != nil {
			fmt.Println("Deleting in-flight record (no longer needed)")
			if !dryrun {
				inFlightDAO.RemoveTransaction(context.Background(), txID)
				fmt.Println("Done - in-flight record deleted")
			} else {
				fmt.Println("Skipping due to dryrun")
			}
		} else if txRecord != nil && inFlightRecord == nil {
			fmt.Println("Already fixed")
		} else {
			panic("should never hit this case")
		}

		fmt.Println("")
	}
}
