package main

import (
	"bufio"
	"os"

	"context"
	"net/http"
	"strings"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	"code.justin.tv/commerce/payday/clients/pantheon"
	fileUtil "code.justin.tv/commerce/payday/utils/close"
	"github.com/codegangsta/cli"
)

var tuidFilePath string
var isProd bool = true

func main() {
	dynamoSetup := cli.NewApp()
	dynamoSetup.Name = "Payday User Report"
	dynamoSetup.Usage = "Makes it rain (by generating user reports)"

	dynamoSetup.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "source, s",
			Value:       "",
			Usage:       "path to tuids file",
			Destination: &tuidFilePath,
		},
		cli.BoolFlag{
			Name:        "isProd",
			Usage:       "bool true/false",
			Destination: &isProd,
		},
	}

	dynamoSetup.Action = genReport
	err := dynamoSetup.Run(os.Args)
	if err != nil {
		log.WithError(err).Error("Error generating the report")
	}
}

func genReport(c *cli.Context) {
	file, err := os.Open(tuidFilePath)
	defer fileUtil.Close(file)
	if err != nil {
		panic(err)
	}

	endpoint := "http://pantheon-dev.internal.justin.tv"
	if isProd {
		endpoint = "http://main.us-west-2.prod.pantheon.twitch.a2z.com"
	}

	var pantheonClient pantheonrpc.Pantheon = pantheonrpc.NewPantheonProtobufClient(endpoint, http.DefaultClient)
	var pantheonClientWrapper pantheon.IPantheonClientWrapper = pantheon.NewPantheonClientWrapper(pantheonClient)

	r := bufio.NewReader(file)
	s := bufio.NewScanner(r)
	s.Split(bufio.ScanLines)

	count := 1
	for s.Scan() {
		log.Infof("Record #: %d", count)

		line := s.Text()
		splitLine := strings.Split(line, ",")
		if len(splitLine) < 2 {
			log.Errorf("Error digesting line: [%s]", line)
			count++
			continue
		}
		tuid := splitLine[0]
		channel := splitLine[1]

		req := pantheonrpc.ModerateEntryReq{}
		req.Domain = pantheon.PantheonBitsUsageDomain
		req.EntryKey = tuid
		req.GroupingKey = channel
		req.ModerationAction = pantheonrpc.ModerationAction_MODERATE
		err = pantheonClientWrapper.ModerateEntry(context.Background(), req)
		//should this crash the whole job? is it a safety net for us or for user service?
		if err != nil {
			log.Errorf("Error calling pantheon")
			panic(err)
		}
		count++
	}
}
