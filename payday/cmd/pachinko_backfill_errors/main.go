package main

import (
	"bufio"
	"encoding/csv"
	"fmt"
	"os"
	"strings"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/s3"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/endpoints"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/codegangsta/cli"
)

const (
	jobFormat           = "%s-%s"
	bitsAdminJobsBucket = "bits-admin-jobs"
)

var environment string
var creator string
var outputFile string
var jobID string
var jobType string
var includeIncomplete bool

func main() {
	payday := cli.NewApp()
	payday.Name = "Pachinko backfill admin job error fetcher"
	payday.Usage = "Makes it rain"

	payday.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "jobID, j",
			Usage:       "the Job ID that was used to start all the jobs.",
			EnvVar:      "JOB_ID",
			Destination: &jobID,
		},
		cli.StringFlag{
			Name:        "jobType, t",
			Usage:       "the Job type that was used to start all the jobs.",
			EnvVar:      "JOB_TYPE",
			Destination: &jobType,
		},
		cli.StringFlag{
			Name:        "environment, e",
			Value:       "staging",
			Usage:       "Runtime config to use. Options are: 'production' and 'staging'. Defaults to 'production' if unset.",
			EnvVar:      "ENVIRONMENT",
			Destination: &environment,
		},
		cli.StringFlag{
			Name:        "requester, r",
			Usage:       "used to record who started the job",
			Destination: &creator,
			EnvVar:      "REQUESTER",
		},
		cli.StringFlag{
			Name:        "output, o",
			Usage:       "location of output file for backfill errors. Should just be a file full of TUIDs that failed.",
			Destination: &outputFile,
			Value:       "pachinko-backfill-errors-out.csv",
			EnvVar:      "OUTPUT_FILE",
		},
		cli.BoolFlag{
			Name:        "include-incomplete, i",
			Usage:       "include incomplete job results",
			Destination: &includeIncomplete,
			EnvVar:      "INCLUDE_INCOMPLETE",
		},
	}

	payday.Action = getPachinkoBackfillErrors
	err := payday.Run(os.Args)
	if err != nil {
		log.WithError(err).Error("Run returned an err")
	}
}

func getPachinkoBackfillErrors(c *cli.Context) {
	log.SetOutput(os.Stdout)

	err := config.Load(environment)
	if err != nil {
		panic(err)
	}

	cfg := config.Get()
	if err := cfg.Validate(); err != nil {
		panic(err)
	}

	awsCfg := &aws.Config{
		Region: aws.String(endpoints.UsWest2RegionID),
	}
	sess, err := session.NewSession(awsCfg)

	if err != nil {
		log.WithError(err).Panic("could not create new AWS Session")
	}

	s3Client := s3.New(sess, s3.NewDefaultConfig())

	dynamoClientNoAudit := dynamo.NewClient(&dynamo.DynamoClientConfig{
		AwsRegion:   cfg.DynamoRegion,
		TablePrefix: cfg.DynamoTablePrefix,
	})

	adminJobsDAO := dynamo.NewAdminJobDao(dynamoClientNoAudit)

	jobs, err := adminJobsDAO.GetByJobPrefix(creator, fmt.Sprintf(jobFormat, jobType, jobID))
	if err != nil {
		log.WithError(err).Panic("could not fetch admin jobs from DB")
	}

	jobsToProcess := make([]*dynamo.AdminJob, 0)
	incompleteJobs := make([]*dynamo.AdminJob, 0)

	for _, job := range jobs {
		if job.Progress < 1.0 {
			log.WithFields(log.Fields{
				"jobID":    job.JobID,
				"progress": job.Progress,
			}).Info("Found incomplete job")
			incompleteJobs = append(incompleteJobs, job)
		} else {
			jobsToProcess = append(jobsToProcess, job)
		}
	}

	log.Infof("Competed jobs: %v", len(jobsToProcess))
	log.Infof("Incomplete jobs: %v", len(incompleteJobs))

	inputThatErrored := make([]string, 0)

	if includeIncomplete {
		jobsToProcess = append(jobsToProcess, incompleteJobs...)
	}
	for _, job := range jobsToProcess {
		inputThatErrored = append(inputThatErrored, getFailedRecords(s3Client, job)...)
	}

	file, err := os.Create(outputFile)
	if err != nil {
		panic(err)
	}

	fileBuffer := bufio.NewWriter(file)
	csvWriter := csv.NewWriter(fileBuffer)

	for _, tuid := range inputThatErrored {
		err := csvWriter.Write([]string{tuid})
		if err != nil {
			log.WithError(err).Panic("could not write entry to file")
		}
	}

	csvWriter.Flush()
	log.Infof("wrote %v records to error tuids", len(inputThatErrored))
}

func getFailedRecords(s3Client s3.IS3Client, job *dynamo.AdminJob) []string {
	chn := make([]string, 0)
	outputS3Location := strings.ReplaceAll(job.InputS3Location, "input", "output")
	exists, err := s3Client.FileExists(bitsAdminJobsBucket, outputS3Location)
	if err != nil {
		log.WithField("path", outputS3Location).WithError(err).Panic("failed to fetch output file existence")
	}
	if exists {
		files, err := s3Client.ListFileNames(bitsAdminJobsBucket, outputS3Location)
		if err != nil {
			log.WithField("path", outputS3Location).WithError(err).Panic("failed to list file names in output bucket")
		}
		log.WithFields(log.Fields{
			"jobID":     job.JobID,
			"numErrors": len(files),
		}).Info("found job with errors")
		for _, f := range files {
			nameParts := strings.Split(f, ".")
			if len(nameParts) < 2 {
				log.WithField("path", outputS3Location).WithField("file", f).WithError(err).Panic("unexpected file format")
			}
			chn = append(chn, nameParts[1])
		}
	}

	return chn
}
