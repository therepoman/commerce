package main

import (
	"bufio"
	"bytes"
	"context"
	"encoding/csv"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"time"

	"code.justin.tv/commerce/payday/adminjob"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/foundation/twitchclient"
	"code.justin.tv/sse/malachai/pkg/s2s/caller"
	"github.com/gofrs/uuid"
)

const (
	creator     = "jawei"                                   //Change this as needed
	inputFile   = "/Users/jawei/Documents/spring_sale_test" //Change this as needed
	environment = "production"                              //Change this as needed
	promoID     = "spring_sale_2019"                        //Change this as needed.
	batchSize   = 50000
	numAttempts = 5
)

type JobContainer struct {
	Client  twitchclient.Client
	Creator string
	PromoID string
	RunID   string
}

//Before running, change the TPS on adminjob/handler/bulkpromonotif/bulkpromonotif.go as needed.
func main() {
	confirmInTerminal(fmt.Sprintf("Are you %s?", creator))

	confirmInTerminal(fmt.Sprintf("Do you understand that you are running this in %s?", environment))

	fmt.Println("Loading config...")
	client := getClient()

	confirmInTerminal(fmt.Sprintf("Is %s the correct input file?", inputFile))
	csvBytes, err := ioutil.ReadFile(inputFile)
	if err != nil {
		panic(err)
	}

	fileBytesReader := bytes.NewReader(csvBytes)
	csvReader := csv.NewReader(fileBytesReader)
	records, err := csvReader.ReadAll()
	if err != nil {
		fmt.Println("Gonna panic")
		panic(err)
	}

	tuids := make([]string, 0)
	tuidMap := make(map[string]interface{})

	for i, row := range records {
		if len(row) != 1 {
			panic(fmt.Sprintf("Invalid row length on line %d", i+1))
		}

		tuid := row[0]
		_, exists := tuidMap[tuid]
		if exists {
			panic(fmt.Sprintf("Duplicate TUID in input: %s", tuid))
		}

		tuidMap[tuid] = nil
		tuids = append(tuids, tuid)
	}

	confirmInTerminal(fmt.Sprintf("Are you ok sending a %s notification to %d users?", promoID, len(tuids)))

	confirmInTerminal(fmt.Sprintf("Are you ok starting multiple admin jobs with %d users per job?", batchSize))

	uuidV4, err := uuid.NewV4()
	if err != nil {
		panic(err)
	}

	runID := uuidV4.String()

	jc := JobContainer{
		Client:  client,
		Creator: creator,
		PromoID: promoID,
		RunID:   runID,
	}

	fmt.Printf("Starting run %s\n", runID)

	batchCount := 1
	batchTuids := make([]string, 0)
	for i, tuid := range tuids {
		batchTuids = append(batchTuids, tuid)

		if len(batchTuids) >= batchSize || i == (len(tuids)-1) {
			jobID, err := sendAdminJobWithRetryLogic(jc, batchCount, batchTuids)
			if err != nil {
				fmt.Printf("Failed to start job for batch #%d (%d tuids) : %v \n", batchCount, len(batchTuids), err)
			} else {
				fmt.Printf("Started job for batch #%d (%d tuids) : %s \n", batchCount, len(batchTuids), jobID)
			}

			batchCount++
			batchTuids = make([]string, 0)
		}
	}

}

func getClient() twitchclient.Client {
	err := config.Load(environment)
	if err != nil {
		panic(err)
	}

	cfg := config.Get()
	if err := cfg.Validate(); err != nil {
		panic(err)
	}

	clientConfig := twitchclient.ClientConf{
		Host:           cfg.IntegrationTestsEndpoint,
		TimingXactName: "payday",
	}

	s2sConfig := caller.Config{}

	rt, err := caller.NewRoundTripper(cfg.S2S.Name, &s2sConfig, nil)
	if err != nil {
		panic(err)
	}

	clientConfig.RoundTripperWrappers = append(clientConfig.RoundTripperWrappers,
		func(inner http.RoundTripper) http.RoundTripper {
			rt.SetInnerRoundTripper(inner)
			return rt
		},
	)

	client, err := twitchclient.NewClient(clientConfig)
	if err != nil {
		panic(err)
	}

	return client

}

func sendAdminJobWithRetryLogic(jc JobContainer, batchIndex int, batchTuids []string) (string, error) {
	var jobID string
	var err error
	for i := 0; i < numAttempts; i++ {
		jobID, err = sendAdminJob(jc, batchIndex, batchTuids)
		if err != nil {
			fmt.Printf("error on attempt %d to send admin job: %v\n", i+1, err)
		} else {
			return jobID, nil
		}
		time.Sleep(time.Second)
	}
	return "", fmt.Errorf("failed sending admin job after %d attempts", numAttempts)
}

func sendAdminJob(jc JobContainer, batchIndex int, batchTuids []string) (string, error) {
	input := ""
	for i, tuid := range batchTuids {
		if i != 0 {
			input += "\n"
		}
		input += tuid
	}

	requestData := adminjob.StartAdminJobRequest{
		Creator: jc.Creator,
		JobID:   fmt.Sprintf("%s-%s-%s-batch-%d", string(adminjob.BulkPromoNotif), jc.PromoID, jc.RunID, batchIndex),
		Type:    string(adminjob.BulkPromoNotif),
		Input:   input,
		Options: fmt.Sprintf("{\"PromoNotifCampaignID\": \"%s\"}", jc.PromoID),
	}

	requestDataJson, err := json.Marshal(requestData)
	if err != nil {
		return "", err
	}

	req, err := jc.Client.NewRequest("POST", "/authed/admin/jobs", bytes.NewReader(requestDataJson))
	if err != nil {
		return "", err
	}

	req.Header.Add("content-type", "application/json")
	resp, err := jc.Client.Do(context.Background(), req, twitchclient.ReqOpts{})
	if err != nil {
		return "", err
	}

	defer func() {
		_ = resp.Body.Close()
	}()

	if resp.StatusCode != 200 {
		return "", twitchclient.HandleFailedResponse(resp)
	}

	return requestData.JobID, nil
}

func confirmInTerminal(msg string) {
	reader := bufio.NewReader(os.Stdin)
	fmt.Printf("%s\n", msg)
	text, err := reader.ReadString('\n')
	if err != nil {
		panic(err)
	}
	text = strings.TrimSpace(strings.ToLower(text))
	if text != "yes" && text != "y" {
		panic("user cancelled operation")
	}
}
