package main

import (
	"bytes"
	"encoding/csv"
	"fmt"
	"io/ioutil"
	"path/filepath"
	"time"

	actionsCache "code.justin.tv/commerce/payday/actions"
	"code.justin.tv/commerce/payday/audit"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/dynamo/actions"
	"code.justin.tv/commerce/payday/iam"
	"code.justin.tv/commerce/payday/lambda"
	model "code.justin.tv/commerce/payday/models/api"
	"code.justin.tv/commerce/payday/s3"
	string_utils "code.justin.tv/commerce/payday/utils/strings"
)

const (
	environment = "staging"
)

func main() {
	err := config.Load(environment)
	if err != nil {
		panic(err)
	}

	cfg := config.Get()
	if err := cfg.Validate(); err != nil {
		panic(err)
	}

	s3Client := s3.NewFromDefaultConfig()
	lambdaClient := lambda.NewFromDefaultConfig()
	iamClient := iam.NewFromDefaultConfig()
	auditSetup := audit.NewAuditSetup(s3Client, lambdaClient, iamClient)

	dynamoClient := dynamo.NewClientWithAudit(&dynamo.DynamoClientConfig{
		AwsRegion:   cfg.DynamoRegion,
		TablePrefix: cfg.DynamoTablePrefix,
	}, auditSetup, &dynamo.DynamoAuditConfig{
		RecordsS3Bucket:      cfg.AuditRecordsS3Bucket,
		LambdaMemorySizeMB:   cfg.AuditLambdaMemorySizeMB,
		LambdaTimeoutSec:     cfg.AuditLambdaTimeoutSec,
		LambdaRoleName:       cfg.AuditLambdaRoleName,
		LambdaCodeS3Bucket:   cfg.AuditLambdaCodeS3Bucket,
		LambdaEventBatchSize: cfg.AuditLambdaEventBatchSize,
	})
	actionDao := actions.NewActionDao(dynamoClient)

	actionsFilePath := fmt.Sprintf("actions/%s", actionsCache.PrefixesFileNameStaging)
	if environment == "production" {
		actionsFilePath = fmt.Sprintf("actions/%s", actionsCache.PrefixesFileNameProduction)
	}

	inFile, err := filepath.Abs(actionsFilePath)
	if err != nil {
		panic(err)
	}

	fileBytes, err := ioutil.ReadFile(inFile)
	if err != nil {
		panic(err)
	}

	fileBytesReader := bytes.NewReader(fileBytes)
	csvReader := csv.NewReader(fileBytesReader)

	rows, err := csvReader.ReadAll()
	if err != nil {
		panic(err)
	}

	canonicalCasingMap := map[string]string{
		"cheer":         "Cheer",
		"4head":         "4Head",
		"bitboss":       "BitBoss",
		"bday":          "bday",
		"dansgame":      "DansGame",
		"doodlecheer":   "DoodleCheer",
		"elegiggle":     "EleGiggle",
		"failfish":      "FailFish",
		"frankerz":      "FrankerZ",
		"heyguys":       "HeyGuys",
		"holidaycheer":  "HolidayCheer",
		"kappa":         "Kappa",
		"kreygasm":      "Kreygasm",
		"mrdestructoid": "MrDestructoid",
		"muxy":          "Muxy",
		"notlikethis":   "NotLikeThis",
		"party":         "Party",
		"pjsalt":        "PJSalt",
		"ripcheer":      "RIPCheer",
		"seemsgood":     "SeemsGood",
		"shamrock":      "Shamrock",
		"streamlabs":    "Streamlabs",
		"swiftrage":     "SwiftRage",
		"trihard":       "TriHard",
		"tww":           "tww",
		"vohiyo":        "VoHiYo",
	}

	action := &actions.Action{
		Prefix:          "cheer",
		CreatedTime:     getCreatedTime("cheer"),
		Support100K:     getSupported100K("cheer"),
		Type:            getType("cheer"),
		CanonicalCasing: canonicalCasingMap["cheer"],
		StartTime:       getCreatedTime("cheer"), //A lot of the time, start time and created time will be equal
	}
	err = actionDao.Update(action)
	if err != nil {
		panic(err)
	}

	for _, row := range rows {
		prefix := row[0]
		action := &actions.Action{
			Prefix:          prefix,
			CreatedTime:     getCreatedTime(prefix),
			Support100K:     getSupported100K(prefix),
			Type:            getType(prefix),
			CanonicalCasing: canonicalCasingMap[prefix],
			StartTime:       getCreatedTime(prefix),
		}
		err := actionDao.Update(action)
		if err != nil {
			panic(err)
		}
	}
}

func getCreatedTime(prefix string) time.Time {
	if string_utils.OneOf(prefix, "cheer") {
		return actionsCache.BitsLaunchDay
	} else if string_utils.OneOf(prefix, "kappa", "kreygasm", "swiftrage", "muxy", "streamlabs", "tww") {
		return actionsCache.AnimatedEmotesSet1LaunchDay
	} else if string_utils.OneOf(prefix, "4head", "notlikethis", "failfish", "mrdestructoid", "pjsalt", "trihard", "vohiyo") {
		return actionsCache.AnimatedEmotesSet2LaunchDay
	} else if string_utils.OneOf(prefix, "bitboss") {
		return actionsCache.BitbossLaunch
	} else if string_utils.OneOf(prefix, "bday") {
		return actionsCache.BdayLaunch
	} else if string_utils.OneOf(prefix, "doodlecheer") {
		return actionsCache.DoodlecheerLaunch
	} else if string_utils.OneOf(prefix, "ripcheer") {
		return actionsCache.RipcheerLaunch
	} else if string_utils.OneOf(prefix, "holidaycheer") {
		return actionsCache.HolidaycheerLaunch
	} else if string_utils.OneOf(prefix, "dansgame", "elegiggle") {
		return actionsCache.DansgameElegiggleLaunch
	} else if string_utils.OneOf(prefix, "shamrock") {
		return actionsCache.ShamrockLaunch
	} else if string_utils.OneOf(prefix, "heyguys", "frankerz", "seemsgood") {
		return actionsCache.HeyguysTrioLaunch
	} else if string_utils.OneOf(prefix, "party") {
		return actionsCache.PartyLaunch
	}

	panic(fmt.Sprintf("could not find created time for prefix: %s", prefix))
}

func getSupported100K(prefix string) bool {
	return prefix == "cheer"
}

func getType(prefix string) string {

	if string_utils.OneOf(prefix, "cheer", "kappa", "kreygasm", "swiftrage", "tww", "4head", "notlikethis", "failfish", "mrdestructoid", "pjsalt", "trihard", "vohiyo", "bday", "ripcheer", "dansgame", "elegiggle", "shamrock", "heyguys", "frankerz", "seemsgood", "party") {
		return model.GlobalFirstPartyAction
	} else if string_utils.OneOf(prefix, "muxy", "streamlabs", "bitboss", "doodlecheer", "holidaycheer") {
		return model.GlobalThirdPartyAction
	}

	panic(fmt.Sprintf("could not find type for prefix: %s", prefix))
}
