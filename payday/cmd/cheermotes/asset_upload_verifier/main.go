package main

import (
	"fmt"
	"image/gif"
	"io/ioutil"
	"os"
	"strings"

	"code.justin.tv/commerce/payday/actions/constants"
	util "code.justin.tv/commerce/payday/utils/close"
	"github.com/codegangsta/cli"
)

//README:
// Update the values below to run this in intelliJ,
// otherwise use -basePath/-path and -cheermotes/-c to specify on the command line
const (
	baseFilePath     = "/Users/jawei/Downloads/ForProduction/"
	baseCheermoteSet = "frankerz,heyguys,seemsgood"
)

var basePath string
var cheermotesToCheck string

func main() {
	cheermoteValidation := cli.NewApp()
	cheermoteValidation.Name = "CheermoteValidator"
	cheermoteValidation.Usage = "Validates your cheermote directory structure and files are properly generated."

	cheermoteValidation.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "basePath, path",
			Value:       baseFilePath,
			Usage:       "path to file directory",
			Destination: &basePath,
		},
		cli.StringFlag{
			Name:        "cheermotes, c",
			Value:       baseCheermoteSet,
			Usage:       "cheermotes to check, comma separated with no space",
			Destination: &cheermotesToCheck,
		},
	}

	cheermoteValidation.Action = validateCheermotes
	err := cheermoteValidation.Run(os.Args)
	if err != nil {
		panic(err)
	}
}

var cheermoteList []string

var imageFiles = []string{"1", "1.5", "2", "3", "4"}
var imageFileSize = map[string]int{
	string(constants.Scale1):  28,
	string(constants.Scale15): 42,
	string(constants.Scale2):  56,
	string(constants.Scale3):  84,
	string(constants.Scale4):  112,
}
var cheerMoteSizes = []string{string(constants.Tier1), string(constants.Tier100), string(constants.Tier1000), string(constants.Tier5000), string(constants.Tier10000)}
var imageTypes = []string{string(constants.StaticAnimationType), string(constants.AnimatedAnimationType)}

var staticImageExtension = ".png"
var animatedImageExtension = ".gif"
var imageDisplayType = []string{string(constants.LightBackground), string(constants.DarkBackground)}
var ignoreFile = ".DS_Store"

func validateCheermotes(c *cli.Context) {
	cheermoteList = strings.Split(cheermotesToCheck, ",")
	var files = make(map[string]string)

	fileInfo, err := ioutil.ReadDir(basePath)
	if err != nil {
		return
	}
	for _, file := range fileInfo {
		if file.Name() == ignoreFile {
			continue
		}
		files[file.Name()] = file.Name()
	}

	if len(cheermoteList) < 1 {
		fmt.Println("No input cheermotes")
	}

	for _, cheermote := range cheermoteList {
		fmt.Println("Checking cheermote: " + cheermote)
		_, ok := files[cheermote]
		if !ok {
			fmt.Println("No cheermote directory found for:" + cheermote)
		}
		for _, displayType := range imageDisplayType {
			_, err := ioutil.ReadDir(basePath + cheermote + "/" + displayType)
			if err != nil {
				fmt.Println("No displayType directory found for:" + cheermote + " " + displayType)
			}

			for _, animatedOrStatic := range imageTypes {
				_, err := ioutil.ReadDir(basePath + cheermote + "/" + displayType + "/" + animatedOrStatic)
				if err != nil {
					fmt.Println("No animated or static directory found for:" + cheermote + " " + displayType + " " + animatedOrStatic)
				}

				for _, size := range cheerMoteSizes {
					_, err := ioutil.ReadDir(basePath + cheermote + "/" + displayType + "/" + animatedOrStatic + "/" + size)
					if err != nil {
						fmt.Println("No animated or static directory found for:" + cheermote + " " + displayType + " " + animatedOrStatic + " " + size)
					}
					for _, image := range imageFiles {
						if animatedOrStatic == "animated" {
							if !Exists(basePath + cheermote + "/" + displayType + "/" + animatedOrStatic + "/" + size + "/" + image + animatedImageExtension) {
								fmt.Println("Crap!")
							} else {
								var path = basePath + cheermote + "/" + displayType + "/" + animatedOrStatic + "/" + size + "/" + image + animatedImageExtension
								inputFile, err := os.Open(path)

								defer util.Close(inputFile)

								if err != nil {
									fmt.Println("Could not open gif from path: " + path)
									continue
								}

								gifFile, err := gif.DecodeAll(inputFile)

								if err != nil {
									fmt.Println("Could not decode gif from path: " + path)
									continue
								}

								if gifFile.LoopCount > 0 {
									fmt.Println(path + "has a gif that loops a limited amont of times.")
								}

								if imageFileSize[image] != gifFile.Config.Height {
									fmt.Println(path + "has wrong image height.")
									fmt.Println(imageFileSize[image])
									fmt.Println(gifFile.Config.Height)
								}

								if imageFileSize[image] != gifFile.Config.Width {
									fmt.Println(path + "has wrong image width.")
									fmt.Println(imageFileSize[image])
									fmt.Println(gifFile.Config.Width)
								}
							}
						}

						if animatedOrStatic == "static" {
							if !Exists(basePath + cheermote + "/" + displayType + "/" + animatedOrStatic + "/" + size + "/" + image + staticImageExtension) {
								fmt.Println("Crap!")
							}
						}
					}
				}
			}
		}
	}
}

// Checks whether the named file or directory exists.
func Exists(name string) bool {
	if _, err := os.Stat(name); err != nil {
		if os.IsNotExist(err) {
			fmt.Println("No cheermote file found for:" + name)
			return false
		}
	}
	//fmt.Println("cheermote file found for:" + name)
	return true
}
