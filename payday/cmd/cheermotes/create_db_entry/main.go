package main

import (
	"fmt"
	"time"

	"code.justin.tv/commerce/payday/audit"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/dynamo"
	dynamo_actions "code.justin.tv/commerce/payday/dynamo/actions"
	"code.justin.tv/commerce/payday/iam"
	"code.justin.tv/commerce/payday/lambda"
	"code.justin.tv/commerce/payday/models/api"
	"code.justin.tv/commerce/payday/s3"
)

const (
	environment = "staging"
)

func main() {
	err := config.Load(environment)
	if err != nil {
		panic(err)
	}

	cfg := config.Get()
	if err := cfg.Validate(); err != nil {
		panic(err)
	}

	s3Client := s3.NewFromDefaultConfig()
	lambdaClient := lambda.NewFromDefaultConfig()
	iamClient := iam.NewFromDefaultConfig()
	auditSetup := audit.NewAuditSetup(s3Client, lambdaClient, iamClient)

	dynamoClient := dynamo.NewClientWithAudit(&dynamo.DynamoClientConfig{
		AwsRegion:   cfg.DynamoRegion,
		TablePrefix: cfg.DynamoTablePrefix,
	}, auditSetup, &dynamo.DynamoAuditConfig{
		RecordsS3Bucket:      cfg.AuditRecordsS3Bucket,
		LambdaMemorySizeMB:   cfg.AuditLambdaMemorySizeMB,
		LambdaTimeoutSec:     cfg.AuditLambdaTimeoutSec,
		LambdaRoleName:       cfg.AuditLambdaRoleName,
		LambdaCodeS3Bucket:   cfg.AuditLambdaCodeS3Bucket,
		LambdaEventBatchSize: cfg.AuditLambdaEventBatchSize,
	})
	actionsDao := dynamo_actions.NewActionDao(dynamoClient)

	cheermote := dynamo_actions.Action{
		Prefix:          "foo",
		CanonicalCasing: "Foo",
		Type:            api.GlobalFirstPartyAction,
		CreatedTime:     time.Now(),
		StartTime:       time.Date(2018, 07, 03, 12, 00, 00, 00, time.Local),
	}

	err = actionsDao.Update(&cheermote)
	if err != nil {
		panic(err)
	}

	action, err := actionsDao.Get(cheermote.Prefix)
	if err != nil {
		panic(err)
	}

	fmt.Printf("%+v", action)
}
