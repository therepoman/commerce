package main

import (
	"bufio"
	"encoding/csv"
	"fmt"
	"os"
	"strings"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/dynamo"
	"github.com/codegangsta/cli"
)

// Usage:
// go run main.go -e <staging|production> -f <your_transaction_id.csv>

// UPDATE THESE TO WHATEVER YOU WANT
const (
	adyenSuffix    = "_ADYEN"
	unknownSuffix  = "_UNKNOWN"
	amazonSuffix   = "_AMAZON"
	amazonPlatform = "AMAZON"

	erroredTransactionIDFilename = "errored_transaction_ids.csv"
)

type ErroredTransaction struct {
	TransactionID string
	Error         error
}

var environment string
var csvFileName string
var dryRun bool
var missingTransactions int
var skippedTransactions int

func main() {
	dynamoSetup := cli.NewApp()
	dynamoSetup.Name = "Fix Bits Type"
	dynamoSetup.Usage = "Fixes bits type"

	dynamoSetup.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "environment, e",
			Value:       "staging",
			Usage:       "Runtime config to use. Options are: 'production' and 'staging'. Defaults to 'staging' if unset.",
			Destination: &environment,
		},
		cli.StringFlag{
			Name:        "file, f",
			Usage:       "Name of file to csv.",
			Destination: &csvFileName,
		},
		cli.BoolFlag{
			Name:        "dryrun, r",
			Usage:       "Whether to run a dryrun or not.",
			Destination: &dryRun,
		},
	}

	dynamoSetup.Action = fixBitsType
	err := dynamoSetup.Run(os.Args)
	if err != nil {
		panic(err)
	}
}

func fixBitsType(c *cli.Context) {
	err := config.Load(environment)
	if err != nil {
		logrus.WithError(err).Panic("Rekt while loading config")
	}

	cfg := config.Get()
	if environment != config.DevelopmentEnvironment {
		err = cfg.Validate()
		if err != nil {
			logrus.WithError(err).Panic("Configuration file could not be validated")
		}
	}

	dynamoClient := dynamo.NewClient(&dynamo.DynamoClientConfig{
		AwsRegion:   cfg.DynamoRegion,
		TablePrefix: cfg.DynamoTablePrefix,
	})

	transactionsDAO := dynamo.NewTransactionsDao(dynamoClient)

	// Get transaction IDs from CSV
	csvFile, _ := os.Open(csvFileName)

	reader := csv.NewReader(bufio.NewReader(csvFile))

	logrus.Infof("Environment: %s", environment)
	logrus.Infof("File: %s", csvFileName)
	if dryRun {
		logrus.Info("Dryrun flag passed -- this script will not perform actual updates to transactions")
	}

	rows, err := reader.ReadAll()
	if err != nil {
		logrus.WithError(err).Panicf("error reading from %s", csvFileName)
	}

	numberOfTransactions := len(rows)
	logrus.WithField("NumberOfTransactions", numberOfTransactions).Info("Starting updates...")

	var erroredTransactions []ErroredTransaction
	for _, row := range rows {
		transactionID := row[0]

		err = updateTransaction(transactionsDAO, transactionID)
		if err != nil {
			logrus.WithField("transactionID", transactionID).WithError(err).Warn("errored updating transaction")
			erroredTransactions = append(erroredTransactions, ErroredTransaction{
				TransactionID: transactionID,
				Error:         err,
			})
		}
	}

	logrus.WithFields(logrus.Fields{
		"skippedTransactions": skippedTransactions,
		"missingTranactions":  missingTransactions,
	}).Info("Finished processing")

	if len(erroredTransactions) > 0 {
		logrus.WithField("NumberOfErroredTransactions", len(erroredTransactions)).Errorf("some transaction updates failed, saving to %s", erroredTransactionIDFilename)

		erroredTransactionsFile, err := os.Create(erroredTransactionIDFilename)
		if err != nil {
			logrus.WithError(err).Panicf("error reading from %s", erroredTransactionIDFilename)
		}
		defer erroredTransactionsFile.Close()

		writer := csv.NewWriter(erroredTransactionsFile)
		defer writer.Flush()

		for _, etid := range erroredTransactions {
			row := []string{etid.TransactionID, etid.Error.Error()}
			err := writer.Write(row)
			if err != nil {
				logrus.WithError(err).WithField("etid", etid).Panic("error writing row")
			}
		}
	}
}

func updateTransaction(transactionsDAO dynamo.ITransactionsDao, transactionID string) error {
	transaction, err := transactionsDAO.Get(transactionID)
	if err != nil {
		return err
	}

	if transaction == nil {
		missingTransactions++
		logrus.WithFields(logrus.Fields{
			"transactionID":       transactionID,
			"missingTransactions": missingTransactions,
		}).Warn("Transaction does not exist, skipping")
		return nil
	}

	if !strings.HasSuffix(*transaction.BitsType, adyenSuffix) && *transaction.Platform == amazonPlatform {
		if strings.HasSuffix(*transaction.BitsType, unknownSuffix) {
			logrus.WithFields(logrus.Fields{
				"transactionID": transactionID,
			}).Info(fmt.Sprintf("Found (%s) Bits Type, trimming", unknownSuffix))
			*transaction.BitsType = strings.TrimSuffix(*transaction.BitsType, unknownSuffix)
		} else if strings.HasSuffix(*transaction.BitsType, amazonSuffix) {
			logrus.WithFields(logrus.Fields{
				"transactionID": transactionID,
			}).Info(fmt.Sprintf("Found (%s) Bits Type, trimming", amazonSuffix))
			*transaction.BitsType = strings.TrimSuffix(*transaction.BitsType, amazonSuffix)
		}

		*transaction.BitsType = *transaction.BitsType + adyenSuffix

		logrus.WithFields(logrus.Fields{
			"transactionID": transactionID,
		}).Info(fmt.Sprintf("New Bits type for transaction %s", *transaction.BitsType))
	} else {
		skippedTransactions++
		logrus.WithFields(logrus.Fields{
			"transactionID":       transactionID,
			"skippedTransactions": skippedTransactions,
		}).Info(fmt.Sprintf("Skipping transaction with Bits Type (%s) and platform (%s)", *transaction.BitsType, *transaction.Platform))
	}

	// Don't actually perform update if dry run
	if dryRun {
		logrus.WithField("transactionID", transactionID).Info("skipping processing transaction due to dryrun")
		return nil
	}

	logrus.WithField("transactionID", transactionID).Info("processing transaction")
	return transactionsDAO.Update(transaction)
}
