package main

import (
	"bufio"
	"bytes"
	"context"
	"encoding/csv"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"os"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/adminjob"
	"code.justin.tv/commerce/payday/adminjob/handler/singlepurchasebackfill"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/foundation/twitchclient"
	"code.justin.tv/sse/malachai/pkg/s2s/caller"
	"github.com/codegangsta/cli"
	"github.com/gofrs/uuid"
)

/*
	Make an input csv file that has two columns: 1. UserID 2. BitsType
	from Redshift.
	e.g.
		SELECT DISTINCT
		  t.requestingtwitchuserid,
		  b.bitaccounttype
		FROM
		  transactioninfo t
		INNER JOIN
		  balanceupdatesperaccounttype b
		ON
		  t.transactionid = b.transactionid
		WHERE
		  t.transactiontype = 'AddBitsToCustomerAccount'
		AND
		  t.timeofevent < 1567321482 -- September 1, 2019 12:04:42 AM GMT-07:00 DST
		AND
		  b.bitaccounttype
		IN
		(
		  '500_BITS_FOR_350_PROMO',
		  'BitsForRetailPromo',
		  'CB_099_PRIME_PROMO',
		  'CB_099_PRIME_PROMO_PAYPAL',
		  'CB_10000_CBP_00100',
		  'CB_1000_CBP_00100',
		  'CB_1000_CBP_00100_PAYPAL',
		  'CB_1000_CBP_00100_XSOLLA',
		  'CB_100_CBP_00020',
		  'CB_1999_CBP_00100',
		  'CB_2000_CBP_00100',
		  'CB_200_CBP_00020',
		  'CB_2500_CBP_00100',
		  'CB_2500_CBP_00100_PAYPAL',
		  'CB_2500_CBP_00100_SPRING',
		  'CB_2500_CBP_00100_SPRING_PAYPAL',
		  'CB_2500_CBP_00100_XSOLLA',
		  'CB_350_PROMO',
		  'CB_3700_CBP_00100',
		  'CB_400_JACK_RYAN_PROMO',
		  'CB_700_CBP_00100',
		  'CB_9999_CBP_00100',
		  'CB_999_CBP_00100',
		  'CB_PRIME_VIDEO_THE_PURGE_PROMO',
		  'NEW_YEARS_2018_NINJA_PROMO',
		  'phanto.commerce.promo',
		  'phanto.forte.rallypromo',
		  'phanto.primehannahpromo2019',
		  'phanto.twitchcon.2018.gameon',
		  'tv.twitch.android.iap.bits.promotion.t1',
		  'tv.twitch.android.iap.bits.promotion.t25',
		  'tv.twitch.android.iap.bits.promotion.t7',
		  'tv.twitch.ios.iap.bits.promotion.t1',
		  'tv.twitch.ios.iap.bits.promotion.t25'
		)
*/

const (
	batchSize = 3000
)

var environment string
var creator string
var inputFile string

func main() {
	payday := cli.NewApp()
	payday.Name = "Single purchase table backfill admin job starter"
	payday.Usage = "Run this script to start admin jobs"

	payday.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "environment, e",
			Value:       "staging",
			Usage:       "Runtime config to use. Options are: 'production' and 'staging'. Defaults to 'production' if unset.",
			EnvVar:      "ENVIRONMENT",
			Destination: &environment,
		},
		cli.StringFlag{
			Name:        "requester, r",
			Usage:       "used to record who started the job (the LDAP of the user)",
			Destination: &creator,
		},
		cli.StringFlag{
			Name:        "input, i",
			Usage:       "location of input for backfill.",
			Destination: &inputFile,
		},
	}

	payday.Action = startBackfill
	err := payday.Run(os.Args)
	if err != nil {
		log.WithError(err).Error("Run returned an err")
	}
}

func startBackfill(c *cli.Context) {
	log.SetOutput(os.Stdout)

	runID := uuid.Must(uuid.NewV4()).String()

	err := config.Load(environment)
	if err != nil {
		panic(err)
	}

	cfg := config.Get()
	if err := cfg.Validate(); err != nil {
		panic(err)
	}

	// Create HTTP Client with S2S
	clientConfig := twitchclient.ClientConf{
		Host:           cfg.IntegrationTestsEndpoint,
		TimingXactName: "payday",
	}

	s2sConfig := caller.Config{}

	rt, err := caller.NewRoundTripper(cfg.S2S.Name, &s2sConfig, nil)
	if err != nil {
		panic(err)
	}

	clientConfig.RoundTripperWrappers = append(clientConfig.RoundTripperWrappers,
		func(inner http.RoundTripper) http.RoundTripper {
			rt.SetInnerRoundTripper(inner)
			return rt
		},
	)

	client, err := twitchclient.NewClient(clientConfig)

	if err != nil {
		panic(err)
	}

	file, err := os.Open(inputFile)
	if err != nil {
		panic(err)
	}

	fileBuffer := bufio.NewReader(file)
	csvReader := csv.NewReader(fileBuffer)

	batch := make([]singlepurchasebackfill.BitsEntitlement, 0)
	rowIndex := 0
	batchIndex := 0

	log.Infof("starting backfill run %v", runID)

	for {
		row, csvErr := csvReader.Read()

		if csvErr != nil && csvErr != io.EOF {
			log.WithField("rowIndex", rowIndex).WithError(err).Error("failed to read row from input file")
			continue
		}

		if row != nil {
			tx, err := parseRow(row)
			if err != nil {
				log.WithField("rowIndex", rowIndex).WithError(err).Error("failed to parse row from input file")
				continue
			}

			batch = append(batch, tx)
			rowIndex++
		}

		if rowIndex%batchSize == 0 || csvErr == io.EOF {
			batchIndex++
			log.Infof("Sending batch #%d", batchIndex)

			attempt := 0
			for {
				err = sendAdminJob(client, runID, batch, batchIndex)
				if err == nil {
					log.Infof("Successfully sent batch #%d", batchIndex)
					break
				} else if attempt == 10 {
					log.WithError(err).Errorf("Error sending batch #%d", batchIndex)
					break
				}

				attempt++
			}

			batch = make([]singlepurchasebackfill.BitsEntitlement, 0)
			if csvErr == io.EOF {
				log.Infof("Hit EOF after batch #%d. Terminating!", batchIndex)
				break
			}

			time.Sleep(100 * time.Millisecond)
		}
	}

	log.WithFields(log.Fields{
		"numOfRows":    rowIndex,
		"numOfBatches": batchIndex,
	}).Info("Script complete")
}

func sendAdminJob(client twitchclient.Client, runId string, batch []singlepurchasebackfill.BitsEntitlement, batchIndex int) error {
	// Create the StartAdminJobRequest
	batchJson, err := json.Marshal(batch)
	if err != nil {
		return err
	}

	requestData := adminjob.StartAdminJobRequest{
		Creator: creator,
		JobID:   fmt.Sprintf("%s-%s-batch-%d", string(adminjob.SinglePurchaseBackfill), runId, batchIndex),
		Type:    string(adminjob.SinglePurchaseBackfill),
		Input:   string(batchJson),
	}

	requestDataJson, err := json.Marshal(requestData)
	if err != nil {
		return err
	}

	// Create request
	req, err := client.NewRequest("POST", "/authed/admin/jobs", bytes.NewReader(requestDataJson))
	if err != nil {
		return err
	}

	req.Header.Add("content-type", "application/json")

	// Send request
	resp, err := client.Do(context.Background(), req, twitchclient.ReqOpts{})
	if err != nil {
		return err
	}

	defer func() {
		_ = resp.Body.Close()
	}()

	if resp.StatusCode != 200 {
		return twitchclient.HandleFailedResponse(resp)
	}

	return nil
}

func parseRow(row []string) (singlepurchasebackfill.BitsEntitlement, error) {
	if len(row) < 2 {
		return singlepurchasebackfill.BitsEntitlement{}, errors.New("row was not of length 2")
	}

	return singlepurchasebackfill.BitsEntitlement{
		UserID:   row[0],
		BitsType: row[1],
	}, nil
}
