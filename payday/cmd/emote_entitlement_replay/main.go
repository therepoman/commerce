package main

import (
	"context"
	"encoding/json"
	"fmt"
	"os"
	"path"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/models/api"
	"code.justin.tv/commerce/payday/sns"
	"github.com/codegangsta/cli"
	"golang.org/x/time/rate"
)

var environment string
var maxEmoteGroupEntitlementTPS rate.Limit = 5

var emoteGroupEntitlementLimiter = rate.NewLimiter(maxEmoteGroupEntitlementTPS, 1)

func main() {
	emoteBackfill := cli.NewApp()
	emoteBackfill.Name = "Payday Bits Emote Rewards Backfill"
	emoteBackfill.Usage = "Backfills emote entitlements for all bits tiers currently in the <env>_badge_tier_emote_groupids dynamo table"

	environment = "production"
	emoteBackfill.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "environment, e",
			Value:       "production",
			Usage:       "Runtime config to use. Options are: 'production', 'staging', and `development`. Defaults to 'development' if unset.",
			EnvVar:      "payday_APP_ENV",
			Destination: &environment,
		},
	}

	emoteBackfill.Action = runBackfill
	err := emoteBackfill.Run(os.Args)
	if err != nil {
		panic(err)
	}
}

func runBackfill(c *cli.Context) {
	ctx := context.Background()
	log.Infof("Loading config file")
	err := config.Load(c.String("e"))
	if err != nil {
		log.Panicf("Error while loading configuration file: %v", err)
	}

	cfg := config.Get()
	if err := cfg.Validate(); err != nil {
		log.Panicf("Rekt while validating configuration file: %v", err)
	}

	if err != nil {
		log.WithError(err).Panic("Could not start new aws session")
	}

	log.Infof("Setting up SNS")
	snsClient := sns.NewSnsClient(cfg.SQSRegion)

	// leaving the groupID blank will make a call to Mako to generate one
	// Enter ChannelID here
	sendBackfillSNSMessage(ctx, snsClient, cfg, 10000, "", "456164749")
}

func sendBackfillSNSMessage(ctx context.Context, client *sns.SNSClient, cfg config.Configuration, threshold int64, groupID string, channelID string) {
	if threshold == 1 || threshold == 100 {
		return
	}

	_ = emoteGroupEntitlementLimiter.Wait(ctx)

	message := api.EmoticonBackfillSNSMessage{
		Threshold: threshold,
		GroupID:   groupID,
		ChannelID: channelID,
		// if you have know specific user that needs to be back filled add it to list of Users below - remove if not needed
		UserIDs: &[]string{"476135480"},
	}

	messageJSON, err := json.Marshal(message)
	if err != nil {
		log.WithError(err).Panic("Error marshaling emoticon backfill message json")
	}

	log.Infof("Sending message for entitlement for channel %s and threshold %d and group %s", channelID, threshold, groupID)

	err = client.PostToTopic(cfg.EmoticonBackfillSNSTopic, string(messageJSON))
	if err != nil {
		log.WithError(err).Panic("Error posting emoticon backfill to SNS")
	}

	writeProgress("progress", groupID)
}

func writeProgress(destFileName string, groupID string) {
	dir, err := os.Getwd()
	if err != nil {
		panic(err)
	}

	dest := path.Join(dir, fmt.Sprintf("%s.txt", destFileName))
	f, err := os.OpenFile(dest, os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		panic(err)
	}

	_, err = f.WriteString(groupID + "\n")
	if err != nil {
		panic(err)
	}

	f.Close()
}
