package main

import (
	"bufio"
	"bytes"
	"context"
	"encoding/csv"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"os"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/adminjob"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/foundation/twitchclient"
	"code.justin.tv/sse/malachai/pkg/s2s/caller"
	"github.com/codegangsta/cli"
	"github.com/gofrs/uuid"
)

const (
	batchSize = 4000
)

var environment string
var creator string
var inputFile string

func main() {
	payday := cli.NewApp()
	payday.Name = "Pachinko backfill admin job starter"
	payday.Usage = "Makes it rain"

	payday.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "environment, e",
			Value:       "staging",
			Usage:       "Runtime config to use. Options are: 'production' and 'staging'. Defaults to 'production' if unset.",
			EnvVar:      "ENVIRONMENT",
			Destination: &environment,
		},
		cli.StringFlag{
			Name:        "requester, r",
			Usage:       "used to record who started the job",
			Destination: &creator,
		},
		cli.StringFlag{
			Name:        "input, i",
			Usage:       "location of input for backfill. Should just be a file full of TUIDs.",
			Destination: &inputFile,
			EnvVar:      "INPUT_FILE",
		},
	}

	payday.Action = startBackfill
	err := payday.Run(os.Args)
	if err != nil {
		log.WithError(err).Error("Run returned an err")
	}
}

func startBackfill(c *cli.Context) {
	log.SetOutput(os.Stdout)

	runID := uuid.Must(uuid.NewV4()).String()

	err := config.Load(environment)
	if err != nil {
		panic(err)
	}

	cfg := config.Get()
	if err := cfg.Validate(); err != nil {
		panic(err)
	}

	// Create HTTP Client with S2S
	clientConfig := twitchclient.ClientConf{
		Host:           cfg.IntegrationTestsEndpoint,
		TimingXactName: "payday",
	}

	s2sConfig := caller.Config{}

	rt, err := caller.NewRoundTripper(cfg.S2S.Name, &s2sConfig, nil)
	if err != nil {
		panic(err)
	}

	clientConfig.RoundTripperWrappers = append(clientConfig.RoundTripperWrappers,
		func(inner http.RoundTripper) http.RoundTripper {
			rt.SetInnerRoundTripper(inner)
			return rt
		},
	)

	client, err := twitchclient.NewClient(clientConfig)

	if err != nil {
		panic(err)
	}

	file, err := os.Open(inputFile)
	if err != nil {
		panic(err)
	}

	fileBuffer := bufio.NewReader(file)
	csvReader := csv.NewReader(fileBuffer)

	batch := make([]string, 0)
	rowIndex := 0
	batchIndex := 0

	log.Infof("starting backfill run %v", runID)

	for {
		row, csvErr := csvReader.Read()

		if csvErr != nil && csvErr != io.EOF {
			log.WithField("rowIndex", rowIndex).WithError(err).Error("failed to read row from input file")
			continue
		}

		if row != nil {
			tuid, err := parseRow(row)
			if err != nil {
				log.WithField("rowIndex", rowIndex).WithError(err).Error("failed to parse row from input file")
				continue
			}

			batch = append(batch, tuid)
			rowIndex++
		}

		if (rowIndex+1)%batchSize == 0 || csvErr == io.EOF {
			log.Infof("Sending batch #%d", batchIndex)

			err = sendAdminJob(client, runID, batch, batchIndex)
			if err != nil {
				log.WithError(err).Errorf("Error sending batch #%d", batchIndex)
			} else {
				log.Infof("Successfully sent batch #%d", batchIndex)
			}

			if csvErr == io.EOF {
				log.Infof("Hit EOF after batch #%d. Terminating!", batchIndex)
				break
			}

			batch = make([]string, 0)
			batchIndex++

			log.Info("Sleeping for 1 second...")
			time.Sleep(1 * time.Second)
		}
	}

	log.WithFields(log.Fields{
		"numOfRows":    rowIndex + 1,
		"numOfBatches": batchIndex + 1,
	}).Info("Script complete")
}

func sendAdminJob(client twitchclient.Client, runId string, batch []string, batchIndex int) error {
	// Create the StartAdminJobRequest
	batchJson, err := json.Marshal(batch)
	if err != nil {
		return err
	}

	requestData := adminjob.StartAdminJobRequest{
		Creator: creator,
		JobID:   fmt.Sprintf("%s-%s-batch-%d", string(adminjob.PachinkoBackfill), runId, batchIndex),
		Type:    string(adminjob.PachinkoBackfill),
		Input:   string(batchJson),
	}

	requestDataJson, err := json.Marshal(requestData)
	if err != nil {
		return err
	}

	// Create request
	req, err := client.NewRequest("POST", "/authed/admin/jobs", bytes.NewReader(requestDataJson))
	if err != nil {
		return err
	}

	req.Header.Add("content-type", "application/json")

	// Send request
	resp, err := client.Do(context.Background(), req, twitchclient.ReqOpts{})
	if err != nil {
		return err
	}

	defer func() {
		_ = resp.Body.Close()
	}()

	if resp.StatusCode != 200 {
		return twitchclient.HandleFailedResponse(resp)
	}

	return nil
}

func parseRow(row []string) (string, error) {
	if len(row) != 1 {
		return "", errors.New("row was not of length 1")
	}

	return row[0], nil
}
