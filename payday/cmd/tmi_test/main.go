package main

import (
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/clients/tmi"
)

func main() {
	tmiClient := tmi.NewHttpTMIClient("https://prod.clue.twitch.a2z.com")
	err := tmiClient.SendSystemMessage(tmi.SystemMessageParams{
		Recipient: "116076154",
		Message:   "Test system message",
	})
	if err != nil {
		log.WithError(err).Error("TMI call failed")
	}
}
