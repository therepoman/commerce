package main

import (
	"bufio"
	"bytes"
	"context"
	"encoding/csv"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"os"
	"strconv"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/adminjob"
	"code.justin.tv/commerce/payday/adminjob/handler/transactionbackfill"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/foundation/twitchclient"
	"code.justin.tv/sse/malachai/pkg/s2s/caller"
	"github.com/codegangsta/cli"
	"github.com/gofrs/uuid"
)

/*
  Dataset is exported using the SQL query below against raincatcher:
	select distinct
		t.transactionid,
		t.transactiontype,
		min(t.timeofevent) as timeofevent,
		t.requestingtwitchuserid,
		t.targettwitchuserid,
		b.bitaccounttype,
		t.processedbitamount,
		t.eventreasoncode
	from
		transactioninfo t
	left join
		balanceupdatesperaccounttype b
	on
		t.transactionid = b.transactionid
	where
		t.transactiontype <> \'UseBitsOnExtension\'
	and
		t.timeofevent < \'1563606000\'
	group by t.transactionid,
			t.transactiontype,
			t.requestingtwitchuserid,
			t.targettwitchuserid,
			b.bitaccounttype,
			t.processedbitamount,
			t.eventreasoncode')

	WARNING: this is just an example and is not 100% correct, modify this logic/query as necessary
*/

const (
	batchSize = 3000
)

var environment string
var creator string
var inputFile string

func main() {
	payday := cli.NewApp()
	payday.Name = "Transactions table backfill admin job starter"
	payday.Usage = "Makes it rain"

	payday.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "environment, e",
			Value:       "staging",
			Usage:       "Runtime config to use. Options are: 'production' and 'staging'. Defaults to 'production' if unset.",
			EnvVar:      "ENVIRONMENT",
			Destination: &environment,
		},
		cli.StringFlag{
			Name:        "requester, r",
			Usage:       "used to record who started the job",
			Destination: &creator,
		},
		cli.StringFlag{
			Name:        "input, i",
			Usage:       "location of input for backfill.",
			Destination: &inputFile,
			EnvVar:      "INPUT_FILE",
		},
	}

	payday.Action = startBackfill
	err := payday.Run(os.Args)
	if err != nil {
		log.WithError(err).Error("Run returned an err")
	}
}

func startBackfill(c *cli.Context) {
	log.SetOutput(os.Stdout)

	runID := uuid.Must(uuid.NewV4()).String()

	err := config.Load(environment)
	if err != nil {
		panic(err)
	}

	cfg := config.Get()
	if err := cfg.Validate(); err != nil {
		panic(err)
	}

	// Create HTTP Client with S2S
	clientConfig := twitchclient.ClientConf{
		Host:           cfg.IntegrationTestsEndpoint,
		TimingXactName: "payday",
	}

	s2sConfig := caller.Config{}

	rt, err := caller.NewRoundTripper(cfg.S2S.Name, &s2sConfig, nil)
	if err != nil {
		panic(err)
	}

	clientConfig.RoundTripperWrappers = append(clientConfig.RoundTripperWrappers,
		func(inner http.RoundTripper) http.RoundTripper {
			rt.SetInnerRoundTripper(inner)
			return rt
		},
	)

	client, err := twitchclient.NewClient(clientConfig)

	if err != nil {
		panic(err)
	}

	file, err := os.Open(inputFile)
	if err != nil {
		panic(err)
	}

	fileBuffer := bufio.NewReader(file)
	csvReader := csv.NewReader(fileBuffer)

	batch := make([]*transactionbackfill.Transaction, 0)
	rowIndex := 0
	batchIndex := 0

	log.Infof("starting backfill run %v", runID)

	for {
		row, csvErr := csvReader.Read()

		if csvErr != nil && csvErr != io.EOF {
			log.WithField("rowIndex", rowIndex).WithError(err).Error("failed to read row from input file")
			continue
		}

		if row != nil {
			tx, err := parseRow(row)
			if err != nil {
				log.WithField("rowIndex", rowIndex).WithError(err).Error("failed to parse row from input file")
				continue
			}

			batch = append(batch, tx)
			rowIndex++
		}

		if (rowIndex+1)%batchSize == 0 || csvErr == io.EOF {
			batchIndex++
			log.Infof("Sending batch #%d", batchIndex)

			attempt := 0
			for {
				err = sendAdminJob(client, runID, batch, batchIndex)
				if err == nil {
					log.Infof("Successfully sent batch #%d", batchIndex)
					break
				} else if attempt == 10 {
					log.WithError(err).Errorf("Error sending batch #%d", batchIndex)
					break
				}

				attempt = attempt + 1
			}

			batch = make([]*transactionbackfill.Transaction, 0)
			if csvErr == io.EOF {
				log.Infof("Hit EOF after batch #%d. Terminating!", batchIndex)
				break
			}

			time.Sleep(100 * time.Millisecond)
		}
	}

	log.WithFields(log.Fields{
		"numOfRows":    rowIndex,
		"numOfBatches": batchIndex,
	}).Info("Script complete")
}

func sendAdminJob(client twitchclient.Client, runId string, batch []*transactionbackfill.Transaction, batchIndex int) error {
	// Create the StartAdminJobRequest
	batchJson, err := json.Marshal(batch)
	if err != nil {
		return err
	}

	requestData := adminjob.StartAdminJobRequest{
		Creator: creator,
		JobID:   fmt.Sprintf("%s-%s-batch-%d", string(adminjob.TransactionsBackfill), runId, batchIndex),
		Type:    string(adminjob.TransactionsBackfill),
		Input:   string(batchJson),
	}

	requestDataJson, err := json.Marshal(requestData)
	if err != nil {
		return err
	}

	// Create request
	req, err := client.NewRequest("POST", "/authed/admin/jobs", bytes.NewReader(requestDataJson))
	if err != nil {
		return err
	}

	req.Header.Add("content-type", "application/json")

	// Send request
	resp, err := client.Do(context.Background(), req, twitchclient.ReqOpts{})
	if err != nil {
		return err
	}

	defer func() {
		_ = resp.Body.Close()
	}()

	if resp.StatusCode != 200 {
		return twitchclient.HandleFailedResponse(resp)
	}

	return nil
}

func parseRow(row []string) (*transactionbackfill.Transaction, error) {
	if len(row) < 17 {
		return nil, errors.New("row was not of length 17")
	}

	datetimeSeconds, err := strconv.ParseInt(row[2], 10, 64)
	if err != nil {
		return nil, err
	}

	bitsAmount, err := strconv.ParseInt(row[6], 10, 64)
	if err != nil {
		return nil, err
	}

	isAnonymous := false
	if len(row[13]) > 0 {
		v, err := strconv.ParseBool(row[13])
		if err == nil {
			isAnonymous = v
		}
	}

	numberOfBitsSponsored := int64(0)
	if len(row[14]) > 0 {
		v, err := strconv.Atoi(row[14])
		if err == nil {
			numberOfBitsSponsored = int64(v)
		}
	}

	sponsoredAmounts := make(map[string]int64)
	if len(row[15]) > 0 {
		err := json.Unmarshal([]byte(row[15]), &sponsoredAmounts)
		if err != nil {
			return nil, err
		}
	}
	emoteUsages := make(map[string]int64)
	if len(row[16]) > 0 {
		err := json.Unmarshal([]byte(row[16]), &emoteUsages)
		if err != nil {
			return nil, err
		}
	}

	return &transactionbackfill.Transaction{
		TransactionID:         row[0],
		TransactionType:       row[1],
		DateTime:              time.Unix(datetimeSeconds, 0),
		UserID:                row[3],
		ChannelID:             row[4],
		BitsType:              row[5],
		BitsAmount:            bitsAmount,
		EventReasonCode:       row[7],
		Price:                 row[8],
		Currency:              row[9],
		Platform:              row[10],
		RawMessage:            row[11],
		TMIMessage:            row[12],
		IsAnonymous:           isAnonymous,
		NumberOfBitsSponsored: numberOfBitsSponsored,
		SponsoredAmounts:      sponsoredAmounts,
		EmoteTotals:           emoteUsages,
	}, nil
}
