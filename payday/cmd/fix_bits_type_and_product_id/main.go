package main

import (
	"bufio"
	"encoding/csv"
	"errors"
	"os"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/dynamo"
	"github.com/codegangsta/cli"
)

// Usage:
// go run main.go -e <staging|production> -f <your_transaction_id.csv>

// UPDATE THESE TO WHATEVER YOU WANT
const (
	oldBitsType = "phanto.Niantic.Pokemon Go Battle League.July2020"
	newBitsType = "phanto.Niantic.PoGo Battle League.July2020"

	oldProductID = "phanto.Niantic.Pokemon Go Battle League.July2020.100"
	newProductID = "phanto.Niantic.PoGo Battle League.July2020.100"

	erroredTransactionIDFilename = "errored_transaction_ids.csv"
)

type ErroredTransaction struct {
	TransactionID string
	Error         error
}

var environment string
var csvFileName string
var dryRun bool

func main() {
	dynamoSetup := cli.NewApp()
	dynamoSetup.Name = "Fix Bits Type and Product ID"
	dynamoSetup.Usage = "Fixes product and bits type ids"

	dynamoSetup.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "environment, e",
			Value:       "staging",
			Usage:       "Runtime config to use. Options are: 'production' and 'staging'. Defaults to 'staging' if unset.",
			Destination: &environment,
		},
		cli.StringFlag{
			Name:        "file, f",
			Usage:       "Name of file to csv.",
			Destination: &csvFileName,
		},
		cli.BoolFlag{
			Name:        "dryrun, r",
			Usage:       "Whether to run a dryrun or not.",
			Destination: &dryRun,
		},
	}

	dynamoSetup.Action = fixBitsTypeAndProductID
	err := dynamoSetup.Run(os.Args)
	if err != nil {
		panic(err)
	}
}

func fixBitsTypeAndProductID(c *cli.Context) {
	err := config.Load(environment)
	if err != nil {
		logrus.WithError(err).Panic("Rekt while loading config")
	}

	cfg := config.Get()
	if environment != config.DevelopmentEnvironment {
		err = cfg.Validate()
		if err != nil {
			logrus.WithError(err).Panic("Configuration file could not be validated")
		}
	}

	dynamoClient := dynamo.NewClient(&dynamo.DynamoClientConfig{
		AwsRegion:   cfg.DynamoRegion,
		TablePrefix: cfg.DynamoTablePrefix,
	})

	transactionsDAO := dynamo.NewTransactionsDao(dynamoClient)

	// Get transaction IDs from CSV
	csvFile, _ := os.Open(csvFileName)

	reader := csv.NewReader(bufio.NewReader(csvFile))

	logrus.Infof("Environment: %s", environment)
	logrus.Infof("File: %s", csvFileName)
	if dryRun {
		logrus.Info("Dryrun flag passed -- this script will not perform actual updates to transactions")
	}

	rows, err := reader.ReadAll()
	if err != nil {
		logrus.WithError(err).Panicf("error reading from %s", csvFileName)
	}

	numberOfTransactions := len(rows)
	logrus.WithField("NumberOfTransactions", numberOfTransactions).Info("Starting updates...")

	var erroredTransactions []ErroredTransaction
	for _, row := range rows {
		transactionID := row[0]

		err = updateTransaction(transactionsDAO, transactionID)
		if err != nil {
			logrus.WithField("transactionID", transactionID).WithError(err).Warn("errored updating transaction")
			erroredTransactions = append(erroredTransactions, ErroredTransaction{
				TransactionID: transactionID,
				Error:         err,
			})
		} else if dryRun {
			logrus.WithField("transactionID", transactionID).Info("transaction would be updated but in dryrun mode")
		} else {
			logrus.WithField("transactionID", transactionID).Info("transaction updated")
		}
	}

	if len(erroredTransactions) > 0 {
		logrus.WithField("NumberOfErroredTransactions", len(erroredTransactions)).Errorf("some transaction updates failed, saving to %s", erroredTransactionIDFilename)

		erroredTransactionsFile, err := os.Create(erroredTransactionIDFilename)
		if err != nil {
			logrus.WithError(err).Panicf("error reading from %s", erroredTransactionIDFilename)
		}
		defer erroredTransactionsFile.Close()

		writer := csv.NewWriter(erroredTransactionsFile)
		defer writer.Flush()

		for _, etid := range erroredTransactions {
			row := []string{etid.TransactionID, etid.Error.Error()}
			err := writer.Write(row)
			if err != nil {
				logrus.WithError(err).WithField("etid", etid).Panic("error writing row")
			}
		}
	}
}

func updateTransaction(transactionsDAO dynamo.ITransactionsDao, transactionID string) error {
	transaction, err := transactionsDAO.Get(transactionID)
	if err != nil {
		return err
	}

	if transaction == nil {
		return errors.New("no transaction found")
	}

	if transaction.BitsType == nil {
		logrus.WithFields(logrus.Fields{
			"transactionID": transactionID,
		}).Warn("found transaction with no bits type")
		return errors.New("no bits type")
	}

	if transaction.ProductId == nil {
		logrus.WithFields(logrus.Fields{
			"transactionID": transactionID,
		}).Warn("found transaction with no product id")
		return errors.New("no product id")
	}

	if *transaction.BitsType != oldBitsType || *transaction.ProductId != oldProductID {
		logrus.WithFields(logrus.Fields{
			"transactionID": transactionID,
			"bitsType":      *transaction.BitsType,
			"productID":     *transaction.ProductId,
		}).Warn("found transaction that does not match old bits_type or old product_id we are looking for")
		return errors.New("transaction does not match old bits_type or old product_id")
	}

	*transaction.BitsType = newBitsType
	*transaction.ProductId = newProductID

	// Don't actually perform update if dry run
	if dryRun {
		return nil
	}

	return transactionsDAO.Update(transaction)
}
