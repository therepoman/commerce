package main

import (
	"bufio"
	"bytes"
	"fmt"
	"os"
	"strings"

	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/dynamo/actions"
	"code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/campaigns"
	"code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/channel_statuses"
	"code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/eligible_channels"
	metrics_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/metrics"
	"code.justin.tv/commerce/payday/s3"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/codegangsta/cli"
	"github.com/stretchr/testify/mock"
)

const BitsAssetsBucketKey = "bits-assets"
const CDDPrefix = "https://d3aqoihi2n8ty8.cloudfront.net/"
const sponsoredActionS3KeyPrefix = "sponsored-actions"
const pollerBucket = "bits-sponsored-cheermote-campaigns"

var environment string
var environmentPrefix string
var campaignID string
var channelID string
var displayBlacklist bool

var campaignDAO campaigns.ISponsoredCheermoteCampaignDao
var channelStatusesDAO channel_statuses.ISponsoredCheermoteChannelStatusesDao
var eligibilityDAO eligible_channels.ISponsoredCheermoteEligibleChannelsDao
var actionsDAO actions.IActionDao
var s3Client s3.IS3Client

func main() {
	app := cli.NewApp()
	app.Name = "SpoValidator"
	app.Usage = "Validate Sponsored Cheermote Campaign Configuration"

	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "environment",
			Value:       "staging",
			Usage:       "Runtime config to use. Options are: 'production' and 'staging'. Defaults to 'staging' if unset.",
			EnvVar:      "ENVIRONMENT",
			Destination: &environment,
		},
		cli.StringFlag{
			Name:        "environmentPrefix",
			Value:       "test",
			EnvVar:      "ENVIRONMENT_PREFIX",
			Destination: &environmentPrefix,
		},
		cli.StringFlag{
			Name:        "campaignID",
			Usage:       "Campaign ID of the sponsored cheermote campaign",
			EnvVar:      "CAMPIGN_ID",
			Destination: &campaignID,
		},
		cli.BoolFlag{
			Name:        "displayBlacklist",
			Usage:       "Display all user IDs in the campaign blacklist",
			EnvVar:      "DISPLAY_BLACKLIST",
			Destination: &displayBlacklist,
		},
		cli.StringFlag{
			Name:        "channelID",
			Usage:       "Channel ID of the sponsored cheermote campaign",
			EnvVar:      "CHANNEL_ID",
			Destination: &channelID,
		},
	}

	app.Action = action
	err := app.Run(os.Args)
	if err != nil {
		panic(err)
	}
}

func action(ctx *cli.Context) {
	dynamoClient := dynamo.NewClient(&dynamo.DynamoClientConfig{
		AwsRegion:   "us-west-2",
		TablePrefix: environmentPrefix,
	})
	campaignDAO = campaigns.NewSponsoredCheermoteCampaignDao(dynamoClient)
	channelStatusesDAO = channel_statuses.NewSponsoredCheermoteChannelStatusesDao(dynamoClient)
	eligibilityDAO = eligible_channels.NewSponsoredCheermoteEligibleChannelsDao(dynamoClient)

	statter := new(metrics_mock.Statter)
	statter.On("TimingDuration", mock.Anything, mock.Anything)
	actionsDAO = &actions.ActionDao{
		BaseDao: dynamo.NewBaseDao(&actions.ActionsTable{}, dynamoClient),
		Statter: statter,
	}
	s3Client = s3.NewFromConfig(&aws.Config{
		Region: aws.String("us-west-2"),
	})

	// check campaign
	campaign, err := campaignDAO.Get(campaignID)
	if err != nil {
		fmt.Println(err)
		return
	}

	if campaign == nil {
		fmt.Printf("No campaign is found in the campaigns table for ID %s\n", campaignID)
		return
	}

	PrintCampaignDetails(campaign)

	// check blacklist/whitelist
	if len(campaign.ChannelBlacklist) > 0 {
		fmt.Printf("Campaign uses a blacklist with %d entries", len(campaign.ChannelBlacklist))

		if displayBlacklist {
			for tuid := range campaign.ChannelBlacklist {
				fmt.Printf("\t\t%s", tuid)
			}
		}
	} else {
		fmt.Printf("Campaign uses whitelists, checking whitelist for channel %s\n", channelID)
		// check channel statuses
		fmt.Printf("Checking channel statuses...")
		channelStatuses, err := channelStatusesDAO.Get(channelID, campaignID)
		if err != nil {
			fmt.Println(err)
			return
		}

		if channelStatuses == nil {
			fmt.Println("No channel statuses record is configured for channel")
			return
		}
		fmt.Println("OK")

		// check eligible channels
		fmt.Printf("Checking channel eligibility...")
		eligibility, err := eligibilityDAO.Get(channelID, campaignID)
		if err != nil {
			fmt.Println(err)
			return
		}

		if eligibility == nil {
			fmt.Println("No eligibility record is configured for channel")
			return
		}
		fmt.Println("OK")
	}

	// check brand image
	fmt.Printf("Checking brand image exists in S3...")
	if len(campaign.BrandImageURL) == 0 {
		fmt.Println("Brand image URL is unset")
		return
	}

	brandImageKey := strings.ReplaceAll(campaign.BrandImageURL, CDDPrefix, "")
	brandImageExists, err := s3Client.FileExists(BitsAssetsBucketKey, brandImageKey)
	if err != nil {
		fmt.Println(err)
		return
	}
	if !brandImageExists {
		fmt.Println("Brand image asset does not exist in S3")
		return
	}

	fmt.Println("OK")

	// check cheermote assets
	fmt.Print("Checking cheermote assets folder exists in S3...")
	if len(campaign.Prefix) == 0 {
		fmt.Println("Campaign Cheermote Prefix is unset")
		return
	}

	cheermoteAssetsFolderExists, err := s3Client.FileExists(BitsAssetsBucketKey, fmt.Sprintf("%s/%s", sponsoredActionS3KeyPrefix, campaign.Prefix))
	if err != nil {
		fmt.Println(err)
		return
	}

	if !cheermoteAssetsFolderExists {
		fmt.Println("Sponsored Cheermote assets folder does not exist in S3")
		return
	}
	fmt.Println("OK")

	// check action
	fmt.Print("Checking action...")
	action, err := actionsDAO.Get(campaign.Prefix)
	if err != nil {
		fmt.Println(err)
		return
	}

	if action == nil {
		fmt.Println("No action is configured for the prefix")
	}
	fmt.Println("OK")

	// check poller
	fmt.Print("Checking poller config file...")
	pollerKey := fmt.Sprintf("%s-campaigns", environment)
	pollerConfig, err := s3Client.LoadFile(pollerBucket, pollerKey)
	if err != nil {
		fmt.Println(err)
		return
	}

	scanner := bufio.NewScanner(bytes.NewReader(pollerConfig))
	pollerConfigFound := false
	for scanner.Scan() {
		if strings.Contains(scanner.Text(), campaignID) {
			pollerConfigFound = true
			break
		}
	}

	if !pollerConfigFound {
		fmt.Println("Campaign ID does not exist in poller config file")
		return
	}
	fmt.Println("OK")

	fmt.Println("Complete!")
}

func PrintCampaignDetails(campaign *campaigns.SponsoredCheermoteCampaign) {
	fmt.Printf("ID: %s\n", campaign.CampaignID)
	fmt.Printf("Enabled: %t\n", campaign.IsEnabled)
	fmt.Printf("Start: %v\n", campaign.StartTime)
	fmt.Printf("End: %+v\n", campaign.EndTime)
	fmt.Printf("Duration: %+v\n", campaign.EndTime.Sub(campaign.StartTime))
	fmt.Printf("LastUpdated: %+v\n", campaign.LastUpdated)
	fmt.Printf("Prefix: %s\n", campaign.Prefix)
	fmt.Printf("Minimum Cheer: %d\n", campaign.MinBitsToBeSponsored)
	fmt.Printf("Match: %+v\n", campaign.SponsoredAmountThresholds)
	fmt.Printf("Total Bits: %d\n", campaign.TotalBits)
	fmt.Printf("Blacklist Size: %d\n", len(campaign.ChannelBlacklist))
}
