package main

import (
	"bufio"
	"fmt"
	"os"
	"os/user"
	"path/filepath"
	"time"

	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/dynamo"
	fileUtil "code.justin.tv/commerce/payday/utils/close"
	"code.justin.tv/commerce/payday/utils/pointers"
	"github.com/codegangsta/cli"
)

const staging = "staging"

func main() {
	app := cli.NewApp()

	gopath := os.Getenv("GOPATH")
	paydayDir := fmt.Sprintf("%s/src/code.justin.tv/commerce/payday", gopath)

	app.Flags = []cli.Flag{

		cli.StringFlag{
			Name:  "dir, d",
			Usage: "Your payday directory",
			Value: paydayDir,
		},

		cli.StringFlag{
			Name:  "environment, e",
			Usage: "The payday environment to emulate",
			Value: staging,
		},
	}

	app.Action = action
	err := app.Run(os.Args)
	if err != nil {
		panic(err)
	}
}

func action(ctx *cli.Context) {
	argument := ctx.Args().Get(0)
	absolutePath, err := filepath.Abs(argument)
	if err != nil {
		panic(err)
	}

	f, err := os.Open(absolutePath)
	if err != nil {
		panic(err)
	}
	defer fileUtil.Close(f)

	paydayDir := ctx.String("dir")
	payday, err := os.Stat(paydayDir)
	if err != nil {
		panic(err)
	}

	if !payday.IsDir() {
		panic(fmt.Sprintf("The specified directory %s is not a directory", paydayDir))
	}

	fmt.Printf("Changing working directory to %s\n", paydayDir)
	err = os.Chdir(paydayDir)
	if err != nil {
		panic(err)
	}

	environment := ctx.String("e")
	err = config.Load(environment)
	if err != nil {
		panic(err)
	}

	cfg := config.Get()
	if err = cfg.Validate(); err != nil {
		panic(err)
	}

	dynamoClient := dynamo.NewClient(&dynamo.DynamoClientConfig{
		AwsRegion:   cfg.DynamoRegion,
		TablePrefix: cfg.DynamoTablePrefix,
	})

	channelDao := dynamo.NewChannelDao(dynamoClient)

	scanner := bufio.NewScanner(f)
	now := time.Now()
	u, err := user.Current()
	if err != nil {
		panic(err)
	}

	for scanner.Scan() {
		if err := scanner.Err(); err != nil {
			panic(err)
		}

		channelID := scanner.Text()

		originalChannel, err := channelDao.Get(dynamo.ChannelId(channelID))
		if err != nil {
			panic(err)
		}

		if !pointers.BoolOrDefault(originalChannel.Onboarded, false) {
			fmt.Printf("Enabling channel %s\n", channelID)
			channel := dynamo.Channel{
				Id:         dynamo.ChannelId(channelID),
				Annotation: pointers.StringP(fmt.Sprintf("Enabled by %s on %s", u.Username, now.Format(time.RFC822))),
				Onboarded:  pointers.BoolP(true),
			}

			err := channelDao.Update(&channel)
			if err != nil {
				panic(err)
			}
		} else {
			fmt.Printf("Found channel %s that was already onboarded. Skipping.\n", channelID)
		}
	}

	fmt.Println("Successfully enabled all users")
}
