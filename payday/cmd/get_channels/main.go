package main

import (
	"os"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/dynamo"
	"github.com/codegangsta/cli"
)

var environment string

func main() {
	dynamoSetup := cli.NewApp()
	dynamoSetup.Name = "Payday Get Channels"
	dynamoSetup.Usage = "Makes it rain (by getting all channels from dynamo)"

	dynamoSetup.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "environment, e",
			Value:       "staging",
			Usage:       "Runtime config to use. Options are: 'production', 'staging', and development. Defaults to 'development' if unset.",
			Destination: &environment,
		},
	}

	dynamoSetup.Action = getChannels
	err := dynamoSetup.Run(os.Args)
	if err != nil {
		panic(err)
	}
}

func getChannels(c *cli.Context) {
	log.Infof("Loading config file")
	err := config.Load(c.String("e"))
	if err != nil {
		panic(err)
	}

	cfg := config.Get()
	if err := cfg.Validate(); err != nil {
		log.Panicf("Rekt while loading configuration file: %v", err)
	}

	dynamoClient := dynamo.NewClient(&dynamo.DynamoClientConfig{
		AwsRegion:   cfg.DynamoRegion,
		TablePrefix: cfg.DynamoTablePrefix,
	})

	channelDao := dynamo.NewChannelDao(dynamoClient)
	channels, err := channelDao.GetAll()

	if err != nil {
		log.Panic("Failed to get all from dynamo")
	}

	onboardCount := 0

	recentOnCount := 0
	recentOffCount := 0
	recentNilCount := 0

	topOnCount := 0
	topOffCount := 0
	topNilCount := 0

	for _, channel := range channels {
		if channel.Onboarded != nil && *channel.Onboarded {
			onboardCount++

			if channel.PinRecentCheers != nil {
				if *channel.PinRecentCheers {
					recentOnCount++
				} else {
					recentOffCount++
				}
			} else {
				recentNilCount++
			}

			if channel.PinTopCheers != nil {
				if *channel.PinTopCheers {
					topOnCount++
				} else {
					topOffCount++
				}
			} else {
				topNilCount++
			}
		}
	}

	log.Infof("Found %d records", len(channels))
	log.Infof("Found %d onboarded", onboardCount)

	log.Infof("Found %d recent on", recentOnCount)
	log.Infof("Found %d recent off", recentOffCount)
	log.Infof("Found %d recent nil", recentNilCount)

	log.Infof("Found %d top on", topOnCount)
	log.Infof("Found %d top off", topOffCount)
	log.Infof("Found %d top nil", topNilCount)
}
