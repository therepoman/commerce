package main

import (
	"bufio"
	"encoding/csv"
	"encoding/json"
	"io"
	"os"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/models"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/codegangsta/cli"
)

const (
	BATCH_SIZE = 100

	PACHINKO_PERSEUS_QUEUE_STAGING = "https://sqs.us-west-2.amazonaws.com/021561903526/payday_pachinko_perseus_queues_staging"
	PACHINKO_PERSEUS_QUEUE_PROD    = "https://sqs.us-west-2.amazonaws.com/021561903526/payday_pachinko_perseus_queues"
)

var inputFile string
var env string
var sqsURL string
var sqsClient *sqs.SQS

func main() {
	script := cli.NewApp()
	script.Name = "Perseus balance notification backfill script"
	script.Usage = "Backfill"

	script.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "inputFile, i",
			Usage:       "the input file with transaction ids to backfill",
			EnvVar:      "INPUT_FILE",
			Destination: &inputFile,
		},
		cli.StringFlag{
			Name:        "environment, e",
			Usage:       "the environment in which the backfill occurs. Either 'staging' or 'prod' (default to staging)",
			Value:       "staging",
			Destination: &env,
		},
	}

	script.Action = run
	err := script.Run(os.Args)
	if err != nil {
		logrus.WithError(err).Error("Run returned an err")
	}

}

func run(c *cli.Context) {
	logrus.SetOutput(os.Stdout)

	sess := session.Must(session.NewSessionWithOptions(session.Options{
		SharedConfigState: session.SharedConfigEnable,
	}))
	sqsClient = sqs.New(sess)

	sqsURL = PACHINKO_PERSEUS_QUEUE_STAGING
	if env == "prod" {
		sqsURL = PACHINKO_PERSEUS_QUEUE_PROD
	}

	csvFile, _ := os.Open(inputFile)
	csvReader := csv.NewReader(bufio.NewReader(csvFile))
	counter := 0
	batchCounter := 0
	batchTransactionIDs := []string{}

	logrus.Infof("starting the backfill")
	logrus.Infof("env: %s", env)
	logrus.Infof("target queue: %s", sqsURL)
	for {
		row, err := csvReader.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			logrus.WithError(err).Errorf("failed to read transactionid at transaction counter: %d", counter)
			return
		}
		if len(row) < 1 {
			logrus.Errorf("transactionid is not present in the input file at transaction counter: %d", counter)
			return
		}

		transactionID := row[0]
		batchTransactionIDs = append(batchTransactionIDs, transactionID)
		counter++
		if len(batchTransactionIDs) == BATCH_SIZE {
			batchCounter++
			err = send(batchTransactionIDs)
			if err != nil {
				logrus.Infof("failed to send at counter: %d, batch counter: %d", counter, batchCounter)
				return
			}

			batchTransactionIDs = []string{}
			logrus.Infof("%d records send", counter)
		}
	}

	if len(batchTransactionIDs) > 0 {
		batchCounter++
		err := send(batchTransactionIDs)
		if err != nil {
			logrus.Infof("failed to send at counter: %d, batch counter: %d", counter, batchCounter)
			return
		}
		logrus.Infof("%d remaining records sent", len(batchTransactionIDs))
	}

	logrus.Infof("finished. %d records sent with %d batches", counter, batchCounter)
}

func send(transactionIDs []string) error {
	pachinkoMsg := models.PachinkoSNSUpdate{
		TransactionIDs: transactionIDs,
	}
	pachinkoMsgJson, err := json.Marshal(&pachinkoMsg)
	if err != nil {
		logrus.WithError(err).Error("failed to marshal pachinko msg")
		return err
	}

	snsMessage := models.SNSMessage{
		Message: string(pachinkoMsgJson),
	}
	snsMessageJson, err := json.Marshal(&snsMessage)
	if err != nil {
		logrus.WithError(err).Error("failed to marshal sns msg")
		return err
	}

	_, err = sqsClient.SendMessage(&sqs.SendMessageInput{
		QueueUrl:    aws.String(sqsURL),
		MessageBody: aws.String(string(snsMessageJson)),
	})

	return err
}
