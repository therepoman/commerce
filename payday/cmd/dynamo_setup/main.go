package main

import (
	"os"

	"strings"

	"fmt"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/audit"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/iam"
	"code.justin.tv/commerce/payday/lambda"
	"code.justin.tv/commerce/payday/s3"
	"github.com/codegangsta/cli"
)

var environment string

func main() {
	dynamoSetup := cli.NewApp()
	dynamoSetup.Name = "Payday Dynamo Setup"
	dynamoSetup.Usage = "Makes it rain (by setting up dynamo stuff)"

	dynamoSetup.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "environment, e",
			Value:       "development",
			Usage:       "Runtime config to use. Options are: 'production', 'staging', and development. Defaults to 'development' if unset.",
			EnvVar:      "payday_APP_ENV",
			Destination: &environment,
		},
	}

	dynamoSetup.Action = setupPaydayDynamo
	err := dynamoSetup.Run(os.Args)
	if err != nil {
		panic(err)
	}
}

func setupPaydayDynamo(c *cli.Context) {
	log.Infof("Setting up Payday dynamo")

	log.Infof("Loading config file")
	err := config.Load(c.String("e"))
	if err != nil {
		log.Panicf("Error while loading configuration file: %v", err)
	}
	cfg := config.Get()

	if err := cfg.Validate(); err != nil {
		log.Panicf("Rekt while validating configuration file: %v", err)
	}

	s3Client := s3.NewFromDefaultConfig()
	lambdaClient := lambda.NewFromDefaultConfig()
	iamClient := iam.NewFromDefaultConfig()
	auditSetup := audit.NewAuditSetup(s3Client, lambdaClient, iamClient)

	dynamoClient := dynamo.NewClientWithAudit(&dynamo.DynamoClientConfig{
		AwsRegion:   cfg.DynamoRegion,
		TablePrefix: cfg.DynamoTablePrefix,
	}, auditSetup, &dynamo.DynamoAuditConfig{
		RecordsS3Bucket:      cfg.AuditRecordsS3Bucket,
		LambdaMemorySizeMB:   cfg.AuditLambdaMemorySizeMB,
		LambdaTimeoutSec:     cfg.AuditLambdaTimeoutSec,
		LambdaRoleName:       cfg.AuditLambdaRoleName,
		LambdaCodeS3Bucket:   cfg.AuditLambdaCodeS3Bucket,
		LambdaEventBatchSize: cfg.AuditLambdaEventBatchSize,
	})

	fmt.Print(dynamoClient)

	//TODO: Create your DAO here with dynamo.New{{Name of your Dao}}Dao()

	currentUser, err := iamClient.GetCurrentUser()
	if err != nil {
		log.Panicf("Error getting current AWS user: %v", err)
	}

	expectedAccount := "arn:aws:iam::021561903526" // twitch-bits-aws
	if !strings.HasPrefix(currentUser, expectedAccount) {
		log.Errorf("Current user [%s], Expected account [%s]", currentUser, expectedAccount)
		return
	}

	log.Infof("Setting up dynamo tables")

	//TODO: Create your dynamo table by calling {{nameOfYourDao}}.SetupTable()

}
