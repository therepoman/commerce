package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"time"

	log "code.justin.tv/commerce/logrus"
	makoRPC "code.justin.tv/commerce/mako/twirp"
	"code.justin.tv/commerce/payday/clients/mako"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/dynamo/badge_tier_emote_groupids"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/codegangsta/cli"
	"golang.org/x/time/rate"
)

// CLI Flags, detail for each flag can be found in main()
var (
	environment         string
	dryrun              bool
	forceGroupIDRefetch bool
)

// Limit how fast we are querying Mako's , this is disabled for dry run
const maxGetEmoticonsByGroupsTPS rate.Limit = 100
const groupIDsBatchSize = 10
const savedListFilename = "saved_bter_groups_%s.json"

var emoteGroupEntitlementLimiter = rate.NewLimiter(maxGetEmoticonsByGroupsTPS, 1)

func main() {
	app := cli.NewApp()
	app.Name = "BTER Emotes Investigation"
	app.Usage = "Investigate BTER emotes for all BTER Group IDs"

	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "environment, e",
			Value:       "staging",
			Usage:       "Runtime config to use. Options are: 'production' and 'staging'. Defaults to 'staging' if unset.",
			EnvVar:      "environment",
			Destination: &environment,
		},
		cli.BoolFlag{
			Name:        "dryrun",
			Usage:       "Runtime config to use. If this flag is given, will do a dry run without SNS publish and its associated rate limiting",
			EnvVar:      "dryrun",
			Destination: &dryrun,
		},
		cli.BoolFlag{
			Name:        "forceGroupIDRefetch, fresh",
			Usage:       "Runtime config to use. If this flag is given, will always fetch new group IDs from Dynamo even if one was previously saved",
			EnvVar:      "forceGroupIDRefetch",
			Destination: &forceGroupIDRefetch,
		},
	}

	app.Action = runProgram
	err := app.Run(os.Args)
	if err != nil {
		panic(err)
	}
}

func runProgram(c *cli.Context) {
	ctx := context.Background()
	log.Infof("Loading config file")

	err := config.Load(environment)
	if err != nil {
		log.Panicf("Error while loading configuration file: %v", err)
	}
	log.Infof("[OPTIONS] Environment: %v", environment)
	log.Infof("[OPTIONS] Dry Run: %v", dryrun)
	log.Infof("[OPTIONS] Force fetch fresh Group IDs from Dynamo: %v", forceGroupIDRefetch)

	cfg := config.Get()
	if err := cfg.Validate(); err != nil {
		log.Panicf("Rekt while validating configuration file: %v", err)
	}

	var awsRegion string
	if environment == "production" {
		// we have a backup prod table on us-east, this is the one we want to scan off of
		awsRegion = "us-east-2"
	} else {
		// our staging table is in us-west only
		awsRegion = "us-west-2"
	}

	log.Infof("Setting up aws session")
	sess, err := session.NewSession(&aws.Config{
		Region: aws.String(awsRegion),
	})
	if err != nil {
		log.WithError(err).Panic("Could not start new aws session")
	}

	log.Infof("Setting up Dynamo DAO")
	dao := badge_tier_emote_groupids.NewBadgeTierEmoteGroupIDDAO(sess, cfg)

	log.Infof("Setting up Mako Client")
	makoClient := mako.NewClient(cfg)

	log.Info("Getting BTER Group IDs")
	var bterGroups []*badge_tier_emote_groupids.BadgeTierEmoteGroupID
	invalidProgressWarning := "If you have previous progress, this could panic if progress file does not match up."

	start := time.Now()
	if !forceGroupIDRefetch {
		bterGroups, err = getGroupIDsFromSavedList(environment)
		if err != nil {
			log.Warnf("Failed to get Group IDs from SavedList, fetching from Dynamo. %s", invalidProgressWarning)
			bterGroups = getBTERGroupsFromDynamo(ctx, dao)
			saveGroupIDsToSavedList(environment, bterGroups)
		}
	} else {
		log.Warnf("You are force fetching group IDs from Dynamo. %s", invalidProgressWarning)
		bterGroups = getBTERGroupsFromDynamo(ctx, dao)
		saveGroupIDsToSavedList(environment, bterGroups)
	}
	log.Infof("BTER Group fetching took %s", time.Since(start))

	var bterGroupsFiltered []*badge_tier_emote_groupids.BadgeTierEmoteGroupID
	for _, bterGroup := range bterGroups {
		if bterGroup.Threshold != 1 && bterGroup.Threshold != 100 {
			bterGroupsFiltered = append(bterGroupsFiltered, bterGroup)
		}
	}

	log.Infof("Setting up Progress Tracker")
	progressTracker := NewProgressTracker(bterGroupsFiltered, ProgressTrackerConfig{
		environment: environment,
		dryrun:      dryrun,
	})

	for i := progressTracker.GetProgress(); i < len(bterGroupsFiltered); i += groupIDsBatchSize {
		topIndex := i + groupIDsBatchSize
		if topIndex >= len(bterGroupsFiltered) {
			topIndex = len(bterGroupsFiltered) - 1
		}
		bterGroupsBatch := bterGroupsFiltered[i:topIndex]

		var groupIDsBatch []string
		for _, bterGroup := range bterGroupsBatch {
			groupIDsBatch = append(groupIDsBatch, bterGroup.GroupID)
		}
		if dryrun {
			log.WithField("groupIDs", groupIDsBatch).Infof("(%d/%d) Dryrun: won't send GetEmoticonsByGroups request to Mako", i, len(bterGroupsFiltered))
		} else {
			log.WithField("groupIDs", groupIDsBatch).Infof("(%d/%d) Real Run: sending GetEmoticonsByGroups request to Mako and saving progress", i, len(bterGroupsFiltered))
			_ = emoteGroupEntitlementLimiter.Wait(ctx)
			emoticonGroups, err := makoClient.GetEmoticonsByGroups(ctx, groupIDsBatch, []makoRPC.EmoteState{
				makoRPC.EmoteState_active,
				makoRPC.EmoteState_pending,
			})
			if err != nil {
				log.WithError(err).Fatal("error calling Mako's GetEmoticonsByGroups endpoint")
			}
			saveProgress(progressTracker, bterGroupsBatch, emoticonGroups)
		}
	}

	log.WithField("count", len(bterGroups)).Info("Total Number of BTER Groups")
	log.WithField("count", len(bterGroupsFiltered)).Info("Total Number of BTER Groups above thresholds")
	if !dryrun {
		log.WithField("count", progressTracker.GetTotalGroupsWithMultipleEmotes()).Info("Total Groups With Multiple Emotes")
	}
}

func saveProgress(progressTracker *ProgressTracker, bterGroups []*badge_tier_emote_groupids.BadgeTierEmoteGroupID, emoticonGroups []makoRPC.EmoticonGroup) {
	emoticonGroupsLookup := make(map[string][]*makoRPC.Emoticon)
	for _, emoticonGroup := range emoticonGroups {
		emoticonGroupsLookup[emoticonGroup.Id] = emoticonGroup.Emoticons
	}

	countGroupsWithMultipleEmotes := 0
	for _, group := range bterGroups {
		var numOfEmotes int
		if emoticons, ok := emoticonGroupsLookup[group.GroupID]; ok {
			numOfEmotes = len(emoticons)
		}
		if numOfEmotes > 1 {
			countGroupsWithMultipleEmotes++
		}
		err := progressTracker.SaveProgress(group, numOfEmotes)
		if err != nil {
			log.WithError(err).Errorf("Saving progress failed for group %v", group.GroupID)
		}
	}
}

func saveGroupIDsToSavedList(environment string, groupIDs []*badge_tier_emote_groupids.BadgeTierEmoteGroupID) {
	dir, err := os.Getwd()
	if err != nil {
		panic(err)
	}

	dest := path.Join(dir, fmt.Sprintf(savedListFilename, environment))
	f, err := os.OpenFile(dest, os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		panic(err)
	}
	defer f.Close()

	messageJSON, err := json.Marshal(groupIDs)
	if err != nil {
		panic(err)
	}

	_, err = f.WriteString(string(messageJSON))
	if err != nil {
		panic(err)
	}
}

func getGroupIDsFromSavedList(environment string) ([]*badge_tier_emote_groupids.BadgeTierEmoteGroupID, error) {
	dir, err := os.Getwd()
	if err != nil {
		return nil, err
	}

	dest := path.Join(dir, fmt.Sprintf(savedListFilename, environment))
	f, err := os.OpenFile(dest, os.O_RDONLY, 0666)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	byteValue, _ := ioutil.ReadAll(f)

	var groupIDs []*badge_tier_emote_groupids.BadgeTierEmoteGroupID
	err = json.Unmarshal([]byte(byteValue), &groupIDs)
	if err != nil {
		return nil, err
	}

	return groupIDs, nil
}

func getBTERGroupsFromDynamo(ctx context.Context, dao badge_tier_emote_groupids.BadgeTierEmoteGroupIDDAO) []*badge_tier_emote_groupids.BadgeTierEmoteGroupID {
	log.Infof("Fetching BTER groups from DynamoDB")
	// bterGroups, err := dao.GetAll(ctx)
	// Digital assets team ran a backfill to fill in unused BTER groups, we are not interested in these
	bterGroups, err := dao.GetAllWithFilter(ctx, "created < ?", "2020-10-07T07:00:00.000000-07:00")
	if err != nil {
		log.WithError(err).Panic("Error getting BTER groups from DynamoDB")
	}
	log.Infof("Number of BTER groups found: %d\n", len(bterGroups))
	return bterGroups
}
