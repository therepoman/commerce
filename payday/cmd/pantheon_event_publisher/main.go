package main

import (
	"context"
	"net/http"
	"os"
	"time"

	"strconv"

	"fmt"

	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	"code.justin.tv/commerce/payday/config"

	log "code.justin.tv/commerce/logrus"
	"github.com/codegangsta/cli"
	"github.com/gofrs/uuid"
)

const (
	DEFAULT_DOMAIN           = "pantheon-data-generator"
	DEFAULT_GROUPING_KEY_NUM = 10
	DEFAULT_ENTRY_KEY_NUM    = 5
)

var environment string
var domain string
var groupingKeyNum int
var entryKeyNum int

func main() {
	pantheonDataGen := cli.NewApp()
	pantheonDataGen.Name = "Pantheon Event Publisher"
	pantheonDataGen.Usage = "Publishes data to Pantheon to generate leaderboards and leaderboard entries. Entries are populated with values to create fake leaderboards."
	pantheonDataGen.Action = publishPantheonData

	pantheonDataGen.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "environment, e",
			Value:       config.DevelopmentEnvironment,
			Usage:       fmt.Sprintf("Runtime config to use. Options are: '%v', '%v', and '%v'. Defaults to '%v' if unset.", config.DevelopmentEnvironment, config.StagingEnvironment, config.ProductionEnvironment, config.DevelopmentEnvironment),
			Destination: &environment,
		},
		cli.StringFlag{
			Name:        "domain, d",
			Value:       DEFAULT_DOMAIN,
			Usage:       fmt.Sprintf("Pantheon domain. Useful to set if debugging. Defaults to '%v' if unset.", DEFAULT_DOMAIN),
			Destination: &domain,
		},
		cli.IntFlag{
			Name:        "groupingKeyNum, g",
			Value:       DEFAULT_GROUPING_KEY_NUM,
			Usage:       fmt.Sprintf("Number of grouping keys to create. Defaults to '%v' if unset", DEFAULT_GROUPING_KEY_NUM),
			Destination: &groupingKeyNum,
		},
		cli.IntFlag{
			Name:        "entryKeyNum, n",
			Value:       DEFAULT_ENTRY_KEY_NUM,
			Usage:       fmt.Sprintf("Number of grouping keys to create. Defaults to '%v' if unset", DEFAULT_ENTRY_KEY_NUM),
			Destination: &entryKeyNum,
		},
	}

	err := pantheonDataGen.Run(os.Args)
	if err != nil {
		log.WithError(err).Error("Run returned an err")
	}
}

func publishPantheonData(c *cli.Context) {
	log.Infof("Starting Pantheon Event Publisher in " + environment)

	if environment != config.ProductionEnvironment && (groupingKeyNum*entryKeyNum > DEFAULT_GROUPING_KEY_NUM*DEFAULT_ENTRY_KEY_NUM) {
		log.Warnf("Large grouping key (%v) and entry key (%v) detected in a non-prod environment. This may take some time for Pantheon to process.", groupingKeyNum, entryKeyNum)
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	err := config.Load(environment)
	if err != nil {
		panic(err)
	}

	configuration := config.Get()

	now := time.Now().UnixNano()
	pantheonClient := pantheonrpc.NewPantheonProtobufClient(configuration.PantheonServiceEndpoint, http.DefaultClient)

	for i := 0; i < groupingKeyNum; i++ {
		groupingKey := "grouping-key-" + strconv.Itoa(i)

		for j := 0; j < entryKeyNum; j++ {
			value := 100 + (j * 100)
			entryKey := "entry-key-" + strconv.Itoa(value)

			eventIDUUID, err := uuid.NewV4()
			if err != nil {
				log.WithError(err).Panic("Error generating UUID")
			}

			req := &pantheonrpc.PublishEventReq{
				Domain:      domain,
				EventId:     eventIDUUID.String(),
				TimeOfEvent: uint64(now),
				GroupingKey: groupingKey,
				EntryKey:    entryKey,
				EventValue:  int64(value),
			}

			log.Infof("Sending Publish Event Request to Pantheon: %+v", req)

			_, err = pantheonClient.PublishEvent(ctx, req)
			if err != nil {
				log.WithError(err).Error("Error calling Pantheon.")
			} else {
				log.Debugf("200 response from Pantheon.")
			}
		}

		log.Debugf("Created leaderboard for grouping key: %v", groupingKey)
	}
}
