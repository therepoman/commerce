package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
	"testing"
	"time"

	"code.justin.tv/commerce/payday/dynamo/badge_tier_emote_groupids"
	. "github.com/smartystreets/goconvey/convey"
)

var (
	testGroupIDs = []*badge_tier_emote_groupids.BadgeTierEmoteGroupID{
		{
			ChannelID:    "some-channel-id",
			Threshold:    1000,
			GroupID:      "some-group-id",
			CreatedAt:    time.Now(),
			IsBackfilled: false,
		},
		{
			ChannelID:    "some-channel-id-1",
			Threshold:    10000,
			GroupID:      "some-group-id-1",
			CreatedAt:    time.Now(),
			IsBackfilled: true,
		},
		{
			ChannelID:    "some-channel-id-2",
			Threshold:    100000,
			GroupID:      "some-group-id-2",
			CreatedAt:    time.Now(),
			IsBackfilled: false,
		},
	}
)

func TestNewProgressTracker(t *testing.T) {
	Convey("Struct initializes with all fields defined", t, func() {
		testProgressTracker := NewProgressTracker(testGroupIDs, ProgressTrackerConfig{
			environment:          "testing",
			dryrun:               false,
			useSpecifiedChannels: false,
		})
		So(testProgressTracker.progressFileName, ShouldNotBeNil)
		So(testProgressTracker.groupIDs, ShouldNotBeNil)
		So(testProgressTracker.totalItems, ShouldNotBeNil)
		So(testProgressTracker.progress, ShouldNotBeNil)
	})
}

func TestProgressTracker_SaveProgress(t *testing.T) {
	Convey("Given Progress Tracker", t, func() {
		testProgressTracker := NewProgressTracker(testGroupIDs, ProgressTrackerConfig{
			environment:          "testing",
			dryrun:               false,
			useSpecifiedChannels: false,
		})
		testProgressFileName := testProgressTracker.progressFileName

		Convey("When SaveProgress is called, it should create a blank file if no previous file exists, and append progress to it.", func() {
			err := testProgressTracker.SaveProgress(testGroupIDs[0], false)
			So(err, ShouldBeNil)
			So(progressFileExists(testProgressFileName), ShouldEqual, true)
			So(getLastGroupIDFromProgressFile(testProgressFileName), ShouldEqual, testGroupIDs[0].GroupID)

			err = testProgressTracker.SaveProgress(testGroupIDs[1], false)
			So(err, ShouldBeNil)
			So(getLastGroupIDFromProgressFile(testProgressFileName), ShouldEqual, testGroupIDs[1].GroupID)

			err = testProgressTracker.SaveProgress(testGroupIDs[2], true)
			So(err, ShouldBeNil)
			So(getLastGroupIDFromProgressFile(testProgressFileName), ShouldEqual, testGroupIDs[2].GroupID)

			// clean up
			os.Remove(getFilePath(testProgressFileName))
			So(progressFileExists(testProgressFileName), ShouldEqual, false)
		})
		Convey("When group that's passed in does not match what's expected from the dataset, it will panic", func() {
			So(func() {
				err := testProgressTracker.SaveProgress(testGroupIDs[2], false)
				if err != nil {
					fmt.Print(err)
				}
			}, ShouldPanic)
		})
	})
}

func TestProgressTracker_buildProgressFileName(t *testing.T) {
	Convey("Different combinations of params produce correct results", t, func() {
		So(buildProgressFileName("staging", true, false), ShouldEqual, "progress_staging_dryrun.csv")
		So(buildProgressFileName("production", true, false), ShouldEqual, "progress_production_dryrun.csv")
		So(buildProgressFileName("staging", false, false), ShouldEqual, "progress_staging.csv")
		So(buildProgressFileName("staging", false, true), ShouldEqual, "progress_staging_specified_channels.csv")
	})
}

func TestProgressTracker_getSavedProgressFromFile(t *testing.T) {
	Convey("When no progress file exists, should return 0", t, func() {
		testProgressFileName := "some-file.csv"
		So(progressFileExists(testProgressFileName), ShouldEqual, false)
		So(getSavedProgressFromFile(testProgressFileName, testGroupIDs), ShouldEqual, 0)
	})
	Convey("When progress validates, should return the right progress count", t, func() {
		testProgressTracker := NewProgressTracker(testGroupIDs, ProgressTrackerConfig{
			environment:          "testing",
			dryrun:               false,
			useSpecifiedChannels: false,
		})
		testProgressFileName := testProgressTracker.progressFileName

		So(progressFileExists(testProgressFileName), ShouldEqual, false)

		err := testProgressTracker.SaveProgress(testGroupIDs[0], false)
		So(err, ShouldBeNil)
		So(progressFileExists(testProgressFileName), ShouldEqual, true)
		So(getSavedProgressFromFile(testProgressFileName, testGroupIDs), ShouldEqual, 1)

		err = testProgressTracker.SaveProgress(testGroupIDs[1], false)
		So(err, ShouldBeNil)
		So(getSavedProgressFromFile(testProgressFileName, testGroupIDs), ShouldEqual, 2)

		// clean up
		os.Remove(getFilePath(testProgressFileName))
		So(progressFileExists(testProgressFileName), ShouldEqual, false)
	})
	Convey("When progress does not validate, should panic to encourage manual inspection", t, func() {
		testProgressTracker := NewProgressTracker(testGroupIDs, ProgressTrackerConfig{
			environment:          "testing",
			dryrun:               false,
			useSpecifiedChannels: false,
		})
		testProgressFileName := testProgressTracker.progressFileName

		So(progressFileExists(testProgressFileName), ShouldEqual, false)
		err := testProgressTracker.SaveProgress(testGroupIDs[0], false)
		So(err, ShouldBeNil)
		So(progressFileExists(testProgressFileName), ShouldEqual, true)

		invalidTestGroupIDs := testGroupIDs[1:]
		So(func() { getSavedProgressFromFile(testProgressFileName, invalidTestGroupIDs) }, ShouldPanic)

		// clean up
		os.Remove(getFilePath(testProgressFileName))
		So(progressFileExists(testProgressFileName), ShouldEqual, false)
	})
}

func getLastGroupIDFromProgressFile(progressFileName string) string {
	dest := getFilePath(progressFileName)
	f, err := os.OpenFile(dest, os.O_RDONLY, 0666)
	if err != nil {
		panic(err)
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)
	var lastLine string
	for scanner.Scan() {
		lastLine = scanner.Text()
	}
	lineSplit := strings.Split(lastLine, ",")
	return lineSplit[progressFieldGroupID]
}
