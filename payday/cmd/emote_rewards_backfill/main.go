package main

import (
	"bufio"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/dynamo/badge_tier_emote_groupids"
	"code.justin.tv/commerce/payday/models/api"
	"code.justin.tv/commerce/payday/sns"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/codegangsta/cli"
	"golang.org/x/time/rate"
)

// CLI Flags, detail for each flag can be found in main()
var (
	environment                string
	dryrun                     bool
	useSpecifiedChannels       bool
	ignoreIsBackfilledInDynamo bool
	forceGroupIDRefetch        bool
)

// Name of the file Group IDs fetched from Dynamo will be saved to, environment will be interpolated
const savedListFilename = "saved_group_ids_%s.json"

// Name of the file that contains predefined channels to publish messages for
const specifiedChannelsFilename = "specified_channels/%s.txt"

// Limit how fast we are posting to the SNS topic, this is disabled for dry run
const maxEmoteGroupEntitlementTPS rate.Limit = 5

var emoteGroupEntitlementLimiter = rate.NewLimiter(maxEmoteGroupEntitlementTPS, 1)

/*
Set to true to pause Publish when there are too many messages in the queue

Explanation:
The SQS queue that receives the publish from the SNS topic will also receive payload from the handler in the following steps:
- In this script, We publish group ID without users to entitle
- SQS Handler takes the payload, queries Pantheon to get users to entitle and requeues it in batches of 25 users
  (If a BTER group has 100 users to entitle, it will requeue 4 messages, for large channels the fanout could be big)
- SQS Handler takes the requeued payload, entitles emote slot with Mako

Because of the requeueing step, number of messages in the queue will at least double, it is unknown whether the SQS
would break or has performance drop when it's filled, so we added this feature to regularly poll SQS for message count
and stop to keep it at a comfortable level.

The trade off is this would prolong the execution time of this script.
*/
const (
	pauseOnPublishThresholdBreach = true
	pausePublishThreshold         = 5000              // pause SNS publish if sqs has this amount of messages or more
	pausePublishCheckInterval     = 60 * time.Second  // how Often we check SQS queue message count
	pausePublishSleepDuration     = 300 * time.Second // if we breached the threshold, how long do we wait till we check again
)

func main() {
	emoteBackfill := cli.NewApp()
	emoteBackfill.Name = "Payday Bits Emote Rewards Backfill"
	emoteBackfill.Usage = "Backfills emote entitlements for all bits tiers currently in the <env>_badge_tier_emote_groupids dynamo table"

	emoteBackfill.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "environment, e",
			Value:       "staging",
			Usage:       "Runtime config to use. Options are: 'production' and 'staging'. Defaults to 'staging' if unset.",
			EnvVar:      "environment",
			Destination: &environment,
		},
		cli.BoolFlag{
			Name:        "dryrun",
			Usage:       "Runtime config to use. If this flag is given, will do a dry run without SNS publish and its associated rate limiting",
			EnvVar:      "dryrun",
			Destination: &dryrun,
		},
		cli.BoolFlag{
			Name:        "useSpecifiedChannels",
			Usage:       "Runtime config to use. If this flag is given, will only run for specified channels in the environment that's given",
			EnvVar:      "useSpecifiedChannels",
			Destination: &useSpecifiedChannels,
		},
		cli.BoolFlag{
			Name:        "ignoreIsBackfilledInDynamo, ignoreBackfilled",
			Usage:       "Runtime config to use. If this flag is given, will run for all groups regardless of whether it was previously backfilled",
			EnvVar:      "ignoreIsBackfilledInDynamo",
			Destination: &ignoreIsBackfilledInDynamo,
		},
		cli.BoolFlag{
			Name:        "forceGroupIDRefetch, fresh",
			Usage:       "Runtime config to use. If this flag is given, will always fetch new group IDs from Dynamo even if one was previously saved",
			EnvVar:      "forceGroupIDRefetch",
			Destination: &forceGroupIDRefetch,
		},
	}

	emoteBackfill.Action = runBackfill
	err := emoteBackfill.Run(os.Args)
	if err != nil {
		panic(err)
	}
}

func runBackfill(c *cli.Context) {
	ctx := context.Background()
	log.Infof("Loading config file")

	err := config.Load(environment)
	if err != nil {
		log.Panicf("Error while loading configuration file: %v", err)
	}
	log.Infof("[OPTIONS] Environment: %v", environment)
	log.Infof("[OPTIONS] Dry Run: %v", dryrun)
	log.Infof("[OPTIONS] Use Specified Channels: %v", useSpecifiedChannels)
	log.Infof("[OPTIONS] Ignore is_backfilled in Dynamo: %v", ignoreIsBackfilledInDynamo)
	log.Infof("[OPTIONS] Force fetch fresh Group IDs from Dynamo: %v", forceGroupIDRefetch)
	log.Infof("[OPTIONS] Pause When SQS has too many messages: %v", pauseOnPublishThresholdBreach)

	if environment == "production" && !dryrun {
		panicBufferDuration := 10 * time.Second
		fmt.Printf("!!!!Running backfill for real in prod. Giving you %v to rethink this decision!!!!\n", panicBufferDuration)
		time.Sleep(panicBufferDuration)
		fmt.Println("Here... We... Go!! (Joker's voice)")
	}

	cfg := config.Get()
	if err := cfg.Validate(); err != nil {
		log.Panicf("Rekt while validating configuration file: %v", err)
	}

	log.Infof("Setting up aws session")
	sess, err := session.NewSession(&aws.Config{
		Region: aws.String(cfg.DynamoRegion),
	})
	if err != nil {
		log.WithError(err).Panic("Could not start new aws session")
	}

	log.Infof("Setting up Dynamo DAO")
	dao := badge_tier_emote_groupids.NewBadgeTierEmoteGroupIDDAO(sess, cfg)

	log.Infof("Setting up SNS Client")
	snsClient := sns.NewSnsClient(cfg.SQSRegion)

	log.Infof("Setting up SQS Queue Size Monitor")
	sqsQueueSizeMonitor := NewSQSQueueSizeMonitor(sess, SQSQueueSizeMonitorConfig{
		SQSRegion:                      &cfg.SQSRegion,
		QueueURL:                       &cfg.EmoticonBackfillQueueURL,
		PausePublishThreshold:          pausePublishThreshold,
		CheckInterval:                  pausePublishCheckInterval,
		CheckIntervalOnThresholdBreach: pausePublishSleepDuration,
	})

	var groupIDs []*badge_tier_emote_groupids.BadgeTierEmoteGroupID
	invalidProgressWarning := "If you have previous progress, this could panic if progress file does not match up."
	if !forceGroupIDRefetch {
		groupIDs, err = getGroupIDsFromSavedList(environment)
		if err != nil {
			log.Warnf("Failed to get Group IDs from SavedList, fetching from Dynamo. %s", invalidProgressWarning)
			groupIDs = getGroupIDsFromDynamo(ctx, dao)
			saveGroupIDsToSavedList(environment, groupIDs)
		}
	} else {
		log.Warnf("You are force fetching group IDs from Dynamo. %s", invalidProgressWarning)
		groupIDs = getGroupIDsFromDynamo(ctx, dao)
		saveGroupIDsToSavedList(environment, groupIDs)
	}

	if useSpecifiedChannels {
		groupIDs = filterGroupIDs(groupIDs, environment)
	}

	log.Infof("Setting up Progress Tracker")
	progressTracker := NewProgressTracker(groupIDs, ProgressTrackerConfig{
		environment:          environment,
		dryrun:               dryrun,
		useSpecifiedChannels: useSpecifiedChannels,
	})

	for _, group := range groupIDs[progressTracker.GetProgress():] {
		if pauseOnPublishThresholdBreach {
			for sqsQueueSizeMonitor.PublishShouldPause() {
				log.Warnf("SQS Queue Size Monitor has recommended pausing, pausing for %v", pausePublishSleepDuration)
				time.Sleep(pausePublishSleepDuration)
			}
		}
		currProgress := progressTracker.GetProgress() + 1
		totalItems := progressTracker.GetTotalItems()
		if currProgress%1000 == 0 || currProgress == totalItems {
			log.Infof("Progress Report: Completed %v groups out of %v total", progressTracker.GetProgress()+1, progressTracker.GetTotalItems())
		}
		sendBackfillSNSMessage(ctx, snsClient, cfg.EmoticonBackfillSNSTopic, progressTracker, group)
	}

	if !dryrun {
		log.Infof("All Messages have been published to SNS Topic!")
	} else {
		log.Infof("Dryrun complete!")
	}
}

func sendBackfillSNSMessage(ctx context.Context, client *sns.SNSClient, snsTopic string, progressTracker *ProgressTracker, group *badge_tier_emote_groupids.BadgeTierEmoteGroupID) {
	log := log.WithFields(log.Fields{
		"channelID":        group.ChannelID,
		"threshold":        group.Threshold,
		"groupID":          group.GroupID,
		"ignoreBackfilled": ignoreIsBackfilledInDynamo,
	})

	if group.Threshold == 1 || group.Threshold == 100 {
		err := progressTracker.SaveProgress(group, true)
		if err != nil {
			log.WithError(err).Errorf("Saving progress failed for group %v, if you rerun the script it will try to evaluate this group again.", group.GroupID)
		}
		return
	}

	if !dryrun {
		_ = emoteGroupEntitlementLimiter.Wait(ctx)
	}

	message := api.EmoticonBackfillSNSMessage{
		Threshold:        group.Threshold,
		GroupID:          group.GroupID,
		ChannelID:        group.ChannelID,
		IgnoreBackfilled: ignoreIsBackfilledInDynamo,
	}

	messageJSON, err := json.Marshal(message)
	if err != nil {
		log.WithError(err).Panic("Error marshaling emoticon backfill message json")
	}

	if !dryrun {
		err = client.PostToTopic(snsTopic, string(messageJSON))
		if err != nil {
			log.WithError(err).Panic("Error posting emoticon backfill to SNS")
		}
	}

	err = progressTracker.SaveProgress(group, false)
	if err != nil {
		log.WithError(err).Errorf("Saving progress failed for group %v, if you rerun the script it will try to post this group to SNS again.", group.GroupID)
	}

}

func getGroupIDsFromDynamo(ctx context.Context, dao badge_tier_emote_groupids.BadgeTierEmoteGroupIDDAO) []*badge_tier_emote_groupids.BadgeTierEmoteGroupID {
	log.Infof("Fetching groups for entitlement from DynamoDB")
	groupIDs, err := dao.GetAll(ctx)
	if err != nil {
		log.WithError(err).Panic("Error getting emote groups to entitle from DynamoDB")
	}
	log.Infof("Number of groups to entitle: %d\n", len(groupIDs))
	return groupIDs
}

func getGroupIDsFromSavedList(environment string) ([]*badge_tier_emote_groupids.BadgeTierEmoteGroupID, error) {
	dir, err := os.Getwd()
	if err != nil {
		return nil, err
	}

	dest := path.Join(dir, fmt.Sprintf(savedListFilename, environment))
	f, err := os.OpenFile(dest, os.O_RDONLY, 0666)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	byteValue, _ := ioutil.ReadAll(f)

	var groupIDs []*badge_tier_emote_groupids.BadgeTierEmoteGroupID
	err = json.Unmarshal([]byte(byteValue), &groupIDs)
	if err != nil {
		return nil, err
	}

	return groupIDs, nil
}

func saveGroupIDsToSavedList(environment string, groupIDs []*badge_tier_emote_groupids.BadgeTierEmoteGroupID) {
	dir, err := os.Getwd()
	if err != nil {
		panic(err)
	}

	dest := path.Join(dir, fmt.Sprintf(savedListFilename, environment))
	f, err := os.OpenFile(dest, os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		panic(err)
	}
	defer f.Close()

	messageJSON, err := json.Marshal(groupIDs)
	if err != nil {
		panic(err)
	}

	_, err = f.WriteString(string(messageJSON))
	if err != nil {
		panic(err)
	}
}

func filterGroupIDs(groupIDs []*badge_tier_emote_groupids.BadgeTierEmoteGroupID, env string) []*badge_tier_emote_groupids.BadgeTierEmoteGroupID {
	specifiedChannels := getSpecifiedChannels(env)
	var newGroupIDs []*badge_tier_emote_groupids.BadgeTierEmoteGroupID
	for _, group := range groupIDs {
		if _, isSpecified := specifiedChannels[group.ChannelID]; isSpecified {
			newGroupIDs = append(newGroupIDs, group)
		}
	}
	return newGroupIDs
}

func getSpecifiedChannels(env string) map[string]struct{} {
	dir, err := os.Getwd()
	if err != nil {
		panic(err)
	}
	dest := path.Join(dir, fmt.Sprintf(specifiedChannelsFilename, env))
	f, err := os.OpenFile(dest, os.O_RDONLY, 0666)
	if err != nil {
		panic(err)
	}
	defer f.Close()

	var specifiedChannelsLookup = map[string]struct{}{}
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		specifiedChannelsLookup[scanner.Text()] = struct{}{}
	}

	return specifiedChannelsLookup
}
