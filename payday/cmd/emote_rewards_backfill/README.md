# BTER Backfill Script

This script does the following:
- get all Badge Tier Emote Groups
- publishes these groups to SNS topic which is subscribed to by a SQS queue
- SQS queue handler takes these messages, queries [Pantheon](https://git.xarth.tv/commerce/pantheon) to get all users elliglbe for this group
- SQS handler requeues these messages
- The second time it gets it, it tries to call [Mako](https://git.xarth.tv/commerce/mako) 's `CreateGroupEntitlements` endpoint, because Mako is not a service we own, we rate limit on this.

## Local Execution

look at the CLI flags in `main.go`

- make sure you have AWS creds set up in `~/.aws/credentials` for Payday
- `go build`
- `./emote_rewards_backfill {flags}`

## Remote Execution

We also have a bash script that allows you to run this remotely in an EC2 host. This was needed because the execution time could be really long and it's not ideal to run this locally. You can use the script to check the remote process to make sure it's still running, and download the progress file after everything is done.

Set up `config.sh` using the following template

```bash
export IP=
export USERNAME=
export AWS_ACCESS_KEY_ID=
export AWS_SECRET_ACCESS_KEY=

# expects production/staging
export environment=

# expects true/false
export dryrun=
export useSpecifiedChannels=
export ignoreBackfilled=
```

then `bash remote_exec.sh`
