package main

import (
	"bufio"
	"fmt"
	"os"
	"path"
	"strconv"
	"strings"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/dynamo/badge_tier_emote_groupids"
)

/*
The Progress Tracker Struct takes care of saving progress to a progress file (.csv) when SaveProgress() is called.

This file is important because it allows the Caller to pick up where last execution left off if it was stopped.
There are validation mechanism where it uses the dataset that's passed in on Struct initialization to make sure the
entries in the progress file matches the entries in groupIDs in the same order.

It will panic if validation fails, this is by design to encourage manual inspection of the progress file.
The Caller can call GetProgress() and GetTotalItems() to find out how much has been completed and how much is left.
*/
type ProgressTracker struct {
	progressFileName    string
	groupIDs            []*badge_tier_emote_groupids.BadgeTierEmoteGroupID
	totalItems          int
	progress            int
	getSavedProgressImp func(string, []*badge_tier_emote_groupids.BadgeTierEmoteGroupID) int
}

type ProgressTrackerConfig struct {
	environment          string
	dryrun               bool
	useSpecifiedChannels bool
}

// fields for the saved progress file
const (
	progressFieldItemNum = iota
	progressFieldTotalItems
	progressFieldDatetime
	progressFieldChannelID
	progressFieldGroupID
	progressFieldThreshold
	progressFieldNotes
	progressFieldNumOfFields
	progressFilename = "progress_%s%s%s.csv" // interpolated in buildProgressFileName()
)

func NewProgressTracker(groupIDs []*badge_tier_emote_groupids.BadgeTierEmoteGroupID, config ProgressTrackerConfig) *ProgressTracker {
	progressTracker := ProgressTracker{
		buildProgressFileName(config.environment, config.dryrun, config.useSpecifiedChannels),
		groupIDs,
		len(groupIDs),
		0,
		getSavedProgressFromFile,
	}
	progressTracker.progress = progressTracker.getSavedProgress()

	log.Infof("ProgressTracker initialized. Progress will be saved in %v", progressTracker.progressFileName)
	return &progressTracker
}

func (p *ProgressTracker) GetProgress() int {
	return p.progress
}

func (p *ProgressTracker) GetTotalItems() int {
	return p.totalItems
}

func (p *ProgressTracker) SaveProgress(group *badge_tier_emote_groupids.BadgeTierEmoteGroupID, skipped bool) error {
	ok, expectedID := p.validateGroup(group)
	if !ok {
		log.WithFields(log.Fields{
			"actualID":   group.GroupID,
			"expectedID": expectedID,
		}).Panic("Group ID that's being saved does not match what's expected.")
	}

	shouldAddHeaderRow := !progressFileExists(p.progressFileName)
	f, err := os.OpenFile(getFilePath(p.progressFileName), os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		return err
	}
	defer f.Close()

	if shouldAddHeaderRow {
		headers := []string{
			"progress",
			"total",
			"timestamp",
			"channelID",
			"groupID",
			"threshold",
		}
		_, err = f.WriteString(strings.Join(headers[:], ",") + "\n")
		if err != nil {
			return err
		}
	}

	var progress [progressFieldNumOfFields]string
	progress[progressFieldItemNum] = strconv.Itoa(p.progress + 1)
	progress[progressFieldTotalItems] = strconv.Itoa(p.totalItems)
	progress[progressFieldDatetime] = strconv.Itoa(int(time.Now().Unix()))
	progress[progressFieldChannelID] = group.ChannelID
	progress[progressFieldGroupID] = group.GroupID
	progress[progressFieldThreshold] = strconv.Itoa(int(group.Threshold))

	if skipped {
		progress[progressFieldNotes] = "skipped"
	} else {
		progress[progressFieldNotes] = "processed"
	}

	_, err = f.WriteString(strings.Join(progress[:], ",") + "\n")
	if err != nil {
		return err
	}
	p.progress++
	return nil
}

func (p *ProgressTracker) getSavedProgress() int {
	return p.getSavedProgressImp(p.progressFileName, p.groupIDs)
}

func (p *ProgressTracker) validateGroup(group *badge_tier_emote_groupids.BadgeTierEmoteGroupID) (bool, string) {
	expectedID := p.groupIDs[p.progress].GroupID
	actualID := group.GroupID
	return expectedID == actualID, expectedID
}

func buildProgressFileName(environment string, dryrun bool, useSpecifiedChannels bool) string {
	var dryrunToken string
	if dryrun {
		dryrunToken = "_dryrun"
	}
	var specifiedChannelsToken string
	if useSpecifiedChannels {
		specifiedChannelsToken = "_specified_channels"
	}
	return fmt.Sprintf(progressFilename, environment, specifiedChannelsToken, dryrunToken)
}

func getSavedProgressFromFile(progressFileName string, groupIDs []*badge_tier_emote_groupids.BadgeTierEmoteGroupID) int {
	dest := getFilePath(progressFileName)
	f, err := os.OpenFile(dest, os.O_RDONLY, 0666)
	if err != nil {
		log.WithError(err).Warnf("Error opening progress file %v, returning 0 as no progress", dest)
		return 0
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)
	scanner.Scan() // skip header row
	index := 0
	for scanner.Scan() {
		line := scanner.Text()
		lineSplit := strings.Split(line, ",")
		savedGroupID := lineSplit[progressFieldGroupID]
		expectedGroupID := groupIDs[index].GroupID
		if savedGroupID != expectedGroupID {
			log.Panicf("We found a groupID %v that does not match what's expected (%v) in progress file, did you pass in the right groupIDs dataset?", savedGroupID, expectedGroupID)
		}
		index++
	}
	log.Infof("Successfully counted through the whole file, currently %v groups have been processed", index+1)
	return index
}

func progressFileExists(progressFileName string) bool {
	dest := getFilePath(progressFileName)
	info, err := os.Stat(dest)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

func getFilePath(filename string) string {
	dir, err := os.Getwd()
	if err != nil {
		panic(err)
	}
	return path.Join(dir, filename)
}
