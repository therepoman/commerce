package main

import (
	"testing"
	"time"

	"github.com/aws/aws-sdk-go/service/sqs"
	. "github.com/smartystreets/goconvey/convey"
)

var (
	testSQSRegion                      = "some-region"
	testQueueURL                       = "some-url"
	testPausePublishThreshold          = 5
	testCheckInterval                  = 10 * time.Millisecond
	testCheckIntervalOnThresholdBreach = 10 * time.Millisecond

	// struct field does not get updated instantly because of the go routine, need update buffer time
	testUpdateBufferTime = 100 * time.Millisecond
)

func TestSQSChecker_PublishShouldPause(t *testing.T) {
	Convey("Given Publish Threshold Not Breached", t, func() {
		sqsClient := &sqs.SQS{}
		config := SQSQueueSizeMonitorConfig{
			&testSQSRegion,
			&testQueueURL,
			testPausePublishThreshold,
			testCheckInterval,
			testCheckIntervalOnThresholdBreach,
		}
		testSQSChecker := SQSQueueSizeMonitor{
			sqsClient:             sqsClient,
			config:                config,
			shouldPauseChannel:    make(chan bool),
			shouldPause:           false,
			getSQSMessageCountImp: getSQSMessageCountImpNotBreached,
		}
		go testSQSChecker.startPoller()

		So(testSQSChecker.PublishShouldPause(), ShouldEqual, false)
		time.Sleep(testCheckInterval + testUpdateBufferTime)
		So(testSQSChecker.PublishShouldPause(), ShouldEqual, false)
	})

	Convey("Given Publish Threshold Breached", t, func() {
		sqsClient := &sqs.SQS{}
		config := SQSQueueSizeMonitorConfig{
			&testSQSRegion,
			&testQueueURL,
			testPausePublishThreshold,
			testCheckInterval,
			testCheckIntervalOnThresholdBreach,
		}
		testSQSChecker := SQSQueueSizeMonitor{
			sqsClient:             sqsClient,
			config:                config,
			shouldPauseChannel:    make(chan bool),
			shouldPause:           false,
			getSQSMessageCountImp: getSQSMessageCountImpBreached,
		}
		go testSQSChecker.startPoller()

		So(testSQSChecker.PublishShouldPause(), ShouldEqual, false)
		time.Sleep(testCheckInterval + testUpdateBufferTime)
		So(testSQSChecker.PublishShouldPause(), ShouldEqual, true)
	})
}

func getSQSMessageCountImpBreached(s *sqs.SQS, cfg SQSQueueSizeMonitorConfig) int {
	return testPausePublishThreshold
}

func getSQSMessageCountImpNotBreached(s *sqs.SQS, cfg SQSQueueSizeMonitorConfig) int {
	return 0
}
