package main

import (
	"strconv"
	"time"

	log "code.justin.tv/commerce/logrus"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sqs"
)

/*
The SQS Queue Size Monitor Struct takes care of periodically polling the message count in the SQS queue.
When the threshold in the config is breached, it will set the struct variable shouldPause to true, which is
accessible through PublishShouldPause(). The polling will keep on happening regardless of breach status.
Responsibility falls on the Caller to decide what to do on threshold breach, the monitor only provides recommendation.
*/
type SQSQueueSizeMonitor struct {
	sqsClient             *sqs.SQS
	timer                 *time.Timer
	config                SQSQueueSizeMonitorConfig
	getSQSMessageCountImp func(*sqs.SQS, SQSQueueSizeMonitorConfig) int
	shouldPauseChannel    chan bool
	shouldPause           bool
}

type SQSQueueSizeMonitorConfig struct {
	SQSRegion                      *string
	QueueURL                       *string
	PausePublishThreshold          int           // pause SNS publish if sqs has this amount of messages or more
	CheckInterval                  time.Duration // how Often we check SQS queue message count
	CheckIntervalOnThresholdBreach time.Duration // if we breached the threshold, how long do we wait till we check again
}

func NewSQSQueueSizeMonitor(sess *session.Session, config SQSQueueSizeMonitorConfig) *SQSQueueSizeMonitor {
	sqsClient := sqs.New(sess, &aws.Config{Region: aws.String(*config.SQSRegion)})
	sqsQueueSizeMonitor := SQSQueueSizeMonitor{
		sqsClient:             sqsClient,
		config:                config,
		getSQSMessageCountImp: getSQSMessageCountImp,
		shouldPauseChannel:    make(chan bool),
		shouldPause:           false,
	}
	go sqsQueueSizeMonitor.startPoller()
	return &sqsQueueSizeMonitor
}

func (s *SQSQueueSizeMonitor) PublishShouldPause() bool {
	select {
	case statusUpdate := <-s.shouldPauseChannel:
		s.shouldPause = statusUpdate
	default:
	}
	return s.shouldPause
}

func (s *SQSQueueSizeMonitor) startPoller() {
	s.timer = time.NewTimer(s.config.CheckInterval)
	for {
		<-s.timer.C
		msgCount := s.getSQSMessageCount()
		if msgCount >= s.config.PausePublishThreshold {
			log.WithField("msgCount", msgCount).Warnf("SQS Queue Message is above the threshold, checking again in %v", s.config.CheckIntervalOnThresholdBreach)
			s.shouldPauseChannel <- true
			s.timer.Reset(s.config.CheckIntervalOnThresholdBreach)
		} else {
			log.WithField("msgCount", msgCount).Warnf("SQS Queue Message count looks fine, checking again in %v", s.config.CheckInterval)
			s.shouldPauseChannel <- false
			s.timer.Reset(s.config.CheckInterval)
		}
	}
}

func (s *SQSQueueSizeMonitor) getSQSMessageCount() int {
	return s.getSQSMessageCountImp(s.sqsClient, s.config)
}

func getSQSMessageCountImp(s *sqs.SQS, cfg SQSQueueSizeMonitorConfig) int {
	field := sqs.QueueAttributeNameApproximateNumberOfMessages
	attributeInput := sqs.GetQueueAttributesInput{
		QueueUrl:       cfg.QueueURL,
		AttributeNames: []*string{&field},
	}
	output, err := s.GetQueueAttributes(&attributeInput)
	if err != nil {
		log.WithError(err).Panic("Error getting queue attribute")
	}
	count, err := strconv.Atoi(*(output.Attributes[field]))
	if err != nil {
		log.WithError(err).Panic("Error converting Approximate Number of Messages to Int")
	}
	return count
}
