# folder name on EC2 instance
dir=bter-backfill

function main() {
  echo -------------------------------
  echo "Welcome to BTER Backfill Remote Execution CLI"
  load_config
  echo "1. Start Remote Execution"
  echo "2. Check Remote Process"
  echo "3. Print Current Progress"
  echo "4. Download SCP Progress file"
  echo "5. Download Execution Logs"
  echo "6. Clean up Remote Environment (Irreversible)"
  read -p "What do you want to do (1,2,3,4,5,6,exit)? " ans

  if [ $ans == 1 ]; then
    echo "Starting Remote Execution..."
    start_remote_execution
  elif [ $ans == 2 ]; then
    echo "Checking Remote Process..."
    check_remote_proces
  elif [ $ans == 3 ]; then
    echo "Printing Current Progress..."
    print_progress
  elif [ $ans == 4 ]; then
    echo "Downloading SCP Progress file..."
    download_progress
  elif [ $ans == 5 ]; then
    echo "Downloading Execution logs..."
    download_logs
  elif [ $ans == 6 ]; then
    echo "Prepping Cleanup..."
    cleanup
  else
    echo "exiting..."
  fi
  echo -------------------------------
}

function load_config() {
  . ./config.sh

  load_success=true
  if [ -z $IP ] ; then
    echo "Empty IP in config"
    load_success=false
  elif [  -z $USERNAME ]; then
    echo "Empty USERNAME in config"
    load_success=false
  elif [ -z $AWS_ACCESS_KEY_ID ]; then
    echo "Empty $AWS_ACCESS_KEY_ID in config"
    load_success=false
  elif [ -z $AWS_SECRET_ACCESS_KEY ]; then
    echo "Empty $AWS_SECRET_ACCESS_KEY in config"
    load_success=false
  elif [ -z $environment ] ; then
    echo "Empty environment in config (staging/production)"
    load_success=false
  elif [  -z $dryrun ]; then
    echo "Empty dryrun setting in config (true/false)"
    load_success=false
  elif [ -z $useSpecifiedChannels ]; then
    echo "Empty $useSpecifiedChannels in config (true/false)"
    load_success=false
  elif [ -z $ignoreBackfilled ]; then
    echo "Empty $ignoreBackfilled in config (true/false)"
    load_success=false
  fi

  if [ $load_success == false ]; then
    echo "Missing fields! Check your config!"
    exit
  fi

  echo -------------------------------
  echo "IP: $IP"
  echo "USERNAME: $USERNAME"
  echo "AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID"
  echo "AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY"
  echo -------------------------------
  echo "environment: $environment"
  echo "dryrun: $dryrun"
  echo "useSpecifiedChannels: $useSpecifiedChannels"
  echo "ignoreBackfilled: $ignoreBackfilled"
  echo -------------------------------
}

function get_progress_file_name() {
  FILENAME="progress"
  if [ $environment == staging ]; then
    FILENAME="${FILENAME}_staging"
  else
    FILENAME="${FILENAME}_production"
  fi
    if [ $useSpecifiedChannels == true ]; then
    FILENAME="${FILENAME}_specified_channels"
  fi
  if [ $dryrun == true ]; then
    FILENAME="${FILENAME}_dryrun"
  fi
  echo "${FILENAME}.csv"
}

function start_remote_execution() {
  COMMAND="./emote_rewards_backfill"
  if [ $environment != staging ]; then
    COMMAND="$COMMAND -e=production"
  fi
  if [ $dryrun == true ]; then
    COMMAND="$COMMAND -dryrun"
  fi
  if [ $useSpecifiedChannels == true ]; then
    COMMAND="$COMMAND -useSpecifiedChannels"
  fi
  if [ $ignoreBackfilled == true ]; then
    COMMAND="$COMMAND -ignoreBackfilled"
  fi
  echo "To be executed: $COMMAND"

  read -p "Upload Assets (y/n)? " ans

  if [ $ans == y ]; then
    # build the linux compatible binary
    env GOOS=linux go build

    echo "SCPing to $IP"
    ssh "${USERNAME}@${IP}" "mkdir -p $dir; mkdir -p $dir/config; mkdir -p $dir/specified_channels; mkdir -p $dir/logs"
    scp ./emote_rewards_backfill "${USERNAME}@${IP}:$dir/emote_rewards_backfill"
    scp ./specified_channels/* "${USERNAME}@${IP}:$dir/specified_channels"
    scp ../../config/*.json "${USERNAME}@${IP}:$dir/config"
  fi

  TIMESTAMP=$(date '+%s')
  LOGNAME=$environment
  if [ $dryrun == true ]; then
    LOGNAME="${LOGNAME}_dryrun"
  fi
  LOGNAME="${LOGNAME}_${TIMESTAMP}.log"

  COMMAND='$(nohup '$COMMAND' > logs/'$LOGNAME' 2>&1 &)'
  echo -------------------------------
  ssh "${USERNAME}@${IP}" "cd $dir\
    && echo \"logs will be stored in $dir/logs/$LOGNAME\"\
    && export AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID\
    && export AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY\
    && $COMMAND\
    & exit"
  echo "Script is now remotely executing, rerun this script and select 2 or 3 to check progress"
}

function check_remote_proces() {
  echo -------------------------------
  ssh "${USERNAME}@${IP}" "\
    echo ------------------------------- ;\
    ps aux | head -1 ;\
    ps aux | grep [emote]_rewards_backfill"
}

function print_progress() {
  echo -------------------------------
  FILENAME=$(get_progress_file_name)
  ssh "${USERNAME}@${IP}" "cd $dir\
    && echo -------------------------------\
    && head -n 1 $FILENAME\
    && tail $FILENAME"
}

function download_progress() {
  echo -------------------------------
  output_dir=remote_progress
  mkdir -p $output_dir
  FILENAME=$(get_progress_file_name)
  scp "${USERNAME}@${IP}:$dir/$FILENAME" ./$output_dir && echo "progress \"$FILENAME\" downloaded to folder \"$output_dir\""
}

function download_logs() {
  echo -------------------------------
  output_dir=remote_progress
  mkdir -p $output_dir
  scp "${USERNAME}@${IP}:$dir/logs/*" ./$output_dir && echo "downloaded all logs to folder \"$output_dir\""
}

function cleanup() {
  echo -------------------------------
  read -p "YOU ARE ABOUT TO CLEAN UP THE REMOTE ENVIRONMENT, ARE YOU SURE (y/n)? " ans
  if [ $ans == 'y' ]; then
    echo 'deleting...'
    ssh "${USERNAME}@${IP}" "rm -rf $dir"
  else
    echo 'aborted'
  fi
}

main