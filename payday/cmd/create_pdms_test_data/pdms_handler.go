package main

import (
	"context"
	"time"

	"code.justin.tv/commerce/payday/clients/pdms"
	"github.com/golang/protobuf/ptypes/timestamp"

	log "code.justin.tv/commerce/logrus"
)

type PDMSHandler struct {
	PDMSClient pdms.IPDMSClientWrapper
	Dryrun     bool
}

func NewPDMSHandler(pdmsClient pdms.IPDMSClientWrapper, dryrun bool) *PDMSHandler {
	return &PDMSHandler{
		PDMSClient: pdmsClient,
		Dryrun:     dryrun,
	}
}

func (p *PDMSHandler) createTestDataForPDMS(ctx context.Context) error {
	createTimestamp := timestamp.Timestamp{Seconds: time.Now().Unix()}
	log := log.WithFields(log.Fields{
		"userID":    TestUserID,
		"timestamp": createTimestamp.AsTime(),
	})
	if p.Dryrun {
		log.Info("Dryrun: will not create data in PDMS")
	} else {
		log.Infof("Real Run: creating data in PDMS for user")
		err := p.PDMSClient.SetTestDeletion(ctx, TestUserID, &createTimestamp, nil, nil, nil)
		if err != nil {
			return err
		}
	}
	return nil
}
