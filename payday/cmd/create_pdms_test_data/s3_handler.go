package main

import (
	"context"
	"strings"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/s3"
	"github.com/aws/aws-sdk-go/aws"
)

const rawMaterialFolder = "raw-material-for-pdms/"
const destinationFolder = TestUserID + "/"
const cheermoteFolder = "partner-actions/"

type S3Handler struct {
	S3Client s3.IS3Client
	Dryrun   bool
	cfg      *config.Configuration
}

func NewS3Handler(cfg *config.Configuration, dryrun bool) *S3Handler {
	s3Client := s3.NewFromConfig(&aws.Config{
		Region: aws.String("us-west-2"),
	})
	return &S3Handler{
		S3Client: s3Client,
		Dryrun:   dryrun,
		cfg:      cfg,
	}
}

func (s *S3Handler) createTestAssetForBitsOnboardEvents(ctx context.Context) error {
	bucketName := s.cfg.BitsOnboardEventsBucketName
	files, err := s.S3Client.ListFileNames(bucketName, rawMaterialFolder)
	if err != nil {
		return err
	}

	for _, file := range files {
		// ListFileNames returns object only except for the first item, which will always be the containing folder and is not copy-able
		if file[len(file)-1:] == "/" {
			log.WithField("path", file).Info("Skipping folder")
			continue
		}
		fileWithPrefix := bucketName + "/" + file
		destinationKey := strings.Replace(file, rawMaterialFolder, destinationFolder, 1)
		log := log.WithFields(log.Fields{
			"bucket":   bucketName,
			"copyFrom": fileWithPrefix,
			"copyTo":   destinationKey,
		})
		if s.Dryrun {
			log.Info("Dryrun: BitsOnboardEvent won't be copied")
		} else {
			log.Info("Real Run: Copying BitsOnboardEvent")
			err := s.S3Client.CopyFile(ctx, bucketName, fileWithPrefix, destinationKey)
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func (s *S3Handler) createTestAssetForCheermotes(ctx context.Context) error {
	bucketName := s.cfg.BitsAssetsS3BucketName
	files, err := s.S3Client.ListFileNames(bucketName, cheermoteFolder+rawMaterialFolder)
	if err != nil {
		return err
	}

	for _, file := range files {
		// ListFileNames returns object only except for the first item, which will always be the containing folder and is not copy-able
		if file[len(file)-1:] == "/" {
			log.WithField("path", file).Info("Skipping folder")
			continue
		}
		fileWithPrefix := bucketName + "/" + file
		destinationKey := strings.Replace(file, rawMaterialFolder, destinationFolder, 1)
		log := log.WithFields(log.Fields{
			"bucket":   bucketName,
			"copyFrom": fileWithPrefix,
			"copyTo":   destinationKey,
		})
		if s.Dryrun {
			log.Info("Dryrun: Cheermote Assets won't be copied")
		} else {
			log.Info("Real Run: Copying Cheermote Assets")
			err := s.S3Client.CopyFile(ctx, bucketName, fileWithPrefix, destinationKey)
			if err != nil {
				return err
			}
		}
	}
	return nil
}
