package main

import (
	"context"
	"fmt"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/dynamo/badge_tier_emote_groupids"
	"code.justin.tv/commerce/payday/dynamo/leaderboard_badge_holders"
	"code.justin.tv/commerce/payday/dynamo/purchase_by_most_recent_super_platform"
	"code.justin.tv/commerce/payday/dynamo/single_purchase_consumption"
	"code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/channel_statuses"
	"code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/eligible_channels"
	"code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/user_campaign_statuses"
	"github.com/aws/aws-sdk-go/aws/session"
)

type DynamoDAOs struct {
	PurchaseByMostRecentSuperPlatformDAO purchase_by_most_recent_super_platform.PurchaseByMostRecentSuperPlatformDAO
	SinglePurchaseConsumptionDAO         single_purchase_consumption.SinglePurchaseConsumptionDAO
	BadgeTiersDAO                        dynamo.IBadgeTierDao
	BadgeTierEmoteGroupIDsDAO            badge_tier_emote_groupids.BadgeTierEmoteGroupIDDAO
	UserDAO                              dynamo.IUserDao
	LeaderboardBadgeHoldersDAO           leaderboard_badge_holders.ILeaderboardBadgeHoldersDao
	ChannelsDAO                          dynamo.IChannelDao
	ImagesDAO                            dynamo.IImageDao
	SpomoteUserStatusesDAO               user_campaign_statuses.ISponsoredCheermoteUserStatusesDao
	SpomoteEligibleChannelsDAO           eligible_channels.ISponsoredCheermoteEligibleChannelsDao
	SpomoteChannelStatusesDAO            channel_statuses.ISponsoredCheermoteChannelStatusesDao
}

type DynamoHandler struct {
	DynamoDAOs DynamoDAOs
	Dryrun     bool
}

func NewDynamoHandler(cfg *config.Configuration, sess *session.Session, dryrun bool) *DynamoHandler {
	dynamoClientNoAudit := dynamo.NewClient(&dynamo.DynamoClientConfig{
		AwsRegion:   cfg.DynamoRegion,
		TablePrefix: cfg.DynamoTablePrefix,
	})

	dynamoDAOs := DynamoDAOs{
		PurchaseByMostRecentSuperPlatformDAO: purchase_by_most_recent_super_platform.NewPurchaseByMostRecentSuperPlatformDAO(sess, *cfg),
		SinglePurchaseConsumptionDAO:         single_purchase_consumption.NewSinglePurchaseConsumptionDAO(sess, *cfg),
		BadgeTiersDAO:                        dynamo.NewBadgeTierDao(dynamoClientNoAudit),
		BadgeTierEmoteGroupIDsDAO:            badge_tier_emote_groupids.NewBadgeTierEmoteGroupIDDAO(sess, *cfg),
		UserDAO:                              dynamo.NewUserDao(dynamoClientNoAudit),
		LeaderboardBadgeHoldersDAO:           leaderboard_badge_holders.NewLeaderboardBadgesDao(dynamoClientNoAudit),
		ChannelsDAO:                          dynamo.NewChannelDao(dynamoClientNoAudit),
		ImagesDAO:                            dynamo.NewImageDao(dynamoClientNoAudit),
		SpomoteUserStatusesDAO:               user_campaign_statuses.NewSponsoredCheermoteUserStatusesDao(dynamoClientNoAudit),
		SpomoteEligibleChannelsDAO:           eligible_channels.NewSponsoredCheermoteEligibleChannelsDao(dynamoClientNoAudit),
		SpomoteChannelStatusesDAO:            channel_statuses.NewSponsoredCheermoteChannelStatusesDao(dynamoClientNoAudit),
	}

	handler := DynamoHandler{
		DynamoDAOs: dynamoDAOs,
		Dryrun:     dryrun,
	}

	return &handler
}

func (d *DynamoHandler) createTestDataForPurchaseBySuperPlatform(ctx context.Context) error {
	numOfRows := 20

	testSuperPlatformPrefix := "WEB"
	testPlatformPrefix := "WALLET"
	testProductPrefix := "TEST_PRODUCT"

	for i := 1; i <= numOfRows; i++ {
		testSuperPlatform := fmt.Sprintf("%s-%d", testSuperPlatformPrefix, i)
		testPlatform := fmt.Sprintf("%s-%d", testPlatformPrefix, i)
		testProductID := fmt.Sprintf("%s-%d", testProductPrefix, i)
		log := log.WithFields(log.Fields{
			"userID":        TestUserID,
			"superPlatform": testSuperPlatform,
			"platform":      testPlatform,
			"productID":     testProductID,
		})
		if d.Dryrun {
			log.Info("Dryrun: PurchaseByMostRecentSuperPlatform")
		} else {
			log.Info("Real Run: PurchaseByMostRecentSuperPlatform")
			_, err := d.DynamoDAOs.PurchaseByMostRecentSuperPlatformDAO.
				Create(ctx, TestUserID, testSuperPlatform, testPlatform, testProductID)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func (d *DynamoHandler) createTestDataForSinglePurchaseConsumption(ctx context.Context) error {
	numOfRows := 20
	testProductPrefix := "TEST_PRODUCT"
	for i := 1; i <= numOfRows; i++ {
		testProductID := fmt.Sprintf("%s-%d", testProductPrefix, i)
		log := log.WithFields(log.Fields{
			"userID":    TestUserID,
			"productID": testProductID,
		})
		if d.Dryrun {
			log.Info("Dryrun: SinglePurchaseConsumption")
		} else {
			log.Info("Real Run: SinglePurchaseConsumption")
			_, err := d.DynamoDAOs.SinglePurchaseConsumptionDAO.Create(ctx, TestUserID, testProductID)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func (d *DynamoHandler) createTestDataForBadgeTiersAndEmoteGroupIDs(ctx context.Context) error {
	testTiers := []*dynamo.BadgeTier{
		{
			ChannelId:   TestUserID,
			Threshold:   1,
			Enabled:     false,
			LastUpdated: time.Now(),
		},
		{
			ChannelId:   TestUserID,
			Threshold:   100,
			Enabled:     false,
			LastUpdated: time.Now(),
		},
	}
	if d.Dryrun {
		for _, testTier := range testTiers {
			log.WithField("testTier", testTier).Info("Dryrun: BadgeTiers")
		}
	} else {
		for _, testTier := range testTiers {
			log.WithField("testTier", testTier).Info("Real Run: BadgeTiers")
		}
		err := d.DynamoDAOs.BadgeTiersDAO.PutAll(testTiers)
		if err != nil {
			return err
		}
	}

	testGroupID := "test-pdms-badge-tier-group-id"
	log := log.WithFields(log.Fields{
		"userID":           TestUserID,
		"badgeTierGroupID": testGroupID,
	})
	if d.Dryrun {
		log.WithField("badgeTier", 1).Info("Dryrun: BadgeTierEmoteGroupIDs")
		log.WithField("badgeTier", 100).Info("Dryrun: BadgeTierEmoteGroupIDs")
	} else {
		log.WithField("badgeTier", 1).Info("Real Run: BadgeTierEmoteGroupIDs")
		_, alreadyExists, err := d.DynamoDAOs.BadgeTierEmoteGroupIDsDAO.Create(ctx, TestUserID, 1, testGroupID)
		if err != nil {
			if !alreadyExists {
				return err
			} else {
				log.WithField("badgeTier", 1).Info("Record already exists: BadgeTierEmoteGroupIDs")
			}
		}
		log.WithField("badgeTier", 100).Infof("Real Run: BadgeTierEmoteGroupIDs")
		_, alreadyExists, err = d.DynamoDAOs.BadgeTierEmoteGroupIDsDAO.Create(ctx, TestUserID, 100, testGroupID)
		if err != nil {
			if !alreadyExists {
				return err
			} else {
				log.WithField("badgeTier", 100).Info("Record already exists: BadgeTierEmoteGroupIDs")
			}
		}
	}
	return nil
}

func (d *DynamoHandler) createTestDataForUsersTable() error {
	testUser := dynamo.User{
		Id: TestUserID,
	}
	if d.Dryrun {
		log.WithField("user", testUser).Info("Dryrun: Users")
	} else {
		log.WithField("user", testUser).Info("Real Run: Users")
		err := d.DynamoDAOs.UserDAO.Put(&testUser)
		if err != nil {
			return err
		}
	}
	return nil
}

func (d *DynamoHandler) createTestDataForLeaderboardBadgeHolders() error {
	// follows closely with pantheon_handler's logic because the generated data is meant to be related
	var batchRequest []leaderboard_badge_holders.LeaderboardBadgeHolders

	// create fake data for pdms user channel
	req := leaderboard_badge_holders.LeaderboardBadgeHolders{
		ChannelID:         TestUserID,
		FirstPlaceUserID:  fmt.Sprintf("%s-%d", testUserPrefix, 1),
		SecondPlaceUserID: fmt.Sprintf("%s-%d", testUserPrefix, 2),
		ThirdPlaceUserID:  fmt.Sprintf("%s-%d", testUserPrefix, 3),
		LastUpdated:       time.Now(),
	}
	log.WithFields(log.Fields{
		"channel": TestUserID,
		"request": req,
	}).Info("Added data for channel to batch request (not sent yet)")
	batchRequest = append(batchRequest, req)

	channelCount := 0
	// create fake leaderboard entries where pdms user is first place
	for i := 0; i < testLeaderboardsPerGroup; i, channelCount = i+1, channelCount+1 {
		channelID := fmt.Sprintf("%s-%d", channelPrefix, channelCount+1)

		req := leaderboard_badge_holders.LeaderboardBadgeHolders{
			ChannelID:         channelID,
			FirstPlaceUserID:  TestUserID,
			SecondPlaceUserID: fmt.Sprintf("%s-%d", testUserPrefix, 1),
			ThirdPlaceUserID:  fmt.Sprintf("%s-%d", testUserPrefix, 2),
			LastUpdated:       time.Now(),
		}

		log.WithFields(log.Fields{
			"channel": TestUserID,
			"request": req,
		}).Info("Added data for channel to batch request (not sent yet)")
		batchRequest = append(batchRequest, req)
	}

	// create fake leaderboard entries where pdms user is second place
	for i := 0; i < testLeaderboardsPerGroup; i, channelCount = i+1, channelCount+1 {
		channelID := fmt.Sprintf("%s-%d", channelPrefix, channelCount+1)

		req := leaderboard_badge_holders.LeaderboardBadgeHolders{
			ChannelID:         channelID,
			FirstPlaceUserID:  fmt.Sprintf("%s-%d", testUserPrefix, 1),
			SecondPlaceUserID: TestUserID,
			ThirdPlaceUserID:  fmt.Sprintf("%s-%d", testUserPrefix, 2),
			LastUpdated:       time.Now(),
		}

		log.WithFields(log.Fields{
			"channel": TestUserID,
			"request": req,
		}).Info("Added data for channel to batch request (not sent yet)")
		batchRequest = append(batchRequest, req)
	}

	// create fake leaderboard entries where pdms user is third place
	for i := 0; i < testLeaderboardsPerGroup; i, channelCount = i+1, channelCount+1 {
		channelID := fmt.Sprintf("%s-%d", channelPrefix, channelCount+1)

		req := leaderboard_badge_holders.LeaderboardBadgeHolders{
			ChannelID:         channelID,
			FirstPlaceUserID:  fmt.Sprintf("%s-%d", testUserPrefix, 1),
			SecondPlaceUserID: fmt.Sprintf("%s-%d", testUserPrefix, 2),
			ThirdPlaceUserID:  TestUserID,
			LastUpdated:       time.Now(),
		}

		log.WithFields(log.Fields{
			"channel": TestUserID,
			"request": req,
		}).Info("Added data for channel to batch request (not sent yet)")
		batchRequest = append(batchRequest, req)
	}

	// create fake leaderboard entries where only pdms user cheered
	for i := 0; i < testLeaderboardsPerGroup; i, channelCount = i+1, channelCount+1 {
		channelID := fmt.Sprintf("%s-%d", channelPrefix, channelCount+1)

		req := leaderboard_badge_holders.LeaderboardBadgeHolders{
			ChannelID:        channelID,
			FirstPlaceUserID: TestUserID,
			LastUpdated:      time.Now(),
		}

		log.WithFields(log.Fields{
			"channel": TestUserID,
			"request": req,
		}).Info("Added data for channel to batch request (not sent yet)")
		batchRequest = append(batchRequest, req)
	}

	if d.Dryrun {
		log.Infof("Dryrun: %d rows of data have been batched but not sent to leaderboard badge holders table", len(batchRequest))
	} else {
		log.Infof("Real Run: %d rows of batched data have been sent to leaderboard badge holders table", len(batchRequest))
		err := d.DynamoDAOs.LeaderboardBadgeHoldersDAO.BatchPut(batchRequest)
		if err != nil {
			return err
		}
	}

	return nil
}

func (d *DynamoHandler) createTestDataForChannelsAndImages() error {
	testImageSetID := "test-pdms-image-set-id"
	testChannel := dynamo.Channel{
		Id:                        TestUserID,
		CustomCheermoteImageSetId: &testImageSetID,
	}
	if d.Dryrun {
		log.WithField("channel", testChannel).Info("Dryrun: Channels")
	} else {
		log.WithField("channel", testChannel).Info("Real Run: Channels")
		err := d.DynamoDAOs.ChannelsDAO.Update(&testChannel)
		if err != nil {
			return err
		}
	}

	testImages := []*dynamo.Image{
		{
			SetId:         testImageSetID,
			AssetType:     "1-dark-animated-1",
			AnimationType: "animated",
			Background:    "dark",
		},
		{
			SetId:         testImageSetID,
			AssetType:     "1-light-static-1",
			AnimationType: "static",
			Background:    "light",
		},
	}
	if d.Dryrun {
		for _, testImage := range testImages {
			log.WithField("images", testImage).Info("Dryrun: Images")
		}
	} else {
		for _, testImage := range testImages {
			log.WithField("images", testImage).Info("Real Run: Images")
			err := d.DynamoDAOs.ImagesDAO.Update(testImage)
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func (d *DynamoHandler) createTestDataForSpomoteStatuses() error {
	numOfRows := 20
	testCampaignPrefix := "hyperscape-megacommerce-test"

	for i := 1; i <= numOfRows; i++ {
		testUserStatus := user_campaign_statuses.SponsoredCheermoteUserStatus{
			UserID:      TestUserID,
			CampaignID:  fmt.Sprintf("%v-%d", testCampaignPrefix, i),
			LastUpdated: time.Now(),
			UsedBits:    1,
		}
		if d.Dryrun {
			log.WithField("userStatus", testUserStatus).Info("Dryrun: SponsoredCheermoteUserStatus")
		} else {
			log.WithField("userStatus", testUserStatus).Info("Real Run: SponsoredCheermoteUserStatus")
			err := d.DynamoDAOs.SpomoteUserStatusesDAO.Update(&testUserStatus)
			if err != nil {
				return err
			}
		}
	}

	for i := 1; i <= numOfRows; i++ {
		testEligibility := eligible_channels.SponsoredCheermoteEligibleChannel{
			ChannelID:   TestUserID,
			CampaignID:  fmt.Sprintf("%v-%d", testCampaignPrefix, i),
			LastUpdated: time.Now(),
		}
		if d.Dryrun {
			log.WithField("eligibility", testEligibility).Info("Dryrun: SponsoredCheermoteEligibleChannel")
		} else {
			log.WithField("eligibility", testEligibility).Info("Real Run: SponsoredCheermoteEligibleChannel")
			err := d.DynamoDAOs.SpomoteEligibleChannelsDAO.Update(&testEligibility)
			if err != nil {
				return err
			}
		}
	}

	for i := 1; i <= numOfRows; i++ {
		testChannelStatus := channel_statuses.SponsoredCheermoteChannelStatus{
			ChannelID:   TestUserID,
			CampaignID:  fmt.Sprintf("%v-%d", testCampaignPrefix, i),
			OptedIn:     true,
			LastUpdated: time.Now(),
		}
		if d.Dryrun {
			log.WithField("channelStatus", testChannelStatus).Info("Dryrun: SponsoredCheermoteChannelStatus")
		} else {
			log.WithField("channelStatus", testChannelStatus).Info("Real Run: SponsoredCheermoteChannelStatus")
			err := d.DynamoDAOs.SpomoteChannelStatusesDAO.Update(&testChannelStatus)
			if err != nil {
				return err
			}
		}

	}

	return nil
}
