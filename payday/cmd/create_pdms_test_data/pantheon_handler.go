package main

import (
	"context"
	"fmt"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	"code.justin.tv/commerce/payday/clients/pantheon"
)

type PantheonHandler struct {
	PantheonClient pantheon.IPantheonClientWrapper
	Dryrun         bool
}

const (
	channelPrefix            = "test-leaderboard-user"
	testUserPrefix           = "test-user"
	testEventPrefix          = "pdms-test-event"
	testTimeOfEvent          = 1599794333000000000 // arbitrary time
	testLeaderboardsPerGroup = 3
)

func NewPantheonHandler(pantheonClient pantheon.IPantheonClientWrapper, dryrun bool) *PantheonHandler {
	return &PantheonHandler{
		PantheonClient: pantheonClient,
		Dryrun:         dryrun,
	}
}

func (p *PantheonHandler) createTestDataForPantheon(ctx context.Context) error {
	channelCount := 0
	eventCount := 0

	// create fake leaderboard entries where pdms user is first place
	for i := 0; i < testLeaderboardsPerGroup; i, channelCount = i+1, channelCount+1 {
		groupingKey := fmt.Sprintf("%s-%d", channelPrefix, channelCount+1)

		leaders := [4]string{
			TestUserID,
			fmt.Sprintf("%s-%d", testUserPrefix, 1),
			fmt.Sprintf("%s-%d", testUserPrefix, 2),
			fmt.Sprintf("%s-%d", testUserPrefix, 3),
		}

		for j, leader := range leaders {
			eventID := fmt.Sprintf("%s-%d", testEventPrefix, eventCount)
			cheerAmount := len(leaders) - j
			err := p.publishEvent(ctx, eventID, groupingKey, leader, cheerAmount)
			if err != nil {
				return err
			}
			eventCount++
		}
	}

	// create fake leaderboard entries where pdms user is second place
	for i := 0; i < testLeaderboardsPerGroup; i, channelCount = i+1, channelCount+1 {
		groupingKey := fmt.Sprintf("%s-%d", channelPrefix, channelCount+1)

		leaders := [4]string{
			fmt.Sprintf("%s-%d", testUserPrefix, 1),
			TestUserID,
			fmt.Sprintf("%s-%d", testUserPrefix, 2),
			fmt.Sprintf("%s-%d", testUserPrefix, 3),
		}

		for j, leader := range leaders {
			eventID := fmt.Sprintf("%s-%d", testEventPrefix, eventCount)
			cheerAmount := len(leaders) - j
			err := p.publishEvent(ctx, eventID, groupingKey, leader, cheerAmount)
			if err != nil {
				return err
			}
			eventCount++
		}
	}

	// create fake leaderboard entries where pdms user is third place
	for i := 0; i < testLeaderboardsPerGroup; i, channelCount = i+1, channelCount+1 {
		groupingKey := fmt.Sprintf("%s-%d", channelPrefix, channelCount+1)

		leaders := [4]string{
			fmt.Sprintf("%s-%d", testUserPrefix, 1),
			fmt.Sprintf("%s-%d", testUserPrefix, 2),
			TestUserID,
			fmt.Sprintf("%s-%d", testUserPrefix, 3),
		}

		for j, leader := range leaders {
			eventID := fmt.Sprintf("%s-%d", testEventPrefix, eventCount)
			cheerAmount := len(leaders) - j
			err := p.publishEvent(ctx, eventID, groupingKey, leader, cheerAmount)
			if err != nil {
				return err
			}
			eventCount++
		}
	}

	// create fake leaderboard entries where only pdms user cheered
	for i := 0; i < testLeaderboardsPerGroup; i, channelCount = i+1, channelCount+1 {
		groupingKey := fmt.Sprintf("%s-%d", channelPrefix, channelCount+1)

		eventID := fmt.Sprintf("%s-%d", testEventPrefix, eventCount)
		cheerAmount := 1
		err := p.publishEvent(ctx, eventID, groupingKey, TestUserID, cheerAmount)
		if err != nil {
			return err
		}
		eventCount++
	}

	return nil
}

func (p *PantheonHandler) publishEvent(ctx context.Context, eventID, groupingKey, entryKey string, cheerAmount int) error {
	log := log.WithFields(log.Fields{
		"eventID":     eventID,
		"channel":     groupingKey,
		"user":        entryKey,
		"cheerAmount": cheerAmount,
	})
	if p.Dryrun {
		log.Info("Dryrun: will not publish event")
	} else {
		log.Info("Real Run: publishing event")
		err := p.PantheonClient.PublishEvent(ctx, pantheonrpc.PublishEventReq{
			Domain:      pantheon.PantheonBitsUsageDomain,
			EventId:     eventID,
			TimeOfEvent: testTimeOfEvent,
			GroupingKey: groupingKey,
			EntryKey:    entryKey,
			EventValue:  int64(cheerAmount),
		})
		if err != nil {
			return err
		}
	}
	return nil
}
