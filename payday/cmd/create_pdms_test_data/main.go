package main

import (
	"context"
	"os"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/backend/clients/pantheon"
	"code.justin.tv/commerce/payday/backend/clients/pdms"
	"code.justin.tv/commerce/payday/config"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/codegangsta/cli"
)

var dryrun bool

const environment = "staging"
const TestUserID = "test-pdms-user"

func main() {
	app := cli.NewApp()
	app.Name = "CreatePDMSTestData"

	app.Flags = []cli.Flag{
		cli.BoolFlag{
			Name:        "dryrun",
			Usage:       "Pass in this flag to only show things to be added and not actually add them",
			Destination: &dryrun,
		},
	}

	app.Action = handle

	err := app.Run(os.Args)
	if err != nil {
		panic(err)
	}
}

func handle(c *cli.Context) {
	err := config.Load(environment)
	if err != nil {
		log.WithError(err).Panicf("error loading config")
	}
	cfg := config.Get()

	log.WithFields(log.Fields{
		"environment": environment,
		"dryrun":      dryrun,
		"testUserID":  TestUserID,
	}).Info("Loaded config")

	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(2)*time.Minute)
	defer cancel()

	log.Info("[S3 Test Data Creation]")
	s3Handler := NewS3Handler(&cfg, dryrun)
	err = s3Handler.createTestAssetForBitsOnboardEvents(ctx)
	if err != nil {
		panic(err)
	}
	err = s3Handler.createTestAssetForCheermotes(ctx)
	if err != nil {
		panic(err)
	}

	log.Info("[Dynamo Test Data Creation]")
	sess, err := session.NewSession(&aws.Config{
		Region: aws.String("us-west-2"),
	})
	if err != nil {
		log.WithError(err).Panic("Could not start new aws session")
	}
	dynamoHandler := NewDynamoHandler(&cfg, sess, dryrun)
	err = dynamoHandler.createTestDataForPurchaseBySuperPlatform(ctx)
	if err != nil {
		panic(err)
	}
	err = dynamoHandler.createTestDataForSinglePurchaseConsumption(ctx)
	if err != nil {
		panic(err)
	}
	err = dynamoHandler.createTestDataForBadgeTiersAndEmoteGroupIDs(ctx)
	if err != nil {
		panic(err)
	}
	err = dynamoHandler.createTestDataForUsersTable()
	if err != nil {
		panic(err)
	}
	err = dynamoHandler.createTestDataForLeaderboardBadgeHolders()
	if err != nil {
		panic(err)
	}
	err = dynamoHandler.createTestDataForChannelsAndImages()
	if err != nil {
		panic(err)
	}
	err = dynamoHandler.createTestDataForSpomoteStatuses()
	if err != nil {
		panic(err)
	}

	log.Info("[Pantheon Test Data Creation]")
	statsClient, err := statsd.NewNoopClient()
	if err != nil {
		log.WithError(err).Panic("Could not create noop statter")
	}
	pantheonClient := pantheon.NewClient(cfg, statsClient)
	pantheonHandler := NewPantheonHandler(pantheonClient, dryrun)
	err = pantheonHandler.createTestDataForPantheon(ctx)
	if err != nil {
		panic(err)
	}

	log.Info("[PDMS Test Data Creation]")
	pdmsClient := pdms.NewClient(cfg)
	pdmsHandler := NewPDMSHandler(pdmsClient, dryrun)
	err = pdmsHandler.createTestDataForPDMS(ctx)
	if err != nil {
		panic(err)
	}

	log.Info("Finished!")
}
