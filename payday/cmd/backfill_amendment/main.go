package main

import (
	"bytes"
	"encoding/csv"
	"fmt"
	"io/ioutil"

	"code.justin.tv/commerce/payday/audit"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/iam"
	"code.justin.tv/commerce/payday/lambda"
	"code.justin.tv/commerce/payday/s3"
	"code.justin.tv/commerce/payday/utils/pointers"
)

const (
	inFile      = "/Users/seph/GoWorkspace/src/code.justin.tv/commerce/payday/cmd/backfill_amendment/bits_amendment.csv"
	environment = "development"
	dryrun      = true
)

func main() {
	err := config.Load(environment)
	if err != nil {
		panic(err)
	}

	cfg := config.Get()
	if err := cfg.Validate(); err != nil {
		panic(err)
	}

	s3Client := s3.NewFromDefaultConfig()
	lambdaClient := lambda.NewFromDefaultConfig()
	iamClient := iam.NewFromDefaultConfig()
	auditSetup := audit.NewAuditSetup(s3Client, lambdaClient, iamClient)

	dynamoClient := dynamo.NewClientWithAudit(&dynamo.DynamoClientConfig{
		AwsRegion:   cfg.DynamoRegion,
		TablePrefix: cfg.DynamoTablePrefix,
	}, auditSetup, &dynamo.DynamoAuditConfig{
		RecordsS3Bucket:      cfg.AuditRecordsS3Bucket,
		LambdaMemorySizeMB:   cfg.AuditLambdaMemorySizeMB,
		LambdaTimeoutSec:     cfg.AuditLambdaTimeoutSec,
		LambdaRoleName:       cfg.AuditLambdaRoleName,
		LambdaCodeS3Bucket:   cfg.AuditLambdaCodeS3Bucket,
		LambdaEventBatchSize: cfg.AuditLambdaEventBatchSize,
	})
	channelDao := dynamo.NewChannelDao(dynamoClient)

	fileBytes, err := ioutil.ReadFile(inFile)
	if err != nil {
		panic(err)
	}

	fileBytesReader := bytes.NewReader(fileBytes)
	csvReader := csv.NewReader(fileBytesReader)

	rows, err := csvReader.ReadAll()
	if err != nil {
		panic(err)
	}

	new := []string{}
	updated := []string{}
	skipped := []string{}

	totalRows := len(rows)
	for rowNum, row := range rows {
		fmt.Printf("%d / %d\n", rowNum+1, totalRows)
		tuid := row[0]
		amendmentState := row[1]

		if amendmentState != "signed" && amendmentState != "unsigned" {
			panic(fmt.Sprintf("Invalid amendment state: %s", amendmentState))
		}

		channel, err := channelDao.Get(dynamo.ChannelId(tuid))
		if err != nil {
			panic(err)
		}

		if channel == nil {
			channel := &dynamo.Channel{
				Id:             dynamo.ChannelId(tuid),
				Annotation:     pointers.StringP("Amendment State Backfill"),
				AmendmentState: pointers.StringP(amendmentState),
			}
			if !dryrun {
				err := channelDao.Update(channel)
				if err != nil {
					panic(err)
				}
			}
			new = append(new, tuid)
		} else {
			previousState := pointers.StringOrDefault(channel.AmendmentState, "")
			if previousState == "signed" {
				skipped = append(skipped, tuid)
			} else {
				channel.AmendmentState = pointers.StringP(amendmentState)
				if !dryrun {
					err := channelDao.Update(channel)
					if err != nil {
						panic(err)
					}
				}
				updated = append(updated, tuid)
			}
		}
	}

	fmt.Printf("New: %d \n", len(new))
	fmt.Printf("Skipped: %d \n", len(skipped))
	fmt.Printf("Updated: %d \n", len(updated))

}
