package main

import (
	"encoding/csv"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"sort"
	"strings"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/dynamo/extension_transaction"
	"github.com/codegangsta/cli"
)

var (
	environment string
	input       string
	output      string
	isExt       bool
)

func main() {
	app := cli.NewApp()
	app.Name = "Accounting Audit"
	app.Usage = "Get Transactions"

	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "environment, e",
			Value:       "staging",
			Usage:       "Runtime config to use. Options are: 'production' and 'staging'. Defaults to 'staging' if unset.",
			EnvVar:      "environment",
			Destination: &environment,
		},
		cli.StringFlag{
			Name:        "input, in, i",
			Usage:       "the csv file you are using, we'll assume there is a header row and use just 1st column",
			Destination: &input,
		},
		cli.StringFlag{
			Name:        "output, out, o",
			Usage:       "the name of file you'll be outputting to, this will be a json file",
			Destination: &output,
		},
		cli.BoolFlag{
			Name:        "extension, ext",
			Usage:       "if set, fetches transactions from extension transactions table instead of transactions table",
			Destination: &isExt,
		},
	}

	app.Action = runProgram
	err := app.Run(os.Args)
	if err != nil {
		panic(err)
	}
}

func runProgram(c *cli.Context) {
	log.Infof("Loading file: %s", input)
	txIds := getTXFromCSV()

	log.Info("Loading config file")
	err := config.Load(environment)
	if err != nil {
		log.WithError(err).Panic("Error while loading configuration file")
	}
	log.Infof("[OPTIONS] Environment: %s", environment)

	cfg := config.Get()
	if err := cfg.Validate(); err != nil {
		log.WithError(err).Panic("Rekt while validating configuration file")
	}

	var awsRegion string
	if environment == "production" {
		awsRegion = "us-east-2"
	} else {
		awsRegion = "us-west-2"
	}

	dynamoClient := dynamo.NewClient(&dynamo.DynamoClientConfig{
		AwsRegion:   awsRegion,
		TablePrefix: cfg.DynamoTablePrefix,
	})
	if isExt {
		extTxDAO := extension_transaction.NewExtensionsTransactionsDAO(dynamoClient)
		txs, err := extTxDAO.GetByTransactionIDs(txIds)
		if err != nil {
			log.WithError(err).Panic("Rekt while trying to get extension transaction")
		}
		sort.Slice(txs, func(i, j int) bool {
			return txs[i].TransactionID < txs[j].TransactionID
		})
		saveToJSON(txs)
	} else {
		txDAO := dynamo.NewTransactionsDao(dynamoClient)
		txs, err := txDAO.BatchGet(txIds)
		if err != nil {
			log.WithError(err).Panic("Rekt while trying to get extension transaction")
		}
		sort.Slice(txs, func(i, j int) bool {
			return txs[i].TransactionId < txs[j].TransactionId
		})
		saveToJSON(txs)
	}
}

func getTXFromCSV() []string {
	csvfile, err := os.Open(input)
	if err != nil {
		log.WithError(err).Panicf("Rekt while opening csv %s", input)
	}
	r := csv.NewReader(csvfile)
	var txIds []string

	headerSkipped := false
	for {
		record, err := r.Read()
		if !headerSkipped {
			headerSkipped = true
			continue
		}
		if err == io.EOF {
			break
		}
		if err != nil {
			log.WithError(err).Panicf("Rekt while parsing csv %s", input)
		}
		txId := strings.Trim(record[0], " ")
		txIds = append(txIds, txId)
		log.Info(txId)
	}
	return txIds
}

func saveToJSON(txs interface{}) {
	file, err := json.MarshalIndent(txs, "", " ")
	if err != nil {
		log.WithError(err).Panic("Rekt while marshalling transactions to json")
	}
	outputname := fmt.Sprintf("%s.json", output)
	err = ioutil.WriteFile(outputname, file, 0644)
	if err != nil {
		log.WithError(err).Panic("Rekt while saving json file")
	}
	log.Infof("Finished, file saved to %s", outputname)
}
