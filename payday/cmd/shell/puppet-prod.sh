# /bin/bash
# Requires jsawk to be installed
hosts=$(./list-prod.sh)

read -p "Are you sure? (y/n)" -n 1 -r
echo    # Move to a new line
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
   exit 1
fi

pdsh -b -w $hosts "sudo puppet agent --test"
