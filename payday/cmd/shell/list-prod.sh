# /bin/bash
# Requires jsawk to be installed
aws ec2 describe-instances --filters "Name=tag:Environment,Values=prod" "Name=tag:Service,Values=commerce/payday" | jsawk 'return this.Reservations' | jsawk 'RS=RS.concat(this.Instances); return null' | jsawk 'return this.PrivateIpAddress' | tr -d '[]"'
