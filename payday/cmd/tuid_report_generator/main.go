package main

import (
	"bufio"
	"context"
	"encoding/csv"
	"os"

	"code.justin.tv/amzn/TwitchS2S2/s2s2"
	"code.justin.tv/commerce/gogogadget/pointers"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/backend"
	s2s2client "code.justin.tv/commerce/payday/backend/s2s2"
	"code.justin.tv/commerce/payday/clients/cloudwatchlogger"
	"code.justin.tv/commerce/payday/config"
	fileUtil "code.justin.tv/commerce/payday/utils/close"
	"github.com/codegangsta/cli"
)

const (
	allBadTuidsLocal = "/Users/seph/Sandbox/all-bad-tuids-unique"
	reportLocal      = "/Users/seph/Sandbox/asymmetric-link.csv"
)

var tuidFilePath string
var destReportFilePath string
var environment string

func main() {
	dynamoSetup := cli.NewApp()
	dynamoSetup.Name = "Payday User Report"
	dynamoSetup.Usage = "Makes it rain (by generating user reports)"

	dynamoSetup.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "environment, e",
			Value:       "production",
			Usage:       "Runtime config to use. Options are: 'production' and 'staging'. Defaults to 'production' if unset.",
			EnvVar:      "payday_APP_ENV",
			Destination: &environment,
		},
		cli.StringFlag{
			Name:        "source, s",
			Value:       allBadTuidsLocal,
			Usage:       "path to tuids file",
			Destination: &tuidFilePath,
		},
		cli.StringFlag{
			Name:        "dest, d",
			Value:       reportLocal,
			Usage:       "path to dest report",
			Destination: &destReportFilePath,
		},
	}

	dynamoSetup.Action = genReport
	err := dynamoSetup.Run(os.Args)
	if err != nil {
		log.WithError(err).Error("Run returned an error")
	}
}

func genReport(c *cli.Context) {
	file, err := os.Open(tuidFilePath)
	defer fileUtil.Close(file)
	if err != nil {
		panic(err)
	}

	err = config.Load(environment)
	if err != nil {
		log.WithError(err).Panicf("Rekt while loading config")
	}

	configuration := config.Get()
	if environment != config.DevelopmentEnvironment {
		err = configuration.Validate()
		if err != nil {
			log.WithError(err).Panicf("Configuration file could not be validated")
		}
	}

	s2s2logger := cloudwatchlogger.NewCloudWatchLogNoopClient()
	var s2s2Client *s2s2.S2S2
	if configuration.S2S2.Enabled {
		s2s2logger, err = cloudwatchlogger.NewCloudWatchLogClient(configuration.CloudWatchRegion, configuration.S2S2.AuthLogGroupName)
		if err != nil {
			log.WithError(err).Fatal("Unable to create s2s2 logger")
		}

		s2s2Client, err = s2s2client.NewS2S2Client(configuration, cloudwatchlogger.AdaptToTwitchLoggingLogger(s2s2logger, configuration.S2S2.AuthLogGroupName))
		if err != nil {
			log.WithError(err).Panic("Error initializing s2s2 client")
		}
	}
	defer s2s2logger.Shutdown()

	b, err := backend.Provide(configuration, s2s2Client)
	if err != nil {
		panic(err)
	}

	r := bufio.NewReader(file)
	s := bufio.NewScanner(r)
	s.Split(bufio.ScanLines)

	destFile, err := os.Create(destReportFilePath)
	defer fileUtil.Close(destFile)

	if err != nil {
		panic(err)
	}

	count := 1
	writer := csv.NewWriter(destFile)
	ctx := context.Background()
	for s.Scan() {
		log.Infof("Record #: %d", count)

		tuid := s.Text()
		login := ""
		email := ""
		language := ""

		user, err := b.Clients.UserServiceClient.GetUserByID(ctx, tuid, nil)
		if err != nil {
			panic(err)
		}

		if user != nil {
			login = pointers.StringOrDefault(user.Login, "<login not found>")
			email = pointers.StringOrDefault(user.Email, "<email not found>")
			language = pointers.StringOrDefault(user.Language, "<language not found>")
		}

		record := []string{tuid, login, email, language}
		err = writer.Write(record)
		if err != nil {
			panic(err)
		}
		count++
	}
	writer.Flush()
}
