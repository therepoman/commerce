package main

import (
	"context"
	"fmt"
	"math/rand"
	"os"
	"syscall"
	"time"

	identifier "code.justin.tv/amzn/TwitchProcessIdentifier"
	"code.justin.tv/amzn/TwitchProcessIdentifier/expvars"
	"code.justin.tv/amzn/TwitchS2S2/s2s2"
	g "code.justin.tv/chat/golibs/graceful"
	gogoctx "code.justin.tv/commerce/gogogadget/ctx"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/backend"
	"code.justin.tv/commerce/payday/backend/mux"
	"code.justin.tv/commerce/payday/backend/s2s"
	s2s2client "code.justin.tv/commerce/payday/backend/s2s2"
	"code.justin.tv/commerce/payday/backend/sandstorm"
	"code.justin.tv/commerce/payday/clients/cloudwatchlogger"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/common/golibs/bininfo"
	"code.justin.tv/video/autoprof/profs3"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/codegangsta/cli"
	goji_graceful "github.com/zenazn/goji/graceful"
)

var environment string
var disableSQSHandlers bool

func main() {
	payday := cli.NewApp()
	payday.Name = "Payday"
	payday.Usage = "Makes it rain"

	payday.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "environment, e",
			Value:       "production",
			Usage:       "Runtime config to use. Options are: 'production' and 'staging'. Defaults to 'production' if unset.",
			EnvVar:      "payday_APP_ENV",
			Destination: &environment,
		},
		cli.BoolFlag{
			Name:        "disableSQSHandlers",
			Usage:       "Prevents start of SQS handlers if true.",
			EnvVar:      "DISABLE_SQS_HANDLERS",
			Destination: &disableSQSHandlers,
		},
	}

	payday.Action = startPayday
	err := payday.Run(os.Args)
	if err != nil {
		log.WithError(err).Error("Run returned an err")
	}
}

func startPayday(c *cli.Context) {
	// Timeout for waiting to gracefully shut the service down
	g.ShutdownTimeout = 30 * time.Second

	var err error

	log.SetOutput(os.Stderr)

	// Do not log context cancellation errors
	log.AddHook(gogoctx.NewCtxCancellationLogrusHook())

	hostname, err := os.Hostname()
	if err != nil {
		log.WithError(err).Error("error getting hostname from OS")
	}

	log.Infof("Starting payday (env: %s, hostname: %s, revision: %s)", environment, hostname, bininfo.Revision())

	// Seed the RNG based on the current time in nanoseconds
	rand.Seed(time.Now().UnixNano())

	err = config.Load(environment)
	if err != nil {
		log.WithError(err).Panicf("Rekt while loading config")
	}

	configuration := config.Get()
	if environment != config.DevelopmentEnvironment {
		err = configuration.Validate()
		if err != nil {
			log.WithError(err).Panicf("Configuration file could not be validated")
		}
	}

	configuration, err = startSandstorm(configuration)
	if err != nil {
		log.WithError(err).Panic("Could not start sandstorm client")
	}

	s2s2logger := cloudwatchlogger.NewCloudWatchLogNoopClient()
	var s2s2Client *s2s2.S2S2
	if configuration.S2S2.Enabled {
		s2s2logger, err = cloudwatchlogger.NewCloudWatchLogClient(configuration.CloudWatchRegion, configuration.S2S2.AuthLogGroupName)
		if err != nil {
			log.WithError(err).Fatal("Unable to create s2s2 logger")
		}

		s2s2Client, err = s2s2client.NewS2S2Client(configuration, cloudwatchlogger.AdaptToTwitchLoggingLogger(s2s2logger, configuration.S2S2.AuthLogGroupName))
		if err != nil {
			log.WithError(err).Panic("Error initializing s2s2 client")
		}
	}

	b, err := backend.Provide(configuration, s2s2Client)
	if err != nil {
		log.WithError(err).Panic("Could not initialize backend")
	}

	if configuration.Environment != config.DevelopmentEnvironment {
		for _, poller := range b.Pollers.List {
			err := poller.Init()
			if err != nil {
				panic(err)
			}
			g.Go(poller.Start)
		}
	}

	if disableSQSHandlers {
		log.Infoln("Starting Payday without SQS Handlers")
	} else {
		b.SQS.Start()
		log.Infoln("SQS processor initialization complete")
	}

	startProfiler(configuration, "Payday", environment, "primary", bininfo.Revision())

	// Makes goji respect SIGINT, SIGTERM, and SIGKILL for graceful shutdown
	goji_graceful.AddSignal(syscall.SIGTERM, syscall.SIGKILL)
	goji_graceful.HandleSignals()

	s2sClient := s2s.Init(configuration)
	parentMux, secureMux := mux.New(configuration, b, s2sClient, s2s2Client)

	// When goji begins the shutdown process, start shutting down:
	// * SQS processors
	// * Audit Logger that sends to CW
	// * S2S Auth Logger that sends to CW
	var shutdownStart time.Time
	goji_graceful.PreHook(func() {
		shutdownStart = time.Now()
		g.Shutdown()
		b.Clients.AuditLogger.Shutdown()
		s2s2logger.Shutdown()
		log.Info("Initiated shutdown process")
	})

	go func() {
		err := goji_graceful.ListenAndServe(fmt.Sprintf(":%d", configuration.AdminPort), secureMux)
		if err != nil {
			log.WithError(err).Error("Error returned from internal mux")
		}
	}()

	log.Infoln("Server is now listening")
	err = goji_graceful.ListenAndServe(fmt.Sprintf(":%d", configuration.PaydayRestPort), parentMux)
	if err != nil {
		log.WithError(err).Fatal("Error returned from main mux")
	}

	err = g.Wait()
	if err != nil {
		log.WithError(err).Error("Error returned from wait method")
	}

	log.Infof("Finished shutting down in: %v", time.Since(shutdownStart))
}

func startProfiler(c config.Configuration, service, stage, substage, version string) {
	if c.AutoprofBucket == "" {
		log.Info("autoprof: Skipping autoprof as AutoprofBucket was not set")
		return // Skip if the bucket isn't set
	}

	pid := identifier.ProcessIdentifier{
		Service:  service,
		Stage:    stage,
		Substage: substage,
		// Region env var provided by ECS
		Region: os.Getenv("AWS_DEFAULT_REGION"),
		// Task id (obtained from task ARN)
		Machine:  os.Getenv("SERVICE_TASK_ID"),
		LaunchID: os.Getenv("HOSTNAME"),
		Version:  version,
	}
	expvars.Publish(&pid)

	go func() {
		sess, err := session.NewSession(&aws.Config{
			Region: aws.String(c.SQSRegion),
		})
		if err != nil {
			log.WithError(err).Error("autoprof: Error starting aws session")
			return
		}
		autoprofCollector := profs3.Collector{
			S3:       s3.New(sess),
			S3Bucket: c.AutoprofBucket,
			OnError: func(err error) error {
				log.WithError(err).Error("autoprof profs3.Collector.Run error")
				return err
			},
		}
		err = autoprofCollector.Run(context.Background())
		if err != nil {
			log.WithError(err).Error("autoprof: Error starting autoprof collector")
			return
		}
		log.Info("autoprof: Started autoprof in bucket", c.AutoprofBucket)
	}()
}

func startSandstorm(cfg config.Configuration) (config.Configuration, error) {
	sandstormGetter := sandstorm.NewSandstorm(cfg)

	// populate config with slack webhook URLs from sandstorm
	var sandstormErr error
	cfg.CustomCheermoteChannelSlackWebhookURL, sandstormErr = sandstorm.GetSecretPlainText(sandstormGetter, cfg.SandstormCustomCheermoteChannelSlackWebhookURLKey)
	if sandstormErr != nil {
		return cfg, errors.Newf("Could not get CustomCheermoteChannelSlackWebhookURL from sandstorm: %s", sandstormErr)
	}

	cfg.CustomBadgesChannelSlackWebhookURL, sandstormErr = sandstorm.GetSecretPlainText(sandstormGetter, cfg.SandstormCustomBadgeChannelSlackWebhookURLKey)
	if sandstormErr != nil {
		return cfg, errors.Newf("Could not get CustomBadgesChannelSlackWebhookURL from sandstorm: %s", sandstormErr)
	}

	cfg.TMIFailuresSlackWebhookURL, sandstormErr = sandstorm.GetSecretPlainText(sandstormGetter, cfg.SandstormTMIFailuresSlackWebhookURLKey)
	if sandstormErr != nil {
		return cfg, errors.Newf("Could not get TMIFailuresSlackWebhookURL from sandstorm: %s", sandstormErr)
	}

	cfg.BigBitsSlackWebhookURL, sandstormErr = sandstorm.GetSecretPlainText(sandstormGetter, cfg.SandstormBigBitsSlackWebhookURLKey)
	if sandstormErr != nil {
		return cfg, errors.Newf("Could not get BigBitsSlackWebhookURL from sandstorm: %s", sandstormErr)
	}

	return cfg, nil
}
