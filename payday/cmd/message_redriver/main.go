package main

import (
	"os"

	log "code.justin.tv/commerce/logrus"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	awssqs "github.com/aws/aws-sdk-go/service/sqs"
	"github.com/codegangsta/cli"
)

var sourceQueueUrl string
var destinationQueueUrl string

var sqsClient *awssqs.SQS

const (
	sqsRegion          = "us-west-2"
	prodPubSubQueueUrl = "https://sqs.us-west-2.amazonaws.com/021561903526/payday_pubsub_queue"
	prodPubSubDlqUrl   = "https://sqs.us-west-2.amazonaws.com/021561903526/payday_pubsub_queue_deadletter"
)

func main() {
	messageDumper := cli.NewApp()
	messageDumper.Name = "Payday Message Redriver"
	messageDumper.Usage = "Makes it rain (by redriving SQS messages between queues)"

	messageDumper.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "source, s",
			Value:       prodPubSubDlqUrl,
			Usage:       "Source SQS queue URL. Defaults to prod pub sub DLQ.",
			Destination: &sourceQueueUrl,
		},
		cli.StringFlag{
			Name:        "destination, d",
			Value:       prodPubSubQueueUrl,
			Usage:       "Destination SQS queue URL. Defaults to prod pub sub queue.",
			Destination: &destinationQueueUrl,
		},
	}

	sess, _ := session.NewSession()
	sqsClient = awssqs.New(sess, &aws.Config{Region: aws.String(sqsRegion)})

	messageDumper.Action = redriveMessages
	err := messageDumper.Run(os.Args)
	if err != nil {
		panic(err)
	}
}

func pollSourceMessages() ([]*awssqs.Message, error) {
	messages := make([]*awssqs.Message, 0)

	for {
		params := &awssqs.ReceiveMessageInput{
			QueueUrl: aws.String(sourceQueueUrl),
			AttributeNames: []*string{
				aws.String("All"),
			},
			MaxNumberOfMessages: aws.Int64(10),
			MessageAttributeNames: []*string{
				aws.String("All"),
			},
			VisibilityTimeout: aws.Int64(10),
			WaitTimeSeconds:   aws.Int64(1),
		}

		resp, err := sqsClient.ReceiveMessage(params)
		if err != nil {
			log.WithError(err).Error("Unable to receive message from SQS queue")
			return nil, err
		}

		if len(resp.Messages) > 0 {
			messages = append(messages, resp.Messages...)
		} else {
			return messages, nil
		}
	}
}

func redriveMessages(c *cli.Context) {
	log.Infof("Polling messages...")
	messages, err := pollSourceMessages()

	log.Infof("Found %d messages", len(messages))
	if err != nil {
		return
	}

	log.Infof("Redriving messages...")
	for _, message := range messages {
		redriveMessage(message)
	}
}

func redriveMessage(message *awssqs.Message) {
	log.Infof("Redriving message %s...", *message.MessageId)
	_, err := sqsClient.SendMessage(&awssqs.SendMessageInput{
		QueueUrl:    aws.String(destinationQueueUrl),
		MessageBody: message.Body,
	})

	if err != nil {
		log.WithError(err).WithField("messageId", *message.MessageId).Error("Failed to redrive message")
		return
	}

	log.Infof("Deleting message %s from originl queue...", *message.MessageId)
	_, err = sqsClient.DeleteMessage(&awssqs.DeleteMessageInput{
		QueueUrl:      aws.String(sourceQueueUrl),
		ReceiptHandle: message.ReceiptHandle,
	})

	if err != nil {
		log.WithError(err).WithField("messageId", *message.MessageId).Error("Failed to delete message after redriving")
		return
	}
}
