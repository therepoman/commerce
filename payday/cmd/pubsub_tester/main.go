package main

import (
	"context"
	"os"
	"time"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/backend/sandstorm"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/common/golibs/pubsubclient"
	"github.com/codegangsta/cli"
)

const (
	pubsubEnpoint            = "wss://pubsub-edge.twitch.tv"
	qaBitsTestsPrivatePubSub = "channel-bitsevents.145903336"
	sandstormSecretName      = "commerce/payday/staging/qa_bits_partner_oauth_token"
)

var environment string
var topic string

func main() {
	payday := cli.NewApp()
	payday.Name = "pubsub tester"
	payday.Usage = "Makes it rain"

	payday.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "environment, e",
			Value:       "staging",
			Usage:       "Runtime config to use. Options are: 'production' and 'staging'. Defaults to 'production' if unset.",
			EnvVar:      "ENVIRONMENT",
			Destination: &environment,
		},
		cli.StringFlag{
			Name:        "topic, t",
			Usage:       "used to record who started the job",
			Value:       qaBitsTestsPrivatePubSub,
			Destination: &topic,
		},
	}

	payday.Action = startPubsubTest
	err := payday.Run(os.Args)
	if err != nil {
		logrus.WithError(err).Error("Run returned an err")
	}
}

func startPubsubTest(c *cli.Context) {
	logrus.SetOutput(os.Stdout)

	err := config.Load(environment)
	if err != nil {
		panic(err)
	}

	cfg := config.Get()
	if err := cfg.Validate(); err != nil {
		panic(err)
	}

	sandstormClient := sandstorm.NewSandstorm(cfg)
	oAuthSecret, err := sandstormClient.Get(sandstormSecretName)
	if err != nil {
		logrus.WithError(err).Panic("could not fetch OAuth token from Sandstorm")
	} else if oAuthSecret == nil {
		logrus.Panic("OAuth secret value was nil")
	}

	oAuthToken := string(oAuthSecret.Plaintext)

	client := pubsubclient.New(pubsubEnpoint, nil)
	err = client.Subscribe(topic, oAuthToken)
	if err != nil {
		logrus.WithError(err).Panic("Could not subscribe to pubsub")
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Minute*10)
	defer cancel()
	output := make(chan *pubsubclient.Message)
	go func() {
		for {
			msg, err := client.NextMessage()
			if err != nil {
				return
			} else if err == pubsubclient.ErrDisconnected {
				//see: http://bit.ly/2j7EkmW
				continue
			}

			output <- msg
		}
	}()

	select {
	case o := <-output:
		logrus.WithFields(logrus.Fields{
			"message": o.Message,
			"topic":   o.Topic,
		}).Infof("received message on pubsub topic")
	case <-ctx.Done():
		logrus.Panic("timed out waiting")
	}
}
