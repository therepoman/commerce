package main

import (
	"bufio"
	"context"
	"encoding/csv"
	"io"
	"os"
	"time"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/audit"
	"code.justin.tv/commerce/payday/backend/clients/user"
	userService "code.justin.tv/commerce/payday/clients/user"
	"code.justin.tv/commerce/payday/config"
	spadeModel "code.justin.tv/commerce/payday/datascience"
	datascience "code.justin.tv/commerce/payday/datascience/impl"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/httputils"
	"code.justin.tv/commerce/payday/iam"
	"code.justin.tv/commerce/payday/lambda"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/s3"
	"code.justin.tv/commerce/payday/userservice"
	"code.justin.tv/foundation/twitchclient"
	userServiceModel "code.justin.tv/web/users-service/models"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/codegangsta/cli"
	"golang.org/x/time/rate"
)

var inputFile string

func main() {
	script := cli.NewApp()
	script.Name = "Datascience (bits_acquired_v2) backfill script"
	script.Usage = "Backfill"

	script.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "inputFile, i",
			Usage:       "the input file with transaction ids to backfill",
			EnvVar:      "INPUT_FILE",
			Destination: &inputFile,
		},
	}

	script.Action = run
	err := script.Run(os.Args)
	if err != nil {
		logrus.WithError(err).Error("Run returned an err")
	}

}

func run(c *cli.Context) {
	logrus.SetOutput(os.Stdout)

	cfg := getProdConfig()
	s3Client := s3.NewFromDefaultConfig()
	lambdaClient := lambda.NewFromDefaultConfig()
	iamClient := iam.NewFromDefaultConfig()
	auditSetup := audit.NewAuditSetup(s3Client, lambdaClient, iamClient)
	dynamoClient := dynamo.NewClientWithAudit(&dynamo.DynamoClientConfig{
		AwsRegion:   cfg.DynamoRegion,
		TablePrefix: cfg.DynamoTablePrefix,
	}, auditSetup, &dynamo.DynamoAuditConfig{
		RecordsS3Bucket:      cfg.AuditRecordsS3Bucket,
		LambdaMemorySizeMB:   cfg.AuditLambdaMemorySizeMB,
		LambdaTimeoutSec:     cfg.AuditLambdaTimeoutSec,
		LambdaRoleName:       cfg.AuditLambdaRoleName,
		LambdaCodeS3Bucket:   cfg.AuditLambdaCodeS3Bucket,
		LambdaEventBatchSize: cfg.AuditLambdaEventBatchSize,
	})

	transactionDAO := dynamo.NewTransactionsDao(dynamoClient)
	datascienceClient, err := datascience.NewDataScienceApiWithUserService(spadeModel.DataScienceApiParams{
		HttpUtilClient:     httputils.NewDefaultHttpUtilClientApi(),
		DataScienceEnabled: cfg.DataScienceEnabled,
		SpadeEnabled:       cfg.DataScienceSpadeEnabled,
		SpadeURL:           cfg.DataScienceSpadeURL,
	}, getUserFetcher(cfg))
	if err != nil {
		logrus.WithError(err).Panic("Could not instantiate data science client")
	}

	ctx := context.Background()
	csvFile, _ := os.Open(inputFile)
	csvReader := csv.NewReader(bufio.NewReader(csvFile))
	successCounter := 0
	failureCounter := 0
	rateLimiter := rate.NewLimiter(rate.Limit(10), 10)

	logrus.Infof("starting the backfill")
	for {
		row, err := csvReader.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			logrus.WithError(err).Errorf("failed to read transactionID at transaction successCounter: %d", successCounter)
			return
		}
		if len(row) < 1 {
			logrus.Errorf("transactionID is not present in the input file at transaction successCounter: %d", successCounter)
			return
		}

		transactionID := row[0]
		transaction, err := transactionDAO.Get(transactionID)
		if err != nil {
			logrus.
				WithField("transaction_id", transactionID).
				WithError(err).Error("failed to get transaction from dynamo")
			failureCounter++
			continue
		}

		bitsAcquiredV2 := getSpadeModel(transaction)
		if bitsAcquiredV2 == nil {
			failureCounter++
			continue
		}

		_ = rateLimiter.Wait(ctx)
		err = datascienceClient.TrackBitsEvent(ctx, spadeModel.BitsAcquiredEventNameV2, bitsAcquiredV2)
		if err != nil {
			logrus.
				WithField("transactionId", bitsAcquiredV2.TransactionID).
				WithError(err).Error("failed to send to spade")
			failureCounter++
		} else {
			successCounter++
		}

		if (successCounter+failureCounter)%1000 == 0 {
			logrus.Infof("%d transactions processed", successCounter+failureCounter)
		}
	}

	logrus.Infof("finished. %d records sent, %d records NOT sent", successCounter, failureCounter)
}

func getProdConfig() config.Configuration {
	return config.Configuration{
		DynamoRegion:              "us-west-2",
		DynamoTablePrefix:         "prod",
		AuditRecordsS3Bucket:      "payday-dynamo-audit-records",
		AuditLambdaMemorySizeMB:   512,
		AuditLambdaTimeoutSec:     5,
		AuditLambdaRoleName:       "payday_lambda",
		AuditLambdaCodeS3Bucket:   "payday-audit-lambda-source",
		AuditLambdaEventBatchSize: 100,
		DataScienceEnabled:        true,
		DataScienceSpadeEnabled:   true,
		DataScienceSpadeURL:       "https://spade.twitch.tv/track/",
		UserServiceEndpoint:       "http://users-service-prod.us-west2.twitch.tv",
	}
}

type fetcher struct {
	UserServiceClient userService.IUserServiceClientWrapper
}

func (f *fetcher) Fetch(ctx context.Context, userID string) (*userServiceModel.Properties, error) {
	ctx, cancel := context.WithTimeout(ctx, 10*time.Second)
	defer cancel()

	return f.UserServiceClient.GetUserByID(ctx, userID, nil)
}

func getUserFetcher(cfg config.Configuration) userservice.Fetcher {
	statsClient, err := statsd.NewNoopClient()
	if err != nil {
		logrus.WithError(err).Panic("Could not create noop statter")
	}

	clientHttpTransport := twitchclient.TransportConf{
		MaxIdleConnsPerHost: 200,
	}

	userServiceClient := user.NewClient(cfg, clientHttpTransport, statsClient)
	return &fetcher{
		UserServiceClient: userServiceClient,
	}
}

func getSpadeModel(tx *dynamo.Transaction) *models.BitsAcquiredV2 {
	if tx == nil {
		logrus.Error("transaction is nil")
		return nil
	}

	log := logrus.WithField("transactionId", tx.TransactionId)
	if tx.UserId == nil {
		log.Error("UserId is nil")
		return nil
	}
	if tx.NumberOfBits == nil {
		log.Error("NumberOfBits is nil")
		return nil
	}
	if tx.BitsType == nil {
		log.Error("BitsType is nil")
		return nil
	}
	if tx.TotalBitsAfterTransaction == nil {
		log.Error("TotalBitsAfterTransaction is nil")
		return nil
	}
	if tx.Platform == nil {
		log.Error("Platform is nil")
		return nil
	}

	bitsAmountAcquired := int(*tx.NumberOfBits)
	skuQuantity := 1
	if tx.SkuQuantity != nil {
		skuQuantity = int(*tx.SkuQuantity)
	}

	return &models.BitsAcquiredV2{
		TransactionID:         string(tx.TransactionId),
		UserID:                *tx.UserId,
		ServerTime:            tx.TimeOfEvent,
		AcquiredTotal:         bitsAmountAcquired,
		AcquiredOfThisType:    bitsAmountAcquired,
		BitType:               *tx.BitsType,
		AdminID:               getStrPVal(tx.AdminID),
		AdminReason:           getStrPVal(tx.AdminReason),
		CurrentBalance:        int(*tx.TotalBitsAfterTransaction),
		PurchasePrice:         getStrPVal(tx.PurchasePrice),
		PurchasePriceCurrency: getStrPVal(tx.PurchasePriceCurrency),
		PurchaseMarketplace:   *tx.Platform,
		AccountTypeSpendOrder: bitsAmountAcquired,
		TrackingID:            getStrPVal(tx.TrackingID),
		Quantity:              skuQuantity,
	}
}

func getStrPVal(str *string) string {
	if str == nil {
		return ""
	} else {
		return *str
	}
}
