package main

import (
	"bytes"
	"context"
	"encoding/csv"
	"fmt"
	"io/ioutil"
	"path/filepath"

	users_wrapper "code.justin.tv/commerce/payday/clients/user"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/foundation/twitchclient"
	"code.justin.tv/web/users-service/client/usersclient_internal"
	"github.com/cactus/go-statsd-client/statsd"
)

const (
	environment                = "staging"
	defaultNumberOfConnections = 200
)

func main() {
	err := config.Load(environment)
	if err != nil {
		panic(err)
	}

	cfg := config.Get()
	if err := cfg.Validate(); err != nil {
		panic(err)
	}

	statsClient, err := statsd.NewNoopClient()
	if err != nil {
		panic(err)
	}

	httpHttpTransport := twitchclient.TransportConf{
		MaxIdleConnsPerHost: defaultNumberOfConnections,
	}

	userServiceConfig := twitchclient.ClientConf{
		Host:      cfg.UserServiceEndpoint,
		Transport: httpHttpTransport,
		Stats:     statsClient,
	}

	userService, err := usersclient_internal.NewClient(userServiceConfig)
	if err != nil {
		panic(err)
	}

	userServiceClientWrapper := users_wrapper.NewUserServiceClientWrapper(userService)

	inFile, err := filepath.Abs("cmd/users/tuids.csv")
	if err != nil {
		panic(err)
	}

	fileBytes, err := ioutil.ReadFile(inFile)
	if err != nil {
		panic(err)
	}

	fileBytesReader := bytes.NewReader(fileBytes)
	csvReader := csv.NewReader(fileBytesReader)

	rows, err := csvReader.ReadAll()
	if err != nil {
		panic(err)
	}

	totalRows := len(rows)
	for rowNum, row := range rows {
		tuid := row[0]

		login, err := userServiceClientWrapper.GetUserByID(context.Background(), tuid, nil)
		if err != nil {
			panic(err)
		}
		fmt.Printf("%d / %d / %s / %s \n", rowNum+1, totalRows, tuid, *login.Login)
	}
}
