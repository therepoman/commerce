package config

import (
	"encoding/json"
	"fmt"
	"os"
	"path"
	"sync"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/clients/sandstorm"
	"code.justin.tv/commerce/payday/file"
	valid "github.com/asaskevich/govalidator"
)

const (
	DevelopmentEnvironment = "development"
	StagingEnvironment     = "staging"
	ProductionEnvironment  = "production"

	EC2MetadataURL = "http://169.254.169.254/latest/meta-data/local-ipv4"

	LambdaRootPathEnvironmentVariable = "LAMBDA_TASK_ROOT"
	EnvironmentEnvironmentVariable    = "payday_APP_ENV"

	Local   = Environment(DevelopmentEnvironment)
	Staging = Environment(StagingEnvironment)
	Prod    = Environment(ProductionEnvironment)
	Default = Local

	LambdaConfigFilePath = "/config/%s"
)

type Environment string

var Environments = map[Environment]interface{}{Local: nil, Staging: nil, Prod: nil}

type Configuration struct {
	// AWS SQS configuration
	SQSRegion                              string `valid:"required"`
	PubSubSQSQueueURL                      string `valid:"url"`
	DataScienceSQSQueueURL                 string `valid:"url"`
	BigBitsSQSQueueURL                     string `valid:"url"`
	BitEventV1SQSQueueURL                  string `valid:"url"`
	BitCacheSQSQueueURL                    string `valid:"url"`
	ImageModerationSQSQueueURL             string `valid:"url"`
	ImageUpdatesSQSQueueURL                string `valid:"url"`
	ChannelLeaderboardIngestionSQSQueueURL string `valid:"url"`
	PartnerOnboardingSQSQueueURL           string `valid:"url"`
	AdminJobSQSQueueURL                    string `valid:"url"`
	PantheonEventSQSQueueURL               string `valid:"url"`
	ExtensionsPubSubSQSQueueURL            string `valid:"url"`
	ChannelSettingsSQSQueueURL             string `valid:"url"`
	RevenueSQSQueueURL                     string `valid:"url"`
	BadgeSQSQueueURL                       string `valid:"url"`
	AutoModSQSQueueURL                     string `valid:"url"`
	PushySQSQueueURL                       string `valid:"url"`
	PhantoSQSQueueURL                      string `valid:"url"`
	UserStateQueueURL                      string `valid:"url"`
	EventbusQueueURL                       string `valid:"url"`
	PerseusQueueURL                        string `valid:"url"`
	PachinkoQueueURL                       string `valid:"url"`
	AutoRefillQueueURL                     string `valid:"url"`
	PachterServiceEndpointURL              string `valid:"url"`
	PagleConditionCompletionSQSQueueURL    string `valid:"url"`
	UserDestroySQSQueueURL                 string `valid:"url"`
	PachterQueueURL                        string `valid:"url"`

	// SFN
	SFNRegion                   string
	BitsUsageEntitlementsSFNARN string
	UserDestroySFNARN           string

	EmoticonUploadQueueURL                   string `valid:"url"`
	EmoticonUploadCallbackSNSTopic           string
	EmoticonBackfillQueueURL                 string `valid:"url"`
	EmoticonBackfillSNSTopic                 string
	EmoticonBackfillVisibilityTimeoutSeconds int64

	KMSRegion       string `valid:"required"`
	KMSMasterKeyARN string `valid:"required"`

	// AWS Dynamo configuration
	DynamoRegion      string `valid:"required"`
	DynamoTablePrefix string `valid:"required"`

	// SWS portal configuration
	SWSHostAddress string `valid:"required"`
	SWSHostPort    uint16 `valid:"required"`

	// Middleware request validation
	JsonContentTypeEnforcement   bool
	OriginEnforcement            bool
	CartmanEnforcement           bool
	ForwardedProtocolEnforcement bool

	// Eligibility cache configuration
	PartnerCacheExpirationMinutes int64 `valid:"required"`
	PartnerCacheCleanupMinutes    int64 `valid:"required"`
	DefaultCacheExpirationMinutes int64 `valid:"required"`
	DefaultCacheCleanupMinutes    int64 `valid:"required"`

	// Payday Server Properties
	AdminPort      int64 `valid:"required"`
	PaydayRestPort int64 `valid:"required"`

	// DataScience configuration
	DataScienceEnabled      bool
	DataScienceSpadeEnabled bool
	DataScienceSpadeURL     string `valid:"url"`

	// Dynamo audit configuration
	AuditRecordsS3Bucket      string `valid:"required"`
	AuditLambdaMemorySizeMB   int64  `valid:"required"`
	AuditLambdaTimeoutSec     int64  `valid:"required"`
	AuditLambdaRoleName       string `valid:"required"`
	AuditLambdaCodeS3Bucket   string `valid:"required"`
	AuditLambdaEventBatchSize int64  `valid:"required"`

	// Pubsub
	PubsubHostAddress     string `valid:"url,required"`
	PubsubControlEndpoint string `valid:"url,required"`

	// TMI
	TMIHostAddress     string `valid:"url,required"`
	TMIFailureSNSTopic string

	// User Service
	UserServiceEndpoint string `valid:"url,required"`

	// Leaderboards Service
	PantheonServiceEndpoint string `valid:"url,required"`

	// Cloudwatch
	CloudwatchBufferSize               int     `valid:"required"`
	CloudwatchBatchSize                int     `valid:"required"`
	CloudwatchFlushIntervalMinutes     int     `valid:"required"`
	CloudwatchFlushCheckDelayMs        int     `valid:"required"`
	CloudwatchEmergencyFlushPercentage float64 `valid:"required"`

	// Integration Tests
	IntegrationTestsEndpoint string `valid:"url"`

	// Profiling
	ProfilingEnabled bool

	// Include Duplicate Post Endpoints (if cartman isn't around to rewrite HTTP verbs)
	IncludeDuplicatePostEndpoints bool

	// Chat Badges Service
	ChatBadgesHostAddress string `valid:"url,required"`

	// Supported Country Override
	SupportedCountryOverride []string

	ZumaHost string      `valid:"required"`
	Redis    RedisConfig `valid:"required"`

	// TODO: Remove this
	// Clips Client
	ClipsOauthTokenFilePath string

	PrefixesEndpoint string

	// Image Processing
	ImageModerationSNSTopic string

	// Bits Assets (Custom Cheermotes)
	CustomCheermoteEnabled                bool
	CustomCheermoteChannelSlackWebhookURL string
	BitsAssetsS3BucketName                string
	BitsAssetsCloudfrontPrefix            string `valid:"url,required"`

	// Bits Onboard Events
	BitsOnboardEventsBucketName string

	// Revenue Services
	MoneyPennyEndpoint string
	MulanEndpoint      string

	// Ripley
	RipleyServiceEndpoint string `valid:"url,required"`

	// Pagle
	PagleServiceEndpoint string `valid:"url,required"`
	PagleServiceDomain   string

	// Custom badges
	CustomBadgesChannelSlackWebhookURL string

	// TMI Failures
	TMIFailuresSlackWebhookURL string

	BigBitsSlackWebhookURL string

	// Summer Games Done Quick (SGDQ) 2017
	SGDQChannel string

	// Bits Purchase Information string
	BitsPurchaseURL string

	Environment string

	// Identity Connections Host
	ConnectionsHost string

	MakoSNSTopic string

	RespawnRoleARN            string
	CorMapping                map[string]string
	CheckoutPageMapping       map[string]string
	TaxExclusiveCountriesList map[string]bool

	MarkedChannels map[string]bool

	MobileConfig MobileConfig

	CheckoutBaseURL           string
	PaypalEnabledCountries    map[string]bool
	PaypalRestrictedCountries []string
	FloServiceEndpoint        string

	BitsUsageSNSTopic string

	S2S                                               S2SConfig  `valid:"required"`
	S2S2                                              S2S2Config `valid:"required"`
	SandstormRole                                     string
	SandstormManager                                  sandstorm.SecretGetter
	SandtormSWSSecret                                 string `valid:"sandstorm"`
	SandtormAmazonRootCert                            string `valid:"sandstorm"`
	SandstormCartmanRSAPublicKey                      string `valid:"sandstorm"`
	SandstormCartmanECCPublicKey                      string `valid:"sandstorm"`
	SandstormCartmanHMACSecretKey                     string `valid:"sandstorm"`
	SandstormCustomCheermoteChannelSlackWebhookURLKey string `valid:"sandstorm"`
	SandstormCustomBadgeChannelSlackWebhookURLKey     string `valid:"sandstorm"`
	SandstormTMIFailuresSlackWebhookURLKey            string `valid:"sandstorm"`
	SandstormBigBitsSlackWebhookURLKey                string `valid:"sandstorm"`
	SandstormClipsOauthToken                          string `valid:"sandstorm"`
	SandstormTwitchClientId                           string `valid:"sandstorm"`
	SandstormRollbarToken                             string `valid:"sandstorm"`

	// Admin Jobs
	AdminJobsSNSTopic string

	// Channel Settings Topic
	ChannelSettingsSNSTopic string

	// Subs service client
	SubsServiceEndpoint string

	EventsBucket string
	EventsKey    string

	// Extensions
	FaladorURL                string `valid:"url"`
	ExtensionsValidatorUrl    string `valid:"url"`
	ExtensionBroadcasterShare float64
	ExtensionDeveloperShare   float64

	// Extension Revshare Whitelist
	ExtensionRevshareWhitelist map[string]bool

	// Pushy Service
	PushyServiceSNSTopic string

	// Revenue Service
	RevenueServiceSNSTopic string

	OWLEndpoint string `valid:"url"`

	UploadServiceURL          string `valid:"url"`
	UploadCallBackSNSTopic    string
	UploadCallBackSQSQueueURL string `valid:"url"`

	Translations BitsTranslations

	// Actions
	ActionsBucket string
	ActionsKey    string

	// Actions V2 (Global Cheers)
	GlobalCheersBucket string
	GlobalCheersKey    string

	// Sponsored Cheermote Campaigns
	SponsoredCheermoteCampaignsBucket                         string
	SponsoredCheermoteCampaignsKey                            string
	SponsoredCheermoteCampaignPubsubDefaultRateLimitPerSecond int64

	// Football Campaign
	BitsFootballEventEnabled   bool
	BitsFootballEventWhitelist []string

	AutoModSNSTopic string

	EMSEndpoint string `valid:"url"`

	UseBitsOnExtensionEventSNSTopic string

	// Enable middleware for Twirp APIs for Cartman business
	TwirpMiddlewareEnabled bool

	// Dynamo tables
	BadgeTierEmoteGroupIDTableName             string
	BadgeTierNotificationTableName             string
	VoldemotePackTableName                     string
	SinglePurchaseConsumptionsTableName        string
	PurchaseByMostRecentSuperPlatformTableName string
	TransactionTableName                       string
	InFlightTransactionTableName               string
	HoldsTableName                             string
	BalanceChangeProcessingTableName           string
	ChannelImagesTableName                     string

	PantheonPingDelaySeconds int

	BulkEntitleCampaignsBucket string
	BulkEntitleCampaignsKey    string

	AprilFoolsStartTime string
	AprilFoolsEndTime   string

	PetoziServiceEndpointURL string `valid:"url"`

	PercentRolloutBucket string
	PercentRolloutKey    string

	PachinkoURL string

	NitroServiceEndpoint string `valid:"url"`

	MakoURL               string
	MakoEmoteUploadBucket string

	PollsWhitelistBucket string
	PollsWhitelistKey    string

	PerseusSNSTopic string
	PachterSNSTopic string `valid:"required"`

	AutoRefillDynamoRoleARN         string
	AutoRefillDynamoTableName       string
	AutoRefillRecordDynamoTableName string

	EverdeenServiceEndpoint string `valid:"url"`
	EverdeenResponseQueue   string
	DartReceiverEndpoint    string `valid:"url"`

	BitsAssetsCloudFrontDistributionId string

	AutoprofBucket string

	// PDMS (more info https://wiki.twitch.com/display/SEC/PDMS-+Delete+pipeline+Service+Onboarding)
	PDMSLambdaCallerRoleARN string
	PDMSLambdaARN           string
	PDMSEnabled             bool

	// Audit Logs for SOX Compliance
	CloudWatchRegion  string
	AuditLogGroupName string
}

func IsValidEnvironment(env Environment) bool {
	_, ok := Environments[env]
	return ok
}

// make sure that nothing is nil
// returns nil if everything is a-ok; otherwise returns an error containing a list of all fields that are unpopulated / invalid
func (c Configuration) Validate() error {
	valid.TagMap["filepath"] = valid.Validator(func(str string) bool {
		if _, err := os.Stat(str); os.IsNotExist(err) {
			return false
		}
		return true
	})

	valid.TagMap["sandstorm"] = func(str string) bool {
		secret, err := c.SandstormManager.Get(str)
		if err != nil {
			log.WithError(err).WithField("secret", str).Error("Error getting secret")
		}
		return err == nil && secret != nil
	}

	// TODO: write a verifier for AWS region (I think they're all in the form of: LOCALE-COMPASS_DIRECTION-INTEGER)

	return c.validateStruct()
}

func (c Configuration) validateStruct() error {
	result, err := valid.ValidateStruct(&c)
	if err != nil {
		return err
	}

	if !result {
		return fmt.Errorf("error occurred while during validation of config file. Additional errors: %v", err)
	}

	return nil
}

type S2SConfig struct {
	Name                   string `valid:"required"`
	Enabled                bool
	MakoEnabled            bool // Please don't use this directly, use IsMakoEnabled instead
	CreateBitsEventEnabled bool // Please don't use this directly, use IsCreateBitsEventEnabled instead
	TwirpEnabled           bool // Please don't use this directly, use IsTwirpEnabled instead
}

func (c *S2SConfig) IsCreateBitsEventEnabled() bool {
	return c.Enabled && c.CreateBitsEventEnabled
}

func (c *S2SConfig) IsTwirpEnabled() bool {
	return c.Enabled && c.TwirpEnabled
}

func (c *S2SConfig) IsMakoEnabled() bool {
	return c.Enabled && c.MakoEnabled
}

type S2S2Config struct {
	Name                      string `valid:"required"`
	ServiceOrigin             string
	AuthLogGroupName          string
	Enabled                   bool
	DisablePassThroughMode    bool
	UseBitsOnExtensionEnabled bool // Please don't use this directly, use IsUseBitsOnExtensionEnabled instead
	UseBitsOnPollEnabled      bool // Please don't use this directly, use IsUseBitsOnPollEnabled instead
}

func (c *S2S2Config) IsUseBitsOnExtensionEnabled() bool {
	return c.Enabled && c.UseBitsOnExtensionEnabled
}

func (c *S2S2Config) IsUseBitsOnPollEnabled() bool {
	return c.Enabled && c.UseBitsOnPollEnabled
}

type MobileConfig struct {
	IOS     MobilePlatformConfig
	Android MobilePlatformConfig
}

type MobilePlatformConfig struct {
	IAPEnabledCountries []string
}

type BitsTranslations struct {
	BitsPrimePromo     map[string]string
	BitsFirstTimePromo map[string]string
	BlackFriday2018    map[string]string
}

type RedisConfig struct {
	Cluster string
	Lock    string
}

const configFileName = "%s.json"

var once = new(sync.Once)
var cfg *Configuration

var configFilePaths = []string{"config/", "../config/", "../../config/"}

func getConfigFileName(environment string) string {
	return fmt.Sprintf(configFileName, environment)
}

func getConfigFilePath(environment string) (string, error) {
	fileName := getConfigFileName(environment)

	if lambdaRootPath := os.Getenv(LambdaRootPathEnvironmentVariable); lambdaRootPath != "" {
		lambdaFName := path.Join(lambdaRootPath, fmt.Sprintf(LambdaConfigFilePath, fileName))
		_, err := os.Stat(lambdaFName)
		return lambdaFName, err
	}

	return file.SearchFileInPaths(fileName, configFilePaths)
}

func GetEnv() Environment {
	env := os.Getenv(EnvironmentEnvironmentVariable)
	if env != "" {
		log.Infof("Found ENVIRONMENT environment variable: %s", env)
	}

	args := os.Args
	if len(args) > 2 {
		log.Panic("Received too many CLI args")
	} else if len(args) == 2 {
		env = args[1]
		log.Infof("Using environment from CLI arg: %s", env)
	}

	if !IsValidEnvironment(Environment(env)) {
		log.Errorf("Invalid environment: %s", env)
		log.Infof("Falling back to default environment: %s", string(Default))
		return Default
	}

	return Environment(env)
}

func IsSet() bool {
	return cfg != nil
}

func Get() Configuration {
	if !IsSet() {
		return Configuration{}
	}

	return *cfg
}

func LoadFromEnvVar() (Configuration, error) {
	env := GetEnv()

	err := Load(string(env))
	if err != nil {
		return Configuration{}, err
	}

	cfg := Get()
	return cfg, nil
}

func Load(environment string) error {
	filePath, err := getConfigFilePath(environment)
	if err != nil {
		return err
	}

	configuration, err := loadConfig(filePath)
	if err != nil {
		return err
	}

	configuration.Environment = environment
	configuration.SandstormManager = sandstorm.New(configuration.SandstormRole)

	once.Do(func() {
		cfg = configuration
	})

	return nil
}

func Set(configuration Configuration) {
	once.Do(func() {
		cfg = &configuration
	})
}

func loadConfig(filepath string) (*Configuration, error) {
	file, err := os.Open(filepath)

	if err != nil {
		return nil, fmt.Errorf("Failed while opening config file: %v", err)
	}
	decoder := json.NewDecoder(file)
	configuration := &Configuration{}

	err = decoder.Decode(configuration)
	if err != nil {
		return nil, fmt.Errorf("Failed while parsing configuration file: %v", err)
	}

	return configuration, nil
}
