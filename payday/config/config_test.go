package config

import (
	"testing"

	sandstorm_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/sandstorm"
	"code.justin.tv/systems/sandstorm/manager"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestAllConfigs(t *testing.T) {
	Convey("Given a config", t, func() {
		sandstormManager := new(sandstorm_mock.SecretGetter)
		sandstormManager.On("Get", mock.Anything).Return(&manager.Secret{}, nil)

		Convey("When loading the developer config JSON", func() {
			filePath, err := getConfigFilePath(DevelopmentEnvironment)
			So(err, ShouldBeNil)

			c, err := loadConfig(filePath)
			So(err, ShouldBeNil)
			So(c, ShouldNotBeNil)

			// we do not run validate on the dev config because we do not do this in main.go
		})
		Convey("When loading the staging config JSON", func() {
			filePath, err := getConfigFilePath(StagingEnvironment)
			So(err, ShouldBeNil)

			c, err := loadConfig(filePath)
			So(err, ShouldBeNil)
			So(c, ShouldNotBeNil)
			c.SandstormManager = sandstormManager

			err = c.Validate()
			So(err, ShouldBeNil)
		})
		Convey("When loading the production config JSON", func() {
			filePath, err := getConfigFilePath(ProductionEnvironment)
			So(err, ShouldBeNil)

			c, err := loadConfig(filePath)
			So(err, ShouldBeNil)
			So(c, ShouldNotBeNil)
			c.SandstormManager = sandstormManager

			err = c.Validate()
			So(err, ShouldBeNil)
		})
	})
}
