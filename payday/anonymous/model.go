package anonymous

const (
	AnonymousCheermotePrefix = "anon"
	PrometheusState          = "ANONYMOUS"
)
