package throttle

import (
	"context"
	"time"
)

type IThrottler interface {
	Throttle(ctx context.Context, key string, expiration time.Duration) bool
	TestToken(ctx context.Context, key string, permits int64, expiration time.Duration) bool
}
