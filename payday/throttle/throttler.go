package throttle

import (
	"context"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/clients/redis"
	"code.justin.tv/commerce/payday/hystrix"
	cmds "code.justin.tv/commerce/payday/hystrix"
	go_redis "github.com/go-redis/redis"
)

type Throttler struct {
	RedisClient redis.Client `inject:"redisClient"`
}

func NewThrottler() *Throttler {
	return &Throttler{}
}

func (t *Throttler) Throttle(ctx context.Context, key string, expiration time.Duration) bool {
	var val string
	err := hystrix.Do(cmds.ThrottleGet, func() error {
		val = t.RedisClient.Get(ctx, key).Val()
		return nil
	}, nil)
	if err != nil {
		log.WithError(err).Warn("hystrix error fetching throttle key")
	}

	if val != "" {
		return true
	}

	err = hystrix.Do(cmds.ThrottleSet, func() error {
		err := t.RedisClient.Set(ctx, key, "true", expiration).Err()
		if err != nil {
			log.WithError(err).WithField("key", key).Error("Error setting redis value")
		}
		return nil
	}, nil)
	if err != nil {
		log.WithError(err).Warn("hystrix error setting throttle key")
	}

	return false
}

func (t *Throttler) TestToken(ctx context.Context, key string, permits int64, expiration time.Duration) bool {
	val, err := t.RedisClient.Get(ctx, key).Int64()

	if err == go_redis.Nil {
		return true
	} else if err != nil {
		log.WithError(err).Error("Got error fetching value from redis")
		return false
	}

	return val <= permits
}
