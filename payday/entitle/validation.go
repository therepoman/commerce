package entitle

import (
	"net/http"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/dynamo"
	httperror "code.justin.tv/commerce/payday/errors/http"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/models/api"
)

type Validator interface {
	IsValid(req api.EntitleBitsRequest, platform models.Platform) error
}

func NewValidator() Validator {
	return &validator{}
}

type validator struct {
	TransactionDAO dynamo.ITransactionsDao `inject:""`
}

func (v *validator) IsValid(req api.EntitleBitsRequest, platform models.Platform) error {
	if req.Platform == "" {
		log.Debug("No platform id sent in.")
		return httperror.New("Platform is required", http.StatusBadRequest)
	}

	if req.TransactionId == "" {
		log.Debug("No transaction id sent in.")
		return httperror.New("Transaction id is required.", http.StatusBadRequest)
	}

	if req.ProductId != "" && req.BitAmount > 0 {
		return httperror.New("Bits amount and product id are not allowed in same request.", http.StatusBadRequest)
	}

	if !models.IsQuantityEntitlingPlatform(platform) && req.BitAmount > 0 {
		log.Debug("Bit amount sent in for wrong platform.")
		return httperror.New("Bits amount is not expected for this platform", http.StatusBadRequest)
	}

	if !models.IsQuantityEntitlingPlatform(platform) && req.ProductId == "" {
		log.Debug("No product id sent in.")
		return httperror.New("Product id is required.", http.StatusBadRequest)
	}

	if models.IsPlatformEntitlingService(platform) && models.ValidateBitsType(req.BitsType) != nil {
		log.Debug("Invalid bits type")
		return httperror.New("Invalid bits type", http.StatusBadRequest)
	}

	if !models.IsQuantityEntitlingPlatform(platform) && req.ProductQuantity < 0 {
		return httperror.New("Product quantity should never be negative", http.StatusBadRequest)
	}

	return nil
}
