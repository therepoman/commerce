package entitle

import (
	"context"
	"testing"

	"code.justin.tv/commerce/gogogadget/random"
	pachinko_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/pachinko"
	metrics_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/metrics"
	products_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/products"
	"code.justin.tv/commerce/payday/models/api"
	petozi "code.justin.tv/commerce/petozi/rpc"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
)

func TestPachinkoEntitler_Entitle(t *testing.T) {
	Convey("given a pachinko entitler", t, func() {
		pachinkoClient := new(pachinko_mock.PachinkoClient)
		productsFetcher := new(products_mock.Fetcher)
		statter := new(metrics_mock.Statter)

		p := &pachinkoEntitler{
			Pachinko:        pachinkoClient,
			ProductsFetcher: productsFetcher,
			Statter:         statter,
		}

		ctx := context.Background()

		req := api.EntitleBitsRequest{
			TransactionId: random.String(32),
			TwitchUserId:  random.String(16),
		}

		Convey("when we have a successful request", func() {
			updatedBalance := 644

			Convey("quantity entitling platform", func() {
				req.BitAmount = int32(322)
				req.Platform = "Upsight"

				pachinkoClient.On("AddBits", ctx, req.TransactionId, req.TwitchUserId, int(req.BitAmount)).Return(updatedBalance, nil)
			})

			Convey("product entitling platform", func() {
				req.Platform = "AMAZON"
				req.ProductId = "WALRUS_PRODUCT"
				req.Price = "WALRUS_PRICE"
				req.Currency = "WALRUS_BUCKS"

				bitsAmount := uint64(322)

				productsFetcher.On("FetchAllFromPetozi", ctx).Return([]*petozi.BitsProduct{
					{
						Id:       req.ProductId,
						Quantity: bitsAmount,
						Platform: petozi.Platform_AMAZON,
					},
				}, nil)

				Convey("when we have multiple quantity", func() {
					req.ProductQuantity = 3
				})

				Convey("when the Products request returns the product but we have an incorrect max quantity requested", func() {
					req.ProductQuantity = 11
					statter.On("Inc", greaterThanMaxProducts, int64(1)).Return()
				})

				Convey("when we have a quantity less than or equal to 1", func() {
					req.ProductQuantity = 1
				})

				Convey("when we have a quantity set to zero", func() {
					req.ProductQuantity = 0
				})

				var finalBitsAmount int
				if req.ProductQuantity == 0 {
					finalBitsAmount = int(bitsAmount)
				} else {
					finalBitsAmount = int(req.ProductQuantity) * int(bitsAmount)
				}
				pachinkoClient.On("AddBits", ctx, req.TransactionId, req.TwitchUserId, finalBitsAmount).Return(updatedBalance, nil)
			})

			resp, err := p.Entitle(ctx, req)
			So(err, ShouldBeNil)
			So(resp, ShouldEqual, updatedBalance)
		})

		Convey("when we have a failing request", func() {
			Convey("when we have a valid request", func() {
				req.Platform = "AMAZON"
				req.ProductId = "WALRUS_PRODUCT"
				req.ProductQuantity = 1

				Convey("when the Products request fails", func() {
					productsFetcher.On("FetchAllFromPetozi", ctx).Return(nil, errors.New("WALRUS STRIKE"))
				})

				Convey("when the Products request returns no products", func() {
					productsFetcher.On("FetchAllFromPetozi", ctx).Return(nil, nil)
				})

				Convey("when the Products request returns a product list that doesn't contain the requested product", func() {
					productsFetcher.On("FetchAllFromPetozi", ctx).Return([]*petozi.BitsProduct{
						{
							Id:       "OTHER_PRODUCT",
							Quantity: 322,
						},
					}, nil)
				})

				Convey("when the Products request returns a product list that does contain the requested product", func() {
					bitsAmount := uint64(322)

					productsFetcher.On("FetchAllFromPetozi", ctx).Return([]*petozi.BitsProduct{
						{
							Id:       req.ProductId,
							Quantity: bitsAmount,
							Platform: petozi.Platform_AMAZON,
						},
					}, nil)

					Convey("when the Pachinko request fails", func() {
						pachinkoClient.On("AddBits", ctx, req.TransactionId, req.TwitchUserId, int(bitsAmount)).Return(0, errors.New("WALRUS STRIKE"))
					})
				})

				_, err := p.Entitle(ctx, req)
				So(err, ShouldNotBeNil)
			})
		})
	})
}
