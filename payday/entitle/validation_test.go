package entitle

import (
	"testing"

	"code.justin.tv/commerce/gogogadget/random"
	dynamo_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/models/api"
	. "github.com/smartystreets/goconvey/convey"
)

func TestValidator_IsValid(t *testing.T) {
	Convey("given an entitle bits validator", t, func() {
		transactionDAO := new(dynamo_mock.ITransactionsDao)
		validator := validator{
			TransactionDAO: transactionDAO,
		}

		req := api.EntitleBitsRequest{
			TwitchUserId:  random.String(16),
			TransactionId: random.String(32),
			Platform:      "FORTUNA",
		}

		platform := models.FORTUNA

		Convey("when we have a valid request", func() {
			Convey("Mobile", func() {
				platform = models.IOS
				req.Platform = "IOS"
				req.ProductId = "walrusBits"
			})

			Convey("Web", func() {
				platform = models.AMAZON
				req.Platform = "AMAZON"
				req.ProductId = "walrusBits"
			})

			Convey("truex", func() {
				platform = models.TRUEX
				req.Platform = "TRUEX"
				req.BitAmount = int32(322)
			})

			Convey("upsight", func() {
				platform = models.UPSIGHT
				req.Platform = "UPSIGHT"
				req.BitAmount = int32(322)
			})

			Convey("fortuna", func() {
				platform = models.FORTUNA
				req.Platform = "FORTUNA"
				req.BitAmount = int32(322)
				req.BitsType = "CB_0"
			})

			err := validator.IsValid(req, platform)
			So(err, ShouldBeNil)
		})

		Convey("when we have an invalid request", func() {
			Convey("when the platform is blank", func() {
				req.Platform = ""
			})

			Convey("when the transaction ID is blank", func() {
				req.TransactionId = ""
			})

			Convey("when the product and bits amount are set", func() {
				req.ProductId = "walrusProduct"
				req.BitAmount = int32(322)
			})

			Convey("when the platform is not an entitling platform", func() {
				platform = models.IOS

				Convey("when the bits amount is set", func() {
					req.BitAmount = int32(322)
				})

				Convey("when the product ID is blank", func() {
					req.ProductId = ""
				})

				Convey("when the quantity is negative", func() {
					req.ProductQuantity = -1
				})
			})

			Convey("when the platform is an entitling platform without a valid bits type", func() {
				platform = models.FORTUNA
				req.BitsType = "walrusBits"
			})

			err := validator.IsValid(req, platform)
			So(err, ShouldNotBeNil)
		})
	})
}
