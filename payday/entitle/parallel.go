package entitle

import (
	"context"
	"errors"
	"time"

	"code.justin.tv/commerce/gogogadget/pointers"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/cache"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/dynamo/bits_entitlement"
	recent_platform "code.justin.tv/commerce/payday/dynamo/purchase_by_most_recent_super_platform"
	"code.justin.tv/commerce/payday/dynamo/single_purchase_consumption"
	"code.justin.tv/commerce/payday/metrics"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/models/api"
	petozi "code.justin.tv/commerce/petozi/rpc"
)

type FabsResponse struct {
	UpdatedBalance int
	Error          error
}

type PachinkoResponse struct {
	UpdatedBalance int
	Error          error
}

type TransactionResponse struct {
	Error error
}

type MostRecentPlatformResponse struct {
	Error error
}

type SinglePurchaseResponse struct {
	Error error
}

func NewParallel() Parallel {
	return &parallel{}
}

type Parallel interface {
	Entitle(ctx context.Context, req api.EntitleBitsRequest) (int32, error)
}

type parallel struct {
	Pachinko               Pachinko                                                 `inject:"pachinkoEntitler"`
	Statter                metrics.Statter                                          `inject:""`
	InFlightTransactionDao dynamo.InFlightTransactionDao                            `inject:""`
	TransactionDAO         bits_entitlement.BitsEntitlementDAO                      `inject:""`
	SinglePurchaseDAO      single_purchase_consumption.SinglePurchaseConsumptionDAO `inject:""`
	MostRecentPlatformDAO  recent_platform.PurchaseByMostRecentSuperPlatformDAO     `inject:""`
	PetoziClient           petozi.Petozi                                            `inject:""`
	CachePurger            cache.Purger                                             `inject:""`
}

func (m *parallel) Entitle(ctx context.Context, req api.EntitleBitsRequest) (int32, error) {
	timeOfEvent := time.Now()

	err := m.createInFlightTransactionRecord(ctx, req, timeOfEvent)
	if err != nil {
		return 0, err
	}

	updatedBalance, err := m.Pachinko.Entitle(ctx, req)
	if err != nil {
		return 0, err
	}

	m.CachePurger.BalanceUpdatePurge(ctx, req.TwitchUserId)

	go func() {
		_ = m.updateDynamoTables(context.Background(), req, timeOfEvent, int32(updatedBalance))
	}()

	return int32(updatedBalance), nil
}

func (m *parallel) createInFlightTransactionRecord(ctx context.Context, req api.EntitleBitsRequest, timeOfEvent time.Time) error {
	bitsType, bitsAmount, err := m.calculateTypeAndAmount(ctx, req, "Parallel_CreateInFlightTransactionRecord")

	if err != nil {
		return err
	}

	transaction := dynamo.Transaction{
		TransactionId:         req.TransactionId,
		UserId:                pointers.StringP(req.TwitchUserId),
		TimeOfEvent:           timeOfEvent,
		LastUpdated:           time.Now(),
		ProductId:             pointers.StringP(req.ProductId),
		Platform:              pointers.StringP(req.Platform),
		BitsType:              pointers.StringP(bitsType),
		NumberOfBits:          pointers.Int64P(int64(bitsAmount)),
		TransactionType:       pointers.StringP(bits_entitlement.AddBitsTransactionType),
		PurchasePrice:         pointers.StringP(req.Price),
		PurchasePriceCurrency: pointers.StringP(req.Currency),
		TrackingID:            pointers.StringP(req.TrackingId),
		SkuQuantity:           pointers.Int64P(int64(req.ProductQuantity)),
		OriginOrderId:         pointers.StringP(req.OriginOrderId),
	}

	return m.InFlightTransactionDao.CreateTransaction(ctx, transaction)
}

func (m *parallel) updateDynamoTables(ctx context.Context, req api.EntitleBitsRequest, timeOfEvent time.Time, updatedBalance int32) error {
	logger := log.WithFields(log.Fields{
		"transactionID": req.TransactionId,
		"twitchUserID":  req.TwitchUserId,
		"productID":     req.ProductId,
		"quanity":       req.ProductQuantity,
		"platform":      req.Platform,
	})

	transactionChan := make(chan TransactionResponse)
	go func() {
		bitsType, bitsAmount, err := m.calculateTypeAndAmount(ctx, req, "Parallel_UpdateDynamoTables")

		if err != nil {
			transactionChan <- TransactionResponse{
				Error: err,
			}
			return
		}

		_, err = m.TransactionDAO.Create(ctx, req.TransactionId, req.TwitchUserId,
			req.ProductId, req.Platform, bitsType, bitsAmount, timeOfEvent, &bits_entitlement.BitsEntitlementOptions{
				TotalBitsAfterTransaction: pointers.Int64P(int64(updatedBalance)),
				PurchasePrice:             pointers.StringP(req.Price),
				PurchasePriceCurrency:     pointers.StringP(req.Currency),
				TrackingID:                pointers.StringP(req.TrackingId),
				SkuQuantity:               pointers.Int64P(int64(req.ProductQuantity)),
				OriginOrderId:             pointers.StringP(req.OriginOrderId),
			})

		transactionChan <- TransactionResponse{
			Error: err,
		}

		if err == nil {
			m.InFlightTransactionDao.RemoveTransaction(ctx, req.TransactionId)
		}
	}()

	mostRecentChan := make(chan MostRecentPlatformResponse)
	go func() {
		// skip ad bits, phanto bits, etc
		superPlatform := models.ParseSuperPlatform(req.Platform)
		if !recordSuperPlatform(superPlatform) || req.ProductId == "" {
			mostRecentChan <- MostRecentPlatformResponse{
				Error: nil,
			}
			return
		}

		_, err := m.MostRecentPlatformDAO.Create(ctx, req.TwitchUserId, string(superPlatform), req.Platform, req.ProductId)
		mostRecentChan <- MostRecentPlatformResponse{
			Error: err,
		}
	}()

	singlePurchaseChan := make(chan SinglePurchaseResponse)
	go func() {
		// skip if id is empty (e.g. Truex has no product id)
		if req.ProductId == "" {
			singlePurchaseChan <- SinglePurchaseResponse{
				Error: nil,
			}
			return
		}

		resp, err := m.PetoziClient.GetBitsProduct(ctx, &petozi.GetBitsProductReq{
			Id:       req.ProductId,
			Platform: models.ParsePetoziPlatform(req.Platform),
		})
		if err != nil {
			singlePurchaseChan <- SinglePurchaseResponse{
				Error: err,
			}
			return
		}

		if isSinglePurchasePromo(resp) {
			_, err = m.SinglePurchaseDAO.Create(ctx, req.TwitchUserId, req.ProductId)
		}
		singlePurchaseChan <- SinglePurchaseResponse{
			Error: err,
		}
	}()

	transactionDone, mostRecentDone, singlePurchaseDone := false, false, false

	for {
		select {
		case transactionResp := <-transactionChan:
			if transactionResp.Error != nil {
				logger.WithError(transactionResp.Error).Error("Error recording transaction")
				return transactionResp.Error
			}
			transactionDone = true
		case mostRecentResp := <-mostRecentChan:
			if mostRecentResp.Error != nil {
				logger.WithError(mostRecentResp.Error).Error("Error recording most recent platform")
				return mostRecentResp.Error
			}
			mostRecentDone = true
		case singlePurchaseResp := <-singlePurchaseChan:
			if singlePurchaseResp.Error != nil {
				logger.WithError(singlePurchaseResp.Error).Error("Error recording single purchase consumption")
				return singlePurchaseResp.Error
			}
			singlePurchaseDone = true
		case <-ctx.Done():
			logger.Error("Dynamo update timed out after entitlements")
		}

		if transactionDone && mostRecentDone && singlePurchaseDone {
			return nil
		}
	}
}

func (m *parallel) calculateTypeAndAmount(ctx context.Context, req api.EntitleBitsRequest, sourceContext string) (string, int32, error) {
	bitsType := req.BitsType
	bitsAmount := req.BitAmount

	if models.ParsePlatform(req.Platform, pointers.StringP(sourceContext), pointers.StringP(req.TransactionId), nil) == models.TRUEX {
		bitsType = string(models.BitsForAds)
	} else if models.ParsePlatform(req.Platform, pointers.StringP(sourceContext), pointers.StringP(req.TransactionId), nil) == models.UPSIGHT {
		bitsType = string(models.BitsForAdsUpsight)
	}
	if req.ProductId != "" {
		resp, err := m.PetoziClient.GetBitsProduct(ctx, &petozi.GetBitsProductReq{
			Id:       req.ProductId,
			Platform: models.ParsePetoziPlatform(req.Platform),
		})

		if err != nil {
			return bitsType, bitsAmount, err
		}

		if resp == nil || resp.BitsProduct == nil {
			return bitsType, bitsAmount, errors.New("petozi returned nil response")
		}

		bitsType = resp.BitsProduct.BitsTypeId
		// So because both entitle/parallel.go and entitle/pachinko.go both calculate the final Bit amount based on multi quantity
		// independently, we need to calculate this in different places.
		if !req.OverrideProductBitsAmount {
			bitsAmount = int32(resp.BitsProduct.Quantity)
		}
		if req.ProductQuantity > 1 {
			bitsAmount = int32(resp.BitsProduct.Quantity) * req.ProductQuantity
		}
	}

	return bitsType, bitsAmount, nil
}

func isSinglePurchasePromo(resp *petozi.GetBitsProductResp) bool {
	if resp == nil || resp.BitsProduct == nil || resp.BitsProduct.Promo == nil {
		return false
	}

	return resp.BitsProduct.Promo.Type == petozi.PromoType_SINGLE_PURCHASE ||
		resp.BitsProduct.Promo.Type == petozi.PromoType_PRIME_SINGLE_PURCHASE ||
		resp.BitsProduct.Promo.Type == petozi.PromoType_FIRST_TIME_PURCHASE
}

func recordSuperPlatform(superPlatform models.SuperPlatform) bool {
	return superPlatform == models.SuperPlatform_WEB ||
		superPlatform == models.SuperPlatform_ANDROID ||
		superPlatform == models.SuperPlatform_IOS
}
