package entitle

import (
	"context"
	"testing"
	"time"

	"code.justin.tv/commerce/gogogadget/random"
	cache_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/cache"
	dynamo_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo"
	bits_entitlement_dao_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo/bits_entitlement"
	purchase_by_most_recent_super_platform_dao_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo/purchase_by_most_recent_super_platform"
	single_purchase_consumption_dao_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo/single_purchase_consumption"
	entitle_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/entitle"
	metrics_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/metrics"
	petozi_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/petozi/rpc"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/models/api"
	petozi "code.justin.tv/commerce/petozi/rpc"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestParallel_Entitle(t *testing.T) {
	Convey("given an entitle bits API", t, func() {
		pachinkoEntitler := new(entitle_mock.Pachinko)
		transactionDAO := new(bits_entitlement_dao_mock.BitsEntitlementDAO)
		inFlightTransactionDAO := new(dynamo_mock.InFlightTransactionDao)
		recentPlatformDAO := new(purchase_by_most_recent_super_platform_dao_mock.PurchaseByMostRecentSuperPlatformDAO)
		singlePurchaseDAO := new(single_purchase_consumption_dao_mock.SinglePurchaseConsumptionDAO)
		petoziClient := new(petozi_mock.Petozi)
		statter := new(metrics_mock.Statter)
		cp := new(cache_mock.Purger)

		cp.On("BalanceUpdatePurge", mock.Anything, mock.Anything)

		p := &parallel{
			Pachinko:               pachinkoEntitler,
			Statter:                statter,
			TransactionDAO:         transactionDAO,
			InFlightTransactionDao: inFlightTransactionDAO,
			SinglePurchaseDAO:      singlePurchaseDAO,
			MostRecentPlatformDAO:  recentPlatformDAO,
			PetoziClient:           petoziClient,
			CachePurger:            cp,
		}

		ctx := context.Background()
		updatedBitsAmount := int32(322)
		userID := random.String(16)

		req := api.EntitleBitsRequest{
			TransactionId: random.String(32),
			TwitchUserId:  userID,
			BitAmount:     updatedBitsAmount,
			Platform:      string(models.TRUEX),
		}

		inFlightTransactionDAO.On("CreateTransaction", mock.Anything, mock.Anything).Return(nil)
		inFlightTransactionDAO.On("RemoveTransaction", mock.Anything, mock.Anything).Return(nil)
		transactionDAO.On("Create", mock.Anything, req.TransactionId, req.TwitchUserId, req.ProductId, req.Platform, string(models.BitsForAds), req.BitAmount, mock.Anything, mock.Anything).Return(nil, nil).Once()
		recentPlatformDAO.On("Create", mock.Anything, req.TwitchUserId, superPlatform(req.Platform), req.Platform, req.ProductId).Return(nil, nil).Once()
		petoziClient.On("GetBitsProduct", mock.Anything, &petozi.GetBitsProductReq{
			Id:       req.ProductId,
			Platform: models.ParsePetoziPlatform(req.Platform),
		}).Return(nil, nil)

		Convey("when Pachinko returns an error", func() {
			pachinkoEntitler.On("Entitle", mock.Anything, req).Return(0, errors.New("WALRUS STRIKE"))

			_, err := p.Entitle(ctx, req)
			So(err, ShouldNotBeNil)
			transactionDAO.AssertNumberOfCalls(t, "Create", 0)
			recentPlatformDAO.AssertNumberOfCalls(t, "Create", 0)
			singlePurchaseDAO.AssertNumberOfCalls(t, "Create", 0)
		})

		Convey("when Pachinko succeeds", func() {
			pachinkoEntitler.On("Entitle", mock.Anything, req).Return(int(updatedBitsAmount), nil)

			resp, err := p.Entitle(ctx, req)
			So(err, ShouldBeNil)
			So(resp, ShouldEqual, updatedBitsAmount)

			time.Sleep(10 * time.Millisecond)
			transactionDAO.AssertNumberOfCalls(t, "Create", 1)
			recentPlatformDAO.AssertNumberOfCalls(t, "Create", 0)
			singlePurchaseDAO.AssertNumberOfCalls(t, "Create", 0)
		})
	})
}

func Test_updateDynamoTables(t *testing.T) {
	Convey("Given parallel entitler", t, func() {
		transactionDAO := new(bits_entitlement_dao_mock.BitsEntitlementDAO)
		inFlightTransactionDAO := new(dynamo_mock.InFlightTransactionDao)
		recentPlatformDAO := new(purchase_by_most_recent_super_platform_dao_mock.PurchaseByMostRecentSuperPlatformDAO)
		singlePurchaseDAO := new(single_purchase_consumption_dao_mock.SinglePurchaseConsumptionDAO)
		petoziClient := new(petozi_mock.Petozi)

		ctx := context.Background()
		timeOfEvent := time.Now()
		updatedBitsAmount := int32(322)
		req := api.EntitleBitsRequest{
			TransactionId: random.String(32),
			TwitchUserId:  random.String(16),
			BitAmount:     updatedBitsAmount,
			Platform:      string(models.AMAZON),
			ProductId:     "pagliacci-bits",
		}

		p := &parallel{
			TransactionDAO:         transactionDAO,
			InFlightTransactionDao: inFlightTransactionDAO,
			SinglePurchaseDAO:      singlePurchaseDAO,
			MostRecentPlatformDAO:  recentPlatformDAO,
			PetoziClient:           petoziClient,
		}

		Convey("when updating transaction table fails", func() {
			transactionDAO.On("Create", mock.Anything, req.TransactionId, req.TwitchUserId, req.ProductId, req.Platform, "500_BITS_FOR_350_PROMO", int32(500), timeOfEvent, mock.Anything).Return(nil, errors.New("e")).Once()
			recentPlatformDAO.On("Create", mock.Anything, req.TwitchUserId, superPlatform(req.Platform), req.Platform, req.ProductId).Return(nil, nil).Once()
			singlePurchaseDAO.On("Create", mock.Anything, req.TwitchUserId, req.ProductId).Return(nil, nil).Once()
			petoziClient.On("GetBitsProduct", mock.Anything, &petozi.GetBitsProductReq{
				Id:       req.ProductId,
				Platform: models.ParsePetoziPlatform(req.Platform),
			}).Return(&petozi.GetBitsProductResp{
				BitsProduct: &petozi.BitsProduct{
					BitsTypeId: "500_BITS_FOR_350_PROMO",
					Id:         "B075ZD2VZQ",
					Quantity:   500,
					Promo:      nil,
				},
			}, nil)

			err := p.updateDynamoTables(ctx, req, timeOfEvent, updatedBitsAmount)
			So(err, ShouldNotBeNil)
		})

		Convey("when updating most recent platform table fails", func() {
			transactionDAO.On("Create", mock.Anything, req.TransactionId, req.TwitchUserId, req.ProductId, req.Platform, "500_BITS_FOR_350_PROMO", int32(500), timeOfEvent, mock.Anything).Return(nil, nil).Once()
			inFlightTransactionDAO.On("RemoveTransaction", mock.Anything, mock.Anything).Return(nil)
			recentPlatformDAO.On("Create", mock.Anything, req.TwitchUserId, superPlatform(req.Platform), req.Platform, req.ProductId).Return(nil, errors.New("e")).Once()
			singlePurchaseDAO.On("Create", mock.Anything, req.TwitchUserId, req.ProductId).Return(nil, nil).Once()
			petoziClient.On("GetBitsProduct", mock.Anything, &petozi.GetBitsProductReq{
				Id:       req.ProductId,
				Platform: models.ParsePetoziPlatform(req.Platform),
			}).Return(&petozi.GetBitsProductResp{
				BitsProduct: &petozi.BitsProduct{
					BitsTypeId: "500_BITS_FOR_350_PROMO",
					Id:         "B075ZD2VZQ",
					Quantity:   500,
					Promo:      nil,
				},
			}, nil)

			err := p.updateDynamoTables(ctx, req, timeOfEvent, updatedBitsAmount)
			So(err, ShouldNotBeNil)
		})

		Convey("when updating single purchase table fails", func() {
			transactionDAO.On("Create", mock.Anything, req.TransactionId, req.TwitchUserId, req.ProductId, req.Platform, "500_BITS_FOR_350_PROMO", int32(500), timeOfEvent, mock.Anything).Return(nil, nil).Once()
			inFlightTransactionDAO.On("RemoveTransaction", mock.Anything, mock.Anything).Return(nil)
			recentPlatformDAO.On("Create", mock.Anything, req.TwitchUserId, superPlatform(req.Platform), req.Platform, req.ProductId).Return(nil, nil).Once()
			singlePurchaseDAO.On("Create", mock.Anything, req.TwitchUserId, req.ProductId).Return(nil, errors.New("e")).Once()
			petoziClient.On("GetBitsProduct", mock.Anything, &petozi.GetBitsProductReq{
				Id:       req.ProductId,
				Platform: models.ParsePetoziPlatform(req.Platform),
			}).Return(&petozi.GetBitsProductResp{
				BitsProduct: &petozi.BitsProduct{
					BitsTypeId: "500_BITS_FOR_350_PROMO",
					Id:         "B075ZD2VZQ",
					Quantity:   500,
					Promo: &petozi.Promo{
						Type: petozi.PromoType_SINGLE_PURCHASE,
					},
				},
			}, nil)

			err := p.updateDynamoTables(ctx, req, timeOfEvent, updatedBitsAmount)
			So(err, ShouldNotBeNil)
		})

		Convey("when petozi call fails", func() {
			transactionDAO.On("Create", mock.Anything, req.TransactionId, req.TwitchUserId, req.ProductId, req.Platform, req.BitsType, req.BitAmount, timeOfEvent, mock.Anything).Return(nil, nil).Once()
			inFlightTransactionDAO.On("RemoveTransaction", mock.Anything, mock.Anything).Return(nil)
			recentPlatformDAO.On("Create", mock.Anything, req.TwitchUserId, superPlatform(req.Platform), req.Platform, req.ProductId).Return(nil, nil).Once()
			petoziClient.On("GetBitsProduct", mock.Anything, &petozi.GetBitsProductReq{
				Id:       req.ProductId,
				Platform: models.ParsePetoziPlatform(req.Platform),
			}).Return(nil, errors.New("petozi err"))

			err := p.updateDynamoTables(ctx, req, timeOfEvent, updatedBitsAmount)
			So(err, ShouldNotBeNil)
		})

		Convey("when it's ad (TRUEX) bits entitlement", func() {
			req.ProductId = ""
			req.Platform = string(models.TRUEX)
			transactionDAO.On("Create", mock.Anything, req.TransactionId, req.TwitchUserId, req.ProductId, req.Platform, string(models.BitsForAds), req.BitAmount, timeOfEvent, mock.Anything).Return(nil, nil).Once()
			inFlightTransactionDAO.On("RemoveTransaction", mock.Anything, mock.Anything).Return(nil)

			err := p.updateDynamoTables(ctx, req, timeOfEvent, updatedBitsAmount)
			So(err, ShouldBeNil)
			petoziClient.AssertNumberOfCalls(t, "GetBitsProduct", 0)
			recentPlatformDAO.AssertNumberOfCalls(t, "Create", 0)
		})

		Convey("when it's ad (UPSIGHT) bits entitlement", func() {
			req.ProductId = ""
			req.Platform = string(models.UPSIGHT)
			transactionDAO.On("Create", mock.Anything, req.TransactionId, req.TwitchUserId, req.ProductId, req.Platform, string(models.BitsForAdsUpsight), req.BitAmount, timeOfEvent, mock.Anything).Return(nil, nil).Once()
			inFlightTransactionDAO.On("RemoveTransaction", mock.Anything, mock.Anything).Return(nil)

			err := p.updateDynamoTables(ctx, req, timeOfEvent, updatedBitsAmount)
			So(err, ShouldBeNil)
			petoziClient.AssertNumberOfCalls(t, "GetBitsProduct", 0)
			recentPlatformDAO.AssertNumberOfCalls(t, "Create", 0)
		})

		Convey("when dynamo updates all succeed", func() {
			transactionDAO.On("Create", mock.Anything, req.TransactionId, req.TwitchUserId, req.ProductId, req.Platform, "500_BITS_FOR_350_PROMO", int32(500), timeOfEvent, mock.Anything).Return(nil, nil).Once()
			inFlightTransactionDAO.On("RemoveTransaction", mock.Anything, mock.Anything).Return(nil)
			recentPlatformDAO.On("Create", mock.Anything, req.TwitchUserId, superPlatform(req.Platform), req.Platform, req.ProductId).Return(nil, nil).Once()
			singlePurchaseDAO.On("Create", mock.Anything, req.TwitchUserId, req.ProductId).Return(nil, nil).Once()
			petoziClient.On("GetBitsProduct", mock.Anything, &petozi.GetBitsProductReq{
				Id:       req.ProductId,
				Platform: models.ParsePetoziPlatform(req.Platform),
			}).Return(&petozi.GetBitsProductResp{
				BitsProduct: &petozi.BitsProduct{
					BitsTypeId: "500_BITS_FOR_350_PROMO",
					Id:         "B075ZD2VZQ",
					Quantity:   500,
					Promo: &petozi.Promo{
						Type: petozi.PromoType_SINGLE_PURCHASE,
					},
				},
			}, nil)

			err := p.updateDynamoTables(ctx, req, timeOfEvent, updatedBitsAmount)
			So(err, ShouldBeNil)
		})
	})
}

func superPlatform(platform string) string {
	return string(models.ParseSuperPlatform(platform))
}
