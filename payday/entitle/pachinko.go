package entitle

import (
	"context"

	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/payday/clients/pachinko"
	"code.justin.tv/commerce/payday/metrics"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/models/api"
	"code.justin.tv/commerce/payday/products"

	"github.com/pkg/errors"
)

type Pachinko interface {
	Entitle(ctx context.Context, req api.EntitleBitsRequest) (int, error)
}

const (
	greaterThanMaxProducts = "entitle-bits-greater-than-max"
)

func NewPachinko() Pachinko {
	return &pachinkoEntitler{}
}

type pachinkoEntitler struct {
	Pachinko        pachinko.PachinkoClient `inject:""`
	ProductsFetcher products.Fetcher        `inject:""`
	Statter         metrics.Statter         `inject:""`
}

func (p *pachinkoEntitler) Entitle(ctx context.Context, req api.EntitleBitsRequest) (int, error) {
	platform := models.ParsePlatform(req.Platform, pointers.StringP("Pachinko_Entitle"), pointers.StringP(req.TransactionId), nil)
	var bitAmount int
	if models.IsQuantityEntitlingPlatform(platform) || req.OverrideProductBitsAmount {
		bitAmount = int(req.BitAmount)
		// Products with amount override still need to respect the ProductQuantity amounts
		if req.ProductId != "" && req.ProductQuantity > 0 {
			bitAmount = int(req.BitAmount) * int(req.ProductQuantity)
		}
	} else {
		platform := models.ParsePetoziPlatform(req.Platform)
		allProducts, err := p.ProductsFetcher.FetchAllFromPetozi(ctx)
		if err != nil {
			return 0, err
		}

		if len(allProducts) == 0 {
			return 0, errors.New("petozi has no products")
		}

		for _, product := range allProducts {
			if product.Id == req.ProductId && product.Platform == platform {
				if req.ProductQuantity > 0 {
					bitAmount = int(product.Quantity) * int(req.ProductQuantity)
				} else {
					bitAmount = int(product.Quantity)
				}

			}
		}

		if bitAmount == 0 {
			return 0, errors.New("product not found")
		}
	}

	updatedBalance, err := p.Pachinko.AddBits(ctx, req.TransactionId, req.TwitchUserId, bitAmount)
	if err != nil {
		return 0, err
	}

	return updatedBalance, nil
}
