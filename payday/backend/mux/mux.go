package mux

import (
	"fmt"
	"net/http"
	"net/http/pprof"
	"strings"

	"code.justin.tv/amzn/TwitchS2S2/s2s2"
	"code.justin.tv/commerce/gogogadget/audit"
	"code.justin.tv/commerce/payday/backend"
	"code.justin.tv/commerce/payday/backend/mux/authed"
	"code.justin.tv/commerce/payday/backend/mux/healthcheck"
	"code.justin.tv/commerce/payday/backend/mux/secure"
	"code.justin.tv/commerce/payday/config"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	prism_tenant "code.justin.tv/commerce/prism/rpc/tenant"
	offerstenantrpc "code.justin.tv/revenue/offer-tenant-schema/rpc"
	"code.justin.tv/sse/malachai/pkg/s2s/callee"
	"github.com/zenazn/goji/web"
)

const (
	BitsPrismTenantPrefix  = "/bits-prism-tenant"
	VoldsPrismTenantPrefix = "/volds-prism-tenant"
)

// Generated twirp handlers do not allow any modification to the path that it is served on. To get around this,
// and to allow us to have more than one prism tenant in payday, we listen to unique paths on mux, but then
// rewrite the URL before passing the request to their respective twirp handlers.
func makePrismTenantPathRewriter(pathPrefix string) func(inner http.Handler) http.Handler {
	return func(inner http.Handler) http.Handler {
		fn := func(w http.ResponseWriter, r *http.Request) {
			r.URL.Path = strings.Replace(
				r.URL.Path,
				fmt.Sprintf("%s%s", pathPrefix, prism_tenant.PrismTenantPathPrefix),
				prism_tenant.PrismTenantPathPrefix,
				1,
			)
			inner.ServeHTTP(w, r)
		}

		return http.HandlerFunc(fn)
	}
}

func New(cfg config.Configuration, b *backend.Backend, s2sClient *callee.Client, s2s2Client *s2s2.S2S2) (*web.Mux, *web.Mux) {
	twirpHandler := paydayrpc.NewPaydayServer(b.TwirpServer, nil)
	bitsPrismTenantTwirpHandler := prism_tenant.NewPrismTenantServer(b.BitsPrismTenantServer, nil)
	voldsPrismTenantTwirpHandler := prism_tenant.NewPrismTenantServer(b.VoldsPrismTenantServer, nil)
	offersTenantTwirpHandler := offerstenantrpc.NewOffersTenantServer(b.OfferTenantServer, nil)

	// For audit logging
	twirpHandlerWrapper := audit.NewMuxWrapper(twirpHandler)
	bitsPrismTenantTwirpHandlerWrapper := audit.NewMuxWrapper(bitsPrismTenantTwirpHandler)
	offersTenantTwirpHandlerWrapper := audit.NewMuxWrapper(offersTenantTwirpHandler)

	healthCheckMux := healthcheck.Mux(b)
	// For internal and admin calls
	secureMux := secure.Mux(b)
	// Service to Service calls, require authentication
	authedMux := authed.Mux(b)
	parentMux := web.New()

	bitsPrismTenantRewriter := makePrismTenantPathRewriter(BitsPrismTenantPrefix)
	voldsPrismTenantRewriter := makePrismTenantPathRewriter(VoldsPrismTenantPrefix)

	parentMux.Handle("/health-check", healthCheckMux)
	parentMux.Handle("/", healthCheckMux)
	parentMux.Handle("/profiler", healthCheckMux)

	secureMux.Handle("/health-check", healthCheckMux)
	secureMux.Handle("/", healthCheckMux)

	// S2S2 onboarding
	// Need to add specific routes first b/c goji handles routing in the same order they are added,
	// and the first matching route is used.
	if cfg.S2S2.IsUseBitsOnExtensionEnabled() {
		handler := s2s2Client.RequireAuthentication(twirpHandlerWrapper)
		if cfg.S2S2.DisablePassThroughMode {
			secureMux.Handle(fmt.Sprintf("%s%s", paydayrpc.PaydayPathPrefix, "UseBitsOnExtension"), handler)
		} else {
			secureMux.Handle(fmt.Sprintf("%s%s", paydayrpc.PaydayPathPrefix, "UseBitsOnExtension"), handler.RecordMetricsOnly())
		}
	}
	if cfg.S2S2.IsUseBitsOnPollEnabled() {
		handler := s2s2Client.RequireAuthentication(twirpHandlerWrapper)
		if cfg.S2S2.DisablePassThroughMode {
			secureMux.Handle(fmt.Sprintf("%s%s", paydayrpc.PaydayPathPrefix, "UseBitsOnPoll"), handler)
		} else {
			secureMux.Handle(fmt.Sprintf("%s%s", paydayrpc.PaydayPathPrefix, "UseBitsOnPoll"), handler.RecordMetricsOnly())
		}
	}

	if cfg.S2S.IsTwirpEnabled() {
		secureMux.Handle(fmt.Sprintf("%s%s", paydayrpc.PaydayPathPrefix, "*"), s2sClient.CapabilitiesAuthorizerMiddleware(twirpHandlerWrapper))
	} else {
		secureMux.Handle(fmt.Sprintf("%s%s", paydayrpc.PaydayPathPrefix, "*"), twirpHandlerWrapper)
	}

	if cfg.S2S.Enabled {
		secureMux.Handle(fmt.Sprintf("%s%s%s", BitsPrismTenantPrefix, prism_tenant.PrismTenantPathPrefix, "*"), s2sClient.CapabilitiesAuthorizerMiddleware(bitsPrismTenantRewriter(bitsPrismTenantTwirpHandlerWrapper)))
		secureMux.Handle(fmt.Sprintf("%s%s%s", VoldsPrismTenantPrefix, prism_tenant.PrismTenantPathPrefix, "*"), s2sClient.CapabilitiesAuthorizerMiddleware(voldsPrismTenantRewriter(voldsPrismTenantTwirpHandler)))
		secureMux.Handle("/authed/entitleBits", s2sClient.CapabilitiesAuthorizerMiddleware(authedMux))
		secureMux.Handle("/authed/admin/*", s2sClient.CapabilitiesAuthorizerMiddleware(authedMux))
		secureMux.Handle(fmt.Sprintf("%s%s", offerstenantrpc.OffersTenantPathPrefix, "*"), s2sClient.CapabilitiesAuthorizerMiddleware(offersTenantTwirpHandlerWrapper))
	} else {
		secureMux.Handle(fmt.Sprintf("%s%s%s", BitsPrismTenantPrefix, prism_tenant.PrismTenantPathPrefix, "*"), bitsPrismTenantRewriter(bitsPrismTenantTwirpHandlerWrapper))
		secureMux.Handle(fmt.Sprintf("%s%s%s", VoldsPrismTenantPrefix, prism_tenant.PrismTenantPathPrefix, "*"), voldsPrismTenantRewriter(voldsPrismTenantTwirpHandler))
		secureMux.Handle("/authed/entitleBits", authedMux)
		secureMux.Handle("/authed/admin/*", authedMux)
		secureMux.Handle(fmt.Sprintf("%s%s", offerstenantrpc.OffersTenantPathPrefix, "*"), offersTenantTwirpHandlerWrapper)
	}

	if cfg.S2S.IsCreateBitsEventEnabled() {
		secureMux.Handle("/authed/events", s2sClient.CapabilitiesAuthorizerMiddleware(authedMux))
	} else {
		secureMux.Handle("/authed/events", authedMux)
	}

	// Configure pprof profiler
	if cfg.ProfilingEnabled {
		parentMux.Handle("/debug/pprof/", pprof.Index)
		parentMux.Handle("/debug/pprof/profile", pprof.Profile)
		parentMux.Handle("/debug/pprof/trace", pprof.Trace)
		parentMux.Handle("/debug/pprof/symbol", pprof.Symbol)
		parentMux.Handle("/debug/pprof/heap", pprof.Handler("heap").ServeHTTP)
		parentMux.Handle("/debug/pprof/block", pprof.Handler("block").ServeHTTP)
		parentMux.Handle("/debug/pprof/goroutine", pprof.Handler("goroutine").ServeHTTP)
		parentMux.Handle("/debug/pprof/threadcreate", pprof.Handler("threadcreate").ServeHTTP)
	}

	return parentMux, secureMux
}
