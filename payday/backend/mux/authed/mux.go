package authed

import (
	"code.justin.tv/commerce/payday/api/apidef"
	"code.justin.tv/commerce/payday/backend"
	"code.justin.tv/commerce/payday/decorators"
	"github.com/zenazn/goji/web"
)

func Mux(b *backend.Backend) *web.Mux {
	// Service to Service calls, require authentication
	authedMux := web.New()

	authedMux.Post("/authed/entitleBits", decorators.DecorateAndAdapt(b.API.Entitle.EntitleBits, apidef.EntitleBits))
	authedMux.Post("/authed/admin/addBits", decorators.DecorateAndAdapt(b.API.Admin.AddBits, apidef.AdminAddBits))
	authedMux.Post("/authed/admin/removeBits", decorators.DecorateAndAdapt(b.API.Admin.RemoveBits, apidef.AdminRemoveBits))
	authedMux.Get("/authed/admin/balance/:tuid", decorators.DecorateAndAdapt(b.API.Balance.GetBalanceWithoutCartman, apidef.AdminGetBalance))

	authedMux.Get("/authed/admin/channels/:tuid", decorators.DecorateAndAdapt(b.API.Channel.GetChannelInfo, apidef.AdminGetChannelInfo))
	authedMux.Get("/authed/admin/channels/:tuid/min-bits", decorators.DecorateAndAdapt(b.API.Channel.GetMinBits, apidef.AdminGetChannelMinBits))
	authedMux.Put("/authed/admin/channels/:tuid/min-bits", decorators.DecorateAndAdapt(b.API.Channel.SetMinBits, apidef.AdminSetChannelMinBits))

	authedMux.Get("/authed/admin/channels/:tuid/opt-out", decorators.DecorateAndAdapt(b.API.Channel.GetOptOut, apidef.AdminGetChannelOptOut))
	authedMux.Put("/authed/admin/channels/:tuid/opt-out", decorators.DecorateAndAdapt(b.API.Channel.SetOptOut, apidef.AdminSetChannelOptOut))

	authedMux.Get("/authed/admin/users/:tuid/banned", decorators.DecorateAndAdapt(b.API.User.GetBanned, apidef.AdminGetUserBanned))
	authedMux.Put("/authed/admin/users/:tuid/banned", decorators.DecorateAndAdapt(b.API.User.SetBanned, apidef.AdminSetUserBanned))
	authedMux.Get("/authed/admin/users/:tuid", decorators.DecorateAndAdapt(b.API.User.GetUserInfoAdmin, apidef.AdminGetUserInfo))
	authedMux.Get("/authed/admin/users/:tuid/events", decorators.DecorateAndAdapt(b.API.Event.AdminGetUserEvents, apidef.AdminGetBitsEventsForUser))

	authedMux.Get("/authed/admin/badges/:tuid/tiers", decorators.DecorateAndAdapt(b.API.Badge.GetBadgeTiersAdmin, apidef.AdminGetBadgeTiers))
	authedMux.Put("/authed/admin/badges/:tuid/tiers", decorators.DecorateAndAdapt(b.API.Badge.SetBadgeTiersAdmin, apidef.AdminSetBadgeTiers))
	authedMux.Patch("/authed/admin/badges/:tuid/tiers", decorators.DecorateAndAdapt(b.API.Badge.SetBadgeTiersAdmin, apidef.AdminSetBadgeTiers))

	authedMux.Get("/authed/admin/dashboard/:tuid/revenue", decorators.DecorateAndAdapt(b.API.Dashboard.GetBroadcasterRevenue, apidef.AdminGetBroadcasterRevenue))

	authedMux.Get("/authed/admin/withholding/:accountId", decorators.DecorateAndAdapt(b.API.Witholding.GetWithholdingByAccountId, apidef.GetWithholdingByPayoutEntityId))
	authedMux.Get("/authed/admin/withholding/account/:payoutEntityId", decorators.DecorateAndAdapt(b.API.Witholding.GetWithholdingByPayoutEntityId, apidef.GetWithholdingByAccountId))

	authedMux.Get("/authed/admin/channels/:tuid/images", decorators.DecorateAndAdapt(b.API.Image.DownloadChannelImages, apidef.DownloadChannelImages))
	authedMux.Delete("/authed/admin/channels/:tuid/images", decorators.DecorateAndAdapt(b.API.Image.ModerateChannelImages, apidef.AdminModerateChannelImages))

	authedMux.Get("/authed/admin/jobs", decorators.DecorateAndAdapt(b.API.AdminJob.GetAdminJobsByCreator, apidef.GetAdminJobsByCreator))
	authedMux.Get("/authed/admin/job/input", decorators.DecorateAndAdapt(b.API.AdminJob.GetAdminJobInput, apidef.GetAdminJobInput))
	authedMux.Get("/authed/admin/job/options", decorators.DecorateAndAdapt(b.API.AdminJob.GetAdminJobOptions, apidef.GetAdminJobOptions))
	authedMux.Get("/authed/admin/job/output", decorators.DecorateAndAdapt(b.API.AdminJob.GetAdminJobOutput, apidef.GetAdminJobOutput))
	authedMux.Post("/authed/admin/jobs", decorators.DecorateAndAdapt(b.API.AdminJob.StartAdminJob, apidef.StartAdminJob))
	authedMux.Post("/authed/admin/jobs/bulk-entitle", decorators.DecorateAndAdapt(b.API.AdminJob.StartBulkEntitleJob, apidef.StartBulkEntitleJob))

	authedMux.Use(authedMux.Router)

	return authedMux
}
