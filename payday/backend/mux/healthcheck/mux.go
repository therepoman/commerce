package healthcheck

import (
	"net/http"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/api"
	"code.justin.tv/commerce/payday/backend"
	"github.com/zenazn/goji/web"
	goji_middleware "github.com/zenazn/goji/web/middleware"
)

func Health(c web.C, w http.ResponseWriter, r *http.Request) {
	_, err := w.Write([]byte("OK"))
	if err != nil {
		log.Fatalf("Failed writing OK in response to a health check [Error: %v]", err)
	}
}

func Mux(b *backend.Backend) *web.Mux {
	healthCheckMux := web.New()

	healthCheckMux.Use(goji_middleware.Recoverer)
	healthCheckMux.Use(goji_middleware.EnvInit)
	healthCheckMux.Use(healthCheckMux.Router)
	healthCheckMux.Use(goji_middleware.RequestID)
	healthCheckMux.Use(b.Middleware.Metrics.Metrics)
	healthCheckMux.Get("/health-check", Health)
	healthCheckMux.Get("/", Health)
	healthCheckMux.Get("/profiler", api.Profiler)

	return healthCheckMux
}
