package secure

import (
	"code.justin.tv/commerce/payday/api/apidef"
	"code.justin.tv/commerce/payday/backend"
	"code.justin.tv/commerce/payday/decorators"
	"github.com/zenazn/goji/web"
	goji_middleware "github.com/zenazn/goji/web/middleware"
)

func Mux(b *backend.Backend) *web.Mux {
	secureMux := web.New()

	// This one is used web/revenuereporting-workers presumably to generate reports for the dashboard.
	// This is left because MoneyPenny has a python script that calls this and there is definitely not support for that in S2S.
	secureMux.Get("/admin/dashboard/:tuid/revenue", decorators.DecorateAndAdapt(b.API.Dashboard.GetBroadcasterRevenue, apidef.AdminGetBroadcasterRevenue))
	// This is because MoneyPenny calls these, we need to work with them to migrate to S2S.
	secureMux.Get("/admin/withholding/:accountId", decorators.DecorateAndAdapt(b.API.Witholding.GetWithholdingByAccountId, apidef.GetWithholdingByPayoutEntityId))
	secureMux.Get("/admin/withholding/account/:payoutEntityId", decorators.DecorateAndAdapt(b.API.Witholding.GetWithholdingByPayoutEntityId, apidef.GetWithholdingByAccountId))

	// Publicly documented / accessible API calls, proxied to Payday through visage
	secureMux.Get("/api/actions", decorators.DecorateAndAdapt(b.API.Action.GetActions, apidef.GetActions))
	secureMux.Get("/api/actions/asset", decorators.DecorateAndAdapt(b.API.ActionAsset.GetActionAsset, apidef.GetActionAsset))
	secureMux.Get("/api/actions/info", decorators.DecorateAndAdapt(b.API.ActionInfo.GetActionsInfo, apidef.GetActionsInfo))
	secureMux.Get("/api/users/:tuid/events", decorators.DecorateAndAdapt(b.API.Event.GetUserEventsWithAuth, apidef.GetBitsEventsForUser))

	// LEADERBOARDS
	secureMux.Get("/api/leaderboards/:channel", decorators.DecorateAndAdapt(b.API.Leaderboard.GetChannelLeaderboard, apidef.GetChannelLeaderboard))
	secureMux.Get("/api/products", decorators.DecorateAndAdapt(b.API.Products.GetProducts, apidef.GetProducts))

	// APIs moved off NGINX to Visage
	secureMux.Get("/api/channels/:tuid", decorators.DecorateAndAdapt(b.API.Channel.GetChannelInfo, apidef.GetChannelInfoVisage))
	secureMux.Get("/api/balance/:tuid", decorators.DecorateAndAdapt(b.API.Balance.GetBalanceWithCartmanAuthentication, apidef.GetBalanceVisage))
	secureMux.Get("/api/users/:tuid", decorators.DecorateAndAdapt(b.API.User.GetUserInfoWithCartmanAuthentication, apidef.GetUserInfoVisage))
	secureMux.Get("/api/prices", decorators.DecorateAndAdapt(b.API.Prices.GetPrices, apidef.GetPricesVisage))
	secureMux.Get("/api/channels/:tuid/settings", decorators.DecorateAndAdapt(b.API.Channel.GetSettingsWithCartmanAuthentication, apidef.GetChannelSettingsVisage))
	secureMux.Put("/api/channels/:tuid/settings", decorators.DecorateAndAdapt(b.API.Channel.UpdateSettingsWithCartmanAuthentication, apidef.UpdateChannelSettingsVisage))
	secureMux.Get("/api/badges/:tuid/tiers", decorators.DecorateAndAdapt(b.API.Badge.GetBadgeTiersWithCartmanAuthentication, apidef.GetBadgeTiersVisage))
	secureMux.Put("/api/badges/:tuid/tiers", decorators.DecorateAndAdapt(b.API.Badge.SetBadgeTiersWithCartmanAuthentication, apidef.SetBadgeTiersVisage))
	secureMux.Get("/api/channels/:tuid/customcheermotes", decorators.DecorateAndAdapt(b.API.Image.GetCustomCheermotes, apidef.GetCustomCheermotes))
	secureMux.Post("/api/customcheermotes", decorators.DecorateAndAdapt(b.API.Image.StartCustomCheermoteUpload, apidef.StartCustomCheermoteUpload))

	// Configure payday goji middleware for admin calls
	secureMux.Use(goji_middleware.Recoverer)
	secureMux.Use(goji_middleware.EnvInit)
	secureMux.Use(secureMux.Router)
	secureMux.Use(goji_middleware.RequestID)
	secureMux.Use(b.Middleware.Metrics.Metrics)
	secureMux.Use(b.Middleware.ClientID.GetClientId)
	secureMux.Use(b.Middleware.Cartman.WithCartmanToken)
	secureMux.Use(b.Middleware.AuditLogging.AttachRequestInfoToContext)

	return secureMux
}
