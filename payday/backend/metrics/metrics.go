package metrics

import (
	"time"

	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/metrics"
	"code.justin.tv/commerce/splatter"
	"github.com/cactus/go-statsd-client/statsd"
)

func NewMetricsLogger(cfg config.Configuration) (metrics.Statter, error) {
	statters := make([]statsd.Statter, 0)

	cloudwatchStatter := splatter.NewBufferedTelemetryCloudWatchStatter(&splatter.BufferedTelemetryConfig{
		AWSRegion:         cfg.SQSRegion,
		ServiceName:       "payday",
		Stage:             cfg.Environment,
		Substage:          "primary",
		BufferSize:        100000,
		AggregationPeriod: time.Minute,
		FlushPeriod:       time.Second * 30,
	}, map[string]bool{})

	statters = append(statters, cloudwatchStatter)

	stats, err := splatter.NewCompositeStatter(statters)
	if err != nil {
		return nil, err
	}

	return metrics.NewStatter(stats), nil
}
