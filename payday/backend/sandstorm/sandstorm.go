package sandstorm

import (
	"code.justin.tv/commerce/payday/clients/sandstorm"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/errors"
)

func NewSandstorm(cfg config.Configuration) sandstorm.SecretGetter {
	var sand sandstorm.SecretGetter

	if cfg.Environment == config.DevelopmentEnvironment {
		sand = &sandstorm.NoOpSecretGetter{}
	} else {
		sand = sandstorm.New(cfg.SandstormRole)
	}

	return sand
}

func GetSecretPlainText(sandstormGetter sandstorm.SecretGetter, secretKey string) (string, error) {
	secret, err := sandstormGetter.Get(secretKey)
	if err != nil {
		return "", err
	}

	secretStr := string(secret.Plaintext)
	if secretStr == "" {
		return secretStr, errors.New("empty secret returned from sandstorm")
	}

	return secretStr, nil
}
