package backend

import (
	"code.justin.tv/amzn/TwitchS2S2/s2s2"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/backend/actions"
	"code.justin.tv/commerce/payday/backend/api"
	"code.justin.tv/commerce/payday/backend/caches"
	"code.justin.tv/commerce/payday/backend/cartman"
	"code.justin.tv/commerce/payday/backend/clients"
	"code.justin.tv/commerce/payday/backend/dynamo"
	"code.justin.tv/commerce/payday/backend/features"
	"code.justin.tv/commerce/payday/backend/fetchers"
	"code.justin.tv/commerce/payday/backend/lambdas"
	"code.justin.tv/commerce/payday/backend/metrics"
	"code.justin.tv/commerce/payday/backend/middleware"
	"code.justin.tv/commerce/payday/backend/observers"
	"code.justin.tv/commerce/payday/backend/pollers"
	"code.justin.tv/commerce/payday/backend/sqs"
	"code.justin.tv/commerce/payday/config"
	metrics2 "code.justin.tv/commerce/payday/metrics"
	"code.justin.tv/commerce/payday/rpc/corleone_tenant_server"
	"code.justin.tv/commerce/payday/rpc/paydayserver"
	bitsprismtenant "code.justin.tv/commerce/payday/rpc/prism_tenant_server/bits"
	voldsprismtenant "code.justin.tv/commerce/payday/rpc/prism_tenant_server/volds"
	"code.justin.tv/commerce/payday/s3"
	prism_tenant "code.justin.tv/commerce/prism/rpc/tenant"
	offerstenantrpc "code.justin.tv/revenue/offer-tenant-schema/rpc"
	metricCollector "github.com/afex/hystrix-go/hystrix/metric_collector" //nolint:depguard
	"github.com/facebookgo/inject"
)

type Backend struct {
	TwirpServer            *paydayserver.Server         `inject:""`
	BitsPrismTenantServer  prism_tenant.PrismTenant     `inject:"bits-prism-tenant-server"`
	VoldsPrismTenantServer prism_tenant.PrismTenant     `inject:"volds-prism-tenant-server"`
	OfferTenantServer      offerstenantrpc.OffersTenant `inject:""`
	API                    *api.Handlers                `inject:""`
	Middleware             *middleware.Middleware       `inject:""`
	SQS                    *sqs.SQS                     `inject:""`
	Pollers                *pollers.Pollers             `inject:""`
	Clients                *clients.Clients             `inject:""`
	S3Client               s3.IS3Client                 `inject:""`
}

func Provide(cfg config.Configuration, s2s2Client *s2s2.S2S2) (*Backend, error) {
	m, err := metrics.NewMetricsLogger(cfg)
	if err != nil {
		log.WithError(err).Fatal("Could not start up metrics loggers")
	}

	serviceClients, err := clients.NewClients(cfg, s2s2Client)
	if err != nil {
		return nil, err
	}

	graph := inject.Graph{}

	twirpServer := paydayserver.NewServer()
	bitsPrismTenantServer := bitsprismtenant.NewServer()
	voldsPrismTenantServer := voldsprismtenant.NewServer()
	corleoneTenantServer := corleone_tenant_server.NewServer()
	backend := &Backend{}

	hystrixMetrics := metrics2.NewHystrixMetricsCollector()

	objectList := []*inject.Object{
		{Value: &cfg},
		{Value: m},
		{Value: backend},
		{Value: twirpServer},
		{Value: bitsPrismTenantServer, Name: "bits-prism-tenant-server"},
		{Value: voldsPrismTenantServer, Name: "volds-prism-tenant-server"},
		{Value: corleoneTenantServer},
		{Value: hystrixMetrics},
	}

	objectList = append(objectList, serviceClients...)
	objectList = append(objectList, dynamo.Provide(cfg)...)
	objectList = append(objectList, actions.Provide(cfg)...)
	objectList = append(objectList, caches.Provide(cfg)...)
	objectList = append(objectList, fetchers.Provide(cfg)...)
	objectList = append(objectList, pollers.Provide(cfg)...)
	objectList = append(objectList, observers.Provide()...)
	objectList = append(objectList, api.Provide()...)
	objectList = append(objectList, middleware.Provide(cfg)...)
	objectList = append(objectList, sqs.Provide(cfg)...)
	objectList = append(objectList, cartman.ProvideCartman(cfg, cfg.SandstormManager)...)
	objectList = append(objectList, features.Provide(cfg)...)
	objectList = append(objectList, lambdas.Provide(cfg)...)

	err = graph.Provide(objectList...)
	if err != nil {
		log.Panicf("inject used Provide, It's super effective. %v", err)
	}

	err = graph.Populate()
	if err != nil {
		log.Panicf("inject used Populate. It's super effective. %v", err)
	}

	metricCollector.Registry.Register(func(name string) metricCollector.MetricCollector {
		return hystrixMetrics.NewHystrixCommandMetricsCollector(name)
	})

	return backend, nil
}
