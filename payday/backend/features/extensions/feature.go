package extensions

import (
	"code.justin.tv/commerce/payday/extension/pubsub"
	"code.justin.tv/commerce/payday/extension/transaction"
	"github.com/facebookgo/inject"
)

func Provide() []*inject.Object {
	return []*inject.Object{
		{Value: &transaction.ManagerImpl{}},
		{Value: &pubsub.ManagerImpl{}},
	}
}
