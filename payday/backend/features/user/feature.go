package user

import (
	"code.justin.tv/commerce/payday/user"
	"github.com/facebookgo/inject"
)

func Provide() []*inject.Object {
	return []*inject.Object{
		{Value: user.NewFetcher()},
		{Value: user.NewChecker()},
		{Value: user.NewSetter()},
	}
}
