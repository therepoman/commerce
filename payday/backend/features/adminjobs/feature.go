package adminjobs

import (
	"code.justin.tv/commerce/payday/adminjob"
	"code.justin.tv/commerce/payday/adminjob/handler"
	"code.justin.tv/commerce/payday/adminjob/handler/bulkentitle"
	"code.justin.tv/commerce/payday/adminjob/handler/bulkpromonotif"
	"code.justin.tv/commerce/payday/adminjob/handler/lbbackfill"
	"code.justin.tv/commerce/payday/adminjob/handler/singlepurchasebackfill"
	"code.justin.tv/commerce/payday/adminjob/handler/transactionbackfill"
	"code.justin.tv/commerce/payday/config"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/facebookgo/inject"
)

func Provide(cfg config.Configuration) []*inject.Object {
	sess, _ := session.NewSession(&aws.Config{
		Region: aws.String(cfg.DynamoRegion),
	})

	return []*inject.Object{
		{Value: &handler.HandlerFactoryImpl{}},
		{Value: &bulkentitle.FactoryImpl{}},
		{Value: &bulkpromonotif.FactoryImpl{}},
		{Value: &lbbackfill.FactoryImpl{}},
		{Value: &adminjob.Manager{}},
		{Value: transactionbackfill.NewFactory()},
		{Value: transactionbackfill.NewDAO(sess, cfg)},
		{Value: singlepurchasebackfill.NewFactory()},
	}
}
