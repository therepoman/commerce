package custom_cheermotes

import (
	"code.justin.tv/commerce/payday/cheers"
	"github.com/facebookgo/inject"
)

func Provide() []*inject.Object {
	return []*inject.Object{
		// CustomChannel Cheers (Actions API V2)
		{Value: cheers.NewChannelCustomCheermoteGetter()},
	}
}
