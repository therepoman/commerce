package balance

import (
	"code.justin.tv/commerce/payday/balance"
	"github.com/facebookgo/inject"
)

func Provide() []*inject.Object {
	return []*inject.Object{
		{Value: balance.NewGetter()},
	}
}
