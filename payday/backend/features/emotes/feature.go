package emotes

import (
	"code.justin.tv/commerce/payday/emote"
	"code.justin.tv/commerce/payday/message"
	"github.com/facebookgo/inject"
)

func Provide() []*inject.Object {
	return []*inject.Object{
		{Value: &emote.PrefixManager{}},
		{Value: &message.TokenizerFactory{}},
	}
}
