package image

import (
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/image"
	"github.com/facebookgo/inject"
)

func Provide(cfg config.Configuration) []*inject.Object {
	return []*inject.Object{
		{Value: &image.ImageManager{}},
	}
}
