package refill

import (
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/refill"
	"github.com/facebookgo/inject"
)

func Provide(cfg config.Configuration) []*inject.Object {
	return []*inject.Object{
		{Value: refill.NewRefillManager(cfg.EverdeenResponseQueue)},
	}
}
