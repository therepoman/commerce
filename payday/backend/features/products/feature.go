package products

import (
	"code.justin.tv/commerce/payday/products"
	"code.justin.tv/commerce/payday/products/availability"
	bits_entitlements "code.justin.tv/commerce/payday/products/entitlements/getter"
	recent_platform "code.justin.tv/commerce/payday/products/purchase_by_most_recent_super_platform/getter"
	single_purchase "code.justin.tv/commerce/payday/products/single_purchase_consumption/getter"
	"github.com/facebookgo/inject"
)

func Provide() []*inject.Object {
	return []*inject.Object{
		{Value: products.NewValidator()},
		{Value: products.NewChecker()},
		{Value: products.NewParser()},
		{Value: products.NewGetter()},
		{Value: products.NewTranslator()},
		{Value: products.NewSorter()},
		{Value: products.NewLoggedOutResolver()},
		{Value: availability.NewProductAvailabilityChecker()},
		{Value: single_purchase.NewGetter()},
		{Value: bits_entitlements.NewGetter()},
		{Value: recent_platform.NewGetter()},
	}
}
