package entitle

import (
	"code.justin.tv/commerce/payday/entitle"
	"github.com/facebookgo/inject"
)

func Provide() []*inject.Object {
	return []*inject.Object{
		{Value: entitle.NewValidator()},
		{Value: entitle.NewPachinko(), Name: "pachinkoEntitler"},
		{Value: entitle.NewParallel(), Name: "parallelEntitler"},
	}
}
