package polls

import (
	"code.justin.tv/commerce/payday/rpc/paydayserver/use_bits_on_poll/transaction"
	"code.justin.tv/commerce/payday/rpc/paydayserver/use_bits_on_poll/transaction/dynamo"
	"code.justin.tv/commerce/payday/rpc/paydayserver/use_bits_on_poll/transaction/pachinko"
	"github.com/facebookgo/inject"
)

func Provide() []*inject.Object {
	return []*inject.Object{
		{Value: transaction.NewTransactor()},
		{Value: pachinko.NewTransactor(), Name: "pachinko_transactor"},
		{Value: dynamo.NewSaver()},
	}
}
