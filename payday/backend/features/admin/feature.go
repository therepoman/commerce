package admin

import (
	"code.justin.tv/commerce/payday/admin/add_bits"
	"code.justin.tv/commerce/payday/admin/remove_bits"
	"github.com/facebookgo/inject"
)

func Provide() []*inject.Object {
	return []*inject.Object{
		{Value: &add_bits.ValidatorImpl{}},
		{Value: &remove_bits.ValidatorImpl{}},
		{Value: &add_bits.ProcessorImpl{}},
		{Value: &remove_bits.ProcessorImpl{}},
		{Value: &add_bits.ThresholdBreachProcessorImpl{}, Name: "thresholdBreachProcessor"},
	}
}
