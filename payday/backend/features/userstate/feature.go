package userstate

import (
	"code.justin.tv/commerce/payday/userstate"
	"code.justin.tv/commerce/payday/userstate/firstcheer"
	"code.justin.tv/commerce/payday/userstate/firstpurchase"
	"code.justin.tv/commerce/payday/userstate/search/purchasebits"
	"code.justin.tv/commerce/payday/userstate/search/usercheered"
	"code.justin.tv/commerce/payday/userstate/settings"
	"github.com/facebookgo/inject"
)

func Provide() []*inject.Object {
	return []*inject.Object{
		// user settings interfaces
		{Value: settings.NewGetter()},
		{Value: settings.NewFetcher()},
		{Value: settings.NewSetter()},

		{Value: userstate.NewBouncer()},
		{Value: userstate.NewGetter()},
		{Value: userstate.NewFetcher()},
		{Value: userstate.NewSetter()},
		{Value: userstate.NewSearcher()},

		{Value: purchasebits.NewSearcher(), Name: "purchaseBitsSearcher"},
		{Value: usercheered.NewSearcher()},

		{Value: firstpurchase.NewPubSubber()},
		{Value: firstcheer.NewUserNotice()},
	}
}
