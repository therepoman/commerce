package userservice

import (
	"code.justin.tv/commerce/payday/userservice"
	"github.com/facebookgo/inject"
)

func Provide() []*inject.Object {
	return []*inject.Object{
		{Value: userservice.NewFetcher()},
		{Value: userservice.NewRetriever()},
		{Value: userservice.NewChecker()},
	}
}
