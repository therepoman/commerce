package givers

import (
	"code.justin.tv/commerce/payday/event"
	"github.com/facebookgo/inject"
)

func Provide() []*inject.Object {
	return []*inject.Object{
		{Value: event.NewPachinkoGiver(), Name: "pachinkoGiver"},
	}
}
