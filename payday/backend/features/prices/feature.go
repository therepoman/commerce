package prices

import (
	"code.justin.tv/commerce/payday/prices"
	"github.com/facebookgo/inject"
)

func Provide() []*inject.Object {
	return []*inject.Object{
		{Value: &prices.FloPricer{}},
	}
}
