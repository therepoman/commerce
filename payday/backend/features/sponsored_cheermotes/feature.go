package sponsored_cheermotes

import (
	"code.justin.tv/commerce/payday/cheers"
	"code.justin.tv/commerce/payday/sponsored_cheermote"
	"code.justin.tv/commerce/payday/sponsored_cheermote/actions"
	"code.justin.tv/commerce/payday/sponsored_cheermote/campaign"
	"code.justin.tv/commerce/payday/sponsored_cheermote/channel_status"
	"code.justin.tv/commerce/payday/sponsored_cheermote/eligible_channel"
	"code.justin.tv/commerce/payday/sponsored_cheermote/user_campaign_status"
	"github.com/facebookgo/inject"
)

func Provide() []*inject.Object {
	return []*inject.Object{
		{Value: sponsored_cheermote.NewIncrementor()},

		// campaigns
		{Value: campaign.NewManager()},
		{Value: campaign.NewPubSubber()},
		{Value: campaign.NewGetter(), Name: "campaignGetter"},

		// campaign channel status
		{Value: channel_status.NewGetter(), Name: "channelStatusGetter"},
		{Value: channel_status.NewSetter()},
		{Value: channel_status.NewFilterer()},
		{Value: channel_status.NewPubSubber()},

		// campaign user status
		{Value: user_campaign_status.NewGetter()},

		// campaign actions
		{Value: actions.NewGetter()},

		// campaign channel eligibility
		{Value: eligible_channel.NewGetter()},

		// Campaign Cheers (Actions API V2)
		{Value: cheers.NewSponsoredCheermoteGetter()},
	}
}
