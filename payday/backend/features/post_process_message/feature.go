package post_process_message

import (
	"code.justin.tv/commerce/payday/prism_tenant/post_process_message/bits"
	"code.justin.tv/commerce/payday/prism_tenant/post_process_message/bits/transaction"
	"code.justin.tv/commerce/payday/prism_tenant/post_process_message/volds"
	"github.com/facebookgo/inject"
)

func Provide() []*inject.Object {
	return []*inject.Object{
		// bits
		{Value: transaction.NewSaver()},
		{Value: bits.NewMessagePostProcessor(), Name: "bits-message-post-processor"},

		// volds
		{Value: volds.NewMessagePostProcessor(), Name: "volds-message-post-processor"},
	}
}
