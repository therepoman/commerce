package slack

import (
	"code.justin.tv/commerce/payday/badges"
	"code.justin.tv/commerce/payday/clients/slack"
	"code.justin.tv/commerce/payday/clips"
	"code.justin.tv/commerce/payday/image"
	"code.justin.tv/commerce/payday/tmi"
	"github.com/facebookgo/inject"
)

func Provide() []*inject.Object {
	return []*inject.Object{
		{Value: slack.NewSlacker()},
		{Value: &badges.CustomBitsBadgeSlacker{}},
		{Value: &tmi.TMIFailureSlacker{}},
		{Value: &image.CustomCheermoteImageSlacker{}},
		{Value: &clips.ClipsManager{}},
	}
}
