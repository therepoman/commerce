package transactions

import (
	"code.justin.tv/commerce/payday/transactions"
	"github.com/facebookgo/inject"
)

func Provide() []*inject.Object {
	return []*inject.Object{
		{Value: transactions.NewGetter()},
		{Value: transactions.NewFetcher()},
		{Value: transactions.NewConverter()},
	}
}
