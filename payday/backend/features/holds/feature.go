package holds

import (
	"code.justin.tv/commerce/payday/holds"
	"code.justin.tv/commerce/payday/rpc/paydayserver/check_hold"
	"code.justin.tv/commerce/payday/rpc/paydayserver/create_hold"
	"code.justin.tv/commerce/payday/rpc/paydayserver/finalize_hold"
	"code.justin.tv/commerce/payday/rpc/paydayserver/release_hold"
	"github.com/facebookgo/inject"
)

func Provide() []*inject.Object {
	return []*inject.Object{
		{Value: holds.NewFetcher()},
		{Value: holds.NewGetter(), Name: "holdsGetter"},
		{Value: check_hold.NewValidator()},
		{Value: holds.NewCreator(), Name: "holdCreator"},
		{Value: create_hold.NewValidator()},
		{Value: holds.NewReleaser()},
		{Value: release_hold.NewValidator()},
		{Value: holds.NewFinalizer()},
		{Value: finalize_hold.NewValidator()},
	}
}
