package process_message

import (
	"code.justin.tv/commerce/payday/prism_tenant/process_message/bits"
	"code.justin.tv/commerce/payday/prism_tenant/process_message/bits/datascience"
	"code.justin.tv/commerce/payday/prism_tenant/process_message/bits/sns"
	"code.justin.tv/commerce/payday/prism_tenant/process_message/bits/sponsored_cheermotes"
	"code.justin.tv/commerce/payday/prism_tenant/process_message/bits/transaction"
	"code.justin.tv/commerce/payday/prism_tenant/process_message/bits/transformations"
	"code.justin.tv/commerce/payday/prism_tenant/process_message/bits/transformations/sponsored_cheermotes_bonus"
	"code.justin.tv/commerce/payday/prism_tenant/process_message/volds"
	"github.com/facebookgo/inject"
)

func Provide() []*inject.Object {
	return []*inject.Object{
		// bits
		{Value: datascience.NewDataScienceTracker()},
		{Value: sns.NewSNSPublisher()},
		{Value: transformations.NewMessageTransformer()},
		{Value: sponsored_cheermotes.NewSponsoredCheermoteValidator()},
		{Value: sponsored_cheermotes_bonus.NewSponsoredCheermoteBonusMessageTransformer()},
		{Value: bits.NewMessageProcessor(), Name: "bits-message-processor"},
		{Value: transaction.NewCreator()},

		// volds
		{Value: volds.NewMessageProcessor(), Name: "volds-message-processor"},
	}
}
