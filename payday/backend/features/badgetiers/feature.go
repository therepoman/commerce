package badgetiers

import (
	"code.justin.tv/commerce/payday/badgetiers/admin"
	"code.justin.tv/commerce/payday/badgetiers/fetcher"
	"code.justin.tv/commerce/payday/badgetiers/logician"
	bitsbadges "code.justin.tv/commerce/payday/badgetiers/products/badges"
	badgetieremotes "code.justin.tv/commerce/payday/badgetiers/products/emotes"
	tiernotifications "code.justin.tv/commerce/payday/badgetiers/products/notifications"
	badgeusers "code.justin.tv/commerce/payday/badgetiers/products/users"
	"code.justin.tv/commerce/payday/leaderboard"
	"github.com/facebookgo/inject"
)

func Provide() []*inject.Object {
	return []*inject.Object{
		{Value: fetcher.NewFetcher()},
		{Value: logician.NewChatBadgeLogician()},

		// badge images and settings
		{Value: bitsbadges.NewEquipper()},
		{Value: bitsbadges.NewLogician()},
		{Value: bitsbadges.NewFiller()},
		{Value: bitsbadges.NewSaver()},

		// leaderboard stuff
		{Value: &leaderboard.BadgeReconcilerImpl{}},
		{Value: leaderboard.NewBitsToBroadcaster()},

		// badge tier notifications
		{Value: tiernotifications.NewGetter()},
		{Value: tiernotifications.NewFetcher()},
		{Value: tiernotifications.NewSetter()},
		{Value: tiernotifications.NewUserNotice()},
		{Value: tiernotifications.NewPubSubber()},
		{Value: tiernotifications.NewCache()},
		{Value: tiernotifications.NewDataScience()},
		{Value: tiernotifications.NewMessageValidator()},

		// badge tier unlocked users
		{Value: badgeusers.NewFetcher()},
		{Value: badgeusers.NewFiller()},

		// badge tier emote rewards
		{Value: badgetieremotes.NewFiller()},
		{Value: badgetieremotes.NewSaver()},
		{Value: badgetieremotes.NewEntitler()},
		{Value: badgetieremotes.NewValidator()},
		{Value: badgetieremotes.NewLogician()},
		{Value: badgetieremotes.NewAssigner()},

		// admin handlers
		{Value: admin.NewBadgeTierAdmin()},
	}
}
