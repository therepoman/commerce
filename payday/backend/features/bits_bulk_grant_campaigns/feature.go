package bits_bulk_grant_campaigns

import (
	get_bits_bulk_grant_campaigns "code.justin.tv/commerce/payday/rpc/paydayserver/bits_bulk_grant_campaigns"
	"github.com/facebookgo/inject"
)

func Provide() []*inject.Object {
	return []*inject.Object{
		{Value: &get_bits_bulk_grant_campaigns.ServerImpl{}},
	}
}
