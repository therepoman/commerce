package tokenize_and_validate

import (
	"code.justin.tv/commerce/payday/prism_tenant/tokenize_and_validate_message/bits"
	"code.justin.tv/commerce/payday/prism_tenant/tokenize_and_validate_message/bits/datascience"
	"code.justin.tv/commerce/payday/prism_tenant/tokenize_and_validate_message/bits/validate/balance"
	"code.justin.tv/commerce/payday/prism_tenant/tokenize_and_validate_message/bits/validate/bits_amount"
	"code.justin.tv/commerce/payday/prism_tenant/tokenize_and_validate_message/bits/validate/channel_eligibility"
	"code.justin.tv/commerce/payday/prism_tenant/tokenize_and_validate_message/bits/validate/channel_min_bits"
	"code.justin.tv/commerce/payday/prism_tenant/tokenize_and_validate_message/bits/validate/channel_min_bits_emote"
	"code.justin.tv/commerce/payday/prism_tenant/tokenize_and_validate_message/bits/validate/token"
	"code.justin.tv/commerce/payday/prism_tenant/tokenize_and_validate_message/bits/validate/user_eligibility"
	"code.justin.tv/commerce/payday/prism_tenant/tokenize_and_validate_message/volds"
	"github.com/facebookgo/inject"
)

func Provide() []*inject.Object {
	return []*inject.Object{
		// bits
		{Value: bits_amount.NewValidator()},
		{Value: channel_eligibility.NewValidator()},
		{Value: channel_min_bits.NewValidator()},
		{Value: channel_min_bits_emote.NewValidator()},
		{Value: datascience.NewDataScienceTracker()},
		{Value: token.NewValidator()},
		{Value: user_eligibility.NewValidator()},
		{Value: balance.NewValidator()},
		{Value: bits.NewTokenizeAndValidate(), Name: "bits-tokenize-and-validate"},

		// volds
		{Value: volds.NewTokenizeAndValidate(), Name: "volds-tokenize-and-validate"},
	}
}
