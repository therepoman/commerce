package features

import (
	"code.justin.tv/commerce/payday/backend/features/admin"
	"code.justin.tv/commerce/payday/backend/features/adminjobs"
	"code.justin.tv/commerce/payday/backend/features/badgetiers"
	"code.justin.tv/commerce/payday/backend/features/balance"
	"code.justin.tv/commerce/payday/backend/features/bits_bulk_grant_campaigns"
	"code.justin.tv/commerce/payday/backend/features/custom_cheermotes"
	"code.justin.tv/commerce/payday/backend/features/emotes"
	"code.justin.tv/commerce/payday/backend/features/entitle"
	"code.justin.tv/commerce/payday/backend/features/extensions"
	"code.justin.tv/commerce/payday/backend/features/givers"
	"code.justin.tv/commerce/payday/backend/features/holds"
	"code.justin.tv/commerce/payday/backend/features/image"
	"code.justin.tv/commerce/payday/backend/features/polls"
	"code.justin.tv/commerce/payday/backend/features/post_process_message"
	"code.justin.tv/commerce/payday/backend/features/prices"
	"code.justin.tv/commerce/payday/backend/features/process_message"
	"code.justin.tv/commerce/payday/backend/features/products"
	"code.justin.tv/commerce/payday/backend/features/refill"
	"code.justin.tv/commerce/payday/backend/features/slack"
	"code.justin.tv/commerce/payday/backend/features/sponsored_cheermotes"
	"code.justin.tv/commerce/payday/backend/features/tokenize_and_validate"
	"code.justin.tv/commerce/payday/backend/features/transactions"
	"code.justin.tv/commerce/payday/backend/features/user"
	"code.justin.tv/commerce/payday/backend/features/userservice"
	"code.justin.tv/commerce/payday/backend/features/userstate"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/pachter"
	"code.justin.tv/commerce/payday/payout"
	"code.justin.tv/commerce/payday/throttle"
	"github.com/facebookgo/inject"
)

func Provide(cfg config.Configuration) []*inject.Object {
	featureObjects := []*inject.Object{
		{Value: &payout.PayoutManager{}},
		{Value: pachter.NewPachterMessenger()},
		// Throttler
		{Value: throttle.NewThrottler()},
	}

	featureObjects = append(featureObjects, admin.Provide()...)
	featureObjects = append(featureObjects, adminjobs.Provide(cfg)...)
	featureObjects = append(featureObjects, balance.Provide()...)
	featureObjects = append(featureObjects, bits_bulk_grant_campaigns.Provide()...)
	featureObjects = append(featureObjects, badgetiers.Provide()...)
	featureObjects = append(featureObjects, givers.Provide()...)
	featureObjects = append(featureObjects, emotes.Provide()...)
	featureObjects = append(featureObjects, entitle.Provide()...)
	featureObjects = append(featureObjects, extensions.Provide()...)
	featureObjects = append(featureObjects, holds.Provide()...)
	featureObjects = append(featureObjects, image.Provide(cfg)...)
	featureObjects = append(featureObjects, post_process_message.Provide()...)
	featureObjects = append(featureObjects, prices.Provide()...)
	featureObjects = append(featureObjects, process_message.Provide()...)
	featureObjects = append(featureObjects, products.Provide()...)
	featureObjects = append(featureObjects, slack.Provide()...)
	featureObjects = append(featureObjects, sponsored_cheermotes.Provide()...)
	featureObjects = append(featureObjects, custom_cheermotes.Provide()...)
	featureObjects = append(featureObjects, tokenize_and_validate.Provide()...)
	featureObjects = append(featureObjects, userstate.Provide()...)
	featureObjects = append(featureObjects, userservice.Provide()...)
	featureObjects = append(featureObjects, user.Provide()...)
	featureObjects = append(featureObjects, transactions.Provide()...)
	featureObjects = append(featureObjects, polls.Provide()...)
	featureObjects = append(featureObjects, refill.Provide(cfg)...)

	return featureObjects
}
