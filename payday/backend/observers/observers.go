package observers

import (
	"code.justin.tv/commerce/payday/sqs/handlers/pantheon/observers"
	"code.justin.tv/commerce/payday/sqs/handlers/pantheon/observers/chat_badges"
	"code.justin.tv/commerce/payday/sqs/handlers/pantheon/observers/datascience"
	"github.com/facebookgo/inject"
)

func Provide() []*inject.Object {
	var pantheonEventChatBadgesObserver chat_badges.Observer
	var pantheonEventDataScienceObserver datascience.Observer
	pantheonEventObservers := observers.ObserverChain{}
	pantheonEventObservers.AddObservers(&pantheonEventChatBadgesObserver, &pantheonEventDataScienceObserver)

	return []*inject.Object{
		{Value: &pantheonEventChatBadgesObserver},
		{Value: &pantheonEventDataScienceObserver},
		{Value: &pantheonEventObservers},
	}
}
