package pollers

import (
	"code.justin.tv/commerce/payday/actions"
	"code.justin.tv/commerce/payday/backend/clients/pantheon"
	"code.justin.tv/commerce/payday/bulk_grant"
	"code.justin.tv/commerce/payday/cheers"
	"code.justin.tv/commerce/payday/clients/redis"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/sponsored_cheermote/campaign"
	"github.com/facebookgo/inject"
)

type Initer func() error

type Starter func()

type Poller struct {
	Start Starter
	Init  Initer
}

type Pollers struct {
	List []Poller
}

func Provide(cfg config.Configuration) []*inject.Object {
	actionsPoller := actions.NewSupportedActionsPoller(cfg.ActionsBucket, cfg.ActionsKey)
	globalCheersPoller := cheers.NewGlobalCheersPoller(cfg.GlobalCheersBucket, cfg.GlobalCheersKey)
	sponsoredCheermotePoller := campaign.NewPoller(cfg.SponsoredCheermoteCampaignsBucket, cfg.SponsoredCheermoteCampaignsKey)
	bulkGrantCampaignsPoller := bulk_grant.NewCampaignsPoller(cfg.BulkEntitleCampaignsBucket, cfg.BulkEntitleCampaignsKey)
	percentRolloutPoller := redis.NewDialer(cfg.PercentRolloutBucket, cfg.PercentRolloutKey)

	pantheonPinger := pantheon.NewPinger()

	pollers := &Pollers{
		List: []Poller{
			{
				Start: actionsPoller.RefreshActionListAtInterval,
				Init:  actionsPoller.Init,
			},
			{
				Start: globalCheersPoller.RefreshGlobalCheersAtInterval,
				Init:  globalCheersPoller.Init,
			},
			{
				Start: sponsoredCheermotePoller.RefreshCampaignsListAtInterval,
				Init:  sponsoredCheermotePoller.Init,
			},
			{
				Start: bulkGrantCampaignsPoller.RefreshCampaignsListAtInterval,
				Init:  bulkGrantCampaignsPoller.Init,
			},
			{
				Start: pantheonPinger.PingAtInterval,
				Init:  pantheonPinger.Init,
			},
			{
				Init:  percentRolloutPoller.Init,
				Start: percentRolloutPoller.RefreshPercentRolloutAtInterval,
			},
		},
	}

	return []*inject.Object{
		{Value: pollers},
		{Value: actionsPoller},
		{Value: globalCheersPoller},
		{Value: sponsoredCheermotePoller},
		{Value: bulkGrantCampaignsPoller},
		{Value: pantheonPinger},
		{Value: percentRolloutPoller},
	}
}
