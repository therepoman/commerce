package s2s

import (
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/sse/malachai/pkg/events"
	"code.justin.tv/sse/malachai/pkg/s2s/callee"
)

func Init(cfg config.Configuration) *callee.Client {
	var s2sCalleeClient callee.Client

	eventsWriterClient, err := events.NewEventLogger(events.Config{
		Environment:     "production", //environment of malachai, not the using service
		BufferSizeLimit: 0,
	}, log.New())

	if err != nil {
		log.Fatalf("Unable to create events writer client: %v", err)
	}

	if cfg.S2S.Enabled {
		calleeConfig := callee.Config{
			SupportWildCard: true,
		}

		s2sCalleeClient = callee.Client{
			ServiceName:        cfg.S2S.Name,
			EventsWriterClient: eventsWriterClient,
			Config:             &calleeConfig,
		}

		err := s2sCalleeClient.Start()
		if err != nil {
			log.Fatalf("Unable to create S2S Client: %v", err)
		}
	}

	return &s2sCalleeClient
}
