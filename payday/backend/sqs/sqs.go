package sqs

import (
	"code.justin.tv/commerce/payday/backend/sqs/handlers/adminjob"
	"code.justin.tv/commerce/payday/backend/sqs/handlers/autorefill"
	"code.justin.tv/commerce/payday/backend/sqs/handlers/badge"
	"code.justin.tv/commerce/payday/backend/sqs/handlers/badgetieremoticonbackfills"
	"code.justin.tv/commerce/payday/backend/sqs/handlers/balance"
	"code.justin.tv/commerce/payday/backend/sqs/handlers/bigbits"
	"code.justin.tv/commerce/payday/backend/sqs/handlers/channelsettings"
	"code.justin.tv/commerce/payday/backend/sqs/handlers/datascience"
	"code.justin.tv/commerce/payday/backend/sqs/handlers/eventbus"
	"code.justin.tv/commerce/payday/backend/sqs/handlers/extensionpubsub"
	"code.justin.tv/commerce/payday/backend/sqs/handlers/imagemoderation"
	"code.justin.tv/commerce/payday/backend/sqs/handlers/imageupdate"
	"code.justin.tv/commerce/payday/backend/sqs/handlers/leaderboard"
	"code.justin.tv/commerce/payday/backend/sqs/handlers/pachinko"
	"code.justin.tv/commerce/payday/backend/sqs/handlers/pachter"
	"code.justin.tv/commerce/payday/backend/sqs/handlers/pantheon"
	"code.justin.tv/commerce/payday/backend/sqs/handlers/perseus"
	"code.justin.tv/commerce/payday/backend/sqs/handlers/phanto"
	"code.justin.tv/commerce/payday/backend/sqs/handlers/poa"
	"code.justin.tv/commerce/payday/backend/sqs/handlers/pubsub/deprecatedpubsub"
	"code.justin.tv/commerce/payday/backend/sqs/handlers/pubsub/externalpubsub"
	"code.justin.tv/commerce/payday/backend/sqs/handlers/pushy"
	"code.justin.tv/commerce/payday/backend/sqs/handlers/revenue"
	"code.justin.tv/commerce/payday/backend/sqs/handlers/uploadservicestatus"
	"code.justin.tv/commerce/payday/backend/sqs/handlers/userdestroy"
	"code.justin.tv/commerce/payday/backend/sqs/handlers/userstate"
	"code.justin.tv/commerce/payday/config"
	psqs "code.justin.tv/commerce/payday/sqs"
	"code.justin.tv/commerce/payday/sqs/crypto"
	"github.com/facebookgo/inject"
)

type SQS struct {
	AdminJob                *adminjob.Processor                   `inject:""`
	Balance                 *balance.Processor                    `inject:""`
	Badge                   *badge.Processor                      `inject:""`
	BadgeTierEmoteBackfills *badgetieremoticonbackfills.Processor `inject:""`
	BigBits                 *bigbits.Processor                    `inject:""`
	ChannelSettings         *channelsettings.Processor            `inject:""`
	Datascience             *datascience.Processor                `inject:""`
	DeprecatedPubsub        *deprecatedpubsub.Processor           `inject:""`
	ExtensionPubSub         *extensionpubsub.Processor            `inject:""`
	ExternalPubsub          *externalpubsub.Processor             `inject:""`
	ImageModeration         *imagemoderation.Processor            `inject:""`
	ImageUpdate             *imageupdate.Processor                `inject:""`
	Leaderboard             *leaderboard.Processor                `inject:""`
	Pantheon                *pantheon.Processor                   `inject:""`
	POA                     *poa.Processor                        `inject:""`
	Revenue                 *revenue.Processor                    `inject:""`
	UploadServiceStatus     *uploadservicestatus.Processor        `inject:""`
	Pushy                   *pushy.Processor                      `inject:""`
	Phanto                  *phanto.Processor                     `inject:""`
	UserState               *userstate.Processor                  `inject:""`
	EventBus                *eventbus.Processor                   `inject:""`
	Perseus                 *perseus.Processor                    `inject:""`
	Pachinko                *pachinko.Processor                   `inject:""`
	AutoRefill              *autorefill.Processor                 `inject:""`
	EverdeenResponse        *autorefill.EverdeenResponseProcessor `inject:""`
	UserDestroy             *userdestroy.Processor                `inject:""`
	Pachter                 *pachter.Processor                    `inject:""`
}

func Provide(cfg config.Configuration) []*inject.Object {
	sqsHandlerObjects := []*inject.Object{
		{Value: &SQS{}},
		{Value: &psqs.SQSPollerParams{
			SQSRegion:                cfg.SQSRegion,
			VisibilityTimeoutSeconds: psqs.DefaultVisibilityTimeoutSeconds,
		}},
		{Value: crypto.NewKMSCipher(cfg)},
		{Value: psqs.NewFABSMessageAdapter()},
		{Value: cfg.BigBitsSlackWebhookURL, Name: "bigBitsSlackWebhookUrl"},
	}

	sqsHandlerObjects = append(sqsHandlerObjects, adminjob.ProvideObjects()...)
	sqsHandlerObjects = append(sqsHandlerObjects, badge.ProvideObjects()...)
	sqsHandlerObjects = append(sqsHandlerObjects, badgetieremoticonbackfills.ProvideObjects()...)
	sqsHandlerObjects = append(sqsHandlerObjects, balance.ProvideObjects()...)
	sqsHandlerObjects = append(sqsHandlerObjects, bigbits.ProvideObjects()...)
	sqsHandlerObjects = append(sqsHandlerObjects, channelsettings.ProvideObjects()...)
	sqsHandlerObjects = append(sqsHandlerObjects, datascience.ProvideObjects()...)
	sqsHandlerObjects = append(sqsHandlerObjects, extensionpubsub.ProvideObjects()...)
	sqsHandlerObjects = append(sqsHandlerObjects, externalpubsub.ProvideObjects()...)
	sqsHandlerObjects = append(sqsHandlerObjects, imagemoderation.ProvideObjects()...)
	sqsHandlerObjects = append(sqsHandlerObjects, imageupdate.ProvideObjects()...)
	sqsHandlerObjects = append(sqsHandlerObjects, deprecatedpubsub.ProvideObjects()...)
	sqsHandlerObjects = append(sqsHandlerObjects, leaderboard.ProvideObjects()...)
	sqsHandlerObjects = append(sqsHandlerObjects, pantheon.ProvideObjects()...)
	sqsHandlerObjects = append(sqsHandlerObjects, poa.ProvideObjects()...)
	sqsHandlerObjects = append(sqsHandlerObjects, revenue.ProvideObjects()...)
	sqsHandlerObjects = append(sqsHandlerObjects, uploadservicestatus.ProvideObjects()...)
	sqsHandlerObjects = append(sqsHandlerObjects, pushy.ProvideObjects()...)
	sqsHandlerObjects = append(sqsHandlerObjects, phanto.ProvideObjects()...)
	sqsHandlerObjects = append(sqsHandlerObjects, userstate.ProvideObjects()...)
	sqsHandlerObjects = append(sqsHandlerObjects, eventbus.ProvideObjects()...)
	sqsHandlerObjects = append(sqsHandlerObjects, perseus.ProvideObjects()...)
	sqsHandlerObjects = append(sqsHandlerObjects, pachinko.ProvideObjects()...)
	sqsHandlerObjects = append(sqsHandlerObjects, autorefill.ProvideObjects()...)
	sqsHandlerObjects = append(sqsHandlerObjects, userdestroy.ProvideObjects()...)
	sqsHandlerObjects = append(sqsHandlerObjects, pachter.ProvideObjects()...)

	return sqsHandlerObjects
}

func (s *SQS) Start() {
	s.AdminJob.Start()
	s.BigBits.Start()
	s.Balance.Start()
	s.Badge.Start()
	s.BadgeTierEmoteBackfills.Start()
	s.ChannelSettings.Start()
	s.Datascience.Start()
	s.ExtensionPubSub.Start()
	s.ExternalPubsub.Start()
	s.ImageModeration.Start()
	s.ImageUpdate.Start()
	s.DeprecatedPubsub.Start()
	s.Leaderboard.Start()
	s.Pantheon.Start()
	s.POA.Start()
	s.Revenue.Start()
	s.UploadServiceStatus.Start()
	s.Pushy.Start()
	s.Phanto.Start()
	s.UserState.Start()
	s.EventBus.Start()
	s.Perseus.Start()
	s.Pachinko.Start()
	s.AutoRefill.Start()
	s.EverdeenResponse.Start()
	s.UserDestroy.Start()
	s.Pachter.Start()
}
