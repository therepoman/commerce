package imagemoderation

import (
	"code.justin.tv/chat/golibs/graceful"
	"code.justin.tv/commerce/payday/backend/sqs/handlers/utils"
	"code.justin.tv/commerce/payday/config"
	psqs "code.justin.tv/commerce/payday/sqs"
	imagemoderationSQS "code.justin.tv/commerce/payday/sqs/handlers/imagemoderation"
	"github.com/facebookgo/inject"
)

type Processor struct {
	Handler *imagemoderationSQS.ImageModerationHandler `inject:""`
	Cfg     *config.Configuration                      `inject:""`
	SQSCfg  *psqs.SQSPollerParams                      `inject:""`
}

func ProvideObjects() []*inject.Object {
	return []*inject.Object{
		{Value: &Processor{}},
		{Value: &imagemoderationSQS.ImageModerationHandler{}},
	}
}

func (p *Processor) Start() {
	if utils.ShouldStartProcessingSQSMessages(p.Cfg.ImageModerationSQSQueueURL) {
		imageModerationSQSProcessorConfig := psqs.SQSPollerParams{
			SQSRegion:                p.Cfg.SQSRegion,
			VisibilityTimeoutSeconds: 60 * 5,
		}
		imageProcessor := psqs.NewProcessor(imageModerationSQSProcessorConfig, p.Cfg.ImageModerationSQSQueueURL, p.Handler.HandleMessage)
		graceful.Go(imageProcessor.PollSQS)
	}
}
