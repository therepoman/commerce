package deprecatedpubsub

import (
	"code.justin.tv/chat/golibs/graceful"
	"code.justin.tv/commerce/payday/backend/sqs/handlers/utils"
	"code.justin.tv/commerce/payday/config"
	psqs "code.justin.tv/commerce/payday/sqs"
	pubsubV1 "code.justin.tv/commerce/payday/sqs/handlers/pubsub/externalpubsub"
	"github.com/facebookgo/inject"
)

type Processor struct {
	Handler *pubsubV1.PubsubHandler  `inject:""`
	Cfg     *config.Configuration    `inject:""`
	Adapter *psqs.FABSMessageAdapter `inject:""`
	SQSCfg  *psqs.SQSPollerParams    `inject:""`
}

func ProvideObjects() []*inject.Object {
	return []*inject.Object{
		{Value: &Processor{}},
		{Value: &pubsubV1.PubsubHandler{}},
	}
}

func (p *Processor) Start() {
	if utils.ShouldStartProcessingSQSMessages(p.Cfg.BitEventV1SQSQueueURL) {
		pubSubProcessor := psqs.NewProcessor(*p.SQSCfg, p.Cfg.BitEventV1SQSQueueURL, p.Adapter.Adapt(p.Handler))
		graceful.Go(pubSubProcessor.PollSQS)
	}
}
