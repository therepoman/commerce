package externalpubsub

import (
	"code.justin.tv/chat/golibs/graceful"
	"code.justin.tv/commerce/payday/backend/sqs/handlers/utils"
	"code.justin.tv/commerce/payday/config"
	psqs "code.justin.tv/commerce/payday/sqs"
	pubsub "code.justin.tv/commerce/payday/sqs/handlers/pubsub/deprecatedpubsub"
	"github.com/facebookgo/inject"
)

type Processor struct {
	Handler *pubsub.PubsubHandler    `inject:""`
	Cfg     *config.Configuration    `inject:""`
	Adapter *psqs.FABSMessageAdapter `inject:""`
	SQSCfg  *psqs.SQSPollerParams    `inject:""`
}

func ProvideObjects() []*inject.Object {
	return []*inject.Object{
		{Value: &Processor{}},
		{Value: &pubsub.PubsubHandler{}},
	}
}

func (p *Processor) Start() {
	if utils.ShouldStartProcessingSQSMessages(p.Cfg.PubSubSQSQueueURL) {
		processor := psqs.NewProcessor(*p.SQSCfg, p.Cfg.PubSubSQSQueueURL, p.Adapter.Adapt(p.Handler))
		graceful.Go(processor.PollSQS)
	}
}
