package bigbits

import (
	"code.justin.tv/chat/golibs/graceful"
	"code.justin.tv/commerce/payday/backend/sqs/handlers/utils"
	"code.justin.tv/commerce/payday/config"
	psqs "code.justin.tv/commerce/payday/sqs"
	bigbitsSQS "code.justin.tv/commerce/payday/sqs/handlers/bigbits"
	"github.com/facebookgo/inject"
)

type Processor struct {
	Handler *bigbitsSQS.BigBitsHandler `inject:""`
	Cfg     *config.Configuration      `inject:""`
	Adapter *psqs.FABSMessageAdapter   `inject:""`
	SQSCfg  *psqs.SQSPollerParams      `inject:""`
}

func ProvideObjects() []*inject.Object {
	return []*inject.Object{
		{Value: &Processor{}},
		{Value: &bigbitsSQS.BigBitsHandler{}},
	}
}

func (p *Processor) Start() {
	if utils.ShouldStartProcessingSQSMessages(p.Cfg.BigBitsSQSQueueURL) {
		processor := psqs.NewProcessor(*p.SQSCfg, p.Cfg.BigBitsSQSQueueURL, p.Adapter.Adapt(p.Handler))
		graceful.Go(processor.PollSQS)
	}
}
