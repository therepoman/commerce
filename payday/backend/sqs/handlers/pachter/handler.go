package pachter

import (
	"code.justin.tv/chat/golibs/graceful"
	"code.justin.tv/commerce/payday/backend/sqs/handlers/utils"
	"code.justin.tv/commerce/payday/config"
	psqs "code.justin.tv/commerce/payday/sqs"
	"code.justin.tv/commerce/payday/sqs/handlers/pachter"
	"github.com/facebookgo/inject"
)

type Processor struct {
	Handler *pachter.Handler      `inject:""`
	Cfg     *config.Configuration `inject:""`
}

func ProvideObjects() []*inject.Object {
	return []*inject.Object{
		{Value: &Processor{}},
		{Value: pachter.NewHandler()},
	}
}

func (p *Processor) Start() {
	if utils.ShouldStartProcessingSQSMessages(p.Cfg.PachterQueueURL) {
		pachterProcessorConfig := psqs.SQSPollerParams{
			SQSRegion:                p.Cfg.SQSRegion,
			VisibilityTimeoutSeconds: 60,
		}
		pachterEventPoller := psqs.NewProcessor(pachterProcessorConfig, p.Cfg.PachterQueueURL, p.Handler.Handle)
		graceful.Go(pachterEventPoller.PollSQS)
	}
}
