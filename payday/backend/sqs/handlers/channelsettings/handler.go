package channelsettings

import (
	"code.justin.tv/chat/golibs/graceful"
	"code.justin.tv/commerce/payday/backend/sqs/handlers/utils"
	"code.justin.tv/commerce/payday/config"
	psqs "code.justin.tv/commerce/payday/sqs"
	"code.justin.tv/commerce/payday/sqs/handlers/channelsettings"
	"github.com/facebookgo/inject"
)

type Processor struct {
	Handler *channelsettings.ChannelSettingsHandler `inject:""`
	Cfg     *config.Configuration                   `inject:""`
	SQSCfg  *psqs.SQSPollerParams                   `inject:""`
}

func ProvideObjects() []*inject.Object {
	return []*inject.Object{
		{Value: &channelsettings.ChannelSettingsHandler{}},
		{Value: &Processor{}},
	}
}

func (p *Processor) Start() {
	if utils.ShouldStartProcessingSQSMessages(p.Cfg.ChannelSettingsSQSQueueURL) {
		processor := psqs.NewProcessor(*p.SQSCfg, p.Cfg.ChannelSettingsSQSQueueURL, p.Handler.HandleMessage)
		graceful.Go(processor.PollSQS)
	}
}
