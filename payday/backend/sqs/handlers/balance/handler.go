package balance

import (
	"code.justin.tv/chat/golibs/graceful"
	"code.justin.tv/commerce/payday/backend/sqs/handlers/utils"
	"code.justin.tv/commerce/payday/config"
	psqs "code.justin.tv/commerce/payday/sqs"
	balanceSQS "code.justin.tv/commerce/payday/sqs/handlers/balance"
	"github.com/facebookgo/inject"
)

type Processor struct {
	Handler *balanceSQS.BalanceHandler `inject:""`
	Cfg     *config.Configuration      `inject:""`
	Adapter *psqs.FABSMessageAdapter   `inject:""`
	SQSCfg  *psqs.SQSPollerParams      `inject:""`
}

func ProvideObjects() []*inject.Object {
	return []*inject.Object{
		{Value: &balanceSQS.BalanceHandler{}},
		{Value: &Processor{}},
	}
}

func (p *Processor) Start() {
	if utils.ShouldStartProcessingSQSMessages(p.Cfg.BitCacheSQSQueueURL) {
		processor := psqs.NewProcessor(*p.SQSCfg, p.Cfg.BitCacheSQSQueueURL, p.Adapter.Adapt(p.Handler))
		graceful.Go(processor.PollSQS)
	}
}
