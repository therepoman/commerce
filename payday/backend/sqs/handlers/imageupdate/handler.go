package imageupdate

import (
	"code.justin.tv/chat/golibs/graceful"
	"code.justin.tv/commerce/payday/backend/sqs/handlers/utils"
	"code.justin.tv/commerce/payday/config"
	psqs "code.justin.tv/commerce/payday/sqs"
	"code.justin.tv/commerce/payday/sqs/handlers/imageupdate"
	"github.com/facebookgo/inject"
)

type Processor struct {
	Handler *imageupdate.ImageUpdateHandler `inject:""`
	Cfg     *config.Configuration           `inject:""`
	SQSCfg  *psqs.SQSPollerParams           `inject:""`
}

func ProvideObjects() []*inject.Object {
	return []*inject.Object{
		{Value: &Processor{}},
		{Value: &imageupdate.ImageUpdateHandler{}},
	}
}

func (p *Processor) Start() {
	if utils.ShouldStartProcessingSQSMessages(p.Cfg.ImageUpdatesSQSQueueURL) {
		imageProcessor := psqs.NewProcessor(*p.SQSCfg, p.Cfg.ImageUpdatesSQSQueueURL, p.Handler.HandleMessage)
		graceful.Go(imageProcessor.PollSQS)
	}
}
