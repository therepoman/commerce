package pushy

import (
	"code.justin.tv/chat/golibs/graceful"
	"code.justin.tv/commerce/payday/backend/sqs/handlers/utils"
	"code.justin.tv/commerce/payday/config"
	psqs "code.justin.tv/commerce/payday/sqs"
	"code.justin.tv/commerce/payday/sqs/handlers/pushy"
	"github.com/facebookgo/inject"
)

type Processor struct {
	Handler *pushy.DARTNotificationHandler `inject:""`
	Cfg     *config.Configuration          `inject:""`
	Adapter *psqs.FABSMessageAdapter       `inject:""`
	SQSCfg  *psqs.SQSPollerParams          `inject:""`
}

func ProvideObjects() []*inject.Object {
	return []*inject.Object{
		{Value: &Processor{}},
		{Value: &pushy.DARTNotificationHandler{}},
	}
}

func (p *Processor) Start() {
	if utils.ShouldStartProcessingSQSMessages(p.Cfg.PushySQSQueueURL) {
		pushyEventPoller := psqs.NewProcessor(*p.SQSCfg, p.Cfg.PushySQSQueueURL, p.Adapter.Adapt(p.Handler))
		graceful.Go(pushyEventPoller.PollSQS)
	}
}
