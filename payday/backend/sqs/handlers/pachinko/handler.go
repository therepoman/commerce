package pachinko

import (
	"code.justin.tv/chat/golibs/graceful"
	"code.justin.tv/commerce/payday/backend/sqs/handlers/utils"
	"code.justin.tv/commerce/payday/config"
	psqs "code.justin.tv/commerce/payday/sqs"
	"code.justin.tv/commerce/payday/sqs/handlers/pachinko"
	"github.com/facebookgo/inject"
)

type Processor struct {
	Handler *pachinko.Handler     `inject:""`
	Cfg     *config.Configuration `inject:""`
}

func ProvideObjects() []*inject.Object {
	return []*inject.Object{
		{Value: &Processor{}},
		{Value: pachinko.NewHandler()},
	}
}

func (p *Processor) Start() {
	if utils.ShouldStartProcessingSQSMessages(p.Cfg.PachinkoQueueURL) {
		pachinkoEventProcessorConfig := psqs.SQSPollerParams{
			SQSRegion:                p.Cfg.SQSRegion,
			VisibilityTimeoutSeconds: 60,
		}
		pachinkoEventPoller := psqs.NewProcessor(pachinkoEventProcessorConfig, p.Cfg.PachinkoQueueURL, p.Handler.HandleMessage)
		graceful.Go(pachinkoEventPoller.PollSQS)
	}
}
