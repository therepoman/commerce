package poa

import (
	"code.justin.tv/chat/golibs/graceful"
	"code.justin.tv/commerce/payday/backend/sqs/handlers/utils"
	"code.justin.tv/commerce/payday/config"
	onboard2 "code.justin.tv/commerce/payday/onboard"
	psqs "code.justin.tv/commerce/payday/sqs"
	"code.justin.tv/commerce/payday/sqs/handlers/poa"
	"code.justin.tv/commerce/payday/sqs/handlers/poa/onboard"
	"github.com/facebookgo/inject"
)

type Processor struct {
	Handler *poa.Handler          `inject:""`
	Cfg     *config.Configuration `inject:""`
	SQSCfg  *psqs.SQSPollerParams `inject:""`
}

func ProvideObjects() []*inject.Object {
	return []*inject.Object{
		{Value: &Processor{}},
		{Value: &poa.Handler{}},
		{Value: &onboard.Onboarder{}},
		{Value: &onboard2.OnboardManager{}},
	}
}

func (p *Processor) Start() {
	if utils.ShouldStartProcessingSQSMessages(p.Cfg.PartnerOnboardingSQSQueueURL) {
		poaQueuePoller := psqs.NewProcessor(*p.SQSCfg, p.Cfg.PartnerOnboardingSQSQueueURL, p.Handler.HandleMessage)
		graceful.Go(poaQueuePoller.PollSQS)
	}
}
