package userstate

import (
	"code.justin.tv/chat/golibs/graceful"
	"code.justin.tv/commerce/payday/backend/sqs/handlers/utils"
	"code.justin.tv/commerce/payday/config"
	psqs "code.justin.tv/commerce/payday/sqs"
	"code.justin.tv/commerce/payday/sqs/handlers/userstate"
	"github.com/facebookgo/inject"
)

type Processor struct {
	Handler *userstate.Handler       `inject:""`
	Cfg     *config.Configuration    `inject:""`
	Adapter *psqs.FABSMessageAdapter `inject:""`
	SQSCfg  *psqs.SQSPollerParams    `inject:""`
}

func ProvideObjects() []*inject.Object {
	return []*inject.Object{
		{Value: &Processor{}},
		{Value: &userstate.Handler{}},
	}
}

func (p *Processor) Start() {
	if utils.ShouldStartProcessingSQSMessages(p.Cfg.UserStateQueueURL) {
		newUserStatePoller := psqs.NewProcessor(*p.SQSCfg, p.Cfg.UserStateQueueURL, p.Adapter.Adapt(p.Handler))
		graceful.Go(newUserStatePoller.PollSQS)
	}
}
