package extensionpubsub

import (
	"code.justin.tv/chat/golibs/graceful"
	"code.justin.tv/commerce/payday/backend/sqs/handlers/utils"
	"code.justin.tv/commerce/payday/config"
	psqs "code.justin.tv/commerce/payday/sqs"
	"code.justin.tv/commerce/payday/sqs/handlers/extensionspubsub"
	"github.com/facebookgo/inject"
)

type Processor struct {
	Handler *extensionspubsub.ExtensionsPubsubHandler `inject:""`
	Cfg     *config.Configuration                     `inject:""`
	Adapter *psqs.FABSMessageAdapter                  `inject:""`
	SQSCfg  *psqs.SQSPollerParams                     `inject:""`
}

func ProvideObjects() []*inject.Object {
	return []*inject.Object{
		{Value: &extensionspubsub.ExtensionsPubsubHandler{}},
		{Value: &Processor{}},
	}
}

func (p *Processor) Start() {
	if utils.ShouldStartProcessingSQSMessages(p.Cfg.ExtensionsPubSubSQSQueueURL) {
		extensionsPubSubProcessor := psqs.NewProcessor(*p.SQSCfg, p.Cfg.ExtensionsPubSubSQSQueueURL, p.Adapter.Adapt(p.Handler))
		graceful.Go(extensionsPubSubProcessor.PollSQS)
	}
}
