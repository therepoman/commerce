package badgetieremoticonbackfills

import (
	"code.justin.tv/chat/golibs/graceful"
	"code.justin.tv/commerce/payday/backend/sqs/handlers/utils"
	"code.justin.tv/commerce/payday/config"
	psqs "code.justin.tv/commerce/payday/sqs"
	"code.justin.tv/commerce/payday/sqs/handlers/badgetieremotebackfills"
	"github.com/facebookgo/inject"
)

type Processor struct {
	Handler *badgetieremotebackfills.Handler `inject:""`
	Cfg     *config.Configuration            `inject:""`
	SQSCfg  *psqs.SQSPollerParams            `inject:""`
}

func ProvideObjects() []*inject.Object {
	return []*inject.Object{
		{Value: &Processor{}},
		{Value: &badgetieremotebackfills.Handler{}},
	}
}

func (p *Processor) Start() {
	if utils.ShouldStartProcessingSQSMessages(p.Cfg.EmoticonBackfillQueueURL) {
		p.SQSCfg.VisibilityTimeoutSeconds = p.Cfg.EmoticonBackfillVisibilityTimeoutSeconds
		emoticonBackfillProcessor := psqs.NewProcessor(*p.SQSCfg, p.Cfg.EmoticonBackfillQueueURL, p.Handler.HandleMessage)
		graceful.Go(emoticonBackfillProcessor.PollSQS)
	}
}
