package adminjob

import (
	"code.justin.tv/chat/golibs/graceful"
	"code.justin.tv/commerce/payday/backend/sqs/handlers/utils"
	"code.justin.tv/commerce/payday/config"
	psqs "code.justin.tv/commerce/payday/sqs"
	adminjobSQS "code.justin.tv/commerce/payday/sqs/handlers/adminjob"
	"github.com/facebookgo/inject"
)

type Processor struct {
	Handler *adminjobSQS.AdminJobHandler `inject:""`
	Cfg     *config.Configuration        `inject:""`
}

func ProvideObjects() []*inject.Object {
	return []*inject.Object{
		{Value: &adminjobSQS.AdminJobHandler{}},
		{Value: &Processor{}},
	}
}

func (p *Processor) Start() {
	if utils.ShouldStartProcessingSQSMessages(p.Cfg.AdminJobSQSQueueURL) {
		adminJobSQSProcessorConfig := psqs.SQSPollerParams{
			SQSRegion:                p.Cfg.SQSRegion,
			VisibilityTimeoutSeconds: 14400, // 4 Hours
		}
		adminJobPoller := psqs.NewProcessor(adminJobSQSProcessorConfig, p.Cfg.AdminJobSQSQueueURL, p.Handler.HandleMessage)
		graceful.Go(adminJobPoller.PollSQS)
	}
}
