package utils

import (
	"os"
	"strings"

	string_utils "code.justin.tv/commerce/gogogadget/strings"
	log "code.justin.tv/commerce/logrus"
)

const (
	developerBoxHostnameSuffix = ".ant.amazon.com"
)

func ShouldStartProcessingSQSMessages(sqsURL string) bool {
	if string_utils.Blank(sqsURL) {
		return false
	}

	hostname, err := os.Hostname()
	if err != nil {
		log.WithError(err).Error("error getting hostname from OS")
		hostname = ""
	}

	if strings.HasSuffix(hostname, developerBoxHostnameSuffix) {
		log.Infof("Detected a dev box. Not polling on: %s", sqsURL)
		return false
	}

	return true
}
