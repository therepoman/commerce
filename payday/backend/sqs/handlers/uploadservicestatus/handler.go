package uploadservicestatus

import (
	"code.justin.tv/chat/golibs/graceful"
	"code.justin.tv/commerce/payday/backend/sqs/handlers/utils"
	"code.justin.tv/commerce/payday/config"
	psqs "code.justin.tv/commerce/payday/sqs"
	uploadserviceSQS "code.justin.tv/commerce/payday/sqs/handlers/uploadservicestatus"
	"github.com/facebookgo/inject"
)

type Processor struct {
	Handler *uploadserviceSQS.UploadServiceStatusHandler `inject:""`
	Cfg     *config.Configuration                        `inject:""`
}

func ProvideObjects() []*inject.Object {
	return []*inject.Object{
		{Value: &Processor{}},
		{Value: &uploadserviceSQS.UploadServiceStatusHandler{}},
	}
}

func (p *Processor) Start() {
	if utils.ShouldStartProcessingSQSMessages(p.Cfg.UploadCallBackSQSQueueURL) {
		uploadServiceStatusSQSProcessorConfig := psqs.SQSPollerParams{
			SQSRegion:                p.Cfg.SQSRegion,
			VisibilityTimeoutSeconds: 60 * 5,
		}
		uploadServiceStatusProcessor := psqs.NewProcessor(uploadServiceStatusSQSProcessorConfig, p.Cfg.UploadCallBackSQSQueueURL, p.Handler.HandleMessage)
		graceful.Go(uploadServiceStatusProcessor.PollSQS)
	}
}
