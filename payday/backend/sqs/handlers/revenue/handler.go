package revenue

import (
	"code.justin.tv/chat/golibs/graceful"
	"code.justin.tv/commerce/payday/backend/sqs/handlers/utils"
	"code.justin.tv/commerce/payday/config"
	psqs "code.justin.tv/commerce/payday/sqs"
	"code.justin.tv/commerce/payday/sqs/handlers/revenue"
	"github.com/facebookgo/inject"
)

type Processor struct {
	Handler *revenue.Handler         `inject:""`
	Cfg     *config.Configuration    `inject:""`
	Adapter *psqs.FABSMessageAdapter `inject:""`
	SQSCfg  *psqs.SQSPollerParams    `inject:""`
}

func ProvideObjects() []*inject.Object {
	return []*inject.Object{
		{Value: &revenue.Handler{}},
		{Value: &Processor{}},
	}
}

func (p *Processor) Start() {
	if utils.ShouldStartProcessingSQSMessages(p.Cfg.RevenueSQSQueueURL) {
		processor := psqs.NewProcessor(*p.SQSCfg, p.Cfg.RevenueSQSQueueURL, p.Adapter.Adapt(p.Handler))
		graceful.Go(processor.PollSQS)
	}
}
