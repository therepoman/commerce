package eventbus

import (
	"code.justin.tv/chat/golibs/graceful"
	"code.justin.tv/commerce/payday/backend/sqs/handlers/utils"
	"code.justin.tv/commerce/payday/config"
	psqs "code.justin.tv/commerce/payday/sqs"
	eventbusSQS "code.justin.tv/commerce/payday/sqs/handlers/eventbus"
	"github.com/facebookgo/inject"
)

type Processor struct {
	Handler *eventbusSQS.Handler     `inject:""`
	Cfg     *config.Configuration    `inject:""`
	Adapter *psqs.FABSMessageAdapter `inject:""`
	SQSCfg  *psqs.SQSPollerParams    `inject:""`
}

func ProvideObjects() []*inject.Object {
	return []*inject.Object{
		{Value: &Processor{}},
		{Value: &eventbusSQS.Handler{}},
	}
}

func (p *Processor) Start() {
	if utils.ShouldStartProcessingSQSMessages(p.Cfg.EventbusQueueURL) {
		processor := psqs.NewProcessor(*p.SQSCfg, p.Cfg.EventbusQueueURL, p.Adapter.Adapt(p.Handler))
		graceful.Go(processor.PollSQS)
	}
}
