package userdestroy

import (
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/backend/sqs/handlers/utils"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/sqs/handlers/userdestroy"
	eventbus "code.justin.tv/eventbus/client"
	"code.justin.tv/eventbus/client/subscriber/sqsclient"
	"code.justin.tv/eventbus/schema/pkg/user"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/facebookgo/inject"
)

type Processor struct {
	Handler   *userdestroy.Handler  `inject:""`
	Cfg       *config.Configuration `inject:""`
	SQSClient *sqsclient.SQSClient
}

func ProvideObjects() []*inject.Object {
	return []*inject.Object{
		{Value: &userdestroy.Handler{}},
		{Value: &Processor{}},
	}
}

func (p *Processor) Start() {
	if utils.ShouldStartProcessingSQSMessages(p.Cfg.UserDestroySQSQueueURL) {

		mux := eventbus.NewMux()
		user.RegisterDestroyHandler(mux, p.Handler.HandleMessage)

		sess, err := session.NewSession(&aws.Config{
			Region: aws.String(p.Cfg.SQSRegion),
		})
		if err != nil {
			log.WithError(err).Error("Could not start new aws session")
			return
		}

		// starting a new eventbus sqsclient will already start the message ingest workflow,
		// there's no need to explicitly start
		sqsClient, err := sqsclient.New(sqsclient.Config{
			Session:           sess,
			Dispatcher:        mux.Dispatcher(),
			QueueURL:          p.Cfg.UserDestroySQSQueueURL,
			VisibilityTimeout: 40 * time.Second,
		})
		if err != nil {
			log.WithError(err).Error("Error starting eventbus SQS client")
			return
		}

		p.SQSClient = sqsClient
	}
}
