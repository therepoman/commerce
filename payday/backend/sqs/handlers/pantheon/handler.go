package pantheon

import (
	"code.justin.tv/chat/golibs/graceful"
	"code.justin.tv/commerce/payday/backend/sqs/handlers/utils"
	"code.justin.tv/commerce/payday/config"
	psqs "code.justin.tv/commerce/payday/sqs"
	"code.justin.tv/commerce/payday/sqs/handlers/pantheon"
	"github.com/facebookgo/inject"
)

type Processor struct {
	Handler *pantheon.SQSHandler  `inject:""`
	Cfg     *config.Configuration `inject:""`
}

func ProvideObjects() []*inject.Object {
	return []*inject.Object{
		{Value: &Processor{}},
		{Value: &pantheon.SQSHandler{}},
	}
}

func (p *Processor) Start() {
	if utils.ShouldStartProcessingSQSMessages(p.Cfg.PantheonEventSQSQueueURL) {
		pantheonEventProcessorConfig := psqs.SQSPollerParams{
			SQSRegion:                p.Cfg.SQSRegion,
			VisibilityTimeoutSeconds: 60,
		}
		pantheonEventPoller := psqs.NewProcessor(pantheonEventProcessorConfig, p.Cfg.PantheonEventSQSQueueURL, p.Handler.HandleMessage)
		graceful.Go(pantheonEventPoller.PollSQS)
	}
}
