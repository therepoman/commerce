package leaderboard

import (
	"code.justin.tv/chat/golibs/graceful"
	"code.justin.tv/commerce/payday/backend/sqs/handlers/utils"
	"code.justin.tv/commerce/payday/config"
	psqs "code.justin.tv/commerce/payday/sqs"
	"code.justin.tv/commerce/payday/sqs/handlers/leaderboard"
	"github.com/facebookgo/inject"
)

type Processor struct {
	Handler *leaderboard.Handler     `inject:""`
	Cfg     *config.Configuration    `inject:""`
	Adapter *psqs.FABSMessageAdapter `inject:""`
	SQSCfg  *psqs.SQSPollerParams    `inject:""`
}

func ProvideObjects() []*inject.Object {
	return []*inject.Object{
		{Value: &Processor{}},
		{Value: &leaderboard.Handler{}},
	}
}

func (p *Processor) Start() {
	if utils.ShouldStartProcessingSQSMessages(p.Cfg.ChannelLeaderboardIngestionSQSQueueURL) {
		leaderboardPoller := psqs.NewProcessor(*p.SQSCfg, p.Cfg.ChannelLeaderboardIngestionSQSQueueURL, p.Adapter.Adapt(p.Handler))
		graceful.Go(leaderboardPoller.PollSQS)
	}
}
