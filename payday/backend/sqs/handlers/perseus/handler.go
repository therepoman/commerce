package perseus

import (
	"code.justin.tv/chat/golibs/graceful"
	"code.justin.tv/commerce/payday/backend/sqs/handlers/utils"
	"code.justin.tv/commerce/payday/config"
	psqs "code.justin.tv/commerce/payday/sqs"
	"code.justin.tv/commerce/payday/sqs/handlers/perseus"
	"code.justin.tv/commerce/payday/sqs/handlers/perseus/format"
	"github.com/facebookgo/inject"
)

type Processor struct {
	Handler *perseus.Handler      `inject:""`
	Cfg     *config.Configuration `inject:""`
}

func ProvideObjects() []*inject.Object {
	return []*inject.Object{
		{Value: &Processor{}},
		{Value: perseus.NewHandler()},
		{Value: perseus.NewDeduplicator()},
		{Value: perseus.NewFormatter()},
		{Value: format.NewGiveBitsToBroadcasterFormatter(), Name: "giveBitsToBroadcasterFormatter"},
		{Value: format.NewAddBitsToCustomerAccountFormatter(), Name: "addBitsToCustomerAccountFormatter"},
		{Value: format.NewRemoveBitsFromCustomerAccountFormatter(), Name: "removeBitsFromCustomerAccountFormatter"},
		{Value: format.NewUseBitsOnExtensionFormatter(), Name: "useBitsOnExtensionFormatter"},
		{Value: format.NewUseBitsOnPollFormatter(), Name: "useBitsOnPollFormatter"},
		{Value: format.NewCreateHoldFormatter(), Name: "createHoldFormatter"},
		{Value: format.NewFinalizeHoldFormatter(), Name: "finalizeHoldFormatter"},
		{Value: format.NewReleaseHoldFormatter(), Name: "releaseHoldFormatter"},
		{Value: perseus.NewSNS()},
	}
}

func (p *Processor) Start() {
	if utils.ShouldStartProcessingSQSMessages(p.Cfg.PerseusQueueURL) {
		perseusEventProcessorConfig := psqs.SQSPollerParams{
			SQSRegion:                p.Cfg.SQSRegion,
			VisibilityTimeoutSeconds: 60,
		}
		perseusEventPoller := psqs.NewProcessor(perseusEventProcessorConfig, p.Cfg.PerseusQueueURL, p.Handler.HandleMessage)
		graceful.Go(perseusEventPoller.PollSQS)
	}
}
