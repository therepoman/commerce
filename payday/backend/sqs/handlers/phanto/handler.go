package phanto

import (
	"code.justin.tv/chat/golibs/graceful"
	"code.justin.tv/commerce/payday/backend/sqs/handlers/utils"
	"code.justin.tv/commerce/payday/config"
	psqs "code.justin.tv/commerce/payday/sqs"
	"code.justin.tv/commerce/payday/sqs/handlers/phanto"
	"github.com/facebookgo/inject"
)

type Processor struct {
	Handler *phanto.PhantoHandler `inject:""`
	Cfg     *config.Configuration `inject:""`
}

func ProvideObjects() []*inject.Object {
	return []*inject.Object{
		{Value: &phanto.PhantoHandler{}},
		{Value: &Processor{}},
	}
}

func (p *Processor) Start() {
	if utils.ShouldStartProcessingSQSMessages(p.Cfg.AutoModSQSQueueURL) {
		phantoProcessorConfig := psqs.SQSPollerParams{
			SQSRegion:                p.Cfg.SQSRegion,
			VisibilityTimeoutSeconds: 60 * 5,
		}
		processor := psqs.NewProcessor(phantoProcessorConfig, p.Cfg.PhantoSQSQueueURL, p.Handler.Handle)
		graceful.Go(processor.PollSQS)
	}
}
