package autorefill

import (
	"code.justin.tv/chat/golibs/graceful"
	"code.justin.tv/commerce/payday/backend/sqs/handlers/utils"
	"code.justin.tv/commerce/payday/config"
	psqs "code.justin.tv/commerce/payday/sqs"
	"code.justin.tv/commerce/payday/sqs/handlers/autorefill"
	"github.com/facebookgo/inject"
)

type Processor struct {
	Handler *autorefill.AutoResponseHandler `inject:""`
	Cfg     *config.Configuration           `inject:""`
	Adapter *psqs.FABSMessageAdapter        `inject:""`
	SQSCfg  *psqs.SQSPollerParams           `inject:""`
}

type EverdeenResponseProcessor struct {
	Handler *autorefill.EverdeenAutoRefillResponseHandler `inject:""`
	Cfg     *config.Configuration                         `inject:""`
	Adapter *psqs.FABSMessageAdapter                      `inject:""`
	SQSCfg  *psqs.SQSPollerParams                         `inject:""`
}

func ProvideObjects() []*inject.Object {
	return []*inject.Object{
		{Value: &Processor{}},
		{Value: &autorefill.AutoResponseHandler{}},
		{Value: &autorefill.EverdeenAutoRefillResponseHandler{}},
		{Value: &EverdeenResponseProcessor{}},
	}
}

func (p *Processor) Start() {
	if utils.ShouldStartProcessingSQSMessages(p.Cfg.AutoRefillQueueURL) {
		pubSubProcessor := psqs.NewProcessor(*p.SQSCfg, p.Cfg.AutoRefillQueueURL, p.Adapter.Adapt(p.Handler))
		graceful.Go(pubSubProcessor.PollSQS)
	}
}

func (p *EverdeenResponseProcessor) Start() {
	if utils.ShouldStartProcessingSQSMessages(p.Cfg.EverdeenResponseQueue) {
		processor := psqs.NewProcessor(*p.SQSCfg, p.Cfg.EverdeenResponseQueue, p.Handler.HandleMessage)
		graceful.Go(processor.PollSQS)
	}
}
