package datascience

import (
	"code.justin.tv/chat/golibs/graceful"
	"code.justin.tv/commerce/payday/backend/sqs/handlers/utils"
	"code.justin.tv/commerce/payday/config"
	psqs "code.justin.tv/commerce/payday/sqs"
	datascienceSQS "code.justin.tv/commerce/payday/sqs/handlers/datascience"
	"github.com/facebookgo/inject"
)

type Processor struct {
	Handler *datascienceSQS.DataScienceHandler `inject:""`
	Cfg     *config.Configuration              `inject:""`
	Adapter *psqs.FABSMessageAdapter           `inject:""`
	SQSCfg  *psqs.SQSPollerParams              `inject:""`
}

func ProvideObjects() []*inject.Object {
	return []*inject.Object{
		{Value: &Processor{}},
		{Value: &datascienceSQS.DataScienceHandler{}},
	}
}

func (p *Processor) Start() {
	if utils.ShouldStartProcessingSQSMessages(p.Cfg.DataScienceSQSQueueURL) {
		processor := psqs.NewProcessor(*p.SQSCfg, p.Cfg.DataScienceSQSQueueURL, p.Adapter.Adapt(p.Handler))
		graceful.Go(processor.PollSQS)
	}
}
