package lambdas

import (
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/config"
	sfn_client "code.justin.tv/commerce/payday/sfn/client"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sfn"
	"github.com/facebookgo/inject"
)

func Provide(cfg config.Configuration) []*inject.Object {
	awsConfig := aws.NewConfig().WithRegion(cfg.SFNRegion)

	sess, err := session.NewSession(&aws.Config{
		Region: aws.String(cfg.SFNRegion),
	})
	if err != nil {
		log.WithError(err).Panic("Could not start new aws session")
	}

	return []*inject.Object{
		{Value: sfn.New(sess, awsConfig)},
		{Value: sfn_client.NewSFNClient()},
	}
}
