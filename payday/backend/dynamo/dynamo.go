package dynamo

import (
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/audit"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/dynamo/actions"
	"code.justin.tv/commerce/payday/dynamo/auto_refill_settings"
	"code.justin.tv/commerce/payday/dynamo/badge_tier_emote_groupids"
	"code.justin.tv/commerce/payday/dynamo/badge_tier_notification"
	"code.justin.tv/commerce/payday/dynamo/bits_entitlement"
	"code.justin.tv/commerce/payday/dynamo/cheer_images"
	"code.justin.tv/commerce/payday/dynamo/extension_transaction"
	"code.justin.tv/commerce/payday/dynamo/holds"
	"code.justin.tv/commerce/payday/dynamo/leaderboard_badge_holders"
	"code.justin.tv/commerce/payday/dynamo/purchase_by_most_recent_super_platform"
	"code.justin.tv/commerce/payday/dynamo/single_purchase_consumption"
	"code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/campaigns"
	"code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/channel_statuses"
	"code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/eligible_channels"
	"code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/user_campaign_statuses"
	"code.justin.tv/commerce/payday/iam"
	"code.justin.tv/commerce/payday/lambda"
	"code.justin.tv/commerce/payday/s3"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/facebookgo/inject"
)

func Provide(cfg config.Configuration) []*inject.Object {
	s3Client := s3.NewFromDefaultConfig()
	lambdaClient := lambda.NewFromDefaultConfig()
	iamClient := iam.NewFromDefaultConfig()
	auditSetup := audit.NewAuditSetup(s3Client, lambdaClient, iamClient)

	dynamoClient := dynamo.NewClientWithAudit(&dynamo.DynamoClientConfig{
		AwsRegion:   cfg.DynamoRegion,
		TablePrefix: cfg.DynamoTablePrefix,
	}, auditSetup, &dynamo.DynamoAuditConfig{
		RecordsS3Bucket:      cfg.AuditRecordsS3Bucket,
		LambdaMemorySizeMB:   cfg.AuditLambdaMemorySizeMB,
		LambdaTimeoutSec:     cfg.AuditLambdaTimeoutSec,
		LambdaRoleName:       cfg.AuditLambdaRoleName,
		LambdaCodeS3Bucket:   cfg.AuditLambdaCodeS3Bucket,
		LambdaEventBatchSize: cfg.AuditLambdaEventBatchSize,
	})

	dynamoClientNoAudit := dynamo.NewClient(&dynamo.DynamoClientConfig{
		AwsRegion:   cfg.DynamoRegion,
		TablePrefix: cfg.DynamoTablePrefix,
	})

	sess, err := session.NewSession(&aws.Config{
		Region: aws.String(cfg.DynamoRegion),
	})

	if err != nil {
		log.WithError(err).Panic("Could not start new aws session")
	}

	return []*inject.Object{
		{Value: dynamo.NewChannelDao(dynamoClient)},
		{Value: dynamo.NewUserDao(dynamoClient)},
		{Value: dynamo.NewBadgeTierDao(dynamoClient)},
		{Value: dynamo.NewTransactionsDao(dynamoClient)},
		{Value: dynamo.NewInFlightTransactionDao(sess, cfg)},
		{Value: dynamo.NewImageDao(dynamoClient)},
		{Value: dynamo.NewTaxInfoDao(dynamoClient)},
		{Value: dynamo.NewAdminJobDao(dynamoClient)},
		{Value: dynamo.NewAdminJobTaskDao(dynamoClient)},
		{Value: dynamo.NewRevenueDao(dynamoClientNoAudit)},
		{Value: leaderboard_badge_holders.NewLeaderboardBadgesDao(dynamoClient)},
		{Value: extension_transaction.NewExtensionsTransactionsDAO(dynamoClient)},
		{Value: actions.NewActionDao(dynamoClient)},
		{Value: campaigns.NewSponsoredCheermoteCampaignDao(dynamoClient)},
		{Value: channel_statuses.NewSponsoredCheermoteChannelStatusesDao(dynamoClient)},
		{Value: user_campaign_statuses.NewSponsoredCheermoteUserStatusesDao(dynamoClient)},
		{Value: eligible_channels.NewSponsoredCheermoteEligibleChannelsDao(dynamoClient)},
		{Value: badge_tier_emote_groupids.NewBadgeTierEmoteGroupIDDAO(sess, cfg)},
		{Value: badge_tier_notification.NewBadgeTierNotificationDao(sess, cfg)},
		{Value: bits_entitlement.NewTransactionBasedBitsEntitlementDAO(sess, cfg)},
		{Value: single_purchase_consumption.NewSinglePurchaseConsumptionDAO(sess, cfg)},
		{Value: purchase_by_most_recent_super_platform.NewPurchaseByMostRecentSuperPlatformDAO(sess, cfg)},
		{Value: holds.NewTransactionBasedDAO(sess, cfg)},
		{Value: auto_refill_settings.NewAutoRefillDao(sess, cfg)},
		{Value: auto_refill_settings.NewAutoRefillRecordDao(sess, cfg)},
		{Value: cheer_images.NewChannelImagesDao(sess, cfg)},
	}
}
