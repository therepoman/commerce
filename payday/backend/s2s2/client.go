package s2s2

import (
	"os"

	logging "code.justin.tv/amzn/TwitchLogging"
	identifier "code.justin.tv/amzn/TwitchProcessIdentifier"
	"code.justin.tv/amzn/TwitchS2S2/c7s"
	"code.justin.tv/amzn/TwitchS2S2/s2s2"
	twitchtelemetry "code.justin.tv/amzn/TwitchTelemetry"
	twitchtelemetrymiddleware "code.justin.tv/amzn/TwitchTelemetryMetricsMiddleware"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/safety/telemetry"
	"code.justin.tv/video/metrics-middleware/v2/operation"
)

const (
	substageCanary = "canary"
)

func NewS2S2Client(cfg config.Configuration, logger logging.Logger) (*s2s2.S2S2, error) {
	s2s2Client, err := s2s2.New(&s2s2.Options{
		Config: &c7s.Config{
			ClientServiceName:   cfg.S2S2.Name,
			ServiceOrigins:      cfg.S2S2.ServiceOrigin,
			EnableAccessLogging: true,
		},
		OperationStarter: &operation.Starter{
			OpMonitors: []operation.OpMonitor{
				&twitchtelemetrymiddleware.OperationMonitor{
					SampleReporter: getSampleReporter(cfg),
				},
			},
		},
		Logger: logger,
	})
	if err != nil {
		return nil, err
	}

	return s2s2Client, nil
}

func getSampleReporter(cfg config.Configuration) twitchtelemetry.SampleReporter {
	hostname := "unknown"
	if envHostname, err := os.Hostname(); err == nil {
		hostname = envHostname
	}

	var substage string
	if isCanary() {
		substage = substageCanary
	} else {
		substage = cfg.Environment
	}

	telemetryReporter := telemetry.ConfigureTelemetry(&identifier.ProcessIdentifier{
		Service:  "payday",
		Region:   cfg.CloudWatchRegion,
		Stage:    cfg.Environment,
		Machine:  hostname,
		Substage: substage,
		LaunchID: "",
	}, nil)

	return telemetryReporter
}

func isCanary() bool {
	// terracode stores canary info as env var
	// https://git.xarth.tv/subs/terracode/blob/master/twitch-service/canary.tf#L2-L7
	canaryEnvVar := os.Getenv("CANARY")
	return canaryEnvVar == "true"
}
