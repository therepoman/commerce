package middleware

import (
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/middleware"
	"github.com/facebookgo/inject"
)

type Middleware struct {
	Metrics           *middleware.MetricsMiddleware             `inject:""`
	ClientID          *middleware.ClientIdMiddleware            `inject:""`
	ProtoForward      *middleware.ForwardedProtoMiddleware      `inject:""`
	RequestValidation *middleware.RequestValidationMiddleware   `inject:""`
	Cartman           middleware.CartmanMiddleware              `inject:""`
	AuditLogging      *middleware.AuditLoggingContextMiddleware `inject:""`
}

func Provide(cfg config.Configuration) []*inject.Object {
	return []*inject.Object{
		{Value: &Middleware{}},
		{Value: middleware.NewMetricsMiddleware()},
		{Value: &middleware.ClientIdMiddleware{}},
		{Value: middleware.NewForwardedProtoMiddleware(cfg.ForwardedProtocolEnforcement)},
		{Value: middleware.NewRequestValidationMiddleware(cfg.JsonContentTypeEnforcement, cfg.OriginEnforcement)},
		{Value: middleware.NewCartmanMiddlware()},
		{Value: middleware.NewAuditLoggingContextMiddleware()},
	}
}
