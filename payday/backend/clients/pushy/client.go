package pushy

import (
	"code.justin.tv/chat/pushy/client/events"
	"code.justin.tv/commerce/payday/config"
	"github.com/aws/aws-sdk-go/service/sns"
	"github.com/cactus/go-statsd-client/statsd"
)

func NewClient(cfg config.Configuration, statsClient statsd.Statter, snsClient *sns.SNS) events.Publisher {
	pushyClient, _ := events.NewPublisher(events.Config{
		DispatchSNSARN: cfg.PushyServiceSNSTopic,
		Stats:          statsClient,
		SNSClient:      snsClient,
	})

	return pushyClient
}
