package eventbus

import (
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/config"
	eventbus "code.justin.tv/eventbus/client"
	"code.justin.tv/eventbus/client/mock"
	"code.justin.tv/eventbus/client/publisher"
	"code.justin.tv/eventbus/schema/pkg/bits_use"
	"code.justin.tv/eventbus/schema/pkg/cheer"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
)

// NewClient returns a new Eventbus client
func NewClient(cfg config.Configuration) eventbus.Publisher {
	env := cfg.Environment

	if publisher.Environment(env) == publisher.EnvDevelopment {
		log.Info("running in development, using mock eventbus publisher client")
		return &mock.Publisher{}
	}
	awsSession, err := session.NewSession(&aws.Config{
		Region: aws.String(cfg.DynamoRegion),
	})
	if err != nil {
		log.WithError(err).Panic("Could not start new aws session")
	}
	publisherClientCfg := publisher.Config{
		Environment: publisher.Environment(env),
		Session:     awsSession,
		EventTypes:  []string{cheer.CreateEventType, bits_use.CreateEventType},
	}

	for i := 0; i < 3; i++ {
		var c eventbus.Publisher
		c, err = publisher.New(publisherClientCfg)
		if err == nil {
			return c
		}
		time.Sleep(time.Duration(1<<uint64(i)) * time.Second)
	}
	log.WithError(err).Fatal("Could not initialize eventbus client, aborting")
	return nil
}
