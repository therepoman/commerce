package clients

import (
	"net/http"

	receiver "code.justin.tv/amzn/TwitchDartReceiverTwirp"
	falador_client "code.justin.tv/amzn/TwitchExtCatalogServiceTwirp"
	"code.justin.tv/amzn/TwitchS2S2/s2s2"
	clueservice "code.justin.tv/chat/tmi/client"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/backend/clients/badges"
	"code.justin.tv/commerce/payday/backend/clients/connections"
	"code.justin.tv/commerce/payday/backend/clients/datascience"
	"code.justin.tv/commerce/payday/backend/clients/eventbus"
	"code.justin.tv/commerce/payday/backend/clients/extensions"
	"code.justin.tv/commerce/payday/backend/clients/flo"
	"code.justin.tv/commerce/payday/backend/clients/moneypenny"
	"code.justin.tv/commerce/payday/backend/clients/notification"
	"code.justin.tv/commerce/payday/backend/clients/owl"
	"code.justin.tv/commerce/payday/backend/clients/pachter"
	"code.justin.tv/commerce/payday/backend/clients/pantheon"
	"code.justin.tv/commerce/payday/backend/clients/pdms"
	"code.justin.tv/commerce/payday/backend/clients/pubsub"
	"code.justin.tv/commerce/payday/backend/clients/pushy"
	"code.justin.tv/commerce/payday/backend/clients/ripley"
	"code.justin.tv/commerce/payday/backend/clients/sns"
	"code.justin.tv/commerce/payday/backend/clients/upload"
	"code.justin.tv/commerce/payday/backend/clients/user"
	"code.justin.tv/commerce/payday/backend/clients/zuma"
	"code.justin.tv/commerce/payday/clients/cloudwatchlogger"
	"code.justin.tv/commerce/payday/clients/ems"
	"code.justin.tv/commerce/payday/clients/mako"
	"code.justin.tv/commerce/payday/clients/miniexperiments"
	"code.justin.tv/commerce/payday/clients/nitro"
	"code.justin.tv/commerce/payday/clients/pachinko"
	"code.justin.tv/commerce/payday/clients/prefixes"
	pubsub_utils "code.justin.tv/commerce/payday/clients/pubsub"
	"code.justin.tv/commerce/payday/clients/tmi"
	user2 "code.justin.tv/commerce/payday/clients/user"
	"code.justin.tv/commerce/payday/cloudfront"
	"code.justin.tv/commerce/payday/config"
	partner_manager "code.justin.tv/commerce/payday/partner"
	"code.justin.tv/commerce/payday/s3"
	paydaySNS "code.justin.tv/commerce/payday/sns"
	petozi "code.justin.tv/commerce/petozi/rpc"
	"code.justin.tv/foundation/twitchclient"
	everdeentwirp "code.justin.tv/revenue/everdeen/rpc"
	mulan "code.justin.tv/revenue/mulan/rpc"
	substwirp "code.justin.tv/revenue/subscriptions/twirp"
	clips "code.justin.tv/video/clips-upload/client"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/facebookgo/inject"
)

const (
	DefaultNumberOfConnections   = 200
	defaultClipsInternalEndpoint = "https://production.clips.twitch.a2z.com"
)

type Clients struct {
	UserServiceClient user2.IUserServiceClientWrapper   `inject:""`
	AuditLogger       cloudwatchlogger.CloudWatchLogger `inject:""`
}

func NewClients(cfg config.Configuration, s2s2Client *s2s2.S2S2) ([]*inject.Object, error) {
	statsClient, err := statsd.NewNoopClient()
	if err != nil {
		log.WithError(err).Error("Could not create noop statter")
		return nil, err
	}

	clientHttpTransport := twitchclient.TransportConf{
		MaxIdleConnsPerHost: DefaultNumberOfConnections,
	}

	clipClient, err := clips.NewClient(twitchclient.ClientConf{
		Host: defaultClipsInternalEndpoint,
	})
	if err != nil {
		return nil, err
	}

	moneyPennyClient := moneypenny.NewClient(cfg, clientHttpTransport, statsClient)
	snsClient := sns.NewClient(cfg)

	pubsubControlClient, err := pubsub.NewPubsubControlClient(cfg, clientHttpTransport, statsClient)
	if err != nil {
		return nil, err
	}

	clueServiceClient, err := clueservice.NewClient(twitchclient.ClientConf{
		Host:      cfg.TMIHostAddress,
		Transport: clientHttpTransport,
		Stats:     statsClient,
	})
	if err != nil {
		return nil, err
	}

	auditLogger, err := cloudwatchlogger.NewCloudWatchLogClient(cfg.CloudWatchRegion, cfg.AuditLogGroupName)
	if err != nil {
		return nil, err
	}

	return []*inject.Object{
		// Twitch Internal Clients
		{Value: badges.NewClient(cfg)},
		{Value: clipClient},
		{Value: connections.NewClient(cfg, statsClient)},
		{Value: datascience.NewClient(cfg)},
		{Value: extensions.NewClient(cfg)},
		{Value: falador_client.NewTwitchExtCatalogServiceProtobufClient(cfg.FaladorURL, http.DefaultClient)},
		{Value: moneyPennyClient},
		{Value: mulan.NewMulanProtobufClient(cfg.MulanEndpoint, http.DefaultClient)},
		{Value: notification.NewClient(cfg)},
		{Value: owl.NewClient(cfg, clientHttpTransport, statsClient)},
		{Value: pantheon.NewClient(cfg, statsClient)},
		{Value: &partner_manager.PartnerManager{RipleyClient: ripley.NewClient(cfg)}},
		{Value: paydaySNS.NewSnsClient("us-west-2"), Name: "usWestSNSClient"},
		{Value: paydaySNS.NewSnsClient("us-east-1"), Name: "usEastSNSClient"},
		{Value: &prefixes.PrefixesClient{}},
		{Value: pubsub_utils.NewPubSubUtils(pubsubControlClient)},
		{Value: pubsub.NewClient(cfg, statsClient)},
		{Value: pushy.NewClient(cfg, statsClient, snsClient)},
		{Value: snsClient},
		{Value: substwirp.NewSubscriptionsProtobufClient(cfg.SubsServiceEndpoint, http.DefaultClient)},
		{Value: tmi.NewHttpTMIClient(cfg.TMIHostAddress)},
		{Value: upload.NewClient(cfg, statsClient)},
		{Value: user.NewClient(cfg, clientHttpTransport, statsClient)},
		{Value: zuma.NewClient(cfg, clientHttpTransport, statsClient)},
		{Value: flo.NewClient(cfg)},
		{Value: ems.NewEMSClient(cfg.EMSEndpoint)},
		{Value: petozi.NewPetoziProtobufClient(cfg.PetoziServiceEndpointURL, http.DefaultClient)},
		{Value: pachinko.NewClient(cfg, s2s2Client)},
		{Value: eventbus.NewClient(cfg)},
		{Value: nitro.NewClient(cfg)},
		{Value: clueServiceClient},
		{Value: mako.NewClient(cfg)},
		{Value: everdeentwirp.NewEverdeenProtobufClient(cfg.EverdeenServiceEndpoint, http.DefaultClient)},
		{Value: receiver.NewReceiverProtobufClient(cfg.DartReceiverEndpoint, http.DefaultClient)},
		{Value: pachter.NewClient(cfg)},
		{Value: pdms.NewClient(cfg)},
		{Value: auditLogger},
		{Value: miniexperiments.NewMiniExperimentClient()},

		// AWS Clients
		{Value: s3.NewFromDefaultConfig()},
		{Value: cloudfront.NewFromDefaultConfig(cfg.BitsAssetsCloudFrontDistributionId)},

		{Value: &Clients{}},
	}, nil
}
