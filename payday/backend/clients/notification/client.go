package notification

import (
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/notification"
)

func NewClient(cfg config.Configuration) notification.INotification {
	notificationClient, _ := notification.NewNotification(cfg.Environment)

	return notificationClient
}
