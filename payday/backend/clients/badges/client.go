package badges

import (
	"code.justin.tv/commerce/payday/clients/chatbadges"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/foundation/twitchclient"
)

func NewClient(cfg config.Configuration) chatbadges.IChatBadgesClient {
	chatBadgeCfg := twitchclient.ClientConf{
		Host: cfg.ChatBadgesHostAddress,
	}

	chatBadgesClient, _ := chatbadges.NewChatBadgesClient(chatBadgeCfg)

	return chatBadgesClient
}
