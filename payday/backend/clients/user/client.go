package user

import (
	users_wrapper "code.justin.tv/commerce/payday/clients/user"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/foundation/twitchclient"
	users "code.justin.tv/web/users-service/client/usersclient_internal"
	"github.com/cactus/go-statsd-client/statsd"
)

func NewClient(cfg config.Configuration, transport twitchclient.TransportConf, statsClient statsd.Statter) users_wrapper.IUserServiceClientWrapper {
	userServiceConfig := twitchclient.ClientConf{
		Host:      cfg.UserServiceEndpoint,
		Transport: transport,
		Stats:     statsClient,
	}

	userService, _ := users.NewClient(userServiceConfig)
	return users_wrapper.NewUserServiceClientWrapper(userService)
}
