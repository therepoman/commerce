package extensions

import (
	"code.justin.tv/commerce/payday/clients/extensions/validator"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/gds/gds/extensions/validator/client/s2s"
)

func NewClient(cfg config.Configuration) s2s.Client {
	var extensionValidatorS2SClient s2s.Client

	// only use real validator client in non-development environments, in development use a stub validator that will
	// not have the ability to create JWT tokens, but won't require S2S.
	if cfg.Environment != config.DevelopmentEnvironment {
		extensionValidatorS2SClient = validator.New(cfg.ExtensionsValidatorUrl, cfg.S2S.Name)
	} else {
		extensionValidatorS2SClient = validator.NewNoOp()
	}

	return extensionValidatorS2SClient
}
