package connections

import (
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/foundation/twitchclient"
	connections "code.justin.tv/identity/connections/client"
	"github.com/cactus/go-statsd-client/statsd"
)

func NewClient(cfg config.Configuration, statsClient statsd.Statter) connections.Client {
	connectionsClientCfg := twitchclient.ClientConf{
		Host:                     cfg.ConnectionsHost,
		Stats:                    statsClient,
		SuppressRepositoryHeader: true,
	}

	connectionsClient, _ := connections.NewClient(connectionsClientCfg)

	return connectionsClient
}
