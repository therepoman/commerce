package upload

import (
	"fmt"

	"code.justin.tv/commerce/payday/clients/uploadservice"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/foundation/twitchclient"
	"github.com/cactus/go-statsd-client/statsd"
)

func NewClient(cfg config.Configuration, statsClient statsd.Statter) uploadservice.IImageUploader {
	twitchHTTPUploadClient := twitchclient.NewHTTPClient(twitchclient.ClientConf{
		Host:  cfg.UploadServiceURL,
		Stats: statsClient,
	})

	cheermotePath := fmt.Sprintf("%v/partner-actions/", cfg.BitsAssetsS3BucketName)
	cheermoteCloudfrontURL := fmt.Sprintf("%v/partner-actions", cfg.BitsAssetsCloudfrontPrefix)

	uploadServiceClient, _ := uploadservice.NewImageUploader(uploadservice.ImageUploaderConfig{
		UploadServiceURL:       cfg.UploadServiceURL,
		UploadCallbackSNSTopic: cfg.UploadCallBackSNSTopic,
		UploadS3BucketName:     cheermotePath,
		CloudFrontPrefix:       cheermoteCloudfrontURL,
	}, twitchHTTPUploadClient)

	return uploadServiceClient
}
