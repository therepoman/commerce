package pubsub

import (
	pubsub_control "code.justin.tv/chat/pubsub-control/client"
	pubclient "code.justin.tv/chat/pubsub-go-pubclient/client"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/foundation/twitchclient"
	"github.com/cactus/go-statsd-client/statsd"
)

func NewClient(cfg config.Configuration, statsClient statsd.Statter) pubclient.PubClient {
	pubConfig := pubclient.PubConfig{
		Host:           cfg.PubsubHostAddress,
		Stats:          statsClient,
		StatsPrefix:    "payday_service",
		TimingXactName: "payday",
	}
	pubsubClient, _ := pubclient.NewPubClient(pubConfig)

	return pubsubClient
}

func NewPubsubControlClient(cfg config.Configuration, transport twitchclient.TransportConf, statsClient statsd.Statter) (pubsub_control.Client, error) {
	pubSubControlClientCfg := twitchclient.ClientConf{
		Host:      cfg.PubsubControlEndpoint,
		Stats:     statsClient,
		Transport: transport,
	}

	return pubsub_control.NewClient(pubSubControlClientCfg)
}
