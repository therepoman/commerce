package pachter

import (
	"context"
	"net/http"
	"time"

	pachter "code.justin.tv/commerce/pachter/rpc"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/hystrix"
	cmd "code.justin.tv/commerce/payday/hystrix"
	"github.com/cenkalti/backoff"
)

type Client interface {
	GetBitsSpendOrder(ctx context.Context, transactionID string) (map[string]int, error)
}

type client struct {
	PachterClient pachter.Pachter
}

func NewClient(cfg config.Configuration) Client {
	return &client{
		PachterClient: pachter.NewPachterJSONClient(cfg.PachterServiceEndpointURL, http.DefaultClient),
	}
}

func (c *client) GetBitsSpendOrder(ctx context.Context, transactionID string) (map[string]int, error) {
	var resp *pachter.GetBitsSpendOrderResp
	var err error
	var bo backoff.BackOff = backoff.WithContext(newBackoff(), ctx)
	bo = backoff.WithMaxTries(bo, 3)
	backoffFunc := func() error {
		return hystrix.Do(cmd.PachterGetBitsSpendOrderCommand, func() error {
			resp, err = c.PachterClient.GetBitsSpendOrder(ctx, &pachter.GetBitsSpendOrderReq{
				TransactionId: transactionID,
			})
			return err
		}, nil)
	}

	err = backoff.Retry(backoffFunc, bo)
	if err != nil {
		return nil, err
	}

	bitsTypeToAmount := make(map[string]int)
	for bitsType, amount := range resp.BitsTypes {
		bitsTypeToAmount[bitsType] = -int(amount)
	}

	return bitsTypeToAmount, nil
}

func newBackoff() backoff.BackOff {
	exponetial := backoff.NewExponentialBackOff()
	exponetial.InitialInterval = 10 * time.Millisecond
	exponetial.MaxElapsedTime = 1 * time.Second
	exponetial.Multiplier = 2
	return exponetial
}
