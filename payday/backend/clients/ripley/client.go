package ripley

import (
	"context"
	"net/http"

	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/hystrix"
	cmd "code.justin.tv/commerce/payday/hystrix"
	ripley "code.justin.tv/revenue/ripley/rpc"
)

type Client interface {
	GetPayoutType(ctx context.Context, channelID string) (*ripley.GetPayoutTypeResponse, error)
}

type client struct {
	RipleyClient ripley.Ripley
}

func NewClient(cfg config.Configuration) Client {
	return &client{
		RipleyClient: ripley.NewRipleyProtobufClient(cfg.RipleyServiceEndpoint, http.DefaultClient),
	}
}

func (c *client) GetPayoutType(ctx context.Context, channelID string) (*ripley.GetPayoutTypeResponse, error) {
	var resp *ripley.GetPayoutTypeResponse
	var err error
	err = hystrix.Do(cmd.GetUserPayoutType, func() error {
		resp, err = c.RipleyClient.GetPayoutType(ctx, &ripley.GetPayoutTypeRequest{
			ChannelId: channelID,
		})
		return err
	}, nil)

	return resp, err
}
