package pdms

import (
	"net/http"
	"time"

	"code.justin.tv/commerce/payday/clients/pdms"
	"code.justin.tv/commerce/payday/config"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/endpoints"
	"github.com/aws/aws-sdk-go/aws/session"
)

func NewClient(cfg config.Configuration) pdms.IPDMSClientWrapper {
	// dedicating a session for just PDMS client
	// see https://wiki.twitch.com/display/SEC/PDMS-+Delete+pipeline+Service+Onboarding
	sess := session.Must(session.NewSessionWithOptions(session.Options{
		Config: aws.Config{
			Region:              aws.String("us-west-2"),
			STSRegionalEndpoint: endpoints.RegionalSTSEndpoint,
			// We want a long timeout for PDMS, as Lambda based services can take a while to warm up on cold start.
			HTTPClient: &http.Client{Timeout: 10 * time.Second},
		},
	}))
	return pdms.NewPDMSClientWrapper(cfg, sess)
}
