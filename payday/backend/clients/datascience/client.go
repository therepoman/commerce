package datascience

import (
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/datascience"
	"code.justin.tv/commerce/payday/datascience/impl"
	"code.justin.tv/commerce/payday/httputils"
)

func NewClient(cfg config.Configuration) datascience.DataScience {
	dataScienceClient, _ := impl.NewDataScienceApi(datascience.DataScienceApiParams{
		HttpUtilClient:     httputils.NewDefaultHttpUtilClientApi(),
		DataScienceEnabled: cfg.DataScienceEnabled,
		SpadeEnabled:       cfg.DataScienceSpadeEnabled,
		SpadeURL:           cfg.DataScienceSpadeURL,
	})

	return dataScienceClient
}
