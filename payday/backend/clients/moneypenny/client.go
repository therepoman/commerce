package moneypenny

import (
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/foundation/twitchclient"
	moneypenny "code.justin.tv/revenue/moneypenny/client"
	"github.com/cactus/go-statsd-client/statsd"
)

func NewClient(cfg config.Configuration, transport twitchclient.TransportConf, statsClient statsd.Statter) moneypenny.Client {
	moneyPennyCfg := twitchclient.ClientConf{
		Host:      cfg.MoneyPennyEndpoint,
		Transport: transport,
		Stats:     statsClient,
	}
	moneyPennyClient, _ := moneypenny.NewClient(moneyPennyCfg)

	return moneyPennyClient
}
