package flo

import (
	"net/http"

	"code.justin.tv/commerce/payday/config"
	flotwirp "code.justin.tv/revenue/flo/rpc"
)

func NewClient(cfg config.Configuration) flotwirp.Flo {
	return flotwirp.NewFloProtobufClient(cfg.FloServiceEndpoint, http.DefaultClient)
}
