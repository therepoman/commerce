package owl

import (
	owl_wrapper "code.justin.tv/commerce/payday/clients/owl"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/foundation/twitchclient"
	owl "code.justin.tv/web/owl/client"
	"github.com/cactus/go-statsd-client/statsd"
)

func NewClient(cfg config.Configuration, transport twitchclient.TransportConf, statsClient statsd.Statter) owl_wrapper.ClientWrapper {
	owlClientConfig := twitchclient.ClientConf{
		Host:      cfg.OWLEndpoint,
		Transport: transport,
		Stats:     statsClient,
	}
	owlClient, _ := owl.NewClient(owlClientConfig)

	return owl_wrapper.New(owlClient)
}
