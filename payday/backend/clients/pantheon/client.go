package pantheon

import (
	"crypto/tls"
	"net/http"

	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	"code.justin.tv/commerce/payday/clients/pantheon"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/foundation/twitchclient"
	"github.com/cactus/go-statsd-client/statsd"
)

func NewClient(cfg config.Configuration, stats statsd.Statter) pantheon.IPantheonClientWrapper {
	pantheonCfg := twitchclient.ClientConf{
		Host:  cfg.PantheonServiceEndpoint,
		Stats: stats,
		Transport: twitchclient.TransportConf{
			MaxIdleConnsPerHost: 200,
			TLSNextProto:        map[string]func(string, *tls.Conn) http.RoundTripper{}, // Disable HTTP/2.
		},
	}
	pantheonClient := pantheonrpc.NewPantheonProtobufClient(cfg.PantheonServiceEndpoint, twitchclient.NewHTTPClient(pantheonCfg))
	return pantheon.NewPantheonClientWrapper(pantheonClient)
}
