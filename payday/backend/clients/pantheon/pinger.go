package pantheon

import (
	"context"
	"time"

	"code.justin.tv/chat/golibs/graceful"
	"code.justin.tv/commerce/gogogadget/random"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	"code.justin.tv/commerce/payday/clients/pantheon"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/metrics"
)

const (
	minPingDelay = 6 * time.Second
	timeout      = 3 * time.Second

	pantheonErrorMetric   = "Pantheon_Ping_Errors"
	pantheonLatencyMetric = "Pantheon_Ping_Latency"
)

type Pinger interface {
	Init() error
	PingAtInterval()
}

type PingerImpl struct {
	PingDelay      time.Duration
	PantheonClient pantheon.IPantheonClientWrapper `inject:""`
	Statter        metrics.Statter                 `inject:""`
}

func NewPinger() Pinger {
	return &PingerImpl{}
}

func (p *PingerImpl) Init() error {
	cfg := config.Get()

	pingDelay := time.Duration(cfg.PantheonPingDelaySeconds) * time.Second

	if pingDelay < minPingDelay {
		pingDelay = minPingDelay
	}

	p.PingDelay = pingDelay
	return nil
}

func (p *PingerImpl) PingAtInterval() {
	// Wait a random duration before we start pinging, this is so the ping load from our various hosts is more evenly distributed over time
	initialStartDelay := random.Duration(time.Duration(0), 2*p.PingDelay)
	initialStartDelayChan := time.After(initialStartDelay)
	select {
	case <-graceful.ShuttingDown():
		return
	case <-initialStartDelayChan:
	}

	timer := time.NewTicker(p.PingDelay)
	for {
		select {
		case <-graceful.ShuttingDown():
			return
		case <-timer.C:
			p.ping()
		}
	}
}

func (p *PingerImpl) ping() {
	ctx, cancelFunc := context.WithTimeout(context.Background(), timeout)
	defer cancelFunc()

	startTime := time.Now()

	err := p.PantheonClient.HealthCheck(ctx, pantheonrpc.HealthCheckReq{})

	duration := time.Since(startTime)

	if err != nil {
		log.WithError(err).Error("error pinging Pantheon")
		p.Statter.Inc(pantheonErrorMetric, 1)
	}

	p.Statter.TimingDuration(pantheonLatencyMetric, duration)
}
