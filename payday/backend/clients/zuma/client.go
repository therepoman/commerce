package zuma

import (
	zuma_client "code.justin.tv/chat/zuma/client"
	"code.justin.tv/commerce/payday/clients/zuma"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/foundation/twitchclient"
	"github.com/cactus/go-statsd-client/statsd"
)

func NewClient(cfg config.Configuration, transport twitchclient.TransportConf, statsClient statsd.Statter) zuma.IZumaClientWrapper {
	zumaClientConfig := twitchclient.ClientConf{
		Host:      cfg.ZumaHost,
		Transport: transport,
		Stats:     statsClient,
	}

	zumaClient, _ := zuma_client.NewClient(zumaClientConfig)

	return zuma.NewZumaClientWrapper(zumaClient)
}
