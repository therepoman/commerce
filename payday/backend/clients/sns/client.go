package sns

import (
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/config"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sns"
)

func NewClient(cfg config.Configuration) *sns.SNS {
	awsSession, err := session.NewSession(&aws.Config{
		Region: aws.String(cfg.DynamoRegion),
	})
	if err != nil {
		log.WithError(err).Panic("Could not start new aws session")
	}

	return sns.New(awsSession)

}
