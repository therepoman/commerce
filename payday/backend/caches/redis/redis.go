package redis

import (
	"code.justin.tv/commerce/payday/actions"
	"code.justin.tv/commerce/payday/cache"
	"code.justin.tv/commerce/payday/cache/chatbadges"
	"code.justin.tv/commerce/payday/cache/ripley"
	"code.justin.tv/commerce/payday/cache/user"
	"code.justin.tv/commerce/payday/cache/userservice"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/holds"
	recent_platform_cache "code.justin.tv/commerce/payday/products/purchase_by_most_recent_super_platform/cache"
	single_purchase_cache "code.justin.tv/commerce/payday/products/single_purchase_consumption/cache"
	"code.justin.tv/commerce/payday/sponsored_cheermote/campaign"
	"code.justin.tv/commerce/payday/sponsored_cheermote/channel_status"
	"code.justin.tv/commerce/payday/sponsored_cheermote/eligible_channel"
	"code.justin.tv/commerce/payday/sponsored_cheermote/user_campaign_status"
	"github.com/facebookgo/inject"
)

func NewRedisCaches(cfg config.Configuration) []*inject.Object {
	return []*inject.Object{
		{Value: &cache.AutoModCache{}},
		{Value: &cache.BalanceCache{}},
		{Value: &cache.PrefixesCache{}},
		{Value: &cache.ImagesCache{}},
		{Value: campaign.NewCache()},
		{Value: user_campaign_status.NewCache()},
		{Value: channel_status.NewCache()},
		{Value: eligible_channel.NewCache()},
		{Value: actions.NewCache()},
		{Value: &cache.EarnedBadgesCache{}},
		{Value: cache.NewFloPricesCache()},
		{Value: user.NewCache()},
		{Value: userservice.NewCache()},
		{Value: chatbadges.NewCache()},
		{Value: single_purchase_cache.NewCache(), Name: "singlePurchaseConsumptionCache"},
		{Value: recent_platform_cache.NewCache()},
		{Value: holds.NewCache()},
		{Value: ripley.NewCache()},
		{Value: cache.NewIAutoRefillProfileCache()},
		{Value: cache.NewPurger()},
		{Value: cache.NewLast24HoursAcquiredCache()},
		{Value: cache.NewMostRecentlyAcquiredCache()},
		{Value: cache.NewBadgeTierEmotesCache()},
	}
}
