package caches

import (
	"code.justin.tv/chat/rediczar/redefault"
	"code.justin.tv/commerce/gogogadget/strings"
	log "code.justin.tv/commerce/logrus"
	redis_caches "code.justin.tv/commerce/payday/backend/caches/redis"
	"code.justin.tv/commerce/payday/cache/channel"
	payday_redis "code.justin.tv/commerce/payday/clients/redis"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/lock"
	"github.com/facebookgo/inject"
	"github.com/go-redis/redis"
	redis_lock "github.com/go-redis/redis"
	"github.com/go-redis/redis_rate"
)

func Provide(cfg config.Configuration) []*inject.Object {
	var redisClient redis.Cmdable

	if !strings.Blank(cfg.Redis.Cluster) {
		redisClient = redefault.NewClusterClient(cfg.Redis.Cluster, &redefault.ClusterOpts{
			ReadOnly: true,
		})
	} else {
		redisClient = &redis.Client{}
	}

	if cfg.Environment != config.DevelopmentEnvironment {
		err := redisClient.Ping().Err()
		if err != nil {
			log.WithError(err).Error("Could not ping redis instance.")
		}
	}

	redisClientForLock := redis_lock.NewClient(&redis_lock.Options{
		Addr: cfg.Redis.Lock,
	})

	redisLockingClient := lock.NewRedisLockClient(redisClientForLock, lock.DefaultLockOptions())

	redisRateLimiter := redis_rate.NewLimiter(redisClientForLock)

	caches := []*inject.Object{
		{Value: redisLockingClient},
		{Value: redisClient, Name: "clusterClient"},
		{Value: channel.NewChannelManager()},
		{Value: redisRateLimiter},
		{Value: payday_redis.NewClient(cfg), Name: "redisClient"},
	}

	caches = append(caches, redis_caches.NewRedisCaches(cfg)...)

	return caches
}
