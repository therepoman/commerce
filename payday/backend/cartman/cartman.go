package cartman

import (
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/cartman"
	"code.justin.tv/commerce/payday/cartman/decoder/selector"
	"code.justin.tv/commerce/payday/clients/sandstorm"
	"code.justin.tv/commerce/payday/config"
	"github.com/facebookgo/inject"
)

func ProvideCartman(cfg config.Configuration, sand sandstorm.SecretGetter) []*inject.Object {
	decoderSelector, err := selector.NewDefaultAlgorithmAwareDecoderSelector(sand)
	if err != nil {
		log.WithError(err).Panic("Rekt while creating new decoder selector")
	}

	var cartmanValidator cartman.IValidator
	if cfg.CartmanEnforcement {
		cartmanValidator = cartman.NewValidator()
	} else {
		cartmanValidator = cartman.NewAlwaysValidValidator()
	}

	return []*inject.Object{
		{Value: decoderSelector},
		{Value: cartmanValidator},
	}
}
