package api

import (
	"code.justin.tv/commerce/payday/api"
	"code.justin.tv/commerce/payday/api/balance"
	"code.justin.tv/commerce/payday/api/clienttracking"
	"code.justin.tv/commerce/payday/rpc/corleone_tenant_server/handle_fulfillment"
	"code.justin.tv/commerce/payday/rpc/corleone_tenant_server/is_eligible"
	"code.justin.tv/commerce/payday/rpc/paydayserver/admin_grant_unlocked_badge_tier_rewards"
	"code.justin.tv/commerce/payday/rpc/paydayserver/assign_emote_to_bits_tier"
	"code.justin.tv/commerce/payday/rpc/paydayserver/check_bits_products_eligibility"
	"code.justin.tv/commerce/payday/rpc/paydayserver/check_hold"
	"code.justin.tv/commerce/payday/rpc/paydayserver/create_hold"
	"code.justin.tv/commerce/payday/rpc/paydayserver/delete_cheermote_tier"
	"code.justin.tv/commerce/payday/rpc/paydayserver/finalize_hold"
	"code.justin.tv/commerce/payday/rpc/paydayserver/get_auto_refill_settings"
	"code.justin.tv/commerce/payday/rpc/paydayserver/get_badge_tier_emotes"
	"code.justin.tv/commerce/payday/rpc/paydayserver/get_badge_tier_notification"
	"code.justin.tv/commerce/payday/rpc/paydayserver/get_badge_tier_summary_for_emote_group_id"
	"code.justin.tv/commerce/payday/rpc/paydayserver/get_balance"
	"code.justin.tv/commerce/payday/rpc/paydayserver/get_bits_tier_emote_groups"
	"code.justin.tv/commerce/payday/rpc/paydayserver/get_bits_to_broadcaster"
	"code.justin.tv/commerce/payday/rpc/paydayserver/get_cheer_screenshot"
	"code.justin.tv/commerce/payday/rpc/paydayserver/get_cheer_screenshots_for_channel"
	"code.justin.tv/commerce/payday/rpc/paydayserver/get_cheers"
	"code.justin.tv/commerce/payday/rpc/paydayserver/get_extension_transactions"
	"code.justin.tv/commerce/payday/rpc/paydayserver/get_transactions_by_id"
	"code.justin.tv/commerce/payday/rpc/paydayserver/get_user_bits_for_campaign"
	"code.justin.tv/commerce/payday/rpc/paydayserver/get_user_campaign_status"
	"code.justin.tv/commerce/payday/rpc/paydayserver/get_user_settings"
	"code.justin.tv/commerce/payday/rpc/paydayserver/is_eligible_to_use_bits_on_extension"
	"code.justin.tv/commerce/payday/rpc/paydayserver/micro_cheer"
	"code.justin.tv/commerce/payday/rpc/paydayserver/release_hold"
	"code.justin.tv/commerce/payday/rpc/paydayserver/search_transactions"
	"code.justin.tv/commerce/payday/rpc/paydayserver/set_auto_refill_settings"
	"code.justin.tv/commerce/payday/rpc/paydayserver/set_badge_tier_notification_state"
	"code.justin.tv/commerce/payday/rpc/paydayserver/set_user_campaign_status"
	"code.justin.tv/commerce/payday/rpc/paydayserver/set_user_settings"
	"code.justin.tv/commerce/payday/rpc/paydayserver/update_cheermote_tier"
	"code.justin.tv/commerce/payday/rpc/paydayserver/use_bits_on_extension"
	"code.justin.tv/commerce/payday/rpc/paydayserver/use_bits_on_poll"
	"code.justin.tv/commerce/payday/rpc/paydayserver/voldemotes/create_or_update_voldemote"
	"code.justin.tv/commerce/payday/rpc/paydayserver/voldemotes/create_or_update_voldemote_pack"
	"code.justin.tv/commerce/payday/rpc/paydayserver/voldemotes/list_voldemote_packs_for_broadcaster"
	"code.justin.tv/commerce/payday/rpc/paydayserver/voldemotes/list_voldemote_packs_for_viewer"
	"code.justin.tv/commerce/payday/rpc/paydayserver/voldemotes/list_voldemotes"
	"code.justin.tv/commerce/payday/rpc/paydayserver/voldemotes/moderate_voldemote"
	"code.justin.tv/commerce/payday/rpc/paydayserver/voldemotes/remove_voldemote"
	bits_post_process_message "code.justin.tv/commerce/payday/rpc/prism_tenant_server/bits/post_process_message"
	bits_process_message "code.justin.tv/commerce/payday/rpc/prism_tenant_server/bits/process_message"
	bits_tokenize_and_validate "code.justin.tv/commerce/payday/rpc/prism_tenant_server/bits/tokenize_and_validate_message"
	volds_post_process_message "code.justin.tv/commerce/payday/rpc/prism_tenant_server/volds/post_process_message"
	volds_process_message "code.justin.tv/commerce/payday/rpc/prism_tenant_server/volds/process_message"
	volds_tokenize_and_validate "code.justin.tv/commerce/payday/rpc/prism_tenant_server/volds/tokenize_and_validate_message"
	"github.com/facebookgo/inject"
)

type Handlers struct {
	Action      *api.ActionApi      `inject:""`
	ActionAsset *api.ActionAssetApi `inject:""`
	ActionInfo  *api.ActionInfoApi  `inject:""`
	Admin       *api.AdminApi       `inject:""`
	AdminJob    *api.AdminJobApi    `inject:""`
	Balance     *api.BalanceApi     `inject:""`
	Badge       *api.BadgeApi       `inject:""`
	Channel     *api.ChannelApi     `inject:""`
	Dashboard   *api.DashboardApi   `inject:""`
	Entitle     *api.EntitleApi     `inject:""`
	Event       *api.EventApi       `inject:""`
	Image       *api.ImageApi       `inject:""`
	Leaderboard *api.LeaderboardApi `inject:""`
	Prices      *api.PricesApi      `inject:""`
	Products    *api.ProductsApi    `inject:""`
	User        *api.UserApi        `inject:""`
	Witholding  *api.WithholdingApi `inject:""`
}

func Provide() []*inject.Object {
	return []*inject.Object{
		{Value: &Handlers{}},
		{Value: api.NewActionsAPI()},
		{Value: &api.ActionAssetApi{}},
		{Value: &api.ActionInfoApi{}},
		{Value: &api.AdminApi{}},
		{Value: &api.AdminJobApi{}},
		{Value: &api.BalanceApi{}},
		{Value: &api.BadgeApi{}},
		{Value: &api.ChannelApi{}},
		{Value: &api.DashboardApi{}},
		{Value: &api.EntitleApi{}},
		{Value: &api.EventApi{}},
		{Value: &api.ImageApi{}},
		{Value: &api.LeaderboardApi{}},
		{Value: &api.PricesApi{}},
		{Value: &api.ProductsApi{}},
		{Value: &api.UserApi{}},
		{Value: &api.WithholdingApi{}},

		// Twirp APIs, which don't need to be exported like the other APIs to connect to the Mux.
		{Value: &use_bits_on_extension.ServerImpl{}},
		{Value: &is_eligible_to_use_bits_on_extension.ServerImpl{}},
		{Value: &get_extension_transactions.ServerImpl{}},
		{Value: &get_user_bits_for_campaign.ServerImpl{}},
		{Value: &get_user_campaign_status.ServerImpl{}},
		{Value: &set_user_campaign_status.ServerImpl{}},
		{Value: check_bits_products_eligibility.NewServer()},
		{Value: &set_user_settings.ServerImpl{}},
		{Value: &get_user_settings.ServerImpl{}},
		{Value: &get_badge_tier_notification.ServerImpl{}},
		{Value: &set_badge_tier_notification_state.ServerImpl{}},
		{Value: &get_badge_tier_summary_for_emote_group_id.ServerImpl{}},
		{Value: &get_badge_tier_emotes.ServerImpl{}},
		{Value: &admin_grant_unlocked_badge_tier_rewards.ServerImpl{}},
		{Value: &bits_tokenize_and_validate.ServerImpl{}, Name: "bits-tokenize-and-validate-server"},
		{Value: &bits_process_message.ServerImpl{}, Name: "bits-process-message-server"},
		{Value: &bits_post_process_message.ServerImpl{}, Name: "bits-post-process-message-server"},
		{Value: &volds_tokenize_and_validate.ServerImpl{}, Name: "volds-tokenize-and-validate-server"},
		{Value: &volds_process_message.ServerImpl{}, Name: "volds-process-message-server"},
		{Value: &volds_post_process_message.ServerImpl{}, Name: "volds-post-process-message-server"},
		{Value: &get_transactions_by_id.ServerImpl{}},
		{Value: &search_transactions.ServerImpl{}},
		{Value: &use_bits_on_poll.ServerImpl{}},
		{Value: &get_auto_refill_settings.ServerImpl{}},
		{Value: &set_auto_refill_settings.ServerImpl{}},
		{Value: get_balance.NewServer()},
		{Value: get_bits_to_broadcaster.NewServer()},
		{Value: balance.NewGetter()},
		{Value: &is_eligible.ServerImpl{}},
		{Value: &handle_fulfillment.ServerImpl{}},
		{Value: get_cheers.NewServer()},
		{Value: &delete_cheermote_tier.ServerImpl{}},
		{Value: &update_cheermote_tier.ServerImpl{}},
		{Value: &assign_emote_to_bits_tier.ServerImpl{}},
		{Value: &get_cheer_screenshot.ServerImpl{}},
		{Value: &get_cheer_screenshots_for_channel.ServerImpl{}},
		{Value: &get_bits_tier_emote_groups.ServerImpl{}},
		{Value: &micro_cheer.ServerImpl{}},

		// Voldemote CRUD
		{Value: &create_or_update_voldemote.ServerImpl{}},
		{Value: &list_voldemote_packs_for_broadcaster.ServerImpl{}},
		{Value: &list_voldemotes.ServerImpl{}},
		{Value: &moderate_voldemote.ServerImpl{}},
		{Value: &list_voldemote_packs_for_viewer.ServerImpl{}},
		{Value: &create_or_update_voldemote_pack.ServerImpl{}},
		{Value: &remove_voldemote.ServerImpl{}},

		// Holds API
		{Value: check_hold.NewServer()},
		{Value: create_hold.NewServer()},
		{Value: finalize_hold.NewServer()},
		{Value: release_hold.NewServer()},

		// client ID tracking
		{Value: clienttracking.NewTracker()},
	}
}
