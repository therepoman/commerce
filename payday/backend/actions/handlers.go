package actions

import (
	"code.justin.tv/commerce/payday/actions"
	"code.justin.tv/commerce/payday/actions/handlers"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/events/football"
	"github.com/facebookgo/inject"
)

func Provide(cfg config.Configuration) []*inject.Object {
	actionCache, _ := actions.NewActionCache(cfg.Environment)

	sgdq2017Handler := new(handlers.SGDQ2017Handler)
	globalsHandler := new(handlers.GlobalsHandler)
	channelHandler := new(handlers.ChannelHandler)
	sponsoredHandler := new(handlers.SponsoredCheermoteHandler)
	footballEventHandler := new(handlers.FootballEventHandler)
	megaCheermoteHandler := new(handlers.MegaCheermoteHandler)
	sixTierCheermoteHandler := new(handlers.SixTierCheermoteHandler)
	charityHandler := new(handlers.CharityHandler)
	chain := new(handlers.HandlerChain).AppendHandlers(
		sgdq2017Handler.Handle,
		channelHandler.Handle,
		sponsoredHandler.Handle,
		footballEventHandler.Handle,
		megaCheermoteHandler.Handle,
		sixTierCheermoteHandler.Handle,
		charityHandler.Handle,
		globalsHandler.Handle)

	return []*inject.Object{
		{Value: actionCache},
		{Value: football.NewBouncer(), Name: "football-event-bouncer"},
		{Value: sgdq2017Handler},
		{Value: globalsHandler},
		{Value: channelHandler},
		{Value: sponsoredHandler},
		{Value: footballEventHandler},
		{Value: megaCheermoteHandler},
		{Value: sixTierCheermoteHandler},
		{Value: charityHandler},
		{Value: chain, Name: "chain"},
	}
}
