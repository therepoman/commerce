package fetchers

import (
	"code.justin.tv/commerce/payday/actions"
	"code.justin.tv/commerce/payday/cheers"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/products"
	"code.justin.tv/commerce/payday/sponsored_cheermote/campaign"
	"code.justin.tv/commerce/payday/sponsored_cheermote/channel_status"
	"code.justin.tv/commerce/payday/sponsored_cheermote/eligible_channel"
	"code.justin.tv/commerce/payday/sponsored_cheermote/user_campaign_status"
	"code.justin.tv/commerce/payday/utils/experiments"
	"github.com/facebookgo/inject"
)

func Provide(cfg config.Configuration) []*inject.Object {
	return []*inject.Object{
		{Value: actions.NewFetcher()},
		{Value: cheers.NewGlobalCheerFetcher()},
		{Value: campaign.NewFetcher()},
		{Value: channel_status.NewFetcher()},
		{Value: user_campaign_status.NewFetcher()},
		{Value: eligible_channel.NewFetcher()},
		{Value: products.NewFetcher()},
		{Value: experiments.NewFetcher()},
	}
}
