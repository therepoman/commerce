package holds

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	pachinko_client "code.justin.tv/commerce/payday/clients/pachinko"
	actual_pantheon "code.justin.tv/commerce/payday/clients/pantheon"
	"code.justin.tv/commerce/payday/dynamo/holds"
	pachinko_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/pachinko"
	pantheon_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/pantheon"
	holds_dao_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo/holds"
	holds_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/holds"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"github.com/gofrs/uuid"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestFinalizer_Finalizer(t *testing.T) {
	Convey("given a finalizer", t, func() {
		pachinko := new(pachinko_mock.PachinkoClient)
		dao := new(holds_dao_mock.DAO)
		cache := new(holds_mock.Cache)
		pantheon := new(pantheon_mock.IPantheonClientWrapper)

		finalizer := &finalizer{
			Pachinko: pachinko,
			DAO:      dao,
			Cache:    cache,
			Pantheon: pantheon,
		}

		holdID := uuid.Must(uuid.NewV4()).String()
		userID := "user-id"
		operatorTransactionID := uuid.Must(uuid.NewV4()).String()

		oldRecipientFormat := []*paydayrpc.Recipient{
			{
				Id:              "recipient-id-1",
				Amount:          10,
				TransactionType: paydayrpc.TransactionType_GiveBitsToBroadcaster,
			},
			{
				Id:              "recipient-id-2",
				Amount:          20,
				TransactionType: paydayrpc.TransactionType_GiveBitsToBroadcaster,
			},
			{

				Id:              "recipient-id-3",
				Amount:          30,
				TransactionType: paydayrpc.TransactionType_UseBitsOnExtension,
			},
		}

		req := &paydayrpc.FinalizeHoldReq{
			HoldId: holdID,
			UserId: userID,
			BitsUsageAttributions: map[string]*paydayrpc.BitsUsageAttribution{
				"recipient-id-1": {
					BitsRevenueAttribution: 10,
					RewardAttribution:      20,
				},
				"recipient-id-2": {
					BitsRevenueAttribution: 20,
					RewardAttribution:      40,
				},
				"recipient-id-3": {
					BitsRevenueAttribution: 30,
					RewardAttribution:      60,
				},
			},
			TransactionType: paydayrpc.TransactionType_GiveBitsToBroadcaster,
			IgnorePayout:    false,
		}

		hold := &paydayrpc.Hold{
			Amount: 60,
		}

		pantheonResponse := pantheonrpc.GetLeaderboardResp{
			EntryContext: &pantheonrpc.EntryContext{
				Entry: &pantheonrpc.LeaderboardEntry{
					Score: 123,
				},
			},
		}

		ctx := context.Background()

		Convey("given a valid request", func() {
			dao.On("GetOperatorTransactionID", ctx, req.UserId, req.HoldId).Return("123", nil)
			pachinko.On("FinalizeHold", ctx, mock.Anything, req.HoldId, req.UserId).Return(0, nil)
			dao.On("FinalizeHold", ctx, mock.Anything, req.UserId, req.HoldId, mock.Anything, false, mock.Anything, hold.Amount).Return(nil).Times(3)
			cache.On("Del", ctx, req.UserId, req.HoldId).Return()
			pantheon.On("GetLeaderboard", ctx, actual_pantheon.PantheonBitsUsageDomain, pantheonrpc.TimeUnit_ALLTIME, mock.Anything, req.UserId, int64(1), int64(1), false).Return(&pantheonResponse, nil).Times(3)

			err := finalizer.Finalize(ctx, req, hold)
			So(err, ShouldBeNil)

			Convey("calls pachinko to finalize the hold", func() {
				pachinko.AssertCalled(t, "FinalizeHold", ctx, mock.Anything, req.HoldId, req.UserId)
			})

			Convey("calls DAO to save a transaction", func() {
				recipients := make([]*holds.BitsUsageAttribution, 0, len(oldRecipientFormat))
				for _, r := range oldRecipientFormat {
					recipients = append(recipients, &holds.BitsUsageAttribution{
						ID:                                  r.Id,
						BitsRevenueAttributionToBroadcaster: r.Amount,
						RewardAttribution:                   r.Amount * 2,
						TotalWithBroadcasterPrior:           123,
					})
				}

				dao.AssertCalled(t, "FinalizeHold", ctx, mock.Anything, req.UserId, req.HoldId, mock.Anything, false, mock.Anything, hold.Amount)
				savedRecipients := dao.Calls[1].Arguments[4].([]*holds.BitsUsageAttribution)
				So(savedRecipients, ShouldResemble, recipients)
			})
		})

		Convey("given a valid request that has already been committed in pachinko", func() {
			dao.On("GetOperatorTransactionID", ctx, req.UserId, req.HoldId).Return(operatorTransactionID, nil)
			pachinko.On("FinalizeHold", ctx, mock.Anything, req.HoldId, req.UserId).Return(0, pachinko_client.ErrAlreadyCommitted)
			dao.On("FinalizeHold", ctx, mock.Anything, req.UserId, req.HoldId, mock.Anything, false, mock.Anything, hold.Amount).Return(nil)
			cache.On("Del", ctx, req.UserId, req.HoldId).Return()
			pantheon.On("GetLeaderboard", ctx, actual_pantheon.PantheonBitsUsageDomain, pantheonrpc.TimeUnit_ALLTIME, mock.Anything, req.UserId, int64(1), int64(1), false).Return(&pantheonResponse, nil).Times(3)

			err := finalizer.Finalize(ctx, req, hold)

			Convey("ignores the pachinko error", func() {
				So(err, ShouldBeNil)
			})

			Convey("calls DAO to save a transaction", func() {
				recipients := make([]*holds.BitsUsageAttribution, 0, len(oldRecipientFormat))
				for _, r := range oldRecipientFormat {
					recipients = append(recipients, &holds.BitsUsageAttribution{
						ID:                                  r.Id,
						BitsRevenueAttributionToBroadcaster: r.Amount,
						RewardAttribution:                   r.Amount * 2,
						TotalWithBroadcasterPrior:           123,
					})
				}

				dao.AssertCalled(t, "FinalizeHold", ctx, mock.Anything, req.UserId, req.HoldId, mock.Anything, false, mock.Anything, hold.Amount)
				pantheon.AssertCalled(t, "GetLeaderboard", ctx, actual_pantheon.PantheonBitsUsageDomain, pantheonrpc.TimeUnit_ALLTIME, mock.Anything, req.UserId, int64(1), int64(1), false)
				savedRecipients := dao.Calls[1].Arguments[4].([]*holds.BitsUsageAttribution)
				So(savedRecipients, ShouldResemble, recipients)
			})
		})

		Convey("returns an error if", func() {
			Convey("request is nil", func() {
				req = nil
			})

			Convey("holds DAO fails to get an operator transaction ID", func() {
				dao.On("GetOperatorTransactionID", ctx, req.UserId, req.HoldId).Return("", errors.New("can't"))
			})

			Convey("pachinko client fails", func() {
				dao.On("GetOperatorTransactionID", ctx, req.UserId, req.HoldId).Return(operatorTransactionID, nil)
				pantheon.On("GetLeaderboard", ctx, actual_pantheon.PantheonBitsUsageDomain, pantheonrpc.TimeUnit_ALLTIME, mock.Anything, req.UserId, int64(1), int64(1), false).Return(&pantheonResponse, nil).Times(3)
				pachinko.On("FinalizeHold", ctx, mock.Anything, req.HoldId, req.UserId).Return(0, errors.New("error"))
			})

			Convey("holds DAO fails to finalize", func() {
				dao.On("GetOperatorTransactionID", ctx, req.UserId, req.HoldId).Return(operatorTransactionID, nil)
				pachinko.On("FinalizeHold", ctx, mock.Anything, req.HoldId, req.UserId).Return(0, nil)
				pantheon.On("GetLeaderboard", ctx, actual_pantheon.PantheonBitsUsageDomain, pantheonrpc.TimeUnit_ALLTIME, mock.Anything, req.UserId, int64(1), int64(1), false).Return(&pantheonResponse, nil).Times(3)
				dao.On("FinalizeHold", ctx, mock.Anything, req.UserId, req.HoldId, mock.Anything, false, mock.Anything, hold.Amount).Return(errors.New("error"))
			})

			err := finalizer.Finalize(ctx, req, hold)
			So(err, ShouldNotBeNil)
		})
	})
}
