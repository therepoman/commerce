package holds

import (
	"context"

	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"

	"code.justin.tv/commerce/payday/clients/pachinko"
	"code.justin.tv/commerce/payday/dynamo/holds"
)

type Releaser interface {
	Release(ctx context.Context, userID, holdID string, hold *paydayrpc.Hold) error
}

func NewReleaser() Releaser {
	return &releaser{}
}

type releaser struct {
	Pachinko pachinko.PachinkoClient `inject:""`
	Cache    Cache                   `inject:""`
	DAO      holds.DAO               `inject:""`
}

func (r *releaser) Release(ctx context.Context, userID, holdID string, hold *paydayrpc.Hold) error {
	// get an idempotent transaction ID to call pachinko with
	transactionID, err := r.DAO.GetOperatorTransactionID(ctx, userID, holdID)
	if err != nil {
		return err
	}

	_, err = r.Pachinko.ReleaseHold(ctx, transactionID, holdID, userID)
	if err != nil && err != pachinko.ErrAlreadyCommitted {
		return err
	}

	err = r.DAO.ReleaseHold(ctx, transactionID, userID, holdID, hold.Amount)
	if err != nil {
		return err
	}

	go r.Cache.Del(ctx, userID, holdID)

	return nil
}
