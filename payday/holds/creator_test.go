package holds

import (
	"context"
	"errors"
	"testing"
	"time"

	"code.justin.tv/commerce/gogogadget/random"
	"code.justin.tv/commerce/payday/dynamo/holds"
	pachinko_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/pachinko"
	holds_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo/holds"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"github.com/golang/protobuf/ptypes"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestCreator_Create(t *testing.T) {
	Convey("given a hold creator", t, func() {
		pachinkoClient := new(pachinko_mock.PachinkoClient)
		dao := new(holds_mock.DAO)

		c := creator{
			DAO:      dao,
			Pachinko: pachinkoClient,
		}

		ctx := context.Background()
		transactionID := random.String(32)
		userID := random.String(16)
		amount := 322
		expiresAt := time.Now().Add(time.Minute * 10)
		pbExpiresAt, _ := ptypes.TimestampProto(expiresAt)
		req := &paydayrpc.CreateHoldReq{
			HoldId:    transactionID,
			UserId:    userID,
			Amount:    int64(amount),
			ExpiresAt: pbExpiresAt,
		}

		Convey("when we are successful", func() {
			pachinkoClient.On("CreateHold", ctx, transactionID, userID, amount, mock.Anything).Return(0, nil)
			dao.On("Create", ctx, userID, transactionID, int64(amount)).Return(&holds.BalanceHold{
				TransactionID: transactionID,
				UserID:        userID,
				IsActive:      true,
			}, nil)

			hold, err := c.Create(ctx, req)
			So(err, ShouldBeNil)
			So(hold, ShouldNotBeNil)
			So(hold.TransactionID, ShouldEqual, transactionID)
		})

		Convey("when we fail", func() {
			Convey("when we fail to call the holds dao", func() {
				dao.On("Create", ctx, userID, transactionID, int64(amount)).Return(nil, errors.New("WALRUS STRIKE"))
			})

			Convey("when we fail to call Pachinko", func() {
				dao.On("Create", ctx, userID, transactionID, int64(amount)).Return(&holds.BalanceHold{
					TransactionID: transactionID,
					UserID:        userID,
					IsActive:      true,
				}, nil)
				pachinkoClient.On("CreateHold", ctx, transactionID, userID, amount, mock.Anything).Return(0, errors.New("WALRUS STRIKE"))
			})

			hold, err := c.Create(ctx, req)
			So(err, ShouldNotBeNil)
			So(hold, ShouldBeNil)
		})
	})
}
