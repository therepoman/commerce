package holds

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/gogogadget/random"
	"code.justin.tv/commerce/payday/dynamo/holds"
	holds_dao_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo/holds"
	holds_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/holds"
	. "github.com/smartystreets/goconvey/convey"
)

func TestFetcher_Fetch(t *testing.T) {
	Convey("given a holds fetcher", t, func() {
		cache := new(holds_mock.Cache)
		dao := new(holds_dao_mock.DAO)

		f := fetcher{
			Cache: cache,
			DAO:   dao,
		}

		ctx := context.Background()
		userID := random.String(16)
		holdID := random.String(32)
		hold := &holds.BalanceHold{
			TransactionID: holdID,
			UserID:        userID,
		}

		Convey("when the cache returns an entry", func() {
			cache.On("Get", ctx, userID, holdID).Return(hold)

			res, err := f.Fetch(ctx, userID, holdID)
			So(err, ShouldBeNil)
			So(res, ShouldEqual, res)
		})

		Convey("when the cache returns empty", func() {
			cache.On("Get", ctx, userID, holdID).Return(nil)

			Convey("when the DB errors", func() {
				dao.On("GetHold", ctx, userID, holdID).Return(nil, errors.New("WALRUS STRIKE"))

				_, err := f.Fetch(ctx, userID, holdID)
				So(err, ShouldNotBeNil)
			})

			Convey("when the DB returns nothing", func() {
				dao.On("GetHold", ctx, userID, holdID).Return(nil, nil)

				res, err := f.Fetch(ctx, userID, holdID)
				So(err, ShouldBeNil)
				So(res, ShouldBeNil)
			})

			Convey("when the DB returns something", func() {
				dao.On("GetHold", ctx, userID, holdID).Return(hold, nil)
				cache.On("Set", ctx, hold).Return()

				res, err := f.Fetch(ctx, userID, holdID)
				So(err, ShouldBeNil)
				So(res, ShouldEqual, res)
			})
		})
	})
}
