package holds

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/gogogadget/random"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	pachinko_client "code.justin.tv/commerce/payday/clients/pachinko"
	"code.justin.tv/commerce/payday/dynamo/holds"
	pachinko_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/pachinko"
	holds_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/holds"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestGetter_Get(t *testing.T) {
	Convey("given a holds getter", t, func() {
		fetcher := new(holds_mock.Fetcher)
		pachinkoClient := new(pachinko_mock.PachinkoClient)

		g := getter{
			Fetcher:        fetcher,
			PachinkoClient: pachinkoClient,
		}

		ctx := context.Background()
		userID := random.String(16)
		holdID := random.String(32)
		dbHold := &holds.BalanceHold{
			TransactionID: holdID,
			UserID:        userID,
		}

		Convey("when we error getting holds data from db", func() {
			fetcher.On("Fetch", ctx, userID, holdID).Return(nil, errors.New("WALRUS STRIKE"))

			_, err := g.Get(ctx, userID, holdID)
			So(err, ShouldNotBeNil)
		})

		Convey("when we do not have any data about the hold", func() {
			fetcher.On("Fetch", ctx, userID, holdID).Return(nil, nil)

			res, err := g.Get(ctx, userID, holdID)
			So(err, ShouldBeNil)
			So(res, ShouldBeNil)
		})

		Convey("when we have the hold in our db", func() {
			fetcher.On("Fetch", ctx, userID, holdID).Return(dbHold, nil)

			Convey("when we fail to call pachinko", func() {
				pachinkoClient.On("CheckHold", ctx, mock.Anything, mock.Anything).Return(nil, errors.New("WALRUS STRIKE"))

				_, err := g.Get(ctx, userID, holdID)
				So(err, ShouldNotBeNil)
			})

			Convey("when we don't have a record in pachinko", func() {
				pachinkoClient.On("CheckHold", ctx, mock.Anything, mock.Anything).Return(nil, nil)

				res, err := g.Get(ctx, userID, holdID)
				So(err, ShouldBeNil)
				So(res, ShouldBeNil)
			})

			Convey("when we have the record in pachinko", func() {
				hold := &pachinko.Hold{
					TransactionId: holdID,
					Tokens: []*pachinko.Token{
						{
							Quantity: int64(322),
							OwnerId:  userID,
							GroupId:  pachinko_client.GROUP_ID,
							Domain:   pachinko.Domain_BITS,
						},
					},
				}
				pachinkoClient.On("CheckHold", ctx, mock.Anything, mock.Anything).Return(hold, nil)

				res, err := g.Get(ctx, userID, holdID)
				So(err, ShouldBeNil)
				So(res, ShouldNotBeNil)
				So(res.UserId, ShouldEqual, userID)
				So(res.HoldId, ShouldEqual, holdID)
			})
		})
	})
}
