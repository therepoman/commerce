package holds

import (
	"context"

	"code.justin.tv/commerce/payday/dynamo/holds"
)

type Fetcher interface {
	Fetch(ctx context.Context, userID, holdID string) (*holds.BalanceHold, error)
}

func NewFetcher() Fetcher {
	return &fetcher{}
}

type fetcher struct {
	Cache Cache     `inject:""`
	DAO   holds.DAO `inject:""`
}

func (f *fetcher) Fetch(ctx context.Context, userID, holdID string) (*holds.BalanceHold, error) {
	hold := f.Cache.Get(ctx, userID, holdID)
	if hold != nil {
		return hold, nil
	}

	hold, err := f.DAO.GetHold(ctx, userID, holdID)
	if err != nil {
		return nil, err
	}

	if hold != nil {
		go f.Cache.Set(context.Background(), hold)
	}

	return hold, nil
}
