package holds

import (
	"context"

	"code.justin.tv/commerce/payday/clients/pachinko"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
)

type Getter interface {
	Get(ctx context.Context, userID, holdID string) (*paydayrpc.Hold, error)
}

func NewGetter() Getter {
	return &getter{}
}

type getter struct {
	Fetcher                 Fetcher `inject:""`
	pachinko.PachinkoClient `inject:""`
}

func (g *getter) Get(ctx context.Context, userID, holdID string) (*paydayrpc.Hold, error) {
	hold, err := g.Fetcher.Fetch(ctx, userID, holdID)
	if err != nil || hold == nil {
		return nil, err
	}

	pachinkoHold, err := g.PachinkoClient.CheckHold(ctx, userID, holdID)
	if err != nil || pachinkoHold == nil {
		return nil, err
	}

	return &paydayrpc.Hold{
		UserId:      userID,
		HoldId:      holdID,
		ExpiresAt:   pachinkoHold.ExpiresAt,
		Amount:      pachinkoHold.Tokens[0].Quantity,
		IsReleased:  pachinkoHold.IsReleased,
		IsFinalized: pachinkoHold.IsFinalized,
	}, nil
}
