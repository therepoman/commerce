package holds

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"code.justin.tv/commerce/gogogadget/strings"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/clients/redis"
	"code.justin.tv/commerce/payday/dynamo/holds"
	"code.justin.tv/commerce/payday/hystrix"
	cmds "code.justin.tv/commerce/payday/hystrix"
	goredis "github.com/go-redis/redis"
)

const (
	cachePrefix = "holds.%s.%s"
	cacheTTL    = 5 * time.Minute
)

type Cache interface {
	Get(ctx context.Context, userID, holdID string) *holds.BalanceHold
	Set(ctx context.Context, hold *holds.BalanceHold)
	Del(ctx context.Context, userID, holdID string)
}

func NewCache() Cache {
	return &cache{}
}

type cache struct {
	Redis redis.Client `inject:"redisClient"`
}

func cacheKey(userID, holdID string) string {
	return fmt.Sprintf(cachePrefix, userID, holdID)
}

func (c *cache) Get(ctx context.Context, userID, holdID string) *holds.BalanceHold {
	var cacheVal string
	err := hystrix.Do(cmds.HoldsCacheGet, func() error {
		var err error
		cacheVal, err = c.Redis.Get(ctx, cacheKey(userID, holdID)).Result()
		if err != nil && err != goredis.Nil {
			log.WithError(err).Error("could not get hold out of cache")
		}
		return nil
	}, nil)
	if err != nil {
		log.WithError(err).Warn("hystrix error while getting hold out of cache")
	}

	if strings.Blank(cacheVal) {
		return nil
	}

	var hold holds.BalanceHold
	err = json.Unmarshal([]byte(cacheVal), &hold)
	if err != nil {
		c.Del(ctx, userID, holdID)
		log.WithError(err).Error("could not unmarshal cache entry for hold")
	}

	return &hold
}

func (c *cache) Set(ctx context.Context, hold *holds.BalanceHold) {
	data, err := json.Marshal(hold)
	if err != nil {
		log.WithError(err).Error("could not marshal cache entry for hold")
		return
	}

	err = hystrix.Do(cmds.HoldsCacheSet, func() error {
		err := c.Redis.Set(ctx, cacheKey(hold.UserID, hold.TransactionID), string(data), cacheTTL).Err()
		if err != nil {
			log.WithError(err).Error("could not set hold entry in cache")
		}
		return nil
	}, nil)
	if err != nil {
		log.WithError(err).Warn("hystrix error while getting hold out of cache")
	}
}

func (c *cache) Del(ctx context.Context, userID, holdID string) {
	err := hystrix.Do(cmds.HoldsCacheDel, func() error {
		err := c.Redis.Del(ctx, cacheKey(userID, holdID)).Err()
		if err != nil {
			log.WithError(err).Error("could not delete hold entry from cache")
		}
		return nil
	}, nil)
	if err != nil {
		log.WithError(err).Warn("hystrix error while deleting hold out of cache")
	}
}
