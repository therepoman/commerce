package holds

import (
	"context"
	"errors"
	"testing"

	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"

	"code.justin.tv/commerce/gogogadget/random"
	pachinko_rpc "code.justin.tv/commerce/payday/clients/pachinko"
	pachinko_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/pachinko"
	holds_dao_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo/holds"
	holds_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/holds"
	"github.com/gofrs/uuid"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestReleaser_Release(t *testing.T) {
	Convey("given a hold releaser", t, func() {
		pachinkoClient := new(pachinko_mock.PachinkoClient)
		cache := new(holds_mock.Cache)
		dao := new(holds_dao_mock.DAO)

		r := releaser{
			Pachinko: pachinkoClient,
			Cache:    cache,
			DAO:      dao,
		}

		ctx := context.Background()
		userID := random.String(16)
		holdID := random.String(32)
		operatorTransactionID := uuid.Must(uuid.NewV4()).String()

		hold := &paydayrpc.Hold{
			Amount: 1,
		}

		Convey("when we succeed", func() {
			dao.On("GetOperatorTransactionID", ctx, userID, holdID).Return(operatorTransactionID, nil)
			pachinkoClient.On("ReleaseHold", ctx, mock.Anything, holdID, userID).Return(0, nil)
			dao.On("ReleaseHold", ctx, mock.Anything, userID, holdID, hold.Amount).Return(nil)
			cache.On("Del", ctx, userID, holdID).Return()

			err := r.Release(ctx, userID, holdID, hold)
			So(err, ShouldBeNil)
		})

		Convey("when we retry a committed hold", func() {
			dao.On("GetOperatorTransactionID", ctx, userID, holdID).Return(operatorTransactionID, nil)
			pachinkoClient.On("ReleaseHold", ctx, mock.Anything, holdID, userID).Return(0, pachinko_rpc.ErrAlreadyCommitted)
			dao.On("ReleaseHold", ctx, mock.Anything, userID, holdID, hold.Amount).Return(nil)
			cache.On("Del", ctx, userID, holdID).Return()

			err := r.Release(ctx, userID, holdID, hold)
			So(err, ShouldBeNil)

			dao.AssertCalled(t, "ReleaseHold", ctx, operatorTransactionID, userID, holdID, hold.Amount)
		})

		Convey("when we fail", func() {
			Convey("when getting operator transaction ID fails", func() {
				dao.On("GetOperatorTransactionID", ctx, userID, holdID).Return("", errors.New("WALRUS STRIKE"))
			})

			Convey("when pachinko fails", func() {
				dao.On("GetOperatorTransactionID", ctx, userID, holdID).Return(operatorTransactionID, nil)
				pachinkoClient.On("ReleaseHold", ctx, mock.Anything, holdID, userID).Return(0, errors.New("WALRUS STRIKE"))
			})

			Convey("when dynamo update fails", func() {
				dao.On("GetOperatorTransactionID", ctx, userID, holdID).Return(operatorTransactionID, nil)
				pachinkoClient.On("ReleaseHold", ctx, mock.Anything, holdID, userID).Return(0, nil)
				dao.On("ReleaseHold", ctx, mock.Anything, userID, holdID, hold.Amount).Return(errors.New("WALRUS STRIKE"))
			})

			err := r.Release(ctx, userID, holdID, hold)
			So(err, ShouldNotBeNil)
		})
	})
}
