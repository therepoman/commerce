package holds

import (
	"context"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/clients/pachinko"
	"code.justin.tv/commerce/payday/dynamo/holds"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"github.com/golang/protobuf/ptypes"
	"github.com/pkg/errors"
)

type Creator interface {
	Create(ctx context.Context, req *paydayrpc.CreateHoldReq) (*holds.BalanceHold, error)
}

func NewCreator() Creator {
	return &creator{}
}

type creator struct {
	Pachinko pachinko.PachinkoClient `inject:""`
	DAO      holds.DAO               `inject:""`
}

func (c *creator) Create(ctx context.Context, req *paydayrpc.CreateHoldReq) (*holds.BalanceHold, error) {
	var expiresAt *time.Time
	if req.ExpiresAt != nil {
		expiresAtConv, err := ptypes.Timestamp(req.ExpiresAt)
		if err != nil {
			return nil, errors.Wrap(err, "could not parse expires at timestamp for hold")
		}
		expiresAt = &expiresAtConv
	}

	hold, err := c.DAO.Create(ctx, req.UserId, req.HoldId, req.Amount)
	if err != nil {
		return nil, err
	}

	balance, err := c.Pachinko.CreateHold(ctx, req.HoldId, req.UserId, int(req.Amount), expiresAt)
	if err != nil {
		return nil, err
	}

	log.WithFields(log.Fields{
		"operation":        "create_hold",
		"hold_id":          req.HoldId,
		"tuid":             req.UserId,
		"Pachinko_Balance": balance,
	}).Info("Create hold completed successfully.")

	return hold, nil
}
