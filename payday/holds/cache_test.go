package holds

import (
	"context"
	"encoding/json"
	"errors"
	"testing"

	"github.com/stretchr/testify/mock"

	"code.justin.tv/commerce/gogogadget/random"
	"code.justin.tv/commerce/payday/dynamo/holds"
	redis_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/redis"
	"github.com/go-redis/redis"
	. "github.com/smartystreets/goconvey/convey"
)

func TestCache_Get(t *testing.T) {
	Convey("Given a cache getter", t, func() {
		redisMock := new(redis_mock.Client)

		cache := cache{
			Redis: redisMock,
		}

		ctx := context.Background()
		userID := random.String(32)
		holdID := random.String(16)

		cacheKey := cacheKey(userID, holdID)

		Convey("when Redis errors", func() {
			resp := redis.NewStringResult("", errors.New("e"))
			redisMock.On("Get", mock.Anything, cacheKey).Return(resp)

			hold := cache.Get(ctx, userID, holdID)
			So(hold, ShouldBeNil)
		})

		Convey("when Redis errors with Redis.Nil", func() {
			resp := redis.NewStringResult("", redis.Nil)
			redisMock.On("Get", mock.Anything, cacheKey).Return(resp)

			hold := cache.Get(ctx, userID, holdID)
			So(hold, ShouldBeNil)
		})

		Convey("when everything succeeds", func() {
			dynamoGemConfigs := &holds.BalanceHold{
				TransactionID: holdID,
				UserID:        userID,
			}
			gemConfigsJson, _ := json.Marshal(&dynamoGemConfigs)

			resp := redis.NewStringResult(string(gemConfigsJson), nil)
			redisMock.On("Get", mock.Anything, cacheKey).Return(resp)

			hold := cache.Get(ctx, userID, holdID)
			So(hold, ShouldNotBeNil)
			So(hold.UserID, ShouldEqual, userID)
			So(hold.TransactionID, ShouldEqual, holdID)
		})
	})
}

func TestCache_Set(t *testing.T) {
	Convey("Given a cache setter", t, func() {
		redisMock := new(redis_mock.Client)

		cache := cache{
			Redis: redisMock,
		}

		ctx := context.Background()
		holdID := random.String(32)
		userID := random.String(16)

		cacheKey := cacheKey(userID, holdID)

		input := &holds.BalanceHold{
			TransactionID: holdID,
			UserID:        userID,
		}

		Convey("when Redis fails", func() {
			jsonInput, _ := json.Marshal(&input)
			redisMock.On("Set", mock.Anything, cacheKey, string(jsonInput), cacheTTL).Return(redis.NewStatusCmd(errors.New("e")))

			cache.Set(ctx, input)
			redisMock.AssertCalled(t, "Set", mock.Anything, cacheKey, string(jsonInput), cacheTTL)
		})

		Convey("when everything succeeds", func() {
			jsonInput, _ := json.Marshal(&input)
			redisMock.On("Set", mock.Anything, cacheKey, string(jsonInput), cacheTTL).Return(redis.NewStatusCmd(nil))

			cache.Set(ctx, input)
			redisMock.AssertCalled(t, "Set", mock.Anything, cacheKey, string(jsonInput), cacheTTL)
		})
	})
}

func TestCache_Del(t *testing.T) {
	Convey("Given a cache del", t, func() {
		redisMock := new(redis_mock.Client)

		cache := cache{
			Redis: redisMock,
		}

		ctx := context.Background()
		holdID := random.String(32)
		userID := random.String(16)

		cacheKey := cacheKey(userID, holdID)

		Convey("when Redis errors", func() {
			redisMock.On("Del", mock.Anything, cacheKey).Return(redis.NewIntCmd(errors.New("e")))

			cache.Del(ctx, userID, holdID)
			redisMock.AssertCalled(t, "Del", mock.Anything, cacheKey)
		})

		Convey("when Redis succeeds", func() {
			redisMock.On("Del", mock.Anything, cacheKey).Return(redis.NewIntCmd(nil))

			cache.Del(ctx, userID, holdID)
			redisMock.AssertCalled(t, "Del", mock.Anything, cacheKey)
		})
	})
}
