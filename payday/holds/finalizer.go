package holds

import (
	"context"
	"errors"
	"sort"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	"code.justin.tv/commerce/payday/clients/pachinko"
	"code.justin.tv/commerce/payday/clients/pantheon"
	"code.justin.tv/commerce/payday/dynamo/holds"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
)

type Finalizer interface {
	Finalize(ctx context.Context, req *paydayrpc.FinalizeHoldReq, hold *paydayrpc.Hold) error
}

func NewFinalizer() Finalizer {
	return &finalizer{}
}

type finalizer struct {
	Pachinko pachinko.PachinkoClient         `inject:""`
	DAO      holds.DAO                       `inject:""`
	Cache    Cache                           `inject:""`
	Pantheon pantheon.IPantheonClientWrapper `inject:""`
}

func (f *finalizer) Finalize(ctx context.Context, req *paydayrpc.FinalizeHoldReq, hold *paydayrpc.Hold) error {
	if req == nil {
		return errors.New("req is missing")
	}

	// get an idempotent transaction ID to call pachinko with
	transactionID, err := f.DAO.GetOperatorTransactionID(ctx, req.UserId, req.HoldId)
	if err != nil {
		return err
	}

	// we should consider revisiting this as it's expensive but for now it's probably fine.
	bitTotals := make(map[string]int64)
	for k := range req.BitsUsageAttributions {
		leaderboardEntry, err := f.Pantheon.GetLeaderboard(ctx, pantheon.PantheonBitsUsageDomain, pantheonrpc.TimeUnit_ALLTIME, k, req.UserId, 1, 1, false)
		if err != nil {
			log.WithError(err).Error("failed to fetch leaderboard for transaction creation on cheer")
			return err
		}

		if leaderboardEntry != nil && leaderboardEntry.GetEntryContext() != nil && leaderboardEntry.GetEntryContext().GetEntry() != nil {
			bitTotals[k] = leaderboardEntry.GetEntryContext().GetEntry().Score
		}
	}

	balance, err := f.Pachinko.FinalizeHold(ctx, transactionID, req.HoldId, req.UserId)
	if err != nil && err != pachinko.ErrAlreadyCommitted {
		return err
	}

	log.WithFields(log.Fields{
		"operation":        "finalize_hold",
		"transaction_id":   transactionID,
		"hold_id":          req.HoldId,
		"tuid":             req.UserId,
		"Pachinko_Balance": balance,
	}).Info("Finalizing hold completed successfully.")

	recipients := make([]*holds.BitsUsageAttribution, 0, len(req.BitsUsageAttributions))
	for k, v := range req.BitsUsageAttributions {
		recipients = append(recipients, &holds.BitsUsageAttribution{
			ID:                                  k,
			BitsRevenueAttributionToBroadcaster: v.BitsRevenueAttribution,
			RewardAttribution:                   v.RewardAttribution,
			TotalWithBroadcasterPrior:           bitTotals[k],
		})
	}

	// Golang randomly iterates through maps, so slices generated from indexing through a Map are in a random order.
	// Sort these to keep them in a consistent order, for unit tests and other things
	sort.Slice(recipients, func(i, j int) bool { return recipients[i].ID < recipients[j].ID })

	err = f.DAO.FinalizeHold(ctx, transactionID, req.UserId, req.HoldId, recipients, req.IgnorePayout, paydayrpc.TransactionType_name[int32(req.TransactionType)], hold.Amount)
	if err != nil {
		return err
	}

	go f.Cache.Del(context.Background(), req.UserId, req.HoldId)

	return nil
}
