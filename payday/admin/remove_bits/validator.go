package remove_bits

import (
	"net/http"
	"strconv"

	httperror "code.justin.tv/commerce/payday/errors/http"
	apimodel "code.justin.tv/commerce/payday/models/api"
)

type Validator interface {
	Validate(request apimodel.RemoveBitsRequest) error
}

type ValidatorImpl struct {
}

func (v *ValidatorImpl) Validate(request apimodel.RemoveBitsRequest) error {
	if request.AmountOfBitsToRemove < 0 {
		return httperror.New("Positive removeBits amount required", http.StatusBadRequest)
	}
	if request.AdminUserId == "" {
		return httperror.New("AdminUserId required", http.StatusBadRequest)
	}
	if request.AdminReason == "" {
		return httperror.New("AdminReason required", http.StatusBadRequest)
	}
	if request.EventId == "" {
		return httperror.New("EventId required", http.StatusBadRequest)
	}
	_, err := strconv.Atoi(request.TwitchUserId)
	if err != nil {
		return httperror.New("Numeric twitchUserId required", http.StatusBadRequest)
	}
	return nil
}
