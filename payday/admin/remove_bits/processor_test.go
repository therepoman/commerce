package remove_bits

import (
	"context"
	"errors"
	"testing"
	"time"

	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/errors/http"
	cache_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/cache"
	pachinko_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/pachinko"
	dynamo_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo"
	metrics_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/metrics"
	apimodel "code.justin.tv/commerce/payday/models/api"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

var transactionId = "testTransactionId"
var adminUserId = "123"
var adminReason = "testReason"
var testUserId = "456"

var pachinkoMock *pachinko_mock.PachinkoClient
var txDAOMock *dynamo_mock.ITransactionsDao

func SetupRemoveBits() *ProcessorImpl {
	pachinkoMock = new(pachinko_mock.PachinkoClient)
	txDAOMock = new(dynamo_mock.ITransactionsDao)
	cp := new(cache_mock.Purger)

	cp.On("BalanceUpdatePurge", mock.Anything, mock.Anything)

	ab := &ProcessorImpl{
		Validator:       &ValidatorImpl{},
		CachePurger:     cp,
		TransactionsDao: txDAOMock,
		Pachinko:        pachinkoMock,
	}

	return ab
}

func MockTransactionsDAO(err error) {
	txDAOMock.On("Update", mock.Anything).Return(err)
	txDAOMock.On("Get", mock.Anything).Return(nil, nil)
}

func MockPachinkoRemoveBits(updatedBalance int, err error) {
	pachinkoMock.On("RemoveBits",
		mock.Anything,
		mock.Anything,
		mock.Anything,
		mock.Anything,
	).Return(updatedBalance, err)
}

func TestRemoveBitsNoBody(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	ab := SetupRemoveBits()

	err := ab.Process(ctx, apimodel.RemoveBitsRequest{})
	assert.Equal(t, 400, err.(http.HttpError).StatusCode())
}

func TestRemoveBitsNegativeAmount(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	ab := SetupRemoveBits()
	MockTransactionsDAO(nil)

	err := ab.Process(ctx, apimodel.RemoveBitsRequest{
		AmountOfBitsToRemove: -1,
	})

	assert.Equal(t, 400, err.(http.HttpError).StatusCode())
}

func TestRemoveBitsAdminUserIdMissing(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	ab := SetupRemoveBits()
	MockTransactionsDAO(nil)

	err := ab.Process(ctx, apimodel.RemoveBitsRequest{
		AmountOfBitsToRemove: 1,
	})

	assert.Equal(t, 400, err.(http.HttpError).StatusCode())
}

func TestRemoveBitsAdminUserIdNotNumeric(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	ab := SetupRemoveBits()

	req := apimodel.RemoveBitsRequest{
		AmountOfBitsToRemove: 1,
		AdminUserId:          "notNumeric",
		AdminReason:          adminReason,
		EventId:              transactionId,
		TwitchUserId:         testUserId,
	}

	MockTransactionsDAO(nil)
	MockPachinkoRemoveBits(1138, nil)

	err := ab.Process(ctx, req)

	assert.Nil(t, err)
}

func TestRemoveBitsAdminReasonMissing(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	ab := SetupRemoveBits()

	err := ab.Process(ctx, apimodel.RemoveBitsRequest{
		AmountOfBitsToRemove: 1,
		AdminUserId:          adminUserId,
	})

	assert.Equal(t, 400, err.(http.HttpError).StatusCode())
}

func TestRemoveBitsTransactionIdMissing(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	ab := SetupRemoveBits()

	err := ab.Process(ctx, apimodel.RemoveBitsRequest{
		AmountOfBitsToRemove: 1,
		AdminUserId:          adminUserId,
		AdminReason:          adminReason,
	})

	assert.Equal(t, 400, err.(http.HttpError).StatusCode())
}

func TestRemoveBitsTwitchUserIdMissing(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	ab := SetupRemoveBits()

	err := ab.Process(ctx, apimodel.RemoveBitsRequest{
		AmountOfBitsToRemove: 1,
		AdminUserId:          adminUserId,
		AdminReason:          adminReason,
		EventId:              transactionId,
	})

	assert.Equal(t, 400, err.(http.HttpError).StatusCode())
}

func TestRemoveBitsTwitchUserIdNotNumeric(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	ab := SetupRemoveBits()

	err := ab.Process(ctx, apimodel.RemoveBitsRequest{
		AmountOfBitsToRemove: 1,
		AdminUserId:          adminUserId,
		AdminReason:          adminReason,
		EventId:              transactionId,
		TwitchUserId:         "notNumeric",
	})

	assert.Equal(t, 400, err.(http.HttpError).StatusCode())
}

func TestRemoveBitsdTransactionDAOError(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	ab := SetupRemoveBits()

	req := apimodel.RemoveBitsRequest{
		AmountOfBitsToRemove: 1,
		AdminUserId:          adminUserId,
		AdminReason:          adminReason,
		EventId:              transactionId,
		TwitchUserId:         testUserId,
	}

	MockTransactionsDAO(errors.New("transactions DAO error"))
	MockPachinkoRemoveBits(0, errors.New("Test Pachinko Error"))

	err := ab.Process(ctx, req)

	assert.Equal(t, 500, err.(http.HttpError).StatusCode())
}

func TestRemoveBitsFabsError(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	ab := SetupRemoveBits()

	req := apimodel.RemoveBitsRequest{
		AmountOfBitsToRemove: 1,
		AdminUserId:          adminUserId,
		AdminReason:          adminReason,
		EventId:              transactionId,
		TwitchUserId:         testUserId,
	}

	MockTransactionsDAO(nil)
	MockPachinkoRemoveBits(0, errors.New("Test Pachinko Error"))

	err := ab.Process(ctx, req)

	assert.Equal(t, 500, err.(http.HttpError).StatusCode())
}

func TestRemoveBitsHappy(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	ab := SetupRemoveBits()

	req := apimodel.RemoveBitsRequest{
		AmountOfBitsToRemove: 1,
		AdminUserId:          adminUserId,
		AdminReason:          adminReason,
		EventId:              transactionId,
		TwitchUserId:         testUserId,
	}

	MockTransactionsDAO(nil)
	MockPachinkoRemoveBits(1138, nil)

	err := ab.Process(ctx, req)

	assert.Nil(t, err)
}

func TestRemoveBitsBalanceMismatch(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	ab := SetupRemoveBits()

	req := apimodel.RemoveBitsRequest{
		AmountOfBitsToRemove: 1,
		AdminUserId:          "177167226",
		AdminReason:          adminReason,
		EventId:              transactionId,
		TwitchUserId:         testUserId,
	}

	MockTransactionsDAO(nil)
	MockPachinkoRemoveBits(1776, nil)

	err := ab.Process(ctx, req)

	// Sleep for just a brief moment so that the async process can catch up
	time.Sleep(20 * time.Millisecond)

	assert.Nil(t, err)
}

func TestRemoveBitsIdempotency(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	quanity := int64(1)

	// intentionally sparsely populated processor should fail if any actual operations come around.
	td := new(dynamo_mock.ITransactionsDao)
	ab := &ProcessorImpl{
		Validator:       &ValidatorImpl{},
		TransactionsDao: td,
	}
	td.On("Get", mock.Anything).Return(&dynamo.Transaction{
		TransactionId: transactionId,
		UserId:        &testUserId,
		NumberOfBits:  &quanity,
		AdminID:       &adminUserId,
		AdminReason:   &adminReason,
	}, nil)

	req := apimodel.RemoveBitsRequest{
		AmountOfBitsToRemove: 1,
		AdminUserId:          adminUserId,
		AdminReason:          adminReason,
		EventId:              transactionId,
		TwitchUserId:         testUserId,
	}
	err := ab.Process(ctx, req)
	assert.Nil(t, err)
}

func TestRemoveBitsIdempotencyError(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	td := new(dynamo_mock.ITransactionsDao)
	statter := new(metrics_mock.Statter)
	ab := &ProcessorImpl{
		Validator:       &ValidatorImpl{},
		TransactionsDao: td,
		Statter:         statter,
	}
	td.On("Get", mock.Anything).Return(&dynamo.Transaction{}, nil)
	statter.On("Inc", "AdminRemove_Bad_Idempotency_Error", int64(1))

	req := apimodel.RemoveBitsRequest{
		AmountOfBitsToRemove: 1,
		AdminUserId:          adminUserId,
		AdminReason:          adminReason,
		EventId:              transactionId,
		TwitchUserId:         testUserId,
	}
	err := ab.Process(ctx, req)

	// Sleep for just a brief moment so that the async process can catch up
	time.Sleep(50 * time.Millisecond)

	assert.Equal(t, badIdempotencyError(), err)
	statter.AssertCalled(t, "Inc", "AdminRemove_Bad_Idempotency_Error", int64(1))
}
