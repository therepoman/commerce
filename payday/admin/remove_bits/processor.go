package remove_bits

import (
	"context"
	"net/http"
	"time"

	"code.justin.tv/commerce/gogogadget/pointers"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/cache"
	"code.justin.tv/commerce/payday/clients/pachinko"
	"code.justin.tv/commerce/payday/dynamo"
	httperror "code.justin.tv/commerce/payday/errors/http"
	"code.justin.tv/commerce/payday/metrics"
	"code.justin.tv/commerce/payday/models"
	apimodel "code.justin.tv/commerce/payday/models/api"
)

type Processor interface {
	Process(ctx context.Context, req apimodel.RemoveBitsRequest) error
}

type ProcessorImpl struct {
	Validator       Validator               `inject:""`
	TransactionsDao dynamo.ITransactionsDao `inject:""`
	CachePurger     cache.Purger            `inject:""`
	Pachinko        pachinko.PachinkoClient `inject:""`
	Statter         metrics.Statter         `inject:""`
}

type FabsResponse struct {
	UpdatedBalance int
	Error          error
}

type PachinkoResponse struct {
	UpdatedBalance int
	Error          error
}

func (a *ProcessorImpl) Process(ctx context.Context, req apimodel.RemoveBitsRequest) error {
	validationErr := a.Validator.Validate(req)
	if validationErr != nil {
		log.WithError(validationErr).Error("Invalid removeBits request")
		return validationErr
	}

	isDup, err := a.idempotencyCheck(ctx, req)
	if err != nil {
		return err
	} else if isDup {
		return nil
	}

	err = a.TransactionsDao.Update(&dynamo.Transaction{
		TransactionId:   req.EventId,
		TransactionType: pointers.StringP(models.AdminRemoveBitsTransactionType),
		UserId:          pointers.StringP(req.TwitchUserId),
		NumberOfBits:    pointers.Int64P(int64(req.AmountOfBitsToRemove)),
		AdminID:         pointers.StringP(req.AdminUserId),
		AdminReason:     pointers.StringP(req.AdminReason),
		TimeOfEvent:     time.Now(),
	})

	if err != nil {
		log.WithError(err).Error("Failed to write to transactions table on admin remove")
		return httperror.NewWithCause("Error saving transaction", http.StatusInternalServerError, err)
	}

	updatedBalance, err := a.Pachinko.RemoveBits(ctx, req.EventId, req.TwitchUserId, int(req.AmountOfBitsToRemove))
	if err != nil {
		log.WithFields(log.Fields{
			"EventId":      req.EventId,
			"TwitchUserId": req.TwitchUserId,
		}).WithError(err).Error("Error doing AdminRemoveBits primary call")
		return httperror.NewWithCause("Downstream service error", http.StatusInternalServerError, err)
	}

	a.CachePurger.BalanceUpdatePurge(ctx, req.TwitchUserId)

	go func() {
		err := a.TransactionsDao.Update(&dynamo.Transaction{
			TransactionId:             req.EventId,
			TotalBitsAfterTransaction: pointers.Int64P(int64(updatedBalance)),
		})

		if err != nil {
			log.WithFields(log.Fields{
				"transactionID":             req.EventId,
				"totalBitsAfterTransaction": updatedBalance,
			}).WithError(err).Error("Failed to update to transactions table on admin remove")
		}
	}()

	return nil
}

func (a *ProcessorImpl) idempotencyCheck(ctx context.Context, req apimodel.RemoveBitsRequest) (bool, error) {
	tx, err := a.TransactionsDao.Get(req.EventId)
	if err != nil {
		return false, err
	}

	if tx != nil {
		txUserId := pointers.StringOrDefault(tx.UserId, "")
		txNumOfBits := int32(pointers.Int64OrDefault(tx.NumberOfBits, 0))
		txAdminUserId := pointers.StringOrDefault(tx.AdminID, "")
		txAdminReason := pointers.StringOrDefault(tx.AdminReason, "")

		if req.TwitchUserId != txUserId ||
			req.AmountOfBitsToRemove != txNumOfBits ||
			req.AdminUserId != txAdminUserId ||
			req.AdminReason != txAdminReason {
			log.WithFields(log.Fields{
				"request":    req,
				"txAdminId":  txAdminUserId,
				"reqAdminId": req.AdminUserId,
				"txUserId":   txUserId,
				"reqUserId":  req.TwitchUserId,
			}).Warn("Idempotency error on admin remove")
			go a.Statter.Inc("AdminRemove_Bad_Idempotency_Error", 1)
			return false, badIdempotencyError()
		}
		return true, nil
	}
	return false, nil
}

func badIdempotencyError() error {
	return httperror.New("Duplicate transaction id for non-duplicate request", http.StatusBadRequest)
}
