package add_bits

import (
	"context"
	"net/http"
	"time"

	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/gogogadget/strings"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/bulk_grant"
	"code.justin.tv/commerce/payday/cache"
	"code.justin.tv/commerce/payday/clients/pachinko"
	"code.justin.tv/commerce/payday/dynamo"
	httperror "code.justin.tv/commerce/payday/errors/http"
	"code.justin.tv/commerce/payday/metrics"
	"code.justin.tv/commerce/payday/models"
	apimodel "code.justin.tv/commerce/payday/models/api"
	"github.com/pkg/errors"
)

type Processor interface {
	Process(ctx context.Context, req apimodel.AddBitsRequest) error
}

type ProcessorImpl struct {
	Validator                Validator                  `inject:""`
	ThresholdBreachProcessor ThresholdBreachProcessor   `inject:"thresholdBreachProcessor"`
	TransactionsDao          dynamo.ITransactionsDao    `inject:""`
	CachePurger              cache.Purger               `inject:""`
	BulkGrantCampaignsGetter bulk_grant.CampaignsGetter `inject:""`
	Pachinko                 pachinko.PachinkoClient    `inject:""`
	Statter                  metrics.Statter            `inject:""`
}

type FabsResponse struct {
	UpdatedBalance int
	Error          error
}

type PachinkoResponse struct {
	UpdatedBalance int
	Error          error
}

func (a *ProcessorImpl) Process(ctx context.Context, req apimodel.AddBitsRequest) error {
	validationErr := a.Validator.Validate(req)
	if validationErr != nil {
		log.WithError(validationErr).Error("Invalid addBits request")
		return validationErr
	}

	thresholdBreachProcessErr := a.ThresholdBreachProcessor.Process(ctx, req)
	if thresholdBreachProcessErr != nil {
		return errors.Wrap(thresholdBreachProcessErr, "Error analyzing threshold breach for admin grant")
	}

	isDup, err := a.idempotencyCheck(ctx, req)
	if err != nil {
		return err
	} else if isDup {
		return nil
	}

	if !strings.Blank(req.BulkGrantCampaignID) {
		campaign := a.BulkGrantCampaignsGetter.GetCampaign(req.BulkGrantCampaignID)
		if campaign == nil {
			return httperror.New("BulkGrantCampaignID not found", http.StatusBadRequest)
		}

		req.AmountOfBitsToAdd = campaign.BitAmount
		req.TypeOfBitsToAdd = campaign.BitType
		req.AdminReason = campaign.AdminReason
		req.NotifyUser = !strings.Blank(campaign.PushyNotificationOrigin)
		req.Origin = campaign.PushyNotificationOrigin

		// TODO: Add user limit validation
	}

	numberOfBits := int64(req.AmountOfBitsToAdd)
	err = a.TransactionsDao.Update(&dynamo.Transaction{
		TransactionId:   req.EventId,
		TransactionType: pointers.StringP(models.AdminAddBitsTransactionType),
		BitsType:        pointers.StringP(req.TypeOfBitsToAdd),
		UserId:          pointers.StringP(req.TwitchUserId),
		NumberOfBits:    pointers.Int64P(numberOfBits),
		NotifyUser:      pointers.BoolP(req.NotifyUser),
		Origin:          pointers.StringP(req.Origin),
		AdminID:         pointers.StringP(req.AdminUserId),
		AdminReason:     pointers.StringP(req.AdminReason),
		TimeOfEvent:     time.Now(),
	})

	if err != nil {
		err = errors.Wrap(err, "Failed to write to transactions table on admin add")
		return httperror.NewWithCause("Error saving transaction", http.StatusInternalServerError, err)
	}

	updatedBalance, err := a.Pachinko.AddBits(ctx, req.EventId, req.TwitchUserId, int(req.AmountOfBitsToAdd))
	if err != nil {
		log.WithFields(log.Fields{
			"EventId":      req.EventId,
			"TwitchUserId": req.TwitchUserId,
		}).WithError(err).Error("Error doing AdminAddBits primary call")
		return httperror.NewWithCause("Downstream service error", http.StatusInternalServerError, err)
	}

	a.CachePurger.BalanceUpdatePurge(ctx, req.TwitchUserId)

	go func() {
		err := a.TransactionsDao.Update(&dynamo.Transaction{
			TransactionId:             req.EventId,
			TotalBitsAfterTransaction: pointers.Int64P(int64(updatedBalance)),
		})

		if err != nil {
			log.WithFields(log.Fields{
				"transactionID":             req.EventId,
				"totalBitsAfterTransaction": updatedBalance,
			}).WithError(err).Error("Failed to update to transactions table on admin add")
		}
	}()

	return nil
}

func (a *ProcessorImpl) idempotencyCheck(ctx context.Context, req apimodel.AddBitsRequest) (bool, error) {
	tx, err := a.TransactionsDao.Get(req.EventId)
	if err != nil {
		return false, err
	}

	if tx != nil {
		txBitType := pointers.StringOrDefault(tx.BitsType, "")
		txUserId := pointers.StringOrDefault(tx.UserId, "")
		txNumOfBits := int32(pointers.Int64OrDefault(tx.NumberOfBits, 0))
		txAdminUserId := pointers.StringOrDefault(tx.AdminID, "")
		txAdminReason := pointers.StringOrDefault(tx.AdminReason, "")

		if req.TwitchUserId != txUserId || req.TypeOfBitsToAdd != txBitType || req.AmountOfBitsToAdd != txNumOfBits || req.AdminUserId != txAdminUserId || req.AdminReason != txAdminReason {
			log.WithFields(log.Fields{
				"request":    req,
				"txAdminId":  txAdminUserId,
				"reqAdminId": req.AdminUserId,
				"txUserId":   txUserId,
				"reqUserId":  req.TwitchUserId,
			}).Warn("Idempotency error on admin add")
			go a.Statter.Inc("AdminAdd_Bad_Idempotency_Error", 1)
			return false, badIdempotencyError()
		}
		return true, nil
	}
	return false, nil
}

func badIdempotencyError() error {
	return httperror.New("Duplicate transaction id for non-duplicate request", http.StatusBadRequest)
}
