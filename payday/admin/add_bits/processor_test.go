package add_bits

import (
	"context"
	"errors"
	"testing"
	"time"

	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/errors/http"
	add_bits_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/admin/add_bits"
	cache_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/cache"
	pachinko_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/pachinko"
	dynamo_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo"
	metrics_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/metrics"
	apimodel "code.justin.tv/commerce/payday/models/api"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

var transactionId = "testTransactionId"
var adminUserId = "123"
var adminReason = "testReason"
var testUserId = "456"
var bitType = "CB_0"

var pachinkoMock *pachinko_mock.PachinkoClient

func SetupAddBits() *ProcessorImpl {
	pachinkoMock = new(pachinko_mock.PachinkoClient)
	thresholdBreachMock := new(add_bits_mock.ThresholdBreachProcessor)
	cp := new(cache_mock.Purger)
	td := new(dynamo_mock.ITransactionsDao)

	cp.On("BalanceUpdatePurge", mock.Anything, mock.Anything)
	thresholdBreachMock.On("Process", mock.Anything, mock.Anything).Return(nil)
	td.On("Update", mock.Anything).Return(nil)
	td.On("Get", mock.Anything).Return(nil, nil)
	//AddBits(ctx, req.EventId, req.TwitchUserId, int(req.AmountOfBitsToAdd)

	ab := &ProcessorImpl{
		Validator:                &ValidatorImpl{},
		ThresholdBreachProcessor: thresholdBreachMock,
		Pachinko:                 pachinkoMock,
		CachePurger:              cp,
		TransactionsDao:          td,
	}

	return ab
}

func MockPachinkoAddBits(balance int, response error) {
	pachinkoMock.On("AddBits",
		mock.Anything,
		mock.Anything,
		mock.Anything,
		mock.Anything,
	).Return(balance, response)
}

func TestAddBitsNoBody(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	ab := SetupAddBits()

	err := ab.Process(ctx, apimodel.AddBitsRequest{})
	assert.Equal(t, 400, err.(http.HttpError).StatusCode())
}

func TestAddBitsNegativeAmount(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	ab := SetupAddBits()

	err := ab.Process(ctx, apimodel.AddBitsRequest{
		AmountOfBitsToAdd: -1,
	})

	assert.Equal(t, 400, err.(http.HttpError).StatusCode())
}

func TestAddBitsAdminUserIdMissing(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	ab := SetupAddBits()

	err := ab.Process(ctx, apimodel.AddBitsRequest{
		AmountOfBitsToAdd: 1,
	})

	assert.Equal(t, 400, err.(http.HttpError).StatusCode())
}

func TestAddBitsAdminUserIdNotNumeric(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	ab := SetupAddBits()

	req := apimodel.AddBitsRequest{
		AmountOfBitsToAdd: 1,
		AdminUserId:       "notNumeric",
		AdminReason:       adminReason,
		EventId:           transactionId,
		TwitchUserId:      testUserId,
		TypeOfBitsToAdd:   bitType,
	}

	MockPachinkoAddBits(7, nil)

	err := ab.Process(ctx, req)

	assert.Nil(t, err)
}

func TestAddBitsAdminReasonMissing(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	ab := SetupAddBits()

	err := ab.Process(ctx, apimodel.AddBitsRequest{
		AmountOfBitsToAdd: 1,
		AdminUserId:       adminUserId,
	})

	assert.Equal(t, 400, err.(http.HttpError).StatusCode())
}

func TestAddBitsTransactionIdMissing(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	ab := SetupAddBits()

	err := ab.Process(ctx, apimodel.AddBitsRequest{
		AmountOfBitsToAdd: 1,
		AdminUserId:       adminUserId,
		AdminReason:       adminReason,
	})

	assert.Equal(t, 400, err.(http.HttpError).StatusCode())
}

func TestAddBitsTwitchUserIdMissing(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	ab := SetupAddBits()

	err := ab.Process(ctx, apimodel.AddBitsRequest{
		AmountOfBitsToAdd: 1,
		AdminUserId:       adminUserId,
		AdminReason:       adminReason,
		EventId:           transactionId,
	})

	assert.Equal(t, 400, err.(http.HttpError).StatusCode())
}

func TestAddBitsTwitchUserIdNotNumeric(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	ab := SetupAddBits()

	err := ab.Process(ctx, apimodel.AddBitsRequest{
		AmountOfBitsToAdd: 1,
		AdminUserId:       adminUserId,
		AdminReason:       adminReason,
		EventId:           transactionId,
		TwitchUserId:      "notNumeric",
	})

	assert.Equal(t, 400, err.(http.HttpError).StatusCode())
}

func TestAddBitsTypeMissing(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	ab := SetupAddBits()

	err := ab.Process(ctx, apimodel.AddBitsRequest{
		AmountOfBitsToAdd: 1,
		AdminUserId:       adminUserId,
		AdminReason:       adminReason,
		EventId:           transactionId,
		TwitchUserId:      testUserId,
	})

	assert.Equal(t, 400, err.(http.HttpError).StatusCode())
}

func TestAddBitsInvalidType(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	ab := SetupAddBits()

	err := ab.Process(ctx, apimodel.AddBitsRequest{
		AmountOfBitsToAdd: 1,
		AdminUserId:       adminUserId,
		AdminReason:       adminReason,
		EventId:           transactionId,
		TwitchUserId:      testUserId,
		TypeOfBitsToAdd:   "NotValidType",
	})

	assert.Equal(t, 400, err.(http.HttpError).StatusCode())
}

func TestAddBitsOriginMissing(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	ab := SetupAddBits()

	err := ab.Process(ctx, apimodel.AddBitsRequest{
		AmountOfBitsToAdd: 1,
		AdminUserId:       adminUserId,
		AdminReason:       adminReason,
		EventId:           transactionId,
		TwitchUserId:      testUserId,
		TypeOfBitsToAdd:   bitType,
		NotifyUser:        true,
	})

	assert.Equal(t, 400, err.(http.HttpError).StatusCode())
}

func TestAddBitsFabsError(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	ab := SetupAddBits()

	req := apimodel.AddBitsRequest{
		AmountOfBitsToAdd: 1,
		AdminUserId:       adminUserId,
		AdminReason:       adminReason,
		EventId:           transactionId,
		TwitchUserId:      testUserId,
		TypeOfBitsToAdd:   bitType,
	}

	MockPachinkoAddBits(0, errors.New("Test Pachinko Error"))

	err := ab.Process(ctx, req)

	assert.Equal(t, 500, err.(http.HttpError).StatusCode())
}

func TestAddBitsHappy(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	ab := SetupAddBits()

	req := apimodel.AddBitsRequest{
		AmountOfBitsToAdd: 1,
		AdminUserId:       adminUserId,
		AdminReason:       adminReason,
		EventId:           transactionId,
		TwitchUserId:      testUserId,
		TypeOfBitsToAdd:   bitType,
	}

	MockPachinkoAddBits(7, nil)

	err := ab.Process(ctx, req)

	assert.Nil(t, err)
}

func TestAddBitsBalanceMismatch(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	ab := SetupAddBits()

	req := apimodel.AddBitsRequest{
		AmountOfBitsToAdd: 1,
		AdminUserId:       adminUserId,
		AdminReason:       adminReason,
		EventId:           transactionId,
		TwitchUserId:      testUserId,
		TypeOfBitsToAdd:   bitType,
	}

	MockPachinkoAddBits(5, nil)
	err := ab.Process(ctx, req)

	// Sleep for just a brief moment so that the async process can catch up
	time.Sleep(50 * time.Millisecond)

	assert.Nil(t, err)
}

func TestAddBitsIdempotency(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	quanity := int64(1)

	thresholdBreachMock := new(add_bits_mock.ThresholdBreachProcessor)

	// intentionally sparsely populated processor should fail if any actual operations come around.
	td := new(dynamo_mock.ITransactionsDao)
	ab := &ProcessorImpl{
		Validator:                &ValidatorImpl{},
		ThresholdBreachProcessor: thresholdBreachMock,
		TransactionsDao:          td,
	}
	td.On("Get", mock.Anything).Return(&dynamo.Transaction{
		TransactionId: transactionId,
		UserId:        &testUserId,
		BitsType:      &bitType,
		NumberOfBits:  &quanity,
		AdminID:       &adminUserId,
		AdminReason:   &adminReason,
	}, nil)
	thresholdBreachMock.On("Process", mock.Anything, mock.Anything).Return(nil)

	req := apimodel.AddBitsRequest{
		AmountOfBitsToAdd: 1,
		AdminUserId:       adminUserId,
		AdminReason:       adminReason,
		EventId:           transactionId,
		TwitchUserId:      testUserId,
		TypeOfBitsToAdd:   bitType,
	}
	err := ab.Process(ctx, req)
	assert.Nil(t, err)
}

func TestAddBitsIdempotencyError(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	thresholdBreachMock := new(add_bits_mock.ThresholdBreachProcessor)

	td := new(dynamo_mock.ITransactionsDao)
	statter := new(metrics_mock.Statter)
	ab := &ProcessorImpl{
		Validator:                &ValidatorImpl{},
		ThresholdBreachProcessor: thresholdBreachMock,
		TransactionsDao:          td,
		Statter:                  statter,
	}
	td.On("Get", mock.Anything).Return(&dynamo.Transaction{}, nil)
	statter.On("Inc", "AdminAdd_Bad_Idempotency_Error", int64(1))
	thresholdBreachMock.On("Process", mock.Anything, mock.Anything).Return(nil)

	req := apimodel.AddBitsRequest{
		AmountOfBitsToAdd: 1,
		AdminUserId:       adminUserId,
		AdminReason:       adminReason,
		EventId:           transactionId,
		TwitchUserId:      testUserId,
		TypeOfBitsToAdd:   bitType,
	}
	err := ab.Process(ctx, req)

	// Sleep for just a brief moment so that the async process can catch up
	time.Sleep(50 * time.Millisecond)

	assert.Equal(t, badIdempotencyError(), err)
	statter.AssertCalled(t, "Inc", "AdminAdd_Bad_Idempotency_Error", int64(1))
}
