package add_bits

import (
	"context"
	"errors"
	"testing"
	"time"

	dart "code.justin.tv/amzn/TwitchDartReceiverTwirp"
	"code.justin.tv/commerce/payday/dynamo"
	dart_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/amzn/TwitchDartReceiverTwirp"
	transactions_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/transactions"
	apimodel "code.justin.tv/commerce/payday/models/api"
	"code.justin.tv/commerce/payday/utils/pointers"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

const (
	testTwitchUserId  = "12345"
	testAdminID       = "some-admin-id"
	testAdminReason   = "some-excuse"
	testTransactionID = "some-transaction-id"
)

func TestThresholdBreachProcessor_Process(t *testing.T) {
	Convey("Given a threshold breach processor", t, func() {
		ctx := context.Background()
		transactionFetcherMock := new(transactions_mock.Fetcher)
		dartMock := new(dart_mock.Receiver)
		thresholdBreachProcessor := &ThresholdBreachProcessorImpl{
			TransactionsFetcher: transactionFetcherMock,
			DartReceiver:        dartMock,
		}
		addBitsRequest := apimodel.AddBitsRequest{
			TwitchUserId: testTwitchUserId,
			AdminUserId:  testAdminID,
			AdminReason:  testAdminReason,
			EventId:      testTransactionID,
		}

		Convey("When there is no breach", func() {
			transactionFetcherMock.On("FetchTransactionsByUserIDAndTransactionType", ctx, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return([]*dynamo.Transaction{}, nil)
			addBitsRequest.AmountOfBitsToAdd = 1
			err := thresholdBreachProcessor.Process(ctx, addBitsRequest)
			So(err, ShouldBeNil)
			So(dartMock.AssertNumberOfCalls(t, "PublishNotification", 0), ShouldBeTrue)

			addBitsRequest.AmountOfBitsToAdd = oneTimeBreachThreshold
			err = thresholdBreachProcessor.Process(ctx, addBitsRequest)
			So(err, ShouldBeNil)
			So(dartMock.AssertNumberOfCalls(t, "PublishNotification", 0), ShouldBeTrue)
		})

		Convey("When there is one time breach", func() {
			transactionFetcherMock.On("FetchTransactionsByUserIDAndTransactionType", ctx, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return([]*dynamo.Transaction{}, nil)
			dartMock.On("PublishNotification", ctx, mock.Anything).
				Return(&dart.PublishNotificationResponse{}, nil)
			addBitsRequest.AmountOfBitsToAdd = oneTimeBreachThreshold + 1
			err := thresholdBreachProcessor.Process(ctx, addBitsRequest)
			So(err, ShouldBeNil)
			So(dartMock.AssertNumberOfCalls(t, "PublishNotification", 1), ShouldBeTrue)
		})

		Convey("When there is cumulative breach", func() {
			transactionFetcherMock.On("FetchTransactionsByUserIDAndTransactionType", ctx, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return([]*dynamo.Transaction{
				{
					TimeOfEvent:  time.Now(),
					NumberOfBits: pointers.Int64P(cumulativeBreachThreshold7Days),
				},
			}, nil)

			dartMock.On("PublishNotification", ctx, mock.Anything).
				Return(&dart.PublishNotificationResponse{}, nil)
			addBitsRequest.AmountOfBitsToAdd = 1
			err := thresholdBreachProcessor.Process(ctx, addBitsRequest)
			So(err, ShouldBeNil)
			So(dartMock.AssertNumberOfCalls(t, "PublishNotification", 1), ShouldBeTrue)
		})

		Convey("When there are both one-time and cumulative breaches", func() {
			transactionFetcherMock.On("FetchTransactionsByUserIDAndTransactionType", ctx, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return([]*dynamo.Transaction{
				{
					TimeOfEvent:  time.Now(),
					NumberOfBits: pointers.Int64P(cumulativeBreachThreshold7Days),
				},
			}, nil)
			dartMock.On("PublishNotification", ctx, mock.Anything).
				Return(&dart.PublishNotificationResponse{}, nil)
			addBitsRequest.AmountOfBitsToAdd = oneTimeBreachThreshold + 1
			err := thresholdBreachProcessor.Process(ctx, addBitsRequest)
			So(err, ShouldBeNil)
			So(dartMock.AssertNumberOfCalls(t, "PublishNotification", 2), ShouldBeTrue)
		})

		Convey("When searching transactions errors", func() {
			transactionFetcherMock.On("FetchTransactionsByUserIDAndTransactionType", ctx, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return([]*dynamo.Transaction{}, errors.New("ERROR"))

			addBitsRequest.AmountOfBitsToAdd = 123
			err := thresholdBreachProcessor.Process(ctx, addBitsRequest)
			So(err, ShouldNotBeNil)
		})

		Convey("When Dart errors", func() {
			transactionFetcherMock.On("FetchTransactionsByUserIDAndTransactionType", ctx, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return([]*dynamo.Transaction{}, nil)

			dartMock.On("PublishNotification", ctx, mock.Anything).
				Return(nil, errors.New("DART ERROR"))
			addBitsRequest.AmountOfBitsToAdd = oneTimeBreachThreshold + 1
			err := thresholdBreachProcessor.Process(ctx, addBitsRequest)
			So(err, ShouldNotBeNil)
		})
	})
}
