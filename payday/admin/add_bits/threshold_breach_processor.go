package add_bits

import (
	"context"
	"strconv"
	"time"

	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/transactions"

	dart "code.justin.tv/amzn/TwitchDartReceiverTwirp"
	log "code.justin.tv/commerce/logrus"
	apimodel "code.justin.tv/commerce/payday/models/api"
)

const (
	// We need to send internal email(s) on Bits Grant threshold breach for SOX compliance
	// Dart notification campaign:
	// https://dart.osiris.xarth.tv/campaigns/view/06efeb01-9c08-4ebc-b0c7-35b4b0ae9c0c
	// Dart staging campaign:
	// https://lcs-stage.osiris.xarth.tv/campaigns/view/50bfae39-9bf4-4b84-812d-57fe427601cc
	dartFieldThresholdBreachType   = "breach_type"
	dartFieldGrantedUserID         = "granted_user_id"
	dartFieldGrantAmount           = "grant_amount"
	dartFieldAdminID               = "admin_id"
	dartFieldAdminReason           = "admin_reason"
	dartFieldTransactionID         = "tid"
	dartFieldTotalAmountLast7Days  = "total_amount_last_7_days"
	dartFieldTotalAmountLast30Days = "total_amount_last_30_days"
	dartFieldGrantCountLast7Days   = "grant_count_last_7_days"
	dartFieldGrantCountLast30Days  = "grant_count_last_30_days"
	dartFieldBCC                   = "bcc"

	// Dart Values
	dartCampaignID           = "admin_bits_grant_threshold_breach"
	dartOneTimeBreachType    = "One Time"
	dartCumulativeBreachType = "Cumulative"
	dartMonitorEmail         = "internal-bits-grant-alerts@justin.tv" // https://groups.google.com/a/justin.tv/g/internal-bits-grant-alerts
	dartMonitorEmailLocale   = "en"
	twitchEmailDomain        = "@justin.tv"

	// Breach Thresholds
	oneTimeBreachThreshold          = 10000
	cumulativeBreachThreshold7Days  = 30000
	cumulativeBreachThreshold30Days = 100000

	// Helper consts
	daysBefore = -24 * time.Hour
)

type ThresholdBreachProcessor interface {
	Process(ctx context.Context, request apimodel.AddBitsRequest) error
}

type ThresholdBreachProcessorImpl struct {
	DartReceiver        dart.Receiver        `inject:""`
	TransactionsFetcher transactions.Fetcher `inject:""`
}

type notifyPayload struct {
	UserID                string
	GrantAmount           int
	AdminID               string
	GrantReason           string
	TransactionID         string
	TotalAmountLast7Days  int
	GrantCountLast7Days   int
	TotalAmountLast30Days int
	GrantCountLast30Days  int
}

func (t *ThresholdBreachProcessorImpl) Process(ctx context.Context, request apimodel.AddBitsRequest) error {
	payload := notifyPayload{
		UserID:        request.TwitchUserId,
		GrantAmount:   int(request.AmountOfBitsToAdd),
		AdminID:       request.AdminUserId,
		GrantReason:   request.AdminReason,
		TransactionID: request.EventId,
	}

	endTime := time.Now()
	startTime := endTime.Add(30 * daysBefore)

	grantsLast30Days, err := t.getPastGrants(ctx, request.TwitchUserId, endTime, startTime)
	if err != nil {
		return err
	}

	payload.TotalAmountLast7Days = payload.GrantAmount
	payload.GrantCountLast7Days = 1
	payload.TotalAmountLast30Days = payload.GrantAmount
	payload.GrantCountLast30Days = len(grantsLast30Days) + 1
	for _, grant := range grantsLast30Days {
		// Should not be possible. All transactions of type AdminAddBits have a NumberOfBits value.
		if grant.NumberOfBits == nil {
			continue
		}

		bitsAmount := int(*grant.NumberOfBits)

		payload.TotalAmountLast30Days += bitsAmount

		// tally last 7 day grants
		if grant.TimeOfEvent.After(endTime.Add(7 * daysBefore)) {
			payload.TotalAmountLast7Days += bitsAmount
			payload.GrantCountLast7Days++
		}
	}

	if payload.GrantAmount > oneTimeBreachThreshold {
		err := t.notifyBreach(ctx, payload, true)
		if err != nil {
			return err
		}
	}

	if payload.TotalAmountLast7Days > cumulativeBreachThreshold7Days ||
		payload.TotalAmountLast30Days > cumulativeBreachThreshold30Days {
		err := t.notifyBreach(ctx, payload, false)
		if err != nil {
			return err
		}
	}

	return nil
}

func (t *ThresholdBreachProcessorImpl) getPastGrants(ctx context.Context, userID string, eventBefore time.Time, eventAfter time.Time) ([]*dynamo.Transaction, error) {
	return t.TransactionsFetcher.FetchTransactionsByUserIDAndTransactionType(ctx, userID, models.AdminAddBitsTransactionType, 1000, true, &eventBefore, &eventAfter)
}

func (t *ThresholdBreachProcessorImpl) notifyBreach(ctx context.Context, payload notifyPayload, isOneTimeBreach bool) error {
	var breachType string
	if isOneTimeBreach {
		breachType = dartOneTimeBreachType
	} else {
		breachType = dartCumulativeBreachType
	}

	dartParams := &dart.PublishNotificationRequest{
		Recipient: &dart.PublishNotificationRequest_RecipientEmailAddressWithLocale{
			RecipientEmailAddressWithLocale: &dart.RecipientEmailAddressWithLocale{
				Email:  dartMonitorEmail,
				Locale: dartMonitorEmailLocale,
			},
		},
		NotificationType: dartCampaignID,
		Metadata: map[string]string{
			dartFieldThresholdBreachType:   breachType,
			dartFieldGrantedUserID:         payload.UserID,
			dartFieldGrantAmount:           strconv.Itoa(payload.GrantAmount),
			dartFieldAdminID:               payload.AdminID,
			dartFieldAdminReason:           payload.GrantReason,
			dartFieldTransactionID:         payload.TransactionID,
			dartFieldTotalAmountLast7Days:  strconv.Itoa(payload.TotalAmountLast7Days),
			dartFieldGrantCountLast7Days:   strconv.Itoa(payload.GrantCountLast7Days),
			dartFieldTotalAmountLast30Days: strconv.Itoa(payload.TotalAmountLast30Days),
			dartFieldGrantCountLast30Days:  strconv.Itoa(payload.GrantCountLast30Days),
			dartFieldBCC:                   payload.AdminID + twitchEmailDomain,
		},
	}

	log.WithFields(log.Fields{
		"dartCampaign": dartCampaignID,
		"payload":      dartParams.Metadata,
	}).Info("Publishing notification to dart")

	_, err := t.DartReceiver.PublishNotification(ctx, dartParams)
	if err != nil {
		return err
	}

	return nil
}
