package add_bits

import (
	"net/http"
	"strconv"

	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/payday/bulk_grant"
	httperror "code.justin.tv/commerce/payday/errors/http"
	"code.justin.tv/commerce/payday/models"
	apimodel "code.justin.tv/commerce/payday/models/api"
)

type Validator interface {
	Validate(request apimodel.AddBitsRequest) error
}

type ValidatorImpl struct {
	BulkGrantCampaignsGetter bulk_grant.CampaignsGetter `inject:""`
}

func (v *ValidatorImpl) Validate(request apimodel.AddBitsRequest) error {
	if strings.Blank(request.BulkGrantCampaignID) {
		err := v.validateRegularRequest(request)
		if err != nil {
			return err
		}
	} else {
		err := v.validateBulkGrantCampaignRequest(request)
		if err != nil {
			return err
		}
	}

	if strings.Blank(request.EventId) {
		return httperror.New("EventId required", http.StatusBadRequest)
	}
	_, err := strconv.Atoi(request.TwitchUserId)
	if err != nil {
		return httperror.New("Numeric twitchUserId required", http.StatusBadRequest)
	}
	if request.NotifyUser && strings.Blank(request.Origin) {
		return httperror.New("Origin required if NotifyUser is true", http.StatusBadRequest)
	}

	return nil
}

func (v *ValidatorImpl) validateBulkGrantCampaignRequest(request apimodel.AddBitsRequest) error {
	campaign := v.BulkGrantCampaignsGetter.GetCampaign(request.BulkGrantCampaignID)
	if campaign == nil {
		return httperror.New("BulkGrantCampaignID not found", http.StatusBadRequest)
	}
	if request.AmountOfBitsToAdd > 0 {
		return httperror.New("AmountOfBitsToAdd should not be set for a Bulk Grant Campaign", http.StatusBadRequest)
	}
	if !strings.Blank(request.AdminReason) {
		return httperror.New("AdminReason should not be set for a Bulk Grant Campaign", http.StatusBadRequest)
	}
	if !strings.Blank(request.Origin) {
		return httperror.New("Origin should not be set for a Bulk Grant Campaign", http.StatusBadRequest)
	}
	if !strings.Blank(request.TypeOfBitsToAdd) {
		return httperror.New("TypeOfBitsToAdd should not be set for a Bulk Grant Campaign", http.StatusBadRequest)
	}
	return nil
}

func (v *ValidatorImpl) validateRegularRequest(request apimodel.AddBitsRequest) error {
	if request.AmountOfBitsToAdd < 0 {
		return httperror.New("Positive addBits amount required", http.StatusBadRequest)
	}
	if strings.Blank(request.AdminUserId) {
		return httperror.New("AdminUserId required", http.StatusBadRequest)
	}
	if strings.Blank(request.AdminReason) {
		return httperror.New("AdminReason required", http.StatusBadRequest)
	}
	err := models.ValidateBitsType(request.TypeOfBitsToAdd)
	if err != nil {
		return httperror.NewWithCause("Invalid bits type", http.StatusBadRequest, err)
	}
	return nil
}
