package message

import (
	"context"
	"testing"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/actions"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/message/tokenizer"
	"code.justin.tv/commerce/payday/models/api"
)

type ITokenizerFactory interface {
	NewTokenizer(ctx context.Context, channelId string, userId string) (tokenizer.ITokenizer, error)
}

type TokenizerFactory struct {
	ActionRetriever actions.Retriever `inject:"chain"`
}

func (tf *TokenizerFactory) NewTokenizer(ctx context.Context, channelId string, userId string) (tokenizer.ITokenizer, error) {
	acts, err := tf.ActionRetriever.GetActions(ctx, api.GetActionsRequest{ChannelId: channelId, UserId: userId, IncludeUpperTiers: true, IncludeSponsored: true})
	if err != nil {
		return nil, err
	}

	tk, err := tokenizer.NewTokenizer(acts)
	if err != nil {
		msg := "Could not create cheermote tokenizer"
		log.WithError(err).Error(msg)
		return nil, errors.Notef(err, msg)
	}

	return tk, nil
}

func NewTestTokenizerFactory(t *testing.T, prefixes ...string) ITokenizerFactory {
	chain := NewTestHandlerChain(prefixes...)
	return &TokenizerFactory{
		ActionRetriever: chain,
	}
}
