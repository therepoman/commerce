package tokenizer

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/anonymous"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/models/api"
)

const bitsValueRegex = `\d+\s*$`
const tagRegex = `(^|\s+)#\S+`
const numberCutset = "0123456789"
const bitsEventRegexTemplate = `(?i)^\s*%s\d+\s*$`
const bonusCheermote = " bonus"

var bitsValueRegexp *regexp.Regexp
var tagRegexp *regexp.Regexp

type RegexMap map[string]*regexp.Regexp

func init() {
	var err error
	bitsValueRegexp, err = regexp.Compile(bitsValueRegex)
	if err != nil {
		panic(err)
	}

	tagRegexp, err = regexp.Compile(tagRegex)
	if err != nil {
		panic(err)
	}
}

func ExtractBitsTokenEmote(token string) string {
	token = strings.TrimSpace(token)
	emote := strings.TrimRight(token, numberCutset)
	emote = strings.ToLower(emote)
	return emote
}

func ExtractBitsTokenValue(token string) (int64, error) {
	valueStr := bitsValueRegexp.FindString(token)
	valueStr = strings.TrimSpace(valueStr)

	value, err := strconv.ParseInt(valueStr, 10, 64)
	if err != nil {
		errorMsg := "Error parsing string value into integer for bits token"
		log.WithError(err).Error(errorMsg)
		return 0, errors.Notef(err, errorMsg)
	}
	return value, nil
}

func GetRegexMap(actions []api.Action) (RegexMap, error) {
	prefixToRegex := make(RegexMap)
	for _, action := range actions {
		regex, err := regexp.Compile(fmt.Sprintf(bitsEventRegexTemplate, action.Prefix))
		if err != nil {
			msg := "Error building regex for prefix"
			log.WithError(err).WithField("prefix", action.Prefix).Error(msg)
			return nil, errors.Notef(err, msg)
		}
		prefixToRegex[action.Prefix] = regex
	}

	return prefixToRegex, nil
}

type ITokenizer interface {
	Tokenize(msg string) (total int64, allTokenValues []int64, emoteTotals map[string]int64, outErr error)
	TokenizeWithEmoteOrder(msg string) (total int64, allTokenValues []int64, emoteOrder []string, emoteTotals map[string]int64, outErr error)
	TokenizeWithEmoteOrderAndIncludesChecks(msg string) (total int64, allTokenValues []int64, emoteOrder []string, emoteTotals map[string]int64, includesNonBitsToken bool, includesDisplayOnlyAction bool, outErr error)
	IsBitsToken(token string) bool
	SanitizeMessage(msg string) string
	InsertSponsoredCheermoteBonus(msg string, emoteTotals map[string]models.BitEmoteTotal) string
	StripText(msg string) string
	Anonymize(msg string) (string, error)
}

type tokenizer struct {
	prefixToRegex map[string]*regexp.Regexp
	actionsMap    map[string]api.Action
}

func NewTokenizer(actions []api.Action) (ITokenizer, error) {
	regexMap, err := GetRegexMap(actions)
	if err != nil {
		return nil, err
	}
	actionsMap := make(map[string]api.Action)
	for _, action := range actions {
		actionsMap[strings.ToLower(action.Prefix)] = action
	}

	return &tokenizer{
		prefixToRegex: regexMap,
		actionsMap:    actionsMap,
	}, nil
}

func (t *tokenizer) Tokenize(msg string) (total int64, allTokenValues []int64, emoteTotals map[string]int64, outErr error) {
	total, allTokenValues, _, emoteTotals, err := t.TokenizeWithEmoteOrder(msg)

	return total, allTokenValues, emoteTotals, err
}

func (t *tokenizer) TokenizeWithEmoteOrder(msg string) (total int64, allTokenValues []int64, emoteOrder []string, emoteTotals map[string]int64, outErr error) {
	total, allTokenValues, emoteOrder, emoteTotals, _, _, outErr = t.TokenizeWithEmoteOrderAndIncludesChecks(msg)
	return total, allTokenValues, emoteOrder, emoteTotals, outErr
}

func (t *tokenizer) TokenizeWithEmoteOrderAndIncludesChecks(msg string) (total int64, allTokenValues []int64, emoteOrder []string, emoteTotals map[string]int64, includesNonBitsToken bool, includesDisplayOnlyAction bool, outErr error) {
	total = int64(0)
	allTokenValues = make([]int64, 0)
	emoteOrder = make([]string, 0)
	emoteTotals = make(map[string]int64)
	includesNonBitsToken = false
	includesDisplayOnlyAction = false
	for _, token := range strings.Fields(msg) {
		if t.IsBitsToken(token) {
			emote := ExtractBitsTokenEmote(token)
			action := t.actionsMap[emote]
			if action.Type == api.DisplayOnlyAction && emote != anonymous.AnonymousCheermotePrefix /* TODO: Remove this once we make an anonymous action type */ {
				includesDisplayOnlyAction = true
			}
			value, err := ExtractBitsTokenValue(token)
			if err != nil {
				msg := "Error extracting value from bits token"
				log.WithError(err).WithField("token", token).Error(msg)
				return 0, []int64{}, []string{}, map[string]int64{}, false, false, errors.Notef(err, msg)
			}
			emoteTotals[emote] += value
			total += value
			allTokenValues = append(allTokenValues, value)
			emoteOrder = append(emoteOrder, emote)
		} else {
			includesNonBitsToken = true
		}
	}
	return total, allTokenValues, emoteOrder, emoteTotals, includesNonBitsToken, includesDisplayOnlyAction, nil
}

func (t *tokenizer) IsBitsToken(token string) bool {
	for _, regex := range t.prefixToRegex {
		if regex.MatchString(token) {
			return true
		}
	}
	return false
}

// Replace non-standard whitespace with spaces and force bits event strings to lowercase
func (t *tokenizer) SanitizeMessage(msg string) string {
	sanitizedMsg := ""
	sep := ""
	for i, token := range strings.Fields(msg) {
		if i > 0 {
			sep = " "
		}

		if t.IsBitsToken(token) {
			token = strings.ToLower(token)
		}

		sanitizedMsg += sep + token
	}
	return sanitizedMsg
}

func (t *tokenizer) InsertSponsoredCheermoteBonus(msg string, emoteTotals map[string]models.BitEmoteTotal) string {
	updatedMessage := msg

	// n squared, cool good thing the main logic is only run one time per sponsored cheermote.
	for prefix, total := range emoteTotals {
		var lastIndex int
		var lastIndexLength int

		if total.MatchedTotal > 0 {
			for _, token := range strings.Fields(updatedMessage) {
				// toLower to prevent from having to worry about case.
				if t.IsBitsToken(token) && strings.HasPrefix(strings.ToLower(token), strings.ToLower(prefix)) {
					lastIndex = strings.LastIndex(updatedMessage, token)
					lastIndexLength = len(token)
				}
			}

			// make some substrings, to make the last line less hideous, also useful when debugging
			before := updatedMessage[0:(lastIndex + lastIndexLength)]
			after := updatedMessage[(lastIndex + lastIndexLength):]

			updatedMessage = before + bonusCheermote + strconv.Itoa(int(total.MatchedTotal)) + after
		}
	}
	return updatedMessage
}

// Strip all text content, leaving only the cheer strings
func (t *tokenizer) StripText(msg string) string {
	sanitizedMsg := t.SanitizeMessage(msg)

	strippedMsg := ""
	sep := ""
	for i, token := range strings.Fields(sanitizedMsg) {
		if i > 0 && strippedMsg != "" {
			sep = " "
		}

		if t.IsBitsToken(token) {
			strippedMsg += sep + token
		}
	}

	return strippedMsg
}

func (t *tokenizer) Anonymize(msg string) (string, error) {
	_, allTokenValues, emoteOrder, _, err := t.TokenizeWithEmoteOrder(msg)
	if err != nil {
		return "", err
	}

	anonymousMessageParts := make([]string, len(emoteOrder))
	for i := range emoteOrder {
		anonymousMessageParts[i] = fmt.Sprintf("%s%d", anonymous.AnonymousCheermotePrefix, allTokenValues[i])
	}

	return strings.Join(anonymousMessageParts, " "), nil
}
