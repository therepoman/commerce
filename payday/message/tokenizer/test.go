package tokenizer

import (
	"testing"

	"code.justin.tv/commerce/payday/models/api"
	"github.com/stretchr/testify/require"
)

func NewDefaultTestTokenizer(t *testing.T) ITokenizer {
	var actions []api.Action
	actions = append(actions, api.Action{Prefix: "cheer"})
	return NewTestTokenizer(t, actions)
}

func NewTestTokenizer(t *testing.T, actions []api.Action) ITokenizer {
	tok, err := NewTokenizer(actions)
	require.NoError(t, err)
	return tok
}
