package tokenizer

import (
	"testing"

	"code.justin.tv/commerce/payday/anonymous"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/models/api"
	"github.com/stretchr/testify/assert"
)

func TestIsBitsToken_Empty(t *testing.T) {
	assert.Equal(t, false, NewDefaultTestTokenizer(t).IsBitsToken(""))
}

func TestIsBitsToken_Whitespace(t *testing.T) {
	assert.Equal(t, false, NewDefaultTestTokenizer(t).IsBitsToken("\r\t \n"))
}

func TestIsBitsToken_Typo(t *testing.T) {
	assert.Equal(t, false, NewDefaultTestTokenizer(t).IsBitsToken("cher1"))
}

func TestIsBitsToken_NoNum(t *testing.T) {
	assert.Equal(t, false, NewDefaultTestTokenizer(t).IsBitsToken("cheer"))
}

func TestIsBitsToken_ValidSimple(t *testing.T) {
	assert.Equal(t, true, NewDefaultTestTokenizer(t).IsBitsToken("cheer1"))
}

func TestIsBitsToken_ValidLarge(t *testing.T) {
	assert.Equal(t, true, NewDefaultTestTokenizer(t).IsBitsToken("cheer999999999999999999999"))
}

func TestIsBitsToken_ValidWhitespace(t *testing.T) {
	assert.Equal(t, true, NewDefaultTestTokenizer(t).IsBitsToken("\r \n\t\tcheer123\t \r\n"))
}

func TestIsBitsToken_ValidNonStandardCase(t *testing.T) {
	assert.Equal(t, true, NewDefaultTestTokenizer(t).IsBitsToken("ChEeR123"))
}

func TestExtractBitsTokenValue_Empty(t *testing.T) {
	_, err := ExtractBitsTokenValue("")
	assert.Error(t, err)
}

func TestExtractBitsTokenValue_Whitespace(t *testing.T) {
	_, err := ExtractBitsTokenValue("\r\n \t")
	assert.Error(t, err)
}

func TestExtractBitsTokenValue_NoNum(t *testing.T) {
	_, err := ExtractBitsTokenValue("cheer")
	assert.Error(t, err)
}

func TestExtractBitsTokenValue_JustNum(t *testing.T) {
	val, err := ExtractBitsTokenValue("123")
	assert.NoError(t, err)
	assert.Equal(t, int64(123), val)
}

func TestExtractBitsTokenValue_Cheer(t *testing.T) {
	val, err := ExtractBitsTokenValue("cheer456")
	assert.NoError(t, err)
	assert.Equal(t, int64(456), val)
}

func TestExtractBitsTokenValue_ComplexCheer(t *testing.T) {
	val, err := ExtractBitsTokenValue("\r\n\t ChEeR789\t \r\n")
	assert.NoError(t, err)
	assert.Equal(t, int64(789), val)
}

func TestExtractBitsTokenValue_PrefixWithNumber(t *testing.T) {
	val, err := ExtractBitsTokenValue("s3ph123")
	assert.NoError(t, err)
	assert.Equal(t, int64(123), val)
}

func TestExtractBitsTokenValue_PrefixWithNumberEndingWithWhitespace(t *testing.T) {
	val, err := ExtractBitsTokenValue("s3ph456 \t \r   \n")
	assert.NoError(t, err)
	assert.Equal(t, int64(456), val)
}

func TestExtractBitsTokenEmote_Empty(t *testing.T) {
	assert.Equal(t, "", ExtractBitsTokenEmote(""))
}

func TestExtractBitsTokenEmote_Whitespace(t *testing.T) {
	assert.Equal(t, "", ExtractBitsTokenEmote("   \n \r \n\n"))
}

func TestExtractBitsTokenEmote_Emote(t *testing.T) {
	assert.Equal(t, "cheer", ExtractBitsTokenEmote("cheer123"))
}

func TestExtractBitsTokenEmote_ComplexEmote(t *testing.T) {
	assert.Equal(t, "kappa", ExtractBitsTokenEmote(" \r \n KaPpA999 \r \n"))
}
func TestExtractBitsTokenEmote_NumberStart(t *testing.T) {
	assert.Equal(t, "4head", ExtractBitsTokenEmote(" \r \n 4head100 \r \n"))
}

func TestExtractBitsTokenEmote_NumberMiddle(t *testing.T) {
	assert.Equal(t, "test123test", ExtractBitsTokenEmote(" \r \n test123test100 \r \n"))
}

func TestWithMultipleActions(t *testing.T) {
	actions := []api.Action{{Prefix: "cheer"}, {Prefix: "kappa"}, {Prefix: "biblethump"}, {Prefix: "somethingelse"},
		{Prefix: "yetanotheremote"}, {Prefix: "givebits"}, {Prefix: "showlove"}, {Prefix: "etcetc"}}
	tokenizer := NewTestTokenizer(t, actions)
	assert.True(t, tokenizer.IsBitsToken("cHeEr1"))
	assert.True(t, tokenizer.IsBitsToken("KaPPa9001"))
	assert.True(t, tokenizer.IsBitsToken("bIbLeThUmP1010110111"))
	assert.False(t, tokenizer.IsBitsToken("biblethumper420"))

	val, err := ExtractBitsTokenValue("cHeEr1")
	assert.NoError(t, err)
	assert.Equal(t, int64(1), val)

	val, err = ExtractBitsTokenValue("KaPPa9001")
	assert.NoError(t, err)
	assert.Equal(t, int64(9001), val)

	val, err = ExtractBitsTokenValue("bIbLeThUmP1010110111")
	assert.NoError(t, err)
	assert.Equal(t, int64(1010110111), val)
}

func TestSanitizeMessage_Empty(t *testing.T) {
	assert.Equal(t, "", NewDefaultTestTokenizer(t).SanitizeMessage(""))
}

func TestSanitizeMessage_WhitespaceOnly(t *testing.T) {
	assert.Equal(t, "", NewDefaultTestTokenizer(t).SanitizeMessage("\r\n \t"))
}

func TestSanitizeMessage_WhitespaceWithNoise(t *testing.T) {
	assert.Equal(t, "A B C D", NewDefaultTestTokenizer(t).SanitizeMessage("\r\rA\n\nB\t\tC  D"))
}

func TestSanitizeMessage_MessageWithSomeNormalCheers(t *testing.T) {
	assert.Equal(t, "cheer1 cheer2 cheer3 test", NewDefaultTestTokenizer(t).SanitizeMessage("\r\r\rcheer1\t\tcheer2  \t  cheer3\r test \r\n"))
}

func TestSanitizeMessage_MessageWithSomeNonStandardCasingCheers(t *testing.T) {
	assert.Equal(t, "cheer1 cheer2 cheer3 test", NewDefaultTestTokenizer(t).SanitizeMessage("\r\r\rcHeEr1\t\tCHEER2  \t  ChEEr3\r test \r\n"))
}

func TestSanitizeMessage_MessageWithMultipleActions(t *testing.T) {
	actions := []api.Action{{Prefix: "cheer"}, {Prefix: "kappa"}, {Prefix: "biblethump"}, {Prefix: "somethingelse"},
		{Prefix: "yetanotheremote"}, {Prefix: "givebits"}, {Prefix: "showlove"}, {Prefix: "etcetc"}}
	tokenizer := NewTestTokenizer(t, actions)
	assert.Equal(t, "cheer1 kappa2 biblethump3 BIBLETHUMPER10 BIBLETHIMPEST100 kappa4 testTest", tokenizer.SanitizeMessage("\r\r\rcHeEr1\t\tKAPPA2  \t  bIbLETHUMP3\r BIBLETHUMPER10 BIBLETHIMPEST100 kApPa4 testTest\r\n"))
}

func TestStripText_MessageWithNormalCheers(t *testing.T) {
	assert.Equal(t, "cheer1 cheer2 cheer3", NewDefaultTestTokenizer(t).StripText("VoHiYo \t\r\r cheer1 PogChamp cheer2 \t FrankerZ cheer3 HeyGuys"))
}

func TestStripText_MessageWithMultipleActions(t *testing.T) {
	actions := []api.Action{{Prefix: "cheer"}, {Prefix: "kappa"}, {Prefix: "biblethump"}, {Prefix: "slaye"}}
	tokenizer := NewTestTokenizer(t, actions)
	assert.Equal(t, "cheer1 kappa2 biblethump3 slaye100", tokenizer.StripText("cheer1 HeyGuys \t kappa2 \t This \r\r\t    is a test biblethump3 VoHiYo slaye100"))
}

func TestSponsoredBonusWithLeadingMessage(t *testing.T) {
	tokenizer := NewTestTokenizer(t, []api.Action{{Prefix: "brand"}, {Prefix: "kappa"}})
	updatedString := tokenizer.InsertSponsoredCheermoteBonus("SO HAPPY brand500", map[string]models.BitEmoteTotal{
		"brand": {
			MatchedTotal: 100,
		},
	})
	assert.Equal(t, "SO HAPPY brand500 bonus100", updatedString)
}

func TestSponsoredBonusWithSimpleSingleLine(t *testing.T) {
	tokenizer := NewTestTokenizer(t, []api.Action{{Prefix: "brand"}, {Prefix: "kappa"}})
	updatedString := tokenizer.InsertSponsoredCheermoteBonus("brand500 kappa250", map[string]models.BitEmoteTotal{
		"brand": {
			MatchedTotal: 100,
		},
	})
	assert.Equal(t, "brand500 bonus100 kappa250", updatedString)
}

func TestSponsoredBonusWithSimpleNewLine(t *testing.T) {
	tokenizer := NewTestTokenizer(t, []api.Action{{Prefix: "brand"}, {Prefix: "kappa"}})
	updatedString := tokenizer.InsertSponsoredCheermoteBonus("brand150\nkappa250", map[string]models.BitEmoteTotal{
		"brand": {
			MatchedTotal: 15,
		},
	})
	assert.Equal(t, "brand150 bonus15\nkappa250", updatedString)
}

func TestSponsoredBonusWithSimpleTab(t *testing.T) {
	tokenizer := NewTestTokenizer(t, []api.Action{{Prefix: "brand"}, {Prefix: "kappa"}})
	updatedString := tokenizer.InsertSponsoredCheermoteBonus("brand200	kappa250", map[string]models.BitEmoteTotal{
		"brand": {
			MatchedTotal: 20,
		},
	})
	assert.Equal(t, "brand200 bonus20	kappa250", updatedString)
}

func TestSponsoredBonusWithTwoUsages(t *testing.T) {
	tokenizer := NewTestTokenizer(t, []api.Action{{Prefix: "brand"}, {Prefix: "kappa"}})
	updatedString := tokenizer.InsertSponsoredCheermoteBonus("brand200 brand150 kappa250", map[string]models.BitEmoteTotal{
		"brand": {
			MatchedTotal: 35,
		},
	})
	assert.Equal(t, "brand200 brand150 bonus35 kappa250", updatedString)
}

func TestSponsoredBonusWithOnlyBonusCheermote(t *testing.T) {
	tokenizer := NewTestTokenizer(t, []api.Action{{Prefix: "brand"}, {Prefix: "kappa"}})
	updatedString := tokenizer.InsertSponsoredCheermoteBonus("brand20", map[string]models.BitEmoteTotal{
		"brand": {
			MatchedTotal: 2,
		},
	})
	assert.Equal(t, "brand20 bonus2", updatedString)
}

// this also tests substring matching, since both brand cheermotes end with brand#
func TestSponsoredBonusWithTwoSponsoredCheermotes(t *testing.T) {
	tokenizer := NewTestTokenizer(t, []api.Action{{Prefix: "brand"}, {Prefix: "coolerBrand"}, {Prefix: "kappa"}})
	updatedString := tokenizer.InsertSponsoredCheermoteBonus("brand8 coolerBrand50", map[string]models.BitEmoteTotal{
		"brand": {
			MatchedTotal: 1,
		},
		"coolerBrand": {
			MatchedTotal: 50,
		},
	})
	assert.Equal(t, "brand8 bonus1 coolerBrand50 bonus50", updatedString)
}

func TestSponsoredBonusWithMixedCasing(t *testing.T) {
	tokenizer := NewTestTokenizer(t, []api.Action{{Prefix: "brand"}, {Prefix: "kappa"}})
	updatedString := tokenizer.InsertSponsoredCheermoteBonus("BRAND20", map[string]models.BitEmoteTotal{
		"brand": {
			MatchedTotal: 2,
		},
	})
	assert.Equal(t, "BRAND20 bonus2", updatedString)
}

func TestContainsCheermoteNameOutsideOfCheer(t *testing.T) {
	tokenizer := NewTestTokenizer(t, []api.Action{{Prefix: "brand"}, {Prefix: "kappa"}})
	updatedString := tokenizer.InsertSponsoredCheermoteBonus("brand5 I like this brand ya know", map[string]models.BitEmoteTotal{
		"brand": {
			MatchedTotal: 1,
		},
	})
	assert.Equal(t, "brand5 bonus1 I like this brand ya know", updatedString)
}

func TestSponsoredBonusWithNoSponsoredCheermote(t *testing.T) {
	tokenizer := NewTestTokenizer(t, []api.Action{{Prefix: "brand"}, {Prefix: "kappa"}})
	updatedString := tokenizer.InsertSponsoredCheermoteBonus("kappa20", map[string]models.BitEmoteTotal{})
	assert.Equal(t, "kappa20", updatedString)
}

func TestAnonymize(t *testing.T) {
	tokenizer := NewTestTokenizer(t, []api.Action{{Prefix: anonymous.AnonymousCheermotePrefix}, {Prefix: "cheer"}, {Prefix: "kappa"}})
	anonymizedText, _ := tokenizer.Anonymize("kappa20 some text blah blah cheer200 anon100")
	assert.Equal(t, "anon20 anon200 anon100", anonymizedText)
}
