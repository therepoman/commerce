package message

import (
	"context"

	"code.justin.tv/commerce/payday/actions"
	"code.justin.tv/commerce/payday/actions/handlers"
	"code.justin.tv/commerce/payday/models/api"
)

func NewTestHandlerChain(prefixes ...string) actions.Retriever {
	chainLink := func(next actions.Retriever) actions.Retriever {
		actionList := []api.Action{}
		for _, a := range prefixes {
			actionList = append(actionList, api.Action{
				Prefix: a,
			})
		}

		return actions.RetrieverFunc(func(ctx context.Context, request api.GetActionsRequest) ([]api.Action, error) {
			return actionList, nil
		})
	}

	return new(handlers.HandlerChain).WithHandler(chainLink)
}
