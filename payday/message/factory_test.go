package message_test

import (
	"context"
	"testing"

	"code.justin.tv/commerce/payday/message"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

var factory *message.TokenizerFactory

func setup() {
	chain := message.NewTestHandlerChain("cheer")
	factory = &message.TokenizerFactory{
		ActionRetriever: chain,
	}
}

func TestEmoteTokenizationErrorFromPrefix(t *testing.T) {
	setup()
	tokenizer, err := factory.NewTokenizer(context.Background(), "channel", "user")
	require.NoError(t, err)
	total, _, emoteTotal, err := tokenizer.Tokenize("Cheer100 nice play!!! PogChamp")
	assert.NoError(t, err)
	assert.Equal(t, int64(100), total)
	assert.Equal(t, int64(100), emoteTotal["cheer"])
}
