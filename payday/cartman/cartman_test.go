package cartman

import (
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
	"time"

	"code.justin.tv/commerce/payday/cartman/decoder/rsa"
	"code.justin.tv/commerce/payday/cartman/decoder/selector"
	"code.justin.tv/commerce/payday/errors"
	user_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/user"
	"code.justin.tv/commerce/payday/test"
	"code.justin.tv/commerce/payday/utils/pointers"
	"code.justin.tv/common/goauthorization"
	auth "code.justin.tv/common/goauthorization"
	"code.justin.tv/web/users-service/models"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

const (
	testPublicKey         = "testdata/id_rsa.pub"
	iPhoneClientID        = "85lcqzxpb9bqu9z6ga1ol55du"
	nonFirstPartyClientID = "rwxuvbt5pg5tnogl3jab1ngorywa5s0"
)

var enc auth.Encoder
var dec auth.Decoder
var decoderSelector selector.DecoderSelector

func setup() {
	var err error
	enc, err = auth.NewEncoder("RS256", "code.justin.tv/web/cartman", test.ReadFile("testdata/id_rsa.pem"))

	if err != nil {
		panic(err)
	}

	dec, err = auth.NewDecoder("RS256", "code.justin.tv/commerce/payday", "code.justin.tv/web/cartman", test.ReadFile(testPublicKey))
	if err != nil {
		panic(err)
	}

	decoderSelector = selector.NewAlgorithmAwareDecoderSelector(map[string]map[string]goauthorization.Decoder{
		selector.PaydayAudience: {
			rsa.CartmanAlgorithm: dec,
		},
	})
}

func getCapabilities() auth.CapabilityClaims {
	capClaim := map[string]auth.CapValue{"channel": "dathass", "user_id": 123}
	return map[string]auth.CapabilityClaim{"spectre::edit_channel": capClaim}
}

func generateToken(claim auth.CapabilityClaims) *auth.AuthorizationToken {
	params := auth.TokenParams{
		Exp:    time.Now().Add(10 * time.Minute),
		Nbf:    time.Now(),
		Aud:    []string{"code.justin.tv/commerce/payday"},
		Sub:    "dianers",
		Claims: claim,
	}

	return enc.Encode(params)
}

func generateOAuthTokenWithSubject(claim auth.CapabilityClaims, clientId string, subject string, firstParty bool) *auth.AuthorizationToken {
	params := auth.TokenParams{
		Exp:      time.Now().Add(10 * time.Minute),
		Nbf:      time.Now(),
		Aud:      []string{"code.justin.tv/commerce/payday"},
		Sub:      subject,
		Claims:   claim,
		ClientID: clientId,
		Fpc:      firstParty, // indicates first party
	}

	return enc.Encode(params)
}

func generateOAuthToken(claim auth.CapabilityClaims, clientId string, firstParty bool) *auth.AuthorizationToken {
	return generateOAuthTokenWithSubject(claim, clientId, "dianers", firstParty)
}

func getTargetUserCapabilities() auth.CapabilityClaims {
	capClaim := map[string]auth.CapValue{"target_user_id": "78631695"}
	return map[string]auth.CapabilityClaim{"get_user_info": capClaim}
}

func TestMain(m *testing.M) {
	setup()
	os.Exit(m.Run())
}

func TestHappy(t *testing.T) {
	capabilities := getCapabilities()
	token := generateToken(capabilities)
	tokenString, _ := token.String()

	val, err := NewValidatorWithClients(decoderSelector, nil)
	if err != nil {
		assert.NoError(t, err)
		return
	}

	request := httptest.NewRequest("", "/path", nil)
	request.Header.Set(TwitchAuthHeader, tokenString)

	err = val.Validate(request, &capabilities)
	assert.Nil(t, err, "Received error when validating the cartman token")
}

func TestOAuthHappy(t *testing.T) {
	capabilities := getTargetUserCapabilities()
	token := generateOAuthToken(capabilities, iPhoneClientID, true)
	tokenString, _ := token.String()

	val, err := NewValidatorWithClients(decoderSelector, nil)
	if err != nil {
		assert.NoError(t, err)
		return
	}

	request := httptest.NewRequest("", "/path", nil)
	request.Header.Set(TwitchAuthHeader, tokenString)

	err = val.Validate(request, &capabilities)

	assert.NoError(t, err, "Received error when validating the cartman token")
}

func TestOAuthInvalid(t *testing.T) {
	capabilities := getTargetUserCapabilities()
	token := generateOAuthToken(capabilities, nonFirstPartyClientID, true)
	tokenString, _ := token.String()

	val, err := NewValidatorWithClients(decoderSelector, nil)
	if err != nil {
		assert.NoError(t, err)
		return
	}

	request := httptest.NewRequest("", "/path", nil)
	request.Header.Set(TwitchAuthHeader, tokenString)

	err = val.Validate(request, &capabilities)
	assert.Error(t, err, "Did not receive error when validating a cartman token from unsupported client")
}

func testRequest(authenticatedUserID string, clientID string) *http.Request {
	capabilities := getTargetUserCapabilities()
	token := generateOAuthTokenWithSubject(capabilities, clientID, authenticatedUserID, clientID == iPhoneClientID)
	tokenString, _ := token.String()
	req := httptest.NewRequest("", "/path", nil)
	req.Header.Set(TwitchAuthHeader, tokenString)
	return req
}

func TestValidateAuthenticatedUserID(t *testing.T) {
	Convey("Given a validator", t, func() {
		userClient := new(user_mock.IUserServiceClientWrapper)
		val, err := NewValidatorWithClients(decoderSelector, userClient)
		So(err, ShouldBeNil)

		requireFirstParty := false
		allowAdminOverride := false
		reqUserID := "12345"
		authedUserID := "12345"
		clientID := nonFirstPartyClientID

		Convey("Errors when token fails to validate", func() {
			req := httptest.NewRequest("", "/path", nil)
			req.Header.Set(TwitchAuthHeader, "nothing but trash")
			err = val.ValidateAuthenticatedUserID(req, reqUserID, requireFirstParty, allowAdminOverride)
			So(err, ShouldNotBeNil)
		})

		Convey("Succeeds when userID matches authenticated userID", func() {
			req := testRequest(authedUserID, clientID)
			err = val.ValidateAuthenticatedUserID(req, reqUserID, requireFirstParty, allowAdminOverride)
			So(err, ShouldBeNil)
		})

		Convey("When first party is required", func() {
			requireFirstParty = true

			Convey("Errors when called by a non-first party client", func() {
				req := testRequest(authedUserID, clientID)
				err = val.ValidateAuthenticatedUserID(req, reqUserID, requireFirstParty, allowAdminOverride)
				So(err, ShouldNotBeNil)
			})

			Convey("When request comes from a first party client ID", func() {
				clientID = iPhoneClientID

				Convey("Succeeds when userID matches authenticated userID", func() {
					req := testRequest(authedUserID, clientID)
					err = val.ValidateAuthenticatedUserID(req, reqUserID, requireFirstParty, allowAdminOverride)
					So(err, ShouldBeNil)
				})

				Convey("When userID does not match authenticated userID", func() {
					reqUserID := "12345"
					authedUserID := "98765"

					Convey("Errors when admin override is not allowed", func() {
						req := testRequest(authedUserID, clientID)
						err = val.ValidateAuthenticatedUserID(req, reqUserID, requireFirstParty, allowAdminOverride)
						So(err, ShouldNotBeNil)
						So(len(userClient.Calls), ShouldEqual, 0)
					})

					Convey("When admin override is allowed", func() {
						allowAdminOverride = true

						Convey("Errors when user service call fails", func() {
							userClient.On("GetUserByID", mock.Anything, authedUserID, mock.Anything).Return(nil, errors.New("test error"))
							req := testRequest(authedUserID, clientID)
							err = val.ValidateAuthenticatedUserID(req, reqUserID, requireFirstParty, allowAdminOverride)
							So(err, ShouldNotBeNil)
						})

						Convey("Errors when user service call returns nil", func() {
							userClient.On("GetUserByID", mock.Anything, authedUserID, mock.Anything).Return(nil, nil)
							req := testRequest(authedUserID, clientID)
							err = val.ValidateAuthenticatedUserID(req, reqUserID, requireFirstParty, allowAdminOverride)
							So(err, ShouldNotBeNil)
						})

						Convey("Errors when user service call returns nil admin field", func() {
							userClient.On("GetUserByID", mock.Anything, authedUserID, mock.Anything).Return(&models.Properties{
								Admin: nil,
							}, nil)
							req := testRequest(authedUserID, clientID)
							err = val.ValidateAuthenticatedUserID(req, reqUserID, requireFirstParty, allowAdminOverride)
							So(err, ShouldNotBeNil)
						})

						Convey("Errors when user service call returns non admin", func() {
							userClient.On("GetUserByID", mock.Anything, authedUserID, mock.Anything).Return(&models.Properties{
								Admin: pointers.BoolP(false),
							}, nil)
							req := testRequest(authedUserID, clientID)
							err = val.ValidateAuthenticatedUserID(req, reqUserID, requireFirstParty, allowAdminOverride)
							So(err, ShouldNotBeNil)
						})

						Convey("Succeeds when user service call returns admin", func() {
							userClient.On("GetUserByID", mock.Anything, authedUserID, mock.Anything).Return(&models.Properties{
								Admin: pointers.BoolP(true),
							}, nil)
							req := testRequest(authedUserID, clientID)
							err = val.ValidateAuthenticatedUserID(req, reqUserID, requireFirstParty, allowAdminOverride)
							So(err, ShouldBeNil)
						})
					})
				})
			})
		})
	})
}
