package hmac

import (
	"code.justin.tv/commerce/payday/cartman/decoder"
	"code.justin.tv/commerce/payday/clients/sandstorm"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/common/goauthorization"
)

const CartmanAlgorithm = "HS512"

func NewDecoder(sand sandstorm.SecretGetter, audience string) (goauthorization.Decoder, error) {
	hmacSecret, err := sand.Get(config.Get().SandstormCartmanHMACSecretKey)
	if err != nil || hmacSecret == nil {
		return nil, errors.Notef(err, "could not get HMAC secret key from sandstorm")
	}
	return decoder.NewDecoder(CartmanAlgorithm, audience, hmacSecret)
}
