package selector

import (
	"net/http"

	"code.justin.tv/commerce/payday/cartman/decoder/ecc"
	"code.justin.tv/commerce/payday/cartman/decoder/hmac"
	"code.justin.tv/commerce/payday/cartman/decoder/noop"
	"code.justin.tv/commerce/payday/cartman/decoder/rsa"
	"code.justin.tv/commerce/payday/clients/sandstorm"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/utils/strings"
	"code.justin.tv/common/goauthorization"
	"code.justin.tv/common/jwt"
)

const (
	TwitchAuthorizationHeader = "Twitch-Authorization"
	PaydayAudience            = "code.justin.tv/commerce/payday"
	CartmanAudience           = "code.justin.tv/web/cartman"
)

type DecoderSelector interface {
	SelectDecoderFromRequest(r *http.Request) (goauthorization.Decoder, error)
	SelectDecoderFromToken(t *goauthorization.AuthorizationToken) (goauthorization.Decoder, error)
}

type NoopDecoderSelector struct {
}

func (nods *NoopDecoderSelector) SelectDecoderFromRequest(r *http.Request) (goauthorization.Decoder, error) {
	return &noop.NoopDecoder{}, nil
}

func (nods *NoopDecoderSelector) SelectDecoderFromToken(t *goauthorization.AuthorizationToken) (goauthorization.Decoder, error) {
	return &noop.NoopDecoder{}, nil
}

type AlgorithmAwareDecoderSelector struct {
	audienceToAlgorithmToDecoder map[string]map[string]goauthorization.Decoder
}

func NewAlgorithmAwareDecoderSelector(audienceToAlgorithmToDecoder map[string]map[string]goauthorization.Decoder) DecoderSelector {
	return &AlgorithmAwareDecoderSelector{
		audienceToAlgorithmToDecoder: audienceToAlgorithmToDecoder,
	}
}

func NewDefaultAlgorithmAwareDecoderSelector(sand sandstorm.SecretGetter) (DecoderSelector, error) {
	if config.Get().Environment == config.DevelopmentEnvironment {
		return &NoopDecoderSelector{}, nil
	}

	paydayRSADecoder, err := rsa.NewDecoder(sand, PaydayAudience)
	if err != nil {
		return nil, errors.Notef(err, "error creating rsa decoder for decoder selector")
	}

	cartmanRSADecoder, err := rsa.NewDecoder(sand, CartmanAudience)
	if err != nil {
		return nil, errors.Notef(err, "error creating rsa decoder for decoder selector")
	}

	paydayECCDecoder, err := ecc.NewDecoder(sand, PaydayAudience)
	if err != nil {
		return nil, errors.Notef(err, "error creating ecc decoder for decoder selector")
	}

	cartmanECCDecoder, err := ecc.NewDecoder(sand, CartmanAudience)
	if err != nil {
		return nil, errors.Notef(err, "error creating ecc decoder for decoder selector")
	}

	paydayHMACDecoder, err := hmac.NewDecoder(sand, PaydayAudience)
	if err != nil {
		return nil, errors.Notef(err, "error creating hmac decoder for decoder selector")
	}

	cartmanHMACDecoder, err := hmac.NewDecoder(sand, CartmanAudience)
	if err != nil {
		return nil, errors.Notef(err, "error creating hmac decoder for decoder selector")
	}

	return NewAlgorithmAwareDecoderSelector(map[string]map[string]goauthorization.Decoder{
		PaydayAudience: {
			rsa.CartmanAlgorithm:  paydayRSADecoder,
			ecc.CartmanAlgorithm:  paydayECCDecoder,
			hmac.CartmanAlgorithm: paydayHMACDecoder,
		},
		CartmanAudience: {
			rsa.CartmanAlgorithm:  cartmanRSADecoder,
			ecc.CartmanAlgorithm:  cartmanECCDecoder,
			hmac.CartmanAlgorithm: cartmanHMACDecoder,
		},
	}), nil
}

func (aads *AlgorithmAwareDecoderSelector) getAlgorithmToDecoderMapFromAudiences(audiences []string) (map[string]goauthorization.Decoder, error) {
	for _, audience := range audiences {
		algorithmToDecoder, isPresent := aads.audienceToAlgorithmToDecoder[audience]
		if isPresent {
			return algorithmToDecoder, nil
		}
	}
	return make(map[string]goauthorization.Decoder), errors.Newf("none of the audiences provided are supported (%v)", audiences)
}

func (aads *AlgorithmAwareDecoderSelector) SelectDecoderFromRequest(r *http.Request) (goauthorization.Decoder, error) {
	authHeader := r.Header.Get(TwitchAuthorizationHeader)
	if strings.Blank(authHeader) {
		return nil, errors.New("request is missing Twitch-Authorization header")
	}

	var token jwt.Fragment
	var err error
	if token, err = jwt.Parse([]byte(authHeader)); err != nil {
		return nil, errors.Notef(err, "error parsing JWT from Twitch-Authorization header")
	}

	var tokenHeader jwt.Header
	var tokenClaims goauthorization.TokenClaims
	if err = token.Decode(&tokenHeader, &tokenClaims); err != nil {
		return nil, errors.Notef(err, "error decoding JWT")
	}

	algorithmToDecoder, err := aads.getAlgorithmToDecoderMapFromAudiences(tokenClaims.Audience)
	if err != nil {
		return nil, errors.Notef(err, "error getting algorithmToDecoder map from audiences")
	}

	decoder, isPresent := algorithmToDecoder[tokenHeader.Algorithm]
	if !isPresent {
		return nil, errors.Newf("algorithm %s not supported", tokenHeader.Algorithm)
	}

	return decoder, nil
}

func (aads *AlgorithmAwareDecoderSelector) SelectDecoderFromToken(t *goauthorization.AuthorizationToken) (goauthorization.Decoder, error) {
	if t == nil {
		return nil, errors.Newf("received a nil token")
	}

	algorithmToDecoder, err := aads.getAlgorithmToDecoderMapFromAudiences(t.Claims.Audience)
	if err != nil {
		return nil, errors.Notef(err, "error getting algorithmToDecoder map from audiences")
	}

	decoder, isPresent := algorithmToDecoder[t.Algorithm.Name()]
	if !isPresent {
		return nil, errors.Newf("algorithm %s not supported", t.Algorithm.Name())
	}

	return decoder, nil

}
