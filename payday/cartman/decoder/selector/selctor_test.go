package selector_test

import (
	"net/http"
	"strings"
	"testing"

	"code.justin.tv/commerce/payday/cartman/decoder/ecc"
	"code.justin.tv/commerce/payday/cartman/decoder/hmac"
	"code.justin.tv/commerce/payday/cartman/decoder/rsa"
	"code.justin.tv/commerce/payday/cartman/decoder/selector"
	goauthorization_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/common/goauthorization"
	"code.justin.tv/common/goauthorization"
	"code.justin.tv/common/jwt"
	. "github.com/smartystreets/goconvey/convey"
)

func testRequest(authHeader string) *http.Request {
	req, err := http.NewRequest("GET", "http://www.someurl.com", strings.NewReader("body"))
	So(err, ShouldBeNil)

	if authHeader != "" {
		req.Header.Add(selector.TwitchAuthorizationHeader, authHeader)
	}
	return req
}

func testPaydayAudience() []string {
	return []string{selector.PaydayAudience}
}

func testCartmanAudience() []string {
	return []string{selector.CartmanAudience}
}

func testJWT(a jwt.Algorithm, audiences []string) string {
	header := jwt.NewHeader(a)
	claims := goauthorization.TokenClaims{
		Audience: audiences,
	}
	jwtBytes, err := jwt.Encode(header, claims, a)
	So(err, ShouldBeNil)
	return string(jwtBytes)
}

func testRSA512Alg() jwt.Algorithm {
	pk, err := jwt.ReadRSAPrivateKey("../../testdata/id_rsa.pem")
	So(err, ShouldBeNil)
	return jwt.RS512(pk)
}

func testRSA256Alg() jwt.Algorithm {
	pk, err := jwt.ReadRSAPrivateKey("../../testdata/id_rsa.pem")
	So(err, ShouldBeNil)
	return jwt.RS256(pk)
}

func testECCAlg() jwt.Algorithm {
	pk, err := jwt.ReadECDSAPrivateKey("../../testdata/ec256.pem")
	So(err, ShouldBeNil)
	return jwt.ES256(pk)
}

func testHMACAlg() jwt.Algorithm {
	return jwt.HS512([]byte("super secret hmac secret"))
}

func testGoAuthToken(a jwt.Algorithm, audiences []string) *goauthorization.AuthorizationToken {
	return &goauthorization.AuthorizationToken{
		Header:    jwt.NewHeader(a),
		Algorithm: a,
		Claims: goauthorization.TokenClaims{
			Audience: audiences,
		},
	}
}

func TestAlgorithmAwareDecoderSelector_SelectDecoderFromRequest(t *testing.T) {
	Convey("Given several decoders", t, func() {

		paydayRSADecoder := new(goauthorization_mock.Decoder)
		paydayECCDecoder := new(goauthorization_mock.Decoder)
		paydayHMACDecoder := new(goauthorization_mock.Decoder)

		cartmanRSADecoder := new(goauthorization_mock.Decoder)
		cartmanECCDecoder := new(goauthorization_mock.Decoder)
		cartmanHMACDecoder := new(goauthorization_mock.Decoder)

		Convey("Create an algorithm aware decoder selector", func() {
			aads := selector.NewAlgorithmAwareDecoderSelector(map[string]map[string]goauthorization.Decoder{
				selector.PaydayAudience: {
					rsa.CartmanAlgorithm:  paydayRSADecoder,
					ecc.CartmanAlgorithm:  paydayECCDecoder,
					hmac.CartmanAlgorithm: paydayHMACDecoder,
				},
				selector.CartmanAudience: {
					rsa.CartmanAlgorithm:  cartmanRSADecoder,
					ecc.CartmanAlgorithm:  cartmanECCDecoder,
					hmac.CartmanAlgorithm: cartmanHMACDecoder,
				},
			})

			Convey("Errors when Twitch-Authorization header is missing", func() {
				req := testRequest("")
				dec, err := aads.SelectDecoderFromRequest(req)
				So(dec, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})

			Convey("Errors when Twitch-Authorization header is invalid JWT", func() {
				req := testRequest("this is not a valid JWT")
				dec, err := aads.SelectDecoderFromRequest(req)
				So(dec, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})

			Convey("Errors for JWT encoded with unsupported algorithm", func() {
				req := testRequest(testJWT(testRSA512Alg(), testPaydayAudience()))
				dec, err := aads.SelectDecoderFromRequest(req)
				So(dec, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})

			Convey("Errors for JWT with unknown audience", func() {
				req := testRequest(testJWT(testRSA256Alg(), []string{"this is not a valid audience"}))
				dec, err := aads.SelectDecoderFromRequest(req)
				So(dec, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})

			Convey("Given a payday audience", func() {
				audience := testPaydayAudience()

				Convey("Returns RSA decoder for corresponding RSA encoded JWT", func() {
					req := testRequest(testJWT(testRSA256Alg(), audience))
					dec, err := aads.SelectDecoderFromRequest(req)
					So(dec, ShouldEqual, paydayRSADecoder)
					So(err, ShouldBeNil)
				})

				Convey("Returns ECC decoder for corresponding ECC encoded JWT", func() {
					req := testRequest(testJWT(testECCAlg(), audience))
					dec, err := aads.SelectDecoderFromRequest(req)
					So(dec, ShouldEqual, paydayECCDecoder)
					So(err, ShouldBeNil)
				})

				Convey("Returns HMAC decoder for corresponding HMAC encoded JWT", func() {
					req := testRequest(testJWT(testHMACAlg(), audience))
					dec, err := aads.SelectDecoderFromRequest(req)
					So(dec, ShouldEqual, paydayHMACDecoder)
					So(err, ShouldBeNil)
				})
			})

			Convey("Given a cartman audience", func() {
				audience := testCartmanAudience()

				Convey("Returns RSA decoder for corresponding RSA encoded JWT", func() {
					req := testRequest(testJWT(testRSA256Alg(), audience))
					dec, err := aads.SelectDecoderFromRequest(req)
					So(dec, ShouldEqual, cartmanRSADecoder)
					So(err, ShouldBeNil)
				})

				Convey("Returns ECC decoder for corresponding ECC encoded JWT", func() {
					req := testRequest(testJWT(testECCAlg(), audience))
					dec, err := aads.SelectDecoderFromRequest(req)
					So(dec, ShouldEqual, cartmanECCDecoder)
					So(err, ShouldBeNil)
				})

				Convey("Returns HMAC decoder for corresponding HMAC encoded JWT", func() {
					req := testRequest(testJWT(testHMACAlg(), audience))
					dec, err := aads.SelectDecoderFromRequest(req)
					So(dec, ShouldEqual, cartmanHMACDecoder)
					So(err, ShouldBeNil)
				})
			})
		})
	})
}

func TestAlgorithmAwareDecoderSelector_SelectDecoderFromToken(t *testing.T) {
	Convey("Given several decoders", t, func() {

		paydayRSADecoder := new(goauthorization_mock.Decoder)
		paydayECCDecoder := new(goauthorization_mock.Decoder)
		paydayHMACDecoder := new(goauthorization_mock.Decoder)

		cartmanRSADecoder := new(goauthorization_mock.Decoder)
		cartmanECCDecoder := new(goauthorization_mock.Decoder)
		cartmanHMACDecoder := new(goauthorization_mock.Decoder)

		Convey("Create an algorithm aware decoder selector", func() {
			aads := selector.NewAlgorithmAwareDecoderSelector(map[string]map[string]goauthorization.Decoder{
				selector.PaydayAudience: {
					rsa.CartmanAlgorithm:  paydayRSADecoder,
					ecc.CartmanAlgorithm:  paydayECCDecoder,
					hmac.CartmanAlgorithm: paydayHMACDecoder,
				},
				selector.CartmanAudience: {
					rsa.CartmanAlgorithm:  cartmanRSADecoder,
					ecc.CartmanAlgorithm:  cartmanECCDecoder,
					hmac.CartmanAlgorithm: cartmanHMACDecoder,
				},
			})

			Convey("Errors for a nil token", func() {
				dec, err := aads.SelectDecoderFromToken(nil)
				So(dec, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})

			Convey("Errors for token with unsupported algorithm", func() {
				token := testGoAuthToken(testRSA512Alg(), testPaydayAudience())
				dec, err := aads.SelectDecoderFromToken(token)
				So(dec, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})

			Convey("Errors for token with unsupported audience", func() {
				token := testGoAuthToken(testRSA256Alg(), []string{"this is not a valid audience"})
				dec, err := aads.SelectDecoderFromToken(token)
				So(dec, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})

			Convey("Given a payday audience", func() {
				audience := testPaydayAudience()

				Convey("Returns RSA decoder for corresponding RSA token", func() {
					token := testGoAuthToken(testRSA256Alg(), audience)
					dec, err := aads.SelectDecoderFromToken(token)
					So(dec, ShouldEqual, paydayRSADecoder)
					So(err, ShouldBeNil)
				})

				Convey("Returns ECC decoder for corresponding ECC token", func() {
					token := testGoAuthToken(testECCAlg(), audience)
					dec, err := aads.SelectDecoderFromToken(token)
					So(dec, ShouldEqual, paydayECCDecoder)
					So(err, ShouldBeNil)
				})

				Convey("Returns HMAC decoder for corresponding HMAC token", func() {
					token := testGoAuthToken(testHMACAlg(), audience)
					dec, err := aads.SelectDecoderFromToken(token)
					So(dec, ShouldEqual, paydayHMACDecoder)
					So(err, ShouldBeNil)
				})
			})

			Convey("Given a cartman audience", func() {
				audience := testCartmanAudience()

				Convey("Returns RSA decoder for corresponding RSA token", func() {
					token := testGoAuthToken(testRSA256Alg(), audience)
					dec, err := aads.SelectDecoderFromToken(token)
					So(dec, ShouldEqual, cartmanRSADecoder)
					So(err, ShouldBeNil)
				})

				Convey("Returns ECC decoder for corresponding ECC token", func() {
					token := testGoAuthToken(testECCAlg(), audience)
					dec, err := aads.SelectDecoderFromToken(token)
					So(dec, ShouldEqual, cartmanECCDecoder)
					So(err, ShouldBeNil)
				})

				Convey("Returns HMAC decoder for corresponding HMAC token", func() {
					token := testGoAuthToken(testHMACAlg(), audience)
					dec, err := aads.SelectDecoderFromToken(token)
					So(dec, ShouldEqual, cartmanHMACDecoder)
					So(err, ShouldBeNil)
				})
			})
		})
	})
}
