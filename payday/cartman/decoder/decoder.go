package decoder

import (
	"code.justin.tv/commerce/payday/cartman/decoder/noop"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/common/goauthorization"
	"code.justin.tv/systems/sandstorm/manager"
)

func NewDecoder(alg string, audience string, secret *manager.Secret) (goauthorization.Decoder, error) {
	var dec goauthorization.Decoder
	var err error

	environment := config.Get().Environment

	if environment != config.DevelopmentEnvironment {
		dec, err = goauthorization.NewDecoder(alg, audience, "code.justin.tv/web/cartman", secret.Plaintext)
		if err != nil {
			return nil, errors.Notef(err, "error creating cartman decoder")
		}
	} else {
		dec = &noop.NoopDecoder{}
	}
	return dec, nil
}
