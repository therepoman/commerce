package rsa

import (
	"code.justin.tv/commerce/payday/cartman/decoder"
	"code.justin.tv/commerce/payday/clients/sandstorm"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/common/goauthorization"
)

const CartmanAlgorithm = "RS256"

func NewDecoder(sand sandstorm.SecretGetter, audience string) (goauthorization.Decoder, error) {
	rsaSecret, err := sand.Get(config.Get().SandstormCartmanRSAPublicKey)
	if err != nil || rsaSecret == nil {
		return nil, errors.Notef(err, "could not get RSA public key from sandstorm")
	}
	return decoder.NewDecoder(CartmanAlgorithm, audience, rsaSecret)
}
