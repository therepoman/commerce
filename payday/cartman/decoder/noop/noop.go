package noop

import (
	"net/http"

	"code.justin.tv/common/goauthorization"
)

type NoopDecoder struct {
}

func (nod *NoopDecoder) Decode(string) (*goauthorization.AuthorizationToken, error) {
	return nil, nil
}

func (nod *NoopDecoder) ParseToken(*http.Request) (*goauthorization.AuthorizationToken, error) {
	return nil, nil
}

func (nod *NoopDecoder) Validate(*goauthorization.AuthorizationToken, goauthorization.CapabilityClaims) error {
	return nil
}
