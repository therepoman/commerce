package ecc

import (
	"code.justin.tv/commerce/payday/cartman/decoder"
	"code.justin.tv/commerce/payday/clients/sandstorm"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/common/goauthorization"
)

const CartmanAlgorithm = "ES256"

func NewDecoder(sand sandstorm.SecretGetter, audience string) (goauthorization.Decoder, error) {
	eccSecret, err := sand.Get(config.Get().SandstormCartmanECCPublicKey)
	if err != nil || eccSecret == nil {
		return nil, errors.Notef(err, "could not get ECC public key from sandstorm")
	}
	return decoder.NewDecoder(CartmanAlgorithm, audience, eccSecret)
}
