package cartman

import (
	"context"
	"net/http"
	"net/http/httputil"

	"time"

	logger "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/cartman/decoder/selector"
	"code.justin.tv/commerce/payday/clients/user"
	"code.justin.tv/commerce/payday/errors"
	http_errors "code.justin.tv/commerce/payday/errors/http"
	"code.justin.tv/commerce/payday/middleware"
	"code.justin.tv/commerce/payday/oauth"
	"code.justin.tv/commerce/payday/utils/pointers"
	goauth "code.justin.tv/common/goauthorization"
)

const (
	unauthorizedErrorMessage   = "Could not authorize request"
	internalServerErrorMessage = "Internal server error"
	TwitchAuthHeader           = "Twitch-Authorization"
)

type alwaysValidValidator struct{}

func (v *alwaysValidValidator) Validate(r *http.Request, capabilities *goauth.CapabilityClaims) error {
	return nil
}

func (v *alwaysValidValidator) ValidateAuthenticatedUserID(r *http.Request, userID string, requireFirstParty bool, allowAdminOverride bool) error {
	return nil
}

type validator struct {
	DecoderSelector   selector.DecoderSelector       `inject:""`
	UserServiceClient user.IUserServiceClientWrapper `inject:""`
}

type IValidator interface {
	Validate(r *http.Request, capabilities *goauth.CapabilityClaims) error
	ValidateAuthenticatedUserID(r *http.Request, userID string, requireFirstParty bool, allowAdminOverride bool) error
}

func NewAlwaysValidValidator() IValidator {
	return &alwaysValidValidator{}
}

func NewValidator() IValidator {
	return &validator{}
}

func NewValidatorWithClients(decoderSelector selector.DecoderSelector, userServiceClient user.IUserServiceClientWrapper) (IValidator, error) {
	if decoderSelector == nil {
		return nil, errors.New("Passed a nil decoder")
	}

	return &validator{
		DecoderSelector:   decoderSelector,
		UserServiceClient: userServiceClient,
	}, nil
}

func (val *validator) Validate(r *http.Request, capabilities *goauth.CapabilityClaims) error {
	_, err := val.getAndValidateToken(r, capabilities)
	return err
}

func (val *validator) getAndValidateToken(r *http.Request, capabilities *goauth.CapabilityClaims) (*goauth.AuthorizationToken, error) {
	if capabilities == nil || r == nil {
		logger.Warn("Validator was passed nil pointer")
		return nil, http_errors.New(unauthorizedErrorMessage, http.StatusUnauthorized)
	}

	log := logger.WithField("capabilities", *capabilities)
	if req, err := httputil.DumpRequest(r, true); err == nil {
		log = log.WithField("request", string(req))
	}

	v := r.Context().Value(middleware.CartmanTokenContextKey)
	token, ok := v.(*goauth.AuthorizationToken)

	var decoder goauth.Decoder
	var err error
	if !ok || token == nil {
		decoder, err = val.DecoderSelector.SelectDecoderFromRequest(r)
		if err != nil {
			return nil, http_errors.NewWithCause(unauthorizedErrorMessage, http.StatusUnauthorized, err)
		}

		token, err = decoder.ParseToken(r)
		if err != nil {
			log.WithError(err).Warn("Could not parse cartman token.")
			return nil, http_errors.NewWithCause(unauthorizedErrorMessage, http.StatusUnauthorized, err)
		}
	}

	if decoder == nil {
		decoder, err = val.DecoderSelector.SelectDecoderFromToken(token)
		if err != nil {
			return nil, http_errors.NewWithCause(unauthorizedErrorMessage, http.StatusUnauthorized, err)
		}
	}

	err = decoder.Validate(token, *capabilities)
	if err != nil {
		log.WithError(err).Warn("Could not validate cartman token.")
		return nil, http_errors.NewWithCause(unauthorizedErrorMessage, http.StatusUnauthorized, err)
	}

	_, whitelisted := oauth.GetClient(token.GetClientID())

	if token.IsFirstPartyClient() && !whitelisted {
		log.WithField("client_id", token.GetClientID()).Warn("Received a first party request from a non-authorized client")
		return nil, http_errors.New(unauthorizedErrorMessage, http.StatusUnauthorized)
	}

	return token, nil
}

func (val *validator) ValidateAuthenticatedUserID(r *http.Request, userID string, requireFirstParty bool, allowAdminOverride bool) error {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	token, err := val.getAndValidateToken(r, &goauth.CapabilityClaims{})
	if err != nil {
		return err
	}

	authenticatedUserID := ""
	if token != nil {
		authenticatedUserID = token.GetSubject()
	}

	if requireFirstParty && !token.Claims.FirstPartyClient {
		logger.WithFields(logger.Fields{
			"authenticatedUserID": authenticatedUserID,
			"userID":              userID,
		}).Warn("Received a third-party authenticated request for a first-party only API")
		return http_errors.New(unauthorizedErrorMessage, http.StatusUnauthorized)
	}

	if authenticatedUserID == userID {
		return nil
	}

	if allowAdminOverride {
		authenticatedUser, err := val.UserServiceClient.GetUserByID(ctx, authenticatedUserID, nil)
		if err != nil {
			logger.WithError(err).WithFields(logger.Fields{
				"authenticatedUserID": authenticatedUserID,
				"userID":              userID,
			}).Warn("Error calling user service for authenticated user")
			return http_errors.New(internalServerErrorMessage, http.StatusInternalServerError)
		}

		if authenticatedUser != nil && pointers.BoolOrDefault(authenticatedUser.Admin, false) {
			return nil
		}
	}

	logger.WithFields(logger.Fields{
		"authenticatedUserID": authenticatedUserID,
		"userID":              userID,
	}).Info("Authenticated userID does not match request userID")
	return http_errors.New(unauthorizedErrorMessage, http.StatusUnauthorized)
}
