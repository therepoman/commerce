package dynamo_test

import (
	"testing"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/campaigns"
	"code.justin.tv/commerce/payday/test"
	"github.com/stretchr/testify/assert"
)

func mergeSponsoredCheermoteCampaignRecords(existingCampaign *campaigns.SponsoredCheermoteCampaign, campaignModifications *campaigns.SponsoredCheermoteCampaign) *campaigns.SponsoredCheermoteCampaign {
	mergedCampaign := &campaigns.SponsoredCheermoteCampaign{
		CampaignID: existingCampaign.CampaignID,
		Prefix:     campaignModifications.Prefix,
		TotalBits:  campaignModifications.TotalBits,
		UsedBits:   campaignModifications.UsedBits,
		StartTime:  campaignModifications.StartTime,
		EndTime:    campaignModifications.EndTime,
		IsEnabled:  campaignModifications.IsEnabled,
	}
	return mergedCampaign
}

func TestSponsoredCheermoteCampaignsDao(t *testing.T) {
	if dynamoContext.SkipDynamoTests {
		t.Skip("Skipping Dynamo Tests")
	}
	sponsoredCheermoteCampaignsDao, _ := dynamoContext.CreateDefaultSponsoredCheermoteCampaignsDao("test-dao-complete")
	err := sponsoredCheermoteCampaignsDao.SetupTable()
	assert.NoError(t, err)

	now := time.Now()
	savedCampaigns := make(map[string]*campaigns.SponsoredCheermoteCampaign)
	for i := 0; i < numChannelsToTest; i++ {
		campaignID := test.RandomString(16)
		testSponsoredCheermoteCampaign := &campaigns.SponsoredCheermoteCampaign{
			CampaignID: campaignID,
			Prefix:     test.RandomString(10),
			TotalBits:  1000,
			UsedBits:   0,
			IsEnabled:  true,
		}
		err := sponsoredCheermoteCampaignsDao.Put(testSponsoredCheermoteCampaign)
		if err != nil {
			t.Fail()
		}
		savedCampaigns[campaignID] = testSponsoredCheermoteCampaign
	}

	log.Infof("Retrieving and verifying campaign records from dynamo")
	for campaignID, expectedCampaign := range savedCampaigns {
		actualCampaign, err := sponsoredCheermoteCampaignsDao.Get(campaignID)
		if err != nil {
			t.Fail()
		}
		assert.True(t, expectedCampaign.EqualsWithExpectedLastUpdated(actualCampaign, now, dynamoContext.AcceptableUpdateTimeDelta))
	}

	now = time.Now()
	log.Infof("Modifying existing campaign records")
	for existingCampaignID, existingSponsoredCheermoteCampaigns := range savedCampaigns {
		modifiedSponsoredCheermoteCampaigns := &campaigns.SponsoredCheermoteCampaign{
			CampaignID: existingCampaignID,
			Prefix:     test.RandomString(10),
			TotalBits:  1000,
			UsedBits:   100,
		}
		err := sponsoredCheermoteCampaignsDao.Put(modifiedSponsoredCheermoteCampaigns)
		if err != nil {
			t.Fail()
		}
		savedCampaigns[existingCampaignID] = mergeSponsoredCheermoteCampaignRecords(existingSponsoredCheermoteCampaigns, modifiedSponsoredCheermoteCampaigns)
	}

	log.Infof("Retrieving and verifying modified campaign records from dynamo")
	for campaignID, expectedSponsoredCheermoteCampaign := range savedCampaigns {
		actualCampaign, err := sponsoredCheermoteCampaignsDao.Get(campaignID)
		if err != nil {
			t.Fail()
		}
		assert.True(t, expectedSponsoredCheermoteCampaign.EqualsWithExpectedLastUpdated(actualCampaign, now, dynamoContext.AcceptableUpdateTimeDelta))
	}

	log.Info("Testing the atomic increment of bits used")
	for existingCampaignID, existingSponsoredCheermoteCampaigns := range savedCampaigns {
		modifiedSponsoredCheermoteCampaigns := &campaigns.SponsoredCheermoteCampaign{
			CampaignID: existingCampaignID,
			Prefix:     existingSponsoredCheermoteCampaigns.Prefix,
			TotalBits:  1000,
			UsedBits:   200,
		}

		err := sponsoredCheermoteCampaignsDao.IncrementBitsUsed(existingSponsoredCheermoteCampaigns, 100)
		if err != nil {
			t.Fail()
		}
		savedCampaigns[existingCampaignID] = mergeSponsoredCheermoteCampaignRecords(existingSponsoredCheermoteCampaigns, modifiedSponsoredCheermoteCampaigns)
	}

	log.Infof("Retrieving and verifying increment of bits used")
	for campaignID, expectedSponsoredCheermoteCampaign := range savedCampaigns {
		actualCampaign, err := sponsoredCheermoteCampaignsDao.Get(campaignID)
		if err != nil {
			t.Fail()
		}
		log.WithField("campaign", expectedSponsoredCheermoteCampaign).Info("expected")
		log.WithField("campaign", actualCampaign).Info("actual")
		assert.True(t, expectedSponsoredCheermoteCampaign.EqualsWithExpectedLastUpdated(actualCampaign, now, dynamoContext.AcceptableUpdateTimeDelta))
	}

	log.Info("Testing the atomic increment of bits used trying to go past the total")
	for existingCampaignID, existingSponsoredCheermoteCampaigns := range savedCampaigns {
		modifiedSponsoredCheermoteCampaigns := &campaigns.SponsoredCheermoteCampaign{
			CampaignID: existingCampaignID,
			Prefix:     existingSponsoredCheermoteCampaigns.Prefix,
			TotalBits:  1000,
			UsedBits:   200,
		}

		err := sponsoredCheermoteCampaignsDao.IncrementBitsUsed(existingSponsoredCheermoteCampaigns, 1000)
		if err == nil {
			t.Fail()
		}
		savedCampaigns[existingCampaignID] = mergeSponsoredCheermoteCampaignRecords(existingSponsoredCheermoteCampaigns, modifiedSponsoredCheermoteCampaigns)
	}

	log.Infof("Retrieving and verifying increment of bits used trying to go past the total")
	for campaignID, expectedSponsoredCheermoteCampaign := range savedCampaigns {
		actualCampaign, err := sponsoredCheermoteCampaignsDao.Get(campaignID)
		if err != nil {
			t.Fail()
		}
		log.WithField("campaign", expectedSponsoredCheermoteCampaign).Info("expected")
		log.WithField("campaign", actualCampaign).Info("actual")
		assert.True(t, expectedSponsoredCheermoteCampaign.EqualsWithExpectedLastUpdated(actualCampaign, now, dynamoContext.AcceptableUpdateTimeDelta))
	}

	log.Infof("Deleting campaign records from dynamo")
	for campaignID := range savedCampaigns {
		err := sponsoredCheermoteCampaignsDao.Delete(campaignID)
		assert.NoError(t, err)
	}

	log.Infof("Verifying that campaign records no longer exist in dynamo")
	for campaignID := range savedCampaigns {
		actualSponsoredCheermoteCampaign, err := sponsoredCheermoteCampaignsDao.Get(campaignID)
		if err != nil {
			t.Fail()
		}
		assert.Nil(t, actualSponsoredCheermoteCampaign)
	}
}
