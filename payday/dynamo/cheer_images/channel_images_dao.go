package cheer_images

import (
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/config"
	"github.com/aws/aws-sdk-go/aws/session"
	d "github.com/guregu/dynamo"
)

const (
	HashKey = "user_id"
	GSI     = "image_id"
	GSIName = "image_id-index"
)

type channelImagesDao struct {
	*d.Table
}

type ChannelImagesDao interface {
	GetImagesByChannelId(channelId string) ([]ChannelImage, error)
	GetImageByImageId(imageId string) (*ChannelImage, error)
}

type ChannelImage struct {
	UserID    string `dynamo:"user_id,hash"`
	CreatedAt string `dynamo:"created_at,range"`
	ImageURL  string `dynamo:"image_url"`
	ImageID   string `dynamo:"image_id"`
}

func NewChannelImagesDao(sess *session.Session, cfg config.Configuration) ChannelImagesDao {
	db := d.New(sess)
	table := db.Table(cfg.ChannelImagesTableName)

	return &channelImagesDao{Table: &table}
}

func (dao *channelImagesDao) GetImagesByChannelId(channelId string) ([]ChannelImage, error) {
	var channelImages []ChannelImage
	err := dao.Get(HashKey, channelId).All(&channelImages)
	if err == d.ErrNotFound {
		return nil, nil
	}
	if err != nil {
		return nil, err
	}

	return channelImages, nil
}

func (dao *channelImagesDao) GetImageByImageId(imageID string) (*ChannelImage, error) {
	var channelImages []ChannelImage
	err := dao.Get(GSI, imageID).Index(GSIName).All(&channelImages)

	if err == d.ErrNotFound || len(channelImages) < 1 {
		return nil, nil
	}
	if err != nil {
		return nil, err
	}

	if len(channelImages) > 1 {
		// probably should warn but not worried
		log.Warnf("Too many cheer images for image id: %s", imageID)
	}

	return &channelImages[0], nil
}
