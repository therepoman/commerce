package dynamo_test

import (
	"testing"
	"time"

	"code.justin.tv/commerce/payday/dynamo"
	"github.com/stretchr/testify/assert"
)

func TestTaxInfoDaoUpdateGetDelete(t *testing.T) {
	if dynamoContext.SkipDynamoTests {
		t.Skip("Skipping Dynamo Tests")
	}
	taxInfoDao, _ := dynamoContext.CreateDefaultTaxInfoDao("test-dao-update-get-delete-tax-info")
	err := taxInfoDao.SetupTable()
	assert.NoError(t, err)

	key := "entityId"

	originalAction := &dynamo.TaxInfo{
		PayoutEntityId:            key,
		TaxWithholdingRateMapJson: "{\"SER\":60.0}",
		TaxInterviewStatus:        "done",
		AddressId:                 "addr",
		LastUpdated:               time.Now(),
	}

	err = taxInfoDao.Update(originalAction)
	assert.NoError(t, err)

	retrievedAction, err := taxInfoDao.Get(key)

	assert.NotNil(t, originalAction)
	assert.NoError(t, err)

	assert.True(t, originalAction.Equals(retrievedAction))

	err = taxInfoDao.Delete(key)
	assert.NoError(t, err)

	deletedAction, err := taxInfoDao.Get(key)
	assert.NoError(t, err)
	assert.Nil(t, deletedAction)
}
