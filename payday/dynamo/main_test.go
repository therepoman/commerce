/*
	These tests require dynamo to be running locally.
	Running the tests with "make" will ensure that dynamo is running.

	Local Testing:
	- Running "make local" will ensure dynamo is started locally
	- Running "make dynamo_start" will manually start dynamo locally
	- Set the following in test/dynamo:
		"ideTesting" to true
  		"skipDynamoTests" to false
	- Tip: When developing, it's much faster to run individual dynamo test files manually than `make test`
*/
package dynamo_test

import (
	"os"
	"testing"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/test"
)

var dynamoContext *test.DynamoTestContext

func TestMain(m *testing.M) {
	var err error
	dynamoContext, err = test.NewDynamoTestContext("dynamo")
	if err != nil {
		log.WithError(err).Error("Failed during test setup")
		os.Exit(1)
	}

	if !dynamoContext.SkipDynamoTests {
		exitCode := m.Run()

		err = dynamoContext.Cleanup()
		if err != nil {
			log.WithError(err).Error("Failed during test cleanup")
			os.Exit(1)
		}

		os.Exit(exitCode)
	}

	os.Exit(0)
}
