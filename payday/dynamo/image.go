package dynamo

import (
	"strconv"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/hystrix"
	cmd "code.justin.tv/commerce/payday/hystrix"
	"code.justin.tv/commerce/payday/utils/pointers"
	timeUtils "code.justin.tv/commerce/payday/utils/time"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

const (
	maxImagesPerSet = 100
)

type ImagesTable struct {
}

type Image struct {
	SetId           string
	AssetType       string
	AssetId         string
	S3Key           string
	Tier            string
	Background      string
	AnimationType   string
	Scale           string
	AssociatedJobId string
	CreationMethod  string
	Version         int64
	LastUpdated     time.Time
}

type ImageDao struct {
	BaseDao
}

type IImageDao interface {
	Get(setId string, assetType string) (*Image, error)
	GetAllBySetId(setId string) ([]*Image, error)
	GetByAssetId(assetId string) (*Image, error)
	Update(newImage *Image) error
	Delete(setId string, assetType string) error
	BatchDelete(images []*Image) error
	DeleteTable() error
	SetupTable() error
	GetClientConfig() *DynamoClientConfig
	GetLatestStreamArn() (string, error)
}

func NewImageDao(client *DynamoClient) IImageDao {
	dao := &ImageDao{}
	dao.client = client
	dao.table = &ImagesTable{}
	return dao
}

func (i *ImagesTable) GetBaseTableName() BaseTableName {
	return "images"
}

func (i *ImagesTable) GetNodeJsRecordKeySelector() NodeJsRecordKeySelector {
	return "record.dynamodb.Keys.setId.S + '_' + record.dynamodb.Keys.assetType.S"
}

func (i *ImagesTable) GetAssetIdGSI(name FullTableName) string {
	return string(name) + "-assetId-GSI"
}

func (i *ImagesTable) NewCreateTableInput(name FullTableName, read ReadCapacity, write WriteCapacity) *dynamodb.CreateTableInput {
	return &dynamodb.CreateTableInput{
		TableName: aws.String(string(name)),
		ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
			ReadCapacityUnits:  aws.Int64(int64(read)),
			WriteCapacityUnits: aws.Int64(int64(write)),
		},
		AttributeDefinitions: []*dynamodb.AttributeDefinition{
			{
				AttributeName: aws.String("setId"),
				AttributeType: aws.String("S"),
			},
			{
				AttributeName: aws.String("assetType"),
				AttributeType: aws.String("S"),
			},
			{
				AttributeName: aws.String("assetId"),
				AttributeType: aws.String("S"),
			},
		},
		KeySchema: []*dynamodb.KeySchemaElement{
			{
				AttributeName: aws.String("setId"),
				KeyType:       aws.String("HASH"),
			},
			{
				AttributeName: aws.String("assetType"),
				KeyType:       aws.String("RANGE"),
			},
		},
		GlobalSecondaryIndexes: []*dynamodb.GlobalSecondaryIndex{
			{
				IndexName: aws.String(i.GetAssetIdGSI(name)),
				KeySchema: []*dynamodb.KeySchemaElement{
					{
						AttributeName: aws.String("assetId"),
						KeyType:       aws.String("HASH"),
					},
				},
				Projection: &dynamodb.Projection{
					ProjectionType: aws.String("ALL"),
				},
				ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
					ReadCapacityUnits:  aws.Int64(int64(read)),
					WriteCapacityUnits: aws.Int64(int64(write)),
				},
			},
		},
		StreamSpecification: &dynamodb.StreamSpecification{
			StreamEnabled:  aws.Bool(true),
			StreamViewType: aws.String(dynamodb.StreamViewTypeNewAndOldImages),
		},
	}
}

func (i *ImagesTable) ConvertAttributeMapToRecord(attributeMap map[string]*dynamodb.AttributeValue) (DynamoTableRecord, error) {
	version, err := Int64FromAttributes(attributeMap, "version")
	if err != nil {
		return nil, err
	}

	lastUpdated, err := TimeFromAttributes(attributeMap, "lastUpdated")
	if err != nil {
		return nil, err
	}

	return &Image{
		SetId:           StringFromAttributes(attributeMap, "setId"),
		AssetType:       StringFromAttributes(attributeMap, "assetType"),
		AssetId:         StringFromAttributes(attributeMap, "assetId"),
		S3Key:           StringFromAttributes(attributeMap, "s3Key"),
		Background:      StringFromAttributes(attributeMap, "background"),
		AnimationType:   StringFromAttributes(attributeMap, "animationType"),
		Tier:            StringFromAttributes(attributeMap, "tier"),
		Scale:           StringFromAttributes(attributeMap, "scale"),
		AssociatedJobId: StringFromAttributes(attributeMap, "associatedJobId"),
		CreationMethod:  StringFromAttributes(attributeMap, "imageType"),
		Version:         version,
		LastUpdated:     lastUpdated,
	}, nil
}

func (i *Image) GetTable() DynamoTable {
	return &ImagesTable{}
}

func (i *Image) GetScanQueryTable() DynamoScanQueryTable {
	return &ImagesTable{}
}

func (i *Image) NewAttributeMap() map[string]*dynamodb.AttributeValue {
	itemMap := make(map[string]*dynamodb.AttributeValue)

	PopulateAttributesString(itemMap, "setId", i.SetId)
	PopulateAttributesString(itemMap, "assetType", i.AssetType)
	PopulateAttributesString(itemMap, "assetId", i.AssetId)
	PopulateAttributesString(itemMap, "s3Key", i.S3Key)
	PopulateAttributesString(itemMap, "background", i.Background)
	PopulateAttributesString(itemMap, "animationType", i.AnimationType)
	PopulateAttributesString(itemMap, "tier", i.Tier)
	PopulateAttributesString(itemMap, "scale", i.Scale)
	PopulateAttributesString(itemMap, "associatedJobId", i.AssociatedJobId)
	PopulateAttributesString(itemMap, "imageType", i.CreationMethod)
	PopulateAttributesInt64(itemMap, "version", i.Version)
	PopulateAttributesTime(itemMap, "lastUpdated", i.LastUpdated)

	return itemMap
}

func (i *Image) NewItemKey() map[string]*dynamodb.AttributeValue {
	keyMap := make(map[string]*dynamodb.AttributeValue)
	keyMap["setId"] = &dynamodb.AttributeValue{S: aws.String(string(i.SetId))}
	keyMap["assetType"] = &dynamodb.AttributeValue{S: aws.String(string(i.AssetType))}
	return keyMap
}

func (i *Image) NewHashKeyEqualsExpression() string {
	return "setId = :setId"
}

func (i *Image) NewHashKeyExpressionAttributeValues() map[string]*dynamodb.AttributeValue {
	attributeMap := make(map[string]*dynamodb.AttributeValue)
	PopulateAttributesString(attributeMap, ":setId", i.SetId)
	return attributeMap
}

func (i *Image) UpdateWithCurrentTimestamp() {
	i.LastUpdated = time.Now()
}

func (i *Image) GetTimestamp() time.Time {
	return i.LastUpdated
}

func (i *Image) ApplyTimestamp(timestamp time.Time) {
	i.LastUpdated = timestamp
}

func (i *Image) Equals(other DynamoTableRecord) bool {
	otherImage, isImage := other.(*Image)
	if !isImage {
		return false
	}

	return i.SetId == otherImage.SetId &&
		i.AssetType == otherImage.AssetType &&
		i.AssetId == otherImage.AssetId &&
		i.S3Key == otherImage.S3Key &&
		i.Background == otherImage.Background &&
		i.AnimationType == otherImage.AnimationType &&
		i.Tier == otherImage.Tier &&
		i.Scale == otherImage.Scale &&
		i.AssociatedJobId == otherImage.AssociatedJobId &&
		i.Version == otherImage.Version &&
		i.CreationMethod == otherImage.CreationMethod

}

func (i *Image) EqualsWithExpectedLastUpdated(other DynamoTableRecord, lastUpdatedTime time.Time, lastUpdatedDelta time.Duration) bool {
	return i.Equals(other) && timeUtils.IsTimeInForwardDelta(other.GetTimestamp(), lastUpdatedTime, lastUpdatedDelta)
}

func (dao *ImageDao) Update(newImage *Image) error {
	oldImage, err := dao.Get(newImage.SetId, newImage.AssetType)
	if err != nil {
		return err
	}

	return dao.update(newImage, oldImage)
}

func (dao *ImageDao) update(newImage *Image, oldImage *Image) error {
	var conditionalUpdate *ConditionalExpression
	if oldImage != nil {
		newImage.Version = oldImage.Version + 1
		// Validate the existing DB record has version value we just read
		conditionalUpdate = &ConditionalExpression{
			Expected: map[string]*dynamodb.ExpectedAttributeValue{
				"version": {
					Value:              &dynamodb.AttributeValue{N: aws.String(strconv.FormatInt(oldImage.Version, 10))},
					ComparisonOperator: aws.String("EQ"),
				},
			},
		}
	} else {
		newImage.Version = 1
		// Validate that no DB record exists with a version
		conditionalUpdate = &ConditionalExpression{
			Expected: map[string]*dynamodb.ExpectedAttributeValue{
				"version": {
					Exists: aws.Bool(false),
				},
			},
		}

	}

	return hystrix.Do(cmd.UpdateImageCommand, func() error {
		return dao.client.UpdateItemConditional(newImage, conditionalUpdate)
	}, nil)
}

func (dao *ImageDao) GetAllBySetId(setId string) ([]*Image, error) {
	imagesChan := make(chan []*Image, 1)
	fn := func() error {
		images, err := dao.getAllBySetId(setId)
		if err != nil {
			return err
		}
		imagesChan <- images
		return nil
	}

	err := hystrix.Do(cmd.GetImageCommand, fn, nil)
	if err != nil {
		return nil, err
	}

	images := <-imagesChan
	return images, nil
}

func (dao *ImageDao) getAllBySetId(setId string) ([]*Image, error) {
	imageQuery := &Image{
		SetId: setId,
	}

	results, err := dao.client.QueryByHashKeyWithLimitAndConsistentRead(imageQuery, maxImagesPerSet, true)
	if err != nil {
		return nil, nil
	}

	imageResults := make([]*Image, len(results))
	for i, result := range results {
		imageResult, isImage := result.(*Image)
		if !isImage {
			msg := "Encountered an unexpected type while converting dynamo result to image"
			log.Error(msg)
			return nil, errors.New(msg)
		}
		imageResults[i] = imageResult
	}

	return imageResults, nil
}

func (dao *ImageDao) Get(setId string, assetType string) (*Image, error) {
	imageChan := make(chan *Image, 1)
	fn := func() error {
		image, err := dao.get(setId, assetType)
		if err != nil {
			return err
		}
		imageChan <- image
		return nil
	}

	err := hystrix.Do(cmd.GetImageCommand, fn, nil)
	if err != nil {
		return nil, err
	}

	image := <-imageChan
	return image, nil
}

func (dao *ImageDao) get(setId string, assetType string) (*Image, error) {
	var imageResult *Image

	imageQuery := &Image{
		SetId:     setId,
		AssetType: assetType,
	}

	result, err := dao.client.GetItem(imageQuery)
	if err != nil {
		return imageResult, err
	}

	// Result will be nil if no item is found in dynamo
	if result == nil {
		return nil, nil
	}

	imageResult, isImage := result.(*Image)
	if !isImage {
		msg := "Encountered an unexpected type while converting dynamo result to image"
		log.Error(msg)
		return imageResult, errors.New(msg)
	}

	return imageResult, nil
}

func (dao *ImageDao) GetByAssetId(assetId string) (*Image, error) {
	imageChan := make(chan *Image, 1)
	fn := func() error {
		image, err := dao.getByAssetId(assetId)
		if err != nil {
			return err
		}
		imageChan <- image
		return nil
	}

	err := hystrix.Do(cmd.GetImageByAssetIdCommand, fn, nil)
	if err != nil {
		return nil, err
	}

	image := <-imageChan
	return image, nil
}

func (dao *ImageDao) getByAssetId(assetId string) (*Image, error) {
	table := &ImagesTable{}
	idxName := table.GetAssetIdGSI(dao.client.GetFullTableName(table))
	assetIdQueryFilter := QueryFilter{
		Names: map[string]*string{
			"#ID": aws.String("assetId"),
		},
		Values: map[string]*dynamodb.AttributeValue{
			":id": {S: aws.String(string(assetId))},
		},
		KeyConditionExpression: aws.String("#ID = :id"),
		Limit:                  pointers.Int64P(1),
		Descending:             false,
		IndexName:              pointers.StringP(idxName),
	}

	result, _, err := dao.client.QueryTable(table, assetIdQueryFilter)
	if err != nil {
		return nil, err
	}

	if len(result) == 0 {
		return nil, nil
	}

	if len(result) > 1 {
		msg := "Error querying for image by assetId. Found multiple results but only expected 1."
		log.WithFields(log.Fields{
			"assetId":    assetId,
			"numResults": len(result),
		}).Error(msg)
		return nil, errors.New(msg)
	}

	record := result[0]
	image, isImage := record.(*Image)
	if !isImage {
		msg := "Non-image query result for image by assetId"
		log.WithField("assetId", assetId).Error(msg)
		return nil, errors.New(msg)
	}

	return image, nil
}

func (dao *ImageDao) Delete(setlId string, assetType string) error {
	return hystrix.Do(cmd.UpdateImageCommand, func() error {
		return dao.delete(setlId, assetType)
	}, nil)
}

func (dao *ImageDao) delete(setId string, assetType string) error {
	deletionRecord := &Image{
		SetId:     setId,
		AssetType: assetType,
	}
	return dao.client.DeleteItem(deletionRecord)
}

func (dao *ImageDao) BatchDelete(images []*Image) error {
	return hystrix.Do(cmd.BatchDeleteImagesCommand, func() error {
		return dao.batchDelete(images)
	}, nil)
}

func (dao *ImageDao) batchDelete(images []*Image) error {
	var records []DynamoTableRecord
	for _, image := range images {
		records = append(records, image)
	}
	return dao.client.BatchDeleteItem(records)
}

func (i *ImagesTable) ConvertScanOutputToRecords(so *dynamodb.ScanOutput) ([]DynamoScanQueryTableRecord, error) {
	records := make([]DynamoScanQueryTableRecord, *so.Count)

	for idx, attributes := range so.Items {
		recordToAdd, err := i.ConvertAttributeMapToRecord(attributes)
		if err != nil {
			return nil, err
		}

		scanQueryRecord, err := recordToScanQueryRecord(recordToAdd)
		if err != nil {
			return nil, err
		}

		records[idx] = scanQueryRecord
	}
	return records, nil
}

func (i *ImagesTable) ConvertQueryOutputToRecords(qo *dynamodb.QueryOutput) ([]DynamoScanQueryTableRecord, error) {
	records := make([]DynamoScanQueryTableRecord, *qo.Count)

	for idx, attributes := range qo.Items {
		recordToAdd, err := i.ConvertAttributeMapToRecord(attributes)
		if err != nil {
			return nil, err
		}

		scanQueryRecord, err := recordToScanQueryRecord(recordToAdd)
		if err != nil {
			return nil, err
		}

		records[idx] = scanQueryRecord
	}
	return records, nil
}
