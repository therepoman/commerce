package bits_entitlement

import (
	"context"
	"errors"
	"fmt"
	"time"

	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/payday/config"
	"github.com/aws/aws-sdk-go/aws/session"
	d "github.com/guregu/dynamo"
)

const (
	AddBitsTransactionType      = "AddBits"
	userIDTransactionTypeFormat = "%s-%s"
	userIDTransactionTypeKey    = "user_id-transaction_type"
	userIDTransactionTypeIndex  = "user_id-transaction_type-index"
)

type BitsEntitlementDAO interface {
	Create(ctx context.Context, transactionID, userID, productID, platform, bitsType string, amount int32, timeOfEvent time.Time, options *BitsEntitlementOptions) (*BitsEntitlement, error)
	GetMostRecent(ctx context.Context, userID string) (*BitsEntitlement, error)
	GetAfter(ctx context.Context, userID string, startTime time.Time) ([]BitsEntitlement, error)
}

type BitsEntitlement struct {
	TwitchUserID  string    `dynamo:"twitchUserId"`  //hash key
	DateTime      time.Time `dynamo:"datetime"`      //range key
	TransactionID string    `dynamo:"transactionId"` //GSI
	ProductID     string    `dynamo:"productId"`
	Platform      string    `dynamo:"platform"`
	BitsType      string    `dynamo:"string"`
	Amount        int32     `dynamo:"amount"`
}

type bitsEntitlementTransaction struct {
	TwitchUserID  string `dynamo:"user_id"`
	DateTime      int64  `dynamo:"last_updated"`   // GSI range key
	TransactionID string `dynamo:"transaction_id"` // table hash key
	ProductID     string `dynamo:"product_id"`
	Platform      string `dynamo:"platform"`
	BitsType      string `dynamo:"bits_type"`
	Amount        int32  `dynamo:"number_of_bits"`

	TransactionType             string  `dynamo:"transaction_type"`
	TwitchUserIDTransactionType string  `dynamo:"user_id-transaction_type"` // GSI hash key
	TimeOfEvent                 int64   `dynamo:"time_of_event"`
	TotalBitsAfterTransaction   *int64  `dynamo:"total_bits_after_transaction"`
	AdminID                     *string `dynamo:"admin_id"`
	AdminReason                 *string `dynamo:"admin_reason"`
	PurchasePrice               *string `dynamo:"purchase_price"`
	PurchasePriceCurrency       *string `dynamo:"purchase_price_currency"`
	TrackingID                  *string `dynamo:"tracking_id"`
	SkuQuantity                 *int64  `dynamo:"sku_quantity"`
	OriginOrderId               *string `dynamo:"origin_order_id"`
}

type BitsEntitlementOptions struct {
	TotalBitsAfterTransaction *int64
	AdminID                   *string
	AdminReason               *string
	PurchasePrice             *string
	PurchasePriceCurrency     *string
	TrackingID                *string
	SkuQuantity               *int64
	OriginOrderId             *string
}

func (tx *bitsEntitlementTransaction) ToBitsEntitlement() *BitsEntitlement {
	return &BitsEntitlement{
		TwitchUserID:  tx.TwitchUserID,
		DateTime:      time.Unix(0, tx.DateTime),
		TransactionID: tx.TransactionID,
		ProductID:     tx.ProductID,
		BitsType:      tx.BitsType,
		Amount:        tx.Amount,
	}
}

type transactionBasedBitsEntitlementDao struct {
	*d.Table
}

func NewTransactionBasedBitsEntitlementDAO(sess *session.Session, cfg config.Configuration) BitsEntitlementDAO {
	db := d.New(sess)
	table := db.Table(cfg.TransactionTableName)
	return &transactionBasedBitsEntitlementDao{Table: &table}
}

func (dao *transactionBasedBitsEntitlementDao) Create(ctx context.Context, transactionID, userID, productID, platform, bitsType string, amount int32, timeOfEvent time.Time, options *BitsEntitlementOptions) (*BitsEntitlement, error) {
	if strings.Blank(transactionID) || strings.Blank(userID) {
		return nil, errors.New("transactionID and userID must be provided")
	}

	entitlement := &bitsEntitlementTransaction{
		TwitchUserID:  userID,
		DateTime:      time.Now().UnixNano(),
		TransactionID: transactionID,
		ProductID:     productID,
		Platform:      platform,
		BitsType:      bitsType,
		Amount:        amount,

		TransactionType:             AddBitsTransactionType,
		TwitchUserIDTransactionType: getUserIDTransactionTypeKey(userID),
		TimeOfEvent:                 timeOfEvent.UnixNano(),
	}

	if options != nil {
		entitlement.TotalBitsAfterTransaction = options.TotalBitsAfterTransaction
		entitlement.AdminID = options.AdminID
		entitlement.AdminReason = options.AdminReason
		entitlement.PurchasePrice = options.PurchasePrice
		entitlement.PurchasePriceCurrency = options.PurchasePriceCurrency
		entitlement.TrackingID = options.TrackingID
		entitlement.SkuQuantity = options.SkuQuantity
		entitlement.OriginOrderId = options.OriginOrderId
	}

	err := dao.Put(entitlement).If("attribute_not_exists('transaction_id')").RunWithContext(ctx)
	return entitlement.ToBitsEntitlement(), err
}

func (dao *transactionBasedBitsEntitlementDao) GetMostRecent(ctx context.Context, userID string) (*BitsEntitlement, error) {
	if strings.Blank(userID) {
		return nil, errors.New("user ID is missing")
	}

	var transaction *bitsEntitlementTransaction
	err := dao.Get(userIDTransactionTypeKey, getUserIDTransactionTypeKey(userID)).
		Index(userIDTransactionTypeIndex).
		Range("last_updated", d.Less, time.Now().UnixNano()).
		Order(d.Descending). //default order is descending apparently, passing it anyways to be sure
		Limit(1).
		OneWithContext(ctx, &transaction)

	if err == d.ErrNotFound {
		return nil, nil
	}

	if err != nil {
		return nil, err
	}

	if transaction == nil {
		return nil, nil
	}

	return transaction.ToBitsEntitlement(), nil
}

func (dao *transactionBasedBitsEntitlementDao) GetAfter(ctx context.Context, userID string, startTime time.Time) ([]BitsEntitlement, error) {
	if strings.Blank(userID) {
		return nil, errors.New("userID is missing")
	}
	if !startTime.Before(time.Now()) {
		return nil, errors.New("startTime must be before now")
	}

	var transactions []*bitsEntitlementTransaction
	err := dao.Get(userIDTransactionTypeKey, getUserIDTransactionTypeKey(userID)).
		Index(userIDTransactionTypeIndex).
		Range("last_updated", d.Greater, startTime.UnixNano()).
		AllWithContext(ctx, &transactions)

	if err == d.ErrNotFound {
		return []BitsEntitlement{}, nil
	}

	if err != nil {
		return nil, err
	}

	entitlements := make([]BitsEntitlement, 0, len(transactions))
	for _, tx := range transactions {
		if tx != nil {
			entitlements = append(entitlements, *tx.ToBitsEntitlement())
		}
	}

	return entitlements, nil
}

func getUserIDTransactionTypeKey(userID string) string {
	return fmt.Sprintf(userIDTransactionTypeFormat, userID, AddBitsTransactionType)
}
