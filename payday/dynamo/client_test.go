package dynamo_test

import (
	"fmt"
	"testing"
	"time"

	"code.justin.tv/commerce/gogogadget/random"
	"code.justin.tv/commerce/payday/dynamo"
	"github.com/stretchr/testify/assert"
)

func TestGetForNonExistentRecord(t *testing.T) {
	if dynamoContext.SkipDynamoTests {
		t.Skip("Skipping Dynamo Tests")
	}
	dao, client := dynamoContext.CreateDefaultUserDao("test-get-non-existent")
	err := dao.SetupTable()
	assert.NoError(t, err)
	userQuery := &dynamo.User{
		Id: "non-existent user",
	}
	result, err := client.GetItem(userQuery)
	assert.Nil(t, result)
	assert.Nil(t, err)
}

func TestPutThenGet(t *testing.T) {
	if dynamoContext.SkipDynamoTests {
		t.Skip("Skipping Dynamo Tests")
	}
	dao, client := dynamoContext.CreateDefaultUserDao("test-put-get")
	err := dao.SetupTable()
	assert.NoError(t, err)
	now := time.Now()
	user := &dynamo.User{
		Id:         "someUser",
		Banned:     true,
		BannedBy:   "someBanner",
		Annotation: "someAnnotation",
	}
	err = client.PutItem(user)
	assert.Nil(t, err)

	userQuery := &dynamo.User{
		Id: "someUser",
	}
	result, err := client.GetItem(userQuery)
	assert.Nil(t, err)
	assert.True(t, user.EqualsWithExpectedLastUpdated(result, now, dynamoContext.AcceptableUpdateTimeDelta))
}

func TestPutThenBatchGet(t *testing.T) {
	if dynamoContext.SkipDynamoTests {
		t.Skip("Skipping Dynamo Tests")
	}
	dao, client := dynamoContext.CreateDefaultUserDao("test-put-batch-get")
	err := dao.SetupTable()
	assert.NoError(t, err)
	now := time.Now()
	user1 := &dynamo.User{
		Id:         "someUser",
		Banned:     true,
		BannedBy:   "someBanner",
		Annotation: "someAnnotation",
	}
	err = client.PutItem(user1)
	assert.Nil(t, err)

	testRecordCount := 202

	users := make(map[string]dynamo.DynamoTableRecord)
	for i := 0; i < testRecordCount; i++ {
		user := &dynamo.User{
			Id:         dynamo.UserId(random.String(16)),
			Banned:     true,
			BannedBy:   "someBanner",
			Annotation: "someAnnotation",
		}
		err := client.PutItem(user)
		assert.Nil(t, err)
		users[string(user.Id)] = user
	}

	userQuery := make([]dynamo.DynamoTableRecord, 0, testRecordCount)
	for _, user := range users {
		userQuery = append(userQuery, user)
	}

	results, err := client.BatchGetItem(userQuery)
	assert.Nil(t, err)
	assert.Equal(t, testRecordCount, len(results))
	for _, result := range results {
		actualUser, ok := result.(*dynamo.User)
		assert.True(t, ok)

		expectedUser := users[string(actualUser.Id)]

		assert.True(t, expectedUser.EqualsWithExpectedLastUpdated(actualUser, now, dynamoContext.AcceptableUpdateTimeDelta))
	}
}

func TestPutWithMissingFields(t *testing.T) {
	if dynamoContext.SkipDynamoTests {
		t.Skip("Skipping Dynamo Tests")
	}
	dao, client := dynamoContext.CreateDefaultUserDao("test-put-missing-fields")
	err := dao.SetupTable()
	assert.NoError(t, err)
	now := time.Now()
	user := &dynamo.User{
		Id: "someUser",
	}
	err = client.PutItem(user)
	assert.Nil(t, err)

	userQuery := &dynamo.User{
		Id: "someUser",
	}
	result, err := client.GetItem(userQuery)
	assert.Nil(t, err)
	assert.True(t, user.EqualsWithExpectedLastUpdated(result, now, dynamoContext.AcceptableUpdateTimeDelta))
}

func TestPutReplaceExisting(t *testing.T) {
	if dynamoContext.SkipDynamoTests {
		t.Skip("Skipping Dynamo Tests")
	}
	dao, client := dynamoContext.CreateDefaultUserDao("test-put-replace")
	err := dao.SetupTable()
	assert.NoError(t, err)
	now := time.Now()

	user1 := &dynamo.User{
		Id:         "someUser",
		Banned:     false,
		BannedBy:   "someBanner1",
		Annotation: "someAnnotation1",
	}
	err = client.PutItem(user1)
	assert.Nil(t, err)

	user2 := &dynamo.User{
		Id:         "someUser",
		Banned:     true,
		BannedBy:   "someBanner2",
		Annotation: "someAnnotation2",
	}
	err = client.PutItem(user2)
	assert.Nil(t, err)

	userQuery := &dynamo.User{
		Id: "someUser",
	}
	result, err := client.GetItem(userQuery)
	assert.Nil(t, err)
	assert.True(t, user2.EqualsWithExpectedLastUpdated(result, now, dynamoContext.AcceptableUpdateTimeDelta))
}

func TestDeleteNonExistent(t *testing.T) {
	if dynamoContext.SkipDynamoTests {
		t.Skip("Skipping Dynamo Tests")
	}
	dao, client := dynamoContext.CreateDefaultUserDao("test-delete-non-existent")
	err := dao.SetupTable()
	assert.NoError(t, err)
	user := &dynamo.User{
		Id: "someUser",
	}
	err = client.DeleteItem(user)
	assert.Nil(t, err)
}

func TestPutThenDelete(t *testing.T) {
	if dynamoContext.SkipDynamoTests {
		t.Skip("Skipping Dynamo Tests")
	}
	dao, client := dynamoContext.CreateDefaultUserDao("test-put-delete")
	err := dao.SetupTable()
	assert.NoError(t, err)
	now := time.Now()
	user := &dynamo.User{
		Id:         "someUser",
		Banned:     true,
		BannedBy:   "someBanner",
		Annotation: "someAnnotation",
	}
	err = client.PutItem(user)
	assert.Nil(t, err)

	userQuery := &dynamo.User{
		Id: "someUser",
	}
	result, err := client.GetItem(userQuery)
	assert.Nil(t, err)
	assert.True(t, user.EqualsWithExpectedLastUpdated(result, now, dynamoContext.AcceptableUpdateTimeDelta))

	err = client.DeleteItem(userQuery)
	assert.Nil(t, err)

	result, err = client.GetItem(userQuery)
	assert.Nil(t, result)
	assert.Nil(t, err)
}

func TestPutThenBatchDelete(t *testing.T) {
	if dynamoContext.SkipDynamoTests {
		t.Skip("Skipping Dynamo Tests")
	}
	dao, client := dynamoContext.CreateDefaultUserDao("test-put-batch-delete")
	err := dao.SetupTable()
	assert.NoError(t, err)
	testRecordCount := 50

	users := make(map[string]dynamo.DynamoTableRecord)
	for i := 0; i < testRecordCount; i++ {
		username := fmt.Sprintf("someUser-%d", i)
		user := &dynamo.User{
			Id:     dynamo.UserId(username),
			Banned: true,
		}
		err = client.PutItem(user)
		assert.Nil(t, err)
		users[string(user.Id)] = user
	}

	userQuery := make([]dynamo.DynamoTableRecord, 0, testRecordCount)
	for _, user := range users {
		userQuery = append(userQuery, user)
	}

	results, err := client.BatchGetItem(userQuery)
	assert.Nil(t, err)
	assert.Equal(t, testRecordCount, len(results))

	err = client.BatchDeleteItem(userQuery)
	assert.Nil(t, err)

	results, err = client.BatchGetItem(userQuery)
	assert.Nil(t, err)
	assert.Equal(t, 0, len(results))
}

func TestPutThenBatchUpdate(t *testing.T) {
	if dynamoContext.SkipDynamoTests {
		t.Skip("Skipping Dynamo Tests")
	}
	dao, client := dynamoContext.CreateDefaultUserDao("test-put-batch-update")
	err := dao.SetupTable()
	assert.NoError(t, err)
	testRecordCount := 50

	users := make(map[string]dynamo.DynamoTableRecord)
	for i := 0; i < testRecordCount; i++ {
		username := fmt.Sprintf("someUser-%d", i)
		user := &dynamo.User{
			Id:         dynamo.UserId(username),
			Banned:     true,
			BannedBy:   "someBanner",
			Annotation: "someAnnotation",
		}
		err = client.PutItem(user)
		assert.Nil(t, err)
		users[string(user.Id)] = user
	}

	userQuery := make([]dynamo.DynamoTableRecord, 0, testRecordCount)
	for _, user := range users {
		userQuery = append(userQuery, user)
	}

	results, err := client.BatchGetItem(userQuery)
	assert.Nil(t, err)
	assert.Equal(t, testRecordCount, len(results))

	for i := 0; i < testRecordCount; i++ {
		username := fmt.Sprintf("someUser-%d", i)
		user := &dynamo.User{
			Id:         dynamo.UserId(username),
			Banned:     false,
			BannedBy:   "newBanner",
			Annotation: "newAnnotation",
		}
		users[string(user.Id)] = user
	}

	userQueryUpdated := make([]dynamo.DynamoTableRecord, 0, testRecordCount)
	for _, user := range users {
		userQueryUpdated = append(userQueryUpdated, user)
	}

	err = client.BatchPutItem(userQueryUpdated)
	assert.Nil(t, err)

	results, err = client.BatchGetItem(userQueryUpdated)
	assert.Nil(t, err)
	assert.Equal(t, testRecordCount, len(results))

	for _, result := range results {
		actualUser, ok := result.(*dynamo.User)
		assert.True(t, ok)

		expectedUser := users[string(actualUser.Id)]
		assert.True(t, expectedUser.Equals(actualUser))
	}
}
