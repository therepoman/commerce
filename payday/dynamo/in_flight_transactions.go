package dynamo

import (
	"context"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/config"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	d "github.com/guregu/dynamo"
)

const (
	IN_FLIGHT_HASH_KEY = "transaction_id"
)

type InFlightTransactionDao interface {
	CreateTransaction(ctx context.Context, transaction Transaction) error
	GetTransaction(ctx context.Context, id string) (*Transaction, error)
	GetTransactions(ctx context.Context, ids []string) ([]Transaction, error)
	RemoveTransaction(ctx context.Context, id string)
}

type inFlightTransactionDao struct {
	*d.Table
}

func NewInFlightTransactionDao(sess *session.Session, cfg config.Configuration) InFlightTransactionDao {
	db := d.New(sess)
	table := db.Table(cfg.InFlightTransactionTableName)
	return &inFlightTransactionDao{Table: &table}
}

func (dao *inFlightTransactionDao) CreateTransaction(ctx context.Context, transaction Transaction) error {
	err := dao.Put(&transaction).If("attribute_not_exists('transaction_id')").RunWithContext(ctx)

	if err != nil { // we'll handle the already created error here, IE, we don't care and continue.
		if awsErr, ok := err.(awserr.Error); ok && awsErr.Code() == dynamodb.ErrCodeConditionalCheckFailedException {
			return nil
		}
	}

	return err
}

func (dao *inFlightTransactionDao) GetTransaction(ctx context.Context, id string) (*Transaction, error) {
	var transaction *Transaction
	err := dao.Get(IN_FLIGHT_HASH_KEY, id).Limit(1).OneWithContext(ctx, &transaction)

	if err != nil && err == d.ErrNotFound {
		return nil, nil
	}
	return transaction, err
}

func (dao *inFlightTransactionDao) GetTransactions(ctx context.Context, ids []string) ([]Transaction, error) {
	var transactions []Transaction
	var keys []d.Keyed
	for _, id := range ids {
		keys = append(keys, d.Keys{id})
	}

	err := dao.Batch(IN_FLIGHT_HASH_KEY).Get(keys...).AllWithContext(ctx, &transactions)
	if err != nil && err != d.ErrNotFound {
		return nil, err
	}

	return transactions, nil
}

func (dao *inFlightTransactionDao) RemoveTransaction(ctx context.Context, id string) {
	err := dao.Delete(IN_FLIGHT_HASH_KEY, string(id)).RunWithContext(ctx)
	if err != nil { // We don't need to bubble this error up, as worst case is we already have deleted it.
		log.WithError(err).WithFields(
			log.Fields{
				"transactionId": id,
			}).Warn("Failed to cleanup in flight transaction.")
	}
}
