package badge_tier_emote_groupids

import (
	"context"
	"fmt"
	"time"

	"code.justin.tv/commerce/payday/config"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	d "github.com/guregu/dynamo"
)

const (
	hashKey                     = "channel_id"
	rangeKey                    = "threshold"
	groupIDHashKey              = "group_id"
	groupIDGlobalSecondaryIndex = "group_id-index"
	isBackfilledAttribute       = "is_backfilled"
)

type BadgeTierEmoteGroupID struct {
	ChannelID string    `dynamo:"channel_id"` //hash key
	Threshold int64     `dynamo:"threshold"`  //range key
	GroupID   string    `dynamo:"group_id"`
	CreatedAt time.Time `dynamo:"created"`
	// This flag is used to indicate whether a bits tier has already backfilled all entitled users the emote group ID.
	// It should be set to false initially, and then set to true once a bits tier emote backfill is complete.
	IsBackfilled bool `dynamo:"is_backfilled"`
}

type BadgeTierEmoteGroupIDDAO interface {
	Get(ctx context.Context, channelID string) ([]*BadgeTierEmoteGroupID, error)
	GetAboveThreshold(ctx context.Context, channelID string, minThreshold int64) ([]*BadgeTierEmoteGroupID, error)
	GetByChannelAndThreshold(ctx context.Context, channelID string, threshold int64) (*BadgeTierEmoteGroupID, error)
	BatchGetByChannelAndThresholds(ctx context.Context, channelID string, thresholds []int64) ([]*BadgeTierEmoteGroupID, error)
	GetByGroupID(ctx context.Context, groupIDs string) (*BadgeTierEmoteGroupID, bool, error)
	Create(ctx context.Context, channelID string, threshold int64, groupID string) (*BadgeTierEmoteGroupID, bool, error)
	UpdateBackfillState(ctx context.Context, channelID string, threshold int64, isBackfilled bool) error
	Delete(ctx context.Context, channelID string, threshold int64) error
	// Note: These are a Scan operation and should only be used for backfill purposes
	GetAll(ctx context.Context) ([]*BadgeTierEmoteGroupID, error)
	GetAllWithFilter(ctx context.Context, expr string, args ...interface{}) ([]*BadgeTierEmoteGroupID, error)
}

type dao struct {
	*d.Table
}

func NewBadgeTierEmoteGroupIDDAO(sess *session.Session, cfg config.Configuration) BadgeTierEmoteGroupIDDAO {
	db := d.New(sess)
	table := db.Table(cfg.BadgeTierEmoteGroupIDTableName)

	return &dao{Table: &table}
}

func (dao *dao) Get(ctx context.Context, channelID string) ([]*BadgeTierEmoteGroupID, error) {
	var badgeTierEmoteGroupIDs []*BadgeTierEmoteGroupID

	err := dao.Table.
		Get(hashKey, channelID).
		AllWithContext(ctx, &badgeTierEmoteGroupIDs)

	return badgeTierEmoteGroupIDs, err
}

func (dao *dao) GetAboveThreshold(ctx context.Context, channelID string, minThreshold int64) ([]*BadgeTierEmoteGroupID, error) {
	var badgeTierEmoteGroupIDs []*BadgeTierEmoteGroupID

	err := dao.Table.Get(hashKey, channelID).
		Range(rangeKey, d.Greater, minThreshold).
		Order(d.Ascending).
		AllWithContext(ctx, &badgeTierEmoteGroupIDs)

	return badgeTierEmoteGroupIDs, err
}

func (dao *dao) GetByChannelAndThreshold(ctx context.Context, channelID string, threshold int64) (*BadgeTierEmoteGroupID, error) {
	var badgeTierEmoteGroupID BadgeTierEmoteGroupID

	err := dao.Table.
		Get(hashKey, channelID).
		Range(rangeKey, d.Equal, threshold).
		OneWithContext(ctx, &badgeTierEmoteGroupID)

	if err != nil && err != d.ErrNotFound {
		return nil, err
	}

	//TODO: we should probably handle the "not found" case by returning that error rather than suppressing it
	return &badgeTierEmoteGroupID, nil
}

func (dao *dao) BatchGetByChannelAndThresholds(ctx context.Context, channelID string, thresholds []int64) ([]*BadgeTierEmoteGroupID, error) {
	var badgeTierEmoteGroupIDs []*BadgeTierEmoteGroupID

	var dynamoKeys = make([]d.Keyed, 0, len(thresholds))
	for _, threshold := range thresholds {
		dynamoKeys = append(dynamoKeys, d.Keys{channelID, threshold})
	}

	err := dao.Table.
		Batch(hashKey, rangeKey).
		Get(dynamoKeys...).
		AllWithContext(ctx, &badgeTierEmoteGroupIDs)

	return badgeTierEmoteGroupIDs, err
}

func (dao *dao) GetByGroupID(ctx context.Context, groupID string) (*BadgeTierEmoteGroupID, bool, error) {
	var badgeTierEmoteGroupID BadgeTierEmoteGroupID

	err := dao.Table.
		Get(groupIDHashKey, groupID).
		Index(groupIDGlobalSecondaryIndex).
		OneWithContext(ctx, &badgeTierEmoteGroupID)

	return &badgeTierEmoteGroupID, err == d.ErrNotFound, err
}

func (dao *dao) Create(ctx context.Context, channelID string, threshold int64, groupID string) (*BadgeTierEmoteGroupID, bool, error) {
	badgeTierEmoteGroupID := BadgeTierEmoteGroupID{
		ChannelID: channelID,
		Threshold: threshold,
		GroupID:   groupID,
		CreatedAt: time.Now(),
		// By default, when a new tier-groupID mapping is created, backfilling should be flagged as not done yet.
		// Once at least one backfill is complete, this value can be flipped to true.
		IsBackfilled: false,
	}

	err := dao.Table.Put(badgeTierEmoteGroupID).
		If(fmt.Sprintf("attribute_not_exists(%s) AND attribute_not_exists(%s)", hashKey, rangeKey)).
		RunWithContext(ctx)

	alreadyExists := false

	if err != nil {
		if dynamoErr, ok := err.(awserr.Error); ok {
			if dynamoErr.Code() == dynamodb.ErrCodeConditionalCheckFailedException {
				alreadyExists = true
			}
		}
	}

	return &badgeTierEmoteGroupID, alreadyExists, err
}

func (dao *dao) UpdateBackfillState(ctx context.Context, channelID string, threshold int64, isBackfilled bool) error {
	return dao.Table.
		Update(hashKey, channelID).
		Range(rangeKey, threshold).
		Set(isBackfilledAttribute, isBackfilled).
		RunWithContext(ctx)
}

func (dao *dao) Delete(ctx context.Context, channelID string, threshold int64) error {
	err := dao.Table.
		Delete(hashKey, channelID).
		Range(rangeKey, threshold).
		RunWithContext(ctx)
	return err
}

// Note: This is a Scan operation and should only be used for backfill purposes
func (dao *dao) GetAll(ctx context.Context) ([]*BadgeTierEmoteGroupID, error) {
	var badgeTierEmoteGroupIDsResult []*BadgeTierEmoteGroupID

	err := dao.Table.
		Scan().
		AllWithContext(ctx, &badgeTierEmoteGroupIDsResult)
	if err != nil {
		return nil, err
	}

	return badgeTierEmoteGroupIDsResult, err
}

// Note: This is a Scan operation and should only be used for backfill purposes
func (dao *dao) GetAllWithFilter(ctx context.Context, expr string, args ...interface{}) ([]*BadgeTierEmoteGroupID, error) {
	var badgeTierEmoteGroupIDsResult []*BadgeTierEmoteGroupID

	err := dao.Table.
		Scan().
		Filter(expr, args...).
		AllWithContext(ctx, &badgeTierEmoteGroupIDsResult)
	if err != nil {
		return nil, err
	}

	return badgeTierEmoteGroupIDsResult, err
}
