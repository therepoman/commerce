package dynamo_test

import (
	"fmt"
	"testing"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/eligible_channels"
	"code.justin.tv/commerce/payday/test"
	"github.com/stretchr/testify/assert"
)

func TestSponsoredCheermoteEligibleChannelsDao(t *testing.T) {
	if dynamoContext.SkipDynamoTests {
		t.Skip("Skipping Dynamo Tests")
	}
	spomoteEligibleChannelsDao, _ := dynamoContext.CreateDefaultSponsoredCheermoteEligibleChannelsDao("test-dao-complete")
	err := spomoteEligibleChannelsDao.SetupTable()
	assert.NoError(t, err)

	testChannelID := "test-user-spomote-eligible-channels"

	now := time.Now()
	savedRecords := make(map[string]*eligible_channels.SponsoredCheermoteEligibleChannel)
	for i := 0; i < numChannelsToTest; i++ {
		campaignID := test.RandomString(16)
		testSpomoteEligibility := &eligible_channels.SponsoredCheermoteEligibleChannel{
			CampaignID:  campaignID,
			ChannelID:   testChannelID,
			LastUpdated: time.Now(),
		}
		err := spomoteEligibleChannelsDao.Update(testSpomoteEligibility)
		if err != nil {
			t.Fail()
		}
		savedRecords[campaignID] = testSpomoteEligibility
	}

	log.Infof("Retrieving and verifying spomote channel eligibility records from dynamo")
	for campaignID, expectedEligibility := range savedRecords {
		actualEligibility, err := spomoteEligibleChannelsDao.Get(testChannelID, campaignID)
		if err != nil {
			t.Fail()
		}
		assert.True(t, expectedEligibility.EqualsWithExpectedLastUpdated(actualEligibility, now, dynamoContext.AcceptableUpdateTimeDelta))
	}

	now = time.Now()
	log.Infof("Modifying existing spomote channel eligibility records")
	for campaignID := range savedRecords {
		modifiedSpomoteEligibility := &eligible_channels.SponsoredCheermoteEligibleChannel{
			CampaignID:  campaignID,
			ChannelID:   testChannelID,
			LastUpdated: time.Now(),
		}
		err := spomoteEligibleChannelsDao.Update(modifiedSpomoteEligibility)
		if err != nil {
			t.Fail()
		}
		savedRecords[campaignID] = modifiedSpomoteEligibility
	}

	log.Infof("Retrieving and verifying modified spomote channel eligibility records from dynamo")
	for campaignID, expectedEligibility := range savedRecords {
		actualEligibility, err := spomoteEligibleChannelsDao.Get(testChannelID, campaignID)
		if err != nil {
			t.Fail()
		}
		assert.True(t, expectedEligibility.EqualsWithExpectedLastUpdated(actualEligibility, now, dynamoContext.AcceptableUpdateTimeDelta))
	}

	log.Infof("Deleting spomote channel eligibility records from dynamo")
	for campaignID := range savedRecords {
		err := spomoteEligibleChannelsDao.Delete(testChannelID, campaignID)
		assert.NoError(t, err)
	}

	log.Infof("Verifying that spomote channel eligibility records no longer exist in dynamo")
	for campaignID := range savedRecords {
		actualEligibility, err := spomoteEligibleChannelsDao.Get(testChannelID, campaignID)
		if err != nil {
			t.Fail()
		}
		assert.Nil(t, actualEligibility)
	}

	log.Infof("Adding more records to test QueryWithLimit")
	var testRecords []*eligible_channels.SponsoredCheermoteEligibleChannel
	numOfRecords := 50
	campaignPrefix := "test"
	for i := 1; i <= numOfRecords; i++ {
		testRecords = append(testRecords, &eligible_channels.SponsoredCheermoteEligibleChannel{
			ChannelID:   testChannelID,
			CampaignID:  fmt.Sprintf("%s-%d", campaignPrefix, i),
			LastUpdated: time.Now(),
		})
	}
	for _, testRecord := range testRecords {
		err := spomoteEligibleChannelsDao.Update(testRecord)
		if err != nil {
			t.Fail()
		}
	}

	log.Infof("Test QueryWithLimit to see it returns the right amount of data and right pagination")
	pageSize := 10
	var pagingKey *string
	for i := 0; i < numOfRecords/pageSize; i++ {
		actualRecords, newPagingKey, err := spomoteEligibleChannelsDao.QueryWithLimit(testChannelID, int64(pageSize), pagingKey)
		if err != nil {
			t.Fail()
		}
		assert.Equal(t, len(actualRecords), pageSize)
		assert.NotNil(t, newPagingKey)
		pagingKey = newPagingKey
	}

	lastPageRecords, lastPagePagingKey, err := spomoteEligibleChannelsDao.QueryWithLimit(testChannelID, int64(pageSize), pagingKey)
	if err != nil {
		t.Fail()
	}
	assert.Equal(t, len(lastPageRecords), 0)
	assert.Nil(t, lastPagePagingKey)

	log.Infof("Test BatchDelete to see all records are deleted")
	err = spomoteEligibleChannelsDao.BatchDelete(testRecords)
	if err != nil {
		t.Fail()
	}

	actualRecords, pagingKey, err := spomoteEligibleChannelsDao.QueryWithLimit(testChannelID, int64(pageSize), nil)
	if err != nil {
		t.Fail()
	}
	assert.Equal(t, len(actualRecords), 0)
	assert.Nil(t, pagingKey)

}
