package dynamo_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestExistenceTimeout(t *testing.T) {
	if dynamoContext.SkipDynamoTests {
		t.Skip("Skipping Dynamo Tests")
	}
	userDao, _ := dynamoContext.CreateUserDao(1, dynamoContext.WaitTimeout, "test-existence-timeout")
	channelDao, _ := dynamoContext.CreateChannelDao(1, dynamoContext.WaitTimeout, "test-existence-timeout")

	assert.Error(t, userDao.SetupTable())
	assert.Error(t, channelDao.SetupTable())
}

func TestWaitTimeout(t *testing.T) {
	userDao, _ := dynamoContext.CreateUserDao(dynamoContext.WaitTimeout, 1, "test-wait-timeout")
	channelDao, _ := dynamoContext.CreateChannelDao(dynamoContext.WaitTimeout, 1, "test-wait-timeout")

	assert.Error(t, userDao.SetupTable())
	assert.Error(t, channelDao.SetupTable())
}
