package badge_tier_notification

import (
	"fmt"
	"time"

	"code.justin.tv/commerce/payday/badgetiers/models"
	"code.justin.tv/commerce/payday/config"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/gofrs/uuid"
	d "github.com/guregu/dynamo"
)

type badgeTierNotificationImpl struct {
	*d.Table
}

type BadgeTierNotificationDAO interface {
	CreateNotification(channelID string, userID string, threshold int64) (*BadgeTierNotification, error)
	GetNotification(notificationID string) (*BadgeTierNotification, error)
	UpdateItemNotificationState(channelID string, userID string, threshold int64, state models.BadgeTierNotificationStatus) error
	GetLargestNotificationByThreshold(channelID, userID string) (*BadgeTierNotification, error)
	GetNotificationByChannelUserThreshold(channelID, userID string, threshold int64) (*BadgeTierNotification, error)
}

//TODO: remove GSI from dynamo table (notification_id)
type BadgeTierNotification struct {
	ChannelIDUserID   string                             `dynamo:"channel_id-user_id,hash"`
	ChannelID         string                             `dynamo:"channel_id"`
	UserID            string                             `dynamo:"user_id"`
	Threshold         int64                              `dynamo:"threshold,range"`
	NotificationState models.BadgeTierNotificationStatus `dynamo:"notification_state"`
	NotificationID    string                             `dynamo:"notification_id" index:"notification_id-index,hash"`
	CreatedAt         time.Time                          `dynamo:"created_at"`
	UpdatedAt         time.Time                          `dynamo:"updated_at"`
	ExpiresAt         time.Time                          `dynamo:"expires_at,unixtime"`
}

func NewBadgeTierNotificationDao(sess *session.Session, cfg config.Configuration) BadgeTierNotificationDAO {
	db := d.New(sess)
	table := db.Table(cfg.BadgeTierNotificationTableName)
	return &badgeTierNotificationImpl{Table: &table}
}

func (dao *badgeTierNotificationImpl) GetLargestNotificationByThreshold(channelID string, userID string) (*BadgeTierNotification, error) {
	var notification BadgeTierNotification

	err := dao.Get("channel_id-user_id", makeHashKey(channelID, userID)).
		Range("threshold", d.Greater, 0).
		Order(d.Descending).
		Limit(1).
		Consistent(true).
		One(&notification)

	if err == d.ErrNotFound {
		return nil, nil
	}
	return &notification, err
}

func (dao *badgeTierNotificationImpl) CreateNotification(channelID string, userID string, threshold int64) (*BadgeTierNotification, error) {
	now := time.Now()
	notificationID, err := uuid.NewV4()

	if err != nil {
		return nil, err
	}

	badgeTierNotification := &BadgeTierNotification{
		ChannelIDUserID:   makeHashKey(channelID, userID),
		ChannelID:         channelID,
		UserID:            userID,
		Threshold:         threshold,
		NotificationState: models.Show,
		NotificationID:    notificationID.String(),
		CreatedAt:         now,
		UpdatedAt:         now,
		ExpiresAt:         now.Add(time.Hour * 24 * 60), // 60 days TTL
	}

	err = dao.
		Put(badgeTierNotification).
		If("attribute_not_exists('channel_id-user_id') AND attribute_not_exists(threshold)").
		Run()

	return badgeTierNotification, err
}

func (dao *badgeTierNotificationImpl) GetNotificationByChannelUserThreshold(channelID, userID string, threshold int64) (*BadgeTierNotification, error) {
	var notification BadgeTierNotification

	err := dao.Get("channel_id-user_id", makeHashKey(channelID, userID)).
		Range("threshold", d.Equal, threshold).
		Consistent(true).
		One(&notification)

	if err == d.ErrNotFound {
		return nil, nil
	}

	return &notification, err
}

func (dao *badgeTierNotificationImpl) GetNotification(notificationID string) (*BadgeTierNotification, error) {
	var notification BadgeTierNotification

	err := dao.Get("notification_id", notificationID).
		Index("notification_id-index").
		One(&notification)

	if err == d.ErrNotFound {
		return nil, nil
	}

	return &notification, err
}

func (dao *badgeTierNotificationImpl) UpdateItemNotificationState(channelID string, userID string, threshold int64, state models.BadgeTierNotificationStatus) error {
	return dao.
		Update("channel_id-user_id", makeHashKey(channelID, userID)).
		Range("threshold", threshold).
		Set("notification_state", state).
		Set("updated_at", time.Now()).
		Run()
}

func makeHashKey(channelID string, userID string) string {
	return fmt.Sprintf("%s-%s", channelID, userID)
}
