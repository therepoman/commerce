package dynamo_test

import (
	"fmt"
	"testing"
	"time"

	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	"code.justin.tv/commerce/payday/models/api"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/test"
	"code.justin.tv/commerce/payday/utils/pointers"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

const numChannelsToTest = 3
const randomNilChance = .5

func mergeChannelRecords(existingChannel *dynamo.Channel, channelModifications *dynamo.Channel) *dynamo.Channel {
	mergedChannel := &dynamo.Channel{
		Id:                          existingChannel.Id,
		MinBits:                     pointers.Int64PMerge(existingChannel.MinBits, channelModifications.MinBits),
		MinBitsEmote:                pointers.Int64PMerge(existingChannel.MinBitsEmote, channelModifications.MinBitsEmote),
		OptedOut:                    pointers.BoolPMerge(existingChannel.OptedOut, channelModifications.OptedOut),
		Onboarded:                   pointers.BoolPMerge(existingChannel.Onboarded, channelModifications.Onboarded),
		Annotation:                  pointers.StringPMerge(existingChannel.Annotation, channelModifications.Annotation),
		PinRecentCheers:             pointers.BoolPMerge(existingChannel.PinRecentCheers, channelModifications.PinRecentCheers),
		RecentCheerMin:              pointers.Int64PMerge(existingChannel.RecentCheerMin, channelModifications.RecentCheerMin),
		RecentCheerTimeout:          pointers.Int64PMerge(existingChannel.RecentCheerTimeout, channelModifications.RecentCheerTimeout),
		CustomCheermoteImageSetId:   pointers.StringPMerge(existingChannel.CustomCheermoteImageSetId, channelModifications.CustomCheermoteImageSetId),
		CustomCheermoteStatus:       pointers.StringPMerge(existingChannel.CustomCheermoteStatus, channelModifications.CustomCheermoteStatus),
		OptedOutOfVoldemort:         pointers.BoolPMerge(existingChannel.OptedOutOfVoldemort, channelModifications.OptedOutOfVoldemort),
		LeaderboardEnabled:          pointers.BoolPMerge(existingChannel.LeaderboardEnabled, channelModifications.LeaderboardEnabled),
		IsCheerLeaderboardEnabled:   pointers.BoolPMerge(existingChannel.IsCheerLeaderboardEnabled, channelModifications.IsCheerLeaderboardEnabled),
		IsSubGiftLeaderboardEnabled: pointers.BoolPMerge(existingChannel.IsSubGiftLeaderboardEnabled, channelModifications.IsSubGiftLeaderboardEnabled),
		DefaultLeaderboard:          pointers.StringPMerge(existingChannel.DefaultLeaderboard, channelModifications.DefaultLeaderboard),
		LeaderboardTimePeriod:       pointers.StringPMerge(existingChannel.LeaderboardTimePeriod, channelModifications.LeaderboardTimePeriod),
	}
	return mergedChannel
}

func TestChannelDao(t *testing.T) {
	if dynamoContext.SkipDynamoTests {
		t.Skip("Skipping Dynamo Tests")
	}
	channelDao, _ := dynamoContext.CreateDefaultChannelDao("test-dao-complete")
	_ = channelDao.SetupTable()

	now := time.Now()
	log.Infof("Saving %d random channel records to dynamo", numChannelsToTest)
	savedChannels := make(map[dynamo.ChannelId]*dynamo.Channel)
	for i := 0; i < numChannelsToTest; i++ {
		channelId := dynamo.ChannelId(test.RandomString(16))
		testChannel := &dynamo.Channel{
			Id:                          channelId,
			MinBits:                     test.RandomInt64OrNil(randomNilChance, 1, 1000),
			MinBitsEmote:                test.RandomInt64OrNil(randomNilChance, 1, 1000),
			OptedOut:                    test.RandomBoolOrNil(randomNilChance),
			Onboarded:                   test.RandomBoolOrNil(randomNilChance),
			Annotation:                  test.RandomStringOrNil(randomNilChance, 16),
			PinRecentCheers:             test.RandomBoolOrNil(randomNilChance),
			RecentCheerMin:              test.RandomInt64OrNil(randomNilChance, 1, 1000),
			RecentCheerTimeout:          test.RandomInt64OrNil(randomNilChance, 1, 1000),
			CustomCheermoteImageSetId:   test.RandomStringOrNil(randomNilChance, 16),
			CustomCheermoteStatus:       test.RandomStringOrNil(randomNilChance, 16),
			OptedOutOfVoldemort:         test.RandomBoolOrNil(randomNilChance),
			LeaderboardEnabled:          test.RandomBoolOrNil(randomNilChance),
			IsCheerLeaderboardEnabled:   test.RandomBoolOrNil(randomNilChance),
			IsSubGiftLeaderboardEnabled: test.RandomBoolOrNil(randomNilChance),
			DefaultLeaderboard:          pointers.StringP(api.DefaultLeaderboardSubGift),
			LeaderboardTimePeriod:       pointers.StringP(pantheonrpc.TimeUnit_name[0]),
		}
		err := channelDao.Update(testChannel)
		if err != nil {
			t.Fail()
		}
		savedChannels[channelId] = testChannel
	}

	log.Infof("Retrieving and verifying channel records from dynamo")
	for channelId, expectedChannel := range savedChannels {
		actualChannel, err := channelDao.Get(channelId)
		if err != nil {
			t.Fail()
		}
		assert.True(t, expectedChannel.EqualsWithExpectedLastUpdated(actualChannel, now, dynamoContext.AcceptableUpdateTimeDelta))
	}

	now = time.Now()
	log.Infof("Modifying existing channel records")
	for existingChannelId, existingChannel := range savedChannels {
		modifiedChannel := &dynamo.Channel{
			Id:                          existingChannelId,
			MinBits:                     test.RandomInt64OrNil(randomNilChance, 1, 1000),
			MinBitsEmote:                test.RandomInt64OrNil(randomNilChance, 1, 1000),
			OptedOut:                    test.RandomBoolOrNil(randomNilChance),
			Onboarded:                   test.RandomBoolOrNil(randomNilChance),
			Annotation:                  test.RandomStringOrNil(randomNilChance, 16),
			PinRecentCheers:             test.RandomBoolOrNil(randomNilChance),
			RecentCheerMin:              test.RandomInt64OrNil(randomNilChance, 1, 1000),
			RecentCheerTimeout:          test.RandomInt64OrNil(randomNilChance, 1, 1000),
			CustomCheermoteImageSetId:   test.RandomStringOrNil(randomNilChance, 16),
			CustomCheermoteStatus:       test.RandomStringOrNil(randomNilChance, 16),
			OptedOutOfVoldemort:         test.RandomBoolOrNil(randomNilChance),
			LeaderboardEnabled:          test.RandomBoolOrNil(randomNilChance),
			IsCheerLeaderboardEnabled:   test.RandomBoolOrNil(randomNilChance),
			IsSubGiftLeaderboardEnabled: test.RandomBoolOrNil(randomNilChance),
			DefaultLeaderboard:          pointers.StringP(api.DefaultLeaderboardSubGift),
			LeaderboardTimePeriod:       pointers.StringP(pantheonrpc.TimeUnit_name[0]),
		}
		err := channelDao.Update(modifiedChannel)
		if err != nil {
			t.Fail()
		}
		savedChannels[existingChannelId] = mergeChannelRecords(existingChannel, modifiedChannel)
	}

	log.Infof("Retrieving and verifying modified channel records from dynamo")
	for channelId, expectedChannel := range savedChannels {
		actualChannel, err := channelDao.Get(channelId)
		if err != nil {
			t.Fail()
		}
		assert.True(t, expectedChannel.EqualsWithExpectedLastUpdated(actualChannel, now, dynamoContext.AcceptableUpdateTimeDelta))
	}

	log.Infof("Deleting channel records from dynamo")
	for channelId := range savedChannels {
		err := channelDao.Delete(channelId)
		assert.NoError(t, err)
	}

	log.Infof("Verifying that channel records no longer exist in dynamo")
	for channelId := range savedChannels {
		actualChannel, err := channelDao.Get(channelId)
		if err != nil {
			t.Fail()
		}
		assert.Nil(t, actualChannel)
	}
}

func TestGetBySetId(t *testing.T) {
	if dynamoContext.SkipDynamoTests {
		t.Skip("Skipping Dynamo Tests")
	}
	channelDao, _ := dynamoContext.CreateDefaultChannelDao("test-get-by-set-id")

	err := channelDao.SetupTable()
	if err != nil {
		assert.NoError(t, err)
		return
	}

	setId := test.RandomString(16)
	testChannel := &dynamo.Channel{
		Id:                          "1234",
		MinBits:                     test.RandomInt64OrNil(randomNilChance, 1, 1000),
		MinBitsEmote:                test.RandomInt64OrNil(randomNilChance, 1, 1000),
		OptedOut:                    test.RandomBoolOrNil(randomNilChance),
		Onboarded:                   test.RandomBoolOrNil(randomNilChance),
		Annotation:                  test.RandomStringOrNil(randomNilChance, 16),
		PinRecentCheers:             test.RandomBoolOrNil(randomNilChance),
		RecentCheerMin:              test.RandomInt64OrNil(randomNilChance, 1, 1000),
		RecentCheerTimeout:          test.RandomInt64OrNil(randomNilChance, 1, 1000),
		CustomCheermoteImageSetId:   &setId,
		CustomCheermoteStatus:       test.RandomStringOrNil(randomNilChance, 16),
		OptedOutOfVoldemort:         test.RandomBoolOrNil(randomNilChance),
		LeaderboardEnabled:          test.RandomBoolOrNil(randomNilChance),
		IsCheerLeaderboardEnabled:   test.RandomBoolOrNil(randomNilChance),
		IsSubGiftLeaderboardEnabled: test.RandomBoolOrNil(randomNilChance),
		DefaultLeaderboard:          pointers.StringP(api.DefaultLeaderboardSubGift),
		LeaderboardTimePeriod:       pointers.StringP(pantheonrpc.TimeUnit_name[0]),
	}

	fmt.Printf("Test Channel: %v \n", testChannel)
	err = channelDao.Update(testChannel)
	if err != nil {
		assert.NoError(t, err)
		return
	}

	retrieved, err := channelDao.GetByImageSetId(setId)
	if err != nil {
		assert.NoError(t, err)
	}

	if retrieved == nil {
		assert.NotNil(t, retrieved)
		return
	}

	assert.Equal(t, testChannel.Id, retrieved.Id)
}

func TestFailsOutOfOrderUpdate(t *testing.T) {
	if dynamoContext.SkipDynamoTests {
		t.Skip("Skipping Dynamo Tests")
	}
	channelDao, _ := dynamoContext.CreateDefaultChannelDao("test-conditional-update-last-updated")

	err := channelDao.SetupTable()
	if err != nil {
		assert.NoError(t, err)
		return
	}

	now := time.Now()
	inAnHour := now.Add(time.Hour)

	setId := test.RandomString(16)
	testChannel := &dynamo.Channel{
		Id:                          "1234",
		MinBits:                     test.RandomInt64OrNil(randomNilChance, 1, 1000),
		MinBitsEmote:                test.RandomInt64OrNil(randomNilChance, 1, 1000),
		OptedOut:                    test.RandomBoolOrNil(randomNilChance),
		Onboarded:                   test.RandomBoolOrNil(randomNilChance),
		Annotation:                  test.RandomStringOrNil(randomNilChance, 16),
		PinRecentCheers:             test.RandomBoolOrNil(randomNilChance),
		RecentCheerMin:              test.RandomInt64OrNil(randomNilChance, 1, 1000),
		RecentCheerTimeout:          test.RandomInt64OrNil(randomNilChance, 1, 1000),
		CustomCheermoteImageSetId:   &setId,
		CustomCheermoteStatus:       test.RandomStringOrNil(randomNilChance, 16),
		OptedOutOfVoldemort:         test.RandomBoolOrNil(randomNilChance),
		LeaderboardEnabled:          test.RandomBoolOrNil(randomNilChance),
		IsCheerLeaderboardEnabled:   test.RandomBoolOrNil(randomNilChance),
		IsSubGiftLeaderboardEnabled: test.RandomBoolOrNil(randomNilChance),
		DefaultLeaderboard:          pointers.StringP(api.DefaultLeaderboardSubGift),
		LeaderboardTimePeriod:       pointers.StringP(pantheonrpc.TimeUnit_name[0]),
	}

	testChannel.LastOnboardedEventTime = pointers.Int64P(inAnHour.UnixNano())

	fmt.Printf("Test Channel: %v \n", testChannel)
	err = channelDao.UpdateIfStatusIsNewer(*testChannel)
	require.NoError(t, err)

	testChannel.LastOnboardedEventTime = pointers.Int64P(now.UnixNano())
	err = channelDao.UpdateIfStatusIsNewer(*testChannel)

	require.Error(t, err)
	ae, ok := err.(awserr.Error)
	require.True(t, ok)
	require.Equal(t, "ConditionalCheckFailedException", ae.Code())

	testChannel.LastOnboardedEventTime = pointers.Int64P(inAnHour.UnixNano())
	currentChannel, err := channelDao.Get(testChannel.Id)
	require.NoError(t, err)
	require.True(t, currentChannel.Equals(testChannel))
}
