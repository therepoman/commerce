package dynamo

import (
	"encoding/json"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestJsonMarshalImageUpdate(t *testing.T) {
	file, _ := os.Open("testdata/image_update.json")
	var tableUpdateRecord TableUpdate
	decoder := json.NewDecoder(file)
	err := decoder.Decode(&tableUpdateRecord)
	if err != nil {
		assert.NoError(t, err)
		return
	}

	table := ImagesTable{}
	record, err := table.ConvertAttributeMapToRecord(tableUpdateRecord.DynamoDB.NewImage)
	if err != nil {
		assert.NoError(t, err)
		return
	}

	image, _ := record.(*Image)
	assert.Equal(t, "31b19dfd-b32c-4037-85f6-ade134453121", image.AssetId)
	assert.Equal(t, "2e0d432b-ce17-4655-bc91-78b88b291933", image.SetId)
}

func TestJsonMarshalExampleUpdate(t *testing.T) {
	file, _ := os.Open("testdata/test_update.json")
	var tableUpdateRecord TableUpdate
	decoder := json.NewDecoder(file)
	err := decoder.Decode(&tableUpdateRecord)
	if err != nil {
		assert.NoError(t, err)
		return
	}
	assert.Equal(t, "2", tableUpdateRecord.EventId)
}

func TestJsonMarshalExampleCreate(t *testing.T) {
	file, _ := os.Open("testdata/test_create.json")
	var tableUpdateRecord TableUpdate
	decoder := json.NewDecoder(file)
	err := decoder.Decode(&tableUpdateRecord)
	if err != nil {
		assert.NoError(t, err)
		return
	}
	assert.Equal(t, "1", tableUpdateRecord.EventId)
}

func TestJsonMarshalExampleDelete(t *testing.T) {
	file, _ := os.Open("testdata/test_delete.json")
	var tableUpdateRecord TableUpdate
	decoder := json.NewDecoder(file)
	err := decoder.Decode(&tableUpdateRecord)
	if err != nil {
		assert.NoError(t, err)
		return
	}
	assert.Equal(t, "3", tableUpdateRecord.EventId)
}
