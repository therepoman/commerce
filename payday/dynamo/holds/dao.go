package holds

import (
	"context"
)

const (
	HashKey  = "user_id"
	RangeKey = "transaction_id"

	RecipientIDKey                     = "id"
	RecipientBitsRevenueAttributionKey = "number_of_bits"
	RecipientRewardAttributionKey      = "reward_attribution"
	RecipientTotalWithBroadcasterKey   = "total_with_broadcaster"

	CreateHoldTransactionType   = "CreateHold"
	FinalizeHoldTransactionType = "FinalizeHold"
	ReleaseHoldTransactionType  = "ReleaseHold"
)

type BalanceHold struct {
	UserID        string `dynamo:"user_id"`
	TransactionID string `dynamo:"transaction_id"` // hash key
	IsActive      bool   `dynamo:"is_active"`

	// transaction fields
	TransactionType       string  `dynamo:"transaction_type"`
	UserIDTransactionType string  `dynamo:"user_id-transaction_type"` // GSI hash key
	LastUpdated           int64   `dynamo:"last_updated"`
	OperatorTransactionID *string `dynamo:"operator_transaction_id"`
	TimeOfEvent           int64   `dynamo:"time_of_event"`
	NumberOfBits          int64   `dynamo:"number_of_bits"`

	Recipients               []*BitsUsageAttribution `dynamo:"recipients"`
	OperandTransactionID     string                  `dynamo:"operand_transaction_id"`
	IgnorePayout             bool                    `dynamo:"ignore_payout"`
	FinalizedTransactionType string                  `dynamo:"finalized_transaction_type"`
}

type BitsUsageAttribution struct {
	// The ID of the broadcaster
	ID string `dynamo:"id" json:"id"`
	// This is the total number of Bits that pay out to the Broadcaster from this hold
	BitsRevenueAttributionToBroadcaster int64 `dynamo:"number_of_bits" json:"number_of_bits"`
	// This is the total number of Bits that go towards Rewards in the broadcaster's channel from this hold
	RewardAttribution int64 `dynamo:"reward_attribution" json:"reward_attribution"`
	// This is the leaderboard entry for the user just prior to processing this transaction
	TotalWithBroadcasterPrior int64 `dynamo:"total_with_broadcaster" json:"total_with_broadcaster"`
}

type DAO interface {
	Create(ctx context.Context, userID, transactionID string, totalBitsUsedAmount int64) (*BalanceHold, error)
	GetHold(ctx context.Context, userID, holdID string) (*BalanceHold, error)
	FinalizeHold(ctx context.Context, transactionID, userID, holdID string, recipients []*BitsUsageAttribution, ignorePayout bool, finalizedTransactionType string, totalBitsUsedAmount int64) error
	ReleaseHold(ctx context.Context, transactionID, userID, holdID string, totalBitsReturned int64) error
	GetActive(ctx context.Context, userID string) ([]*BalanceHold, error)
	GetOperatorTransactionID(ctx context.Context, userID, holdID string) (string, error)
}
