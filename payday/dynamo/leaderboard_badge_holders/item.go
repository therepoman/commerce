package leaderboard_badge_holders

import (
	"time"

	"code.justin.tv/commerce/payday/dynamo"
	timeUtils "code.justin.tv/commerce/payday/utils/time"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

type LeaderboardBadgeHolders struct {
	ChannelID         string
	FirstPlaceUserID  string
	SecondPlaceUserID string
	ThirdPlaceUserID  string
	LastUpdated       time.Time
}

func (l *LeaderboardBadgeHolders) GetTable() dynamo.DynamoTable {
	return &LeaderboardBadgeHoldersTable{}
}

func (scss *LeaderboardBadgeHolders) GetScanQueryTable() dynamo.DynamoScanQueryTable {
	return &LeaderboardBadgeHoldersTable{}
}

func (l *LeaderboardBadgeHolders) NewAttributeMap() map[string]*dynamodb.AttributeValue {
	itemMap := make(map[string]*dynamodb.AttributeValue)

	// Required attributes
	dynamo.PopulateAttributesString(itemMap, "channelID", string(l.ChannelID))
	dynamo.PopulateAttributesTime(itemMap, "lastUpdated", l.LastUpdated)

	// Optional attributes
	dynamo.PopulateAttributesString(itemMap, "firstPlaceUserID", l.FirstPlaceUserID)
	dynamo.PopulateAttributesString(itemMap, "secondPlaceUserID", l.SecondPlaceUserID)
	dynamo.PopulateAttributesString(itemMap, "thirdPlaceUserID", l.ThirdPlaceUserID)

	return itemMap
}

func (l *LeaderboardBadgeHolders) NewItemKey() map[string]*dynamodb.AttributeValue {
	keyMap := make(map[string]*dynamodb.AttributeValue)
	keyMap["channelID"] = &dynamodb.AttributeValue{S: aws.String(string(l.ChannelID))}
	return keyMap
}

func (l *LeaderboardBadgeHolders) NewHashKeyEqualsExpression() string {
	return "channelID = :channelID"
}

func (l *LeaderboardBadgeHolders) NewHashKeyExpressionAttributeValues() map[string]*dynamodb.AttributeValue {
	attributeValue := &dynamodb.AttributeValue{
		S: aws.String(string(l.ChannelID)),
	}
	attributeMap := make(map[string]*dynamodb.AttributeValue)
	attributeMap[":channelID"] = attributeValue
	return attributeMap
}

func (l *LeaderboardBadgeHolders) UpdateWithCurrentTimestamp() {
	l.LastUpdated = time.Now()
}

func (l *LeaderboardBadgeHolders) GetTimestamp() time.Time {
	return l.LastUpdated
}

func (l *LeaderboardBadgeHolders) ApplyTimestamp(timestamp time.Time) {
	l.LastUpdated = timestamp
}

func (l *LeaderboardBadgeHolders) Equals(other dynamo.DynamoTableRecord) bool {
	otherLB, isLB := other.(*LeaderboardBadgeHolders)
	if !isLB {
		return false
	}

	return l.ChannelID == otherLB.ChannelID &&
		l.FirstPlaceUserID == otherLB.FirstPlaceUserID &&
		l.SecondPlaceUserID == otherLB.SecondPlaceUserID &&
		l.ThirdPlaceUserID == otherLB.ThirdPlaceUserID
}

func (l *LeaderboardBadgeHolders) EqualsWithExpectedLastUpdated(other dynamo.DynamoTableRecord, lastUpdatedTime time.Time, lastUpdatedDelta time.Duration) bool {
	return l.Equals(other) && timeUtils.IsTimeInForwardDelta(other.GetTimestamp(), lastUpdatedTime, lastUpdatedDelta)
}
