package leaderboard_badge_holders

import (
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/hystrix"
	cmd "code.justin.tv/commerce/payday/hystrix"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

type LeaderboardBadgeHoldersDao struct {
	dynamo.BaseDao
	dynamo.DynamoTable
	dynamo.DynamoScanQueryTable
}

type ILeaderboardBadgeHoldersDao interface {
	Put(badges *LeaderboardBadgeHolders) error
	Get(channelId string) (*LeaderboardBadgeHolders, error)
	QueryByFirstPlaceUserID(firstPlaceUserID string, limit int, pagingKey *string) ([]LeaderboardBadgeHolders, *string, error)
	QueryBySecondPlaceUserID(secondPlaceUserID string, limit int, pagingKey *string) ([]LeaderboardBadgeHolders, *string, error)
	QueryByThirdPlaceUserID(thirdPlaceUserID string, limit int, pagingKey *string) ([]LeaderboardBadgeHolders, *string, error)
	BatchPut([]LeaderboardBadgeHolders) error
	Delete(channelId string) error
	GetClientConfig() *dynamo.DynamoClientConfig
	GetLatestStreamArn() (string, error)
	DeleteTable() error
	SetupTable() error
}

type GSIName string
type GSIHashKey string

const (
	FirstPlaceUserIDIndex  GSIName    = "leaderboard_badge_holders_firstPlaceUserID-index"
	SecondPlaceUserIDIndex GSIName    = "leaderboard_badge_holders_secondPlaceUserID-index"
	ThirdPlaceUserIDIndex  GSIName    = "leaderboard_badge_holders_thirdPlaceUserID-index"
	FirstPlaceUserIDKey    GSIHashKey = "firstPlaceUserID"
	SecondPlaceUserIDKey   GSIHashKey = "secondPlaceUserID"
	ThirdPlaceUserIDKey    GSIHashKey = "thirdPlaceUserID"
)

func NewLeaderboardBadgesDao(client *dynamo.DynamoClient) ILeaderboardBadgeHoldersDao {
	table := &LeaderboardBadgeHoldersTable{}
	item := &LeaderboardBadgeHolders{}
	queryScanTable := item.GetScanQueryTable()

	return &LeaderboardBadgeHoldersDao{
		BaseDao:              dynamo.NewBaseDao(table, client),
		DynamoTable:          table,
		DynamoScanQueryTable: queryScanTable,
	}
}

func (d *LeaderboardBadgeHoldersDao) Put(badges *LeaderboardBadgeHolders) error {
	return d.GetClient().PutItem(badges)
}

func (d *LeaderboardBadgeHoldersDao) Get(channelID string) (*LeaderboardBadgeHolders, error) {
	leaderboardBadgeHoldersChan := make(chan *LeaderboardBadgeHolders, 1)

	fn := func() error {
		leaderboardBadgeHolders, err := d.get(channelID)
		if err != nil {
			return err
		}
		leaderboardBadgeHoldersChan <- leaderboardBadgeHolders
		return nil
	}

	err := hystrix.Do(cmd.GetLeaderboardBadgeHoldersCommand, fn, nil)
	if err != nil {
		return nil, err
	}

	leaderboardBadgeHolders := <-leaderboardBadgeHoldersChan
	return leaderboardBadgeHolders, nil
}

func (d *LeaderboardBadgeHoldersDao) get(channelID string) (*LeaderboardBadgeHolders, error) {
	leaderboardBadgeHoldersQuery := &LeaderboardBadgeHolders{
		ChannelID: channelID,
	}

	result, err := d.GetClient().GetItem(leaderboardBadgeHoldersQuery)
	if err != nil {
		return nil, err
	}

	if result == nil {
		return nil, nil
	}

	leaderboardBadgeHoldersResult, isLeaderboardBadgeHolders := result.(*LeaderboardBadgeHolders)
	if !isLeaderboardBadgeHolders {
		msg := "encountered an unexpected type while converting dynamo result to leaderboard badge holders"
		log.Error(msg)
		return nil, errors.New(msg)
	}

	return leaderboardBadgeHoldersResult, nil
}

func (d *LeaderboardBadgeHoldersDao) QueryByFirstPlaceUserID(firstPlaceUserID string, limit int, pagingKey *string) ([]LeaderboardBadgeHolders, *string, error) {
	return d.queryGSIWithLimit(FirstPlaceUserIDIndex, firstPlaceUserID, int64(limit), pagingKey)
}
func (d *LeaderboardBadgeHoldersDao) QueryBySecondPlaceUserID(secondPlaceUserID string, limit int, pagingKey *string) ([]LeaderboardBadgeHolders, *string, error) {
	return d.queryGSIWithLimit(SecondPlaceUserIDIndex, secondPlaceUserID, int64(limit), pagingKey)
}
func (d *LeaderboardBadgeHoldersDao) QueryByThirdPlaceUserID(thirdPlaceUserID string, limit int, pagingKey *string) ([]LeaderboardBadgeHolders, *string, error) {
	return d.queryGSIWithLimit(ThirdPlaceUserIDIndex, thirdPlaceUserID, int64(limit), pagingKey)
}

func (d *LeaderboardBadgeHoldersDao) queryGSIWithLimit(gsiName GSIName, channelID string, limit int64, pagingKey *string) ([]LeaderboardBadgeHolders, *string, error) {
	var leaderboardBadgeHolders []LeaderboardBadgeHolders

	sortKeyForGSI := "channelID"
	var hashKeyForGSI string
	switch gsiName {
	case FirstPlaceUserIDIndex:
		hashKeyForGSI = string(FirstPlaceUserIDKey)
	case SecondPlaceUserIDIndex:
		hashKeyForGSI = string(SecondPlaceUserIDKey)
	case ThirdPlaceUserIDIndex:
		hashKeyForGSI = string(ThirdPlaceUserIDKey)
	default:
		return leaderboardBadgeHolders, nil, errors.Newf("Invalid GSIName: %s", gsiName)
	}

	var exclusiveStartKey map[string]*dynamodb.AttributeValue
	if pagingKey != nil {
		exclusiveStartKey = map[string]*dynamodb.AttributeValue{
			hashKeyForGSI: {S: aws.String(channelID)},
			sortKeyForGSI: {S: pagingKey},
		}
	}

	query := dynamo.QueryFilter{
		Names: map[string]*string{
			"#HashKey": aws.String(hashKeyForGSI),
		},
		Values: map[string]*dynamodb.AttributeValue{
			":id": {S: aws.String(channelID)},
		},
		IndexName:              aws.String(string(gsiName)),
		KeyConditionExpression: aws.String("#HashKey = :id"),
		Limit:                  aws.Int64(limit),
		ExclusiveStartKey:      exclusiveStartKey,
	}

	results, lastEvaluatedKey, err := d.GetClient().QueryTable(d.DynamoScanQueryTable, query)
	if err != nil {
		return leaderboardBadgeHolders, nil, err
	}
	var newPagingKey *string
	if lastEvaluatedKey != nil {
		newPagingKey = lastEvaluatedKey[sortKeyForGSI].S
	}

	for _, result := range results {
		leaderboardBadgeHoldersResult, isLeaderboardBadgeHolders := result.(*LeaderboardBadgeHolders)
		if !isLeaderboardBadgeHolders {
			msg := "Encountered an unexpected type while converting dynamo result to leaderboard badge holders"
			log.Error(msg)
			return leaderboardBadgeHolders, newPagingKey, errors.New(msg)
		}
		leaderboardBadgeHolders = append(leaderboardBadgeHolders, *leaderboardBadgeHoldersResult)
	}

	return leaderboardBadgeHolders, newPagingKey, nil
}

func (d *LeaderboardBadgeHoldersDao) BatchPut(leaderboardBadgeHolders []LeaderboardBadgeHolders) error {
	updateQuery := make([]dynamo.DynamoTableRecord, 0)
	for i := range leaderboardBadgeHolders {
		updateQuery = append(updateQuery, &leaderboardBadgeHolders[i])
	}
	return d.GetClient().BatchPutItem(updateQuery)
}

func (d *LeaderboardBadgeHoldersDao) Delete(channelID string) error {
	return d.GetClient().DeleteItem(&LeaderboardBadgeHolders{
		ChannelID: channelID,
	})
}
