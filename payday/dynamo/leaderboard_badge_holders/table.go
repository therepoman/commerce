package leaderboard_badge_holders

import (
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/errors"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

type LeaderboardBadgeHoldersTable struct{}

func (t *LeaderboardBadgeHoldersTable) GetBaseTableName() dynamo.BaseTableName {
	return "leaderboard_badge_holders"
}

func (t *LeaderboardBadgeHoldersTable) GetNodeJsRecordKeySelector() dynamo.NodeJsRecordKeySelector {
	return "record.dynamodb.Keys.channelID.S"
}

func (t *LeaderboardBadgeHoldersTable) NewCreateTableInput(name dynamo.FullTableName, read dynamo.ReadCapacity, write dynamo.WriteCapacity) *dynamodb.CreateTableInput {
	return &dynamodb.CreateTableInput{
		TableName: aws.String(string(name)),
		ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
			ReadCapacityUnits:  aws.Int64(int64(read)),
			WriteCapacityUnits: aws.Int64(int64(write)),
		},
		AttributeDefinitions: []*dynamodb.AttributeDefinition{
			{
				AttributeName: aws.String("channelID"),
				AttributeType: aws.String("S"),
			},
			{
				AttributeName: aws.String(string(FirstPlaceUserIDKey)),
				AttributeType: aws.String("S"),
			},
			{
				AttributeName: aws.String(string(SecondPlaceUserIDKey)),
				AttributeType: aws.String("S"),
			},
			{
				AttributeName: aws.String(string(ThirdPlaceUserIDKey)),
				AttributeType: aws.String("S"),
			},
		},
		KeySchema: []*dynamodb.KeySchemaElement{
			{
				AttributeName: aws.String("channelID"),
				KeyType:       aws.String("HASH"),
			},
		},
		StreamSpecification: &dynamodb.StreamSpecification{
			StreamEnabled:  aws.Bool(true),
			StreamViewType: aws.String(dynamodb.StreamViewTypeNewAndOldImages),
		},
		GlobalSecondaryIndexes: []*dynamodb.GlobalSecondaryIndex{
			{
				IndexName: aws.String(string(FirstPlaceUserIDIndex)),
				KeySchema: []*dynamodb.KeySchemaElement{
					{
						AttributeName: aws.String(string(FirstPlaceUserIDKey)),
						KeyType:       aws.String("HASH"),
					},
				},
				Projection: &dynamodb.Projection{
					ProjectionType: aws.String("ALL"),
				},
				ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
					ReadCapacityUnits:  aws.Int64(int64(read)),
					WriteCapacityUnits: aws.Int64(int64(write)),
				},
			},
			{
				IndexName: aws.String(string(SecondPlaceUserIDIndex)),
				KeySchema: []*dynamodb.KeySchemaElement{
					{
						AttributeName: aws.String(string(SecondPlaceUserIDKey)),
						KeyType:       aws.String("HASH"),
					},
				},
				Projection: &dynamodb.Projection{
					ProjectionType: aws.String("ALL"),
				},
				ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
					ReadCapacityUnits:  aws.Int64(int64(read)),
					WriteCapacityUnits: aws.Int64(int64(write)),
				},
			},
			{
				IndexName: aws.String(string(ThirdPlaceUserIDIndex)),
				KeySchema: []*dynamodb.KeySchemaElement{
					{
						AttributeName: aws.String(string(ThirdPlaceUserIDKey)),
						KeyType:       aws.String("HASH"),
					},
				},
				Projection: &dynamodb.Projection{
					ProjectionType: aws.String("ALL"),
				},
				ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
					ReadCapacityUnits:  aws.Int64(int64(read)),
					WriteCapacityUnits: aws.Int64(int64(write)),
				},
			},
		},
	}
}

func (t *LeaderboardBadgeHoldersTable) ConvertAttributeMapToRecord(attributeMap map[string]*dynamodb.AttributeValue) (dynamo.DynamoTableRecord, error) {
	return &LeaderboardBadgeHolders{
		ChannelID:         dynamo.StringFromAttributes(attributeMap, "channelID"),
		FirstPlaceUserID:  dynamo.StringFromAttributes(attributeMap, "firstPlaceUserID"),
		SecondPlaceUserID: dynamo.StringFromAttributes(attributeMap, "secondPlaceUserID"),
		ThirdPlaceUserID:  dynamo.StringFromAttributes(attributeMap, "thirdPlaceUserID"),
	}, nil
}

func (sccst *LeaderboardBadgeHoldersTable) ConvertScanOutputToRecords(so *dynamodb.ScanOutput) ([]dynamo.DynamoScanQueryTableRecord, error) {
	records := make([]dynamo.DynamoScanQueryTableRecord, *so.Count)

	for i, attributes := range so.Items {
		recordToAdd, err := sccst.ConvertAttributeMapToRecord(attributes)
		if err != nil {
			return nil, err
		}

		scanQueryRecord, err := recordToScanQueryRecord(recordToAdd)
		if err != nil {
			return nil, err
		}

		records[i] = scanQueryRecord
	}
	return records, nil
}

func (sccst *LeaderboardBadgeHoldersTable) ConvertQueryOutputToRecords(qo *dynamodb.QueryOutput) ([]dynamo.DynamoScanQueryTableRecord, error) {
	records := make([]dynamo.DynamoScanQueryTableRecord, *qo.Count)

	for i, attributes := range qo.Items {
		recordToAdd, err := sccst.ConvertAttributeMapToRecord(attributes)
		if err != nil {
			return nil, err
		}

		scanQueryRecord, err := recordToScanQueryRecord(recordToAdd)
		if err != nil {
			return nil, err
		}

		records[i] = scanQueryRecord
	}
	return records, nil
}

func recordToScanQueryRecord(record dynamo.DynamoTableRecord) (dynamo.DynamoScanQueryTableRecord, error) {
	scanQueryRecord, isScanQueryRecord := record.(dynamo.DynamoScanQueryTableRecord)
	if !isScanQueryRecord {
		return nil, errors.New("record not a scanQueryRecord")
	}
	return scanQueryRecord, nil
}
