package eligible_channels

import (
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/errors"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

type SponsoredCheermoteEligibleChannelsDao struct {
	dynamo.BaseDao
	dynamo.DynamoTable
	dynamo.DynamoScanQueryTable
}

type ISponsoredCheermoteEligibleChannelsDao interface {
	Get(channelID string, campaignID string) (*SponsoredCheermoteEligibleChannel, error)
	QueryWithLimit(channelID string, limit int64, pagingKey *string) ([]*SponsoredCheermoteEligibleChannel, *string, error)
	Update(sponsoredCheermoteEligibleChannel *SponsoredCheermoteEligibleChannel) error
	Delete(channelID string, campaignID string) error
	BatchDelete([]*SponsoredCheermoteEligibleChannel) error
	DeleteTable() error
	SetupTable() error
	GetClientConfig() *dynamo.DynamoClientConfig
	GetLatestStreamArn() (string, error)
}

func NewSponsoredCheermoteEligibleChannelsDao(client *dynamo.DynamoClient) ISponsoredCheermoteEligibleChannelsDao {
	table := &SponsoredCheermoteEligibleChannelsTable{}
	item := &SponsoredCheermoteEligibleChannel{}
	queryScanTable := item.GetScanQueryTable()

	return &SponsoredCheermoteEligibleChannelsDao{
		BaseDao:              dynamo.NewBaseDao(table, client),
		DynamoTable:          table,
		DynamoScanQueryTable: queryScanTable,
	}
}

func (this *SponsoredCheermoteEligibleChannelsDao) Get(channelID string, campaignID string) (*SponsoredCheermoteEligibleChannel, error) {
	var eligibleChannelResult *SponsoredCheermoteEligibleChannel

	eligibleChannelQuery := &SponsoredCheermoteEligibleChannel{
		ChannelID:  channelID,
		CampaignID: campaignID,
	}

	result, err := this.GetClient().GetItem(eligibleChannelQuery)
	if err != nil {
		return eligibleChannelResult, err
	}

	if result == nil {
		return nil, nil
	}

	eligibleChannelResult, ok := result.(*SponsoredCheermoteEligibleChannel)
	if !ok {
		msg := "Encountered an unexpected type while converting dynamo query result to sponsored cheermote eligible channel"
		log.Error(msg)
		return eligibleChannelResult, errors.New(msg)
	}

	return eligibleChannelResult, nil
}

func (this *SponsoredCheermoteEligibleChannelsDao) QueryWithLimit(channelID string, limit int64, pagingKey *string) ([]*SponsoredCheermoteEligibleChannel, *string, error) {
	var eligibleChannelResults []*SponsoredCheermoteEligibleChannel
	var exclusiveStartKey map[string]*dynamodb.AttributeValue
	if pagingKey != nil {
		exclusiveStartKey = map[string]*dynamodb.AttributeValue{
			"campaignId": {S: pagingKey},
			"channelId":  {S: aws.String(channelID)},
		}
	}

	query := dynamo.QueryFilter{
		Names: map[string]*string{
			"#ID": aws.String("channelId"),
		},
		Values: map[string]*dynamodb.AttributeValue{
			":id": {S: aws.String(channelID)},
		},
		KeyConditionExpression: aws.String("#ID = :id"),
		Limit:                  aws.Int64(limit),
		ExclusiveStartKey:      exclusiveStartKey,
	}
	results, lastEvaluatedKey, err := this.GetClient().QueryTable(this.DynamoScanQueryTable, query)
	if err != nil {
		return eligibleChannelResults, nil, err
	}
	var newPagingKey *string
	if lastEvaluatedKey != nil {
		newPagingKey = lastEvaluatedKey["campaignId"].S
	}

	for _, eligibleChannel := range results {
		eligibleChannelResult, isEligibleCampaign := eligibleChannel.(*SponsoredCheermoteEligibleChannel)
		if !isEligibleCampaign {
			msg := "Encountered an unexpected type while converting dynamo result to sponsored cheermote channel eligibility entry"
			log.Error(msg)
			return eligibleChannelResults, newPagingKey, errors.New(msg)
		}
		eligibleChannelResults = append(eligibleChannelResults, eligibleChannelResult)
	}

	return eligibleChannelResults, newPagingKey, nil
}

func (this *SponsoredCheermoteEligibleChannelsDao) Update(sponsoredCheermoteEligibleChannel *SponsoredCheermoteEligibleChannel) error {
	return this.GetClient().UpdateItem(sponsoredCheermoteEligibleChannel)
}

func (this *SponsoredCheermoteEligibleChannelsDao) Delete(channelID string, campaignID string) error {
	deletionRecord := &SponsoredCheermoteEligibleChannel{
		ChannelID:  channelID,
		CampaignID: campaignID,
	}
	return this.GetClient().DeleteItem(deletionRecord)
}

func (this *SponsoredCheermoteEligibleChannelsDao) BatchDelete(sponsoredCheermoteEligibleChannels []*SponsoredCheermoteEligibleChannel) error {
	deleteQuery := make([]dynamo.DynamoTableRecord, 0)
	for _, eligibleChannel := range sponsoredCheermoteEligibleChannels {
		deleteQuery = append(deleteQuery, eligibleChannel)
	}
	return this.GetClient().BatchDeleteItem(deleteQuery)
}
