package eligible_channels

import (
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/errors"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

type SponsoredCheermoteEligibleChannelsTable struct {
}

func (this *SponsoredCheermoteEligibleChannelsTable) GetBaseTableName() dynamo.BaseTableName {
	return "sponsored_cheermote_eligible_channels"
}

func (this *SponsoredCheermoteEligibleChannelsTable) GetNodeJsRecordKeySelector() dynamo.NodeJsRecordKeySelector {
	return "record.dynamodb.Keys.channelId.S + '_' + record.dynamodb.Keys.campaignId.S"
}

func (this *SponsoredCheermoteEligibleChannelsTable) NewCreateTableInput(name dynamo.FullTableName, read dynamo.ReadCapacity, write dynamo.WriteCapacity) *dynamodb.CreateTableInput {
	return &dynamodb.CreateTableInput{
		TableName: aws.String(string(name)),
		ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
			ReadCapacityUnits:  aws.Int64(int64(read)),
			WriteCapacityUnits: aws.Int64(int64(write)),
		},
		AttributeDefinitions: []*dynamodb.AttributeDefinition{
			{
				AttributeName: aws.String("channelId"),
				AttributeType: aws.String("S"),
			},
			{
				AttributeName: aws.String("campaignId"),
				AttributeType: aws.String("S"),
			},
		},
		KeySchema: []*dynamodb.KeySchemaElement{
			{
				AttributeName: aws.String("channelId"),
				KeyType:       aws.String("HASH"),
			},
			{
				AttributeName: aws.String("campaignId"),
				KeyType:       aws.String("RANGE"),
			},
		},
		StreamSpecification: &dynamodb.StreamSpecification{
			StreamEnabled:  aws.Bool(true),
			StreamViewType: aws.String(dynamodb.StreamViewTypeNewAndOldImages),
		},
	}
}

func (this *SponsoredCheermoteEligibleChannelsTable) ConvertAttributeMapToRecord(attributeMap map[string]*dynamodb.AttributeValue) (dynamo.DynamoTableRecord, error) {
	lastUpdated, err := dynamo.TimeFromAttributes(attributeMap, "lastUpdated")
	if err != nil {
		return nil, err
	}

	return &SponsoredCheermoteEligibleChannel{
		ChannelID:   dynamo.StringFromAttributes(attributeMap, "channelId"),
		CampaignID:  dynamo.StringFromAttributes(attributeMap, "campaignId"),
		LastUpdated: lastUpdated,
	}, nil
}

func (this *SponsoredCheermoteEligibleChannelsTable) ConvertScanOutputToRecords(so *dynamodb.ScanOutput) ([]dynamo.DynamoScanQueryTableRecord, error) {
	records := make([]dynamo.DynamoScanQueryTableRecord, *so.Count)

	for i, attributes := range so.Items {
		recordToAdd, err := this.ConvertAttributeMapToRecord(attributes)
		if err != nil {
			return nil, err
		}

		scanQueryRecord, err := recordToScanQueryRecord(recordToAdd)
		if err != nil {
			return nil, err
		}

		records[i] = scanQueryRecord
	}
	return records, nil
}

func (this *SponsoredCheermoteEligibleChannelsTable) ConvertQueryOutputToRecords(qo *dynamodb.QueryOutput) ([]dynamo.DynamoScanQueryTableRecord, error) {
	records := make([]dynamo.DynamoScanQueryTableRecord, *qo.Count)

	for i, attributes := range qo.Items {
		recordToAdd, err := this.ConvertAttributeMapToRecord(attributes)
		if err != nil {
			return nil, err
		}

		scanQueryRecord, err := recordToScanQueryRecord(recordToAdd)
		if err != nil {
			return nil, err
		}

		records[i] = scanQueryRecord
	}
	return records, nil
}

func recordToScanQueryRecord(record dynamo.DynamoTableRecord) (dynamo.DynamoScanQueryTableRecord, error) {
	scanQueryRecord, isScanQueryRecord := record.(dynamo.DynamoScanQueryTableRecord)
	if !isScanQueryRecord {
		return nil, errors.New("record not a scanQueryRecord")
	}
	return scanQueryRecord, nil
}
