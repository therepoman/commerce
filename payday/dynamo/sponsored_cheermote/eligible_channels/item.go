package eligible_channels

import (
	"time"

	"code.justin.tv/commerce/payday/dynamo"
	timeUtils "code.justin.tv/commerce/payday/utils/time"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

type SponsoredCheermoteEligibleChannel struct {
	ChannelID   string
	CampaignID  string
	LastUpdated time.Time
}

func (this *SponsoredCheermoteEligibleChannel) GetTable() dynamo.DynamoTable {
	return &SponsoredCheermoteEligibleChannelsTable{}
}

func (this *SponsoredCheermoteEligibleChannel) GetScanQueryTable() dynamo.DynamoScanQueryTable {
	return &SponsoredCheermoteEligibleChannelsTable{}
}

func (this *SponsoredCheermoteEligibleChannel) NewAttributeMap() map[string]*dynamodb.AttributeValue {
	itemMap := make(map[string]*dynamodb.AttributeValue)
	dynamo.PopulateAttributesString(itemMap, "channelId", this.ChannelID)
	dynamo.PopulateAttributesString(itemMap, "campaignId", this.CampaignID)
	dynamo.PopulateAttributesTime(itemMap, "lastUpdated", this.LastUpdated)
	return itemMap
}

func (this *SponsoredCheermoteEligibleChannel) NewItemKey() map[string]*dynamodb.AttributeValue {
	keyMap := make(map[string]*dynamodb.AttributeValue)
	dynamo.PopulateAttributesString(keyMap, "channelId", this.ChannelID)
	dynamo.PopulateAttributesString(keyMap, "campaignId", this.CampaignID)

	return keyMap
}

func (this *SponsoredCheermoteEligibleChannel) NewHashKeyEqualsExpression() string {
	return "channelId = :channelId"
}

func (this *SponsoredCheermoteEligibleChannel) NewHashKeyExpressionAttributeValues() map[string]*dynamodb.AttributeValue {
	attributeMap := make(map[string]*dynamodb.AttributeValue)
	dynamo.PopulateAttributesString(attributeMap, "channelId", this.ChannelID)

	return attributeMap
}

func (this *SponsoredCheermoteEligibleChannel) EqualsWithExpectedLastUpdated(other dynamo.DynamoTableRecord, expectedLastUpdatedTime time.Time, acceptableTimeDelta time.Duration) bool {
	return this.Equals(other) && timeUtils.IsTimeInForwardDelta(other.GetTimestamp(), expectedLastUpdatedTime, acceptableTimeDelta)
}

func (this *SponsoredCheermoteEligibleChannel) GetTimestamp() time.Time {
	return this.LastUpdated
}

func (this *SponsoredCheermoteEligibleChannel) UpdateWithCurrentTimestamp() {
	this.LastUpdated = time.Now()
}

func (this *SponsoredCheermoteEligibleChannel) ApplyTimestamp(timestamp time.Time) {
	this.LastUpdated = timestamp
}

func (this *SponsoredCheermoteEligibleChannel) Equals(other dynamo.DynamoTableRecord) bool {
	otherSponsoredCheermoteEligibleChannel, ok := other.(*SponsoredCheermoteEligibleChannel)
	if !ok {
		return false
	}

	return this.ChannelID == otherSponsoredCheermoteEligibleChannel.ChannelID &&
		this.CampaignID == otherSponsoredCheermoteEligibleChannel.CampaignID
}
