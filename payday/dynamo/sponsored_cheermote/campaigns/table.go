package campaigns

import (
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/errors"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

type SponsoredCheermoteCampaignsTable struct {
}

func (a *SponsoredCheermoteCampaignsTable) GetBaseTableName() dynamo.BaseTableName {
	return "sponsored_cheermote_campaigns"
}

func (a *SponsoredCheermoteCampaignsTable) GetNodeJsRecordKeySelector() dynamo.NodeJsRecordKeySelector {
	return "record.dynamodb.Keys.campaignId.S"
}

func (a *SponsoredCheermoteCampaignsTable) NewCreateTableInput(name dynamo.FullTableName, read dynamo.ReadCapacity, write dynamo.WriteCapacity) *dynamodb.CreateTableInput {
	return &dynamodb.CreateTableInput{
		TableName: aws.String(string(name)),
		ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
			ReadCapacityUnits:  aws.Int64(int64(read)),
			WriteCapacityUnits: aws.Int64(int64(write)),
		},
		AttributeDefinitions: []*dynamodb.AttributeDefinition{
			{
				AttributeName: aws.String("campaignId"),
				AttributeType: aws.String("S"),
			},
		},
		KeySchema: []*dynamodb.KeySchemaElement{
			{
				AttributeName: aws.String("campaignId"),
				KeyType:       aws.String("HASH"),
			},
		},
		StreamSpecification: &dynamodb.StreamSpecification{
			StreamEnabled:  aws.Bool(true),
			StreamViewType: aws.String(dynamodb.StreamViewTypeNewAndOldImages),
		},
	}
}

func (a *SponsoredCheermoteCampaignsTable) ConvertAttributeMapToRecord(attributeMap map[string]*dynamodb.AttributeValue) (dynamo.DynamoTableRecord, error) {
	usedBits, err := dynamo.Int64FromAttributes(attributeMap, "usedBits")
	if err != nil {
		return nil, err
	}

	totalBits, err := dynamo.Int64FromAttributes(attributeMap, "totalBits")
	if err != nil {
		return nil, err
	}

	startTime, err := dynamo.TimeFromAttributes(attributeMap, "startTime")
	if err != nil {
		return nil, err
	}

	endTime, err := dynamo.TimeFromAttributes(attributeMap, "endTime")
	if err != nil {
		return nil, err
	}

	lastUpdated, err := dynamo.TimeFromAttributes(attributeMap, "lastUpdated")
	if err != nil {
		return nil, err
	}

	enabled := dynamo.BoolFromAttributes(attributeMap, "enabled")

	minSponsoredAmount, err := dynamo.Int64FromAttributes(attributeMap, "minSponsoredAmount")
	if err != nil {
		return nil, err
	}

	sponsoredAmountThresholds, err := dynamo.MapInt64ToFloat64FromAttribute(attributeMap, "sponsoredAmountThresholds")
	if err != nil {
		return nil, err
	}

	userLimit, err := dynamo.Int64FromAttributes(attributeMap, "userLimit")
	if err != nil {
		return nil, err
	}

	brandName := dynamo.StringFromAttributes(attributeMap, "brandName")
	brandImageURL := dynamo.StringFromAttributes(attributeMap, "brandImageURL")
	eligibilityType := dynamo.StringFromAttributes(attributeMap, "eligibilityType")

	channelBlacklist, err := dynamo.MapStringToBoolFromAttribute(attributeMap, "channelBlacklist")
	if err != nil {
		return nil, err
	}

	return &SponsoredCheermoteCampaign{
		CampaignID:                dynamo.StringFromAttributes(attributeMap, "campaignId"),
		Prefix:                    dynamo.StringFromAttributes(attributeMap, "prefix"),
		UsedBits:                  usedBits,
		TotalBits:                 totalBits,
		StartTime:                 startTime,
		EndTime:                   endTime,
		LastUpdated:               lastUpdated,
		IsEnabled:                 enabled,
		MinBitsToBeSponsored:      minSponsoredAmount,
		SponsoredAmountThresholds: sponsoredAmountThresholds,
		UserLimit:                 userLimit,
		BrandName:                 brandName,
		BrandImageURL:             brandImageURL,
		EligibilityType:           eligibilityType,
		ChannelBlacklist:          channelBlacklist,
	}, nil
}

func (scc *SponsoredCheermoteCampaign) GetTable() dynamo.DynamoTable {
	return &SponsoredCheermoteCampaignsTable{}
}

func (scc *SponsoredCheermoteCampaign) GetScanQueryTable() dynamo.DynamoScanQueryTable {
	return &SponsoredCheermoteCampaignsTable{}
}

func (a *SponsoredCheermoteCampaignsTable) ConvertScanOutputToRecords(so *dynamodb.ScanOutput) ([]dynamo.DynamoScanQueryTableRecord, error) {
	records := make([]dynamo.DynamoScanQueryTableRecord, *so.Count)

	for i, attributes := range so.Items {
		recordToAdd, err := a.ConvertAttributeMapToRecord(attributes)
		if err != nil {
			return nil, err
		}

		scanQueryRecord, err := recordToScanQueryRecord(recordToAdd)
		if err != nil {
			return nil, err
		}

		records[i] = scanQueryRecord
	}
	return records, nil
}

func (a *SponsoredCheermoteCampaignsTable) ConvertQueryOutputToRecords(qo *dynamodb.QueryOutput) ([]dynamo.DynamoScanQueryTableRecord, error) {
	records := make([]dynamo.DynamoScanQueryTableRecord, *qo.Count)

	for i, attributes := range qo.Items {
		recordToAdd, err := a.ConvertAttributeMapToRecord(attributes)
		if err != nil {
			return nil, err
		}

		scanQueryRecord, err := recordToScanQueryRecord(recordToAdd)
		if err != nil {
			return nil, err
		}

		records[i] = scanQueryRecord
	}
	return records, nil
}

func recordToScanQueryRecord(record dynamo.DynamoTableRecord) (dynamo.DynamoScanQueryTableRecord, error) {
	scanQueryRecord, isScanQueryRecord := record.(dynamo.DynamoScanQueryTableRecord)
	if !isScanQueryRecord {
		return nil, errors.New("record not a scanQueryRecord")
	}
	return scanQueryRecord, nil
}
