package campaigns

import (
	"time"

	"code.justin.tv/commerce/payday/dynamo"
	timeUtils "code.justin.tv/commerce/payday/utils/time"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

type SponsoredCheermoteCampaign struct {
	CampaignID                string
	Prefix                    string
	TotalBits                 int64
	UsedBits                  int64
	StartTime                 time.Time
	EndTime                   time.Time
	LastUpdated               time.Time
	IsEnabled                 bool
	MinBitsToBeSponsored      int64
	SponsoredAmountThresholds map[int64]float64
	UserLimit                 int64
	BrandName                 string
	BrandImageURL             string
	PubSubRateLimitSeconds    int64

	EligibilityType  string
	ChannelBlacklist map[string]bool
}

const SponsoredCheermoteEligibilityTypeWhitelist = "Whitelist"
const SponsoredCheermoteEligibilityTypeBlacklist = "Blacklist"
const SponsoredCheermoteEligibilityTypeDefault = SponsoredCheermoteEligibilityTypeWhitelist

func (scc *SponsoredCheermoteCampaign) NewAttributeMap() map[string]*dynamodb.AttributeValue {
	itemMap := make(map[string]*dynamodb.AttributeValue)
	dynamo.PopulateAttributesString(itemMap, "campaignId", scc.CampaignID)
	dynamo.PopulateAttributesString(itemMap, "prefix", scc.Prefix)
	dynamo.PopulateAttributesInt64(itemMap, "totalBits", scc.TotalBits)
	dynamo.PopulateAttributesInt64(itemMap, "usedBits", scc.UsedBits)
	dynamo.PopulateAttributesTime(itemMap, "startTime", scc.StartTime)
	dynamo.PopulateAttributesTime(itemMap, "endTime", scc.EndTime)
	dynamo.PopulateAttributesBool(itemMap, "enabled", scc.IsEnabled)
	dynamo.PopulateAttributesTime(itemMap, "lastUpdated", scc.LastUpdated)
	dynamo.PopulateAttributesInt64(itemMap, "minSponsoredAmount", scc.MinBitsToBeSponsored)
	dynamo.PopulateAttributesMapInt64ToFloat64(itemMap, "sponsoredAmountThresholds", scc.SponsoredAmountThresholds)
	dynamo.PopulateAttributesInt64(itemMap, "userLimit", scc.UserLimit)
	dynamo.PopulateAttributesString(itemMap, "brandName", scc.BrandName)
	dynamo.PopulateAttributesString(itemMap, "brandImageURL", scc.BrandImageURL)
	dynamo.PopulateAttributesInt64(itemMap, "pubsubRateLimit", scc.PubSubRateLimitSeconds)

	dynamo.PopulateAttributesMapStringToBool(itemMap, "channelBlacklist", scc.ChannelBlacklist)
	dynamo.PopulateAttributesString(itemMap, "eligibilityType", scc.EligibilityType)

	return itemMap
}

func (scc *SponsoredCheermoteCampaign) NewItemKey() map[string]*dynamodb.AttributeValue {
	keyMap := make(map[string]*dynamodb.AttributeValue)
	dynamo.PopulateAttributesString(keyMap, "campaignId", scc.CampaignID)

	return keyMap
}

func (scc *SponsoredCheermoteCampaign) NewHashKeyEqualsExpression() string {
	return "campaignId = :campaignId"
}

func (scc *SponsoredCheermoteCampaign) NewHashKeyExpressionAttributeValues() map[string]*dynamodb.AttributeValue {
	attributeMap := make(map[string]*dynamodb.AttributeValue)
	dynamo.PopulateAttributesString(attributeMap, "campaignId", scc.CampaignID)

	return attributeMap
}

func (scc *SponsoredCheermoteCampaign) UpdateWithCurrentTimestamp() {
	scc.LastUpdated = time.Now()
}

func (scc *SponsoredCheermoteCampaign) GetTimestamp() time.Time {
	return scc.LastUpdated
}

func (scc *SponsoredCheermoteCampaign) ApplyTimestamp(timestamp time.Time) {
	scc.LastUpdated = timestamp
}

func (scc *SponsoredCheermoteCampaign) Equals(other dynamo.DynamoTableRecord) bool {
	otherSponsoredCheermoteCampaign, isSponsoredCheermoteCampaign := other.(*SponsoredCheermoteCampaign)
	if !isSponsoredCheermoteCampaign {
		return false
	}

	if !scc.SponsoredAmountThresholdsEqual(otherSponsoredCheermoteCampaign) {
		return false
	}

	if !scc.ChannelBlacklistEqual(otherSponsoredCheermoteCampaign) {
		return false
	}

	return scc.CampaignID == otherSponsoredCheermoteCampaign.CampaignID &&
		scc.Prefix == otherSponsoredCheermoteCampaign.Prefix &&
		scc.TotalBits == otherSponsoredCheermoteCampaign.TotalBits &&
		scc.UsedBits == otherSponsoredCheermoteCampaign.UsedBits &&
		scc.StartTime == otherSponsoredCheermoteCampaign.StartTime &&
		scc.EndTime == otherSponsoredCheermoteCampaign.EndTime &&
		scc.IsEnabled == otherSponsoredCheermoteCampaign.IsEnabled &&
		scc.MinBitsToBeSponsored == otherSponsoredCheermoteCampaign.MinBitsToBeSponsored &&
		scc.UserLimit == otherSponsoredCheermoteCampaign.UserLimit &&
		scc.BrandName == otherSponsoredCheermoteCampaign.BrandName &&
		scc.BrandImageURL == otherSponsoredCheermoteCampaign.BrandImageURL &&
		scc.PubSubRateLimitSeconds == otherSponsoredCheermoteCampaign.PubSubRateLimitSeconds &&
		scc.EligibilityType == otherSponsoredCheermoteCampaign.EligibilityType
}

func (scc *SponsoredCheermoteCampaign) SponsoredAmountThresholdsEqual(other *SponsoredCheermoteCampaign) bool {
	if len(scc.SponsoredAmountThresholds) != len(other.SponsoredAmountThresholds) {
		return false
	}

	for key, value := range scc.SponsoredAmountThresholds {
		_, exists := other.SponsoredAmountThresholds[key]
		if !exists {
			return false
		}
		if other.SponsoredAmountThresholds[key] != value {
			return false
		}
	}

	return true
}

func (scc *SponsoredCheermoteCampaign) ChannelBlacklistEqual(other *SponsoredCheermoteCampaign) bool {
	if len(scc.ChannelBlacklist) != len(other.ChannelBlacklist) {
		return false
	}

	for key, value := range scc.ChannelBlacklist {
		_, exists := other.ChannelBlacklist[key]
		if !exists {
			return false
		}
		if other.ChannelBlacklist[key] != value {
			return false
		}
	}

	return true
}

func (scc *SponsoredCheermoteCampaign) EqualsWithExpectedLastUpdated(other dynamo.DynamoTableRecord, lastUpdatedTime time.Time, lastUpdatedDelta time.Duration) bool {
	return scc.Equals(other) && timeUtils.IsTimeInForwardDelta(other.GetTimestamp(), lastUpdatedTime, lastUpdatedDelta)
}

func (scc *SponsoredCheermoteCampaign) IsActive() bool {
	return scc.IsEnabled && timeUtils.IsTimeInRange(time.Now(), scc.StartTime, scc.EndTime)
}
