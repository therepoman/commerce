package campaigns

import (
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/errors"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

type SponsoredCheermoteCampaignDao struct {
	dynamo.BaseDao
}

type ISponsoredCheermoteCampaignDao interface {
	Put(campaign *SponsoredCheermoteCampaign) error
	Get(campaignID string) (*SponsoredCheermoteCampaign, error)
	GetAll() ([]*SponsoredCheermoteCampaign, error)
	GetBatch(campaignIDs []string) ([]*SponsoredCheermoteCampaign, error)
	Update(sponsoredCheermoteCampaign *SponsoredCheermoteCampaign) error
	Delete(campaignID string) error
	DeleteTable() error
	SetupTable() error
	GetClientConfig() *dynamo.DynamoClientConfig
	GetLatestStreamArn() (string, error)
	IncrementBitsUsed(sponsoredCheermoteCampaign *SponsoredCheermoteCampaign, amountToInc int64) error
}

func NewSponsoredCheermoteCampaignDao(client *dynamo.DynamoClient) ISponsoredCheermoteCampaignDao {
	return &SponsoredCheermoteCampaignDao{
		BaseDao: dynamo.NewBaseDao(&SponsoredCheermoteCampaignsTable{}, client),
	}
}

func (dao *SponsoredCheermoteCampaignDao) GetBatch(campaignIDs []string) ([]*SponsoredCheermoteCampaign, error) {
	campaignsResult := make([]*SponsoredCheermoteCampaign, 0)

	campaignQuery := make([]dynamo.DynamoTableRecord, 0)
	for _, campaignID := range campaignIDs {
		campaignQuery = append(campaignQuery, &SponsoredCheermoteCampaign{
			CampaignID: campaignID,
		})
	}

	dynamoRecords, err := dao.GetClient().BatchGetItem(campaignQuery)
	if err != nil {
		return campaignsResult, err
	}

	for _, dynamoRecord := range dynamoRecords {
		campaignResult := dynamoRecord.(*SponsoredCheermoteCampaign)
		campaignsResult = append(campaignsResult, campaignResult)
	}

	return campaignsResult, nil
}

func (dao *SponsoredCheermoteCampaignDao) Get(campaignID string) (*SponsoredCheermoteCampaign, error) {
	var campaignResult *SponsoredCheermoteCampaign

	campaignQuery := &SponsoredCheermoteCampaign{
		CampaignID: campaignID,
	}

	result, err := dao.GetClient().GetItem(campaignQuery)
	if err != nil {
		return campaignResult, err
	}

	// Result will be nil if no item is found in dynamo
	if result == nil {
		return nil, nil
	}

	campaignResult, isCampaign := result.(*SponsoredCheermoteCampaign)
	if !isCampaign {
		msg := "Encountered an unexpected type while converting dynamo result to sponsored cheermote campaign"
		log.Error(msg)
		return campaignResult, errors.New(msg)
	}

	return campaignResult, nil
}

func (dao *SponsoredCheermoteCampaignDao) GetAll() ([]*SponsoredCheermoteCampaign, error) {
	filter := dynamo.ScanFilter{
		Names: map[string]*string{
			"#CAMPAIGNID": aws.String("CampaignID"),
		},
		Values: map[string]*dynamodb.AttributeValue{
			":campaignid": {S: aws.String(string("0"))},
		},

		Expression: aws.String("#CAMPAIGNID <> :campaignId"),
	}

	result, err := dao.GetClient().ScanTable(&SponsoredCheermoteCampaignsTable{}, filter)
	if err != nil {
		return nil, err
	}

	// Need to type assert the slice of records
	campaignResult := make([]*SponsoredCheermoteCampaign, len(result))
	for i, record := range result {
		var isCampaign bool
		campaignResult[i], isCampaign = record.(*SponsoredCheermoteCampaign)
		if !isCampaign {
			msg := "Encountered an unexpected type while converting dynamo result to sponsored cheermote campaign record"
			log.Error(msg)
			return campaignResult, errors.New(msg)
		}
	}

	return campaignResult, nil
}

func (dao *SponsoredCheermoteCampaignDao) Put(campaign *SponsoredCheermoteCampaign) error {
	return dao.GetClient().PutItem(campaign)
}

func (dao *SponsoredCheermoteCampaignDao) Update(sponsoredCheermoteCampaign *SponsoredCheermoteCampaign) error {
	return dao.GetClient().UpdateItem(sponsoredCheermoteCampaign)
}

func (dao *SponsoredCheermoteCampaignDao) IncrementBitsUsed(sponsoredCheermoteCampaign *SponsoredCheermoteCampaign, amountToInc int64) error {
	keyMap := make(map[string]*dynamodb.AttributeValue)
	dynamo.PopulateAttributesString(keyMap, "campaignId", sponsoredCheermoteCampaign.CampaignID)

	expressionAttributeValues := make(map[string]*dynamodb.AttributeValue)
	dynamo.PopulateAttributesInt64(expressionAttributeValues, ":amountToInc", amountToInc)
	dynamo.PopulateAttributesInt64(expressionAttributeValues, ":threshold", sponsoredCheermoteCampaign.TotalBits-amountToInc)

	updateExpression := "ADD usedBits :amountToInc"

	conditionalExpression := "usedBits <= :threshold"

	fullTableName := dao.GetClient().GetFullTableName(sponsoredCheermoteCampaign.GetTable())
	updateItemInput := &dynamodb.UpdateItemInput{
		TableName:                 aws.String(string(fullTableName)),
		Key:                       keyMap,
		UpdateExpression:          &updateExpression,
		ConditionExpression:       &conditionalExpression,
		ExpressionAttributeValues: expressionAttributeValues,
	}

	// we need to use this version of UpdateItem because we need to just decrement the attribute.
	_, err := dao.GetClient().Dynamo.UpdateItem(updateItemInput)
	if err != nil {
		msg := "Sponsored Cheermote Campaigns DAO Inc: Encountered an error calling dynamo UpdateItem"
		log.WithError(err).Error(msg)
		return errors.Notef(err, msg)
	}
	return nil
}

func (dao *SponsoredCheermoteCampaignDao) Delete(campaignID string) error {
	deletionRecord := &SponsoredCheermoteCampaign{
		CampaignID: campaignID,
	}
	return dao.GetClient().DeleteItem(deletionRecord)
}
