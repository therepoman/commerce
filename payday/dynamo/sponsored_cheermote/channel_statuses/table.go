package channel_statuses

import (
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/errors"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

type SponsoredCheermoteChannelStatusesTable struct {
}

func (sccst *SponsoredCheermoteChannelStatusesTable) GetBaseTableName() dynamo.BaseTableName {
	return "sponsored_cheermote_channel_statuses"
}

func (sccst *SponsoredCheermoteChannelStatusesTable) GetNodeJsRecordKeySelector() dynamo.NodeJsRecordKeySelector {
	return "record.dynamodb.Keys.channelId.S + '_' + record.dynamodb.Keys.campaignId.S"
}

func (sccst *SponsoredCheermoteChannelStatusesTable) NewCreateTableInput(name dynamo.FullTableName, read dynamo.ReadCapacity, write dynamo.WriteCapacity) *dynamodb.CreateTableInput {
	return &dynamodb.CreateTableInput{
		TableName: aws.String(string(name)),
		ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
			ReadCapacityUnits:  aws.Int64(int64(read)),
			WriteCapacityUnits: aws.Int64(int64(write)),
		},
		AttributeDefinitions: []*dynamodb.AttributeDefinition{
			{
				AttributeName: aws.String("channelId"),
				AttributeType: aws.String("S"),
			},
			{
				AttributeName: aws.String("campaignId"),
				AttributeType: aws.String("S"),
			},
		},
		KeySchema: []*dynamodb.KeySchemaElement{
			{
				AttributeName: aws.String("channelId"),
				KeyType:       aws.String("HASH"),
			},
			{
				AttributeName: aws.String("campaignId"),
				KeyType:       aws.String("RANGE"),
			},
		},
		StreamSpecification: &dynamodb.StreamSpecification{
			StreamEnabled:  aws.Bool(true),
			StreamViewType: aws.String(dynamodb.StreamViewTypeNewAndOldImages),
		},
	}
}

func (sccst *SponsoredCheermoteChannelStatusesTable) ConvertAttributeMapToRecord(attributeMap map[string]*dynamodb.AttributeValue) (dynamo.DynamoTableRecord, error) {
	lastUpdated, err := dynamo.TimeFromAttributes(attributeMap, "lastUpdated")
	if err != nil {
		return nil, err
	}

	return &SponsoredCheermoteChannelStatus{
		ChannelID:   dynamo.StringFromAttributes(attributeMap, "channelId"),
		CampaignID:  dynamo.StringFromAttributes(attributeMap, "campaignId"),
		OptedIn:     dynamo.BoolFromAttributes(attributeMap, "optedIn"),
		LastUpdated: lastUpdated,
	}, nil
}

func (sccst *SponsoredCheermoteChannelStatusesTable) ConvertScanOutputToRecords(so *dynamodb.ScanOutput) ([]dynamo.DynamoScanQueryTableRecord, error) {
	records := make([]dynamo.DynamoScanQueryTableRecord, *so.Count)

	for i, attributes := range so.Items {
		recordToAdd, err := sccst.ConvertAttributeMapToRecord(attributes)
		if err != nil {
			return nil, err
		}

		scanQueryRecord, err := recordToScanQueryRecord(recordToAdd)
		if err != nil {
			return nil, err
		}

		records[i] = scanQueryRecord
	}
	return records, nil
}

func (sccst *SponsoredCheermoteChannelStatusesTable) ConvertQueryOutputToRecords(qo *dynamodb.QueryOutput) ([]dynamo.DynamoScanQueryTableRecord, error) {
	records := make([]dynamo.DynamoScanQueryTableRecord, *qo.Count)

	for i, attributes := range qo.Items {
		recordToAdd, err := sccst.ConvertAttributeMapToRecord(attributes)
		if err != nil {
			return nil, err
		}

		scanQueryRecord, err := recordToScanQueryRecord(recordToAdd)
		if err != nil {
			return nil, err
		}

		records[i] = scanQueryRecord
	}
	return records, nil
}

func recordToScanQueryRecord(record dynamo.DynamoTableRecord) (dynamo.DynamoScanQueryTableRecord, error) {
	scanQueryRecord, isScanQueryRecord := record.(dynamo.DynamoScanQueryTableRecord)
	if !isScanQueryRecord {
		return nil, errors.New("record not a scanQueryRecord")
	}
	return scanQueryRecord, nil
}
