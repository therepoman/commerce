package channel_statuses

import (
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/campaigns"
	"code.justin.tv/commerce/payday/errors"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

type SponsoredCheermoteChannelStatusesDao struct {
	dynamo.BaseDao
	dynamo.DynamoTable
	dynamo.DynamoScanQueryTable
}

type ISponsoredCheermoteChannelStatusesDao interface {
	Get(channelID string, campaignID string) (*SponsoredCheermoteChannelStatus, error)
	GetBatch(channelID string, c []*campaigns.SponsoredCheermoteCampaign) ([]*SponsoredCheermoteChannelStatus, error)
	QueryWithLimit(channelID string, limit int64, pagingKey *string) ([]*SponsoredCheermoteChannelStatus, *string, error)
	Update(sponsoredCheermoteChannelStatus *SponsoredCheermoteChannelStatus) error
	Delete(channelID string, campaignID string) error
	BatchDelete([]*SponsoredCheermoteChannelStatus) error
	DeleteTable() error
	SetupTable() error
	GetClientConfig() *dynamo.DynamoClientConfig
	GetLatestStreamArn() (string, error)
}

func NewSponsoredCheermoteChannelStatusesDao(client *dynamo.DynamoClient) ISponsoredCheermoteChannelStatusesDao {
	table := &SponsoredCheermoteChannelStatusesTable{}
	item := &SponsoredCheermoteChannelStatus{}
	queryScanTable := item.GetScanQueryTable()

	return &SponsoredCheermoteChannelStatusesDao{
		BaseDao:              dynamo.NewBaseDao(table, client),
		DynamoTable:          table,
		DynamoScanQueryTable: queryScanTable,
	}
}

func (dao *SponsoredCheermoteChannelStatusesDao) GetBatch(channelID string, c []*campaigns.SponsoredCheermoteCampaign) ([]*SponsoredCheermoteChannelStatus, error) {
	channelStatusesResult := make([]*SponsoredCheermoteChannelStatus, 0)

	channelStatusesQuery := make([]dynamo.DynamoTableRecord, 0)
	for _, campaign := range c {
		if campaign != nil {
			channelStatusesQuery = append(channelStatusesQuery, &SponsoredCheermoteChannelStatus{
				CampaignID: campaign.CampaignID,
				ChannelID:  channelID,
			})
		}
	}

	dynamoRecords, err := dao.GetClient().BatchGetItem(channelStatusesQuery)
	if err != nil {
		return channelStatusesResult, err
	}

	for _, dynamoRecord := range dynamoRecords {
		channelStatusResult := dynamoRecord.(*SponsoredCheermoteChannelStatus)
		channelStatusesResult = append(channelStatusesResult, channelStatusResult)
	}

	return channelStatusesResult, nil
}

func (dao *SponsoredCheermoteChannelStatusesDao) Get(channelID string, campaignID string) (*SponsoredCheermoteChannelStatus, error) {
	var channelStatusResult *SponsoredCheermoteChannelStatus

	channelStatusQuery := &SponsoredCheermoteChannelStatus{
		CampaignID: campaignID,
		ChannelID:  channelID,
	}

	result, err := dao.GetClient().GetItem(channelStatusQuery)
	if err != nil {
		return channelStatusResult, err
	}

	// Result will be nil if no item is found in dynamo
	if result == nil {
		return nil, nil
	}

	channelStatusResult, isChannelStatus := result.(*SponsoredCheermoteChannelStatus)
	if !isChannelStatus {
		msg := "Encountered an unexpected type while converting dynamo result to sponsored cheermote channel status"
		log.Error(msg)
		return channelStatusResult, errors.New(msg)
	}

	return channelStatusResult, nil
}

func (this *SponsoredCheermoteChannelStatusesDao) QueryWithLimit(channelID string, limit int64, pagingKey *string) ([]*SponsoredCheermoteChannelStatus, *string, error) {
	var channelStatusResults []*SponsoredCheermoteChannelStatus
	var exclusiveStartKey map[string]*dynamodb.AttributeValue
	if pagingKey != nil {
		exclusiveStartKey = map[string]*dynamodb.AttributeValue{
			"campaignId": {S: pagingKey},
			"channelId":  {S: aws.String(channelID)},
		}
	}

	query := dynamo.QueryFilter{
		Names: map[string]*string{
			"#ID": aws.String("channelId"),
		},
		Values: map[string]*dynamodb.AttributeValue{
			":id": {S: aws.String(channelID)},
		},
		KeyConditionExpression: aws.String("#ID = :id"),
		Limit:                  aws.Int64(limit),
		ExclusiveStartKey:      exclusiveStartKey,
	}
	results, lastEvaluatedKey, err := this.GetClient().QueryTable(this.DynamoScanQueryTable, query)
	if err != nil {
		return channelStatusResults, nil, err
	}
	var newPagingKey *string
	if lastEvaluatedKey != nil {
		newPagingKey = lastEvaluatedKey["campaignId"].S
	}

	for _, channelStatus := range results {
		channelStatusResult, isChannelStatus := channelStatus.(*SponsoredCheermoteChannelStatus)
		if !isChannelStatus {
			msg := "Encountered an unexpected type while converting dynamo result to sponsored cheermote channel status"
			log.Error(msg)
			return channelStatusResults, newPagingKey, errors.New(msg)
		}
		channelStatusResults = append(channelStatusResults, channelStatusResult)
	}

	return channelStatusResults, newPagingKey, nil
}

func (dao *SponsoredCheermoteChannelStatusesDao) Update(sponsoredCheermoteChannelStatus *SponsoredCheermoteChannelStatus) error {
	return dao.GetClient().UpdateItem(sponsoredCheermoteChannelStatus)
}

func (dao *SponsoredCheermoteChannelStatusesDao) Delete(channelID string, campaignID string) error {
	deletionRecord := &SponsoredCheermoteChannelStatus{
		CampaignID: campaignID,
		ChannelID:  channelID,
	}
	return dao.GetClient().DeleteItem(deletionRecord)
}

func (this *SponsoredCheermoteChannelStatusesDao) BatchDelete(sponsoredCheermoteChannelStatuses []*SponsoredCheermoteChannelStatus) error {
	deleteQuery := make([]dynamo.DynamoTableRecord, 0)
	for _, status := range sponsoredCheermoteChannelStatuses {
		deleteQuery = append(deleteQuery, status)
	}
	return this.GetClient().BatchDeleteItem(deleteQuery)
}
