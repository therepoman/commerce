package channel_statuses

import (
	"time"

	"code.justin.tv/commerce/payday/dynamo"
	timeUtils "code.justin.tv/commerce/payday/utils/time"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

type SponsoredCheermoteChannelStatus struct {
	CampaignID  string
	ChannelID   string
	OptedIn     bool
	LastUpdated time.Time
}

func (scss *SponsoredCheermoteChannelStatus) GetTable() dynamo.DynamoTable {
	return &SponsoredCheermoteChannelStatusesTable{}
}

func (scss *SponsoredCheermoteChannelStatus) GetScanQueryTable() dynamo.DynamoScanQueryTable {
	return &SponsoredCheermoteChannelStatusesTable{}
}

func (scss *SponsoredCheermoteChannelStatus) NewAttributeMap() map[string]*dynamodb.AttributeValue {
	itemMap := make(map[string]*dynamodb.AttributeValue)
	dynamo.PopulateAttributesString(itemMap, "channelId", scss.ChannelID)
	dynamo.PopulateAttributesString(itemMap, "campaignId", scss.CampaignID)
	dynamo.PopulateAttributesBool(itemMap, "optedIn", scss.OptedIn)
	dynamo.PopulateAttributesTime(itemMap, "lastUpdated", scss.LastUpdated)

	return itemMap
}

func (scss *SponsoredCheermoteChannelStatus) NewItemKey() map[string]*dynamodb.AttributeValue {
	keyMap := make(map[string]*dynamodb.AttributeValue)
	dynamo.PopulateAttributesString(keyMap, "channelId", scss.ChannelID)
	dynamo.PopulateAttributesString(keyMap, "campaignId", scss.CampaignID)

	return keyMap
}

func (scss *SponsoredCheermoteChannelStatus) NewHashKeyEqualsExpression() string {
	return "channelId = :channelId"
}

func (scss *SponsoredCheermoteChannelStatus) NewHashKeyExpressionAttributeValues() map[string]*dynamodb.AttributeValue {
	attributeMap := make(map[string]*dynamodb.AttributeValue)
	dynamo.PopulateAttributesString(attributeMap, "channelId", scss.ChannelID)

	return attributeMap
}

func (scss *SponsoredCheermoteChannelStatus) UpdateWithCurrentTimestamp() {
	scss.LastUpdated = time.Now()
}

func (scss *SponsoredCheermoteChannelStatus) GetTimestamp() time.Time {
	return scss.LastUpdated
}

func (scss *SponsoredCheermoteChannelStatus) ApplyTimestamp(timestamp time.Time) {
	scss.LastUpdated = timestamp
}

func (scss *SponsoredCheermoteChannelStatus) Equals(other dynamo.DynamoTableRecord) bool {
	otherSponsoredCheermoteChannelStatus, isSponsoredCheermoteChannelStatus := other.(*SponsoredCheermoteChannelStatus)
	if !isSponsoredCheermoteChannelStatus {
		return false
	}

	return scss.CampaignID == otherSponsoredCheermoteChannelStatus.CampaignID &&
		scss.ChannelID == otherSponsoredCheermoteChannelStatus.ChannelID &&
		scss.OptedIn == otherSponsoredCheermoteChannelStatus.OptedIn
}

func (scss *SponsoredCheermoteChannelStatus) EqualsWithExpectedLastUpdated(other dynamo.DynamoTableRecord, lastUpdatedTime time.Time, lastUpdatedDelta time.Duration) bool {
	return scss.Equals(other) && timeUtils.IsTimeInForwardDelta(other.GetTimestamp(), lastUpdatedTime, lastUpdatedDelta)
}
