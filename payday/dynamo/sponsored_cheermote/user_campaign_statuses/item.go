package user_campaign_statuses

import (
	"time"

	"code.justin.tv/commerce/payday/dynamo"
	timeUtils "code.justin.tv/commerce/payday/utils/time"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

type SponsoredCheermoteUserStatus struct {
	CampaignID  string
	UserID      string
	LastUpdated time.Time
	UsedBits    int64
}

func (scss *SponsoredCheermoteUserStatus) GetTable() dynamo.DynamoTable {
	return &SponsoredCheermoteUserStatusesTable{}
}

func (scss *SponsoredCheermoteUserStatus) GetScanQueryTable() dynamo.DynamoScanQueryTable {
	return &SponsoredCheermoteUserStatusesTable{}
}

func (this *SponsoredCheermoteUserStatus) NewAttributeMap() map[string]*dynamodb.AttributeValue {
	itemMap := make(map[string]*dynamodb.AttributeValue)
	dynamo.PopulateAttributesString(itemMap, "userId", this.UserID)
	dynamo.PopulateAttributesString(itemMap, "campaignId", this.CampaignID)
	dynamo.PopulateAttributesInt64(itemMap, "usedBits", this.UsedBits)
	dynamo.PopulateAttributesTime(itemMap, "lastUpdated", this.LastUpdated)
	return itemMap
}

func (this *SponsoredCheermoteUserStatus) NewItemKey() map[string]*dynamodb.AttributeValue {
	keyMap := make(map[string]*dynamodb.AttributeValue)
	dynamo.PopulateAttributesString(keyMap, "userId", this.UserID)
	dynamo.PopulateAttributesString(keyMap, "campaignId", this.CampaignID)

	return keyMap
}

func (this *SponsoredCheermoteUserStatus) NewHashKeyEqualsExpression() string {
	return "userId = :userId"
}

func (this *SponsoredCheermoteUserStatus) NewHashKeyExpressionAttributeValues() map[string]*dynamodb.AttributeValue {
	attributeMap := make(map[string]*dynamodb.AttributeValue)
	dynamo.PopulateAttributesString(attributeMap, "userId", this.UserID)

	return attributeMap
}

func (this *SponsoredCheermoteUserStatus) UpdateWithCurrentTimestamp() {
	this.LastUpdated = time.Now()
}

func (this *SponsoredCheermoteUserStatus) GetTimestamp() time.Time {
	return this.LastUpdated
}

func (this *SponsoredCheermoteUserStatus) ApplyTimestamp(timestamp time.Time) {
	this.LastUpdated = timestamp
}

func (this *SponsoredCheermoteUserStatus) Equals(other dynamo.DynamoTableRecord) bool {
	otherSponsoredCheermoteUserStatus, isSponsoredCheermoteUserStatus := other.(*SponsoredCheermoteUserStatus)
	if !isSponsoredCheermoteUserStatus {
		return false
	}

	return this.CampaignID == otherSponsoredCheermoteUserStatus.CampaignID &&
		this.UserID == otherSponsoredCheermoteUserStatus.UserID &&
		this.UsedBits == otherSponsoredCheermoteUserStatus.UsedBits
}

func (scss *SponsoredCheermoteUserStatus) EqualsWithExpectedLastUpdated(other dynamo.DynamoTableRecord, lastUpdatedTime time.Time, lastUpdatedDelta time.Duration) bool {
	return scss.Equals(other) && timeUtils.IsTimeInForwardDelta(other.GetTimestamp(), lastUpdatedTime, lastUpdatedDelta)
}
