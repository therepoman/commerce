package user_campaign_statuses

import (
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/errors"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

type SponsoredCheermoteUserStatusesDao struct {
	dynamo.BaseDao
	dynamo.DynamoTable
	dynamo.DynamoScanQueryTable
}

type ISponsoredCheermoteUserStatusesDao interface {
	Get(userID string, campaignID string) (*SponsoredCheermoteUserStatus, error)
	QueryWithLimit(channelID string, limit int64, pagingKey *string) ([]*SponsoredCheermoteUserStatus, *string, error)
	Update(sponsoredCheermoteUserStatus *SponsoredCheermoteUserStatus) error
	Delete(userID string, campaignID string) error
	DeleteTable() error
	BatchDelete([]*SponsoredCheermoteUserStatus) error
	SetupTable() error
	IncrementBitsUsed(campaignID, userID string, amountToInc int64) error
	GetClientConfig() *dynamo.DynamoClientConfig
	GetLatestStreamArn() (string, error)
}

func NewSponsoredCheermoteUserStatusesDao(client *dynamo.DynamoClient) ISponsoredCheermoteUserStatusesDao {
	table := &SponsoredCheermoteUserStatusesTable{}
	item := &SponsoredCheermoteUserStatus{}
	queryScanTable := item.GetScanQueryTable()

	return &SponsoredCheermoteUserStatusesDao{
		BaseDao:              dynamo.NewBaseDao(table, client),
		DynamoTable:          table,
		DynamoScanQueryTable: queryScanTable,
	}
}

func (this *SponsoredCheermoteUserStatusesDao) Get(userID string, campaignID string) (*SponsoredCheermoteUserStatus, error) {
	var userStatusResult *SponsoredCheermoteUserStatus

	userStatusQuery := &SponsoredCheermoteUserStatus{
		CampaignID: campaignID,
		UserID:     userID,
	}

	result, err := this.GetClient().GetItem(userStatusQuery)
	if err != nil {
		return userStatusResult, err
	}

	// Result will be nil if no item is found in dynamo
	if result == nil {
		return nil, nil
	}

	userStatusResult, isUserStatus := result.(*SponsoredCheermoteUserStatus)
	if !isUserStatus {
		msg := "Encountered an unexpected type while converting dynamo result to sponsored cheermote user status"
		log.Error(msg)
		return userStatusResult, errors.New(msg)
	}

	return userStatusResult, nil
}

func (this *SponsoredCheermoteUserStatusesDao) QueryWithLimit(userID string, limit int64, pagingKey *string) ([]*SponsoredCheermoteUserStatus, *string, error) {
	var userStatusResults []*SponsoredCheermoteUserStatus
	var exclusiveStartKey map[string]*dynamodb.AttributeValue
	if pagingKey != nil {
		exclusiveStartKey = map[string]*dynamodb.AttributeValue{
			"campaignId": {S: pagingKey},
			"userId":     {S: aws.String(userID)},
		}
	}

	query := dynamo.QueryFilter{
		Names: map[string]*string{
			"#ID": aws.String("userId"),
		},
		Values: map[string]*dynamodb.AttributeValue{
			":id": {S: aws.String(userID)},
		},
		KeyConditionExpression: aws.String("#ID = :id"),
		Limit:                  aws.Int64(limit),
		ExclusiveStartKey:      exclusiveStartKey,
	}
	results, lastEvaluatedKey, err := this.GetClient().QueryTable(this.DynamoScanQueryTable, query)
	if err != nil {
		return userStatusResults, nil, err
	}
	var newPagingKey *string
	if lastEvaluatedKey != nil {
		newPagingKey = lastEvaluatedKey["campaignId"].S
	}

	for _, userStatus := range results {
		userStatusResult, isUserStatus := userStatus.(*SponsoredCheermoteUserStatus)
		if !isUserStatus {
			msg := "Encountered an unexpected type while converting dynamo result to sponsored cheermote user status"
			log.Error(msg)
			return userStatusResults, newPagingKey, errors.New(msg)
		}
		userStatusResults = append(userStatusResults, userStatusResult)
	}

	return userStatusResults, newPagingKey, nil
}

func (this *SponsoredCheermoteUserStatusesDao) Update(sponsoredCheermoteUserStatus *SponsoredCheermoteUserStatus) error {
	return this.GetClient().UpdateItem(sponsoredCheermoteUserStatus)
}

func (this *SponsoredCheermoteUserStatusesDao) Delete(userID string, campaignID string) error {
	deletionRecord := &SponsoredCheermoteUserStatus{
		CampaignID: campaignID,
		UserID:     userID,
	}
	return this.GetClient().DeleteItem(deletionRecord)
}

func (this *SponsoredCheermoteUserStatusesDao) IncrementBitsUsed(campaignID string, userID string, amountToInc int64) error {

	keyMap := make(map[string]*dynamodb.AttributeValue)
	dynamo.PopulateAttributesString(keyMap, "campaignId", campaignID)
	dynamo.PopulateAttributesString(keyMap, "userId", userID)

	expressionAttributeValues := make(map[string]*dynamodb.AttributeValue)
	dynamo.PopulateAttributesInt64(expressionAttributeValues, ":amountToInc", amountToInc)
	updateExpression := "ADD usedBits :amountToInc"

	fullTableName := this.GetClient().GetFullTableName(this.DynamoTable)
	updateItemInput := &dynamodb.UpdateItemInput{
		TableName:                 aws.String(string(fullTableName)),
		Key:                       keyMap,
		UpdateExpression:          &updateExpression,
		ExpressionAttributeValues: expressionAttributeValues,
	}

	// we need to use this version of UpdateItem because we need to just decrement the attribute.
	_, err := this.GetClient().Dynamo.UpdateItem(updateItemInput)
	if err != nil {
		msg := "Sponsored Cheermote User DAO Inc: Encountered an error calling dynamo UpdateItem"
		log.WithError(err).Error(msg)
		return errors.Notef(err, msg)
	}
	return nil
}

func (this *SponsoredCheermoteUserStatusesDao) BatchDelete(sponsoredCheermoteUserStatuses []*SponsoredCheermoteUserStatus) error {
	deleteQuery := make([]dynamo.DynamoTableRecord, 0)
	for _, status := range sponsoredCheermoteUserStatuses {
		deleteQuery = append(deleteQuery, status)
	}
	return this.GetClient().BatchDeleteItem(deleteQuery)
}
