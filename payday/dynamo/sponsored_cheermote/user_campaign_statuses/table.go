package user_campaign_statuses

import (
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/errors"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

type SponsoredCheermoteUserStatusesTable struct {
}

func (this *SponsoredCheermoteUserStatusesTable) GetBaseTableName() dynamo.BaseTableName {
	return "sponsored_cheermote_user_statuses"
}

func (this *SponsoredCheermoteUserStatusesTable) GetNodeJsRecordKeySelector() dynamo.NodeJsRecordKeySelector {
	return "record.dynamodb.Keys.userId.S + '_' + record.dynamodb.Keys.campaignId.S"
}

func (this *SponsoredCheermoteUserStatusesTable) NewCreateTableInput(name dynamo.FullTableName, read dynamo.ReadCapacity, write dynamo.WriteCapacity) *dynamodb.CreateTableInput {
	return &dynamodb.CreateTableInput{
		TableName: aws.String(string(name)),
		ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
			ReadCapacityUnits:  aws.Int64(int64(read)),
			WriteCapacityUnits: aws.Int64(int64(write)),
		},
		AttributeDefinitions: []*dynamodb.AttributeDefinition{
			{
				AttributeName: aws.String("userId"),
				AttributeType: aws.String("S"),
			},
			{
				AttributeName: aws.String("campaignId"),
				AttributeType: aws.String("S"),
			},
		},
		KeySchema: []*dynamodb.KeySchemaElement{
			{
				AttributeName: aws.String("userId"),
				KeyType:       aws.String("HASH"),
			},
			{
				AttributeName: aws.String("campaignId"),
				KeyType:       aws.String("RANGE"),
			},
		},
		StreamSpecification: &dynamodb.StreamSpecification{
			StreamEnabled:  aws.Bool(true),
			StreamViewType: aws.String(dynamodb.StreamViewTypeNewAndOldImages),
		},
	}
}

func (this *SponsoredCheermoteUserStatusesTable) ConvertAttributeMapToRecord(attributeMap map[string]*dynamodb.AttributeValue) (dynamo.DynamoTableRecord, error) {
	lastUpdated, err := dynamo.TimeFromAttributes(attributeMap, "lastUpdated")
	if err != nil {
		return nil, err
	}

	usedBits, err := dynamo.Int64FromAttributes(attributeMap, "usedBits")
	if err != nil {
		return nil, err
	}

	return &SponsoredCheermoteUserStatus{
		UserID:      dynamo.StringFromAttributes(attributeMap, "userId"),
		CampaignID:  dynamo.StringFromAttributes(attributeMap, "campaignId"),
		UsedBits:    usedBits,
		LastUpdated: lastUpdated,
	}, nil
}

func (this *SponsoredCheermoteUserStatusesTable) ConvertScanOutputToRecords(so *dynamodb.ScanOutput) ([]dynamo.DynamoScanQueryTableRecord, error) {
	records := make([]dynamo.DynamoScanQueryTableRecord, *so.Count)

	for i, attributes := range so.Items {
		recordToAdd, err := this.ConvertAttributeMapToRecord(attributes)
		if err != nil {
			return nil, err
		}

		scanQueryRecord, err := recordToScanQueryRecord(recordToAdd)
		if err != nil {
			return nil, err
		}

		records[i] = scanQueryRecord
	}
	return records, nil
}

func (this *SponsoredCheermoteUserStatusesTable) ConvertQueryOutputToRecords(qo *dynamodb.QueryOutput) ([]dynamo.DynamoScanQueryTableRecord, error) {
	records := make([]dynamo.DynamoScanQueryTableRecord, *qo.Count)

	for i, attributes := range qo.Items {
		recordToAdd, err := this.ConvertAttributeMapToRecord(attributes)
		if err != nil {
			return nil, err
		}

		scanQueryRecord, err := recordToScanQueryRecord(recordToAdd)
		if err != nil {
			return nil, err
		}

		records[i] = scanQueryRecord
	}
	return records, nil
}

func recordToScanQueryRecord(record dynamo.DynamoTableRecord) (dynamo.DynamoScanQueryTableRecord, error) {
	scanQueryRecord, isScanQueryRecord := record.(dynamo.DynamoScanQueryTableRecord)
	if !isScanQueryRecord {
		return nil, errors.New("record not a scanQueryRecord")
	}
	return scanQueryRecord, nil
}
