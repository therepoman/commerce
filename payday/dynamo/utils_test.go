package dynamo_test

import (
	"testing"

	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/dynamo/holds"
	"code.justin.tv/commerce/payday/utils/pointers"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	. "github.com/smartystreets/goconvey/convey"
)

func TestInt64FromAttributes(t *testing.T) {
	Convey("given an attribute map int64 field", t, func() {

		testName := "name"
		expected := int64(55)

		attributeMap := map[string]*dynamodb.AttributeValue{
			testName: {N: aws.String("55")},
		}

		result, err := dynamo.Int64FromAttributes(attributeMap, testName)

		So(err, ShouldBeNil)
		So(result, ShouldEqual, expected)
	})
}

func TestRecipientsFromAttributes(t *testing.T) {
	Convey("given an attribute map with recipient field", t, func() {
		attributeName := "recipients"
		attributeMap := map[string]*dynamodb.AttributeValue{
			attributeName: {
				L: []*dynamodb.AttributeValue{
					{
						M: map[string]*dynamodb.AttributeValue{
							holds.RecipientIDKey: {
								S: pointers.StringP("recipient1"),
							},
							holds.RecipientBitsRevenueAttributionKey: {
								N: pointers.StringP("20"),
							},
							holds.RecipientRewardAttributionKey: {
								N: pointers.StringP("21"),
							},
							holds.RecipientTotalWithBroadcasterKey: {
								N: pointers.StringP("22"),
							},
						},
					},
					{
						M: map[string]*dynamodb.AttributeValue{
							holds.RecipientIDKey: {
								S: pointers.StringP("recipient2"),
							},
							holds.RecipientBitsRevenueAttributionKey: {
								N: pointers.StringP("50"),
							},
							holds.RecipientRewardAttributionKey: {
								N: pointers.StringP("51"),
							},
							holds.RecipientTotalWithBroadcasterKey: {
								N: pointers.StringP("52"),
							},
						},
					},
					{
						M: map[string]*dynamodb.AttributeValue{
							holds.RecipientIDKey: {
								S: pointers.StringP("recipient3"),
							},
							holds.RecipientBitsRevenueAttributionKey: {
								N: pointers.StringP("100"),
							},
							holds.RecipientRewardAttributionKey: {
								N: pointers.StringP("101"),
							},
							holds.RecipientTotalWithBroadcasterKey: {
								N: pointers.StringP("102"),
							},
						},
					},
				},
			},
		}

		expected := []*holds.BitsUsageAttribution{
			{
				ID:                                  "recipient1",
				BitsRevenueAttributionToBroadcaster: 20,
				RewardAttribution:                   21,
				TotalWithBroadcasterPrior:           22,
			},
			{
				ID:                                  "recipient2",
				BitsRevenueAttributionToBroadcaster: 50,
				RewardAttribution:                   51,
				TotalWithBroadcasterPrior:           52,
			},
			{
				ID:                                  "recipient3",
				BitsRevenueAttributionToBroadcaster: 100,
				RewardAttribution:                   101,
				TotalWithBroadcasterPrior:           102,
			},
		}

		Convey("convert into structures", func() {
			recipients := dynamo.RecipientsFromAttributes(attributeMap, attributeName)
			So(recipients, ShouldHaveLength, 3)
			So(recipients, ShouldResemble, expected)
		})
	})
}

func TestPopulateAttributeRecipients(t *testing.T) {
	Convey("give recipients", t, func() {
		recipients := []*holds.BitsUsageAttribution{
			{
				ID:                                  "recipient1",
				BitsRevenueAttributionToBroadcaster: 20,
				RewardAttribution:                   21,
				TotalWithBroadcasterPrior:           22,
			},
			{
				ID:                                  "recipient2",
				BitsRevenueAttributionToBroadcaster: 50,
				RewardAttribution:                   51,
				TotalWithBroadcasterPrior:           52,
			},
			{
				ID:                                  "recipient3",
				BitsRevenueAttributionToBroadcaster: 100,
				RewardAttribution:                   101,
				TotalWithBroadcasterPrior:           102,
			},
		}

		attributeName := "recipients"
		expected := map[string]*dynamodb.AttributeValue{
			attributeName: {
				L: []*dynamodb.AttributeValue{
					{
						M: map[string]*dynamodb.AttributeValue{
							holds.RecipientIDKey: {
								S: pointers.StringP("recipient1"),
							},
							holds.RecipientBitsRevenueAttributionKey: {
								N: pointers.StringP("20"),
							},
							holds.RecipientRewardAttributionKey: {
								N: pointers.StringP("21"),
							},
							holds.RecipientTotalWithBroadcasterKey: {
								N: pointers.StringP("2"),
							},
						},
					},
					{
						M: map[string]*dynamodb.AttributeValue{
							holds.RecipientIDKey: {
								S: pointers.StringP("recipient"),
							},
							holds.RecipientBitsRevenueAttributionKey: {
								N: pointers.StringP("50"),
							},
							holds.RecipientRewardAttributionKey: {
								N: pointers.StringP("51"),
							},
							holds.RecipientTotalWithBroadcasterKey: {
								N: pointers.StringP("52"),
							},
						},
					},
					{
						M: map[string]*dynamodb.AttributeValue{
							holds.RecipientIDKey: {
								S: pointers.StringP("recipient"),
							},
							holds.RecipientBitsRevenueAttributionKey: {
								N: pointers.StringP("100"),
							},
							holds.RecipientRewardAttributionKey: {
								N: pointers.StringP("101"),
							},
							holds.RecipientTotalWithBroadcasterKey: {
								N: pointers.StringP("102"),
							},
						},
					},
				},
			},
		}

		Convey("populates the attribute map", func() {
			attributeMap := make(map[string]*dynamodb.AttributeValue)
			dynamo.PopulateAttributeRecipients(attributeMap, attributeName, recipients)
			So(attributeMap, ShouldResemble, expected)
		})
	})
}
