package dynamo_test

import (
	"testing"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/test"
	"github.com/stretchr/testify/assert"
)

const numChannelsToTestForBadgeTiers = 3

type tierKey struct {
	channelId string
	threshold int64
}

var tierDao dynamo.IBadgeTierDao

func makeKey(channelId string, threshold int64) tierKey {
	return tierKey{
		channelId: channelId,
		threshold: threshold,
	}
}

func validateExpectedDataInDB(t *testing.T, saveTime time.Time, savedTiers map[tierKey]*dynamo.BadgeTier, savedChannels map[string]map[int64]*dynamo.BadgeTier) {
	log.Infof("Retrieving and verifying tier records from dynamo by get")
	for tierKey, expectedTier := range savedTiers {
		actualTier, err := tierDao.Get(tierKey.channelId, tierKey.threshold)
		if err != nil {
			t.Fail()
		}
		assert.True(t, expectedTier.EqualsWithExpectedLastUpdated(actualTier, saveTime, dynamoContext.AcceptableUpdateTimeDelta))
	}

	log.Infof("Retrieving and verifying tier records from dynamo by query")
	for channel, expectedTiers := range savedChannels {
		actualTiers, err := tierDao.GetAll(channel, true)
		if err != nil {
			t.Fail()
		}

		assert.Equal(t, len(expectedTiers), len(actualTiers))

		for _, actualTier := range actualTiers {
			expectedTier, present := expectedTiers[actualTier.Threshold]
			if !present {
				t.Fail()
			}
			assert.True(t, expectedTier.EqualsWithExpectedLastUpdated(actualTier, saveTime, dynamoContext.AcceptableUpdateTimeDelta))
		}
	}
}

func TestBadgeTierDao(t *testing.T) {
	if dynamoContext.SkipDynamoTests {
		t.Skip("Skipping Dynamo Tests")
	}
	tierDao, _ = dynamoContext.CreateDefaultBadgeTierDao("test-dao-complete")
	err := tierDao.SetupTable()
	assert.NoError(t, err)

	now := time.Now()
	log.Infof("Saving %d random badge tier records to dynamo", numChannelsToTestForBadgeTiers)
	savedTiers := make(map[tierKey]*dynamo.BadgeTier)
	savedChannels := make(map[string]map[int64]*dynamo.BadgeTier)
	for i := 0; i < numChannelsToTestForBadgeTiers; i++ {
		channelId := test.RandomString(16)
		numTiersForChannel := test.RandomInt(0, 10)
		tiersForChannel := make(map[int64]*dynamo.BadgeTier)

		for j := 0; j < numTiersForChannel; j++ {
			threshold := int64(test.RandomInt(0, 10000000))
			testTier := &dynamo.BadgeTier{
				ChannelId: channelId,
				Threshold: threshold,
				Enabled:   test.RandomBool(),
			}

			err := tierDao.Put(testTier)
			if err != nil {
				t.Fail()
			}

			tiersForChannel[threshold] = testTier
			savedTiers[makeKey(channelId, threshold)] = testTier
		}
		savedChannels[channelId] = tiersForChannel
	}

	validateExpectedDataInDB(t, now, savedTiers, savedChannels)

	savedTiers = make(map[tierKey]*dynamo.BadgeTier)
	savedChannels = make(map[string]map[int64]*dynamo.BadgeTier)
	now = time.Now()
	log.Infof("Modifying existing tier records")
	for _, existingTier := range savedTiers {
		modifiedTier := &dynamo.BadgeTier{
			ChannelId: existingTier.ChannelId,
			Threshold: existingTier.Threshold,
			Enabled:   test.RandomBool(),
		}
		err := tierDao.Put(modifiedTier)
		if err != nil {
			t.Fail()
		}

		savedTiers[makeKey(existingTier.ChannelId, existingTier.Threshold)] = modifiedTier
		savedChannels[existingTier.ChannelId][existingTier.Threshold] = modifiedTier
	}

	log.Infof("Retrieving and verifying modified tier records from dynamo")
	validateExpectedDataInDB(t, now, savedTiers, savedChannels)

	log.Infof("Deleting tier records from dynamo")
	for _, savedTier := range savedTiers {
		err := tierDao.Delete(savedTier.ChannelId, savedTier.Threshold)
		assert.NoError(t, err)
	}

	log.Infof("Verifying that tier records no longer exist in dynamo")
	for _, deletedTier := range savedTiers {
		actualTier, err := tierDao.Get(deletedTier.ChannelId, deletedTier.Threshold)
		if err != nil {
			t.Fail()
		}
		assert.Nil(t, actualTier)
	}
}
