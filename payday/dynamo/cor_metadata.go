package dynamo

import (
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/errors"
	timeUtils "code.justin.tv/commerce/payday/utils/time"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

type CorMetadataTable struct {
}

type CorMetadata struct {
	COR                     string
	ObfuscatedMarketplaceId *string
	OverriddenMarketplaceId *string
	Version                 int64
	AllowStaff              bool
}

type CorMetadataDao struct {
	BaseDao
}

type ICorMetadataDao interface {
	Get(cor string) (*CorMetadata, error)
	GetClientConfig() *DynamoClientConfig
}

func (u *CorMetadataTable) GetBaseTableName() BaseTableName {
	return "cor-metadata"
}

func (u *CorMetadataTable) GetNodeJsRecordKeySelector() NodeJsRecordKeySelector {
	return "record.dynamodb.Keys.cor.S"
}

func (u *CorMetadataTable) NewCreateTableInput(name FullTableName, read ReadCapacity, write WriteCapacity) *dynamodb.CreateTableInput {
	return nil
}

func (u *CorMetadataTable) ConvertAttributeMapToRecord(attributeMap map[string]*dynamodb.AttributeValue) (DynamoTableRecord, error) {
	corAttributeVal := StringFromAttributes(attributeMap, "cor")
	obfuscatedMarketplaceIdAttributeVal := OptionalStringFromAttributes(attributeMap, "obfuscatedMarketplaceId")
	overriddenMarketplaceIdAttributeVal := OptionalStringFromAttributes(attributeMap, "overriddenMarketplaceId")
	allowStaffAttributeVal := BoolFromAttributes(attributeMap, "allowStaff")
	versionAttributeVal, err := Int64FromAttributes(attributeMap, "version")

	if err != nil {
		msg := "Error converting attribute map to Hashtag record"
		log.WithError(err).Error(msg)
		return nil, errors.Notef(err, msg)
	}

	return &CorMetadata{
		COR:                     corAttributeVal,
		ObfuscatedMarketplaceId: obfuscatedMarketplaceIdAttributeVal,
		OverriddenMarketplaceId: overriddenMarketplaceIdAttributeVal,
		Version:                 versionAttributeVal,
		AllowStaff:              allowStaffAttributeVal,
	}, nil
}

func (u *CorMetadata) GetTable() DynamoTable {
	return &CorMetadataTable{}
}

func (u *CorMetadata) NewAttributeMap() map[string]*dynamodb.AttributeValue {
	itemMap := make(map[string]*dynamodb.AttributeValue)
	PopulateAttributesString(itemMap, "cor", u.COR)
	PopulateAttributesOptionalString(itemMap, "obfuscatedMarketplaceId", u.ObfuscatedMarketplaceId)
	PopulateAttributesOptionalString(itemMap, "overriddenMarketplaceId", u.OverriddenMarketplaceId)
	PopulateAttributesBool(itemMap, "allowStaff", u.AllowStaff)
	PopulateAttributesInt64(itemMap, "version", u.Version)
	return itemMap
}

func (u *CorMetadata) NewItemKey() map[string]*dynamodb.AttributeValue {
	keyMap := make(map[string]*dynamodb.AttributeValue)
	PopulateAttributesString(keyMap, "cor", u.COR)
	return keyMap
}

func (u *CorMetadata) NewHashKeyEqualsExpression() string {
	return "cor=:cor"
}

func (u *CorMetadata) NewHashKeyExpressionAttributeValues() map[string]*dynamodb.AttributeValue {
	attributeValue := &dynamodb.AttributeValue{
		S: aws.String(u.COR),
	}
	attributeMap := make(map[string]*dynamodb.AttributeValue)
	attributeMap[":cor"] = attributeValue
	return attributeMap
}

func (u *CorMetadata) Equals(other DynamoTableRecord) bool {
	otherEntry, isCorMetadata := other.(*CorMetadata)
	if !isCorMetadata {
		return false
	}

	return u.COR == otherEntry.COR &&
		u.ObfuscatedMarketplaceId == otherEntry.ObfuscatedMarketplaceId &&
		u.OverriddenMarketplaceId == otherEntry.OverriddenMarketplaceId &&
		u.AllowStaff == otherEntry.AllowStaff &&
		u.Version == otherEntry.Version
}

func (u *CorMetadata) EqualsWithExpectedLastUpdated(other DynamoTableRecord, lastUpdatedTime time.Time, lastUpdatedDelta time.Duration) bool {
	return u.Equals(other) && timeUtils.IsTimeInForwardDelta(other.GetTimestamp(), lastUpdatedTime, lastUpdatedDelta)
}

func NewCorMetadataDao(client *DynamoClient) ICorMetadataDao {
	dao := &CorMetadataDao{}
	dao.client = client
	dao.table = &CorMetadataTable{}
	return dao
}

func (dao *CorMetadataDao) Get(cor string) (*CorMetadata, error) {
	var corMetadataResult *CorMetadata

	corMetadataQuery := &CorMetadata{
		COR: cor,
	}

	result, err := dao.client.GetItem(corMetadataQuery)
	if err != nil {
		return corMetadataResult, err
	}

	// Result will be nil if no item is found in dynamo
	if result == nil {
		return nil, nil
	}

	corMetadataResult, isCorMetadata := result.(*CorMetadata)
	if !isCorMetadata {
		msg := "Encountered an unexpected type while converting dynamo result to cor-metadata"
		log.Error(msg)
		return corMetadataResult, errors.New(msg)
	}

	return corMetadataResult, nil
}

func (dao *CorMetadataDao) Delete(corMetadata string) error {
	return errors.New("Don't delete stuff we don't own")
}

func (c *CorMetadata) ApplyTimestamp(timestamp time.Time) {
	//no timestamp field to update, but this method is needed to compile
}

func (c *CorMetadata) UpdateWithCurrentTimestamp() {
	//no timestamp field to update, but this method is needed to compile
}

func (c *CorMetadata) GetTimestamp() time.Time {
	//no timestamp field to update, but this method is needed to compile
	return time.Now()
}
