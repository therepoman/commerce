package dynamo

import (
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/hystrix"
	cmd "code.justin.tv/commerce/payday/hystrix"
	timeUtils "code.justin.tv/commerce/payday/utils/time"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

type TaxInfoTable struct {
}

type TaxInfo struct {
	PayoutEntityId            string
	AddressId                 string
	TaxInterviewStatus        string
	TaxWithholdingRateMapJson string
	LastUpdated               time.Time
}

type TaxInfoDao struct {
	BaseDao
}

type ITaxInfoDao interface {
	Get(payoutEntityId string) (*TaxInfo, error)
	Update(taxInfo *TaxInfo) error
	SetupTable() error
	DeleteTable() error
	Delete(payoutEntityId string) error
	GetClientConfig() *DynamoClientConfig
	GetLatestStreamArn() (string, error)
}

func (u *TaxInfoTable) GetBaseTableName() BaseTableName {
	return "tax-info-by-payout-entity-id"
}

func (u *TaxInfoTable) NewCreateTableInput(name FullTableName, read ReadCapacity, write WriteCapacity) *dynamodb.CreateTableInput {
	return &dynamodb.CreateTableInput{
		TableName: aws.String(string(name)),
		ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
			ReadCapacityUnits:  aws.Int64(int64(read)),
			WriteCapacityUnits: aws.Int64(int64(write)),
		},
		AttributeDefinitions: []*dynamodb.AttributeDefinition{
			{
				AttributeName: aws.String("payoutEntityId"),
				AttributeType: aws.String("S"),
			},
		},
		KeySchema: []*dynamodb.KeySchemaElement{
			{
				AttributeName: aws.String("payoutEntityId"),
				KeyType:       aws.String("HASH"),
			},
		},
		StreamSpecification: &dynamodb.StreamSpecification{
			StreamEnabled:  aws.Bool(true),
			StreamViewType: aws.String(dynamodb.StreamViewTypeNewAndOldImages),
		},
	}
}

func (u *TaxInfoTable) GetNodeJsRecordKeySelector() NodeJsRecordKeySelector {
	return "record.dynamodb.Keys.payoutEntityId.S"
}

func NewTaxInfoDao(client *DynamoClient) ITaxInfoDao {
	dao := &TaxInfoDao{BaseDao{
		client: client,
		table:  &TaxInfoTable{},
	}}
	return dao
}

func (dao *TaxInfoDao) Get(payoutEntityId string) (*TaxInfo, error) {
	taxInfoChan := make(chan *TaxInfo, 1)
	fn := func() error {
		channel, err := dao.get(payoutEntityId)
		if err != nil {
			return err
		}
		taxInfoChan <- channel
		return nil
	}

	err := hystrix.Do(cmd.GetTaxInfo, fn, nil)
	if err != nil {
		return nil, err
	}

	channel := <-taxInfoChan
	return channel, nil
}

func (dao *TaxInfoDao) get(payoutEntityId string) (*TaxInfo, error) {
	var taxInfoResult *TaxInfo

	taxQuery := &TaxInfo{
		PayoutEntityId: payoutEntityId,
	}

	result, err := dao.client.GetItem(taxQuery)
	if err != nil {
		return taxInfoResult, err
	}

	// Result will be nil if no item is found in dynamo
	if result == nil {
		return nil, nil
	}

	taxInfoResult, isChannel := result.(*TaxInfo)
	if !isChannel {
		msg := "Encountered an unexpected type while converting dynamo result to channel"
		log.Error(msg)
		return taxInfoResult, errors.New(msg)
	}
	return taxInfoResult, nil
}

func (dao *TaxInfoDao) Update(taxInfo *TaxInfo) error {
	return dao.client.UpdateItem(taxInfo)
}

func (dao *TaxInfoDao) Delete(payoutEntityId string) error {
	deletionRecord := &TaxInfo{
		PayoutEntityId: payoutEntityId,
	}
	return dao.client.DeleteItem(deletionRecord)
}

func (t *TaxInfo) UpdateWithCurrentTimestamp() {
	t.LastUpdated = time.Now()
}

func (t *TaxInfo) GetTimestamp() time.Time {
	return t.LastUpdated
}

func (t *TaxInfo) ApplyTimestamp(timestamp time.Time) {
	t.LastUpdated = timestamp
}

func (t *TaxInfo) Equals(other DynamoTableRecord) bool {
	if other == nil {
		return false
	}

	otherTaxInfo, isTaxInfo := other.(*TaxInfo)
	if !isTaxInfo {
		return false
	}

	return t.TaxWithholdingRateMapJson == otherTaxInfo.TaxWithholdingRateMapJson &&
		t.TaxInterviewStatus == otherTaxInfo.TaxInterviewStatus &&
		t.AddressId == otherTaxInfo.AddressId &&
		t.PayoutEntityId == otherTaxInfo.PayoutEntityId
}

func (t *TaxInfo) EqualsWithExpectedLastUpdated(other DynamoTableRecord, lastUpdatedTime time.Time, lastUpdatedDelta time.Duration) bool {
	return t.Equals(other) && timeUtils.IsTimeInForwardDelta(other.GetTimestamp(), lastUpdatedTime, lastUpdatedDelta)
}

func (t *TaxInfo) GetTable() DynamoTable {
	return &TaxInfoTable{}
}

func (t *TaxInfo) GetScanQueryTable() DynamoScanQueryTable {
	return &TaxInfoTable{}
}

func (t *TaxInfo) NewAttributeMap() map[string]*dynamodb.AttributeValue {
	itemMap := make(map[string]*dynamodb.AttributeValue)
	PopulateAttributesString(itemMap, "payoutEntityId", t.PayoutEntityId)
	PopulateAttributesString(itemMap, "addressId", t.AddressId)
	PopulateAttributesString(itemMap, "taxInterviewStatus", t.TaxInterviewStatus)
	PopulateAttributesString(itemMap, "taxWithholdingRateMap", t.TaxWithholdingRateMapJson)
	PopulateAttributesTime(itemMap, "lastUpdated", t.LastUpdated)

	return itemMap
}

func (t *TaxInfo) NewItemKey() map[string]*dynamodb.AttributeValue {
	keyMap := make(map[string]*dynamodb.AttributeValue)
	PopulateAttributesString(keyMap, "payoutEntityId", t.PayoutEntityId)

	return keyMap
}

func (t *TaxInfo) NewHashKeyEqualsExpression() string {
	return "payoutEntityId = :payoutEntityId"
}

func (t *TaxInfo) NewHashKeyExpressionAttributeValues() map[string]*dynamodb.AttributeValue {
	attributeMap := make(map[string]*dynamodb.AttributeValue)
	PopulateAttributesString(attributeMap, "payoutEntityId", t.PayoutEntityId)

	return attributeMap
}

func (t *TaxInfoTable) ConvertAttributeMapToRecord(attributeMap map[string]*dynamodb.AttributeValue) (DynamoTableRecord, error) {

	payoutEntityId := StringFromAttributes(attributeMap, "payoutEntityId")
	addressId := StringFromAttributes(attributeMap, "addressId")
	taxInterviewStatus := StringFromAttributes(attributeMap, "taxInterviewStatus")
	taxWithholdingRateMap := StringFromAttributes(attributeMap, "taxWithholdingRateMap")
	lastUpdated, err := TimeFromAttributes(attributeMap, "lastUpdated")
	if err != nil {
		return nil, err
	}

	return &TaxInfo{
		PayoutEntityId:            payoutEntityId,
		AddressId:                 addressId,
		TaxInterviewStatus:        taxInterviewStatus,
		TaxWithholdingRateMapJson: taxWithholdingRateMap,
		LastUpdated:               lastUpdated,
	}, nil
}

func (t *TaxInfoTable) ConvertScanOutputToRecords(so *dynamodb.ScanOutput) ([]DynamoScanQueryTableRecord, error) {
	records := make([]DynamoScanQueryTableRecord, *so.Count)

	for i, attributes := range so.Items {
		recordToAdd, err := t.ConvertAttributeMapToRecord(attributes)
		if err != nil {
			return nil, err
		}

		scanQueryRecord, err := recordToScanQueryRecord(recordToAdd)
		if err != nil {
			return nil, err
		}

		records[i] = scanQueryRecord
	}
	return records, nil
}

func (t *TaxInfoTable) ConvertQueryOutputToRecords(qo *dynamodb.QueryOutput) ([]DynamoScanQueryTableRecord, error) {
	records := make([]DynamoScanQueryTableRecord, *qo.Count)

	for i, attributes := range qo.Items {
		recordToAdd, err := t.ConvertAttributeMapToRecord(attributes)
		if err != nil {
			return nil, err
		}

		scanQueryRecord, err := recordToScanQueryRecord(recordToAdd)
		if err != nil {
			return nil, err
		}

		records[i] = scanQueryRecord
	}
	return records, nil
}
