package extension_transaction

import (
	"context"
	"fmt"
	"sync"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/hystrix"
	cmd "code.justin.tv/commerce/payday/hystrix"
	"code.justin.tv/commerce/payday/utils/pointers"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
	"github.com/hashicorp/go-multierror"
	"github.com/pkg/errors"
)

type ExtensionTransactionsDAO struct {
	dynamo.BaseDao
}

const (
	pageSize                 = 25
	maxConcurrentWorkers     = 4
	listTransactionsPageSize = 15
)

type IExtensionTransactionsDAO interface {
	CreateNew(transaction *ExtensionTransaction) error
	UpdateWithConditionalStatus(transaction *ExtensionTransaction, expectedStatus string) error
	Put(extensionTransaction *ExtensionTransaction) error
	Get(userID string, extensionID string) (*ExtensionTransaction, error)
	GetByTransactionIDs(transactionIDs []string) ([]*ExtensionTransaction, error)
	GetAllByUser(userID string, cursor *string) ([]*ExtensionTransaction, *string, error)
	GetAllByExtensionAndUser(extensionID string, userID string, cursor *string) ([]*ExtensionTransaction, *string, error)
	GetAllByChannelAndExtension(channelID string, extensionID string, cursor *string) ([]*ExtensionTransaction, *string, error)
	GetAllByChannel(channelID string, cursor *string) ([]*ExtensionTransaction, *string, error)
	GetAllByChannelAndExtensionAndUser(channelID string, extensionID string, userID string, cursor *string) ([]*ExtensionTransaction, *string, error)
	Delete(userID string, extensionID string) error
	GetClientConfig() *dynamo.DynamoClientConfig
	GetLatestStreamArn() (string, error)
	GetLastUpdatedTransactionsByUser(ctx context.Context, userID string, args GetLastUpdatedTransactionsArgs) ([]*ExtensionTransaction, string, error)
	GetLastUpdatedTransactionsByChannel(ctx context.Context, channelID string, args GetLastUpdatedTransactionsArgs) ([]*ExtensionTransaction, string, error)
	DeleteTable() error
	SetupTable() error
}

func NewExtensionsTransactionsDAO(client *dynamo.DynamoClient) IExtensionTransactionsDAO {
	return &ExtensionTransactionsDAO{
		BaseDao: dynamo.NewBaseDao(&ExtensionTransactionsTable{}, client),
	}
}

// Errors if transaction already exists
func (d *ExtensionTransactionsDAO) CreateNew(transaction *ExtensionTransaction) error {
	conditionalUpdate := &dynamo.ConditionalExpression{
		Expected: map[string]*dynamodb.ExpectedAttributeValue{
			"transactionID": {
				Exists: aws.Bool(false),
			},
		},
	}
	return d.GetClient().UpdateItemConditional(transaction, conditionalUpdate)
}

// Errors if transaction status does not match expected status
func (d *ExtensionTransactionsDAO) UpdateWithConditionalStatus(transaction *ExtensionTransaction, expectedStatus string) error {
	conditionalUpdate := &dynamo.ConditionalExpression{
		Expected: map[string]*dynamodb.ExpectedAttributeValue{
			"status": {
				ComparisonOperator: aws.String(dynamodb.ComparisonOperatorEq),
				Value:              &dynamodb.AttributeValue{S: aws.String(expectedStatus)},
			},
		},
	}
	return d.GetClient().UpdateItemConditional(transaction, conditionalUpdate)
}

func (d *ExtensionTransactionsDAO) Put(transaction *ExtensionTransaction) error {
	return d.GetClient().PutItem(transaction)
}

func (d *ExtensionTransactionsDAO) Get(userID string, transactionID string) (*ExtensionTransaction, error) {
	extensionTransactionChan := make(chan *ExtensionTransaction, 1)
	fn := func() error {
		extensionTransaction, err := d.get(userID, transactionID)
		if err != nil {
			return err
		}
		extensionTransactionChan <- extensionTransaction
		return nil
	}

	err := hystrix.Do(cmd.GetExtensionTransactionCommand, fn, nil)
	if err != nil {
		return nil, err
	}

	extensionTransaction := <-extensionTransactionChan
	return extensionTransaction, nil
}

func (d *ExtensionTransactionsDAO) get(userID string, transactionID string) (*ExtensionTransaction, error) {
	extensionTransactionQuery := &ExtensionTransaction{
		UserID:        userID,
		TransactionID: transactionID,
	}

	result, err := d.GetClient().GetItemWithConsistentRead(extensionTransactionQuery)
	if err != nil {
		return nil, err
	}

	if result == nil {
		return nil, nil
	}

	extensionTransactionResult, isExtensionTransaction := result.(*ExtensionTransaction)
	if !isExtensionTransaction {
		msg := "encountered an unexpected type while converting dynamo result to extension transaction"
		log.Error(msg)
		return nil, errors.New(msg)
	}

	return extensionTransactionResult, nil
}

func (d *ExtensionTransactionsDAO) GetByTransactionIDs(transactionIDs []string) ([]*ExtensionTransaction, error) {
	if len(transactionIDs) == 0 {
		return nil, nil
	}

	extensionTransactionsChan := make(chan []*ExtensionTransaction, 1)
	fn := func() error {
		extensionTransactions, err := d.getByTransactionIDs(transactionIDs)
		if err != nil {
			return err
		}
		extensionTransactionsChan <- extensionTransactions
		return nil
	}

	err := hystrix.Do(cmd.GetExtensionTransactionCommand, fn, nil)
	if err != nil {
		return nil, err
	}

	extensionTransactions := <-extensionTransactionsChan
	return extensionTransactions, nil
}

func (d *ExtensionTransactionsDAO) getByTransactionIDs(transactionIDs []string) ([]*ExtensionTransaction, error) {
	extensionTransactions := make([]*ExtensionTransaction, 0, len(transactionIDs))
	idsChan := make(chan string, len(transactionIDs))
	var multierr *multierror.Error

	mutex := &sync.Mutex{}
	wg := &sync.WaitGroup{}
	wg.Add(maxConcurrentWorkers)

	for _, transactionID := range transactionIDs {
		idsChan <- transactionID
	}
	close(idsChan)

	for i := 0; i < maxConcurrentWorkers; i++ {
		go func() {
			for {
				id, ok := <-idsChan
				if !ok {
					wg.Done()
					return
				}

				transaction, err := d.getTransactionByID(id)
				mutex.Lock()
				if err != nil {
					multierr = multierror.Append(multierr, err)
					mutex.Unlock()
					continue
				}

				if transaction != nil {
					extensionTransactions = append(extensionTransactions, transaction)
				}

				mutex.Unlock()
			}
		}()
	}

	wg.Wait()
	return extensionTransactions, multierr.ErrorOrNil()
}

func (d *ExtensionTransactionsDAO) getTransactionByID(transactionID string) (*ExtensionTransaction, error) {
	table := &ExtensionTransactionsTable{}

	queryFilter := dynamo.QueryFilter{
		Names: map[string]*string{
			"#ID": aws.String("transactionID"),
		},
		Values: map[string]*dynamodb.AttributeValue{
			":id": {S: aws.String(transactionID)},
		},
		KeyConditionExpression: aws.String("#ID = :id"),
		IndexName:              aws.String(table.GetTransactionGSI()),
	}
	results, _, err := d.GetClient().QueryTable(table, queryFilter)

	if err != nil {
		return nil, err
	}

	transactions, err := toExtensionTransactions(results)
	if err != nil {
		return nil, err
	}

	if len(transactions) > 1 {
		log.WithField("extension_transaction_id", transactionID).Warn("Found multiple extension transactions with the same transaction ID")
		return transactions[0], nil
	} else if len(transactions) == 1 {
		return transactions[0], nil
	}

	return nil, nil
}

func (d *ExtensionTransactionsDAO) Delete(userID string, transactionID string) error {
	return d.GetClient().DeleteItem(&ExtensionTransaction{
		UserID:        userID,
		TransactionID: transactionID,
	})
}

func (d *ExtensionTransactionsDAO) GetAllByUser(userID string, cursor *string) ([]*ExtensionTransaction, *string, error) {
	var transactions []*ExtensionTransaction
	var newCursor *string
	err := hystrix.Do(cmd.GetExtensionTransactionCommand, func() error {
		var innerErr error
		transactions, newCursor, innerErr = d.getAllByUser(userID, cursor)
		return innerErr
	}, nil)
	return transactions, newCursor, err
}

func (d *ExtensionTransactionsDAO) getAllByUser(userID string, cursor *string) ([]*ExtensionTransaction, *string, error) {
	table := &ExtensionTransactionsTable{}

	var exclusiveStartKey map[string]*dynamodb.AttributeValue
	if cursor != nil {
		exclusiveStartKey = map[string]*dynamodb.AttributeValue{
			"transactionID": {S: cursor},
			"userID":        {S: aws.String(userID)},
		}
	}

	queryFilter := dynamo.QueryFilter{
		Names: map[string]*string{
			"#ID": aws.String("userID"),
		},
		Values: map[string]*dynamodb.AttributeValue{
			":id": {S: aws.String(userID)},
		},
		KeyConditionExpression: aws.String("#ID = :id"),
		Limit:                  pointers.Int64P(pageSize),
		ExclusiveStartKey:      exclusiveStartKey,
	}

	result, lastEvaluatedKeyMap, err := d.GetClient().QueryTable(table, queryFilter)
	if err != nil {
		return nil, nil, err
	}

	transactions, err := toExtensionTransactions(result)
	if err != nil {
		return nil, nil, err
	}

	var lastEvaluatedKey *string
	lastEvaluatedAttribute, ok := lastEvaluatedKeyMap["transactionID"]
	if ok && lastEvaluatedAttribute != nil {
		lastEvaluatedKey = lastEvaluatedAttribute.S
	}

	return transactions, lastEvaluatedKey, nil
}

func (d *ExtensionTransactionsDAO) GetAllByExtensionAndUser(extensionID string, userID string, cursor *string) ([]*ExtensionTransaction, *string, error) {
	var transactions []*ExtensionTransaction
	var newCursor *string
	err := hystrix.Do(cmd.GetExtensionTransactionCommand, func() error {
		var innerErr error
		transactions, newCursor, innerErr = d.getAllByExtensionAndUser(extensionID, userID, cursor)
		return innerErr
	}, nil)
	return transactions, newCursor, err
}

func (d *ExtensionTransactionsDAO) getAllByExtensionAndUser(extensionID string, userID string, cursor *string) ([]*ExtensionTransaction, *string, error) {
	table := &ExtensionTransactionsTable{}
	extensionIDUserID := fmt.Sprintf("%s.%s", extensionID, userID)

	var exclusiveStartKey map[string]*dynamodb.AttributeValue
	if cursor != nil {
		exclusiveStartKey = map[string]*dynamodb.AttributeValue{
			"transactionID":     {S: cursor},
			"userID":            {S: aws.String(userID)},
			"extensionIDUserID": {S: aws.String(extensionIDUserID)},
		}
	}

	queryFilter := dynamo.QueryFilter{
		Names: map[string]*string{
			"#ID": aws.String("extensionIDUserID"),
		},
		Values: map[string]*dynamodb.AttributeValue{
			":id": {S: aws.String(extensionIDUserID)},
		},
		KeyConditionExpression: aws.String("#ID = :id"),
		Limit:                  pointers.Int64P(pageSize),
		ExclusiveStartKey:      exclusiveStartKey,
		IndexName:              aws.String(table.GetExtensionUserGSI()),
	}

	result, lastEvaluatedKeyMap, err := d.GetClient().QueryTable(table, queryFilter)
	if err != nil {
		return nil, nil, err
	}

	transactions, err := toExtensionTransactions(result)
	if err != nil {
		return nil, nil, err
	}

	var lastEvaluatedKey *string
	lastEvaluatedAttribute, ok := lastEvaluatedKeyMap["transactionID"]
	if ok && lastEvaluatedAttribute != nil {
		lastEvaluatedKey = lastEvaluatedAttribute.S
	}

	return transactions, lastEvaluatedKey, nil
}

func (d *ExtensionTransactionsDAO) GetAllByChannelAndExtensionAndUser(channelID string, extensionID string, userID string, cursor *string) ([]*ExtensionTransaction, *string, error) {
	var transactions []*ExtensionTransaction
	var newCursor *string
	err := hystrix.Do(cmd.GetExtensionTransactionCommand, func() error {
		var innerErr error
		transactions, newCursor, innerErr = d.getAllByChannelAndExtensionAndUser(channelID, extensionID, userID, cursor)
		return innerErr
	}, nil)
	return transactions, newCursor, err
}

func (d *ExtensionTransactionsDAO) getAllByChannelAndExtensionAndUser(channelID string, extensionID string, userID string, cursor *string) ([]*ExtensionTransaction, *string, error) {
	table := &ExtensionTransactionsTable{}
	channelIDextensionIDUserID := fmt.Sprintf("%s.%s.%s", channelID, extensionID, userID)

	var exclusiveStartKey map[string]*dynamodb.AttributeValue
	if cursor != nil {
		exclusiveStartKey = map[string]*dynamodb.AttributeValue{
			"transactionID":              {S: cursor},
			"userID":                     {S: aws.String(userID)},
			"channelIDExtensionIDUserID": {S: aws.String(channelIDextensionIDUserID)},
		}
	}

	queryFilter := dynamo.QueryFilter{
		Names: map[string]*string{
			"#ID": aws.String("channelIDExtensionIDUserID"),
		},
		Values: map[string]*dynamodb.AttributeValue{
			":id": {S: aws.String(channelIDextensionIDUserID)},
		},
		KeyConditionExpression: aws.String("#ID = :id"),
		Limit:                  pointers.Int64P(pageSize),
		ExclusiveStartKey:      exclusiveStartKey,
		IndexName:              aws.String(table.GetChannelExtensionUserGSI()),
	}

	result, lastEvaluatedKeyMap, err := d.GetClient().QueryTable(table, queryFilter)
	if err != nil {
		return nil, nil, err
	}

	transactions, err := toExtensionTransactions(result)
	if err != nil {
		return nil, nil, err
	}

	var lastEvaluatedKey *string
	lastEvaluatedAttribute, ok := lastEvaluatedKeyMap["transactionID"]
	if ok && lastEvaluatedAttribute != nil {
		lastEvaluatedKey = lastEvaluatedAttribute.S
	}

	return transactions, lastEvaluatedKey, nil
}

func (d *ExtensionTransactionsDAO) GetAllByChannelAndExtension(channelID string, extensionID string, cursor *string) ([]*ExtensionTransaction, *string, error) {
	// TODO: Implement
	return nil, nil, errors.New("not implemented")
}

func (d *ExtensionTransactionsDAO) GetAllByChannel(channelID string, cursor *string) ([]*ExtensionTransaction, *string, error) {
	// TODO: Implement
	return nil, nil, errors.New("not implemented")
}

type GetLastUpdatedTransactionsArgs struct {
	Cursor      string
	EventBefore *time.Time
	EventAfter  *time.Time
	Status      string
}

func (d *ExtensionTransactionsDAO) GetLastUpdatedTransactionsByUser(ctx context.Context, userID string, args GetLastUpdatedTransactionsArgs) ([]*ExtensionTransaction, string, error) {
	table := &ExtensionTransactionsTable{}
	var transactions []*ExtensionTransaction
	var cursor string
	err := hystrix.Do(cmd.GetLastUpdatedExtensionTransactionsByUserCommand, func() error {
		var innerErr error
		transactions, cursor, innerErr = d.getLastUpdatedTransactions(ctx, table.GetUserLastUpdatedGSI(), userIDStatusKey, userID, args)
		return innerErr
	}, nil)
	return transactions, cursor, err
}

func (d *ExtensionTransactionsDAO) GetLastUpdatedTransactionsByChannel(ctx context.Context, channelID string, args GetLastUpdatedTransactionsArgs) ([]*ExtensionTransaction, string, error) {
	table := &ExtensionTransactionsTable{}
	var transactions []*ExtensionTransaction
	var cursor string
	err := hystrix.Do(cmd.GetLastUpdatedExtensionTransactionsByUserCommand, func() error {
		var innerErr error
		transactions, cursor, innerErr = d.getLastUpdatedTransactions(ctx, table.GetChannelLastUpdatedGSI(), channelIDStatusKey, channelID, args)
		return innerErr
	}, nil)
	return transactions, cursor, err
}

// Searches a GSI $indexName with a partition key of {channelID|userID}Status and sort key of lastUpdated
func (d *ExtensionTransactionsDAO) getLastUpdatedTransactions(ctx context.Context, indexName, indexPK, customerID string, args GetLastUpdatedTransactionsArgs) ([]*ExtensionTransaction, string, error) {
	table := &ExtensionTransactionsTable{}
	var exclusiveStartKey map[string]*dynamodb.AttributeValue
	var err error

	if args.Cursor != "" {
		exclusiveStartKey, err = dynamo.DecodeCursor(args.Cursor)
	}

	if err != nil {
		return nil, "", errors.Wrap(err, "failed to decode cursor")
	}

	var eventAfter int64 = 0
	if args.EventAfter != nil {
		eventAfter = args.EventAfter.UnixNano()
	}

	eventBefore := time.Now().UnixNano()
	if args.EventBefore != nil {
		eventBefore = args.EventBefore.UnixNano()
	}

	keyCondition := expression.KeyAnd(
		expression.Key(indexPK).Equal(expression.Value(fmt.Sprintf("%s.%s", customerID, args.Status))),
		expression.KeyBetween(expression.Key(lastUpdatedKey), expression.Value(eventAfter), expression.Value(eventBefore)),
	)

	expr, err := expression.NewBuilder().WithKeyCondition(keyCondition).Build()

	if err != nil {
		return nil, "", err
	}

	queryFilter := dynamo.QueryFilter{
		Names:                  expr.Names(),
		Values:                 expr.Values(),
		KeyConditionExpression: expr.KeyCondition(),
		Limit:                  pointers.Int64P(listTransactionsPageSize),
		ExclusiveStartKey:      exclusiveStartKey,
		IndexName:              aws.String(indexName),
		Descending:             true,
	}

	result, lastEvaluatedKey, err := d.GetClient().QueryTableWithContext(ctx, table, queryFilter)
	if err != nil {
		return nil, "", err
	}

	transactions, err := toExtensionTransactions(result)
	if err != nil {
		return nil, "", err
	}

	cursor, err := dynamo.EncodeCursor(lastEvaluatedKey)
	if err != nil {
		return nil, "", err
	}

	return transactions, cursor, nil
}

func toExtensionTransactions(queryResult []dynamo.DynamoScanQueryTableRecord) ([]*ExtensionTransaction, error) {
	if len(queryResult) == 0 {
		return []*ExtensionTransaction{}, nil
	}

	result := make([]*ExtensionTransaction, len(queryResult))
	for i, record := range queryResult {
		extensionTransaction, ok := record.(*ExtensionTransaction)
		if !ok {
			return result, errors.New("non-extensionTransaction query result")
		}
		result[i] = extensionTransaction
	}
	return result, nil
}
