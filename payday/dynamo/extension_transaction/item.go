package extension_transaction

import (
	"time"

	"code.justin.tv/commerce/payday/dynamo"
	timeUtils "code.justin.tv/commerce/payday/utils/time"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

type ExtensionTransaction struct {
	UserID                     string // HASH
	TransactionID              string // RANGE
	ExtensionIDUserID          string // GSI HASH
	ChannelIDExtensionID       string // GSI HASH
	ChannelIDExtensionIDUserID string // GSI HASH
	ChannelID                  string // GSI HASH
	UserIDStatus               string // GSI HASH
	ChannelIDStatus            string // GSI HASH
	ExtensionID                string
	Product                    string
	InDevelopment              bool
	Broadcast                  bool
	Status                     string
	CreateTime                 time.Time
	LastUpdated                time.Time
	TotalBitsAfterTransaction  int64
	IPAddress                  string
	ZipCode                    string
	CountryCode                string
	City                       string
	TotalBitsToBroadcaster     int64
}

func (t *ExtensionTransaction) GetTable() dynamo.DynamoTable {
	return &ExtensionTransactionsTable{}
}

func (t *ExtensionTransaction) NewAttributeMap() map[string]*dynamodb.AttributeValue {
	itemMap := make(map[string]*dynamodb.AttributeValue)
	dynamo.PopulateAttributesString(itemMap, "userID", t.UserID)
	dynamo.PopulateAttributesString(itemMap, "extensionIDUserID", t.ExtensionIDUserID)
	dynamo.PopulateAttributesString(itemMap, "channelIDExtensionID", t.ChannelIDExtensionID)
	dynamo.PopulateAttributesString(itemMap, "channelIDExtensionIDUserID", t.ChannelIDExtensionIDUserID)
	dynamo.PopulateAttributesString(itemMap, userIDStatusKey, t.UserIDStatus)
	dynamo.PopulateAttributesString(itemMap, channelIDStatusKey, t.ChannelIDStatus)
	dynamo.PopulateAttributesString(itemMap, "channelID", t.ChannelID)
	dynamo.PopulateAttributesString(itemMap, "transactionID", t.TransactionID)
	dynamo.PopulateAttributesString(itemMap, "extensionID", t.ExtensionID)
	dynamo.PopulateAttributesString(itemMap, "product", t.Product)
	dynamo.PopulateAttributesBool(itemMap, "inDevelopment", t.InDevelopment)
	dynamo.PopulateAttributesBool(itemMap, "broadcast", t.Broadcast)
	dynamo.PopulateAttributesString(itemMap, "status", t.Status)
	dynamo.PopulateAttributesTime(itemMap, "createTime", t.CreateTime)
	dynamo.PopulateAttributesTime(itemMap, lastUpdatedKey, t.LastUpdated)
	dynamo.PopulateAttributesInt64(itemMap, "totalBitsAfterTransaction", t.TotalBitsAfterTransaction)
	dynamo.PopulateAttributesString(itemMap, "ipAddress", t.IPAddress)
	dynamo.PopulateAttributesString(itemMap, "zipCode", t.ZipCode)
	dynamo.PopulateAttributesString(itemMap, "countryCode", t.CountryCode)
	dynamo.PopulateAttributesString(itemMap, "city", t.City)
	dynamo.PopulateAttributesInt64(itemMap, "totalBitsToBroadcaster", t.TotalBitsToBroadcaster)
	return itemMap
}

func (t *ExtensionTransaction) NewItemKey() map[string]*dynamodb.AttributeValue {
	keyMap := make(map[string]*dynamodb.AttributeValue)
	keyMap["userID"] = &dynamodb.AttributeValue{S: aws.String(t.UserID)}
	keyMap["transactionID"] = &dynamodb.AttributeValue{S: aws.String(t.TransactionID)}
	return keyMap
}

func (t *ExtensionTransaction) NewHashKeyEqualsExpression() string {
	return "userID = :userID"
}

func (t *ExtensionTransaction) NewHashKeyExpressionAttributeValues() map[string]*dynamodb.AttributeValue {
	attributeValue := &dynamodb.AttributeValue{
		S: aws.String(t.UserID),
	}
	attributeMap := make(map[string]*dynamodb.AttributeValue)
	attributeMap[":userID"] = attributeValue
	return attributeMap
}

func (t *ExtensionTransaction) UpdateWithCurrentTimestamp() {
	t.LastUpdated = time.Now()
}

func (t *ExtensionTransaction) GetTimestamp() time.Time {
	return t.LastUpdated
}

func (t *ExtensionTransaction) ApplyTimestamp(timestamp time.Time) {
	t.LastUpdated = timestamp
}

func (t *ExtensionTransaction) Equals(other dynamo.DynamoTableRecord) bool {
	otherT, isT := other.(*ExtensionTransaction)
	if !isT {
		return false
	}

	return t.UserID == otherT.UserID &&
		t.ExtensionIDUserID == otherT.ExtensionIDUserID &&
		t.ChannelIDExtensionID == otherT.ChannelIDExtensionID &&
		t.ChannelIDExtensionIDUserID == otherT.ChannelIDExtensionIDUserID &&
		t.UserIDStatus == otherT.UserIDStatus &&
		t.ChannelIDStatus == otherT.ChannelIDStatus &&
		t.ChannelID == otherT.ChannelID &&
		t.TransactionID == otherT.TransactionID &&
		t.ExtensionID == otherT.ExtensionID &&
		t.Product == otherT.Product &&
		t.InDevelopment == otherT.InDevelopment &&
		t.Broadcast == otherT.Broadcast &&
		t.Status == otherT.Status &&
		t.CreateTime == otherT.CreateTime &&
		t.TotalBitsAfterTransaction == otherT.TotalBitsAfterTransaction &&
		t.IPAddress == otherT.IPAddress &&
		t.ZipCode == otherT.ZipCode &&
		t.CountryCode == otherT.CountryCode &&
		t.City == otherT.City &&
		t.TotalBitsToBroadcaster == otherT.TotalBitsToBroadcaster
}

func (t *ExtensionTransaction) EqualsWithExpectedLastUpdated(other dynamo.DynamoTableRecord, lastUpdatedTime time.Time, lastUpdatedDelta time.Duration) bool {
	return t.Equals(other) && timeUtils.IsTimeInForwardDelta(other.GetTimestamp(), lastUpdatedTime, lastUpdatedDelta)
}

func (t *ExtensionTransaction) GetScanQueryTable() dynamo.DynamoScanQueryTable {
	return &ExtensionTransactionsTable{}
}
