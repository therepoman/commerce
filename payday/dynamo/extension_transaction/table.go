package extension_transaction

import (
	"errors"

	"code.justin.tv/commerce/payday/dynamo"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

const (
	userIDStatusKey    = "userIDStatus"
	channelIDStatusKey = "channelIDStatus"
	lastUpdatedKey     = "lastUpdated"
)

type ExtensionTransactionsTable struct{}

func (t *ExtensionTransactionsTable) GetBaseTableName() dynamo.BaseTableName {
	return "extension_transactions"
}

func (t *ExtensionTransactionsTable) GetNodeJsRecordKeySelector() dynamo.NodeJsRecordKeySelector {
	return "record.dynamodb.Keys.userID.S + '_' + record.dynamodb.Keys.transactionID.S"
}

func (t *ExtensionTransactionsTable) GetExtensionUserGSI() string {
	return "extension-user-index"
}

func (t *ExtensionTransactionsTable) GetChannelExtensionUserGSI() string {
	return "channel-extension-user-index"
}

func (t *ExtensionTransactionsTable) GetChannelExtensionGSI() string {
	return "channel-extension-index"
}

func (t *ExtensionTransactionsTable) GetChannelGSI() string {
	return "channel-index"
}

func (t *ExtensionTransactionsTable) GetTransactionGSI() string {
	return "transaction-index"
}

func (t *ExtensionTransactionsTable) GetChannelLastUpdatedGSI() string {
	return "channel-status-lastupdated-index"
}

func (t *ExtensionTransactionsTable) GetUserLastUpdatedGSI() string {
	return "user-status-lastupdated-index"
}

func (t *ExtensionTransactionsTable) NewCreateTableInput(name dynamo.FullTableName, read dynamo.ReadCapacity, write dynamo.WriteCapacity) *dynamodb.CreateTableInput {
	return &dynamodb.CreateTableInput{
		TableName: aws.String(string(name)),
		ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
			ReadCapacityUnits:  aws.Int64(int64(read)),
			WriteCapacityUnits: aws.Int64(int64(write)),
		},
		AttributeDefinitions: []*dynamodb.AttributeDefinition{
			{
				AttributeName: aws.String("userID"),
				AttributeType: aws.String("S"),
			},
			{
				AttributeName: aws.String("transactionID"),
				AttributeType: aws.String("S"),
			},
			{
				AttributeName: aws.String("extensionIDUserID"),
				AttributeType: aws.String("S"),
			},
			{
				AttributeName: aws.String("channelIDExtensionIDUserID"),
				AttributeType: aws.String("S"),
			},
			{
				AttributeName: aws.String("channelIDExtensionID"),
				AttributeType: aws.String("S"),
			},
			{
				AttributeName: aws.String("channelID"),
				AttributeType: aws.String("S"),
			},
		},
		KeySchema: []*dynamodb.KeySchemaElement{
			{
				AttributeName: aws.String("userID"),
				KeyType:       aws.String("HASH"),
			},
			{
				AttributeName: aws.String("transactionID"),
				KeyType:       aws.String("RANGE"),
			},
		},
		GlobalSecondaryIndexes: []*dynamodb.GlobalSecondaryIndex{
			{
				IndexName: aws.String(t.GetExtensionUserGSI()),
				KeySchema: []*dynamodb.KeySchemaElement{
					{
						AttributeName: aws.String("extensionIDUserID"),
						KeyType:       aws.String("HASH"),
					},
					{
						AttributeName: aws.String("transactionID"),
						KeyType:       aws.String("RANGE"),
					},
				},
				Projection: &dynamodb.Projection{
					ProjectionType: aws.String("ALL"),
				},
				ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
					ReadCapacityUnits:  aws.Int64(int64(read)),
					WriteCapacityUnits: aws.Int64(int64(write)),
				},
			},
			{
				IndexName: aws.String(t.GetChannelExtensionUserGSI()),
				KeySchema: []*dynamodb.KeySchemaElement{
					{
						AttributeName: aws.String("channelIDExtensionIDUserID"),
						KeyType:       aws.String("HASH"),
					},
					{
						AttributeName: aws.String("transactionID"),
						KeyType:       aws.String("RANGE"),
					},
				},
				Projection: &dynamodb.Projection{
					ProjectionType: aws.String("ALL"),
				},
				ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
					ReadCapacityUnits:  aws.Int64(int64(read)),
					WriteCapacityUnits: aws.Int64(int64(write)),
				},
			},
			{
				IndexName: aws.String(t.GetChannelExtensionGSI()),
				KeySchema: []*dynamodb.KeySchemaElement{
					{
						AttributeName: aws.String("channelIDExtensionID"),
						KeyType:       aws.String("HASH"),
					},
					{
						AttributeName: aws.String("transactionID"),
						KeyType:       aws.String("RANGE"),
					},
				},
				Projection: &dynamodb.Projection{
					ProjectionType: aws.String("ALL"),
				},
				ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
					ReadCapacityUnits:  aws.Int64(int64(read)),
					WriteCapacityUnits: aws.Int64(int64(write)),
				},
			},
			{
				IndexName: aws.String(t.GetChannelGSI()),
				KeySchema: []*dynamodb.KeySchemaElement{
					{
						AttributeName: aws.String("channelID"),
						KeyType:       aws.String("HASH"),
					},
					{
						AttributeName: aws.String("transactionID"),
						KeyType:       aws.String("RANGE"),
					},
				},
				Projection: &dynamodb.Projection{
					ProjectionType: aws.String("ALL"),
				},
				ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
					ReadCapacityUnits:  aws.Int64(int64(read)),
					WriteCapacityUnits: aws.Int64(int64(write)),
				},
			},
			{
				IndexName: aws.String(t.GetTransactionGSI()),
				KeySchema: []*dynamodb.KeySchemaElement{
					{
						AttributeName: aws.String("transactionID"),
						KeyType:       aws.String("HASH"),
					},
				},
				Projection: &dynamodb.Projection{
					ProjectionType: aws.String("ALL"),
				},
				ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
					ReadCapacityUnits:  aws.Int64(int64(read)),
					WriteCapacityUnits: aws.Int64(int64(write)),
				},
			},
		},
		StreamSpecification: &dynamodb.StreamSpecification{
			StreamEnabled:  aws.Bool(true),
			StreamViewType: aws.String(dynamodb.StreamViewTypeNewAndOldImages),
		},
	}
}

func (t *ExtensionTransactionsTable) ConvertAttributeMapToRecord(attributeMap map[string]*dynamodb.AttributeValue) (dynamo.DynamoTableRecord, error) {
	lastUpdated, err := dynamo.TimeFromAttributes(attributeMap, lastUpdatedKey)
	if err != nil {
		return nil, err
	}

	createTime, err := dynamo.TimeFromAttributes(attributeMap, "createTime")
	if err != nil {
		return nil, err
	}

	totalBitsToBroadcaster, err := dynamo.Int64FromAttributes(attributeMap, "totalBitsToBroadcaster")
	if err != nil {
		return nil, err
	}

	totalBitsAfterTransaction, err := dynamo.Int64FromAttributes(attributeMap, "totalBitsAfterTransaction")
	if err != nil {
		return nil, err
	}

	return &ExtensionTransaction{
		UserID:                     dynamo.StringFromAttributes(attributeMap, "userID"),
		ExtensionIDUserID:          dynamo.StringFromAttributes(attributeMap, "extensionIDUserID"),
		ChannelIDExtensionID:       dynamo.StringFromAttributes(attributeMap, "channelIDExtensionID"),
		ChannelIDExtensionIDUserID: dynamo.StringFromAttributes(attributeMap, "channelIDExtensionIDUserID"),
		UserIDStatus:               dynamo.StringFromAttributes(attributeMap, userIDStatusKey),
		ChannelIDStatus:            dynamo.StringFromAttributes(attributeMap, channelIDStatusKey),
		ChannelID:                  dynamo.StringFromAttributes(attributeMap, "channelID"),
		TransactionID:              dynamo.StringFromAttributes(attributeMap, "transactionID"),
		ExtensionID:                dynamo.StringFromAttributes(attributeMap, "extensionID"),
		Product:                    dynamo.StringFromAttributes(attributeMap, "product"),
		InDevelopment:              dynamo.BoolFromAttributes(attributeMap, "inDevelopment"),
		Broadcast:                  dynamo.BoolFromAttributes(attributeMap, "broadcast"),
		Status:                     dynamo.StringFromAttributes(attributeMap, "status"),
		CreateTime:                 createTime,
		LastUpdated:                lastUpdated,
		TotalBitsAfterTransaction:  totalBitsAfterTransaction,
		TotalBitsToBroadcaster:     totalBitsToBroadcaster,
		City:                       dynamo.StringFromAttributes(attributeMap, "city"),
		CountryCode:                dynamo.StringFromAttributes(attributeMap, "countryCode"),
		ZipCode:                    dynamo.StringFromAttributes(attributeMap, "zipCode"),
		IPAddress:                  dynamo.StringFromAttributes(attributeMap, "ipAddress"),
	}, nil
}

func (t *ExtensionTransactionsTable) ConvertScanOutputToRecords(so *dynamodb.ScanOutput) ([]dynamo.DynamoScanQueryTableRecord, error) {
	records := make([]dynamo.DynamoScanQueryTableRecord, *so.Count)

	for i, attributes := range so.Items {
		recordToAdd, err := t.ConvertAttributeMapToRecord(attributes)
		if err != nil {
			return nil, err
		}

		scanQueryRecord, err := recordToScanQueryRecord(recordToAdd)
		if err != nil {
			return nil, err
		}

		records[i] = scanQueryRecord
	}
	return records, nil
}

func (t *ExtensionTransactionsTable) ConvertQueryOutputToRecords(qo *dynamodb.QueryOutput) ([]dynamo.DynamoScanQueryTableRecord, error) {
	records := make([]dynamo.DynamoScanQueryTableRecord, *qo.Count)

	for i, attributes := range qo.Items {
		recordToAdd, err := t.ConvertAttributeMapToRecord(attributes)
		if err != nil {
			return nil, err
		}

		scanQueryRecord, err := recordToScanQueryRecord(recordToAdd)
		if err != nil {
			return nil, err
		}

		records[i] = scanQueryRecord
	}
	return records, nil
}

func recordToScanQueryRecord(record dynamo.DynamoTableRecord) (dynamo.DynamoScanQueryTableRecord, error) {
	scanQueryRecord, isScanQueryRecord := record.(dynamo.DynamoScanQueryTableRecord)
	if !isScanQueryRecord {
		return nil, errors.New("record not a scanQueryRecord")
	}
	return scanQueryRecord, nil
}
