package dynamo_test

import (
	"testing"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/dynamo/leaderboard_badge_holders"
	"code.justin.tv/commerce/payday/test"
	"github.com/stretchr/testify/assert"
)

func mergeLeaderboardBadgeHoldersRecords(existingChannel *leaderboard_badge_holders.LeaderboardBadgeHolders, channelModifications *leaderboard_badge_holders.LeaderboardBadgeHolders) *leaderboard_badge_holders.LeaderboardBadgeHolders {
	mergedChannel := &leaderboard_badge_holders.LeaderboardBadgeHolders{
		ChannelID:         existingChannel.ChannelID,
		FirstPlaceUserID:  channelModifications.FirstPlaceUserID,
		SecondPlaceUserID: channelModifications.SecondPlaceUserID,
		ThirdPlaceUserID:  channelModifications.ThirdPlaceUserID,
	}
	return mergedChannel
}

func TestLeaderboardBadgeHoldersDao(t *testing.T) {
	if dynamoContext.SkipDynamoTests {
		t.Skip("Skipping Dynamo Tests")
	}
	leaderboardBadgeHoldersDao, _ := dynamoContext.CreateDefaultLeaderboardBadgeHoldersDao("test-dao-complete")
	err := leaderboardBadgeHoldersDao.SetupTable()
	assert.NoError(t, err)

	now := time.Now()
	log.Infof("Saving %d random channel records to dynamo", numChannelsToTest)
	savedChannels := make(map[string]*leaderboard_badge_holders.LeaderboardBadgeHolders)
	for i := 0; i < numChannelsToTest; i++ {
		channelID := test.RandomString(16)
		testLeaderboardBadgeHolders := &leaderboard_badge_holders.LeaderboardBadgeHolders{
			ChannelID:         channelID,
			FirstPlaceUserID:  test.RandomString(16),
			SecondPlaceUserID: test.RandomString(16),
			ThirdPlaceUserID:  test.RandomString(16),
		}
		err := leaderboardBadgeHoldersDao.Put(testLeaderboardBadgeHolders)
		if err != nil {
			t.Fail()
		}
		savedChannels[channelID] = testLeaderboardBadgeHolders
	}

	log.Infof("Retrieving and verifying channel records from dynamo")
	for channelId, expectedChannel := range savedChannels {
		actualChannel, err := leaderboardBadgeHoldersDao.Get(channelId)
		if err != nil {
			t.Fail()
		}
		assert.True(t, expectedChannel.EqualsWithExpectedLastUpdated(actualChannel, now, dynamoContext.AcceptableUpdateTimeDelta))
	}

	now = time.Now()
	log.Infof("Modifying existing channel records")
	for existingChannelId, existingLeaderboardBadgeHolders := range savedChannels {
		modifiedLeaderboardBadgeHolders := &leaderboard_badge_holders.LeaderboardBadgeHolders{
			ChannelID:         existingChannelId,
			FirstPlaceUserID:  test.RandomString(16),
			SecondPlaceUserID: test.RandomString(16),
			ThirdPlaceUserID:  test.RandomString(16),
		}
		err := leaderboardBadgeHoldersDao.Put(modifiedLeaderboardBadgeHolders)
		if err != nil {
			t.Fail()
		}
		savedChannels[existingChannelId] = mergeLeaderboardBadgeHoldersRecords(existingLeaderboardBadgeHolders, modifiedLeaderboardBadgeHolders)
	}

	log.Infof("Retrieving and verifying modified channel records from dynamo")
	for channelId, expectedLeaderboardBadgeHolders := range savedChannels {
		actualChannel, err := leaderboardBadgeHoldersDao.Get(channelId)
		if err != nil {
			t.Fail()
		}
		assert.True(t, expectedLeaderboardBadgeHolders.EqualsWithExpectedLastUpdated(actualChannel, now, dynamoContext.AcceptableUpdateTimeDelta))
	}

	log.Infof("Deleting channel records from dynamo")
	for channelID := range savedChannels {
		err := leaderboardBadgeHoldersDao.Delete(channelID)
		assert.NoError(t, err)
	}

	log.Infof("Verifying that channel records no longer exist in dynamo")
	for channelID := range savedChannels {
		actualLeaderboardBadgeHolders, err := leaderboardBadgeHoldersDao.Get(channelID)
		if err != nil {
			t.Fail()
		}
		assert.Nil(t, actualLeaderboardBadgeHolders)
	}
}

func TestLeaderboardBadgeHoldersDaoGSI(t *testing.T) {
	if dynamoContext.SkipDynamoTests {
		t.Skip("Skipping Dynamo Tests")
	}
	leaderboardBadgeHoldersDao, _ := dynamoContext.CreateDefaultLeaderboardBadgeHoldersDao("test-dao-complete")
	err := leaderboardBadgeHoldersDao.SetupTable()
	assert.NoError(t, err)

	numChannelsToTestForGSI := 10
	log.Infof("Saving %d badge holder records for testing GSI Queries to dynamo", numChannelsToTestForGSI)
	savedChannelsGSI := make(map[string]leaderboard_badge_holders.LeaderboardBadgeHolders)
	firstPlaceUserID := test.RandomString(16)
	secondPlaceUserID := test.RandomString(16)
	thirdPlaceUserID := test.RandomString(16)
	for i := 0; i < numChannelsToTestForGSI; i++ {
		channelID := test.RandomString(16)
		testLeaderboardBadgeHolders := leaderboard_badge_holders.LeaderboardBadgeHolders{
			ChannelID:         channelID,
			FirstPlaceUserID:  firstPlaceUserID,
			SecondPlaceUserID: secondPlaceUserID,
			ThirdPlaceUserID:  thirdPlaceUserID,
		}
		err := leaderboardBadgeHoldersDao.Put(&testLeaderboardBadgeHolders)
		if err != nil {
			t.Fail()
		}
		savedChannelsGSI[channelID] = testLeaderboardBadgeHolders
	}

	gsiNames := []leaderboard_badge_holders.GSIName{
		leaderboard_badge_holders.FirstPlaceUserIDIndex,
		leaderboard_badge_holders.SecondPlaceUserIDIndex,
		leaderboard_badge_holders.ThirdPlaceUserIDIndex,
	}
	type queryGSIFunc func(string, int, *string) ([]leaderboard_badge_holders.LeaderboardBadgeHolders, *string, error)
	pageSize := 2
	for _, gsiName := range gsiNames {
		var userID string
		var queryFunc queryGSIFunc

		switch gsiName {
		case leaderboard_badge_holders.FirstPlaceUserIDIndex:
			userID = firstPlaceUserID
			queryFunc = leaderboardBadgeHoldersDao.QueryByFirstPlaceUserID
		case leaderboard_badge_holders.SecondPlaceUserIDIndex:
			userID = secondPlaceUserID
			queryFunc = leaderboardBadgeHoldersDao.QueryBySecondPlaceUserID
		case leaderboard_badge_holders.ThirdPlaceUserIDIndex:
			userID = thirdPlaceUserID
			queryFunc = leaderboardBadgeHoldersDao.QueryByThirdPlaceUserID
		}
		log.Infof("Retrieving using QueryGSIWithLimit for GSI %s", gsiName)
		retry := true
		var lastEvaluatedKey *string
		for retry {
			resultsGSI, newLastEvaluatedKey, err := queryFunc(userID, pageSize, lastEvaluatedKey)
			assert.NoError(t, err)
			for _, result := range resultsGSI {
				expectedChannel := savedChannelsGSI[result.ChannelID]
				assert.True(t, result.Equals(&expectedChannel))
			}
			if newLastEvaluatedKey == nil {
				log.Info("No more paging keys")
				retry = false
			} else {
				log.Infof("paging key: %s", *newLastEvaluatedKey)
				lastEvaluatedKey = newLastEvaluatedKey
			}
		}

		log.Infof("Updating records with BatchUpdate for GSI %s", gsiName)
		resultsGSI, _, err := queryFunc(userID, numChannelsToTestForGSI, nil)
		assert.Nil(t, err)
		firstPlaceUserID = test.RandomString(16)
		secondPlaceUserID = test.RandomString(16)
		thirdPlaceUserID = test.RandomString(16)
		var updateQuery []leaderboard_badge_holders.LeaderboardBadgeHolders
		for _, result := range resultsGSI {
			newLeaderboardBadgeHolders := leaderboard_badge_holders.LeaderboardBadgeHolders{
				ChannelID:         result.ChannelID,
				FirstPlaceUserID:  firstPlaceUserID,
				SecondPlaceUserID: secondPlaceUserID,
				ThirdPlaceUserID:  thirdPlaceUserID,
			}
			updateQuery = append(updateQuery, newLeaderboardBadgeHolders)
			savedChannelsGSI[result.ChannelID] = newLeaderboardBadgeHolders
		}
		err = leaderboardBadgeHoldersDao.BatchPut(updateQuery)
		assert.Nil(t, err)

		log.Infof("Check that BatchUpdate was successful for GSI %s", gsiName)
		resultsGSI, _, err = queryFunc(userID, numChannelsToTestForGSI, nil)
		assert.Nil(t, err)

		for _, actualResult := range resultsGSI {
			expectedResult := savedChannelsGSI[actualResult.ChannelID]
			assert.True(t, actualResult.Equals(&expectedResult))
		}
	}

}
