package purchase_by_most_recent_super_platform

import (
	"context"
	"time"

	"code.justin.tv/commerce/payday/config"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	d "github.com/guregu/dynamo"
)

type PurchaseByMostRecentSuperPlatformDAO interface {
	Create(ctx context.Context, userID, superPlatform, platform, productID string) (*PurchaseByMostRecentSuperPlatform, error)
	Get(ctx context.Context, userID, superPlatform string) (*PurchaseByMostRecentSuperPlatform, error)
	QueryWithLimit(ctx context.Context, userID string, limit int64, pagingKey *string) ([]PurchaseByMostRecentSuperPlatform, *string, error)
	BatchDelete(ctx context.Context, records []PurchaseByMostRecentSuperPlatform) error
}

type PurchaseByMostRecentSuperPlatform struct {
	TwitchUserID  string    `dynamo:"twitchUserId"`  //hash key
	SuperPlatform string    `dynamo:"superPlatform"` //range key
	Platform      string    `dynamo:"platform"`
	ProductID     string    `dynamo:"productId"`
	DateTime      time.Time `dynamo:"datetime"`
}

type purchaseByMostRecentSuperPlatformImpl struct {
	*d.Table
}

const (
	primaryKey = "twitchUserId"
	sortKey    = "superPlatform"
)

func NewPurchaseByMostRecentSuperPlatformDAO(sess *session.Session, cfg config.Configuration) PurchaseByMostRecentSuperPlatformDAO {
	db := d.New(sess)
	table := db.Table(cfg.PurchaseByMostRecentSuperPlatformTableName)
	return &purchaseByMostRecentSuperPlatformImpl{Table: &table}
}

func (dao *purchaseByMostRecentSuperPlatformImpl) Create(ctx context.Context, userID, superPlatform, platform, productID string) (*PurchaseByMostRecentSuperPlatform, error) {
	purchaseMostRecent := &PurchaseByMostRecentSuperPlatform{
		TwitchUserID:  userID,
		SuperPlatform: superPlatform,
		Platform:      platform,
		ProductID:     productID,
		DateTime:      time.Now(),
	}

	err := dao.Put(purchaseMostRecent).RunWithContext(ctx)

	return purchaseMostRecent, err
}

func (dao *purchaseByMostRecentSuperPlatformImpl) Get(ctx context.Context, userID, superPlatform string) (*PurchaseByMostRecentSuperPlatform, error) {
	var purchaseMostRecentPlatform PurchaseByMostRecentSuperPlatform

	err := dao.Table.Get(primaryKey, userID).Range(sortKey, d.Equal, superPlatform).OneWithContext(ctx, &purchaseMostRecentPlatform)
	if err == d.ErrNotFound {
		return nil, nil
	}

	return &purchaseMostRecentPlatform, err
}

func (dao *purchaseByMostRecentSuperPlatformImpl) QueryWithLimit(ctx context.Context, userID string, limit int64, pagingKey *string) ([]PurchaseByMostRecentSuperPlatform, *string, error) {
	var purchasesMostRecentPlatform []PurchaseByMostRecentSuperPlatform

	query := dao.Table.Get(primaryKey, userID).SearchLimit(limit)

	if pagingKey != nil {
		builtPagingKey := dao.buildPagingKey(userID, *pagingKey)
		query = query.StartFrom(builtPagingKey)
	}

	newPagingKey, err := query.AllWithLastEvaluatedKeyContext(ctx, &purchasesMostRecentPlatform)
	if err != nil {
		return nil, nil, err
	}

	if newPagingKeySortKey, ok := newPagingKey[sortKey]; ok {
		return purchasesMostRecentPlatform, newPagingKeySortKey.S, nil
	}

	return purchasesMostRecentPlatform, nil, nil
}

func (dao *purchaseByMostRecentSuperPlatformImpl) buildPagingKey(userID, pagingKey string) d.PagingKey {
	newPagingKey := map[string]*dynamodb.AttributeValue{
		primaryKey: {S: aws.String(userID)},
		sortKey:    {S: &pagingKey},
	}
	return newPagingKey
}

func (dao *purchaseByMostRecentSuperPlatformImpl) BatchDelete(ctx context.Context, records []PurchaseByMostRecentSuperPlatform) error {
	deleteKeys := make([]d.Keyed, 0)
	for _, record := range records {
		deleteKeys = append(deleteKeys, d.Keys{
			record.TwitchUserID,
			record.SuperPlatform,
		})
	}

	// TODO: potentially handle retry on partial wrote, currently we are treating success of this run as all-or-nothing.
	// RunWithContext takes care of batching requests under the hood, so we are safe to pass in all the keys at once.
	_, err := dao.Table.Batch(primaryKey, sortKey).Write().Delete(deleteKeys...).RunWithContext(ctx)
	return err
}
