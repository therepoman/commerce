package dynamo_test

import (
	"testing"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/test"
	"github.com/stretchr/testify/assert"
)

const (
	numImagesToTest = 10
)

func TestImageDao(t *testing.T) {
	if dynamoContext.SkipDynamoTests {
		t.Skip("Skipping Dynamo Tests")
	}
	dao, _ := dynamoContext.CreateDefaultImageDao("test-dao-complete")
	err := dao.SetupTable()
	assert.NoError(t, err)

	now := time.Now()
	images := make([]*dynamo.Image, 0, numImagesToTest)

	log.Infof("Saving %d random image records to dynamo", numImagesToTest)
	for i := 0; i < numImagesToTest; i++ {
		image := &dynamo.Image{
			SetId:         test.RandomString(16),
			AssetType:     test.RandomString(16),
			AssetId:       test.RandomString(32),
			S3Key:         test.RandomString(128),
			AnimationType: test.RandomString(16),
			Tier:          test.RandomString(16),
			Scale:         test.RandomString(16),
		}
		err = dao.Update(image)

		assert.NoError(t, err)
		images = append(images, image)
	}

	log.Infof("Retrieving and verifying image records from dynamo")
	for i := 0; i < numImagesToTest; i++ {
		expectedImage := images[i]
		expectedImage.Version = 1

		actualImage, err := dao.Get(expectedImage.SetId, expectedImage.AssetType)
		assert.NoError(t, err)
		assert.True(t, expectedImage.EqualsWithExpectedLastUpdated(actualImage, now, dynamoContext.AcceptableUpdateTimeDelta))

		actualImageByAssetId, err := dao.GetByAssetId(expectedImage.AssetId)
		assert.NoError(t, err)
		assert.True(t, expectedImage.EqualsWithExpectedLastUpdated(actualImageByAssetId, now, dynamoContext.AcceptableUpdateTimeDelta))
	}

	now = time.Now()
	log.Infof("Modifying image records in dynamo")
	for i := 0; i < numImagesToTest; i++ {
		image := images[i]
		image.S3Key = test.RandomString(128)
		image.AnimationType = test.RandomString(16)
		image.Tier = test.RandomString(16)
		image.Scale = test.RandomString(16)

		err = dao.Update(image)
		assert.NoError(t, err)

		images[i] = image
	}

	log.Infof("Retrieving and verifying modified image records from dynamo")
	for i := 0; i < numImagesToTest; i++ {
		expectedImage := images[i]
		expectedImage.Version = 2

		actualImage, err := dao.Get(expectedImage.SetId, expectedImage.AssetType)
		assert.NoError(t, err)
		assert.True(t, expectedImage.EqualsWithExpectedLastUpdated(actualImage, now, dynamoContext.AcceptableUpdateTimeDelta))

	}

	log.Infof("Deleting image records from dynamo")
	for i := 0; i < numImagesToTest; i++ {
		image := images[i]
		err := dao.Delete(image.SetId, image.SetId)
		assert.NoError(t, err)
	}
}

func TestImageDao_GetAllBySetlId(t *testing.T) {
	if dynamoContext.SkipDynamoTests {
		t.Skip("Skipping Dynamo Tests")
	}
	dao, _ := dynamoContext.CreateDefaultImageDao("test-dao-get-all-by-set-id")
	err := dao.SetupTable()
	assert.NoError(t, err)

	images := make(map[string]*dynamo.Image)

	imageSetId := test.RandomString(16)

	numToTest := 75

	log.Infof("Saving %d random image records to dynamo", numToTest)
	for i := 0; i < numToTest; i++ {
		image := &dynamo.Image{
			SetId:         imageSetId,
			AssetType:     test.RandomString(16),
			AssetId:       test.RandomString(32),
			S3Key:         test.RandomString(128),
			AnimationType: test.RandomString(16),
			Tier:          test.RandomString(16),
			Scale:         test.RandomString(16),
		}
		err = dao.Update(image)

		assert.NoError(t, err)
		images[image.AssetId] = image
	}

	all, err := dao.GetAllBySetId(imageSetId)
	assert.NoError(t, err)

	for _, actualImage := range all {
		expectedImage, found := images[actualImage.AssetId]
		assert.True(t, found)
		assert.True(t, expectedImage.Equals(actualImage))
	}
}

func TestNewImageDao_BatchDelete(t *testing.T) {
	if dynamoContext.SkipDynamoTests {
		t.Skip("Skipping Dynamo Tests")
	}
	dao, _ := dynamoContext.CreateDefaultImageDao("test-dao-batch-delete")
	err := dao.SetupTable()
	assert.NoError(t, err)

	images := make(map[string]*dynamo.Image)

	imageSetId := test.RandomString(16)

	numToTest := 75

	log.Infof("Saving %d random image records to dynamo", numToTest)
	for i := 0; i < numToTest; i++ {
		image := &dynamo.Image{
			SetId:         imageSetId,
			AssetType:     test.RandomString(16),
			AssetId:       test.RandomString(32),
			S3Key:         test.RandomString(128),
			AnimationType: test.RandomString(16),
			Tier:          test.RandomString(16),
			Scale:         test.RandomString(16),
		}
		err = dao.Update(image)

		assert.NoError(t, err)
		images[image.AssetId] = image
	}

	all, err := dao.GetAllBySetId(imageSetId)
	assert.NoError(t, err)

	log.Infof("Batch deleting %d test image records from dynamo", numToTest)
	err = dao.BatchDelete(all)
	assert.NoError(t, err)

	all, err = dao.GetAllBySetId(imageSetId)
	assert.Empty(t, all)
	assert.NoError(t, err)
}
