package auto_refill_settings

import (
	"code.justin.tv/commerce/payday/config"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials/stscreds"
	"github.com/aws/aws-sdk-go/aws/endpoints"
	"github.com/aws/aws-sdk-go/aws/session"
	d "github.com/guregu/dynamo"
)

const (
	StatusSuccess = "SUCCESS"
	StatusAttempt = "ATTEMPT"
	StatusFailure = "FAILURE"

	StatusNoteDoubleEntitle = "POSSIBLE_DOUBLE_ENTITLE"
	StatusNoteNormal        = "NORMAL"
)

type AutoRefillPurchaseAttemptRecord struct {
	UserID        string `dynamo:"user_id,hash"`
	Time          string `dynamo:"time,range"`
	ProfileID     string `dynamo:"profile_id"`
	Threshold     int32  `dynamo:"threshold"`
	BalanceAtTime int32  `dynamo:"balance_at_time"`
	Status        string `dynamo:"status"`
	StatusNote    string `dynamo:"status_note"`
	RequestID     string `dynamo:"request_id"`
	TransactionID string `dynamo:"transaction_id"`
	OfferID       string `dynamo:"offer_id"`
}

func NewAutoRefillRecordDao(sess *session.Session, cfg config.Configuration) AutoRefillRecordDao {
	//This table is stored in a separate AWS account so use the specified role to access
	refillCreds := stscreds.NewCredentials(sess, cfg.AutoRefillDynamoRoleARN)

	db := d.New(sess, &aws.Config{
		Credentials:         refillCreds,
		STSRegionalEndpoint: endpoints.RegionalSTSEndpoint,
	})
	table := db.Table(cfg.AutoRefillRecordDynamoTableName)

	return &autoRefillRecordDaoImpl{Table: &table}
}

type AutoRefillRecordDao interface {
	WriteRecord(profile *AutoRefillPurchaseAttemptRecord) error
}

type autoRefillRecordDaoImpl struct {
	*d.Table
}

func (dao *autoRefillRecordDaoImpl) WriteRecord(profile *AutoRefillPurchaseAttemptRecord) error {
	err := dao.Put(profile).Run()

	if err != nil {
		return err
	}

	return nil
}
