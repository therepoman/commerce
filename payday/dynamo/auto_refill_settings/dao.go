package auto_refill_settings

import (
	"time"

	"code.justin.tv/commerce/payday/config"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials/stscreds"
	"github.com/aws/aws-sdk-go/aws/endpoints"
	"github.com/aws/aws-sdk-go/aws/session"
	d "github.com/guregu/dynamo"
)

const (
	LSIKey       = "user_id-active_state-index"
	LSIRangeKey  = "active_state"
	GSIKeyIndex  = "id-index"
	GSIHash      = "id"
	EnabledState = "enabled"
	HashKey      = "user_id"
	RangeKey     = "created_at"
)

type autoRefillDaoImpl struct {
	*d.Table
}

type AutoRefillDao interface {
	GetActiveSettings(userId string) ([]AutoRefillSettingsProfile, error)
	GetMostRecentSettings(userId string) (*AutoRefillSettingsProfile, error)
	GetSettingsByID(userId string) (*AutoRefillSettingsProfile, error)
	SetSettings(profile *AutoRefillSettingsProfile) (*AutoRefillSettingsProfile, error)
	GetExactSettings(userId, timestamp string) (*AutoRefillSettingsProfile, error)
}

type AutoRefillSettingsProfile struct {
	Id                         string    `dynamo:"id" index:"id-index,hash"` // UUID
	UserId                     string    `dynamo:"user_id,hash"`
	ChargeInstrumentCategory   string    `dynamo:"charge_instrument_category"`                                 // e.g. I want to be charged using “TwitchPaymentInst”
	ChargeInstrumentId         string    `dynamo:"charge_instrument_id"`                                       // uuid e.g. Refers to which TwitchPaymentInst to use
	Tpl                        string    `dynamo:"tpl"`                                                        // Tenant product line e.g. “bits_bundle”, “sub_gift”
	OfferId                    string    `dynamo:"offer_id"`                                                   // which offer to reload with
	Threshold                  int32     `dynamo:"threshold"`                                                  // If balance is below, reload offer
	IsEmailNotificationEnabled bool      `dynamo:"is_email_notification_enabled"`                              // Will send user an email with status of autoreload
	State                      string    `dynamo:"active_state" localIndex:"user_id-active_state-index,range"` // ‘active’, ‘inactive’
	CreatedAt                  string    `dynamo:"created_at,range"`                                           // timestamp
	InProgressRequestID        string    `dynamo:"in_progress_request_id"`
	InProgressRequestTime      time.Time `dynamo:"in_progress_request_time"`
	SKUQuantity                int32     `dynamo:"quantity"` // bits refill quantity, how many SKUs get refilled as part of this offer.
	SKU                        string    `dynamo:"sku"`      // SKU / id of the associated bits product (identified by offerID)
	BitsType                   string    `dynamo:"bits_type"`
	BitsQuantity               int32     `dynamo:"bits_quantity"` // bits refill quantity, how many Bits get entitled as part of this refill.
	AttemptCount               int32     `dynamo:"attempt_count"`
	PauseUntilTime             time.Time `dynamo:"pause_until_time"`
}

func NewAutoRefillDao(sess *session.Session, cfg config.Configuration) AutoRefillDao {
	//This table is stored in a separate AWS account so use the specified role to access
	refillCreds := stscreds.NewCredentials(sess, cfg.AutoRefillDynamoRoleARN)

	db := d.New(sess, &aws.Config{
		Credentials:         refillCreds,
		STSRegionalEndpoint: endpoints.RegionalSTSEndpoint,
	})
	table := db.Table(cfg.AutoRefillDynamoTableName)

	return &autoRefillDaoImpl{Table: &table}
}

func (dao *autoRefillDaoImpl) GetSettingsByID(Id string) (*AutoRefillSettingsProfile, error) {
	var userActiveRefillSettings AutoRefillSettingsProfile
	err := dao.Get(GSIHash, Id).Index(GSIKeyIndex).One(&userActiveRefillSettings)
	if err == d.ErrNotFound {
		return nil, nil
	}
	if err != nil {
		return nil, err
	}

	return &userActiveRefillSettings, nil
}

func (dao *autoRefillDaoImpl) GetActiveSettings(userId string) ([]AutoRefillSettingsProfile, error) {
	var userActiveRefillSettings []AutoRefillSettingsProfile
	err := dao.Get(HashKey, userId).Index(LSIKey).Range(LSIRangeKey, d.Equal, EnabledState).Consistent(true).All(&userActiveRefillSettings)
	if err == d.ErrNotFound {
		return nil, nil
	}
	if err != nil {
		return nil, err
	}

	return userActiveRefillSettings, nil
}

func (dao *autoRefillDaoImpl) GetMostRecentSettings(userId string) (*AutoRefillSettingsProfile, error) {
	var userMostRecentRefillSetting AutoRefillSettingsProfile
	err := dao.Get(HashKey, userId).
		Range(RangeKey, d.Greater, "0").
		Limit(1).
		Order(d.Descending).
		One(&userMostRecentRefillSetting)

	if err == d.ErrNotFound {
		return nil, nil
	}
	if err != nil {
		return nil, err
	}

	return &userMostRecentRefillSetting, nil
}

func (dao *autoRefillDaoImpl) SetSettings(profile *AutoRefillSettingsProfile) (*AutoRefillSettingsProfile, error) {
	var oldValue AutoRefillSettingsProfile
	err := dao.Put(profile).OldValue(&oldValue)

	// OldValue returns ErrNotFound if there wasn't one so eat this
	if err == d.ErrNotFound {
		return nil, nil
	}

	if err != nil {
		return nil, err
	}

	return &oldValue, nil
}

func (dao *autoRefillDaoImpl) GetExactSettings(userId, timestamp string) (*AutoRefillSettingsProfile, error) {
	var profile AutoRefillSettingsProfile

	err := dao.Get(HashKey, userId).Range(RangeKey, d.Equal, timestamp).Consistent(true).One(&profile)

	if err != nil {
		return nil, err
	}

	return &profile, nil
}
