'use strict';

var QUEUE_URL = 'https://sqs.us-west-2.amazonaws.com/021561903526/test-junk-queue';
var AWS = require('aws-sdk');
var sqs = new AWS.SQS({region : 'us-west-2'});

exports.handler = (event, context, callback) => {
    console.log('Received event:', JSON.stringify(event, null, 2));
    event.Records.forEach((record) => {
        var params = {
            MessageBody: JSON.stringify(record),
            QueueUrl: QUEUE_URL
        };
    sqs.sendMessage(params, function(err,data){
        if(err) {
            console.log('error:',"Fail Send Message" + err);
            throw err
        }else{
            console.log('Sent message: ',data.MessageId);
        }
    });
});
    callback(null, `Successfully processed ${event.Records.length} records.`);
};
