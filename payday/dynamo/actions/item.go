package actions

import (
	"time"

	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/utils/pointers"
	timeUtils "code.justin.tv/commerce/payday/utils/time"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

type Action struct {
	Prefix          string
	CreatedTime     time.Time
	Type            string
	Support100K     bool
	LastUpdated     time.Time
	CanonicalCasing string
	StartTime       time.Time
	EndTime         *time.Time
	IsCharitable    bool
}

func (a *Action) NewAttributeMap() map[string]*dynamodb.AttributeValue {
	itemMap := make(map[string]*dynamodb.AttributeValue)
	dynamo.PopulateAttributesString(itemMap, "prefix", a.Prefix)
	dynamo.PopulateAttributesTime(itemMap, "createdTime", a.CreatedTime)
	dynamo.PopulateAttributesString(itemMap, "type", a.Type)
	dynamo.PopulateAttributesBool(itemMap, "support100K", a.Support100K)
	dynamo.PopulateAttributesTime(itemMap, "lastUpdated", a.LastUpdated)
	dynamo.PopulateAttributesString(itemMap, "canonicalCasing", a.CanonicalCasing)
	dynamo.PopulateAttributesTime(itemMap, "startTime", a.StartTime)
	dynamo.PopulateAttributesOptionalTime(itemMap, "endTime", a.EndTime)
	dynamo.PopulateAttributesBool(itemMap, "isCharitable", a.IsCharitable)

	return itemMap
}

func (a *Action) NewItemKey() map[string]*dynamodb.AttributeValue {
	keyMap := make(map[string]*dynamodb.AttributeValue)
	dynamo.PopulateAttributesString(keyMap, "prefix", a.Prefix)

	return keyMap
}

func (a *Action) NewHashKeyEqualsExpression() string {
	return "prefix = :prefix"
}

func (a *Action) NewHashKeyExpressionAttributeValues() map[string]*dynamodb.AttributeValue {
	attributeMap := make(map[string]*dynamodb.AttributeValue)
	dynamo.PopulateAttributesString(attributeMap, "prefix", a.Prefix)

	return attributeMap
}

func (a *Action) UpdateWithCurrentTimestamp() {
	a.LastUpdated = time.Now()
}

func (a *Action) GetTimestamp() time.Time {
	return a.LastUpdated
}

func (a *Action) ApplyTimestamp(timestamp time.Time) {
	a.LastUpdated = timestamp
}

func (a *Action) Equals(other dynamo.DynamoTableRecord) bool {
	otherAction, isAction := other.(*Action)
	if !isAction {
		return false
	}

	return a.Prefix == otherAction.Prefix &&
		a.CreatedTime == otherAction.CreatedTime &&
		a.Type == otherAction.Type &&
		a.Support100K == otherAction.Support100K &&
		a.CanonicalCasing == otherAction.CanonicalCasing &&
		a.StartTime.Equal(otherAction.StartTime) &&
		pointers.TimePEquals(a.EndTime, otherAction.EndTime) &&
		a.IsCharitable == otherAction.IsCharitable
}

func (a *Action) EqualsWithExpectedLastUpdated(other dynamo.DynamoTableRecord, lastUpdatedTime time.Time, lastUpdatedDelta time.Duration) bool {
	return a.Equals(other) && timeUtils.IsTimeInForwardDelta(other.GetTimestamp(), lastUpdatedTime, lastUpdatedDelta)
}

func (a *Action) IsActive() bool {
	now := time.Now()

	return now.After(a.StartTime) && (a.EndTime == nil || now.Before(*a.EndTime))
}
