package actions

import (
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/hystrix"
	cmds "code.justin.tv/commerce/payday/hystrix"
	"code.justin.tv/commerce/payday/metrics"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

const (
	actionsDynamoGetLatency      = "actions_dynamo_get_latency"
	actionsDynamoBatchGetLatency = "actions_dynamo_get_batch_latency"
	actionsDynamoGetAllLatency   = "actions_dynamo_get_all_latency"
	actionsDynamoUpdateLatency   = "actions_dynamo_update_latency"
	actionsDynamoDeleteLatency   = "actions_dynamo_delete_latency"
)

type ActionDao struct {
	dynamo.BaseDao
	Statter metrics.Statter `inject:""`
}

type IActionDao interface {
	Get(prefix string) (*Action, error)
	GetAll() ([]*Action, error)
	GetBatch([]string) (map[string]*Action, error)
	Update(action *Action) error
	Delete(prefix string) error
	DeleteTable() error
	SetupTable() error
}

func NewActionDao(client *dynamo.DynamoClient) IActionDao {
	return &ActionDao{
		BaseDao: dynamo.NewBaseDao(&ActionsTable{}, client),
	}
}

func (dao *ActionDao) GetBatch(prefixes []string) (map[string]*Action, error) {
	actionsResult := make(map[string]*Action)

	actionQuery := make([]dynamo.DynamoTableRecord, 0)
	for _, prefix := range prefixes {
		actionQuery = append(actionQuery, &Action{
			Prefix: prefix,
		})
	}

	start := time.Now()
	var dynamoRecords []dynamo.DynamoTableRecord
	err := hystrix.Do(cmds.ActionsDBBatchGet, func() error {
		var err error
		dynamoRecords, err = dao.GetClient().BatchGetItem(actionQuery)
		return err
	}, nil)
	go dao.Statter.TimingDuration(actionsDynamoBatchGetLatency, time.Since(start))

	if err != nil {
		return actionsResult, err
	}

	for _, dynamoRecord := range dynamoRecords {
		actionResult := dynamoRecord.(*Action)
		actionsResult[actionResult.Prefix] = actionResult
	}

	return actionsResult, nil
}

func (dao *ActionDao) Get(prefix string) (*Action, error) {
	var actionResult *Action

	actionQuery := &Action{
		Prefix: prefix,
	}

	start := time.Now()
	var result dynamo.DynamoTableRecord
	err := hystrix.Do(cmds.ActionsDBGet, func() error {
		var err error
		result, err = dao.GetClient().GetItem(actionQuery)
		return err
	}, nil)
	go dao.Statter.TimingDuration(actionsDynamoGetLatency, time.Since(start))

	if err != nil {
		return actionResult, err
	}

	// Result will be nil if no item is found in dynamo
	if result == nil {
		return nil, nil
	}

	actionResult, isAction := result.(*Action)
	if !isAction {
		msg := "Encountered an unexpected type while converting dynamo result to action"
		log.Error(msg)
		return actionResult, errors.New(msg)
	}

	return actionResult, nil
}

func (dao *ActionDao) GetAll() ([]*Action, error) {
	filter := dynamo.ScanFilter{
		Names: map[string]*string{
			"#PREFIX": aws.String("Prefix"),
		},
		Values: map[string]*dynamodb.AttributeValue{
			":prefix": {S: aws.String(string("0"))},
		},

		Expression: aws.String("#PREFIX <> :prefix"),
	}

	start := time.Now()
	var result []dynamo.DynamoScanQueryTableRecord
	err := hystrix.Do(cmds.ActionsDBGetAll, func() error {
		var err error
		result, err = dao.GetClient().ScanTable(&ActionsTable{}, filter)
		return err
	}, nil)
	go dao.Statter.TimingDuration(actionsDynamoGetAllLatency, time.Since(start))

	if err != nil {
		return nil, err
	}

	// Need to type assert the slice of records
	actionResult := make([]*Action, len(result))
	for i, record := range result {
		var isAction bool
		actionResult[i], isAction = record.(*Action)
		if !isAction {
			msg := "Encountered an unexpected type while converting dynamo result to action record"
			log.Error(msg)
			return actionResult, errors.New(msg)
		}
	}

	return actionResult, nil
}

func (dao *ActionDao) Update(action *Action) error {
	start := time.Now()
	err := hystrix.Do(cmds.ActionsDBUpdate, func() error {
		return dao.GetClient().UpdateItem(action)
	}, nil)
	go dao.Statter.TimingDuration(actionsDynamoUpdateLatency, time.Since(start))
	return err
}

func (dao *ActionDao) Delete(prefix string) error {
	start := time.Now()
	err := hystrix.Do(cmds.ActionsDBDelete, func() error {
		deletionRecord := &Action{
			Prefix: prefix,
		}
		return dao.GetClient().DeleteItem(deletionRecord)
	}, nil)
	go dao.Statter.TimingDuration(actionsDynamoDeleteLatency, time.Since(start))
	return err
}
