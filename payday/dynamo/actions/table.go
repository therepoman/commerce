package actions

import (
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/errors"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

type ActionsTable struct {
}

func (a *ActionsTable) GetBaseTableName() dynamo.BaseTableName {
	return "actions"
}

func (a *ActionsTable) GetNodeJsRecordKeySelector() dynamo.NodeJsRecordKeySelector {
	return "record.dynamodb.Keys.prefix.S"
}

func (a *ActionsTable) NewCreateTableInput(name dynamo.FullTableName, read dynamo.ReadCapacity, write dynamo.WriteCapacity) *dynamodb.CreateTableInput {
	return &dynamodb.CreateTableInput{
		TableName: aws.String(string(name)),
		ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
			ReadCapacityUnits:  aws.Int64(int64(read)),
			WriteCapacityUnits: aws.Int64(int64(write)),
		},
		AttributeDefinitions: []*dynamodb.AttributeDefinition{
			{
				AttributeName: aws.String("prefix"),
				AttributeType: aws.String("S"),
			},
		},
		KeySchema: []*dynamodb.KeySchemaElement{
			{
				AttributeName: aws.String("prefix"),
				KeyType:       aws.String("HASH"),
			},
		},
		StreamSpecification: &dynamodb.StreamSpecification{
			StreamEnabled:  aws.Bool(true),
			StreamViewType: aws.String(dynamodb.StreamViewTypeNewAndOldImages),
		},
	}
}

func (a *ActionsTable) ConvertAttributeMapToRecord(attributeMap map[string]*dynamodb.AttributeValue) (dynamo.DynamoTableRecord, error) {
	createdTime, err := dynamo.TimeFromAttributes(attributeMap, "createdTime")
	if err != nil {
		return nil, err
	}

	startTime, err := dynamo.TimeFromAttributes(attributeMap, "startTime")
	if err != nil {
		return nil, err
	}

	endTime, err := dynamo.OptionalTimeFromAttributes(attributeMap, "endTime")
	if err != nil {
		return nil, err
	}

	lastUpdated, err := dynamo.TimeFromAttributes(attributeMap, "lastUpdated")
	if err != nil {
		return nil, err
	}

	return &Action{
		Prefix:          dynamo.StringFromAttributes(attributeMap, "prefix"),
		CreatedTime:     createdTime,
		Type:            dynamo.StringFromAttributes(attributeMap, "type"),
		Support100K:     dynamo.BoolFromAttributes(attributeMap, "support100K"),
		LastUpdated:     lastUpdated,
		CanonicalCasing: dynamo.StringFromAttributes(attributeMap, "canonicalCasing"),
		StartTime:       startTime,
		EndTime:         endTime,
		IsCharitable:    dynamo.BoolFromAttributes(attributeMap, "isCharitable"),
	}, nil
}

func (a *Action) GetTable() dynamo.DynamoTable {
	return &ActionsTable{}
}

func (a *Action) GetScanQueryTable() dynamo.DynamoScanQueryTable {
	return &ActionsTable{}
}

func (a *ActionsTable) ConvertScanOutputToRecords(so *dynamodb.ScanOutput) ([]dynamo.DynamoScanQueryTableRecord, error) {
	records := make([]dynamo.DynamoScanQueryTableRecord, *so.Count)

	for i, attributes := range so.Items {
		recordToAdd, err := a.ConvertAttributeMapToRecord(attributes)
		if err != nil {
			return nil, err
		}

		scanQueryRecord, err := recordToScanQueryRecord(recordToAdd)
		if err != nil {
			return nil, err
		}

		records[i] = scanQueryRecord
	}
	return records, nil
}

func (a *ActionsTable) ConvertQueryOutputToRecords(qo *dynamodb.QueryOutput) ([]dynamo.DynamoScanQueryTableRecord, error) {
	records := make([]dynamo.DynamoScanQueryTableRecord, *qo.Count)

	for i, attributes := range qo.Items {
		recordToAdd, err := a.ConvertAttributeMapToRecord(attributes)
		if err != nil {
			return nil, err
		}

		scanQueryRecord, err := recordToScanQueryRecord(recordToAdd)
		if err != nil {
			return nil, err
		}

		records[i] = scanQueryRecord
	}
	return records, nil
}

func recordToScanQueryRecord(record dynamo.DynamoTableRecord) (dynamo.DynamoScanQueryTableRecord, error) {
	scanQueryRecord, isScanQueryRecord := record.(dynamo.DynamoScanQueryTableRecord)
	if !isScanQueryRecord {
		return nil, errors.New("record not a scanQueryRecord")
	}
	return scanQueryRecord, nil
}
