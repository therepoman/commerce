package dynamo_test

import (
	"fmt"
	"testing"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/user_campaign_statuses"
	"code.justin.tv/commerce/payday/test"
	"github.com/stretchr/testify/assert"
)

func TestSponsoredCheermoteUserStatusesDao(t *testing.T) {
	if dynamoContext.SkipDynamoTests {
		t.Skip("Skipping Dynamo Tests")
	}
	spomoteUserStatusesDao, _ := dynamoContext.CreateDefaultSponsoredCheermoteUserStatusesDao("test-dao-complete")
	err := spomoteUserStatusesDao.SetupTable()
	assert.NoError(t, err)

	testUserID := "test-user-spomote-user-statuses"

	now := time.Now()
	savedStatuses := make(map[string]*user_campaign_statuses.SponsoredCheermoteUserStatus)
	for i := 0; i < numChannelsToTest; i++ {
		campaignID := test.RandomString(16)
		testSpomoteStatus := &user_campaign_statuses.SponsoredCheermoteUserStatus{
			CampaignID:  campaignID,
			UserID:      testUserID,
			UsedBits:    1,
			LastUpdated: time.Now(),
		}
		err := spomoteUserStatusesDao.Update(testSpomoteStatus)
		if err != nil {
			t.Fail()
		}
		savedStatuses[campaignID] = testSpomoteStatus
	}

	log.Infof("Retrieving and verifying spomote user status records from dynamo")
	for campaignID, expectedStatus := range savedStatuses {
		actualStatus, err := spomoteUserStatusesDao.Get(testUserID, campaignID)
		if err != nil {
			t.Fail()
		}
		assert.True(t, expectedStatus.EqualsWithExpectedLastUpdated(actualStatus, now, dynamoContext.AcceptableUpdateTimeDelta))
	}

	now = time.Now()
	log.Infof("Modifying existing spomote channel status status records")
	for campaignID := range savedStatuses {
		modifiedSpomoteChannelStatus := &user_campaign_statuses.SponsoredCheermoteUserStatus{
			CampaignID:  campaignID,
			UserID:      testUserID,
			UsedBits:    100,
			LastUpdated: time.Now(),
		}
		err := spomoteUserStatusesDao.Update(modifiedSpomoteChannelStatus)
		if err != nil {
			t.Fail()
		}
		savedStatuses[campaignID] = modifiedSpomoteChannelStatus
	}

	log.Infof("Retrieving and verifying modified spomote user status records from dynamo")
	for campaignID, expectedStatus := range savedStatuses {
		actualStatus, err := spomoteUserStatusesDao.Get(testUserID, campaignID)
		if err != nil {
			t.Fail()
		}
		assert.True(t, expectedStatus.EqualsWithExpectedLastUpdated(actualStatus, now, dynamoContext.AcceptableUpdateTimeDelta))
	}

	log.Infof("Deleting spomote user status records from dynamo")
	for campaignID := range savedStatuses {
		err := spomoteUserStatusesDao.Delete(testUserID, campaignID)
		assert.NoError(t, err)
	}

	log.Infof("Verifying that spomote user status records no longer exist in dynamo")
	for campaignID := range savedStatuses {
		actualStatus, err := spomoteUserStatusesDao.Get(testUserID, campaignID)
		if err != nil {
			t.Fail()
		}
		assert.Nil(t, actualStatus)
	}

	log.Infof("Adding more records to test QueryWithLimit")
	var testRecords []*user_campaign_statuses.SponsoredCheermoteUserStatus
	numOfRecords := 50
	campaignPrefix := "test"
	for i := 1; i <= numOfRecords; i++ {
		testRecords = append(testRecords, &user_campaign_statuses.SponsoredCheermoteUserStatus{
			UserID:      testUserID,
			CampaignID:  fmt.Sprintf("%s-%d", campaignPrefix, i),
			UsedBits:    1,
			LastUpdated: time.Now(),
		})
	}
	for _, testRecord := range testRecords {
		err := spomoteUserStatusesDao.Update(testRecord)
		if err != nil {
			t.Fail()
		}
	}

	log.Infof("Test QueryWithLimit to see it returns the right amount of data and right pagination")
	pageSize := 10
	var pagingKey *string
	for i := 0; i < numOfRecords/pageSize; i++ {
		actualRecords, newPagingKey, err := spomoteUserStatusesDao.QueryWithLimit(testUserID, int64(pageSize), pagingKey)
		if err != nil {
			t.Fail()
		}
		assert.Equal(t, len(actualRecords), pageSize)
		assert.NotNil(t, newPagingKey)
		pagingKey = newPagingKey
	}

	lastPageRecords, lastPagePagingKey, err := spomoteUserStatusesDao.QueryWithLimit(testUserID, int64(pageSize), pagingKey)
	if err != nil {
		t.Fail()
	}
	assert.Equal(t, len(lastPageRecords), 0)
	assert.Nil(t, lastPagePagingKey)

	log.Infof("Test BatchDelete to see all records are deleted")
	err = spomoteUserStatusesDao.BatchDelete(testRecords)
	if err != nil {
		t.Fail()
	}

	actualRecords, pagingKey, err := spomoteUserStatusesDao.QueryWithLimit(testUserID, int64(pageSize), nil)
	if err != nil {
		t.Fail()
	}
	assert.Equal(t, len(actualRecords), 0)
	assert.Nil(t, pagingKey)

}
