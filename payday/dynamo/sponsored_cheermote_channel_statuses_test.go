package dynamo_test

import (
	"fmt"
	"testing"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/channel_statuses"
	"code.justin.tv/commerce/payday/test"
	"github.com/stretchr/testify/assert"
)

func TestSponsoredCheermoteChannelStatusesDao(t *testing.T) {
	if dynamoContext.SkipDynamoTests {
		t.Skip("Skipping Dynamo Tests")
	}
	spomoteChannelStatusesDao, _ := dynamoContext.CreateDefaultSponsoredCheermoteChannelStatusesDao("test-dao-complete")
	err := spomoteChannelStatusesDao.SetupTable()
	assert.NoError(t, err)

	testChannelID := "test-user-spomote-channel-statuses"

	now := time.Now()
	savedStatuses := make(map[string]*channel_statuses.SponsoredCheermoteChannelStatus)
	for i := 0; i < numChannelsToTest; i++ {
		campaignID := test.RandomString(16)
		testSpomoteStatus := &channel_statuses.SponsoredCheermoteChannelStatus{
			CampaignID:  campaignID,
			ChannelID:   testChannelID,
			OptedIn:     false,
			LastUpdated: time.Now(),
		}
		err := spomoteChannelStatusesDao.Update(testSpomoteStatus)
		if err != nil {
			t.Fail()
		}
		savedStatuses[campaignID] = testSpomoteStatus
	}

	log.Infof("Retrieving and verifying spomote channel status records from dynamo")
	for campaignID, expectedStatus := range savedStatuses {
		actualStatus, err := spomoteChannelStatusesDao.Get(testChannelID, campaignID)
		if err != nil {
			t.Fail()
		}
		assert.True(t, expectedStatus.EqualsWithExpectedLastUpdated(actualStatus, now, dynamoContext.AcceptableUpdateTimeDelta))
	}

	now = time.Now()
	log.Infof("Modifying existing spomote channel status status records")
	for campaignID := range savedStatuses {
		modifiedSpomoteChannelStatus := &channel_statuses.SponsoredCheermoteChannelStatus{
			CampaignID:  campaignID,
			ChannelID:   testChannelID,
			OptedIn:     true,
			LastUpdated: time.Now(),
		}
		err := spomoteChannelStatusesDao.Update(modifiedSpomoteChannelStatus)
		if err != nil {
			t.Fail()
		}
		savedStatuses[campaignID] = modifiedSpomoteChannelStatus
	}

	log.Infof("Retrieving and verifying modified spomote channel status records from dynamo")
	for campaignID, expectedStatus := range savedStatuses {
		actualStatus, err := spomoteChannelStatusesDao.Get(testChannelID, campaignID)
		if err != nil {
			t.Fail()
		}
		assert.True(t, expectedStatus.EqualsWithExpectedLastUpdated(actualStatus, now, dynamoContext.AcceptableUpdateTimeDelta))
	}

	log.Infof("Deleting spomote channel status records from dynamo")
	for campaignID := range savedStatuses {
		err := spomoteChannelStatusesDao.Delete(testChannelID, campaignID)
		assert.NoError(t, err)
	}

	log.Infof("Verifying that spomote channel status records no longer exist in dynamo")
	for campaignID := range savedStatuses {
		actualStatus, err := spomoteChannelStatusesDao.Get(testChannelID, campaignID)
		if err != nil {
			t.Fail()
		}
		assert.Nil(t, actualStatus)
	}

	log.Infof("Adding more records to test QueryWithLimit")
	var testRecords []*channel_statuses.SponsoredCheermoteChannelStatus
	numOfRecords := 50
	campaignPrefix := "test"
	for i := 1; i <= numOfRecords; i++ {
		testRecords = append(testRecords, &channel_statuses.SponsoredCheermoteChannelStatus{
			ChannelID:   testChannelID,
			CampaignID:  fmt.Sprintf("%s-%d", campaignPrefix, i),
			OptedIn:     true,
			LastUpdated: time.Now(),
		})
	}
	for _, testRecord := range testRecords {
		err := spomoteChannelStatusesDao.Update(testRecord)
		if err != nil {
			t.Fail()
		}
	}

	log.Infof("Test QueryWithLimit to see it returns the right amount of data and right pagination")
	pageSize := 10
	var pagingKey *string
	for i := 0; i < numOfRecords/pageSize; i++ {
		actualRecords, newPagingKey, err := spomoteChannelStatusesDao.QueryWithLimit(testChannelID, int64(pageSize), pagingKey)
		if err != nil {
			t.Fail()
		}
		assert.Equal(t, len(actualRecords), pageSize)
		assert.NotNil(t, newPagingKey)
		pagingKey = newPagingKey
	}

	lastPageRecords, lastPagePagingKey, err := spomoteChannelStatusesDao.QueryWithLimit(testChannelID, int64(pageSize), pagingKey)
	if err != nil {
		t.Fail()
	}
	assert.Equal(t, len(lastPageRecords), 0)
	assert.Nil(t, lastPagePagingKey)

	log.Infof("Test BatchDelete to see all records are deleted")
	err = spomoteChannelStatusesDao.BatchDelete(testRecords)
	if err != nil {
		t.Fail()
	}

	actualRecords, pagingKey, err := spomoteChannelStatusesDao.QueryWithLimit(testChannelID, int64(pageSize), nil)
	if err != nil {
		t.Fail()
	}
	assert.Equal(t, len(actualRecords), 0)
	assert.Nil(t, pagingKey)

}
