package single_purchase_consumption

import (
	"context"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"

	"code.justin.tv/commerce/payday/config"
	"github.com/aws/aws-sdk-go/aws/session"
	d "github.com/guregu/dynamo"
)

type SinglePurchaseConsumptionDAO interface {
	Create(ctx context.Context, userID, productID string) (*SinglePurchaseConsumption, error)
	Get(ctx context.Context, userID, productID string) (*SinglePurchaseConsumption, error)
	QueryWithLimit(ctx context.Context, userID string, limit int64, pagingKey *string) ([]SinglePurchaseConsumption, *string, error)
	BatchDelete(ctx context.Context, records []SinglePurchaseConsumption) error
}

type SinglePurchaseConsumption struct {
	TwitchUserID string    `dynamo:"twitchUserId"` //hash key
	ProductID    string    `dynamo:"productId"`    //range key
	DateTime     time.Time `dynamo:"datetime"`
}

type singlePurchaseConsumptionDaoImpl struct {
	*d.Table
}

const (
	primaryKey = "twitchUserId"
	sortKey    = "productId"
)

func NewSinglePurchaseConsumptionDAO(sess *session.Session, cfg config.Configuration) SinglePurchaseConsumptionDAO {
	db := d.New(sess)
	table := db.Table(cfg.SinglePurchaseConsumptionsTableName)
	return &singlePurchaseConsumptionDaoImpl{Table: &table}
}

func (dao *singlePurchaseConsumptionDaoImpl) Create(ctx context.Context, userID, productID string) (*SinglePurchaseConsumption, error) {
	purchase := &SinglePurchaseConsumption{
		TwitchUserID: userID,
		ProductID:    productID,
		DateTime:     time.Now(),
	}

	err := dao.Put(purchase).RunWithContext(ctx)

	return purchase, err
}

func (dao *singlePurchaseConsumptionDaoImpl) Get(ctx context.Context, userID, productID string) (*SinglePurchaseConsumption, error) {
	var singlePurchase SinglePurchaseConsumption

	err := dao.Table.Get(primaryKey, userID).Range(sortKey, d.Equal, productID).OneWithContext(ctx, &singlePurchase)
	if err == d.ErrNotFound {
		return nil, nil
	}

	return &singlePurchase, err
}

func (dao *singlePurchaseConsumptionDaoImpl) QueryWithLimit(ctx context.Context, userID string, limit int64, pagingKey *string) ([]SinglePurchaseConsumption, *string, error) {
	var singlePurchaseConsumptions []SinglePurchaseConsumption

	query := dao.Table.Get(primaryKey, userID).SearchLimit(limit)

	if pagingKey != nil {
		builtPagingKey := dao.buildPagingKey(userID, *pagingKey)
		query = query.StartFrom(builtPagingKey)
	}

	newPagingKey, err := query.AllWithLastEvaluatedKeyContext(ctx, &singlePurchaseConsumptions)
	if err != nil {
		return nil, nil, err
	}

	if newPagingKeySortKey, ok := newPagingKey[sortKey]; ok {
		return singlePurchaseConsumptions, newPagingKeySortKey.S, nil
	}

	return singlePurchaseConsumptions, nil, nil
}

func (dao *singlePurchaseConsumptionDaoImpl) buildPagingKey(userID, pagingKey string) d.PagingKey {
	newPagingKey := map[string]*dynamodb.AttributeValue{
		primaryKey: {S: aws.String(userID)},
		sortKey:    {S: &pagingKey},
	}
	return newPagingKey
}

func (dao *singlePurchaseConsumptionDaoImpl) BatchDelete(ctx context.Context, records []SinglePurchaseConsumption) error {
	deleteKeys := make([]d.Keyed, 0)
	for _, record := range records {
		deleteKeys = append(deleteKeys, d.Keys{
			record.TwitchUserID,
			record.ProductID,
		})
	}

	// TODO: potentially handle retry on partial wrote, currently we are treating success of this run as all-or-nothing.
	// RunWithContext takes care of batching requests under the hood, so we are safe to pass in all the keys at once.
	_, err := dao.Table.Batch(primaryKey, sortKey).Write().Delete(deleteKeys...).RunWithContext(ctx)
	return err
}
