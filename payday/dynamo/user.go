package dynamo

import (
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/errors"
	timeUtils "code.justin.tv/commerce/payday/utils/time"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

type UserId string
type BannedById string

type UsersTable struct {
}

// Using pointers for user "settings" so that User struct defaults
// do not override existing state.
type User struct {
	Id                          UserId
	Banned                      bool
	BannedBy                    BannedById
	Annotation                  string
	LastUpdated                 time.Time
	Skipped                     bool // Deprecated in favor of SkippedFirstCheerTutorial
	UserState                   string
	SkippedFirstCheerTutorial   *bool
	AbandonedFirstCheerTutorial *bool
}

type UserDao struct {
	BaseDao
}

type IUserDao interface {
	Put(user *User) error
	Update(user *User) error
	Get(id UserId) (*User, error)
	Delete(id UserId) error
	DeleteTable() error
	SetupTable() error
	GetClientConfig() *DynamoClientConfig
	GetLatestStreamArn() (string, error)
}

func (u *UsersTable) GetBaseTableName() BaseTableName {
	return "users"
}

func (u *UsersTable) GetNodeJsRecordKeySelector() NodeJsRecordKeySelector {
	return "record.dynamodb.Keys.id.S"
}

func (u *UsersTable) NewCreateTableInput(name FullTableName, read ReadCapacity, write WriteCapacity) *dynamodb.CreateTableInput {
	return &dynamodb.CreateTableInput{
		TableName: aws.String(string(name)),
		ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
			ReadCapacityUnits:  aws.Int64(int64(read)),
			WriteCapacityUnits: aws.Int64(int64(write)),
		},
		AttributeDefinitions: []*dynamodb.AttributeDefinition{
			{
				AttributeName: aws.String("id"),
				AttributeType: aws.String("S"),
			},
		},
		KeySchema: []*dynamodb.KeySchemaElement{
			{
				AttributeName: aws.String("id"),
				KeyType:       aws.String("HASH"),
			},
		},
		StreamSpecification: &dynamodb.StreamSpecification{
			StreamEnabled:  aws.Bool(true),
			StreamViewType: aws.String(dynamodb.StreamViewTypeNewAndOldImages),
		},
	}
}

func (u *UsersTable) ConvertAttributeMapToRecord(attributeMap map[string]*dynamodb.AttributeValue) (DynamoTableRecord, error) {
	return &User{
		Id:                          UserId(StringFromAttributes(attributeMap, "id")),
		Banned:                      BoolFromAttributes(attributeMap, "banned"),
		BannedBy:                    BannedById(StringFromAttributes(attributeMap, "bannedBy")),
		Annotation:                  StringFromAttributes(attributeMap, "annotation"),
		SkippedFirstCheerTutorial:   OptionalBoolFromAttributes(attributeMap, "skipped_first_cheer_tutorial"),
		AbandonedFirstCheerTutorial: OptionalBoolFromAttributes(attributeMap, "abandoned_first_cheer_tutorial"),
		Skipped:                     BoolFromAttributes(attributeMap, "skipped"), // Deprecated in favor of SkippedFirstCheerTutorial
		UserState:                   StringFromAttributes(attributeMap, "userState"),
	}, nil
}

func (u *User) GetTable() DynamoTable {
	return &UsersTable{}
}

func (u *User) NewAttributeMap() map[string]*dynamodb.AttributeValue {
	itemMap := make(map[string]*dynamodb.AttributeValue)
	PopulateAttributesString(itemMap, "id", string(u.Id))
	PopulateAttributesBool(itemMap, "banned", u.Banned)
	PopulateAttributesString(itemMap, "bannedBy", string(u.BannedBy))
	PopulateAttributesString(itemMap, "annotation", u.Annotation)
	PopulateAttributesTime(itemMap, "lastUpdated", u.LastUpdated)
	PopulateAttributesBool(itemMap, "skipped", u.Skipped) // Deprecated in favor of SkippedFirstCheerTutorial
	PopulateAttributesString(itemMap, "userState", u.UserState)
	PopulateAttributesOptionalBool(itemMap, "skipped_first_cheer_tutorial", u.SkippedFirstCheerTutorial)
	PopulateAttributesOptionalBool(itemMap, "abandoned_first_cheer_tutorial", u.AbandonedFirstCheerTutorial)

	return itemMap
}

func (u *User) NewItemKey() map[string]*dynamodb.AttributeValue {
	keyMap := make(map[string]*dynamodb.AttributeValue)
	keyMap["id"] = &dynamodb.AttributeValue{S: aws.String(string(u.Id))}
	return keyMap
}

func (u *User) NewHashKeyEqualsExpression() string {
	return "id = :id"
}

func (u *User) NewHashKeyExpressionAttributeValues() map[string]*dynamodb.AttributeValue {
	attributeValue := &dynamodb.AttributeValue{
		S: aws.String(string(u.Id)),
	}
	attributeMap := make(map[string]*dynamodb.AttributeValue)
	attributeMap[":id"] = attributeValue
	return attributeMap
}

func (u *User) UpdateWithCurrentTimestamp() {
	u.LastUpdated = time.Now()
}

func (u *User) GetTimestamp() time.Time {
	return u.LastUpdated
}

func (u *User) ApplyTimestamp(timestamp time.Time) {
	u.LastUpdated = timestamp
}

func (u *User) Equals(other DynamoTableRecord) bool {
	otherUser, isUser := other.(*User)
	if !isUser {
		return false
	}

	return u.Id == otherUser.Id &&
		u.Banned == otherUser.Banned &&
		u.BannedBy == otherUser.BannedBy &&
		u.Annotation == otherUser.Annotation
}

func (u *User) EqualsWithExpectedLastUpdated(other DynamoTableRecord, lastUpdatedTime time.Time, lastUpdatedDelta time.Duration) bool {
	return u.Equals(other) && timeUtils.IsTimeInForwardDelta(other.GetTimestamp(), lastUpdatedTime, lastUpdatedDelta)
}

func NewUserDao(client *DynamoClient) IUserDao {
	dao := &UserDao{}
	dao.client = client
	dao.table = &UsersTable{}
	return dao
}

func (dao *UserDao) Put(user *User) error {
	return dao.client.PutItem(user)
}

func (dao *UserDao) Update(user *User) error {
	return dao.client.UpdateItem(user)
}

func (dao *UserDao) Get(userId UserId) (*User, error) {
	var userResult *User

	userQuery := &User{
		Id: UserId(userId),
	}

	result, err := dao.client.GetItem(userQuery)
	if err != nil {
		return userResult, err
	}

	// Result will be nil if no item is found in dynamo
	if result == nil {
		return nil, nil
	}

	userResult, isUser := result.(*User)
	if !isUser {
		msg := "Encountered an unexpected type while converting dynamo result to user"
		log.Error(msg)
		return userResult, errors.New(msg)
	}

	return userResult, nil
}

func (dao *UserDao) Delete(userId UserId) error {
	deletionRecord := &User{
		Id: UserId(userId),
	}
	return dao.client.DeleteItem(deletionRecord)
}
