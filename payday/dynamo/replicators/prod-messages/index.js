'use strict';
 
require('es6-promise').polyfill();
 
var replicator = require('dynamo-replicator')({
    region: 'us-west-2',
    table: 'prod_transactions'
});
 
exports.handler = function(event, context) {
    replicator.process(event)
        .then(function() {
            context.succeed('success');
        })
        .catch(function(err) {
            context.fail(err);
        });
};
