package balance_change_processing

import (
	"context"
	"fmt"
	"time"

	"code.justin.tv/commerce/payday/config"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/guregu/dynamo"
)

type state string

const (
	hashKey   = "transaction_id.step_identifier"
	stateKey  = "step_state"
	outputKey = "output"
	errorKey  = "handler_error"

	ProcessingState = state("processing")
	CompleteState   = state("complete")
	ErrorState      = state("handler_error")
)

type BalanceChangeProcessingRecord struct {
	TransactionIDStepIdentifier string    `dynamo:"transaction_id.step_identifier"` // hash key
	TransactionID               string    `dynamo:"transaction_id"`
	StepIdentifier              string    `dynamo:"step_identifier"`
	CreatedAt                   time.Time `dynamo:"created_at"`
	Input                       string    `dynamo:"input"`
	Output                      string    `dynamo:"output"`
	State                       state     `dynamo:"step_state"`
	Error                       string    `dynamo:"handler_error"`
}

type BalanceChangeProcessingDAO interface {
	GetRecord(ctx context.Context, transactionID, stepIdentifier string) (*BalanceChangeProcessingRecord, error)
	CreateRecord(ctx context.Context, transactionID, stepIdentifier, input string) error
	FinalizeRecord(ctx context.Context, transactionID, stepIdentifier, output string, handlerErr error) error
}

type dao struct {
	*dynamo.Table
}

func NewBalanceChangeProcessingDAO(sess *session.Session, cfg config.Configuration) BalanceChangeProcessingDAO {
	db := dynamo.New(sess)
	table := db.Table(cfg.BalanceChangeProcessingTableName) //todo

	return &dao{Table: &table}
}

func (d *dao) GetRecord(ctx context.Context, transactionID, stepIdentifier string) (*BalanceChangeProcessingRecord, error) {
	var record *BalanceChangeProcessingRecord

	err := d.Table.
		Get(hashKey, makeKey(transactionID, stepIdentifier)).
		Consistent(true).
		OneWithContext(ctx, &record)

	if err == dynamo.ErrNotFound {
		return nil, nil
	}

	return record, err
}

func (d *dao) CreateRecord(ctx context.Context, transactionID, stepIdentifier, input string) error {
	key := makeKey(transactionID, stepIdentifier)

	record := BalanceChangeProcessingRecord{
		TransactionIDStepIdentifier: key,
		TransactionID:               transactionID,
		StepIdentifier:              stepIdentifier,
		CreatedAt:                   time.Now(),
		Input:                       input,
		State:                       ProcessingState,
	}

	err := d.Table.
		Put(&record).
		RunWithContext(ctx)

	if err != nil {
		return err
	}

	return nil
}

func (d *dao) FinalizeRecord(ctx context.Context, transactionID, stepIdentifier, output string, handlerErr error) error {
	key := makeKey(transactionID, stepIdentifier)

	updater := d.Table.
		Update(hashKey, key).
		Set(outputKey, output).
		If("$ = ?", stateKey, ProcessingState)

	if handlerErr != nil {
		updater = updater.
			Set(stateKey, ErrorState).
			Set(errorKey, handlerErr.Error())
	} else {
		updater = updater.Set(stateKey, CompleteState)
	}

	err := updater.RunWithContext(ctx)

	if err != nil {
		return err
	}

	return nil
}

func makeKey(transactionID, stepIdentifier string) string {
	return fmt.Sprintf("%s.%s", transactionID, stepIdentifier)
}
