package dynamo

import (
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/hystrix"
	cmd "code.justin.tv/commerce/payday/hystrix"
	"code.justin.tv/commerce/payday/utils/pointers"
	timeUtils "code.justin.tv/commerce/payday/utils/time"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

type BadgeTierTable struct {
}

type BadgeTier struct {
	ChannelId   string
	Threshold   int64
	Enabled     bool
	LastUpdated time.Time
}

type BadgeTierDao struct {
	BaseDao
}

type IBadgeTierDao interface {
	Put(tier *BadgeTier) error
	PutAll(tiers []*BadgeTier) error
	Get(channelId string, threshold int64) (*BadgeTier, error)
	GetAll(channelId string, consistentRead bool) ([]*BadgeTier, error)
	Delete(channelId string, threshold int64) error
	DeleteTable() error
	SetupTable() error
	GetClientConfig() *DynamoClientConfig
	GetLatestStreamArn() (string, error)
}

func (u *BadgeTierTable) GetBaseTableName() BaseTableName {
	return "badge-tiers"
}

func (u *BadgeTierTable) GetNodeJsRecordKeySelector() NodeJsRecordKeySelector {
	return "record.dynamodb.Keys.channelId.S + '_' + record.dynamodb.Keys.threshold.N"
}

func (u *BadgeTierTable) NewCreateTableInput(name FullTableName, read ReadCapacity, write WriteCapacity) *dynamodb.CreateTableInput {
	return &dynamodb.CreateTableInput{
		TableName: aws.String(string(name)),
		ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
			ReadCapacityUnits:  aws.Int64(int64(read)),
			WriteCapacityUnits: aws.Int64(int64(write)),
		},
		AttributeDefinitions: []*dynamodb.AttributeDefinition{
			{
				AttributeName: aws.String("channelId"),
				AttributeType: aws.String("S"),
			},
			{
				AttributeName: aws.String("threshold"),
				AttributeType: aws.String("N"),
			},
		},
		KeySchema: []*dynamodb.KeySchemaElement{
			{
				AttributeName: aws.String("channelId"),
				KeyType:       aws.String("HASH"),
			},
			{
				AttributeName: aws.String("threshold"),
				KeyType:       aws.String("RANGE"),
			},
		},
		StreamSpecification: &dynamodb.StreamSpecification{
			StreamEnabled:  aws.Bool(true),
			StreamViewType: aws.String(dynamodb.StreamViewTypeNewAndOldImages),
		},
	}
}

func (u *BadgeTierTable) ConvertAttributeMapToRecord(attributeMap map[string]*dynamodb.AttributeValue) (DynamoTableRecord, error) {
	channelIdAttributeVal := StringFromAttributes(attributeMap, "channelId")
	thresholdAttributeVal, err := Int64FromAttributes(attributeMap, "threshold")
	if err != nil {
		return nil, err
	}
	enabledAttributeVal := BoolFromAttributes(attributeMap, "enabled")

	return &BadgeTier{
		ChannelId: channelIdAttributeVal,
		Threshold: thresholdAttributeVal,
		Enabled:   enabledAttributeVal,
	}, nil
}

func (u *BadgeTier) GetTable() DynamoTable {
	return &BadgeTierTable{}
}

func (u *BadgeTier) NewAttributeMap() map[string]*dynamodb.AttributeValue {
	itemMap := make(map[string]*dynamodb.AttributeValue)
	PopulateAttributesString(itemMap, "channelId", u.ChannelId)
	PopulateAttributesInt64(itemMap, "threshold", u.Threshold)
	PopulateAttributesBool(itemMap, "enabled", u.Enabled)
	PopulateAttributesTime(itemMap, "lastUpdated", u.LastUpdated)
	return itemMap
}

func (u *BadgeTier) NewItemKey() map[string]*dynamodb.AttributeValue {
	keyMap := make(map[string]*dynamodb.AttributeValue)
	PopulateAttributesString(keyMap, "channelId", u.ChannelId)
	PopulateAttributesInt64(keyMap, "threshold", u.Threshold)
	return keyMap
}

func (u *BadgeTier) NewHashKeyEqualsExpression() string {
	return "channelId=:ch"
}

func (u *BadgeTier) NewHashKeyExpressionAttributeValues() map[string]*dynamodb.AttributeValue {
	attributeValue := &dynamodb.AttributeValue{
		S: aws.String(u.ChannelId),
	}
	attributeMap := make(map[string]*dynamodb.AttributeValue)
	attributeMap[":ch"] = attributeValue
	return attributeMap
}

func (u *BadgeTier) UpdateWithCurrentTimestamp() {
	u.LastUpdated = time.Now()
}

func (u *BadgeTier) GetTimestamp() time.Time {
	return u.LastUpdated
}

func (u *BadgeTier) ApplyTimestamp(timestamp time.Time) {
	u.LastUpdated = timestamp
}

func (u *BadgeTier) Equals(other DynamoTableRecord) bool {
	otherTier, isTier := other.(*BadgeTier)
	if !isTier {
		return false
	}

	return u.ChannelId == otherTier.ChannelId &&
		u.Threshold == otherTier.Threshold &&
		u.Enabled == otherTier.Enabled
}

func (u *BadgeTier) EqualsWithExpectedLastUpdated(other DynamoTableRecord, lastUpdatedTime time.Time, lastUpdatedDelta time.Duration) bool {
	return u.Equals(other) && timeUtils.IsTimeInForwardDelta(other.GetTimestamp(), lastUpdatedTime, lastUpdatedDelta)
}

func NewBadgeTierDao(client *DynamoClient) IBadgeTierDao {
	dao := &BadgeTierDao{}
	dao.client = client
	dao.table = &BadgeTierTable{}
	return dao
}

func (dao *BadgeTierDao) Put(tier *BadgeTier) error {
	return dao.client.PutItem(tier)
}

func (dao *BadgeTierDao) PutAll(tiers []*BadgeTier) error {
	for _, tier := range tiers {
		err := dao.Put(tier)
		if err != nil {
			msg := "Encountered an error saving one of multiple badge tiers"
			log.Error(msg)
			return errors.Notef(err, msg)
		}
	}
	return nil
}

func (dao *BadgeTierDao) Get(channelId string, threshold int64) (*BadgeTier, error) {
	var badgeTierResult *BadgeTier

	badgeTierQuery := &BadgeTier{
		ChannelId: channelId,
		Threshold: threshold,
	}

	result, err := dao.client.GetItem(badgeTierQuery)
	if err != nil {
		return badgeTierResult, err
	}

	// Result will be nil if no item is found in dynamo
	if result == nil {
		return nil, nil
	}

	badgeTierResult, isBadgeTier := result.(*BadgeTier)
	if !isBadgeTier {
		msg := "Encountered an unexpected type while converting dynamo result to badge tier"
		log.Error(msg)
		return badgeTierResult, errors.New(msg)
	}

	return badgeTierResult, nil
}

func (dao *BadgeTierDao) GetAll(channelId string, consistentRead bool) ([]*BadgeTier, error) {
	badgeTierQuery := &BadgeTier{
		ChannelId: channelId,
	}

	badgeTierChan := make(chan []DynamoTableRecord, 1)
	fn := func() error {
		tiers, err := dao.client.queryByHashKey(badgeTierQuery, nil, pointers.BoolP(consistentRead))
		if err != nil {
			return err
		}
		badgeTierChan <- tiers
		return nil
	}

	err := hystrix.Do(cmd.GetBadgeTiersCommand, fn, nil)
	if err != nil {
		return nil, err
	}

	results := <-badgeTierChan

	badgeTierResults := make([]*BadgeTier, 0, len(results))
	for _, result := range results {
		badgeTierResult, isBadgeTier := result.(*BadgeTier)
		if !isBadgeTier {
			msg := "Encountered an unexpected type while converting dynamo result to badge tier"
			log.Error(msg)
			return nil, errors.New(msg)
		}
		badgeTierResults = append(badgeTierResults, badgeTierResult)
	}

	return badgeTierResults, nil
}

func (dao *BadgeTierDao) Delete(channelId string, threshold int64) error {
	deletionRecord := &BadgeTier{
		ChannelId: channelId,
		Threshold: threshold,
	}
	return dao.client.DeleteItem(deletionRecord)
}
