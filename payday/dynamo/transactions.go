package dynamo

import (
	"context"
	"fmt"
	"reflect"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/dynamo/holds"
	"code.justin.tv/commerce/payday/utils/pointers"
	timeUtils "code.justin.tv/commerce/payday/utils/time"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
	"github.com/pkg/errors"
)

const (
	transactionIdColumn             = "transaction_id" // Hash key
	numberOfBitsColumn              = "number_of_bits"
	numberOfBitsSponsoredColumn     = "number_of_bits_sponsored"
	tmiResponseColumn               = "tmi_response"
	tmiMessageColumn                = "tmi_message"
	rawMessageColumn                = "raw_message"
	channelIdColumn                 = "channel_id"
	transactionTypeColumn           = "transaction_type"
	userIdColumn                    = "user_id"
	lastUpdatedColumn               = "last_updated"
	notifyUserColumn                = "notify_user"
	notifiedUserColumn              = "notified_user"
	originColumn                    = "origin"
	tableName                       = "transactions"
	clientAppColumn                 = "client_app"
	bitsTypeColumn                  = "bits_type"
	productIdColumn                 = "product_id"
	platformColumn                  = "platform"
	emoteTotalsColumn               = "emote_totals"
	recipientsColumn                = "recipients"               // for executions on holds
	operandTransactionIDColumn      = "operand_transaction_id"   // for executions on holds
	userIdTransactionTypeColumn     = "user_id-transaction_type" // GSI for user - transaction type search
	pollIdColumn                    = "poll_id"
	pollChoiceIdColumn              = "poll_choice_id"
	timeOfEventColumn               = "time_of_event"
	adminIdColumn                   = "admin_id"
	adminReasonColumn               = "admin_reason"
	purchasePriceColumn             = "purchase_price"
	purchasePriceCurrencyColumn     = "purchase_price_currency"
	purchaseMarketplaceColumn       = "purchase_marketplace"
	trackingIDColumn                = "tracking_id"
	skuQuantityColumn               = "sku_quantity"
	totalBitsAfterTransactionColumn = "total_bits_after_transaction"
	totalBitsToBroadcasterColumn    = "total_bits_to_broadcaster"
	zipCodeColumn                   = "zip_code"
	partnerTypeColumn               = "partner_type"
	ipAddressColumn                 = "ip_address"
	cityColumn                      = "city"
	clientIDColumn                  = "client_id"
	countryCodeColumn               = "country_code"
	numChattersColumn               = "num_chatters_str"
	isAnonymousColumn               = "is_anonymous"
	sponsoredAmountsColumn          = "sponsored_amounts"
	originOrderIdColumn             = "origin_order_id"
	ignorePayoutColumn              = "ignore_payout"
	finalizedTransactionTypeColumn  = "finalized_transaction_type"

	userIdTransactionTypeColumnFormat = "%s-%s"
	userIDTransactionTypeIndex        = "user_id-transaction_type-index"
	userIDLastUpdatedIndex            = "user_id-last_updated-index"
	channelIDLastUpdatedIndex         = "channel_id-last_updated-index"

	getLastUpdatedTransactionsPageSize = 15
)

type TransactionsTable struct{}

type Transaction struct {
	TransactionId             string                        `dynamo:"transaction_id"`
	TmiResponse               *string                       `dynamo:"tmi_response"`
	TmiMessage                *string                       `dynamo:"tmi_message"`
	RawMessage                *string                       `dynamo:"raw_message"`
	ChannelId                 *string                       `dynamo:"channel_id"`
	UserId                    *string                       `dynamo:"user_id"`
	TransactionType           *string                       `dynamo:"transaction_type"`
	NumberOfBits              *int64                        `dynamo:"number_of_bits"`
	NumberOfBitsSponsored     *int64                        `dynamo:"number_of_bits_sponsored"`
	BitsType                  *string                       `dynamo:"bits_type"`
	ProductId                 *string                       `dynamo:"product_id"`
	Platform                  *string                       `dynamo:"platform"`
	LastUpdated               time.Time                     `dynamo:"last_updated"`
	NotifyUser                *bool                         `dynamo:"notify_user"`
	NotifiedUser              *bool                         `dynamo:"notified_user"`
	Origin                    *string                       `dynamo:"origin"`
	ClientApp                 *string                       `dynamo:"client_app"`
	EmoteTotals               map[string]int64              `dynamo:"emote_totals"`
	ExpiresAt                 *int64                        `dynamo:"expires_at"` // use in TTL for in flight transactions
	OperandTransactionID      *string                       `dynamo:"operand_transaction_id"`
	Recipients                []*holds.BitsUsageAttribution `dynamo:"recipients"`
	PollId                    *string                       `dynamo:"poll_id"`
	PollChoiceId              *string                       `dynamo:"poll_choice_id"`
	TimeOfEvent               time.Time                     `dynamo:"time_of_event"`
	AdminID                   *string                       `dynamo:"admin_id"`
	AdminReason               *string                       `dynamo:"admin_reason"`
	PurchasePrice             *string                       `dynamo:"purchase_price"`
	PurchasePriceCurrency     *string                       `dynamo:"purchase_price_currency"`
	PurchaseMarketplace       *string                       `dynamo:"purchase_marketplace"`
	TrackingID                *string                       `dynamo:"tracking_id"`
	SkuQuantity               *int64                        `dynamo:"sku_quantity"`
	TotalBitsAfterTransaction *int64                        `dynamo:"total_bits_after_transaction"`
	TotalBitsToBroadcaster    *int64                        `dynamo:"total_bits_to_broadcaster"`
	ZipCode                   *string                       `dynamo:"zip_code"`
	PartnerType               *string                       `dynamo:"partner_type"`
	IPAddress                 *string                       `dynamo:"ip_address"`
	City                      *string                       `dynamo:"city"`
	ClientID                  *string                       `dynamo:"client_id"`
	CountryCode               *string                       `dynamo:"country_code"`
	NumChatters               *string                       `dynamo:"num_chatters_str"`
	IsAnonymous               *bool                         `dynamo:"is_anonymous"`
	SponsoredAmounts          map[string]int64              `dynamo:"sponsored_amounts"`
	OriginOrderId             *string                       `dynamo:"origin_order_id"`
	IgnorePayout              *bool                         `dynamo:"ignore_payout"`
	IsMicroCheer              bool                          `dynamo:"is_micro_cheer"`
	// FinalizeHold has it's own TransactionType but also lets the user pass in an intended transaction type.
	// This field can be any of the Valid TransactionTypes (GiveBitsToBroadcaster/UseBitsOnExtension/etc) --
	// including the "FinalizeHold" type, if desired, to force the SQS processors to process this as a FinalizeHold transaction.
	FinalizedTransactionType *string `dynamo:"finalized_transaction_type"`
}

type TransactionsDao struct {
	BaseDao
}

type ITransactionsDao interface {
	CreateNew(tx *Transaction) error
	Update(tx *Transaction) error
	Get(id string) (*Transaction, error)
	BatchGet(ids []string) ([]*Transaction, error)
	Delete(id string) error
	DeleteTable() error
	SetupTable() error
	GetClientConfig() *DynamoClientConfig
	GetLatestStreamArn() (string, error)
	GetByUserIDAndTransactionType(ctx context.Context, args GetByUserIDAndTransactionTypeArgs) ([]*Transaction, error)
	GetLastUpdatedTransactionsByUser(ctx context.Context, userID string, args GetLastUpdatedTransactionsArgs) ([]*Transaction, string, error)
	GetLastUpdatedTransactionsByChannel(ctx context.Context, channelID string, args GetLastUpdatedTransactionsArgs) ([]*Transaction, string, error)
}

func (t *TransactionsTable) GetBaseTableName() BaseTableName {
	return tableName
}

func (t *TransactionsTable) GetNodeJsRecordKeySelector() NodeJsRecordKeySelector {
	return "record.dynamodb.Keys.transaction_id.S"
}

func (t *TransactionsTable) NewCreateTableInput(name FullTableName, read ReadCapacity, write WriteCapacity) *dynamodb.CreateTableInput {
	return &dynamodb.CreateTableInput{
		TableName: aws.String(string(name)),
		ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
			ReadCapacityUnits:  aws.Int64(int64(read)),
			WriteCapacityUnits: aws.Int64(int64(write)),
		},
		AttributeDefinitions: []*dynamodb.AttributeDefinition{
			{
				AttributeName: aws.String(transactionIdColumn),
				AttributeType: aws.String("S"),
			},
		},
		KeySchema: []*dynamodb.KeySchemaElement{
			{
				AttributeName: aws.String(transactionIdColumn),
				KeyType:       aws.String("HASH"),
			},
		},
		StreamSpecification: &dynamodb.StreamSpecification{
			StreamEnabled:  aws.Bool(true),
			StreamViewType: aws.String(dynamodb.StreamViewTypeNewAndOldImages),
		},
	}
}

func (t *TransactionsTable) ConvertAttributeMapToRecord(attributeMap map[string]*dynamodb.AttributeValue) (DynamoTableRecord, error) {
	numberOfBits, err := OptionalInt64FromAttributes(attributeMap, numberOfBitsColumn)
	if err != nil {
		return nil, err
	}

	numberOfBitsSponsored, err := OptionalInt64FromAttributes(attributeMap, numberOfBitsSponsoredColumn)
	if err != nil {
		return nil, err
	}

	lastUpdated, err := TimeFromAttributes(attributeMap, lastUpdatedColumn)
	if err != nil {
		return nil, err
	}

	timeOfEvent, err := TimeFromAttributes(attributeMap, timeOfEventColumn)
	if err != nil {
		return nil, err
	}

	emoteTotals, err := MapStringToInt64FromAttribute(attributeMap, emoteTotalsColumn)
	if err != nil {
		return nil, err
	}

	sponsoredAmounts, err := MapStringToInt64FromAttribute(attributeMap, sponsoredAmountsColumn)
	if err != nil {
		return nil, err
	}

	skuQuantity, err := OptionalInt64FromAttributes(attributeMap, skuQuantityColumn)
	if err != nil {
		return nil, err
	}

	totalBitsAfterTransaction, err := OptionalInt64FromAttributes(attributeMap, totalBitsAfterTransactionColumn)
	if err != nil {
		return nil, err
	}

	totalBitsToBroadcaster, err := OptionalInt64FromAttributes(attributeMap, totalBitsToBroadcasterColumn)
	if err != nil {
		return nil, err
	}

	return &Transaction{
		TransactionId:             StringFromAttributes(attributeMap, transactionIdColumn),
		TransactionType:           OptionalStringFromAttributes(attributeMap, transactionTypeColumn),
		TmiResponse:               OptionalStringFromAttributes(attributeMap, tmiResponseColumn),
		TmiMessage:                OptionalStringFromAttributes(attributeMap, tmiMessageColumn),
		RawMessage:                OptionalStringFromAttributes(attributeMap, rawMessageColumn),
		ChannelId:                 OptionalStringFromAttributes(attributeMap, channelIdColumn),
		UserId:                    OptionalStringFromAttributes(attributeMap, userIdColumn),
		NotifyUser:                OptionalBoolFromAttributes(attributeMap, notifyUserColumn),
		NotifiedUser:              OptionalBoolFromAttributes(attributeMap, notifiedUserColumn),
		Origin:                    OptionalStringFromAttributes(attributeMap, originColumn),
		NumberOfBits:              numberOfBits,
		NumberOfBitsSponsored:     numberOfBitsSponsored,
		LastUpdated:               lastUpdated,
		ClientApp:                 OptionalStringFromAttributes(attributeMap, clientAppColumn),
		BitsType:                  OptionalStringFromAttributes(attributeMap, bitsTypeColumn),
		ProductId:                 OptionalStringFromAttributes(attributeMap, productIdColumn),
		Platform:                  OptionalStringFromAttributes(attributeMap, platformColumn),
		EmoteTotals:               emoteTotals,
		OperandTransactionID:      OptionalStringFromAttributes(attributeMap, operandTransactionIDColumn),
		Recipients:                RecipientsFromAttributes(attributeMap, recipientsColumn),
		PollId:                    OptionalStringFromAttributes(attributeMap, pollIdColumn),
		PollChoiceId:              OptionalStringFromAttributes(attributeMap, pollChoiceIdColumn),
		TimeOfEvent:               timeOfEvent,
		AdminID:                   OptionalStringFromAttributes(attributeMap, adminIdColumn),
		AdminReason:               OptionalStringFromAttributes(attributeMap, adminReasonColumn),
		PurchasePrice:             OptionalStringFromAttributes(attributeMap, purchasePriceColumn),
		PurchasePriceCurrency:     OptionalStringFromAttributes(attributeMap, purchasePriceCurrencyColumn),
		PurchaseMarketplace:       OptionalStringFromAttributes(attributeMap, purchaseMarketplaceColumn),
		TrackingID:                OptionalStringFromAttributes(attributeMap, trackingIDColumn),
		SkuQuantity:               skuQuantity,
		TotalBitsAfterTransaction: totalBitsAfterTransaction,
		TotalBitsToBroadcaster:    totalBitsToBroadcaster,
		ZipCode:                   OptionalStringFromAttributes(attributeMap, zipCodeColumn),
		PartnerType:               OptionalStringFromAttributes(attributeMap, partnerTypeColumn),
		IPAddress:                 OptionalStringFromAttributes(attributeMap, ipAddressColumn),
		City:                      OptionalStringFromAttributes(attributeMap, cityColumn),
		ClientID:                  OptionalStringFromAttributes(attributeMap, clientIDColumn),
		CountryCode:               OptionalStringFromAttributes(attributeMap, countryCodeColumn),
		NumChatters:               OptionalStringFromAttributes(attributeMap, numChattersColumn),
		IsAnonymous:               OptionalBoolFromAttributes(attributeMap, isAnonymousColumn),
		OriginOrderId:             OptionalStringFromAttributes(attributeMap, originOrderIdColumn),
		SponsoredAmounts:          sponsoredAmounts,
		IgnorePayout:              OptionalBoolFromAttributes(attributeMap, ignorePayoutColumn),
		FinalizedTransactionType:  OptionalStringFromAttributes(attributeMap, finalizedTransactionTypeColumn),
	}, nil
}

func (t *TransactionsTable) ConvertScanOutputToRecords(so *dynamodb.ScanOutput) ([]DynamoScanQueryTableRecord, error) {
	records := make([]DynamoScanQueryTableRecord, *so.Count)

	for i, attributes := range so.Items {
		recordToAdd, err := t.ConvertAttributeMapToRecord(attributes)
		if err != nil {
			return nil, err
		}

		scanQueryRecord, err := recordToScanQueryRecord(recordToAdd)
		if err != nil {
			return nil, err
		}

		records[i] = scanQueryRecord
	}
	return records, nil
}

func (t *TransactionsTable) ConvertQueryOutputToRecords(qo *dynamodb.QueryOutput) ([]DynamoScanQueryTableRecord, error) {
	records := make([]DynamoScanQueryTableRecord, *qo.Count)

	for i, attributes := range qo.Items {
		recordToAdd, err := t.ConvertAttributeMapToRecord(attributes)
		if err != nil {
			return nil, err
		}

		scanQueryRecord, err := recordToScanQueryRecord(recordToAdd)
		if err != nil {
			return nil, err
		}

		records[i] = scanQueryRecord
	}
	return records, nil
}

func (t *Transaction) GetTable() DynamoTable {
	return &TransactionsTable{}
}

func (t *Transaction) NewAttributeMap() map[string]*dynamodb.AttributeValue {
	itemMap := make(map[string]*dynamodb.AttributeValue)

	PopulateAttributesTime(itemMap, lastUpdatedColumn, t.LastUpdated)

	PopulateAttributesTime(itemMap, timeOfEventColumn, t.TimeOfEvent)

	// Required attributes
	PopulateAttributesString(itemMap, transactionIdColumn, t.TransactionId)

	// Optional attributes
	if t.TmiResponse != nil {
		PopulateAttributesString(itemMap, tmiResponseColumn, *t.TmiResponse)
	}

	if t.TmiMessage != nil {
		PopulateAttributesString(itemMap, tmiMessageColumn, *t.TmiMessage)
	}

	if t.RawMessage != nil {
		PopulateAttributesString(itemMap, rawMessageColumn, *t.RawMessage)
	}

	if t.ChannelId != nil {
		PopulateAttributesString(itemMap, channelIdColumn, *t.ChannelId)
	}

	if t.UserId != nil {
		PopulateAttributesString(itemMap, userIdColumn, *t.UserId)
	}

	if t.NumberOfBits != nil {
		PopulateAttributesInt64(itemMap, numberOfBitsColumn, *t.NumberOfBits)
	}

	if t.NumberOfBitsSponsored != nil {
		PopulateAttributesInt64(itemMap, numberOfBitsSponsoredColumn, *t.NumberOfBitsSponsored)
	}

	if t.TransactionType != nil {
		PopulateAttributesString(itemMap, transactionTypeColumn, *t.TransactionType)
	}

	if t.NotifyUser != nil {
		PopulateAttributesBool(itemMap, notifyUserColumn, *t.NotifyUser)
	}

	if t.NotifiedUser != nil {
		PopulateAttributesBool(itemMap, notifiedUserColumn, *t.NotifiedUser)
	}

	if t.Origin != nil {
		PopulateAttributesString(itemMap, originColumn, *t.Origin)
	}

	if t.ClientApp != nil {
		PopulateAttributesString(itemMap, clientAppColumn, *t.ClientApp)
	}

	if !t.LastUpdated.IsZero() {
		PopulateAttributesTime(itemMap, lastUpdatedColumn, t.LastUpdated)
	}

	if t.ProductId != nil {
		PopulateAttributesString(itemMap, productIdColumn, *t.ProductId)
	}

	if t.BitsType != nil {
		PopulateAttributesString(itemMap, bitsTypeColumn, *t.BitsType)
	}

	if t.Platform != nil {
		PopulateAttributesString(itemMap, platformColumn, *t.Platform)
	}

	if len(t.EmoteTotals) > 0 {
		PopulateAttributesMapStringToInt64(itemMap, emoteTotalsColumn, t.EmoteTotals)
	}

	if t.UserId != nil && t.TransactionType != nil {
		PopulateAttributesString(itemMap, userIdTransactionTypeColumn, fmt.Sprintf(userIdTransactionTypeColumnFormat, *t.UserId, *t.TransactionType))
	}

	if t.OperandTransactionID != nil {
		PopulateAttributesString(itemMap, operandTransactionIDColumn, *t.OperandTransactionID)
	}

	if t.Recipients != nil {
		PopulateAttributeRecipients(itemMap, recipientsColumn, t.Recipients)
	}

	if t.PollId != nil {
		PopulateAttributesString(itemMap, pollIdColumn, *t.PollId)
	}

	if t.PollChoiceId != nil {
		PopulateAttributesString(itemMap, pollChoiceIdColumn, *t.PollChoiceId)
	}

	if t.AdminID != nil {
		PopulateAttributesString(itemMap, adminIdColumn, *t.AdminID)
	}

	if t.AdminReason != nil {
		PopulateAttributesString(itemMap, adminReasonColumn, *t.AdminReason)
	}

	if t.PurchasePrice != nil {
		PopulateAttributesString(itemMap, purchasePriceColumn, *t.PurchasePrice)
	}

	if t.PurchasePriceCurrency != nil {
		PopulateAttributesString(itemMap, purchasePriceCurrencyColumn, *t.PurchasePriceCurrency)
	}

	if t.PurchaseMarketplace != nil {
		PopulateAttributesString(itemMap, purchaseMarketplaceColumn, *t.PurchaseMarketplace)
	}

	if t.TrackingID != nil {
		PopulateAttributesString(itemMap, trackingIDColumn, *t.TrackingID)
	}

	if t.SkuQuantity != nil {
		PopulateAttributesInt64(itemMap, skuQuantityColumn, *t.SkuQuantity)
	}

	if t.TotalBitsAfterTransaction != nil {
		PopulateAttributesInt64(itemMap, totalBitsAfterTransactionColumn, *t.TotalBitsAfterTransaction)
	}

	if t.TotalBitsToBroadcaster != nil {
		PopulateAttributesInt64(itemMap, totalBitsToBroadcasterColumn, *t.TotalBitsToBroadcaster)
	}

	if t.ZipCode != nil {
		PopulateAttributesString(itemMap, zipCodeColumn, *t.ZipCode)
	}

	if t.PartnerType != nil {
		PopulateAttributesString(itemMap, partnerTypeColumn, *t.PartnerType)
	}

	if t.IPAddress != nil {
		PopulateAttributesString(itemMap, ipAddressColumn, *t.IPAddress)
	}

	if t.City != nil {
		PopulateAttributesString(itemMap, cityColumn, *t.City)
	}

	if t.ClientID != nil {
		PopulateAttributesString(itemMap, clientIDColumn, *t.ClientID)
	}

	if t.CountryCode != nil {
		PopulateAttributesString(itemMap, countryCodeColumn, *t.CountryCode)
	}

	if t.NumChatters != nil {
		PopulateAttributesString(itemMap, numChattersColumn, *t.NumChatters)
	}

	if t.IsAnonymous != nil {
		PopulateAttributesBool(itemMap, isAnonymousColumn, *t.IsAnonymous)
	}

	if len(t.SponsoredAmounts) > 0 {
		PopulateAttributesMapStringToInt64(itemMap, sponsoredAmountsColumn, t.SponsoredAmounts)
	}

	if t.OriginOrderId != nil {
		PopulateAttributesString(itemMap, originOrderIdColumn, *t.OriginOrderId)
	}

	if t.IgnorePayout != nil {
		PopulateAttributesBool(itemMap, ignorePayoutColumn, *t.IgnorePayout)
	}

	if t.FinalizedTransactionType != nil {
		PopulateAttributesString(itemMap, finalizedTransactionTypeColumn, *t.FinalizedTransactionType)
	}

	return itemMap
}

func (t *Transaction) NewItemKey() map[string]*dynamodb.AttributeValue {
	keyMap := make(map[string]*dynamodb.AttributeValue)
	keyMap[transactionIdColumn] = &dynamodb.AttributeValue{S: aws.String(t.TransactionId)}
	return keyMap
}

func (t *Transaction) NewHashKeyEqualsExpression() string {
	return fmt.Sprintf("%s = :%s", transactionIdColumn, transactionIdColumn)
}

func (t *Transaction) NewHashKeyExpressionAttributeValues() map[string]*dynamodb.AttributeValue {
	attributeValue := &dynamodb.AttributeValue{
		S: aws.String(t.TransactionId),
	}

	attributeMap := make(map[string]*dynamodb.AttributeValue)
	attributeMap[fmt.Sprintf(":%s", transactionIdColumn)] = attributeValue
	return attributeMap
}

func (t *Transaction) UpdateWithCurrentTimestamp() {
	t.LastUpdated = time.Now()
}

func (t *Transaction) GetTimestamp() time.Time {
	return t.LastUpdated
}

func (t *Transaction) ApplyTimestamp(timestamp time.Time) {
	t.LastUpdated = timestamp
}

func (t *Transaction) Equals(other DynamoTableRecord) bool {
	otherTransaction, isTransaction := other.(*Transaction)
	if !isTransaction {
		return false
	}

	return t.TransactionId == otherTransaction.TransactionId &&
		pointers.StringPEquals(t.TmiResponse, otherTransaction.TmiResponse) &&
		pointers.StringPEquals(t.TmiMessage, otherTransaction.TmiMessage) &&
		pointers.StringPEquals(t.RawMessage, otherTransaction.RawMessage) &&
		pointers.StringPEquals(t.ChannelId, otherTransaction.ChannelId) &&
		pointers.StringPEquals(t.UserId, otherTransaction.UserId) &&
		pointers.Int64PEquals(t.NumberOfBits, otherTransaction.NumberOfBits) &&
		pointers.Int64PEquals(t.NumberOfBitsSponsored, otherTransaction.NumberOfBitsSponsored) &&
		pointers.BoolPEquals(t.NotifyUser, otherTransaction.NotifyUser) &&
		pointers.BoolPEquals(t.NotifiedUser, otherTransaction.NotifiedUser) &&
		pointers.StringPEquals(t.Origin, otherTransaction.Origin) &&
		pointers.StringPEquals(t.ProductId, otherTransaction.ProductId) &&
		pointers.StringPEquals(t.BitsType, otherTransaction.BitsType) &&
		pointers.StringPEquals(t.Platform, otherTransaction.Platform) &&
		reflect.DeepEqual(t.EmoteTotals, otherTransaction.EmoteTotals) &&
		pointers.StringPEquals(t.OperandTransactionID, otherTransaction.OperandTransactionID) &&
		reflect.DeepEqual(t.Recipients, otherTransaction.Recipients) &&
		pointers.StringPEquals(t.PollId, otherTransaction.PollId) &&
		pointers.StringPEquals(t.PollChoiceId, otherTransaction.PollChoiceId) &&
		t.TimeOfEvent.Equal(otherTransaction.TimeOfEvent) &&
		pointers.StringPEquals(t.AdminID, otherTransaction.AdminID) &&
		pointers.StringPEquals(t.AdminReason, otherTransaction.AdminReason) &&
		pointers.StringPEquals(t.PurchasePrice, otherTransaction.PurchasePrice) &&
		pointers.StringPEquals(t.PurchasePriceCurrency, otherTransaction.PurchasePriceCurrency) &&
		pointers.StringPEquals(t.PurchaseMarketplace, otherTransaction.PurchaseMarketplace) &&
		pointers.StringPEquals(t.TrackingID, otherTransaction.TrackingID) &&
		pointers.Int64PEquals(t.SkuQuantity, otherTransaction.SkuQuantity) &&
		pointers.Int64PEquals(t.TotalBitsAfterTransaction, otherTransaction.TotalBitsAfterTransaction) &&
		pointers.Int64PEquals(t.TotalBitsToBroadcaster, otherTransaction.TotalBitsToBroadcaster) &&
		pointers.StringPEquals(t.ZipCode, otherTransaction.ZipCode) &&
		pointers.StringPEquals(t.PartnerType, otherTransaction.PartnerType) &&
		pointers.StringPEquals(t.IPAddress, otherTransaction.IPAddress) &&
		pointers.StringPEquals(t.City, otherTransaction.City) &&
		pointers.StringPEquals(t.ClientID, otherTransaction.ClientID) &&
		pointers.StringPEquals(t.CountryCode, otherTransaction.CountryCode) &&
		pointers.StringPEquals(t.NumChatters, otherTransaction.NumChatters) &&
		pointers.BoolPEquals(t.IsAnonymous, otherTransaction.IsAnonymous) &&
		pointers.StringPEquals(t.OriginOrderId, otherTransaction.OriginOrderId) &&
		reflect.DeepEqual(t.SponsoredAmounts, otherTransaction.SponsoredAmounts) &&
		pointers.BoolPEquals(t.IgnorePayout, otherTransaction.IgnorePayout) &&
		pointers.StringPEquals(t.FinalizedTransactionType, otherTransaction.FinalizedTransactionType)
}

func (t *Transaction) EqualsWithExpectedLastUpdated(other DynamoTableRecord, lastUpdatedTime time.Time, lastUpdatedDelta time.Duration) bool {
	return t.Equals(other) && timeUtils.IsTimeInForwardDelta(other.GetTimestamp(), lastUpdatedTime, lastUpdatedDelta)
}

func (t *Transaction) GetScanQueryTable() DynamoScanQueryTable {
	return &TransactionsTable{}
}

func NewTransactionsDao(client *DynamoClient) ITransactionsDao {
	dao := &TransactionsDao{}
	dao.client = client
	dao.table = &TransactionsTable{}
	return dao
}

func (dao *TransactionsDao) CreateNew(tx *Transaction) error {
	conditionalUpdate := &ConditionalExpression{
		Expected: map[string]*dynamodb.ExpectedAttributeValue{
			transactionIdColumn: {
				Exists: aws.Bool(false),
			},
		},
	}
	return dao.client.UpdateItemConditional(tx, conditionalUpdate)
}

func (dao *TransactionsDao) Update(tx *Transaction) error {
	return dao.client.UpdateItem(tx)
}

func (dao *TransactionsDao) Get(id string) (*Transaction, error) {
	var transactionResult *Transaction

	transactionQuery := &Transaction{
		TransactionId: id,
	}

	result, err := dao.client.GetItemWithConsistentRead(transactionQuery)
	if err != nil {
		return transactionResult, err
	}

	// Result will be nil if no item is found in dynamo
	if result == nil {
		return nil, nil
	}

	transactionResult, isTransaction := result.(*Transaction)
	if !isTransaction {
		msg := "Encountered an unexpected type while converting dynamo result to transaction"
		log.Error(msg)
		return transactionResult, errors.New(msg)
	}

	return transactionResult, nil
}

func (dao *TransactionsDao) BatchGet(ids []string) ([]*Transaction, error) {
	if len(ids) == 0 {
		return nil, nil
	}

	dedupedIDs := make(map[string]bool)
	queryRecords := make([]DynamoTableRecord, 0, len(ids))
	for _, id := range ids {
		if !dedupedIDs[id] {
			queryRecords = append(queryRecords, &Transaction{
				TransactionId: id,
			})
			dedupedIDs[id] = true
		}
	}

	results, err := dao.client.BatchGetItem(queryRecords)
	if err != nil {
		return nil, err
	}

	transactions := make([]*Transaction, 0, len(results))
	for _, result := range results {
		transaction, isTransaction := result.(*Transaction)
		if !isTransaction {
			msg := "Encountered an unexpected type while converting dynamo result to transaction"
			log.Error(msg)
			return nil, errors.New(msg)
		}
		transactions = append(transactions, transaction)
	}

	return transactions, nil
}

func (dao *TransactionsDao) Delete(transaction string) error {
	deletionRecord := &Transaction{
		TransactionId: transaction,
	}
	return dao.client.DeleteItem(deletionRecord)
}

type GetByUserIDAndTransactionTypeArgs struct {
	EventBefore     *time.Time
	EventAfter      *time.Time
	Limit           int64
	UserID          string
	TransactionType string
	Descending      bool
}

func (dao *TransactionsDao) GetByUserIDAndTransactionType(ctx context.Context, args GetByUserIDAndTransactionTypeArgs) ([]*Transaction, error) {
	table := &TransactionsTable{}

	var eventAfter int64 = 0
	if args.EventAfter != nil {
		eventAfter = args.EventAfter.UnixNano()
	}

	eventBefore := time.Now().UnixNano()
	if args.EventBefore != nil {
		eventBefore = args.EventBefore.UnixNano()
	}

	keyCondition := expression.KeyAnd(
		expression.Key(userIdTransactionTypeColumn).Equal(expression.Value(fmt.Sprintf(userIdTransactionTypeColumnFormat, args.UserID, args.TransactionType))),
		expression.KeyBetween(expression.Key(lastUpdatedColumn), expression.Value(eventAfter), expression.Value(eventBefore)),
	)

	expr, err := expression.NewBuilder().WithKeyCondition(keyCondition).Build()

	if err != nil {
		return nil, err
	}

	queryFilter := QueryFilter{
		Names:                  expr.Names(),
		Values:                 expr.Values(),
		KeyConditionExpression: expr.KeyCondition(),
		Limit:                  pointers.Int64P(args.Limit),
		Descending:             args.Descending,
		IndexName:              pointers.StringP(userIDTransactionTypeIndex),
	}

	results, _, err := dao.client.QueryTableWithContext(ctx, table, queryFilter)
	if err != nil {
		return nil, err
	}

	transactions, err := dynamoResultsToTransaction(results)
	if err != nil {
		return nil, err
	}

	return transactions, err
}

func (dao *TransactionsDao) GetLastUpdatedTransactionsByUser(ctx context.Context, userID string, args GetLastUpdatedTransactionsArgs) ([]*Transaction, string, error) {
	return dao.getLastUpdatedTransactions(ctx, userIDLastUpdatedIndex, userIdColumn, userID, args)
}

func (dao *TransactionsDao) GetLastUpdatedTransactionsByChannel(ctx context.Context, channelID string, args GetLastUpdatedTransactionsArgs) ([]*Transaction, string, error) {
	return dao.getLastUpdatedTransactions(ctx, channelIDLastUpdatedIndex, channelIdColumn, channelID, args)
}

type GetLastUpdatedTransactionsArgs struct {
	Cursor      string
	EventBefore *time.Time
	EventAfter  *time.Time
}

// Searches a GSI $indexName with a partition key of {channel_id|user_id} and sort key of last_updated
func (dao *TransactionsDao) getLastUpdatedTransactions(ctx context.Context, indexName, indexPK, customerID string, args GetLastUpdatedTransactionsArgs) ([]*Transaction, string, error) {
	table := &TransactionsTable{}

	var exclusiveStartKey map[string]*dynamodb.AttributeValue
	var err error

	if args.Cursor != "" {
		exclusiveStartKey, err = DecodeCursor(args.Cursor)
	}

	if err != nil {
		return nil, "", errors.Wrap(err, "failed to decode cursor")
	}

	var eventAfter int64 = 0
	if args.EventAfter != nil {
		eventAfter = args.EventAfter.UnixNano()
	}

	eventBefore := time.Now().UnixNano()
	if args.EventBefore != nil {
		eventBefore = args.EventBefore.UnixNano()
	}

	keyCondition := expression.KeyAnd(
		expression.Key(indexPK).Equal(expression.Value(customerID)),
		expression.KeyBetween(expression.Key(lastUpdatedColumn), expression.Value(eventAfter), expression.Value(eventBefore)),
	)

	expr, err := expression.NewBuilder().WithKeyCondition(keyCondition).Build()

	if err != nil {
		return nil, "", err
	}

	queryFilter := QueryFilter{
		Names:                  expr.Names(),
		Values:                 expr.Values(),
		KeyConditionExpression: expr.KeyCondition(),
		Limit:                  pointers.Int64P(getLastUpdatedTransactionsPageSize),
		ExclusiveStartKey:      exclusiveStartKey,
		IndexName:              aws.String(indexName),
		Descending:             true,
	}

	result, lastEvaluatedKey, err := dao.GetClient().QueryTableWithContext(ctx, table, queryFilter)
	if err != nil {
		return nil, "", err
	}

	transactions, err := dynamoResultsToTransaction(result)
	if err != nil {
		return nil, "", err
	}

	cursor, err := EncodeCursor(lastEvaluatedKey)
	if err != nil {
		return nil, "", err
	}

	return transactions, cursor, nil
}

func dynamoResultsToTransaction(results []DynamoScanQueryTableRecord) ([]*Transaction, error) {
	transactions := make([]*Transaction, 0, len(results))
	for _, result := range results {
		transaction, isTransaction := result.(*Transaction)
		if !isTransaction {
			return nil, errors.New("Encountered an unexpected type while converting dynamo result to transaction")
		}
		transactions = append(transactions, transaction)
	}

	return transactions, nil
}
