package dynamo_test

import (
	"testing"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/test"
	"github.com/stretchr/testify/assert"
)

const numUsersToTest = 3

func TestUserDao(t *testing.T) {
	if dynamoContext.SkipDynamoTests {
		t.Skip("Skipping Dynamo Tests")
	}
	userDao, _ := dynamoContext.CreateDefaultUserDao("test-dao-complete")
	err := userDao.SetupTable()
	assert.NoError(t, err)

	now := time.Now()
	log.Infof("Saving %d random user records to dynamo", numUsersToTest)
	savedUsers := make(map[dynamo.UserId]*dynamo.User)
	for i := 0; i < numUsersToTest; i++ {
		userName := dynamo.UserId(test.RandomString(16))
		testUser := &dynamo.User{
			Id:         userName,
			Banned:     test.RandomBool(),
			BannedBy:   dynamo.BannedById(test.RandomString(16)),
			Annotation: test.RandomString(16),
		}
		err := userDao.Put(testUser)
		if err != nil {
			t.Fail()
		}
		savedUsers[userName] = testUser
	}

	log.Infof("Retrieving and verifying user records from dynamo")
	for userName, expectedUser := range savedUsers {
		actualUser, err := userDao.Get(userName)
		if err != nil {
			t.Fail()
		}
		assert.True(t, expectedUser.EqualsWithExpectedLastUpdated(actualUser, now, dynamoContext.AcceptableUpdateTimeDelta))
	}

	now = time.Now()
	log.Infof("Modifying existing user records")
	for existingUserName := range savedUsers {
		modifiedUser := &dynamo.User{
			Id:         existingUserName,
			Banned:     test.RandomBool(),
			BannedBy:   dynamo.BannedById(test.RandomString(16)),
			Annotation: test.RandomString(16),
		}
		err := userDao.Put(modifiedUser)
		if err != nil {
			t.Fail()
		}
		savedUsers[existingUserName] = modifiedUser
	}

	log.Infof("Retrieving and verifying modified user records from dynamo")
	for userName, expectedUser := range savedUsers {
		actualUser, err := userDao.Get(userName)
		if err != nil {
			t.Fail()
		}
		assert.True(t, expectedUser.EqualsWithExpectedLastUpdated(actualUser, now, dynamoContext.AcceptableUpdateTimeDelta))
	}

	log.Infof("Deleting user records from dynamo")
	for userName := range savedUsers {
		err := userDao.Delete(userName)
		assert.NoError(t, err)
	}

	log.Infof("Verifying that user records no longer exist in dynamo")
	for userName := range savedUsers {
		actualUser, err := userDao.Get(userName)
		if err != nil {
			t.Fail()
		}
		assert.Nil(t, actualUser)
	}
}
