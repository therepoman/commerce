package dynamo

import (
	"context"
	"fmt"
	"strings"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/audit"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/utils/pointers"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/credentials/stscreds"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/cenkalti/backoff"
)

const (
	dynamoActiveStatus                 = "ACTIVE"
	auditLambdaSuffix                  = "_save_audit_record"
	auditLambdaHandler                 = "auditorLambda.handler"
	auditLambdaFileName                = "auditorLambda.js"
	defaultReadCapacity  ReadCapacity  = 100
	defaultWriteCapacity WriteCapacity = 100
	defaultWaitTimeoutMs time.Duration = 1000000
	batchSize                          = 100
	batchWriteSize                     = 25 // https://docs.aws.amazon.com/amazondynamodb/latest/APIReference/API_BatchWriteItem.html
	batchWriteRetryCount               = 7
)

type DynamoAuditConfig struct {
	RecordsS3Bucket      string
	LambdaMemorySizeMB   int64
	LambdaTimeoutSec     int64
	LambdaRoleName       string
	LambdaCodeS3Bucket   string
	LambdaEventBatchSize int64
	OverwriteExisting    bool
}

type DynamoClientConfig struct {
	AwsRegion                           string
	TablePrefix                         string
	TableSuffix                         string
	NewTableReadCapacity                ReadCapacity
	NewTableWriteCapacity               WriteCapacity
	OverwriteExistingTablesDuringSetup  bool
	EndpointOverride                    string
	CredentialsOverride                 *credentials.Credentials
	TableExistenceWaitTimeoutMsOverride time.Duration
	TableStatusWaitTimeoutMsOverride    time.Duration
}

type DynamoClient struct {
	Dynamo       *dynamodb.DynamoDB
	ClientConfig *DynamoClientConfig
	AuditSetup   *audit.AuditSetup
	AuditConfig  *DynamoAuditConfig
}

type ScanFilter struct {
	Expression *string
	Names      map[string]*string
	Values     map[string]*dynamodb.AttributeValue
}

type QueryFilter struct {
	KeyConditionExpression *string
	Names                  map[string]*string
	Values                 map[string]*dynamodb.AttributeValue
	Limit                  *int64
	Descending             bool
	IndexName              *string
	ExclusiveStartKey      map[string]*dynamodb.AttributeValue
}

type ConditionalExpression struct {
	Expected map[string]*dynamodb.ExpectedAttributeValue
}

func NewClient(config *DynamoClientConfig) *DynamoClient {
	return NewClientWithAudit(config, nil, nil)
}

func NewClientWithAudit(config *DynamoClientConfig, auditSetup *audit.AuditSetup, auditConfig *DynamoAuditConfig) *DynamoClient {
	awsConfig := &aws.Config{
		Region:      aws.String(config.AwsRegion),
		Credentials: config.CredentialsOverride,
	}
	sess, _ := session.NewSession()
	dynamo := dynamodb.New(sess, awsConfig)
	if config.EndpointOverride != "" {
		dynamo.Endpoint = config.EndpointOverride
	}
	return &DynamoClient{
		Dynamo:       dynamo,
		ClientConfig: config,
		AuditSetup:   auditSetup,
		AuditConfig:  auditConfig,
	}
}

func NewClientWithCustomRoleArn(config *DynamoClientConfig, roleArn string) *DynamoClient {
	mySession := session.Must(session.NewSession())
	creds := stscreds.NewCredentials(mySession, roleArn)
	dynamo := dynamodb.New(mySession, &aws.Config{
		Region:      aws.String(config.AwsRegion),
		Credentials: creds,
	})
	if config.EndpointOverride != "" {
		dynamo.Endpoint = config.EndpointOverride
	}
	return &DynamoClient{
		Dynamo:       dynamo,
		ClientConfig: config,
	}
}

func (c *DynamoClient) GetFullTableName(table BaseDynamoTable) FullTableName {
	var parts []string

	if c.ClientConfig.TablePrefix != "" {
		parts = append(parts, c.ClientConfig.TablePrefix)
	}

	parts = append(parts, string(table.GetBaseTableName()))

	if c.ClientConfig.TableSuffix != "" {
		parts = append(parts, c.ClientConfig.TableSuffix)
	}

	return FullTableName(strings.Join(parts, "_"))
}

func (c *DynamoClient) PutItem(record DynamoTableRecord) error {
	fullTableName := c.GetFullTableName(record.GetTable())
	record.UpdateWithCurrentTimestamp()
	_, err := c.Dynamo.PutItem(NewPutItemInput(fullTableName, record))
	if err != nil {
		msg := "DynamoClient: Encountered an error calling dynamo PutItem"
		log.WithError(err).Error(msg)
		return errors.Notef(err, msg)
	}
	return nil
}

func (c *DynamoClient) updateItem(record DynamoTableRecord, conditionalExpression *ConditionalExpression) error {
	fullTableName := c.GetFullTableName(record.GetTable())
	record.UpdateWithCurrentTimestamp()

	updateItemInput := NewUpdateItemInput(fullTableName, record)
	if conditionalExpression != nil {
		updateItemInput.Expected = conditionalExpression.Expected
	}

	_, err := c.Dynamo.UpdateItem(updateItemInput)
	return err
}

func (c *DynamoClient) UpdateItem(record DynamoTableRecord) error {
	return c.updateItem(record, nil)
}

func (c *DynamoClient) UpdateItemConditional(record DynamoTableRecord, conditionalExpression *ConditionalExpression) error {
	return c.updateItem(record, conditionalExpression)
}

func (c *DynamoClient) GetItem(record DynamoTableRecord) (DynamoTableRecord, error) {
	return c.getItem(record, nil)
}

func (c *DynamoClient) GetItemWithConsistentRead(record DynamoTableRecord) (DynamoTableRecord, error) {
	return c.getItem(record, pointers.BoolP(true))
}

func (c *DynamoClient) getItem(record DynamoTableRecord, consistentRead *bool) (DynamoTableRecord, error) {
	fullTableName := c.GetFullTableName(record.GetTable())
	result, err := c.Dynamo.GetItem(NewGetItemInput(fullTableName, record, consistentRead))
	if err != nil {
		msg := "DynamoClient: Encountered an error calling dynamo GetItem"
		log.WithError(err).Error(msg)
		return nil, errors.Notef(err, msg)
	}

	// Result map is empty if no record is found
	if len(result.Item) != 0 {
		lastUpdatedTime, err := TimeFromAttributes(result.Item, "lastUpdated")
		if err != nil {
			msg := "DynamoClient: Encountered an error getting lastUpdated time after dynamo GetItem"
			log.WithError(err).Error(msg)
			return nil, errors.Notef(err, msg)
		}

		record, err := record.GetTable().ConvertAttributeMapToRecord(result.Item)
		if err != nil {
			msg := "DynamoClient: Encountered an error converting dynamo GetItem result to record"
			log.WithError(err).Error(msg)
			return nil, errors.Notef(err, msg)
		}

		record.ApplyTimestamp(lastUpdatedTime)
		return record, nil
	} else {
		return nil, nil
	}
}

//This function assumes records will always be from the same dynamo table
func (c *DynamoClient) BatchGetItem(records []DynamoTableRecord) ([]DynamoTableRecord, error) {
	results := make([]DynamoTableRecord, 0)
	for i := 0; i <= len(records)/batchSize; i++ {
		start := i * batchSize
		end := i*batchSize + batchSize
		if end > len(records) {
			end = len(records)
		}

		batch := records[start:end]
		batchResults, err := c.executeBatchGetItem(batch)
		if err != nil {
			return nil, err
		}
		results = append(results, batchResults...)
	}
	return results, nil
}

func (c *DynamoClient) executeBatchGetItem(records []DynamoTableRecord) ([]DynamoTableRecord, error) {
	result := make([]DynamoTableRecord, 0)
	if len(records) == 0 {
		return result, nil
	}

	table := records[0].GetTable()
	fullTableName := c.GetFullTableName(table)

	keys := make([]map[string]*dynamodb.AttributeValue, 0, len(records))
	for _, record := range records {
		keys = append(keys, record.NewItemKey())
	}

	batchItemOutput, err := c.Dynamo.BatchGetItem(NewBatchGetItemInput(fullTableName, keys, pointers.BoolP(false)))
	if err != nil {
		msg := "DynamoClient: Encountered an error calling BatchGetItem"
		log.WithError(err).Error(msg)
		return nil, errors.Notef(err, msg)
	}
	if batchItemOutput == nil {
		msg := "DynamoClient: Encountered unexpected nil batch item output"
		log.WithFields(log.Fields{
			"fullTableName": fullTableName,
			"keys":          keys,
		}).WithError(err).Error(msg)
		return nil, errors.Notef(err, msg)
	}
	if len(batchItemOutput.UnprocessedKeys) > 0 {
		msg := "DynamoClient: Failed to processes keys during batch get"
		log.WithField("UnprocessedKeys", batchItemOutput.UnprocessedKeys).Error(msg)
	}

	for _, attributeMaps := range batchItemOutput.Responses {
		for _, attributeMap := range attributeMaps {
			record, err := table.ConvertAttributeMapToRecord(attributeMap)
			if err != nil {
				msg := "DynamoClient: Encountered an error converting dynamo BatchGetItem result to record"
				log.WithError(err).Error(msg)
				return nil, errors.Notef(err, msg)
			}

			lastUpdatedKey := "lastUpdated"
			// unfortunately, some tables use snake case... =(
			if _, ok := attributeMap["last_updated"]; ok {
				lastUpdatedKey = "last_updated"
			}

			lastUpdatedTime, err := TimeFromAttributes(attributeMap, lastUpdatedKey)
			if err != nil {
				msg := "DynamoClient: Encountered an error getting lastUpdated time after dynamo BatchGetItem"
				log.WithError(err).Error(msg)
				return nil, errors.Notef(err, msg)
			}
			record.ApplyTimestamp(lastUpdatedTime)
			result = append(result, record)
		}
	}
	return result, nil
}

//This function assumes records will always be from the same dynamo table
func (c *DynamoClient) BatchPutItem(records []DynamoTableRecord) error {
	table := records[0].GetTable()
	fullTableName := c.GetFullTableName(table)
	for i := 0; i <= len(records)/batchWriteSize; i++ {
		start := i * batchWriteSize
		end := start + batchWriteSize
		if end > len(records) {
			end = len(records)
		}

		batch := records[start:end]
		if len(batch) == 0 {
			return nil
		}

		requests := make([]*dynamodb.PutRequest, 0)
		for _, record := range batch {
			requests = append(requests, &dynamodb.PutRequest{
				Item: record.NewAttributeMap(),
			})
		}
		batchWriteInput := NewBatchPutItemInput(fullTableName, requests)

		err := c.executeBatchWriteItem(batchWriteInput)
		if err != nil {
			return err
		}
	}
	return nil
}

func (c *DynamoClient) BatchDeleteItem(records []DynamoTableRecord) error {
	table := records[0].GetTable()
	fullTableName := c.GetFullTableName(table)
	for i := 0; i <= len(records)/batchWriteSize; i++ {
		start := i * batchWriteSize
		end := start + batchWriteSize
		if end > len(records) {
			end = len(records)
		}

		batch := records[start:end]
		if len(batch) == 0 {
			return nil
		}

		requests := make([]*dynamodb.DeleteRequest, 0)
		for _, record := range batch {
			requests = append(requests, &dynamodb.DeleteRequest{
				Key: record.NewItemKey(),
			})
		}
		batchWriteInput := NewBatchDeleteItemInput(fullTableName, requests)

		err := c.executeBatchWriteItem(batchWriteInput)
		if err != nil {
			return err
		}
	}
	return nil
}

func (c *DynamoClient) executeBatchWriteItem(batchWriteInput *dynamodb.BatchWriteItemInput) error {
	// AWS doc recommends exponential retry: https://docs.aws.amazon.com/amazondynamodb/latest/APIReference/API_BatchWriteItem.html
	bo := backoff.WithMaxTries(newBackOff(), batchWriteRetryCount)
	backoffFunc := func() error {
		batchWriteOutput, err := c.Dynamo.BatchWriteItem(batchWriteInput)
		if err != nil {
			msg := "DynamoClient: Encountered an error calling BatchWriteItem"
			log.WithError(err).Error(msg)
			return errors.Notef(err, msg)
		}
		if len(batchWriteOutput.UnprocessedItems) != 0 {
			// update input for next retry
			batchWriteInput = &dynamodb.BatchWriteItemInput{
				RequestItems: batchWriteOutput.UnprocessedItems,
			}
			msg := "DynamoClient: BatchWriteItem had Unprocessed Items"
			log.WithField("UnprocessedItems", batchWriteOutput.UnprocessedItems).Error(msg)
			return errors.Notef(err, msg)
		}
		return nil
	}

	return backoff.Retry(backoffFunc, bo)
}

func newBackOff() backoff.BackOff {
	exponetial := backoff.NewExponentialBackOff()
	exponetial.InitialInterval = 10 * time.Millisecond
	exponetial.MaxElapsedTime = 10 * time.Second
	exponetial.Multiplier = 2
	return exponetial
}

func (c *DynamoClient) DeleteItem(record DynamoTableRecord) error {
	fullTableName := c.GetFullTableName(record.GetTable())
	_, err := c.Dynamo.DeleteItem(NewDeleteItemInput(fullTableName, record))
	if err != nil {
		msg := fmt.Sprintf("DynamoClient: Encountered an error calling dynamo DeleteItem, %v", err)
		log.Error(msg)
		return errors.Notef(err, msg)
	}
	return nil
}

func (c *DynamoClient) QueryByHashKey(record DynamoTableRecord) ([]DynamoTableRecord, error) {
	return c.queryByHashKey(record, nil, nil)
}

func (c *DynamoClient) QueryByHashKeyWithLimit(record DynamoTableRecord, limit int64) ([]DynamoTableRecord, error) {
	return c.queryByHashKey(record, pointers.Int64P(limit), nil)
}

func (c *DynamoClient) QueryByHashKeyWithLimitAndConsistentRead(record DynamoTableRecord, limit int64, consistentRead bool) ([]DynamoTableRecord, error) {
	return c.queryByHashKey(record, pointers.Int64P(limit), pointers.BoolP(consistentRead))
}

// Only returns up to 1MB of results
// TODO: Add pagination support
func (c *DynamoClient) queryByHashKey(record DynamoTableRecord, limit *int64, consistentRead *bool) ([]DynamoTableRecord, error) {
	fullTableName := c.GetFullTableName(record.GetTable())
	queryInput := &dynamodb.QueryInput{
		TableName:                 aws.String(string(fullTableName)),
		KeyConditionExpression:    aws.String(record.NewHashKeyEqualsExpression()),
		ExpressionAttributeValues: record.NewHashKeyExpressionAttributeValues(),
	}

	if limit != nil {
		queryInput.Limit = limit
	}

	if consistentRead != nil {
		queryInput.ConsistentRead = consistentRead
	}

	queryOutput, err := c.Dynamo.Query(queryInput)
	if err != nil {
		msg := "Encountered an error calling dynamo Query"
		log.WithError(err).Error(msg)
		return nil, errors.Notef(err, msg)
	}

	results := make([]DynamoTableRecord, 0, len(queryOutput.Items))
	for _, item := range queryOutput.Items {
		lastUpdatedTime, err := TimeFromAttributes(item, "lastUpdated")
		if err != nil {
			msg := fmt.Sprintf("DynamoClient: Encountered an error getting lastUpdated time after dynamo Query, %v", err)
			log.Error(msg)
			return nil, errors.Notef(err, msg)
		}

		record, err := record.GetTable().ConvertAttributeMapToRecord(item)
		if err != nil {
			msg := fmt.Sprintf("DynamoClient: Encountered an error converting dyanmo Query result item to record, %v", err)
			log.Error(msg)
			return nil, errors.Notef(err, msg)
		}

		record.ApplyTimestamp(lastUpdatedTime)

		results = append(results, record)
	}
	return results, nil
}

func (c *DynamoClient) ScanTable(table DynamoScanQueryTable, filter ScanFilter) (records []DynamoScanQueryTableRecord, err error) {
	fullTableName := c.GetFullTableName(table)

	input := &dynamodb.ScanInput{
		TableName:                 aws.String(string(fullTableName)),
		FilterExpression:          filter.Expression,
		ExpressionAttributeNames:  filter.Names,
		ExpressionAttributeValues: filter.Values,
	}

	output, err := c.Dynamo.Scan(input)
	if err != nil {
		msg := fmt.Sprintf("DynamoClient: Encountered an error calling dynamo Scan, %v", err)
		log.Error(msg)
		return nil, errors.Notef(err, msg)
	}

	records, err = table.ConvertScanOutputToRecords(output)
	if err != nil {
		msg := fmt.Sprintf("DynamoClient: Failed to parse Scan output, %v", err)
		log.Error(msg)
		return nil, errors.Notef(err, msg)
	}

	// Continue scanning until we've exhausted the table
	for output.LastEvaluatedKey != nil {
		input.ExclusiveStartKey = output.LastEvaluatedKey
		output, err = c.Dynamo.Scan(input)
		if err != nil {
			msg := fmt.Sprintf("DynamoClient: Encountered an error calling dynamo Scan, %v", err)
			log.Error(msg)
			return nil, errors.Notef(err, msg)
		}
		toAppend, err := table.ConvertScanOutputToRecords(output)
		if err != nil {
			msg := fmt.Sprintf("DynamoClient: Failed to parse Scan output, %v", err)
			log.Error(msg)
			return nil, errors.Notef(err, msg)
		}

		records = append(records, toAppend...)
	}

	return records, nil
}

func (c *DynamoClient) QueryTable(table DynamoScanQueryTable, filter QueryFilter) (records []DynamoScanQueryTableRecord, lastEvaluatedKey map[string]*dynamodb.AttributeValue, err error) {
	fullTableName := c.GetFullTableName(table)

	input := &dynamodb.QueryInput{
		TableName:                 aws.String(string(fullTableName)),
		KeyConditionExpression:    filter.KeyConditionExpression,
		ExpressionAttributeNames:  filter.Names,
		ExpressionAttributeValues: filter.Values,
		Limit:                     filter.Limit,
		IndexName:                 filter.IndexName,
		ExclusiveStartKey:         filter.ExclusiveStartKey,
	}

	if filter.Descending {
		input.ScanIndexForward = aws.Bool(false)
	}

	output, err := c.Dynamo.Query(input)
	if err != nil {
		msg := fmt.Sprintf("DynamoClient: Encountered an error calling dynamo Query, %v", err)
		log.Error(msg)
		return nil, nil, errors.Notef(err, msg)
	}

	records, err = table.ConvertQueryOutputToRecords(output)
	if err != nil {
		msg := fmt.Sprintf("DynamoClient: Failed to parse Query output, %v", err)
		log.Error(msg)
		return nil, nil, errors.Notef(err, msg)
	}

	// Continue querying until we've exhausted the table or hit the limit
	for output.LastEvaluatedKey != nil && (filter.Limit == nil || int64(len(records)) < *filter.Limit) {
		input.ExclusiveStartKey = output.LastEvaluatedKey
		output, err = c.Dynamo.Query(input)
		if err != nil {
			msg := fmt.Sprintf("DynamoClient: Encountered an error calling dynamo Query, %v", err)
			log.Error(msg)
			return nil, nil, errors.Notef(err, msg)
		}
		toAppend, err := table.ConvertQueryOutputToRecords(output)
		if err != nil {
			msg := fmt.Sprintf("DynamoClient: Failed to parse Query output, %v", err)
			log.Error(msg)
			return nil, nil, errors.Notef(err, msg)
		}

		records = append(records, toAppend...)
	}

	return records, output.LastEvaluatedKey, nil
}

func (c *DynamoClient) QueryTableWithContext(ctx context.Context, table DynamoScanQueryTable, filter QueryFilter) (records []DynamoScanQueryTableRecord, lastEvaluatedKey map[string]*dynamodb.AttributeValue, err error) {
	fullTableName := c.GetFullTableName(table)

	input := &dynamodb.QueryInput{
		TableName:                 aws.String(string(fullTableName)),
		KeyConditionExpression:    filter.KeyConditionExpression,
		ExpressionAttributeNames:  filter.Names,
		ExpressionAttributeValues: filter.Values,
		Limit:                     filter.Limit,
		IndexName:                 filter.IndexName,
		ExclusiveStartKey:         filter.ExclusiveStartKey,
	}

	if filter.Descending {
		input.ScanIndexForward = aws.Bool(false)
	}

	output, err := c.Dynamo.QueryWithContext(ctx, input)
	if err != nil {
		return nil, nil, errors.Notef(err, fmt.Sprintf("DynamoClient: Encountered an error calling dynamo Query, %v", err))
	}

	records, err = table.ConvertQueryOutputToRecords(output)
	if err != nil {
		return nil, nil, errors.Notef(err, fmt.Sprintf("DynamoClient: Failed to parse Query output, %v", err))
	}

	// Continue querying until we've exhausted the table or hit the limit
	for output.LastEvaluatedKey != nil && (filter.Limit == nil || int64(len(records)) < *filter.Limit) {
		input.ExclusiveStartKey = output.LastEvaluatedKey
		output, err = c.Dynamo.QueryWithContext(ctx, input)
		if err != nil {
			return nil, nil, errors.Notef(err, fmt.Sprintf("DynamoClient: Encountered an error calling dynamo Query, %v", err))
		}
		toAppend, err := table.ConvertQueryOutputToRecords(output)
		if err != nil {
			return nil, nil, errors.Notef(err, fmt.Sprintf("DynamoClient: Failed to parse Query output, %v", err))
		}

		records = append(records, toAppend...)
	}

	return records, output.LastEvaluatedKey, nil
}

func (c *DynamoClient) describeTable(table DynamoTable) (*dynamodb.DescribeTableOutput, error) {
	fullTableName := c.GetFullTableName(table)
	dti := &dynamodb.DescribeTableInput{
		TableName: aws.String(string(fullTableName)),
	}

	dto, err := c.Dynamo.DescribeTable(dti)
	if err != nil {
		msg := fmt.Sprintf("DynamoClient: Encountered an error calling dynamo DescribeTable for table: %s, %v", fullTableName, err)
		log.Error(msg)
		return nil, errors.Notef(err, msg)
	}

	return dto, nil
}

func (c *DynamoClient) GetTableStatus(table DynamoTable) (string, error) {
	dto, err := c.describeTable(table)
	if err != nil {
		msg := fmt.Sprintf("DynamoClient: Encountered an error getting table status for table: %s, %v", c.GetFullTableName(table), err)
		log.Error(msg)
		return "", errors.Notef(err, msg)
	}
	return *dto.Table.TableStatus, nil
}

func (c *DynamoClient) TableExists(targetTable DynamoTable) (bool, error) {
	fullTableName := c.GetFullTableName(targetTable)
	var lastTableName *string
	for {
		params := &dynamodb.ListTablesInput{}
		if lastTableName != nil {
			params.ExclusiveStartTableName = lastTableName
		}
		result, err := c.Dynamo.ListTables(params)
		if err != nil {
			msg := fmt.Sprintf("DynamoClient: Encountered an error calling dynamo ListTables %v", err)
			log.Error(msg)
			return false, errors.Notef(err, msg)
		}
		for _, tableName := range result.TableNames {
			if *tableName == string(fullTableName) {
				return true, nil
			}
		}
		if result.LastEvaluatedTableName == nil {
			return false, nil
		}
		lastTableName = result.LastEvaluatedTableName
	}
}

func (c *DynamoClient) DeleteTable(table DynamoTable) error {
	err := c.WaitForTableToBeActive(table)
	if err != nil {
		log.Error(err)
		return err
	}
	fullTableName := c.GetFullTableName(table)
	log.Infof("Deleting table %s", fullTableName)
	params := &dynamodb.DeleteTableInput{
		TableName: aws.String(string(fullTableName)),
	}

	if strings.Contains(string(fullTableName), "prod") {
		msg := "Please don't delete production tables!"
		log.Error(msg)
		return errors.New(msg)
	}

	_, err = c.Dynamo.DeleteTable(params)
	if err != nil {
		msg := fmt.Sprintf("DynamoClient: Encountered an error calling dynamo DeleteTable for %s, %v", fullTableName, err)
		log.Error(msg)
		return errors.Notef(err, msg)
	}
	return nil
}

func (c *DynamoClient) waitForTableExistence(targetTable DynamoTable, shouldTableExist bool) error {
	// Creates a go routine that writes to a channel once table existence in expected state
	ch := make(chan error, 1)
	go func() {
		for {
			exists, err := c.TableExists(targetTable)
			if err != nil {
				ch <- err
				return
			}
			if exists == shouldTableExist {
				ch <- nil
				return
			}
		}
	}()

	timeoutMs := defaultWaitTimeoutMs
	if c.ClientConfig.TableExistenceWaitTimeoutMsOverride != 0 {
		timeoutMs = c.ClientConfig.TableExistenceWaitTimeoutMsOverride
	}

	// Selects the first channel to respond. Either table existence check finishes or times out
	select {
	case result := <-ch:
		return result
	case <-time.After(time.Millisecond * timeoutMs):
		fullTableName := c.GetFullTableName(targetTable)
		var msg string
		if shouldTableExist {
			msg = fmt.Sprintf("DynamoClient: Timeout while waiting for table %s to exist", string(fullTableName))
		} else {
			msg = fmt.Sprintf("DynamoClient: Timeout while waiting for table %s to not exist", string(fullTableName))
		}
		log.Error(msg)
		return errors.New(msg)
	}
}

func (c *DynamoClient) WaitForTableToNotExist(targetTable DynamoTable) error {
	return c.waitForTableExistence(targetTable, false)
}

func (c *DynamoClient) WaitForTableToExist(targetTable DynamoTable) error {
	return c.waitForTableExistence(targetTable, true)
}

func (c *DynamoClient) WaitForTableStatus(targetTable DynamoTable, targetTableStatus string) error {
	// Creates a goroutine that writes to a channel once table in expected status
	ch := make(chan error, 1)
	go func() {
		for {
			status, err := c.GetTableStatus(targetTable)
			if err != nil {
				ch <- err
				return
			}
			if status == targetTableStatus {
				ch <- nil
				return
			}
		}
	}()

	timeoutMs := defaultWaitTimeoutMs
	if c.ClientConfig.TableStatusWaitTimeoutMsOverride != 0 {
		timeoutMs = c.ClientConfig.TableStatusWaitTimeoutMsOverride
	}

	// Selects the first channel to respond. Either table status check finishes or times out
	select {
	case result := <-ch:
		return result
	case <-time.After(time.Millisecond * timeoutMs):
		fullTableName := c.GetFullTableName(targetTable)
		msg := fmt.Sprintf("DynamoClient: Timeout while waiting for table %s to move to status %s", string(fullTableName), targetTableStatus)
		log.Error(msg)
		return errors.New(msg)
	}
}

func (c *DynamoClient) WaitForTableToExistInStatus(targetTable DynamoTable, targetTableStatus string) error {
	err := c.WaitForTableToExist(targetTable)
	if err != nil {
		return err
	}

	err = c.WaitForTableStatus(targetTable, targetTableStatus)
	return err
}

func (c *DynamoClient) WaitForTableToBeActive(targetTable DynamoTable) error {
	return c.WaitForTableToExistInStatus(targetTable, dynamoActiveStatus)
}

func (c *DynamoClient) CreateTable(input *dynamodb.CreateTableInput) error {
	_, err := c.Dynamo.CreateTable(input)
	return err
}

func (c *DynamoClient) SetupTable(table DynamoTable) error {
	fullTableName := c.GetFullTableName(table)

	log.Infof("Setting up table %s", fullTableName)

	tableExists, err := c.TableExists(table)
	if err != nil {
		return err
	}

	if !tableExists || c.ClientConfig.OverwriteExistingTablesDuringSetup {
		if tableExists {
			log.Infof("Table %s already exists, deleting it", fullTableName)
			err = c.DeleteTable(table)
			if err != nil {
				return err
			}

			log.Infof("Wating for table %s to finish deleting", fullTableName)
			err = c.WaitForTableToNotExist(table)
			if err != nil {
				return err
			}

			log.Infof("Table %s deleted", fullTableName)
		}

		var readCapacity ReadCapacity = defaultReadCapacity
		if c.ClientConfig.NewTableReadCapacity > 0 {
			readCapacity = c.ClientConfig.NewTableReadCapacity
		}
		var writeCapacity WriteCapacity = defaultWriteCapacity
		if c.ClientConfig.NewTableWriteCapacity > 0 {
			writeCapacity = c.ClientConfig.NewTableWriteCapacity
		}

		log.Infof("Creating table %s", fullTableName)
		cti := table.NewCreateTableInput(fullTableName, readCapacity, writeCapacity)
		err = c.CreateTable(cti)
		if err != nil {
			return err
		}

		log.Infof("Wating for table %s to finish creating", fullTableName)
		err = c.WaitForTableToBeActive(table)
		if err != nil {
			return err
		}
	}

	if c.AuditSetup != nil {
		log.Infof("Setting up table auditing for %s", fullTableName)
		err := c.SetupAuditing(table)
		if err != nil {
			return err
		}
	}

	return nil
}

func (c *DynamoClient) SetupAuditing(table DynamoTable) error {
	fullTableName := string(c.GetFullTableName(table))
	recordKeySelector := string(table.GetNodeJsRecordKeySelector())

	latestStreamArn, err := c.GetLatestStreamArn(table)
	if err != nil {
		log.Error("Error getting dynamo stream during audit setup")
		return err
	}

	tableAuditSetup := audit.NewTableAuditSetup(c.AuditSetup, &audit.TableAuditSetupConfig{
		DynamoFullTableName: fullTableName,
		AuditS3Bucket:       c.AuditConfig.RecordsS3Bucket,
		RecordKeySelector:   recordKeySelector,
		LambdaFunctionName:  fullTableName + auditLambdaSuffix,
		LambdaHandler:       auditLambdaHandler,
		LambdaFileName:      auditLambdaFileName,
		LambdaDescription:   fmt.Sprintf("Save audit records for dynamo table %s", fullTableName),
		LambdaMemorySizeMB:  c.AuditConfig.LambdaMemorySizeMB,
		LambdaTimeoutSec:    c.AuditConfig.LambdaTimeoutSec,
		LambdaRoleName:      c.AuditConfig.LambdaRoleName,
		LambdaCodeS3Bucket:  c.AuditConfig.LambdaCodeS3Bucket,
		EventBatchSize:      c.AuditConfig.LambdaEventBatchSize,
		EventSource:         latestStreamArn,
		OverwriteExisting:   c.AuditConfig.OverwriteExisting,
	})
	return tableAuditSetup.Setup()
}

func (c *DynamoClient) GetLatestStreamArn(table DynamoTable) (string, error) {
	dto, err := c.describeTable(table)
	if err != nil {
		msg := fmt.Sprintf("DynamoClient: Encountered an error getting lastest stream ARN for table: %s, %v", c.GetFullTableName(table), err)
		log.Error(msg)
		return "", errors.Notef(err, msg)
	}

	if dto.Table != nil && dto.Table.LatestStreamArn != nil {
		return *dto.Table.LatestStreamArn, nil
	}
	return "", nil
}
