package dynamo_test

import (
	"testing"
	"time"

	"code.justin.tv/commerce/payday/dynamo/holds"

	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/payday/dynamo"
	"github.com/gofrs/uuid"
	"github.com/stretchr/testify/assert"
)

func TestMessageDaoUpdateGetDelete(t *testing.T) {
	if dynamoContext.SkipDynamoTests {
		t.Skip("Skipping Dynamo Tests")
	}
	messagesDao, _ := dynamoContext.CreateDefaultTransactionsDao("test-dao-update-get-delete")
	_ = messagesDao.SetupTable()

	transactionIdUUI, err := uuid.NewV4()
	assert.NoError(t, err)

	transactionId := transactionIdUUI.String()
	msg := "Message 1"
	originalMessage := &dynamo.Transaction{
		TransactionId:         transactionId,
		LastUpdated:           time.Now(),
		RawMessage:            &msg,
		NumberOfBits:          pointers.Int64P(300),
		NumberOfBitsSponsored: pointers.Int64P(30),
		EmoteTotals: map[string]int64{
			"prefixA": int64(100),
			"prefixB": int64(200),
		},
		Recipients: []*holds.BitsUsageAttribution{{ID: "recipient1", RewardAttribution: 1, BitsRevenueAttributionToBroadcaster: 2, TotalWithBroadcasterPrior: 3}},
	}

	err = messagesDao.Update(originalMessage)
	assert.NoError(t, err)

	retrievedMessage, err := messagesDao.Get(transactionId)
	assert.Equal(t, originalMessage.Recipients[0].TotalWithBroadcasterPrior, retrievedMessage.Recipients[0].TotalWithBroadcasterPrior)
	assert.Equal(t, originalMessage.Recipients[0].RewardAttribution, retrievedMessage.Recipients[0].RewardAttribution)
	assert.Equal(t, originalMessage.Recipients[0].BitsRevenueAttributionToBroadcaster, retrievedMessage.Recipients[0].BitsRevenueAttributionToBroadcaster)

	assert.NoError(t, err)
	assert.True(t, originalMessage.Equals(retrievedMessage))

	err = messagesDao.Delete(transactionId)
	assert.NoError(t, err)

	deletedMessage, err := messagesDao.Get(transactionId)
	assert.NoError(t, err)
	assert.Nil(t, deletedMessage)
}
