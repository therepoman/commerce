package dynamo

import (
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/utils/pointers"
	timeUtils "code.justin.tv/commerce/payday/utils/time"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

type AdminJobsTable struct {
}

type AdminJob struct {
	Creator           string    // HASH
	JobID             string    // RANGE
	StartTime         time.Time // LSI
	Type              string
	Progress          float64
	InputS3Location   string
	OptionsS3Location string
	LastUpdated       time.Time
}

type AdminJobDao struct {
	BaseDao
}

type IAdminJobDao interface {
	Get(creator string, jobID string) (*AdminJob, error)
	GetRecentByCreator(creator string, n int64) ([]*AdminJob, error)
	GetByJobPrefix(creator string, prefix string) ([]*AdminJob, error)
	Update(newAdminJob *AdminJob) error
	Delete(setId string, assetType string) error
	DeleteTable() error
	SetupTable() error
	GetClientConfig() *DynamoClientConfig
	GetLatestStreamArn() (string, error)
}

func NewAdminJobDao(client *DynamoClient) IAdminJobDao {
	dao := &AdminJobDao{}
	dao.client = client
	dao.table = &AdminJobsTable{}
	return dao
}

func (j *AdminJobsTable) GetBaseTableName() BaseTableName {
	return "admin-jobs"
}

func (j *AdminJobsTable) GetNodeJsRecordKeySelector() NodeJsRecordKeySelector {
	return "record.dynamodb.Keys.creator.S + '_' + record.dynamodb.Keys.jobId.S"
}

func (j *AdminJobsTable) GetStartTimeLSI(name FullTableName) string {
	return string(name) + "-startTime-LSI"
}

func (j *AdminJobsTable) NewCreateTableInput(name FullTableName, read ReadCapacity, write WriteCapacity) *dynamodb.CreateTableInput {
	return &dynamodb.CreateTableInput{
		TableName: aws.String(string(name)),
		ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
			ReadCapacityUnits:  aws.Int64(int64(read)),
			WriteCapacityUnits: aws.Int64(int64(write)),
		},
		AttributeDefinitions: []*dynamodb.AttributeDefinition{
			{
				AttributeName: aws.String("creator"),
				AttributeType: aws.String("S"),
			},
			{
				AttributeName: aws.String("jobId"),
				AttributeType: aws.String("S"),
			},
			{
				AttributeName: aws.String("startTime"),
				AttributeType: aws.String("N"),
			},
		},
		KeySchema: []*dynamodb.KeySchemaElement{
			{
				AttributeName: aws.String("creator"),
				KeyType:       aws.String("HASH"),
			},
			{
				AttributeName: aws.String("jobId"),
				KeyType:       aws.String("RANGE"),
			},
		},
		LocalSecondaryIndexes: []*dynamodb.LocalSecondaryIndex{
			{
				IndexName: aws.String(j.GetStartTimeLSI(name)),
				KeySchema: []*dynamodb.KeySchemaElement{
					{
						AttributeName: aws.String("creator"),
						KeyType:       aws.String("HASH"),
					},
					{
						AttributeName: aws.String("startTime"),
						KeyType:       aws.String("RANGE"),
					},
				},
				Projection: &dynamodb.Projection{
					ProjectionType: aws.String("ALL"),
				},
			},
		},
		StreamSpecification: &dynamodb.StreamSpecification{
			StreamEnabled:  aws.Bool(true),
			StreamViewType: aws.String(dynamodb.StreamViewTypeNewAndOldImages),
		},
	}
}

func (j *AdminJobsTable) ConvertAttributeMapToRecord(attributeMap map[string]*dynamodb.AttributeValue) (DynamoTableRecord, error) {
	startTime, err := TimeFromAttributes(attributeMap, "startTime")
	if err != nil {
		return nil, err
	}

	progress, err := Float64FromAttributes(attributeMap, "progress")
	if err != nil {
		return nil, err
	}

	lastUpdated, err := TimeFromAttributes(attributeMap, "lastUpdated")
	if err != nil {
		return nil, err
	}

	return &AdminJob{
		Creator:           StringFromAttributes(attributeMap, "creator"),
		JobID:             StringFromAttributes(attributeMap, "jobId"),
		StartTime:         startTime,
		Type:              StringFromAttributes(attributeMap, "type"),
		Progress:          progress,
		InputS3Location:   StringFromAttributes(attributeMap, "inputS3Location"),
		OptionsS3Location: StringFromAttributes(attributeMap, "optionsS3Location"),
		LastUpdated:       lastUpdated,
	}, nil
}

func (j *AdminJob) GetTable() DynamoTable {
	return &AdminJobsTable{}
}

func (j *AdminJob) GetScanQueryTable() DynamoScanQueryTable {
	return &AdminJobsTable{}
}

func (j *AdminJob) NewAttributeMap() map[string]*dynamodb.AttributeValue {
	itemMap := make(map[string]*dynamodb.AttributeValue)

	PopulateAttributesString(itemMap, "creator", j.Creator)
	PopulateAttributesString(itemMap, "jobId", j.JobID)
	PopulateAttributesTime(itemMap, "startTime", j.StartTime)
	PopulateAttributesString(itemMap, "type", j.Type)
	PopulateAttributesFloat64(itemMap, "progress", j.Progress)
	PopulateAttributesString(itemMap, "inputS3Location", j.InputS3Location)
	PopulateAttributesString(itemMap, "optionsS3Location", j.OptionsS3Location)
	PopulateAttributesTime(itemMap, "lastUpdated", j.LastUpdated)

	return itemMap
}

func (j *AdminJob) NewItemKey() map[string]*dynamodb.AttributeValue {
	keyMap := make(map[string]*dynamodb.AttributeValue)
	keyMap["creator"] = &dynamodb.AttributeValue{S: aws.String(j.Creator)}
	keyMap["jobId"] = &dynamodb.AttributeValue{S: aws.String(j.JobID)}
	return keyMap
}

func (j *AdminJob) NewHashKeyEqualsExpression() string {
	return "creator = :creator"
}

func (j *AdminJob) NewHashKeyExpressionAttributeValues() map[string]*dynamodb.AttributeValue {
	attributeMap := make(map[string]*dynamodb.AttributeValue)
	PopulateAttributesString(attributeMap, ":creator", j.Creator)
	return attributeMap
}

func (j *AdminJob) UpdateWithCurrentTimestamp() {
	j.LastUpdated = time.Now()
}

func (j *AdminJob) GetTimestamp() time.Time {
	return j.LastUpdated
}

func (j *AdminJob) ApplyTimestamp(timestamp time.Time) {
	j.LastUpdated = timestamp
}

func (j *AdminJob) Equals(other DynamoTableRecord) bool {
	otherAdminJob, isAdminJob := other.(*AdminJob)
	if !isAdminJob {
		return false
	}

	return j.Creator == otherAdminJob.Creator &&
		j.JobID == otherAdminJob.JobID &&
		j.StartTime == otherAdminJob.StartTime &&
		j.Type == otherAdminJob.Type &&
		j.Progress == otherAdminJob.Progress &&
		j.InputS3Location == otherAdminJob.InputS3Location &&
		j.OptionsS3Location == otherAdminJob.OptionsS3Location
}

func (j *AdminJob) EqualsWithExpectedLastUpdated(other DynamoTableRecord, lastUpdatedTime time.Time, lastUpdatedDelta time.Duration) bool {
	return j.Equals(other) && timeUtils.IsTimeInForwardDelta(other.GetTimestamp(), lastUpdatedTime, lastUpdatedDelta)
}

func (dao *AdminJobDao) Update(newAdminJob *AdminJob) error {
	return dao.client.UpdateItem(newAdminJob)
}

func (dao *AdminJobDao) GetRecentByCreator(creator string, n int64) ([]*AdminJob, error) {
	var result []*AdminJob

	table := &AdminJobsTable{}
	idxName := table.GetStartTimeLSI(dao.client.GetFullTableName(table))

	getMostRecentFilter := QueryFilter{
		Names:                  map[string]*string{"#creator": aws.String("creator")},
		Values:                 map[string]*dynamodb.AttributeValue{":creator": {S: aws.String(creator)}},
		KeyConditionExpression: aws.String("#creator = :creator"),
		Limit:                  pointers.Int64P(n),
		Descending:             true,
		IndexName:              pointers.StringP(idxName),
	}

	queryResult, _, err := dao.client.QueryTable(table, getMostRecentFilter)
	if err != nil {
		return result, err
	}

	if len(queryResult) == 0 {
		return result, nil
	}

	result = make([]*AdminJob, len(queryResult))
	for i, record := range queryResult {
		adminJob, isAdminJob := record.(*AdminJob)
		if !isAdminJob {
			msg := "Non-adminJob query result for most recent adminJobs by creator"
			log.WithField("creator", creator).Error(msg)
			return result, errors.New(msg)
		}
		result[i] = adminJob
	}

	return result, nil
}

func (dao *AdminJobDao) GetByJobPrefix(creator string, prefix string) ([]*AdminJob, error) {
	var result []*AdminJob

	table := &AdminJobsTable{}

	getIncompleteJobsFilter := QueryFilter{
		Names: map[string]*string{
			"#creator": aws.String("creator"),
			"#jobId":   aws.String("jobId"),
		},
		Values: map[string]*dynamodb.AttributeValue{
			":creator": {S: aws.String(creator)},
			":jobId":   {S: aws.String(prefix)},
		},
		KeyConditionExpression: aws.String("#creator = :creator AND begins_with(#jobId, :jobId)"),
		Descending:             true,
	}

	queryResult, _, err := dao.client.QueryTable(table, getIncompleteJobsFilter)
	if err != nil {
		return result, err
	}

	if len(queryResult) == 0 {
		return result, nil
	}

	result = make([]*AdminJob, len(queryResult))
	for i, record := range queryResult {
		adminJob, isAdminJob := record.(*AdminJob)
		if !isAdminJob {
			msg := "Non-adminJob query result for most recent adminJobs by creator"
			log.WithField("creator", creator).Error(msg)
			return result, errors.New(msg)
		}
		result[i] = adminJob
	}

	return result, nil
}

func (dao *AdminJobDao) Get(creator string, jobID string) (*AdminJob, error) {
	var adminJobResult *AdminJob

	adminJobQuery := &AdminJob{
		Creator: creator,
		JobID:   jobID,
	}

	result, err := dao.client.GetItem(adminJobQuery)
	if err != nil {
		return nil, err
	}

	if result == nil {
		return nil, nil
	}

	adminJobResult, isAdminJob := result.(*AdminJob)
	if !isAdminJob {
		msg := "Encountered an unexpected type while converting dynamo result to adminJob"
		log.Error(msg)
		return nil, errors.New(msg)
	}

	return adminJobResult, nil
}

func (dao *AdminJobDao) Delete(creator string, jobID string) error {
	deletionRecord := &AdminJob{
		Creator: creator,
		JobID:   jobID,
	}
	return dao.client.DeleteItem(deletionRecord)
}

func (j *AdminJobsTable) ConvertScanOutputToRecords(so *dynamodb.ScanOutput) ([]DynamoScanQueryTableRecord, error) {
	records := make([]DynamoScanQueryTableRecord, *so.Count)

	for idx, attributes := range so.Items {
		recordToAdd, err := j.ConvertAttributeMapToRecord(attributes)
		if err != nil {
			return nil, err
		}

		scanQueryRecord, err := recordToScanQueryRecord(recordToAdd)
		if err != nil {
			return nil, err
		}

		records[idx] = scanQueryRecord
	}
	return records, nil
}

func (j *AdminJobsTable) ConvertQueryOutputToRecords(qo *dynamodb.QueryOutput) ([]DynamoScanQueryTableRecord, error) {
	records := make([]DynamoScanQueryTableRecord, *qo.Count)

	for idx, attributes := range qo.Items {
		recordToAdd, err := j.ConvertAttributeMapToRecord(attributes)
		if err != nil {
			return nil, err
		}

		scanQueryRecord, err := recordToScanQueryRecord(recordToAdd)
		if err != nil {
			return nil, err
		}

		records[idx] = scanQueryRecord
	}
	return records, nil
}

func recordToScanQueryRecord(record DynamoTableRecord) (DynamoScanQueryTableRecord, error) {
	scanQueryRecord, isScanQueryRecord := record.(DynamoScanQueryTableRecord)
	if !isScanQueryRecord {
		return nil, errors.New("Record not a scanQueryRecord")
	}
	return scanQueryRecord, nil
}
