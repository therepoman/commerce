package fetcher

import (
	"context"
	"time"

	"code.justin.tv/commerce/payday/badgetiers"
	"code.justin.tv/commerce/payday/badgetiers/models"
	"code.justin.tv/commerce/payday/cache/chatbadges"
	"code.justin.tv/commerce/payday/dynamo"
)

type Fetcher interface {
	Fetch(ctx context.Context, channelID string) (models.BitsChatBadgeTiers, error)
}

func NewFetcher() Fetcher {
	return &fetcher{}
}

type fetcher struct {
	Cache chatbadges.Cache     `inject:""`
	Dao   dynamo.IBadgeTierDao `inject:""`
}

func (f *fetcher) Fetch(ctx context.Context, channelID string) (models.BitsChatBadgeTiers, error) {
	cachedTiers := f.Cache.Get(ctx, channelID)

	if cachedTiers != nil {
		return *cachedTiers, nil
	}

	dynamoBadgeTiers, err := f.Dao.GetAll(channelID, false)
	if err != nil {
		return models.BitsChatBadgeTiers{}, err
	}

	var bitsBadgeTiers []dynamo.BadgeTier
	for _, tier := range dynamoBadgeTiers {
		if tier != nil {
			bitsBadgeTiers = append(bitsBadgeTiers, *tier)
		}
	}

	bitsBadgeTiers = FillMissingTiers(channelID, bitsBadgeTiers)

	chatBitsBadgeTiers := models.BitsChatBadgeTiers{
		BadgeTiers:  bitsBadgeTiers,
		ChannelId:   channelID,
		LastUpdated: time.Now(),
	}

	f.Cache.Set(ctx, channelID, &chatBitsBadgeTiers)

	return chatBitsBadgeTiers, nil
}

func FillMissingTiers(channelId string, tiers []dynamo.BadgeTier) []dynamo.BadgeTier {
	allTiers := make([]dynamo.BadgeTier, 0)

	presentTiers := make(map[int64]bool)
	for _, tier := range tiers {
		presentTiers[tier.Threshold] = true
		allTiers = append(allTiers, tier)
	}

	for tierThreshold, tierConfig := range badgetiers.BitsChatBadgeTiers {
		_, alreadyPresent := presentTiers[tierThreshold]
		if tierConfig.Enabled && !alreadyPresent {
			missingTier := dynamo.BadgeTier{
				ChannelId: channelId,
				Threshold: tierThreshold,
				Enabled:   true,
			}
			allTiers = append(allTiers, missingTier)
		}
	}

	return allTiers
}
