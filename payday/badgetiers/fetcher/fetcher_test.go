package fetcher

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/payday/badgetiers/models"
	"code.justin.tv/commerce/payday/dynamo"
	chatbadges_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/cache/chatbadges"
	dynamo_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestFetcher_Fetch(t *testing.T) {
	Convey("given a user service fetcher", t, func() {
		ctx := context.Background()
		cache := new(chatbadges_mock.Cache)
		dao := new(dynamo_mock.IBadgeTierDao)

		fetcher := &fetcher{
			Cache: cache,
			Dao:   dao,
		}

		userID := "123123123"
		dynamoTiers := []*dynamo.BadgeTier{
			{ChannelId: userID, Enabled: true, Threshold: int64(10)},
		}
		badgeTiers := []dynamo.BadgeTier{
			{ChannelId: userID, Enabled: true, Threshold: int64(10)},
		}
		chatBadges := &models.BitsChatBadgeTiers{
			ChannelId:  userID,
			BadgeTiers: badgeTiers,
		}

		Convey("when we find a record in the cache", func() {
			cache.On("Get", mock.Anything, userID).Return(chatBadges)

			Convey("then we return the cached chatBadges", func() {
				actual, err := fetcher.Fetch(ctx, userID)

				So(err, ShouldBeNil)
				So(actual, ShouldNotBeNil)
			})
		})

		Convey("when we don't find a record in the cache", func() {
			cache.On("Get", mock.Anything, userID).Return(nil)

			Convey("when we error looking up the chatBadges from the dao", func() {
				dao.On("GetAll", userID, mock.Anything).Return(nil, errors.New("WALRUS STRIKE"))

				Convey("then we return an error", func() {
					actual, err := fetcher.Fetch(ctx, userID)

					So(err, ShouldNotBeNil)
					So(actual.ChannelId, ShouldBeEmpty)
					So(actual.BadgeTiers, ShouldBeEmpty)
				})
			})

			Convey("when we find a chatBadges", func() {
				dao.On("GetAll", userID, mock.Anything).Return(dynamoTiers, nil)

				Convey("then we set the chatBadges in the cache and we return the chatBadges", func() {
					cache.On("Set", mock.Anything, userID, mock.Anything).Return()
					actual, err := fetcher.Fetch(ctx, userID)

					So(err, ShouldBeNil)
					So(actual, ShouldNotBeNil)

					cache.AssertCalled(t, "Set", mock.Anything, userID, mock.Anything)
				})
			})
		})
	})
}
