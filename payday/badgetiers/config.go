package badgetiers

type ChatBadgeTier struct {
	Enabled         bool
	BadgeSetVersion string
}

var BitsChatBadgeTiers = map[int64]ChatBadgeTier{
	1:       {Enabled: true, BadgeSetVersion: "1"},
	100:     {Enabled: true, BadgeSetVersion: "100"},
	1000:    {Enabled: true, BadgeSetVersion: "1000"},
	5000:    {Enabled: true, BadgeSetVersion: "5000"},
	10000:   {Enabled: true, BadgeSetVersion: "10000"},
	25000:   {Enabled: true, BadgeSetVersion: "25000"},
	50000:   {Enabled: true, BadgeSetVersion: "50000"},
	75000:   {Enabled: true, BadgeSetVersion: "75000"},
	100000:  {Enabled: true, BadgeSetVersion: "100000"},
	200000:  {Enabled: true, BadgeSetVersion: "200000"},
	300000:  {Enabled: true, BadgeSetVersion: "300000"},
	400000:  {Enabled: true, BadgeSetVersion: "400000"},
	500000:  {Enabled: true, BadgeSetVersion: "500000"},
	600000:  {Enabled: true, BadgeSetVersion: "600000"},
	700000:  {Enabled: true, BadgeSetVersion: "700000"},
	800000:  {Enabled: true, BadgeSetVersion: "800000"},
	900000:  {Enabled: true, BadgeSetVersion: "900000"},
	1000000: {Enabled: true, BadgeSetVersion: "1000000"},
	1250000: {Enabled: true, BadgeSetVersion: "1250000"},
	1500000: {Enabled: true, BadgeSetVersion: "1500000"},
	1750000: {Enabled: true, BadgeSetVersion: "1750000"},
	2000000: {Enabled: true, BadgeSetVersion: "2000000"},
	2500000: {Enabled: true, BadgeSetVersion: "2500000"},
	3000000: {Enabled: true, BadgeSetVersion: "3000000"},
	3500000: {Enabled: true, BadgeSetVersion: "3500000"},
	4000000: {Enabled: true, BadgeSetVersion: "4000000"},
	4500000: {Enabled: true, BadgeSetVersion: "4500000"},
	5000000: {Enabled: true, BadgeSetVersion: "5000000"},
}
