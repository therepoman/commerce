package logician

import (
	"context"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/badgetiers"
	"code.justin.tv/commerce/payday/badgetiers/fetcher"
	"code.justin.tv/commerce/payday/badgetiers/utils"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/errors"
)

type Logician interface {
	GetBestAchievedTierEntitlement(ctx context.Context, channelID string, bitsTotal int64) (dynamo.BadgeTier, error)
	GetEnabledBadgeTiers(tiers []dynamo.BadgeTier, channelId string) []int64
	GetBreachedThresholds(bitsUsed int64, bitsTotal int64) []int64
}

type logician struct {
	Fetcher fetcher.Fetcher `inject:""`
}

func NewChatBadgeLogician() Logician {
	return &logician{}
}

func (l *logician) GetBestAchievedTierEntitlement(ctx context.Context, channelID string, bitsTotal int64) (dynamo.BadgeTier, error) {
	var tiers, err = l.Fetcher.Fetch(ctx, channelID)
	if err != nil {
		msg := "Error getting badge tiers to calculate best achieved tier"
		log.WithError(err).Error(msg)
		return dynamo.BadgeTier{}, errors.Notef(err, msg)
	}

	bestTier := getBestAchievedTier(tiers.BadgeTiers, bitsTotal)
	return bestTier, nil
}

//Takes an array of dynamo tiers, fills in the missing ones, and returns the filtered list of enabled tiers' thresholds
func (l *logician) GetEnabledBadgeTiers(tiers []dynamo.BadgeTier, channelID string) []int64 {
	tiers = fetcher.FillMissingTiers(channelID, tiers)

	enabledTiers := make([]int64, 0)

	for _, tier := range tiers {
		if tier.Enabled {
			enabledTiers = append(enabledTiers, tier.Threshold)
		}
	}

	utils.SortInt64s(enabledTiers)

	return enabledTiers
}

//Returns all thresholds (regardless of enabled status) that were between the previous and current total of bits used
func (l *logician) GetBreachedThresholds(bitsUsed int64, bitsTotal int64) []int64 {
	var breachedThresholds []int64
	prevTotal := bitsTotal - bitsUsed
	for threshold := range badgetiers.BitsChatBadgeTiers {
		if threshold > prevTotal && threshold <= bitsTotal {
			breachedThresholds = append(breachedThresholds, threshold)
		}
	}
	return breachedThresholds
}

func getBestAchievedTier(allTiers []dynamo.BadgeTier, bitsTotal int64) dynamo.BadgeTier {
	var bestTier dynamo.BadgeTier

	for _, tier := range allTiers {
		if tierConditionsMet(tier, bitsTotal) && isTierBetter(tier, bestTier) {
			bestTier = tier
		}
	}
	return bestTier
}

func tierConditionsMet(tier dynamo.BadgeTier, bitsTotal int64) bool {
	return tier.Enabled && tier.Threshold <= bitsTotal
}

func isTierBetter(candidateTier dynamo.BadgeTier, baseTier dynamo.BadgeTier) bool {
	return candidateTier.Threshold > baseTier.Threshold
}
