package logician

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/mock"

	"code.justin.tv/commerce/payday/badgetiers"
	"code.justin.tv/commerce/payday/badgetiers/models"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/errors"
	chatbadges_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/badgetiers/fetcher"
	"github.com/stretchr/testify/assert"
)

func getAllTiers() []int64 {
	allTiers := make([]int64, 0)
	for tier := range badgetiers.BitsChatBadgeTiers {
		allTiers = append(allTiers, tier)
	}
	return allTiers
}

func buildExpectedTier(channelId string, threshold int64) dynamo.BadgeTier {
	return dynamo.BadgeTier{
		ChannelId: channelId,
		Threshold: threshold,
		Enabled:   true,
	}
}

func TestGetNeededEntitlement_FetcherError(t *testing.T) {
	testChannelId := "123"
	mockedFetcher := new(chatbadges_mock.Fetcher)
	logician := logician{
		Fetcher: mockedFetcher,
	}

	mockedFetcher.On("Fetch", mock.Anything, testChannelId).Return(models.BitsChatBadgeTiers{}, errors.New("test error"))
	result, err := logician.GetBestAchievedTierEntitlement(context.Background(), testChannelId, 1)
	assert.Error(t, err)
	assert.Empty(t, result)
}

func TestGetNeededEntitlement_FetcherEmptyResult(t *testing.T) {
	testChannelId := "123"
	mockedFetcher := new(chatbadges_mock.Fetcher)
	logician := logician{
		Fetcher: mockedFetcher,
	}

	mockedFetcher.On("Fetch", mock.Anything, testChannelId).Return(models.BitsChatBadgeTiers{}, nil)
	result, err := logician.GetBestAchievedTierEntitlement(context.Background(), testChannelId, 1)
	assert.NoError(t, err)
	assert.Equal(t, dynamo.BadgeTier{}, result)
}

func TestGetNeededEntitlement_AllDisabledTiers(t *testing.T) {
	testChannelId := "123"
	mockedFetcher := new(chatbadges_mock.Fetcher)
	logician := logician{
		Fetcher: mockedFetcher,
	}

	mockedFetcher.On("Fetch", mock.Anything, testChannelId).Return(mockFetcherResponse(testChannelId, []int64{}, []int64{1, 10, 100, 1000, 5000, 10000, 100000}), nil)
	result, err := logician.GetBestAchievedTierEntitlement(context.Background(), testChannelId, 1)
	assert.NoError(t, err)
	assert.Empty(t, result)
}

func TestGetNeededEntitlement_NoEnabledTiersMet(t *testing.T) {
	testChannelId := "123"
	mockedFetcher := new(chatbadges_mock.Fetcher)
	logician := logician{
		Fetcher: mockedFetcher,
	}

	mockedFetcher.On("Fetch", mock.Anything, testChannelId).Return(mockFetcherResponse(testChannelId, []int64{10, 100}, []int64{1, 1000, 10000}), nil)
	result, err := logician.GetBestAchievedTierEntitlement(context.Background(), testChannelId, 9)
	assert.NoError(t, err)
	assert.Empty(t, result)
}

func TestGetNeededEntitlement_SmallestTierExactlyMet(t *testing.T) {
	testChannelId := "123"
	mockedFetcher := new(chatbadges_mock.Fetcher)
	logician := logician{
		Fetcher: mockedFetcher,
	}

	mockedFetcher.On("Fetch", mock.Anything, testChannelId).Return(mockFetcherResponse(testChannelId, []int64{10, 100}, []int64{1, 1000, 10000}), nil)
	result, err := logician.GetBestAchievedTierEntitlement(context.Background(), testChannelId, 10)

	assert.NoError(t, err)
	assert.Equal(t, buildExpectedTier(testChannelId, 10), result)
}

func TestGetNeededEntitlement_MiddleTierMet(t *testing.T) {
	testChannelId := "123"
	mockedFetcher := new(chatbadges_mock.Fetcher)
	logician := logician{
		Fetcher: mockedFetcher,
	}

	mockedFetcher.On("Fetch", mock.Anything, testChannelId).Return(mockFetcherResponse(testChannelId, []int64{10, 27, 93, 997}, []int64{1, 1000, 10000}), nil)
	result, err := logician.GetBestAchievedTierEntitlement(context.Background(), testChannelId, 92)

	assert.NoError(t, err)
	assert.Equal(t, buildExpectedTier(testChannelId, 27), result)
}

func TestGetNeededEntitlement_LargestTierExactlyMet(t *testing.T) {
	testChannelId := "123"
	mockedFetcher := new(chatbadges_mock.Fetcher)
	logician := logician{
		Fetcher: mockedFetcher,
	}

	mockedFetcher.On("Fetch", mock.Anything, testChannelId).Return(mockFetcherResponse(testChannelId, []int64{10, 27, 93, 997}, []int64{1, 1000, 10000}), nil)
	result, err := logician.GetBestAchievedTierEntitlement(context.Background(), testChannelId, 997)

	assert.NoError(t, err)
	assert.Equal(t, buildExpectedTier(testChannelId, 997), result)
}

func TestGetNeededEntitlement_LargestTierExceeded(t *testing.T) {
	testChannelId := "123"
	mockedFetcher := new(chatbadges_mock.Fetcher)
	logician := logician{
		Fetcher: mockedFetcher,
	}

	mockedFetcher.On("Fetch", mock.Anything, testChannelId).Return(mockFetcherResponse(testChannelId, []int64{10, 27, 93, 997}, getAllTiers()), nil)
	result, err := logician.GetBestAchievedTierEntitlement(context.Background(), testChannelId, 9999999999999999)

	assert.NoError(t, err)
	assert.Equal(t, buildExpectedTier(testChannelId, 997), result)
}

func TestDefaultEntitlementTiersAllWork(t *testing.T) {
	testChannelId := "123"

	for defaultTierThreshold, defaultTier := range badgetiers.BitsChatBadgeTiers {
		mockedFetcher := new(chatbadges_mock.Fetcher)
		logician := logician{
			Fetcher: mockedFetcher,
		}

		if defaultTier.Enabled {
			mockedFetcher.On("Fetch", mock.Anything, testChannelId).Return(mockFetcherResponse(testChannelId, []int64{defaultTierThreshold}, []int64{}), nil)
			result, err := logician.GetBestAchievedTierEntitlement(context.Background(), testChannelId, defaultTierThreshold)
			assert.NoError(t, err)
			assert.Equal(t, buildExpectedTier(testChannelId, defaultTierThreshold), result)
		}
	}
}

func mockFetcherResponse(channelId string, enabledThresholds []int64, disabledThresholds []int64) models.BitsChatBadgeTiers {
	tiers := make([]dynamo.BadgeTier, 0, len(enabledThresholds)+len(disabledThresholds))

	for _, threshold := range enabledThresholds {
		thisEnabledTier := dynamo.BadgeTier{
			ChannelId: channelId,
			Threshold: threshold,
			Enabled:   true,
		}
		tiers = append(tiers, thisEnabledTier)
	}

	for _, threshold := range disabledThresholds {
		thisDisabledTier := dynamo.BadgeTier{
			ChannelId: channelId,
			Threshold: threshold,
			Enabled:   false,
		}
		tiers = append(tiers, thisDisabledTier)
	}

	return models.BitsChatBadgeTiers{
		ChannelId:   channelId,
		BadgeTiers:  tiers,
		LastUpdated: time.Now(),
	}
}
