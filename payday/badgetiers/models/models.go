package models

import (
	"time"

	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/models/api"
)

type BadgeTierNotificationStatus string

const (
	Show    BadgeTierNotificationStatus = "SHOW"
	Skipped BadgeTierNotificationStatus = "SKIPPED"
	Shared  BadgeTierNotificationStatus = "SHARED"

	ShareableThreshold int64 = 1000
)

type BitsChatBadgeTiers struct {
	ChannelId   string
	BadgeTiers  []dynamo.BadgeTier
	LastUpdated time.Time
}

type BadgeTierNotification struct {
	Threshold         int64
	NotificationState BadgeTierNotificationStatus
	NotificationId    string
	CanShare          bool
	EmoteIDs          []string
}

type BadgeTierNotificationCacheGetResp struct {
	BadgeTierNotification *BadgeTierNotification
}

type EmoticonValidationPayload struct {
	ChannelID                         string
	Threshold                         int64
	EmoticonSetting                   api.EmoticonSetting
	AllowedTiersMap                   map[int64]bool
	CachedNumberOfUploadedEmoticons   *int64
	NumberOfTierEmotesAlreadyUploaded int64
}
