package tiernotifications

import (
	"context"
	"encoding/json"
	"errors"
	"testing"

	"github.com/stretchr/testify/mock"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/badgetiers/models"
	redis_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/redis"
	"github.com/go-redis/redis"
	. "github.com/smartystreets/goconvey/convey"
)

func TestCache_Get(t *testing.T) {
	Convey("Given a badge tier notification cache", t, func() {
		ctx := context.Background()
		redisClient := new(redis_mock.Client)

		cache := &cache{
			RedisClient: redisClient,
		}

		userID := "abc"
		channelID := "123"
		notificationID := "some-fake-id"
		cacheKey := MakeCacheKey(userID, channelID)

		badgeTierNotification := models.BadgeTierNotification{
			Threshold:         int64(100),
			NotificationState: models.Show,
			NotificationId:    notificationID,
		}

		notificationBytes, err := json.Marshal(badgeTierNotification)
		if err != nil {
			log.WithError(err).Error("Error marshaling badge tier notification")
			t.Fail()
		}

		Convey("when Cache returns an error", func() {
			getResp := redis.NewStringResult("", errors.New("ERROR"))
			redisClient.On("Get", mock.Anything, cacheKey).Return(getResp)

			Convey("we should return nil", func() {
				actual := cache.Get(ctx, userID, channelID)
				So(actual, ShouldBeNil)
			})
		})

		Convey("when the record does not exist in the cache", func() {
			getResp := redis.NewStringResult("", redis.Nil)
			redisClient.On("Get", mock.Anything, cacheKey).Return(getResp)

			Convey("we should return nil", func() {
				actual := cache.Get(ctx, userID, channelID)
				So(actual, ShouldBeNil)
			})
		})

		Convey("when a blank record exists in the cache", func() {
			getResp := redis.NewStringResult("", nil)
			redisClient.On("Get", mock.Anything, cacheKey).Return(getResp)

			Convey("we should return the BadgeTierNotificationCacheGetResp struct with a nil badge tier notification", func() {
				actual := cache.Get(ctx, userID, channelID)
				So(actual, ShouldNotBeNil)
				So(actual.BadgeTierNotification, ShouldBeNil)
			})
		})

		Convey("when a record exists in the cache", func() {
			getResp := redis.NewStringResult(string(notificationBytes), nil)
			redisClient.On("Get", mock.Anything, cacheKey).Return(getResp)

			Convey("we should return the BadgeTierNotificationCacheGetResp struct with a nil badge tier notification", func() {
				actual := cache.Get(ctx, userID, channelID)
				So(actual, ShouldNotBeNil)
				So(*actual.BadgeTierNotification, ShouldResemble, badgeTierNotification)
			})
		})
	})
}

func TestCache_SetEmpty(t *testing.T) {
	ctx := context.Background()
	redisClient := new(redis_mock.Client)

	cache := &cache{
		RedisClient: redisClient,
	}

	userID := "abc"
	channelID := "123"
	cacheKey := MakeCacheKey(userID, channelID)

	redisClient.On("Set", mock.Anything, cacheKey, "", badgeTierNotificationCacheTTL).Return(redis.NewStatusCmd(nil))

	Convey("we should call Cache.Set with an empty string", t, func() {
		cache.SetEmpty(ctx, userID, channelID)
		redisClient.AssertCalled(t, "Set", mock.Anything, cacheKey, "", badgeTierNotificationCacheTTL)
	})
}

func TestCache_Set(t *testing.T) {
	ctx := context.Background()
	redisClient := new(redis_mock.Client)

	cache := &cache{
		RedisClient: redisClient,
	}

	userID := "abc"
	channelID := "123"
	notificationID := "some-fake-id"
	cacheKey := MakeCacheKey(userID, channelID)

	badgeTierNotification := models.BadgeTierNotification{
		Threshold:         int64(100),
		NotificationState: models.Show,
		NotificationId:    notificationID,
	}

	notificationBytes, err := json.Marshal(badgeTierNotification)
	if err != nil {
		log.WithError(err).Error("Error marshaling badge tier notification")
		t.Fail()
	}

	redisClient.On("Set", mock.Anything, cacheKey, string(notificationBytes), badgeTierNotificationCacheTTL).Return(redis.NewStatusCmd(nil))

	Convey("we should call Cache.Set with the notification", t, func() {
		cache.Set(ctx, userID, channelID, badgeTierNotification)
		redisClient.AssertCalled(t, "Set", ctx, cacheKey, string(notificationBytes), badgeTierNotificationCacheTTL)
	})
}

func TestCache_Del(t *testing.T) {
	ctx := context.Background()
	redisClient := new(redis_mock.Client)

	cache := &cache{
		RedisClient: redisClient,
	}

	userID := "abc"
	channelID := "123"
	cacheKey := MakeCacheKey(userID, channelID)

	redisClient.On("Del", mock.Anything, cacheKey).Return(redis.NewIntCmd(nil))

	Convey("we should call Cache.Del", t, func() {
		cache.Del(ctx, userID, channelID)
		redisClient.AssertCalled(t, "Del", ctx, cacheKey)
	})
}
