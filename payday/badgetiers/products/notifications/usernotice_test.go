package tiernotifications

import (
	"errors"
	"testing"

	tiernotifications_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/badgetiers/products/notifications"
	tmi_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/tmi"
	metrics_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/metrics"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestUserNotice_SendMessage(t *testing.T) {
	Convey("Given an badge tier notification user notice", t, func() {
		tmiClient := new(tmi_mock.ITMIClient)
		statter := new(metrics_mock.Statter)
		messageValidator := new(tiernotifications_mock.MessageValidator)

		usernotice := userNotice{
			TmiClient:        tmiClient,
			Statter:          statter,
			MessageValidator: messageValidator,
		}

		statter.On("TimingDuration", mock.Anything, mock.Anything).Return()

		channelID := "19942018"
		userID := "20181994"
		message := "hi"
		var threshold int64 = 100

		Convey("when the parsing of channelID errors", func() {
			tmiClient.On("SendUserNotice", mock.Anything).Return(nil)

			Convey("then it should return an error", func() {
				err := usernotice.SendMessage("notanint", userID, threshold, message)

				tmiClient.AssertNotCalled(t, "SendUserNotice", mock.Anything)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when the parsing of userID errors", func() {
			tmiClient.On("SendUserNotice", mock.Anything).Return(nil)

			Convey("then it should return an error", func() {
				err := usernotice.SendMessage(channelID, "notanint", threshold, message)

				tmiClient.AssertNotCalled(t, "SendUserNotice", mock.Anything)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when the given message fails validation", func() {
			messageValidator.On("ValidateMessage", channelID, userID, message).Return(errors.New("test error"))

			Convey("then it should return an error", func() {
				err := usernotice.SendMessage(channelID, userID, threshold, message)

				tmiClient.AssertNotCalled(t, "SendUserNotice", mock.Anything)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when message validation succeeds", func() {
			messageValidator.On("ValidateMessage", mock.Anything, mock.Anything, mock.Anything).Return(nil)

			Convey("when TMI client succeeds", func() {
				tmiClient.On("SendUserNotice", mock.Anything).Return(nil)

				Convey("we should return nil", func() {
					err := usernotice.SendMessage(channelID, userID, threshold, message)

					tmiClient.AssertCalled(t, "SendUserNotice", mock.Anything)
					So(err, ShouldBeNil)
				})
			})

			Convey("when TMI client errors", func() {
				tmiClient.On("SendUserNotice", mock.Anything).Return(errors.New("ERROR"))

				Convey("we should return the error", func() {
					err := usernotice.SendMessage(channelID, userID, threshold, message)

					tmiClient.AssertCalled(t, "SendUserNotice", mock.Anything)
					So(err, ShouldNotBeNil)
				})
			})
		})
	})
}
