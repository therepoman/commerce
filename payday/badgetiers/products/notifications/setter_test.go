package tiernotifications

import (
	"context"
	"errors"
	"testing"
	"time"

	"code.justin.tv/common/twirp"

	"code.justin.tv/commerce/payday/badgetiers/models"
	"code.justin.tv/commerce/payday/dynamo/badge_tier_notification"
	badgetieremotes_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/badgetiers/products/emotes"
	tiernotifications_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/badgetiers/products/notifications"
	chatbadges_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/chatbadges"
	badge_tier_notification_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo/badge_tier_notification"
	paydayModels "code.justin.tv/commerce/payday/models"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestSetter_SetNotificationState(t *testing.T) {
	Convey("given a badge tier notification state setter", t, func() {
		ctx := context.Background()
		dao := new(badge_tier_notification_mock.BadgeTierNotificationDAO)
		usernotice := new(tiernotifications_mock.UserNotice)
		cache := new(tiernotifications_mock.Cache)
		dataScience := new(tiernotifications_mock.DataScience)
		pubSubber := new(tiernotifications_mock.PubSubber)
		chatBadgeClient := new(chatbadges_mock.IChatBadgesClient)
		logicianMock := new(badgetieremotes_mock.Logician)
		setter := &setter{
			ChatBadgesClient:        chatBadgeClient,
			Dao:                     dao,
			UserNotice:              usernotice,
			Cache:                   cache,
			DataScience:             dataScience,
			PubSubber:               pubSubber,
			BadgeTierEmotesLogician: logicianMock,
		}

		userID := "123123123"
		channelID := "abcdefg"
		notificationID := "some-fake-id"
		notificationState := models.Skipped
		message := ""
		badgeType := "default"
		var threshold int64 = models.ShareableThreshold

		badgeTierNotification := &badge_tier_notification.BadgeTierNotification{
			NotificationState: models.Show,
			NotificationID:    notificationID,
			ChannelID:         channelID,
			UserID:            userID,
			Threshold:         threshold,
		}

		dataScienceEvent := paydayModels.BitsBadgeTierNotification{
			TransactionId:  notificationID,
			UserId:         userID,
			ChannelID:      channelID,
			Action:         string(notificationState),
			BadgeAmount:    threshold,
			ContextDetails: message,
			Type:           badgeType,
		}

		chatBadgeClient.On("GetBitsImages", mock.Anything, mock.Anything, mock.Anything).Return(nil, nil)
		logicianMock.On("GetEmoteIDsForBadgeTier", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil, nil)

		Convey("when the dao returns an error calling GetNotification", func() {
			dao.On("GetNotification", notificationID).Return(nil, errors.New("ERROR"))

			Convey("we should return the error", func() {
				err := setter.SetNotificationState(ctx, notificationID, notificationState, message, userID)

				So(err, ShouldNotBeNil)
			})
		})

		Convey("when the dao returns nil calling GetNotification", func() {
			dao.On("GetNotification", notificationID).Return(nil, nil)

			Convey("we should return the error", func() {
				err := setter.SetNotificationState(ctx, notificationID, notificationState, message, userID)

				So(err, ShouldNotBeNil)
			})
		})

		Convey("when the notification is in the share state", func() {
			badgeTierNotification := &badge_tier_notification.BadgeTierNotification{
				NotificationState: models.Shared,
				NotificationID:    notificationID,
				UserID:            userID,
			}

			dao.On("GetNotification", notificationID).Return(badgeTierNotification, nil)

			Convey("we should return an error", func() {
				err := setter.SetNotificationState(ctx, notificationID, notificationState, message, userID)

				So(err, ShouldNotBeNil)
			})
		})

		Convey("when the notification is in the skipped state", func() {
			badgeTierNotification := &badge_tier_notification.BadgeTierNotification{
				NotificationState: models.Skipped,
				NotificationID:    notificationID,
				UserID:            userID,
				Threshold:         models.ShareableThreshold + 1,
			}

			dao.On("GetNotification", notificationID).Return(badgeTierNotification, nil)
			cache.On("Del", mock.Anything, mock.Anything, mock.Anything)
			dataScience.On("TrackBadgeTierNotification", mock.Anything).Return(nil)
			pubSubber.On("SendPublicNotificationShareMessage", mock.Anything, mock.Anything).Return(nil)
			usernotice.On("SendMessage", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)
			dao.On("UpdateItemNotificationState", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)

			Convey("we should not error when updating to the shared state", func() {
				err := setter.SetNotificationState(ctx, notificationID, models.Shared, message, userID)

				So(err, ShouldBeNil)
			})
		})

		Convey("when the userID associated with the notification does not match the requesting user", func() {
			badgeTierNotification := &badge_tier_notification.BadgeTierNotification{
				NotificationState: models.Skipped,
				NotificationID:    notificationID,
				UserID:            "some-other-user",
			}

			dao.On("GetNotification", notificationID).Return(badgeTierNotification, nil)

			Convey("we should return the error", func() {
				err := setter.SetNotificationState(ctx, notificationID, notificationState, message, userID)

				So(err, ShouldNotBeNil)
			})
		})

		Convey("when the user chooses to share the notification", func() {
			dataScienceEvent := paydayModels.BitsBadgeTierNotification{
				TransactionId:  notificationID,
				UserId:         userID,
				ChannelID:      channelID,
				Action:         string(models.Shared),
				BadgeAmount:    threshold,
				ContextDetails: message,
				Type:           badgeType,
			}

			cache.On("Del", mock.Anything, userID, channelID)
			dataScience.On("TrackBadgeTierNotification", dataScienceEvent).Return(nil)
			pubSubber.On("SendPublicNotificationShareMessage", badgeTierNotification, message).Return(nil)

			Convey("we should return an error when the threshold is below the allowable level", func() {
				dao.On("GetNotification", notificationID).Return(&badge_tier_notification.BadgeTierNotification{
					NotificationState: models.Show,
					NotificationID:    notificationID,
					ChannelID:         channelID,
					UserID:            userID,
					Threshold:         models.ShareableThreshold - 1,
				}, nil)
				dao.On("UpdateItemNotificationState", channelID, userID, threshold, models.Shared).Return(nil)

				err := setter.SetNotificationState(ctx, notificationID, models.Shared, message, userID)

				So(err, ShouldNotBeNil)
			})

			Convey("we should call UserNotice.SendMessage", func() {
				dao.On("GetNotification", notificationID).Return(badgeTierNotification, nil)
				dao.On("UpdateItemNotificationState", channelID, userID, threshold, models.Shared).Return(nil)
				usernotice.On("SendMessage", channelID, userID, threshold, message).Return(nil)

				err := setter.SetNotificationState(ctx, notificationID, models.Shared, message, userID)

				So(err, ShouldBeNil)
				usernotice.AssertCalled(t, "SendMessage", channelID, userID, threshold, message)

				Convey("we should send a public pubsub", func() {
					time.Sleep(time.Second)
					pubSubber.AssertCalled(t, "SendPublicNotificationShareMessage", badgeTierNotification, message)
				})
			})

			Convey("when UserNotice.SendMessage returns a validation error", func() {
				dao.On("GetNotification", notificationID).Return(badgeTierNotification, nil)
				dao.On("UpdateItemNotificationState", channelID, userID, threshold, models.Shared).Return(nil)
				usernotice.On("SendMessage", channelID, userID, threshold, message).Return(&MessageValidationErr{
					msg: "ERROR",
				})

				Convey("we should return a twirp validation error", func() {
					err := setter.SetNotificationState(ctx, notificationID, models.Shared, message, userID)

					So(err, ShouldNotBeNil)
					twerr, isTwirpError := err.(twirp.Error)
					So(isTwirpError, ShouldBeTrue)
					So(twerr.Code(), ShouldEqual, twirp.InvalidArgument)
				})
			})

			Convey("when UserNotice.SendMessage returns any other type of error", func() {
				dao.On("GetNotification", notificationID).Return(badgeTierNotification, nil)
				dao.On("UpdateItemNotificationState", channelID, userID, threshold, models.Shared).Return(nil)
				usernotice.On("SendMessage", channelID, userID, threshold, message).Return(errors.New("ERROR"))

				Convey("we should return an internal error", func() {
					err := setter.SetNotificationState(ctx, notificationID, models.Shared, message, userID)

					So(err, ShouldNotBeNil)
					twerr, isTwirpError := err.(twirp.Error)
					So(isTwirpError, ShouldBeTrue)
					So(twerr.Code(), ShouldEqual, twirp.Internal)
				})
			})
		})

		Convey("when the dao returns an error calling UpdateItemNotificationState", func() {
			dao.On("GetNotification", notificationID).Return(badgeTierNotification, nil)
			dao.On("UpdateItemNotificationState", channelID, userID, threshold, notificationState).Return(errors.New("ERROR"))

			Convey("we should return the error", func() {
				err := setter.SetNotificationState(ctx, notificationID, notificationState, message, userID)

				So(err, ShouldNotBeNil)
			})
		})

		Convey("when the dao does not return an error", func() {
			dao.On("GetNotification", notificationID).Return(badgeTierNotification, nil)
			dao.On("UpdateItemNotificationState", channelID, userID, threshold, notificationState).Return(nil)
			cache.On("Del", mock.Anything, userID, channelID)
			dataScience.On("TrackBadgeTierNotification", dataScienceEvent).Return(nil)

			Convey("we should return nil", func() {
				err := setter.SetNotificationState(ctx, notificationID, notificationState, message, userID)

				So(err, ShouldBeNil)

				Convey("we should delete the notification cache", func() {
					cache.AssertCalled(t, "Del", ctx, userID, channelID)
				})

				Convey("we should publish a data science event", func() {
					time.Sleep(time.Second)
					dataScience.AssertCalled(t, "TrackBadgeTierNotification", dataScienceEvent)
				})
			})
		})
	})
}

func TestSetter_CreateNotification(t *testing.T) {
	Convey("given a badge tier notification creator", t, func() {
		ctx := context.Background()
		dao := new(badge_tier_notification_mock.BadgeTierNotificationDAO)
		pubSubber := new(tiernotifications_mock.PubSubber)
		cache := new(tiernotifications_mock.Cache)
		dataScience := new(tiernotifications_mock.DataScience)
		chatBadgeClient := new(chatbadges_mock.IChatBadgesClient)
		logicianMock := new(badgetieremotes_mock.Logician)

		setter := &setter{
			ChatBadgesClient:        chatBadgeClient,
			Dao:                     dao,
			PubSubber:               pubSubber,
			Cache:                   cache,
			DataScience:             dataScience,
			BadgeTierEmotesLogician: logicianMock,
		}

		userID := "123123123"
		channelID := "abcdefg"
		notificationID := "some-notification-id"
		var threshold int64 = 100
		badgeType := "default"

		badgeTierNotification := &badge_tier_notification.BadgeTierNotification{
			UserID:            userID,
			ChannelID:         channelID,
			Threshold:         threshold,
			NotificationID:    notificationID,
			NotificationState: models.Show,
		}

		dataScienceEvent := paydayModels.BitsBadgeTierNotification{
			TransactionId: notificationID,
			UserId:        userID,
			ChannelID:     channelID,
			Action:        string(models.Show),
			BadgeAmount:   threshold,
			Type:          badgeType,
		}

		chatBadgeClient.On("GetBitsImages", mock.Anything, mock.Anything, mock.Anything).Return(nil, nil)
		logicianMock.On("GetEmoteIDsForBadgeTier", mock.Anything, mock.Anything, mock.Anything).Return(nil, nil)

		Convey("when attempting to get an existing notification errors", func() {
			dao.On("GetNotificationByChannelUserThreshold", channelID, userID, threshold).Return(nil, errors.New("ERROR"))

			Convey("we should return the error", func() {
				err := setter.CreateNotification(ctx, channelID, userID, threshold)

				So(err, ShouldNotBeNil)
			})
		})

		Convey("when attempting to create a duplicate notification", func() {
			dao.On("GetNotificationByChannelUserThreshold", channelID, userID, threshold).Return(badgeTierNotification, nil)

			err := setter.CreateNotification(ctx, channelID, userID, threshold)

			Convey("we should not error", func() {
				So(err, ShouldBeNil)
			})

			Convey("we should not call CreateNotification", func() {
				dao.AssertNotCalled(t, "CreateNotification", mock.Anything, mock.Anything, mock.Anything)
			})
		})

		Convey("when the dao returns an error", func() {
			dao.On("GetNotificationByChannelUserThreshold", channelID, userID, threshold).Return(nil, nil)
			dao.On("CreateNotification", channelID, userID, threshold).Return(nil, errors.New("ERROR"))

			Convey("we should return the error", func() {
				err := setter.CreateNotification(ctx, channelID, userID, threshold)

				So(err, ShouldNotBeNil)
			})
		})

		Convey("when the dao does not return an error", func() {
			dao.On("GetNotificationByChannelUserThreshold", channelID, userID, threshold).Return(nil, nil)
			dao.On("CreateNotification", channelID, userID, threshold).Return(badgeTierNotification, nil)
			cache.On("Del", mock.Anything, userID, channelID).Return(nil)
			dataScience.On("TrackBadgeTierNotification", dataScienceEvent).Return(nil)

			Convey("when PubSubber returns an error, we should return the error", func() {
				pubSubTestErrorMessage := "pubsub ERROR"
				pubSubber.On("SendNotificationCreationMessage", mock.Anything, badgeTierNotification).Return(errors.New(pubSubTestErrorMessage))

				err := setter.CreateNotification(ctx, channelID, userID, threshold)

				So(err, ShouldNotBeNil)
				So(err.Error(), ShouldContainSubstring, pubSubTestErrorMessage)
			})

			Convey("when PubSubber succeeds", func() {
				pubSubber.On("SendNotificationCreationMessage", mock.Anything, badgeTierNotification).Return(nil)

				Convey("we should return nil", func() {
					err := setter.CreateNotification(ctx, channelID, userID, threshold)

					So(err, ShouldBeNil)

					Convey("we should delete the notification cache", func() {
						cache.AssertCalled(t, "Del", mock.Anything, userID, channelID)
					})

					Convey("we should publish a data science event", func() {
						time.Sleep(time.Second)

						dataScience.AssertCalled(t, "TrackBadgeTierNotification", dataScienceEvent)
					})
				})
			})
		})
	})
}
