package tiernotifications

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"code.justin.tv/chat/pubsub-go-pubclient/client"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/dynamo/badge_tier_notification"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/userservice"
	"github.com/gofrs/uuid"
)

type UserNameRequestResult struct {
	UserName string
	Error    error
}

type PubSubber interface {
	SendNotificationCreationMessage(ctx context.Context, badgeTierNotification *badge_tier_notification.BadgeTierNotification) error
	SendPublicNotificationShareMessageWithContext(ctx context.Context, badgeTierNotification *badge_tier_notification.BadgeTierNotification, message string)
	SendPublicNotificationShareMessage(badgeTierNotification *badge_tier_notification.BadgeTierNotification, message string)
}

type pubSubber struct {
	PubClient          client.PubClient    `inject:""`
	UserServiceFetcher userservice.Fetcher `inject:""`
}

func NewPubSubber() PubSubber {
	return &pubSubber{}
}

func (p *pubSubber) SendNotificationCreationMessage(ctx context.Context, badgeTierNotification *badge_tier_notification.BadgeTierNotification) error {
	badgeUpdate := models.BadgeUpdateNotification{
		UserID:         badgeTierNotification.UserID,
		ChannelID:      badgeTierNotification.ChannelID,
		Threshold:      badgeTierNotification.Threshold,
		NotificationID: badgeTierNotification.NotificationID,
		CanShare:       canShareNotification(badgeTierNotification.Threshold),
	}

	messageIdUUID, err := uuid.NewV4()
	if err != nil {
		return errors.Notef(err, "Failed to generate UUID")
	}

	pubsubMessage := models.PrivateBitsMessage{
		Data:        badgeUpdate,
		MessageId:   messageIdUUID.String(),
		MessageType: models.BitBadgeUpdateNotificationMessageType,
		Version:     models.CurrentPrivateMessageVersion,
	}

	marshaled, err := json.Marshal(pubsubMessage)
	if err != nil {
		return errors.Notef(err, "Could not marshal pubsub message %v", pubsubMessage)
	}

	err = p.PubClient.Publish(ctx, makeNotificationCreationTopic(badgeTierNotification.UserID), string(marshaled), nil)
	if err != nil {
		return errors.Notef(err, "Could not publish message %s to pubsub", string(marshaled))
	}

	return nil
}

func (p *pubSubber) SendPublicNotificationShareMessageWithContext(ctx context.Context, badgeTierNotification *badge_tier_notification.BadgeTierNotification, message string) {
	userNameChan := make(chan UserNameRequestResult)
	channelNameChan := make(chan UserNameRequestResult)

	go p.getUserName(ctx, badgeTierNotification.UserID, userNameChan)
	go p.getUserName(ctx, badgeTierNotification.ChannelID, channelNameChan)

	var userNameResult, channelNameResult UserNameRequestResult
	select {
	case userNameResult = <-userNameChan:
	case <-ctx.Done():
		return
	}
	select {
	case channelNameResult = <-channelNameChan:
	case <-ctx.Done():
		return
	}

	if userNameResult.Error != nil {
		log.WithError(userNameResult.Error).
			WithField("userID", badgeTierNotification.UserID).
			Error("Failed to get user from user service")
		return
	}

	if channelNameResult.Error != nil {
		log.WithError(channelNameResult.Error).
			WithField("channelID", badgeTierNotification.ChannelID).
			Error("Failed to get channel from user service")
		return
	}

	pubsubMessage := models.PublicBadgeTierUnlockShare{
		UserID:      badgeTierNotification.UserID,
		UserName:    userNameResult.UserName,
		ChannelID:   badgeTierNotification.ChannelID,
		ChannelName: channelNameResult.UserName,
		Threshold:   badgeTierNotification.Threshold,
		ChatMessage: message,
		Time:        badgeTierNotification.CreatedAt,
	}

	marshaled, err := json.Marshal(pubsubMessage)
	if err != nil {
		log.WithError(err).Error("Could not marshal pubsub message")
		return
	}

	err = p.PubClient.Publish(context.Background(), makePublicNotificationShareTopic(badgeTierNotification.ChannelID), string(marshaled), nil)
	if err != nil {
		log.WithError(err).WithField("message", string(marshaled)).Error("Could not publish message to pubsub")
		return
	}
}

func (p *pubSubber) SendPublicNotificationShareMessage(badgeTierNotification *badge_tier_notification.BadgeTierNotification, message string) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	p.SendPublicNotificationShareMessageWithContext(ctx, badgeTierNotification, message)
}

func (p *pubSubber) getUserName(ctx context.Context, userID string, resultChan chan UserNameRequestResult) {
	username, err := p.UserServiceFetcher.Fetch(ctx, userID)

	if err != nil {
		resultChan <- UserNameRequestResult{
			Error: err,
		}
		return
	}

	if username == nil || username.Login == nil {
		resultChan <- UserNameRequestResult{
			Error: errors.New("Could not find channel in user service lookup for id"),
		}
		return
	}

	resultChan <- UserNameRequestResult{
		UserName: *username.Login,
	}
}

func makePublicNotificationShareTopic(channelID string) []string {
	return []string{fmt.Sprintf("%s.%s", models.BitsPublicBadgeTierUnlockShare, channelID)}
}

func makeNotificationCreationTopic(userID string) []string {
	return []string{fmt.Sprintf("%s.%s", models.UserBitsUpdatesPubsubMessageTopic, userID)}
}
