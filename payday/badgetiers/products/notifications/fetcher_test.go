package tiernotifications

import (
	"context"
	"testing"
	"time"

	"code.justin.tv/commerce/payday/badgetiers/models"
	"code.justin.tv/commerce/payday/dynamo/badge_tier_notification"
	badgetieremotes_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/badgetiers/products/emotes"
	tiernotifications_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/badgetiers/products/notifications"
	badge_tier_notification_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo/badge_tier_notification"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestFetcher_Fetch(t *testing.T) {
	Convey("given a badge tier notification fetcher", t, func() {
		dao := new(badge_tier_notification_mock.BadgeTierNotificationDAO)
		cache := new(tiernotifications_mock.Cache)
		logicianMock := new(badgetieremotes_mock.Logician)

		fetcher := &fetcher{
			Dao:                     dao,
			Cache:                   cache,
			BadgeTierEmotesLogician: logicianMock,
		}

		userID := "123123123"
		channelID := "abcdefg"
		notificationID := "some-fake-id"

		Convey("when the cache does not return nil", func() {
			Convey("when the badge tier notification is nil", func() {
				cache.On("Get", mock.Anything, userID, channelID).Return(&models.BadgeTierNotificationCacheGetResp{
					BadgeTierNotification: nil,
				})

				Convey("we should return nil", func() {
					returnValue, err := fetcher.Fetch(context.Background(), userID, channelID)
					So(err, ShouldBeNil)
					So(returnValue, ShouldBeNil)
				})
			})

			Convey("when the badge tier notification is not nil", func() {
				badgeTierNotification := &models.BadgeTierNotification{}

				cache.On("Get", mock.Anything, userID, channelID).Return(&models.BadgeTierNotificationCacheGetResp{
					BadgeTierNotification: badgeTierNotification,
				})

				Convey("we should return the badge tier notification", func() {
					returnValue, err := fetcher.Fetch(context.Background(), userID, channelID)
					So(err, ShouldBeNil)
					So(*returnValue, ShouldResemble, *badgeTierNotification)
				})
			})
		})

		Convey("when the cache returns nil", func() {
			cache.On("Get", mock.Anything, userID, channelID).Return(nil)

			Convey("when the dao returns an error", func() {
				dao.On("GetLargestNotificationByThreshold", channelID, userID).Return(nil, errors.New("ERROR"))

				Convey("we should return the error", func() {
					_, err := fetcher.Fetch(context.Background(), userID, channelID)

					So(err, ShouldNotBeNil)
				})
			})

			Convey("when the dao returns nil", func() {
				dao.On("GetLargestNotificationByThreshold", channelID, userID).Return(nil, nil)
				cache.On("SetEmpty", mock.Anything, userID, channelID).Return(nil)

				Convey("we should return nil", func() {
					returnValue, err := fetcher.Fetch(context.Background(), userID, channelID)

					So(err, ShouldBeNil)
					So(returnValue, ShouldBeNil)

					Convey("we should set an empty cache value", func() {
						cache.AssertCalled(t, "SetEmpty", mock.Anything, userID, channelID)
					})
				})
			})

			Convey("when the dao returns a record with a status other than Show", func() {
				badgeTierNotificationRecord := &badge_tier_notification.BadgeTierNotification{
					NotificationState: models.Skipped,
					Threshold:         100,
					NotificationID:    notificationID,
					CreatedAt:         time.Now(),
				}
				dao.On("GetLargestNotificationByThreshold", channelID, userID).Return(badgeTierNotificationRecord, nil)
				cache.On("SetEmpty", mock.Anything, userID, channelID).Return(nil)

				Convey("we should return nil", func() {
					returnValue, err := fetcher.Fetch(context.Background(), userID, channelID)

					So(err, ShouldBeNil)
					So(returnValue, ShouldBeNil)

					Convey("we should set an empty cache value", func() {
						cache.AssertCalled(t, "SetEmpty", mock.Anything, userID, channelID)
					})
				})
			})

			Convey("when the notification has expired", func() {
				badgeTierNotificationRecord := &badge_tier_notification.BadgeTierNotification{
					NotificationState: models.Show,
					Threshold:         100,
					NotificationID:    notificationID,
					CreatedAt:         time.Now().Add((-notificatonExpirationTimeMinutes - 1) * time.Minute), // 1 minute past expiration
				}
				dao.On("GetLargestNotificationByThreshold", channelID, userID).Return(badgeTierNotificationRecord, nil)
				cache.On("SetEmpty", mock.Anything, userID, channelID).Return(nil)

				Convey("we should return nil", func() {
					returnValue, err := fetcher.Fetch(context.Background(), userID, channelID)

					So(err, ShouldBeNil)
					So(returnValue, ShouldBeNil)

					Convey("we should set an empty cache value", func() {
						cache.AssertCalled(t, "SetEmpty", mock.Anything, userID, channelID)
					})
				})
			})

			Convey("when the dao returns a record", func() {
				badgeTierNotificationRecord := &badge_tier_notification.BadgeTierNotification{
					NotificationState: models.Show,
					Threshold:         100,
					NotificationID:    notificationID,
					CreatedAt:         time.Now(),
				}
				badgeTierNotification := models.BadgeTierNotification{
					NotificationState: badgeTierNotificationRecord.NotificationState,
					Threshold:         badgeTierNotificationRecord.Threshold,
					NotificationId:    badgeTierNotificationRecord.NotificationID,
					CanShare:          canShareNotification(100),
				}
				dao.On("GetLargestNotificationByThreshold", channelID, userID).Return(badgeTierNotificationRecord, nil)
				cache.On("Set", mock.Anything, userID, channelID, mock.Anything).Return(nil)

				Convey("when the logician errors", func() {
					logicianMock.On("GetEmoteIDsForBadgeTier", mock.Anything, channelID, badgeTierNotificationRecord.Threshold).Return(nil, errors.New("test error"))

					Convey("we should return an error", func() {
						_, err := fetcher.Fetch(context.Background(), userID, channelID)

						So(err, ShouldNotBeNil)
					})
				})

				Convey("when the logician returns emoteIDs", func() {
					emoteID := "5678"

					logicianMock.On("GetEmoteIDsForBadgeTier", mock.Anything, channelID, badgeTierNotificationRecord.Threshold).Return([]string{emoteID}, nil)

					Convey("we should return a badge tier notification with emote IDs", func() {
						badgeTierNotification.EmoteIDs = []string{emoteID}

						returnValue, err := fetcher.Fetch(context.Background(), userID, channelID)

						So(err, ShouldBeNil)
						So(*returnValue, ShouldResemble, badgeTierNotification)

						Convey("we should set set the cache", func() {
							cache.AssertCalled(t, "Set", mock.Anything, userID, channelID, badgeTierNotification)
						})
					})
				})
			})
		})
	})
}
