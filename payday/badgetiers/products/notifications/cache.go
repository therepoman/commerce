package tiernotifications

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/badgetiers/models"
	"code.justin.tv/commerce/payday/clients/redis"
	"code.justin.tv/commerce/payday/hystrix"
	cmds "code.justin.tv/commerce/payday/hystrix"
	"code.justin.tv/commerce/payday/utils/strings"
	go_redis "github.com/go-redis/redis"
)

const (
	badgeTierNotificationKeyFormat = "badge-tier-notification-%s-%s"
	badgeTierNotificationCacheTTL  = time.Hour * 1
)

type Cache interface {
	Get(ctx context.Context, userID, channelID string) *models.BadgeTierNotificationCacheGetResp
	SetEmpty(ctx context.Context, userID, channelID string)
	Set(ctx context.Context, userID, channelID string, notification models.BadgeTierNotification)
	Del(ctx context.Context, userID, channelID string)
}

type cache struct {
	RedisClient redis.Client `inject:"redisClient"`
}

func NewCache() Cache {
	return &cache{}
}

// Returns three possible values
//  * nil - error or no record exists for the user/channel
//    -> will call through to dynamo downstream
//  * BadgeTierNotificationCacheGetResp with nil badgeTierNotification - no notification exists for user/channel
//    -> does not call through to dynamo downstream
//  * BadgeTierNotificationCacheGetResp with badgeTierNotification - cached notification exists for user/channel
//    -> does not call through to dynamo downstream
func (c *cache) Get(ctx context.Context, userID, channelID string) *models.BadgeTierNotificationCacheGetResp {
	var cacheValue string
	var goRedisNilReturned bool
	err := hystrix.Do(cmds.TierNotificationsCacheGet, func() error {
		var err error
		cacheValue, err = c.RedisClient.
			Get(ctx, MakeCacheKey(userID, channelID)).
			Result()
		if err != nil && err != go_redis.Nil {
			log.WithError(err).Error("Error getting notification out of cache")
			return err
		} else if err == go_redis.Nil {
			goRedisNilReturned = true
		}
		return nil
	}, nil)

	if err != nil && err != go_redis.Nil {
		log.WithError(err).Warn("hystrix error fetching tier notification from cache")
		return nil
	}

	if goRedisNilReturned {
		return nil
	}

	if strings.Blank(cacheValue) {
		return &models.BadgeTierNotificationCacheGetResp{
			BadgeTierNotification: nil,
		}
	}

	var notification models.BadgeTierNotification
	err = json.Unmarshal([]byte(cacheValue), &notification)

	if err != nil {
		c.Del(ctx, userID, channelID)
		log.WithError(err).Error("Error unmarshaling notification")
		return nil
	}

	return &models.BadgeTierNotificationCacheGetResp{
		BadgeTierNotification: &notification,
	}
}

// A value of "" represents a user/channel without a notification.
func (c *cache) SetEmpty(ctx context.Context, userID, channelID string) {
	err := hystrix.Do(cmds.TierNotificationsCacheSet, func() error {
		err := c.RedisClient.
			Set(ctx, MakeCacheKey(userID, channelID), "", badgeTierNotificationCacheTTL).
			Err()
		if err != nil {
			log.WithError(err).Error("Error setting empty notification in cache")
		}
		return nil
	}, nil)

	if err != nil {
		log.WithError(err).Warn("hystrix error setting empty tier notification in cache")
	}
}

func (c *cache) Set(ctx context.Context, userID, channelID string, notification models.BadgeTierNotification) {
	notificationBytes, err := json.Marshal(notification)

	if err != nil {
		log.WithError(err).Error("Error marshaling notification")
		return
	}

	err = hystrix.Do(cmds.TierNotificationsCacheSet, func() error {
		err := c.RedisClient.
			Set(ctx, MakeCacheKey(userID, channelID), string(notificationBytes), badgeTierNotificationCacheTTL).
			Err()

		if err != nil {
			log.WithError(err).Error("Error setting notification in cache")
		}
		return nil
	}, nil)

	if err != nil {
		log.WithError(err).Warn("hystrix error setting tier notification in cache")
	}
}

func (c *cache) Del(ctx context.Context, userID, channelID string) {
	err := hystrix.Do(cmds.TierNotificationsCacheDel, func() error {
		err := c.RedisClient.
			Del(ctx, MakeCacheKey(userID, channelID)).
			Err()

		if err != nil {
			log.WithError(err).Error("Error deleting notification from cache")
		}
		return nil
	}, nil)

	if err != nil {
		log.WithError(err).Warn("hystrix error deleting tier notification from cache")
	}
}

func MakeCacheKey(userID, channelID string) string {
	return fmt.Sprintf(badgeTierNotificationKeyFormat, userID, channelID)
}
