package tiernotifications

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	zuma "code.justin.tv/chat/zuma/app/api"
	log "code.justin.tv/commerce/logrus"
	zumaWrapper "code.justin.tv/commerce/payday/clients/zuma"
	"code.justin.tv/commerce/payday/metrics"
	"code.justin.tv/commerce/payday/utils/strings"
	"code.justin.tv/feeds/feeds-common/entity"
)

type MessageValidator interface {
	ValidateMessage(channelID string, userID string, message string) error
}

type messageValidator struct {
	ZumaClient zumaWrapper.IZumaClientWrapper `inject:""`
	Statter    metrics.Statter                `inject:""`
}

type MessageValidationErr struct {
	msg string
}

func (e *MessageValidationErr) Error() string {
	return e.msg
}

func NewMessageValidator() MessageValidator {
	return &messageValidator{}
}

func (mv *messageValidator) ValidateMessage(channelID string, userID string, message string) error {
	containerOwner := entity.New(entity.NamespaceUser, channelID)

	enforcementRules := zuma.EnforcementRuleset{
		AutoMod:             true,
		Banned:              true,
		Blocked:             true,
		ChannelBlockedTerms: true,
		DisposableEmail:     true,
		EmotesOnly:          true,
		Entropy:             true,
		FollowersOnly:       true,
		Spam:                true,
		SubscribersOnly:     true,
		Suspended:           true,
		VerifiedOnly:        true,
		Zalgo:               true,
	}

	extractMessageReq := zuma.EnforceMessageRequest{
		SkipExtraction:   true,
		SenderID:         userID,
		MessageText:      message,
		ContainerOwner:   &containerOwner,
		EnforcementRules: enforcementRules,
	}

	zumaCallStartTime := time.Now()

	zumaResponse, err := mv.ZumaClient.EnforceMessage(context.Background(), extractMessageReq, nil)

	go mv.Statter.TimingDuration("BadgeTierNotification_CallZumaEnforceMessage", time.Since(zumaCallStartTime))

	// If Zuma fails but the message is blank, we should optimistically ignore the error and succeed
	// See similar approach from Subs:
	// https://git-aws.internal.justin.tv/revenue/subscriptions/blob/191a507a5d35f329c5c8c44a4c20b911feed50ce/internal/api/legacy/internal_post_chat_notification.go#L153
	if err != nil && !strings.Blank(message) {
		return err
	}

	if err == nil && !zumaResponse.AllPassed {
		zumaResponseJson, err := json.Marshal(zumaResponse)
		msg := "zuma validation failed for badge tier sharing user notice message"
		if err != nil {
			log.WithError(err).Error("error marshalling zuma response for badge tier sharing user notice message eligibility")
		} else {
			log.Info(fmt.Sprintf(msg+": %s\n", zumaResponseJson))
		}
		return &MessageValidationErr{
			msg: msg,
		}
	}
	return nil
}
