package tiernotifications

import (
	"context"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/badgetiers/models"
	badgetieremotes "code.justin.tv/commerce/payday/badgetiers/products/emotes"
	"code.justin.tv/commerce/payday/badgetiers/utils"
	"code.justin.tv/commerce/payday/clients/chatbadges"
	"code.justin.tv/commerce/payday/dynamo/badge_tier_notification"
	paydayModels "code.justin.tv/commerce/payday/models"
	"github.com/twitchtv/twirp"
)

const (
	notificationTrackingTimeout = 5 * time.Second
)

type Setter interface {
	SetNotificationState(ctx context.Context, notificationID string, newNotificationState models.BadgeTierNotificationStatus, message string, userID string) error
	CreateNotification(ctx context.Context, channelID string, userID string, threshold int64) error
}

type setter struct {
	ChatBadgesClient        chatbadges.IChatBadgesClient                     `inject:""`
	Dao                     badge_tier_notification.BadgeTierNotificationDAO `inject:""`
	UserNotice              UserNotice                                       `inject:""`
	PubSubber               PubSubber                                        `inject:""`
	Cache                   Cache                                            `inject:""`
	DataScience             DataScience                                      `inject:""`
	BadgeTierEmotesLogician badgetieremotes.Logician                         `inject:""`
}

func NewSetter() Setter {
	return &setter{}
}

func (s *setter) SetNotificationState(ctx context.Context, notificationID string, newNotificationState models.BadgeTierNotificationStatus, message string, userID string) error {
	notification, err := s.Dao.GetNotification(notificationID)
	if err != nil {
		return twirp.InternalErrorWith(err)
	}

	if notification == nil {
		return twirp.NotFoundError("notification not found")
	}

	if notification.NotificationState == models.Shared {
		return twirp.NewError(twirp.AlreadyExists, "notification has already been shared")
	}

	if notification.UserID != userID {
		return twirp.NewError(twirp.PermissionDenied, "attempting to update a notification not owned by user")
	}

	if newNotificationState == models.Shared {
		if !canShareNotification(notification.Threshold) {
			return twirp.NewError(twirp.InvalidArgument, "attempting to share a notification below the allowable threshold")
		}

		err = s.UserNotice.SendMessage(notification.ChannelID, notification.UserID, notification.Threshold, message)

		if err != nil {
			switch err.(type) {
			case *MessageValidationErr:
				return twirp.InvalidArgumentError("message", "invalid message")
			default:
				return twirp.InternalErrorWith(err)

			}
		}

		go s.PubSubber.SendPublicNotificationShareMessage(notification, message)
	}

	err = s.Dao.UpdateItemNotificationState(notification.ChannelID, notification.UserID, notification.Threshold, newNotificationState)

	if err != nil {
		return twirp.InternalErrorWith(err)
	}

	s.Cache.Del(ctx, notification.UserID, notification.ChannelID)

	go func() {
		ctx, cancel := context.WithTimeout(context.Background(), notificationTrackingTimeout)
		defer cancel()

		s.performNotificationTracking(ctx, notification, newNotificationState, message)
	}()

	return nil
}

func (s setter) performNotificationTracking(ctx context.Context, notification *badge_tier_notification.BadgeTierNotification, notificationState models.BadgeTierNotificationStatus, message string) {
	badgeType := "default"
	customizedBadges, err := utils.GetBitsBadgeCustomizationInfo(s.ChatBadgesClient, notification.ChannelID)
	if err != nil {
		msg := "Encountered an error getting channel badges from chat badge service"
		log.WithError(err).Error(msg)
	}
	if customized, present := customizedBadges[notification.Threshold]; present && customized {
		badgeType = "custom"
	}

	emoteID, err := s.getNotificationEmoteID(ctx, notification.ChannelID, notification.Threshold)
	if err != nil {
		log.WithError(err).Error("Encountered an error getting emoteID for bits badge tier notification tracking")
	}

	s.DataScience.TrackBadgeTierNotification(paydayModels.BitsBadgeTierNotification{
		TransactionId:  notification.NotificationID,
		UserId:         notification.UserID,
		ChannelID:      notification.ChannelID,
		Action:         string(notificationState),
		BadgeAmount:    notification.Threshold,
		ContextDetails: message,
		Type:           badgeType,
		EmoteID:        emoteID,
	})
}

func (s setter) getNotificationEmoteID(ctx context.Context, channelID string, threshold int64) (string, error) {
	notificationEmoteIDs, err := s.BadgeTierEmotesLogician.GetEmoteIDsForBadgeTier(ctx, channelID, threshold)
	if err != nil {
		return "", err
	}

	if len(notificationEmoteIDs) > 0 {
		return notificationEmoteIDs[0], nil
	}

	return "", nil
}

func (s *setter) CreateNotification(ctx context.Context, channelID string, userID string, threshold int64) error {
	log := log.WithFields(log.Fields{
		"channelId": channelID,
		"userId":    userID,
		"threshold": threshold,
	})

	existingNotification, err := s.Dao.GetNotificationByChannelUserThreshold(channelID, userID, threshold)

	if err != nil {
		return err
	}

	if existingNotification != nil {
		log.Warn("attempting to create a duplicate badge tier notification")
		return nil
	} else {
		log.Info("no existing duplicate badge tier notification")
	}

	notification, err := s.Dao.CreateNotification(channelID, userID, threshold)
	if err != nil {
		return err
	} else {
		log.WithField("notification", notification).Info("created badge tier notification")
	}

	s.Cache.Del(ctx, notification.UserID, notification.ChannelID)

	err = s.PubSubber.SendNotificationCreationMessage(ctx, notification)

	if err != nil {
		return err
	} else {
		log.WithField("notification", notification).Info("sent badge tier notification to pubsub")
	}

	go func() {
		ctx, cancel := context.WithTimeout(context.Background(), notificationTrackingTimeout)
		defer cancel()

		s.performNotificationTracking(ctx, notification, notification.NotificationState, "")
		log.WithField("notification", notification).Info("performed badge tier notification tracking")
	}()

	return nil
}
