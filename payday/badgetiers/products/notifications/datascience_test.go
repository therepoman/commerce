package tiernotifications

import (
	"testing"

	chatbadgeModels "code.justin.tv/commerce/payday/badgetiers/models"
	"code.justin.tv/commerce/payday/datascience"
	datascience_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/datascience"
	"code.justin.tv/commerce/payday/models"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestDataScience_TrackBadgeTierNotification(t *testing.T) {
	Convey("Given a badge tier notification tracker", t, func() {
		dataScienceClient := new(datascience_mock.DataScience)

		dataScience := &dataScience{
			DataScienceClient: dataScienceClient,
		}

		userID := "abc"
		channelID := "123"
		notificationID := "1-1-1"
		state := chatbadgeModels.Shared
		message := "hi"
		var threshold int64 = 1000

		Convey("we should call TrackBitsEvent", func() {
			expectedEvent := models.BitsBadgeTierNotification{
				TransactionId:  notificationID,
				UserId:         userID,
				ChannelID:      channelID,
				Action:         string(state),
				BadgeAmount:    threshold,
				ContextDetails: message,
			}
			dataScienceClient.On("TrackBitsEvent", mock.Anything, datascience.BitsBadgeTierNotification, &expectedEvent).Return(nil)

			dataScience.TrackBadgeTierNotification(expectedEvent)
			dataScienceClient.AssertCalled(t, "TrackBitsEvent", mock.Anything, datascience.BitsBadgeTierNotification, &expectedEvent)
		})
	})
}
