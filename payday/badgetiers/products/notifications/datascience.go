package tiernotifications

import (
	"context"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/datascience"
	"code.justin.tv/commerce/payday/models"
)

const (
	trackingErrorMessage = "Failed to send data science event for badge tier notification"
)

type DataScience interface {
	TrackBadgeTierNotification(event models.BitsBadgeTierNotification)
}

type dataScience struct {
	DataScienceClient datascience.DataScience `inject:""`
}

func NewDataScience() DataScience {
	return &dataScience{}
}

func (d *dataScience) TrackBadgeTierNotification(event models.BitsBadgeTierNotification) {
	err := d.DataScienceClient.TrackBitsEvent(context.Background(), datascience.BitsBadgeTierNotification, &event)

	if err != nil {
		log.WithFields(log.Fields{
			"notification_id":    event.TransactionId,
			"notification_state": event.Action,
		}).Error(trackingErrorMessage)
	}
}
