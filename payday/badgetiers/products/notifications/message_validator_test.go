package tiernotifications

import (
	"errors"
	"testing"

	zuma "code.justin.tv/chat/zuma/app/api"
	zuma_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/zuma"
	metrics_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/metrics"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestMessageValidator_ValidateMessage(t *testing.T) {
	Convey("Given a message validator", t, func() {
		zumaClientWrapper := new(zuma_mock.IZumaClientWrapper)
		statter := new(metrics_mock.Statter)

		messageValidator := messageValidator{
			ZumaClient: zumaClientWrapper,
			Statter:    statter,
		}

		statter.On("TimingDuration", mock.Anything, mock.Anything).Return()

		channelID := "116076154"
		userID := "232889822"
		message := "test message"

		Convey("when zuma EnforceMessage call fails", func() {
			zumaClientWrapper.On("EnforceMessage", mock.Anything, mock.Anything, mock.Anything).Return(zuma.EnforceMessageResponse{}, errors.New("test error"))

			Convey("when message for user notice is not blank then it should return an error", func() {
				err := messageValidator.ValidateMessage(channelID, userID, message)

				So(err, ShouldNotBeNil)
			})

			Convey("when message for user notice is blank then it should succeed", func() {
				err := messageValidator.ValidateMessage(channelID, userID, "")

				So(err, ShouldBeNil)
			})
		})

		Convey("when zuma EnforceMessage call succeeds but validation fails", func() {
			zumaClientWrapper.On("EnforceMessage", mock.Anything, mock.Anything, mock.Anything).Return(zuma.EnforceMessageResponse{
				EnforceProperties: zuma.EnforceProperties{
					AllPassed: false,
					Banned: &zuma.BannedResponse{
						GenericEnforcementResponse: zuma.GenericEnforcementResponse{
							Passed: false,
						},
					},
				},
			}, nil)

			Convey("it should return an error", func() {
				err := messageValidator.ValidateMessage(channelID, userID, message)

				So(err, ShouldNotBeNil)
			})
		})

		Convey("when zuma EnforceMessage call succeeds and validation passes", func() {
			zumaClientWrapper.On("EnforceMessage", mock.Anything, mock.Anything, mock.Anything).Return(zuma.EnforceMessageResponse{
				EnforceProperties: zuma.EnforceProperties{
					AllPassed: true,
				},
			}, nil)

			Convey("it should succeed", func() {
				err := messageValidator.ValidateMessage(channelID, userID, message)

				So(err, ShouldBeNil)
			})
		})
	})
}
