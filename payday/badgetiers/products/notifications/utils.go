package tiernotifications

import (
	"code.justin.tv/commerce/payday/badgetiers/models"
)

func canShareNotification(threshold int64) bool {
	return threshold >= models.ShareableThreshold
}
