package tiernotifications

import (
	"testing"

	"code.justin.tv/commerce/payday/badgetiers/models"
	. "github.com/smartystreets/goconvey/convey"
)

func TestUtils_CanShareNotification(t *testing.T) {
	Convey("given a canShareNotification util", t, func() {
		Convey("we should return false when threshold is < shareable threshold", func() {
			canShare := canShareNotification(models.ShareableThreshold - 1)

			So(canShare, ShouldBeFalse)
		})

		Convey("we should return true when threshold == shareable threshold", func() {
			canShare := canShareNotification(models.ShareableThreshold)

			So(canShare, ShouldBeTrue)
		})

		Convey("we should return true when threshold > shareable threshold", func() {
			canShare := canShareNotification(models.ShareableThreshold + 1)

			So(canShare, ShouldBeTrue)
		})
	})
}
