package tiernotifications

import (
	"context"
	"encoding/json"
	"errors"
	"testing"
	"time"

	"code.justin.tv/commerce/gogogadget/pointers"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/dynamo/badge_tier_notification"
	client_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/chat/pubsub-go-pubclient/client"
	userservice_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/userservice"
	"code.justin.tv/commerce/payday/models"
	userServiceModels "code.justin.tv/web/users-service/models"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestPubSubber_SendNotificationCreationUpdate(t *testing.T) {
	Convey("Given a tier notification creation PubSubber", t, func() {
		ctx := context.Background()
		pubClient := new(client_mock.PubClient)

		pubSubber := pubSubber{
			PubClient: pubClient,
		}

		userID := "123123123"
		channelID := "test_channel_id"
		notificationID := "some-notification-id"
		var threshold int64 = 100

		badgeTierNotification := &badge_tier_notification.BadgeTierNotification{
			UserID:         userID,
			ChannelID:      channelID,
			Threshold:      threshold,
			NotificationID: notificationID,
		}

		Convey("when PubClient fails, PubSubber returns the error", func() {
			pubClientTestError := "pub client ERROR"
			pubClient.On("Publish", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(errors.New(pubClientTestError))

			err := pubSubber.SendNotificationCreationMessage(ctx, badgeTierNotification)

			So(err, ShouldNotBeNil)
			So(err.Error(), ShouldContainSubstring, pubClientTestError)
		})

		Convey("when PubClient succeeds, PubSubber returns nil", func() {
			pubClient.On("Publish", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)

			err := pubSubber.SendNotificationCreationMessage(ctx, badgeTierNotification)

			So(err, ShouldBeNil)
		})
	})
}

func TestPubSubber_SendPublicNotificationShareUpdate(t *testing.T) {
	Convey("Given a public tier notification share PubSubber", t, func() {
		pubClient := new(client_mock.PubClient)
		userFetcher := new(userservice_mock.Fetcher)

		pubSubber := pubSubber{
			PubClient:          pubClient,
			UserServiceFetcher: userFetcher,
		}

		userID := "123123123"
		channelID := "test_channel_id"
		notificationID := "some-notification-id"
		message := ""
		var threshold int64 = 100

		expectedTopic := makePublicNotificationShareTopic(channelID)

		badgeTierNotification := &badge_tier_notification.BadgeTierNotification{
			UserID:         userID,
			ChannelID:      channelID,
			Threshold:      threshold,
			NotificationID: notificationID,
			CreatedAt:      time.Now(),
		}

		Convey("when fetching the user name fails, we should not publish the message", func() {
			mockChannel := &userServiceModels.Properties{
				Login: pointers.StringP("ChannelName"),
			}

			userFetcher.On("Fetch", mock.Anything, userID).Return(nil, errors.New("ERROR"))
			userFetcher.On("Fetch", mock.Anything, channelID).Return(mockChannel, nil)
			pubClient.On("Publish", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)

			pubSubber.SendPublicNotificationShareMessage(badgeTierNotification, message)

			pubClient.AssertNotCalled(t, "Publish", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything)
		})

		Convey("when fetching the channel name fails, we should not publish the message", func() {
			mockUser := &userServiceModels.Properties{
				Login: pointers.StringP("UserName"),
			}

			userFetcher.On("Fetch", mock.Anything, userID).Return(mockUser, nil)
			userFetcher.On("Fetch", mock.Anything, channelID).Return(nil, errors.New("ERROR"))
			pubClient.On("Publish", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)

			pubSubber.SendPublicNotificationShareMessage(badgeTierNotification, message)

			pubClient.AssertNotCalled(t, "Publish", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything)
		})

		Convey("when all necessary data is available, we should publish a message", func() {
			mockUser := &userServiceModels.Properties{
				Login: pointers.StringP("UserName"),
			}

			mockChannel := &userServiceModels.Properties{
				Login: pointers.StringP("ChannelName"),
			}

			expectedMessage := models.PublicBadgeTierUnlockShare{
				UserID:      badgeTierNotification.UserID,
				UserName:    "UserName",
				ChannelID:   badgeTierNotification.ChannelID,
				ChannelName: "ChannelName",
				Threshold:   badgeTierNotification.Threshold,
				ChatMessage: message,
				Time:        badgeTierNotification.CreatedAt,
			}

			marshaledMessage, err := json.Marshal(expectedMessage)

			if err != nil {
				log.WithError(err).Error("Failed to serialize JSON for mock pubsub message")
				t.Fail()
			}

			userFetcher.On("Fetch", mock.Anything, userID).Return(mockUser, nil)
			userFetcher.On("Fetch", mock.Anything, channelID).Return(mockChannel, nil)
			pubClient.On("Publish", mock.Anything, expectedTopic, string(marshaledMessage), mock.Anything).Return(nil)

			pubSubber.SendPublicNotificationShareMessage(badgeTierNotification, message)

			pubClient.AssertCalled(t, "Publish", mock.Anything, expectedTopic, string(marshaledMessage), mock.Anything)
		})
	})
}
