package tiernotifications

import (
	"context"

	"code.justin.tv/commerce/payday/badgetiers/models"
)

type Getter interface {
	Get(ctx context.Context, userID string, channelID string) (*models.BadgeTierNotification, error)
}

type getter struct {
	Fetcher Fetcher `inject:""`
}

func NewGetter() Getter {
	return &getter{}
}

func (g *getter) Get(ctx context.Context, userID string, channelID string) (*models.BadgeTierNotification, error) {
	badgeTierNotification, err := g.Fetcher.Fetch(ctx, userID, channelID)

	if err != nil {
		return nil, err
	}

	return badgeTierNotification, nil
}
