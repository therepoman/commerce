package tiernotifications

import (
	"strconv"
	"time"

	"code.justin.tv/commerce/payday/clients/tmi"
	"code.justin.tv/commerce/payday/metrics"
	"code.justin.tv/commerce/payday/models"
)

const (
	SystemMessageID = "bitsbadgetier"
)

type UserNotice interface {
	SendMessage(channelID string, userID string, threshold int64, message string) error
}

type userNotice struct {
	TmiClient        tmi.ITMIClient   `inject:""`
	Statter          metrics.Statter  `inject:""`
	MessageValidator MessageValidator `inject:""`
}

func NewUserNotice() UserNotice {
	return &userNotice{}
}

func (un *userNotice) SendMessage(channelID string, userID string, threshold int64, message string) error {
	parsedChannelID, err := strconv.ParseInt(channelID, 10, 64)

	if err != nil {
		return err
	}

	parsedUserID, err := strconv.ParseInt(userID, 10, 64)

	if err != nil {
		return err
	}

	// Calling zuma here allows us to be more confident that the TMI call will be successful.
	// If the message fails zuma validation when we call TMI (TMI calls zuma too), TMI returns a 400, which we retry
	// based on our default http utils retry strategy. Rather than trying to rework the strategy for this TMI call, we
	// decided to call zuma here instead, just like we do with cheers.
	// ToDo: Remove this if we move TMI call to a twirp client. Then we can react differently to validation specific failures from TMI.
	err = un.MessageValidator.ValidateMessage(channelID, userID, message)

	if err != nil {
		return err
	}

	tmiCallStartTime := time.Now()

	err = un.TmiClient.SendUserNotice(models.UserNoticeParams{
		SenderUserID:    parsedUserID,
		TargetChannelID: parsedChannelID,
		Body:            message,
		MsgId:           SystemMessageID,
		MsgParams: []models.UserNoticeMsgParam{
			{
				Key:   "threshold",
				Value: strconv.FormatInt(threshold, 10),
			},
		},
		DefaultSystemBody: "bits badge tier notification",
	})

	go un.Statter.TimingDuration("BadgeTierNotification_CallTMIUserNotice", time.Since(tmiCallStartTime))

	return err
}
