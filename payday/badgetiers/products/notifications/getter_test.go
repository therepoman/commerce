package tiernotifications

import (
	"context"
	"testing"

	"code.justin.tv/commerce/payday/badgetiers/models"
	tiernotifications_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/badgetiers/products/notifications"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestGetter_Get(t *testing.T) {
	Convey("given a badge tier notification getter", t, func() {
		fetcher := new(tiernotifications_mock.Fetcher)

		getter := &getter{
			Fetcher: fetcher,
		}

		userID := "123123123"
		channelID := "abcdefg"

		Convey("when we error fetching the tier notification", func() {
			fetcher.On("Fetch", mock.Anything, userID, channelID).Return(nil, errors.New("ERROR"))

			Convey("We should return an error", func() {
				_, err := getter.Get(context.Background(), userID, channelID)

				So(err, ShouldNotBeNil)
			})
		})

		Convey("when we succeed fetching the tier notification", func() {
			badgeTierNotification := &models.BadgeTierNotification{}

			fetcher.On("Fetch", mock.Anything, userID, channelID).Return(badgeTierNotification, nil)

			Convey("we should return the tier notification", func() {
				returnValue, err := getter.Get(context.Background(), userID, channelID)

				So(err, ShouldBeNil)
				So(returnValue, ShouldEqual, badgeTierNotification)
			})
		})
	})
}
