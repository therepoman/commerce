package tiernotifications

import (
	"context"
	"time"

	"code.justin.tv/commerce/payday/badgetiers/models"
	badgetieremotes "code.justin.tv/commerce/payday/badgetiers/products/emotes"
	"code.justin.tv/commerce/payday/dynamo/badge_tier_notification"
)

const notificatonExpirationTimeMinutes = 10

type Fetcher interface {
	Fetch(ctx context.Context, userID string, channelID string) (*models.BadgeTierNotification, error)
}

type fetcher struct {
	Dao                     badge_tier_notification.BadgeTierNotificationDAO `inject:""`
	Cache                   Cache                                            `inject:""`
	BadgeTierEmotesLogician badgetieremotes.Logician                         `inject:""`
}

func NewFetcher() Fetcher {
	return &fetcher{}
}

func (f *fetcher) Fetch(ctx context.Context, userID string, channelID string) (*models.BadgeTierNotification, error) {
	badgeTierNotificationCacheGetResp := f.Cache.Get(ctx, userID, channelID)

	if badgeTierNotificationCacheGetResp != nil {
		if badgeTierNotificationCacheGetResp.BadgeTierNotification == nil {
			return nil, nil
		} else {
			return badgeTierNotificationCacheGetResp.BadgeTierNotification, nil
		}
	}

	badgeTierNotificationRecord, err := f.Dao.GetLargestNotificationByThreshold(channelID, userID)

	if err != nil {
		return nil, err
	}

	if badgeTierNotificationRecord == nil ||
		shouldHideNotification(badgeTierNotificationRecord.NotificationState) ||
		hasNotificationExpired(badgeTierNotificationRecord.CreatedAt) {

		f.Cache.SetEmpty(ctx, userID, channelID)

		return nil, nil
	}

	// Get the latest emote IDs that should be sent with the tier notification
	notificationEmoteIDs, err := f.BadgeTierEmotesLogician.GetEmoteIDsForBadgeTier(ctx, channelID, badgeTierNotificationRecord.Threshold)

	if err != nil {
		return nil, err
	}

	badgeTierNotification := models.BadgeTierNotification{
		Threshold:         badgeTierNotificationRecord.Threshold,
		NotificationState: badgeTierNotificationRecord.NotificationState,
		NotificationId:    badgeTierNotificationRecord.NotificationID,
		CanShare:          canShareNotification(badgeTierNotificationRecord.Threshold),
		EmoteIDs:          notificationEmoteIDs,
	}

	f.Cache.Set(ctx, userID, channelID, badgeTierNotification)

	return &badgeTierNotification, nil
}

// Notifications older than 10 minutes are considered expired, and not shown
// to users.
func hasNotificationExpired(createdAt time.Time) bool {
	return createdAt.Before(time.Now().Add(-notificatonExpirationTimeMinutes * time.Minute))
}

func shouldHideNotification(notificationState models.BadgeTierNotificationStatus) bool {
	return notificationState != models.Show
}
