package badgetieremotes

import (
	"context"
	"errors"
	"sort"
	"time"

	"code.justin.tv/commerce/gogogadget/math"
	log "code.justin.tv/commerce/logrus"
	rpc "code.justin.tv/commerce/mako/twirp"
	"code.justin.tv/commerce/payday/badgetiers"
	"code.justin.tv/commerce/payday/cache"
	"code.justin.tv/commerce/payday/cache/channel"
	"code.justin.tv/commerce/payday/clients/mako"
	"code.justin.tv/commerce/payday/dynamo/badge_tier_emote_groupids"
	"code.justin.tv/commerce/payday/hystrix"
	cmds "code.justin.tv/commerce/payday/hystrix"
	"code.justin.tv/commerce/payday/leaderboard"
	"code.justin.tv/commerce/payday/metrics"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"code.justin.tv/commerce/payday/utils/strings"
	"github.com/cenkalti/backoff"
)

const (
	// Locked Preview shouldn't show more than 3 emotes (pending an future experimentation)
	MaxTiersForLockedPreview    = int64(3)
	MinThresholdForEmoteRewards = int64(999)

	ChannelManagerLatency  = "GetBadgeTierEmotes_ChannelManagerLatency"
	PantheonLatency        = "GetBadgeTierEmotes_PantheonLatency"
	BadgeTierDynamoLatency = "GetBadgeTierEmotes_BadgeTierDynamoLatency"
	MakoLatency            = "GetBadgeTierEmotes_MakoLatency"
)

type Logician interface {
	GetAndStoreGroupIDForBadgeTier(ctx context.Context, channelID string, threshold int64) (*badge_tier_emote_groupids.BadgeTierEmoteGroupID, bool, error)
	CreateAndStoreAllGroupIDsForChannel(ctx context.Context, channelID string) error
	GetBitsTierSummaryForEmoteGroupID(ctx context.Context, groupID string, userID *string) (*paydayrpc.GetBitsTierSummaryForEmoteGroupIDResp, bool, error)
	GetBadgeTierEmotes(ctx context.Context, channelID string, userID string, filter paydayrpc.BitsBadgeTierEmoteFilter) ([]*paydayrpc.BitsBadgeTierEmote, error)
	GetEmoteIDsForBadgeTier(ctx context.Context, channelID string, threshold int64) ([]string, error)
}
type logician struct {
	ChannelManager          channel.ChannelManager                             `inject:""`
	Mako                    mako.MakoClient                                    `inject:""`
	BadgeTierGroupIDDAO     badge_tier_emote_groupids.BadgeTierEmoteGroupIDDAO `inject:""`
	BadgeTierEmotesCache    cache.BadgeTierEmotesCache                         `inject:""`
	BitsToBroadcasterGetter leaderboard.BitsToBroadcasterGetter                `inject:""`
	Statter                 metrics.Statter                                    `inject:""`
}

func NewLogician() Logician {
	return &logician{}
}

// GetAndStoreGroupIDForBadgeTier checks dynamo for the given channel/threshold combination for an existing group ID mapping.
// If it exists, this returns the group ID mapping and a bool flag (false) to indicate if the group ID is new.
// If it does not exist, we call Mako to create one, store it in dynamo, and return the new group ID mapping and true.
func (l *logician) GetAndStoreGroupIDForBadgeTier(ctx context.Context, channelID string, threshold int64) (*badge_tier_emote_groupids.BadgeTierEmoteGroupID, bool, error) {
	fields := log.Fields{
		"channel_id": channelID,
		"threshold":  threshold,
	}

	badgeTierGroupID, err := l.BadgeTierGroupIDDAO.GetByChannelAndThreshold(ctx, channelID, threshold)
	if err != nil {
		log.WithError(err).WithFields(fields).Error("Error fetching group id for threshold from dynamo")
		return nil, false, err
	}

	if badgeTierGroupID != nil && strings.NotBlank(badgeTierGroupID.GroupID) {
		return badgeTierGroupID, false, nil
	}

	newBadgeTierGroupID, groupIDAlreadyExists, err := l.createAndStoreGroupIDForBadgeTier(ctx, channelID, threshold)
	if err != nil {
		if groupIDAlreadyExists {
			// GroupID actually already exists for this channel and tier. Try getting it again.
			// This could happen in potential race conditions.
			retryBadgeTierGroupID, retryErr := l.BadgeTierGroupIDDAO.GetByChannelAndThreshold(ctx, channelID, threshold)
			if retryErr != nil {
				log.WithError(retryErr).WithFields(fields).Error("Error fetching group id for threshold from dynamo while retrying")
				return nil, false, retryErr
			}

			if retryBadgeTierGroupID != nil && strings.NotBlank(retryBadgeTierGroupID.GroupID) {
				return retryBadgeTierGroupID, false, nil
			}
		}

		log.WithError(err).WithFields(fields).Error("Error creating and storing groupID for badge tier")
		return nil, false, err
	}

	return newBadgeTierGroupID, true, err
}

// CreateAndStoreAllGroupIDsForChannel is used to create and store all missing badge tier group IDs for a channel.
// Existing group IDs will not be over-written with new ones.
// At the time of writing, this can result in up to 26 parallel network calls to Mako and DynamoDB, so this should only
// be used in low TPS call paths or planned script runs.
func (l *logician) CreateAndStoreAllGroupIDsForChannel(ctx context.Context, channelID string) error {
	// Check if the channel already has some groupIDs stored
	badgeTierGroups, err := l.BadgeTierGroupIDDAO.Get(ctx, channelID)
	if err != nil {
		log.WithError(err).WithField("channel_id", channelID).Error("Error getting channel badge tier group IDs from DynamoDB while attempting to create all channel group IDs")
		return err
	}

	thresholdsWithExistingGroupIDs := make(map[int64]bool)
	for _, group := range badgeTierGroups {
		if strings.NotBlank(group.GroupID) {
			thresholdsWithExistingGroupIDs[group.Threshold] = true
		}
	}

	// Filter out thresholds with existing group IDs and those below the minimum threshold allowed for emotes
	var validThresholds []int64
	for threshold := range badgetiers.BitsChatBadgeTiers {
		if !thresholdsWithExistingGroupIDs[threshold] && threshold >= lowestAllowedTierForEmotes {
			validThresholds = append(validThresholds, threshold)
		}
	}

	// Create all missing group IDs in parallel
	errChan := make(chan error)
	for _, threshold := range validThresholds {
		go func(channelID string, threshold int64) {
			localCtx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
			defer cancel()

			_, _, err := l.createAndStoreGroupIDForBadgeTier(localCtx, channelID, threshold)
			errChan <- err
		}(channelID, threshold)
	}

	// Error out if at least one fails or we time out
	for i := 0; i < len(validThresholds); i++ {
		select {
		case err := <-errChan:
			if err != nil {
				return err
			}
		case <-ctx.Done():
			return errors.New("timed out creating and storing all group IDs for channel")
		}
	}

	return nil
}

func (l *logician) createAndStoreGroupIDForBadgeTier(ctx context.Context, channelID string, threshold int64) (*badge_tier_emote_groupids.BadgeTierEmoteGroupID, bool, error) {
	fields := log.Fields{
		"channel_id": channelID,
		"threshold":  threshold,
	}

	groupID, err := l.Mako.CreateEmoteGroup(ctx)
	if err != nil {
		log.WithError(err).WithFields(fields).Error("Error creating new emote group in Mako")
		return nil, false, err
	}

	newBadgeTierGroupID, groupIDAlreadyExists, err := l.BadgeTierGroupIDDAO.Create(ctx, channelID, threshold, groupID)
	if err != nil {
		fields["group_id"] = groupID
		log.WithError(err).WithFields(fields).Error("Error creating badge tier / group ID mapping in DynamoDB")
	}

	return newBadgeTierGroupID, groupIDAlreadyExists, err
}

func (l *logician) GetBitsTierSummaryForEmoteGroupID(ctx context.Context, groupID string, userID *string) (*paydayrpc.GetBitsTierSummaryForEmoteGroupIDResp, bool, error) {
	badgeTierGroupIDInfo, wasNotFound, err := l.BadgeTierGroupIDDAO.GetByGroupID(ctx, groupID)

	if err != nil {
		if wasNotFound {
			return nil, true, err
		}
		msg := "Error retrieving badge tier summary from dynamo"
		log.WithError(err).WithField("groupID", groupID).Error(msg)
		return nil, false, err
	}

	if badgeTierGroupIDInfo == nil {
		msg := "groupID info was not found in dynamo"
		log.WithField("groupID", groupID).Error(msg)
		return nil, false, nil
	}

	resp := paydayrpc.GetBitsTierSummaryForEmoteGroupIDResp{
		GroupId:                 groupID,
		EmoteChannelId:          badgeTierGroupIDInfo.ChannelID,
		BadgeTierThreshold:      badgeTierGroupIDInfo.Threshold,
		NumberOfBitsUntilUnlock: badgeTierGroupIDInfo.Threshold,
		BadgeTierSummary: &paydayrpc.EmoteBitsBadgeTierSummary{
			BadgeTierThreshold:      badgeTierGroupIDInfo.Threshold,
			NumberOfBitsUntilUnlock: badgeTierGroupIDInfo.Threshold,
		},
	}
	if userID != nil && *userID != "" {
		if badgeTierGroupIDInfo.ChannelID == "" {
			msg := "missing channelID (primary key) for groupID (global secondary index) in dynamo should never happen"
			log.WithField("groupID", groupID).Error(msg)
			err := errors.New(msg)
			return nil, false, err
		}

		emoteChannelID := badgeTierGroupIDInfo.ChannelID

		// Channel owners always have their emotes unlocked automatically
		if *userID == emoteChannelID {
			resp.IsEmoteUnlockedForUser = true
			resp.NumberOfBitsUntilUnlock = 0
			resp.BadgeTierSummary.IsEmoteUnlockedForUser = true
			resp.BadgeTierSummary.NumberOfBitsUntilUnlock = 0
		} else {
			numberOfBitsCheered, err := l.BitsToBroadcasterGetter.GetBitsToBroadcaster(ctx, *userID, emoteChannelID)
			if err != nil {
				msg := "Error getting leaderboard entry (spent bits) for user in channel from Pantheon for determining bits tier summary"
				log.WithError(err).WithFields(log.Fields{
					"emoteChannelID": emoteChannelID,
					"userID":         userID,
				}).Error(msg)
				return nil, false, err
			}

			//If the user has already cheered enough, then return zero bits remaining until unlock
			resp.NumberOfBitsUntilUnlock = math.MaxInt64(badgeTierGroupIDInfo.Threshold-numberOfBitsCheered, int64(0))
			resp.BadgeTierSummary.NumberOfBitsUntilUnlock = resp.NumberOfBitsUntilUnlock
			resp.IsEmoteUnlockedForUser = numberOfBitsCheered >= badgeTierGroupIDInfo.Threshold
			resp.BadgeTierSummary.IsEmoteUnlockedForUser = resp.IsEmoteUnlockedForUser
		}

	}

	return &resp, false, nil
}

func (l *logician) GetBadgeTierEmotes(ctx context.Context, channelID string, userID string, filter paydayrpc.BitsBadgeTierEmoteFilter) ([]*paydayrpc.BitsBadgeTierEmote, error) {
	logger := log.WithFields(log.Fields{
		"operation": "GetBadgeTierEmotes",
		"channelID": channelID,
		"userID":    userID,
	})

	start := time.Now()
	ch, err := l.ChannelManager.Get(ctx, channelID)
	l.Statter.TimingDuration(ChannelManagerLatency, time.Since(start))
	if err != nil {
		logger.WithError(err).Error("Error getting channel info for bits badge tier emotes")
		return nil, err
	} else if !channel.GetChannelEligibility(ch) {
		return nil, nil // not eligible to use bits in this channel, no need for the rest of this logic.
	}

	start = time.Now()
	groupMap, groupIds, err := l.getConfiguredEmoteGroups(ctx, channelID)
	l.Statter.TimingDuration(BadgeTierDynamoLatency, time.Since(start))
	if err != nil {
		logger.WithError(err).Error("Error getting and formatting emote groups for bits badge tier emotes")
		return nil, err
	} else if len(groupIds) == 0 {
		return nil, nil // no configured group ids for this channel, no need to ask Mako.
	}
	logger = logger.WithField("groupIds", groupIds)

	// Purposely starting the pantheon query below the `getConfiguredEmoteGroups` call. In the case that the channel
	// does not have BTER emotes configured, we will return early, canceling the request context. This was previously
	// resulting in many cascading cancels to pantheon.
	errChan := make(chan error, 1)
	bitsGivenChan := make(chan int64, 1)
	go func() {
		start := time.Now()
		numberOfBitsGiven, err := l.getBitsCheered(ctx, channelID, userID)
		l.Statter.TimingDuration(PantheonLatency, time.Since(start))
		if err != nil {
			errChan <- err
		} else {
			bitsGivenChan <- numberOfBitsGiven
		}
	}()

	start = time.Now()
	emoteGroups, err := l.Mako.GetEmoticonsByGroups(ctx, groupIds, []rpc.EmoteState{rpc.EmoteState_active})
	l.Statter.TimingDuration(MakoLatency, time.Since(start))
	if err != nil {
		logger.WithError(err).WithField("emoteGroupIds", groupIds).Error("Error getting emotes from Mako for bits badge tier emotes")
		return nil, err
	}

	var numberOfBitsGiven int64
	select {
	case err := <-errChan:
		logger.WithError(err).Error("Error getting user contribution and threshold from Pantheon for bits badge tier emotes")
		return nil, err
	case numberOfBitsGiven = <-bitsGivenChan:
	case <-ctx.Done():
		return nil, ctx.Err()
	}

	emotes := l.combineEmoteDataAndBitsTierData(numberOfBitsGiven, groupMap, emoteGroups)
	return applyFilter(emotes, filter), nil
}

func applyFilter(emotes []*paydayrpc.BitsBadgeTierEmote, filter paydayrpc.BitsBadgeTierEmoteFilter) []*paydayrpc.BitsBadgeTierEmote {
	filteredEmotes := make([]*paydayrpc.BitsBadgeTierEmote, 0)
	previewCount := int64(0)

	for _, emote := range emotes {
		switch filter {
		case paydayrpc.BitsBadgeTierEmoteFilter_UNLOCKED:
			if emote.BadgeTierSummary.IsEmoteUnlockedForUser {
				filteredEmotes = append(filteredEmotes, emote)
			}
		case paydayrpc.BitsBadgeTierEmoteFilter_LOCKED_PREVIEW:
			if !emote.BadgeTierSummary.IsEmoteUnlockedForUser {
				if previewCount < MaxTiersForLockedPreview {
					filteredEmotes = append(filteredEmotes, emote)
					previewCount++
				} else {
					return filteredEmotes //bail early we have enough
				}
			}
		case paydayrpc.BitsBadgeTierEmoteFilter_LOCKED:
			if !emote.BadgeTierSummary.IsEmoteUnlockedForUser {
				filteredEmotes = append(filteredEmotes, emote)
			}
		case paydayrpc.BitsBadgeTierEmoteFilter_HIGHEST_UNLOCKED_AND_NEXT:
			if emote.BadgeTierSummary.IsEmoteUnlockedForUser {
				filteredEmotes = []*paydayrpc.BitsBadgeTierEmote{emote}
			} else if previewCount < 1 {
				filteredEmotes = append(filteredEmotes, emote)
				previewCount++
			}
		case paydayrpc.BitsBadgeTierEmoteFilter_ALL:
			fallthrough
		default:
			filteredEmotes = append(filteredEmotes, emote)
		}
	}

	return filteredEmotes
}

func (l *logician) combineEmoteDataAndBitsTierData(bitsGiven int64, tierInformation map[string]badge_tier_emote_groupids.BadgeTierEmoteGroupID, emoteGroups []rpc.EmoticonGroup) []*paydayrpc.BitsBadgeTierEmote {
	emotes := make([]*paydayrpc.BitsBadgeTierEmote, 0)
	for _, emoteGroup := range emoteGroups {
		if len(emoteGroup.Emoticons) > 1 {
			log.WithFields(log.Fields{
				"channelId":    tierInformation[emoteGroup.Id].ChannelID,
				"emoteGroupId": emoteGroup.Id,
				"count":        len(emoteGroup.Emoticons),
			}).Warning("Found more than 1 emote in a bits tier group")
			l.Statter.Inc("ExtraEmoteAttachedToBitsTier", int64(len(emoteGroup.Emoticons)-1))
		}

		threshold := tierInformation[emoteGroup.Id].Threshold

		for _, emote := range emoteGroup.Emoticons {
			emotes = append(emotes, &paydayrpc.BitsBadgeTierEmote{
				EmoteId:        emote.Id,
				EmoteCode:      emote.Code,
				EmoteChannelId: emote.OwnerId,
				EmoteGroupId:   emoteGroup.Id,
				BadgeTierSummary: &paydayrpc.EmoteBitsBadgeTierSummary{
					BadgeTierThreshold:      threshold,
					IsEmoteUnlockedForUser:  threshold <= bitsGiven,
					NumberOfBitsUntilUnlock: math.MaxInt64(threshold-bitsGiven, 0),
				},
			})
		}
	}

	sort.Slice(emotes, func(i, j int) bool {
		return emotes[i].BadgeTierSummary.BadgeTierThreshold < emotes[j].BadgeTierSummary.BadgeTierThreshold
	})

	return emotes
}

func (l *logician) getBitsCheered(ctx context.Context, channelID string, userID string) (int64, error) {
	if strings.Blank(userID) || strings.Blank(channelID) {
		return 0, nil
	} else {
		return l.BitsToBroadcasterGetter.GetBitsToBroadcaster(ctx, userID, channelID)
	}
}

func (l *logician) getConfiguredEmoteGroups(ctx context.Context, channelID string) (map[string]badge_tier_emote_groupids.BadgeTierEmoteGroupID, []string, error) {
	var err error
	groups := l.BadgeTierEmotesCache.Get(ctx, channelID)

	if groups == nil {
		f := func() error {
			return hystrix.Do(cmds.BadgeTierEmoteGetGroupIds, func() error {
				groups, err = l.BadgeTierGroupIDDAO.GetAboveThreshold(ctx, channelID, MinThresholdForEmoteRewards)
				return err
			}, nil)
		}

		b := backoff.WithMaxTries(backoff.WithContext(newBackoff(), ctx), 3)
		err = backoff.Retry(f, b)
		if err != nil {
			return nil, nil, err
		} else {
			go l.BadgeTierEmotesCache.Set(context.Background(), channelID, groups)
		}
	}

	sort.Slice(groups, func(i, j int) bool {
		return groups[i].Threshold < groups[j].Threshold
	})

	// use those to get emotes from mako, in some cases you can reduce the quantity of data you're retrieving
	groupMap, groupIds := convertGroupsToMapAndIdArray(groups)
	return groupMap, groupIds, nil
}

func convertGroupsToMapAndIdArray(groups []*badge_tier_emote_groupids.BadgeTierEmoteGroupID) (map[string]badge_tier_emote_groupids.BadgeTierEmoteGroupID, []string) {
	groupIds := make([]string, len(groups))
	groupMap := make(map[string]badge_tier_emote_groupids.BadgeTierEmoteGroupID)

	for i, group := range groups {
		if group != nil {
			groupIds[i] = group.GroupID
			groupMap[group.GroupID] = *group
		}
	}

	return groupMap, groupIds
}

func (l *logician) GetEmoteIDsForBadgeTier(ctx context.Context, channelID string, threshold int64) ([]string, error) {
	fields := log.Fields{
		"channel_id": channelID,
		"threshold":  threshold,
	}

	badgeTierGroupID, err := l.BadgeTierGroupIDDAO.GetByChannelAndThreshold(ctx, channelID, threshold)
	if err != nil {
		log.WithError(err).WithFields(fields).Error("Error fetching group id for threshold from dynamo while getting emoteIDs for badge tier")
		return nil, err
	}

	var tierEmoteIDs []string

	if badgeTierGroupID != nil && strings.NotBlank(badgeTierGroupID.GroupID) {
		emoticonGroups, err := l.Mako.GetEmoticonsByGroups(ctx,
			[]string{badgeTierGroupID.GroupID},
			[]rpc.EmoteState{rpc.EmoteState_active, rpc.EmoteState_pending})
		if err != nil {
			log.WithError(err).WithFields(fields).Error("Error getting emotes by groups from Mako while getting emoteIDs for badge tier")
			return nil, err
		}

		if len(emoticonGroups) > 0 {
			for _, emote := range emoticonGroups[0].Emoticons {
				if emote != nil {
					tierEmoteIDs = append(tierEmoteIDs, (*emote).Id)
				}
			}
		}
	}

	return tierEmoteIDs, nil
}

func newBackoff() *backoff.ExponentialBackOff {
	exponential := backoff.NewExponentialBackOff()
	exponential.MaxElapsedTime = 1 * time.Second
	exponential.Multiplier = 2
	return exponential
}
