package badgetieremotes

import (
	"context"
	"errors"
	"net/http"

	log "code.justin.tv/commerce/logrus"
	makorpcDashboard "code.justin.tv/commerce/mako/twirp/dashboard"
	"code.justin.tv/commerce/payday/api/constants"
	"code.justin.tv/commerce/payday/clients/mako"
	"code.justin.tv/commerce/payday/dynamo/badge_tier_emote_groupids"
	httperror "code.justin.tv/commerce/payday/errors/http"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/models/api"
	string_utils "code.justin.tv/commerce/payday/utils/strings"
)

type Filler interface {
	PopulateWithBadgeTierEmoticons(ctx context.Context, channelID string, tierMap map[int64]*api.BadgeTier) (map[int64]*api.BadgeTier, error)
	PopulateWithUploadingBadgeTierEmoticons(ctx context.Context, tierMap map[int64]*api.BadgeTier, emoticonDataMap map[int64][]api.EmoticonUploadConfiguration) (map[int64]*api.BadgeTier, error)
}

type filler struct {
	MakoClient          mako.MakoClient                                    `inject:""`
	BadgeTierGroupIDDAO badge_tier_emote_groupids.BadgeTierEmoteGroupIDDAO `inject:""`
}

func NewFiller() Filler {
	return &filler{}
}

func (f *filler) PopulateWithBadgeTierEmoticons(ctx context.Context, channelID string, tierMap map[int64]*api.BadgeTier) (map[int64]*api.BadgeTier, error) {
	if len(tierMap) == 0 {
		return nil, nil
	}

	allowedTiersMap := GetAllowedTiersMap(tierMap)

	badgeTierGroups, err := f.BadgeTierGroupIDDAO.Get(ctx, channelID)
	if err != nil {
		msg := "Error getting bits badge tier group ids from dynamo"
		log.WithField("channelID", channelID).WithError(err).Error(msg)
		return nil, httperror.NewWithCause(constants.DownstreamDependencyError, http.StatusInternalServerError, errors.New(msg))
	}

	groupIDs := make([]string, 0, len(tierMap))
	thresholdsByGroupID := make(map[string]int64, len(tierMap))
	for _, group := range badgeTierGroups {
		if allowedTiersMap[group.Threshold] && string_utils.NotBlank(group.GroupID) {
			groupIDs = append(groupIDs, group.GroupID)
			thresholdsByGroupID[group.GroupID] = group.Threshold
		}
	}

	emoticonGroups, err := f.MakoClient.DashboardGetEmotesByGroups(ctx, groupIDs, []makorpcDashboard.EmoteState{makorpcDashboard.EmoteState_active, makorpcDashboard.EmoteState_pending})
	if err != nil {
		msg := "Error getting bits badge tier emoticons by groups from Mako"
		log.WithField("channelID", channelID).WithField("groupIDs", groupIDs).WithError(err).Error(msg)
		return nil, httperror.NewWithCause(constants.DownstreamDependencyError, http.StatusInternalServerError, errors.New(msg))
	}

	emoticonsByThreshold, err := toEmoticonsByThreshold(emoticonGroups, thresholdsByGroupID)
	if err != nil {
		msg := "Error converting emoticons received from Mako"
		log.WithField("channelID", channelID).WithField("groupIDs", groupIDs).WithError(err).Error(msg)
		return nil, httperror.NewWithCause(constants.DownstreamDependencyError, http.StatusInternalServerError, errors.New(msg))
	}

	for threshold, tier := range tierMap {
		if makoEmoticons, present := emoticonsByThreshold[threshold]; present {
			filteredEmoticons := filterToOneEmoticon(makoEmoticons)

			emoticons, err := models.ToPaydayEmoticons(filteredEmoticons)
			if err != nil {
				return nil, httperror.NewWithCause(constants.EmoticonStateInvalid, http.StatusInternalServerError, err)
			}
			tier.Emoticons = &emoticons
		}
		tier.CanUploadEmotesForTier = allowedTiersMap[threshold]
	}

	return tierMap, nil
}

func (f *filler) PopulateWithUploadingBadgeTierEmoticons(ctx context.Context, tierMap map[int64]*api.BadgeTier, emoticonDataMap map[int64][]api.EmoticonUploadConfiguration) (map[int64]*api.BadgeTier, error) {
	for threshold, tier := range tierMap {
		if tier != nil {
			if emoticonData, present := emoticonDataMap[threshold]; present {
				tier.EmoticonUploadConfigurations = &emoticonData
			}
		}
	}

	return tierMap, nil
}

//Because we do not lock on emoticon saves, it is theoretically possible for a user to send multiple SetBadgeTiers POST
//calls at the same time, which each has a different singular emoticon to create. This could get their channel into a
//state of having multiple emoticons created for a single tier, since we only validate that the individual request has
//at most one emoticon. In order to combat this possible edge case, we consistently filter out the extra emotes down to
//just one.
//TODO: change this if product ever asks to allow more than one emoticon per tier
func filterToOneEmoticon(emoticons []makorpcDashboard.Emote) []makorpcDashboard.Emote {
	if len(emoticons) <= allowedEmoticonsPerBadgeTier {
		return emoticons
	}

	//It doesn't matter what method we use to filter as long as it's consistent,
	//so we use alphanumeric by emote code since there's no timestamp
	bestEmoticon := makorpcDashboard.Emote{}

	for _, emoticon := range emoticons {
		if bestEmoticon.Code == "" {
			bestEmoticon = emoticon
		} else {
			if bestEmoticon.Code > emoticon.Code {
				bestEmoticon = emoticon
			}
		}
	}

	return []makorpcDashboard.Emote{bestEmoticon}
}

func toEmoticonsByThreshold(emoticonGroups []makorpcDashboard.EmoteGroup, thresholdsByGroupID map[string]int64) (map[int64][]makorpcDashboard.Emote, error) {
	emoticonsByThreshold := make(map[int64][]makorpcDashboard.Emote)

	for _, emoticonGroup := range emoticonGroups {
		if emoticonGroup.Id == "" || len(emoticonGroup.Emotes) == 0 {
			continue
		}

		threshold, ok := thresholdsByGroupID[emoticonGroup.Id]
		if !ok {
			continue
		}

		if emoticonGroup.Emotes != nil {
			emoticonGroupEmoticons := emoticonGroup.Emotes

			var emoticons []makorpcDashboard.Emote
			for _, emoticonP := range emoticonGroupEmoticons {
				if emoticonP != nil {
					emoticon := *emoticonP
					emoticons = append(emoticons, emoticon)
				}
			}
			emoticonsByThreshold[threshold] = emoticons
		}
	}

	return emoticonsByThreshold, nil
}
