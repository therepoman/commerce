package badgetieremotes

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/payday/badgetiers/models"
	badgeusers_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/badgetiers/products/users"
	emote_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/emote"
	"code.justin.tv/commerce/payday/models/api"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestValidator_GetAllowedTiersMap(t *testing.T) {
	Convey("Given a validator", t, func() {
		ctx := context.Background()
		channelID := "456"

		badgeUsersFetcherMock := new(badgeusers_mock.Fetcher)

		testSaver := validator{
			BadgeUsersFetcher: badgeUsersFetcherMock,
		}

		badgeTier1000Setting := api.BadgeTierSetting{
			Threshold: 1000,
			Enabled:   pointers.BoolP(true),
		}

		Convey("and a request with no tiers", func() {
			tiers := make([]*api.BadgeTierSetting, 0)

			resp, err := testSaver.GetAllowedTiersMap(ctx, channelID, tiers)
			So(resp, ShouldBeNil)
			So(err, ShouldBeNil)
		})

		Convey("and a badge users fetcher that errors", func() {
			tiers := []*api.BadgeTierSetting{
				&badgeTier1000Setting,
			}

			badgeUsersFetcherMock.On("GetUnlockedUsersCountsByThresholds", mock.Anything, channelID, []int64{badgeTier1000Setting.Threshold}).Return(nil, errors.New("test error"))

			resp, err := testSaver.GetAllowedTiersMap(ctx, channelID, tiers)
			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("and a badge users fetcher that succeeds", func() {
			tiers := []*api.BadgeTierSetting{
				&badgeTier1000Setting,
			}

			usersCountMap := map[int64]int64{
				badgeTier1000Setting.Threshold: 1,
			}

			badgeUsersFetcherMock.On("GetUnlockedUsersCountsByThresholds", mock.Anything, channelID, []int64{badgeTier1000Setting.Threshold}).Return(usersCountMap, nil)

			resp, err := testSaver.GetAllowedTiersMap(ctx, channelID, tiers)
			So(resp, ShouldNotBeNil)
			So(resp[badgeTier1000Setting.Threshold], ShouldBeTrue)
			So(err, ShouldBeNil)
		})
	})
}

func TestValidator_ValidateUpload(t *testing.T) {
	Convey("Given a validator", t, func() {
		ctx := context.Background()
		channelID := "456"

		badgeUsersFetcherMock := new(badgeusers_mock.Fetcher)
		prefixManagerMock := new(emote_mock.IPrefixManager)

		testSaver := validator{
			BadgeUsersFetcher: badgeUsersFetcherMock,
			PrefixManager:     prefixManagerMock,
		}

		Convey("and a tier threshold that's not allowed", func() {
			allowedTiersMap := map[int64]bool{
				1: false,
			}

			validationPayload := models.EmoticonValidationPayload{
				ChannelID:       channelID,
				Threshold:       1,
				AllowedTiersMap: allowedTiersMap,
			}

			err := testSaver.ValidateUpload(ctx, validationPayload)
			validateSaverError(err, CreateEmoticonTierNotAllowedError)
		})

		Convey("and a tier that has all of its slots filled already", func() {
			allowedTiersMap := map[int64]bool{
				1000: true,
			}

			validationPayload := models.EmoticonValidationPayload{
				ChannelID:                         channelID,
				Threshold:                         1000,
				AllowedTiersMap:                   allowedTiersMap,
				NumberOfTierEmotesAlreadyUploaded: 1,
			}

			err := testSaver.ValidateUpload(ctx, validationPayload)
			validateSaverError(err, CreateEmoticonSlotsAlreadyFullError)
		})

		Convey("and a missing emote code suffix", func() {
			emoticonSetting := api.EmoticonSetting{
				Code: "tooSmart",
			}

			allowedTiersMap := map[int64]bool{
				1000: true,
			}

			emoticonCodes := strings.NewSet()
			emoticonCodes.Add("tooDumb")

			validationPayload := models.EmoticonValidationPayload{
				ChannelID:                         channelID,
				Threshold:                         1000,
				NumberOfTierEmotesAlreadyUploaded: 0,
				EmoticonSetting:                   emoticonSetting,
				AllowedTiersMap:                   allowedTiersMap,
			}

			err := testSaver.ValidateUpload(ctx, validationPayload)
			validateSaverError(err, CreateEmoticonValidationError)
		})

		Convey("and an emote code suffix that does not begin with an uppercase character", func() {
			emoticonSetting := api.EmoticonSetting{
				Code:       "toosmart",
				CodeSuffix: pointers.StringP("smart"),
			}

			allowedTiersMap := map[int64]bool{
				1000: true,
			}

			emoticonCodes := strings.NewSet()
			emoticonCodes.Add("tooDumb")

			validationPayload := models.EmoticonValidationPayload{
				ChannelID:                         channelID,
				Threshold:                         1000,
				NumberOfTierEmotesAlreadyUploaded: 0,
				EmoticonSetting:                   emoticonSetting,
				AllowedTiersMap:                   allowedTiersMap,
			}

			err := testSaver.ValidateUpload(ctx, validationPayload)
			validateSaverError(err, CreateEmoticonValidationError)
		})

		Convey("and an emote code suffix that does not match the suffix regex", func() {
			emoticonSetting := api.EmoticonSetting{
				Code:       "tooSmart!?",
				CodeSuffix: pointers.StringP("Smart!?"),
			}

			allowedTiersMap := map[int64]bool{
				1000: true,
			}

			emoticonCodes := strings.NewSet()
			emoticonCodes.Add("tooDumb")

			validationPayload := models.EmoticonValidationPayload{
				ChannelID:                         channelID,
				Threshold:                         1000,
				NumberOfTierEmotesAlreadyUploaded: 0,
				EmoticonSetting:                   emoticonSetting,
				AllowedTiersMap:                   allowedTiersMap,
			}

			err := testSaver.ValidateUpload(ctx, validationPayload)
			validateSaverError(err, CreateEmoticonValidationError)
		})

		Convey("and a failed PrefixManager call", func() {
			emoticonSetting := api.EmoticonSetting{
				Code:       "tooSmart",
				CodeSuffix: pointers.StringP("Smart"),
			}

			allowedTiersMap := map[int64]bool{
				1000: true,
			}

			emoticonCodes := strings.NewSet()
			emoticonCodes.Add("tooDumb")

			prefixManagerMock.On("GetEmotePrefix", mock.Anything, channelID).Return("", false, errors.New("test error"))

			validationPayload := models.EmoticonValidationPayload{
				ChannelID:                         channelID,
				Threshold:                         1000,
				NumberOfTierEmotesAlreadyUploaded: 0,
				EmoticonSetting:                   emoticonSetting,
				AllowedTiersMap:                   allowedTiersMap,
			}

			err := testSaver.ValidateUpload(ctx, validationPayload)
			validateSaverError(err, CreateEmoticonDownstreamError)
		})

		Convey("and a non-existent emote prefix", func() {
			emoticonSetting := api.EmoticonSetting{
				Code:       "tooSmart",
				CodeSuffix: pointers.StringP("Smart"),
			}

			allowedTiersMap := map[int64]bool{
				1000: true,
			}

			emoticonCodes := strings.NewSet()
			emoticonCodes.Add("tooDumb")

			prefixManagerMock.On("GetEmotePrefix", mock.Anything, channelID).Return("", false, nil)

			validationPayload := models.EmoticonValidationPayload{
				ChannelID:                         channelID,
				Threshold:                         1000,
				NumberOfTierEmotesAlreadyUploaded: 0,
				EmoticonSetting:                   emoticonSetting,
				AllowedTiersMap:                   allowedTiersMap,
			}

			err := testSaver.ValidateUpload(ctx, validationPayload)
			validateSaverError(err, CreateEmoticonValidationError)
		})

		Convey("and an invalid emote code pattern supplied", func() {
			emoticonSetting := api.EmoticonSetting{
				Code:       "tooSmart",
				CodeSuffix: pointers.StringP("Smart"),
			}

			allowedTiersMap := map[int64]bool{
				1000: true,
			}

			emoticonCodes := strings.NewSet()
			emoticonCodes.Add("tooDumb")

			prefixManagerMock.On("GetEmotePrefix", mock.Anything, channelID).Return("two", true, nil)

			validationPayload := models.EmoticonValidationPayload{
				ChannelID:                         channelID,
				Threshold:                         1000,
				NumberOfTierEmotesAlreadyUploaded: 0,
				EmoticonSetting:                   emoticonSetting,
				AllowedTiersMap:                   allowedTiersMap,
			}

			err := testSaver.ValidateUpload(ctx, validationPayload)
			validateSaverError(err, CreateEmoticonValidationError)
		})

		Convey("and a valid upload request", func() {
			emoticonSetting := api.EmoticonSetting{
				Code:       "tooSmart",
				CodeSuffix: pointers.StringP("Smart"),
			}

			allowedTiersMap := map[int64]bool{
				1000: true,
			}

			emoticonCodes := strings.NewSet()
			emoticonCodes.Add("tooDumb")

			prefixManagerMock.On("GetEmotePrefix", mock.Anything, channelID).Return("too", true, nil)

			validationPayload := models.EmoticonValidationPayload{
				ChannelID:                         channelID,
				Threshold:                         1000,
				NumberOfTierEmotesAlreadyUploaded: 0,
				EmoticonSetting:                   emoticonSetting,
				AllowedTiersMap:                   allowedTiersMap,
			}

			err := testSaver.ValidateUpload(ctx, validationPayload)
			So(err, ShouldBeNil)
		})
	})
}

func Test_GetAllowedTiersMap(t *testing.T) {
	Convey("Calling GetAllowedTiersMap", t, func() {
		Convey("with a nil tier", func() {
			tierMap := map[int64]*api.BadgeTier{
				1000: nil,
			}

			allowedTiers := GetAllowedTiersMap(tierMap)

			So(allowedTiers[1000], ShouldBeFalse)
		})

		Convey("with tiers below the allowed limit", func() {
			tierMap := map[int64]*api.BadgeTier{
				1: {
					Threshold:          1,
					UnlockedUsersCount: 10,
				},
				100: {
					Threshold:          100,
					UnlockedUsersCount: 5,
				},
			}

			allowedTiers := GetAllowedTiersMap(tierMap)

			So(allowedTiers[1], ShouldBeFalse)
			So(allowedTiers[100], ShouldBeFalse)
		})

		Convey("with tiers in the always-allowed range", func() {
			tierMap := map[int64]*api.BadgeTier{
				1000: {
					Threshold:          1000,
					UnlockedUsersCount: 3,
				},
				5000: {
					Threshold:          5000,
					UnlockedUsersCount: 0,
				},
				10000: {
					Threshold:          10000,
					UnlockedUsersCount: 0,
				},
			}

			allowedTiers := GetAllowedTiersMap(tierMap)

			So(allowedTiers[1000], ShouldBeTrue)
			So(allowedTiers[5000], ShouldBeTrue)
			So(allowedTiers[10000], ShouldBeTrue)
		})

		Convey("with tiers that have unlocked users above the always-allowed range", func() {
			tierMap := map[int64]*api.BadgeTier{
				100000: {
					Threshold:          100000,
					UnlockedUsersCount: 3,
				},
				200000: {
					Threshold:          200000,
					UnlockedUsersCount: 0,
				},
				300000: {
					Threshold:          300000,
					UnlockedUsersCount: 0,
				},
			}

			allowedTiers := GetAllowedTiersMap(tierMap)

			So(allowedTiers[100000], ShouldBeTrue)
			So(allowedTiers[200000], ShouldBeTrue)
			So(allowedTiers[300000], ShouldBeFalse)
		})

		Convey("with the first tier that has zero unlocked users above the always-allowed range", func() {
			tierMap := map[int64]*api.BadgeTier{
				10000: {
					Threshold:          10000,
					UnlockedUsersCount: 3,
				},
				25000: {
					Threshold:          25000,
					UnlockedUsersCount: 0,
				},
				50000: {
					Threshold:          50000,
					UnlockedUsersCount: 0,
				},
			}

			allowedTiers := GetAllowedTiersMap(tierMap)

			So(allowedTiers[10000], ShouldBeTrue)
			So(allowedTiers[25000], ShouldBeTrue)
			So(allowedTiers[50000], ShouldBeFalse)
		})

		Convey("when the first tier that has zero unlocked users is in the always-allowed range", func() {
			tierMap := map[int64]*api.BadgeTier{
				5000: {
					Threshold:          5000,
					UnlockedUsersCount: 3,
				},
				10000: {
					Threshold:          10000,
					UnlockedUsersCount: 0,
				},
				25000: {
					Threshold:          25000,
					UnlockedUsersCount: 0,
				},
			}

			allowedTiers := GetAllowedTiersMap(tierMap)

			So(allowedTiers[5000], ShouldBeTrue)
			So(allowedTiers[10000], ShouldBeTrue)
			So(allowedTiers[25000], ShouldBeFalse)
		})
	})
}
