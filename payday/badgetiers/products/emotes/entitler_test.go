package badgetieremotes

import (
	"context"
	"strconv"
	"testing"

	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/dynamo/badge_tier_emote_groupids"
	"code.justin.tv/commerce/payday/errors"
	badgetiers_logician_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/badgetiers/logician"
	mako_client_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/mako"
	badge_tier_emote_groupids_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo/badge_tier_emote_groupids"
	metrics_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/metrics"
	sns_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sns"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestEntitler_Entitle(t *testing.T) {
	Convey("Given an entitler", t, func() {
		ctx := context.Background()
		userID := "123"
		channelID := "456"
		bitsTotal := int64(100)
		transactionID := "test-transaction-id"

		var mockConfig = &config.Configuration{
			EmoticonBackfillSNSTopic: "foo",
		}

		logicianMock := new(badgetiers_logician_mock.Logician)
		statter := new(metrics_mock.Statter)
		snsClientMock := new(sns_mock.ISNSClient)
		badgeTierEmotesGroupIDDAOMock := new(badge_tier_emote_groupids_mock.BadgeTierEmoteGroupIDDAO)

		testEntitler := entitler{
			BadgeTiersLogician:  logicianMock,
			BadgeTierGroupIDDAO: badgeTierEmotesGroupIDDAOMock,
			Statter:             statter,
			SNSClient:           snsClientMock,
			Config:              mockConfig,
		}

		statter.On("Inc", mock.Anything, int64(1)).Return()

		Convey("when zero bits are used, we should return nil", func() {
			bitsUsed := int64(0)

			resp, err := testEntitler.Entitle(ctx, userID, channelID, bitsUsed, bitsTotal, transactionID)
			So(resp, ShouldBeNil)
			So(err, ShouldBeNil)
		})

		Convey("when negative bits are used, we should return error", func() {
			bitsUsed := int64(-1)

			resp, err := testEntitler.Entitle(ctx, userID, channelID, bitsUsed, bitsTotal, transactionID)
			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("when more than zero bits are used", func() {
			bitsUsed := int64(10)

			Convey("when the call to dynamo errors, we should error", func() {
				breachedThreshold := int64(1000)
				logicianMock.On("GetBreachedThresholds", bitsUsed, bitsTotal).Return([]int64{breachedThreshold})
				badgeTierEmotesGroupIDDAOMock.On("GetByChannelAndThreshold", mock.Anything, channelID, breachedThreshold).Return(&badge_tier_emote_groupids.BadgeTierEmoteGroupID{}, errors.New("ERROR"))

				resp, err := testEntitler.Entitle(ctx, userID, channelID, bitsUsed, bitsTotal, transactionID)
				So(resp, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})

			Convey("and there is a breached threshold", func() {
				Convey("for a tier that has entitlements ignored", func() {
					var breachedThresholds = []int64{1}

					logicianMock.On("GetBreachedThresholds", bitsUsed, bitsTotal).Return(breachedThresholds)

					resp, err := testEntitler.Entitle(ctx, userID, channelID, bitsUsed, bitsTotal, transactionID)
					So(resp, ShouldNotBeNil)
					So(len(*resp), ShouldBeZeroValue)
					So(err, ShouldBeNil)
				})

				breachedThreshold := int64(1000)
				groupID := badge_tier_emote_groupids.BadgeTierEmoteGroupID{
					GroupID:   "1234567890",
					ChannelID: channelID,
					Threshold: breachedThreshold,
				}

				logicianMock.On("GetBreachedThresholds", bitsUsed, bitsTotal).Return([]int64{breachedThreshold})
				badgeTierEmotesGroupIDDAOMock.On("GetByChannelAndThreshold", mock.Anything, channelID, breachedThreshold).Return(&groupID, nil)

				Convey("and we successfully post to SNS", func() {
					snsClientMock.On("PostToTopic", mock.Anything, mock.Anything).Return(nil)

					resp, err := testEntitler.Entitle(ctx, userID, channelID, bitsUsed, bitsTotal, transactionID)
					So(resp, ShouldNotBeNil)
					So(len(*resp), ShouldBeZeroValue)
					So(err, ShouldBeNil)
				})

				Convey("and we error posting to SNS, we should error", func() {
					snsClientMock.On("PostToTopic", mock.Anything, mock.Anything).Return(errors.New("ERROR"))

					resp, err := testEntitler.Entitle(ctx, userID, channelID, bitsUsed, bitsTotal, transactionID)
					So(resp, ShouldBeNil)
					So(err, ShouldNotBeNil)
				})
			})

			Convey("and there is no breached threshold", func() {
				logicianMock.On("GetBreachedThresholds", bitsUsed, bitsTotal).Return(nil)

				resp, err := testEntitler.Entitle(ctx, userID, channelID, bitsUsed, bitsTotal, transactionID)
				So(resp, ShouldBeNil)
				So(err, ShouldBeNil)
			})
		})
	})
}

func TestEntitler_EntitleAll(t *testing.T) {
	Convey("Given an entitler", t, func() {
		var mockConfig = &config.Configuration{
			EmoticonBackfillSNSTopic: "foo",
		}

		logicianMock := new(badgetiers_logician_mock.Logician)
		statter := new(metrics_mock.Statter)
		snsClientMock := new(sns_mock.ISNSClient)
		badgeTierEmotesGroupIDDAOMock := new(badge_tier_emote_groupids_mock.BadgeTierEmoteGroupIDDAO)
		makoMock := new(mako_client_mock.MakoClient)

		testEntitler := entitler{
			BadgeTiersLogician:  logicianMock,
			BadgeTierGroupIDDAO: badgeTierEmotesGroupIDDAOMock,
			Statter:             statter,
			SNSClient:           snsClientMock,
			Config:              mockConfig,
			Mako:                makoMock,
		}
		userID := "123"
		channelID := "456"
		bitsTotal := int64(123456)

		Convey("when no tier is unlocked for user", func() {
			logicianMock.On("GetBreachedThresholds", bitsTotal, bitsTotal).Return([]int64{})

			err := testEntitler.EntitleAll(context.Background(), userID, channelID, bitsTotal)
			So(err, ShouldBeNil)
			makoMock.AssertNumberOfCalls(t, "CreateEmoteGroupEntitlements", 0)
		})

		Convey("when some tiers are unlocked for user", func() {
			logicianMock.On("GetBreachedThresholds", bitsTotal, bitsTotal).Return([]int64{
				int64(100), // does not meet the threshold to have emotes.
				int64(1000),
				int64(5000),
			})

			groups := []*badge_tier_emote_groupids.BadgeTierEmoteGroupID{
				{
					ChannelID: channelID,
					Threshold: int64(1000),
					GroupID:   "1000",
				},
				{
					ChannelID: channelID,
					Threshold: int64(5000),
					GroupID:   "5000",
				},
			}
			badgeTierEmotesGroupIDDAOMock.On("BatchGetByChannelAndThresholds", mock.Anything, channelID, []int64{
				int64(1000),
				int64(5000),
			}).Return(groups, nil)

			makoMock.On("CreateEmoteGroupEntitlements", mock.Anything, []string{userID}, channelID, []string{"1000", "5000"}).Return(nil)

			err := testEntitler.EntitleAll(context.Background(), userID, channelID, bitsTotal)
			So(err, ShouldBeNil)
			makoMock.AssertCalled(t, "CreateEmoteGroupEntitlements", mock.Anything, []string{userID}, channelID, []string{"1000", "5000"})
		})

		Convey("when many tiers are unlocked for user", func() {
			// make a really long list of breached thresholds, such that we'd need to make multiple calls to Mako
			longListOfThresholds := []int64{}
			for i := 1000; i < 30000; i += 1000 {
				longListOfThresholds = append(longListOfThresholds, int64(i))
			}

			logicianMock.On("GetBreachedThresholds", bitsTotal, bitsTotal).Return(longListOfThresholds)

			longListOfEmoteGroupIDs := make([]*badge_tier_emote_groupids.BadgeTierEmoteGroupID, 0, len(longListOfThresholds))
			longListOfStringThresholds := make([]string, 0, len(longListOfThresholds))
			for _, threshold := range longListOfThresholds {
				longListOfEmoteGroupIDs = append(longListOfEmoteGroupIDs, &badge_tier_emote_groupids.BadgeTierEmoteGroupID{
					ChannelID: channelID,
					Threshold: threshold,
					GroupID:   strconv.Itoa(int(threshold)),
				})
				longListOfStringThresholds = append(longListOfStringThresholds, strconv.Itoa(int(threshold)))
			}
			badgeTierEmotesGroupIDDAOMock.On("BatchGetByChannelAndThresholds", mock.Anything, channelID, longListOfThresholds).Return(longListOfEmoteGroupIDs, nil)

			makoMock.On("CreateEmoteGroupEntitlements", mock.Anything, []string{userID}, channelID, longListOfStringThresholds[0:25]).Return(nil)
			makoMock.On("CreateEmoteGroupEntitlements", mock.Anything, []string{userID}, channelID, longListOfStringThresholds[25:]).Return(nil)

			err := testEntitler.EntitleAll(context.Background(), userID, channelID, bitsTotal)
			So(err, ShouldBeNil)
			makoMock.AssertExpectations(t)
		})

		Convey("returns an error when", func() {
			logicianMock.On("GetBreachedThresholds", bitsTotal, bitsTotal).Return([]int64{int64(1000)})

			Convey("user ID is blank", func() {
				userID = ""
			})

			Convey("channel ID is blank", func() {
				channelID = ""
			})

			Convey("badge tier group DAO fails to get groups", func() {
				badgeTierEmotesGroupIDDAOMock.On("BatchGetByChannelAndThresholds", mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("nah"))
			})

			Convey("mako fails to entitle emote groups", func() {
				groups := []*badge_tier_emote_groupids.BadgeTierEmoteGroupID{
					{
						ChannelID: channelID,
						Threshold: int64(1000),
						GroupID:   "1000",
					},
				}
				badgeTierEmotesGroupIDDAOMock.On("BatchGetByChannelAndThresholds", mock.Anything, channelID, []int64{
					int64(1000),
				}).Return(groups, nil)
				makoMock.On("CreateEmoteGroupEntitlements", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(errors.New("can't entitle"))
			})

			err := testEntitler.EntitleAll(context.Background(), userID, channelID, bitsTotal)
			So(err, ShouldNotBeNil)
		})
	})
}
