package badgetieremotes

import (
	"context"
	"testing"
	"time"

	mako_dashboard "code.justin.tv/commerce/mako/twirp/dashboard"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/dynamo/badge_tier_emote_groupids"
	"code.justin.tv/commerce/payday/errors"
	badgetieremotes_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/badgetiers/products/emotes"
	mako_client_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/mako"
	sns_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sns"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestAssigner_AssignEmoteToBitsTier(t *testing.T) {
	Convey("Given an assigner", t, func() {
		makoClientMock := new(mako_client_mock.MakoClient)
		validatorMock := new(badgetieremotes_mock.Validator)
		logicianMock := new(badgetieremotes_mock.Logician)
		snsClientMock := new(sns_mock.ISNSClient)
		configMock := &config.Configuration{
			EmoticonBackfillSNSTopic: "test_topic",
		}

		assigner := assigner{
			Validator:  validatorMock,
			Logician:   logicianMock,
			MakoClient: makoClientMock,
			SNSClient:  snsClientMock,
			Config:     configMock,
		}

		channelID := "1234"
		emoteID := "someemoteid"
		groupID := "somegroupid"
		bitsTierThreshold := int64(1000)
		dummyError := errors.New("dummy error")

		Convey("When the validator GetAllowedTiersMap errors", func() {
			validatorMock.On("GetAllowedTiersMap", mock.Anything, channelID, mock.Anything).Return(nil, dummyError)

			err := assigner.AssignEmoteToBitsTier(context.Background(), channelID, emoteID, bitsTierThreshold)

			Convey("we should return an error", func() {
				So(err, ShouldNotBeNil)
			})
		})

		Convey("When the tier does not allow emotes", func() {
			validatorMock.On("GetAllowedTiersMap", mock.Anything, channelID, mock.Anything).Return(map[int64]bool{bitsTierThreshold: false}, nil)

			err := assigner.AssignEmoteToBitsTier(context.Background(), channelID, emoteID, bitsTierThreshold)

			Convey("we should return an error", func() {
				So(err, ShouldEqual, EmotesNotAllowedInTierAssignError)
			})
		})

		Convey("When the logician fails", func() {
			validatorMock.On("GetAllowedTiersMap", mock.Anything, channelID, mock.Anything).Return(map[int64]bool{bitsTierThreshold: true}, nil)
			logicianMock.On("GetAndStoreGroupIDForBadgeTier", mock.Anything, channelID, bitsTierThreshold).Return(nil, false, dummyError)

			err := assigner.AssignEmoteToBitsTier(context.Background(), channelID, emoteID, bitsTierThreshold)

			Convey("we should return an error", func() {
				So(err, ShouldNotBeNil)
			})
		})

		Convey("When the logician returns a nil groupID mapping", func() {
			validatorMock.On("GetAllowedTiersMap", mock.Anything, channelID, mock.Anything).Return(map[int64]bool{bitsTierThreshold: true}, nil)
			logicianMock.On("GetAndStoreGroupIDForBadgeTier", mock.Anything, channelID, bitsTierThreshold).Return(nil, false, nil)

			err := assigner.AssignEmoteToBitsTier(context.Background(), channelID, emoteID, bitsTierThreshold)

			Convey("we should return an error", func() {
				So(err, ShouldNotBeNil)
			})
		})

		Convey("When Mako DashboardGetEmotesByGroups errors", func() {
			validatorMock.On("GetAllowedTiersMap", mock.Anything, channelID, mock.Anything).Return(map[int64]bool{bitsTierThreshold: true}, nil)
			logicianMock.On("GetAndStoreGroupIDForBadgeTier", mock.Anything, channelID, bitsTierThreshold).Return(&badge_tier_emote_groupids.BadgeTierEmoteGroupID{GroupID: groupID}, false, nil)
			makoClientMock.On("DashboardGetEmotesByGroups", mock.Anything, []string{groupID}, []mako_dashboard.EmoteState{mako_dashboard.EmoteState_active, mako_dashboard.EmoteState_pending}).Return(nil, dummyError)

			err := assigner.AssignEmoteToBitsTier(context.Background(), channelID, emoteID, bitsTierThreshold)

			Convey("we should return an error", func() {
				So(err, ShouldNotBeNil)
			})
		})

		Convey("When the tier is already full of emotes", func() {
			validatorMock.On("GetAllowedTiersMap", mock.Anything, channelID, mock.Anything).Return(map[int64]bool{bitsTierThreshold: true}, nil)
			logicianMock.On("GetAndStoreGroupIDForBadgeTier", mock.Anything, channelID, bitsTierThreshold).Return(&badge_tier_emote_groupids.BadgeTierEmoteGroupID{GroupID: groupID}, false, nil)
			makoClientMock.On("DashboardGetEmotesByGroups", mock.Anything, []string{groupID}, []mako_dashboard.EmoteState{mako_dashboard.EmoteState_active, mako_dashboard.EmoteState_pending}).Return([]mako_dashboard.EmoteGroup{
				{
					Emotes: []*mako_dashboard.Emote{
						{
							Id: "anotheremote",
						},
					},
				},
			}, nil)

			err := assigner.AssignEmoteToBitsTier(context.Background(), channelID, emoteID, bitsTierThreshold)

			Convey("we should return an error", func() {
				So(err, ShouldEqual, TierAlreadyFullAssignError)
			})
		})

		Convey("When Mako AssignEmoteToGroup errors", func() {
			validatorMock.On("GetAllowedTiersMap", mock.Anything, channelID, mock.Anything).Return(map[int64]bool{bitsTierThreshold: true}, nil)
			logicianMock.On("GetAndStoreGroupIDForBadgeTier", mock.Anything, channelID, bitsTierThreshold).Return(&badge_tier_emote_groupids.BadgeTierEmoteGroupID{GroupID: groupID}, false, nil)
			makoClientMock.On("DashboardGetEmotesByGroups", mock.Anything, []string{groupID}, []mako_dashboard.EmoteState{mako_dashboard.EmoteState_active, mako_dashboard.EmoteState_pending}).Return([]mako_dashboard.EmoteGroup{}, nil)
			makoClientMock.On("AssignEmoteToGroup", mock.Anything, channelID, emoteID, groupID).Return(mako_dashboard.Emote{}, dummyError)

			err := assigner.AssignEmoteToBitsTier(context.Background(), channelID, emoteID, bitsTierThreshold)

			Convey("we should return an error", func() {
				So(err, ShouldNotBeNil)
			})
		})

		Convey("When Mako AssignEmoteToGroup succeeds", func() {
			validatorMock.On("GetAllowedTiersMap", mock.Anything, channelID, mock.Anything).Return(map[int64]bool{bitsTierThreshold: true}, nil)
			logicianMock.On("GetAndStoreGroupIDForBadgeTier", mock.Anything, channelID, bitsTierThreshold).Return(&badge_tier_emote_groupids.BadgeTierEmoteGroupID{GroupID: groupID}, false, nil)
			makoClientMock.On("DashboardGetEmotesByGroups", mock.Anything, []string{groupID}, []mako_dashboard.EmoteState{mako_dashboard.EmoteState_active, mako_dashboard.EmoteState_pending}).Return([]mako_dashboard.EmoteGroup{}, nil)
			makoClientMock.On("AssignEmoteToGroup", mock.Anything, channelID, emoteID, groupID).Return(mako_dashboard.Emote{}, nil)
			snsClientMock.On("PostToTopic", configMock.EmoticonBackfillSNSTopic, mock.Anything).Return(nil)

			err := assigner.AssignEmoteToBitsTier(context.Background(), channelID, emoteID, bitsTierThreshold)

			Convey("we should return a successful response", func() {
				So(err, ShouldBeNil)

				// Assert that the backfill is enqueued
				time.Sleep(100 * time.Millisecond)
				snsClientMock.AssertCalled(t, "PostToTopic", configMock.EmoticonBackfillSNSTopic, mock.Anything)
			})
		})
	})
}
