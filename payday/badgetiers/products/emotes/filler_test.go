package badgetieremotes

import (
	"context"
	"errors"
	"testing"

	mako_dashboard "code.justin.tv/commerce/mako/twirp/dashboard"
	"code.justin.tv/commerce/payday/dynamo/badge_tier_emote_groupids"
	mako_client_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/mako"
	badge_tier_emote_groupids_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo/badge_tier_emote_groupids"
	"code.justin.tv/commerce/payday/models/api"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestFiller_PopulateWithBadgeTierEmotes(t *testing.T) {
	Convey("Given a filler", t, func() {
		ctx := context.Background()
		channelID := "123"
		tier10000 := int64(10000)
		groupID10000 := "1234567890"
		tooSmart := mako_dashboard.Emote{
			Id:      "123",
			Code:    "tooSmart",
			GroupId: groupID10000,
			State:   mako_dashboard.EmoteState_active,
			Domain:  mako_dashboard.Domain_emotes,
			OwnerId: "456",
		}
		tooCool := mako_dashboard.Emote{
			Id:      "234",
			Code:    "tooCool",
			GroupId: groupID10000,
			State:   mako_dashboard.EmoteState_active,
			Domain:  mako_dashboard.Domain_emotes,
			OwnerId: "456",
		}

		makoClientMock := new(mako_client_mock.MakoClient)
		badgeTierEmotesGroupIDDAOMock := new(badge_tier_emote_groupids_mock.BadgeTierEmoteGroupIDDAO)

		testFiller := filler{
			MakoClient:          makoClientMock,
			BadgeTierGroupIDDAO: badgeTierEmotesGroupIDDAOMock,
		}

		Convey("and a non-nil tier map", func() {
			tierMap := map[int64]*api.BadgeTier{
				tier10000: {
					Threshold: tier10000,
					ChannelId: channelID,
					Enabled:   true,
				},
			}

			Convey("and a badge tier group id dao that errors", func() {
				badgeTierEmotesGroupIDDAOMock.On("Get", mock.Anything, mock.Anything).Return(nil, errors.New("dummy dao error"))

				resp, err := testFiller.PopulateWithBadgeTierEmoticons(ctx, channelID, tierMap)

				So(resp, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})

			Convey("and a badge tier group id dao that succeeds", func() {
				dynamoEmoteGroup := badge_tier_emote_groupids.BadgeTierEmoteGroupID{
					ChannelID: channelID,
					Threshold: tier10000,
					GroupID:   groupID10000,
				}
				badgeTierEmotesGroupIDDAOMock.On("Get", mock.Anything, mock.Anything).Return([]*badge_tier_emote_groupids.BadgeTierEmoteGroupID{&dynamoEmoteGroup}, nil)

				Convey("and a mako client that succeeds", func() {
					Convey("and returns emoticon groups", func() {
						Convey("with no ID", func() {
							emoticonGroups := []mako_dashboard.EmoteGroup{
								{},
							}

							makoClientMock.On("DashboardGetEmotesByGroups", mock.Anything, mock.Anything, mock.Anything).Return(emoticonGroups, nil)

							resp, err := testFiller.PopulateWithBadgeTierEmoticons(ctx, channelID, tierMap)
							So(resp, ShouldNotBeNil)
							So(resp[tier10000], ShouldNotBeNil)
							So(resp[tier10000].EmoticonUploadConfigurations, ShouldBeNil)
							So(resp[tier10000].Emoticons, ShouldBeNil)
							So(err, ShouldBeNil)
						})

						Convey("with an ID that is correctly formed", func() {
							Convey("and multiple emoticons", func() {
								emoticonGroups := []mako_dashboard.EmoteGroup{
									{
										Id:     groupID10000,
										Emotes: []*mako_dashboard.Emote{&tooSmart, &tooCool},
									},
								}

								makoClientMock.On("DashboardGetEmotesByGroups", mock.Anything, mock.Anything, mock.Anything).Return(emoticonGroups, nil)

								resp, err := testFiller.PopulateWithBadgeTierEmoticons(ctx, channelID, tierMap)
								So(resp, ShouldNotBeNil)
								So(resp[tier10000], ShouldNotBeNil)
								So(resp[tier10000].Emoticons, ShouldNotBeNil)
								So(len(*resp[tier10000].Emoticons), ShouldEqual, 1)

								emoticons := *resp[tier10000].Emoticons
								So(emoticons[0].Code, ShouldEqual, tooCool.Code)

								So(err, ShouldBeNil)
							})

							Convey("and no emoticons", func() {
								emoticonGroups := []mako_dashboard.EmoteGroup{
									{
										Id:     groupID10000,
										Emotes: []*mako_dashboard.Emote{},
									},
								}

								makoClientMock.On("DashboardGetEmotesByGroups", mock.Anything, mock.Anything, mock.Anything).Return(emoticonGroups, nil)

								resp, err := testFiller.PopulateWithBadgeTierEmoticons(ctx, channelID, tierMap)
								So(resp, ShouldNotBeNil)
								So(resp[tier10000], ShouldNotBeNil)
								So(resp[tier10000].Emoticons, ShouldBeNil)
								So(err, ShouldBeNil)
							})
						})
					})

					Convey("and returns no emoticon groups", func() {
						var emoticonGroups []mako_dashboard.EmoteGroup

						makoClientMock.On("DashboardGetEmotesByGroups", mock.Anything, mock.Anything, mock.Anything).Return(emoticonGroups, nil)

						resp, err := testFiller.PopulateWithBadgeTierEmoticons(ctx, channelID, tierMap)
						So(resp, ShouldNotBeNil)
						So(resp[tier10000], ShouldNotBeNil)
						So(resp[tier10000].Emoticons, ShouldBeNil)
						So(err, ShouldBeNil)
					})
				})

				Convey("and a mako client that errors", func() {
					makoClientMock.On("DashboardGetEmotesByGroups", mock.Anything, mock.Anything, mock.Anything).Return([]mako_dashboard.EmoteGroup{}, errors.New("test error"))

					resp, err := testFiller.PopulateWithBadgeTierEmoticons(ctx, channelID, tierMap)
					So(resp, ShouldBeNil)
					So(err, ShouldNotBeNil)
				})
			})
		})

		Convey("and a request for tiers that are disallowed", func() {
			Convey("due to being below the lower limit", func() {
				tier1 := int64(1)
				tier100 := int64(100)
				tierMap := map[int64]*api.BadgeTier{
					tier1: {
						Threshold: tier1,
						ChannelId: channelID,
						Enabled:   true,
					},
					tier100: {
						Threshold: tier100,
						ChannelId: channelID,
						Enabled:   true,
					},
				}
				badgeTierEmotesGroupIDDAOMock.On("Get", mock.Anything, mock.Anything).Return(nil, nil)
				makoClientMock.On("DashboardGetEmotesByGroups", mock.Anything, []string{}, mock.Anything).Return(nil, nil)

				resp, err := testFiller.PopulateWithBadgeTierEmoticons(ctx, channelID, tierMap)
				So(resp, ShouldNotBeNil)
				So(resp[tier1], ShouldNotBeNil)
				So(resp[tier1].EmoticonUploadConfigurations, ShouldBeNil)
				So(resp[tier1].Emoticons, ShouldBeNil)
				So(resp[tier100], ShouldNotBeNil)
				So(resp[tier100].EmoticonUploadConfigurations, ShouldBeNil)
				So(resp[tier100].Emoticons, ShouldBeNil)
				So(resp[tier100].CanUploadEmotesForTier, ShouldBeFalse)
				So(err, ShouldBeNil)
			})

			Convey("due to being above the upper limit", func() {
				tier5000 := int64(5000)
				tier10000 := int64(10000)
				tier25000 := int64(25000)
				groupID5000 := "1234565000"
				tierMap := map[int64]*api.BadgeTier{
					tier5000: {
						Threshold:          tier5000,
						ChannelId:          channelID,
						Enabled:            true,
						UnlockedUsersCount: 1,
					},
					tier10000: {
						Threshold:          tier10000,
						ChannelId:          channelID,
						Enabled:            true,
						UnlockedUsersCount: 0,
					},
					tier25000: {
						Threshold:          tier25000,
						ChannelId:          channelID,
						Enabled:            true,
						UnlockedUsersCount: 0,
					},
				}

				emoticonGroups := []mako_dashboard.EmoteGroup{
					{
						Id:     groupID10000,
						Emotes: []*mako_dashboard.Emote{&tooSmart},
					},
				}
				dynamoEmoteGroup5000 := badge_tier_emote_groupids.BadgeTierEmoteGroupID{
					ChannelID: channelID,
					Threshold: tier5000,
					GroupID:   groupID5000,
				}
				dynamoEmoteGroup10000 := badge_tier_emote_groupids.BadgeTierEmoteGroupID{
					ChannelID: channelID,
					Threshold: tier10000,
					GroupID:   groupID10000,
				}
				badgeTierEmotesGroupIDDAOMock.On("Get", mock.Anything, mock.Anything).Return([]*badge_tier_emote_groupids.BadgeTierEmoteGroupID{&dynamoEmoteGroup5000, &dynamoEmoteGroup10000}, nil)
				makoClientMock.On("DashboardGetEmotesByGroups", mock.Anything, mock.Anything, mock.Anything).Return(emoticonGroups, nil)

				resp, err := testFiller.PopulateWithBadgeTierEmoticons(ctx, channelID, tierMap)
				So(resp, ShouldNotBeNil)
				So(resp[tier5000], ShouldNotBeNil)
				So(resp[tier5000].EmoticonUploadConfigurations, ShouldBeNil)
				So(resp[tier5000].Emoticons, ShouldBeNil)
				So(resp[tier5000].CanUploadEmotesForTier, ShouldBeTrue)
				So(resp[tier10000], ShouldNotBeNil)
				So(resp[tier10000].EmoticonUploadConfigurations, ShouldBeNil)
				So(resp[tier10000].Emoticons, ShouldNotBeNil)
				So(len(*resp[tier10000].Emoticons), ShouldEqual, 1)
				So(resp[tier10000].CanUploadEmotesForTier, ShouldBeTrue)
				So(resp[tier25000], ShouldNotBeNil)
				So(resp[tier25000].EmoticonUploadConfigurations, ShouldBeNil)
				So(resp[tier25000].Emoticons, ShouldBeNil)
				So(resp[tier25000].CanUploadEmotesForTier, ShouldBeFalse)
				So(err, ShouldBeNil)
			})
		})

		Convey("and a nil tier map", func() {
			resp, err := testFiller.PopulateWithBadgeTierEmoticons(ctx, channelID, nil)
			So(resp, ShouldBeNil)
			So(err, ShouldBeNil)
		})
	})
}

func TestFiller_PopulateWithUploadingBadgeTierEmoticons(t *testing.T) {
	Convey("Given a filler", t, func() {
		ctx := context.Background()
		channelID := "123"
		tier1 := int64(1)

		tooSpicyBeforeImageData := api.EmoticonUploadConfiguration{
			Code:    "tooSpicy",
			GroupID: "BitsBadgeTier-6-1",
			OwnerID: "456",
		}

		tooSpicyImageData1X := api.ImageUploadConfig{
			UploadID:         "upload28ID",
			EmoteSignedS3URL: "https://s3.biz/signed/url/28",
			ImageID:          "image28ID",
		}

		tooSpicyImageData2X := api.ImageUploadConfig{
			UploadID:         "upload56ID",
			EmoteSignedS3URL: "https://s3.biz/signed/url/56",
			ImageID:          "image56ID",
		}

		tooSpicyImageData4X := api.ImageUploadConfig{
			UploadID:         "upload112ID",
			EmoteSignedS3URL: "https://s3.biz/signed/url/112",
			ImageID:          "image112ID",
		}

		makoClientMock := new(mako_client_mock.MakoClient)

		testFiller := filler{
			MakoClient: makoClientMock,
		}

		Convey("and a non-nil tier map", func() {
			tierMap := map[int64]*api.BadgeTier{
				tier1: {
					Threshold: tier1,
					ChannelId: channelID,
					Enabled:   true,
				},
			}

			emoticonDataMap := map[int64][]api.EmoticonUploadConfiguration{
				tier1: {
					api.EmoticonUploadConfiguration{
						Code:                tooSpicyBeforeImageData.Code,
						ImageUploadConfig1X: tooSpicyImageData1X,
						ImageUploadConfig2X: tooSpicyImageData2X,
						ImageUploadConfig4X: tooSpicyImageData4X,
					},
				},
			}

			resp, err := testFiller.PopulateWithUploadingBadgeTierEmoticons(ctx, tierMap, emoticonDataMap)
			So(resp, ShouldNotBeNil)
			So(resp[tier1], ShouldNotBeNil)
			So(resp[tier1].EmoticonUploadConfigurations, ShouldNotBeNil)

			emoticonData := *resp[tier1].EmoticonUploadConfigurations

			So(emoticonData[0], ShouldNotBeNil)
			So(emoticonData[0].Code, ShouldEqual, tooSpicyBeforeImageData.Code)
			So(emoticonData[0].ImageUploadConfig1X, ShouldNotBeNil)
			So(emoticonData[0].ImageUploadConfig2X, ShouldNotBeNil)
			So(emoticonData[0].ImageUploadConfig4X, ShouldNotBeNil)
			So(err, ShouldBeNil)
		})
	})
}
