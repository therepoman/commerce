package badgetieremotes

import (
	"context"
	"errors"
	"fmt"
	"regexp"
	"strings"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/badgetiers/models"
	badgeusers "code.justin.tv/commerce/payday/badgetiers/products/users"
	"code.justin.tv/commerce/payday/badgetiers/utils"
	"code.justin.tv/commerce/payday/clients/mako"
	"code.justin.tv/commerce/payday/emote"
	"code.justin.tv/commerce/payday/models/api"
)

const (
	allowedEmoticonsPerBadgeTier = 1
	lowestAllowedTierForEmotes   = 1000
	highestAutoUnlockedTier      = 10000
)

// Obtained from Mako's regex: https://git-aws.internal.justin.tv/commerce/mako/blob/master/clients/emoticonsmanager/emoticons_manager.go#L51
var SuffixRegex = regexp.MustCompile(`^[0-9a-zA-Z]+$`)

type Validator interface {
	GetAllowedTiersMap(ctx context.Context, channelID string, tiers []*api.BadgeTierSetting) (map[int64]bool, error)
	ValidateUpload(ctx context.Context, validationPayload models.EmoticonValidationPayload) error
}

type validator struct {
	BadgeUsersFetcher badgeusers.Fetcher   `inject:""`
	PrefixManager     emote.IPrefixManager `inject:""`
	MakoClient        mako.MakoClient      `inject:""`
}

func NewValidator() Validator {
	return &validator{}
}

func (v *validator) GetAllowedTiersMap(ctx context.Context, channelID string, tiers []*api.BadgeTierSetting) (map[int64]bool, error) {
	var allowedTiersMap map[int64]bool

	var tierThresholds []int64
	for _, tier := range tiers {
		if tier != nil {
			tierThresholds = append(tierThresholds, tier.Threshold)
		}
	}

	if len(tierThresholds) == 0 {
		return nil, nil
	}

	userCountsByThresholds, err := v.BadgeUsersFetcher.GetUnlockedUsersCountsByThresholds(ctx, channelID, tierThresholds)
	if err != nil {
		log.WithField("channelID", channelID).WithError(err).Error("error calling badge users fetcher for unlocked badge user counts")
		return nil, NewSaverError(err, GetUnlockedBadgeUserCountsError)
	}

	tierMap := map[int64]*api.BadgeTier{}
	for threshold, userCount := range userCountsByThresholds {
		tierMap[threshold] = &api.BadgeTier{
			Threshold:          threshold,
			UnlockedUsersCount: userCount,
		}
	}

	allowedTiersMap = GetAllowedTiersMap(tierMap)

	return allowedTiersMap, nil
}

func (v *validator) ValidateUpload(ctx context.Context, validationPayload models.EmoticonValidationPayload) error {
	err := validateEmoticonsAllowedForTier(validationPayload.Threshold, validationPayload.AllowedTiersMap)
	if err != nil {
		return err
	}

	err = validateEmoticonSlotOpen(validationPayload.NumberOfTierEmotesAlreadyUploaded, validationPayload.CachedNumberOfUploadedEmoticons)
	if err != nil {
		return err
	}

	err = v.validateNewEmoticonCode(ctx, validationPayload.ChannelID, validationPayload.EmoticonSetting.Code, validationPayload.EmoticonSetting.CodeSuffix)
	if err != nil {
		return err
	}

	return nil
}

func (v *validator) validateNewEmoticonCode(ctx context.Context, channelID string, code string, suffix *string) error {
	// Check that suffix is provided
	if suffix == nil {
		return NewSaverError(errors.New("emote code suffix is required"), CreateEmoticonValidationError)
	}

	// Check that suffix begins with an uppercase character
	if (*suffix)[0] != strings.ToUpper(*suffix)[0] {
		return NewSaverError(errors.New("emote code suffix must begin with an uppercase character"), CreateEmoticonValidationError)
	}

	// Check that suffix matches the suffix regex
	if !SuffixRegex.MatchString(*suffix) {
		return NewSaverError(errors.New("emote code suffix has an invalid format"), CreateEmoticonValidationError)
	}

	// Check that a prefix is available for this channel
	emotePrefix, hasEmotePrefix, err := v.PrefixManager.GetEmotePrefix(ctx, channelID)
	if err != nil {
		msg := "error calling prefix manager to get emote prefix"
		log.WithField("channelID", channelID).WithError(err).Error(msg)
		return NewSaverError(errors.New(msg), CreateEmoticonDownstreamError)
	} else if !hasEmotePrefix {
		return NewSaverError(errors.New("channel does not have an emote prefix"), CreateEmoticonValidationError)
	}

	// Check that the code is the combination of prefix and suffix
	if code != fmt.Sprintf("%s%s", emotePrefix, *suffix) {
		return NewSaverError(errors.New("emote code has invalid pattern"), CreateEmoticonValidationError)
	}

	return nil
}

func GetAllowedTiersMap(tierMap map[int64]*api.BadgeTier) map[int64]bool {
	allowedTiersMap := map[int64]bool{}

	isFirstTierWithZeroUsers := true

	// We need the thresholds sorted for the second product requirement below
	sortedThresholds := make([]int64, len(tierMap))
	for threshold := range tierMap {
		sortedThresholds = append(sortedThresholds, threshold)
	}
	utils.SortInt64s(sortedThresholds)

	for _, threshold := range sortedThresholds {
		tier := tierMap[threshold]

		if tier == nil {
			allowedTiersMap[threshold] = false
			continue
		}

		// Product requires that 1 and 100 tiers do not have emoticons
		if threshold < lowestAllowedTierForEmotes {
			allowedTiersMap[threshold] = false
			continue
		}

		if tier.UnlockedUsersCount == 0 {
			// Product requires that creators only be able to upload one more emote than they have "unlocked tiers" on their
			// channel. "Unlocked tiers" are ones which have at least one unlocked user. The first tier with zero users
			// can upload emoticon(s) for this feature, but higher tiers cannot. This starts applying when tier 10000 gains
			// its first unlocked user.
			if isFirstTierWithZeroUsers {
				isFirstTierWithZeroUsers = false
				allowedTiersMap[threshold] = true
				continue
			}

			// Product requires that creators always be able to upload emotes for the 1000, 5000, and 10000 tiers
			// regardless of the number of users who have unlocked that badge tier
			if threshold >= lowestAllowedTierForEmotes && threshold <= highestAutoUnlockedTier {
				allowedTiersMap[threshold] = true
				continue
			}

			allowedTiersMap[threshold] = false
			continue
		}

		allowedTiersMap[threshold] = true
	}

	return allowedTiersMap
}

func validateEmoticonsAllowedForTier(threshold int64, allowedTiersMap map[int64]bool) error {
	if !allowedTiersMap[threshold] {
		return NewSaverError(errors.New("this tier is not yet allowed to have an emoticon uploaded"), CreateEmoticonTierNotAllowedError)
	}

	return nil
}

func validateEmoticonSlotOpen(emoteNumberForTier int64, cachedNumberOfEmoticons *int64) error {
	//check cached value from redis first, in case there is an emoticon upload in progress
	if cachedNumberOfEmoticons != nil && *cachedNumberOfEmoticons >= allowedEmoticonsPerBadgeTier {
		return NewSaverError(errors.New("emoticon slots are all filled for this tier"), CreateEmoticonSlotsAlreadyFullError)
	}

	//fallback to the emoticons that are already uploaded in mako if there's no cached value in redis
	if emoteNumberForTier >= allowedEmoticonsPerBadgeTier {
		return NewSaverError(errors.New("emoticon slots are all filled for this tier"), CreateEmoticonSlotsAlreadyFullError)
	}

	return nil
}

func validateEmoticonUploadConfigurationData(emoticonUploadConfiguration api.EmoticonUploadConfiguration) error {
	msg := "No image upload config %s found for emoticon upload configuration"

	if emoticonUploadConfiguration.ImageUploadConfig1X.ImageID == "" {
		errMsg := fmt.Sprintf(msg, "1x")
		log.WithField("emoticonUploadConfiguration", emoticonUploadConfiguration).Error(errMsg)
		return errors.New(errMsg)
	}

	if emoticonUploadConfiguration.ImageUploadConfig2X.ImageID == "" {
		errMsg := fmt.Sprintf(msg, "2x")
		log.WithField("emoticonUploadConfiguration", emoticonUploadConfiguration).Error(errMsg)
		return errors.New(errMsg)
	}

	if emoticonUploadConfiguration.ImageUploadConfig4X.ImageID == "" {
		errMsg := fmt.Sprintf(msg, "4x")
		log.WithField("emoticonUploadConfiguration", emoticonUploadConfiguration).Error(errMsg)
		return errors.New(errMsg)
	}

	return nil
}
