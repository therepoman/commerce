// +build !race
// the race condition check doesn't like me setting cache data async

package badgetieremotes

import (
	"context"
	"fmt"
	"testing"
	"time"

	"code.justin.tv/commerce/gogogadget/pointers"
	makorpc "code.justin.tv/commerce/mako/twirp"
	"code.justin.tv/commerce/payday/badgetiers"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/dynamo/badge_tier_emote_groupids"
	"code.justin.tv/commerce/payday/errors"
	cache_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/cache"
	channel_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/cache/channel"
	mako_client_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/mako"
	badge_tier_emote_groupids_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo/badge_tier_emote_groupids"
	leaderboard_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/leaderboard"
	metrics_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/metrics"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	d "github.com/guregu/dynamo"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestLogician_GetAndStoreGroupIDForBadgeTier(t *testing.T) {
	Convey("Given a logician", t, func() {
		ctx := context.Background()
		testChannelID := "267893713"
		testThreshold := int64(1000)
		testGroupID := "1234567890"
		newGroupID := "1234569999"

		testGroupIDMapping := &badge_tier_emote_groupids.BadgeTierEmoteGroupID{
			Threshold: testThreshold,
			ChannelID: testChannelID,
			GroupID:   testGroupID,
		}

		newGroupIDMapping := &badge_tier_emote_groupids.BadgeTierEmoteGroupID{
			Threshold: testThreshold,
			ChannelID: testChannelID,
			GroupID:   newGroupID,
		}

		makoClientMock := new(mako_client_mock.MakoClient)
		badgeTierEmotesGroupIDDAOMock := new(badge_tier_emote_groupids_mock.BadgeTierEmoteGroupIDDAO)

		testLogician := logician{
			Mako:                makoClientMock,
			BadgeTierGroupIDDAO: badgeTierEmotesGroupIDDAOMock,
		}

		Convey("Given tier group id dao returns error then return error", func() {
			badgeTierEmotesGroupIDDAOMock.On("GetByChannelAndThreshold", ctx, testChannelID, testThreshold).Return(nil, errors.New("dummy dao error"))

			groupID, isNew, err := testLogician.GetAndStoreGroupIDForBadgeTier(ctx, testChannelID, testThreshold)

			So(groupID, ShouldBeNil)
			So(isNew, ShouldBeFalse)
			So(err, ShouldNotBeNil)
			So(err.Error(), ShouldContainSubstring, "dummy dao error")
		})

		Convey("Given tier group id dao returns group id then return group id and false", func() {
			badgeTierEmotesGroupIDDAOMock.On("GetByChannelAndThreshold", ctx, testChannelID, testThreshold).Return(testGroupIDMapping, nil)

			groupID, isNew, err := testLogician.GetAndStoreGroupIDForBadgeTier(ctx, testChannelID, testThreshold)

			So(groupID, ShouldResemble, testGroupIDMapping)
			So(isNew, ShouldBeFalse)
			So(err, ShouldBeNil)
		})

		Convey("Given badge tier requested is not found in dynamo", func() {
			badgeTierEmotesGroupIDDAOMock.On("GetByChannelAndThreshold", ctx, testChannelID, testThreshold).Return(nil, nil).Once()

			Convey("Given mako create emote group returns error then return error", func() {
				makoClientMock.On("CreateEmoteGroup", ctx).Return("", errors.New("dummy mako error"))

				groupID, isNew, err := testLogician.GetAndStoreGroupIDForBadgeTier(ctx, testChannelID, testThreshold)

				So(groupID, ShouldBeNil)
				So(isNew, ShouldBeFalse)
				So(err, ShouldNotBeNil)
				So(err.Error(), ShouldContainSubstring, "dummy mako error")
			})

			Convey("Given mako create emote group returns a group id", func() {
				makoClientMock.On("CreateEmoteGroup", ctx).Return(newGroupID, nil)

				Convey("Given tier group id dao fails to save new group id then return error", func() {
					Convey("Given the failure is a ConditionalCheckFailed error", func() {
						badgeTierEmotesGroupIDDAOMock.On("Create", ctx, testChannelID, testThreshold, newGroupID).Return(nil, true, errors.New("dummy dao error"))

						Convey("Given tier group id dao returns error then return error", func() {
							badgeTierEmotesGroupIDDAOMock.On("GetByChannelAndThreshold", ctx, testChannelID, testThreshold).Return(nil, errors.New("dummy dao error"))

							groupID, isNew, err := testLogician.GetAndStoreGroupIDForBadgeTier(ctx, testChannelID, testThreshold)

							So(groupID, ShouldBeNil)
							So(isNew, ShouldBeFalse)
							So(err, ShouldNotBeNil)
							So(err.Error(), ShouldContainSubstring, "dummy dao error")
						})

						Convey("Given tier group id dao returns group id then return group id and false", func() {
							badgeTierEmotesGroupIDDAOMock.On("GetByChannelAndThreshold", ctx, testChannelID, testThreshold).Return(testGroupIDMapping, nil)

							groupID, isNew, err := testLogician.GetAndStoreGroupIDForBadgeTier(ctx, testChannelID, testThreshold)

							So(groupID, ShouldResemble, testGroupIDMapping)
							So(isNew, ShouldBeFalse)
							So(err, ShouldBeNil)
						})
					})

					Convey("Given the failure is not a ConditionalCheckFailed error", func() {
						badgeTierEmotesGroupIDDAOMock.On("Create", ctx, testChannelID, testThreshold, newGroupID).Return(nil, false, errors.New("dummy dao error"))

						groupID, isNew, err := testLogician.GetAndStoreGroupIDForBadgeTier(ctx, testChannelID, testThreshold)

						So(groupID, ShouldBeNil)
						So(isNew, ShouldBeFalse)
						So(err, ShouldNotBeNil)
						So(err.Error(), ShouldContainSubstring, "dummy dao error")
					})
				})

				Convey("Given tier group id dao succeeds in saving new group id then return new group id and true", func() {
					badgeTierEmotesGroupIDDAOMock.On("Create", ctx, testChannelID, testThreshold, newGroupID).Return(newGroupIDMapping, false, nil)

					groupID, isNew, err := testLogician.GetAndStoreGroupIDForBadgeTier(ctx, testChannelID, testThreshold)

					So(groupID, ShouldResemble, newGroupIDMapping)
					So(isNew, ShouldBeTrue)
					So(err, ShouldBeNil)
				})
			})
		})
	})
}

func TestLogician_CreateAndStoreAllGroupIDsForChannel(t *testing.T) {
	Convey("Given a logician", t, func() {
		ctx := context.Background()
		testChannelID := "267893713"
		testThreshold := int64(1000)
		testGroupID := "1234567890"
		newGroupID := "1234569999"

		testGroupIDMapping := &badge_tier_emote_groupids.BadgeTierEmoteGroupID{
			Threshold: testThreshold,
			ChannelID: testChannelID,
			GroupID:   testGroupID,
		}

		newGroupIDMapping := &badge_tier_emote_groupids.BadgeTierEmoteGroupID{
			Threshold: testThreshold,
			ChannelID: testChannelID,
			GroupID:   newGroupID,
		}

		makoClientMock := new(mako_client_mock.MakoClient)
		badgeTierEmotesGroupIDDAOMock := new(badge_tier_emote_groupids_mock.BadgeTierEmoteGroupIDDAO)

		testLogician := logician{
			Mako:                makoClientMock,
			BadgeTierGroupIDDAO: badgeTierEmotesGroupIDDAOMock,
		}

		Convey("When the DAO errors when getting the channel's existing groups, then error", func() {
			badgeTierEmotesGroupIDDAOMock.On("Get", ctx, testChannelID).Return(nil, errors.New("dummy dao error"))

			err := testLogician.CreateAndStoreAllGroupIDsForChannel(ctx, testChannelID)

			So(err, ShouldNotBeNil)
		})

		Convey("When the DAO succeeds in getting existing groups, but Mako errors, we error", func() {
			badgeTierEmotesGroupIDDAOMock.On("Get", ctx, testChannelID).Return([]*badge_tier_emote_groupids.BadgeTierEmoteGroupID{}, nil)
			makoClientMock.On("CreateEmoteGroup", mock.Anything).Return("", errors.New("dummy mako error"))

			err := testLogician.CreateAndStoreAllGroupIDsForChannel(ctx, testChannelID)

			time.Sleep(100 * time.Millisecond) // wait for goroutines to return
			So(err, ShouldNotBeNil)
		})

		Convey("When the DAO succeeds in getting existing groups, Mako succeeds, but the DAO fails to create an entry, we error", func() {
			badgeTierEmotesGroupIDDAOMock.On("Get", ctx, testChannelID).Return([]*badge_tier_emote_groupids.BadgeTierEmoteGroupID{}, nil)
			makoClientMock.On("CreateEmoteGroup", mock.Anything).Return(newGroupID, nil)
			badgeTierEmotesGroupIDDAOMock.On("Create", mock.Anything, testChannelID, mock.Anything, newGroupID).Return(nil, false, errors.New("dummy dao error"))

			err := testLogician.CreateAndStoreAllGroupIDsForChannel(ctx, testChannelID)

			time.Sleep(100 * time.Millisecond) // wait for goroutines to return
			So(err, ShouldNotBeNil)
		})

		Convey("When the DAO returns no existing groups, we create and store group IDs for all thresholds", func() {
			badgeTierEmotesGroupIDDAOMock.On("Get", ctx, testChannelID).Return([]*badge_tier_emote_groupids.BadgeTierEmoteGroupID{}, nil)
			makoClientMock.On("CreateEmoteGroup", mock.Anything).Return(newGroupID, nil)
			badgeTierEmotesGroupIDDAOMock.On("Create", mock.Anything, testChannelID, mock.Anything, newGroupID).Return(newGroupIDMapping, false, nil)

			err := testLogician.CreateAndStoreAllGroupIDsForChannel(ctx, testChannelID)

			time.Sleep(100 * time.Millisecond) // wait for goroutines to return
			So(err, ShouldBeNil)
			makoClientMock.AssertNumberOfCalls(t, "CreateEmoteGroup", len(badgetiers.BitsChatBadgeTiers)-2)      // excluding tiers 1 and 100
			badgeTierEmotesGroupIDDAOMock.AssertNumberOfCalls(t, "Create", len(badgetiers.BitsChatBadgeTiers)-2) // excluding tiers 1 and 100
		})

		Convey("When the DAO returns an existing group with an empty groupID, we create and store group IDs for all thresholds", func() {
			badgeTierEmotesGroupIDDAOMock.On("Get", ctx, testChannelID).Return([]*badge_tier_emote_groupids.BadgeTierEmoteGroupID{
				{
					ChannelID: testChannelID,
					Threshold: testThreshold,
					GroupID:   "",
				},
			}, nil)
			makoClientMock.On("CreateEmoteGroup", mock.Anything).Return(newGroupID, nil)
			badgeTierEmotesGroupIDDAOMock.On("Create", mock.Anything, testChannelID, mock.Anything, newGroupID).Return(newGroupIDMapping, false, nil)

			err := testLogician.CreateAndStoreAllGroupIDsForChannel(ctx, testChannelID)

			time.Sleep(100 * time.Millisecond) // wait for goroutines to return
			So(err, ShouldBeNil)
			makoClientMock.AssertNumberOfCalls(t, "CreateEmoteGroup", len(badgetiers.BitsChatBadgeTiers)-2)      // excluding tiers 1 and 100
			badgeTierEmotesGroupIDDAOMock.AssertNumberOfCalls(t, "Create", len(badgetiers.BitsChatBadgeTiers)-2) // excluding tiers 1 and 100
		})

		Convey("When the DAO returns an existing group with a threshold below the minimum for emotes, we create and store group IDs for all thresholds", func() {
			badgeTierEmotesGroupIDDAOMock.On("Get", ctx, testChannelID).Return([]*badge_tier_emote_groupids.BadgeTierEmoteGroupID{
				{
					ChannelID: testChannelID,
					Threshold: 100,
					GroupID:   testGroupID,
				},
			}, nil)
			makoClientMock.On("CreateEmoteGroup", mock.Anything).Return(newGroupID, nil)
			badgeTierEmotesGroupIDDAOMock.On("Create", mock.Anything, testChannelID, mock.Anything, newGroupID).Return(newGroupIDMapping, false, nil)

			err := testLogician.CreateAndStoreAllGroupIDsForChannel(ctx, testChannelID)

			time.Sleep(100 * time.Millisecond) // wait for goroutines to return
			So(err, ShouldBeNil)
			makoClientMock.AssertNumberOfCalls(t, "CreateEmoteGroup", len(badgetiers.BitsChatBadgeTiers)-2)      // excluding tiers 1 and 100
			badgeTierEmotesGroupIDDAOMock.AssertNumberOfCalls(t, "Create", len(badgetiers.BitsChatBadgeTiers)-2) // excluding tiers 1 and 100
		})

		Convey("When the DAO returns at least one existing group, we create and store group IDs for the missing thresholds only", func() {
			badgeTierEmotesGroupIDDAOMock.On("Get", ctx, testChannelID).Return([]*badge_tier_emote_groupids.BadgeTierEmoteGroupID{testGroupIDMapping}, nil)
			makoClientMock.On("CreateEmoteGroup", mock.Anything).Return(newGroupID, nil)
			badgeTierEmotesGroupIDDAOMock.On("Create", mock.Anything, testChannelID, mock.Anything, newGroupID).Return(newGroupIDMapping, false, nil)

			err := testLogician.CreateAndStoreAllGroupIDsForChannel(ctx, testChannelID)

			time.Sleep(100 * time.Millisecond) // wait for goroutines to return
			So(err, ShouldBeNil)
			makoClientMock.AssertNumberOfCalls(t, "CreateEmoteGroup", len(badgetiers.BitsChatBadgeTiers)-3)      // excluding tiers 1 and 100, and 1000 since it exists already
			badgeTierEmotesGroupIDDAOMock.AssertNumberOfCalls(t, "Create", len(badgetiers.BitsChatBadgeTiers)-3) // excluding tiers 1 and 100, and 1000 since it exists already
		})
	})
}

func TestLogician_GetEmoteInfoByGroupID(t *testing.T) {
	Convey("Given a logician", t, func() {
		ctx := context.Background()

		testChannelID := "267893713"
		testThreshold := int64(1000)
		testGroupID := "1234567890"

		testGroupIDMapping := &badge_tier_emote_groupids.BadgeTierEmoteGroupID{
			Threshold: testThreshold,
			ChannelID: testChannelID,
			GroupID:   testGroupID,
		}

		makoClientMock := new(mako_client_mock.MakoClient)
		badgeTierEmotesGroupIDDAOMock := new(badge_tier_emote_groupids_mock.BadgeTierEmoteGroupIDDAO)
		bitsToBroadcasterGetterMock := new(leaderboard_mock.BitsToBroadcasterGetter)

		testLogician := logician{
			Mako:                    makoClientMock,
			BadgeTierGroupIDDAO:     badgeTierEmotesGroupIDDAOMock,
			BitsToBroadcasterGetter: bitsToBroadcasterGetterMock,
		}

		Convey("if dynamo errors", func() {
			badgeTierEmotesGroupIDDAOMock.On("GetByGroupID", mock.Anything, testGroupID).Return(nil, false, errors.New("test error"))

			resp, wasNotFound, err := testLogician.GetBitsTierSummaryForEmoteGroupID(ctx, testGroupID, nil)

			So(resp, ShouldBeNil)
			So(wasNotFound, ShouldBeFalse)
			So(err, ShouldNotBeNil)
		})

		Convey("if dynamo returns nothing", func() {
			badgeTierEmotesGroupIDDAOMock.On("GetByGroupID", mock.Anything, testGroupID).Return(nil, true, d.ErrNotFound)

			resp, wasNotFound, err := testLogician.GetBitsTierSummaryForEmoteGroupID(ctx, testGroupID, nil)

			So(resp, ShouldBeNil)
			So(wasNotFound, ShouldBeTrue)
			So(err, ShouldNotBeNil)
		})

		Convey("if dynamo returns emote group info", func() {
			badgeTierEmotesGroupIDDAOMock.On("GetByGroupID", mock.Anything, testGroupID).Return(testGroupIDMapping, false, nil)

			resp, wasNotFound, err := testLogician.GetBitsTierSummaryForEmoteGroupID(ctx, testGroupID, nil)

			So(resp, ShouldNotBeNil)
			So(wasNotFound, ShouldBeFalse)
			So(err, ShouldBeNil)
		})

		Convey("if the request includes a userID", func() {
			badgeTierEmotesGroupIDDAOMock.On("GetByGroupID", mock.Anything, testGroupID).Return(testGroupIDMapping, false, nil)

			Convey("and the userID is the same as the channelID for the tier", func() {
				resp, wasNotFound, err := testLogician.GetBitsTierSummaryForEmoteGroupID(ctx, testGroupID, &testGroupIDMapping.ChannelID)
				So(resp, ShouldNotBeNil)
				So(resp.NumberOfBitsUntilUnlock, ShouldBeZeroValue)
				So(resp.IsEmoteUnlockedForUser, ShouldBeTrue)
				So(wasNotFound, ShouldBeFalse)
				So(err, ShouldBeNil)
			})

			userID := "12345678"

			Convey("and GetBitsToBroadcaster returns an error", func() {
				bitsToBroadcasterGetterMock.On("GetBitsToBroadcaster", mock.Anything, userID, testGroupIDMapping.ChannelID).Return(int64(0), errors.New("test error"))

				resp, wasNotFound, err := testLogician.GetBitsTierSummaryForEmoteGroupID(ctx, testGroupID, &userID)

				So(resp, ShouldBeNil)
				So(wasNotFound, ShouldBeFalse)
				So(err, ShouldNotBeNil)
			})

			Convey("and GetBitsToBroadcaster returns 0", func() {
				bitsToBroadcasterGetterMock.On("GetBitsToBroadcaster", mock.Anything, userID, testGroupIDMapping.ChannelID).Return(int64(0), nil)

				resp, wasNotFound, err := testLogician.GetBitsTierSummaryForEmoteGroupID(ctx, testGroupID, &userID)

				So(resp, ShouldNotBeNil)
				So(wasNotFound, ShouldBeFalse)
				So(err, ShouldBeNil)
				So(resp.IsEmoteUnlockedForUser, ShouldBeFalse)
				So(resp.NumberOfBitsUntilUnlock, ShouldEqual, testThreshold)
			})

			Convey("and GetBitsToBroadcaster returns below the threshold", func() {
				bitsToBroadcasterGetterMock.On("GetBitsToBroadcaster", mock.Anything, userID, testGroupIDMapping.ChannelID).Return(int64(500), nil)

				resp, wasNotFound, err := testLogician.GetBitsTierSummaryForEmoteGroupID(ctx, testGroupID, &userID)

				So(resp, ShouldNotBeNil)
				So(wasNotFound, ShouldBeFalse)
				So(err, ShouldBeNil)
				So(resp.IsEmoteUnlockedForUser, ShouldBeFalse)
				So(resp.NumberOfBitsUntilUnlock, ShouldEqual, 500)
			})

			Convey("and GetBitsToBroadcaster returns equal to the threshold", func() {
				bitsToBroadcasterGetterMock.On("GetBitsToBroadcaster", mock.Anything, userID, testGroupIDMapping.ChannelID).Return(int64(1000), nil)

				resp, wasNotFound, err := testLogician.GetBitsTierSummaryForEmoteGroupID(ctx, testGroupID, &userID)

				So(resp, ShouldNotBeNil)
				So(wasNotFound, ShouldBeFalse)
				So(err, ShouldBeNil)
				So(resp.IsEmoteUnlockedForUser, ShouldBeTrue)
				So(resp.NumberOfBitsUntilUnlock, ShouldBeZeroValue)
			})

			Convey("and GetBitsToBroadcaster returns over the threshold", func() {
				bitsToBroadcasterGetterMock.On("GetBitsToBroadcaster", mock.Anything, userID, testGroupIDMapping.ChannelID).Return(int64(1100), nil)

				resp, wasNotFound, err := testLogician.GetBitsTierSummaryForEmoteGroupID(ctx, testGroupID, &userID)

				So(resp, ShouldNotBeNil)
				So(wasNotFound, ShouldBeFalse)
				So(err, ShouldBeNil)
				So(resp.IsEmoteUnlockedForUser, ShouldBeTrue)
				So(resp.NumberOfBitsUntilUnlock, ShouldBeZeroValue)
			})
		})
	})
}

func TestLogician_GetBadgeTierEmotes(t *testing.T) {
	channelId := "265716088"
	userId := "46024993"

	Convey("Given a logician", t, func() {
		ctx := context.Background()

		statsMock := new(metrics_mock.Statter)
		channelManagerMock := new(channel_mock.ChannelManager)
		bitsToBroadcasterMock := new(leaderboard_mock.BitsToBroadcasterGetter)
		badgeTierEmotesDaoMock := new(badge_tier_emote_groupids_mock.BadgeTierEmoteGroupIDDAO)
		cacheMock := new(cache_mock.BadgeTierEmotesCache)
		makoMock := new(mako_client_mock.MakoClient)

		testLogician := logician{
			ChannelManager:          channelManagerMock,
			BitsToBroadcasterGetter: bitsToBroadcasterMock,
			BadgeTierGroupIDDAO:     badgeTierEmotesDaoMock,
			BadgeTierEmotesCache:    cacheMock,
			Mako:                    makoMock,
			Statter:                 statsMock,
		}

		// these won't change the behavior of the logic if they error, so we'll just mock them working fine.
		statsMock.On("TimingDuration", mock.Anything, mock.Anything)
		cacheMock.On("Set", mock.Anything, mock.Anything, mock.Anything)

		// I test different behaviors of this call in TestLogician_getConfiguredEmoteGroups
		cacheMock.On("Get", ctx, mock.Anything).Return(nil)

		Convey("when the channel manager errors", func() {
			animeBearError := errors.New("BONTA UNORIGINAL AGAIN")
			channelManagerMock.On("Get", ctx, channelId).Return(nil, animeBearError)

			resp, err := testLogician.GetBadgeTierEmotes(ctx, channelId, userId, paydayrpc.BitsBadgeTierEmoteFilter_ALL)
			So(resp, ShouldBeNil)
			So(err, ShouldEqual, animeBearError)
		})

		Convey("when the channel manager returns nothing configured", func() {
			channelManagerMock.On("Get", ctx, channelId).Return(nil, nil)

			resp, err := testLogician.GetBadgeTierEmotes(ctx, channelId, userId, paydayrpc.BitsBadgeTierEmoteFilter_ALL)
			So(resp, ShouldBeNil)
			So(err, ShouldBeNil)
		})

		Convey("when the channel manager returns an opted out channel", func() {
			channelManagerMock.On("Get", ctx, channelId).Return(&dynamo.Channel{
				OptedOut:  pointers.BoolP(true),
				Onboarded: pointers.BoolP(true),
			}, nil)

			resp, err := testLogician.GetBadgeTierEmotes(ctx, channelId, userId, paydayrpc.BitsBadgeTierEmoteFilter_ALL)
			So(resp, ShouldBeNil)
			So(err, ShouldBeNil)
		})

		Convey("when the channel manager returns a non-bits-enabled channel", func() {
			channelManagerMock.On("Get", ctx, channelId).Return(&dynamo.Channel{
				OptedOut:  pointers.BoolP(false),
				Onboarded: pointers.BoolP(false),
			}, nil)

			resp, err := testLogician.GetBadgeTierEmotes(ctx, channelId, userId, paydayrpc.BitsBadgeTierEmoteFilter_ALL)
			So(resp, ShouldBeNil)
			So(err, ShouldBeNil)
		})

		Convey("when the channel is all clear to use bits", func() {
			channelManagerMock.On("Get", ctx, channelId).Return(&dynamo.Channel{
				OptedOut:  pointers.BoolP(false),
				Onboarded: pointers.BoolP(true),
			}, nil)

			Convey("when BitsToBroadcasterGetter errors", func() {
				animeBearError := errors.New("WHY ANIME BEARS?")
				bitsToBroadcasterMock.On("GetBitsToBroadcaster", mock.Anything, userId, channelId).Return(int64(0), animeBearError)

				Convey("when BadgeTierGroupIdDao returns a single", func() {
					badgeTierEmotesDaoMock.On("GetAboveThreshold", ctx, channelId, MinThresholdForEmoteRewards).Return(
						[]*badge_tier_emote_groupids.BadgeTierEmoteGroupID{{
							ChannelID: channelId,
							Threshold: 1000,
							GroupID:   "group-1000",
						}}, nil)

					Convey("When makoMock returns the single emote", func() {
						makoMock.On("GetEmoticonsByGroups", ctx, mock.Anything, []makorpc.EmoteState{makorpc.EmoteState_active}).Return(
							[]makorpc.EmoticonGroup{{
								Id: "group-1000",
								Emoticons: []*makorpc.Emoticon{
									{
										Id:      "emote-1000",
										OwnerId: channelId,
										Code:    "anime1000",
									},
								},
							}},
							nil)

						resp, err := testLogician.GetBadgeTierEmotes(ctx, channelId, userId, paydayrpc.BitsBadgeTierEmoteFilter_ALL)
						So(resp, ShouldBeNil)
						So(err, ShouldNotBeNil)
						So(err, ShouldEqual, animeBearError)
					})
				})
			})

			Convey("when BitsToBroadcasterGetter returns some numbers", func() {
				bitsToBroadcasterMock.On("GetBitsToBroadcaster", ctx, userId, channelId).Return(int64(51128), nil)

				Convey("when BadgeTierGroupIdDao errors", func() {
					animeBearError := errors.New("TO MANY ANIME BEARS IN TEST SUITE")
					badgeTierEmotesDaoMock.On("GetAboveThreshold", ctx, channelId, MinThresholdForEmoteRewards).Return(nil, animeBearError)

					resp, err := testLogician.GetBadgeTierEmotes(ctx, channelId, userId, paydayrpc.BitsBadgeTierEmoteFilter_ALL)

					So(resp, ShouldBeNil)
					So(err, ShouldEqual, animeBearError)
				})

				Convey("when BadgeTierGroupIdDao returns some things", func() {

					group1000 := badge_tier_emote_groupids.BadgeTierEmoteGroupID{
						ChannelID: channelId,
						Threshold: 1000,
						GroupID:   "group-1000",
					}

					group5000 := badge_tier_emote_groupids.BadgeTierEmoteGroupID{
						ChannelID: channelId,
						Threshold: 5000,
						GroupID:   "group-5000",
					}

					group100000 := badge_tier_emote_groupids.BadgeTierEmoteGroupID{
						ChannelID: channelId,
						Threshold: 100000,
						GroupID:   "group-100000",
					}

					badgeTierEmotesDaoMock.On("GetAboveThreshold", ctx, channelId, MinThresholdForEmoteRewards).Return([]*badge_tier_emote_groupids.BadgeTierEmoteGroupID{
						&group5000, &group1000, &group100000}, nil)

					Convey("when makoMock return an error", func() {
						animeBearError := errors.New("HAD_ENOUGH_WITH_ANIME_BEARS")

						makoMock.On("GetEmoticonsByGroups", ctx, mock.Anything, []makorpc.EmoteState{makorpc.EmoteState_active}).Return(
							nil, animeBearError)

						resp, err := testLogician.GetBadgeTierEmotes(ctx, channelId, userId, paydayrpc.BitsBadgeTierEmoteFilter_ALL)
						So(resp, ShouldBeNil)
						So(err, ShouldNotBeNil)
						So(err, ShouldEqual, animeBearError)
					})

					Convey("When makoMock returns emotes", func() {
						group1000emoteGroup := makorpc.EmoticonGroup{
							Id: group1000.GroupID,
							Emoticons: []*makorpc.Emoticon{
								{
									Id:      "emote-1000",
									OwnerId: channelId,
									Code:    "anime1000",
								},
							},
						}

						group5000emoteGroup := makorpc.EmoticonGroup{
							Id: group5000.GroupID,
							Emoticons: []*makorpc.Emoticon{
								{
									Id:      "emote-5000",
									OwnerId: channelId,
									Code:    "anime5000",
								},
							},
						}

						group100000emoteGroup := makorpc.EmoticonGroup{
							Id: group100000.GroupID,
							Emoticons: []*makorpc.Emoticon{
								{
									Id:      "emote-100000",
									OwnerId: channelId,
									Code:    "anime100000",
								},
							},
						}

						makoMock.On("GetEmoticonsByGroups", ctx, mock.Anything, []makorpc.EmoteState{makorpc.EmoteState_active}).Return(
							[]makorpc.EmoticonGroup{group100000emoteGroup, group1000emoteGroup, group5000emoteGroup},
							nil)

						resp, err := testLogician.GetBadgeTierEmotes(ctx, channelId, userId, paydayrpc.BitsBadgeTierEmoteFilter_ALL)

						So(err, ShouldBeNil)
						So(resp, ShouldNotBeNil)
						So(resp, ShouldHaveLength, 3)
						So(resp[0].EmoteId, ShouldEqual, "emote-1000")
						So(resp[0].BadgeTierSummary.IsEmoteUnlockedForUser, ShouldBeTrue)
						So(resp[1].EmoteId, ShouldEqual, "emote-5000")
						So(resp[1].BadgeTierSummary.IsEmoteUnlockedForUser, ShouldBeTrue)
						So(resp[2].EmoteId, ShouldEqual, "emote-100000")
						So(resp[2].BadgeTierSummary.IsEmoteUnlockedForUser, ShouldBeFalse)
						So(resp[2].BadgeTierSummary.NumberOfBitsUntilUnlock, ShouldEqual, 48872)
					})
				})
			})
		})
	})
}

func TestLogician_getConfiguredEmoteGroups(t *testing.T) {
	channelId := "265716088"
	groupId := "group-1000"

	Convey("Given a logician", t, func() {
		ctx := context.Background()

		statsMock := new(metrics_mock.Statter)
		badgeTierEmotesDaoMock := new(badge_tier_emote_groupids_mock.BadgeTierEmoteGroupIDDAO)
		cacheMock := new(cache_mock.BadgeTierEmotesCache)

		testLogician := logician{
			BadgeTierGroupIDDAO:  badgeTierEmotesDaoMock,
			BadgeTierEmotesCache: cacheMock,
		}

		// these won't change the behavior of the logic if they error, so we'll just mock them working fine.
		statsMock.On("TimingDuration", mock.Anything, mock.Anything)
		cacheMock.On("Set", mock.Anything, channelId, mock.Anything)

		Convey("when the cache returns nothing", func() {
			cacheMock.On("Get", ctx, channelId).Return(nil)

			Convey("when the DAO returns an error", func() {
				animeBearError := errors.New("ANIME BEARS ENDANGERED")
				badgeTierEmotesDaoMock.On("GetAboveThreshold", ctx, channelId, MinThresholdForEmoteRewards).Return(nil, animeBearError)

				groupMap, groupIds, err := testLogician.getConfiguredEmoteGroups(ctx, channelId)
				So(groupMap, ShouldBeNil)
				So(groupIds, ShouldBeNil)
				So(err, ShouldNotBeNil)
				So(err, ShouldEqual, animeBearError)
			})

			Convey("when the DAO returns a single result", func() {
				badgeTierEmotesDaoMock.On("GetAboveThreshold", ctx, channelId, MinThresholdForEmoteRewards).Return(
					[]*badge_tier_emote_groupids.BadgeTierEmoteGroupID{{
						ChannelID: channelId,
						Threshold: 1000,
						GroupID:   groupId,
					}}, nil)

				groupMap, groupIds, err := testLogician.getConfiguredEmoteGroups(ctx, channelId)
				So(groupMap, ShouldHaveLength, 1)
				So(groupIds, ShouldHaveLength, 1)
				So(groupIds[0], ShouldEqual, groupId)
				So(err, ShouldBeNil)
			})
		})

		Convey("when the cache returns empty", func() {
			cacheMock.On("Get", ctx, mock.Anything).Return([]*badge_tier_emote_groupids.BadgeTierEmoteGroupID{})

			groupMap, groupIds, err := testLogician.getConfiguredEmoteGroups(ctx, channelId)
			So(groupMap, ShouldHaveLength, 0)
			So(groupIds, ShouldHaveLength, 0)
			So(err, ShouldBeNil)
			badgeTierEmotesDaoMock.AssertNotCalled(t, "GetAboveThreshold", channelId, MinThresholdForEmoteRewards)
		})

		Convey("when the cache returns data", func() {
			cacheMock.On("Get", ctx, mock.Anything).Return([]*badge_tier_emote_groupids.BadgeTierEmoteGroupID{{
				ChannelID: channelId,
				Threshold: 1000,
				GroupID:   groupId,
			}})

			groupMap, groupIds, err := testLogician.getConfiguredEmoteGroups(ctx, channelId)
			So(groupMap, ShouldHaveLength, 1)
			So(groupIds, ShouldHaveLength, 1)
			So(err, ShouldBeNil)
			So(groupIds[0], ShouldEqual, groupId)
			badgeTierEmotesDaoMock.AssertNotCalled(t, "GetAboveThreshold", channelId, MinThresholdForEmoteRewards)
		})
	})
}

func TestLogician_applyFilter(t *testing.T) {

	emotes := []*paydayrpc.BitsBadgeTierEmote{
		{
			EmoteId: "1", BadgeTierSummary: &paydayrpc.EmoteBitsBadgeTierSummary{IsEmoteUnlockedForUser: true},
		}, {
			EmoteId: "2", BadgeTierSummary: &paydayrpc.EmoteBitsBadgeTierSummary{IsEmoteUnlockedForUser: true},
		}, {
			EmoteId: "3", BadgeTierSummary: &paydayrpc.EmoteBitsBadgeTierSummary{IsEmoteUnlockedForUser: true},
		}, {
			EmoteId: "4", BadgeTierSummary: &paydayrpc.EmoteBitsBadgeTierSummary{IsEmoteUnlockedForUser: true},
		}, {
			EmoteId: "5", BadgeTierSummary: &paydayrpc.EmoteBitsBadgeTierSummary{IsEmoteUnlockedForUser: true},
		}, {
			EmoteId: "6", BadgeTierSummary: &paydayrpc.EmoteBitsBadgeTierSummary{IsEmoteUnlockedForUser: false},
		}, {
			EmoteId: "7", BadgeTierSummary: &paydayrpc.EmoteBitsBadgeTierSummary{IsEmoteUnlockedForUser: false},
		}, {
			EmoteId: "8", BadgeTierSummary: &paydayrpc.EmoteBitsBadgeTierSummary{IsEmoteUnlockedForUser: false},
		}, {
			EmoteId: "9", BadgeTierSummary: &paydayrpc.EmoteBitsBadgeTierSummary{IsEmoteUnlockedForUser: false},
		}, {
			EmoteId: "10", BadgeTierSummary: &paydayrpc.EmoteBitsBadgeTierSummary{IsEmoteUnlockedForUser: false},
		}, {
			EmoteId: "11", BadgeTierSummary: &paydayrpc.EmoteBitsBadgeTierSummary{IsEmoteUnlockedForUser: false},
		}, {
			EmoteId: "12", BadgeTierSummary: &paydayrpc.EmoteBitsBadgeTierSummary{IsEmoteUnlockedForUser: false},
		},
	}

	Convey("We don't need a base", t, func() {
		Convey("when no emotes are passed in", func() {
			So(applyFilter(nil, paydayrpc.BitsBadgeTierEmoteFilter_ALL), ShouldHaveLength, 0)
		})

		Convey("when we want all from a big list", func() {
			resp := applyFilter(emotes, paydayrpc.BitsBadgeTierEmoteFilter_ALL)
			So(resp, ShouldHaveLength, 12)
		})

		Convey("when we want unlocked only from a big list", func() {
			resp := applyFilter(emotes, paydayrpc.BitsBadgeTierEmoteFilter_UNLOCKED)
			So(resp, ShouldHaveLength, 5)
		})

		Convey("when we want locked only from a big list", func() {
			resp := applyFilter(emotes, paydayrpc.BitsBadgeTierEmoteFilter_LOCKED)
			So(resp, ShouldHaveLength, 7)
		})

		Convey("when we want locked preview only from a big list", func() {
			resp := applyFilter(emotes, paydayrpc.BitsBadgeTierEmoteFilter_LOCKED_PREVIEW)
			So(resp, ShouldHaveLength, 3)
		})

		Convey("when we want current highest unlocked and next locked from a big list", func() {
			resp := applyFilter(emotes, paydayrpc.BitsBadgeTierEmoteFilter_HIGHEST_UNLOCKED_AND_NEXT)
			So(resp, ShouldHaveLength, 2)
		})
	})
}

func TestLogician_combineEmoteDataAndBitsTierData(t *testing.T) {

	Convey("Given a logician", t, func() {
		statter := new(metrics_mock.Statter)
		testLogician := logician{
			Statter: statter,
		}

		Convey("when you have nothing", func() {
			badgeTierData := testLogician.combineEmoteDataAndBitsTierData(0, nil, nil)
			So(badgeTierData, ShouldHaveLength, 0)
		})

		Convey("when you have totally normal data", func() {
			channelId := "bonta_channel"
			emoteGroupId := "group_id_7"
			emoteId := "bonta_emote"
			emoteCode := "bonta_code"

			badgeTierData := testLogician.combineEmoteDataAndBitsTierData(1000, map[string]badge_tier_emote_groupids.BadgeTierEmoteGroupID{
				emoteGroupId: {
					ChannelID: channelId,
					Threshold: int64(5000),
				},
			}, []makorpc.EmoticonGroup{
				{
					Id: emoteGroupId,
					Emoticons: []*makorpc.Emoticon{
						{
							Id:      emoteId,
							Code:    emoteCode,
							OwnerId: channelId,
						},
					},
				},
			})
			So(badgeTierData, ShouldHaveLength, 1)
			So(badgeTierData[0].EmoteCode, ShouldEqual, emoteCode)
			So(badgeTierData[0].BadgeTierSummary.NumberOfBitsUntilUnlock, ShouldEqual, 4000)
		})

		Convey("when you have an extra emote that shouldn't be there", func() {
			channelId := "bonta_channel"
			emoteGroupId := "group_id_7"
			emoteId1 := "bonta_emote"
			emoteCode1 := "bonta_code"
			emoteId2 := "bonta_emoticon"
			emoteCode2 := "bonta_cody"

			statter.On("Inc", "ExtraEmoteAttachedToBitsTier", int64(1))

			badgeTierData := testLogician.combineEmoteDataAndBitsTierData(1000, map[string]badge_tier_emote_groupids.BadgeTierEmoteGroupID{
				emoteGroupId: {ChannelID: channelId, Threshold: int64(5000)},
			}, []makorpc.EmoticonGroup{
				{
					Id: emoteGroupId,
					Emoticons: []*makorpc.Emoticon{
						{
							Id:      emoteId1,
							Code:    emoteCode1,
							OwnerId: channelId,
						},
						{
							Id:      emoteId2,
							Code:    emoteCode2,
							OwnerId: channelId,
						},
					},
				},
			})
			So(badgeTierData, ShouldHaveLength, 2)
			So(badgeTierData[0].EmoteCode, ShouldEqual, emoteCode1)
			So(badgeTierData[0].BadgeTierSummary.NumberOfBitsUntilUnlock, ShouldEqual, 4000)
			So(badgeTierData[1].EmoteCode, ShouldEqual, emoteCode2)
			So(badgeTierData[1].BadgeTierSummary.NumberOfBitsUntilUnlock, ShouldEqual, 4000)

			statter.AssertCalled(t, "Inc", "ExtraEmoteAttachedToBitsTier", int64(1))
		})

		Convey("when you have a lot of emote that shouldn't be there", func() {
			channelId := "bonta_channel"
			emoteGroupId := "group_id_7"
			emoteGroup := makorpc.EmoticonGroup{
				Id:        emoteGroupId,
				Emoticons: []*makorpc.Emoticon{},
			}

			for i := 0; i < 100; i++ {
				emoteGroup.Emoticons = append(emoteGroup.Emoticons, &makorpc.Emoticon{
					Id:      fmt.Sprintf("emote_id_%d", i),
					Code:    fmt.Sprintf("emote_code_%d", i),
					OwnerId: channelId,
				})
			}

			statter.On("Inc", "ExtraEmoteAttachedToBitsTier", int64(99))
			badgeTierData := testLogician.combineEmoteDataAndBitsTierData(1000, map[string]badge_tier_emote_groupids.BadgeTierEmoteGroupID{
				emoteGroupId: {ChannelID: channelId, Threshold: int64(5000)},
			}, []makorpc.EmoticonGroup{emoteGroup})

			So(badgeTierData, ShouldHaveLength, 100)
			statter.AssertCalled(t, "Inc", "ExtraEmoteAttachedToBitsTier", int64(99))
		})
	})
}
