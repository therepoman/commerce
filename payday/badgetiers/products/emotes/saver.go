package badgetieremotes

import (
	"context"
	"encoding/json"

	log "code.justin.tv/commerce/logrus"
	makotwirp "code.justin.tv/commerce/mako/twirp"
	mako_dashboard "code.justin.tv/commerce/mako/twirp/dashboard"
	badgetiermodels "code.justin.tv/commerce/payday/badgetiers/models"
	"code.justin.tv/commerce/payday/clients/mako"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/dynamo/badge_tier_emote_groupids"
	apiModels "code.justin.tv/commerce/payday/models/api"
	apimodels "code.justin.tv/commerce/payday/models/api"
	"code.justin.tv/commerce/payday/sns"
	"code.justin.tv/commerce/payday/utils/strings"
	"github.com/hashicorp/go-multierror"
	"github.com/pkg/errors"
	"github.com/twitchtv/twirp"
)

const (
	GetEmoticonsDownstreamError                   = "GetEmoticonsDownstreamError"
	GetEmoticonsValidationError                   = "GetEmoticonsValidationError"
	GetUnlockedBadgeUserCountsError               = "GetUnlockedBadgeUserCountsError"
	CreateEmoticonCodeAlreadyExistsError          = "CreateEmoticonCodeAlreadyExistsError"
	CreateEmoticonCodeUnacceptableError           = "CreateEmoticonCodeUnacceptableError"
	CreateEmoticonUserOwnedEmoteLimitReachedError = "CreateEmoticonUserOwnedEmoteLimitReachedError"
	CreateEmoticonSlotsAlreadyFullError           = "CreateEmoticonSlotsAlreadyFullError"
	CreateEmoticonTierNotAllowedError             = "CreateEmoticonTierNotAllowedError"
	CreateEmoticonValidationError                 = "CreateEmoticonValidationError"
	CreateEmoticonDownstreamError                 = "CreateEmoticonDownstreamError"
	DeleteEmoticonValidationError                 = "DeleteEmoticonValidationError"
	DeleteEmoticonDownstreamError                 = "DeleteEmoticonDownstreamError"
)

type Saver interface {
	SaveEmoticons(ctx context.Context, channelId string, request apiModels.SetBadgesRequest) (map[int64][]apiModels.EmoticonUploadConfiguration, error)
	CreateEmoticon(ctx context.Context, emoteUploadConfiguration apiModels.EmoticonUploadConfiguration) (*mako_dashboard.CreateEmoteResponse, error)
}

type saver struct {
	MakoClient              mako.MakoClient                                    `inject:""`
	Validator               Validator                                          `inject:""`
	BadgeTierEmotesLogician Logician                                           `inject:""`
	BadgeTierGroupIDDAO     badge_tier_emote_groupids.BadgeTierEmoteGroupIDDAO `inject:""`
	SNSClient               sns.ISNSClient                                     `inject:"usWestSNSClient"`
	Config                  *config.Configuration                              `inject:""`
}

type SaverError struct {
	Err       error
	ErrorType string
}

func NewSaverError(err error, errType string) error {
	return &SaverError{
		Err:       err,
		ErrorType: errType,
	}
}

func (s *SaverError) Error() string {
	return s.Err.Error()
}

func NewSaver() Saver {
	return &saver{}
}

func (s *saver) SaveEmoticons(ctx context.Context, channelID string, request apiModels.SetBadgesRequest) (map[int64][]apiModels.EmoticonUploadConfiguration, error) {
	emoticonConfigMap := map[int64][]apiModels.EmoticonUploadConfiguration{}
	var saveEmoticonErrors error

	allowedTiersMap, err := s.Validator.GetAllowedTiersMap(ctx, channelID, request.Tiers)
	if err != nil {
		saveEmoticonErrors = multierror.Append(saveEmoticonErrors, err)
		return nil, saveEmoticonErrors
	}

	emoticonsByTier, err := s.getAllEmoticonsForTiers(ctx, channelID)
	if err != nil {
		saveEmoticonErrors = multierror.Append(saveEmoticonErrors, err)
		return nil, saveEmoticonErrors
	}

	for _, tier := range request.Tiers {
		// Skip if there's no tier, or if there's no tier information relevant to saving emoticons
		if shouldSkipTier(tier) {
			continue
		}

		threshold := tier.Threshold
		numberOfUploadedTierEmotesInMako := int64(len(emoticonsByTier[threshold]))
		emoticonSettings := *tier.EmoticonSettings

		for _, emoticonSetting := range emoticonSettings {
			/* TODO: once there's a batch Create or Delete in Mako, call that instead so that we minimize call volume.
			Not a problem until badge tiers support multiple emotes from the product side of things */

			if emoticonSetting.DeleteEmoticon != nil {
				err := s.DeleteEmoticon(ctx, emoticonSetting)
				if err != nil {
					saveEmoticonErrors = multierror.Append(saveEmoticonErrors, err)
					continue
				}
			} else {
				if emoticonSetting.ImageID1x == nil || emoticonSetting.ImageID2x == nil || emoticonSetting.ImageID4x == nil {
					missingImageIDErr := errors.New("missing imageIDs for create emoticon request")
					saveEmoticonErrors = multierror.Append(saveEmoticonErrors, missingImageIDErr)
					continue
				}

				// images are already uploaded from the new auto-resizing uploader component, let's make the emote immediately

				err = s.Validator.ValidateUpload(ctx, badgetiermodels.EmoticonValidationPayload{
					ChannelID:                         channelID,
					Threshold:                         threshold,
					EmoticonSetting:                   emoticonSetting,
					AllowedTiersMap:                   allowedTiersMap,
					NumberOfTierEmotesAlreadyUploaded: numberOfUploadedTierEmotesInMako,
				})
				if err != nil {
					// the actual error here usually gets swallowed by the mutlierror and returned is something super duper vague.
					log.WithError(err).WithFields(log.Fields{
						"channelID":                         channelID,
						"threshold":                         threshold,
						"emoticonSetting":                   emoticonSetting,
						"allowedTiersMap":                   allowedTiersMap,
						"numberOfTierEmotesAlreadyUploaded": numberOfUploadedTierEmotesInMako,
					}).Debug("Validation failed for previously uploaded image assets for badge tier emotes")
					saveEmoticonErrors = multierror.Append(saveEmoticonErrors, err)
					continue
				}

				groupIDMapping, _, err := s.BadgeTierEmotesLogician.GetAndStoreGroupIDForBadgeTier(ctx, channelID, threshold)
				if err != nil {
					saveEmoticonErrors = multierror.Append(saveEmoticonErrors, err)
					continue
				}

				if groupIDMapping == nil {
					nilGroupIDErr := errors.New("received unexpected nil groupID while saving")
					saveEmoticonErrors = multierror.Append(nilGroupIDErr)
					continue
				}

				if !groupIDMapping.IsBackfilled {
					err := s.enqueueEmoticonBackfill(threshold, channelID, groupIDMapping.GroupID)
					if err != nil {
						log.WithFields(log.Fields{
							"channelID": channelID,
							"threshold": threshold,
							"groupID":   groupIDMapping.GroupID,
						}).Error("error enqueueing full backfill message for non-backfilled mako group")
					}
				}

				resp, err := s.CreateEmoticon(ctx, apiModels.EmoticonUploadConfiguration{
					Threshold:  threshold,
					Code:       emoticonSetting.Code,
					CodeSuffix: *emoticonSetting.CodeSuffix,
					OwnerID:    channelID,
					GroupID:    groupIDMapping.GroupID,
					ImageUploadConfig1X: apiModels.ImageUploadConfig{
						ImageID: *emoticonSetting.ImageID1x,
					},
					ImageUploadConfig2X: apiModels.ImageUploadConfig{
						ImageID: *emoticonSetting.ImageID2x,
					},
					ImageUploadConfig4X: apiModels.ImageUploadConfig{
						ImageID: *emoticonSetting.ImageID4x,
					},
					ImageSource: emoticonSetting.ImageSource,
				})
				if err != nil {
					saveEmoticonErrors = multierror.Append(saveEmoticonErrors, err)
					continue
				}

				log.WithFields(log.Fields{
					"emoteID":  resp.GetId(),
					"groupIDs": resp.GetGroupIds(),
					"code":     resp.GetCode(),
					"state":    resp.GetState(),
				}).Debug("CreateEmoticon response from previously uploaded image assets for badge tier emotes")
			}
		}
	}

	return emoticonConfigMap, saveEmoticonErrors
}

func (s *saver) enqueueEmoticonBackfill(threshold int64, channelID string, groupID string) error {
	logWithFields := log.WithFields(log.Fields{
		"channelID": channelID,
		"threshold": threshold,
		"groupID":   groupID,
	})

	message := apimodels.EmoticonBackfillSNSMessage{
		Threshold: threshold,
		ChannelID: channelID,
		GroupID:   groupID,
		UserIDs:   nil,
	}

	messageJSON, err := json.Marshal(message)
	if err != nil {
		logWithFields.WithError(err).Error("Error marshaling emoticon backfill message json")
		return err
	}

	err = s.SNSClient.PostToTopic(s.Config.EmoticonBackfillSNSTopic, string(messageJSON))
	if err != nil {
		logWithFields.WithError(err).Error("Error posting emoticon backfill to SNS")
		return err
	}

	return nil
}

func (s *saver) CreateEmoticon(ctx context.Context, emoticonUploadConfiguration apiModels.EmoticonUploadConfiguration) (*mako_dashboard.CreateEmoteResponse, error) {
	err := validateEmoticonUploadConfigurationData(emoticonUploadConfiguration)
	if err != nil {
		return nil, NewSaverError(err, CreateEmoticonValidationError)
	}

	createEmoteResp, err := s.MakoClient.CreateEmote(ctx, emoticonUploadConfiguration.OwnerID, emoticonUploadConfiguration)
	if err != nil {
		// Check for expected validation errors first
		createErr := convertMakoCreateError(err)
		if createErr != nil {
			return nil, createErr
		}

		// If the error wasn't an expected validation error, return generic errors
		log.WithError(err).WithField("emoticonUploadConfiguration", emoticonUploadConfiguration).Error("unexpected error from Mako while creating emoticon")
		if twirpError, ok := err.(twirp.Error); ok && twirpError.Code() == twirp.InvalidArgument {
			return nil, NewSaverError(err, CreateEmoticonValidationError)
		}
		return nil, NewSaverError(err, CreateEmoticonDownstreamError)
	}

	return &createEmoteResp, nil
}

func convertMakoCreateError(createError error) error {
	var rpcErr makotwirp.ClientError
	parseErr := makotwirp.ParseClientError(errors.Cause(createError), &rpcErr)
	if parseErr != nil {
		// Error couldn't be parsed as a Mako client error
		return nil
	}

	switch rpcErr.ErrorCode {
	case makotwirp.ErrCodeCodeNotUnique:
		return NewSaverError(createError, CreateEmoticonCodeAlreadyExistsError)
	case makotwirp.ErrCodeCodeUnacceptable:
		return NewSaverError(createError, CreateEmoticonCodeUnacceptableError)
	case makotwirp.ErrCodeUserOwnedEmoteLimitReached:
		return NewSaverError(createError, CreateEmoticonUserOwnedEmoteLimitReachedError)
	default:
		// Unknown client error
		return nil
	}
}

func (s *saver) DeleteEmoticon(ctx context.Context, emoticonSetting apiModels.EmoticonSetting) error {
	if !*emoticonSetting.DeleteEmoticon {
		// If client calls with "DeleteEmoticon: false", do not call Mako
		return NewSaverError(errors.New("deleteEmoticon should be either true or nil, not false"), DeleteEmoticonValidationError)
	}

	if emoticonSetting.EmoteID == nil {
		return NewSaverError(errors.New("delete emoticon requires a non-nil emoteID"), DeleteEmoticonValidationError)
	}

	err := s.MakoClient.DeleteEmoticon(ctx, *emoticonSetting.EmoteID, emoticonSetting.Code, false)
	if err != nil {
		log.WithFields(log.Fields{
			"emoteID": *emoticonSetting.EmoteID,
			"code":    emoticonSetting.Code,
		}).WithError(err).Error("unexpected error from Mako while deleting emoticon")
		return NewSaverError(err, DeleteEmoticonDownstreamError)
	}

	return nil
}

// Retrieves all "tier -> group_id" mappings we have in dynamo for the given channel, uses those group ids to fetch
// all emotes for them from Mako and then returns a map all emotes for the given channel by tier
// TODO: Combine this logic with the logic under PopulateWithBadgeTierEmoticons as they do very similar work
func (s *saver) getAllEmoticonsForTiers(ctx context.Context, channelID string) (map[int64][]mako_dashboard.Emote, error) {
	var emoticonsByThreshold = make(map[int64][]mako_dashboard.Emote)

	badgeTierGroups, err := s.BadgeTierGroupIDDAO.Get(ctx, channelID)
	if err != nil {
		log.WithError(err).WithField("channel_id", channelID).Error("error getting group ids for badge tiers from dynamo")
		return nil, NewSaverError(err, GetEmoticonsDownstreamError)
	}

	if len(badgeTierGroups) == 0 {
		return nil, nil
	}

	groupIDs := make([]string, 0, len(badgeTierGroups))
	thresholdsByGroupID := make(map[string]int64, len(badgeTierGroups))
	for _, group := range badgeTierGroups {
		if strings.NotBlank(group.GroupID) {
			groupIDs = append(groupIDs, group.GroupID)
			thresholdsByGroupID[group.GroupID] = group.Threshold
		}
	}

	resp, err := s.MakoClient.DashboardGetEmotesByGroups(ctx, groupIDs, []mako_dashboard.EmoteState{mako_dashboard.EmoteState_active, mako_dashboard.EmoteState_pending})
	if err != nil {
		log.WithError(err).WithField("groupIDs", groupIDs).Error("error getting emoticons by groups")
		return nil, NewSaverError(err, GetEmoticonsDownstreamError)
	}

	if len(resp) == 0 {
		return emoticonsByThreshold, nil
	}

	for _, emoticonGroup := range resp {
		if emoticonGroup.Id == "" {
			continue
		}

		threshold, ok := thresholdsByGroupID[emoticonGroup.Id]
		if !ok {
			log.WithError(err).WithFields(log.Fields{
				"groupID":   emoticonGroup.Id,
				"channelID": channelID,
			}).Error("there is no tier threshold mapping for the given group id")
			return nil, NewSaverError(err, GetEmoticonsValidationError)
		}

		respEmoticons := emoticonGroup.Emotes
		if respEmoticons != nil {
			emoticons := make([]mako_dashboard.Emote, 0)
			for _, respEmoticon := range respEmoticons {
				if respEmoticon != nil {
					emoticons = append(emoticons, *respEmoticon)
				}
			}
			emoticonsByThreshold[threshold] = emoticons
		}
	}

	return emoticonsByThreshold, nil
}

func shouldSkipTier(tier *apiModels.BadgeTierSetting) bool {
	return tier == nil || tier.EmoticonSettings == nil || len(*tier.EmoticonSettings) == 0
}
