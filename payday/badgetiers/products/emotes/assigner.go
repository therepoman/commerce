package badgetieremotes

import (
	"context"
	"encoding/json"

	log "code.justin.tv/commerce/logrus"
	makotwirp "code.justin.tv/commerce/mako/twirp"
	mako_dashboard "code.justin.tv/commerce/mako/twirp/dashboard"
	"code.justin.tv/commerce/payday/badgetiers"
	"code.justin.tv/commerce/payday/clients/mako"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/models/api"
	apimodels "code.justin.tv/commerce/payday/models/api"
	"code.justin.tv/commerce/payday/sns"
	"github.com/pkg/errors"
)

var (
	EmotesNotAllowedInTierAssignError = errors.New("Emotes are not allowed to be assigned to this tier")
	TierAlreadyFullAssignError        = errors.New("Tier is already full of emotes")
	UserNotPermittedAssignError       = errors.New("User is not permitted to assign the emote")
	EmoteDoesNotExistAssignError      = errors.New("Emote does not exist")
	InvalidEmoteStateAssignError      = errors.New("Emote state is invalid for bits tier assignment")
	EmoteCodeNotUniqueAssignError     = errors.New("Emote code is not unique")
)

type Assigner interface {
	AssignEmoteToBitsTier(ctx context.Context, channelID string, emoteID string, bitsTierThreshold int64) error
}

type assigner struct {
	Validator  Validator             `inject:""`
	Logician   Logician              `inject:""`
	MakoClient mako.MakoClient       `inject:""`
	SNSClient  sns.ISNSClient        `inject:"usWestSNSClient"`
	Config     *config.Configuration `inject:""`
}

func NewAssigner() Assigner {
	return &assigner{}
}

func (a *assigner) AssignEmoteToBitsTier(ctx context.Context, channelID string, emoteID string, bitsTierThreshold int64) error {
	// Check if the bits tier allows emotes to be uploaded or assigned to it
	allowedTiersMap, err := a.Validator.GetAllowedTiersMap(ctx, channelID, buildAllTierSettingsList())
	if err != nil {
		return errors.Wrap(err, "Error getting allowed tiers map during emote bits tier assignment")
	} else if !allowedTiersMap[bitsTierThreshold] {
		return EmotesNotAllowedInTierAssignError
	}

	// Get the bits tier groupID mapping
	groupIDMapping, _, err := a.Logician.GetAndStoreGroupIDForBadgeTier(ctx, channelID, bitsTierThreshold)
	if err != nil {
		return errors.Wrap(err, "Error getting bits tier groupID mapping during emote bits tier assignment")
	} else if groupIDMapping == nil {
		return errors.New("Received unexpected nil bits tier groupID mapping during emote bits tier assignment")
	}

	// Check if the bits tier is already full of emotes
	groupsResp, err := a.MakoClient.DashboardGetEmotesByGroups(ctx, []string{groupIDMapping.GroupID}, []mako_dashboard.EmoteState{mako_dashboard.EmoteState_active, mako_dashboard.EmoteState_pending})
	if err != nil {
		return errors.Wrap(err, "Error getting emotes by groups during emote bits tier assignment")
	} else if len(groupsResp) > 0 && len(groupsResp[0].Emotes) >= allowedEmoticonsPerBadgeTier {
		return TierAlreadyFullAssignError
	}

	// Call Mako to assign emote to the bits tier's mapped group
	_, err = a.MakoClient.AssignEmoteToGroup(ctx, channelID, emoteID, groupIDMapping.GroupID)
	if err != nil {
		assignErr := convertMakoAssignError(err)
		if assignErr != nil {
			return assignErr
		}
		return errors.Wrap(err, "Error calling Mako AssignEmoteToGroup during emote bits tier assignment")
	}

	// Backfill the groupID entitlements if they haven't been backfilled already
	if !groupIDMapping.IsBackfilled {
		go func(threshold int64, channelID string, groupID string) {
			err := a.enqueueEmoteBackfill(threshold, channelID, groupID)
			if err != nil {
				log.WithFields(log.Fields{
					"channelID": channelID,
					"threshold": bitsTierThreshold,
					"groupID":   groupIDMapping.GroupID,
				}).Error("Error enqueuing emote backfill message for non-backfilled mako group during emote bits tier assignment")
			}
		}(bitsTierThreshold, channelID, groupIDMapping.GroupID)
	}

	return nil
}

func (a *assigner) enqueueEmoteBackfill(threshold int64, channelID string, groupID string) error {
	logWithFields := log.WithFields(log.Fields{
		"channelID": channelID,
		"threshold": threshold,
		"groupID":   groupID,
	})

	message := apimodels.EmoticonBackfillSNSMessage{
		Threshold: threshold,
		ChannelID: channelID,
		GroupID:   groupID,
		UserIDs:   nil,
	}

	messageJSON, err := json.Marshal(message)
	if err != nil {
		logWithFields.WithError(err).Error("Error marshaling emoticon backfill message json during emote bits tier assignment")
		return err
	}

	err = a.SNSClient.PostToTopic(a.Config.EmoticonBackfillSNSTopic, string(messageJSON))
	if err != nil {
		logWithFields.WithError(err).Error("Error posting emoticon backfill to SNS during emote bits tier assignment")
		return err
	}

	return nil
}

func buildAllTierSettingsList() []*api.BadgeTierSetting {
	var allTierSettings []*api.BadgeTierSetting
	for threshold, tierConfig := range badgetiers.BitsChatBadgeTiers {
		if tierConfig.Enabled {
			allTierSettings = append(allTierSettings, &api.BadgeTierSetting{Threshold: threshold})
		}
	}
	return allTierSettings
}

func convertMakoAssignError(assignError error) error {
	var rpcErr makotwirp.ClientError
	parseErr := makotwirp.ParseClientError(errors.Cause(assignError), &rpcErr)
	if parseErr != nil {
		// Error couldn't be parsed as a Mako client error
		return nil
	}

	switch rpcErr.ErrorCode {
	case makotwirp.ErrCodeUserNotPermitted:
		return UserNotPermittedAssignError
	case makotwirp.ErrEmoteDoesNotExist:
		return EmoteDoesNotExistAssignError
	case makotwirp.ErrCodeInvalidState:
		return InvalidEmoteStateAssignError
	case makotwirp.ErrCodeCodeNotUnique:
		return EmoteCodeNotUniqueAssignError
	default:
		// Unknown client error
		return nil
	}
}
