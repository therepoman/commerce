package badgetieremotes

import (
	"context"
	"encoding/json"
	"fmt"

	"code.justin.tv/commerce/logrus"
	mako_client "code.justin.tv/commerce/mako/twirp"
	badgetiers "code.justin.tv/commerce/payday/badgetiers/logician"
	"code.justin.tv/commerce/payday/clients/mako"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/dynamo/badge_tier_emote_groupids"
	"code.justin.tv/commerce/payday/metrics"
	apimodels "code.justin.tv/commerce/payday/models/api"
	"code.justin.tv/commerce/payday/sns"
	"code.justin.tv/commerce/payday/utils/strings"
	"github.com/pkg/errors"
)

const (
	NEW_TIERS_BREACHED_METRIC = "New_Bits_Tiers_Breached_%d"
)

type Entitler interface {
	Entitle(context.Context, string, string, int64, int64, string) (*[]mako_client.Emoticon, error)
	EntitleAll(ctx context.Context, userID, channelID string, bitsTotal int64) error
}

func NewEntitler() Entitler {
	return &entitler{}
}

type entitler struct {
	BadgeTiersLogician  badgetiers.Logician                                `inject:""`
	BadgeTierGroupIDDAO badge_tier_emote_groupids.BadgeTierEmoteGroupIDDAO `inject:""`
	Statter             metrics.Statter                                    `inject:""`
	SNSClient           sns.ISNSClient                                     `inject:"usWestSNSClient"`
	Config              *config.Configuration                              `inject:""`
	Mako                mako.MakoClient                                    `inject:""`
}

func (e *entitler) Entitle(ctx context.Context, userID string, channelID string, bitsUsed int64, bitsTotal int64, transactionID string) (*[]mako_client.Emoticon, error) {
	log := logrus.WithFields(logrus.Fields{
		"transactionID": transactionID,
		"userID":        userID,
		"channelID":     channelID,
		"bitsUsed":      bitsUsed,
		"bitsTotal":     bitsTotal,
	})

	if bitsUsed == 0 {
		log.WithField("bitsUsed", bitsUsed).Info("No entitlement because no bits were used")
		return nil, nil
	} else if bitsUsed < 0 {
		msg := "Negative bitsUsed were passed in when we expect a 0 or a positive int64"
		log.WithField("bitsUsed", bitsUsed).Error(msg)
		return nil, errors.New(msg)
	}

	totalNewTiersBreached := 0
	//We want to entitle all thresholds that were breached, even if they weren't enabled so that future enabled tiers
	//will allow use of the emotes on upload automatically
	thresholdsBreached := e.BadgeTiersLogician.GetBreachedThresholds(bitsUsed, bitsTotal)

	if len(thresholdsBreached) == 0 {
		log.Info("0 badge tier emote entitlement thresholds were breached")
		return nil, nil
	} else {
		log.WithField("thresholdsBreached", thresholdsBreached).Info("Badge tier emote entitlement thresholds were breached")
	}

	for _, threshold := range thresholdsBreached {
		if threshold < lowestAllowedTierForEmotes {
			continue
		}

		groupID, err := e.BadgeTierGroupIDDAO.GetByChannelAndThreshold(ctx, channelID, threshold)
		if err != nil {
			log.WithError(err).WithField("threshold", threshold).Error("failed to get new group id for badge tier emote entitlement from dynamo")

			// log a metric for us to alarm on to ensure that the user is correctly entitled
			e.Statter.Inc("emote_entitlement_failure__get_group_id", 1)

			return nil, err
		} else {
			log.WithField("groupID", groupID).Info("successfully fetched group id for badge tier emote entitlement from dynamo")
		}

		err = e.enqueueEmoticonBackfill(threshold, channelID, userID)
		if err != nil {
			log.WithError(err).WithField("threshold", threshold).Error("failed to send sns message for emote entitlement")

			// log a metric for us to alarm on to ensure that the user is correctly entitled
			e.Statter.Inc("emote_entitlement_failure__send_sns_message", 1)

			return nil, err
		} else {
			log.Info("successfully sent sns message for badge tier emote entitlement")
		}

		if groupID == nil {
			totalNewTiersBreached++
		}
	}

	// Used to track the number of times we need end up calling Mako's CreateEmoteGroup API more than once for a
	// single entitlement event. We will use these results to determine whether a BatchCreateEmoteGroup API is necessary.
	// TODO: Remove metric once decision to migrate to Batch API or not is reached
	go e.Statter.Inc(fmt.Sprintf(NEW_TIERS_BREACHED_METRIC, totalNewTiersBreached), 1)

	// TODO(via @graham):
	//   Eventually it’d be nice to allow us to send a pubsub message with the new emotes so that the users don’t need to reload in order to get their new emotes in the emote picker
	//   Whether that happens in Mako or Payday or somewhere else ¯\_(ツ)_/¯
	return &[]mako_client.Emoticon{}, nil
}

func (e *entitler) EntitleAll(ctx context.Context, userID, channelID string, bitsTotal int64) error {
	if strings.Blank(userID) {
		return errors.New("user ID is blank")
	}

	if strings.Blank(channelID) {
		return errors.New("channel ID is blank")
	}

	if bitsTotal == 0 {
		return nil
	}

	// look for all tiers unlocked with the total bits usage.
	// similar to the logic in #Entitle, but treat as one big bits usage with no tiers breached prior.
	thresholdsBreached := e.BadgeTiersLogician.GetBreachedThresholds(bitsTotal, bitsTotal)

	thresholdsAllowed := make([]int64, 0, len(thresholdsBreached))
	for _, threshold := range thresholdsBreached {
		if threshold >= lowestAllowedTierForEmotes {
			thresholdsAllowed = append(thresholdsAllowed, threshold)
		}
	}

	if len(thresholdsAllowed) == 0 {
		return nil
	}

	groups, err := e.BadgeTierGroupIDDAO.BatchGetByChannelAndThresholds(ctx, channelID, thresholdsAllowed)
	if err != nil {
		return errors.Wrap(err, "error retrieving channel bits tier groups")
	}

	groupIDs := make([]string, 0, len(groups))
	for _, group := range groups {
		groupIDs = append(groupIDs, group.GroupID)
	}

	if len(groupIDs) == 0 {
		return nil
	}

	// batch create in group of 25s (25 is the downstream API limit)
	batchSize := 25
	for start := 0; start < len(groupIDs); start += batchSize {
		end := start + batchSize
		if end > len(groupIDs) {
			end = len(groupIDs)
		}

		batch := groupIDs[start:end]

		if err := e.Mako.CreateEmoteGroupEntitlements(ctx, []string{userID}, channelID, batch); err != nil {
			return errors.Wrap(err, "error creating emote group entitlements")
		}
	}

	return nil
}

func (e *entitler) enqueueEmoticonBackfill(threshold int64, channelID string, userID string) error {
	log := logrus.WithFields(logrus.Fields{
		"userID":    userID,
		"channelID": channelID,
		"threshold": threshold,
	})

	message := apimodels.EmoticonBackfillSNSMessage{
		Threshold: threshold,
		ChannelID: channelID,
		UserIDs:   &[]string{userID},
	}

	messageJSON, err := json.Marshal(message)
	if err != nil {
		log.WithError(err).Error("Error marshaling emoticon backfill message json")
		return err
	}

	err = e.SNSClient.PostToTopic(e.Config.EmoticonBackfillSNSTopic, string(messageJSON))
	if err != nil {
		log.WithError(err).Error("Error posting emoticon backfill to SNS")
		return err
	}

	return nil
}
