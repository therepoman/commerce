package badgetieremotes

import (
	"context"
	"errors"
	"fmt"
	"testing"
	"time"

	"code.justin.tv/commerce/gogogadget/pointers"
	mako_client "code.justin.tv/commerce/mako/twirp"
	mako_dashboard "code.justin.tv/commerce/mako/twirp/dashboard"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/dynamo/badge_tier_emote_groupids"
	badgetieremotes_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/badgetiers/products/emotes"
	mako_client_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/mako"
	badge_tier_emote_groupids_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo/badge_tier_emote_groupids"
	sns_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sns"
	"code.justin.tv/commerce/payday/models/api"
	"github.com/hashicorp/go-multierror"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
	"github.com/twitchtv/twirp"
)

func validateAndExtractMultiError(err error) *multierror.Error {
	So(err, ShouldNotBeNil)
	multiErr, ok := err.(*multierror.Error)
	So(ok, ShouldBeTrue)
	return multiErr
}

func validateMultiError(err error, errorType string) {
	multiErr := validateAndExtractMultiError(err)
	So(len(multiErr.Errors), ShouldEqual, 1)
	validateSaverError(multiErr.Errors[0], errorType)
}

func validateSaverError(err error, errorType string) {
	So(err, ShouldNotBeNil)

	saverErr, ok := err.(*SaverError)
	So(ok, ShouldBeTrue)
	So(saverErr.ErrorType, ShouldEqual, errorType)
}

func TestSaver_SaveEmoticons_ErrorsBeforeTierProcessing(t *testing.T) {
	Convey("Given a saver", t, func() {
		ctx := context.Background()
		channelID := "456"
		tier1000 := int64(1000)
		groupIDTier1000 := "1234567890"

		makoClientMock := new(mako_client_mock.MakoClient)
		emoticonValidatorMock := new(badgetieremotes_mock.Validator)
		badgeTierEmotesGroupIDDAOMock := new(badge_tier_emote_groupids_mock.BadgeTierEmoteGroupIDDAO)

		testSaver := saver{
			MakoClient:          makoClientMock,
			Validator:           emoticonValidatorMock,
			BadgeTierGroupIDDAO: badgeTierEmotesGroupIDDAOMock,
		}

		Convey("and a validator that returns an error", func() {
			emoticonValidatorMock.On("GetAllowedTiersMap", mock.Anything, channelID, mock.Anything).Return(nil, errors.New("test error"))

			resp, err := testSaver.SaveEmoticons(ctx, channelID, api.SetBadgesRequest{})

			So(resp, ShouldBeEmpty)
			So(err, ShouldNotBeNil)
		})

		Convey("and a badge tier emotes dao that returns an error", func() {
			allowedTiers := map[int64]bool{
				tier1000: true,
			}

			emoticonValidatorMock.On("GetAllowedTiersMap", mock.Anything, channelID, mock.Anything).Return(allowedTiers, nil)
			badgeTierEmotesGroupIDDAOMock.On("Get", mock.Anything, channelID).Return(nil, errors.New("dummy dao error"))

			resp, err := testSaver.SaveEmoticons(ctx, channelID, api.SetBadgesRequest{
				Tiers: []*api.BadgeTierSetting{
					{
						Threshold: tier1000,
						Enabled:   pointers.BoolP(true),
						EmoticonSettings: &[]api.EmoticonSetting{
							{
								Code:       "tooSmart",
								CodeSuffix: pointers.StringP("Smart"),
							},
						},
					},
				},
			})

			So(resp, ShouldBeEmpty)
			So(err, ShouldNotBeNil)
			So(err.Error(), ShouldContainSubstring, "dummy dao error")
		})

		Convey("and a mako client that returns an error", func() {
			allowedTiers := map[int64]bool{
				tier1000: true,
			}

			emoticonValidatorMock.On("GetAllowedTiersMap", mock.Anything, channelID, mock.Anything).Return(allowedTiers, nil)
			emoteGroup := badge_tier_emote_groupids.BadgeTierEmoteGroupID{
				ChannelID: channelID,
				Threshold: int64(1000),
				GroupID:   groupIDTier1000,
			}
			badgeTierEmotesGroupIDDAOMock.On("Get", mock.Anything, mock.Anything).Return([]*badge_tier_emote_groupids.BadgeTierEmoteGroupID{&emoteGroup}, nil)
			makoClientMock.On("DashboardGetEmotesByGroups", mock.Anything, []string{groupIDTier1000}, mock.Anything).Return(nil, errors.New("test error"))

			resp, err := testSaver.SaveEmoticons(ctx, channelID, api.SetBadgesRequest{
				Tiers: []*api.BadgeTierSetting{
					{
						Threshold: tier1000,
						Enabled:   pointers.BoolP(true),
						EmoticonSettings: &[]api.EmoticonSetting{
							{
								Code:       "tooSmart",
								CodeSuffix: pointers.StringP("Smart"),
							},
						},
					},
				},
			})

			So(resp, ShouldBeEmpty)
			So(err, ShouldNotBeNil)
		})
	})
}

func TestSaver_SaveEmoticons_DeleteEmoticon_FullHappyPath(t *testing.T) {
	Convey("Given a saver", t, func() {
		ctx := context.Background()
		channelID := "456"

		tier5000 := int64(5000)
		groupIDTier5000 := "1234567890"
		tooSmart := mako_dashboard.Emote{
			Id:      "123",
			Code:    "tooSmart",
			GroupId: "BitsBadgeTier-456-5000",
			State:   mako_dashboard.EmoteState_active,
			Domain:  mako_dashboard.Domain_emotes,
			OwnerId: "456",
		}

		makoClientMock := new(mako_client_mock.MakoClient)
		emoticonValidatorMock := new(badgetieremotes_mock.Validator)
		badgeTierEmotesGroupIDDAOMock := new(badge_tier_emote_groupids_mock.BadgeTierEmoteGroupIDDAO)

		testSaver := saver{
			MakoClient:          makoClientMock,
			Validator:           emoticonValidatorMock,
			BadgeTierGroupIDDAO: badgeTierEmotesGroupIDDAOMock,
		}

		allowedTiers := map[int64]bool{
			tier5000: true,
		}

		emoticonGroups := []mako_dashboard.EmoteGroup{
			{
				Id:     groupIDTier5000,
				Emotes: []*mako_dashboard.Emote{&tooSmart},
			},
		}

		request := api.SetBadgesRequest{
			Tiers: []*api.BadgeTierSetting{
				{
					Threshold: tier5000,
					Enabled:   pointers.BoolP(true),
					EmoticonSettings: &[]api.EmoticonSetting{
						{
							Code:           tooSmart.Code,
							EmoteID:        pointers.StringP(tooSmart.Id),
							DeleteEmoticon: pointers.BoolP(true),
						},
					},
				},
			},
		}

		emoticonValidatorMock.On("GetAllowedTiersMap", mock.Anything, channelID, mock.Anything).Return(allowedTiers, nil)
		makoClientMock.On("DashboardGetEmotesByGroups", mock.Anything, []string{groupIDTier5000}, mock.Anything).Return(emoticonGroups, nil)

		emoteGroup := badge_tier_emote_groupids.BadgeTierEmoteGroupID{
			ChannelID: channelID,
			Threshold: tier5000,
			GroupID:   groupIDTier5000,
		}
		badgeTierEmotesGroupIDDAOMock.On("Get", mock.Anything, mock.Anything).Return([]*badge_tier_emote_groupids.BadgeTierEmoteGroupID{&emoteGroup}, nil)

		makoClientMock.On("DeleteEmoticon", mock.Anything, tooSmart.Id, tooSmart.Code, false).Return(nil)

		resp, err := testSaver.SaveEmoticons(ctx, channelID, request)

		So(resp, ShouldBeEmpty)
		So(err, ShouldBeNil)
	})
}

func TestSaver_DeleteEmoticon_EdgeCases(t *testing.T) {
	Convey("Given a saver", t, func() {
		ctx := context.Background()
		tooSmart := mako_client.Emoticon{
			Id:      "123",
			Code:    "tooSmart",
			GroupId: "BitsBadgeTier-456-5000",
			State:   mako_client.EmoteState_active,
			Domain:  mako_client.Domain_emotes,
			OwnerId: "456",
		}

		makoClientMock := new(mako_client_mock.MakoClient)

		testSaver := saver{
			MakoClient: makoClientMock,
		}

		Convey("and a bad request", func() {
			Convey("because it's missing an emoteID", func() {
				emoticonSetting := api.EmoticonSetting{
					Code:           tooSmart.Code,
					DeleteEmoticon: pointers.BoolP(true),
				}

				err := testSaver.DeleteEmoticon(ctx, emoticonSetting)
				validateSaverError(err, DeleteEmoticonValidationError)
			})

			Convey("because of a deleteEmoticon flag set to false", func() {
				emoticonSetting := api.EmoticonSetting{
					Code:           tooSmart.Code,
					DeleteEmoticon: pointers.BoolP(false),
					EmoteID:        pointers.StringP(tooSmart.Id),
				}

				err := testSaver.DeleteEmoticon(ctx, emoticonSetting)
				validateSaverError(err, DeleteEmoticonValidationError)
			})
		})

		Convey("and an error from Mako", func() {
			makoClientMock.On("DeleteEmoticon", mock.Anything, tooSmart.Id, tooSmart.Code, false).Return(errors.New("test error"))

			emoticonSetting := api.EmoticonSetting{
				Code:           tooSmart.Code,
				DeleteEmoticon: pointers.BoolP(true),
				EmoteID:        pointers.StringP(tooSmart.Id),
			}

			err := testSaver.DeleteEmoticon(ctx, emoticonSetting)
			validateSaverError(err, DeleteEmoticonDownstreamError)
		})
	})
}

func TestSaver_SaveEmoticons_EdgeCasesBefore_GetUploadConfiguration(t *testing.T) {
	Convey("Given a saver", t, func() {
		ctx := context.Background()
		channelID := "456"

		tier5000 := int64(5000)
		groupIDTier5000 := "1234567890"
		tooSmart := mako_dashboard.Emote{
			Id:      "123",
			Code:    "tooSmart",
			GroupId: "BitsBadgeTier-456-5000",
			State:   mako_dashboard.EmoteState_active,
			Domain:  mako_dashboard.Domain_emotes,
			OwnerId: "456",
		}

		makoClientMock := new(mako_client_mock.MakoClient)
		emoticonValidatorMock := new(badgetieremotes_mock.Validator)
		badgeTierEmotesLogicianMock := new(badgetieremotes_mock.Logician)
		badgeTierEmotesGroupIDDAOMock := new(badge_tier_emote_groupids_mock.BadgeTierEmoteGroupIDDAO)

		testSaver := saver{
			MakoClient:              makoClientMock,
			Validator:               emoticonValidatorMock,
			BadgeTierEmotesLogician: badgeTierEmotesLogicianMock,
			BadgeTierGroupIDDAO:     badgeTierEmotesGroupIDDAOMock,
		}

		allowedTiers := map[int64]bool{
			tier5000: true,
		}

		emoticonGroups := []mako_dashboard.EmoteGroup{
			{
				Id:     groupIDTier5000,
				Emotes: []*mako_dashboard.Emote{},
			},
		}

		request := api.SetBadgesRequest{
			Tiers: []*api.BadgeTierSetting{
				{
					Threshold: tier5000,
					Enabled:   pointers.BoolP(true),
					EmoticonSettings: &[]api.EmoticonSetting{
						{
							Code:       tooSmart.Code,
							CodeSuffix: pointers.StringP("Smart"),
							ImageID1x:  pointers.StringP("test_image_id_1x"),
							ImageID2x:  pointers.StringP("test_image_id_2x"),
							ImageID4x:  pointers.StringP("test_image_id_4x"),
						},
					},
				},
			},
		}

		emoticonValidatorMock.On("GetAllowedTiersMap", mock.Anything, channelID, mock.Anything).Return(allowedTiers, nil)
		makoClientMock.On("DashboardGetEmotesByGroups", mock.Anything, []string{groupIDTier5000}, mock.Anything).Return(emoticonGroups, nil)
		badgeTierEmotesGroupIDDAOMock.On("Get", mock.Anything, mock.Anything).Return(nil, nil)

		Convey("and an emoticonValidator that returns an error", func() {
			emoticonValidatorMock.On("ValidateUpload", mock.Anything, mock.Anything).Return(NewSaverError(errors.New("test error"), CreateEmoticonTierNotAllowedError))

			resp, err := testSaver.SaveEmoticons(ctx, channelID, request)

			So(resp, ShouldBeEmpty)
			validateMultiError(err, CreateEmoticonTierNotAllowedError)
		})

		Convey("and a badge tier emotes logician that returns an error", func() {
			emoticonValidatorMock.On("ValidateUpload", mock.Anything, mock.Anything).Return(nil)
			badgeTierEmotesLogicianMock.On("GetAndStoreGroupIDForBadgeTier", mock.Anything, channelID, tier5000).Return(nil, false, errors.New("dummy logician error"))

			resp, err := testSaver.SaveEmoticons(ctx, channelID, request)

			So(resp, ShouldBeEmpty)
			So(err, ShouldNotBeNil)
			So(err.Error(), ShouldContainSubstring, "dummy logician error")
		})

		Convey("and a badge tier emotes logician that returns a nil groupID mapping", func() {
			emoticonValidatorMock.On("ValidateUpload", mock.Anything, mock.Anything).Return(nil)
			badgeTierEmotesLogicianMock.On("GetAndStoreGroupIDForBadgeTier", mock.Anything, channelID, tier5000).Return(nil, false, nil)

			resp, err := testSaver.SaveEmoticons(ctx, channelID, request)

			So(resp, ShouldBeEmpty)
			So(err, ShouldNotBeNil)
		})
	})
}

func TestSaver_SaveEmoticons_ImmediateSave(t *testing.T) {
	Convey("Given a saver", t, func() {
		ctx := context.Background()

		channelID := "46024993"
		codeSuffix := "RANGER"
		code := "kenobi" + codeSuffix
		threshold := int64(1000)
		groupID := fmt.Sprintf("BitsBadgeTier-%s-%d", channelID, threshold)

		validatorMock := new(badgetieremotes_mock.Validator)
		badgeTierEmotesGroupDaoMock := new(badge_tier_emote_groupids_mock.BadgeTierEmoteGroupIDDAO)
		makoClientMock := new(mako_client_mock.MakoClient)
		badgeTierEmotesLogicianMock := new(badgetieremotes_mock.Logician)
		snsClientMock := new(sns_mock.ISNSClient)
		configMock := config.Configuration{
			EmoticonBackfillSNSTopic: "test_topic",
		}

		testSaver := saver{
			Validator:               validatorMock,
			BadgeTierGroupIDDAO:     badgeTierEmotesGroupDaoMock,
			MakoClient:              makoClientMock,
			BadgeTierEmotesLogician: badgeTierEmotesLogicianMock,
			SNSClient:               snsClientMock,
			Config:                  &configMock,
		}

		request := api.SetBadgesRequest{
			Tiers: []*api.BadgeTierSetting{
				{
					Threshold: threshold,
					Enabled:   pointers.BoolP(true),
					EmoticonSettings: &[]api.EmoticonSetting{
						{
							Code:       code,
							CodeSuffix: pointers.StringP(codeSuffix),
							ImageID1x:  pointers.StringP("image-kenobiRANGER-1x"),
							ImageID2x:  pointers.StringP("image-kenobiRANGER-2x"),
							ImageID4x:  pointers.StringP("image-kenobiRANGER-4x"),
						},
					},
				},
			},
		}

		//testing of the values from these functional calls are handled in the validator struct which we're mocking here.
		makoClientMock.On("DashboardGetEmotesByGroups", ctx, mock.Anything,
			[]mako_dashboard.EmoteState{mako_dashboard.EmoteState_active, mako_dashboard.EmoteState_pending}).Return(
			[]mako_dashboard.EmoteGroup{}, nil)
		badgeTierEmotesGroupDaoMock.On("Get", ctx, channelID).Return([]*badge_tier_emote_groupids.BadgeTierEmoteGroupID{
			{
				ChannelID:    channelID,
				Threshold:    threshold,
				GroupID:      groupID,
				CreatedAt:    time.Now(),
				IsBackfilled: false,
			},
		}, nil)
		validatorMock.On("GetAllowedTiersMap", ctx, channelID, request.Tiers).Return(map[int64]bool{1000: true}, nil)
		snsClientMock.On("PostToTopic", mock.Anything, mock.Anything).Return(nil)

		Convey("when validator.ValidateUpload thinks things are invalid", func() {
			animeBearErr := errors.New("ANIME BEARS OVERPOPULATION")
			validatorMock.On("ValidateUpload", ctx, mock.Anything).Return(animeBearErr)

			resp, err := testSaver.SaveEmoticons(ctx, channelID, request)
			multiErr := validateAndExtractMultiError(err)
			So(multiErr.WrappedErrors(), ShouldHaveLength, 1)
			So(multiErr.WrappedErrors()[0], ShouldEqual, animeBearErr)
			So(resp, ShouldBeEmpty)
		})

		Convey("when validator.ValidateUpload thinks everything is on the up and also up", func() {
			validatorMock.On("ValidateUpload", ctx, mock.Anything).Return(nil)

			Convey("when BadgeTierEmotesLogician.GetAndStoreGroupIDForBadgeTier returns an error", func() {
				animeBearErr := errors.New("ANIME BEAR INVASION, EMOTES DELAYED")
				badgeTierEmotesLogicianMock.On("GetAndStoreGroupIDForBadgeTier", ctx, channelID, threshold).Return(
					nil, false, animeBearErr)

				resp, err := testSaver.SaveEmoticons(ctx, channelID, request)
				multiErr := validateAndExtractMultiError(err)
				So(multiErr.WrappedErrors(), ShouldHaveLength, 1)
				So(multiErr.WrappedErrors()[0], ShouldEqual, animeBearErr)
				So(resp, ShouldBeEmpty)
			})

			Convey("when BadgeTierEmotesLogician.GetAndStoreGroupIDForBadgeTier returns our tier", func() {
				Convey("when the tier is backfilled already", func() {
					badgeTierEmotesLogicianMock.On("GetAndStoreGroupIDForBadgeTier", ctx, channelID, threshold).Return(
						&badge_tier_emote_groupids.BadgeTierEmoteGroupID{GroupID: groupID, IsBackfilled: true}, false, nil)

					Convey("a backfill is not enqueued", func() {
						snsClientMock.AssertNumberOfCalls(t, "PostToTopic", 0)
					})

					Convey("when Mako.CreateEmote errors", func() {
						makoClientMock.On("CreateEmote", ctx, channelID, mock.Anything).Return(mako_dashboard.CreateEmoteResponse{}, errors.New("ANIME BEARS"))

						resp, err := testSaver.SaveEmoticons(ctx, channelID, request)
						validateMultiError(err, "CreateEmoticonDownstreamError")
						So(resp, ShouldBeEmpty)
					})

					Convey("when Mako.CreateEmote succeeds", func() {
						makoClientMock.On("CreateEmote", ctx, channelID, mock.Anything).Return(mako_dashboard.CreateEmoteResponse{}, nil)

						Convey("when cache purger is working too", func() {
							resp, err := testSaver.SaveEmoticons(ctx, channelID, request)
							So(err, ShouldBeNil)
							So(resp, ShouldBeEmpty)
						})
					})
				})

				Convey("when the tier is not backfilled yet", func() {
					makoClientMock.On("CreateEmote", ctx, channelID, mock.Anything).Return(mako_dashboard.CreateEmoteResponse{}, nil)
					badgeTierEmotesLogicianMock.On("GetAndStoreGroupIDForBadgeTier", ctx, channelID, threshold).Return(
						&badge_tier_emote_groupids.BadgeTierEmoteGroupID{GroupID: groupID, IsBackfilled: false}, false, nil)

					Convey("a backfill is enqueued", func() {
						resp, err := testSaver.SaveEmoticons(ctx, channelID, request)
						So(err, ShouldBeNil)
						So(resp, ShouldBeEmpty)
						snsClientMock.AssertNumberOfCalls(t, "PostToTopic", 1)
					})
				})
			})
		})
	})

}

func TestSaver_SaveEmoticons_Noop(t *testing.T) {
	Convey("Given a saver", t, func() {
		ctx := context.Background()
		channelID := "456"

		tier75000 := int64(75000)
		groupIDTier75000 := "1234567890"
		tooSmart := mako_client.Emoticon{
			Id:      "123",
			Code:    "tooSmart",
			GroupId: groupIDTier75000,
			State:   mako_client.EmoteState_active,
			Domain:  mako_client.Domain_emotes,
			OwnerId: "456",
		}

		makoClientMock := new(mako_client_mock.MakoClient)
		emoticonValidatorMock := new(badgetieremotes_mock.Validator)
		badgeTierEmotesGroupIDDAOMock := new(badge_tier_emote_groupids_mock.BadgeTierEmoteGroupIDDAO)

		testSaver := saver{
			MakoClient:          makoClientMock,
			Validator:           emoticonValidatorMock,
			BadgeTierGroupIDDAO: badgeTierEmotesGroupIDDAOMock,
		}

		allowedTiers := map[int64]bool{
			tier75000: false,
		}

		Convey("and a noop request", func() {
			emoticonValidatorMock.On("GetAllowedTiersMap", mock.Anything, channelID, mock.Anything).Return(allowedTiers, nil)
			badgeTierEmotesGroupIDDAOMock.On("Get", mock.Anything, mock.Anything).Return(nil, nil)

			Convey("because it had no tiers", func() {
				request := api.SetBadgesRequest{
					Tiers: []*api.BadgeTierSetting{},
				}

				resp, err := testSaver.SaveEmoticons(ctx, channelID, request)

				So(resp, ShouldBeEmpty)
				So(err, ShouldBeNil)
			})

			Convey("because the tier should be skipped", func() {
				Convey("because each of its tiers was nil", func() {
					request := api.SetBadgesRequest{
						Tiers: []*api.BadgeTierSetting{
							nil,
						},
					}

					resp, err := testSaver.SaveEmoticons(ctx, channelID, request)

					So(resp, ShouldBeEmpty)
					So(err, ShouldBeNil)
				})

				Convey("because emoticonSettings was nil", func() {
					request := api.SetBadgesRequest{
						Tiers: []*api.BadgeTierSetting{
							{
								Threshold: tier75000,
							},
						},
					}

					resp, err := testSaver.SaveEmoticons(ctx, channelID, request)

					So(resp, ShouldBeEmpty)
					So(err, ShouldBeNil)
				})

				Convey("because emoticonSettings was empty", func() {
					request := api.SetBadgesRequest{
						Tiers: []*api.BadgeTierSetting{
							{
								Threshold:        tier75000,
								EmoticonSettings: &[]api.EmoticonSetting{},
							},
						},
					}

					resp, err := testSaver.SaveEmoticons(ctx, channelID, request)

					So(resp, ShouldBeEmpty)
					So(err, ShouldBeNil)
				})
			})

			Convey("because the tier was not allowed", func() {
				makoClientMock.On("DashboardGetEmotesByGroups", mock.Anything, []string{groupIDTier75000}, mock.Anything).Return(nil, nil)
				emoticonValidatorMock.On("ValidateUpload", mock.Anything, mock.Anything).Return(errors.New("test error"))

				request := api.SetBadgesRequest{
					Tiers: []*api.BadgeTierSetting{
						{
							Threshold: tier75000,
							Enabled:   pointers.BoolP(true),
							EmoticonSettings: &[]api.EmoticonSetting{
								{
									Code:       tooSmart.Code,
									CodeSuffix: pointers.StringP("Smart"),
								},
							},
						},
					},
				}

				resp, err := testSaver.SaveEmoticons(ctx, channelID, request)

				So(resp, ShouldBeEmpty)
				So(err, ShouldNotBeNil)
			})
		})
	})
}

func TestSaver_CreateEmoticon(t *testing.T) {
	Convey("Given a saver", t, func() {
		emoteID := "123"
		code := "tooSmart"
		groupID := "BitsBadgeTier-123-1000"

		makoClientMock := new(mako_client_mock.MakoClient)
		emoticonValidatorMock := new(badgetieremotes_mock.Validator)

		testSaver := saver{
			MakoClient: makoClientMock,
			Validator:  emoticonValidatorMock,
		}

		config := api.EmoticonUploadConfiguration{
			Code:       code,
			CodeSuffix: "Smart",
			ImageUploadConfig1X: api.ImageUploadConfig{
				ImageID: "id-1",
			},
			ImageUploadConfig2X: api.ImageUploadConfig{
				ImageID: "id-2",
			},
			ImageUploadConfig4X: api.ImageUploadConfig{
				ImageID: "id-3",
			},
			GroupID: groupID,
			OwnerID: "456",
		}

		makoResp := mako_dashboard.CreateEmoteResponse{
			Id:       emoteID,
			Code:     code,
			GroupIds: []string{groupID},
			State:    mako_dashboard.EmoteState_active,
		}

		Convey("and an emoticon upload config", func() {
			Convey("with a missing image ID", func() {
				config.ImageUploadConfig1X.ImageID = ""

				resp, err := testSaver.CreateEmoticon(context.Background(), config)

				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})

			Convey("with valid values", func() {
				Convey("and a mako client that errors on CreateEmoticon", func() {
					makoClientMock.On("CreateEmote", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(mako_dashboard.CreateEmoteResponse{}, errors.New("test error"))

					resp, err := testSaver.CreateEmoticon(context.Background(), config)

					So(err, ShouldNotBeNil)
					So(resp, ShouldBeNil)
				})

				Convey("and a mako client that errors on CreateEmoticon with a code already exists error", func() {
					makoClientMock.On("CreateEmote", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(mako_dashboard.CreateEmoteResponse{}, mako_client.NewClientError(
						twirp.InvalidArgumentError("code", "must be unique"),
						mako_client.ErrCodeCodeNotUnique))

					resp, err := testSaver.CreateEmoticon(context.Background(), config)

					So(err, ShouldNotBeNil)
					saverErr, ok := err.(*SaverError)
					So(ok, ShouldBeTrue)
					So(saverErr.ErrorType, ShouldEqual, CreateEmoticonCodeAlreadyExistsError)
					So(resp, ShouldBeNil)
				})

				Convey("and a mako client that errors on CreateEmoticon with a code unacceptable error", func() {
					makoClientMock.On("CreateEmote", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(mako_dashboard.CreateEmoteResponse{}, mako_client.NewClientError(
						twirp.InvalidArgumentError("code", "is unacceptable"),
						mako_client.ErrCodeCodeUnacceptable))

					resp, err := testSaver.CreateEmoticon(context.Background(), config)

					So(err, ShouldNotBeNil)
					saverErr, ok := err.(*SaverError)
					So(ok, ShouldBeTrue)
					So(saverErr.ErrorType, ShouldEqual, CreateEmoticonCodeUnacceptableError)
					So(resp, ShouldBeNil)
				})

				Convey("and a mako client that errors on CreateEmoticon with a user owned emote limit reached error", func() {
					makoClientMock.On("CreateEmote", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(mako_dashboard.CreateEmoteResponse{}, mako_client.NewClientError(
						twirp.NewError(twirp.FailedPrecondition, "user cannot own any more emotes of this type"),
						mako_client.ErrCodeUserOwnedEmoteLimitReached))

					resp, err := testSaver.CreateEmoticon(context.Background(), config)

					So(err, ShouldNotBeNil)
					saverErr, ok := err.(*SaverError)
					So(ok, ShouldBeTrue)
					So(saverErr.ErrorType, ShouldEqual, CreateEmoticonUserOwnedEmoteLimitReachedError)
					So(resp, ShouldBeNil)
				})

				Convey("and a mako client that returns a valid response", func() {
					makoClientMock.On("CreateEmote", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(makoResp, nil)

					resp, err := testSaver.CreateEmoticon(context.Background(), config)

					So(err, ShouldBeNil)
					So(resp, ShouldResemble, &makoResp)
				})

				Convey("and a mako client that returns a valid response with default image", func() {
					config.ImageSource = api.DefaultLibraryImage
					makoClientMock.On("CreateEmote", mock.Anything, mock.Anything, config).Return(makoResp, nil)
					resp, err := testSaver.CreateEmoticon(context.Background(), config)

					So(err, ShouldBeNil)
					So(resp, ShouldResemble, &makoResp)
				})
			})
		})
	})
}
