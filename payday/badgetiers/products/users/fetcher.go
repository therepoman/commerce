package badgeusers

import (
	"context"
	"errors"

	"code.justin.tv/commerce/gogogadget/strings"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	"code.justin.tv/commerce/payday/clients/pantheon"
	"code.justin.tv/commerce/payday/utils/experiments"
)

type Fetcher interface {
	GetUnlockedUsersCountsByThresholds(ctx context.Context, channelID string, tierThresholds []int64) (map[int64]int64, error)
}

type fetcher struct {
	Pantheon          pantheon.IPantheonClientWrapper     `inject:""`
	ExperimentFetcher experiments.ExperimentConfigFetcher `inject:""`
}

func NewFetcher() Fetcher {
	return &fetcher{}
}

func (f *fetcher) GetUnlockedUsersCountsByThresholds(ctx context.Context, channelID string, tierThresholds []int64) (map[int64]int64, error) {
	if strings.Blank(channelID) {
		return nil, errors.New("channelID is required")
	}

	countsByThresholds := make(map[int64]int64)

	if len(tierThresholds) == 0 {
		return countsByThresholds, nil
	}

	resp, err := f.Pantheon.GetBucketedLeaderboardCounts(ctx, pantheon.PantheonBitsUsageDomain, pantheonrpc.TimeUnit_ALLTIME, channelID, tierThresholds, true)
	if err != nil {
		return nil, err
	}

	for _, bucket := range resp.Buckets {
		countsByThresholds[bucket.Threshold] = bucket.Count
	}

	// default to 0 users for missing tiers
	var missingThresholds []int64
	for _, threshold := range tierThresholds {
		if _, present := countsByThresholds[threshold]; !present {
			countsByThresholds[threshold] = 0
			missingThresholds = append(missingThresholds, threshold)
		}
	}

	if len(missingThresholds) > 0 {
		log.WithFields(log.Fields{
			"channel_id":         channelID,
			"missing_thresholds": missingThresholds,
		}).Warn("missing unlocked users counts data for some badge tier thresholds, default them to 0")
	}

	return countsByThresholds, nil
}
