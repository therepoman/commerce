package badgeusers

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	"code.justin.tv/commerce/payday/clients/pantheon"
	pantheon_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/pantheon"
	experiments_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/utils/experiments"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestFetcher(t *testing.T) {
	Convey("test GetUnlockedUsersCountsByThresholds", t, func() {
		pantheonMock := new(pantheon_mock.IPantheonClientWrapper)
		experimentFetcherMock := new(experiments_mock.ExperimentConfigFetcher)

		fetcher := &fetcher{
			Pantheon:          pantheonMock,
			ExperimentFetcher: experimentFetcherMock,
		}

		channelID := "123"
		tierThresholds := []int64{1, 10, 100, 1000}

		Convey("with empty channel ID", func() {
			_, err := fetcher.GetUnlockedUsersCountsByThresholds(context.Background(), "", nil)
			So(err, ShouldNotBeNil)
		})

		Convey("with no threshold", func() {
			countsByThreshold, err := fetcher.GetUnlockedUsersCountsByThresholds(context.Background(), channelID, nil)

			So(err, ShouldBeNil)
			So(countsByThreshold, ShouldBeEmpty)
			pantheonMock.AssertNotCalled(t, "GetBucketedLeaderboardCounts")
		})

		Convey("for happy paths", func() {
			var pantheonResp *pantheonrpc.GetBucketedLeaderboardCountsResp
			bucketsWithCounts := []*pantheonrpc.Bucket{
				{
					Threshold: 1,
					Count:     99,
				},
				{
					Threshold: 10,
					Count:     88,
				},
				{
					Threshold: 100,
					Count:     77,
				},
				{
					Threshold: 1000,
					Count:     66,
				},
			}

			Convey("it should construct a counts by threshold map based on pantheon results", func() {
				Convey("when there are counts for thresholds", func() {
					pantheonResp = &pantheonrpc.GetBucketedLeaderboardCountsResp{Buckets: bucketsWithCounts}
				})

				Convey("when there is no count results for thresholds", func() {
					pantheonResp = &pantheonrpc.GetBucketedLeaderboardCountsResp{Buckets: nil}
				})

				pantheonMock.On("GetBucketedLeaderboardCounts", mock.Anything, pantheon.PantheonBitsUsageDomain, pantheonrpc.TimeUnit_ALLTIME, channelID, tierThresholds, true).Return(pantheonResp, nil)
				countsByThreshold, err := fetcher.GetUnlockedUsersCountsByThresholds(context.Background(), channelID, tierThresholds)

				So(err, ShouldBeNil)
				So(countsByThreshold, ShouldNotBeNil)
				So(countsByThreshold, ShouldHaveLength, len(tierThresholds))

				for _, bucket := range pantheonResp.Buckets {
					So(countsByThreshold[bucket.Threshold], ShouldEqual, bucket.Count)
				}
			})
		})

		Convey("for failures", func() {
			Convey("it should return an error if pantheon fails", func() {
				pantheonMock.On("GetBucketedLeaderboardCounts", mock.Anything, pantheon.PantheonBitsUsageDomain, pantheonrpc.TimeUnit_ALLTIME, channelID, tierThresholds, true).Return(nil, errors.New("pantheon error"))
				_, err := fetcher.GetUnlockedUsersCountsByThresholds(context.Background(), channelID, tierThresholds)

				So(err, ShouldNotBeNil)

			})
		})
	})
}
