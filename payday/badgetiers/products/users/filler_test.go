package badgeusers

import (
	"context"
	"errors"
	"testing"

	badgeusers_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/badgetiers/products/users"
	"code.justin.tv/commerce/payday/models/api"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestFiller_PopulateWithUnlockedUsersCounts(t *testing.T) {
	Convey("Given a filler", t, func() {
		ctx := context.Background()
		channelID := "123"
		tier1 := int64(1)

		fetcherMock := new(badgeusers_mock.Fetcher)

		testFiller := filler{
			BadgeUsersFetcher: fetcherMock,
		}

		Convey("and a request", func() {
			Convey("with at least one tier", func() {
				tierMap := map[int64]*api.BadgeTier{
					tier1: {
						Enabled:   true,
						Threshold: tier1,
						ChannelId: channelID,
					},
				}

				Convey("and a fetcher that succeeds", func() {
					Convey("and returns a matching tier", func() {
						countsByThresholds := map[int64]int64{
							tier1: 123,
						}

						fetcherMock.On("GetUnlockedUsersCountsByThresholds", mock.Anything, mock.Anything, mock.Anything).Return(countsByThresholds, nil)

						newTierMap, err := testFiller.PopulateWithUnlockedUsersCounts(ctx, channelID, tierMap)

						So(newTierMap, ShouldNotBeNil)
						So(newTierMap[tier1].UnlockedUsersCount, ShouldEqual, countsByThresholds[tier1])
						So(err, ShouldBeNil)
					})

					Convey("and returns no matching tier", func() {
						countsByThresholds := map[int64]int64{
							420: 123,
						}

						fetcherMock.On("GetUnlockedUsersCountsByThresholds", mock.Anything, mock.Anything, mock.Anything).Return(countsByThresholds, nil)

						newTierMap, err := testFiller.PopulateWithUnlockedUsersCounts(ctx, channelID, tierMap)

						So(newTierMap, ShouldNotBeNil)
						So(newTierMap[tier1].UnlockedUsersCount, ShouldBeZeroValue)
						So(err, ShouldBeNil)
					})
				})

				Convey("and a fetcher that errors", func() {
					fetcherMock.On("GetUnlockedUsersCountsByThresholds", mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("test error"))

					newTierMap, err := testFiller.PopulateWithUnlockedUsersCounts(ctx, channelID, tierMap)

					So(newTierMap, ShouldBeNil)
					So(err, ShouldNotBeNil)
				})
			})

			Convey("with no tiers", func() {
				tierMap := map[int64]*api.BadgeTier{}

				newTierMap, err := testFiller.PopulateWithUnlockedUsersCounts(ctx, channelID, tierMap)

				So(newTierMap, ShouldBeNil)
				So(err, ShouldBeNil)
			})
		})
	})
}
