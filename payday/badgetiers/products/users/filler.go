package badgeusers

import (
	"context"
	"errors"
	"net/http"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/api/constants"
	httperror "code.justin.tv/commerce/payday/errors/http"
	"code.justin.tv/commerce/payday/models/api"
)

type Filler interface {
	PopulateWithUnlockedUsersCounts(ctx context.Context, channelID string, tierMap map[int64]*api.BadgeTier) (map[int64]*api.BadgeTier, error)
}

type filler struct {
	BadgeUsersFetcher Fetcher `inject:""`
}

func NewFiller() Filler {
	return &filler{}
}

func (f *filler) PopulateWithUnlockedUsersCounts(ctx context.Context, channelID string, tierMap map[int64]*api.BadgeTier) (map[int64]*api.BadgeTier, error) {
	if len(tierMap) == 0 {
		return nil, nil
	}

	tierThresholds := make([]int64, 0, len(tierMap))
	for threshold := range tierMap {
		tierThresholds = append(tierThresholds, threshold)
	}

	countsByThreshold, err := f.BadgeUsersFetcher.GetUnlockedUsersCountsByThresholds(ctx, channelID, tierThresholds)
	if err != nil {
		msg := "Error getting unlocked users counts for badge tiers"
		log.WithField("channel_id", channelID).WithError(err).Error(msg)
		return nil, httperror.NewWithCause(constants.DownstreamDependencyError, http.StatusInternalServerError, errors.New(msg))
	}

	for threshold, tier := range tierMap {
		if count, present := countsByThreshold[threshold]; present {
			tier.UnlockedUsersCount = count
		}
	}

	return tierMap, nil
}
