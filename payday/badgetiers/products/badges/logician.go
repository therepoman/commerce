package bitsbadges

import (
	"context"
	"sort"
	"strconv"

	clueservice "code.justin.tv/chat/tmi/client"
	"code.justin.tv/commerce/gogogadget/strings"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	"code.justin.tv/commerce/payday/badgetiers/fetcher"
	"code.justin.tv/commerce/payday/clients/pantheon"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/metrics"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"github.com/twitchtv/twirp"
)

const (
	BadgeSetID                = "bits"
	ClueCall                  = "clue_chat_badge_call"
	UserIsBannedBadgeCheck    = "user_is_banned_badge_check"
	UserIsNotBannedBadgeCheck = "user_is_not_banned_badge_check"
)

type Logician interface {
	GetUserBitsBadgesAvailableForChannel(ctx context.Context, userID string, channelID string) ([]*paydayrpc.Badge, error)
}
type logician struct {
	Pantheon    pantheon.IPantheonClientWrapper `inject:""`
	Fetcher     fetcher.Fetcher                 `inject:""`
	Metrics     metrics.Statter                 `inject:""`
	ClueService clueservice.Client              `inject:""`
}

func NewLogician() Logician {
	return &logician{}
}

// GetUserBitsBadgesAvailableForChannel returns the highest badge a user has available in a given channel. It calls Pantheon to get their spent bits amount.
// Availability is determined by the spent bits amount being over the threshold and the Broadcaster having activated the bits badge.
// This returns a list of ALL available bits badges to the user in the channel. Availability follows the same rules.
// 		- the results are sorted by version in increasing order e.g. {1, 100, 1000}
func (f *logician) GetUserBitsBadgesAvailableForChannel(ctx context.Context, userID string, channelID string) (badges []*paydayrpc.Badge, err error) {
	if strings.Blank(userID) {
		return nil, twirp.InvalidArgumentError("userID", "cannot have empty user id")
	}
	if strings.Blank(channelID) {
		return nil, twirp.InvalidArgumentError("ChannelId", "cannot have empty channel id")
	}

	userLeaderboard, err := f.Pantheon.GetLeaderboard(ctx, pantheon.PantheonBitsUsageDomain, pantheonrpc.TimeUnit_ALLTIME, channelID, userID, 0, 0, false)
	if err != nil {
		msg := "Error getting leaderboard entry (spent bits) for user in channel from Pantheon for determining badges available to user."
		log.WithError(err).WithFields(log.Fields{
			"channelId": channelID,
			"userId":    userID,
		}).Error(msg)

		return nil, twirp.InternalErrorWith(errors.Notef(err, msg))
	}

	if userLeaderboard == nil ||
		userLeaderboard.EntryContext == nil ||
		userLeaderboard.EntryContext.Entry == nil {
		// user never spent bits, ergo no bits badges
		return []*paydayrpc.Badge{}, nil
	}

	if userLeaderboard.EntryContext.Entry.Moderated {
		f.Metrics.Inc(ClueCall, 1)
		go f.VerifyAndUnmoderateIfNotBanned(context.Background(), userID, channelID)
	} else {
		f.Metrics.Inc(ClueCall, 0)
	}

	// get badge tiers
	bitsBadgeTiers, err := f.Fetcher.Fetch(ctx, channelID)
	if err != nil {
		msg := "Error getting badge tiers from dynamo for determining badges available to user."
		log.WithError(err).Error(msg)
		return nil, twirp.InternalErrorWith(errors.Notef(err, msg))
	}

	var availableBadgeVersions []int64
	for _, badge := range bitsBadgeTiers.BadgeTiers {
		if badge.Enabled && userLeaderboard.EntryContext.Entry.Score >= badge.Threshold {
			availableBadgeVersions = append(availableBadgeVersions, badge.Threshold)
		}
	}
	sort.Slice(availableBadgeVersions, func(i, j int) bool {
		return availableBadgeVersions[i] < availableBadgeVersions[j]
	})

	availableBadges := make([]*paydayrpc.Badge, len(availableBadgeVersions))
	for index, version := range availableBadgeVersions {
		availableBadges[index] = &paydayrpc.Badge{
			BadgeSetId:      "bits",
			BadgeSetVersion: strconv.FormatInt(version, 10),
		}
	}

	// return all badges lower than balance
	return availableBadges, nil
}

func (f *logician) VerifyAndUnmoderateIfNotBanned(ctx context.Context, userID string, channelID string) {
	// GetBanStatus returns whether a user is banned or timed out from a channel.
	_, isBanned, err := f.ClueService.GetBanStatus(ctx, channelID, userID, nil, nil)

	if err != nil {
		log.WithError(err).Error("Error calling Clue to check a user is banned.")
		return
	}

	if isBanned {
		// user is banned
		f.Metrics.Inc(UserIsBannedBadgeCheck, 1)
		f.Metrics.Inc(UserIsNotBannedBadgeCheck, 0)
	} else {
		f.Metrics.Inc(UserIsBannedBadgeCheck, 0)
		f.Metrics.Inc(UserIsNotBannedBadgeCheck, 1)

		moderateReq := pantheonrpc.ModerateEntryReq{
			Domain:           pantheon.PantheonBitsUsageDomain,
			GroupingKey:      channelID,
			EntryKey:         userID,
			ModerationAction: pantheonrpc.ModerationAction_UNMODERATE,
		}

		err = f.Pantheon.ModerateEntry(ctx, moderateReq)
		if err != nil {
			log.WithError(err).Error("Error calling Pantheon to unban user")
			return
		}
	}
}
