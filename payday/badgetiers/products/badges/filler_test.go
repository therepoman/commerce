package bitsbadges

import (
	"context"
	"errors"
	"testing"
	"time"

	"code.justin.tv/chat/badges/app/models"
	"code.justin.tv/commerce/gogogadget/pointers"
	chatbadges_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/chatbadges"
	"code.justin.tv/commerce/payday/models/api"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestFiller_PopulateWithCustomImages(t *testing.T) {
	Convey("Given a filler", t, func() {
		ctx := context.Background()
		channelID := "123"
		tier1 := int64(1)

		chatBadgeClientMock := new(chatbadges_mock.IChatBadgesClient)

		testFiller := filler{
			ChatBadgesClient: chatBadgeClientMock,
		}

		Convey("and a badge service that succeeds", func() {
			Convey("and returns badges", func() {
				Convey("that match the requested tiers", func() {
					tierMap := map[int64]*api.BadgeTier{
						tier1: {
							Enabled:   true,
							Threshold: tier1,
							ChannelId: channelID,
						},
					}

					lastUpdated := time.Now()

					badgeSet := &models.BadgeSet{
						Versions: map[string]models.BadgeVersion{
							"1": {
								ImageURL1x:  pointers.StringP("image/url/1x"),
								ImageURL2x:  pointers.StringP("image/url/2x"),
								ImageURL4x:  pointers.StringP("image/url/4x"),
								Title:       "test title",
								LastUpdated: &lastUpdated,
							},
						},
					}

					chatBadgeClientMock.On("GetBitsImages", mock.Anything, channelID, mock.Anything).Return(badgeSet, nil)

					newTierMap, err := testFiller.PopulateWithCustomImages(ctx, channelID, tierMap)

					So(newTierMap, ShouldNotBeNil)
					So(err, ShouldBeNil)
				})

				Convey("that do not match the requested tiers", func() {
					tierMap := map[int64]*api.BadgeTier{
						tier1: {
							Enabled:   true,
							Threshold: tier1,
							ChannelId: channelID,
						},
					}

					lastUpdated := time.Now()

					badgeSet := &models.BadgeSet{
						Versions: map[string]models.BadgeVersion{
							"420": {
								ImageURL1x:  pointers.StringP("image/url/1x"),
								ImageURL2x:  pointers.StringP("image/url/2x"),
								ImageURL4x:  pointers.StringP("image/url/4x"),
								Title:       "test title",
								LastUpdated: &lastUpdated,
							},
						},
					}

					chatBadgeClientMock.On("GetBitsImages", mock.Anything, channelID, mock.Anything).Return(badgeSet, nil)

					newTierMap, err := testFiller.PopulateWithCustomImages(ctx, channelID, tierMap)

					So(newTierMap, ShouldNotBeNil)
					So(err, ShouldBeNil)
				})

				Convey("that do not have properly formed version names", func() {
					tierMap := map[int64]*api.BadgeTier{
						tier1: {
							Enabled:   true,
							Threshold: tier1,
							ChannelId: channelID,
						},
					}

					lastUpdated := time.Now()

					badgeSet := &models.BadgeSet{
						Versions: map[string]models.BadgeVersion{
							"one": {
								ImageURL1x:  pointers.StringP("image/url/1x"),
								ImageURL2x:  pointers.StringP("image/url/2x"),
								ImageURL4x:  pointers.StringP("image/url/4x"),
								Title:       "test title",
								LastUpdated: &lastUpdated,
							},
						},
					}

					chatBadgeClientMock.On("GetBitsImages", mock.Anything, channelID, mock.Anything).Return(badgeSet, nil)

					newTierMap, err := testFiller.PopulateWithCustomImages(ctx, channelID, tierMap)

					So(newTierMap, ShouldNotBeNil)
					So(err, ShouldBeNil)
				})
			})

			Convey("and returns no badges", func() {
				tierMap := map[int64]*api.BadgeTier{
					tier1: {
						Enabled:   true,
						Threshold: tier1,
						ChannelId: channelID,
					},
				}

				badgeSet := &models.BadgeSet{}

				chatBadgeClientMock.On("GetBitsImages", mock.Anything, channelID, mock.Anything).Return(badgeSet, nil)

				newTierMap, err := testFiller.PopulateWithCustomImages(ctx, channelID, tierMap)

				So(newTierMap, ShouldNotBeNil)
				So(err, ShouldBeNil)
			})
		})

		Convey("and a badge service that errors", func() {
			tierMap := map[int64]*api.BadgeTier{
				tier1: {
					Enabled:   true,
					Threshold: tier1,
					ChannelId: channelID,
				},
			}

			chatBadgeClientMock.On("GetBitsImages", mock.Anything, channelID, mock.Anything).Return(nil, errors.New("test error"))

			newTierMap, err := testFiller.PopulateWithCustomImages(ctx, channelID, tierMap)

			So(newTierMap, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("and a badge service that returns nil", func() {
			tierMap := map[int64]*api.BadgeTier{
				tier1: {
					Enabled:   true,
					Threshold: tier1,
					ChannelId: channelID,
				},
			}

			chatBadgeClientMock.On("GetBitsImages", mock.Anything, channelID, mock.Anything).Return(nil, nil)

			newTierMap, err := testFiller.PopulateWithCustomImages(ctx, channelID, tierMap)

			So(newTierMap, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

	})
}
