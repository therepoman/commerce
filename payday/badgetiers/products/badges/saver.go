package bitsbadges

import (
	"context"
	"strconv"
	"time"

	chatBadgesModel "code.justin.tv/chat/badges/app/models"
	zuma "code.justin.tv/chat/zuma/app/api"
	"code.justin.tv/commerce/gogogadget/pointers"
	log "code.justin.tv/commerce/logrus"
	chatBadgesClient "code.justin.tv/commerce/payday/clients/chatbadges"
	zumaWrapper "code.justin.tv/commerce/payday/clients/zuma"
	"code.justin.tv/commerce/payday/datascience"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/image"
	"code.justin.tv/commerce/payday/models"
	apiModels "code.justin.tv/commerce/payday/models/api"
	"code.justin.tv/commerce/payday/utils/strings"
	"github.com/hashicorp/go-multierror"
)

const (
	AutoModLevel4              = 4
	BadgeTitleMaxLength        = 25
	BitsBadgeImageNoAction     = "none"
	BitsBadgeImageResetAction  = "reset"
	BitsBadgeImageUploadAction = "upload"
	BitsBadgeTitleNoAction     = "none"
	BitsBadgeTitleResetAction  = "reset"
	BitsBadgeTitleUpdateAction = "update"

	DeleteAndUpdateError         = "DeleteAndUpdateError"
	RemoveBitsImageError         = "RemoveBitsImageError"
	TitleTooLongError            = "TitleTooLongError"
	TitleFailedModerationError   = "TitleFailedModerationError"
	ValidateBitsBadgeImagesError = "ValidateBitsBadgeImagesError"
	UploadBitsBadgeImagesError   = "UploadBitsBadgeImagesError"
)

type Saver interface {
	SaveBadgeImageSettings(ctx context.Context, channelId string, dynamoBadgeTiers []*dynamo.BadgeTier, request apiModels.SetBadgesRequest) (*chatBadgesModel.BitsUploadImagesResponse, error)
}

type saver struct {
	ChatBadgesClient  chatBadgesClient.IChatBadgesClient `inject:""`
	DataScienceClient datascience.DataScience            `inject:""`
	ZumaClient        zumaWrapper.IZumaClientWrapper     `inject:""`
}

type SaverError struct {
	Err       error
	ErrorType string
}

func NewSaverError(err error, errType string) error {
	return &SaverError{
		Err:       err,
		ErrorType: errType,
	}
}

func (s *SaverError) Error() string {
	return s.Err.Error()
}

func NewSaver() Saver {
	return &saver{}
}

func (s *saver) SaveBadgeImageSettings(ctx context.Context, channelId string, dynamoBadgeTiers []*dynamo.BadgeTier, request apiModels.SetBadgesRequest) (*chatBadgesModel.BitsUploadImagesResponse, error) {
	versions := map[string]chatBadgesModel.ImageVersionUpdate{}
	var err error

	for _, tier := range request.Tiers {
		badgeVersion := strconv.FormatInt(tier.Threshold, 10)

		shouldDeleteImage := tier.DeleteImage != nil && *tier.DeleteImage
		shouldDeleteTitle := tier.DeleteTitle != nil && *tier.DeleteTitle
		shouldUpdateImage := strings.NotBlankP(tier.ImageData1x) && strings.NotBlankP(tier.ImageData2x) && strings.NotBlankP(tier.ImageData4x)
		shouldUpdateTitle := strings.NotBlankP(tier.Title)

		if shouldDeleteImage && shouldUpdateImage || shouldDeleteTitle && shouldUpdateTitle {
			err = multierror.Append(err, NewSaverError(errors.New("Invalid request: cannot delete and update the same field in one request"), DeleteAndUpdateError))
			continue
		}

		if shouldDeleteImage && shouldDeleteTitle {
			removeBitsImageErr := s.ChatBadgesClient.RemoveBitsImage(ctx, channelId, badgeVersion, nil)
			if removeBitsImageErr != nil {
				msg := "Error removing bits badge image version to chat badges service"
				log.WithError(removeBitsImageErr).WithFields(log.Fields{
					"badgeVersion": badgeVersion,
					"channelId":    channelId,
				}).Error(msg)
				err = multierror.Append(err, NewSaverError(errors.Notef(removeBitsImageErr, msg), RemoveBitsImageError))
			}
			continue
		}

		version := chatBadgesModel.ImageVersionUpdate{}

		if shouldDeleteImage {
			version.ResetImage = pointers.BoolP(true)
		}

		if shouldDeleteTitle {
			version.ResetTitle = pointers.BoolP(true)
		}

		if shouldUpdateImage {
			version.ImageBytes1x = tier.ImageData1x
			version.ImageBytes2x = tier.ImageData2x
			version.ImageBytes4x = tier.ImageData4x
		}

		if shouldUpdateTitle {
			requiredBadgeSuffix := " " + badgeVersion
			badgeTitleLengthErr := s.validateBadgeTitleLength(*tier.Title, requiredBadgeSuffix)
			if badgeTitleLengthErr != nil {
				msg := "Error validating badge tier title length."
				log.WithError(badgeTitleLengthErr).WithFields(log.Fields{
					"tierTitle": *tier.Title,
				}).Error(msg)
				err = multierror.Append(err, NewSaverError(errors.Notef(badgeTitleLengthErr, msg), TitleTooLongError))
				continue
			}

			badgeTitleEligibilityErr := s.validateBadgeTitleEligibility(channelId, *tier.Title, ctx)
			if badgeTitleEligibilityErr != nil {
				msg := "Error validating badge tier title eligibility."
				log.WithError(badgeTitleEligibilityErr).WithFields(log.Fields{
					"tierTitle": *tier.Title,
				}).Error(msg)
				err = multierror.Append(err, NewSaverError(errors.Notef(badgeTitleEligibilityErr, msg), TitleFailedModerationError))
				continue
			}

			version.Title = tier.Title
		}

		//If version is empty, don't add it to the versions map
		if (version != chatBadgesModel.ImageVersionUpdate{}) {
			versions[badgeVersion] = version
		}
	}

	// collect all the errors while looping through tiers and return them as one single error
	if err != nil {
		return nil, err
	}

	var resp *chatBadgesModel.BitsUploadImagesResponse
	if len(versions) > 0 {
		badgeImageUploadRequestErr := s.validateBadgeImageUploadRequest(versions)
		if badgeImageUploadRequestErr != nil {
			err = multierror.Append(err, NewSaverError(badgeImageUploadRequestErr, ValidateBitsBadgeImagesError))
			return nil, err
		}

		chatBadgesUploadParams := chatBadgesModel.UploadBitsImagesParams{
			Versions: versions,
		}

		var uploadBitsImagesErr error
		resp, uploadBitsImagesErr = s.ChatBadgesClient.UploadBitsImages(ctx, channelId, chatBadgesUploadParams, nil)
		if uploadBitsImagesErr != nil {
			msg := "Error uploading bits badge images to chat badges service"
			log.WithError(uploadBitsImagesErr).WithField("channelId", channelId).Error(msg)
			err = multierror.Append(err, NewSaverError(errors.Notef(uploadBitsImagesErr, msg), UploadBitsBadgeImagesError))
			return nil, err
		}

		go s.trackBitsBadgeCustomization(channelId, dynamoBadgeTiers, request)
	}
	return resp, nil
}

func (s *saver) validateBadgeTitleLength(tierTitle string, requiredTitleSuffix string) error {
	if len(tierTitle) < len(requiredTitleSuffix) {
		return errors.New("badge tier title failed length validation. Must end in suffix \"" + requiredTitleSuffix + "\"")
	}

	titleEnd := tierTitle[len(tierTitle)-len(requiredTitleSuffix):]
	if titleEnd != requiredTitleSuffix {
		return errors.New("badge tier title failed length validation. Must end in suffix \"" + requiredTitleSuffix + "\"")
	}

	if len(tierTitle)-len(requiredTitleSuffix) > BadgeTitleMaxLength {
		return errors.New("badge tier title failed length validation. Must be <= " + strconv.Itoa(BadgeTitleMaxLength) + " characters.")
	}
	return nil
}

func (s *saver) validateBadgeImageUploadRequest(versions map[string]chatBadgesModel.ImageVersionUpdate) error {
	for _, version := range versions {
		if version.ImageBytes1x != nil && version.ImageBytes2x != nil && version.ImageBytes4x != nil {
			var err1x, err2x, err4x error

			// This API allows a subset of the image sizes to be uploaded at once. Consider the image bytes valid if
			// the field is an empty string or a static base64 encoded png.
			imageBytes1x := *version.ImageBytes1x
			if imageBytes1x != "" {
				err1x = image.ValidateStaticBase64PNG(imageBytes1x)
			}
			imageBytes2x := *version.ImageBytes2x
			if imageBytes2x != "" {
				err2x = image.ValidateStaticBase64PNG(imageBytes2x)
			}
			imageBytes4x := *version.ImageBytes4x
			if imageBytes4x != "" {
				err4x = image.ValidateStaticBase64PNG(imageBytes4x)
			}

			if err1x != nil || err2x != nil || err4x != nil {
				msg := "Invalid bits badge images contained in upload request"
				log.WithFields(log.Fields{
					"Error1x": err1x,
					"Error2x": err2x,
					"Error4x": err4x,
				}).Warn(msg)
				return errors.New(msg)
			}
		}
	}
	return nil
}

func (s *saver) trackBitsBadgeCustomization(channelID string, dynamoBadgeTiers []*dynamo.BadgeTier, request apiModels.SetBadgesRequest) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*3)
	defer cancel()

	for _, tier := range request.Tiers {
		imageAction := BitsBadgeImageNoAction
		titleAction := BitsBadgeTitleNoAction
		var customTitle string

		if tier.ImageData1x != nil && tier.ImageData2x != nil && tier.ImageData4x != nil {
			imageAction = BitsBadgeImageUploadAction
		} else if tier.DeleteImage != nil && *tier.DeleteImage {
			imageAction = BitsBadgeImageResetAction
		}

		if tier.Title != nil {
			titleAction = BitsBadgeTitleUpdateAction
			customTitle = *tier.Title
		} else if tier.DeleteTitle != nil && *tier.DeleteTitle {
			titleAction = BitsBadgeTitleResetAction
		}

		if imageAction == BitsBadgeImageNoAction && titleAction == BitsBadgeTitleNoAction {
			continue
		}

		savedTierEnabled := false
		savedTier := findTierByThreshold(tier.Threshold, dynamoBadgeTiers)
		if savedTier != nil {
			savedTierEnabled = savedTier.Enabled
		}

		bitsBadgeUploadEvent := &models.BitsBadgeUploadEvent{
			ChannelID:           channelID,
			BadgeAmount:         tier.Threshold,
			BadgeFeatureEnabled: savedTierEnabled,
			Action:              imageAction,
			TitleAction:         titleAction,
			CustomTitle:         customTitle,
		}

		err := s.DataScienceClient.TrackBitsEvent(ctx, datascience.BitsPartnerBadgeDetail, bitsBadgeUploadEvent)
		if err != nil {
			log.WithError(err).WithField("bitsBadgeUploadEvent", bitsBadgeUploadEvent).Error("Could not send datascience event")
		}
	}

}

func (s *saver) validateBadgeTitleEligibility(channelId string, tierTitle string, ctx context.Context) error {
	enforcementRules := zuma.EnforcementRuleset{
		AutoMod:             true,
		Banned:              false,
		Blocked:             false,
		ChannelBlockedTerms: false,
		DisposableEmail:     false,
		EmotesOnly:          false,
		Entropy:             false,
		FollowersOnly:       false,
		Spam:                false,
		SubscribersOnly:     false,
		Suspended:           false,
		VerifiedOnly:        false,
		Zalgo:               true,
	}
	enforcementConfig := zuma.EnforcementConfiguration{
		DefaultAutoModLevel: pointers.IntP(AutoModLevel4),
	}
	extractMessageReq := zuma.EnforceMessageRequest{
		SkipExtraction:    true,
		SenderID:          channelId,
		MessageText:       tierTitle,
		EnforcementRules:  enforcementRules,
		EnforcementConfig: enforcementConfig,
	}
	zumaResponse, err := s.ZumaClient.EnforceMessage(ctx, extractMessageReq, nil)
	if err != nil {
		log.WithError(err).Error("error getting zuma eligibility from zuma service")
		return err
	}

	if zumaResponse.AllPassed {
		return nil
	} else if zumaResponse.AutoMod != nil && !zumaResponse.AutoMod.Passed {
		return errors.New("badge tier title failed automod check.")
	} else if zumaResponse.Zalgo != nil && !zumaResponse.Zalgo.Passed {
		return errors.New("badge tier title failed zalgo check.")
	} else {
		return errors.New("badge tier title failed zuma eligibility.")
	}
}

//TODO: migrate this func and the func of the same name in api/badge.go into a util without making any import loops
func findTierByThreshold(threshold int64, dynamoBadgeTiers []*dynamo.BadgeTier) *dynamo.BadgeTier {
	for _, dynamoTier := range dynamoBadgeTiers {
		if dynamoTier != nil && dynamoTier.Threshold == threshold {
			return dynamoTier
		}
	}

	return nil
}
