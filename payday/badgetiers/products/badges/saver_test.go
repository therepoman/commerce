package bitsbadges

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/chat/badges/app/models"
	zuma_api "code.justin.tv/chat/zuma/app/api"
	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/payday/dynamo"
	chatbadges_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/chatbadges"
	zuma_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/zuma"
	datascience_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/datascience"
	"code.justin.tv/commerce/payday/models/api"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestSaver_SaveBadgeImages(t *testing.T) {
	Convey("Given a saver", t, func() {
		ctx := context.Background()
		channelID := "123"
		tier1 := int64(1)

		chatBadgeClientMock := new(chatbadges_mock.IChatBadgesClient)
		datascienceMock := new(datascience_mock.DataScience)
		zumaMock := new(zuma_mock.IZumaClientWrapper)

		testSaver := saver{
			ChatBadgesClient:  chatBadgeClientMock,
			DataScienceClient: datascienceMock,
			ZumaClient:        zumaMock,
		}

		datascienceMock.On("TrackBitsEvent", mock.Anything, mock.Anything, mock.Anything).Return(nil)

		Convey("for a request with at least one tier", func() {
			Convey("that has no title, deleteTitle, imageData, or deleteImage", func() {
				Convey("that is enabling a tier", func() {
					request := api.SetBadgesRequest{
						Tiers: []*api.BadgeTierSetting{
							{
								Enabled:   pointers.BoolP(true),
								Threshold: tier1,
							},
						},
					}

					resp, err := testSaver.SaveBadgeImageSettings(ctx, channelID, []*dynamo.BadgeTier{}, request)

					So(resp, ShouldBeNil)
					So(err, ShouldBeNil)
				})

				Convey("that is disabling a tier", func() {
					request := api.SetBadgesRequest{
						Tiers: []*api.BadgeTierSetting{
							{
								Enabled:   pointers.BoolP(true),
								Threshold: tier1,
							},
						},
					}

					resp, err := testSaver.SaveBadgeImageSettings(ctx, channelID, []*dynamo.BadgeTier{}, request)

					So(resp, ShouldBeNil)
					So(err, ShouldBeNil)
				})
			})

			Convey("that has a title and a deleteTitle", func() {
				request := api.SetBadgesRequest{
					Tiers: []*api.BadgeTierSetting{
						{
							Enabled:     pointers.BoolP(true),
							Threshold:   tier1,
							Title:       pointers.StringP("test title"),
							DeleteTitle: pointers.BoolP(true),
						},
					},
				}

				resp, err := testSaver.SaveBadgeImageSettings(ctx, channelID, []*dynamo.BadgeTier{}, request)

				So(resp, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})

			Convey("that has image data and a delete image", func() {
				request := api.SetBadgesRequest{
					Tiers: []*api.BadgeTierSetting{
						{
							Enabled:     pointers.BoolP(true),
							Threshold:   tier1,
							ImageData1x: pointers.StringP("test data 1x"),
							ImageData2x: pointers.StringP("test data 2x"),
							ImageData4x: pointers.StringP("test data 4x"),
							DeleteImage: pointers.BoolP(true),
						},
					},
				}

				resp, err := testSaver.SaveBadgeImageSettings(ctx, channelID, []*dynamo.BadgeTier{}, request)

				So(resp, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})

			Convey("that has a delete title and a delete image", func() {
				request := api.SetBadgesRequest{
					Tiers: []*api.BadgeTierSetting{
						{
							Enabled:     pointers.BoolP(true),
							Threshold:   tier1,
							DeleteImage: pointers.BoolP(true),
							DeleteTitle: pointers.BoolP(true),
						},
					},
				}

				Convey("when badge service succeeds", func() {
					chatBadgeClientMock.On("RemoveBitsImage", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)

					resp, err := testSaver.SaveBadgeImageSettings(ctx, channelID, []*dynamo.BadgeTier{}, request)

					So(resp, ShouldBeNil)
					So(err, ShouldBeNil)
				})

				Convey("when badge service errors", func() {
					chatBadgeClientMock.On("RemoveBitsImage", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(errors.New("test error"))

					resp, err := testSaver.SaveBadgeImageSettings(ctx, channelID, []*dynamo.BadgeTier{}, request)

					So(resp, ShouldBeNil)
					So(err, ShouldNotBeNil)
				})
			})

			Convey("that has a title change", func() {
				Convey("to a title that is properly formed", func() {
					Convey("with a zuma client that succeeds", func() {
						request := api.SetBadgesRequest{
							Tiers: []*api.BadgeTierSetting{
								{
									Enabled:   pointers.BoolP(true),
									Threshold: tier1,
									Title:     pointers.StringP("test title 1"),
								},
							},
						}

						zumaResponse := zuma_api.EnforceMessageResponse{
							EnforceProperties: zuma_api.EnforceProperties{
								AllPassed: true,
							},
						}

						zumaMock.On("EnforceMessage", mock.Anything, mock.Anything, mock.Anything).Return(zumaResponse, nil)

						Convey("with a badge client that succeeds on upload", func() {
							chatBadgeUploadResponse := &models.BitsUploadImagesResponse{
								Versions: map[string]models.BitsBadgeUploadImageResponse{
									"1": {
										Title: "test title 1",
									},
								},
							}

							chatBadgeClientMock.On("UploadBitsImages", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(chatBadgeUploadResponse, nil)

							resp, err := testSaver.SaveBadgeImageSettings(ctx, channelID, []*dynamo.BadgeTier{}, request)

							So(resp, ShouldNotBeNil)
							So(resp.Versions, ShouldNotBeNil)
							So(resp.Versions["1"].Title, ShouldEqual, *request.Tiers[0].Title)
							So(err, ShouldBeNil)
						})

						Convey("with a badge client that errors on upload", func() {
							chatBadgeClientMock.On("UploadBitsImages", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("test error"))

							resp, err := testSaver.SaveBadgeImageSettings(ctx, channelID, []*dynamo.BadgeTier{}, request)

							So(resp, ShouldBeNil)
							So(err, ShouldNotBeNil)
						})
					})

					Convey("with a zuma client that errors", func() {
						Convey("due to AutoMod", func() {
							request := api.SetBadgesRequest{
								Tiers: []*api.BadgeTierSetting{
									{
										Enabled:   pointers.BoolP(true),
										Threshold: tier1,
										Title:     pointers.StringP("bad automod 1"),
									},
								},
							}

							zumaResponse := zuma_api.EnforceMessageResponse{
								EnforceProperties: zuma_api.EnforceProperties{
									AllPassed: false,
									AutoMod: &zuma_api.AutoModResponse{
										GenericEnforcementResponse: zuma_api.GenericEnforcementResponse{
											Passed: false,
										},
									},
								},
							}

							zumaMock.On("EnforceMessage", mock.Anything, mock.Anything, mock.Anything).Return(zumaResponse, nil)

							resp, err := testSaver.SaveBadgeImageSettings(ctx, channelID, []*dynamo.BadgeTier{}, request)

							So(resp, ShouldBeNil)
							So(err, ShouldNotBeNil)
						})

						Convey("due to Zalgo", func() {
							request := api.SetBadgesRequest{
								Tiers: []*api.BadgeTierSetting{
									{
										Enabled:   pointers.BoolP(true),
										Threshold: tier1,
										Title:     pointers.StringP("bad zalgo 1"),
									},
								},
							}

							zumaResponse := zuma_api.EnforceMessageResponse{
								EnforceProperties: zuma_api.EnforceProperties{
									AllPassed: false,
									Zalgo: &zuma_api.GenericEnforcementResponse{
										Passed: false,
									},
								},
							}

							zumaMock.On("EnforceMessage", mock.Anything, mock.Anything, mock.Anything).Return(zumaResponse, nil)

							resp, err := testSaver.SaveBadgeImageSettings(ctx, channelID, []*dynamo.BadgeTier{}, request)

							So(resp, ShouldBeNil)
							So(err, ShouldNotBeNil)
						})

						Convey("due to any other check", func() {
							request := api.SetBadgesRequest{
								Tiers: []*api.BadgeTierSetting{
									{
										Enabled:   pointers.BoolP(true),
										Threshold: tier1,
										Title:     pointers.StringP("bad other check 1"),
									},
								},
							}

							zumaResponse := zuma_api.EnforceMessageResponse{
								EnforceProperties: zuma_api.EnforceProperties{
									AllPassed: false,
									Banned: &zuma_api.BannedResponse{
										GenericEnforcementResponse: zuma_api.GenericEnforcementResponse{
											Passed: false,
										},
									},
								},
							}

							zumaMock.On("EnforceMessage", mock.Anything, mock.Anything, mock.Anything).Return(zumaResponse, nil)

							resp, err := testSaver.SaveBadgeImageSettings(ctx, channelID, []*dynamo.BadgeTier{}, request)

							So(resp, ShouldBeNil)
							So(err, ShouldNotBeNil)
						})

						Convey("due to the service itself", func() {
							request := api.SetBadgesRequest{
								Tiers: []*api.BadgeTierSetting{
									{
										Enabled:   pointers.BoolP(true),
										Threshold: tier1,
										Title:     pointers.StringP("test title 1"),
									},
								},
							}

							zumaMock.On("EnforceMessage", mock.Anything, mock.Anything, mock.Anything).Return(zuma_api.EnforceMessageResponse{}, errors.New("test error"))

							resp, err := testSaver.SaveBadgeImageSettings(ctx, channelID, []*dynamo.BadgeTier{}, request)

							So(resp, ShouldBeNil)
							So(err, ShouldNotBeNil)
						})
					})
				})

				Convey("to a title that is too short", func() {
					request := api.SetBadgesRequest{
						Tiers: []*api.BadgeTierSetting{
							{
								Enabled:   pointers.BoolP(true),
								Threshold: 100000,
								Title:     pointers.StringP("f"),
							},
						},
					}

					resp, err := testSaver.SaveBadgeImageSettings(ctx, channelID, []*dynamo.BadgeTier{}, request)

					So(resp, ShouldBeNil)
					So(err, ShouldNotBeNil)
				})

				Convey("to a title that doesn't end in the required suffix", func() {
					request := api.SetBadgesRequest{
						Tiers: []*api.BadgeTierSetting{
							{
								Enabled:   pointers.BoolP(true),
								Threshold: tier1,
								Title:     pointers.StringP("test title"),
							},
						},
					}

					resp, err := testSaver.SaveBadgeImageSettings(ctx, channelID, []*dynamo.BadgeTier{}, request)

					So(resp, ShouldBeNil)
					So(err, ShouldNotBeNil)
				})

				Convey("to a title that is too long", func() {
					request := api.SetBadgesRequest{
						Tiers: []*api.BadgeTierSetting{
							{
								Enabled:   pointers.BoolP(true),
								Threshold: tier1,
								Title:     pointers.StringP("extremely long title that is far beyond the maximum length 1"),
							},
						},
					}

					resp, err := testSaver.SaveBadgeImageSettings(ctx, channelID, []*dynamo.BadgeTier{}, request)

					So(resp, ShouldBeNil)
					So(err, ShouldNotBeNil)
				})
			})

			Convey("that has an image change", func() {
				Convey("to an image that is properly formed", func() {
					request := api.SetBadgesRequest{
						Tiers: []*api.BadgeTierSetting{
							{
								Enabled:     pointers.BoolP(true),
								Threshold:   tier1,
								ImageData1x: getBase64EncodedTestString1x(),
								ImageData2x: getBase64EncodedTestString2x(),
								ImageData4x: getBase64EncodedTestString4x(),
							},
						},
					}

					Convey("with a badge client that succeeds on upload", func() {
						chatBadgeUploadResponse := &models.BitsUploadImagesResponse{
							Versions: map[string]models.BitsBadgeUploadImageResponse{
								"1": {
									ImageURL1x: "url/to/image/1.png",
									ImageURL2x: "url/to/image/2.png",
									ImageURL4x: "url/to/image/4.png",
								},
							},
						}

						chatBadgeClientMock.On("UploadBitsImages", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(chatBadgeUploadResponse, nil)

						resp, err := testSaver.SaveBadgeImageSettings(ctx, channelID, []*dynamo.BadgeTier{}, request)

						So(resp, ShouldNotBeNil)
						So(resp.Versions, ShouldNotBeNil)
						So(resp.Versions["1"].ImageURL1x, ShouldNotBeNil)
						So(err, ShouldBeNil)
					})

					Convey("with a badge client that errors on upload", func() {
						chatBadgeClientMock.On("UploadBitsImages", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("test error"))

						resp, err := testSaver.SaveBadgeImageSettings(ctx, channelID, []*dynamo.BadgeTier{}, request)

						So(resp, ShouldBeNil)
						So(err, ShouldNotBeNil)
					})
				})

				Convey("to an image that is not properly formed", func() {
					Convey("because image data is not a string of bytes", func() {
						request := api.SetBadgesRequest{
							Tiers: []*api.BadgeTierSetting{
								{
									Enabled:     pointers.BoolP(true),
									Threshold:   tier1,
									ImageData1x: pointers.StringP("image data 1x"),
									ImageData2x: pointers.StringP("image data 2x"),
									ImageData4x: pointers.StringP("image data 4x"),
								},
							},
						}

						resp, err := testSaver.SaveBadgeImageSettings(ctx, channelID, []*dynamo.BadgeTier{}, request)

						So(resp, ShouldBeNil)
						So(err, ShouldNotBeNil)
					})
				})
			})
		})

		Convey("for a request without tiers", func() {
			resp, err := testSaver.SaveBadgeImageSettings(ctx, channelID, []*dynamo.BadgeTier{}, api.SetBadgesRequest{})

			So(resp, ShouldBeNil)
			So(err, ShouldBeNil)
		})
	})
}

func getBase64EncodedTestString1x() *string {
	return pointers.StringP("iVBORw0KGgoAAAANSUhEUgAAABAAAAASCAMAAABl5a5YAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAABGlBMVEUAAACQSP+PRv+QSP+QSP+QSP+QSP+PR/+QSP+QSP+QSP+QSP+QSP+zgv+PR/+QSP+LQP+SS/+KP/+ORP+NQ/+QSP+QSP+QSP+QSP+QSP+QSP+QSP+QSP+QSP+QSP+QSP+QSP+QSP+QSP+QSP+RSv+SS/+QSP/Ho//m1v/k0//m1f/Rs/+PR//awv/////n1/+TTP/ZwP/38v/Xvf/59f/28f/Xvv/7+f/r3v+bWv/w5v/p2/+bW//z6//s3/+cXP/q3P+dXf+eX//w5/+eYP/69//o2f/+/f/izv+STP/y6v/Yvv/x6P+1hP+hZP+pcf/Fn//hzf+ve/+qc/+jaP+PRv+MQf+xf/+gYv+ORP+ORf+fYP+iZf+QR/9mngIgAAAAJHRSTlMAAAABhQJ09/lw+Hv1/aKUCpcLmAubC54Nn5iWxu+ZgxBxYW/5Bfe0AAAAAWJLR0QuVNMQhwAAAAd0SU1FB+MIGhEND2sllS0AAADLSURBVBjTY2BgZGRmUVGFATUgn5WNXV1DEwy0tFUZGJnZOHR09aBA34CBkZNLTcdQT8/I2MTUzFzDgIGbRw0sYGFpZW1jCxRQU4MI2NlbOTiiCDg5O7jY6iMEXN3MXd3cPTzhAmDgxcsHEfDWc9fTc3f38eUXAAuo+fkH6AUGBYcICjFBBELDwgMjIqOERZgYIQJqodExoWqiYoyMDFABtVgdNXEJEB8moKYmCeEzSElJy6jJisvJK0D4QO8zKvIoMYIAAxQwKrMh8QAlYCjSH+sOYgAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAxOS0wOC0yM1QxNzo1MTo0NSswMDowMIjcJbAAAAAldEVYdGRhdGU6bW9kaWZ5ADIwMTktMDgtMjNUMTc6MjY6NTMrMDA6MDCdHKZSAAAAAElFTkSuQmCC")
}

func getBase64EncodedTestString2x() *string {
	return pointers.StringP("iVBORw0KGgoAAAANSUhEUgAAACAAAAAkCAMAAADfNcjQAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAB9VBMVEUAAACQSP+QSP+QSP+QSP+QSP+QSP+QSP+QSP+QSP+QSP+QSP+QSP+QSP+QSP+QSP+QSP+QSP+QSP+QSP+QSP+PRv+QSP+PRv+QSP+QSP+PR/+QSP+ORf+QSf+QSf+PR/+QSf+QSP+QSP+QSP+QSP+QSP+QSP+QSP+QSP+QSP+QSP+QSP+QSP+QSP+QSP+QSP+QSP+QSP+QSP+QSP+QSP+QSP+QSP+QSP+QSP+QSP+QSP+QSP+QSP+QSP+QSP+QSP+QSP+QSP+QSP+QSP+QSP+QSP+QSP+QSP+QSP+QSP+PR/+NRP+ORf+ORP+nbv/Stv/RtP/UuP+4i/+PRv+MQf+4iv/////Xvf+MQv+3iP/Vuf/+/v/9+//9/P///v/p2/+rdf+uef/u4//k0/+pcv+wff/z6//iz/+QSf/o2v/bw/+LQP+UT//j0P+RSf+UTv/cxf+XVP/v5P+RSv+VUP/u5P/z7P/StP/Ttv/28P/x5//Rs//Uuf/49P/Vu//7+f/Lqv/7+P/dx/+vev/8+f/eyf+sdv/8+v/fyv+qc//gy/+TTP+gYv++lP++k/+8kP/k0v/+/f/q3f/Gof+8kf+7jv+rdP+STP+JPP/PsP/q3P+7kP+NQ/+/lf+TTf/Qsv/v5f+/lv+SS//Bmf+QR/+aWf+aWP9fWmynAAAASXRSTlMAAId+53oIae4LW/gE+/Nz5m7gbOPzw/uvCqUQmh0lpyL5uBf8vg+0FAGnIqUtK8Ahyfr+wRoCPjxH3t1EPUAF0tRQDNNa31fV44PfHwAAAAFiS0dEVgoN6YkAAAAHdElNRQfjCBoRDRjo9hDqAAABpElEQVQ4y2NggABGRiZPrAAmzcjM4uWNAXygCoDyrCy+fv4BaCAwKNgTIs/GzuEZEhqGAcJ9PcHynFzcnp4RkZgKokAKGBl5uHg98Spg5/NEUhAdExsHI0EKGBn5BTyRFcQnJCaFhSWnpKZBFQgKeaIoSPfOyAwLy8rOSYIoEBbxRFWQm5cfHxZW4FNYBFEADzI6Kkj3KQY5MqSkFIeCsvKKyrCwquqa2rCwOmwKkEB9gw9eBY1NzaJieBS0tLaJS0jiVtDe0VksJc2IUICWHmK6OrtlZBmRFPj09Pb19U8IC5s4afKUvt6p06bLyAHlEQo8g0NCQmbMDIudNTsnOyQkwldeASSPpAAE5pSHTZo7zwvIUlRSBsujK5i/YOEiEENFVQ0ij6bAp29xPpihrqGJVcGSpcuCQbSWNiNUHk2BZzBYXkcXJo2hAAwk9BDyWBTw6hsgyWMqUDFURpZnMIIAY6CUMYjhaWJqhqLAHAQsLK2ACqxtbM3N7XQZUeRBORsI7B08PR2dGKGAAQMwOrt4OrhilYIpcHRzxy0NUuDBhUMeADlSGVSnwG2cAAAAJXRFWHRkYXRlOmNyZWF0ZQAyMDE5LTA4LTIzVDE3OjUxOjQ1KzAwOjAwiNwlsAAAACV0RVh0ZGF0ZTptb2RpZnkAMjAxOS0wOC0yM1QxNzoyNjo1MyswMDowMJ0cplIAAAAASUVORK5CYII=")
}

func getBase64EncodedTestString4x() *string {
	return pointers.StringP("iVBORw0KGgoAAAANSUhEUgAAAEEAAABICAYAAACp+JiNAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QA/wD/AP+gvaeTAAAAB3RJTUUH4wgaEQ0gwPSodAAACHtJREFUeNrtm+tzG1cVwH93V3JkJ37VeKhxS2hKIdO0UArl0WkpDMxQPjItwwf+iTL8I3zjK5+AGWBgGGjLKyVp0g7NO8R2nLht4tiyZVsvW6/VSnv5cHb1yuppyVqbnpkdyfLR6u7vnnPuOefuKnqQX7yq/T7+AvAz4KfAeC/nHZaE+nDxI8AU8BQwAxgAaPC01ZAvsm4cPoPpCkITOQm8ArwIPA2EnTJoR47AUFCgFBgmKKO/ECaB54HXgW/hukFoBJQpPxok0Q54E9QVhCYuEAaeBL4CfA84g2ZCawEwMQfT8xAZR9xCt/uVwYoyZAz5NCQfwO4WOKWqRfRqCVOIC7wGnAFmNELZDMPsE/DUyzD1GbFEpzxcCGZIZn/nY7hzDjJxKFlgKkC1gNDEAiLALPAc8BLwdcQlZMYdcYOxafjUKYEQJNEaRq+Lm9ZaZ7eWMA/8EPiOC8J/KVTBiwfgjskbVysIPhbgfXUS+BLwA+C7wPH6U1VFO+JzQZO6oFgzSZ1awmngm8DLwDMugIZTHV7pBMIU8A0kE/wyMDHsQQ8EQosg+BjiAt92X2eGPeCBQWgis0gQfBVZBieHPdiBQfCxghFgGkmEXkFWgrGDGEypCMUs2Ja73BoQPgYjxyUJa5RyEawc2IWqfugYHBuT144h+Hz2JBL9X3JBHAgAgFwC1hcgcR+KeQiPwiOPw/wzMDn3sH42BdEFiN+DYg7CEZh+HObPwNR87xCm3Av/ERIMT1ApPw4AQgrWbsL9y5LiRibg5POSdPlByKdg/SZ8fEneHxuHzz4HU3O9QRgBTgFfBb6PxIAD7wmUbSjswt425JKS2uZ35XNf/RIU9iCzA9k4jBZEv1Ts7nc9CNOI//8YKYeHswq4pa4ZlsMIu6VvEztUStJ0M+R+J+RfKncC4QzSEHkRqQWG1hXymh7KkMMwGlLdZiCMmqMHxw0BP0cs4YtUM8H/KwkBP0FaYiGOSBrcC4QDWwKDKl2GkKMpn0DgEwhAwCDo2jdug7Ztj9Zr5HaiexggoKUjVSrKUS7K340t8oq6lm5RuUa/3EK/mfRj86VvYphSNEXGQZelFgiPghFqoR8RfackryMt9A8FhMgEzJ2W9LeYg5ExmD0Fo006GZFxePS0wLCyAmz2CRib8tdvln0GCsKJGfjcCzD3tMysYUovYWzaX//4I3DyawKiTr8JBC92BBpCeBQmR7vQj8Dko53pliyxLr+KNFAQBiV2Xnad9rYFhNb1hdaRh2AXBEBiFZJr0n/QGkcpLKAAlI4sBK3FArIJ2YPcWILtDwWIdgDFIorrwNaRhVCyBEBiFTZvw/otSK3L51qTVAb/AX4HLB85CNqRbnU2LhYQuwtbK5DegPwueQVrRojrSnERuAkkjhwEuyAA4qvSid5Ygt2YuIZSxIC/An9DsQDsoQcZGA+4PaN11QXiq7DlWkBiFawsRWWQMEyuAf8GzqHJe98dCASvWXqQUrKk67zzEUQXBUBqvbIxc1cp3kFxAcWNWgD9h6AALcWMlXVv3tL+2/SVZqrpf6pKZehUz+P3e0q5AOKyBMbuwsYiJB5IbECRMENcQfEH4DKabHWk/Ybgdn3LJUhH4f4VMUWov13Hq/Ai47JbNPlpn4LHNW0rK/sPmR1537gN5NUC5aLsN+xtVe9JsjJYWrNihriK4l/AEpBpHPYbb6v+QfBujyvbEpWziZr9wJpZ9IBMPyb3NZ2YeRiCU5akJh2FrQ9hcwnSMaksH7IcJWDLtruXmZNDmcSV5iyKPwJLKJLNGg59dQflDiibFAh+Juy5RsmG+Wfra3/PdfJp8eetFdhclnV+NyZwmrlPJQ1W5BXElOIqigvAB0C2FsAbb9dH7cGsDrq5HzvuRfv937Ehl4ZUVJa26KKYdz5dhdW0YeKVyZpVDN5UcM4Ngrl2wx0IBC/oNR0s7pZZjY52xK9TUUlvY3dge0U2adFtVxuvSE4A14C3gHfRFKiJIo0WMFAInZGqB5VNyqxv3JY1PrEqcaFst96PdMUBFoH3gfNIECzUAGopw4PgDs0pyU60FwOiC2IJhYxYhxHqaH8xAVwEfg3cwmcVCBSEym2EjiQyuZRccGwZNu/IWp9NChyzPYAc8AC4DrwL/BdINSo1c4OhQfAolG0BkFyD9CY8uCEukE/L/w2TTlLvTeAvwN8Rd+jKAoYGwZvZUlGSGzMkwXDnHmS2XR2jJQANFIE4cBWvFgCrVqnd7A8Vgndxdl7uNcrEZfatPVk+lWprAAq4A7wDXEBcwGIfMjRLsC1IRmWZdJxqDOhA4sAl4PeIJbTNAwIHwRNdBtumUg8YrV0AZMlbAa4AZ4HbQLZRqRs3GDoE78IrNVH7se8A/wD+BCzjswr0KsPtLLX3fxBzjwGXkRhwCer7Ab3MfnAgdCb3gDeRTPBGI4B+SFAheLVAnGotcBFZBSoNkf1aQNAhlIEF4D3EAm5TXQb7/lhZUCHEkTT4N+wjEzysELJILXANCYK3gN1GpX65QVAhRIE/I0vhEj55wCAkBHzUhb7nj2HkMaBxwGyhXwDS7sU4NL89aASwobIvcB6pDyrS79lvhPDLLvQd95hFHgx7gdb3Qu+4F+Xl9836QyEkGN5HLKDL+9T3D+FXXeiX3MF+HrkT9lkfCF4SaCGFzltImptBnqvyE2/ZsxlAHtAJhJ0evhcF9lwgfhe0iiQ255GGx6b7v66j/CDdoBZCLzKCxAK/EeaRFPe37mts4FexXwjdkK55aMyv5rORXt8ysry9D6w1nuMgZrZrCH08Vwr4J7K8XaE3Nzt0EGo3NQtIansW6fnFaxWDOPv9gKCQmOAgGd57SJr7QSOAwyD7sQRvbb+LNDrOuwDqtr0Pg/TDHZJI2yvaqBB0N/Dkf6psVBuNXgAdAAAAJXRFWHRkYXRlOmNyZWF0ZQAyMDE5LTA4LTIzVDE3OjUxOjQ1KzAwOjAwiNwlsAAAACV0RVh0ZGF0ZTptb2RpZnkAMjAxOS0wOC0yM1QxNzoyNjo1MyswMDowMJ0cplIAAAAASUVORK5CYII=")
}
