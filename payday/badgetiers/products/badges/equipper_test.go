package bitsbadges

import (
	"context"
	"testing"

	"code.justin.tv/chat/badges/app/models"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/datascience"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/errors"
	client_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/chat/pubsub-go-pubclient/client"
	badgetiers_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/badgetiers/logician"
	tiernotifications_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/badgetiers/products/notifications"
	chatbadgeclient_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/chatbadges"
	"code.justin.tv/foundation/twitchclient"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestAutoEquip_HappyCase(t *testing.T) {
	testUserId := "123"
	testChannelId := "456"
	mockedLogician := new(badgetiers_mock.Logician)
	mockedChatBadgeClient := new(chatbadgeclient_mock.IChatBadgesClient)
	mockedPubsubClient := new(client_mock.PubClient)
	configuration := &config.Configuration{}
	dataScience := &datascience.DataScienceApiNoOp{}
	mockedTierNotificationsSetter := new(tiernotifications_mock.Setter)

	equipper := chatBadgeEquipper{
		ChatBadgeLogician:      mockedLogician,
		ChatBadgesClient:       mockedChatBadgeClient,
		DataScienceClient:      dataScience,
		PubClient:              mockedPubsubClient,
		Config:                 configuration,
		TierNotificationSetter: mockedTierNotificationsSetter,
	}

	mockedChatBadgeClient.On("GetBitsImages", mock.Anything, mock.Anything, mock.Anything).Return(nil, nil)
	mockedChatBadgeClient.On("GetSelectedChannelBadge", mock.Anything, testUserId, testChannelId, (*twitchclient.ReqOpts)(nil)).Return(&models.Badge{
		BadgeSetID:      "bits",
		BadgeSetVersion: "5000",
	}, nil)
	mockedChatBadgeClient.On("SelectBitsBadge", mock.Anything, testUserId, testChannelId, int64(10000), (*twitchclient.ReqOpts)(nil)).Return(true, nil)
	mockedPubsubClient.On("Publish", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)
	mockedTierNotificationsSetter.On("CreateNotification", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)

	Convey("Bits Badge Auto Equip works in happy case", t, func() {
		bitsTotal := int64(10001)
		bitsCheered := int64(1000)
		bestAchievedTierEntitlement := dynamo.BadgeTier{
			ChannelId: testChannelId,
			Threshold: 10000,
			Enabled:   true,
		}
		previousBestAchievedTierEntitlement := dynamo.BadgeTier{
			ChannelId: testChannelId,
			Threshold: 5000,
			Enabled:   true,
		}

		mockedLogician.On("GetBestAchievedTierEntitlement", mock.Anything, testChannelId, bitsTotal).Return(bestAchievedTierEntitlement, nil)
		mockedLogician.On("GetBestAchievedTierEntitlement", mock.Anything, testChannelId, bitsTotal-bitsCheered).Return(previousBestAchievedTierEntitlement, nil)

		badgeEquipment, err := equipper.AutoEquip(context.Background(), testUserId, testChannelId, bitsCheered, bitsTotal, "")
		So(err, ShouldBeNil)
		So(badgeEquipment.NewVersion, ShouldEqual, bestAchievedTierEntitlement.Threshold)
		So(badgeEquipment.PreviousVersion, ShouldEqual, previousBestAchievedTierEntitlement.Threshold)
	})

	Convey("Auto Equip returns nil when no bits are cheered", t, func() {
		bitsTotal := int64(10001)
		bitsCheered := int64(0)
		bestAchievedTierEntitlement := dynamo.BadgeTier{
			ChannelId: testChannelId,
			Threshold: 10000,
			Enabled:   true,
		}

		mockedLogician.On("GetBestAchievedTierEntitlement", mock.Anything, testChannelId, bitsTotal).Return(bestAchievedTierEntitlement, nil)

		badgeEquipment, err := equipper.AutoEquip(context.Background(), testUserId, testChannelId, bitsCheered, bitsTotal, "")
		So(err, ShouldBeNil)
		So(badgeEquipment, ShouldBeNil)
	})

	Convey("Auto Equip returns error when negative bits are cheered", t, func() {
		bitsTotal := int64(10001)
		bitsCheered := int64(-1)
		bestAchievedTierEntitlement := dynamo.BadgeTier{
			ChannelId: testChannelId,
			Threshold: 10000,
			Enabled:   true,
		}

		mockedLogician.On("GetBestAchievedTierEntitlement", mock.Anything, testChannelId, bitsTotal).Return(bestAchievedTierEntitlement, nil)

		badgeEquipment, err := equipper.AutoEquip(context.Background(), testUserId, testChannelId, bitsCheered, bitsTotal, "")
		So(err, ShouldNotBeNil)
		So(badgeEquipment, ShouldBeNil)
	})

	Convey("Auto Equip returns nil when no new threshold was passed", t, func() {
		bitsTotal := int64(10006)
		bitsCheered := int64(5)
		bestAchievedTierEntitlement := dynamo.BadgeTier{
			ChannelId: testChannelId,
			Threshold: 10000,
			Enabled:   true,
		}

		mockedLogician.On("GetBestAchievedTierEntitlement", mock.Anything, testChannelId, bitsTotal).Return(bestAchievedTierEntitlement, nil)

		badgeEquipment, err := equipper.AutoEquip(context.Background(), testUserId, testChannelId, bitsCheered, bitsTotal, "")
		So(err, ShouldBeNil)
		So(badgeEquipment, ShouldBeNil)
	})
}

func TestAutoEquip_BadgesAvailableError(t *testing.T) {
	testUserId := "123"
	testChannelId := "456"
	mockedLogician := new(badgetiers_mock.Logician)
	mockedChatBadgeClient := new(chatbadgeclient_mock.IChatBadgesClient)
	mockedPubsubClient := new(client_mock.PubClient)
	configuration := &config.Configuration{}
	dataScience := &datascience.DataScienceApiNoOp{}
	mockedTierNotificationsSetter := new(tiernotifications_mock.Setter)

	equipper := chatBadgeEquipper{
		ChatBadgeLogician:      mockedLogician,
		ChatBadgesClient:       mockedChatBadgeClient,
		DataScienceClient:      dataScience,
		PubClient:              mockedPubsubClient,
		Config:                 configuration,
		TierNotificationSetter: mockedTierNotificationsSetter,
	}

	Convey("Bits Badge Auto Equip returns error if there was an error when getting the best badge tier", t, func() {
		bitsCheered := int64(1000)
		bitsTotal := int64(10001)

		mockedLogician.On("GetBestAchievedTierEntitlement", mock.Anything, testChannelId, bitsTotal).Return(dynamo.BadgeTier{}, errors.New("test error"))

		badgeEquipment, err := equipper.AutoEquip(context.Background(), testUserId, testChannelId, bitsCheered, bitsTotal, "")
		So(err, ShouldNotBeNil)
		So(badgeEquipment, ShouldBeNil)
	})

	Convey("Bits Badge Auto Equip returns nil if no badge tiers have been met", t, func() {
		bitsCheered := int64(1)
		bitsTotal := int64(1)
		bestAchievedTierEntitlement := dynamo.BadgeTier{}

		mockedLogician.On("GetBestAchievedTierEntitlement", mock.Anything, testChannelId, bitsTotal).Return(bestAchievedTierEntitlement, nil)

		badgeEquipment, err := equipper.AutoEquip(context.Background(), testUserId, testChannelId, bitsCheered, bitsTotal, "")
		So(err, ShouldBeNil)
		So(badgeEquipment, ShouldBeNil)
	})
}

func TestAutoEquip_SetSelectedBadgeError(t *testing.T) {
	testUserId := "123"
	testChannelId := "456"
	bitsCheered := int64(1000)
	bitsTotal := int64(10001)
	bestAchievedTierEntitlement := dynamo.BadgeTier{
		ChannelId: testChannelId,
		Threshold: 10000,
		Enabled:   true,
	}
	mockedLogician := new(badgetiers_mock.Logician)
	mockedChatBadgeClient := new(chatbadgeclient_mock.IChatBadgesClient)
	mockedPubsubClient := new(client_mock.PubClient)
	configuration := &config.Configuration{}
	dataScience := &datascience.DataScienceApiNoOp{}
	mockedTierNotificationsSetter := new(tiernotifications_mock.Setter)

	equipper := chatBadgeEquipper{
		ChatBadgeLogician:      mockedLogician,
		ChatBadgesClient:       mockedChatBadgeClient,
		DataScienceClient:      dataScience,
		PubClient:              mockedPubsubClient,
		Config:                 configuration,
		TierNotificationSetter: mockedTierNotificationsSetter,
	}

	mockedLogician.On("GetBestAchievedTierEntitlement", mock.Anything, testChannelId, bitsTotal).Return(bestAchievedTierEntitlement, nil)
	mockedChatBadgeClient.On("GetBitsImages", mock.Anything, mock.Anything, mock.Anything).Return(nil, nil)
	mockedChatBadgeClient.On("GetSelectedChannelBadge", mock.Anything, testUserId, testChannelId, (*twitchclient.ReqOpts)(nil)).Return(&models.Badge{
		BadgeSetID:      "bits",
		BadgeSetVersion: "5000",
	}, nil)

	Convey("Bits Badge Auto Equip returns error if the badge wasn't available in badge service", t, func() {
		mockedChatBadgeClient.On("SelectBitsBadge", mock.Anything, testUserId, testChannelId, int64(10000), (*twitchclient.ReqOpts)(nil)).Return(false, nil).Once()
		badgeEquipment, err := equipper.AutoEquip(context.Background(), testUserId, testChannelId, bitsCheered, bitsTotal, "")
		So(err, ShouldNotBeNil)
		So(err.Error(), ShouldStartWith, "Bits badge with threshold 10000 not available in badge service for user "+testUserId+" on channel "+testChannelId)
		So(badgeEquipment, ShouldBeNil)
	})

	Convey("Bits Badge Auto Equip returns error if there was an error on setting selected badges", t, func() {
		mockedChatBadgeClient.On("SelectBitsBadge", mock.Anything, testUserId, testChannelId, int64(10000), (*twitchclient.ReqOpts)(nil)).Return(false, errors.New("testError"))
		badgeEquipment, err := equipper.AutoEquip(context.Background(), testUserId, testChannelId, bitsCheered, bitsTotal, "")
		So(err, ShouldNotBeNil)
		So(err.Error(), ShouldStartWith, "Encountered an error selecting new bits badge in chat badge service")
		So(badgeEquipment, ShouldBeNil)
	})
}

func TestAutoEquip_TierNotificationError(t *testing.T) {
	testUserId := "123"
	testChannelId := "456"
	bitsCheered := int64(1000)
	bitsTotal := int64(10001)
	bestAchievedTierEntitlement := dynamo.BadgeTier{
		ChannelId: testChannelId,
		Threshold: 10000,
		Enabled:   true,
	}
	previousBestAchievedTierEntitlement := dynamo.BadgeTier{
		ChannelId: testChannelId,
		Threshold: 5000,
		Enabled:   true,
	}

	mockedLogician := new(badgetiers_mock.Logician)
	mockedChatBadgeClient := new(chatbadgeclient_mock.IChatBadgesClient)
	mockedPubsubClient := new(client_mock.PubClient)
	configuration := &config.Configuration{}
	dataScience := &datascience.DataScienceApiNoOp{}
	mockedTierNotificationsSetter := new(tiernotifications_mock.Setter)

	equipper := chatBadgeEquipper{
		ChatBadgeLogician:      mockedLogician,
		ChatBadgesClient:       mockedChatBadgeClient,
		DataScienceClient:      dataScience,
		PubClient:              mockedPubsubClient,
		Config:                 configuration,
		TierNotificationSetter: mockedTierNotificationsSetter,
	}

	mockedLogician.On("GetBestAchievedTierEntitlement", mock.Anything, testChannelId, bitsTotal).Return(bestAchievedTierEntitlement, nil)
	mockedLogician.On("GetBestAchievedTierEntitlement", mock.Anything, testChannelId, bitsTotal-bitsCheered).Return(previousBestAchievedTierEntitlement, nil)
	mockedChatBadgeClient.On("GetBitsImages", mock.Anything, mock.Anything, mock.Anything).Return(nil, nil)
	mockedChatBadgeClient.On("GetSelectedChannelBadge", mock.Anything, testUserId, testChannelId, (*twitchclient.ReqOpts)(nil)).Return(&models.Badge{
		BadgeSetID:      "bits",
		BadgeSetVersion: "5000",
	}, nil)
	mockedChatBadgeClient.On("SelectBitsBadge", mock.Anything, testUserId, testChannelId, int64(10000), (*twitchclient.ReqOpts)(nil)).Return(true, nil)
	mockedPubsubClient.On("Publish", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)

	Convey("Auto-Equip continues as normal when the tier notification creation fails", t, func() {
		mockedTierNotificationsSetter.On("CreateNotification", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(errors.New("testError"))
		badgeEquipment, err := equipper.AutoEquip(context.Background(), testUserId, testChannelId, bitsCheered, bitsTotal, "")
		So(err, ShouldBeNil)
		So(badgeEquipment.NewVersion, ShouldEqual, bestAchievedTierEntitlement.Threshold)
		So(badgeEquipment.PreviousVersion, ShouldEqual, previousBestAchievedTierEntitlement.Threshold)
	})
}

func TestAutoEquip_PubSubError(t *testing.T) {
	testUserId := "123"
	testChannelId := "456"
	bitsCheered := int64(1000)
	bitsTotal := int64(10001)
	bestAchievedTierEntitlement := dynamo.BadgeTier{
		ChannelId: testChannelId,
		Threshold: 10000,
		Enabled:   true,
	}
	previousBestAchievedTierEntitlement := dynamo.BadgeTier{
		ChannelId: testChannelId,
		Threshold: 5000,
		Enabled:   true,
	}

	mockedLogician := new(badgetiers_mock.Logician)
	mockedChatBadgeClient := new(chatbadgeclient_mock.IChatBadgesClient)
	mockedPubsubClient := new(client_mock.PubClient)
	configuration := &config.Configuration{}
	dataScience := &datascience.DataScienceApiNoOp{}
	mockedTierNotificationsSetter := new(tiernotifications_mock.Setter)

	equipper := chatBadgeEquipper{
		ChatBadgeLogician:      mockedLogician,
		ChatBadgesClient:       mockedChatBadgeClient,
		DataScienceClient:      dataScience,
		PubClient:              mockedPubsubClient,
		Config:                 configuration,
		TierNotificationSetter: mockedTierNotificationsSetter,
	}

	mockedLogician.On("GetBestAchievedTierEntitlement", mock.Anything, testChannelId, bitsTotal).Return(bestAchievedTierEntitlement, nil)
	mockedLogician.On("GetBestAchievedTierEntitlement", mock.Anything, testChannelId, bitsTotal-bitsCheered).Return(previousBestAchievedTierEntitlement, nil)
	mockedChatBadgeClient.On("GetBitsImages", mock.Anything, mock.Anything, mock.Anything).Return(nil, nil)
	mockedChatBadgeClient.On("GetSelectedChannelBadge", mock.Anything, testUserId, testChannelId, (*twitchclient.ReqOpts)(nil)).Return(&models.Badge{
		BadgeSetID:      "bits",
		BadgeSetVersion: "5000",
	}, nil)
	mockedChatBadgeClient.On("SelectBitsBadge", mock.Anything, testUserId, testChannelId, int64(10000), (*twitchclient.ReqOpts)(nil)).Return(true, nil)
	mockedTierNotificationsSetter.On("CreateNotification", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)

	Convey("Auto-Equip continues as normal when the pubsub message fails", t, func() {
		mockedPubsubClient.On("Publish", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(errors.New("testError"))
		badgeEquipment, err := equipper.AutoEquip(context.Background(), testUserId, testChannelId, bitsCheered, bitsTotal, "")
		So(err, ShouldBeNil)
		So(badgeEquipment.NewVersion, ShouldEqual, bestAchievedTierEntitlement.Threshold)
		So(badgeEquipment.PreviousVersion, ShouldEqual, previousBestAchievedTierEntitlement.Threshold)
	})
}
