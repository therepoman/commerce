package bitsbadges

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"code.justin.tv/chat/badges/app/models"
	pubclient "code.justin.tv/chat/pubsub-go-pubclient/client"
	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/badgetiers"
	badgeTierLogician "code.justin.tv/commerce/payday/badgetiers/logician"
	tiernotifications "code.justin.tv/commerce/payday/badgetiers/products/notifications"
	"code.justin.tv/commerce/payday/badgetiers/utils"
	chatbadgesClient "code.justin.tv/commerce/payday/clients/chatbadges"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/datascience"
	"code.justin.tv/commerce/payday/errors"
	payday_models "code.justin.tv/commerce/payday/models"
	"github.com/gofrs/uuid"
)

const (
	BadgeEquipEarnType = "chat"
)

type ChatBadgeEquipper interface {
	AutoEquip(ctx context.Context, userId string, channelId string, bitsCheered int64, bitsTotal int64, transactionID string) (*payday_models.BadgeEntitlement, error)
}

type chatBadgeEquipper struct {
	ChatBadgeLogician      badgeTierLogician.Logician         `inject:""`
	ChatBadgesClient       chatbadgesClient.IChatBadgesClient `inject:""`
	Config                 *config.Configuration              `inject:""`
	DataScienceClient      datascience.DataScience            `inject:""`
	PubClient              pubclient.PubClient                `inject:""`
	TierNotificationSetter tiernotifications.Setter           `inject:""`
}

func NewEquipper() ChatBadgeEquipper {
	return &chatBadgeEquipper{}
}

type ChatBadgeEquipRecord struct {
	UserID    string `json:"user_id"`
	ChannelID string `json:"channel_id"`
	Threshold string `json:"badge_threshold"`
	Date      string `json:"date"`
	ErrorMsg  string `json:"error_msg"`
}

// AutoEquip checks if the user has passed a new and available bits badge threshold for a channel, and auto-equips this new badge if the user has a lower bits badge selected.
// Also issues a tier notification and tracks related data science.
func (e *chatBadgeEquipper) AutoEquip(ctx context.Context, userId string, channelId string, bitsCheered int64, bitsTotal int64, transactionID string) (*payday_models.BadgeEntitlement, error) {
	log := logrus.WithFields(logrus.Fields{
		"transactionID": transactionID,
		"userID":        userId,
		"channelID":     channelId,
		"bitsCheered":   bitsCheered,
		"bitsTotal":     bitsTotal,
	})

	if bitsCheered == 0 {
		msg := fmt.Sprintf("No Bits cheered for user %s on channel %s", userId, channelId)
		log.Info(msg)
		return nil, nil
	} else if bitsCheered < 0 {
		msg := "Negative bitsCheered were passed in when we expect a 0 or a positive int64"
		log.WithField("bitsUsed", bitsCheered).Error(msg)
		return nil, errors.New(msg)
	}

	highestAchievedTier, err := e.ChatBadgeLogician.GetBestAchievedTierEntitlement(ctx, channelId, bitsTotal)
	if err != nil {
		msg := "Error determining badge entitlements from dynamoDB"
		log.WithError(err).Error(msg)
		return nil, errors.Notef(err, msg)
	} else {
		log.WithField("highestAchievedTier", highestAchievedTier).Info("successfully fetched highest achieved tier for determining badge entitlements")
	}

	// bits cheer did not put user over the threshold --> do not auto-equip
	if highestAchievedTier.Threshold == 0 || highestAchievedTier.Threshold <= bitsTotal-bitsCheered {
		log.WithField("threshold", highestAchievedTier.Threshold).Info("bits cheer did not put user over the threshold for badge entitlement, returning nil")
		return nil, nil
	}

	chatBadgeTier, isPresent := badgetiers.BitsChatBadgeTiers[highestAchievedTier.Threshold]
	if !isPresent || !chatBadgeTier.Enabled {
		msg := "Dynamo tier records resolve to invalid chat badge tier"
		log.Error(msg)
		return nil, errors.New(msg)
	}

	newBadgeRecord := ChatBadgeEquipRecord{
		UserID:    userId,
		ChannelID: channelId,
		Threshold: chatBadgeTier.BadgeSetVersion,
		Date:      time.Now().Format(time.RFC1123),
	}

	success, err := e.ChatBadgesClient.SelectBitsBadge(ctx, userId, channelId, highestAchievedTier.Threshold, nil)
	if err != nil {
		msg := "Encountered an error selecting new bits badge in chat badge service"
		log.WithError(err).Error(msg)
		newBadgeRecord.ErrorMsg = msg
		return nil, errors.Notef(err, msg)
	} else if !success {
		msg := fmt.Sprintf("Bits badge with threshold %d not available in badge service for user %s on channel %s", highestAchievedTier.Threshold, userId, channelId)
		log.Error(msg)
		newBadgeRecord.ErrorMsg = msg
		return nil, errors.New(msg)
	}

	go func() {
		createNotifCtx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()
		tierError := e.TierNotificationSetter.CreateNotification(createNotifCtx, channelId, userId, highestAchievedTier.Threshold)

		if tierError != nil {
			log.WithError(tierError).WithField("threshold", highestAchievedTier.Threshold).Error("failed to create bits badge tier notification")
		} else {
			log.Info("created bits badge tier notification successfully")
		}
	}()

	err = e.publishMessage(ctx, channelId, userId, models.GrantBadgeParams{
		BadgeSetVersion: newBadgeRecord.Threshold,
	})
	if err != nil {
		log.WithError(err).Error("Could not send pubsub message for badge equipment")
	} else {
		log.Info("Sent pubsub message for badge equipment")
	}

	go e.trackEquipEvent(channelId, userId, highestAchievedTier.Threshold, BadgeEquipEarnType, transactionID)

	go e.trackBitsBadgeDisplaySelectionEvent(channelId, userId, highestAchievedTier.Threshold, "AUTO-EQUIP")

	equipment := payday_models.BadgeEntitlement{
		NewVersion:      highestAchievedTier.Threshold,
		PreviousVersion: 0,
	}

	previousTier, err := e.ChatBadgeLogician.GetBestAchievedTierEntitlement(ctx, channelId, bitsTotal-bitsCheered)
	if err != nil {
		msg := "Error determining previous badge tier"
		log.WithError(err).Error(msg)
		return &equipment, errors.Notef(err, msg)
	} else {
		log.WithField("previousTier", previousTier).Info("Successfully fetched previous badge tier")
	}

	equipment.PreviousVersion = previousTier.Threshold
	log.WithField("equipment", equipment).Info("AutoEquipping bits badge completed")

	return &equipment, nil
}

func (e *chatBadgeEquipper) publishMessage(ctx context.Context, channelID, userID string, newBadge models.GrantBadgeParams) error {
	badgeUpdate := payday_models.BadgeUpdate{
		UserID:        userID,
		ChannelID:     channelID,
		NewestVersion: newBadge.BadgeSetVersion,
		SetID:         payday_models.BitsBadgeUpdateSetID,
	}

	messageIdUUID, err := uuid.NewV4()
	if err != nil {
		return errors.Notef(err, "Could not generate UUID")
	}

	topics := []string{fmt.Sprintf("%s.%s", payday_models.UserBitsUpdatesPubsubMessageTopic, userID)}
	pubsubMessage := payday_models.PrivateBitsMessage{
		Data:        badgeUpdate,
		MessageId:   messageIdUUID.String(),
		MessageType: payday_models.BitBadgeUpdateMessageType,
		Version:     payday_models.CurrentPrivateMessageVersion,
	}

	marshaled, err := json.Marshal(pubsubMessage)
	if err != nil {
		return errors.Notef(err, "Could not marshal pubsub message %v", pubsubMessage)
	}

	err = e.PubClient.Publish(ctx, topics, string(marshaled), nil)
	if err != nil {
		return errors.Notef(err, "Could not publish message %s to pubsub", string(marshaled))
	}

	return nil
}

func (e *chatBadgeEquipper) trackEquipEvent(channelID string, userID string, threshold int64, earnType string, transactionID string) {
	log := logrus.WithFields(logrus.Fields{
		"transactionID": transactionID,
		"userID":        userID,
		"channelID":     channelID,
		"threshold":     threshold,
		"earnType":      earnType,
	})

	badgeType := "default"
	customizedBadges, err := utils.GetBitsBadgeCustomizationInfo(e.ChatBadgesClient, channelID)
	if err != nil {
		msg := "Encountered an error getting channel badges from chat badge service"
		log.WithError(err).Error(msg)
	}
	if customized, present := customizedBadges[threshold]; present && customized {
		badgeType = "custom"
	}

	badgeEquipEvent := &payday_models.BadgeEquipEvent{
		ChannelID:     channelID,
		UserID:        userID,
		BadgeAmount:   threshold,
		EarnType:      earnType,
		TransactionID: transactionID,
		Type:          badgeType,
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*3)
	defer cancel()
	err = e.DataScienceClient.TrackBitsEvent(ctx, datascience.BitsBadgeEntitleEventName, badgeEquipEvent)
	if err != nil {
		log.WithError(err).Error("ChatBadgeEquipper: Failed to send Equip event to data science")
	} else {
		log.Info("ChatBadgeEquipper: Successfully sent Equip event to data science")
	}
}

func (e *chatBadgeEquipper) trackBitsBadgeDisplaySelectionEvent(channelID string, userID string, threshold int64, action string) {
	log := logrus.WithFields(logrus.Fields{
		"userID":    userID,
		"channelID": channelID,
		"threshold": threshold,
		"action":    action,
	})

	event := &payday_models.BitsBadgeDisplaySelectionEvent{
		ChannelID:   channelID,
		UserID:      userID,
		BadgeAmount: threshold,
		Action:      action,
	}

	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	err := e.DataScienceClient.TrackBitsEvent(ctx, datascience.BitsBadgeDisplaySelectionName, event)
	if err != nil {
		log.WithError(err).Error("ChatBadgeEquipper: Failed to send BitsBadgeDisplaySelection event to data science")
	} else {
		log.Info("ChatBadgeEquipper: Successfully sent BitsBadgeDisplaySelection event to data science")
	}
}
