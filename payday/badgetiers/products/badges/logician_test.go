package bitsbadges

import (
	"context"
	"testing"
	"time"

	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	chatbadges "code.justin.tv/commerce/payday/badgetiers"
	"code.justin.tv/commerce/payday/badgetiers/models"
	"code.justin.tv/commerce/payday/clients/pantheon"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/errors"
	clue_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/chat/tmi/client"
	chatbadges_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/badgetiers/fetcher"
	pantheon_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/pantheon"
	metrics_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/metrics"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

type testContext struct {
	PantheonResponse *pantheonrpc.GetLeaderboardResp
	PantheonError    error
	FetchResponse    models.BitsChatBadgeTiers
	FetchError       error
}

const ChannelId = "test_channel_id"
const UserId = "test_user_id"

func generateChannelBadges() models.BitsChatBadgeTiers {
	badges := make([]dynamo.BadgeTier, 0, len(chatbadges.BitsChatBadgeTiers))
	for tierThreshold, tierConfig := range chatbadges.BitsChatBadgeTiers {
		badges = append(badges, dynamo.BadgeTier{
			ChannelId:   ChannelId,
			Threshold:   tierThreshold,
			Enabled:     tierConfig.Enabled,
			LastUpdated: time.Time{},
		})
	}
	return models.BitsChatBadgeTiers{
		BadgeTiers:  badges,
		ChannelId:   ChannelId,
		LastUpdated: time.Now(),
	}
}

func setup(c testContext) (*pantheon_mock.IPantheonClientWrapper, *chatbadges_mock.Fetcher, Logician) {
	Pantheon := new(pantheon_mock.IPantheonClientWrapper)
	Pantheon.On("GetLeaderboard",
		mock.Anything,
		pantheon.PantheonBitsUsageDomain,
		pantheonrpc.TimeUnit_ALLTIME,
		ChannelId,
		UserId,
		int64(0),
		int64(0),
		mock.Anything).Return(c.PantheonResponse, c.PantheonError)

	mockFetcher := new(chatbadges_mock.Fetcher)
	mockFetcher.On("Fetch", mock.Anything, ChannelId).Return(c.FetchResponse, c.FetchError)

	Metrics := new(metrics_mock.Statter)
	Metrics.On("Inc", ClueCall, mock.AnythingOfType("int64")).Return()

	Clue := new(clue_mock.Client)

	fetcher := &logician{
		Pantheon,
		mockFetcher,
		Metrics,
		Clue,
	}

	return Pantheon, mockFetcher, fetcher
}

func TestFetcher_Valid_Case(t *testing.T) {
	totalBitsSpent := int64(25001)
	testResponses := testContext{
		PantheonResponse: &pantheonrpc.GetLeaderboardResp{
			EntryContext: &pantheonrpc.EntryContext{
				Entry: &pantheonrpc.LeaderboardEntry{
					Rank:     1,
					Score:    totalBitsSpent,
					EntryKey: UserId,
				},
			},
		},
		PantheonError: nil,
		FetchResponse: generateChannelBadges(),
		FetchError:    nil,
	}

	balanceManager, badgeFetcher, fetcher := setup(testResponses)

	ctx := context.Background()

	Convey("New case fetches all available bits badges in the right order", t, func() {

		resultBitsBadges, err := fetcher.GetUserBitsBadgesAvailableForChannel(ctx, UserId, ChannelId)
		So(len(balanceManager.Calls), ShouldEqual, 1)
		So(len(badgeFetcher.Calls), ShouldEqual, 1)
		So(len(resultBitsBadges), ShouldEqual, 6)
		So(err, ShouldBeNil)
		So(resultBitsBadges[0].BadgeSetVersion, ShouldEqual, "1")
		So(resultBitsBadges[0].BadgeSetId, ShouldEqual, "bits")
		So(resultBitsBadges[1].BadgeSetVersion, ShouldEqual, "100")
		So(resultBitsBadges[1].BadgeSetId, ShouldEqual, "bits")
		So(resultBitsBadges[2].BadgeSetVersion, ShouldEqual, "1000")
		So(resultBitsBadges[2].BadgeSetId, ShouldEqual, "bits")
		So(resultBitsBadges[3].BadgeSetVersion, ShouldEqual, "5000")
		So(resultBitsBadges[3].BadgeSetId, ShouldEqual, "bits")
		So(resultBitsBadges[4].BadgeSetVersion, ShouldEqual, "10000")
		So(resultBitsBadges[4].BadgeSetId, ShouldEqual, "bits")
		So(resultBitsBadges[5].BadgeSetVersion, ShouldEqual, "25000")
		So(resultBitsBadges[5].BadgeSetId, ShouldEqual, "bits")
	})
}

func TestFetcher_NoUserID(t *testing.T) {
	Convey("For a logician with no user id in the request", t, func() {
		ctx := context.Background()
		testResponses := testContext{
			PantheonResponse: nil,
			PantheonError:    nil,
			FetchResponse:    generateChannelBadges(),
			FetchError:       nil,
		}
		balanceManager, badgeFetcher, fetcher := setup(testResponses)

		_, err := fetcher.GetUserBitsBadgesAvailableForChannel(ctx, "", ChannelId)
		So(err, ShouldNotBeNil)
		So(len(balanceManager.Calls), ShouldBeZeroValue)
		So(len(badgeFetcher.Calls), ShouldBeZeroValue)
	})
}

func TestFetcher_NoChannelID(t *testing.T) {
	ctx := context.Background()
	testResponses := testContext{
		PantheonResponse: nil,
		PantheonError:    nil,
		FetchResponse:    generateChannelBadges(),
		FetchError:       nil,
	}
	balanceManager, badgeFetcher, fetcher := setup(testResponses)

	Convey("It should return and error when no channel ID is passed", t, func() {
		_, err := fetcher.GetUserBitsBadgesAvailableForChannel(ctx, UserId, "")
		So(err, ShouldNotBeNil)
		So(err.Error(), ShouldEqual, "twirp error invalid_argument: ChannelId cannot have empty channel id")
		So(len(balanceManager.Calls), ShouldEqual, 0)
		So(len(badgeFetcher.Calls), ShouldEqual, 0)
	})
}

func TestFetcher_pantheonGeneralError(t *testing.T) {
	ctx := context.Background()
	testResponses := testContext{
		PantheonResponse: nil,
		PantheonError:    errors.New("testBalanceError"),
		FetchResponse:    generateChannelBadges(),
		FetchError:       nil,
	}
	balanceManager, badgeFetcher, fetcher := setup(testResponses)

	Convey("Returns an error when Pantheon errors", t, func() {
		_, err := fetcher.GetUserBitsBadgesAvailableForChannel(ctx, UserId, ChannelId)
		So(err, ShouldNotBeNil)
		So(err.Error(), ShouldStartWith, "twirp error internal: Error getting leaderboard entry (spent bits) for user in channel from Pantheon for determining badges available to user")
		So(len(balanceManager.Calls), ShouldEqual, 1)
		So(len(badgeFetcher.Calls), ShouldEqual, 0)
	})
}

func TestFetcher_pantheonResponseNull(t *testing.T) {
	ctx := context.Background()
	testResponses := testContext{
		PantheonResponse: nil,
		PantheonError:    nil,
		FetchResponse:    generateChannelBadges(),
		FetchError:       nil,
	}
	balanceManager, badgeFetcher, fetcher := setup(testResponses)

	Convey("Returns an empty array when pantheon's response is null", t, func() {
		resp, err := fetcher.GetUserBitsBadgesAvailableForChannel(ctx, UserId, ChannelId)
		So(err, ShouldBeNil)
		So(resp, ShouldResemble, []*paydayrpc.Badge{})
		So(len(balanceManager.Calls), ShouldEqual, 1)
		So(len(badgeFetcher.Calls), ShouldEqual, 0)
	})
}

func TestFetcher_entryContextNullError(t *testing.T) {
	ctx := context.Background()
	testResponses := testContext{
		PantheonResponse: &pantheonrpc.GetLeaderboardResp{
			EntryContext: nil,
		},
		PantheonError: nil,
		FetchResponse: generateChannelBadges(),
		FetchError:    nil,
	}
	balanceManager, badgeFetcher, fetcher := setup(testResponses)

	Convey("Returns an empty array when pantheon's response does not contain an entry context", t, func() {
		resp, err := fetcher.GetUserBitsBadgesAvailableForChannel(ctx, UserId, ChannelId)
		So(err, ShouldBeNil)
		So(resp, ShouldResemble, []*paydayrpc.Badge{})
		So(len(balanceManager.Calls), ShouldEqual, 1)
		So(len(badgeFetcher.Calls), ShouldEqual, 0)
	})
}

func TestFetcher_entryNullError(t *testing.T) {
	ctx := context.Background()
	testResponses := testContext{
		PantheonResponse: &pantheonrpc.GetLeaderboardResp{
			EntryContext: &pantheonrpc.EntryContext{
				Entry: nil,
			},
		},
		PantheonError: nil,
		FetchResponse: generateChannelBadges(),
		FetchError:    nil,
	}
	balanceManager, badgeFetcher, fetcher := setup(testResponses)

	Convey("Returns an empty array when pantheon's response does not contain an entry", t, func() {
		resp, err := fetcher.GetUserBitsBadgesAvailableForChannel(ctx, UserId, ChannelId)
		So(err, ShouldBeNil)
		So(resp, ShouldResemble, []*paydayrpc.Badge{})
		So(len(balanceManager.Calls), ShouldEqual, 1)
		So(len(badgeFetcher.Calls), ShouldEqual, 0)
	})
}

func TestFetcher_FetcherError(t *testing.T) {
	ctx := context.Background()
	totalBitsSpent := int64(10000)
	testResponses := testContext{
		PantheonResponse: &pantheonrpc.GetLeaderboardResp{
			EntryContext: &pantheonrpc.EntryContext{
				Entry: &pantheonrpc.LeaderboardEntry{
					Rank:     1,
					Score:    totalBitsSpent,
					EntryKey: UserId,
				},
			},
		},
		PantheonError: nil,
		FetchError:    errors.New("testFetcherError"),
	}
	balanceManager, badgeFetcher, fetcher := setup(testResponses)

	Convey("Returns an error when the badgeFetcher errors", t, func() {
		_, err := fetcher.GetUserBitsBadgesAvailableForChannel(ctx, UserId, ChannelId)
		So(err, ShouldNotBeNil)
		So(len(balanceManager.Calls), ShouldEqual, 1)
		So(len(badgeFetcher.Calls), ShouldEqual, 1)
	})
}

func TestFetcher_Valid_FetcherEmpty(t *testing.T) {
	totalBitsSpent := int64(25001)
	testResponses := testContext{
		PantheonResponse: &pantheonrpc.GetLeaderboardResp{
			EntryContext: &pantheonrpc.EntryContext{
				Entry: &pantheonrpc.LeaderboardEntry{
					Rank:     1,
					Score:    totalBitsSpent,
					EntryKey: UserId,
				},
			},
		},
		PantheonError: nil,
		FetchResponse: models.BitsChatBadgeTiers{
			BadgeTiers: []dynamo.BadgeTier{
				{
					Threshold: 1,
					Enabled:   true,
				},
				{
					Threshold: 100,
					Enabled:   true,
				},
				{
					Threshold: 5000,
					Enabled:   true,
				},
				{
					Threshold: 1000,
					Enabled:   true,
				},
				{
					Threshold: 25000,
					Enabled:   true,
				},
				{
					Threshold: 10000,
					Enabled:   true,
				},
				{
					Threshold: 50000,
					Enabled:   true,
				},
			},
		},
		FetchError: nil,
	}

	balanceManager, badgeFetcher, fetcher := setup(testResponses)

	ctx := context.Background()

	Convey("Returns the available badges, sorted lowest to highest, even if badgeFetcher returns the default badge settings", t, func() {
		resultBitsBadges, err := fetcher.GetUserBitsBadgesAvailableForChannel(ctx, UserId, ChannelId)
		So(len(balanceManager.Calls), ShouldEqual, 1)
		So(len(badgeFetcher.Calls), ShouldEqual, 1)
		So(len(resultBitsBadges), ShouldEqual, 6)
		So(err, ShouldBeNil)
		So(resultBitsBadges[0].BadgeSetVersion, ShouldEqual, "1")
		So(resultBitsBadges[5].BadgeSetVersion, ShouldEqual, "25000")
		So(resultBitsBadges[0].BadgeSetId, ShouldEqual, "bits")
	})
}

func TestFetcher_Valid_FetcherLowestDisabled(t *testing.T) {
	totalBitsSpent := int64(90)
	testResponses := testContext{
		PantheonResponse: &pantheonrpc.GetLeaderboardResp{
			EntryContext: &pantheonrpc.EntryContext{
				Entry: &pantheonrpc.LeaderboardEntry{
					Rank:     1,
					Score:    totalBitsSpent,
					EntryKey: UserId,
				},
			},
		},
		PantheonError: nil,
		FetchResponse: models.BitsChatBadgeTiers{
			ChannelId: ChannelId,
			BadgeTiers: []dynamo.BadgeTier{
				{
					ChannelId:   ChannelId,
					Threshold:   1,
					Enabled:     false,
					LastUpdated: time.Time{},
				},
			},
		},
		FetchError: nil,
	}

	balanceManager, badgeFetcher, fetcher := setup(testResponses)

	ctx := context.Background()

	Convey("Returns no Badge if the lower thresholds are disabled", t, func() {
		resultBitsBadges, err := fetcher.GetUserBitsBadgesAvailableForChannel(ctx, UserId, ChannelId)
		So(len(balanceManager.Calls), ShouldEqual, 1)
		So(len(badgeFetcher.Calls), ShouldEqual, 1)
		So(len(resultBitsBadges), ShouldEqual, 0)
		So(err, ShouldBeNil)
	})
}
