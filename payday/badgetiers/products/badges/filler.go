package bitsbadges

import (
	"context"
	"errors"
	"net/http"
	"strconv"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/api/constants"
	chatBadgesClient "code.justin.tv/commerce/payday/clients/chatbadges"
	httperror "code.justin.tv/commerce/payday/errors/http"
	"code.justin.tv/commerce/payday/models/api"
	"code.justin.tv/commerce/payday/utils/pointers"
)

type Filler interface {
	PopulateWithCustomImages(ctx context.Context, channelID string, tierMap map[int64]*api.BadgeTier) (map[int64]*api.BadgeTier, error)
}

type filler struct {
	ChatBadgesClient chatBadgesClient.IChatBadgesClient `inject:""`
}

func NewFiller() Filler {
	return &filler{}
}

func (f *filler) PopulateWithCustomImages(ctx context.Context, channelID string, tierMap map[int64]*api.BadgeTier) (map[int64]*api.BadgeTier, error) {
	badgesResp, err := f.ChatBadgesClient.GetBitsImages(ctx, channelID, nil)
	if err != nil {
		msg := "error getting bits badge images for channel from chat badges service"
		log.WithError(err).WithField("channelId", channelID).Error(msg)
		return nil, httperror.NewWithCause(constants.DownstreamDependencyError, http.StatusInternalServerError, errors.New(msg))
	}

	if badgesResp == nil {
		msg := "received a nil response from chat badges client get bits images call"
		log.WithError(err).WithField("channelId", channelID).Error(msg)
		return nil, httperror.NewWithCause(constants.DownstreamDependencyError, http.StatusInternalServerError, errors.New(msg))
	}

	for badgeSetVersion, badgeSet := range badgesResp.Versions {
		threshold, err := strconv.ParseInt(badgeSetVersion, 10, 64)
		if err != nil {
			log.WithError(err).WithFields(log.Fields{
				"badgeSetVersion": badgeSetVersion,
				"channelId":       channelID,
			}).Error("Chat badges service returned bits badge with non-numeric bits badge set version")
			continue
		}

		currentTier, present := tierMap[threshold]
		if !present {
			log.WithFields(log.Fields{
				"threshold": threshold,
				"channelId": channelID,
			}).Error("Chat badges service returned bits badge for non-existent tier threshold")
			continue
		}

		currentTier.ImageURL1x = pointers.StringOrDefault(badgeSet.ImageURL1x, "")
		currentTier.ImageURL2x = pointers.StringOrDefault(badgeSet.ImageURL2x, "")
		currentTier.ImageURL4x = pointers.StringOrDefault(badgeSet.ImageURL4x, "")
		currentTier.Title = badgeSet.Title
		currentTier.LastUpdated = badgeSet.LastUpdated
	}
	return tierMap, nil
}
