package admin

import (
	"context"

	badgetieremotes "code.justin.tv/commerce/payday/badgetiers/products/emotes"
	"code.justin.tv/commerce/payday/leaderboard"
)

type BadgeTierAdmin interface {
	GrantUnlockedRewards(ctx context.Context, userID, channelID string) (int64, error)
}

type badgeTierAdmin struct {
	BitsUsageGetter leaderboard.BitsToBroadcasterGetter `inject:""`
	EmoteEntitler   badgetieremotes.Entitler            `inject:""`
}

func NewBadgeTierAdmin() BadgeTierAdmin {
	return &badgeTierAdmin{}
}
