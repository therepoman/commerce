package admin

import (
	"context"

	"code.justin.tv/commerce/payday/badgetiers"
	"code.justin.tv/commerce/payday/utils/strings"
	"github.com/pkg/errors"
)

func (admin *badgeTierAdmin) GrantUnlockedRewards(ctx context.Context, userID, channelID string) (int64, error) {
	if strings.Blank(userID) {
		return 0, errors.New("user ID is blank")
	}

	if strings.Blank(channelID) {
		return 0, errors.New("channel ID is blank")
	}

	totalBits, err := admin.BitsUsageGetter.GetBitsToBroadcaster(ctx, userID, channelID)
	if err != nil {
		return 0, errors.Wrap(err, "error retrieving total bits usage for user")
	}

	// for a channel viewer, use the user's total bits usage to entitle all unlocked rewards
	// for a broadcaster, find and use the highest tier's threshold to entitle all rewards
	totalBitsToCheckUnlockStatus := totalBits
	if userID == channelID {
		for threshold := range badgetiers.BitsChatBadgeTiers {
			if threshold > totalBitsToCheckUnlockStatus {
				totalBitsToCheckUnlockStatus = threshold
			}
		}
	}

	if totalBitsToCheckUnlockStatus == 0 {
		return 0, nil
	}

	err = admin.EmoteEntitler.EntitleAll(ctx, userID, channelID, totalBitsToCheckUnlockStatus)
	if err != nil {
		return 0, errors.Wrap(err, "error entitling emotes")
	}

	return totalBits, nil
}
