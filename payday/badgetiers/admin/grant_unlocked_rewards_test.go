package admin

import (
	"context"
	"errors"
	"testing"

	badgetieremotes_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/badgetiers/products/emotes"
	leaderboard_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/leaderboard"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestGrantUnlockedRewards(t *testing.T) {
	Convey("Given an admin handler", t, func() {
		bitsUsageGetter := new(leaderboard_mock.BitsToBroadcasterGetter)
		emoteEntitler := new(badgetieremotes_mock.Entitler)

		admin := badgeTierAdmin{
			BitsUsageGetter: bitsUsageGetter,
			EmoteEntitler:   emoteEntitler,
		}

		userID := "123"
		channelID := "456"
		totalBitsCheered := int64(123456)

		Convey("when user has cheered in the channel", func() {
			bitsUsageGetter.On("GetBitsToBroadcaster", mock.Anything, userID, channelID).Return(totalBitsCheered, nil)
			emoteEntitler.On("EntitleAll", mock.Anything, userID, channelID, totalBitsCheered).Return(nil)

			totalBits, err := admin.GrantUnlockedRewards(context.Background(), userID, channelID)
			So(err, ShouldBeNil)
			So(totalBits, ShouldEqual, totalBitsCheered)
		})

		Convey("when user has not cheered in the channel", func() {
			bitsUsageGetter.On("GetBitsToBroadcaster", mock.Anything, userID, channelID).Return(int64(0), nil)
			emoteEntitler.On("EntitleAll", mock.Anything, userID, channelID, totalBitsCheered).Return(nil)

			totalBits, err := admin.GrantUnlockedRewards(context.Background(), userID, channelID)
			So(err, ShouldBeNil)
			So(totalBits, ShouldEqual, 0)
			emoteEntitler.AssertNumberOfCalls(t, "EntitleAll", 0)
		})

		Convey("when user is the channel owner", func() {
			highestTierThreshold := int64(5000000) // see badgetiers.BitsChatBadgeTiers
			bitsUsageGetter.On("GetBitsToBroadcaster", mock.Anything, channelID, channelID).Return(int64(0), nil)
			emoteEntitler.On("EntitleAll", mock.Anything, channelID, channelID, highestTierThreshold).Return(nil)

			totalBits, err := admin.GrantUnlockedRewards(context.Background(), channelID, channelID)
			So(err, ShouldBeNil)
			So(totalBits, ShouldEqual, int64(0))
			emoteEntitler.AssertNumberOfCalls(t, "EntitleAll", 1)
		})

		Convey("returns an error when", func() {
			Convey("user ID is blank", func() {
				userID = ""
			})

			Convey("channel ID is blank", func() {
				channelID = ""
			})

			Convey("it fails to retrieve user's bits usage", func() {
				bitsUsageGetter.On("GetBitsToBroadcaster", mock.Anything, userID, channelID).Return(int64(0), errors.New("can't get"))
			})

			Convey("it fails to entitle emotes", func() {
				bitsUsageGetter.On("GetBitsToBroadcaster", mock.Anything, userID, channelID).Return(totalBitsCheered, nil)
				emoteEntitler.On("EntitleAll", mock.Anything, userID, channelID, totalBitsCheered).Return(errors.New("can't entitle"))
			})

			_, err := admin.GrantUnlockedRewards(context.Background(), userID, channelID)
			So(err, ShouldNotBeNil)
		})
	})
}
