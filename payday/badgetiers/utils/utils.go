package utils

import (
	"context"
	"errors"
	"sort"
	"strconv"
	"time"

	chatclient "code.justin.tv/commerce/payday/clients/chatbadges"
)

// int64slice attaches the methods of Interface to []int64, sorting in increasing order.
type int64Slice []int64

func (p int64Slice) Len() int           { return len(p) }
func (p int64Slice) Less(i, j int) bool { return p[i] < p[j] }
func (p int64Slice) Swap(i, j int)      { p[i], p[j] = p[j], p[i] }

// Sort is a convenience method.
func (p int64Slice) Sort() { sort.Sort(p) }

// SortInt64s sorts a slice of int64s in increasing order.
func SortInt64s(a []int64) { sort.Sort(int64Slice(a)) }

func GetBitsBadgeCustomizationInfo(chatClient chatclient.IChatBadgesClient, channelID string) (map[int64]bool, error) {
	results := make(map[int64]bool)

	if chatClient == nil {
		return results, errors.New("chat client must be provided")
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*3)
	defer cancel()

	channelBadges, err := chatClient.GetBitsImages(ctx, channelID, nil)
	if err != nil || channelBadges == nil {
		return results, err
	}

	for version := range channelBadges.Versions {
		threshold, err := strconv.Atoi(version)
		if err != nil && threshold > 0 {
			results[int64(threshold)] = true
		}
	}

	return results, nil
}
