# Payday Service Docs

## How-To Guides

### Backend Experiments

We have a lightweight utility in payday for running backend experiments [here](https://git-aws.internal.justin.tv/commerce/payday/blob/master/utils/experiments/experiments.go). To set up a new experiment, create a folder under the "payday-experiment-config" S3 bucket in the Bits AWS account. The name of this folder should be used as the "experiment prefix", which are listed [here](https://git-aws.internal.justin.tv/commerce/payday/blob/master/utils/experiments/experiments.go#L15-L16). Then, add a file to that folder called "experiment_config.json" that looks like the following:
```
{
	"dial_up_percentage": 0,
	"user_whitelist": ["232889822"],
	"channel_whitelist": ["116076154"]
}
``` 
* dial_up_percentage: An integer [0-100] with 0 indicating the experiment is OFF, 100 indicating ON, and any int in between indicating the percentage of users that should be put into your exerimental treatment.
* user_whitelist: A list of string tuids of users that will receive the experimental treatment regardless of the dial_up_percentage.
* channel_whitelist: A list of string tuids of channels, where all users in that channel will receive the experimental treatment regardless of the dial_up_percentage.

The utility is designed in such a way that, if there is ever an error in fetching the experiment config, `IsExperimentActiveForUserAndChannel` will return false.

### Validating SWS Cert (or any cert)
Payday has SWS certs it fetches from Sandstorm dynamically. Here are steps to validate the new certs are being fetched when they rotate:

Start Payday safely in Prod env:
1. Use the [sws_cert](https://git-aws.internal.justin.tv/commerce/payday/tree/sws_cert) branch of payday. Take a look at the [backend.go](https://git-aws.internal.justin.tv/commerce/payday/blob/sws_cert/backend/backend.go) and [main.go](https://git-aws.internal.justin.tv/commerce/payday/blob/sws_cert/cmd/payday/main.go) changes. The goal is to disable all of the service except only what is necessary to instantiate the SWS manager and fetch the cert. Specifically, check out [these](https://git-aws.internal.justin.tv/commerce/payday/blob/sws_cert/cmd/payday/main.go#L119-L127) lines.
2. Run the following to start up the server in the Prod environment:
```
IMPORTANT: DON'T follow this step until you are sure that all of the backend.go file has been disabled. Otherwise, your laptop will run as a Payday Prod host and begin consuming Prod messages.

make build
${GOPATH}/bin/payday -e production
``` 
Extract details from the cert:
1. The above command should have started Payday, printed the cert to the terminal, and then exited. Add the printed cert to a new .crt file. 
2. Run the following to print the start and expiration dates of the cert, which you can use to validate that Payday is fetching the most recent cert:
```
openssl x509 -startdate -enddate -noout -in <name_of_cert_file>.crt
``` 