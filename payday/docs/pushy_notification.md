# Sending Bell Notification
## Overview
As of Black Friday promo (11/21/2018 - 11/26/2018), we built our own endpoint for sending bell notifications through [Pushy](https://git-aws.internal.justin.tv/chat/pushy). This document explains how to add per-event text for a new promo/event that we want to send bell notifications for step by step.

### Pushy change - 3 phases
Phase 1: add the new string and get it translated.
1. Make a branch and add the new text [here](https://git-aws.internal.justin.tv/chat/pushy/blob/e7b208022c0a203e5324e2e28f46afb1329b2381/internal/translation/phrases.go#L2439)
2. Run the following command:
    ```
    make extract-strings
    ```
    This is a necessary step to prepare for submitting translation later.
3. Create a PR and ping someone from #notifications (slack channel) to look at the change and get approval. After approval, merge it.
4. Switch branch to master, pull your change, and run the following command:
    ```
    make upload-strings
    ```
    This will submit the new string to Smartling.
5. Clone this Jira [LOC-128](https://jira.twitch.com/browse/LOC-128) and fill details, then click the "LOC Review" button

Phase 2: download the translation into Pushy repo
0. Wait for the translation to be done. When the LOC ticket is marked as done, go to the next step.
1. Make a branch and run the following command:
    ```
    make download-strings
    ```
2. Create a PR and ping someone from #notifications (slack channel) to look at the change and get approval. After approval, merge it.

Phase 3: make a code change
1. Make a branch and add a new entry in this [map](https://git-aws.internal.justin.tv/chat/pushy/blob/master/internal/onsite/bitspromo_renderer.go#L13) for the new text following the Black Friday example. This should be the only actual code change.
2. Add unit test as needed.
3. Create a PR and ping someone from #notifications (slack channel) to look at the change and get approval. After approval, merge it.
4. Deploy master to darklaunch(staging) and production.

You are done with Pushy at ths point. Greater details are actually all documented in their repo [here](https://git-aws.internal.justin.tv/chat/pushy/blob/master/docs/how_to_add_notification.md), if you are interested.

### Payday change
Add the event specific information in this [map](https://git-aws.internal.justin.tv/commerce/payday/blob/master/bulk_promo_notif/bulk_promo_notif.go#L22). Be aware that the `PushyPromoID` must match the new key you added in the map `promoIDtoText` in Pushy. Also, change the value of TPS [here](https://git-aws.internal.justin.tv/commerce/payday/blob/master/adminjob/handler/bulkpromonotif/bulkpromonotif.go#L154) as needed. For Black Friday promo, Pushy team was ok with 100 TPS but you never know so just ask them an appropriate value they are happy with.

### Send the bell notification!
After both Pushy and Payday changes are deployed, you can run the script at `cmd/promo_notif_job/main.go` to actually send the notification. The const values at the beginning of the file controls
* what promo you are sending notification about (`promoID`)
* who you are sending bell notifications to (`inputFile`) - give it a full path to the csv file which contains the list of user IDs in one column. On user ID per row without anything else
* what environment you are running the script in (`environment`) - should be either `production` or `staging`
* who you are (`creater`) - should be your LDAP

Especially you must make sure `promoID` matches the key of the map to the campaign you are targeting which you added in the Payday change step. When you made these change in main.go, go to the Payday root directory and run:
```
go build cmd/promo_notif_job/main.go
./main
```

It will start the admin job for sending notifications.