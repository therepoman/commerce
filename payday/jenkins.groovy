def ECR_URL = '021561903526.dkr.ecr.us-west-2.amazonaws.com'
def PRODUCTION_ECR_PATH = 'commerce/ecs/payday/production'
def STAGING_ECR_PATH = 'commerce/ecs/payday/staging'
def STAGING_LAMBDA_S3_BUCKET = "payday-staging-lambdas"
def PRODUCTION_LAMBDA_S3_BUCKET = "payday-production-lambdas"

job {
	name 'payday'
	using 'TEMPLATE-autobuild'

	scm {
		git {
			remote {
				github 'commerce/payday', 'ssh', 'git.xarth.tv'
				credentials 'git-aws-read-key'
			}
		}
	}

	wrappers {
		credentialsBinding {
			file('COURIERD_PRIVATE_KEY', 'courierd')
			file('AWS_CONFIG_FILE', 'aws_config')
			string 'AWS_ACCESS_KEY_ID', 'payday-deployment-access-key'
			string 'AWS_SECRET_ACCESS_KEY', 'payday-deployment-secret-key'
		}
		environmentVariables {
			env('PRODUCTION_ECR_PATH', "${PRODUCTION_ECR_PATH}")
			env('STAGING_ECR_PATH', "${STAGING_ECR_PATH}")
			env('ECR_URL', "${ECR_URL}")
			env('STAGING_LAMBDA_S3_BUCKET', "${STAGING_LAMBDA_S3_BUCKET}")
			env('PRODUCTION_LAMBDA_S3_BUCKET', "${PRODUCTION_LAMBDA_S3_BUCKET}")
		}
	}

	steps {
		/*
			Build Docker Container
		 */
		steps {
			shell 'manta -v -f .manta.json'
			// Container tags
			shell 'docker build -t $PRODUCTION_ECR_PATH -t $STAGING_ECR_PATH .'
		}

		/*
			Build Lambdas
		 */
		steps {
			shell "make docker_build_lambda"
		}

		/*
			TODO: Not sure if the manta one is used anywhere. Definitely can delete once we remove manta usage
		 */
		steps {
			saveDeployArtifact 'commerce/payday', '.manta'
		}

		/*
			Push Container to ECS
		 */

		// Push to staging ECR/lambdas only when not master
		steps {
			conditionalSteps{
				condition {
					not {
						stringsMatch('$GIT_BRANCH', 'origin/master', true)
					}
				}

				steps {
					/* ECR Login */
					shell "aws ecr get-login --no-include-email --region us-west-2 | cut -d' ' -f6 | docker login -u AWS --password-stdin ${ECR_URL}"

					/* Push to ECR */
					shell 'docker tag $STAGING_ECR_PATH $ECR_URL/$STAGING_ECR_PATH:$GIT_COMMIT'
					shell 'docker push $ECR_URL/$STAGING_ECR_PATH:$GIT_COMMIT'

					/* Push Lambdas */
					shell 'ENVIRONMENT=staging ./scripts/lambda/push_lambdas.sh $STAGING_LAMBDA_S3_BUCKET $GIT_COMMIT'
				}
			}
		}

		// Push to staging and prod ECR/lambdas when master
		steps {
			conditionalSteps {
				condition {
					stringsMatch('$GIT_BRANCH', 'origin/master', true)
				}

				steps {
					/*
						Staging
					 */
					shell "aws ecr get-login --no-include-email --region us-west-2 | cut -d' ' -f6 | docker login -u AWS --password-stdin ${ECR_URL}"

					// Push to ECR - tag latest IF master
					shell 'docker tag $STAGING_ECR_PATH $ECR_URL/$STAGING_ECR_PATH:$GIT_COMMIT'
					shell 'docker push $ECR_URL/$STAGING_ECR_PATH:$GIT_COMMIT'

					shell 'docker tag $STAGING_ECR_PATH $ECR_URL/$STAGING_ECR_PATH:latest'
					shell 'docker push $ECR_URL/$STAGING_ECR_PATH:latest'

					// Push Lambdas
					shell 'ENVIRONMENT=staging ./scripts/lambda/push_lambdas.sh $STAGING_LAMBDA_S3_BUCKET $GIT_COMMIT'
					shell 'ENVIRONMENT=staging ./scripts/lambda/push_lambdas.sh $STAGING_LAMBDA_S3_BUCKET latest'

					/*
                        Prod
                     */
					shell "aws ecr get-login --no-include-email --region us-west-2 | cut -d' ' -f6 | docker login -u AWS --password-stdin ${ECR_URL}"

					// Push to ECR - tag latest IF master
					shell 'docker tag $PRODUCTION_ECR_PATH $ECR_URL/$PRODUCTION_ECR_PATH:$GIT_COMMIT'
					shell 'docker push $ECR_URL/$PRODUCTION_ECR_PATH:$GIT_COMMIT'

					shell 'docker tag $PRODUCTION_ECR_PATH $ECR_URL/$PRODUCTION_ECR_PATH:latest'
					shell 'docker push $ECR_URL/$PRODUCTION_ECR_PATH:latest'

					// Push Lambdas
					shell 'ENVIRONMENT=production ./scripts/lambda/push_lambdas.sh $PRODUCTION_LAMBDA_S3_BUCKET $GIT_COMMIT'
					shell 'ENVIRONMENT=production ./scripts/lambda/push_lambdas.sh $PRODUCTION_LAMBDA_S3_BUCKET latest'
				}
			}
		}
	}
}

job {
	name 'commerce-payday-coverage'
	using 'TEMPLATE-autobuild'

	scm {
		git {
			remote {
				github 'commerce/payday', 'ssh', 'git.xarth.tv'
				credentials 'git-aws-read-key'
			}
		}
	}

	wrappers {
		credentialsBinding {
			string 'AWS_ACCESS_KEY', 'jenkins-terraform-aws-access-key'
			string 'AWS_SECRET_KEY', 'jenkins-terraform-aws-secret-key'
		}
		environmentVariables {
			env('GIT_BRANCH', '${GIT_BRANCH}')
			env('GIT_COMMIT', '${GIT_COMMIT}')
			env('GIT_URL', '${GIT_URL}')
		}
	}

	steps {
		shell 'manta -v -f .manta_coverage.json'
	}

	publishers {
		reportQuality('commerce/payday', '.manta/coverage', '*.xml')
	}
}