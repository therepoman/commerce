package volds

import (
	"context"

	"code.justin.tv/commerce/payday/prism_tenant/tokenize_and_validate_message"
	prism_tenant "code.justin.tv/commerce/prism/rpc/tenant"
)

func NewTokenizeAndValidate() tokenize_and_validate_message.TokenizeAndValidate {
	return &tokenizeAndValidateImpl{}
}

type tokenizeAndValidateImpl struct {
}

func (t *tokenizeAndValidateImpl) TokenizeAndValidate(ctx context.Context, req *prism_tenant.TokenizeAndValidateMessageReq) (prism_tenant.TokenizeAndValidateMessageResp, error) {
	return prism_tenant.TokenizeAndValidateMessageResp{}, nil
}
