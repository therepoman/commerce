package tokenize_and_validate_message

import (
	"context"

	prism_tenant "code.justin.tv/commerce/prism/rpc/tenant"
)

type TokenizeAndValidate interface {
	TokenizeAndValidate(ctx context.Context, req *prism_tenant.TokenizeAndValidateMessageReq) (prism_tenant.TokenizeAndValidateMessageResp, error)
}
