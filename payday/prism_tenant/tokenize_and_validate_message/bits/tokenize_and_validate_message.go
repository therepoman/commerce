package bits

import (
	"context"
	"errors"
	"regexp"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/api/constants"
	"code.justin.tv/commerce/payday/cache/channel"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/metrics"
	"code.justin.tv/commerce/payday/middleware"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/models/api"
	"code.justin.tv/commerce/payday/prism_tenant/tokenize_and_validate_message"
	"code.justin.tv/commerce/payday/prism_tenant/tokenize_and_validate_message/bits/datascience"
	"code.justin.tv/commerce/payday/prism_tenant/tokenize_and_validate_message/bits/validate"
	"code.justin.tv/commerce/payday/prism_tenant/tokenize_and_validate_message/bits/validate/balance"
	"code.justin.tv/commerce/payday/prism_tenant/tokenize_and_validate_message/bits/validate/channel_eligibility"
	"code.justin.tv/commerce/payday/prism_tenant/tokenize_and_validate_message/bits/validate/channel_min_bits"
	"code.justin.tv/commerce/payday/prism_tenant/tokenize_and_validate_message/bits/validate/channel_min_bits_emote"
	"code.justin.tv/commerce/payday/prism_tenant/tokenize_and_validate_message/bits/validate/token"
	"code.justin.tv/commerce/payday/prism_tenant/tokenize_and_validate_message/bits/validate/user_eligibility"
	prism "code.justin.tv/commerce/prism/rpc"
	prism_models "code.justin.tv/commerce/prism/rpc/models"
	prism_tenant "code.justin.tv/commerce/prism/rpc/tenant"
	"github.com/twitchtv/twirp"
)

// Valid transaction ids can only contain alphanumeric characters and dashes
var validTransactionIDRegex = regexp.MustCompile(`^[a-zA-Z\d-]+$`)

type validatorChannels struct {
	TokenChannel              chan validate.TokenResponse
	UserEligibilityChannel    chan validate.UserEligibilityResponse
	ChannelEligibilityChannel chan validate.ChannelEligibilityResponse
}

func NewTokenizeAndValidate() tokenize_and_validate_message.TokenizeAndValidate {
	return &tokenizeAndValidateImpl{}
}

type tokenizeAndValidateImpl struct {
	ChannelManager               channel.ChannelManager           `inject:""`
	ChannelEligibilityValidator  channel_eligibility.Validator    `inject:""`
	ChannelMinBitsValidator      channel_min_bits.Validator       `inject:""`
	ChannelMinBitsEmoteValidator channel_min_bits_emote.Validator `inject:""`
	TokenValidator               token.Validator                  `inject:""`
	UserEligibilityValidator     user_eligibility.Validator       `inject:""`
	BalanceValidator             balance.Validator                `inject:""`
	DataScienceTracker           datascience.DataScienceTracker   `inject:""`
	TransactionsDao              dynamo.ITransactionsDao          `inject:""`
	Statter                      metrics.Statter                  `inject:""`
}

func (s *tokenizeAndValidateImpl) TokenizeAndValidate(ctx context.Context, req *prism_tenant.TokenizeAndValidateMessageReq) (prism_tenant.TokenizeAndValidateMessageResp, error) {
	tokenResponseChan := make(chan validate.TokenResponse, 1)
	userEligibilityResponseChan := make(chan validate.UserEligibilityResponse, 1)
	channelEligibilityResponseChan := make(chan validate.ChannelEligibilityResponse, 1)

	return s.tokenizeAndValidate(ctx, req, validatorChannels{
		TokenChannel:              tokenResponseChan,
		UserEligibilityChannel:    userEligibilityResponseChan,
		ChannelEligibilityChannel: channelEligibilityResponseChan,
	})
}

func (t *tokenizeAndValidateImpl) tokenizeAndValidate(ctx context.Context, req *prism_tenant.TokenizeAndValidateMessageReq, validatorChannels validatorChannels) (prism_tenant.TokenizeAndValidateMessageResp, error) {
	var errorType models.ErrorType
	errCtx, errCancel := middleware.CopyContextValues(ctx)
	defer func() {
		go func() {
			defer errCancel()
			if errorType != models.Default {
				t.DataScienceTracker.TrackBitsUsedErrorEvent(errCtx, req, errorType)
			}
		}()
	}()

	// Before proceeding, check that the transaction ID does not already exist
	logFields := log.Fields{
		"ChannelID": req.ChannelId,
		"UserID":    req.UserId,
		"EventID":   req.MessageId,
		"Message":   req.Message,
	}

	dynamoLookupTimeStart := time.Now()
	tx, err := t.TransactionsDao.Get(req.MessageId)
	go t.Statter.TimingDuration("GiveBitsStep_DynamoTransactionLookup", time.Since(dynamoLookupTimeStart))

	if err != nil {
		msg := "Error getting transaction from dynamo"
		log.WithError(err).WithFields(logFields).Error(msg)
		return prism_tenant.TokenizeAndValidateMessageResp{}, twirp.InternalError(msg)
	}
	if tx != nil {
		log.WithFields(logFields).Warn("Received request for a duplicate EventID")
		return prism_tenant.TokenizeAndValidateMessageResp{}, twirp.NewError(twirp.InvalidArgument, "Request contained an existing EventID")
	}

	if req.UserId == req.ChannelId {
		return prism_tenant.TokenizeAndValidateMessageResp{}, twirp.NewError(twirp.InvalidArgument, "User cannot use bits in their own channel")
	}

	if !validTransactionIDRegex.MatchString(req.MessageId) {
		return prism_tenant.TokenizeAndValidateMessageResp{}, twirp.NewError(twirp.InvalidArgument, "MessageID is invalid")
	}

	bitsAmount := req.ConsumableAmounts[prism_models.ConsumableType_Bits]

	var totalBits int32
	var allTokenValues []int64
	var tokens []*prism.Token

	go t.TokenValidator.Validate(context.Background(), req, validatorChannels.TokenChannel)

	go t.UserEligibilityValidator.Validate(context.Background(), req.UserId, validatorChannels.UserEligibilityChannel)

	ch, err := t.ChannelManager.Get(ctx, req.ChannelId)
	if err != nil {
		log.WithField("channelID", req.ChannelId).Error("Error getting channel")
		return prism_tenant.TokenizeAndValidateMessageResp{}, twirp.InternalErrorWith(err)
	} else if ch == nil {
		errMsg := "Requested channel is invalid"
		log.WithField("channelID", req.ChannelId).Error(errMsg)
		return prism_tenant.TokenizeAndValidateMessageResp{}, prism_models.NewClientError(
			twirp.NewError(twirp.InvalidArgument, constants.ChannelIneligibleError),
			api.CreateEventErrorResponse{
				Status:  api.ChannelIneligible,
				Message: constants.ChannelIneligibleError,
			})
	}

	validAmount, minBitsErrType, err := t.ChannelMinBitsValidator.Validate(ch, bitsAmount)
	if !validAmount {
		errorType = minBitsErrType
		log.WithFields(log.Fields{
			"userId":    req.UserId,
			"channelId": req.ChannelId,
			"message":   req.Message,
			"amount":    bitsAmount,
		}).Warn("Invalid amount attempted to be cheered")
		if bitsAmount == 0 {
			t.DataScienceTracker.TrackBitsCheerZeroEvent(ctx, req)
		}
		return prism_tenant.TokenizeAndValidateMessageResp{}, validationError(err)
	}

	go t.ChannelEligibilityValidator.Validate(context.Background(), ch, validatorChannels.ChannelEligibilityChannel)

	tokenizedFinished, userEligibilityFinished, channelEligibilityFinished := false, false, false
	var invalidAnonCheerErr twirp.Error

	for {
		select {
		case tokenizedResponse := <-validatorChannels.TokenChannel:
			if tokenizedResponse.Error != nil {
				if tokenizedResponse.AnonymousMessageError != nil {
					// Don't return if this was an anon cheering error, other errors take precedence
					invalidAnonCheerErr = tokenizedResponse.AnonymousMessageError
				} else {
					return prism_tenant.TokenizeAndValidateMessageResp{}, tokenizedResponse.Error
				}
			}
			tokenizedFinished = true
			totalBits = int32(tokenizedResponse.TotalBits)
			allTokenValues = tokenizedResponse.AllTokenValues
			tokens = tokenizedResponse.Tokens
		case userEligibilityResponse := <-validatorChannels.UserEligibilityChannel:
			if userEligibilityResponse.Error != nil {
				errorType = userEligibilityResponse.ErrorType
				return prism_tenant.TokenizeAndValidateMessageResp{}, validationError(userEligibilityResponse.Error)
			}
			userEligibilityFinished = true
		case channelEligibilityResponse := <-validatorChannels.ChannelEligibilityChannel:
			if channelEligibilityResponse.Error != nil {
				errorType = channelEligibilityResponse.ErrorType
				return prism_tenant.TokenizeAndValidateMessageResp{}, validationError(channelEligibilityResponse.Error)
			}
			channelEligibilityFinished = true
		case <-ctx.Done():
			err = errors.New("timed out while validating Give Bits preconditions")
			log.WithError(err).WithFields(log.Fields{
				"tokenizedFinished":          tokenizedFinished,
				"userEligibilityFinished":    userEligibilityFinished,
				"channelEligibilityFinished": channelEligibilityFinished,
			})
			return prism_tenant.TokenizeAndValidateMessageResp{}, validationError(err)
		}

		if tokenizedFinished && userEligibilityFinished && channelEligibilityFinished {
			validAmount, minBitsEmoteErrType, err := t.ChannelMinBitsEmoteValidator.Validate(allTokenValues, ch)
			if !validAmount {
				errorType = minBitsEmoteErrType
				return prism_tenant.TokenizeAndValidateMessageResp{}, validationError(err)
			}
			break
		}
	}

	// Only return the invalid anon cheer error after all other checks have finished. This is to ensure that when the
	// user decides to "send anyway" after their anon message was rejected that they've resolved all other issues
	// with their message first.
	if invalidAnonCheerErr != nil {
		return prism_tenant.TokenizeAndValidateMessageResp{}, invalidAnonCheerErr
	}

	return prism_tenant.TokenizeAndValidateMessageResp{
		ConsumableAmounts: map[string]int64{
			prism_models.ConsumableType_Bits: int64(totalBits),
		},
		Tokens: tokens,
	}, nil
}

func validationError(err error) error {
	twErr, ok := err.(twirp.Error)
	if !ok {
		// If the validation error is not a twirp error, make sure to return a twirp error
		return twirp.InternalErrorWith(err)
	}
	return twErr
}
