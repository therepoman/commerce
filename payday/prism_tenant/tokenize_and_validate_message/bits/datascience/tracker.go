package datascience

import (
	"context"
	"strconv"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/balance"
	"code.justin.tv/commerce/payday/cache/channel"
	"code.justin.tv/commerce/payday/datascience"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/partner"
	"code.justin.tv/commerce/payday/utils/pointers"
	prism_models "code.justin.tv/commerce/prism/rpc/models"
	prism_tenant "code.justin.tv/commerce/prism/rpc/tenant"
)

type DataScienceTracker interface {
	TrackBitsUsedErrorEvent(ctx context.Context, req *prism_tenant.TokenizeAndValidateMessageReq, errorType models.ErrorType)
	TrackBitsCheerZeroEvent(ctx context.Context, req *prism_tenant.TokenizeAndValidateMessageReq)
}

func NewDataScienceTracker() DataScienceTracker {
	return &dataScienceTracker{}
}

type dataScienceTracker struct {
	ChannelManager    channel.ChannelManager  `inject:""`
	PartnerManager    partner.IPartnerManager `inject:""`
	DataScienceClient datascience.DataScience `inject:""`
	BalanceGetter     balance.Getter          `inject:""`
}

func (d *dataScienceTracker) TrackBitsCheerZeroEvent(ctx context.Context, req *prism_tenant.TokenizeAndValidateMessageReq) {
	err := d.DataScienceClient.TrackBitsEvent(ctx, datascience.BitsCheerZeroEventName, &models.BitsCheerZeroEvent{
		UserId:        req.UserId,
		ChannelId:     req.ChannelId,
		ServerTime:    time.Now(),
		Message:       req.Message,
		TransactionId: req.MessageId,
	})

	if err != nil {
		log.WithError(err).Error("Failed to track cheer zero event")
	}
}

func (d *dataScienceTracker) TrackBitsUsedErrorEvent(ctx context.Context, req *prism_tenant.TokenizeAndValidateMessageReq, errorType models.ErrorType) {
	bitsUsedError := &models.BitsUsedError{
		UserID:            req.UserId,
		ChannelID:         req.ChannelId,
		QuantityAttempted: int32(req.ConsumableAmounts[prism_models.ConsumableType_Bits]),
	}

	switch errorType {
	default:
		bitsUsedError.ErrorType = models.BitsUnavailableError
	case models.InsufficientFunds:
		response, err := d.BalanceGetter.GetBalance(ctx, req.UserId, true)
		if err != nil {
			bitsUsedError.ErrorType = models.BitsUnavailableError
		} else {
			bitsUsedError.CurrentBalance = strconv.Itoa(int(response))
			bitsUsedError.ErrorType = models.InsufficientFundsError
		}
	case models.EmoteBelowMinBitsEmote:
		bitsUsedError.ErrorType = models.AmountBelowMinBitsEmoteError
	case models.InvalidCheer:
		bitsUsedError.ErrorType = models.InvalidCheerError
	case models.UserIneligible:
		bitsUsedError.ErrorType = models.UserBannedError
	case models.AmountBelowMinBits:
		bitsUsedError.ErrorType = models.AmountBelowMinBitsError
	case models.BitsIneligibleChannelMismatch:
		bitsUsedError.ErrorType = models.BitsIneligibleChannelMismatchError
	case models.ChannelIneligible:
		partnerType, err := d.PartnerManager.GetPartnerType(ctx, req.ChannelId)
		if err != nil {
			log.WithField("channel_id", req.ChannelId).WithError(err).
				Info("Could not get partner type")
			return
		}

		if !partner.IsEligibleForBits(partnerType) {
			bitsUsedError.ErrorType = models.NonPartnerError
		} else {
			channel, err := d.ChannelManager.Get(ctx, req.ChannelId)
			if err != nil {
				log.WithError(err).WithField("channelId", req.ChannelId).Error("Error getting channel")
				return
			}

			if channel == nil {
				log.WithField("channelId", req.ChannelId).Error("Channel not found")
				return
			}

			if !pointers.BoolOrDefault(channel.Onboarded, false) {
				bitsUsedError.ErrorType = models.NotOnboardedError
			} else if pointers.BoolOrDefault(channel.OptedOut, false) {
				bitsUsedError.ErrorType = models.OptOutError
			}
		}
	}

	bitsUsedError.Context = models.CheerContext
	bitsUsedError.ContextDetails = req.Message

	err := d.DataScienceClient.TrackBitsEvent(ctx, datascience.BitsUsedErrorEventName, bitsUsedError)
	if err != nil {
		log.WithError(err).WithField("BitsUsedErrorEvent", datascience.BitsUsedErrorEventName).Error("Could not send track bits event")
	}
}
