package bits

import (
	"context"
	"errors"
	"fmt"
	"testing"
	"time"

	"code.justin.tv/commerce/payday/dynamo"
	channel_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/cache/channel"
	dynamo_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo"
	metrics_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/metrics"
	datascience_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/prism_tenant/tokenize_and_validate_message/bits/datascience"
	channel_eligibility_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/prism_tenant/tokenize_and_validate_message/bits/validate/channel_eligibility"
	channel_min_bits_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/prism_tenant/tokenize_and_validate_message/bits/validate/channel_min_bits"
	channel_min_bits_emote_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/prism_tenant/tokenize_and_validate_message/bits/validate/channel_min_bits_emote"
	token_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/prism_tenant/tokenize_and_validate_message/bits/validate/token"
	user_eligibility_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/prism_tenant/tokenize_and_validate_message/bits/validate/user_eligibility"
	"code.justin.tv/commerce/payday/models"
	apimodels "code.justin.tv/commerce/payday/models/api"
	"code.justin.tv/commerce/payday/prism_tenant/tokenize_and_validate_message/bits/validate"
	"code.justin.tv/commerce/payday/utils/pointers"
	prism "code.justin.tv/commerce/prism/rpc"
	prism_models "code.justin.tv/commerce/prism/rpc/models"
	prism_tenant "code.justin.tv/commerce/prism/rpc/tenant"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
	"github.com/twitchtv/twirp"
)

func TestTokenizeAndValidateMessage(t *testing.T) {
	Convey("Given a TokenizeAndValidateMessage server", t, func() {
		channelID := "108706201"
		userID := "108706200"
		amount := 420

		channelManager := new(channel_mock.ChannelManager)
		dataScienceTracker := new(datascience_mock.DataScienceTracker)
		tokenValidator := new(token_mock.Validator)
		userEligibilityValidator := new(user_eligibility_mock.Validator)
		channelEligibilityValidator := new(channel_eligibility_mock.Validator)
		channelMinBitsValidator := new(channel_min_bits_mock.Validator)
		channelMinBitsEmoteValidator := new(channel_min_bits_emote_mock.Validator)
		transactionsDao := new(dynamo_mock.ITransactionsDao)
		statter := new(metrics_mock.Statter)

		api := &tokenizeAndValidateImpl{
			ChannelManager:               channelManager,
			DataScienceTracker:           dataScienceTracker,
			TokenValidator:               tokenValidator,
			UserEligibilityValidator:     userEligibilityValidator,
			ChannelEligibilityValidator:  channelEligibilityValidator,
			ChannelMinBitsValidator:      channelMinBitsValidator,
			ChannelMinBitsEmoteValidator: channelMinBitsEmoteValidator,
			TransactionsDao:              transactionsDao,
			Statter:                      statter,
		}

		channel := &dynamo.Channel{
			Id:          dynamo.ChannelId(channelID),
			Onboarded:   pointers.BoolP(true),
			LastUpdated: time.Now(),
		}

		req := &prism_tenant.TokenizeAndValidateMessageReq{
			UserId:    userID,
			ChannelId: channelID,
			ConsumableAmounts: map[string]int64{
				prism_models.ConsumableType_Bits: int64(amount),
			},
			Message:   fmt.Sprintf("cheer%d", amount),
			MessageId: "test-MESSAGE-123",
			Anonymous: false,
		}

		dataScienceTracker.On("TrackBitsUsedErrorEvent", mock.Anything, mock.Anything, mock.Anything).Return()
		dataScienceTracker.On("TrackBitsCheerZeroEvent", mock.Anything, mock.Anything).Return()
		tokenValidator.On("Validate", mock.Anything, mock.Anything, mock.Anything).Return()
		userEligibilityValidator.On("Validate", mock.Anything, mock.Anything, mock.Anything).Return()
		channelEligibilityValidator.On("Validate", mock.Anything, mock.Anything, mock.Anything).Return()
		statter.On("TimingDuration", mock.Anything, mock.Anything).Return()

		Convey("When the transactions DAO fails", func() {
			transactionsDao.On("Get", mock.Anything).Return(nil, errors.New("test error"))

			Convey("Then an error is returned", func() {
				res, err := api.TokenizeAndValidate(context.Background(), req)

				So(res, ShouldResemble, prism_tenant.TokenizeAndValidateMessageResp{})
				So(err, ShouldNotBeNil)
			})
		})

		Convey("When the transactions DAO succeeds", func() {
			Convey("When an existing transaction is found", func() {
				transactionsDao.On("Get", mock.Anything).Return(&dynamo.Transaction{}, nil)

				Convey("Then an error is returned", func() {
					res, err := api.TokenizeAndValidate(context.Background(), req)

					So(res, ShouldResemble, prism_tenant.TokenizeAndValidateMessageResp{})
					So(err, ShouldNotBeNil)
				})
			})

			Convey("When an existing transaction is NOT found", func() {
				transactionsDao.On("Get", mock.Anything).Return(nil, nil)

				Convey("Errors when the user is the same as the channel", func() {
					ctx := context.Background()
					ctx, cancel := context.WithTimeout(ctx, 1*time.Second)
					defer cancel()

					req.UserId = "same_user"
					req.ChannelId = "same_user"
					res, err := api.TokenizeAndValidate(ctx, req)
					So(res, ShouldResemble, prism_tenant.TokenizeAndValidateMessageResp{})
					So(err, ShouldNotBeNil)
				})

				Convey("Errors when the message id contains space", func() {
					ctx := context.Background()
					ctx, cancel := context.WithTimeout(ctx, 1*time.Second)
					defer cancel()

					req.MessageId = "this-id-has-a-trailing-space-character "
					res, err := api.TokenizeAndValidate(ctx, req)
					So(res, ShouldResemble, prism_tenant.TokenizeAndValidateMessageResp{})
					So(err, ShouldNotBeNil)
				})

				Convey("Errors when the message id contains colon", func() {
					ctx := context.Background()
					ctx, cancel := context.WithTimeout(ctx, 1*time.Second)
					defer cancel()

					req.MessageId = "this-id-has-a:colon-character"
					res, err := api.TokenizeAndValidate(ctx, req)
					So(res, ShouldResemble, prism_tenant.TokenizeAndValidateMessageResp{})
					So(err, ShouldNotBeNil)
				})

				Convey("When the channel manager fails", func() {
					Convey("When the channel manager errors out", func() {
						channelManager.On("Get", mock.Anything, req.ChannelId).Return(nil, errors.New("test error"))

						Convey("Then an error is returned", func() {
							ctx := context.Background()
							ctx, cancel := context.WithTimeout(ctx, 1*time.Second)
							defer cancel()

							res, err := api.TokenizeAndValidate(ctx, req)

							So(res, ShouldResemble, prism_tenant.TokenizeAndValidateMessageResp{})
							So(err, ShouldNotBeNil)
						})
					})

					Convey("When a nil channel is returned", func() {
						channelManager.On("Get", mock.Anything, req.ChannelId).Return(nil, nil)

						Convey("Then an error is returned", func() {
							ctx := context.Background()
							ctx, cancel := context.WithTimeout(ctx, 1*time.Second)
							defer cancel()

							res, err := api.TokenizeAndValidate(ctx, req)

							So(res, ShouldResemble, prism_tenant.TokenizeAndValidateMessageResp{})
							So(err, ShouldNotBeNil)

							twirpErr, ok := err.(twirp.Error)
							So(ok, ShouldBeTrue)

							clientErr := apimodels.CreateEventErrorResponse{}
							parseErr := prism_models.ParseClientError(twirpErr, &clientErr)
							So(parseErr, ShouldBeNil)
							So(clientErr.Status, ShouldEqual, string(apimodels.ChannelIneligible))
						})
					})
				})

				Convey("When the channel manager succeeds", func() {
					channelManager.On("Get", mock.Anything, mock.Anything).Return(channel, nil)

					Convey("When the channel min bits validator fails", func() {
						channelMinBitsValidator.On("Validate", mock.Anything, mock.Anything).Return(false, models.Default, errors.New("test error"))

						Convey("Then an error is returned", func() {
							ctx := context.Background()
							ctx, cancel := context.WithTimeout(ctx, 1*time.Second)
							defer cancel()

							res, err := api.TokenizeAndValidate(ctx, req)

							So(res, ShouldResemble, prism_tenant.TokenizeAndValidateMessageResp{})
							So(err, ShouldNotBeNil)
						})
					})

					Convey("When the channel min bits validator succeeds", func() {
						channelMinBitsValidator.On("Validate", mock.Anything, mock.Anything).Return(true, models.Default, nil)

						Convey("When the token validator fails", func() {
							tokenResponse := validate.TokenResponse{
								Error: twirp.NewError(twirp.Internal, "test error"),
							}

							Convey("Then we return an error", func() {
								tokenResponseChan := make(chan validate.TokenResponse, 1)
								userEligibilityResponseChan := make(chan validate.UserEligibilityResponse, 1)
								channelEligibilityResponseChan := make(chan validate.ChannelEligibilityResponse, 1)

								validatorChannels := validatorChannels{
									TokenChannel:              tokenResponseChan,
									UserEligibilityChannel:    userEligibilityResponseChan,
									ChannelEligibilityChannel: channelEligibilityResponseChan,
								}

								ctx := context.Background()
								ctx, cancel := context.WithTimeout(ctx, 1*time.Second)
								defer cancel()

								validatorChannels.TokenChannel <- tokenResponse

								res, err := api.tokenizeAndValidate(ctx, req, validatorChannels)

								So(res, ShouldResemble, prism_tenant.TokenizeAndValidateMessageResp{})
								So(err, ShouldNotBeNil)
							})
						})

						Convey("When the token validator succeeds", func() {
							tokenResponse := validate.TokenResponse{
								TotalBits:      int64(amount),
								AllTokenValues: []int64{int64(amount)},
								Tokens:         []*prism.Token{{Text: fmt.Sprintf("cheer%d", amount), Amount: int64(amount)}},
							}

							Convey("When the user eligibility validator fails", func() {
								userEligibilityResponse := validate.UserEligibilityResponse{
									Error: errors.New("test error"),
								}

								Convey("Then we return an error", func() {
									tokenResponseChan := make(chan validate.TokenResponse, 1)
									userEligibilityResponseChan := make(chan validate.UserEligibilityResponse, 1)
									channelEligibilityResponseChan := make(chan validate.ChannelEligibilityResponse, 1)

									validatorChannels := validatorChannels{
										TokenChannel:              tokenResponseChan,
										UserEligibilityChannel:    userEligibilityResponseChan,
										ChannelEligibilityChannel: channelEligibilityResponseChan,
									}

									ctx := context.Background()
									ctx, cancel := context.WithTimeout(ctx, 1*time.Second)
									defer cancel()

									validatorChannels.TokenChannel <- tokenResponse
									validatorChannels.UserEligibilityChannel <- userEligibilityResponse

									res, err := api.tokenizeAndValidate(ctx, req, validatorChannels)

									So(res, ShouldResemble, prism_tenant.TokenizeAndValidateMessageResp{})
									So(err, ShouldNotBeNil)
								})
							})

							Convey("When the user eligibility validator succeeds", func() {
								userEligibilityResponse := validate.UserEligibilityResponse{}

								Convey("When the channel eligibility validator fails", func() {
									channelEligibilityResponse := validate.ChannelEligibilityResponse{
										Error: errors.New("test error"),
									}

									Convey("Then we return an error", func() {
										tokenResponseChan := make(chan validate.TokenResponse, 1)
										userEligibilityResponseChan := make(chan validate.UserEligibilityResponse, 1)
										channelEligibilityResponseChan := make(chan validate.ChannelEligibilityResponse, 1)

										validatorChannels := validatorChannels{
											TokenChannel:              tokenResponseChan,
											UserEligibilityChannel:    userEligibilityResponseChan,
											ChannelEligibilityChannel: channelEligibilityResponseChan,
										}

										ctx := context.Background()
										ctx, cancel := context.WithTimeout(ctx, 1*time.Second)
										defer cancel()

										validatorChannels.TokenChannel <- tokenResponse
										validatorChannels.UserEligibilityChannel <- userEligibilityResponse
										validatorChannels.ChannelEligibilityChannel <- channelEligibilityResponse

										res, err := api.tokenizeAndValidate(ctx, req, validatorChannels)

										So(res, ShouldResemble, prism_tenant.TokenizeAndValidateMessageResp{})
										So(err, ShouldNotBeNil)
									})
								})

								Convey("When the channel eligibility validator succeeds", func() {
									channelEligibilityResponse := validate.ChannelEligibilityResponse{}

									Convey("When the channel min bits emote validator fails", func() {
										channelMinBitsEmoteValidator.On("Validate", mock.Anything, mock.Anything).Return(false, models.Default, errors.New("test error"))

										Convey("Then we return an error", func() {
											tokenResponseChan := make(chan validate.TokenResponse, 1)
											userEligibilityResponseChan := make(chan validate.UserEligibilityResponse, 1)
											channelEligibilityResponseChan := make(chan validate.ChannelEligibilityResponse, 1)

											validatorChannels := validatorChannels{
												TokenChannel:              tokenResponseChan,
												UserEligibilityChannel:    userEligibilityResponseChan,
												ChannelEligibilityChannel: channelEligibilityResponseChan,
											}

											ctx := context.Background()
											ctx, cancel := context.WithTimeout(ctx, 1*time.Second)
											defer cancel()

											validatorChannels.TokenChannel <- tokenResponse
											validatorChannels.UserEligibilityChannel <- userEligibilityResponse
											validatorChannels.ChannelEligibilityChannel <- channelEligibilityResponse

											res, err := api.tokenizeAndValidate(ctx, req, validatorChannels)

											So(res, ShouldResemble, prism_tenant.TokenizeAndValidateMessageResp{})
											So(err, ShouldNotBeNil)
										})
									})

									Convey("When the channel min bits emote validator succeeds", func() {
										channelMinBitsEmoteValidator.On("Validate", mock.Anything, mock.Anything).Return(true, models.Default, nil)

										Convey("Then we return a tokenized response", func() {
											tokenResponseChan := make(chan validate.TokenResponse, 1)
											userEligibilityResponseChan := make(chan validate.UserEligibilityResponse, 1)
											channelEligibilityResponseChan := make(chan validate.ChannelEligibilityResponse, 1)

											validatorChannels := validatorChannels{
												TokenChannel:              tokenResponseChan,
												UserEligibilityChannel:    userEligibilityResponseChan,
												ChannelEligibilityChannel: channelEligibilityResponseChan,
											}

											ctx := context.Background()
											ctx, cancel := context.WithTimeout(ctx, 1*time.Second)
											defer cancel()

											validatorChannels.TokenChannel <- tokenResponse
											validatorChannels.UserEligibilityChannel <- userEligibilityResponse
											validatorChannels.ChannelEligibilityChannel <- channelEligibilityResponse

											res, err := api.tokenizeAndValidate(ctx, req, validatorChannels)

											So(res, ShouldResemble, prism_tenant.TokenizeAndValidateMessageResp{
												ConsumableAmounts: map[string]int64{
													prism_models.ConsumableType_Bits: int64(amount),
												},
												Tokens: tokenResponse.Tokens,
											})
											So(err, ShouldBeNil)
										})
									})
								})
							})
						})

						Convey("When the context times out", func() {
							ctx := context.Background()
							ctx, cancel := context.WithTimeout(ctx, 1*time.Second)
							defer cancel()

							Convey("Then we return an error", func() {
								res, err := api.TokenizeAndValidate(ctx, req)

								So(res, ShouldResemble, prism_tenant.TokenizeAndValidateMessageResp{})
								So(err, ShouldNotBeNil)
							})
						})
					})
				})
			})
		})
	})
}
