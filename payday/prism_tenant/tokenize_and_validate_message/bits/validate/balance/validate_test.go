package balance

import (
	"context"
	"errors"
	"testing"
	"time"

	balance_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/balance"
	metrics_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/metrics"
	"code.justin.tv/commerce/payday/prism_tenant/tokenize_and_validate_message/bits/validate"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestValidator_Validate(t *testing.T) {
	Convey("Given a user eligibility validator", t, func() {
		userID := "108706200"
		channelID := "108706201"
		bitsAmount := int32(420)

		getter := new(balance_mock.Getter)
		statter := new(metrics_mock.Statter)

		balanceValidator := &validator{
			BalanceGetter: getter,
			Statter:       statter,
		}

		statter.On("TimingDuration", mock.Anything, mock.Anything).Return()

		Convey("When balance manager fails", func() {
			getter.On("GetBalance", mock.Anything, mock.Anything, false).Return(int32(0), nil, errors.New("test error"))

			Convey("Then an error is returned", func() {
				ctx := context.Background()
				ctx, cancel := context.WithTimeout(ctx, 1*time.Second)
				defer cancel()

				respChan := make(chan validate.BalanceResponse, 1)

				go balanceValidator.Validate(context.Background(), userID, channelID, bitsAmount, respChan)

				for {
					select {
					case resp := <-respChan:
						So(resp.Error, ShouldNotBeNil)
						return
					case <-ctx.Done():
						panic("test doesn't work")
					}
				}
			})
		})

		Convey("When balance manager succeeds", func() {
			Convey("When the user has insufficient funds", func() {
				getter.On("GetBalance", mock.Anything, mock.Anything, false).Return(int32(bitsAmount-1), nil, nil)

				Convey("Then an error is returned", func() {
					ctx := context.Background()
					ctx, cancel := context.WithTimeout(ctx, 1*time.Second)
					defer cancel()

					respChan := make(chan validate.BalanceResponse, 1)

					go balanceValidator.Validate(context.Background(), userID, channelID, bitsAmount, respChan)

					for {
						select {
						case resp := <-respChan:
							So(resp.Error, ShouldNotBeNil)
							return
						case <-ctx.Done():
							panic("test doesn't work")
						}
					}
				})
			})

			Convey("When the user has sufficient funds", func() {
				getter.On("GetBalance", mock.Anything, mock.Anything, false).Return(int32(bitsAmount+1), nil, nil)

				Convey("Then a successful balance validation response is returned", func() {
					ctx := context.Background()
					ctx, cancel := context.WithTimeout(ctx, 1*time.Second)
					defer cancel()

					respChan := make(chan validate.BalanceResponse, 1)

					go balanceValidator.Validate(context.Background(), userID, channelID, bitsAmount, respChan)

					for {
						select {
						case resp := <-respChan:
							So(resp.Error, ShouldBeNil)
							return
						case <-ctx.Done():
							panic("test doesn't work")
						}
					}
				})
			})
		})
	})
}
