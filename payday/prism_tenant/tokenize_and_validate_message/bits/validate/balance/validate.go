package balance

import (
	"context"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/api/constants"
	"code.justin.tv/commerce/payday/balance"
	"code.justin.tv/commerce/payday/metrics"
	"code.justin.tv/commerce/payday/models"
	apimodel "code.justin.tv/commerce/payday/models/api"
	"code.justin.tv/commerce/payday/prism_tenant/tokenize_and_validate_message/bits/validate"
	prism_models "code.justin.tv/commerce/prism/rpc/models"
	"github.com/twitchtv/twirp"
)

type Validator interface {
	Validate(ctx context.Context, userID string, channelID string, bitsAmount int32, balanceChan chan validate.BalanceResponse)
}

func NewValidator() Validator {
	return &validator{}
}

type validator struct {
	BalanceGetter balance.Getter  `inject:""`
	Statter       metrics.Statter `inject:""`
}

func (v *validator) Validate(ctx context.Context, userID string, channelID string, bitsAmount int32, balanceChan chan validate.BalanceResponse) {
	startTime := time.Now()
	defer v.logDurationMetric("GiveBitsStep_RequestValidation_BalanceValidation", startTime)

	userBalance, err := v.BalanceGetter.GetBalance(ctx, userID, false)
	if err != nil {
		log.WithError(err).Error("Error getting bits balance from Pachinko")
		balanceChan <- validate.BalanceResponse{
			Error:     err,
			ErrorType: models.BitsUnavailable,
		}
		return
	}

	if userBalance < bitsAmount {
		balanceChan <- validate.BalanceResponse{
			Error: prism_models.NewClientError(
				twirp.NewError(twirp.InvalidArgument, constants.InsufficientBalanceError),
				apimodel.CreateEventErrorResponse{
					Status:  apimodel.InsufficientBalance,
					Message: constants.InsufficientBalanceError,
				}),
			ErrorType: models.InsufficientFunds,
		}
		return
	}

	balanceChan <- validate.BalanceResponse{}
}

func (v *validator) logDurationMetric(name string, startTime time.Time) {
	go v.Statter.TimingDuration(name, time.Since(startTime))
}
