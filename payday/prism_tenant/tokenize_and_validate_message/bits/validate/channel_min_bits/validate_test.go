package channel_min_bits

import (
	"testing"
	"time"

	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/utils/pointers"
	. "github.com/smartystreets/goconvey/convey"
)

func TestValidator_Validate(t *testing.T) {
	Convey("Given a channel min bits validator", t, func() {
		channelID := "108706201"
		minBits := int64(50)

		channelMinBitsValidator := &validator{}

		channel := &dynamo.Channel{
			Id:          dynamo.ChannelId(channelID),
			Onboarded:   pointers.BoolP(true),
			OptedOut:    pointers.BoolP(false),
			LastUpdated: time.Now(),
			MinBits:     &minBits,
		}

		Convey("When number of bits used is invalid", func() {
			bitsUsed := int64(5)

			Convey("Then an error is returned", func() {
				valid, _, err := channelMinBitsValidator.Validate(channel, bitsUsed)

				So(err, ShouldNotBeNil)
				So(valid, ShouldBeFalse)
			})
		})

		Convey("When number of bits used is valid", func() {
			bitsUsed := int64(420)

			Convey("Then a successful response is returned", func() {
				valid, _, err := channelMinBitsValidator.Validate(channel, bitsUsed)

				So(err, ShouldBeNil)
				So(valid, ShouldBeTrue)
			})
		})
	})
}
