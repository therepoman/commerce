package channel_min_bits

import (
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/api/constants"
	"code.justin.tv/commerce/payday/cache/channel"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/models"
	apimodel "code.justin.tv/commerce/payday/models/api"
	prism_models "code.justin.tv/commerce/prism/rpc/models"
	"github.com/twitchtv/twirp"
)

type Validator interface {
	Validate(ch *dynamo.Channel, bitsUsed int64) (bool, models.ErrorType, error)
}

func NewValidator() Validator {
	return &validator{}
}

type validator struct{}

func (v *validator) Validate(ch *dynamo.Channel, bitsUsed int64) (bool, models.ErrorType, error) {
	minBits := channel.GetChannelMinBits(ch)
	validBitsAmountToUse := bitsUsed >= minBits

	if !validBitsAmountToUse {
		var channelID string
		if ch != nil {
			channelID = string(ch.Id)
		}
		log.WithFields(log.Fields{
			"channelId": channelID,
			"bitsUsed":  bitsUsed,
			"minBits":   minBits,
		}).Infof("Attempt to use bits amount below channel minimum")
		err := prism_models.NewClientError(
			twirp.NewError(twirp.InvalidArgument, constants.AmountBelowMinBitsError),
			apimodel.CreateEventErrorResponse{
				Status:  apimodel.AmountBelowMinBits,
				Message: constants.AmountBelowMinBitsError,
			})
		return validBitsAmountToUse, models.AmountBelowMinBits, err
	}

	return validBitsAmountToUse, models.Default, nil
}
