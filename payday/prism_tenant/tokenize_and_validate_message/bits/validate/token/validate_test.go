package token

import (
	"context"
	"errors"
	"fmt"
	"testing"
	"time"

	"code.justin.tv/commerce/payday/message/tokenizer"
	message_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/message"
	bits_amount_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/prism_tenant/tokenize_and_validate_message/bits/validate/bits_amount"
	apimodel "code.justin.tv/commerce/payday/models/api"
	"code.justin.tv/commerce/payday/prism_tenant/tokenize_and_validate_message/bits/validate"
	prism "code.justin.tv/commerce/prism/rpc"
	prism_models "code.justin.tv/commerce/prism/rpc/models"
	prism_tenant "code.justin.tv/commerce/prism/rpc/tenant"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestValidator_Validate(t *testing.T) {
	Convey("Given a token validator", t, func() {
		channelID := "108706201"
		userID := "108706200"
		amount := 420

		tokenizerFactory := new(message_mock.ITokenizerFactory)
		bitsAmountValidator := new(bits_amount_mock.Validator)

		tokenValidator := &validator{
			TokenizerFactory:    tokenizerFactory,
			BitsAmountValidator: bitsAmountValidator,
		}

		req := &prism_tenant.TokenizeAndValidateMessageReq{
			UserId:    userID,
			ChannelId: channelID,
			ConsumableAmounts: map[string]int64{
				prism_models.ConsumableType_Bits: int64(amount),
			},
			Message:   fmt.Sprintf("cheer%d", amount),
			MessageId: "test-message",
			Anonymous: false,
		}

		actions := []apimodel.Action{{Prefix: "cheer"}, {Prefix: "anon"}}

		Convey("When tokenizer creation fails", func() {
			tokenizerFactory.On("NewTokenizer", mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("test error"))

			Convey("Then an error is returned", func() {
				ctx := context.Background()
				ctx, cancel := context.WithTimeout(ctx, 1*time.Second)
				defer cancel()

				respChan := make(chan validate.TokenResponse, 1)

				go tokenValidator.Validate(ctx, req, respChan)

				for {
					select {
					case resp := <-respChan:
						So(resp.Error, ShouldNotBeNil)
						return
					case <-ctx.Done():
						panic("test doesn't work")
					}
				}
			})
		})

		Convey("When tokenizer creation succeeds", func() {
			tokenizerFactory.On("NewTokenizer", mock.Anything, mock.Anything, mock.Anything).Return(tokenizer.NewTestTokenizer(t, actions), nil)

			Convey("When bits amount validator fails", func() {
				bitsAmountValidator.On("Validate", mock.Anything, mock.Anything, mock.Anything).Return(errors.New("test error"))

				Convey("Then an error is returned", func() {
					ctx := context.Background()
					ctx, cancel := context.WithTimeout(ctx, 1*time.Second)
					defer cancel()

					respChan := make(chan validate.TokenResponse, 1)

					go tokenValidator.Validate(ctx, req, respChan)

					for {
						select {
						case resp := <-respChan:
							So(resp.Error, ShouldNotBeNil)
							return
						case <-ctx.Done():
							panic("test doesn't work")
						}
					}
				})
			})

			Convey("When bits amount validator succeeds", func() {
				bitsAmountValidator.On("Validate", mock.Anything, mock.Anything, mock.Anything).Return(nil)

				Convey("When the cheer is NOT anonymous", func() {
					req.Anonymous = false

					Convey("When an anon cheermote is present in a non-anonymous message", func() {
						req.Message = fmt.Sprintf("anon%d", amount)

						Convey("Then an error is returned", func() {
							ctx := context.Background()
							ctx, cancel := context.WithTimeout(ctx, 1*time.Second)
							defer cancel()

							respChan := make(chan validate.TokenResponse, 1)

							go tokenValidator.Validate(ctx, req, respChan)

							for {
								select {
								case resp := <-respChan:
									So(resp.Error, ShouldNotBeNil)
									return
								case <-ctx.Done():
									panic("test doesn't work")
								}
							}
						})
					})

					Convey("When no anon cheermotes are present in a non-anonymous message", func() {
						Convey("Then a successful token response is returned", func() {
							ctx := context.Background()
							ctx, cancel := context.WithTimeout(ctx, 1*time.Second)
							defer cancel()

							respChan := make(chan validate.TokenResponse, 1)

							go tokenValidator.Validate(ctx, req, respChan)

							for {
								select {
								case resp := <-respChan:
									So(resp.Error, ShouldBeNil)
									So(resp.TotalBits, ShouldEqual, int64(amount))
									So(resp.AllTokenValues, ShouldResemble, []int64{int64(amount)})
									So(resp.Tokens, ShouldResemble, []*prism.Token{{Text: fmt.Sprintf("cheer%d", amount), Amount: int64(amount)}})
									return
								case <-ctx.Done():
									panic("test doesn't work")
								}
							}
						})
					})
				})

				Convey("When the cheer is anonymous", func() {
					req.Anonymous = true

					Convey("When the message contains text", func() {
						req.Message = fmt.Sprintf("anon%d goats yooo", amount)

						Convey("Then an error is returned", func() {
							ctx := context.Background()
							ctx, cancel := context.WithTimeout(ctx, 1*time.Second)
							defer cancel()

							respChan := make(chan validate.TokenResponse, 1)

							go tokenValidator.Validate(ctx, req, respChan)

							for {
								select {
								case resp := <-respChan:
									So(resp.Error, ShouldNotBeNil)
									return
								case <-ctx.Done():
									panic("test doesn't work")
								}
							}
						})
					})

					Convey("When the message contains emotes", func() {
						req.Message = fmt.Sprintf("anon%d Kappa", amount)

						Convey("Then an error is returned", func() {
							ctx := context.Background()
							ctx, cancel := context.WithTimeout(ctx, 1*time.Second)
							defer cancel()

							respChan := make(chan validate.TokenResponse, 1)

							go tokenValidator.Validate(ctx, req, respChan)

							for {
								select {
								case resp := <-respChan:
									So(resp.Error, ShouldNotBeNil)
									return
								case <-ctx.Done():
									panic("test doesn't work")
								}
							}
						})
					})

					Convey("When the message contains non-anon cheermotes", func() {
						req.Message = fmt.Sprintf("cheer%d", amount)

						Convey("Then an error is returned", func() {
							ctx := context.Background()
							ctx, cancel := context.WithTimeout(ctx, 1*time.Second)
							defer cancel()

							respChan := make(chan validate.TokenResponse, 1)

							go tokenValidator.Validate(ctx, req, respChan)

							for {
								select {
								case resp := <-respChan:
									So(resp.Error, ShouldNotBeNil)
									return
								case <-ctx.Done():
									panic("test doesn't work")
								}
							}
						})
					})

					Convey("When the message contains only anon cheermotes", func() {
						req.Message = fmt.Sprintf("anon%d", amount)

						Convey("Then a successful token response is returned", func() {
							ctx := context.Background()
							ctx, cancel := context.WithTimeout(ctx, 1*time.Second)
							defer cancel()

							respChan := make(chan validate.TokenResponse, 1)

							go tokenValidator.Validate(ctx, req, respChan)

							for {
								select {
								case resp := <-respChan:
									So(resp.Error, ShouldBeNil)
									So(resp.TotalBits, ShouldEqual, int64(amount))
									So(resp.AllTokenValues, ShouldResemble, []int64{int64(amount)})
									So(resp.Tokens, ShouldResemble, []*prism.Token{{Text: fmt.Sprintf("anon%d", amount), Amount: int64(amount)}})
									return
								case <-ctx.Done():
									panic("test doesn't work")
								}
							}
						})
					})
				})
			})
		})
	})
}
