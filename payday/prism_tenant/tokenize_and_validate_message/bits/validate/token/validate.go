package token

import (
	"context"
	"fmt"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/anonymous"
	"code.justin.tv/commerce/payday/api/constants"
	bitsmessage "code.justin.tv/commerce/payday/message"
	apimodel "code.justin.tv/commerce/payday/models/api"
	"code.justin.tv/commerce/payday/prism_tenant/tokenize_and_validate_message/bits/validate"
	"code.justin.tv/commerce/payday/prism_tenant/tokenize_and_validate_message/bits/validate/bits_amount"
	prism "code.justin.tv/commerce/prism/rpc"
	prism_models "code.justin.tv/commerce/prism/rpc/models"
	prism_tenant "code.justin.tv/commerce/prism/rpc/tenant"
	"github.com/twitchtv/twirp"
)

type Validator interface {
	Validate(ctx context.Context, req *prism_tenant.TokenizeAndValidateMessageReq, tokenizedChan chan validate.TokenResponse)
}

func NewValidator() Validator {
	return &validator{}
}

type validator struct {
	TokenizerFactory    bitsmessage.ITokenizerFactory `inject:""`
	BitsAmountValidator bits_amount.Validator         `inject:""`
}

func (v *validator) Validate(ctx context.Context, req *prism_tenant.TokenizeAndValidateMessageReq, tokenChan chan validate.TokenResponse) {
	tokenizer, err := v.TokenizerFactory.NewTokenizer(ctx, req.ChannelId, req.UserId)
	if err != nil {
		tokenChan <- validate.TokenResponse{
			Error: twirp.NewError(twirp.Internal, err.Error()),
		}
		return
	}

	logFields := log.Fields{
		"message":   req.Message,
		"userID":    req.UserId,
		"channelID": req.ChannelId,
	}

	totalBits, allTokenValues, emoteOrder, _, _, includesDisplayOnlyAction, err := tokenizer.TokenizeWithEmoteOrderAndIncludesChecks(req.Message)
	if err != nil {
		log.WithError(err).WithFields(logFields).Error("Error tokenizing bits message")
		tokenChan <- validate.TokenResponse{
			Error: twirp.NewError(twirp.InvalidArgument, err.Error()),
		}
		return
	}

	if includesDisplayOnlyAction {
		tokenChan <- validate.TokenResponse{
			Error: twirp.NewError(twirp.InvalidArgument, "Message includes display only cheermotes"),
		}
		return
	}

	var tokens []*prism.Token
	for i := 0; i < len(emoteOrder); i++ {
		tokens = append(tokens, &prism.Token{
			Text:   fmt.Sprintf("%s%d", emoteOrder[i], allTokenValues[i]),
			Amount: allTokenValues[i],
		})
	}

	err = v.BitsAmountValidator.Validate(allTokenValues, totalBits, req.ConsumableAmounts[prism_models.ConsumableType_Bits])
	if err != nil {
		tokenChan <- validate.TokenResponse{
			Error: twirp.NewError(twirp.InvalidArgument, err.Error()),
		}
		return
	}

	if !req.Anonymous {
		for _, emote := range emoteOrder {
			if emote == anonymous.AnonymousCheermotePrefix {
				tokenChan <- validate.TokenResponse{
					Error: prism_models.NewClientError(
						twirp.NewError(twirp.InvalidArgument, constants.NoAnonymousCheermoteAllowedError),
						apimodel.CreateEventErrorResponse{
							Status:  apimodel.AnonymousCheermoteNotAllowed,
							Message: constants.NoAnonymousCheermoteAllowedError,
						}),
				}
				return
			}
		}
	} else {
		// Anonymous Cheering Validation: Only Anon cheermotes are allowed
		hasTextOrEmote := tokenizer.SanitizeMessage(req.Message) != tokenizer.StripText(req.Message)

		hasNonAnonCheermote := false
		for _, emote := range emoteOrder {
			if emote != anonymous.AnonymousCheermotePrefix {
				hasNonAnonCheermote = true
				break
			}
		}

		if hasTextOrEmote || hasNonAnonCheermote {
			anonymousMessage, err := tokenizer.Anonymize(req.Message)
			if err != nil {
				tokenChan <- validate.TokenResponse{
					Error: twirp.NewError(twirp.InvalidArgument, err.Error()),
				}
				return
			}

			log.WithFields(log.Fields{
				"userID":    req.UserId,
				"channelID": req.ChannelId,
			}).Info("Anonymous message had non-Anon cheermote text, returning InvalidAnonymousMessageError")
			tokenChan <- validate.TokenResponse{
				Error:                 twirp.NewError(twirp.InvalidArgument, "Anonymous message had non-Anon cheermote text"),
				AnonymousMessageError: prism_models.NewInvalidAnonymousMessageError(constants.AnonymousMessageInvalidError, anonymousMessage),
			}
			return
		}
	}

	tokenChan <- validate.TokenResponse{
		TotalBits:      totalBits,
		AllTokenValues: allTokenValues,
		Tokens:         tokens,
	}
}
