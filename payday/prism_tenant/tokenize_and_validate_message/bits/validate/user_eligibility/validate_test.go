package user_eligibility

import (
	"context"
	"errors"
	"testing"
	"time"

	metrics_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/metrics"
	user_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/user"
	"code.justin.tv/commerce/payday/prism_tenant/tokenize_and_validate_message/bits/validate"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestValidator_Validate(t *testing.T) {
	Convey("Given a user eligibility validator", t, func() {
		userID := "108706200"

		userChecker := new(user_mock.Checker)
		statter := new(metrics_mock.Statter)

		userEligibilityValidator := &validator{
			UserChecker: userChecker,
			Statter:     statter,
		}

		statter.On("TimingDuration", mock.Anything, mock.Anything).Return()

		Convey("When user checker fails", func() {
			userChecker.On("IsEligible", mock.Anything, mock.Anything).Return(false, errors.New("test error"))

			Convey("Then an error is returned", func() {
				ctx := context.Background()
				ctx, cancel := context.WithTimeout(ctx, 1*time.Second)
				defer cancel()

				respChan := make(chan validate.UserEligibilityResponse, 1)

				go userEligibilityValidator.Validate(context.Background(), userID, respChan)

				for {
					select {
					case resp := <-respChan:
						So(resp.Error, ShouldNotBeNil)
						return
					case <-ctx.Done():
						panic("test doesn't work")
					}
				}
			})
		})

		Convey("When user checker succeeds", func() {
			Convey("When the user is NOT eligible", func() {
				userChecker.On("IsEligible", mock.Anything, mock.Anything).Return(false, nil)

				Convey("Then an error is returned", func() {
					ctx := context.Background()
					ctx, cancel := context.WithTimeout(ctx, 1*time.Second)
					defer cancel()

					respChan := make(chan validate.UserEligibilityResponse, 1)

					go userEligibilityValidator.Validate(context.Background(), userID, respChan)

					for {
						select {
						case resp := <-respChan:
							So(resp.Error, ShouldNotBeNil)
							return
						case <-ctx.Done():
							panic("test doesn't work")
						}
					}
				})
			})

			Convey("When the user is eligible", func() {
				userChecker.On("IsEligible", mock.Anything, mock.Anything).Return(true, nil)

				Convey("Then a successful user eligibility response is returned", func() {
					ctx := context.Background()
					ctx, cancel := context.WithTimeout(ctx, 1*time.Second)
					defer cancel()

					respChan := make(chan validate.UserEligibilityResponse, 1)

					go userEligibilityValidator.Validate(context.Background(), userID, respChan)

					for {
						select {
						case resp := <-respChan:
							So(resp.Error, ShouldBeNil)
							return
						case <-ctx.Done():
							panic("test doesn't work")
						}
					}
				})
			})
		})
	})
}
