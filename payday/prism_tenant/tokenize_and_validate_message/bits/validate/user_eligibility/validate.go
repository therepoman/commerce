package user_eligibility

import (
	"context"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/api/constants"
	"code.justin.tv/commerce/payday/metrics"
	"code.justin.tv/commerce/payday/models"
	apimodel "code.justin.tv/commerce/payday/models/api"
	"code.justin.tv/commerce/payday/prism_tenant/tokenize_and_validate_message/bits/validate"
	"code.justin.tv/commerce/payday/user"
	prism_models "code.justin.tv/commerce/prism/rpc/models"
	"github.com/twitchtv/twirp"
)

type Validator interface {
	Validate(ctx context.Context, userID string, userEligibilityChan chan validate.UserEligibilityResponse)
}

func NewValidator() Validator {
	return &validator{}
}

type validator struct {
	UserChecker user.Checker    `inject:""`
	Statter     metrics.Statter `inject:""`
}

func (v *validator) Validate(ctx context.Context, userID string, userEligibilityChan chan validate.UserEligibilityResponse) {
	startTime := time.Now()
	defer v.logDurationMetric("GiveBitsStep_RequestValidation_UserEligibility", startTime)

	eligible, err := v.UserChecker.IsEligible(ctx, userID)
	if err != nil {
		log.WithError(err).Error("Error getting user eligibility from eligibility retriever")
		userEligibilityChan <- validate.UserEligibilityResponse{
			Error:     err,
			ErrorType: models.BitsUnavailable,
		}
		return
	}

	if !eligible {
		log.Info("User is not eligible to use Bits")
		userEligibilityChan <- validate.UserEligibilityResponse{
			Error: prism_models.NewClientError(
				twirp.NewError(twirp.InvalidArgument, constants.UserIneligibleError),
				apimodel.CreateEventErrorResponse{
					Status:  apimodel.UserIneligible,
					Message: constants.UserIneligibleError,
				}),
			ErrorType: models.UserIneligible,
		}
		return
	}

	userEligibilityChan <- validate.UserEligibilityResponse{}
}

func (v *validator) logDurationMetric(name string, startTime time.Time) {
	go v.Statter.TimingDuration(name, time.Since(startTime))
}
