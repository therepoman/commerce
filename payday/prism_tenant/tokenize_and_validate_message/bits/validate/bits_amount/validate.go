package bits_amount

import (
	log "code.justin.tv/commerce/logrus"
	actionsUtils "code.justin.tv/commerce/payday/actions/utils"
	"code.justin.tv/commerce/payday/api/constants"
	apimodel "code.justin.tv/commerce/payday/models/api"
	prism_models "code.justin.tv/commerce/prism/rpc/models"
	"github.com/twitchtv/twirp"
)

type Validator interface {
	Validate(allTokenValues []int64, actualBitsAmount int64, requestBitsAmount int64) error
}

func NewValidator() Validator {
	return &validator{}
}

type validator struct{}

func (v *validator) Validate(allTokenValues []int64, actualBitsAmount int64, requestBitsAmount int64) error {
	for _, tokenValue := range allTokenValues {
		if tokenValue > actionsUtils.MaximumCheermoteMaximum {
			log.WithField("allTokenValues", allTokenValues).Warn(constants.TooLargeBitsEmoteError)
			err := prism_models.NewClientError(
				twirp.NewError(twirp.InvalidArgument, constants.TooLargeBitsEmoteError),
				apimodel.CreateEventErrorResponse{
					Status:  apimodel.TooLargeBitsEmote,
					Message: constants.TooLargeBitsEmoteError,
				})
			return err
		}
	}

	if actualBitsAmount <= 0 {
		log.WithField("allTokenValues", allTokenValues).Warn(constants.InvalidBitsMessageError)
		err := prism_models.NewClientError(
			twirp.NewError(twirp.InvalidArgument, constants.InvalidBitsMessageError),
			apimodel.CreateEventErrorResponse{
				Status:  apimodel.InvalidBitsMessage,
				Message: constants.InvalidBitsMessageError,
			})
		return err
	}

	if actualBitsAmount != requestBitsAmount {
		log.WithField("allTokenValues", allTokenValues).Warn(constants.InvalidBitsAmountError)
		err := prism_models.NewClientError(
			twirp.NewError(twirp.InvalidArgument, constants.InvalidBitsAmountError),
			apimodel.CreateEventErrorResponse{
				Status:  apimodel.InvalidBitsAmount,
				Message: constants.InvalidBitsAmountError,
			})
		return err
	}

	if actualBitsAmount > actionsUtils.MaximumBitsMessage {
		log.WithField("allTokenValues", allTokenValues).Warn(constants.TooLargeCheerError)
		err := prism_models.NewClientError(
			twirp.NewError(twirp.InvalidArgument, constants.TooLargeCheerError),
			apimodel.CreateEventErrorResponse{
				Status:  apimodel.TooLargeCheer,
				Message: constants.TooLargeCheerError,
			})
		return err
	}

	return nil
}
