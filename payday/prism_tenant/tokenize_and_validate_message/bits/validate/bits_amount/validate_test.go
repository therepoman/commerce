package bits_amount

import (
	"testing"

	actionsUtils "code.justin.tv/commerce/payday/actions/utils"
	. "github.com/smartystreets/goconvey/convey"
)

func TestValidator_Validate(t *testing.T) {
	Convey("Given a bits amount validator", t, func() {
		tokenValues := []int64{int64(420)}
		actualBitsAmount := int64(420)
		requestBitsAmount := int64(420)

		bitsAmountValidator := &validator{}

		Convey("When a token value exceeds the maximum", func() {
			tokenValues := []int64{int64(actionsUtils.MaximumCheermoteMaximum + 1)}

			Convey("Then an error is returned", func() {
				err := bitsAmountValidator.Validate(tokenValues, actualBitsAmount, requestBitsAmount)

				So(err, ShouldNotBeNil)
			})
		})

		Convey("When no token value exceeds the maximum", func() {
			Convey("When the actual bits amount is less than or equal to zero", func() {
				actualBitsAmount := int64(0)

				Convey("Then an error is returned", func() {
					err := bitsAmountValidator.Validate(tokenValues, actualBitsAmount, requestBitsAmount)

					So(err, ShouldNotBeNil)
				})
			})

			Convey("When the bits amount is greater than zero", func() {
				Convey("When the actual bits amount and the request bits amount do NOT match", func() {
					requestBitsAmount := actualBitsAmount + 1

					Convey("Then an error is returned", func() {
						err := bitsAmountValidator.Validate(tokenValues, actualBitsAmount, requestBitsAmount)

						So(err, ShouldNotBeNil)
					})
				})

				Convey("When the actual bits amount and the request bits amount match", func() {
					Convey("When the actual bits amount exceeds the maximum cheer amount per message", func() {
						actualBitsAmount := int64(actionsUtils.MaximumBitsMessage + 1)

						Convey("Then an error is returned", func() {
							err := bitsAmountValidator.Validate(tokenValues, actualBitsAmount, requestBitsAmount)

							So(err, ShouldNotBeNil)
						})
					})

					Convey("When the actual bits amount does NOT exceed the maximum cheer amount per message", func() {
						Convey("Then NO error is returned", func() {
							err := bitsAmountValidator.Validate(tokenValues, actualBitsAmount, requestBitsAmount)

							So(err, ShouldBeNil)
						})
					})
				})
			})
		})
	})
}
