package channel_min_bits_emote

import (
	"testing"
	"time"

	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/utils/pointers"
	. "github.com/smartystreets/goconvey/convey"
)

func TestValidator_Validate(t *testing.T) {
	Convey("Given a channel min bits emote validator", t, func() {
		channelID := "108706201"
		minBitsEmote := int64(50)

		channelMinBitsEmoteValidator := &validator{}

		channel := &dynamo.Channel{
			Id:           dynamo.ChannelId(channelID),
			Onboarded:    pointers.BoolP(true),
			OptedOut:     pointers.BoolP(false),
			LastUpdated:  time.Now(),
			MinBitsEmote: &minBitsEmote,
		}

		Convey("When token value is less than the channel emote minimum", func() {
			tokenValues := []int64{int64(5)}

			Convey("Then an error is returned", func() {
				valid, _, err := channelMinBitsEmoteValidator.Validate(tokenValues, channel)

				So(err, ShouldNotBeNil)
				So(valid, ShouldBeFalse)
			})
		})

		Convey("When token value is greater than the channel emote minimum", func() {
			tokenValues := []int64{int64(420)}

			Convey("Then a successful response is returned", func() {
				valid, _, err := channelMinBitsEmoteValidator.Validate(tokenValues, channel)

				So(err, ShouldBeNil)
				So(valid, ShouldBeTrue)
			})
		})
	})
}
