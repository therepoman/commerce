package channel_min_bits_emote

import (
	"code.justin.tv/commerce/payday/api/constants"
	"code.justin.tv/commerce/payday/cache/channel"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/models"
	apimodel "code.justin.tv/commerce/payday/models/api"
	prism_models "code.justin.tv/commerce/prism/rpc/models"
	"github.com/twitchtv/twirp"
)

type Validator interface {
	Validate(tokenValues []int64, ch *dynamo.Channel) (bool, models.ErrorType, error)
}

func NewValidator() Validator {
	return &validator{}
}

type validator struct{}

func (v *validator) Validate(tokenValues []int64, ch *dynamo.Channel) (bool, models.ErrorType, error) {
	minBitsEmote := channel.GetChannelMinBitsEmote(ch)
	for _, tokenValue := range tokenValues {
		if tokenValue < minBitsEmote {
			err := prism_models.NewClientError(
				twirp.NewError(twirp.InvalidArgument, constants.EmoteAmountBelowMinBitsEmoteError),
				apimodel.CreateEventErrorResponse{
					Status:  apimodel.EmoteAmountBelowMinBits,
					Message: constants.EmoteAmountBelowMinBitsEmoteError,
				})
			return false, models.EmoteBelowMinBitsEmote, err
		}
	}
	return true, models.Default, nil
}
