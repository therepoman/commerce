package channel_eligibility

import (
	"context"
	"errors"
	"testing"
	"time"

	"code.justin.tv/commerce/payday/dynamo"
	metrics_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/metrics"
	partner_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/partner"
	"code.justin.tv/commerce/payday/partner"
	"code.justin.tv/commerce/payday/prism_tenant/tokenize_and_validate_message/bits/validate"
	"code.justin.tv/commerce/payday/utils/pointers"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestValidator_Validate(t *testing.T) {
	Convey("Given a channel eligibility validator", t, func() {
		channelID := "108706201"

		partnerManager := new(partner_mock.IPartnerManager)
		statter := new(metrics_mock.Statter)

		channelEligibilityValidator := &validator{
			PartnerManager: partnerManager,
			Statter:        statter,
		}

		channel := &dynamo.Channel{
			Id:          dynamo.ChannelId(channelID),
			Onboarded:   pointers.BoolP(true),
			OptedOut:    pointers.BoolP(false),
			LastUpdated: time.Now(),
		}

		statter.On("TimingDuration", mock.Anything, mock.Anything).Return()

		Convey("When the channel is NOT eligible", func() {
			channel.Onboarded = pointers.BoolP(false)

			Convey("Then an error is returned", func() {
				ctx := context.Background()
				ctx, cancel := context.WithTimeout(ctx, 1*time.Second)
				defer cancel()

				respChan := make(chan validate.ChannelEligibilityResponse, 1)

				go channelEligibilityValidator.Validate(ctx, channel, respChan)

				for {
					select {
					case resp := <-respChan:
						So(resp.PartnerType, ShouldEqual, partner.NonPartnerPartnerType)
						So(resp.Error, ShouldNotBeNil)
						return
					case <-ctx.Done():
						panic("test doesn't work")
					}
				}
			})
		})

		Convey("When the channel is eligible", func() {
			Convey("When getting partner type fails", func() {
				partnerManager.On("GetPartnerType", mock.Anything, mock.Anything).Return(partner.UnknownPartnerType, errors.New("test error"))

				Convey("Then an error is returned", func() {
					ctx := context.Background()
					ctx, cancel := context.WithTimeout(ctx, 1*time.Second)
					defer cancel()

					respChan := make(chan validate.ChannelEligibilityResponse, 1)

					go channelEligibilityValidator.Validate(ctx, channel, respChan)

					for {
						select {
						case resp := <-respChan:
							So(resp.PartnerType, ShouldEqual, partner.UnknownPartnerType)
							So(resp.Error, ShouldNotBeNil)
							return
						case <-ctx.Done():
							panic("test doesn't work")
						}
					}
				})
			})

			Convey("When getting partner type succeeds", func() {
				Convey("When the partner type is NOT eligible for bits", func() {
					partnerManager.On("GetPartnerType", mock.Anything, mock.Anything).Return(partner.NonPartnerPartnerType, nil)

					Convey("Then an error is returned", func() {
						ctx := context.Background()
						ctx, cancel := context.WithTimeout(ctx, 1*time.Second)
						defer cancel()

						respChan := make(chan validate.ChannelEligibilityResponse, 1)

						go channelEligibilityValidator.Validate(ctx, channel, respChan)

						for {
							select {
							case resp := <-respChan:
								So(resp.PartnerType, ShouldEqual, partner.NonPartnerPartnerType)
								So(resp.Error, ShouldNotBeNil)
								return
							case <-ctx.Done():
								panic("test doesn't work")
							}
						}
					})
				})

				Convey("When the partner type is eligible for bits", func() {
					partnerManager.On("GetPartnerType", mock.Anything, mock.Anything).Return(partner.TraditionalPartnerType, nil)

					Convey("Then a successful channel eligibility response is returned", func() {
						ctx := context.Background()
						ctx, cancel := context.WithTimeout(ctx, 1*time.Second)
						defer cancel()

						respChan := make(chan validate.ChannelEligibilityResponse, 1)

						go channelEligibilityValidator.Validate(ctx, channel, respChan)

						for {
							select {
							case resp := <-respChan:
								So(resp.PartnerType, ShouldEqual, partner.TraditionalPartnerType)
								So(resp.Error, ShouldBeNil)
								return
							case <-ctx.Done():
								panic("test doesn't work")
							}
						}
					})
				})
			})
		})
	})
}
