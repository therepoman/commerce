package channel_eligibility

import (
	"context"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/api/constants"
	"code.justin.tv/commerce/payday/cache/channel"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/metrics"
	"code.justin.tv/commerce/payday/models"
	apimodel "code.justin.tv/commerce/payday/models/api"
	"code.justin.tv/commerce/payday/partner"
	"code.justin.tv/commerce/payday/prism_tenant/tokenize_and_validate_message/bits/validate"
	prism_models "code.justin.tv/commerce/prism/rpc/models"
	"github.com/twitchtv/twirp"
)

type Validator interface {
	Validate(ctx context.Context, ch *dynamo.Channel, channelEligibilityChan chan validate.ChannelEligibilityResponse)
}

func NewValidator() Validator {
	return &validator{}
}

type validator struct {
	PartnerManager partner.IPartnerManager `inject:""`
	Statter        metrics.Statter         `inject:""`
}

func (v *validator) Validate(ctx context.Context, ch *dynamo.Channel, channelEligibilityChan chan validate.ChannelEligibilityResponse) {
	startTime := time.Now()
	defer v.logDurationMetric("GiveBitsStep_RequestValidation_ChannelElgibility", startTime)

	channelEligible := channel.GetChannelEligibility(ch)
	if !channelEligible {
		log.Info("Channel is not eligible to receive Bits")
		channelEligibilityChan <- validate.ChannelEligibilityResponse{
			PartnerType: partner.NonPartnerPartnerType,
			Error: prism_models.NewClientError(
				twirp.NewError(twirp.InvalidArgument, constants.ChannelIneligibleError),
				apimodel.CreateEventErrorResponse{
					Status:  apimodel.ChannelIneligible,
					Message: constants.ChannelIneligibleError,
				}),
			ErrorType: models.ChannelIneligible,
		}
		return
	}

	channelID := string(ch.Id)
	partnerType, err := v.PartnerManager.GetPartnerType(ctx, channelID)
	if err != nil {
		log.WithError(err).Error("Error getting partner type from Ripley")
		channelEligibilityChan <- validate.ChannelEligibilityResponse{
			PartnerType: partnerType,
			Error:       err,
			ErrorType:   models.ChannelIneligible,
		}
		return
	}

	isEligiblePartnerType := partner.IsEligibleForBits(partnerType)

	if !isEligiblePartnerType {
		log.WithFields(log.Fields{
			"channelID":   channelID,
			"partnerType": partnerType,
		}).Error("partner type and Bits eligibility mismatch")
		channelEligibilityChan <- validate.ChannelEligibilityResponse{
			PartnerType: partnerType,
			Error: prism_models.NewClientError(
				twirp.NewError(twirp.InvalidArgument, constants.ChannelIneligibleError),
				apimodel.CreateEventErrorResponse{
					Status:  apimodel.ChannelIneligible,
					Message: constants.ChannelIneligibleError,
				}),
			ErrorType: models.BitsIneligibleChannelMismatch,
		}
		return
	}

	channelEligibilityChan <- validate.ChannelEligibilityResponse{
		PartnerType: partnerType,
		ErrorType:   models.Default,
	}
}

func (v *validator) logDurationMetric(name string, startTime time.Time) {
	go v.Statter.TimingDuration(name, time.Since(startTime))
}
