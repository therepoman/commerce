package validate

import (
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/partner"
	prism "code.justin.tv/commerce/prism/rpc"
	"github.com/twitchtv/twirp"
)

type UserEligibilityResponse struct {
	Error     error
	ErrorType models.ErrorType
}

type ChannelEligibilityResponse struct {
	PartnerType partner.PartnerType
	Error       error
	ErrorType   models.ErrorType
}

type TokenResponse struct {
	TotalBits             int64
	AllTokenValues        []int64
	Tokens                []*prism.Token
	Error                 twirp.Error
	AnonymousMessageError twirp.Error
}

type BalanceResponse struct {
	Error     error
	ErrorType models.ErrorType
}
