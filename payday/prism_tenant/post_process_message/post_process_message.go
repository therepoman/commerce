package post_process_message

import (
	"context"

	prism_tenant "code.justin.tv/commerce/prism/rpc/tenant"
)

type PostProcessMessage interface {
	PostProcess(ctx context.Context, req *prism_tenant.PostProcessMessageReq) (*prism_tenant.PostProcessMessageResp, error)
}
