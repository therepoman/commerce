package bits

import (
	"context"

	"code.justin.tv/commerce/payday/prism_tenant/post_process_message"
	"code.justin.tv/commerce/payday/prism_tenant/post_process_message/bits/transaction"
	"code.justin.tv/commerce/payday/prism_tenant/process_message/bits/sns"
	"code.justin.tv/commerce/payday/prism_tenant/process_message/bits/transformations"
	prism_tenant "code.justin.tv/commerce/prism/rpc/tenant"
)

func NewMessagePostProcessor() post_process_message.PostProcessMessage {
	return &messagePostProcessorImpl{}
}

type messagePostProcessorImpl struct {
	SNSPublisher       sns.SNSPublisher                   `inject:""`
	TransactionSaver   transaction.Saver                  `inject:""`
	MessageTransformer transformations.MessageTransformer `inject:""`
}

func (m *messagePostProcessorImpl) PostProcess(ctx context.Context, req *prism_tenant.PostProcessMessageReq) (*prism_tenant.PostProcessMessageResp, error) {
	processReq := &prism_tenant.ProcessMessageReq{
		UserId:            req.UserId,
		ChannelId:         req.ChannelId,
		RoomId:            req.RoomId,
		MessageId:         req.MessageId,
		Message:           req.Message,
		ConsumableAmounts: req.ConsumableAmounts,
		Anonymous:         req.Anonymous,
		GeoIpMetadata:     req.GeoIpMetadata,
	}

	_, emoteTotals, transErr := m.MessageTransformer.GetTransformations(ctx, processReq)
	if transErr != nil {
		return nil, transErr.Error
	}

	go m.SNSPublisher.PublishToBitsUsageSNSTopic(req, emoteTotals)

	err := m.TransactionSaver.SaveTransaction(ctx, req, emoteTotals)
	if err != nil {
		return nil, err
	}

	return &prism_tenant.PostProcessMessageResp{}, nil
}
