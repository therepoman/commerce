package transaction

import (
	"context"
	"errors"

	"code.justin.tv/commerce/gogogadget/pointers"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/hystrix"
	cmds "code.justin.tv/commerce/payday/hystrix"
	"code.justin.tv/commerce/payday/middleware"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/oauth"
	prism_models "code.justin.tv/commerce/prism/rpc/models"
	prism_tenant "code.justin.tv/commerce/prism/rpc/tenant"
)

type Saver interface {
	SaveTransaction(ctx context.Context, req *prism_tenant.PostProcessMessageReq, emoteTotals map[string]models.BitEmoteTotal) error
}

type saver struct {
	TransactionsDao dynamo.ITransactionsDao `inject:""`
}

func NewSaver() Saver {
	return &saver{}
}

func (s *saver) SaveTransaction(ctx context.Context, req *prism_tenant.PostProcessMessageReq, emoteTotals map[string]models.BitEmoteTotal) error {
	if req == nil {
		return errors.New("request cannot be nil")
	}

	bitsAmount := int64(req.ConsumableAmounts[prism_models.ConsumableType_Bits])
	bitsAmountSponsored := int64(0)

	clientID := middleware.GetClientID(ctx)
	platform, _ := oauth.GetClient(clientID)

	emoteTotalsInt64 := make(map[string]int64)
	for prefix, total := range emoteTotals {
		emoteTotalsInt64[prefix] = total.CheeredTotal
		bitsAmountSponsored += total.MatchedTotal
	}

	transaction := dynamo.Transaction{
		TransactionId:         req.MessageId,
		TmiMessage:            &req.SentMessage,
		RawMessage:            &req.Message,
		ChannelId:             &req.ChannelId,
		UserId:                &req.UserId,
		NumberOfBits:          &bitsAmount,
		NumberOfBitsSponsored: &bitsAmountSponsored,
		TransactionType:       pointers.StringP(models.BitsEventTransactionType),
		ClientApp:             &platform,
		EmoteTotals:           emoteTotalsInt64,
	}

	err := hystrix.Do(cmds.UpdateTransactionsCommand, func() error {
		return s.TransactionsDao.Update(&transaction)
	}, nil)

	if err != nil {
		log.WithError(err).WithField("transaction", transaction).Error("failed to save transaction record to dynamo")
		return err
	}
	return nil
}
