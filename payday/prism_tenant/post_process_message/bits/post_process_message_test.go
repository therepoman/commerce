package bits

import (
	"context"
	"errors"
	"fmt"
	"testing"

	transaction_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/prism_tenant/post_process_message/bits/transaction"
	sns_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/prism_tenant/process_message/bits/sns"
	transformations_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/prism_tenant/process_message/bits/transformations"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/prism_tenant/process_message/bits/transformations"
	prism "code.justin.tv/commerce/prism/rpc"
	prism_models "code.justin.tv/commerce/prism/rpc/models"
	prism_tenant "code.justin.tv/commerce/prism/rpc/tenant"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestPostProcessMessage(t *testing.T) {
	Convey("Given a bits message post processor", t, func() {
		channelID := "108706201"
		userID := "108706200"
		amount := 420

		snsPublisher := new(sns_mock.SNSPublisher)
		messageTransformer := new(transformations_mock.MessageTransformer)
		transactionSaver := new(transaction_mock.Saver)

		snsPublisher.On("PublishToBitsUsageSNSTopic", mock.Anything, mock.Anything, mock.Anything).Return()

		api := &messagePostProcessorImpl{
			SNSPublisher:       snsPublisher,
			MessageTransformer: messageTransformer,
			TransactionSaver:   transactionSaver,
		}

		req := &prism_tenant.PostProcessMessageReq{
			UserId:    userID,
			ChannelId: channelID,
			ConsumableAmounts: map[string]int64{
				prism_models.ConsumableType_Bits: int64(amount),
			},
			Message:     fmt.Sprintf("cheer%d coregamers", amount),
			MessageId:   "test-message",
			Anonymous:   false,
			SentMessage: fmt.Sprintf("cheer%d ***", amount),
		}

		Convey("When the message transformer fails", func() {
			messageTransformer.On("GetTransformations", mock.Anything, mock.Anything).Return(nil, nil, &transformations.TransformerError{
				Type:  transformations.TokenizationError,
				Error: errors.New("e"),
			})

			Convey("Then an error is returned", func() {
				res, err := api.PostProcess(context.Background(), req)

				So(res, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("When the message transformer succeeds", func() {
			messageTransformer.On("GetTransformations", mock.Anything, mock.Anything).Return([]*prism.Transformation{}, map[string]models.BitEmoteTotal{}, nil)

			Convey("When the transaction saver errors", func() {
				transactionSaver.On("SaveTransaction", mock.Anything, mock.Anything, mock.Anything).Return(errors.New("test error"))

				Convey("Then an error is returned", func() {
					res, err := api.PostProcess(context.Background(), req)

					So(res, ShouldBeNil)
					So(err, ShouldNotBeNil)
				})
			})

			Convey("when the transaction saver succeeds", func() {
				transactionSaver.On("SaveTransaction", mock.Anything, mock.Anything, mock.Anything).Return(nil)

				Convey("No error is returned", func() {
					res, err := api.PostProcess(context.Background(), req)

					So(res, ShouldNotBeNil)
					So(err, ShouldBeNil)
				})
			})
		})
	})
}
