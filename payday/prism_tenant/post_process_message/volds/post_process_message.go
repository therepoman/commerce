package volds

import (
	"context"

	"code.justin.tv/commerce/payday/prism_tenant/post_process_message"
	prism_tenant "code.justin.tv/commerce/prism/rpc/tenant"
)

func NewMessagePostProcessor() post_process_message.PostProcessMessage {
	return &messagePostProcessorImpl{}
}

type messagePostProcessorImpl struct {
}

func (m *messagePostProcessorImpl) PostProcess(ctx context.Context, req *prism_tenant.PostProcessMessageReq) (*prism_tenant.PostProcessMessageResp, error) {
	return &prism_tenant.PostProcessMessageResp{}, nil
}
