package volds

import (
	"context"

	"code.justin.tv/commerce/payday/prism_tenant/process_message"
	prism_tenant "code.justin.tv/commerce/prism/rpc/tenant"
)

func NewMessageProcessor() process_message.ProcessMessage {
	return &messageProcessorImpl{}
}

type messageProcessorImpl struct {
}

func (m *messageProcessorImpl) Process(ctx context.Context, req *prism_tenant.ProcessMessageReq) (prism_tenant.ProcessMessageResp, error) {
	return prism_tenant.ProcessMessageResp{}, nil
}
