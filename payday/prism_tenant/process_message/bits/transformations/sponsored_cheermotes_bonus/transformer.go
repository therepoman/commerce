package sponsored_cheermotes_bonus

import (
	"fmt"

	"code.justin.tv/commerce/payday/models"
	prism "code.justin.tv/commerce/prism/rpc"
	prism_models "code.justin.tv/commerce/prism/rpc/models"
)

const bonusCheermote = "bonus"

type SponsoredCheermoteBonusMessageTransformer interface {
	GetTransformations(emoteTotals map[string]models.BitEmoteTotal) []*prism.Transformation
}

func NewSponsoredCheermoteBonusMessageTransformer() SponsoredCheermoteBonusMessageTransformer {
	return &sponsoredCheermoteBonusMessageTransformer{}
}

type sponsoredCheermoteBonusMessageTransformer struct {
}

func (t *sponsoredCheermoteBonusMessageTransformer) GetTransformations(emoteTotals map[string]models.BitEmoteTotal) []*prism.Transformation {
	var transformations []*prism.Transformation
	for prefix, total := range emoteTotals {
		if total.MatchedTotal > 0 {
			transformations = append(transformations, &prism.Transformation{
				Type: prism.TransformationType_APPEND_TOKEN_AFTER_LAST_OCCURRENCE_OF_CHEERMOTE,
				Arguments: map[string]string{
					prism_models.TransformationArg_TokenToAppend:   fmt.Sprintf("%s%d", bonusCheermote, total.MatchedTotal),
					prism_models.TransformationArg_CheermotePrefix: prefix,
				},
			})
		}
	}
	return transformations
}
