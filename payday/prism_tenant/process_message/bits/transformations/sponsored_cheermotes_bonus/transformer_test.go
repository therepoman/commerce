package sponsored_cheermotes_bonus

import (
	"testing"

	"code.justin.tv/commerce/payday/models"
	. "github.com/smartystreets/goconvey/convey"
)

func TestMessageTransformer_Transform(t *testing.T) {
	Convey("Given a sponsored cheermote bonus message transformer", t, func() {
		messageTransformer := &sponsoredCheermoteBonusMessageTransformer{}

		emoteTotals := map[string]models.BitEmoteTotal{}

		Convey("When no cheermotes have a MatchedTotal greater than zero", func() {
			emoteTotals = map[string]models.BitEmoteTotal{
				"cheer": {
					CheeredTotal: 100,
				},
			}

			Convey("Then no transformations are returned", func() {
				transformations := messageTransformer.GetTransformations(emoteTotals)

				So(transformations, ShouldBeEmpty)
			})
		})

		Convey("When some cheermotes have a MatchedTotal greater than zero", func() {
			emoteTotals = map[string]models.BitEmoteTotal{
				"Doritos": {
					CheeredTotal: 100,
					MatchedTotal: 10,
				},
				"Dominos": {
					CheeredTotal: 50,
					MatchedTotal: 5,
				},
				"Cheer": {
					CheeredTotal: 100,
				},
			}

			Convey("Then one transformation is returned for each one of those cheermotes", func() {
				transformations := messageTransformer.GetTransformations(emoteTotals)

				So(len(transformations), ShouldEqual, 2)
			})
		})
	})
}
