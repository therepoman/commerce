package transformations

import (
	"context"
	"errors"
	"fmt"
	"testing"

	message_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/message"
	tokenizer_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/message/tokenizer"
	sponsored_cheermotes_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/prism_tenant/process_message/bits/sponsored_cheermotes"
	sponsored_cheermotes_bonus_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/prism_tenant/process_message/bits/transformations/sponsored_cheermotes_bonus"
	"code.justin.tv/commerce/payday/models"
	prism "code.justin.tv/commerce/prism/rpc"
	prism_models "code.justin.tv/commerce/prism/rpc/models"
	prism_tenant "code.justin.tv/commerce/prism/rpc/tenant"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestMessageTransformer_Transform(t *testing.T) {
	Convey("Given a message transformer", t, func() {
		channelID := "108706201"
		userID := "108706200"
		amount := 420

		tokenizerFactory := new(message_mock.ITokenizerFactory)
		sponsoredCheermoteValidator := new(sponsored_cheermotes_mock.SponsoredCheermoteValidator)
		sponsoredCheermoteBonusMessageTransformer := new(sponsored_cheermotes_bonus_mock.SponsoredCheermoteBonusMessageTransformer)
		tokenizer := new(tokenizer_mock.ITokenizer)

		messageTransformer := &messageTransformer{
			TokenizerFactory:                          tokenizerFactory,
			SponsoredCheermoteValidator:               sponsoredCheermoteValidator,
			SponsoredCheermoteBonusMessageTransformer: sponsoredCheermoteBonusMessageTransformer,
		}

		req := &prism_tenant.ProcessMessageReq{
			UserId:    userID,
			ChannelId: channelID,
			ConsumableAmounts: map[string]int64{
				prism_models.ConsumableType_Bits: int64(amount),
			},
			Message:   fmt.Sprintf("cheer%d", amount),
			MessageId: "test-message",
			Anonymous: false,
		}

		sponsoredCheermoteBonusMessageTransformer.On("GetTransformations", mock.Anything).Return([]*prism.Transformation{{}})

		Convey("When tokenizer creation fails", func() {
			tokenizerFactory.On("NewTokenizer", mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("test error"))

			Convey("Then an error is returned", func() {
				transformations, _, err := messageTransformer.GetTransformations(context.Background(), req)

				So(err.Type, ShouldEqual, TokenizationError)
				So(transformations, ShouldBeNil)
			})
		})

		Convey("When tokenizer creation succeeds", func() {
			tokenizerFactory.On("NewTokenizer", mock.Anything, mock.Anything, mock.Anything).Return(tokenizer, nil)

			Convey("When tokenization fails", func() {
				tokenizer.On("TokenizeWithEmoteOrder", mock.Anything).Return(int64(0), nil, nil, nil, errors.New("test error"))

				Convey("Then an error is returned", func() {
					transformations, _, err := messageTransformer.GetTransformations(context.Background(), req)

					So(err.Type, ShouldEqual, TokenizationError)
					So(transformations, ShouldBeNil)
				})
			})

			Convey("When tokenization succeeds", func() {
				tokenizer.On("TokenizeWithEmoteOrder", mock.Anything).Return(int64(0), nil, nil, map[string]int64{"cheer": int64(amount)}, nil)

				Convey("When the sponsored cheermote validator succeeds", func() {
					sponsoredCheermoteValidator.On("AddAndValidateSponsoredCheermotes", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(map[string]models.BitEmoteTotal{}, nil)

					Convey("Then all transformers are called, and a successful response is returned", func() {
						transformations, _, err := messageTransformer.GetTransformations(context.Background(), req)

						So(err, ShouldBeNil)
						So(len(transformations), ShouldEqual, 1)
						sponsoredCheermoteBonusMessageTransformer.AssertNumberOfCalls(t, "GetTransformations", 1)
					})
				})
			})
		})
	})
}
