package transformations

import (
	"context"

	log "code.justin.tv/commerce/logrus"
	bitsmessage "code.justin.tv/commerce/payday/message"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/prism_tenant/process_message/bits/sponsored_cheermotes"
	"code.justin.tv/commerce/payday/prism_tenant/process_message/bits/transformations/sponsored_cheermotes_bonus"
	prism "code.justin.tv/commerce/prism/rpc"
	prism_tenant "code.justin.tv/commerce/prism/rpc/tenant"
)

type TransformerErrorType string

const (
	TokenizationError       TransformerErrorType = "error_tokenization"
	SponsoredCheermoteError TransformerErrorType = "error_sponsored_cheermote"
)

type TransformerError struct {
	Error error
	Type  TransformerErrorType
}

type MessageTransformer interface {
	GetTransformations(ctx context.Context, req *prism_tenant.ProcessMessageReq) ([]*prism.Transformation, map[string]models.BitEmoteTotal, *TransformerError)
}

func NewMessageTransformer() MessageTransformer {
	return &messageTransformer{}
}

type messageTransformer struct {
	TokenizerFactory                          bitsmessage.ITokenizerFactory                                        `inject:""`
	SponsoredCheermoteValidator               sponsored_cheermotes.SponsoredCheermoteValidator                     `inject:""`
	SponsoredCheermoteBonusMessageTransformer sponsored_cheermotes_bonus.SponsoredCheermoteBonusMessageTransformer `inject:""`
}

func (t *messageTransformer) GetTransformations(ctx context.Context, req *prism_tenant.ProcessMessageReq) ([]*prism.Transformation, map[string]models.BitEmoteTotal, *TransformerError) {
	tokenizer, err := t.TokenizerFactory.NewTokenizer(ctx, req.ChannelId, req.UserId)
	if err != nil {
		return nil, nil, &TransformerError{
			Error: err,
			Type:  TokenizationError,
		}
	}

	_, _, _, emoteTotals, err := tokenizer.TokenizeWithEmoteOrder(req.Message)
	if err != nil {
		log.WithError(err).WithFields(log.Fields{
			"message":   req.Message,
			"userID":    req.UserId,
			"channelID": req.ChannelId,
		}).Error("Error tokenizing bits message")
		return nil, nil, &TransformerError{
			Error: err,
			Type:  TokenizationError,
		}
	}

	var transformations []*prism.Transformation
	emoteTotalsWithSponsoredCheermotes, err := t.SponsoredCheermoteValidator.AddAndValidateSponsoredCheermotes(ctx, emoteTotals, req.ChannelId, req.UserId)
	if err != nil {
		return nil, nil, &TransformerError{
			Error: err,
			Type:  SponsoredCheermoteError,
		}
	}

	// Sponsored cheermote bonus transformations
	sponsoredCheermoteBonusTransformations := t.SponsoredCheermoteBonusMessageTransformer.GetTransformations(emoteTotalsWithSponsoredCheermotes)
	transformations = append(transformations, sponsoredCheermoteBonusTransformations...)

	return transformations, emoteTotalsWithSponsoredCheermotes, nil
}
