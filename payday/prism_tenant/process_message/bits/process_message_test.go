package bits

import (
	"context"
	"errors"
	"fmt"
	"testing"

	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	"code.justin.tv/commerce/payday/clients/pantheon"
	balance_api_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/api/balance"
	pantheon_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/pantheon"
	pubsub_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/pubsub"
	event_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/event"
	metrics_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/metrics"
	partner_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/partner"
	datascience_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/prism_tenant/process_message/bits/datascience"
	transaction_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/prism_tenant/process_message/bits/transaction"
	transformations_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/prism_tenant/process_message/bits/transformations"
	sponsored_cheermote_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sponsored_cheermote"
	campaign_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sponsored_cheermote/campaign"
	throttle_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/throttle"
	"code.justin.tv/commerce/payday/models"
	apimodels "code.justin.tv/commerce/payday/models/api"
	"code.justin.tv/commerce/payday/partner"
	"code.justin.tv/commerce/payday/prism_tenant/process_message/bits/transformations"
	prism "code.justin.tv/commerce/prism/rpc"
	prism_models "code.justin.tv/commerce/prism/rpc/models"
	prism_tenant "code.justin.tv/commerce/prism/rpc/tenant"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
	"github.com/twitchtv/twirp"
)

func TestProcessMessage(t *testing.T) {
	Convey("Given a bits message processor", t, func() {
		channelID := "108706201"
		userID := "108706200"
		amount := 420
		newBitsBalance := int32(1000)
		bitsToBroadcasterBefore := int64(322)

		throttler := new(throttle_mock.IThrottler)
		statter := new(metrics_mock.Statter)
		pubsubUtils := new(pubsub_mock.IPubSubUtils)
		partnerManager := new(partner_mock.IPartnerManager)
		dataScienceTracker := new(datascience_mock.DataScienceTracker)
		messageTransformer := new(transformations_mock.MessageTransformer)
		sponsoredCheermoteIncrementor := new(sponsored_cheermote_mock.Incrementor)
		sponsoredCheermotePubSubber := new(campaign_mock.PubSubber)
		balanceGetter := new(balance_api_mock.Getter)
		giver := new(event_mock.PachinkoGiver)
		transactionCreator := new(transaction_mock.Creator)
		pantheonClient := new(pantheon_mock.IPantheonClientWrapper)

		api := &messageProcessorImpl{
			Throttler:                     throttler,
			Statter:                       statter,
			PubSubUtils:                   pubsubUtils,
			PartnerManager:                partnerManager,
			DataScienceTracker:            dataScienceTracker,
			MessageTransformer:            messageTransformer,
			SponsoredCheermoteIncrementor: sponsoredCheermoteIncrementor,
			SponsoredCheermotePubSubber:   sponsoredCheermotePubSubber,
			BalanceGetter:                 balanceGetter,
			Giver:                         giver,
			TransactionCreator:            transactionCreator,
			Pantheon:                      pantheonClient,
		}

		req := &prism_tenant.ProcessMessageReq{
			UserId:    userID,
			ChannelId: channelID,
			ConsumableAmounts: map[string]int64{
				prism_models.ConsumableType_Bits: int64(amount),
			},
			Message:   fmt.Sprintf("cheer%d", amount),
			MessageId: "test-message",
			Anonymous: false,
		}

		statter.On("TimingDuration", mock.Anything, mock.Anything).Return()
		statter.On("Inc", mock.Anything, mock.Anything).Return()
		pubsubUtils.On("GetNumberOfListeners", mock.Anything, mock.Anything, mock.Anything).Return(1, nil)
		dataScienceTracker.On("TrackBitsUsedErrorEvent", mock.Anything, mock.Anything, mock.Anything).Return()
		sponsoredCheermoteIncrementor.On("IncrementUsedBits", mock.Anything, mock.Anything, mock.Anything).Return()
		sponsoredCheermotePubSubber.On("SendTotalUpdates", mock.Anything, mock.Anything).Return()
		transactionCreator.On("CreateInFlightTransaction", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)

		Convey("When a duplicate transaction ID is throttled", func() {
			throttler.On("Throttle", mock.Anything, mock.Anything, mock.Anything).Return(true).Once()

			Convey("Then an error is returned", func() {
				res, err := api.Process(context.Background(), req)

				So(res, ShouldResemble, prism_tenant.ProcessMessageResp{})
				So(err, ShouldNotBeNil)
			})
		})

		Convey("When a duplicate transaction ID is NOT throttled", func() {
			throttler.On("Throttle", mock.Anything, mock.Anything, mock.Anything).Return(false)

			Convey("When the message transformer fails", func() {
				messageTransformer.On("GetTransformations", mock.Anything, mock.Anything).Return(nil, nil, &transformations.TransformerError{
					Type:  transformations.TokenizationError,
					Error: errors.New("e"),
				})

				Convey("Then an error is returned", func() {
					res, err := api.Process(context.Background(), req)

					So(res, ShouldResemble, prism_tenant.ProcessMessageResp{})
					So(err, ShouldNotBeNil)
				})
			})

			Convey("When the message transformer succeeds", func() {
				messageTransformer.On("GetTransformations", mock.Anything, mock.Anything).Return([]*prism.Transformation{}, map[string]models.BitEmoteTotal{}, nil)

				Convey("When getting the partner type fails", func() {
					partnerManager.On("GetPartnerType", mock.Anything, mock.Anything).Return(partner.UnknownPartnerType, errors.New("test error"))

					Convey("Then an error is returned", func() {
						res, err := api.Process(context.Background(), req)

						So(res, ShouldResemble, prism_tenant.ProcessMessageResp{})
						So(err, ShouldNotBeNil)
					})
				})

				Convey("When getting the partner type succeeds", func() {
					partnerManager.On("GetPartnerType", mock.Anything, mock.Anything).Return(partner.TraditionalPartnerType, nil)

					Convey("when the leaderboard fetch fails", func() {
						pantheonClient.On("GetLeaderboard", mock.Anything, pantheon.PantheonBitsUsageDomain, pantheonrpc.TimeUnit_ALLTIME, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("WALRUS STRIKE"))
					})

					Convey("when the leaderboard fetch succeeds", func() {
						pantheonClient.On("GetLeaderboard", mock.Anything, pantheon.PantheonBitsUsageDomain, pantheonrpc.TimeUnit_ALLTIME, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(&pantheonrpc.GetLeaderboardResp{
							EntryContext: &pantheonrpc.EntryContext{
								Entry: &pantheonrpc.LeaderboardEntry{
									EntryKey: req.UserId,
									Score:    bitsToBroadcasterBefore,
								},
							},
						}, nil)

						Convey("When the Pachinko Giver call fails because of insufficient balance", func() {
							giver.On("Give", mock.Anything, mock.Anything).Return(int32(0), models.InsufficientFunds, errors.New("WALRUS STRIKE"))

							Convey("Then an error is returned", func() {
								res, err := api.Process(context.Background(), req)

								So(res, ShouldResemble, prism_tenant.ProcessMessageResp{})
								So(err, ShouldNotBeNil)

								twirpErr, ok := err.(twirp.Error)
								So(ok, ShouldBeTrue)

								clientErr := apimodels.CreateEventErrorResponse{}
								parseErr := prism_models.ParseClientError(twirpErr, &clientErr)
								So(parseErr, ShouldBeNil)
								So(clientErr.Status, ShouldEqual, string(apimodels.InsufficientBalance))
							})
						})

						Convey("When the Pachinko Giver call fails because of duplicate transaction ID", func() {
							giver.On("Give", mock.Anything, mock.Anything).Return(int32(0), models.DuplicateEventId, errors.New("WALRUS STRIKE"))

							Convey("Then an error is returned", func() {
								res, err := api.Process(context.Background(), req)

								So(res, ShouldResemble, prism_tenant.ProcessMessageResp{})
								So(err, ShouldNotBeNil)

								twirpErr, ok := err.(twirp.Error)
								So(ok, ShouldBeTrue)

								clientErr := apimodels.CreateEventErrorResponse{}
								parseErr := prism_models.ParseClientError(twirpErr, &clientErr)
								So(parseErr, ShouldBeNil)
								So(clientErr.Status, ShouldEqual, string(apimodels.InvalidEventId))
							})
						})

						Convey("When the Pachinko Giver call fails because of an invalid cheer", func() {
							giver.On("Give", mock.Anything, mock.Anything).Return(int32(0), models.InvalidCheer, errors.New("WALRUS STRIKE"))

							Convey("Then an error is returned", func() {
								res, err := api.Process(context.Background(), req)

								So(res, ShouldResemble, prism_tenant.ProcessMessageResp{})
								So(err, ShouldNotBeNil)

								twirpErr, ok := err.(twirp.Error)
								So(ok, ShouldBeTrue)

								clientErr := apimodels.CreateEventErrorResponse{}
								parseErr := prism_models.ParseClientError(twirpErr, &clientErr)
								So(parseErr, ShouldBeNil)
								So(clientErr.Status, ShouldEqual, string(apimodels.InvalidBitsMessage))
							})
						})

						Convey("When the Pachinko Giver call fails", func() {
							giver.On("Give", mock.Anything, mock.Anything).Return(int32(0), models.BitsUnavailable, errors.New("test error"))

							Convey("Then an error is returned", func() {
								res, err := api.Process(context.Background(), req)

								So(res, ShouldResemble, prism_tenant.ProcessMessageResp{})
								So(err, ShouldNotBeNil)
							})
						})

						Convey("When the Pachinko Giver call succeeds", func() {
							giver.On("Give", mock.Anything, mock.Anything).Return(newBitsBalance, models.Default, nil)

							Convey("when we fail to update the transaction record", func() {
								transactionCreator.On("CreateTransactionFromProcessMessage", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(errors.New("WALRUS STRIKE"))

								Convey("Then an error is returned", func() {
									res, err := api.Process(context.Background(), req)

									So(res, ShouldResemble, prism_tenant.ProcessMessageResp{})
									So(err, ShouldNotBeNil)
								})
							})

							Convey("when we succeed to update the transaction record", func() {
								transactionCreator.On("CreateTransactionFromProcessMessage", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)

								Convey("When the request is NOT anonymous", func() {
									req.Anonymous = false

									Convey("Then a successful response is returned and EntitleBadgeTier is called", func() {
										res, err := api.Process(context.Background(), req)

										So(res.NewBalance[prism_models.ConsumableType_Bits], ShouldEqual, int64(newBitsBalance))
										So(err, ShouldBeNil)
									})
								})

								Convey("When the request is anonymous", func() {
									req.Anonymous = true

									Convey("Then a successful response is returned and EntitleBadgeTier is NOT called", func() {
										res, err := api.Process(context.Background(), req)

										So(res.NewBalance[prism_models.ConsumableType_Bits], ShouldEqual, int64(newBitsBalance))
										So(err, ShouldBeNil)
									})
								})
							})
						})
					})
				})
			})
		})
	})
}
