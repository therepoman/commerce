package models

import (
	"code.justin.tv/commerce/payday/models"
)

type FABSGetBalanceResult struct {
	Balance *models.Balance
	Error   error
}

type PubSubGetNumberOfListenersResult struct {
	NumberOfListeners int
	Error             error
}

type BadgeTierEntitleResult struct {
	BadgeTierEntitlement *models.BadgeTierEntitlement
	Error                error
}

func ConvertEmoteTotalsForFabs(emoteTotals map[string]models.BitEmoteTotal) map[string]models.Bits {
	fabsEmoteTotals := make(map[string]models.Bits)
	for emote, emoteTotal := range emoteTotals {
		fabsEmoteTotals[emote] = models.Bits(emoteTotal.CheeredTotal)
	}
	return fabsEmoteTotals
}

func CollectSponsoredCheermotesForFabs(emoteTotals map[string]models.BitEmoteTotal) map[string]models.Bits {
	fabsSponsoredTotals := make(map[string]models.Bits)
	for _, emoteTotal := range emoteTotals {
		if emoteTotal.Campaign != nil {
			fabsSponsoredTotals[emoteTotal.Campaign.CampaignID] = models.Bits(emoteTotal.MatchedTotal)
		}
	}

	return fabsSponsoredTotals
}
