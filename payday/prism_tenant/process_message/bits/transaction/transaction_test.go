package transaction

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/gogogadget/random"
	dynamo_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/partner"
	prism_models "code.justin.tv/commerce/prism/rpc/models"
	prism_tenant "code.justin.tv/commerce/prism/rpc/tenant"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestCreator_CreateTransaction(t *testing.T) {
	Convey("given a transaction creator", t, func() {
		dao := new(dynamo_mock.ITransactionsDao)
		inFlightDao := new(dynamo_mock.InFlightTransactionDao)

		c := creator{
			TransactionsDao:        dao,
			InFlightTransactionDao: inFlightDao,
		}

		ctx := context.Background()
		req := &prism_tenant.ProcessMessageReq{
			MessageId: random.String(32),
			UserId:    random.String(16),
			ChannelId: random.String(16),
			Message:   "Cheer1 Walrus are best",
			ConsumableAmounts: map[string]int64{
				prism_models.ConsumableType_Bits: int64(1),
			},
		}
		emoteTotals := map[string]models.BitEmoteTotal{
			"cheer": {
				CheeredTotal: int64(1),
			},
		}
		sponsoredAmounts := map[string]models.Bits{
			"walrus": models.Bits(3),
		}
		partnerType := partner.TraditionalPartnerType
		updatedBalance := int32(322)
		numChatters := "322"
		totalBitsToBroadcasterBefore := int64(322)

		Convey("when the transaction dao fails", func() {
			dao.On("CreateNew", mock.Anything).Return(errors.New("WALRUS STRIKE"))

			err := c.CreateTransactionFromProcessMessage(ctx, req, emoteTotals, sponsoredAmounts, partnerType, numChatters, updatedBalance, totalBitsToBroadcasterBefore)
			So(err, ShouldNotBeNil)
		})

		Convey("when the transaction dao returns conditional check exception", func() {
			dao.On("CreateNew", mock.Anything).Return(awserr.New(dynamodb.ErrCodeConditionalCheckFailedException, "WALRUS STRIKE", errors.New("WALRUS STRIKE")))

			err := c.CreateTransactionFromProcessMessage(ctx, req, emoteTotals, sponsoredAmounts, partnerType, numChatters, updatedBalance, totalBitsToBroadcasterBefore)
			So(err, ShouldNotBeNil)
			So(err, ShouldEqual, ErrTransactionExists)
		})

		Convey("when the transaction dao succeeds", func() {
			dao.On("CreateNew", mock.Anything).Return(nil)

			Convey("when inflight dao succeeds", func() {
				inFlightDao.On("RemoveTransaction", ctx, mock.Anything).Return(nil)

				err := c.CreateTransactionFromProcessMessage(ctx, req, emoteTotals, sponsoredAmounts, partnerType, numChatters, updatedBalance, totalBitsToBroadcasterBefore)
				So(err, ShouldBeNil)
			})

			Convey("when inflight dao fails", func() {
				inFlightDao.On("RemoveTransaction", ctx, mock.Anything).Return(errors.New("ANIME_BEAR_OVERLOAD"))

				err := c.CreateTransactionFromProcessMessage(ctx, req, emoteTotals, sponsoredAmounts, partnerType, numChatters, updatedBalance, totalBitsToBroadcasterBefore)
				So(err, ShouldBeNil)
			})
		})
	})
}
