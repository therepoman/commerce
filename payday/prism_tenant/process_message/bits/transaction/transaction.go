package transaction

import (
	"context"
	"errors"
	"time"

	"code.justin.tv/commerce/gogogadget/pointers"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/hystrix"
	cmds "code.justin.tv/commerce/payday/hystrix"
	"code.justin.tv/commerce/payday/middleware"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/oauth"
	"code.justin.tv/commerce/payday/partner"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	prism_models "code.justin.tv/commerce/prism/rpc/models"
	prism_tenant "code.justin.tv/commerce/prism/rpc/tenant"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

const (
	expirationTime = time.Hour
)

var ErrTransactionExists = errors.New("transaction was attempted to be created but already exists")

type Creator interface {
	CreateTransactionFromProcessMessage(ctx context.Context, req *prism_tenant.ProcessMessageReq, emoteTotals map[string]models.BitEmoteTotal, sponsoredAmounts map[string]models.Bits, partnerType partner.PartnerType, numChatters string, updatedBalance int32, totalBitsToBroadcasterBefore int64) error
	CreateTransactionFromMicroCheer(ctx context.Context, req *paydayrpc.MicroCheerReq, emoteTotals map[string]models.BitEmoteTotal, sponsoredAmounts map[string]models.Bits, partnerType partner.PartnerType, numChatters string, updatedBalance int32, totalBitsToBroadcasterBefore int64) error
	CreateInFlightTransaction(ctx context.Context, req *prism_tenant.ProcessMessageReq, emoteTotals map[string]models.BitEmoteTotal, sponsoredAmounts map[string]models.Bits, partnerType partner.PartnerType, numChatters string, totalBitsToBroadcasterBefore int64) error
}

type creator struct {
	TransactionsDao        dynamo.ITransactionsDao       `inject:""`
	InFlightTransactionDao dynamo.InFlightTransactionDao `inject:""`
}

func NewCreator() Creator {
	return &creator{}
}

func (c *creator) CreateInFlightTransaction(ctx context.Context, req *prism_tenant.ProcessMessageReq, emoteTotals map[string]models.BitEmoteTotal, sponsoredAmounts map[string]models.Bits, partnerType partner.PartnerType, numChatters string, totalBitsToBroadcasterBefore int64) error {
	transaction := convertProcessMessageToTransaction(ctx, req, emoteTotals, sponsoredAmounts, partnerType, numChatters, 0, totalBitsToBroadcasterBefore, true)
	return c.InFlightTransactionDao.CreateTransaction(ctx, transaction)
}

func convertProcessMessageToTransaction(ctx context.Context, req *prism_tenant.ProcessMessageReq, emoteTotals map[string]models.BitEmoteTotal, sponsoredAmounts map[string]models.Bits, partnerType partner.PartnerType, numChatters string, updatedBalance int32, totalBitsToBroadcasterBefore int64, inFlight bool) dynamo.Transaction {
	bitsAmount := req.ConsumableAmounts[prism_models.ConsumableType_Bits]
	bitsAmountSponsored := int64(0)

	clientID := middleware.GetClientID(ctx)
	platform, _ := oauth.GetClient(clientID)

	emoteTotalsInt64 := make(map[string]int64)
	for prefix, total := range emoteTotals {
		emoteTotalsInt64[prefix] = total.CheeredTotal
		bitsAmountSponsored += total.MatchedTotal
	}

	sponsoredAmountInt64 := make(map[string]int64)
	for prefix, total := range sponsoredAmounts {
		sponsoredAmountInt64[prefix] = int64(total)
	}

	// Set our expiration to 1 hour, should be plenty of time.
	expiration := time.Now().Add(expirationTime).Unix()
	var expiresAt *int64
	if inFlight {
		expiresAt = &expiration
	}

	transaction := dynamo.Transaction{
		TransactionId:             req.MessageId,
		RawMessage:                &req.Message,
		ChannelId:                 &req.ChannelId,
		UserId:                    &req.UserId,
		NumberOfBits:              &bitsAmount,
		NumberOfBitsSponsored:     &bitsAmountSponsored,
		TransactionType:           pointers.StringP(models.BitsEventTransactionType),
		ClientApp:                 &platform,
		EmoteTotals:               emoteTotalsInt64,
		ClientID:                  &clientID,
		IsAnonymous:               pointers.BoolP(req.Anonymous),
		PartnerType:               pointers.StringP(string(partnerType)),
		NumChatters:               pointers.StringP(numChatters),
		TimeOfEvent:               time.Now(),
		LastUpdated:               time.Now(),
		ExpiresAt:                 expiresAt,
		TotalBitsAfterTransaction: pointers.Int64P(int64(updatedBalance)),
		TotalBitsToBroadcaster:    pointers.Int64P(totalBitsToBroadcasterBefore + bitsAmount),
		SponsoredAmounts:          sponsoredAmountInt64,
		IsMicroCheer:              false,
	}

	if req.GeoIpMetadata != nil {
		transaction.CountryCode = pointers.StringP(req.GetGeoIpMetadata().ClientCountryCode)
		transaction.ZipCode = pointers.StringP(req.GetGeoIpMetadata().ClientPostalCode)
		transaction.City = pointers.StringP(req.GetGeoIpMetadata().ClientCity)
		transaction.IPAddress = pointers.StringP(req.GetGeoIpMetadata().ClientIpAddress)
	}
	return transaction
}

func convertMicroCheerToTransaction(ctx context.Context, req *paydayrpc.MicroCheerReq, emoteTotals map[string]models.BitEmoteTotal, sponsoredAmounts map[string]models.Bits, partnerType partner.PartnerType, numChatters string, updatedBalance int32, totalBitsToBroadcasterBefore int64, inFlight bool) dynamo.Transaction {
	bitsAmountSponsored := int64(0)

	clientID := middleware.GetClientID(ctx)
	platform, _ := oauth.GetClient(clientID)

	emoteTotalsInt64 := make(map[string]int64)
	for prefix, total := range emoteTotals {
		emoteTotalsInt64[prefix] = total.CheeredTotal
		bitsAmountSponsored += total.MatchedTotal
	}

	sponsoredAmountInt64 := make(map[string]int64)
	for prefix, total := range sponsoredAmounts {
		sponsoredAmountInt64[prefix] = int64(total)
	}

	// Set our expiration to 1 hour, should be plenty of time.
	expiration := time.Now().Add(expirationTime).Unix()
	var expiresAt *int64
	if inFlight {
		expiresAt = &expiration
	}

	transaction := dynamo.Transaction{
		TransactionId:             req.TransactionId,
		ChannelId:                 &req.ChannelId,
		UserId:                    &req.UserId,
		NumberOfBits:              &req.Quantity,
		NumberOfBitsSponsored:     &bitsAmountSponsored,
		TransactionType:           pointers.StringP(models.BitsEventTransactionType),
		ClientApp:                 &platform,
		EmoteTotals:               emoteTotalsInt64,
		ClientID:                  &clientID,
		IsAnonymous:               pointers.BoolP(false),
		PartnerType:               pointers.StringP(string(partnerType)),
		NumChatters:               pointers.StringP(numChatters),
		TimeOfEvent:               time.Now(),
		LastUpdated:               time.Now(),
		ExpiresAt:                 expiresAt,
		TotalBitsAfterTransaction: pointers.Int64P(int64(updatedBalance)),
		TotalBitsToBroadcaster:    pointers.Int64P(totalBitsToBroadcasterBefore + req.Quantity),
		SponsoredAmounts:          sponsoredAmountInt64,
		IsMicroCheer:              true,
	}
	return transaction
}

func (c *creator) CreateTransactionFromProcessMessage(ctx context.Context, req *prism_tenant.ProcessMessageReq, emoteTotals map[string]models.BitEmoteTotal, sponsoredAmounts map[string]models.Bits, partnerType partner.PartnerType, numChatters string, updatedBalance int32, totalBitsToBroadcasterBefore int64) error {
	if req == nil {
		return errors.New("request cannot be nil")
	}

	transaction := convertProcessMessageToTransaction(ctx, req, emoteTotals, sponsoredAmounts, partnerType, numChatters, updatedBalance, totalBitsToBroadcasterBefore, false)
	return c.createTransaction(ctx, transaction)
}

func (c *creator) CreateTransactionFromMicroCheer(ctx context.Context, req *paydayrpc.MicroCheerReq, emoteTotals map[string]models.BitEmoteTotal, sponsoredAmounts map[string]models.Bits, partnerType partner.PartnerType, numChatters string, updatedBalance int32, totalBitsToBroadcasterBefore int64) error {
	if req == nil {
		return errors.New("request cannot be nil")
	}

	transaction := convertMicroCheerToTransaction(ctx, req, emoteTotals, sponsoredAmounts, partnerType, numChatters, updatedBalance, totalBitsToBroadcasterBefore, false)
	return c.createTransaction(ctx, transaction)
}

func (c *creator) createTransaction(ctx context.Context, transaction dynamo.Transaction) error {
	recordExists := false
	err := hystrix.Do(cmds.UpdateTransactionsCommand, func() error {
		err := c.TransactionsDao.CreateNew(&transaction)
		if err != nil {
			if awsErr, ok := err.(awserr.Error); ok && awsErr.Code() == dynamodb.ErrCodeConditionalCheckFailedException {
				recordExists = true
				return nil
			}
			return err
		}
		return nil
	}, nil)

	if err != nil {
		log.WithError(err).WithField("transaction", transaction).Error("failed to save transaction record to dynamo")
		return err
	}

	if recordExists {
		log.WithError(err).WithField("transaction", transaction).Error("transaction was attempted that already exists")
		return ErrTransactionExists
	}

	c.InFlightTransactionDao.RemoveTransaction(ctx, transaction.TransactionId)

	return nil
}
