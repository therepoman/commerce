package bits

import (
	"context"
	"fmt"
	"strconv"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	"code.justin.tv/commerce/payday/api/balance"
	"code.justin.tv/commerce/payday/api/constants"
	"code.justin.tv/commerce/payday/clients/pantheon"
	"code.justin.tv/commerce/payday/clients/pubsub"
	"code.justin.tv/commerce/payday/event"
	event_models "code.justin.tv/commerce/payday/event/models"
	"code.justin.tv/commerce/payday/metrics"
	"code.justin.tv/commerce/payday/middleware"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/models/api"
	"code.justin.tv/commerce/payday/partner"
	"code.justin.tv/commerce/payday/prism_tenant/process_message"
	"code.justin.tv/commerce/payday/prism_tenant/process_message/bits/datascience"
	process_models "code.justin.tv/commerce/payday/prism_tenant/process_message/bits/models"
	"code.justin.tv/commerce/payday/prism_tenant/process_message/bits/transaction"
	"code.justin.tv/commerce/payday/prism_tenant/process_message/bits/transformations"
	"code.justin.tv/commerce/payday/sponsored_cheermote"
	"code.justin.tv/commerce/payday/sponsored_cheermote/campaign"
	"code.justin.tv/commerce/payday/throttle"
	prism_models "code.justin.tv/commerce/prism/rpc/models"
	prism_tenant "code.justin.tv/commerce/prism/rpc/tenant"
	"github.com/twitchtv/twirp"
)

func NewMessageProcessor() process_message.ProcessMessage {
	return &messageProcessorImpl{}
}

type messageProcessorImpl struct {
	Throttler                     throttle.IThrottler                `inject:""`
	Statter                       metrics.Statter                    `inject:""`
	PubSubUtils                   pubsub.IPubSubUtils                `inject:""`
	PartnerManager                partner.IPartnerManager            `inject:""`
	DataScienceTracker            datascience.DataScienceTracker     `inject:""`
	MessageTransformer            transformations.MessageTransformer `inject:""`
	SponsoredCheermoteIncrementor sponsored_cheermote.Incrementor    `inject:""`
	SponsoredCheermotePubSubber   campaign.PubSubber                 `inject:""`
	BalanceGetter                 balance.Getter                     `inject:""`
	Giver                         event.PachinkoGiver                `inject:"pachinkoGiver"`
	TransactionCreator            transaction.Creator                `inject:""`
	Pantheon                      pantheon.IPantheonClientWrapper    `inject:""`
}

func (m *messageProcessorImpl) Process(ctx context.Context, req *prism_tenant.ProcessMessageReq) (prism_tenant.ProcessMessageResp, error) {
	var errorType models.ErrorType
	errCtx, errCancel := middleware.CopyContextValues(ctx)
	defer func() {
		go func() {
			defer errCancel()
			if errorType != models.Default {
				m.DataScienceTracker.TrackBitsUsedErrorEvent(errCtx, req, errorType)
			}
		}()
	}()

	bitsAmount := req.ConsumableAmounts[prism_models.ConsumableType_Bits]

	numChattersChannel := make(chan process_models.PubSubGetNumberOfListenersResult)
	go func() {
		numChatters, err := m.PubSubUtils.GetNumberOfListeners(ctx, pubsub.ChannelListenersPubSubTopic, req.ChannelId)
		numChattersChannel <- process_models.PubSubGetNumberOfListenersResult{
			NumberOfListeners: numChatters,
			Error:             err,
		}
	}()

	duplicateIDThrottleCheckTimeStart := time.Now()
	duplicateIDThrottled := m.Throttler.Throttle(ctx, fmt.Sprintf("%s.%s.eventID", req.UserId, req.MessageId), 5*time.Minute)
	go m.Statter.TimingDuration("GiveBitsStep_TransactionDedupeThrottler", time.Since(duplicateIDThrottleCheckTimeStart))

	if duplicateIDThrottled {
		errorType = models.RequestThrottled
		return prism_tenant.ProcessMessageResp{}, twirp.InternalError(constants.RequestThrottledError)
	}

	transformed, emoteTotals, transErr := m.MessageTransformer.GetTransformations(ctx, req)
	if transErr != nil {
		errorType = models.BitsUnavailable

		switch transErr.Type {
		case transformations.SponsoredCheermoteError:
			return prism_tenant.ProcessMessageResp{}, twirp.InvalidArgumentError("message", "contains invalid sponsored cheermotes amount")
		case transformations.TokenizationError:
			return prism_tenant.ProcessMessageResp{}, twirp.InternalError(constants.DownstreamDependencyError)
		}
	}

	clientID := middleware.GetClientID(ctx)

	numChattersResult := <-numChattersChannel
	var numChattersString string
	if numChattersResult.Error != nil {
		log.Warn("Error retrieving number of chatters.")
		numChattersString = "err"
	} else {
		numChattersString = strconv.Itoa(numChattersResult.NumberOfListeners)
	}

	partnerType, err := m.PartnerManager.GetPartnerType(ctx, req.ChannelId)
	if err != nil {
		log.WithField("channel_id", req.ChannelId).WithError(err).Info("Could not get partner type")
		return prism_tenant.ProcessMessageResp{}, twirp.InternalError(constants.DownstreamDependencyError)
	}

	leaderboardEntry, err := m.Pantheon.GetLeaderboard(ctx, pantheon.PantheonBitsUsageDomain, pantheonrpc.TimeUnit_ALLTIME, req.ChannelId, req.UserId, 1, 1, false)
	if err != nil {
		log.WithError(err).Error("failed to fetch leaderboard for transaction creation on cheer")
		return prism_tenant.ProcessMessageResp{}, twirp.InternalError(constants.DownstreamDependencyError)
	}
	totalBitsToBroadcasterBefore := int64(0)
	if leaderboardEntry != nil && leaderboardEntry.GetEntryContext() != nil && leaderboardEntry.GetEntryContext().GetEntry() != nil {
		totalBitsToBroadcasterBefore = leaderboardEntry.GetEntryContext().GetEntry().Score
	}

	sponsoredAmounts := process_models.CollectSponsoredCheermotesForFabs(emoteTotals)
	giveBitsParams := event_models.Request{
		UserID:           req.UserId,
		ChannelID:        req.ChannelId,
		Amount:           models.Bits(bitsAmount),
		Message:          req.Message,
		EventID:          req.MessageId,
		EventReasonCode:  models.EventReasonCheer,
		EmoteUses:        process_models.ConvertEmoteTotalsForFabs(emoteTotals),
		ClientID:         clientID,
		PartnerType:      partnerType,
		SponsoredAmounts: sponsoredAmounts,
		NumChatters:      numChattersString,
		IsAnonymous:      req.Anonymous,
	}

	if req.GeoIpMetadata != nil {
		giveBitsParams.IPAddress = req.GeoIpMetadata.ClientIpAddress
		giveBitsParams.CountryCode = req.GeoIpMetadata.ClientCountryCode
		giveBitsParams.City = req.GeoIpMetadata.ClientCity
		giveBitsParams.PostalCode = req.GeoIpMetadata.ClientPostalCode
	}

	err = m.TransactionCreator.CreateInFlightTransaction(ctx, req, emoteTotals, sponsoredAmounts, partnerType, numChattersString, totalBitsToBroadcasterBefore)
	if err != nil {
		log.WithError(err).WithField("transaction_id", req.MessageId).Error("Failure to call pre operation dynamo write.")
		return prism_tenant.ProcessMessageResp{}, twirp.InternalError("Failure to call pre operation dynamo write.")
	}

	updatedBalance, errorType, err := m.Giver.Give(ctx, giveBitsParams)

	if err != nil {
		if errorType == models.InsufficientFunds {
			return prism_tenant.ProcessMessageResp{}, prism_models.NewClientError(
				twirp.NewError(twirp.InvalidArgument, constants.InsufficientBalanceError),
				api.CreateEventErrorResponse{
					Status:  api.InsufficientBalance,
					Message: constants.InsufficientBalanceError,
				})
		}

		if errorType == models.DuplicateEventId {
			return prism_tenant.ProcessMessageResp{}, prism_models.NewClientError(
				twirp.InvalidArgumentError("message_id", "Request contained an existing eventId"),
				api.CreateEventErrorResponse{
					Status:  api.InvalidEventId,
					Message: constants.InvalidEventIdError,
				})
		} else if errorType == models.InvalidCheer {
			return prism_tenant.ProcessMessageResp{}, prism_models.NewClientError(
				twirp.InvalidArgumentError("message", "Request was invalidated by downstream dependency"),
				api.CreateEventErrorResponse{
					Status:  api.InvalidBitsMessage,
					Message: constants.InvalidBitsMessageError,
				})
		} else {
			errorType = models.BitsUnavailable
			log.WithError(err).Error("Error calling GiveBitsToBroadcaster")
			return prism_tenant.ProcessMessageResp{}, twirp.InternalError("Encountered unknown error from downstream dependency")
		}
	}

	m.Throttler.Throttle(ctx, fmt.Sprintf("%s.%s.entitle", req.UserId, req.ChannelId), 5*time.Minute)

	m.SponsoredCheermoteIncrementor.IncrementUsedBits(ctx, req.UserId, emoteTotals)
	go m.SponsoredCheermotePubSubber.SendTotalUpdates(context.Background(), emoteTotals)

	err = m.TransactionCreator.CreateTransactionFromProcessMessage(ctx, req, emoteTotals, sponsoredAmounts, partnerType, numChattersString, updatedBalance, totalBitsToBroadcasterBefore)
	if err != nil {
		return prism_tenant.ProcessMessageResp{}, twirp.InternalError("could not record transaction in dynamo")
	}

	return prism_tenant.ProcessMessageResp{
		NewBalance: map[string]int64{
			prism_models.ConsumableType_Bits: int64(updatedBalance),
		},
		ConsumableAmountsProcessed: map[string]int64{
			prism_models.ConsumableType_Bits: int64(bitsAmount),
		},
		Transformations:  transformed,
		MessageTreatment: string(prism_models.DEFAULT_MESSAGE),
	}, nil
}
