package sns

import (
	"encoding/json"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/sns"
	prism_models "code.justin.tv/commerce/prism/rpc/models"
	prism_tenant "code.justin.tv/commerce/prism/rpc/tenant"
)

type SNSPublisher interface {
	PublishToBitsUsageSNSTopic(req *prism_tenant.PostProcessMessageReq, emoteTotals map[string]models.BitEmoteTotal)
}

type BitsUsageSNS struct {
	ChannelID   string           `json:"channel_id"`
	UserID      string           `json:"user_id"`
	EventID     string           `json:"event_id"`
	Message     string           `json:"message"`
	Amount      int32            `json:"amount"`
	EmoteTotals map[string]int64 `json:"emote_totals"`
	IsAnonymous bool             `json:"is_anonymous"`
}

func NewSNSPublisher() SNSPublisher {
	return &snsPublisher{}
}

type snsPublisher struct {
	SNSClient sns.ISNSClient        `inject:"usWestSNSClient"`
	Config    *config.Configuration `inject:""`
}

func (p *snsPublisher) PublishToBitsUsageSNSTopic(req *prism_tenant.PostProcessMessageReq, emoteTotals map[string]models.BitEmoteTotal) {
	if req == nil {
		return
	}

	emoteTotalsInt64 := make(map[string]int64)
	for prefix, emoteTotal := range emoteTotals {
		emoteTotalsInt64[prefix] = emoteTotal.CheeredTotal
	}

	jobSNS := &BitsUsageSNS{
		ChannelID:   req.ChannelId,
		UserID:      req.UserId,
		EventID:     req.MessageId,
		Message:     req.SentMessage,
		Amount:      int32(req.ConsumableAmounts[prism_models.ConsumableType_Bits]),
		EmoteTotals: emoteTotalsInt64,
		IsAnonymous: req.Anonymous,
	}
	snsJSON, err := json.Marshal(jobSNS)
	if err != nil {
		msg := "Failed to marshal BitsUsage msg into json to send to SNS"
		log.WithError(err).Error(msg)
		return
	}

	err = p.SNSClient.PostToTopicWithMessageAttributes(p.Config.BitsUsageSNSTopic, string(snsJSON), sns.MessageAttributesV1)
	if err != nil {
		msg := "Failed to post BitsUsage message to topic"
		log.WithError(err).WithFields(log.Fields{
			"snsTopic": p.Config.BitsUsageSNSTopic,
			"eventID":  req.MessageId,
		}).Error(msg)
	}
}
