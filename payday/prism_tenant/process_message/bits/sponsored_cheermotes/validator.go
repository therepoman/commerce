package sponsored_cheermotes

import (
	"context"
	"errors"
	"math"
	"sort"
	"strings"

	dynamo_campaigns "code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/campaigns"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/sponsored_cheermote/campaign"
	"code.justin.tv/commerce/payday/sponsored_cheermote/channel_status"
	"code.justin.tv/commerce/payday/sponsored_cheermote/user_campaign_status"
)

type SponsoredCheermoteValidator interface {
	AddAndValidateSponsoredCheermotes(ctx context.Context, cheermoteTotals map[string]int64, channelID, userID string) (map[string]models.BitEmoteTotal, error)
}

func NewSponsoredCheermoteValidator() SponsoredCheermoteValidator {
	return &sponsoredCheermoteValidator{}
}

type sponsoredCheermoteValidator struct {
	SponsoredCheermotePubSubber  campaign.PubSubber          `inject:""`
	SponsoredCheermoteUserGetter user_campaign_status.Getter `inject:""`
	SponsoredCheermoteFilterer   channel_status.Filterer     `inject:""`
	SponsoredCheermoteManager    campaign.Manager            `inject:""`
}

func (v *sponsoredCheermoteValidator) AddAndValidateSponsoredCheermotes(ctx context.Context, cheermoteTotals map[string]int64, channelID, userID string) (map[string]models.BitEmoteTotal, error) {
	filteredCampaigns := v.getSponsoredCampaignByPrefix(ctx, channelID)

	sponsoredCheermoteAmounts := make(map[string]models.BitEmoteTotal)

	// Failures:
	// 1 - if there are no more bits error ✔️
	// 2 - if matched amount is more than bits available (should overlap with 1) ✔️
	// 3 - if mobile and any non-zero value ✔️
	// 4 - if user has cheered too much and hit the user limit ✔️
	// 5 - if amount of bits is below minimum to cheer ✔️

	for prefix, total := range cheermoteTotals {
		bitEmoteTotal := models.BitEmoteTotal{
			CheeredTotal: total,
		}

		campaign, ok := filteredCampaigns[strings.ToLower(prefix)]

		if ok {
			if total < campaign.MinBitsToBeSponsored {
				return map[string]models.BitEmoteTotal{}, errors.New("cheer didn't have enough to be sponsored")
			}

			sponsoredAmount := v.calculateSponsoredAmount(total, campaign)

			// TODO: add buffer here for going a little over.
			if sponsoredAmount+campaign.UsedBits >= campaign.TotalBits {
				err := v.SponsoredCheermotePubSubber.CompleteCampaign(ctx, campaign.CampaignID)
				if err != nil {
					return map[string]models.BitEmoteTotal{}, err
				}
				return map[string]models.BitEmoteTotal{}, errors.New("not enough bits in pool to support cheer")
			}

			// TODO: add buffer here for going a very small amount over
			userUsedBits, err := v.SponsoredCheermoteUserGetter.Get(ctx, campaign.CampaignID, userID)
			if err != nil {
				return map[string]models.BitEmoteTotal{}, err
			}

			if userUsedBits > campaign.UserLimit {
				return map[string]models.BitEmoteTotal{}, errors.New("not enough bits in pool to support cheer")
			}

			bitEmoteTotal.MatchedTotal = sponsoredAmount
			bitEmoteTotal.Campaign = campaign
		}
		sponsoredCheermoteAmounts[prefix] = bitEmoteTotal
	}

	return sponsoredCheermoteAmounts, nil
}

func (v *sponsoredCheermoteValidator) getSponsoredCampaignByPrefix(ctx context.Context, channelID string) map[string]*dynamo_campaigns.SponsoredCheermoteCampaign {
	filteredCampaigns, err := v.SponsoredCheermoteFilterer.Filter(ctx, v.SponsoredCheermoteManager.GetActiveCampaigns(ctx), channelID)
	if err != nil {
		return map[string]*dynamo_campaigns.SponsoredCheermoteCampaign{}
	}

	campaignsByPrefix := make(map[string]*dynamo_campaigns.SponsoredCheermoteCampaign)
	for _, campaign := range filteredCampaigns {
		campaignsByPrefix[strings.ToLower(campaign.Prefix)] = campaign
	}

	return campaignsByPrefix
}

func (v *sponsoredCheermoteValidator) calculateSponsoredAmount(amount int64, campaign *dynamo_campaigns.SponsoredCheermoteCampaign) int64 {
	sponsoredAmount := int64(0)
	thresholds := make([]int, 0, len(campaign.SponsoredAmountThresholds))
	for threshold := range campaign.SponsoredAmountThresholds {
		thresholds = append(thresholds, int(threshold))
	}
	sort.Ints(thresholds)

	for _, threshold := range thresholds {
		if amount >= int64(threshold) {
			percentage := campaign.SponsoredAmountThresholds[int64(threshold)]
			sponsoredAmount = int64(math.Floor(float64(amount) * percentage))
		}
	}

	return sponsoredAmount
}
