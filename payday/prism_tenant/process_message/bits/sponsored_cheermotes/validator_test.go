package sponsored_cheermotes

import (
	"context"
	"errors"
	"testing"

	dynamo_campaigns "code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/campaigns"
	campaign_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sponsored_cheermote/campaign"
	channel_status_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sponsored_cheermote/channel_status"
	user_campaign_status_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sponsored_cheermote/user_campaign_status"
	"code.justin.tv/commerce/payday/models"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestProcessMessage(t *testing.T) {
	Convey("Given a ProcessMessage server", t, func() {
		channelID := "108706201"
		userID := "108706200"
		cheermoteTotals := map[string]int64{
			"doritos": 100,
		}

		campaign := &dynamo_campaigns.SponsoredCheermoteCampaign{
			Prefix:               "doritos",
			MinBitsToBeSponsored: 100,
			TotalBits:            300,
			UsedBits:             100,
			UserLimit:            150,
			SponsoredAmountThresholds: map[int64]float64{
				100: 1.0,
			},
		}

		sponsoredCheermotePubSubber := new(campaign_mock.PubSubber)
		sponsoredCheermoteManager := new(campaign_mock.Manager)
		sponsoredCheermoteFilterer := new(channel_status_mock.Filterer)
		sponsoredCheermoteUserGetter := new(user_campaign_status_mock.Getter)

		validator := &sponsoredCheermoteValidator{
			SponsoredCheermotePubSubber:  sponsoredCheermotePubSubber,
			SponsoredCheermoteFilterer:   sponsoredCheermoteFilterer,
			SponsoredCheermoteManager:    sponsoredCheermoteManager,
			SponsoredCheermoteUserGetter: sponsoredCheermoteUserGetter,
		}

		sponsoredCheermoteManager.On("GetActiveCampaigns", mock.Anything).Return(nil)
		sponsoredCheermotePubSubber.On("CompleteCampaign", mock.Anything, mock.Anything).Return(nil)

		Convey("When getting sponsored campaigns fails", func() {
			sponsoredCheermoteFilterer.On("Filter", mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("test error"))

			Convey("Then a response is returned with no sponsored cheermote data populated", func() {
				res, err := validator.AddAndValidateSponsoredCheermotes(context.Background(), cheermoteTotals, channelID, userID)

				So(err, ShouldBeNil)
				So(res, ShouldResemble, map[string]models.BitEmoteTotal{
					"doritos": {
						CheeredTotal: 100,
					},
				})
			})
		})

		Convey("When getting sponsored campaigns succeeds", func() {
			sponsoredCheermoteFilterer.On("Filter", mock.Anything, mock.Anything, mock.Anything).Return([]*dynamo_campaigns.SponsoredCheermoteCampaign{campaign}, nil)

			Convey("When the channel can use sponsored cheermotes", func() {
				Convey("When the sponsored cheermote cheered bits is not above the campaign's min bits", func() {
					cheermoteTotals = map[string]int64{
						"doritos": 50,
					}

					Convey("Then an error is returned", func() {
						res, err := validator.AddAndValidateSponsoredCheermotes(context.Background(), cheermoteTotals, channelID, userID)

						So(err, ShouldNotBeNil)
						So(res, ShouldResemble, map[string]models.BitEmoteTotal{})
					})
				})

				Convey("When the sponsored cheermote cheered bits is above the campaign's min bits", func() {
					cheermoteTotals = map[string]int64{
						"doritos": 100,
					}

					Convey("When the cheered bits cause the campaign used bits to exceed the campaign total bits", func() {
						cheermoteTotals = map[string]int64{
							"doritos": 200,
						}

						Convey("Then an error is returned, and the campaign gets completed", func() {
							res, err := validator.AddAndValidateSponsoredCheermotes(context.Background(), cheermoteTotals, channelID, userID)

							So(err, ShouldNotBeNil)
							So(res, ShouldResemble, map[string]models.BitEmoteTotal{})
							sponsoredCheermotePubSubber.AssertNumberOfCalls(t, "CompleteCampaign", 1)
						})
					})

					Convey("When the cheered bits cause the campaign used bits to stay below the campaign total bits", func() {
						cheermoteTotals = map[string]int64{
							"doritos": 100,
						}

						Convey("When the user getter fails", func() {
							sponsoredCheermoteUserGetter.On("Get", mock.Anything, mock.Anything, mock.Anything).Return(int64(0), errors.New("test error"))

							Convey("Then an error is returned", func() {
								res, err := validator.AddAndValidateSponsoredCheermotes(context.Background(), cheermoteTotals, channelID, userID)

								So(err, ShouldNotBeNil)
								So(res, ShouldResemble, map[string]models.BitEmoteTotal{})
							})
						})

						Convey("When the user getter succeeds", func() {
							Convey("When the user's cheer has exceeded the user bits limit", func() {
								sponsoredCheermoteUserGetter.On("Get", mock.Anything, mock.Anything, mock.Anything).Return(int64(151), nil)

								Convey("Then an error is returned", func() {
									res, err := validator.AddAndValidateSponsoredCheermotes(context.Background(), cheermoteTotals, channelID, userID)

									So(err, ShouldNotBeNil)
									So(res, ShouldResemble, map[string]models.BitEmoteTotal{})
								})
							})

							Convey("When the user's cheer has NOT exceeded the user bits limit", func() {
								sponsoredCheermoteUserGetter.On("Get", mock.Anything, mock.Anything, mock.Anything).Return(int64(100), nil)

								Convey("Then a successful response is returned with populated cheermote data", func() {
									res, err := validator.AddAndValidateSponsoredCheermotes(context.Background(), cheermoteTotals, channelID, userID)

									So(err, ShouldBeNil)
									So(res, ShouldResemble, map[string]models.BitEmoteTotal{
										"doritos": {
											CheeredTotal: 100,
											MatchedTotal: 100,
											Campaign:     campaign,
										},
									})
								})
							})
						})
					})
				})
			})
		})
	})
}
