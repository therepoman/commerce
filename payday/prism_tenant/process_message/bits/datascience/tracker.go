package datascience

import (
	"context"
	"strconv"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/balance"
	"code.justin.tv/commerce/payday/datascience"
	"code.justin.tv/commerce/payday/models"
	prism_models "code.justin.tv/commerce/prism/rpc/models"
	prism_tenant "code.justin.tv/commerce/prism/rpc/tenant"
)

type DataScienceTracker interface {
	TrackBitsUsedErrorEvent(ctx context.Context, req *prism_tenant.ProcessMessageReq, errorType models.ErrorType)
}

func NewDataScienceTracker() DataScienceTracker {
	return &dataScienceTracker{}
}

type dataScienceTracker struct {
	DataScienceClient datascience.DataScience `inject:""`
	BalanceGetter     balance.Getter          `inject:""`
}

func (d *dataScienceTracker) TrackBitsUsedErrorEvent(ctx context.Context, req *prism_tenant.ProcessMessageReq, errorType models.ErrorType) {
	bitsUsedError := &models.BitsUsedError{
		UserID:            req.UserId,
		ChannelID:         req.ChannelId,
		QuantityAttempted: int32(req.ConsumableAmounts[prism_models.ConsumableType_Bits]),
	}

	switch errorType {
	default:
		bitsUsedError.ErrorType = models.BitsUnavailableError
	case models.InsufficientFunds:
		response, err := d.BalanceGetter.GetBalance(ctx, req.UserId, true)
		if err != nil {
			bitsUsedError.ErrorType = models.BitsUnavailableError
		} else {
			bitsUsedError.CurrentBalance = strconv.Itoa(int(response))
			bitsUsedError.ErrorType = models.InsufficientFundsError
		}
	case models.DuplicateEventId:
		bitsUsedError.ErrorType = models.DuplicateEventIdError
	case models.InvalidCheer:
		bitsUsedError.ErrorType = models.InvalidCheerError
	case models.RequestThrottled:
		bitsUsedError.ErrorType = models.RequestThrottledError
	}

	bitsUsedError.Context = models.CheerContext
	bitsUsedError.ContextDetails = req.Message

	err := d.DataScienceClient.TrackBitsEvent(ctx, datascience.BitsUsedErrorEventName, bitsUsedError)
	if err != nil {
		log.WithError(err).WithField("BitsUsedErrorEvent", datascience.BitsUsedErrorEventName).Error("Could not send track bits event")
	}
}
