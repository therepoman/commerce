package process_message

import (
	"context"

	prism_tenant "code.justin.tv/commerce/prism/rpc/tenant"
)

type ProcessMessage interface {
	Process(ctx context.Context, req *prism_tenant.ProcessMessageReq) (prism_tenant.ProcessMessageResp, error)
}
