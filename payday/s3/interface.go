package s3

import (
	"context"
	"time"
)

type IS3Client interface {
	CreateBucket(bucketName string) error
	EnsureBucketExists(bucketName string) error
	UploadFileToBucket(bucketName string, fileKey string, file []byte) error
	FileExists(bucket string, key string) (bool, error)
	BucketExists(targetBucket string) (bool, error)
	ListFileNames(bucket string, prefix string) ([]string, error)
	PaginatedListFileNames(bucket string, prefix string, continuationKey *string) ([]string, *string, error)
	WaitForNFiles(bucket string, prefix string, numFiles int, timeout time.Duration) ([]string, error)
	LoadFile(bucket string, fileName string) ([]byte, error)
	DeleteFile(bucket string, key string) error
	DeleteFiles(bucket string, keys ...string) error
	CopyFile(ctx context.Context, bucket, from, to string) error
}
