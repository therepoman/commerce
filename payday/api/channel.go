package api

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	"code.justin.tv/commerce/payday/api/clienttracking"
	"code.justin.tv/commerce/payday/api/constants"
	"code.justin.tv/commerce/payday/api/utils"
	"code.justin.tv/commerce/payday/badgetiers/fetcher"
	"code.justin.tv/commerce/payday/badgetiers/logician"
	"code.justin.tv/commerce/payday/cache/channel"
	"code.justin.tv/commerce/payday/cartman"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/datascience"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/emote"
	"code.justin.tv/commerce/payday/errors"
	httperror "code.justin.tv/commerce/payday/errors/http"
	leaderboardUtils "code.justin.tv/commerce/payday/leaderboard/utils"
	"code.justin.tv/commerce/payday/middleware"
	"code.justin.tv/commerce/payday/models"
	apimodel "code.justin.tv/commerce/payday/models/api"
	"code.justin.tv/commerce/payday/notification"
	"code.justin.tv/commerce/payday/onboard"
	partnerManager "code.justin.tv/commerce/payday/partner"
	"code.justin.tv/commerce/payday/payout"
	"code.justin.tv/commerce/payday/sns"
	"code.justin.tv/commerce/payday/utils/pointers"
	timeUtils "code.justin.tv/commerce/payday/utils/time"
	"github.com/zenazn/goji/web"
)

// APIs regarding bits channel

type ChannelApi struct {
	ChatBadgeFetcher  fetcher.Fetcher                `inject:""`
	ChatBadgeLogician logician.Logician              `inject:""`
	ChannelManager    channel.ChannelManager         `inject:""`
	CartmanValidator  cartman.IValidator             `inject:""`
	PayoutManager     payout.IPayoutManager          `inject:""`
	DataScienceClient datascience.DataScience        `inject:""`
	PrefixManager     emote.IPrefixManager           `inject:""`
	PartnerManager    partnerManager.IPartnerManager `inject:""`
	OnboardManager    onboard.IOnboardManager        `inject:""`
	Config            *config.Configuration          `inject:""`
	Notification      notification.INotification     `inject:""`
	SNSClient         sns.ISNSClient                 `inject:"usWestSNSClient"`
	ClientTracking    clienttracking.Tracker         `inject:""`
}

const (
	channelNotFoundError         = "Channel attribute not found"
	minMinBits                   = 1
	maxMinBits                   = 100000
	minRecentCheer               = 1
	maxRecentCheer               = 100000
	minRecentCheerTimeoutSeconds = 5
	maxRecentCheerTimeoutSeconds = 300
)

func (channelApi *ChannelApi) GetSettingsWithCartmanAuthentication(c web.C, w http.ResponseWriter, r *http.Request) error {
	ctx, cancel := context.WithTimeout(r.Context(), utils.DefaultTimeout)
	defer cancel()

	channelId := c.URLParams["tuid"]
	partnerType, err := channelApi.validateGetSettingsRequest(ctx, channelId)
	if err != nil {
		return err
	}

	err = channelApi.CartmanValidator.ValidateAuthenticatedUserID(r, channelId, true, true)
	if err != nil {
		log.Warnf("Cartman token could not be validated for getting channel settings")
		return err
	}

	return channelApi.processGetSettings(ctx, w, channelId, partnerType)
}

func (channelApi *ChannelApi) validateGetSettingsRequest(ctx context.Context, channelId string) (partnerManager.PartnerType, error) {
	err := utils.ValidateRequestUserId(channelId)
	if err != nil {
		return "", err
	}

	partnerType, err := channelApi.PartnerManager.GetPartnerType(ctx, channelId)
	if err != nil {
		msg := fmt.Sprintf("Error getting partner type for channel: %s", channelId)
		log.WithField("channel_id", channelId).WithError(err).Error("Could Not Get partner for channel")
		return "", errors.Notef(err, msg)
	}

	return partnerType, nil
}

func (channelApi *ChannelApi) processGetSettings(ctx context.Context, w http.ResponseWriter, channelId string, partnerType partnerManager.PartnerType) error {
	channel, err := channelApi.getChannelFromDynamo(ctx, channelId)
	if err != nil {
		return err
	}

	hasServices, err := channelApi.PayoutManager.HasServicesTaxInfo(ctx, channelId)

	if err != nil {
		return httperror.NewWithCause("Could not retrieve eligibility", http.StatusInternalServerError, err)
	}

	amendmentState := true
	if channel != nil {
		isExplicitlyUnsigned := pointers.StringOrDefault(channel.AmendmentState, string(partnerManager.BitsAmendmentStateSigned)) == string(partnerManager.BitsAmendmentStateUnsigned)
		if isExplicitlyUnsigned {
			amendmentState = false
		}
	}

	prefix, hasCustomCheermoteAction, err := channelApi.PrefixManager.GetCustomCheermoteAction(ctx, channelId)
	if !hasCustomCheermoteAction {
		prefix = ""
	} else if err != nil {
		log.WithError(err).Error("Failed to load prefixes")
		return httperror.New(constants.DownstreamDependencyError, http.StatusInternalServerError)
	}

	showNotification := channelApi.Notification.InExperimentGroup(channelId)

	response := models.NewGetSettingsResponse(channel, true, amendmentState, hasServices, prefix, partnerType, showNotification)
	utils.WriteResponse(response, w)

	return nil
}

func (channelApi *ChannelApi) UpdateSettingsWithCartmanAuthentication(c web.C, w http.ResponseWriter, r *http.Request) error {
	ctx := r.Context()
	channelId := c.URLParams["tuid"]
	err := utils.ValidateRequestUserId(channelId)
	if err != nil {
		return err
	}

	err = channelApi.CartmanValidator.ValidateAuthenticatedUserID(r, channelId, true, true)
	if err != nil {
		log.Warnf("Cartman token could not be validated for updating channel settings")
		return err
	}

	return channelApi.processUpdateSettings(ctx, channelId, w, r)
}

func (channelApi *ChannelApi) processUpdateSettings(ctx context.Context, channelId string, w http.ResponseWriter, r *http.Request) error {
	request, err := models.ExtractUpdateSettingsRequest(r)
	if err != nil {
		log.WithError(err).Error("Error decoding JSON request for UpdateSettings call")
		return httperror.NewWithCause(constants.InvalidJsonError, http.StatusBadRequest, err)
	}

	err = validateMinBits(request.MinBits)
	if err != nil {
		return err
	}

	err = validateMinBits(request.MinBitsEmote)
	if err != nil {
		return err
	}

	err = validatePinRecentMin(request.RecentCheerMin)
	if err != nil {
		return err
	}

	err = validatePinRecentTimeout(request.RecentCheerTimeout)
	if err != nil {
		return err
	}

	err = validateLeaderboardTimePeriod(request.LeaderboardTimePeriod)
	if err != nil {
		return err
	}

	err = validateDefaultLeaderboard(request.DefaultLeaderboard)
	if err != nil {
		return err
	}

	// Because the caller may not pass in all options in the request we may need to check against existing settings too
	currentChannel, err := channelApi.getChannelFromDynamo(ctx, channelId)
	if err != nil {
		return err
	}

	// If the user doesn't have any settings, use the request as the current settings
	if currentChannel == nil {
		currentChannel = models.UpdateSettingsRequestToDynamoChannel(request, channelId)
	}

	// Next, onboard the partner if requested and they are not currently onboarded
	if pointers.BoolOrDefault(request.Onboarded, false) && !pointers.BoolOrDefault(currentChannel.Onboarded, false) {
		err := channelApi.OnboardManager.OnboardForBits(r.Context(), channelId)
		if err != nil {
			log.WithError(err).WithField("channelID", channelId).Error("Failed to onboard channel for bits")
			return httperror.NewWithCause(constants.DownstreamDependencyError, http.StatusInternalServerError, err)
		}

		currentChannel.Onboarded = pointers.BoolP(true)
	} else if request.Onboarded != nil && !*request.Onboarded {
		return httperror.NewWithCause(constants.InvalidOffboardError, http.StatusBadRequest, nil)
	}

	desiredMinBits := pointers.Int64OrDefault(request.MinBits, pointers.Int64OrDefault(currentChannel.MinBits, apimodel.DefaultMinBits))
	desiredMinBitsEmote := pointers.Int64OrDefault(request.MinBitsEmote, pointers.Int64OrDefault(currentChannel.MinBitsEmote, apimodel.DefaultMinBits))

	if desiredMinBits < desiredMinBitsEmote {
		log.Warnf("Received request to set min bits (%d) lower than min bits emote (%d)", desiredMinBits, desiredMinBitsEmote)
		return httperror.NewWithCause(constants.MinBitsBelowMinBitsEmoteError, http.StatusBadRequest, err)
	}

	desiredPinTop := pointers.BoolOrDefault(request.PinTopCheers, pointers.BoolOrDefault(currentChannel.PinTopCheers, apimodel.DefaultPinTopCheers))
	desiredPinRecent := pointers.BoolOrDefault(request.PinRecentCheers, pointers.BoolOrDefault(currentChannel.PinRecentCheers, apimodel.DefaultPinRecentCheers))
	desiredRecentTimeout := pointers.Int64OrDefault(request.RecentCheerTimeout, pointers.Int64OrDefault(currentChannel.RecentCheerTimeout, apimodel.DefaultRecentCheerTimeoutSeconds))

	if desiredPinTop && desiredPinRecent && desiredRecentTimeout == 0 {
		log.Warnf("Received request to enable top and recent cheers with no timeout")
		return httperror.NewWithCause(constants.TopAndRecentRequiresDuration, http.StatusBadRequest, err)
	}

	updatedChannel := models.UpdateSettingsRequestToDynamoChannel(request, channelId)
	err = channelApi.ChannelManager.Update(ctx, updatedChannel)

	if err != nil {
		log.WithError(err).Error("Error updating dynamo for UpdateMinBits call")
		return httperror.NewWithCause(constants.DownstreamDependencyError, http.StatusInternalServerError, err)
	}

	ctx, cancel := middleware.CopyContextValues(r.Context())
	go func() {
		defer cancel()
		event := &models.BitsChannelSettings{
			ChannelID:                   string(updatedChannel.Id),
			PinCheerFeatureEnabled:      updatedChannel.PinRecentCheers,
			TopCheerFeatureEnabled:      updatedChannel.PinTopCheers,
			PinRecentCheerMin:           updatedChannel.RecentCheerMin,
			PinRecentCheerTimeout:       updatedChannel.RecentCheerTimeout,
			MinimumBits:                 updatedChannel.MinBits,
			MinimumBitsEmote:            updatedChannel.MinBitsEmote,
			LeaderboardEnabled:          updatedChannel.LeaderboardEnabled,
			IsCheerLeaderboardEnabled:   updatedChannel.IsCheerLeaderboardEnabled,
			IsSubGiftLeaderboardEnabled: updatedChannel.IsSubGiftLeaderboardEnabled,
			DefaultLeaderboard:          updatedChannel.DefaultLeaderboard,
			LeaderboardTimePeriod:       updatedChannel.LeaderboardTimePeriod,
			CheerBombEventOptOut:        updatedChannel.CheerBombEventOptOut,
			Onboarded:                   updatedChannel.Onboarded,
			OptedOutOfVoldemort:         updatedChannel.OptedOutOfVoldemort,
		}
		err = channelApi.DataScienceClient.TrackBitsEvent(ctx, datascience.BitsChannelSettingsEventName, event)
		if err != nil {
			log.WithError(err).Error("Error sending channel settings update to data science")
		}
	}()

	go func() {
		channelSettingsUpdateMessage := models.ChannelSettingsMessage{
			ChannelId:                   string(updatedChannel.Id),
			LeaderboardEnabled:          updatedChannel.LeaderboardEnabled,
			IsCheerLeaderboardEnabled:   updatedChannel.IsCheerLeaderboardEnabled,
			IsSubGiftLeaderboardEnabled: updatedChannel.IsSubGiftLeaderboardEnabled,
			DefaultLeaderboard:          updatedChannel.DefaultLeaderboard,
			LeaderboardTimePeriod:       updatedChannel.LeaderboardTimePeriod,
			MinimumBits:                 updatedChannel.MinBits,
			MinimumBitsEmote:            updatedChannel.MinBitsEmote,
			CheerBombEventOptOut:        updatedChannel.CheerBombEventOptOut,
		}

		messageJSON, err := json.Marshal(channelSettingsUpdateMessage)
		if err != nil {
			log.WithError(err).Error("Error marshaling channel settings update json")
		}

		err = channelApi.SNSClient.PostToTopic(channelApi.Config.ChannelSettingsSNSTopic, string(messageJSON))
		if err != nil {
			log.WithError(err).Error("Error posting channel settings update to SNS")
		}
	}()

	return nil
}

func (channelApi *ChannelApi) GetMinBits(c web.C, w http.ResponseWriter, r *http.Request) error {
	// P99.9 As of 2020-02-07 is ~100ms, with some spikes up to 450ms.
	ctx, cancel := context.WithTimeout(r.Context(), 500*time.Millisecond)
	defer cancel()

	channelId := c.URLParams["tuid"]
	err := utils.ValidateRequestUserId(channelId)
	if err != nil {
		return err
	}

	channel, err := channelApi.getChannelFromDynamo(ctx, channelId)
	if err != nil {
		return err
	}

	if channel != nil && channel.MinBits != nil {
		response := models.NewGetMinBitsResponse(channel)
		utils.WriteResponse(response, w)
	} else {
		return httperror.New(channelNotFoundError, http.StatusNotFound)
	}

	return nil
}

func (channelApi *ChannelApi) SetMinBits(c web.C, w http.ResponseWriter, r *http.Request) error {
	// P99.9 As of 2020-02-07 is ~40ms, with very low request volume.
	ctx, cancel := context.WithTimeout(r.Context(), 100*time.Millisecond)
	defer cancel()

	channelId := c.URLParams["tuid"]
	err := utils.ValidateRequestUserId(channelId)
	if err != nil {
		return err
	}

	request, err := models.ExtractSetMinBitsRequest(r)
	if err != nil {
		log.WithError(err).Error("Error decoding JSON request for SetMinBits call")
		return httperror.NewWithCause(constants.InvalidJsonError, http.StatusBadRequest, err)
	}

	err = validateMinBits(pointers.Int64P(request.MinBits))
	if err != nil {
		return err
	}

	updatedChannel := models.SetMinBitsRequestToDynamoChannel(request, channelId)
	err = channelApi.ChannelManager.Update(ctx, updatedChannel)

	if err != nil {
		log.WithError(err).Error("Error updating dynamo for SetMinBits call")
		return httperror.NewWithCause(constants.DownstreamDependencyError, http.StatusInternalServerError, err)
	}

	return nil
}

func (channelApi *ChannelApi) GetOptOut(c web.C, w http.ResponseWriter, r *http.Request) error {
	// P99.9 As of 2020-02-07 is ~100ms, with some spikes up to 450ms.
	ctx, cancel := context.WithTimeout(r.Context(), 500*time.Millisecond)
	defer cancel()

	channelId := c.URLParams["tuid"]
	err := utils.ValidateRequestUserId(channelId)
	if err != nil {
		return err
	}

	channel, err := channelApi.getChannelFromDynamo(ctx, channelId)
	if err != nil {
		return err
	}

	if channel != nil && channel.OptedOut != nil {
		response := models.NewGetOptOutResponse(channel)
		utils.WriteResponse(response, w)
	} else {
		return httperror.New(channelNotFoundError, http.StatusNotFound)
	}

	return nil
}

func (channelApi *ChannelApi) SetOptOut(c web.C, w http.ResponseWriter, r *http.Request) error {
	// P99.9 As of 2020-02-07 is ~40ms, with very low request volume.
	ctx, cancel := context.WithTimeout(r.Context(), 100*time.Millisecond)
	defer cancel()

	channelId := c.URLParams["tuid"]
	err := utils.ValidateRequestUserId(channelId)
	if err != nil {
		return err
	}

	request, err := models.ExtractSetOptOutRequest(r)
	if err != nil {
		log.WithError(err).Error("Error decoding JSON request for SetOptOut call")
		return httperror.NewWithCause(constants.InvalidJsonError, http.StatusBadRequest, err)
	}

	updatedChannel := models.SetOptOutRequestToDynamoChannel(request, channelId)
	err = channelApi.ChannelManager.Update(ctx, updatedChannel)

	if err != nil {
		log.WithError(err).Error("Error updating dynamo for SetOptOut call")
		return httperror.NewWithCause(constants.DownstreamDependencyError, http.StatusInternalServerError, err)
	}

	return nil
}

type SGDQResponse struct {
	Total int64 `json:"total"`
}

type ExtraLifeResponse struct {
	Total string `json:"total"`
}

func (channelApi *ChannelApi) GetChannelInfo(c web.C, w http.ResponseWriter, r *http.Request) error {
	ctx, cancel := context.WithTimeout(r.Context(), time.Millisecond*500)
	defer cancel()

	go channelApi.ClientTracking.Track(ctx, clienttracking.GetChannelInfo)

	channelId := c.URLParams["tuid"]
	err := utils.ValidateRequestUserId(channelId)
	if err != nil {
		return err
	}

	enabledBitsBadgeTiersChan := make(chan []int64, 1)
	errChan := make(chan error)

	// Run getChannelEnabledBitsBadgeTiers in parallel with getChannelFromDynamo
	go func() {
		enabledBitsBadgeTiers, err := channelApi.getChannelEnabledBitsBadgeTiers(ctx, channelId)
		if err != nil {
			errChan <- err
			return
		}

		enabledBitsBadgeTiersChan <- enabledBitsBadgeTiers
	}()

	ch, err := channelApi.getChannelFromDynamo(ctx, channelId)
	if err != nil {
		return err
	}

	eligible := channelApi.getChannelEligibility(ch)

	var enabledBitsBadgeTiers []int64

	response := &apimodel.ChannelEligibleResponse{
		Eligible:                    eligible,
		MinimumBits:                 int64(1),
		MinimumBitsEmote:            int64(1),
		EnabledBitsBadgeTiers:       enabledBitsBadgeTiers,
		RecentCheerMin:              int64(1),
		LeaderboardEnabled:          apimodel.DefaultLeaderboardEnabled,
		IsCheerLeaderboardEnabled:   apimodel.DefaultLeaderboardEnabled,
		IsSubGiftLeaderboardEnabled: apimodel.DefaultLeaderboardEnabled,
		DefaultLeaderboard:          apimodel.DefaultLeaderboard,
		LeaderboardTimePeriod:       models.DefaultLeaderboardTimePeriod,
		CheerBombEventOptOut:        apimodel.DefaultCheerBombEventOptOut,
		OptedOutOfVoldemort:         apimodel.DefaultOptedOutOfVoldemort,
		PollsEnabled:                true,
	}

	if !eligible {
		utils.WriteResponse(response, w)
		return nil
	}

	minBits := channelApi.getChannelMinBits(ch)
	response.MinimumBits = minBits

	minBitsEmote := channelApi.getChannelMinBitsEmote(ch)
	response.MinimumBitsEmote = minBitsEmote

	pinTopCheers := channelApi.getChannelPinTopCheers(ch)
	response.PinTopCheers = pinTopCheers

	pinRecentCheers := channelApi.getChannelPinRecentCheers(ch)
	response.PinRecentCheers = pinRecentCheers

	recentCheerMin := channelApi.getChannelRecentCheerMin(ch)
	response.RecentCheerMin = recentCheerMin

	recentCheerTimeoutMs := channelApi.getChannelRecentCheerTimeoutInMs(ch)
	response.RecentCheerTimeoutMs = recentCheerTimeoutMs

	leaderboardSettings := channelApi.getChannelLeaderboardSettings(ch)
	response.LeaderboardEnabled = leaderboardSettings.LeaderboardEnabled
	response.IsCheerLeaderboardEnabled = leaderboardSettings.IsCheerLeaderboardEnabled
	response.IsSubGiftLeaderboardEnabled = leaderboardSettings.IsSubGiftLeaderboardEnabled
	response.DefaultLeaderboard = leaderboardSettings.DefaultLeaderboard
	response.LeaderboardTimePeriod = leaderboardSettings.LeaderboardTimePeriod

	cheerBombEventOptOut := channelApi.getChannelCheerBombEventOptOut(ch)
	response.CheerBombEventOptOut = cheerBombEventOptOut

	optedOutOfVoldemort := channelApi.getChannelOptedOutOfVoldemort(ch)
	response.OptedOutOfVoldemort = optedOutOfVoldemort

	// wait for response from getChannelEnabledBitsBadgeTiers
	select {
	case enabledBitsBadgeTiers := <-enabledBitsBadgeTiersChan:
		response.EnabledBitsBadgeTiers = enabledBitsBadgeTiers
	case err := <-errChan:
		return err
	case <-ctx.Done():
		msg := "timed out getting enabled bits badge tiers"
		return errors.New(msg)
	}

	utils.WriteResponse(response, w)
	return nil
}

func (channelApi *ChannelApi) getChannelFromDynamo(ctx context.Context, userId string) (*dynamo.Channel, error) {
	channel, err := channelApi.ChannelManager.Get(ctx, userId)
	if err != nil {
		log.WithError(err).WithField("channel_id", userId).Error("Error getting channel from dynamo")
		return nil, httperror.NewWithCause(constants.DownstreamDependencyError, http.StatusInternalServerError, err)
	}
	return channel, nil
}

func (channelApi *ChannelApi) getChannelEligibility(ch *dynamo.Channel) bool {
	return channel.GetChannelEligibility(ch)
}

func (channelApi *ChannelApi) getChannelMinBits(ch *dynamo.Channel) int64 {
	return channel.GetChannelMinBits(ch)
}

func (channelApi *ChannelApi) getChannelMinBitsEmote(ch *dynamo.Channel) int64 {
	return channel.GetChannelMinBitsEmote(ch)
}

func (channelApi *ChannelApi) getChannelEnabledBitsBadgeTiers(ctx context.Context, userId string) ([]int64, error) {
	chatBitsBadgeTiers, err := channelApi.ChatBadgeFetcher.Fetch(ctx, userId)
	if err != nil {
		log.WithError(err).Error("Error getting bits badge tiers from cache chatBadgesFetcher")
		return nil, httperror.NewWithCause(constants.DownstreamDependencyError, http.StatusInternalServerError, err)
	}

	enabledTiers := channelApi.ChatBadgeLogician.GetEnabledBadgeTiers(chatBitsBadgeTiers.BadgeTiers, chatBitsBadgeTiers.ChannelId)

	return enabledTiers, nil
}

func (channelApi *ChannelApi) getChannelPinTopCheers(ch *dynamo.Channel) bool {
	return channel.GetChannelPinTopCheers(ch)
}

func (channelApi *ChannelApi) getChannelPinRecentCheers(ch *dynamo.Channel) bool {
	return channel.GetChannelPinRecentCheers(ch)
}

func (channelApi *ChannelApi) getChannelRecentCheerMin(ch *dynamo.Channel) int64 {
	return channel.GetChannelRecentCheerMin(ch)
}

func (channelApi *ChannelApi) getChannelRecentCheerTimeoutInMs(ch *dynamo.Channel) int64 {
	return timeUtils.NanosecondsToMilliseconds(channel.GetChannelRecentCheerTimeout(ch))
}

func (channelApi *ChannelApi) getChannelLeaderboardSettings(ch *dynamo.Channel) *apimodel.LeaderboardSettings {
	return channel.GetChannelLeaderboardSettings(ch)
}

func (channelApi *ChannelApi) getChannelCheerBombEventOptOut(ch *dynamo.Channel) bool {
	return channel.GetChannelCheerBombEventOptOut(ch)
}

func (channelApi *ChannelApi) getChannelOptedOutOfVoldemort(ch *dynamo.Channel) bool {
	return channel.GetChannelOptedOutOfVoldemort(ch)
}

func validateMinBits(minBits *int64) error {
	if minBits != nil && (*minBits < minMinBits || *minBits > maxMinBits) {
		log.Warnf("Received invalid min bits value %d. Min bits must be between %d and %d", minBits, minMinBits, maxMinBits)
		return httperror.New(constants.MinBitsOutsideRangeError, http.StatusBadRequest)
	}
	return nil
}

func validatePinRecentMin(recentMin *int64) error {
	if recentMin != nil && (*recentMin < minRecentCheer || *recentMin > maxRecentCheer) {
		log.Warnf("Received invalid min recent pinned cheer value %d. Must be between %d and %d.", *recentMin, minRecentCheer, maxRecentCheer)
		return httperror.New(constants.MinRecentOutsideRangeError, http.StatusBadRequest)
	}
	return nil
}

func validatePinRecentTimeout(recentTimeout *int64) error {
	if recentTimeout != nil && *recentTimeout != 0 && (*recentTimeout < minRecentCheerTimeoutSeconds || *recentTimeout > maxRecentCheerTimeoutSeconds) {
		log.Warnf("Received invalid recent pinned cheer timeout value %d. Must be 0 or between %d and %d.", *recentTimeout, minRecentCheerTimeoutSeconds, maxRecentCheerTimeoutSeconds)
		return httperror.New(constants.RecentTimeoutOutsideRangeError, http.StatusBadRequest)
	}
	return nil
}

func validateLeaderboardTimePeriod(leaderboardTimePeriod *string) error {
	if leaderboardTimePeriod != nil {
		tp, ok := pantheonrpc.TimeUnit_value[*leaderboardTimePeriod]
		if !ok {
			log.WithField("time_period", *leaderboardTimePeriod).Warnf("Received invalid leaderboard time period")
			return httperror.New(constants.InvalidLeaderboardSettingError, http.StatusBadRequest)
		}

		if !leaderboardUtils.IsSupportedTimeUnit(pantheonrpc.TimeUnit(tp)) {
			log.WithField("time_period", *leaderboardTimePeriod).Warnf("Received leaderboard time period that is currently not supported")
			return httperror.New(constants.InvalidLeaderboardSettingError, http.StatusBadRequest)
		}
	}

	return nil
}

func validateDefaultLeaderboard(defaultLeaderboard *string) error {
	if defaultLeaderboard != nil {
		_, ok := apimodel.DefaultLeaderboard_value[*defaultLeaderboard]
		if !ok {
			log.WithField("default_leaderboard", *defaultLeaderboard).Warnf("Received invalid default leaderboard")
			return httperror.New(constants.InvalidLeaderboardSettingError, http.StatusBadRequest)
		}
	}

	return nil
}
