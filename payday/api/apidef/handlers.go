package apidef

import (
	"net/http"

	"code.justin.tv/commerce/payday/errors"
	"github.com/zenazn/goji/web"
)

type HandlerName string

const (
	// APIs moved off NGINX to Visage
	GetBalanceVisage            HandlerName = "GetBalanceVisage"
	GetUserInfoVisage           HandlerName = "GetUserInfoVisage"
	GetPricesVisage             HandlerName = "GetPricesVisage"
	GetChannelInfoVisage        HandlerName = "GetChannelInfoVisage"
	GetChannelSettingsVisage    HandlerName = "GetChannelSettingsVisage"
	UpdateChannelSettingsVisage HandlerName = "UpdateChannelSettingsVisage"
	GetBadgeTiersVisage         HandlerName = "GetBadgeTiersVisage"
	SetBadgeTiersVisage         HandlerName = "SetBadgeTiersVisage"
	StartCustomCheermoteUpload  HandlerName = "StartCustomCheermoteUpload"
	GetCustomCheermotes         HandlerName = "GetCustomCheermotes"

	// Public APIs
	GetPrices   HandlerName = "GetPrices"
	GetProducts HandlerName = "GetProducts"

	GetChannelSettings    HandlerName = "GetChannelSettings"
	UpdateChannelSettings HandlerName = "UpdateChannelSettings"
	GetChannelInfo        HandlerName = "GetChannelInfo"
	GetUserInfo           HandlerName = "GetUserInfo"
	GetBitsEventsForUser  HandlerName = "GetBitsEventsForUser"

	GetActions     HandlerName = "GetActions"
	GetActionAsset HandlerName = "GetActionAsset"
	GetActionsInfo HandlerName = "GetActionsInfo"

	GetBadgeTiers HandlerName = "GetBadgeTiers"
	SetBadgeTiers HandlerName = "SetBadgeTiers"

	DownloadChannelImages HandlerName = "DownloadChannelImages"

	GetChannelLeaderboard HandlerName = "GetBitsUsageLeaderboard"

	// Internal (non-Admin) API
	EntitleBits HandlerName = "EntitleBits"

	// Admin (Internal) APIs
	AdminAddBits    HandlerName = "AdminAddBits"
	AdminRemoveBits HandlerName = "AdminRemoveBits"

	AdminGetBadgeTiers HandlerName = "AdminGetBadgeTiers"
	AdminSetBadgeTiers HandlerName = "AdminSetBadgeTiers"

	AdminGetChannelMinBits HandlerName = "AdminGetChannelMinBits"
	AdminSetChannelMinBits HandlerName = "AdminSetChannelMinBits"
	AdminGetChannelOptOut  HandlerName = "AdminGetChannelOptOut"
	AdminSetChannelOptOut  HandlerName = "AdminSetChannelOptOut"
	AdminGetChannelInfo    HandlerName = "AdminGetChannelInfo"

	AdminGetUserBanned HandlerName = "AdminGetUserBanned"
	AdminSetUserBanned HandlerName = "AdminSetUserBanned"
	AdminGetUserInfo   HandlerName = "AdminGetUserInfo"

	AdminGetBalance            HandlerName = "AdminGetBalance"
	AdminGetBitsEventsForUser  HandlerName = "AdminGetBitsEventsForUser"
	AdminGetBroadcasterRevenue HandlerName = "AdminGetBroadcasterRevenue"

	GetWithholdingByAccountId      HandlerName = "GetWithholdingByAccountId"
	GetWithholdingByPayoutEntityId HandlerName = "GetWithholdingByPayoutEntityId"

	AdminModerateChannelImages HandlerName = "AdminModerateChannelImages"

	GetAdminJobsByCreator HandlerName = "GetAdminJobsByCreator"
	GetAdminJobInput      HandlerName = "GetAdminJobInput"
	GetAdminJobOptions    HandlerName = "GetAdminJobOptions"
	GetAdminJobOutput     HandlerName = "GetAdminJobOutput"
	StartAdminJob         HandlerName = "StartAdminJob"
	StartBulkEntitleJob   HandlerName = "StartBulkEntitleJob"
)

type NamedHandler interface {
	GetHandlerName() HandlerName
}

type TestNamedHandler struct {
	Name HandlerName
}

func (t TestNamedHandler) GetHandlerName() HandlerName {
	return t.Name
}

func (t TestNamedHandler) ServeHTTPC(web.C, http.ResponseWriter, *http.Request) {
	// no op
}

func GetNamedHandler(c *web.C) (NamedHandler, web.Pattern, bool, error) {
	match := web.GetMatch(*c)
	if match.Handler != nil {
		namedHandler, isNamed := match.Handler.(NamedHandler)
		return namedHandler, match.Pattern, isNamed, nil
	}
	return nil, nil, false, errors.New("Matched handler for request was nil")
}

func IsMatch(namedHandler NamedHandler, namesToCheck ...HandlerName) bool {
	handlerName := namedHandler.GetHandlerName()
	for _, nameToCheck := range namesToCheck {
		if handlerName == nameToCheck {
			return true
		}
	}
	return false
}
