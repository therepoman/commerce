package api

import (
	"context"
	"fmt"
	"net/http"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/actions"
	actionsUtils "code.justin.tv/commerce/payday/actions/utils"
	"code.justin.tv/commerce/payday/api/constants"
	"code.justin.tv/commerce/payday/api/utils"
	"code.justin.tv/commerce/payday/api/validation"
	"code.justin.tv/commerce/payday/cache/channel"
	"code.justin.tv/commerce/payday/emote"
	"code.justin.tv/commerce/payday/errors"
	httperror "code.justin.tv/commerce/payday/errors/http"
	"code.justin.tv/commerce/payday/image"
	"code.justin.tv/commerce/payday/models/api"
	"code.justin.tv/commerce/payday/utils/pointers"
	bits_strings "code.justin.tv/commerce/payday/utils/strings"
	"github.com/zenazn/goji/web"
)

type ActionAssetApi struct {
	ActionCache    *actions.ActionCache   `inject:""`
	ChannelManager channel.ChannelManager `inject:""`
	ImageManager   image.IImageManager    `inject:""`
	PrefixManager  emote.IPrefixManager   `inject:""`
}

func (actionApi *ActionAssetApi) GetActionAsset(c web.C, w http.ResponseWriter, r *http.Request) error {
	ctx, cancel := utils.ContextWithDefaultTimeout(r.Context())
	defer cancel()

	request, err := validation.CreateActionAssetRequest(r.URL.Query())
	if err != nil {
		return err
	}

	var actionResponse *api.GetActionAssetResponse
	if request.ChannelID != "" {
		actionResponse, err = actionApi.getChannelAction(ctx, request.ChannelID, request)
	} else {
		actionResponse, err = actionApi.ActionCache.GetActionAsset(request)
	}

	if err != nil {
		actionType := "global"
		if request.ChannelID == "" {
			actionType = "channel"
		}
		log.WithFields(log.Fields{
			"prefix":     request.Prefix,
			"background": request.Background,
			"scale":      request.Scale,
			"state":      request.State,
			"bits":       request.Bits,
			"channelId":  request.ChannelID,
		}).WithError(err).Error(fmt.Sprintf("Could not get %s action", actionType))
		return httperror.NewWithCause(constants.BadMessage, http.StatusBadRequest, err)
	}

	utils.WriteResponse(actionResponse, w)
	return nil
}

func (actionApi *ActionAssetApi) getChannelAction(ctx context.Context, channelId string, options api.GetActionAssetOptions) (*api.GetActionAssetResponse, error) {
	channel, err := actionApi.ChannelManager.Get(ctx, channelId)
	if err != nil {
		log.WithError(err).Warn("Error looking up channel from dynamo")
		return nil, err
	}

	if channel == nil {
		return nil, nil
	}

	customCheermoteImageSetId := pointers.StringOrDefault(channel.CustomCheermoteImageSetId, "")
	customCheermoteStatus := pointers.StringOrDefault(channel.CustomCheermoteStatus, api.DefaultCustomCheermoteStatus)
	customCheermoteEnabledInChannel := customCheermoteStatus == api.CustomCheermoteStatusEnabled

	if !customCheermoteEnabledInChannel || customCheermoteImageSetId == "" {
		return nil, nil
	}

	images, err := actionApi.ImageManager.GetImages(ctx, customCheermoteImageSetId)
	if err != nil {
		log.WithError(err).Warn("Failed to get images")
		return nil, err
	}

	if !images.AllImagesSet() {
		return nil, nil
	}

	backgrounds, states, scales := bits_strings.NewSet(), bits_strings.NewSet(), bits_strings.NewSet()
	lastUpdated := time.Time{}

	tiers, err := images.GetActionTiers(backgrounds, states, scales, lastUpdated, api.ChannelCustomAction)
	if err != nil {
		return nil, err
	}

	_, hasCustomCheermoteAction, err := actionApi.PrefixManager.GetCustomCheermoteAction(ctx, channelId)
	if err != nil {
		return nil, errors.Notef(err, "Failed to get prefix for channelId: %s", channelId)
	}

	if !hasCustomCheermoteAction {
		return nil, nil
	}

	tier := actionsUtils.GetCheerTierForThreshold(tiers, options.Bits)

	imageUrl, err := tier.Images.Get(options.Background, options.State, options.Scale)
	if err != nil {
		return nil, err
	}

	response := &api.GetActionAssetResponse{
		ImageURL: imageUrl,
		Color:    tier.Color,
		BitsTier: tier.BitsMinimum,
		CanCheer: tier.CanCheer,
	}

	return response, nil
}
