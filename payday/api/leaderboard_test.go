package api

import (
	"errors"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	"code.justin.tv/commerce/payday/api/apidef"
	"code.justin.tv/commerce/payday/decorators"
	"code.justin.tv/commerce/payday/dynamo"
	channel_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/cache/channel"
	pantheon_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/pantheon"
	"code.justin.tv/commerce/payday/models/api"
	"code.justin.tv/commerce/payday/utils/pointers"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
	"github.com/zenazn/goji/web"
)

func TestGetLeaderboard(t *testing.T) {
	Convey("Given a test server and payday client", t, func() {
		pantheonClient := new(pantheon_mock.IPantheonClientWrapper)
		channelManager := new(channel_mock.ChannelManager)

		leaderBoardApi := &LeaderboardApi{
			PantheonClient: pantheonClient,
			ChannelManager: channelManager,
		}

		channel := &dynamo.Channel{
			Onboarded: pointers.BoolP(true),
		}

		m := web.New()
		m.Get("/api/leaderboards/:channel", decorators.DecorateAndAdapt(leaderBoardApi.GetChannelLeaderboard, apidef.GetChannelLeaderboard))

		Convey("happy path with default params and no entry key returns a leaderboard", func() {
			channelId := "123"
			channelManager.On("Get", mock.Anything, channelId).
				Return(channel, nil)
			pantheonClient.On("GetLeaderboard", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).
				Return(&pantheonrpc.GetLeaderboardResp{}, nil)
			resp := getChannelLeaderboard(m, channelId, api.GetLeaderboardOptions{})
			So(resp.Code, ShouldEqual, http.StatusOK)
		})

		Convey("500 on pantheon error", func() {
			channelId := "123"
			channelManager.On("Get", mock.Anything, channelId).
				Return(channel, nil)
			pantheonClient.On("GetLeaderboard", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).
				Return(&pantheonrpc.GetLeaderboardResp{}, errors.New("unit test error"))
			resp := getChannelLeaderboard(m, channelId, api.GetLeaderboardOptions{})
			So(resp.Code, ShouldEqual, http.StatusInternalServerError)
		})

		Convey("412 on Bits ineligible channel", func() {
			channelId := "123"
			channelManager.On("Get", mock.Anything, channelId).
				Return(nil, nil)
			resp := getChannelLeaderboard(m, channelId, api.GetLeaderboardOptions{})
			So(resp.Code, ShouldEqual, http.StatusPreconditionFailed)
		})

		Convey("500 on invalid leaderboard time period", func() {
			channelId := "123"
			channelManager.On("Get", mock.Anything, channelId).
				Return(nil, errors.New("Test error"))
			resp := getChannelLeaderboard(m, channelId, api.GetLeaderboardOptions{})
			So(resp.Code, ShouldEqual, http.StatusInternalServerError)
		})
	})
}

func getChannelLeaderboard(m *web.Mux, channelId string, options api.GetLeaderboardOptions) *httptest.ResponseRecorder {
	r, err := http.NewRequest("GET", fmt.Sprintf("/api/leaderboards/%s", channelId), nil)
	So(err, ShouldBeNil)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)
	return w
}
