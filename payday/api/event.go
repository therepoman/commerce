package api

import (
	"net/http"
	"strconv"
	"strings"

	"code.justin.tv/commerce/payday/api/utils"
	"code.justin.tv/commerce/payday/cartman"
	httperror "code.justin.tv/commerce/payday/errors/http"
	apimodel "code.justin.tv/commerce/payday/models/api"
	goauth "code.justin.tv/common/goauthorization"
	"github.com/zenazn/goji/web"
)

const (
	defaultEventsLimit = int32(25)
	maxEventsLimit     = int32(1000)
)

type EventApi struct {
	CartmanValidator cartman.IValidator `inject:""`
}

// TODO: Migrate this API off of cartman capabilities - this is currently called by visage
func (eventApi *EventApi) GetUserEventsWithAuth(c web.C, w http.ResponseWriter, r *http.Request) error {
	userParam := c.URLParams["tuid"]
	capability := goauth.CapabilityClaims{
		"get_user_events": goauth.CapabilityClaim{
			"target_user_id": userParam,
		},
	}

	err := eventApi.CartmanValidator.Validate(r, &capability)
	if err != nil {
		return err
	}
	return eventApi.GetUserEvents(c, w, r, false)
}

func (eventApi *EventApi) AdminGetUserEvents(c web.C, w http.ResponseWriter, r *http.Request) error {
	return eventApi.GetUserEvents(c, w, r, true)
}

func (eventApi *EventApi) GetUserEvents(c web.C, w http.ResponseWriter, r *http.Request, includeAdminInfo bool) error {
	strLimit := r.URL.Query().Get("limit")

	var limit64 int64
	var err error
	if strings.TrimSpace(strLimit) == "" {
		limit64 = int64(defaultEventsLimit)
	} else {
		limit64, err = strconv.ParseInt(strLimit, 10, 32)
		if err != nil {
			return httperror.NewWithCause("Received invalid limit", http.StatusBadRequest, err)
		}
	}
	if limit64 < 1 {
		limit64 = int64(defaultEventsLimit)
	}

	limit32 := int32(limit64)
	if limit32 > maxEventsLimit {
		return httperror.New("Limit above maximum", http.StatusBadRequest)
	}

	if includeAdminInfo {
		writeAdminUserEventsResponse("", w)
	} else {
		writeUserEventsResponse("", w)
	}

	return nil
}

func writeUserEventsResponse(cursor string, w http.ResponseWriter) {
	resp := apimodel.UserEventsResponse{
		Events: make([]apimodel.Event, 0),
		Cursor: string(cursor),
	}

	utils.WriteResponse(resp, w)
}

func writeAdminUserEventsResponse(cursor string, w http.ResponseWriter) {
	resp := apimodel.AdminUserEventsResponse{
		Events: make([]apimodel.AdminEvent, 0),
		Cursor: string(cursor),
	}

	utils.WriteResponse(resp, w)
}
