package api_test

import (
	"os"
	"testing"
	"time"

	"code.justin.tv/commerce/payday/cartman"
	"code.justin.tv/commerce/payday/cartman/decoder/rsa"
	"code.justin.tv/commerce/payday/cartman/decoder/selector"
	"code.justin.tv/commerce/payday/oauth"
	"code.justin.tv/commerce/payday/test"
	auth "code.justin.tv/common/goauthorization"
	goauth "code.justin.tv/common/goauthorization"
	"github.com/zenazn/goji/web"
)

var cartmanValidator cartman.IValidator
var cartmanEncoder auth.Encoder

func setup() {
	var err error

	rsaDecoder, err := goauth.NewDecoder("RS256", "code.justin.tv/commerce/payday", "code.justin.tv/web/cartman", test.ReadFile("testdata/id_rsa.pub"))
	if err != nil {
		panic(err)
	}

	decodeSelector := selector.NewAlgorithmAwareDecoderSelector(map[string]map[string]goauth.Decoder{
		selector.PaydayAudience: {
			rsa.CartmanAlgorithm: rsaDecoder,
		},
	})

	cartmanValidator, err = cartman.NewValidatorWithClients(decodeSelector, nil)
	if err != nil {
		panic("Cannot initialize cartman")
	}

	cartmanEncoder, err = auth.NewEncoder("RS256", "code.justin.tv/web/cartman", test.ReadFile("testdata/id_rsa.pem"))
	if err != nil {
		panic("Cannot initialize cartman")
	}
}

func getCapabilities() auth.CapabilityClaims {
	capClaim := map[string]auth.CapValue{"channel": "dathass", "user_id": 123}
	return map[string]auth.CapabilityClaim{"spectre::edit_channel": capClaim}
}

func generateInvalidToken() *auth.AuthorizationToken {
	params := generateTokenParams(getCapabilities(), []string{"code.justin.tv/web/web"}, "dianers")
	return cartmanEncoder.Encode(params)
}

func generateAuthenticatedUserToken(authedUserID string) *auth.AuthorizationToken {
	params := generateTokenParams(auth.CapabilityClaims{}, []string{"code.justin.tv/commerce/payday"}, authedUserID)
	params.Fpc = true // first party client
	params.ClientID = oauth.TwilightClientID
	return cartmanEncoder.Encode(params)
}

func generateTokenParams(cap auth.CapabilityClaims, aud []string, sub string) auth.TokenParams {
	return auth.TokenParams{
		Exp:    time.Now().Add(10 * time.Minute),
		Nbf:    time.Now(),
		Aud:    aud,
		Sub:    sub,
		Claims: cap,
	}
}

func getWebContext() web.C {
	c := web.C{}
	c.Env = make(map[interface{}]interface{})
	c.URLParams = make(map[string]string)
	return c
}

func TestMain(m *testing.M) {
	setup()
	os.Exit(m.Run())
}
