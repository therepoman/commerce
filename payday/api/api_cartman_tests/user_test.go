package api_test

import (
	"errors"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/mock"

	"code.justin.tv/commerce/payday/api"
	"code.justin.tv/commerce/payday/cartman"
	httperrors "code.justin.tv/commerce/payday/errors/http"
	dynamo_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo"
	user_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/user"
	userstate_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/userstate"
	apimodel "code.justin.tv/commerce/payday/models/api"
	"github.com/stretchr/testify/assert"
)

func TestGetUserEligibilityInvalid(t *testing.T) {
	userId := "12345"

	c := getWebContext()
	c.URLParams["tuid"] = userId

	er := new(user_mock.Checker)
	dao := new(dynamo_mock.IUserDao)
	userStateGetter := new(userstate_mock.Getter)
	userApi := api.UserApi{
		CartmanValidator: cartmanValidator,
		UserChecker:      er,
		Dao:              dao,
		UserStateGetter:  userStateGetter,
	}

	request, _ := http.NewRequest("GET", "/users/"+userId, nil)
	recorder := httptest.NewRecorder()
	err := userApi.GetUserInfoWithCartmanAuthentication(c, recorder, request)
	httperr, ok := err.(httperrors.HttpError)

	assert.True(t, ok, "Response was not an http error")
	assert.Equal(t, http.StatusUnauthorized, httperr.StatusCode())
}

func TestGetUserEligibilityCacheInvalid(t *testing.T) {
	userId := "12345"

	token := generateAuthenticatedUserToken(userId)
	tokenSting, _ := token.String()
	c := getWebContext()
	c.URLParams["tuid"] = userId

	er := new(user_mock.Checker)
	dao := new(dynamo_mock.IUserDao)
	userStateGetter := new(userstate_mock.Getter)
	userApi := api.UserApi{
		CartmanValidator: cartmanValidator,
		UserChecker:      er,
		Dao:              dao,
		UserStateGetter:  userStateGetter,
	}
	er.On("IsEligible", mock.Anything, userId).Return(true, nil)
	userStateGetter.On("Get", userId).Return(apimodel.UserStateUnknown, errors.New("test broken cache"))

	request, _ := http.NewRequest("GET", "/users/"+userId, nil)
	request.Header.Set(cartman.TwitchAuthHeader, tokenSting)
	recorder := httptest.NewRecorder()
	err := userApi.GetUserInfoWithCartmanAuthentication(c, recorder, request)
	httperr, ok := err.(httperrors.HttpError)

	assert.True(t, ok, "Response was not an http error")
	assert.Equal(t, http.StatusInternalServerError, httperr.StatusCode())
}

func TestGetUserEligibilityHappy(t *testing.T) {
	userId := "12345"

	token := generateAuthenticatedUserToken(userId)
	tokenSting, _ := token.String()
	c := getWebContext()
	c.URLParams["tuid"] = userId

	er := new(user_mock.Checker)
	dao := new(dynamo_mock.IUserDao)
	userStateGetter := new(userstate_mock.Getter)
	userApi := api.UserApi{
		CartmanValidator: cartmanValidator,
		UserChecker:      er,
		Dao:              dao,
		UserStateGetter:  userStateGetter,
	}
	er.On("IsEligible", mock.Anything, userId).Return(true, nil)
	userStateGetter.On("Get", userId).Return(apimodel.UserStateCheered, nil)

	request, _ := http.NewRequest("GET", "/users/"+userId, nil)
	request.Header.Set(cartman.TwitchAuthHeader, tokenSting)
	recorder := httptest.NewRecorder()
	err := userApi.GetUserInfoWithCartmanAuthentication(c, recorder, request)
	assert.NoError(t, err)

	response, err := apimodel.ConvertJsonToGetUserInfoResponse(recorder.Body.Bytes())
	assert.NoError(t, err)
	assert.Equal(t, apimodel.GetUserInfoResponse{Eligible: true, UserState: apimodel.CHEERED}, *response)
}
