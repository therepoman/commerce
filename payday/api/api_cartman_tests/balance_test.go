package api_test

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"code.justin.tv/commerce/payday/api"
	"code.justin.tv/commerce/payday/api/clienttracking"
	"code.justin.tv/commerce/payday/cartman"
	errors "code.justin.tv/commerce/payday/errors/http"
	balance_api_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/api/balance"
	clienttracking_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/api/clienttracking"
	bitsbadges_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/badgetiers/products/badges"
	throttle_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/throttle"
	"code.justin.tv/commerce/payday/models"
	apimodel "code.justin.tv/commerce/payday/models/api"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func TestGetBalanceInvalid(t *testing.T) {
	userId := "12345"

	c := getWebContext()
	invalidToken := generateInvalidToken()
	tokenString, _ := invalidToken.String()
	c.URLParams["tuid"] = userId

	th := new(throttle_mock.IThrottler)
	getter := new(balance_api_mock.Getter)
	badgeLogicianMock := new(bitsbadges_mock.Logician)
	clientTracking := new(clienttracking_mock.Tracker)
	clientTracking.On("Track", mock.Anything, clienttracking.GetBalance).Return()

	balanceApi := api.BalanceApi{
		CartmanValidator:        cartmanValidator,
		BalanceGetter:           getter,
		Throttler:               th,
		AvailableBadgesLogician: badgeLogicianMock,
		ClientTracking:          clientTracking,
	}

	r, _ := http.NewRequest("GET", "/balance/"+userId, nil)
	r.Header.Set(cartman.TwitchAuthHeader, tokenString)
	w := httptest.NewRecorder()
	err := balanceApi.GetBalanceWithCartmanAuthentication(c, w, r)
	httperr, ok := err.(errors.HttpError)

	assert.True(t, ok, "Response was not an http error")
	assert.Equal(t, http.StatusUnauthorized, httperr.StatusCode())
}

func TestGetBalanceHappy(t *testing.T) {
	c := getWebContext()
	userId := "12345"
	channelId := "999"
	actualBalance := int32(11)

	token := generateAuthenticatedUserToken(userId)
	tokenString, _ := token.String()
	c.URLParams["tuid"] = userId

	th := new(throttle_mock.IThrottler)
	getter := new(balance_api_mock.Getter)
	badgeLogicianMock := new(bitsbadges_mock.Logician)
	clientTracking := new(clienttracking_mock.Tracker)
	clientTracking.On("Track", mock.Anything, clienttracking.GetBalance).Return()

	balanceApi := api.BalanceApi{
		CartmanValidator:        cartmanValidator,
		BalanceGetter:           getter,
		Throttler:               th,
		AvailableBadgesLogician: badgeLogicianMock,
		ClientTracking:          clientTracking,
	}

	output := &models.Balance{
		ProductInformation: map[string]models.BitsProductInformation{},
		CanPurchase:        map[string]bool{},
		TotalBits:          models.Bits(actualBalance),
	}
	badgeResponse := int64(100)

	getter.On("Get", mock.Anything, userId, channelId, false).Return(actualBalance, output, nil)
	badgeLogicianMock.On("GetUserBitsBadgesAvailableForChannel", mock.Anything, userId, channelId).Return([]*paydayrpc.Badge{
		{
			BadgeSetId:      "bits",
			BadgeSetVersion: "1",
		},
		{
			BadgeSetId:      "bits",
			BadgeSetVersion: "100",
		},
	}, nil)
	th.On("Throttle", mock.Anything, mock.Anything).Return(true)

	r, _ := http.NewRequest("GET", "/balance/"+userId+"?channel="+channelId, nil)
	r.Header.Set(cartman.TwitchAuthHeader, tokenString)
	w := httptest.NewRecorder()
	err := balanceApi.GetBalanceWithCartmanAuthentication(c, w, r)
	assert.NoError(t, err)

	response, err := apimodel.ConvertJsonToGetBalanceResponse(w.Body.Bytes())
	assert.NoError(t, err)
	assert.Equal(t, apimodel.GetBalanceResponse{Balance: actualBalance, HighestEntitledBadge: badgeResponse, Bundles: map[string]bool{}}, *response)
}

func TestAdminGetBalanceHappy(t *testing.T) {
	userId := "12345"
	actualBalance := int32(42)

	c := getWebContext()
	c.URLParams["tuid"] = userId

	th := new(throttle_mock.IThrottler)
	getter := new(balance_api_mock.Getter)
	badgeLogicianMock := new(bitsbadges_mock.Logician)
	clientTracking := new(clienttracking_mock.Tracker)
	clientTracking.On("Track", mock.Anything, clienttracking.GetBalance).Return()

	balanceApi := api.BalanceApi{
		CartmanValidator:        cartmanValidator,
		BalanceGetter:           getter,
		Throttler:               th,
		AvailableBadgesLogician: badgeLogicianMock,
		ClientTracking:          clientTracking,
	}

	output := &models.Balance{
		ProductInformation: map[string]models.BitsProductInformation{},
		CanPurchase:        map[string]bool{},
		TotalBits:          models.Bits(actualBalance),
	}
	badgeResponse := int64(0)

	getter.On("Get", mock.Anything, userId, "", false).Return(actualBalance, output, nil)
	th.On("Throttle", mock.Anything, mock.Anything).Return(true)

	r, _ := http.NewRequest("GET", "/admin/"+userId+"/balance", nil)
	w := httptest.NewRecorder()
	err := balanceApi.GetBalanceWithoutCartman(c, w, r)
	assert.NoError(t, err)

	response, err := apimodel.ConvertJsonToGetBalanceResponse(w.Body.Bytes())
	assert.NoError(t, err)
	assert.Equal(t, apimodel.GetBalanceResponse{Balance: actualBalance, HighestEntitledBadge: badgeResponse, Bundles: map[string]bool{}}, *response)
}
