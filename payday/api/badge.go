package api

import (
	"context"
	"fmt"
	"net/http"
	"strconv"

	dart "code.justin.tv/amzn/TwitchDartReceiverTwirp"
	chatBadgesModel "code.justin.tv/chat/badges/app/models"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/api/constants"
	"code.justin.tv/commerce/payday/api/utils"
	chatbadges "code.justin.tv/commerce/payday/badgetiers"
	bitsbadges "code.justin.tv/commerce/payday/badgetiers/products/badges"
	badgetieremotes "code.justin.tv/commerce/payday/badgetiers/products/emotes"
	badgeUsers "code.justin.tv/commerce/payday/badgetiers/products/users"
	badgeUtils "code.justin.tv/commerce/payday/badgetiers/utils"
	"code.justin.tv/commerce/payday/cartman"
	chatBadgesClient "code.justin.tv/commerce/payday/clients/chatbadges"
	"code.justin.tv/commerce/payday/datascience"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/errors"
	httperror "code.justin.tv/commerce/payday/errors/http"
	"code.justin.tv/commerce/payday/middleware"
	"code.justin.tv/commerce/payday/models"
	apiModels "code.justin.tv/commerce/payday/models/api"
	partnerManager "code.justin.tv/commerce/payday/partner"
	"code.justin.tv/commerce/payday/utils/experiments"
	"github.com/hashicorp/go-multierror"
	"github.com/zenazn/goji/web"
)

const (
	// Our 2 Dart notification campaigns:
	// Affiliate: https://dart.osiris.xarth.tv/campaigns/view/5d900590-c288-495a-bdc7-872dcfb02a35
	// Partner: https://dart.osiris.xarth.tv/campaigns/view/4dbc78c7-381c-40ee-9959-bb9f8d00c6dc
	BadgeRemovedNotificationForAffiliate = "badge_removed"
	BadgeRemovedNotificationForPartner   = "badge_removed_partner"

	// TODO:
	// Currently we do not expose a way to provide a specific reason for take-down,
	// setting it to "other" because it will show as "Other inappropriate content" which is the most general option.
	// But in the future we may need to address this
	BitsBadgeModerationEmailBadgeType = "Bit Badge"
	BitsBadgeModerationEmailReason    = "other"

	// Data science
	BitsBadgeTierUpdateAction  = "update"
	BitsBadgeTierEnableAction  = "enable"
	BitsBadgeTierDisableAction = "disable"
	BitsBadgeDefaultType       = "default"
	BitsBadgeCustomType        = "custom"
)

// APIs regarding bits chat badges
type BadgeApi struct {
	Dao                   dynamo.IBadgeTierDao                `inject:""`
	CartmanValidator      cartman.IValidator                  `inject:""`
	DataScienceClient     datascience.DataScience             `inject:""`
	ChatBadgesClient      chatBadgesClient.IChatBadgesClient  `inject:""`
	PartnerManager        partnerManager.IPartnerManager      `inject:""`
	DartReceiver          dart.Receiver                       `inject:""`
	AvailableBadgesFiller bitsbadges.Filler                   `inject:""`
	BadgeSaver            bitsbadges.Saver                    `inject:""`
	BadgeUsersFiller      badgeUsers.Filler                   `inject:""`
	BadgeTierEmotesFiller badgetieremotes.Filler              `inject:""`
	BadgeTierEmotesSaver  badgetieremotes.Saver               `inject:""`
	ExperimentFetcher     experiments.ExperimentConfigFetcher `inject:""`
}

func newBadgeTiers(dynamoTiers []*dynamo.BadgeTier) map[int64]*apiModels.BadgeTier {
	tierMap := make(map[int64]*apiModels.BadgeTier)
	for _, dynamoTier := range dynamoTiers {
		tier := models.NewBadgeTier(dynamoTier)
		tierMap[dynamoTier.Threshold] = tier
	}
	return tierMap
}

func fillMissingTiers(channelId string, tierMap map[int64]*apiModels.BadgeTier) {
	for tierThreshold, tierConfig := range chatbadges.BitsChatBadgeTiers {
		_, isPresent := tierMap[tierThreshold]
		if !isPresent && tierConfig.Enabled {
			missingTier := &apiModels.BadgeTier{
				ChannelId: channelId,
				Threshold: tierThreshold,
				Enabled:   true,
			}
			tierMap[tierThreshold] = missingTier
		}
	}
}

func tierMapToSlice(tierMap map[int64]*apiModels.BadgeTier) []*apiModels.BadgeTier {
	tiers := make([]*apiModels.BadgeTier, 0, len(tierMap))
	for _, tier := range tierMap {
		tiers = append(tiers, tier)
	}

	return tiers
}

func (badgeApi *BadgeApi) getTiersFromDynamo(channelId string) ([]*dynamo.BadgeTier, error) {
	tiers, err := badgeApi.Dao.GetAll(channelId, true)
	if err != nil {
		log.WithError(err).Error("Error getting badge tiers from dynamo")
		return nil, httperror.NewWithCause(constants.DownstreamDependencyError, http.StatusInternalServerError, err)
	}
	return tiers, nil
}

func (badgeApi *BadgeApi) GetBadgeTiersAdmin(c web.C, w http.ResponseWriter, r *http.Request) error {
	return badgeApi.GetBadgeTiers(c, w, r)
}

func (badgeApi *BadgeApi) GetBadgeTiersWithCartmanAuthentication(c web.C, w http.ResponseWriter, r *http.Request) error {
	ctx, cancel := context.WithTimeout(r.Context(), utils.DefaultTimeout)
	defer cancel()

	channelId := c.URLParams["tuid"]
	err := utils.ValidateRequestUserId(channelId)
	if err != nil {
		return err
	}

	err = badgeApi.CartmanValidator.ValidateAuthenticatedUserID(r, channelId, true, true)
	if err != nil {
		log.Warnf("Cartman token could not be validated for getting badge tiers")
		return err
	}

	return badgeApi.processGetBadgeTiers(ctx, w, channelId)
}

func (badgeApi *BadgeApi) GetBadgeTiers(c web.C, w http.ResponseWriter, r *http.Request) error {
	ctx, cancel := context.WithTimeout(r.Context(), utils.DefaultTimeout)
	defer cancel()

	channelId := c.URLParams["tuid"]
	err := utils.ValidateRequestUserId(channelId)
	if err != nil {
		return err
	}

	return badgeApi.processGetBadgeTiers(ctx, w, channelId)
}

func (badgeApi *BadgeApi) processGetBadgeTiers(ctx context.Context, w http.ResponseWriter, channelId string) error {
	tiers, err := badgeApi.getTiersFromDynamo(channelId)
	if err != nil {
		return err
	}

	tierMap := newBadgeTiers(tiers)
	fillMissingTiers(channelId, tierMap)

	// fill in images for badge tiers
	tierMap, err = badgeApi.AvailableBadgesFiller.PopulateWithCustomImages(ctx, channelId, tierMap)
	if err != nil {
		return err
	}

	// fill in unlocked users count for badge tiers
	tierMap, err = badgeApi.BadgeUsersFiller.PopulateWithUnlockedUsersCounts(ctx, channelId, tierMap)
	if err != nil {
		return err
	}

	// fill in uploaded emotes for badge tiers
	tierMap, err = badgeApi.BadgeTierEmotesFiller.PopulateWithBadgeTierEmoticons(ctx, channelId, tierMap)
	if err != nil {
		return err
	}

	allTiers := tierMapToSlice(tierMap)
	canUploadBitsBadgeTierEmotes := true // Bits badge tier emotes have been rolled out to all affiliates and partners
	getBadgesResponse := models.NewGetBadgesResponse(allTiers, &canUploadBitsBadgeTierEmotes)
	utils.WriteResponse(getBadgesResponse, w)
	return nil
}

func (badgeApi *BadgeApi) SetBadgeTiersAdmin(c web.C, w http.ResponseWriter, r *http.Request) error {
	channelID, request, err := parseSetBadgeTiersRequest(c, r)
	if err != nil {
		log.WithError(err).Error("Failed to parse set badge tiers request")
		return err
	}

	ctx, cancel := context.WithTimeout(r.Context(), utils.DefaultTimeout)
	defer cancel()
	resp, err := badgeApi.processAdminSetBadgeTiers(ctx, w, request, channelID)
	if err != nil {
		return err
	}

	utils.WriteResponse(resp, w)
	return nil
}

func (badgeApi *BadgeApi) processAdminSetBadgeTiers(ctx context.Context, w http.ResponseWriter, request *apiModels.SetBadgesRequest, channelID string) (*apiModels.GetBadgesResponse, error) {
	logger := log.WithFields(log.Fields{
		"channel_id": channelID,
	})

	partnerType, err := badgeApi.PartnerManager.GetPartnerType(ctx, channelID)
	if err != nil {
		logger.WithError(err).Error("Failed to retrieve partner type for bits badge moderation")
		return nil, httperror.NewWithCause(constants.BitsBadgeModerationPartnerStatusError, http.StatusInternalServerError, err)
	}

	logger = logger.WithFields(log.Fields{
		"partner_type": partnerType,
	})

	// check if there there are badges to be moderated
	// if user is a partner, don't delete the customization on the badge
	// if user is an affiliate, allow deletions of customization on the badge
	hasTiersToModerate := false
	for _, tier := range request.Tiers {
		shouldDeleteTitle := tier.DeleteTitle != nil && *tier.DeleteTitle
		shouldDeleteImage := tier.DeleteImage != nil && *tier.DeleteImage

		if shouldDeleteTitle || shouldDeleteImage {
			hasTiersToModerate = true
		}
	}

	resp, err := badgeApi.processSetBadgeTiers(ctx, request, channelID)
	if err != nil {
		return nil, err
	}

	// send out email for Bits badge moderation
	if hasTiersToModerate && badgeApi.isExperimentActiveForBadgeModerationEmails(ctx, channelID) {
		err = badgeApi.sendBadgeModerationEmail(ctx, channelID, partnerType)
		if err != nil {
			logger.WithError(err).Error("Failed to send out email notifications for bits badge moderation")
			return nil, err
		} else {
			logger.Info("Successfully sent out bits badge moderation email")
		}
	}

	return resp, nil
}

func (badgeApi *BadgeApi) SetBadgeTiersWithCartmanAuthentication(c web.C, w http.ResponseWriter, r *http.Request) error {
	channelID, request, err := parseSetBadgeTiersRequest(c, r)
	if err != nil {
		log.WithError(err).Error("Failed to parse set badge tiers request")
		return err
	}

	err = badgeApi.CartmanValidator.ValidateAuthenticatedUserID(r, channelID, true, true)
	if err != nil {
		log.Warnf("Cartman token could not be validated for setting badge tiers")
		return err
	}

	ctx, cancel := context.WithTimeout(r.Context(), utils.DefaultTimeout)
	defer cancel()
	resp, err := badgeApi.processSetBadgeTiers(ctx, request, channelID)
	if err != nil {
		return err
	}

	utils.WriteResponse(resp, w)
	return nil
}

func (badgeApi *BadgeApi) SetBadgeTiers(c web.C, w http.ResponseWriter, r *http.Request) error {
	channelID, request, err := parseSetBadgeTiersRequest(c, r)
	if err != nil {
		log.WithError(err).Error("Failed to parse set badge tiers request")
		return err
	}

	ctx, cancel := context.WithTimeout(r.Context(), utils.DefaultTimeout)
	defer cancel()
	resp, err := badgeApi.processSetBadgeTiers(ctx, request, channelID)
	if err != nil {
		return err
	}

	utils.WriteResponse(resp, w)
	return nil
}

func (badgeApi *BadgeApi) processSetBadgeTiers(ctx context.Context, request *apiModels.SetBadgesRequest, channelId string) (*apiModels.GetBadgesResponse, error) {
	err := validateTiersToSet(request.Tiers)
	if err != nil {
		return nil, err
	}

	err = badgeApi.Dao.PutAll(models.ToDynamoTiers(channelId, request))
	if err != nil {
		log.WithError(err).Error("Error saving in dynamo for SetBadgesCall call")
		return nil, httperror.NewWithCause(constants.DownstreamDependencyError, http.StatusInternalServerError, err)
	}

	dynamoBadgeTiers, err := badgeApi.getTiersFromDynamo(channelId)
	if err != nil {
		return nil, err
	}

	partnerType, err := badgeApi.PartnerManager.GetPartnerType(ctx, channelId)
	if err != nil {
		return nil, httperror.NewWithCause(constants.DownstreamDependencyError, http.StatusInternalServerError, err)
	}

	var uploadImagesResponse *chatBadgesModel.BitsUploadImagesResponse
	if partnerType == partnerManager.TraditionalPartnerType || partnerType == partnerManager.AffiliatePartnerType {
		uploadImagesResponse, err = badgeApi.BadgeSaver.SaveBadgeImageSettings(ctx, channelId, dynamoBadgeTiers, *request)
		if err != nil {
			returnedErr := handleBadgeSettingsMultiErr(err)
			return nil, returnedErr
		}
	} else {
		// Bits badge tiers are only for partners and affiliates
		return nil, nil
	}

	var emoticonUploadConfigsMap map[int64][]apiModels.EmoticonUploadConfiguration
	var saveEmoticonsErrors error
	emoticonUploadConfigsMap, saveEmoticonsErrors = badgeApi.BadgeTierEmotesSaver.SaveEmoticons(ctx, channelId, *request)
	if saveEmoticonsErrors != nil {
		returnedErr := handleEmoteRewardsMultiErr(saveEmoticonsErrors)
		return nil, returnedErr
	}

	resp := createSetBadgesResponse(channelId, dynamoBadgeTiers, request, uploadImagesResponse, true)
	tierMap := make(map[int64]*apiModels.BadgeTier)
	for _, tier := range resp.Tiers {
		tierMap[tier.Threshold] = tier
	}

	tierMap, err = badgeApi.BadgeUsersFiller.PopulateWithUnlockedUsersCounts(ctx, channelId, tierMap)
	if err != nil {
		return nil, err
	}

	// fill in uploaded emotes for badge tiers
	tierMap, err = badgeApi.BadgeTierEmotesFiller.PopulateWithBadgeTierEmoticons(ctx, channelId, tierMap)
	if err != nil {
		return nil, err
	}

	// fill in currently uploading emotes for badge tiers
	tierMap, err = badgeApi.BadgeTierEmotesFiller.PopulateWithUploadingBadgeTierEmoticons(ctx, tierMap, emoticonUploadConfigsMap)
	if err != nil {
		return nil, err
	}

	var tiers []*apiModels.BadgeTier
	for _, tier := range tierMap {
		tiers = append(tiers, tier)
	}
	resp.Tiers = tiers

	dsCtx, cancel := middleware.CopyContextValues(ctx)
	go func() {
		defer cancel()
		badgeApi.trackBitsBadgeTierSettings(dsCtx, channelId, dynamoBadgeTiers, request.Tiers, resp.Tiers)
	}()

	return resp, nil
}

func createSetBadgesResponse(channelID string, dynamoBadgeTiers []*dynamo.BadgeTier, setBadgesRequest *apiModels.SetBadgesRequest, uploadBadgeImagesResponse *chatBadgesModel.BitsUploadImagesResponse, canUploadBitsBadgeTierEmotes bool) *apiModels.GetBadgesResponse {
	if setBadgesRequest == nil {
		return models.NewGetBadgesResponse(nil, nil)
	}

	responseTiers := make([]*apiModels.BadgeTier, 0)
	for _, requestTier := range setBadgesRequest.Tiers {
		if requestTier != nil {
			savedTierEnabled := false
			savedTier := findDynamoTierByThreshold(requestTier.Threshold, dynamoBadgeTiers)
			if savedTier != nil {
				savedTierEnabled = savedTier.Enabled
			}

			responseTier := &apiModels.BadgeTier{
				ChannelId: channelID,
				Threshold: requestTier.Threshold,
				Enabled:   savedTierEnabled,
			}

			if uploadBadgeImagesResponse != nil {
				badgeVersion := strconv.FormatInt(requestTier.Threshold, 10)
				badgeData, isPresent := uploadBadgeImagesResponse.Versions[badgeVersion]
				if isPresent {
					responseTier.ImageURL1x = badgeData.ImageURL1x
					responseTier.ImageURL2x = badgeData.ImageURL2x
					responseTier.ImageURL4x = badgeData.ImageURL4x
					responseTier.Title = badgeData.Title
					responseTier.LastUpdated = badgeData.LastUpdated
				}
			}
			responseTiers = append(responseTiers, responseTier)
		}
	}
	return models.NewGetBadgesResponse(responseTiers, &canUploadBitsBadgeTierEmotes)
}

func (badgeApi *BadgeApi) trackBitsBadgeTierSettings(ctx context.Context, channelId string, dynamoBadgeTiers []*dynamo.BadgeTier, request []*apiModels.BadgeTierSetting, response []*apiModels.BadgeTier) {
	var badgeTiers []interface{}

	customizedBadges, err := badgeUtils.GetBitsBadgeCustomizationInfo(badgeApi.ChatBadgesClient, channelId)
	if err != nil {
		msg := "Encountered an error getting channel badges from chat badge service"
		log.WithError(err).Error(msg)
	}

	for _, tier := range request {
		action := BitsBadgeTierUpdateAction
		if tier.Enabled != nil {
			if *tier.Enabled {
				action = BitsBadgeTierEnableAction
			} else {
				action = BitsBadgeTierDisableAction
			}
		}

		savedTierEnabled := false
		savedTier := findDynamoTierByThreshold(tier.Threshold, dynamoBadgeTiers)
		if savedTier != nil {
			savedTierEnabled = savedTier.Enabled
		}

		badgeType := BitsBadgeDefaultType
		if customized, present := customizedBadges[tier.Threshold]; present && customized {
			badgeType = BitsBadgeCustomType
		}

		emoteID := ""
		respTier := findAPITierByThreshold(tier.Threshold, response)
		if respTier != nil {
			if respTier.Emoticons != nil && len(*respTier.Emoticons) > 0 {
				// Only one emote is currently supported per badge tier
				emoteID = (*respTier.Emoticons)[0].EmoteID
			}
		}

		bitsBadgeTierSettings := &models.BitsBadgeTierSettings{
			ChannelID:           channelId,
			BadgeAmount:         tier.Threshold,
			BadgeFeatureEnabled: savedTierEnabled,
			Type:                badgeType,
			Action:              action,
			EmoteID:             emoteID,
		}
		badgeTiers = append(badgeTiers, bitsBadgeTierSettings)
	}

	err = badgeApi.DataScienceClient.TrackBatchBitsEvent(ctx, datascience.BitsBadgeTierSettingsEventName, badgeTiers)
	if err != nil {
		log.WithError(err).WithField("badgeTiers", badgeTiers).Error("Could not send datascience event")
	}
}

func (badgeApi *BadgeApi) sendBadgeModerationEmail(ctx context.Context, channelID string, partnerType partnerManager.PartnerType) error {
	if channelID == "" {
		return errors.New("channel ID is required")
	}

	// Notification Logic
	// If partner: send straight to Twitch account as partner-conduct, no bcc (unless otherwise requested)
	// If affiliate: send straight to Twitch account as no-reply, no bcc
	var notificationType string
	if partnerType == partnerManager.TraditionalPartnerType {
		notificationType = BadgeRemovedNotificationForPartner
	} else {
		notificationType = BadgeRemovedNotificationForAffiliate
	}

	params := &dart.PublishNotificationRequest{
		Recipient:        &dart.PublishNotificationRequest_RecipientId{RecipientId: channelID},
		NotificationType: notificationType,
		Metadata: map[string]string{
			"reason":     BitsBadgeModerationEmailReason,
			"badge_type": BitsBadgeModerationEmailBadgeType,
		},
	}

	res, err := badgeApi.DartReceiver.PublishNotification(ctx, params)
	if err != nil {
		log.WithError(err).Error("Failed to publish notification to take down bits badges to dart")
		return err
	}
	log.WithField("response", res).Info("Published notification to take down bits badges to dart")
	return nil
}

func (badgeApi *BadgeApi) isExperimentActiveForBadgeModerationEmails(ctx context.Context, channelID string) bool {
	return badgeApi.ExperimentFetcher.IsExperimentActiveForUserOrChannel(ctx, experiments.BitsBadgeModerationEmailPrefix, channelID, channelID)
}

func parseSetBadgeTiersRequest(c web.C, r *http.Request) (string, *apiModels.SetBadgesRequest, error) {
	channelID := c.URLParams["tuid"]
	err := utils.ValidateRequestUserId(channelID)
	if err != nil {
		return "", nil, err
	}

	request, err := models.ExtractSetBadgesRequest(r)
	if err != nil {
		log.WithError(err).Error("Error decoding JSON request for SetBadgeTiersWithCartmanAuthentication call")
		return "", nil, httperror.NewWithCause(constants.InvalidJsonError, http.StatusBadRequest, err)
	}

	return channelID, request, nil
}

func validateTiersToSet(tierSettings []*apiModels.BadgeTierSetting) error {
	for _, tierSetting := range tierSettings {
		tierConfig, isPresent := chatbadges.BitsChatBadgeTiers[tierSetting.Threshold]
		if !isPresent || !tierConfig.Enabled {
			msg := fmt.Sprintf("Request contains invalid tier threshold: %d", tierSetting.Threshold)
			log.Warnln(msg)
			return httperror.New(constants.InvalidBitsBadgeTierThreshold, http.StatusBadRequest)
		}
	}
	return nil
}

func findDynamoTierByThreshold(threshold int64, dynamoBadgeTiers []*dynamo.BadgeTier) *dynamo.BadgeTier {
	for _, dynamoTier := range dynamoBadgeTiers {
		if dynamoTier != nil && dynamoTier.Threshold == threshold {
			return dynamoTier
		}
	}

	return nil
}

func findAPITierByThreshold(threshold int64, apiBadgeTiers []*apiModels.BadgeTier) *apiModels.BadgeTier {
	for _, apiTier := range apiBadgeTiers {
		if apiTier != nil && apiTier.Threshold == threshold {
			return apiTier
		}
	}

	return nil
}

func handleBadgeSettingsMultiErr(err error) httperror.HttpError {
	if merr, ok := err.(*multierror.Error); ok {
		imageSettingsErrors := merr.Errors

		for _, imageSettingsError := range imageSettingsErrors {
			if saverError, ok := imageSettingsError.(*bitsbadges.SaverError); ok {
				switch saverError.ErrorType {
				case bitsbadges.DeleteAndUpdateError:
					return httperror.New(constants.InvalidRequestParam, http.StatusBadRequest)
				case bitsbadges.RemoveBitsImageError:
					return httperror.NewWithCause(constants.DownstreamDependencyError, http.StatusInternalServerError, imageSettingsError)
				case bitsbadges.TitleTooLongError:
					return httperror.New(constants.BitsBadgeTitleExceedsCharacterLimitError, http.StatusBadRequest)
				case bitsbadges.TitleFailedModerationError:
					return httperror.New(constants.BitsBadgeTitleFailedModerationError, http.StatusBadRequest)
				case bitsbadges.ValidateBitsBadgeImagesError:
					return httperror.New(constants.InvalidBitsBadgeImage, http.StatusBadRequest)
				case bitsbadges.UploadBitsBadgeImagesError:
					return httperror.NewWithCause(constants.DownstreamDependencyError, http.StatusInternalServerError, imageSettingsError)
				}
			}
		}
	}

	return httperror.New(constants.DownstreamDependencyError, http.StatusInternalServerError)
}

func handleEmoteRewardsMultiErr(err error) httperror.HttpError {
	if merr, ok := err.(*multierror.Error); ok {
		emoticonSettingsErrors := merr.Errors

		for _, emoticonSettingsError := range emoticonSettingsErrors {
			if saverError, ok := emoticonSettingsError.(*badgetieremotes.SaverError); ok {
				switch saverError.ErrorType {
				case badgetieremotes.GetEmoticonsDownstreamError:
					return httperror.New(constants.DownstreamDependencyError, http.StatusInternalServerError)
				case badgetieremotes.GetEmoticonsValidationError:
					return httperror.NewWithCause(constants.InvalidRequestParam, http.StatusBadRequest, emoticonSettingsError)
				case badgetieremotes.GetUnlockedBadgeUserCountsError:
					return httperror.New(constants.DownstreamDependencyError, http.StatusInternalServerError)
				case badgetieremotes.CreateEmoticonTierNotAllowedError:
					return httperror.New(constants.EmoteNotAllowedForTier, http.StatusBadRequest)
				case badgetieremotes.CreateEmoticonCodeAlreadyExistsError:
					return httperror.New(constants.EmoteCodeAlreadyExists, http.StatusBadRequest)
				case badgetieremotes.CreateEmoticonCodeUnacceptableError:
					return httperror.New(constants.EmoteCodeUnacceptable, http.StatusBadRequest)
				case badgetieremotes.CreateEmoticonUserOwnedEmoteLimitReachedError:
					return httperror.New(constants.UserOwnedEmoteLimitReached, http.StatusBadRequest)
				case badgetieremotes.CreateEmoticonSlotsAlreadyFullError:
					return httperror.New(constants.EmoticonSlotAlreadyFull, http.StatusBadRequest)
				case badgetieremotes.CreateEmoticonValidationError:
					return httperror.NewWithCause(constants.InvalidRequestParam, http.StatusBadRequest, emoticonSettingsError)
				case badgetieremotes.CreateEmoticonDownstreamError:
					return httperror.New(constants.DownstreamDependencyError, http.StatusInternalServerError)
				case badgetieremotes.DeleteEmoticonValidationError:
					return httperror.NewWithCause(constants.InvalidRequestParam, http.StatusBadRequest, emoticonSettingsError)
				case badgetieremotes.DeleteEmoticonDownstreamError:
					return httperror.New(constants.DownstreamDependencyError, http.StatusInternalServerError)
				}
			}
		}
	}

	return httperror.New(constants.DownstreamDependencyError, http.StatusInternalServerError)
}
