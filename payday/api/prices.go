package api

import (
	"context"
	"math"
	"net/http"
	"sort"
	"strconv"
	"strings"
	"time"

	"code.justin.tv/commerce/gogogadget/pointers"
	gadgetStrings "code.justin.tv/commerce/gogogadget/strings"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/api/utils"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/datascience"
	httperror "code.justin.tv/commerce/payday/errors/http"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/models/api"
	"code.justin.tv/commerce/payday/prices"
	"code.justin.tv/commerce/payday/products"
	"code.justin.tv/commerce/payday/userservice"
	petozi "code.justin.tv/commerce/petozi/rpc"
	"github.com/zenazn/goji/web"
)

const (
	paymentTypeSelectionURL = "/bits-checkout/select?asin="
	amazonPayCheckoutURL    = "/bits-checkout/purchase?asin="
	paypalCheckoutURL       = "/bits-checkout/summary?asin="
	xsollaCheckoutURL       = "/bits-checkout/other?asin="

	MinimumAdBits = 5
)

type PricesApi struct {
	DataScience        datascience.DataScience    `inject:""`
	Config             *config.Configuration      `inject:""`
	FloPricer          prices.IFloPricer          `inject:""`
	UserServiceChecker userservice.Checker        `inject:""`
	LoggedOutResolver  products.LoggedOutResolver `inject:""`
	ProductsGetter     products.Getter            `inject:""`
}

func (p *PricesApi) GetPrices(c web.C, w http.ResponseWriter, r *http.Request) error {
	ctx, cancel := context.WithTimeout(r.Context(), 5*time.Second)
	defer cancel()

	tuid := r.URL.Query().Get("user_id")

	banned, err := p.UserServiceChecker.IsBanned(ctx, tuid)
	if err != nil {
		msg := "Error getting user status"
		log.WithError(err).Error(msg)
		return httperror.NewWithCause(msg, http.StatusInternalServerError, err)
	}

	//if user is banned or suspended, skip product info call and nil out the lists.
	if banned {
		response := api.GetPricesResponse{
			MinimumAdBits:    MinimumAdBits,
			BitsPurchaseURL:  p.Config.CheckoutBaseURL,
			PriceIncludesVAT: false,
			Prices:           []api.Price{},
		}

		utils.WriteResponse(response, w)
		return nil
	}

	corQueryParam := r.URL.Query().Get("cor")
	paydayPlatform, petoziPlatform := p.resolvePlatforms(r, corQueryParam)
	var response api.GetPricesResponse
	bitsProducts, err := p.resolveProducts(ctx, r.URL.Query()["asins"], petoziPlatform)

	if err != nil {
		log.WithError(err).Error("could not resolve products for prices call")
		return httperror.NewWithCause("could not resolve products for prices call", http.StatusInternalServerError, err)
	}

	response, err = p.retrievePrices(ctx, paydayPlatform, bitsProducts, tuid, corQueryParam)
	if err != nil {
		return err
	}

	go func() {
		p.sendToDatascience(bitsProducts, tuid)
	}()

	utils.WriteResponse(response, w)
	return nil
}

func (p *PricesApi) retrievePrices(ctx context.Context, paydayPlatform models.Platform, bitsProducts map[string]*petozi.BitsProduct, tuid string, corQueryParam string) (api.GetPricesResponse, error) {
	reqProducts := make([]string, 0)
	for productID := range bitsProducts {
		reqProducts = append(reqProducts, productID)
	}
	floPrices, floCountry, err := p.FloPricer.GetPrices(ctx, reqProducts, tuid, corQueryParam)
	if err != nil {
		return api.GetPricesResponse{}, httperror.NewWithCause("Encountered unknown error from downstream pricing dependency", http.StatusInternalServerError, err)
	}

	basePrice, baseBitsAmount := p.getDiscountsBase(floPrices, bitsProducts)
	apiPrices := make([]api.Price, 0)
	for _, floPrice := range floPrices {
		apiPrice := api.Price{
			ASIN:         floPrice.ProductId,
			Price:        floPrice.Price,
			USPrice:      floPrice.Price,
			CurrencyUnit: floPrice.Currency,
			PurchaseURL:  p.getFinalPurchaseUrl(paydayPlatform, floPrice),
		}

		product, ok := bitsProducts[floPrice.ProductId]
		if ok {
			apiPrice.OfferID = product.OfferId
			apiPrice.BitsAmount = int(product.Quantity)
			apiPrice.Discount = getDiscount(basePrice, baseBitsAmount, apiPrice.Price, int(product.Quantity))
			if product.Promo != nil {
				apiPrice.Promo = true
				apiPrice.PromoType = strings.ToLower(string(product.Promo.Type))
				apiPrice.PromoID = product.Promo.Id
				apiPrice.ShowPromoWhenLoggedOut = product.ShowWhenLoggedOut
			}

			apiPrices = append(apiPrices, apiPrice)
		} else {
			log.WithFields(log.Fields{
				"platform":  paydayPlatform,
				"productID": floPrice.ProductId,
			}).Error("invalid product ID on price")
			continue
		}
	}

	sort.Slice(apiPrices, func(i, j int) bool { return apiPrices[i].BitsAmount < apiPrices[j].BitsAmount })

	country := floCountry
	if gadgetStrings.BlankP(country) {
		country = &corQueryParam
	}

	priceIncludesVAT := p.priceIncludesVAT(country)
	if len(floPrices) > 0 {
		// Try to use Flo response if available.
		// All bits products within the same country have the same tax behavior
		priceIncludesVAT = floPrices[0].TaxInclusive
	}

	return api.GetPricesResponse{
		MinimumAdBits:    MinimumAdBits,
		BitsPurchaseURL:  p.Config.CheckoutBaseURL,
		PriceIncludesVAT: priceIncludesVAT,
		Prices:           apiPrices,
	}, nil
}

func (p *PricesApi) getFinalPurchaseUrl(platform models.Platform, price models.Price) string {
	if platform == models.AMAZON {
		return p.Config.CheckoutBaseURL + amazonPayCheckoutURL + price.ProductId
	} else if platform == models.PAYPAL {
		return p.Config.CheckoutBaseURL + paypalCheckoutURL + price.ProductId
	} else if platform == models.XSOLLA {
		return p.Config.CheckoutBaseURL + xsollaCheckoutURL + price.ProductId
	} else {
		return p.Config.CheckoutBaseURL + paymentTypeSelectionURL + price.ProductId
	}
}

func (p *PricesApi) sendToDatascience(bitsProducts map[string]*petozi.BitsProduct, tuid string) {
	productsList := make([]string, 0)
	for productID := range bitsProducts {
		productsList = append(productsList, productID)
	}
	sort.Strings(productsList)
	productsListString := strings.Join(productsList, ",")
	dsTuid := tuid
	if tuid == utils.DefaultTUID {
		dsTuid = ""
	}
	bitsPrices := models.BitsPrices{
		UserID:   dsTuid,
		AsinList: productsListString,
	}
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	err := p.DataScience.TrackBitsEvent(ctx, datascience.BitsPricesEventName, &bitsPrices)
	if err != nil {
		log.WithError(err).Error("Failed to send BitsEvent to data science")
	}
}

func (p *PricesApi) resolveProducts(ctx context.Context, productsList []string, platform petozi.Platform) (map[string]*petozi.BitsProduct, error) {
	if len(productsList) > 0 {
		return p.ProductsGetter.GetByIDs(ctx, platform, productsList)
	}

	return p.LoggedOutResolver.Resolve(ctx, platform)
}

func (p *PricesApi) resolvePlatforms(r *http.Request, corQueryParam string) (models.Platform, petozi.Platform) {
	platform := models.ParsePlatform(r.URL.Query().Get("platform"), pointers.StringP("ResolvePlatforms"), nil, nil)

	var paypalEnabled, inMap bool
	paypalRestricted := false

	if corQueryParam != "" {
		paypalEnabled, inMap = p.Config.PaypalEnabledCountries[corQueryParam]
		paypalRestricted = p.isRestrictedCountry(corQueryParam)
	} else {
		paypalEnabled = true
		//since inMap is used to determine countries that aren't configured, assume "" is in map since we allow
		//no country code to go to the paypal checkout.
		inMap = true
	}

	if paypalRestricted && platform != models.AMAZON {
		// We'll assume that restricted countries don't include the US.
		if platform == models.DEFAULT {
			return platform, petozi.Platform_PAYPAL
		}
		return platform, petozi.Platform(petozi.Platform_value[string(platform)])
	} else if !inMap || !paypalEnabled {
		return models.AMAZON, petozi.Platform_AMAZON
	}

	if platform == models.DEFAULT {
		return platform, petozi.Platform_PAYPAL
	}
	return platform, petozi.Platform(petozi.Platform_value[string(platform)])
}

func (p *PricesApi) isRestrictedCountry(cor string) bool {
	for _, restrictedCountry := range p.Config.PaypalRestrictedCountries {
		if restrictedCountry == cor {
			return true
		}
	}
	return false
}

func (p *PricesApi) priceIncludesVAT(cor *string) bool {
	if gadgetStrings.BlankP(cor) {
		//if the cor is nil, assume US in which case price does not include vat
		return false
	} else {
		// We want the countries that are VAT inclusive, so take the
		// opposite of what's in this Tax Exclusive map
		// (where the default means VAT is included)
		return !p.Config.TaxExclusiveCountriesList[*cor]
	}
}

func (p *PricesApi) getDiscountsBase(floPrices []models.Price, products map[string]*petozi.BitsProduct) (basePrice float64, baseBitsAmount int) {
	if len(floPrices) < 1 {
		return
	}

	basePrice = float64(0.0)
	baseBitsAmount = 0
	worstPriceRate := float64(0.0)

	for _, floPrice := range floPrices {
		bundle, ok := products[floPrice.ProductId]

		if !ok {
			log.WithField("sku", floPrice.ProductId).Error("invalid sku")
			continue
		}
		bundlePrice, err := strconv.ParseFloat(floPrice.Price, 64)
		if err != nil {
			log.WithFields(log.Fields{
				"sku":   floPrice.ProductId,
				"price": floPrice.Price,
			}).WithError(err).Error("Failed to convert flo price to int")
			continue
		}

		if bundle.Quantity == 0 {
			log.WithField("sku", floPrice.ProductId).Error("invalid bits amount 0")
			continue
		}

		if bundlePrice/float64(bundle.Quantity) > worstPriceRate {
			basePrice = bundlePrice
			baseBitsAmount = int(bundle.Quantity)
			worstPriceRate = basePrice / float64(baseBitsAmount)
		}
	}
	return
}

func getDiscount(baseBitsPrice float64, baseBitsAmount int, targetPriceStr string, targetBitsAmount int) int {
	targetPrice, err := strconv.ParseFloat(targetPriceStr, 64)
	if err != nil || baseBitsAmount == 0 || baseBitsPrice == 0 || targetBitsAmount == 0 {
		return 0
	}

	expectedPrice := (baseBitsPrice * float64(targetBitsAmount)) / float64(baseBitsAmount)
	r := targetPrice / expectedPrice
	rate := int(math.Round(r * 100))

	return 100 - rate
}
