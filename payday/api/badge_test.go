package api

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"math"
	"net/http"
	"net/http/httptest"
	"sort"
	"strconv"
	"strings"
	"testing"
	"time"

	chatBadgesModel "code.justin.tv/chat/badges/app/models"
	mako_client "code.justin.tv/commerce/mako/twirp"
	"code.justin.tv/commerce/payday/api/apidef"
	"code.justin.tv/commerce/payday/api/constants"
	"code.justin.tv/commerce/payday/badgetiers"
	bitsbadges "code.justin.tv/commerce/payday/badgetiers/products/badges"
	badgetieremotes "code.justin.tv/commerce/payday/badgetiers/products/emotes"
	"code.justin.tv/commerce/payday/cartman"
	"code.justin.tv/commerce/payday/datascience"
	"code.justin.tv/commerce/payday/decorators"
	"code.justin.tv/commerce/payday/dynamo"
	httperror "code.justin.tv/commerce/payday/errors/http"
	dart_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/amzn/TwitchDartReceiverTwirp"
	bitsbadges_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/badgetiers/products/badges"
	badgetieremotes_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/badgetiers/products/emotes"
	badgeusers_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/badgetiers/products/users"
	chatbadges_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/chatbadges"
	dynamo_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo"
	partner_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/partner"
	experiments_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/utils/experiments"
	"code.justin.tv/commerce/payday/models"
	apiModels "code.justin.tv/commerce/payday/models/api"
	"code.justin.tv/commerce/payday/partner"
	"code.justin.tv/commerce/payday/utils/pointers"
	"github.com/hashicorp/go-multierror"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/zenazn/goji/web"
)

var tierDaoMock *dynamo_mock.IBadgeTierDao
var chatBadgeClientMock *chatbadges_mock.IChatBadgesClient
var partnerManagerMock *partner_mock.IPartnerManager
var dartMock *dart_mock.Receiver
var availableBadgesFillerMock *bitsbadges_mock.Filler
var badgeSaverMock *bitsbadges_mock.Saver
var badgeUsersFillerMock *badgeusers_mock.Filler
var badgeTierEmotesFillerMock *badgetieremotes_mock.Filler
var badgeTierEmotesSaverMock *badgetieremotes_mock.Saver
var experimentsFetcherMock *experiments_mock.ExperimentConfigFetcher

const (
	maxBadgeUnlockedUserCount = math.MaxInt64
)

var activeState = fmt.Sprint(mako_client.EmoteState_active)

func getTierMap(defaultValue bool) map[int64]bool {
	tierMap := make(map[int64]bool)
	for tier := range badgetiers.BitsChatBadgeTiers {
		tierMap[tier] = defaultValue
	}
	return tierMap
}

func generateExpectedTiers(channelId string, enabled map[int64]bool) map[int64]*apiModels.BadgeTier {
	tierMap := make(map[int64]*apiModels.BadgeTier)
	now := time.Now()
	for tierThreshold, tierConfig := range badgetiers.BitsChatBadgeTiers {
		if tierConfig.Enabled {
			tierMap[tierThreshold] = &apiModels.BadgeTier{
				ChannelId:          channelId,
				Threshold:          tierThreshold,
				Enabled:            enabled[tierThreshold],
				UnlockedUsersCount: maxBadgeUnlockedUserCount - tierThreshold,
				Title:              "",
				LastUpdated:        &now,
			}
		}
	}
	return tierMap
}

func generateExpectedTiersBasedOnBadgesResponse(channelId string, enabled map[int64]bool, badgesResponse *chatBadgesModel.BadgeSet) map[int64]*apiModels.BadgeTier {
	tierMap := make(map[int64]*apiModels.BadgeTier)
	for tierThreshold, tierConfig := range badgetiers.BitsChatBadgeTiers {
		if tierConfig.Enabled {
			tierMap[tierThreshold] = &apiModels.BadgeTier{
				ChannelId:          channelId,
				Threshold:          tierThreshold,
				Enabled:            enabled[tierThreshold],
				UnlockedUsersCount: maxBadgeUnlockedUserCount - tierThreshold,
			}

			if version, present := badgesResponse.Versions[strconv.FormatInt(tierThreshold, 10)]; present {
				tierMap[tierThreshold].LastUpdated = version.LastUpdated
			}
		}
	}
	return tierMap
}

func convertToTierMap(getBadgesResponse *apiModels.GetBadgesResponse) map[int64]*apiModels.BadgeTier {
	tierMap := make(map[int64]*apiModels.BadgeTier)
	for _, tier := range getBadgesResponse.Tiers {
		tierMap[tier.Threshold] = tier
	}
	return tierMap
}

func setupBadgeTierEndpoints() *web.Mux {
	chatBadgeClientMock = new(chatbadges_mock.IChatBadgesClient)
	partnerManagerMock = new(partner_mock.IPartnerManager)
	tierDaoMock = new(dynamo_mock.IBadgeTierDao)
	dartMock = new(dart_mock.Receiver)
	availableBadgesFillerMock = new(bitsbadges_mock.Filler)
	badgeSaverMock = new(bitsbadges_mock.Saver)
	badgeUsersFillerMock = new(badgeusers_mock.Filler)
	badgeTierEmotesFillerMock = new(badgetieremotes_mock.Filler)
	badgeTierEmotesSaverMock = new(badgetieremotes_mock.Saver)
	experimentsFetcherMock = new(experiments_mock.ExperimentConfigFetcher)

	context := &BadgeApi{
		Dao:                   tierDaoMock,
		CartmanValidator:      cartman.NewAlwaysValidValidator(),
		DataScienceClient:     &datascience.DataScienceApiNoOp{},
		ChatBadgesClient:      chatBadgeClientMock,
		PartnerManager:        partnerManagerMock,
		DartReceiver:          dartMock,
		AvailableBadgesFiller: availableBadgesFillerMock,
		BadgeSaver:            badgeSaverMock,
		BadgeUsersFiller:      badgeUsersFillerMock,
		BadgeTierEmotesFiller: badgeTierEmotesFillerMock,
		BadgeTierEmotesSaver:  badgeTierEmotesSaverMock,
		ExperimentFetcher:     experimentsFetcherMock,
	}

	m := web.New()
	m.Get("/badges/:tuid/tiers", decorators.DecorateAndAdapt(context.GetBadgeTiersWithCartmanAuthentication, apidef.GetBadgeTiers))
	m.Post("/badges/:tuid/tiers", decorators.DecorateAndAdapt(context.SetBadgeTiersWithCartmanAuthentication, apidef.SetBadgeTiers))
	m.Patch("/authed/admin/badges/:tuid/tiers", decorators.DecorateAndAdapt(context.SetBadgeTiersAdmin, apidef.AdminSetBadgeTiers))
	return m
}

func validateResult(t *testing.T, expected map[int64]*apiModels.BadgeTier, actual map[int64]*apiModels.BadgeTier) {
	for _, expectedTier := range expected {
		actualTier := actual[expectedTier.Threshold]
		assert.Equal(t, expectedTier.ChannelId, actualTier.ChannelId)
		assert.Equal(t, expectedTier.Enabled, actualTier.Enabled)
		assert.Equal(t, expectedTier.Threshold, actualTier.Threshold)
		assert.Equal(t, expectedTier.Title, actualTier.Title)
		assert.Equal(t, expectedTier.ImageURL1x, actualTier.ImageURL1x)
		assert.Equal(t, expectedTier.ImageURL2x, actualTier.ImageURL2x)
		assert.Equal(t, expectedTier.ImageURL4x, actualTier.ImageURL4x)

		if expectedTier.LastUpdated != nil && actualTier.LastUpdated != nil {
			assert.WithinDuration(t, *expectedTier.LastUpdated, *actualTier.LastUpdated, 10*time.Second)
		}
	}
}

func standardGet(t *testing.T, m *web.Mux, channelId string) *apiModels.GetBadgesResponse {
	r, _ := http.NewRequest("GET", "/badges/"+channelId+"/tiers", nil)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)
	assert.Equal(t, 200, w.Code)

	response, err := models.ConvertJsonToGetBadgesResponse(w.Body.Bytes())
	assert.NoError(t, err)

	return response
}

func standardGetWithInternalError(t *testing.T, m *web.Mux, channelId string) *httptest.ResponseRecorder {
	r, _ := http.NewRequest("GET", "/badges/"+channelId+"/tiers", nil)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)
	return w
}

func badgesPostExpectSuccess(t *testing.T, m *web.Mux, channelId string, enabled map[int64]bool, imageData map[int64]string, badgeDeletes map[int64]bool, imageTitles map[int64]string) *apiModels.GetBadgesResponse {
	w := badgesPost(m, channelId, enabled, imageData, badgeDeletes, imageTitles)
	assert.Equal(t, 200, w.Code)

	response, err := models.ConvertJsonToGetBadgesResponse(w.Body.Bytes())
	assert.NoError(t, err)

	return response
}

func badgesPost(m *web.Mux, channelId string, enabled map[int64]bool, imageData map[int64]string, badgeDeletes map[int64]bool, imageTitles map[int64]string) *httptest.ResponseRecorder {
	badgeTierSettings := make([]*apiModels.BadgeTierSetting, 0, len(enabled))
	for tierThreshold, tierEnabled := range enabled {
		setting := &apiModels.BadgeTierSetting{
			Threshold: tierThreshold,
			Enabled:   pointers.BoolP(tierEnabled),
		}

		tierImageData, hasTierImageData := imageData[tierThreshold]
		if hasTierImageData {
			setting.ImageData1x = pointers.StringP(tierImageData)
			setting.ImageData2x = pointers.StringP(tierImageData)
			setting.ImageData4x = pointers.StringP(tierImageData)
		}

		shouldDelete, hasDelete := badgeDeletes[tierThreshold]
		if hasDelete && shouldDelete {
			setting.DeleteImage = pointers.BoolP(shouldDelete)
			setting.DeleteTitle = pointers.BoolP(shouldDelete)
		}

		tierTitle, hasTierTitle := imageTitles[tierThreshold]
		if hasTierTitle {
			setting.Title = pointers.StringP(tierTitle)
		}

		badgeTierSettings = append(badgeTierSettings, setting)
	}

	request := &apiModels.SetBadgesRequest{
		Tiers: badgeTierSettings,
	}
	requestJson, _ := json.Marshal(request)
	requestJsonReader := bytes.NewReader(requestJson)

	r, _ := http.NewRequest("POST", "/badges/"+channelId+"/tiers", requestJsonReader)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	return w
}

func adminSetBadgeTiers(m *web.Mux, channelId string, enabled map[int64]bool, imageData map[int64]string, badgeDeletes map[int64]bool, imageTitles map[int64]string) *httptest.ResponseRecorder {
	badgeTierSettings := make([]*apiModels.BadgeTierSetting, 0, len(enabled))
	for tierThreshold, tierEnabled := range enabled {
		setting := &apiModels.BadgeTierSetting{
			Threshold: tierThreshold,
			Enabled:   pointers.BoolP(tierEnabled),
		}

		tierImageData, hasTierImageData := imageData[tierThreshold]
		if hasTierImageData {
			setting.ImageData1x = pointers.StringP(tierImageData)
			setting.ImageData2x = pointers.StringP(tierImageData)
			setting.ImageData4x = pointers.StringP(tierImageData)
		}

		tierTitle, hasTierTitle := imageTitles[tierThreshold]
		if hasTierTitle {
			setting.Title = pointers.StringP(tierTitle)
		}

		shouldDelete, hasDelete := badgeDeletes[tierThreshold]
		if hasDelete && shouldDelete {
			setting.DeleteImage = pointers.BoolP(shouldDelete)
			setting.DeleteTitle = pointers.BoolP(shouldDelete)
			setting.Title = nil
			setting.ImageData1x = nil
			setting.ImageData2x = nil
			setting.ImageData4x = nil
		}

		badgeTierSettings = append(badgeTierSettings, setting)
	}

	request := &apiModels.SetBadgesRequest{
		Tiers: badgeTierSettings,
	}
	requestJson, _ := json.Marshal(request)
	requestJsonReader := bytes.NewReader(requestJson)

	r, _ := http.NewRequest("PATCH", "/authed/admin/badges/"+channelId+"/tiers", requestJsonReader)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	return w
}

func standardBadgesPost(t *testing.T, m *web.Mux, channelId string, enabled map[int64]bool) {
	badgesPostExpectSuccess(t, m, channelId, enabled, map[int64]string{}, map[int64]bool{}, map[int64]string{})
}

func TestNonNumericChannelIdGet(t *testing.T) {
	m := setupBadgeTierEndpoints()

	r, _ := http.NewRequest("GET", "/badges/abc/tiers", nil)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 400, w.Code)
}

func TestNonNumericChannelIdPost(t *testing.T) {
	m := setupBadgeTierEndpoints()

	jsonReader := strings.NewReader("{\"tiers\":[{\"threshold\":10,\"enabled\":true}]}")
	r, _ := http.NewRequest("POST", "/badges/abc/tiers", jsonReader)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 400, w.Code)
}

func TestPostNonNumericThreshold(t *testing.T) {
	m := setupBadgeTierEndpoints()

	jsonReader := strings.NewReader("{\"tiers\":[{\"threshold\":\"abc\",\"enabled\":true}]}")
	r, _ := http.NewRequest("POST", "/badges/123/tiers", jsonReader)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 400, w.Code)
}

func TestPostNonBoolEnabled(t *testing.T) {
	m := setupBadgeTierEndpoints()

	jsonReader := strings.NewReader("{\"tiers\":[{\"threshold\":10,\"enabled\":falseo}]}")
	r, _ := http.NewRequest("POST", "/badges/123/tiers", jsonReader)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 400, w.Code)
}

func TestPostContainsDisabledThreshold(t *testing.T) {
	m := setupBadgeTierEndpoints()

	jsonReader := strings.NewReader("{\"tiers\":[{\"threshold\":100,\"enabled\":false},{\"threshold\":10000000,\"enabled\":false}]}")
	r, _ := http.NewRequest("POST", "/badges/123/tiers", jsonReader)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 400, w.Code)
}

func TestPostContainsNonExistentThreshold(t *testing.T) {
	m := setupBadgeTierEndpoints()

	jsonReader := strings.NewReader("{\"tiers\":[{\"threshold\":100,\"enabled\":false},{\"threshold\":101,\"enabled\":false}]}")
	r, _ := http.NewRequest("POST", "/badges/123/tiers", jsonReader)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 400, w.Code)
}

func mockedChatBadgeResponse() *chatBadgesModel.BadgeSet {
	now := time.Now()
	mockBadgeSet := &chatBadgesModel.BadgeSet{
		Versions: map[string]chatBadgesModel.BadgeVersion{
			"1": {
				ImageURL1x:  pointers.StringP(""),
				ImageURL2x:  pointers.StringP(""),
				ImageURL4x:  pointers.StringP(""),
				Title:       "",
				LastUpdated: &now,
			},
		},
	}
	return mockBadgeSet
}

func mockedPartnerTypeResponse() partner.PartnerType {
	return partner.TraditionalPartnerType
}

func mockedTierMap(channelID string) map[int64]*apiModels.BadgeTier {
	tierMap := map[int64]*apiModels.BadgeTier{
		1: {
			Enabled:    true,
			ImageURL1x: "data/uri/1",
			ImageURL2x: "data/uri/2",
			ImageURL4x: "data/uri/4",
			ChannelId:  channelID,
			Threshold:  1,
		},
		100: {
			Enabled:    false,
			ImageURL1x: "data/uri/1",
			ImageURL2x: "data/uri/2",
			ImageURL4x: "data/uri/4",
			ChannelId:  channelID,
			Threshold:  100,
		},
		1000: {
			Enabled:    false,
			ImageURL1x: "data/uri/1",
			ImageURL2x: "data/uri/2",
			ImageURL4x: "data/uri/4",
			ChannelId:  channelID,
			Threshold:  1000,
		},
		5000000: {
			Enabled:    false,
			ImageURL1x: "data/uri/1",
			ImageURL2x: "data/uri/2",
			ImageURL4x: "data/uri/4",
			ChannelId:  channelID,
			Threshold:  5000000,
		},
	}

	return tierMap
}

func mockedTierMapWithUserCounts(channelID string) map[int64]*apiModels.BadgeTier {
	tierMap := mockedTierMap(channelID)

	tierMap[1].UnlockedUsersCount = 500
	tierMap[100].UnlockedUsersCount = 420
	tierMap[1000].UnlockedUsersCount = 69
	tierMap[5000000].UnlockedUsersCount = 1

	return tierMap
}

func mockedTierMapWithEmoticonUploadConfigs(channelID string) map[int64]*apiModels.BadgeTier {
	tierMap := mockedTierMapWithUserCounts(channelID)

	tooSmart := apiModels.EmoticonUploadConfiguration{
		Code:    "tooSmart",
		GroupID: "BitsBadgeTier-6-1",
		OwnerID: "456",
	}

	tooCool := apiModels.EmoticonUploadConfiguration{
		Code:    "tooCool",
		GroupID: "BitsBadgeTier-6-100",
		OwnerID: "456",
	}

	tooSpicy := apiModels.EmoticonUploadConfiguration{
		Code:    "tooSpicy",
		GroupID: "BitsBadgeTier-6-5000000",
		OwnerID: "456",
	}

	tierMap[1].EmoticonUploadConfigurations = &[]apiModels.EmoticonUploadConfiguration{tooSmart}
	tierMap[100].EmoticonUploadConfigurations = &[]apiModels.EmoticonUploadConfiguration{tooCool}
	tierMap[5000000].EmoticonUploadConfigurations = &[]apiModels.EmoticonUploadConfiguration{tooSpicy}

	return tierMap
}

func mockedTierMapWithEmoticons(channelID string) map[int64]*apiModels.BadgeTier {
	tierMap := mockedTierMapWithEmoticonUploadConfigs(channelID)

	tooTrue := apiModels.Emoticon{
		Code:    "tooTrue",
		GroupID: "BitsBadgeTier-6-1",
		OwnerID: "456",
		State:   activeState,
		EmoteID: "uuidv4-abcd-whatever",
	}

	tooStrong := apiModels.Emoticon{
		Code:    "tooStrong",
		GroupID: "BitsBadgeTier-6-100",
		OwnerID: "456",
		State:   activeState,
		EmoteID: "uuidv4-efgh-whatever",
	}

	tooThirsty := apiModels.Emoticon{
		Code:    "tooThirsty",
		GroupID: "BitsBadgeTier-6-5000000",
		OwnerID: "456",
		State:   activeState,
		EmoteID: "uuidv4-ijkl-whatever",
	}

	tierMap[1].Emoticons = &[]apiModels.Emoticon{tooTrue}
	tierMap[1000].Emoticons = &[]apiModels.Emoticon{tooStrong}
	tierMap[5000000].Emoticons = &[]apiModels.Emoticon{tooThirsty}

	return tierMap
}

func TestTiersInThresholdAscOrder(t *testing.T) {
	channelId := "741852963"
	m := setupBadgeTierEndpoints()

	exTiers := getTierMap(true)
	expectedTierMap := generateExpectedTiers(channelId, exTiers)

	chatBadgeClientMock.On("GetBitsImages", mock.Anything, channelId, mock.Anything).Return(mockedChatBadgeResponse(), nil)
	partnerManagerMock.On("GetPartnerType", mock.Anything, mock.Anything).Return(mockedPartnerTypeResponse(), nil)
	tierDaoMock.On("GetAll", channelId, mock.Anything).Return(nil, nil)
	availableBadgesFillerMock.On("PopulateWithCustomImages", mock.Anything, mock.Anything, mock.Anything).Return(expectedTierMap, nil)
	badgeUsersFillerMock.On("PopulateWithUnlockedUsersCounts", mock.Anything, mock.Anything, mock.Anything).Return(expectedTierMap, nil)
	badgeTierEmotesFillerMock.On("PopulateWithBadgeTierEmoticons", mock.Anything, mock.Anything, mock.Anything).Return(expectedTierMap, nil)
	response := standardGet(t, m, channelId)

	assert.True(t, len(response.Tiers) > 0)
	assert.True(t, sort.SliceIsSorted(response.Tiers, func(i, j int) bool {
		return response.Tiers[i].Threshold < response.Tiers[j].Threshold
	}))
}

func TestNoTiersPosted(t *testing.T) {
	channelId := "1"
	m := setupBadgeTierEndpoints()

	exTiers := getTierMap(true)
	expectedTierMap := generateExpectedTiersBasedOnBadgesResponse(channelId, exTiers, mockedChatBadgeResponse())

	chatBadgeClientMock.On("GetBitsImages", mock.Anything, channelId, mock.Anything).Return(mockedChatBadgeResponse(), nil)
	partnerManagerMock.On("GetPartnerType", mock.Anything, mock.Anything).Return(mockedPartnerTypeResponse(), nil)
	tierDaoMock.On("GetAll", channelId, mock.Anything).Return(nil, nil)
	availableBadgesFillerMock.On("PopulateWithCustomImages", mock.Anything, mock.Anything, mock.Anything).Return(expectedTierMap, nil)
	badgeUsersFillerMock.On("PopulateWithUnlockedUsersCounts", mock.Anything, mock.Anything, mock.Anything).Return(expectedTierMap, nil)
	badgeTierEmotesFillerMock.On("PopulateWithBadgeTierEmoticons", mock.Anything, mock.Anything, mock.Anything).Return(expectedTierMap, nil)
	response := standardGet(t, m, channelId)

	actualTierMap := convertToTierMap(response)
	validateResult(t, expectedTierMap, actualTierMap)
}

func TestOneTierPostedDisabled(t *testing.T) {
	channelId := "2"
	m := setupBadgeTierEndpoints()
	tier := int64(5000)

	postTiers := map[int64]bool{
		tier: false,
	}

	putRequest := models.ToDynamoTiers(channelId, &apiModels.SetBadgesRequest{
		Tiers: []*apiModels.BadgeTierSetting{
			{
				Threshold: tier,
				Enabled:   pointers.BoolP(false),
			},
		},
	})

	expectedTierMap := map[int64]*apiModels.BadgeTier{
		tier: {
			Threshold: tier,
			Enabled:   false,
		},
	}

	chatBadgeClientMock.On("GetBitsImages", mock.Anything, channelId, mock.Anything).Return(nil, nil)
	partnerManagerMock.On("GetPartnerType", mock.Anything, mock.Anything).Return(mockedPartnerTypeResponse(), nil)
	tierDaoMock.On("GetAll", channelId, mock.Anything).Return(nil, nil)
	tierDaoMock.On("PutAll", putRequest).Return(nil)
	badgeSaverMock.On("SaveBadgeImageSettings", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil, nil)
	badgeTierEmotesSaverMock.On("SaveEmoticons", mock.Anything, mock.Anything, mock.Anything).Return(nil, nil)
	badgeUsersFillerMock.On("PopulateWithUnlockedUsersCounts", mock.Anything, mock.Anything, mock.Anything).Return(expectedTierMap, nil)
	badgeTierEmotesFillerMock.On("PopulateWithBadgeTierEmoticons", mock.Anything, mock.Anything, mock.Anything).Return(expectedTierMap, nil)
	badgeTierEmotesFillerMock.On("PopulateWithUploadingBadgeTierEmoticons", mock.Anything, mock.Anything, mock.Anything).Return(expectedTierMap, nil)
	standardBadgesPost(t, m, channelId, postTiers)

	tierDaoMock.AssertCalled(t, "PutAll", putRequest)
}

func TestOneTierPostedEnabled(t *testing.T) {
	channelId := "3"
	m := setupBadgeTierEndpoints()
	tier := int64(10000)

	postTiers := map[int64]bool{
		tier: true,
	}

	putRequest := models.ToDynamoTiers(channelId, &apiModels.SetBadgesRequest{
		Tiers: []*apiModels.BadgeTierSetting{
			{
				Threshold: tier,
				Enabled:   pointers.BoolP(true),
			},
		},
	})

	expectedTierMap := map[int64]*apiModels.BadgeTier{
		tier: {
			Threshold: tier,
			Enabled:   true,
		},
	}

	chatBadgeClientMock.On("GetBitsImages", mock.Anything, channelId, mock.Anything).Return(nil, nil)
	partnerManagerMock.On("GetPartnerType", mock.Anything, mock.Anything).Return(mockedPartnerTypeResponse(), nil)
	tierDaoMock.On("GetAll", channelId, mock.Anything).Return(nil, nil)
	tierDaoMock.On("PutAll", putRequest).Return(nil)
	badgeSaverMock.On("SaveBadgeImageSettings", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil, nil)
	badgeTierEmotesSaverMock.On("SaveEmoticons", mock.Anything, mock.Anything, mock.Anything).Return(nil, nil)
	badgeUsersFillerMock.On("PopulateWithUnlockedUsersCounts", mock.Anything, mock.Anything, mock.Anything).Return(expectedTierMap, nil)
	badgeTierEmotesFillerMock.On("PopulateWithBadgeTierEmoticons", mock.Anything, mock.Anything, mock.Anything).Return(expectedTierMap, nil)
	badgeTierEmotesFillerMock.On("PopulateWithUploadingBadgeTierEmoticons", mock.Anything, mock.Anything, mock.Anything).Return(expectedTierMap, nil)
	standardBadgesPost(t, m, channelId, postTiers)

	tierDaoMock.AssertCalled(t, "PutAll", putRequest)
}

func TestAllTierInDBSomeEnabled(t *testing.T) {
	channelId := "4"
	m := setupBadgeTierEndpoints()

	postTiers := getTierMap(true)
	postTiers[100] = false
	postTiers[5000] = false
	postTiers[100000] = false

	expectedTierMap := map[int64]*apiModels.BadgeTier{
		100: {
			Threshold: 100,
			Enabled:   false,
		},
		5000: {
			Threshold: 5000,
			Enabled:   false,
		},
		100000: {
			Threshold: 10000,
			Enabled:   false,
		},
	}

	chatBadgeClientMock.On("GetBitsImages", mock.Anything, channelId, mock.Anything).Return(nil, nil)
	partnerManagerMock.On("GetPartnerType", mock.Anything, mock.Anything).Return(mockedPartnerTypeResponse(), nil)
	tierDaoMock.On("GetAll", channelId, mock.Anything).Return(nil, nil)
	tierDaoMock.On("PutAll", mock.Anything).Return(nil)
	badgeSaverMock.On("SaveBadgeImageSettings", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil, nil)
	badgeTierEmotesSaverMock.On("SaveEmoticons", mock.Anything, mock.Anything, mock.Anything).Return(nil, nil)
	badgeUsersFillerMock.On("PopulateWithUnlockedUsersCounts", mock.Anything, mock.Anything, mock.Anything).Return(expectedTierMap, nil)
	badgeTierEmotesFillerMock.On("PopulateWithBadgeTierEmoticons", mock.Anything, mock.Anything, mock.Anything).Return(expectedTierMap, nil)
	badgeTierEmotesFillerMock.On("PopulateWithUploadingBadgeTierEmoticons", mock.Anything, mock.Anything, mock.Anything).Return(expectedTierMap, nil)
	standardBadgesPost(t, m, channelId, postTiers)

	tierDaoMock.AssertCalled(t, "PutAll", mock.Anything)
}

func TestAllTierInDBAllDisabled(t *testing.T) {
	channelId := "5"
	m := setupBadgeTierEndpoints()

	postTiers := getTierMap(false)
	expectedTierMap := map[int64]*apiModels.BadgeTier{}

	chatBadgeClientMock.On("GetBitsImages", mock.Anything, channelId, mock.Anything).Return(mockedChatBadgeResponse(), nil)
	partnerManagerMock.On("GetPartnerType", mock.Anything, mock.Anything).Return(mockedPartnerTypeResponse(), nil)
	tierDaoMock.On("GetAll", channelId, mock.Anything).Return(nil, nil)
	tierDaoMock.On("PutAll", mock.Anything).Return(nil)
	badgeSaverMock.On("SaveBadgeImageSettings", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil, nil)
	badgeTierEmotesSaverMock.On("SaveEmoticons", mock.Anything, mock.Anything, mock.Anything).Return(nil, nil)
	badgeUsersFillerMock.On("PopulateWithUnlockedUsersCounts", mock.Anything, mock.Anything, mock.Anything).Return(expectedTierMap, nil)
	badgeTierEmotesFillerMock.On("PopulateWithBadgeTierEmoticons", mock.Anything, mock.Anything, mock.Anything).Return(expectedTierMap, nil)
	badgeTierEmotesFillerMock.On("PopulateWithUploadingBadgeTierEmoticons", mock.Anything, mock.Anything, mock.Anything).Return(expectedTierMap, nil)
	chatBadgeClientMock.On("GetBadgeImages", mock.Anything, mock.Anything, mock.Anything).Return(nil, nil)
	standardBadgesPost(t, m, channelId, postTiers)

	tierDaoMock.AssertCalled(t, "PutAll", mock.Anything)
}

func TestAllTierInDBAllEnabled(t *testing.T) {
	channelId := "6"
	m := setupBadgeTierEndpoints()

	postTiers := getTierMap(true)
	expectedTierMap := map[int64]*apiModels.BadgeTier{}

	chatBadgeClientMock.On("GetBitsImages", mock.Anything, channelId, mock.Anything).Return(mockedChatBadgeResponse(), nil)
	partnerManagerMock.On("GetPartnerType", mock.Anything, mock.Anything).Return(mockedPartnerTypeResponse(), nil)
	tierDaoMock.On("GetAll", channelId, mock.Anything).Return(nil, nil)
	tierDaoMock.On("PutAll", mock.Anything).Return(nil)
	badgeSaverMock.On("SaveBadgeImageSettings", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil, nil)
	badgeTierEmotesSaverMock.On("SaveEmoticons", mock.Anything, mock.Anything, mock.Anything).Return(nil, nil)
	badgeUsersFillerMock.On("PopulateWithUnlockedUsersCounts", mock.Anything, mock.Anything, mock.Anything).Return(expectedTierMap, nil)
	badgeTierEmotesFillerMock.On("PopulateWithBadgeTierEmoticons", mock.Anything, mock.Anything, mock.Anything).Return(expectedTierMap, nil)
	badgeTierEmotesFillerMock.On("PopulateWithUploadingBadgeTierEmoticons", mock.Anything, mock.Anything, mock.Anything).Return(expectedTierMap, nil)
	standardBadgesPost(t, m, channelId, postTiers)

	tierDaoMock.AssertCalled(t, "PutAll", mock.Anything)
}

func TestBadgeApi_GetBadgeTiers(t *testing.T) {
	Convey("Given valid badge tier endpionts", t, func() {
		m := setupBadgeTierEndpoints()

		channelId := "6"
		tier1 := int64(1)
		tier1UnlockedUsersCount := int64(123)
		tooSmartUploadConfig := apiModels.EmoticonUploadConfiguration{
			Code:    "tooSmart",
			GroupID: "BitsBadgeTier-6-1",
			OwnerID: "456",
		}

		Convey("and a badge tier DAO that succeeds", func() {
			tierDaoMock.On("GetAll", channelId, mock.Anything).Return([]*dynamo.BadgeTier{{ChannelId: channelId, Enabled: true, Threshold: tier1}}, nil)

			Convey("and an available badge filler that succeeds", func() {
				tierMap := map[int64]*apiModels.BadgeTier{}
				fillMissingTiers(channelId, tierMap)

				availableBadgesFillerMock.On("PopulateWithCustomImages", mock.Anything, mock.Anything, mock.Anything).Return(tierMap, nil)

				Convey("and a badge users filler that succeeds", func() {
					tierMap[tier1].UnlockedUsersCount = tier1UnlockedUsersCount

					badgeUsersFillerMock.On("PopulateWithUnlockedUsersCounts", mock.Anything, mock.Anything, mock.Anything).Return(tierMap, nil)

					Convey("and a badge emotes filler that succeeds on filling existing emoticons", func() {
						tierMap[tier1].EmoticonUploadConfigurations = &[]apiModels.EmoticonUploadConfiguration{tooSmartUploadConfig}

						badgeTierEmotesFillerMock.On("PopulateWithBadgeTierEmoticons", mock.Anything, mock.Anything, mock.Anything).Return(tierMap, nil)

						resp := standardGet(t, m, channelId)

						So(resp.CanUploadBadgeTierEmotes, ShouldNotBeNil)
						So(*resp.CanUploadBadgeTierEmotes, ShouldBeTrue)
						So(len(resp.Tiers), ShouldEqual, 28)
						for _, tier := range resp.Tiers {
							if tier.Threshold == tier1 {
								So(tier.EmoticonUploadConfigurations, ShouldNotBeNil)
								So(len(*tier.EmoticonUploadConfigurations), ShouldEqual, 1)
							} else {
								So(tier.EmoticonUploadConfigurations, ShouldBeNil)
							}
						}
					})

					Convey("and a badge emotes filler that errors on filling existing emoticons", func() {
						err := httperror.NewWithCause(constants.DownstreamDependencyError, http.StatusInternalServerError, errors.New("test error"))

						badgeTierEmotesFillerMock.On("PopulateWithBadgeTierEmoticons", mock.Anything, mock.Anything, mock.Anything).Return(nil, err)

						respRecorder := standardGetWithInternalError(t, m, channelId)

						So(respRecorder.Code, ShouldEqual, 500)
					})
				})

				Convey("and a badge users filler that errors", func() {
					err := httperror.NewWithCause(constants.DownstreamDependencyError, http.StatusInternalServerError, errors.New("test error"))

					badgeUsersFillerMock.On("PopulateWithUnlockedUsersCounts", mock.Anything, mock.Anything, mock.Anything).Return(nil, err)

					respRecorder := standardGetWithInternalError(t, m, channelId)

					So(respRecorder.Code, ShouldEqual, 500)
				})
			})

			Convey("and an available badge filler that errors", func() {
				err := httperror.NewWithCause(constants.DownstreamDependencyError, http.StatusInternalServerError, errors.New("test error"))

				availableBadgesFillerMock.On("PopulateWithCustomImages", mock.Anything, mock.Anything, mock.Anything).Return(nil, err)

				respRecorder := standardGetWithInternalError(t, m, channelId)

				So(respRecorder.Code, ShouldEqual, 500)
			})
		})

		Convey("and a badge tier DAO that errors", func() {
			tierDaoMock.On("GetAll", channelId, mock.Anything).Return([]*dynamo.BadgeTier{}, errors.New("test error"))

			responseRecorder := standardGetWithInternalError(t, m, channelId)

			So(responseRecorder.Code, ShouldEqual, 500)
		})
	})
}

func TestBadgeApi_SetBadgeTiers(t *testing.T) {
	Convey("Given valid badge tier endpionts", t, func() {
		m := setupBadgeTierEndpoints()

		Convey("and a valid channelID param", func() {
			channelID := "6"

			Convey("with valid badge tiers to set", func() {
				enabledBadgeChanges := map[int64]bool{
					1:       true,
					100:     false,
					1000:    false,
					5000000: true,
				}

				Convey("and a badge tier DAO that succeeds on PUTs", func() {
					tierDaoMock.On("PutAll", mock.Anything).Return(nil)

					Convey("and a badge tier DAO that succeeds on GETs", func() {
						Convey("with no custom tiers", func() {
							tierDaoMock.On("GetAll", mock.Anything, mock.Anything).Return(nil, nil)

							Convey("and a partner manager that succeeds", func() {
								Convey("returning a regular partner", func() {
									partnerManagerMock.On("GetPartnerType", mock.Anything, mock.Anything).Return(partner.TraditionalPartnerType, nil)

									Convey("and a badge saver that succeeds", func() {
										uploadImagesResponse := &chatBadgesModel.BitsUploadImagesResponse{
											Versions: map[string]chatBadgesModel.BitsBadgeUploadImageResponse{
												"1": {
													ImageURL1x: "data/uri/1",
													ImageURL2x: "data/uri/2",
													ImageURL4x: "data/uri/4",
												},
												"100": {
													ImageURL1x: "data/uri/1",
													ImageURL2x: "data/uri/2",
													ImageURL4x: "data/uri/4",
												},
												"1000": {
													ImageURL1x: "data/uri/1",
													ImageURL2x: "data/uri/2",
													ImageURL4x: "data/uri/4",
												},
												"5000000": {
													ImageURL1x: "data/uri/1",
													ImageURL2x: "data/uri/2",
													ImageURL4x: "data/uri/4",
												},
											},
										}

										badgeSaverMock.On("SaveBadgeImageSettings", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(uploadImagesResponse, nil)

										Convey("with a bits badge emotes saver that succeeds", func() {
											badgeTierEmotesSaverMock.On("SaveEmoticons", mock.Anything, mock.Anything, mock.Anything).Return(nil, nil)

											Convey("and a badge users filler that succeeds", func() {
												badgeUsersFillerMock.On("PopulateWithUnlockedUsersCounts", mock.Anything, mock.Anything, mock.Anything).Return(mockedTierMapWithUserCounts(channelID), nil)

												Convey("and a badge tier emotes filler that succeeds on filling existing emoticons", func() {
													badgeTierEmotesFillerMock.On("PopulateWithBadgeTierEmoticons", mock.Anything, mock.Anything, mock.Anything).Return(mockedTierMapWithEmoticons(channelID), nil)

													Convey("and a badge filler that succeeds on filling newly uploaded emoticons", func() {
														badgeTierEmotesFillerMock.On("PopulateWithUploadingBadgeTierEmoticons", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(mockedTierMapWithEmoticons(channelID), nil)

														Convey("and asynchronous datascience succeeds", func() {
															chatBadgeClientMock.On("GetBitsImages", mock.Anything, mock.Anything, mock.Anything).Return(nil, nil)

															resp := badgesPostExpectSuccess(t, m, channelID, enabledBadgeChanges, map[int64]string{}, map[int64]bool{}, map[int64]string{})

															So(resp.CanUploadBadgeTierEmotes, ShouldNotBeNil)
															So(*resp.CanUploadBadgeTierEmotes, ShouldBeTrue)
															for _, tier := range resp.Tiers {
																So(tier.Enabled, ShouldEqual, mockedTierMapWithEmoticons(channelID)[tier.Threshold].Enabled)
															}
														})

														Convey("and asynchronous datascience errors", func() {
															chatBadgeClientMock.On("GetBitsImages", mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("test error"))

															respRecorder := badgesPost(m, channelID, enabledBadgeChanges, map[int64]string{}, map[int64]bool{}, map[int64]string{})

															So(respRecorder.Code, ShouldEqual, 200)
														})
													})

													Convey("and a badge filler that errors on filling newly uploaded emoticons", func() {
														badgeTierEmotesFillerMock.On("PopulateWithUploadingBadgeTierEmoticons", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(mockedTierMapWithEmoticons(channelID), errors.New("test error"))

														respRecorder := badgesPost(m, channelID, enabledBadgeChanges, map[int64]string{}, map[int64]bool{}, map[int64]string{})

														So(respRecorder.Code, ShouldEqual, 500)
													})
												})

												Convey("and a badge tier emotes filler that errors on filling existing emoticons", func() {
													badgeTierEmotesFillerMock.On("PopulateWithBadgeTierEmoticons", mock.Anything, mock.Anything, mock.Anything).Return(mockedTierMapWithEmoticons(channelID), errors.New("test error"))

													respRecorder := badgesPost(m, channelID, enabledBadgeChanges, map[int64]string{}, map[int64]bool{}, map[int64]string{})

													So(respRecorder.Code, ShouldEqual, 500)
												})
											})

											Convey("and a badge users filler that errors", func() {
												badgeUsersFillerMock.On("PopulateWithUnlockedUsersCounts", mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("test error"))

												respRecorder := badgesPost(m, channelID, enabledBadgeChanges, map[int64]string{}, map[int64]bool{}, map[int64]string{})

												So(respRecorder.Code, ShouldEqual, 500)
											})
										})

										Convey("with a bits badge emotes saver that errors", func() {
											badgeTierEmotesSaverMock.On("SaveEmoticons", mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("test error"))

											respRecorder := badgesPost(m, channelID, enabledBadgeChanges, map[int64]string{}, map[int64]bool{}, map[int64]string{})

											So(respRecorder.Code, ShouldEqual, 500)
										})
									})

									Convey("and a badge saver that errors", func() {
										Convey("with an unknown error type", func() {
											badgeSaverMock.On("SaveBadgeImageSettings", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("test error"))

											respRecorder := badgesPost(m, channelID, enabledBadgeChanges, map[int64]string{}, map[int64]bool{}, map[int64]string{})

											So(respRecorder.Code, ShouldEqual, 500)
										})

										Convey("with a known bad request error type", func() {
											var err error
											err = multierror.Append(err, bitsbadges.NewSaverError(errors.New("test error"), bitsbadges.TitleFailedModerationError))

											badgeSaverMock.On("SaveBadgeImageSettings", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil, err)

											respRecorder := badgesPost(m, channelID, enabledBadgeChanges, map[int64]string{}, map[int64]bool{}, map[int64]string{})

											So(respRecorder.Code, ShouldEqual, 400)
										})
									})
								})

								Convey("returning an affiliate", func() {
									partnerManagerMock.On("GetPartnerType", mock.Anything, mock.Anything).Return(partner.AffiliatePartnerType, nil)

									Convey("and a badge saver that succeeds", func() {
										uploadImagesResponse := &chatBadgesModel.BitsUploadImagesResponse{
											Versions: map[string]chatBadgesModel.BitsBadgeUploadImageResponse{
												"1": {
													ImageURL1x: "data/uri/1",
													ImageURL2x: "data/uri/2",
													ImageURL4x: "data/uri/4",
												},
												"100": {
													ImageURL1x: "data/uri/1",
													ImageURL2x: "data/uri/2",
													ImageURL4x: "data/uri/4",
												},
												"1000": {
													ImageURL1x: "data/uri/1",
													ImageURL2x: "data/uri/2",
													ImageURL4x: "data/uri/4",
												},
												"5000000": {
													ImageURL1x: "data/uri/1",
													ImageURL2x: "data/uri/2",
													ImageURL4x: "data/uri/4",
												},
											},
										}

										badgeSaverMock.On("SaveBadgeImageSettings", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(uploadImagesResponse, nil)

										Convey("with a bits badge emotes saver that succeeds", func() {
											badgeTierEmotesSaverMock.On("SaveEmoticons", mock.Anything, mock.Anything, mock.Anything).Return(nil, nil)

											Convey("and a badge users filler that succeeds", func() {
												badgeUsersFillerMock.On("PopulateWithUnlockedUsersCounts", mock.Anything, mock.Anything, mock.Anything).Return(mockedTierMapWithUserCounts(channelID), nil)

												Convey("and a badge tier emotes filler that succeeds on filling existing emoticons", func() {
													badgeTierEmotesFillerMock.On("PopulateWithBadgeTierEmoticons", mock.Anything, mock.Anything, mock.Anything).Return(mockedTierMapWithEmoticons(channelID), nil)

													Convey("and a badge filler that succeeds on filling newly uploaded emoticons", func() {
														badgeTierEmotesFillerMock.On("PopulateWithUploadingBadgeTierEmoticons", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(mockedTierMapWithEmoticons(channelID), nil)
														chatBadgeClientMock.On("GetBitsImages", mock.Anything, mock.Anything, mock.Anything).Return(nil, nil)

														resp := badgesPostExpectSuccess(t, m, channelID, enabledBadgeChanges, map[int64]string{}, map[int64]bool{}, map[int64]string{})

														So(resp.CanUploadBadgeTierEmotes, ShouldNotBeNil)
														So(*resp.CanUploadBadgeTierEmotes, ShouldBeTrue)
														for _, tier := range resp.Tiers {
															So(tier.Enabled, ShouldEqual, mockedTierMapWithEmoticons(channelID)[tier.Threshold].Enabled)
														}
													})

													Convey("and a badge filler that errors on filling newly uploaded emoticons", func() {
														badgeTierEmotesFillerMock.On("PopulateWithUploadingBadgeTierEmoticons", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(mockedTierMapWithEmoticons(channelID), errors.New("test error"))

														respRecorder := badgesPost(m, channelID, enabledBadgeChanges, map[int64]string{}, map[int64]bool{}, map[int64]string{})

														So(respRecorder.Code, ShouldEqual, 500)
													})
												})

												Convey("and a badge tier emotes filler that errors on filling existing emoticons", func() {
													badgeTierEmotesFillerMock.On("PopulateWithBadgeTierEmoticons", mock.Anything, mock.Anything, mock.Anything).Return(mockedTierMapWithEmoticons(channelID), errors.New("test error"))

													respRecorder := badgesPost(m, channelID, enabledBadgeChanges, map[int64]string{}, map[int64]bool{}, map[int64]string{})

													So(respRecorder.Code, ShouldEqual, 500)
												})
											})

											Convey("and a badge users filler that errors", func() {
												badgeUsersFillerMock.On("PopulateWithUnlockedUsersCounts", mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("test error"))

												respRecorder := badgesPost(m, channelID, enabledBadgeChanges, map[int64]string{}, map[int64]bool{}, map[int64]string{})

												So(respRecorder.Code, ShouldEqual, 500)
											})
										})

										Convey("with a bits badge emotes saver that errors at least once", func() {
											Convey("with an unknown error type", func() {
												badgeTierEmotesSaverMock.On("SaveEmoticons", mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("test error"))

												respRecorder := badgesPost(m, channelID, enabledBadgeChanges, map[int64]string{}, map[int64]bool{}, map[int64]string{})

												So(respRecorder.Code, ShouldEqual, 500)
											})

											Convey("with a known bad request error type", func() {
												err := multierror.Append(nil, badgetieremotes.NewSaverError(errors.New("test error"), badgetieremotes.CreateEmoticonCodeAlreadyExistsError))

												badgeTierEmotesSaverMock.On("SaveEmoticons", mock.Anything, mock.Anything, mock.Anything).Return(nil, err)

												respRecorder := badgesPost(m, channelID, enabledBadgeChanges, map[int64]string{}, map[int64]bool{}, map[int64]string{})

												So(respRecorder.Code, ShouldEqual, 400)
											})
										})
									})

									Convey("and a badge saver that errors", func() {
										badgeSaverMock.On("SaveBadgeImageSettings", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("test error"))

										respRecorder := badgesPost(m, channelID, enabledBadgeChanges, map[int64]string{}, map[int64]bool{}, map[int64]string{})

										So(respRecorder.Code, ShouldEqual, 500)
									})
								})

								Convey("returning any other type of user", func() {
									partnerManagerMock.On("GetPartnerType", mock.Anything, mock.Anything).Return(partner.PartnerType("unsupported type"), nil)

									resp := badgesPostExpectSuccess(t, m, channelID, enabledBadgeChanges, map[int64]string{}, map[int64]bool{}, map[int64]string{})

									So(resp, ShouldNotBeNil)
									So(resp.Tiers, ShouldBeEmpty)
									So(resp.CanUploadBadgeTierEmotes, ShouldBeNil)
								})
							})

							Convey("and a partner manager that errors", func() {
								partnerManagerMock.On("GetPartnerType", mock.Anything, mock.Anything).Return(partner.PartnerType(""), errors.New("test error"))

								respRecorder := badgesPost(m, channelID, enabledBadgeChanges, map[int64]string{}, map[int64]bool{}, map[int64]string{})

								So(respRecorder.Code, ShouldEqual, 500)
							})
						})

						Convey("with at least one custom tier", func() {
							tierDaoMock.On("GetAll", mock.Anything, mock.Anything).Return([]*dynamo.BadgeTier{
								{
									Threshold: 1,
									Enabled:   true,
									ChannelId: channelID,
								},
							}, nil)

							Convey("and a partner manager that succeeds", func() {
								Convey("returning a regular partner", func() {
									partnerManagerMock.On("GetPartnerType", mock.Anything, mock.Anything).Return(partner.TraditionalPartnerType, nil)

									Convey("and a badge saver that succeeds", func() {
										uploadImagesResponse := &chatBadgesModel.BitsUploadImagesResponse{
											Versions: map[string]chatBadgesModel.BitsBadgeUploadImageResponse{
												"1": {
													ImageURL1x: "data/uri/1",
													ImageURL2x: "data/uri/2",
													ImageURL4x: "data/uri/4",
												},
												"100": {
													ImageURL1x: "data/uri/1",
													ImageURL2x: "data/uri/2",
													ImageURL4x: "data/uri/4",
												},
												"1000": {
													ImageURL1x: "data/uri/1",
													ImageURL2x: "data/uri/2",
													ImageURL4x: "data/uri/4",
												},
												"5000000": {
													ImageURL1x: "data/uri/1",
													ImageURL2x: "data/uri/2",
													ImageURL4x: "data/uri/4",
												},
											},
										}

										badgeSaverMock.On("SaveBadgeImageSettings", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(uploadImagesResponse, nil)

										Convey("with a bits badge emotes saver that succeeds", func() {
											badgeTierEmotesSaverMock.On("SaveEmoticons", mock.Anything, mock.Anything, mock.Anything).Return(nil, nil)

											Convey("and a badge users filler that succeeds", func() {
												badgeUsersFillerMock.On("PopulateWithUnlockedUsersCounts", mock.Anything, mock.Anything, mock.Anything).Return(mockedTierMapWithUserCounts(channelID), nil)

												Convey("and a badge tier emotes filler that succeeds on filling existing emoticons", func() {
													badgeTierEmotesFillerMock.On("PopulateWithBadgeTierEmoticons", mock.Anything, mock.Anything, mock.Anything).Return(mockedTierMapWithEmoticons(channelID), nil)

													Convey("and a badge tier emotes filler that succeeds on filling newly created emoticons", func() {
														badgeTierEmotesFillerMock.On("PopulateWithUploadingBadgeTierEmoticons", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(mockedTierMapWithEmoticons(channelID), nil)
														chatBadgeClientMock.On("GetBitsImages", mock.Anything, mock.Anything, mock.Anything).Return(nil, nil)

														resp := badgesPostExpectSuccess(t, m, channelID, enabledBadgeChanges, map[int64]string{}, map[int64]bool{}, map[int64]string{})

														So(resp.CanUploadBadgeTierEmotes, ShouldNotBeNil)
														So(*resp.CanUploadBadgeTierEmotes, ShouldBeTrue)
														for _, tier := range resp.Tiers {
															So(tier.Enabled, ShouldEqual, mockedTierMapWithEmoticons(channelID)[tier.Threshold].Enabled)
														}
													})

													Convey("and a badge filler that errors on filling newly uploaded emoticons", func() {
														badgeTierEmotesFillerMock.On("PopulateWithUploadingBadgeTierEmoticons", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(mockedTierMapWithEmoticons(channelID), errors.New("test error"))

														respRecorder := badgesPost(m, channelID, enabledBadgeChanges, map[int64]string{}, map[int64]bool{}, map[int64]string{})

														So(respRecorder.Code, ShouldEqual, 500)
													})
												})

												Convey("and a badge tier emotes filler that errors on filling existing emoticons", func() {
													badgeTierEmotesFillerMock.On("PopulateWithBadgeTierEmoticons", mock.Anything, mock.Anything, mock.Anything).Return(mockedTierMapWithEmoticons(channelID), errors.New("test error"))

													respRecorder := badgesPost(m, channelID, enabledBadgeChanges, map[int64]string{}, map[int64]bool{}, map[int64]string{})

													So(respRecorder.Code, ShouldEqual, 500)
												})
											})

											Convey("and a badge users filler that errors", func() {
												badgeUsersFillerMock.On("PopulateWithUnlockedUsersCounts", mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("test error"))

												respRecorder := badgesPost(m, channelID, enabledBadgeChanges, map[int64]string{}, map[int64]bool{}, map[int64]string{})

												So(respRecorder.Code, ShouldEqual, 500)
											})
										})

										Convey("with a bits badge emotes saver that errors at least once", func() {
											badgeTierEmotesSaverMock.On("SaveEmoticons", mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("test error"))

											respRecorder := badgesPost(m, channelID, enabledBadgeChanges, map[int64]string{}, map[int64]bool{}, map[int64]string{})

											So(respRecorder.Code, ShouldEqual, 500)
										})
									})

									Convey("and a badge saver that errors", func() {
										badgeSaverMock.On("SaveBadgeImageSettings", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("test error"))

										respRecorder := badgesPost(m, channelID, enabledBadgeChanges, map[int64]string{}, map[int64]bool{}, map[int64]string{})

										So(respRecorder.Code, ShouldEqual, 500)
									})
								})

								Convey("returning an affiliate", func() {
									partnerManagerMock.On("GetPartnerType", mock.Anything, mock.Anything).Return(partner.AffiliatePartnerType, nil)

									Convey("and a badge saver that succeeds", func() {
										uploadImagesResponse := &chatBadgesModel.BitsUploadImagesResponse{
											Versions: map[string]chatBadgesModel.BitsBadgeUploadImageResponse{
												"1": {
													ImageURL1x: "data/uri/1",
													ImageURL2x: "data/uri/2",
													ImageURL4x: "data/uri/4",
												},
												"100": {
													ImageURL1x: "data/uri/1",
													ImageURL2x: "data/uri/2",
													ImageURL4x: "data/uri/4",
												},
												"1000": {
													ImageURL1x: "data/uri/1",
													ImageURL2x: "data/uri/2",
													ImageURL4x: "data/uri/4",
												},
												"5000000": {
													ImageURL1x: "data/uri/1",
													ImageURL2x: "data/uri/2",
													ImageURL4x: "data/uri/4",
												},
											},
										}

										badgeSaverMock.On("SaveBadgeImageSettings", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(uploadImagesResponse, nil)

										Convey("with a bits badge emotes saver that succeeds", func() {
											badgeTierEmotesSaverMock.On("SaveEmoticons", mock.Anything, mock.Anything, mock.Anything).Return(nil, nil)

											Convey("and a badge users filler that succeeds", func() {
												badgeUsersFillerMock.On("PopulateWithUnlockedUsersCounts", mock.Anything, mock.Anything, mock.Anything).Return(mockedTierMapWithUserCounts(channelID), nil)

												Convey("and a badge tier emotes filler that succeeds on filling existing emoticons", func() {
													badgeTierEmotesFillerMock.On("PopulateWithBadgeTierEmoticons", mock.Anything, mock.Anything, mock.Anything).Return(mockedTierMapWithEmoticons(channelID), nil)

													Convey("and a badge tier emotes filler that succeeds on filling newly created emoticons", func() {
														badgeTierEmotesFillerMock.On("PopulateWithUploadingBadgeTierEmoticons", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(mockedTierMapWithEmoticons(channelID), nil)
														chatBadgeClientMock.On("GetBitsImages", mock.Anything, mock.Anything, mock.Anything).Return(nil, nil)

														resp := badgesPostExpectSuccess(t, m, channelID, enabledBadgeChanges, map[int64]string{}, map[int64]bool{}, map[int64]string{})

														So(resp.CanUploadBadgeTierEmotes, ShouldNotBeNil)
														So(*resp.CanUploadBadgeTierEmotes, ShouldBeTrue)
														for _, tier := range resp.Tiers {
															So(tier.Enabled, ShouldEqual, mockedTierMapWithEmoticons(channelID)[tier.Threshold].Enabled)
														}
													})

													Convey("and a badge filler that errors on filling newly uploaded emoticons", func() {
														badgeTierEmotesFillerMock.On("PopulateWithUploadingBadgeTierEmoticons", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(mockedTierMapWithEmoticons(channelID), errors.New("test error"))

														respRecorder := badgesPost(m, channelID, enabledBadgeChanges, map[int64]string{}, map[int64]bool{}, map[int64]string{})

														So(respRecorder.Code, ShouldEqual, 500)
													})
												})

												Convey("and a badge tier emotes filler that errors on filling existing emoticons", func() {
													badgeTierEmotesFillerMock.On("PopulateWithBadgeTierEmoticons", mock.Anything, mock.Anything, mock.Anything).Return(mockedTierMapWithEmoticons(channelID), errors.New("test error"))

													respRecorder := badgesPost(m, channelID, enabledBadgeChanges, map[int64]string{}, map[int64]bool{}, map[int64]string{})

													So(respRecorder.Code, ShouldEqual, 500)
												})
											})

											Convey("and a badge users filler that errors", func() {
												badgeUsersFillerMock.On("PopulateWithUnlockedUsersCounts", mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("test error"))

												respRecorder := badgesPost(m, channelID, enabledBadgeChanges, map[int64]string{}, map[int64]bool{}, map[int64]string{})

												So(respRecorder.Code, ShouldEqual, 500)
											})
										})

										Convey("with a bits badge emotes saver that errors at least once", func() {
											badgeTierEmotesSaverMock.On("SaveEmoticons", mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("test error"))

											respRecorder := badgesPost(m, channelID, enabledBadgeChanges, map[int64]string{}, map[int64]bool{}, map[int64]string{})

											So(respRecorder.Code, ShouldEqual, 500)
										})
									})

									Convey("and a badge saver that errors", func() {
										badgeSaverMock.On("SaveBadgeImageSettings", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("test error"))

										respRecorder := badgesPost(m, channelID, enabledBadgeChanges, map[int64]string{}, map[int64]bool{}, map[int64]string{})

										So(respRecorder.Code, ShouldEqual, 500)
									})
								})

								Convey("returning any other type of user", func() {
									partnerManagerMock.On("GetPartnerType", mock.Anything, mock.Anything).Return(partner.PartnerType("unsupported type"), nil)

									resp := badgesPostExpectSuccess(t, m, channelID, enabledBadgeChanges, map[int64]string{}, map[int64]bool{}, map[int64]string{})

									So(resp, ShouldNotBeNil)
									So(resp.Tiers, ShouldBeEmpty)
									So(resp.CanUploadBadgeTierEmotes, ShouldBeNil)
								})
							})

							Convey("and a partner manager that errors", func() {
								partnerManagerMock.On("GetPartnerType", mock.Anything, mock.Anything).Return(partner.PartnerType(""), errors.New("test error"))

								respRecorder := badgesPost(m, channelID, enabledBadgeChanges, map[int64]string{}, map[int64]bool{}, map[int64]string{})

								So(respRecorder.Code, ShouldEqual, 500)
							})
						})
					})

					Convey("and a badge tier DAO that errors on GETs", func() {
						tierDaoMock.On("GetAll", mock.Anything, mock.Anything).Return(nil, errors.New("test error"))

						respRecorder := badgesPost(m, channelID, enabledBadgeChanges, map[int64]string{}, map[int64]bool{}, map[int64]string{})

						So(respRecorder.Code, ShouldEqual, 500)
					})

				})

				Convey("and a badge tier DAO that errors on PUTs", func() {
					tierDaoMock.On("PutAll", mock.Anything, mock.Anything).Return(errors.New("test error"))

					respRecorder := badgesPost(m, channelID, enabledBadgeChanges, map[int64]string{}, map[int64]bool{}, map[int64]string{})

					So(respRecorder.Code, ShouldEqual, 500)
				})

			})

			Convey("with invalid badge tiers to set", func() {
				enabledBadgeChanges := map[int64]bool{
					69:  true,
					420: false,
				}

				respRecorder := badgesPost(m, channelID, enabledBadgeChanges, map[int64]string{}, map[int64]bool{}, map[int64]string{})

				So(respRecorder.Code, ShouldEqual, 400)
			})
		})

		Convey("and an invalid channelID", func() {
			channelID := "six"

			respRecorder := badgesPost(m, channelID, map[int64]bool{}, map[int64]string{}, map[int64]bool{}, map[int64]string{})

			So(respRecorder.Code, ShouldEqual, 400)
		})
	})
}

func TestBadgeApi_SetBadgeTiersAdmin(t *testing.T) {
	Convey("Given valid badge tier endpionts", t, func() {
		m := setupBadgeTierEndpoints()

		Convey("and a valid channelID param", func() {
			channelID := "6"

			Convey("with valid badge tiers to set", func() {
				enabledBadgeChanges := map[int64]bool{
					1:       true,
					100:     false,
					1000:    false,
					5000000: true,
				}

				Convey("and a partner manager that succeeds", func() {
					Convey("returning a regular partner", func() {
						partnerManagerMock.On("GetPartnerType", mock.Anything, mock.Anything).Return(partner.TraditionalPartnerType, nil)

						Convey("and a badge tier DAO that succeeds on PUTs", func() {
							tierDaoMock.On("PutAll", mock.Anything).Return(nil)

							Convey("with no custom tiers", func() {
								tierDaoMock.On("GetAll", mock.Anything, mock.Anything).Return(nil, nil)

								Convey("and a badge saver that succeeds", func() {
									uploadImagesResponse := &chatBadgesModel.BitsUploadImagesResponse{
										Versions: map[string]chatBadgesModel.BitsBadgeUploadImageResponse{
											"1": {
												ImageURL1x: "data/uri/1",
												ImageURL2x: "data/uri/2",
												ImageURL4x: "data/uri/4",
											},
											"100": {
												ImageURL1x: "data/uri/1",
												ImageURL2x: "data/uri/2",
												ImageURL4x: "data/uri/4",
											},
											"1000": {
												ImageURL1x: "data/uri/1",
												ImageURL2x: "data/uri/2",
												ImageURL4x: "data/uri/4",
											},
											"5000000": {
												ImageURL1x: "data/uri/1",
												ImageURL2x: "data/uri/2",
												ImageURL4x: "data/uri/4",
											},
										},
									}

									badgeSaverMock.On("SaveBadgeImageSettings", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(uploadImagesResponse, nil)

									Convey("with a bits badge emotes saver that succeeds", func() {
										badgeTierEmotesSaverMock.On("SaveEmoticons", mock.Anything, mock.Anything, mock.Anything).Return(nil, nil)

										Convey("and a badge users filler that succeeds", func() {
											badgeUsersFillerMock.On("PopulateWithUnlockedUsersCounts", mock.Anything, mock.Anything, mock.Anything).Return(mockedTierMapWithUserCounts(channelID), nil)

											Convey("and a badge tier emotes filler that succeeds on filling existing emoticons", func() {
												badgeTierEmotesFillerMock.On("PopulateWithBadgeTierEmoticons", mock.Anything, mock.Anything, mock.Anything).Return(mockedTierMapWithEmoticons(channelID), nil)

												Convey("and a badge tier emotes filler that succeeds on filling newly created emoticons", func() {
													badgeTierEmotesFillerMock.On("PopulateWithUploadingBadgeTierEmoticons", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(mockedTierMapWithEmoticons(channelID), nil)
													chatBadgeClientMock.On("GetBitsImages", mock.Anything, mock.Anything, mock.Anything).Return(nil, nil)

													respRecorder := adminSetBadgeTiers(m, channelID, enabledBadgeChanges, map[int64]string{}, map[int64]bool{}, map[int64]string{})

													So(respRecorder.Code, ShouldEqual, 200)
												})
											})
										})
									})
								})
							})
						})
					})

					Convey("returning an affiliate", func() {
						partnerManagerMock.On("GetPartnerType", mock.Anything, mock.Anything).Return(partner.AffiliatePartnerType, nil)

						Convey("and a badge tier DAO that succeeds on PUTs", func() {
							tierDaoMock.On("PutAll", mock.Anything).Return(nil)

							Convey("with no custom tiers", func() {
								tierDaoMock.On("GetAll", mock.Anything, mock.Anything).Return(nil, nil)

								Convey("and a badge saver that succeeds", func() {
									uploadImagesResponse := &chatBadgesModel.BitsUploadImagesResponse{
										Versions: map[string]chatBadgesModel.BitsBadgeUploadImageResponse{
											"1": {
												ImageURL1x: "data/uri/1",
												ImageURL2x: "data/uri/2",
												ImageURL4x: "data/uri/4",
											},
											"100": {
												ImageURL1x: "data/uri/1",
												ImageURL2x: "data/uri/2",
												ImageURL4x: "data/uri/4",
											},
											"1000": {
												ImageURL1x: "data/uri/1",
												ImageURL2x: "data/uri/2",
												ImageURL4x: "data/uri/4",
											},
											"5000000": {
												ImageURL1x: "data/uri/1",
												ImageURL2x: "data/uri/2",
												ImageURL4x: "data/uri/4",
											},
										},
									}

									badgeSaverMock.On("SaveBadgeImageSettings", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(uploadImagesResponse, nil)

									Convey("with a bits badge emotes saver that succeeds", func() {
										badgeTierEmotesSaverMock.On("SaveEmoticons", mock.Anything, mock.Anything, mock.Anything).Return(nil, nil)

										Convey("and a badge users filler that succeeds", func() {
											badgeUsersFillerMock.On("PopulateWithUnlockedUsersCounts", mock.Anything, mock.Anything, mock.Anything).Return(mockedTierMapWithUserCounts(channelID), nil)

											Convey("and a badge tier emotes filler that succeeds on filling existing emoticons", func() {
												badgeTierEmotesFillerMock.On("PopulateWithBadgeTierEmoticons", mock.Anything, mock.Anything, mock.Anything).Return(mockedTierMapWithEmoticons(channelID), nil)

												Convey("and a badge tier emotes filler that succeeds on filling newly created emoticons", func() {
													badgeTierEmotesFillerMock.On("PopulateWithUploadingBadgeTierEmoticons", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(mockedTierMapWithEmoticons(channelID), nil)
													chatBadgeClientMock.On("GetBitsImages", mock.Anything, mock.Anything, mock.Anything).Return(nil, nil)

													respRecorder := adminSetBadgeTiers(m, channelID, enabledBadgeChanges, map[int64]string{}, map[int64]bool{}, map[int64]string{})

													So(respRecorder.Code, ShouldEqual, 200)
												})
											})
										})
									})
								})
							})
						})
					})

					Convey("returning any other type of user", func() {
						Convey("and a badge tier DAO that succeeds on PUTs", func() {
							tierDaoMock.On("PutAll", mock.Anything).Return(nil)

							Convey("with no custom tiers", func() {
								tierDaoMock.On("GetAll", mock.Anything, mock.Anything).Return(nil, nil)

								partnerManagerMock.On("GetPartnerType", mock.Anything, mock.Anything).Return(partner.PartnerType("unsupported type"), nil)

								respRecorder := adminSetBadgeTiers(m, channelID, enabledBadgeChanges, map[int64]string{}, map[int64]bool{}, map[int64]string{})

								So(respRecorder.Code, ShouldEqual, 200)

								// no badge images are saved
								badgeSaverMock.AssertNotCalled(t, "SaveBadgeImageSettings", mock.Anything, mock.Anything, mock.Anything, mock.Anything)
							})
						})
					})
				})
			})
		})
	})
}
