package api

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"code.justin.tv/commerce/payday/api/apidef"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/decorators"
	"code.justin.tv/commerce/payday/errors"
	httperrors "code.justin.tv/commerce/payday/errors/http"
	datascience_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/datascience"
	prices_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/prices"
	products_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/products"
	userservice_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/userservice"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/models/api"
	petozi "code.justin.tv/commerce/petozi/rpc"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
	"github.com/zenazn/goji/web"
)

func TestPricesApi_GetPrices(t *testing.T) {
	Convey("Given a prices API", t, func() {

		dataScience := new(datascience_mock.DataScience)
		userServiceChecker := new(userservice_mock.Checker)
		floPricer := new(prices_mock.IFloPricer)
		loggedOutResolver := new(products_mock.LoggedOutResolver)

		tuid := "123123123"
		asin := "turbo-bits"

		pricesApi := PricesApi{
			DataScience:        dataScience,
			Config:             &config.Configuration{},
			FloPricer:          floPricer,
			UserServiceChecker: userServiceChecker,
			LoggedOutResolver:  loggedOutResolver,
		}

		dataScience.On("TrackBitsEvent", mock.Anything, mock.Anything, mock.Anything).Return(nil)

		Convey("when user check fails, returns an error", func() {
			userServiceChecker.On("IsBanned", mock.Anything, tuid).Return(false, errors.New("no anime, pls error"))

			r, _ := http.NewRequest("GET", fmt.Sprintf("/prices?user_id=%v", tuid), nil)
			w := httptest.NewRecorder()

			m := web.New()
			m.Get("/prices", decorators.DecorateAndAdapt(pricesApi.GetPrices, apidef.GetPrices))
			m.ServeHTTP(w, r)
			So(w.Code, ShouldEqual, 500)

			var response httperrors.ErrorMessageResponse
			err := json.Unmarshal(w.Body.Bytes(), &response)
			So(err, ShouldBeNil)
			So(response.Message, ShouldEqual, "Error getting user status")
		})

		Convey("when banned, returns empty pricing", func() {
			userServiceChecker.On("IsBanned", mock.Anything, tuid).Return(true, nil)

			r, _ := http.NewRequest("GET", fmt.Sprintf("/prices?user_id=%v", tuid), nil)
			w := httptest.NewRecorder()

			m := web.New()
			m.Get("/prices", decorators.DecorateAndAdapt(pricesApi.GetPrices, apidef.GetPrices))
			m.ServeHTTP(w, r)
			So(w.Code, ShouldEqual, 200)

			var response api.GetPricesResponse
			err := json.Unmarshal(w.Body.Bytes(), &response)
			So(err, ShouldBeNil)
			So(response.Prices, ShouldHaveLength, 0)
		})

		Convey("when user is not banned", func() {
			userServiceChecker.On("IsBanned", mock.Anything, tuid).Return(false, nil)

			Convey("when the logged out resolver fails", func() {
				loggedOutResolver.On("Resolve", mock.Anything, mock.Anything).Return(nil, errors.New("test error"))
				r, _ := http.NewRequest("GET", fmt.Sprintf("/prices?user_id=%v", tuid), nil)
				w := httptest.NewRecorder()

				m := web.New()
				m.Get("/prices", decorators.DecorateAndAdapt(pricesApi.GetPrices, apidef.GetPrices))
				m.ServeHTTP(w, r)
				So(w.Code, ShouldEqual, 500)
			})

			Convey("when the logged out resolver succeeds", func() {
				loggedOutResolver.On("Resolve", mock.Anything, mock.Anything).Return(map[string]*petozi.BitsProduct{
					asin: {
						Quantity:          50,
						ShowWhenLoggedOut: true,
					},
				}, nil)

				Convey("flo pricer returns an error", func() {
					floPricer.On("GetPrices", mock.Anything, mock.Anything, tuid, mock.Anything).Return(nil, nil, errors.New("pricing broke"))
					r, _ := http.NewRequest("GET", fmt.Sprintf("/prices?user_id=%v", tuid), nil)
					w := httptest.NewRecorder()

					m := web.New()
					m.Get("/prices", decorators.DecorateAndAdapt(pricesApi.GetPrices, apidef.GetPrices))
					m.ServeHTTP(w, r)
					So(w.Code, ShouldEqual, 500)

					var response httperrors.ErrorMessageResponse
					err := json.Unmarshal(w.Body.Bytes(), &response)
					So(err, ShouldBeNil)
					So(response.Message, ShouldEqual, "Encountered unknown error from downstream pricing dependency")
				})

				Convey("flo pricer returns no results", func() {
					floPricer.On("GetPrices", mock.Anything, mock.Anything, tuid, mock.Anything).Return([]models.Price{}, nil, nil)

					r, _ := http.NewRequest("GET", fmt.Sprintf("/prices?user_id=%v", tuid), nil)
					w := httptest.NewRecorder()

					m := web.New()
					m.Get("/prices", decorators.DecorateAndAdapt(pricesApi.GetPrices, apidef.GetPrices))
					m.ServeHTTP(w, r)
					So(w.Code, ShouldEqual, 200)

					var response api.GetPricesResponse
					err := json.Unmarshal(w.Body.Bytes(), &response)
					So(err, ShouldBeNil)
					So(response.Prices, ShouldHaveLength, 0)
				})

				Convey("flo pricer returns one result", func() {
					floPricer.On("GetPrices", mock.Anything, mock.Anything, tuid, mock.Anything).Return([]models.Price{
						{
							ProductId:    asin,
							Price:        "1.99",
							Currency:     "USD",
							TaxInclusive: false,
						},
					}, nil, nil)

					r, _ := http.NewRequest("GET", fmt.Sprintf("/prices?user_id=%v", tuid), nil)
					w := httptest.NewRecorder()

					m := web.New()
					m.Get("/prices", decorators.DecorateAndAdapt(pricesApi.GetPrices, apidef.GetPrices))
					m.ServeHTTP(w, r)
					So(w.Code, ShouldEqual, 200)

					var response api.GetPricesResponse
					err := json.Unmarshal(w.Body.Bytes(), &response)
					So(err, ShouldBeNil)

					So(response.Prices, ShouldHaveLength, 1)
					So(response.Prices[0].ASIN, ShouldEqual, asin)
					So(response.Prices[0].BitsAmount, ShouldEqual, 50)
					So(response.Prices[0].Price, ShouldEqual, "1.99")
					So(response.Prices[0].USPrice, ShouldEqual, "1.99")
					So(response.Prices[0].CurrencyUnit, ShouldEqual, "USD")
				})
			})
		})
	})
}

func TestPricesApi_ResolveAsins(t *testing.T) {
	Convey("Given a prices API", t, func() {
		loggedOutResolver := new(products_mock.LoggedOutResolver)
		productsGetter := new(products_mock.Getter)

		asin := "turbo-bits"

		pricesApi := PricesApi{
			Config:            &config.Configuration{},
			LoggedOutResolver: loggedOutResolver,
			ProductsGetter:    productsGetter,
		}

		ctx := context.Background()

		Convey("resolving asins from configs", func() {
			productIDs := make([]string, 0)

			Convey("when the logged out resolver fails", func() {
				loggedOutResolver.On("Resolve", ctx, petozi.Platform_AMAZON).Return(nil, errors.New("WALRUS STRIKE"))

				_, err := pricesApi.resolveProducts(ctx, productIDs, petozi.Platform_AMAZON)
				So(err, ShouldNotBeNil)
			})

			Convey("when the logged out resolver succeeds", func() {
				defaultProducts := map[string]*petozi.BitsProduct{
					asin: {
						Platform: petozi.Platform_AMAZON,
						Id:       asin,
						Quantity: 322,
					},
				}

				loggedOutResolver.On("Resolve", ctx, petozi.Platform_AMAZON).Return(defaultProducts, nil)

				asins, err := pricesApi.resolveProducts(ctx, productIDs, petozi.Platform_AMAZON)
				So(err, ShouldBeNil)
				So(asins, ShouldHaveLength, 1)
				So(asins[asin], ShouldNotBeNil)
			})
		})

		Convey("returning passed in asins", func() {
			productIDs := []string{"secret-sku", "double-secret-sku"}

			Convey("when the products getter fails", func() {
				productsGetter.On("GetByIDs", ctx, petozi.Platform_AMAZON, productIDs).Return(nil, errors.New("WALRUS STRIKE"))

				_, err := pricesApi.resolveProducts(ctx, productIDs, petozi.Platform_AMAZON)
				So(err, ShouldNotBeNil)
			})

			Convey("when the products getter succeeds", func() {
				defaultProducts := map[string]*petozi.BitsProduct{
					"secret-sku": {
						Platform: petozi.Platform_AMAZON,
						Id:       "secret-sku",
						Quantity: 322,
					},
					"double-secret-sku": {
						Platform: petozi.Platform_AMAZON,
						Id:       "double-secret-sku",
						Quantity: 644,
					},
				}

				productsGetter.On("GetByIDs", ctx, petozi.Platform_AMAZON, productIDs).Return(defaultProducts, nil)

				asins, err := pricesApi.resolveProducts(ctx, productIDs, petozi.Platform_AMAZON)
				So(err, ShouldBeNil)
				So(asins, ShouldHaveLength, 2)
				So(asins["secret-sku"], ShouldNotBeNil)
				So(asins["double-secret-sku"], ShouldNotBeNil)
			})
		})
	})
}

func TestPricesApi_getFinalPurchaseUrl(t *testing.T) {
	Convey("Given a prices API", t, func() {
		pricesApi := PricesApi{
			Config: &config.Configuration{
				CheckoutBaseURL: "https://testing.twitch.tech",
			},
		}

		Convey("with a platform of Amazon", func() {
			actual := pricesApi.getFinalPurchaseUrl(models.AMAZON, models.Price{ProductId: "testingASIN"})
			So(actual, ShouldEqual, "https://testing.twitch.tech/bits-checkout/purchase?asin=testingASIN")
		})

		Convey("with a platform of PayPal", func() {
			actual := pricesApi.getFinalPurchaseUrl(models.PAYPAL, models.Price{ProductId: "testingASIN"})
			So(actual, ShouldEqual, "https://testing.twitch.tech/bits-checkout/summary?asin=testingASIN")
		})

		Convey("with a platform of Xsolla", func() {
			actual := pricesApi.getFinalPurchaseUrl(models.XSOLLA, models.Price{ProductId: "testingASIN"})
			So(actual, ShouldEqual, "https://testing.twitch.tech/bits-checkout/other?asin=testingASIN")
		})

		Convey("with a platform of default", func() {
			actual := pricesApi.getFinalPurchaseUrl(models.DEFAULT, models.Price{ProductId: "testingASIN"})
			So(actual, ShouldEqual, "https://testing.twitch.tech/bits-checkout/select?asin=testingASIN")
		})

		Convey("with a platform that isn't mapped", func() {
			actual := pricesApi.getFinalPurchaseUrl(models.Platform("foobar"), models.Price{ProductId: "testingASIN"})
			So(actual, ShouldEqual, "https://testing.twitch.tech/bits-checkout/select?asin=testingASIN")
		})
	})
}

func TestPricesApi_getDiscountsBase(t *testing.T) {
	Convey("Given a prices API", t, func() {
		sku := "B017L2UX4C"
		Convey("when no base price found", func() {
			bitsProducts := map[string]*petozi.BitsProduct{
				sku: {
					Platform: petozi.Platform_AMAZON,
					Id:       sku,
					Quantity: 100,
				},
			}

			Convey("with no flo prices provided", func() {
				pricesApi := PricesApi{
					Config: &config.Configuration{},
				}

				baseBitsPrice, baseBitsAmount := pricesApi.getDiscountsBase([]models.Price{}, bitsProducts)
				So(baseBitsPrice, ShouldEqual, 0)
				So(baseBitsAmount, ShouldEqual, 0)
			})

			Convey("with no bundle found", func() {
				pricesApi := PricesApi{
					Config: &config.Configuration{},
				}
				floPrices := []models.Price{
					{
						ProductId: "some-different-sku",
						Price:     "1.4",
					},
				}

				baseBitsPrice, baseBitsAmount := pricesApi.getDiscountsBase(floPrices, bitsProducts)
				So(baseBitsPrice, ShouldEqual, 0)
				So(baseBitsAmount, ShouldEqual, 0)
			})

			Convey("with invalid flo price", func() {
				pricesApi := PricesApi{
					Config: &config.Configuration{},
				}
				floPrices := []models.Price{
					{
						ProductId: sku,
						Price:     "1.4a", //invalid price
					},
				}

				baseBitsPrice, baseBitsAmount := pricesApi.getDiscountsBase(floPrices, bitsProducts)
				So(baseBitsPrice, ShouldEqual, 0)
				So(baseBitsAmount, ShouldEqual, 0)
			})

			Convey("when first bundle found has amount of 0", func() {
				bitsProducts[sku].Quantity = 0
				pricesApi := PricesApi{
					Config: &config.Configuration{},
				}
				floPrices := []models.Price{
					{
						ProductId: sku,
						Price:     "1.4",
					},
				}

				baseBitsPrice, baseBitsAmount := pricesApi.getDiscountsBase(floPrices, bitsProducts)
				So(baseBitsPrice, ShouldEqual, 0)
				So(baseBitsAmount, ShouldEqual, 0)
			})
		})

		Convey("with valid bundles to calculate the worst price", func() {
			sku1 := "sku1"
			sku2 := "sku2"
			sku3 := "sku3"

			bitsProducts := map[string]*petozi.BitsProduct{
				sku1: {
					Platform: petozi.Platform_AMAZON,
					Id:       sku,
					Quantity: 100,
				},
				sku2: {
					Platform: petozi.Platform_AMAZON,
					Id:       sku,
					Quantity: 500,
				},
				sku3: {
					Platform: petozi.Platform_AMAZON,
					Id:       sku,
					Quantity: 1000,
				},
			}

			pricesApi := PricesApi{
				Config: &config.Configuration{},
			}
			floPrices := []models.Price{
				{
					ProductId: sku1,
					Price:     "1.4",
				},
				{
					ProductId: sku2,
					Price:     "7",
				},
				{
					ProductId: sku3,
					Price:     "30.5",
				},
			}

			baseBitsPrice, baseBitsAmount := pricesApi.getDiscountsBase(floPrices, bitsProducts)
			So(baseBitsPrice, ShouldEqual, 30.5)
			So(baseBitsAmount, ShouldEqual, 1000)
		})

		Convey("with valid and sometimes invalid bundles to calculate the worst price", func() {
			sku1 := "sku1"
			sku2 := "sku2"
			sku3 := "sku3"
			bitsProducts := map[string]*petozi.BitsProduct{
				sku1: {
					Platform: petozi.Platform_AMAZON,
					Id:       sku,
					Quantity: 100,
				},
				sku2: {
					Platform: petozi.Platform_AMAZON,
					Id:       sku,
					Quantity: 500,
				},
				sku3: {
					Platform: petozi.Platform_AMAZON,
					Id:       sku,
					Quantity: 0,
				},
			}

			pricesApi := PricesApi{
				Config: &config.Configuration{},
			}
			floPrices := []models.Price{
				{
					ProductId: sku1,
					Price:     "1.4",
				},
				{
					ProductId: sku2,
					Price:     "10", //worst price!
				},
				{
					ProductId: "invalid-sku",
					Price:     "3.0",
				},
				{
					ProductId: sku3,
					Price:     "1.2aaa", //invalid price, should be ignored in calculation
				},
				{
					ProductId: sku3,
					Price:     "1.2",
				},
			}

			baseBitsPrice, baseBitsAmount := pricesApi.getDiscountsBase(floPrices, bitsProducts)
			So(baseBitsPrice, ShouldEqual, 10)
			So(baseBitsAmount, ShouldEqual, 500)
		})
	})
}

func TestPricesApi_getDiscount(t *testing.T) {
	Convey("Given a prices API", t, func() {
		Convey("with invalid inputs", func() {
			Convey("with an invalid target bits price string", func() {
				discount := getDiscount(140, 100, "140a", 100)
				So(discount, ShouldEqual, 0)
			})

			Convey("with base bits price of 0", func() {
				discount := getDiscount(0, 100, "140", 100)
				So(discount, ShouldEqual, 0)
			})

			Convey("with base bits amount of 0", func() {
				discount := getDiscount(140, 0, "140", 100)
				So(discount, ShouldEqual, 0)
			})

			Convey("with target bits amount of 0", func() {
				discount := getDiscount(0, 100, "140", 0)
				So(discount, ShouldEqual, 0)
			})
		})

		Convey("with valid inputs", func() {
			Convey("with no discount", func() {
				discount := getDiscount(1.4, 100, "1.4", 100)
				So(discount, ShouldEqual, 0)
			})

			Convey("with 5% discount", func() {
				discount := getDiscount(140, 100, "1995", 1500)
				So(discount, ShouldEqual, 5)
			})

			Convey("with 8% discount", func() {
				discount := getDiscount(140, 100, "6440", 5000)
				So(discount, ShouldEqual, 8)
			})

			Convey("with 10% discount", func() {
				discount := getDiscount(140, 100, "12600", 10000)
				So(discount, ShouldEqual, 10)
			})

			Convey("with 12% discount", func() {
				discount := getDiscount(140, 100, "30800", 25000)
				So(discount, ShouldEqual, 12)
			})
		})
	})
}
