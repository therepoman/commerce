package api

import (
	"net/http"

	"code.justin.tv/commerce/payday/actions"
	"code.justin.tv/commerce/payday/actions/constants"
	apiconstants "code.justin.tv/commerce/payday/api/constants"
	"code.justin.tv/commerce/payday/api/utils"
	"code.justin.tv/commerce/payday/emote"
	"code.justin.tv/commerce/payday/errors"
	httperror "code.justin.tv/commerce/payday/errors/http"
	"code.justin.tv/commerce/payday/models/api"
	"github.com/zenazn/goji/web"
)

type ActionInfoApi struct {
	PrefixManager emote.IPrefixManager `inject:""`
	ActionCache   *actions.ActionCache `inject:""`
}

func (actionApi *ActionInfoApi) GetActionsInfo(c web.C, w http.ResponseWriter, r *http.Request) error {
	ctx, cancel := utils.ContextWithDefaultTimeout(r.Context())
	defer cancel()

	channelId := r.URL.Query().Get("channel_id")
	if channelId == "" {
		return httperror.NewWithCause(apiconstants.InvalidRequestParam, http.StatusBadRequest, errors.New("channelId cannot be empty."))
	}

	actions := actionApi.ActionCache.GetAll()

	prefixes, backgrounds, scales, states, tiers := make([]string, 0), make([]string, 0), make([]string, 0), make([]string, 0), make([]string, 0)

	for _, action := range actions {
		prefixes = append(prefixes, action.Prefix)
	}

	for background := range constants.Backgrounds {
		backgrounds = append(backgrounds, string(background))
	}

	for state := range constants.AnimationTypes {
		states = append(states, string(state))
	}

	for tier := range constants.Tiers {
		tiers = append(tiers, string(tier))
	}

	for scale := range constants.Scales {
		scales = append(scales, string(scale))
	}

	customPrefix, hasCustomCheerPrefix, err := actionApi.PrefixManager.GetCustomCheermoteAction(ctx, channelId)
	if err == nil && hasCustomCheerPrefix {
		prefixes = append(prefixes, customPrefix)
	}

	response := &api.GetActionsInfoResponse{
		Prefixes:    prefixes,
		Backgrounds: backgrounds,
		States:      states,
		Tiers:       tiers,
		Scales:      scales,
	}

	utils.WriteResponse(response, w)
	return nil
}
