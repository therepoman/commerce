package api

import (
	"bytes"
	"encoding/json"
	"errors"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/stretchr/testify/mock"

	"code.justin.tv/commerce/payday/api/apidef"
	"code.justin.tv/commerce/payday/api/constants"
	"code.justin.tv/commerce/payday/cartman"
	"code.justin.tv/commerce/payday/decorators"
	"code.justin.tv/commerce/payday/dynamo"
	httperrors "code.justin.tv/commerce/payday/errors/http"
	dynamo_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo"
	user_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/user"
	userstate_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/userstate"
	"code.justin.tv/commerce/payday/models"
	apimodel "code.justin.tv/commerce/payday/models/api"
	"github.com/stretchr/testify/assert"
	"github.com/zenazn/goji/web"
)

var userDaoMock = new(dynamo_mock.IUserDao)
var eligibilityRetriever = new(user_mock.Checker)
var userStateGetter = new(userstate_mock.Getter)

func SetupUserEndpoints() *web.Mux {
	userDaoMock = new(dynamo_mock.IUserDao)
	eligibilityRetriever = new(user_mock.Checker)
	userStateGetter = new(userstate_mock.Getter)

	context := &UserApi{
		Dao:              userDaoMock,
		UserChecker:      eligibilityRetriever,
		CartmanValidator: cartman.NewAlwaysValidValidator(),
		UserStateGetter:  userStateGetter,
	}

	m := web.New()
	m.Get("/users/:tuid/banned", decorators.DecorateAndAdapt(context.GetBanned, apidef.AdminGetUserBanned))
	m.Put("/users/:tuid/banned", decorators.DecorateAndAdapt(context.SetBanned, apidef.AdminSetUserBanned))
	m.Get("/users/:tuid", decorators.DecorateAndAdapt(context.GetUserInfoWithCartmanAuthentication, apidef.GetUserInfo))
	return m
}

func TestGetBannedNonNumericUser(t *testing.T) {
	m := SetupUserEndpoints()

	r, _ := http.NewRequest("GET", "/users/user/banned", nil)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 400, w.Code)
	resp, err := httperrors.ConvertJsonToErrorMessageResponse(w.Body.Bytes())
	assert.Nil(t, err)
	assert.Equal(t, constants.NonNumericUserIdError, resp.Message)
}

func TestGetBannedNotInDynamo(t *testing.T) {
	m := SetupUserEndpoints()

	userDaoMock.On("Get", dynamo.UserId("123")).Return(nil, nil)
	r, _ := http.NewRequest("GET", "/users/123/banned", nil)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 404, w.Code)
	resp, err := httperrors.ConvertJsonToErrorMessageResponse(w.Body.Bytes())
	assert.Nil(t, err)
	assert.Equal(t, userNotFoundError, resp.Message)
}

func TestGetBannedExists(t *testing.T) {
	m := SetupUserEndpoints()

	expectedUser := &dynamo.User{
		Id:         "123",
		Banned:     true,
		BannedBy:   "testBanner",
		Annotation: "test",
	}
	userDaoMock.On("Get", expectedUser.Id).Return(expectedUser, nil)

	r, _ := http.NewRequest("GET", "/users/123/banned", nil)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 200, w.Code)

	resultJson := w.Body.Bytes()
	result, err := apimodel.ConvertJsonToGetBannedResponse(resultJson)
	if err != nil {
		t.Fail()
	}

	actualUser := models.GetBannedResponseToDynamoUser(result)
	assert.True(t, expectedUser.Equals(actualUser))
}

func TestSetBannedNonNumericUser(t *testing.T) {
	m := SetupUserEndpoints()

	request := &apimodel.SetBannedRequest{
		Banned:     true,
		BannedBy:   "testBanner",
		Annotation: "test",
	}
	requestJson, _ := json.Marshal(request)
	requestJsonReader := bytes.NewReader(requestJson)
	r, _ := http.NewRequest("PUT", "/users/user/banned", requestJsonReader)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 400, w.Code)
	resp, err := httperrors.ConvertJsonToErrorMessageResponse(w.Body.Bytes())
	assert.Nil(t, err)
	assert.Equal(t, constants.NonNumericUserIdError, resp.Message)
}

func TestSetBannedInvalidJson(t *testing.T) {
	m := SetupUserEndpoints()

	requestJsonReader := strings.NewReader("NotValidJson")
	r, _ := http.NewRequest("PUT", "/users/123/banned", requestJsonReader)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 400, w.Code)
	resp, err := httperrors.ConvertJsonToErrorMessageResponse(w.Body.Bytes())
	assert.Nil(t, err)
	assert.Equal(t, constants.InvalidJsonError, resp.Message)
}

func TestSetBannedDynamoGetFail(t *testing.T) {
	userId := "12345"
	m := SetupUserEndpoints()

	request := &apimodel.SetBannedRequest{
		Banned:     true,
		BannedBy:   "testBanner",
		Annotation: "test",
	}

	userDaoMock.On("Get", dynamo.UserId(userId)).Return(nil, errors.New("test error"))

	requestJson, _ := json.Marshal(request)
	requestJsonReader := bytes.NewReader(requestJson)
	r, _ := http.NewRequest("PUT", "/users/12345/banned", requestJsonReader)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 500, w.Code)
	resp, err := httperrors.ConvertJsonToErrorMessageResponse(w.Body.Bytes())
	assert.Nil(t, err)
	assert.Equal(t, constants.DownstreamDependencyError, resp.Message)
}

func TestSetBanned(t *testing.T) {
	userId := "12345"
	m := SetupUserEndpoints()

	request := &apimodel.SetBannedRequest{
		Banned:     true,
		BannedBy:   "testBanner",
		Annotation: "test",
	}

	bannedUserPutObject := models.SetBannedRequestToDynamoUser(request, userId)
	userDaoMock.On("Put", bannedUserPutObject).Return(nil)
	userDaoMock.On("Get", dynamo.UserId(userId)).Return(nil, nil)

	requestJson, _ := json.Marshal(request)
	requestJsonReader := bytes.NewReader(requestJson)
	r, _ := http.NewRequest("PUT", "/users/12345/banned", requestJsonReader)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 200, w.Code)
}

func TestSetBannedWithExistingDynamoEntry(t *testing.T) {
	userId := "12345"
	m := SetupUserEndpoints()

	request := &apimodel.SetBannedRequest{
		Banned:     true,
		BannedBy:   "testBanner",
		Annotation: "test",
	}

	existingUser := dynamo.User{
		Skipped: true,
	}

	bannedUserPutObject := models.SetBannedRequestToDynamoUser(request, userId)
	bannedUserPutObject.Skipped = true
	userDaoMock.On("Get", dynamo.UserId(userId)).Return(&existingUser, nil)
	userDaoMock.On("Update", bannedUserPutObject).Return(nil)

	requestJson, _ := json.Marshal(request)
	requestJsonReader := bytes.NewReader(requestJson)
	r, _ := http.NewRequest("PUT", "/users/12345/banned", requestJsonReader)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 200, w.Code)
}

func TestEligibleToUseBitsNonNumericUserId(t *testing.T) {
	m := SetupUserEndpoints()

	r, _ := http.NewRequest("GET", "/users/user", nil)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 400, w.Code)
	resp, err := httperrors.ConvertJsonToErrorMessageResponse(w.Body.Bytes())
	assert.Nil(t, err)
	assert.Equal(t, constants.NonNumericUserIdError, resp.Message)
}

func TestEligibleToUseBitsButCacheBroken(t *testing.T) {
	m := SetupUserEndpoints()
	userId := "123456"

	eligibilityRetriever.On("IsEligible", mock.Anything, userId).Return(true, nil)
	userStateGetter.On("Get", userId).Return(apimodel.UserStateUnknown, errors.New("test broken cache"))

	// No banned
	r, _ := http.NewRequest("GET", "/users/123456", nil)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 500, w.Code)
	resp, err := httperrors.ConvertJsonToErrorMessageResponse(w.Body.Bytes())
	assert.Nil(t, err)
	assert.Equal(t, constants.DownstreamDependencyError, resp.Message)
}

func TestEligibleToUseBits(t *testing.T) {
	m := SetupUserEndpoints()
	userId := "123456"

	eligibilityRetriever.On("IsEligible", mock.Anything, userId).Return(true, nil)
	userStateGetter.On("Get", userId).Return(apimodel.UserStateCheered, nil)

	// No banned
	r, _ := http.NewRequest("GET", "/users/123456", nil)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 200, w.Code)
	response, err := apimodel.ConvertJsonToGetUserInfoResponse(w.Body.Bytes())
	assert.NoError(t, err)
	assert.True(t, response.Eligible)
}

func TestNotEligibleToUseBits(t *testing.T) {
	m := SetupUserEndpoints()
	userId := "123456"

	eligibilityRetriever.On("IsEligible", mock.Anything, userId).Return(false, nil)
	userStateGetter.On("Get", userId).Return(apimodel.UserStateCheered, nil)

	r, _ := http.NewRequest("GET", "/users/123456", nil)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 200, w.Code)
	response, err := apimodel.ConvertJsonToGetUserInfoResponse(w.Body.Bytes())
	assert.NoError(t, err)
	assert.False(t, response.Eligible)
}
