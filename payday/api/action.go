package api

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"sort"
	"strconv"
	"strings"
	"time"

	"code.justin.tv/commerce/gogogadget/ttlcache"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/actions"
	"code.justin.tv/commerce/payday/api/clienttracking"
	"code.justin.tv/commerce/payday/api/constants"
	"code.justin.tv/commerce/payday/api/utils"
	httperror "code.justin.tv/commerce/payday/errors/http"
	"code.justin.tv/commerce/payday/metrics"
	"code.justin.tv/commerce/payday/models/api"
	"github.com/zenazn/goji/web"
)

const (
	responseCacheCleanupInterval = 10 * time.Second
	responseCacheTime            = 1 * time.Minute
)

var cacheableActionTypes = map[string]interface{}{
	api.GlobalFirstPartyAction: nil,
	api.GlobalThirdPartyAction: nil,
	api.DisplayOnlyAction:      nil,
}

// APIs regarding bits actions
type ActionApi struct {
	ActionsRetriever    actions.Retriever               `inject:"chain"`
	GlobalActionsPoller actions.ISupportedActionsPoller `inject:""`
	Statter             metrics.Statter                 `inject:""`
	ClientTracking      clienttracking.Tracker          `inject:""`
	responseCache       ttlcache.Cache
}

func NewActionsAPI() *ActionApi {
	return &ActionApi{
		responseCache: ttlcache.NewCache(responseCacheCleanupInterval),
	}
}

func (actionApi *ActionApi) GetActions(c web.C, w http.ResponseWriter, r *http.Request) error {
	ctx, cancel := context.WithTimeout(r.Context(), 100*time.Millisecond)
	defer cancel()

	go actionApi.ClientTracking.Track(ctx, clienttracking.GetActions)

	channelId := r.URL.Query().Get("channel_id")
	tuid := r.URL.Query().Get("user_id")

	includeSponsored, err := strconv.ParseBool(r.URL.Query().Get("include_sponsored"))
	if err != nil {
		includeSponsored = false
	}

	includeUpperTiers, err := strconv.ParseBool(r.URL.Query().Get("include_upper_tiers"))
	if err != nil {
		includeUpperTiers = false
	}

	getActionsRequest := api.GetActionsRequest{ChannelId: channelId, UserId: tuid, IncludeUpperTiers: includeUpperTiers, IncludeSponsored: includeSponsored}

	actions, err := actionApi.ActionsRetriever.GetActions(ctx, getActionsRequest)
	if err != nil {
		log.WithField("channel_id", channelId).WithError(err).Error("Could not get channel actions")
		return httperror.NewWithCause(constants.DownstreamDependencyError, http.StatusInternalServerError, err)
	}

	dedupedActions := dedupeActions(actions)

	response := api.GetActionsResponse{
		Actions: sortActionsByOrder(dedupedActions),
	}

	if actionApi.isCacheableResponse(response) {
		cacheKey, err := actionApi.getCacheKey(getActionsRequest, response)
		if err != nil {
			log.WithField("cache_key", cacheKey).WithError(err).Warn("error getting cache key for get actions API")
		} else {
			cachedResponse, err := actionApi.getCachedResponse(cacheKey, response)
			if err != nil {
				log.WithField("cache_key", cacheKey).WithError(err).Warn("error getting cached response")
			} else {
				actionApi.Statter.Inc("used_cached_actions_response", 1)
				utils.WriteJSONResponse(cachedResponse, w)
				return nil
			}
		}
	}

	utils.WriteResponse(response, w)
	return nil
}

/**
 * Sort non-destructively actions by order and string (in that priority).
 * This is unnecessary, but it would be good if migrated to using the ordering of the array instead of the order field.
 */
func sortActionsByOrder(actions []api.Action) []api.Action {
	sorted := make([]api.Action, len(actions))
	copy(sorted, actions)
	sort.Sort(byOrder(sorted))
	return sorted
}

func dedupeActions(actions []api.Action) []api.Action {
	dedupedActionsMap := map[string]api.Action{}

	for _, action := range actions {
		existingAction, ok := dedupedActionsMap[action.Prefix]
		if ok && len(existingAction.Tiers) > len(action.Tiers) {
			continue
		}

		dedupedActionsMap[action.Prefix] = action
	}

	dedupedActions := make([]api.Action, 0)
	for _, dedupedAction := range dedupedActionsMap {
		dedupedActions = append(dedupedActions, dedupedAction)
	}

	return dedupedActions
}

// SORTING CODE
type byOrder []api.Action

func (s byOrder) Len() int {
	return len(s)
}
func (s byOrder) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}
func (s byOrder) Less(i, j int) bool {
	if s[i].Order == s[j].Order {
		return s[i].Prefix < s[j].Prefix
	}

	return s[i].Order < s[j].Order
}

func (actionApi *ActionApi) isCacheableResponse(response api.GetActionsResponse) bool {
	for _, action := range response.Actions {
		if _, ok := cacheableActionTypes[action.Type]; !ok {
			return false
		}
	}
	return true
}

func (actionApi *ActionApi) getCacheKey(request api.GetActionsRequest, response api.GetActionsResponse) (string, error) {
	var cacheKey strings.Builder
	_, err := cacheKey.WriteString(fmt.Sprintf("ut:%s;prefixes:", strconv.FormatBool(request.IncludeUpperTiers)))
	if err != nil {
		return "", err
	}
	for _, action := range response.Actions {
		_, err = cacheKey.WriteString(fmt.Sprintf("%s,", action.Prefix))
		if err != nil {
			return "", err
		}
	}
	return cacheKey.String(), nil
}

func (actionApi *ActionApi) getCachedResponse(cacheKey string, response api.GetActionsResponse) ([]byte, error) {
	cachedResp := actionApi.responseCache.Get(cacheKey)
	if cachedResp == nil {
		jsonResp, err := json.Marshal(response)
		if err != nil {
			return nil, err
		}

		actionApi.responseCache.Set(cacheKey, jsonResp, responseCacheTime)
		return jsonResp, nil
	}

	cachedBytes, ok := cachedResp.([]byte)
	if !ok {
		return nil, errors.New("wrong type stored in cache actions response")
	}

	return cachedBytes, nil
}
