package api

import (
	"context"

	"code.justin.tv/commerce/payday/dynamo"
	httperror "code.justin.tv/commerce/payday/errors/http"
	apimodel "code.justin.tv/commerce/payday/models/api"

	json "encoding/json"
	"net/http"

	"code.justin.tv/commerce/payday/api/utils"
	"github.com/zenazn/goji/web"
)

type WithholdingApi struct {
	TaxDao dynamo.ITaxInfoDao `inject:""`
}

const ACCOUNT_ID_DELIMETER = ":"
const SERVICE_ACCOUNT_ID_SUFFIX = "SER"
const ROYALTY_ACCOUNT_ID_SUFFIX = "ROY"
const SERVICE_WITHHOLDING_MAP_KEY = "Service"
const ROYALTY_WITHHOLDING_MAP_KEY = "Royalty"

type WitholdingRateMap map[string]string
type InterviewStatusMap map[string]string

func (this *WithholdingApi) GetWithholdingByAccountId(c web.C, w http.ResponseWriter, r *http.Request) error {
	_, cancel := context.WithTimeout(r.Context(), utils.DefaultTimeout)
	defer cancel()

	accountId := c.URLParams["accountId"]

	taxInfo, err := this.TaxDao.Get(accountId)

	if err != nil {
		return httperror.NewWithCause("Unable to serialize request", http.StatusInternalServerError, err)
	}
	getWithholdingResponse := apimodel.GetWithholdingByAccountIdResponse{}
	var taxWithholdingRateMap WitholdingRateMap

	if taxInfo != nil {
		err = json.Unmarshal([]byte(taxInfo.TaxWithholdingRateMapJson), &taxWithholdingRateMap)
		if err != nil {
			return httperror.NewWithCause("Unable to unmarshal data", http.StatusInternalServerError, err)
		}
		getWithholdingResponse.AddressId = taxInfo.AddressId
		getWithholdingResponse.AccountId = accountId
		getWithholdingResponse.TaxInterviewStatus = taxInfo.TaxInterviewStatus
		getWithholdingResponse.TaxWithholdingRateMap = taxWithholdingRateMap
	}

	utils.WriteResponse(getWithholdingResponse, w)
	return nil
}

func (this *WithholdingApi) GetWithholdingByPayoutEntityId(c web.C, w http.ResponseWriter, r *http.Request) error {
	_, cancel := context.WithTimeout(r.Context(), utils.DefaultTimeout)
	defer cancel()

	payoutEntityId := c.URLParams["payoutEntityId"]

	taxWithholdingRateMap := make(WitholdingRateMap)
	taxInterviewStatusMap := make(InterviewStatusMap)

	servicesTaxInfo, err := this.TaxDao.Get(payoutEntityId + ACCOUNT_ID_DELIMETER + SERVICE_ACCOUNT_ID_SUFFIX)
	if err != nil {
		return httperror.NewWithCause("Unable to serialize request", http.StatusInternalServerError, err)
	} else if servicesTaxInfo != nil {
		var serTaxWithholdingRateMap WitholdingRateMap
		err = json.Unmarshal([]byte(servicesTaxInfo.TaxWithholdingRateMapJson), &serTaxWithholdingRateMap)
		if err != nil {
			return httperror.NewWithCause("Unable to unmarshal data", http.StatusInternalServerError, err)
		}
		taxWithholdingRateMap[SERVICE_ACCOUNT_ID_SUFFIX] = serTaxWithholdingRateMap[SERVICE_WITHHOLDING_MAP_KEY]
		taxInterviewStatusMap[SERVICE_ACCOUNT_ID_SUFFIX] = servicesTaxInfo.TaxInterviewStatus
	}

	royaltiesTaxInfo, err := this.TaxDao.Get(payoutEntityId + ACCOUNT_ID_DELIMETER + ROYALTY_ACCOUNT_ID_SUFFIX)
	if err != nil {
		return httperror.NewWithCause("Unable to serialize request", http.StatusInternalServerError, err)
	} else if royaltiesTaxInfo != nil {
		var royTaxWithholdingRateMap WitholdingRateMap
		err = json.Unmarshal([]byte(royaltiesTaxInfo.TaxWithholdingRateMapJson), &royTaxWithholdingRateMap)
		if err != nil {
			return httperror.NewWithCause("Unable to unmarshal data", http.StatusInternalServerError, err)
		}
		taxWithholdingRateMap[ROYALTY_ACCOUNT_ID_SUFFIX] = royTaxWithholdingRateMap[ROYALTY_WITHHOLDING_MAP_KEY]
		taxInterviewStatusMap[ROYALTY_ACCOUNT_ID_SUFFIX] = royaltiesTaxInfo.TaxInterviewStatus

	}
	getWithholdingResponse := apimodel.GetWithholdingByPayoutEntityIdResponse{}

	if servicesTaxInfo != nil {
		getWithholdingResponse.AddressId = servicesTaxInfo.AddressId
	} else if royaltiesTaxInfo != nil {
		getWithholdingResponse.AddressId = royaltiesTaxInfo.AddressId
	}

	getWithholdingResponse.PayoutEntityId = payoutEntityId
	getWithholdingResponse.TaxInterviewStatus = taxInterviewStatusMap
	getWithholdingResponse.TaxWithholdingRateMap = taxWithholdingRateMap
	utils.WriteResponse(getWithholdingResponse, w)
	return nil
}
