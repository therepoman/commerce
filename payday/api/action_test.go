package api

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"code.justin.tv/commerce/gogogadget/ttlcache"
	actions2 "code.justin.tv/commerce/payday/actions"
	"code.justin.tv/commerce/payday/api/apidef"
	"code.justin.tv/commerce/payday/api/clienttracking"
	"code.justin.tv/commerce/payday/decorators"
	"code.justin.tv/commerce/payday/metrics"
	actions_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/actions"
	clienttracking_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/api/clienttracking"
	"code.justin.tv/commerce/payday/models/api"
	model "code.justin.tv/commerce/payday/models/api"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
	"github.com/zenazn/goji/web"
)

const (
	TestChannelId    = "123456789"
	TestActionPrefix = "testActionPrefix"
)

func TestGetActions(t *testing.T) {
	Convey("Given an actions retriever", t, func() {
		actionsRetriever := new(actions_mock.Retriever)
		clientTracking := new(clienttracking_mock.Tracker)
		clientTracking.On("Track", mock.Anything, clienttracking.GetActions).Return()

		actionsApi := ActionApi{
			ActionsRetriever:    actionsRetriever,
			GlobalActionsPoller: actions2.NewTestSupportedActionsPoller([]model.Action{}),
			Statter:             metrics.NewNoopStatter(),
			responseCache:       ttlcache.NewCache(responseCacheCleanupInterval),
			ClientTracking:      clientTracking,
		}

		actions := []api.Action{{
			Prefix: TestActionPrefix,
		}}

		m := web.New()
		m.Get("/api/actions", decorators.DecorateAndAdapt(actionsApi.GetActions, apidef.GetActions))

		Convey("when channel_id query parameter is provided then channel ID is passed to retriever and retriever returns valid action response", func() {
			actionsRetriever.On("GetActions", mock.Anything, api.GetActionsRequest{ChannelId: TestChannelId}).Return(actions, nil)

			response := createRequestAndCallGetActions(m, TestChannelId, "")

			var unmarshalledResponse api.GetActionsResponse
			_ = json.Unmarshal(response.Body.Bytes(), &unmarshalledResponse)
			So(response.Code, ShouldEqual, http.StatusOK)
			So(len(unmarshalledResponse.Actions), ShouldEqual, 1)
			So(unmarshalledResponse.Actions[0].Prefix, ShouldEqual, TestActionPrefix)
		})

		Convey("when include_upper_tiers query parameter is provided then it is passed to retriever and retriever returns valid action response", func() {
			actionsRetriever.On("GetActions", mock.Anything, api.GetActionsRequest{IncludeUpperTiers: true}).Return(actions, nil)

			response := createRequestAndCallGetActions(m, "", "true")

			var unmarshalledResponse api.GetActionsResponse
			_ = json.Unmarshal(response.Body.Bytes(), &unmarshalledResponse)
			So(response.Code, ShouldEqual, http.StatusOK)
			So(len(unmarshalledResponse.Actions), ShouldEqual, 1)
			So(unmarshalledResponse.Actions[0].Prefix, ShouldEqual, TestActionPrefix)
		})

		Convey("when retriever throws an error then return a 500", func() {
			actionsRetriever.On("GetActions", mock.Anything, mock.Anything).Return(nil, errors.New("unit test error"))

			response := createRequestAndCallGetActions(m, TestChannelId, "true")

			var unmarshalledResponse api.GetActionsResponse
			_ = json.Unmarshal(response.Body.Bytes(), &unmarshalledResponse)
			So(response.Code, ShouldEqual, http.StatusInternalServerError)
		})
	})
}

func TestGetActionsResponseCache(t *testing.T) {
	Convey("Given an actions retriever", t, func() {
		actionsRetriever := new(actions_mock.Retriever)
		clientTracking := new(clienttracking_mock.Tracker)
		clientTracking.On("Track", mock.Anything, clienttracking.GetActions).Return()

		actionsApi := ActionApi{
			ActionsRetriever: actionsRetriever,
			GlobalActionsPoller: actions2.NewTestSupportedActionsPoller([]model.Action{
				{
					Prefix: TestActionPrefix,
				},
			}),
			Statter:        metrics.NewNoopStatter(),
			responseCache:  ttlcache.NewCache(responseCacheCleanupInterval),
			ClientTracking: clientTracking,
		}

		actions := []api.Action{{
			Prefix: TestActionPrefix,
			Type:   api.GlobalFirstPartyAction,
		}}

		m := web.New()
		m.Get("/api/actions", decorators.DecorateAndAdapt(actionsApi.GetActions, apidef.GetActions))

		Convey("when retriever returns valid action response", func() {
			req := api.GetActionsRequest{ChannelId: TestChannelId, IncludeUpperTiers: true}
			actionsRetriever.On("GetActions", mock.Anything, req).Return(actions, nil)

			Convey("The HTTP request is successful and sets the response cache", func() {

				response := createRequestAndCallGetActions(m, TestChannelId, "true")
				var unmarshalledResponse api.GetActionsResponse
				_ = json.Unmarshal(response.Body.Bytes(), &unmarshalledResponse)
				So(response.Code, ShouldEqual, http.StatusOK)
				So(len(unmarshalledResponse.Actions), ShouldEqual, 1)
				So(unmarshalledResponse.Actions[0].Prefix, ShouldEqual, TestActionPrefix)

				So(actionsApi.responseCache.Len(), ShouldEqual, 1)
				cacheKey, err := actionsApi.getCacheKey(req, unmarshalledResponse)
				So(err, ShouldBeNil)

				cachedResponse := actionsApi.responseCache.Get(cacheKey).([]byte)
				var cachedUnmarshalledResponse api.GetActionsResponse
				_ = json.Unmarshal(cachedResponse, &cachedUnmarshalledResponse)

				So(len(cachedUnmarshalledResponse.Actions), ShouldEqual, 1)
				So(cachedUnmarshalledResponse.Actions[0].Prefix, ShouldEqual, TestActionPrefix)

				Convey("A subsequent HTTP request is also successful", func() {

					response := createRequestAndCallGetActions(m, TestChannelId, "true")
					var unmarshalledResponse api.GetActionsResponse
					_ = json.Unmarshal(response.Body.Bytes(), &unmarshalledResponse)
					So(response.Code, ShouldEqual, http.StatusOK)
					So(len(unmarshalledResponse.Actions), ShouldEqual, 1)
					So(unmarshalledResponse.Actions[0].Prefix, ShouldEqual, TestActionPrefix)
				})
			})
		})

	})
}

func createRequestAndCallGetActions(m *web.Mux, channelId string, includeUpperTiers string) *httptest.ResponseRecorder {
	r, err := http.NewRequest("GET", fmt.Sprintf("/api/actions?channel_id=%s&include_upper_tiers=%s", channelId, includeUpperTiers), nil)
	So(err, ShouldBeNil)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)
	return w
}
