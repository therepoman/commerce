package api

import (
	"bytes"
	"encoding/json"
	go_errors "errors"
	"net/http"
	"net/http/httptest"
	"strconv"
	"strings"
	"testing"

	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	"code.justin.tv/commerce/payday/api/apidef"
	"code.justin.tv/commerce/payday/api/clienttracking"
	"code.justin.tv/commerce/payday/api/constants"
	chatbadge_models "code.justin.tv/commerce/payday/badgetiers/models"
	"code.justin.tv/commerce/payday/cartman"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/datascience"
	"code.justin.tv/commerce/payday/decorators"
	"code.justin.tv/commerce/payday/dynamo"
	errors "code.justin.tv/commerce/payday/errors/http"
	clienttracking_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/api/clienttracking"
	chatbadgefetcher_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/badgetiers/fetcher"
	chatbadgelogician_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/badgetiers/logician"
	channel_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/cache/channel"
	emote_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/emote"
	notification_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/notification"
	onboard_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/onboard"
	partner_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/partner"
	payout_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/payout"
	sns_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sns"
	"code.justin.tv/commerce/payday/models"
	apimodel "code.justin.tv/commerce/payday/models/api"
	"code.justin.tv/commerce/payday/partner"
	"code.justin.tv/commerce/payday/utils/pointers"
	timeUtils "code.justin.tv/commerce/payday/utils/time"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/zenazn/goji/web"
)

var pm = new(payout_mock.IPayoutManager)
var pfm = new(emote_mock.IPrefixManager)
var pnm = new(partner_mock.IPartnerManager)
var n = new(notification_mock.INotification)
var chatBadgesFetcher = new(chatbadgefetcher_mock.Fetcher)
var chatBadgesLogician = new(chatbadgelogician_mock.Logician)
var channelManagerMock = new(channel_mock.ChannelManager)
var channelApi *ChannelApi

var channelId = 0

func getTestChannelId() string {
	channelId++
	return strconv.Itoa(channelId)
}

func SetupChannelEndpoints(partnerResponse bool) *web.Mux {

	// Cache for one nanosecond (effectively no cache)
	config := &config.Configuration{
		ChannelSettingsSNSTopic: "foo",
	}

	pm = new(payout_mock.IPayoutManager)
	chatBadgesFetcher = new(chatbadgefetcher_mock.Fetcher)
	chatBadgesLogician = new(chatbadgelogician_mock.Logician)
	channelManagerMock = new(channel_mock.ChannelManager)

	onboardManager := new(onboard_mock.IOnboardManager)
	onboardManager.On("OnboardForBits", mock.Anything, mock.Anything).Return(nil)

	n = new(notification_mock.INotification)
	snsClient := new(sns_mock.ISNSClient)
	snsClient.On("PostToTopic", mock.Anything, mock.Anything).Return(nil)
	clientTracking := new(clienttracking_mock.Tracker)
	clientTracking.On("Track", mock.Anything, clienttracking.GetChannelInfo).Return()

	channelApi = &ChannelApi{
		ChannelManager:    channelManagerMock,
		ChatBadgeFetcher:  chatBadgesFetcher,
		ChatBadgeLogician: chatBadgesLogician,
		CartmanValidator:  cartman.NewAlwaysValidValidator(),
		PayoutManager:     pm,
		DataScienceClient: &datascience.DataScienceApiNoOp{},
		PrefixManager:     pfm,
		PartnerManager:    pnm,
		OnboardManager:    onboardManager,
		Notification:      n,
		SNSClient:         snsClient,
		Config:            config,
		ClientTracking:    clientTracking,
	}

	m := web.New()
	m.Get("/channels/:tuid/settings", decorators.DecorateAndAdapt(channelApi.GetSettingsWithCartmanAuthentication, apidef.GetChannelSettings))
	m.Put("/channels/:tuid/settings", decorators.DecorateAndAdapt(channelApi.UpdateSettingsWithCartmanAuthentication, apidef.UpdateChannelSettings))
	m.Get("/channels/:tuid/min-bits", decorators.DecorateAndAdapt(channelApi.GetMinBits, apidef.AdminGetChannelMinBits))
	m.Put("/channels/:tuid/min-bits", decorators.DecorateAndAdapt(channelApi.SetMinBits, apidef.AdminSetChannelMinBits))
	m.Get("/channels/:tuid/opt-out", decorators.DecorateAndAdapt(channelApi.GetOptOut, apidef.AdminGetChannelOptOut))
	m.Put("/channels/:tuid/opt-out", decorators.DecorateAndAdapt(channelApi.SetOptOut, apidef.AdminSetChannelOptOut))
	m.Get("/channels/:tuid", decorators.DecorateAndAdapt(channelApi.GetChannelInfo, apidef.GetChannelInfo))
	return m
}

// Get Min Bits Tests

func TestGetMinBitsNonNumericUser(t *testing.T) {
	m := SetupChannelEndpoints(false)

	r, _ := http.NewRequest("GET", "/channels/user/min-bits", nil)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 400, w.Code)
	resp, err := errors.ConvertJsonToErrorMessageResponse(w.Body.Bytes())
	assert.Nil(t, err)
	assert.Equal(t, constants.NonNumericUserIdError, resp.Message)
}

func TestGetMinBitsNotInDynamo(t *testing.T) {
	m := SetupChannelEndpoints(false)

	channelId := getTestChannelId()

	channelManagerMock.On("Get", mock.Anything, channelId).Return(nil, nil)

	r, _ := http.NewRequest("GET", "/channels/"+channelId+"/min-bits", nil)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 404, w.Code)
	resp, err := errors.ConvertJsonToErrorMessageResponse(w.Body.Bytes())
	assert.Nil(t, err)
	assert.Equal(t, channelNotFoundError, resp.Message)
}

func TestGetMinBitsAttributeNotInDynamo(t *testing.T) {
	m := SetupChannelEndpoints(false)

	channelId := getTestChannelId()

	expectedChannel := &dynamo.Channel{
		Id:         dynamo.ChannelId(channelId),
		Annotation: pointers.StringP("abc"),
	}
	channelManagerMock.On("Get", mock.Anything, channelId).Return(expectedChannel, nil)

	r, _ := http.NewRequest("GET", "/channels/"+channelId+"/min-bits", nil)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 404, w.Code)
	resp, err := errors.ConvertJsonToErrorMessageResponse(w.Body.Bytes())
	assert.Nil(t, err)
	assert.Equal(t, channelNotFoundError, resp.Message)
}

func TestGetMinBitsExists(t *testing.T) {
	m := SetupChannelEndpoints(false)

	channelId := getTestChannelId()

	expectedChannel := &dynamo.Channel{
		Id:      dynamo.ChannelId(channelId),
		MinBits: pointers.Int64P(10),
	}
	channelManagerMock.On("Get", mock.Anything, channelId).Return(expectedChannel, nil)

	r, _ := http.NewRequest("GET", "/channels/"+channelId+"/min-bits", nil)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 200, w.Code)

	resultJson := w.Body.Bytes()
	result, err := apimodel.ConvertJsonToGetMinBitsResponse(resultJson)
	if err != nil {
		t.Fail()
	}

	actualChannel := models.GetMinBitsResponseToDynamoChannel(result)
	assert.True(t, expectedChannel.Equals(actualChannel))
}

// Set Min Bits Tests

func TestSetMinBitsNonNumericUser(t *testing.T) {
	m := SetupChannelEndpoints(false)

	request := &apimodel.SetMinBitsRequest{
		MinBits: 10,
	}
	requestJson, _ := json.Marshal(request)
	requestJsonReader := bytes.NewReader(requestJson)
	r, _ := http.NewRequest("PUT", "/channels/user/min-bits", requestJsonReader)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 400, w.Code)
	resp, err := errors.ConvertJsonToErrorMessageResponse(w.Body.Bytes())
	assert.Nil(t, err)
	assert.Equal(t, constants.NonNumericUserIdError, resp.Message)
}

func TestSetMinBitsInvalidJson(t *testing.T) {
	m := SetupChannelEndpoints(false)

	channelId := getTestChannelId()

	requestJsonReader := strings.NewReader("NotValidJson")
	r, _ := http.NewRequest("PUT", "/channels/"+channelId+"/min-bits", requestJsonReader)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 400, w.Code)
	resp, err := errors.ConvertJsonToErrorMessageResponse(w.Body.Bytes())
	assert.Nil(t, err)
	assert.Equal(t, constants.InvalidJsonError, resp.Message)
}

func TestSetMinBitsTooLow(t *testing.T) {
	m := SetupChannelEndpoints(false)

	channelId := getTestChannelId()

	request := &apimodel.SetMinBitsRequest{
		MinBits: 0,
	}
	requestJson, _ := json.Marshal(request)
	requestJsonReader := bytes.NewReader(requestJson)
	r, _ := http.NewRequest("PUT", "/channels/"+channelId+"/min-bits", requestJsonReader)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 400, w.Code)
	resp, err := errors.ConvertJsonToErrorMessageResponse(w.Body.Bytes())
	assert.Nil(t, err)
	assert.Equal(t, constants.MinBitsOutsideRangeError, resp.Message)
}

func TestSetMinBitsTooHigh(t *testing.T) {
	m := SetupChannelEndpoints(false)

	channelId := getTestChannelId()

	request := &apimodel.SetMinBitsRequest{
		MinBits: 100001,
	}
	requestJson, _ := json.Marshal(request)
	requestJsonReader := bytes.NewReader(requestJson)
	r, _ := http.NewRequest("PUT", "/channels/"+channelId+"/min-bits", requestJsonReader)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 400, w.Code)
	resp, err := errors.ConvertJsonToErrorMessageResponse(w.Body.Bytes())
	assert.Nil(t, err)
	assert.Equal(t, constants.MinBitsOutsideRangeError, resp.Message)
}

func TestSetMinBits(t *testing.T) {
	m := SetupChannelEndpoints(false)

	channelId := getTestChannelId()

	request := &apimodel.SetMinBitsRequest{
		MinBits: 10,
	}
	expectedChannel := models.SetMinBitsRequestToDynamoChannel(request, channelId)
	channelManagerMock.On("Update", mock.Anything, expectedChannel).Return(nil)

	requestJson, _ := json.Marshal(request)
	requestJsonReader := bytes.NewReader(requestJson)
	r, _ := http.NewRequest("PUT", "/channels/"+channelId+"/min-bits", requestJsonReader)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 200, w.Code)

	channelManagerMock.AssertCalled(t, "Update", mock.Anything, expectedChannel)
}

// Get Settings Tests
func TestGetSettingsNonNumericUser(t *testing.T) {
	m := SetupChannelEndpoints(false)

	r, _ := http.NewRequest("GET", "/channels/user/settings", nil)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 400, w.Code)
	resp, err := errors.ConvertJsonToErrorMessageResponse(w.Body.Bytes())
	assert.NoError(t, err)
	assert.Equal(t, constants.NonNumericUserIdError, resp.Message)
}

func TestGetSettingsNotInDynamo(t *testing.T) {
	m := SetupChannelEndpoints(false)

	channelId := getTestChannelId()

	pnm.On("GetPartnerType", mock.Anything, channelId).Return(partner.TraditionalPartnerType, nil)
	pm.On("HasServicesTaxInfo", mock.Anything, channelId).Return(true, nil)
	pfm.On("GetCustomCheermoteAction", mock.Anything, channelId).Return("testPrefix", true, nil)
	n.On("InExperimentGroup", mock.Anything).Return(false)
	channelManagerMock.On("Get", mock.Anything, channelId).Return(nil, nil)

	r, _ := http.NewRequest("GET", "/channels/"+channelId+"/settings", nil)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 200, w.Code)
	result, err := apimodel.ConvertJsonToGetSettingsResponse(w.Body.Bytes())

	assert.NoError(t, err)
	assert.Equal(t, apimodel.DefaultMinBits, result.MinBits)
	assert.Equal(t, apimodel.DefaultMinBits, result.MinBitsEmote)
	assert.Equal(t, false, result.PinRecentCheers)
	assert.Equal(t, apimodel.DefaultRecentCheerMin, result.RecentCheerMin)
	assert.Equal(t, apimodel.DefaultRecentCheerTimeoutSeconds, result.RecentCheerTimeout)
}

func TestGetSettingsAttributesNotInDynamo(t *testing.T) {
	m := SetupChannelEndpoints(false)

	channelId := getTestChannelId()

	pnm.On("GetPartnerType", mock.Anything, channelId).Return(partner.TraditionalPartnerType, nil)
	pm.On("HasServicesTaxInfo", mock.Anything, channelId).Return(true, nil)
	pfm.On("GetCustomCheermoteAction", mock.Anything, channelId).Return("testPrefix", true, nil)
	n.On("InExperimentGroup", mock.Anything).Return(false)

	expectedChannel := &dynamo.Channel{
		Id:         dynamo.ChannelId(channelId),
		Annotation: pointers.StringP("abc"),
	}
	channelManagerMock.On("Get", mock.Anything, channelId).Return(expectedChannel, nil)

	r, _ := http.NewRequest("GET", "/channels/"+channelId+"/settings", nil)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 200, w.Code)
	result, err := apimodel.ConvertJsonToGetSettingsResponse(w.Body.Bytes())

	assert.NoError(t, err)
	assert.Equal(t, apimodel.DefaultMinBits, result.MinBits)
	assert.Equal(t, apimodel.DefaultMinBits, result.MinBitsEmote)
	//	assert.Equal(t, apimodel.ex experiments.InPinnedCheersExperiment(channelId), result.PinRecentCheers)
	assert.Equal(t, apimodel.DefaultRecentCheerMin, result.RecentCheerMin)
	assert.Equal(t, apimodel.DefaultRecentCheerTimeoutSeconds, result.RecentCheerTimeout)
}

func TestGetSettingsSomeAttributesExist(t *testing.T) {
	m := SetupChannelEndpoints(false)

	channelId := getTestChannelId()

	pnm.On("GetPartnerType", mock.Anything, channelId).Return(partner.TraditionalPartnerType, nil)
	pm.On("HasServicesTaxInfo", mock.Anything, channelId).Return(true, nil)
	pfm.On("GetCustomCheermoteAction", mock.Anything, channelId).Return("testPrefix", true, nil)
	n.On("InExperimentGroup", mock.Anything).Return(false)

	expectedChannel := &dynamo.Channel{
		Id:             dynamo.ChannelId(channelId),
		MinBits:        pointers.Int64P(10),
		RecentCheerMin: pointers.Int64P(100),
	}
	channelManagerMock.On("Get", mock.Anything, channelId).Return(expectedChannel, nil)

	r, _ := http.NewRequest("GET", "/channels/"+channelId+"/settings", nil)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 200, w.Code)
	result, err := apimodel.ConvertJsonToGetSettingsResponse(w.Body.Bytes())

	assert.NoError(t, err)
	assert.Equal(t, int64(10), result.MinBits)
	assert.Equal(t, apimodel.DefaultMinBits, result.MinBitsEmote)
	assert.Equal(t, true, result.PinRecentCheers)
	assert.Equal(t, int64(100), result.RecentCheerMin)
	assert.Equal(t, apimodel.DefaultRecentCheerTimeoutSeconds, result.RecentCheerTimeout)
}

func TestGetSettingsAllAttributesExist(t *testing.T) {
	m := SetupChannelEndpoints(false)

	channelId := getTestChannelId()

	pnm.On("GetPartnerType", mock.Anything, channelId).Return(partner.TraditionalPartnerType, nil)
	pm.On("HasServicesTaxInfo", mock.Anything, channelId).Return(true, nil)
	pfm.On("GetCustomCheermoteAction", mock.Anything, channelId).Return("testPrefix", true, nil)
	n.On("InExperimentGroup", mock.Anything).Return(false)

	expectedChannel := &dynamo.Channel{
		Id:                 dynamo.ChannelId(channelId),
		MinBits:            pointers.Int64P(10),
		MinBitsEmote:       pointers.Int64P(5),
		PinRecentCheers:    pointers.BoolP(true),
		RecentCheerMin:     pointers.Int64P(100),
		RecentCheerTimeout: timeUtils.SecondsToNanosecondsP(pointers.Int64P(60)),
	}
	channelManagerMock.On("Get", mock.Anything, channelId).Return(expectedChannel, nil)

	r, _ := http.NewRequest("GET", "/channels/"+channelId+"/settings", nil)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 200, w.Code)
	result, err := apimodel.ConvertJsonToGetSettingsResponse(w.Body.Bytes())

	assert.NoError(t, err)
	assert.Equal(t, int64(10), result.MinBits)
	assert.Equal(t, int64(5), result.MinBitsEmote)
	assert.Equal(t, true, result.PinRecentCheers)
	assert.Equal(t, int64(100), result.RecentCheerMin)
	assert.Equal(t, int64(60), result.RecentCheerTimeout)
}

func TestGetChannelLeaderboardSettings(t *testing.T) {
	m := SetupChannelEndpoints(false)

	channelId := getTestChannelId()

	pnm.On("GetPartnerType", mock.Anything, channelId).Return(partner.TraditionalPartnerType, nil)
	pm.On("HasServicesTaxInfo", mock.Anything, channelId).Return(true, nil)
	pfm.On("GetCustomCheermoteAction", mock.Anything, channelId).Return("testPrefix", true, nil)
	n.On("InExperimentGroup", mock.Anything).Return(false)

	expectedChannel := &dynamo.Channel{
		Id:                          dynamo.ChannelId(channelId),
		LeaderboardEnabled:          pointers.BoolP(true),
		IsCheerLeaderboardEnabled:   pointers.BoolP(false),
		IsSubGiftLeaderboardEnabled: pointers.BoolP(true),
		DefaultLeaderboard:          pointers.StringP(apimodel.DefaultLeaderboardCheer),
		LeaderboardTimePeriod:       pointers.StringP(pantheonrpc.TimeUnit_name[0]),
	}
	channelManagerMock.On("Get", mock.Anything, channelId).Return(expectedChannel, nil)

	r, _ := http.NewRequest("GET", "/channels/"+channelId+"/settings", nil)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 200, w.Code)
	result, err := apimodel.ConvertJsonToGetSettingsResponse(w.Body.Bytes())

	assert.NoError(t, err)
	assert.Equal(t, true, result.LeaderboardEnabled)
	assert.Equal(t, false, result.IsCheerLeaderboardEnabled)
	assert.Equal(t, true, result.IsSubGiftLeaderboardEnabled)
	assert.Equal(t, apimodel.DefaultLeaderboardCheer, result.DefaultLeaderboard)
	assert.Equal(t, pantheonrpc.TimeUnit_name[0], result.LeaderboardTimePeriod)
}

func TestGetSettingsAmendmentStates(t *testing.T) {
	Convey("Given a partner who has been onboarded for Bits", t, func() {
		m := SetupChannelEndpoints(false)

		channelId := getTestChannelId()

		pnm.On("GetPartnerType", mock.Anything, channelId).Return(partner.TraditionalPartnerType, nil)
		pm.On("HasServicesTaxInfo", mock.Anything, channelId).Return(true, nil)
		pfm.On("GetCustomCheermoteAction", mock.Anything, channelId).Return("testPrefix", true, nil)
		n.On("InExperimentGroup", mock.Anything).Return(false)

		expectedChannel := &dynamo.Channel{
			Id:        dynamo.ChannelId(channelId),
			Onboarded: pointers.BoolP(true),
		}

		Convey("when the amendment is nil", func() {
			channelManagerMock.On("Get", mock.Anything, channelId).Return(expectedChannel, nil)

			r, _ := http.NewRequest("GET", "/channels/"+channelId+"/settings", nil)
			w := httptest.NewRecorder()
			m.ServeHTTP(w, r)

			assert.Equal(t, 200, w.Code)
			result, err := apimodel.ConvertJsonToGetSettingsResponse(w.Body.Bytes())

			assert.NoError(t, err)
			assert.Equal(t, result.AmendmentSigned, true)
		})

		Convey("when the amendment is signed", func() {
			expectedChannel.AmendmentState = pointers.StringP(string(partner.BitsAmendmentStateSigned))
			channelManagerMock.On("Get", mock.Anything, channelId).Return(expectedChannel, nil)

			r, _ := http.NewRequest("GET", "/channels/"+channelId+"/settings", nil)
			w := httptest.NewRecorder()
			m.ServeHTTP(w, r)

			assert.Equal(t, 200, w.Code)
			result, err := apimodel.ConvertJsonToGetSettingsResponse(w.Body.Bytes())

			assert.NoError(t, err)
			assert.Equal(t, result.AmendmentSigned, true)
		})

		Convey("when the amendment is explicitly unsigned", func() {
			expectedChannel.AmendmentState = pointers.StringP(string(partner.BitsAmendmentStateUnsigned))
			channelManagerMock.On("Get", mock.Anything, channelId).Return(expectedChannel, nil)

			r, _ := http.NewRequest("GET", "/channels/"+channelId+"/settings", nil)
			w := httptest.NewRecorder()
			m.ServeHTTP(w, r)

			assert.Equal(t, 200, w.Code)
			result, err := apimodel.ConvertJsonToGetSettingsResponse(w.Body.Bytes())

			assert.NoError(t, err)
			assert.Equal(t, result.AmendmentSigned, false)
		})

	})
}

// Update Settings Tests

func TestUpdateSettingsNonNumericUser(t *testing.T) {
	m := SetupChannelEndpoints(false)

	request := &apimodel.UpdateSettingsRequest{
		MinBits: pointers.Int64P(10),
	}
	requestJson, _ := json.Marshal(request)
	requestJsonReader := bytes.NewReader(requestJson)
	r, _ := http.NewRequest("PUT", "/channels/user/settings", requestJsonReader)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 400, w.Code)
	resp, err := errors.ConvertJsonToErrorMessageResponse(w.Body.Bytes())
	assert.Nil(t, err)
	assert.Equal(t, constants.NonNumericUserIdError, resp.Message)
}

func TestUpdateSettingsInvalidJson(t *testing.T) {
	m := SetupChannelEndpoints(false)

	channelId := getTestChannelId()

	requestJsonReader := strings.NewReader("NotValidJson")
	r, _ := http.NewRequest("PUT", "/channels/"+channelId+"/settings", requestJsonReader)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 400, w.Code)
	resp, err := errors.ConvertJsonToErrorMessageResponse(w.Body.Bytes())
	assert.Nil(t, err)
	assert.Equal(t, constants.InvalidJsonError, resp.Message)
}

func TestUpdateSettingsSetMinBitsTooLow(t *testing.T) {
	m := SetupChannelEndpoints(false)

	channelId := getTestChannelId()

	request := &apimodel.UpdateSettingsRequest{
		MinBits: pointers.Int64P(0),
	}
	requestJson, _ := json.Marshal(request)
	requestJsonReader := bytes.NewReader(requestJson)
	r, _ := http.NewRequest("PUT", "/channels/"+channelId+"/settings", requestJsonReader)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 400, w.Code)
	resp, err := errors.ConvertJsonToErrorMessageResponse(w.Body.Bytes())
	assert.Nil(t, err)
	assert.Equal(t, constants.MinBitsOutsideRangeError, resp.Message)
}

func TestUpdateSettingsSetMinBitsTooHigh(t *testing.T) {
	m := SetupChannelEndpoints(false)

	channelId := getTestChannelId()

	request := &apimodel.SetMinBitsRequest{
		MinBits: 100001,
	}
	requestJson, _ := json.Marshal(request)
	requestJsonReader := bytes.NewReader(requestJson)
	r, _ := http.NewRequest("PUT", "/channels/"+channelId+"/min-bits", requestJsonReader)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 400, w.Code)
	resp, err := errors.ConvertJsonToErrorMessageResponse(w.Body.Bytes())
	assert.Nil(t, err)
	assert.Equal(t, constants.MinBitsOutsideRangeError, resp.Message)
}

func TestUpdateSettingsSetMinBitsEmoteTooLow(t *testing.T) {
	m := SetupChannelEndpoints(false)

	channelId := getTestChannelId()

	request := &apimodel.UpdateSettingsRequest{
		MinBitsEmote: pointers.Int64P(0),
	}
	requestJson, _ := json.Marshal(request)
	requestJsonReader := bytes.NewReader(requestJson)
	r, _ := http.NewRequest("PUT", "/channels/"+channelId+"/settings", requestJsonReader)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 400, w.Code)
	resp, err := errors.ConvertJsonToErrorMessageResponse(w.Body.Bytes())
	assert.Nil(t, err)
	assert.Equal(t, constants.MinBitsOutsideRangeError, resp.Message)
}

func TestUpdateSettingsSetMinBitsEmoteTooHigh(t *testing.T) {
	m := SetupChannelEndpoints(false)

	channelId := getTestChannelId()

	request := &apimodel.UpdateSettingsRequest{
		MinBitsEmote: pointers.Int64P(100001),
	}
	requestJson, _ := json.Marshal(request)
	requestJsonReader := bytes.NewReader(requestJson)
	r, _ := http.NewRequest("PUT", "/channels/"+channelId+"/settings", requestJsonReader)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 400, w.Code)
	resp, err := errors.ConvertJsonToErrorMessageResponse(w.Body.Bytes())
	assert.Nil(t, err)
	assert.Equal(t, constants.MinBitsOutsideRangeError, resp.Message)
}

func TestUpdateSettingsMinBitsBelowMinBitsEmote(t *testing.T) {
	m := SetupChannelEndpoints(false)

	channelId := getTestChannelId()

	expectedChannel := &dynamo.Channel{
		Id:                 dynamo.ChannelId(channelId),
		MinBits:            pointers.Int64P(10),
		MinBitsEmote:       pointers.Int64P(5),
		PinRecentCheers:    pointers.BoolP(true),
		RecentCheerMin:     pointers.Int64P(100),
		RecentCheerTimeout: timeUtils.SecondsToNanosecondsP(pointers.Int64P(60)),
	}
	channelManagerMock.On("Get", mock.Anything, channelId).Return(expectedChannel, nil)

	request := &apimodel.UpdateSettingsRequest{
		MinBits:      pointers.Int64P(5),
		MinBitsEmote: pointers.Int64P(10),
	}
	requestJson, _ := json.Marshal(request)
	requestJsonReader := bytes.NewReader(requestJson)
	r, _ := http.NewRequest("PUT", "/channels/"+channelId+"/settings", requestJsonReader)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 400, w.Code)
	resp, err := errors.ConvertJsonToErrorMessageResponse(w.Body.Bytes())
	assert.Nil(t, err)
	assert.Equal(t, constants.MinBitsBelowMinBitsEmoteError, resp.Message)
}

func TestUpdateSettingsDefaultLeaderboardIncorrectValue(t *testing.T) {
	m := SetupChannelEndpoints(false)

	channelId := getTestChannelId()

	request := &apimodel.UpdateSettingsRequest{
		DefaultLeaderboard: pointers.StringP("NOT_VALID_LEADERBOARD"),
	}
	requestJson, _ := json.Marshal(request)
	requestJsonReader := bytes.NewReader(requestJson)
	r, _ := http.NewRequest("PUT", "/channels/"+channelId+"/settings", requestJsonReader)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 400, w.Code)
	resp, err := errors.ConvertJsonToErrorMessageResponse(w.Body.Bytes())
	assert.Nil(t, err)
	assert.Equal(t, constants.InvalidLeaderboardSettingError, resp.Message)
}

func TestUpdateSettingsLeaderboardTimePeriodUnsupportedValue(t *testing.T) {
	m := SetupChannelEndpoints(false)

	channelId := getTestChannelId()

	request := &apimodel.UpdateSettingsRequest{
		LeaderboardTimePeriod: pointers.StringP(pantheonrpc.TimeUnit_name[1]), // YEAR is not supported
	}
	requestJson, _ := json.Marshal(request)
	requestJsonReader := bytes.NewReader(requestJson)
	r, _ := http.NewRequest("PUT", "/channels/"+channelId+"/settings", requestJsonReader)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 400, w.Code)
	resp, err := errors.ConvertJsonToErrorMessageResponse(w.Body.Bytes())
	assert.Nil(t, err)
	assert.Equal(t, constants.InvalidLeaderboardSettingError, resp.Message)
}

func TestUpdateSettingsOnlyMin(t *testing.T) {
	m := SetupChannelEndpoints(false)

	channelId := getTestChannelId()

	expectedChannel := &dynamo.Channel{
		Id:                 dynamo.ChannelId(channelId),
		MinBits:            pointers.Int64P(5),
		MinBitsEmote:       pointers.Int64P(5),
		PinRecentCheers:    pointers.BoolP(true),
		RecentCheerMin:     pointers.Int64P(100),
		RecentCheerTimeout: timeUtils.SecondsToNanosecondsP(pointers.Int64P(60)),
	}
	channelManagerMock.On("Get", mock.Anything, channelId).Return(expectedChannel, nil)

	request := &apimodel.UpdateSettingsRequest{
		MinBits:      pointers.Int64P(10),
		MinBitsEmote: pointers.Int64P(5),
	}
	updatedChannel := models.UpdateSettingsRequestToDynamoChannel(request, channelId)
	channelManagerMock.On("Update", mock.Anything, updatedChannel).Return(nil)

	requestJson, _ := json.Marshal(request)
	requestJsonReader := bytes.NewReader(requestJson)
	r, _ := http.NewRequest("PUT", "/channels/"+channelId+"/settings", requestJsonReader)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 200, w.Code)

	channelManagerMock.AssertCalled(t, "Update", mock.Anything, updatedChannel)
}

func TestUpdateSettingsOnlyRecent(t *testing.T) {
	m := SetupChannelEndpoints(false)

	channelId := getTestChannelId()
	expectedChannel := &dynamo.Channel{
		Id:                 dynamo.ChannelId(channelId),
		MinBits:            pointers.Int64P(5),
		MinBitsEmote:       pointers.Int64P(5),
		PinRecentCheers:    pointers.BoolP(true),
		RecentCheerMin:     pointers.Int64P(100),
		RecentCheerTimeout: timeUtils.SecondsToNanosecondsP(pointers.Int64P(60)),
	}
	channelManagerMock.On("Get", mock.Anything, channelId).Return(expectedChannel, nil)

	request := &apimodel.UpdateSettingsRequest{
		PinRecentCheers:    pointers.BoolP(true),
		RecentCheerMin:     pointers.Int64P(1234),
		RecentCheerTimeout: pointers.Int64P(300),
	}
	updatedChannel := models.UpdateSettingsRequestToDynamoChannel(request, channelId)
	channelManagerMock.On("Update", mock.Anything, updatedChannel).Return(nil)

	requestJson, _ := json.Marshal(request)
	requestJsonReader := bytes.NewReader(requestJson)
	r, _ := http.NewRequest("PUT", "/channels/"+channelId+"/settings", requestJsonReader)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 200, w.Code)

	channelManagerMock.AssertCalled(t, "Update", mock.Anything, updatedChannel)
}

func TestUpdateSettingsOnlyOnboarded(t *testing.T) {
	m := SetupChannelEndpoints(false)

	channelId := getTestChannelId()
	expectedChannel := &dynamo.Channel{
		Id:                 dynamo.ChannelId(channelId),
		MinBits:            pointers.Int64P(5),
		MinBitsEmote:       pointers.Int64P(5),
		PinRecentCheers:    pointers.BoolP(true),
		RecentCheerMin:     pointers.Int64P(100),
		RecentCheerTimeout: timeUtils.SecondsToNanosecondsP(pointers.Int64P(60)),
	}
	channelManagerMock.On("Get", mock.Anything, channelId).Return(expectedChannel, nil)

	request := &apimodel.UpdateSettingsRequest{
		Onboarded: pointers.BoolP(true),
	}
	updatedChannel := models.UpdateSettingsRequestToDynamoChannel(request, channelId)
	channelManagerMock.On("Update", mock.Anything, updatedChannel).Return(nil)

	requestJson, _ := json.Marshal(request)
	requestJsonReader := bytes.NewReader(requestJson)
	r, _ := http.NewRequest("PUT", "/channels/"+channelId+"/settings", requestJsonReader)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 200, w.Code)

	channelManagerMock.AssertCalled(t, "Update", mock.Anything, updatedChannel)
}

func TestUpdateSettingsAll(t *testing.T) {
	m := SetupChannelEndpoints(false)

	channelId := getTestChannelId()
	expectedChannel := &dynamo.Channel{
		Id:                  dynamo.ChannelId(channelId),
		MinBits:             pointers.Int64P(5),
		MinBitsEmote:        pointers.Int64P(5),
		PinRecentCheers:     pointers.BoolP(true),
		RecentCheerMin:      pointers.Int64P(100),
		RecentCheerTimeout:  timeUtils.SecondsToNanosecondsP(pointers.Int64P(60)),
		OptedOutOfVoldemort: pointers.BoolP(false),
	}
	channelManagerMock.On("Get", mock.Anything, channelId).Return(expectedChannel, nil)

	request := &apimodel.UpdateSettingsRequest{
		MinBits:             pointers.Int64P(10),
		MinBitsEmote:        pointers.Int64P(5),
		PinRecentCheers:     pointers.BoolP(true),
		PinTopCheers:        pointers.BoolP(false),
		RecentCheerMin:      pointers.Int64P(1234),
		RecentCheerTimeout:  pointers.Int64P(300),
		Onboarded:           pointers.BoolP(true),
		OptedOutOfVoldemort: pointers.BoolP(true),
	}
	updatedChannel := models.UpdateSettingsRequestToDynamoChannel(request, channelId)
	channelManagerMock.On("Update", mock.Anything, updatedChannel).Return(nil)

	requestJson, _ := json.Marshal(request)
	requestJsonReader := bytes.NewReader(requestJson)
	r, _ := http.NewRequest("PUT", "/channels/"+channelId+"/settings", requestJsonReader)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 200, w.Code)

	channelManagerMock.AssertCalled(t, "Update", mock.Anything, updatedChannel)
}

func TestUpdateSettingsPinnedDurationTooShort(t *testing.T) {
	m := SetupChannelEndpoints(false)

	channelId := getTestChannelId()

	request := &apimodel.UpdateSettingsRequest{
		RecentCheerTimeout: pointers.Int64P(minRecentCheerTimeoutSeconds - 1),
	}
	requestJson, _ := json.Marshal(request)
	requestJsonReader := bytes.NewReader(requestJson)
	r, _ := http.NewRequest("PUT", "/channels/"+channelId+"/settings", requestJsonReader)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 400, w.Code)
	resp, err := errors.ConvertJsonToErrorMessageResponse(w.Body.Bytes())
	assert.Nil(t, err)
	assert.Equal(t, constants.RecentTimeoutOutsideRangeError, resp.Message)
}

func TestUpdateSettingsPinnedDurationTooLong(t *testing.T) {
	m := SetupChannelEndpoints(false)

	channelId := getTestChannelId()

	request := &apimodel.UpdateSettingsRequest{
		RecentCheerTimeout: pointers.Int64P(maxRecentCheerTimeoutSeconds + 1),
	}
	requestJson, _ := json.Marshal(request)
	requestJsonReader := bytes.NewReader(requestJson)
	r, _ := http.NewRequest("PUT", "/channels/"+channelId+"/settings", requestJsonReader)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 400, w.Code)
	resp, err := errors.ConvertJsonToErrorMessageResponse(w.Body.Bytes())
	assert.Nil(t, err)
	assert.Equal(t, constants.RecentTimeoutOutsideRangeError, resp.Message)
}

func TestUpdateSettingsRecentAndTopWithoutDuration(t *testing.T) {
	m := SetupChannelEndpoints(false)

	channelId := getTestChannelId()
	expectedChannel := &dynamo.Channel{
		Id:                 dynamo.ChannelId(channelId),
		MinBits:            pointers.Int64P(5),
		MinBitsEmote:       pointers.Int64P(5),
		PinRecentCheers:    pointers.BoolP(true),
		RecentCheerMin:     pointers.Int64P(100),
		RecentCheerTimeout: timeUtils.SecondsToNanosecondsP(pointers.Int64P(60)),
	}
	channelManagerMock.On("Get", mock.Anything, channelId).Return(expectedChannel, nil)

	request := &apimodel.UpdateSettingsRequest{
		PinTopCheers:       pointers.BoolP(true),
		PinRecentCheers:    pointers.BoolP(true),
		RecentCheerTimeout: pointers.Int64P(0),
	}
	requestJson, _ := json.Marshal(request)
	requestJsonReader := bytes.NewReader(requestJson)
	r, _ := http.NewRequest("PUT", "/channels/"+channelId+"/settings", requestJsonReader)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 400, w.Code)
	resp, err := errors.ConvertJsonToErrorMessageResponse(w.Body.Bytes())
	assert.Nil(t, err)
	assert.Equal(t, constants.TopAndRecentRequiresDuration, resp.Message)
}

func TestUpdateSettingsPinnedAndRecentWithDuration(t *testing.T) {
	m := SetupChannelEndpoints(false)

	channelId := getTestChannelId()
	expectedChannel := &dynamo.Channel{
		Id:                 dynamo.ChannelId(channelId),
		MinBits:            pointers.Int64P(5),
		MinBitsEmote:       pointers.Int64P(5),
		PinRecentCheers:    pointers.BoolP(true),
		RecentCheerMin:     pointers.Int64P(100),
		RecentCheerTimeout: timeUtils.SecondsToNanosecondsP(pointers.Int64P(60)),
	}
	channelManagerMock.On("Get", mock.Anything, channelId).Return(expectedChannel, nil)

	request := &apimodel.UpdateSettingsRequest{
		PinRecentCheers:    pointers.BoolP(true),
		PinTopCheers:       pointers.BoolP(true),
		RecentCheerTimeout: pointers.Int64P(10),
	}
	updatedChannel := models.UpdateSettingsRequestToDynamoChannel(request, channelId)
	channelManagerMock.On("Update", mock.Anything, updatedChannel).Return(nil)

	requestJson, _ := json.Marshal(request)
	requestJsonReader := bytes.NewReader(requestJson)
	r, _ := http.NewRequest("PUT", "/channels/"+channelId+"/settings", requestJsonReader)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 200, w.Code)

	channelManagerMock.AssertCalled(t, "Update", mock.Anything, updatedChannel)
}

func TestUpdateSettingsLeaderboardSettings(t *testing.T) {
	m := SetupChannelEndpoints(false)

	channelId := getTestChannelId()
	expectedChannel := &dynamo.Channel{
		Id:                          dynamo.ChannelId(channelId),
		LeaderboardEnabled:          pointers.BoolP(true),
		IsCheerLeaderboardEnabled:   pointers.BoolP(false),
		IsSubGiftLeaderboardEnabled: pointers.BoolP(true),
		DefaultLeaderboard:          pointers.StringP(apimodel.DefaultLeaderboardCheer),
		LeaderboardTimePeriod:       pointers.StringP(pantheonrpc.TimeUnit_name[0]),
	}
	channelManagerMock.On("Get", mock.Anything, channelId).Return(expectedChannel, nil)

	request := &apimodel.UpdateSettingsRequest{
		LeaderboardEnabled:          pointers.BoolP(false),
		IsCheerLeaderboardEnabled:   pointers.BoolP(true),
		IsSubGiftLeaderboardEnabled: pointers.BoolP(false),
		DefaultLeaderboard:          pointers.StringP(apimodel.DefaultLeaderboardSubGift),
		LeaderboardTimePeriod:       pointers.StringP(pantheonrpc.TimeUnit_name[2]),
	}
	updatedChannel := models.UpdateSettingsRequestToDynamoChannel(request, channelId)
	channelManagerMock.On("Update", mock.Anything, updatedChannel).Return(nil)

	requestJson, _ := json.Marshal(request)
	requestJsonReader := bytes.NewReader(requestJson)
	r, _ := http.NewRequest("PUT", "/channels/"+channelId+"/settings", requestJsonReader)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 200, w.Code)

	channelManagerMock.AssertCalled(t, "Update", mock.Anything, updatedChannel)
}

// Get Opt Out Tests

func TestGetOptOutNonNumericUser(t *testing.T) {
	m := SetupChannelEndpoints(false)

	r, _ := http.NewRequest("GET", "/channels/user/opt-out", nil)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 400, w.Code)
	resp, err := errors.ConvertJsonToErrorMessageResponse(w.Body.Bytes())
	assert.Nil(t, err)
	assert.Equal(t, constants.NonNumericUserIdError, resp.Message)
}

func TestGetOptOutNotInDynamo(t *testing.T) {
	m := SetupChannelEndpoints(false)

	channelId := getTestChannelId()
	channelManagerMock.On("Get", mock.Anything, channelId).Return(nil, nil)

	r, _ := http.NewRequest("GET", "/channels/"+channelId+"/opt-out", nil)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 404, w.Code)
	resp, err := errors.ConvertJsonToErrorMessageResponse(w.Body.Bytes())
	assert.Nil(t, err)
	assert.Equal(t, channelNotFoundError, resp.Message)
}

func TestGetOptOutAttributeNotInDynamo(t *testing.T) {
	m := SetupChannelEndpoints(false)

	channelId := getTestChannelId()

	expectedChannel := &dynamo.Channel{
		Id:         dynamo.ChannelId(channelId),
		Annotation: pointers.StringP("test"),
	}
	channelManagerMock.On("Get", mock.Anything, channelId).Return(expectedChannel, nil)

	r, _ := http.NewRequest("GET", "/channels/"+channelId+"/opt-out", nil)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 404, w.Code)
	resp, err := errors.ConvertJsonToErrorMessageResponse(w.Body.Bytes())
	assert.Nil(t, err)
	assert.Equal(t, channelNotFoundError, resp.Message)
}

func TestGetOptOutExists(t *testing.T) {
	m := SetupChannelEndpoints(false)

	channelId := getTestChannelId()

	expectedChannel := &dynamo.Channel{
		Id:         dynamo.ChannelId(channelId),
		OptedOut:   pointers.BoolP(true),
		Annotation: pointers.StringP("test"),
	}
	channelManagerMock.On("Get", mock.Anything, channelId).Return(expectedChannel, nil)

	r, _ := http.NewRequest("GET", "/channels/"+channelId+"/opt-out", nil)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 200, w.Code)

	resultJson := w.Body.Bytes()
	result, err := apimodel.ConvertJsonToGetOptOutResponse(resultJson)
	if err != nil {
		t.Fail()
	}

	actualChannel := models.GetOptOutResponseToDynamoChannel(result)
	assert.True(t, expectedChannel.Equals(actualChannel))
}

// Set Opt Out Tests

func TestSetOptOutNonNumericUser(t *testing.T) {
	m := SetupChannelEndpoints(false)

	request := &apimodel.SetOptOutRequest{
		OptedOut:   true,
		Annotation: "test",
	}
	requestJson, _ := json.Marshal(request)
	requestJsonReader := bytes.NewReader(requestJson)
	r, _ := http.NewRequest("PUT", "/channels/user/opt-out", requestJsonReader)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 400, w.Code)
	resp, err := errors.ConvertJsonToErrorMessageResponse(w.Body.Bytes())
	assert.Nil(t, err)
	assert.Equal(t, constants.NonNumericUserIdError, resp.Message)
}

func TestSetOptOutInvalidJson(t *testing.T) {
	m := SetupChannelEndpoints(false)

	channelId := getTestChannelId()

	requestJsonReader := strings.NewReader("NotValidJson")
	r, _ := http.NewRequest("PUT", "/channels/"+channelId+"/opt-out", requestJsonReader)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 400, w.Code)
	resp, err := errors.ConvertJsonToErrorMessageResponse(w.Body.Bytes())
	assert.Nil(t, err)
	assert.Equal(t, constants.InvalidJsonError, resp.Message)
}

func TestSetOptOut(t *testing.T) {
	m := SetupChannelEndpoints(false)

	channelId := getTestChannelId()
	expectedChannel := &dynamo.Channel{
		Id:                 dynamo.ChannelId(channelId),
		MinBits:            pointers.Int64P(5),
		MinBitsEmote:       pointers.Int64P(5),
		PinRecentCheers:    pointers.BoolP(true),
		RecentCheerMin:     pointers.Int64P(100),
		RecentCheerTimeout: timeUtils.SecondsToNanosecondsP(pointers.Int64P(60)),
	}
	channelManagerMock.On("Get", mock.Anything, channelId).Return(expectedChannel, nil)

	request := &apimodel.SetOptOutRequest{
		OptedOut:   true,
		Annotation: "test",
	}
	updatedChannel := models.SetOptOutRequestToDynamoChannel(request, channelId)
	channelManagerMock.On("Update", mock.Anything, updatedChannel).Return(nil)

	requestJson, _ := json.Marshal(request)
	requestJsonReader := bytes.NewReader(requestJson)
	r, _ := http.NewRequest("PUT", "/channels/"+channelId+"/opt-out", requestJsonReader)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 200, w.Code)

	channelManagerMock.AssertCalled(t, "Update", mock.Anything, updatedChannel)
}

// Get Channel Info Tests

func TestGetChannelInfoNonNumericUserId(t *testing.T) {
	m := SetupChannelEndpoints(false)

	r, _ := http.NewRequest("GET", "/channels/user", nil)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 400, w.Code)
	resp, err := errors.ConvertJsonToErrorMessageResponse(w.Body.Bytes())
	assert.Nil(t, err)
	assert.Equal(t, constants.NonNumericUserIdError, resp.Message)
}

func TestGetChannelInfoNotPartnerNotInDatabase(t *testing.T) {
	m := SetupChannelEndpoints(false)

	channelId := getTestChannelId()
	channelManagerMock.On("Get", mock.Anything, channelId).Return(nil, nil)
	chatBadgesFetcher.On("Fetch", mock.Anything, channelId).Return(chatbadge_models.BitsChatBadgeTiers{BadgeTiers: []dynamo.BadgeTier{}, ChannelId: channelId}, nil)
	chatBadgesLogician.On("GetEnabledBadgeTiers", mock.Anything, channelId).Return([]int64{})
	// No opt out
	r, _ := http.NewRequest("GET", "/channels/"+channelId, nil)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 200, w.Code)
	response, err := apimodel.ConvertJsonToChannelEligibleResponse(w.Body.Bytes())
	assert.NoError(t, err)
	assert.False(t, response.Eligible)
	assert.Equal(t, int64(1), response.MinimumBits)
}

func TestGetChannelInfoNotPartnerFalseOptOut(t *testing.T) {
	m := SetupChannelEndpoints(false)

	channelId := getTestChannelId()
	expectedChannel := &dynamo.Channel{
		Id:       dynamo.ChannelId(channelId),
		OptedOut: pointers.BoolP(false),
	}
	channelManagerMock.On("Get", mock.Anything, channelId).Return(expectedChannel, nil)
	chatBadgesFetcher.On("Fetch", mock.Anything, channelId).Return(chatbadge_models.BitsChatBadgeTiers{BadgeTiers: []dynamo.BadgeTier{}, ChannelId: channelId}, nil)
	chatBadgesLogician.On("GetEnabledBadgeTiers", mock.Anything, channelId).Return([]int64{})

	r, _ := http.NewRequest("GET", "/channels/"+channelId, nil)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 200, w.Code)
	response, err := apimodel.ConvertJsonToChannelEligibleResponse(w.Body.Bytes())
	assert.NoError(t, err)
	assert.False(t, response.Eligible)
	assert.Equal(t, int64(1), response.MinimumBits)
}

func TestGetChannelInfoNotPartnerTrueOptOut(t *testing.T) {
	m := SetupChannelEndpoints(false)

	channelId := getTestChannelId()
	expectedChannel := &dynamo.Channel{
		Id:       dynamo.ChannelId(channelId),
		OptedOut: pointers.BoolP(true),
	}
	channelManagerMock.On("Get", mock.Anything, channelId).Return(expectedChannel, nil)
	chatBadgesFetcher.On("Fetch", mock.Anything, channelId).Return(chatbadge_models.BitsChatBadgeTiers{BadgeTiers: []dynamo.BadgeTier{}, ChannelId: channelId}, nil)
	chatBadgesLogician.On("GetEnabledBadgeTiers", mock.Anything, channelId).Return([]int64{})

	r, _ := http.NewRequest("GET", "/channels/"+channelId, nil)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 200, w.Code)
	response, err := apimodel.ConvertJsonToChannelEligibleResponse(w.Body.Bytes())
	assert.NoError(t, err)
	assert.False(t, response.Eligible)
	assert.Equal(t, int64(1), response.MinimumBits)
}

func TestGetChannelInfoPartnerNotOptedOutOrOnboarded(t *testing.T) {
	m := SetupChannelEndpoints(true)

	channelId := getTestChannelId()
	channelManagerMock.On("Get", mock.Anything, channelId).Return(nil, nil)
	chatBadgesFetcher.On("Fetch", mock.Anything, channelId).Return(chatbadge_models.BitsChatBadgeTiers{BadgeTiers: []dynamo.BadgeTier{}, ChannelId: channelId}, nil)
	chatBadgesLogician.On("GetEnabledBadgeTiers", mock.Anything, channelId).Return([]int64{})
	// No opt out or onboarded
	r, _ := http.NewRequest("GET", "/channels/"+channelId, nil)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 200, w.Code)
	response, err := apimodel.ConvertJsonToChannelEligibleResponse(w.Body.Bytes())
	assert.NoError(t, err)
	assert.False(t, response.Eligible)
	assert.Equal(t, int64(1), response.MinimumBits)
}

func TestGetChannelInfoPartnerNotOptedOutButOnboarded(t *testing.T) {
	// False opt out and onboarded
	m := SetupChannelEndpoints(true)

	channelId := getTestChannelId()

	expectedChannel := &dynamo.Channel{
		Id:        dynamo.ChannelId(channelId),
		OptedOut:  pointers.BoolP(false),
		Onboarded: pointers.BoolP(true),
	}
	channelManagerMock.On("Get", mock.Anything, channelId).Return(expectedChannel, nil)
	chatBadgesFetcher.On("Fetch", mock.Anything, channelId).Return(chatbadge_models.BitsChatBadgeTiers{BadgeTiers: []dynamo.BadgeTier{}, ChannelId: channelId}, nil)
	chatBadgesLogician.On("GetEnabledBadgeTiers", mock.Anything, channelId).Return([]int64{})

	r, _ := http.NewRequest("GET", "/channels/"+channelId, nil)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 200, w.Code)
	response, err := apimodel.ConvertJsonToChannelEligibleResponse(w.Body.Bytes())
	assert.NoError(t, err)
	assert.True(t, response.Eligible)
	assert.Equal(t, int64(1), response.MinimumBits)
}

func TestGetChannelInfoPartnerOptedOut(t *testing.T) {
	// True opt out (still onboarded)
	m := SetupChannelEndpoints(true)

	channelId := getTestChannelId()

	expectedChannel := &dynamo.Channel{
		Id:       dynamo.ChannelId(channelId),
		OptedOut: pointers.BoolP(true),
	}
	channelManagerMock.On("Get", mock.Anything, channelId).Return(expectedChannel, nil)
	chatBadgesFetcher.On("Fetch", mock.Anything, channelId).Return(chatbadge_models.BitsChatBadgeTiers{BadgeTiers: []dynamo.BadgeTier{}, ChannelId: channelId}, nil)
	chatBadgesLogician.On("GetEnabledBadgeTiers", mock.Anything, channelId).Return([]int64{})

	r, _ := http.NewRequest("GET", "/channels/"+channelId, nil)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 200, w.Code)
	response, err := apimodel.ConvertJsonToChannelEligibleResponse(w.Body.Bytes())
	assert.NoError(t, err)
	assert.False(t, response.Eligible)
	assert.Equal(t, int64(1), response.MinimumBits)
}

func TestGetChannelInfoEventNotEnabled(t *testing.T) {
	m := SetupChannelEndpoints(true)

	channelId := getTestChannelId()

	expectedChannel := &dynamo.Channel{
		Id:        dynamo.ChannelId(channelId),
		OptedOut:  pointers.BoolP(false),
		Onboarded: pointers.BoolP(true),
	}
	channelManagerMock.On("Get", mock.Anything, channelId).Return(expectedChannel, nil)
	chatBadgesFetcher.On("Fetch", mock.Anything, channelId).Return(chatbadge_models.BitsChatBadgeTiers{BadgeTiers: []dynamo.BadgeTier{}, ChannelId: channelId}, nil)
	chatBadgesLogician.On("GetEnabledBadgeTiers", mock.Anything, channelId).Return([]int64{})

	// should default to false
	r, _ := http.NewRequest("GET", "/channels/"+channelId, nil)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 200, w.Code)
	response, err := apimodel.ConvertJsonToChannelEligibleResponse(w.Body.Bytes())
	assert.NoError(t, err)
	assert.True(t, response.Eligible)
	assert.Equal(t, "", response.Event)
}

func TestGetChannelInfoEventEnabled(t *testing.T) {
	m := SetupChannelEndpoints(true)

	channelId := getTestChannelId()
	expectedChannel := &dynamo.Channel{
		Id:        dynamo.ChannelId(channelId),
		OptedOut:  pointers.BoolP(false),
		Onboarded: pointers.BoolP(true),
	}
	channelManagerMock.On("Get", mock.Anything, channelId).Return(expectedChannel, nil)
	chatBadgesFetcher.On("Fetch", mock.Anything, channelId).Return(chatbadge_models.BitsChatBadgeTiers{BadgeTiers: []dynamo.BadgeTier{}, ChannelId: channelId}, nil)
	chatBadgesLogician.On("GetEnabledBadgeTiers", mock.Anything, channelId).Return([]int64{})

	r, _ := http.NewRequest("GET", "/channels/"+channelId, nil)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 200, w.Code)
	response, err := apimodel.ConvertJsonToChannelEligibleResponse(w.Body.Bytes())
	assert.NoError(t, err)
	assert.True(t, response.Eligible)
}

func TestGetChannelInfoSetMinBitsNoChannelInDynamo(t *testing.T) {
	m := SetupChannelEndpoints(true)

	channelId := getTestChannelId()
	channelManagerMock.On("Get", mock.Anything, channelId).Return(nil, nil)
	chatBadgesFetcher.On("Fetch", mock.Anything, channelId).Return(chatbadge_models.BitsChatBadgeTiers{BadgeTiers: []dynamo.BadgeTier{}, ChannelId: channelId}, nil)
	chatBadgesLogician.On("GetEnabledBadgeTiers", mock.Anything, channelId).Return([]int64{})

	// No channel in dynamo
	r, _ := http.NewRequest("GET", "/channels/"+channelId, nil)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 200, w.Code)
	response, err := apimodel.ConvertJsonToChannelEligibleResponse(w.Body.Bytes())
	assert.NoError(t, err)
	assert.False(t, response.Eligible)
	assert.Equal(t, int64(1), response.MinimumBits)
}

func TestGetChannelInfoSetMinBitsNoMinBitsAndOnboarded(t *testing.T) {
	m := SetupChannelEndpoints(true)

	channelId := getTestChannelId()
	// Set channel in dynamo, no min bits and onboarded
	expectedChannel := &dynamo.Channel{
		Id:        dynamo.ChannelId(channelId),
		OptedOut:  pointers.BoolP(false),
		Onboarded: pointers.BoolP(true),
	}
	channelManagerMock.On("Get", mock.Anything, channelId).Return(expectedChannel, nil)
	chatBadgesFetcher.On("Fetch", mock.Anything, channelId).Return(chatbadge_models.BitsChatBadgeTiers{BadgeTiers: []dynamo.BadgeTier{}, ChannelId: channelId}, nil)
	chatBadgesLogician.On("GetEnabledBadgeTiers", mock.Anything, channelId).Return([]int64{})

	r, _ := http.NewRequest("GET", "/channels/"+channelId, nil)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 200, w.Code)
	response, err := apimodel.ConvertJsonToChannelEligibleResponse(w.Body.Bytes())
	assert.NoError(t, err)
	assert.True(t, response.Eligible)
	assert.Equal(t, int64(1), response.MinimumBits)
}

func TestGetChannelInfoSetMinBitsAndMinBitsEmote(t *testing.T) {
	m := SetupChannelEndpoints(true)

	channelId := getTestChannelId()
	// Set min bits and min bits emote in dynamo
	expectedChannel := &dynamo.Channel{
		Id:           dynamo.ChannelId(channelId),
		OptedOut:     pointers.BoolP(false),
		Onboarded:    pointers.BoolP(true),
		MinBits:      pointers.Int64P(1234),
		MinBitsEmote: pointers.Int64P(999),
	}
	channelManagerMock.On("Get", mock.Anything, channelId).Return(expectedChannel, nil)
	chatBadgesFetcher.On("Fetch", mock.Anything, channelId).Return(chatbadge_models.BitsChatBadgeTiers{BadgeTiers: []dynamo.BadgeTier{}, ChannelId: channelId}, nil)
	chatBadgesLogician.On("GetEnabledBadgeTiers", mock.Anything, channelId).Return([]int64{})

	r, _ := http.NewRequest("GET", "/channels/"+channelId, nil)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 200, w.Code)
	response, err := apimodel.ConvertJsonToChannelEligibleResponse(w.Body.Bytes())
	assert.NoError(t, err)
	assert.True(t, response.Eligible)
	assert.Equal(t, int64(1234), response.MinimumBits)
	assert.Equal(t, int64(999), response.MinimumBitsEmote)
}

func TestGetChannelInfoOptedOutOfVoldemort(t *testing.T) {
	m := SetupChannelEndpoints(false)

	channelId := getTestChannelId()

	expectedChannel := &dynamo.Channel{
		Id:                  dynamo.ChannelId(channelId),
		OptedOutOfVoldemort: pointers.BoolP(false),
	}
	channelManagerMock.On("Get", mock.Anything, channelId).Return(expectedChannel, nil)
	chatBadgesFetcher.On("Fetch", mock.Anything, channelId).Return(chatbadge_models.BitsChatBadgeTiers{BadgeTiers: []dynamo.BadgeTier{}, ChannelId: channelId}, nil)
	chatBadgesLogician.On("GetEnabledBadgeTiers", mock.Anything, channelId).Return([]int64{})

	r, _ := http.NewRequest("GET", "/channels/"+channelId, nil)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 200, w.Code)
	response, err := apimodel.ConvertJsonToChannelEligibleResponse(w.Body.Bytes())
	assert.NoError(t, err)
	assert.False(t, response.OptedOutOfVoldemort)
}

func TestGetChannelInfoErrorFromGetBadgeTiers(t *testing.T) {
	m := SetupChannelEndpoints(false)

	channelId := getTestChannelId()

	expectedChannel := &dynamo.Channel{
		Id:                  dynamo.ChannelId(channelId),
		OptedOutOfVoldemort: pointers.BoolP(false),
		Onboarded:           pointers.BoolP(true),
	}
	channelManagerMock.On("Get", mock.Anything, channelId).Return(expectedChannel, nil)
	chatBadgesFetcher.On("Fetch", mock.Anything, channelId).Return(chatbadge_models.BitsChatBadgeTiers{}, go_errors.New("dummy fetch error"))

	r, _ := http.NewRequest("GET", "/channels/"+channelId, nil)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 500, w.Code)
	assert.Contains(t, w.Body.String(), "Encountered error from downstream dependency")
}

func TestUpdateSettingsOnlyOptedOutOfVoldemort(t *testing.T) {
	m := SetupChannelEndpoints(false)

	channelId := getTestChannelId()
	expectedChannel := &dynamo.Channel{
		Id:                  dynamo.ChannelId(channelId),
		OptedOutOfVoldemort: pointers.BoolP(false),
	}
	channelManagerMock.On("Get", mock.Anything, channelId).Return(expectedChannel, nil)

	request := &apimodel.UpdateSettingsRequest{
		OptedOutOfVoldemort: pointers.BoolP(true),
	}
	updatedChannel := models.UpdateSettingsRequestToDynamoChannel(request, channelId)
	channelManagerMock.On("Update", mock.Anything, updatedChannel).Return(nil)

	requestJson, _ := json.Marshal(request)
	requestJsonReader := bytes.NewReader(requestJson)
	r, _ := http.NewRequest("PUT", "/channels/"+channelId+"/settings", requestJsonReader)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 200, w.Code)

	channelManagerMock.AssertCalled(t, "Update", mock.Anything, updatedChannel)
}
