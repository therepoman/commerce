package api

import (
	"bytes"
	"context"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"code.justin.tv/commerce/payday/api/apidef"
	"code.justin.tv/commerce/payday/clients/cloudwatchlogger"
	"code.justin.tv/commerce/payday/decorators"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/entitle"
	"code.justin.tv/commerce/payday/errors"
	balance_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/balance"
	dynamo_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo"
	entitle_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/entitle"
	metrics_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/metrics"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/models/api"
	"code.justin.tv/commerce/payday/utils/pointers"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/zenazn/goji/web"
)

func TestEntitleApi_EntitleBits(t *testing.T) {
	Convey("given an entitle bits API", t, func() {
		entitler := new(entitle_mock.Parallel)
		validator := entitle.NewValidator()
		balanceGetter := new(balance_mock.Getter)
		transactionsDao := new(dynamo_mock.ITransactionsDao)
		statter := new(metrics_mock.Statter)
		cwlogger := cloudwatchlogger.NewCloudWatchLogNoopClient()

		entitleAPI := EntitleApi{
			Entitler:       entitler,
			TransactionDAO: transactionsDao,
			Validator:      validator,
			BalanceGetter:  balanceGetter,
			Statter:        statter,
			AuditLogger:    cwlogger,
		}

		updatedBitsAmount := int32(322)

		statter.On("Inc", "Entitle_Bad_Idempotency_Error", int64(1)).Return()

		Convey("when we have invalid JSON", func() {
			buffer := new(bytes.Buffer)
			buffer.WriteString("{")
			r, _ := http.NewRequest("POST", "/authed/entitleBits", buffer)
			w := httptest.NewRecorder()

			m := web.New()

			m.Post("/authed/entitleBits", decorators.DecorateAndAdapt(entitleAPI.EntitleBits, apidef.EntitleBits))
			m.ServeHTTP(w, r)

			assert.Equal(t, 400, w.Code)
			assert.Empty(t, entitler.Calls)
		})

		Convey("when we have valid JSON", func() {
			Convey("when transaction DAO returns an error", func() {
				transactionsDao.On("Get", mock.Anything).Return(nil, errors.New("ANIME_BEAR_REBELLION_IN_PROGRESS"))
				file, _ := os.Open("testdata/entitle-request.json")
				r, _ := http.NewRequest("POST", "/authed/entitleBits", file)
				w := httptest.NewRecorder()

				m := web.New()

				m.Post("/authed/entitleBits", decorators.DecorateAndAdapt(entitleAPI.EntitleBits, apidef.EntitleBits))
				m.ServeHTTP(w, r)

				assert.Equal(t, 500, w.Code)
			})

			Convey("when transaction DAO returns no transaction", func() {
				transactionsDao.On("Get", mock.Anything).Return(nil, nil)
				Convey("when entitler returns an error", func() {
					entitler.On("Entitle", mock.Anything, mock.Anything).Return(int32(0), errors.New("WALRUS STRIKE"))

					file, _ := os.Open("testdata/entitle-request.json")
					r, _ := http.NewRequest("POST", "/authed/entitleBits", file)
					w := httptest.NewRecorder()

					m := web.New()

					m.Post("/authed/entitleBits", decorators.DecorateAndAdapt(entitleAPI.EntitleBits, apidef.EntitleBits))
					m.ServeHTTP(w, r)

					assert.Equal(t, 500, w.Code)
				})

				Convey("when Pachinko succeeds", func() {
					entitler.On("Entitle", mock.Anything, mock.Anything).Return(updatedBitsAmount, nil)

					file, _ := os.Open("testdata/entitle-request.json")
					r, _ := http.NewRequest("POST", "/authed/entitleBits", file)
					w := httptest.NewRecorder()

					m := web.New()

					m.Post("/authed/entitleBits", decorators.DecorateAndAdapt(entitleAPI.EntitleBits, apidef.EntitleBits))
					m.ServeHTTP(w, r)

					assert.Equal(t, 200, w.Code)
				})
			})

			Convey("when transaction DAO returns a match", func() {
				transactionsDao.On("Get", mock.Anything).Return(&dynamo.Transaction{
					UserId:        pointers.StringP("46024993"),
					TransactionId: "10569-2a6be410dbfe7a2bb3b6b41448b6b50a3289476b-1483737459",
					Platform:      pointers.StringP("ios"),
					ProductId:     pointers.StringP("1234"),
				}, nil)
				balanceGetter.On("GetBalance", mock.Anything, mock.Anything, mock.Anything).Return(int32(44), nil, nil)

				file, _ := os.Open("testdata/entitle-request.json")
				r, _ := http.NewRequest("POST", "/authed/entitleBits", file)
				w := httptest.NewRecorder()

				m := web.New()

				m.Post("/authed/entitleBits", decorators.DecorateAndAdapt(entitleAPI.EntitleBits, apidef.EntitleBits))
				m.ServeHTTP(w, r)

				assert.Equal(t, 200, w.Code)
				var response api.EntitleBitsResponse
				_ = json.Unmarshal(w.Body.Bytes(), &response)
				So(response.BitsBalance, ShouldEqual, int32(44))
			})

			Convey("when transactions DAO returns a mismatch", func() {
				transactionsDao.On("Get", mock.Anything).Return(&dynamo.Transaction{
					UserId:        pointers.StringP("46024993"),
					TransactionId: "10569-2a6be410dbfe7a2bb3b6b41448b6b50a3289476b-1483737459",
					Platform:      pointers.StringP("android"),
					ProductId:     pointers.StringP("56"),
				}, nil)

				file, _ := os.Open("testdata/entitle-request.json")
				r, _ := http.NewRequest("POST", "/authed/entitleBits", file)
				w := httptest.NewRecorder()

				m := web.New()

				m.Post("/authed/entitleBits", decorators.DecorateAndAdapt(entitleAPI.EntitleBits, apidef.EntitleBits))
				m.ServeHTTP(w, r)

				assert.Equal(t, 400, w.Code)
			})
		})
	})
}

func TestEntitleApi_idempotencyCheck(t *testing.T) {
	ctx := context.Background()

	Convey("given an entitle bits API", t, func() {
		balanceGetter := new(balance_mock.Getter)
		transactionsDao := new(dynamo_mock.ITransactionsDao)
		statter := new(metrics_mock.Statter)

		entitleAPI := EntitleApi{
			TransactionDAO: transactionsDao,
			BalanceGetter:  balanceGetter,
			Statter:        statter,
		}

		statter.On("Inc", "Entitle_Bad_Idempotency_Error", int64(1)).Return()

		Convey("when no transaction exists", func() {
			transactionsDao.On("Get", mock.Anything).Return(nil, nil)

			resp, err := entitleAPI.idempotencyCheck(ctx, api.EntitleBitsRequest{
				TransactionId: "1234",
			})

			So(resp, ShouldBeNil)
			So(err, ShouldBeNil)
		})

		Convey("when balance getter is working", func() {
			balanceGetter.On("GetBalance", ctx, mock.Anything, mock.Anything).Return(int32(14), nil, nil)

			Convey("when the transaction exists both are empty", func() {
				transactionsDao.On("Get", mock.Anything).Return(&dynamo.Transaction{
					TransactionId: "1234",
				}, nil)

				resp, err := entitleAPI.idempotencyCheck(ctx, api.EntitleBitsRequest{
					TransactionId: "1234",
				})

				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
				So(resp.BitsBalance, ShouldEqual, 14)
			})

			Convey("when the transaction exists, but they're different", func() {
				transactionsDao.On("Get", mock.Anything).Return(&dynamo.Transaction{
					TransactionId: "1234",
				}, nil)

				resp, err := entitleAPI.idempotencyCheck(ctx, api.EntitleBitsRequest{
					TransactionId: "1234",
					Platform:      string(models.IOS),
				})

				So(err, ShouldNotBeNil)
				So(err.Error(), ShouldContainSubstring, "Duplicate transaction id for non-duplicate request")
				So(resp, ShouldBeNil)
			})
		})

		Convey("when balance getter is failing", func() {
			balanceGetter.On("GetBalance", ctx, mock.Anything, mock.Anything).Return(int32(0), nil, errors.New("ANIME_BEAR_REBELLION_IN_PROGRESS"))

			Convey("when the transactions don't match", func() {
				transactionsDao.On("Get", mock.Anything).Return(&dynamo.Transaction{
					TransactionId: "1234",
				}, nil)

				resp, err := entitleAPI.idempotencyCheck(ctx, api.EntitleBitsRequest{
					TransactionId: "1234",
					Platform:      string(models.PAYPAL),
				})

				So(err, ShouldNotBeNil)
				So(err.Error(), ShouldContainSubstring, "Duplicate transaction id for non-duplicate request")
				So(resp, ShouldBeNil)
			})

			Convey("when the transactions match", func() {
				transactionsDao.On("Get", mock.Anything).Return(&dynamo.Transaction{
					TransactionId: "1234",
				}, nil)

				resp, err := entitleAPI.idempotencyCheck(ctx, api.EntitleBitsRequest{
					TransactionId: "1234",
				})

				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
				So(resp.BitsBalance, ShouldEqual, int32(0))
			})
		})
	})
}
