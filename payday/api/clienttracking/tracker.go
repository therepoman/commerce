package clienttracking

import (
	"context"
	"fmt"

	"code.justin.tv/commerce/payday/metrics"
	"code.justin.tv/commerce/payday/middleware"
	"code.justin.tv/commerce/payday/oauth"
)

type Source string

const (
	GetBalance     = Source("get_balance")
	GetProducts    = Source("get_products")
	GetActions     = Source("get_actions")
	GetChannelInfo = Source("get_channel_info")

	clientIDNotFound = "NOT_FOUND"
	metricFormat     = "clienttracking.%s.%s"
)

type Tracker interface {
	Track(ctx context.Context, source Source)
}

type tracker struct {
	Statter metrics.Statter `inject:""`
}

func NewTracker() Tracker {
	return &tracker{}
}

func metricName(source Source, clientID string) string {
	return fmt.Sprintf(metricFormat, string(source), clientID)
}

func (t *tracker) Track(ctx context.Context, source Source) {
	clientID := middleware.GetClientID(ctx)

	if clientID == "" {
		clientID = clientIDNotFound
	}

	platform, _ := oauth.GetClient(clientID)
	if platform != oauth.UnknownPlatform {
		t.Statter.Inc(metricName(source, platform), 1)
	} else {
		t.Statter.Inc(metricName(source, clientID), 1)
	}
}
