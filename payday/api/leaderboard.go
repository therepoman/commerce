package api

import (
	"context"
	"fmt"
	"net/http"
	"strconv"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	"code.justin.tv/commerce/payday/api/constants"
	"code.justin.tv/commerce/payday/api/utils"
	cache "code.justin.tv/commerce/payday/cache/channel"
	"code.justin.tv/commerce/payday/clients/pantheon"
	cfg "code.justin.tv/commerce/payday/config"
	httperror "code.justin.tv/commerce/payday/errors/http"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/models/api"
	"code.justin.tv/commerce/payday/utils/pointers"
	"github.com/zenazn/goji/web"
)

const (
	DefaultTopN              = 10
	DefaultRange             = 5
	PantheonBitsGlobalDomain = "GLOBAL"
	TopNQueryParam           = "top_n"
	RangeQueryParam          = "range"
	TuidQueryParam           = "tuid"
)

type LeaderboardApi struct {
	PantheonClient pantheon.IPantheonClientWrapper `inject:""`
	PaydayConfig   *cfg.Configuration              `inject:""`
	ChannelManager cache.ChannelManager            `inject:""`
}

func (l *LeaderboardApi) GetChannelLeaderboard(c web.C, w http.ResponseWriter, r *http.Request) error {
	ctx, cancel := context.WithTimeout(r.Context(), utils.DefaultTimeout)
	defer cancel()

	var topNum int
	topNumParam := r.URL.Query().Get(TopNQueryParam)
	topNum, err := strconv.Atoi(topNumParam)
	if err != nil {
		topNum = DefaultTopN
	}

	var surroundingRange int
	surroundingRangeParam := r.URL.Query().Get(RangeQueryParam)
	surroundingRange, err = strconv.Atoi(surroundingRangeParam)
	if err != nil {
		surroundingRange = DefaultRange
	}

	tuid := r.URL.Query().Get(TuidQueryParam)
	channelID := c.URLParams["channel"]

	channel, err := l.ChannelManager.Get(ctx, channelID)
	if err != nil {
		log.WithError(err).WithField("channelID", channelID).Error("Error getting channel from dynamo for GetLeaderboard")
		return err
	}

	if !cache.GetChannelEligibility(channel) {
		return httperror.New(constants.ChannelIneligibleError, http.StatusPreconditionFailed)
	}

	ltp := pointers.StringOrDefault(channel.LeaderboardTimePeriod, models.DefaultLeaderboardTimePeriod)

	timeUnitValue, ok := pantheonrpc.TimeUnit_value[ltp]
	if !ok {
		log.WithField("time_period", *channel.LeaderboardTimePeriod).Error("Received invalid leaderboard time period from DynamoDB")
		return httperror.New(constants.InvalidLeaderboardSettingError, http.StatusInternalServerError)
	}

	pantheonApiResponse, err := l.PantheonClient.GetLeaderboard(ctx, pantheon.PantheonBitsUsageDomain, pantheonrpc.TimeUnit(timeUnitValue), channelID, tuid, int64(topNum), int64(surroundingRange), true)
	if err != nil {
		log.WithError(err).WithFields(log.Fields{
			"channelID": channelID,
			"userID":    tuid,
			"timeUnit":  timeUnitValue,
		}).Error("Error calling pantheon for GetLeaderboard")
		return err
	}

	response := makeGetLeaderboardApiResponse(pantheonApiResponse)
	utils.WriteResponse(response, w)
	return nil
}

func makeGetLeaderboardApiResponse(apiResp *pantheonrpc.GetLeaderboardResp) (leaderboardResponse api.GetLeaderboardResponse) {
	var topEntries = make([]api.LeaderboardEntry, 0)
	var entryContext = make([]api.LeaderboardEntry, 0)

	for _, entry := range apiResp.Top {
		if entry != nil {
			topEntries = append(topEntries, api.LeaderboardEntry{
				Rank:     entry.Rank,
				Score:    entry.Score,
				EntryKey: entry.EntryKey,
				ID:       fmt.Sprintf("%s_%s", apiResp.Identifier, entry.EntryKey),
			})
		}
	}

	leaderboardResponse.Top = topEntries

	if apiResp.EntryContext == nil {
		return
	}

	if apiResp.EntryContext.Entry != nil {
		leaderboardResponse.Entry = api.LeaderboardEntry{
			EntryKey: apiResp.EntryContext.Entry.EntryKey,
			Rank:     apiResp.EntryContext.Entry.Rank,
			Score:    apiResp.EntryContext.Entry.Score,
			ID:       fmt.Sprintf("%s_%s", apiResp.Identifier, apiResp.EntryContext.Entry.EntryKey),
		}
	}

	for _, entry := range apiResp.EntryContext.Context {
		if entry != nil {
			entryContext = append(entryContext, api.LeaderboardEntry{
				Rank:     entry.Rank,
				Score:    entry.Score,
				EntryKey: entry.EntryKey,
				ID:       fmt.Sprintf("%s_%s", apiResp.Identifier, entry.EntryKey),
			})
		}
	}

	leaderboardResponse.EntryContext = entryContext
	leaderboardResponse.ID = apiResp.Identifier
	leaderboardResponse.SecondsRemaining = getLeaderboardSecondsRemaining(apiResp.EndTime)
	return
}

func getLeaderboardSecondsRemaining(endTimeInt uint64) int32 {
	if endTimeInt == 0 {
		return 0
	}

	durationRemaining := time.Until(time.Unix(0, int64(endTimeInt)))
	secRemaining := durationRemaining.Seconds()
	return int32(secRemaining)
}
