package utils

// Helper functions used by API calls

import (
	"context"
	"encoding/json"
	"net/http"
	"strconv"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/api/constants"
	httperror "code.justin.tv/commerce/payday/errors/http"
	"github.com/davecgh/go-spew/spew"
)

const (
	DefaultTUID    = "0"
	DefaultTimeout = 5 * time.Second
)

func WriteResponse(t interface{}, w http.ResponseWriter) {
	defer func() {
		if r := recover(); r != nil {
			log.WithFields(log.Fields{
				"panic": r,
				// using `spew` to print the struct so that we don't panic again if the source of the original panic was
				// json serialization
				// DO NOT USE this `spew` anywhere else!
				// FYI, we used to use fmt.Sprintf("%+v", t) but this was causing panic...
				"payload": spew.Sdump(t),
			}).Error("Panicked writing HTTP response")
			w.WriteHeader(http.StatusInternalServerError)
			_, _ = w.Write([]byte("Internal server error"))
		}
	}()

	resp, err := json.Marshal(t)

	if err != nil {
		log.WithError(err).WithField("payload", t).Error("Unable to marshal into json")
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte("Unable to marshal response"))
		return
	}

	w.Header().Add("Content-Type", "application/json")
	_, err = w.Write(resp)

	if err != nil {
		log.WithError(err).Error("Failed while writing a (normal) response out via HTTP")
	}
}

func WriteJSONResponse(jsonBytes []byte, w http.ResponseWriter) {
	defer func() {
		if r := recover(); r != nil {
			log.WithFields(log.Fields{
				"panic": r,
				// using `spew` to print the struct so that we don't panic again if the source of the original panic was
				// json serialization
				// DO NOT USE this `spew` anywhere else!
				// FYI, we used to use fmt.Sprintf("%+v", t) but this was causing panic...
				"payload": spew.Sdump(string(jsonBytes)),
			}).Error("Panicked writing HTTP response")
			w.WriteHeader(http.StatusInternalServerError)
			_, _ = w.Write([]byte("Internal server error"))
		}
	}()

	w.Header().Add("Content-Type", "application/json")
	_, err := w.Write(jsonBytes)
	if err != nil {
		log.WithError(err).Error("Failed while writing a (normal) response out via HTTP")
	}
}

func IsNumericUserId(userId string) bool {
	_, conversionError := strconv.ParseInt(userId, 10, 64)
	return conversionError == nil
}

func ValidateRequestUserId(userId string) error {
	if !IsNumericUserId(userId) {
		log.WithField("userId", userId).Error("Called with non-numeric userId")
		return httperror.New(constants.NonNumericUserIdError, http.StatusBadRequest)
	}
	return nil
}

func NewContextWithDefaultTimeout() (context.Context, context.CancelFunc) {
	return context.WithTimeout(context.Background(), DefaultTimeout)
}

func ContextWithDefaultTimeout(ctx context.Context) (context.Context, context.CancelFunc) {
	return context.WithTimeout(ctx, DefaultTimeout)
}
