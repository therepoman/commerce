package api

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"time"

	"code.justin.tv/commerce/gogogadget/audit"
	"code.justin.tv/commerce/gogogadget/pointers"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/api/utils"
	"code.justin.tv/commerce/payday/balance"
	"code.justin.tv/commerce/payday/clients/cloudwatchlogger"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/entitle"
	httperror "code.justin.tv/commerce/payday/errors/http"
	"code.justin.tv/commerce/payday/metrics"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/models/api"
	"github.com/zenazn/goji/web"
)

type EntitleApi struct {
	Entitler       entitle.Parallel                  `inject:"parallelEntitler"`
	Validator      entitle.Validator                 `inject:""`
	TransactionDAO dynamo.ITransactionsDao           `inject:""`
	Statter        metrics.Statter                   `inject:""`
	BalanceGetter  balance.Getter                    `inject:""`
	AuditLogger    cloudwatchlogger.CloudWatchLogger `inject:""`
}

func (e *EntitleApi) EntitleBits(c web.C, w http.ResponseWriter, r *http.Request) error {
	ctx, cancel := context.WithTimeout(r.Context(), 10*time.Second)
	defer cancel()

	body, err := e.deserializeBody(r.Body)
	if err != nil {
		return err
	}

	apiName := "EntitleBits"
	msg, err := audit.CreateAccessLogMsg(ctx, apiName, body)
	if err != nil {
		log.WithError(err).Error(fmt.Sprintf("failed to generate access log msg in %s", apiName))
	} else {
		err = e.AuditLogger.Send(ctx, msg)
		if err != nil {
			log.WithError(err).Warn(fmt.Sprintf("error while sending audit log in %s", apiName))
		}
	}

	platform := models.ParsePlatform(body.Platform, pointers.StringP("EntitleBits"), pointers.StringP(body.TransactionId), nil)
	err = e.Validator.IsValid(body, platform)
	if err != nil {
		return err
	}

	resp, err := e.idempotencyCheck(ctx, body)
	if err != nil {
		return err
	} else if resp == nil {
		updatedBalance, err := e.Entitler.Entitle(ctx, body)
		if err != nil {
			log.WithError(err).Error("failed to entitle bits")
			return httperror.NewWithCause("Downstream dependency error", http.StatusInternalServerError, err)
		}

		resp = &api.EntitleBitsResponse{
			BitsBalance: int32(updatedBalance),
		}
	}

	utils.WriteResponse(*resp, w)

	return nil
}

func (e *EntitleApi) deserializeBody(body io.Reader) (api.EntitleBitsRequest, error) {
	decoder := json.NewDecoder(body)
	var request api.EntitleBitsRequest

	err := decoder.Decode(&request)
	if err != nil {
		log.WithError(err).Error("Error decoding JSON payload for entitle bits call")
		return api.EntitleBitsRequest{}, httperror.NewWithCause("Unable to deserialize request", http.StatusBadRequest, err)
	}

	return request, nil
}

func (e *EntitleApi) idempotencyCheck(ctx context.Context, request api.EntitleBitsRequest) (*api.EntitleBitsResponse, error) {
	tx, err := e.TransactionDAO.Get(request.TransactionId)
	if err != nil {
		return nil, err
	}

	if tx != nil {
		txPlatform := pointers.StringOrDefault(tx.Platform, "")
		txUserId := pointers.StringOrDefault(tx.UserId, "")
		txProductId := pointers.StringOrDefault(tx.ProductId, "")

		if txPlatform != request.Platform ||
			txUserId != request.TwitchUserId ||
			txProductId != request.ProductId {
			log.WithFields(log.Fields{
				"request":     request,
				"txPlatform":  txPlatform,
				"txProductId": txProductId,
				"txUserId":    txUserId,
			}).Warn("Idempotency error on entitle")
			go e.Statter.Inc("Entitle_Bad_Idempotency_Error", 1)
			return nil, badIdempotencyError()
		}

		updatedBalance, _ := e.BalanceGetter.GetBalance(ctx, request.TwitchUserId, false)
		return &api.EntitleBitsResponse{
			BitsBalance: updatedBalance,
		}, nil
	}
	return nil, nil
}

func badIdempotencyError() error {
	return httperror.New("Duplicate transaction id for non-duplicate request", http.StatusBadRequest)
}
