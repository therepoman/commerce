package api

import (
	"context"
	"net/http"
	"time"

	"code.justin.tv/commerce/gogogadget/pointers"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/api/clienttracking"
	"code.justin.tv/commerce/payday/api/utils"
	"code.justin.tv/commerce/payday/config"
	httperror "code.justin.tv/commerce/payday/errors/http"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/models/api"
	"code.justin.tv/commerce/payday/products"
	petozi "code.justin.tv/commerce/petozi/rpc"
	"github.com/zenazn/goji/web"
)

type ProductsApi struct {
	Validator      products.Validator     `inject:""`
	Getter         products.Getter        `inject:""`
	Config         *config.Configuration  `inject:""`
	ClientTracking clienttracking.Tracker `inject:""`
}

func (a *ProductsApi) GetProducts(c web.C, w http.ResponseWriter, r *http.Request) error {
	ctx, cancel := context.WithTimeout(r.Context(), 1*time.Second)
	defer cancel()

	go a.ClientTracking.Track(ctx, clienttracking.GetProducts)

	tuid := r.URL.Query().Get("user_id")
	locale := r.URL.Query().Get("locale")
	country := r.URL.Query().Get("country")
	parsedPlatform, petoziPlatform := a.getPlatform(r.URL.Query().Get("platform"))

	err, returnEmpty := a.Validator.Validate(ctx, tuid, locale, country, parsedPlatform)
	if err != nil {
		return err
	} else if returnEmpty {
		utils.WriteResponse([]api.Product{}, w)
		return nil
	}

	productRecords, err := a.Getter.Get(ctx, tuid, locale, parsedPlatform)
	if err != nil {
		log.WithError(err).WithField("platform", parsedPlatform).Error("Error getting products")
		return httperror.NewWithCause("Failure to get products", http.StatusInternalServerError, err)
	}

	productRecords = a.getPricingId(ctx, petoziPlatform, productRecords)
	utils.WriteResponse(productRecords, w)

	return nil
}

func (a *ProductsApi) getPricingId(ctx context.Context, platform petozi.Platform, apiProducts []api.Product) []api.Product {
	productIDs := make([]string, 0)
	for _, product := range apiProducts {
		productIDs = append(productIDs, product.Id)
	}
	bitsProducts, err := a.Getter.GetByIDs(ctx, platform, productIDs)
	if err != nil || bitsProducts == nil {
		return apiProducts
	}

	for index, apiProduct := range apiProducts {
		bitsProduct, ok := bitsProducts[apiProduct.Id]
		if !ok {
			continue
		}
		apiProducts[index].PricingId = &bitsProduct.PricingId
		apiProducts[index].OfferId = &bitsProduct.OfferId
	}

	return apiProducts
}

func (a *ProductsApi) getPlatform(platformString string) (models.Platform, petozi.Platform) {
	parsedPlatform := models.ParsePlatform(platformString, pointers.StringP("GetPlatform"), nil, nil)

	if parsedPlatform == models.DEFAULT {
		return parsedPlatform, petozi.Platform_PAYPAL
	}

	platformValue, ok := petozi.Platform_value[string(parsedPlatform)]
	// Question: What do we do here? Do we ever call this with values like FORTUNA or UPSIGHT?
	if !ok {
		return parsedPlatform, petozi.Platform_PAYPAL
	}

	return parsedPlatform, petozi.Platform(platformValue)
}
