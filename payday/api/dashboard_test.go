package api

import (
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"code.justin.tv/commerce/payday/api/apidef"
	"code.justin.tv/commerce/payday/decorators"
	"code.justin.tv/commerce/payday/dynamo"
	dynamo_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo"
	"github.com/stretchr/testify/assert"
	"github.com/zenazn/goji/web"
)

var revenueMock *dynamo_mock.IRevenueDao

func SetupDashboardEndpoint() *web.Mux {
	revenueMock = new(dynamo_mock.IRevenueDao)
	api := &DashboardApi{
		Dao: revenueMock,
	}

	m := web.New()
	m.Get("/dashboard/:tuid/revenue", decorators.DecorateAndAdapt(api.GetBroadcasterRevenue, apidef.AdminGetBroadcasterRevenue))
	return m
}

func MockDaoGetWithinRange(params GetBitsRevenueParams, resp []*dynamo.RevenueRecord, err error) {
	revenueMock.On("GetWithinRange", dynamo.ChannelId(params.UserId), params.StartDate, params.EndDate).Return(resp, err)
}

func TestMissingStartDate(t *testing.T) {
	m := SetupDashboardEndpoint()

	r, _ := http.NewRequest("GET", "/dashboard/123/revenue?end=86000", nil)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 400, w.Code)
}

func TestMissingEndDate(t *testing.T) {
	m := SetupDashboardEndpoint()

	r, _ := http.NewRequest("GET", "/dashboard/123/revenue?start=0", nil)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 400, w.Code)
}

// Backwards compatibility
func TestStillHasGrouping(t *testing.T) {
	m := SetupDashboardEndpoint()
	resp := []*dynamo.RevenueRecord{}

	MockDaoGetWithinRange(GetBitsRevenueParams{
		UserId:    "123",
		StartDate: time.Unix(0, 0),
		EndDate:   time.Unix(86000, 0),
	}, resp, nil)

	r, _ := http.NewRequest("GET", "/dashboard/123/revenue?start=0&end=86000&group_by=day", nil)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 200, w.Code)
}

func TestGetRevenueHappyCase(t *testing.T) {
	m := SetupDashboardEndpoint()
	resp := []*dynamo.RevenueRecord{}

	MockDaoGetWithinRange(GetBitsRevenueParams{
		UserId:    "123",
		StartDate: time.Unix(86000, 0),
		EndDate:   time.Unix(86000, 0),
	}, resp, nil)

	r, _ := http.NewRequest("GET", "/dashboard/123/revenue?start=86000&end=86000", nil)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 200, w.Code)
}
