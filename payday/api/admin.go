package api

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"code.justin.tv/commerce/gogogadget/audit"
	"code.justin.tv/commerce/gogogadget/strings"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/admin/add_bits"
	"code.justin.tv/commerce/payday/admin/remove_bits"
	"code.justin.tv/commerce/payday/clients/cloudwatchlogger"
	httperror "code.justin.tv/commerce/payday/errors/http"
	"code.justin.tv/commerce/payday/models"
	apimodel "code.justin.tv/commerce/payday/models/api"
	"github.com/zenazn/goji/web"
)

type AdminApi struct {
	AdminAddBits    add_bits.Processor                `inject:""`
	AdminRemoveBits remove_bits.Processor             `inject:""`
	AuditLogger     cloudwatchlogger.CloudWatchLogger `inject:""`
}

func (a *AdminApi) AddBits(c web.C, w http.ResponseWriter, r *http.Request) error {
	ctx, cancel := context.WithTimeout(r.Context(), 10*time.Second)
	defer cancel()

	if r.Body == nil {
		log.Error("No HTTP body present in addBits request")
		return httperror.New("Unable to serialize request", http.StatusBadRequest)
	}

	decoder := json.NewDecoder(r.Body)
	var body apimodel.AddBitsRequest

	decodeErr := decoder.Decode(&body)
	if decodeErr != nil {
		log.WithError(decodeErr).Error("Error decoding JSON payload for addBits call")
		return httperror.NewWithCause("Unable to serialize request", http.StatusBadRequest, decodeErr)
	}

	apiName := "AdminAddBits"
	msg, err := audit.CreateAccessLogMsg(ctx, apiName, body)
	if err != nil {
		log.WithError(err).Error(fmt.Sprintf("failed to generate access log msg in %s", apiName))
	} else {
		err = a.AuditLogger.Send(ctx, msg)
		if err != nil {
			log.WithError(err).Warn(fmt.Sprintf("error while sending audit log in %s", apiName))
		}
	}

	// We know better than our admins, admins almost always add bits with Cost Basis of 0 for concessions.
	// Unless we are dealing with a bulk grant campaign request - in that case we use the bit type associated with the campaign
	if strings.Blank(body.BulkGrantCampaignID) {
		body.TypeOfBitsToAdd = string(models.CB_0)
	}

	err = a.AdminAddBits.Process(ctx, body)
	if err != nil {
		return err
	}

	return nil
}

func (a *AdminApi) RemoveBits(c web.C, w http.ResponseWriter, r *http.Request) error {
	ctx, cancel := context.WithTimeout(r.Context(), 10*time.Second)
	defer cancel()

	if r.Body == nil {
		log.Error("No HTTP body present in removeBits request")
		return httperror.New("Unable to serialize request", http.StatusBadRequest)
	}

	decoder := json.NewDecoder(r.Body)
	var body apimodel.RemoveBitsRequest

	decodeErr := decoder.Decode(&body)
	if decodeErr != nil {
		log.WithError(decodeErr).Error("Error decoding JSON payload for removeBits call")
		return httperror.NewWithCause("Unable to serialize request", http.StatusBadRequest, decodeErr)
	}

	apiName := "AdminRemoveBits"
	msg, err := audit.CreateAccessLogMsg(ctx, apiName, body)
	if err != nil {
		log.WithError(err).Error(fmt.Sprintf("failed to generate access log msg in %s", apiName))
	} else {
		err = a.AuditLogger.Send(ctx, msg)
		if err != nil {
			log.WithError(err).Warn(fmt.Sprintf("error while sending audit log in %s", apiName))
		}
	}

	err = a.AdminRemoveBits.Process(ctx, body)
	if err != nil {
		return err
	}

	return nil
}
