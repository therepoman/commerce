package api

import (
	"context"
	"net/http"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/api/constants"
	"code.justin.tv/commerce/payday/api/utils"
	"code.justin.tv/commerce/payday/cartman"
	"code.justin.tv/commerce/payday/dynamo"
	httperror "code.justin.tv/commerce/payday/errors/http"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/user"
	"code.justin.tv/commerce/payday/userstate"
	"github.com/zenazn/goji/web"
)

// APIs regarding bits user

type UserApi struct {
	UserChecker      user.Checker       `inject:""`
	Dao              dynamo.IUserDao    `inject:""`
	CartmanValidator cartman.IValidator `inject:""`
	UserStateGetter  userstate.Getter   `inject:""`
}

const userNotFoundError = "User not found"

func (userApi *UserApi) GetBanned(c web.C, w http.ResponseWriter, r *http.Request) error {
	userId := c.URLParams["tuid"]
	err := utils.ValidateRequestUserId(userId)
	if err != nil {
		return err
	}

	user, err := userApi.getBannedUserFromDynamo(userId)
	if err != nil {
		return err
	}

	if user != nil {
		response := models.NewGetBannedResponse(user)
		utils.WriteResponse(response, w)
	} else {
		return httperror.New(userNotFoundError, http.StatusNotFound)
	}

	return nil
}

func (userApi *UserApi) SetBanned(c web.C, w http.ResponseWriter, r *http.Request) error {
	userId := c.URLParams["tuid"]
	err := utils.ValidateRequestUserId(userId)
	if err != nil {
		return err
	}

	logger := log.WithField("userId", userId)
	request, err := models.ExtractSetBannedRequest(r)
	if err != nil {
		logger.WithError(err).Error("Error decoding JSON request for SetBanned call")
		return httperror.NewWithCause(constants.InvalidJsonError, http.StatusBadRequest, err)
	}

	user, err := userApi.Dao.Get(dynamo.UserId(userId))
	if err != nil {
		logger.WithError(err).Error("Error getting user from dynamo")
		return httperror.NewWithCause(constants.DownstreamDependencyError, http.StatusInternalServerError, err)
	}

	updatedUser := models.SetBannedRequestToDynamoUser(request, userId)
	if user == nil {
		err = userApi.Dao.Put(updatedUser)

		if err != nil {
			logger.WithError(err).Error("Error creating dynamo entry for SetBanned call")
			return httperror.NewWithCause(constants.DownstreamDependencyError, http.StatusInternalServerError, err)
		}
	} else {
		updatedUser.Skipped = user.Skipped
		err = userApi.Dao.Update(updatedUser)

		if err != nil {
			logger.WithError(err).Error("Error updating dynamo entry for SetBanned call")
			return httperror.NewWithCause(constants.DownstreamDependencyError, http.StatusInternalServerError, err)
		}
	}

	return nil
}

func (userApi *UserApi) GetUserInfoWithCartmanAuthentication(c web.C, w http.ResponseWriter, r *http.Request) error {
	userId := c.URLParams["tuid"]
	err := utils.ValidateRequestUserId(userId)
	if err != nil {
		return err
	}

	err = userApi.CartmanValidator.ValidateAuthenticatedUserID(r, userId, false, false)
	if err != nil {
		return err
	}

	return userApi.getUserInfo(w, userId)
}

func (userApi *UserApi) GetUserInfoAdmin(c web.C, w http.ResponseWriter, r *http.Request) error {
	userId := c.URLParams["tuid"]
	err := utils.ValidateRequestUserId(userId)
	if err != nil {
		return err
	}

	return userApi.getUserInfo(w, userId)
}

func (userApi *UserApi) getUserInfo(w http.ResponseWriter, userId string) error {
	eligible, err := userApi.getUserEligibility(userId)
	if err != nil {
		return err
	}

	userState, err := userApi.UserStateGetter.Get(userId)
	if err != nil {
		log.WithField("userId", userId).WithError(err).Error("Error getting user state")
		return httperror.NewWithCause(constants.DownstreamDependencyError, http.StatusInternalServerError, err)
	}

	response := models.NewGetUserInfoResponse(eligible, userState.ToString())

	utils.WriteResponse(response, w)

	return nil
}

func (userApi *UserApi) getBannedUserFromDynamo(userId string) (*dynamo.User, error) {
	user, err := userApi.Dao.Get(dynamo.UserId(userId))
	if err != nil {
		log.WithField("userId", userId).WithError(err).Error("Error getting user from dynamo")
		return nil, httperror.NewWithCause(constants.DownstreamDependencyError, http.StatusInternalServerError, err)
	}
	if user != nil && user.Banned {
		return user, nil
	}
	return nil, nil
}

func (userApi *UserApi) getUserEligibility(userId string) (bool, error) {
	ctx := context.Background()
	eligible, err := userApi.UserChecker.IsEligible(ctx, userId)
	if err != nil {
		log.WithField("userId", userId).WithError(err).Error("Error getting eligibility from eligibility retriever")
		return false, httperror.NewWithCause(constants.DownstreamDependencyError, http.StatusInternalServerError, err)
	}
	return eligible, nil
}
