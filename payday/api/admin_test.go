package api

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"code.justin.tv/commerce/payday/api/apidef"
	"code.justin.tv/commerce/payday/clients/cloudwatchlogger"
	"code.justin.tv/commerce/payday/decorators"
	"code.justin.tv/commerce/payday/errors"
	add_bits_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/admin/add_bits"
	remove_bits_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/admin/remove_bits"
	apimodel "code.justin.tv/commerce/payday/models/api"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/zenazn/goji/web"
)

var ab *add_bits_mock.Processor
var rb *remove_bits_mock.Processor

func SetupAdminEndpoints() *web.Mux {
	ab = new(add_bits_mock.Processor)
	rb = new(remove_bits_mock.Processor)
	cwlogger := cloudwatchlogger.NewCloudWatchLogNoopClient()

	api := &AdminApi{
		AdminAddBits:    ab,
		AdminRemoveBits: rb,
		AuditLogger:     cwlogger,
	}

	m := web.New()
	m.Post("/admin/addBits", decorators.DecorateAndAdapt(api.AddBits, apidef.AdminAddBits))
	m.Post("/admin/removeBits", decorators.DecorateAndAdapt(api.RemoveBits, apidef.AdminRemoveBits))
	return m
}

func TestAddBitsError(t *testing.T) {
	m := SetupAdminEndpoints()
	ab.On("Process", mock.Anything, mock.Anything).Return(errors.New("Test error"))

	req := &apimodel.AddBitsRequest{
		AmountOfBitsToAdd: 1,
		AdminUserId:       "123",
		AdminReason:       "testReason",
		EventId:           "testTransactionId",
		TwitchUserId:      "456",
		TypeOfBitsToAdd:   "CB_0",
	}
	reqJson, _ := json.Marshal(req)
	reqReader := bytes.NewReader(reqJson)

	r, _ := http.NewRequest("POST", "/admin/addBits", reqReader)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.NotEqual(t, 200, w.Code)
}

func TestAddBitsSuccess(t *testing.T) {
	m := SetupAdminEndpoints()
	ab.On("Process", mock.Anything, mock.Anything).Return(nil)

	req := &apimodel.AddBitsRequest{
		AmountOfBitsToAdd: 1,
		AdminUserId:       "123",
		AdminReason:       "testReason",
		EventId:           "testTransactionId",
		TwitchUserId:      "456",
		TypeOfBitsToAdd:   "CB_0",
	}
	reqJson, _ := json.Marshal(req)
	reqReader := bytes.NewReader(reqJson)

	r, _ := http.NewRequest("POST", "/admin/addBits", reqReader)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 200, w.Code)
}

func TestRemoveBitsError(t *testing.T) {
	m := SetupAdminEndpoints()
	rb.On("Process", mock.Anything, mock.Anything).Return(errors.New("Test error"))

	req := &apimodel.RemoveBitsRequest{
		AmountOfBitsToRemove: 1,
		AdminUserId:          "123",
		AdminReason:          "testReason",
		EventId:              "testTransactionId",
		TwitchUserId:         "456",
		EventReasonCode:      "",
	}
	reqJson, _ := json.Marshal(req)
	reqReader := bytes.NewReader(reqJson)

	r, _ := http.NewRequest("POST", "/admin/removeBits", reqReader)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.NotEqual(t, 200, w.Code)
}

func TestRemoveBitsSuccess(t *testing.T) {
	m := SetupAdminEndpoints()
	rb.On("Process", mock.Anything, mock.Anything).Return(nil)

	req := &apimodel.RemoveBitsRequest{
		AmountOfBitsToRemove: 1,
		AdminUserId:          "123",
		AdminReason:          "testReason",
		EventId:              "testTransactionId",
		TwitchUserId:         "456",
		EventReasonCode:      "",
	}
	reqJson, _ := json.Marshal(req)
	reqReader := bytes.NewReader(reqJson)

	r, _ := http.NewRequest("POST", "/admin/removeBits", reqReader)
	w := httptest.NewRecorder()
	m.ServeHTTP(w, r)

	assert.Equal(t, 200, w.Code)
}
