package api

import (
	"context"
	"encoding/json"
	"net/http"
	"strings"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/adminjob"
	"code.justin.tv/commerce/payday/adminjob/handler"
	"code.justin.tv/commerce/payday/adminjob/handler/bulkentitle"
	"code.justin.tv/commerce/payday/api/constants"
	"code.justin.tv/commerce/payday/api/utils"
	"code.justin.tv/commerce/payday/dynamo"
	httperror "code.justin.tv/commerce/payday/errors/http"
	stringUtils "code.justin.tv/commerce/payday/utils/strings"
	"github.com/zenazn/goji/web"
)

type AdminJobApi struct {
	AdminJobDao            dynamo.IAdminJobDao    `inject:""`
	AdminJobManager        adminjob.IManager      `inject:""`
	AdminJobHandlerFactory handler.HandlerFactory `inject:""`
}

func (a *AdminJobApi) GetAdminJobsByCreator(c web.C, w http.ResponseWriter, r *http.Request) error {
	ctx, cancel := utils.ContextWithDefaultTimeout(r.Context())
	defer cancel()

	creator := r.URL.Query().Get("creator")
	if stringUtils.Blank(creator) {
		return httperror.New("Creator required", http.StatusBadRequest)
	}

	dynamoJobs, err := a.AdminJobDao.GetRecentByCreator(creator, 10)
	if err != nil {
		log.WithError(err).WithField("creator", creator).Error("Error getting recent admin jobs by creator")
		return httperror.New(constants.DownstreamDependencyError, http.StatusInternalServerError)
	}

	response := &adminjob.GetAdminJobsResponse{
		AdminJobs: adminjob.FromDynamo(dynamoJobs),
	}

	for _, aj := range response.AdminJobs {
		if aj == nil {
			continue
		}

		id := adminjob.Identifier{
			Creator: creator,
			JobID:   aj.JobID,
		}
		files, err := a.AdminJobManager.ListOutputFilesFromS3(ctx, id)
		if err != nil {
			log.WithError(err).WithField("identifier", id.String()).Error("Error listing job output files from S3")
			return httperror.New(constants.DownstreamDependencyError, http.StatusInternalServerError)
		}
		aj.NumOutputFiles = len(files)
	}

	utils.WriteResponse(response, w)
	return nil
}

func (a *AdminJobApi) GetAdminJobInput(c web.C, w http.ResponseWriter, r *http.Request) error {
	ctx, cancel := utils.ContextWithDefaultTimeout(r.Context())
	defer cancel()

	creator := r.URL.Query().Get("creator")
	if stringUtils.Blank(creator) {
		return httperror.New("Creator required", http.StatusBadRequest)
	}

	jobID := r.URL.Query().Get("job_id")
	if stringUtils.Blank(jobID) {
		return httperror.New("JobId required", http.StatusBadRequest)
	}

	id := adminjob.Identifier{
		Creator: creator,
		JobID:   jobID,
	}

	rawInput, err := a.AdminJobManager.GetInputFromS3(ctx, id)
	if err != nil {
		log.WithError(err).WithField("identifier", id.String()).WithError(err).Error("error getting input from s3")
		return httperror.New(constants.DownstreamDependencyError, http.StatusInternalServerError)
	}

	_, err = w.Write([]byte(rawInput))
	if err != nil {
		log.WithError(err).WithField("identifier", id.String()).WithError(err).Error("error writing s3 input to response")
		return httperror.New(constants.DownstreamDependencyError, http.StatusInternalServerError)
	}

	return nil
}

func (a *AdminJobApi) GetAdminJobOptions(c web.C, w http.ResponseWriter, r *http.Request) error {
	ctx, cancel := utils.ContextWithDefaultTimeout(r.Context())
	defer cancel()

	creator := r.URL.Query().Get("creator")
	if stringUtils.Blank(creator) {
		return httperror.New("Creator required", http.StatusBadRequest)
	}

	jobID := r.URL.Query().Get("job_id")
	if stringUtils.Blank(jobID) {
		return httperror.New("JobId required", http.StatusBadRequest)
	}

	id := adminjob.Identifier{
		Creator: creator,
		JobID:   jobID,
	}

	rawOptions, err := a.AdminJobManager.GetOptionsFromS3(ctx, id)
	if err != nil {
		log.WithError(err).WithField("identifier", id.String()).WithError(err).Error("error getting options from s3")
		return httperror.New(constants.DownstreamDependencyError, http.StatusInternalServerError)
	}

	_, err = w.Write([]byte(rawOptions))
	if err != nil {
		log.WithError(err).WithField("identifier", id.String()).WithError(err).Error("error writing s3 options to response")
		return httperror.New(constants.DownstreamDependencyError, http.StatusInternalServerError)
	}

	return nil
}

func (a *AdminJobApi) GetAdminJobOutput(c web.C, w http.ResponseWriter, r *http.Request) error {
	ctx, cancel := utils.ContextWithDefaultTimeout(r.Context())
	defer cancel()

	creator := r.URL.Query().Get("creator")
	if stringUtils.Blank(creator) {
		return httperror.New("Creator required", http.StatusBadRequest)
	}

	jobID := r.URL.Query().Get("job_id")
	if stringUtils.Blank(jobID) {
		return httperror.New("JobId required", http.StatusBadRequest)
	}

	id := adminjob.Identifier{
		Creator: creator,
		JobID:   jobID,
	}

	err := a.AdminJobManager.WriteOutputZip(ctx, id, w)
	if err != nil {
		log.WithField("identifier", id.String()).WithError(err).Error("error writing admin job output to zip")
		return httperror.NewWithCause(constants.DownstreamDependencyError, http.StatusInternalServerError, err)
	}

	return nil
}

func (a *AdminJobApi) startAdminJob(ctx context.Context, w http.ResponseWriter, req adminjob.StartAdminJobRequest) error {
	aj, err := a.AdminJobDao.Get(req.Creator, req.JobID)
	if err != nil {
		log.WithError(err).Error("Error getting admin job from dynamo")
		return httperror.NewWithCause(constants.DownstreamDependencyError, http.StatusInternalServerError, err)
	}

	if aj != nil {
		return nil
	}

	id := req.ToIdentifier()
	handler, err := a.AdminJobHandlerFactory.GetTaskHandler(id, adminjob.Type(req.Type), adminjob.RawOptions(req.Options))
	if err != nil {
		log.WithError(err).Error("Could not get task handler")
		return httperror.NewWithCause("Invalid input for admin job", http.StatusBadRequest, err)
	}

	err = handler.ValidateInput(ctx, adminjob.RawInput(req.Input))
	if err != nil {
		log.WithError(err).Error("Received bad input to StartAdminJob API")
		return httperror.NewWithCause("Invalid input for admin job", http.StatusBadRequest, err)
	}

	err = a.AdminJobManager.SaveInputToS3(ctx, req)
	if err != nil {
		log.WithError(err).Error("Error saving admin job input to s3")
		return httperror.NewWithCause(constants.DownstreamDependencyError, http.StatusInternalServerError, err)
	}

	err = a.AdminJobManager.SaveOptionsToS3(ctx, req)
	if err != nil {
		log.WithError(err).Error("Error saving admin job options to s3")
		return httperror.NewWithCause(constants.DownstreamDependencyError, http.StatusInternalServerError, err)
	}

	newAJ := &dynamo.AdminJob{
		Creator:         req.Creator,
		JobID:           req.JobID,
		StartTime:       time.Now(),
		Type:            req.Type,
		Progress:        0,
		InputS3Location: adminjob.GetInputS3Key(id),
	}
	err = a.AdminJobDao.Update(newAJ)
	if err != nil {
		log.WithError(err).Error("Error saving admin job to dynamo")
		return httperror.NewWithCause(constants.DownstreamDependencyError, http.StatusInternalServerError, err)
	}

	err = a.AdminJobManager.SendSNSMessage(ctx, req)
	if err != nil {
		log.WithError(err).Error("Error sending admin job SNS")
		return httperror.NewWithCause(constants.DownstreamDependencyError, http.StatusInternalServerError, err)
	}

	return nil
}

func (a *AdminJobApi) StartAdminJob(c web.C, w http.ResponseWriter, r *http.Request) error {
	ctx, cancel := utils.ContextWithDefaultTimeout(r.Context())
	defer cancel()

	decoder := json.NewDecoder(r.Body)

	var req adminjob.StartAdminJobRequest
	err := decoder.Decode(&req)
	if err != nil {
		log.WithError(err).Error("Error decoding JSON payload for start admin job call")
		return httperror.NewWithCause("Unable to deserialize request", http.StatusBadRequest, err)
	}

	return a.startAdminJob(ctx, w, req)
}

func (a *AdminJobApi) StartBulkEntitleJob(c web.C, w http.ResponseWriter, r *http.Request) error {
	ctx, cancel := utils.ContextWithDefaultTimeout(r.Context())
	defer cancel()

	decoder := json.NewDecoder(r.Body)

	var bulkReq adminjob.StartBulkEntitleJobRequest
	err := decoder.Decode(&bulkReq)
	if err != nil {
		log.WithError(err).Error("Error decoding JSON payload for start bulk entitle admin job call")
		return httperror.NewWithCause("Unable to deserialize request", http.StatusBadRequest, err)
	}

	if len(bulkReq.RecipientLogins) > 0 && len(bulkReq.RecipientIDs) > 0 {
		return httperror.NewWithCause("Request cannot contain both IDs and logins", http.StatusBadRequest, err)
	}

	var userLookupMode bulkentitle.UserLookupMode
	var recipientInput []string
	if len(bulkReq.RecipientIDs) > 0 {
		userLookupMode = bulkentitle.UserLookupMode_ByID
		recipientInput = bulkReq.RecipientIDs
	} else if len(bulkReq.RecipientLogins) > 0 {
		userLookupMode = bulkentitle.UserLookupMode_ByLogin
		recipientInput = bulkReq.RecipientLogins
	} else {
		return httperror.NewWithCause("Request must contain at least one ID or one login", http.StatusBadRequest, err)
	}

	if !stringUtils.Blank(bulkReq.BulkGrantCampaignID) {
		if bulkReq.BitsAmount > 0 {
			return httperror.NewWithCause("BulkGrantCampaign request cannot have bits amount set", http.StatusBadRequest, err)
		}
		if !stringUtils.Blank(bulkReq.Origin) {
			return httperror.NewWithCause("BulkGrantCampaign request cannot have origin set", http.StatusBadRequest, err)
		}
		if !stringUtils.Blank(bulkReq.AdminReason) {
			return httperror.NewWithCause("BulkGrantCampaign request cannot have admin reason set", http.StatusBadRequest, err)
		}
		if !stringUtils.Blank(bulkReq.TypeOfBitsToAdd) {
			return httperror.NewWithCause("BulkGrantCampaign request cannot have type of bits set", http.StatusBadRequest, err)
		}
	}

	options := bulkentitle.Options{
		BitsAmount:          bulkReq.BitsAmount,
		AdminReason:         bulkReq.AdminReason,
		UserLookupMode:      userLookupMode,
		TypeOfBitsToAdd:     bulkReq.TypeOfBitsToAdd,
		NotifyUser:          bulkReq.NotifyUser,
		Origin:              bulkReq.Origin,
		BulkGrantCampaignID: bulkReq.BulkGrantCampaignID,
	}

	optionsJson, err := json.Marshal(&options)
	if err != nil {
		log.WithError(err).Error("Error encoding options for start bulk entitle admin job call")
		return httperror.NewWithCause("Unable to encode options from request", http.StatusBadRequest, err)
	}

	req := adminjob.StartAdminJobRequest{
		Creator: bulkReq.Creator,
		JobID:   bulkReq.JobID,
		Type:    string(adminjob.BulkEntitle),
		Options: string(optionsJson),
		Input:   strings.Join(recipientInput, "\n"),
	}

	return a.startAdminJob(ctx, w, req)
}
