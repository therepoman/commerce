package api

// APIs common to all bits users (both users and channels)

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"strconv"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/api/balance"
	"code.justin.tv/commerce/payday/api/clienttracking"
	"code.justin.tv/commerce/payday/api/utils"
	bitsbadges "code.justin.tv/commerce/payday/badgetiers/products/badges"
	"code.justin.tv/commerce/payday/cartman"
	httperror "code.justin.tv/commerce/payday/errors/http"
	"code.justin.tv/commerce/payday/hystrix"
	cmds "code.justin.tv/commerce/payday/hystrix"
	"code.justin.tv/commerce/payday/models"
	apimodel "code.justin.tv/commerce/payday/models/api"
	"code.justin.tv/commerce/payday/throttle"
	"github.com/zenazn/goji/web"
)

type BalanceApi struct {
	CartmanValidator        cartman.IValidator     `inject:""`
	BalanceGetter           balance.Getter         `inject:""`
	Throttler               throttle.IThrottler    `inject:""`
	AvailableBadgesLogician bitsbadges.Logician    `inject:""`
	ClientTracking          clienttracking.Tracker `inject:""`
}

// return the current balance of a user
func (b *BalanceApi) GetBalanceWithCartmanAuthentication(c web.C, w http.ResponseWriter, r *http.Request) error {
	ctx, cancel := context.WithTimeout(r.Context(), 8*time.Second)
	defer cancel()
	user := c.URLParams["tuid"]
	channel := r.URL.Query().Get("channel")

	err := b.CartmanValidator.ValidateAuthenticatedUserID(r, user, false, false)
	if err != nil {
		return err
	}

	return b.getBalance(ctx, user, channel, w)
}

// returns the current balance of a user bypassing cartman validation
func (b *BalanceApi) GetBalanceWithoutCartman(c web.C, w http.ResponseWriter, r *http.Request) error {
	ctx, cancel := context.WithTimeout(r.Context(), 8*time.Second)
	defer cancel()
	user := c.URLParams["tuid"]
	channel := r.URL.Query().Get("channel")

	return b.getBalance(ctx, user, channel, w)
}

func (b *BalanceApi) getBalance(ctx context.Context, user string, channel string, w http.ResponseWriter) error {
	go b.ClientTracking.Track(ctx, clienttracking.GetBalance)

	pachinkoChan := make(chan *models.Balance, 1)
	pachinkoErrs := make(chan error, 1)

	go func() {
		_, pachinkoResp, err := b.BalanceGetter.Get(ctx, user, channel, false)
		if err != nil {
			pachinkoErrs <- err
			return
		}

		if pachinkoResp == nil {
			pachinkoErrs <- errors.New("pachinko response is nil")
			return
		}

		pachinkoChan <- pachinkoResp
	}()

	cbChan := make(chan int64, 1)
	chatBadgeErrs := hystrix.Go(cmds.GetHighestBadgeCommand, func() error {
		if !isValidChannelId(channel) {
			cbChan <- int64(0)
			return nil
		}

		// GetUserBitsBadgesAvailableForChannel returns all available bits badges in ascending sorted order
		// (i.e. the last item in the array is the badge with the highest threshold / version)
		availableBadges, err := b.AvailableBadgesLogician.GetUserBitsBadgesAvailableForChannel(ctx, user, channel)
		if err != nil {
			return err
		}

		if len(availableBadges) < 1 {
			cbChan <- int64(0)
			return nil
		}

		highestBitsBadge, err := strconv.ParseInt(availableBadges[len(availableBadges)-1].BadgeSetVersion, 10, 64)
		if err != nil {
			log.Error(fmt.Sprintf("Badge version (%s) could not be converted to an int64. ", availableBadges[len(availableBadges)-1].BadgeSetVersion), err)
		}
		cbChan <- highestBitsBadge

		return nil
	}, nil)

	output := apimodel.GetBalanceResponse{}

	select {
	case pachinkoResponse := <-pachinkoChan:
		output.Balance = int32(pachinkoResponse.TotalBits)
		output.Bundles = filterPurchasables(pachinkoResponse.CanPurchase, pachinkoResponse.ProductInformation)
		output.InventoryLimit = pachinkoResponse.MaximumBitsInventoryLimit
		output.ChannelTotal = pachinkoResponse.TotalBitsWithBroadcaster
		output.MostRecentPurchaseWebPlatform = string(pachinkoResponse.MostRecentPurchaseWebPlatform)
	case err := <-pachinkoErrs:
		log.WithError(err).Error("Error calling GetBitsBalance")
		return httperror.NewWithCause("Encountered unknown error from downstream dependency", http.StatusInternalServerError, err)
	case <-ctx.Done():
		if ctx.Err() == context.DeadlineExceeded {
			log.Error("Timed out waiting on Pachinko GetBitsBalance response")
		} else if ctx.Err() == context.Canceled {
			log.Warn("Pachinko GetBitsBalance request cancelled")
		} else {
			log.Error("Pachinko GetBitsBalance context marked as done without a response")
		}
		return httperror.New("Timed out waiting on response", http.StatusInternalServerError)
	}

	select {
	case highestBadge := <-cbChan:
		output.HighestEntitledBadge = highestBadge
	case err := <-chatBadgeErrs:
		log.WithFields(log.Fields{
			"user":    user,
			"channel": channel,
		}).WithError(err).Error("Error obtaining the Highest Entitled Badge")
		output.HighestEntitledBadge = 0
	case <-ctx.Done():
		if ctx.Err() == context.DeadlineExceeded {
			log.Error("Timed out waiting on Highest Entitled Badge response")
		} else if ctx.Err() == context.Canceled {
			log.Warn("Highest Entitled Badge request cancelled")
		} else {
			log.Error("Highest Entitled Badge context marked as done without a response")
		}

		return httperror.New("Timed out waiting on response", http.StatusInternalServerError)
	}

	utils.WriteResponse(output, w)
	return nil
}

func filterPurchasables(purchasables map[string]bool, productInfo map[string]models.BitsProductInformation) map[string]bool {
	filtered := map[string]bool{}
	for key, value := range purchasables {
		if models.ArePlatformsWeb(productInfo[key].Platforms) {
			filtered[key] = value
		}
	}

	return filtered
}

func isValidChannelId(channelId string) bool {
	return channelId != "" && utils.IsNumericUserId(channelId)
}
