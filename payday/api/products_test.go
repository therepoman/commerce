package api

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"code.justin.tv/commerce/payday/api/apidef"
	"code.justin.tv/commerce/payday/api/clienttracking"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/decorators"
	clienttracking_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/api/clienttracking"
	products_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/products"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/models/api"
	petozi "code.justin.tv/commerce/petozi/rpc"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
	"github.com/zenazn/goji/web"
)

func TestProductsApi_GetProducts(t *testing.T) {
	Convey("Given a product API", t, func() {
		validator := new(products_mock.Validator)
		getter := new(products_mock.Getter)

		clientTracking := new(clienttracking_mock.Tracker)
		clientTracking.On("Track", mock.Anything, clienttracking.GetProducts).Return()

		a := ProductsApi{
			Getter:         getter,
			Validator:      validator,
			Config:         &config.Configuration{},
			ClientTracking: clientTracking,
		}

		tuid := "123123123"
		locale := "en-US"
		cor := "US"
		platform := models.AMAZON

		getter.On("GetByIDs", mock.Anything, petozi.Platform_AMAZON, []string{"walrus"}).Return(map[string]*petozi.BitsProduct{
			"walrus": {
				Platform:  petozi.Platform_AMAZON,
				Id:        "walrus",
				PricingId: "walrusPrice",
			},
		}, nil)

		Convey("When the validator returns an errors", func() {
			validator.On("Validate", mock.Anything, tuid, locale, cor, platform).Return(errors.New("WALRUS STRIKE"), false)

			Convey("Then we should return an error", func() {
				r, _ := http.NewRequest("GET", fmt.Sprintf("/products?user_id=%v&locale=%v&country=%v&platform=%v", tuid, locale, cor, string(platform)), nil)
				w := httptest.NewRecorder()

				m := web.New()
				m.Get("/products", decorators.DecorateAndAdapt(a.GetProducts, apidef.GetProducts))
				m.ServeHTTP(w, r)
				So(w.Code, ShouldEqual, 500)
			})
		})

		Convey("When the validator returns that it should return empty", func() {
			validator.On("Validate", mock.Anything, tuid, locale, cor, platform).Return(nil, true)

			Convey("Then we should return an empty array", func() {
				r, _ := http.NewRequest("GET", fmt.Sprintf("/products?user_id=%v&locale=%v&country=%v&platform=%v", tuid, locale, cor, string(platform)), nil)
				w := httptest.NewRecorder()

				m := web.New()
				m.Get("/products", decorators.DecorateAndAdapt(a.GetProducts, apidef.GetProducts))
				m.ServeHTTP(w, r)
				So(w.Code, ShouldEqual, 200)

				var response []api.Product
				_ = json.Unmarshal(w.Body.Bytes(), &response)
				So(response, ShouldHaveLength, 0)
			})
		})

		Convey("When the validator says we have a valid non empty response to provide", func() {
			validator.On("Validate", mock.Anything, tuid, locale, cor, platform).Return(nil, false)

			Convey("When the getter returns an error", func() {
				getter.On("Get", mock.Anything, tuid, locale, platform).Return(nil, errors.New("WALRUS STRIKE"))

				Convey("Then we should return an error", func() {
					r, _ := http.NewRequest("GET", fmt.Sprintf("/products?user_id=%v&locale=%v&country=%v&platform=%v", tuid, locale, cor, string(platform)), nil)
					w := httptest.NewRecorder()

					m := web.New()
					m.Get("/products", decorators.DecorateAndAdapt(a.GetProducts, apidef.GetProducts))
					m.ServeHTTP(w, r)
					So(w.Code, ShouldEqual, 500)
				})
			})

			Convey("When the getter returns a list of products", func() {
				product := api.Product{
					Id: "walrus",
				}

				getter.On("Get", mock.Anything, tuid, locale, platform).Return([]api.Product{product}, nil)

				Convey("Then we should return the products", func() {
					r, _ := http.NewRequest("GET", fmt.Sprintf("/products?user_id=%v&locale=%v&country=%v&platform=%v", tuid, locale, cor, string(platform)), nil)
					w := httptest.NewRecorder()

					m := web.New()
					m.Get("/products", decorators.DecorateAndAdapt(a.GetProducts, apidef.GetProducts))
					m.ServeHTTP(w, r)
					So(w.Code, ShouldEqual, 200)

					var response []api.Product
					_ = json.Unmarshal(w.Body.Bytes(), &response)
					So(response, ShouldHaveLength, 1)

					productRes := response[0]
					So(productRes.Id, ShouldEqual, product.Id)
				})
			})
		})
	})
}
