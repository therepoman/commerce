package api

import (
	"net/http"
	"strconv"
	"time"

	"github.com/pkg/profile"
	"github.com/zenazn/goji/web"
)

func Profiler(c web.C, w http.ResponseWriter, r *http.Request) {
	durationString, ok := c.URLParams["duration"]

	if !ok {
		durationString = "10"
	}

	duration, err := strconv.Atoi(durationString)

	if err != nil {
		duration = 10
	}

	profiler := profile.Start(profile.MemProfile, profile.CPUProfile, profile.ProfilePath("/tmp"))

	fn := func() {

		time.Sleep(time.Minute * time.Duration(duration))
		profiler.Stop()
	}

	go fn()
}
