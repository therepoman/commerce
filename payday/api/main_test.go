package api

import (
	"os"
	"testing"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/cache/channel"
	"code.justin.tv/commerce/payday/clients/redis"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/test"
)

var dynamoContext *test.DynamoTestContext
var userDao dynamo.IUserDao
var channelDao dynamo.IChannelDao
var channelManager channel.ChannelManager
var tierDao dynamo.IBadgeTierDao

func TestMain(m *testing.M) {
	var err error
	dynamoContext, err = test.NewDynamoTestContext("api")
	if err != nil {
		log.WithError(err).Error("Failed creating dynamo context during test setup")
		os.Exit(1)
	}

	if !dynamoContext.SkipDynamoTests {
		userDao, _ = dynamoContext.CreateDefaultUserDao("test-user-apis")
		_ = userDao.SetupTable()

		channelDao, _ = dynamoContext.CreateDefaultChannelDao("test-recipient-apis")
		_ = channelDao.SetupTable()

		tierDao, _ = dynamoContext.CreateDefaultBadgeTierDao("test-badge-tier-apis")
		_ = tierDao.SetupTable()

		redisClient, server := redis.TestRedisClient()
		defer server.Close()
		channelManager = channel.NewChannelManagerWithClients(redisClient, channelDao)
	}

	exitCode := m.Run()

	if !dynamoContext.SkipDynamoTests {
		err = dynamoContext.Cleanup()
		if err != nil {
			log.WithError(err).Error("Failed during test cleanup,")
			os.Exit(1)
		}
	}

	os.Exit(exitCode)
}
