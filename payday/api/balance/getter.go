package balance

import (
	"context"

	"code.justin.tv/commerce/gogogadget/strings"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/balance"
	"code.justin.tv/commerce/payday/leaderboard"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/products/availability"
	platform_getter "code.justin.tv/commerce/payday/products/purchase_by_most_recent_super_platform/getter"
	"code.justin.tv/commerce/payday/utils/new_sku_experiment_gating"
	petozi "code.justin.tv/commerce/petozi/rpc"
)

type PachinkoResponse struct {
	Balance int
	Error   error
}

type Getter interface {
	Get(ctx context.Context, userID, channelID string, skipCache bool) (int32, *models.Balance, error)
}

func NewGetter() Getter {
	return &getter{}
}

type getter struct {
	BalanceGetter           balance.Getter                          `inject:""`
	BitsToBroadcasterGetter leaderboard.BitsToBroadcasterGetter     `inject:""`
	PetoziClient            petozi.Petozi                           `inject:""`
	EligibilityChecker      availability.ProductAvailabilityChecker `inject:""`
	PlatformGetter          platform_getter.Getter                  `inject:""`
}

// GOAL is to move this out somewhere else
func (p *getter) Get(ctx context.Context, userID, channelID string, skipCache bool) (int32, *models.Balance, error) {
	errChan := make(chan error)

	balanceChan := make(chan int32, 1)
	go func() {
		userBalance, err := p.BalanceGetter.GetBalance(ctx, userID, skipCache)
		if err != nil {
			log.WithFields(log.Fields{"userID": userID, "skipCache": skipCache}).WithError(err).Error("Error getting bits balance")
			errChan <- err
		} else {
			balanceChan <- userBalance
		}
	}()

	bitsToBroadcasterChan := make(chan int32, 1)
	go func() {
		bitsToBroadcaster, err := p.getBitsToBroadcaster(ctx, userID, channelID)
		if err != nil {
			log.WithFields(log.Fields{"userID": userID, "channelID": channelID}).WithError(err).Error("Error getting bits to broadcaster amount")
			errChan <- err
		} else {
			bitsToBroadcasterChan <- bitsToBroadcaster
		}
	}()

	productsMapChan := make(chan map[string]models.BitsProductInformation, 1)
	go func() {
		dasProducts, err := p.getBitsProductInfo(ctx)
		if err != nil {
			log.WithError(err).Error("Error getting bits product info")
			errChan <- err
		} else {
			productsMapChan <- dasProducts
		}
	}()

	platformChan := make(chan models.Platform, 1)
	go func() {
		platformChan <- p.PlatformGetter.Get(ctx, userID)
	}()

	purchasabilityMapChan := make(chan map[string]bool, 1)
	go func() {
		purchasabilityMap := p.getCanPurchaseMap(ctx, userID)

		// there are two First time purchase SKUs, we need to delete the old one (if it's there) for the time being
		// until we can remove or disable the old entry from the catalog.
		delete(purchasabilityMap, new_sku_experiment_gating.OldFirstTimePurchaseSKU)

		purchasabilityMapChan <- purchasabilityMap
	}()

	response := models.Balance{
		MaximumBitsInventoryLimit: 800000, // this I pulled as is from old FABS configs.
	}

	//There is probably a better way to do this.
	select {
	case err := <-errChan:
		return 0, nil, err
	case userBalance := <-balanceChan:
		response.TotalBits = models.Bits(userBalance)
	case <-ctx.Done():
		return 0, nil, ctx.Err()
	}

	select {
	case err := <-errChan:
		return 0, nil, err
	case bitsToBroadcaster := <-bitsToBroadcasterChan:
		response.TotalBitsWithBroadcaster = &bitsToBroadcaster
	case <-ctx.Done():
		return 0, nil, ctx.Err()
	}

	select {
	case err := <-errChan:
		return 0, nil, err
	case productsMaps := <-productsMapChan:
		response.ProductInformation = productsMaps
	case <-ctx.Done():
		return 0, nil, ctx.Err()
	}

	select {
	case err := <-errChan:
		return 0, nil, err
	case platform := <-platformChan:
		response.MostRecentPurchaseWebPlatform = platform
	case <-ctx.Done():
		return 0, nil, ctx.Err()
	}

	select {
	case err := <-errChan:
		return 0, nil, err
	case purchasabilityMap := <-purchasabilityMapChan:
		response.CanPurchase = purchasabilityMap
	case <-ctx.Done():
		return 0, nil, ctx.Err()
	}

	return int32(response.TotalBits), &response, nil
}

func (p *getter) getBitsToBroadcaster(ctx context.Context, userID, channelID string) (int32, error) {
	if strings.Blank(channelID) {
		return 0, nil
	}

	bitsToBroadcaster, err := p.BitsToBroadcasterGetter.GetBitsToBroadcaster(ctx, userID, channelID)
	if err != nil {
		return 0, err
	}
	return int32(bitsToBroadcaster), nil
}

func (p *getter) getBitsProductInfo(ctx context.Context) (map[string]models.BitsProductInformation, error) {
	results := map[string]models.BitsProductInformation{}

	petoziProducts, err := p.PetoziClient.GetBitsProducts(ctx, &petozi.GetBitsProductsReq{
		UseInMemoryCache: true,
	})
	if err != nil {
		return nil, err
	}

	for _, product := range petoziProducts.BitsProducts {
		if result, ok := results[product.Id]; ok {
			result.Platforms = append(result.Platforms, models.ConvertPetoziPlatform(product.Platform))
			results[product.Id] = result
		} else {
			results[product.Id] = convertProduct(product)
		}
	}

	return results, nil
}

func (p *getter) getCanPurchaseMap(ctx context.Context, userID string) map[string]bool {
	purchasableQuantities := p.EligibilityChecker.GetPurchasableQuantities(ctx, userID)

	results := map[string]bool{}

	for productID, quantity := range purchasableQuantities {
		results[productID] = quantity > 0
	}
	return results
}

func convertProduct(product *petozi.BitsProduct) models.BitsProductInformation {
	var promoType *string
	var promoId *string

	if product.Promo != nil {
		promoTypeValue := product.Promo.Type.String()
		promoType = &promoTypeValue
		promoId = &product.Promo.Id
	}

	return models.BitsProductInformation{
		Platforms:    []models.Platform{models.ConvertPetoziPlatform(product.Platform)},
		NumberOfBits: int32(product.Quantity),
		Promotional:  product.Promo != nil,
		PromoType:    promoType,
		PromoId:      promoId,
	}
}
