package validation

import (
	zuma "code.justin.tv/chat/zuma/app/api"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/message/tokenizer"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/partner"
)

type ValidationResponse struct {
	Tokenizer      tokenizer.ITokenizer
	PartnerType    partner.PartnerType
	AllTokenValues []int64
	EmoteTotals    map[string]models.BitEmoteTotal
	Channel        *dynamo.Channel
	AutoMod        zuma.AutoModResponse
	ErrorType      models.ErrorType
}
