package validation

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"

	"net/url"
)

const (
	testPrefix           = "Kappa"
	testChannelID        = "12312345"
	testBackground       = "light"
	testScale            = "1"
	testState            = "static"
	testBitsAmount       = "100"
	testBitsAmountNumber = int64(100)
)

func TestCreateActionAssetRequest(t *testing.T) {
	Convey("Requests can be validated", t, func() {
		valueMap := map[string]string{
			"prefix":     testPrefix,
			"background": testBackground,
			"scale":      testScale,
			"state":      testState,
			"bits":       testBitsAmount,
		}
		Convey("when the request is valid when prefix is set", func() {
			request, err := CreateActionAssetRequest(createValues(valueMap))
			So(err, ShouldBeNil)
			So(request, ShouldNotBeNil)
			So(request.Prefix, ShouldEqual, testPrefix)
			So(request.ChannelID, ShouldBeBlank)
			So(request.Background, ShouldEqual, testBackground)
			So(request.Scale, ShouldEqual, testScale)
			So(request.State, ShouldEqual, testState)
			So(request.Bits, ShouldEqual, testBitsAmountNumber)
		})
		Convey("when the request is valid when channel ID is set", func() {
			delete(valueMap, "prefix")
			valueMap["channel_id"] = testChannelID
			request, err := CreateActionAssetRequest(createValues(valueMap))
			So(err, ShouldBeNil)
			So(request, ShouldNotBeNil)
			So(request.Prefix, ShouldBeBlank)
			So(request.ChannelID, ShouldEqual, testChannelID)
			So(request.Background, ShouldEqual, testBackground)
			So(request.Scale, ShouldEqual, testScale)
			So(request.State, ShouldEqual, testState)
			So(request.Bits, ShouldEqual, testBitsAmountNumber)
		})
		Convey("when there is no channel ID or prefix", func() {
			delete(valueMap, "prefix")
			_, err := CreateActionAssetRequest(createValues(valueMap))
			So(err, ShouldNotBeNil)
		})
		Convey("when there are both channel ID and prefix set", func() {
			valueMap["channel_id"] = testChannelID
			_, err := CreateActionAssetRequest(createValues(valueMap))
			So(err, ShouldNotBeNil)
		})
		Convey("when background is empty", func() {
			delete(valueMap, "background")
			_, err := CreateActionAssetRequest(createValues(valueMap))
			So(err, ShouldNotBeNil)
		})
		Convey("when background is invalid", func() {
			valueMap["background"] = "notabackground"
			_, err := CreateActionAssetRequest(createValues(valueMap))
			So(err, ShouldNotBeNil)
		})
		Convey("when state is empty", func() {
			delete(valueMap, "state")
			_, err := CreateActionAssetRequest(createValues(valueMap))
			So(err, ShouldNotBeNil)
		})
		Convey("when state is invalid", func() {
			valueMap["state"] = "notastate"
			_, err := CreateActionAssetRequest(createValues(valueMap))
			So(err, ShouldNotBeNil)
		})
		Convey("when scale is empty", func() {
			delete(valueMap, "scale")
			_, err := CreateActionAssetRequest(createValues(valueMap))
			So(err, ShouldNotBeNil)
		})
		Convey("when scale is invalid", func() {
			valueMap["scale"] = "notascale"
			_, err := CreateActionAssetRequest(createValues(valueMap))
			So(err, ShouldNotBeNil)
		})
		Convey("when bits is empty", func() {
			delete(valueMap, "bits")
			_, err := CreateActionAssetRequest(createValues(valueMap))
			So(err, ShouldNotBeNil)
		})
		Convey("when bits is invalid", func() {
			valueMap["bits"] = "notanumber"
			_, err := CreateActionAssetRequest(createValues(valueMap))
			So(err, ShouldNotBeNil)
		})
	})
}

func createValues(valueMap map[string]string) url.Values {
	vals := url.Values{}
	for key, val := range valueMap {
		vals.Add(key, val)
	}
	return vals
}
