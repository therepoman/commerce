package validation

import (
	"net/http"
	"net/url"
	"strconv"
	"strings"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/actions/constants"
	apiconstants "code.justin.tv/commerce/payday/api/constants"
	"code.justin.tv/commerce/payday/errors"
	httperror "code.justin.tv/commerce/payday/errors/http"
	"code.justin.tv/commerce/payday/models/api"
)

func CreateActionAssetRequest(query url.Values) (api.GetActionAssetOptions, error) {
	channelId := query.Get("channel_id")
	prefix := query.Get("prefix")
	if prefix == "" && channelId == "" {
		log.WithField("empty_key", "prefix,channelId").Warn("prefix and channelId cannot both be empty")
		return api.GetActionAssetOptions{}, httperror.NewWithCause(apiconstants.InvalidRequestParam, http.StatusBadRequest, errors.New("prefix and channelId cannot both be empty."))
	}
	if prefix != "" && channelId != "" {
		log.Warn("Both prefix and channelId cannot be set")
		return api.GetActionAssetOptions{}, httperror.NewWithCause(apiconstants.InvalidRequestParam, http.StatusBadRequest, errors.New("Both prefix and channelId cannot be set."))
	}

	background := query.Get("background")
	if background == "" {
		log.WithField("empty_key", "background").Warn("No background in request.")
		return api.GetActionAssetOptions{}, httperror.NewWithCause(apiconstants.InvalidRequestParam, http.StatusBadRequest, errors.New("background is required."))
	}
	if !isValidBackground(background) {
		log.WithFields(log.Fields{"invalid_field": "background", "value": background}).Warn("value for background invalid")
		return api.GetActionAssetOptions{}, httperror.NewWithCause(apiconstants.InvalidRequestParam, http.StatusBadRequest, errors.New("Value for background invalid {dark, light}"))
	}

	scale := query.Get("scale")
	if scale == "" {
		log.WithField("empty_key", "scale").Warn("No scale in request.")
		return api.GetActionAssetOptions{}, httperror.NewWithCause(apiconstants.InvalidRequestParam, http.StatusBadRequest, errors.New("scale is required."))
	}
	if !isValidScale(scale) {
		log.WithFields(log.Fields{"invalid_field": "scale", "value": scale}).Warn("value for scale invalid")
		return api.GetActionAssetOptions{}, httperror.NewWithCause(apiconstants.InvalidRequestParam, http.StatusBadRequest, errors.New("Value for scale invalid {1, 1.5, 2, 3, 4}"))
	}

	state := query.Get("state")
	if state == "" {
		log.WithField("empty_key", "state").Warn("No state in request.")
		return api.GetActionAssetOptions{}, httperror.NewWithCause(apiconstants.InvalidRequestParam, http.StatusBadRequest, errors.New("state is required."))
	}
	if !isValidState(state) {
		log.WithFields(log.Fields{"invalid_field": "state", "value": state}).Warn("value for state invalid")
		return api.GetActionAssetOptions{}, httperror.NewWithCause(apiconstants.InvalidRequestParam, http.StatusBadRequest, errors.New("Value for state invalid {animated, static}"))
	}

	bits := query.Get("bits")
	if bits == "" {
		log.WithField("empty_key", "bits").Warn("No bits amount in request.")
		return api.GetActionAssetOptions{}, httperror.NewWithCause(apiconstants.InvalidRequestParam, http.StatusBadRequest, errors.New("bits amount is required."))
	}
	bitsAmount, err := strconv.ParseInt(bits, 10, 64)
	if err != nil {
		log.WithFields(log.Fields{"invalid_field": "bits", "value": bits}).WithError(err).Warn("Error parsing bits amount")
		return api.GetActionAssetOptions{}, httperror.NewWithCause(apiconstants.InvalidRequestParam, http.StatusBadRequest, errors.New("bits amount must be a number"))
	}

	return api.GetActionAssetOptions{
		Prefix:     prefix,
		Background: api.Background(background),
		Bits:       bitsAmount,
		State:      api.AnimationType(state),
		Scale:      api.Scale(scale),
		ChannelID:  channelId,
	}, nil
}

func isValidBackground(background string) bool {
	bg := api.Background(strings.ToLower(background))
	_, valid := constants.Backgrounds[bg]
	return valid
}

func isValidState(state string) bool {
	s := api.AnimationType(strings.ToLower(state))
	_, valid := constants.AnimationTypes[s]
	return valid
}

func isValidScale(scale string) bool {
	s := api.Scale(strings.ToLower(scale))
	_, valid := constants.Scales[s]
	return valid
}
