package api

import (
	"context"
	"net/http"
	"strconv"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/actions/constants"
	apiconstants "code.justin.tv/commerce/payday/api/constants"
	"code.justin.tv/commerce/payday/api/utils"
	"code.justin.tv/commerce/payday/cache/channel"
	"code.justin.tv/commerce/payday/cartman"
	"code.justin.tv/commerce/payday/clients/uploadservice"
	"code.justin.tv/commerce/payday/datascience"
	"code.justin.tv/commerce/payday/emote"
	httperror "code.justin.tv/commerce/payday/errors/http"
	"code.justin.tv/commerce/payday/image"
	"code.justin.tv/commerce/payday/middleware"
	"code.justin.tv/commerce/payday/models"
	apimodel "code.justin.tv/commerce/payday/models/api"
	"code.justin.tv/commerce/payday/utils/pointers"
	"code.justin.tv/commerce/payday/utils/strings"
	"github.com/gofrs/uuid"
	"github.com/zenazn/goji/web"
)

type ImageApi struct {
	ChannelManager   channel.ChannelManager       `inject:""`
	ImageManager     image.IImageManager          `inject:""`
	CartmanValidator cartman.IValidator           `inject:""`
	PrefixManager    emote.IPrefixManager         `inject:""`
	Datascience      datascience.DataScience      `inject:""`
	UploadService    uploadservice.IImageUploader `inject:""`
}

const (
	defaultVariation    = string(constants.LightBackground) + "_" + string(constants.AnimatedAnimationType)
	darkVariation       = string(constants.DarkBackground) + "_" + string(constants.AnimatedAnimationType)
	staticVariation     = string(constants.LightBackground) + "_" + string(constants.StaticAnimationType)
	staticDarkVariation = string(constants.DarkBackground) + "_" + string(constants.StaticAnimationType)
	simpleUploadType    = "simple"
	advancedUploadType  = "advanced"
)

var ScaleToMaxFileSize map[apimodel.Scale]int = map[apimodel.Scale]int{
	constants.Scale1:  512,
	constants.Scale15: 512,
	constants.Scale2:  512,
	constants.Scale3:  512,
	constants.Scale4:  512,
}

// DEPRECATED: This is the entry point for the legacy cheermote upload flow. The new flow would use Mako's GetEmoteUploadConfig to accomplish this step.
// The validation steps are now handled by PaydayRPC UpdateCheermoteTier
func (api *ImageApi) StartCustomCheermoteUpload(c web.C, w http.ResponseWriter, r *http.Request) error {
	// P99 As of 2020-03-03 is ~2 Seconds
	// We are adding archive logic, want to be less strict
	ctx, cancel := context.WithTimeout(r.Context(), 5*time.Second)
	defer cancel()

	request, err := models.ExtractCustomCheermoteRequest(r)
	if err != nil {
		return httperror.New("Invalid request", http.StatusBadRequest)
	}

	channelId := request.ChannelId
	if strings.Blank(channelId) {
		return httperror.New("Invalid channelId", http.StatusBadRequest)
	}

	err = api.CartmanValidator.ValidateAuthenticatedUserID(r, channelId, true, true)
	if err != nil {
		log.WithError(err).WithField("ChannelID", channelId).Warn("[Image API - StartCustomCheermoteUpload] Cartman token could not be validated for uploading image")
		return err
	}

	channel, err := api.ChannelManager.Get(ctx, channelId)
	if err != nil {
		log.WithError(err).WithField("ChannelID", channelId).Error("[Image API - StartCustomCheermoteUpload] Failed to get get channel from dynamo")
		return httperror.New(apiconstants.DownstreamDependencyError, http.StatusInternalServerError)
	}

	if channel == nil {
		log.WithField("ChannelID", channelId).Error("[Image API - StartCustomCheermoteUpload] Could not find dynamo channel record")
		return httperror.New(apiconstants.DownstreamDependencyError, http.StatusInternalServerError)
	}

	if !pointers.BoolOrDefault(channel.Onboarded, false) {
		log.WithError(err).WithField("ChannelID", channelId).Warn("[Image API - StartCustomCheermoteUpload] Save image request for non-bits enabled channel")
		return httperror.New(apiconstants.UserIneligibleError, http.StatusInternalServerError)
	}

	var imageSetId string
	if strings.BlankP(channel.CustomCheermoteImageSetId) {
		imageSetIdUUID, err := uuid.NewV4()
		if err != nil {
			log.WithError(err).Error("[Image API - StartCustomCheermoteUpload] Error generating UUID")
			return httperror.New(apiconstants.DownstreamDependencyError, http.StatusInternalServerError)
		}

		imageSetId = imageSetIdUUID.String()

		channel.CustomCheermoteImageSetId = pointers.StringP(imageSetId)
		err = api.ChannelManager.Update(ctx, channel)
		if err != nil {
			log.WithError(err).WithField("ChannelID", channelId).Error("[Image API - StartCustomCheermoteUpload] Could not update dynamo channel record with image set id")
			return httperror.New(apiconstants.DownstreamDependencyError, http.StatusInternalServerError)
		}

	} else {
		imageSetId = *channel.CustomCheermoteImageSetId
	}

	uploadType := apimodel.UploadType(request.UploadType)
	if strings.Blank(string(uploadType)) {
		uploadType = image.IndividualUpload
	}

	if !image.IsValidUploadType(uploadType) {
		return httperror.New("Invalid uploadType", http.StatusBadRequest)
	}
	isSmartUpload := uploadType == image.SmartUpload

	tier := apimodel.Tier(request.Tier)
	if !image.IsValidTier(tier) {
		return httperror.New("Invalid tier", http.StatusBadRequest)
	}

	animationType := apimodel.AnimationType(request.AnimationType)
	if !image.IsValidAnimationType(animationType) {
		return httperror.New("Invalid animationType", http.StatusBadRequest)
	}

	background := apimodel.Background(request.Background)
	if !image.IsValidBackground(background) {
		return httperror.New("Invalid background", http.StatusBadRequest)
	}

	scale := apimodel.Scale(request.Scale)
	if !image.IsValidScale(scale) {
		return httperror.New("Invalid scale", http.StatusBadRequest)
	}

	input := uploadservice.UploadServiceInput{
		ChannelId:     channelId,
		SetId:         imageSetId,
		Tier:          tier,
		Background:    background,
		AnimationType: animationType,
		Scale:         scale,
		FileMaxSize:   strconv.Itoa(ScaleToMaxFileSize[scale]) + "KB",
	}

	var response *apimodel.UploadCustomCheermoteResponse
	var keysToArchive []string
	if isSmartUpload {
		response, keysToArchive, err = api.UploadService.SmartUpload(ctx, input)
	} else {
		response, keysToArchive, err = api.UploadService.Upload(ctx, input)
	}

	if err != nil {
		log.WithError(err).WithField("ChannelID", channelId).Error("[Image API - StartCustomCheermoteUpload] Failed to upload image")
		return httperror.New(apiconstants.DownstreamDependencyError, http.StatusInternalServerError)
	}

	// Since this is needed for moderation, we can't put it in goroutine and must block the API to ensure it is successful
	err = api.ImageManager.ArchiveImagesForChannel(ctx, channelId, keysToArchive)
	if err != nil {
		log.WithError(err).WithField("ChannelID", channelId).Error("[Image API - StartCustomCheermoteUpload] Failed to archive image/s")
		return httperror.New(apiconstants.DownstreamDependencyError, http.StatusInternalServerError)
	}

	newCtx, newCancel := middleware.CopyContextValues(ctx)
	go func() {
		defer newCancel()
		api.trackImageUpload(newCtx, image.UploadImageInput{
			ChannelId:           channelId,
			SetId:               imageSetId,
			Tier:                tier,
			Background:          background,
			AnimationType:       animationType,
			Scale:               scale,
			CreationMethod:      image.CreationMethodSource,
			StartSmartUploadJob: isSmartUpload,
		})
	}()

	utils.WriteResponse(response, w)
	return nil
}

func (api *ImageApi) trackImageUpload(ctx context.Context, input image.UploadImageInput) {
	cheermote, hasCustomCheermoteAction, err := api.PrefixManager.GetCustomCheermoteAction(ctx, input.ChannelId)
	if !hasCustomCheermoteAction || err == emote.TooManyPrefixesError {
		cheermote = ""
	} else if err != nil {
		log.WithError(err).WithField("ChannelID", input.ChannelId).Warn("[Image API - trackImageUpload] Could not send datascience for cheermote upload")
		return
	}

	size, err := image.GetDimensionFromScale(input.Scale)
	if err != nil {
		log.WithError(err).WithField("ChannelID", input.ChannelId).Warn("[Image API - trackImageUpload] Could not send datascience for cheermote upload")
		return
	}

	var imageSetting string
	variation := string(input.Background) + "_" + string(input.AnimationType)
	switch variation {
	case defaultVariation:
		imageSetting = "default"
	case darkVariation:
		imageSetting = "dark"
	case staticVariation:
		imageSetting = "static"
	case staticDarkVariation:
		imageSetting = "static_dark"
	default:
		imageSetting = "unknown"
	}

	var uploadType string
	if input.StartSmartUploadJob {
		uploadType = simpleUploadType
	} else {
		uploadType = advancedUploadType
	}

	event := models.BitsPartnerCheermoteDetail{
		ImageSetId:      input.SetId,
		CheermoteString: cheermote,
		ChannelId:       input.ChannelId,
		Tier:            string(input.Tier),
		ImageSize:       int64(size),
		ImageSetting:    imageSetting,
		UploadType:      uploadType,
	}

	err = api.Datascience.TrackBitsEvent(ctx, datascience.BitsPartnerCheemoteDetail, &event)
	if err != nil {
		log.WithError(err).WithField("ChannelID", input.ChannelId).Error("[Image API - trackImageUpload] Could not track upload event")
	}
}

func (api *ImageApi) ModerateChannelImages(c web.C, w http.ResponseWriter, r *http.Request) error {
	ctx, cancel := context.WithTimeout(r.Context(), utils.DefaultTimeout)
	defer cancel()

	channelId := c.URLParams["tuid"]
	if strings.Blank(channelId) {
		return httperror.New("Invalid channelId", http.StatusBadRequest)
	}

	channel, err := api.ChannelManager.Get(ctx, channelId)
	if err != nil {
		log.WithError(err).WithField("ChannelID", channelId).Error("[Image API - ModerateChannelImages] Failed to get get channel from dynamo")
		return httperror.New(apiconstants.DownstreamDependencyError, http.StatusInternalServerError)
	}

	if channel == nil {
		log.WithField("ChannelID", channelId).Error("[Image API - ModerateChannelImages] Could not find dynamo channel record")
		return httperror.New(apiconstants.DownstreamDependencyError, http.StatusInternalServerError)
	}

	customCheermoteImageSetIdUUID, err := uuid.NewV4()
	if err != nil {
		log.WithError(err).Error("[Image API - ModerateChannelImages] Error generating UUID")
		return httperror.New(apiconstants.DownstreamDependencyError, http.StatusInternalServerError)
	}

	channel.CustomCheermoteStatus = pointers.StringP(apimodel.CustomCheermoteStatusModerated)
	channel.CustomCheermoteImageSetId = pointers.StringP(customCheermoteImageSetIdUUID.String())

	err = api.ChannelManager.Update(ctx, channel)
	if err != nil {
		log.WithError(err).WithField("ChannelID", channelId).Error("[Image API - ModerateChannelImages] Failed to update channel record during moderation")
		return httperror.New(apiconstants.DownstreamDependencyError, http.StatusInternalServerError)
	}

	err = api.ImageManager.PublishImageModerationJob(channelId)
	if err != nil {
		log.WithError(err).WithField("ChannelID", channelId).Error("[Image API - ModerateChannelImages] Failed to publish image moderation job for channelId")
		return httperror.New(apiconstants.DownstreamDependencyError, http.StatusInternalServerError)
	}

	return nil
}

func (api *ImageApi) DownloadChannelImages(c web.C, w http.ResponseWriter, r *http.Request) error {
	ctx, cancel := context.WithTimeout(r.Context(), time.Second*7)
	defer cancel()

	channelId := c.URLParams["tuid"]

	channel, err := api.ChannelManager.Get(ctx, channelId)
	if err != nil {
		log.WithError(err).WithField("ChannelID", channelId).Error("[Image API - ModerateChannelImages] Failed to get get channel from dynamo")
		return httperror.New(apiconstants.DownstreamDependencyError, http.StatusInternalServerError)
	}

	if channel == nil {
		log.WithField("ChannelID", channelId).Error("[Image API - ModerateChannelImages] Could not find dynamo channel record")
		return httperror.New(apiconstants.DownstreamDependencyError, http.StatusInternalServerError)
	}

	if strings.BlankP(channel.CustomCheermoteImageSetId) {
		return nil
	}

	w.Header().Set("Content-Disposition", "attachment; filename=custom_cheermotes.zip")
	w.Header().Set("Content-Type", r.Header.Get("Content-Type"))

	err = api.ImageManager.WriteImagesZip(ctx, *channel.CustomCheermoteImageSetId, w)
	if err != nil {
		log.WithError(err).WithField("ChannelID", channelId).Error("[Image API - ModerateChannelImages] Error downloading images for channelId")
		return httperror.New(apiconstants.DownstreamDependencyError, http.StatusInternalServerError)
	}

	return nil
}

func (api *ImageApi) getChannelImagesData(c web.C, w http.ResponseWriter, r *http.Request) (apimodel.Images, string, error) {
	ctx, cancel := utils.ContextWithDefaultTimeout(r.Context())
	defer cancel()

	channelId := c.URLParams["tuid"]
	if strings.Blank(channelId) {
		return apimodel.Images{}, "", httperror.New("Invalid channelId", http.StatusBadRequest)
	}

	channel, err := api.ChannelManager.Get(ctx, channelId)
	if err != nil {
		log.WithError(err).WithField("ChannelID", channelId).Error("[Image API - GetChannelImages] Failed to get channel from dynamo")
		return apimodel.Images{}, "", httperror.New(apiconstants.DownstreamDependencyError, http.StatusInternalServerError)
	}

	if channel == nil {
		log.WithField("ChannelID", channelId).Warn("[Image API - GetChannelImages] Could not find dynamo channel record")
		return apimodel.Images{}, "", httperror.New("Channel not found", http.StatusNotFound)
	}

	if strings.BlankP(channel.CustomCheermoteImageSetId) {
		response := &apimodel.GetChannelImagesResponse{
			Jobs: []apimodel.Job{},
		}
		utils.WriteResponse(response, w)
		return apimodel.Images{}, "", nil
	}

	imageSetId := *channel.CustomCheermoteImageSetId

	imagesByTier, err := api.ImageManager.GetImages(ctx, imageSetId)
	if err != nil {
		log.WithError(err).WithField("ChannelID", channelId).Error("[Image API - GetChannelImages] Failed to get images")
		return apimodel.Images{}, "", httperror.New(apiconstants.DownstreamDependencyError, http.StatusInternalServerError)
	}

	imagesResponse := apimodel.Images{}
	for tier, imagesByBackground := range imagesByTier {
		imageTier := apimodel.ImageTier{}

		for background, imagesByAnimationType := range imagesByBackground {
			imageTierForBackground := apimodel.ImageTierForBackground{}

			for animationType, imagesByScale := range imagesByAnimationType {
				imageSet := apimodel.ImageSet{}

				for scale, img := range imagesByScale {
					url := image.GetCloudfrontURLFromImg(*img)
					switch scale {
					case constants.Scale1:
						imageSet.Scale1 = url
					case constants.Scale15:
						imageSet.Scale15 = url
					case constants.Scale2:
						imageSet.Scale2 = url
					case constants.Scale3:
						imageSet.Scale3 = url
					case constants.Scale4:
						imageSet.Scale4 = url
					}
				}

				switch animationType {
				case constants.AnimatedAnimationType:
					imageTierForBackground.Animated = imageSet
				case constants.StaticAnimationType:
					imageTierForBackground.Static = imageSet
				}
			}

			switch background {
			case constants.LightBackground:
				imageTier.Light = imageTierForBackground
			case constants.DarkBackground:
				imageTier.Dark = imageTierForBackground
			}
		}

		switch tier {
		case constants.Tier1:
			imagesResponse.Tier1 = imageTier
		case constants.Tier100:
			imagesResponse.Tier100 = imageTier
		case constants.Tier1000:
			imagesResponse.Tier1000 = imageTier
		case constants.Tier5000:
			imagesResponse.Tier5000 = imageTier
		case constants.Tier10000:
			imagesResponse.Tier10000 = imageTier
		}
	}

	prefix, hasCustomCheermoteAction, err := api.PrefixManager.GetCustomCheermoteAction(ctx, channelId)
	if !hasCustomCheermoteAction {
		prefix = ""
	} else if err != nil {
		log.WithError(err).WithField("ChannelID", channelId).Error("[Image API - GetChannelImages] Failed to load prefixes")
		return apimodel.Images{}, "", httperror.New(apiconstants.DownstreamDependencyError, http.StatusInternalServerError)
	}

	return imagesResponse, prefix, nil
}

func (api *ImageApi) GetCustomCheermotes(c web.C, w http.ResponseWriter, r *http.Request) error {
	channelId := c.URLParams["tuid"]
	if strings.Blank(channelId) {
		return httperror.New("Invalid channelId", http.StatusBadRequest)
	}

	err := utils.ValidateRequestUserId(channelId)
	if err != nil {
		return err
	}

	err = api.CartmanValidator.ValidateAuthenticatedUserID(r, channelId, true, true)
	if err != nil {
		log.Warnf("Cartman token could not be validated for getting custom cheermote images")
		return err
	}

	imagesResponse, prefix, err := api.getChannelImagesData(c, w, r)
	if err != nil {
		return err
	}

	response := &apimodel.GetCustomCheermotesResponse{
		Images: imagesResponse,
		Prefix: prefix,
	}

	utils.WriteResponse(response, w)
	return nil
}
