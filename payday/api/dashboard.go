package api

import (
	"fmt"
	"net/http"
	"strconv"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/api/utils"
	"code.justin.tv/commerce/payday/dynamo"
	httperror "code.justin.tv/commerce/payday/errors/http"
	"code.justin.tv/commerce/payday/models"
	"github.com/zenazn/goji/web"
)

type DashboardApi struct {
	Dao dynamo.IRevenueDao `inject:""`
}

type GetBitsRevenueParams struct {
	UserId    string
	StartDate time.Time
	EndDate   time.Time
}

type DateDuration struct {
	Months int
	Days   int
}

func (d *DashboardApi) GetBroadcasterRevenue(c web.C, w http.ResponseWriter, r *http.Request) error {

	params, err := processGetBitsRevenueParams(c, r)
	if err != nil {
		return err
	}

	dynamoRecords, err := d.Dao.GetWithinRange(dynamo.ChannelId(params.UserId), params.StartDate, params.EndDate)
	if err != nil {
		log.Error("Failed to get broadcaster revenue data from dynamo")
		return httperror.NewWithCause("Failed to get revenue data", http.StatusInternalServerError, err)
	}

	revenueRecords, err := convertRecords(dynamoRecords)
	if err != nil {
		msg := fmt.Sprintf("Failed to group broadcaster revenue")
		log.WithField("userId", params.UserId).Error(msg)
		return httperror.NewWithCause(msg, http.StatusInternalServerError, err)
	}

	response := models.GetBroadcasterRevenueResponse{
		Records: revenueRecords,
	}

	utils.WriteResponse(response, w)

	return nil
}

func processGetBitsRevenueParams(c web.C, r *http.Request) (*GetBitsRevenueParams, error) {
	userId, ok := c.URLParams["tuid"]
	if !ok {
		log.Warn("Missing user parameter")
		return nil, httperror.New("Missing user parameter", http.StatusBadRequest)
	}

	startDateStr := r.URL.Query().Get("start")
	if startDateStr == "" {
		log.Warn("Missing start parameter")
		return nil, httperror.New("Missing start parameter", http.StatusBadRequest)
	}

	endDateStr := r.URL.Query().Get("end")
	if endDateStr == "" {
		log.Warn("Missing end parameter")
		return nil, httperror.New("Missing end parameter", http.StatusBadRequest)
	}

	startDateUnix, err := strconv.ParseInt(startDateStr, 10, 64)
	if err != nil {
		log.Warnf("Invalid start date parameter: %s", startDateStr)
		return nil, httperror.NewWithCause(fmt.Sprintf("Invalid start date parameter: %s", startDateStr), http.StatusBadRequest, err)
	}

	endDateUnix, err := strconv.ParseInt(endDateStr, 10, 64)
	if err != nil {
		log.Warnf("Invalid end date parameter: %s", endDateStr)
		return nil, httperror.NewWithCause(fmt.Sprintf("Invalid end date parameter: %s", endDateStr), http.StatusBadRequest, err)
	}

	startDateTime := time.Unix(startDateUnix, 0)
	endDateTime := time.Unix(endDateUnix, 0)

	if startDateTime.After(endDateTime) {
		msg := fmt.Sprintf("End date must be after start date")
		log.WithFields(log.Fields{
			"StartDate": startDateTime,
			"EndDate":   endDateTime,
		}).Info(msg)
		return nil, httperror.NewWithCause(msg, http.StatusBadRequest, nil)
	}

	return &GetBitsRevenueParams{
		UserId:    userId,
		StartDate: startDateTime,
		EndDate:   endDateTime,
	}, nil
}

func convertRecords(dynamoRecords []*dynamo.RevenueRecord) ([]models.BroadcasterRevenueRecord, error) {
	var result []models.BroadcasterRevenueRecord

	for _, dr := range dynamoRecords {
		record := models.BroadcasterRevenueRecord{
			Date:  dr.Date,
			Bits:  int64(dr.BitCount),
			Cents: int64(dr.CentCount),
		}
		result = append(result, record)
	}

	return result, nil
}
