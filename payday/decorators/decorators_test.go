package decorators

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"code.justin.tv/commerce/payday/api/apidef"
	"code.justin.tv/commerce/payday/errors"
	errs "code.justin.tv/commerce/payday/errors/http"
	"github.com/stretchr/testify/assert"
	"github.com/zenazn/goji/web"
)

func TestErrorDecoratorHttpError(t *testing.T) {
	statusCode := 200
	message := "test"

	f := func(web.C, http.ResponseWriter, *http.Request) error {
		return errs.New(message, statusCode)
	}

	classUnderTest := NewErrorHandlingDecorator(f, apidef.AdminAddBits)

	writer := httptest.NewRecorder()
	request := &http.Request{}
	context := web.C{}

	_ = classUnderTest.Handle(context, writer, request)

	assert.Equal(t, statusCode, writer.Code)
	resp, err := errs.ConvertJsonToErrorMessageResponse(writer.Body.Bytes())
	assert.Nil(t, err)
	assert.Equal(t, message, resp.Message)

}

func TestErrorDecoratorUnknownError(t *testing.T) {
	f := func(web.C, http.ResponseWriter, *http.Request) error {
		return errors.New("test")
	}

	classUnderTest := NewErrorHandlingDecorator(f, apidef.AdminAddBits)

	writer := httptest.NewRecorder()
	request := &http.Request{}
	context := web.C{}

	_ = classUnderTest.Handle(context, writer, request)

	assert.Equal(t, http.StatusInternalServerError, writer.Code)
}
