package decorators

import (
	"encoding/json"
	"net/http"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/api/apidef"
	errors "code.justin.tv/commerce/payday/errors/http"
	"github.com/zenazn/goji/web"
)

const SmallMaxBytesRequestSize int64 = 1024 * 1024      // 1mb
const LargeMaxBytesRequestSize int64 = 50 * 1024 * 1024 // 50mb

var LargeRequestSizeWhitelist = map[apidef.HandlerName]bool{
	apidef.StartAdminJob: true,
}

type RequestHandler interface {
	Handle(web.C, http.ResponseWriter, *http.Request) error
}

type ErrorHandlingDecorator struct {
	handlerFunction func(web.C, http.ResponseWriter, *http.Request) error
	handlerName     apidef.HandlerName
}

type RequestHandlerAdapter struct {
	handler     RequestHandler
	handlerName apidef.HandlerName
}

func (a RequestHandlerAdapter) GetHandlerName() apidef.HandlerName {
	return a.handlerName
}

func Adapt(rh RequestHandler, handlerName apidef.HandlerName) web.Handler {
	return RequestHandlerAdapter{
		handler:     rh,
		handlerName: handlerName,
	}
}

func NewErrorHandlingDecorator(funct func(web.C, http.ResponseWriter, *http.Request) error, handlerName apidef.HandlerName) RequestHandler {
	return ErrorHandlingDecorator{funct, handlerName}
}

func DecorateAndAdapt(funct func(web.C, http.ResponseWriter, *http.Request) error, handlerName apidef.HandlerName) web.Handler {
	rh := NewErrorHandlingDecorator(funct, handlerName)
	return Adapt(rh, handlerName)
}

func (dec ErrorHandlingDecorator) Handle(c web.C, rw http.ResponseWriter, r *http.Request) error {
	if r.Body != nil {
		if LargeRequestSizeWhitelist[dec.handlerName] {
			r.Body = http.MaxBytesReader(rw, r.Body, LargeMaxBytesRequestSize)
		} else {
			r.Body = http.MaxBytesReader(rw, r.Body, SmallMaxBytesRequestSize)
		}
	}

	err := dec.handlerFunction(c, rw, r)

	if err != nil {
		httpErr, ok := err.(errors.HttpError)

		if ok {
			resp, err := json.Marshal(httpErr.Response())
			if err != nil {
				log.WithError(err).WithField("response", httpErr.Response()).Error("Unable to marshal HttpErrorResponse into json")
				rw.WriteHeader(http.StatusInternalServerError)
				_, _ = rw.Write([]byte("Unable to marshal response"))
				return err
			}

			statusCode := httpErr.StatusCode()
			rw.WriteHeader(statusCode)

			rw.Header().Add("Content-Type", "application/json")
			_, err = rw.Write(resp)

			if err != nil {
				log.WithError(err).Error("Failed while writing error response out via HTTP")
			}

		} else {
			rw.WriteHeader(http.StatusInternalServerError)
			_, _ = rw.Write([]byte("Encountered unknown error"))
		}
	}

	return nil
}

func (a RequestHandlerAdapter) ServeHTTPC(c web.C, rw http.ResponseWriter, r *http.Request) {
	_ = a.handler.Handle(c, rw, r)
}
