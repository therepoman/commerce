###############################################################################
## Option Settings
##    Namespace: "aws:elasticbeanstalk:application:environment"
##    OptionName: StdouterrCWLogGroup
##       Default:   <EnvironmentName>-stdouterr
##    Description: This is the name of the cloudwatch log group for the stdouterr log
##
###############################################################################


Mappings:
  CWLogs:
    StdouterrLogGroup:
      LogFile: "/var/log/eb-docker/containers/eb-current-app/*-stdouterr.log"
      #2018-01-05T18:29:54Z
      TimestampFormat: "%Y-%m-%dT%H:%M:%S"

Outputs:
  StdouterrCWLogGroup:
    Description: "The name of the Cloudwatch Logs Log Group created for this environments stdouterr logs. You can specify this by setting the value for the environment variable: StdouterrCWLogGroup. Please note: if you update this value, then you will need to go and clear out the old cloudwatch logs group and delete it through Cloudwatch Logs."
    Value: { "Ref" : "AWSEBCloudWatchLogsStdouterrLogGroup"}


Resources :
  AWSEBCloudWatchLogsStdouterrLogGroup:    ## Must have prefix:  AWSEBCloudWatchLogs
    Type: "AWS::Logs::LogGroup"
    DependsOn: AWSEBBeanstalkMetadata
    DeletionPolicy: Retain     ## this is required
    Properties:
      LogGroupName:
        "Fn::GetOptionSetting":
          Namespace: "aws:elasticbeanstalk:application:environment"
          OptionName: StdouterrCWLogGroup
          DefaultValue: {"Fn::Join":["/", ["/twitch", {"Ref":"AWS::Region"}, { "Ref":"AWSEBEnvironmentName" }]]}
      RetentionInDays: 90

  ## Register the files/log groups for monitoring
  AWSEBAutoScalingGroup:
    Metadata:
      "AWS::CloudFormation::Init":
        CWLogsAgentConfigSetup:
          files:
            ## any .conf file put into /tmp/cwlogs/conf.d will be added to the cwlogs config (see cwl-agent.config)
            "/tmp/cwlogs/conf.d/stdouterr-logs.conf":
              content : |
                [stdouterr]
                file = `{"Fn::FindInMap":["CWLogs", "StdouterrLogGroup", "LogFile"]}`
                log_group_name = `{ "Ref" : "AWSEBCloudWatchLogsStdouterrLogGroup" }`
                log_stream_name = {instance_id}
                datetime_format = `{"Fn::FindInMap":["CWLogs", "StdouterrLogGroup", "TimestampFormat"]}`
              mode  : "000400"
              owner : root
              group : root