package metrics

import (
	"time"

	log "code.justin.tv/commerce/logrus"
	"github.com/cactus/go-statsd-client/statsd"
)

type Statter interface {
	Inc(name string, value int64)
	TimingDuration(name string, dur time.Duration)
	Gauge(name string, val int64)
}

func NewStatter(s statsd.Statter) Statter {
	return &statter{
		Statsd: s,
	}
}

func NewNoopStatter() Statter {
	s, _ := statsd.NewNoop()

	return &statter{
		Statsd: s,
	}
}

type statter struct {
	Statsd statsd.Statter
}

func (s *statter) Inc(name string, value int64) {
	err := s.Statsd.Inc(name, value, 1.0)
	if err != nil {
		log.WithError(err).WithFields(log.Fields{
			"metricName": name,
			"value":      value,
		}).Error("failed to queue inc metric")
	}
}

func (s *statter) TimingDuration(name string, dur time.Duration) {
	err := s.Statsd.TimingDuration(name, dur, 1.0)
	if err != nil {
		log.WithError(err).WithFields(log.Fields{
			"metricName": name,
			"duration":   dur,
		}).Error("failed to queue duration metric")
	}
}

func (s *statter) Gauge(name string, val int64) {
	err := s.Statsd.Gauge(name, val, 1.0)
	if err != nil {
		log.WithError(err).WithFields(log.Fields{
			"metricName": name,
			"value":      val,
		}).Error("failed to queue gauge metric")
	}
}
