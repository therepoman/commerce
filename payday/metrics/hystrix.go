package metrics

import (
	metricCollector "github.com/afex/hystrix-go/hystrix/metric_collector" //nolint:depguard
)

// HystrixMetricsCollector holds information about the circuit state.
// This implementation of MetricCollector is the canonical source of information about the circuit.
// It is used for for all internal hystrix operations
// including circuit health checks and metrics sent to the hystrix dashboard.
//
// Metric Collectors do not need Mutexes as they are updated by circuits within a locked context.
type HystrixMetricsCollector struct {
	Statter Statter `inject:""`
}

func NewHystrixMetricsCollector() *HystrixMetricsCollector {
	return &HystrixMetricsCollector{}
}

type HystrixCommandMetricsCollector struct {
	name string

	attemptsMetric          string
	errorsMetric            string
	successesMetric         string
	failuresMetric          string
	rejectsMetric           string
	shortCircuitsMetric     string
	timeoutsMetric          string
	fallbackSuccessesMetric string
	fallbackFailuresMetric  string
	contextCancelledMetric  string
	contextDeadlineMetric   string
	totalDurationMetric     string
	runDurationMetric       string

	Statter Statter
}

func (h *HystrixMetricsCollector) NewHystrixCommandMetricsCollector(name string) metricCollector.MetricCollector {
	statsPrefix := "hystrix." + name

	return &HystrixCommandMetricsCollector{
		name:                    name,
		attemptsMetric:          statsPrefix + ".attempts",
		errorsMetric:            statsPrefix + ".errors",
		failuresMetric:          statsPrefix + ".failures",
		rejectsMetric:           statsPrefix + ".rejects",
		shortCircuitsMetric:     statsPrefix + ".shortCircuits",
		timeoutsMetric:          statsPrefix + ".timeouts",
		fallbackSuccessesMetric: statsPrefix + ".fallbackSuccesses",
		fallbackFailuresMetric:  statsPrefix + ".fallbackFailures",
		contextCancelledMetric:  statsPrefix + ".contextCancelled",
		contextDeadlineMetric:   statsPrefix + ".contextDeadlineExceeded",
		totalDurationMetric:     statsPrefix + ".totalDuration",
		runDurationMetric:       statsPrefix + ".runDuration",
		Statter:                 h.Statter,
	}
}

func (d *HystrixCommandMetricsCollector) Update(r metricCollector.MetricResult) {
	go func() {
		d.Statter.Inc(d.attemptsMetric, int64(r.Attempts))
		d.Statter.Inc(d.errorsMetric, int64(r.Errors))
		d.Statter.Inc(d.successesMetric, int64(r.Successes))
		d.Statter.Inc(d.failuresMetric, int64(r.Failures))
		d.Statter.Inc(d.rejectsMetric, int64(r.Rejects))
		d.Statter.Inc(d.shortCircuitsMetric, int64(r.ShortCircuits))
		d.Statter.Inc(d.timeoutsMetric, int64(r.Timeouts))
		d.Statter.Inc(d.fallbackSuccessesMetric, int64(r.FallbackSuccesses))
		d.Statter.Inc(d.fallbackFailuresMetric, int64(r.FallbackFailures))
		d.Statter.Inc(d.contextCancelledMetric, int64(r.ContextCanceled))
		d.Statter.Inc(d.contextDeadlineMetric, int64(r.ContextDeadlineExceeded))

		d.Statter.TimingDuration(d.totalDurationMetric, r.TotalDuration)
		d.Statter.TimingDuration(d.runDurationMetric, r.RunDuration)
	}()
}

// It's a noop because it's all set remotely
func (d *HystrixCommandMetricsCollector) Reset() {}
