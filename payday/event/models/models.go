package models

import (
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/partner"
)

type Request struct {
	UserID           string
	ChannelID        string
	Amount           models.Bits
	Message          string
	EventID          string
	EventReasonCode  string
	EmoteUses        map[string]models.Bits
	ClientID         string
	PartnerType      partner.PartnerType
	SponsoredAmounts map[string]models.Bits
	IPAddress        string
	NumChatters      string
	IsAnonymous      bool
	IsLegacy         bool
	CountryCode      string
	City             string
	PostalCode       string
}
