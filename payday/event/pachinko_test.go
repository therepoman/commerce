package event

import (
	"context"
	"testing"

	"code.justin.tv/commerce/gogogadget/random"
	"code.justin.tv/commerce/payday/event/models"
	pachinko_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/pachinko"
	payday_models "code.justin.tv/commerce/payday/models"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
)

func TestPachinkoGiver_Give(t *testing.T) {
	Convey("given a pachinko giver", t, func() {
		pachinkoClient := new(pachinko_mock.PachinkoClient)

		giver := &pachinkoGiver{
			Pachinko: pachinkoClient,
		}

		ctx := context.Background()
		req := models.Request{
			EventID: random.String(32),
			UserID:  random.String(16),
			Amount:  payday_models.Bits(322),
		}

		Convey("when we fail to consume balance", func() {
			pachinkoClient.On("RemoveBits", ctx, req.EventID, req.UserID, int(req.Amount)).Return(0, errors.New("WALRUS STRIKE"))

			_, errType, err := giver.Give(ctx, req)

			So(err, ShouldNotBeNil)
			So(errType, ShouldEqual, payday_models.BitsUnavailable)
		})

		Convey("when we succeed to consume balance", func() {
			pachinkoClient.On("RemoveBits", ctx, req.EventID, req.UserID, int(req.Amount)).Return(123, nil)

			resp, errType, err := giver.Give(ctx, req)

			So(resp, ShouldEqual, int32(123))
			So(err, ShouldBeNil)
			So(errType, ShouldEqual, payday_models.Default)
		})
	})
}
