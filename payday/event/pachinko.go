package event

import (
	"context"
	"net/http"

	"code.justin.tv/commerce/payday/clients/pachinko"
	httperror "code.justin.tv/commerce/payday/errors/http"
	"code.justin.tv/commerce/payday/event/models"
	payday_models "code.justin.tv/commerce/payday/models"
)

type PachinkoGiver interface {
	Give(ctx context.Context, request models.Request) (int32, payday_models.ErrorType, error)
}

func NewPachinkoGiver() PachinkoGiver {
	return &pachinkoGiver{}
}

type pachinkoGiver struct {
	Pachinko pachinko.PachinkoClient `inject:""`
}

func (p *pachinkoGiver) Give(ctx context.Context, request models.Request) (int32, payday_models.ErrorType, error) {
	// TODO: Need other logic to do when we remove bits, like tracking of the emote uses and stuff.

	resp, err := p.Pachinko.RemoveBits(ctx, request.EventID, request.UserID, int(request.Amount))

	if err != nil {
		if err == pachinko.ErrInsufficientBalance {
			return 0, payday_models.InsufficientFunds, httperror.NewWithBlobAndCause(err, http.StatusUnprocessableEntity, err)
		} else if err == pachinko.ErrAlreadyCommitted {
			return 0, payday_models.DuplicateEventId, httperror.NewWithCause("Request contained an existing eventId", http.StatusBadRequest, err)
		} else {
			return 0, payday_models.BitsUnavailable, err
		}
	}

	return int32(resp), payday_models.Default, nil
}
