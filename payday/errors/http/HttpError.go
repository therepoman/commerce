package http

import (
	"encoding/json"
	"fmt"

	"code.justin.tv/commerce/payday/errors"
)

type ErrorMessageResponse struct {
	Message string `json:"message"`
}

type ErrorResponse struct {
	Message string `json:"message"`
	Status  string `json:"status"`
}

type HttpError struct {
	response interface{}

	statusCode int

	cause error
}

func New(message string, statusCode int) HttpError {
	response := ErrorMessageResponse{
		Message: message,
	}
	return HttpError{response, statusCode, nil}
}

func NewWithCause(message string, statusCode int, cause error) HttpError {
	response := ErrorMessageResponse{
		Message: message,
	}
	return HttpError{response, statusCode, cause}
}

func NewWithBlob(response interface{}, statusCode int) HttpError {
	return HttpError{response, statusCode, nil}
}

func NewWithBlobAndCause(response interface{}, statusCode int, cause error) HttpError {
	return HttpError{response, statusCode, cause}
}

func (e HttpError) Error() string {
	return fmt.Sprintf("HTTP Error: Response [%+v], Code [%d], Cause [%v]", e.response, e.statusCode, e.cause)
}

func (e HttpError) StatusCode() int {
	return e.statusCode
}

func (e HttpError) Response() interface{} {
	return e.response
}

func (e HttpError) Underlying() error {
	return e.cause
}

func ConvertJsonToErrorMessageResponse(jsonResponse []byte) (*ErrorMessageResponse, error) {
	var response ErrorMessageResponse
	err := json.Unmarshal(jsonResponse, &response)
	if err != nil {
		msg := "Could not convert json to PostTransactionResponse"
		return nil, errors.Notef(err, msg)
	}
	return &response, nil
}
