package errors

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNew(t *testing.T) {
	err := New("Friendly error")
	assert.Equal(t, "Friendly error", err.Message())
}

func TestNewf(t *testing.T) {
	err := Newf("broke %s", "by bounty hunters")
	assert.Equal(t, "broke by bounty hunters", err.Message())
}

func TestNotef(t *testing.T) {
	cause := New("cause!")
	err := Notef(cause, "broke %s", "by rebels")

	assert.Equal(t, "broke by rebels", err.Message())
	assert.Equal(t, cause, err.Cause())
}
