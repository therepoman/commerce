package products

import (
	"testing"

	"code.justin.tv/commerce/payday/config"
	petozi "code.justin.tv/commerce/petozi/rpc"
	. "github.com/smartystreets/goconvey/convey"
)

func TestTranslator_Translate(t *testing.T) {
	Convey("Given a translator", t, func() {
		c := &config.Configuration{
			Translations: config.BitsTranslations{
				BitsPrimePromo: map[string]string{
					"en": "Walrus prime promo",
				},
				BitsFirstTimePromo: map[string]string{
					"en": "Walrus first time promo",
				},
			},
		}

		translator := &translator{
			Config: c,
		}

		locale := "en"
		Convey("When the product is the Bits Prime Promo", func() {
			petoziProductInput := petozi.BitsProduct{
				Platform:               petozi.Platform_AMAZON,
				Quantity:               322,
				MaxPurchasableQuantity: 1,
				Promo: &petozi.Promo{
					Id: TwitchPrime2018Promo,
				},
			}

			Convey("When the locale is not in our config", func() {
				locale = "es"

				Convey("Then we should return the correct localized string from the default translations", func() {
					output := translator.Translate(petoziProductInput, locale)

					So(*output, ShouldEqual, translations[TwitchPrime2018Promo])
				})
			})

			Convey("When the locale is in our config", func() {
				Convey("Then we should return the correct localized string from the config translations", func() {
					output := translator.Translate(petoziProductInput, locale)

					So(*output, ShouldEqual, c.Translations.BitsPrimePromo[locale])
				})
			})
		})

		Convey("When the product is the first time promo", func() {
			petoziProductInput := petozi.BitsProduct{
				Platform:               petozi.Platform_AMAZON,
				Quantity:               322,
				MaxPurchasableQuantity: 1,
				Promo: &petozi.Promo{
					Id: FirstTimePromo,
				},
			}

			Convey("When the locale is not in our config", func() {
				locale = "es"

				Convey("Then we should return the correct localized string from the default translations", func() {
					output := translator.Translate(petoziProductInput, locale)

					So(*output, ShouldEqual, translations[FirstTimePromo])
				})
			})

			Convey("When the locale is in our config", func() {
				Convey("Then we should return the correct localized string from the config translations", func() {
					output := translator.Translate(petoziProductInput, locale)

					So(*output, ShouldEqual, c.Translations.BitsFirstTimePromo[locale])
				})
			})
		})

		Convey("When the product is a different promo", func() {
			petoziProductInput := petozi.BitsProduct{
				Platform:               petozi.Platform_AMAZON,
				Quantity:               322,
				MaxPurchasableQuantity: 1,
				Promo: &petozi.Promo{
					Id: "SomeOtherPromo",
				},
			}

			Convey("When the promo is not in our default translations", func() {
				Convey("Then we should return the correct localized string from the default translations", func() {
					output := translator.Translate(petoziProductInput, locale)

					So(*output, ShouldEqual, translations["default"])
				})
			})

			Convey("When the promo is in our default translations", func() {
				translations["SomeOtherPromo"] = "Some other Walrus Promo"

				Convey("Then we should return the correct localized string from the default translations", func() {
					output := translator.Translate(petoziProductInput, locale)

					So(*output, ShouldEqual, translations["SomeOtherPromo"])
				})
			})
		})

		Convey("When the product is a not a promo", func() {
			petoziProductInput := petozi.BitsProduct{
				Platform:               petozi.Platform_AMAZON,
				Quantity:               322,
				MaxPurchasableQuantity: 1,
			}

			Convey("Then it should add the product to the return list", func() {
				output := translator.Translate(petoziProductInput, locale)

				So(output, ShouldBeNil)
			})
		})
	})
}
