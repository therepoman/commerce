package products

import (
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/models"
)

type Checker interface {
	IsMobileCountryEnabled(country string, platform models.Platform) bool
}

type checker struct {
	Config *config.Configuration `inject:""`
}

func NewChecker() Checker {
	return &checker{}
}

func (c *checker) IsMobileCountryEnabled(country string, platform models.Platform) bool {
	var iapEnabledCountries []string
	if platform == models.ANDROID {
		iapEnabledCountries = c.Config.MobileConfig.Android.IAPEnabledCountries
	} else if platform == models.IOS {
		iapEnabledCountries = c.Config.MobileConfig.IOS.IAPEnabledCountries
	}

	for _, allowedCountry := range iapEnabledCountries {
		if allowedCountry == country {
			return true
		}
	}
	return false
}
