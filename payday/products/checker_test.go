package products

import (
	"testing"

	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/models"

	. "github.com/smartystreets/goconvey/convey"
)

func TestChecker_IsMobileCountryEnabled(t *testing.T) {
	Convey("Given a checker", t, func() {
		mockConfig := &config.Configuration{
			MobileConfig: config.MobileConfig{
				Android: config.MobilePlatformConfig{
					IAPEnabledCountries: []string{
						"US",
					},
				},
				IOS: config.MobilePlatformConfig{
					IAPEnabledCountries: []string{
						"US",
					},
				},
			},
		}

		checker := &checker{
			Config: mockConfig,
		}

		Convey("When the platform is Android", func() {
			platform := models.ANDROID

			Convey("When the country is not IAP enabled", func() {
				cor := "GB"

				Convey("We should return false", func() {
					eligible := checker.IsMobileCountryEnabled(cor, platform)

					So(eligible, ShouldBeFalse)
				})

			})

			Convey("When the country is IAP enabled", func() {
				cor := "US"

				Convey("We should return true", func() {
					eligible := checker.IsMobileCountryEnabled(cor, platform)

					So(eligible, ShouldBeTrue)
				})
			})
		})

		Convey("When the platform is iOS", func() {
			platform := models.IOS

			Convey("When the country is not IAP enabled", func() {
				cor := "GB"

				Convey("We should return false", func() {
					eligible := checker.IsMobileCountryEnabled(cor, platform)

					So(eligible, ShouldBeFalse)
				})
			})

			Convey("When the country is IAP enabled", func() {
				cor := "US"

				Convey("We should return true", func() {
					eligible := checker.IsMobileCountryEnabled(cor, platform)

					So(eligible, ShouldBeTrue)
				})
			})
		})
	})
}
