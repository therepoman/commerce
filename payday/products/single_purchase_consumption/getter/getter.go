package getter

import (
	"context"

	log "code.justin.tv/commerce/logrus"
	dynamo "code.justin.tv/commerce/payday/dynamo/single_purchase_consumption"
	"code.justin.tv/commerce/payday/products/single_purchase_consumption/cache"
)

type Getter interface {
	Get(ctx context.Context, userID, productID string) *dynamo.SinglePurchaseConsumption
}

type getter struct {
	Cache cache.Cache                         `inject:"singlePurchaseConsumptionCache"`
	DAO   dynamo.SinglePurchaseConsumptionDAO `inject:""`
}

func NewGetter() Getter {
	return &getter{}
}

func (f *getter) Get(ctx context.Context, userID, productID string) *dynamo.SinglePurchaseConsumption {
	cached := f.Cache.Get(ctx, userID, productID)
	if cached != nil {
		return cached.Body
	}

	dynamoRecord, err := f.DAO.Get(ctx, userID, productID)
	if err != nil {
		log.WithFields(log.Fields{
			"user_id":    userID,
			"product_id": productID,
		}).WithError(err).Error("Failed to query dynamo for single purchase consumption")
		return nil
	}

	go f.Cache.Set(context.Background(), userID, productID, dynamoRecord)

	return dynamoRecord
}
