package getter

import (
	"context"
	"errors"
	"testing"

	"github.com/stretchr/testify/mock"

	dynamo "code.justin.tv/commerce/payday/dynamo/single_purchase_consumption"
	dao_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo/single_purchase_consumption"
	cache_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/products/single_purchase_consumption/cache"
	"code.justin.tv/commerce/payday/products/single_purchase_consumption/cache"
	. "github.com/smartystreets/goconvey/convey"
)

func TestGetter(t *testing.T) {
	Convey("Given single purchase getter", t, func() {
		cacheMock := new(cache_mock.Cache)
		daoMock := new(dao_mock.SinglePurchaseConsumptionDAO)
		getter := getter{
			Cache: cacheMock,
			DAO:   daoMock,
		}
		userID := "test-user"
		productID := "pagliacci-bits-sku"
		singlePurchase := dynamo.SinglePurchaseConsumption{
			TwitchUserID: userID,
			ProductID:    productID,
		}
		ctx := context.Background()
		cacheMock.On("Set", mock.Anything, userID, productID, &singlePurchase).Return()

		Convey("when cache returns value", func() {
			data := cache.Data{
				Body: &singlePurchase,
			}
			cacheMock.On("Get", mock.Anything, userID, productID).Return(&data).Once()

			result := getter.Get(ctx, userID, productID)
			if result == nil {
				t.Fail()
			} else {
				So(result.ProductID, ShouldEqual, productID)
			}
		})

		Convey("when cache returns empty", func() {
			cacheMock.On("Get", mock.Anything, userID, productID).Return(nil).Once()
			Convey("when dao errors", func() {
				daoMock.On("Get", ctx, userID, productID).Return(nil, errors.New("ouch")).Once()

				result := getter.Get(ctx, userID, productID)
				So(result, ShouldBeNil)
			})

			Convey("when dao succeeds", func() {
				daoMock.On("Get", ctx, userID, productID).Return(&singlePurchase, nil).Once()

				result := getter.Get(ctx, userID, productID)
				if result == nil {
					t.Fail()
				} else {
					So(result.ProductID, ShouldEqual, productID)
				}
			})
		})
	})
}
