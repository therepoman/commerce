package cache

import (
	"context"
	"encoding/json"
	"errors"
	"testing"

	dynamo "code.justin.tv/commerce/payday/dynamo/single_purchase_consumption"
	redis_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/redis"
	"github.com/go-redis/redis"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestCache_Set(t *testing.T) {
	Convey("Given single purchase cache set", t, func() {
		ctx := context.Background()
		redisMock := new(redis_mock.Client)
		cache := cache{
			RedisClient: redisMock,
		}
		userID := "test-user"
		productID := "pagliacci-bits"
		singlePurchase := dynamo.SinglePurchaseConsumption{
			TwitchUserID: userID,
			ProductID:    productID,
		}
		data := Data{
			Body: &singlePurchase,
		}
		jsonInput, _ := json.Marshal(&data)

		redisMock.On("Expire", mock.Anything, mock.Anything, mock.Anything).Return(nil)

		Convey("when redis errors", func() {
			resp := redis.NewBoolCmd(errors.New("e"))
			redisMock.On("HSet", mock.Anything, userKey(userID), productID, string(jsonInput)).Return(resp).Once()

			cache.Set(ctx, userID, productID, &singlePurchase)
			redisMock.AssertCalled(t, "HSet", mock.Anything, userKey(userID), productID, string(jsonInput))
		})

		Convey("when redis succeeds", func() {
			resp := redis.NewBoolCmd(nil)
			redisMock.On("HSet", mock.Anything, userKey(userID), productID, string(jsonInput)).Return(resp).Once()

			cache.Set(ctx, userID, productID, &singlePurchase)
			redisMock.AssertCalled(t, "HSet", mock.Anything, userKey(userID), productID, string(jsonInput))
		})
	})
}

func TestCache_Get(t *testing.T) {
	Convey("Given single purchase cache get", t, func() {
		ctx := context.Background()
		redisMock := new(redis_mock.Client)
		cache := cache{
			RedisClient: redisMock,
		}
		userID := "test-user"
		productID := "pagliacci-bits"

		Convey("when redis errors", func() {
			resp := redis.NewStringResult("", errors.New("e"))
			redisMock.On("HGet", mock.Anything, userKey(userID), productID).Return(resp).Once()

			result := cache.Get(ctx, userID, productID)
			So(result, ShouldBeNil)
		})

		Convey("when redis returns Nil", func() {
			resp := redis.NewStringResult("", redis.Nil)
			redisMock.On("HGet", mock.Anything, userKey(userID), productID).Return(resp).Once()

			result := cache.Get(ctx, userID, productID)
			So(result, ShouldBeNil)
		})

		Convey("when redis returns value with body content", func() {
			singlePurchase := dynamo.SinglePurchaseConsumption{
				TwitchUserID: userID,
				ProductID:    productID,
			}
			data := Data{
				Body: &singlePurchase,
			}
			jsonOutput, _ := json.Marshal(&data)
			resp := redis.NewStringResult(string(jsonOutput), nil)
			redisMock.On("HGet", mock.Anything, userKey(userID), productID).Return(resp).Once()

			result := cache.Get(ctx, userID, productID)
			if result == nil || result.Body == nil {
				t.Fail()
			} else {
				So(result.Body.ProductID, ShouldEqual, productID)
			}
		})

		Convey("when redis returns value with body content empty", func() {
			data := Data{
				Body: nil,
			}
			jsonOutput, _ := json.Marshal(&data)
			resp := redis.NewStringResult(string(jsonOutput), nil)
			redisMock.On("HGet", mock.Anything, userKey(userID), productID).Return(resp).Once()

			result := cache.Get(ctx, userID, productID)
			if result == nil {
				t.Fail()
			} else {
				So(result.Body, ShouldBeNil)
			}
		})
	})
}

func TestCache_Del(t *testing.T) {
	Convey("Given single purchase cache del", t, func() {
		ctx := context.Background()
		redisMock := new(redis_mock.Client)
		cache := cache{
			RedisClient: redisMock,
		}
		userID := "test-user"

		Convey("when redis errors", func() {
			redisMock.On("Del", mock.Anything, userKey(userID)).Return(redis.NewIntCmd(errors.New("e")))

			cache.Del(ctx, userID)
			redisMock.AssertCalled(t, "Del", mock.Anything, userKey(userID))
		})

		Convey("when redis succeeds", func() {
			redisMock.On("Del", mock.Anything, userKey(userID)).Return(redis.NewIntCmd(nil))

			cache.Del(ctx, userID)
			redisMock.AssertCalled(t, "Del", mock.Anything, userKey(userID))
		})
	})
}
