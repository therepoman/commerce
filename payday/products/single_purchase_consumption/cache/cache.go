package cache

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/clients/redis"
	dynamo "code.justin.tv/commerce/payday/dynamo/single_purchase_consumption"
	"code.justin.tv/commerce/payday/hystrix"
	cmds "code.justin.tv/commerce/payday/hystrix"
	"code.justin.tv/commerce/payday/utils/strings"
	go_redis "github.com/go-redis/redis"
)

const (
	userKeyFormat = "single-purchase-%s"
	ttl           = time.Minute * 5
)

type Data struct {
	Body *dynamo.SinglePurchaseConsumption
}

type Cache interface {
	Set(ctx context.Context, userID, productID string, singlePurchase *dynamo.SinglePurchaseConsumption)
	Get(ctx context.Context, userID, productID string) *Data
	Del(ctx context.Context, userID string)
}

type cache struct {
	RedisClient redis.Client `inject:"redisClient"`
}

func NewCache() Cache {
	return &cache{}
}

func (c *cache) Set(ctx context.Context, userID, productID string, purchase *dynamo.SinglePurchaseConsumption) {
	logger := log.WithFields(log.Fields{
		"TwitchUserID": userID,
		"ProductID":    productID,
	})

	data := &Data{
		Body: purchase,
	}

	purchaseJson, err := json.Marshal(data)
	if err != nil {
		logger.WithError(err).Error("Failed to marshal single purchase consumption to cache")
		return
	}

	err = hystrix.Do(cmds.ProductsSinglePurchaseCacheSet, func() error {
		err = c.RedisClient.HSet(ctx, userKey(userID), productID, string(purchaseJson)).Err()
		if err != nil {
			logger.WithError(err).Error("Failed to cache single purchase consumption")
		} else {
			c.RedisClient.Expire(ctx, userKey(userID), ttl)
		}
		return err
	}, nil)
	if err != nil {
		logger.WithError(err).Warn("hystrix error setting single purchase in cache")
	}
}

func (c *cache) Get(ctx context.Context, userID, productID string) *Data {
	logger := log.WithFields(log.Fields{
		"TwitchUserID": userID,
		"ProductID":    productID,
	})
	var cachedValue string

	err := hystrix.Do(cmds.ProductsSinglePurchaseCacheGet, func() error {
		var err error
		cachedValue, err = c.RedisClient.HGet(ctx, userKey(userID), productID).Result()
		if err != nil && err != go_redis.Nil {
			logger.WithError(err).Error("Failed to get cached single purchase consumption")
			return err
		}
		return nil
	}, nil)
	if err != nil {
		logger.WithError(err).Warn("hystrix error getting single purchase")
	}

	if strings.Blank(cachedValue) {
		return nil
	}

	var singlePurchase Data
	err = json.Unmarshal([]byte(cachedValue), &singlePurchase)
	if err != nil {
		logger.WithError(err).Error("Failed to unmarshal cached single purchase consumption")
		c.del(ctx, userID, productID)
		return nil
	}

	return &singlePurchase
}

func (c *cache) del(ctx context.Context, userID, productID string) {
	_ = c.RedisClient.HDel(ctx, userKey(userID), productID).Err()
}

func (c *cache) Del(ctx context.Context, userID string) {
	_ = c.RedisClient.Del(ctx, userKey(userID))
}

func userKey(userID string) string {
	return fmt.Sprintf(userKeyFormat, userID)
}
