package products

import (
	"context"

	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/products/availability"
	platform_getter "code.justin.tv/commerce/payday/products/purchase_by_most_recent_super_platform/getter"
	petozi "code.justin.tv/commerce/petozi/rpc"
)

type Fetcher interface {
	FetchAllFromPetozi(ctx context.Context) ([]*petozi.BitsProduct, error)
	Fetch(ctx context.Context, tuid string) (map[string][]petozi.BitsProduct, models.Platform, error)
}

func NewFetcher() Fetcher {
	return &fetcher{}
}

type fetcher struct {
	PetoziClient       petozi.Petozi                           `inject:""`
	EligibilityChecker availability.ProductAvailabilityChecker `inject:""`
	PlatformGetter     platform_getter.Getter                  `inject:""`
}

func (f *fetcher) FetchAllFromPetozi(ctx context.Context) ([]*petozi.BitsProduct, error) {
	productsResp, err := f.PetoziClient.GetBitsProducts(ctx, &petozi.GetBitsProductsReq{
		UseInMemoryCache: true,
	})
	if err != nil || productsResp == nil {
		return nil, err
	}

	return productsResp.BitsProducts, nil
}

func (f *fetcher) Fetch(ctx context.Context, tuid string) (map[string][]petozi.BitsProduct, models.Platform, error) {
	return f.getPetoziProductEligibility(ctx, tuid)
}

func (f *fetcher) getPetoziProductEligibility(ctx context.Context, tuid string) (map[string][]petozi.BitsProduct, models.Platform, error) {
	allProducts, err := f.FetchAllFromPetozi(ctx)
	if err != nil {
		return nil, models.DEFAULT, err
	}
	purchasableQuantities := f.EligibilityChecker.GetPurchasableQuantities(ctx, tuid)

	eligibleProducts := make(map[string][]petozi.BitsProduct)
	for _, product := range allProducts {
		purchasableQuantity := purchasableQuantities[product.Id]
		if purchasableQuantity > 0 {
			product.MaxPurchasableQuantity = uint32(purchasableQuantity)
			eligibleProducts[product.Id] = append(eligibleProducts[product.Id], *product)
		}
	}

	return eligibleProducts, f.PlatformGetter.Get(ctx, tuid), nil
}

func GetProductForOfferID(offerId string, bitsProducts []*petozi.BitsProduct) (*petozi.BitsProduct, map[petozi.Platform]bool) {
	var lastProduct *petozi.BitsProduct
	availablePlatforms := map[petozi.Platform]bool{}
	for _, product := range bitsProducts {
		if product != nil && product.OfferId == offerId {
			availablePlatforms[product.Platform] = true
			lastProduct = product
		}
	}

	return lastProduct, availablePlatforms
}
