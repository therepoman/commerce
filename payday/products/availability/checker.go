package availability

import (
	"context"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/clients/nitro"
	"code.justin.tv/commerce/payday/clients/pachinko"
	"code.justin.tv/commerce/payday/metrics"
	entitlements_getter "code.justin.tv/commerce/payday/products/entitlements/getter"
	single_purchase_getter "code.justin.tv/commerce/payday/products/single_purchase_consumption/getter"
	petozi "code.justin.tv/commerce/petozi/rpc"
	"github.com/golang/protobuf/ptypes"
)

const (
	// This is how many Bits a user could have up to
	maxBitsInventoryThreshold = 800000

	// Daily purchase limit is $9,999, and we convert it into Bits amount
	// using the worst price of 100 Bits for $1.4
	dailyBitsPurchaseLimitInBits = 100 * 9999 / 1.4
)

type ProductAvailabilityChecker interface {
	GetPurchasableQuantities(ctx context.Context, userID string) map[string]int
	GetPurchasableQuantityByInventoryLimit(ctx context.Context, product *petozi.BitsProduct, userID string) (int, error)
	GetPurchasableByDailyPurchaseLimit(ctx context.Context, product *petozi.BitsProduct, userID string) (int, error)
}

type availabilityChecker struct {
	PetoziClient           petozi.Petozi                 `inject:""`
	SinglePurchaseGetter   single_purchase_getter.Getter `inject:""`
	BitsEntitlementsGetter entitlements_getter.Getter    `inject:""`
	NitroClient            nitro.NitroClient             `inject:""`
	PachinkoClient         pachinko.PachinkoClient       `inject:""`
	Statter                metrics.Statter               `inject:""`
}

func NewProductAvailabilityChecker() ProductAvailabilityChecker {
	return &availabilityChecker{}
}

func (ac *availabilityChecker) GetPurchasableQuantities(ctx context.Context, userID string) map[string]int {
	errChan := make(chan error)
	var userBalance int
	balanceChan := make(chan int, 1)
	var products []*petozi.BitsProduct
	productsChan := make(chan []*petozi.BitsProduct, 1)
	var purchasedBitsAmountLast24Hours int
	past24HoursChan := make(chan int, 1)

	go func() {
		start := time.Now()
		purchasedBitsAmountLast24Hours, err := ac.getLast24HoursPurchases(ctx, userID)
		if err != nil {
			log.WithError(err).Error("Failed to get past 24 hours of activity")
			errChan <- err
		} else {
			past24HoursChan <- purchasedBitsAmountLast24Hours
			ac.Statter.TimingDuration("availabilityChecker.last24hourspurchases", time.Since(start))
		}
	}()

	go func() {
		start := time.Now()
		productsResp, err := ac.PetoziClient.GetBitsProducts(ctx, &petozi.GetBitsProductsReq{
			UseInMemoryCache: true,
		})
		if err != nil {
			log.WithError(err).Error("Failed to get bits products")
			errChan <- err
		} else {
			productsChan <- productsResp.BitsProducts
			ac.Statter.TimingDuration("availabilityChecker.petoziProducts", time.Since(start))
		}
	}()

	go func() {
		start := time.Now()
		balance, err := ac.PachinkoClient.GetBalance(ctx, userID)
		if err != nil {
			log.WithError(err).Error("Failed to get pachinko balance")
			errChan <- err
		} else {
			balanceChan <- balance
			ac.Statter.TimingDuration("availabilityChecker.petoziProducts", time.Since(start))

		}
	}()

	select {
	case err := <-errChan:
		log.WithError(err).Error("Failed to get necessary components")
		return nil
	case balance := <-balanceChan:
		userBalance = balance
	case <-ctx.Done():
		return nil
	}

	select {
	case err := <-errChan:
		log.WithError(err).Error("Failed to get necessary components")
		return nil
	case productsResp := <-productsChan:
		products = productsResp
	case <-ctx.Done():
		return nil
	}

	select {
	case err := <-errChan:
		log.WithError(err).Error("Failed to get necessary components")
		return nil
	case past24Hours := <-past24HoursChan:
		purchasedBitsAmountLast24Hours = past24Hours
	case <-ctx.Done():
		return nil
	}

	purchasableQuantities := make(map[string]int)
	for _, product := range products {
		if _, ok := purchasableQuantities[product.Id]; !ok {
			purchasableQuantities[product.Id] = ac.getPurchasableQuantity(ctx, userID, userBalance, purchasedBitsAmountLast24Hours, product)
		}
	}

	return purchasableQuantities
}

func (ac *availabilityChecker) getPurchasableQuantity(ctx context.Context, userID string, balance, purchasedBitsAmountLast24Hours int, product *petozi.BitsProduct) int {
	// if promo, check promo sku availability
	if product.Promo != nil {
		return ac.getPromoAvailability(ctx, product, userID)
	}

	// inventory limit (800,000 bits)
	purchasableByInventoryLimit, err := ac.getPurchasableQuantityByInventoryLimit(product, balance)
	if err != nil || purchasableByInventoryLimit < 1 {
		return 0
	}

	// daily purchase limit $9,999
	purchasableByDailyPurchaseLimit := ac.getPurchasableByDailyPurchaseLimit(product, purchasedBitsAmountLast24Hours)
	if purchasableByDailyPurchaseLimit < 1 {
		return 0
	}

	purchasableQuantity := int(product.MaxPurchasableQuantity)
	if purchasableByInventoryLimit < purchasableQuantity {
		purchasableQuantity = purchasableByInventoryLimit
	}
	if purchasableByDailyPurchaseLimit < purchasableQuantity {
		purchasableQuantity = purchasableByDailyPurchaseLimit
	}

	return purchasableQuantity
}

func (ac *availabilityChecker) getPromoAvailability(ctx context.Context, product *petozi.BitsProduct, userID string) (purchasableQuantity int) {
	if product.Promo.Start == nil {
		return
	}

	startTime, err := ptypes.Timestamp(product.Promo.Start)
	if err != nil {
		return
	}

	if time.Now().Before(startTime) {
		return
	}

	if product.Promo.End != nil {
		endTime, err := ptypes.Timestamp(product.Promo.End)
		if err != nil {
			return
		}

		if endTime.Before(startTime) {
			return
		}

		if time.Now().After(endTime) {
			return
		}
	}

	start := time.Now()
	switch product.Promo.Type {
	case petozi.PromoType_SINGLE_PURCHASE:
		purchase := ac.SinglePurchaseGetter.Get(ctx, userID, product.Id)
		if purchase == nil {
			purchasableQuantity = 1
		}
		ac.Statter.TimingDuration("availabilityChecker.promo.singlePurchaseCheck", time.Since(start))
	case petozi.PromoType_FIRST_TIME_PURCHASE:
		bitsEntitlement := ac.BitsEntitlementsGetter.GetMostRecent(ctx, userID)
		if bitsEntitlement == nil {
			purchasableQuantity = 1
		}
		ac.Statter.TimingDuration("availabilityChecker.promo.firstTimePurchaseCheck", time.Since(start))
	case petozi.PromoType_PRIME_SINGLE_PURCHASE:
		primeSinglePurchase := ac.SinglePurchaseGetter.Get(ctx, userID, product.Id)
		if primeSinglePurchase != nil {
			return
		}
		hasPrime, err := ac.NitroClient.GetPrimeStatus(ctx, userID)
		if err == nil && hasPrime {
			purchasableQuantity = 1
		}
		ac.Statter.TimingDuration("availabilityChecker.promo.primeStatusCheck", time.Since(start))
	}
	return
}

func (ac *availabilityChecker) GetPurchasableQuantityByInventoryLimit(ctx context.Context, product *petozi.BitsProduct, userID string) (int, error) {
	balance, err := ac.PachinkoClient.GetBalance(ctx, userID)
	if err != nil {
		return 0, err
	}

	return ac.getPurchasableQuantityByInventoryLimit(product, balance)
}

func (ac *availabilityChecker) getPurchasableQuantityByInventoryLimit(product *petozi.BitsProduct, balance int) (int, error) {
	return (maxBitsInventoryThreshold - balance) / int(product.Quantity), nil
}

func (ac *availabilityChecker) GetPurchasableByDailyPurchaseLimit(ctx context.Context, product *petozi.BitsProduct, userID string) (int, error) {
	purchasedBitsAmountLast24Hours, err := ac.getLast24HoursPurchases(ctx, userID)
	if err != nil {
		return 0, err
	}
	return ac.getPurchasableByDailyPurchaseLimit(product, int(purchasedBitsAmountLast24Hours)), nil
}

func (ac *availabilityChecker) getPurchasableByDailyPurchaseLimit(product *petozi.BitsProduct, purchasedBitsAmountLast24Hours int) int {
	dailyPurchaseLimit := dailyBitsPurchaseLimitInBits
	return (int(dailyPurchaseLimit) - int(purchasedBitsAmountLast24Hours)) / int(product.Quantity)
}

func (ac *availabilityChecker) getLast24HoursPurchases(ctx context.Context, userID string) (int, error) {
	acquiredInLast24Hours, err := ac.BitsEntitlementsGetter.GetEntitlementsLast24Hours(ctx, userID)
	if err != nil {
		return 0, err
	}

	return int(acquiredInLast24Hours), nil
}
