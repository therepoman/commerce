package products

import (
	"context"
	"net/http"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/api/utils"
	"code.justin.tv/commerce/payday/datascience"
	httperror "code.justin.tv/commerce/payday/errors/http"
	"code.justin.tv/commerce/payday/metrics"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/user"
	"code.justin.tv/commerce/payday/userservice"
)

const (
	getProductsBannedUserIDMetric       = "GetProducts_BadRequest_BannedUserID"
	getProductsUserServiceErrorMetric   = "GetProducts_BadRequest_UserServiceError"
	getProductsNoUserIDMetric           = "GetProducts_BadRequest_NoUserID"
	getProductsNoLocaleMetric           = "GetProducts_BadRequest_NoLocale"
	getProductsNoCountryMetric          = "GetProducts_BadRequest_NoCountry"
	getProductsUnsupportedCountryMetric = "GetProducts_BadRequest_UnsupportedLocale"
)

type Validator interface {
	Validate(ctx context.Context, tuid string, locale string, country string, platform models.Platform) (error, bool)
}

type validator struct {
	Statter            metrics.Statter         `inject:""`
	DataScienceClient  datascience.DataScience `inject:""`
	Checker            Checker                 `inject:""`
	UserServiceChecker userservice.Checker     `inject:""`
	UserChecker        user.Checker            `inject:""`
}

func NewValidator() Validator {
	return &validator{}
}

func (v *validator) Validate(ctx context.Context, tuid string, locale string, country string, platform models.Platform) (error, bool) {
	if tuid == "" {
		v.Statter.Inc(getProductsNoUserIDMetric, 1)
		return httperror.New("user id is required", http.StatusBadRequest), false
	} else if tuid != utils.DefaultTUID {
		userBanned, err := v.UserServiceChecker.IsBanned(ctx, tuid)
		if err != nil {
			v.Statter.Inc(getProductsUserServiceErrorMetric, 1)
			log.WithError(err).Error("User service had error retrieving twitch userRecord.")
			return httperror.New("issue fetching user banned status from user service", http.StatusInternalServerError), false
		}

		isBitsEligible, err := v.UserChecker.IsEligible(ctx, tuid)
		if err != nil {
			log.Error("Error getting user bits eligible status")
			return httperror.New("issue fetching user bits eligible status", http.StatusInternalServerError), false
		}

		if userBanned || !isBitsEligible {
			v.Statter.Inc(getProductsBannedUserIDMetric, 1)
			return nil, true
		}
	}

	if models.IsPlatformMobile(platform) {
		if locale == "" {
			v.Statter.Inc(getProductsNoLocaleMetric, 1)
			return httperror.New("locale is required for mobile products.", http.StatusBadRequest), false
		} else if country == "" {
			v.Statter.Inc(getProductsNoCountryMetric, 1)
			return nil, true
		} else if !v.Checker.IsMobileCountryEnabled(country, platform) {
			v.Statter.Inc(getProductsUnsupportedCountryMetric, 1)
			v.trackUnsupportedCountryMetrics(tuid, country, platform)
			return nil, true
		}
	}

	return nil, false
}

func (v *validator) trackUnsupportedCountryMetrics(tuid string, country string, platform models.Platform) {
	datascienceEvent := &models.MobileCountryUnavailable{
		UserID:        tuid,
		Platform:      string(platform),
		Country:       country,
		FailureReason: "wrong country",
	}

	go func(e *models.MobileCountryUnavailable) {
		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()

		err := v.DataScienceClient.TrackBitsEvent(ctx, datascience.BitsMobileUnavailable, e)
		if err != nil {
			log.WithError(err).Error("Error tracking mobile country unavailable event with data science")
		}
	}(datascienceEvent)
}
