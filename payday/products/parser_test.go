package products

import (
	"testing"

	"code.justin.tv/commerce/gogogadget/pointers"
	products_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/products"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/models/api"
	petozi "code.justin.tv/commerce/petozi/rpc"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestParser_Parse(t *testing.T) {
	Convey("Given a parser", t, func() {
		translator := new(products_mock.Translator)
		sorter := new(products_mock.Sorter)

		parser := &parser{
			Translator: translator,
			Sorter:     sorter,
		}

		platform := models.AMAZON
		locale := "en"
		product := api.Product{
			Id:          "foo",
			MaxQuantity: 1,
			BitsAmount:  322,
			ProductType: pointers.StringP(purchaseProductTypeConstant),
		}
		productsInput := map[string][]petozi.BitsProduct{
			"foo": {
				{
					Platform:               petozi.Platform_AMAZON,
					Quantity:               322,
					MaxPurchasableQuantity: 1,
				},
			},
		}

		translator.On("Translate", mock.Anything, locale).Return(product).Return(nil)

		Convey("When the product is not the correct platform, but they are both not web platforms", func() {
			platform = models.IOS

			sorter.On("Sort", []api.Product{}).Return()

			Convey("Then we do not add the product to the return list", func() {
				output := parser.Parse(platform, locale, productsInput)

				So(output, ShouldHaveLength, 0)
				translator.AssertNotCalled(t, "Translate", mock.Anything, locale)
			})
		})

		Convey("When the product is the correct platform", func() {
			platform = models.AMAZON

			sorter.On("Sort", []api.Product{product}).Return()

			Convey("then we add the product to the return list", func() {
				output := parser.Parse(platform, locale, productsInput)

				So(output, ShouldHaveLength, 1)
				translator.AssertCalled(t, "Translate", mock.Anything, locale)
			})
		})
	})
}
