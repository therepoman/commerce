package products

import (
	"context"
	"testing"

	"code.justin.tv/commerce/payday/api/utils"
	"code.justin.tv/commerce/payday/datascience"
	datascience_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/datascience"
	metrics_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/metrics"
	products_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/products"
	user_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/user"
	userservice_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/userservice"
	"code.justin.tv/commerce/payday/models"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestValidator_Validate(t *testing.T) {
	Convey("Given a validator", t, func() {
		tuid := "123123123"
		locale := "en"
		country := "US"
		platform := models.AMAZON

		checker := new(products_mock.Checker)
		statter := new(metrics_mock.Statter)
		dataScienceClient := new(datascience_mock.DataScience)
		userServiceChecker := new(userservice_mock.Checker)
		userChecker := new(user_mock.Checker)

		validator := &validator{
			Statter:            statter,
			DataScienceClient:  dataScienceClient,
			UserServiceChecker: userServiceChecker,
			Checker:            checker,
			UserChecker:        userChecker,
		}

		ctx := context.Background()

		Convey("When the tuid is empty", func() {
			tuid = ""
			statter.On("Inc", getProductsNoUserIDMetric, int64(1)).Return()

			Convey("we should post a metric and return an error", func() {
				err, returnEmpty := validator.Validate(ctx, tuid, locale, country, platform)

				So(err, ShouldNotBeNil)
				So(returnEmpty, ShouldBeFalse)
				statter.AssertCalled(t, "Inc", getProductsNoUserIDMetric, int64(1))
			})
		})

		Convey("When the tuid is the default tuid", func() {
			tuid = utils.DefaultTUID

			Convey("When the platform is mobile", func() {
				platform = models.IOS

				Convey("When the locale is blank", func() {
					locale = ""
					statter.On("Inc", getProductsNoLocaleMetric, int64(1)).Return()

					Convey("we should post a metric and return an error", func() {
						err, returnEmpty := validator.Validate(ctx, tuid, locale, country, platform)

						So(err, ShouldNotBeNil)
						So(returnEmpty, ShouldBeFalse)
						statter.AssertCalled(t, "Inc", getProductsNoLocaleMetric, int64(1))
					})
				})

				Convey("When the country is blank", func() {
					country = ""
					statter.On("Inc", getProductsNoCountryMetric, int64(1)).Return()

					Convey("we should post a metric and not return an error", func() {
						err, returnEmpty := validator.Validate(ctx, tuid, locale, country, platform)

						So(err, ShouldBeNil)
						So(returnEmpty, ShouldBeTrue)
						statter.AssertCalled(t, "Inc", getProductsNoCountryMetric, int64(1))
					})
				})

				Convey("When the country is not enabled for mobile", func() {
					checker.On("IsMobileCountryEnabled", country, platform).Return(false)
					statter.On("Inc", getProductsUnsupportedCountryMetric, int64(1)).Return()
					dataScienceClient.On("TrackBitsEvent", mock.Anything, datascience.BitsMobileUnavailable, mock.Anything).Return(nil)

					Convey("we should post a metric, post datascience, and not return an error", func() {
						err, returnEmpty := validator.Validate(ctx, tuid, locale, country, platform)

						So(err, ShouldBeNil)
						So(returnEmpty, ShouldBeTrue)
						statter.AssertCalled(t, "Inc", getProductsUnsupportedCountryMetric, int64(1))
					})
				})

				Convey("When the country is enabled for mobile", func() {
					checker.On("IsMobileCountryEnabled", country, platform).Return(true)

					Convey("we should return no error and non empty response body", func() {
						err, returnEmpty := validator.Validate(ctx, tuid, locale, country, platform)

						So(err, ShouldBeNil)
						So(returnEmpty, ShouldBeFalse)
					})
				})
			})

			Convey("When the platform is not mobile", func() {
				platform = models.AMAZON

				Convey("we should return no error and non empty response body", func() {
					err, returnEmpty := validator.Validate(ctx, tuid, locale, country, platform)

					So(err, ShouldBeNil)
					So(returnEmpty, ShouldBeFalse)
				})
			})
		})

		Convey("When the tuid is non empty", func() {
			tuid = "123123123"

			Convey("When there is an error fetching the user record from user service", func() {
				userServiceChecker.On("IsBanned", mock.Anything, tuid).Return(false, errors.New("WALRUS STRIKE"))
				statter.On("Inc", getProductsUserServiceErrorMetric, int64(1)).Return()

				Convey("we should post a metric and return an error", func() {
					err, returnEmpty := validator.Validate(ctx, tuid, locale, country, platform)
					So(err, ShouldNotBeNil)
					So(returnEmpty, ShouldBeFalse)
					statter.AssertCalled(t, "Inc", getProductsUserServiceErrorMetric, int64(1))
				})
			})

			Convey("When the user is banned in user service", func() {
				userServiceChecker.On("IsBanned", mock.Anything, tuid).Return(true, nil)
				userChecker.On("IsEligible", mock.Anything, tuid).Return(true, nil)
				statter.On("Inc", getProductsBannedUserIDMetric, int64(1)).Return()

				Convey("we should post a metric, return an empty response, and not return an error", func() {
					err, returnEmpty := validator.Validate(ctx, tuid, locale, country, platform)

					So(err, ShouldBeNil)
					So(returnEmpty, ShouldBeTrue)
					statter.AssertCalled(t, "Inc", getProductsBannedUserIDMetric, int64(1))
				})

			})

			Convey("When the user is bits ineligible", func() {
				userServiceChecker.On("IsBanned", mock.Anything, tuid).Return(false, nil)
				userChecker.On("IsEligible", mock.Anything, tuid).Return(false, nil)
				statter.On("Inc", getProductsBannedUserIDMetric, int64(1)).Return()

				Convey("we should post a metric, return an empty response, and not return an error", func() {
					err, returnEmpty := validator.Validate(ctx, tuid, locale, country, platform)

					So(err, ShouldBeNil)
					So(returnEmpty, ShouldBeTrue)
					statter.AssertCalled(t, "Inc", getProductsBannedUserIDMetric, int64(1))
				})

			})

			Convey("when the user is not banned and is bits eligible", func() {
				userServiceChecker.On("IsBanned", mock.Anything, tuid).Return(false, nil)
				userChecker.On("IsEligible", mock.Anything, tuid).Return(true, nil)

				Convey("When the platform is mobile", func() {
					platform = models.IOS

					Convey("When the locale is blank", func() {
						locale = ""
						statter.On("Inc", getProductsNoLocaleMetric, int64(1)).Return()

						Convey("we should post a metric and return an error", func() {
							err, returnEmpty := validator.Validate(ctx, tuid, locale, country, platform)

							So(err, ShouldNotBeNil)
							So(returnEmpty, ShouldBeFalse)
							statter.AssertCalled(t, "Inc", getProductsNoLocaleMetric, int64(1))
						})
					})

					Convey("When the country is blank", func() {
						country = ""
						statter.On("Inc", getProductsNoCountryMetric, int64(1)).Return()

						Convey("we should post a metric and not return an error", func() {
							err, returnEmpty := validator.Validate(ctx, tuid, locale, country, platform)

							So(err, ShouldBeNil)
							So(returnEmpty, ShouldBeTrue)
							statter.AssertCalled(t, "Inc", getProductsNoCountryMetric, int64(1))
						})
					})

					Convey("When the country is not enabled for mobile", func() {
						checker.On("IsMobileCountryEnabled", country, platform).Return(false)
						statter.On("Inc", getProductsUnsupportedCountryMetric, int64(1)).Return()
						dataScienceClient.On("TrackBitsEvent", mock.Anything, datascience.BitsMobileUnavailable, mock.Anything).Return(nil)

						Convey("we should post a metric, post datascience, and not return an error", func() {
							err, returnEmpty := validator.Validate(ctx, tuid, locale, country, platform)

							So(err, ShouldBeNil)
							So(returnEmpty, ShouldBeTrue)
							statter.AssertCalled(t, "Inc", getProductsUnsupportedCountryMetric, int64(1))
						})
					})

					Convey("When the country is enabled for mobile", func() {
						checker.On("IsMobileCountryEnabled", country, platform).Return(true)

						Convey("we should return no error and non empty response body", func() {
							err, returnEmpty := validator.Validate(ctx, tuid, locale, country, platform)

							So(err, ShouldBeNil)
							So(returnEmpty, ShouldBeFalse)
						})
					})
				})

				Convey("When the platform is not mobile", func() {
					platform = models.AMAZON

					Convey("we should return no error and non empty response body", func() {
						err, returnEmpty := validator.Validate(ctx, tuid, locale, country, platform)

						So(err, ShouldBeNil)
						So(returnEmpty, ShouldBeFalse)
					})
				})
			})
		})
	})
}
