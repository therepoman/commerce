package products

import (
	"context"

	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/models/api"
	"code.justin.tv/commerce/payday/utils/new_sku_experiment_gating"
	petozi "code.justin.tv/commerce/petozi/rpc"
)

type Getter interface {
	GetByIDs(ctx context.Context, platform petozi.Platform, productIDs []string) (map[string]*petozi.BitsProduct, error)
	Get(ctx context.Context, tuid string, locale string, platform models.Platform) ([]api.Product, error)
}

func NewGetter() Getter {
	return &getter{}
}

type getter struct {
	Fetcher Fetcher               `inject:""`
	Parser  Parser                `inject:""`
	Config  *config.Configuration `inject:""`
}

func (g *getter) Get(ctx context.Context, tuid string, locale string, platform models.Platform) ([]api.Product, error) {
	response, mostRecentPlatform, err := g.Fetcher.Fetch(ctx, tuid)
	if err != nil {
		return nil, err
	}

	// there are two First time purchase SKUs, we need to delete the old one (if it's there) for the time being
	// until we can remove or disable the old entry from the catalog.
	delete(response, new_sku_experiment_gating.OldFirstTimePurchaseSKU)

	return g.Parser.Parse(g.resolvePlatform(platform, mostRecentPlatform), locale, response), nil
}

func (g *getter) resolvePlatform(requestedPlatform, mostRecentPlatform models.Platform) models.Platform {
	if requestedPlatform != models.DEFAULT {
		return requestedPlatform
	}

	if mostRecentPlatform != models.DEFAULT {
		return mostRecentPlatform
	}

	return models.PAYPAL
}

func (g *getter) GetByIDs(ctx context.Context, platform petozi.Platform, productIDs []string) (map[string]*petozi.BitsProduct, error) {
	allProducts, err := g.Fetcher.FetchAllFromPetozi(ctx)
	if err != nil || allProducts == nil {
		return nil, err
	}

	products := map[string]*petozi.BitsProduct{}
	for _, p := range allProducts {
		if p.Platform == platform {
			for _, reqId := range productIDs {
				if reqId == p.Id {
					products[p.Id] = p
				}
			}
		}
	}

	return products, nil
}
