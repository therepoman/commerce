package products

import (
	"strings"

	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/models/api"
	petozi "code.justin.tv/commerce/petozi/rpc"
)

const (
	purchaseProductTypeConstant = "purchased"
)

type Parser interface {
	Parse(platform models.Platform, locale string, productInformation map[string][]petozi.BitsProduct) []api.Product
}

func NewParser() Parser {
	return &parser{}
}

type parser struct {
	Sorter     Sorter     `inject:""`
	Translator Translator `inject:""`
}

func (p *parser) Parse(platform models.Platform, locale string, productInformation map[string][]petozi.BitsProduct) []api.Product {
	response := make([]api.Product, 0)

	for productId, productInfoWithVariousPlatforms := range productInformation {
		for _, productInfoByPlatform := range productInfoWithVariousPlatforms {
			if isTargetPlatform(platform, productInfoByPlatform) {
				maxQuantity := int32(productInfoByPlatform.MaxPurchasableQuantity)
				product := api.Product{
					Id:                     productId,
					BitsAmount:             int(productInfoByPlatform.Quantity),
					Promo:                  productInfoByPlatform.Promo != nil,
					PromoType:              promoType(productInfoByPlatform),
					PromoId:                promoID(productInfoByPlatform),
					ProductType:            pointers.StringP(purchaseProductTypeConstant),
					MaxQuantity:            pointers.Int32OrDefault(&maxQuantity, 0),
					OfferId:                offerID(productInfoByPlatform),
					RestrictedToCurrencies: productInfoByPlatform.RestrictedToCurrencies,
				}

				//TODO: remove after we've integrated with translation sources
				product.LocalizedTitle = p.Translator.Translate(productInfoByPlatform, locale)

				response = append(response, product)
			}
		}
	}

	p.Sorter.Sort(response)

	return response
}

func isTargetPlatform(platformRequested models.Platform, productActual petozi.BitsProduct) bool {
	return platformRequested == models.ParsePlatform(productActual.Platform.String(), pointers.StringP("IsTargetPlatform"), nil, pointers.StringP(productActual.DisplayName))
}

func promoType(product petozi.BitsProduct) *string {
	if product.Promo == nil {
		return nil
	}

	promoType := strings.ToLower(product.Promo.Type.String())
	return &promoType
}

func promoID(product petozi.BitsProduct) *string {
	if product.Promo == nil {
		return nil
	}

	return &product.Promo.Id
}

func offerID(product petozi.BitsProduct) *string {
	if product.OfferId == "" {
		return nil
	}
	return &product.OfferId
}
