package getter

import (
	"context"
	"errors"
	"testing"
	"time"

	dynamo "code.justin.tv/commerce/payday/dynamo/bits_entitlement"
	cache_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/cache"
	bits_entitlement_dao_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo/bits_entitlement"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestGetter_GetMostRecent(t *testing.T) {
	Convey("Given Getter", t, func() {
		daoMock := new(bits_entitlement_dao_mock.BitsEntitlementDAO)
		cacheMock := new(cache_mock.MostRecentlyAcquiredCache)

		getter := getter{
			DAO:                       daoMock,
			MostRecentlyAcquiredCache: cacheMock,
		}
		userID := "test-user"
		entitlement := dynamo.BitsEntitlement{
			TwitchUserID: userID,
			ProductID:    "pagliacci-bits",
		}

		ctx := context.Background()

		cacheMock.On("Set", mock.Anything, userID, &entitlement).Return()
		Convey("when cache return value", func() {
			cacheMock.On("Get", mock.Anything, userID).Return(&entitlement, false).Once()

			result := getter.GetMostRecent(ctx, userID)
			So(result.ProductID, ShouldEqual, entitlement.ProductID)
		})

		Convey("when cache is empty", func() {
			cacheMock.On("Get", mock.Anything, userID).Return(nil, false).Once()

			Convey("when dao errors", func() {
				daoMock.On("GetMostRecent", ctx, userID).Return(nil, errors.New("oops")).Once()

				result := getter.GetMostRecent(ctx, userID)
				So(result, ShouldBeNil)
			})

			Convey("when dao succeeds", func() {
				daoMock.On("GetMostRecent", ctx, userID).Return(&entitlement, nil).Once()

				result := *getter.GetMostRecent(ctx, userID)
				So(result.ProductID, ShouldEqual, entitlement.ProductID)
			})
		})
	})
}

func TestGetter_GetEntitlementsLast24Hours(t *testing.T) {
	Convey("Given Getter", t, func() {
		daoMock := new(bits_entitlement_dao_mock.BitsEntitlementDAO)
		cacheMock := new(cache_mock.Last24HoursAcquiredCache)

		getter := getter{
			DAO:                      daoMock,
			Last24HoursAcquiredCache: cacheMock,
		}
		userID := "test-user"
		entitlements := []dynamo.BitsEntitlement{
			{
				TwitchUserID: userID,
				ProductID:    "pagliacci-bits",
				DateTime:     time.Now().AddDate(-1, 0, 0),
				Amount:       44,
			},
			{
				TwitchUserID: userID,
				ProductID:    "dominos-bits",
				DateTime:     time.Now(),
				Amount:       1,
			},
		}
		ctx := context.Background()

		Convey("when cache returns value", func() {
			Convey("of number greater than zero", func() {
				bitsIn24Hours := int64(56)
				cacheMock.On("Get", mock.Anything, userID).Return(&bitsIn24Hours).Once()

				bitsAcquiredInPast24Hours, _ := getter.GetEntitlementsLast24Hours(ctx, userID)
				So(bitsAcquiredInPast24Hours, ShouldEqual, bitsIn24Hours)
			})
			Convey("of zero", func() {
				bitsIn24Hours := int64(0)
				cacheMock.On("Get", mock.Anything, userID).Return(&bitsIn24Hours).Once()

				bitsAcquiredInPast24Hours, _ := getter.GetEntitlementsLast24Hours(ctx, userID)
				So(bitsAcquiredInPast24Hours, ShouldEqual, bitsIn24Hours)
			})
		})

		Convey("when it was never cached", func() {
			cacheMock.On("Get", mock.Anything, userID).Return(nil).Once()

			Convey("when dao errors", func() {
				daoMock.On("GetAfter", mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("e")).Once()

				bitsAcquiredInPast24Hours, err := getter.GetEntitlementsLast24Hours(ctx, userID)
				So(bitsAcquiredInPast24Hours, ShouldEqual, int64(0))
				So(err, ShouldNotBeNil)
			})

			Convey("when dao succeeds", func() {
				cacheMock.On("Set", mock.Anything, userID, mock.Anything).Return()
				daoMock.On("GetAfter", mock.Anything, mock.Anything, mock.Anything).Return(entitlements, nil).Once()

				bitsAcquiredInPast24Hours, _ := getter.GetEntitlementsLast24Hours(ctx, userID)
				So(bitsAcquiredInPast24Hours, ShouldEqual, int64(45))
			})
		})
	})
}
