package getter

import (
	"context"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/cache"
	dynamo "code.justin.tv/commerce/payday/dynamo/bits_entitlement"
)

type Getter interface {
	GetMostRecent(ctx context.Context, userID string) *dynamo.BitsEntitlement
	GetEntitlementsLast24Hours(ctx context.Context, userID string) (int64, error)
}

type getter struct {
	MostRecentlyAcquiredCache cache.MostRecentlyAcquiredCache `inject:""`
	Last24HoursAcquiredCache  cache.Last24HoursAcquiredCache  `inject:""`
	DAO                       dynamo.BitsEntitlementDAO       `inject:""`
}

func NewGetter() Getter {
	return &getter{}
}

func (f *getter) GetMostRecent(ctx context.Context, userID string) *dynamo.BitsEntitlement {
	mostRecentAcquisition, isCached := f.MostRecentlyAcquiredCache.Get(ctx, userID)
	if mostRecentAcquisition != nil || isCached {
		return mostRecentAcquisition
	}

	entitlement, err := f.DAO.GetMostRecent(ctx, userID)
	if err != nil {
		log.WithField("user_id", userID).WithError(err).Error("Failed to query dynamo for bits entitlements")
		return nil
	}

	go f.MostRecentlyAcquiredCache.Set(context.Background(), userID, entitlement)

	return entitlement
}

func (f *getter) GetEntitlementsLast24Hours(ctx context.Context, userID string) (int64, error) {
	last24HoursAcquired := f.Last24HoursAcquiredCache.Get(ctx, userID)
	if last24HoursAcquired != nil {
		return *last24HoursAcquired, nil
	}

	yesterday := time.Now().AddDate(0, 0, -1)
	entitlements, err := f.DAO.GetAfter(ctx, userID, yesterday)
	if err != nil {
		log.WithField("user_id", userID).WithError(err).Error("Failed to query dynamo for bits entitlements")
		return 0, err
	}

	acquiredBitsAmountLast24Hours := int64(0)
	for _, entitlement := range entitlements {
		acquiredBitsAmountLast24Hours += int64(entitlement.Amount)
	}

	go f.Last24HoursAcquiredCache.Set(context.Background(), userID, acquiredBitsAmountLast24Hours)

	return acquiredBitsAmountLast24Hours, nil
}
