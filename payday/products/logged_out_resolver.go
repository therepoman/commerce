package products

import (
	"context"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/config"
	petozi "code.justin.tv/commerce/petozi/rpc"
)

type LoggedOutResolver interface {
	Resolve(ctx context.Context, platform petozi.Platform) (map[string]*petozi.BitsProduct, error)
}

func NewLoggedOutResolver() LoggedOutResolver {
	return &loggedOutResolver{}
}

type loggedOutResolver struct {
	Fetcher Fetcher               `inject:""`
	Config  *config.Configuration `inject:""`
}

func (l *loggedOutResolver) Resolve(ctx context.Context, platform petozi.Platform) (map[string]*petozi.BitsProduct, error) {
	allProducts, err := l.Fetcher.FetchAllFromPetozi(ctx)
	if err != nil {
		log.WithError(err).Error("failed to fetch products from Petozi")
		return nil, err
	}

	if allProducts == nil {
		return nil, nil
	}

	products := map[string]*petozi.BitsProduct{}
	for _, p := range allProducts {
		if p.ShowWhenLoggedOut && p.Platform == platform {
			products[p.Id] = p
		}
	}

	return products, nil
}
