package products

import (
	"context"
	"errors"
	"testing"

	products_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/products"
	petozi "code.justin.tv/commerce/petozi/rpc"
	. "github.com/smartystreets/goconvey/convey"
)

func TestLoggedOutResolver_Resolve(t *testing.T) {
	Convey("given a logged out products resolver", t, func() {
		fetcher := new(products_mock.Fetcher)

		resolver := &loggedOutResolver{
			Fetcher: fetcher,
		}

		ctx := context.Background()

		Convey("when the petozi client fails", func() {
			fetcher.On("FetchAllFromPetozi", ctx).Return(nil, errors.New("WALRUS STRIKE"))
			_, err := resolver.Resolve(ctx, petozi.Platform_AMAZON)
			So(err, ShouldNotBeNil)
		})

		Convey("when the petozi client succeeds", func() {
			products := []*petozi.BitsProduct{
				{
					Platform:          petozi.Platform_AMAZON,
					Id:                "product1",
					ShowWhenLoggedOut: true,
				},
				{
					Platform:          petozi.Platform_AMAZON,
					Id:                "product2",
					ShowWhenLoggedOut: false,
				},
				{
					Platform:          petozi.Platform_XSOLLA,
					Id:                "product3",
					ShowWhenLoggedOut: true,
				},
			}

			fetcher.On("FetchAllFromPetozi", ctx).Return(products, nil)

			res, err := resolver.Resolve(ctx, petozi.Platform_AMAZON)
			So(err, ShouldBeNil)
			So(res, ShouldHaveLength, 1)
			So(res["product1"], ShouldResemble, products[0])
			So(res["product2"], ShouldBeNil)
			So(res["product3"], ShouldBeNil)
		})
	})
}
