package products

import (
	"context"
	"testing"
	"time"

	products_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/products/availability"
	platform_getter "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/products/purchase_by_most_recent_super_platform/getter"
	petozi_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/petozi/rpc"
	"code.justin.tv/commerce/payday/models"
	petozi "code.justin.tv/commerce/petozi/rpc"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
)

func TestFetcher_FetchAllFromPetozi(t *testing.T) {
	Convey("given a fetcher", t, func() {
		petoziClient := new(petozi_mock.Petozi)
		fetcher := &fetcher{
			PetoziClient: petoziClient,
		}
		ctx := context.Background()

		Convey("when the petozi call fails", func() {
			petoziClient.On("GetBitsProducts", ctx, &petozi.GetBitsProductsReq{
				UseInMemoryCache: true,
			}).Return(nil, errors.New("WALRUS STRIKE")).Once()

			_, err := fetcher.FetchAllFromPetozi(ctx)
			So(err, ShouldNotBeNil)
		})

		Convey("when the petozi call succeeds but returns an empty response", func() {
			petoziClient.On("GetBitsProducts", ctx, &petozi.GetBitsProductsReq{
				UseInMemoryCache: true,
			}).Return(nil, nil).Once()

			resp, err := fetcher.FetchAllFromPetozi(ctx)
			So(err, ShouldBeNil)
			So(resp, ShouldBeNil)
		})

		Convey("when the petozi call succeeds and returns products", func() {
			products := []*petozi.BitsProduct{
				{
					Platform:          petozi.Platform_AMAZON,
					Id:                "product1",
					ShowWhenLoggedOut: true,
				},
				{
					Platform:          petozi.Platform_AMAZON,
					Id:                "product2",
					ShowWhenLoggedOut: false,
				},
				{
					Platform:          petozi.Platform_XSOLLA,
					Id:                "product3",
					ShowWhenLoggedOut: true,
				},
			}
			petoziClient.On("GetBitsProducts", ctx, &petozi.GetBitsProductsReq{
				UseInMemoryCache: true,
			}).Return(&petozi.GetBitsProductsResp{BitsProducts: products}, nil).Once()

			resp, err := fetcher.FetchAllFromPetozi(ctx)
			So(err, ShouldBeNil)
			So(resp, ShouldResemble, products)
		})
	})
}

func TestFetcher_Fetch_from_Petozi(t *testing.T) {
	Convey("Given a fetcher", t, func() {
		petoziClient := new(petozi_mock.Petozi)
		eligibilityChecker := new(products_mock.ProductAvailabilityChecker)
		platformGetter := new(platform_getter.Getter)

		fetcher := &fetcher{
			PetoziClient:       petoziClient,
			EligibilityChecker: eligibilityChecker,
			PlatformGetter:     platformGetter,
		}

		ctx := context.Background()
		ctx, cancel := context.WithTimeout(ctx, time.Second*2)
		defer cancel()
		tuid := "123123123"

		products := []*petozi.BitsProduct{
			{
				Id:       "test-sku-1",
				Quantity: 219,
			},
			{
				Id: "test-sku-2",
			},
		}
		petoziClient.On("GetBitsProducts", ctx, &petozi.GetBitsProductsReq{
			UseInMemoryCache: true,
		}).Return(&petozi.GetBitsProductsResp{
			BitsProducts: products,
		}, nil)
		eligibilityChecker.On("GetPurchasableQuantities", ctx, tuid).Return(map[string]int{
			"test-sku-1": 10,
			"test-sku-2": 0,
		})
		platformGetter.On("Get", ctx, tuid).Return(models.XSOLLA)

		Convey("it filters unavailable product", func() {
			resp, platform, err := fetcher.Fetch(ctx, tuid)
			So(err, ShouldBeNil)
			So(len(resp), ShouldEqual, 1)
			So(resp["test-sku-1"][0].Quantity, ShouldEqual, 219)
			So(resp["test-sku-1"][0].MaxPurchasableQuantity, ShouldEqual, 10)
			So(platform, ShouldEqual, models.XSOLLA)
		})
	})
}
