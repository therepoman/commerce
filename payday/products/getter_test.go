package products

import (
	"context"
	"testing"

	products_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/products"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/models/api"
	petozi "code.justin.tv/commerce/petozi/rpc"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
)

func TestGetter_GetByIDs(t *testing.T) {
	Convey("given a getter", t, func() {
		fetcher := new(products_mock.Fetcher)

		ctx := context.Background()

		g := &getter{
			Fetcher: fetcher,
		}

		platform := petozi.Platform_AMAZON
		productIDs := []string{"product1"}

		Convey("when the fetcher returns an error", func() {
			fetcher.On("FetchAllFromPetozi", ctx).Return(nil, errors.New("WALRUS STRIKE"))

			_, err := g.GetByIDs(ctx, platform, productIDs)
			So(err, ShouldNotBeNil)
		})

		Convey("when the fetcher returns no products", func() {
			fetcher.On("FetchAllFromPetozi", ctx).Return(nil, nil)

			products, err := g.GetByIDs(ctx, platform, productIDs)
			So(err, ShouldBeNil)
			So(products, ShouldBeNil)
		})

		Convey("when the fetcher does return products", func() {
			allProducts := []*petozi.BitsProduct{
				{
					Platform:          petozi.Platform_AMAZON,
					Id:                "product1",
					ShowWhenLoggedOut: true,
				},
				{
					Platform:          petozi.Platform_AMAZON,
					Id:                "product2",
					ShowWhenLoggedOut: false,
				},
				{
					Platform:          petozi.Platform_XSOLLA,
					Id:                "product3",
					ShowWhenLoggedOut: true,
				},
			}

			fetcher.On("FetchAllFromPetozi", ctx).Return(allProducts, nil)

			products, err := g.GetByIDs(ctx, platform, productIDs)
			So(err, ShouldBeNil)
			So(products, ShouldNotBeNil)

			Convey("we do return the product that matches the requested platform and productID", func() {
				So(products["product1"], ShouldNotBeNil)
			})

			Convey("we do not return the product that matches the requested platform but not the productID", func() {
				So(products["product2"], ShouldBeNil)
			})

			Convey("we do not return the product that does not match the requested platform and productID", func() {
				So(products["product3"], ShouldBeNil)
			})
		})
	})
}

func TestGetter_Get(t *testing.T) {
	Convey("Given a getter", t, func() {
		fetcher := new(products_mock.Fetcher)
		parser := new(products_mock.Parser)

		getter := &getter{
			Fetcher: fetcher,
			Parser:  parser,
		}

		ctx := context.Background()
		locale := "en"
		platform := models.AMAZON
		tuid := ""

		Convey("When we error to fetch products", func() {
			fetcher.On("Fetch", ctx, tuid).Return(nil, models.DEFAULT, errors.New("WALRUS STRIKE"))

			Convey("Then we should return an error", func() {
				resp, err := getter.Get(ctx, tuid, locale, platform)

				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		Convey("When we fetch products", func() {
			products := map[string][]petozi.BitsProduct{
				"foo": {
					{
						Platform:               petozi.Platform_AMAZON,
						Quantity:               322,
						MaxPurchasableQuantity: 1,
					},
				},
			}

			fetcher.On("Fetch", ctx, tuid).Return(products, models.DEFAULT, nil)

			Convey("Then we should return the products", func() {
				parser.On("Parse", platform, locale, products).Return([]api.Product{
					{Id: "foo", BitsAmount: 322, MaxQuantity: int32(1)},
				})

				resp, err := getter.Get(ctx, tuid, locale, platform)

				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
			})
		})
	})
}
