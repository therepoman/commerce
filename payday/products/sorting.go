package products

import (
	"sort"

	"code.justin.tv/commerce/payday/models/api"
)

type Sorter interface {
	Sort(products []api.Product)
}

type sorter struct{}

func NewSorter() Sorter {
	return &sorter{}
}

func (s *sorter) Sort(products []api.Product) {
	sort.Sort(byBitsAmount(products))
}

type byBitsAmount []api.Product

func (s byBitsAmount) Len() int {
	return len(s)
}
func (s byBitsAmount) Swap(k, j int) {
	s[k], s[j] = s[j], s[k]
}
func (s byBitsAmount) Less(k, j int) bool {
	if s[k].PromoType != nil && s[j].PromoType == nil {
		return true
	} else if s[k].PromoType == nil && s[j].PromoType != nil {
		return false
	}
	return s[k].BitsAmount < s[j].BitsAmount
}
