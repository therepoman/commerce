package cache

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/clients/redis"
	dynamo "code.justin.tv/commerce/payday/dynamo/purchase_by_most_recent_super_platform"
	"code.justin.tv/commerce/payday/hystrix"
	cmds "code.justin.tv/commerce/payday/hystrix"
	"code.justin.tv/commerce/payday/utils/strings"
	go_redis "github.com/go-redis/redis"
)

const (
	keyFormat = "purchase-super-platform-%s"
	ttl       = time.Minute * 5
)

type Data struct {
	Body *dynamo.PurchaseByMostRecentSuperPlatform
}

type Cache interface {
	Set(ctx context.Context, userID, superPlatform string, data *dynamo.PurchaseByMostRecentSuperPlatform)
	Get(ctx context.Context, userID, superPlatform string) *Data
	Del(ctx context.Context, userID string)
}

type cache struct {
	RedisClient redis.Client `inject:"redisClient"`
}

func NewCache() Cache {
	return &cache{}
}

func (c *cache) Set(ctx context.Context, userID, superPlatform string, body *dynamo.PurchaseByMostRecentSuperPlatform) {
	logger := log.WithFields(log.Fields{
		"UserID":         userID,
		"Super Platform": superPlatform,
	})

	data := &Data{
		Body: body,
	}

	purchasePlatformJson, err := json.Marshal(data)
	if err != nil {
		logger.WithError(err).Error("Failed to marshal purchase by most recent super platform")
		return
	}

	err = hystrix.Do(cmds.ProductsRecentPlatformCacheSet, func() error {
		err = c.RedisClient.HSet(ctx, userKey(userID), superPlatform, string(purchasePlatformJson)).Err()
		if err != nil {
			logger.WithError(err).Error("Failed to cache purchase super platform")
		} else {
			c.RedisClient.Expire(ctx, userKey(userID), ttl)
		}
		return err
	}, nil)
	if err != nil {
		logger.WithError(err).Warn("hystrix error setting most recent platform in cache")
	}
}

func (c *cache) Get(ctx context.Context, userID, superPlatform string) *Data {
	logger := log.WithFields(log.Fields{
		"UserID":         userID,
		"Super Platform": superPlatform,
	})
	var cachedValue string

	err := hystrix.Do(cmds.ProductsRecentPlatformCacheGet, func() error {
		var err error
		cachedValue, err = c.RedisClient.HGet(ctx, userKey(userID), superPlatform).Result()
		if err != nil && err != go_redis.Nil {
			logger.WithError(err).Error("Failed to get cached purchase by platform")
			return err
		}
		return nil
	}, nil)
	if err != nil {
		logger.WithError(err).Error("hystrix error getting most recent platform")
	}

	if strings.Blank(cachedValue) {
		return nil
	}

	var purchaseByPlatform Data
	err = json.Unmarshal([]byte(cachedValue), &purchaseByPlatform)
	if err != nil {
		logger.WithError(err).Error("Failed to unmarshal cached purchase by platform")
		c.del(ctx, userID, superPlatform)
		return nil
	}

	return &purchaseByPlatform
}

func (c *cache) Del(ctx context.Context, userID string) {
	_ = c.RedisClient.Del(ctx, userKey(userID)).Err()
}

func (c *cache) del(ctx context.Context, userID, superPlatform string) {
	_ = c.RedisClient.HDel(ctx, userKey(userID), superPlatform).Err()
}

func userKey(userID string) string {
	return fmt.Sprintf(keyFormat, userID)
}
