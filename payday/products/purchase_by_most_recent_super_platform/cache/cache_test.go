package cache

import (
	"context"
	"encoding/json"
	"errors"
	"testing"

	dynamo "code.justin.tv/commerce/payday/dynamo/purchase_by_most_recent_super_platform"
	redis_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/redis"
	"github.com/go-redis/redis"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestCache_Set(t *testing.T) {
	Convey("Given purchase platform cache Set", t, func() {
		ctx := context.Background()
		redisMock := new(redis_mock.Client)

		cache := cache{
			RedisClient: redisMock,
		}
		userID := "test-user"
		superPlatform := "AMAZON"
		purchasePlatform := dynamo.PurchaseByMostRecentSuperPlatform{
			TwitchUserID:  userID,
			SuperPlatform: superPlatform,
			ProductID:     "pagliacci-bits",
		}
		data := Data{
			Body: &purchasePlatform,
		}
		jsonInput, _ := json.Marshal(&data)

		redisMock.On("Expire", mock.Anything, mock.Anything, mock.Anything).Return(nil)

		Convey("when redis errors", func() {
			resp := redis.NewBoolCmd(errors.New("e"))
			redisMock.On("HSet", mock.Anything, userKey(userID), superPlatform, string(jsonInput)).Return(resp).Once()

			cache.Set(ctx, userID, superPlatform, &purchasePlatform)
			redisMock.AssertCalled(t, "HSet", mock.Anything, userKey(userID), superPlatform, string(jsonInput))
		})

		Convey("when redis succeeds", func() {
			resp := redis.NewBoolCmd(nil)
			redisMock.On("HSet", mock.Anything, userKey(userID), superPlatform, string(jsonInput)).Return(resp).Once()

			cache.Set(ctx, userID, superPlatform, &purchasePlatform)
			redisMock.AssertCalled(t, "HSet", mock.Anything, userKey(userID), superPlatform, string(jsonInput))
		})
	})
}

func TestCache_Get(t *testing.T) {
	Convey("Given purchase platform cache Get", t, func() {
		ctx := context.Background()
		redisMock := new(redis_mock.Client)

		cache := cache{
			RedisClient: redisMock,
		}
		userID := "test-user"
		superPlatform := "AMAZON"

		Convey("when redis errors", func() {
			resp := redis.NewStringResult("", errors.New("e"))
			redisMock.On("HGet", mock.Anything, userKey(userID), superPlatform).Return(resp).Once()

			result := cache.Get(ctx, userID, superPlatform)
			So(result, ShouldBeNil)
		})

		Convey("when redis returns Nil", func() {
			resp := redis.NewStringResult("", redis.Nil)
			redisMock.On("HGet", mock.Anything, userKey(userID), superPlatform).Return(resp).Once()

			result := cache.Get(ctx, userID, superPlatform)
			So(result, ShouldBeNil)
		})

		Convey("when redis returns value with body content", func() {
			purchasePlatform := dynamo.PurchaseByMostRecentSuperPlatform{
				TwitchUserID:  userID,
				SuperPlatform: superPlatform,
				ProductID:     "pagliacci-bits",
			}
			data := Data{
				Body: &purchasePlatform,
			}
			jsonOutput, _ := json.Marshal(&data)
			resp := redis.NewStringResult(string(jsonOutput), nil)
			redisMock.On("HGet", mock.Anything, userKey(userID), superPlatform).Return(resp).Once()

			result := cache.Get(ctx, userID, superPlatform)
			if result == nil || result.Body == nil {
				t.Fail()
			} else {
				So(result.Body.SuperPlatform, ShouldEqual, superPlatform)
			}
		})

		Convey("when redis returns value with body content empty", func() {
			data := Data{
				Body: nil,
			}
			jsonOutput, _ := json.Marshal(&data)
			resp := redis.NewStringResult(string(jsonOutput), nil)
			redisMock.On("HGet", mock.Anything, userKey(userID), superPlatform).Return(resp).Once()

			result := cache.Get(ctx, userID, superPlatform)
			if result == nil {
				t.Fail()
			} else {
				So(result.Body, ShouldBeNil)
			}
		})
	})
}

func TestCache_Del(t *testing.T) {
	Convey("Given purchase platform cache Del", t, func() {
		ctx := context.Background()
		redisMock := new(redis_mock.Client)

		cache := cache{
			RedisClient: redisMock,
		}
		userID := "test-user"

		Convey("when redis errors", func() {
			redisMock.On("Del", mock.Anything, userKey(userID)).Return(redis.NewIntCmd(errors.New("e")))

			cache.Del(ctx, userID)
			redisMock.AssertCalled(t, "Del", mock.Anything, userKey(userID))
		})

		Convey("when redis succeeds", func() {
			redisMock.On("Del", mock.Anything, userKey(userID)).Return(redis.NewIntCmd(nil))

			cache.Del(ctx, userID)
			redisMock.AssertCalled(t, "Del", mock.Anything, userKey(userID))
		})
	})
}
