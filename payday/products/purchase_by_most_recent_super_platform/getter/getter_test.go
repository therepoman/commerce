package getter

import (
	"context"
	"errors"
	"testing"

	"github.com/stretchr/testify/mock"

	dynamo "code.justin.tv/commerce/payday/dynamo/purchase_by_most_recent_super_platform"
	dao_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo/purchase_by_most_recent_super_platform"
	cache_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/products/purchase_by_most_recent_super_platform/cache"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/products/purchase_by_most_recent_super_platform/cache"
	. "github.com/smartystreets/goconvey/convey"
)

func TestGetter(t *testing.T) {
	Convey("Given recent platform getter", t, func() {
		cacheMock := new(cache_mock.Cache)
		daoMock := new(dao_mock.PurchaseByMostRecentSuperPlatformDAO)
		getter := getter{
			Cache: cacheMock,
			DAO:   daoMock,
		}
		userID := "12345"
		dynamoResult := dynamo.PurchaseByMostRecentSuperPlatform{
			TwitchUserID:  userID,
			SuperPlatform: "WEB",
			Platform:      "AMAZON",
			ProductID:     "pizza-bits",
		}
		ctx := context.Background()

		Convey("when recent platform is cached", func() {
			data := cache.Data{
				Body: &dynamoResult,
			}
			cacheMock.On("Get", mock.Anything, userID, "WEB").Return(&data).Once()

			mostRecentPlatform := getter.Get(ctx, userID)
			So(mostRecentPlatform, ShouldEqual, models.AMAZON)
		})

		Convey("when empty value is cached", func() {
			data := cache.Data{}
			cacheMock.On("Get", mock.Anything, userID, "WEB").Return(&data).Once()

			mostRecentPlatform := getter.Get(ctx, userID)
			So(mostRecentPlatform, ShouldEqual, models.DEFAULT)
		})

		Convey("when nothing has been cached", func() {
			cacheMock.On("Get", mock.Anything, userID, "WEB").Return(nil).Once()

			Convey("when dao errors", func() {
				daoMock.On("Get", ctx, userID, "WEB").Return(nil, errors.New("e")).Once()

				mostRecentPlatform := getter.Get(ctx, userID)
				So(mostRecentPlatform, ShouldEqual, models.DEFAULT)
			})

			Convey("when dao returns no result", func() {
				daoMock.On("Get", ctx, userID, "WEB").Return(nil, nil).Once()
				cacheMock.On("Set", mock.Anything, userID, "WEB", (*dynamo.PurchaseByMostRecentSuperPlatform)(nil)).Return().Once()

				mostRecentPlatform := getter.Get(ctx, userID)
				So(mostRecentPlatform, ShouldEqual, models.DEFAULT)
			})

			Convey("when dao returns a value", func() {
				daoMock.On("Get", ctx, userID, "WEB").Return(&dynamoResult, nil).Once()
				cacheMock.On("Set", mock.Anything, userID, "WEB", &dynamoResult).Return().Once()

				mostRecentPlatform := getter.Get(ctx, userID)
				So(mostRecentPlatform, ShouldEqual, models.AMAZON)
			})
		})
	})
}
