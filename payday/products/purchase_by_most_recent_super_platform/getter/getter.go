package getter

import (
	"context"

	"code.justin.tv/commerce/gogogadget/pointers"
	log "code.justin.tv/commerce/logrus"
	dynamo "code.justin.tv/commerce/payday/dynamo/purchase_by_most_recent_super_platform"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/products/purchase_by_most_recent_super_platform/cache"
)

type Getter interface {
	Get(ctx context.Context, userID string) models.Platform
}

type getter struct {
	Cache cache.Cache                                 `inject:""`
	DAO   dynamo.PurchaseByMostRecentSuperPlatformDAO `inject:""`
}

func NewGetter() Getter {
	return &getter{}
}

func (g *getter) Get(ctx context.Context, userID string) models.Platform {
	superPlatform := string(models.SuperPlatform_WEB)
	mostRecentSuperPlatform := models.DEFAULT

	cachedMostRecentPlatform := g.Cache.Get(ctx, userID, superPlatform)
	if cachedMostRecentPlatform != nil {
		if cachedMostRecentPlatform.Body != nil {
			mostRecentSuperPlatform = models.ParsePlatform(cachedMostRecentPlatform.Body.Platform, pointers.StringP("PurchaseByMostRecentSuperPlatform_Get"), nil, nil)
		}

		return mostRecentSuperPlatform
	}

	dynamoResult, err := g.DAO.Get(ctx, userID, superPlatform)
	if err != nil {
		log.WithError(err).Error("Failed to get most recent purchase platform")
		return mostRecentSuperPlatform
	}

	go g.Cache.Set(context.Background(), userID, superPlatform, dynamoResult)

	if dynamoResult != nil {
		mostRecentSuperPlatform = models.ParsePlatform(dynamoResult.Platform, pointers.StringP("PurchaseByMostRecentSuperPlatform_Get"), nil, nil)
	}

	return mostRecentSuperPlatform
}
