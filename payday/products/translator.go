package products

import (
	"strings"

	"code.justin.tv/commerce/payday/config"
	petozi "code.justin.tv/commerce/petozi/rpc"
)

const (
	BlackFriday2018Promo = "black_friday_2018"
	TwitchPrime2018Promo = "twitch_prime_2018"
	FirstTimePromo       = "first_time_purchase"
)

// This is a placeholder so that we can unblock mobile
var translations = map[string]string{
	TwitchPrime2018Promo: "Twitch Prime Member Bits Discount",
	FirstTimePromo:       "Special Offer: 1st Time Buyers",
	BlackFriday2018Promo: "Black Friday Sale!",
	"default":            "PROMOTIONAL",
}

type Translator interface {
	Translate(productInfo petozi.BitsProduct, locale string) *string
}

type translator struct {
	Config *config.Configuration `inject:""`
}

func NewTranslator() Translator {
	return &translator{}
}

func translationFor(translationMap map[string]string, promotionID string, locale string) *string {
	translation, ok := translationMap[locale]
	if ok {
		return &translation
	} else {
		splitLocal := strings.Split(locale, "-")
		translation, ok = translationMap[splitLocal[0]]
		if ok {
			return &translation
		} else {
			defaultTranslation := translations[promotionID]
			return &defaultTranslation
		}
	}
}

func (t *translator) Translate(productInfo petozi.BitsProduct, locale string) *string {
	if productInfo.Promo == nil {
		return nil
	}

	promoID := productInfo.Promo.Id
	switch promoID {
	case TwitchPrime2018Promo:
		return translationFor(t.Config.Translations.BitsPrimePromo, promoID, locale)
	case FirstTimePromo:
		return translationFor(t.Config.Translations.BitsFirstTimePromo, promoID, locale)
	case BlackFriday2018Promo:
		return translationFor(t.Config.Translations.BlackFriday2018, promoID, locale)
	default:
		translation, ok := translations[promoID]
		if ok {
			return &translation
		} else {
			defaultTranslation := translations["default"]
			return &defaultTranslation
		}
	}
}
