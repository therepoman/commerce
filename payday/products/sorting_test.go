package products

import (
	"testing"

	"code.justin.tv/commerce/payday/models/api"
	. "github.com/smartystreets/goconvey/convey"
)

func TestSorter_Sort(t *testing.T) {
	Convey("Given a sorter", t, func() {
		sorter := &sorter{}

		Convey("When we have a promo product in the input", func() {
			promoType := "super user"
			productPromo := api.Product{
				Id:         "promoProduct",
				BitsAmount: 10,
				PromoType:  &promoType,
			}

			productNonPromo := api.Product{
				Id:         "nonPromoProduct",
				BitsAmount: 5,
			}

			products := []api.Product{productNonPromo, productPromo}

			Convey("The promo product should be first in the list", func() {
				sorter.Sort(products)

				So(products[0].Id, ShouldEqual, productPromo.Id)
				So(products[1].Id, ShouldEqual, productNonPromo.Id)
			})
		})

		Convey("When we have two non-promo products", func() {
			product10 := api.Product{
				Id:         "10bits",
				BitsAmount: 10,
			}

			product5 := api.Product{
				Id:         "5bits",
				BitsAmount: 5,
			}

			products := []api.Product{product10, product5}

			Convey("The products should be sorted by bits amount", func() {
				sorter.Sort(products)

				So(products[0].Id, ShouldEqual, product5.Id)
				So(products[1].Id, ShouldEqual, product10.Id)
			})
		})
	})
}
