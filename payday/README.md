# payday: An Amazing Project
![Dont Panic](https://s3-us-west-2.amazonaws.com/payday-image-magick/dont-panic.jpg)

Payday makes it rain

<a target="_blank" href="https://eventbus.xarth.tv/events#CheerCreate"><img src="https://event-bus-badges.s3-us-west-2.amazonaws.com/CheerCreate.svg"></a><br>

[![Build Status](https://jenkins.internal.justin.tv/buildStatus/icon?job=payday)](https://jenkins.internal.justin.tv/job/payday/)
[![Go Report Card](https://goreportcard.internal.justin.tv/badge/code.justin.tv/commerce/payday)](https://goreportcard.internal.justin.tv/report/code.justin.tv/commerce/payday)

## Endpoints

| Environment | Endpoint |
| --- | --- |
| Prod | https://prod.payday.twitch.a2z.com/ |
| Staging | https://main.us-west-2.beta.payday.twitch.a2z.com/ |

## [Development Docs](https://git.xarth.tv/commerce/payday/tree/master/docs)

# Development Environment Setup




## MacOS


### Checkout development packages
```
go get code.justin.tv/commerce/payday
```

You now have a checked out and working payday. The build loop is to run 
> make local

# Code Pushes

Send a pull request via github

***When making pull requests, run `make local_lint` first and make sure not to introduce new lint warnings***

# Update dependency and mocks

Run `make dep` to pull in new dependencies. To require dependencies use a specific version range modify the `Gopkg.toml` file.

Running `make generate` to generate mocks _globally_ (for all the classes defined in the `mocks/generate.go` file) for testing has similar issue. You have to have dependent services' repositories locally.


# Deploys

Payday is deployed courier currently. On twitch vpn head to:

https://deploy.xarth.tv/#/commerce/payday

Available deploy branches are shown.

# Local Machine Support


## Setting up aws user role
You will need to go set up your own user account in isengard

1. Open [Isengard](https://isengard.amazon.com) and search for the `bits-eng-team@twitch.tv (021561903526)` aws account
2. Click on `Admin`
3. On the following page, search for and open `IAM`
4. Click on `Users` in the left-hand side. You should see a list of users created by other team members. 
5. Click `Add user` to create a new user role
6. Enter your user name and select `Programmatic access`, then click `Next`
7. On the permissions settings, select `Add user to group` and Select `BistTeamDev`
8. Click `Next`, don't add any tags (unless you really want to!)
9. Create the user!

## Set up S2S
Do this if you're getting access denied errors to malachai (specifically this error: 
```
AccessDenied: User: arn:aws:iam::021561901112:user/username is not authorized to perform: sts:AssumeRole on resource: arn:aws:iam::180116294062:role/malachai/registration/production/v1/payday-staging/a685c154-29d4-420d-96f1-d947f8fcd735
```

You need to add your local IAM user to the allowed ARN's for payday-staging on the [S2S configuration page](https://dashboard.internal.justin.tv/s2s/services).
1. Go to the [S2S configuration page](https://dashboard.internal.justin.tv/s2s/services)
2. Search for `payday`
3. Click on `payday-staging`
4. Under `Allowed ARNs`, click on `ADD ARN`
5. Add the ARN of the user you created above


## Local Sandstorm Access
Payday uses Sandstorm to manage secrets. 
To start payday up locally with the staging environment, your IAM user will need to be granted access to the payday-staging role.

To grant access:
1. Navigate to the [edit role page on the sandstorm dashboard](https://dashboard.internal.justin.tv/sandstorm/manage-roles?role=payday-staging&action=editrole).
2. Add the IAM user ARN that you run payday with as a new line in th `Allowed ARNs` section.
3. Click Save


## Integration tests

Payday's integration tests are all in the `/integration` folder.   
They have a custom tag `integration`. For these to be recognized by Intellij IDEA correctly, you will have to add this tag to your custom tags in the GO project settings. (it will show a warning at the top of the files and auto-complete will not work, if this isn't set up correctly)
