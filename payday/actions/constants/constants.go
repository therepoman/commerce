package constants

import (
	"math"

	"code.justin.tv/commerce/payday/models/api"
)

const (
	StaticAnimationType   = api.AnimationType("static")
	AnimatedAnimationType = api.AnimationType("animated")
	BonusCheerPrefix      = "bonus"
)

const (
	StaticExtensionType   = api.Extension("png")
	AnimatedExtensionType = api.Extension("gif")
)

const (
	LightBackground = api.Background("light")
	DarkBackground  = api.Background("dark")
)

const (
	Tier1      = api.Tier("1")
	Tier100    = api.Tier("100")
	Tier1000   = api.Tier("1000")
	Tier5000   = api.Tier("5000")
	Tier10000  = api.Tier("10000")
	Tier100000 = api.Tier("100000")
)

const (
	Scale1  = api.Scale("1")
	Scale15 = api.Scale("1.5")
	Scale2  = api.Scale("2")
	Scale3  = api.Scale("3")
	Scale4  = api.Scale("4")
)

const (
	Tier1Color      = "#979797"
	Tier100Color    = "#9c3ee8"
	Tier1000Color   = "#1db2a5"
	Tier5000Color   = "#0099fe"
	Tier10000Color  = "#f43021"
	Tier100000Color = "#f3a71a"
)

// Cheermote priorities
const (
	SponsoredCheermote   = 0
	DefaultCheermote     = 1
	BroadcasterCheermote = 2
	CharityCheermote     = 3
	MegaCheermote        = 4
	FootballCheermote    = 5
	GlobalCheermotes     = 6
	// push read only cheermotes to the bottom, so that in the case they're displayed they're at least at the bottom.
	DisplayOnlyCheermotes = math.MaxInt32
)

var TierToColor = map[api.Tier]string{
	Tier1:      Tier1Color,
	Tier100:    Tier100Color,
	Tier1000:   Tier1000Color,
	Tier5000:   Tier5000Color,
	Tier10000:  Tier10000Color,
	Tier100000: Tier100000Color,
}

var AnimationTypes = map[api.AnimationType]interface{}{
	AnimatedAnimationType: nil,
	StaticAnimationType:   nil,
}

var Backgrounds = map[api.Background]interface{}{
	LightBackground: nil,
	DarkBackground:  nil,
}

var Tiers = map[api.Tier]interface{}{
	Tier1:     nil,
	Tier100:   nil,
	Tier1000:  nil,
	Tier5000:  nil,
	Tier10000: nil,
}

var Scales = map[api.Scale]interface{}{
	Scale1:  nil,
	Scale15: nil,
	Scale2:  nil,
	Scale3:  nil,
	Scale4:  nil,
}
