package actions

import (
	"encoding/csv"
	"fmt"
	"os"
	"strings"
	"time"

	"code.justin.tv/commerce/payday/actions/utils"
	"code.justin.tv/commerce/payday/dynamo/actions"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/file"
	"code.justin.tv/commerce/payday/models"
	model "code.justin.tv/commerce/payday/models/api"
	fileUtil "code.justin.tv/commerce/payday/utils/close"
)

//The global prefixes file tells the cache what to get from dynamo on startup
//What's loaded into dynamo ahead of time is done in cmd/populate_global_actions
const PrefixesFileNameProduction = "global_prefixes_production"
const PrefixesFileNameStaging = "global_prefixes_staging"
const CheerPrefix = "cheer"
const AnonymousCheerPrefix = "anon"
const cheerOrder = 0
const GlobalActionsFolder = "actions"

var filePaths = []string{"actions/", "../actions/", "../../actions/", "../../../actions/"}

type ActionCache struct {
	actions map[string]model.Action
}

func NewActionCache(environment string) (*ActionCache, error) {
	a := &ActionCache{}
	var err error
	if environment != "development" {
		err = a.setup(environment)
	}
	return a, err
}

func (a *ActionCache) setup(environment string) error {
	a.actions = make(map[string]model.Action)

	fileName := PrefixesFileNameStaging
	if environment == "production" {
		fileName = PrefixesFileNameProduction
	}

	filePath, err := file.SearchFileInPaths(fileName, filePaths)
	if err != nil {
		return errors.Notef(err, "Error finding required prefixes file: %s", fileName)
	}

	file, err := os.Open(filePath)
	if err != nil {
		return errors.Notef(err, "Failed while opening file: %s", filePath)
	}
	defer fileUtil.Close(file)

	reader := csv.NewReader(file)
	records, err := reader.ReadAll()
	if err != nil {
		return errors.Notef(err, "Failed while reading file as CSV: %s", filePath)
	}

	actions := GetAllActions()

	cheerAction := actions[CheerPrefix]
	cheerAction.Order = cheerOrder

	a.actions[CheerPrefix] = cheerAction

	for index, line := range records {
		if len(line) == 0 {
			return errors.Notef(err, "Error loading prefixes in cache -- a blank line was passed in as a prefix")
		}
		prefix := line[0]

		action, found := actions[prefix]
		if !found {
			return errors.Newf("Global action does not exist: %s", prefix)
		}

		//cheer order 0, custom cheermote order 1, the rest follow file ordering for orders 2+
		action.Order = index + 2

		a.actions[prefix] = action
	}

	return nil
}

func (a *ActionCache) Get(prefix string) (model.Action, bool) {
	action, found := a.actions[prefix]
	return action, found
}

func (a *ActionCache) GetActionAsset(options model.GetActionAssetOptions) (*model.GetActionAssetResponse, error) {
	action, found := a.actions[strings.ToLower(options.Prefix)]
	if !found {
		return nil, fmt.Errorf("Prefix %s not found in cache.", options.Prefix)
	}
	if options.Bits < 1 {
		return nil, errors.New("cannot have bits amount less than 1")
	}

	tier := utils.GetCheerTierForThreshold(action.Tiers, options.Bits)

	actionUrl, err := tier.Images.Get(options.Background, options.State, options.Scale)

	if err != nil {
		return nil, err
	}

	response := &model.GetActionAssetResponse{
		ImageURL: actionUrl,
		BitsTier: tier.BitsMinimum,
		Color:    tier.Color,
		CanCheer: tier.CanCheer,
	}

	return response, nil
}

func (a *ActionCache) GetAll() []model.Action {
	actions := make([]model.Action, 0, len(a.actions))
	for _, action := range a.actions {
		actions = append(actions, action)
	}
	return actions
}

func (a *ActionCache) List() []string {
	actionPrefixes := make([]string, len(a.actions))
	i := 0
	for prefix := range a.actions {
		actionPrefixes[i] = prefix
		i++
	}
	return actionPrefixes
}

var BitsLaunchDay = time.Date(2016, 6, 27, 0, 0, 0, 0, time.UTC)
var AnimatedEmotesSet1LaunchDay = time.Date(2016, 12, 15, 0, 0, 0, 0, time.UTC)
var AnimatedEmotesSet2LaunchDay = time.Date(2017, 5, 23, 0, 0, 0, 0, time.UTC)
var BdayLaunch = time.Date(2017, 8, 4, 0, 0, 0, 0, time.UTC)
var BitbossLaunch = time.Date(2017, 8, 21, 0, 0, 0, 0, time.UTC)
var DoodlecheerLaunch = time.Date(2017, 9, 22, 0, 0, 0, 0, time.UTC)
var RipcheerLaunch = time.Date(2017, 10, 24, 0, 0, 0, 0, time.UTC)
var HolidaycheerLaunch = time.Date(2017, 12, 14, 0, 0, 0, 0, time.UTC)
var DansgameElegiggleLaunch = time.Date(2018, 1, 17, 0, 0, 0, 0, time.UTC)
var ShamrockLaunch = time.Date(2018, 3, 13, 0, 0, 0, 0, time.UTC)
var PartyLaunch = time.Date(2018, 4, 24, 0, 0, 0, 0, time.UTC)
var HeyguysTrioLaunch = time.Date(2018, 5, 8, 0, 0, 0, 0, time.UTC)

var defaultBackgrounds = []string{"light", "dark"}
var defaultStates = []string{"static", "animated"}
var defaultScales = []string{"1", "1.5", "2", "3", "4"}

type actionMap map[string]model.Action

func (m actionMap) PutNewAction(prefix string, s3Prefix string, actionsFolder string, typeStr string, lastUpdated time.Time, include100k bool) {
	m[strings.ToLower(prefix)] = newAction(prefix, s3Prefix, actionsFolder, typeStr, lastUpdated, include100k)
}

func ConvertDynamoActionToAPIAction(dynamoAction actions.Action) model.Action {
	action := model.Action{
		Prefix:       dynamoAction.CanonicalCasing,
		Type:         dynamoAction.Type,
		LastUpdated:  dynamoAction.LastUpdated,
		Backgrounds:  defaultBackgrounds,
		Scales:       defaultScales,
		States:       defaultStates,
		Tiers:        utils.NewDefaultTier(false, dynamoAction.Type), //TODO: change this false to dynamoAction.Support100K
		IsCharitable: dynamoAction.IsCharitable,
	}
	return models.MapActionImageTierURLs(action, dynamoAction.Prefix)
}

func newAction(prefix string, s3Prefix string, actionsFolder string, typeStr string, lastUpdated time.Time, include100k bool) model.Action {
	action := model.Action{
		Prefix:      prefix,
		Type:        typeStr,
		LastUpdated: lastUpdated,
		Tiers:       utils.NewDefaultTier(include100k, typeStr),
		Backgrounds: defaultBackgrounds,
		States:      defaultStates,
		Scales:      defaultScales,
	}
	return models.ConvertTemplateToURLs(action, actionsFolder, s3Prefix)
}

func GetAllActions() actionMap {
	actions := make(actionMap)
	actions.PutNewAction("Cheer", "cheer", GlobalActionsFolder, model.GlobalFirstPartyAction, BitsLaunchDay, false)

	// Seasonal Cheers
	actions.PutNewAction("bday", "bday", GlobalActionsFolder, model.GlobalFirstPartyAction, BdayLaunch, false)
	actions.PutNewAction("RIPCheer", "ripcheer", GlobalActionsFolder, model.GlobalFirstPartyAction, RipcheerLaunch, false)
	actions.PutNewAction("HolidayCheer", "holidaycheer", GlobalActionsFolder, model.GlobalThirdPartyAction, HolidaycheerLaunch, false)
	actions.PutNewAction("Shamrock", "shamrock", GlobalActionsFolder, model.GlobalFirstPartyAction, ShamrockLaunch, false)

	actions.PutNewAction("tww", "tww", GlobalActionsFolder, model.GlobalFirstPartyAction, AnimatedEmotesSet1LaunchDay, false)
	actions.PutNewAction("Kappa", "kappa", GlobalActionsFolder, model.GlobalFirstPartyAction, AnimatedEmotesSet1LaunchDay, false)
	actions.PutNewAction("Kreygasm", "kreygasm", GlobalActionsFolder, model.GlobalFirstPartyAction, AnimatedEmotesSet1LaunchDay, false)
	actions.PutNewAction("SwiftRage", "swiftrage", GlobalActionsFolder, model.GlobalFirstPartyAction, AnimatedEmotesSet1LaunchDay, false)
	actions.PutNewAction("Muxy", "muxy", GlobalActionsFolder, model.GlobalThirdPartyAction, AnimatedEmotesSet1LaunchDay, false)
	actions.PutNewAction("Streamlabs", "streamlabs", GlobalActionsFolder, model.GlobalThirdPartyAction, AnimatedEmotesSet1LaunchDay, false)

	actions.PutNewAction("4Head", "4head", GlobalActionsFolder, model.GlobalFirstPartyAction, AnimatedEmotesSet2LaunchDay, false)
	actions.PutNewAction("NotLikeThis", "notlikethis", GlobalActionsFolder, model.GlobalFirstPartyAction, AnimatedEmotesSet2LaunchDay, false)
	actions.PutNewAction("FailFish", "failfish", GlobalActionsFolder, model.GlobalFirstPartyAction, AnimatedEmotesSet2LaunchDay, false)
	actions.PutNewAction("MrDestructoid", "mrdestructoid", GlobalActionsFolder, model.GlobalFirstPartyAction, AnimatedEmotesSet2LaunchDay, false)
	actions.PutNewAction("PJSalt", "pjsalt", GlobalActionsFolder, model.GlobalFirstPartyAction, AnimatedEmotesSet2LaunchDay, false)
	actions.PutNewAction("TriHard", "trihard", GlobalActionsFolder, model.GlobalFirstPartyAction, AnimatedEmotesSet2LaunchDay, false)
	actions.PutNewAction("VoHiYo", "vohiyo", GlobalActionsFolder, model.GlobalFirstPartyAction, AnimatedEmotesSet2LaunchDay, false)

	actions.PutNewAction("BitBoss", "bitboss", GlobalActionsFolder, model.GlobalThirdPartyAction, BitbossLaunch, false)
	actions.PutNewAction("DoodleCheer", "doodlecheer", GlobalActionsFolder, model.GlobalThirdPartyAction, DoodlecheerLaunch, false)

	actions.PutNewAction("DansGame", "dansgame", GlobalActionsFolder, model.GlobalFirstPartyAction, DansgameElegiggleLaunch, false)
	actions.PutNewAction("EleGiggle", "elegiggle", GlobalActionsFolder, model.GlobalFirstPartyAction, DansgameElegiggleLaunch, false)

	actions.PutNewAction("Party", "party", GlobalActionsFolder, model.GlobalFirstPartyAction, PartyLaunch, false)

	actions.PutNewAction("HeyGuys", "heyguys", GlobalActionsFolder, model.GlobalFirstPartyAction, HeyguysTrioLaunch, false)
	actions.PutNewAction("FrankerZ", "frankerz", GlobalActionsFolder, model.GlobalFirstPartyAction, HeyguysTrioLaunch, false)
	actions.PutNewAction("SeemsGood", "seemsgood", GlobalActionsFolder, model.GlobalFirstPartyAction, HeyguysTrioLaunch, false)
	return actions
}

func GetActions(prefixes []string, actionsFolder string, launchDate time.Time) actionMap {
	actions := make(actionMap)
	for _, prefix := range prefixes {
		actions.PutNewAction(prefix, strings.ToLower(prefix), actionsFolder, model.GlobalFirstPartyAction, launchDate, false)
	}
	return actions
}
