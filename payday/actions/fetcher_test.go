package actions

import (
	"context"
	"testing"
	"time"

	"code.justin.tv/commerce/payday/dynamo/actions"
	actions_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/actions"
	dynamo_actions_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo/actions"
	"code.justin.tv/commerce/payday/models/api"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestFetcher_Fetch(t *testing.T) {
	Convey("Given a fetcher", t, func() {
		ctx := context.Background()
		dao := new(dynamo_actions_mock.IActionDao)
		cache := new(actions_mock.Cache)

		f := &fetcher{
			Dao:   dao,
			Cache: cache,
		}

		prefix1 := "test1"

		action1 := &actions.Action{
			Prefix:          prefix1,
			CanonicalCasing: "Test1",
			Support100K:     false,
			Type:            api.GlobalFirstPartyAction,
			StartTime:       time.Date(2018, 1, 1, 1, 0, 0, 0, time.UTC),
		}

		Convey("Given the campaign is in the cache", func() {
			cache.On("Get", mock.Anything, prefix1).Return(action1)

			Convey("When we fetch the list of records", func() {
				actual, err := f.Fetch(ctx, prefix1)

				So(err, ShouldBeNil)
				So(actual, ShouldEqual, action1)

				Convey("We only fetch from the cache and do not hit Dynamo", func() {
					dao.AssertNotCalled(t, "Get", mock.Anything)
					cache.AssertCalled(t, "Get", mock.Anything, prefix1)
				})
			})
		})

		Convey("Given the campaign is not in the cache", func() {
			cache.On("Get", mock.Anything, prefix1).Return(nil)
			cache.On("Set", mock.Anything, prefix1, *action1).Return(nil)

			dao.On("Get", prefix1).Return(action1, nil)

			Convey("When we fetch the list of records", func() {
				actual, err := f.Fetch(ctx, prefix1)

				So(err, ShouldBeNil)
				So(actual, ShouldEqual, action1)

				Convey("We fetch from both the cache and Dynamo", func() {
					cache.AssertCalled(t, "Get", mock.Anything, prefix1)
					dao.AssertCalled(t, "Get", prefix1)
				})

				Convey("We set the new record from Dynamo in the cache", func() {
					cache.AssertCalled(t, "Set", mock.Anything, prefix1, *action1)
				})
			})
		})
	})
}

func TestFetcher_FetchBatch(t *testing.T) {
	Convey("Given a Fetcher", t, func() {
		ctx := context.Background()
		dao := new(dynamo_actions_mock.IActionDao)
		cache := new(actions_mock.Cache)

		f := &fetcher{
			Dao:   dao,
			Cache: cache,
		}

		prefix1 := "test1"
		prefix2 := "test2"

		action1 := &actions.Action{
			Prefix:          prefix1,
			CanonicalCasing: "Test1",
			Support100K:     false,
			Type:            api.GlobalFirstPartyAction,
			StartTime:       time.Date(2018, 1, 1, 1, 0, 0, 0, time.UTC),
		}

		action2 := &actions.Action{
			Prefix:          prefix2,
			CanonicalCasing: "Test2",
			Support100K:     false,
			Type:            api.GlobalFirstPartyAction,
			StartTime:       time.Date(2018, 1, 1, 1, 0, 0, 0, time.UTC),
		}

		Convey("Given a list of actions that are in the cache", func() {
			cache.On("Get", mock.Anything, prefix1).Return(action1)
			cache.On("Get", mock.Anything, prefix2).Return(action2)

			Convey("When we fetch the list of records", func() {
				actual, err := f.FetchBatch(ctx, []string{prefix1, prefix2})

				So(err, ShouldBeNil)
				actionsResult := map[string]*actions.Action{prefix1: action1, prefix2: action2}
				So(actionListsEqual(actual, actionsResult), ShouldBeTrue)

				Convey("We only fetch from the cache and do not hit Dynamo", func() {
					dao.AssertNotCalled(t, "GetBatch", mock.Anything)
					cache.AssertCalled(t, "Get", mock.Anything, prefix1)
					cache.AssertCalled(t, "Get", mock.Anything, prefix2)
				})
			})
		})

		Convey("Given a list of actions both in the cache and not in the cache", func() {
			cache.On("Get", mock.Anything, prefix1).Return(action1)
			cache.On("Get", mock.Anything, prefix2).Return(nil)

			cache.On("Set", mock.Anything, prefix2, *action2).Return(nil)

			dao.On("GetBatch", []string{prefix2}).Return(map[string]*actions.Action{prefix1: action2}, nil)

			Convey("When we fetch the list of records", func() {
				actual, err := f.FetchBatch(ctx, []string{prefix1, prefix2})

				So(err, ShouldBeNil)
				actionsResult := map[string]*actions.Action{prefix1: action1, prefix2: action2}
				So(actionListsEqual(actual, actionsResult), ShouldBeTrue)

				Convey("We fetch from both the cache and Dynamo", func() {
					cache.AssertCalled(t, "Get", mock.Anything, prefix1)
					cache.AssertCalled(t, "Get", mock.Anything, prefix2)
					dao.AssertCalled(t, "GetBatch", []string{prefix2})
				})

				Convey("We set the new record from Dynamo in the cache", func() {
					cache.AssertCalled(t, "Set", mock.Anything, prefix2, *action2)
				})
			})
		})

		Convey("Given a list of actions not at all in the cache", func() {
			cache.On("Get", mock.Anything, prefix1).Return(nil)
			cache.On("Get", mock.Anything, prefix2).Return(nil)

			cache.On("Set", mock.Anything, prefix1, *action1).Return(nil)
			cache.On("Set", mock.Anything, prefix2, *action2).Return(nil)

			actionsResult := map[string]*actions.Action{prefix1: action1, prefix2: action2}
			dao.On("GetBatch", []string{prefix1, prefix2}).Return(actionsResult, nil)

			Convey("When we fetch the list of records", func() {
				actual, err := f.FetchBatch(ctx, []string{prefix1, prefix2})

				So(err, ShouldBeNil)
				So(actionListsEqual(actual, actionsResult), ShouldBeTrue)

				Convey("We fetch from both the cache and Dynamo", func() {
					cache.AssertCalled(t, "Get", mock.Anything, prefix1)
					cache.AssertCalled(t, "Get", mock.Anything, prefix2)
					dao.AssertCalled(t, "GetBatch", []string{prefix1, prefix2})
				})

				Convey("We set the new records from Dynamo in the cache", func() {
					cache.AssertCalled(t, "Set", mock.Anything, prefix1, *action1)
					cache.AssertCalled(t, "Set", mock.Anything, prefix2, *action2)
				})
			})
		})
	})
}

func TestDeduplicate(t *testing.T) {
	Convey("dedupes with duplicates", t, func() {
		So(deduplicatePrefixes([]string{"sand", "desert", "skywalker", "sand"}), ShouldResemble, []string{"sand", "desert", "skywalker"})
	})

	Convey("dedupes is fine with no duplicates", t, func() {
		So(deduplicatePrefixes([]string{"ig-88"}), ShouldResemble, []string{"ig-88"})
	})

	Convey("dedupes is fine with empty", t, func() {
		So(deduplicatePrefixes([]string{}), ShouldResemble, []string{})
	})
}

func actionListsEqual(am1, am2 map[string]*actions.Action) bool {
	if len(am1) != len(am2) {
		return false
	}

	for _, a1 := range am1 {
		a2 := am2[a1.Prefix]
		if a1 == nil || a2 == nil {
			if a1 == a2 {
				continue
			}
			return false
		}

		if !a1.Equals(a2) {
			return false
		}
	}
	return true
}
