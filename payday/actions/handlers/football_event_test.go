package handlers

import (
	"context"
	"errors"
	"testing"
	"time"

	"code.justin.tv/commerce/payday/actions"
	"code.justin.tv/commerce/payday/dynamo"
	dynamo_actions "code.justin.tv/commerce/payday/dynamo/actions"
	actions_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/actions"
	channel_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/cache/channel"
	football_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/events/football"
	metrics_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/metrics"
	"code.justin.tv/commerce/payday/models/api"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestFootballEventHandler_Handle(t *testing.T) {
	Convey("Given a football event handler", t, func() {
		channelManager := new(channel_mock.ChannelManager)
		actionsFetcher := new(actions_mock.Fetcher)
		bouncer := new(football_mock.Bouncer)
		statter := new(metrics_mock.Statter)
		statter.On("TimingDuration", mock.Anything, mock.Anything)

		handler := &FootballEventHandler{
			ChannelManager: channelManager,
			Fetcher:        actionsFetcher,
			Bouncer:        bouncer,
			Statter:        statter,
		}
		getActionsRequest := api.GetActionsRequest{ChannelId: "123"}

		var nextActionRetriever actions.Retriever
		Convey("When next action retriever errors", func() {
			nextActionRetriever = &errorActionRetriever{}

			Convey("Handler should error", func() {
				actionsOut, err := handler.Handle(nextActionRetriever).GetActions(context.Background(), getActionsRequest)
				So(err, ShouldNotBeNil)
				So(actionsOut, ShouldHaveLength, 0)
			})
		})

		Convey("When next action retriever succeeds", func() {
			nextActionRetriever = &emptyActionRetriever{}

			Convey("When the bouncer says we aren't on the list", func() {
				bouncer.On("Bounce", getActionsRequest.ChannelId).Return(false)

				Convey("Handler should error", func() {
					actionsOut, err := handler.Handle(nextActionRetriever).GetActions(context.Background(), getActionsRequest)
					So(err, ShouldBeNil)
					So(actionsOut, ShouldHaveLength, 0)
				})
			})

			Convey("When the bouncer says we are on the list", func() {
				bouncer.On("Bounce", getActionsRequest.ChannelId).Return(true)

				Convey("When fetching the channel returns an error", func() {
					channelManager.On("Get", mock.Anything, getActionsRequest.ChannelId).Return(nil, errors.New("THE WALRUS STRIKES"))

					Convey("Handler should return nothing", func() {
						actionsOut, err := handler.Handle(nextActionRetriever).GetActions(context.Background(), getActionsRequest)
						So(err, ShouldBeNil)
						So(actionsOut, ShouldHaveLength, 0)
					})
				})

				Convey("When fetching the channel returns nothing", func() {
					channelManager.On("Get", mock.Anything, getActionsRequest.ChannelId).Return(nil, nil)

					Convey("Handler should return nothing", func() {
						actionsOut, err := handler.Handle(nextActionRetriever).GetActions(context.Background(), getActionsRequest)
						So(err, ShouldBeNil)
						So(actionsOut, ShouldHaveLength, 0)
					})
				})

				Convey("When fetching the channel returns successfully", func() {
					Convey("When the user is opted out of the football event", func() {
						isOptedOut := true
						channelInfo := dynamo.Channel{CheerBombEventOptOut: &isOptedOut}
						channelManager.On("Get", mock.Anything, getActionsRequest.ChannelId).Return(&channelInfo, nil)

						Convey("Handler should return nothing", func() {
							actionsOut, err := handler.Handle(nextActionRetriever).GetActions(context.Background(), getActionsRequest)
							So(err, ShouldBeNil)
							So(actionsOut, ShouldHaveLength, 0)
						})
					})

					Convey("When the user is opted in to the football event", func() {
						isOptedOut := false
						channelInfo := dynamo.Channel{CheerBombEventOptOut: &isOptedOut}
						channelManager.On("Get", mock.Anything, getActionsRequest.ChannelId).Return(&channelInfo, nil)

						Convey("When the actions fetcher errors to get the action info", func() {
							actionsFetcher.On("Fetch", mock.Anything, footballEventCheermotePrefix).Return(nil, errors.New("THE WALRUS STRIKES AGAIN"))

							Convey("Handler should return an error", func() {
								actionsOut, err := handler.Handle(nextActionRetriever).GetActions(context.Background(), getActionsRequest)
								So(err, ShouldNotBeNil)
								So(actionsOut, ShouldHaveLength, 0)
							})
						})

						Convey("When the actions fetcher returns successfully with nothing", func() {
							actionsFetcher.On("Fetch", mock.Anything, footballEventCheermotePrefix).Return(nil, nil)

							Convey("Handler should return nothing", func() {
								actionsOut, err := handler.Handle(nextActionRetriever).GetActions(context.Background(), getActionsRequest)
								So(err, ShouldBeNil)
								So(actionsOut, ShouldHaveLength, 0)
							})
						})

						Convey("When the actions fetcher returns successfully with an action", func() {
							action := dynamo_actions.Action{
								Prefix:          footballEventCheermotePrefix,
								CanonicalCasing: "Goal",
								Type:            api.GlobalFirstPartyAction,
								Support100K:     false,
								StartTime:       time.Date(2018, 1, 1, 1, 0, 0, 0, time.UTC),
							}
							actionsFetcher.On("Fetch", mock.Anything, footballEventCheermotePrefix).Return(&action, nil)

							Convey("Handler should return the action", func() {
								actionsOut, err := handler.Handle(nextActionRetriever).GetActions(context.Background(), getActionsRequest)
								So(err, ShouldBeNil)
								So(actionsOut, ShouldHaveLength, 1)
							})
						})
					})
				})
			})
		})
	})
}
