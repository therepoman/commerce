package handlers

import (
	"context"
	"strconv"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/actions"
	"code.justin.tv/commerce/payday/actions/constants"
	"code.justin.tv/commerce/payday/actions/utils"
	"code.justin.tv/commerce/payday/metrics"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/models/api"
)

const (
	sixthTierAmount   = 100000
	doodleCheerPrefix = "doodlecheer"
)

type SixTierCheermoteHandler struct {
	Fetcher actions.Fetcher `inject:""`
	Statter metrics.Statter `inject:""`
}

var supportedActions = []string{actions.CheerPrefix, doodleCheerPrefix}

func (h *SixTierCheermoteHandler) Handle(next actions.Retriever) actions.Retriever {
	return actions.RetrieverFunc(func(ctx context.Context, request api.GetActionsRequest) ([]api.Action, error) {
		allActions, err := next.GetActions(ctx, request)
		if err != nil {
			log.WithError(err).Error("Error getting actions from the next piece of the chain")
			return allActions, err
		}

		if enableStepMetrics {
			startTime := time.Now()
			defer func(st time.Time) {
				go h.Statter.TimingDuration("get_actions_handlers__step_six_tier", time.Since(st))
			}(startTime)
		}

		sixTierCheermotes, err := h.Fetcher.FetchBatch(ctx, supportedActions)
		if err != nil {
			log.WithError(err).Error("Error looking up SixTierCheermote from actions table")
			return allActions, err
		}

		for _, cachedAction := range sixTierCheermotes {
			if cachedAction != nil && cachedAction.IsActive() {
				action := actions.ConvertDynamoActionToAPIAction(*cachedAction)
				action.Order = constants.DefaultCheermote

				clientType := utils.GetClientType(ctx)
				if utils.SupportsUpperTiers(clientType) {
					action.Tiers = make100K(action)
				}

				action = models.ConvertTemplateToURLs(action, actions.GlobalActionsFolder, action.Prefix)
				allActions = append(allActions, action)
			}
		}

		return allActions, nil
	})
}

func make100K(action api.Action) []api.ActionTier {
	tiers := action.Tiers

	tierId := strconv.FormatInt(sixthTierAmount, 10)

	tierForColor := api.Tier(tierId)

	tier := api.ActionTier{
		Id:             tierId,
		BitsMinimum:    sixthTierAmount,
		CanCheer:       utils.CanCheerActionTier(sixthTierAmount, action.Type),
		ShowInBitsCard: utils.CanShowTierInBitsCard(sixthTierAmount, action.Type),
		Color:          constants.TierToColor[tierForColor],
	}

	tiers = append(tiers, tier)

	return tiers
}
