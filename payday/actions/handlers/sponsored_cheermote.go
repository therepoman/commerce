package handlers

import (
	"context"
	"strings"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/actions"
	"code.justin.tv/commerce/payday/actions/constants"
	"code.justin.tv/commerce/payday/actions/utils"
	"code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/campaigns"
	"code.justin.tv/commerce/payday/metrics"
	model "code.justin.tv/commerce/payday/models/api"
	sponsored_actions "code.justin.tv/commerce/payday/sponsored_cheermote/actions"
	"code.justin.tv/commerce/payday/sponsored_cheermote/campaign"
	"code.justin.tv/commerce/payday/sponsored_cheermote/channel_status"
	"code.justin.tv/commerce/payday/sponsored_cheermote/user_campaign_status"
)

type SponsoredCheermoteHandler struct {
	SponsoredCheermoteManager    campaign.Manager            `inject:""`
	SponsoredCheermoteFilterer   channel_status.Filterer     `inject:""`
	SponsoredActionsGetter       sponsored_actions.Getter    `inject:""`
	SponsoredCheermoteUserGetter user_campaign_status.Getter `inject:""`
	Statter                      metrics.Statter             `inject:""`
}

func (h *SponsoredCheermoteHandler) Handle(next actions.Retriever) actions.Retriever {
	return actions.RetrieverFunc(func(ctx context.Context, request model.GetActionsRequest) ([]model.Action, error) {
		if !request.IncludeSponsored {
			return next.GetActions(ctx, request)
		}

		allActions, err := next.GetActions(ctx, request)
		if err != nil {
			return allActions, err
		}

		if enableStepMetrics {
			startTime := time.Now()
			defer func(st time.Time) {
				go h.Statter.TimingDuration("get_actions_handlers__step_sponsored_cheermote", time.Since(st))
			}(startTime)
		}

		allCampaigns := h.SponsoredCheermoteManager.GetAllCampaigns(ctx)
		if len(allCampaigns) < 1 {
			return allActions, nil
		}

		enabledCampaigns, err := h.SponsoredCheermoteFilterer.Filter(ctx, allCampaigns, request.ChannelId)
		if err != nil {
			return allActions, err
		}

		dynamoCampaignActions, err := h.SponsoredActionsGetter.GetBatch(ctx, allCampaigns)
		if err != nil {
			return allActions, err
		}
		campaignActions := make([]model.Action, 0)

		for _, dynamoAction := range dynamoCampaignActions {
			if dynamoAction == nil {
				continue
			}
			action := actions.ConvertDynamoActionToAPIAction(*dynamoAction)
			campaignForAction := getCampaignForAction(action.Prefix, allCampaigns, enabledCampaigns)

			if campaignForAction != nil {
				if campaignForAction.IsActive() && isEnabled(campaignForAction.CampaignID, enabledCampaigns) {
					var userUsedBits int64
					if request.UserId != "" {
						userUsedBits, err = h.SponsoredCheermoteUserGetter.Get(ctx, campaignForAction.CampaignID, request.UserId)
						if err != nil {
							log.WithError(err).WithFields(log.Fields{"campaignID": campaignForAction.CampaignID, "userID": request.UserId}).Error("Failed to retrieve the user contribution against campaign")
							continue
						}
					}
					action = utils.AddCampaignToAction(action, campaignForAction, userUsedBits)
					action.Order = constants.SponsoredCheermote
				} else {
					action = utils.AddCampaignToAction(action, campaignForAction, 0)
					action.Type = model.DisplayOnlyAction
					action.Order = constants.DisplayOnlyCheermotes
					action.Campaign = nil
				}
			}
			campaignActions = append(campaignActions, action)
		}

		// Add bonus cheermote
		bonus, err := h.SponsoredActionsGetter.GetBonus(ctx)
		if err != nil {
			log.WithError(err).Error("Failed to get bonus cheermote")
		} else {
			bonusAction := actions.ConvertDynamoActionToAPIAction(*bonus)
			bonusAction.Order = constants.DisplayOnlyCheermotes
			allActions = append(allActions, bonusAction)
		}

		allActions = append(allActions, campaignActions...)

		return allActions, nil
	})
}

func isEnabled(campaignId string, enabledCampaigns []*campaigns.SponsoredCheermoteCampaign) bool {
	for _, campaign := range enabledCampaigns {
		if campaignId == campaign.CampaignID {
			return true
		}
	}
	return false
}

func getCampaignForAction(prefix string, allCampaigns, enabledCampaigns []*campaigns.SponsoredCheermoteCampaign) *campaigns.SponsoredCheermoteCampaign {
	// Campaigns can potentially overlap, so we want to make sure we find the one that is most relevant to the user.
	// If we don't find any campaigns, we need to at least return the one that matches the prefix so we can render
	// it for Clips/VODS usecases.
	for _, campaign := range enabledCampaigns {
		if campaign != nil {
			if strings.EqualFold(campaign.Prefix, prefix) {
				return campaign
			}
		}
	}

	for _, campaign := range allCampaigns {
		if campaign != nil {
			if strings.EqualFold(campaign.Prefix, prefix) {
				return campaign
			}
		}
	}
	return nil
}
