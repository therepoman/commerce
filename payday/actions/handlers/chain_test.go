package handlers

import (
	"context"
	"testing"

	"code.justin.tv/commerce/payday/actions"
	"code.justin.tv/commerce/payday/models/api"
	"github.com/stretchr/testify/assert"
)

type CallsNext struct {
	wasCalled bool
}

func (c *CallsNext) Handle(h actions.Retriever) actions.Retriever {
	return actions.RetrieverFunc(func(ctx context.Context, request api.GetActionsRequest) ([]api.Action, error) {
		c.wasCalled = true
		return h.GetActions(ctx, request)
	})
}

type Terminates struct {
	wasCalled bool
}

func (c *Terminates) Handle(h actions.Retriever) actions.Retriever {
	return actions.RetrieverFunc(func(ctx context.Context, request api.GetActionsRequest) ([]api.Action, error) {
		c.wasCalled = true
		return []api.Action{}, nil
	})
}

func TestEmptyChainReturnsAnError(t *testing.T) {
	chain := HandlerChain{}
	a, err := chain.GetActions(context.Background(), api.GetActionsRequest{ChannelId: "test"})
	assert.Error(t, err)
	assert.Equal(t, 0, len(a))
}

func TestChainCallsAllHandlers(t *testing.T) {
	callsNext := &CallsNext{}
	terminates := &Terminates{}

	chain := new(HandlerChain).AppendHandlers(callsNext.Handle, terminates.Handle)
	_, err := chain.GetActions(context.Background(), api.GetActionsRequest{ChannelId: "test"})

	assert.NoError(t, err)
	assert.True(t, callsNext.wasCalled)
	assert.True(t, terminates.wasCalled)
}
