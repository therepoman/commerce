package handlers

import (
	"context"
	"errors"
	"fmt"
	"testing"
	"time"

	"code.justin.tv/commerce/payday/actions"
	"code.justin.tv/commerce/payday/actions/utils"
	dynamo_actions "code.justin.tv/commerce/payday/dynamo/actions"
	"code.justin.tv/commerce/payday/middleware"
	actions_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/actions"
	metrics_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/metrics"
	"code.justin.tv/commerce/payday/models/api"
	"code.justin.tv/commerce/payday/oauth"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestSixTierCheermoteHandler_Handle(t *testing.T) {
	Convey("Given a sixTierCheermote event handler", t, func() {
		actionsFetcher := new(actions_mock.Fetcher)
		statter := new(metrics_mock.Statter)
		statter.On("TimingDuration", mock.Anything, mock.Anything)

		handler := &SixTierCheermoteHandler{
			Fetcher: actionsFetcher,
			Statter: statter,
		}
		getActionsRequest := api.GetActionsRequest{ChannelId: "123"}

		ctx := context.Background()

		var nextActionRetriever actions.Retriever
		Convey("When next action retriever errors", func() {
			nextActionRetriever = &errorActionRetriever{}

			Convey("Handler should error", func() {
				actionsOut, err := handler.Handle(nextActionRetriever).GetActions(ctx, getActionsRequest)
				So(err, ShouldNotBeNil)
				So(actionsOut, ShouldHaveLength, 0)
			})
		})

		Convey("When next action retriever succeeds", func() {
			nextActionRetriever = &emptyActionRetriever{}

			Convey("When the actions fetcher errors to get the action info", func() {
				actionsFetcher.On("FetchBatch", mock.Anything, []string{actions.CheerPrefix, doodleCheerPrefix}).Return(nil, errors.New("test error fetching action info"))

				Convey("Handler should return an error", func() {
					actionsOut, err := handler.Handle(nextActionRetriever).GetActions(ctx, getActionsRequest)
					So(err, ShouldNotBeNil)
					So(actionsOut, ShouldHaveLength, 0)
				})
			})

			Convey("When the actions fetcher returns successfully with nothing", func() {
				actionsFetcher.On("FetchBatch", mock.Anything, []string{actions.CheerPrefix, doodleCheerPrefix}).Return(map[string]*dynamo_actions.Action{}, nil)

				Convey("Handler should return nothing", func() {
					actionsOut, err := handler.Handle(nextActionRetriever).GetActions(ctx, getActionsRequest)
					So(err, ShouldBeNil)
					So(actionsOut, ShouldHaveLength, 0)
				})
			})

			Convey("When the actions fetcher returns successfully with an action", func() {
				Convey("If the action is active", func() {
					cheerAction := dynamo_actions.Action{
						Prefix:          actions.CheerPrefix,
						CanonicalCasing: "Cheer",
						Type:            api.GlobalFirstPartyAction,
						Support100K:     true,
						StartTime:       time.Date(2018, 1, 1, 1, 0, 0, 0, time.UTC),
					}
					doodleAction := dynamo_actions.Action{
						Prefix:          doodleCheerPrefix,
						CanonicalCasing: "DoodleCheer",
						Type:            api.GlobalThirdPartyAction,
						Support100K:     true,
						StartTime:       time.Date(2018, 1, 1, 1, 0, 0, 0, time.UTC),
					}
					actionsFetcher.On("FetchBatch", mock.Anything, []string{actions.CheerPrefix, doodleCheerPrefix}).Return(map[string]*dynamo_actions.Action{doodleCheerPrefix: &doodleAction, actions.CheerPrefix: &cheerAction}, nil)

					Convey("If the user is on an unsupported client", func() {
						//TODO: replace this with a test of some way of telling whether the client supports hiding sixtiercheermote tiers
						ctx = context.WithValue(context.Background(), middleware.ClientIDContextKey, oauth.AndroidClientId)

						Convey("Handler should return the actions", func() {
							actionsOut, err := handler.Handle(nextActionRetriever).GetActions(ctx, getActionsRequest)
							So(err, ShouldBeNil)
							So(actionsOut, ShouldHaveLength, 2)

							Convey("With regular and without upper tiers included", func() {
								tierIds := utils.GetDefaultTierIds(false)

								for i, tier := range actionsOut[0].Tiers {
									So(tier.Id, ShouldEqual, fmt.Sprint(tierIds[i]))
								}
							})
						})
					})

					Convey("If the user is on a supported client", func() {
						ctx = context.WithValue(context.Background(), middleware.ClientIDContextKey, oauth.DesktopAppClientID)

						Convey("Handler should return the actions", func() {
							actionsOut, err := handler.Handle(nextActionRetriever).GetActions(ctx, getActionsRequest)
							So(err, ShouldBeNil)
							So(actionsOut, ShouldHaveLength, 2)

							Convey("With regular and upper tiers included", func() {
								tierIds := utils.GetDefaultTierIds(true)

								for i, tier := range actionsOut[0].Tiers {
									So(tier.Id, ShouldEqual, fmt.Sprint(tierIds[i]))
								}
							})
						})
					})
				})

				Convey("If the action is not yet active", func() {
					cheerAction := dynamo_actions.Action{
						Prefix:          actions.CheerPrefix,
						CanonicalCasing: "Cheer",
						Type:            api.GlobalFirstPartyAction,
						Support100K:     true,
						StartTime:       time.Date(3018, 1, 1, 1, 0, 0, 0, time.UTC),
					}
					doodleAction := dynamo_actions.Action{
						Prefix:          doodleCheerPrefix,
						CanonicalCasing: "DoodleCheer",
						Type:            api.GlobalThirdPartyAction,
						Support100K:     true,
						StartTime:       time.Date(3018, 1, 1, 1, 0, 0, 0, time.UTC),
					}
					actionsFetcher.On("FetchBatch", mock.Anything, []string{actions.CheerPrefix, doodleCheerPrefix}).Return(map[string]*dynamo_actions.Action{doodleCheerPrefix: &doodleAction, actions.CheerPrefix: &cheerAction}, nil)

					Convey("Handler should return nothing", func() {
						actionsOut, err := handler.Handle(nextActionRetriever).GetActions(ctx, getActionsRequest)
						So(err, ShouldBeNil)
						So(actionsOut, ShouldHaveLength, 0)
					})
				})
			})
		})
	})
}
