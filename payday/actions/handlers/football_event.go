package handlers

import (
	"context"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/actions"
	"code.justin.tv/commerce/payday/actions/constants"
	"code.justin.tv/commerce/payday/cache/channel"
	"code.justin.tv/commerce/payday/events/football"
	"code.justin.tv/commerce/payday/metrics"
	"code.justin.tv/commerce/payday/models/api"
	"code.justin.tv/commerce/payday/utils/pointers"
)

const (
	footballEventCheermotePrefix = "goal"
)

type FootballEventHandler struct {
	ChannelManager channel.ChannelManager `inject:""`
	Fetcher        actions.Fetcher        `inject:""`
	Bouncer        football.Bouncer       `inject:"football-event-bouncer"`
	Statter        metrics.Statter        `inject:""`
}

func (h *FootballEventHandler) Handle(next actions.Retriever) actions.Retriever {
	return actions.RetrieverFunc(func(ctx context.Context, request api.GetActionsRequest) ([]api.Action, error) {
		allActions, err := next.GetActions(ctx, request)
		if err != nil {
			return allActions, err
		}

		if enableStepMetrics {
			startTime := time.Now()
			defer func(st time.Time) {
				go h.Statter.TimingDuration("get_actions_handlers__step_football", time.Since(st))
			}(startTime)
		}

		if !h.Bouncer.Bounce(request.ChannelId) {
			return allActions, nil
		}

		channelInfo, err := h.ChannelManager.Get(ctx, request.ChannelId)
		if err != nil {
			log.WithError(err).Warn("Error looking up channel from dynamo")
			return allActions, nil
		}

		if channelInfo == nil {
			return allActions, nil
		}

		isOptedOut := pointers.BoolOrDefault(channelInfo.CheerBombEventOptOut, api.DefaultCheerBombEventOptOut)
		if !isOptedOut {
			footballAction, err := h.Fetcher.Fetch(ctx, footballEventCheermotePrefix)
			if err != nil {
				log.WithError(err).Error("Error looking up football event cheermote from actions table")
				return allActions, err
			}

			if footballAction != nil {
				action := actions.ConvertDynamoActionToAPIAction(*footballAction)
				action.Order = constants.FootballCheermote
				allActions = append(allActions, action)
			}
		}

		return allActions, nil
	})
}
