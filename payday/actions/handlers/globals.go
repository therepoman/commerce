package handlers

import (
	"context"
	"time"

	"code.justin.tv/commerce/payday/actions"
	"code.justin.tv/commerce/payday/metrics"
	model "code.justin.tv/commerce/payday/models/api"
)

type GlobalsHandler struct {
	Cache                  *actions.ActionCache            `inject:""`
	SupportedActionsPoller actions.ISupportedActionsPoller `inject:""`
	Statter                metrics.Statter                 `inject:""`
}

func (gh *GlobalsHandler) GetActions(ctx context.Context, request model.GetActionsRequest) ([]model.Action, error) {
	if enableStepMetrics {
		startTime := time.Now()
		defer func(st time.Time) {
			go gh.Statter.TimingDuration("get_actions_handlers__step_globals", time.Since(st))
		}(startTime)
	}
	pollerActions := gh.SupportedActionsPoller.GetList()
	return pollerActions, nil
}

func (gh *GlobalsHandler) Handle(next actions.Retriever) actions.Retriever {
	return gh
}
