package handlers

import (
	"context"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/actions"
	"code.justin.tv/commerce/payday/actions/constants"
	dynamo_actions "code.justin.tv/commerce/payday/dynamo/actions"
	"code.justin.tv/commerce/payday/models/api"
)

const (
	charityCheermotePrefix = "charity"
)

type CharityHandler struct {
}

func (h *CharityHandler) Handle(next actions.Retriever) actions.Retriever {
	return actions.RetrieverFunc(func(ctx context.Context, request api.GetActionsRequest) ([]api.Action, error) {
		allActions, err := next.GetActions(ctx, request)
		if err != nil {
			log.WithError(err).Error("Error getting actions from the next piece of the chain")
			return allActions, err
		}

		// Still return charity just so that VODs don't break
		action := actions.ConvertDynamoActionToAPIAction(dynamo_actions.Action{
			Prefix:          charityCheermotePrefix,
			Type:            api.DisplayOnlyAction,
			CanonicalCasing: "Charity",
			StartTime:       time.Date(2018, 9, 10, 0, 0, 0, 0, time.UTC),
			LastUpdated:     time.Date(2018, 8, 31, 22, 36, 22, 0, time.UTC),
			IsCharitable:    true,
		})
		action.Order = constants.DisplayOnlyCheermotes
		action.Type = api.DisplayOnlyAction

		allActions = append(allActions, action)

		return allActions, nil
	})
}
