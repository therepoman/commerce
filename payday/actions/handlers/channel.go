package handlers

import (
	"context"
	"sort"
	"time"

	logger "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/actions"
	"code.justin.tv/commerce/payday/actions/constants"
	"code.justin.tv/commerce/payday/cache/channel"
	"code.justin.tv/commerce/payday/emote"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/image"
	"code.justin.tv/commerce/payday/metrics"
	"code.justin.tv/commerce/payday/models/api"
	"code.justin.tv/commerce/payday/utils/pointers"
	"code.justin.tv/commerce/payday/utils/strings"
)

type ChannelHandler struct {
	ImageManager   image.IImageManager    `inject:""`
	ChannelManager channel.ChannelManager `inject:""`
	PrefixManager  emote.IPrefixManager   `inject:""`
	Statter        metrics.Statter        `inject:""`
}

func (ch *ChannelHandler) Handle(next actions.Retriever) actions.Retriever {
	return actions.RetrieverFunc(func(ctx context.Context, request api.GetActionsRequest) ([]api.Action, error) {
		actions, err := next.GetActions(ctx, request)
		if err != nil {
			return actions, err
		}

		if enableStepMetrics {
			startTime := time.Now()
			defer func(st time.Time) {
				go ch.Statter.TimingDuration("get_actions_handlers__step_channel", time.Since(st))
			}(startTime)
		}

		channelAction := ch.getChannelAction(ctx, request.ChannelId)
		if channelAction != nil {
			actions = append(actions, *channelAction)
		}

		return actions, nil
	})
}

func (ch *ChannelHandler) getChannelAction(ctx context.Context, channelId string) *api.Action {
	if strings.Blank(channelId) {
		return nil
	}

	log := logger.WithField("channel_id", channelId)

	channel, err := ch.ChannelManager.Get(ctx, channelId)
	if err != nil {
		log.WithError(err).Warn("Error looking up channel from dynamo")
		return nil
	}

	if channel == nil {
		return nil
	}

	customCheermoteImageSetId := pointers.StringOrDefault(channel.CustomCheermoteImageSetId, "")
	customCheermoteStatus := pointers.StringOrDefault(channel.CustomCheermoteStatus, api.DefaultCustomCheermoteStatus)
	customCheermoteEnabledInChannel := customCheermoteStatus == api.CustomCheermoteStatusEnabled

	if !customCheermoteEnabledInChannel || strings.Blank(customCheermoteImageSetId) {
		return nil
	}

	log = log.WithField("image_set_id", customCheermoteImageSetId)
	images, err := ch.ImageManager.GetImages(ctx, customCheermoteImageSetId)
	if err != nil {
		log.WithError(err).Warn("Failed to get images")
		return nil
	}

	if !images.AllImagesSet() {
		return nil
	}

	action, hasAction, err := ch.imageMapToChannelAction(ctx, channelId, images)
	if err != nil {
		log.Warn("Failed to convert image map to actions")
		return nil
	}

	if !hasAction {
		return nil
	}
	return &action
}

func (ch *ChannelHandler) imageMapToChannelAction(ctx context.Context, channelId string, imageMap image.ImageMap) (api.Action, bool, error) {
	var action api.Action
	backgrounds := strings.NewSet()
	states := strings.NewSet()
	scales := strings.NewSet()
	lastUpdated := time.Time{}

	tiers, err := imageMap.GetActionTiers(backgrounds, states, scales, lastUpdated, api.ChannelCustomAction)
	if err != nil {
		return api.Action{}, false, err
	}

	sort.Sort(byTier(tiers))

	prefix, hasCustomCheermoteAction, err := ch.PrefixManager.GetCustomCheermoteAction(ctx, channelId)
	if err != nil {
		return action, false, errors.Notef(err, "Failed to get prefix for channelId: %s", channelId)
	}

	if !hasCustomCheermoteAction {
		return action, false, nil
	}

	action = api.Action{
		Prefix:      prefix,
		Scales:      scales.GetList(),
		Tiers:       tiers,
		Backgrounds: backgrounds.GetList(),
		States:      states.GetList(),
		LastUpdated: lastUpdated,
		Type:        api.ChannelCustomAction,
		Order:       constants.BroadcasterCheermote,
	}

	return action, true, nil
}

// SORTING CODE
type byTier []api.ActionTier

func (s byTier) Len() int {
	return len(s)
}
func (s byTier) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}
func (s byTier) Less(i, j int) bool {
	return s[i].BitsMinimum < s[j].BitsMinimum
}
