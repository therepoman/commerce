package handlers

import (
	"context"

	"code.justin.tv/commerce/payday/actions"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/models/api"
)

var enableStepMetrics = false

type Handler func(next actions.Retriever) actions.Retriever

type HandlerChain struct {
	chain []Handler
}

func (h *HandlerChain) WithHandler(handler Handler) *HandlerChain {
	h.chain = append(h.chain, handler)
	return h
}

func (h *HandlerChain) AppendHandlers(handler ...Handler) *HandlerChain {
	h.chain = append(h.chain, handler...)
	return h
}

func (h *HandlerChain) GetActions(ctx context.Context, request api.GetActionsRequest) ([]api.Action, error) {
	return h.retriever().GetActions(ctx, request)
}

func (h *HandlerChain) retriever() actions.Retriever {
	var next actions.Retriever = actions.RetrieverFunc(func(ctx context.Context, request api.GetActionsRequest) ([]api.Action, error) {
		return []api.Action{}, errors.New("Could not get action")
	})

	for i := len(h.chain) - 1; i >= 0; i-- {
		next = h.chain[i](next)
	}

	return next
}
