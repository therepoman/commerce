package handlers

import (
	"context"
	"strconv"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/actions"
	"code.justin.tv/commerce/payday/actions/constants"
	"code.justin.tv/commerce/payday/actions/utils"
	"code.justin.tv/commerce/payday/metrics"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/models/api"
)

const (
	pogChampCheermotePrefix = "pogchamp"
)

var MegaCheermoteUpperTiers = []int64{30000, 50000, 70000, 90000} //representing 30K, 50K, 70K, 90K tiers for PogChamp

type MegaCheermoteHandler struct {
	Fetcher actions.Fetcher `inject:""`
	Statter metrics.Statter `inject:""`
}

func (h *MegaCheermoteHandler) Handle(next actions.Retriever) actions.Retriever {
	return actions.RetrieverFunc(func(ctx context.Context, request api.GetActionsRequest) ([]api.Action, error) {
		allActions, err := next.GetActions(ctx, request)
		if err != nil {
			log.WithError(err).Error("Error getting actions from the next piece of the chain")
			return allActions, err
		}

		if enableStepMetrics {
			startTime := time.Now()
			defer func(st time.Time) {
				go h.Statter.TimingDuration("get_actions_handlers__step_mega_cheermote", time.Since(st))
			}(startTime)
		}

		megaCheermote, err := h.Fetcher.Fetch(ctx, pogChampCheermotePrefix)
		if err != nil {
			log.WithError(err).Error("Error looking up MegaCheermote from actions table")
			return allActions, err
		}

		if megaCheermote != nil && megaCheermote.IsActive() {
			action := actions.ConvertDynamoActionToAPIAction(*megaCheermote)
			action.Order = constants.MegaCheermote

			if request.IncludeUpperTiers {
				action.Tiers = makeUpperTiers(action)
				action = models.ConvertTemplateToURLs(action, actions.GlobalActionsFolder, action.Prefix)
			}

			allActions = append(allActions, action)
		}

		return allActions, nil
	})
}

func makeUpperTiers(action api.Action) []api.ActionTier {
	tiers := action.Tiers

	for _, upperTierAmount := range MegaCheermoteUpperTiers {
		tierId := strconv.FormatInt(upperTierAmount, 10)

		tierForColor := api.Tier(utils.GetCheerTierForThreshold(action.Tiers, upperTierAmount).Id)

		tier := api.ActionTier{
			Id:             tierId,
			BitsMinimum:    upperTierAmount,
			CanCheer:       utils.CanCheerActionTier(upperTierAmount, action.Type),
			ShowInBitsCard: utils.CanShowTierInBitsCard(upperTierAmount, action.Type),
			Color:          constants.TierToColor[tierForColor],
		}

		tiers = append(tiers, tier)
	}
	return tiers
}
