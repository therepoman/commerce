package handlers

import (
	"context"
	"testing"
	"time"

	"code.justin.tv/commerce/payday/actions"
	dynamo_actions "code.justin.tv/commerce/payday/dynamo/actions"
	dynamo_campaigns "code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/campaigns"
	"code.justin.tv/commerce/payday/errors"
	metrics_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/metrics"
	actions_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sponsored_cheermote/actions"
	campaign_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sponsored_cheermote/campaign"
	channel_status_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sponsored_cheermote/channel_status"
	user_campaign_status_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sponsored_cheermote/user_campaign_status"
	model "code.justin.tv/commerce/payday/models/api"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

type emptyActionRetriever struct {
}

func (r *emptyActionRetriever) GetActions(ctx context.Context, request model.GetActionsRequest) ([]model.Action, error) {
	return []model.Action{}, nil
}

type errorActionRetriever struct {
}

func (r *errorActionRetriever) GetActions(ctx context.Context, request model.GetActionsRequest) ([]model.Action, error) {
	return nil, errors.New("test error")
}

func TestSponsoredCheermoteHandler_Handle(t *testing.T) {
	Convey("Given a sponsored cheermote handler", t, func() {
		sponsoredCheermoteManager := new(campaign_mock.Manager)
		sponsoredCheermoteFilterer := new(channel_status_mock.Filterer)
		sponsoredActionsGetter := new(actions_mock.Getter)
		sponsoredUserGetter := new(user_campaign_status_mock.Getter)
		statter := new(metrics_mock.Statter)
		statter.On("TimingDuration", mock.Anything, mock.Anything)

		handler := &SponsoredCheermoteHandler{
			SponsoredCheermoteManager:    sponsoredCheermoteManager,
			SponsoredCheermoteFilterer:   sponsoredCheermoteFilterer,
			SponsoredActionsGetter:       sponsoredActionsGetter,
			SponsoredCheermoteUserGetter: sponsoredUserGetter,
			Statter:                      statter,
		}
		getActionsRequest := model.GetActionsRequest{ChannelId: "123", IncludeSponsored: true}

		var nextActionRetriever actions.Retriever
		Convey("When next action retriever errors", func() {
			nextActionRetriever = &errorActionRetriever{}

			Convey("Handler should error", func() {
				_, err := handler.Handle(nextActionRetriever).GetActions(context.Background(), getActionsRequest)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("When next action retriever succeeds", func() {
			nextActionRetriever = &emptyActionRetriever{}

			Convey("When sponsored cheering is enabled", func() {
				Convey("When GetAllCampaigns errors", func() {
					sponsoredCheermoteManager.On("GetAllCampaigns", mock.Anything).Return(nil, errors.New("test error"))

					Convey("Handler should return nothing", func() {
						_, err := handler.Handle(nextActionRetriever).GetActions(context.Background(), getActionsRequest)
						So(err, ShouldBeNil)
					})
				})

				campaign1 := &dynamo_campaigns.SponsoredCheermoteCampaign{
					CampaignID:           "C1",
					Prefix:               "p1",
					IsEnabled:            true,
					EndTime:              time.Date(3018, 1, 1, 0, 0, 0, 0, time.UTC),
					StartTime:            time.Date(2018, 1, 1, 0, 0, 0, 0, time.UTC),
					UsedBits:             int64(100),
					TotalBits:            int64(10000),
					MinBitsToBeSponsored: int64(10),
					UserLimit:            int64(1000),
				}
				campaign2 := &dynamo_campaigns.SponsoredCheermoteCampaign{
					CampaignID:           "C2",
					Prefix:               "p2",
					IsEnabled:            true,
					EndTime:              time.Date(3018, 1, 1, 0, 0, 0, 0, time.UTC),
					StartTime:            time.Date(2018, 1, 1, 0, 0, 0, 0, time.UTC),
					UsedBits:             int64(100),
					TotalBits:            int64(10000),
					MinBitsToBeSponsored: int64(10),
					UserLimit:            int64(10),
				}
				campaign3 := &dynamo_campaigns.SponsoredCheermoteCampaign{
					CampaignID:           "C3",
					Prefix:               "p3",
					IsEnabled:            true,
					EndTime:              time.Date(3018, 1, 1, 0, 0, 0, 0, time.UTC),
					StartTime:            time.Date(2018, 1, 1, 0, 0, 0, 0, time.UTC),
					UsedBits:             int64(100),
					TotalBits:            int64(10000),
					MinBitsToBeSponsored: int64(10),
					UserLimit:            int64(100),
				}
				campaign4 := &dynamo_campaigns.SponsoredCheermoteCampaign{
					CampaignID:           "C4-copy of C3 but disabled",
					Prefix:               "p3",
					IsEnabled:            false,
					EndTime:              time.Date(3018, 1, 1, 0, 0, 0, 0, time.UTC),
					StartTime:            time.Date(2018, 1, 1, 0, 0, 0, 0, time.UTC),
					UsedBits:             int64(100),
					TotalBits:            int64(10000),
					MinBitsToBeSponsored: int64(10),
					UserLimit:            int64(100),
				}

				Convey("When GetAllCampaigns returns campaigns", func() {
					campaigns := []*dynamo_campaigns.SponsoredCheermoteCampaign{campaign1, campaign2, campaign3, campaign4}
					sponsoredCheermoteManager.On("GetAllCampaigns", mock.Anything).Return(campaigns, nil)

					Convey("When FilterCampaignsByChannel errors", func() {
						sponsoredCheermoteFilterer.On("Filter", mock.Anything, campaigns, getActionsRequest.ChannelId).Return(nil, errors.New("test error"))

						Convey("Handler should error", func() {
							_, err := handler.Handle(nextActionRetriever).GetActions(context.Background(), getActionsRequest)
							So(err, ShouldNotBeNil)
						})
					})

					Convey("When FilterCampaignsByChannel succeeds", func() {
						getActionsRequest.UserId = "234"
						filteredCampaigns := []*dynamo_campaigns.SponsoredCheermoteCampaign{campaign1, campaign2, campaign3}
						sponsoredCheermoteFilterer.On("Filter", mock.Anything, campaigns, getActionsRequest.ChannelId).Return(filteredCampaigns, nil)

						Convey("When GetActionsFromCampaigns errors", func() {
							sponsoredActionsGetter.On("GetBatch", mock.Anything, campaigns).Return(nil, errors.New("test error"))

							Convey("Handler should error", func() {
								_, err := handler.Handle(nextActionRetriever).GetActions(context.Background(), getActionsRequest)
								So(err, ShouldNotBeNil)
							})
						})

						Convey("When GetActionsFromCampaigns succeeds with a single campaigns", func() {
							dynamoActions := map[string]*dynamo_actions.Action{
								"p1": {Prefix: "p1", CanonicalCasing: "P1"},
							}
							sponsoredActionsGetter.On("GetBatch", mock.Anything, campaigns).Return(dynamoActions, nil)

							Convey("When user has contributed", func() {
								sponsoredUserGetter.On("Get", mock.Anything, "C1", mock.Anything).Return(int64(1000000000), nil)

								Convey("When bonus campaign retrieval errors ", func() {
									sponsoredActionsGetter.On("GetBonus", mock.Anything).Return(nil, errors.New("anime-bears-not-allowed-error"))

									actionsOut, err := handler.Handle(nextActionRetriever).GetActions(context.Background(), getActionsRequest)
									So(err, ShouldBeNil)
									So(actionsOut, ShouldHaveLength, 1)
									So(actionsOut[0].Campaign, ShouldNotBeNil)
								})

								Convey("When bonus campaign retrieval works ", func() {
									sponsoredActionsGetter.On("GetBonus", mock.Anything).Return(&dynamo_actions.Action{
										Prefix:          "bonus",
										CreatedTime:     time.Now(),
										Type:            "display_only",
										Support100K:     false,
										LastUpdated:     time.Now(),
										CanonicalCasing: "bonus",
										StartTime:       time.Now(),
										EndTime:         nil,
										IsCharitable:    false,
									}, nil)

									actionsOut, err := handler.Handle(nextActionRetriever).GetActions(context.Background(), getActionsRequest)
									So(err, ShouldBeNil)
									So(actionsOut, ShouldHaveLength, 2)

									// order doesn't matter, because we sort in the api package, so bonus being first isn't a problem (and makes testing easier!)
									So(actionsOut[0].Prefix, ShouldEqual, "bonus")
									So(actionsOut[1].Campaign, ShouldNotBeNil)
									So(actionsOut[1].Prefix, ShouldEqual, "P1")
								})
							})
						})

						Convey("When GetActionsFromCampaigns succeeds with multiple campaigns", func() {
							dynamoActions := map[string]*dynamo_actions.Action{
								"p1": {Prefix: "p1", CanonicalCasing: "P1"},
								"p2": {Prefix: "p2", CanonicalCasing: "P2"},
							}
							sponsoredActionsGetter.On("GetBatch", mock.Anything, campaigns).Return(dynamoActions, nil)
							sponsoredActionsGetter.On("GetBonus", mock.Anything).Return(nil, errors.New("anime-bears-not-allowed-error"))

							Convey("When user has contributed nothing", func() {
								sponsoredUserGetter.On("Get", mock.Anything, mock.Anything, mock.Anything).Return(int64(0), nil)

								actionsOut, err := handler.Handle(nextActionRetriever).GetActions(context.Background(), getActionsRequest)
								So(err, ShouldBeNil)
								So(actionsOut, ShouldHaveLength, 2)
								So(actionsOut[0].Campaign, ShouldNotBeNil)
								So(actionsOut[0].Campaign.UserUsedBits, ShouldEqual, 0)
								So(actionsOut[1].Campaign, ShouldNotBeNil)
								So(actionsOut[1].Campaign.UserUsedBits, ShouldEqual, 0)
								So(actionsOut[0].Campaign.IsEnabled, ShouldBeTrue)
								So(actionsOut[1].Campaign.IsEnabled, ShouldBeTrue)
							})

							Convey("When user has contributed", func() {
								sponsoredUserGetter.On("Get", mock.Anything, "C1", mock.Anything).Return(int64(1), nil)
								sponsoredUserGetter.On("Get", mock.Anything, "C2", mock.Anything).Return(int64(10), nil)
								sponsoredUserGetter.On("Get", mock.Anything, "C3", mock.Anything).Return(int64(1000000000), nil)

								actionsOut, err := handler.Handle(nextActionRetriever).GetActions(context.Background(), getActionsRequest)
								So(err, ShouldBeNil)
								So(actionsOut, ShouldHaveLength, 2)
								So(actionsOut[0].Campaign, ShouldNotBeNil)
								So(actionsOut[1].Campaign, ShouldNotBeNil)
							})

							Convey("When getting user contribution returns an error", func() {
								sponsoredUserGetter.On("Get", mock.Anything, mock.Anything, mock.Anything).Return(int64(0), errors.New("HORRIBLE ERROR"))

								actionsOut, err := handler.Handle(nextActionRetriever).GetActions(context.Background(), getActionsRequest)
								So(err, ShouldBeNil)
								So(actionsOut, ShouldHaveLength, 0)
							})
						})
					})
				})

				Convey("When GetAllCampaigns returns campaigns but in a different order", func() {

					campaigns := []*dynamo_campaigns.SponsoredCheermoteCampaign{campaign1, campaign2, campaign4, campaign3}
					sponsoredCheermoteManager.On("GetAllCampaigns", mock.Anything).Return(campaigns, nil)
					Convey("When FilterCampaignsByChannel succeeds", func() {
						getActionsRequest.UserId = "234"
						filteredCampaigns := []*dynamo_campaigns.SponsoredCheermoteCampaign{campaign1, campaign2, campaign3}
						sponsoredCheermoteFilterer.On("Filter", mock.Anything, campaigns, getActionsRequest.ChannelId).Return(filteredCampaigns, nil)
						Convey("When GetActionsFromCampaigns succeeds with multiple campaigns", func() {
							dynamoActions := map[string]*dynamo_actions.Action{
								"p1": {Prefix: "p1", CanonicalCasing: "P1"},
								"p3": {Prefix: "p3", CanonicalCasing: "P3"},
							}
							sponsoredActionsGetter.On("GetBatch", mock.Anything, campaigns).Return(dynamoActions, nil)
							sponsoredActionsGetter.On("GetBonus", mock.Anything).Return(nil, errors.New("anime-bears-not-allowed-error"))
							Convey("When user has contributed nothing", func() {
								sponsoredUserGetter.On("Get", mock.Anything, mock.Anything, mock.Anything).Return(int64(0), nil)

								actionsOut, err := handler.Handle(nextActionRetriever).GetActions(context.Background(), getActionsRequest)
								So(err, ShouldBeNil)
								So(actionsOut, ShouldHaveLength, 2)
								So(actionsOut[0].Campaign, ShouldNotBeNil)
								So(actionsOut[0].Campaign.UserUsedBits, ShouldEqual, 0)
								So(actionsOut[1].Campaign, ShouldNotBeNil)
								So(actionsOut[1].Campaign.UserUsedBits, ShouldEqual, 0)
								So(actionsOut[0].Campaign.IsEnabled, ShouldBeTrue)
								So(actionsOut[1].Campaign.IsEnabled, ShouldBeTrue)
							})
						})
					})
				})
			})
		})
	})
}
