package handlers

import (
	"context"
	"errors"
	"fmt"
	"testing"
	"time"

	"code.justin.tv/commerce/payday/actions"
	"code.justin.tv/commerce/payday/actions/utils"
	dynamo_actions "code.justin.tv/commerce/payday/dynamo/actions"
	actions_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/actions"
	metrics_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/metrics"
	"code.justin.tv/commerce/payday/models/api"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestMegaCheermoteHandler_Handle(t *testing.T) {
	Convey("Given a megacheermote event handler", t, func() {
		actionsFetcher := new(actions_mock.Fetcher)
		statter := new(metrics_mock.Statter)
		statter.On("TimingDuration", mock.Anything, mock.Anything)

		handler := &MegaCheermoteHandler{
			Fetcher: actionsFetcher,
			Statter: statter,
		}
		getActionsRequest := api.GetActionsRequest{ChannelId: "123"}

		ctx := context.Background()

		var nextActionRetriever actions.Retriever
		Convey("When next action retriever errors", func() {
			nextActionRetriever = &errorActionRetriever{}

			Convey("Handler should error", func() {
				actionsOut, err := handler.Handle(nextActionRetriever).GetActions(ctx, getActionsRequest)
				So(err, ShouldNotBeNil)
				So(actionsOut, ShouldHaveLength, 0)
			})
		})

		Convey("When next action retriever succeeds", func() {
			nextActionRetriever = &emptyActionRetriever{}

			Convey("When the actions fetcher errors to get the action info", func() {
				actionsFetcher.On("Fetch", mock.Anything, pogChampCheermotePrefix).Return(nil, errors.New("test error fetching action info"))

				Convey("Handler should return an error", func() {
					actionsOut, err := handler.Handle(nextActionRetriever).GetActions(ctx, getActionsRequest)
					So(err, ShouldNotBeNil)
					So(actionsOut, ShouldHaveLength, 0)
				})
			})

			Convey("When the actions fetcher returns successfully with nothing", func() {
				actionsFetcher.On("Fetch", mock.Anything, pogChampCheermotePrefix).Return(nil, nil)

				Convey("Handler should return nothing", func() {
					actionsOut, err := handler.Handle(nextActionRetriever).GetActions(ctx, getActionsRequest)
					So(err, ShouldBeNil)
					So(actionsOut, ShouldHaveLength, 0)
				})
			})

			Convey("When the actions fetcher returns successfully with an action", func() {
				Convey("If the action is active", func() {
					action := dynamo_actions.Action{
						Prefix:          pogChampCheermotePrefix,
						CanonicalCasing: "PogChamp",
						Type:            api.GlobalFirstPartyAction,
						Support100K:     false,
						StartTime:       time.Date(2018, 1, 1, 1, 0, 0, 0, time.UTC),
					}
					actionsFetcher.On("Fetch", mock.Anything, pogChampCheermotePrefix).Return(&action, nil)

					Convey("If the caller sets the include_upper_tiers parameter in the request", func() {
						getActionsRequestWithUpperTiers := api.GetActionsRequest{ChannelId: "123", IncludeUpperTiers: true}

						Convey("Handler should return the action", func() {
							actionsOut, err := handler.Handle(nextActionRetriever).GetActions(ctx, getActionsRequestWithUpperTiers)
							So(err, ShouldBeNil)
							So(actionsOut, ShouldHaveLength, 1)

							Convey("With regular and upper tiers included", func() {
								tierIds := utils.GetDefaultTierIds(false)
								for _, megaCheermoteUpperTier := range MegaCheermoteUpperTiers {
									tierIds = append(tierIds, int(megaCheermoteUpperTier))
								}

								for i, tier := range actionsOut[0].Tiers {
									So(tier.Id, ShouldEqual, fmt.Sprint(tierIds[i]))
								}
							})
						})
					})
				})

				Convey("If the action is not yet active", func() {
					action := dynamo_actions.Action{
						Prefix:          pogChampCheermotePrefix,
						CanonicalCasing: "PogChamp",
						Type:            api.GlobalFirstPartyAction,
						Support100K:     false,
						StartTime:       time.Date(3018, 1, 1, 1, 0, 0, 0, time.UTC),
					}
					actionsFetcher.On("Fetch", mock.Anything, pogChampCheermotePrefix).Return(&action, nil)

					Convey("Handler should return nothing", func() {
						actionsOut, err := handler.Handle(nextActionRetriever).GetActions(ctx, getActionsRequest)
						So(err, ShouldBeNil)
						So(actionsOut, ShouldHaveLength, 0)
					})
				})
			})
		})
	})
}
