package handlers

import (
	"context"
	"time"

	"code.justin.tv/commerce/payday/actions"
	"code.justin.tv/commerce/payday/actions/constants"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/metrics"
	model "code.justin.tv/commerce/payday/models/api"
)

var SGDQ2017StartDate = time.Date(2017, time.July, 2, 0, 0, 0, 0, time.UTC)

type SGDQ2017Handler struct {
	Config  *config.Configuration `inject:""`
	Statter metrics.Statter       `inject:""`
}

func (h *SGDQ2017Handler) getSGDQ2017Actions(ctx context.Context, channelId string) []model.Action {
	if channelId != h.Config.SGDQChannel {
		return []model.Action{}
	}

	actionsMap := actions.GetActions([]string{"gdqMash", "gdqBlind"}, "actions", SGDQ2017StartDate)
	acs := make([]model.Action, 0)
	for _, action := range actionsMap {
		action.Order = constants.BroadcasterCheermote
		acs = append(acs, action)
	}
	return acs
}

func (h *SGDQ2017Handler) Handle(next actions.Retriever) actions.Retriever {
	return actions.RetrieverFunc(func(ctx context.Context, request model.GetActionsRequest) ([]model.Action, error) {
		actions, err := next.GetActions(ctx, request)
		if err != nil {
			return actions, err
		}

		if enableStepMetrics {
			startTime := time.Now()
			defer func(st time.Time) {
				go h.Statter.TimingDuration("get_actions_handlers__step_sgdq2017", time.Since(st))
			}(startTime)
		}

		sgdqActions := h.getSGDQ2017Actions(ctx, request.ChannelId)
		if len(sgdqActions) > 0 {
			actions = append(actions, sgdqActions...)
		}

		return actions, nil
	})
}
