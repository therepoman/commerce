package utils

import (
	"context"
	"sort"
	"strconv"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/actions/constants"
	"code.justin.tv/commerce/payday/dynamo/sponsored_cheermote/campaigns"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/middleware"
	"code.justin.tv/commerce/payday/models/api"
	"code.justin.tv/commerce/payday/oauth"
)

const (
	MaximumCheermoteMaximum = 100000
	MaximumBitsMessage      = 250000
	MaximumShownInBitsCard  = 10000
)

func CanCheerActionTier(tierThreshold int64, actionType string) bool {
	return tierThreshold <= MaximumCheermoteMaximum && actionType != api.DisplayOnlyAction
}

func CanShowTierInBitsCard(tierThreshold int64, actionType string) bool {
	return tierThreshold <= MaximumShownInBitsCard && actionType != api.DisplayOnlyAction && actionType != api.GlobalThirdPartyAction
}

func GetCheerTierForThreshold(tiers []api.ActionTier, threshold int64) (tier api.ActionTier) {
	for _, t := range tiers {
		minBitsForTier := t.BitsMinimum
		if threshold >= minBitsForTier {
			tier = t
		}
	}
	return
}

func NewDefaultTier(include100k bool, actionType string) []api.ActionTier {
	actions := make([]api.ActionTier, 0)
	tierIds := GetDefaultTierIds(include100k)
	for _, tierId := range tierIds {
		numericTier := int64(tierId)
		tierIdString := strconv.FormatInt(numericTier, 10)
		tierColorString := constants.TierToColor[api.Tier(tierIdString)]
		canCheer := CanCheerActionTier(numericTier, actionType)
		showInBitsCard := CanShowTierInBitsCard(numericTier, actionType)
		actions = append(actions, api.ActionTier{
			BitsMinimum:    numericTier,
			Id:             tierIdString,
			Color:          tierColorString,
			CanCheer:       canCheer,
			ShowInBitsCard: showInBitsCard,
		})

	}
	return actions
}

func GetDefaultTierIds(include100k bool) []int {
	tierIds := make([]int, 0)
	for tierId := range constants.TierToColor {
		tierIdString := string(tierId)
		numericTier, _ := strconv.ParseInt(tierIdString, 10, 64)
		if numericTier < 100000 || include100k {
			tierIds = append(tierIds, int(numericTier))
		}
	}
	sort.Ints(tierIds)
	return tierIds
}

func GetClientType(ctx context.Context) string {
	var clientId string
	clientIdFromContext := ctx.Value(middleware.ClientIDContextKey)

	if clientIdFromContext == nil {
		clientId = ""
	} else {
		clientIdString, ok := clientIdFromContext.(string)
		if !ok {
			err := errors.New("Could not get client id from context")
			log.WithError(err).Error()
			return ""
		}
		clientId = clientIdString
	}

	platform, _ := oauth.GetClient(clientId)

	return platform
}

func SupportsUpperTiers(clientType string) bool {
	//TODO: replace this with some way of telling whether the client supports hiding sixtiercheermote  tiers
	return clientType == oauth.DesktopPlatform || clientType == oauth.WebClientPlatform
}

func AddCampaignToAction(action api.Action, campaign *campaigns.SponsoredCheermoteCampaign, userUsedBits int64) api.Action {
	action.Campaign = &api.ActionCampaign{
		ID:                        campaign.CampaignID,
		StartTime:                 campaign.StartTime,
		EndTime:                   campaign.EndTime,
		TotalBits:                 campaign.TotalBits,
		UsedBits:                  campaign.UsedBits,
		IsEnabled:                 campaign.IsEnabled,
		SponsoredAmountThresholds: campaign.SponsoredAmountThresholds,
		MinBitsToBeSponsored:      campaign.MinBitsToBeSponsored,
		UserLimit:                 campaign.UserLimit,
		UserUsedBits:              userUsedBits,
		BrandName:                 campaign.BrandName,
		BrandImageURL:             campaign.BrandImageURL,
	}

	return action
}
