package actions

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/mock"

	"code.justin.tv/commerce/gogogadget/ttlcache"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/dynamo/actions"
	"code.justin.tv/commerce/payday/metrics"
	redis_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/redis"
	"code.justin.tv/commerce/payday/models/api"
	"github.com/go-redis/redis"
	. "github.com/smartystreets/goconvey/convey"
)

func TestCacheImpl_Get(t *testing.T) {
	Convey("Given an actions cache", t, func() {
		ctx := context.Background()
		redisClient := new(redis_mock.Client)

		cache := &cache{
			RedisClient:   redisClient,
			InMemoryCache: ttlcache.NewCache(10 * time.Millisecond),
			Statter:       metrics.NewNoopStatter(),
		}

		actionPrefix := "walrus"
		cacheKey := fmt.Sprintf(actionsKeyFormat, actionPrefix)

		cachedAction := actions.Action{
			Prefix:          actionPrefix,
			CanonicalCasing: "Walrus",
			Support100K:     false,
			Type:            api.GlobalFirstPartyAction,
			StartTime:       time.Date(2018, 1, 1, 1, 0, 0, 0, time.UTC),
		}

		cachedActionBytes, err := json.Marshal(cachedAction)
		if err != nil {
			log.WithError(err).Error("Error marshaling cachedAction")
			t.Fail()
		}

		Convey("Given a record exists in the cache", func() {
			getResp := redis.NewStringResult(string(cachedActionBytes), nil)
			redisClient.On("Get", mock.Anything, cacheKey).Return(getResp)

			Convey("It should return the correct record", func() {
				actual := cache.Get(ctx, actionPrefix)
				So(actual, ShouldNotBeNil)
				So(actual.Prefix, ShouldEqual, actionPrefix)
			})
		})

		Convey("Given there is an error fetching the record from cache", func() {
			getResp := redis.NewStringResult("", errors.New("WALRUS ERROR"))
			redisClient.On("Get", mock.Anything, cacheKey).Return(getResp)

			Convey("It should return nil", func() {
				actual := cache.Get(ctx, actionPrefix)
				So(actual, ShouldBeNil)
			})
		})

		Convey("Given the record does not exist in the cache", func() {
			getResp := redis.NewStringResult("", nil)
			redisClient.On("Get", mock.Anything, cacheKey).Return(getResp)

			Convey("It should return nil", func() {
				actual := cache.Get(ctx, actionPrefix)
				So(actual, ShouldBeNil)
			})
		})

		Convey("Given the record is not an Action struct", func() {
			getResp := redis.NewStringResult(string(cachedActionBytes[5:]), nil)
			redisClient.On("Get", mock.Anything, cacheKey).Return(getResp)
			delResp := redis.NewIntCmd("del", nil)
			redisClient.On("Del", mock.Anything, cacheKey).Return(delResp)

			Convey("It should return nil and delete the record", func() {
				actual := cache.Get(ctx, actionPrefix)
				So(actual, ShouldBeNil)
				redisClient.AssertCalled(t, "Del", mock.Anything, cacheKey)
			})
		})
	})
}
