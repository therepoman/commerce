package actions

import (
	"context"
	"math/rand"
	"sync"
	"time"

	"code.justin.tv/chat/golibs/graceful"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/actions/constants"
	"code.justin.tv/commerce/payday/actions/utils"
	dynamo_actions "code.justin.tv/commerce/payday/dynamo/actions"
	"code.justin.tv/commerce/payday/models"
	model "code.justin.tv/commerce/payday/models/api"
	"code.justin.tv/commerce/payday/s3"
	"gopkg.in/yaml.v2"
)

const (
	refreshDelayMaxRandomOffset = time.Second * 30
	refreshDelay                = time.Minute * 5
	defaultCheer                = "cheer"
)

type ISupportedActionsPoller interface {
	RefreshActionListAtInterval()
	GetList() []model.Action
	Init() error
}

type SupportedActionsPoller struct {
	actions  []model.Action
	S3Client s3.IS3Client `inject:""`
	bucket   string
	key      string
	mutex    *sync.RWMutex
	Fetcher  Fetcher `inject:""`
}

type S3Action struct {
	Prefix string `yaml:"prefix"`
}

func (sa *SupportedActionsPoller) getSupportedActionsFromDynamo(ctx context.Context, supportedActions []S3Action) ([]model.Action, error) {
	prefixes := []string{defaultCheer}
	for _, supportedAction := range supportedActions {
		prefixes = append(prefixes, supportedAction.Prefix)
	}

	dynamoActions, err := sa.Fetcher.FetchBatch(ctx, prefixes)
	if err != nil {
		return nil, err
	}
	modelActions := make([]model.Action, 0)

	for i, prefix := range prefixes {
		dynamoAction, ok := dynamoActions[prefix]
		if ok && dynamoAction.IsActive() {
			priority := 0
			if prefix != defaultCheer {
				priority = i + constants.GlobalCheermotes
			}

			action := convertDynamoActionToAPIAction(*dynamoAction, priority)
			modelActions = append(modelActions, action)
		}
	}

	return modelActions, nil
}

func NewSupportedActionsPoller(bucket string, key string) ISupportedActionsPoller {
	actions := make([]model.Action, 0)

	mutex := &sync.RWMutex{}

	return &SupportedActionsPoller{
		actions: actions,
		mutex:   mutex,
		bucket:  bucket,
		key:     key,
	}
}

func NewTestSupportedActionsPoller(actions []model.Action) ISupportedActionsPoller {
	mutex := &sync.RWMutex{}
	return &SupportedActionsPoller{
		actions: actions,
		mutex:   mutex,
	}
}

func createSupportedPrefixesListFromS3(s3client s3.IS3Client, bucket string, key string) ([]S3Action, error) {
	tagBytes, err := s3client.LoadFile(bucket, key)
	if err != nil {
		log.WithFields(log.Fields{
			"key":    key,
			"bucket": bucket,
		}).Error("could not load file from S3 for SupportedActions", err)
		return nil, err
	}

	return loadActionsYaml(tagBytes)
}

func loadActionsYaml(bytes []byte) ([]S3Action, error) {
	actions := make([]S3Action, 0)
	err := yaml.Unmarshal(bytes, &actions)
	if err != nil {
		return nil, err
	}

	return actions, nil
}

func (sa *SupportedActionsPoller) refreshActionList(ctx context.Context) error {

	s3Actions, err := createSupportedPrefixesListFromS3(sa.S3Client, sa.bucket, sa.key)
	if err != nil {
		return err
	}

	actions, err := sa.getSupportedActionsFromDynamo(ctx, s3Actions)
	if err != nil {
		return err
	}

	sa.mutex.Lock()
	defer sa.mutex.Unlock()

	sa.actions = actions

	return nil
}

func (sa *SupportedActionsPoller) Init() error {
	ctx := context.Background()
	if exists, err := sa.S3Client.BucketExists(sa.bucket); !exists || err != nil {
		log.WithError(err).WithField("bucket", sa.bucket).Error("Bucket does not exist for SupportedActions object creation")
		return err
	}

	if exists, err := sa.S3Client.FileExists(sa.bucket, sa.key); !exists || err != nil {
		log.WithFields(log.Fields{
			"key":    sa.key,
			"bucket": sa.bucket,
		}).WithError(err).Error("Key does not exist in Bucket for SupportedActions object creation")
		return err
	}

	err := sa.refreshActionList(ctx)
	if err != nil {
		log.WithError(err).Error("Error while refreshing action list")
		return err
	}

	return nil
}

func (sa *SupportedActionsPoller) RefreshActionListAtInterval() {
	ctx := context.Background()
	delayOffset := rand.Int63n(int64(refreshDelayMaxRandomOffset))
	timer := time.NewTicker(refreshDelay + time.Duration(delayOffset))

	for {
		select {
		case <-graceful.ShuttingDown():
			return
		case <-timer.C:
			err := sa.refreshActionList(ctx)
			if err != nil {
				log.WithError(err).Error("Error refreshing actions")
			}
		}
	}
}

func (sa *SupportedActionsPoller) GetList() []model.Action {
	sa.mutex.RLock()
	defer sa.mutex.RUnlock()

	return sa.actions
}

func convertDynamoActionToAPIAction(dynamoAction dynamo_actions.Action, priority int) model.Action {
	action := model.Action{
		Prefix:       dynamoAction.CanonicalCasing,
		Type:         dynamoAction.Type,
		LastUpdated:  dynamoAction.LastUpdated,
		Order:        priority,
		Backgrounds:  defaultBackgrounds,
		Scales:       defaultScales,
		States:       defaultStates,
		Tiers:        utils.NewDefaultTier(false, dynamoAction.Type), //TODO: change this false to dynamoAction.Support100K
		IsCharitable: dynamoAction.IsCharitable,
	}
	return models.ConvertTemplateToURLs(action, GlobalActionsFolder, dynamoAction.Prefix)
}
