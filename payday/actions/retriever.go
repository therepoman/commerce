package actions

import (
	"context"

	"code.justin.tv/commerce/payday/models/api"
)

type Retriever interface {
	GetActions(ctx context.Context, request api.GetActionsRequest) ([]api.Action, error)
}

type RetrieverFunc func(ctx context.Context, request api.GetActionsRequest) ([]api.Action, error)

func (h RetrieverFunc) GetActions(ctx context.Context, request api.GetActionsRequest) ([]api.Action, error) {
	return h(ctx, request)
}
