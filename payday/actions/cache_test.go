package actions

import (
	"testing"

	"code.justin.tv/commerce/payday/test"
	"github.com/stretchr/testify/assert"
)

func TestActionCache(t *testing.T) {
	var err error
	dynamoContext, err := test.NewDynamoTestContext("action")
	assert.NoError(t, err)

	err = dynamoContext.Cleanup()
	assert.NoError(t, err)

	cache, err := NewActionCache("test")
	assert.NoError(t, err)

	cheer, success := cache.Get("cheer")
	assert.NotNil(t, cheer)
	assert.True(t, success)

	kappa, success := cache.Get("kappa")
	assert.NotNil(t, kappa)
	assert.True(t, success)

	kreygasm, success := cache.Get("kreygasm")
	assert.NotNil(t, kreygasm)
	assert.True(t, success)

	err = dynamoContext.Cleanup()
	assert.NoError(t, err)
}
