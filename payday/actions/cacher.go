package actions

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"code.justin.tv/commerce/gogogadget/ttlcache"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/clients/redis"
	"code.justin.tv/commerce/payday/dynamo/actions"
	"code.justin.tv/commerce/payday/hystrix"
	cmds "code.justin.tv/commerce/payday/hystrix"
	"code.justin.tv/commerce/payday/metrics"
	"code.justin.tv/commerce/payday/models/api"
	"code.justin.tv/commerce/payday/utils/strings"
	go_redis "github.com/go-redis/redis"
)

var actionTypesToCacheInMemory = map[string]interface{}{
	api.GlobalFirstPartyAction: nil,
	api.GlobalThirdPartyAction: nil,
	api.DisplayOnlyAction:      nil,
	api.SponsoredAction:        nil,
}

const (
	actionsKeyFormat             = "actions-%s"
	inMemoryCacheTTL             = time.Minute
	redisCacheTTL                = time.Minute * 5
	inMemoryCacheCleanupInterval = 10 * time.Second
)

type Cache interface {
	Get(ctx context.Context, prefix string) *actions.Action
	Set(ctx context.Context, prefix string, action actions.Action)
	Del(ctx context.Context, prefix string)
}

type cache struct {
	RedisClient   redis.Client    `inject:"redisClient"`
	Statter       metrics.Statter `inject:""`
	InMemoryCache ttlcache.Cache
}

func NewCache() Cache {
	return &cache{
		InMemoryCache: ttlcache.NewCache(inMemoryCacheCleanupInterval),
	}
}

func (c *cache) Get(ctx context.Context, prefix string) *actions.Action {
	cachedItem := c.InMemoryCache.Get(prefix)
	if cachedItem != nil {
		cachedAction, ok := cachedItem.(actions.Action)
		if ok {
			c.EmitMetric("in_memory", &cachedAction)
			return &cachedAction
		} else {
			log.WithField("prefix", prefix).Error("received the wrong type when getting action from in-memory cache")
		}
	}

	var actionsCacheValue string
	err := hystrix.Do(cmds.ActionsCacheGet, func() error {
		var err error
		actionsCacheValue, err = c.RedisClient.Get(ctx, fmt.Sprintf(actionsKeyFormat, prefix)).Result()
		if err != nil && err != go_redis.Nil {
			log.WithError(err).Error("Error getting action out of cache")
		}
		return nil
	}, nil)
	if err != nil {
		log.WithError(err).Warn("hystrix error while fetching actions from cache")
	}

	if strings.Blank(actionsCacheValue) {
		c.EmitMetric("redis", nil)
		return nil
	}

	var action actions.Action
	err = json.Unmarshal([]byte(actionsCacheValue), &action)
	if err != nil {
		c.Del(ctx, prefix)
		log.WithError(err).Error("Error unmarshaling action")
		return nil
	}

	if shouldUseInMemoryCache(action) {
		c.InMemoryCache.Set(prefix, action, inMemoryCacheTTL)
	}

	c.EmitMetric("redis", &action)
	return &action
}

func (c *cache) Set(ctx context.Context, prefix string, action actions.Action) {
	if shouldUseInMemoryCache(action) {
		c.InMemoryCache.Set(prefix, action, inMemoryCacheTTL)
	}

	actionJson, err := json.Marshal(action)
	if err != nil {
		log.WithError(err).Error("Error marshaling action")
		return
	}

	err = hystrix.Do(cmds.ActionsCacheSet, func() error {
		err := c.RedisClient.Set(ctx, fmt.Sprintf(actionsKeyFormat, prefix), string(actionJson), redisCacheTTL).Err()
		if err != nil {
			log.WithError(err).Error("Error setting action in cache")
		}
		return nil
	}, nil)
	if err != nil {
		log.WithError(err).Warn("hystrix error setting actions in cache")
	}
}

func (c *cache) Del(ctx context.Context, prefix string) {
	c.InMemoryCache.Delete(prefix)

	err := hystrix.Do(cmds.ActionsCacheDel, func() error {
		err := c.RedisClient.Del(ctx, fmt.Sprintf(actionsKeyFormat, prefix)).Err()
		if err != nil {
			log.WithError(err).Error("Error deleting action from cache")
		}
		return nil
	}, nil)

	if err != nil {
		log.WithError(err).Warn("hystrix error deleting actions in cache")
	}
}

func shouldUseInMemoryCache(action actions.Action) bool {
	_, ok := actionTypesToCacheInMemory[action.Type]
	return ok
}

func (c *cache) EmitMetric(src string, action *actions.Action) {
	actionType := "nil"
	if action != nil {
		actionType = action.Type
	}

	c.Statter.Inc(fmt.Sprintf("retrieved_action_from_cache.%s.%s", src, actionType), 1)
}
