package actions

import (
	"context"

	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/payday/dynamo/actions"
)

type Fetcher interface {
	Fetch(ctx context.Context, prefix string) (*actions.Action, error)
	FetchBatch(ctx context.Context, prefixes []string) (map[string]*actions.Action, error)
}

type fetcher struct {
	Dao   actions.IActionDao `inject:""`
	Cache Cache              `inject:""`
}

func NewFetcher() Fetcher {
	return &fetcher{}
}

func (f *fetcher) Fetch(ctx context.Context, prefix string) (*actions.Action, error) {
	cachedRecord := f.Cache.Get(ctx, prefix)
	if cachedRecord != nil {
		return cachedRecord, nil
	}

	dynamoRecord, err := f.Dao.Get(prefix)
	if err != nil || dynamoRecord == nil {
		return nil, err
	}

	f.Cache.Set(ctx, prefix, *dynamoRecord)
	return dynamoRecord, nil
}

func (f *fetcher) FetchBatch(ctx context.Context, prefixes []string) (map[string]*actions.Action, error) {
	records := make(map[string]*actions.Action)
	prefixesToFetch := make([]string, 0)
	for _, prefix := range prefixes {
		cachedDynamoRecord := f.Cache.Get(ctx, prefix)
		if cachedDynamoRecord != nil {
			records[prefix] = cachedDynamoRecord
		} else {
			prefixesToFetch = append(prefixesToFetch, prefix)
		}
	}

	if len(prefixesToFetch) > 0 {
		dynamoActions, err := f.Dao.GetBatch(deduplicatePrefixes(prefixesToFetch))
		if err != nil {
			return nil, err
		}

		for _, action := range dynamoActions {
			if action != nil {
				f.Cache.Set(ctx, action.Prefix, *action)
				records[action.Prefix] = action
			}
		}
	}

	return records, nil
}

func deduplicatePrefixes(prefixes []string) []string {
	set := strings.NewSet()
	for _, prefix := range prefixes {
		set.Add(prefix)
	}
	return set.GetList()
}
