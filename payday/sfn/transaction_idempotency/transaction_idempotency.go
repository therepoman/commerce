package transaction_idempotency

import (
	"context"
	"encoding/json"
	"errors"
	"time"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/dynamo/balance_change_processing"
)

const (
	// This assumes that the max execution duration for Lambdas using this utility is 10s (which is currently the case).
	// If we add longer running Lambdas, this will need to be configurable.
	lambdaTimeoutDuration = 10 * time.Second
)

type Enforcer interface {
	/**
	Ensures balance change processing steps are idempotent. If a transaction has already been processed successfully,
	cached output from the previous execution will be unmarshaled into cachedOutput
	*/
	Enforce(ctx context.Context, transactionID, stepIdentifier string, input interface{}, cachedOutput interface{}) (ok bool, err error)
	/**
	Marks processing as complete for a transaction, storing step output for:
		* auditing
		* cache of step output (useful if parent sfn needs to be redriven)
	If handlerErr is populated, the record for the step transaction will be moved into an error state
	*/
	Finalize(ctx context.Context, transactionID, stepIdentifier string, output interface{}, handlerErr error)
}

type enforcerImpl struct {
	BalanceChangeProcessingDAO balance_change_processing.BalanceChangeProcessingDAO `inject:""`
}

func NewIdempotencyEnforcer() Enforcer {
	return &enforcerImpl{}
}

func (e *enforcerImpl) Enforce(ctx context.Context, transactionID, stepIdentifier string, input interface{}, cachedOutput interface{}) (ok bool, err error) {
	log := logrus.WithField("transactionID", transactionID).WithField("stepIdentifier", stepIdentifier)

	record, err := e.BalanceChangeProcessingDAO.GetRecord(ctx, transactionID, stepIdentifier)
	if err != nil {
		return false, err
	}

	if record != nil {
		switch record.State {
		case balance_change_processing.ProcessingState:
			// If the Lambda is stuck in the processing state for 10 seconds or more, allow the task to be retried. We can
			// assume that the previous attempt timed out or hit a Lambda internal error before it had a chance to complete.
			//
			// A better solution would be to catch this class of error and correct the dynamo state, but the engineering
			// effort for such a rare case is not currently worth.
			if record.CreatedAt.Before(time.Now().Add(-lambdaTimeoutDuration)) {
				log.Info("encountered potential deadlock in idempotency enforcer, allowing work")
				break
			}

			message := "conflicting transaction processing balance change"
			log.Error(message)
			return false, errors.New(message)
		case balance_change_processing.CompleteState:
			if record.Output != "" && cachedOutput != nil {
				err := json.Unmarshal([]byte(record.Output), cachedOutput)
				if err != nil {
					message := "error marshaling balance change processing cachedOutput"
					log.Error(message)
					return false, errors.New(message)
				}
			} else {
				log.Info("Idempotency Check: this balance change processing step was previously completed.")
			}

			return false, nil
		case balance_change_processing.ErrorState:
			break // previous execution attempt erred
		default:
			message := "unhandled state while processing balance change"
			log.Error(message)
			return false, errors.New(message)
		}
	}

	inputJSON, err := json.Marshal(input)
	if err != nil {
		log.WithError(err).Error("error marshaling balance change processing input")
		return false, err
	}

	err = e.BalanceChangeProcessingDAO.CreateRecord(ctx, transactionID, stepIdentifier, string(inputJSON))
	if err != nil {
		log.WithError(err).Error("error creating idempotency record for balance change processing in dynamoDB")
		return false, err
	}

	return true, nil
}

func (e *enforcerImpl) Finalize(ctx context.Context, transactionID, stepIdentifier string, output interface{}, handlerErr error) {
	log := logrus.WithField("transactionID", transactionID).WithField("stepIdentifier", stepIdentifier)
	var outputStr string

	// Can't store an empty string, so if a step has no output, default to any empty json object {}
	if output == nil {
		output = struct{}{}
	}

	outputJSON, err := json.Marshal(output)
	if err != nil {
		log.WithError(err).Error("error marshaling balance change processing output")
		return
	}
	outputStr = string(outputJSON)

	err = e.BalanceChangeProcessingDAO.FinalizeRecord(ctx, transactionID, stepIdentifier, outputStr, handlerErr)

	if err != nil {
		log.WithError(err).Error("Finalizing Record: failed to write sfn idempotency record to dynamoDB")
	}
}
