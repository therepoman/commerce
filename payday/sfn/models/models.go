package models

import (
	"time"

	mako_client "code.justin.tv/commerce/mako/twirp"
	"code.justin.tv/commerce/payday/models"
)

type BitsUsageEntitlerInput struct {
	TransactionId          string `json:"transactionId"`
	TransactionType        string `json:"transactionType"`
	BitsProcessed          int    `json:"totalBits"`
	TotalBitsToBroadcaster int    `json:"totalBitsGivenFromUserToBroadcaster"`
	RequestingTwitchUserId string `json:"requestingTwitchUserId"`
	TargetTwitchUserId     string `json:"targetTwitchUserId"`

	ShouldPubsub bool `json:"shouldPubsub"`

	// Everything below this line is only used when ShouldPubsub=true
	TimeOfEvent      time.Time      `json:"timeOfEvent"`
	IsAnonymous      bool           `json:"anonymous"`
	ExtraContext     string         `json:"extraContext"`
	SponsoredAmounts map[string]int `json:"sponsoredAmounts"`
}

type EntitlementPubsubInput struct {
	OriginalInput     BitsUsageEntitlerInput `json:"originalInput"`
	EmoteEntitlements EmoteEntitlerOutput    `json:"emoteEntitlements"`
	BadgeEntitlements BadgeEntitlerOutput    `json:"badgeEntitlements"`
}

type UserDestroyInput struct {
	UserId string `json:"user_id"`
}

type EmoteEntitlerOutput struct {
	Emotes *[]mako_client.Emoticon `json:"emotes"`
}

type BadgeEntitlerOutput struct {
	Badge *models.BadgeEntitlement `json:"badge"`
}

type UserDestroyOutput struct {
	UserId string `json:"user_id"`
}

type UserDestroyReportDeletionOutput struct {
	UserId    string    `json:"user_id"`
	Timestamp time.Time `json:"timestamp"`
}
