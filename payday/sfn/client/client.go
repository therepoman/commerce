package client

import (
	"encoding/json"
	"time"

	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/metrics"
	"code.justin.tv/commerce/payday/sfn/models"
	"github.com/aws/aws-sdk-go/service/sfn"
	"github.com/aws/aws-sdk-go/service/sfn/sfniface"
)

const (
	startExecutionLatencyStatName = "sfn_start_execution_latency"
	startExecutionErrorStatName   = "sfn_start_execution_error"
)

type SFNClient interface {
	ExecuteBitsUsageEntitlementsStateMachine(executionInput models.BitsUsageEntitlerInput) error
	ExecuteUserDestroyStateMachine(executioninput models.UserDestroyInput) error
}

func NewSFNClient() SFNClient {
	return &sfnClientImpl{}
}

type sfnClientImpl struct {
	SFN     sfniface.SFNAPI       `inject:""`
	Config  *config.Configuration `inject:""`
	Statter metrics.Statter       `inject:""`
}

func (s *sfnClientImpl) ExecuteBitsUsageEntitlementsStateMachine(executionInput models.BitsUsageEntitlerInput) error {
	return s.execute(s.Config.BitsUsageEntitlementsSFNARN, executionInput.TransactionId, executionInput)
}

func (s *sfnClientImpl) ExecuteUserDestroyStateMachine(executionInput models.UserDestroyInput) error {
	return s.execute(s.Config.UserDestroySFNARN, executionInput.UserId, executionInput)
}

func (s *sfnClientImpl) statExecuteError() {
	s.Statter.Inc(startExecutionErrorStatName, 1)
}

func (s *sfnClientImpl) execute(stateMachineARN string, executionName string, executionInput interface{}) error {
	startTime := time.Now()

	defer func() {
		s.Statter.TimingDuration(startExecutionLatencyStatName, time.Since(startTime))
	}()

	inputJSON, err := json.Marshal(executionInput)

	if err != nil {
		s.statExecuteError()
		return err
	}

	startExecInput := &sfn.StartExecutionInput{
		Input:           pointers.StringP(string(inputJSON)),
		Name:            pointers.StringP(executionName),
		StateMachineArn: pointers.StringP(stateMachineARN),
	}

	_, err = s.SFN.StartExecution(startExecInput)

	if err != nil {
		s.statExecuteError()
		return err
	}

	return nil
}
