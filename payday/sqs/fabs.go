package psqs

import (
	"context"
	"fmt"

	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/sqs/crypto"
	"github.com/aws/aws-sdk-go/service/sqs"
)

const (
	qifansError = "Suffered terrible terrible damage in a type assertion."
)

type IFABSMessageHandler interface {
	HandleGiveBitsToBroadcaster(ctx context.Context, msg *models.GiveBitsToBroadcasterMessage) error
	HandleAddBitsToCustomerAccount(ctx context.Context, msg *models.AddBitsToCustomerAccountMessage) error
	HandleRemoveBitsFromCustomerAccount(ctx context.Context, msg *models.RemoveBitsFromCustomerAccountMessage) error
	HandleUseBitsOnExtension(ctx context.Context, msg *models.UseBitsOnExtensionMessage) error
	HandleUseBitsOnPoll(ctx context.Context, msg *models.UseBitsOnPollMessage) error
	HandleCreateHold(ctx context.Context, msg *models.CreateHoldMessage) error
	HandleFinalizeHold(ctx context.Context, msg *models.FinalizeHoldMessage) error
	HandleReleaseHold(ctx context.Context, msg *models.ReleaseHoldMessage) error
}

type FABSMessageAdapter struct {
	Decrypter crypto.IDecrypter     `inject:""`
	Config    *config.Configuration `inject:""`
}

func NewFABSMessageAdapter() *FABSMessageAdapter {
	return &FABSMessageAdapter{}
}

func NewFABSMessageAdapterWithDecrypter(decrypter crypto.IDecrypter) *FABSMessageAdapter {
	return &FABSMessageAdapter{
		Decrypter: decrypter,
	}
}

func (a *FABSMessageAdapter) Adapt(handler IFABSMessageHandler) MessageHandler {
	return func(parent context.Context, message *sqs.Message) error {
		ctx := context.WithValue(parent, "message", message)
		bitsMessage, err := a.Decrypter.ProcessEncryptedBitsMessage(message)

		if err != nil {
			return err
		}

		// we should return success and not process the message if
		// we shouldn't process the message. Noop essentially.
		if !a.ShouldProcessMessage(bitsMessage) {
			return nil
		}

		return ProcessFABSMsg(ctx, bitsMessage, handler)
	}
}

func ProcessFABSMsg(ctx context.Context, msg models.SQSMessageJSON, fabsMsgHandler IFABSMessageHandler) error {
	var err error
	messageType := msg.SQSMessage.MessageType()

	switch messageType {
	case models.GiveBitsToBroadcaster, models.GiveBitsToBroadcasterChallenge:
		giveBitsMessage, ok := msg.SQSMessage.(*models.GiveBitsToBroadcasterMessage)
		if !ok {
			err = errors.Newf("%s Could not convert to GiveBitsToBroadcasterMessage", qifansError)
		} else {
			err = fabsMsgHandler.HandleGiveBitsToBroadcaster(ctx, giveBitsMessage)
		}
	case models.AddBitsToCustomerAccount:
		addBitsMessage, ok := msg.SQSMessage.(*models.AddBitsToCustomerAccountMessage)
		if !ok {
			err = errors.Newf("%s Could not convert to AddBitsToCustomerAccountMessage", qifansError)
		} else {
			err = fabsMsgHandler.HandleAddBitsToCustomerAccount(ctx, addBitsMessage)
		}
	case models.RemoveBitsFromCustomerAccount:
		removeBitsMessage, ok := msg.SQSMessage.(*models.RemoveBitsFromCustomerAccountMessage)
		if !ok {
			err = errors.Newf("%s Could not convert to RemoveBitsFromCustomerAccount", qifansError)
		} else {
			err = fabsMsgHandler.HandleRemoveBitsFromCustomerAccount(ctx, removeBitsMessage)
		}
	case models.UseBitsOnExtension, models.UseBitsOnExtensionChallenge:
		useBitsOnExtensionMessage, ok := msg.SQSMessage.(*models.UseBitsOnExtensionMessage)
		if !ok {
			err = errors.Newf("%s Could not convert to UseBitsOnExtensionMessage", qifansError)
		} else {
			err = fabsMsgHandler.HandleUseBitsOnExtension(ctx, useBitsOnExtensionMessage)
		}
	case models.UseBitsOnPoll:
		useBitsOnPollMessage, ok := msg.SQSMessage.(*models.UseBitsOnPollMessage)
		if !ok {
			err = errors.Newf("%s Could not convert to UseBitsOnPollMessage", qifansError)
		} else {
			err = fabsMsgHandler.HandleUseBitsOnPoll(ctx, useBitsOnPollMessage)
		}
	case models.CreateHold:
		createHoldMessage, ok := msg.SQSMessage.(*models.CreateHoldMessage)
		if !ok {
			err = errors.Newf("%s Could not convert to CreateHoldMessage", qifansError)
		} else {
			err = fabsMsgHandler.HandleCreateHold(ctx, createHoldMessage)
		}
	case models.FinalizeHold:
		finalizeHoldMessage, ok := msg.SQSMessage.(*models.FinalizeHoldMessage)
		if !ok {
			err = errors.Newf("%s Could not convert to FinalizeHoldMessage", qifansError)
		} else {
			err = fabsMsgHandler.HandleFinalizeHold(ctx, finalizeHoldMessage)
		}
	case models.ReleaseHold:
		releaseHoldMessage, ok := msg.SQSMessage.(*models.ReleaseHoldMessage)
		if !ok {
			err = errors.Newf("%s Could not convert to ReleaseHoldMessage", qifansError)
		} else {
			err = fabsMsgHandler.HandleReleaseHold(ctx, releaseHoldMessage)
		}
	default:
		err = fmt.Errorf("no message handler available for SQS messageType: %v", messageType.Name())
	}

	if err != nil {
		return fmt.Errorf("failed to handle SQS message of messageType %v: %v", messageType.Name(), err)
	}
	return nil
}

func (a *FABSMessageAdapter) ShouldProcessMessage(msg models.SQSMessageJSON) bool {
	// we should process the message if we want only andes messages and it is
	// marked as a pachinko message

	if msg.SQSMessage.IsPachinkoMessage() {
		return true
	} else {
		return false
	}
}
