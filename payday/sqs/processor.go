package psqs

import (
	"context"
	"sync"
	"time"

	g "code.justin.tv/chat/golibs/graceful"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/utils/pointers"
	syncutils "code.justin.tv/commerce/payday/utils/sync"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/request"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/aws/aws-sdk-go/service/sqs/sqsiface"
)

const (
	DefaultVisibilityTimeoutSeconds = 40
	WaitTimeSeconds                 = 20
)

type MessageHandler func(ctx context.Context, message *sqs.Message) error

type IMessageHandler interface {
	HandleMessage(ctx context.Context, message *sqs.Message) error
}

type SQSPoller struct {
	SQSClient                sqsiface.SQSAPI
	QueueURL                 string
	MessageHandler           MessageHandler
	VisibilityTimeoutSeconds int64
}

type SQSPollerParams struct {
	SQSRegion                string
	VisibilityTimeoutSeconds int64
}

func NewProcessor(params SQSPollerParams, queueUrl string, messageHandler MessageHandler) *SQSPoller {
	var visibilityTimeout int64
	if params.VisibilityTimeoutSeconds <= int64(0) {
		visibilityTimeout = DefaultVisibilityTimeoutSeconds
	} else {
		visibilityTimeout = params.VisibilityTimeoutSeconds
	}

	sess, _ := session.NewSession()
	return &SQSPoller{
		SQSClient:                sqs.New(sess, &aws.Config{Region: aws.String(params.SQSRegion)}),
		QueueURL:                 queueUrl,
		VisibilityTimeoutSeconds: visibilityTimeout,
		MessageHandler:           messageHandler,
	}
}

func (p *SQSPoller) deleteSQSMessage(m *sqs.Message) (err error) {
	params := &sqs.DeleteMessageInput{
		QueueUrl:      aws.String(p.QueueURL),
		ReceiptHandle: aws.String(*m.ReceiptHandle),
	}

	_, err = p.SQSClient.DeleteMessage(params)
	return err
}

func (p *SQSPoller) HandleMessage(ctx context.Context, msg *sqs.Message) {
	err := p.MessageHandler(ctx, msg)

	if err != nil {
		if msg.MessageId != nil {
			approxReceiveCount := pointers.StringOrDefault(msg.Attributes[sqs.MessageSystemAttributeNameApproximateReceiveCount], "")
			log.WithError(err).WithFields(
				log.Fields{
					"messageId":          *msg.MessageId,
					"approxReceiveCount": approxReceiveCount,
				},
			).Warn("Error while handling message")
		} else {
			log.WithError(err).Error("Error while handling message with nil id")
		}
		// do NOT delete message that fail to parse or process; they get dumped to the DLQ automagically
	} else {
		deleteErr := p.deleteSQSMessage(msg)
		if deleteErr != nil {
			log.WithError(deleteErr).Error("Failed to delete SQS message after reading")
		}
	}
}

// simple parallelism implementation using unlimited goroutines
// TODO: consider replacing with a worker-dispatch model to prevent resource starvation
func (p *SQSPoller) HandleMessages(visibilityTimeout time.Duration, m []*sqs.Message) {
	//sqs visibility is 30s so set timeout to same
	ctx, cancel := context.WithTimeout(context.Background(), visibilityTimeout)
	defer cancel()
	wg := new(sync.WaitGroup)

	for _, message := range m {
		wg.Add(1)
		go func(messageClosure *sqs.Message) {
			defer wg.Done()

			p.HandleMessage(ctx, messageClosure)
		}(message)
	}

	c := syncutils.GetChannel(wg)
	select {
	case <-ctx.Done():
		log.Error("Timed out waiting on sqs messages to process")
	case <-c:
		break
	}

}

// reads from the SQS queue, deleting messages that have been successfully retrieved
func (p *SQSPoller) PollSQS() {
	params := &sqs.ReceiveMessageInput{
		QueueUrl: aws.String(p.QueueURL),
		AttributeNames: []*string{
			aws.String("All"),
		},
		MaxNumberOfMessages: aws.Int64(1),
		MessageAttributeNames: []*string{
			aws.String("All"),
		},
		VisibilityTimeout: aws.Int64(p.VisibilityTimeoutSeconds),
		WaitTimeSeconds:   aws.Int64(WaitTimeSeconds),
	}
	for {
		select {
		case <-g.ShuttingDown():
			return
		default:
			resp, err := p.receiveSQSMessages(params)
			if err != nil {
				log.WithError(err).Error("Unable to receive message from SQS queue")
				time.Sleep(time.Second)
				break
			}

			if resp != nil {
				p.HandleMessages(time.Duration(p.VisibilityTimeoutSeconds)*time.Second, resp.Messages)
			}
		}
	}
}

func (p *SQSPoller) receiveSQSMessages(rmi *sqs.ReceiveMessageInput) (*sqs.ReceiveMessageOutput, error) {
	// cancels the SQS long poll when shutting down
	sqsReceiveCtx, cancel := context.WithCancel(context.Background())
	defer cancel()
	go func() {
		select {
		case <-sqsReceiveCtx.Done():
			return
		case <-g.ShuttingDown():
			cancel()
			return
		}
	}()

	resp, err := p.SQSClient.ReceiveMessageWithContext(sqsReceiveCtx, rmi)
	if err != nil {
		if awsErr, ok := err.(awserr.Error); ok && awsErr.Code() == request.CanceledErrorCode {
			log.WithField("queue_url", p.QueueURL).WithError(err).Info("Request cancelled for polling SQS queue")
			return nil, nil
		}
		return nil, err
	}
	return resp, err
}
