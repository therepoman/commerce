package crypto

import (
	"crypto/aes"
	"crypto/cipher"
	"encoding/base64"
	"encoding/json"
	"fmt"

	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/revenue/go-libs/encryption"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/kms"
	"github.com/aws/aws-sdk-go/service/kms/kmsiface"
	"github.com/aws/aws-sdk-go/service/sqs"
	lru "github.com/hashicorp/golang-lru"
	"github.com/pkg/errors"
)

const blockSizeBytes = 32 // (32 bytes * 8 = 256 bits) we are using AES-256
const cacheSize = 16

type Encrypter interface {
	EncryptMessage(input []byte) (*models.EncryptedNotification, error)
}

type IDecrypter interface {
	DecryptMessage(m models.EncryptedNotification) ([]byte, error)
	ProcessEncryptedBitsMessage(message *sqs.Message) (models.SQSMessageJSON, error)
}

type KMSCipher struct {
	kmsClient   kmsiface.KMSAPI
	masterKeyID string
	cache       *lru.Cache
}

func NewKMSCipher(cfg config.Configuration) *KMSCipher {
	var d KMSCipher

	sess, _ := session.NewSession(&aws.Config{Region: aws.String(cfg.KMSRegion)})
	d.kmsClient = kms.New(sess)
	d.masterKeyID = cfg.KMSMasterKeyARN
	d.cache, _ = lru.New(cacheSize)

	return &d
}

func (d *KMSCipher) EncryptMessage(input []byte) (*models.EncryptedNotification, error) {
	dataKeyOutput, err := d.kmsClient.GenerateDataKey(&kms.GenerateDataKeyInput{
		KeySpec: aws.String(kms.DataKeySpecAes256),
		KeyId:   aws.String(d.masterKeyID),
	})
	if err != nil {
		return nil, errors.Wrap(err, "could not get new AES data key")
	}

	encryptedPayload, nonce, err := encryption.EncryptBytes(dataKeyOutput.Plaintext, input, nil, false)
	if err != nil {
		return nil, errors.Wrap(err, "failed to encrypt payload")
	}

	message := &models.EncryptedNotification{
		InitializationVector: base64.StdEncoding.EncodeToString(nonce),
		Payload:              base64.StdEncoding.EncodeToString(encryptedPayload),
		Key:                  base64.StdEncoding.EncodeToString(dataKeyOutput.CiphertextBlob),
	}

	return message, nil
}

func (d *KMSCipher) DecryptMessage(m models.EncryptedNotification) ([]byte, error) {
	var sessionKey []byte

	val, hit := d.cache.Get(m.Key)

	if hit {
		var ok bool

		sessionKey, ok = val.([]byte)
		if !ok {
			// evict the problematic value from the cache and return an error
			d.cache.Remove(m.Key)

			return nil, fmt.Errorf("DecryptMessage: type assertion failure in cache value: expected []byte and got %T", sessionKey)
		}
	} else {
		// decrypt the session key using KMS and store it in the cache
		decodedKey, err := base64.StdEncoding.DecodeString(m.Key)
		if err != nil {
			return nil, fmt.Errorf("DecryptMessage: failed to base64-decode the session key: %v", err)
		}

		params := &kms.DecryptInput{
			CiphertextBlob: decodedKey,
		}

		resp, err := d.kmsClient.Decrypt(params)
		if err != nil {
			return nil, fmt.Errorf("DecryptMessage: failed to use KMS to decrypt the session key with the master key: %v", err)
		}
		sessionKey = resp.Plaintext
		d.cache.Add(m.Key, sessionKey)
	}

	if len(sessionKey) != blockSizeBytes {
		return nil, fmt.Errorf("DecryptMessage: incorrect session key size. Expected %v and instead got %v", blockSizeBytes, len(sessionKey))
	}

	// decrypt the ciphertext using the session key
	aesCipher, err := aes.NewCipher(sessionKey)
	if err != nil {
		return nil, fmt.Errorf("DecryptMessage: error while createing an AES cipher with the session key: %v", err)
	}

	decodedIV, err := base64.StdEncoding.DecodeString(m.InitializationVector)
	if err != nil {
		return nil, fmt.Errorf("DecryptMessage: failed to base64-decode the IV/nonce: %v", err)
	}
	gcmDecrypter, err := cipher.NewGCM(aesCipher)
	if err != nil {
		return nil, fmt.Errorf("DecryptMessage: failed to create a AES-Galois/Counter Mode decrypter: %v", err)
	}

	decodedPayload, err := base64.StdEncoding.DecodeString(m.Payload)
	if err != nil {
		return nil, fmt.Errorf("DecryptMessage: failed to base-64 decode the payload: %v", err) // base64 of payload ruined
	}

	plaintext, err := gcmDecrypter.Open(nil, decodedIV, decodedPayload, nil)
	if err != nil {
		return nil, fmt.Errorf("DecryptMessage: failed to decrypt the payload with the session key: %v", err) // decryption failed
	}

	return plaintext, nil
}

func (d *KMSCipher) ProcessEncryptedBitsMessage(message *sqs.Message) (models.SQSMessageJSON, error) {
	var fabsMessage models.SQSMessageJSON

	var sns models.SNSMessage
	err := json.Unmarshal([]byte(*message.Body), &sns)
	if err != nil {
		return fabsMessage, fmt.Errorf("Unable to unmarshal SNS response from queue: %s", err)
	}

	var ciphertextNotification models.EncryptedNotification
	err = json.Unmarshal([]byte(sns.Message), &ciphertextNotification)
	if err != nil {
		return fabsMessage, fmt.Errorf("Unable to unmarshal SNS message into encrypted Pachinko notification from queue: %s", err)
	}

	plaintextNotificationJSON, err := d.DecryptMessage(ciphertextNotification)
	if err != nil {
		return fabsMessage, fmt.Errorf("Failed to decrypt a notification from Pachinko: %s", err)
	}

	err = fabsMessage.UnmarshalJSON([]byte(plaintextNotificationJSON))
	if err != nil {
		return fabsMessage, fmt.Errorf("Failed to unmarshal SQS message from Pachinko: %s", err)
	}

	return fabsMessage, nil
}
