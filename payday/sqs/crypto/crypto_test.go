package crypto

import (
	"encoding/base64"
	"testing"

	"code.justin.tv/commerce/payday/config"
	kmsiface_mock "code.justin.tv/commerce/payday/mocks/github.com/aws/aws-sdk-go/service/kms/kmsiface"
	"code.justin.tv/commerce/payday/models"
	"github.com/aws/aws-sdk-go/service/kms"
	"github.com/aws/aws-sdk-go/service/kms/kmsiface"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

var sampleNotification = models.EncryptedNotification{
	InitializationVector: "O3laWi4JLPIDdK+t",
	Payload:              "l6oK+dpGAxC2PUP5OLPkmDYsA2+y3g4gnKMml8ZQZoepuJNM/GI2ahV1iUsqaTyEznkH3Qye+KxLkEzALKaV2tAuBmTIx0XsDSkTqfoA2p2MwxeTPaPkQNHkUaF/dSw3BgUijSo1DX+6MUTF5b5jqyenHbENSBIYAnQGB2FkWYrmtwe25fNyeooWwUsAEppYixvyl0nJF7/dqPH/amU/TiB6mUfUhxTwuCliMDKng9hvCewM6po/po8aIkUiXFUdosS7hFEJ674ytjb9RYGDedd2ODcj4twq2PyjNXOWhnwlyZx5tl/gREOkLBLXToWBPMqdQvWndo8zI/0lXXR0FgzFzm/DCbajfTBT4MB61Ye0LjJCWOu1rdQsglF3dcgEM1BGM26l4y5il41/QBwgVFeaAcjN/hFkWdmAgkF7VTEehBc=",
	Key:                  "CiBQsXefba8qa37lpYP4zrnNbgZGm27jPkWMMmFimn5kJRKnAQEBAgB4ULF3n22vKmt+5aWD+M65zW4GRptu4z5FjDJhYpp+ZCUAAAB+MHwGCSqGSIb3DQEHBqBvMG0CAQAwaAYJKoZIhvcNAQcBMB4GCWCGSAFlAwQBLjARBAwvHsPV78Fco2H7riMCARCAO5cYV3jx6tJejS30Tfnv6drdOHj77k8ejHrsLGubfK1aGNfNTgDl0KhWsBSuJjjJJGeY39tuc2vQpIae",
}

var uncachedNotification = models.EncryptedNotification{
	InitializationVector: "O3laWi4JLPIDdK+t",
	Payload:              "l6oK+dpGAxC2PUP5OLPkmDYsA2+y3g4gnKMml8ZQZoepuJNM/GI2ahV1iUsqaTyEznkH3Qye+KxLkEzALKaV2tAuBmTIx0XsDSkTqfoA2p2MwxeTPaPkQNHkUaF/dSw3BgUijSo1DX+6MUTF5b5jqyenHbENSBIYAnQGB2FkWYrmtwe25fNyeooWwUsAEppYixvyl0nJF7/dqPH/amU/TiB6mUfUhxTwuCliMDKng9hvCewM6po/po8aIkUiXFUdosS7hFEJ674ytjb9RYGDedd2ODcj4twq2PyjNXOWhnwlyZx5tl/gREOkLBLXToWBPMqdQvWndo8zI/0lXXR0FgzFzm/DCbajfTBT4MB61Ye0LjJCWOu1rdQsglF3dcgEM1BGM26l4y5il41/QBwgVFeaAcjN/hFkWdmAgkF7VTEehBc=",
	Key:                  "uncached",
}

var sampleDecryptedKey = "GtlbkJng1KKpIpEUN0CQmfX8hRKTvd8ONzSB+eXf+sg=" // base64-encoded session key decrypted from sampleNotification

var samplePlaintext = "{\"transactionId\":\"5b1cd03b-0e36-4915-b00f-482df7612306\",\"totalBits\":1,\"giveBitsPublicMessage\":\"Whats love got to do with it\",\"targetTwitchUserId\":\"39141793\",\"timeOfEvent\":\"1450233031389\",\"requestingTwitchUserId\":\"65380937\",\"transactionType\":\"GiveBitsToBroadcaster\",\"accountTypeToBitAmount\":{\"PURCHASED\":-1}}"

func initDecrypter(mockedKMS kmsiface.KMSAPI) *KMSCipher {
	kmsMockedDecrypter := NewKMSCipher(config.Configuration{
		KMSRegion:       "unit-test-aws-region",
		KMSMasterKeyARN: "alias/mock-master-key",
	})
	kmsMockedDecrypter.kmsClient = mockedKMS
	return kmsMockedDecrypter
}

// test successful AES decryption with sample message
func TestDecryptMessage(t *testing.T) {
	mockedKMSAPI := new(kmsiface_mock.KMSAPI)

	classUnderTest := initDecrypter(mockedKMSAPI)

	sessionKey, err := base64.StdEncoding.DecodeString(sampleDecryptedKey)
	assert.NoError(t, err)

	mockedKMSAPI.On("Decrypt", mock.Anything).Return(&kms.DecryptOutput{
		Plaintext: sessionKey,
	}, nil)

	plaintext, err := classUnderTest.DecryptMessage(sampleNotification)
	assert.NoError(t, err)

	assert.Equal(t, string(plaintext), samplePlaintext)
}

// test KMS is being called on cache miss
func TestCacheMiss(t *testing.T) {
	mockedKMSAPI := new(kmsiface_mock.KMSAPI)
	classUnderTest := initDecrypter(mockedKMSAPI)
	sessionKey, err := base64.StdEncoding.DecodeString(sampleDecryptedKey)
	assert.NoError(t, err)

	mockedKMSAPI.On("Decrypt", mock.Anything).Return(&kms.DecryptOutput{
		Plaintext: sessionKey,
	}, nil)

	_, err = classUnderTest.DecryptMessage(sampleNotification)
	assert.NoError(t, err)

	_, err = classUnderTest.DecryptMessage(uncachedNotification)
	assert.NoError(t, err)

	mockedKMSAPI.AssertNumberOfCalls(t, "Decrypt", 2)
}

// test KMS is not called on cache hit
func TestCacheHit(t *testing.T) {
	mockedKMSAPI := new(kmsiface_mock.KMSAPI)
	classUnderTest := initDecrypter(mockedKMSAPI)
	sessionKey, err := base64.StdEncoding.DecodeString(sampleDecryptedKey)
	assert.NoError(t, err)

	mockedKMSAPI.On("Decrypt", mock.Anything).Return(&kms.DecryptOutput{
		Plaintext: sessionKey,
	}, nil)

	_, err = classUnderTest.DecryptMessage(sampleNotification)
	assert.NoError(t, err)

	_, err = classUnderTest.DecryptMessage(sampleNotification)
	assert.NoError(t, err)

	mockedKMSAPI.AssertNumberOfCalls(t, "Decrypt", 1)
}
