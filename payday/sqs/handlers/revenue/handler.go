package revenue

import (
	"context"
	"encoding/json"
	"fmt"
	"strconv"

	gringotts "code.justin.tv/commerce/gringotts/sns"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/clients/owl"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/partner"
	"code.justin.tv/commerce/payday/sns"
	"code.justin.tv/commerce/payday/utils/math"
)

const (
	partnerParticipantRole              = "partner"
	affiliateParticipantRole            = "affiliate"
	extensionDeveloperParticipantRole   = "extension-dev"
	bitsOnExtensionsRevenueSourcePrefix = "bits-on-extension"
	bitsOnPollsRevenueSourcePrefix      = "bits-on-poll"
	bitsUsedTypeOfTransaction           = "bits-used"
	bitsOnPollSKU                       = "bits-on-poll"
)

type Handler struct {
	Config         *config.Configuration   `inject:""`
	SNSClient      sns.ISNSClient          `inject:"usWestSNSClient"`
	OWLClient      owl.ClientWrapper       `inject:""`
	PartnerManager partner.IPartnerManager `inject:""`
}

func (h *Handler) HandleGiveBitsToBroadcaster(ctx context.Context, msg *models.GiveBitsToBroadcasterMessage) error {
	return nil
}

func (h *Handler) HandleAddBitsToCustomerAccount(ctx context.Context, msg *models.AddBitsToCustomerAccountMessage) error {
	return nil
}

func (h *Handler) HandleRemoveBitsFromCustomerAccount(ctx context.Context, msg *models.RemoveBitsFromCustomerAccountMessage) error {
	return nil
}

func (h *Handler) HandleUseBitsOnExtension(ctx context.Context, msg *models.UseBitsOnExtensionMessage) error {
	if msg == nil {
		return nil
	}

	if msg.IgnorePayout {
		log.Infof("Skipping sending message for txID: %s as IgnorePayout == true", msg.TransactionId)
		return nil
	}

	fields := log.Fields{
		"userID":      msg.RequestingTwitchUserId,
		"channelID":   msg.TargetTwitchUserId,
		"extensionID": msg.ExtensionID,
		"bitsAmount":  msg.BitsProccessed,
	}

	client, err := h.OWLClient.GetClient(ctx, msg.ExtensionID)
	if err != nil {
		msg := "Error getting client from extensionID"
		log.WithFields(fields).WithError(err).Error(msg)
		return errors.Note(err, msg)
	}

	if client == nil || client.OwnerID == nil {
		msg := "No owner found for extensionID"
		log.WithFields(fields).WithError(err).Error(msg)
		return errors.Note(err, msg)
	}

	extensionCreatorID := strconv.Itoa(*client.OwnerID)

	extensionBroadcasterShare := h.Config.ExtensionBroadcasterShare
	extensionDeveloperShare := h.Config.ExtensionDeveloperShare

	if h.Config.ExtensionRevshareWhitelist[msg.ExtensionID] {
		extensionBroadcasterShare = 1
		extensionDeveloperShare = 0
	}

	return msg.HandleBitsRevenueAttributions(func(broadcasterId string, numBits int) error {
		bitsProcessed := math.IntAbs(numBits)
		revenueCentsForBroadcaster := extensionBroadcasterShare * float64(bitsProcessed)
		revenueCentsForDeveloper := extensionDeveloperShare * float64(bitsProcessed)

		broadcasterParticipantRole, err := h.getBroadcasterParticipantRole(ctx, broadcasterId)
		if err != nil {
			log.WithError(err).WithFields(fields).Error("error getting broadcaster participant role for use bits on extension")
			return err
		}

		err = h.submitExtensionRevenueTransaction(msg, revenueCentsForBroadcaster, broadcasterId, getExtensionRevenueSource(msg.ExtensionID), broadcasterParticipantRole)
		if err != nil {
			msg := "Error submitting revenue transaction for channel"
			log.WithFields(fields).WithError(err).Error(msg)
			return errors.Note(err, msg)
		}

		err = h.submitExtensionRevenueTransaction(msg, revenueCentsForDeveloper, extensionCreatorID, getExtensionRevenueSource(broadcasterId), extensionDeveloperParticipantRole)
		if err != nil {
			msg := "Error submitting revenue transaction for extension creator"
			log.WithFields(fields).WithError(err).Error(msg)
			return errors.Note(err, msg)
		}

		return nil
	})
}

func getExtensionRevenueSource(revenueSourceID string) string {
	return fmt.Sprintf("%s:%s", bitsOnExtensionsRevenueSourcePrefix, revenueSourceID)
}

func (h *Handler) submitExtensionRevenueTransaction(msg *models.UseBitsOnExtensionMessage, revenueCents float64, revenueRecipient string, revenueSource string, participantRole string) error {
	var extraContext models.BitsExtensionsExtraContext
	err := json.Unmarshal([]byte(msg.ExtraContext), &extraContext)

	var zipCode string
	if err != nil {
		log.WithError(err).Warn("Failed to unmarshal ipAddress from Extensions Bit use extra context.")
	} else if extraContext.CountryCode == "US" {
		// Revenue reporting only supports postal codes in the US
		zipCode = extraContext.ZipCode
	}

	tx := &gringotts.RevenueEventMessage{
		ChannelId:       revenueRecipient,
		TimeOfEvent:     msg.TimeOfEvent,
		TransactionId:   msg.TransactionId,
		TransactionType: bitsUsedTypeOfTransaction,
		RevenueSource:   revenueSource,
		Revenue:         revenueCents,
		ParticipantRole: participantRole,
		ZipCode:         zipCode,
		SKU:             msg.SKU,
	}

	return h.submitRevenueTransaction(tx)

}

func (h *Handler) HandleUseBitsOnPoll(ctx context.Context, msg *models.UseBitsOnPollMessage) error {
	if msg == nil {
		return nil
	}

	fields := log.Fields{
		"userID":     msg.RequestingTwitchUserId,
		"channelID":  msg.TargetTwitchUserId,
		"bitsAmount": msg.BitsProccessed,
		"pollID":     msg.PollId,
	}

	broadcasterParticipantRole, err := h.getBroadcasterParticipantRole(ctx, msg.TargetTwitchUserId)
	if err != nil {
		log.WithError(err).WithFields(fields).Error("error getting broadcaster participant role for use bits on poll")
		return err
	}

	tx := &gringotts.RevenueEventMessage{
		ChannelId:       msg.TargetTwitchUserId,
		TimeOfEvent:     msg.TimeOfEvent,
		TransactionId:   msg.TransactionId,
		TransactionType: bitsUsedTypeOfTransaction,
		RevenueSource:   getPollRevenueSource(msg.PollId),
		Revenue:         float64(math.IntAbs(msg.BitsProccessed)),
		ParticipantRole: broadcasterParticipantRole,
		SKU:             bitsOnPollSKU,
	}

	err = h.submitRevenueTransaction(tx)
	if err != nil {
		log.WithFields(fields).WithError(err).Error("error submitting polls revenue transaction")
		return err
	}

	return nil
}

func (h *Handler) HandleCreateHold(ctx context.Context, msg *models.CreateHoldMessage) error {
	// we don't care about non-finalized holds in gringotts
	return nil
}

func (h *Handler) HandleFinalizeHold(ctx context.Context, msg *models.FinalizeHoldMessage) error {
	//TODO: THIS PROBABLY NEEDS TO BE HANDLED BEFORE WE SHIP HOLD/CREATE
	return nil
}

func (h *Handler) HandleReleaseHold(ctx context.Context, msg *models.ReleaseHoldMessage) error {
	// we don't care about non-finalized holds in gringotts
	return nil
}

func getPollRevenueSource(revenueSourceID string) string {
	return fmt.Sprintf("%s:%s", bitsOnPollsRevenueSourcePrefix, revenueSourceID)
}

func (h *Handler) submitRevenueTransaction(tx *gringotts.RevenueEventMessage) error {
	transactionJSON, err := json.Marshal(tx)
	if err != nil {
		return errors.Note(err, "Error marshalling revenue transaction into json")
	}

	err = h.SNSClient.PostToTopic(h.Config.RevenueServiceSNSTopic, string(transactionJSON))
	if err != nil {
		return errors.Note(err, "Error posting revenue transaction to revenue SNS topic")
	}

	return nil

}

func (h *Handler) getBroadcasterParticipantRole(ctx context.Context, channelID string) (string, error) {
	partnerType, err := h.PartnerManager.GetPartnerType(ctx, channelID)
	if err != nil {
		return "", errors.Note(err, "Error getting partner type for channel")
	}

	var broadcasterParticipantRole string
	if partnerType == partner.TraditionalPartnerType {
		broadcasterParticipantRole = partnerParticipantRole
	} else if partnerType == partner.AffiliatePartnerType {
		broadcasterParticipantRole = affiliateParticipantRole
	} else {
		return "", errors.Note(err, "Received revenue message for non-partner")
	}

	return broadcasterParticipantRole, nil
}
