package revenue

import (
	"context"
	"encoding/json"
	"testing"

	"code.justin.tv/commerce/payday/dynamo/holds"

	gringotts "code.justin.tv/commerce/gringotts/sns"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/errors"
	partner_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/partner"
	sns_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sns"
	owl_mocks "code.justin.tv/commerce/payday/mocks/owl"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/partner"
	"code.justin.tv/commerce/payday/utils/pointers"
	"code.justin.tv/web/owl/oauth2"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func jsonToRevenueMessage(jsonMsg string) *gringotts.RevenueEventMessage {
	var msg gringotts.RevenueEventMessage
	err := json.Unmarshal([]byte(jsonMsg), &msg)
	So(err, ShouldBeNil)
	return &msg
}

func testUseBitsOnExtensionMessage() *models.UseBitsOnExtensionMessage {
	return &models.UseBitsOnExtensionMessage{
		TransactionId:          "testTransactionID",
		RequestingTwitchUserId: "userID",
		TargetTwitchUserId:     "testChannelID",
		ExtensionID:            "testExtensionID",
		BitsProccessed:         1,
	}
}

func testUseBitsOnExtensionMessageWhitelisted() *models.UseBitsOnExtensionMessage {
	return &models.UseBitsOnExtensionMessage{
		TransactionId:          "testTransactionID",
		RequestingTwitchUserId: "userID",
		TargetTwitchUserId:     "testChannelID",
		ExtensionID:            "ut0qm2nbknuy64d7t509w616a8d85l",
		BitsProccessed:         1,
	}
}

func testUseBitsOnExtensionBitsAttributionMessage() *models.UseBitsOnExtensionMessage {
	return &models.UseBitsOnExtensionMessage{
		TransactionId:          "testTransactionID",
		RequestingTwitchUserId: "userID",
		TargetTwitchUserId:     "testChannelID",
		ExtensionID:            "testExtensionID",
		BitsProccessed:         2,
		UsageOverrideList: map[string]holds.BitsUsageAttribution{
			"testChannelID": {
				RewardAttribution:                   1,
				BitsRevenueAttributionToBroadcaster: 1,
				TotalWithBroadcasterPrior:           0,
				ID:                                  "testChannelID",
			},
			"otherTestChannelID": {
				RewardAttribution:                   1,
				BitsRevenueAttributionToBroadcaster: 1,
				TotalWithBroadcasterPrior:           0,
				ID:                                  "otherTestChannelID",
			},
		},
	}
}

func TestHandler_HandleUseBitsOnExtension(t *testing.T) {
	Convey("Given a revenue handler", t, func() {
		snsClient := new(sns_mock.ISNSClient)
		owlClient := new(owl_mocks.ClientWrapper)
		partnerManager := new(partner_mock.IPartnerManager)
		handler := &Handler{
			Config: &config.Configuration{
				ExtensionBroadcasterShare: 0.8,
				ExtensionDeveloperShare:   0.2,
				ExtensionRevshareWhitelist: map[string]bool{
					"ut0qm2nbknuy64d7t509w616a8d85l": true,
				},
			},
			SNSClient:      snsClient,
			OWLClient:      owlClient,
			PartnerManager: partnerManager,
		}

		Convey("Returns early when it receives a nil message", func() {
			err := handler.HandleUseBitsOnExtension(context.Background(), nil)
			So(err, ShouldBeNil)
			snsClient.AssertNotCalled(t, "PostToTopic", mock.Anything, mock.Anything)
		})

		Convey("Returns early when it receives a IgnorePayout == true message", func() {
			msg := testUseBitsOnExtensionMessage()
			msg.IgnorePayout = true
			err := handler.HandleUseBitsOnExtension(context.Background(), msg)
			So(err, ShouldBeNil)
			owlClient.AssertNotCalled(t, "GetClient", mock.Anything, mock.Anything)
			snsClient.AssertNotCalled(t, "PostToTopic", mock.Anything, mock.Anything)
		})

		Convey("Errors when OWL call fails", func() {
			owlClient.On("GetClient", mock.Anything, mock.Anything).Return(nil, errors.New("test error"))
			err := handler.HandleUseBitsOnExtension(context.Background(), testUseBitsOnExtensionMessage())
			So(err, ShouldNotBeNil)
		})

		Convey("Errors when OWL call returns nil client", func() {
			owlClient.On("GetClient", mock.Anything, mock.Anything).Return(nil, nil)
			err := handler.HandleUseBitsOnExtension(context.Background(), testUseBitsOnExtensionMessage())
			So(err, ShouldNotBeNil)
		})

		Convey("Errors when OWL call returns nil owner id", func() {
			owlClient.On("GetClient", mock.Anything, mock.Anything).Return(&oauth2.Client{
				OwnerID: nil,
			}, nil)
			err := handler.HandleUseBitsOnExtension(context.Background(), testUseBitsOnExtensionMessage())
			So(err, ShouldNotBeNil)
		})

		Convey("When OWL call returns a valid client", func() {
			owlClient.On("GetClient", mock.Anything, "testExtensionID").Return(&oauth2.Client{
				OwnerID: pointers.IntP(123456),
			}, nil)

			owlClient.On("GetClient", mock.Anything, "ut0qm2nbknuy64d7t509w616a8d85l").Return(&oauth2.Client{
				OwnerID: pointers.IntP(1234567),
			}, nil)

			Convey("Errors when PartnerManager errors", func() {
				partnerManager.On("GetPartnerType", mock.Anything, "testChannelID").Return(partner.TraditionalPartnerType, errors.New("test error"))
				err := handler.HandleUseBitsOnExtension(context.Background(), testUseBitsOnExtensionMessage())
				So(err, ShouldNotBeNil)
			})

			Convey("Errors when PartnerManager returns non-partner", func() {
				partnerManager.On("GetPartnerType", mock.Anything, "testChannelID").Return(partner.NonPartnerPartnerType, nil)
				err := handler.HandleUseBitsOnExtension(context.Background(), testUseBitsOnExtensionMessage())
				So(err, ShouldNotBeNil)
			})

			Convey("When PartnerManager returns partner", func() {
				partnerManager.On("GetPartnerType", mock.Anything, "testChannelID").Return(partner.TraditionalPartnerType, nil)

				Convey("Errors when SNS client errors on first call", func() {
					snsClient.On("PostToTopic", mock.Anything, mock.Anything).Return(errors.New("test error"))
					err := handler.HandleUseBitsOnExtension(context.Background(), testUseBitsOnExtensionMessage())
					So(err, ShouldNotBeNil)
				})

				Convey("When SNS client succeeds on first call", func() {
					snsClient.On("PostToTopic", mock.Anything, mock.Anything).Return(nil).Once()

					Convey("Errors when SNS client errors on second call", func() {
						snsClient.On("PostToTopic", mock.Anything, mock.Anything).Return(errors.New("test error")).Once()
						err := handler.HandleUseBitsOnExtension(context.Background(), testUseBitsOnExtensionMessage())
						So(err, ShouldNotBeNil)
					})

					Convey("Succeeds when SNS client succeeds on second call with regular extension", func() {
						snsClient.On("PostToTopic", mock.Anything, mock.Anything).Return(nil).Once()
						err := handler.HandleUseBitsOnExtension(context.Background(), testUseBitsOnExtensionMessage())
						So(err, ShouldBeNil)

						channelJSON := snsClient.Calls[0].Arguments.Get(1).(string)
						extensionCreatorJSON := snsClient.Calls[1].Arguments.Get(1).(string)

						channelTx := jsonToRevenueMessage(channelJSON)
						extensionCreator := jsonToRevenueMessage(extensionCreatorJSON)

						So(channelTx.TransactionId, ShouldEqual, "testTransactionID")
						So(channelTx.ChannelId, ShouldEqual, "testChannelID")
						So(channelTx.Revenue, ShouldEqual, .8)

						So(extensionCreator.TransactionId, ShouldEqual, "testTransactionID")
						So(extensionCreator.ChannelId, ShouldEqual, "123456")
						So(extensionCreator.Revenue, ShouldEqual, .2)
					})

					Convey("Succeeds with Bits Usage Override set", func() {
						snsClient.On("PostToTopic", mock.Anything, mock.Anything).Return(nil).Times(4)
						partnerManager.On("GetPartnerType", mock.Anything, "otherTestChannelID").Return(partner.TraditionalPartnerType, nil)
						err := handler.HandleUseBitsOnExtension(context.Background(), testUseBitsOnExtensionBitsAttributionMessage())
						So(err, ShouldBeNil)

						channelJSON := snsClient.Calls[0].Arguments.Get(1).(string)
						extensionCreatorJSON := snsClient.Calls[1].Arguments.Get(1).(string)

						channelTx := jsonToRevenueMessage(channelJSON)
						extensionCreator := jsonToRevenueMessage(extensionCreatorJSON)

						So(channelTx.TransactionId, ShouldEqual, "testTransactionID")
						So(channelTx.ChannelId, ShouldEqual, "otherTestChannelID")
						So(channelTx.Revenue, ShouldEqual, .8)

						So(extensionCreator.TransactionId, ShouldEqual, "testTransactionID")
						So(extensionCreator.ChannelId, ShouldEqual, "123456")
						So(extensionCreator.Revenue, ShouldEqual, .2)

						channelJSONPart2 := snsClient.Calls[2].Arguments.Get(1).(string)
						extensionCreatorJSONPart2 := snsClient.Calls[3].Arguments.Get(1).(string)

						channelTxUser2 := jsonToRevenueMessage(channelJSONPart2)
						extensionCreatorUser2 := jsonToRevenueMessage(extensionCreatorJSONPart2)

						So(channelTxUser2.TransactionId, ShouldEqual, "testTransactionID")
						So(channelTxUser2.ChannelId, ShouldEqual, "testChannelID")
						So(channelTxUser2.Revenue, ShouldEqual, .8)

						So(extensionCreatorUser2.TransactionId, ShouldEqual, "testTransactionID")
						So(extensionCreatorUser2.ChannelId, ShouldEqual, "123456")
						So(extensionCreatorUser2.Revenue, ShouldEqual, .2)

					})

					Convey("Succeeds when SNS client succeeds on second call with whitelisted extension", func() {
						snsClient.On("PostToTopic", mock.Anything, mock.Anything).Return(nil).Once()
						err := handler.HandleUseBitsOnExtension(context.Background(), testUseBitsOnExtensionMessageWhitelisted())
						So(err, ShouldBeNil)

						channelJSON := snsClient.Calls[0].Arguments.Get(1).(string)
						extensionCreatorJSON := snsClient.Calls[1].Arguments.Get(1).(string)

						channelTx := jsonToRevenueMessage(channelJSON)
						extensionCreator := jsonToRevenueMessage(extensionCreatorJSON)

						So(channelTx.TransactionId, ShouldEqual, "testTransactionID")
						So(channelTx.ChannelId, ShouldEqual, "testChannelID")
						So(channelTx.Revenue, ShouldEqual, 1)

						So(extensionCreator.TransactionId, ShouldEqual, "testTransactionID")
						So(extensionCreator.ChannelId, ShouldEqual, "1234567")
						So(extensionCreator.Revenue, ShouldEqual, 0)
					})
				})
			})
		})
	})
}

func testUseBitsOnPollMessage() *models.UseBitsOnPollMessage {
	return &models.UseBitsOnPollMessage{
		TransactionId:          "testTransactionID",
		RequestingTwitchUserId: "userID",
		TargetTwitchUserId:     "testChannelID",
		BitsProccessed:         1,
		PollId:                 "testPollID",
		PollChoiceId:           "testPollChoiceID",
	}
}

func TestHandler_HandleUseBitsOnPoll(t *testing.T) {
	Convey("Given a revenue handler", t, func() {
		snsClient := new(sns_mock.ISNSClient)
		partnerManager := new(partner_mock.IPartnerManager)
		handler := &Handler{
			Config: &config.Configuration{
				ExtensionBroadcasterShare: 0.8,
				ExtensionDeveloperShare:   0.2,
				ExtensionRevshareWhitelist: map[string]bool{
					"ut0qm2nbknuy64d7t509w616a8d85l": true,
				},
			},
			SNSClient:      snsClient,
			PartnerManager: partnerManager,
		}

		Convey("Returns early when it receives a nil message", func() {
			err := handler.HandleUseBitsOnPoll(context.Background(), nil)
			So(err, ShouldBeNil)
			snsClient.AssertNotCalled(t, "PostToTopic", mock.Anything, mock.Anything)
		})

		Convey("Errors when PartnerManager errors", func() {
			partnerManager.On("GetPartnerType", mock.Anything, "testChannelID").Return(partner.TraditionalPartnerType, errors.New("test error"))
			err := handler.HandleUseBitsOnPoll(context.Background(), testUseBitsOnPollMessage())
			So(err, ShouldNotBeNil)
		})

		Convey("Errors when PartnerManager returns non-partner", func() {
			partnerManager.On("GetPartnerType", mock.Anything, "testChannelID").Return(partner.NonPartnerPartnerType, nil)
			err := handler.HandleUseBitsOnPoll(context.Background(), testUseBitsOnPollMessage())
			So(err, ShouldNotBeNil)
		})

		Convey("When PartnerManager returns partner", func() {
			partnerManager.On("GetPartnerType", mock.Anything, "testChannelID").Return(partner.TraditionalPartnerType, nil)

			Convey("Errors when SNS client errors", func() {
				snsClient.On("PostToTopic", mock.Anything, mock.Anything).Return(errors.New("test error"))
				err := handler.HandleUseBitsOnPoll(context.Background(), testUseBitsOnPollMessage())
				So(err, ShouldNotBeNil)
			})

			Convey("Succeeds when SNS client succeeds", func() {
				snsClient.On("PostToTopic", mock.Anything, mock.Anything).Return(nil).Once()
				err := handler.HandleUseBitsOnPoll(context.Background(), testUseBitsOnPollMessage())
				So(err, ShouldBeNil)

				json := snsClient.Calls[0].Arguments.Get(1).(string)
				tx := jsonToRevenueMessage(json)

				So(tx.TransactionId, ShouldEqual, "testTransactionID")
				So(tx.ChannelId, ShouldEqual, "testChannelID")
				So(tx.Revenue, ShouldEqual, 1)
				So(tx.RevenueSource, ShouldEqual, "bits-on-poll:testPollID")
				So(tx.SKU, ShouldEqual, "bits-on-poll")
			})
		})
	})
}
