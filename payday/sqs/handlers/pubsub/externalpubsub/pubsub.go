package externalpubsub

import (
	"context"
	"time"

	pubclient "code.justin.tv/chat/pubsub-go-pubclient/client"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/metrics"
	"code.justin.tv/commerce/payday/models"
	sfn_client "code.justin.tv/commerce/payday/sfn/client"
	sfn_models "code.justin.tv/commerce/payday/sfn/models"
	"code.justin.tv/commerce/payday/userservice"
	"code.justin.tv/commerce/payday/utils/experiments"
	"code.justin.tv/commerce/payday/utils/math"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/service/sfn"
)

type PubsubHandler struct {
	UserServiceFetcher userservice.Fetcher                 `inject:""`
	PubClient          pubclient.PubClient                 `inject:""`
	Statter            metrics.Statter                     `inject:""`
	TransactionsDao    dynamo.ITransactionsDao             `inject:""`
	ExperimentFetcher  experiments.ExperimentConfigFetcher `inject:""`
	SFNClient          sfn_client.SFNClient                `inject:""`
}

func (h *PubsubHandler) HandleGiveBitsToBroadcaster(parent context.Context, msg *models.GiveBitsToBroadcasterMessage) error {
	startTime := time.Now()
	defer func() {
		h.Statter.TimingDuration("PaydaySQSPubsubV1Handler", time.Since(startTime))
	}()

	userID := msg.RequestingTwitchUserId
	channelID := msg.TargetTwitchUserId
	transactionID := msg.TransactionId

	err := h.SFNClient.ExecuteBitsUsageEntitlementsStateMachine(sfn_models.BitsUsageEntitlerInput{
		TransactionId:          transactionID,
		TransactionType:        msg.TransactionType,
		BitsProcessed:          math.IntAbs(msg.BitsSent),
		TotalBitsToBroadcaster: msg.TotalBitsToBroadcaster,
		RequestingTwitchUserId: userID,
		TargetTwitchUserId:     channelID,
		ShouldPubsub:           true,
		TimeOfEvent:            msg.TimeOfEvent,
		IsAnonymous:            msg.IsAnonymous,
		ExtraContext:           msg.ExtraContext,
		SponsoredAmounts:       msg.SponsoredAmounts,
	})
	if err != nil {
		var existingExecutionError bool
		if awsErr, ok := err.(awserr.Error); ok {
			if awsErr.Code() == sfn.ErrCodeExecutionAlreadyExists {
				existingExecutionError = true
			}
		}

		fields := log.Fields{
			"TransactionType": msg.TransactionType,
			"TransactionID":   transactionID,
			"UserID":          userID,
			"ChannelID":       channelID,
		}
		if existingExecutionError {
			log.WithFields(fields).Infof("found existing bits usage entitlements SFN execution in externalpubsub/pubsub")
		} else {
			log.WithFields(fields).WithError(err).Error("error creating bits usage entitlements SFN execution in externalpubsub/pubsub")
			return err
		}
	}

	return nil
}

func (h *PubsubHandler) HandleAddBitsToCustomerAccount(ctx context.Context, msg *models.AddBitsToCustomerAccountMessage) error {
	// Pubsub doesn't consume this event
	return nil
}

func (h *PubsubHandler) HandleRemoveBitsFromCustomerAccount(ctx context.Context, msg *models.RemoveBitsFromCustomerAccountMessage) error {
	// Pubsub doesn't consume this event
	return nil
}

func (h *PubsubHandler) HandleUseBitsOnExtension(ctx context.Context, msg *models.UseBitsOnExtensionMessage) error {
	// External pubsub doesn't consume this event
	return nil
}

func (h *PubsubHandler) HandleUseBitsOnPoll(ctx context.Context, msg *models.UseBitsOnPollMessage) error {
	// External pubsub doesn't consume this event
	return nil
}

func (h *PubsubHandler) HandleCreateHold(ctx context.Context, msg *models.CreateHoldMessage) error {
	// External pubsub doesn't consume this event
	return nil
}

func (h *PubsubHandler) HandleFinalizeHold(ctx context.Context, msg *models.FinalizeHoldMessage) error {
	// External pubsub doesn't consume this event
	return nil
}

func (h *PubsubHandler) HandleReleaseHold(ctx context.Context, msg *models.ReleaseHoldMessage) error {
	// External pubsub doesn't consume this event
	return nil
}
