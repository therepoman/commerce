package deprecatedpubsub

import (
	"context"
	"encoding/json"
	"time"

	pubclient "code.justin.tv/chat/pubsub-go-pubclient/client"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/hystrix"
	cmds "code.justin.tv/commerce/payday/hystrix"
	"code.justin.tv/commerce/payday/metrics"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/sqs/handlers"
	"code.justin.tv/commerce/payday/userservice"
	"code.justin.tv/commerce/payday/utils/pointers"
)

const (
	//Max message age in seconds
	maxMessageAge = 45.0
)

type PubsubHandler struct {
	UserServiceFetcher userservice.Fetcher     `inject:""`
	PubClient          pubclient.PubClient     `inject:""`
	Statter            metrics.Statter         `inject:""`
	TransactionsDao    dynamo.ITransactionsDao `inject:""`
}

func (h *PubsubHandler) HandleGiveBitsToBroadcaster(ctx context.Context, msg *models.GiveBitsToBroadcasterMessage) error {
	requestStart := time.Now()
	defer func() {
		h.Statter.TimingDuration("PaydaySQSPubsubHandler", time.Since(requestStart))
	}()

	secondsSince := time.Since(msg.TimeOfEvent).Seconds()

	if msg.IsAnonymous {
		//skip anonymous cheer for deprecated pubsub so we don't leak info
		return nil
	}

	if secondsSince > maxMessageAge {
		log.WithField("msg", msg).Warn("Encountered old pubsub message skipping message")
		h.Statter.Inc("SkippedPubSubMessage", 1)
		return nil
	}

	messagesChannel := make(chan *dynamo.Transaction, 1)
	err := hystrix.Do(cmds.GetTransactionsCommand, func() error {
		message, err := h.TransactionsDao.Get(msg.TransactionId)

		if err != nil {
			return err
		}

		messagesChannel <- message
		return nil
	}, nil)

	if err != nil {
		return errors.Notef(err, "Could not retrieve transaction %s.", msg.TransactionId)
	}

	message := <-messagesChannel
	if message == nil {
		return errors.Newf("No dynamo record for transaction %s.", msg.TransactionId)
	}

	sender, err := h.resolveUserName(ctx, msg.RequestingTwitchUserId)
	if err != nil {
		return errors.Notef(err, "Could not retrieve username for user [%s]", msg.RequestingTwitchUserId)
	}
	recipient, err := h.resolveUserName(ctx, msg.TargetTwitchUserId)
	if err != nil {
		return errors.Notef(err, "Could not retrieve username for channel [%s]", msg.TargetTwitchUserId)
	}

	topics := []string{"channel-bitsevents" + "." + msg.TargetTwitchUserId}
	pubsubMessage := models.PubsubTransactionMessage{
		UserName:      sender,
		ChannelName:   recipient,
		UserId:        msg.RequestingTwitchUserId,
		ChannelId:     msg.TargetTwitchUserId,
		Time:          msg.TimeOfEvent,
		ChatMessage:   pointers.StringOrDefault(message.TmiMessage, ""),
		BitsUsed:      handlers.BitsUsedWithSponsor(msg.BitsSent, msg.SponsoredAmounts),
		TotalBitsUsed: msg.TotalBitsToBroadcaster,
		Context:       "cheer",
	}
	marshalled, err := json.Marshal(pubsubMessage)

	if err != nil {
		return errors.Notef(err, "Could not marshal pubsub message %v", pubsubMessage)
	}

	err = h.PubClient.Publish(ctx, topics, string(marshalled), nil)
	if err != nil {
		return errors.Notef(err, "Could not publish message %s to pubsub", string(marshalled))
	}

	return nil
}

func (h *PubsubHandler) HandleAddBitsToCustomerAccount(ctx context.Context, msg *models.AddBitsToCustomerAccountMessage) error {
	// Pubsub doesn't consume this event
	return nil
}

func (h *PubsubHandler) HandleRemoveBitsFromCustomerAccount(ctx context.Context, msg *models.RemoveBitsFromCustomerAccountMessage) error {
	// Pubsub doesn't consume this event
	return nil
}

func (h *PubsubHandler) HandleUseBitsOnExtension(ctx context.Context, msg *models.UseBitsOnExtensionMessage) error {
	// External pubsub doesn't consume this event
	return nil
}

func (h *PubsubHandler) HandleUseBitsOnPoll(ctx context.Context, msg *models.UseBitsOnPollMessage) error {
	// External pubsub doesn't consume this event
	return nil
}

func (h *PubsubHandler) HandleCreateHold(ctx context.Context, msg *models.CreateHoldMessage) error {
	// External pubsub doesn't consume this event
	return nil
}

func (h *PubsubHandler) HandleFinalizeHold(ctx context.Context, msg *models.FinalizeHoldMessage) error {
	// External pubsub doesn't consume this event
	return nil
}

func (h *PubsubHandler) HandleReleaseHold(ctx context.Context, msg *models.ReleaseHoldMessage) error {
	// External pubsub doesn't consume this event
	return nil
}

func (h *PubsubHandler) resolveUserName(ctx context.Context, userId string) (string, error) {
	username, err := h.UserServiceFetcher.Fetch(ctx, userId)
	if err != nil {
		msg := "Failed looking up channel via user service"
		log.WithError(err).Error(msg)
		return "", errors.Notef(err, msg)
	}
	if username == nil || username.Login == nil {
		msg := "Could not find channel in user service lookup for id"
		log.WithError(err).WithField("userId", userId).Error(msg)
		return "", errors.New(msg)
	}

	return *username.Login, nil
}
