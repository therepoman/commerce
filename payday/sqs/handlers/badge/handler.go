package badge

import (
	"context"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/models"
	sfn_client "code.justin.tv/commerce/payday/sfn/client"
	sfn_models "code.justin.tv/commerce/payday/sfn/models"
	"code.justin.tv/commerce/payday/utils/experiments"
	"code.justin.tv/commerce/payday/utils/math"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/service/sfn"
)

type Handler struct {
	ExperimentFetcher experiments.ExperimentConfigFetcher `inject:""`
	SFNClient         sfn_client.SFNClient                `inject:""`
}

func (h *Handler) HandleGiveBitsToBroadcaster(ctx context.Context, msg *models.GiveBitsToBroadcasterMessage) error {
	// Chat badge entitlement is performed by the externalpubsub/v1 handler since pubsub messages contain new chat badge entitlements
	return nil
}

func (h *Handler) HandleAddBitsToCustomerAccount(ctx context.Context, msg *models.AddBitsToCustomerAccountMessage) error {
	return nil
}

func (h *Handler) HandleRemoveBitsFromCustomerAccount(ctx context.Context, msg *models.RemoveBitsFromCustomerAccountMessage) error {
	return nil
}

func (h *Handler) HandleUseBitsOnExtension(ctx context.Context, msg *models.UseBitsOnExtensionMessage) error {
	if msg == nil {
		return nil
	}

	return msg.HandleBitsRewardAttributions(func(broadcaster string, bits int, totalBitsWithBroadcaster int) error {
		return h.handle(ctx, "extension", totalBitsWithBroadcaster, msg.RequestingTwitchUserId, broadcaster, bits, msg.TransactionId, msg.TransactionType)
	})
}

func (h *Handler) HandleUseBitsOnPoll(ctx context.Context, msg *models.UseBitsOnPollMessage) error {
	if msg == nil {
		return nil
	}

	return h.handle(ctx, "poll", msg.TotalBitsToBroadcaster, msg.RequestingTwitchUserId, msg.TargetTwitchUserId, msg.BitsProccessed, msg.TransactionId, msg.TransactionType)
}

func (h *Handler) handle(ctx context.Context, msgType string, totalBitsToBroadcaster int, requestingTwitchUserId string, targetTwitchUserId string, bitsProccessed int, transactionId string, transactionType string) error {
	if totalBitsToBroadcaster <= 0 {
		return nil
	}

	err := h.SFNClient.ExecuteBitsUsageEntitlementsStateMachine(sfn_models.BitsUsageEntitlerInput{
		TransactionId:          transactionId,
		TransactionType:        transactionType,
		BitsProcessed:          math.IntAbs(bitsProccessed),
		TotalBitsToBroadcaster: totalBitsToBroadcaster,
		RequestingTwitchUserId: requestingTwitchUserId,
		TargetTwitchUserId:     targetTwitchUserId,
		ShouldPubsub:           false,
	})
	if err != nil {
		var existingExecutionError bool
		if awsErr, ok := err.(awserr.Error); ok {
			if awsErr.Code() == sfn.ErrCodeExecutionAlreadyExists {
				existingExecutionError = true
			}
		}

		fields := log.Fields{
			"TransactionType": transactionType,
			"TransactionID":   transactionId,
			"UserID":          requestingTwitchUserId,
			"ChannelID":       targetTwitchUserId,
		}
		if existingExecutionError {
			log.WithFields(fields).Infof("found existing bits usage entitlements SFN execution in badge/handler")
		} else {
			log.WithFields(fields).WithError(err).Error("error creating bits usage entitlements execution in badge/handler")
			return err
		}
	}

	return nil
}

func (h *Handler) HandleCreateHold(ctx context.Context, msg *models.CreateHoldMessage) error {
	// we don't care about holds
	return nil
}

func (h *Handler) HandleFinalizeHold(ctx context.Context, msg *models.FinalizeHoldMessage) error {
	//TODO: We should be doing the check for badge entitlement
	return nil
}

func (h *Handler) HandleReleaseHold(ctx context.Context, msg *models.ReleaseHoldMessage) error {
	// we don't care about holds here
	return nil
}
