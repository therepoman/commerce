package pachter

import (
	"context"
	"encoding/json"
	"errors"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/pachter"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"code.justin.tv/commerce/payday/transactions"
	"github.com/aws/aws-sdk-go/service/sqs"
)

type Handler struct {
	TransactionGetter transactions.Getter   `inject:""`
	Config            *config.Configuration `inject:""`
	Messenger         pachter.Messenger     `inject:""`
}

func NewHandler() *Handler {
	return &Handler{}
}

func (h *Handler) Handle(ctx context.Context, sqsMessage *sqs.Message) error {
	log := logrus.WithField("SQS", "pachter-ingestion")
	if sqsMessage == nil {
		msg := "received nil message"
		log.Error(msg)
		return errors.New(msg)
	}

	var snsMsg models.SNSMessage
	err := json.Unmarshal([]byte(*sqsMessage.Body), &snsMsg)
	if err != nil {
		log.WithError(err).Error("unable to unmarshal SNS message struct")
		return err
	}

	var pachinkoMsg models.PachinkoSNSUpdate
	err = json.Unmarshal([]byte(snsMsg.Message), &pachinkoMsg)
	if err != nil {
		log.WithError(err).Error("unable to unmarshal Pachinko message struct")
		return err
	}

	transactionIDs := dedup(pachinkoMsg.TransactionIDs)
	log = log.WithFields(logrus.Fields{
		"# of Pachinko transactions": len(transactionIDs),
		"Pachinko transaction IDs":   transactionIDs,
	})
	log.Info("Pachter ingestion")

	txs, err := h.TransactionGetter.GetTransactionsByID(ctx, transactionIDs)
	if err != nil {
		log.WithError(err).Error("unable to fetch transactions from Dynamo")
		return err
	}

	// Send all the transactions that were returned from the TransactionGetter for now,
	// even if some transactions might not have been returned. We handle that case after
	// this loop. See the next comment section.
	for _, tx := range txs {
		err := h.Messenger.SendTransactionToPachter(ctx, tx)
		if err != nil {
			return err
		}
	}

	// If some transactions weren't returned from the TransactionGetter above, we error here
	// so that the SQS queue retries until all the transactions have been returned and sent
	// to Pachter. FYI, Pachter has idempotency check on transaction id, so publishing the
	// same transactions multiple times to Pachter won't cause issues.
	if len(txs) != len(transactionIDs) {
		msg := "TransactionGetter didn't return all transactions requested"
		log.WithFields(logrus.Fields{
			"# of Payday transactions":  len(txs),
			"Payday transactions":       txs,
			"Transactions not returned": findMissingTransactions(txs, transactionIDs),
		}).Error(msg)
		return errors.New(msg)
	}

	return nil
}

func dedup(original []string) (deduped []string) {
	exists := make(map[string]bool)
	for _, e := range original {
		if _, exist := exists[e]; !exist {
			exists[e] = true
			deduped = append(deduped, e)
		}
	}
	return
}

func findMissingTransactions(txs []*paydayrpc.Transaction, txIDs []string) []string {
	missingTransactionIDs := make([]string, 0)
	for _, txID := range txIDs {
		for i, tx := range txs {
			// this should not happen, but just a safeguard
			if tx == nil {
				continue
			}
			if tx.TransactionId == txID {
				break
			}
			if i == len(txs)-1 {
				missingTransactionIDs = append(missingTransactionIDs, txID)
			}
		}
	}
	return missingTransactionIDs
}
