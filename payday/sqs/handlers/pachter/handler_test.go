package pachter

import (
	"context"
	"encoding/json"
	"errors"
	"testing"

	"code.justin.tv/commerce/payday/config"
	pachter_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/pachter"
	transactions_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/transactions"
	"code.justin.tv/commerce/payday/models"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"code.justin.tv/commerce/payday/utils/pointers"
	"github.com/aws/aws-sdk-go/service/sqs"
	. "github.com/smartystreets/goconvey/convey"
)

func TestHandler_Handle(t *testing.T) {
	mockCfg := config.Configuration{PachterSNSTopic: "lesad-pachter-sns"}
	mockPachterMessenger := new(pachter_mock.Messenger)
	mockTransactionGetter := new(transactions_mock.Getter)
	handler := Handler{
		TransactionGetter: mockTransactionGetter,
		Config:            &mockCfg,
		Messenger:         mockPachterMessenger,
	}
	ctx := context.Background()

	Convey("Given Pachter handler", t, func() {
		Convey("when SQS message is nil", func() {
			err := handler.Handle(ctx, nil)

			So(err, ShouldNotBeNil)
			mockTransactionGetter.AssertNumberOfCalls(t, "GetTransactionsByID", 0)
		})

		Convey("when the SQS body is not even an SNS message", func() {
			err := handler.Handle(ctx, &sqs.Message{
				Body: pointers.StringP("invalid"),
			})

			So(err, ShouldNotBeNil)
			mockTransactionGetter.AssertNumberOfCalls(t, "GetTransactionsByID", 0)
		})

		Convey("when the SNS message is not valid Pachinko balance update", func() {
			snsMsg := &models.SNSMessage{
				MessageAttributes: map[string]models.SNSMessageAttribute{},
				Message:           string("invalid message"),
			}
			snsJson, _ := json.Marshal(snsMsg)
			err := handler.Handle(ctx, &sqs.Message{
				Body: pointers.StringP(string(snsJson)),
			})

			So(err, ShouldNotBeNil)
			mockTransactionGetter.AssertNumberOfCalls(t, "GetTransactionsByID", 0)
		})

		Convey("when transaction getter fails", func() {
			mockTransactionGetter.On("GetTransactionsByID", ctx, []string{"123"}).
				Return(nil, errors.New("lesad getter")).Once()
			err := handler.Handle(ctx, testSQSMessage("123"))

			So(err, ShouldNotBeNil)
			mockTransactionGetter.AssertCalled(t, "GetTransactionsByID", ctx, []string{"123"})
			mockPachterMessenger.AssertNumberOfCalls(t, "SendTransactionToPachter", 0)
		})

		Convey("when SNS publish fails", func() {
			mockTransactionGetter.On("GetTransactionsByID", ctx, []string{"123"}).
				Return(testTransactions("123"), nil).Once()
			mockPachterMessenger.On("SendTransactionToPachter", ctx, &paydayrpc.Transaction{
				TransactionId: "123",
			}).
				Return(errors.New("lesad SNS")).Once()
			err := handler.Handle(ctx, testSQSMessage("123"))

			So(err, ShouldNotBeNil)
			mockPachterMessenger.AssertNumberOfCalls(t, "SendTransactionToPachter", 1)
		})

		Convey("when all succeeds", func() {
			mockTransactionGetter.On("GetTransactionsByID", ctx, []string{"123"}).
				Return(testTransactions("123"), nil).Once()

			mockPachterMessenger.On("SendTransactionToPachter", ctx, &paydayrpc.Transaction{
				TransactionId: "123",
			}).Return(nil).Once()
			err := handler.Handle(ctx, testSQSMessage("123"))

			So(err, ShouldBeNil)
		})

		Convey("when transaction getter doesn't return all transaction requested", func() {
			mockTransactionGetter.On("GetTransactionsByID", ctx, []string{"123", "456"}).
				Return(testTransactions("123"), nil).Once()

			mockPachterMessenger.On("SendTransactionToPachter", ctx, &paydayrpc.Transaction{
				TransactionId: "123",
			}).Return(nil).Once()
			err := handler.Handle(ctx, testSQSMessage("123", "456"))

			So(err, ShouldNotBeNil)
			mockTransactionGetter.AssertCalled(t, "GetTransactionsByID", ctx, []string{"123", "456"})
			mockPachterMessenger.AssertCalled(t, "SendTransactionToPachter", ctx, &paydayrpc.Transaction{
				TransactionId: "123",
			})
		})
	})
}

func testSQSMessage(transactionIDs ...string) *sqs.Message {
	event := &models.PachinkoSNSUpdate{
		TransactionIDs: transactionIDs,
	}
	eventBytes, err := json.Marshal(event)
	So(err, ShouldBeNil)
	snsMsg := &models.SNSMessage{
		MessageAttributes: map[string]models.SNSMessageAttribute{},
		Message:           string(eventBytes),
	}
	snsJsonBytes, err := json.Marshal(snsMsg)
	So(err, ShouldBeNil)
	return &sqs.Message{
		Body: pointers.StringP(string(snsJsonBytes)),
	}
}

func testTransactions(transactionID string) []*paydayrpc.Transaction {
	return []*paydayrpc.Transaction{
		{
			TransactionId: transactionID,
		},
	}
}
