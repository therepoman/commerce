package imageupdate

import (
	"context"
	"encoding/json"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/cache/channel"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/image"
	"code.justin.tv/commerce/payday/models/api"
	"code.justin.tv/commerce/payday/utils/pointers"
	"code.justin.tv/commerce/payday/utils/strings"
	"github.com/aws/aws-sdk-go/service/sqs"
)

type ImageUpdateHandler struct {
	ImageManager   image.IImageManager    `inject:""`
	ChannelManager channel.ChannelManager `inject:""`
}

func (h *ImageUpdateHandler) HandleMessage(ctx context.Context, message *sqs.Message) (outErr error) {
	if strings.BlankP(message.Body) {
		return errors.New("Cannot parse sqs message with nil body")
	}

	var update dynamo.TableUpdate
	err := json.Unmarshal([]byte(*message.Body), &update)
	if err != nil {
		return errors.Notef(err, "Cannot unmarshal event json")
	}

	if update.EventName == "REMOVE" {
		log.Infof("Received dynamo update [%v] for image deletion. Skipping", update)
		return nil
	}

	table := &dynamo.ImagesTable{}
	record, err := table.ConvertAttributeMapToRecord(update.DynamoDB.NewImage)

	if err != nil {
		return errors.Notef(err, "Could not convert record into dynamo record")
	}

	imageRecord, ok := record.(*dynamo.Image)
	if !ok {
		return errors.New("Could not convert record into image")
	}

	channel, err := h.ChannelManager.GetByImageSetId(imageRecord.SetId)
	if err != nil {
		return errors.Notef(err, "Could not get channel from dynamo")
	} else if channel == nil {
		log.Infof("Could not find channel with set id %s. Skipping", imageRecord.SetId)
		return nil
	}

	images, err := h.ImageManager.GetImages(ctx, imageRecord.SetId)
	if err != nil {
		return errors.Notef(err, "Could not get images")
	}

	if !images.AllImagesSet() {
		return nil
	}

	if channel.CustomCheermoteStatus != nil {
		return nil
	}

	channel.CustomCheermoteStatus = pointers.StringP(api.CustomCheermoteStatusEnabled)
	err = h.ChannelManager.Update(ctx, channel)
	if err != nil {
		log.WithError(err).Error("Failed to update dynamo channel")
		return errors.Notef(err, "Set channel to enabled")
	}

	err = h.ImageManager.TrackChannelEnabled(ctx, *channel)
	if err != nil {
		log.WithError(err).Error("Could not send datascience event")
		return
	}
	return nil
}
