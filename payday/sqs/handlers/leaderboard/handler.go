package leaderboard

import (
	"context"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	"code.justin.tv/commerce/payday/api"
	"code.justin.tv/commerce/payday/clients/pantheon"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/sqs/handlers"
	"code.justin.tv/commerce/payday/utils/math"
)

type Handler struct {
	PantheonClient pantheon.IPantheonClientWrapper `inject:""`
}

func (h *Handler) HandleGiveBitsToBroadcaster(ctx context.Context, msg *models.GiveBitsToBroadcasterMessage) error {
	// Do not update leaderboard for anonymous cheering
	if msg.IsAnonymous {
		return nil
	}

	baseBitsPublishEvent := pantheonrpc.PublishEventReq{
		TimeOfEvent: uint64(msg.TimeOfEvent.UnixNano()),
		Domain:      "",
		GroupingKey: msg.TargetTwitchUserId,
		EntryKey:    msg.RequestingTwitchUserId,
		EventValue:  int64(handlers.BitsUsedWithSponsor(msg.BitsSent, msg.SponsoredAmounts)),
		EventId:     msg.TransactionId,
	}
	return h.publishToBothDomains(ctx, baseBitsPublishEvent)
}

func (h *Handler) HandleAddBitsToCustomerAccount(ctx context.Context, msg *models.AddBitsToCustomerAccountMessage) error {
	return nil
}

func (h *Handler) HandleRemoveBitsFromCustomerAccount(ctx context.Context, msg *models.RemoveBitsFromCustomerAccountMessage) error {
	return nil
}

func (h *Handler) HandleUseBitsOnExtension(ctx context.Context, msg *models.UseBitsOnExtensionMessage) error {
	return msg.HandleBitsRewardAttributions(func(broadcasterId string, bits int, unusedTotalBitsWithBroadcaster int) error {
		if msg.TimeOfEvent.UnixNano() == 0 {
			msg.TimeOfEvent = time.Now()
		}
		baseBitsPublishEvent := pantheonrpc.PublishEventReq{
			TimeOfEvent: uint64(msg.TimeOfEvent.UnixNano()),
			Domain:      "",
			GroupingKey: broadcasterId,
			EntryKey:    msg.RequestingTwitchUserId,
			EventValue:  int64(math.IntAbs(bits)),
			EventId:     msg.TransactionId,
		}

		err := h.publishToBothDomains(ctx, baseBitsPublishEvent)
		return err
	})
}

func (h *Handler) HandleUseBitsOnPoll(ctx context.Context, msg *models.UseBitsOnPollMessage) error {
	baseBitsPublishEvent := pantheonrpc.PublishEventReq{
		TimeOfEvent: uint64(msg.TimeOfEvent.UnixNano()),
		Domain:      "",
		GroupingKey: msg.TargetTwitchUserId,
		EntryKey:    msg.RequestingTwitchUserId,
		EventValue:  int64(math.IntAbs(msg.BitsProccessed)),
		EventId:     msg.TransactionId,
	}
	return h.publishToBothDomains(ctx, baseBitsPublishEvent)
}

func (h *Handler) HandleCreateHold(ctx context.Context, msg *models.CreateHoldMessage) error {
	// we don't care about holds
	return nil
}

func (h *Handler) HandleFinalizeHold(ctx context.Context, msg *models.FinalizeHoldMessage) error {
	//TODO: definitely need to do things here (probably)
	return nil
}

func (h *Handler) HandleReleaseHold(ctx context.Context, msg *models.ReleaseHoldMessage) error {
	// we don't care about holds here
	return nil
}

func (h *Handler) publishToBothDomains(ctx context.Context, bitsEvent pantheonrpc.PublishEventReq) error {
	err := h.publishEvent(ctx, bitsEvent, pantheon.PantheonBitsUsageDomain)
	if err != nil {
		log.WithError(err).Error("error publishing event to channel pantheon leaderboard")
		return err
	}

	err = h.publishEvent(ctx, bitsEvent, api.PantheonBitsGlobalDomain)
	if err != nil {
		log.WithError(err).Error("error publishing event to global pantheon leaderboard")
	}

	return nil
}

func (h *Handler) publishEvent(ctx context.Context, bitsEvent pantheonrpc.PublishEventReq, domain string) error {
	bitsEvent.Domain = domain
	err := h.PantheonClient.PublishEvent(ctx, bitsEvent)

	if err != nil {
		logFields := log.Fields{
			"publish_event_request": bitsEvent,
			"domain":                bitsEvent.Domain,
			"grouping_key":          bitsEvent.GroupingKey,
			"entry_key":             bitsEvent.EntryKey,
			"entry_value":           bitsEvent.EventValue,
		}
		logMsg := "Error while publishing leaderboard event"
		log.WithFields(logFields).WithError(err).Error(logMsg)
		err = errors.Notef(err, logMsg, logFields)
		return err
	}

	return nil
}
