package datascienceSQS

import (
	"context"
	"encoding/json"
	"fmt"
	"strconv"
	"strings"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/actions"
	"code.justin.tv/commerce/payday/backend/clients/pachter"
	"code.justin.tv/commerce/payday/datascience"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/errors"
	bitsmessage "code.justin.tv/commerce/payday/message"
	"code.justin.tv/commerce/payday/metrics"
	"code.justin.tv/commerce/payday/middleware"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/models/api"
	"code.justin.tv/commerce/payday/sqs/handlers"
	"code.justin.tv/commerce/payday/utils/math"
)

type DataScienceHandler struct {
	DataScienceClient datascience.DataScience       `inject:""`
	TokenizerFactory  bitsmessage.ITokenizerFactory `inject:""`
	Statter           metrics.Statter               `inject:""`
	TransactionsDao   dynamo.ITransactionsDao       `inject:""`
	ActionsRetriever  actions.Retriever             `inject:"chain"`
	PachterClient     pachter.Client                `inject:""`
}

func (h *DataScienceHandler) HandleGiveBitsToBroadcaster(parent context.Context, msg *models.GiveBitsToBroadcasterMessage) error {
	msgExtraContext := msg.ExtraContext
	var extraContext models.FabsGiveBitsExtraContext
	err := json.Unmarshal([]byte(msgExtraContext), &extraContext)
	var ctx context.Context
	if err == nil {
		ctx = context.WithValue(parent, middleware.ClientIDContextKey, extraContext.ClientID)
	} else {
		ctx = parent
	}
	logger := log.WithFields(log.Fields{
		"transactionId": msg.TransactionId,
		"event":         datascience.BitsUsedEventNameV2,
		"type":          models.GiveBitsToBroadcaster.Name(),
	})

	tokenizer, err := h.TokenizerFactory.NewTokenizer(ctx, msg.TargetTwitchUserId, msg.RequestingTwitchUserId)
	if err != nil {
		logger.WithFields(log.Fields{
			"targetTwitchUserId":     msg.TargetTwitchUserId,
			"requestingTwitchUserId": msg.RequestingTwitchUserId,
		}).WithError(err).Error("error creating tokenizer")
		return err
	}

	_, allTokenValues, emoteOrder, _, includesCustomText, _, err := tokenizer.TokenizeWithEmoteOrderAndIncludesChecks(msg.PublicMessage)
	if err != nil {
		logger.WithError(err).Error("error creating tokenizing public message")
		return err
	}

	numChatters, err := strconv.Atoi(extraContext.NumChatters)
	if err != nil {
		numChatters = -1
	}

	actions, err := h.ActionsRetriever.GetActions(parent, api.GetActionsRequest{
		IncludeSponsored: true,
		UserId:           msg.RequestingTwitchUserId,
		ChannelId:        msg.TargetTwitchUserId,
	})

	if err != nil {
		logger.WithFields(log.Fields{
			"targetTwitchUserId":     msg.TargetTwitchUserId,
			"requestingTwitchUserId": msg.RequestingTwitchUserId,
		}).WithError(err).Error("error getting actions")
		return err
	}

	campaignsString := ""
	for _, action := range actions {
		if action.Campaign != nil && action.Campaign.IsEnabled {
			campaignsString += action.Prefix + ", "
		}
	}

	amountTotalEmote := int64(0)
	bitsEmoteCheerBatch := make([]interface{}, len(emoteOrder))
	for index, emote := range emoteOrder {
		bitsEmoteCheerBatch[index] = &models.BitsEmoteCheer{
			TransactionID:      msg.TransactionId,
			UserID:             msg.RequestingTwitchUserId,
			ChannelID:          msg.TargetTwitchUserId,
			EmoteType:          emote,
			EmoteTotal:         allTokenValues[index],
			ServerTime:         msg.TimeOfEvent,
			NumPubSubListeners: numChatters,
			EmoteIndex:         index,
		}

		if emote != "cheer" {
			amountTotalEmote += allTokenValues[index]
		}
	}

	err = h.DataScienceClient.TrackBatchBitsEvent(ctx, datascience.BitsEmoteCheerEventName, bitsEmoteCheerBatch)
	if err != nil {
		logger.WithError(err).Error("Error calling TrackBatchBitsEvent")
		return err
	}

	transaction, err := h.TransactionsDao.Get(msg.TransactionId)
	clientApp := ""
	if err != nil {
		logger.WithError(err).Error("Error getting transaction from dynamo")
	} else if transaction != nil && transaction.ClientApp != nil {
		clientApp = *transaction.ClientApp
	}

	sponsoredAmount := handlers.BitsUsedWithSponsor(0, msg.SponsoredAmounts)

	if msg.IsPachinko {
		accountTypeToBitAmount, err := h.PachterClient.GetBitsSpendOrder(ctx, msg.TransactionId)
		if err != nil {
			logger.WithError(err).Warn("Error calling Pachter for datascience") //TODO: make me back to error
			return err
		}
		if len(accountTypeToBitAmount) == 0 {
			log.WithFields(log.Fields{
				"queue":         "datascience",
				"handler":       "HandleGiveBitsToBroadcaster",
				"transactionID": msg.TransactionId,
			}).Warn("received no bits type from Pachter")
			return errors.New("HandleGiveBitsToBroadcaster: no bits type from Pachter")
		}

		msg.AccountTypeToBitAmount = accountTypeToBitAmount
	}

	cheeringContext := models.CheerContext
	if msg.TransactionType == models.GiveBitsToBroadcasterChallengeTransactionType {
		cheeringContext = models.CheerChallengeContext
	}

	for key, value := range msg.AccountTypeToBitAmount {
		bitsUsedV2 := &models.BitsUsedV2{
			TransactionId:         msg.TransactionId,
			UserId:                msg.RequestingTwitchUserId,
			ChannelID:             msg.TargetTwitchUserId,
			ServerTime:            msg.TimeOfEvent,
			UsedTotal:             handlers.BitsUsedWithSponsor(msg.BitsSent, msg.SponsoredAmounts),
			UsedOfThisType:        math.IntAbs(value),
			BitType:               key,
			CurrentBalance:        int32(msg.TotalBitsAfterTransaction),
			Context:               cheeringContext,
			ContextDetails:        msg.PublicMessage,
			AmountTotalEmote:      int(amountTotalEmote),
			NumPubSubListeners:    numChatters,
			NumOfCheermotes:       len(allTokenValues),
			IsAnonymous:           msg.IsAnonymous,
			AccountTypeSpendOrder: msg.AccountTypeSpendOrderPriorityValueMap[key],
			ClientApp:             clientApp,
			SponsoredAmount:       sponsoredAmount,
			Promotion:             campaignsString,
			IncludesCustomText:    includesCustomText,
			ZipCode:               extraContext.ZipCode,
			CountryCode:           extraContext.CountryCode,
			City:                  extraContext.City,
		}

		err = h.DataScienceClient.TrackBitsEvent(ctx, datascience.BitsUsedEventNameV2, bitsUsedV2)

		if err != nil {
			logger.WithError(err).Error("error sending event")
			return err
		}
	}

	if sponsoredAmount > 0 {
		bitsUsedV2 := &models.BitsUsedV2{
			TransactionId:         msg.TransactionId,
			UserId:                msg.RequestingTwitchUserId,
			ChannelID:             msg.TargetTwitchUserId,
			ServerTime:            msg.TimeOfEvent,
			UsedTotal:             handlers.BitsUsedWithSponsor(msg.BitsSent, msg.SponsoredAmounts),
			UsedOfThisType:        math.IntAbs(sponsoredAmount),
			BitType:               models.SponsoredCheermoteBitType,
			CurrentBalance:        int32(msg.TotalBitsAfterTransaction),
			Context:               cheeringContext,
			ContextDetails:        msg.PublicMessage,
			AmountTotalEmote:      int(amountTotalEmote),
			NumPubSubListeners:    numChatters,
			NumOfCheermotes:       len(allTokenValues),
			IsAnonymous:           msg.IsAnonymous,
			AccountTypeSpendOrder: 0,
			ClientApp:             clientApp,
			SponsoredAmount:       sponsoredAmount,
			Promotion:             campaignsString,
			IncludesCustomText:    includesCustomText,
			ZipCode:               extraContext.ZipCode,
			CountryCode:           extraContext.CountryCode,
			City:                  extraContext.City,
		}

		err = h.DataScienceClient.TrackBitsEvent(ctx, datascience.BitsUsedEventNameV2, bitsUsedV2)

		if err != nil {
			logger.WithError(err).Error("error sending event")
			return err
		}
	}

	for key, value := range msg.AccountTypeToBitAmount {
		bitsCheered := &models.BitsCheered{
			TransactionId:    msg.TransactionId,
			UserId:           msg.RequestingTwitchUserId,
			ChannelID:        msg.TargetTwitchUserId,
			ServerTime:       msg.TimeOfEvent,
			UsedTotal:        math.IntAbs(msg.BitsSent),
			UsedOfThisType:   math.IntAbs(value),
			BitType:          key,
			CurrentBalance:   int32(msg.TotalBitsAfterTransaction),
			Context:          cheeringContext,
			ContextDetails:   msg.PublicMessage,
			AmountTotalEmote: int(amountTotalEmote),
		}

		err = h.DataScienceClient.TrackBitsEvent(ctx, datascience.BitsCheeredEventName, bitsCheered)

		if err != nil {
			logger.WithError(err).Error("error sending event")
			return err
		}
	}

	return nil
}

func (h *DataScienceHandler) HandleAddBitsToCustomerAccount(ctx context.Context, msg *models.AddBitsToCustomerAccountMessage) error {
	transactionRecord, err := msg.GetTransactionRecord()
	if err != nil {
		log.WithFields(log.Fields{
			"transactionId": msg.TransactionId,
			"type":          models.AddBitsToCustomerAccount.Name(),
		}).WithError(err).Errorf("error getting transaction record")
		return err
	}

	// sometimes quantity is not set, in this case, make sure it's set to 1 for data science accuracy.
	if msg.SkuQuantity < 1 {
		msg.SkuQuantity = 1
	}

	for key, value := range msg.AccountTypeToBitAmount {
		bitsAcquiredV2 := &models.BitsAcquiredV2{
			TransactionID:         msg.TransactionId,
			UserID:                msg.TargetTwitchUserId,
			ServerTime:            msg.TimeOfEvent,
			AcquiredTotal:         math.IntAbs(msg.TotalBits),
			AcquiredOfThisType:    math.IntAbs(value),
			BitType:               key,
			AdminID:               msg.AdminID,
			AdminReason:           msg.AdminReason,
			CurrentBalance:        msg.TotalBitsAfterTransaction,
			PurchasePrice:         msg.PurchasePrice,
			PurchasePriceCurrency: msg.PurchasePriceCurrency,
			PurchaseMarketplace:   msg.PurchaseMarketplace,
			PurchaseOrderID:       transactionRecord.DoxOrderID,
			AccountTypeSpendOrder: msg.AccountTypeSpendOrderPriorityValueMap[key],
			TrackingID:            transactionRecord.TrackingID,
			Quantity:              msg.SkuQuantity,
		}

		err = h.DataScienceClient.TrackBitsEvent(ctx, datascience.BitsAcquiredEventNameV2, bitsAcquiredV2)

		if err != nil {
			log.WithFields(log.Fields{
				"transactionId": msg.TransactionId,
				"event":         datascience.BitsAcquiredEventNameV2,
				"type":          models.AddBitsToCustomerAccount.Name(),
			}).WithError(err).Errorf("error sending event")
			return err
		}

		h.Statter.Inc(fmt.Sprintf("BitsAcquired.%s", key), 1)
		h.HandleAddBitsFromSource(key)
	}
	return nil
}

func (h *DataScienceHandler) HandleAddBitsFromSource(source string) {
	if strings.Contains(source, "CB_") {
		if strings.Contains(source, "_PAYPAL") {
			h.Statter.Inc("BitsAcquired.Paypal", 1)
		} else if strings.Contains(source, "_XSOLLA") {
			h.Statter.Inc("BitsAcquired.Xsolla", 1)
		} else {
			h.Statter.Inc("BitsAcquired.Amazon", 1)
		}
	} else if strings.Contains(source, ".android.") {
		h.Statter.Inc("BitsAcquired.Android", 1)
	} else if strings.Contains(source, ".ios.") {
		h.Statter.Inc("BitsAcquired.iOS", 1)
	}
}

func (h *DataScienceHandler) HandleRemoveBitsFromCustomerAccount(ctx context.Context, msg *models.RemoveBitsFromCustomerAccountMessage) error {
	logger := log.WithFields(log.Fields{
		"transactionId": msg.TransactionId,
		"event":         datascience.BitsRemovedEventNameV2,
		"type":          models.RemoveBitsFromCustomerAccount.Name(),
	})

	if msg.IsPachinko {
		accountTypeToBitAmount, err := h.PachterClient.GetBitsSpendOrder(ctx, msg.TransactionId)
		if err != nil {
			logger.WithError(err).Warn("Error calling Pachter for datascience") //TODO: make me back to error
			return err
		}
		if len(accountTypeToBitAmount) == 0 {
			log.WithFields(log.Fields{
				"queue":         "datascience",
				"handler":       "HandleRemoveBitsFromCustomerAccount",
				"transactionID": msg.TransactionId,
			}).Warn("received no bits type from Pachter")
			return errors.New("HandleRemoveBitsFromCustomerAccount: no bits type from Pachter")
		}

		msg.AccountTypeToBitAmount = accountTypeToBitAmount
	}

	for key, value := range msg.AccountTypeToBitAmount {
		bitsRemovedV2 := &models.BitsRemovedV2{
			TransactionID:         msg.TransactionId,
			UserID:                msg.TargetTwitchUserId,
			ServerTime:            msg.TimeOfEvent,
			RemovedTotal:          math.IntAbs(msg.TotalBits),
			RemovedOfThisType:     math.IntAbs(value),
			BitType:               key,
			AdminID:               msg.AdminID,
			AdminReason:           msg.AdminReason,
			CurrentBalance:        msg.TotalBitsAfterTransaction,
			AccountTypeSpendOrder: msg.AccountTypeSpendOrderPriorityValueMap[key],
		}

		err := h.DataScienceClient.TrackBitsEvent(ctx, datascience.BitsRemovedEventNameV2, bitsRemovedV2)
		if err != nil {
			logger.WithError(err).Errorf("error sending event")
			return err
		}
	}
	return nil
}

func (h *DataScienceHandler) HandleUseBitsOnExtension(ctx context.Context, msg *models.UseBitsOnExtensionMessage) error {
	logger := log.WithFields(log.Fields{
		"transactionId": msg.TransactionId,
		"type":          msg.TransactionType,
	})

	if msg.IsPachinko {
		accountTypeToBitAmount, err := h.PachterClient.GetBitsSpendOrder(ctx, msg.TransactionId)
		if err != nil {
			logger.WithError(err).Warn("Error calling Pachter for datascience") //TODO: make me back to error
			return err
		}
		if len(accountTypeToBitAmount) == 0 {
			log.WithFields(log.Fields{
				"queue":         "datascience",
				"handler":       "HandleUseBitsOnExtension",
				"transactionID": msg.TransactionId,
			}).Warn("received no bits type from Pachter")
			return errors.New("HandleUseBitsOnExtension: no bits type from Pachter")
		}

		msg.AccountTypeToBitAmount = accountTypeToBitAmount
	}

	usageContext := models.UseBitsOnExtensionContext
	if msg.TransactionType == models.UseBitsOnExtensionChallengeTransactionType {
		usageContext = models.UseBitsOnExtensionChallengeContext
	}

	for key, value := range msg.AccountTypeToBitAmount {
		bitsUsedOnExtension := &models.BitsUsedOnExtension{
			TransactionID:      msg.TransactionId,
			ExtensionClientID:  msg.ExtensionID,
			SKU:                msg.SKU,
			UserID:             msg.RequestingTwitchUserId,
			ChannelID:          msg.TargetTwitchUserId,
			TotalBitsProcessed: msg.BitsProccessed,
			ExtraContext:       msg.ExtraContext,
			Amount:             value,
			CostBasis:          key,
			Context:            usageContext,
		}
		err := h.DataScienceClient.TrackBitsEvent(ctx, datascience.BitsUsedOnExtensionEventName, bitsUsedOnExtension)
		if err != nil {
			logger.WithField("event", datascience.BitsUsedOnExtensionEventName).WithError(err).Errorf("error sending event")
			return err
		}
	}

	for key, value := range msg.AccountTypeToBitAmount {
		bitsUsedV2 := &models.BitsUsedV2{
			TransactionId:         msg.TransactionId,
			UserId:                msg.RequestingTwitchUserId,
			ChannelID:             msg.TargetTwitchUserId,
			ServerTime:            msg.TimeOfEvent,
			UsedTotal:             math.IntAbs(msg.BitsProccessed),
			UsedOfThisType:        math.IntAbs(value),
			BitType:               key,
			CurrentBalance:        int32(msg.TotalBitsAfterTransaction),
			Context:               usageContext,
			AccountTypeSpendOrder: msg.AccountTypeSpendOrderPriorityValueMap[key],
		}

		err := h.DataScienceClient.TrackBitsEvent(ctx, datascience.BitsUsedEventNameV2, bitsUsedV2)

		if err != nil {
			logger.WithField("event", datascience.BitsUsedEventNameV2).WithError(err).Errorf("error sending event")
			return err
		}
	}

	return nil
}

func (h *DataScienceHandler) HandleUseBitsOnPoll(ctx context.Context, msg *models.UseBitsOnPollMessage) error {
	logger := log.WithFields(log.Fields{
		"transactionId": msg.TransactionId,
		"event":         datascience.BitsUsedEventNameV2,
		"type":          models.UseBitsOnPoll.Name(),
	})

	if msg.IsPachinko {
		accountTypeToBitAmount, err := h.PachterClient.GetBitsSpendOrder(ctx, msg.TransactionId)
		if err != nil {
			logger.WithError(err).Warn("Error calling Pachter for datascience") //TODO: make me back to error
			return err
		}
		if len(accountTypeToBitAmount) == 0 {
			log.WithFields(log.Fields{
				"queue":         "datascience",
				"handler":       "HandleUseBitsOnPoll",
				"transactionID": msg.TransactionId,
			}).Warn("received no bits type from Pachter")
			return errors.New("HandleUseBitsOnPoll: no bits type from Pachter")
		}

		msg.AccountTypeToBitAmount = accountTypeToBitAmount
	}

	for key, value := range msg.AccountTypeToBitAmount {
		bitsUsedV2 := &models.BitsUsedV2{
			TransactionId:         msg.TransactionId,
			UserId:                msg.RequestingTwitchUserId,
			ChannelID:             msg.TargetTwitchUserId,
			ServerTime:            msg.TimeOfEvent,
			UsedTotal:             math.IntAbs(msg.BitsProccessed),
			UsedOfThisType:        math.IntAbs(value),
			BitType:               key,
			CurrentBalance:        int32(msg.TotalBitsAfterTransaction),
			Context:               models.UseBitsOnPollContext,
			ContextID:             msg.PollId,
			AccountTypeSpendOrder: msg.AccountTypeSpendOrderPriorityValueMap[key],
		}

		err := h.DataScienceClient.TrackBitsEvent(ctx, datascience.BitsUsedEventNameV2, bitsUsedV2)

		if err != nil {
			logger.WithError(err).Errorf("error sending event")
			return err
		}
	}
	return nil
}

func (h *DataScienceHandler) HandleCreateHold(ctx context.Context, msg *models.CreateHoldMessage) error {
	//TODO: Work with Erica to see what we want here.
	return nil
}

func (h *DataScienceHandler) HandleFinalizeHold(ctx context.Context, msg *models.FinalizeHoldMessage) error {
	//TODO: Work with Erica to see what we want here & implement bitsUsedv2
	return nil
}

func (h *DataScienceHandler) HandleReleaseHold(ctx context.Context, msg *models.ReleaseHoldMessage) error {
	//TODO: Work with Erica to see what we want here.
	return nil
}
