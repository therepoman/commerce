package affiliateonboarding

import (
	"context"
	"encoding/json"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/metrics"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/onboard"
	"github.com/aws/aws-sdk-go/service/sqs"
)

type AffiliateOnboardEvent struct {
	ChannelID     string    `json:"ChannelID"`
	AffiliateDate time.Time `json:"AffiliateDate"`
}

type Handler struct {
	Statter        metrics.Statter         `inject:""`
	OnboardManager onboard.IOnboardManager `inject:""`
}

func (h *Handler) HandleMessage(ctx context.Context, message *sqs.Message) (outErr error) {
	startTime := time.Now()
	defer func() {
		var success int64
		if outErr != nil {
			success = 0
		} else {
			success = 100
			h.Statter.TimingDuration("OnboardAffiliate", time.Since(startTime))
		}
		h.Statter.Inc("OnboardAffiliateSuccess", success)
	}()

	var sns models.SNSMessage
	if err := json.Unmarshal([]byte(*message.Body), &sns); err != nil {
		msg := "Unable to covert to SNS message struct in affiliate onboarding SQS handler"
		log.WithError(err).Error(msg)
		return errors.Notef(err, msg)
	}

	var affiliateOnboardEvent AffiliateOnboardEvent
	if err := json.Unmarshal([]byte(sns.Message), &affiliateOnboardEvent); err != nil {
		msg := "Unable to covert to affiliate onboard event struct in affiliate onboarding SQS handler"
		log.WithError(err).Error(msg)
		return errors.Notef(err, msg)
	}

	if err := h.OnboardManager.OnboardForBits(ctx, affiliateOnboardEvent.ChannelID); err != nil {
		msg := "Failed to onboard new affiliate for bits"
		log.WithField("channelId", affiliateOnboardEvent.ChannelID).WithError(err).Error(msg)
		return errors.Notef(err, msg)
	}

	return nil
}
