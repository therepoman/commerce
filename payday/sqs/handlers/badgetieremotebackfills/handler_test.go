package badgetieremotebackfills

import (
	"context"
	"encoding/json"
	"fmt"
	"testing"
	"time"

	"code.justin.tv/commerce/gogogadget/random"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	"code.justin.tv/commerce/payday/clients/pantheon"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/dynamo/badge_tier_emote_groupids"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/errors/http"
	badgetieremotes_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/badgetiers/products/emotes"
	mako_client_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/mako"
	pantheon_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/pantheon"
	badge_tier_emote_groupids_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo/badge_tier_emote_groupids"
	sns_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sns"
	redis_mock "code.justin.tv/commerce/payday/mocks/github.com/go-redis/redis"
	"code.justin.tv/commerce/payday/models"
	apimodels "code.justin.tv/commerce/payday/models/api"
	"code.justin.tv/commerce/payday/utils/pointers"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/go-redis/redis"
	"github.com/go-redis/redis_rate"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestHandler_HandleMessage_Bad_Message_Errors(t *testing.T) {
	Convey("Given a handler", t, func() {
		pantheonClientMock := new(pantheon_mock.IPantheonClientWrapper)
		makoClientMock := new(mako_client_mock.MakoClient)

		handler := Handler{
			PantheonClient: pantheonClientMock,
			MakoClient:     makoClientMock,
		}

		ctx := context.Background()

		Convey("and an incoming SQS message", func() {
			Convey("with an unparsable SNS message body", func() {
				sqsBodyJSON := ""

				sqsMsg := &sqs.Message{
					Body: pointers.StringP(sqsBodyJSON),
				}

				err := handler.HandleMessage(ctx, sqsMsg)
				So(err, ShouldNotBeNil)
			})

			Convey("with a parsable SNS message body", func() {
				Convey("but an unparsable emote backfill message", func() {
					sqsBodyJSON := "{ \"Message\": \"\"}"

					sqsMsg := &sqs.Message{
						Body: pointers.StringP(sqsBodyJSON),
					}

					err := handler.HandleMessage(ctx, sqsMsg)
					So(err, ShouldNotBeNil)
				})
			})
		})
	})
}

func TestHandler_HandleMessage_Broken_Service_Errors(t *testing.T) {
	Convey("Given a handler", t, func() {
		pantheonClientMock := new(pantheon_mock.IPantheonClientWrapper)
		makoClientMock := new(mako_client_mock.MakoClient)
		snsClientMock := new(sns_mock.ISNSClient)
		configMock := config.Configuration{
			EmoticonBackfillSNSTopic: "test_topic",
		}
		redisMock := new(redis_mock.Cmdable)
		throttlerMock := redis_rate.NewLimiter(redisMock)
		redisPipelinerMock := new(redis_mock.Pipeliner)
		logicianMock := new(badgetieremotes_mock.Logician)
		badgeTierEmotesGroupIDDAOMock := new(badge_tier_emote_groupids_mock.BadgeTierEmoteGroupIDDAO)

		handler := Handler{
			PantheonClient:      pantheonClientMock,
			MakoClient:          makoClientMock,
			SNSClient:           snsClientMock,
			Config:              &configMock,
			Throttle:            throttlerMock,
			Logician:            logicianMock,
			BadgeTierGroupIDDAO: badgeTierEmotesGroupIDDAOMock,
		}

		threshold := int64(1000)
		groupID := "1234567"
		channelID := "2345678"
		userToEntitle := "3456789"

		ctx := context.Background()

		sqsMsg := getWellFormedMessage(threshold, groupID, channelID, nil, false)

		leaderboardEntriesAboveThresholdResp := &pantheonrpc.GetLeaderboardEntriesAboveThresholdResp{
			Entries: []*pantheonrpc.LeaderboardEntry{
				{
					Rank:      1,
					Score:     1000,
					EntryKey:  userToEntitle,
					Moderated: false,
				},
			},
		}

		badgeTierEmotesGroupIDDAOMock.On("GetByChannelAndThreshold", mock.Anything, channelID, threshold).Return(&badge_tier_emote_groupids.BadgeTierEmoteGroupID{IsBackfilled: false}, nil)

		Convey("when groupID is not provided", func() {
			sqsMsgWithoutGroupID := getWellFormedMessage(threshold, "", channelID, nil, false)

			Convey("when the badge tier emotes logician returns an error", func() {
				logicianMock.On("GetAndStoreGroupIDForBadgeTier", mock.Anything, channelID, threshold).Return(nil, false, errors.New("dummy logician error"))

				err := handler.HandleMessage(ctx, sqsMsgWithoutGroupID)
				So(err, ShouldNotBeNil)
			})

			Convey("when the badge tier emotes logician returns a nil groupID mapping", func() {
				logicianMock.On("GetAndStoreGroupIDForBadgeTier", mock.Anything, channelID, threshold).Return(nil, false, nil)

				err := handler.HandleMessage(ctx, sqsMsgWithoutGroupID)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when pantheon returns a 500 service failure error", func() {
			pantheonClientMock.On("GetLeaderboardEntriesAboveThreshold", mock.Anything, pantheon.PantheonBitsUsageDomain, pantheonrpc.TimeUnit_ALLTIME, channelID, threshold, int64(DefaultPantheonPaginationLimit), int64(0)).Return(nil, http.New("test error", 500))

			err := handler.HandleMessage(ctx, sqsMsg)
			So(err, ShouldNotBeNil)
		})

		Convey("when pantheon returns a nil response", func() {
			pantheonClientMock.On("GetLeaderboardEntriesAboveThreshold", mock.Anything, pantheon.PantheonBitsUsageDomain, pantheonrpc.TimeUnit_ALLTIME, channelID, threshold, int64(DefaultPantheonPaginationLimit), int64(0)).Return(nil, nil)

			err := handler.HandleMessage(ctx, sqsMsg)
			So(err, ShouldNotBeNil)
		})

		Convey("when pantheon returns successfully", func() {
			pantheonClientMock.On("GetLeaderboardEntriesAboveThreshold", mock.Anything, pantheon.PantheonBitsUsageDomain, pantheonrpc.TimeUnit_ALLTIME, channelID, threshold, int64(DefaultPantheonPaginationLimit), int64(0)).Return(leaderboardEntriesAboveThresholdResp, nil)

			Convey("when sns returns error then return error", func() {
				snsClientMock.On("PostToTopic", mock.Anything, mock.Anything).Return(errors.New("dummy sns error"))

				err := handler.HandleMessage(ctx, sqsMsg)
				So(err, ShouldNotBeNil)
				So(err.Error(), ShouldContainSubstring, "dummy sns error")
			})

			Convey("when sns returns successfully", func() {
				snsClientMock.On("PostToTopic", mock.Anything, mock.Anything).Return(nil)

				Convey("when the badge tier emotes group ID dao returns an error", func() {
					badgeTierEmotesGroupIDDAOMock.On("UpdateBackfillState", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(errors.New("dummy dao error"))

					err := handler.HandleMessage(ctx, sqsMsg)
					So(err, ShouldNotBeNil)
					So(err.Error(), ShouldContainSubstring, "dummy dao error")
				})
			})
		})

		Convey("given users to entitle in message", func() {
			sqsMsgWithUsers := getWellFormedMessage(threshold, groupID, channelID, &[]string{userToEntitle}, false)

			Convey("when mako returns a 500 service failure error then return error", func() {
				mockThrottler(redisMock, redisPipelinerMock)
				makoClientMock.On("CreateEmoteGroupEntitlements", mock.Anything, []string{userToEntitle}, channelID, []string{groupID}).Return(http.New("test error", 500))

				err := handler.HandleMessage(ctx, sqsMsgWithUsers)

				So(err, ShouldNotBeNil)
			})
		})
	})
}

func TestHandler_HandleMessage_HappyPath(t *testing.T) {
	Convey("Given a handler", t, func() {
		pantheonClientMock := new(pantheon_mock.IPantheonClientWrapper)
		makoClientMock := new(mako_client_mock.MakoClient)
		snsClientMock := new(sns_mock.ISNSClient)
		configMock := config.Configuration{
			EmoticonBackfillSNSTopic: "test_topic",
		}
		redisMock := new(redis_mock.Cmdable)
		throttlerMock := redis_rate.NewLimiter(redisMock)
		redisPipelinerMock := new(redis_mock.Pipeliner)
		logicianMock := new(badgetieremotes_mock.Logician)
		badgeTierEmotesGroupIDDAOMock := new(badge_tier_emote_groupids_mock.BadgeTierEmoteGroupIDDAO)

		handler := Handler{
			PantheonClient:      pantheonClientMock,
			MakoClient:          makoClientMock,
			SNSClient:           snsClientMock,
			Config:              &configMock,
			Throttle:            throttlerMock,
			Logician:            logicianMock,
			BadgeTierGroupIDDAO: badgeTierEmotesGroupIDDAOMock,
		}

		threshold := int64(1000)
		groupID := "1234567"
		channelID := "2345678"

		ctx := context.Background()

		sqsMsg := getWellFormedMessage(threshold, groupID, channelID, nil, false)

		Convey("when the message contains no groupID and the returned groupID is not backfilled", func() {
			userIDs := []string{"123"}
			sqsMsgWithoutGroupID := getWellFormedMessage(threshold, "", channelID, &userIDs, false)

			logicianMock.On("GetAndStoreGroupIDForBadgeTier", mock.Anything, channelID, threshold).Return(&badge_tier_emote_groupids.BadgeTierEmoteGroupID{GroupID: groupID, IsBackfilled: false}, false, nil)
			mockThrottler(redisMock, redisPipelinerMock)
			makoClientMock.On("CreateEmoteGroupEntitlements", mock.Anything, userIDs, channelID, []string{groupID}).Return(nil)
			snsClientMock.On("PostToTopic", configMock.EmoticonBackfillSNSTopic, mock.Anything).Return(nil)

			err := handler.HandleMessage(ctx, sqsMsgWithoutGroupID)
			So(err, ShouldBeNil)
			snsClientMock.AssertNumberOfCalls(t, "PostToTopic", 1)
		})

		Convey("when the message contains no groupID and the returned groupID is backfilled", func() {
			userIDs := []string{"123"}
			sqsMsgWithoutGroupID := getWellFormedMessage(threshold, "", channelID, &userIDs, false)

			logicianMock.On("GetAndStoreGroupIDForBadgeTier", mock.Anything, channelID, threshold).Return(&badge_tier_emote_groupids.BadgeTierEmoteGroupID{GroupID: groupID, IsBackfilled: true}, false, nil)
			mockThrottler(redisMock, redisPipelinerMock)
			makoClientMock.On("CreateEmoteGroupEntitlements", mock.Anything, userIDs, channelID, []string{groupID}).Return(nil)

			err := handler.HandleMessage(ctx, sqsMsgWithoutGroupID)
			So(err, ShouldBeNil)
			snsClientMock.AssertNumberOfCalls(t, "PostToTopic", 0)
		})

		Convey("when the message contains userIDs", func() {
			userIDs := []string{"123", "456", "789"}
			sqsMsgWithUserIDs := getWellFormedMessage(threshold, groupID, channelID, &userIDs, false)

			mockThrottler(redisMock, redisPipelinerMock)
			makoClientMock.On("CreateEmoteGroupEntitlements", mock.Anything, userIDs, channelID, []string{groupID}).Return(nil)

			err := handler.HandleMessage(ctx, sqsMsgWithUserIDs)
			So(err, ShouldBeNil)
		})

		Convey("when the groupID is already backfilled, then the handler returns immediately", func() {
			badgeTierEmotesGroupIDDAOMock.On("GetByChannelAndThreshold", mock.Anything, channelID, threshold).Return(&badge_tier_emote_groupids.BadgeTierEmoteGroupID{IsBackfilled: true}, nil)

			err := handler.HandleMessage(ctx, sqsMsg)
			So(err, ShouldBeNil)
			pantheonClientMock.AssertNumberOfCalls(t, "GetLeaderboardEntriesAboveThreshold", 0)
			snsClientMock.AssertNumberOfCalls(t, "PostToTopic", 0)
		})

		Convey("when ignoreBackfilled is set to true when the groupID is already backfilled, it should always try to re-entitle.", func() {
			sqsMsgWithIgnoreBackfilled := getWellFormedMessage(threshold, groupID, channelID, nil, true)

			badgeTierEmotesGroupIDDAOMock.On("GetByChannelAndThreshold", mock.Anything, channelID, threshold).Return(
				&badge_tier_emote_groupids.BadgeTierEmoteGroupID{
					ChannelID:    channelID,
					Threshold:    threshold,
					GroupID:      groupID,
					CreatedAt:    time.Now(),
					IsBackfilled: true,
				}, nil)

			usersToEntitle := getUsersToEntitle(1)
			mockedLeaderboardEntries := getMockedLeaderboardEntries(usersToEntitle)
			leaderboardEntriesAboveThresholdResp := &pantheonrpc.GetLeaderboardEntriesAboveThresholdResp{Entries: mockedLeaderboardEntries}
			pantheonClientMock.On("GetLeaderboardEntriesAboveThreshold", mock.Anything, pantheon.PantheonBitsUsageDomain, pantheonrpc.TimeUnit_ALLTIME, channelID, threshold, int64(DefaultPantheonPaginationLimit), int64(0)).Return(leaderboardEntriesAboveThresholdResp, nil)
			badgeTierEmotesGroupIDDAOMock.On("UpdateBackfillState", mock.Anything, channelID, threshold, mock.Anything).Return(nil)
			snsClientMock.On("PostToTopic", configMock.EmoticonBackfillSNSTopic, mock.Anything).Return(nil)

			err := handler.HandleMessage(ctx, sqsMsgWithIgnoreBackfilled)
			So(err, ShouldBeNil)
			pantheonClientMock.AssertNumberOfCalls(t, "GetLeaderboardEntriesAboveThreshold", 1)
			badgeTierEmotesGroupIDDAOMock.AssertNumberOfCalls(t, "UpdateBackfillState", 1)
			snsClientMock.AssertNumberOfCalls(t, "PostToTopic", 1)
		})

		Convey("when pantheon returns zero users then sns is called with only the channel id", func() {
			leaderboardEntriesAboveThresholdResp := &pantheonrpc.GetLeaderboardEntriesAboveThresholdResp{}

			badgeTierEmotesGroupIDDAOMock.On("GetByChannelAndThreshold", mock.Anything, channelID, threshold).Return(&badge_tier_emote_groupids.BadgeTierEmoteGroupID{IsBackfilled: false}, nil)
			badgeTierEmotesGroupIDDAOMock.On("UpdateBackfillState", mock.Anything, channelID, threshold, mock.Anything).Return(nil)
			pantheonClientMock.On("GetLeaderboardEntriesAboveThreshold", mock.Anything, pantheon.PantheonBitsUsageDomain, pantheonrpc.TimeUnit_ALLTIME, channelID, threshold, int64(DefaultPantheonPaginationLimit), int64(0)).Return(leaderboardEntriesAboveThresholdResp, nil)
			snsClientMock.On("PostToTopic", configMock.EmoticonBackfillSNSTopic, mock.Anything).Return(nil)

			err := handler.HandleMessage(ctx, sqsMsg)
			So(err, ShouldBeNil)
			snsClientMock.AssertNumberOfCalls(t, "PostToTopic", 1)
			badgeTierEmotesGroupIDDAOMock.AssertNumberOfCalls(t, "UpdateBackfillState", 1)

			jsonMessage := snsClientMock.Calls[0].Arguments.Get(1).(string)
			snsMessage := getSNSMessageFromJson(jsonMessage)

			So(snsMessage.UserIDs, ShouldNotBeNil)
			So(len(*snsMessage.UserIDs), ShouldEqual, 1)
			So((*snsMessage.UserIDs)[0], ShouldEqual, channelID)
			So(snsMessage.Threshold, ShouldEqual, threshold)
			So(snsMessage.ChannelID, ShouldEqual, channelID)
			So(snsMessage.GroupID, ShouldEqual, groupID)
		})

		Convey("for a small number of users to entitle", func() {
			userToEntitle := "3456789"
			secondUserToEntitle := "4567890"
			moderatedUser := "5678901"
			leaderboardEntriesAboveThresholdResp := &pantheonrpc.GetLeaderboardEntriesAboveThresholdResp{
				Entries: []*pantheonrpc.LeaderboardEntry{
					{
						Rank:      1,
						Score:     1000,
						EntryKey:  userToEntitle,
						Moderated: false,
					},
					{
						Rank:      2,
						Score:     1000,
						EntryKey:  moderatedUser,
						Moderated: true,
					},
					{
						Rank:      3,
						Score:     1000,
						EntryKey:  secondUserToEntitle,
						Moderated: false,
					},
				},
			}
			badgeTierEmotesGroupIDDAOMock.On("GetByChannelAndThreshold", mock.Anything, channelID, threshold).Return(&badge_tier_emote_groupids.BadgeTierEmoteGroupID{IsBackfilled: false}, nil)
			badgeTierEmotesGroupIDDAOMock.On("UpdateBackfillState", mock.Anything, channelID, threshold, mock.Anything).Return(nil)
			pantheonClientMock.On("GetLeaderboardEntriesAboveThreshold", mock.Anything, pantheon.PantheonBitsUsageDomain, pantheonrpc.TimeUnit_ALLTIME, channelID, threshold, int64(DefaultPantheonPaginationLimit), int64(0)).Return(leaderboardEntriesAboveThresholdResp, nil)
			snsClientMock.On("PostToTopic", configMock.EmoticonBackfillSNSTopic, mock.Anything).Return(nil)

			err := handler.HandleMessage(ctx, sqsMsg)
			So(err, ShouldBeNil)
			snsClientMock.AssertNumberOfCalls(t, "PostToTopic", 1)
			badgeTierEmotesGroupIDDAOMock.AssertNumberOfCalls(t, "UpdateBackfillState", 1)

			jsonMessage := snsClientMock.Calls[0].Arguments.Get(1).(string)
			snsMessage := getSNSMessageFromJson(jsonMessage)

			So(snsMessage.UserIDs, ShouldNotBeNil)
			So(len(*snsMessage.UserIDs), ShouldEqual, 3)
			So(*snsMessage.UserIDs, ShouldContain, userToEntitle)
			So(*snsMessage.UserIDs, ShouldNotContain, moderatedUser)
			So(*snsMessage.UserIDs, ShouldContain, secondUserToEntitle)
			So(*snsMessage.UserIDs, ShouldContain, channelID)
			So(snsMessage.Threshold, ShouldEqual, threshold)
			So(snsMessage.ChannelID, ShouldEqual, channelID)
			So(snsMessage.GroupID, ShouldEqual, groupID)
		})

		Convey("for a number of users equal to the entitlements batch limit", func() {
			usersToEntitle := getUsersToEntitle(MaxEntitlementBatchSizeLimit - 1) // -1 because we entitle the channel owner
			mockedLeaderboardEntries := getMockedLeaderboardEntries(usersToEntitle)
			leaderboardEntriesAboveThresholdResp := &pantheonrpc.GetLeaderboardEntriesAboveThresholdResp{Entries: mockedLeaderboardEntries}

			badgeTierEmotesGroupIDDAOMock.On("GetByChannelAndThreshold", mock.Anything, channelID, threshold).Return(&badge_tier_emote_groupids.BadgeTierEmoteGroupID{IsBackfilled: false}, nil)
			badgeTierEmotesGroupIDDAOMock.On("UpdateBackfillState", mock.Anything, channelID, threshold, mock.Anything).Return(nil)
			pantheonClientMock.On("GetLeaderboardEntriesAboveThreshold", mock.Anything, pantheon.PantheonBitsUsageDomain, pantheonrpc.TimeUnit_ALLTIME, channelID, threshold, int64(DefaultPantheonPaginationLimit), int64(0)).Return(leaderboardEntriesAboveThresholdResp, nil)
			snsClientMock.On("PostToTopic", configMock.EmoticonBackfillSNSTopic, mock.Anything).Return(nil)

			err := handler.HandleMessage(ctx, sqsMsg)
			So(err, ShouldBeNil)
			snsClientMock.AssertNumberOfCalls(t, "PostToTopic", 1)
			badgeTierEmotesGroupIDDAOMock.AssertNumberOfCalls(t, "UpdateBackfillState", 1)

			jsonMessage := snsClientMock.Calls[0].Arguments.Get(1).(string)
			snsMessage := getSNSMessageFromJson(jsonMessage)

			So(snsMessage.UserIDs, ShouldNotBeNil)
			So(len(*snsMessage.UserIDs), ShouldEqual, 25)
		})

		Convey("for a number of users greater than the entitlements batch limit but fewer than the pantheon pagination limit", func() {
			usersToEntitle := getUsersToEntitle(MaxEntitlementBatchSizeLimit) // we include the channel owner, so this is 1 higher than the batch limit
			mockedLeaderboardEntries := getMockedLeaderboardEntries(usersToEntitle)
			leaderboardEntriesAboveThresholdResp := &pantheonrpc.GetLeaderboardEntriesAboveThresholdResp{Entries: mockedLeaderboardEntries}

			badgeTierEmotesGroupIDDAOMock.On("GetByChannelAndThreshold", mock.Anything, channelID, threshold).Return(&badge_tier_emote_groupids.BadgeTierEmoteGroupID{IsBackfilled: false}, nil)
			badgeTierEmotesGroupIDDAOMock.On("UpdateBackfillState", mock.Anything, channelID, threshold, mock.Anything).Return(nil)
			pantheonClientMock.On("GetLeaderboardEntriesAboveThreshold", mock.Anything, pantheon.PantheonBitsUsageDomain, pantheonrpc.TimeUnit_ALLTIME, channelID, threshold, int64(DefaultPantheonPaginationLimit), int64(0)).Return(leaderboardEntriesAboveThresholdResp, nil)
			snsClientMock.On("PostToTopic", configMock.EmoticonBackfillSNSTopic, mock.Anything).Return(nil)

			err := handler.HandleMessage(ctx, sqsMsg)

			So(err, ShouldBeNil)
			snsClientMock.AssertNumberOfCalls(t, "PostToTopic", 2)
			badgeTierEmotesGroupIDDAOMock.AssertNumberOfCalls(t, "UpdateBackfillState", 1)

			jsonMessage := snsClientMock.Calls[0].Arguments.Get(1).(string)
			snsMessage := getSNSMessageFromJson(jsonMessage)
			So(snsMessage.UserIDs, ShouldNotBeNil)
			So(len(*snsMessage.UserIDs), ShouldEqual, 25)

			jsonMessage2 := snsClientMock.Calls[1].Arguments.Get(1).(string)
			snsMessage2 := getSNSMessageFromJson(jsonMessage2)
			So(snsMessage2.UserIDs, ShouldNotBeNil)
			So(len(*snsMessage2.UserIDs), ShouldEqual, 1)
		})

		Convey("for a number of users equal to the pantheon pagination limit", func() {
			usersToEntitle := getUsersToEntitle(DefaultPantheonPaginationLimit - 1) // -1 because we always include the channel owner
			mockedLeaderboardEntries := getMockedLeaderboardEntries(usersToEntitle)
			leaderboardEntriesAboveThresholdResp := &pantheonrpc.GetLeaderboardEntriesAboveThresholdResp{Entries: mockedLeaderboardEntries}
			emptyLeaderboardEntriesAboveThresholdResp := &pantheonrpc.GetLeaderboardEntriesAboveThresholdResp{}

			badgeTierEmotesGroupIDDAOMock.On("GetByChannelAndThreshold", mock.Anything, channelID, threshold).Return(&badge_tier_emote_groupids.BadgeTierEmoteGroupID{IsBackfilled: false}, nil)
			badgeTierEmotesGroupIDDAOMock.On("UpdateBackfillState", mock.Anything, channelID, threshold, mock.Anything).Return(nil)
			pantheonClientMock.On("GetLeaderboardEntriesAboveThreshold", mock.Anything, pantheon.PantheonBitsUsageDomain, pantheonrpc.TimeUnit_ALLTIME, channelID, threshold, int64(DefaultPantheonPaginationLimit), int64(0)).Return(leaderboardEntriesAboveThresholdResp, nil)
			pantheonClientMock.On("GetLeaderboardEntriesAboveThreshold", mock.Anything, pantheon.PantheonBitsUsageDomain, pantheonrpc.TimeUnit_ALLTIME, channelID, threshold, int64(DefaultPantheonPaginationLimit), int64(DefaultPantheonPaginationLimit)).Return(emptyLeaderboardEntriesAboveThresholdResp, nil)
			snsClientMock.On("PostToTopic", configMock.EmoticonBackfillSNSTopic, mock.Anything).Return(nil)

			err := handler.HandleMessage(ctx, sqsMsg)

			So(err, ShouldBeNil)
			snsClientMock.AssertNumberOfCalls(t, "PostToTopic", 4)
			badgeTierEmotesGroupIDDAOMock.AssertNumberOfCalls(t, "UpdateBackfillState", 1)

			jsonMessage := snsClientMock.Calls[0].Arguments.Get(1).(string)
			snsMessage := getSNSMessageFromJson(jsonMessage)
			So(snsMessage.UserIDs, ShouldNotBeNil)
			So(len(*snsMessage.UserIDs), ShouldEqual, 25)

			jsonMessage2 := snsClientMock.Calls[1].Arguments.Get(1).(string)
			snsMessage2 := getSNSMessageFromJson(jsonMessage2)
			So(snsMessage2.UserIDs, ShouldNotBeNil)
			So(len(*snsMessage2.UserIDs), ShouldEqual, 25)

			jsonMessage3 := snsClientMock.Calls[2].Arguments.Get(1).(string)
			snsMessage3 := getSNSMessageFromJson(jsonMessage3)
			So(snsMessage3.UserIDs, ShouldNotBeNil)
			So(len(*snsMessage3.UserIDs), ShouldEqual, 25)

			jsonMessage4 := snsClientMock.Calls[3].Arguments.Get(1).(string)
			snsMessage4 := getSNSMessageFromJson(jsonMessage4)
			So(snsMessage4.UserIDs, ShouldNotBeNil)
			So(len(*snsMessage4.UserIDs), ShouldEqual, 25)
		})

		Convey("for a number of users greater than the pantheon pagination limit", func() {
			usersToEntitle := getUsersToEntitle(DefaultPantheonPaginationLimit) //  we include the channel owner, so this is 1 higher than the pagination limit
			mockedLeaderboardEntries := getMockedLeaderboardEntries(usersToEntitle)
			leaderboardEntriesAboveThresholdResp := &pantheonrpc.GetLeaderboardEntriesAboveThresholdResp{Entries: mockedLeaderboardEntries}
			emptyLeaderboardEntriesAboveThresholdResp := &pantheonrpc.GetLeaderboardEntriesAboveThresholdResp{}

			badgeTierEmotesGroupIDDAOMock.On("GetByChannelAndThreshold", mock.Anything, channelID, threshold).Return(&badge_tier_emote_groupids.BadgeTierEmoteGroupID{IsBackfilled: false}, nil)
			badgeTierEmotesGroupIDDAOMock.On("UpdateBackfillState", mock.Anything, channelID, threshold, mock.Anything).Return(nil)
			pantheonClientMock.On("GetLeaderboardEntriesAboveThreshold", mock.Anything, pantheon.PantheonBitsUsageDomain, pantheonrpc.TimeUnit_ALLTIME, channelID, threshold, int64(DefaultPantheonPaginationLimit), int64(0)).Return(leaderboardEntriesAboveThresholdResp, nil)
			pantheonClientMock.On("GetLeaderboardEntriesAboveThreshold", mock.Anything, pantheon.PantheonBitsUsageDomain, pantheonrpc.TimeUnit_ALLTIME, channelID, threshold, int64(DefaultPantheonPaginationLimit), int64(DefaultPantheonPaginationLimit)).Return(emptyLeaderboardEntriesAboveThresholdResp, nil)
			snsClientMock.On("PostToTopic", configMock.EmoticonBackfillSNSTopic, mock.Anything).Return(nil)

			err := handler.HandleMessage(ctx, sqsMsg)

			So(err, ShouldBeNil)
			snsClientMock.AssertNumberOfCalls(t, "PostToTopic", 5)
			badgeTierEmotesGroupIDDAOMock.AssertNumberOfCalls(t, "UpdateBackfillState", 1)

			jsonMessage := snsClientMock.Calls[0].Arguments.Get(1).(string)
			snsMessage := getSNSMessageFromJson(jsonMessage)
			So(snsMessage.UserIDs, ShouldNotBeNil)
			So(len(*snsMessage.UserIDs), ShouldEqual, 25)

			jsonMessage2 := snsClientMock.Calls[1].Arguments.Get(1).(string)
			snsMessage2 := getSNSMessageFromJson(jsonMessage2)
			So(snsMessage2.UserIDs, ShouldNotBeNil)
			So(len(*snsMessage2.UserIDs), ShouldEqual, 25)

			jsonMessage3 := snsClientMock.Calls[2].Arguments.Get(1).(string)
			snsMessage3 := getSNSMessageFromJson(jsonMessage3)
			So(snsMessage3.UserIDs, ShouldNotBeNil)
			So(len(*snsMessage3.UserIDs), ShouldEqual, 25)

			jsonMessage4 := snsClientMock.Calls[3].Arguments.Get(1).(string)
			snsMessage4 := getSNSMessageFromJson(jsonMessage4)
			So(snsMessage4.UserIDs, ShouldNotBeNil)
			So(len(*snsMessage4.UserIDs), ShouldEqual, 25)

			jsonMessage5 := snsClientMock.Calls[4].Arguments.Get(1).(string)
			snsMessage5 := getSNSMessageFromJson(jsonMessage5)
			So(snsMessage5.UserIDs, ShouldNotBeNil)
			So(len(*snsMessage5.UserIDs), ShouldEqual, 1)
		})
	})
}

func getWellFormedMessage(threshold int64, groupID string, channelID string, userIDs *[]string, ignoreBackfilled bool) *sqs.Message {
	emoticonBackfillDataBytes, err := json.Marshal(apimodels.EmoticonBackfillSNSMessage{
		Threshold:        threshold,
		GroupID:          groupID,
		ChannelID:        channelID,
		UserIDs:          userIDs,
		IgnoreBackfilled: ignoreBackfilled,
	})
	So(err, ShouldBeNil)

	snsMessageBytes, err := json.Marshal(models.SNSMessage{
		Message: string(emoticonBackfillDataBytes),
	})
	So(err, ShouldBeNil)

	sqsMsg := &sqs.Message{
		Body: pointers.StringP(string(snsMessageBytes)),
	}

	return sqsMsg
}

func getUsersToEntitle(numUsers int) []string {
	tuids := make([]string, 0, numUsers)
	for i := 0; i < numUsers; i++ {
		tuid := fmt.Sprintf("%d", random.Int(1, 1000000))
		tuids = append(tuids, tuid)
	}
	return tuids
}

func getMockedLeaderboardEntries(users []string) []*pantheonrpc.LeaderboardEntry {
	mockedLeaderboardEntries := make([]*pantheonrpc.LeaderboardEntry, 0, len(users))
	for i, user := range users {
		mockedLeaderboardEntries = append(mockedLeaderboardEntries, &pantheonrpc.LeaderboardEntry{
			Rank:      int64(i + 1),
			Score:     1000,
			EntryKey:  user,
			Moderated: false,
		})
	}
	return mockedLeaderboardEntries
}

func mockThrottler(redisMock *redis_mock.Cmdable, redisPipelinerMock *redis_mock.Pipeliner) {
	// If our throttling library had an interface, we could mock the "AllowRate" method here.
	// Given that it doesn't, we need to mock the underlying redis amd redis pipeliner,
	// and invoke the function the pipeliner is passed in the mock.
	functionMatcher := func(fn func(pipe redis.Pipeliner) error) bool {
		err := fn(redisPipelinerMock)
		So(err, ShouldBeNil)
		return true
	}
	redisPipelinerMock.On("IncrBy", mock.Anything, mock.Anything).Return(&redis.IntCmd{})
	redisPipelinerMock.On("Expire", mock.Anything, mock.Anything).Return(&redis.BoolCmd{})
	redisMock.On("Pipelined", mock.MatchedBy(functionMatcher)).Return(nil, nil)
}

func getSNSMessageFromJson(jsonMessage string) apimodels.EmoticonBackfillSNSMessage {
	snsMessage := apimodels.EmoticonBackfillSNSMessage{}
	err := json.Unmarshal([]byte(jsonMessage), &snsMessage)
	So(err, ShouldBeNil)
	return snsMessage
}
