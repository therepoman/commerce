package badgetieremotebackfills

import (
	"context"
	"encoding/json"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	badgetieremotes "code.justin.tv/commerce/payday/badgetiers/products/emotes"
	"code.justin.tv/commerce/payday/clients/mako"
	"code.justin.tv/commerce/payday/clients/pantheon"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/dynamo/badge_tier_emote_groupids"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/models"
	apimodels "code.justin.tv/commerce/payday/models/api"
	"code.justin.tv/commerce/payday/sns"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/cenkalti/backoff"
	"github.com/go-redis/redis_rate"
)

const (
	DefaultPantheonPaginationLimit       = 100
	MaxEntitlementBatchSizeLimit         = 25
	MakoGroupEntitlementsAllowedCallRate = 5 // TPS that we are comfortable calling mako's CreateGroupEntitlementsAPI
	RedisThrottleKey                     = "bits-emote-group-entitlement-mako-throttle"
)

type Handler struct {
	PantheonClient      pantheon.IPantheonClientWrapper                    `inject:""`
	MakoClient          mako.MakoClient                                    `inject:""`
	SNSClient           sns.ISNSClient                                     `inject:"usWestSNSClient"`
	Config              *config.Configuration                              `inject:""`
	Throttle            *redis_rate.Limiter                                `inject:""`
	Logician            badgetieremotes.Logician                           `inject:""`
	BadgeTierGroupIDDAO badge_tier_emote_groupids.BadgeTierEmoteGroupIDDAO `inject:""`
}

func (h *Handler) HandleMessage(ctx context.Context, message *sqs.Message) (outErr error) {
	var sns models.SNSMessage
	err := json.Unmarshal([]byte(*message.Body), &sns)
	if err != nil {
		msg := "Unable to convert to SNS message struct in badge tier emote backfills handler"
		log.WithError(err).WithField("sns message", *message).Error(msg)
		return errors.Notef(err, msg)
	}

	var emoticonBackfillSNSMessage apimodels.EmoticonBackfillSNSMessage
	err = json.Unmarshal([]byte(sns.Message), &emoticonBackfillSNSMessage)
	if err != nil {
		msg := "Unable to convert to emoticon backfill msg struct in badge tier emote backfills handler"
		log.WithError(err).WithField("sns message", sns.Message).Error(msg)
		return errors.Notef(err, msg)
	}

	channelID := emoticonBackfillSNSMessage.ChannelID
	threshold := emoticonBackfillSNSMessage.Threshold
	groupID := emoticonBackfillSNSMessage.GroupID
	ignoreBackfilled := emoticonBackfillSNSMessage.IgnoreBackfilled
	var groupIDMapping *badge_tier_emote_groupids.BadgeTierEmoteGroupID

	logFields := log.Fields{
		"channelID":        channelID,
		"threshold":        threshold,
		"groupID":          groupID,
		"ignoreBackfilled": ignoreBackfilled,
	}

	if groupID == "" {
		groupIDMapping, _, err = h.Logician.GetAndStoreGroupIDForBadgeTier(ctx, channelID, threshold)
		if err != nil {
			msg := "failed to get new group id for badge tier emote entitlement"
			log.WithError(err).WithFields(logFields).Error(msg)
			return errors.Notef(err, msg)
		} else if groupIDMapping == nil {
			msg := "encountered unexpected nil groupID mapping while getting groupID for emote entitlements backfill"
			log.WithFields(logFields).Error(msg)
			return errors.New(msg)
		}
		groupID = groupIDMapping.GroupID

		if !groupIDMapping.IsBackfilled && emoticonBackfillSNSMessage.UserIDs != nil {
			err := h.enqueueEmoticonBackfillBatch(threshold, groupID, channelID, nil)
			if err != nil {
				log.WithField("newGroupID", groupID).WithFields(logFields).Error("error enqueueing full backfill message for non-backfilled mako group")
			}
		}
	}

	var usersToEntitle []string
	// Emote backfill SNS Messages allow a client to enqueue either a backfill for all users above a threshold based on
	// the data we get from Pantheon or to entitle a certain user or users based on a provided list, which would not
	// call Pantheon at all.
	if emoticonBackfillSNSMessage.UserIDs == nil {
		log.WithFields(logFields).Info("Backfill SNS Message had no UserIDs, attempting to requeue the message")
		if groupIDMapping == nil {
			// The groupID mapping was not obtained previously because the groupID was passed in with the SQS message.
			// Call DynamoDB to get it in order to check whether we should backfill or not.
			groupIDMapping, err = h.BadgeTierGroupIDDAO.GetByChannelAndThreshold(ctx, channelID, threshold)
			if err != nil {
				// Log an error if this call fails, but do not error out. It is okay to proceed with a backfill as a
				// fallback.
				log.WithError(err).WithFields(logFields).Error("failed to get group id for badge tier emote entitlement to check backfill state")
			} else if groupIDMapping == nil {
				log.WithFields(logFields).Error("encountered unexpected nil groupID mapping while checking backfill state for emote entitlements backfill")
			}
		}

		if groupIDMapping != nil && groupIDMapping.IsBackfilled {
			log.WithFields(logFields).Infof("channel %v at threshold %v has already been backfilled", channelID, threshold)
			// This tier has already been backfilled. No need to do a backfill again.
			if !ignoreBackfilled {
				return nil
			} else {
				log.WithFields(logFields).Infof("channel %v at threshold %v has already been backfilled, but SNS Message passed in ignore override, ignoring.", channelID, threshold)
			}
		}

		usersToEntitle, err = h.getAllUsersToEntitleAboveThreshold(ctx, channelID, threshold)
		if err != nil {
			return err
		} else {
			log.WithFields(logFields).WithField("UserIDs", usersToEntitle).Infof("Fetched %v Users to entitle successfully for channel %v at threshold %v", len(usersToEntitle), channelID, threshold)
		}

		err = h.sendEmoteGroupEntitlementMessagesInBatches(ctx, usersToEntitle, channelID, groupID, threshold)
		if err != nil {
			return err
		} else {
			log.WithFields(logFields).Infof("Sent new Backfill SNS message with %v UserIDs", len(usersToEntitle))
		}

		err = h.BadgeTierGroupIDDAO.UpdateBackfillState(ctx, channelID, threshold, true)
		if err != nil {
			return err
		} else {
			log.WithFields(logFields).Infof("Updated is_backfill to true for channel %v at threshold %v", channelID, threshold)
		}

	} else {
		log.WithFields(logFields).WithField("UserIDs", *emoticonBackfillSNSMessage.UserIDs).Info("Fetched Users to entitle successfully from Backfill SNS Message.")
		return h.createEmoteGroupEntitlements(ctx, *emoticonBackfillSNSMessage.UserIDs, channelID, groupID)
	}

	return nil
}

func (h *Handler) getAllUsersToEntitleAboveThreshold(ctx context.Context, channelID string, threshold int64) ([]string, error) {
	offset := int64(0)
	entries, err := h.getLeaderboardEntriesAboveThreshold(ctx, channelID, threshold, offset)
	if err != nil {
		return []string{}, err
	}

	usersToEntitle := make([]string, 0, len(entries)+1)
	usersToEntitle = append(usersToEntitle, channelID) //always entitle the channel owner to their emotes

	for {
		// Only extract users from entries which are not moderated
		for _, entry := range entries {
			if entry == nil || entry.Moderated {
				continue
			}
			usersToEntitle = append(usersToEntitle, entry.EntryKey)
		}

		// Increment the offset to the highest rank (lowest score) in the returned leaderboard results
		// If there are no entries returned, then do not need to make additional calls
		if len(entries) > 0 {
			offset = entries[len(entries)-1].Rank
		}

		// Only continue to get new entries from pantheon if we have a full page of entries
		// This will call unnecessarily one time in the case where we get back exactly the pagination limit number of
		// results.
		if len(entries) == DefaultPantheonPaginationLimit {
			entries, err = h.getLeaderboardEntriesAboveThreshold(ctx, channelID, threshold, offset)
			if err != nil {
				return []string{}, err
			}
		} else {
			break
		}
	}

	return usersToEntitle, nil
}

func (h *Handler) getLeaderboardEntriesAboveThreshold(ctx context.Context, channelID string, threshold int64, offset int64) ([]*pantheonrpc.LeaderboardEntry, error) {
	resp, err := h.PantheonClient.GetLeaderboardEntriesAboveThreshold(ctx, pantheon.PantheonBitsUsageDomain, pantheonrpc.TimeUnit_ALLTIME, channelID, threshold, DefaultPantheonPaginationLimit, offset)
	if err != nil {
		log.WithError(err).WithFields(log.Fields{
			"channelID": channelID,
			"threshold": threshold,
			"offset":    offset,
		}).Error("error getting leaderboard entries above threshold")
		return []*pantheonrpc.LeaderboardEntry{}, err
	}

	if resp == nil {
		msg := "got nil response from get leaderboard entries above threshold"
		log.WithFields(log.Fields{
			"channelID": channelID,
			"threshold": threshold,
			"offset":    offset,
		}).Error(msg)
		return []*pantheonrpc.LeaderboardEntry{}, errors.New(msg)
	}

	return resp.Entries, nil
}

func (h *Handler) sendEmoteGroupEntitlementMessagesInBatches(ctx context.Context, usersToEntitle []string, channelID string, groupID string, threshold int64) error {
	if len(usersToEntitle) == 0 {
		// no users to entitle
		return nil
	}

	var batchesForSns [][]string
	batchesForSns = append(batchesForSns, []string{})
	batchCounter := 0
	for _, user := range usersToEntitle {
		if len(batchesForSns[batchCounter]) < MaxEntitlementBatchSizeLimit {
			batchesForSns[batchCounter] = append(batchesForSns[batchCounter], user)
		} else {
			batchCounter++
			batchesForSns = append(batchesForSns, []string{user})
		}
	}

	for _, batch := range batchesForSns {
		thisBatch := batch
		err := h.enqueueEmoticonBackfillBatch(threshold, groupID, channelID, &thisBatch)
		if err != nil {
			return err
		}
	}

	return nil
}

func (h *Handler) createEmoteGroupEntitlements(ctx context.Context, usersToEntitle []string, channelID string, groupID string) error {
	if len(usersToEntitle) == 0 {
		// no users to entitle
		return nil
	}

	fields := log.Fields{
		"userIDs":   usersToEntitle,
		"channelID": channelID,
		"groupID":   groupID,
	}

	// Retry getting the mako rate limiter key from redis with exponential backoff
	f := func() error {
		_, _, allowed := h.Throttle.Allow(RedisThrottleKey, int64(MakoGroupEntitlementsAllowedCallRate), time.Second)
		if !allowed {
			return errors.New("unable to obtain redis throttle lock")
		}
		return nil
	}

	b := backoff.WithMaxTries(backoff.WithContext(newBackoff(), ctx), 3)
	err := backoff.Retry(f, b)

	if err == nil {
		err := h.MakoClient.CreateEmoteGroupEntitlements(ctx, usersToEntitle, channelID, []string{groupID})
		if err != nil {
			log.WithError(err).WithFields(fields).Error("error creating group entitlements in mako")
			return err
		} else {
			log.WithFields(fields).Info("succesfully created group entitlements in mako")
		}
	} else {
		// Return error to retry via SQS
		return errors.New("Unable to obtain redis throttle lock to call mako create group entitlements")
	}

	return nil
}

func (h *Handler) enqueueEmoticonBackfillBatch(threshold int64, groupID string, channelID string, userIDs *[]string) error {
	if userIDs != nil && len(*userIDs) == 0 {
		// no users to backfill
		return nil
	}

	fields := log.Fields{
		"userIDs":   userIDs,
		"channelID": channelID,
		"threshold": threshold,
		"groupID":   groupID,
	}

	message := apimodels.EmoticonBackfillSNSMessage{
		Threshold: threshold,
		GroupID:   groupID,
		ChannelID: channelID,
		UserIDs:   userIDs,
	}

	messageJSON, err := json.Marshal(message)
	if err != nil {
		log.WithError(err).WithFields(fields).Error("Error marshaling emoticon backfill batch message json")
		return err
	}

	// Posting message back to the topic that feeds this queue, but with a batch of users to entitle
	err = h.SNSClient.PostToTopic(h.Config.EmoticonBackfillSNSTopic, string(messageJSON))
	if err != nil {
		log.WithError(err).WithFields(fields).Error("Error posting emoticon backfill batch to SNS")
		return err
	}

	return nil
}

func newBackoff() backoff.BackOff {
	exponential := backoff.NewExponentialBackOff()
	exponential.InitialInterval = 2 * time.Second
	exponential.MaxElapsedTime = 32 * time.Second
	exponential.Multiplier = 2
	return exponential
}
