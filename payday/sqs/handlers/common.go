package handlers

import "code.justin.tv/commerce/payday/utils/math"

func BitsUsedWithSponsor(bitsSent int, sponsoredAmounts map[string]int) int {
	bitsUsed := math.IntAbs(bitsSent)
	if len(sponsoredAmounts) > 0 {
		for _, amount := range sponsoredAmounts {
			bitsUsed = bitsUsed + amount
		}
	}
	return bitsUsed
}
