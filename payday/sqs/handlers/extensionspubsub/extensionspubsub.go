package extensionspubsub

import (
	"context"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/extension/pubsub"
	"code.justin.tv/commerce/payday/models"
)

type ExtensionsPubsubHandler struct {
	ExtensionPubsubManager pubsub.Manager `inject:""`
}

func (h *ExtensionsPubsubHandler) HandleGiveBitsToBroadcaster(ctx context.Context, msg *models.GiveBitsToBroadcasterMessage) error {
	// Extensions pubsub doesn't consume this event
	return nil
}

func (h *ExtensionsPubsubHandler) HandleAddBitsToCustomerAccount(ctx context.Context, msg *models.AddBitsToCustomerAccountMessage) error {
	// Extensions pubsub doesn't consume this event
	return nil
}

func (h *ExtensionsPubsubHandler) HandleRemoveBitsFromCustomerAccount(ctx context.Context, msg *models.RemoveBitsFromCustomerAccountMessage) error {
	// Extensions pubsub doesn't consume this event
	return nil
}

func (h *ExtensionsPubsubHandler) HandleUseBitsOnPoll(ctx context.Context, msg *models.UseBitsOnPollMessage) error {
	// Extensions pubsub doesn't consume this event
	return nil
}

func (h *ExtensionsPubsubHandler) HandleUseBitsOnExtension(ctx context.Context, msg *models.UseBitsOnExtensionMessage) error {
	if msg == nil {
		errMsg := "received a nil message in bits extensions pubsub sqs handler"
		log.Error(errMsg)
		return errors.New(errMsg)
	}
	// We don't want to process these messages because they aren't true extension usage.
	if msg.TransactionType == models.UseBitsOnExtensionChallengeTransactionType {
		return nil
	}
	if err := h.ExtensionPubsubManager.ProcessUseBitsOnExtensionMsg(ctx, msg); err != nil {
		errMsg := "error handling message in bits extensions pubsub sqs handler"
		log.WithFields(log.Fields{
			"TransactionID": msg.TransactionId,
			"UserID":        msg.RequestingTwitchUserId,
			"ChannelID":     msg.TargetTwitchUserId,
			"ExtensionID":   msg.ExtensionID,
		}).WithError(err).Error(errMsg)
		return errors.Notef(err, errMsg)
	}
	return nil
}

func (h *ExtensionsPubsubHandler) HandleCreateHold(ctx context.Context, msg *models.CreateHoldMessage) error {
	// we don't care about holds
	return nil
}

func (h *ExtensionsPubsubHandler) HandleFinalizeHold(ctx context.Context, msg *models.FinalizeHoldMessage) error {
	// we don't care about holds
	return nil
}

func (h *ExtensionsPubsubHandler) HandleReleaseHold(ctx context.Context, msg *models.ReleaseHoldMessage) error {
	// we don't care about holds
	return nil
}
