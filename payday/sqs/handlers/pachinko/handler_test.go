package pachinko

import (
	"context"
	"encoding/json"
	"testing"

	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/extension/transaction"
	dynamo_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo"
	transaction_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/extension/transaction"
	metrics_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/metrics"
	"code.justin.tv/commerce/payday/models"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/sqs"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestHandler_HandleMessage(t *testing.T) {
	ctx := context.Background()

	Convey("Testing Pachinko Message Handler", t, func() {
		handler := NewHandler()

		transactionMock := new(dynamo_mock.ITransactionsDao)
		handler.TransactionDao = transactionMock

		inflightTransactionMock := new(dynamo_mock.InFlightTransactionDao)
		inflightTransactionMock.On("RemoveTransaction", ctx, mock.Anything).Return()
		handler.InFlightTransactionDao = inflightTransactionMock

		extTransactionManagerMock := new(transaction_mock.Manager)
		handler.ExtensionTransactionManager = extTransactionManagerMock

		statterMock := new(metrics_mock.Statter)
		statterMock.On("Inc", "Pachinko_Transaction_Fixed", int64(1)).Return()
		handler.Statter = statterMock

		Convey("payload is nil", func() {
			So(handler.HandleMessage(ctx, nil), ShouldBeError)
		})

		Convey("payload body is empty string", func() {
			So(handler.HandleMessage(ctx, &sqs.Message{Body: aws.String("")}), ShouldBeError)
		})

		Convey("payload body is bad", func() {
			So(handler.HandleMessage(ctx, &sqs.Message{Body: aws.String("{")}), ShouldBeError)
		})

		Convey("payload body is empty JSON", func() {
			snsMessage := "{}"
			So(handler.HandleMessage(ctx, &sqs.Message{Body: aws.String(snsMessage)}), ShouldBeError)
		})

		Convey("payload body is populated", func() {
			Convey("message single transaction id", func() {
				transactionId := "1234"

				message := makeSQSMessage(models.PachinkoSNSUpdate{
					TransactionIDs: []string{transactionId},
				})

				Convey("when transaction dao errors", func() {
					transactionMock.On("Get", transactionId).Return(nil, errors.New("anime bear overload"))
					So(handler.HandleMessage(ctx, message), ShouldBeError)
				})

				Convey("when transaction returns a transaction", func() {
					transactionMock.On("Get", transactionId).Return(&dynamo.Transaction{}, nil)
					So(handler.HandleMessage(ctx, message), ShouldBeNil) //transaction exists, it bails early.
				})

				Convey("when transaction doesn't return a transaction", func() {
					transactionMock.On("Get", transactionId).Return(nil, nil)

					Convey("when extension transaction manage errors", func() {
						extTransactionManagerMock.On("GetTransactionsByID", []string{transactionId}).Return(nil, errors.New("anime bear limits failed"))
						So(handler.HandleMessage(ctx, message), ShouldBeError)
					})

					Convey("when extension transaction returns a transaction", func() {
						results := []*transaction.Transaction{
							{},
						}
						extTransactionManagerMock.On("GetTransactionsByID", []string{transactionId}).Return(results, nil)
						So(handler.HandleMessage(ctx, message), ShouldBeNil) //transaction exists, it bails early.
					})

					Convey("when extension transaction returns an empty list of transactions", func() {
						// this is the new case that we didn't handle correctly in the original implementation.

						var results []*transaction.Transaction
						extTransactionManagerMock.On("GetTransactionsByID", []string{transactionId}).Return(results, nil)

						Convey("when inflight transaction doesn't returns an error", func() {
							inflightTransactionMock.On("GetTransaction", ctx, transactionId).Return(nil, errors.New("no bears in flight"))
							So(handler.HandleMessage(ctx, message), ShouldBeError)
						})

						Convey("when inflight transaction doesn't return a transaction", func() {
							inflightTransactionMock.On("GetTransaction", ctx, transactionId).Return(nil, nil)
							So(handler.HandleMessage(ctx, message), ShouldBeError)
						})

						Convey("when inflight transaction returns a transaction", func() {
							inflightTransactionMock.On("GetTransaction", ctx, transactionId).Return(&dynamo.Transaction{}, nil)

							Convey("when create new errors", func() {
								transactionMock.On("CreateNew", mock.Anything).Return(errors.New("anime bear malfunction"))
								So(handler.HandleMessage(ctx, message), ShouldBeError)
							})

							Convey("when create new doesn't error", func() {
								transactionMock.On("CreateNew", mock.Anything).Return(nil)
								So(handler.HandleMessage(ctx, message), ShouldBeNil)

								transactionMock.AssertNumberOfCalls(t, "CreateNew", 1)
								statterMock.AssertNumberOfCalls(t, "Inc", 1)
							})
						})
					})

					Convey("when extension transaction doesn't returns nil", func() {
						extTransactionManagerMock.On("GetTransactionsByID", []string{transactionId}).Return(nil, nil)

						Convey("when inflight transaction doesn't returns an error", func() {
							inflightTransactionMock.On("GetTransaction", ctx, transactionId).Return(nil, errors.New("no bears in flight"))
							So(handler.HandleMessage(ctx, message), ShouldBeError)
						})

						Convey("when inflight transaction doesn't return a transaction", func() {
							inflightTransactionMock.On("GetTransaction", ctx, transactionId).Return(nil, nil)
							So(handler.HandleMessage(ctx, message), ShouldBeError)
						})

						Convey("when inflight transaction returns a transaction", func() {
							inflightTransactionMock.On("GetTransaction", ctx, transactionId).Return(&dynamo.Transaction{}, nil)

							Convey("when create new errors", func() {
								transactionMock.On("CreateNew", mock.Anything).Return(errors.New("anime bear malfunction"))
								So(handler.HandleMessage(ctx, message), ShouldBeError)
							})

							Convey("when create new doesn't error", func() {
								transactionMock.On("CreateNew", mock.Anything).Return(nil)
								So(handler.HandleMessage(ctx, message), ShouldBeNil)

								transactionMock.AssertNumberOfCalls(t, "CreateNew", 1)
								statterMock.AssertNumberOfCalls(t, "Inc", 1)
							})
						})
					})
				})
			})
		})
	})
}

func makeSQSMessage(update models.PachinkoSNSUpdate) *sqs.Message {
	message, _ := json.Marshal(update)

	snsMessage := models.SNSMessage{
		Message: string(message),
	}

	snsMessagePayload, _ := json.Marshal(&snsMessage)

	return &sqs.Message{Body: aws.String(string(snsMessagePayload))}
}
