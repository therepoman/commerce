package pachinko

import (
	"context"
	"encoding/json"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/errors"
	extension_tx "code.justin.tv/commerce/payday/extension/transaction"
	"code.justin.tv/commerce/payday/metrics"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/utils/pointers"
	"github.com/aws/aws-sdk-go/service/sqs"
)

type Handler struct {
	TransactionDao              dynamo.ITransactionsDao       `inject:""`
	InFlightTransactionDao      dynamo.InFlightTransactionDao `inject:""`
	ExtensionTransactionManager extension_tx.Manager          `inject:""`
	Statter                     metrics.Statter               `inject:""`
}

func NewHandler() *Handler {
	return &Handler{}
}

func (h *Handler) HandleMessage(ctx context.Context, sqsMessage *sqs.Message) error {
	if sqsMessage == nil {
		msg := "received nil message on pachinko queue"
		log.Error(msg)
		return errors.New(msg)
	}

	var snsMsg models.SNSMessage
	err := json.Unmarshal([]byte(*sqsMessage.Body), &snsMsg)
	if err != nil {
		msg := "unable to unmarshal SNS message struct in Pachinko event SQS handler"
		log.WithError(err).Error(msg)
		return errors.Notef(err, msg)
	}

	var pachinkoMsg models.PachinkoSNSUpdate
	err = json.Unmarshal([]byte(snsMsg.Message), &pachinkoMsg)
	if err != nil {
		msg := "unable to unmarshal Pachinko message struct in Pachinko Transaction event SQS handler"
		log.WithError(err).Error(msg)
		return errors.Notef(err, msg)
	}

	for _, txid := range pachinkoMsg.TransactionIDs {
		// let's check if we even need to do this first with regular transactions
		existingTx, err := h.TransactionDao.Get(txid)
		if err != nil {
			log.WithError(err).WithField("transaction-id", txid).Error("Failed to get existing dynamo record, the library should return nil if not found")
			return err
		}
		if existingTx != nil {
			continue
		}

		// and now let's see if it was an extension transaction
		extensionExistingTx, err := h.ExtensionTransactionManager.GetTransactionsByID([]string{txid})
		if err != nil {
			log.WithError(err).WithField("transaction-id", txid).Error("Failed to get existing extension dynamo record, the library should return nil if not found")
			return err
		}
		if len(extensionExistingTx) > 0 {
			continue
		}

		transaction, err := h.InFlightTransactionDao.GetTransaction(ctx, txid)
		if err != nil {
			log.WithError(err).WithField("transaction-id", txid).Error("Failed to get inflight transaction entry.")
			return err
		} else if transaction == nil {
			log.WithField("transaction-id", txid).Error("Couldn't find inflight transaction entry.")
			return errors.New("Couldn't find in flight entry.")
		}

		// Nuke this just in case, so we don't copy it over.
		transaction.ExpiresAt = nil

		err = h.TransactionDao.CreateNew(transaction)
		if err != nil {
			log.WithError(err).WithField("transaction-id", txid).Error("Failed to write finalized transaction entry.")
			return err
		}

		h.Statter.Inc("Pachinko_Transaction_Fixed", 1)
		log.WithFields(log.Fields{
			"transaction-id":   txid,
			"transaction-type": pointers.StringOrDefault(transaction.TransactionType, "UNKNOWN"),
		}).Warn("Had to recreate transaction from inflight version.")

		h.InFlightTransactionDao.RemoveTransaction(ctx, txid)
	}
	return nil
}
