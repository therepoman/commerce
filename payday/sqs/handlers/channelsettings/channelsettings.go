package channelsettings

import (
	"context"
	"encoding/json"
	"fmt"

	pubclient "code.justin.tv/chat/pubsub-go-pubclient/client"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	"code.justin.tv/commerce/payday/cache/channel"
	"code.justin.tv/commerce/payday/clients/pantheon"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/leaderboard"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/models/api"
	"code.justin.tv/commerce/payday/utils/pointers"
	"github.com/aws/aws-sdk-go/service/sqs"
)

type ChannelSettingsHandler struct {
	PubClient       pubclient.PubClient             `inject:""`
	ChannelManager  channel.ChannelManager          `inject:""`
	BadgeReconciler leaderboard.BadgeReconciler     `inject:""`
	PantheonClient  pantheon.IPantheonClientWrapper `inject:""`
}

func (h *ChannelSettingsHandler) HandleMessage(ctx context.Context, message *sqs.Message) (outErr error) {
	var snsMessage models.SNSMessage
	err := json.Unmarshal([]byte(*message.Body), &snsMessage)
	if err != nil {
		msg := "Unable to convert to SNS message struct"
		log.WithError(err).Error(msg)
		return errors.Notef(err, msg)
	}

	var channelSettingsMessage models.ChannelSettingsMessage
	err = json.Unmarshal([]byte(snsMessage.Message), &channelSettingsMessage)
	if err != nil {
		msg := "Unable to convert to Update Settings Request"
		log.WithError(err).Error(msg)
		return errors.Notef(err, msg)
	}

	lb, err := h.fetchUpdatedLeaderboard(ctx, channelSettingsMessage)
	if err != nil {
		return err
	}

	err = h.updateChatBadges(ctx, channelSettingsMessage, lb)
	if err != nil {
		return err
	}

	err = h.sendPubSubMessage(ctx, channelSettingsMessage, lb)
	return err
}

func (h *ChannelSettingsHandler) fetchUpdatedLeaderboard(ctx context.Context, channelSettingsMessage models.ChannelSettingsMessage) ([]*pantheonrpc.LeaderboardEntry, error) {
	leaderboardEnabled := pointers.BoolOrDefault(channelSettingsMessage.LeaderboardEnabled, api.DefaultLeaderboardEnabled)
	isCheerLeaderboardEnabled := pointers.BoolOrDefault(channelSettingsMessage.IsCheerLeaderboardEnabled, leaderboardEnabled)
	leaderboardTimePeriod := pointers.StringOrDefault(channelSettingsMessage.LeaderboardTimePeriod, models.DefaultLeaderboardTimePeriod)

	timeUnitValue, ok := pantheonrpc.TimeUnit_value[leaderboardTimePeriod]
	if !ok {
		return nil, errors.Newf("Received invalid leaderboard time period from Channel Settings Message")
	}

	var lb []*pantheonrpc.LeaderboardEntry
	if isCheerLeaderboardEnabled {
		getLeaderboardResponse, err := h.PantheonClient.GetLeaderboard(ctx, pantheon.PantheonBitsUsageDomain, pantheonrpc.TimeUnit(timeUnitValue), channelSettingsMessage.ChannelId, channelSettingsMessage.ChannelId, 10, 0, false)
		if err != nil {
			return nil, err
		}

		lb = getLeaderboardResponse.GetTop()
	}

	return lb, nil
}

func (h *ChannelSettingsHandler) updateChatBadges(ctx context.Context, channelSettingsMessage models.ChannelSettingsMessage, lb []*pantheonrpc.LeaderboardEntry) (outErr error) {
	leaderboardTimePeriod := pointers.StringOrDefault(channelSettingsMessage.LeaderboardTimePeriod, models.DefaultLeaderboardTimePeriod)

	timeUnitValue, ok := pantheonrpc.TimeUnit_value[leaderboardTimePeriod]
	if !ok {
		return errors.Newf("Received invalid leaderboard time period from Channel Settings Message")
	}

	err := h.BadgeReconciler.ReconcileBadges(ctx, channelSettingsMessage.ChannelId, pantheonrpc.TimeUnit(timeUnitValue), toBadgeHolders(lb), true)
	if err != nil {
		return errors.Newf("Failed to reconcile badges")
	}

	return nil
}

func (h *ChannelSettingsHandler) sendPubSubMessage(ctx context.Context, channelSettingsMessage models.ChannelSettingsMessage, lb []*pantheonrpc.LeaderboardEntry) (outErr error) {
	channelId := channelSettingsMessage.ChannelId

	pubsubContent := models.ChannelSettingsUpdateMessage{}

	if channelSettingsMessage.LeaderboardEnabled != nil {
		pubsubContent.LeaderboardEnabled = channelSettingsMessage.LeaderboardEnabled
	}

	if channelSettingsMessage.IsCheerLeaderboardEnabled != nil {
		pubsubContent.IsCheerLeaderboardEnabled = channelSettingsMessage.IsCheerLeaderboardEnabled
	}

	if channelSettingsMessage.IsSubGiftLeaderboardEnabled != nil {
		pubsubContent.IsSubGiftLeaderboardEnabled = channelSettingsMessage.IsSubGiftLeaderboardEnabled
	}

	if channelSettingsMessage.DefaultLeaderboard != nil {
		pubsubContent.DefaultLeaderboard = channelSettingsMessage.DefaultLeaderboard
	}

	if channelSettingsMessage.LeaderboardTimePeriod != nil {
		pubsubContent.LeaderboardTimePeriod = channelSettingsMessage.LeaderboardTimePeriod
	}

	if lb != nil {
		pubsubContent.Leaderboard = &lb
	}

	if channelSettingsMessage.MinimumBits != nil {
		pubsubContent.MinimumBits = channelSettingsMessage.MinimumBits
	}

	if channelSettingsMessage.MinimumBitsEmote != nil {
		pubsubContent.MinimumBitsEmote = channelSettingsMessage.MinimumBitsEmote
	}

	if channelSettingsMessage.CheerBombEventOptOut != nil {
		pubsubContent.CheerBombEventOptOut = channelSettingsMessage.CheerBombEventOptOut
	}

	if channelSettingsMessage.OptedOutOfVoldemort != nil {
		pubsubContent.OptedOutOfVoldemort = channelSettingsMessage.OptedOutOfVoldemort
	}

	pubsubMsg := models.ChannelSettingsUpdatePubsubMessage{
		Type:    models.ChannelSettingsUpdateMessageType,
		Updates: pubsubContent,
	}

	pubsubMsgJson, err := json.Marshal(pubsubMsg)
	if err != nil {
		msg := "Failed to marshal channel settings update pubsub message"
		log.WithError(err).Error(msg)
		return errors.Notef(err, msg)
	}

	topics := []string{fmt.Sprintf("%s.%s", models.BitsEventPublicPubsubTopic, channelId)}
	err = h.PubClient.Publish(ctx, topics, string(pubsubMsgJson), nil)
	if err != nil {
		msg := "Failed to publish channel settings update to pubsub"
		log.WithError(err).Error(msg)
		return errors.Notef(err, msg)
	}

	return nil
}

func toBadgeHolders(entries []*pantheonrpc.LeaderboardEntry) leaderboard.BadgeHolders {
	newBadgeHolders := make(leaderboard.BadgeHolders, 0)

	for i, entry := range entries {
		if i > 2 {
			break
		}

		newBadgeHolders = append(newBadgeHolders, leaderboard.LeaderboardBadge{
			UserID:        entry.EntryKey,
			BadgePosition: entry.Rank,
		})
	}

	return newBadgeHolders
}
