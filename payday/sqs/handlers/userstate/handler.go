package userstate

import (
	"context"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/models/api"
	"code.justin.tv/commerce/payday/userstate"
	"code.justin.tv/commerce/payday/userstate/firstpurchase"
	timeutils "code.justin.tv/commerce/payday/utils/time"
)

type Handler struct {
	UserStateGetter        userstate.Getter        `inject:""`
	UserStateSetter        userstate.Setter        `inject:""`
	FirstPurchasePubSubber firstpurchase.PubSubber `inject:""`
}

func (h *Handler) HandleGiveBitsToBroadcaster(ctx context.Context, msg *models.GiveBitsToBroadcasterMessage) error {
	if msg == nil {
		return nil
	}

	err := h.UserStateSetter.Set(ctx, msg.RequestingTwitchUserId, api.UserStateCheered)
	if err != nil {
		log.WithError(err).WithField("userID", msg.RequestingTwitchUserId).Error("could not set user to cheered state")
	}

	return nil
}

func (h *Handler) HandleAddBitsToCustomerAccount(ctx context.Context, msg *models.AddBitsToCustomerAccountMessage) error {
	if msg == nil {
		return nil
	}

	currentUserState, err := h.UserStateGetter.Get(msg.TargetTwitchUserId)
	if err != nil {
		log.WithFields(log.Fields{
			"user_id":        msg.TargetTwitchUserId,
			"transaction_id": msg.TransactionId,
		}).WithError(err).Error("failed to get value from user state cache")
		return err
	}

	if currentUserState == api.UserStateNew {
		err = h.UserStateSetter.Set(ctx, msg.TargetTwitchUserId, api.UserStateAcquired)
		if err != nil {
			log.WithFields(log.Fields{
				"user_id":        msg.TargetTwitchUserId,
				"transaction_id": msg.TransactionId,
			}).WithError(err).Error("failed to set value in user state cache")
			return err
		}

		now := time.Now()
		if timeutils.IsTimeInRange(msg.TimeOfEvent, now.Add(-5*time.Second), now.Add(5*time.Second)) {
			err = h.FirstPurchasePubSubber.Notify(ctx, msg.TargetTwitchUserId, int64(msg.TotalBits))
			if err != nil {
				log.WithFields(log.Fields{
					"user_id":        msg.TargetTwitchUserId,
					"transaction_id": msg.TransactionId,
				}).WithError(err).Error("failed to notify user of first purchase")
				return err
			}
		}
	}

	return nil
}

func (h *Handler) HandleRemoveBitsFromCustomerAccount(ctx context.Context, msg *models.RemoveBitsFromCustomerAccountMessage) error {
	return nil
}

func (h *Handler) HandleUseBitsOnExtension(ctx context.Context, msg *models.UseBitsOnExtensionMessage) error {
	return nil
}

func (h *Handler) HandleUseBitsOnPoll(ctx context.Context, msg *models.UseBitsOnPollMessage) error {
	return nil
}

func (h *Handler) HandleCreateHold(ctx context.Context, msg *models.CreateHoldMessage) error {
	return nil
}

func (h *Handler) HandleFinalizeHold(ctx context.Context, msg *models.FinalizeHoldMessage) error {
	return nil
}

func (h *Handler) HandleReleaseHold(ctx context.Context, msg *models.ReleaseHoldMessage) error {
	return nil
}
