package userstate

import (
	"context"
	"testing"
	"time"

	firstpurchase_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/firstpurchase"
	userstate_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/userstate"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/models/api"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestUserStateHandler_HandleGiveBitsToBroadcaster(t *testing.T) {
	Convey("Given a prometheus handler", t, func() {
		userStateSetter := new(userstate_mock.Setter)

		handler := &Handler{
			UserStateSetter: userStateSetter,
		}

		Convey("when it receives a nil message", func() {
			var msg *models.GiveBitsToBroadcasterMessage

			Convey("then we exit early and return nil", func() {
				err := handler.HandleGiveBitsToBroadcaster(context.Background(), msg)
				So(err, ShouldBeNil)
				userStateSetter.AssertNotCalled(t, "Set", mock.Anything, mock.Anything, mock.Anything)
			})
		})

		Convey("when we receive a non nil message", func() {
			msg := models.GiveBitsToBroadcasterMessage{
				TransactionId:          "test-transaction-id",
				RequestingTwitchUserId: "test-requesting-twitch-user-id",
				TargetTwitchUserId:     "test-target-twitch-user-id",
				TimeOfEvent:            time.Now(),
				BitsSent:               100,
			}

			Convey("Errors when user state setter fails", func() {
				userStateSetter.On("Set", mock.Anything, msg.RequestingTwitchUserId, api.UserStateCheered).Return(errors.New("test error"))

				Convey("then we return nil, since this isn't blocking", func() {
					err := handler.HandleGiveBitsToBroadcaster(context.Background(), &msg)
					So(err, ShouldBeNil)
				})
			})

			Convey("Succeeds when user state setter succeeds", func() {
				userStateSetter.On("Set", mock.Anything, msg.RequestingTwitchUserId, api.UserStateCheered).Return(nil)

				Convey("then we return nil", func() {
					err := handler.HandleGiveBitsToBroadcaster(context.Background(), &msg)
					So(err, ShouldBeNil)
				})
			})
		})
	})
}

func TestUserStateHandler_HandleAddBitsToCustomerAccount(t *testing.T) {
	Convey("Given a prometheus handler", t, func() {
		userStateGetter := new(userstate_mock.Getter)
		userStateSetter := new(userstate_mock.Setter)
		firstPurchasePubSubber := new(firstpurchase_mock.PubSubber)

		handler := &Handler{
			UserStateGetter:        userStateGetter,
			UserStateSetter:        userStateSetter,
			FirstPurchasePubSubber: firstPurchasePubSubber,
		}

		ctx := context.Background()

		Convey("when it receives a nil message", func() {
			var msg *models.AddBitsToCustomerAccountMessage

			Convey("then we return nil and do nothing", func() {
				err := handler.HandleAddBitsToCustomerAccount(ctx, msg)
				So(err, ShouldBeNil)
				userStateGetter.AssertNotCalled(t, "Get", mock.Anything)
				userStateSetter.AssertNotCalled(t, "Set", mock.Anything, mock.Anything, mock.Anything)
				firstPurchasePubSubber.AssertNotCalled(t, "Notify", mock.Anything, mock.Anything, mock.Anything)
			})
		})

		Convey("when the message is not nil", func() {
			msg := models.AddBitsToCustomerAccountMessage{
				TransactionId:      "test-transaction-id",
				TargetTwitchUserId: "test-target-twitch-user-id",
				TimeOfEvent:        time.Now(),
				TotalBits:          100,
			}

			Convey("when the user state getter fails", func() {
				userStateGetter.On("Get", msg.TargetTwitchUserId).Return(api.UserStateUnknown, errors.New("WALRUS STRIKE"))

				Convey("then we return an error", func() {
					err := handler.HandleAddBitsToCustomerAccount(ctx, &msg)

					So(err, ShouldNotBeNil)
				})
			})

			Convey("when the user state getter returns a user that is not new", func() {
				userStateGetter.On("Get", msg.TargetTwitchUserId).Return(api.UserStateCheered, nil)

				Convey("then we should return nil and not set any new state", func() {
					err := handler.HandleAddBitsToCustomerAccount(ctx, &msg)

					So(err, ShouldBeNil)
					userStateSetter.AssertNotCalled(t, "Set", mock.Anything, msg.TargetTwitchUserId, api.UserStateAcquired)
					firstPurchasePubSubber.AssertNotCalled(t, "Notify", ctx, msg.TargetTwitchUserId, int64(msg.TotalBits))
				})
			})

			Convey("when the user state getter returns a user that is new", func() {
				userStateGetter.On("Get", msg.TargetTwitchUserId).Return(api.UserStateNew, nil)

				Convey("when the user state setter fails", func() {
					userStateSetter.On("Set", mock.Anything, msg.TargetTwitchUserId, api.UserStateAcquired).Return(errors.New("WALRUS STRIKE"))

					Convey("then we return an error", func() {
						err := handler.HandleAddBitsToCustomerAccount(ctx, &msg)

						So(err, ShouldNotBeNil)
					})
				})

				Convey("when the user state setter succeeds", func() {
					userStateSetter.On("Set", mock.Anything, msg.TargetTwitchUserId, api.UserStateAcquired).Return(nil)

					Convey("when the first purchase pubsubber fails", func() {
						firstPurchasePubSubber.On("Notify", ctx, msg.TargetTwitchUserId, int64(msg.TotalBits)).Return(errors.New("WALRUS STRIKE"))

						Convey("then we return an error", func() {
							err := handler.HandleAddBitsToCustomerAccount(ctx, &msg)

							So(err, ShouldNotBeNil)
						})
					})

					Convey("when the first purchase pubsubber succeeds", func() {
						firstPurchasePubSubber.On("Notify", ctx, msg.TargetTwitchUserId, int64(msg.TotalBits)).Return(nil)

						Convey("then we return nil and we set the new user state", func() {
							err := handler.HandleAddBitsToCustomerAccount(ctx, &msg)

							So(err, ShouldBeNil)
							userStateSetter.AssertCalled(t, "Set", mock.Anything, msg.TargetTwitchUserId, api.UserStateAcquired)
							firstPurchasePubSubber.AssertCalled(t, "Notify", ctx, msg.TargetTwitchUserId, int64(msg.TotalBits))
						})
					})
				})
			})
		})
	})
}
