package onboard

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	log "code.justin.tv/commerce/logrus"
	badgetieremotes "code.justin.tv/commerce/payday/badgetiers/products/emotes"
	"code.justin.tv/commerce/payday/cache/channel"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/datascience"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/s3"
	"code.justin.tv/commerce/payday/sns"
	"code.justin.tv/commerce/payday/utils/experiments"
	"code.justin.tv/commerce/payday/utils/pointers"
	moneypenny "code.justin.tv/revenue/moneypenny/client"
	"github.com/aws/aws-sdk-go/aws/awserr"
)

const (
	offboardStatus    = "offboard"
	onboardStatus     = "onboard"
	partnerCategory   = "partner"
	affiliateCategory = "affiliate"
	developerCategory = "developer"
)

type Handler interface {
	Handle(ctx context.Context, msg moneypenny.OnboardingStatusMessage) error
}

type Onboarder struct {
	Config                  *config.Configuration               `inject:""`
	S3Client                s3.IS3Client                        `inject:""`
	DataScienceClient       datascience.DataScience             `inject:""`
	SNSClient               sns.ISNSClient                      `inject:"usWestSNSClient"`
	BadgeTierEmotesLogician badgetieremotes.Logician            `inject:""`
	ExperimentFetcher       experiments.ExperimentConfigFetcher `inject:""`
	ChannelManager          channel.ChannelManager              `inject:""`
}

func (o *Onboarder) saveAuditInfo(msg moneypenny.OnboardingStatusMessage) error {
	s3Key := fmt.Sprintf("%s/%s/%d", o.Config.Environment, msg.ChannelID, msg.Timestamp.UnixNano())
	bs, err := json.Marshal(msg)
	if err != nil {
		return errors.New("Error marshalling partner onboard message to JSON")
	}

	err = o.S3Client.UploadFileToBucket(o.Config.BitsOnboardEventsBucketName, s3Key, bs)
	if err != nil {
		return errors.New("Error uploading partner onboard message to S3")
	}

	return nil
}

func (o *Onboarder) Handle(ctx context.Context, msg moneypenny.OnboardingStatusMessage) error {
	err := o.saveAuditInfo(msg)
	if err != nil {
		log.WithError(err).WithField("channelID", msg.ChannelID).Error("Error saving onboard audit info")
		return err
	}

	featuresFieldExists, _, err := getFeatures(msg.Features)
	if err != nil {
		return err
	}

	if msg.Action == offboardStatus {
		return o.Offboard(ctx, msg)
	} else if featuresFieldExists || msg.Action == onboardStatus && msg.ActionCategory == affiliateCategory {
		// Onboard channel onto Bits
		err := o.OnBoard(ctx, msg)
		if err != nil {
			return err
		}

		if o.ExperimentFetcher.IsExperimentActiveForChannel(ctx, experiments.BitsTierEmoteGroupIDOnboardPrefix, msg.ChannelID) {
			// Pre-generate all bits tier emote group IDs for this channel
			return o.BadgeTierEmotesLogician.CreateAndStoreAllGroupIDsForChannel(ctx, msg.ChannelID)
		}

		return nil
	} else {
		log.WithField("msg", msg).Info("Skipping message")
		return nil
	}
}

func (o *Onboarder) Offboard(ctx context.Context, msg moneypenny.OnboardingStatusMessage) error {
	if msg.ActionCategory == developerCategory {
		log.WithField("msg", msg).Info("Offboard from the developer program. Skipping...")
		return nil
	}

	if msg.ActionCategory == partnerCategory && msg.Category == affiliateCategory {
		log.WithField("msg", msg).Info("Partner downgrading to affiliateCategory. Skipping...")
		return nil
	}

	err, _ := o.setBitsEnabled(msg.ChannelID, false, msg.Timestamp)
	return err
}

func (o *Onboarder) OnBoard(ctx context.Context, msg moneypenny.OnboardingStatusMessage) error {
	featuresFieldExists, featuresMap, err := getFeatures(msg.Features)
	if err != nil {
		return err
	}

	if !featuresFieldExists {
		err, performedUpdate := o.setBitsEnabled(msg.ChannelID, true, msg.Timestamp)
		if performedUpdate {
			o.sendDatascience(ctx, msg)
		}
		return err
	}

	bitsIface, present := featuresMap["bits"]
	if !present {
		return errors.Newf("Bits feature not present in features. %v", featuresMap)
	}

	onboarded, ok := bitsIface.(bool)
	if !ok {
		return errors.Newf("Cannot read bits feature value. %v", bitsIface)
	}

	err, performedUpdate := o.setBitsEnabled(msg.ChannelID, onboarded, msg.Timestamp)
	if performedUpdate {
		o.sendDatascience(ctx, msg)
	}

	return err
}
func (o *Onboarder) sendDatascience(ctx context.Context, msg moneypenny.OnboardingStatusMessage) {
	po := &models.PartnerOnboarded{ChannelID: msg.ChannelID, PartnerType: string(msg.ActionCategory)}
	err := o.DataScienceClient.TrackBitsEvent(ctx, datascience.PartnerOnboardedEventName, po)
	if err != nil {
		log.WithError(err).Error("Error tracking enable bits onboarding event with data science")
	}
}

func (o *Onboarder) setBitsEnabled(channelId string, onboarded bool, lastupdated time.Time) (err error, wasUpdated bool) {
	var optedOut *bool
	if onboarded {
		optedOut = pointers.BoolP(false)
	}
	record := dynamo.Channel{
		Id:                     dynamo.ChannelId(channelId),
		Onboarded:              pointers.BoolP(onboarded),
		OptedOut:               optedOut,
		LastOnboardedEventTime: pointers.Int64P(lastupdated.UnixNano()),
	}

	// TODO: this call didn't use to take in a context so leaving an empty one for now
	err = o.ChannelManager.UpdateIfStatusIsNewer(context.Background(), &record)
	if ae, ok := err.(awserr.RequestFailure); ok && ae.Code() == "ConditionalCheckFailedException" {
		log.WithField("channel_id", channelId).Info("Previously persisted update. Skipping message")
		return nil, false
	} else if err != nil {
		return errors.Notef(err, "Could not persist status update"), false
	}

	return nil, true
}

// We treat {features: {}} and {features: null} as empty
func getFeatures(features interface{}) (exists bool, featuresMap map[string]interface{}, err error) {
	if features == nil {
		return false, nil, nil
	}

	featuresMap, ok := features.(map[string]interface{})
	if !ok {
		return false, nil, errors.New("Features cannot be converted to a map of string interface")
	}

	if len(featuresMap) == 0 {
		return false, nil, nil
	}

	return true, featuresMap, nil
}
