package onboard

import (
	"context"
	"encoding/json"
	"os"
	"testing"

	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/dynamo"
	badgetieremotes_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/badgetiers/products/emotes"
	channel_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/cache/channel"
	datascience_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/datascience"
	s3_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/s3"
	sns_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sns"
	experiments_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/utils/experiments"
	moneypenny "code.justin.tv/revenue/moneypenny/client"
	"github.com/gofrs/uuid"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestOnboarder_Handle(t *testing.T) {
	Convey("Onboarder Properly Handles Message", t, func() {
		mockedDatascience := new(datascience_mock.DataScience)
		mockedS3Client := new(s3_mock.IS3Client)
		mockedSNSClient := new(sns_mock.ISNSClient)
		mockBadgeTierEmotesLogician := new(badgetieremotes_mock.Logician)
		mockExperimentsFetcher := new(experiments_mock.ExperimentConfigFetcher)
		mockChannelManager := new(channel_mock.ChannelManager)

		o := &Onboarder{
			DataScienceClient:       mockedDatascience,
			S3Client:                mockedS3Client,
			Config:                  &config.Configuration{Environment: "UnitTest"},
			SNSClient:               mockedSNSClient,
			BadgeTierEmotesLogician: mockBadgeTierEmotesLogician,
			ExperimentFetcher:       mockExperimentsFetcher,
			ChannelManager:          mockChannelManager,
		}

		mockedS3Client.On("UploadFileToBucket", mock.Anything, mock.Anything, mock.Anything).Return(nil)
		mockedDatascience.On("TrackBitsEvent", mock.Anything, mock.Anything, mock.Anything).Return(nil)
		mockChannelManager.On("UpdateIfStatusIsNewer", mock.Anything, mock.Anything).Return(nil)
		mockedSNSClient.On("PostToTopic", mock.Anything, mock.Anything).Return(nil)
		mockBadgeTierEmotesLogician.On("CreateAndStoreAllGroupIDsForChannel", mock.Anything, mock.Anything).Return(nil)
		mockExperimentsFetcher.On("IsExperimentActiveForChannel", mock.Anything, mock.Anything, mock.Anything).Return(true)

		Convey("When user is on-boarded as a partner", func() {
			msg := getMessage("testdata/partner-onboard.json")
			msg.ChannelID = getUUID()
			Convey("Partner is marked as bits enabled", func() {
				err := o.Handle(context.TODO(), msg)

				So(err, ShouldBeNil)

				mockChannelManager.AssertNumberOfCalls(t, "UpdateIfStatusIsNewer", 1)
				daoRecord := mockChannelManager.Calls[0].Arguments.Get(1).(*dynamo.Channel)
				So(*daoRecord.Onboarded, ShouldBeTrue)

				Convey("Bits emote group IDs are generated", func() {
					mockBadgeTierEmotesLogician.AssertNumberOfCalls(t, "CreateAndStoreAllGroupIDsForChannel", 1)
				})
			})
		})

		Convey("When user leaves the partner program", func() {
			msg := getMessage("testdata/partner-offboard.json")
			msg.ChannelID = getUUID()
			Convey("Partner is marked as not bits enabled", func() {
				err := o.Handle(context.TODO(), msg)

				So(err, ShouldBeNil)

				mockChannelManager.AssertNumberOfCalls(t, "UpdateIfStatusIsNewer", 1)
				daoRecord := mockChannelManager.Calls[0].Arguments.Get(1).(*dynamo.Channel)
				So(*daoRecord.Onboarded, ShouldBeFalse)

				Convey("Bits emote group IDs are not generated", func() {
					mockBadgeTierEmotesLogician.AssertNumberOfCalls(t, "CreateAndStoreAllGroupIDsForChannel", 0)
				})
			})
		})

		Convey("When User is on-boarded as an affiliate", func() {
			msg := getMessage("testdata/affiliate-onboard.json")
			msg.ChannelID = getUUID()
			Convey("Affiliate is marked as bits enabled", func() {
				err := o.Handle(context.TODO(), msg)

				So(err, ShouldBeNil)

				mockChannelManager.AssertNumberOfCalls(t, "UpdateIfStatusIsNewer", 1)
				daoRecord := mockChannelManager.Calls[0].Arguments.Get(1).(*dynamo.Channel)
				So(*daoRecord.Onboarded, ShouldBeTrue)

				Convey("All bits emote group IDs are generated", func() {
					mockBadgeTierEmotesLogician.AssertNumberOfCalls(t, "CreateAndStoreAllGroupIDsForChannel", 1)
				})
			})
		})

		Convey("When User is on-boarded as an esport affiliateCategory", func() {
			msg := getMessage("testdata/esport-affiliate-onboard.json")
			msg.ChannelID = getUUID()
			Convey("Affiliate is marked as bits enabled", func() {
				err := o.Handle(context.TODO(), msg)

				So(err, ShouldBeNil)

				mockChannelManager.AssertNumberOfCalls(t, "UpdateIfStatusIsNewer", 1)
				daoRecord := mockChannelManager.Calls[0].Arguments.Get(1).(*dynamo.Channel)
				So(*daoRecord.Onboarded, ShouldBeTrue)

				Convey("All bits emote group IDs are generated", func() {
					mockBadgeTierEmotesLogician.AssertNumberOfCalls(t, "CreateAndStoreAllGroupIDsForChannel", 1)
				})
			})
		})

		Convey("When an Affiliate is upgraded to Partner", func() {
			msg := getMessage("testdata/affiliate-upgrade.json")
			msg.ChannelID = getUUID()
			Convey("Affiliate is marked as bits enabled", func() {
				err := o.Handle(context.TODO(), msg)

				So(err, ShouldBeNil)

				mockChannelManager.AssertNumberOfCalls(t, "UpdateIfStatusIsNewer", 1)
				daoRecord := mockChannelManager.Calls[0].Arguments.Get(1).(*dynamo.Channel)
				So(*daoRecord.Onboarded, ShouldBeTrue)

				Convey("All bits emote group IDs are generated", func() {
					mockBadgeTierEmotesLogician.AssertNumberOfCalls(t, "CreateAndStoreAllGroupIDsForChannel", 1)
				})
			})
		})

		Convey("When a partner applies for a developerCategory account", func() {
			msg := getMessage("testdata/partner-apply-developer.json")
			msg.ChannelID = getUUID()
			Convey("Bits enabled is unaffected", func() {
				err := o.Handle(context.TODO(), msg)

				So(err, ShouldBeNil)

				mockChannelManager.AssertNumberOfCalls(t, "UpdateIfStatusIsNewer", 0)

				Convey("All bits emote group IDs are not generated", func() {
					mockBadgeTierEmotesLogician.AssertNumberOfCalls(t, "CreateAndStoreAllGroupIDsForChannel", 0)
				})
			})
		})

		Convey("When a partner upgrades to a premium partner", func() {
			msg := getMessage("testdata/partner-upgrade-premium.json")
			msg.ChannelID = getUUID()
			Convey("Bits enabled is set to Enabled", func() {
				err := o.Handle(context.TODO(), msg)

				So(err, ShouldBeNil)

				mockChannelManager.AssertNumberOfCalls(t, "UpdateIfStatusIsNewer", 1)
				daoRecord := mockChannelManager.Calls[0].Arguments.Get(1).(*dynamo.Channel)
				So(*daoRecord.Onboarded, ShouldBeTrue)

				Convey("All bits emote group IDs are generated", func() {
					mockBadgeTierEmotesLogician.AssertNumberOfCalls(t, "CreateAndStoreAllGroupIDsForChannel", 1)
				})
			})
		})

		Convey("When a partner upgrades to a custom partner", func() {
			msg := getMessage("testdata/partner-upgrade-custom.json")
			msg.ChannelID = getUUID()
			Convey("Bits enabled is set to what is specified in features", func() {
				err := o.Handle(context.TODO(), msg)

				So(err, ShouldBeNil)

				mockChannelManager.AssertNumberOfCalls(t, "UpdateIfStatusIsNewer", 1)
				daoRecord := mockChannelManager.Calls[0].Arguments.Get(1).(*dynamo.Channel)
				So(*daoRecord.Onboarded, ShouldBeFalse)

				Convey("All bits emote group IDs are generated", func() {
					mockBadgeTierEmotesLogician.AssertNumberOfCalls(t, "CreateAndStoreAllGroupIDsForChannel", 1)
				})
			})
		})

		Convey("When a partner downgrades to an affiliateCategory", func() {
			msg := getMessage("testdata/partner-downgrade-affiliate.json")
			msg.ChannelID = getUUID()
			Convey("Bits enabled is left alone", func() {
				err := o.Handle(context.TODO(), msg)

				So(err, ShouldBeNil)

				mockChannelManager.AssertNumberOfCalls(t, "UpdateIfStatusIsNewer", 0)

				Convey("All bits emote group IDs are not generated", func() {
					mockBadgeTierEmotesLogician.AssertNumberOfCalls(t, "CreateAndStoreAllGroupIDsForChannel", 0)
				})
			})
		})

		Convey("When a user leaves the affiliateCategory program", func() {
			msg := getMessage("testdata/affiliate-offboard.json")
			msg.ChannelID = getUUID()
			Convey("Bits enabled is set to not enabled", func() {
				err := o.Handle(context.TODO(), msg)

				So(err, ShouldBeNil)

				mockChannelManager.AssertNumberOfCalls(t, "UpdateIfStatusIsNewer", 1)
				daoRecord := mockChannelManager.Calls[0].Arguments.Get(1).(*dynamo.Channel)
				So(*daoRecord.Onboarded, ShouldBeFalse)

				Convey("All bits emote group IDs are not generated", func() {
					mockBadgeTierEmotesLogician.AssertNumberOfCalls(t, "CreateAndStoreAllGroupIDsForChannel", 0)
				})
			})
		})

		Convey("When a 0cpm partner joins", func() {
			msg := getMessage("testdata/partner-0cpm.json")
			msg.ChannelID = getUUID()
			Convey("Bits enabled is unchanged", func() {
				err := o.Handle(context.TODO(), msg)

				So(err, ShouldBeNil)

				mockChannelManager.AssertNumberOfCalls(t, "UpdateIfStatusIsNewer", 0)

				Convey("All bits emote group IDs are not generated", func() {
					mockBadgeTierEmotesLogician.AssertNumberOfCalls(t, "CreateAndStoreAllGroupIDsForChannel", 0)
				})
			})
		})

		// As of 3/9/2021, features are now sent as empty objects, rather than null
		Convey("When a 0cpm partner joins (v2 contract)", func() {
			msg := getMessage("testdata/partner-0cpm-empty-features-object.json")
			msg.ChannelID = getUUID()
			Convey("Bits enabled is unchanged", func() {
				err := o.Handle(context.TODO(), msg)

				So(err, ShouldBeNil)

				mockChannelManager.AssertNumberOfCalls(t, "UpdateIfStatusIsNewer", 0)

				Convey("All bits emote group IDs are not generated", func() {
					mockBadgeTierEmotesLogician.AssertNumberOfCalls(t, "CreateAndStoreAllGroupIDsForChannel", 0)
				})
			})
		})
	})
}

func getUUID() string {
	u, err := uuid.NewV4()
	So(err, ShouldBeNil)
	return u.String()
}

func getMessage(filepath string) moneypenny.OnboardingStatusMessage {
	file, err := os.Open(filepath)
	So(err, ShouldBeNil)
	var msg moneypenny.OnboardingStatusMessage
	decoder := json.NewDecoder(file)
	err = decoder.Decode(&msg)
	So(err, ShouldBeNil)
	return msg
}
