package poa

import (
	"context"
	"encoding/json"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/sqs/handlers/poa/onboard"
	moneypenny "code.justin.tv/revenue/moneypenny/client"
	"github.com/aws/aws-sdk-go/service/sqs"
)

type Handler struct {
	OnboardHandler onboard.Handler `inject:""`
}

func (h *Handler) HandleMessage(ctx context.Context, message *sqs.Message) (outErr error) {
	var sns models.SNSMessage
	err := json.Unmarshal([]byte(*message.Body), &sns)
	if err != nil {
		msg := "Unable to covert to SNS message struct"
		log.WithError(err).Error(msg)
		return errors.Notef(err, msg)
	}

	var moneypennyMessage moneypenny.OnboardingStatusMessage
	err = json.Unmarshal([]byte(sns.Message), &moneypennyMessage)
	if err != nil {
		msg := "Unable to covert to SNS message struct to moneypenny message"
		log.WithError(err).Error(msg)
		return errors.Notef(err, msg)
	}

	return h.OnboardHandler.Handle(ctx, moneypennyMessage)
}
