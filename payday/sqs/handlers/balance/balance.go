package balanceSQS

import (
	"context"
	"encoding/json"

	pubclient "code.justin.tv/chat/pubsub-go-pubclient/client"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/api/balance"
	"code.justin.tv/commerce/payday/cache"
	"code.justin.tv/commerce/payday/datascience"
	"code.justin.tv/commerce/payday/errors"
	payday_error "code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/hystrix"
	cmds "code.justin.tv/commerce/payday/hystrix"
	"code.justin.tv/commerce/payday/middleware"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/utils/pointers"
	"github.com/gofrs/uuid"
)

const (
	noChannel                    = ""
	bitsBalanceReasonUse         = "use"
	bitsBalanceReasonAcquisition = "acquisition"
	bitsBalanceReasonRemove      = "remove"
)

type BalanceHandler struct {
	BalanceGetter     balance.Getter          `inject:""`
	BalanceCache      cache.IBalanceCache     `inject:""`
	PubClient         pubclient.PubClient     `inject:""`
	DataScienceClient datascience.DataScience `inject:""`
}

type balanceupdate struct {
	user          string
	channelId     string
	updateReason  string
	transactionId string
	amountchanged int
}

func (bh *BalanceHandler) getBalance(user string, channel string) (*models.Balance, error) {
	var response *models.Balance
	err := hystrix.Do(cmds.GetBalanceCommand, func() error {
		_, getBitsBalanceResponse, err := bh.BalanceGetter.Get(context.Background(), user, channel, true)
		response = getBitsBalanceResponse
		return err
	}, nil)

	if err != nil {
		msg := "Error calling GetBitsBalance in Pachinko"
		log.WithError(err).Error(msg)
		return nil, payday_error.New(msg)
	}

	return response, nil
}

func (bh *BalanceHandler) handleUpdate(ctx context.Context, balanceupdate balanceupdate) error {
	response, err := bh.getBalance(balanceupdate.user, balanceupdate.channelId)
	if err != nil {
		return err
	}

	bh.BalanceCache.Set(ctx, balanceupdate.user, balanceupdate.channelId, response)

	balanceUpdates := models.BalanceUpdates{
		UserId:  balanceupdate.user,
		Balance: int64(response.TotalBits),
	}

	if balanceupdate.channelId != noChannel {
		balanceUpdates.ChannelTotal = &models.ChannelTotal{
			ChannelId:    balanceupdate.channelId,
			ChannelTotal: int64(pointers.Int32OrDefault(response.TotalBitsWithBroadcaster, 0)),
		}
	}

	messageIdUUID, err := uuid.NewV4()
	if err != nil {
		return errors.Notef(err, "Error generating UUID")
	}

	topics := []string{models.UserBitsUpdatesPubsubMessageTopic + "." + balanceupdate.user}
	pubsubMessage := models.PrivateBitsMessage{
		Data:        balanceUpdates,
		MessageId:   messageIdUUID.String(),
		MessageType: models.BitBalanceUpdateMessageType,
		Version:     models.CurrentPrivateMessageVersion,
	}

	marshaled, err := json.Marshal(pubsubMessage)
	if err != nil {
		return errors.Notef(err, "Could not marshal pubsub message %v", pubsubMessage)
	}

	err = bh.PubClient.Publish(ctx, topics, string(marshaled), nil)
	if err != nil {
		return errors.Notef(err, "Could not publish message %s to pubsub", string(marshaled))
	}

	//Determine if it is ok to send is message if this is a chargeback or customer revoke
	bitsBalance := &models.BitsBalance{
		TransactionID:     balanceupdate.transactionId,
		UserID:            balanceupdate.user,
		CurrentBalance:    int(response.TotalBits),
		Reason:            balanceupdate.updateReason,
		BitsAmountChanged: balanceupdate.amountchanged,
	}

	return bh.DataScienceClient.TrackBitsEvent(ctx, datascience.BitsBalanceEventName, bitsBalance)
}

//TODO: have jake review the number of bits changed here
func (bh *BalanceHandler) HandleGiveBitsToBroadcaster(parent context.Context, msg *models.GiveBitsToBroadcasterMessage) error {
	// No balance change notification needed for GiveBitsToBroadcaster triggered by a FinalizeHold. Balance deduction
	// sent when hold is created.
	if msg.TransactionType != models.GiveBitsToBroadcasterTransactionType {
		return nil
	}

	msgExtraContext := msg.ExtraContext
	var extraContext models.FabsGiveBitsExtraContext
	err := json.Unmarshal([]byte(msgExtraContext), &extraContext)
	var ctx context.Context
	if err == nil {
		ctx = context.WithValue(parent, middleware.ClientIDContextKey, extraContext.ClientID)
	} else {
		log.Warnf("Could not get extra context from give bits event %v. Error was %v", msg, err)
		ctx = parent
	}

	balanceUpdate := balanceupdate{
		user:          msg.RequestingTwitchUserId,
		channelId:     msg.TargetTwitchUserId,
		updateReason:  bitsBalanceReasonUse,
		transactionId: msg.TransactionId,
		amountchanged: msg.BitsSent,
	}

	return bh.handleUpdate(ctx, balanceUpdate)
}

func (bh *BalanceHandler) HandleAddBitsToCustomerAccount(ctx context.Context, msg *models.AddBitsToCustomerAccountMessage) error {
	balanceUpdate := balanceupdate{
		user:          msg.TargetTwitchUserId,
		channelId:     noChannel,
		updateReason:  bitsBalanceReasonAcquisition,
		transactionId: msg.TransactionId,
		amountchanged: msg.TotalBits,
	}

	return bh.handleUpdate(ctx, balanceUpdate)
}

func (bh *BalanceHandler) HandleRemoveBitsFromCustomerAccount(ctx context.Context, msg *models.RemoveBitsFromCustomerAccountMessage) error {
	balanceUpdate := balanceupdate{
		user:          msg.TargetTwitchUserId,
		channelId:     noChannel,
		updateReason:  bitsBalanceReasonRemove,
		transactionId: msg.TransactionId,
		amountchanged: msg.TotalBits,
	}

	return bh.handleUpdate(ctx, balanceUpdate)
}

func (bh *BalanceHandler) HandleUseBitsOnExtension(ctx context.Context, msg *models.UseBitsOnExtensionMessage) error {
	// No balance change notification needed for UseBitsOnExtension triggered by a FinalizeHold. Balance deduction
	// sent when hold is created.
	if msg.TransactionType != models.UseBitsOnExtensionTransactionType {
		return nil
	}

	balanceUpdate := balanceupdate{
		user:          msg.RequestingTwitchUserId,
		channelId:     msg.TargetTwitchUserId,
		updateReason:  bitsBalanceReasonUse,
		transactionId: msg.TransactionId,
		amountchanged: msg.BitsProccessed,
	}

	return bh.handleUpdate(ctx, balanceUpdate)
}

func (bh *BalanceHandler) HandleUseBitsOnPoll(ctx context.Context, msg *models.UseBitsOnPollMessage) error {
	balanceUpdate := balanceupdate{
		user:          msg.RequestingTwitchUserId,
		channelId:     msg.TargetTwitchUserId,
		updateReason:  bitsBalanceReasonUse,
		transactionId: msg.TransactionId,
		amountchanged: msg.BitsProccessed,
	}

	return bh.handleUpdate(ctx, balanceUpdate)
}

func (bh *BalanceHandler) HandleCreateHold(ctx context.Context, msg *models.CreateHoldMessage) error {
	balanceUpdate := balanceupdate{
		user:          msg.RequestingTwitchUserId,
		channelId:     noChannel,
		updateReason:  bitsBalanceReasonUse,
		transactionId: msg.TransactionId,
		amountchanged: msg.BitsProcessed,
	}

	return bh.handleUpdate(ctx, balanceUpdate)
}

func (bh *BalanceHandler) HandleFinalizeHold(ctx context.Context, msg *models.FinalizeHoldMessage) error {
	//TODO: probably don't need a notification, but wait to verify.
	return nil
}

func (h *BalanceHandler) HandleReleaseHold(ctx context.Context, msg *models.ReleaseHoldMessage) error {
	balanceUpdate := balanceupdate{
		user:          msg.RequestingTwitchUserId,
		channelId:     noChannel,
		updateReason:  bitsBalanceReasonAcquisition,
		transactionId: msg.TransactionId,
		amountchanged: msg.BitsReturned,
	}

	return h.handleUpdate(ctx, balanceUpdate)
}
