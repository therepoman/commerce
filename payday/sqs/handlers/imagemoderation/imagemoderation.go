package imagemoderationSQS

import (
	"context"
	"encoding/json"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/image"
	"code.justin.tv/commerce/payday/metrics"
	"code.justin.tv/commerce/payday/models"
	"github.com/aws/aws-sdk-go/service/sqs"
)

type ImageModerationHandler struct {
	ImageManager image.IImageManager `inject:""`
	Statter      metrics.Statter     `inject:""`
}

func (h *ImageModerationHandler) HandleMessage(ctx context.Context, message *sqs.Message) (outErr error) {
	startTime := time.Now()
	defer func() {
		var success int64
		if outErr != nil {
			success = 0
		} else {
			success = 100
			h.Statter.TimingDuration("ProcessImageModeration", time.Since(startTime))
		}
		h.Statter.Inc("ProcessImageModerationSuccess", success)
	}()

	var sns models.SNSMessage
	err := json.Unmarshal([]byte(*message.Body), &sns)
	if err != nil {
		msg := "Unable to covert to SNS message struct in imageModeration handler"
		log.WithError(err).Error(msg)
		return errors.Notef(err, msg)
	}

	var imageModerationMsg image.ImageModerationSNS
	err = json.Unmarshal([]byte(sns.Message), &imageModerationMsg)
	if err != nil {
		msg := "Unable to covert to ImageModeration msg struct in imageModeration handler"
		log.WithError(err).Error(msg)
		return errors.Notef(err, msg)
	}

	switch imageModerationMsg.ModerationType {
	case image.NUCLEAR_MODERATION_TYPE:
		err = h.ImageManager.DeleteChannelS3Images(ctx, imageModerationMsg.ChannelId)
		if err != nil {
			msg := "Unable to process image moderation job"
			log.WithError(err).WithFields(log.Fields{
				"channelId":      imageModerationMsg.ChannelId,
				"moderationType": imageModerationMsg.ModerationType,
			}).Error(msg)
			return errors.Notef(err, msg)
		}
	default:
		log.WithFields(log.Fields{
			"channelId":      imageModerationMsg.ChannelId,
			"moderationType": imageModerationMsg.ModerationType,
		}).Error("Received msg to moderate channel images with unknown type")
	}

	return nil
}
