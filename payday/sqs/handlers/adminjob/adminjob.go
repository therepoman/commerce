package adminjobSQS

import (
	"context"
	"encoding/json"
	"time"

	"code.justin.tv/commerce/gogogadget/random"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/adminjob"
	"code.justin.tv/commerce/payday/adminjob/handler"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/utils/strings"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/go-redis/redis_rate"
)

const (
	progressUpdateInterval = 50
)

type AdminJobHandler struct {
	AdminJobDao            dynamo.IAdminJobDao     `inject:""`
	AdminJobTaskDao        dynamo.IAdminJobTaskDao `inject:""`
	AdminJobManager        adminjob.IManager       `inject:""`
	AdminJobHandlerFactory handler.HandlerFactory  `inject:""`
	RateLimiter            *redis_rate.Limiter     `inject:""`
}

func (h *AdminJobHandler) HandleMessage(ctx context.Context, message *sqs.Message) error {
	var sns models.SNSMessage
	err := json.Unmarshal([]byte(*message.Body), &sns)
	if err != nil {
		msg := "Unable to covert to SNS message struct in adminJob handler"
		log.WithError(err).Error(msg)
		return errors.Notef(err, msg)
	}

	var identifier adminjob.Identifier
	err = json.Unmarshal([]byte(sns.Message), &identifier)
	if err != nil {
		msg := "Unable to covert to AdminJob msg struct in adminJob handler"
		log.WithError(err).Error(msg)
		return errors.Notef(err, msg)
	}

	aj, err := h.AdminJobDao.Get(identifier.Creator, identifier.JobID)
	if err != nil {
		msg := "Failed to get admin job from dynamo"
		log.WithError(err).Error(msg)
		return errors.Notef(err, msg)
	}

	if aj == nil {
		msg := "Admin job does not exist in dynamo"
		log.WithError(err).Error(msg)
		return errors.Notef(err, msg)
	}

	options, err := h.AdminJobManager.GetOptionsFromS3(ctx, identifier)
	if err != nil {
		msg := "Failed to get admin job options from S3"
		log.WithError(err).Error(msg)
		return errors.Notef(err, msg)
	}

	taskHandler, err := h.AdminJobHandlerFactory.GetTaskHandler(identifier, adminjob.Type(aj.Type), options)
	if err != nil {
		msg := "Could not get task handler for admin job type"
		log.WithError(err).Error(msg)
		return errors.Notef(err, msg)
	}

	input, err := h.AdminJobManager.GetInputFromS3(ctx, identifier)
	if err != nil {
		msg := "Failed to get admin job input from S3"
		log.WithError(err).Error(msg)
		return errors.Notef(err, msg)
	}

	err = taskHandler.ValidateInput(ctx, input)
	if err != nil {
		msg := "Could not validate admin job input"
		log.WithError(err).Error(msg)
		return errors.Notef(err, msg)
	}

	tasks, err := taskHandler.GetTasks(ctx, input)
	if err != nil {
		msg := "Failed to get tasks from S3 input"
		log.WithError(err).Error(msg)
		return errors.Notef(err, msg)
	}

	rateLimitCFG := taskHandler.GetRateLimitConfig()
	for i, task := range tasks {
		taskKey, err := taskHandler.GetTaskKey(ctx, adminjob.TaskIndex(i), task)
		if err != nil {
			msg := "Failed to get task key for admin job task"
			log.WithError(err).Error(msg)
			return errors.Notef(err, msg)
		}

		jobID := identifier.String()
		ajt, err := h.AdminJobTaskDao.Get(jobID, string(taskKey))
		if err != nil {
			msg := "Failed to get admin job task from dynamo"
			log.WithError(err).Error(msg)
			return errors.Notef(err, msg)
		}

		if ajt != nil {
			continue
		}

		if rateLimitCFG.Enabled {
			for {
				delay, allow := h.RateLimiter.AllowRate(rateLimitCFG.Key, rateLimitCFG.TPS)
				if allow {
					break
				}
				time.Sleep(random.Duration(delay, delay*10))
			}
		}

		taskOut, taskErr := taskHandler.Handle(ctx, task)
		if taskErr != nil {
			msg := "Failed to handle task"
			log.WithError(taskErr).Error(msg)
		}

		err = h.AdminJobTaskDao.Update(&dynamo.AdminJobTask{
			JobID:   jobID,
			TaskKey: string(taskKey),
		})
		if err != nil {
			msg := "Failed to update admin job task in dynamo"
			log.WithError(err).Error(msg)
			return errors.Notef(err, msg)
		}

		if strings.NotBlank(string(taskOut)) || taskErr != nil {
			err = h.AdminJobManager.SaveOutputToS3(ctx, identifier, taskKey, taskOut, taskErr)
			if err != nil {
				msg := "Failed to save output to S3"
				log.WithError(err).Error(msg)
				return errors.Notef(err, msg)
			}
		}

		if i != 0 && i%progressUpdateInterval == 0 {
			aj.Progress = float64(i) / float64(len(tasks))
			err = h.AdminJobDao.Update(aj)
			if err != nil {
				msg := "Failed to update admin job in dynamo"
				log.WithError(err).Error(msg)
				return errors.Notef(err, msg)
			}
		}
	}

	aj.Progress = 1
	err = h.AdminJobDao.Update(aj)
	if err != nil {
		msg := "Failed to update admin job in dynamo"
		log.WithError(err).Error(msg)
		return errors.Notef(err, msg)
	}

	return nil
}
