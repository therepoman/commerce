package perseus

import (
	"context"
	"errors"
	"testing"
	"time"

	format_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sqs/handlers/perseus/format"
	"code.justin.tv/commerce/payday/models"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"code.justin.tv/commerce/payday/sqs/handlers/perseus/format"
	"github.com/gofrs/uuid"
	. "github.com/smartystreets/goconvey/convey"
)

func TestFormatter_Format(t *testing.T) {
	Convey("given a perseus message formatter", t, func() {
		giveBitsToBroadcasterFormatter := new(format_mock.MessageFormatter)
		addBitsToCustomerAccountFormatter := new(format_mock.MessageFormatter)
		removeBitsFromCustomerAccountFormatter := new(format_mock.MessageFormatter)
		useBitsOnExtensionFormatter := new(format_mock.MessageFormatter)
		useBitsOnPollFormatter := new(format_mock.MessageFormatter)

		f := formatter{
			GiveBitsToBroadcasterFormatter:         giveBitsToBroadcasterFormatter,
			UseBitsOnExtensionFormatter:            useBitsOnExtensionFormatter,
			AddBitsToCustomerAccountFormatter:      addBitsToCustomerAccountFormatter,
			RemoveBitsFromCustomerAccountFormatter: removeBitsFromCustomerAccountFormatter,
			UseBitsOnPollFormatter:                 useBitsOnPollFormatter,
		}

		ctx := context.Background()

		Convey("when we succeed", func() {
			tx := &paydayrpc.Transaction{
				TransactionId: uuid.Must(uuid.NewV4()).String(),
			}

			Convey("when we have a Give Bits to Broadcaster transaction", func() {
				tx.TransactionType = models.GiveBitsToBroadcasterTransactionType
				giveBitsToBroadcasterFormatter.On("Format", tx).Return(&models.GiveBitsToBroadcasterMessage{
					TransactionId:   tx.TransactionId,
					TransactionType: models.GiveBitsToBroadcasterTransactionType,
					TimeOfEvent:     time.Now(),
				}, nil)

				bytes, err := f.Format(ctx, tx)
				So(err, ShouldBeNil)
				So(bytes, ShouldNotBeNil)
				giveBitsToBroadcasterFormatter.AssertCalled(t, "Format", tx)

				var message models.SQSMessageJSON
				err = message.UnmarshalJSON(bytes)
				So(err, ShouldBeNil)
				So(message.MessageType(), ShouldEqual, models.GiveBitsToBroadcaster)
			})

			Convey("when we have an Add Bits to Customer Account transaction", func() {
				tx.TransactionType = models.AddBitsTransactionType
				addBitsToCustomerAccountFormatter.On("Format", tx).Return(&models.AddBitsToCustomerAccountMessage{
					TransactionId:   tx.TransactionId,
					TransactionType: format.AddBitsToCustomerAccountMessageType,
					TimeOfEvent:     time.Now(),
				}, nil)

				bytes, err := f.Format(ctx, tx)
				So(err, ShouldBeNil)
				So(bytes, ShouldNotBeNil)
				addBitsToCustomerAccountFormatter.AssertCalled(t, "Format", tx)

				var message models.SQSMessageJSON
				err = message.UnmarshalJSON(bytes)
				So(err, ShouldBeNil)
				So(message.MessageType(), ShouldEqual, models.AddBitsToCustomerAccount)
			})

			Convey("when we have a Remove Bits from Customer Account transaction", func() {
				tx.TransactionType = models.AdminRemoveBitsTransactionType
				removeBitsFromCustomerAccountFormatter.On("Format", tx).Return(&models.RemoveBitsFromCustomerAccountMessage{
					TransactionId:   tx.TransactionId,
					TransactionType: format.RemoveBitsFromCustomerAccountMessageType,
					TimeOfEvent:     time.Now(),
				}, nil)

				bytes, err := f.Format(ctx, tx)
				So(err, ShouldBeNil)
				So(bytes, ShouldNotBeNil)
				removeBitsFromCustomerAccountFormatter.AssertCalled(t, "Format", tx)

				var message models.SQSMessageJSON
				err = message.UnmarshalJSON(bytes)
				So(err, ShouldBeNil)
				So(message.MessageType(), ShouldEqual, models.RemoveBitsFromCustomerAccount)
			})

			Convey("when we have a Use Bits on Extension transaction", func() {
				tx.TransactionType = models.UseBitsOnExtensionTransactionType
				useBitsOnExtensionFormatter.On("Format", tx).Return(&models.UseBitsOnExtensionMessage{
					TransactionId:   tx.TransactionId,
					TransactionType: models.UseBitsOnExtensionTransactionType,
					TimeOfEvent:     time.Now(),
				}, nil)

				bytes, err := f.Format(ctx, tx)
				So(err, ShouldBeNil)
				So(bytes, ShouldNotBeNil)
				useBitsOnExtensionFormatter.AssertCalled(t, "Format", tx)

				var message models.SQSMessageJSON
				err = message.UnmarshalJSON(bytes)
				So(err, ShouldBeNil)
				So(message.MessageType(), ShouldEqual, models.UseBitsOnExtension)
			})

			Convey("when we have a Use Bits on Poll transaction", func() {
				tx.TransactionType = models.UseBitsOnPollTransactionType
				useBitsOnPollFormatter.On("Format", tx).Return(&models.UseBitsOnPollMessage{
					TransactionId:   tx.TransactionId,
					TransactionType: format.UseBitsOnPollMessageType,
					TimeOfEvent:     time.Now(),
				}, nil)

				bytes, err := f.Format(ctx, tx)
				So(err, ShouldBeNil)
				So(bytes, ShouldNotBeNil)
				useBitsOnPollFormatter.AssertCalled(t, "Format", tx)

				var message models.SQSMessageJSON
				err = message.UnmarshalJSON(bytes)
				So(err, ShouldBeNil)
				So(message.MessageType(), ShouldEqual, models.UseBitsOnPoll)
			})
		})

		Convey("when we fail", func() {
			tx := &paydayrpc.Transaction{
				TransactionId: uuid.Must(uuid.NewV4()).String(),
			}

			Convey("when we fail to convert a Give Bits to Broadcaster transaction", func() {
				tx.TransactionType = models.GiveBitsToBroadcasterTransactionType
				giveBitsToBroadcasterFormatter.On("Format", tx).Return(nil, errors.New("WALRUS STRIKE"))

				bytes, err := f.Format(ctx, tx)
				So(err, ShouldNotBeNil)
				So(bytes, ShouldBeNil)
				giveBitsToBroadcasterFormatter.AssertCalled(t, "Format", tx)
			})

			Convey("when we fail to convert an Add Bits to Customer Account transaction", func() {
				tx.TransactionType = models.AddBitsTransactionType
				addBitsToCustomerAccountFormatter.On("Format", tx).Return(nil, errors.New("WALRUS STRIKE"))

				bytes, err := f.Format(ctx, tx)
				So(err, ShouldNotBeNil)
				So(bytes, ShouldBeNil)
				addBitsToCustomerAccountFormatter.AssertCalled(t, "Format", tx)
			})

			Convey("when we fail to convert a Remove Bits from Customer Account transaction", func() {
				tx.TransactionType = models.AdminRemoveBitsTransactionType
				removeBitsFromCustomerAccountFormatter.On("Format", tx).Return(nil, errors.New("WALRUS STRIKE"))

				bytes, err := f.Format(ctx, tx)
				So(err, ShouldNotBeNil)
				So(bytes, ShouldBeNil)
				removeBitsFromCustomerAccountFormatter.AssertCalled(t, "Format", tx)
			})

			Convey("when we fail to convert a Use Bits on Extension transaction", func() {
				tx.TransactionType = models.UseBitsOnExtensionTransactionType
				useBitsOnExtensionFormatter.On("Format", tx).Return(nil, errors.New("WALRUS STRIKE"))

				bytes, err := f.Format(ctx, tx)
				So(err, ShouldNotBeNil)
				So(bytes, ShouldBeNil)
				useBitsOnExtensionFormatter.AssertCalled(t, "Format", tx)
			})

			Convey("when we fail to convert a Use Bits on Poll transaction", func() {
				tx.TransactionType = models.UseBitsOnPollTransactionType
				useBitsOnPollFormatter.On("Format", tx).Return(nil, errors.New("WALRUS STRIKE"))

				bytes, err := f.Format(ctx, tx)
				So(err, ShouldNotBeNil)
				So(bytes, ShouldBeNil)
				useBitsOnPollFormatter.AssertCalled(t, "Format", tx)
			})

			Convey("when we get an unknown transaction type", func() {
				tx.TransactionType = "WalrusTransaction"

				bytes, err := f.Format(ctx, tx)
				So(err, ShouldNotBeNil)
				So(bytes, ShouldBeNil)
			})
		})
	})
}
