package format

import (
	"code.justin.tv/commerce/payday/models"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"github.com/golang/protobuf/ptypes"
)

type releaseHoldFormatter struct{}

func NewReleaseHoldFormatter() *releaseHoldFormatter {
	return &releaseHoldFormatter{}
}

func (f *releaseHoldFormatter) Format(tx *paydayrpc.Transaction) (models.SQSMessage, error) {
	timeOfEvent, err := ptypes.Timestamp(tx.TimeOfEvent)
	if err != nil {
		return nil, err
	}

	return &models.ReleaseHoldMessage{
		TransactionId:          tx.TransactionId,
		TransactionType:        models.ReleaseHoldTransactionType,
		TimeOfEvent:            timeOfEvent,
		IsPachinko:             true,
		BitsReturned:           int(tx.NumberOfBits),
		RequestingTwitchUserId: tx.UserId,
		HoldId:                 tx.OperandTransactionId,
	}, nil
}
