package format

import (
	"code.justin.tv/commerce/payday/models"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
)

var TransactionTypeToEventReasonCodeMap = map[string]string{
	models.AdminAddBitsTransactionType:          models.EventReasonGenericAdminAdd,
	models.AdminRemoveBitsTransactionType:       models.EventReasonGenericAdminRemove,
	models.AddBitsTransactionType:               models.EventReasonEntitleBits,
	models.WATEBTransactionType:                 models.EventReasonBitsForAds,
	models.GiveBitsToBroadcasterTransactionType: models.EventReasonCheer,
}

type MessageFormatter interface {
	Format(tx *paydayrpc.Transaction) (models.SQSMessage, error)
}
