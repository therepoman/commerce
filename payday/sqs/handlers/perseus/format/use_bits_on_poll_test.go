package format

import (
	"testing"

	"code.justin.tv/commerce/gogogadget/random"
	"code.justin.tv/commerce/payday/models"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"github.com/gofrs/uuid"
	"github.com/golang/protobuf/ptypes"
	. "github.com/smartystreets/goconvey/convey"
)

func TestUseBitsOnPollFormatter_Format(t *testing.T) {
	Convey("given a use bits on poll formatter", t, func() {
		formatter := useBitsOnPollFormatter{}

		timestamp := ptypes.TimestampNow()

		txRecord := &paydayrpc.Transaction{
			TransactionId:             uuid.Must(uuid.NewV4()).String(),
			UserId:                    random.String(16),
			ChannelId:                 random.String(16),
			NumberOfBits:              322,
			TransactionType:           models.UseBitsOnPollTransactionType,
			TimeOfEvent:               timestamp,
			TotalBitsAfterTransaction: 644,
			TotalBitsToBroadcaster:    644,
			PollId:                    random.String(16),
			PollChoiceId:              random.String(16),
		}

		Convey("successfully make a use bits on poll message", func() {
			message, err := formatter.Format(txRecord)
			So(err, ShouldBeNil)
			So(message, ShouldNotBeNil)
			So(message.MessageType(), ShouldEqual, models.UseBitsOnPoll)
			So(message.(*models.UseBitsOnPollMessage).BitsProccessed, ShouldEqual, -322)
		})

		Convey("failure cases", func() {
			Convey("when we have a bad timestamp", func() {
				txRecord.TimeOfEvent = nil
			})

			_, err := formatter.Format(txRecord)
			So(err, ShouldNotBeNil)
		})
	})
}
