package format

import (
	"encoding/json"

	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/partner"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"github.com/golang/protobuf/ptypes"
)

type giveBitsToBroadcasterFormatter struct{}

func NewGiveBitsToBroadcasterFormatter() *giveBitsToBroadcasterFormatter {
	return &giveBitsToBroadcasterFormatter{}
}

func (f *giveBitsToBroadcasterFormatter) Format(tx *paydayrpc.Transaction) (models.SQSMessage, error) {
	/*
		Need to figure out how to handle fields for:
		* AccountTypeToBitAmount
	*/

	emoteUses := map[string]models.Bits{}
	for emote, bits := range tx.EmoteTotals {
		emoteUses[emote] = models.Bits(bits)
	}

	sponsoredAmounts := map[string]int{}
	for emote, bits := range tx.SponsoredAmounts {
		sponsoredAmounts[emote] = int(bits)
	}

	extraContext := models.FabsGiveBitsExtraContext{
		ClientID:    tx.ClientId,
		PartnerType: partner.GetPartnerTypeFromValue(tx.PartnerType),
		NumChatters: tx.NumChatters,
		IPAddress:   tx.IpAddress,
		City:        tx.City,
		CountryCode: tx.CountryCode,
		ZipCode:     tx.ZipCode,
		EmoteUses:   emoteUses,
	}
	extraContextBytes, err := json.Marshal(&extraContext)
	if err != nil {
		return nil, err
	}

	timeOfEvent, err := ptypes.Timestamp(tx.TimeOfEvent)
	if err != nil {
		return nil, err
	}

	return &models.GiveBitsToBroadcasterMessage{
		TransactionId:             tx.TransactionId,
		TransactionType:           tx.TransactionType,
		TargetTwitchUserId:        tx.ChannelId,
		RequestingTwitchUserId:    tx.UserId,
		PublicMessage:             tx.RawMessage,
		ExtraContext:              string(extraContextBytes),
		TimeOfEvent:               timeOfEvent,
		TotalBitsAfterTransaction: int(tx.TotalBitsAfterTransaction),
		IsAnonymous:               tx.IsAnonymous,
		BitsSent:                  -int(tx.NumberOfBits),
		TotalBitsToBroadcaster:    int(tx.TotalBitsToBroadcaster),
		EventReasonCode:           TransactionTypeToEventReasonCodeMap[tx.TransactionType],
		SponsoredAmounts:          sponsoredAmounts,
		IsPachinko:                true,
		HoldId:                    tx.OperandTransactionId,
	}, nil
}
