package format

import (
	"testing"

	"code.justin.tv/commerce/gogogadget/random"
	"code.justin.tv/commerce/payday/models"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"github.com/gofrs/uuid"
	"github.com/golang/protobuf/ptypes"
	. "github.com/smartystreets/goconvey/convey"
)

func TestUseBitsOnExtensionFormatter_Format(t *testing.T) {
	Convey("given a use bits on extension formatter", t, func() {
		formatter := useBitsOnExtensionFormatter{}

		timestamp := ptypes.TimestampNow()

		txRecord := &paydayrpc.Transaction{
			TransactionId:             uuid.Must(uuid.NewV4()).String(),
			UserId:                    random.String(16),
			ChannelId:                 random.String(16),
			ExtensionClientId:         random.String(32),
			ProductId:                 random.String(16),
			ZipCode:                   "98101",
			CountryCode:               "US",
			City:                      "Seattle",
			IpAddress:                 "10.0.0.3",
			NumberOfBits:              322,
			TransactionType:           models.UseBitsOnExtensionTransactionType,
			TimeOfEvent:               timestamp,
			TotalBitsAfterTransaction: 644,
			TotalBitsToBroadcaster:    644,
			BitsUsageAttributions: map[string]*paydayrpc.BitsUsageAttributionWithTotal{
				"user1": {
					RewardAttribution:        1,
					BitsRevenueAttribution:   2,
					TotalBitsWithBroadcaster: 3,
				},
			},
		}

		Convey("successfully make a use bits on extension message", func() {
			message, err := formatter.Format(txRecord)
			So(err, ShouldBeNil)
			So(message, ShouldNotBeNil)
			So(message.MessageType(), ShouldEqual, models.UseBitsOnExtension)
			So(message.(*models.UseBitsOnExtensionMessage).BitsProccessed, ShouldEqual, -322)
			So(len(message.(*models.UseBitsOnExtensionMessage).UsageOverrideList), ShouldEqual, 1)
			So(message.(*models.UseBitsOnExtensionMessage).UsageOverrideList["user1"].RewardAttribution, ShouldEqual, txRecord.BitsUsageAttributions["user1"].RewardAttribution)
			So(message.(*models.UseBitsOnExtensionMessage).UsageOverrideList["user1"].TotalWithBroadcasterPrior, ShouldEqual, txRecord.BitsUsageAttributions["user1"].TotalBitsWithBroadcaster)
			So(message.(*models.UseBitsOnExtensionMessage).UsageOverrideList["user1"].BitsRevenueAttributionToBroadcaster, ShouldEqual, txRecord.BitsUsageAttributions["user1"].BitsRevenueAttribution)
		})

		Convey("failure cases", func() {
			Convey("when we have a bad timestamp", func() {
				txRecord.TimeOfEvent = nil
			})

			_, err := formatter.Format(txRecord)
			So(err, ShouldNotBeNil)
		})
	})
}
