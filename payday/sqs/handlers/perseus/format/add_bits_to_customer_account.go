package format

import (
	"encoding/json"

	"code.justin.tv/commerce/payday/models"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"github.com/golang/protobuf/ptypes"
)

const AddBitsToCustomerAccountMessageType = "AddBitsToCustomerAccount"

type addBitsToCustomerAccountFormatter struct{}

func NewAddBitsToCustomerAccountFormatter() *addBitsToCustomerAccountFormatter {
	return &addBitsToCustomerAccountFormatter{}
}

func (f *addBitsToCustomerAccountFormatter) Format(tx *paydayrpc.Transaction) (models.SQSMessage, error) {
	/*
		Need to figure out how to handle fields for:
		* AccountTypeToBitAmount
	*/
	extraContext := models.AddBitsToCustomerAccountExtraContext{
		DoxOrderID: tx.OriginOrderId,
		TrackingID: tx.TrackingId,
	}
	extraContextBytes, err := json.Marshal(&extraContext)
	if err != nil {
		return nil, err
	}

	timeOfEvent, err := ptypes.Timestamp(tx.TimeOfEvent)
	if err != nil {
		return nil, err
	}

	return &models.AddBitsToCustomerAccountMessage{
		TransactionId:             tx.TransactionId,
		TransactionType:           AddBitsToCustomerAccountMessageType,
		TargetTwitchUserId:        tx.UserId,
		TotalBits:                 int(tx.NumberOfBits),
		ExtraContext:              string(extraContextBytes),
		TimeOfEvent:               timeOfEvent,
		TotalBitsAfterTransaction: int(tx.TotalBitsAfterTransaction),
		AccountTypeToBitAmount: map[string]int{
			tx.BitsType: int(tx.NumberOfBits),
		},
		AdminID:               tx.AdminId,
		AdminReason:           tx.AdminReason,
		PurchasePrice:         tx.PurchasePrice,
		PurchasePriceCurrency: tx.PurchaseCurrency,
		PurchaseMarketplace:   tx.Platform,
		SkuQuantity:           int(tx.SkuQuantity),
		EventReasonCode:       TransactionTypeToEventReasonCodeMap[tx.TransactionType],
		IsPachinko:            true,
	}, nil
}
