package format

import (
	"testing"

	"code.justin.tv/commerce/gogogadget/random"
	"code.justin.tv/commerce/payday/models"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"github.com/gofrs/uuid"
	"github.com/golang/protobuf/ptypes"
	. "github.com/smartystreets/goconvey/convey"
)

func TestAddBitsToCustomerAccountFormatter_Format(t *testing.T) {
	Convey("given a add bits to customer account message formatter", t, func() {
		formatter := &addBitsToCustomerAccountFormatter{}

		timestamp := ptypes.TimestampNow()

		txRecord := &paydayrpc.Transaction{
			TransactionId:             uuid.Must(uuid.NewV4()).String(),
			TransactionType:           models.AdminAddBitsTransactionType,
			UserId:                    random.String(16),
			NumberOfBits:              322,
			TotalBitsAfterTransaction: 644,
			TimeOfEvent:               timestamp,
			AdminId:                   random.String(16),
			AdminReason:               random.String(16),
			PurchasePrice:             "3.99",
			PurchaseCurrency:          "USD",
			Platform:                  string(models.AMAZON),
			SkuQuantity:               1,
			TrackingId:                uuid.Must(uuid.NewV4()).String(),
		}

		Convey("successfully make a add bits to customer account message", func() {
			message, err := formatter.Format(txRecord)
			So(err, ShouldBeNil)
			So(message, ShouldNotBeNil)
			So(message.MessageType(), ShouldEqual, models.AddBitsToCustomerAccount)
		})

		Convey("failure cases", func() {
			Convey("when we have a bad timestamp", func() {
				txRecord.TimeOfEvent = nil
			})

			_, err := formatter.Format(txRecord)
			So(err, ShouldNotBeNil)
		})
	})
}
