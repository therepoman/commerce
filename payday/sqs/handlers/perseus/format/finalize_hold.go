package format

import (
	"code.justin.tv/commerce/payday/dynamo/holds"
	"code.justin.tv/commerce/payday/models"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"github.com/golang/protobuf/ptypes"
)

type finalizeHoldFormatter struct{}

func NewFinalizeHoldFormatter() *finalizeHoldFormatter {
	return &finalizeHoldFormatter{}
}

func (f *finalizeHoldFormatter) Format(tx *paydayrpc.Transaction) (models.SQSMessage, error) {
	timeOfEvent, err := ptypes.Timestamp(tx.TimeOfEvent)
	if err != nil {
		return nil, err
	}

	recipients := map[string]holds.BitsUsageAttribution{}

	for _, r := range recipients {
		recipients[r.ID] = holds.BitsUsageAttribution{
			ID:                                  r.ID,
			BitsRevenueAttributionToBroadcaster: r.BitsRevenueAttributionToBroadcaster,
			RewardAttribution:                   r.RewardAttribution,
			TotalWithBroadcasterPrior:           r.TotalWithBroadcasterPrior,
		}
	}

	return &models.FinalizeHoldMessage{
		TransactionId:          tx.TransactionId,
		TransactionType:        models.FinalizeHoldTransactionType,
		TimeOfEvent:            timeOfEvent,
		IsPachinko:             true,
		RequestingTwitchUserId: tx.UserId,
		Recipients:             recipients,
	}, nil
}
