package format

import (
	"testing"

	"code.justin.tv/commerce/gogogadget/random"
	"code.justin.tv/commerce/payday/models"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"github.com/gofrs/uuid"
	"github.com/golang/protobuf/ptypes"
	. "github.com/smartystreets/goconvey/convey"
)

func TestRemoveBitsFromCustomerAccountFormatter_Format(t *testing.T) {
	Convey("given a remove bits from customer account formatter", t, func() {
		formatter := removeBitsFromCustomerAccountFormatter{}

		timestamp := ptypes.TimestampNow()

		txRecord := &paydayrpc.Transaction{
			TransactionId:             uuid.Must(uuid.NewV4()).String(),
			UserId:                    random.String(16),
			NumberOfBits:              322,
			TransactionType:           models.AdminRemoveBitsTransactionType,
			TimeOfEvent:               timestamp,
			AdminId:                   random.String(16),
			AdminReason:               random.String(255),
			TotalBitsAfterTransaction: 644,
		}

		Convey("successfully make a remove bits from customer account message", func() {
			message, err := formatter.Format(txRecord)
			So(err, ShouldBeNil)
			So(message, ShouldNotBeNil)
			So(message.MessageType(), ShouldEqual, models.RemoveBitsFromCustomerAccount)
		})

		Convey("failure cases", func() {
			Convey("when we have a bad timestamp", func() {
				txRecord.TimeOfEvent = nil
			})

			_, err := formatter.Format(txRecord)
			So(err, ShouldNotBeNil)
		})
	})
}
