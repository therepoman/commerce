package format

import (
	"code.justin.tv/commerce/payday/models"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"github.com/golang/protobuf/ptypes"
)

const UseBitsOnPollMessageType = "UseBitsOnPoll"

type useBitsOnPollFormatter struct{}

func NewUseBitsOnPollFormatter() *useBitsOnPollFormatter {
	return &useBitsOnPollFormatter{}
}

func (f *useBitsOnPollFormatter) Format(tx *paydayrpc.Transaction) (models.SQSMessage, error) {
	/*
		Need to figure out how to handle fields for:
		* AccountTypeToBitAmount
	*/
	timeOfEvent, err := ptypes.Timestamp(tx.TimeOfEvent)
	if err != nil {
		return nil, err
	}

	return &models.UseBitsOnPollMessage{
		TransactionId:             tx.TransactionId,
		TransactionType:           UseBitsOnPollMessageType,
		RequestingTwitchUserId:    tx.UserId,
		TargetTwitchUserId:        tx.ChannelId,
		TimeOfEvent:               timeOfEvent,
		BitsProccessed:            -int(tx.NumberOfBits),
		TotalBitsAfterTransaction: int(tx.TotalBitsAfterTransaction),
		TotalBitsToBroadcaster:    int(tx.TotalBitsToBroadcaster),
		PollChoiceId:              tx.PollChoiceId,
		PollId:                    tx.PollId,
		IsPachinko:                true,
	}, nil
}
