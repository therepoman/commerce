package format

import (
	"testing"

	"code.justin.tv/commerce/gogogadget/random"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/partner"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"github.com/gofrs/uuid"
	"github.com/golang/protobuf/ptypes"
	. "github.com/smartystreets/goconvey/convey"
)

func TestGiveBitsToBroadcasterFormatter_Format(t *testing.T) {
	Convey("given a give bits to broadcaster message formatter", t, func() {
		formatter := giveBitsToBroadcasterFormatter{}

		timestamp := ptypes.TimestampNow()

		txRecord := &paydayrpc.Transaction{
			TransactionId:   uuid.Must(uuid.NewV4()).String(),
			ChannelId:       random.String(16),
			UserId:          random.String(16),
			NumberOfBits:    322,
			RawMessage:      "cheer322 throw the game walRus",
			TransactionType: models.GiveBitsToBroadcasterTransactionType,
			EmoteTotals: map[string]int64{
				"cheer": int64(322),
			},
			NumberOfBitsSponsored:     0,
			TimeOfEvent:               timestamp,
			TotalBitsAfterTransaction: 644,
			TotalBitsToBroadcaster:    322,
			ZipCode:                   "98101",
			City:                      "Seattle",
			CountryCode:               "USA",
			IpAddress:                 "10.0.0.10",
			PartnerType:               string(partner.TraditionalPartnerType),
			ClientId:                  random.String(16),
			NumChatters:               "28",
			IsAnonymous:               false,
			SponsoredAmounts:          nil,
		}

		Convey("successfully make a give bits to broadcaster message", func() {
			message, err := formatter.Format(txRecord)
			So(err, ShouldBeNil)
			So(message, ShouldNotBeNil)
			So(message.MessageType(), ShouldEqual, models.GiveBitsToBroadcaster)
			So(message.(*models.GiveBitsToBroadcasterMessage).BitsSent, ShouldEqual, -322)
		})

		Convey("failure cases", func() {
			Convey("when we have a bad timestamp", func() {
				txRecord.TimeOfEvent = nil
			})

			_, err := formatter.Format(txRecord)
			So(err, ShouldNotBeNil)
		})
	})
}
