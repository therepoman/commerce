package format

import (
	"code.justin.tv/commerce/payday/models"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"github.com/golang/protobuf/ptypes"
)

type createHoldFormatter struct{}

func NewCreateHoldFormatter() *createHoldFormatter {
	return &createHoldFormatter{}
}

func (f *createHoldFormatter) Format(tx *paydayrpc.Transaction) (models.SQSMessage, error) {
	timeOfEvent, err := ptypes.Timestamp(tx.TimeOfEvent)
	if err != nil {
		return nil, err
	}

	return &models.CreateHoldMessage{
		TransactionId:          tx.TransactionId,
		TransactionType:        models.CreateHoldTransactionType,
		TimeOfEvent:            timeOfEvent,
		IsPachinko:             true,
		BitsProcessed:          -int(tx.NumberOfBits),
		RequestingTwitchUserId: tx.UserId,
	}, nil
}
