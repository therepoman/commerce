package format

import (
	"encoding/json"

	"code.justin.tv/commerce/payday/dynamo/holds"

	"code.justin.tv/commerce/payday/models"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"github.com/golang/protobuf/ptypes"
)

type useBitsOnExtensionFormatter struct{}

func NewUseBitsOnExtensionFormatter() *useBitsOnExtensionFormatter {
	return &useBitsOnExtensionFormatter{}
}

func (f *useBitsOnExtensionFormatter) Format(tx *paydayrpc.Transaction) (models.SQSMessage, error) {
	/*
		Need to figure out how to handle fields for:
		* AccountTypeToBitAmount
	*/
	timeOfEvent, err := ptypes.Timestamp(tx.TimeOfEvent)
	if err != nil {
		return nil, err
	}

	extraContext := models.BitsExtensionsExtraContext{
		ZipCode:     tx.ZipCode,
		CountryCode: tx.CountryCode,
		City:        tx.City,
		IPAddress:   tx.IpAddress,
	}
	extraContextBytes, err := json.Marshal(&extraContext)
	if err != nil {
		return nil, err
	}

	usageOverrideList := map[string]holds.BitsUsageAttribution{}

	for k, v := range tx.BitsUsageAttributions {
		usageOverrideList[k] = holds.BitsUsageAttribution{
			BitsRevenueAttributionToBroadcaster: v.BitsRevenueAttribution,
			RewardAttribution:                   v.RewardAttribution,
			TotalWithBroadcasterPrior:           v.TotalBitsWithBroadcaster,
			ID:                                  k,
		}
	}

	return &models.UseBitsOnExtensionMessage{
		TransactionId:             tx.TransactionId,
		TransactionType:           tx.TransactionType,
		RequestingTwitchUserId:    tx.UserId,
		TargetTwitchUserId:        tx.ChannelId,
		ExtensionID:               tx.ExtensionClientId,
		SKU:                       tx.ProductId,
		BitsProccessed:            -int(tx.NumberOfBits),
		ExtraContext:              string(extraContextBytes),
		TotalBitsAfterTransaction: int(tx.TotalBitsAfterTransaction),
		TotalBitsToBroadcaster:    int(tx.TotalBitsToBroadcaster),
		TimeOfEvent:               timeOfEvent,
		IsPachinko:                true,
		IgnorePayout:              tx.PayoutIgnored,
		UsageOverrideList:         usageOverrideList,
		HoldId:                    tx.OperandTransactionId,
	}, nil
}
