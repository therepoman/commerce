package format

import (
	"code.justin.tv/commerce/payday/models"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"github.com/golang/protobuf/ptypes"
)

const RemoveBitsFromCustomerAccountMessageType = "RemoveBitsFromCustomerAccount"

type removeBitsFromCustomerAccountFormatter struct{}

func NewRemoveBitsFromCustomerAccountFormatter() *removeBitsFromCustomerAccountFormatter {
	return &removeBitsFromCustomerAccountFormatter{}
}

func (f *removeBitsFromCustomerAccountFormatter) Format(tx *paydayrpc.Transaction) (models.SQSMessage, error) {
	/*
		Need to figure out how to handle fields for:
		* AccountTypeToBitAmount
	*/
	timeOfEvent, err := ptypes.Timestamp(tx.TimeOfEvent)
	if err != nil {
		return nil, err
	}

	return &models.RemoveBitsFromCustomerAccountMessage{
		TransactionId:             tx.TransactionId,
		TransactionType:           RemoveBitsFromCustomerAccountMessageType,
		TargetTwitchUserId:        tx.UserId,
		TimeOfEvent:               timeOfEvent,
		EventReasonCode:           TransactionTypeToEventReasonCodeMap[tx.TransactionType],
		TotalBitsAfterTransaction: int(tx.TotalBitsAfterTransaction),
		AdminID:                   tx.AdminId,
		AdminReason:               tx.AdminReason,
		TotalBits:                 int(tx.NumberOfBits),
		ProcessedBits:             int(tx.NumberOfBits),
		IsPachinko:                true,
	}, nil
}
