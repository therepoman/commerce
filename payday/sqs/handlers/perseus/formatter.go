package perseus

import (
	"context"
	"encoding/json"
	"errors"

	"code.justin.tv/commerce/payday/models"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"code.justin.tv/commerce/payday/sqs/handlers/perseus/format"
)

type SQSMessageWrapper struct {
	models.SQSMessage
	TransactionType string `json:"transactionType"`
}

type Formatter interface {
	Format(ctx context.Context, tx *paydayrpc.Transaction) ([]byte, error)
}

type formatter struct {
	GiveBitsToBroadcasterFormatter         format.MessageFormatter `inject:"giveBitsToBroadcasterFormatter"`
	AddBitsToCustomerAccountFormatter      format.MessageFormatter `inject:"addBitsToCustomerAccountFormatter"`
	RemoveBitsFromCustomerAccountFormatter format.MessageFormatter `inject:"removeBitsFromCustomerAccountFormatter"`
	UseBitsOnExtensionFormatter            format.MessageFormatter `inject:"useBitsOnExtensionFormatter"`
	UseBitsOnPollFormatter                 format.MessageFormatter `inject:"useBitsOnPollFormatter"`
	CreateHoldFormatter                    format.MessageFormatter `inject:"createHoldFormatter"`
	FinalizeHoldFormatter                  format.MessageFormatter `inject:"finalizeHoldFormatter"`
	ReleaseHoldFormatter                   format.MessageFormatter `inject:"releaseHoldFormatter"`
}

func NewFormatter() Formatter {
	return &formatter{}
}

func (f *formatter) Format(ctx context.Context, tx *paydayrpc.Transaction) ([]byte, error) {
	var sqsMessage models.SQSMessage
	var err error
	switch tx.TransactionType {
	case models.GiveBitsToBroadcasterTransactionType, models.GiveBitsToBroadcasterChallengeTransactionType:
		sqsMessage, err = f.GiveBitsToBroadcasterFormatter.Format(tx)
	case models.AdminAddBitsTransactionType, models.AddBitsTransactionType:
		sqsMessage, err = f.AddBitsToCustomerAccountFormatter.Format(tx)
	case models.AdminRemoveBitsTransactionType:
		sqsMessage, err = f.RemoveBitsFromCustomerAccountFormatter.Format(tx)
	case models.UseBitsOnExtensionTransactionType, models.UseBitsOnExtensionChallengeTransactionType:
		sqsMessage, err = f.UseBitsOnExtensionFormatter.Format(tx)
	case models.UseBitsOnPollTransactionType:
		sqsMessage, err = f.UseBitsOnPollFormatter.Format(tx)
	case models.CreateHoldTransactionType:
		sqsMessage, err = f.CreateHoldFormatter.Format(tx)
	case models.FinalizeHoldTransactionType:
		sqsMessage, err = f.FinalizeHoldFormatter.Format(tx)
	case models.ReleaseHoldTransactionType:
		sqsMessage, err = f.ReleaseHoldFormatter.Format(tx)
	default:
		return nil, errors.New("Could not determine matching transactionType for unmarshaling: " + tx.TransactionType)
	}

	if err != nil {
		return nil, err
	}

	output, err := json.Marshal(&sqsMessage)
	if err != nil {
		return nil, err
	}

	return output, nil
}
