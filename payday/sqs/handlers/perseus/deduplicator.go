package perseus

import (
	"context"
	"fmt"
	"strconv"
	"time"

	"code.justin.tv/commerce/payday/clients/redis"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/lock"
	go_redis "github.com/go-redis/redis"
)

const (
	redisTimeToLive    = 5 * time.Minute
	redisLockKeyPrefix = "bits-perseus-pachinko-lock-%s"
	redisDuplicateKey  = "bits-perseus-pachinko-recent-transaction-%s"
)

var (
	ErrAlreadySent = errors.New("Notification already sent")
)

type Deduplicator interface {
	Hold(ctx context.Context, transactionId string) (Hold, error)
	Finish(ctx context.Context, transactionLock Hold) error
}

type deduplicator struct {
	RedisClient   redis.Client       `inject:"redisClient"`
	LockingClient lock.LockingClient `inject:""`
}

type Hold interface {
	getTransactionId() string
	getLock() lock.Lock
}

type hold struct {
	transactionId   string
	transactionLock lock.Lock
}

// Used only in tests
func NewDuplicatorHold(transactionId string, lockie lock.Lock) Hold {
	return &hold{
		transactionId:   transactionId,
		transactionLock: lockie,
	}
}

func NewDeduplicator() Deduplicator {
	return &deduplicator{}
}

func (d *deduplicator) Hold(ctx context.Context, transactionId string) (Hold, error) {
	transactionLock, err := d.LockingClient.ObtainLock(getLockKey(transactionId))
	if err != nil {
		return nil, err
	}

	result, err := d.RedisClient.Get(ctx, getDeduplicateKey(transactionId)).Result()
	if err != nil && err != go_redis.Nil {
		return nil, err
	}

	if value, _ := strconv.ParseBool(result); value {
		return nil, ErrAlreadySent
	}

	return &hold{
		transactionId,
		transactionLock,
	}, nil
}

func (d *deduplicator) Finish(ctx context.Context, transactionLock Hold) error {
	if transactionLock == nil {
		return errors.New("transaction lock is invalid.")
	}

	err := d.RedisClient.Set(ctx, getDeduplicateKey(transactionLock.getTransactionId()), strconv.FormatBool(true), redisTimeToLive).Err()
	if err != nil {
		return err
	}

	return transactionLock.getLock().Unlock()
}

func getLockKey(transactionId string) string {
	return fmt.Sprintf(redisLockKeyPrefix, transactionId)
}

func getDeduplicateKey(transactionId string) string {
	return fmt.Sprintf(redisDuplicateKey, transactionId)
}

func (h *hold) getTransactionId() string {
	return h.transactionId
}
func (h *hold) getLock() lock.Lock {
	return h.transactionLock
}
