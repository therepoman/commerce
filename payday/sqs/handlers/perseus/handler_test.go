package perseus_test

import (
	"context"
	"encoding/json"
	"errors"
	"testing"
	"time"

	"code.justin.tv/commerce/gogogadget/pointers"
	lock_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/lock"
	crypto_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sqs/crypto"
	perseus_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sqs/handlers/perseus"
	transactions_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/transactions"
	"code.justin.tv/commerce/payday/models"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"code.justin.tv/commerce/payday/sqs/handlers/perseus"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/gofrs/uuid"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func BuildTestGiveBitsToBroadcasterMessage(transactionID string) models.GiveBitsToBroadcasterMessage {
	return models.GiveBitsToBroadcasterMessage{
		TransactionId:          transactionID,
		TransactionType:        models.GiveBitsToBroadcasterTransactionType,
		RequestingTwitchUserId: "test-requesting-twitch-user-id",
		TargetTwitchUserId:     "test-target-twitch-user-id",
		TimeOfEvent:            time.Now(),
		BitsSent:               100,
	}
}

func testSQSMessage(transactionID string) *sqs.Message {
	event := &models.PachinkoSNSUpdate{
		TransactionIDs: []string{transactionID},
	}
	eventBytes, err := json.Marshal(event)
	So(err, ShouldBeNil)
	snsMsg := &models.SNSMessage{
		MessageAttributes: map[string]models.SNSMessageAttribute{},
		Message:           string(eventBytes),
	}
	snsJsonBytes, err := json.Marshal(snsMsg)
	So(err, ShouldBeNil)
	return &sqs.Message{
		Body: pointers.StringP(string(snsJsonBytes)),
	}
}

func TestHandler_HandleMessage(t *testing.T) {
	Convey("given a perseus message handler", t, func() {
		formatter := new(perseus_mock.Formatter)
		sns := new(perseus_mock.SNS)
		transactionGetter := new(transactions_mock.Getter)
		encrypter := new(crypto_mock.Encrypter)
		deduplicator := new(perseus_mock.Deduplicator)

		h := perseus.Handler{
			SNS:               sns,
			Encrypter:         encrypter,
			Formatter:         formatter,
			TransactionGetter: transactionGetter,
			Deduplicator:      deduplicator,
		}

		ctx := context.Background()
		transactionID := uuid.Must(uuid.NewV4()).String()
		tx := &paydayrpc.Transaction{
			TransactionId: transactionID,
		}
		perseusMessage := BuildTestGiveBitsToBroadcasterMessage(transactionID)
		perseusMessageBytes, _ := json.Marshal(&perseusMessage)

		Convey("when we succeed", func() {
			transactionGetter.On("GetTransactionsByID", ctx, []string{transactionID}).Return([]*paydayrpc.Transaction{
				tx,
			}, nil)
			formatter.On("Format", ctx, tx).Return(perseusMessageBytes, nil)
			encrypter.On("EncryptMessage", mock.Anything).Return(&models.EncryptedNotification{}, nil)
			sns.On("PostMessage", ctx, mock.Anything).Return(nil)

			Convey("when hold works", func() {
				lockie := new(lock_mock.Lock)
				hold := perseus.NewDuplicatorHold(transactionID, lockie)
				deduplicator.On("Hold", mock.Anything, mock.Anything).Return(hold, nil)

				Convey("when finish works", func() {
					deduplicator.On("Finish", mock.Anything, mock.Anything).Return(nil)
					err := h.HandleMessage(ctx, testSQSMessage(transactionID))
					So(err, ShouldBeNil)
				})
			})
		})

		Convey("when we fail", func() {
			Convey("when we receive a nil SQS message", func() {
				err := h.HandleMessage(ctx, nil)
				So(err, ShouldNotBeNil)
			})

			Convey("when we fail to unmarshal SNS message", func() {
				err := h.HandleMessage(ctx, &sqs.Message{
					Body: pointers.StringP("not valid json"),
				})
				So(err, ShouldNotBeNil)
			})

			Convey("when we fail to unmarshal Pachinko update", func() {
				snsMsg := &models.SNSMessage{
					MessageAttributes: map[string]models.SNSMessageAttribute{},
					Message:           string("Not valid JSON whoops"),
				}
				snsJsonBytes, _ := json.Marshal(snsMsg)

				err := h.HandleMessage(ctx, &sqs.Message{
					Body: pointers.StringP(string(snsJsonBytes)),
				})
				So(err, ShouldNotBeNil)
			})

			Convey("when we fail to fetch transaction record", func() {
				transactionGetter.On("GetTransactionsByID", ctx, []string{transactionID}).Return(nil, errors.New("WALRUS STRIKE"))

				err := h.HandleMessage(ctx, testSQSMessage(transactionID))
				So(err, ShouldNotBeNil)
			})

			Convey("when we succeed to fetch transaction record", func() {
				transactionGetter.On("GetTransactionsByID", ctx, []string{transactionID}).Return([]*paydayrpc.Transaction{
					tx,
				}, nil)

				Convey("when hold works", func() {
					lockie := new(lock_mock.Lock)
					hold := perseus.NewDuplicatorHold(transactionID, lockie)
					deduplicator.On("Hold", mock.Anything, mock.Anything).Return(hold, nil)

					Convey("when we fail to format the message", func() {
						formatter.On("Format", ctx, tx).Return(nil, errors.New("WALRUS STRIKE"))

						err := h.HandleMessage(ctx, testSQSMessage(transactionID))
						So(err, ShouldNotBeNil)
					})

					Convey("when we succeed to format the message", func() {
						formatter.On("Format", ctx, tx).Return(perseusMessageBytes, nil)

						Convey("when we fail to encrypt the message", func() {
							encrypter.On("EncryptMessage", mock.Anything).Return(nil, errors.New("WALRUS STRIKE"))

							err := h.HandleMessage(ctx, testSQSMessage(transactionID))
							So(err, ShouldNotBeNil)
						})

						Convey("when we succeed to encrypt the message", func() {
							encrypter.On("EncryptMessage", mock.Anything).Return(&models.EncryptedNotification{}, nil)

							Convey("when we fail to send the SNS message", func() {
								sns.On("PostMessage", ctx, mock.Anything).Return(errors.New("WALRUS STRIKE"))

								err := h.HandleMessage(ctx, testSQSMessage(transactionID))
								So(err, ShouldNotBeNil)
							})
						})
					})
				})

				Convey("when hold tells us it's a duplicate", func() {
					deduplicator.On("Hold", mock.Anything, mock.Anything).Return(nil, perseus.ErrAlreadySent)
					err := h.HandleMessage(ctx, testSQSMessage(transactionID))
					So(err, ShouldBeNil)

					// because format, encrypt and SNS are not mocked in this flow, they'd throw errors if they were being called.
				})
			})
		})
	})
}
