package perseus

import (
	"context"
	"encoding/json"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/sqs/crypto"
	"code.justin.tv/commerce/payday/transactions"
	"github.com/aws/aws-sdk-go/service/sqs"
)

type Handler struct {
	TransactionGetter transactions.Getter `inject:""`
	Formatter         Formatter           `inject:""`
	SNS               SNS                 `inject:""`
	Encrypter         crypto.Encrypter    `inject:""`
	Deduplicator      Deduplicator        `inject:""`
}

func NewHandler() *Handler {
	return &Handler{}
}

func (h *Handler) HandleMessage(ctx context.Context, sqsMessage *sqs.Message) error {
	if sqsMessage == nil {
		msg := "received nil message on perseus queue"
		logrus.Error(msg)
		return errors.New(msg)
	}

	var snsMsg models.SNSMessage
	err := json.Unmarshal([]byte(*sqsMessage.Body), &snsMsg)
	if err != nil {
		msg := "unable to unmarshal SNS message struct in Perseus event SQS handler"
		logrus.WithError(err).Error(msg)
		return errors.Notef(err, msg)
	}

	var pachinkoMsg models.PachinkoSNSUpdate
	err = json.Unmarshal([]byte(snsMsg.Message), &pachinkoMsg)
	if err != nil {
		msg := "unable to unmarshal Pachinko message struct in Perseus event SQS handler"
		logrus.WithError(err).Error(msg)
		return errors.Notef(err, msg)
	}

	txs, err := h.TransactionGetter.GetTransactionsByID(ctx, pachinkoMsg.TransactionIDs)
	if err != nil {
		msg := "unable to fetch transactions from Dynamo"
		logrus.WithError(err).Error(msg)
		return errors.Notef(err, msg)
	}

	for _, tx := range txs {
		held, err := h.Deduplicator.Hold(ctx, tx.TransactionId)
		if err != nil {
			if err == ErrAlreadySent {
				continue
			} else {
				return err
			}
		}

		formatted, err := h.Formatter.Format(ctx, tx)
		if err != nil {
			logrus.WithField("transactionID", tx.TransactionId).WithError(err).Error("could not format message to Perseus message")
			return err
		}

		// TODO: This is where we will kick off the balance change processing SFN

		encryptedNotification, err := h.Encrypter.EncryptMessage(formatted)
		if err != nil {
			logrus.WithError(err).Error("failed to encrypt message")
			return err
		}

		encryptedNotificationBytes, err := json.Marshal(encryptedNotification)
		if err != nil {
			logrus.WithError(err).Error("failed to marshal encrypted notification message")
			return err
		}

		err = h.SNS.PostMessage(ctx, encryptedNotificationBytes)
		if err != nil {
			logrus.WithError(err).Error("could not send Perseus message to SNS topic")
			return err
		}
		err = h.Deduplicator.Finish(ctx, held)
		if err != nil {
			return err
		}
	}
	return nil
}
