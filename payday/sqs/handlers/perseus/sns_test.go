package perseus

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/payday/config"
	sns_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sns"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestSnsClient_PostMessage(t *testing.T) {
	Convey("given a perseus SNS publisher", t, func() {
		cfg := &config.Configuration{}
		snsMock := new(sns_mock.ISNSClient)

		s := &snsClient{
			Config: cfg,
			Client: snsMock,
		}

		ctx := context.Background()

		Convey("when there's no SNS topic configured", func() {
			cfg.PerseusSNSTopic = ""

			err := s.PostMessage(ctx, []byte{})
			So(err, ShouldNotBeNil)
		})

		Convey("when there's an SNS topic configured", func() {
			cfg.PerseusSNSTopic = "walrusArn"

			Convey("when the SNS client fails", func() {
				snsMock.On("PostToTopicWithContext", ctx, cfg.PerseusSNSTopic, mock.Anything).Return(errors.New("WALRUS STRIKE"))

				err := s.PostMessage(ctx, []byte{})
				So(err, ShouldNotBeNil)
				// We call SNS publish 4 times since we initially call it and then retry 3 times.
				snsMock.AssertNumberOfCalls(t, "PostToTopicWithContext", 4)
			})

			Convey("when the SNS client succeeds", func() {
				snsMock.On("PostToTopicWithContext", ctx, cfg.PerseusSNSTopic, mock.Anything).Return(nil)

				err := s.PostMessage(ctx, []byte{})
				So(err, ShouldBeNil)
			})
		})
	})
}
