package perseus

import (
	"context"
	"testing"

	"code.justin.tv/commerce/payday/errors"
	redis_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/clients/redis"
	lock_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/lock"
	"github.com/go-redis/redis"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestGetDeduplicateKey(t *testing.T) {
	Convey("given nothing at all", t, func() {
		So(getDeduplicateKey("foo"), ShouldEqual, "bits-perseus-pachinko-recent-transaction-foo")
		So(getDeduplicateKey(""), ShouldEqual, "bits-perseus-pachinko-recent-transaction-")
	})
}

func TestGetLockKey(t *testing.T) {
	Convey("given nothing at all", t, func() {
		So(getLockKey("foo"), ShouldEqual, "bits-perseus-pachinko-lock-foo")
		So(getLockKey(""), ShouldEqual, "bits-perseus-pachinko-lock-")
	})
}

func TestGetAndFinishHold(t *testing.T) {
	transactionId := "food-is-tasty"

	Convey("given a probably functional deduplicator", t, func() {
		ctx := context.Background()
		redisClient := new(redis_mock.Client)
		lockClient := new(lock_mock.LockingClient)

		deduplicator := deduplicator{
			RedisClient:   redisClient,
			LockingClient: lockClient,
		}

		Convey("when redis has no issue with locks", func() {
			lockie := new(lock_mock.Lock)
			lockClient.On("ObtainLock", mock.Anything).Return(lockie, nil)

			Convey("when redis doesn't have an entry", func() {
				redisClient.On("Get", mock.Anything, mock.Anything).Return(redis.NewStringResult("", nil))

				hold, err := deduplicator.Hold(ctx, transactionId)
				So(err, ShouldBeNil)
				So(hold, ShouldNotBeNil)

				Convey("when we can set an entry in it too", func() {
					redisClient.On("Set", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(redis.NewStatusCmd(nil))

					Convey("when we can release the lock", func() {
						lockie.On("Unlock").Return(nil)

						err = deduplicator.Finish(ctx, hold)
						So(err, ShouldBeNil)
					})
				})
			})

			Convey("when redis does have an entry", func() {
				redisClient.On("Get", mock.Anything, mock.Anything).Return(redis.NewStringResult("true", nil))

				hold, err := deduplicator.Hold(ctx, transactionId)
				So(hold, ShouldBeNil)
				So(err, ShouldEqual, ErrAlreadySent)
			})

			Convey("when redis errors getting an entry", func() {
				redisErr := errors.New("redis get failure")
				redisClient.On("Get", mock.Anything, mock.Anything).Return(redis.NewStringResult("", redisErr))

				hold, err := deduplicator.Hold(ctx, transactionId)
				So(hold, ShouldBeNil)
				So(err, ShouldEqual, redisErr)
			})
		})

		Convey("when locking presents an error", func() {
			lockErr := errors.New("lock error")
			lockClient.On("ObtainLock", mock.Anything).Return(nil, lockErr)

			hold, err := deduplicator.Hold(ctx, transactionId)
			So(hold, ShouldBeNil)
			So(err, ShouldEqual, lockErr)
		})
	})
}
