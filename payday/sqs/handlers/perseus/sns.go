package perseus

import (
	"context"
	"time"

	"code.justin.tv/commerce/gogogadget/strings"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/sns"
	"github.com/cenkalti/backoff"
)

type SNS interface {
	PostMessage(ctx context.Context, payload []byte) error
}

func NewSNS() SNS {
	return &snsClient{}
}

type snsClient struct {
	Client sns.ISNSClient        `inject:"usEastSNSClient"`
	Config *config.Configuration `inject:""`
}

func (s *snsClient) PostMessage(ctx context.Context, payload []byte) error {
	if strings.Blank(s.Config.PerseusSNSTopic) {
		return errors.New("Perseus SNS topic isn't configured!")
	}

	f := func() error {
		err := s.Client.PostToTopicWithContext(ctx, s.Config.PerseusSNSTopic, string(payload))
		if err != nil {
			log.WithFields(log.Fields{
				"topicARN":     s.Config.PerseusSNSTopic,
				"payload":      string(payload),
				"payload_size": len(payload),
			}).WithError(err).Error("Error submitting to Perseus topic.")
		}
		return err
	}

	var b backoff.BackOff = backoff.WithContext(newBackoff(), ctx)
	b = backoff.WithMaxTries(b, 3)

	return backoff.Retry(f, b)
}

func newBackoff() backoff.BackOff {
	exponential := backoff.NewExponentialBackOff()
	exponential.InitialInterval = 10 * time.Millisecond
	exponential.MaxElapsedTime = 1 * time.Second
	exponential.Multiplier = 2
	return exponential
}
