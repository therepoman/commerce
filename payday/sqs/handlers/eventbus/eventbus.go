package eventbus

import (
	"context"
	"encoding/json"
	"fmt"

	"code.justin.tv/commerce/payday/dynamo/extension_transaction"
	"code.justin.tv/commerce/payday/extension/transaction"
	"code.justin.tv/commerce/payday/models"
	eventbus "code.justin.tv/eventbus/client"
	"code.justin.tv/eventbus/schema/pkg/bits_use"
	"code.justin.tv/eventbus/schema/pkg/cheer"
)

// This package publishes bits-related events to the eventbus, where subscribers
// can then go and subscribe to these events. Currently, the only supported bits
// event is public cheering.
//
// For protobuf schema information see: https://git-aws.internal.justin.tv/eventbus/schema/blob/master/events/cheer/create.proto
// Eventbus service information: https://eventbus.internal.justin.tv/services/7

// eventbusCheerCreate takes in a *models.GiveBitsToBroadcasterMessage and hands back
// a struct ready to publish to the eventbus
func eventbusCheerCreate(msg *models.GiveBitsToBroadcasterMessage) *cheer.Create {
	c := &cheer.Create{}
	c.BitsTransactionId = msg.TransactionId
	c.ToUserId = msg.TargetTwitchUserId
	c.PublicMessage = msg.PublicMessage
	c.Bits = int64(msg.BitsSent)
	if msg.IsAnonymous {
		c.From = &cheer.CheerCreate_AnonymousUser{
			AnonymousUser: &cheer.AnonymousUser{},
		}
	} else {
		c.From = &cheer.CheerCreate_User{
			User: &cheer.User{
				Id: msg.RequestingTwitchUserId,
			},
		}
	}
	return c
}

func eventbusUseBitsOnExtension(msg *models.UseBitsOnExtensionMessage, tx *extension_transaction.ExtensionTransaction, product *transaction.FaladorProduct) *bits_use.BitsUseCreate {
	return &bits_use.BitsUseCreate{
		BitsTransactionId: msg.TransactionId,
		FromUserId:        msg.RequestingTwitchUserId,
		From: &bits_use.BitsUseCreate_Extension{
			Extension: &bits_use.Extension{
				Id:                 msg.ExtensionID,
				ProductId:          msg.SKU,
				InDevelopment:      tx.InDevelopment,
				Broadcast:          tx.Broadcast,
				ProductDisplayName: product.DisplayName,
			},
		},
	}
}

func eventbusUseBitsOnPoll(msg *models.UseBitsOnPollMessage) *bits_use.BitsUseCreate {
	return &bits_use.BitsUseCreate{
		BitsTransactionId: msg.TransactionId,
		BitsAmount:        int64(msg.BitsProccessed),
		FromUserId:        msg.RequestingTwitchUserId,
		ToUserId:          msg.TargetTwitchUserId,
		From: &bits_use.BitsUseCreate_Poll{
			Poll: &bits_use.Poll{
				Id:       msg.PollId,
				ChoiceId: msg.PollChoiceId,
			},
		},
	}
}

// Handler holds a client that publishes events to the eventbus
type Handler struct {
	Client                  eventbus.Publisher                              `inject:""`
	ExtensionTransactionDAO extension_transaction.IExtensionTransactionsDAO `inject:""`
}

// HandleGiveBitsToBroadcaster marshals the cheer event into a protobuf message and publishes to the eventbus
func (h *Handler) HandleGiveBitsToBroadcaster(ctx context.Context, msg *models.GiveBitsToBroadcasterMessage) error {
	cheerEvent := eventbusCheerCreate(msg)
	return h.Client.Publish(ctx, cheerEvent)
}

// HandleAddBitsToCustomerAccount noop
func (h *Handler) HandleAddBitsToCustomerAccount(ctx context.Context, msg *models.AddBitsToCustomerAccountMessage) error {
	return nil
}

// HandleRemoveBitsFromCustomerAccount noop
func (h *Handler) HandleRemoveBitsFromCustomerAccount(ctx context.Context, msg *models.RemoveBitsFromCustomerAccountMessage) error {
	return nil
}

// HandleUseBitsOnExtension marshals the bits on extension event into a protobuf message and publishes to the eventbus
func (h *Handler) HandleUseBitsOnExtension(ctx context.Context, msg *models.UseBitsOnExtensionMessage) error {
	txs, err := h.ExtensionTransactionDAO.GetByTransactionIDs([]string{msg.TransactionId})
	if err != nil {
		return err
	} else if len(txs) != 1 {
		return fmt.Errorf("received unexpected number of transactions for transactionID %s", msg.TransactionId)
	}
	tx := txs[0]

	var product transaction.FaladorProduct
	err = json.Unmarshal([]byte(tx.Product), &product)
	if err != nil {
		return err
	}

	return msg.HandleBitsRewardAttributions(func(broadcasterId string, bits int, totalBitsWithBroadcaster int) error {
		bitsUseCreateMsg := eventbusUseBitsOnExtension(msg, tx, &product)
		bitsUseCreateMsg.ToUserId = broadcasterId
		bitsUseCreateMsg.BitsAmount = int64(bits)
		return h.Client.Publish(ctx, bitsUseCreateMsg)
	})
}

// HandleUseBitsOnPoll marshals the bits on poll event into a protobuf message and publishes to the eventbus
func (h *Handler) HandleUseBitsOnPoll(ctx context.Context, msg *models.UseBitsOnPollMessage) error {
	bitsUseCreateMsg := eventbusUseBitsOnPoll(msg)
	return h.Client.Publish(ctx, bitsUseCreateMsg)
}

// CreateHold noop
func (h *Handler) HandleCreateHold(ctx context.Context, msg *models.CreateHoldMessage) error {
	return nil
}

// FinalizeHold noop
func (h *Handler) HandleFinalizeHold(ctx context.Context, msg *models.FinalizeHoldMessage) error {
	return nil
}

// ReleaseHold noop
func (h *Handler) HandleReleaseHold(ctx context.Context, msg *models.ReleaseHoldMessage) error {
	return nil
}
