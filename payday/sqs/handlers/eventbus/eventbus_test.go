package eventbus

import (
	"code.justin.tv/commerce/payday/dynamo/extension_transaction"
	"code.justin.tv/commerce/payday/dynamo/holds"
	extension_transaction_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/dynamo/extension_transaction"
	mock_eventbus "code.justin.tv/commerce/payday/mocks/code.justin.tv/eventbus/client"
	"code.justin.tv/commerce/payday/models"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"

	"context"
	"errors"
	"testing"
)

func testUseBitsOnExtensionMessage() *models.UseBitsOnExtensionMessage {
	return &models.UseBitsOnExtensionMessage{
		TransactionId:          "testTransactionID",
		RequestingTwitchUserId: "userID",
		TargetTwitchUserId:     "testChannelID",
		ExtensionID:            "testExtensionID",
		BitsProccessed:         1,
	}
}

func testUseBitsOnExtensionBitsAttributionMessage() *models.UseBitsOnExtensionMessage {
	return &models.UseBitsOnExtensionMessage{
		TransactionId:          "testTransactionID",
		RequestingTwitchUserId: "userID",
		TargetTwitchUserId:     "testChannelID",
		ExtensionID:            "testExtensionID",
		BitsProccessed:         2,
		UsageOverrideList: map[string]holds.BitsUsageAttribution{
			"testChannelID": {
				RewardAttribution:                   1,
				BitsRevenueAttributionToBroadcaster: 1,
				TotalWithBroadcasterPrior:           0,
				ID:                                  "testChannelID",
			},
			"otherTestChannelID": {
				RewardAttribution:                   1,
				BitsRevenueAttributionToBroadcaster: 1,
				TotalWithBroadcasterPrior:           0,
				ID:                                  "otherTestChannelID",
			},
		},
	}
}

func TestHandler_HandleUseBitsOnExtension(t *testing.T) {
	Convey("Given a EventBus handler", t, func() {
		mockEventBus := new(mock_eventbus.Publisher)
		mockDAO := new(extension_transaction_mock.IExtensionTransactionsDAO)

		handler := Handler{
			Client:                  mockEventBus,
			ExtensionTransactionDAO: mockDAO,
		}

		Convey("Returns error if Publish fails", func() {
			mockDAO.On("GetByTransactionIDs", mock.Anything).Return([]*extension_transaction.ExtensionTransaction{{Product: "{}"}}, nil)
			mockEventBus.On("Publish", context.Background(), mock.Anything).Return(errors.New("test error"))
			err := handler.HandleUseBitsOnExtension(context.Background(), testUseBitsOnExtensionMessage())
			So(err, ShouldNotBeNil)
		})

		Convey("Returns no error if Publish succeeds", func() {
			Convey("With No UsageOverride Entries, is called once", func() {
				mockEventBus.On("Publish", context.Background(), mock.Anything).Return(nil).Times(1)
				mockDAO.On("GetByTransactionIDs", mock.Anything).Return([]*extension_transaction.ExtensionTransaction{{Product: "{}"}}, nil)
				err := handler.HandleUseBitsOnExtension(context.Background(), testUseBitsOnExtensionMessage())
				So(err, ShouldBeNil)
				mockEventBus.AssertNumberOfCalls(t, "Publish", 1)
			})
			Convey("With 2 UsageOverride Entries, is called twice", func() {
				mockEventBus.On("Publish", context.Background(), mock.Anything).Return(nil).Times(2)
				mockDAO.On("GetByTransactionIDs", mock.Anything).Return([]*extension_transaction.ExtensionTransaction{{Product: "{}"}}, nil)
				err := handler.HandleUseBitsOnExtension(context.Background(), testUseBitsOnExtensionBitsAttributionMessage())
				So(err, ShouldBeNil)
				mockEventBus.AssertNumberOfCalls(t, "Publish", 2)
			})
		})
	})
}
