package pantheon

import (
	"context"
	"encoding/json"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/clients/pantheon"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/sqs/handlers/pantheon/observers"
	"github.com/aws/aws-sdk-go/service/sqs"
)

type SQSHandler struct {
	Observers *observers.ObserverChain `inject:""`
}

func (h *SQSHandler) HandleMessage(ctx context.Context, sqsMessage *sqs.Message) error {
	var snsMsg models.SNSMessage
	err := json.Unmarshal([]byte(*sqsMessage.Body), &snsMsg)
	if err != nil {
		msg := "unable to unmarshal SNS message struct in Pantheon event SQS handler"
		log.WithError(err).Error(msg)
		return errors.Notef(err, msg)
	}

	var pantheonMsg models.PantheonSNSMessage
	err = json.Unmarshal([]byte(snsMsg.Message), &pantheonMsg)
	if err != nil {
		msg := "unable to unmarshal Pantheon message struct in Pantheon event SQS handler"
		log.WithError(err).Error(msg)
		return errors.Notef(err, msg)
	}

	if pantheonMsg.Identifier.Domain != pantheon.PantheonBitsUsageDomain {
		return nil
	}

	if h.Observers != nil {
		err = h.Observers.Observer(ctx, pantheonMsg)
		if err != nil {
			return err
		}
	}

	return nil
}
