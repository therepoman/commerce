package observers

import (
	"context"

	"code.justin.tv/commerce/payday/models"
)

type Observer interface {
	Observe(ctx context.Context, msg models.PantheonSNSMessage) error
}

type ObserverChain struct {
	Observers []Observer
}

func (o *ObserverChain) AddObservers(observer ...Observer) {
	o.Observers = append(o.Observers, observer...)
}

func (o *ObserverChain) Observer(ctx context.Context, msg models.PantheonSNSMessage) error {
	for _, observer := range o.Observers {
		err := observer.Observe(ctx, msg)
		if err != nil {
			return err
		}
	}

	return nil
}
