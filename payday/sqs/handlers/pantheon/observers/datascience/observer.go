package datascience

import (
	"context"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/cache/channel"
	"code.justin.tv/commerce/payday/datascience"
	"code.justin.tv/commerce/payday/models"
	math_utils "code.justin.tv/commerce/payday/utils/math"
	"code.justin.tv/commerce/payday/utils/pointers"
)

type Observer struct {
	ChannelManager channel.ChannelManager  `inject:""`
	DataScience    datascience.DataScience `inject:""`
}

func (o *Observer) Observe(ctx context.Context, msg models.PantheonSNSMessage) error {
	channelID := msg.Identifier.GroupingKey
	ch, err := o.ChannelManager.Get(ctx, channelID)
	if err != nil {
		log.WithField("channelID", channelID).Error("unable to get channel from channel manager")
		return err
	}

	if ch == nil {
		log.WithField("channelID", msg.Identifier.GroupingKey).Warn("no dynamo channel record found while processing datascience pantheon event observer")
		return nil
	}

	// If bits leaderboards are disabled, don't send events
	if !models.GetIsCheerLeaderboardEnabled(ch) {
		return nil
	}

	updatedEntry := msg.EntryContext.Entry
	var diffScoreBelow int64
	var diffScoreAbove int64

	// TODO: Pantheon is emitting updates with score = 0 when a person who has not cheered is banned
	// https://jira.twitch.com/browse/BITS-2925
	// As a bandaid fix, we'll ignore these until this bug is fixed
	if updatedEntry.Score == 0 {
		return nil
	}

	for i, entry := range msg.EntryContext.Context {
		if entry == updatedEntry {
			if i > 0 {
				entryAbove := msg.EntryContext.Context[i-1]
				diffScoreAbove = math_utils.Int64Abs(int64(entryAbove.Score) - int64(updatedEntry.Score))
			}

			if i < len(msg.EntryContext.Context)-1 {
				entryBelow := msg.EntryContext.Context[i+1]
				diffScoreBelow = math_utils.Int64Abs(int64(entryBelow.Score) - int64(updatedEntry.Score))
			}
		}
	}

	updateEvent := msg.Event

	localTimeOfEvent := time.Unix(0, updateEvent.TimeOfEvent)
	utcTimeOfEvent := localTimeOfEvent.UTC()

	bitsLeaderboardUpdate := &models.BitsLeaderboardUpdate{
		UserID:                              updatedEntry.EntryKey,
		RankAfter:                           updatedEntry.Rank,
		DiffScoreAboveAfter:                 diffScoreAbove,
		DiffScoreBelowAfter:                 diffScoreBelow,
		ScoreAfter:                          updatedEntry.Score,
		UsedTotal:                           updateEvent.EventValue,
		TransactionID:                       updateEvent.ID,
		Time:                                localTimeOfEvent,
		TimeUTC:                             utcTimeOfEvent,
		ChannelID:                           channelID,
		EventLeaderboardTimePeriod:          msg.Identifier.TimeAggregationUnit,
		ChannelSettingLeaderboardTimePeriod: pointers.StringOrDefault(ch.LeaderboardTimePeriod, models.DefaultLeaderboardTimePeriod),
	}

	err = o.DataScience.TrackBitsEvent(ctx, datascience.BitsLeaderboardUpdate, bitsLeaderboardUpdate)
	if err != nil {
		log.WithError(err).Error("unable to send bits leaderboard update datascience")
		return err
	}

	return nil
}
