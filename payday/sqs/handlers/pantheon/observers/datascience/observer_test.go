package datascience

import (
	"context"
	"testing"
	"time"

	"code.justin.tv/commerce/payday/utils/pointers"

	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/errors"
	channel_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/cache/channel"
	datascience_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/datascience"
	"code.justin.tv/commerce/payday/models"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

const (
	entryUserID       = "123123123"
	testDomain        = "test"
	testGroupingKey   = "testChannel"
	testLeaderboardID = "test.testChannel.weekly"
)

func TestObserver_Observe(t *testing.T) {
	Convey("For an observer reporting datascience", t, func() {
		channelManagerMock := new(channel_mock.ChannelManager)
		datascienceMock := new(datascience_mock.DataScience)

		observer := Observer{
			ChannelManager: channelManagerMock,
			DataScience:    datascienceMock,
		}

		entry := models.PantheonSNSLeaderboardEntry{
			EntryKey: entryUserID,
			Rank:     42,
			Score:    322,
		}

		eventContext := []models.PantheonSNSLeaderboardEntry{
			{
				EntryKey: "above-user",
				Rank:     41,
				Score:    333,
			},
			{
				EntryKey: entryUserID,
				Rank:     42,
				Score:    322,
			},
			{
				EntryKey: "below-user",
				Rank:     43,
				Score:    311,
			},
		}

		entryContext := models.PantheonSNSLeaderboardEntryContext{
			Entry:   entry,
			Context: eventContext,
		}

		identifier := models.PantheonSNSLeaderboardIdentifier{
			Domain:              testDomain,
			GroupingKey:         testGroupingKey,
			TimeAggregationUnit: "weekly",
			TimeBucket:          "foo",
		}

		event := models.PantheonSNSLeaderboardEvent{
			Domain:      testDomain,
			GroupingKey: testGroupingKey,
			EntryKey:    entryUserID,
			TimeOfEvent: time.Date(2018, 3, 22, 3, 2, 2, 0, time.UTC).Unix(),
			EventValue:  20,
			ID:          testLeaderboardID,
		}

		message := models.PantheonSNSMessage{
			Identifier:   identifier,
			EntryContext: entryContext,
			Event:        event,
		}

		ctx := context.Background()

		Convey("Errors when channel manager errors", func() {
			channelManagerMock.On("Get", mock.Anything, mock.Anything).Return(nil, errors.New("test error"))
			err := observer.Observe(ctx, message)
			So(err, ShouldNotBeNil)
		})

		Convey("nil when channel manager returns nil", func() {
			channelManagerMock.On("Get", mock.Anything, mock.Anything).Return(nil, nil)
			err := observer.Observe(ctx, message)
			So(err, ShouldBeNil)
		})

		Convey("nil when cheering leaderboards disabled", func() {
			channelManagerMock.On("Get", mock.Anything, mock.Anything).Return(&dynamo.Channel{
				IsCheerLeaderboardEnabled: pointers.BoolP(false),
			}, nil)
			err := observer.Observe(ctx, message)
			So(err, ShouldBeNil)
		})

		Convey("When channel manager returns a channel", func() {
			channelManagerMock.On("Get", mock.Anything, mock.Anything).Return(&dynamo.Channel{
				Id:                        dynamo.ChannelId(testGroupingKey),
				IsCheerLeaderboardEnabled: pointers.BoolP(true),
			}, nil)

			Convey("works with cheerer above and below", func() {
				datascienceMock.On("TrackBitsEvent", mock.Anything, mock.Anything, mock.Anything).Return(nil)

				err := observer.Observe(ctx, message)
				So(err, ShouldBeNil)
			})

			Convey("works with only cheerer above", func() {
				datascienceMock.On("TrackBitsEvent", mock.Anything, mock.Anything, mock.Anything).Return(nil)

				eventContext := []models.PantheonSNSLeaderboardEntry{
					{
						EntryKey: "above-user",
						Rank:     41,
						Score:    333,
					},
					{
						EntryKey: entryUserID,
						Rank:     42,
						Score:    322,
					},
				}

				message.EntryContext.Context = eventContext

				err := observer.Observe(ctx, message)
				So(err, ShouldBeNil)
			})

			Convey("works with only cheerer below", func() {
				datascienceMock.On("TrackBitsEvent", mock.Anything, mock.Anything, mock.Anything).Return(nil)

				eventContext := []models.PantheonSNSLeaderboardEntry{
					{
						EntryKey: entryUserID,
						Rank:     42,
						Score:    322,
					},
					{
						EntryKey: "below-user",
						Rank:     43,
						Score:    311,
					},
				}

				message.EntryContext.Context = eventContext

				err := observer.Observe(ctx, message)
				So(err, ShouldBeNil)
			})

			Convey("does not send if score is 0", func() {
				message.EntryContext.Entry = models.PantheonSNSLeaderboardEntry{
					EntryKey: entryUserID,
					Rank:     1,
					Score:    0,
				}

				err := observer.Observe(ctx, message)
				datascienceMock.AssertNotCalled(t, "TrackBitsEvent", mock.Anything, mock.Anything, mock.Anything)
				So(err, ShouldBeNil)
			})
		})
	})
}
