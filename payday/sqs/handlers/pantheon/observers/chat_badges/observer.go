package chat_badges

import (
	"context"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	"code.justin.tv/commerce/payday/cache/channel"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/leaderboard"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/utils/pointers"
)

type Observer struct {
	ChannelManager             channel.ChannelManager      `inject:""`
	LeaderboardBadgeReconciler leaderboard.BadgeReconciler `inject:""`
}

func (o *Observer) Observe(ctx context.Context, msg models.PantheonSNSMessage) error {
	ch, err := o.ChannelManager.Get(ctx, msg.Identifier.GroupingKey)
	if err != nil {
		return err
	}

	if ch == nil {
		logrus.WithField("channelID", msg.Identifier.GroupingKey).Warn("no dynamo channel record found while processing chat badges pantheon event observer")
		return nil
	}

	if !models.GetIsCheerLeaderboardEnabled(ch) {
		return nil
	}

	lbTimePeriod := pointers.StringOrDefault(ch.LeaderboardTimePeriod, models.DefaultLeaderboardTimePeriod)

	timeUnitInt, isPresent := pantheonrpc.TimeUnit_value[lbTimePeriod]
	if !isPresent {
		return errors.Newf("channel (%s) set to use invalid time period: %s", msg.Identifier.GroupingKey, lbTimePeriod)
	}

	timeUnit := pantheonrpc.TimeUnit(timeUnitInt)

	if msg.Identifier.TimeAggregationUnit != timeUnit.String() {
		return nil
	}

	channelID := string(msg.Identifier.GroupingKey)

	newBadgeHolders := make(leaderboard.BadgeHolders, 0)
	for _, msg := range msg.Top {
		if msg.Rank <= 3 {
			newBadgeHolders = append(newBadgeHolders, leaderboard.LeaderboardBadge{
				UserID:        msg.EntryKey,
				BadgePosition: msg.Rank,
			})
		}
		if len(newBadgeHolders) == 3 {
			break
		}
	}

	err = o.LeaderboardBadgeReconciler.ReconcileBadges(ctx, channelID, timeUnit, newBadgeHolders, false)
	if err != nil {
		return errors.Notef(err, "error reconciling badges based on new pantheon update for channel: %s", channelID)
	}

	return nil
}
