package chat_badges

import (
	"context"
	"testing"

	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	"code.justin.tv/commerce/payday/clients/pantheon"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/leaderboard"
	channel_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/cache/channel"
	leaderboard_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/leaderboard"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/utils/pointers"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

const (
	testLeaderboardChannelID = "123123123123"
)

func TestObserver_Observe(t *testing.T) {
	Convey("For an observer entitling chat badges", t, func() {
		channelManager := new(channel_mock.ChannelManager)
		lbBadgeReconciler := new(leaderboard_mock.BadgeReconciler)
		observer := Observer{
			LeaderboardBadgeReconciler: lbBadgeReconciler,
			ChannelManager:             channelManager,
		}

		ctx := context.Background()

		lbIdentifier := models.PantheonSNSLeaderboardIdentifier{
			Domain:              pantheon.PantheonBitsUsageDomain,
			GroupingKey:         testLeaderboardChannelID,
			TimeAggregationUnit: pantheonrpc.TimeUnit_WEEK.String(),
		}
		input := models.PantheonSNSMessage{
			Identifier: lbIdentifier,
			Top:        []models.PantheonSNSLeaderboardEntry{},
		}

		Convey("errors when failing to get from dynamo", func() {
			channelManager.On("Get", mock.Anything, mock.Anything).Return(nil, errors.New("test error"))

			err := observer.Observe(ctx, input)
			So(err, ShouldNotBeNil)
		})

		Convey("nil when getting nil channel from dynamo", func() {
			channelManager.On("Get", mock.Anything, mock.Anything).Return(nil, nil)

			err := observer.Observe(ctx, input)
			So(err, ShouldBeNil)
		})

		Convey("exits early when dynamo is set to have lbs disabled", func() {
			channelManager.On("Get", mock.Anything, mock.Anything).Return(&dynamo.Channel{
				LeaderboardEnabled:        pointers.BoolP(false),
				IsCheerLeaderboardEnabled: pointers.BoolP(false),
			}, nil)

			err := observer.Observe(ctx, input)
			So(err, ShouldBeNil)
			So(len(lbBadgeReconciler.Calls), ShouldEqual, 0)
		})

		Convey("exits early when dynamo is set to a different lb time period", func() {
			channelManager.On("Get", mock.Anything, mock.Anything).Return(&dynamo.Channel{
				LeaderboardEnabled:    pointers.BoolP(true),
				LeaderboardTimePeriod: pointers.StringP(pantheonrpc.TimeUnit_ALLTIME.String()),
			}, nil)

			err := observer.Observe(ctx, input)
			So(err, ShouldBeNil)
			So(len(lbBadgeReconciler.Calls), ShouldEqual, 0)
		})

		Convey("when dynamo returns a channel", func() {
			channelManager.On("Get", mock.Anything, mock.Anything).Return(&dynamo.Channel{
				LeaderboardEnabled:    pointers.BoolP(true),
				LeaderboardTimePeriod: pointers.StringP(pantheonrpc.TimeUnit_WEEK.String()),
			}, nil)

			Convey("errors when badge reconciler errors", func() {
				lbBadgeReconciler.On("ReconcileBadges", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(errors.New("test error"))
				err := observer.Observe(ctx, input)
				So(err, ShouldNotBeNil)
			})

			Convey("calls badge reconciler with empty array when top is empty", func() {
				lbBadgeReconciler.On("ReconcileBadges", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)
				err := observer.Observe(ctx, input)
				So(err, ShouldBeNil)
				newBadgeHolders := lbBadgeReconciler.Calls[0].Arguments[3].(leaderboard.BadgeHolders)
				So(len(newBadgeHolders), ShouldEqual, 0)
			})

			Convey("calls badge reconciler with one element when top only has one element", func() {
				lbBadgeReconciler.On("ReconcileBadges", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)
				input.Top = []models.PantheonSNSLeaderboardEntry{
					{Rank: 1, Score: 10, EntryKey: "walrus"},
				}
				err := observer.Observe(ctx, input)
				So(err, ShouldBeNil)
				newBadgeHolders := lbBadgeReconciler.Calls[0].Arguments[3].(leaderboard.BadgeHolders)
				So(len(newBadgeHolders), ShouldEqual, 1)
				So(newBadgeHolders[0].BadgePosition, ShouldEqual, 1)
				So(newBadgeHolders[0].UserID, ShouldEqual, "walrus")
			})

			Convey("calls badge reconciler with three elements when top has more than three", func() {
				lbBadgeReconciler.On("ReconcileBadges", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)
				input.Top = []models.PantheonSNSLeaderboardEntry{
					{Rank: 1, Score: 10, EntryKey: "walrus"},
					{Rank: 2, Score: 9, EntryKey: "not-as-good-walrus"},
					{Rank: 3, Score: 8, EntryKey: "even-less-good-walrus"},
					{Rank: 4, Score: 7, EntryKey: "yet-even-worse-walrus"},
				}
				err := observer.Observe(ctx, input)
				So(err, ShouldBeNil)
				newBadgeHolders := lbBadgeReconciler.Calls[0].Arguments[3].(leaderboard.BadgeHolders)
				So(len(newBadgeHolders), ShouldEqual, 3)
				So(newBadgeHolders[0].BadgePosition, ShouldEqual, 1)
				So(newBadgeHolders[0].UserID, ShouldEqual, "walrus")
				So(newBadgeHolders[1].BadgePosition, ShouldEqual, 2)
				So(newBadgeHolders[1].UserID, ShouldEqual, "not-as-good-walrus")
				So(newBadgeHolders[2].BadgePosition, ShouldEqual, 3)
				So(newBadgeHolders[2].UserID, ShouldEqual, "even-less-good-walrus")
			})
		})

		Convey("when dynamo returns a channel with IsCheerLeaderboardEnabled or IsSubGiftLeaderboardEnabled set", func() {
			channelManager.On("Get", mock.Anything, mock.Anything).Return(&dynamo.Channel{
				LeaderboardEnabled:        pointers.BoolP(false),
				IsCheerLeaderboardEnabled: pointers.BoolP(true),
				LeaderboardTimePeriod:     pointers.StringP(pantheonrpc.TimeUnit_WEEK.String()),
			}, nil)

			Convey("calls badge reconciler with empty array when top is empty", func() {
				lbBadgeReconciler.On("ReconcileBadges", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)
				err := observer.Observe(ctx, input)
				So(err, ShouldBeNil)
				newBadgeHolders := lbBadgeReconciler.Calls[0].Arguments[3].(leaderboard.BadgeHolders)
				So(len(newBadgeHolders), ShouldEqual, 0)
			})
		})
	})
}
