package pushy

import (
	"context"
	"fmt"
	"strconv"
	"strings"

	dart "code.justin.tv/amzn/TwitchDartReceiverTwirp"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/lock"
	"code.justin.tv/commerce/payday/metrics"
	paydaymodel "code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/utils/pointers"
	pStrings "code.justin.tv/commerce/payday/utils/strings"
)

const (
	RedisLockKeyPrefix          = "pushy-redis-lock"
	bitsGrantedNotificationType = "bits_granted_notification"

	rpgGroupOrigin      = "rpg_group"
	studentGroupOrigin  = "student_program"
	twitchSupportOrigin = "twitch_support"
	twitchOrigin        = "twitch"

	oldStudentGroupText  = "the Twitch Student Program"
	oldRpgGroupText      = "the Twitch Research Power Group"
	oldTestGroupText     = "the Test Bits Delivery Service"
	oldTwitchSupportText = "Twitch Support"
)

// Historically, this used to be the Pushy notification handler but we migrated over time.
type DARTNotificationHandler struct {
	DartReceiver    dart.Receiver           `inject:""`
	TransactionsDao dynamo.ITransactionsDao `inject:""`
	Statter         metrics.Statter         `inject:""`
	LockingClient   lock.LockingClient      `inject:""`
}

func (h *DARTNotificationHandler) HandleGiveBitsToBroadcaster(ctx context.Context, msg *paydaymodel.GiveBitsToBroadcasterMessage) error {
	return nil
}

func (h *DARTNotificationHandler) HandleAddBitsToCustomerAccount(ctx context.Context, msg *paydaymodel.AddBitsToCustomerAccountMessage) error {
	// This flow only works for AdminAddBits
	if msg.AdminID == "" {
		return nil
	}

	transactionLock, err := h.LockingClient.ObtainLock(fmt.Sprintf("%s-%s", RedisLockKeyPrefix, msg.TransactionId))
	if err != nil {
		log.Error(err)
		return err
	}
	defer func() {
		err = transactionLock.Unlock()
		if err != nil {
			log.Error(err)
		}
	}()

	transaction, err := h.TransactionsDao.Get(msg.TransactionId)
	if err != nil {
		return err
	}

	if transaction == nil {
		errMsg := "Received Bits Usage Event with no associated Transaction Dynamo entry"
		log.WithField("transactionId", msg.TransactionId).Warn(errMsg)
		return errors.New(errMsg)
	}

	if !pointers.BoolOrDefault(transaction.NotifyUser, false) {
		return nil
	}

	if pointers.BoolOrDefault(transaction.NotifiedUser, false) {
		h.Statter.Inc("SkippedPushyNotificationDedupe", 1)
		return nil
	}

	if pStrings.BlankP(transaction.Origin) {
		errMsg := "Received bits usage event to notify user with but origin field is empty"
		log.WithField("transactionId", msg.TransactionId).Error(errMsg)
		return errors.New(errMsg)
	}

	log.WithField("transactionId", msg.TransactionId).WithField("userId", msg.TargetTwitchUserId).Info("Publishing BitsGrantEvent notification to user")

	err = h.craftAndSendNotification(ctx, msg.TargetTwitchUserId, *transaction.Origin, msg.TransactionId, strconv.FormatInt(int64(msg.TotalBits), 10))
	if err != nil {
		return err
	}

	transaction.NotifiedUser = pointers.BoolP(true)
	err = h.TransactionsDao.Update(transaction)
	if err != nil {
		return err
	}

	h.Statter.Inc("SentPushyNotification", 1)

	return nil
}

func (h *DARTNotificationHandler) HandleRemoveBitsFromCustomerAccount(ctx context.Context, msg *paydaymodel.RemoveBitsFromCustomerAccountMessage) error {
	return nil
}

func (h *DARTNotificationHandler) HandleUseBitsOnExtension(ctx context.Context, msg *paydaymodel.UseBitsOnExtensionMessage) error {
	return nil
}

func (h *DARTNotificationHandler) HandleUseBitsOnPoll(ctx context.Context, msg *paydaymodel.UseBitsOnPollMessage) error {
	return nil
}

func (h *DARTNotificationHandler) HandleCreateHold(ctx context.Context, msg *paydaymodel.CreateHoldMessage) error {
	return nil
}

func (h *DARTNotificationHandler) HandleFinalizeHold(ctx context.Context, msg *paydaymodel.FinalizeHoldMessage) error {
	return nil
}

func (h *DARTNotificationHandler) HandleReleaseHold(ctx context.Context, msg *paydaymodel.ReleaseHoldMessage) error {
	return nil
}

func (h *DARTNotificationHandler) craftAndSendNotification(ctx context.Context, userID, origin, transactionID, totalBits string) error {
	switch origin {
	case oldStudentGroupText, studentGroupOrigin:
		origin = studentGroupOrigin
	case rpgGroupOrigin, oldRpgGroupText:
		origin = rpgGroupOrigin
	case oldTwitchSupportText:
		origin = twitchSupportOrigin
	case twitchOrigin:
		break
	case oldTestGroupText:
		//nothing to publish
		return nil
	default:
		toLowerOrigin := strings.ToLower(origin)
		if strings.Contains(toLowerOrigin, "student") {
			log.Warnf("Substituting origin: %s for origin: %s in admin grant dart notification", origin, studentGroupOrigin)
			origin = studentGroupOrigin
			break
		}
		if strings.Contains(toLowerOrigin, "research power group") {
			log.Warnf("Substituting origin: %s for origin: %s in admin grant dart notification", origin, rpgGroupOrigin)
			origin = rpgGroupOrigin
			break
		}

		log.WithField("transactionId", transactionID).Error(fmt.Sprintf("Received bits usage event to notify user with but origin %s was not matched. Falling back to Twitch origin type", origin))
		origin = twitchOrigin
	}

	metadata := make(map[string]string)
	metadata["bits_sources"] = origin
	metadata["num_bits"] = totalBits

	request := &dart.PublishNotificationRequest{
		Recipient:                      &dart.PublishNotificationRequest_RecipientId{RecipientId: userID},
		NotificationType:               bitsGrantedNotificationType,
		Metadata:                       metadata,
		DropNotificationBeforeDelivery: false,
	}
	resp, err := h.DartReceiver.PublishNotification(ctx, request)
	if err != nil {
		log.WithError(err).Errorf("Publishing notification for admin grant notification type: %s", origin)
		return err
	}
	log.Infof("Successfully published admin grant %s notification, tracking id: %s", bitsGrantedNotificationType, resp.NotificationTraceId)

	return nil
}
