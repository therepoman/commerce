package uploadserviceSQS

import (
	"context"
	"encoding/json"
	"time"

	pubclient "code.justin.tv/chat/pubsub-go-pubclient/client"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/clients/uploadservice"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/image"
	"code.justin.tv/commerce/payday/metrics"
	"code.justin.tv/commerce/payday/models"
	uploadmodel "code.justin.tv/web/upload-service/models"
	"code.justin.tv/web/upload-service/rpc/uploader"
	"github.com/aws/aws-sdk-go/service/sqs"
)

type UploadServiceStatusHandler struct {
	ImageManager  image.IImageManager          `inject:""`
	Statter       metrics.Statter              `inject:""`
	UploadService uploadservice.IImageUploader `inject:""`
	PubSub        pubclient.PubClient          `inject:""`
}

func (h *UploadServiceStatusHandler) notifyCallBack(ctx context.Context, pubsubMsg uploadmodel.SNSCallback, status uploader.Status) error {
	topics := []string{"upload" + "." + pubsubMsg.UploadID}
	pubsubMsg.Status = int64(status)
	pubsubMessage, error := json.Marshal(pubsubMsg)
	if error != nil {
		return errors.Notef(error, "Error while marshaling Upload Service Pusbub message")
	}

	error = h.PubSub.Publish(ctx, topics, string(pubsubMessage), nil)
	if error != nil {
		return errors.Notef(error, "Error publishing message to pubsub")
	}

	return nil
}

func (h *UploadServiceStatusHandler) HandleMessage(ctx context.Context, message *sqs.Message) (outErr error) {
	startTime := time.Now()

	defer func() {
		var success int64
		if outErr != nil {
			success = 0
		} else {
			success = 100
			h.Statter.TimingDuration("ProcessUploadServiceStatus", time.Since(startTime))
		}
		h.Statter.Inc("ProcessUploadServiceStatusSuccess", success)
	}()

	var sns models.SNSMessage
	err := json.Unmarshal([]byte(*message.Body), &sns)
	if err != nil {
		msg := "Unable to covert to SNS message struct in uploadService SQS handler"
		log.WithError(err).Error(msg)
		return errors.Notef(err, msg)
	}

	var uploadServiceMsg uploadmodel.SNSCallback
	err = json.Unmarshal([]byte(sns.Message), &uploadServiceMsg)
	if err != nil {
		msg := "Unable to covert to UploadService msg struct in uploadService SQS handler"
		log.WithError(err).Error(msg)
		return errors.Notef(err, msg)
	}

	if uploader.Status(uploadServiceMsg.Status) == uploader.Status_POSTPROCESS_COMPLETE {
		statusReq := &uploader.SetStatusRequest{
			UploadId: uploadServiceMsg.UploadID,
		}

		err = h.ImageManager.ProcessUploadService(ctx, uploadServiceMsg)
		if err != nil {
			msg := "Unable to process upload service status"
			log.WithFields(log.Fields{
				"UploadID": uploadServiceMsg.UploadID,
			}).WithError(err).Error(msg)

			statusReq.Status = uploader.Status_FEATURE_SERVICE_FAILED
			_, statusErr := h.UploadService.GetUploader().SetStatus(ctx, statusReq)
			if statusErr != nil {
				log.WithError(statusErr).Error("Error getting upload status from Upload Service")
			}

			err = h.notifyCallBack(ctx, uploadServiceMsg, uploader.Status_FEATURE_SERVICE_FAILED)
			return errors.Notef(err, msg)
		}

		statusReq.Status = uploader.Status_COMPLETE
		_, setStatusErr := h.UploadService.GetUploader().SetStatus(ctx, statusReq)
		if setStatusErr != nil {
			return setStatusErr
		}

		err = h.notifyCallBack(ctx, uploadServiceMsg, uploader.Status_COMPLETE)
		if err != nil {
			return err
		}
	}

	return nil
}
