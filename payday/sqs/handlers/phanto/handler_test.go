package phanto

import (
	"context"
	"encoding/json"
	"testing"

	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/gogogadget/random"
	entitle_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/entitle"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/models/api"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
)

func testSQSMessage(userID string) (*sqs.Message, *models.PhantoSNSMessage) {
	phantoMsg := &models.PhantoSNSMessage{
		UserID:  userID,
		ClaimID: "WALRUS_CLAIM",
		SKU:     "WALRUS_SKU",
	}
	phantoMsgBytes, _ := json.Marshal(phantoMsg)
	snsMsg := &models.SNSMessage{
		MessageAttributes: map[string]models.SNSMessageAttribute{},
		Message:           string(phantoMsgBytes),
	}
	snsJsonBytes, _ := json.Marshal(snsMsg)
	return &sqs.Message{
		Body: pointers.StringP(string(snsJsonBytes)),
	}, phantoMsg
}

func TestPhantoHandler_Handle(t *testing.T) {
	Convey("given a phanto SQS processor", t, func() {
		entitler := new(entitle_mock.Parallel)

		handler := PhantoHandler{
			Entitler: entitler,
		}

		ctx := context.Background()

		Convey("when the message is not a valid SNS message", func() {
			msg := &sqs.Message{
				Body: aws.String("WALRUS STRIKE"),
			}

			err := handler.Handle(ctx, msg)
			So(err, ShouldNotBeNil)
		})

		Convey("when we have a valid SNS message, but not a valid Phanto message", func() {
			snsMsg := &models.SNSMessage{
				Message: "WALRUS STRIKE",
			}
			snsJson, _ := json.Marshal(snsMsg)
			msg := &sqs.Message{
				Body: aws.String(string(snsJson)),
			}

			err := handler.Handle(ctx, msg)
			So(err, ShouldNotBeNil)
		})

		Convey("when we have a valid message", func() {
			userID := random.String(16)

			msg, phantoMsg := testSQSMessage(userID)

			Convey("when the entitler fails", func() {
				entitler.On("Entitle", ctx, api.EntitleBitsRequest{
					TwitchUserId:  phantoMsg.UserID,
					TransactionId: phantoMsg.ClaimID,
					ProductId:     phantoMsg.SKU,
					Platform:      string(models.PHANTO),
				}).Return(int32(0), errors.New("WALRUS STRIKE"))

				err := handler.Handle(ctx, msg)
				So(err, ShouldNotBeNil)
			})

			Convey("when the entitler succeeds", func() {
				entitler.On("Entitle", ctx, api.EntitleBitsRequest{
					TwitchUserId:  phantoMsg.UserID,
					TransactionId: phantoMsg.ClaimID,
					ProductId:     phantoMsg.SKU,
					Platform:      string(models.PHANTO),
				}).Return(int32(322), nil)

				err := handler.Handle(ctx, msg)
				So(err, ShouldBeNil)
			})
		})
	})
}
