package phanto

import (
	"context"
	"encoding/json"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/entitle"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/models/api"
	"github.com/aws/aws-sdk-go/service/sqs"
)

type PhantoHandler struct {
	Entitler entitle.Parallel `inject:"parallelEntitler"`
}

func (h *PhantoHandler) Handle(ctx context.Context, message *sqs.Message) error {
	var redeemKeyEvent models.PhantoSNSMessage

	var snsMsg models.SNSMessage
	err := json.Unmarshal([]byte(*message.Body), &snsMsg)
	if err != nil {
		msg := "PhantoHandler: Unable to unmarshal SNS message"
		log.WithError(err).Error(msg)
		return errors.Notef(err, msg)
	}

	err = json.Unmarshal([]byte(snsMsg.Message), &redeemKeyEvent)
	if err != nil {
		msg := "PhantoHandler: Unable to unmarshal SNS message to PhantoSNSMessage"
		log.WithError(err).Error(msg)
		return errors.Notef(err, msg)
	}

	_, err = h.Entitler.Entitle(ctx, api.EntitleBitsRequest{
		TwitchUserId:  redeemKeyEvent.UserID,
		TransactionId: redeemKeyEvent.ClaimID,
		ProductId:     redeemKeyEvent.SKU,
		Platform:      string(models.PHANTO),
	})

	if err != nil {
		msg := "PhantoHandler: Entitlement failed"
		log.WithError(err).Error(msg)
		return errors.Notef(err, msg)
	}

	return nil
}
