package bigbitsSQS

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/clients/slack"
	"code.justin.tv/commerce/payday/clips"
	"code.justin.tv/commerce/payday/message"
	"code.justin.tv/commerce/payday/metrics"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/partner"
	"code.justin.tv/commerce/payday/sns"
	"code.justin.tv/commerce/payday/userservice"
	contextUtils "code.justin.tv/commerce/payday/utils/context"
	"code.justin.tv/commerce/payday/utils/strings"
	"github.com/go-errors/errors"
)

const (
	notificationThreshold = 100000
	bitsThreshold         = 10000
	bigBitsSnsTopic       = "arn:aws:sns:us-west-2:021561903526:BigBits"
	numClipsToCreate      = 3
)

type BigBitsHandler struct {
	SNSClient          sns.ISNSClient            `inject:"usWestSNSClient"`
	UserServiceFetcher userservice.Fetcher       `inject:""`
	ClipsManager       *clips.ClipsManager       `inject:""`
	Slacker            slack.ISlacker            `inject:""`
	Statter            metrics.Statter           `inject:""`
	TokenizerFactory   message.ITokenizerFactory `inject:""`
	SlackWebhookURL    string                    `inject:"bigBitsSlackWebhookUrl"`
}

type BigBitsMsg struct {
	Default     string    `json:"default"`
	ChannelId   string    `json:"channel_id"`
	ChannelName string    `json:"channel_name"`
	UserId      string    `json:"user_id"`
	UserName    string    `json:"user_name"`
	IsAnonymous bool      `json:"is_anonymous"`
	BitsAmount  int       `json:"bits_amount"`
	Message     string    `json:"message"`
	Timestamp   time.Time `json:"timestamp"`
}

type createdClip struct {
	URL string
	err error
}

func getClipUrlFromChan(ctx context.Context, createdClipChan chan createdClip) (string, error) {
	var clipNum int
	for !contextUtils.TimedOut(ctx) {
		select {
		case createdClip := <-createdClipChan:
			clipNum++
			if createdClip.err != nil {
				if clipNum == numClipsToCreate {
					log.Warnf("Error creating clip after all attempts: %v", createdClip.err)
					return "", createdClip.err
				}
			} else {
				if createdClip.URL == "" {
					return "_Received a blank URL_", nil
				}

				return createdClip.URL, nil
			}
		default:
		}
	}

	msg := "Timed out waiting for clip in channel"
	log.Warn(msg)
	return "", errors.New(msg)
}

func (h *BigBitsHandler) HandleGiveBitsToBroadcaster(ctx context.Context, msg *models.GiveBitsToBroadcasterMessage) error {
	bitsAmount := -1 * msg.BitsSent
	if bitsAmount >= bitsThreshold {

		bbm := h.newBigBitsMsgJson(ctx, msg)

		marshalled, err := json.Marshal(bbm)
		if err != nil {
			log.WithError(err).Warn("Error converting big bits msg to JSON")
			return nil
		}

		bigBitsJSON := string(marshalled)

		timeToClip := msg.TimeOfEvent.Add(time.Second * 23)

		if time.Now().Before(timeToClip) {
			sleepTime := time.Until(timeToClip)
			signal := time.After(sleepTime)
			select {
			case <-ctx.Done():
				msg := "Timed out while waiting for the future"
				log.Warn(msg)
				return errors.New(msg)
			case <-signal:
				//continue
			}
		}

		isAffiliate := false
		if msg != nil && strings.NotBlank(msg.ExtraContext) {
			var extraContext models.GiveBitsExtraContext
			err = json.Unmarshal([]byte(msg.ExtraContext), &extraContext)
			if err != nil {
				log.Warn("Error parsing extra context in big bits SQS", err)
			} else {
				isAffiliate = partner.PartnerType(extraContext.PartnerType) == partner.AffiliatePartnerType
			}
		}

		h.Statter.TimingDuration("ClipDelay", time.Since(msg.TimeOfEvent))

		createdClipChan := make(chan createdClip, numClipsToCreate)

		for i := 0; i < numClipsToCreate; i++ {
			go func(j int) {
				delta := time.Duration(j) * time.Second
				time.Sleep(delta)
				clipURL, err := h.ClipsManager.CreateClip(ctx, bbm.ChannelId)
				createdClip := createdClip{
					URL: clipURL,
					err: err,
				}
				createdClipChan <- createdClip
			}(i)
		}

		message, err := getClipUrlFromChan(ctx, createdClipChan)
		if err != nil {
			log.Warnf("Error getting clip url from chan: %v", err)
			return err
		}

		tokenizer, err := h.TokenizerFactory.NewTokenizer(ctx, bbm.ChannelId, bbm.UserId)
		if err != nil {
			return err
		}

		_, _, emoteTotals, err := tokenizer.Tokenize(bbm.Message)
		if err != nil {
			log.Warnf("Error tokenizing big bits message: %s , %v", bbm.Message, err)
		}

		notLikeThisTotal := emoteTotals["notlikethis"]
		if notLikeThisTotal >= 10000 {
			benswStr := "@bensw - Big Ben bits alert! :notlikethis10000: \n"
			benswStr += fmt.Sprintf(":notlikethis: bits total: %d ", notLikeThisTotal)
			numEmotes := int(notLikeThisTotal / 10000)
			for i := 0; i < numEmotes; i++ {
				benswStr += ":money_with_wings: "
			}
			message = benswStr + "\n" + message
		}

		pogchampTotal := emoteTotals["pogchamp"]
		if pogchampTotal >= 30000 {
			pogchampStr := "REALLY WIDE POGCHAMP ALERT :cheerpogchamp:"
			if pogchampTotal >= 90000 {
				pogchampStr += "\n @here Dark PogChamp Rises :darkpogchamp: :pogchamp10000: :darkpogchamp:"
			}
			message = pogchampStr + "\n" + message
		}

		if bitsAmount >= notificationThreshold {
			message = "@here - Really big bits alert! \n " + message
		}

		if bbm.IsAnonymous {
			message = ":bogashh: *Anonymous Cheer* :bogashh: \n" + message
		}

		if isAffiliate {
			message = ":unverified: Affiliate \n " + message
		} else {
			message = ":verified: Partner \n " + message
		}

		if msg.SponsoredAmounts != nil && len(msg.SponsoredAmounts) > 0 {
			message = "\n" + message
			for campaignID, amount := range msg.SponsoredAmounts {
				message = message + " Sponsored " + campaignID + " cheer of " + fmt.Sprintf("%d", amount) + " bits"
			}
		}

		err = h.Slacker.PostMsg(h.SlackWebhookURL, message)
		if err != nil {
			log.Warnf("Error posting to slack: %v", err)
			return err
		}

		err = h.SNSClient.PostToTopic(bigBitsSnsTopic, bigBitsJSON)
		if err != nil {
			log.WithError(err).Warn("Error publishing big bits json to SNS topic")
			return nil
		}

	}
	return nil
}

func (h *BigBitsHandler) HandleAddBitsToCustomerAccount(ctx context.Context, msg *models.AddBitsToCustomerAccountMessage) error {
	return nil
}

func (h *BigBitsHandler) HandleRemoveBitsFromCustomerAccount(ctx context.Context, msg *models.RemoveBitsFromCustomerAccountMessage) error {
	return nil
}

func (h *BigBitsHandler) HandleUseBitsOnExtension(ctx context.Context, msg *models.UseBitsOnExtensionMessage) error {
	return nil
}

func (h *BigBitsHandler) HandleUseBitsOnPoll(ctx context.Context, msg *models.UseBitsOnPollMessage) error {
	return nil
}

func (h *BigBitsHandler) newBigBitsMsgJson(ctx context.Context, msg *models.GiveBitsToBroadcasterMessage) BigBitsMsg {
	var userName, channelName string

	user, err := h.UserServiceFetcher.Fetch(ctx, msg.RequestingTwitchUserId)
	if user != nil && err == nil && user.Login != nil {
		userName = *user.Login
	}

	channel, err := h.UserServiceFetcher.Fetch(ctx, msg.TargetTwitchUserId)
	if channel != nil && err == nil && channel.Login != nil {
		channelName = *channel.Login
	}

	bitsAmount := -1 * msg.BitsSent

	defaultMessage := fmt.Sprintf("%s cheered %d in %s's channel", userName, bitsAmount, channelName)
	if msg.IsAnonymous {
		defaultMessage = fmt.Sprintf("An anonymous user cheered %d in %s's channel", bitsAmount, channelName)
	}

	bigBitsMsg := BigBitsMsg{
		Default:     defaultMessage,
		ChannelId:   msg.TargetTwitchUserId,
		ChannelName: channelName,
		UserId:      msg.RequestingTwitchUserId,
		UserName:    userName,
		IsAnonymous: msg.IsAnonymous,
		BitsAmount:  bitsAmount,
		Message:     msg.PublicMessage,
		Timestamp:   msg.TimeOfEvent,
	}
	return bigBitsMsg
}

func (h *BigBitsHandler) HandleCreateHold(ctx context.Context, msg *models.CreateHoldMessage) error {
	return nil
}

func (h *BigBitsHandler) HandleFinalizeHold(ctx context.Context, msg *models.FinalizeHoldMessage) error {
	return nil
}

func (h *BigBitsHandler) HandleReleaseHold(ctx context.Context, msg *models.ReleaseHoldMessage) error {
	return nil
}
