package autorefill

import (
	"context"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/refill"
)

type AutoResponseHandler struct {
	Refill refill.Refill `inject:""`
}

func (h *AutoResponseHandler) HandleGiveBitsToBroadcaster(parent context.Context, msg *models.GiveBitsToBroadcasterMessage) error {
	err := h.Refill.CheckForProfileAndSendNotificationIfNeeded(parent, msg.RequestingTwitchUserId)
	if err != nil {
		log.WithError(err).Warn("Issue attempting to carry out auto refill process")
		return err
	}
	return nil
}

func (h *AutoResponseHandler) HandleAddBitsToCustomerAccount(ctx context.Context, msg *models.AddBitsToCustomerAccountMessage) error {
	return nil
}

func (h *AutoResponseHandler) HandleRemoveBitsFromCustomerAccount(ctx context.Context, msg *models.RemoveBitsFromCustomerAccountMessage) error {
	return nil
}

func (h *AutoResponseHandler) HandleUseBitsOnExtension(ctx context.Context, msg *models.UseBitsOnExtensionMessage) error {
	err := h.Refill.CheckForProfileAndSendNotificationIfNeeded(ctx, msg.RequestingTwitchUserId)
	if err != nil {
		log.WithError(err).Warn("Issue attempting to carry out auto refill process")
		return err
	}
	return nil
}

func (h *AutoResponseHandler) HandleUseBitsOnPoll(ctx context.Context, msg *models.UseBitsOnPollMessage) error {
	err := h.Refill.CheckForProfileAndSendNotificationIfNeeded(ctx, msg.RequestingTwitchUserId)
	if err != nil {
		log.WithError(err).Warn("Issue attempting to carry out auto refill process")
		return err
	}
	return nil
}

func (h *AutoResponseHandler) HandleCreateHold(ctx context.Context, msg *models.CreateHoldMessage) error {
	// we don't care about holds
	return nil
}

func (h *AutoResponseHandler) HandleFinalizeHold(ctx context.Context, msg *models.FinalizeHoldMessage) error {
	err := h.Refill.CheckForProfileAndSendNotificationIfNeeded(ctx, msg.RequestingTwitchUserId)
	if err != nil {
		log.WithError(err).Warn("Issue attempting to carry out auto refill process")
		return err
	}
	return nil
}

func (h *AutoResponseHandler) HandleReleaseHold(ctx context.Context, msg *models.ReleaseHoldMessage) error {
	// we don't care about holds here
	return nil
}
