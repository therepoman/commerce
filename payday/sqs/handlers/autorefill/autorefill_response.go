package autorefill

import (
	"context"
	"encoding/json"
	"time"

	"code.justin.tv/commerce/payday/metrics"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/clients/pachinko"
	"code.justin.tv/commerce/payday/datascience"
	"code.justin.tv/commerce/payday/dynamo/auto_refill_settings"
	"code.justin.tv/commerce/payday/errors"
	sns_model "code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/refill"
	"code.justin.tv/revenue/everdeen/models"
	"github.com/aws/aws-sdk-go/service/sqs"
)

const auto_refill_double_entitle_metric_name = "BitsAutoRefill.DoubleEntitle"

type EverdeenAutoRefillResponseHandler struct {
	Refill            refill.Refill                            `inject:""`
	DataScienceClient datascience.DataScience                  `inject:""`
	RefillRecordDao   auto_refill_settings.AutoRefillRecordDao `inject:""`
	PachinkoClient    pachinko.PachinkoClient                  `inject:""`
	Statter           metrics.Statter                          `inject:""`
}

func (a *EverdeenAutoRefillResponseHandler) HandleMessage(ctx context.Context, message *sqs.Message) error {
	if message == nil {
		msg := "Received nil SQS message in EverdeenAutoRefillResponseHandler"
		log.Error(msg)
		return errors.New(msg)
	}

	var sns sns_model.SNSMessage
	err := json.Unmarshal([]byte(*message.Body), &sns)
	if err != nil {
		msg := "Unable to covert to SNS message struct in EverdeenAutoRefillResponseHandler"
		log.WithError(err).Error(msg)
		return errors.Notef(err, msg)
	}

	var envelope models.PreauthPurchaseResponse
	err = json.Unmarshal([]byte(sns.Message), &envelope)
	if err != nil {
		msg := "Unable to covert to PreauthPurchaseResponse msg struct in EverdeenAutoRefillResponseHandler"
		log.WithError(err).Error(msg)
		return errors.Notef(err, msg)
	}

	log.Infof("Response message from everdeen obtained: %d %s %d %s", envelope.ReasonCode, envelope.UserID, envelope.Status, envelope.RequestID)

	// Get the profile and clean up the in progress request id if it matches.
	profile, err := a.Refill.Get(ctx, envelope.UserID)

	if err != nil {
		log.WithError(err).Warn("Issue getting auto refill profile processing auto refill success.")
		return err
	}

	if envelope.ReasonCode == models.ReasonSuccess.Value() {
		if profile.InProgressRequestID == envelope.RequestID {
			profile.InProgressRequestID = ""
		}

		attemptCount := profile.AttemptCount
		// reset attempt count now that we have a success.
		profile.AttemptCount = 0
		// reset timer to default value such that subsequent purchase checks will pass
		profile.InProgressRequestTime = time.Time{}
		_, _, err = a.Refill.SetWithoutNotification(ctx, *profile, "", false)

		if err != nil {
			log.WithError(err).Warn("Issue clearing InProgressRequestID on auto refill profile processing auto refill success.")
			return err
		}

		err = a.DataScienceClient.TrackBitsEvent(ctx, datascience.AutoRefillPurchaseSuccess, &sns_model.AutoRefillPurchaseCompleted{
			UserID:        profile.UserId,
			Quantity:      1,
			Product:       "BITS",
			ServerTime:    time.Now(),
			SKU:           profile.SKU,
			TransactionID: envelope.TransactionID,
			RequestID:     envelope.RequestID,
			AttemptNumber: int(attemptCount),
			OfferID:       profile.OfferId,
			BitType:       profile.BitsType,
		})

		if err != nil {
			log.WithError(err).Warn("Issue sending auto refill profile success data science.")
			return err
		}

		balance, err := a.PachinkoClient.GetBalance(ctx, profile.UserId)

		if err != nil {
			log.WithError(err).Warn("Issue getting balance for auto refill profile success data science.")
			return err
		}

		var statusNote string
		if int32(balance) > profile.Threshold+int32(profile.BitsQuantity) {
			statusNote = auto_refill_settings.StatusNoteDoubleEntitle
			log.Warnf("Possible Double entitle found for auto refill user: %s requestID: %s transactionID: %s", profile.UserId, envelope.RequestID, envelope.TransactionID)
			a.Statter.Inc(auto_refill_double_entitle_metric_name, 1)
		} else {
			statusNote = auto_refill_settings.StatusNoteNormal
		}

		err = a.RefillRecordDao.WriteRecord(&auto_refill_settings.AutoRefillPurchaseAttemptRecord{
			Threshold:     profile.Threshold,
			UserID:        profile.UserId,
			Status:        auto_refill_settings.StatusSuccess,
			BalanceAtTime: int32(balance),
			ProfileID:     profile.Id,
			Time:          time.Now().Format(time.RFC3339),
			StatusNote:    statusNote,
			RequestID:     envelope.RequestID,
			TransactionID: envelope.TransactionID,
			OfferID:       profile.OfferId,
		})

		if err != nil {
			log.WithError(err).Error("Error writing Refill audit entry to dynamodb.")
			return err
		}
	} else { // Failure cases
		log.Infof("[HandleEverdeenAsyncTwirpError] %+v", envelope)
		_ = a.DataScienceClient.TrackBitsEvent(ctx, datascience.AutoRefillPurchaseFailure, &sns_model.AutoRefillPurchaseFailure{
			UserID:        profile.UserId,
			Quantity:      1,
			Product:       "BITS",
			ServerTime:    time.Now(),
			SKU:           profile.SKU, //needs mapping to petozi sku
			TransactionID: "",          // failure so no transaction ID
			FailureType:   envelope.Message,
			RequestID:     profile.InProgressRequestID,
			AttemptNumber: int(profile.AttemptCount),
			OfferID:       profile.OfferId,
			BitType:       profile.BitsType,
		})

		switch envelope.ReasonCode {
		case models.ReasonTwoFADisabled.Value():
			// disable with notification from https://docs.google.com/document/d/1v2p6yEurquXmU21Uhzxbj1EC4V4PF_opH62QZCyf3sw
			_, err = a.Refill.DisableProfileAndRecordFailure(ctx, envelope.UserID, refill.DisabledReasonTwoFactor, envelope.TransactionID, envelope.RequestID)
			return err

		case models.ReasonEntitleFailed.Value():
			fallthrough
		case models.ReasonSystemError.Value():
			err := a.Refill.CheckForProfileAndSendNotificationIfNeeded(ctx, envelope.UserID)

			if err != nil {
				log.WithError(err).Errorf("Error attempting to send auto refill notification for user %s", envelope.UserID)
				return err
			}

		case models.ReasonFraudDeclined.Value():
			// maybe add ds here?
			fallthrough
		case models.ReasonCardDeclined.Value():
			// disable with notification from https://docs.google.com/document/d/1v2p6yEurquXmU21Uhzxbj1EC4V4PF_opH62QZCyf3sw
			_, err := a.Refill.DisableProfileAndRecordFailure(ctx, envelope.UserID, refill.DisabledReasonCardDeclined, envelope.TransactionID, envelope.RequestID)
			return err

		case models.ReasonVelocityLimit.Value():
			// disable temporarily with notification from https://docs.google.com/document/d/1v2p6yEurquXmU21Uhzxbj1EC4V4PF_opH62QZCyf3sw
			profile, err := a.Refill.Get(ctx, envelope.UserID)
			if err != nil || profile == nil {
				log.WithError(err).Errorf("Error attempting to get auto refill profile for user %s", envelope.UserID)
			}
			_, err = a.Refill.HandleProfileHitPurchaseVelocity(ctx, *profile)
			return err

		case models.ReasonInvalidArguments.Value():
			// no retry, no msg, disable? definitely log error, shouldnt happen
			log.Errorf("Error processing Bits Auto Refill, invalid arguments used with everdeen: %s %s %s", envelope.RequestID, refill.DisabledReasonInvalidArguments, envelope.Message)
			profile, err := a.Refill.Get(ctx, envelope.UserID)

			if err != nil {
				log.WithError(err).Errorf("Error attempting to get auto refill profile to disable for user %s", envelope.UserID)
				return err
			}

			profile.State = refill.DisabledState
			profile.InProgressRequestID = ""
			profile.AttemptCount = 0
			_, _, err = a.Refill.SetWithoutNotification(ctx, *profile, refill.DisabledReasonInvalidArguments, true)

			if err != nil {
				log.WithError(err).Errorf("Error attempting to set auto refill profile to disable for user %s", envelope.UserID)
				return err
			}
		case models.ReasonInvalidPaymentInstrument.Value():
			// disable with notification from https://docs.google.com/document/d/1v2p6yEurquXmU21Uhzxbj1EC4V4PF_opH62QZCyf3sw
			_, err = a.Refill.DisableProfileAndRecordFailure(ctx, envelope.UserID, refill.DisabledReasonInvalidPayment, envelope.TransactionID, envelope.RequestID)
			return err
		default:
			log.Errorf("Error processing auto refill message with unknown status: %d %s %d %s", envelope.ReasonCode, envelope.UserID, envelope.Status, envelope.RequestID)
		}
	}

	return nil
}
