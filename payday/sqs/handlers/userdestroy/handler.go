package userdestroy

import (
	"context"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/clients/pdms"
	httperror "code.justin.tv/commerce/payday/errors/http"
	sfn_client "code.justin.tv/commerce/payday/sfn/client"
	sfn_models "code.justin.tv/commerce/payday/sfn/models"
	eventbus "code.justin.tv/eventbus/client"
	"code.justin.tv/eventbus/schema/pkg/user"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/service/sfn"
	"github.com/golang/protobuf/ptypes"
	"github.com/twitchtv/twirp"
)

type Handler struct {
	SFNClient  sfn_client.SFNClient    `inject:""`
	PDMSClient pdms.IPDMSClientWrapper `inject:""`
}

const (
	promiseDeleteNumOfDays = 30 * 24 * time.Hour // this value may change depending on recommendations from PDMS
)

func (h *Handler) HandleMessage(ctx context.Context, header *eventbus.Header, event *user.Destroy) error {
	log.WithField("userID", event.UserId).Info("Starting User Destroy Step Function")
	err := h.SFNClient.ExecuteUserDestroyStateMachine(sfn_models.UserDestroyInput{
		UserId: event.UserId,
	})
	if err != nil {
		var existingExecutionError bool
		if awsErr, ok := err.(awserr.Error); ok {
			if awsErr.Code() == sfn.ErrCodeExecutionAlreadyExists {
				existingExecutionError = true
			}
		}

		if existingExecutionError {
			log.WithField("userID", event.UserId).Info("found existing User Destroy SFN execution, skipping.")
		} else {
			log.WithField("userID", event.UserId).WithError(err).Error("error creating User Destroy SFN execution")
			return err
		}
	}

	timeOfDeletionPromise := time.Now().Add(promiseDeleteNumOfDays)
	timeOfDeletionPromiseTwirp, err := ptypes.TimestampProto(timeOfDeletionPromise)
	if err != nil {
		return err
	}
	log := log.WithFields(log.Fields{
		"userID":    event.UserId,
		"timestamp": timeOfDeletionPromise,
	})
	log.Info("Sending PromiseDeletion to PDMS")
	err = h.PDMSClient.PromiseDeletion(ctx, event.UserId, timeOfDeletionPromiseTwirp, false)

	if err == nil {
		return nil
	}
	// PDMSClientWrapper's `promiseDeletion` function returns an HttpError wrapping a twirp error.
	if httpError, ok := err.(httperror.HttpError); ok {
		if twirpError, ok := httpError.Underlying().(twirp.Error); ok {
			if twirpError.Code() == twirp.AlreadyExists {
				log.Info("User has already been deleted in PDMS")
				return nil
			}
		}
	}

	log.WithError(err).Error("error sending user PromiseDeletion to PDMS")
	return err
}
