package psqs_test

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"testing"
	"time"

	datascience_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/datascience"
	crypto_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/sqs/crypto"
	sqsiface_mock "code.justin.tv/commerce/payday/mocks/github.com/aws/aws-sdk-go/service/sqs/sqsiface"
	"code.justin.tv/commerce/payday/models"
	psqs "code.justin.tv/commerce/payday/sqs"
	"code.justin.tv/commerce/payday/sqs/crypto"
	"code.justin.tv/commerce/payday/utils/pointers"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/aws/aws-sdk-go/service/sqs/sqsiface"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

var validReceiptHandle = "2c8d67ba-fcbb-4a65-9dc6-064ece1dd921" // required; using a fixed string for repeatability

type TestHandler struct {
	HasBeenCalled map[models.MessageType]int
}

func NewTestHandler(handler psqs.IFABSMessageHandler, decrypter crypto.IDecrypter) psqs.MessageHandler {
	adapter := psqs.NewFABSMessageAdapterWithDecrypter(decrypter)
	return adapter.Adapt(handler)
}

func (h *TestHandler) HandleGiveBitsToBroadcaster(ctx context.Context, msg *models.GiveBitsToBroadcasterMessage) error {
	h.HasBeenCalled[models.GiveBitsToBroadcaster] += 1
	return nil
}

func (h *TestHandler) HandleAddBitsToCustomerAccount(ctx context.Context, msg *models.AddBitsToCustomerAccountMessage) error {
	h.HasBeenCalled[models.AddBitsToCustomerAccount] += 1
	return nil
}

func (h *TestHandler) HandleRemoveBitsFromCustomerAccount(ctx context.Context, msg *models.RemoveBitsFromCustomerAccountMessage) error {
	h.HasBeenCalled[models.RemoveBitsFromCustomerAccount] += 1
	return nil
}

func (h *TestHandler) HandleUseBitsOnExtension(ctx context.Context, msg *models.UseBitsOnExtensionMessage) error {
	h.HasBeenCalled[models.UseBitsOnExtension] += 1
	return nil
}

func (h *TestHandler) HandleUseBitsOnPoll(ctx context.Context, msg *models.UseBitsOnPollMessage) error {
	h.HasBeenCalled[models.UseBitsOnPoll] += 1
	return nil
}

func (h *TestHandler) HandleCreateHold(ctx context.Context, msg *models.CreateHoldMessage) error {
	h.HasBeenCalled[models.CreateHold] += 1
	return nil
}

func (h *TestHandler) HandleFinalizeHold(ctx context.Context, msg *models.FinalizeHoldMessage) error {
	h.HasBeenCalled[models.FinalizeHold] += 1
	return nil
}

func (h *TestHandler) HandleReleaseHold(ctx context.Context, msg *models.ReleaseHoldMessage) error {
	h.HasBeenCalled[models.ReleaseHold] += 1
	return nil
}

func initProcessor(mockedSQS sqsiface.SQSAPI, handler psqs.MessageHandler) *psqs.SQSPoller {

	mockedDataScience := new(datascience_mock.DataScience)
	mockedDataScience.On("TrackBitsSentEvent", mock.Anything)

	return &psqs.SQSPoller{
		SQSClient:      mockedSQS,
		MessageHandler: handler,
	}
}

func noopDecrypt(jsonIn []byte) (models.SQSMessageJSON, error) {
	var sqsMsg models.SQSMessageJSON

	var snsMsg models.SNSMessage
	err := json.Unmarshal(jsonIn, &snsMsg)
	if err != nil {
		return sqsMsg, err
	}

	var encMsg models.EncryptedNotification
	err = json.Unmarshal([]byte(snsMsg.Message), &encMsg)
	if err != nil {
		return sqsMsg, err
	}

	err = sqsMsg.UnmarshalJSON([]byte(encMsg.Payload))
	return sqsMsg, err
}

// fail properly if messageType is not known
func TestHandleUnknownMessage(t *testing.T) {
	mockedSQSAPI := new(sqsiface_mock.SQSAPI)
	mockedDecrypter := new(crypto_mock.IDecrypter)
	handler := &TestHandler{
		HasBeenCalled: map[models.MessageType]int{},
	}
	testHandler := NewTestHandler(handler, mockedDecrypter)

	classUnderTest := initProcessor(mockedSQSAPI, testHandler)

	bodyJSON, err := ioutil.ReadFile("testdata/unknownSNS_plaintext.json")
	assert.NoError(t, err)

	msg := &sqs.Message{
		Body:          pointers.StringP(string(bodyJSON)),
		ReceiptHandle: &validReceiptHandle,
	}

	mockedDecrypter.On("ProcessEncryptedBitsMessage", mock.Anything).Return(noopDecrypt(bodyJSON))
	mockedSQSAPI.On("DeleteMessage", mock.Anything).Return(nil, nil)

	ctx, cancel := context.WithTimeout(context.Background(), 20*time.Second)
	defer cancel()

	classUnderTest.HandleMessage(ctx, msg)

	// Handler fails and message is NOT deleted from the queue
	mockedSQSAPI.AssertNumberOfCalls(t, "DeleteMessage", 0)
}

// succeed in handling a givebits message
func TestHandleGiveBitsToBroadcasterMessage(t *testing.T) {
	mockedSQSAPI := new(sqsiface_mock.SQSAPI)
	mockedDecrypter := new(crypto_mock.IDecrypter)
	handler := &TestHandler{
		HasBeenCalled: map[models.MessageType]int{},
	}

	testHandler := NewTestHandler(handler, mockedDecrypter)

	classUnderTest := initProcessor(mockedSQSAPI, testHandler)

	bodyJSON, err := ioutil.ReadFile("testdata/validGiveBitsToBroadcasterSNS_plaintext.json")
	if err != nil {
		t.Fail()
	}

	msg := &sqs.Message{
		Body:          pointers.StringP(string(bodyJSON)),
		ReceiptHandle: &validReceiptHandle,
	}

	mockedDecrypter.On("ProcessEncryptedBitsMessage", mock.Anything).Return(noopDecrypt(bodyJSON))
	mockedSQSAPI.On("DeleteMessage", mock.Anything).Return(nil, nil)

	ctx, cancel := context.WithTimeout(context.Background(), 20*time.Second)
	defer cancel()

	classUnderTest.HandleMessage(ctx, msg)

	// Handler succeeds and message IS deleted from the queue
	assert.Nil(t, err)
	mockedSQSAPI.AssertNumberOfCalls(t, "DeleteMessage", 1)
	assert.Equal(t, 1, handler.HasBeenCalled[models.GiveBitsToBroadcaster])
}

// succeed in handling a givebits message
func TestHandleGiveBitsToBroadcasterMessage_PachinkoMessageShouldProcessWhenConfigured(t *testing.T) {
	mockedSQSAPI := new(sqsiface_mock.SQSAPI)
	mockedDecrypter := new(crypto_mock.IDecrypter)
	handler := &TestHandler{
		HasBeenCalled: map[models.MessageType]int{},
	}
	testHandler := NewTestHandler(handler, mockedDecrypter)

	classUnderTest := initProcessor(mockedSQSAPI, testHandler)

	bodyJSON, err := ioutil.ReadFile("testdata/validGiveBitsToBroadcasterSNS_plaintext_andes.json")
	if err != nil {
		t.Fail()
	}

	msg := &sqs.Message{
		Body:          pointers.StringP(string(bodyJSON)),
		ReceiptHandle: &validReceiptHandle,
	}

	mockedDecrypter.On("ProcessEncryptedBitsMessage", mock.Anything).Return(noopDecrypt(bodyJSON))
	mockedSQSAPI.On("DeleteMessage", mock.Anything).Return(nil, nil)

	ctx, cancel := context.WithTimeout(context.Background(), 20*time.Second)
	defer cancel()

	classUnderTest.HandleMessage(ctx, msg)

	// Handler succeeds and message IS deleted from the queue
	assert.Nil(t, err)
	mockedSQSAPI.AssertNumberOfCalls(t, "DeleteMessage", 1)
	assert.Equal(t, 1, handler.HasBeenCalled[models.GiveBitsToBroadcaster])
}

// succeed in handling an add bits message
func TestAddBitsMessageIgnored(t *testing.T) {
	mockedSQSAPI := new(sqsiface_mock.SQSAPI)
	mockedDecrypter := new(crypto_mock.IDecrypter)
	handler := &TestHandler{
		HasBeenCalled: map[models.MessageType]int{},
	}
	testHandler := NewTestHandler(handler, mockedDecrypter)

	classUnderTest := initProcessor(mockedSQSAPI, testHandler)

	bodyJSON, err := ioutil.ReadFile("testdata/validAddBitsSNS_plaintext.json")
	if err != nil {
		t.Fail()
	}

	msg := &sqs.Message{
		Body:          pointers.StringP(string(bodyJSON)),
		ReceiptHandle: &validReceiptHandle,
	}

	mockedDecrypter.On("ProcessEncryptedBitsMessage", mock.Anything).Return(noopDecrypt(bodyJSON))
	mockedSQSAPI.On("DeleteMessage", mock.Anything).Return(nil, nil)

	ctx, cancel := context.WithTimeout(context.Background(), 20*time.Second)
	defer cancel()

	classUnderTest.HandleMessage(ctx, msg)

	// Handler succeeds and message IS deleted from the queue
	mockedSQSAPI.AssertNumberOfCalls(t, "DeleteMessage", 1)
	assert.Equal(t, 1, handler.HasBeenCalled[models.AddBitsToCustomerAccount])
}

// succeed in handling remove bits message
func TestRemoveBitsMessageIgnored(t *testing.T) {
	mockedSQSAPI := new(sqsiface_mock.SQSAPI)
	mockedDecrypter := new(crypto_mock.IDecrypter)
	handler := &TestHandler{
		HasBeenCalled: map[models.MessageType]int{},
	}
	testHandler := NewTestHandler(handler, mockedDecrypter)

	classUnderTest := initProcessor(mockedSQSAPI, testHandler)

	bodyJSON, err := ioutil.ReadFile("testdata/validRemoveBitsSNS_plaintext.json")
	if err != nil {
		t.Fail()
	}

	msg := &sqs.Message{
		Body:          pointers.StringP(string(bodyJSON)),
		ReceiptHandle: &validReceiptHandle,
	}

	mockedDecrypter.On("ProcessEncryptedBitsMessage", mock.Anything).Return(noopDecrypt(bodyJSON))
	mockedSQSAPI.On("DeleteMessage", mock.Anything).Return(nil, nil)

	ctx, cancel := context.WithTimeout(context.Background(), 20*time.Second)
	defer cancel()

	classUnderTest.HandleMessage(ctx, msg)

	// Handler succeeds and message IS deleted from the queue
	mockedSQSAPI.AssertNumberOfCalls(t, "DeleteMessage", 1)
	assert.Equal(t, 1, handler.HasBeenCalled[models.RemoveBitsFromCustomerAccount])
}

// delete a message on success
func TestDeleteMessageOnSuccess(t *testing.T) {
	mockedSQSAPI := new(sqsiface_mock.SQSAPI)
	mockedDecrypter := new(crypto_mock.IDecrypter)
	handler := &TestHandler{
		HasBeenCalled: map[models.MessageType]int{},
	}
	testHandler := NewTestHandler(handler, mockedDecrypter)

	classUnderTest := initProcessor(mockedSQSAPI, testHandler)

	bodyJSON, err := ioutil.ReadFile("testdata/validGiveBitsToBroadcasterSNS_plaintext.json")
	if err != nil {
		t.Fail()
	}

	msg := &sqs.Message{
		Body:          pointers.StringP(string(bodyJSON)),
		ReceiptHandle: &validReceiptHandle,
	}

	mockedDecrypter.On("ProcessEncryptedBitsMessage", mock.Anything).Return(noopDecrypt(bodyJSON))
	mockedSQSAPI.On("DeleteMessage", mock.Anything).Return(nil, nil)

	ctx, cancel := context.WithTimeout(context.Background(), 20*time.Second)
	defer cancel()

	classUnderTest.HandleMessage(ctx, msg)

	// Handler succeeds and message IS deleted from the queue
	mockedSQSAPI.AssertNumberOfCalls(t, "DeleteMessage", 1)
	assert.Equal(t, 1, handler.HasBeenCalled[models.GiveBitsToBroadcaster])
}
