package bulk_grant

type Campaign struct {
	ID                      string `yaml:"id"`
	Name                    string `yaml:"name"`
	BitType                 string `yaml:"bit_type"`
	LimitPerUser            int32  `yaml:"limit_per_user"`
	BitAmount               int32  `yaml:"bit_amount"`
	AdminReason             string `yaml:"admin_reason"`
	PushyNotificationOrigin string `yaml:"pushy_notification_origin"`
}
