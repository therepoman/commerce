package bulk_grant

import (
	"math/rand"
	"sort"
	"strings"
	"sync"
	"time"

	"code.justin.tv/chat/golibs/graceful"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/s3"
	"gopkg.in/yaml.v2"
)

const (
	refreshDelayMaxRandomOffset = time.Second * 30
	refreshDelay                = time.Minute * 5
)

type CampaignsGetter interface {
	GetList() []Campaign
	GetCampaign(campaignID string) *Campaign
}

type CampaignsPoller struct {
	campaigns []Campaign
	S3Client  s3.IS3Client `inject:""`
	bucket    string
	key       string
	mutex     *sync.RWMutex
}

func NewCampaignsPoller(bucket string, key string) *CampaignsPoller {
	campaigns := make([]Campaign, 0)
	mutex := &sync.RWMutex{}

	return &CampaignsPoller{
		campaigns: campaigns,
		mutex:     mutex,
		bucket:    bucket,
		key:       key,
	}
}

func createCampaignsList(s3client s3.IS3Client, bucket string, key string) ([]Campaign, error) {
	campaignsBytes, err := s3client.LoadFile(bucket, key)
	if err != nil {
		log.WithFields(log.Fields{
			"key":    key,
			"bucket": bucket,
		}).Error("could not load file from S3 for campaigns", err)
		return nil, err
	}

	campaigns, err := loadCampaignsFromYaml(campaignsBytes)
	if err != nil {
		return nil, err
	}
	return campaigns, nil
}

func loadCampaignsFromYaml(bytes []byte) ([]Campaign, error) {
	campaigns := make([]Campaign, 0)
	err := yaml.Unmarshal(bytes, &campaigns)
	if err != nil {
		return nil, err
	}

	return campaigns, nil
}

func (p *CampaignsPoller) refreshCampaignsList() error {
	campaigns, err := createCampaignsList(p.S3Client, p.bucket, p.key)
	if err != nil {
		return err
	}

	sort.Slice(campaigns, func(i, j int) bool {
		return strings.Compare(campaigns[i].Name, campaigns[j].Name) < 0
	})

	p.mutex.Lock()
	defer p.mutex.Unlock()

	p.campaigns = campaigns

	return nil
}

func (p *CampaignsPoller) Init() error {
	if exists, err := p.S3Client.BucketExists(p.bucket); !exists || err != nil {
		log.WithError(err).WithField("bucket", p.bucket).Error("Bucket does not exist for campaigns object creation")
		return err
	}

	if exists, err := p.S3Client.FileExists(p.bucket, p.key); !exists || err != nil {
		log.WithFields(log.Fields{
			"key":    p.key,
			"bucket": p.bucket,
		}).WithError(err).Error("Key does not exist in Bucket for campaigns object creation")
		return err
	}

	err := p.refreshCampaignsList()
	if err != nil {
		log.WithError(err).Error("Error initializing campaigns")
		return err
	}

	return nil
}

func (p *CampaignsPoller) RefreshCampaignsListAtInterval() {
	delayOffset := rand.Int63n(int64(refreshDelayMaxRandomOffset))
	timer := time.NewTicker(refreshDelay + time.Duration(delayOffset))

	for {
		select {
		case <-graceful.ShuttingDown():
			return
		case <-timer.C:
			err := p.refreshCampaignsList()
			if err != nil {
				log.WithError(err).Error("Error refreshing campaigns")
			}
		}
	}
}

func (p *CampaignsPoller) GetList() []Campaign {
	p.mutex.RLock()
	defer p.mutex.RUnlock()

	return p.campaigns
}

func (p *CampaignsPoller) GetCampaign(campaignID string) *Campaign {
	campaigns := p.GetList()
	for _, campaign := range campaigns {
		c := campaign
		if c.ID == campaignID {
			return &c
		}
	}
	return nil
}
