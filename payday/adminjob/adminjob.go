package adminjob

import (
	"archive/zip"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"time"

	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/s3"
	"code.justin.tv/commerce/payday/sns"
	util "code.justin.tv/commerce/payday/utils/close"
	"code.justin.tv/commerce/payday/utils/strings"
)

type Type string

const (
	BulkEntitle            = Type("bulk-entitle")
	BulkPromoNotif         = Type("bulk-promo-notif")
	LeaderboardBackfill    = Type("leaderboard-backfill")
	Test                   = Type("test")
	PachinkoBackfill       = Type("pachinko-backfill")
	TransactionsBackfill   = Type("transactions-backfill")
	SinglePurchaseBackfill = Type("single-purchase-table-backfill")
)

var Types = map[Type]interface{}{
	BulkEntitle:            nil,
	BulkPromoNotif:         nil,
	LeaderboardBackfill:    nil,
	Test:                   nil,
	PachinkoBackfill:       nil,
	TransactionsBackfill:   nil,
	SinglePurchaseBackfill: nil,
}

func IsValidType(t Type) bool {
	_, ok := Types[t]
	return ok
}

const (
	bitsAdminJobsBucket = "bits-admin-jobs"
)

type StartBulkEntitleJobRequest struct {
	Creator             string   `json:"creator"`
	JobID               string   `json:"job_id"`
	BitsAmount          int32    `json:"bits_amount"`
	AdminReason         string   `json:"admin_reason"`
	RecipientLogins     []string `json:"recipient_logins"`
	RecipientIDs        []string `json:"recipient_ids"`
	TypeOfBitsToAdd     string   `json:"type_of_bits_to_add"`
	NotifyUser          bool     `json:"notify_user"`
	Origin              string   `json:"origin"`
	BulkGrantCampaignID string   `json:"bulk_grant_campaign_id"`
}

type StartAdminJobRequest struct {
	Creator string `json:"creator"`
	JobID   string `json:"job_id"`
	Type    string `json:"type"`
	Input   string `json:"input"`
	Options string `json:"options"`
}

type AdminJob struct {
	Creator        string    `json:"creator"`
	JobID          string    `json:"job_id"`
	StartTime      time.Time `json:"start_time"`
	Type           string    `json:"type"`
	Progress       float64   `json:"progress"`
	NumOutputFiles int       `json:"num_output_files"`
}

type GetAdminJobsResponse struct {
	AdminJobs []*AdminJob `json:"admin_jobs"`
}

func FromDynamo(dynamoJobs []*dynamo.AdminJob) []*AdminJob {
	jobs := make([]*AdminJob, 0)
	for _, dynamoJob := range dynamoJobs {
		if dynamoJob != nil {
			jobs = append(jobs, &AdminJob{
				Creator:   dynamoJob.Creator,
				JobID:     dynamoJob.JobID,
				StartTime: dynamoJob.StartTime,
				Type:      dynamoJob.Type,
				Progress:  dynamoJob.Progress,
			})
		}
	}
	return jobs
}

func (req StartAdminJobRequest) ToIdentifier() Identifier {
	return Identifier{
		Creator: req.Creator,
		JobID:   req.JobID,
	}
}

type Identifier struct {
	Creator string `json:"creator"`
	JobID   string `json:"job_id"`
}

func (id Identifier) String() string {
	return fmt.Sprintf("%s.%s", id.Creator, id.JobID)
}

type IManager interface {
	SaveInputToS3(ctx context.Context, req StartAdminJobRequest) error
	SaveOptionsToS3(ctx context.Context, req StartAdminJobRequest) error
	SendSNSMessage(ctx context.Context, req StartAdminJobRequest) error
	GetInputFromS3(ctx context.Context, id Identifier) (RawInput, error)
	GetOptionsFromS3(ctx context.Context, id Identifier) (RawOptions, error)
	ListOutputFilesFromS3(ctx context.Context, id Identifier) ([]string, error)
	WriteOutputZip(ctx context.Context, id Identifier, w io.Writer) error
	SaveOutputToS3(ctx context.Context, id Identifier, key TaskKey, taskOut TaskOutput, taskErr error) error
}

type Manager struct {
	S3Client  s3.IS3Client   `inject:""`
	SNSClient sns.ISNSClient `inject:"usWestSNSClient"`
}

type RawInput string
type RawOptions string
type TaskInput string
type TaskIndex int
type TaskKey string
type TaskOutput string

func GetInputS3Key(id Identifier) string {
	return fmt.Sprintf("%s/%s/%s/input", config.Get().Environment, id.Creator, id.JobID)
}

func GetOptionsS3Key(id Identifier) string {
	return fmt.Sprintf("%s/%s/%s/options", config.Get().Environment, id.Creator, id.JobID)
}

func GetOutputS3Prefix(id Identifier) string {
	return fmt.Sprintf("%s/%s/%s/output", config.Get().Environment, id.Creator, id.JobID)
}

func GetTaskOutputS3Key(id Identifier, key TaskKey) string {
	return fmt.Sprintf("%s/%s/%s/output/%s.txt", config.Get().Environment, id.Creator, id.JobID, key)
}

func (m *Manager) SaveOptionsToS3(ctx context.Context, req StartAdminJobRequest) error {
	return m.S3Client.UploadFileToBucket(bitsAdminJobsBucket, GetOptionsS3Key(req.ToIdentifier()), []byte(req.Options))
}

func (m *Manager) SaveInputToS3(ctx context.Context, req StartAdminJobRequest) error {
	return m.S3Client.UploadFileToBucket(bitsAdminJobsBucket, GetInputS3Key(req.ToIdentifier()), []byte(req.Input))
}

func (m *Manager) ListOutputFilesFromS3(ctx context.Context, id Identifier) ([]string, error) {
	return m.S3Client.ListFileNames(bitsAdminJobsBucket, GetOutputS3Prefix(id))
}

func (m *Manager) WriteOutputZip(ctx context.Context, id Identifier, w io.Writer) error {
	s3Keys, err := m.ListOutputFilesFromS3(ctx, id)
	if err != nil {
		return err
	}

	type s3File struct {
		key      string
		contents []byte
		err      error
	}

	fileChan := make(chan s3File)
	totalFiles := 0
	for _, s3Key := range s3Keys {
		go func(k string) {
			contents, err := m.S3Client.LoadFile(bitsAdminJobsBucket, k)
			file := s3File{
				key:      k,
				contents: contents,
				err:      err,
			}
			fileChan <- file
		}(s3Key)
		totalFiles++
	}

	zipWriter := zip.NewWriter(w)
	defer util.Close(zipWriter)
	for i := 0; i < totalFiles; i++ {
		var file s3File
		select {
		case <-ctx.Done():
			return errors.New("timed out waiting for s3 responses")
		case file = <-fileChan:
		}

		if file.err != nil {
			return errors.Notef(file.err, "error loading file from S3")
		}

		fileWriter, err := zipWriter.Create(file.key)
		if err != nil {
			return errors.Notef(err, "error creating a new file within the zip")
		}

		_, err = fileWriter.Write(file.contents)
		if err != nil {
			return errors.Notef(err, "error writing S3 file contents to file within the zip")
		}
	}

	return nil
}

func (m *Manager) SendSNSMessage(ctx context.Context, req StartAdminJobRequest) error {
	cfg := config.Get()
	snsMsg := &Identifier{
		Creator: req.Creator,
		JobID:   req.JobID,
	}
	snsJson, err := json.Marshal(snsMsg)
	if err != nil {
		return err
	}
	return m.SNSClient.PostToTopic(cfg.AdminJobsSNSTopic, string(snsJson))
}

func (m *Manager) GetInputFromS3(ctx context.Context, id Identifier) (RawInput, error) {
	bs, err := m.S3Client.LoadFile(bitsAdminJobsBucket, GetInputS3Key(id))
	if err != nil {
		return "", err
	}
	return RawInput(bs), nil
}

func (m *Manager) GetOptionsFromS3(ctx context.Context, id Identifier) (RawOptions, error) {
	bs, err := m.S3Client.LoadFile(bitsAdminJobsBucket, GetOptionsS3Key(id))
	if err != nil {
		return "", err
	}
	return RawOptions(bs), nil
}

func (m *Manager) SaveOutputToS3(ctx context.Context, id Identifier, key TaskKey, taskOut TaskOutput, taskErr error) error {
	fileForS3 := fmt.Sprintf("%s\n", string(key))

	if strings.NotBlank(string(taskOut)) {
		fileForS3 += fmt.Sprintf("%s\n", string(taskOut))
	}

	if taskErr != nil {
		fileForS3 += fmt.Sprintf("%s\n", taskErr.Error())
	}

	return m.S3Client.UploadFileToBucket(bitsAdminJobsBucket, GetTaskOutputS3Key(id, key), []byte(fileForS3))
}
