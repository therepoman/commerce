package transactionbackfill

import (
	"context"
	"encoding/json"
	"fmt"
	"strings"
	"time"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/adminjob"
	"code.justin.tv/commerce/payday/adminjob/handler/ratelimit"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/utils/pointers"
	petozi "code.justin.tv/commerce/petozi/rpc"
	"github.com/aws/aws-sdk-go/aws/session"
	d "github.com/guregu/dynamo"
	"github.com/pkg/errors"
	"golang.org/x/time/rate"
)

type Factory interface {
	New(id adminjob.Identifier, rawOpt adminjob.RawOptions) (*taskHandler, error)
}

type factory struct {
	BackfillDAO  DAO           `inject:""`
	PetoziClient petozi.Petozi `inject:""`
}

func NewFactory() Factory {
	return &factory{}
}

func (h *factory) New(id adminjob.Identifier, rawOpt adminjob.RawOptions) (*taskHandler, error) {
	return &taskHandler{
		backfillDAO:  h.BackfillDAO,
		petoziClient: h.PetoziClient,
	}, nil
}

type taskHandler struct {
	productsByBitsType map[string]*petozi.BitsProduct
	backfillDAO        DAO
	petoziClient       petozi.Petozi
}

type Transaction struct {
	TransactionID   string
	TransactionType string
	DateTime        time.Time
	UserID          string
	ChannelID       string
	BitsAmount      int64
	EventReasonCode string

	BitsType string
	Price    string
	Currency string
	Platform string

	RawMessage            string
	TMIMessage            string
	IsAnonymous           bool
	NumberOfBitsSponsored int64
	SponsoredAmounts      map[string]int64
	EmoteTotals           map[string]int64
}

type DynamoTransaction struct {
	TransactionID         string `dynamo:"transaction_id"`
	TransactionType       string `dynamo:"transaction_type"`
	UserID                string `dynamo:"user_id"`
	UserIDTransactionType string `dynamo:"user_id-transaction_type"`
	ChannelID             string `dynamo:"channel_id"`
	LastUpdated           int64  `dynamo:"last_updated"`
	NumberOfBits          int64  `dynamo:"number_of_bits"`

	BitsType              *string `dynamo:"bits_type"`
	Platform              *string `dynamo:"platform"`
	ProductID             *string `dynamo:"product_id"`
	PurchasePrice         *string `dynamo:"purchase_price"`
	PurchasePriceCurrency *string `dynamo:"purchase_price_currency"`
	SKUQuantity           *int64  `dynamo:"sku_quantity"`

	RawMessage            *string          `dynamo:"raw_message"`
	TMIMessage            *string          `dynamo:"tmi_message"`
	IsAnonymous           *bool            `dynamo:"is_anonymous"`
	NumberOfBitsSponsored *int64           `dynamo:"number_of_bits_sponsored"`
	SponsoredAmounts      map[string]int64 `dynamo:"sponsored_amounts"`
	EmoteTotals           map[string]int64 `dynamo:"emote_totals"`
}

type MessageList []Transaction

func (h *taskHandler) ValidateInput(ctx context.Context, input adminjob.RawInput) error {
	var messageList MessageList
	return json.Unmarshal([]byte(input), &messageList)
}

func (h *taskHandler) GetTasks(ctx context.Context, input adminjob.RawInput) ([]adminjob.TaskInput, error) {
	tasks := make([]adminjob.TaskInput, 0)

	var messageList []Transaction

	err := json.Unmarshal([]byte(input), &messageList)
	if err != nil {
		return tasks, err
	}

	for _, message := range messageList {
		messageBytes, err := json.Marshal(message)
		if err != nil {
			return tasks, err
		}
		tasks = append(tasks, adminjob.TaskInput(string(messageBytes)))
	}

	return tasks, nil
}

func (h *taskHandler) GetTaskKey(ctx context.Context, idx adminjob.TaskIndex, input adminjob.TaskInput) (adminjob.TaskKey, error) {
	tx, err := parseTransaction(string(input))
	if err != nil {
		return "", err
	}
	if tx == nil {
		return "", errors.Errorf("transaction is nil. input: %s", input)
	}

	return adminjob.TaskKey(fmt.Sprintf("%d, %s", int(idx), tx.TransactionID)), nil
}

func (h *taskHandler) Handle(ctx context.Context, input adminjob.TaskInput) (adminjob.TaskOutput, error) {
	tx, err := parseTransaction(string(input))
	if err != nil {
		logrus.WithField("input", input).WithError(err).Error("admin job: Failed to parse transaction")
		return "", err
	} else {
		logrus.WithField("transaction", tx).Info("admin job: received transaction")
	}

	// load all products if we have not done so
	if h.productsByBitsType == nil || len(h.productsByBitsType) == 0 {
		h.productsByBitsType = make(map[string]*petozi.BitsProduct)

		productsResp, err := h.petoziClient.GetBitsProducts(ctx, &petozi.GetBitsProductsReq{
			UseInMemoryCache: true,
		})
		if err != nil {
			return "", err
		}

		for _, product := range productsResp.BitsProducts {
			h.productsByBitsType[product.BitsTypeId] = product
		}

		if len(h.productsByBitsType) == 0 {
			return "", errors.New("found no bits type from petozi")
		}
	}

	dynamoTx, err := h.buildDynamoTransaction(tx)
	if err != nil {
		return "", errors.Wrap(err, "failed to build a dynamo transaction")
	}

	// nil for skipping a record
	if dynamoTx == nil {
		return "", nil
	}

	return "", h.backfillDAO.Update(dynamoTx)
}

func (h *taskHandler) buildDynamoTransaction(tx *Transaction) (*DynamoTransaction, error) {
	dynamoTx := &DynamoTransaction{
		UserID:                tx.UserID,
		ChannelID:             tx.ChannelID,
		LastUpdated:           tx.DateTime.UnixNano(),
		TransactionID:         tx.TransactionID,
		TransactionType:       tx.TransactionType,
		NumberOfBits:          tx.BitsAmount,
		UserIDTransactionType: fmt.Sprintf("%s-%s", tx.UserID, tx.TransactionType),
		BitsType:              pointers.StringP(tx.BitsType),
		Platform:              pointers.StringP(tx.Platform), // Ads have no products in Petozi, but still have platforms
	}

	// populate product information if a bits product is found given bits type
	if product, found := h.productsByBitsType[tx.BitsType]; found {
		dynamoTx.ProductID = pointers.StringP(product.Id)
		dynamoTx.Platform = pointers.StringP(strings.ToLower(product.Platform.String()))

		dynamoTx.PurchasePrice = pointers.StringP(tx.Price)
		dynamoTx.PurchasePriceCurrency = pointers.StringP(tx.Currency)
		skuQuantity := tx.BitsAmount / int64(product.Quantity)
		dynamoTx.SKUQuantity = pointers.Int64P(skuQuantity)
	}

	if tx.TransactionType == "BITS_EVENT" {
		dynamoTx.RawMessage = pointers.StringP(tx.RawMessage)
		dynamoTx.TMIMessage = pointers.StringP(tx.TMIMessage)
		dynamoTx.IsAnonymous = pointers.BoolP(tx.IsAnonymous)
		dynamoTx.EmoteTotals = tx.EmoteTotals

		if tx.NumberOfBitsSponsored > 0 {
			dynamoTx.NumberOfBitsSponsored = pointers.Int64P(tx.NumberOfBitsSponsored)
			dynamoTx.SponsoredAmounts = tx.SponsoredAmounts
		}
	}

	return dynamoTx, nil
}

func parseTransaction(marshalledTx string) (*Transaction, error) {
	var tx Transaction
	err := json.Unmarshal([]byte(marshalledTx), &tx)
	if err != nil {
		return nil, err
	}

	switch tx.TransactionType {
	case "RemoveBitsFromCustomerAccount":
		tx.TransactionType = "AdminRemoveBits"
	case "AddBitsToCustomerAccount":
		if tx.EventReasonCode == "ActiveAdminAddBits" {
			tx.TransactionType = "AdminAddBits"
		} else {
			tx.TransactionType = "AddBits"
		}

		// fill in missing bits type info with CB_0
		if len(tx.BitsType) == 0 {
			tx.BitsType = "CB_0"
		}
	case "GiveBitsToBroadcaster":
		tx.TransactionType = "BITS_EVENT"

		// Ignore bits type on cheers, it's to be calculated by Pachter
		tx.BitsType = ""
	case "UseBitsOnExtension":
		// just to double make sure we don't backfill UseBitsOnExtension data
		return nil, nil
	}

	// Number of bits is always recorded as a positive number
	if tx.BitsAmount < 0 {
		tx.BitsAmount = tx.BitsAmount * -1
	}

	return &tx, nil
}

func (h *taskHandler) GetRateLimitConfig() ratelimit.Config {
	return ratelimit.Config{
		Enabled: true,
		Key:     "transactions-backfill-job",
		TPS:     rate.Limit(5000),
	}
}

type DAO interface {
	Update(tx *DynamoTransaction) error
	Get(id string) (*DynamoTransaction, error)
}

type dao struct {
	*d.Table
}

func NewDAO(sess *session.Session, cfg config.Configuration) DAO {
	db := d.New(sess)
	table := db.Table(cfg.TransactionTableName)
	return &dao{
		Table: &table,
	}
}

func (dao *dao) Get(id string) (*DynamoTransaction, error) {
	var tx DynamoTransaction
	err := dao.Table.Get("transaction_id", id).One(&tx)

	return &tx, err
}

func (dao *dao) Update(tx *DynamoTransaction) error {
	update := dao.Table.Update("transaction_id", tx.TransactionID).
		Set("last_updated", tx.LastUpdated).
		Set("transaction_type", tx.TransactionType).
		Set("user_id", tx.UserID).
		Set("'user_id-transaction_type'", tx.UserIDTransactionType).
		Set("channel_id", tx.ChannelID).
		Set("number_of_bits", tx.NumberOfBits)

	if tx.BitsType != nil && len(*tx.BitsType) > 0 {
		update = update.Set("bits_type", *tx.BitsType)
	}

	if tx.Platform != nil && len(*tx.Platform) > 0 {
		update = update.Set("platform", *tx.Platform)
	}

	if tx.ProductID != nil && len(*tx.ProductID) > 0 {
		update = update.Set("product_id", *tx.ProductID)
	}

	if tx.PurchasePrice != nil && len(*tx.PurchasePrice) > 0 {
		update = update.Set("purchase_price", *tx.PurchasePrice)
	}

	if tx.PurchasePriceCurrency != nil && len(*tx.PurchasePriceCurrency) > 0 {
		update = update.Set("purchase_price_currency", *tx.PurchasePriceCurrency)
	}

	if tx.SKUQuantity != nil && *tx.SKUQuantity > 0 {
		update = update.Set("sku_quantity", *tx.SKUQuantity)
	}

	if tx.RawMessage != nil && len(*tx.RawMessage) > 0 {
		update = update.Set("raw_message", *tx.RawMessage)
	}

	if tx.TMIMessage != nil && len(*tx.TMIMessage) > 0 {
		update = update.Set("tmi_message", *tx.TMIMessage)
	}

	if tx.IsAnonymous != nil {
		update = update.Set("is_anonymous", *tx.IsAnonymous)
	}

	if tx.NumberOfBitsSponsored != nil && *tx.NumberOfBitsSponsored > 0 {
		update = update.Set("number_of_bits_sponsored", *tx.NumberOfBitsSponsored)
	}

	if len(tx.SponsoredAmounts) > 0 {
		update = update.Set("sponsored_amounts", tx.SponsoredAmounts)
	}

	if len(tx.EmoteTotals) > 0 {
		update = update.Set("emote_totals", tx.EmoteTotals)
	}

	return update.Run()
}
