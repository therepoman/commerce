package lbbackfill

import (
	"context"
	"fmt"
	"strconv"
	"strings"
	"time"

	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	"code.justin.tv/commerce/payday/adminjob"
	"code.justin.tv/commerce/payday/adminjob/handler/ratelimit"
	"code.justin.tv/commerce/payday/clients/pantheon"
	"code.justin.tv/commerce/payday/clients/user"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/utils/math"
	stringUtils "code.justin.tv/commerce/payday/utils/strings"
)

const (
	maxAttempts = 5
	retryDelay  = 300 * time.Millisecond
)

type Factory interface {
	New(id adminjob.Identifier, rawOpt adminjob.RawOptions) (*TaskHandler, error)
}

type FactoryImpl struct {
	UserServiceClient user.IUserServiceClientWrapper  `inject:""`
	PantheonClient    pantheon.IPantheonClientWrapper `inject:""`
}

type TaskHandler struct {
	UserServiceClient user.IUserServiceClientWrapper
	PantheonClient    pantheon.IPantheonClientWrapper
	JobID             adminjob.Identifier
}

type Transaction struct {
	TransactionID string
	UserID        string
	ChannelID     string
	BitsAmount    int
	TimeOfEvent   time.Time
}

func (f *FactoryImpl) New(id adminjob.Identifier, rawOpt adminjob.RawOptions) (*TaskHandler, error) {
	return &TaskHandler{
		UserServiceClient: f.UserServiceClient,
		PantheonClient:    f.PantheonClient,
		JobID:             id,
	}, nil
}

func parseLine(line string) (*Transaction, error) {
	cols := strings.Split(line, ",")
	if len(cols) != 5 {
		return nil, errors.Newf("Each line must be 5 columns. Line was %d columns", len(cols))
	}
	txID := cols[0]
	userID := cols[1]
	channelID := cols[2]
	bitsAmountStr := cols[3]
	timeOfEventStr := cols[4]

	if stringUtils.Blank(txID) {
		return nil, errors.New("Encountered a blank transaction id")
	}

	if stringUtils.Blank(userID) {
		return nil, errors.New("Encountered a blank user")
	}

	if stringUtils.Blank(channelID) {
		return nil, errors.New("Encountered a blank channel")
	}

	bitsAmount, err := strconv.Atoi(bitsAmountStr)
	if err != nil {
		return nil, errors.Newf("Encountered a non-numeric bits amount (%s)", bitsAmountStr)
	}

	timeOfEvent, err := time.Parse("2006-01-02 15:04:05", timeOfEventStr)
	if err != nil {
		return nil, errors.Newf("Encountered an un-parseable time (%s)", timeOfEventStr)
	}

	return &Transaction{
		TransactionID: txID,
		UserID:        userID,
		ChannelID:     channelID,
		BitsAmount:    math.IntAbs(bitsAmount),
		TimeOfEvent:   timeOfEvent,
	}, nil
}

func (th *TaskHandler) ValidateInput(ctx context.Context, input adminjob.RawInput) error {
	lines := strings.Split(string(input), "\n")
	for i, line := range lines {
		lineNum := i + 1
		_, err := parseLine(line)
		if err != nil {
			return errors.Newf("Error on line #%d. Error: %v", lineNum, err)
		}
	}
	return nil
}

func (th *TaskHandler) GetTasks(ctx context.Context, input adminjob.RawInput) ([]adminjob.TaskInput, error) {
	lines := strings.Split(string(input), "\n")
	tasks := make([]adminjob.TaskInput, 0)
	for _, line := range lines {
		tasks = append(tasks, adminjob.TaskInput(strings.TrimSpace(line)))
	}
	return tasks, nil
}

func (th *TaskHandler) GetTaskKey(ctx context.Context, idx adminjob.TaskIndex, input adminjob.TaskInput) (adminjob.TaskKey, error) {
	transaction, err := parseLine(string(input))
	if err != nil {
		return "", err
	}
	return adminjob.TaskKey(fmt.Sprintf("%d.%s", int(idx), transaction.TransactionID)), nil
}

func (th *TaskHandler) Handle(ctx context.Context, input adminjob.TaskInput) (adminjob.TaskOutput, error) {
	transaction, err := parseLine(string(input))
	if err != nil {
		return "", err
	}

	req := pantheonrpc.PublishEventReq{
		Domain:      pantheon.PantheonBitsUsageDomain,
		EventId:     transaction.TransactionID,
		TimeOfEvent: uint64(transaction.TimeOfEvent.UnixNano()),
		GroupingKey: transaction.ChannelID,
		EntryKey:    transaction.UserID,
		EventValue:  int64(transaction.BitsAmount),
	}
	err = th.publishEvent(ctx, req)
	if err != nil {
		return "", err
	}

	return "", nil
}

func (th *TaskHandler) publishEvent(ctx context.Context, req pantheonrpc.PublishEventReq) error {
	var err error
	for i := 0; i < maxAttempts; i++ {
		err = th.PantheonClient.PublishEvent(ctx, req)
		if err == nil {
			return nil
		}
		time.Sleep(retryDelay * time.Duration(i+1))
	}
	return err
}

func (th *TaskHandler) GetRateLimitConfig() ratelimit.Config {
	return ratelimit.Config{
		Enabled: false,
	}
}
