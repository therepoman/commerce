package handler

import (
	"context"

	"code.justin.tv/commerce/payday/adminjob"
	"code.justin.tv/commerce/payday/adminjob/handler/bulkentitle"
	"code.justin.tv/commerce/payday/adminjob/handler/bulkpromonotif"
	"code.justin.tv/commerce/payday/adminjob/handler/lbbackfill"
	"code.justin.tv/commerce/payday/adminjob/handler/ratelimit"
	"code.justin.tv/commerce/payday/adminjob/handler/singlepurchasebackfill"
	"code.justin.tv/commerce/payday/adminjob/handler/test"
	"code.justin.tv/commerce/payday/adminjob/handler/transactionbackfill"
	"code.justin.tv/commerce/payday/errors"
)

type TaskHandler interface {
	ValidateInput(ctx context.Context, input adminjob.RawInput) error
	GetTasks(ctx context.Context, input adminjob.RawInput) ([]adminjob.TaskInput, error)
	GetTaskKey(ctx context.Context, idx adminjob.TaskIndex, input adminjob.TaskInput) (adminjob.TaskKey, error)
	Handle(ctx context.Context, input adminjob.TaskInput) (adminjob.TaskOutput, error)
	GetRateLimitConfig() ratelimit.Config
}

type HandlerFactory interface {
	GetTaskHandler(id adminjob.Identifier, t adminjob.Type, options adminjob.RawOptions) (TaskHandler, error)
}

type HandlerFactoryImpl struct {
	BulkEntitleHandlerFactory            bulkentitle.Factory            `inject:""`
	BulkPromoNotifHandlerFactory         bulkpromonotif.Factory         `inject:""`
	LeaderboardBackfillHandlerFactory    lbbackfill.Factory             `inject:""`
	TransactionsBackfillHandlerFactory   transactionbackfill.Factory    `inject:""`
	SinglePurchaseBackfillHandlerFactory singlepurchasebackfill.Factory `inject:""`
}

func (hf *HandlerFactoryImpl) GetTaskHandler(id adminjob.Identifier, t adminjob.Type, options adminjob.RawOptions) (TaskHandler, error) {
	switch t {
	case adminjob.Test:
		return test.New(options)
	case adminjob.BulkEntitle:
		return hf.BulkEntitleHandlerFactory.New(id, options)
	case adminjob.BulkPromoNotif:
		return hf.BulkPromoNotifHandlerFactory.New(id, options)
	case adminjob.LeaderboardBackfill:
		return hf.LeaderboardBackfillHandlerFactory.New(id, options)
	case adminjob.TransactionsBackfill:
		return hf.TransactionsBackfillHandlerFactory.New(id, options)
	case adminjob.SinglePurchaseBackfill:
		return hf.SinglePurchaseBackfillHandlerFactory.New(id, options)
	default:
		return nil, errors.New("No task handler for type")
	}
}
