package singlepurchasebackfill

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"

	"code.justin.tv/commerce/payday/adminjob"
	"code.justin.tv/commerce/payday/adminjob/handler/ratelimit"
	"code.justin.tv/commerce/payday/dynamo/single_purchase_consumption"
	petozi "code.justin.tv/commerce/petozi/rpc"
	"golang.org/x/time/rate"
)

type Factory interface {
	New(id adminjob.Identifier, rawOpt adminjob.RawOptions) (*entitlementsTaskHandler, error)
}

type singlePurchaseFactory struct {
	SinglePurchaseDAO single_purchase_consumption.SinglePurchaseConsumptionDAO `inject:""`
	PetoziClient      petozi.Petozi                                            `inject:""`
}

func NewFactory() Factory {
	return &singlePurchaseFactory{}
}

func (spf *singlePurchaseFactory) New(id adminjob.Identifier, rawOpt adminjob.RawOptions) (*entitlementsTaskHandler, error) {
	return &entitlementsTaskHandler{
		SinglePurchaseDAO: spf.SinglePurchaseDAO,
		PetoziClient:      spf.PetoziClient,
	}, nil
}

type entitlementsTaskHandler struct {
	SinglePurchaseDAO   single_purchase_consumption.SinglePurchaseConsumptionDAO
	PetoziClient        petozi.Petozi
	petoziProductByType map[string]*petozi.BitsProduct
}

type BitsEntitlement struct {
	UserID   string `json:"userID"`
	BitsType string `json:"bitsType"`
}

func (eth *entitlementsTaskHandler) ValidateInput(ctx context.Context, input adminjob.RawInput) error {
	var entitlements []BitsEntitlement
	return json.Unmarshal([]byte(input), &entitlements)
}

func (eth *entitlementsTaskHandler) GetTasks(ctx context.Context, input adminjob.RawInput) ([]adminjob.TaskInput, error) {
	tasks := make([]adminjob.TaskInput, 0)
	var entitlements []BitsEntitlement

	err := json.Unmarshal([]byte(input), &entitlements)
	if err != nil {
		return tasks, err
	}

	for _, entitlement := range entitlements {
		entitlementJson, err := json.Marshal(entitlement)
		if err != nil {
			return tasks, err
		}
		tasks = append(tasks, adminjob.TaskInput(string(entitlementJson)))
	}

	return tasks, nil
}

func (eth *entitlementsTaskHandler) GetTaskKey(ctx context.Context, idx adminjob.TaskIndex, input adminjob.TaskInput) (adminjob.TaskKey, error) {
	var entitlement BitsEntitlement
	err := json.Unmarshal([]byte(string(input)), &entitlement)
	if err != nil {
		return "", err
	}

	return adminjob.TaskKey(fmt.Sprintf("single-purchase-task-%d-%s-%s", int(idx), entitlement.UserID, entitlement.BitsType)), nil
}

func (eth *entitlementsTaskHandler) Handle(ctx context.Context, input adminjob.TaskInput) (adminjob.TaskOutput, error) {
	var entitlement BitsEntitlement
	err := json.Unmarshal([]byte(string(input)), &entitlement)
	if err != nil {
		return "", err
	}

	if entitlement.BitsType == "" {
		return "", nil
	}

	// load all products if we have not done so
	if eth.petoziProductByType == nil || len(eth.petoziProductByType) == 0 {
		eth.petoziProductByType = make(map[string]*petozi.BitsProduct)

		productsResp, err := eth.PetoziClient.GetBitsProducts(ctx, &petozi.GetBitsProductsReq{
			UseInMemoryCache: true,
		})
		if err != nil {
			return "", err
		}

		for _, product := range productsResp.BitsProducts {
			eth.petoziProductByType[product.BitsTypeId] = product
		}

		if len(eth.petoziProductByType) == 0 {
			return "", errors.New("found no bits products from petozi")
		}
	}

	product, ok := eth.petoziProductByType[entitlement.BitsType]
	if !ok || product == nil {
		return "", nil
	}

	if isSinglePurchasePromo(product) {
		_, err = eth.SinglePurchaseDAO.Create(ctx, entitlement.UserID, product.Id)
	}

	return "", err
}

func (eth *entitlementsTaskHandler) GetRateLimitConfig() ratelimit.Config {
	return ratelimit.Config{
		Enabled: true,
		Key:     "single-purchase-backfill-job",
		TPS:     rate.Limit(1000),
	}
}

func isSinglePurchasePromo(product *petozi.BitsProduct) bool {
	if product.Promo == nil {
		return false
	}

	return product.Promo.Type == petozi.PromoType_SINGLE_PURCHASE ||
		product.Promo.Type == petozi.PromoType_PRIME_SINGLE_PURCHASE ||
		product.Promo.Type == petozi.PromoType_FIRST_TIME_PURCHASE
}
