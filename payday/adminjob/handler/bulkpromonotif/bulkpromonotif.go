package bulkpromonotif

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"regexp"
	"strconv"
	"strings"
	"time"

	"code.justin.tv/chat/pushy/client/events"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/adminjob"
	"code.justin.tv/commerce/payday/adminjob/handler/ratelimit"
	"code.justin.tv/commerce/payday/bulk_promo_notif"
	"code.justin.tv/commerce/payday/clients/redis"
	"code.justin.tv/commerce/payday/hystrix"
	cmds "code.justin.tv/commerce/payday/hystrix"
	"code.justin.tv/commerce/payday/lock"
	go_redis "github.com/go-redis/redis"
	"golang.org/x/time/rate"
)

const pushyStatusSent = "SENT"

const userIDRegex = `(?i)^[0-9]+$`

var userIDRegexp = regexp.MustCompile(userIDRegex)

type Factory interface {
	New(id adminjob.Identifier, rawOpt adminjob.RawOptions) (*TaskHandler, error)
}

type FactoryImpl struct {
	Pushy         events.Publisher   `inject:""`
	LockingClient lock.LockingClient `inject:""`
	RedisClient   redis.Client       `inject:"redisClient"`
}

type Options struct {
	PromoNotifCampaignID string
}

type TaskHandler struct {
	Options       Options
	JobID         adminjob.Identifier
	Pushy         events.Publisher
	LockingClient lock.LockingClient
	RedisClient   redis.Client
}

func (f *FactoryImpl) New(id adminjob.Identifier, rawOpt adminjob.RawOptions) (*TaskHandler, error) {
	var opt Options
	err := json.Unmarshal([]byte(rawOpt), &opt)
	if err != nil {
		return nil, err
	}

	c := bulk_promo_notif.GetCampaign(opt.PromoNotifCampaignID)
	if c == nil {
		return nil, errors.New("invalid campaign id")
	}

	return &TaskHandler{
		Options:       opt,
		JobID:         id,
		Pushy:         f.Pushy,
		LockingClient: f.LockingClient,
		RedisClient:   f.RedisClient,
	}, nil
}

func (th *TaskHandler) ValidateInput(ctx context.Context, input adminjob.RawInput) error {
	lines := strings.Split(string(input), "\n")
	for _, u := range lines {
		u = strings.TrimSpace(u)
		if !userIDRegexp.MatchString(u) {
			return fmt.Errorf("invalid user id: %s", u)
		}
	}
	return nil
}

func (th *TaskHandler) GetTasks(ctx context.Context, input adminjob.RawInput) ([]adminjob.TaskInput, error) {
	lines := strings.Split(string(input), "\n")
	tasks := make([]adminjob.TaskInput, 0)
	for _, line := range lines {
		tasks = append(tasks, adminjob.TaskInput(strings.TrimSpace(line)))
	}
	return tasks, nil
}

func (th *TaskHandler) GetTaskKey(ctx context.Context, idx adminjob.TaskIndex, input adminjob.TaskInput) (adminjob.TaskKey, error) {
	return adminjob.TaskKey(fmt.Sprintf("%s.%s", strconv.Itoa(int(idx)), input)), nil
}

func (th *TaskHandler) Handle(ctx context.Context, input adminjob.TaskInput) (adminjob.TaskOutput, error) {
	userID := strings.TrimSpace(string(input))

	c := bulk_promo_notif.GetCampaign(th.Options.PromoNotifCampaignID)
	if c == nil {
		return "", errors.New("invalid campaign id")
	}

	userPromoKey := getPromoKey(*c, userID)

	userPromoLock, err := th.LockingClient.ObtainLock(fmt.Sprintf("%s-lock", userPromoKey))
	if err != nil {
		return "", err
	}
	defer func() {
		err = userPromoLock.Unlock()
		if err != nil {
			log.WithError(err).Error("error unlocking redis lock in pushy promo notif job handler")
		}
	}()

	redisPushyStatusKey := fmt.Sprintf("%s-pushy-status", userPromoKey)
	var pushyStatus string
	err = hystrix.Do(cmds.PushyStatusCacheGet, func() error {
		var err error
		pushyStatus, err = th.RedisClient.Get(ctx, redisPushyStatusKey).Result()
		if err != nil && err != go_redis.Nil {
			return err
		}
		return nil
	}, nil)
	if err != nil {
		return "", err
	}

	if pushyStatus == pushyStatusSent {
		return "", fmt.Errorf("user %s has already received a promo notification for campaign %s", userID, c.ID)
	}

	err = th.Pushy.Publish(ctx, events.BitsPromoEvent, events.BitsPromoParams{
		UserID:             userID,
		PromoID:            c.PushyPromoID,
		PromoTitle:         c.PromoTitle,
		BitsAmount:         c.BitsAmount,
		DiscountPercentage: c.Discount,
		IconImageURL:       c.IconURL,
		ClickActionURL:     c.ClickURL,
		StartDate:          &c.StartDate,
		EndDate:            &c.EndDate,
		Date:               c.Date,
	})
	if err != nil {
		return "", err
	}

	err = hystrix.Do(cmds.PushyStatusCacheSet, func() error {
		_, err := th.RedisClient.Set(ctx, redisPushyStatusKey, pushyStatusSent, 7*24*time.Hour).Result()
		return err
	}, nil)

	return "", err
}

func (th *TaskHandler) GetRateLimitConfig() ratelimit.Config {
	return ratelimit.Config{
		Enabled: true,
		Key:     "bulk-promo-notif-job-rate-limiting",
		TPS:     rate.Limit(100),
	}
}

func getPromoKey(campaign bulk_promo_notif.Campaign, userID string) string {
	return fmt.Sprintf("%s-%s", campaign.ID, userID)
}
