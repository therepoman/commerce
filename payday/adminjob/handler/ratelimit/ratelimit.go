package ratelimit

import (
	"golang.org/x/time/rate"
)

type Config struct {
	Enabled bool
	Key     string
	TPS     rate.Limit
}
