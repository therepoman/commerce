package bulkentitle

import (
	"context"
	"encoding/json"
	"fmt"
	"strconv"
	"strings"

	string_utils "code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/payday/admin/add_bits"
	"code.justin.tv/commerce/payday/adminjob"
	"code.justin.tv/commerce/payday/adminjob/handler/ratelimit"
	"code.justin.tv/commerce/payday/clients/user"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/models"
	apimodel "code.justin.tv/commerce/payday/models/api"
	"github.com/go-redis/redis_rate"
	"github.com/gofrs/uuid"
	"golang.org/x/time/rate"
)

const (
	uuidNamespace = "5c325dd5-29d0-4ac4-8c85-5250045e66b4"
)

type UserLookupMode string

const (
	UserLookupMode_ByLogin = UserLookupMode("UserLookupMode_ByLogin")
	UserLookupMode_ByID    = UserLookupMode("UserLookupMode_ByID")
)

var ValidUserLookupModes = map[UserLookupMode]interface{}{UserLookupMode_ByID: nil, UserLookupMode_ByLogin: nil}

type Factory interface {
	New(id adminjob.Identifier, rawOpt adminjob.RawOptions) (*TaskHandler, error)
}

type FactoryImpl struct {
	UserServiceClient user.IUserServiceClientWrapper `inject:""`
	AdminAddBits      add_bits.Processor             `inject:""`
}

type Options struct {
	BitsAmount          int32          `json:"bits_amount"`
	AdminReason         string         `json:"admin_reason"`
	UserLookupMode      UserLookupMode `json:"user_lookup_mode"`
	NotifyUser          bool           `json:"notify_user"`
	TypeOfBitsToAdd     string         `json:"type_of_bits_to_add"`
	Origin              string         `json:"origin"`
	BulkGrantCampaignID string         `json:"bulk_grant_campaign_id"`
}

type TaskHandler struct {
	UserServiceClient user.IUserServiceClientWrapper
	AdminAddBits      add_bits.Processor
	RateLimiter       *redis_rate.Limiter
	Options           Options
	JobID             adminjob.Identifier
}

func (f *FactoryImpl) New(id adminjob.Identifier, rawOpt adminjob.RawOptions) (*TaskHandler, error) {
	var opt Options
	err := json.Unmarshal([]byte(rawOpt), &opt)
	if err != nil {
		return nil, err
	}
	return &TaskHandler{
		UserServiceClient: f.UserServiceClient,
		AdminAddBits:      f.AdminAddBits,
		Options:           opt,
		JobID:             id,
	}, nil
}

func (th *TaskHandler) ValidateInput(ctx context.Context, input adminjob.RawInput) error {
	lines := strings.Split(string(input), "\n")
	users := make(map[string]interface{})
	for _, u := range lines {
		if _, exists := users[u]; exists {
			return errors.Newf("User %s appeared multiple times in input", u)
		}
		users[u] = nil
	}
	return nil
}

func (th *TaskHandler) GetTasks(ctx context.Context, input adminjob.RawInput) ([]adminjob.TaskInput, error) {
	lines := strings.Split(string(input), "\n")
	tasks := make([]adminjob.TaskInput, 0)
	for _, line := range lines {
		tasks = append(tasks, adminjob.TaskInput(strings.TrimSpace(line)))
	}
	return tasks, nil
}

func (th *TaskHandler) GetTaskKey(ctx context.Context, idx adminjob.TaskIndex, input adminjob.TaskInput) (adminjob.TaskKey, error) {
	return adminjob.TaskKey(fmt.Sprintf("%s.%s", strconv.Itoa(int(idx)), input)), nil
}

func (th *TaskHandler) Handle(ctx context.Context, input adminjob.TaskInput) (adminjob.TaskOutput, error) {
	userLookupMode := th.Options.UserLookupMode
	if _, ok := ValidUserLookupModes[userLookupMode]; !ok {
		userLookupMode = UserLookupMode_ByLogin
	}

	var userID string
	switch userLookupMode {
	case UserLookupMode_ByLogin:
		u, err := th.UserServiceClient.GetUserByLogin(ctx, string(input), nil)
		if err != nil {
			return "", errors.Notef(err, "Error getting user by login from user service")
		}
		if u == nil {
			return "", errors.Newf("User %s not found", input)
		}
		userID = u.ID
	case UserLookupMode_ByID:
		u, err := th.UserServiceClient.GetUserByID(ctx, string(input), nil)
		if err != nil {
			return "", errors.Notef(err, "Error getting user by id from user service")
		}
		if u == nil {
			return "", errors.Newf("User %s not found", input)
		}
		userID = u.ID
	}

	uuidNamespace, err := uuid.FromString(uuidNamespace)
	if err != nil {
		return "", errors.New("Could not create UUID namespace")
	}

	eventID := uuid.NewV5(uuidNamespace, fmt.Sprintf("%s.%s", th.JobID.String(), userID)).String()

	typeOfBitsToAdd := th.Options.TypeOfBitsToAdd
	if string_utils.Blank(th.Options.TypeOfBitsToAdd) && string_utils.Blank(th.Options.BulkGrantCampaignID) {
		typeOfBitsToAdd = string(models.CB_0)
	}

	err = th.AdminAddBits.Process(ctx, apimodel.AddBitsRequest{
		EventId:             eventID,
		TwitchUserId:        userID,
		AdminUserId:         th.JobID.Creator,
		AdminReason:         th.Options.AdminReason,
		AmountOfBitsToAdd:   th.Options.BitsAmount,
		TypeOfBitsToAdd:     typeOfBitsToAdd,
		EventReasonCode:     "",
		NotifyUser:          th.Options.NotifyUser,
		Origin:              th.Options.Origin,
		BulkGrantCampaignID: th.Options.BulkGrantCampaignID,
	})

	if err != nil {
		return "", err
	}

	return "", nil
}

func (th *TaskHandler) GetRateLimitConfig() ratelimit.Config {
	return ratelimit.Config{
		Enabled: true,
		Key:     "bulk-entitle-job-rate-limiting",
		TPS:     rate.Limit(30),
	}
}
