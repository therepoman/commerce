package test

import (
	"context"
	"encoding/json"
	"strconv"
	"strings"

	"code.justin.tv/commerce/payday/adminjob"
	"code.justin.tv/commerce/payday/adminjob/handler/ratelimit"
)

type Options struct {
	Suffix string `json:"suffix"`
}

type TaskHandler struct {
	Options Options
}

func New(rawOpt adminjob.RawOptions) (*TaskHandler, error) {
	var opt Options
	err := json.Unmarshal([]byte(rawOpt), &opt)
	if err != nil {
		return nil, err
	}
	return &TaskHandler{
		Options: opt,
	}, nil
}

func (th *TaskHandler) ValidateInput(ctx context.Context, input adminjob.RawInput) error {
	return nil
}

func (th *TaskHandler) GetTasks(ctx context.Context, input adminjob.RawInput) ([]adminjob.TaskInput, error) {
	lines := strings.Split(string(input), "\n")
	tasks := make([]adminjob.TaskInput, 0)
	for _, line := range lines {
		tasks = append(tasks, adminjob.TaskInput(line))
	}
	return tasks, nil
}

func (th *TaskHandler) GetTaskKey(ctx context.Context, idx adminjob.TaskIndex, input adminjob.TaskInput) (adminjob.TaskKey, error) {
	return adminjob.TaskKey(strconv.Itoa(int(idx))), nil
}

func (th *TaskHandler) Handle(ctx context.Context, input adminjob.TaskInput) (adminjob.TaskOutput, error) {
	return adminjob.TaskOutput(strings.ToUpper(string(input) + th.Options.Suffix)), nil
}

func (th *TaskHandler) GetRateLimitConfig() ratelimit.Config {
	return ratelimit.Config{
		Enabled: false,
	}
}
