package badges

import (
	"context"
	"fmt"
	"time"

	"code.justin.tv/chat/badges/app/models"
	"code.justin.tv/commerce/payday/clients/slack"
	"code.justin.tv/commerce/payday/clients/user"
	"code.justin.tv/commerce/payday/config"
	userUtils "code.justin.tv/commerce/payday/user"
	"code.justin.tv/commerce/payday/utils/strings"

	log "code.justin.tv/commerce/logrus"
)

type ICustomBitsBadgeSlacker interface {
	PostToSlack(channelId string, uploadResponse *models.BitsUploadImagesResponse)
}

type CustomBitsBadgeSlacker struct {
	UserService user.IUserServiceClientWrapper `inject:""`
	Slacker     slack.ISlacker                 `inject:""`
	Config      *config.Configuration          `inject:""`
}

type NoOpCustomBitsBadgeSlacker struct {
}

func (c *NoOpCustomBitsBadgeSlacker) PostToSlack(channelId string, uploadResponse *models.BitsUploadImagesResponse) {
}

func (c *CustomBitsBadgeSlacker) PostToSlack(channelId string, uploadResponse *models.BitsUploadImagesResponse) {
	if userUtils.IsATestAccount(channelId) {
		return
	}

	if c.Config == nil || strings.Blank(c.Config.CustomBadgesChannelSlackWebhookURL) {
		return
	}

	if uploadResponse == nil {
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	channel := channelId
	userResp, err := c.UserService.GetUserByID(ctx, channelId, nil)
	if err == nil && userResp != nil && userResp.Login != nil {
		channel = *userResp.Login
	}

	for version, resp := range uploadResponse.Versions {
		originalChatBadge := fmt.Sprintf(":cheer%sbadge:", version)
		text := fmt.Sprintf("%s badge uploaded in %s's channel with title \"%s\" %s", originalChatBadge, channel, resp.Title, originalChatBadge)
		msg := slack.SlackMsg{
			Attachments: []slack.SlackAttachment{
				{
					Pretext:  text,
					Fallback: text,
					ImageURL: resp.ImageURL4x,
				},
			},
		}
		err = c.Slacker.Post(c.Config.CustomBadgesChannelSlackWebhookURL, msg)
		if err != nil {
			log.WithError(err).Warn("Failed to post custom cheer badge to slack")
		}
	}
}
