package clips

import (
	"context"
	"fmt"
	"time"

	log "code.justin.tv/commerce/logrus"
	contextUtils "code.justin.tv/commerce/payday/utils/context"
	clips "code.justin.tv/video/clips-upload/client"
	"github.com/go-errors/errors"
)

const (
	clipStatusCreating      = "creating"
	clipStatusCreated       = "created"
	statusCheckAttemptDelay = time.Millisecond * 200
	clipsV3UserID           = "168177340"
)

type ClipsManager struct {
	ClipsClient clips.Client `inject:""`
}

func (cm *ClipsManager) getClipStatus(ctx context.Context, slug string) (string, error) {
	for !contextUtils.TimedOut(ctx) {
		clipStatus, err := cm.ClipsClient.GetClipStatusV3(ctx, slug, nil)
		if err != nil {
			log.Warnf("Failed to get clip status: %v", err)
			return "", err
		}

		if clipStatus.Status != clipStatusCreating {
			return clipStatus.Status, nil
		}
		time.Sleep(statusCheckAttemptDelay)
	}

	msg := fmt.Sprintf("Timed out waiting for clip to create. Slug: %s", slug)
	log.Warnf(msg)
	return "", errors.New(msg)
}

func (cm *ClipsManager) CreateClip(ctx context.Context, channelId string) (string, error) {
	ccResp, err := cm.ClipsClient.CreateLiveClipV3(ctx, &clips.CreateLiveClipParams{
		UserID:    clipsV3UserID,
		ChannelID: channelId,
	}, nil)
	if err != nil {
		log.Warnf("Error creating clip: %v", err)
		return "", err
	}

	status, err := cm.getClipStatus(ctx, ccResp.Slug)
	if err != nil {
		log.Warnf("Error getting clip status: %v", err)
		return "", err
	}

	if status != clipStatusCreated {
		msg := fmt.Sprintf("Failed to create clip. Slug: %s, Resulting Status: %s", ccResp.Slug, status)
		log.Warn(msg)
		return "", errors.New(msg)
	}

	return ccResp.URL, nil
}
