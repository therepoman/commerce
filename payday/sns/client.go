package sns

import (
	"context"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sns"
)

var MessageAttributesV1 = map[string]*sns.MessageAttributeValue{
	"Version": {
		DataType:    aws.String("Number"),
		StringValue: aws.String("1"),
	},
}

type SNSClient struct {
	sns *sns.SNS
}

type ISNSClient interface {
	PostToTopic(topic string, msg string) error
	PostToTopicWithMessageAttributes(topic string, msg string, msgAttributes map[string]*sns.MessageAttributeValue) error
	PostToTopicWithContext(ctx context.Context, topic string, msg string) error
}

func newDefaultConfig(region string) *aws.Config {
	return &aws.Config{
		Region: aws.String(region),
	}
}

func NewSnsClient(region string) *SNSClient {
	sess, _ := session.NewSession()
	return &SNSClient{
		sns: sns.New(sess, newDefaultConfig(region)),
	}
}

func (s *SNSClient) PostToTopic(topic string, msg string) error {
	pi := &sns.PublishInput{
		Message:  aws.String(msg),
		TopicArn: aws.String(topic),
	}
	_, err := s.sns.Publish(pi)
	return err
}

func (s *SNSClient) PostToTopicWithMessageAttributes(topic string, msg string, msgAttributes map[string]*sns.MessageAttributeValue) error {
	pi := &sns.PublishInput{
		Message:           aws.String(msg),
		TopicArn:          aws.String(topic),
		MessageAttributes: msgAttributes,
	}
	_, err := s.sns.Publish(pi)
	return err
}

func (s *SNSClient) PostToTopicWithContext(ctx context.Context, topic string, msg string) error {
	pi := &sns.PublishInput{
		Message:  aws.String(msg),
		TopicArn: aws.String(topic),
	}
	_, err := s.sns.PublishWithContext(ctx, pi)
	return err
}
