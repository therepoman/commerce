package prices

import (
	"context"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/cache"
	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/hystrix"
	cmds "code.justin.tv/commerce/payday/hystrix"
	"code.justin.tv/commerce/payday/metrics"
	"code.justin.tv/commerce/payday/models"
	"code.justin.tv/commerce/payday/products"
	petozi "code.justin.tv/commerce/petozi/rpc"
	flotwirp "code.justin.tv/revenue/flo/rpc"
	"github.com/cenkalti/backoff"
)

const (
	floPricesCacheHitMetric   = "FloPricesCacheHit"
	floPricesCacheHitsLatency = "FloPricesCacheHitLatency"
	floPricesAttempts         = "FloPricesAttempts"
)

type IFloPricer interface {
	GetPrices(ctx context.Context, productIds []string, tuid string, corStr string) ([]models.Price, *string, error)
}

type FloPricer struct {
	FloClient      flotwirp.Flo          `inject:""`
	PricesCache    cache.FloPricesCache  `inject:""`
	Statter        metrics.Statter       `inject:""`
	Config         *config.Configuration `inject:""`
	ProductsGetter products.Getter       `inject:""`
}

func (this *FloPricer) GetPrices(ctx context.Context, productIds []string, tuid string, corStr string) ([]models.Price, *string, error) {
	start := time.Now()

	floPrices, floCountry := this.PricesCache.Get(ctx, productIds, tuid, corStr)
	this.Statter.TimingDuration(floPricesCacheHitsLatency, time.Since(start))

	if len(floPrices) > 0 {
		this.Statter.Inc(floPricesCacheHitMetric, 1)
		return floPrices, floCountry, nil
	} else {
		this.Statter.Inc(floPricesCacheHitMetric, 0)
	}

	pricingIds, mapToSkus := this.convertSkusToPricingId(ctx, productIds)

	tries := 0

	f := func() error {
		tries++
		return hystrix.Do(cmds.GetFloPricesCommand, func() error {
			var err error
			response, err := this.FloClient.GetPreviewPrices(ctx, &flotwirp.GetPreviewPricesRequest{
				UserId:        tuid,
				IpCountryCode: corStr,
				PricingIds:    pricingIds,
			})
			if err != nil {
				return err
			}

			floPrices = this.convertPrices(response.Pricings, mapToSkus)
			floCountry = &response.PricingCountryCode

			go func() {
				this.PricesCache.Set(ctx, tuid, floPrices, response.PricingCountryCode, corStr)
			}()

			return nil
		}, nil)
	}

	var b backoff.BackOff = backoff.WithContext(newBackoff(), ctx)
	b = backoff.WithMaxTries(b, 3)

	defer this.Statter.Inc(floPricesAttempts, int64(tries))

	err := backoff.Retry(f, b)
	if err != nil {
		log.WithError(err).Error("Flo Service Error")
		return nil, nil, err
	}

	cor := &corStr
	if floCountry != nil {
		cor = floCountry
	}

	return floPrices, cor, nil
}

func (this *FloPricer) convertPrices(pricings map[string]*flotwirp.Pricing, mapToSkus map[string]string) []models.Price {
	modelsPrices := make([]models.Price, 0)
	for _, pricing := range pricings {
		if pricing != nil {
			modelsPrices = append(modelsPrices, models.Price{
				ProductId:    mapToSkus[pricing.Id],
				Price:        models.FormatPriceFromFlo(pricing),
				Currency:     pricing.Currency,
				TaxInclusive: pricing.TaxInclusive,
			})
		}
	}
	return modelsPrices
}

func (this *FloPricer) convertSkusToPricingId(ctx context.Context, productIds []string) ([]string, map[string]string) {
	pricingIds := make([]string, 0)
	pricingIdToProductIdMap := make(map[string]string)

	// I assume Paypal here because we only hit this for web prices, and Paypal is the default
	bitsProducts, err := this.ProductsGetter.GetByIDs(ctx, petozi.Platform_PAYPAL, productIds)
	if err != nil || bitsProducts == nil {
		return pricingIds, pricingIdToProductIdMap
	}

	for _, element := range productIds {
		bitsBundle, ok := bitsProducts[element]
		if ok {
			pricingIds = append(pricingIds, bitsBundle.PricingId)
			pricingIdToProductIdMap[bitsBundle.PricingId] = element
		}
	}

	return pricingIds, pricingIdToProductIdMap
}

func newBackoff() backoff.BackOff {
	exponetial := backoff.NewExponentialBackOff()
	exponetial.InitialInterval = 10 * time.Millisecond
	exponetial.MaxElapsedTime = 1 * time.Second
	exponetial.Multiplier = 2
	return exponetial
}
