package prices

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/payday/config"
	cache_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/cache"
	metrics_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/metrics"
	products_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/commerce/payday/products"
	flo_mock "code.justin.tv/commerce/payday/mocks/code.justin.tv/revenue/flo/rpc"
	"code.justin.tv/commerce/payday/models"
	petozi "code.justin.tv/commerce/petozi/rpc"
	flotwirp "code.justin.tv/revenue/flo/rpc"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestMappings(t *testing.T) {
	Convey("When pricer is configued with bits bundles", t, func() {
		productsGetter := new(products_mock.Getter)

		pricer := FloPricer{
			Config:         &config.Configuration{},
			ProductsGetter: productsGetter,
		}

		ctx := context.Background()
		platform := petozi.Platform_PAYPAL
		productIDs := []string{"taco"}

		Convey("when we fail to get any products", func() {
			productsGetter.On("GetByIDs", ctx, platform, productIDs).Return(nil, errors.New("WALRUS STRIKE"))

			ids, _ := pricer.convertSkusToPricingId(ctx, productIDs)
			So(ids, ShouldHaveLength, 0)
		})

		Convey("when we get no products back", func() {
			productsGetter.On("GetByIDs", ctx, platform, productIDs).Return(nil, nil)

			Convey("when it doesn't exist we shouldn't get any conversion", func() {
				ids, _ := pricer.convertSkusToPricingId(ctx, productIDs)
				So(ids, ShouldHaveLength, 0)
			})

			Convey("when it doesn't exist we shouldn't get any mapping", func() {
				_, mapping := pricer.convertSkusToPricingId(ctx, productIDs)
				So(mapping, ShouldHaveLength, 0)
			})
		})

		Convey("when we get products back", func() {
			productsGetter.On("GetByIDs", ctx, platform, productIDs).Return(map[string]*petozi.BitsProduct{
				"taco": {
					Platform:  platform,
					Id:        "taco",
					Quantity:  100,
					PricingId: "TACO-PRICING",
				},
			}, nil)

			Convey("id should be converted based on config", func() {
				ids, _ := pricer.convertSkusToPricingId(ctx, productIDs)
				So(ids[0], ShouldEqual, "TACO-PRICING")
			})

			Convey("mapping should be generated based on both", func() {
				_, mapping := pricer.convertSkusToPricingId(ctx, productIDs)
				So(mapping, ShouldContainKey, "TACO-PRICING")
				So(mapping["TACO-PRICING"], ShouldEqual, "taco")
			})
		})
	})
}

func TestConversionOfPricing(t *testing.T) {
	Convey("When pricer is configued with nothing", t, func() {
		pricer := FloPricer{}

		Convey("id should be converted based on config", func() {
			prices := pricer.convertPrices(
				map[string]*flotwirp.Pricing{
					"TACO-PRICING": {
						Id:           "TACO-PRICING",
						Price:        500,
						Currency:     "USD",
						TaxInclusive: false,
						Exponent:     2,
					},
				},
				map[string]string{
					"TACO-PRICING": "taco",
				})

			So(prices[0].ProductId, ShouldEqual, "taco")
			So(prices[0].Price, ShouldEqual, "5.00")
			So(prices[0].Currency, ShouldEqual, "USD")
			So(prices[0].TaxInclusive, ShouldBeFalse)
		})

		Convey("nil entries should be ignored", func() {
			prices := pricer.convertPrices(
				map[string]*flotwirp.Pricing{
					"TACO-PRICING": nil,
				},
				map[string]string{
					"TACO-PRICING": "taco",
				})

			So(prices, ShouldHaveLength, 0)
		})
	})
}

func TestPricing(t *testing.T) {
	Convey("When pricer is configured with nothing", t, func() {

		statter := new(metrics_mock.Statter)
		statter.On("Inc", mock.Anything, mock.Anything).Return()
		statter.On("TimingDuration", mock.Anything, mock.Anything).Return()

		floCache := new(cache_mock.FloPricesCache)
		floCache.On("Set", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything)
		floCache.On("Delete", mock.Anything, mock.Anything, mock.Anything)

		flo := new(flo_mock.Flo)

		productsGetter := new(products_mock.Getter)

		pricer := FloPricer{
			FloClient:      flo,
			Statter:        statter,
			PricesCache:    floCache,
			Config:         &config.Configuration{},
			ProductsGetter: productsGetter,
		}

		userId := "bonta"
		cor := "US"
		cachedCor := "JP"
		productId := "taco"

		productsGetter.On("GetByIDs", mock.Anything, petozi.Platform_PAYPAL, []string{productId}).Return(map[string]*petozi.BitsProduct{
			productId: {
				Platform:  petozi.Platform_PAYPAL,
				Id:        productId,
				Quantity:  100,
				PricingId: "TACO-PRICING",
			},
		}, nil)

		Convey("Cache contains entry", func() {
			floCache.On("Get", mock.Anything, []string{productId}, userId, cor).Return([]models.Price{
				{
					ProductId:    "taco",
					Price:        "5.00",
					Currency:     "USD",
					TaxInclusive: false,
				},
			}, &cachedCor)

			prices, cor, err := pricer.GetPrices(context.Background(), []string{productId}, userId, cor)

			So(err, ShouldBeNil)
			So(prices, ShouldHaveLength, 1)

			So(prices[0].ProductId, ShouldEqual, productId)
			So(prices[0].Price, ShouldEqual, "5.00")
			So(prices[0].Currency, ShouldEqual, "USD")
			So(prices[0].TaxInclusive, ShouldBeFalse)
			So(*cor, ShouldEqual, cachedCor)
		})

		Convey("Cache doesn't contain entry", func() {
			floCache.On("Get", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return([]models.Price{}, nil)

			Convey("Flo returns pricing", func() {
				flo.On("GetPreviewPrices", mock.Anything, mock.Anything).Return(&flotwirp.GetPreviewPricesResponse{
					Pricings: map[string]*flotwirp.Pricing{
						"TACO-PRICING": {
							Id:           "TACO-PRICING",
							Price:        140,
							Currency:     "USD",
							TaxInclusive: false,
							Exponent:     2,
						},
					},
					PricingCountryCode: "NZ",
				}, nil)

				prices, cor, err := pricer.GetPrices(context.Background(), []string{productId}, userId, cor)

				So(err, ShouldBeNil)
				So(prices, ShouldHaveLength, 1)

				So(prices[0].ProductId, ShouldEqual, productId)
				So(prices[0].Price, ShouldEqual, "1.40")
				So(prices[0].Currency, ShouldEqual, "USD")
				So(prices[0].TaxInclusive, ShouldBeFalse)
				So(*cor, ShouldEqual, "NZ")
			})
		})
	})
}
