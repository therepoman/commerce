package math

import "math/big"

func MaxInt(x int, y int) int {
	if x > y {
		return x
	}

	return y
}

func MaxInt64(x int64, y int64) int64 {
	if x > y {
		return x
	}

	return y
}

func MinInt(x int, y int) int {
	if x < y {
		return x
	}

	return y
}

func MinInt64(x int64, y int64) int64 {
	if x < y {
		return x
	}

	return y
}

func MaxBigFloat(x *big.Float, y *big.Float) *big.Float {
	if x.Cmp(y) > 0 {
		return x
	}

	return y
}

func MinBigFloat(x *big.Float, y *big.Float) *big.Float {
	if x.Cmp(y) < 0 {
		return x
	}

	return y
}

// Restricts the input value between a given minimum and maximum
func ClampBigFloat(in *big.Float, min *big.Float, max *big.Float) *big.Float {
	return MaxBigFloat(MinBigFloat(in, max), min)
}

func Uint64ToBigFloat(val uint64) *big.Float {
	bf := new(big.Float)
	return bf.SetUint64(val)
}

func Int64ToBigFloat(val int64) *big.Float {
	bf := new(big.Float)
	return bf.SetInt64(val)
}
