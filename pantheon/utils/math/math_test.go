package math

import (
	"math"
	"testing"

	"math/big"

	. "github.com/smartystreets/goconvey/convey"
)

func TestMaxInt(t *testing.T) {
	Convey("Test that various combinations of integers pick the correct max value", t, func() {
		So(MaxInt(1, 2), ShouldEqual, 2)
		So(MaxInt(2, 1), ShouldEqual, 2)
		So(MaxInt(0, 0), ShouldEqual, 0)
		So(MaxInt(0, 1), ShouldEqual, 1)
		So(MaxInt(1, 0), ShouldEqual, 1)
		So(MaxInt(-2, 1), ShouldEqual, 1)
		So(MaxInt(1, -2), ShouldEqual, 1)
		So(MaxInt(math.MaxInt32, -math.MinInt32), ShouldEqual, -math.MinInt32)
		So(MaxInt(-math.MaxInt32, math.MinInt32), ShouldEqual, -math.MaxInt32)
	})
}

func TestMinInt(t *testing.T) {
	Convey("Test that various combinations of integers pick the correct max value", t, func() {
		So(MinInt(1, 2), ShouldEqual, 1)
		So(MinInt(2, 1), ShouldEqual, 1)
		So(MinInt(0, 0), ShouldEqual, 0)
		So(MinInt(0, 1), ShouldEqual, 0)
		So(MinInt(1, 0), ShouldEqual, 0)
		So(MinInt(-2, 1), ShouldEqual, -2)
		So(MinInt(1, -2), ShouldEqual, -2)
		So(MinInt(math.MaxInt32, -math.MinInt32), ShouldEqual, math.MaxInt32)
		So(MinInt(-math.MaxInt32, math.MinInt32), ShouldEqual, math.MinInt32)
	})
}

func validateMaxBigFloat(x *big.Float, y *big.Float, expected *big.Float) {
	So(MaxBigFloat(x, y).Cmp(expected), ShouldEqual, 0)
}

func TestMaxBigFloat(t *testing.T) {
	Convey("Test that various combinations of big floats pick the correct max value", t, func() {
		validateMaxBigFloat(big.NewFloat(1), big.NewFloat(2), big.NewFloat(2))
		validateMaxBigFloat(big.NewFloat(2), big.NewFloat(1), big.NewFloat(2))
		validateMaxBigFloat(big.NewFloat(0), big.NewFloat(0), big.NewFloat(0))
		validateMaxBigFloat(big.NewFloat(.0000000000001), big.NewFloat(0), big.NewFloat(.0000000000001))
		validateMaxBigFloat(big.NewFloat(0), big.NewFloat(.0000000000001), big.NewFloat(.0000000000001))
		validateMaxBigFloat(big.NewFloat(-.0000000000001), big.NewFloat(0), big.NewFloat(0))
		validateMaxBigFloat(big.NewFloat(0), big.NewFloat(-.0000000000001), big.NewFloat(0))
		validateMaxBigFloat(big.NewFloat(math.MaxFloat64), big.NewFloat(-math.MaxFloat64), big.NewFloat(math.MaxFloat64))
		validateMaxBigFloat(big.NewFloat(-math.MaxFloat64), big.NewFloat(math.MaxFloat64), big.NewFloat(math.MaxFloat64))

	})
}

func validateMinBigFloat(x *big.Float, y *big.Float, expected *big.Float) {
	So(MinBigFloat(x, y).Cmp(expected), ShouldEqual, 0)
}

func TestMinBigFloat(t *testing.T) {
	Convey("Test that various combinations of big floats pick the correct min value", t, func() {
		validateMinBigFloat(big.NewFloat(1), big.NewFloat(2), big.NewFloat(1))
		validateMinBigFloat(big.NewFloat(2), big.NewFloat(1), big.NewFloat(1))
		validateMinBigFloat(big.NewFloat(0), big.NewFloat(0), big.NewFloat(0))
		validateMinBigFloat(big.NewFloat(.0000000000001), big.NewFloat(0), big.NewFloat(0))
		validateMinBigFloat(big.NewFloat(0), big.NewFloat(.0000000000001), big.NewFloat(0))
		validateMinBigFloat(big.NewFloat(-.0000000000001), big.NewFloat(0), big.NewFloat(-.0000000000001))
		validateMinBigFloat(big.NewFloat(0), big.NewFloat(-.0000000000001), big.NewFloat(-.0000000000001))
		validateMinBigFloat(big.NewFloat(math.MaxFloat64), big.NewFloat(-math.MaxFloat64), big.NewFloat(-math.MaxFloat64))
		validateMinBigFloat(big.NewFloat(-math.MaxFloat64), big.NewFloat(math.MaxFloat64), big.NewFloat(-math.MaxFloat64))
	})
}

func validateClampBigFloat(x *big.Float, min *big.Float, max *big.Float, expected *big.Float) {
	So(ClampBigFloat(x, min, max).Cmp(expected), ShouldEqual, 0)
}

func TestClampBigFloat(t *testing.T) {
	Convey("Test that various combinations of big floats pick the correct clamped value", t, func() {
		validateClampBigFloat(big.NewFloat(-math.MaxFloat64), big.NewFloat(-1), big.NewFloat(1), big.NewFloat(-1))
		validateClampBigFloat(big.NewFloat(-1.0000000001), big.NewFloat(-1), big.NewFloat(1), big.NewFloat(-1))
		validateClampBigFloat(big.NewFloat(-1), big.NewFloat(-1), big.NewFloat(1), big.NewFloat(-1))
		validateClampBigFloat(big.NewFloat(0), big.NewFloat(-1), big.NewFloat(1), big.NewFloat(0))
		validateClampBigFloat(big.NewFloat(1), big.NewFloat(-1), big.NewFloat(1), big.NewFloat(1))
		validateClampBigFloat(big.NewFloat(1.00000000001), big.NewFloat(-1), big.NewFloat(1), big.NewFloat(1))
		validateClampBigFloat(big.NewFloat(math.MaxFloat64), big.NewFloat(-1), big.NewFloat(1), big.NewFloat(1))
	})
}
