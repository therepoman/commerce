package random

import (
	"math/rand"
	"time"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

var numericRunes = []rune("0123456789")
var alphanumericRunes = []rune("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

func Int64(min int64, max int64) int64 {
	return rand.Int63n(max-min+1) + min
}

func Int(min int, max int) int {
	return rand.Intn(max-min+1) + min
}

func AlphanumericRune() rune {
	return alphanumericRunes[rand.Intn(len(alphanumericRunes))]
}

func NumericRune() rune {
	return numericRunes[rand.Intn(len(numericRunes))]
}

func String(size int) string {
	chars := make([]rune, size)
	for i := 0; i < size; i++ {
		chars[i] = AlphanumericRune()
	}
	return string(chars)
}

func NumberString(size int) string {
	chars := make([]rune, size)
	for i := 0; i < size; i++ {
		chars[i] = NumericRune()
	}
	return string(chars)
}

func Duration(minDur time.Duration, maxDur time.Duration) time.Duration {
	minNs := minDur.Nanoseconds()
	maxNs := maxDur.Nanoseconds()
	randNs := Int64(minNs, maxNs)
	return time.Duration(randNs)
}

func Time(maxSub time.Duration) time.Time {
	randDur := Duration(0*time.Nanosecond, maxSub)
	return time.Now().Add(-randDur)
}
