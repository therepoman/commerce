package time

import (
	"testing"
	"time"

	. "github.com/smartystreets/goconvey/convey"
)

func TestDurationToMilliseconds(t *testing.T) {
	Convey("Test that various durations convert to the correct number of milliseconds", t, func() {
		So(DurationToMilliseconds(0*time.Nanosecond), ShouldEqual, 0)
		So(DurationToMilliseconds(time.Nanosecond), ShouldEqual, 0)
		So(DurationToMilliseconds(1000*time.Nanosecond), ShouldEqual, 0)
		So(DurationToMilliseconds(1000*1000*time.Nanosecond), ShouldEqual, 1)

		So(DurationToMilliseconds(1*time.Millisecond), ShouldEqual, 1)
		So(DurationToMilliseconds(999*time.Millisecond), ShouldEqual, 999)

		So(DurationToMilliseconds(1*time.Second), ShouldEqual, 1000)
		So(DurationToMilliseconds(999*time.Second), ShouldEqual, 999*1000)

		So(DurationToMilliseconds(1*time.Minute), ShouldEqual, 60*1000)
		So(DurationToMilliseconds(20*time.Minute), ShouldEqual, 20*60*1000)

		So(DurationToMilliseconds(1*time.Hour), ShouldEqual, 60*60*1000)
		So(DurationToMilliseconds(12*time.Hour), ShouldEqual, 12*60*60*1000)
	})
}
