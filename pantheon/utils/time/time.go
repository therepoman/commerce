package time

import "time"

var defaultLoc *time.Location

func init() {
	loc, err := time.LoadLocation("America/Los_Angeles")
	if err != nil {
		panic(err)
	}
	defaultLoc = loc
}

func DurationToMilliseconds(duration time.Duration) int64 {
	return duration.Nanoseconds() / int64(time.Millisecond)
}

func NanosecondsToTime(nanoseconds uint64) time.Time {
	return time.Unix(0, int64(nanoseconds))
}

func GetTimeZone() *time.Location {
	return defaultLoc
}
