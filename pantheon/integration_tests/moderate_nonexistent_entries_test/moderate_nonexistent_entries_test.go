// +build integration

package moderate_existing_entries_test

import (
	"fmt"
	"strings"
	"testing"
	"time"

	"code.justin.tv/commerce/gogogadget/random"

	"code.justin.tv/commerce/pantheon/integration_tests/common"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	"github.com/gofrs/uuid"
	. "github.com/smartystreets/goconvey/convey"
)

const (
	numEvents = 100

	timeToWaitForLeaderbordsToIngest = 120 * time.Second
	getLeaderboardCallDelay          = 2 * time.Second
)

func TestModerateNonExistentEntries(t *testing.T) {
	Convey("Given a pantheon client", t, func() {
		testRunUUID, err := uuid.NewV4()
		So(err, ShouldBeNil)
		testRunID := strings.Replace(testRunUUID.String(), "-", "", -1)

		pantheonClient := common.GetPantheonClient()

		expectedEntries := make(map[string]int64)
		domain := fmt.Sprintf("IntegrationTests_TestModerateNonExistentEntries_RunID%s", testRunID)

		groupingKey := "GK"

		for i := 1; i <= numEvents; i++ {
			expectedEntries[fmt.Sprintf("EntryKey%d", i)] = 0
		}

		Convey("All non-existent entries should successfully be moderated", func() {
			for entryKey := range expectedEntries {
				common.ModerateEntry(pantheonClient, domain, groupingKey, entryKey, pantheonrpc.ModerationAction_MODERATE)
			}
			for timeUnitInt := range pantheonrpc.TimeUnit_name {
				timeUnit := pantheonrpc.TimeUnit(timeUnitInt)
				common.WaitForLeaderboardToIngest(pantheonClient, domain, groupingKey, timeUnit, map[string]int64{}, timeToWaitForLeaderbordsToIngest, getLeaderboardCallDelay)
			}

			Convey("Events should successfully ingest for the moderated entries", func() {
				eventValue := random.Int64(1, 10000)
				for entry := range expectedEntries {
					common.IngestEvent(pantheonClient, domain, groupingKey, entry, "", eventValue)
					expectedEntries[entry] += eventValue
				}

				for timeUnitInt := range pantheonrpc.TimeUnit_name {
					timeUnit := pantheonrpc.TimeUnit(timeUnitInt)
					common.WaitForLeaderboardToIngest(pantheonClient, domain, groupingKey, timeUnit, map[string]int64{}, timeToWaitForLeaderbordsToIngest, getLeaderboardCallDelay)
				}

				Convey("All entries should successfully be unmoderated", func() {
					for entry := range expectedEntries {
						common.ModerateEntry(pantheonClient, domain, groupingKey, entry, pantheonrpc.ModerationAction_UNMODERATE)
					}

					for timeUnitInt := range pantheonrpc.TimeUnit_name {
						timeUnit := pantheonrpc.TimeUnit(timeUnitInt)
						common.WaitForLeaderboardToIngest(pantheonClient, domain, groupingKey, timeUnit, expectedEntries, timeToWaitForLeaderbordsToIngest, getLeaderboardCallDelay)
					}
				})
			})
		})
	})
}
