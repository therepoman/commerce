// +build integration

package ingest_many_events_and_paginate_test

import (
	"context"
	"errors"
	"fmt"
	"strings"
	"testing"
	"time"

	"code.justin.tv/commerce/gogogadget/random"
	"code.justin.tv/commerce/pantheon/integration_tests/common"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	"github.com/gofrs/uuid"
	. "github.com/smartystreets/goconvey/convey"
)

const (
	numEvents = 100

	timeToWaitForLeaderbordsToIngest = 120 * time.Second
	getLeaderboardCallDelay          = 2 * time.Second
	maxTimeToWaitOnPagination        = 120 * time.Second
)

func TestIngestManyEventsAndPaginate(t *testing.T) {
	Convey("Given a pantheon client", t, func() {
		testRunUUID, err := uuid.NewV4()
		So(err, ShouldBeNil)
		testRunID := strings.Replace(testRunUUID.String(), "-", "", -1)

		pantheonClient := common.GetPantheonClient()

		expectedEntries := make(map[string]int64)
		domain := fmt.Sprintf("IntegrationTests_TestIngestManyEventsAndPaginate_RunID%s", testRunID)
		groupingKey := "GroupingKey"

		Convey("Events should successfully ingest", func() {
			for i := 1; i <= numEvents; i++ {
				entryKey := fmt.Sprintf("EntryKey%d", i)
				eventValue := random.Int64(1, 10000)
				common.IngestEvent(pantheonClient, domain, groupingKey, fmt.Sprintf("EntryKey%d", i), "", eventValue)
				expectedEntries[entryKey] = eventValue
			}

			Convey("Should be able to paginate through the leaderboard using the context entry key", func() {
				for timeUnitInt := range pantheonrpc.TimeUnit_name {
					timeUnit := pantheonrpc.TimeUnit(timeUnitInt)
					getResp := common.WaitForLeaderboardToIngest(pantheonClient, domain, groupingKey, timeUnit, expectedEntries, timeToWaitForLeaderbordsToIngest, getLeaderboardCallDelay)

					entriesFound := make(map[string]interface{})
					contextEntryKey := getResp.Top[0].EntryKey
					paginationStart := time.Now()
					for {
						resp, err := pantheonClient.GetLeaderboard(context.Background(), &pantheonrpc.GetLeaderboardReq{
							Domain:          domain,
							GroupingKey:     groupingKey,
							TimeUnit:        timeUnit,
							ContextN:        10,
							ContextEntryKey: contextEntryKey,
						})
						So(err, ShouldBeNil)
						So(resp, ShouldNotBeNil)
						So(resp.EntryContext.Entry.EntryKey, ShouldEqual, contextEntryKey)
						So(resp.EntryContext.Entry.Score, ShouldEqual, expectedEntries[contextEntryKey])

						for _, entry := range resp.EntryContext.Context {
							So(entry.Score, ShouldEqual, expectedEntries[entry.EntryKey])
							contextEntryKey = entry.EntryKey
							entriesFound[entry.EntryKey] = nil
						}
						if len(entriesFound) == numEvents {
							break
						}
						if time.Since(paginationStart) > maxTimeToWaitOnPagination {
							So(errors.New(fmt.Sprintf("timed out waiting on pagination (found %d / %d events)", len(entriesFound), numEvents)), ShouldBeNil)
							break
						}
						time.Sleep(200 * time.Millisecond)
					}
				}
			})
		})
	})
}
