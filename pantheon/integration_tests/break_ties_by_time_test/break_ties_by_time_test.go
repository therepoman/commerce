// +build integration

package break_ties_by_time_test

import (
	"fmt"
	"strings"
	"testing"
	"time"

	"code.justin.tv/commerce/pantheon/integration_tests/common"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	"github.com/gofrs/uuid"
	. "github.com/smartystreets/goconvey/convey"
)

const (
	numEvents = 100

	timeToWaitForLeaderbordsToIngest = 120 * time.Second
	getLeaderboardCallDelay          = 2 * time.Second
)

func TestBreakTiesByTime(t *testing.T) {
	Convey("Given a pantheon client", t, func() {
		testRunUUID, err := uuid.NewV4()
		So(err, ShouldBeNil)
		testRunID := strings.Replace(testRunUUID.String(), "-", "", -1)

		pantheonClient := common.GetPantheonClient()

		expectedEntries := make(map[string]int64)
		domain := fmt.Sprintf("IntegrationTests_TestBreakTiesByTime_RunID%s", testRunID)

		groupingKey := "GK"

		Convey("Events should successfully ingest and break ties based on time ingested", func() {
			for i := 1; i <= numEvents; i++ {
				eventValue := int64(1)
				entryKey := fmt.Sprintf("EntryKey%d", i)
				common.IngestEvent(pantheonClient, domain, groupingKey, entryKey, "", eventValue)
				expectedEntries[entryKey] = eventValue
				time.Sleep(50 * time.Millisecond)
			}

			for timeUnitInt := range pantheonrpc.TimeUnit_name {
				timeUnit := pantheonrpc.TimeUnit(timeUnitInt)
				getResp := common.WaitForLeaderboardToIngest(pantheonClient, domain, groupingKey, timeUnit, expectedEntries, timeToWaitForLeaderbordsToIngest, getLeaderboardCallDelay)

				for i := 1; i <= numEvents; i++ {
					entryKey := fmt.Sprintf("EntryKey%d", i)
					actualEntry := getResp.Top[i-1]
					So(actualEntry.EntryKey, ShouldEqual, entryKey)
					So(actualEntry.Score, ShouldEqual, 1)
					So(actualEntry.Rank, ShouldEqual, i)
				}
			}
		})
	})
}
