// +build integration

package moderate_existing_entries_test

import (
	"fmt"
	"strings"
	"testing"
	"time"

	"code.justin.tv/commerce/gogogadget/random"
	"code.justin.tv/commerce/pantheon/integration_tests/common"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	"github.com/gofrs/uuid"
	. "github.com/smartystreets/goconvey/convey"
)

const (
	numEvents = 100

	timeToWaitForLeaderbordsToIngest = 120 * time.Second
	getLeaderboardCallDelay          = 2 * time.Second
)

func TestModerateExistingEntries(t *testing.T) {
	Convey("Given a pantheon client", t, func() {
		testRunUUID, err := uuid.NewV4()
		So(err, ShouldBeNil)
		testRunID := strings.Replace(testRunUUID.String(), "-", "", -1)

		pantheonClient := common.GetPantheonClient()

		expectedEntries := make(map[string]int64)
		domain := fmt.Sprintf("IntegrationTests_TestModerateExistingEntries_RunID%s", testRunID)
		groupingKey := "GK"

		Convey("Events should successfully ingest", func() {
			for i := 1; i <= numEvents; i++ {
				entryKey := fmt.Sprintf("EntryKey%d", i)
				eventValue := random.Int64(1, 10000)
				common.IngestEvent(pantheonClient, domain, groupingKey, fmt.Sprintf("EntryKey%d", i), "", eventValue)
				expectedEntries[entryKey] = eventValue
			}

			for timeUnitInt := range pantheonrpc.TimeUnit_name {
				timeUnit := pantheonrpc.TimeUnit(timeUnitInt)
				common.WaitForLeaderboardToIngest(pantheonClient, domain, groupingKey, timeUnit, expectedEntries, timeToWaitForLeaderbordsToIngest, getLeaderboardCallDelay)
			}

			Convey("All entries should successfully be moderated", func() {
				for entry := range expectedEntries {
					common.ModerateEntry(pantheonClient, domain, groupingKey, entry, pantheonrpc.ModerationAction_MODERATE)
				}

				for timeUnitInt := range pantheonrpc.TimeUnit_name {
					timeUnit := pantheonrpc.TimeUnit(timeUnitInt)

					// Wait until all entries are moderated
					common.WaitForLeaderboardToIngest(pantheonClient, domain, groupingKey, timeUnit, map[string]int64{}, timeToWaitForLeaderbordsToIngest, getLeaderboardCallDelay)
				}

				Convey("Events should successfully ingest for the moderated entries", func() {
					eventValue := random.Int64(1, 10000)
					for entry := range expectedEntries {
						common.IngestEvent(pantheonClient, domain, groupingKey, entry, "", eventValue)
						expectedEntries[entry] += eventValue
					}

					Convey("All entries should successfully be unmoderated", func() {
						for entry := range expectedEntries {
							common.ModerateEntry(pantheonClient, domain, groupingKey, entry, pantheonrpc.ModerationAction_UNMODERATE)
						}

						for timeUnitInt := range pantheonrpc.TimeUnit_name {
							timeUnit := pantheonrpc.TimeUnit(timeUnitInt)

							// Wait until all entries are unmoderated
							common.WaitForLeaderboardToIngest(pantheonClient, domain, groupingKey, timeUnit, expectedEntries, timeToWaitForLeaderbordsToIngest, getLeaderboardCallDelay)
						}
					})
				})
			})
		})
	})
}
