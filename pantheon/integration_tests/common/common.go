package common

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"os"
	"strings"
	"time"

	"code.justin.tv/commerce/pantheon/config"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	"github.com/gofrs/uuid"
	log "github.com/sirupsen/logrus"
	. "github.com/smartystreets/goconvey/convey"
)

type Entries map[string]map[string]map[string]int64 // domain : grouping-key : entry-key : total

func (e Entries) Add(domain string, groupingKey string, entryKey string, eventValue int64) {
	if _, ok := e[domain]; !ok {
		e[domain] = make(map[string]map[string]int64)
	}

	if _, ok := e[domain][groupingKey]; !ok {
		e[domain][groupingKey] = make(map[string]int64)
	}

	e[domain][groupingKey][entryKey] += eventValue
}

func GetPantheonClient() pantheonrpc.Pantheon {
	environment := os.Getenv("ENVIRONMENT")
	if environment == "" {
		log.Panic("ENVIRONMENT env variable not set")
	}

	if environment == "production" || environment == "prod" {
		log.Panic("Please don't run these integration tests against prod!")
	}

	log.Infof("Loading config file for environment: %s", environment)
	cfg, err := config.LoadConfig(config.Environment(environment))
	if err != nil {
		log.WithError(err).Panicf("Could not load config file for environment: %s", environment)
	}

	if cfg == nil {
		log.WithError(err).Panicf("Could not find a config file for environment: %s", environment)
	}

	return pantheonrpc.NewPantheonProtobufClient(cfg.PantheonEndpoint, http.DefaultClient)
}

func IngestEvent(pantheonClient pantheonrpc.Pantheon, domain string, groupingKey string, entryKey string, eventID string, eventValue int64) string {
	if eventID == "" {
		eventIDUUID, err := uuid.NewV4()
		So(err, ShouldBeNil)
		eventID = eventIDUUID.String()
	}

	_, err := pantheonClient.PublishEvent(context.Background(), &pantheonrpc.PublishEventReq{
		Domain:      domain,
		EventId:     eventID,
		TimeOfEvent: uint64(time.Now().UnixNano()),
		GroupingKey: groupingKey,
		EntryKey:    entryKey,
		EventValue:  eventValue,
	})
	So(err, ShouldBeNil)
	return eventID
}

func ParseLeaderboardIdentifier(identifier string) (string, string, string, error) {
	parts := strings.Split(identifier, "-")
	if len(parts) != 3 {
		return "", "", "", fmt.Errorf("invalid identifier: %s", identifier)
	}
	return parts[0], parts[1], parts[2], nil
}

// Used by convey assertions
func ShouldContainEntry(actual interface{}, expected ...interface{}) string {
	leaderboard, ok := actual.(*pantheonrpc.GetLeaderboardResp)
	if !ok {
		return "actual arg should be GetLeaderboardResp"
	}

	if len(expected) != 2 {
		return "did not receive an expected entry key and entry value"
	}

	expectedEntryKey, ok := expected[0].(string)
	if !ok {
		return "first expected arg should be entry key as string"
	}

	expectedEntryValue, ok := expected[1].(int64)
	if !ok {
		return "second expected arg should be entry value as int64"
	}

	for _, entry := range leaderboard.Top {
		if entry.EntryKey == expectedEntryKey {
			if entry.Score == expectedEntryValue {
				return ""
			}
			return "entry score does not match expected value"
		}
	}
	return "leaderboard did not contain expected entry"
}

func WaitForLeaderboardToIngest(pantheonClient pantheonrpc.Pantheon, domain string, groupingKey string, timeUnit pantheonrpc.TimeUnit, expectedEntries map[string]int64, waitTime time.Duration, delayTime time.Duration) *pantheonrpc.GetLeaderboardResp {
	startTime := time.Now()
	var getResp *pantheonrpc.GetLeaderboardResp
	var err error
	for {
		getResp, err = pantheonClient.GetLeaderboard(context.Background(), &pantheonrpc.GetLeaderboardReq{
			Domain:      domain,
			GroupingKey: groupingKey,
			TimeUnit:    timeUnit,
			TopN:        100,
		})
		So(err, ShouldBeNil)

		if !EntriesEqual(getResp.Top, expectedEntries) {
			fmt.Printf("leaderboards have not fully ingested (%d / %d) waiting...", len(getResp.Top), len(expectedEntries))
			if time.Since(startTime) > waitTime {
				So(errors.New("leaderboards failed to fully ingest"), ShouldBeNil)
				return nil
			}
			time.Sleep(delayTime)
		} else {
			break
		}
	}
	return getResp
}

func EntriesEqual(lbResp []*pantheonrpc.LeaderboardEntry, expectedEntries map[string]int64) bool {
	if len(lbResp) != len(expectedEntries) {
		return false
	}
	for _, lbEntry := range lbResp {
		expectedScore, ok := expectedEntries[lbEntry.EntryKey]
		if !ok {
			return false
		}
		if expectedScore != lbEntry.Score {
			return false
		}
	}
	return true
}

func ModerateEntry(pantheonClient pantheonrpc.Pantheon, domain string, groupingKey string, entryKey string, action pantheonrpc.ModerationAction) {
	_, err := pantheonClient.ModerateEntry(context.Background(), &pantheonrpc.ModerateEntryReq{
		EntryKey:         entryKey,
		Domain:           domain,
		GroupingKey:      groupingKey,
		ModerationAction: action,
	})
	So(err, ShouldBeNil)
}
