// +build integration

package health_check_test

import (
	"context"
	"testing"

	"code.justin.tv/commerce/pantheon/integration_tests/common"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	. "github.com/smartystreets/goconvey/convey"
)

func TestHealthCheck(t *testing.T) {
	Convey("Given a pantheon client", t, func() {
		pantheonClient := common.GetPantheonClient()
		Convey("Test the health check endpoint", func() {
			_, err := pantheonClient.HealthCheck(context.Background(), &pantheonrpc.HealthCheckReq{})
			So(err, ShouldBeNil)
		})
	})
}
