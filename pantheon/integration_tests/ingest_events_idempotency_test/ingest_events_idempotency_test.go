// +build integration

package ingest_events_and_get_leaderboards_test

import (
	"fmt"
	"strings"
	"testing"
	"time"

	"code.justin.tv/commerce/gogogadget/random"
	"code.justin.tv/commerce/pantheon/integration_tests/common"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	"github.com/gofrs/uuid"
	. "github.com/smartystreets/goconvey/convey"
)

const (
	numEvents                    = 10
	timesToIngestDuplicateEvents = 5

	timeToWaitForLeaderbordsToIngest = 120 * time.Second
	getLeaderboardCallDelay          = 2 * time.Second
)

func TestIngestEventsForIdempotency(t *testing.T) {
	Convey("Given a pantheon client", t, func() {
		testRunUUID, err := uuid.NewV4()
		So(err, ShouldBeNil)
		testRunID := strings.Replace(testRunUUID.String(), "-", "", -1)

		pantheonClient := common.GetPantheonClient()

		events := make(map[string]int64)
		domain := fmt.Sprintf("IntegrationTests_TestIngestEventsForIdempotency_RunID%s", testRunID)
		groupingKey := "GroupingKey"
		entryKey := "EntryKey"
		Convey("Events should successfully ingest", func() {
			total := int64(0)
			for i := 1; i < numEvents; i++ {
				eventValue := random.Int64(1, 10000)
				eventID := common.IngestEvent(pantheonClient, domain, groupingKey, entryKey, "", eventValue)
				events[eventID] = eventValue
				total += eventValue
			}

			for timeUnitInt := range pantheonrpc.TimeUnit_name {
				timeUnit := pantheonrpc.TimeUnit(timeUnitInt)
				common.WaitForLeaderboardToIngest(pantheonClient, domain, groupingKey, timeUnit, map[string]int64{entryKey: total}, timeToWaitForLeaderbordsToIngest, getLeaderboardCallDelay)
			}

			Convey("Duplicate events should successfully ingest", func() {
				for i := 0; i < timesToIngestDuplicateEvents; i++ {
					for eventID, eventValue := range events {
						common.IngestEvent(pantheonClient, domain, groupingKey, entryKey, eventID, eventValue)
					}
				}

				Convey("Final totals should be correct (duplicate events shouldn't be double counted)", func() {
					for timeUnitInt := range pantheonrpc.TimeUnit_name {
						timeUnit := pantheonrpc.TimeUnit(timeUnitInt)
						getResp := common.WaitForLeaderboardToIngest(pantheonClient, domain, groupingKey, timeUnit, map[string]int64{entryKey: total}, timeToWaitForLeaderbordsToIngest, getLeaderboardCallDelay)
						So(len(getResp.Top), ShouldEqual, 1)
						So(getResp.Top[0].EntryKey, ShouldEqual, entryKey)
						So(getResp.Top[0].Score, ShouldEqual, total)
					}
				})
			})
		})
	})
}
