// +build integration

package integration_tests

import (
	"math/rand"
	"testing"
	"time"
)

func TestMain(m *testing.M) {
	rand.Seed(time.Now().UnixNano())
}
