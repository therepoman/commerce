// +build integration

package ingest_events_and_get_leaderboards_test

import (
	"context"
	"fmt"
	"math"
	"strings"
	"testing"
	"time"

	"code.justin.tv/commerce/gogogadget/random"
	"code.justin.tv/commerce/pantheon/integration_tests/common"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	"github.com/gofrs/uuid"
	. "github.com/smartystreets/goconvey/convey"
)

const (
	numDomains                 = 2
	numGroupingKeysPerDomain   = 5
	numEntryKeysPerGroupingKey = 5
	numEventsPerEntryKey       = 5

	timeToWaitForLeaderbordsToIngest = 120 * time.Second
	getLeaderboardCallDelay          = 2 * time.Second
)

func TestIngestEventsAndGetLeaderboards(t *testing.T) {
	Convey("Given a pantheon client", t, func() {
		testRunUUID, err := uuid.NewV4()
		So(err, ShouldBeNil)
		testRunID := strings.Replace(testRunUUID.String(), "-", "", -1)

		pantheonClient := common.GetPantheonClient()
		entryTotals := make(common.Entries)

		Convey("Events should successfully ingest", func() {
			for domainIdx := 1; domainIdx <= numDomains; domainIdx++ {
				domain := fmt.Sprintf("IntegrationTests_TestIngestEventsAndGetLeaderboards_RunID%s_Domain%d", testRunID, domainIdx)
				for groupingKeyIdx := 1; groupingKeyIdx <= numGroupingKeysPerDomain; groupingKeyIdx++ {
					groupingKey := fmt.Sprintf("GroupingKey%d", groupingKeyIdx)
					for entryKeyIdx := 1; entryKeyIdx <= numEntryKeysPerGroupingKey; entryKeyIdx++ {
						entryKey := fmt.Sprintf("EntryKey%d", entryKeyIdx)
						for eventIdx := 1; eventIdx <= numEventsPerEntryKey; eventIdx++ {
							eventValue := random.Int64(1, 10000)
							common.IngestEvent(pantheonClient, domain, groupingKey, entryKey, "", eventValue)
							entryTotals.Add(domain, groupingKey, entryKey, eventValue)
						}
					}
				}
			}

			Convey("Each GetLeaderboard call should return the correct leaderboard", func() {
				for domain := range entryTotals {
					for groupingKey := range entryTotals[domain] {

						for timeUnitInt := range pantheonrpc.TimeUnit_name {
							timeUnit := pantheonrpc.TimeUnit(timeUnitInt)

							getResp := common.WaitForLeaderboardToIngest(pantheonClient, domain, groupingKey, timeUnit, entryTotals[domain][groupingKey], timeToWaitForLeaderbordsToIngest, getLeaderboardCallDelay)

							So(err, ShouldBeNil)
							So(getResp, ShouldNotBeNil)
							So(len(getResp.Top), ShouldEqual, numEntryKeysPerGroupingKey)

							for entryKey, entryTotal := range entryTotals[domain][groupingKey] {
								So(getResp, common.ShouldContainEntry, entryKey, entryTotal)
							}
						}
					}
				}

				Convey("GetLeaderboards (bulk call) should return the correct leaderboards", func() {
					for timeUnitInt := range pantheonrpc.TimeUnit_name {
						timeUnit := pantheonrpc.TimeUnit(timeUnitInt)

						leaderboardRequests := make([]*pantheonrpc.GetLeaderboardReq, 0)
						for domain := range entryTotals {
							for groupingKey := range entryTotals[domain] {
								leaderboardRequests = append(leaderboardRequests, &pantheonrpc.GetLeaderboardReq{
									Domain:      domain,
									GroupingKey: groupingKey,
									TimeUnit:    timeUnit,
								})
							}
						}

						resp, err := pantheonClient.GetLeaderboards(context.Background(), &pantheonrpc.GetLeaderboardsReq{
							LeaderboardRequests: leaderboardRequests,
						})
						So(err, ShouldBeNil)

						for _, leaderboard := range resp.Leaderboards {
							domain, groupingKey, _, err := common.ParseLeaderboardIdentifier(leaderboard.Identifier)
							So(err, ShouldBeNil)

							// Validate that the leaderboard is ordered correctly with the correct values
							previousScore := int64(math.MaxInt64)
							for _, entry := range leaderboard.Top {
								entryScore, ok := entryTotals[domain][groupingKey][entry.EntryKey]
								So(ok, ShouldBeTrue)
								So(leaderboard, common.ShouldContainEntry, entry.EntryKey, entryScore)
								So(entryScore, ShouldBeLessThanOrEqualTo, previousScore)
							}
						}
					}
				})
			})
		})
	})
}
