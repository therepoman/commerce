package pantheonrpcserver

import (
	"context"
	"errors"
	"testing"
	"time"

	"code.justin.tv/commerce/pantheon/backend/api"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/domain"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/entry"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/score"
	api_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/backend/api"
	domain_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/backend/leaderboard/domain"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
	"github.com/twitchtv/twirp"
	"github.com/uber-go/tally"
)

func TestGetLeaderboard(t *testing.T) {
	mockGetLeaderboardAPI := new(api_mock.GetLeaderboardAPI)
	mockDomainConfigManager := new(domain_mock.ConfigManager)
	Convey("Given a server", t, func() {
		s := Server{
			GetLeaderboardAPI:   mockGetLeaderboardAPI,
			DomainConfigManager: mockDomainConfigManager,
			RootMetricsScope:    tally.NoopScope,
		}

		Convey("Given a get leaderboard request", func() {
			request := &pantheonrpc.GetLeaderboardReq{
				Domain:      "test-domain",
				GroupingKey: "default",
			}

			Convey("When the request is nil", func() {
				request = nil

				Convey("Should return invalid argument error", func() {
					resp, err := s.GetLeaderboard(context.Background(), request)
					So(resp, ShouldBeNil)
					tErr, isTErr := err.(twirp.Error)
					So(isTErr, ShouldBeTrue)
					So(tErr.Code(), ShouldEqual, twirp.InvalidArgument)
				})
			})

			Convey("When the request is missing a Domain", func() {
				request.Domain = ""

				Convey("Should return invalid argument error", func() {
					resp, err := s.GetLeaderboard(context.Background(), request)
					So(resp, ShouldBeNil)
					tErr, isTErr := err.(twirp.Error)
					So(isTErr, ShouldBeTrue)
					So(tErr.Code(), ShouldEqual, twirp.InvalidArgument)
				})
			})

			Convey("When the request is missing a GroupingKey", func() {
				request.GroupingKey = ""

				Convey("Should return invalid argument error", func() {
					resp, err := s.GetLeaderboard(context.Background(), request)
					So(resp, ShouldBeNil)
					tErr, isTErr := err.(twirp.Error)
					So(isTErr, ShouldBeTrue)
					So(tErr.Code(), ShouldEqual, twirp.InvalidArgument)
				})
			})

			Convey("When the api backend errors", func() {
				mockGetLeaderboardAPI.On("GetLeaderboard", mock.Anything, mock.Anything).Return(api.LeaderboardSummary{}, errors.New("test error")).Once()

				Convey("Should return internal server error", func() {
					resp, err := s.GetLeaderboard(context.Background(), request)
					So(resp, ShouldBeNil)
					tErr, isTErr := err.(twirp.Error)
					So(isTErr, ShouldBeTrue)
					So(tErr.Code(), ShouldEqual, twirp.Internal)
				})
			})

			Convey("When the api backend succeeds", func() {
				startTime := time.Unix(0, 0)
				endTime := time.Unix(0, 1)

				Convey("Under normal circumstances", func() {
					mockGetLeaderboardAPI.On("GetLeaderboard", mock.Anything, mock.Anything).Return(api.LeaderboardSummary{
						Identifier:   "",
						Top:          []entry.RankedEntry{},
						EntryContext: []entry.RankedEntry{},
						Entry:        entry.RankedEntry{},
						StartTime:    &startTime,
						EndTime:      &endTime,
					}, nil).Once()

					Convey("Should return a success", func() {
						resp, err := s.GetLeaderboard(context.Background(), request)
						So(err, ShouldBeNil)
						So(resp, ShouldNotBeNil)
					})
				})

				Convey("With some moderated entries", func() {
					mockGetLeaderboardAPI.On("GetLeaderboard", mock.Anything, mock.Anything).Return(api.LeaderboardSummary{
						Identifier: "",
						Top: []entry.RankedEntry{
							{
								Entry: entry.Entry{
									Key:   "test-key",
									Score: score.ToEntryScore(score.BaseScore(1), score.EventTime(time.Now().UnixNano()), score.Moderated),
								},
							},
							{
								Entry: entry.Entry{
									Key:   "test-key-2",
									Score: score.ToEntryScore(score.BaseScore(1), score.EventTime(time.Now().UnixNano()), score.NotModerated),
								},
							},
						},
						EntryContext: []entry.RankedEntry{
							{
								Entry: entry.Entry{
									Key:   "test-key",
									Score: score.ToEntryScore(score.BaseScore(1), score.EventTime(time.Now().UnixNano()), score.Moderated),
								},
							},
							{
								Entry: entry.Entry{
									Key:   "test-key-2",
									Score: score.ToEntryScore(score.BaseScore(1), score.EventTime(time.Now().UnixNano()), score.NotModerated),
								},
							},
						},
						Entry: entry.RankedEntry{
							Entry: entry.Entry{
								Key:   "test-key",
								Score: score.ToEntryScore(score.BaseScore(1), score.EventTime(time.Now().UnixNano()), score.NotModerated),
							},
						},
						StartTime: &startTime,
						EndTime:   &endTime,
					}, nil).Once()

					Convey("Should return a success and remove only the moderated entries", func() {
						resp, err := s.GetLeaderboard(context.Background(), request)
						So(err, ShouldBeNil)
						So(resp, ShouldNotBeNil)
						So(len(resp.Top), ShouldEqual, 1)
						So(len(resp.EntryContext.Context), ShouldEqual, 1)
						So(resp.EntryContext.Entry, ShouldNotBeNil)
					})
				})

				Convey("With the selected entry moderated", func() {
					mockGetLeaderboardAPI.On("GetLeaderboard", mock.Anything, mock.Anything).Return(api.LeaderboardSummary{
						Identifier: "",
						Top: []entry.RankedEntry{
							{
								Entry: entry.Entry{
									Key:   "test-key",
									Score: score.ToEntryScore(score.BaseScore(1), score.EventTime(time.Now().UnixNano()), score.Moderated),
								},
							},
							{
								Entry: entry.Entry{
									Key:   "test-key-2",
									Score: score.ToEntryScore(score.BaseScore(1), score.EventTime(time.Now().UnixNano()), score.NotModerated),
								},
							},
						},
						EntryContext: []entry.RankedEntry{
							{
								Entry: entry.Entry{
									Key:   "test-key",
									Score: score.ToEntryScore(score.BaseScore(1), score.EventTime(time.Now().UnixNano()), score.Moderated),
								},
							},
							{
								Entry: entry.Entry{
									Key:   "test-key-2",
									Score: score.ToEntryScore(score.BaseScore(1), score.EventTime(time.Now().UnixNano()), score.NotModerated),
								},
							},
						},
						Entry: entry.RankedEntry{
							Entry: entry.Entry{
								Key:   "test-key",
								Score: score.ToEntryScore(score.BaseScore(1), score.EventTime(time.Now().UnixNano()), score.Moderated),
							},
						},
						StartTime: &startTime,
						EndTime:   &endTime,
					}, nil).Once()

					Convey("Should return a success and remove the moderated entries and return no context entries", func() {
						resp, err := s.GetLeaderboard(context.Background(), request)
						So(err, ShouldBeNil)
						So(resp, ShouldNotBeNil)
						So(len(resp.Top), ShouldEqual, 1)
						So(len(resp.EntryContext.Context), ShouldEqual, 0)
						So(resp.EntryContext.Entry, ShouldNotBeNil)
						So(resp.EntryContext.Entry.Moderated, ShouldBeTrue)
					})
				})
			})
		})
	})
}

func TestGetLeaderboards(t *testing.T) {
	mockGetLeaderboardAPI := new(api_mock.GetLeaderboardAPI)
	mockDomainConfigManager := new(domain_mock.ConfigManager)
	startTime := time.Unix(0, 0)
	endTime := time.Unix(0, 1)
	Convey("Given a server", t, func() {
		s := Server{
			GetLeaderboardAPI:   mockGetLeaderboardAPI,
			DomainConfigManager: mockDomainConfigManager,
			RootMetricsScope:    tally.NoopScope,
		}

		Convey("Given a get leaderboards request", func() {
			request := &pantheonrpc.GetLeaderboardsReq{
				LeaderboardRequests: []*pantheonrpc.GetLeaderboardReq{
					{
						Domain:      "test-domain1",
						GroupingKey: "test-key1",
					},
					{
						Domain:      "test-domain2",
						GroupingKey: "test-key2",
					},
				},
			}

			Convey("When the request is nil", func() {
				request = nil

				Convey("Should return invalid argument error", func() {
					resp, err := s.GetLeaderboards(context.Background(), request)
					So(resp, ShouldBeNil)
					tErr, isTErr := err.(twirp.Error)
					So(isTErr, ShouldBeTrue)
					So(tErr.Code(), ShouldEqual, twirp.InvalidArgument)
				})
			})

			Convey("When the leaderboard requests list is empty", func() {
				request = &pantheonrpc.GetLeaderboardsReq{
					LeaderboardRequests: []*pantheonrpc.GetLeaderboardReq{},
				}

				Convey("Should return invalid argument error", func() {
					resp, err := s.GetLeaderboards(context.Background(), request)
					So(resp, ShouldBeNil)
					tErr, isTErr := err.(twirp.Error)
					So(isTErr, ShouldBeTrue)
					So(tErr.Code(), ShouldEqual, twirp.InvalidArgument)
				})
			})

			Convey("When domain config manager returns an unknown domain", func() {
				mockDomainConfigManager.On("Get", mock.Anything).Return(domain.UnknownDomainConfig())

				Convey("When the number of leaderboard requests exceeds the max limit", func() {
					request = &pantheonrpc.GetLeaderboardsReq{
						LeaderboardRequests: []*pantheonrpc.GetLeaderboardReq{},
					}

					for i := 0; i < domain.DefaultMaxLeaderboards+1; i++ {
						request.LeaderboardRequests = append(request.LeaderboardRequests, &pantheonrpc.GetLeaderboardReq{
							Domain:      "test-domain1",
							GroupingKey: "test-key1",
						})
					}

					Convey("Should return invalid argument error", func() {
						resp, err := s.GetLeaderboards(context.Background(), request)
						So(resp, ShouldBeNil)
						tErr, isTErr := err.(twirp.Error)
						So(isTErr, ShouldBeTrue)
						So(tErr.Code(), ShouldEqual, twirp.InvalidArgument)
					})
				})

				Convey("When one of the leaderboard requests is invalid", func() {
					request = &pantheonrpc.GetLeaderboardsReq{
						LeaderboardRequests: []*pantheonrpc.GetLeaderboardReq{
							{
								Domain:      "test-domain1",
								GroupingKey: "test-key1",
							},
							{
								GroupingKey: "test-key2",
							},
						},
					}

					Convey("Should return invalid argument error", func() {
						mockGetLeaderboardAPI.On("GetLeaderboard", mock.Anything, mock.Anything).Return(api.LeaderboardSummary{
							Identifier:   "",
							Top:          []entry.RankedEntry{},
							EntryContext: []entry.RankedEntry{},
							Entry:        entry.RankedEntry{},
							StartTime:    &startTime,
							EndTime:      &endTime,
						}, nil).Once()

						resp, err := s.GetLeaderboards(context.Background(), request)
						So(resp, ShouldBeNil)
						tErr, isTErr := err.(twirp.Error)
						So(isTErr, ShouldBeTrue)
						So(tErr.Code(), ShouldEqual, twirp.InvalidArgument)
					})
				})

				Convey("When the api backend errors", func() {
					mockGetLeaderboardAPI.On("GetLeaderboard", mock.Anything, mock.Anything).Return(api.LeaderboardSummary{}, errors.New("test error")).Once()

					Convey("Should return internal server error", func() {
						resp, err := s.GetLeaderboards(context.Background(), request)
						So(resp, ShouldBeNil)
						tErr, isTErr := err.(twirp.Error)
						So(isTErr, ShouldBeTrue)
						So(tErr.Code(), ShouldEqual, twirp.Internal)
					})
				})

				Convey("When the request is valid and all contained requests are valid", func() {
					Convey("Should return one leaderboard per leaderboard request", func() {
						mockGetLeaderboardAPI.On("GetLeaderboard", mock.Anything, mock.Anything).Return(api.LeaderboardSummary{
							Identifier:   "",
							Top:          []entry.RankedEntry{},
							EntryContext: []entry.RankedEntry{},
							Entry:        entry.RankedEntry{},
							StartTime:    &startTime,
							EndTime:      &endTime,
						}, nil)

						resp, err := s.GetLeaderboards(context.Background(), request)
						So(resp, ShouldNotBeNil)
						So(err, ShouldBeNil)
						So(len(resp.Leaderboards), ShouldEqual, 2)
					})
				})
			})
		})
	})
}
