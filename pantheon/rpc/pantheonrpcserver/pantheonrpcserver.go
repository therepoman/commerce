package pantheonrpcserver

import (
	"code.justin.tv/commerce/pantheon/backend/api"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/domain"
	"github.com/uber-go/tally"
)

type Server struct {
	GetLeaderboardAPI                      api.GetLeaderboardAPI                      `inject:"get_leaderboard_api"`
	GetBucketedLeaderboardCountsAPI        api.GetBucketedLeaderboardCountsAPI        `inject:"get_bucketed_leaderboard_counts_api"`
	GetLeaderboardEntriesAboveThresholdAPI api.GetLeaderboardEntriesAboveThresholdAPI `inject:"get_leaderboard_entries_above_threshold_api"`
	PublishEventAPI                        api.PublishEventAPI                        `inject:"publish_event_api"`
	ModerateEntryAPI                       api.ModerateEntryAPI                       `inject:"moderate_entry_api"`
	DeleteLeaderboardAPI                   api.DeleteLeaderboardsAPI                  `inject:"delete_leaderboard_api"`
	DeleteEntriesAPI                       api.DeleteEntriesAPI                       `inject:"delete_entries_api"`
	DomainConfigManager                    domain.ConfigManager                       `inject:""`
	RootMetricsScope                       tally.Scope                                `inject:""`
}
