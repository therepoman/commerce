package pantheonrpcserver

import (
	"context"

	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	log "github.com/sirupsen/logrus"
	"github.com/twitchtv/twirp"
)

func (s *Server) DeleteLeaderboards(ctx context.Context, req *pantheonrpc.DeleteLeaderboardsReq) (*pantheonrpc.DeleteLeaderboardsResp, error) {
	if req == nil {
		return nil, twirp.InternalError("Received nil request")
	}

	if req.Domain == "" {
		return nil, twirp.InvalidArgumentError("domain", "should not be blank")
	}

	if req.GroupingKey == "" {
		return nil, twirp.InvalidArgumentError("grouping_key", "should not be blank")
	}

	resp, err := s.DeleteLeaderboardAPI.DeleteLeaderboards(ctx, req)
	if err != nil {
		log.WithFields(log.Fields{
			"domain":      req.Domain,
			"groupingKey": req.GroupingKey,
		}).WithError(err).Error("Error deleting leaderboard")
		return nil, twirp.InternalError("Downstream dependency error")
	}
	return resp, nil
}
