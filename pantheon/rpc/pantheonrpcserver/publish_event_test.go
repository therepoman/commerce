package pantheonrpcserver

import (
	"context"
	"errors"
	"testing"
	"time"

	api_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/backend/api"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
	"github.com/twitchtv/twirp"
)

func TestPublishEvent(t *testing.T) {
	Convey("Given a server", t, func() {
		mockAPI := new(api_mock.PublishEventAPI)
		s := Server{
			PublishEventAPI: mockAPI,
		}

		Convey("Given a publish event request", func() {
			var request *pantheonrpc.PublishEventReq

			Convey("When the request is nil", func() {
				request = nil

				Convey("Should return internal server error", func() {
					resp, err := s.PublishEvent(context.Background(), request)
					So(resp, ShouldBeNil)
					tErr, isTErr := err.(twirp.Error)
					So(isTErr, ShouldBeTrue)
					So(tErr.Code(), ShouldEqual, twirp.Internal)
				})
			})

			Convey("Given a valid request", func() {
				request = &pantheonrpc.PublishEventReq{
					Domain:      "valid-domain",
					EventId:     "valid-event-id",
					TimeOfEvent: uint64(time.Now().UnixNano()),
					GroupingKey: "valid-grouping-key",
					EntryKey:    "valid-entry-key",
					EventValue:  1,
				}

				Convey("Should return invalid argument error when domain is blank", func() {
					request.Domain = ""
					resp, err := s.PublishEvent(context.Background(), request)
					So(resp, ShouldBeNil)
					tErr, isTErr := err.(twirp.Error)
					So(isTErr, ShouldBeTrue)
					So(tErr.Code(), ShouldEqual, twirp.InvalidArgument)
				})

				Convey("Should return invalid argument error when eventID is blank", func() {
					request.EventId = ""
					resp, err := s.PublishEvent(context.Background(), request)
					So(resp, ShouldBeNil)
					tErr, isTErr := err.(twirp.Error)
					So(isTErr, ShouldBeTrue)
					So(tErr.Code(), ShouldEqual, twirp.InvalidArgument)
				})

				Convey("Should return invalid argument error when timeOfEvent is zero", func() {
					request.TimeOfEvent = uint64(0)
					resp, err := s.PublishEvent(context.Background(), request)
					So(resp, ShouldBeNil)
					tErr, isTErr := err.(twirp.Error)
					So(isTErr, ShouldBeTrue)
					So(tErr.Code(), ShouldEqual, twirp.InvalidArgument)
				})

				Convey("Should return invalid argument error when timeOfEvent is some day in the 80s", func() {
					request.TimeOfEvent = uint64(479522561000000000)
					resp, err := s.PublishEvent(context.Background(), request)
					So(resp, ShouldBeNil)
					tErr, isTErr := err.(twirp.Error)
					So(isTErr, ShouldBeTrue)
					So(tErr.Code(), ShouldEqual, twirp.InvalidArgument)
				})

				Convey("Should return invalid argument error when timeOfEvent is from the end of 2005", func() {
					request.TimeOfEvent = uint64(1135989761000000000)
					resp, err := s.PublishEvent(context.Background(), request)
					So(resp, ShouldBeNil)
					tErr, isTErr := err.(twirp.Error)
					So(isTErr, ShouldBeTrue)
					So(tErr.Code(), ShouldEqual, twirp.InvalidArgument)
				})

				Convey("Should return invalid argument error when grouping key is blank", func() {
					request.GroupingKey = ""
					resp, err := s.PublishEvent(context.Background(), request)
					So(resp, ShouldBeNil)
					tErr, isTErr := err.(twirp.Error)
					So(isTErr, ShouldBeTrue)
					So(tErr.Code(), ShouldEqual, twirp.InvalidArgument)
				})

				Convey("Should return invalid argument error when entry key is blank", func() {
					request.EntryKey = ""
					resp, err := s.PublishEvent(context.Background(), request)
					So(resp, ShouldBeNil)
					tErr, isTErr := err.(twirp.Error)
					So(isTErr, ShouldBeTrue)
					So(tErr.Code(), ShouldEqual, twirp.InvalidArgument)
				})

				Convey("Should return invalid argument error when event value is negative", func() {
					request.EventValue = -1
					resp, err := s.PublishEvent(context.Background(), request)
					So(resp, ShouldBeNil)
					tErr, isTErr := err.(twirp.Error)
					So(isTErr, ShouldBeTrue)
					So(tErr.Code(), ShouldEqual, twirp.InvalidArgument)
				})

				Convey("Should return invalid argument error when event value is zero", func() {
					request.EventValue = 0
					resp, err := s.PublishEvent(context.Background(), request)
					So(resp, ShouldBeNil)
					tErr, isTErr := err.(twirp.Error)
					So(isTErr, ShouldBeTrue)
					So(tErr.Code(), ShouldEqual, twirp.InvalidArgument)
				})

				Convey("Should return internal server error when inner API returns an error", func() {
					mockAPI.On("PublishEvent", mock.Anything, mock.Anything).Return(nil, errors.New("test error"))
					resp, err := s.PublishEvent(context.Background(), request)
					So(resp, ShouldBeNil)
					tErr, isTErr := err.(twirp.Error)
					So(isTErr, ShouldBeTrue)
					So(tErr.Code(), ShouldEqual, twirp.Internal)
				})

				Convey("Should succeed when inner API succeeds", func() {
					mockAPI.On("PublishEvent", mock.Anything, mock.Anything).Return(&pantheonrpc.PublishEventResp{}, nil)
					resp, err := s.PublishEvent(context.Background(), request)
					So(resp, ShouldNotBeNil)
					So(err, ShouldBeNil)
				})
			})
		})
	})
}
