package pantheonrpcserver

import (
	"context"
	"testing"

	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/twitchtv/twirp"
)

func TestModerateEntry(t *testing.T) {
	Convey("Given a server", t, func() {
		s := Server{}

		Convey("Given a moderate entry request", func() {
			var request *pantheonrpc.ModerateEntryReq

			Convey("When the request is nil", func() {
				request = nil

				Convey("Should return internal server error", func() {
					resp, err := s.ModerateEntry(context.Background(), request)
					So(resp, ShouldBeNil)
					tErr, isTErr := err.(twirp.Error)
					So(isTErr, ShouldBeTrue)
					So(tErr.Code(), ShouldEqual, twirp.Internal)
				})
			})
		})
	})
}
