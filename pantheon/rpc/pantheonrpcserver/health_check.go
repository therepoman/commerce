package pantheonrpcserver

import (
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	"golang.org/x/net/context"
)

func (s *Server) HealthCheck(ctx context.Context, req *pantheonrpc.HealthCheckReq) (*pantheonrpc.HealthCheckResp, error) {
	return &pantheonrpc.HealthCheckResp{}, nil
}
