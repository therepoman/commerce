package pantheonrpcserver

import (
	"context"
	"time"

	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	log "github.com/sirupsen/logrus"
	"github.com/twitchtv/twirp"
)

const (
	// Timeout based on API latency from CloudWatch
	GetBucketedLeaderboardContextTimeout = 200 * time.Millisecond
)

func (s *Server) GetBucketedLeaderboardCounts(ctx context.Context, req *pantheonrpc.GetBucketedLeaderboardCountsReq) (resp *pantheonrpc.GetBucketedLeaderboardCountsResp, error error) {
	if req == nil {
		return nil, twirp.InvalidArgumentError("Req", "There must be a request body")
	} else if strings.Blank(req.Domain) {
		return nil, twirp.InvalidArgumentError("Domain", "Domain should not be blank")
	} else if strings.Blank(req.GroupingKey) {
		return nil, twirp.InvalidArgumentError("GroupingKey", "GroupingKey should not be blank")
	}
	getBucketedLeaderboardContext, cancel := context.WithTimeout(ctx, GetBucketedLeaderboardContextTimeout)
	defer cancel()
	buckets, err := s.GetBucketedLeaderboardCountsAPI.GetBucketedLeaderboardCounts(getBucketedLeaderboardContext, req)

	if err != nil {
		log.Error("Error getting bucketed leaderboard counts", err)
		return nil, twirp.InternalError("Downstream dependency error getting bucketed leaderboard counts")
	}

	if buckets == nil {
		return &pantheonrpc.GetBucketedLeaderboardCountsResp{}, nil
	}

	return &pantheonrpc.GetBucketedLeaderboardCountsResp{
		Buckets: *buckets,
	}, nil
}
