package pantheonrpcserver

import (
	"context"
	"errors"
	"testing"

	api_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/backend/api"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestGetBucketedLeaderboardCounts(t *testing.T) {
	mockGetBucketedLeaderboardCountsAPI := new(api_mock.GetBucketedLeaderboardCountsAPI)

	mockBucketThresholds := []int64{1}

	s := Server{
		GetBucketedLeaderboardCountsAPI: mockGetBucketedLeaderboardCountsAPI,
	}

	Convey("given a bucketed leaderboard counts getter", t, func() {
		Convey("when the request is nil", func() {
			Convey("we should return an error", func() {
				resp, err := s.GetBucketedLeaderboardCounts(context.Background(), nil)
				So(resp, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when the request is missing a Domain", func() {
			Convey("we should return an error", func() {
				resp, err := s.GetBucketedLeaderboardCounts(context.Background(), &pantheonrpc.GetBucketedLeaderboardCountsReq{
					Domain:           "",
					GroupingKey:      "some-grouping-key",
					BucketThresholds: mockBucketThresholds,
				})

				So(resp, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when the request is missing a GroupingKey", func() {
			Convey("we should return an error", func() {
				resp, err := s.GetBucketedLeaderboardCounts(context.Background(), &pantheonrpc.GetBucketedLeaderboardCountsReq{
					Domain:           "some-domain",
					GroupingKey:      "",
					BucketThresholds: mockBucketThresholds,
				})

				So(resp, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when the request has all necessary params", func() {
			req := &pantheonrpc.GetBucketedLeaderboardCountsReq{
				Domain:           "some-domain",
				GroupingKey:      "some-grouping-key",
				BucketThresholds: mockBucketThresholds,
			}

			Convey("when GetBucketedLeaderboardCounts returns an error", func() {
				mockGetBucketedLeaderboardCountsAPI.On("GetBucketedLeaderboardCounts", mock.Anything, mock.Anything).Return(nil, errors.New("ERROR")).Once()

				Convey("we should return an error", func() {
					resp, err := s.GetBucketedLeaderboardCounts(context.Background(), req)
					So(resp, ShouldBeNil)
					So(err, ShouldNotBeNil)
				})
			})

			Convey("when GetBucketedLeaderboardCounts does not return an error", func() {
				Convey("when GetBucketedLeaderboardCounts returns nil buckets", func() {
					Convey("we should return an empty response", func() {
						mockGetBucketedLeaderboardCountsAPI.On("GetBucketedLeaderboardCounts", mock.Anything, mock.Anything).Return(nil, nil).Once()

						resp, err := s.GetBucketedLeaderboardCounts(context.Background(), req)
						So(resp, ShouldResemble, &pantheonrpc.GetBucketedLeaderboardCountsResp{})
						So(err, ShouldBeNil)
					})
				})

				Convey("when GetBucketedLeaderboardCounts does not return nil buckets", func() {
					mockBuckets := []*pantheonrpc.Bucket{
						{
							Count:     1,
							Threshold: 1,
						},
					}

					mockGetBucketedLeaderboardCountsAPI.On("GetBucketedLeaderboardCounts", mock.Anything, mock.Anything).Return(&mockBuckets, nil).Once()

					Convey("we should return the buckets", func() {
						resp, err := s.GetBucketedLeaderboardCounts(context.Background(), req)
						So(resp, ShouldResemble, &pantheonrpc.GetBucketedLeaderboardCountsResp{
							Buckets: mockBuckets,
						})
						So(err, ShouldBeNil)
					})
				})
			})
		})
	})
}
