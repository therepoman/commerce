package pantheonrpcserver

import (
	"fmt"
	"time"

	"code.justin.tv/commerce/pantheon/backend/api"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/identifier"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	log "github.com/sirupsen/logrus"
	"github.com/twitchtv/twirp"
	"golang.org/x/net/context"
)

const (
	// Timeout based on API latency from CloudWatch and to account for two Redis calls of 50ms each, both with a potential single retry
	GetLeaderboardContextTimeout = 220 * time.Millisecond
)

func (s *Server) validateGetLeaderboardReq(req *pantheonrpc.GetLeaderboardReq) error {
	if req == nil {
		return twirp.InvalidArgumentError("Domain", "There must be a request body")
	} else if req.Domain == "" {
		return twirp.InvalidArgumentError("Domain", "Domain should not be blank")
	} else if req.GroupingKey == "" {
		return twirp.InvalidArgumentError("GroupingKey", "GroupingKey should not be blank")
	}

	return nil
}

func (s *Server) getLeaderboard(ctx context.Context, req *pantheonrpc.GetLeaderboardReq) (*pantheonrpc.GetLeaderboardResp, error) {
	getLeaderboardContext, cancel := context.WithTimeout(ctx, GetLeaderboardContextTimeout)
	defer cancel()
	leaderboard, err := s.GetLeaderboardAPI.GetLeaderboard(getLeaderboardContext, req)
	if err != nil {
		log.WithError(err).Error("Error getting leaderboard")
		return nil, twirp.InternalError("Downstream dependency error getting leaderboard")
	}

	top := api.ConvertModelToAPIEntries(leaderboard.Top)
	e := api.ConvertModelToAPIEntry(leaderboard.Entry)
	var entryContext []*pantheonrpc.LeaderboardEntry
	if e != nil && !e.Moderated {
		entryContext = api.ConvertModelToAPIEntries(leaderboard.EntryContext)
	}
	return &pantheonrpc.GetLeaderboardResp{
		Top: top,
		EntryContext: &pantheonrpc.EntryContext{
			Context: entryContext,
			Entry:   e,
		},
		Identifier: leaderboard.Identifier,
		StartTime:  getNanoseconds(leaderboard.StartTime),
		EndTime:    getNanoseconds(leaderboard.EndTime),
	}, nil
}

func (s *Server) GetLeaderboard(ctx context.Context, req *pantheonrpc.GetLeaderboardReq) (*pantheonrpc.GetLeaderboardResp, error) {
	err := s.validateGetLeaderboardReq(req)
	if err != nil {
		return nil, err
	}

	s.RootMetricsScope.
		Tagged(getMetricTags(identifier.Domain(req.Domain))).
		Counter("GetLeaderboard_RequestsByDomain").
		Inc(1)

	return s.getLeaderboard(ctx, req)
}

func (s *Server) GetLeaderboards(ctx context.Context, req *pantheonrpc.GetLeaderboardsReq) (*pantheonrpc.GetLeaderboardsResp, error) {
	if req == nil {
		return nil, twirp.InvalidArgumentError("LeaderboardRequests", "There must be a request body")
	} else if len(req.LeaderboardRequests) == 0 {
		return nil, twirp.InvalidArgumentError("LeaderboardRequests", "There must be at least one request object")
	}

	domainConfig := s.DomainConfigManager.Get(identifier.Domain(req.LeaderboardRequests[0].Domain))
	if len(req.LeaderboardRequests) > domainConfig.MaxLeaderboards {
		return nil, twirp.InvalidArgumentError("LeaderboardRequests", fmt.Sprintf("Number of leaderboard requests should not exceed %d", domainConfig.MaxLeaderboards))
	}

	s.RootMetricsScope.Gauge("GetLeaderboards_LeaderboardsRequested").Update(float64(len(req.LeaderboardRequests)))

	var leaderboards []*pantheonrpc.GetLeaderboardResp
	for _, leaderboardReq := range req.LeaderboardRequests {
		err := s.validateGetLeaderboardReq(leaderboardReq)
		if err != nil {
			return nil, err
		}

		s.RootMetricsScope.
			Tagged(getMetricTags(identifier.Domain(leaderboardReq.Domain))).
			Counter("GetLeaderboards_NestedRequestsByDomain").
			Inc(1)

		leaderboardResp, err := s.getLeaderboard(ctx, leaderboardReq)
		if err != nil {
			return nil, err
		}
		leaderboards = append(leaderboards, leaderboardResp)
	}

	return &pantheonrpc.GetLeaderboardsResp{
		Leaderboards: leaderboards,
	}, nil
}

func getMetricTags(d identifier.Domain) map[string]string {
	metricTags := map[string]string{
		"domain": string(d),
	}
	return metricTags
}

func getNanoseconds(time *time.Time) uint64 {
	if time == nil {
		return 0
	}

	return uint64(time.UnixNano())
}
