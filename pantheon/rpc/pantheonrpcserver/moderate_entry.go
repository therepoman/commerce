package pantheonrpcserver

import (
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	log "github.com/sirupsen/logrus"
	"github.com/twitchtv/twirp"
	"golang.org/x/net/context"
)

func (s *Server) ModerateEntry(ctx context.Context, req *pantheonrpc.ModerateEntryReq) (*pantheonrpc.ModerateEntryResp, error) {
	if req == nil {
		return nil, twirp.InternalError("Received nil request")
	}

	if req.Domain == "" {
		return nil, twirp.InvalidArgumentError("Domain", "Input should not be blank")
	}

	if req.GroupingKey == "" {
		return nil, twirp.InvalidArgumentError("GroupingKey", "GroupingKey should not be blank")
	}

	if req.EntryKey == "" {
		return nil, twirp.InvalidArgumentError("EntryKey", "EntryKey should not be blank")
	}

	resp, err := s.ModerateEntryAPI.ModerateEntry(ctx, req)
	if err != nil {
		log.WithError(err).Error("Error moderating entry")
		return nil, twirp.InternalError("Downstream dependency error")
	}
	return resp, nil
}
