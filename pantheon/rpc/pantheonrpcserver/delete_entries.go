package pantheonrpcserver

import (
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	log "github.com/sirupsen/logrus"
	"github.com/twitchtv/twirp"
	"golang.org/x/net/context"
)

func (s *Server) DeleteEntries(ctx context.Context, req *pantheonrpc.DeleteEntriesReq) (*pantheonrpc.DeleteEntriesResp, error) {
	if req == nil {
		return nil, twirp.InternalError("Received nil request")
	}

	if req.Domain == "" {
		return nil, twirp.InvalidArgumentError("domain", "should not be blank")
	}

	if req.EntryKey == "" {
		return nil, twirp.InvalidArgumentError("entry_key", "should not be blank")
	}

	resp, err := s.DeleteEntriesAPI.DeleteEntries(ctx, req)
	if err != nil {
		log.WithFields(log.Fields{
			"domain":   req.Domain,
			"entryKey": req.EntryKey,
		}).WithError(err).Error("Error deleting entries")
		return nil, twirp.InternalError("Downstream dependency error")
	}
	return resp, nil
}
