package pantheonrpcserver

import (
	"context"

	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/pantheon/backend/api"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	log "github.com/sirupsen/logrus"
	"github.com/twitchtv/twirp"
)

const (
	maxGetLeaderboardEntriesAboveThresholdLimit = 500
)

func (s *Server) GetLeaderboardEntriesAboveThreshold(ctx context.Context, req *pantheonrpc.GetLeaderboardEntriesAboveThresholdReq) (resp *pantheonrpc.GetLeaderboardEntriesAboveThresholdResp, error error) {
	if req == nil {
		return nil, twirp.InvalidArgumentError("Req", "There must be a request body")
	} else if strings.Blank(req.Domain) {
		return nil, twirp.InvalidArgumentError("Domain", "Domain should not be blank")
	} else if strings.Blank(req.GroupingKey) {
		return nil, twirp.InvalidArgumentError("GroupingKey", "GroupingKey should not be blank")
	} else if req.Limit == 0 || req.Limit > maxGetLeaderboardEntriesAboveThresholdLimit {
		return nil, twirp.InvalidArgumentError("Limit", "Limit should be > 0 and <= 500")
	}

	entries, err := s.GetLeaderboardEntriesAboveThresholdAPI.GetLeaderboardEntriesAboveThreshold(ctx, req)

	if err != nil {
		log.WithError(err).Error("Error getting leaderboard entries above threshold")
		return nil, twirp.InternalError("Downstream dependency error getting leaderboard entries above threshold")
	}

	return &pantheonrpc.GetLeaderboardEntriesAboveThresholdResp{
		Entries: api.ConvertModelToAPIEntries(entries),
	}, nil
}
