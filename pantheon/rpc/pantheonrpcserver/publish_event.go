package pantheonrpcserver

import (
	"code.justin.tv/commerce/pantheon/backend/leaderboard/score"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	log "github.com/sirupsen/logrus"
	"github.com/twitchtv/twirp"
	"golang.org/x/net/context"
)

func (s *Server) PublishEvent(ctx context.Context, req *pantheonrpc.PublishEventReq) (*pantheonrpc.PublishEventResp, error) {
	if req == nil {
		return nil, twirp.InternalError("Received nil request")
	}

	if req.Domain == "" {
		return nil, twirp.InvalidArgumentError("Domain", "Input should not be blank")
	}

	if req.EventId == "" {
		return nil, twirp.InvalidArgumentError("EventId", "EventId should not be blank")
	}

	if int64(req.TimeOfEvent) < score.MinimumEventTime {
		return nil, twirp.InvalidArgumentError("TimeOfEvent", "TimeOfEvent must be a valid timestamp in nanoseconds")
	}

	if req.GroupingKey == "" {
		return nil, twirp.InvalidArgumentError("GroupingKey", "GroupingKey should not be blank")
	}

	if req.EntryKey == "" {
		return nil, twirp.InvalidArgumentError("EntryKey", "EntryKey should not be blank")
	}

	if req.EventValue <= 0 {
		return nil, twirp.InvalidArgumentError("EventValue", "EventValue should be a positive integer")
	}

	resp, err := s.PublishEventAPI.PublishEvent(ctx, req)
	if err != nil {
		log.Error("Error publishing event", err)
		return nil, twirp.InternalError("Downstream dependency error")
	}
	return resp, nil
}
