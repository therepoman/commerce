package pantheonrpcserver

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/pantheon/backend/leaderboard/entry"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/score"
	api_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/backend/api"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestGetLeaderboardEntriesAboveThreshold(t *testing.T) {
	mockGetLeaderboardEntriesAboveThresholdAPI := new(api_mock.GetLeaderboardEntriesAboveThresholdAPI)

	s := Server{
		GetLeaderboardEntriesAboveThresholdAPI: mockGetLeaderboardEntriesAboveThresholdAPI,
	}

	Convey("given a GetLeaderboardEntriesAboveThreshold API", t, func() {
		Convey("when the request is nil", func() {
			Convey("we should return an error", func() {
				resp, err := s.GetLeaderboardEntriesAboveThreshold(context.Background(), nil)
				So(resp, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when the request is missing a Domain", func() {
			Convey("we should return an error", func() {
				resp, err := s.GetLeaderboardEntriesAboveThreshold(context.Background(), &pantheonrpc.GetLeaderboardEntriesAboveThresholdReq{
					Domain:      "",
					GroupingKey: "some-grouping-key",
					Limit:       1,
				})

				So(resp, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when the request is missing a GroupingKey", func() {
			Convey("we should return an error", func() {
				resp, err := s.GetLeaderboardEntriesAboveThreshold(context.Background(), &pantheonrpc.GetLeaderboardEntriesAboveThresholdReq{
					Domain:      "some-domain",
					GroupingKey: "",
					Limit:       1,
				})

				So(resp, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when the request is missing a Limit", func() {
			Convey("we should return an error", func() {
				resp, err := s.GetLeaderboardEntriesAboveThreshold(context.Background(), &pantheonrpc.GetLeaderboardEntriesAboveThresholdReq{
					Domain:      "some-domain",
					GroupingKey: "some-grouping-key",
					Limit:       0,
				})

				So(resp, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when the request is missing a Limit exceeds the upper bound", func() {
			Convey("we should return an error", func() {
				resp, err := s.GetLeaderboardEntriesAboveThreshold(context.Background(), &pantheonrpc.GetLeaderboardEntriesAboveThresholdReq{
					Domain:      "some-domain",
					GroupingKey: "some-grouping-key",
					Limit:       501,
				})

				So(resp, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when the request has all necessary params", func() {
			req := &pantheonrpc.GetLeaderboardEntriesAboveThresholdReq{
				Domain:      "some-domain",
				GroupingKey: "some-grouping-key",
				Limit:       1,
			}

			Convey("when GetLeaderboardEntriesAboveThreshold returns an error", func() {
				mockGetLeaderboardEntriesAboveThresholdAPI.On("GetLeaderboardEntriesAboveThreshold", mock.Anything, mock.Anything).Return(nil, errors.New("ERROR")).Once()

				Convey("we should return an error", func() {
					resp, err := s.GetLeaderboardEntriesAboveThreshold(context.Background(), req)
					So(resp, ShouldBeNil)
					So(err, ShouldNotBeNil)
				})
			})

			Convey("when GetLeaderboardEntriesAboveThreshold does not return an error", func() {
				mockEntry := entry.RankedEntry{
					Entry: entry.Entry{
						Key: "hello",
						Score: score.EntryScore{
							ModeratedFlag: false,
							BaseScore:     10,
							EventTime:     0,
						},
					},
					Rank: entry.Rank(1),
				}
				mockEntries := []entry.RankedEntry{
					mockEntry,
				}
				mockGetLeaderboardEntriesAboveThresholdAPI.On("GetLeaderboardEntriesAboveThreshold", mock.Anything, mock.Anything).Return(mockEntries, nil).Once()

				Convey("we should return the entries", func() {
					resp, err := s.GetLeaderboardEntriesAboveThreshold(context.Background(), req)
					So(resp, ShouldResemble, &pantheonrpc.GetLeaderboardEntriesAboveThresholdResp{
						Entries: []*pantheonrpc.LeaderboardEntry{
							{
								Rank:      1,
								Score:     10,
								EntryKey:  "hello",
								Moderated: false,
							},
						},
					})
					So(err, ShouldBeNil)
				})
			})
		})
	})
}
