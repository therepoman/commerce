package pantheonrpcserver

import (
	"context"
	"errors"
	"testing"

	api_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/backend/api"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
	"github.com/twitchtv/twirp"
)

func TestDeleteEntries(t *testing.T) {
	Convey("Given a server", t, func() {
		mockDeleteEntriesAPI := new(api_mock.DeleteEntriesAPI)

		s := Server{
			DeleteEntriesAPI: mockDeleteEntriesAPI,
		}

		Convey("When the request is nil", func() {
			var request *pantheonrpc.DeleteEntriesReq

			Convey("Should return internal server error", func() {
				_, err := s.DeleteEntries(context.Background(), request)

				twirpErr, isOk := err.(twirp.Error)
				So(isOk, ShouldBeTrue)
				So(twirpErr.Code(), ShouldEqual, twirp.Internal)
			})
		})

		Convey("When the request has no domain", func() {
			request := &pantheonrpc.DeleteEntriesReq{
				Domain: "",
			}

			Convey("Should return invalid argument error", func() {
				_, err := s.DeleteEntries(context.Background(), request)

				twirpErr, isOk := err.(twirp.Error)
				So(isOk, ShouldBeTrue)
				So(twirpErr.Code(), ShouldEqual, twirp.InvalidArgument)
			})
		})

		domain := "test-domain"

		Convey("When the request has no entryKey", func() {
			request := &pantheonrpc.DeleteEntriesReq{
				Domain:   domain,
				EntryKey: "",
			}

			Convey("Should return invalid argument error", func() {
				_, err := s.DeleteEntries(context.Background(), request)

				twirpErr, isOk := err.(twirp.Error)
				So(isOk, ShouldBeTrue)
				So(twirpErr.Code(), ShouldEqual, twirp.InvalidArgument)
			})
		})

		entryKey := "test-entry-key"

		Convey("Given a proper request", func() {
			request := &pantheonrpc.DeleteEntriesReq{
				Domain:   domain,
				EntryKey: entryKey,
			}

			Convey("When delete entries api returns error", func() {
				mockDeleteEntriesAPI.On("DeleteEntries", mock.Anything, request).Return(nil, errors.New("test-error"))

				Convey("Should return an internal error", func() {
					_, err := s.DeleteEntries(context.Background(), request)

					twirpErr, isOk := err.(twirp.Error)
					So(isOk, ShouldBeTrue)
					So(twirpErr.Code(), ShouldEqual, twirp.Internal)
				})
			})

			Convey("When delete entries api succeeds", func() {
				mockDeleteEntriesAPI.On("DeleteEntries", mock.Anything, request).Return(&pantheonrpc.DeleteEntriesResp{}, nil)

				Convey("Should return no error", func() {
					_, err := s.DeleteEntries(context.Background(), request)

					So(err, ShouldBeNil)
				})
			})
		})
	})
}
