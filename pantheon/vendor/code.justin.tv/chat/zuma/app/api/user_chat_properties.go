package api

// UserChatPropertiesRequest is sent to /v1/users/properties via POST.
type UserChatPropertiesRequest struct {
	UserID string `json:"user_id" validate:"nonzero"`
}

// ModifyUserChatPropertiesRequest is sent to /v1/users/set_properties via POST.
type ModifyUserChatPropertiesRequest struct {
	UserID    string  `json:"user_id" validate:"nonzero"`
	ChatColor *string `json:"chat_color"`
}

// UserChatPropertiesResponse is returned by /v1/users/properties and /v1/users/set_properties.
type UserChatPropertiesResponse struct {
	UserID    string `json:"user_id"`
	ChatColor string `json:"chat_color"`
}
