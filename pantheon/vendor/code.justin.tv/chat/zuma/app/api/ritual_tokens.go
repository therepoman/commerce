package api

import "time"

const (
	// RitualTypeNewChatter represents an experiment to grant a user a ritual
	// token 5 minutes after joining a channel.
	RitualTypeNewChatter = "NEW_CHATTER"
)

const (
	// RitualTokenStatusEligible means a user does not have this token and may
	// request it.
	RitualTokenStatusEligible = "ELIGIBLE"
	// RitualTokenStatusIneligible means a user does not have this token and
	// may not request it. A token is typically set to "ineligible" when a
	// backend service has concluded that a user is not eligible for the token.
	RitualTokenStatusIneligible = "INELIGIBLE"
	// RitualTokenStatusAvailable means a user has been granted this token and
	// it can be redeemed.
	RitualTokenStatusAvailable = "AVAILABLE"
	// RitualTokenStatusDismissed means a user has opted out of redeeming this token.
	RitualTokenStatusDismissed = "DISMISSED"
	// RitualTokenStatusRedeemed means a user has successfully consumed this token.
	RitualTokenStatusRedeemed = "REDEEMED"
)

// RitualToken stores properties about a token which the user can use to perform a ritual.
type RitualToken struct {
	ID         string     `json:"id"`
	RitualType string     `json:"type"`
	UserID     string     `json:"user_id"`
	ChannelID  string     `json:"channel_id"`
	ExpiresAt  *time.Time `json:"expires_at,omitempty"`
	Status     string     `json:"status"`
}

// UpdateRitualTokenRequest is the body for the POST /v1/rituals/tokens/update endpoint
type UpdateRitualTokenRequest struct {
	RitualType string `json:"type" validate:"nonzero"`
	UserID     string `json:"user_id" validate:"nonzero"`
	ChannelID  string `json:"channel_id" validate:"nonzero"`
	Status     string `json:"status" validate:"nonzero"`
	// MessageText is a user-specified string that is sent along with a ritual redemption.
	MessageText *string `json:"message_text"`
}

// UpdateRitualTokenResponse is the response for the POST /v1/rituals/tokens/update endpoint
type UpdateRitualTokenResponse struct {
	// ErrorCode is an enum that indicates the user error that occured.
	// Nil if the request was successful.
	//
	// Expected error codes:
	// - ritual_token_not_found
	// - ritual_token_not_available
	// - failed_enforcement_banned
	// - failed_enforcement_timed_out
	// - failed_enforcement_subs_only
	// - failed_enforcement_followers_only
	ErrorCode *string      `json:"error_code,omitempty"`
	Token     *RitualToken `json:"token"`
}

// ListRitualTokensByChannelRequest is the body for the POST /v1/rituals/tokens/list_by_channel endpoint
type ListRitualTokensByChannelRequest struct {
	UserID    string `json:"user_id" validate:"nonzero"`
	ChannelID string `json:"channel_id" validate:"nonzero"`
}

// ListRitualTokensByChannelResponse is the response for the POST /v1/rituals/tokens/list_by_channel endpoint
type ListRitualTokensByChannelResponse struct {
	Tokens []RitualToken `json:"tokens"`
}

// RequestEligibleRitualTokenRequest is the body for the POST /v1/rituals/tokens/request_eligible endpoint
type RequestEligibleRitualTokenRequest struct {
	RitualType string `json:"type" validate:"nonzero"`
	UserID     string `json:"user_id" validate:"nonzero"`
	ChannelID  string `json:"channel_id" validate:"nonzero"`
}

// RequestEligibleRitualTokenResponse is the response for the POST /v1/rituals/tokens/request_eligible endpoint
type RequestEligibleRitualTokenResponse struct {
	// ErrorCode is an enum that indicates the user error that occured.
	// Nil if the request was successful.
	//
	// Expected error codes:
	// - ritual_token_not_found
	// - ritual_token_not_available
	ErrorCode *string      `json:"error_code,omitempty"`
	Token     *RitualToken `json:"token"`
}
