package tmi

const (
	NoPermission = "no_permission"

	// Timeout/bans
	BadTimeoutAnon        = "bad_timeout_anon"
	BadTimeoutBroadcaster = "bad_timeout_broadcaster"
	BadTimeoutStaff       = "bad_timeout_staff"
	BadTimeoutAdmin       = "bad_timeout_admin"
	BadTimeoutSelf        = "bad_timeout_self"
	BadTimeoutMod         = "bad_timeout_mod"
	BadTimeoutDuration    = "bad_timeout_duration"
	TimeoutSuccess        = "timeout_success"

	BadBanAnon        = "bad_ban_anon"
	BadBanBroadcaster = "bad_ban_broadcaster"
	BadBanStaff       = "bad_ban_staff"
	BadBanAdmin       = "bad_ban_admin"
	BadBanSelf        = "bad_ban_self"
	BadBanMod         = "bad_ban_mod"
	BadBanError       = "bad_ban_error"
	BadUnbanError     = "bad_unban_error"
	BadUnbanNoBan     = "bad_unban_no_ban"
	AlreadyBanned     = "already_banned"
	BanSuccess        = "ban_success"
	UnbanSuccess      = "unban_success"
	TimeoutNoTimeout  = "timeout_no_timeout"
	UntimeoutSuccess  = "untimeout_success"
	UntimeoutIsBanned = "untimeout_banned"

	AddChannelBlockedTermSuccess           = "add_channel_blocked_term_success"
	AddChannelBlockedTermNoPermissions     = "bad_add_channel_blocked_term_no_permissions"
	AddChannelBlockedTermInvalidDuration   = "bad_add_channel_blocked_term_invalid_duration"
	AddChannelBlockedTermError             = "bad_add_channel_blocked_term_error"
	AddChannelBlockedTermPermittedConflict = "bad_add_channel_blocked_term_permitted_conflict"
	AddChannelBlockedTermBlockedConflict   = "bad_add_channel_blocked_term_blocked_conflict"

	DeleteChannelBlockedTermSuccess       = "delete_channel_blocked_term_success"
	DeleteChannelBlockedTermNoPermissions = "delete_channel_blocked_term_no_permissions"
	DeleteChannelBlockedTermError         = "delete_channel_blocked_term_error"

	AddChannelPermittedTermSuccess           = "add_channel_permitted_term_success"
	AddChannelPermittedTermNoPermissions     = "bad_add_channel_permitted_term_no_permissions"
	AddChannelPermittedTermInvalidDuration   = "bad_add_channel_permitted_term_invalid_duration"
	AddChannelPermittedTermError             = "bad_add_channel_permitted_term_error"
	AddChannelPermittedTermBlockedConflict   = "bad_add_channel_permitted_term_blocked_conflict"
	AddChannelPermittedTermPermittedConflict = "bad_add_channel_permitted_term_permitted_conflict"

	DeleteChannelPermittedTermSuccess       = "delete_channel_permitted_term_success"
	DeleteChannelPermittedTermNoPermissions = "delete_channel_permitted_term_no_permissions"
	DeleteChannelPermittedTermError         = "delete_channel_permitted_term_error"

	BadDeleteBroadcaster          = "bad_delete_message_broadcaster"
	BadDeleteMod                  = "bad_delete_message_mod"
	BadDeleteChatMessageError     = "bad_delete_message_error"
	DeleteStaffChatMessageSuccess = "delete_staff_message_success"
	DeleteChatMessageSuccess      = "delete_message_success"

	CannotHostSelfError     = "bad_host_self"
	AlreadyHostingError     = "bad_host_hosting"
	AlreadyNotHostingError  = "not_hosting"
	UnhostableChannelError  = "bad_host_rejected"
	HostTargetDisabledError = "host_target_disabled_error"
	HostServerError         = "bad_host_error"
	UnhostServerError       = "bad_unhost_error"
)
