package tmi

const (
	// ChatRichEmbedEventName is published when a chat message contains rich media embed information.
	ChatRichEmbedEventName = "chat_rich_embed"

	// HostTargetChangeEventName is published when a channel host or unhosts another channel.
	HostTargetChangeEventName = "host_target_change"

	// HostTargetChangeV2EventName is published when a channel hosts/unhosts or is hosted/unhosted.
	HostTargetChangeV2EventName = "host_target_change_v2"

	// ExtensionMessageEventName is published when an extension posts a message to chat
	ExtensionMessageEventName = "extension_message"

	// UpdatedRoomEventName is published when a channel's room state changes
	UpdatedRoomEventName = "updated_room"
)

// HostTargetChangePayload is published to pubsub when a channel's host target is changed.
type HostTargetChangePayload struct {
	// ChannelID is the channel that is/was hosting another channel.
	ChannelID string `json:"channel_id"`
	// ChannelLogin is the login matching ChannelID.
	ChannelLogin string `json:"channel_login"`
	// TargetChannelID is the other channel that is being hosted.
	// Nil if the channel is no longer hosting another channel.
	TargetChannelID *string `json:"target_channel_id"`
	// TargetChannelLogin is the login matching TargetChannelID. The login is
	// valuable to connect to a channel's chatroom, which is currently only
	// accessible with the hosted channel's login.
	TargetChannelLogin *string `json:"target_channel_login"`
	// PreviousTargetChannelID is the previous channel that was being hosted.
	// Nil if the channel was not previously hosting a channel.
	PreviousTargetChannelID *string `json:"previous_target_channel_id"`
	// NumViewers is the number of viewers in the hosting channel.
	NumViewers int `json:"num_viewers"`
}
