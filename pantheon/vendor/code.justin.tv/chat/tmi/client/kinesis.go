package tmi

import (
	"time"

	zumaapi "code.justin.tv/chat/zuma/app/api"
)

const (
	// UserMessageName is published to SNS when a user's privmsg is successfully
	// sent to ircpubsub.
	UserMessageName = "user_message"
	// DroppedUserMessageName is published to SNS when a user's privmsg is dropped because of
	// failing enforcement.
	DroppedUserMessageName = "dropped_user_message"
	//AnnouncementMessageName is published to SNS when a usernotice is successfully sent to ircpubsub.
	AnnouncementMessageName = "announcement_message"
	//ServerMessageName is published to SNS when a room notice is successfully sent to ircpubsub.
	ServerMessageName = "server_message"
)

type KinesisData struct {
	UUID       string      `json:"uuid"`
	Event      string      `json:"event"`
	Properties interface{} `json:"properties"`
}

type ContainerPayload struct {
	OwnerID    string `json:"owner_id"`
	Namespace  string `json:"namespace"`
	UniqueName string `json:"unique_name"`
}

type AnnouncementMessageServerContentPayload struct {
	Type   string            `json:"type"`
	Text   string            `json:"text"`
	Params map[string]string `json:"params"`
}

type ServerMessageContentPayload struct {
	Type string `json:"type"`
	Text string `json:"text"`
}

// UserMessagePayload stores data about a user message.
type UserMessagePayload struct {
	ID         string                   `json:"id"`
	Container  ContainerPayload         `json:"container"`
	Sender     zumaapi.MessageSender    `json:"sender"`
	Content    zumaapi.MessageContentV2 `json:"content"`
	BitsAmount int                      `json:"bits_amount"`
	SentAt     time.Time                `json:"sent_at"`
}

// DroppedUserMessagePayload stores data about a dropped user message.
type DroppedUserMessagePayload struct {
	ID                string                   `json:"id"`
	Container         ContainerPayload         `json:"container"`
	Sender            zumaapi.MessageSender    `json:"sender"`
	Content           zumaapi.MessageContentV2 `json:"content"`
	BitsAmount        int                      `json:"bits_amount"`
	EnforcementReason string                   `json:"enforcement_reason"`
	SentAt            time.Time                `json:"sent_at"`
}

// AnnouncementMessagePayload stores data about a usernotice message.
type AnnouncementMessagePayload struct {
	ID            string                                  `json:"id"`
	Container     ContainerPayload                        `json:"container"`
	Sender        zumaapi.MessageSender                   `json:"sender"`
	ServerContent AnnouncementMessageServerContentPayload `json:"server_content"`
	UserContent   zumaapi.MessageContentV2                `json:"user_content"`
	SentAt        time.Time                               `json:"sent_at"`
}

// ServerMessagePayload stores data about a room notice message.
type ServerMessagePayload struct {
	ID        string                      `json:"id"`
	Container ContainerPayload            `json:"container"`
	Content   ServerMessageContentPayload `json:"content"`
	SentAt    time.Time                   `json:"sent_at"`
}
