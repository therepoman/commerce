# workerqueue
Worker-queue is a library that enables easy implementation of an asynchronous workqueue using [Amazon SQS](https://aws.amazon.com/sqs/).

## Development

Install vendored dependencies (these are never committed)

    $ make glide

Run tests

    $ make test

## Contents

* `examples` contains example usages of this library.

* `publisher` contains the publisher logic that adds messages to the queue.

* `worker` contains the generic worker logic, including receiving messages from the queue and deleting completed or stale tasks.

## Usage

1. User creates a queue for a particular kind of task. One way to do so is using SQS's [web UI](https://console.aws.amazon.com/sqs/home).

2. Publisher imports `workerqueue/publisher` and calls `publisher.New()`, passing it the name of the queue created in step 1, a version number, and some other params.

3. Publisher publishes data to the queue as a JSON blob, using `addTask()`.

4. A separate worker service should be instantiated by importing `workerqueue/worker`. `worker.CreateWorkers()` takes several arguments, including the name of the queue, and a `tasks` map which maps `int64` version numbers to `taskFn()` methods, which specify the function to be called on the received data of that version number.

5. IAM permissions are necessary

    Workers
    ```
    sqs:ChangeMessageVisibility
    sqs:ChangeMessageVisibilityBatch
    sqs:DeleteMessage
    sqs:DeleteMessageBatch
    sqs:GetQueueAttributes
    sqs:GetQueueUrl
    sqs:ReceiveMessage
    ```

    Publishers
    ```
    sqs:GetQueueUrl
    sqs:SendMessage
    sqs:SendMessageBatch
    ```

    Both
    ```
    sqs:ChangeMessageVisibility
    sqs:ChangeMessageVisibilityBatch
    sqs:DeleteMessage
    sqs:DeleteMessageBatch
    sqs:GetQueueAttributes
    sqs:GetQueueUrl
    sqs:ReceiveMessage
    sqs:SendMessage
    sqs:SendMessageBatch
    ```

## Best Practices

One queue should be instantiated per type of task. This should be done manually, not programmatically.

Queues have a Message Retention Period which causes stale tasks to be automatically deleted after a period of time. The default is 4 days. See other [SQS Queue Attributes](http://docs.aws.amazon.com/AWSSimpleQueueService/latest/APIReference/API_SetQueueAttributes.html).

A shared JSON struct should be used both to marshal data for `addTask()` and to unmarshal in `taskFn()`. This guarantees the format of data is not specified in more than one place.

Tasks must be **versioned** by passing the publisher an `int64`. Workers can only handle versions that they know about. A different function can be defined for each version number.
