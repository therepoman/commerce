package workerqueue

import (
	"context"
	"errors"
	"fmt"
	"strconv"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/aws/aws-sdk-go/service/sqs/sqsiface"
	"github.com/cactus/go-statsd-client/statsd"
)

// AddTasksMax is the max number of tasks that can be published in one batch command.
const AddTasksMax = 10

// Publisher is an interface to publish to a queue.
type Publisher interface {
	AddTask(ctx context.Context, data []byte, opts *Options) error
	AddTasks(ctx context.Context, data ...[]byte) error
}

// PublisherConfig defines configurable parameters to create a client to publish
// to a queue.
type PublisherConfig struct {
	Version TaskVersion

	// AccountID is the AWS account that created the queue. If unset, defaults
	// to the AWS account of the caller.
	AccountID string

	// QueueName is the name (not ARN) of an SQS queue.
	QueueName string

	// Region is an optional AWS region. Either Region or Client must be set.
	Region string

	// Client is an optional SQS client. Either Region or Client must be set.
	Client sqsiface.SQSAPI
}

func validateConfig(config PublisherConfig) error {
	switch {
	case config.QueueName == "":
		return errors.New("invalid QueueName")
	case config.Client == nil && config.Region == "":
		return errors.New("invalid Region - required because no Client is set")
	}
	return nil
}

// NewPublisher returns a Publisher.
func NewPublisher(config PublisherConfig, stats statsd.Statter) (Publisher, error) {
	if err := validateConfig(config); err != nil {
		return nil, err
	} else if stats == nil {
		return nil, errors.New("invalid stats")
	}

	var client sqsiface.SQSAPI
	if config.Client != nil {
		client = config.Client
	} else {
		client = sqs.New(session.New(), aws.NewConfig().WithRegion(config.Region))
	}

	var accountID *string
	if config.AccountID != "" {
		accountID = aws.String(config.AccountID)
	}

	resp, err := client.GetQueueUrl(&sqs.GetQueueUrlInput{
		QueueName:              aws.String(config.QueueName),
		QueueOwnerAWSAccountId: accountID,
	})
	if err != nil {
		return nil, fmt.Errorf("getting queue URL: %v", err)
	}

	return &publisherImpl{
		client:    client,
		version:   config.Version,
		queueURL:  resp.QueueUrl,
		queueName: config.QueueName,
		stats:     stats,
	}, nil
}

type publisherImpl struct {
	client sqsiface.SQSAPI

	version   TaskVersion
	queueURL  *string
	queueName string
	stats     statsd.Statter
}

// Options defines optional configuration to the SQS message.
type Options struct {
	// Delay defines a duration to delay a specific message. The message becomes
	// visible on the queue after the delay duration. Must be a positive duration.
	// Max of 15 minutes.
	Delay time.Duration
}

func validateOptions(opts *Options) error {
	if opts == nil {
		return nil
	}
	if opts.Delay < 0 || opts.Delay > 15*time.Minute {
		return errors.New("Invalid Delay parameter")
	}
	return nil
}

func (p *publisherImpl) AddTask(ctx context.Context, data []byte, opts *Options) error {
	if err := validateOptions(opts); err != nil {
		return err
	}

	start := time.Now()

	params := &sqs.SendMessageInput{
		MessageBody: aws.String(string(data)),
		QueueUrl:    p.queueURL,
		MessageAttributes: map[string]*sqs.MessageAttributeValue{
			"PublishTime": {
				DataType:    aws.String("Number"),
				StringValue: aws.String(fmt.Sprintf("%d", start.Unix())),
			},
			"Version": {
				DataType:    aws.String("Number"),
				StringValue: aws.String(fmt.Sprintf("%d", p.version)),
			},
		},
	}
	if opts != nil && opts.Delay > 0 {
		params.DelaySeconds = aws.Int64(int64(opts.Delay.Seconds()))
	}

	_, err := p.client.SendMessageWithContext(ctx, params)
	if err != nil {
		p.stats.TimingDuration(p.statName("send.fail"), time.Since(start), 1.0)
		return err
	}
	p.stats.TimingDuration(p.statName("send.success"), time.Since(start), 1.0)
	return nil
}

func (p *publisherImpl) AddTasks(ctx context.Context, datas ...[]byte) error {
	if len(datas) == 0 {
		return errors.New("There must be at least one task")
	}

	if len(datas) > AddTasksMax {
		return errors.New("There are too many tasks")
	}

	var err error
	start := time.Now()

	defer func() {
		if err == nil {
			p.stats.TimingDuration(p.statName("batch_send.success"), time.Since(start), 1.0)
		} else {
			p.stats.TimingDuration(p.statName("batch_send.fail"), time.Since(start), 1.0)
		}
	}()

	entries := make([]*sqs.SendMessageBatchRequestEntry, len(datas))
	for i := range entries {
		data := datas[i]

		entries[i] = &sqs.SendMessageBatchRequestEntry{
			// set the `Id` to be the index in the array for lookup if an entry fails to send
			Id:          aws.String(strconv.Itoa(i)),
			MessageBody: aws.String(string(data)),
			MessageAttributes: map[string]*sqs.MessageAttributeValue{
				"PublishTime": {
					DataType:    aws.String("Number"),
					StringValue: aws.String(fmt.Sprintf("%d", start.Unix())),
				},
				"Version": {
					DataType:    aws.String("Number"),
					StringValue: aws.String(fmt.Sprintf("%d", p.version)),
				},
			},
		}
	}

	params := &sqs.SendMessageBatchInput{
		QueueUrl: p.queueURL,
		Entries:  entries,
	}

	out, err := p.client.SendMessageBatchWithContext(ctx, params)
	if err != nil {
		return err
	}

	if len(out.Failed) > 0 {
		// Per SQS docs "After a batch request, you should always check for individual message failures and retry them if necessary."
		// Ref: http://docs.aws.amazon.com/AWSSimpleQueueService/latest/SQSDeveloperGuide/throughput.html
		err = p.retryFailedAddTasks(entries, out.Failed)
		if err != nil {
			return err
		}
	}

	return nil
}

func (p *publisherImpl) retryFailedAddTasks(entries []*sqs.SendMessageBatchRequestEntry, failedEntries []*sqs.BatchResultErrorEntry) error {
	// construct the entries to retry
	retryEntries := make([]*sqs.SendMessageBatchRequestEntry, 0)

	for _, entry := range failedEntries {
		if entry.Id == nil {
			return fmt.Errorf("batch result entry is malformed, no Id: %v", entry)
		}

		index, err := strconv.Atoi(*entry.Id)
		if err != nil {
			return fmt.Errorf("batch result entry ID is not a number: %v", entry.Id)
		}

		if index < 0 || index >= len(entries) {
			return fmt.Errorf("batch result entry ID is out of range: %v [%d,%d)", index, 0, len(entries))
		}

		retryEntries = append(retryEntries, entries[index])
	}

	params := &sqs.SendMessageBatchInput{
		QueueUrl: p.queueURL,
		Entries:  retryEntries,
	}

	out, err := p.client.SendMessageBatch(params)

	if err != nil {
		return err
	}

	if len(out.Failed) > 0 {
		return fmt.Errorf("tasks failed to send: %v", out.Failed)
	}

	return nil
}

func (p *publisherImpl) statName(stat string) string {
	return fmt.Sprintf("service.%s.%s", p.queueName, stat)
}
