package rediczar

import (
	"context"

	"github.com/go-redis/redis/v7"
)

// cmdableWithContext attaches the context to the Cmdable or returns the Cmdable if the context
// cannot be attached
func cmdableWithContext(ctx context.Context, cmdable redis.Cmdable) redis.Cmdable {
	switch cl := cmdable.(type) {
	case *redis.Client:
		return cl.WithContext(ctx)
	case *redis.ClusterClient:
		return cl.WithContext(ctx)
	case *redis.Ring:
		return cl.WithContext(ctx)
	case *redis.Tx:
		return cl.WithContext(ctx)
	default:
		return cmdable
	}
}
