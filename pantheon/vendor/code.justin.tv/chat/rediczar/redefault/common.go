package redefault

import (
	"context"
	"fmt"
	"net"
	"time"

	"code.justin.tv/chat/ratecache"
	"golang.org/x/time/rate"
)

const (
	defaultPoolSize = 30

	// These Read and Write timeouts were tuned to reduce the number of client timeout errors during memory pressure.
	defaultReadTimeout  = 1 * time.Second
	defaultWriteTimeout = 500 * time.Millisecond

	defaultDialerTimeout = 1 * time.Second
	defaultKeepAlive     = 30 * time.Second
)

// dialerWithRateLimiting wraps an existing dialer with a rate limiter on the number of new connections that can be established
func dialerWithRateLimiting(poolSize int, baseDialer func(ctx context.Context, network, addr string) (net.Conn, error)) func(ctx context.Context, network, addr string) (net.Conn, error) {
	// Maximum number of conns per minute to a node is: (Number of instances) * (Burst+60). The goal of the
	// rate limiting is to prevent tens of thousands of connections per minute, which increases CPU load. Also,
	// too many new connections can exhaust ephemeral ports on a node, which causes internal health checks to
	// fail and trigger a node replacement.
	rateCache := ratecache.Store{
		Limit: rate.Every(time.Second),
		Burst: poolSize + (poolSize / 2), // Add burst headroom
	}

	return func(ctx context.Context, network string, address string) (net.Conn, error) {
		limiter := rateCache.CreateOrGet(address)
		if !limiter.Allow() {
			return nil, fmt.Errorf("dialing connection rate limited: address=%s", address)
		}

		return baseDialer(ctx, network, address)
	}
}
