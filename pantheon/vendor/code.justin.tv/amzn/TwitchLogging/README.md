# `TwitchLogging`

This package represents an interface that can be implemented by many custom logging solutions. The main idea behind this
interface is to establish having structured logs by allowing log messages to be accompanied by key, value pairs.

For more information on logging, please see the [Fulton Logging Doc](https://docs.fulton.twitch.a2z.com/docs/logging.html).
A simple implementation of this interface can be found in [TwitchCommonLoggers](https://code.amazon.com/packages/TwitchLoggingCommonLoggers/trees/mainline).

## Referring to this package
To refer to this dependency, refer to its alias of `code.justin.tv/amzn/TwitchLogging`

## Help
For help using this package, please ping the [#fulton](https://twitch.slack.com/messages/C9BUPDUC8) Slack channel