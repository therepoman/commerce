package logging

// Logger is an interface for logging methods
type Logger interface {
	// Log is the general logging method.
	//
	// keyvals represent the keys and values to be used with the msg.
	// It is expected that implementations support both an even and odd number of keyvals.
	// In an even case, the parameters are expected to alternate between keys and their corresponding values.
	// In the odd length case, it is expected that keyvals alternate between keys and their corresponding values,
	// with the final entry being assigned to a predefined key (the final entry should be treated as a value with some
	// implementation-specific associated key).
	//
	// Keys in keyvals are expected to be strings and both keys and their associated values are expected to be outputted
	// as reasonable strings when processed by fmt.Sprint(%v). fmt.Sprint(%v) is the default processing method for
	// both keys and values and both will be mapped using it (this is the expected behavior for Logger implementations).
	//
	// Use other key/value mapping methods at your own risk.
	Log(msg string, keyvals ...interface{})
}
