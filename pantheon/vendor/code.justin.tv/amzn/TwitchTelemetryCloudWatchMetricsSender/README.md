# `TwitchTelemetryCloudWatchMetricsSender`

This is a package that can be used for sending metrics to the [CloudWatch PutMetricData endpoint](https://docs.aws.amazon.com/AmazonCloudWatch/latest/APIReference/API_PutMetricData.html). 

This package is meant to be used with samples sent from [TwitchTelemetry](https://code.amazon.com/packages/TwitchTelemetry/trees/mainline). Please note that the actual client performing the send operation is the native CloudWatch client that is part of the [AWS Go SDK](https://code.amazon.com/packages/GoAmzn-Github-Aws-AwsSdkGo/trees/aws-sdk-go-1.x).

When taking in new samples, this package creates appropriate rollups using the samples's designated RollupDimensions. This means that, it will create additional datums for the rollups (e.g. there will be multiple metrics in CloudWatch, with the rollup dimensions missing).  For more information on the dimensions being used and the rollups process, please see the [Twitch Telemetry Naming Conventions document](https://docs.google.com/document/d/1-1z7B9HS8_YRYne09DvIwsN_jvYSGw77SjX0u4GWetM/edit#heading=h.uudp9kaqkwsz).

Additionally, please note that, since CloudWatch only allows request sizes of [40 KB or 20 metric datums](https://docs.aws.amazon.com/AmazonCloudWatch/latest/APIReference/API_PutMetricData.html), requests will be made every 20 datums. This means that depending on the number of rollups, a single sample could lead to multiple CloudWatch calls.

This package supports histograms and allows for batching unlimited occurrences of up to 150 unique values for any of the 20 
unique metrics it reports per call (i.e. you can have 20 unique metrics, each with up to 150 different values, where each
value has an associated count for the number of times it occurred). It additionally breaks down requests to prevent the 
40 KB limit from being exceeded.

Each dimension name and value from a TwitchTelemetry is mapped directly to a CloudWatch dimension (no naming/ mapping conversions are made).

#### Referring to this package
To refer to this dependency, refer to its alias of `code.justin.tv/amzn/TwitchTelemetryCloudWatchMetricsSender`

#### Running unit tests
When using brazil, simply run `brazil-build` to build and run all unit tests

#### Help
For help using this package, please ping the [#fulton](https://twitch.slack.com/messages/C9BUPDUC8) Slack channel