freeStyleJob('common-yimg') {
  using 'TEMPLATE-autobuild'
  scm {
    git {
      remote {
        github 'common/yimg', 'ssh', 'git-aws.internal.justin.tv'
        credentials 'git-aws-read-key'
      }
      clean true
    }
  }
  steps {
    shell 'manta -v -f build.json'
  }
}
