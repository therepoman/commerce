package models

import "time"

func isEqualString(s1 *string, s2 *string) bool {
	if s1 == nil && s2 == nil {
		return true
	}
	if s1 == nil || s2 == nil {
		return false
	}
	return *s1 == *s2
}

func isEqualUint64(s1 *uint64, s2 *uint64) bool {
	if s1 == nil && s2 == nil {
		return true
	}
	if s1 == nil || s2 == nil {
		return false
	}
	return *s1 == *s2
}

func isEqualBool(s1 *bool, s2 *bool) bool {
	if s1 == nil && s2 == nil {
		return true
	}
	if s1 == nil || s2 == nil {
		return false
	}
	return *s1 == *s2
}

func isEqualTime(s1 *time.Time, s2 *time.Time) bool {
	if s1 == nil && s2 == nil {
		return true
	}
	if s1 == nil || s2 == nil {
		return false
	}

	return *s1 == *s2
}

func StringValue(s *string) string {
	if s == nil {
		return ""
	} else {
		return *s
	}
}

func TimeValue(t *time.Time) time.Time {
	if t == nil {
		return time.Time{}
	} else {
		return *t
	}
}
func BoolValue(b *bool) bool {
	if b == nil {
		return false
	} else {
		return *b
	}
}

func Bool(v bool) *bool {
	return &v
}
