package models

import (
	"time"

	"code.justin.tv/common/yimg"
)

type UploadImageEvent struct {
	UserID    string      `json:"user_id"`
	Type      string      `json:"type"`
	ImageType string      `json:"image_type"`
	NewImage  yimg.Images `json:"new_image"`
	Status    string      `json:"status"`
	UploadID  string      `json:"upload_id"`
}

type UpdateChannelPropertiesEvent struct {
	ChannelID string  `json:"channel_id"`
	Type      string  `json:"type"`
	Channel   string  `json:"channel"`
	OldStatus *string `json:"old_status"`
	Status    *string `json:"status"`
	OldGame   *string `json:"old_game"`
	Game      *string `json:"game"`
	OldGameID *uint64 `json:"old_game_id"`
	GameID    *uint64 `json:"game_id"`
}

type UpdateUserPropertiesEvent struct {
	UserID    string                  `json:"user_id"`
	Type      string                  `json:"type"`
	Timestamp time.Time               `json:"timestamp"`
	Original  PropertiesUpdatePrivate `json:"original"`
	Changed   PropertiesUpdatePrivate `json:"changed"`
}

// ChannelTosViolationEvent is sent to pubsub on video-playback.<channel_name>
// and video-playback-by-id.<channel_id> when a channel gets a TOS strike.
type ChannelTosViolationEvent struct {
	Type       string  `json:"type"`
	ServerTime float64 `json:"server_time"`
}

// PropertiesUpdatePrivate represents the updateable properties that can be
// exposed to the user who they belong to.
// This notably omits such fields as RemoteIP, Location, and BannedUntil.
type PropertiesUpdatePrivate struct {
	Birthday           *time.Time `json:"birthday,omitempty"`
	Description        *string    `json:"description,omitempty"`
	Displayname        *string    `json:"displayname,omitempty"`
	EmailVerified      *bool      `json:"email_verified,omitempty"`
	Language           *string    `json:"language,omitempty"`
	LastLogin          *string    `json:"last_login,omitempty"`
	Login              *string    `json:"login,omitempty"`
	PhoneNumberChanged *bool      `json:"phone_number_changed,omitempty"`
}

// ExtractUpdate creates a pair of PropertiesUpdatePrivate structs representing
// the changes being made to a user's properties. Only fields that are being
// updated will be non-nil in these structs. If no visible fields are being updated,
// the 'updated' boolean will be false.
func (p Properties) ExtractUpdate(up *UpdateableProperties) (PropertiesUpdatePrivate, PropertiesUpdatePrivate, bool) {
	var original, changed PropertiesUpdatePrivate
	if up == nil {
		return original, changed, false
	}

	updated := false
	if !isEqualTime(up.Birthday, p.Birthday) {
		updated = true
		original.Birthday = p.Birthday
		changed.Birthday = up.Birthday
	}
	if !isEqualString(up.Description, p.Description) {
		updated = true
		original.Description = p.Description
		changed.Description = up.Description
	}
	if !isEqualString(up.Displayname, p.Displayname) {
		updated = true
		original.Displayname = p.Displayname
		changed.Displayname = up.Displayname
	}
	if !isEqualBool(up.EmailVerified, p.EmailVerified) {
		updated = true
		original.EmailVerified = p.EmailVerified
		changed.EmailVerified = up.EmailVerified
	}
	if !isEqualString(up.Language, p.Language) {
		updated = true
		original.Language = p.Language
		changed.Language = up.Language
	}
	if !isEqualString(up.LastLogin, p.LastLogin) {
		updated = true
		original.LastLogin = p.LastLogin
		changed.LastLogin = up.LastLogin
	}
	if !isEqualString(p.Login, up.NewLogin) {
		updated = true
		original.Login = p.Login
		changed.Login = up.NewLogin
	}
	if !isEqualString(up.PhoneNumber, p.PhoneNumber) {
		updated = true
		changed.PhoneNumberChanged = Bool(true)
	}
	return original, changed, updated
}
