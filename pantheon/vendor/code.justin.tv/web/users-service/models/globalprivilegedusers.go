package models

type GlobalPrivilegedUsers struct {
	Admins     []string `json:"admins,omitempty"`
	SubAdmins  []string `json:"subadmins,omitempty"`
	GlobalMods []string `json:"global_mods,omitempty"`

	AdminIDs     []string `json:"admin_ids,omitempty"`
	SubAdminIDs  []string `json:"subadmin_ids,omitempty"`
	GlobalModIDs []string `json:"global_mod_ids,omitempty"`
}
