package models

type CachePair struct {
	Key   string
	Value string
}

type Cacheable interface {
	CachePairs() []CachePair
}

type CacheableIterator interface {
	Each(func(Cacheable) error) error
}
