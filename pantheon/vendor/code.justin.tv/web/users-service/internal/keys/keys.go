package keys

const (
	UserID      = "u.id"
	Login       = "login"
	Email       = "lower(email)"
	DisplayName = "lower(displayname)"
	Ip          = "remote_ip"
	Phone       = "phone_number"
)
