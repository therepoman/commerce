# splatter
Assorted implementations for the statsd.Statter interface

## Why splatter?
1. "All of these twitch libraries are expecting a Statsd integration for metrics, but I want to send my metrics somewhere else."
  * `Solution`: Use the CloudwatchStatter to send them to Cloudwatch! Add your own Statter if you want to send them somewhere else.
2. "I want to send my metrics to Statsd so I can leverage Grafana, but I also want to send them to Cloudwatch to set up monitors."
  * `Solution`: Use the CompositeStatter to send your metrics to multiple destinations!
3. "I am trying to setup metrics in my service but something isn't working."
  * `Solution`: Use the LogStatter to emit stats to logs to aid with debugging!

## Development
Dependencies - uses `dep`

    $ dep ensure

Testing

    $ go test

Run example

    $ go run cmd/example/main.go

## Contents
* New implementations for the `statsd.Statter` interface:
  * `TwitchTelemetryStatter` - Sends metrics to TwitchTelemetry libraries (e.g. NewBufferedTelemetryCloudWatchStatter sends to CloudWatch).
  * `CloudwatchStatter` - Sends metrics to Cloudwatch.
  * `LogStatter` - Sends metrics to log output.
  * `CompositeStatter` - Sends metrics to all injected Statters. Can be used to send metrics to multiple destinations.
* Cloudwatch `MetricLogger` - a simple metric logger for cloudwatch
* Cloudwatch `MetricFlusher` - stores metrics in a local buffer and periodically flushes metrics to Cloudwatch

## Usage
See `cmd/example/main.go` for full example usage of this library.

```go
	// Create a splatter.CloudwatchStatter to send metrics to Cloudwatch
	cloudwatchStatter, err := splatter.NewCloudwatchStatter(metricsLogger, statsPrefix)
	if err != nil {
		log.WithError(err).Fatal("Failed to initialize Cloudwatch Statter")
	}

	// Create a standard statsd Statter to send metrics to the statsd endpoint
	statsdStatter, err := statsd.NewClient("statsd.internal.justin.tv:8125", statsPrefix)
	if err != nil {
		log.WithError(err).Fatal("Failed to connect to statsd endpoint. Make sure you are on JTV-SFO network if running locally.")
	}

	// Create a splatter.LogStatter to send metrics to the logs for debugging
	logStatter, err := splatter.NewLogStatter(statsPrefix)
	if err != nil {
		log.WithError(err).Fatal("Failed to initialize Log Statter")
	}

	// Build a slice of statters that you want to inject into a splatter.CompositeStatter.
	statters := []statsd.Statter{cloudwatchStatter, statsdStatter, logStatter}

	// Create a splatter.CompositeStatter and inject all of the Statters into it! All of the individual Statters will be called.
	stats, err := splatter.NewCompositeStatter(statters)
	if err != nil {
		log.WithError(err).Fatal("Failed to initialize Composite Statter")
	}
	defer stats.Close() // This will close all of the child Statters
```

### TwitchTelemetry
To use TwitchTelemetry, you need to construct a config and use it to generate a buffered Telemetry statter. This acts
as a shim from statsd metrics to your chosen TwitchTelemetry implementation. For example,
`NewBufferedTelemetryCloudWatchStatter` will construct a new TwitchTelemetry Buffered `SampleObserver` that will
aggregate and batch all metrics, flushing metrics to CloudWatch using the provided `BufferedTelemetryConfig.FlushPeriod`.
Likewise, `NewBufferedTelemetryMWSStatter` can be used to flush metrics to MWS (the internal Amazon metrics platform
backed by CloudWatch).

Please note that metrics will appear in CloudWatch/ MWS as-is (the metric name will be the statsd string with the optional
provided prefix). The one exception is `Dec`, will will append `.negative` to the metric name. This can then be used
with metric math alarms/ dashboards to determine the appropriate end value.

Metrics will additionally have dimensions for the provided BufferedTelemetryConfig information (Region, Service, Stage,
and Substage) and will also have appropriate dimensional rollups (Substage will be rolled up). This allows seeing
different metrics for substages like `canary` and `primary` while also maintaining metrics across the entire Stage
(such as `production`)


```go
    // Create a config for TwitchTelemetry Buffered Senders
	buffConfig := &BufferedTelemetryConfig {
		FlushPeriod: 30 * time.Second,  // How often to send metrics
		BufferSize: 100000,             // Size of the buffer
		AggregationPeriod: time.Minute, // Aggregation period of metrics, e.g. 1 minute metrics
		ServiceName:"TwitchMyService",  // Name of your service
		AWSRegion: "us-west-2",         // AWS Region of your service
		Stage: "production",            // Main stage of your service (e.g. production, development, etc.)
		Substage: "canary",             // A substage for the stage (e.g. primary, canary, etc.)
		Prefix: "somePrefix",           // Optional prefix for all metrics
	}
    // Create a statter using the given config and an optional map of metrics to ignore
    // If metrics are in the map as "someMetric" -> true, then "someMetric" will not be reported
	telemetryStatter :=  NewBufferedTelemetryCloudWatchStatter(buffConfig, map[string]bool{})
```