package cloudwatch

import (
	"errors"
	"fmt"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/cloudwatch"
	"github.com/aws/aws-sdk-go/service/cloudwatch/cloudwatchiface"
	log "github.com/sirupsen/logrus"
)

// IMetricFlusher interface
type IMetricFlusher interface {
	AddMetricToBuffer(*cloudwatch.MetricDatum) error
	FlushMetrics() error
}

// MetricFlusher daemon which is responsible for the goroutine that flushes metrics to Cloudwatch
type MetricFlusher struct {
	cloudWatch    cloudwatchiface.CloudWatchAPI
	metricsBuffer chan *cloudwatch.MetricDatum
	config        *MetricFlusherConfig
	lastFlushTime time.Time
}

// MetricFlusherConfig configuration for the MetricFlusher
type MetricFlusherConfig struct {
	AWSRegion                      string        // us-west-2, etc.
	BatchSize                      int           // metrics are sent to cloudwatch in batches
	BufferEmergencyFlushPercentage float64       // (0.0 to 1.0) how close the buffer can get to being full before it forces a flush
	BufferSize                     int           // local metrics buffer size
	FlushInterval                  time.Duration // how frequently the buffer should be flushed
	FlushPollCheckDelay            time.Duration // how long to wait before checking whether or not to flush
	Namespace                      string        // metric namespace
}

// NewMetricFlusher creates a new MetricFlusher using the inputs.
// Returns an error if any inputs are invalid.
func NewMetricFlusher(config *MetricFlusherConfig) (IMetricFlusher, error) {
	if config.AWSRegion == "" {
		return nil, errors.New("AWSRegion cannot be empty")
	}
	awsConfig := &aws.Config{
		Region: aws.String(config.AWSRegion),
	}

	client := cloudwatch.New(session.New(), awsConfig)
	return NewMetricFlusherWithClient(config, client)
}

// NewMetricFlusherWithClient creates a new MetricFlusher using the input cloudwatch client.
// Returns an error if any inputs are invalid.
func NewMetricFlusherWithClient(config *MetricFlusherConfig, cloudwatchClient cloudwatchiface.CloudWatchAPI) (IMetricFlusher, error) {
	err := validateConfig(config)
	if err != nil {
		return nil, fmt.Errorf("Validation error: %v", err)
	}
	metricsBufferChannel := make(chan *cloudwatch.MetricDatum, config.BufferSize)

	flusher := &MetricFlusher{
		cloudWatch:    cloudwatchClient,
		metricsBuffer: metricsBufferChannel,
		config:        config,
		lastFlushTime: time.Now(),
	}

	// Spawn goroutine to run indefinitely
	go flusher.flushMetricsAtInterval()

	return flusher, nil
}

// AddMetricToBuffer adds a metric to the local metric buffer. The metric
// will be flushed to cloudwatch according to the MetricFlusher's configuration
func (flusher *MetricFlusher) AddMetricToBuffer(metricDatum *cloudwatch.MetricDatum) error {
	if len(flusher.metricsBuffer) >= flusher.config.BufferSize {
		return errors.New("Metrics buffer at capacity. Additional metrics will be dropped until the buffer is flushed.")
	}
	flusher.metricsBuffer <- metricDatum
	return nil
}

// FlushMetrics forces a flush of the buffered metrics. Calling this method is not required,
// as the MetricFlusher will periodically call it from its daemon.
// Returns an error if a failure was encountered while flushing the metrics.
func (flusher *MetricFlusher) FlushMetrics() error {
	numMetricsToFlush := len(flusher.metricsBuffer)

	numSucceeded, numFailed := 0, 0
	metricBatch := make([]*cloudwatch.MetricDatum, 0, flusher.config.BatchSize)
	for i := 0; i < numMetricsToFlush; i++ {

		metric := <-flusher.metricsBuffer
		metricBatch = append(metricBatch, metric)

		// Send the batch if it is at capacity or this is the last metric
		if len(metricBatch) >= flusher.config.BatchSize || i == (numMetricsToFlush-1) {
			err := flusher.postMetricBatch(metricBatch)
			if err != nil {
				log.Warnf("Failed to send %d metrics while communicating with cloudwatch. Error: %v", len(metricBatch), err)
				numFailed += len(metricBatch)
			} else {
				numSucceeded += len(metricBatch)
			}
			metricBatch = make([]*cloudwatch.MetricDatum, 0, flusher.config.BatchSize)
		}
	}

	if numFailed > 0 {
		return fmt.Errorf("Finished flushing metrics, but encountered some errors. Succeeded: [%d], Failed: [%d]", numSucceeded, numFailed)
	}
	return nil
}

func (flusher *MetricFlusher) flushMetricsAtInterval() {
	flusher.lastFlushTime = time.Now()
	for {
		// Start the polling delay timer
		timer := time.After(flusher.config.FlushPollCheckDelay)

		if flusher.shouldFlush() {
			err := flusher.FlushMetrics()
			if err != nil {
				log.Warnf("Error flushing metrics: %v", err)
			}
			flusher.lastFlushTime = time.Now()
		}

		// Wait until the polling delay timer expires
		<-timer
	}
}

// shouldFlush determines whether or not the dameon should flush
func (flusher *MetricFlusher) shouldFlush() bool {
	return flusher.isNextFlushInterval() || flusher.isBufferApproachingCapacity()
}

func (flusher *MetricFlusher) isNextFlushInterval() bool {
	timeSinceLastFlush := time.Now().Sub(flusher.lastFlushTime)
	return timeSinceLastFlush > flusher.config.FlushInterval
}

func (flusher *MetricFlusher) isBufferApproachingCapacity() bool {
	bufferUsage := float64(len(flusher.metricsBuffer)) / float64(flusher.config.BufferSize)
	approachingCapacity := bufferUsage >= flusher.config.BufferEmergencyFlushPercentage
	if approachingCapacity {
		log.Warnf("The metrics buffer is approaching capacity. Should execute emergency flush. Current buffer usage: %f", bufferUsage)
	}
	return approachingCapacity
}

func (flusher *MetricFlusher) postMetricBatch(metricsBatch []*cloudwatch.MetricDatum) error {
	request := &cloudwatch.PutMetricDataInput{
		MetricData: metricsBatch,
		Namespace:  aws.String(flusher.config.Namespace),
	}
	_, err := flusher.cloudWatch.PutMetricData(request)
	return err
}

func validateConfig(config *MetricFlusherConfig) error {
	if config.BatchSize <= 0 {
		return errors.New("BatchSize must be greater than 0")
	}
	if config.BufferSize <= 0 {
		return errors.New("BufferSize must be greater than 0")
	}
	if config.BufferEmergencyFlushPercentage <= 0.0 || config.BufferEmergencyFlushPercentage > 1.0 {
		return errors.New("BufferEmergencyFlushPercentage must be greater than 0.0 and less than or equal to 1.0")
	}
	if config.FlushInterval <= time.Duration(0) {
		return errors.New("FlushInterval must be greater than 0")
	}
	if config.FlushPollCheckDelay <= time.Duration(0) {
		return errors.New("FlushPollCheckDelay must be greater than 0")
	}
	if config.Namespace == "" {
		return errors.New("Namespace cannot be empty")
	}
	return nil
}
