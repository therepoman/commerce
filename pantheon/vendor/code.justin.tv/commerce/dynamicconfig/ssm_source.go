package dynamicconfig

import (
	"context"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/request"
	"github.com/aws/aws-sdk-go/service/ssm"
	"github.com/pkg/errors"
)

const ssmBatchSize = 10

// private indirection to support easier testing
type parameterGetter interface {
	GetParametersWithContext(ctx aws.Context, input *ssm.GetParametersInput, opts ...request.Option) (*ssm.GetParametersOutput, error)
}

// SSMSource sources values from SSM Parameter Store.
type SSMSource struct {
	// ssmClient is used to communicate with AWS SSM
	ssmClient parameterGetter

	// keys to fetch from SSM
	keys []string
}

// NewSSMSource creates a source for DynamicConfig backed by SSM.
func NewSSMSource(keys []string, ssmClient *ssm.SSM) *SSMSource {
	return &SSMSource{
		ssmClient: ssmClient,
		keys:      keys,
	}
}

// FetchValues from SSM Parameter Store and return a key->value map.
func (s *SSMSource) FetchValues(ctx context.Context) (map[string]interface{}, error) {
	// Short circuit if there are no keys. Necessary since GetParameters errors out if an empty
	// list of keys is passed in.
	if len(s.keys) == 0 {
		return make(map[string]interface{}), nil
	}

	var retrievedParams []*ssm.Parameter

	// SSM key limit per request is 10, so we divvy into batches.
	for _, batch := range divideKeyBatches(s.keys, ssmBatchSize) {
		ssmResp, err := s.ssmClient.GetParametersWithContext(ctx, &ssm.GetParametersInput{
			Names: mapStringsToPtrs(batch),
		})
		if err != nil {
			return nil, errors.Wrap(err, "getting parameters from SSM")
		}

		retrievedParams = append(retrievedParams, ssmResp.Parameters...)
	}

	newStore := make(map[string]interface{}, len(retrievedParams))
	for _, param := range retrievedParams {
		if param.Value != nil {
			newStore[*param.Name] = *param.Value
		}
	}

	return newStore, nil
}

func divideKeyBatches(input []string, limit int) [][]string {
	var batches [][]string
	for len(input) > 0 {
		i := limit
		if len(input) < limit {
			i = len(input)
		}

		// slice a batch from the front of input and update input
		batches = append(batches, input[:i])
		input = input[i:]
	}
	return batches
}

func mapStringsToPtrs(input []string) []*string {
	var out []*string
	for _, s := range input {
		s := s // shadow to prevent sharing a pointer to the single loop variable, which gets reassigned
		out = append(out, &s)
	}
	return out
}