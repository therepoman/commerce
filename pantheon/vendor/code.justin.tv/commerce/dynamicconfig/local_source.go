package dynamicconfig

import (
	"context"
	"sync"
)

type LocalSource struct {
	data map[string]interface{}
	err  error
	mu   *sync.Mutex
}

func NewLocalSource() *LocalSource {
	return &LocalSource{
		data: make(map[string]interface{}),
		err:  nil,
		mu:   &sync.Mutex{},
	}
}

func (ls *LocalSource) FetchValues(ctx context.Context) (map[string]interface{}, error) {
	ls.mu.Lock()
	defer ls.mu.Unlock()

	if ls.err != nil {
		return nil, ls.err
	}

	return ls.data, nil
}

func (ls *LocalSource) Set(data map[string]interface{}, err error) {
	ls.mu.Lock()
	defer ls.mu.Unlock()

	ls.data = data
	ls.err = err
}
