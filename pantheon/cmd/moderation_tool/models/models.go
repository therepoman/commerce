package models

type Status string

const (
	OK                    = Status("OK")
	ERROR                 = Status("ERROR")
	SHOULD_BE_MODERATED   = Status("SHOULD_BE_MODERATED")
	SHOULD_BE_UNMODERATED = Status("SHOULD_BE_UNMODERATED")
)
