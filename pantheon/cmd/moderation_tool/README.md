# Moderation Tool

So you need to do some kind of moderation thing... how nice. Well, this should help you figure out if the user is correctly or incorrectly moderated, as well as fix the moderation status in the channel from Pantheon's point of view.

## Prerequisites

You're going to need to do a couple things first to get this to work

### Have AWS Credentials for Pantheon

You'll need read permissions to the moderations table you're going to be reading. You'll need an access key and secret key to the account, which is normally provided via a profile you can set up via:

```
[profile-name]
region = us-west-2
credential_process = isengard_credentials --account-id ACCOUNT_ID --role ROLE_YOU_NEED
```

### Have a place where you can call TMI

TMI is the source of truth we use for ban status. You'll need a place that's Privatelinked to TMI to call their ban status API, which the best place is normally a jumpbox in that account. Percy has access (`TC=twitch-percy-aws-prod`), I'd use that.


### Be on the Twitch VPN
You need to be on the Twitch VPN

## Steps

### Make a Proxy to the Jumpbox with TMI Access

First, we need to build the tool you're going to use. 

```bash
TC=bastion-name ssh -D 8080 jumpbox
```

This will create a SOCKS proxy that forwards traffic from 8080 to the jumpbox. If you're specifying a port other than 8080, you'll need to provde that via the `-p` flag in the script. 

### Options for Script
#### Running the Script to Validate

After establishing the proxy, run the following to validate the user moderation in the channel by running (this is an example):

```bash
AWS_PROFILE=profile-name go run cmd/moderation_tool/main.go single-user validate -d bits-usage-by-channel-v1 -c 178002400 -u 466505373 -e prod
```

This will tell you if the user is correct or incorrect moderation state. You can proceed to fixing if they're in an incorrect state. 

#### Running the Script to Fix

first you want to dry run the fix. This will allow you to know what will be done as a result of the script. 

```bash
AWS_PROFILE=profile-name go run cmd/moderation_tool/main.go single-user fix -d bits-usage-by-channel-v1 -c 178002400 -u 466505373 -e prod --is-dry-run
```

You are required to do this to validate what you're doing before you run the script. We don't want to incorrectly moderate anyone.

After someone else has validated the output of the dry run, then you can run the script for real.

```bash
AWS_PROFILE=profile-name go run cmd/moderation_tool/main.go single-user fix -d bits-usage-by-channel-v1 -c 178002400 -u 466505373 -e prod --is-dry-run=false
```

This will send a message to pantheon, so that the corresponding leaderboard in Redis is updated as well.