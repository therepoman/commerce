package tools

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"time"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pantheon/cmd/moderation_tool/models"
	"code.justin.tv/commerce/pantheon/cmd/moderation_tool/setup"
	"code.justin.tv/commerce/pantheon/config"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	"github.com/pkg/errors"
)

type SingleUserCommands struct {
	cfg     *config.Config
	clients *setup.Clients
}

func NewSingleUserCommands(cfg *config.Config, clients *setup.Clients) *SingleUserCommands {
	return &SingleUserCommands{
		cfg:     cfg,
		clients: clients,
	}
}

func (s *SingleUserCommands) ValidateBanStatus(domain, channelID, userID string) error {
	_, err := s.getBanDiff(domain, channelID, userID)
	return err
}

func (s *SingleUserCommands) FixBanStatus(domain, channelID, userID string, isDryRun bool) error {
	status, err := s.getBanDiff(domain, channelID, userID)
	if err != nil {
		return err
	}

	switch status {
	case models.SHOULD_BE_MODERATED:
		moderateEvent := pantheonrpc.ModerateEntryReq{
			Domain:           domain,
			GroupingKey:      channelID,
			EntryKey:         userID,
			TimeOfEvent:      uint64(time.Now().UnixNano()),
			ModerationAction: pantheonrpc.ModerationAction_MODERATE,
		}
		if !isDryRun {
			_, err = s.clients.PantheonClient.ModerateEntry(context.Background(), &moderateEvent)
			if err != nil {
				logrus.WithError(err).Error("failed to send moderate event to pantheon")
				return err
			}
			logrus.Info("successfully sent moderate event to pantheon")
		} else {
			logrus.Info("would send moderate event to pantheon")
		}
		messageJSON, err := json.Marshal(&moderateEvent)
		if err != nil {
			return errors.Wrap(err, "could not marshal JSON")
		}

		var prettyJSON bytes.Buffer
		err = json.Indent(&prettyJSON, messageJSON, "", "  ")
		if err != nil {
			return errors.Wrap(err, "could not format JSON indents")
		}

		fmt.Println(prettyJSON.String())
		return nil
	case models.SHOULD_BE_UNMODERATED:
		unmoderateEvent := pantheonrpc.ModerateEntryReq{
			Domain:           domain,
			GroupingKey:      channelID,
			EntryKey:         userID,
			TimeOfEvent:      uint64(time.Now().UnixNano()),
			ModerationAction: pantheonrpc.ModerationAction_UNMODERATE,
		}
		if !isDryRun {
			_, err = s.clients.PantheonClient.ModerateEntry(context.Background(), &unmoderateEvent)
			if err != nil {
				logrus.WithError(err).Error("failed to send unmoderate event to pantheon")
				return err
			}
			logrus.Info("successfully sent unmoderate event to pantheon")
		} else {
			logrus.Info("would send unmoderate event to pantheon")
		}
		messageJSON, err := json.Marshal(&unmoderateEvent)
		if err != nil {
			return errors.Wrap(err, "could not marshal JSON")
		}

		var prettyJSON bytes.Buffer
		err = json.Indent(&prettyJSON, messageJSON, "", "  ")
		if err != nil {
			return errors.Wrap(err, "could not format JSON indents")
		}
		fmt.Println(prettyJSON.String())
		return nil
	case models.OK:
		logrus.Info("no action is needed")
		return nil
	default:
		logrus.Infof("somehow we didn't catch this case: %v", status)
		return nil
	}
}

func (s *SingleUserCommands) getBanDiff(domain, channelID, userID string) (models.Status, error) {
	pantheonModerationEntry, err := s.clients.ModerationsDAO.GetModeration(fmt.Sprintf("%s/%s", domain, channelID), userID)
	if err != nil {
		return models.ERROR, errors.Wrap(err, "failed to get user from moderations table")
	} else if pantheonModerationEntry == nil {
		logrus.Info("no moderation pantheonModerationEntry for requested domain, channel, and user")
		return models.OK, nil
	}

	_, isBanned, err := s.clients.ClueClient.GetBanStatus(context.Background(), channelID, userID, nil, nil)
	if err != nil {
		return models.ERROR, errors.Wrap(err, "failed to get banned status from clue")
	}

	switch pantheonModerationEntry.State {
	case "MODERATE":
		if !isBanned {
			logrus.Info("user is incorrectly moderated in channel")
			return models.SHOULD_BE_UNMODERATED, nil
		} else {
			logrus.Info("user is correctly moderated in channel")
			return models.OK, nil
		}
	case "UNMODERATE":
		if isBanned {
			logrus.Info("user is incorrectly unmoderated in channel")
			return models.SHOULD_BE_MODERATED, nil
		} else {
			logrus.Info("user is correctly unmoderated in channel")
			return models.OK, nil
		}
	default:
		logrus.WithField("moderationStatus", pantheonModerationEntry.State).Info("found unknown moderation state")
		return models.ERROR, nil
	}
}
