package params

type Params struct {
	Environment string
	IsDryRun    bool
	Tenant      string
	Domain      string
	UserID      string
	ChannelID   string
	ClueProxy   string
}
