package setup

import (
	"errors"
	"fmt"
	"net/http"

	tmi "code.justin.tv/chat/tmi/client"
	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pantheon/backend/dynamo/moderation"
	"code.justin.tv/commerce/pantheon/cmd/moderation_tool/params"
	"code.justin.tv/commerce/pantheon/config"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	"code.justin.tv/foundation/twitchclient"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"golang.org/x/net/proxy"
)

type Clients struct {
	ModerationsDAO moderation.DAO
	ClueClient     tmi.Client
	PantheonClient pantheonrpc.Pantheon
}

func NewClients(cfg *config.Config, params params.Params) (*Clients, error) {
	sess, err := session.NewSession(&aws.Config{
		Region: aws.String("us-west-2"),
	})
	if err != nil {
		logrus.WithError(err).Error("failed to establish AWS session")
		return nil, err
	}

	var tenantConfig config.TenantConfig
	if params.Tenant != "" {
		var ok bool
		tenantConfig, ok = cfg.Tenants[params.Tenant]
		if !ok {
			logrus.WithField("tenant", params.Tenant).Error("could not find tenant in config")
			return nil, errors.New("could not find tenant in config")
		}
	} else {
		tenantConfig = cfg.DefaultTenant
	}

	moderationDao := moderation.NewDAO(sess, tenantConfig.ModerationsTableConfig.GetTableName())

	dialSocksProxy, err := proxy.SOCKS5("tcp", params.ClueProxy, nil, proxy.Direct)
	if err != nil {
		fmt.Println("Error connecting to proxy:", err)
	}
	tr := &http.Transport{Dial: dialSocksProxy.Dial}

	clueClient, err := tmi.NewClient(twitchclient.ClientConf{
		Host:          fmt.Sprintf("https://%s.clue.twitch.a2z.com", params.Environment),
		BaseTransport: tr,
	})
	if err != nil {
		logrus.WithError(err).Error("could not initiate TMI client")
		return nil, err
	}

	pantheonClient := pantheonrpc.NewPantheonProtobufClient(cfg.PantheonEndpoint, http.DefaultClient)

	return &Clients{
		ModerationsDAO: moderationDao,
		ClueClient:     clueClient,
		PantheonClient: pantheonClient,
	}, nil
}
