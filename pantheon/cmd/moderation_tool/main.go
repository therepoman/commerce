package main

import (
	"fmt"
	"os"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pantheon/cmd/moderation_tool/params"
	"code.justin.tv/commerce/pantheon/cmd/moderation_tool/setup"
	"code.justin.tv/commerce/pantheon/cmd/moderation_tool/tools"
	"code.justin.tv/commerce/pantheon/config"
	"github.com/codegangsta/cli"
)

var environment string
var isDryRun bool
var tenant string
var userID string
var channelID string
var domain string
var clueProxy string

func main() {
	app := cli.NewApp()
	app.Name = "Pantheon Moderation Tool"
	app.Usage = "Trues up moderation via the replica table for moderation entries, checks ban status in TMI, and corrects it if they are not matched."

	defaultArgs := []cli.Flag{
		cli.StringFlag{
			Name:        "environment, e",
			Value:       "staging",
			Usage:       "Runtime config to use. Options are: 'prod', 'staging'. Defaults to 'staging' if unset.",
			Destination: &environment,
		},
		cli.StringFlag{
			Name:        "tenant, t",
			Usage:       "provided tenant to use, if left blank will use the default tenant",
			Destination: &tenant,
		},
		cli.StringFlag{
			Name:        "clue-proxy",
			Value:       "localhost:8080",
			Usage:       "the proxy value that is able to make calls to TMI Clue. Defaults to 'localhost:8080'",
			Destination: &clueProxy,
		},
	}

	app.Commands = []cli.Command{
		{
			Name:  "single-user",
			Usage: "actions around validating a single user",
			Subcommands: []cli.Command{
				{
					Name:   "validate",
					Usage:  "Validate if a user is correctly moderated in a channel",
					Action: singleUserValidate,
					Flags: append(defaultArgs, []cli.Flag{
						cli.StringFlag{
							Name:        "domain, d",
							Usage:       "Domain to check ban status",
							Destination: &domain,
						},
						cli.StringFlag{
							Name:        "userID, u",
							Usage:       "User ID to check ban status",
							Destination: &userID,
						},
						cli.StringFlag{
							Name:        "channelID, c",
							Usage:       "Channel ID to check ban status",
							Destination: &channelID,
						},
					}...),
				},
				{
					Name:   "fix",
					Usage:  "Fixes the moderation status of a user in a channel",
					Action: singleUserFix,
					Flags: append(defaultArgs, []cli.Flag{
						cli.StringFlag{
							Name:        "domain, d",
							Usage:       "Domain to check ban status",
							Destination: &domain,
						},
						cli.StringFlag{
							Name:        "userID, u",
							Usage:       "User ID to check ban status",
							Destination: &userID,
						},
						cli.StringFlag{
							Name:        "channelID, c",
							Usage:       "Channel ID to check ban status",
							Destination: &channelID,
						},
						cli.BoolFlag{
							Name:        "is-dry-run",
							Usage:       "Determines whether to dry run script or not, not modifying any resources",
							EnvVar:      "IS_DRY_RUN",
							Required:    true,
							Destination: &isDryRun,
						},
					}...),
				},
			},
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		logrus.WithError(err).Fatal("script failed!")
	}
}

func initModerationTool(c *cli.Context) (*config.Config, *setup.Clients, params.Params, error) {
	p := params.Params{
		Environment: environment,
		IsDryRun:    isDryRun,
		Tenant:      tenant,
		Domain:      domain,
		UserID:      userID,
		ChannelID:   channelID,
		ClueProxy:   clueProxy,
	}

	cfg, err := config.LoadConfig(config.Environment(p.Environment))
	if err != nil {
		logrus.WithError(err).Error("Error loading config")
		return nil, nil, p, err
	}

	clients, err := setup.NewClients(cfg, p)
	if err != nil {
		return nil, nil, p, err
	}

	return cfg, clients, p, nil
}

func singleUser(c *cli.Context) (*tools.SingleUserCommands, params.Params, error) {
	cfg, clients, p, err := initModerationTool(c)
	if err != nil {
		return nil, p, err
	}

	return tools.NewSingleUserCommands(cfg, clients), p, nil
}

func singleUserValidate(c *cli.Context) error {
	singleUserCommands, p, err := singleUser(c)
	if err != nil {
		return err
	}

	return singleUserCommands.ValidateBanStatus(p.Domain, p.ChannelID, p.UserID)
}

func singleUserFix(c *cli.Context) error {
	logrus.Info(fmt.Sprintf("value of 'isDryRun': %t", isDryRun))

	singleUserCommands, p, err := singleUser(c)
	if err != nil {
		return err
	}

	return singleUserCommands.FixBanStatus(p.Domain, p.ChannelID, p.UserID, p.IsDryRun)
}
