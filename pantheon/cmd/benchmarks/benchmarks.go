package main

import (
	"log"
	"os"
	"time"

	"code.justin.tv/commerce/pantheon/backend/leaderboard"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/entry"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/local"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/score"
	"code.justin.tv/commerce/pantheon/utils/random"
	time_utils "code.justin.tv/commerce/pantheon/utils/time"
)

const (
	numRecordsToAdd = 1000
)

// TODO: Consider making an actual golang benchmarks https://dave.cheney.net/2013/06/30/how-to-write-benchmarks-in-go
func main() {
	log.SetOutput(os.Stdout)
	log.Printf("Benchmarking local leaderboard")
	benchmarkAdd(local.NewLeaderboard(), numRecordsToAdd)
}

func benchmarkAdd(lb leaderboard.Leaderboard, n int) {
	log.Printf("Adding %d records", n)
	start := time.Now()
	for i := 0; i < n; i++ {
		entryScore := score.ToEntryScore(score.BaseScore(random.Int64(1, 100000000)), score.EventTime(time.Now().UnixNano()), score.NotModerated)
		_ = lb.UpdateScore(entry.Key(random.String(16)), entryScore, nil)
	}
	end := time.Now()

	log.Printf("%d ms", time_utils.DurationToMilliseconds(end.Sub(start)))
}
