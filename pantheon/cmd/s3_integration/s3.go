package main

import (
	"context"
	"log"

	"code.justin.tv/commerce/pantheon/backend/leaderboard"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/local"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/persistence"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/test"
	"code.justin.tv/commerce/pantheon/clients/s3"
)

// TODO: Hook this into an automated integration test system
func main() {
	ctx := context.Background()

	s3Client, err := s3.NewDefaultClient()
	if err != nil {
		log.Panic("S3 instantiation failed")
	}
	s3Persister := persistence.S3Persister{
		S3Client: s3Client,
	}

	log.Print("Generating random leaderboard")
	id := test.RandomIdentifier()
	lb := local.NewLeaderboard()
	entries := test.RandomEntries(100)
	test.AddEntries(lb, entries...)

	log.Println("Exporting leaderboard to S3")
	err = s3Persister.Export(ctx, id, lb, leaderboard.RetrievalSettings{})
	if err != nil {
		log.Panic("S3 export failed")
	}

	log.Println("Importing leaderboard from S3")
	lb2 := local.NewLeaderboard()
	err = s3Persister.Import(ctx, id, lb2)
	if err != nil {
		log.Panic("S3 import failed")
	}

	if !test.Equal(lb, lb2) {
		log.Panic("S3 Imported LB does not match original")
	}

	log.Println("S3 leaderboard validated against original")
	log.Printf("%+v\n", id)
}
