package main

import (
	"context"
	"errors"
	"log"
	"net/http"
	"time"

	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
)

const (
	smokeTestTimeout        = 10 * time.Second
	healthCheckAttemptDelay = 500 * time.Millisecond
	localhostTwirpEndpoint  = "http://localhost:8000"
)

type SmokeTest struct {
	PantheonClient pantheonrpc.Pantheon
}

func main() {
	log.Println("Starting smoke test")
	pantheonClient := pantheonrpc.NewPantheonProtobufClient(localhostTwirpEndpoint, http.DefaultClient)
	smokeTest := &SmokeTest{
		PantheonClient: pantheonClient,
	}

	start := time.Now()
	for time.Since(start) < smokeTestTimeout {
		err := smokeTest.performHealthCheck()
		if err == nil {
			log.Println("Smoke test pinged health check successfully")
			return
		}

		log.Println(err)
		time.Sleep(healthCheckAttemptDelay)
	}
	log.Panicln("Smoke test failed all attempts to ping health check")
}

func (st *SmokeTest) performHealthCheck() error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	log.Println("Pinging healthcheck API...")

	resp, err := st.PantheonClient.HealthCheck(ctx, &pantheonrpc.HealthCheckReq{})
	if err != nil {
		return err
	}

	if resp == nil {
		return errors.New("Receive nil response from health check")
	}

	return nil
}
