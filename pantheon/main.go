package main

import (
	"context"
	"os"
	"strconv"
	"time"

	"code.justin.tv/commerce/dynamicconfig"
	"code.justin.tv/commerce/pantheon/backend"
	"code.justin.tv/commerce/pantheon/config"
	"code.justin.tv/commerce/pantheon/middleware"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	log "github.com/sirupsen/logrus"
	goji_graceful "github.com/zenazn/goji/graceful"
	goji "goji.io"
	"goji.io/pat"
)

const (
	defaultEnvironment    = "dev"
	workerShutdownTimeout = 1 * time.Minute
)

func getEnv() string {
	env := os.Getenv(config.EnvironmentEnvironmentVariable)
	if env != "" {
		log.Printf("Found ENVIRONMENT environment variable: %s", env)
	}

	args := os.Args
	if len(args) > 2 {
		log.Panic("Received too many CLI args")
	} else if len(args) == 2 {
		env = args[1]
		log.Infof("Using environment from CLI arg: %s", env)
	}

	if env == "" {
		log.Infof("No environment supplied.")
		log.Infof("Falling back to default environment: %s", defaultEnvironment)
		env = defaultEnvironment
	} else if !config.IsValidEnvironment(config.Environment(env)) {
		log.Errorf("Invalid environment supplied: %s", env)
		log.Infof("Falling back to default environment: %s", defaultEnvironment)
		env = defaultEnvironment
	}

	return env
}

func getSubstage() config.Substage {
	canaryEnvVar := os.Getenv(config.CanaryEnvironmentVariable)
	if canaryEnvVar == "" {
		log.Print("Found no CANARY environment variable, falling back to primary")
		return config.Primary
	}

	isCanary, err := strconv.ParseBool(canaryEnvVar)
	if err != nil {
		log.Printf("Found CANARY environment variable %s but it could not be parsed as a bool, falling back to primary", canaryEnvVar)
		return config.Primary
	}

	log.Printf("Found valid CANARY environment variable %s", canaryEnvVar)
	if isCanary {
		return config.Canary
	}
	return config.Primary
}

func main() {
	env := getEnv()
	subStage := getSubstage()

	cfg, err := config.LoadConfigWithSubstage(config.Environment(env), subStage)
	if err != nil {
		log.WithError(err).Panic("Error loading config")
		panic(err)
	}

	log.Infof("Starting up environment: %s", env)

	be, err := backend.NewBackend(cfg)
	if err != nil {
		log.Panic("Error initializing backend", err)
	}

	poller := dynamicconfig.Poller{
		Reloader: be.DynamicConfig,
		Interval: 30 * time.Second,
		Timeout:  30 * time.Second,
	}
	go poller.Poll()
	defer poller.Stop()

	twirpHandler := pantheonrpc.NewPantheonServer(be.Server, nil)

	mux := goji.NewMux()
	mux.Use(be.MetricsLogger)
	mux.Use(middleware.WithRequestInfo)
	mux.HandleFunc(pat.Get("/"), be.Ping)
	mux.HandleFunc(pat.Get("/ping"), be.Ping)
	mux.Handle(pat.Post(pantheonrpc.PantheonPathPrefix+"*"), twirpHandler)

	// Initiate graceful shutdown process in response to SIGINT
	goji_graceful.HandleSignals()

	log.Infof("Pantheon running on port :8000")
	err = goji_graceful.ListenAndServe(":8000", mux)
	if err != nil {
		log.WithError(err).Error("Mux listen and serve error")
	}

	log.Info("Initiated shutdown process")

	shutdownStart := time.Now()
	shutdownContext, shutdownContextCancel := context.WithTimeout(context.Background(), workerShutdownTimeout)
	defer shutdownContextCancel()

	shutdownErrGroup := be.Shutdown(shutdownContext)

	// Wait on all async processors to finish shutting down
	if err = shutdownErrGroup.Wait(); err != nil {
		log.WithError(err).Error("Error attempting to shutdown")
	}

	goji_graceful.Wait()
	log.Infof("Finished shutting down in: %v", time.Since(shutdownStart))
}
