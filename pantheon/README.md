![A little copying is better than a little dependency.](https://s3-us-west-2.amazonaws.com/payday-image-magick/proverb.jpg)

# pantheon

Twitch Leaderboard Service

[![Build Status](https://jenkins.internal.justin.tv/buildStatus/icon?job=commerce-pantheon)](https://jenkins.internal.justin.tv/job/commerce-pantheon/)
[![Go Report Card](https://goreportcard.internal.justin.tv/badge/code.justin.tv/commerce/pantheon)](https://goreportcard.internal.justin.tv/report/code.justin.tv/commerce/pantheon)

## Endpoints

| Environment | Endpoint |
| --- | --- |
| Dev | http://pantheon-dev.internal.justin.tv/ |
| Staging | https://main.us-west-2.beta.pantheon.twitch.a2z.com/ |
| Prod | https://main.us-west-2.prod.pantheon.twitch.a2z.com/ |

## Teleport Bastion

Pantheon has two teleport bastion clusters configured with jumpbox support.

| Environment | Cluster Name | SSH Command |
| --- | --- | --- |
| Staging | teleport-remote-twitch-pantheon-devo | `TC=teleport-remote-twitch-pantheon-devo ssh jumpbox` |
| Prod | teleport-remote-twitch-pantheon-prod | `TC=teleport-remote-twitch-pantheon-prod ssh jumpbox` |

## Disaster Recovery

In case of emergency, there are three options:

- [Recovering from redis and dynamo](docs/recovery/recovering_from_redis_and_dynamo.md) (Quickest Option)
- [Recovering from dynamo](docs/recovery/recovering_from_dynamo.md)
- [Recovering from external source](docs/recovery/recovering_from_external_source.md) (Most Correct Option)

For more guidance about disaster recovery see [this recovery guidance document](docs/recovery/guidance.md).

## Event Pusblishing SNS Topics

| Environment | Topic ARN |
| --- | --- |
| Dev | arn:aws:sns:us-west-2:192960249206:published-events-dev |
| Staging | arn:aws:sns:us-west-2:192960249206:published-events-staging |
| Prod | TODO |

## Twirp

[Twirp](https://git.xarth.tv/common/twirp) is a Twitch internal RPC system for service to service communication.

To add a new API, define the requests and responses in the `pantheonrpc.proto` file and then run:

```sh
make generate_twirp
```

## Dependency Management

Run make dep to pull in new dependencies. To require dependencies use a specific version range modify the Gopkg.toml
file.

## Retool

[Retool](https://github.com/twitchtv/retool) manages versioning of the tools used by this repo. Individual tools and
their versions are stored in the tools.json file

To install retool:

```sh
go get github.com/twitchtv/retool
```

Sync your local environment with tools.json:

```sh
retool sync
```

To add a new tool:

```sh
retool add [repo] [commit]
```

## Mockery

[Mockery](https://github.com/vektra/mockery) is used to generate mocks for golang interfaces.

To generate a mock run this script from the pantheon root:

```sh
./scripts/generate-mock.sh $path_to_mock $interface_to_mock $package_name
```

For example, to generate an SQS mock you would run:

```sh
./scripts/generate-mock.sh github.com/aws/aws-sdk-go/service/sqs/sqsiface SQSAPI sqsiface
```

## AWS

### Accounts

The pantheon AWS accounts are maintained in [isengard](https://isengard.amazon.com). To access them you must be on the
Amazon corporate network (WPA2). Once you login to an account's admin console from isengard you may change networks. An
admin console session will remain valid for 12 hours.

| Environment | Account | Admin Console Login |
| --- | --- | --- |
| Dev / Staging | [twitch-pantheon-aws-devo@amazon.com](https://isengard.amazon.com/account/192960249206) | [Admin](https://isengard.amazon.com/federate?account=192960249206&role=Admin) |
| Prod | [twitch-pantheon-aws-prod@amazon.com](https://isengard.amazon.com/account/375121467453) | [Admin](https://isengard.amazon.com/federate?account=375121467453&role=Admin) |

### Credentials

To run pantheon locally and/or perform terraform updates, you will need an IAM user with credentials for the appropriate
AWS account. You can follow
the [same sorts of instructions as are in Mako](https://git.xarth.tv/commerce/mako#set-up-devostaging-aws-credentials)
or ask one of the owners of Pantheon for help if you're unfamilliar.

### Terraform

As of 9/8/2021, Pantheon uses Terraform version 0.14.7

Terraform is used to configure and manage all AWS infrastucture related to pantheon. To add new AWS resources, define
them in the `terraform/modules/pantheon` folder.

The `.tfstate` files are not checked into source, instead `terraform` maintains them within S3.

To apply an updated terraform config run the following commands:

```sh
# cd into the stage you want to modify
cd terraform/development

# Create the plan file
terraform plan -out newconfig.plan

# Apply the plan file
terraform apply newconfig.plan
```

### Docker

Once you're sshed into a pantheon EC2 host

Change to root (since docker runs as root):

```sh
sudo su
```

List out running docker containers:

```sh
docker ps
```

Open a shell with a given container:

```sh
docker exec -it [container-id] bash
```

## Deployment

Follow the instruction below to deploy your change:

1. Make a branch for your change.
2. Push the code and share the link to your PR in #commerce-crs slack channel. Ping someone to have them review it.
3. Once your change is approved, merge it to master branch.
4. In Twitch network, go to [clean deploy tool](https://clean-deploy.internal.justin.tv/#/commerce/pantheon)
5. Beside the 'master' candidate, there is a dropdown. Choose what environment you want to deploy your change to, fill
   out the reason, and hit "Ship it"
