## Pantheon Disaster Recovery Guidance

### Overview
Dynamo, Redis, and S3 are the primary data stores for pantheon.
Dynamo stores every invent ingested (for auditing and deduping purposes)
Redis maintains all live leaderboards using sorted set data structures.
S3 holds archived redis leaderboards once they expire.

### Automated Backups
- By default tenants have several types of automated backup enabled.
- Backup snapshots are automatically created for redis at set intervals (day / week / month). Older snapshots are deleted to save costs.
- Backup snapshots are automatically created for all dynamo tables at set intervals (day / week / month). Older snapshots are deleted to save costs.
- All dynamo tables are global tables and also have point-in-time recovery enabled.
- The S3 bucket that maintains archived leaderboards ia automatically replicated into another S3 bucket in a different region. 

### General Recovery Strategies
- [Recovering from redis and dynamo](docs/recovery/recovering_from_redis_and_dynamo.md) (Quickest Option)
- [Recovering from dynamo](docs/recovery/recovering_from_dynamo.md)
- [Recovering from external source](docs/recovery/recovering_from_external_source.md) (Most Correct Option)

### Potential Disasters and Recovery Strategies
#### All archived leaderboards in the primary S3 bucket are lost
Recover leaderboards from the cross-region replicated S3 bucket.
Copying the entire contents of the replicated bucket into the original bucket is sufficient.

#### All archived leaderboards in both the primary AND  the backup S3 buckets are lost
All transactions since the beginning will need to be replayed.
Either [recover from pantheon's dynamo](recovering_from_dynamo.md) or [from an external source](recovering_from_external_source.md).
  
#### Dynamo table data is lost
Restore from a dynamo backup or the global replicated table. Then backfill recent transactions since the snapshot from [from an external source](recovering_from_external_source.md).

#### Dynamo table and all backups are lost
Backfill all transactions from [from an external source](recovering_from_external_source.md).

#### Redis data is lost 
Restore from the [lastest redis backup and then dynamo](recovering_from_redis_and_dynamo.md) for the remaining transactions.

#### Redis data and backups are lost 
Backfill all transactions from [from an external source](recovering_from_external_source.md).
