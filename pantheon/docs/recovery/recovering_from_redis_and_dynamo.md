## Recovering from Redis and Dynamo
1. Create a new tenant (ex: polls-v2). Run terraform to create the new redis and dynamo tables.
Deploy a code change configuring the new tenant domain.
2. Start ingesting live events to the new tenant resources.
This can be done either by modifying ingestion clients or adding a rewrite rule in pantheon.
3. Restore the latest redis backup from the old redis into the new redis. Take note of the timestamp on the old redis backup. 
4. Run a script to backfill new dynamo transactions using the PublishEvent API. 
The backfill script should only backfill transactions newer than the timestamp of the redis backup.

## Potential Concerns
- Depending on how exact redis is in generating its snapshot at a single point in time, there could be transactions
double counted after backfilling the dynamo transaction records. This would lead to some leaderboard totals being slightly
higher than they should be.
- Older transactions from before the time of the redis backup will not have a corresponding record in the new dynamo tables.
That means if they are ever replayed (or the ID is reused) they will be double counted in leaderboards.