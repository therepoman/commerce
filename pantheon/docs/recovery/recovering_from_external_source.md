## Recovering from an External Data Source
1. Create a new tenant (ex: polls-v2). Run terraform to create the new redis and dynamo tables.
Deploy a code change configuring the new tenant domain.
2. Start ingesting live events to the new tenant resources.
This can be done either by modifying ingestion clients or adding a rewrite rule in pantheon.
3. Query your external data source (ex: redshift db) to gather all the necessary events to backfill.
4. Run a backfill script such that the PublishEvent API is called for each event you queried from your external data source.
