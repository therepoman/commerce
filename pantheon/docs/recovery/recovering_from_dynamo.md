## Recovering from Dynamo
1. Create a new tenant (ex: polls-v2). Run terraform to create the new redis and dynamo tables.
Deploy a code change configuring the new tenant domain.
2. Start ingesting live events to the new tenant resources.
This can be done either by modifying ingestion clients or adding a rewrite rule in pantheon.
3. Run a script to backfill all dynamo transactions from the old table.
Each distinct event from the dynamo table should be backfilled by calling the PublishEvent API.

## Potential Concerns
- The old original bits tenant dynamo tables do not have full data for sufficiently old records.
A change was made sometime after bits leaderboards launch to start saving full event data.
To get complete bits data we will need to join external data sources.
- The dynamo events table stores the same event id multiple times: one copy for each time unit (daily/weekly/etc).
We only need to call PublishEvent once and it will naturally feed into all the leaderboards for all the time units.
Ideally, the backfill script should dedupe appropriately in order to minimize the duration of the backfill.