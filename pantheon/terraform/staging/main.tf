module "pantheon" {
  source = "../modules/pantheon"
  account_name = "twitch-pantheon-aws-devo"
  account_number = "192960249206"
  region = "us-west-2"
  owner = "twitch-pantheon-aws-devo@amazon.com"
  aws_profile = "twitch-pantheon-aws-devo"
  env = "staging"
  vpc_id = "vpc-11635a76"
  private_subnets = "subnet-ca2c4083,subnet-c4f6bca3,subnet-aec438f5"
  sg_id = "sg-2060fc5b"
  elasticache_nodes = 6
  events_dynamo_min_read_capacity = 50
  events_dynamo_max_read_capacity = 1000
  events_dynamo_min_write_capacity = 100
  events_dynamo_max_write_capacity = 1000
  private_cidr_blocks = [
    "10.201.180.0/24",
    "10.201.181.0/24",
    "10.201.182.0/24"]
  subscriptions-aws-account-id = "958836777662"
  percy-aws-account-id = "999515204624"
  user_moderation_sns_arn = "arn:aws:sns:us-west-2:603200399373:clue_staging_channel_ban_events"
  sandstorm_arn = "arn:aws:iam::734326455073:role/sandstorm/production/templated/role/commerce-staging-pantheon"

  tenant_elasticache_instance_type = "cache.m3.medium"
  tenant_elasticache_nodes = 3

  ecs_instance_type = "c5n.4xlarge"
  ecs_ec2_backed_min_instances = 3
  min_tasks = 3
  max_tasks = 10

  privatelink_whitelist = [
    // payday
    "arn:aws:iam::021561903526:root",

    // Percy
    "arn:aws:iam::999515204624:root",

    // Poliwag
    "arn:aws:iam::720603942490:root",

    // Subs
    "arn:aws:iam::958836777662:root",

    // Helix
    "arn:aws:iam::327140220177:root",

    // Visage
    "arn:aws:iam::628671981940:root",

    // CoPo
    "arn:aws:iam::962912953676:root",
  ]
}

terraform {
  backend "s3" {
    bucket = "pantheon-devo-tcs"
    key = "tfstate/commerce/pantheon/terraform/staging"
    region = "us-west-2"
    profile = "twitch-pantheon-aws-devo"
  }
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "2.57.0"
      # Pinning to specific version as per https://jira.twitch.com/browse/MOMENTS-1166
    }
  }
  required_version = "0.14.7"
}

provider "aws" {
  region = "us-west-2"
  profile = "twitch-pantheon-aws-devo"
}

module "network-admin-role" {
  source = "git::git+ssh://git@git.xarth.tv/terraform-modules/network-admin-role?ref=c75995dc55ee01b03741536dbe79b3a29f23becf"
}

module "twitch-inventory" {
  source = "git::git+ssh://git@git.xarth.tv/terraform-modules/twitch-inventory?ref=e3ba274bf73f68e064a648470006ff982d515fb5"
  account = "twitch-pantheon-aws-devo"
}
