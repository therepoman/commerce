# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/aws" {
  version     = "2.57.0"
  constraints = "2.57.0"
  hashes = [
    "h1:3qgcFVSPvM5zz08Fif8M23BkK8xhqOed6Rzhkn9gROw=",
    "zh:1dd336cd0e097c85aee26722bbe90a8e4ab416cdf16e61aa912ffe9c73abab56",
    "zh:23dd31668c479ba41faf2a9429bab808347dfc23c2ae3b66dc339ccc4611c880",
    "zh:275595922229434e83b8c54f615c294be7378b491d7950bfd5da17156edf444d",
    "zh:2bd9cfa8db879fd7df18f62723144e03e74245a1380bc666cab13f9ec1cf075f",
    "zh:321b185dbe8ea8b2720b8d41900f578fdad58c9ea6686e01133e811dd4092676",
    "zh:402aa981e47f01b64849478be1bce85d9e6738fc8941b7bb0bee9b59d0002e66",
    "zh:84aa842441071a9024eb431ecebc0d2e89fdc305055137609597b038966cf9f4",
    "zh:8a4721e167a6a5409b17aa7dabc050abcd152a213aeec653a769698cdb56f998",
    "zh:c58da1769a6b4e01764ad1e3a9357a3f42b56a2fa95c66899270e31836da7529",
    "zh:d33e3e58835cd7ed313a9e972a7a8c9a628b54eee20d053daf170f35ba4dcf10",
    "zh:de205431a74c5997315d8602bc5e0920dd0aa6be94efc1db5ba609ef4bcf739f",
    "zh:fbb2e93915593a7456bb62de9f4737a807ef3ab25327c205d2997ac83bb9bfc6",
  ]
}

provider "registry.terraform.io/hashicorp/template" {
  version     = "2.2.0"
  constraints = "2.2.0"
  hashes = [
    "h1:0wlehNaxBX7GJQnPfQwTNvvAf38Jm0Nv7ssKGMaG6Og=",
    "zh:01702196f0a0492ec07917db7aaa595843d8f171dc195f4c988d2ffca2a06386",
    "zh:09aae3da826ba3d7df69efeb25d146a1de0d03e951d35019a0f80e4f58c89b53",
    "zh:09ba83c0625b6fe0a954da6fbd0c355ac0b7f07f86c91a2a97849140fea49603",
    "zh:0e3a6c8e16f17f19010accd0844187d524580d9fdb0731f675ffcf4afba03d16",
    "zh:45f2c594b6f2f34ea663704cc72048b212fe7d16fb4cfd959365fa997228a776",
    "zh:77ea3e5a0446784d77114b5e851c970a3dde1e08fa6de38210b8385d7605d451",
    "zh:8a154388f3708e3df5a69122a23bdfaf760a523788a5081976b3d5616f7d30ae",
    "zh:992843002f2db5a11e626b3fc23dc0c87ad3729b3b3cff08e32ffb3df97edbde",
    "zh:ad906f4cebd3ec5e43d5cd6dc8f4c5c9cc3b33d2243c89c5fc18f97f7277b51d",
    "zh:c979425ddb256511137ecd093e23283234da0154b7fa8b21c2687182d9aea8b2",
  ]
}
