module "pantheon" {
  source = "../modules/pantheon"
  account_name = "twitch-pantheon-aws-prod"
  account_number = "375121467453"
  owner = "twitch-pantheon-aws-prod@amazon.com"
  aws_profile = "twitch-pantheon-aws-prod"
  env = "prod"
  vpc_id = "vpc-50635a37"
  private_subnets = "subnet-20cb377b,subnet-e5154082,subnet-892d41c0"
  sg_id = "sg-8d1985f6"
  elasticache_instance_type = "cache.r3.4xlarge"
  elasticache_nodes = 6
  private_cidr_blocks = [
    "10.201.186.0/24",
    "10.201.185.0/24",
    "10.201.184.0/24"]
  events_dynamo_min_read_capacity = 400
  events_dynamo_max_read_capacity = 10000
  events_dynamo_min_write_capacity = 400
  events_dynamo_max_write_capacity = 10000
  moderation_dynamo_min_write_capacity = 1000
  subscriptions-aws-account-id = "522398581884"
  percy-aws-account-id = "958761666246"
  user_moderation_sns_arn = "arn:aws:sns:us-west-2:603200399373:clue_production_channel_ban_events"
  sandstorm_arn = "arn:aws:iam::734326455073:role/sandstorm/production/templated/role/commerce-prod-pantheon"

  tenant_elasticache_instance_type = "cache.r4.2xlarge"
  tenant_elasticache_nodes = 3

  ecs_instance_type = "c5n.4xlarge"
  ecs_ec2_backed_min_instances = 15
  min_tasks = 30
  max_tasks = 60
  ecs_memory = 4096
  ecs_canary_memory = 4096
  ecs_cpu = 4096
  ecs_canary_cpu = 4096

  privatelink_whitelist = [
    // payday
    "arn:aws:iam::021561903526:root",

    // Percy
    "arn:aws:iam::958761666246:root",

    // Poliwag
    "arn:aws:iam::246232734983:root",

    // Subs
    "arn:aws:iam::522398581884:root",

    // Helix
    "arn:aws:iam::028439334451:root",

    // Visage
    "arn:aws:iam::736332675735:root",

    // Copo
    "arn:aws:iam::471167244615:root",
  ]
}

terraform {
  backend "s3" {
    bucket = "pantheon-prod-tcs"
    key = "tfstate/commerce/pantheon/terraform/production"
    region = "us-west-2"
    profile = "twitch-pantheon-aws-prod"
  }
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "2.57.0"
      # Pinning to specific version as per https://jira.twitch.com/browse/MOMENTS-1166
    }
  }
  required_version = "0.14.7"
}

provider "aws" {
  region = "us-west-2"
  profile = "twitch-pantheon-aws-prod"
}

module "network-admin-role" {
  source = "git::git+ssh://git@git.xarth.tv/terraform-modules/network-admin-role?ref=c75995dc55ee01b03741536dbe79b3a29f23becf"
}

module "twitch-inventory" {
  source = "git::git+ssh://git@git.xarth.tv/terraform-modules/twitch-inventory?ref=e3ba274bf73f68e064a648470006ff982d515fb5"
  account = "twitch-pantheon-aws-prod"
}
