variable "dlq_name" {
  type = string
}

resource "aws_cloudwatch_metric_alarm" "queue_dlq_alarm" {
  alarm_name          = var.dlq_name
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "ApproximateNumberOfMessagesVisible"
  namespace           = "AWS/SQS"

  dimensions = {
    QueueName = var.dlq_name
  }

  period             = "300"
  statistic          = "Average"
  threshold          = "1"
  alarm_description  = "This metric monitors the pantheon's queue dlq. It will alarm if there is a message in that queue"
  treat_missing_data = "notBreaching"
}
