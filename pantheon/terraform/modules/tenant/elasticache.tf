resource "aws_elasticache_replication_group" "tenant_redis" {
  node_type                     = var.elasticache_instance_type
  port                          = var.redis_port
  engine                        = "redis"
  engine_version                = "5.0.5"
  replication_group_id          = "${var.tenant_name}-${var.env}"
  replication_group_description = "${var.tenant_name} redis"
  automatic_failover_enabled    = "true"
  availability_zones            = ["us-west-2c", "us-west-2a", "us-west-2b"]
  number_cache_clusters         = var.elasticache_nodes
  parameter_group_name          = aws_elasticache_parameter_group.tenant_redis_params.name
  subnet_group_name             = aws_elasticache_subnet_group.tenant_redis_subnet_group.name
  security_group_ids            = [aws_security_group.tenant_redis_security_group.id]
}

resource "aws_elasticache_replication_group" "pdms_tenant_redis" {
  node_type                     = var.elasticache_instance_type
  port                          = var.redis_port
  engine                        = "redis"
  engine_version                = "5.0.5"
  replication_group_id          = "${var.tenant_name}-${var.env}-pdms"
  replication_group_description = "${var.tenant_name} redis for loading leaderboards for PDMS deletions"
  automatic_failover_enabled    = "true"
  availability_zones            = ["us-west-2c", "us-west-2a", "us-west-2b"]
  number_cache_clusters         = var.elasticache_nodes
  parameter_group_name          = aws_elasticache_parameter_group.tenant_redis_params.name
  subnet_group_name             = aws_elasticache_subnet_group.tenant_redis_subnet_group.name
  security_group_ids            = [aws_security_group.tenant_redis_security_group.id]
}

resource "aws_elasticache_parameter_group" "tenant_redis_params" {
  family = "redis5.0"
  name   = "${var.tenant_name}-pantheon-params-${var.env}"

  parameter {
    name  = "maxmemory-policy"
    value = "allkeys-lru"
  }

  parameter {
    name  = "notify-keyspace-events"
    value = "Ex"
  }
}

resource "aws_elasticache_subnet_group" "tenant_redis_subnet_group" {
  name       = "${var.tenant_name}-${var.env}-cache"
  subnet_ids = split(",", var.private_subnets)
}

resource "aws_security_group" "tenant_redis_security_group" {
  name        = "${var.tenant_name}-${var.env}-redis"
  vpc_id      = var.vpc_id
  description = "Allows communication with redis server"

  ingress {
    from_port   = var.redis_port
    protocol    = "tcp"
    to_port     = var.redis_port
    cidr_blocks = var.private_cidr_blocks
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "${var.tenant_name}-${var.env}-redis"
  }
}

module "redis_backups" {
  source                  = "../scheduled_redis_backup_event"
  redis_backup_lambda_arn = var.redis_backup_lambda_arn
  cluster_name            = "${var.tenant_name}-${var.env}"
}
