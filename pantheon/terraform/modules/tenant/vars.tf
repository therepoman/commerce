variable "tenant_name" {
  description = "Name of this specific tenant"
}

variable "private_cidr_blocks" {
  description = "The private subnet cidr blocks for this stage"
  type        = list(string)
}

variable "vpc_id" {
  description = "Id of the systems-created VPC"
}

variable "env" {
  description = "Whether it's production, dev, etc"
}

variable "redis_port" {
  description = "The port used by the redis server"
  default     = 6379
}

variable "private_subnets" {
  description = "Comma-separated list of private subnets"
}

variable "elasticache_instance_type" {
  description = "Describes the type of instance to be used in our elasticache cluster"
  default     = "cache.m3.medium"
}

variable "account_number" {
  description = "AWS account number"
}

variable "use_on_demand_dyanmo" {
  description = "Use the on-demand (pay-per-request) pricing mode for DynamoDB tables"
  default     = false
}

variable "dynamo_min_read_capacity" {
  description = "Read capacity for the dynamo tables"
  default     = "50"
}

variable "dynamo_min_write_capacity" {
  description = "Write capacity for the dynamo table"
  default     = "5"
}

variable "dynamo_max_read_capacity" {
  description = "Read capacity for the dynamo table"
  default     = "10000"
}

variable "dynamo_max_write_capacity" {
  description = "Write capacity for the dynamo table"
  default     = "10000"
}

variable "dynamo_backup_lambda_arn" {
  description = "ARN for the dynamo backup lambda"
}

variable "redis_backup_lambda_arn" {
  description = "ARN for the redis backup lambda"
}

variable "elasticache_nodes" {
  description = "The number of nodes in elasticache"
  default     = 3
}

variable "events_dynamo_write_throttle_alarm_threshold" {
  default = "0"
  description = "Threshold for alarming on tenant events dynamo write throttles"
}

variable "events_dynamo_read_throttle_alarm_threshold" {
  default = "0"
  description = "Threshold for alarming on tenant events dynamo read throttles"
}

variable "ecs_service_role_arns" {
  type = list(string)
}

variable "pdms_delete_entries_sfn_role_name" {}

variable "pdms_delete_leaderboards_sfn_role_name" {}

variable "pdms_start_delete_entries_lambda_name" {}

variable "pdms_start_delete_leaderboards_lambda_name" {}

variable "pdms_start_delete_entries_lambda_arn" {}

variable "pdms_start_delete_leaderboards_lambda_arn" {}

variable "pdms_start_delete_entries_max_messages" {
  default = 100
  description = "Number of messages to be processed by start delete entries lambda"
}

variable "pdms_start_delete_leaderboards_max_messages" {
  default = 100
  description = "Number of messages to be processed by start delete leaderboards lambda"
}

variable "pager_duty_sns_arn" {
  type    = string
  default = ""
  description = "ARN of the SNS topic set up for Pager Duty alarming"
}
