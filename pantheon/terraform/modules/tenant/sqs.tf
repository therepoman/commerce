resource "aws_sqs_queue" "tenant_published_events_new_processing_queue_dlq" {
  name                      = "${var.tenant_name}-published-events-new-processing-queue-dlq-${var.env}"
  message_retention_seconds = 1209600 // 14 days
}

resource "aws_sqs_queue" "tenant_published_events_new_processing_queue" {
  name           = "${var.tenant_name}-published-events-new-processing-queue-${var.env}"
  redrive_policy = "{\"deadLetterTargetArn\":\"${aws_sqs_queue.tenant_published_events_new_processing_queue_dlq.arn}\",\"maxReceiveCount\":4}"
}

resource "aws_sqs_queue" "tenant_archival_processing_queue_dlq" {
  name       = "${var.tenant_name}-archival-processing-queue-dlq-${var.env}"
  fifo_queue = false
}

resource "aws_sqs_queue" "tenant_archival_processing_queue" {
  name           = "${var.tenant_name}-archival-processing-queue-${var.env}"
  redrive_policy = "{\"deadLetterTargetArn\":\"${aws_sqs_queue.tenant_archival_processing_queue_dlq.arn}\",\"maxReceiveCount\":4}"
}

resource "aws_sqs_queue" "tenant_moderation_processing_non_fifo_queue_dlq" {
  name = "${var.tenant_name}-moderation-processing-non-fifo-queue-dlq-${var.env}"
}

resource "aws_sqs_queue" "tenant_moderation_processing_non_fifo_queue" {
  name           = "${var.tenant_name}-moderation-processing-non-fifo-queue-${var.env}"
  redrive_policy = "{\"deadLetterTargetArn\":\"${aws_sqs_queue.tenant_moderation_processing_non_fifo_queue_dlq.arn}\",\"maxReceiveCount\":4}"
}

module "pdms_delete_leaderboards_sqs" {
  source                     = "../privacy_sqs"
  queue_name                 = "${var.tenant_name}-${var.env}-DeleteLeaderboards"
  alarm_dlq                  = true
  message_sender_arns        = var.ecs_service_role_arns
  message_receiver_role_name = var.pdms_delete_leaderboards_sfn_role_name
}

module "pdms_delete_entries_sqs" {
  source                     = "../privacy_sqs"
  queue_name                 = "${var.tenant_name}-${var.env}-DeleteEntries"
  alarm_dlq                  = true
  message_sender_arns        = var.ecs_service_role_arns
  message_receiver_role_name = var.pdms_delete_entries_sfn_role_name
}
