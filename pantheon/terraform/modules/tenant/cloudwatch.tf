resource "aws_cloudwatch_event_rule" "start_delete_leaderboards" {
  name        = "cron_${var.pdms_start_delete_leaderboards_lambda_name}-${var.tenant_name}"
  description = "Starts the ${var.pdms_start_delete_leaderboards_lambda_name} lambda to kick off delete step fns for ${var.tenant_name} tenant"

  schedule_expression = "rate(1 hour)"
}

resource "aws_cloudwatch_event_target" "start_delete_leaderboards" {
  rule      = aws_cloudwatch_event_rule.start_delete_leaderboards.name
  arn       = var.pdms_start_delete_leaderboards_lambda_arn

  input = <<DOC
{
  "tenant": "${var.tenant_name}",
  "max_messages": ${var.pdms_start_delete_leaderboards_max_messages}
}
DOC
}

resource "aws_lambda_permission" "start_delete_leaderboards_lambda_allow_cloudwatch" {
  statement_id  = "AllowExecutionFromCloudWatch-${var.pdms_start_delete_leaderboards_lambda_name}-${var.tenant_name}"
  action        = "lambda:InvokeFunction"
  function_name = var.pdms_start_delete_leaderboards_lambda_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.start_delete_leaderboards.arn
}

resource "aws_cloudwatch_event_rule" "start_delete_entries" {
  name        = "cron_${var.pdms_start_delete_entries_lambda_name}-${var.tenant_name}"
  description = "Starts the ${var.pdms_start_delete_entries_lambda_name} lambda to kick off delete step fns for ${var.tenant_name} tenant"

  schedule_expression = "rate(1 hour)"
}

resource "aws_cloudwatch_event_target" "start_delete_entries" {
  rule      = aws_cloudwatch_event_rule.start_delete_entries.name
  arn       = var.pdms_start_delete_entries_lambda_arn

  input = <<DOC
{
  "tenant": "${var.tenant_name}",
  "max_messages": ${var.pdms_start_delete_entries_max_messages}
}
DOC
}

resource "aws_lambda_permission" "start_delete_entries_lambda_allow_cloudwatch" {
  statement_id  = "AllowExecutionFromCloudWatch-${var.pdms_start_delete_entries_lambda_name}-${var.tenant_name}"
  action        = "lambda:InvokeFunction"
  function_name = var.pdms_start_delete_entries_lambda_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.start_delete_entries.arn
}

module "event_queue_alarms" {
  source   = "../sqs_alarm"
  dlq_name = aws_sqs_queue.tenant_published_events_new_processing_queue_dlq.name
}

module "moderation_queue_alarms" {
  source   = "../sqs_alarm"
  dlq_name = aws_sqs_queue.tenant_moderation_processing_non_fifo_queue_dlq.name
}

module "moderation_queue_age_alarms" {
  source   = "../sqs_age_alarm"
  queue_name = aws_sqs_queue.tenant_moderation_processing_non_fifo_queue.name
}

module "archival_queue_alarms" {
  source   = "../sqs_alarm"
  dlq_name = aws_sqs_queue.tenant_archival_processing_queue_dlq.name
}
