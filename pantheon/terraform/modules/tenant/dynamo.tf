resource "aws_dynamodb_table" "tenant-events-table" {
  name             = "${var.tenant_name}-events-${var.env}"
  read_capacity    = var.dynamo_min_read_capacity
  write_capacity   = var.dynamo_min_write_capacity
  hash_key         = "leaderboard_id"
  range_key        = "event_id"
  stream_enabled   = "true"
  stream_view_type = "NEW_AND_OLD_IMAGES"
  billing_mode     = var.use_on_demand_dyanmo ? "PAY_PER_REQUEST" : "PROVISIONED"

  point_in_time_recovery {
    enabled = "true"
  }

  attribute {
    name = "leaderboard_id"
    type = "S"
  }

  attribute {
    name = "event_id"
    type = "S"
  }

  attribute {
    name = "entry_key"
    type = "S"
  }

  attribute {
    name = "timestamp"
    type = "N"
  }

  lifecycle {
    ignore_changes = [read_capacity, write_capacity, global_secondary_index]
  }

  global_secondary_index {
    name            = "${var.tenant_name}-events-${var.env}_entry_key-index"
    hash_key        = "entry_key"
    range_key       = "timestamp"
    projection_type = "ALL"
    read_capacity   = var.use_on_demand_dyanmo ? "0" : var.dynamo_min_read_capacity
    write_capacity  = var.use_on_demand_dyanmo ? "0" : var.dynamo_min_write_capacity
  }
}

module "tenant_events_autoscaling_policy" {
  source             = "../dynamo_autoscaling"
  enabled            = !var.use_on_demand_dyanmo
  table_name         = "${var.tenant_name}-events-${var.env}"
  min_read_capacity  = var.dynamo_min_read_capacity
  min_write_capacity = var.dynamo_min_write_capacity
  max_read_capacity  = var.dynamo_max_read_capacity
  max_write_capacity = var.dynamo_max_write_capacity
  autoscaling_role   = "arn:aws:iam::${var.account_number}:role/service-role/DynamoDBAutoscaleRole"
  pager_duty_sns_arn = var.pager_duty_sns_arn
  events_write_throttle_alarm_threshold = var.events_dynamo_write_throttle_alarm_threshold
  events_read_throttle_alarm_threshold = var.events_dynamo_read_throttle_alarm_threshold
}

module "tenant_events_entry_key-index_autoscaling_policy" {
  source                   = "../dynamo_index_autoscaling"
  enabled                  = !var.use_on_demand_dyanmo
  table_name               = "${var.tenant_name}-events-${var.env}"
  index_name               = "${var.tenant_name}-events-${var.env}_entry_key-index"
  min_index_read_capacity  = var.dynamo_min_read_capacity
  min_index_write_capacity = var.dynamo_min_write_capacity
  max_index_read_capacity  = var.dynamo_max_read_capacity
  max_index_write_capacity = var.dynamo_max_write_capacity
  autoscaling_role         = "arn:aws:iam::${var.account_number}:role/service-role/DynamoDBAutoscaleRole"
}

module "tenant_events_backups" {
  source                   = "../scheduled_dynamo_backup_event"
  dynamo_backup_lambda_arn = var.dynamo_backup_lambda_arn
  table_name               = "${var.tenant_name}-events-${var.env}"
}

resource "aws_dynamodb_table" "tenant-moderations-table" {
  name             = "${var.tenant_name}-moderations-${var.env}"
  read_capacity    = var.dynamo_min_read_capacity
  write_capacity   = var.dynamo_min_write_capacity
  hash_key         = "leaderboard_set_id"
  range_key        = "entry_key"
  stream_enabled   = "true"
  stream_view_type = "NEW_AND_OLD_IMAGES"
  billing_mode     = var.use_on_demand_dyanmo ? "PAY_PER_REQUEST" : "PROVISIONED"

  point_in_time_recovery {
    enabled = "true"
  }

  attribute {
    name = "leaderboard_set_id"
    type = "S"
  }

  attribute {
    name = "entry_key"
    type = "S"
  }

  attribute {
    name = "last_updated"
    type = "S"
  }

  lifecycle {
    ignore_changes = [read_capacity, write_capacity, global_secondary_index]
  }

  global_secondary_index {
    name            = "${var.tenant_name}-moderations-${var.env}_entry_key-index"
    hash_key        = "entry_key"
    range_key       = "last_updated"
    projection_type = "ALL"
    read_capacity   = var.use_on_demand_dyanmo ? "0" : var.dynamo_min_read_capacity
    write_capacity  = var.use_on_demand_dyanmo ? "0" : var.dynamo_min_write_capacity
  }
}

module "tenant_moderations_autoscaling_policy" {
  source             = "../dynamo_autoscaling"
  enabled            = !var.use_on_demand_dyanmo
  table_name         = "${var.tenant_name}-moderations-${var.env}"
  min_read_capacity  = "50"
  min_write_capacity = "50"
  autoscaling_role   = "arn:aws:iam::${var.account_number}:role/service-role/DynamoDBAutoscaleRole"
  pager_duty_sns_arn = var.pager_duty_sns_arn
}

module "tenant_moderations_entry_key-index_autoscaling_policy" {
  source                   = "../dynamo_index_autoscaling"
  enabled                  = !var.use_on_demand_dyanmo
  table_name               = "${var.tenant_name}-moderations-${var.env}"
  index_name               = "${var.tenant_name}-moderations-${var.env}_entry_key-index"
  min_index_read_capacity  = "50"
  min_index_write_capacity = "50"
  autoscaling_role         = "arn:aws:iam::${var.account_number}:role/service-role/DynamoDBAutoscaleRole"
}

module "tenant_moderations_backups" {
  source                   = "../scheduled_dynamo_backup_event"
  dynamo_backup_lambda_arn = var.dynamo_backup_lambda_arn
  table_name               = "${var.tenant_name}-moderations-${var.env}"
}
