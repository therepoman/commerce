variable "env" {}

variable "pantheon_logscan_namespace" {}

variable "security_groups" {
  type = list(string)
}

variable "subnets" {}

variable "lambda_name" {}

variable "lambda_bucket" {}

variable "timeout" {
  default = 60
}

variable "memory_size" {
  default = 1024
}

variable "pager_duty_sns_arn" {
  type    = string
  default = ""
  description = "ARN of the SNS topic set up for Pager Duty alarming"
}
