variable "queue_name" {
  type = string
}

resource "aws_cloudwatch_metric_alarm" "queue_oldest_message_alarm" {
  alarm_name = "oldest_message-${var.queue_name}"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = "1"
  metric_name = "ApproximateAgeOfOldestMessage"
  namespace = "AWS/SQS"

  dimensions = {
    QueueName = var.queue_name
  }

  period = "300"
  statistic = "Maximum"
  threshold = "345600"
  alarm_description = "[LowSev] This metric monitors the age of the oldest message in the queue. It will alarm if the oldest message in the queue is older than 4 days."
  treat_missing_data = "notBreaching"
}
