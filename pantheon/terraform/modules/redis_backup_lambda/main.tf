resource "aws_iam_role" "redis_backup_lambda" {
  name = "redis_backup_lambda"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_lambda_function" "redis_backup_lambda" {
  function_name    = "redis_backup"
  filename         = "${path.module}/deployment.zip"
  source_code_hash = filesha256("${path.module}/deployment.zip")
  role             = aws_iam_role.redis_backup_lambda.arn
  handler          = "main"
  runtime          = "go1.x"
}

output "lambda_arn" {
  value = aws_lambda_function.redis_backup_lambda.arn
}

resource "aws_lambda_permission" "allow_cloudwatch" {
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.redis_backup_lambda.function_name
  principal     = "events.amazonaws.com"
}

resource "aws_iam_policy" "redis_manage_backups_policy" {
  name        = "redis_manage_backups_policy"
  description = "manage redis cluster backups"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "elasticache:DescribeReplicationGroups",
        "elasticache:CreateSnapshot",
        "elasticache:DescribeSnapshots",
        "elasticache:DeleteSnapshot"
      ],
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_policy_attachment" "redis_manage_backups_policy_attachment" {
  name       = "redis_manage_backups_policy_attachment"
  policy_arn = aws_iam_policy.redis_manage_backups_policy.arn
  roles      = [aws_iam_role.redis_backup_lambda.name]
}

resource "aws_iam_policy" "redis_write_cloudwatch_logs_policy" {
  name        = "redis_write_cloudwatch_logs_policy"
  description = "Write logs to cloudwatch"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents",
        "logs:DescribeLogStreams"
      ],
      "Effect": "Allow",
      "Resource": "arn:aws:logs:*:*:*"
    }
  ]
}
EOF
}

resource "aws_iam_policy_attachment" "write_cloudwatch_logs_policy_attachment" {
  name       = "redis_write_cloudwatch_logs_policy_attachment"
  policy_arn = aws_iam_policy.redis_write_cloudwatch_logs_policy.arn
  roles      = [aws_iam_role.redis_backup_lambda.name]
}
