output "queue_arn" {
  value = aws_sqs_queue.sqs_queue.arn
}

output "queue_name" {
  value = aws_sqs_queue.sqs_queue.name
}

output "queue_id" {
  value = aws_sqs_queue.sqs_queue.id
}