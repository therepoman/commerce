resource "aws_sqs_queue" "sqs_queue_deadletter" {
  message_retention_seconds = var.message_retention_seconds
  name                      = "${var.queue_name}_deadletter${var.queue_name_suffix}"
}

resource "aws_sqs_queue" "sqs_queue" {
  message_retention_seconds = var.message_retention_seconds
  name                      = "${var.queue_name}${var.queue_name_suffix}"
  receive_wait_time_seconds = var.receive_wait_time
  delay_seconds             = var.delivery_delay
  redrive_policy            = <<EOF
{
  "deadLetterTargetArn": "${aws_sqs_queue.sqs_queue_deadletter.arn}",
  "maxReceiveCount": ${var.max_receive_count}
}
EOF
}