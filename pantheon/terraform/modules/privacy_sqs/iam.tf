resource "aws_sqs_queue_policy" "queue_policy" {
  queue_url = aws_sqs_queue.sqs_queue.id
  policy    = data.aws_iam_policy_document.default_queue_policy_doc.json
}

data "aws_iam_policy_document" "default_queue_policy_doc" {
  statement {
    sid = "senderSID"

    actions = [
      "SQS:SendMessage",
    ]

    principals {
      identifiers = [
        "*",
      ]
      type = "AWS"
    }

    resources = [
      aws_sqs_queue.sqs_queue.arn,
    ]

    condition {
      test     = "ArnEquals"
      variable = "aws:SourceArn"
      values   = var.message_sender_arns
    }
  }
}

resource "aws_iam_policy" "queue_read_policy" {
  name        = "${var.queue_name}-read_sqs_policy"
  description = "This allows read access to ${var.queue_name} queue"
  policy      = data.aws_iam_policy_document.queue_read_policy_doc.json
}

data "aws_iam_policy_document" "queue_read_policy_doc" {
  statement {
    sid    = "receiverSID"
    effect = "Allow"
    actions = [
      "SQS:GetQueueAttributes",
      "SQS:ListQueues",
      "SQS:ReceiveMessage",
      "SQS:ChangeMessageVisibility",
      "SQS:DeleteMessage",
    ]
    resources = [
      aws_sqs_queue.sqs_queue.arn,
    ]
  }
}

resource "aws_iam_policy_attachment" "queue_read_policy_attachment" {
  name       = "${var.queue_name}-read_sqs_policy_attachment"
  roles      = [var.message_receiver_role_name]
  policy_arn = aws_iam_policy.queue_read_policy.arn
}