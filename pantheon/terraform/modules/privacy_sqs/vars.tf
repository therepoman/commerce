variable "queue_name" {
  type = string
}

variable "receive_wait_time" {
  type    = string
  default = 20
}

variable "message_retention_seconds" {
  type    = string
  default = 1209600
}

variable "max_receive_count" {
  type    = string
  default = 7
}

variable "delivery_delay" {
  type    = string
  default = 0
}

variable "alarm_dlq" {
  default     = false
  description = "flag to determine if we should be making a paging alarm for the DLQ"
}

variable "queue_name_suffix" {
  type        = string
  default     = ""
  description = "a way to append _staging to the end of the dlqs in staging"
}

variable "message_sender_arns" {
  type        = list(string)
  description = "the ARNs of the resources that has right to publish to the sqs"
}

variable "message_receiver_role_name" {
  type        = string
  description = "the role name of the resource that has right to read and delete from the sqs"
}