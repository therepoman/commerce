variable "table_name" {
  type = string
}

variable "dynamo_backup_lambda_arn" {
  type = string
}

variable "backups_to_keep_per_time_unit" {
  default = 5
}

resource "aws_cloudwatch_event_rule" "dynamo_backup_event_hourly_rule" {
  name                = "dynamo-backup-hourly-event-rule-${var.table_name}"
  description         = "Dynamo backup hourly event rule for table ${var.table_name}"
  schedule_expression = "rate(1 hour)"
  is_enabled          = "false"
}

resource "aws_cloudwatch_event_target" "lambda_backup_hourly_target" {
  rule      = aws_cloudwatch_event_rule.dynamo_backup_event_hourly_rule.name
  target_id = "dynamo-backup-hourly-lambda-target-${var.table_name}"
  arn       = var.dynamo_backup_lambda_arn

  input = <<EOF
{
  "table_name" : "${var.table_name}",
  "time_unit" : "hour",
  "number_to_keep" : ${var.backups_to_keep_per_time_unit}
}
EOF
}

resource "aws_cloudwatch_event_rule" "dynamo_backup_event_daily_rule" {
  name                = "dynamo-backup-daily-event-rule-${var.table_name}"
  description         = "Dynamo backup daily event rule for table ${var.table_name}"
  schedule_expression = "rate(1 day)"
}

resource "aws_cloudwatch_event_target" "lambda_backup_daily_target" {
  rule      = aws_cloudwatch_event_rule.dynamo_backup_event_daily_rule.name
  target_id = "dynamo-backup-daily-lambda-target-${var.table_name}"
  arn       = var.dynamo_backup_lambda_arn

  input = <<EOF
{
  "table_name" : "${var.table_name}",
  "time_unit" : "day",
  "number_to_keep" : ${var.backups_to_keep_per_time_unit}
}
EOF
}

resource "aws_cloudwatch_event_rule" "dynamo_backup_event_weekly_rule" {
  name                = "dynamo-backup-weekly-event-rule-${var.table_name}"
  description         = "Dynamo backup weekly event rule for table ${var.table_name}"
  schedule_expression = "cron(0 0 ? * MON *)"
}

resource "aws_cloudwatch_event_target" "lambda_backup_weekly_target" {
  rule      = aws_cloudwatch_event_rule.dynamo_backup_event_weekly_rule.name
  target_id = "dynamo-backup-weekly-lambda-target-${var.table_name}"
  arn       = var.dynamo_backup_lambda_arn

  input = <<EOF
{
  "table_name" : "${var.table_name}",
  "time_unit" : "week",
  "number_to_keep" : ${var.backups_to_keep_per_time_unit}
}
EOF
}

resource "aws_cloudwatch_event_rule" "dynamo_backup_event_monthly_rule" {
  name                = "dynamo-backup-monthly-event-rule-${var.table_name}"
  description         = "Dynamo backup monthly event rule for table ${var.table_name}"
  schedule_expression = "cron(0 0 1 * ? *)"
}

resource "aws_cloudwatch_event_target" "lambda_backup_monthly_target" {
  rule      = aws_cloudwatch_event_rule.dynamo_backup_event_monthly_rule.name
  target_id = "dynamo-backup-monthly-lambda-target-${var.table_name}"
  arn       = var.dynamo_backup_lambda_arn

  input = <<EOF
{
  "table_name" : "${var.table_name}",
  "time_unit" : "month",
  "number_to_keep" : ${var.backups_to_keep_per_time_unit}
}
EOF
}
