variable "cluster_name" {
  type = string
}

variable "redis_backup_lambda_arn" {
  type = string
}

variable "backups_to_keep_per_time_unit" {
  default = 5
}

resource "aws_cloudwatch_event_rule" "redis_backup_event_hourly_rule" {
  name                = "redis-backup-hourly-event-rule-${var.cluster_name}"
  description         = "Redis backup hourly event rule for cluster ${var.cluster_name}"
  schedule_expression = "cron(0 * * * ? *)"
  is_enabled          = "false"
}

resource "aws_cloudwatch_event_target" "lambda_backup_hourly_target" {
  rule      = aws_cloudwatch_event_rule.redis_backup_event_hourly_rule.name
  target_id = "redis-backup-hourly-lambda-target-${var.cluster_name}"
  arn       = var.redis_backup_lambda_arn

  input = <<EOF
{
  "cluster_name" : "${var.cluster_name}",
  "time_unit" : "hour",
  "number_to_keep" : ${var.backups_to_keep_per_time_unit}
}
EOF
}

resource "aws_cloudwatch_event_rule" "redis_backup_event_daily_rule" {
  name                = "redis-backup-daily-event-rule-${var.cluster_name}"
  description         = "Redis backup daily event rule for cluster ${var.cluster_name}"
  schedule_expression = "cron(15 0 * * ? *)"
}

resource "aws_cloudwatch_event_target" "lambda_backup_daily_target" {
  rule      = aws_cloudwatch_event_rule.redis_backup_event_daily_rule.name
  target_id = "redis-backup-daily-lambda-target-${var.cluster_name}"
  arn       = var.redis_backup_lambda_arn

  input = <<EOF
{
  "cluster_name" : "${var.cluster_name}",
  "time_unit" : "day",
  "number_to_keep" : ${var.backups_to_keep_per_time_unit}
}
EOF
}

resource "aws_cloudwatch_event_rule" "redis_backup_event_weekly_rule" {
  name                = "redis-backup-weekly-event-rule-${var.cluster_name}"
  description         = "Redis backup weekly event rule for cluster ${var.cluster_name}"
  schedule_expression = "cron(30 0 ? * MON *)"
}

resource "aws_cloudwatch_event_target" "lambda_backup_weekly_target" {
  rule      = aws_cloudwatch_event_rule.redis_backup_event_weekly_rule.name
  target_id = "dynamo-backup-weekly-lambda-target-${var.cluster_name}"
  arn       = var.redis_backup_lambda_arn

  input = <<EOF
{
  "cluster_name" : "${var.cluster_name}",
  "time_unit" : "week",
  "number_to_keep" : ${var.backups_to_keep_per_time_unit}
}
EOF
}

resource "aws_cloudwatch_event_rule" "redis_backup_event_monthly_rule" {
  name                = "redis-backup-monthly-event-rule-${var.cluster_name}"
  description         = "Redis backup monthly event rule for cluster ${var.cluster_name}"
  schedule_expression = "cron(45 0 1 * ? *)"
}

resource "aws_cloudwatch_event_target" "lambda_backup_monthly_target" {
  rule      = aws_cloudwatch_event_rule.redis_backup_event_monthly_rule.name
  target_id = "redis-backup-monthly-lambda-target-${var.cluster_name}"
  arn       = var.redis_backup_lambda_arn

  input = <<EOF
{
  "cluster_name" : "${var.cluster_name}",
  "time_unit" : "month",
  "number_to_keep" : ${var.backups_to_keep_per_time_unit}
}
EOF
}
