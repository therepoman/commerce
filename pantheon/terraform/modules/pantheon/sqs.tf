resource "aws_sqs_queue" "archival_processing_queue_non_fifo_dlq" {
  name       = "archival-processing-queue-dlq-${var.env}"
  fifo_queue = false
  message_retention_seconds = 1209600 // 14 days
}

resource "aws_sqs_queue" "archival_processing_queue_non_fifo" {
  name           = "archival-processing-queue-${var.env}"
  redrive_policy = "{\"deadLetterTargetArn\":\"${aws_sqs_queue.archival_processing_queue_non_fifo_dlq.arn}\",\"maxReceiveCount\":4}"
  message_retention_seconds = 1209600 // 14 days
}

resource "aws_sqs_queue" "moderation_processing_non_fifo_queue_dlq" {
  name = "moderation-processing-non-fifo-queue-dlq-${var.env}"
  message_retention_seconds = 1209600 // 14 days
}

resource "aws_sqs_queue" "moderation_processing_non_fifo_queue" {
  name           = "moderation-processing-non-fifo-queue-${var.env}"
  redrive_policy = "{\"deadLetterTargetArn\":\"${aws_sqs_queue.moderation_processing_non_fifo_queue_dlq.arn}\",\"maxReceiveCount\":4}"
  message_retention_seconds = 1209600 // 14 days
}

resource "aws_sqs_queue" "published_events_new_processing_queue_dlq" {
  name                      = "published-events-new-processing-queue-dlq-${var.env}"
  message_retention_seconds = 1209600 // 14 days
}

resource "aws_sqs_queue" "published_events_new_processing_queue" {
  name                       = "published-events-new-processing-queue-${var.env}"
  redrive_policy             = "{\"deadLetterTargetArn\":\"${aws_sqs_queue.published_events_new_processing_queue_dlq.arn}\",\"maxReceiveCount\":10}"
  visibility_timeout_seconds = "60"
  message_retention_seconds = 1209600 // 14 days
}

resource "aws_sqs_queue" "published_events_owl_2019_processing_queue_dlq" {
  name = "published-events-owl-2019-processing-queue-dlq-${var.env}"
  message_retention_seconds = 1209600 // 14 days
}

resource "aws_sqs_queue" "published_events_owl_2019_processing_queue" {
  name           = "published-events-owl-2019-processing-queue-${var.env}"
  redrive_policy = "{\"deadLetterTargetArn\":\"${aws_sqs_queue.published_events_owl_2019_processing_queue_dlq.arn}\",\"maxReceiveCount\":4}"
  message_retention_seconds = 1209600 // 14 days
}

resource "aws_sqs_queue" "user_moderation_queue_dlq" {
  name = "user-moderation-queue-dlq-${var.env}"
  message_retention_seconds = 1209600 // 14 days
}

resource "aws_sqs_queue" "user_moderation_queue" {
  name           = "user-moderation-queue-${var.env}"
  redrive_policy = "{\"deadLetterTargetArn\":\"${aws_sqs_queue.user_moderation_queue_dlq.arn}\",\"maxReceiveCount\":4}"
  message_retention_seconds = 1209600 // 14 days
}

resource "aws_sns_topic_subscription" "user_moderation_subscription" {
  topic_arn = var.user_moderation_sns_arn
  protocol  = "sqs"
  endpoint  = aws_sqs_queue.user_moderation_queue.arn
}

resource "aws_sqs_queue_policy" "user_moderation_queue_policy" {
  queue_url = aws_sqs_queue.user_moderation_queue.id
  policy    = data.aws_iam_policy_document.user_moderation_queue_policy_doc.json
}

data "aws_iam_policy_document" "user_moderation_queue_policy_doc" {
  statement {
    sid = "${aws_sqs_queue.user_moderation_queue.name}-Sid"

    actions = ["SQS:SendMessage"]

    principals {
      identifiers = ["*"]
      type        = "AWS"
    }

    resources = [aws_sqs_queue.user_moderation_queue.arn]

    condition {
      test     = "ArnEquals"
      variable = "aws:SourceArn"

      values = [var.user_moderation_sns_arn]
    }
  }
}

module "pdms_delete_leaderboards_sqs" {
  source                     = "../privacy_sqs"
  queue_name                 = "${var.env}-DeleteLeaderboards"
  alarm_dlq                  = true
  message_sender_arns        = [module.service.service_role_arn, module.service.canary_role_arn]
  message_receiver_role_name = module.delete_leaderboards_sfn.sfn_role_name
}

module "pdms_delete_entries_sqs" {
  source                     = "../privacy_sqs"
  queue_name                 = "${var.env}-DeleteEntries"
  alarm_dlq                  = true
  message_sender_arns        = [module.service.service_role_arn, module.service.canary_role_arn]
  message_receiver_role_name = module.delete_entries_sfn.sfn_role_name
}