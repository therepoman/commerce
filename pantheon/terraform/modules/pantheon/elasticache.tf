module "redis_backup-lambda" {
  source = "../redis_backup_lambda"
}

resource "aws_elasticache_replication_group" "pantheon_redis" {
  node_type                     = var.elasticache_instance_type
  port                          = var.redis_port
  engine                        = "redis"
  engine_version                = "3.2.4"
  replication_group_description = "pantheon redis cluster"
  replication_group_id          = "pantheon-${var.env}"
  automatic_failover_enabled    = var.elasticache_auto_failover
  availability_zones            = ["us-west-2c", "us-west-2a", "us-west-2b"]
  number_cache_clusters         = var.elasticache_nodes
  parameter_group_name          = aws_elasticache_parameter_group.pantheon_redis_params.name
  subnet_group_name             = aws_elasticache_subnet_group.pantheon_redis_subnet_group.name
  security_group_ids            = [aws_security_group.pantheon_redis_security_group.id]
}

resource "aws_elasticache_replication_group" "pdms_pantheon_redis" {
  node_type                     = var.elasticache_instance_type
  port                          = var.redis_port
  engine                        = "redis"
  engine_version                = "3.2.4"
  replication_group_description = "pantheon redis cluster for loading leaderboards for PDMS deletions"
  replication_group_id          = "pantheon-${var.env}-pdms"
  automatic_failover_enabled    = var.elasticache_auto_failover
  number_cache_clusters         = var.elasticache_nodes
  parameter_group_name          = aws_elasticache_parameter_group.pantheon_redis_params.name
  subnet_group_name             = aws_elasticache_subnet_group.pantheon_redis_subnet_group.name
  security_group_ids            = [aws_security_group.pantheon_redis_security_group.id]
}

resource "aws_elasticache_parameter_group" "pantheon_redis_params" {
  family = "redis3.2"
  name   = "pantheon-params-${var.env}"

  parameter {
    name  = "maxmemory-policy"
    value = "allkeys-lru"
  }

  parameter {
    name  = "notify-keyspace-events"
    value = "Ex"
  }
}

resource "aws_elasticache_subnet_group" "pantheon_redis_subnet_group" {
  name       = "pantheon-${var.env}-cache"
  subnet_ids = split(",", var.private_subnets)
}

resource "aws_security_group" "pantheon_redis_security_group" {
  name        = "pantheon-${var.env}-redis"
  vpc_id      = var.vpc_id
  description = "Allows communication with redis server"

  ingress {
    from_port   = var.redis_port
    protocol    = "tcp"
    to_port     = var.redis_port
    cidr_blocks = var.private_cidr_blocks
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "pantheon-${var.env}-redis"
  }
}

module "redis_backups" {
  source                  = "../scheduled_redis_backup_event"
  redis_backup_lambda_arn = module.redis_backup-lambda.lambda_arn
  cluster_name            = "pantheon-${var.env}"
}
