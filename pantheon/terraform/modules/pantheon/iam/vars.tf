variable "role_name" {
  description = "the name of the role to attach policies to"
}

variable "sandstorm_arn" {
  description = "The sandstorm arn to attach the role to"
}

variable "service" {
  description = "Short name of the service being built. Should probably leave this as the default"
  default     = "pantheon"
}
