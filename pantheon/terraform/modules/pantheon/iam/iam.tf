resource aws_iam_role_policy "ecs_task_role_sandstorm_policy" {
  name = "${var.service}-sandstorm-policy"
  role = var.role_name

  policy = <<EOT
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Resource": "${var.sandstorm_arn}",
      "Action": "sts:AssumeRole"
    }
  ]
}
EOT
}

// Grant the Instance Profile full dynamo access
resource "aws_iam_role_policy_attachment" "dynamo_policy" {
  role       = var.role_name
  policy_arn = "arn:aws:iam::aws:policy/AmazonDynamoDBFullAccess"
}

// Grant the Instance Profile full SQS access
resource "aws_iam_role_policy_attachment" "sqs_policy" {
  role       = var.role_name
  policy_arn = "arn:aws:iam::aws:policy/AmazonSQSFullAccess"
}

// Grant the Instance Profile full cloudwatch access
resource "aws_iam_role_policy_attachment" "cw_policy" {
  role       = var.role_name
  policy_arn = "arn:aws:iam::aws:policy/CloudWatchFullAccess"
}

// Grant the Instance Profile full S3 access
resource "aws_iam_role_policy_attachment" "s3_policy" {
  role       = var.role_name
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
}

// Grant the Instance Profile full SNS access
resource "aws_iam_role_policy_attachment" "sns_policy_attachment" {
  role       = var.role_name
  policy_arn = "arn:aws:iam::aws:policy/AmazonSNSFullAccess"
}

data "aws_iam_policy_document" "sfn_execution_policy_document" {
  version = "2012-10-17"

  statement {
    actions = [
      "states:StartExecution",
    ]

    effect = "Allow"

    resources = [
      "*",
    ]
  }
}

resource "aws_iam_role_policy" "sfn_execution" {
  name   = "sfn_execution"
  role   = var.role_name
  policy = data.aws_iam_policy_document.sfn_execution_policy_document.json
}

data "aws_iam_policy_document" "ssm_access_policy_document" {
  version = "2012-10-17"

  statement {
    actions = [
      "ssm:GetParameterHistory",
      "ssm:GetParametersByPath",
      "ssm:GetParameters",
      "ssm:GetParameter",
      "ssm:DescribeParameters"
    ]

    effect = "Allow"

    resources = [
      "*",
    ]
  }
}

// Grant the Instance Profile access to fetch SSM Data
resource "aws_iam_role_policy" "ssm_access" {
  name   = "ssm_access"
  role   = var.role_name
  policy = data.aws_iam_policy_document.ssm_access_policy_document.json
}
