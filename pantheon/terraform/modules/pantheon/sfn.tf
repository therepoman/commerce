locals {
  default_retry = <<EOF
    [
      {
        "ErrorEquals": [
          "States.ALL"
        ],
        "IntervalSeconds": 5,
        "MaxAttempts": 3,
        "BackoffRate": 3.0
      }
    ]
EOF
}

module "delete_entries_sfn" {
  source          = "../sfn"
  sfn_name        = "delete_entries_v1_sfn"
  sfn_role        = "delete_entries_sfn_role"
  sfn_definition  = <<EOF
{
  "Comment": "This step function goes through Dynamo and Redis to delete user data",
  "StartAt": "InitialWait",
  "States": {
    "InitialWait": {
      "Type": "Wait",
      "SecondsPath": "$.wait_seconds",
      "Next": "DeleteEntryKeyFromEventsTableAndRedis"
    },
    "DeleteEntryKeyFromEventsTableAndRedis": {
      "Type": "Parallel",
      "Branches": [
        {
          "StartAt": "QueryEventsByEntryKey",
          "States": {
            "QueryEventsByEntryKey": {
              "Type": "Task",
              "Resource": "${module.query_events_by_entry_key_lambda.lambda_arn}",
              "Retry": ${local.default_retry},
              "Next": "BatchDeleteEventsWait"
            },
            "BatchDeleteEventsWait": {
              "Type": "Wait",
              "SecondsPath": "$.wait_seconds",
              "Next": "Batch"
            },
            "Batch": {
              "Type": "Parallel",
              "Branches": [
                {
                  "StartAt": "BatchDeleteEvents",
                  "States": {
                    "BatchDeleteEvents": {
                      "Type": "Task",
                      "Resource": "${module.batch_delete_events_lambda.lambda_arn}",
                      "Retry": ${local.default_retry},
                      "End": true
                    }
                  }
                },
                {
                  "StartAt": "DeleteLeaderboardEntries",
                  "States": {
                    "DeleteLeaderboardEntries": {
                      "Type": "Task",
                      "Resource": "${module.delete_leaderboard_entries_lambda.lambda_arn}",
                      "Retry": ${local.default_retry},
                      "End": true
                    }
                  }
                }
              ],
              "Next": "QueryEventsHasLastEvaluatedKey"
            },
            "QueryEventsHasLastEvaluatedKey": {
              "Type": "Choice",
              "InputPath": "$[0]",
              "Choices": [
                {
                  "Variable": "$.leaderboard_id",
                  "StringEquals": "",
                  "Next": "QueryEventsHasLastEvaluatedKeySucceed"
                }
              ],
              "Default": "QueryEventsByEntryKey"
            },
            "QueryEventsHasLastEvaluatedKeySucceed": {
              "Type": "Succeed"
            }
          }
        },
        {
          "StartAt": "QueryModerationsByEntryKey",
          "States": {
            "QueryModerationsByEntryKey": {
              "Type": "Task",
              "Resource": "${module.query_moderations_by_entry_key_lambda.lambda_arn}",
              "Retry": ${local.default_retry},
              "Next": "BatchDeleteModerationsWait"
            },
            "BatchDeleteModerationsWait": {
              "Type": "Wait",
              "SecondsPath": "$.wait_seconds",
              "Next": "BatchDeleteModerations"
            },
            "BatchDeleteModerations": {
              "Type": "Task",
              "Resource": "${module.batch_delete_moderations_lambda.lambda_arn}",
              "Retry": ${local.default_retry},
              "Next": "QueryModerationsHasLastEvaluatedKey"
            },
            "QueryModerationsHasLastEvaluatedKey": {
              "Type": "Choice",
              "Choices": [
                {
                  "Variable": "$.leaderboard_set_id",
                  "StringEquals": "",
                  "Next": "ModerationsDeleteSuccess"
                }
              ],
              "Default": "QueryModerationsByEntryKey"
            },
            "ModerationsDeleteSuccess": {
              "Type": "Succeed"
            }
          }
        }
      ],
      "End": true
    }
  }
}
EOF
}

module "delete_leaderboards_sfn" {
  source          = "../sfn"
  sfn_name        = "delete_leaderboards_v1_sfn"
  sfn_role        = "delete_leaderboards_sfn_role"
  sfn_definition  = <<EOF
{
  "Comment": "This step function deletes leaderboards in Redis and S3 given a domain and grouping key",
  "StartAt": "Wait",
  "States": {
    "Wait": {
      "Type": "Wait",
      "SecondsPath": "$.wait_seconds",
      "Next": "DeleteLeaderboardsByGroupingKey"
    },
    "DeleteLeaderboardsByGroupingKey": {
      "Type": "Task",
      "Resource": "${module.delete_leaderboards_by_grouping_key_lambda.lambda_arn}",
      "Retry": ${local.default_retry},
      "End": true
    }
  }
}
EOF
}
