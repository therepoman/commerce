module "query_events_by_entry_key_lambda" {
  source                     = "../lambda"
  lambda_bucket              = aws_s3_bucket.lambda_bucket.bucket
  lambda_name                = "query_events_by_entry_key"
  pantheon_logscan_namespace = "pantheon"

  env                        = var.env
  security_groups            = [var.sg_id]
  subnets                    = var.private_subnets
  pager_duty_sns_arn         = aws_sns_topic.pager-duty-sns.arn
}

module "query_moderations_by_entry_key_lambda" {
  source                     = "../lambda"
  lambda_bucket              = aws_s3_bucket.lambda_bucket.bucket
  lambda_name                = "query_moderations_by_entry_key"
  pantheon_logscan_namespace = "pantheon"

  env                        = var.env
  security_groups            = [var.sg_id]
  subnets                    = var.private_subnets
  pager_duty_sns_arn         = aws_sns_topic.pager-duty-sns.arn
}

module "batch_delete_moderations_lambda" {
  source                     = "../lambda"
  lambda_bucket              = aws_s3_bucket.lambda_bucket.bucket
  lambda_name                = "batch_delete_moderations"
  pantheon_logscan_namespace = "pantheon"

  env                        = var.env
  security_groups            = [var.sg_id]
  subnets                    = var.private_subnets
  pager_duty_sns_arn         = aws_sns_topic.pager-duty-sns.arn
}

module "batch_delete_events_lambda" {
  source                     = "../lambda"
  lambda_bucket              = aws_s3_bucket.lambda_bucket.bucket
  lambda_name                = "batch_delete_events"
  pantheon_logscan_namespace = "pantheon"

  env                        = var.env
  security_groups            = [var.sg_id]
  subnets                    = var.private_subnets
  pager_duty_sns_arn         = aws_sns_topic.pager-duty-sns.arn
}

module "delete_leaderboard_entries_lambda" {
  source                     = "../lambda"
  lambda_bucket              = aws_s3_bucket.lambda_bucket.bucket
  lambda_name                = "delete_leaderboard_entries"
  pantheon_logscan_namespace = "pantheon"
  timeout                    = "300"

  env                        = var.env
  security_groups            = [var.sg_id]
  subnets                    = var.private_subnets
  pager_duty_sns_arn         = aws_sns_topic.pager-duty-sns.arn
}

module "delete_leaderboards_by_grouping_key_lambda" {
  source                     = "../lambda"
  lambda_bucket              = aws_s3_bucket.lambda_bucket.bucket
  lambda_name                = "delete_leaderboards_by_grouping_key"
  pantheon_logscan_namespace = "pantheon"

  env                        = var.env
  security_groups            = [var.sg_id]
  subnets                    = var.private_subnets
  pager_duty_sns_arn         = aws_sns_topic.pager-duty-sns.arn
}

module "start_delete_leaderboards_lambda" {
  source                     = "../lambda"
  lambda_bucket              = aws_s3_bucket.lambda_bucket.bucket
  lambda_name                = "start_delete_leaderboards"
  pantheon_logscan_namespace = "pantheon"

  env                        = var.env
  security_groups            = [var.sg_id]
  subnets                    = var.private_subnets
  pager_duty_sns_arn         = aws_sns_topic.pager-duty-sns.arn
}

resource "aws_cloudwatch_event_rule" "start_delete_leaderboards" {
  name        = "cron_${module.start_delete_leaderboards_lambda.lambda_name}"
  description = "Starts the ${module.start_delete_leaderboards_lambda.lambda_name} lambda to kick off batch delete step fn"

  schedule_expression = "rate(1 hour)"
}

resource "aws_cloudwatch_event_target" "start_delete_leaderboards" {
  rule      = aws_cloudwatch_event_rule.start_delete_leaderboards.name
  arn       = module.start_delete_leaderboards_lambda.lambda_arn

  input = <<DOC
{
  "max_messages": 100
}
DOC
}

resource "aws_lambda_permission" "start_delete_leaderboards_lambda_allow_cloudwatch" {
  statement_id  = "AllowExecutionFromCloudWatch-${module.start_delete_leaderboards_lambda.lambda_name}"
  action        = "lambda:InvokeFunction"
  function_name = module.start_delete_leaderboards_lambda.lambda_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.start_delete_leaderboards.arn
}

module "start_delete_entries_lambda" {
  source                     = "../lambda"
  lambda_bucket              = aws_s3_bucket.lambda_bucket.bucket
  lambda_name                = "start_delete_entries"
  pantheon_logscan_namespace = "pantheon"
  timeout = 300

  env                        = var.env
  security_groups            = [var.sg_id]
  subnets                    = var.private_subnets
  pager_duty_sns_arn         = aws_sns_topic.pager-duty-sns.arn
}

resource "aws_cloudwatch_event_rule" "start_delete_entries" {
  name        = "cron_${module.start_delete_entries_lambda.lambda_name}"
  description = "Starts the ${module.start_delete_entries_lambda.lambda_name} lambda to kick off batch delete step fn"

  schedule_expression = "rate(1 hour)"
}

resource "aws_cloudwatch_event_target" "start_delete_entries" {
  rule      = aws_cloudwatch_event_rule.start_delete_entries.name
  arn       = module.start_delete_entries_lambda.lambda_arn

  input = <<DOC
{
  "max_messages": 100
}
DOC
}

resource "aws_lambda_permission" "start_delete_entries_lambda_allow_cloudwatch" {
  statement_id  = "AllowExecutionFromCloudWatch-${module.start_delete_entries_lambda.lambda_name}"
  action        = "lambda:InvokeFunction"
  function_name = module.start_delete_entries_lambda.lambda_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.start_delete_entries.arn
}
