resource "aws_s3_bucket" "lambda_bucket" {
  bucket = "pantheon-lambdas-${var.env}"
  acl    = "private"
}
