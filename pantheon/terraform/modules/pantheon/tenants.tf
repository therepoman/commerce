locals {
  polls_read_capacity  = var.env == "prod" ? 50 : 5
  polls_write_capacity = var.env == "prod" ? 50 : 5

  trains_read_capacity  = var.env == "prod" ? 50 : 5
  trains_write_capacity = var.env == "prod" ? 50 : 5

  cogo_read_capacity  = var.env == "prod" ? 50 : 5
  cogo_write_capacity = var.env == "prod" ? 50 : 5
}

module "polls_tenant" {
  source = "../tenant"

  tenant_name                            = "polls"
  elasticache_instance_type              = var.elasticache_instance_type
  env                                    = var.env
  private_cidr_blocks                    = var.private_cidr_blocks
  private_subnets                        = var.private_subnets
  vpc_id                                 = var.vpc_id
  dynamo_backup_lambda_arn               = module.dynamo_backup-lambda.lambda_arn
  account_number                         = var.account_number
  redis_backup_lambda_arn                = module.redis_backup-lambda.lambda_arn
  use_on_demand_dyanmo                   = true
  elasticache_nodes                      = var.tenant_elasticache_nodes
  ecs_service_role_arns                  = [module.service.service_role_arn, module.service.canary_role_arn]
  pdms_delete_entries_sfn_role_name      = module.delete_entries_sfn.sfn_role_name
  pdms_delete_leaderboards_sfn_role_name = module.delete_leaderboards_sfn.sfn_role_name
  pdms_start_delete_entries_lambda_name  = module.start_delete_entries_lambda.lambda_name
  pdms_start_delete_entries_lambda_arn   = module.start_delete_entries_lambda.lambda_arn
  pdms_start_delete_leaderboards_lambda_name = module.start_delete_leaderboards_lambda.lambda_name
  pdms_start_delete_leaderboards_lambda_arn  = module.start_delete_leaderboards_lambda.lambda_arn
  pdms_start_delete_entries_max_messages      = 100
  pager_duty_sns_arn = aws_sns_topic.pager-duty-sns.arn
  events_dynamo_read_throttle_alarm_threshold  = "10000"
  events_dynamo_write_throttle_alarm_threshold = "10000"
}

module "trains_tenant" {
  source = "../tenant"

  tenant_name                            = "trains"
  elasticache_instance_type              = var.elasticache_instance_type
  env                                    = var.env
  private_cidr_blocks                    = var.private_cidr_blocks
  private_subnets                        = var.private_subnets
  vpc_id                                 = var.vpc_id
  dynamo_backup_lambda_arn               = module.dynamo_backup-lambda.lambda_arn
  account_number                         = var.account_number
  redis_backup_lambda_arn                = module.redis_backup-lambda.lambda_arn
  dynamo_min_read_capacity               = local.trains_read_capacity
  dynamo_min_write_capacity              = local.trains_write_capacity
  elasticache_nodes                      = var.tenant_elasticache_nodes
  ecs_service_role_arns                  = [module.service.service_role_arn, module.service.canary_role_arn]
  pdms_delete_entries_sfn_role_name      = module.delete_entries_sfn.sfn_role_name
  pdms_delete_leaderboards_sfn_role_name = module.delete_leaderboards_sfn.sfn_role_name
  pdms_start_delete_entries_lambda_name  = module.start_delete_entries_lambda.lambda_name
  pdms_start_delete_entries_lambda_arn   = module.start_delete_entries_lambda.lambda_arn
  pdms_start_delete_leaderboards_lambda_name = module.start_delete_leaderboards_lambda.lambda_name
  pdms_start_delete_leaderboards_lambda_arn  = module.start_delete_leaderboards_lambda.lambda_arn
  pager_duty_sns_arn = aws_sns_topic.pager-duty-sns.arn
}

module "cogo_tenant" {
  source = "../tenant"

  tenant_name                            = "cogo"
  elasticache_instance_type              = var.elasticache_instance_type
  env                                    = var.env
  private_cidr_blocks                    = var.private_cidr_blocks
  private_subnets                        = var.private_subnets
  vpc_id                                 = var.vpc_id
  dynamo_backup_lambda_arn               = module.dynamo_backup-lambda.lambda_arn
  account_number                         = var.account_number
  redis_backup_lambda_arn                = module.redis_backup-lambda.lambda_arn
  dynamo_min_read_capacity               = local.cogo_read_capacity
  dynamo_min_write_capacity              = local.cogo_write_capacity
  elasticache_nodes                      = var.tenant_elasticache_nodes
  ecs_service_role_arns                  = [module.service.service_role_arn, module.service.canary_role_arn]
  pdms_delete_entries_sfn_role_name      = module.delete_entries_sfn.sfn_role_name
  pdms_delete_leaderboards_sfn_role_name = module.delete_leaderboards_sfn.sfn_role_name
  pdms_start_delete_entries_lambda_name  = module.start_delete_entries_lambda.lambda_name
  pdms_start_delete_entries_lambda_arn   = module.start_delete_entries_lambda.lambda_arn
  pdms_start_delete_leaderboards_lambda_name = module.start_delete_leaderboards_lambda.lambda_name
  pdms_start_delete_leaderboards_lambda_arn  = module.start_delete_leaderboards_lambda.lambda_arn
  pager_duty_sns_arn = aws_sns_topic.pager-duty-sns.arn
}
