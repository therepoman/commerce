# API latency alarms
resource "aws_cloudwatch_metric_alarm" "get_leaderboard_latency_high_sev_alarm" {
  count                     = 1
  alarm_name                = "${var.env}-get-leaderboard-latency-high-sev-alarm"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "5"
  datapoints_to_alarm       = "5"
  metric_name               = "URL:/twirp/code.justin.tv.commerce.pantheonrpc.Pantheon/GetLeaderboard.Latency"
  namespace                 = var.service_name
  period                    = "60"
  extended_statistic        = "p99"
  threshold                 = "0.5"
  unit                      = "Seconds"
  alarm_description         = "High sev latency alarm for the GetLeaderboard API"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled           = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions             = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions                = [aws_sns_topic.pager-duty-sns.arn]

  dimensions = {
    Region   = var.aws_region
    Service  = var.service_name
    Stage    = var.env
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "get_leaderboard_status_high_sev_alarm" {
  alarm_name                = "${var.env}-get-leaderboard-status-high-sev-alarm"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "5"
  datapoints_to_alarm       = "5"
  threshold                 = "5"
  alarm_description         = "High sev status alarm for the GetLeaderboard API"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled           = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions             = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions                = [aws_sns_topic.pager-duty-sns.arn]

  metric_query {
    id          = "e1"
    expression  = "m2/(m1+m2)*100"
    label       = "GetLeaderboard error rate (%)"
    return_data = true
  }

  metric_query {
    id = "m1"

    metric {
      metric_name = "URL:/twirp/code.justin.tv.commerce.pantheonrpc.Pantheon/GetLeaderboard.2xx"
      namespace   = "mako"
      period      = "300"
      stat        = "Sum"

      dimensions = {
        Service  = var.service_name
        Stage    = var.env
        Region   = var.aws_region
        Substage = "primary"
      }
    }
  }

  metric_query {
    id = "m2"

    metric {
      metric_name = "URL:/twirp/code.justin.tv.commerce.pantheonrpc.Pantheon/GetLeaderboard.5xx"
      namespace   = "mako"
      period      = "300"
      stat        = "Sum"

      dimensions = {
        Service  = var.service_name
        Stage    = var.env
        Region   = var.aws_region
        Substage = "primary"
      }
    }
  }
}

resource "aws_cloudwatch_metric_alarm" "get_leaderboards_latency_high_sev_alarm" {
  count                     = 1
  alarm_name                = "${var.env}-get-leaderboards-latency-high-sev-alarm"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "5"
  datapoints_to_alarm       = "5"
  metric_name               = "URL:/twirp/code.justin.tv.commerce.pantheonrpc.Pantheon/GetLeaderboards.Latency"
  namespace                 = var.service_name
  period                    = "300"
  extended_statistic        = "p99"
  threshold                 = "0.5"
  unit                      = "Seconds"
  alarm_description         = "High sev latency alarm for the GetLeaderboards API"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled           = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions             = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions                = [aws_sns_topic.pager-duty-sns.arn]

  dimensions = {
    Region   = var.aws_region
    Service  = var.service_name
    Stage    = var.env
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "get_leaderboards_status_high_sev_alarm" {
  alarm_name                = "${var.env}-get-leaderboards-status-high-sev-alarm"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "5"
  datapoints_to_alarm       = "5"
  threshold                 = "5"
  alarm_description         = "High sev status alarm for the GetLeaderboards API"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled           = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions             = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions                = [aws_sns_topic.pager-duty-sns.arn]

  metric_query {
    id          = "e1"
    expression  = "m2/(m1+m2)*100"
    label       = "GetLeaderboards error rate (%)"
    return_data = true
  }

  metric_query {
    id = "m1"

    metric {
      metric_name = "URL:/twirp/code.justin.tv.commerce.pantheonrpc.Pantheon/GetLeaderboards.2xx"
      namespace   = "mako"
      period      = "300"
      stat        = "Sum"

      dimensions = {
        Service  = var.service_name
        Stage    = var.env
        Region   = var.aws_region
        Substage = "primary"
      }
    }
  }

  metric_query {
    id = "m2"

    metric {
      metric_name = "URL:/twirp/code.justin.tv.commerce.pantheonrpc.Pantheon/GetLeaderboards.5xx"
      namespace   = "mako"
      period      = "300"
      stat        = "Sum"

      dimensions = {
        Service  = var.service_name
        Stage    = var.env
        Region   = var.aws_region
        Substage = "primary"
      }
    }
  }
}

resource "aws_cloudwatch_metric_alarm" "get_bucketed_leaderboard_counts_latency_high_sev_alarm" {
  count                     = 1
  alarm_name                = "${var.env}-get-bucketed-leaderboard-counts-latency-high-sev-alarm"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "5"
  datapoints_to_alarm       = "5"
  metric_name               = "URL:/twirp/code.justin.tv.commerce.pantheonrpc.Pantheon/GetBucketedLeaderboardCounts.Latency"
  namespace                 = var.service_name
  period                    = "300"
  extended_statistic        = "p99"
  threshold                 = "0.5"
  unit                      = "Seconds"
  alarm_description         = "High sev latency alarm for the GetBucketedLeaderboardCounts API"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled           = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions             = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions                = [aws_sns_topic.pager-duty-sns.arn]

  dimensions = {
    Region   = var.aws_region
    Service  = var.service_name
    Stage    = var.env
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "get_bucketed_leaderboard_counts_status_high_sev_alarm" {
  alarm_name                = "${var.env}-get-bucketed-leaderboard-counts-status-high-sev-alarm"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "5"
  datapoints_to_alarm       = "5"
  threshold                 = "5"
  alarm_description         = "High sev status alarm for the GetLeaderboards API"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled           = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions             = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions                = [aws_sns_topic.pager-duty-sns.arn]

  metric_query {
    id          = "e1"
    expression  = "m2/(m1+m2)*100"
    label       = "GetBucketedLeaderboardCounts error rate (%)"
    return_data = true
  }

  metric_query {
    id = "m1"

    metric {
      metric_name = "URL:/twirp/code.justin.tv.commerce.pantheonrpc.Pantheon/GetBucketedLeaderboardCounts.2xx"
      namespace   = "mako"
      period      = "300"
      stat        = "Sum"

      dimensions = {
        Service  = var.service_name
        Stage    = var.env
        Region   = var.aws_region
        Substage = "primary"
      }
    }
  }

  metric_query {
    id = "m2"

    metric {
      metric_name = "URL:/twirp/code.justin.tv.commerce.pantheonrpc.Pantheon/GetBucketedLeaderboardCounts.5xx"
      namespace   = "mako"
      period      = "300"
      stat        = "Sum"

      dimensions = {
        Service  = var.service_name
        Stage    = var.env
        Region   = var.aws_region
        Substage = "primary"
      }
    }
  }
}

resource "aws_cloudwatch_metric_alarm" "get_leaderboard_entries_above_threshold_latency_high_sev_alarm" {
  count                     = 1
  alarm_name                = "${var.env}-get-leaderboard-entries-above-threshold-latency-high-sev-alarm"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "5"
  datapoints_to_alarm       = "5"
  metric_name               = "URL:/twirp/code.justin.tv.commerce.pantheonrpc.Pantheon/GetLeaderboardEntriesAboveThreshold.Latency"
  namespace                 = var.service_name
  period                    = "300"
  extended_statistic        = "p99"
  threshold                 = "0.5"
  unit                      = "Seconds"
  alarm_description         = "High sev latency alarm for the GetLeaderboardEntriesAboveThreshold API"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled           = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions             = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions                = [aws_sns_topic.pager-duty-sns.arn]

  dimensions = {
    Region   = var.aws_region
    Service  = var.service_name
    Stage    = var.env
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "get_leaderboard_entries_above_threshold_status_high_sev_alarm" {
  alarm_name                = "${var.env}-get-leaderboard-entries-above-threshold-status-high-sev-alarm"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "5"
  datapoints_to_alarm       = "5"
  threshold                 = "5"
  alarm_description         = "High sev status alarm for the GetLeaderboardEntriesAboveThreshold API"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled           = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions             = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions                = [aws_sns_topic.pager-duty-sns.arn]

  metric_query {
    id          = "e1"
    expression  = "m2/(m1+m2)*100"
    label       = "GetLeaderboardEntriesAboveThreshold error rate (%)"
    return_data = true
  }

  metric_query {
    id = "m1"

    metric {
      metric_name = "URL:/twirp/code.justin.tv.commerce.pantheonrpc.Pantheon/GetLeaderboardEntriesAboveThreshold.2xx"
      namespace   = "mako"
      period      = "300"
      stat        = "Sum"

      dimensions = {
        Service  = var.service_name
        Stage    = var.env
        Region   = var.aws_region
        Substage = "primary"
      }
    }
  }

  metric_query {
    id = "m2"

    metric {
      metric_name = "URL:/twirp/code.justin.tv.commerce.pantheonrpc.Pantheon/GetLeaderboardEntriesAboveThreshold.5xx"
      namespace   = "mako"
      period      = "300"
      stat        = "Sum"

      dimensions = {
        Service  = var.service_name
        Stage    = var.env
        Region   = var.aws_region
        Substage = "primary"
      }
    }
  }
}

resource "aws_cloudwatch_metric_alarm" "publish_event_latency_high_sev_alarm" {
  count                     = 1
  alarm_name                = "${var.env}-publish-event-latency-high-sev-alarm"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "5"
  datapoints_to_alarm       = "5"
  metric_name               = "URL:/twirp/code.justin.tv.commerce.pantheonrpc.Pantheon/PublishEvent.Latency"
  namespace                 = var.service_name
  period                    = "300"
  extended_statistic        = "p90"
  threshold                 = "1"
  unit                      = "Seconds"
  alarm_description         = "High sev latency alarm for the PublishEvent API"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled           = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions             = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions                = [aws_sns_topic.pager-duty-sns.arn]

  dimensions = {
    Region   = var.aws_region
    Service  = var.service_name
    Stage    = var.env
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "publish_event_status_high_sev_alarm" {
  alarm_name                = "${var.env}-publish-event-status-high-sev-alarm"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "5"
  datapoints_to_alarm       = "5"
  threshold                 = "5"
  alarm_description         = "High sev status alarm for the PublishEvent API"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled           = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions             = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions                = [aws_sns_topic.pager-duty-sns.arn]

  metric_query {
    id          = "e1"
    expression  = "m2/(m1+m2)*100"
    label       = "PublishEvent error rate (%)"
    return_data = true
  }

  metric_query {
    id = "m1"

    metric {
      metric_name = "URL:/twirp/code.justin.tv.commerce.pantheonrpc.Pantheon/PublishEvent.2xx"
      namespace   = "mako"
      period      = "300"
      stat        = "Sum"

      dimensions = {
        Service  = var.service_name
        Stage    = var.env
        Region   = var.aws_region
        Substage = "primary"
      }
    }
  }

  metric_query {
    id = "m2"

    metric {
      metric_name = "URL:/twirp/code.justin.tv.commerce.pantheonrpc.Pantheon/PublishEvent.5xx"
      namespace   = "mako"
      period      = "300"
      stat        = "Sum"

      dimensions = {
        Service  = var.service_name
        Stage    = var.env
        Region   = var.aws_region
        Substage = "primary"
      }
    }
  }
}

resource "aws_cloudwatch_metric_alarm" "moderate_entry_latency_high_sev_alarm" {
  count                     = 1
  alarm_name                = "${var.env}-moderate-entry-latency-high-sev-alarm"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "5"
  datapoints_to_alarm       = "5"
  metric_name               = "URL:/twirp/code.justin.tv.commerce.pantheonrpc.Pantheon/ModerateEntry.Latency"
  namespace                 = var.service_name
  period                    = "300"
  extended_statistic        = "p99"
  threshold                 = "1"
  unit                      = "Seconds"
  alarm_description         = "High sev latency alarm for the ModerateEntry API"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled           = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions             = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions                = [aws_sns_topic.pager-duty-sns.arn]

  dimensions = {
    Region   = var.aws_region
    Service  = var.service_name
    Stage    = var.env
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "moderate_entry_status_high_sev_alarm" {
  alarm_name                = "${var.env}-moderate-entry-status-high-sev-alarm"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "5"
  datapoints_to_alarm       = "5"
  threshold                 = "5"
  alarm_description         = "High sev status alarm for the ModerateEntry API"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled           = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions             = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions                = [aws_sns_topic.pager-duty-sns.arn]

  metric_query {
    id          = "e1"
    expression  = "m2/(m1+m2)*100"
    label       = "ModerateEntry error rate (%)"
    return_data = true
  }

  metric_query {
    id = "m1"

    metric {
      metric_name = "URL:/twirp/code.justin.tv.commerce.pantheonrpc.Pantheon/ModerateEntry.2xx"
      namespace   = "mako"
      period      = "300"
      stat        = "Sum"

      dimensions = {
        Service  = var.service_name
        Stage    = var.env
        Region   = var.aws_region
        Substage = "primary"
      }
    }
  }

  metric_query {
    id = "m2"

    metric {
      metric_name = "URL:/twirp/code.justin.tv.commerce.pantheonrpc.Pantheon/ModerateEntry.5xx"
      namespace   = "mako"
      period      = "300"
      stat        = "Sum"

      dimensions = {
        Service  = var.service_name
        Stage    = var.env
        Region   = var.aws_region
        Substage = "primary"
      }
    }
  }
}

# Logscan Metrics

variable "pantheon_logscan_namespace" {
  default = "pantheon"
  type    = string
}

variable "log_group" {
  description = "Cloudwatch log group in which application logs are written"
  default     = "pantheon"
  type        = string
}

variable "canary_log_group" {
  description = "Cloudwatch log group in which application logs are written"
  default     = "pantheon-canary"
  type        = string
}

resource "aws_cloudwatch_log_metric_filter" "error_scan" {
  name           = "pantheon-${var.env}-error-scan"
  pattern        = "level=error"
  log_group_name = var.log_group

  metric_transformation {
    name      = "pantheon-${var.env}-errors"
    namespace = var.pantheon_logscan_namespace
    value     = "1"
  }
}

resource "aws_cloudwatch_log_metric_filter" "canary_error_scan" {
  name           = "pantheon-canary-${var.env}-error-scan"
  pattern        = "level=error"
  log_group_name = var.canary_log_group

  metric_transformation {
    name      = "pantheon-canary-${var.env}-errors"
    namespace = var.pantheon_logscan_namespace
    value     = "1"
  }
}

resource "aws_cloudwatch_metric_alarm" "pantheon_error_warning" {
  alarm_name                = "pantheon-${var.env}-logscan-error-warning"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "7"
  metric_name               = "pantheon-${var.env}-errors"
  namespace                 = var.pantheon_logscan_namespace
  period                    = "60"
  statistic                 = "Sum"
  threshold                 = "100" // We get ~300k requests per minute, so this represents ~99.97% availibility
  alarm_description         = "This metric is used for low priority logscan error ticketing"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"
}

resource "aws_cloudwatch_metric_alarm" "pantheon_canary_error_warning" {
  alarm_name                = "pantheon-canary-${var.env}-logscan-error-warning"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "7"
  metric_name               = "pantheon-canary-${var.env}-errors"
  namespace                 = var.pantheon_logscan_namespace
  period                    = "60"
  statistic                 = "Sum"
  threshold                 = "5"
  alarm_description         = "This metric is used for low priority canary logscan error ticketing"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"
}

resource "aws_cloudwatch_metric_alarm" "pantheon_error_error" {
  alarm_name                = "pantheon-${var.env}-logscan-error-alert"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "7"
  metric_name               = "pantheon-${var.env}-errors"
  namespace                 = var.pantheon_logscan_namespace
  period                    = "60"
  statistic                 = "Sum"
  threshold                 = "200" // We get ~300k requests per minute, so this represents ~99.94% availibility
  alarm_description         = "This metric is used for higher priority logscan error ticketing"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled           = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions             = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions                = [aws_sns_topic.pager-duty-sns.arn]
}

resource "aws_cloudwatch_metric_alarm" "pantheon_canary_error_error" {
  alarm_name                = "pantheon-canary-${var.env}-logscan-error-alert"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "7"
  metric_name               = "pantheon-canary-${var.env}-errors"
  namespace                 = var.pantheon_logscan_namespace
  period                    = "60"
  statistic                 = "Sum"
  threshold                 = "10"
  alarm_description         = "This metric is used for higher priority canary logscan error ticketing"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled           = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions             = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions                = [aws_sns_topic.pager-duty-sns.arn]
}

resource "aws_cloudwatch_log_metric_filter" "panic_scan" {
  name           = "pantheon-${var.env}-panic-scan"
  pattern        = "panic"
  log_group_name = var.log_group

  metric_transformation {
    name      = "pantheon-${var.env}-panics"
    namespace = var.pantheon_logscan_namespace
    value     = "1"
  }
}

resource "aws_cloudwatch_log_metric_filter" "canary_panic_scan" {
  name           = "pantheon-canary-${var.env}-panic-scan"
  pattern        = "panic"
  log_group_name = var.canary_log_group

  metric_transformation {
    name      = "pantheon-canary-${var.env}-panics"
    namespace = var.pantheon_logscan_namespace
    value     = "1"
  }
}

resource "aws_cloudwatch_metric_alarm" "pantheon_panic_alert" {
  alarm_name                = "pantheon-${var.env}-logscan-panic-alert"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "1"
  metric_name               = "pantheon-${var.env}-panics"
  namespace                 = var.pantheon_logscan_namespace
  period                    = "60"
  statistic                 = "Sum"
  threshold                 = "1"
  alarm_description         = "This metric is used for panic ticketing"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled           = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions             = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions                = [aws_sns_topic.pager-duty-sns.arn]
}

resource "aws_cloudwatch_metric_alarm" "pantheon_canary_panic_alert" {
  alarm_name                = "pantheon-canary-${var.env}-logscan-panic-alert"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "1"
  metric_name               = "pantheon-canary-${var.env}-panics"
  namespace                 = var.pantheon_logscan_namespace
  period                    = "60"
  statistic                 = "Sum"
  threshold                 = "1"
  alarm_description         = "This metric is used for canary panic ticketing"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled           = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions             = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions                = [aws_sns_topic.pager-duty-sns.arn]
}

# SQS Metrics

module "event_queue_alarms" {
  source   = "../sqs_alarm"
  dlq_name = aws_sqs_queue.published_events_new_processing_queue_dlq.name
}

module "user_moderation_queue_alarms" {
  source   = "../sqs_alarm"
  dlq_name = aws_sqs_queue.user_moderation_queue_dlq.name
}

module "moderation_queue_alarms" {
  source   = "../sqs_alarm"
  dlq_name = aws_sqs_queue.moderation_processing_non_fifo_queue_dlq.name
}

module "moderation_queue_age_alarms" {
  source   = "../sqs_age_alarm"
  queue_name = aws_sqs_queue.moderation_processing_non_fifo_queue.name
}

module "archival_queue_alarms" {
  source   = "../sqs_alarm"
  dlq_name = aws_sqs_queue.archival_processing_queue_non_fifo_dlq.name
}

# Step Function Execution

resource "aws_cloudwatch_metric_alarm" "delete_leaderboards_stepfn_executions_failed" {
  alarm_name                = "pantheon-${var.env}-delete-leaderboards-stepfn-executions-failed"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "1"
  datapoints_to_alarm       = "1"
  metric_name               = "ExecutionsFailed"
  namespace                 = "AWS/States"
  period                    = "300"
  statistic                 = "Sum"
  threshold                 = "1"
  unit                      = "Count"
  alarm_description         = "At least one Delete Leaderboards step function execution has failed"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    StateMachineArn = module.delete_leaderboards_sfn.sfn_arn
  }
}

resource "aws_cloudwatch_metric_alarm" "delete_entries_stepfn_executions_failed" {
  alarm_name                = "pantheon-${var.env}-delete-entries-stepfn-executions-failed"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "1"
  datapoints_to_alarm       = "1"
  metric_name               = "ExecutionsFailed"
  namespace                 = "AWS/States"
  period                    = "300"
  statistic                 = "Sum"
  threshold                 = "1"
  unit                      = "Count"
  alarm_description         = "At least one Delete Entries step function execution has failed"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    StateMachineArn = module.delete_entries_sfn.sfn_arn
  }
}