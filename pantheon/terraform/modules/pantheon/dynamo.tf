module "dynamo_backup-lambda" {
  source = "../dynamo_backup_lambda"
}

resource "aws_dynamodb_table" "basic-dynamodb-table" {
  name             = "events-${var.env}"
  read_capacity    = var.events_dynamo_min_read_capacity
  write_capacity   = var.events_dynamo_min_write_capacity
  hash_key         = "leaderboard_id"
  range_key        = "event_id"
  stream_enabled   = "true"
  stream_view_type = "NEW_AND_OLD_IMAGES"

  point_in_time_recovery {
    enabled = "true"
  }

  attribute {
    name = "leaderboard_id"
    type = "S"
  }

  attribute {
    name = "event_id"
    type = "S"
  }

  attribute {
    name = "entry_key"
    type = "S"
  }

  attribute {
    name = "timestamp"
    type = "N"
  }

  lifecycle {
    ignore_changes = [read_capacity, write_capacity, global_secondary_index]
  }

  global_secondary_index {
    name            = "events-${var.env}_entry_key-index"
    hash_key        = "entry_key"
    range_key       = "timestamp"
    projection_type = "ALL"
    read_capacity   = var.events_dynamo_min_read_capacity
    write_capacity  = var.events_dynamo_min_write_capacity
  }
}

module "events_autoscaling_policy" {
  source             = "../dynamo_autoscaling"
  table_name         = "events-${var.env}"
  min_read_capacity  = var.events_dynamo_min_read_capacity
  min_write_capacity = var.events_dynamo_min_write_capacity
  max_read_capacity  = var.events_dynamo_max_read_capacity
  max_write_capacity = var.events_dynamo_max_write_capacity
  autoscaling_role   = "arn:aws:iam::${var.account_number}:role/service-role/DynamoDBAutoscaleRole"
  pager_duty_sns_arn = aws_sns_topic.pager-duty-sns.arn
}

module "events_entry_key-index_autoscaling_policy" {
  source                   = "../dynamo_index_autoscaling"
  table_name               = "events-${var.env}"
  index_name               = "events-${var.env}_entry_key-index"
  min_index_read_capacity  = var.events_dynamo_min_read_capacity
  min_index_write_capacity = var.events_dynamo_min_write_capacity
  max_index_read_capacity  = var.events_dynamo_max_read_capacity
  max_index_write_capacity = var.events_dynamo_max_write_capacity
  autoscaling_role         = "arn:aws:iam::${var.account_number}:role/service-role/DynamoDBAutoscaleRole"
}

module "events_backups" {
  source                   = "../scheduled_dynamo_backup_event"
  dynamo_backup_lambda_arn = module.dynamo_backup-lambda.lambda_arn
  table_name               = "events-${var.env}"
}

resource "aws_dynamodb_table" "moderations-table" {
  name             = "moderations-${var.env}"
  read_capacity    = var.events_dynamo_min_read_capacity
  write_capacity   = var.events_dynamo_min_write_capacity
  hash_key         = "leaderboard_set_id"
  range_key        = "entry_key"
  stream_enabled   = "true"
  stream_view_type = "NEW_AND_OLD_IMAGES"

  point_in_time_recovery {
    enabled = "true"
  }

  attribute {
    name = "leaderboard_set_id"
    type = "S"
  }

  attribute {
    name = "entry_key"
    type = "S"
  }

  attribute {
    name = "last_updated"
    type = "S"
  }

  lifecycle {
    ignore_changes = [read_capacity, write_capacity, global_secondary_index]
  }

  global_secondary_index {
    name            = "moderations-${var.env}_entry_key-index"
    hash_key        = "entry_key"
    range_key       = "last_updated"
    projection_type = "ALL"
    read_capacity   = var.events_dynamo_min_read_capacity
    write_capacity  = var.events_dynamo_min_write_capacity
  }
}

module "moderations_autoscaling_policy" {
  source             = "../dynamo_autoscaling"
  table_name         = "moderations-${var.env}"
  min_read_capacity  = var.moderation_dynamo_min_read_capacity
  min_write_capacity = var.moderation_dynamo_min_write_capacity
  autoscaling_role   = "arn:aws:iam::${var.account_number}:role/service-role/DynamoDBAutoscaleRole"
  pager_duty_sns_arn = aws_sns_topic.pager-duty-sns.arn
}

module "moderations_entry_key-index_autoscaling_policy" {
  source                   = "../dynamo_index_autoscaling"
  table_name               = "moderations-${var.env}"
  index_name               = "moderations-${var.env}_entry_key-index"
  min_index_read_capacity  = var.events_dynamo_min_read_capacity
  min_index_write_capacity = var.events_dynamo_min_write_capacity
  max_index_read_capacity  = var.events_dynamo_max_read_capacity
  max_index_write_capacity = var.events_dynamo_max_write_capacity
  autoscaling_role         = "arn:aws:iam::${var.account_number}:role/service-role/DynamoDBAutoscaleRole"
}

module "moderations_backups" {
  source                   = "../scheduled_dynamo_backup_event"
  dynamo_backup_lambda_arn = module.dynamo_backup-lambda.lambda_arn
  table_name               = "moderations-${var.env}"
}
