module "service" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//twitch-service?ref=0621ec0bd0c777d151a2bfbd549b9ea26bb1f73d"

  name          = var.service_name
  image         = aws_ecr_repository.repository.repository_url
  memory        = var.ecs_memory
  canary_memory = var.ecs_canary_memory
  logging       = var.logging
  cpu           = var.ecs_cpu
  canary_cpu    = var.ecs_canary_cpu
  alb_port      = var.alb_port
  command       = var.command

  counts = {
    min = var.min_tasks
    max = var.max_tasks
  }

  env_vars = [
    {
      name  = "ENVIRONMENT"
      value = var.env
    },
  ]

  port = 8000
  dns  = module.privatelink-cert.dns

  healthcheck    = "/ping"
  fargate_platform_version = ""
  cluster        = module.cluster.ecs_name
  security_group = var.sg_id
  region         = var.region
  vpc_id         = var.vpc_id
  environment    = var.env
  subnets        = var.private_subnets
}


