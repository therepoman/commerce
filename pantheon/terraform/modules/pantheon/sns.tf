resource "aws_sns_topic" "event-topic" {
  name = "${var.service_name}-${var.env}-events"
}

resource "aws_sns_topic_policy" "default" {
  arn = aws_sns_topic.event-topic.arn

  policy = data.aws_iam_policy_document.sns-topic-policy.json
}

data "aws_iam_policy_document" "sns-topic-policy" {
  policy_id = "__default_policy_ID"

  statement {
    actions = [
      "SNS:Subscribe",
      "SNS:SetTopicAttributes",
      "SNS:Receive",
      "SNS:ListSubscriptionsByTopic",
      "SNS:GetTopicAttributes",
    ]

    effect = "Allow"

    principals {
      type = "AWS"

      identifiers = [
        "arn:aws:iam::${var.payday-aws-account-id}:root",
        "arn:aws:iam::${var.subscriptions-aws-account-id}:root",
        "arn:aws:iam::${var.percy-aws-account-id}:root",
      ]
    }

    resources = [
      aws_sns_topic.event-topic.arn,
    ]

    sid = "__default_statement_ID"
  }
}
