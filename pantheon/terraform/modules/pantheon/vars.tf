# AWS general Config
variable "aws_profile" {
  description = "The AWS Named Profile to use when running terraform and creating AWS resources"
}

variable "region" {
  default     = "us-west-2"
  description = "The AWS Region for the resources to live in"
}

variable "private_cidr_blocks" {
  description = "The private subnet cidr blocks for this stage"
  type        = list(string)
}

variable "events_dynamo_min_read_capacity" {
  description = "Read capacity for the dynamo tables"
  default     = "50"
}

variable "events_dynamo_min_write_capacity" {
  description = "Write capacity for the dynamo table"
  default     = "5"
}

variable "events_dynamo_max_read_capacity" {
  description = "Read capacity for the dynamo table"
  default     = "10000"
}

variable "events_dynamo_max_write_capacity" {
  description = "Write capacity for the dynamo table"
  default     = "10000"
}

variable "moderation_dynamo_min_read_capacity" {
  description = "Read capacity for the moderation dynamo table"
  default     = "50"
}

variable "moderation_dynamo_min_write_capacity" {
  description = "Write capacity for the moderation dynamo table"
  default     = "50"
}

variable "aws_region" {
  description = "AWS region to operate in."
  default     = "us-west-2"
}

variable "service" {
  description = "Name of the service being built. Should probably leave this as the default"
  default     = "commerce/pantheon"
}

variable "service_name" {
  description = "Name of the service being built, without the 'commerce/' part."
  default     = "pantheon"
}

variable "logging" {
  default     = "awslogs"
  description = "The logging driver to use (e.g., json, awslogs)"
}

variable "env" {
  description = "Whether it's production, dev, etc"
}

variable "account_name" {
  description = "Name of the AWS account"
}

variable "account_number" {
  description = "AWS account number"
}

variable "owner" {
  description = "Owner of the account"
}

# VPC Config
variable "vpc_id" {
  description = "Id of the systems-created VPC"
}

variable "private_subnets" {
  description = "Comma-separated list of private subnets"
}

variable "sg_id" {
  description = "Id of the security group"
}

variable "ecs_instance_type" {
  description = "ECS instance type"
}

variable "elasticache_instance_type" {
  description = "Describes the type of instance to be used in our elasticache cluster"
  default     = "cache.m3.medium"
}

variable "elasticache_auto_failover" {
  description = "Describes the type of failover behavior for the elasticache groups"
  default     = true
}

variable "elasticache_nodes" {
  description = "The number of nodes in elasticache"
  default     = 3
}

variable "redis_port" {
  description = "The port used by the redis server"
  default     = 6379
}

variable "alb_port" {
  description = "The port the load balancer is accepting traffic from."
  default     = 80
}

variable "ecs_ec2_backed_min_instances" {
  default = 3
}

variable "ecs_cpu" {
  default     = 512
  description = "How many CPU units to use"
}

variable "ecs_memory" {
  default = 512
}

variable "ecs_canary_cpu" {
  default     = 512
  description = "How many CPU units to use for canary"
}

variable "ecs_canary_memory" {
  default = 512
}

variable "min_tasks" {
  default = 3
}

variable "max_tasks" {
  default = 3
}

variable "command" {
  type    = list(string)
  default = []
}

variable "payday-aws-account-id" {
  description = "The aws account used by payday"
  default     = "021561903526"
}

variable "subscriptions-aws-account-id" {
  description = "The id of the subscriptions aws account"
  default     = "958836777662" # twitch-subs-dev
}

variable "percy-aws-account-id" {
  description = "AWS account ID for the Hype train service - Percy"
  default     = "999515204624" # twitch-percy-aws-devo
}

variable "tenant_elasticache_instance_type" {
  description = "Describes the type of instance to be used in the elasticache cluster for each tenant"
  default     = "cache.m3.medium"
}

variable "tenant_elasticache_nodes" {
  description = "The number of nodes in elasticache for each tenant"
  default     = 3
}

variable "user_moderation_sns_arn" {
  description = "ARN of the Clue User Moderation SNS topic"
}

variable "privatelink_whitelist" {
  description = "Whitelist of accounts that can privatelink with Pantheon"
  type        = list(string)
}

variable "sandstorm_arn" {
  description = "The sandstorm arn to attach the role to"
}