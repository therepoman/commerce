module "privatelink-zone" {
  source      = "git::git+ssh://git@git.xarth.tv/subs/terracode//privatelink-zone?ref=0621ec0bd0c777d151a2bfbd549b9ea26bb1f73d"
  name        = "pantheon"
  environment = var.env == "prod" ? "production" : var.env
}

module "privatelink-cert" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//privatelink-cert?ref=0621ec0bd0c777d151a2bfbd549b9ea26bb1f73d"

  name        = "pantheon"
  environment = var.env == "prod" ? "production" : var.env
  zone_id     = module.privatelink-zone.zone_id
  alb_dns     = module.service.dns
}

module "privatelink" {
  source           = "git::git+ssh://git@git.xarth.tv/subs/terracode//privatelink?ref=0621ec0bd0c777d151a2bfbd549b9ea26bb1f73d"
  name             = "pantheon"
  region           = var.aws_region
  environment      = var.env
  alb_dns          = module.service.dns
  cert_arn         = module.privatelink-cert.arn
  vpc_id           = var.vpc_id
  subnets          = split(",", var.private_subnets)
  whitelisted_arns = var.privatelink_whitelist
}

// privatelinks with downstreams

locals {
  pubsub-vpce = {
    staging = "com.amazonaws.vpce.us-west-2.vpce-svc-0a80bc39593294cc4"
    prod = "com.amazonaws.vpce.us-west-2.vpce-svc-044f01799c2129051"
  }
  pubsub-dns = {
    staging = "darklaunch.pubsub-broker.twitch.a2z.com"
    prod = "prod.pubsub-broker.twitch.a2z.com"
  }
  user-service-vpce = {
    staging = "com.amazonaws.vpce.us-west-2.vpce-svc-05dd975d62ccc04fe"
    prod = "com.amazonaws.vpce.us-west-2.vpce-svc-044e493c84b7dc984"
  }
  user-service-dns = {
    staging = "dev.users-service.twitch.a2z.com"
    prod = "prod.users-service.twitch.a2z.com"
  }
}

module "privatelink-ldap" {
  source          = "git::git+ssh://git@git.xarth.tv/subs/terracode//vpc-endpoint?ref=0621ec0bd0c777d151a2bfbd549b9ea26bb1f73d"
  name            = "ldap-a2z"
  endpoint        = "com.amazonaws.vpce.us-west-2.vpce-svc-0437151f68c61b808"
  security_groups = [var.sg_id]
  vpc_id          = var.vpc_id
  subnets         = split(",", var.private_subnets)
  dns             = "ldap.twitch.a2z.com"
}

module "privatelink-pubsub-broker" {
  source          = "git::git+ssh://git@git.xarth.tv/subs/terracode//vpc-endpoint?ref=0621ec0bd0c777d151a2bfbd549b9ea26bb1f73d"
  name            = "pubsub-broker-a2z"
  endpoint        = local.pubsub-vpce[var.env]
  security_groups = [var.sg_id]
  vpc_id          = var.vpc_id
  subnets         = split(",", var.private_subnets)
  dns             = local.pubsub-dns[var.env]
}

module "privatelink-user-service" {
  source          = "git::git+ssh://git@git.xarth.tv/subs/terracode//vpc-endpoint?ref=0621ec0bd0c777d151a2bfbd549b9ea26bb1f73d"
  name            = "user-service-a2z"
  endpoint        = local.user-service-vpce[var.env]
  security_groups = [var.sg_id]
  vpc_id          = var.vpc_id
  subnets         = split(",", var.private_subnets)
  dns             = local.user-service-dns[var.env]
}