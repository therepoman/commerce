module "cluster" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//cluster?ref=0621ec0bd0c777d151a2bfbd549b9ea26bb1f73d"

  name           = "${var.service_name}-cluster"
  min            = var.ecs_ec2_backed_min_instances
  vpc_id         = var.vpc_id
  instance_type  = var.ecs_instance_type
  environment    = var.env
  subnets        = var.private_subnets
  security_group = var.sg_id
}

//creates the lambda, and the SNS topic
module "upgrade_complete" {
  source = "git::git+ssh://git@git.xarth.tv/edge/upgrade-complete//terraform?ref=1171046271eaed04e01a77afd72896c0b7d4c94e"
  aws_account_id = "12345"
  function_zip_path = "${path.module}/files/upgrade-complete.zip"
}

//adds a lifecycle hook to an already existing Autoscaling Group.
resource "aws_autoscaling_lifecycle_hook" "on_instance_terminate_lifecycle_hook" {
  autoscaling_group_name  = module.cluster.ecs_cluster_asg_name
  lifecycle_transition    = "autoscaling:EC2_INSTANCE_TERMINATING"
  name                    = "on_instance_terminate_upgrade_complete_hook"
  notification_target_arn = module.upgrade_complete.on_terminate_lifecycle_hook_sns_topic_arn
  role_arn                = aws_iam_role.lifecycle_hook_sns_write_role.arn
  heartbeat_timeout       = 3600
}

//allows the Lifecycle Hook to publish messages to SNS
resource "aws_iam_role" "lifecycle_hook_sns_write_role" {
  name = "lifecycle-hook-sns-write-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "autoscaling.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" autoscaling_notification_sns_write_policy {
  role       = aws_iam_role.lifecycle_hook_sns_write_role.id
  policy_arn = "arn:aws:iam::aws:policy/service-role/AutoScalingNotificationAccessRole"
}

# Requirement from Twitch Security (https://wiki.xarth.tv/display/SEC/AWS+Systems+Manager+Campaign)
resource "aws_iam_role_policy_attachment" "ecs_ssm_policy_attachment" {
  role       = module.cluster.ecs_instance_role_name
  policy_arn = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
}