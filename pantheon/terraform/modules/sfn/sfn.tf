data "aws_iam_policy_document" "sfn_assume_role_policy_document" {
  statement {
    actions = [
      "sts:AssumeRole",
    ]

    principals {
      type = "Service"

      identifiers = [
        "states.${var.aws_region}.amazonaws.com",
      ]
    }
  }
}

data "aws_iam_policy_document" "lambda_execution_policy_document" {
  version = "2012-10-17"

  statement {
    actions = [
      "lambda:InvokeFunction",
      "states:StartExecution",
    ]

    effect = "Allow"

    resources = [
      "*",
    ]
  }
}

resource "aws_iam_role_policy" "lambda_execution" {
  name   = "lambda_execution"
  role   = aws_iam_role.sfn_role.id
  policy = data.aws_iam_policy_document.lambda_execution_policy_document.json
}

resource "aws_iam_role" "sfn_role" {
  name = var.sfn_role
  assume_role_policy = data.aws_iam_policy_document.sfn_assume_role_policy_document.json
}

resource "aws_sfn_state_machine" "sfn" {
  name       = var.sfn_name
  role_arn   = aws_iam_role.sfn_role.arn
  definition = var.sfn_definition
}
