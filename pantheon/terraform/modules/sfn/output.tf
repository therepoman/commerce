output "sfn_arn" {
  value = aws_sfn_state_machine.sfn.id
}

output "sfn_role_name" {
  value = aws_iam_role.sfn_role.name
}