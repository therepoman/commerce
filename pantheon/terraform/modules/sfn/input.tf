variable "sfn_name" {}

variable "sfn_role" {}

variable "sfn_definition" {}

variable "aws_region" {
  description = "AWS region to operate in."
  default     = "us-west-2"
}
