locals {
  env = "dev"
}

resource "aws_sqs_queue" "published_events_new_processing_queue_dlq" {
  name                      = "published-events-new-processing-queue-dlq-${local.env}"
  message_retention_seconds = 1209600                                                  // 14 days
}

resource "aws_sqs_queue" "published_events_new_processing_queue" {
  name           = "published-events-new-processing-queue-${local.env}"
  redrive_policy = "{\"deadLetterTargetArn\":\"${aws_sqs_queue.published_events_new_processing_queue_dlq.arn}\",\"maxReceiveCount\":4}"
}

resource "aws_sqs_queue" "archival_processing_queue_dlq" {
  name       = "archival-processing-queue-dlq-${local.env}"
  fifo_queue = false
}

resource "aws_sqs_queue" "archival_processing_queue" {
  name           = "archival-processing-queue-${local.env}"
  redrive_policy = "{\"deadLetterTargetArn\":\"${aws_sqs_queue.archival_processing_queue_dlq.arn}\",\"maxReceiveCount\":4}"
}

resource "aws_sqs_queue" "moderation_processing_non_fifo_queue_dlq" {
  name = "moderation-processing-non-fifo-queue-dlq-${local.env}"
}

resource "aws_sqs_queue" "moderation_processing_non_fifo_queue" {
  name           = "moderation-processing-non-fifo-queue-${local.env}"
  redrive_policy = "{\"deadLetterTargetArn\":\"${aws_sqs_queue.moderation_processing_non_fifo_queue_dlq.arn}\",\"maxReceiveCount\":4}"
}

resource "aws_sns_topic" "event_topic" {
  name = "pantheon-${local.env}-events"
}

terraform {
  backend "s3" {
    bucket  = "pantheon-local-tcs"
    key     = "tfstate/commerce/pantheon/terraform/local"
    region  = "us-west-2"
    profile = "pantheon-devo"
  }
}
