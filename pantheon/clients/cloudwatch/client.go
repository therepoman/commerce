package cloudwatch

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	cw "github.com/aws/aws-sdk-go/service/cloudwatch"
	"github.com/aws/aws-sdk-go/service/cloudwatch/cloudwatchiface"
)

const (
	defaultRegion = "us-west-2"
)

func newDefaultConfig() *aws.Config {
	return &aws.Config{
		Region: aws.String(defaultRegion),
	}
}

func NewDefaultClient() (cloudwatchiface.CloudWatchAPI, error) {
	// Create the service's client with the session.
	sess, err := session.NewSession()
	if err != nil {
		return nil, err
	}
	c := cw.New(sess, newDefaultConfig())
	return c, nil
}
