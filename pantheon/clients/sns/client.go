package sns

import (
	"context"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sns"
)

const (
	defaultRegion = "us-west-2"
)

type SDKClientWrapper struct {
	SDKClient *sns.SNS
}

func NewDefaultConfig() *aws.Config {
	return &aws.Config{
		Region: aws.String(defaultRegion),
	}
}

func NewDefaultClient() (Client, error) {
	sess, err := session.NewSession()
	if err != nil {
		return nil, err
	}
	return &SDKClientWrapper{
		SDKClient: sns.New(sess, NewDefaultConfig()),
	}, nil
}

// TODO: Add hystrix
func (c *SDKClientWrapper) Publish(ctx context.Context, topicARN string, message string) error {
	publishInput := &sns.PublishInput{
		TopicArn: aws.String(topicARN),
		Message:  aws.String(message),
	}
	_, err := c.SDKClient.PublishWithContext(ctx, publishInput)
	return err
}
