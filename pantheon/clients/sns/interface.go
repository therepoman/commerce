package sns

import (
	"context"
)

type Client interface {
	Publish(ctx context.Context, topicARN string, message string) error
}
