package redis

import (
	"context"
	"time"

	"code.justin.tv/chat/rediczar"
	"code.justin.tv/chat/rediczar/redefault"
	lock "github.com/bsm/redis-lock"
	"github.com/go-redis/redis"
)

type ReadSettings struct {
	ConsistentRead bool
}

type Client interface {
	// These are provided directly by rediczar
	ConfigSet(ctx context.Context, parameter, value string) (string, error)
	Del(ctx context.Context, keys ...string) (int64, error)
	Dump(ctx context.Context, key string, rs ReadSettings) (string, error)
	Exists(ctx context.Context, keys []string, rs ReadSettings) (int64, error)
	ExpireAt(ctx context.Context, key string, tm time.Time) (bool, error)
	Get(ctx context.Context, key string, rs ReadSettings) (string, error)
	Ping(ctx context.Context) (string, error)
	Restore(ctx context.Context, key string, ttl time.Duration, value string) (string, error)
	SAdd(ctx context.Context, key string, members ...interface{}) (int64, error)
	Scan(ctx context.Context, cursor uint64, match string, count int64, rs ReadSettings) ([]string, uint64, error)
	Set(ctx context.Context, key string, value interface{}, expiration time.Duration) (string, error)
	SMembers(ctx context.Context, key string, rs ReadSettings) ([]string, error)
	SRem(ctx context.Context, key string, members ...interface{}) (int64, error)
	TTL(ctx context.Context, key string, rs ReadSettings) (time.Duration, error)
	ZAdd(ctx context.Context, key string, members ...*redis.Z) (int64, error)
	ZCard(ctx context.Context, key string, rs ReadSettings) (int64, error)
	ZRem(ctx context.Context, key string, members ...interface{}) (int64, error)
	ZCount(ctx context.Context, key, min string, max string, rs ReadSettings) (int64, error)
	ZRevRangeByScoreWithScore(ctx context.Context, key, min, max string, limit, offset int64, rs ReadSettings) ([]redis.Z, error)
	ZRevRank(ctx context.Context, key, member string, rs ReadSettings) (int64, error)
	ZRevRangeWithScores(ctx context.Context, key string, start, stop int64, rs ReadSettings) ([]redis.Z, error)
	ZScore(ctx context.Context, key, member string, rs ReadSettings) (float64, error)

	// These are not provided directly by rediczar
	// rediczar does not provide this at all
	PSubscribe(channels ...string) *redis.PubSub

	// Lock client directly relies on the old-style (non-wrapped) method signatures
	GetLockingClient() lock.RedisClient
}

type client struct {
	writeClient *rediczar.Client
	// The rediczar client doesn't wrap specific commands, keep the non-wrapped client to be able to issue these
	baseClient *redis.Client
	readClient *rediczar.Client
}

func NewClient(writeNode string, readNode string) *client {
	// Add rate limiting + fixed pool size + other smart defaults
	writeClient := redefault.NewSingleNodeClient(writeNode, nil)

	rediczarWriteClient := &rediczar.Client{
		Commands: &rediczar.PrefixedCommands{Redis: writeClient},
	}

	// Add rate limiting + fixed pool size + other smart defaults
	readClient := redefault.NewSingleNodeClient(readNode, nil)

	rediczarReadClient := &rediczar.Client{
		Commands: &rediczar.PrefixedCommands{Redis: readClient},
	}

	return &client{
		writeClient: rediczarWriteClient,
		baseClient:  writeClient,
		readClient:  rediczarReadClient,
	}
}

func (c *client) GetLockingClient() lock.RedisClient {
	return c.baseClient
}

func (c *client) ConfigSet(ctx context.Context, parameter, value string) (string, error) {
	return c.writeClient.ConfigSet(ctx, parameter, value)
}

func (c *client) Del(ctx context.Context, keys ...string) (int64, error) {
	return c.writeClient.Del(ctx, keys...)
}

func (c *client) Dump(ctx context.Context, key string, rs ReadSettings) (string, error) {
	return c.pickReadClient(rs).Dump(ctx, key)
}

func (c *client) Exists(ctx context.Context, keys []string, rs ReadSettings) (int64, error) {
	return c.pickReadClient(rs).Exists(ctx, keys...)
}

func (c *client) ExpireAt(ctx context.Context, key string, tm time.Time) (bool, error) {
	return c.writeClient.ExpireAt(ctx, key, tm)
}

func (c *client) Get(ctx context.Context, key string, rs ReadSettings) (string, error) {
	return c.pickReadClient(rs).Get(ctx, key)
}

func (c *client) Ping(ctx context.Context) (string, error) {
	return c.pickReadClient(ReadSettings{}).Ping(ctx)
}

func (c *client) PSubscribe(channels ...string) *redis.PubSub {
	return c.baseClient.PSubscribe(channels...)
}

func (c *client) Restore(ctx context.Context, key string, ttl time.Duration, value string) (string, error) {
	return c.writeClient.Restore(ctx, key, ttl, value)
}

func (c *client) SAdd(ctx context.Context, key string, members ...interface{}) (int64, error) {
	return c.writeClient.SAdd(ctx, key, members...)
}

func (c *client) Scan(ctx context.Context, cursor uint64, match string, count int64, rs ReadSettings) ([]string, uint64, error) {
	return c.pickReadClient(rs).Scan(ctx, cursor, match, count)
}

func (c *client) Set(ctx context.Context, key string, value interface{}, expiration time.Duration) (string, error) {
	return c.writeClient.Set(ctx, key, value, expiration)
}

func (c *client) SMembers(ctx context.Context, key string, rs ReadSettings) ([]string, error) {
	return c.pickReadClient(rs).SMembers(ctx, key)
}

func (c *client) SRem(ctx context.Context, key string, members ...interface{}) (int64, error) {
	return c.writeClient.SRem(ctx, key, members...)
}

func (c *client) TTL(ctx context.Context, key string, rs ReadSettings) (time.Duration, error) {
	return c.pickReadClient(rs).TTL(ctx, key)
}

func (c *client) ZAdd(ctx context.Context, key string, members ...*redis.Z) (int64, error) {
	return c.writeClient.ZAdd(ctx, key, members...)
}

func (c *client) ZCard(ctx context.Context, key string, rs ReadSettings) (int64, error) {
	return c.pickReadClient(rs).ZCard(ctx, key)
}

func (c *client) ZRem(ctx context.Context, key string, members ...interface{}) (int64, error) {
	return c.writeClient.ZRem(ctx, key, members...)
}

func (c *client) ZRevRank(ctx context.Context, key, member string, rs ReadSettings) (int64, error) {
	return c.pickReadClient(rs).ZRevRank(ctx, key, member)
}

func (c *client) ZRevRangeWithScores(ctx context.Context, key string, start, stop int64, rs ReadSettings) ([]redis.Z, error) {
	return c.pickReadClient(rs).ZRevRangeWithScores(ctx, key, start, stop)
}

func (c *client) ZRevRangeByScoreWithScore(ctx context.Context, key, min, max string, limit, offset int64, rs ReadSettings) ([]redis.Z, error) {
	return c.pickReadClient(rs).ZRevRangeByScoreWithScores(ctx, key, &redis.ZRangeBy{
		Min:    min,
		Max:    max,
		Offset: offset,
		Count:  limit,
	})
}

func (c *client) ZScore(ctx context.Context, key, member string, rs ReadSettings) (float64, error) {
	return c.pickReadClient(rs).ZScore(ctx, key, member)
}

func (c *client) ZCount(ctx context.Context, key, min string, max string, rs ReadSettings) (int64, error) {
	return c.pickReadClient(rs).ZCount(ctx, key, min, max)
}

func (c *client) pickReadClient(rs ReadSettings) *rediczar.Client {
	if rs.ConsistentRead {
		return c.writeClient
	}
	return c.readClient
}
