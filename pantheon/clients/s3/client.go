package s3

import (
	"context"
	"io"
	"io/ioutil"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	log "github.com/sirupsen/logrus"
)

const (
	defaultEndpoint         = "s3-us-west-2.amazonaws.com"
	defaultRegion           = "us-west-2"
	defaultS3ForcePathStyle = true
)

type SDKClientWrapper struct {
	SDKClient *s3.S3
}

func NewDefaultConfig() *aws.Config {
	return &aws.Config{
		Region:           aws.String(defaultRegion),
		Endpoint:         aws.String(defaultEndpoint),
		S3ForcePathStyle: aws.Bool(defaultS3ForcePathStyle),
	}
}

func NewDefaultClient() (Client, error) {
	sess, err := session.NewSession()
	if err != nil {
		return nil, err
	}
	return &SDKClientWrapper{
		SDKClient: s3.New(sess, NewDefaultConfig()),
	}, nil
}

type GetFileResponse struct {
	File   []byte
	Exists bool
}

func fileNotFoundResponse() GetFileResponse {
	return GetFileResponse{
		Exists: false,
	}
}

// TODO: Add hystrix
func (c *SDKClientWrapper) GetFile(ctx context.Context, bucket string, key string) (GetFileResponse, error) {
	input := &s3.GetObjectInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(key),
	}
	out, err := c.SDKClient.GetObjectWithContext(ctx, input)
	if err != nil {
		if awsErr, ok := err.(awserr.Error); ok && awsErr.Code() == s3.ErrCodeNoSuchKey {
			return fileNotFoundResponse(), nil
		}
		return fileNotFoundResponse(), err
	}

	defer func() {
		err = out.Body.Close()
		if err != nil {
			log.Error("Failed to close S3 file", err)
		}
	}()

	bytes, err := ioutil.ReadAll(out.Body)
	if err != nil {
		return fileNotFoundResponse(), err
	}

	return GetFileResponse{
		File:   bytes,
		Exists: true,
	}, nil
}

// TODO: Add hystrix
func (c *SDKClientWrapper) PutFile(ctx context.Context, bucket string, key string, file io.ReadSeeker) error {
	input := &s3.PutObjectInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(key),
		Body:   file,
	}
	_, err := c.SDKClient.PutObjectWithContext(ctx, input)
	return err
}

func (c *SDKClientWrapper) ListFileNames(bucket string, prefix string) ([]string, error) {
	input := &s3.ListObjectsInput{
		Bucket: aws.String(bucket),
		Prefix: aws.String(prefix),
	}
	out, err := c.SDKClient.ListObjects(input)
	if err != nil {
		return nil, err
	}

	allFiles := make([]string, 0)
	for _, obj := range out.Contents {
		allFiles = append(allFiles, *obj.Key)
	}
	return allFiles, nil
}

func (c *SDKClientWrapper) DeleteFiles(bucket string, keys ...string) error {
	objectIdentifiers := make([]*s3.ObjectIdentifier, len(keys))
	for i, key := range keys {
		objectIdentifiers[i] = &s3.ObjectIdentifier{
			Key: aws.String(key),
		}
	}

	input := &s3.DeleteObjectsInput{
		Bucket: aws.String(bucket),
		Delete: &s3.Delete{
			Objects: objectIdentifiers,
		},
	}
	_, err := c.SDKClient.DeleteObjects(input)
	return err
}
