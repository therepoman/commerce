package s3

import (
	"context"
	"io"
)

type Client interface {
	GetFile(ctx context.Context, bucket string, key string) (GetFileResponse, error)
	PutFile(ctx context.Context, bucket string, key string, file io.ReadSeeker) error
	DeleteFiles(bucket string, keys ...string) error
	ListFileNames(bucket string, prefix string) ([]string, error)
}
