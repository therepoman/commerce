package tenant

import (
	dynamo_event "code.justin.tv/commerce/pantheon/backend/dynamo/event"
	dynamo_moderation "code.justin.tv/commerce/pantheon/backend/dynamo/moderation"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/identifier"
	"code.justin.tv/commerce/pantheon/clients/lock"
	"code.justin.tv/commerce/pantheon/clients/redis"
	"code.justin.tv/commerce/pantheon/config"
)

type Landlord interface {
	GetTenant(domain identifier.Domain) Tenant
	GetDefaultTenant() Tenant
	GetTenants() map[string]Tenant
}

type Tenant struct {
	RedisClient            redis.Client
	PDMSRedisClient        redis.Client
	StandardLockingClient  lock.LockingClient
	EventDAO               dynamo_event.DAO
	ModerationDAO          dynamo_moderation.DAO
	Config                 config.TenantConfig
	MaxPublishDelaySeconds int
}

type landlord struct {
	domainToTenant map[string]Tenant
	defaultTenant  Tenant
}

func NewLandlord(domainToTenant map[string]Tenant, defaultTenant Tenant) Landlord {
	return &landlord{
		domainToTenant: domainToTenant,
		defaultTenant:  defaultTenant,
	}
}

func (l *landlord) GetTenant(domain identifier.Domain) Tenant {
	tenant, ok := l.domainToTenant[string(domain)]
	if ok {
		return tenant
	}
	return l.defaultTenant
}

func (l *landlord) GetDefaultTenant() Tenant {
	return l.defaultTenant
}

func (l *landlord) GetTenants() map[string]Tenant {
	return l.domainToTenant
}
