package sfn

import (
	"context"
	"encoding/json"

	"code.justin.tv/commerce/gogogadget/pointers"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sfn"
	"github.com/aws/aws-sdk-go/service/sfn/sfniface"
	"github.com/pkg/errors"
)

const (
	defaultRegion = "us-west-2"
)

type SDKClientWrapper struct {
	SDKClient sfniface.SFNAPI
}

func NewDefaultConfig() *aws.Config {
	return &aws.Config{
		Region: aws.String(defaultRegion),
	}
}

func NewDefaultClient() (Client, error) {
	sess, err := session.NewSession()
	if err != nil {
		return nil, err
	}
	return &SDKClientWrapper{
		SDKClient: sfn.New(sess, NewDefaultConfig()),
	}, nil
}

func (c *SDKClientWrapper) Execute(ctx context.Context, stateMachineARN string, executionName string, executionInput interface{}) error {
	inputBytes, err := json.Marshal(executionInput)
	if err != nil {
		return errors.Wrapf(err, "failed to marshall state machine execution input. stateMachineARN: %s executionInput: %+v", stateMachineARN, executionInput)
	}

	_, err = c.SDKClient.StartExecutionWithContext(ctx, &sfn.StartExecutionInput{
		Input:           pointers.StringP(string(inputBytes)),
		Name:            &executionName,
		StateMachineArn: &stateMachineARN,
	})

	return err
}
