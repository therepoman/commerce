package sqs

import (
	"context"

	"github.com/aws/aws-sdk-go/service/sqs"
)

type Client interface {
	SendMessageWithDeduplication(ctx context.Context, queueURL string, msgDedupeID string, msgGroupID string, msgBody string) error
	SendMessage(ctx context.Context, queueURL string, msgBody string) error
	SendMessageWithDelay(ctx context.Context, queueURL string, msgBody string, delaySeconds int64) error
	ReceiveMessages(ctx context.Context, queueURL string, maxReceivedMessages int) ([]*sqs.Message, error)
	DeleteMessages(ctx context.Context, queueURL string, messages []*sqs.Message) error
}
