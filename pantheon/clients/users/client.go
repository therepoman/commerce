package users

import (
	"context"
	"time"

	"code.justin.tv/foundation/twitchclient"
	users "code.justin.tv/web/users-service/client/usersclient_internal"
	"github.com/cactus/go-statsd-client/statsd"
	"golang.org/x/time/rate"
)

type Client interface {
	GetUserByID(ctx context.Context, userID string) (*UserInfo, error)
}

type UserInfo struct {
	ID          string
	Login       *string
	DisplayName *string
}

type clientImpl struct {
	usersService users.InternalClient
	rateLimiter  *rate.Limiter
}

const (
	userServiceConcurrencyLimit      = 20
	userServiceConcurrencySpikeLimit = 50
	userServiceTimeout               = 2 * time.Second
	rateLimiterTimeout               = 5 * time.Second
)

func NewClient(endpoint string, statter statsd.Statter) (Client, error) {
	if statter == nil {
		statter = &statsd.NoopClient{}
	}
	usersService, err := users.NewClient(twitchclient.ClientConf{
		Host:      endpoint,
		Transport: twitchclient.TransportConf{},
		Stats:     statter,
	})
	if err != nil {
		return nil, err
	}
	return &clientImpl{
		usersService: usersService,
		rateLimiter:  rate.NewLimiter(userServiceConcurrencyLimit, userServiceConcurrencySpikeLimit),
	}, nil
}

func (c *clientImpl) GetUserByID(ctx context.Context, userID string) (*UserInfo, error) {
	err := c.rateLimiter.Wait(ctx)
	if err != nil {
		return nil, err
	}

	ctx, cancel := context.WithTimeout(ctx, userServiceTimeout)
	defer cancel()

	resp, err := c.usersService.GetUserByID(ctx, userID, nil)
	if err != nil {
		return nil, err
	}
	if resp == nil {
		return nil, nil
	}

	return &UserInfo{
		ID:          resp.ID,
		Login:       resp.Login,
		DisplayName: resp.Displayname,
	}, nil
}

func (c *clientImpl) rateLimit(ctx context.Context) error {
	ctx, cancel := context.WithTimeout(ctx, rateLimiterTimeout)
	defer cancel()
	return c.rateLimiter.Wait(ctx)
}
