package lock

import (
	"errors"
	"time"

	log "code.justin.tv/commerce/logrus"
	lock "github.com/bsm/redis-lock"
)

const (
	defaultLockTimeout = 5 * time.Second
	defaultWaitTimeout = 1 * time.Second
	defaultWaitRetry   = 50 * time.Millisecond

	keyField = "key"
)

type redisLockingClient struct {
	RedisClient lock.RedisClient
	LockOptions *lock.LockOptions
}

func NewRedisLockingClient(redisClient lock.RedisClient, lockOptions *lock.LockOptions) LockingClient {
	return &redisLockingClient{
		RedisClient: redisClient,
		LockOptions: lockOptions,
	}
}

func DefaultLockOptions() *lock.LockOptions {
	return &lock.LockOptions{
		LockTimeout: defaultLockTimeout,
		WaitTimeout: defaultWaitTimeout,
		WaitRetry:   defaultWaitRetry,
	}
}

func (c *redisLockingClient) ObtainLock(key string) (Lock, error) {
	log.WithField(keyField, key).Info("[REDIS-LOCK] Attempting to obtain lock")

	rl, err := lock.ObtainLock(c.RedisClient, key, c.LockOptions)
	if err != nil {
		log.WithField(keyField, key).Info("[REDIS-LOCK] Error contacting redis for lock")
		return nil, err
	}

	if rl == nil {
		log.WithField(keyField, key).Info("[REDIS-LOCK] Error obtaining lock")
		return nil, errors.New("failed to obtain redis lock")
	}

	log.WithField(keyField, key).Info("[REDIS-LOCK] Obtained lock")
	return &redisLock{
		key:       key,
		RedisLock: rl,
	}, nil
}

type redisLock struct {
	key       string
	RedisLock *lock.Lock
}

func (l *redisLock) Unlock() error {
	log.WithField(keyField, l.key).Info("[REDIS-LOCK] Released lock")
	return l.RedisLock.Unlock()
}
