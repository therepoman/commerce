package lock

type noOpLockingClient struct {
}

func NewNoOpLockingClient() LockingClient {
	return &noOpLockingClient{}
}

func (c *noOpLockingClient) ObtainLock(key string) (Lock, error) {
	return &noOpLock{}, nil
}

type noOpLock struct {
}

func (l *noOpLock) Unlock() error {
	return nil
}
