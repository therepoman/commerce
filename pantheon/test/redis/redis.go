package redis

import (
	"context"
	"flag"
	"fmt"

	"code.justin.tv/commerce/pantheon/clients/lock"
	rd "code.justin.tv/commerce/pantheon/clients/redis"
	"github.com/alicebob/miniredis"
)

var useLocal = flag.Bool("redis.local", false, "Informs test to use local redis running on port 6379")

func WithRedis(f func(usingLocal bool, redisClient rd.Client, lockingClient lock.LockingClient)) func() {
	return func() {
		var client rd.Client
		var lockingClient lock.LockingClient
		if *useLocal {
			fmt.Println("Using local redis...")
			client = rd.NewClient("localhost:6379", "localhost:6379")

			lockingClient = lock.NewRedisLockingClient(client.GetLockingClient(), lock.DefaultLockOptions())
		} else {
			fmt.Println("Starting redis server...")
			server, err := miniredis.Run()
			if err != nil {
				panic(fmt.Sprintf("Could not start redis server. %v", err))
			}
			defer server.Close()

			client = rd.NewClient(server.Addr(), server.Addr())

			lockingClient = lock.NewNoOpLockingClient()
		}

		_, err := client.Ping(context.Background())
		if err != nil {
			panic(fmt.Sprintf("Could not ping local redis server. %v", err))
		}

		f(*useLocal, client, lockingClient)
	}
}
