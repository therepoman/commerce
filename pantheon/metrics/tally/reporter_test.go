package tally

import (
	"math"
	"math/rand"
	"testing"
	"time"

	"fmt"

	cloudwatchiface_mock "code.justin.tv/commerce/pantheon/mocks/github.com/aws/aws-sdk-go/service/cloudwatch/cloudwatchiface"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/cloudwatch"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestCloudwatchMetricsReporter_Flush(t *testing.T) {
	Convey("Given a cloudwatch interface", t, func() {
		cw := new(cloudwatchiface_mock.CloudWatchAPI)

		reporter := &cloudwatchMetricsReporter{
			cloudwatchAPI: cw,
			namespace:     "test",
			datums:        make(chan cloudwatch.MetricDatum, defaultMetricBufferSize),
		}

		datumsCount := 500

		Convey(fmt.Sprintf("...And %d Metric datums", datumsCount), func() {
			for i := 0; i < datumsCount; i++ {
				datum := cloudwatch.MetricDatum{
					MetricName: aws.String("test"),
					Value:      aws.Float64(rand.Float64()),
					Unit:       aws.String(cloudwatch.StandardUnitNone),
					Timestamp:  aws.Time(time.Now()),
				}

				reporter.datums <- datum
			}

			Convey("When Cloudwatch doesn't throw an error", func() {
				// Make sure each PutMetricData call doesn't exceed the batch size
				cw.On("PutMetricData", mock.MatchedBy(func(input *cloudwatch.PutMetricDataInput) bool {
					return input != nil && len(input.MetricData) <= batchSize
				})).Return(&cloudwatch.PutMetricDataOutput{}, nil)

				Convey("When Flush is called", func() {
					reporter.Flush()
					Convey("Cloudwatch is called in batches", func() {
						numCalls := math.Ceil(float64(datumsCount) / float64(batchSize))
						println(len(cw.Calls))
						So(cw.AssertNumberOfCalls(t, "PutMetricData", int(numCalls)), ShouldBeTrue)
					})
				})
				Convey("When an additional amount of items are added", func() {
					for i := 0; i < batchSize; i++ {
						datum := cloudwatch.MetricDatum{
							MetricName: aws.String("test"),
							Value:      aws.Float64(rand.Float64()),
							Unit:       aws.String(cloudwatch.StandardUnitNone),
							Timestamp:  aws.Time(time.Now()),
						}

						reporter.datums <- datum
					}

					Convey("...And Flush is called", func() {
						reporter.Flush()
						Convey("Cloudwatch is called an extra time", func() {
							numCalls := math.Ceil(float64(datumsCount) / float64(batchSize))
							println(len(cw.Calls))
							So(cw.AssertNumberOfCalls(t, "PutMetricData", int(numCalls)+1), ShouldBeTrue)
						})
					})
				})
			})
		})

	})
}
