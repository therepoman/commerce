package tally

import (
	"time"

	"code.justin.tv/commerce/pantheon/metrics"
	"code.justin.tv/commerce/pantheon/utils/math"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/cloudwatch"
	"github.com/aws/aws-sdk-go/service/cloudwatch/cloudwatchiface"
	log "github.com/sirupsen/logrus"
	uberTally "github.com/uber-go/tally"
)

const (
	batchSize               = 75
	dimensionNameType       = "Type"
	snapshotSummary         = "snapshot-summary"
	defaultMetricBufferSize = 500000
)

type ReporterOptions struct {
	Namespace  string
	BufferSize int
}

func NewReporter(api cloudwatchiface.CloudWatchAPI, options ReporterOptions) metrics.Reporter {
	var metricBufferSize int
	if options.BufferSize == 0 {
		metricBufferSize = defaultMetricBufferSize
	} else {
		metricBufferSize = options.BufferSize
	}

	return &cloudwatchMetricsReporter{
		cloudwatchAPI: api,
		namespace:     options.Namespace,
		datums:        make(chan cloudwatch.MetricDatum, metricBufferSize),
	}
}

type cloudwatchMetricsReporter struct {
	cloudwatchAPI cloudwatchiface.CloudWatchAPI
	namespace     string
	datums        chan cloudwatch.MetricDatum
}

type reporterCapabilities struct{}

// Reporting returns whether the reporter has the ability to actively report.
func (c reporterCapabilities) Reporting() bool {
	return true
}

func (c reporterCapabilities) Tagging() bool {
	return true
}

func (r *cloudwatchMetricsReporter) Capabilities() uberTally.Capabilities {
	return reporterCapabilities{}
}

func (r *cloudwatchMetricsReporter) Flush() {
	size := len(r.datums)
	metrics := make([]*cloudwatch.MetricDatum, size)
	for i := 0; i < size; i++ {
		datum := <-r.datums
		metrics[i] = &datum
	}

	for i := 0; i < size; i += batchSize {
		end := math.MinInt(i+batchSize, size)
		page := metrics[i:end]
		_, err := r.cloudwatchAPI.PutMetricData(&cloudwatch.PutMetricDataInput{
			Namespace:  aws.String(r.namespace),
			MetricData: page,
		})

		if err != nil {
			log.WithField("metrics", page).WithError(err).
				Error("Could not post metrics to cloudwatch")
		}
	}
}

func (r *cloudwatchMetricsReporter) ReportCounter(name string, tags map[string]string, value int64) {
	d := dimensions(tags)
	r.stageMetricValue(name, float64(value), d, cloudwatch.StandardUnitNone)
}

func (r *cloudwatchMetricsReporter) ReportTimer(name string, tags map[string]string, interval time.Duration) {
	d := dimensions(tags)
	r.stageMetricValue(name, toMillis(interval), d, cloudwatch.StandardUnitMilliseconds)
}

func (r *cloudwatchMetricsReporter) ReportGauge(name string, tags map[string]string, value float64) {
	d := dimensions(tags)
	r.stageMetricValue(name, value, d, cloudwatch.StandardUnitNone)
}

// ReportHistogramDurationSamples is loosely based on http://bit.ly/2rWJtlG
func (r *cloudwatchMetricsReporter) ReportHistogramDurationSamples(name string, tags map[string]string,
	buckets uberTally.Buckets, bucketLowerBound, bucketUpperBound time.Duration, samples int64) {

	d := dimensions(tags)
	hist := histogram{
		Buckets:    buckets,
		samples:    samples,
		lowerBound: toMillis(bucketLowerBound),
		upperBound: toMillis(bucketUpperBound),
	}

	d[dimensionNameType] = snapshotSummary
	r.stageStatisticSet(name, hist.StatisticSetMillis(), d, cloudwatch.StandardUnitMilliseconds)
}

// ReportHistogramValueSamples is loosely based on http://bit.ly/2rWJtlG
func (r *cloudwatchMetricsReporter) ReportHistogramValueSamples(name string, tags map[string]string,
	buckets uberTally.Buckets, bucketLowerBound, bucketUpperBound float64, samples int64) {

	d := dimensions(tags)
	hist := histogram{
		Buckets:    buckets,
		lowerBound: bucketLowerBound,
		upperBound: bucketUpperBound,
		samples:    samples,
	}

	d[dimensionNameType] = snapshotSummary
	r.stageStatisticSet(name, hist.StatisticSet(), d, cloudwatch.StandardUnitNone)
}

func toMillis(d time.Duration) float64 {
	return float64(d) / float64(time.Millisecond)
}

func (r *cloudwatchMetricsReporter) stageStatisticSet(name string, stats cloudwatch.StatisticSet, dimensions dimensions, unit string) {
	datum := cloudwatch.MetricDatum{
		Dimensions:      dimensions.Slice(),
		MetricName:      aws.String(name),
		StatisticValues: &stats,
		Unit:            aws.String(unit),
		Timestamp:       aws.Time(time.Now()),
	}

	r.datums <- datum
}

func (r *cloudwatchMetricsReporter) stageMetricValue(name string, value float64, dimensions dimensions, unit string) {
	datum := cloudwatch.MetricDatum{
		Dimensions: dimensions.Slice(),
		MetricName: aws.String(name),
		Value:      aws.Float64(value),
		Unit:       aws.String(unit),
		Timestamp:  aws.Time(time.Now()),
	}

	r.datums <- datum
}
