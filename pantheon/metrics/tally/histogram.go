package tally

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/cloudwatch"
	uberTally "github.com/uber-go/tally"
)

type histogram struct {
	uberTally.Buckets
	samples    int64
	lowerBound float64
	upperBound float64
}

func (h histogram) StatisticSet() cloudwatch.StatisticSet {
	var sum float64
	for _, v := range h.AsValues() {
		sum += v
	}

	stat := cloudwatch.StatisticSet{
		Maximum:     aws.Float64(h.upperBound),
		Minimum:     aws.Float64(h.lowerBound),
		SampleCount: aws.Float64(float64(h.samples)),
		Sum:         aws.Float64(sum),
	}

	return stat
}

func (h histogram) StatisticSetMillis() cloudwatch.StatisticSet {
	var sum float64
	for _, v := range h.AsDurations() {
		millis := toMillis(v)
		sum += millis
	}

	stat := cloudwatch.StatisticSet{
		Maximum:     aws.Float64(h.upperBound),
		Minimum:     aws.Float64(h.lowerBound),
		SampleCount: aws.Float64(float64(h.samples)),
		Sum:         aws.Float64(sum),
	}

	return stat
}
