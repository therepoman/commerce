package tally

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/cloudwatch"
)

type dimensions map[string]string

func (d dimensions) Slice() []*cloudwatch.Dimension {
	dimensions := make([]*cloudwatch.Dimension, len(d))
	var i int
	for k, v := range d {
		dimensions[i] = &cloudwatch.Dimension{
			Name:  aws.String(k),
			Value: aws.String(v),
		}

		i++
	}
	return dimensions
}
