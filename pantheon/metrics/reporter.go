package metrics

import (
	"github.com/uber-go/tally"
)

type Reporter interface {
	tally.StatsReporter
}
