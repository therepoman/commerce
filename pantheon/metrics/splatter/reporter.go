package splatter

import (
	"fmt"
	"sort"
	"time"

	"code.justin.tv/commerce/gogogadget/strings"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pantheon/metrics"
	"github.com/cactus/go-statsd-client/statsd"
	uberTally "github.com/uber-go/tally"
)

const (
	maxMetricNameSize = 255
)

var blacklistedTagKeys = map[string]interface{}{
	"stage": nil, // stage is already its own dimension
}

func NewReporter(statter statsd.Statter) metrics.Reporter {
	return &splatterReporter{
		statter: statter,
	}
}

type splatterReporter struct {
	statter statsd.Statter
}

type reporterCapabilities struct{}

func (c reporterCapabilities) Reporting() bool {
	return true
}

func (c reporterCapabilities) Tagging() bool {
	return false
}

func (r *splatterReporter) Capabilities() uberTally.Capabilities {
	return reporterCapabilities{}
}

func (r *splatterReporter) Flush() {
	// Flushing is handled by splatter
}

func tagsToPrefix(metricName string, tags map[string]string) string {
	keys := make([]string, 0)
	for tagKey := range tags {
		_, blacklisted := blacklistedTagKeys[tagKey]
		if !blacklisted {
			keys = append(keys, tagKey)
		}
	}

	sort.Strings(keys)
	var prefix string
	for _, tagKey := range keys {
		tagValue := tags[tagKey]
		if strings.NotBlank(prefix) {
			prefix += "."
		}
		prefix += fmt.Sprintf("%s:%s", tagKey, tagValue)
	}

	if len(prefix)+len(metricName) > maxMetricNameSize {
		prefixLength := maxMetricNameSize - len(metricName)
		prefix = prefix[:prefixLength]
	}

	return prefix
}

func (r *splatterReporter) ReportCounter(name string, tags map[string]string, value int64) {
	subStatter := r.statter.NewSubStatter(tagsToPrefix(name, tags))
	err := subStatter.Inc(name, value, 1.0)
	if err != nil {
		log.WithError(err).Error("error calling ReportCounter")
	}
}

func (r *splatterReporter) ReportTimer(name string, tags map[string]string, interval time.Duration) {
	subStatter := r.statter.NewSubStatter(tagsToPrefix(name, tags))
	err := subStatter.TimingDuration(name, interval, 1.0)
	if err != nil {
		log.WithError(err).Error("error calling ReportTimer")
	}
}

func (r *splatterReporter) ReportGauge(name string, tags map[string]string, value float64) {
	subStatter := r.statter.NewSubStatter(tagsToPrefix(name, tags))
	err := subStatter.Gauge(name, int64(value), 1.0)
	if err != nil {
		log.WithError(err).Error("error calling ReportGauge")
	}
}

func (r *splatterReporter) ReportHistogramDurationSamples(name string, tags map[string]string,
	buckets uberTally.Buckets, bucketLowerBound, bucketUpperBound time.Duration, samples int64) {
	log.Error("ReportHistogramDurationSamples is not supported")
}

func (r *splatterReporter) ReportHistogramValueSamples(name string, tags map[string]string,
	buckets uberTally.Buckets, bucketLowerBound, bucketUpperBound float64, samples int64) {
	log.Error("ReportHistogramValueSamples is not supported")
}
