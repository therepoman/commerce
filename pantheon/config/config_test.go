package config

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestLoadConfig(t *testing.T) {
	Convey("The unit test config is loadable", t, func() {
		cfg, err := LoadConfigWithSubstage(UnitTest, Primary)
		So(err, ShouldBeNil)
		So(cfg, ShouldNotBeNil)
		So(cfg.EnvironmentName, ShouldEqual, "unit-test")
		So(cfg.SubstageName, ShouldEqual, "primary")

		So(len(cfg.Tenants), ShouldEqual, 1)
		pollsTenant := cfg.Tenants["polls"]
		So(pollsTenant.RedisEndpoint, ShouldNotBeEmpty)
		So(pollsTenant.EventQueueConfig.Name, ShouldEqual, "name")
		So(pollsTenant.EventQueueConfig.URL, ShouldEqual, "url")
		So(pollsTenant.EventQueueConfig.NumWorkers, ShouldEqual, 123)
	})
	Convey("The unit test config is loadable for canary", t, func() {
		cfg, err := LoadConfigWithSubstage(UnitTest, Canary)
		So(err, ShouldBeNil)
		So(cfg, ShouldNotBeNil)
		So(cfg.SubstageName, ShouldEqual, "canary")
	})
}

func TestReaderEndpointPresent(t *testing.T) {
	Convey("The reader endpoint should be there", t, func() {
		for _, stage := range []Environment{Prod, Staging, Dev} {
			cfg, err := LoadConfig(stage)
			So(err, ShouldBeNil)
			So(cfg, ShouldNotBeNil)
			So(cfg.DefaultTenant.RedisReaderEndpoint, ShouldNotBeEmpty)
			for _, tenant := range cfg.Tenants {
				So(tenant.RedisReaderEndpoint, ShouldNotBeEmpty)
			}
		}
	})
}
