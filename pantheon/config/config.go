package config

import (
	"fmt"
	"io/ioutil"
	"os"

	"github.com/go-yaml/yaml"
	log "github.com/sirupsen/logrus"
)

const (
	LocalConfigFilePath  = "/src/code.justin.tv/commerce/pantheon/config/data/%s.yaml"
	GlobalConfigFilePath = "/etc/pantheon/config/%s.yaml"
	LambdaConfigFilePath = "etc/pantheon/config/%s.yaml"

	EnvironmentEnvironmentVariable = "ENVIRONMENT"
	CanaryEnvironmentVariable      = "CANARY"

	UnitTest  = Environment("unit-test")
	SmokeTest = Environment("smoke-test")

	Dev     = Environment("dev")
	Staging = Environment("staging")
	Prod    = Environment("prod")

	Primary = Substage("primary")
	Canary  = Substage("canary")
)

type Privacy struct {
	DeleteLeaderboardsStepFunctionConfig *StepFunctionConfig `yaml:"delete-leaderboards-step-function"`
	DeleteEntriesStepFunctionConfig      *StepFunctionConfig `yaml:"delete-entries-step-function"`
}

type QueueConfig struct {
	Name       string `yaml:"name"`
	URL        string `yaml:"url"`
	NumWorkers int    `yaml:"num-workers"`
}

type DynamoTableConfig struct {
	Name string `yaml:"name"`
}

type StepFunctionConfig struct {
	ARN string `yaml:"arn"`
}

func (c *DynamoTableConfig) GetTableName() string {
	if c == nil {
		return ""
	}
	return c.Name
}

type TenantConfig struct {
	EventQueueConfig                  *QueueConfig       `yaml:"event-queue"`
	ArchivalQueueConfig               *QueueConfig       `yaml:"archival-queue"`
	ModerationNonFifoQueueConfig      *QueueConfig       `yaml:"moderation-non-fifo-queue"`
	PDMSRedisEndpoint                 string             `yaml:"pdms-redis-endpoint"`
	PDMSRedisReaderEndpoint           string             `yaml:"pdms-redis-reader-endpoint"`
	PDMSDeleteEntriesQueueConfig      *QueueConfig       `yaml:"pdms-delete-entries-queue"`
	PDMSDeleteLeaderboardsQueueConfig *QueueConfig       `yaml:"pdms-delete-leaderboards-queue"`
	EventsTableConfig                 *DynamoTableConfig `yaml:"events-table"`
	ModerationsTableConfig            *DynamoTableConfig `yaml:"moderations-table"`
	RedisEndpoint                     string             `yaml:"redis-endpoint"`
	RedisReaderEndpoint               string             `yaml:"redis-reader-endpoint"`
}

type Config struct {
	InitRedisPubsubOnStartup  bool   `yaml:"init-redis-pubsub-on-startup"`
	EnvironmentName           string `yaml:"environment-name"`
	SubstageName              string
	LeaderboardS3Bucket       string                  `yaml:"leaderboard-s3-bucket"`
	EventSNSTopicARN          string                  `yaml:"event-sns-topic-arn"`
	OWL2019EventsQueueConfig  *QueueConfig            `yaml:"owl-2019-events-queue"`
	AWSRegion                 string                  `yaml:"aws-region"`
	ReportMetrics             bool                    `yaml:"report-metrics"`
	UseSplatterMetrics        bool                    `yaml:"use-splatter-metrics"`
	PubsubHost                string                  `yaml:"pubsub-host"`
	UserServiceHost           string                  `yaml:"user-service-host"`
	SandstormRoleArn          string                  `yaml:"sandstorm-role-arn"`
	SandStormRollbarToken     string                  `yaml:"sandstorm-rollbar-token"`
	PantheonEndpoint          string                  `yaml:"pantheon-endpoint"`
	Privacy                   Privacy                 `yaml:"privacy"`
	Tenants                   map[string]TenantConfig `yaml:"tenants"`
	DefaultTenant             TenantConfig            `yaml:"default-tenant"`
	UserModerationQueueConfig *QueueConfig            `yaml:"user-moderation-queue"`
}

type Environment string
type Substage string

var Environments = map[Environment]interface{}{UnitTest: nil, Dev: nil, SmokeTest: nil, Staging: nil, Prod: nil}

func IsValidEnvironment(env Environment) bool {
	_, ok := Environments[env]
	return ok
}

// Provide an entry point for things like lambas which do not have a substage
func LoadConfig(env Environment) (*Config, error) {
	return LoadConfigWithSubstage(env, Primary)
}

// Provde an entry point for the service which does have a substage
func LoadConfigWithSubstage(env Environment, substage Substage) (*Config, error) {
	if !IsValidEnvironment(env) {
		return nil, fmt.Errorf("invalid environment: %s", env)
	}

	baseFileName := string(env)
	filePath, err := getConfigFilePath(baseFileName)
	if err != nil {
		return nil, err
	}
	return loadConfig(filePath, substage)
}

func loadConfig(path string, substage Substage) (*Config, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}

	defer func() {
		err = file.Close()
		if err != nil {
			log.Error("Failed to close config file", err)
		}
	}()

	fileBytes, err := ioutil.ReadAll(file)
	if err != nil {
		return nil, err
	}

	var cfg Config
	err = yaml.Unmarshal(fileBytes, &cfg)
	if err != nil {
		return nil, err
	}

	cfg.SubstageName = string(substage)
	return &cfg, nil
}

func getConfigFilePath(baseFilename string) (string, error) {
	localFname := os.Getenv("GOPATH") + fmt.Sprintf(LocalConfigFilePath, baseFilename)
	if _, err := os.Stat(localFname); !os.IsNotExist(err) {
		return localFname, nil
	}
	lambdaFname := fmt.Sprintf(LambdaConfigFilePath, baseFilename)
	if _, err := os.Stat(lambdaFname); !os.IsNotExist(err) {
		return lambdaFname, nil
	}
	globalFname := fmt.Sprintf(GlobalConfigFilePath, baseFilename)
	if _, err := os.Stat(globalFname); os.IsNotExist(err) {
		return "", err
	}
	return globalFname, nil
}
