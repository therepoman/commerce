package config

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"code.justin.tv/commerce/dynamicconfig"
	"code.justin.tv/commerce/logrus"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/ssm"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/pkg/errors"
)

const (
	ModerationQueueRateLimit        = "moderation-queue-rate-limit"
	moderationQueueRateLimitDefault = 40

	// Metrics
	startupSuccessMetricFormat = "dynconfig.%s.startup.success"
	startupFailureMetricFormat = "dynconfig.%s.startup.failure"
	startupLatencyMetricFormat = "dynconfig.%s.startup.latency"
	reloadSuccessMetricFormat  = "dynconfig.%s.refresh.success"
	reloadFailureMetricFormat  = "dynconfig.%s.refresh.failure"
	reloadLatencyMetricFormat  = "dynconfig.%s.refresh.latency"
)

var (
	configKeys = []string{
		ModerationQueueRateLimit,
	}
)

type moderationQueueRateLimitConfig struct {
	RateLimit int `json:"rate_limit"`
}

type DynamicConfigClient interface {
	GetModerationQueueRateLimit() int
}

type Dynamic struct {
	dynamicConfig *dynamicconfig.DynamicConfig
	statter       statsd.Statter
	statsPrefix   string
}

func NewDynamic(region string, statter statsd.Statter) (*Dynamic, error) {
	// Timeout for initialization of the dynamic configuration
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*30)
	defer cancel()

	statsPrefix := "ssm"
	awsConfig := &aws.Config{Region: aws.String(region)}
	sess, err := session.NewSession(awsConfig)
	if err != nil {
		incrementStatter(statter, fmt.Sprintf(startupFailureMetricFormat, statsPrefix))
		return nil, errors.Wrap(err, "creating aws session")
	}

	// Create ssm client and source
	ssmClient := ssm.New(sess)
	ssmSource := dynamicconfig.NewSSMSource(configKeys, ssmClient)

	loadStart := time.Now()

	// Create dynamic config
	dynamicConfig, err := dynamicconfig.NewDynamicConfig(ctx, ssmSource, map[string]dynamicconfig.Transform{})
	if err != nil {
		// This error is recoverable because NewDynamicConfig still returns a working dynamic config object, so we don't return the error.
		// We passed in a valid source, so the config will be re-fetched on the next call to Reload.
		incrementStatter(statter, fmt.Sprintf(startupFailureMetricFormat, statsPrefix))
		logrus.WithError(err).Error("initializing dynamic config values -- using empty default")
	} else {
		incrementStatter(statter, fmt.Sprintf(startupSuccessMetricFormat, statsPrefix))
	}
	statter.TimingDuration(fmt.Sprintf(startupLatencyMetricFormat, statsPrefix), time.Since(loadStart), 1.0)
	logrus.Info("initialized dynamic config values")

	return &Dynamic{
		dynamicConfig: dynamicConfig,
		statter:       statter,
		statsPrefix:   statsPrefix,
	}, nil
}

// Reload wraps the dynamicConfig.Reload() method and emits metrics on success/failure
func (d *Dynamic) Reload(ctx context.Context) error {
	if d == nil || d.dynamicConfig == nil {
		msg := "failed to reload dynamic config because dynamic wasn't initialized properly"
		err := errors.New(msg)
		incrementStatter(d.statter, fmt.Sprintf(reloadFailureMetricFormat, d.statsPrefix))
		logrus.WithError(err).Error()
		return err
	}

	start := time.Now()

	err := d.dynamicConfig.Reload(ctx)
	if err != nil {
		incrementStatter(d.statter, fmt.Sprintf(reloadFailureMetricFormat, d.statsPrefix))
		logrus.WithError(err).Error("failed to reload dynamic config")
	} else {
		incrementStatter(d.statter, fmt.Sprintf(reloadSuccessMetricFormat, d.statsPrefix))
	}

	d.statter.TimingDuration(fmt.Sprintf(reloadLatencyMetricFormat, d.statsPrefix), time.Since(start), 1.0)

	return err
}

func (d *Dynamic) GetModerationQueueRateLimit() int {
	if d == nil || d.dynamicConfig == nil {
		return moderationQueueRateLimitDefault
	}

	dynamicConfigResult := d.dynamicConfig.Get(ModerationQueueRateLimit)
	if dynamicConfigResult == nil {
		return moderationQueueRateLimitDefault
	}

	dynamicConfigResultString, ok := dynamicConfigResult.(string)
	if !ok {
		return moderationQueueRateLimitDefault
	}

	var rateLimitConfig moderationQueueRateLimitConfig
	if err := json.Unmarshal([]byte(dynamicConfigResultString), &rateLimitConfig); err != nil {
		logrus.WithError(err).Error("failed to unmarshal dynamic config for GetModerationQueueRateLimit")
		return moderationQueueRateLimitDefault
	}

	return rateLimitConfig.RateLimit
}

func incrementStatter(statter statsd.Statter, stat string) {
	statErr := statter.Inc(stat, 1, 1.0)
	if statErr != nil {
		logrus.WithField("stat", stat).WithError(statErr).Error("Failed to increment statter")
	}
}

type dynamicFake struct{}

func NewDynamicConfigFake() DynamicConfigClient {
	return &dynamicFake{}
}

func (d *dynamicFake) GetModerationQueueRateLimit() int {
	return 1
}
