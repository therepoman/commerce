package sfn

type DeleteLeaderboardsStateMachineInput struct {
	Domain      string `json:"domain"`
	GroupingKey string `json:"grouping_key"`
	WaitSeconds int    `json:"wait_seconds"`
}

type DeleteEntriesStateMachineInput struct {
	Domain      string `json:"domain"`
	EntryKey    string `json:"entry_key"`
	WaitSeconds int    `json:"wait_seconds"`
}
