package backend

import (
	"context"
	"fmt"
	"io"
	"net/http"
	"time"

	pubclient "code.justin.tv/chat/pubsub-go-pubclient/client"
	"code.justin.tv/commerce/gogogadget/lrucache"
	"code.justin.tv/commerce/pantheon/backend/api"
	"code.justin.tv/commerce/pantheon/backend/dynamo/event"
	"code.justin.tv/commerce/pantheon/backend/dynamo/moderation"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/domain"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/ingestion"
	localLB "code.justin.tv/commerce/pantheon/backend/leaderboard/local"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/local/adapter"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/observers"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/observers/pubsub"
	sns_observer "code.justin.tv/commerce/pantheon/backend/leaderboard/observers/sns"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/persistence"
	redisLB "code.justin.tv/commerce/pantheon/backend/leaderboard/redis"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/redis/shadow"
	"code.justin.tv/commerce/pantheon/backend/redispubsub"
	redisPubsubWorkers "code.justin.tv/commerce/pantheon/backend/redispubsub/workers"
	sqsWorkerManager "code.justin.tv/commerce/pantheon/backend/sqs"
	"code.justin.tv/commerce/pantheon/backend/sqs/workers/archive"
	"code.justin.tv/commerce/pantheon/backend/sqs/workers/moderate"
	"code.justin.tv/commerce/pantheon/backend/sqs/workers/publish"
	"code.justin.tv/commerce/pantheon/backend/sqs/workers/usermoderation"
	"code.justin.tv/commerce/pantheon/backend/worker"
	cw "code.justin.tv/commerce/pantheon/clients/cloudwatch"
	"code.justin.tv/commerce/pantheon/clients/lock"
	"code.justin.tv/commerce/pantheon/clients/redis"
	"code.justin.tv/commerce/pantheon/clients/s3"
	"code.justin.tv/commerce/pantheon/clients/sfn"
	"code.justin.tv/commerce/pantheon/clients/sns"
	"code.justin.tv/commerce/pantheon/clients/sqs"
	"code.justin.tv/commerce/pantheon/clients/tenant"
	users "code.justin.tv/commerce/pantheon/clients/users"
	"code.justin.tv/commerce/pantheon/config"
	batch_delete_events "code.justin.tv/commerce/pantheon/lambdas/batch_delete_events/handler"
	batch_delete_moderations "code.justin.tv/commerce/pantheon/lambdas/batch_delete_moderations/handler"
	delete_leaderboard_entries "code.justin.tv/commerce/pantheon/lambdas/delete_leaderboard_entries/handler"
	delete_leaderboards_by_grouping_key "code.justin.tv/commerce/pantheon/lambdas/delete_leaderboards_by_grouping_key/handler"
	query_events_by_entry_key "code.justin.tv/commerce/pantheon/lambdas/query_events_by_entry_key/handler"
	query_moderations_by_entry_key "code.justin.tv/commerce/pantheon/lambdas/query_moderations_by_entry_key/handler"
	start_delete_entries "code.justin.tv/commerce/pantheon/lambdas/start_delete_entries/handler"
	start_delete_leaderboards "code.justin.tv/commerce/pantheon/lambdas/start_delete_leaderboards/handler"
	metrics_splatter "code.justin.tv/commerce/pantheon/metrics/splatter"
	metrics_tally "code.justin.tv/commerce/pantheon/metrics/tally"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpcserver"
	"code.justin.tv/commerce/splatter"
	"code.justin.tv/foundation/twitchclient"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sqs/sqsiface"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/facebookgo/inject"
	"github.com/felixge/httpsnoop"
	log "github.com/sirupsen/logrus"
	"github.com/uber-go/tally"
	"golang.org/x/sync/errgroup"
)

type ContextKey string

const (
	localhostServerEndpoint     = "http://localhost:8000"
	pantheonCloudwatchNamespace = "Pantheon"
	tallyFlushInterval          = time.Millisecond * 500
	MetricsScopeKey             = ContextKey("metric-scope")
	twitchTelemetryFlush        = 30 * time.Second // Flush metrics every 30 seconds for TwitchTelemetry
	twitchTelemetryMinBuffer    = 100000           // Define a min buffer size to ensure there is enough room for metrics while flushing

	// The cache size was determined by counting the bytes of the cached items and ensuring we don't consume more than 20% memory.
	// The entries consist of 3 ints (8 bytes), 1 string (16 bytes), and 1 bool (1 byte) = 41 bytes, round up to 50 bytes for overhead.
	// 500 bytes * 50,000 entries = 25MB
	LeaderboardLRUCacheSize = 50000
)

type LambdaHandlers struct {
	BatchDeleteModerationsHandler   batch_delete_moderations.Handler            `inject:""`
	BatchDeleteEventsHandler        batch_delete_events.Handler                 `inject:""`
	QueryModerationsHandler         query_moderations_by_entry_key.Handler      `inject:""`
	QueryEventsHandler              query_events_by_entry_key.Handler           `inject:""`
	DeleteLeaderboardEntriesHandler delete_leaderboard_entries.Handler          `inject:""`
	DeleteLeaderboardsByGroupingKey delete_leaderboards_by_grouping_key.Handler `inject:""`
	StartDeleteEntriesHandler       start_delete_entries.Handler                `inject:""`
	StartDeleteLeaderboardsHandler  start_delete_leaderboards.Handler           `inject:""`
}

type Backend struct {
	// RPC Server
	Server pantheonrpc.Pantheon `inject:""`

	// SQS Workers
	PublishEventWorker       *publish.EventWorker   `inject:""`
	ArchiveLeaderboardWorker *archive.Worker        `inject:""`
	ModerateWorker           *moderate.Worker       `inject:""`
	UserModerationWorker     *usermoderation.Worker `inject:""`

	// Pubsub Workers
	RedisShadowExpirationWorker *redisPubsubWorkers.ShadowLeaderboardExpirationWorker `inject:""`

	// Config
	Config        *config.Config  `inject:""`
	DynamicConfig *config.Dynamic `inject:""`

	// Other dependencies
	SQS              sqsiface.SQSAPI       `inject:""`
	Statsd           statsd.Statter        `inject:""`
	Persister        persistence.Persister `inject:""`
	RootMetricsScope tally.Scope           `inject:""`
	TenantClient     tenant.Landlord       `inject:""`

	// Worker Terminators
	terminators []worker.NamedTerminator

	// Lambda Handlers
	LambdaHandlers *LambdaHandlers `inject:""`
}

func AddMetricScope(ctx context.Context, scope tally.Scope) context.Context {
	out := context.WithValue(ctx, MetricsScopeKey, scope)
	return out
}

func MetricScope(ctx context.Context) tally.Scope {
	val := ctx.Value(MetricsScopeKey)
	if val == nil {
		return tally.NoopScope
	} else if scope, ok := val.(tally.Scope); ok {
		return scope
	} else {
		return tally.NoopScope
	}
}

func (b *Backend) MetricsLogger(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		requestScope := b.RootMetricsScope.Tagged(map[string]string{
			"URL": r.URL.Path,
		})

		ctx := AddMetricScope(r.Context(), requestScope)
		_ = r.WithContext(ctx)

		m := httpsnoop.CaptureMetrics(next, w, r)
		requestScope.Timer("Latency").Record(m.Duration)
		requestScope.Counter("Requests").Inc(1)

		code := m.Code
		switch {
		case code < 200:
			requestScope.Counter("1xx").Inc(1)
		case code >= 200 && code < 300:
			requestScope.Counter("2xx").Inc(1)
		case code >= 300 && code < 400:
			requestScope.Counter("3xx").Inc(1)
		case code >= 400 && code < 500:
			requestScope.Counter("4xx").Inc(1)
		case code >= 500:
			requestScope.Counter("5xx").Inc(1)
		}

	})
}

func (b *Backend) Ping(w http.ResponseWriter, r *http.Request) {
	_, err := b.TenantClient.GetDefaultTenant().RedisClient.Ping(context.Background())
	if err != nil {
		log.WithError(err).Error("Could not ping redis endpoint")
		w.WriteHeader(http.StatusInternalServerError)
		_, err = io.WriteString(w, err.Error())
		log.WithError(err).Error("Failed to write error message to response writer")
		return
	}

	pantheonClient := pantheonrpc.NewPantheonProtobufClient(localhostServerEndpoint, http.DefaultClient)
	_, err = pantheonClient.HealthCheck(r.Context(), &pantheonrpc.HealthCheckReq{})
	if err != nil {
		log.WithError(err).Error("Could not ping pantheon server")
		w.WriteHeader(http.StatusInternalServerError)
		_, err = io.WriteString(w, err.Error())
		log.WithError(err).Error("Failed to write error message to response writer")
		return
	}

	w.WriteHeader(http.StatusOK)
	_, err = io.WriteString(w, "pong")
	if err != nil {
		log.WithError(err).Error("Failed to write error message to response writer")
	}
}

func NewLambdaBackend(config *config.Config) (*Backend, error) {
	backend, err := populateBackend(config)
	if err != nil {
		return nil, err
	}

	return backend, nil
}

func NewBackend(config *config.Config) (*Backend, error) {
	backend, err := populateBackend(config)
	if err != nil {
		return nil, err
	}

	backend.setupWorkers()

	return backend, nil
}

func populateBackend(cfg *config.Config) (*Backend, error) {
	b := &Backend{}

	statter := splatter.NewBufferedTelemetryCloudWatchStatter(&splatter.BufferedTelemetryConfig{
		FlushPeriod:       twitchTelemetryFlush,
		BufferSize:        twitchTelemetryMinBuffer,
		AggregationPeriod: time.Minute, // Place all metrics into 1 minute buckets
		ServiceName:       "pantheon",
		AWSRegion:         cfg.AWSRegion,
		Stage:             cfg.EnvironmentName,
		Substage:          cfg.SubstageName,
		Prefix:            "", // No need for a prefix since service, stage, and substage are all dimensions
	}, map[string]bool{})

	rootScope, terminator, err := setupTally(cfg, statter)
	if err != nil {
		return nil, err
	}
	b.terminators = append(b.terminators, terminator)

	pubsubClient, err := pubclient.NewPubClient(twitchclient.ClientConf{
		Host: cfg.PubsubHost,
	})
	if err != nil {
		return nil, err
	}

	sess, err := session.NewSession(&aws.Config{
		Region: aws.String(cfg.AWSRegion),
	})
	if err != nil {
		return nil, err
	}

	leaderboardCache, err := lrucache.NewLRUCache(lrucache.NewLRUCacheParams{Size: LeaderboardLRUCacheSize})
	if err != nil {
		return nil, err
	}

	s3Client, err := s3.NewDefaultClient()
	if err != nil {
		return nil, err
	}

	sqsClient, err := sqs.NewDefaultClient()
	if err != nil {
		return nil, err
	}

	snsClient, err := sns.NewDefaultClient()
	if err != nil {
		return nil, err
	}

	sfnClient, err := sfn.NewDefaultClient()
	if err != nil {
		return nil, err
	}

	awsSDKClient, err := sqs.NewSDKClient()
	if err != nil {
		return nil, err
	}

	usersService, err := users.NewClient(cfg.UserServiceHost, statter)
	if err != nil {
		return nil, err
	}

	dynamicConfig, err := config.NewDynamic(cfg.AWSRegion, statter)
	if err != nil {
		log.WithError(err).Fatal("failed to initialize SSM dynamic config")
	}

	var graph inject.Graph
	err = graph.Provide(
		// Backend (graph root)
		&inject.Object{Value: b},

		// Config
		&inject.Object{Value: cfg},
		&inject.Object{Value: dynamicConfig},

		// RPC Server
		&inject.Object{Value: &pantheonrpcserver.Server{}},

		// Leaderboard LRU cache
		&inject.Object{Value: leaderboardCache},

		// APIs
		&inject.Object{Value: &api.GetLeaderboardAPIImpl{}, Name: "get_leaderboard_api"},
		&inject.Object{Value: &api.GetBucketedLeaderboardCountsAPIImpl{}, Name: "get_bucketed_leaderboard_counts_api"},
		&inject.Object{Value: &api.GetLeaderboardEntriesAboveThresholdAPIImpl{}, Name: "get_leaderboard_entries_above_threshold_api"},
		&inject.Object{Value: &api.ModerateEntryAPIImpl{}, Name: "moderate_entry_api"},
		&inject.Object{Value: &api.PublishEventAPIImpl{}, Name: "publish_event_api"},
		&inject.Object{Value: &api.DeleteLeaderboardAPIImpl{}, Name: "delete_leaderboard_api"},
		&inject.Object{Value: &api.DeleteEntriesAPIImpl{}, Name: "delete_entries_api"},

		// Clients
		&inject.Object{Value: s3Client},
		&inject.Object{Value: sqsClient},
		&inject.Object{Value: snsClient},
		&inject.Object{Value: sfnClient},

		// Domain Manager
		&inject.Object{Value: domain.NewHardCodedConfigManager()},

		// Shadow Key Manager
		&inject.Object{Value: &shadow.KeyManagerImpl{}},

		// Persister
		&inject.Object{Value: &persistence.S3Persister{}},

		// Ingester
		&inject.Object{Value: &ingestion.LeaderboardIngester{}},

		// Adapter
		&inject.Object{Value: &adapter.LeaderboardAdapterImpl{}},

		// SQS workers
		&inject.Object{Value: &archive.Worker{}},
		&inject.Object{Value: &publish.EventWorker{}},
		&inject.Object{Value: &moderate.Worker{}},
		&inject.Object{Value: &usermoderation.Worker{}},

		// Redis pubsub worker
		&inject.Object{Value: &redisPubsubWorkers.ShadowLeaderboardExpirationWorker{}},

		// AWS-SDK Clients
		&inject.Object{Value: awsSDKClient},

		// Statsd Client
		&inject.Object{Value: statter},

		// Pubsub Client
		&inject.Object{Value: pubsubClient},

		// Pubsub Publisher
		&inject.Object{Value: &pubsub.Observer{}},

		// SNS Publisher
		&inject.Object{Value: &sns_observer.Observer{}},

		// Leaderboard Factory
		&inject.Object{Value: &localLB.Factory{}},
		&inject.Object{Value: &redisLB.PDMSFactory{}, Name: "pdms-lb-factory"},
		&inject.Object{Value: &redisLB.Factory{}},

		// Index Factory
		&inject.Object{Value: &redisLB.IndexFactory{}},

		// Leaderboard topper
		&inject.Object{Value: observers.NewTopper()},

		// User Service
		&inject.Object{Value: usersService},

		// Lambda Handlers
		&inject.Object{Value: batch_delete_moderations.NewHandler()},
		&inject.Object{Value: batch_delete_events.NewHandler()},
		&inject.Object{Value: query_moderations_by_entry_key.NewHandler()},
		&inject.Object{Value: query_events_by_entry_key.NewHandler()},
		&inject.Object{Value: delete_leaderboard_entries.NewHandler()},
		&inject.Object{Value: delete_leaderboards_by_grouping_key.NewHandler()},
		&inject.Object{Value: start_delete_entries.NewHandler()},
		&inject.Object{Value: start_delete_leaderboards.NewHandler()},
		&inject.Object{Value: &LambdaHandlers{}},

		// Tenant Client
		&inject.Object{Value: setupTenants(sess, cfg)},

		// Root Scope
		&inject.Object{Value: rootScope},
	)

	if err != nil {
		return nil, err
	}

	err = graph.Populate()
	if err != nil {
		return nil, err
	}

	return b, nil
}

func setupTenants(sess *session.Session, cfg *config.Config) tenant.Landlord {
	tenantMap := make(map[string]tenant.Tenant)
	for d, tenantCfg := range cfg.Tenants {
		tenantMap[d] = setupTenant(sess, tenantCfg)
	}
	return tenant.NewLandlord(tenantMap, setupTenant(sess, cfg.DefaultTenant))
}

func setupTenant(sess *session.Session, cfg config.TenantConfig) tenant.Tenant {
	redisClient := redis.NewClient(
		cfg.RedisEndpoint,
		cfg.RedisReaderEndpoint,
	)

	pdmsRedisClient := redis.NewClient(cfg.PDMSRedisEndpoint, cfg.PDMSRedisReaderEndpoint)

	redisStandardLockingClient := lock.NewRedisLockingClient(redisClient.GetLockingClient(), lock.DefaultLockOptions())

	eventDAO := event.NewDAO(sess, cfg.EventsTableConfig.GetTableName())
	moderationDAO := moderation.NewDAO(sess, cfg.ModerationsTableConfig.GetTableName())

	return tenant.Tenant{
		RedisClient:           redisClient,
		PDMSRedisClient:       pdmsRedisClient,
		StandardLockingClient: redisStandardLockingClient,
		EventDAO:              eventDAO,
		ModerationDAO:         moderationDAO,
		Config:                cfg,
	}
}

func setupTally(config *config.Config, stats statsd.Statter) (tally.Scope, worker.NamedTerminator, error) {
	tallyOpt := tally.ScopeOptions{
		Tags: map[string]string{
			"stage": config.EnvironmentName,
		},
	}

	if config.ReportMetrics {
		cloudwatchClient, err := cw.NewDefaultClient()
		if err != nil {
			return nil, nil, err
		}

		var r tally.StatsReporter
		if config.UseSplatterMetrics {
			if stats == nil {
				stats = &statsd.NoopClient{}
			}
			r = metrics_splatter.NewReporter(stats)
		} else {
			r = metrics_tally.NewReporter(cloudwatchClient, metrics_tally.ReporterOptions{
				Namespace: pantheonCloudwatchNamespace,
			})
		}

		tallyOpt.Reporter = r

	} else {
		tallyOpt.Reporter = tally.NullStatsReporter
	}
	rootScope, closer := tally.NewRootScope(tallyOpt, tallyFlushInterval)
	terminator := worker.NewCloserTerminator("tallyTerminator", closer)
	return rootScope, terminator, nil
}

func (b *Backend) setupWorkers() {
	// OWL 2019 Queue TODO: remove once we're sure this isn't being used
	b.terminators = b.setupSQSWorker(b.terminators, b.Config.OWL2019EventsQueueConfig, b.PublishEventWorker)

	// UserModeration handler (enqueues moderation events for all opted-in domains)
	b.terminators = b.setupSQSWorker(b.terminators, b.Config.UserModerationQueueConfig, b.UserModerationWorker)

	// Default tenant queues
	b.terminators = b.setupSQSWorker(b.terminators, b.TenantClient.GetDefaultTenant().Config.EventQueueConfig, b.PublishEventWorker)
	b.terminators = b.setupSQSWorker(b.terminators, b.TenantClient.GetDefaultTenant().Config.ArchivalQueueConfig, b.ArchiveLeaderboardWorker)
	b.terminators = b.setupSQSWorker(b.terminators, b.TenantClient.GetDefaultTenant().Config.ModerationNonFifoQueueConfig, b.ModerateWorker)

	// Tenant specific queues
	for _, t := range b.Config.Tenants {
		b.terminators = b.setupSQSWorker(b.terminators, t.EventQueueConfig, b.PublishEventWorker)
		b.terminators = b.setupSQSWorker(b.terminators, t.ArchivalQueueConfig, b.ArchiveLeaderboardWorker)
		b.terminators = b.setupSQSWorker(b.terminators, t.ModerationNonFifoQueueConfig, b.ModerateWorker)
	}

	// Default tenant worker
	b.terminators = append(b.terminators, redispubsub.NewRedisPubsubWorkerManager("default-shadowExpirationWorker", b.TenantClient.GetDefaultTenant().RedisClient, b.RedisShadowExpirationWorker))

	// Tenant specific workers
	for d, t := range b.TenantClient.GetTenants() {
		b.terminators = append(b.terminators, redispubsub.NewRedisPubsubWorkerManager(fmt.Sprintf("%s-shadowExpirationWorker", d), t.RedisClient, b.RedisShadowExpirationWorker))
	}
}

func (b *Backend) setupSQSWorker(terminators []worker.NamedTerminator, queueConfig *config.QueueConfig, worker sqsWorkerManager.Worker) []worker.NamedTerminator {
	if queueConfig != nil && queueConfig.Name != "" {
		terminators = append(terminators, sqsWorkerManager.NewSQSWorkerManager(queueConfig.Name, queueConfig.NumWorkers, b.SQS, worker, b.Statsd))
	}
	return terminators
}

func (b *Backend) Shutdown(ctx context.Context) *errgroup.Group {
	errg, _ := errgroup.WithContext(ctx)

	for _, terminator := range b.terminators {
		t := terminator
		errg.Go(func() error {
			log.Infof("shutting down %s", t.Name())
			err := t.Shutdown(context.Background())
			if err != nil {
				log.WithError(err).Errorf("could not shutdown %s", t.Name())
			}
			return err
		})
	}
	return errg
}

func (b *Backend) GetLambdaHandlers() *LambdaHandlers {
	return b.LambdaHandlers
}
