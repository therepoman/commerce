package api

import (
	"context"
	"errors"
	"time"

	"code.justin.tv/commerce/pantheon/backend/leaderboard"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/bucket"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/identifier"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
)

type GetBucketedLeaderboardCountsAPIImpl struct {
	LeaderboardFactory leaderboard.RedisLeaderboardFactory `inject:""`
}

type GetBucketedLeaderboardCountsAPI interface {
	GetBucketedLeaderboardCounts(ctx context.Context, req *pantheonrpc.GetBucketedLeaderboardCountsReq) (*[]*pantheonrpc.Bucket, error)
}

func (g *GetBucketedLeaderboardCountsAPIImpl) GetBucketedLeaderboardCounts(ctx context.Context, req *pantheonrpc.GetBucketedLeaderboardCountsReq) (*[]*pantheonrpc.Bucket, error) {
	if req == nil {
		return nil, errors.New("Failed to GetBucketedLeaderboardCounts, request is nil")
	}

	t := time.Now()
	if req.Time != uint64(0) {
		t = time.Unix(0, int64(req.Time))
	}

	timeBucket := bucket.TimestampToTimeBucket(t, req.TimeUnit)

	id := identifier.Identifier{
		Domain:              identifier.Domain(req.Domain),
		GroupingKey:         identifier.GroupingKey(req.GroupingKey),
		TimeAggregationUnit: req.TimeUnit,
		TimeBucket:          timeBucket,
	}

	lb, err := g.LeaderboardFactory.GetRedisLeaderboard(ctx, id, leaderboard.RetrievalSettings{})

	if err != nil {
		return nil, err
	}

	var buckets []*pantheonrpc.Bucket

	for _, threshold := range req.BucketThresholds {
		t := threshold

		count, err := lb.GetCountForRange(ctx, &t, nil, leaderboard.RetrievalSettings{})

		if err != nil {
			return nil, err
		}

		if req.IncludeModeratedEntries {
			negT := -t
			moderatedCount, err := lb.GetCountForRange(ctx, nil, &negT, leaderboard.RetrievalSettings{})
			if err != nil {
				return nil, err
			}

			count += moderatedCount
		}

		buckets = append(buckets, &pantheonrpc.Bucket{
			Count:     count,
			Threshold: t,
		})
	}

	return &buckets, nil
}
