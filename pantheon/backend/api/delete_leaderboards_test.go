package api

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/pantheon/backend/leaderboard/identifier"
	"code.justin.tv/commerce/pantheon/clients/tenant"
	"code.justin.tv/commerce/pantheon/config"
	sqs_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/clients/sqs"
	tenant_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/clients/tenant"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestDeleteLeaderboards(t *testing.T) {
	Convey("Given an api", t, func() {
		sqsClient := new(sqs_mock.Client)
		tenantClient := new(tenant_mock.Landlord)

		api := DeleteLeaderboardAPIImpl{
			SQSClient:    sqsClient,
			TenantClient: tenantClient,
		}

		domain := "test-domain"
		groupingKey := "test-grouping-key"
		tenantSQSQueueURL := "queue-url"
		ctx := context.Background()
		tenantClient.On("GetTenant", identifier.Domain(domain)).Return(tenant.Tenant{
			Config: config.TenantConfig{
				PDMSDeleteLeaderboardsQueueConfig: &config.QueueConfig{
					URL: tenantSQSQueueURL,
				},
			},
		})

		Convey("Given a config", func() {
			Convey("When sqs queue publish errors", func() {
				sqsClient.On("SendMessage", ctx, tenantSQSQueueURL, mock.Anything).Return(errors.New("test-error"))

				Convey("Should not return an error", func() {
					_, err := api.DeleteLeaderboards(context.Background(), &pantheonrpc.DeleteLeaderboardsReq{
						Domain:      domain,
						GroupingKey: groupingKey,
					})

					So(err, ShouldNotBeNil)
				})
			})

			Convey("When sqs queue publish succeeds", func() {
				sqsClient.On("SendMessage", ctx, tenantSQSQueueURL, mock.Anything).Return(nil)

				Convey("We should succeed", func() {
					resp, err := api.DeleteLeaderboards(context.Background(), &pantheonrpc.DeleteLeaderboardsReq{
						Domain:      domain,
						GroupingKey: groupingKey,
					})

					So(resp, ShouldNotBeNil)
					So(err, ShouldBeNil)
				})
			})
		})
	})
}
