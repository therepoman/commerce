package api

import (
	"context"
	"encoding/json"
	"math/rand"
	"time"

	"code.justin.tv/commerce/pantheon/backend/leaderboard/identifier"
	sfn_workers "code.justin.tv/commerce/pantheon/backend/sfn"
	"code.justin.tv/commerce/pantheon/clients/sqs"
	"code.justin.tv/commerce/pantheon/clients/tenant"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
)

type DeleteEntriesAPIImpl struct {
	TenantClient tenant.Landlord `inject:""`
	SQSClient    sqs.Client      `inject:""`
}

type DeleteEntriesAPI interface {
	DeleteEntries(ctx context.Context, req *pantheonrpc.DeleteEntriesReq) (*pantheonrpc.DeleteEntriesResp, error)
}

func (d *DeleteEntriesAPIImpl) DeleteEntries(ctx context.Context, req *pantheonrpc.DeleteEntriesReq) (*pantheonrpc.DeleteEntriesResp, error) {
	tenantQueueURL := d.TenantClient.GetTenant(identifier.Domain(req.Domain)).Config.PDMSDeleteEntriesQueueConfig.URL

	// Random number between 0 and 12hr
	rand.Seed(time.Now().UnixNano())
	waitSeconds := rand.Intn(12 * 60 * 60)

	executionInput := sfn_workers.DeleteEntriesStateMachineInput{
		Domain:      req.Domain,
		EntryKey:    req.EntryKey,
		WaitSeconds: waitSeconds,
	}

	executionInputBytes, err := json.Marshal(executionInput)
	if err != nil {
		return nil, err
	}

	err = d.SQSClient.SendMessage(ctx, tenantQueueURL, string(executionInputBytes))
	if err != nil {
		return nil, err
	}

	return &pantheonrpc.DeleteEntriesResp{}, nil
}
