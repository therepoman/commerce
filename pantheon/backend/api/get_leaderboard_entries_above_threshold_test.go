package api

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/pantheon/backend/leaderboard/entry"
	leaderboard_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/backend/leaderboard"
	redis_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/backend/leaderboard/redis"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestGetLeaderboardEntriesAboveThreshold(t *testing.T) {
	Convey("given a GetLeaderboardEntriesAboveThreshold API", t, func() {
		mockLeaderboardFactory := new(redis_mock.LeaderboardFactory)
		mockLeaderboard := new(leaderboard_mock.Leaderboard)
		var mockThreshold int64 = 100
		var mockLimit int64 = 1
		var mockOffset int64 = 0
		mockReq := &pantheonrpc.GetLeaderboardEntriesAboveThresholdReq{
			Domain:      "some-domain",
			GroupingKey: "some-grouping-key",
			TimeUnit:    pantheonrpc.TimeUnit_ALLTIME,
			Threshold:   mockThreshold,
			Limit:       mockLimit,
			Offset:      mockOffset,
		}

		g := GetLeaderboardEntriesAboveThresholdAPIImpl{
			LeaderboardFactory: mockLeaderboardFactory,
		}

		Convey("when request is nil", func() {
			Convey("we should return an error", func() {
				buckets, err := g.GetLeaderboardEntriesAboveThreshold(context.Background(), nil)
				So(buckets, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when GetLeaderboard returns an error", func() {
			mockLeaderboardFactory.On("GetRedisLeaderboard", mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("ERROR")).Once()

			Convey("we should return the error", func() {
				buckets, err := g.GetLeaderboardEntriesAboveThreshold(context.Background(), mockReq)
				So(buckets, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when GetLeaderboard does not return an error", func() {
			Convey("when a call to GetEntriesAboveThreshold errors", func() {
				mockLeaderboard.On("GetEntriesAboveThreshold", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("ERROR")).Once()
				mockLeaderboardFactory.On("GetRedisLeaderboard", mock.Anything, mock.Anything, mock.Anything).Return(mockLeaderboard, nil).Once()

				Convey("we should return the error", func() {
					buckets, err := g.GetLeaderboardEntriesAboveThreshold(context.Background(), mockReq)
					So(buckets, ShouldBeNil)
					So(err, ShouldNotBeNil)
				})
			})

			Convey("when the call to GetEntriesAboveThreshold succeeds", func() {
				mockEntries := []entry.RankedEntry{
					{
						Rank: entry.Rank(1),
					},
				}

				mockLeaderboard.On("GetEntriesAboveThreshold", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(mockEntries, nil).Once()
				mockLeaderboardFactory.On("GetRedisLeaderboard", mock.Anything, mock.Anything, mock.Anything).Return(mockLeaderboard, nil).Once()

				Convey("we should return the entries", func() {
					entries, err := g.GetLeaderboardEntriesAboveThreshold(context.Background(), mockReq)
					So(err, ShouldBeNil)
					So(entries, ShouldResemble, mockEntries)
				})
			})
		})
	})
}
