package api

import (
	"context"
	"math/rand"

	"code.justin.tv/commerce/pantheon/backend/leaderboard/domain"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/event"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/identifier"
	"code.justin.tv/commerce/pantheon/clients/sqs"
	"code.justin.tv/commerce/pantheon/clients/tenant"
	"code.justin.tv/commerce/pantheon/config"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
)

type PublishEventAPIImpl struct {
	SQSClient           sqs.Client           `inject:""`
	TenantClient        tenant.Landlord      `inject:""`
	Config              *config.Config       `inject:""`
	DomainConfigManager domain.ConfigManager `inject:""`
}

type PublishEventAPI interface {
	PublishEvent(ctx context.Context, req *pantheonrpc.PublishEventReq) (*pantheonrpc.PublishEventResp, error)
}

func (api *PublishEventAPIImpl) PublishEvent(ctx context.Context, req *pantheonrpc.PublishEventReq) (*pantheonrpc.PublishEventResp, error) {
	reqEvent := event.FromPublishEventRequest(req)

	queueURL := api.TenantClient.GetTenant(identifier.Domain(reqEvent.Domain)).Config.EventQueueConfig.URL

	var publishDelaySeconds int
	domainConfig := api.DomainConfigManager.Get(identifier.Domain(req.Domain))
	if domainConfig.MaxPublishDelaySeconds > 0 {
		publishDelaySeconds = rand.Intn(domainConfig.MaxPublishDelaySeconds + 1)
	}

	eventJSON, err := event.ToJSON(reqEvent)
	if err != nil {
		return nil, err
	}

	if publishDelaySeconds > 0 {
		err = api.SQSClient.SendMessageWithDelay(ctx, queueURL, eventJSON, int64(publishDelaySeconds))
	} else {
		err = api.SQSClient.SendMessage(ctx, queueURL, eventJSON)
	}
	if err != nil {
		return nil, err
	}

	resp := &pantheonrpc.PublishEventResp{}
	return resp, nil
}
