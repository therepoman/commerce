package api

import (
	"context"
	"errors"
	"math/rand"
	"testing"

	"code.justin.tv/commerce/pantheon/backend/leaderboard/domain"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/identifier"
	"code.justin.tv/commerce/pantheon/clients/tenant"
	"code.justin.tv/commerce/pantheon/config"
	domain_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/backend/leaderboard/domain"
	sqs_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/clients/sqs"
	tenant_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/clients/tenant"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestPublishEvent(t *testing.T) {
	Convey("Given an api", t, func() {
		sqsClient := new(sqs_mock.Client)
		tenantClient := new(tenant_mock.Landlord)
		domainConfigManager := new(domain_mock.ConfigManager)

		api := PublishEventAPIImpl{
			SQSClient:           sqsClient,
			TenantClient:        tenantClient,
			DomainConfigManager: domainConfigManager,
		}

		testDomain := "test-domain"
		entryKey := "test-entry-key"
		groupingKey := "test-grouping-key"
		tenantSQSQueueURL := "queue-url"
		ctx := context.Background()
		tenantClient.On("GetTenant", identifier.Domain(testDomain)).Return(tenant.Tenant{
			Config: config.TenantConfig{
				EventQueueConfig: &config.QueueConfig{
					URL: tenantSQSQueueURL,
				},
			},
		})

		Convey("Sends SQS message", func() {
			sqsClient.On("SendMessage", ctx, tenantSQSQueueURL, mock.Anything).Return(nil)
			domainConfigManager.On("Get", identifier.Domain(testDomain)).Return(domain.Config{})

			_, err := api.PublishEvent(context.Background(), &pantheonrpc.PublishEventReq{
				Domain:      testDomain,
				EntryKey:    entryKey,
				GroupingKey: groupingKey,
				EventValue:  1,
				EventId:     "12345",
			})

			So(err, ShouldBeNil)
			sqsClient.AssertExpectations(t)
		})

		Convey("Sends delayed SQS message", func() {
			sqsClient.On("SendMessageWithDelay", ctx, tenantSQSQueueURL, mock.Anything, mock.Anything).Return(nil)

			Convey("When domain config has non-zero publish delay", func() {
				// simply way to get a non-zero number when calculating jitter
				rand.Seed(1337)
				domainConfigManager.On("Get", identifier.Domain(testDomain)).Return(domain.Config{
					MaxPublishDelaySeconds: 10,
				})
			})

			_, err := api.PublishEvent(context.Background(), &pantheonrpc.PublishEventReq{
				Domain:      testDomain,
				EntryKey:    entryKey,
				GroupingKey: groupingKey,
				EventValue:  1,
				EventId:     "12345",
			})

			So(err, ShouldBeNil)
			sqsClient.AssertExpectations(t)
		})

		Convey("Returns an error", func() {
			domainConfigManager.On("Get", identifier.Domain(testDomain)).Return(domain.Config{})

			Convey("When sqs queue publish errors", func() {
				sqsClient.On("SendMessage", ctx, tenantSQSQueueURL, mock.Anything).Return(errors.New("test-error"))
			})

			_, err := api.PublishEvent(context.Background(), &pantheonrpc.PublishEventReq{
				Domain:      testDomain,
				EntryKey:    entryKey,
				GroupingKey: groupingKey,
				EventValue:  1,
				EventId:     "12345",
			})

			So(err, ShouldNotBeNil)
		})
	})
}
