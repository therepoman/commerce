package api

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/pantheon/backend/leaderboard/identifier"
	"code.justin.tv/commerce/pantheon/clients/tenant"
	"code.justin.tv/commerce/pantheon/config"
	sqs_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/clients/sqs"
	tenant_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/clients/tenant"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestDeleteEntries(t *testing.T) {
	Convey("Given an api", t, func() {
		sqsClient := new(sqs_mock.Client)
		tenantClient := new(tenant_mock.Landlord)

		api := DeleteEntriesAPIImpl{
			SQSClient:    sqsClient,
			TenantClient: tenantClient,
		}

		domain := "test-domain"
		entryKey := "test-entry-key"
		tenantSQSQueueURL := "queue-url"
		ctx := context.Background()
		tenantClient.On("GetTenant", identifier.Domain(domain)).Return(tenant.Tenant{
			Config: config.TenantConfig{
				PDMSDeleteEntriesQueueConfig: &config.QueueConfig{
					URL: tenantSQSQueueURL,
				},
			},
		})

		Convey("Given a config", func() {
			Convey("When sqs queue publish errors", func() {
				sqsClient.On("SendMessage", ctx, tenantSQSQueueURL, mock.Anything).Return(errors.New("test-error"))

				Convey("Should not return an error", func() {
					_, err := api.DeleteEntries(context.Background(), &pantheonrpc.DeleteEntriesReq{
						Domain:   domain,
						EntryKey: entryKey,
					})

					So(err, ShouldNotBeNil)
				})
			})

			Convey("When sqs queue publish succeeds", func() {
				sqsClient.On("SendMessage", ctx, tenantSQSQueueURL, mock.Anything).Return(nil)

				Convey("We should succeed", func() {
					resp, err := api.DeleteEntries(context.Background(), &pantheonrpc.DeleteEntriesReq{
						Domain:   domain,
						EntryKey: entryKey,
					})

					So(resp, ShouldNotBeNil)
					So(err, ShouldBeNil)
				})
			})
		})
	})
}
