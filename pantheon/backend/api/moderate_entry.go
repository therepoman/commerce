package api

import (
	"context"

	"code.justin.tv/commerce/pantheon/backend/leaderboard/identifier"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/moderation"
	"code.justin.tv/commerce/pantheon/clients/sqs"
	"code.justin.tv/commerce/pantheon/clients/tenant"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
)

type ModerateEntryAPIImpl struct {
	SQSClient    sqs.Client      `inject:""`
	TenantClient tenant.Landlord `inject:""`
}

type ModerateEntryAPI interface {
	ModerateEntry(ctx context.Context, req *pantheonrpc.ModerateEntryReq) (*pantheonrpc.ModerateEntryResp, error)
}

func (m *ModerateEntryAPIImpl) ModerateEntry(ctx context.Context, req *pantheonrpc.ModerateEntryReq) (*pantheonrpc.ModerateEntryResp, error) {
	queueURL := m.TenantClient.GetTenant(identifier.Domain(req.Domain)).Config.ModerationNonFifoQueueConfig.URL

	reqModeration := moderation.FromModerateEntryRequest(req)

	moderationJSON, err := moderation.ToJSON(reqModeration)
	if err != nil {
		return nil, err
	}

	err = m.SQSClient.SendMessage(ctx, queueURL, moderationJSON)
	if err != nil {
		return nil, err
	}

	return &pantheonrpc.ModerateEntryResp{}, nil
}
