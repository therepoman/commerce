package api

import (
	"fmt"

	"github.com/uber-go/tally"

	lrucachemock "code.justin.tv/commerce/gogogadget/mocks/code.justin.tv/commerce/gogogadget/lrucache"
	lrucacheitemmock "code.justin.tv/commerce/gogogadget/mocks/code.justin.tv/commerce/gogogadget/lrucacheitem"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/domain"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/entry"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/score"

	"context"
	"errors"
	"testing"

	leaderboardmock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/backend/leaderboard"
	"github.com/go-redis/redis"

	domainmock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/backend/leaderboard/domain"

	redismock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/backend/leaderboard/redis"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestGetLeaderboard(t *testing.T) {
	Convey("GetLeaderboard API", t, func() {
		mockRedisLeaderboardFactory := new(redismock.LeaderboardFactory)
		mockLeaderboard := new(leaderboardmock.Leaderboard)
		mockDomainConfigManager := new(domainmock.ConfigManager)
		mockLRUCache := new(lrucachemock.ILRUCache)
		mockLRUCacheItem := new(lrucacheitemmock.ICacheItem)

		topN := int64(10)
		contextN := int64(10)
		mockReq := &pantheonrpc.GetLeaderboardReq{
			Domain:           "some-domain",
			GroupingKey:      "some-grouping-key",
			ContextEntryKey:  "some-context-entry-key",
			TimeUnit:         pantheonrpc.TimeUnit_ALLTIME,
			TopN:             topN,
			ContextN:         contextN,
			UseInMemoryCache: true,
		}
		pubsubID := fmt.Sprintf("%s-%s-%s", mockReq.Domain, mockReq.GroupingKey, mockReq.TimeUnit.String())
		key := fmt.Sprintf("%s/%s/%s/%d/%d", mockReq.Domain, mockReq.GroupingKey, mockReq.TimeUnit.String(), 0, mockReq.TopN)

		g := GetLeaderboardAPIImpl{
			RootMetricsScope:    tally.NoopScope,
			LeaderboardFactory:  mockRedisLeaderboardFactory,
			LRUCache:            mockLRUCache,
			DomainConfigManager: mockDomainConfigManager,
		}

		item := entry.RankedEntry{
			Entry: entry.Entry{
				Key: "some entry key",
				Score: score.EntryScore{
					ModeratedFlag: false,
					BaseScore:     10,
					EventTime:     0,
				},
			},
			Rank: 1,
		}
		mockDomainConfigManager.On("Get", mock.Anything).Return(domain.UnknownDomainConfig())

		Convey("when request is nil", func() {
			Convey("we should return an empty leaderboard", func() {
				leaderboard, err := g.GetLeaderboard(context.Background(), nil)
				So(leaderboard, ShouldResemble, LeaderboardSummary{})
				So(err, ShouldBeNil)
			})
		})

		Convey("when the requested item is not in the lru cache", func() {
			mockLRUCache.On("GetItem", mock.Anything).Return(mockLRUCacheItem, false)

			Convey("when GetRedisLeaderboard returns an error", func() {
				mockRedisLeaderboardFactory.On("GetRedisLeaderboard", mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("ERROR")).Once()

				Convey("we should return the error", func() {
					leaderboard, err := g.GetLeaderboard(context.Background(), mockReq)
					So(leaderboard, ShouldResemble, LeaderboardSummary{})
					So(err, ShouldNotBeNil)
				})
			})

			Convey("when GetRedisLeaderboard does not return an error", func() {
				mockRedisLeaderboardFactory.On("GetRedisLeaderboard", mock.Anything, mock.Anything, mock.Anything).Return(mockLeaderboard, nil).Once()

				Convey("when redis returns nil for the top entries in the leaderboard", func() {
					ctx := context.Background()
					mockLeaderboard.On("GetTopWithTimeoutAndRetry", ctx, mock.Anything, mock.Anything, GetTopTimeout, GetTopMaxAttempts).Return(nil, redis.Nil)
					mockLRUCache.On("AddItem", key, []entry.RankedEntry{}, LeaderboardCacheTTL).Return(nil)

					leaderboard, err := g.GetLeaderboard(ctx, mockReq)
					So(leaderboard, ShouldResemble, LeaderboardSummary{
						Identifier:   pubsubID,
						Top:          []entry.RankedEntry{},
						EntryContext: nil,
						Entry:        entry.RankedEntry{},
					})
					So(err, ShouldBeNil)
					mockLeaderboard.AssertExpectations(t)
				})

				Convey("when redis returns an error", func() {
					ctx := context.Background()
					mockLeaderboard.On("GetTopWithTimeoutAndRetry", ctx, mock.Anything, mock.Anything, GetTopTimeout, GetTopMaxAttempts).Return(nil, errors.New("REDIS ERROR"))

					leaderboard, err := g.GetLeaderboard(ctx, mockReq)
					So(leaderboard, ShouldResemble, LeaderboardSummary{})
					So(err, ShouldNotBeNil)
					mockLeaderboard.AssertExpectations(t)
				})

				Convey("when redis returns the top entries for the leaderboard", func() {
					topEntries := []entry.RankedEntry{item}

					ctx := context.Background()
					mockLeaderboard.On("GetTopWithTimeoutAndRetry", ctx, mock.Anything, mock.Anything, GetTopTimeout, GetTopMaxAttempts).Return(topEntries, nil)
					mockLeaderboard.On("GetEntryContextWithTimeoutAndRetry", ctx, mock.Anything, mock.Anything, mock.Anything, GetEntryContextTimeout, GetEntryContextMaxAttempts).Return(entry.RankedEntry{}, []entry.RankedEntry{}, nil)

					Convey("it adds the item to the lru cache", func() {
						mockLRUCache.On("AddItem", key, topEntries, LeaderboardCacheTTL).Return(nil)

						leaderboard, err := g.GetLeaderboard(ctx, mockReq)
						So(leaderboard, ShouldResemble, LeaderboardSummary{
							Identifier:   pubsubID,
							Top:          topEntries,
							EntryContext: []entry.RankedEntry{},
							Entry:        entry.RankedEntry{},
						})
						So(err, ShouldBeNil)
						mockLeaderboard.AssertExpectations(t)
					})

				})
			})

			Convey("when the request specifies to not use the in memory cache ", func() {
				ctx := context.Background()
				mockRedisLeaderboardFactory.On("GetRedisLeaderboard", mock.Anything, mock.Anything, mock.Anything).Return(mockLeaderboard, nil).Once()
				mockLeaderboard.On("GetEntryContextWithTimeoutAndRetry", ctx, mock.Anything, mock.Anything, mock.Anything, GetEntryContextTimeout, GetEntryContextMaxAttempts).Return(entry.RankedEntry{}, []entry.RankedEntry{}, nil)

				Convey("it queries redis for the top entries and adds the item to the lru cache", func() {
					topEntries := []entry.RankedEntry{item}
					mockLeaderboard.On("GetTopWithTimeoutAndRetry", ctx, mock.Anything, mock.Anything, GetTopTimeout, GetTopMaxAttempts).Return(topEntries, nil)
					mockLRUCache.On("AddItem", key, topEntries, LeaderboardCacheTTL).Return(nil)

					mockReqNoCache := mockReq
					mockReqNoCache.UseInMemoryCache = false
					leaderboard, err := g.GetLeaderboard(ctx, mockReqNoCache)
					So(leaderboard, ShouldResemble, LeaderboardSummary{
						Identifier:   pubsubID,
						Top:          topEntries,
						EntryContext: []entry.RankedEntry{},
						Entry:        entry.RankedEntry{},
					})
					So(err, ShouldBeNil)
					mockLeaderboard.AssertExpectations(t)
				})
			})
		})

		Convey("when the requested item is in the lru cache", func() {
			mockLRUCache.On("GetItem", mock.Anything).Return(mockLRUCacheItem, true)

			Convey("when topEntries is an empty slice it returns an empty LeaderboardSummary", func() {
				topEntries := []entry.RankedEntry{}
				mockLRUCacheItem.On("Value").Return(topEntries)
				leaderboard, err := g.GetLeaderboard(context.Background(), mockReq)
				So(leaderboard, ShouldResemble, LeaderboardSummary{
					Identifier:   pubsubID,
					Top:          topEntries,
					EntryContext: nil,
					Entry:        entry.RankedEntry{},
				})
				So(err, ShouldBeNil)
				mockLeaderboard.AssertExpectations(t)
			})

			Convey("it doesn't call into GetEntryContextWithTimeoutAndRetry if there is no ContextEntryKey in the request", func() {
				topEntries := []entry.RankedEntry{item}
				mockLRUCacheItem.On("Value").Return(topEntries)
				mockReqNoContextEntryKey := mockReq
				mockReqNoContextEntryKey.ContextEntryKey = ""
				leaderboard, err := g.GetLeaderboard(context.Background(), mockReqNoContextEntryKey)
				So(leaderboard, ShouldResemble, LeaderboardSummary{
					Identifier:   pubsubID,
					Top:          topEntries,
					EntryContext: nil,
					Entry:        entry.RankedEntry{},
				})
				So(err, ShouldBeNil)
				mockLeaderboard.AssertExpectations(t)
			})

			Convey("it returns the cached entry", func() {
				ctx := context.Background()
				mockRedisLeaderboardFactory.On("GetRedisLeaderboard", mock.Anything, mock.Anything, mock.Anything).Return(mockLeaderboard, nil).Once()
				mockLeaderboard.On("GetEntryContextWithTimeoutAndRetry", ctx, mock.Anything, mock.Anything, mock.Anything, GetEntryContextTimeout, GetEntryContextMaxAttempts).Return(entry.RankedEntry{}, []entry.RankedEntry{}, nil)
				topEntries := []entry.RankedEntry{item}
				mockLRUCacheItem.On("Value").Return(topEntries)
				leaderboard, err := g.GetLeaderboard(ctx, mockReq)
				So(leaderboard, ShouldResemble, LeaderboardSummary{
					Identifier:   pubsubID,
					Top:          topEntries,
					EntryContext: []entry.RankedEntry{},
					Entry:        entry.RankedEntry{},
				})
				So(err, ShouldBeNil)
				mockLeaderboard.AssertExpectations(t)
			})
		})
	})
}
