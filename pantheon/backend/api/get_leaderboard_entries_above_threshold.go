package api

import (
	"context"
	"errors"
	"time"

	"code.justin.tv/commerce/pantheon/backend/leaderboard"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/bucket"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/entry"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/identifier"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
)

type GetLeaderboardEntriesAboveThresholdAPIImpl struct {
	LeaderboardFactory leaderboard.RedisLeaderboardFactory `inject:""`
}

type GetLeaderboardEntriesAboveThresholdAPI interface {
	GetLeaderboardEntriesAboveThreshold(ctx context.Context, req *pantheonrpc.GetLeaderboardEntriesAboveThresholdReq) ([]entry.RankedEntry, error)
}

func (g *GetLeaderboardEntriesAboveThresholdAPIImpl) GetLeaderboardEntriesAboveThreshold(ctx context.Context, req *pantheonrpc.GetLeaderboardEntriesAboveThresholdReq) ([]entry.RankedEntry, error) {
	if req == nil {
		return nil, errors.New("request is nil")
	}

	t := time.Now()
	if req.Time != uint64(0) {
		t = time.Unix(0, int64(req.Time))
	}

	timeBucket := bucket.TimestampToTimeBucket(t, req.TimeUnit)

	id := identifier.Identifier{
		Domain:              identifier.Domain(req.Domain),
		GroupingKey:         identifier.GroupingKey(req.GroupingKey),
		TimeAggregationUnit: req.TimeUnit,
		TimeBucket:          timeBucket,
	}

	lb, err := g.LeaderboardFactory.GetRedisLeaderboard(ctx, id, leaderboard.RetrievalSettings{})

	if err != nil {
		return nil, err
	}

	entries, err := lb.GetEntriesAboveThreshold(req.Threshold, req.Limit, req.Offset, leaderboard.RetrievalSettings{})

	if err != nil {
		return nil, err
	}

	return entries, nil
}
