package api

import (
	"context"
	"fmt"
	"time"

	"code.justin.tv/commerce/gogogadget/lrucache"
	"code.justin.tv/commerce/pantheon/backend/leaderboard"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/bucket"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/domain"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/entry"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/identifier"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/score"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	"code.justin.tv/commerce/pantheon/utils/math"
	"github.com/go-redis/redis"
	"github.com/pkg/errors"
	"github.com/uber-go/tally"
)

const (
	TopDefault                   = 10
	SpreadDefault                = 5
	SpreadOne                    = 1
	GetEntryContextTimeout       = 50 * time.Millisecond
	GetEntryContextMaxAttempts   = 2
	GetTopTimeout                = 50 * time.Millisecond
	GetTopMaxAttempts            = 2
	LeaderboardCacheTTL          = 1 * time.Minute
	leaderboardLRUCacheHitMetric = "leaderboard-lru-cache-hit"
)

type GetLeaderboardAPIImpl struct {
	RootMetricsScope    tally.Scope                         `inject:""`
	LRUCache            lrucache.ILRUCache                  `inject:""`
	DomainConfigManager domain.ConfigManager                `inject:""`
	LeaderboardFactory  leaderboard.RedisLeaderboardFactory `inject:""`
}

type GetLeaderboardAPI interface {
	GetLeaderboard(ctx context.Context, req *pantheonrpc.GetLeaderboardReq) (LeaderboardSummary, error)
}

type LeaderboardSummary struct {
	Identifier   string
	Top          []entry.RankedEntry
	EntryContext []entry.RankedEntry
	Entry        entry.RankedEntry
	StartTime    *time.Time
	EndTime      *time.Time
}

func (g *GetLeaderboardAPIImpl) GetLeaderboard(ctx context.Context, req *pantheonrpc.GetLeaderboardReq) (LeaderboardSummary, error) {
	if req == nil {
		return LeaderboardSummary{}, nil
	}

	// If no time is passed in, use the current timestamp to find the appropriate time bucket
	// We also can skip importing missing LBs from S3 (they haven't had time to be archived yet)
	t := time.Now()
	skipImport := true

	// If a time is passed in, use that time to find the correct time bucket
	// Also make sure to attempt importing from S3 if the LB is not found in redis
	if req.Time != uint64(0) {
		t = time.Unix(0, int64(req.Time))
		skipImport = false
	}

	// Reduce the timestamp down to a time bucket (ex: a specific day / week / month / etc)
	timeBucket := bucket.TimestampToTimeBucket(t, req.TimeUnit)

	id := identifier.Identifier{
		Domain:              identifier.Domain(req.Domain),
		GroupingKey:         identifier.GroupingKey(req.GroupingKey),
		TimeAggregationUnit: req.TimeUnit,
		TimeBucket:          timeBucket,
	}

	domainConfig := g.DomainConfigManager.Get(identifier.Domain(req.Domain))

	topN := TopDefault
	if req.TopN > 0 {
		topN = math.MinInt(int(req.TopN), domainConfig.TopMax)
	}

	key := fmt.Sprintf("%s/%d", id.Key(), topN)
	var topEntries []entry.RankedEntry
	var lb leaderboard.Leaderboard
	var err error
	var item lrucache.ICacheItem
	var ok = false

	if req.UseInMemoryCache {
		item, ok = g.LRUCache.GetItem(key)
	}

	if !ok {
		lb, err = g.LeaderboardFactory.GetRedisLeaderboard(ctx, id, leaderboard.RetrievalSettings{
			SkipImport: skipImport,
		})

		if err != nil {
			return LeaderboardSummary{}, errors.Errorf("GetRedisLeaderboard failed with: %v", err)
		}

		topEntries, err = lb.GetTopWithTimeoutAndRetry(ctx, topN, leaderboard.RetrievalSettings{}, GetTopTimeout, GetTopMaxAttempts)
		if err == redis.Nil {
			// add cache entry for empty leaderboard
			topEntries = []entry.RankedEntry{}
		} else if err != nil {
			return LeaderboardSummary{}, errors.Errorf("GetTop failed with: %v", err)
		}

		g.LRUCache.AddItem(key, topEntries, LeaderboardCacheTTL)
		if req.UseInMemoryCache {
			g.RootMetricsScope.Tagged(getMetricTags(id.Domain)).Counter(leaderboardLRUCacheHitMetric).Inc(0)
		}
	} else {
		topEntries, ok = item.Value().([]entry.RankedEntry)
		if !ok {
			g.LRUCache.RemoveItem(key)
			return LeaderboardSummary{}, errors.New(fmt.Sprintf("Leaderboard LRU cache has invalid value for entry %s", key))
		}
		g.RootMetricsScope.Tagged(getMetricTags(id.Domain)).Counter(leaderboardLRUCacheHitMetric).Inc(1)
	}

	var rankedEntry entry.RankedEntry
	var contextEntriesRanked []entry.RankedEntry

	// only process context request if there are entries
	if len(topEntries) > 0 {
		contextN := SpreadDefault
		if req.ContextN >= 0 {
			contextN = math.MinInt(int(req.ContextN), domainConfig.SpreadMax)
		}

		contextEntryKey := entry.Key(req.ContextEntryKey)
		if contextEntryKey != "" {
			// leaderboard can be nil here if entries were cached
			if lb == nil {
				lb, err = g.LeaderboardFactory.GetRedisLeaderboard(ctx, id, leaderboard.RetrievalSettings{
					SkipImport: skipImport,
				})

				if err != nil {
					return LeaderboardSummary{}, errors.Errorf("GetRedisLeaderboard for entry context failed with: %v", err)
				}
			}

			rankedEntry, contextEntriesRanked, err = lb.GetEntryContextWithTimeoutAndRetry(ctx, contextEntryKey, contextN, leaderboard.RetrievalSettings{}, GetEntryContextTimeout, GetEntryContextMaxAttempts)
			if err != nil && err != redis.Nil {
				return LeaderboardSummary{}, errors.Errorf("GetEntryContextWithTimeoutAndRetry failed with: %v", err)
			}
		}
	}

	startTime := timeBucket.ToStartTime(req.TimeUnit)
	endTime := timeBucket.ToExpirationTime(req.TimeUnit)
	pubsubID := id.PubsubIdentifier()

	return LeaderboardSummary{
		Identifier:   pubsubID,
		Top:          topEntries,
		EntryContext: contextEntriesRanked,
		Entry:        rankedEntry,
		StartTime:    startTime,
		EndTime:      endTime,
	}, nil
}

func ConvertModelToAPIEntries(entries []entry.RankedEntry) []*pantheonrpc.LeaderboardEntry {
	leaderboardEntries := make([]*pantheonrpc.LeaderboardEntry, 0)
	for _, e := range entries {
		apiEntry := ConvertModelToAPIEntry(e)
		if apiEntry != nil && !apiEntry.Moderated && apiEntry.Score > 0 {
			leaderboardEntries = append(leaderboardEntries, apiEntry)
		}
	}
	return leaderboardEntries
}

func ConvertModelToAPIEntry(entry entry.RankedEntry) *pantheonrpc.LeaderboardEntry {
	return &pantheonrpc.LeaderboardEntry{
		Rank:      int64(entry.Rank),
		Score:     int64(entry.Score.BaseScore),
		EntryKey:  string(entry.Key),
		Moderated: entry.Score.ModeratedFlag == score.Moderated,
	}
}

func getMetricTags(d identifier.Domain) map[string]string {
	return map[string]string{
		"domain": string(d),
	}
}
