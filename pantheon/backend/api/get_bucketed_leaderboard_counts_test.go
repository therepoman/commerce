package api

import (
	"context"
	"errors"
	"testing"

	leaderboard_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/backend/leaderboard"
	redis_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/backend/leaderboard/redis"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestGetBucketedLeaderboardCounts(t *testing.T) {
	Convey("given a GetBucketedLeaderboardCounts API", t, func() {
		mockRedisLeaderboardFactory := new(redis_mock.LeaderboardFactory)
		mockLeaderboard := new(leaderboard_mock.Leaderboard)
		var mockThreshold int64 = 100
		mockReq := &pantheonrpc.GetBucketedLeaderboardCountsReq{
			Domain:           "some-domain",
			GroupingKey:      "some-grouping-key",
			TimeUnit:         pantheonrpc.TimeUnit_ALLTIME,
			BucketThresholds: []int64{mockThreshold},
		}

		g := GetBucketedLeaderboardCountsAPIImpl{
			LeaderboardFactory: mockRedisLeaderboardFactory,
		}

		Convey("when request is nil", func() {
			Convey("we should return an error", func() {
				buckets, err := g.GetBucketedLeaderboardCounts(context.Background(), nil)
				So(buckets, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when GetLeaderboard returns an error", func() {
			mockRedisLeaderboardFactory.On("GetRedisLeaderboard", mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("ERROR")).Once()

			Convey("we should return the error", func() {
				buckets, err := g.GetBucketedLeaderboardCounts(context.Background(), mockReq)
				So(buckets, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when GetLeaderboard does not return an error", func() {
			Convey("when a call to GetCountForRange errors", func() {
				var mockCount int64 = 0
				mockLeaderboard.On("GetCountForRange", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(mockCount, errors.New("ERROR")).Once()
				mockRedisLeaderboardFactory.On("GetRedisLeaderboard", mock.Anything, mock.Anything, mock.Anything).Return(mockLeaderboard, nil).Once()

				Convey("we should return the error", func() {
					buckets, err := g.GetBucketedLeaderboardCounts(context.Background(), mockReq)
					So(buckets, ShouldBeNil)
					So(err, ShouldNotBeNil)
				})
			})

			Convey("when calls to GetCountForRange succeeds", func() {
				mockRedisLeaderboardFactory.On("GetRedisLeaderboard", mock.Anything, mock.Anything, mock.Anything).Return(mockLeaderboard, nil).Once()

				Convey("we should return the buckets", func() {
					var mockCount int64 = 1
					mockLeaderboard.On("GetCountForRange", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(mockCount, nil).Once()

					buckets, err := g.GetBucketedLeaderboardCounts(context.Background(), mockReq)
					So(err, ShouldBeNil)
					So(buckets, ShouldResemble, &[]*pantheonrpc.Bucket{
						{
							Count:     mockCount,
							Threshold: mockThreshold,
						},
					})
				})

				Convey("we should return the buckets when moderated entries are asked for", func() {
					var mockCount int64 = 2
					mockReq.IncludeModeratedEntries = true
					mockLeaderboard.On("GetCountForRange", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(mockCount, nil).Times(2)
					buckets, err := g.GetBucketedLeaderboardCounts(context.Background(), mockReq)
					So(err, ShouldBeNil)
					So(buckets, ShouldResemble, &[]*pantheonrpc.Bucket{
						{
							Count:     mockCount * 2,
							Threshold: mockThreshold,
						},
					})
				})
			})
		})
	})
}
