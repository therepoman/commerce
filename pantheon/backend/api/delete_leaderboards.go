package api

import (
	"context"
	"encoding/json"
	"math/rand"
	"time"

	"code.justin.tv/commerce/pantheon/backend/leaderboard/identifier"
	sfn_workers "code.justin.tv/commerce/pantheon/backend/sfn"
	"code.justin.tv/commerce/pantheon/clients/sqs"
	"code.justin.tv/commerce/pantheon/clients/tenant"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
)

type DeleteLeaderboardAPIImpl struct {
	TenantClient tenant.Landlord `inject:""`
	SQSClient    sqs.Client      `inject:""`
}

type DeleteLeaderboardsAPI interface {
	DeleteLeaderboards(ctx context.Context, req *pantheonrpc.DeleteLeaderboardsReq) (*pantheonrpc.DeleteLeaderboardsResp, error)
}

func (d *DeleteLeaderboardAPIImpl) DeleteLeaderboards(ctx context.Context, req *pantheonrpc.DeleteLeaderboardsReq) (*pantheonrpc.DeleteLeaderboardsResp, error) {
	tenantQueueURL := d.TenantClient.GetTenant(identifier.Domain(req.Domain)).Config.PDMSDeleteLeaderboardsQueueConfig.URL

	// Random number between 0 and 12hr
	rand.Seed(time.Now().UnixNano())
	waitSeconds := rand.Intn(12 * 60 * 60)

	executionInput := sfn_workers.DeleteLeaderboardsStateMachineInput{
		Domain:      req.Domain,
		GroupingKey: req.GroupingKey,
		WaitSeconds: waitSeconds,
	}

	executionInputBytes, err := json.Marshal(executionInput)
	if err != nil {
		return nil, err
	}

	err = d.SQSClient.SendMessage(ctx, tenantQueueURL, string(executionInputBytes))
	if err != nil {
		return nil, err
	}

	return &pantheonrpc.DeleteLeaderboardsResp{}, nil
}
