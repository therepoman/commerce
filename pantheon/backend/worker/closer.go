package worker

import (
	"context"
	"io"
)

type closerTerminator struct {
	io.Closer
	name string
}

func (ct *closerTerminator) Shutdown(ctx context.Context) error {
	err := ct.Close()
	return err
}

func (ct *closerTerminator) Name() string {
	return ct.name
}

func NewCloserTerminator(name string, closer io.Closer) NamedTerminator {
	return &closerTerminator{
		Closer: closer,
		name:   name,
	}
}
