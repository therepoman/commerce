package worker

import "context"

type NamedTerminator interface {
	Terminator
	Name() string
}

type Terminator interface {
	Shutdown(ctx context.Context) error
}
