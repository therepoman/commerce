package redispubsub_test

import (
	"context"
	"testing"
	"time"

	"code.justin.tv/commerce/pantheon/backend/redispubsub"
	"code.justin.tv/commerce/pantheon/clients/lock"
	"code.justin.tv/commerce/pantheon/clients/redis"
	redispubsub_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/backend/redispubsub"
	. "code.justin.tv/commerce/pantheon/test/redis"
	goRedis "github.com/go-redis/redis"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestRedisPubsubManager(t *testing.T) {
	Convey("Given a redis client and mock worker", t, WithRedis(func(usingLocal bool, redisClient redis.Client, lockingClient lock.LockingClient) {
		if !usingLocal {
			t.Skip("Skipping pubsub test. Pass the test arg redis.local to run this")
		}

		mockWorker := new(redispubsub_mock.Worker)
		Convey("Can create a new redis pubsub worker manager", func() {

			manager := redispubsub.NewRedisPubsubWorkerManager("queuename", redisClient, mockWorker)
			So(manager, ShouldNotBeNil)

			// E = Keyevents - Receive pubsub notifications for events on redis keys
			// x = Expired - Receive pubsub notifications when keys expire
			_, err := redisClient.ConfigSet(context.Background(), "notify-keyspace-events", "Ex")
			So(err, ShouldBeNil)

			Convey("Worker gets called", func() {
				mockWorker.On("Handle", mock.Anything).Return(nil)
				_, _ = redisClient.Set(context.Background(), "test-key", "test-value", time.Millisecond)
				time.Sleep(100 * time.Millisecond)
				_, err := redisClient.Get(context.Background(), "test-key", redis.ReadSettings{})
				So(err, ShouldEqual, goRedis.Nil)
				time.Sleep(100 * time.Millisecond)
				mockWorker.AssertCalled(t, "Handle", mock.Anything)
			})

			Convey("Worker manager can be shutdown", func() {
				err := manager.Shutdown(context.Background())
				So(err, ShouldBeNil)
			})
		})
	}))
}
