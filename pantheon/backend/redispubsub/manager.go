package redispubsub

import (
	"context"
	"errors"
	"fmt"
	"sync"

	"code.justin.tv/commerce/pantheon/clients/redis"
	goRedis "github.com/go-redis/redis"
	log "github.com/sirupsen/logrus"
)

const (
	allKeysChannel = "__key*__:*"
)

type Worker interface {
	Handle(msg *goRedis.Message) error
}

type WorkerManager interface {
	Name() string
	Shutdown(ctx context.Context) error
}

type WorkerManagerImpl struct {
	name        string
	worker      Worker
	waitGroup   *sync.WaitGroup
	stopChannel chan struct{}
}

func NewRedisPubsubWorkerManager(workerName string, redisClient redis.Client, worker Worker) WorkerManager {
	manager := &WorkerManagerImpl{
		name:        workerName,
		worker:      worker,
		stopChannel: make(chan struct{}),
		waitGroup:   new(sync.WaitGroup),
	}

	rPS := redisClient.PSubscribe(allKeysChannel)
	rPSChannel := rPS.Channel()
	manager.waitGroup.Add(1)
	go func() {
		for {
			select {
			case msg := <-rPSChannel:
				if err := worker.Handle(msg); err != nil {
					errMsg := fmt.Sprintf("Error processing message in pubsub worker: %s", workerName)
					log.Error(errMsg, err)
				}
			case <-manager.stopChannel:
				manager.waitGroup.Done()
				return
			}
		}
	}()

	return manager
}

func (mgr *WorkerManagerImpl) Name() string {
	return mgr.name
}

func (mgr *WorkerManagerImpl) Shutdown(ctx context.Context) error {
	close(mgr.stopChannel)
	return wait(ctx, mgr.waitGroup)
}

func wait(ctx context.Context, wg *sync.WaitGroup) error {
	workersDone := make(chan struct{})
	go func() {
		defer close(workersDone)
		wg.Wait()
	}()

	select {
	case <-workersDone:
		return nil
	case <-ctx.Done():
		return errors.New("Timed out while waiting for pubsub worker to complete")
	}
}
