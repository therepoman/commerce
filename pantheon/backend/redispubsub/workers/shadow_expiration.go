package workers

import (
	"context"
	"strings"
	"time"

	"code.justin.tv/commerce/pantheon/backend/leaderboard/archival"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/identifier"
	"code.justin.tv/commerce/pantheon/clients/sqs"
	"code.justin.tv/commerce/pantheon/clients/tenant"
	"github.com/go-redis/redis"
	"github.com/sirupsen/logrus"
)

const (
	timeoutTime        = 30 * time.Second
	shadowSuffix       = "-shadow"
	archivalLockSuffix = "-archival-lock"
)

type ShadowLeaderboardExpirationWorker struct {
	SQSClient    sqs.Client      `inject:""`
	TenantClient tenant.Landlord `inject:""`
}

func (w *ShadowLeaderboardExpirationWorker) Handle(msg *redis.Message) error {
	ctx, cancel := context.WithTimeout(context.Background(), timeoutTime)
	defer cancel()

	if !isShadowKey(msg.Payload) {
		if !isExpectedExpiringKey(msg.Payload) {
			logrus.WithField("MessagePayload", msg.Payload).Warn("Shadow leaderboard expiration worker received a non shadow key payload")
		}
		return nil
	}

	key := trimShadowSuffix(msg.Payload)
	id, err := identifier.FromKey(key)
	if err != nil {
		return err
	}

	queueURL := w.TenantClient.GetTenant(id.Domain).Config.ArchivalQueueConfig.URL

	arMsg := archival.Message{
		Key: key,
	}
	arMsgJSON, err := archival.ToJSON(arMsg)
	if err != nil {
		return err
	}

	return w.SQSClient.SendMessageWithDelay(ctx, queueURL, string(arMsgJSON), sqs.RandomSQSDealy())
}

func isShadowKey(key string) bool {
	return strings.HasSuffix(key, shadowSuffix)
}

func trimShadowSuffix(shadowKey string) string {
	return strings.TrimSuffix(shadowKey, shadowSuffix)
}

func isExpectedExpiringKey(key string) bool {
	return strings.HasSuffix(key, archivalLockSuffix)
}
