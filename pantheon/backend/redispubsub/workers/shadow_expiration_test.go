package workers

import (
	"errors"
	"testing"

	"code.justin.tv/commerce/pantheon/backend/leaderboard/archival"
	"code.justin.tv/commerce/pantheon/clients/tenant"
	"code.justin.tv/commerce/pantheon/config"
	sqs_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/clients/sqs"
	tenant_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/clients/tenant"
	"github.com/go-redis/redis"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

const (
	testQueueURL = "testURL"
)

func testConifg() config.TenantConfig {
	return config.TenantConfig{
		ArchivalQueueConfig: &config.QueueConfig{
			Name:       "testName",
			URL:        testQueueURL,
			NumWorkers: 1,
		},
	}
}

func redisMsg(payload string) *redis.Message {
	return &redis.Message{
		Payload: payload,
	}
}

func TestShadowLeaderboardExpirationWorker_Handle(t *testing.T) {
	Convey("Given a ShadowLeaderboardExpirationWorker", t, func() {
		sqsClient := new(sqs_mock.Client)
		mockTenantClient := new(tenant_mock.Landlord)
		mockTenantClient.On("GetTenant", mock.Anything).Return(tenant.Tenant{
			Config: testConifg(),
		})
		worker := ShadowLeaderboardExpirationWorker{
			TenantClient: mockTenantClient,
			SQSClient:    sqsClient,
		}

		Convey("eats message when payload doesn't have shadow suffix", func() {
			err := worker.Handle(redisMsg("where's my suffix?"))
			So(err, ShouldBeNil)
			So(sqsClient.AssertNotCalled(t, "SendMessage", mock.Anything, mock.Anything, mock.Anything), ShouldBeTrue)
		})

		Convey("fails when shadow suffix payload doesn't have a proper identifier key", func() {
			err := worker.Handle(redisMsg("not/a/valid/identifier-shadow"))
			So(err, ShouldNotBeNil)
		})

		Convey("fails when SQS client fails", func() {
			sqsClient.On("SendMessageWithDelay", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(errors.New("error"))
			err := worker.Handle(redisMsg("domain/gk/ALLTIME/123-shadow"))
			So(err, ShouldNotBeNil)
		})

		Convey("succeeds when SQS client succeeds", func() {
			key := "domain/gk/ALLTIME/123"
			msg := archival.Message{
				Key: key,
			}
			json, _ := archival.ToJSON(msg)
			sqsClient.On("SendMessageWithDelay", mock.Anything, testQueueURL, string(json), mock.Anything).Return(nil)

			err := worker.Handle(redisMsg(key + "-shadow"))
			So(err, ShouldBeNil)

		})

	})
}
