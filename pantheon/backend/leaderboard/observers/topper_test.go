package observers

import (
	"testing"

	"code.justin.tv/commerce/pantheon/backend/leaderboard/domain"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/identifier"
	domain_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/backend/leaderboard/domain"
	. "github.com/smartystreets/goconvey/convey"
)

func TestTopper_Top(t *testing.T) {
	Convey("Given a topper", t, func() {
		c := new(domain_mock.ConfigManager)

		t := topper{
			Config: c,
		}

		Convey("Given an unknown domain", func() {
			id := identifier.Identifier{
				Domain:      "walrus",
				GroupingKey: "fishing-leaders",
			}

			c.On("Get", identifier.Domain("walrus")).Return(domain.UnknownDomainConfig())

			Convey("When we ask for what the top lookup should be", func() {
				top := t.Top(id)

				Convey("It should return 10", func() {
					So(top, ShouldEqual, DefaultTop)
				})
			})
		})

		Convey("Given an OWL 2018 Team leaderboard entry", func() {
			id := identifier.Identifier{
				Domain:      domain.OverwatchLeague2018Domain,
				GroupingKey: domain.Owl2018TeamGroupingKey,
			}

			c.On("Get", domain.OverwatchLeague2018Domain).Return(domain.Config{
				PubsubTopNByGroupingKey: map[identifier.GroupingKey]int{
					domain.Owl2018TeamGroupingKey: domain.Owl2018TeamTopN,
				},
			})

			Convey("When we ask for what the top lookup should be", func() {
				top := t.Top(id)

				Convey("It should return 12", func() {
					So(top, ShouldEqual, domain.Owl2018TeamTopN)
				})
			})
		})

		Convey("Given an OWL 2019 Team leaderboard entry", func() {
			id := identifier.Identifier{
				Domain:      domain.Owl2019Domain,
				GroupingKey: domain.Owl2019TeamGroupingKey,
			}

			c.On("Get", domain.Owl2019Domain).Return(domain.Config{
				PubsubTopNByGroupingKey: map[identifier.GroupingKey]int{
					domain.Owl2019TeamGroupingKey: domain.Owl2019TeamTopN,
				},
			})

			Convey("When we ask for what the top lookup should be", func() {
				top := t.Top(id)

				Convey("It should return 20", func() {
					So(top, ShouldEqual, domain.Owl2019TeamTopN)
				})
			})
		})

		Convey("Given an HGC Team leaderboard entry", func() {
			id := identifier.Identifier{
				Domain:      domain.HGC2018Domain,
				GroupingKey: domain.Hgc2018TeamGroupingKey,
			}

			c.On("Get", domain.HGC2018Domain).Return(domain.Config{
				PubsubTopNByGroupingKey: map[identifier.GroupingKey]int{
					domain.Hgc2018TeamGroupingKey: domain.Hgc2018TeamTopN,
				},
			})

			Convey("When we ask for what the top lookup should be", func() {
				top := t.Top(id)

				Convey("It should return 32", func() {
					So(top, ShouldEqual, domain.Hgc2018TeamTopN)
				})
			})
		})

		Convey("Given an leaderboard with no overrides", func() {
			id := identifier.Identifier{
				Domain:      "walrus",
				GroupingKey: "fishing-leaders",
			}

			c.On("Get", identifier.Domain("walrus")).Return(domain.DefaultConfig(identifier.Domain("walrus")))

			Convey("When we ask for what the top lookup should be", func() {
				top := t.Top(id)

				Convey("It should return 10", func() {
					So(top, ShouldEqual, DefaultTop)
				})
			})
		})
	})
}
