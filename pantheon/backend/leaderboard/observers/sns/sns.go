package sns

import (
	"context"

	"code.justin.tv/commerce/pantheon/backend/leaderboard/observers"
	"code.justin.tv/commerce/pantheon/clients/sns"
	"code.justin.tv/commerce/pantheon/config"
	log "github.com/sirupsen/logrus"
)

type Observer struct {
	SNSClient sns.Client       `inject:""`
	Config    *config.Config   `inject:""`
	Topper    observers.Topper `inject:""`
}

func (i *Observer) Observe(update observers.LeaderboardUpdate) {
	ctx := context.Background()

	top := i.Topper.Top(update.ID)

	data, shouldProcess, err := observers.ProcessLeaderboardUpdate(update, top)
	if err != nil || data == nil || !shouldProcess {
		return
	}

	topicARN := i.Config.EventSNSTopicARN

	err = i.SNSClient.Publish(ctx, topicARN, string(data))
	if err != nil {
		log.WithError(err).WithField("topicARN", topicARN).Error("error publishing leaderboard event to SNS")
	}
}
