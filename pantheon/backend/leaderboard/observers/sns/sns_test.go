package sns

import (
	"testing"
	"time"

	"code.justin.tv/commerce/pantheon/backend/leaderboard/entry"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/identifier"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/local"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/observers"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/score"
	"code.justin.tv/commerce/pantheon/config"
	observer_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/backend/leaderboard/observers"
	sns_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/clients/sns"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestSNSObserver(t *testing.T) {
	Convey("Given a SNS observer", t, func() {
		snsClient := new(sns_mock.Client)
		c := config.Config{
			EventSNSTopicARN: "fooarn",
		}
		topper := new(observer_mock.Topper)

		observer := Observer{
			SNSClient: snsClient,
			Config:    &c,
			Topper:    topper,
		}

		snsClient.On("Publish", mock.Anything, mock.Anything, mock.Anything).Return(nil)
		topper.On("Top", mock.Anything).Return(observers.DefaultTop)

		Convey("When Observe is called", func() {
			observer.Observe(observers.LeaderboardUpdate{
				Leaderboard: local.NewLeaderboard(),
				ID:          identifier.Identifier{},
			})

			Convey("SNS is called", func() {
				So(snsClient.AssertExpectations(t), ShouldBeTrue)
				Convey("With the to the correct topic", func() {
					methodArgs := snsClient.Calls[0].Arguments
					topicsArg, ok := methodArgs.Get(1).(string)
					So(ok, ShouldBeTrue)
					So(topicsArg, ShouldEqual, "fooarn")
				})
			})
		})
	})
}

func TestSNSObserver_DontPublishZeroScore(t *testing.T) {
	Convey("Given a SNS observer", t, func() {
		snsClient := new(sns_mock.Client)
		c := config.Config{
			EventSNSTopicARN: "fooarn",
		}
		topper := new(observer_mock.Topper)

		observer := Observer{
			SNSClient: snsClient,
			Config:    &c,
			Topper:    topper,
		}
		updatedEntryKey := entry.Key("updated-entry-key")

		localLB := local.NewLeaderboard()
		err := localLB.SetScore(updatedEntryKey, score.EntryScore{
			ModeratedFlag: true,
			BaseScore:     0,
			EventTime:     score.EventTime(time.Now().UnixNano()),
		})
		So(err, ShouldBeNil)

		topper.On("Top", mock.Anything).Return(observers.DefaultTop)

		Convey("When Observe is called", func() {
			observer.Observe(observers.LeaderboardUpdate{
				Leaderboard:    local.NewLeaderboard(),
				ID:             identifier.Identifier{},
				UpdateEntryKey: &updatedEntryKey,
			})

			Convey("SNS is not called", func() {
				So(snsClient.Calls, ShouldHaveLength, 0)
			})
		})
	})
}
