package observers

import (
	"time"

	"code.justin.tv/commerce/pantheon/backend/leaderboard"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/entry"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/event"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/identifier"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
)

type LeaderboardUpdate struct {
	//The id for the updated leaderboard
	ID identifier.Identifier
	//The leaderboard that was updated
	Leaderboard leaderboard.Leaderboard
	//The entry key that was updated if there was one
	UpdateEntryKey *entry.Key
	//The update event that occurred
	Event *event.Event
}

type LeaderboardUpdateNotificationIdentifier struct {
	Domain              string    `json:"domain"`
	GroupingKey         string    `json:"grouping_key"`
	TimeAggregationUnit string    `json:"time_aggregation_unit"`
	TimeBucket          time.Time `json:"time_bucket"`
}

type LeaderboardUpdateNotification struct {
	Identifier   LeaderboardUpdateNotificationIdentifier `json:"identifier"`
	Top          []*pantheonrpc.LeaderboardEntry         `json:"top"`
	EntryContext *pantheonrpc.EntryContext               `json:"entry_context"`
	Event        event.Event                             `json:"event"`
}

type Observer interface {
	// The observe method is called when an Observable leaderboard is updated
	Observe(update LeaderboardUpdate)
}

type Observable interface {
	AddObservers(o ...Observer)
}
