package observers

import (
	"context"
	"encoding/json"
	"time"

	"code.justin.tv/commerce/pantheon/backend/api"
	"code.justin.tv/commerce/pantheon/backend/leaderboard"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/event"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	log "github.com/sirupsen/logrus"
)

func ProcessLeaderboardUpdate(update LeaderboardUpdate, topNumber int) (payload []byte, shouldProcess bool, err error) {
	lb := update.Leaderboard

	topRankedEntries, err := lb.GetTop(context.Background(), topNumber, leaderboard.RetrievalSettings{ConsistentRead: true})
	if err != nil {
		log.WithError(err).WithFields(log.Fields{
			"leaderboard": lb,
			"topN":        api.TopDefault,
		}).Error("error getting top from leaderboard")
		return nil, false, err
	}

	var entryContext *pantheonrpc.EntryContext
	if update.UpdateEntryKey != nil {
		entry, entryContextEntries, err := lb.GetEntryContext(context.Background(), *update.UpdateEntryKey, api.SpreadOne, leaderboard.RetrievalSettings{ConsistentRead: true})

		if err != nil {
			log.WithError(err).WithFields(log.Fields{
				"leaderboard":    lb,
				"updateEntryKey": *update.UpdateEntryKey,
			}).Error("error getting entry context from leaderboard")
			return nil, false, err
		}

		updatedEntry := api.ConvertModelToAPIEntry(entry)
		entries := api.ConvertModelToAPIEntries(entryContextEntries)

		entryContext = &pantheonrpc.EntryContext{
			Entry:   updatedEntry,
			Context: entries,
		}
	}

	top := api.ConvertModelToAPIEntries(topRankedEntries)

	var event event.Event
	if update.Event != nil {
		event = *update.Event
	}

	if entryContext != nil && entryContext.Entry != nil && entryContext.Entry.Score == 0 {
		return nil, false, nil
	}

	data, err := json.Marshal(LeaderboardUpdateNotification{
		Top:          top,
		EntryContext: entryContext,
		Identifier: LeaderboardUpdateNotificationIdentifier{
			Domain:              string(update.ID.Domain),
			GroupingKey:         string(update.ID.GroupingKey),
			TimeAggregationUnit: update.ID.TimeAggregationUnit.String(),
			TimeBucket:          time.Time(update.ID.TimeBucket),
		},
		Event: event,
	})
	if err != nil {
		log.WithError(err).WithField("top", top).Error("error marshalling leaderboard into json")
		return nil, false, err
	}

	return data, true, nil
}
