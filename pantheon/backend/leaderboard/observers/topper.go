package observers

import (
	"code.justin.tv/commerce/pantheon/backend/leaderboard/domain"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/identifier"
)

type Topper interface {
	Top(id identifier.Identifier) int
}

type topper struct {
	Config domain.ConfigManager `inject:""`
}

func NewTopper() Topper {
	return &topper{}
}

const (
	DefaultTop = 10
)

func (t *topper) Top(id identifier.Identifier) int {
	domainConfig := t.Config.Get(id.Domain)
	if !domainConfig.IsKnownDomain() {
		return DefaultTop
	}

	topN, present := domainConfig.PubsubTopNByGroupingKey[id.GroupingKey]
	if present {
		return topN
	}

	return DefaultTop
}
