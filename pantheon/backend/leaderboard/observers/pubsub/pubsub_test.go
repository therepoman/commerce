package pubsub

import (
	"encoding/json"
	"errors"
	"fmt"
	"testing"

	"code.justin.tv/commerce/pantheon/backend/leaderboard/domain"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/entry"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/event"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/identifier"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/local"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/observers"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/score"
	"code.justin.tv/commerce/pantheon/clients/users"
	pubclient_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/chat/pubsub-go-pubclient/client"
	observer_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/backend/leaderboard/observers"
	users_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/clients/users"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestPubsubObserver(t *testing.T) {
	Convey("Given a pubsub observer", t, func() {
		pubClient := new(pubclient_mock.PubClient)
		topper := new(observer_mock.Topper)
		usersClient := new(users_mock.Client)

		observer := Observer{
			PubsubClient: pubClient,
			Topper:       topper,
			UserService:  usersClient,
		}

		localLeaderboard := local.NewLeaderboard()
		for i := 0; i < 20; i++ {
			err := localLeaderboard.SetScore(entry.Key("team"+fmt.Sprint(i)), score.EntryScore{
				BaseScore:     score.BaseScore(i + 1),
				EventTime:     score.EventTime(i),
				ModeratedFlag: false,
			})
			So(err, ShouldBeNil)
		}

		pubClient.On("Publish", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)

		Convey("When Observe is called", func() {
			topper.On("Top", mock.Anything).Return(observers.DefaultTop)

			Convey("When leaderboard does not have an associated event", func() {
				observer.Observe(observers.LeaderboardUpdate{
					Leaderboard: localLeaderboard,
					ID:          identifier.Identifier{},
				})
			})

			Convey("When leaderboard has an event in subdomain that calls user service", func() {
				testUserID := "12345"
				testDisplayName := "some-display-name"
				testLogin := "some-login"

				Convey("When User Service returns user info, pubsub payload should contain metadata", func() {
					usersClient.On("GetUserByID", mock.Anything, testUserID).Return(&users.UserInfo{
						ID:          testUserID,
						DisplayName: &testDisplayName,
						Login:       &testLogin,
					}, nil)

					observer.Observe(observers.LeaderboardUpdate{
						Leaderboard: localLeaderboard,
						ID: identifier.Identifier{
							Domain: domain.BitsUsageByChannelDomain,
						},
						Event: &event.Event{
							EntryKey: testUserID,
						},
					})

					methodArgs := pubClient.Calls[0].Arguments
					leaderboardPayload := methodArgs.Get(2).(string)
					So(leaderboardPayload, ShouldContainSubstring, "metadata")
					So(leaderboardPayload, ShouldContainSubstring, "display_name")
					So(leaderboardPayload, ShouldContainSubstring, "login")
				})

				Convey("When User Service errors, pubsub payload should not contain metadata", func() {
					usersClient.On("GetUserByID", mock.Anything, testUserID).Return(nil, errors.New("user service error"))

					observer.Observe(observers.LeaderboardUpdate{
						Leaderboard: localLeaderboard,
						ID: identifier.Identifier{
							Domain: domain.BitsUsageByChannelDomain,
						},
						Event: &event.Event{
							EntryKey: testUserID,
						},
					})

					methodArgs := pubClient.Calls[0].Arguments
					leaderboardPayload := methodArgs.Get(2).(string)
					So(leaderboardPayload, ShouldNotContainSubstring, "metadata")
					So(leaderboardPayload, ShouldNotContainSubstring, "display_name")
					So(leaderboardPayload, ShouldNotContainSubstring, "login")
				})
				So(usersClient.AssertNumberOfCalls(t, "GetUserByID", 1), ShouldBeTrue)
			})

			// PubSub was called
			So(pubClient.AssertExpectations(t), ShouldBeTrue)
			methodArgs := pubClient.Calls[0].Arguments
			topicsArg, ok := methodArgs.Get(1).([]string)
			So(ok, ShouldBeTrue)
			So(len(topicsArg), ShouldEqual, 1)
			So(topicsArg[0], ShouldStartWith, "leaderboard-events-v1")

			leaderboardPayload := methodArgs.Get(2).(string)
			leaderboard := observers.LeaderboardUpdateNotification{}
			err := json.Unmarshal([]byte(leaderboardPayload), &leaderboard)
			So(err, ShouldBeNil)
			So(len(leaderboard.Top), ShouldEqual, observers.DefaultTop)
		})
	})
}
