package pubsub

import (
	"context"
	"fmt"

	pubclient "code.justin.tv/chat/pubsub-go-pubclient/client"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/domain"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/observers"
	"code.justin.tv/commerce/pantheon/clients/users"
	log "github.com/sirupsen/logrus"
)

const (
	channelEventsPubsubTopic = "leaderboard-events-v1"
	anonymous                = "anonymous"
)

type Observer struct {
	PubsubClient pubclient.PubClient `inject:""`
	Topper       observers.Topper    `inject:""`
	UserService  users.Client        `inject:""`
}

func (i *Observer) Observe(update observers.LeaderboardUpdate) {
	ctx := context.Background()

	i.applyTenantSpecificPreprocessing(ctx, &update)

	top := i.Topper.Top(update.ID)

	data, shouldProcess, err := observers.ProcessLeaderboardUpdate(update, top)
	if err != nil || data == nil || !shouldProcess {
		return
	}

	err = i.PubsubClient.Publish(ctx, []string{fmt.Sprintf("%s.%s", channelEventsPubsubTopic, update.ID.PubsubIdentifier())}, string(data), nil)
	if err != nil {
		log.WithError(err).Error("Failed to publish pubsub message")
	}
}

func (i *Observer) applyTenantSpecificPreprocessing(ctx context.Context, update *observers.LeaderboardUpdate) {
	// attempt to append user displayName and login to event metadata field
	// if request to user service fails we log an error but do not block the pubsub
	d := update.ID.Domain
	if d == domain.SubsGiftSentDomain || d == domain.BitsUsageByChannelDomain {
		if update == nil || update.Event == nil || update.Event.EntryKey == anonymous {
			return
		}

		userId := update.Event.EntryKey
		userInfo, err := i.UserService.GetUserByID(ctx, userId)
		if err != nil {
			log.WithField("userId", userId).WithError(err).Error("failed to call users service GetUserByID")
			return
		}
		if userInfo == nil {
			log.WithField("userId", userId).Warn("empty response from users service GetUserByID")
			return
		}
		if update.Event.Metadata == nil {
			update.Event.Metadata = make(map[string]string)
		}
		update.Event.Metadata["display_name"] = *userInfo.DisplayName
		update.Event.Metadata["login"] = *userInfo.Login
	}
}
