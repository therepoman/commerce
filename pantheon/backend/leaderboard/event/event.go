package event

import (
	"encoding/json"
	"fmt"

	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
)

func FromPublishEventRequest(req *pantheonrpc.PublishEventReq) Event {
	return Event{
		Domain:      req.Domain,
		ID:          req.EventId,
		TimeOfEvent: req.TimeOfEvent,
		GroupingKey: req.GroupingKey,
		EntryKey:    req.EntryKey,
		EventValue:  req.EventValue,
	}
}

type Event struct {
	Domain      string            `json:"domain"`
	ID          string            `json:"id"`
	TimeOfEvent uint64            `json:"time_of_event"`
	GroupingKey string            `json:"grouping_key"`
	EntryKey    string            `json:"entry_key"`
	EventValue  int64             `json:"event_value"`
	Metadata    map[string]string `json:"metadata,omitempty"`
}

func ToJSON(e Event) (string, error) {
	b, err := json.Marshal(&e)
	if err != nil {
		return "", err
	}
	return string(b), nil
}

func FromJSON(je string) (*Event, error) {
	var event Event
	err := json.Unmarshal([]byte(je), &event)
	if err != nil {
		return nil, err
	}
	return &event, nil
}

func (e Event) ToLeaderboardSetID() string {
	return fmt.Sprintf("%s/%s", e.Domain, e.GroupingKey)
}
