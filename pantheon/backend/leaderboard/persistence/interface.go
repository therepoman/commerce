package persistence

import (
	"context"

	"code.justin.tv/commerce/pantheon/backend/leaderboard"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/identifier"
)

type Persister interface {
	Export(ctx context.Context, id identifier.Identifier, lb leaderboard.Leaderboard, rs leaderboard.RetrievalSettings) error
	Import(ctx context.Context, id identifier.Identifier, lb leaderboard.Leaderboard) error
}
