package persistence

import (
	"context"

	"code.justin.tv/commerce/pantheon/backend/leaderboard"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/identifier"
)

type BytePersister struct {
	bytes []byte
}

func (a *BytePersister) Export(ctx context.Context, id identifier.Identifier, lb leaderboard.Leaderboard, rs leaderboard.RetrievalSettings) error {
	bs, err := lb.Serialize(rs)
	if err != nil {
		return err
	}
	a.bytes = bs
	return nil
}

func (a *BytePersister) Import(ctx context.Context, id identifier.Identifier, lb leaderboard.Leaderboard) error {
	if len(a.bytes) == 0 {
		return nil
	}
	return lb.Restore(a.bytes)
}
