package persistence

import (
	"bytes"
	"context"
	"fmt"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pantheon/backend/leaderboard"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/identifier"
	"code.justin.tv/commerce/pantheon/clients/s3"
	"code.justin.tv/commerce/pantheon/config"
	"code.justin.tv/commerce/pantheon/middleware"
	"github.com/uber-go/tally"
)

const (
	s3PersisterImportLatencyMetric = "s3-persister-import-latency"
	s3PersisterKeyFoundMetric      = "s3-persister-import-key-found"
	s3PersisterKeyNotFoundMetric   = "s3-persister-import-key-not-found"
)

type S3Persister struct {
	Config           *config.Config `inject:""`
	S3Client         s3.Client      `inject:""`
	RootMetricsScope tally.Scope    `inject:""`
}

func (p *S3Persister) getS3Bucket() string {
	return p.Config.LeaderboardS3Bucket
}

func GetS3Key(id identifier.Identifier, lb leaderboard.Leaderboard) string {
	return fmt.Sprintf("%s-%s", id.Key(), lb.Type())
}

func (p *S3Persister) Export(ctx context.Context, id identifier.Identifier, lb leaderboard.Leaderboard, rs leaderboard.RetrievalSettings) error {
	bs, err := lb.Serialize(rs)
	if err != nil {
		return err
	}
	return p.S3Client.PutFile(ctx, p.getS3Bucket(), GetS3Key(id, lb), bytes.NewReader(bs))
}

func (p *S3Persister) Import(ctx context.Context, id identifier.Identifier, lb leaderboard.Leaderboard) error {
	go logRequest(ctx, id)

	metricStopwatch := p.RootMetricsScope.Tagged(getMetricTags(id.Domain)).Timer(s3PersisterImportLatencyMetric).Start()
	defer metricStopwatch.Stop()

	resp, err := p.S3Client.GetFile(ctx, p.getS3Bucket(), GetS3Key(id, lb))
	if err != nil {
		return err
	}

	if !resp.Exists {
		p.RootMetricsScope.Counter(s3PersisterKeyNotFoundMetric).Inc(1)
		return nil
	}

	p.RootMetricsScope.Counter(s3PersisterKeyFoundMetric).Inc(1)
	return lb.Restore(resp.File)
}

func logRequest(ctx context.Context, id identifier.Identifier) {
	var clientID, ipAddress string

	if id, ok := middleware.ClientID(ctx); ok {
		clientID = id
	}

	if address, ok := middleware.IPAddress(ctx); ok {
		ipAddress = address
	}

	logrus.WithFields(logrus.Fields{
		"domain":                id.Domain,
		"grouping_key":          id.GroupingKey,
		"time_bucket":           id.TimeBucket,
		"time_aggregation_unit": id.TimeAggregationUnit,
		"client_id":             clientID,
		"ip_address":            ipAddress,
	}).Info("Importing leaderboard from S3")
}

func getMetricTags(d identifier.Domain) map[string]string {
	metricTags := map[string]string{
		"domain": string(d),
	}
	return metricTags
}
