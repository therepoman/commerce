package bucket

import (
	"strconv"
	"time"

	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	timeUtils "code.justin.tv/commerce/pantheon/utils/time"
)

const (
	startOfWeek = time.Monday
)

type TimeBucket time.Time

func (b TimeBucket) String() string {
	return strconv.FormatInt(time.Time(b).UnixNano(), 10)
}

func (b TimeBucket) Equal(other TimeBucket) bool {
	return time.Time(b).Equal(time.Time(other))
}

func (b TimeBucket) ToStartTime(timeUnit pantheonrpc.TimeUnit) *time.Time {
	if timeUnit == pantheonrpc.TimeUnit_ALLTIME {
		return nil
	}

	startTime := time.Time(b)

	return &startTime
}

func (b TimeBucket) ToExpirationTime(timeUnit pantheonrpc.TimeUnit) *time.Time {
	if timeUnit == pantheonrpc.TimeUnit_ALLTIME {
		return nil
	}

	bucketTime := time.Time(b)
	year := bucketTime.Year()
	month := bucketTime.Month()
	day := bucketTime.Day()
	hour := bucketTime.Hour()
	minute := bucketTime.Minute()
	second := bucketTime.Second()
	nanosecond := bucketTime.Nanosecond()
	loc := bucketTime.Location()
	switch timeUnit {
	case pantheonrpc.TimeUnit_YEAR:
		year++
	case pantheonrpc.TimeUnit_MONTH:
		month++
	case pantheonrpc.TimeUnit_WEEK:
		day += 7
	case pantheonrpc.TimeUnit_DAY:
		day++
	}
	expTime := time.Date(year, month, day, hour, minute, second, nanosecond, loc)
	return &expTime
}

func TimestampToTimeBucket(timestamp time.Time, timeUnit pantheonrpc.TimeUnit) TimeBucket {
	timestamp = timestamp.In(timeUtils.GetTimeZone())

	var timeBucket TimeBucket
	switch timeUnit {
	case pantheonrpc.TimeUnit_ALLTIME:
		timeBucket = TimeBucket(time.Unix(0, 0))
	case pantheonrpc.TimeUnit_YEAR:
		timeBucket = TimeBucket(getYear(timestamp))
	case pantheonrpc.TimeUnit_MONTH:
		timeBucket = TimeBucket(getMonth(timestamp))
	case pantheonrpc.TimeUnit_WEEK:
		timeBucket = TimeBucket(getWeek(timestamp))
	case pantheonrpc.TimeUnit_DAY:
		timeBucket = TimeBucket(getDay(timestamp))
	}
	return timeBucket
}

func getYear(timestamp time.Time) time.Time {
	return time.Date(timestamp.Year(), time.January, 1, 0, 0, 0, 0, timeUtils.GetTimeZone())
}

func getMonth(timestamp time.Time) time.Time {
	return time.Date(timestamp.Year(), timestamp.Month(), 1, 0, 0, 0, 0, timeUtils.GetTimeZone())
}

func getDay(timestamp time.Time) time.Time {
	return time.Date(timestamp.Year(), timestamp.Month(), timestamp.Day(), 0, 0, 0, 0, timeUtils.GetTimeZone())
}

func getWeek(timestamp time.Time) time.Time {
	day := getDay(timestamp)
	for day.Weekday() != startOfWeek {
		day = day.AddDate(0, 0, -1)
	}
	return day
}
