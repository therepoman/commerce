package bucket

import (
	"testing"
	"time"

	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	"code.justin.tv/commerce/pantheon/utils/random"
	timeUtils "code.justin.tv/commerce/pantheon/utils/time"
	. "github.com/smartystreets/goconvey/convey"
)

func TestTimestampToTimeBucket_AllTime(t *testing.T) {
	Convey("Given the 'alltime' timeUnit", t, func() {
		timeUnit := pantheonrpc.TimeUnit_ALLTIME

		Convey("A random timestamp should return the min epoch time", func() {
			timeStamp := random.Time(24 * time.Hour * 365 * 10)
			resp := TimestampToTimeBucket(timeStamp, timeUnit)
			So(resp.Equal(TimeBucket(time.Unix(0, 0))), ShouldBeTrue)
		})
	})
}

func TestTimestampToTimeBucket_Year(t *testing.T) {
	bucket2015 := TimeBucket(time.Date(2015, time.January, 1, 0, 0, 0, 0, timeUtils.GetTimeZone()))
	bucket2016 := TimeBucket(time.Date(2016, time.January, 1, 0, 0, 0, 0, timeUtils.GetTimeZone()))
	Convey("Given the 'year' timeUnit", t, func() {
		timeUnit := pantheonrpc.TimeUnit_YEAR

		Convey("Beginning of the first day should reduce to year start", func() {
			resp := TimestampToTimeBucket(time.Date(2015, time.January, 1, 0, 0, 0, 0, timeUtils.GetTimeZone()), timeUnit)
			So(resp.Equal(bucket2015), ShouldBeTrue)
		})

		Convey("Mid first day should reduce to year start", func() {
			resp := TimestampToTimeBucket(time.Date(2015, time.January, 1, 12, 30, 12, 23, timeUtils.GetTimeZone()), timeUnit)
			So(resp.Equal(bucket2015), ShouldBeTrue)
		})

		Convey("Mid first week should reduce to year start", func() {
			resp := TimestampToTimeBucket(time.Date(2015, time.January, 4, 17, 19, 59, 12, timeUtils.GetTimeZone()), timeUnit)
			So(resp.Equal(bucket2015), ShouldBeTrue)
		})

		Convey("Mid first month should reduce to year start", func() {
			resp := TimestampToTimeBucket(time.Date(2015, time.January, 18, 23, 47, 22, 33, timeUtils.GetTimeZone()), timeUnit)
			So(resp.Equal(bucket2015), ShouldBeTrue)
		})

		Convey("Second month should reduce to year start", func() {
			resp := TimestampToTimeBucket(time.Date(2015, time.February, 22, 1, 2, 3, 4, timeUtils.GetTimeZone()), timeUnit)
			So(resp.Equal(bucket2015), ShouldBeTrue)
		})

		Convey("Mid-year should reduce to year start", func() {
			resp := TimestampToTimeBucket(time.Date(2015, time.July, 4, 5, 6, 7, 8, timeUtils.GetTimeZone()), timeUnit)
			So(resp.Equal(bucket2015), ShouldBeTrue)
		})

		Convey("Final month should reduce to year start", func() {
			resp := TimestampToTimeBucket(time.Date(2015, time.December, 22, 10, 11, 12, 13, timeUtils.GetTimeZone()), timeUnit)
			So(resp.Equal(bucket2015), ShouldBeTrue)
		})

		Convey("End of year should reduce to year start", func() {
			resp := TimestampToTimeBucket(time.Date(2015, time.December, 31, 23, 59, 59, 999999999, timeUtils.GetTimeZone()), timeUnit)
			So(resp.Equal(bucket2015), ShouldBeTrue)
		})

		Convey("One nanosecond past the end of the year rolls over to the next year", func() {
			resp := TimestampToTimeBucket(time.Date(2015, time.December, 31, 23, 59, 59, 999999999+1, timeUtils.GetTimeZone()), timeUnit)
			So(resp.Equal(bucket2016), ShouldBeTrue)
		})
	})
}

func TestTimestampToTimeBucket_Month_MidYear(t *testing.T) {
	bucketApril2017 := TimeBucket(time.Date(2017, time.April, 1, 0, 0, 0, 0, timeUtils.GetTimeZone()))
	bucketMay2017 := TimeBucket(time.Date(2017, time.May, 1, 0, 0, 0, 0, timeUtils.GetTimeZone()))
	Convey("Given the 'month' timeUnit", t, func() {
		timeUnit := pantheonrpc.TimeUnit_MONTH

		Convey("Beginning of the first day should reduce to month start", func() {
			resp := TimestampToTimeBucket(time.Date(2017, time.April, 1, 0, 0, 0, 0, timeUtils.GetTimeZone()), timeUnit)
			So(resp.Equal(bucketApril2017), ShouldBeTrue)
		})

		Convey("Mid first day should reduce to month start", func() {
			resp := TimestampToTimeBucket(time.Date(2017, time.April, 1, 12, 30, 12, 23, timeUtils.GetTimeZone()), timeUnit)
			So(resp.Equal(bucketApril2017), ShouldBeTrue)
		})

		Convey("Mid first week should reduce to month start", func() {
			resp := TimestampToTimeBucket(time.Date(2017, time.April, 4, 17, 19, 59, 12, timeUtils.GetTimeZone()), timeUnit)
			So(resp.Equal(bucketApril2017), ShouldBeTrue)
		})

		Convey("Mid month should reduce to month start", func() {
			resp := TimestampToTimeBucket(time.Date(2017, time.April, 20, 4, 20, 4, 20, timeUtils.GetTimeZone()), timeUnit)
			So(resp.Equal(bucketApril2017), ShouldBeTrue)
		})

		Convey("End of month should reduce to month start", func() {
			resp := TimestampToTimeBucket(time.Date(2017, time.April, 30, 23, 59, 59, 999999999, timeUtils.GetTimeZone()), timeUnit)
			So(resp.Equal(bucketApril2017), ShouldBeTrue)
		})

		Convey("One nanosecond past the end of the month rolls over to the next month", func() {
			resp := TimestampToTimeBucket(time.Date(2017, time.April, 30, 23, 59, 59, 999999999+1, timeUtils.GetTimeZone()), timeUnit)
			So(resp.Equal(bucketMay2017), ShouldBeTrue)
		})
	})
}

func TestTimestampToTimeBucket_Month_EndOfYear(t *testing.T) {
	bucketDecember2017 := TimeBucket(time.Date(2017, time.December, 1, 0, 0, 0, 0, timeUtils.GetTimeZone()))
	bucketJanuary2018 := TimeBucket(time.Date(2018, time.January, 1, 0, 0, 0, 0, timeUtils.GetTimeZone()))
	Convey("Given the 'month' timeUnit", t, func() {
		timeUnit := pantheonrpc.TimeUnit_MONTH

		Convey("Beginning of the first day should reduce to month start", func() {
			resp := TimestampToTimeBucket(time.Date(2017, time.December, 1, 0, 0, 0, 0, timeUtils.GetTimeZone()), timeUnit)
			So(resp.Equal(bucketDecember2017), ShouldBeTrue)
		})

		Convey("Mid first day should reduce to month start", func() {
			resp := TimestampToTimeBucket(time.Date(2017, time.December, 1, 12, 30, 12, 23, timeUtils.GetTimeZone()), timeUnit)
			So(resp.Equal(bucketDecember2017), ShouldBeTrue)
		})

		Convey("Mid first week should reduce to month start", func() {
			resp := TimestampToTimeBucket(time.Date(2017, time.December, 4, 17, 19, 59, 12, timeUtils.GetTimeZone()), timeUnit)
			So(resp.Equal(bucketDecember2017), ShouldBeTrue)
		})

		Convey("Mid month should reduce to month start", func() {
			resp := TimestampToTimeBucket(time.Date(2017, time.December, 20, 4, 20, 4, 20, timeUtils.GetTimeZone()), timeUnit)
			So(resp.Equal(bucketDecember2017), ShouldBeTrue)
		})

		Convey("End of month should reduce to month start", func() {
			resp := TimestampToTimeBucket(time.Date(2017, time.December, 31, 23, 59, 59, 999999999, timeUtils.GetTimeZone()), timeUnit)
			So(resp.Equal(bucketDecember2017), ShouldBeTrue)
		})

		Convey("One nanosecond past the end of the month rolls over to the next month (which falls into the next year)", func() {
			resp := TimestampToTimeBucket(time.Date(2017, time.December, 31, 23, 59, 59, 999999999+1, timeUtils.GetTimeZone()), timeUnit)
			So(resp.Equal(bucketJanuary2018), ShouldBeTrue)
		})
	})
}

func TestTimestampToTimeBucket_Day_MidMonth(t *testing.T) {
	bucketApril202017 := TimeBucket(time.Date(2017, time.April, 20, 0, 0, 0, 0, timeUtils.GetTimeZone()))
	bucketApril212017 := TimeBucket(time.Date(2017, time.April, 21, 0, 0, 0, 0, timeUtils.GetTimeZone()))
	Convey("Given the 'day' timeUnit", t, func() {
		timeUnit := pantheonrpc.TimeUnit_DAY

		Convey("Beginning of the day should reduce to day start", func() {
			resp := TimestampToTimeBucket(time.Date(2017, time.April, 20, 0, 0, 0, 0, timeUtils.GetTimeZone()), timeUnit)
			So(resp.Equal(bucketApril202017), ShouldBeTrue)
		})

		Convey("Mid first day should reduce to day start", func() {
			resp := TimestampToTimeBucket(time.Date(2017, time.April, 20, 12, 30, 12, 23, timeUtils.GetTimeZone()), timeUnit)
			So(resp.Equal(bucketApril202017), ShouldBeTrue)
		})

		Convey("End of the day should reduce to day start", func() {
			resp := TimestampToTimeBucket(time.Date(2017, time.April, 20, 23, 59, 59, 999999999, timeUtils.GetTimeZone()), timeUnit)
			So(resp.Equal(bucketApril202017), ShouldBeTrue)
		})

		Convey("One nanosecond past the end of the day rolls over to the next day", func() {
			resp := TimestampToTimeBucket(time.Date(2017, time.April, 20, 23, 59, 59, 999999999+1, timeUtils.GetTimeZone()), timeUnit)
			So(resp.Equal(bucketApril212017), ShouldBeTrue)
		})
	})
}

func TestTimestampToTimeBucket_Day_EndOfMonth(t *testing.T) {
	bucketApril302017 := TimeBucket(time.Date(2017, time.April, 30, 0, 0, 0, 0, timeUtils.GetTimeZone()))
	bucketMay12017 := TimeBucket(time.Date(2017, time.May, 1, 0, 0, 0, 0, timeUtils.GetTimeZone()))
	Convey("Given the 'day' timeUnit", t, func() {
		timeUnit := pantheonrpc.TimeUnit_DAY

		Convey("Beginning of the day should reduce to day start", func() {
			resp := TimestampToTimeBucket(time.Date(2017, time.April, 30, 0, 0, 0, 0, timeUtils.GetTimeZone()), timeUnit)
			So(resp.Equal(bucketApril302017), ShouldBeTrue)
		})

		Convey("Mid first day should reduce to day start", func() {
			resp := TimestampToTimeBucket(time.Date(2017, time.April, 30, 12, 30, 12, 23, timeUtils.GetTimeZone()), timeUnit)
			So(resp.Equal(bucketApril302017), ShouldBeTrue)
		})

		Convey("End of the day should reduce to day start", func() {
			resp := TimestampToTimeBucket(time.Date(2017, time.April, 30, 23, 59, 59, 999999999, timeUtils.GetTimeZone()), timeUnit)
			So(resp.Equal(bucketApril302017), ShouldBeTrue)
		})

		Convey("One nanosecond past the end of the day rolls over to the next day (which falls into the next month)", func() {
			resp := TimestampToTimeBucket(time.Date(2017, time.April, 30, 23, 59, 59, 999999999+1, timeUtils.GetTimeZone()), timeUnit)
			So(resp.Equal(bucketMay12017), ShouldBeTrue)
		})
	})
}

func TestTimestampToTimeBucket_Day_EndOfYear(t *testing.T) {
	bucketDecember312017 := TimeBucket(time.Date(2017, time.December, 31, 0, 0, 0, 0, timeUtils.GetTimeZone()))
	bucketJanuary12018 := TimeBucket(time.Date(2018, time.January, 1, 0, 0, 0, 0, timeUtils.GetTimeZone()))
	Convey("Given the 'day' timeUnit", t, func() {
		timeUnit := pantheonrpc.TimeUnit_DAY

		Convey("Beginning of the day should reduce to day start", func() {
			resp := TimestampToTimeBucket(time.Date(2017, time.December, 31, 0, 0, 0, 0, timeUtils.GetTimeZone()), timeUnit)
			So(resp.Equal(bucketDecember312017), ShouldBeTrue)
		})

		Convey("Mid first day should reduce to day start", func() {
			resp := TimestampToTimeBucket(time.Date(2017, time.December, 31, 12, 30, 12, 23, timeUtils.GetTimeZone()), timeUnit)
			So(resp.Equal(bucketDecember312017), ShouldBeTrue)
		})

		Convey("End of the day should reduce to day start", func() {
			resp := TimestampToTimeBucket(time.Date(2017, time.December, 31, 23, 59, 59, 999999999, timeUtils.GetTimeZone()), timeUnit)
			So(resp.Equal(bucketDecember312017), ShouldBeTrue)
		})

		Convey("One nanosecond past the end of the day rolls over to the next day (which falls into the next month / year)", func() {
			resp := TimestampToTimeBucket(time.Date(2017, time.December, 31, 23, 59, 59, 999999999+1, timeUtils.GetTimeZone()), timeUnit)
			So(resp.Equal(bucketJanuary12018), ShouldBeTrue)
		})
	})
}

func TestTimestampToTimeBucket_Week(t *testing.T) {
	bucketWeekOfMay292017 := TimeBucket(time.Date(2017, time.May, 29, 0, 0, 0, 0, timeUtils.GetTimeZone()))
	bucketWeekOfJune52017 := TimeBucket(time.Date(2017, time.June, 5, 0, 0, 0, 0, timeUtils.GetTimeZone()))
	Convey("Given the 'week' timeUnit", t, func() {
		timeUnit := pantheonrpc.TimeUnit_WEEK

		Convey("Beginning of the week should reduce to week start", func() {
			resp := TimestampToTimeBucket(time.Date(2017, time.May, 29, 0, 0, 0, 0, timeUtils.GetTimeZone()), timeUnit)
			So(resp.Equal(bucketWeekOfMay292017), ShouldBeTrue)
		})

		Convey("Mid first day should reduce to week start", func() {
			resp := TimestampToTimeBucket(time.Date(2017, time.May, 29, 12, 30, 12, 23, timeUtils.GetTimeZone()), timeUnit)
			So(resp.Equal(bucketWeekOfMay292017), ShouldBeTrue)
		})

		Convey("Next day should reduce to week start", func() {
			resp := TimestampToTimeBucket(time.Date(2017, time.May, 30, 8, 1, 2, 3, timeUtils.GetTimeZone()), timeUnit)
			So(resp.Equal(bucketWeekOfMay292017), ShouldBeTrue)
		})

		Convey("Mid week should reduce to week start", func() {
			resp := TimestampToTimeBucket(time.Date(2017, time.June, 1, 0, 0, 0, 0, timeUtils.GetTimeZone()), timeUnit)
			So(resp.Equal(bucketWeekOfMay292017), ShouldBeTrue)
		})

		Convey("End of week should reduce to week start", func() {
			resp := TimestampToTimeBucket(time.Date(2017, time.June, 4, 23, 59, 59, 999999999, timeUtils.GetTimeZone()), timeUnit)
			So(resp.Equal(bucketWeekOfMay292017), ShouldBeTrue)
		})

		Convey("One nanosecond past the end of the week rolls over to the next week", func() {
			resp := TimestampToTimeBucket(time.Date(2017, time.June, 4, 23, 59, 59, 999999999+1, timeUtils.GetTimeZone()), timeUnit)
			So(resp.Equal(bucketWeekOfJune52017), ShouldBeTrue)
		})
	})
}

func TestTimeBucketToExpirationTime_AllTime(t *testing.T) {
	Convey("Given the 'alltime' timeUnit", t, func() {
		timeUnit := pantheonrpc.TimeUnit_ALLTIME

		Convey("TimeBucketToExpirationTime converts to a nil expiration time", func() {
			resp := TimeBucket(time.Unix(0, 0)).ToExpirationTime(timeUnit)
			So(resp, ShouldBeNil)
		})
	})
}

func TestTimeBucketToExpirationTime_Year(t *testing.T) {
	year2015 := time.Date(2015, time.January, 1, 0, 0, 0, 0, timeUtils.GetTimeZone())
	year2016 := time.Date(2016, time.January, 1, 0, 0, 0, 0, timeUtils.GetTimeZone())

	Convey("Given the 'year' timeUnit", t, func() {
		timeUnit := pantheonrpc.TimeUnit_YEAR

		Convey("TimeBucketToExpirationTime converts to the next year", func() {
			resp := TimeBucket(year2015).ToExpirationTime(timeUnit)
			So(resp.Equal(year2016), ShouldBeTrue)
		})
	})
}

func TestTimeBucketToExpirationTime_Month(t *testing.T) {
	december2017 := time.Date(2017, time.December, 1, 0, 0, 0, 0, timeUtils.GetTimeZone())
	january2018 := time.Date(2018, time.January, 1, 0, 0, 0, 0, timeUtils.GetTimeZone())

	Convey("Given the 'year' timeUnit", t, func() {
		timeUnit := pantheonrpc.TimeUnit_MONTH

		Convey("TimeBucketToExpirationTime converts to the next month", func() {
			resp := TimeBucket(december2017).ToExpirationTime(timeUnit)
			So(resp.Equal(january2018), ShouldBeTrue)
		})
	})
}

func TestTimeBucketToExpirationTime_Week(t *testing.T) {
	weekOfMay292017 := time.Date(2017, time.May, 29, 0, 0, 0, 0, timeUtils.GetTimeZone())
	weekOfJune52017 := time.Date(2017, time.June, 5, 0, 0, 0, 0, timeUtils.GetTimeZone())

	Convey("Given the 'week' timeUnit", t, func() {
		timeUnit := pantheonrpc.TimeUnit_WEEK

		Convey("TimeBucketToExpirationTime converts to the next week", func() {
			resp := TimeBucket(weekOfMay292017).ToExpirationTime(timeUnit)
			So(resp.Equal(weekOfJune52017), ShouldBeTrue)
		})
	})
}

func TestTimeBucketToExpirationTime_Day(t *testing.T) {
	december312017 := time.Date(2017, time.December, 31, 0, 0, 0, 0, timeUtils.GetTimeZone())
	january12018 := time.Date(2018, time.January, 1, 0, 0, 0, 0, timeUtils.GetTimeZone())

	Convey("Given the 'day' timeUnit", t, func() {
		timeUnit := pantheonrpc.TimeUnit_DAY

		Convey("TimeBucketToExpirationTime converts to the next day", func() {
			resp := TimeBucket(december312017).ToExpirationTime(timeUnit)
			So(resp.Equal(january12018), ShouldBeTrue)
		})
	})
}
