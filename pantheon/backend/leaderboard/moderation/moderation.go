package moderation

import (
	"encoding/json"
	"fmt"
	"time"

	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
)

func FromModerateEntryRequest(req *pantheonrpc.ModerateEntryReq) Moderation {
	var timeOfEvent time.Time
	if req.TimeOfEvent == 0 {
		timeOfEvent = time.Now()
	} else {
		timeOfEvent = time.Unix(0, int64(req.TimeOfEvent))
	}

	return Moderation{
		Domain:           req.Domain,
		GroupingKey:      req.GroupingKey,
		EntryKey:         req.EntryKey,
		ModerationAction: req.ModerationAction.String(),
		TimeOfModeration: timeOfEvent,
	}
}

type Moderation struct {
	Domain           string    `json:"domain"`
	GroupingKey      string    `json:"groupingKey"`
	EntryKey         string    `json:"entry_key"`
	ModerationAction string    `json:"moderation_action"`
	TimeOfModeration time.Time `json:"time_of_moderation"`
}

func (m Moderation) ToLeaderboardSetID() string {
	return fmt.Sprintf("%s/%s", m.Domain, m.GroupingKey)
}

func ToJSON(m Moderation) (string, error) {
	b, err := json.Marshal(&m)
	if err != nil {
		return "", err
	}
	return string(b), nil
}

func FromJSON(jm string) (*Moderation, error) {
	var moderation Moderation
	err := json.Unmarshal([]byte(jm), &moderation)
	if err != nil {
		return nil, err
	}
	return &moderation, nil
}
