package test

import (
	"context"
	"time"

	"code.justin.tv/commerce/pantheon/backend/leaderboard"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/bucket"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/entry"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/event"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/identifier"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/score"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	"code.justin.tv/commerce/pantheon/utils/random"
	. "github.com/smartystreets/goconvey/convey"
)

func RandomEntry() entry.Entry {
	return entry.Entry{
		Key:   entry.Key(random.String(32)),
		Score: RandomEntryScore(),
	}
}

func RandomEntryScore() score.EntryScore {
	return score.EntryScore{
		BaseScore: score.BaseScore(random.Int64(0, 1000000000)),
		EventTime: RandomEventTime(),
	}
}

func RandomIdentifier() identifier.Identifier {
	return identifier.Identifier{
		Domain:              identifier.Domain(random.String(16)),
		GroupingKey:         identifier.GroupingKey(random.String(16)),
		TimeAggregationUnit: pantheonrpc.TimeUnit_ALLTIME, //TODO: replace with random TimeUnit
		TimeBucket:          bucket.TimeBucket(random.Time(time.Hour * 48)),
	}
}

func RandomEntries(n int) []entry.Entry {
	entries := make([]entry.Entry, n)
	for i := 0; i < n; i++ {
		entries[i] = RandomEntry()
	}
	return entries
}

func DescendingScoreEntries(n int) []entry.Entry {
	entries := make([]entry.Entry, n)
	for i := 0; i < n; i++ {
		entries[i] = RandomEntryWithScore(n - i)
	}
	return entries
}

func RandomEntryWithScore(entryScore int) entry.Entry {
	e := RandomEntry()
	e.Score.BaseScore = score.BaseScore(entryScore)
	return e
}

func RandomEvent() event.Event {
	return event.Event{
		Domain:      random.String(16),
		ID:          random.String(16),
		GroupingKey: random.String(16),
		TimeOfEvent: uint64(random.Time(24 * time.Hour).UnixNano()),
		EntryKey:    random.String(16),
		EventValue:  random.Int64(0, 1000000000),
	}
}

func RandomEventTime() score.EventTime {
	return score.EventTime(time.Now().Add(random.Duration(-7*24*time.Hour, 7*24*time.Hour)).UnixNano())
}

func AddEntries(lb leaderboard.Leaderboard, entries ...entry.Entry) {
	for _, entry := range entries {
		_ = lb.UpdateScore(entry.Key, entry.Score, nil)
	}
}

func Equal(lb1 leaderboard.Leaderboard, lb2 leaderboard.Leaderboard) bool {
	lb1Count, err := lb1.Len(leaderboard.RetrievalSettings{})
	if err != nil {
		return false
	}

	lb2Count, err := lb2.Len(leaderboard.RetrievalSettings{})
	if err != nil {
		return false
	}

	if lb1Count != lb2Count {
		return false
	}

	all1, err := lb1.GetTop(context.Background(), lb1Count, leaderboard.RetrievalSettings{})
	if err != nil {
		return false
	}
	all2, err := lb2.GetTop(context.Background(), lb2Count, leaderboard.RetrievalSettings{})
	if err != nil {
		return false
	}
	for i := range all1 {
		if !entry.RankedEntryEquals(all1[i], all2[i]) {
			return false
		}
	}
	return true
}

func RetrievalTest(lb leaderboard.Leaderboard) {
	Convey("Given random input data that is added to the leaderboard", func() {
		numKeys := 100
		inputData := RandomEntries(numKeys)
		AddEntries(lb, inputData...)

		Convey("Validate that all entries are present and correct", func() {
			for i := 0; i < numKeys; i++ {
				entry := inputData[i]
				actualScore, err := lb.GetScore(entry.Key, leaderboard.RetrievalSettings{})
				So(err, ShouldBeNil)
				So(actualScore.DecodeScore().BaseScore, ShouldEqual, entry.Score.BaseScore)
			}
		})

	})
}

func SerializationRoundTripTest(lb leaderboard.Leaderboard) {
	Convey("Given random input data that is added to the leaderboard", func() {
		numKeys := 10
		inputData := RandomEntries(numKeys)
		AddEntries(lb, inputData...)

		Convey("The leaderboard can be serialized", func() {
			lbs, err := lb.Serialize(leaderboard.RetrievalSettings{})
			So(err, ShouldBeNil)
			So(len(lbs), ShouldBeGreaterThan, 0)

			top, err := lb.GetTop(context.Background(), numKeys+1, leaderboard.RetrievalSettings{})
			So(err, ShouldBeNil)
			So(len(top), ShouldEqual, numKeys)

			Convey("The leaderboard is empty when cleared", func() {
				err := lb.ClearScores()
				So(err, ShouldBeNil)
				top, err := lb.GetTop(context.Background(), 1, leaderboard.RetrievalSettings{})
				So(err, ShouldBeNil)
				So(len(top), ShouldEqual, 0)

				Convey("The leaderboard is full when restored and all original entries are present and correct", func() {
					err := lb.Restore(lbs)
					So(err, ShouldBeNil)

					top, err := lb.GetTop(context.Background(), numKeys+1, leaderboard.RetrievalSettings{})
					So(err, ShouldBeNil)
					So(len(top), ShouldEqual, numKeys)

					for i := 0; i < numKeys; i++ {
						entry := inputData[i]
						actualScore, err := lb.GetScore(entry.Key, leaderboard.RetrievalSettings{})
						So(err, ShouldBeNil)
						So(actualScore.DecodeScore().BaseScore, ShouldEqual, entry.Score.BaseScore)
					}
				})
			})
		})

	})
}

func GetTopTest(lb leaderboard.Leaderboard) {
	Convey("Given random input data that is added to the leaderboard", func() {
		numKeys := 100
		inputData := RandomEntries(numKeys)
		AddEntries(lb, inputData...)

		Convey("Requesting the top entry will return exactly one entry", func() {
			results, err := lb.GetTop(context.Background(), 1, leaderboard.RetrievalSettings{})
			So(err, ShouldBeNil)
			So(len(results), ShouldEqual, 1)
		})

		Convey("Requesting the top several entries will return that many entries", func() {
			results, err := lb.GetTop(context.Background(), 50, leaderboard.RetrievalSettings{})
			So(err, ShouldBeNil)
			So(len(results), ShouldEqual, 50)
		})

		Convey("Requesting more than the top total number of entries will return just the total number", func() {
			lbSize, err := lb.Len(leaderboard.RetrievalSettings{})
			So(err, ShouldBeNil)

			results, err := lb.GetTop(context.Background(), lbSize+1, leaderboard.RetrievalSettings{})
			So(err, ShouldBeNil)
			So(len(results), ShouldEqual, lbSize)
		})
	})
}

func GetCountForRangeTest(lb leaderboard.Leaderboard) {
	Convey("Given fixed input data that is added to the leaderboard", func() {
		inputData := make([]entry.Entry, 0)
		inputData = append(inputData, buildCorrectlyOrderedTies(3, 1)...)
		inputData = append(inputData, buildCorrectlyOrderedTies(2, 1)...)
		inputData = append(inputData, buildCorrectlyOrderedTies(1, 1)...)
		AddEntries(lb, inputData...)

		Convey("when a min is not provided", func() {
			Convey("we should return the count of elements that fall in that range, defaulting min to 0", func() {
				var max int64 = 3
				count, err := lb.GetCountForRange(context.Background(), nil, &max, leaderboard.RetrievalSettings{})
				So(err, ShouldBeNil)
				So(count, ShouldEqual, 3)
			})
		})

		Convey("when a max is not provided", func() {
			Convey("we should return the count of elements that fall in that range, defaulting max to infinity", func() {
				var min int64 = 0
				count, err := lb.GetCountForRange(context.Background(), &min, nil, leaderboard.RetrievalSettings{})
				So(err, ShouldBeNil)
				So(count, ShouldEqual, 3)
			})
		})

		Convey("when both a min and max are provided", func() {
			Convey("we should return the count of elements that fall in that range", func() {
				var min int64 = 1
				var max int64 = 2
				count, err := lb.GetCountForRange(context.Background(), &min, &max, leaderboard.RetrievalSettings{})
				So(err, ShouldBeNil)
				So(count, ShouldEqual, 2)
			})
		})
	})
}

func GetEntriesAboveThreshold(lb leaderboard.Leaderboard) {
	Convey("Given fixed input data that is added to the leaderboard", func() {
		inputData := make([]entry.Entry, 0)
		inputData = append(inputData, buildCorrectlyOrderedTies(3, 1)...)
		inputData = append(inputData, buildCorrectlyOrderedTies(2, 1)...)
		inputData = append(inputData, buildCorrectlyOrderedTies(1, 1)...)
		AddEntries(lb, inputData...)

		Convey("when the offset nets an index that is out of range, we should return 0 items", func() {
			entries, err := lb.GetEntriesAboveThreshold(0, 1, 3, leaderboard.RetrievalSettings{})
			So(entries, ShouldHaveLength, 0)
			So(err, ShouldBeNil)
		})

		Convey("when the threshold filters out all results, we should return 0 items", func() {
			entries, err := lb.GetEntriesAboveThreshold(4, 1, 0, leaderboard.RetrievalSettings{})
			So(entries, ShouldHaveLength, 0)
			So(err, ShouldBeNil)
		})

		Convey("we should return all results above a threshold", func() {
			entries, err := lb.GetEntriesAboveThreshold(0, 3, 0, leaderboard.RetrievalSettings{})
			So(err, ShouldBeNil)
			So(entries, ShouldHaveLength, 3)
		})

		Convey("we should apply a pagination offset", func() {
			entries, err := lb.GetEntriesAboveThreshold(0, 2, 1, leaderboard.RetrievalSettings{})
			So(err, ShouldBeNil)
			So(entries, ShouldHaveLength, 2)
		})

		Convey("we should apply a pagination limit", func() {
			entries, err := lb.GetEntriesAboveThreshold(0, 2, 0, leaderboard.RetrievalSettings{})
			So(err, ShouldBeNil)
			So(entries, ShouldHaveLength, 2)
		})

		Convey("we should apply an upper bound to pagination limit", func() {
			entries, err := lb.GetEntriesAboveThreshold(0, 100, 0, leaderboard.RetrievalSettings{})
			So(err, ShouldBeNil)
			So(entries, ShouldHaveLength, 3)
		})
	})
}

func GetEntryContextTest(lb leaderboard.Leaderboard) {
	Convey("Given score-sorted input data that is added to the leaderboard", func() {
		numKeys := 100
		inputData := DescendingScoreEntries(numKeys)
		AddEntries(lb, inputData...)

		Convey("Requesting any context entry with zero spread returns an array with just that entry", func() {
			spread := 0
			index := random.Int(0, 99)
			key := inputData[index].Key

			_, result, err := lb.GetEntryContext(context.Background(), key, spread, leaderboard.RetrievalSettings{})

			So(err, ShouldBeNil)
			So(len(result), ShouldEqual, 1)
			So(result[0].Key, ShouldEqual, key)
		})

		Convey("Requesting a context entry with a spread", func() {
			spread := 5

			Convey("Near the middle of the leaderboard", func() {
				index := len(inputData) / 2
				key := inputData[index].Key

				Convey("Returns the context entry and spread number of entries above and below it", func() {
					_, results, err := lb.GetEntryContext(context.Background(), key, spread, leaderboard.RetrievalSettings{})

					So(err, ShouldBeNil)
					So(len(results), ShouldEqual, spread+1+spread)
					verifyAllEntries(inputData, index-spread, index+spread, results)
				})
			})

			Convey("Near the top of the leaderboard", func() {
				index := 2
				key := inputData[index].Key

				Convey("Returns the context entry and spread number of entries below it and up to spread number of entries above it", func() {
					_, results, err := lb.GetEntryContext(context.Background(), key, spread, leaderboard.RetrievalSettings{})

					So(err, ShouldBeNil)
					So(len(results), ShouldEqual, 2+1+spread)
					verifyAllEntries(inputData, index-2, index+spread, results)
				})
			})

			Convey("At exactly the top of the leaderboard", func() {
				index := 0
				key := inputData[index].Key

				Convey("Returns the context entry and spread number of entries below it", func() {
					_, results, err := lb.GetEntryContext(context.Background(), key, spread, leaderboard.RetrievalSettings{})

					So(err, ShouldBeNil)
					So(len(results), ShouldEqual, 0+1+spread)
					verifyAllEntries(inputData, 0, spread, results)
				})
			})

			Convey("Near the bottom of the leaderboard", func() {
				index := len(inputData) - 3
				key := inputData[index].Key

				Convey("Returns the context entry and spread number of entries above it and up to spread number of entries below it", func() {
					_, results, err := lb.GetEntryContext(context.Background(), key, spread, leaderboard.RetrievalSettings{})

					So(err, ShouldBeNil)
					So(len(results), ShouldEqual, spread+1+2)
					verifyAllEntries(inputData, index-spread, index+2, results)
				})
			})

			Convey("At exactly the bottom of the leaderboard", func() {
				index := len(inputData) - 1
				key := inputData[index].Key

				Convey("Returns the context entry and spread number of entries above it", func() {
					_, results, err := lb.GetEntryContext(context.Background(), key, spread, leaderboard.RetrievalSettings{})

					So(err, ShouldBeNil)
					So(len(results), ShouldEqual, spread+1+0)
					verifyAllEntries(inputData, index-spread, index, results)
				})
			})
		})
	})
}

func buildCorrectlyOrderedTies(baseScore score.BaseScore, n int) []entry.Entry {
	startTime := time.Now()
	timeOffset := time.Duration(0)
	entries := make([]entry.Entry, 0)
	for i := 0; i < n; i++ {
		timeOffset = time.Duration(timeOffset.Nanoseconds() + random.Duration(1*time.Second, 24*time.Hour).Nanoseconds())
		entries = append(entries, entry.Entry{
			Key:   entry.Key(random.String(16)),
			Score: score.ToEntryScore(score.BaseScore(baseScore), score.EventTime(startTime.Add(timeOffset).UnixNano()), score.NotModerated),
		})
	}
	return entries
}

func TimeBreaksTiesTest(lb leaderboard.Leaderboard) {
	Convey("Given fixed input data with many ties", func() {
		inputData := make([]entry.Entry, 0)
		inputData = append(inputData, buildCorrectlyOrderedTies(1000000, 3)...)
		inputData = append(inputData, buildCorrectlyOrderedTies(100000, 3)...)
		inputData = append(inputData, buildCorrectlyOrderedTies(10000, 3)...)
		inputData = append(inputData, buildCorrectlyOrderedTies(1000, 3)...)
		inputData = append(inputData, buildCorrectlyOrderedTies(100, 3)...)
		inputData = append(inputData, buildCorrectlyOrderedTies(2, 3)...)
		inputData = append(inputData, buildCorrectlyOrderedTies(1, 3)...)

		AddEntries(lb, inputData...)

		Convey("Requesting all entries returns them in the proper order", func() {
			results, err := lb.GetTop(context.Background(), len(inputData), leaderboard.RetrievalSettings{})
			So(err, ShouldBeNil)

			for i, thisEntry := range results {
				So(thisEntry.Key, ShouldEqual, inputData[i].Key)
				So(thisEntry.Score.BaseScore, ShouldEqual, inputData[i].Score.BaseScore)
			}
		})
	})
}

func verifyAllEntries(inputData []entry.Entry, startIndex int, endIndex int, results []entry.RankedEntry) {
	for i, e := range inputData[startIndex : endIndex+1] {
		So(results[i].Key, ShouldEqual, e.Key)
		So(results[i].Score.BaseScore, ShouldEqual, e.Score.BaseScore)
	}
}
