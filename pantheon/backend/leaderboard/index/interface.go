package index

import (
	"context"

	"code.justin.tv/commerce/pantheon/backend/leaderboard"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/identifier"
)

type Index interface {
	Key() string
	Add(context context.Context, id identifier.Identifier) error
	Get(context context.Context, rs leaderboard.RetrievalSettings) ([]identifier.Identifier, error)
	Delete(context context.Context) error
	DeleteMember(context context.Context, id identifier.Identifier) error
}
