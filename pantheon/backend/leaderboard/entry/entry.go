package entry

import (
	"fmt"

	"code.justin.tv/commerce/pantheon/backend/leaderboard/score"
)

type Key string

type Rank int64

type Entry struct {
	Key   Key
	Score score.EntryScore
}

type RankedEntry struct {
	Entry
	Rank Rank
}

func Equals(e1 Entry, e2 Entry) bool {
	return e1.Key == e2.Key &&
		score.EntryScoreEquals(e1.Score, e2.Score)
}

func RankedEntryEquals(e1 RankedEntry, e2 RankedEntry) bool {
	return Equals(e1.Entry, e2.Entry) &&
		e1.Rank == e2.Rank

}

func (e Entry) String() string {
	return fmt.Sprintf("%s: %v (%v)", e.Key, e.Score.BaseScore, e.Score.EventTime)
}

func ToRankedEntry(entry Entry, rank int64) RankedEntry {
	return RankedEntry{
		Entry: entry,
		Rank:  Rank(rank),
	}
}

func EntriesToRankedEntries(entries []Entry, startRank int64) []RankedEntry {
	rankedEntries := make([]RankedEntry, len(entries))

	for i, e := range entries {
		rankedEntries[i] = ToRankedEntry(e, startRank+int64(i))
	}

	return rankedEntries
}

func EntriesPtoEntries(entriesP []*Entry) []Entry {
	entries := make([]Entry, len(entriesP))

	for i, e := range entriesP {
		if e == nil {
			continue
		}

		entries[i] = *e
	}

	return entries
}
