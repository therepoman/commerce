package archival

import "encoding/json"

type Message struct {
	Key string `json:"key"`
}

func ToJSON(m Message) (string, error) {
	b, err := json.Marshal(&m)
	if err != nil {
		return "", err
	}
	return string(b), nil
}

func FromJSON(jm string) (*Message, error) {
	var msg Message
	err := json.Unmarshal([]byte(jm), &msg)
	if err != nil {
		return nil, err
	}
	return &msg, nil
}
