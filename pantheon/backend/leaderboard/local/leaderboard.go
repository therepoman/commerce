package local

import (
	"bytes"
	"context"
	"encoding/gob"
	"fmt"
	stdmath "math"
	"sort"
	"time"

	"code.justin.tv/commerce/pantheon/backend/leaderboard"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/entry"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/event"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/score"
	"code.justin.tv/commerce/pantheon/utils/math"
)

type Leaderboard struct {
	orderedEntries []*entry.Entry
	entryLookup    map[entry.Key]*entry.Entry
}

type ByScore []*entry.Entry

func (s ByScore) Len() int      { return len(s) }
func (s ByScore) Swap(i, j int) { s[i], s[j] = s[j], s[i] }
func (s ByScore) Less(i, j int) bool {
	if s[i] == nil && s[j] != nil {
		return true
	}
	if s[i] != nil && s[j] == nil {
		return false
	}
	if s[i].Score.BaseScore != s[j].Score.BaseScore {
		return s[i].Score.BaseScore < s[j].Score.BaseScore
	}
	if s[i].Score.EventTime != s[j].Score.EventTime {
		return s[i].Score.EventTime > s[j].Score.EventTime
	}
	return s[i].Key < s[j].Key
}

func NewLeaderboard() *Leaderboard {
	return &Leaderboard{
		orderedEntries: make([]*entry.Entry, 0),
		entryLookup:    make(map[entry.Key]*entry.Entry),
	}
}

func (l *Leaderboard) Type() string {
	return "local"
}

func (l *Leaderboard) sort() {
	sort.Sort(sort.Reverse(ByScore(l.orderedEntries)))
}

func (l *Leaderboard) SetScore(key entry.Key, score score.EntryScore) error {
	existingEntry, isPresent := l.entryLookup[key]
	if isPresent && existingEntry != nil {
		existingEntry.Score = score
	} else {
		newEntry := &entry.Entry{
			Key:   key,
			Score: score,
		}
		l.orderedEntries = append(l.orderedEntries, newEntry)
		l.entryLookup[key] = newEntry
	}
	l.sort()
	return nil
}

func (l *Leaderboard) UpdateScore(key entry.Key, scoreAddition score.EntryScore, event *event.Event) error {
	prevScore, err := l.GetScore(key, leaderboard.RetrievalSettings{})
	if err != nil {
		return err
	}

	return l.SetScore(key, prevScore.Add(scoreAddition).DecodeScore())
}

func (l *Leaderboard) ClearScores() error {
	l.orderedEntries = make([]*entry.Entry, 0)
	l.entryLookup = make(map[entry.Key]*entry.Entry)
	return nil
}

func (l *Leaderboard) Len(rs leaderboard.RetrievalSettings) (int, error) {
	return len(l.orderedEntries), nil
}

func (l *Leaderboard) GetScore(key entry.Key, rs leaderboard.RetrievalSettings) (score.Score, error) {
	var targetScore score.Score
	existingEntry, isPresent := l.entryLookup[key]
	if isPresent && existingEntry != nil {
		targetScore = existingEntry.Score.EncodeScore()
	}
	return targetScore, nil
}

func (l *Leaderboard) GetTop(ctx context.Context, n int, rs leaderboard.RetrievalSettings) ([]entry.RankedEntry, error) {
	n = math.MinInt(n, len(l.orderedEntries))

	topP := l.orderedEntries[:n]

	top := make([]entry.RankedEntry, len(topP))
	for i := 0; i < len(topP); i++ {
		top[i] = entry.RankedEntry{
			Entry: *topP[i],
			Rank:  entry.Rank(i + 1),
		}
	}

	return top, nil
}

func (l *Leaderboard) GetTopWithTimeoutAndRetry(ctx context.Context, n int, rs leaderboard.RetrievalSettings, timeout time.Duration, maxAttempts int) ([]entry.RankedEntry, error) {
	return l.GetTop(ctx, n, rs)
}

func (l *Leaderboard) GetEntryContext(ctx context.Context, key entry.Key, spread int, rs leaderboard.RetrievalSettings) (entryOut entry.RankedEntry, entries []entry.RankedEntry, err error) {
	_, isPresent := l.entryLookup[key]

	if !isPresent {
		return
	}

	keyIndex := 0
	for i, entry := range l.orderedEntries {
		if entry.Key == key {
			keyIndex = i
			break
		}
	}

	contextIndexStart := math.MaxInt(0, keyIndex-spread)
	contextIndexEnd := math.MinInt(len(l.orderedEntries)-1, keyIndex+spread)

	entriesP := l.orderedEntries[contextIndexStart : contextIndexEnd+1]
	entries = entry.EntriesToRankedEntries(entry.EntriesPtoEntries(entriesP), int64(contextIndexStart+1))
	entryOut = entries[keyIndex-contextIndexStart]
	return
}

func (l *Leaderboard) GetEntryContextWithTimeoutAndRetry(ctx context.Context, key entry.Key, spread int, rs leaderboard.RetrievalSettings, timeout time.Duration, maxAttempts int) (entryOut entry.RankedEntry, entries []entry.RankedEntry, err error) {
	return l.GetEntryContext(ctx, key, spread, rs)
}

func (l *Leaderboard) GetRank(k entry.Key, rs leaderboard.RetrievalSettings) (entry.Rank, error) {
	for i, elem := range l.orderedEntries {
		if elem != nil && k == elem.Key {
			return entry.Rank(i + 1), nil
		}
	}

	return entry.Rank(0), fmt.Errorf("could not find key %v in set", k)
}

func (lb *Leaderboard) GetCountForRange(ctx context.Context, min *int64, max *int64, rs leaderboard.RetrievalSettings) (int64, error) {
	var minValue int64
	var maxValue int64
	var count int64 = 0

	if min == nil {
		minValue = 0
	} else {
		minValue = *min
	}

	if max == nil {
		maxValue = stdmath.MaxInt64
	} else {
		maxValue = *max
	}

	for _, elem := range lb.orderedEntries {
		if elem == nil {
			continue
		}

		baseScore := int64(elem.Score.BaseScore)

		if baseScore >= minValue && baseScore <= maxValue {
			count++
		}
	}

	return count, nil
}

func (lb *Leaderboard) GetEntriesAboveThreshold(threshold, limit, offset int64, rs leaderboard.RetrievalSettings) ([]entry.RankedEntry, error) {
	var filteredEntries []entry.Entry

	for _, elem := range lb.orderedEntries {
		if elem == nil {
			continue
		}

		baseScore := int64(elem.Score.BaseScore)

		if baseScore >= threshold {
			filteredEntries = append(filteredEntries, *elem)
		}
	}

	filteredItemCount := int64(len(filteredEntries))

	// Return nothing if list does not contain at least one item after the offset
	if offset+1 > filteredItemCount {
		return nil, nil
	}

	withOffset := filteredEntries[offset:]

	boundLimit := math.MinInt64(limit, int64(len(withOffset)))

	withLimit := withOffset[0:boundLimit]

	return entry.EntriesToRankedEntries(withLimit, offset+1), nil
}

func (l *Leaderboard) Serialize(rs leaderboard.RetrievalSettings) ([]byte, error) {
	buf := bytes.Buffer{}
	enc := gob.NewEncoder(&buf)
	err := enc.Encode(l.orderedEntries)
	if err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}

func (l *Leaderboard) Restore(bs []byte) error {
	reader := bytes.NewReader(bs)
	dec := gob.NewDecoder(reader)
	var orderedEntries []*entry.Entry
	err := dec.Decode(&orderedEntries)
	if err != nil {
		return err
	}
	l.orderedEntries = orderedEntries
	for _, entry := range l.orderedEntries {
		l.entryLookup[entry.Key] = entry
	}
	return nil
}

func (l *Leaderboard) Destroy() error {
	return l.ClearScores()
}

func (l *Leaderboard) ModerateEntry(key entry.Key) error {
	curScore, err := l.GetScore(key, leaderboard.RetrievalSettings{ConsistentRead: true})
	if err != nil {
		return err
	}
	newScore := score.EntryScore{
		ModeratedFlag: score.Moderated,
		BaseScore:     curScore.DecodeScore().BaseScore,
		EventTime:     curScore.DecodeScore().EventTime,
	}
	return l.SetScore(key, newScore)
}

func (l *Leaderboard) UnmoderateEntry(key entry.Key) error {
	curScore, err := l.GetScore(key, leaderboard.RetrievalSettings{ConsistentRead: true})
	if err != nil {
		return err
	}
	newScore := score.EntryScore{
		ModeratedFlag: score.NotModerated,
		BaseScore:     curScore.DecodeScore().BaseScore,
		EventTime:     curScore.DecodeScore().EventTime,
	}
	return l.SetScore(key, newScore)
}

func (l *Leaderboard) DeleteEntry(targetEntryKey entry.Key) error {
	newOrderedEntry := make([]*entry.Entry, 0)
	for _, entry := range l.orderedEntries {
		if entry.Key != targetEntryKey {
			newOrderedEntry = append(newOrderedEntry, entry)
		}
	}
	l.orderedEntries = newOrderedEntry
	delete(l.entryLookup, targetEntryKey)
	return nil
}
