package local

import (
	"context"
	"fmt"

	"code.justin.tv/commerce/pantheon/backend/leaderboard"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/identifier"
)

const (
	GroupingKeyIndexPrefix = "grouping_key_index"
)

type GroupingKeyIndex struct {
	items       map[string][]string
	Domain      identifier.Domain
	GroupingKey identifier.GroupingKey
}

func NewGroupingKeyIndex(domain identifier.Domain, groupingKey identifier.GroupingKey) *GroupingKeyIndex {
	return &GroupingKeyIndex{
		items:       make(map[string][]string),
		Domain:      domain,
		GroupingKey: groupingKey,
	}
}

func (i *GroupingKeyIndex) Key() string {
	return fmt.Sprintf("%s-%s-%s", GroupingKeyIndexPrefix, i.Domain, i.GroupingKey)
}

func (i *GroupingKeyIndex) Add(context context.Context, id identifier.Identifier) error {
	_, isPresent := i.items[i.Key()]
	if !isPresent {
		i.items[i.Key()] = make([]string, 0)
	}
	i.items[i.Key()] = append(i.items[i.Key()], id.Key())
	return nil
}

func (i *GroupingKeyIndex) Delete(context context.Context) error {
	delete(i.items, i.Key())
	return nil
}

func (i *GroupingKeyIndex) DeleteMember(context context.Context, id identifier.Identifier) error {
	keys, isPresent := i.items[i.Key()]
	if isPresent {
		for index, key := range keys {
			if key == id.Key() {
				i.items[i.Key()] = append(keys[:index], keys[index+1:]...)
				return nil
			}
		}
	}
	return nil
}

func (i *GroupingKeyIndex) Get(context context.Context, rs leaderboard.RetrievalSettings) ([]identifier.Identifier, error) {
	if members, ok := i.items[i.Key()]; ok {
		var ids []identifier.Identifier
		for _, key := range members {
			id, err := identifier.FromKey(key)
			if err != nil {
				return nil, err
			}

			ids = append(ids, id)
		}
		return ids, nil
	}

	return make([]identifier.Identifier, 0), nil
}
