package adapter

import (
	"context"

	"code.justin.tv/commerce/pantheon/backend/leaderboard"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/local"
)

type LeaderboardAdapter interface {
	ConvertToLocal(src leaderboard.Leaderboard) (*local.Leaderboard, error)
}

type LeaderboardAdapterImpl struct {
}

func (la *LeaderboardAdapterImpl) ConvertToLocal(src leaderboard.Leaderboard) (*local.Leaderboard, error) {
	n, err := src.Len(leaderboard.RetrievalSettings{})
	if err != nil {
		return nil, err
	}

	topN, err := src.GetTop(context.Background(), n, leaderboard.RetrievalSettings{})
	if err != nil {
		return nil, err
	}

	dest := local.NewLeaderboard()
	for _, entry := range topN {
		err = dest.SetScore(entry.Key, entry.Score)
		if err != nil {
			return nil, err
		}
	}

	return dest, nil
}
