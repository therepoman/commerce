package adapter

import (
	"context"
	"errors"
	"testing"
	"time"

	"code.justin.tv/commerce/pantheon/backend/leaderboard"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/entry"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/score"
	leaderboard_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/backend/leaderboard"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestLeaderboardAdapterImpl_ConvertToLocal(t *testing.T) {
	Convey("Given a local leaderboard adapter and a source leaderboard", t, func() {
		adapter := &LeaderboardAdapterImpl{}
		src := new(leaderboard_mock.Leaderboard)

		Convey("Errors when source len errors", func() {
			src.On("Len", mock.Anything).Return(0, errors.New("error"))
			_, err := adapter.ConvertToLocal(src)
			So(err, ShouldNotBeNil)
		})

		Convey("When len returns", func() {
			lenResult := 3
			src.On("Len", mock.Anything).Return(lenResult, nil)

			Convey("Errors when get top errors", func() {
				src.On("GetTop", mock.Anything, lenResult, mock.Anything).Return(nil, errors.New("error"))
				_, err := adapter.ConvertToLocal(src)
				So(err, ShouldNotBeNil)
			})

			Convey("Succeeds when get top returns zero records", func() {
				src.On("GetTop", mock.Anything, lenResult, mock.Anything).Return([]entry.RankedEntry{}, nil)
				resp, err := adapter.ConvertToLocal(src)
				So(err, ShouldBeNil)
				localLen, err := resp.Len(leaderboard.RetrievalSettings{})
				So(err, ShouldBeNil)
				So(localLen, ShouldEqual, 0)
			})

			Convey("Succeeds when get top returns multiple records", func() {
				eventTime := score.EventTime(time.Now().UnixNano())
				rankedEntries := []entry.RankedEntry{
					entry.ToRankedEntry(entry.Entry{Key: "a", Score: score.ToEntryScore(3, eventTime, score.NotModerated)}, 1),
					entry.ToRankedEntry(entry.Entry{Key: "b", Score: score.ToEntryScore(2, eventTime, score.NotModerated)}, 2),
					entry.ToRankedEntry(entry.Entry{Key: "c", Score: score.ToEntryScore(1, eventTime, score.NotModerated)}, 3),
				}
				src.On("GetTop", mock.Anything, lenResult, mock.Anything).Return(rankedEntries, nil)
				resp, err := adapter.ConvertToLocal(src)
				So(err, ShouldBeNil)

				localTop, err := resp.GetTop(context.Background(), lenResult, leaderboard.RetrievalSettings{})
				So(err, ShouldBeNil)

				So(entry.RankedEntryEquals(rankedEntries[0], localTop[0]), ShouldBeTrue)
				So(entry.RankedEntryEquals(rankedEntries[1], localTop[1]), ShouldBeTrue)
				So(entry.RankedEntryEquals(rankedEntries[2], localTop[2]), ShouldBeTrue)
			})
		})
	})
}
