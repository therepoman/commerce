package local

import (
	"testing"

	"code.justin.tv/commerce/pantheon/backend/leaderboard/test"
	. "github.com/smartystreets/goconvey/convey"
)

func TestFactory_GetLocalGroupingKeyIndex(t *testing.T) {
	Convey("Given a local groupingKey index factory", t, func() {
		factory := IndexFactory{}

		Convey("Can create a grouping key index", func() {
			id := test.RandomIdentifier()
			i := factory.GetLocalGroupingKeyIndex(id.Domain, id.GroupingKey)
			So(i, ShouldNotBeNil)
		})
	})
}
