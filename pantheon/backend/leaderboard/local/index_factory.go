package local

import (
	"code.justin.tv/commerce/pantheon/backend/leaderboard/identifier"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/index"
)

type GroupingKeyIndexFactory interface {
	GetLocalGroupingKeyIndex(domain identifier.Domain, groupingKey identifier.GroupingKey) index.Index
}

type IndexFactory struct{}

func (f IndexFactory) GetLocalGroupingKeyIndex(domain identifier.Domain, groupingKey identifier.GroupingKey) index.Index {
	return NewGroupingKeyIndex(domain, groupingKey)
}
