package local

import (
	"context"

	"code.justin.tv/commerce/pantheon/backend/leaderboard"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/identifier"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/persistence"
)

type LeaderboardFactory interface {
	GetLocalLeaderboard(ctx context.Context, id identifier.Identifier) (leaderboard.Leaderboard, error)
}

type Factory struct {
	Persister persistence.Persister `inject:""`
}

func (f Factory) GetLocalLeaderboard(ctx context.Context, id identifier.Identifier) (leaderboard.Leaderboard, error) {
	lb := NewLeaderboard()
	err := f.Persister.Import(ctx, id, lb)
	if err != nil {
		return nil, err
	}
	return lb, nil
}
