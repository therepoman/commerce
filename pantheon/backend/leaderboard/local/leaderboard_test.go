package local

import (
	"testing"

	"code.justin.tv/commerce/pantheon/backend/leaderboard"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/entry"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/event"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/score"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/test"
	. "github.com/smartystreets/goconvey/convey"
)

func TestRetrieval(t *testing.T) {
	Convey("Given a local Leaderboard", t, func() {
		test.RetrievalTest(NewLeaderboard())
	})
}

func TestRoundTrip(t *testing.T) {
	Convey("Given a local Leaderboard", t, func() {
		test.SerializationRoundTripTest(NewLeaderboard())
	})
}

func TestGetTop(t *testing.T) {
	Convey("Given a local Leaderboard", t, func() {
		test.GetTopTest(NewLeaderboard())
	})
}

func TestGetCountForRange(t *testing.T) {
	Convey("Given a local Leaderboard", t, func() {
		test.GetCountForRangeTest(NewLeaderboard())
	})
}

func TestGetEntriesAboveThreshold(t *testing.T) {
	Convey("Given a local Leaderboard", t, func() {
		test.GetEntriesAboveThreshold(NewLeaderboard())
	})
}

func TestGetEntryContext(t *testing.T) {
	Convey("Given a local Leaderboard", t, func() {
		test.GetEntryContextTest(NewLeaderboard())
	})
}

func TestTimeBreaksTies(t *testing.T) {
	Convey("Given a local Leaderboard", t, func() {
		test.TimeBreaksTiesTest(NewLeaderboard())
	})
}

func TestModerateAndUnmoderate(t *testing.T) {
	Convey("Given a local Leaderboard", t, func() {
		lb := NewLeaderboard()
		key := entry.Key("entry-key")

		err := lb.UpdateScore(key, score.EntryScore{BaseScore: 1}, &event.Event{})
		So(err, ShouldBeNil)

		err = lb.ModerateEntry(key)
		So(err, ShouldBeNil)

		gottenScore1, err := lb.GetScore(key, leaderboard.RetrievalSettings{ConsistentRead: true})
		So(err, ShouldBeNil)
		So(gottenScore1.DecodeScore().BaseScore, ShouldEqual, score.BaseScore(1))
		So(gottenScore1.DecodeScore().ModeratedFlag, ShouldEqual, score.Moderated)

		err = lb.UnmoderateEntry(key)
		So(err, ShouldBeNil)

		gottenScore2, err := lb.GetScore(key, leaderboard.RetrievalSettings{ConsistentRead: true})
		So(err, ShouldBeNil)
		So(gottenScore2.DecodeScore().BaseScore, ShouldEqual, score.BaseScore(1))
		So(gottenScore2.DecodeScore().ModeratedFlag, ShouldEqual, score.NotModerated)
	})
}

func TestDeleteEntry(t *testing.T) {
	Convey("Given a local Leaderboard", t, func() {
		lb := NewLeaderboard()
		key := entry.Key("entry-key")

		err := lb.UpdateScore(key, score.EntryScore{BaseScore: 1}, &event.Event{})
		So(err, ShouldBeNil)

		err = lb.DeleteEntry(key)
		So(err, ShouldBeNil)

		size, err := lb.Len(leaderboard.RetrievalSettings{ConsistentRead: true})
		So(err, ShouldBeNil)
		So(size, ShouldEqual, 0)
	})
}
