package local

import (
	"context"
	"testing"

	"code.justin.tv/commerce/pantheon/backend/leaderboard"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/identifier"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/test"
	. "github.com/smartystreets/goconvey/convey"
)

func TestGroupingKeyIndex(t *testing.T) {
	Convey("Given a grouping key index", t, func() {
		id := test.RandomIdentifier()
		i := NewGroupingKeyIndex(id.Domain, id.GroupingKey)

		Convey("Members can be added and fetched", func() {
			id1 := test.RandomIdentifier()
			err := i.Add(context.Background(), id1)
			So(err, ShouldBeNil)
			var results []identifier.Identifier
			results, _ = i.Get(context.Background(), leaderboard.RetrievalSettings{})
			So(results[0].Key(), ShouldEqual, id1.Key())
		})

		Convey("Members can be deleted", func() {
			id1 := test.RandomIdentifier()
			err := i.Add(context.Background(), id1)
			So(err, ShouldBeNil)
			err = i.DeleteMember(context.Background(), id1)
			So(err, ShouldBeNil)
			var results []identifier.Identifier
			results, _ = i.Get(context.Background(), leaderboard.RetrievalSettings{})
			So(results, ShouldBeNil)
		})

		Convey("Index can be deleted", func() {
			err := i.Delete(context.Background())
			So(err, ShouldBeNil)
			var results []identifier.Identifier
			results, _ = i.Get(context.Background(), leaderboard.RetrievalSettings{})
			So(len(results), ShouldEqual, 0)
		})
	})
}
