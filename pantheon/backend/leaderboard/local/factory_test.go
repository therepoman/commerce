package local

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/pantheon/backend/leaderboard/test"
	persistence_mock "code.justin.tv/commerce/pantheon/mocks/code.justin.tv/commerce/pantheon/backend/leaderboard/persistence"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestFactory_GetLocalLeaderboard(t *testing.T) {
	Convey("Given a local leaderboard factory", t, func() {
		mockedPersister := new(persistence_mock.Persister)
		factory := Factory{
			Persister: mockedPersister,
		}

		Convey("Errors when import errors", func() {
			mockedPersister.On("Import", mock.Anything, mock.Anything, mock.Anything).Return(errors.New("error"))
			_, err := factory.GetLocalLeaderboard(context.Background(), test.RandomIdentifier())
			So(err, ShouldNotBeNil)
		})

		Convey("Succeeds when import succeeds", func() {
			mockedPersister.On("Import", mock.Anything, mock.Anything, mock.Anything).Return(nil)
			_, err := factory.GetLocalLeaderboard(context.Background(), test.RandomIdentifier())
			So(err, ShouldBeNil)
		})
	})
}
