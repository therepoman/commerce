package domain

import (
	"time"

	"code.justin.tv/commerce/pantheon/backend/leaderboard/identifier"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
)

const (
	UnknownDomain            = identifier.Domain("unknown")
	BitsUsageByChannelDomain = identifier.Domain("bits-usage-by-channel-v1")

	FortunaParticipationDomain          = identifier.Domain("fortuna.participation")
	FortunaParticipationMaxLeaderboards = 150

	FortunaProgressDomain          = identifier.Domain("fortuna.progress")
	FortunaProgressMaxLeaderboards = 150

	SubsGiftSentDomain        = identifier.Domain("sub-gifts-sent")
	HGC2017Domain             = identifier.Domain("hgc2017")
	HGC2018Domain             = identifier.Domain("hgc2018-launch")
	GlobalDomain              = identifier.Domain("GLOBAL")
	OverwatchLeague2018Domain = identifier.Domain("overwatch-league-stage-2")
	Hearthstone2018Domain     = identifier.Domain("hearthstone2018-launch")
	Hearthstone2018TestDomain = identifier.Domain("hearthstone2018-test-launch")
	Owl2019Domain             = identifier.Domain("owl2019")
	Owl2019TestDomain         = identifier.Domain("owl2019-test")
	PollsDomain               = identifier.Domain("polls")
	HypeTrainDomain           = identifier.Domain("trains")
	CoGoTrainDomain           = identifier.Domain("cogo")

	Owl2018TeamGroupingKey = identifier.GroupingKey("team-owl")
	Owl2019TeamGroupingKey = identifier.GroupingKey("GlobalTeam")
	Owl2018TeamTopN        = 12
	Owl2019TeamTopN        = 20
	Owl2019MaxLeaderboards = 150

	Hgc2018TeamGroupingKey = identifier.GroupingKey("GlobalTeam")
	Hgc2018TeamTopN        = 32

	Hearthstone2018TeamGroupingKey     = identifier.GroupingKey("GlobalTeam")
	Hearthstone2018TestTeamGroupingKey = identifier.GroupingKey("GlobalTeam")
	Hearthstone2018TeamTopN            = 16

	DefaultMaxLeaderboards = 50
	DefaultTopMax          = 100
	DefaultSpreadMax       = 50
)

type TimeUnitSet map[pantheonrpc.TimeUnit]interface{}

func (tus TimeUnitSet) Contains(tu pantheonrpc.TimeUnit) bool {
	_, isPresent := tus[tu]
	return isPresent
}

func AllTimeUnits() map[pantheonrpc.TimeUnit]interface{} {
	return map[pantheonrpc.TimeUnit]interface{}{
		pantheonrpc.TimeUnit_DAY:     nil,
		pantheonrpc.TimeUnit_WEEK:    nil,
		pantheonrpc.TimeUnit_MONTH:   nil,
		pantheonrpc.TimeUnit_YEAR:    nil,
		pantheonrpc.TimeUnit_ALLTIME: nil,
	}
}

func SingleTimeUnit(timeUnit pantheonrpc.TimeUnit) map[pantheonrpc.TimeUnit]interface{} {
	return map[pantheonrpc.TimeUnit]interface{}{
		timeUnit: nil,
	}
}

type Config struct {
	Domain                   identifier.Domain
	EnabledTimeUnits         TimeUnitSet
	PubsubTopNByGroupingKey  map[identifier.GroupingKey]int
	TopMax                   int
	SpreadMax                int
	MaxLeaderboards          int
	ArchiveRedisLeaderboards bool
	ArchiveLocalLeaderboards bool
	// If true, removes users from their respective leaderboard when an indefinite channel ban is received
	EnableUserModerationHandling bool
	// Only effectual if EnableUserModerationHandling is `true`. If `true` listens to indefinite and temporary
	// suspensions (timeouts), if `false`, only the former.
	//
	// Note: this has historically been unreliable due to missing and out of order receipt of unban events (resulting in
	// users incorrectly leaderboard banned).
	EnableTemporarySuspensionHandling bool
	LeaderboardTTL                    bool
	LeaderboardTTLDuration            time.Duration
	MaxPublishDelaySeconds            int
}

func (c Config) IsKnownDomain() bool {
	return c.Domain != UnknownDomain
}

func (c Config) WithEnableUserModerationHandling() Config {
	c.EnableUserModerationHandling = true
	return c
}

func (c Config) WithEnableTemporarySuspensionHandling() Config {
	c.EnableTemporarySuspensionHandling = true
	return c
}

func DefaultConfig(domain identifier.Domain) Config {
	return Config{
		Domain:                       domain,
		EnabledTimeUnits:             AllTimeUnits(),
		PubsubTopNByGroupingKey:      map[identifier.GroupingKey]int{},
		TopMax:                       DefaultTopMax,
		SpreadMax:                    DefaultSpreadMax,
		MaxLeaderboards:              DefaultMaxLeaderboards,
		ArchiveRedisLeaderboards:     true,
		ArchiveLocalLeaderboards:     true,
		EnableUserModerationHandling: false,
	}
}

func UnknownDomainConfig() Config {
	return Config{
		Domain:                       UnknownDomain,
		EnabledTimeUnits:             AllTimeUnits(),
		PubsubTopNByGroupingKey:      map[identifier.GroupingKey]int{},
		TopMax:                       DefaultTopMax,
		SpreadMax:                    DefaultSpreadMax,
		MaxLeaderboards:              DefaultMaxLeaderboards,
		ArchiveRedisLeaderboards:     true,
		ArchiveLocalLeaderboards:     true,
		EnableUserModerationHandling: false,
	}
}

type ConfigManager interface {
	Get(domain identifier.Domain) Config
	GetAll() map[identifier.Domain]Config
}

type HardCodedConfigManager struct {
	domainConfigMapping map[identifier.Domain]Config
}

func NewHardCodedConfigManager() *HardCodedConfigManager {
	return &HardCodedConfigManager{
		domainConfigMapping: map[identifier.Domain]Config{
			BitsUsageByChannelDomain: DefaultConfig(BitsUsageByChannelDomain).WithEnableUserModerationHandling(),
			FortunaParticipationDomain: {
				Domain:                   FortunaParticipationDomain,
				EnabledTimeUnits:         SingleTimeUnit(pantheonrpc.TimeUnit_ALLTIME),
				PubsubTopNByGroupingKey:  map[identifier.GroupingKey]int{},
				TopMax:                   DefaultTopMax,
				SpreadMax:                DefaultSpreadMax,
				MaxLeaderboards:          FortunaParticipationMaxLeaderboards,
				ArchiveRedisLeaderboards: true,
				ArchiveLocalLeaderboards: false,
			},
			FortunaProgressDomain: {
				Domain:                   FortunaProgressDomain,
				EnabledTimeUnits:         SingleTimeUnit(pantheonrpc.TimeUnit_ALLTIME),
				PubsubTopNByGroupingKey:  map[identifier.GroupingKey]int{},
				TopMax:                   DefaultTopMax,
				SpreadMax:                DefaultSpreadMax,
				MaxLeaderboards:          FortunaProgressMaxLeaderboards,
				ArchiveRedisLeaderboards: true,
				ArchiveLocalLeaderboards: false,
			},
			SubsGiftSentDomain: DefaultConfig(SubsGiftSentDomain),
			HGC2017Domain:      DefaultConfig(HGC2017Domain),
			GlobalDomain:       DefaultConfig(GlobalDomain),
			OverwatchLeague2018Domain: {
				Domain:           OverwatchLeague2018Domain,
				EnabledTimeUnits: AllTimeUnits(),
				PubsubTopNByGroupingKey: map[identifier.GroupingKey]int{
					Owl2018TeamGroupingKey: Owl2018TeamTopN,
				},
				TopMax:                   DefaultTopMax,
				SpreadMax:                DefaultSpreadMax,
				MaxLeaderboards:          DefaultMaxLeaderboards,
				ArchiveRedisLeaderboards: true,
				ArchiveLocalLeaderboards: true,
			},
			HGC2018Domain: {
				Domain:           HGC2018Domain,
				EnabledTimeUnits: AllTimeUnits(),
				PubsubTopNByGroupingKey: map[identifier.GroupingKey]int{
					Hgc2018TeamGroupingKey: Hgc2018TeamTopN,
				},
				TopMax:                   DefaultTopMax,
				SpreadMax:                DefaultSpreadMax,
				MaxLeaderboards:          DefaultMaxLeaderboards,
				ArchiveRedisLeaderboards: true,
				ArchiveLocalLeaderboards: true,
			},
			Hearthstone2018Domain: {
				Domain:           Hearthstone2018Domain,
				EnabledTimeUnits: AllTimeUnits(),
				PubsubTopNByGroupingKey: map[identifier.GroupingKey]int{
					Hearthstone2018TeamGroupingKey: Hearthstone2018TeamTopN,
				},
				TopMax:                   DefaultTopMax,
				SpreadMax:                DefaultSpreadMax,
				MaxLeaderboards:          DefaultMaxLeaderboards,
				ArchiveRedisLeaderboards: true,
				ArchiveLocalLeaderboards: true,
			},
			Hearthstone2018TestDomain: {
				Domain:           Hearthstone2018TestDomain,
				EnabledTimeUnits: AllTimeUnits(),
				PubsubTopNByGroupingKey: map[identifier.GroupingKey]int{
					Hearthstone2018TestTeamGroupingKey: Hearthstone2018TeamTopN,
				},
				TopMax:                   DefaultTopMax,
				SpreadMax:                DefaultSpreadMax,
				MaxLeaderboards:          DefaultMaxLeaderboards,
				ArchiveRedisLeaderboards: true,
				ArchiveLocalLeaderboards: true,
			},
			Owl2019Domain: {
				Domain:           Owl2019Domain,
				EnabledTimeUnits: AllTimeUnits(),
				PubsubTopNByGroupingKey: map[identifier.GroupingKey]int{
					Owl2019TeamGroupingKey: Owl2019TeamTopN,
				},
				TopMax:                   DefaultTopMax,
				SpreadMax:                DefaultSpreadMax,
				MaxLeaderboards:          Owl2019MaxLeaderboards,
				ArchiveRedisLeaderboards: true,
				ArchiveLocalLeaderboards: true,
			},
			Owl2019TestDomain: {
				Domain:           Owl2019TestDomain,
				EnabledTimeUnits: AllTimeUnits(),
				PubsubTopNByGroupingKey: map[identifier.GroupingKey]int{
					Owl2019TeamGroupingKey: Owl2019TeamTopN,
				},
				TopMax:                   DefaultTopMax,
				SpreadMax:                DefaultSpreadMax,
				MaxLeaderboards:          Owl2019MaxLeaderboards,
				ArchiveRedisLeaderboards: true,
				ArchiveLocalLeaderboards: true,
			},
			PollsDomain: {
				Domain:                   PollsDomain,
				EnabledTimeUnits:         SingleTimeUnit(pantheonrpc.TimeUnit_ALLTIME),
				PubsubTopNByGroupingKey:  map[identifier.GroupingKey]int{},
				TopMax:                   DefaultTopMax,
				SpreadMax:                DefaultSpreadMax,
				MaxLeaderboards:          DefaultMaxLeaderboards,
				ArchiveRedisLeaderboards: true,
				ArchiveLocalLeaderboards: true,
				MaxPublishDelaySeconds:   10,
			},
			HypeTrainDomain: {
				Domain:                   HypeTrainDomain,
				EnabledTimeUnits:         SingleTimeUnit(pantheonrpc.TimeUnit_ALLTIME),
				PubsubTopNByGroupingKey:  map[identifier.GroupingKey]int{},
				TopMax:                   DefaultTopMax,
				SpreadMax:                DefaultSpreadMax,
				MaxLeaderboards:          DefaultMaxLeaderboards,
				ArchiveRedisLeaderboards: false,
				ArchiveLocalLeaderboards: false,
				LeaderboardTTL:           true,
				LeaderboardTTLDuration:   time.Hour * 24 * 30,
			},
			CoGoTrainDomain: Config{
				Domain:                   CoGoTrainDomain,
				EnabledTimeUnits:         SingleTimeUnit(pantheonrpc.TimeUnit_ALLTIME),
				PubsubTopNByGroupingKey:  map[identifier.GroupingKey]int{},
				TopMax:                   DefaultTopMax,
				SpreadMax:                DefaultSpreadMax,
				MaxLeaderboards:          DefaultMaxLeaderboards,
				ArchiveRedisLeaderboards: true,
				ArchiveLocalLeaderboards: true,
			}.WithEnableUserModerationHandling().WithEnableTemporarySuspensionHandling(),
		},
	}
}

func (m *HardCodedConfigManager) Get(domain identifier.Domain) Config {
	config, isPresent := m.domainConfigMapping[domain]
	if !isPresent {
		return UnknownDomainConfig()
	}
	return config
}

func (m *HardCodedConfigManager) GetAll() map[identifier.Domain]Config {
	return m.domainConfigMapping
}
