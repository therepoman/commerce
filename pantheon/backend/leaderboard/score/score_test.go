package score

import (
	"fmt"
	go_math "math"
	"testing"
	"time"

	"code.justin.tv/commerce/pantheon/utils/math"
	"code.justin.tv/commerce/pantheon/utils/random"
	. "github.com/smartystreets/goconvey/convey"
)

func validateEqualityForMultipleBaseScores(time int64, expectedTimeFraction float64, testCase string) {
	Convey(testCase, func() {
		Convey("Test that score computation is correct for a variety of base scores", func() {
			for _, bs := range []int64{0, 1, 10, 100, 1000, 10000, 100000, 1000000, 10000000, 100000000, 1000000000} {
				encodedScore := encodeInput(bs, time).EncodeScore()
				So(encodedScore, ShouldEqual, float64(bs)+expectedTimeFraction)
				So(encodedScore.DecodeScore().BaseScore, ShouldEqual, bs)
			}
		})
	})
}

func encodeInput(score int64, time int64) EntryScore {
	return ToEntryScore(BaseScore(score), EventTime(time), NotModerated)
}

func TestEncodeScore_EdgeCases(t *testing.T) {
	// Times outside the minimum or maximum allowed times should be bound accordingly
	Convey("Test various edge cases", t, func() {
		Convey("Test that scores from before the minimum allowed event time have the maximum time fraction", func() {
			validateEqualityForMultipleBaseScores(0, maximumTimeFraction, "Given a time value of 0")
			validateEqualityForMultipleBaseScores(time.Date(1910, time.January, 1, 0, 0, 0, 0, time.UTC).UnixNano(), maximumTimeFraction, "Given a time value in 1912")
			validateEqualityForMultipleBaseScores(time.Date(2002, time.January, 1, 0, 0, 0, 0, time.UTC).UnixNano(), maximumTimeFraction, "Given a time value in 2002")
			validateEqualityForMultipleBaseScores(MinimumEventTime, maximumTimeFraction, "Given the minimum allowed event time")
		})

		Convey("Test that scores from after the maximum allowed event time have the minimum time fraction", func() {
			validateEqualityForMultipleBaseScores(time.Date(2100, time.January, 1, 0, 0, 0, 0, time.UTC).UnixNano(), float64(1.0)-maximumTimeFraction, "Given a time value in 2100")
			validateEqualityForMultipleBaseScores(time.Date(2055, time.January, 1, 0, 0, 0, 0, time.UTC).UnixNano(), float64(1.0)-maximumTimeFraction, "Given a time value in 2055")
			validateEqualityForMultipleBaseScores(MaximumEventTime, float64(1.0)-maximumTimeFraction, "Given the maximum allowed event time")
		})
	})
}

// This validation function counts up from the start time by adding the time unit duration
// At each step it validates that the new score (at the later time) is less than the previous score
// Due to floating point precision issues there is the possibility of a score colliding with the previous score under certain conditions (hence the acceptableCollisionRate parameter)
func validateTiedScoresDecreaseOverTime(testCase string, baseScore int64, startTime time.Time, timeUnit time.Duration, numberOfSteps int, acceptableCollisionRate float64) {
	Convey(testCase, func() {
		ns := startTime.UnixNano()
		unitNs := timeUnit.Nanoseconds()
		prevScore := Score(baseScore + 1)
		collisions := 0
		for i := 0; i < numberOfSteps; i++ {
			newScore := encodeInput(baseScore, ns).EncodeScore()
			So(newScore, ShouldBeLessThanOrEqualTo, prevScore)
			if prevScore == newScore {
				collisions++
			}
			prevScore = newScore
			ns += unitNs
		}
		collisionRate := float64(collisions) / float64(numberOfSteps)
		So(collisionRate, ShouldBeLessThanOrEqualTo, acceptableCollisionRate)
	})
}

func TestEncodeScore_Ordering(t *testing.T) {
	Convey("Test that base score takes priority and time breaks ties", t, func() {
		t1 := time.Now().UnixNano()
		t2 := time.Now().Add(time.Second).UnixNano()

		s1 := encodeInput(1, t1).EncodeScore()
		s2 := encodeInput(1, t2).EncodeScore()
		s3 := encodeInput(2, t1).EncodeScore()
		s4 := encodeInput(2, t2).EncodeScore()

		Convey("s3 > s4 > s1 > s2", func() {
			So(s3, ShouldBeGreaterThan, s4)
			So(s4, ShouldBeGreaterThan, s1)
			So(s1, ShouldBeGreaterThan, s2)
		})
	})
}

func TestEncodeScore_OrderingOnTies(t *testing.T) {
	Convey("Test the decreasing of scores over time for various time units", t, func() {
		validateTiedScoresDecreaseOverTime("Validate low collisions for millisecond spaced transactions with 1000 base score",
			1000, time.Now(), time.Millisecond, 1000, .005)

		validateTiedScoresDecreaseOverTime("Validate low collisions for second spaced transactions with 10000 base score",
			10000, time.Now(), time.Second, 1000, .005)

		validateTiedScoresDecreaseOverTime("Validate low collisions for second spaced transactions with 100000 base score",
			100000, time.Now(), time.Second, 1000, .005)

		validateTiedScoresDecreaseOverTime("Validate low collisions for second spaced transactions with 1000000 base score",
			1000000, time.Now(), time.Second, 1000, .005)

		validateTiedScoresDecreaseOverTime("Validate lowish collisions for second spaced transactions with 1000000 base score",
			5000000, time.Now(), time.Second, 1000, .1)

		validateTiedScoresDecreaseOverTime("Validate low collisions for 5 second spaced transactions with 10000000 base score",
			10000000, time.Now(), 5*time.Second, 1000, .005)

		validateTiedScoresDecreaseOverTime("Validate no collisions for minute spaced transactions with 10000000 base score",
			10000000, time.Now(), time.Minute, 1000, 0)

		validateTiedScoresDecreaseOverTime("Validate no collisions for hour spaced transactions with 10000000 base score",
			10000000, time.Now(), time.Hour, 1000, 0)

		validateTiedScoresDecreaseOverTime("Validate no collisions for day spaced transactions with 10000000 base score",
			10000000, time.Now(), time.Hour*24, 1000, 0)
	})
}

func validateEventTimesInAcceptableTimeDelta(et1 EventTime, et2 EventTime, maxAllowedTimeDelta time.Duration) {
	timeDelta := time.Duration(math.MaxInt64(int64(et1), int64(et2)) - math.MinInt64(int64(et1), int64(et2)))
	So(timeDelta, ShouldBeLessThanOrEqualTo, maxAllowedTimeDelta)
}

func testRoundTripWithTestCaseName(testCase string, initialBaseScore int64, maxAllowedTimeDelta time.Duration, numChecks int) {
	Convey(testCase, func() {
		t := time.Now()
		for i := 0; i < numChecks; i++ {
			et1 := t.UnixNano()
			s := encodeInput(initialBaseScore, et1).EncodeScore()

			es := s.DecodeScore()
			So(es.BaseScore, ShouldEqual, initialBaseScore)

			validateEventTimesInAcceptableTimeDelta(EventTime(et1), es.EventTime, maxAllowedTimeDelta)

			t = t.Add(random.Duration(time.Millisecond, time.Hour))
		}
	})
}

func testRoundTrip(initialBaseScore int64, maxAllowedTimeDelta time.Duration, numChecks int) {
	testRoundTripWithTestCaseName(
		fmt.Sprintf("Test round trip with base score of %d does not exceed max time delta of %v", initialBaseScore, maxAllowedTimeDelta),
		initialBaseScore, maxAllowedTimeDelta, numChecks)
}

func TestRoundTrip_BaseScoreCorrectness(t *testing.T) {
	Convey("Test that round trip encoding / decoding maintains base score correctness", t, func() {
		numChecksPerScore := 10
		baseScoresTested := map[int64]bool{}
		for i := 1; i <= 500; i++ {
			var baseScore int64
			alreadyTested := true
			for ; alreadyTested; _, alreadyTested = baseScoresTested[baseScore] {
				baseScore = random.Int64(0, 1000000)
			}
			testRoundTripWithTestCaseName(fmt.Sprintf("Round trip test #%d", i), baseScore, 200*time.Millisecond, numChecksPerScore)
			baseScoresTested[baseScore] = true
		}
	})
}

func TestRoundTrip_Precision(t *testing.T) {
	Convey("Test that round trip encoding / decoding maintains an acceptable time precision", t, func() {
		numChecks := 1000
		testRoundTrip(0, 10*time.Microsecond, numChecks)
		testRoundTrip(1, 10*time.Microsecond, numChecks)
		testRoundTrip(10, 10*time.Microsecond, numChecks)
		testRoundTrip(100, 100*time.Microsecond, numChecks)
		testRoundTrip(1000, 100*time.Microsecond, numChecks)
		testRoundTrip(10000, 2*time.Millisecond, numChecks)
		testRoundTrip(100000, 10*time.Millisecond, numChecks)
		testRoundTrip(1000000, 100*time.Millisecond, numChecks)
		testRoundTrip(10000000, 1*time.Second+200*time.Millisecond, numChecks)
		testRoundTrip(100000000, 10*time.Second, numChecks)
		testRoundTrip(1000000000, 2*time.Minute, numChecks)
	})
}

func TestAdd(t *testing.T) {
	Convey("Test various cases of adding scores", t, func() {
		Convey("Test that adding older events doesn't change the time event portion", func() {
			t1 := time.Now()
			initialES := encodeInput(1, t1.UnixNano())
			s := initialES.EncodeScore()

			for i := 1; i <= 100; i++ {
				es := encodeInput(1, t1.Add(time.Duration(i)-time.Hour).UnixNano())
				s = s.Add(es)
			}

			finalES := s.DecodeScore()
			So(finalES.BaseScore, ShouldEqual, BaseScore(101))
			validateEventTimesInAcceptableTimeDelta(initialES.EventTime, finalES.EventTime, 50*time.Microsecond)
		})

		Convey("Test that adding newer events does change the time event portion", func() {
			t1 := time.Now()
			initalES := encodeInput(1, t1.UnixNano())
			s := initalES.EncodeScore()

			var es EntryScore
			for i := 1; i <= 100; i++ {
				es = encodeInput(1, t1.Add(time.Duration(i)+time.Hour).UnixNano())
				s = s.Add(es)
			}
			lastETAdded := es.EventTime

			finalES := s.DecodeScore()
			So(finalES.BaseScore, ShouldEqual, BaseScore(101))
			validateEventTimesInAcceptableTimeDelta(lastETAdded, finalES.EventTime, 50*time.Microsecond)
		})

		Convey("Test that oldest times take precedence in base score ties", func() {
			t1 := time.Now()
			t2 := t1.Add(time.Minute)
			t3 := t1.Add(time.Hour)

			es := encodeInput(1, t1.UnixNano())
			s1 := es.EncodeScore()
			esAdd1 := encodeInput(1, t2.UnixNano())
			s1 = s1.Add(esAdd1)

			s2 := es.EncodeScore()
			esAdd2 := encodeInput(1, t3.UnixNano())
			s2 = s2.Add(esAdd2)

			So(s1, ShouldBeGreaterThan, s2)
		})

		Convey("Test that any time overwrites the time fraction portion of a legacy score", func() {
			oldScore := Score(1)
			t1 := time.Now()

			es := encodeInput(1, t1.UnixNano())
			newScore := oldScore.Add(es)

			newES := newScore.DecodeScore()
			So(newES.BaseScore, ShouldEqual, 2)
			validateEventTimesInAcceptableTimeDelta(es.EventTime, newES.EventTime, 50*time.Microsecond)
		})
	})
}

func TestModeration(t *testing.T) {
	Convey("Test various cases of moderating scores", t, func() {
		Convey("Test that after moderation / unmoderation base score and time fractions components don't change", func() {
			original := encodeInput(1234567, time.Now().UnixNano())

			moderatedScore := original.EncodeScore().Moderate()
			moderated := moderatedScore.DecodeScore()
			So(moderated.ModeratedFlag, ShouldEqual, Moderated)
			So(moderated.BaseScore, ShouldEqual, original.BaseScore)
			validateEventTimesInAcceptableTimeDelta(moderated.EventTime, original.EventTime, time.Second)

			unmoderatedScore := moderatedScore.Unmoderate()
			unmoderated := unmoderatedScore.DecodeScore()
			So(unmoderated.ModeratedFlag, ShouldEqual, NotModerated)
			So(unmoderated.BaseScore, ShouldEqual, original.BaseScore)
			validateEventTimesInAcceptableTimeDelta(unmoderated.EventTime, original.EventTime, time.Second)
		})

		Convey("Test that adding works regardless of whether or not a score is moderated", func() {
			t1 := time.Now()
			scoreNum := int64(123)
			scoreIncr := int64(100)
			original := encodeInput(scoreNum, t1.UnixNano())
			s := original.EncodeScore()
			moderated := false

			var es EntryScore
			for i := int64(1); i <= 100; i++ {
				if i%10 == 0 {
					s = s.Unmoderate()
					moderated = false
				} else if i%5 == 0 {
					s = s.Moderate()
					moderated = true
				}

				es = encodeInput(scoreIncr, t1.Add(time.Duration(i)+time.Hour).UnixNano())
				s = s.Add(es)
				newScore := s.DecodeScore()
				So(newScore.BaseScore, ShouldEqual, scoreNum+scoreIncr*i)
				validateEventTimesInAcceptableTimeDelta(newScore.EventTime, EventTime(t1.Add(time.Duration(i)+time.Hour).UnixNano()), time.Second)

				if moderated {
					So(newScore.ModeratedFlag, ShouldEqual, Moderated)
				} else {
					So(newScore.ModeratedFlag, ShouldEqual, NotModerated)
				}
			}
		})

		Convey("Test moderation of a zero score", func() {
			score := Score(0)
			So(score.DecodeScore().BaseScore, ShouldEqual, BaseScore(0))
			validateEventTimesInAcceptableTimeDelta(score.DecodeScore().EventTime, EventTime(MaximumEventTime), time.Hour)
			So(score.DecodeScore().ModeratedFlag, ShouldEqual, NotModerated)
			So(float64(score), ShouldEqual, 0)
			So(go_math.Signbit(float64(score)), ShouldBeFalse) // positive zero !!!

			score = score.Moderate()
			So(score.DecodeScore().BaseScore, ShouldEqual, BaseScore(0))
			validateEventTimesInAcceptableTimeDelta(score.DecodeScore().EventTime, EventTime(MaximumEventTime), time.Hour)
			So(score.DecodeScore().ModeratedFlag, ShouldEqual, Moderated)
			So(float64(score), ShouldEqual, 0)
			So(go_math.Signbit(float64(score)), ShouldBeTrue) // negative zero !!!

			score = score.Unmoderate()
			So(score.DecodeScore().BaseScore, ShouldEqual, BaseScore(0))
			validateEventTimesInAcceptableTimeDelta(score.DecodeScore().EventTime, EventTime(MaximumEventTime), time.Hour)
			So(score.DecodeScore().ModeratedFlag, ShouldEqual, NotModerated)
			So(float64(score), ShouldEqual, 0)
			So(go_math.Signbit(float64(score)), ShouldBeFalse) // positive zero !!!
		})
	})
}
