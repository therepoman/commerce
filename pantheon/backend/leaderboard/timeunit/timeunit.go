package timeunit

import "code.justin.tv/commerce/pantheon/rpc/pantheonrpc"

func GetTimeUnits() []pantheonrpc.TimeUnit {
	timeUnits := make([]pantheonrpc.TimeUnit, 0)
	for timeUnitInt := range pantheonrpc.TimeUnit_name {
		timeUnit := pantheonrpc.TimeUnit(timeUnitInt)
		timeUnits = append(timeUnits, timeUnit)
	}
	return timeUnits
}

func FromString(strTimeUnit string) (pantheonrpc.TimeUnit, bool) {
	tu, isPresent := pantheonrpc.TimeUnit_value[strTimeUnit]
	return pantheonrpc.TimeUnit(tu), isPresent
}
