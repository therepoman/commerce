package identifier

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
	"time"

	"code.justin.tv/commerce/pantheon/backend/leaderboard/bucket"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/timeunit"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
)

type Domain string

type GroupingKey string

type Identifier struct {
	Domain              Domain
	GroupingKey         GroupingKey
	TimeAggregationUnit pantheonrpc.TimeUnit
	TimeBucket          bucket.TimeBucket
}

func (id Identifier) Key() string {
	return fmt.Sprintf("%s/%s/%s/%s", id.Domain, id.GroupingKey, id.TimeAggregationUnit.String(), id.TimeBucket)
}

func (id Identifier) PubsubIdentifier() string {
	return fmt.Sprintf("%s-%s-%s", id.Domain, id.GroupingKey, id.TimeAggregationUnit.String())
}

func (id Identifier) GetExpirationTime() *time.Time {
	return id.TimeBucket.ToExpirationTime(id.TimeAggregationUnit)
}

func Equals(id1 Identifier, id2 Identifier) bool {
	return id1.Domain == id2.Domain &&
		id1.GroupingKey == id2.GroupingKey &&
		id1.TimeAggregationUnit == id2.TimeAggregationUnit &&
		id1.TimeBucket == id2.TimeBucket
}

func FromKey(key string) (Identifier, error) {
	parts := strings.Split(key, "/")
	if len(parts) != 4 {
		return Identifier{}, errors.New("invalid identifier key: key must have exactly 4 sections delimited by /")
	}

	domain := parts[0]
	if domain == "" {
		return Identifier{}, errors.New("invalid identifier key: domain must not be blank")
	}

	groupingKey := parts[1]
	if groupingKey == "" {
		return Identifier{}, errors.New("invalid identifier key: grouping key must not be blank")
	}

	timeAggregationUnitStr := parts[2]
	timeAggregationUnit, valid := timeunit.FromString(timeAggregationUnitStr)
	if !valid {
		return Identifier{}, errors.New("invalid identifier key: invalid time aggregation unit")
	}

	timeBucketStr := parts[3]
	timeBucketNano, err := strconv.ParseInt(timeBucketStr, 10, 64)
	if err != nil {
		return Identifier{}, errors.New("invalid identifier key: time bucket must be numeric")
	}

	return Identifier{
		Domain:              Domain(domain),
		GroupingKey:         GroupingKey(groupingKey),
		TimeAggregationUnit: timeAggregationUnit,
		TimeBucket:          bucket.TimeBucket(time.Unix(0, timeBucketNano)),
	}, nil
}
