package identifier_test

import (
	"testing"
	"time"

	"code.justin.tv/commerce/pantheon/backend/leaderboard/bucket"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/domain"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/identifier"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	timeUtils "code.justin.tv/commerce/pantheon/utils/time"
	. "github.com/smartystreets/goconvey/convey"
)

func TestFromKey(t *testing.T) {
	Convey("From key conversion", t, func() {
		Convey("fails when key is blank", func() {
			_, err := identifier.FromKey("")
			So(err, ShouldNotBeNil)
		})
		Convey("fails when key contains too few slashes", func() {
			_, err := identifier.FromKey("a/b/c")
			So(err, ShouldNotBeNil)
		})
		Convey("fails when key contains too many slashes", func() {
			_, err := identifier.FromKey("a/b/c/d/e")
			So(err, ShouldNotBeNil)
		})
		Convey("fails when domain is blank", func() {
			_, err := identifier.FromKey("/b/ALLTIME/0")
			So(err, ShouldNotBeNil)
		})
		Convey("fails when grouping key is blank", func() {
			_, err := identifier.FromKey("a//ALLTIME/0")
			So(err, ShouldNotBeNil)
		})
		Convey("fails when time-unit is not valid", func() {
			_, err := identifier.FromKey("a/b/NOTVALID/d")
			So(err, ShouldNotBeNil)
		})
		Convey("fails when time is not numeric", func() {
			_, err := identifier.FromKey("a/b/ALLTIME/d")
			So(err, ShouldNotBeNil)
		})
		Convey("succeeds when everything is valid", func() {
			id, err := identifier.FromKey("a/b/ALLTIME/123")
			So(err, ShouldBeNil)
			So(identifier.Equals(id, identifier.Identifier{
				Domain:              identifier.Domain("a"),
				GroupingKey:         identifier.GroupingKey("b"),
				TimeAggregationUnit: pantheonrpc.TimeUnit_ALLTIME,
				TimeBucket:          bucket.TimeBucket(time.Unix(0, 123)),
			}), ShouldBeTrue)
		})
	})

	Convey("To key conversion", t, func() {
		Convey("with all time", func() {
			id := identifier.Identifier{
				Domain:              domain.SubsGiftSentDomain,
				GroupingKey:         "264051522",
				TimeAggregationUnit: pantheonrpc.TimeUnit_ALLTIME,
				TimeBucket:          bucket.TimestampToTimeBucket(time.Now(), pantheonrpc.TimeUnit_ALLTIME),
			}
			So(id.Key(), ShouldEqual, "sub-gifts-sent/264051522/ALLTIME/0")
		})

		timestamp := time.Date(2020, 12, 05, 12, 30, 0, 0, timeUtils.GetTimeZone())

		Convey("with yearly time", func() {
			timeUnit := pantheonrpc.TimeUnit_YEAR
			id := identifier.Identifier{
				Domain:              domain.SubsGiftSentDomain,
				GroupingKey:         "58682589",
				TimeAggregationUnit: timeUnit,
				TimeBucket:          bucket.TimestampToTimeBucket(timestamp, timeUnit),
			}
			So(id.Key(), ShouldEqual, "sub-gifts-sent/58682589/YEAR/1577865600000000000")
		})

		Convey("with monthly time", func() {
			timeUnit := pantheonrpc.TimeUnit_MONTH
			id := identifier.Identifier{
				Domain:              domain.SubsGiftSentDomain,
				GroupingKey:         "88571417",
				TimeAggregationUnit: timeUnit,
				TimeBucket:          bucket.TimestampToTimeBucket(timestamp, timeUnit),
			}
			So(id.Key(), ShouldEqual, "sub-gifts-sent/88571417/MONTH/1606809600000000000")
		})

		Convey("with weekly time", func() {
			timeUnit := pantheonrpc.TimeUnit_WEEK
			id := identifier.Identifier{
				Domain:              domain.SubsGiftSentDomain,
				GroupingKey:         "265716088",
				TimeAggregationUnit: timeUnit,
				TimeBucket:          bucket.TimestampToTimeBucket(timestamp, timeUnit),
			}
			So(id.Key(), ShouldEqual, "sub-gifts-sent/265716088/WEEK/1606723200000000000")
		})

		Convey("with daily time", func() {
			timeUnit := pantheonrpc.TimeUnit_DAY
			id := identifier.Identifier{
				Domain:              domain.SubsGiftSentDomain,
				GroupingKey:         "58587462",
				TimeAggregationUnit: timeUnit,
				TimeBucket:          bucket.TimestampToTimeBucket(timestamp, timeUnit),
			}
			So(id.Key(), ShouldEqual, "sub-gifts-sent/58587462/DAY/1607155200000000000")
		})
	})
}
