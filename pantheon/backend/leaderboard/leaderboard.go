package leaderboard

import (
	"context"
	"time"

	"code.justin.tv/commerce/pantheon/backend/leaderboard/entry"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/event"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/identifier"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/score"
	"code.justin.tv/commerce/pantheon/clients/redis"
)

type RetrievalSettings struct {
	ConsistentRead bool
	SkipImport     bool
}

func (rs RetrievalSettings) ToRedisReadSettings() redis.ReadSettings {
	return redis.ReadSettings{
		ConsistentRead: rs.ConsistentRead,
	}
}

type Factory interface {
	GetLeaderboard(ctx context.Context, id identifier.Identifier, rs RetrievalSettings) (Leaderboard, error)
}

type RedisLeaderboardFactory interface {
	GetRedisLeaderboard(ctx context.Context, id identifier.Identifier, rs RetrievalSettings) (Leaderboard, error)
}

type ScoreSetter interface {
	UpdateScore(key entry.Key, score score.EntryScore, event *event.Event) error
	ClearScores() error
}

type ScoreRetriever interface {
	Len(rs RetrievalSettings) (int, error)
	GetScore(key entry.Key, rs RetrievalSettings) (score.Score, error)
	GetTop(ctx context.Context, n int, rs RetrievalSettings) ([]entry.RankedEntry, error)
	// Calls GetTop wrapped in a context.WithTimeout, if the deadline is exceeded it retries up to the maximum number of attempts
	GetTopWithTimeoutAndRetry(ctx context.Context, n int, rs RetrievalSettings, timeout time.Duration, maxAttempts int) ([]entry.RankedEntry, error)

	// GetEntryContext gets [spread] many entries before and after the specified entry
	GetEntryContext(ctx context.Context, key entry.Key, spread int, rs RetrievalSettings) (entry entry.RankedEntry, context []entry.RankedEntry, err error)
	// Calls GetEntryContext wrapped in a context.WithTimeout, if the deadline is exceeded it retries up to the maximum number of attempts
	GetEntryContextWithTimeoutAndRetry(ctx context.Context, key entry.Key, spread int, rs RetrievalSettings, timeout time.Duration, maxAttempts int) (entryOut entry.RankedEntry, entries []entry.RankedEntry, err error)

	GetRank(key entry.Key, rs RetrievalSettings) (entry.Rank, error)

	GetCountForRange(ctx context.Context, min *int64, max *int64, rs RetrievalSettings) (int64, error)

	GetEntriesAboveThreshold(threshold, limit, offset int64, rs RetrievalSettings) ([]entry.RankedEntry, error)
}

type EntryModerator interface {
	ModerateEntry(key entry.Key) error
	UnmoderateEntry(key entry.Key) error
	DeleteEntry(key entry.Key) error
}

type Serializable interface {
	Serialize(rs RetrievalSettings) ([]byte, error)
	Restore(bs []byte) error
}

type Leaderboard interface {
	ScoreSetter
	ScoreRetriever
	EntryModerator
	Serializable
	Type() string
	Destroy() error
}
