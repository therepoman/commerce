package ingestion

import (
	"context"
	"errors"
	"fmt"

	dynamo_event "code.justin.tv/commerce/pantheon/backend/dynamo/event"
	"code.justin.tv/commerce/pantheon/backend/leaderboard"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/bucket"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/domain"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/entry"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/event"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/identifier"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/redis"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/redis/shadow"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/score"
	"code.justin.tv/commerce/pantheon/backend/leaderboard/timeunit"
	"code.justin.tv/commerce/pantheon/clients/tenant"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	time_utils "code.justin.tv/commerce/pantheon/utils/time"
	log "github.com/sirupsen/logrus"
	"github.com/uber-go/tally"
)

const (
	ingestMismatchEventMetric  = "ingest-failure-mismatch-event"
	ingestDuplicateEventMetric = "ingest-noop-duplicate-event"
	ingestSuccessMetric        = "ingest-success"
)

var metricsTags = map[string]string{"queue": "publish-events"}

type Ingester interface {
	Ingest(ctx context.Context, e *event.Event) error
}

type LeaderboardIngester struct {
	DomainConfigManager domain.ConfigManager          `inject:""`
	LeaderboardFactory  redis.LeaderboardFactory      `inject:""`
	IndexFactory        redis.GroupingKeyIndexFactory `inject:""`
	ShadowKeyManager    shadow.KeyManager             `inject:""`
	RootMetricsScope    tally.Scope                   `inject:""`
	TenantClient        tenant.Landlord               `inject:""`
}

func (i *LeaderboardIngester) Ingest(ctx context.Context, e *event.Event) error {
	err := validateEvent(e)
	if err != nil {
		return err
	}

	var timeUnits []pantheonrpc.TimeUnit
	domainConfig := i.DomainConfigManager.Get(identifier.Domain(e.Domain))

	if domainConfig.IsKnownDomain() && len(domainConfig.EnabledTimeUnits) > 0 {
		timeUnits = make([]pantheonrpc.TimeUnit, 0)
		for timeUnit := range domainConfig.EnabledTimeUnits {
			timeUnits = append(timeUnits, timeUnit)
		}
	} else {
		timeUnits = timeunit.GetTimeUnits()
	}

	for _, timeUnit := range timeUnits {
		err = i.ingestForTimeUnit(ctx, e, timeUnit)
		if err != nil {
			return err
		}
	}
	return nil
}

func validateEvent(e *event.Event) error {
	if e == nil {
		return errors.New("cannot ingest nil event")
	}

	if e.ID == "" {
		return errors.New("cannot ingest event with empty ID")
	}

	if e.Domain == "" {
		return errors.New("cannot ingest event with blank domain")
	}

	if e.EntryKey == "" {
		return errors.New("cannot ingest event with empty entry key")
	}

	return nil
}

func (i *LeaderboardIngester) ingestForTimeUnit(ctx context.Context, event *event.Event, timeUnit pantheonrpc.TimeUnit) error {
	timeOfEvent := time_utils.NanosecondsToTime(event.TimeOfEvent)
	timeBucket := bucket.TimestampToTimeBucket(timeOfEvent, timeUnit)
	id := identifier.Identifier{
		Domain:              identifier.Domain(event.Domain),
		GroupingKey:         identifier.GroupingKey(event.GroupingKey),
		TimeAggregationUnit: timeUnit,
		TimeBucket:          timeBucket,
	}

	logFields := log.Fields{
		"leaderboard_id": id.Key(),
		"event_id":       event.ID,
	}

	existingEvent, err := i.TenantClient.GetTenant(id.Domain).EventDAO.GetEvent(id.Key(), event.ID)
	if err != nil {
		log.WithError(err).WithFields(logFields).Error("error getting event from dynamo prior to redis ingestion")
		return err
	}

	if existingEvent != nil {
		if existingEvent.EntryKey != event.EntryKey && existingEvent.EntryKey != "" {
			i.RootMetricsScope.Tagged(metricsTags).Counter(ingestMismatchEventMetric).Inc(1)
			return fmt.Errorf("event key (%s) for event_id (%s) does not match previously recorded key (%s)", event.EntryKey, event.ID, existingEvent.EntryKey)
		}
		if existingEvent.Value != event.EventValue {
			i.RootMetricsScope.Tagged(metricsTags).Counter(ingestMismatchEventMetric).Inc(1)
			return fmt.Errorf("event value (%d) for event_id (%s) does not match previously recorded value (%d)", event.EventValue, event.ID, existingEvent.Value)
		}
		i.RootMetricsScope.Tagged(metricsTags).Counter(ingestDuplicateEventMetric).Inc(1)
		return nil
	}

	lb, err := i.LeaderboardFactory.GetRedisLeaderboard(ctx, id, leaderboard.RetrievalSettings{ConsistentRead: true})
	if err != nil {
		log.WithError(err).WithFields(logFields).Error("error getting leaderboard from redis")
		return err
	}

	previousScore, err := lb.GetScore(entry.Key(event.EntryKey), leaderboard.RetrievalSettings{ConsistentRead: true})
	if err != nil {
		log.WithError(err).WithFields(logFields).Error("error getting entry from redis")
		return err
	}

	scoreAddition := score.ToEntryScore(score.BaseScore(event.EventValue), score.EventTime(event.TimeOfEvent), score.NotModerated)
	err = lb.UpdateScore(entry.Key(event.EntryKey), scoreAddition, event)
	if err != nil {
		log.WithError(err).WithFields(logFields).Error("error updating leaderboard's score in redis")
		return err
	}

	groupingKeyIndex := i.IndexFactory.GetRedisGroupingKeyIndex(id.Domain, id.GroupingKey)
	err = groupingKeyIndex.Add(ctx, id)
	if err != nil {
		log.WithError(err).WithFields(logFields).Error("error updating grouping key index in redis")
		return err
	}

	err = i.TenantClient.GetTenant(id.Domain).EventDAO.UpdateEvent(&dynamo_event.Event{
		LeaderboardID: id.Key(),
		EventID:       event.ID,
		EntryKey:      event.EntryKey,
		Value:         event.EventValue,
		Timestamp:     int64(event.TimeOfEvent),
	})
	if err != nil {
		log.WithError(err).WithFields(logFields).Error("error adding dynamo event after ingesting into redis")
		return err
	}

	// If the previous score was zero, we may need to moderate / unmoderate now that the entry is in redis
	// depending on the value of the current dynamo moderation status
	if previousScore.DecodeScore().BaseScore == 0 {
		moderationStatus, err := i.TenantClient.GetTenant(id.Domain).ModerationDAO.GetModeration(event.ToLeaderboardSetID(), event.EntryKey)
		if err != nil {
			log.WithFields(logFields).WithError(err).Error("could not get moderation record from dynamo")
		} else if moderationStatus != nil {

			if moderationStatus.State == ModerationStatusString(pantheonrpc.ModerationAction_MODERATE) &&
				previousScore.DecodeScore().ModeratedFlag == score.NotModerated {

				// If dynamo says the entry IS moderated, but it is NOT moderated in redis -> MODERATE the entry
				err := lb.ModerateEntry(entry.Key(event.EntryKey))
				if err != nil {
					log.WithFields(logFields).WithError(err).Error("could not moderate entry during ingestion")
				}

			} else if moderationStatus.State == ModerationStatusString(pantheonrpc.ModerationAction_UNMODERATE) &&
				previousScore.DecodeScore().ModeratedFlag == score.Moderated {

				// If dynamo says the entry is NOT moderated, but it IS moderated in redis -> UNMODERATE the entry
				err := lb.UnmoderateEntry(entry.Key(event.EntryKey))
				if err != nil {
					log.WithFields(logFields).WithError(err).Error("could not unmoderate entry during ingestion")
				}

			}
		}
	}

	err = i.ShadowKeyManager.EnsureShadowKeyExists(id)
	if err != nil {
		log.WithField("key", id.Key()).WithError(err).Error("could not ensure leaderboard shadow key exists")
	}

	i.RootMetricsScope.Tagged(metricsTags).Counter(ingestSuccessMetric).Inc(1)

	return nil
}

func ModerationStatusString(moderationStatus pantheonrpc.ModerationAction) string {
	moderationStatusString, ok := pantheonrpc.ModerationAction_name[int32(moderationStatus)]
	if !ok {
		log.WithField("moderation_status", moderationStatus).Error("received unknown moderation status")
	}
	return moderationStatusString
}
